package com.singlee.ifs.model;

import javax.persistence.Transient;
import java.io.Serializable;

/***
 * 审批流程 基础表
 * @author singlee4
 *
 */
public class IfsBaseFlow   implements Serializable{
    private static final long serialVersionUID = 1L;
	//审批状态
	private String approveStatus;
	
	//审批发起人
	private String sponsor;
	
	//审批发起机构
	private String sponInst;
	
	//审批开始日期
	private String aDate;
	
	/**opics交易要素  start*/
	//模块
	private String groupMod;
	
	//组合名称
	private String groupName;
	
	//投资组合
	private String port;
	
	//产品代码
	private String product;
	
	//产品类型
	private String prodType;
	
	//成本中心
	private String cost;
	
	//交易员
	private String trader;
	
	//备注
	private String note;
	
	//录入时间
	private String inputTime;
	
	//最后修改时间
	private String lastTime;
	
	//分组ID
	private String groupId;
	
	//清算方式
	private String settleMeans;
	
	//清算账户
	private String settleAcct;
	
	//复核标识
	private String verifySign;
	
	//授权标识
	private String grantSign;
	
	//交易来源
	private String dealSource;
	/**opics交易要素  end*/
	
	//成交编号
	private String contractId;
	
	//本币-本方方向
	private String myDir;
	
	//本币-对方方向
	private String oppoDir;
		
	//流程id
	@Transient
	private String taskId;
	//流程Name
	@Transient
	private String taskName;
	
	//审批发起机构
	@Transient
	private String instName;
	
	//审批发起人
	@Transient
	private String userName;
	//本方交易员中文名
	@Transient
	private String myUserName;
	//交易对手编号
	@Transient
	private String cno;
	
	//opics相关的交易号
	private String dealNo;
	
	/**
	 * opics处理状态
	 */
	private String statcode;
	/**
	 * opics错误码
	 */
	private String errorcode;
	
	//清算路径1
	private String ccysmeans;
	//清算账户1
	private String ccysacct;
	//清算路径2
	private String ctrsmeans;
	//清算账户2
	private String ctrsacct;
	
	
	//交易模式
	private String tradingModel;
	//净额清算状态
	private String nettingStatus;
	
	//延期交易状态
	private String delaydElivind;
	
	/**
	 * 授信占用主体
	 */
	private String custNo;
	
	
	@Transient
	private String startDate;
	
	@Transient
	private String endDate;
	
	@Transient
	private String br;
	
	@Transient
	private String branchId;
	
	@Transient
	private String instfullname;
	
	
	public String getCustNo() {
		return custNo;
	}

	public void setCustNo(String custNo) {
		this.custNo = custNo;
	}

	public String getCcysmeans() {
		return ccysmeans;
	}

	public void setCcysmeans(String ccysmeans) {
		this.ccysmeans = ccysmeans;
	}

	public String getCcysacct() {
		return ccysacct;
	}

	public void setCcysacct(String ccysacct) {
		this.ccysacct = ccysacct;
	}

	public String getCtrsmeans() {
		return ctrsmeans;
	}

	public void setCtrsmeans(String ctrsmeans) {
		this.ctrsmeans = ctrsmeans;
	}

	public String getCtrsacct() {
		return ctrsacct;
	}

	public void setCtrsacct(String ctrsacct) {
		this.ctrsacct = ctrsacct;
	}

	public String getApproveStatus() {
		return approveStatus;
	}

	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}

	public String getSponsor() {
		return sponsor;
	}

	public void setSponsor(String sponsor) {
		this.sponsor = sponsor;
	}

	public String getSponInst() {
		return sponInst;
	}

	public void setSponInst(String sponInst) {
		this.sponInst = sponInst;
	}


	public String getaDate() {
		return aDate;
	}

	public void setaDate(String aDate) {
		this.aDate = aDate;
	}

	public String getTaskId() {
		return taskId;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getInstName() {
		return instName;
	}

	public void setInstName(String instName) {
		this.instName = instName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getMyUserName() {
		return myUserName;
	}

	public void setMyUserName(String myUserName) {
		this.myUserName = myUserName;
	}

	public String getGroupMod() {
		return groupMod;
	}

	public void setGroupMod(String groupMod) {
		this.groupMod = groupMod;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getProdType() {
		return prodType;
	}

	public void setProdType(String prodType) {
		this.prodType = prodType;
	}

	public String getCost() {
		return cost;
	}

	public void setCost(String cost) {
		this.cost = cost;
	}

	public String getTrader() {
		return trader;
	}

	public void setTrader(String trader) {
		this.trader = trader;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getInputTime() {
		return inputTime;
	}

	public void setInputTime(String inputTime) {
		this.inputTime = inputTime;
	}

	public String getLastTime() {
		return lastTime;
	}

	public void setLastTime(String lastTime) {
		this.lastTime = lastTime;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getSettleMeans() {
		return settleMeans;
	}

	public void setSettleMeans(String settleMeans) {
		this.settleMeans = settleMeans;
	}

	public String getSettleAcct() {
		return settleAcct;
	}

	public void setSettleAcct(String settleAcct) {
		this.settleAcct = settleAcct;
	}

	public String getVerifySign() {
		return verifySign;
	}

	public void setVerifySign(String verifySign) {
		this.verifySign = verifySign;
	}

	public String getGrantSign() {
		return grantSign;
	}

	public void setGrantSign(String grantSign) {
		this.grantSign = grantSign;
	}

	public String getDealSource() {
		return dealSource;
	}

	public void setDealSource(String dealSource) {
		this.dealSource = dealSource;
	}

	public String getContractId() {
		return contractId;
	}

	public void setContractId(String contractId) {
		this.contractId = contractId;
	}

	public String getMyDir() {
		return myDir;
	}

	public void setMyDir(String myDir) {
		this.myDir = myDir;
	}

	public String getOppoDir() {
		return oppoDir;
	}

	public void setOppoDir(String oppoDir) {
		this.oppoDir = oppoDir;
	}

	public String getCno() {
		return cno;
	}

	public void setCno(String cno) {
		this.cno = cno;
	}

	public String getDealNo() {
		return dealNo;
	}

	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}

	public void setStatcode(String statcode) {
		this.statcode = statcode;
	}

	public String getStatcode() {
		return statcode;
	}

	public void setErrorcode(String errorcode) {
		this.errorcode = errorcode;
	}

	public String getErrorcode() {
		return errorcode;
	}

	public void setNettingStatus(String nettingStatus) {
		this.nettingStatus = nettingStatus;
	}

	public String getNettingStatus() {
		return nettingStatus==null?"0":nettingStatus;
	}

	public void setTradingModel(String tradingModel) {
		this.tradingModel = tradingModel;
	}

	public String getTradingModel() {
		return tradingModel;
	}

	public String getDelaydElivind() {
		return delaydElivind;
	}

	public void setDelaydElivind(String delaydElivind) {
		this.delaydElivind = delaydElivind;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getBr() {
		return br;
	}

	public void setInstfullname(String instfullname) {
		this.instfullname = instfullname;
	}

	public String getInstfullname() {
		return instfullname;
	}

	public String getBranchId() {
		return branchId;
	}

	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}
}
