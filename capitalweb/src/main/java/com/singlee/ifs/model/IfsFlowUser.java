package com.singlee.ifs.model;

import java.io.Serializable;

public class IfsFlowUser implements Serializable{
	private String productType;
	private String condition;
	private String isActiviti;
	private String amt;
	private String userId;
	private static final long serialVersionUID = 1L;
	
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public String getCondition() {
		return condition;
	}
	public void setCondition(String condition) {
		this.condition = condition;
	}
	public String getIsActiviti() {
		return isActiviti;
	}
	public void setIsActiviti(String isActiviti) {
		this.isActiviti = isActiviti;
	}
	public String getAmt() {
		return amt;
	}
	public void setAmt(String amt) {
		this.amt = amt;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	
}
