package com.singlee.ifs.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class IfsIntfcSeclStatus implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String br;
	
	private String secid;
	
	private String chois_Ref_No;
	
	private String chois_His_No;
	
	private BigDecimal clsPrice_8;
	
	private String ccy;
	
	private String postdate;
	
	private String sendTime;
	
	private String isSend;
	
	private String isReSend;
	
	private String sendResult;
	
	private String errMsg;
	
	private String sendMsg;
	
	private String tableName;
	
	private int upCount;
	
	private String upOwn;
	 
	private String upTime;
	
	private String inputTime;

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getSecid() {
		return secid;
	}

	public void setSecid(String secid) {
		this.secid = secid;
	}

	public String getChois_Ref_No() {
		return chois_Ref_No;
	}

	public void setChois_Ref_No(String chois_Ref_No) {
		this.chois_Ref_No = chois_Ref_No;
	}

	public String getChois_His_No() {
		return chois_His_No;
	}

	public void setChois_His_No(String chois_His_No) {
		this.chois_His_No = chois_His_No;
	}

	public BigDecimal getClsPrice_8() {
		return clsPrice_8;
	}

	public void setClsPrice_8(BigDecimal clsPrice_8) {
		this.clsPrice_8 = clsPrice_8;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getPostdate() {
		return postdate;
	}

	public void setPostdate(String postdate) {
		this.postdate = postdate;
	}

	public String getSendTime() {
		return sendTime;
	}

	public void setSendTime(String sendTime) {
		this.sendTime = sendTime;
	}

	public String getIsSend() {
		return isSend;
	}

	public void setIsSend(String isSend) {
		this.isSend = isSend;
	}

	public String getIsReSend() {
		return isReSend;
	}

	public void setIsReSend(String isReSend) {
		this.isReSend = isReSend;
	}

	public String getSendResult() {
		return sendResult;
	}

	public void setSendResult(String sendResult) {
		this.sendResult = sendResult;
	}

	public String getSendMsg() {
		return sendMsg;
	}

	public void setSendMsg(String sendMsg) {
		this.sendMsg = sendMsg;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public int getUpCount() {
		return upCount;
	}

	public void setUpCount(int upCount) {
		this.upCount = upCount;
	}

	public String getUpOwn() {
		return upOwn;
	}

	public void setUpOwn(String upOwn) {
		this.upOwn = upOwn;
	}

	public String getUpTime() {
		return upTime;
	}

	public void setUpTime(String upTime) {
		this.upTime = upTime;
	}

	public String getInputTime() {
		return inputTime;
	}

	public void setInputTime(String inputTime) {
		this.inputTime = inputTime;
	}

	public String getErrMsg() {
		return errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}
	
	
	

}
