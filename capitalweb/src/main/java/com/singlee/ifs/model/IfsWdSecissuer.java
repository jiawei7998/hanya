package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;

@Entity
@Table(name = "IFS_WD_SECISSUER")
public class IfsWdSecissuer implements Serializable{
	private static final long serialVersionUID = -5552204956986113465L;
	private String issuerid;
	private String  industry;
	private String  industry2;
	private String  property;
	private String  plat1;
	private String  plat2;
	private String  plat3;
	@Transient
	private String vDate;
	public String getIssuerid() {
		return issuerid;
	}
	public void setIssuerid(String issuerid) {
		this.issuerid = issuerid;
	}
	public String getIndustry() {
		return industry;
	}
	public void setIndustry(String industry) {
		this.industry = industry;
	}
	public String getIndustry2() {
		return industry2;
	}
	public void setIndustry2(String industry2) {
		this.industry2 = industry2;
	}
	public String getProperty() {
		return property;
	}
	public void setProperty(String property) {
		this.property = property;
	}
	public String getPlat1() {
		return plat1;
	}
	public void setPlat1(String plat1) {
		this.plat1 = plat1;
	}
	public String getPlat2() {
		return plat2;
	}
	public void setPlat2(String plat2) {
		this.plat2 = plat2;
	}
	public String getPlat3() {
		return plat3;
	}
	public void setPlat3(String plat3) {
		this.plat3 = plat3;
	}
	public void setvDate(String vDate) {
		this.vDate = vDate;
	}
	public String getvDate() {
		return vDate;
	}

}
