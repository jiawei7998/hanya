package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;
@Entity
@Table(name = "IFS_APPROVERMB_IRS")
public class IfsApprovermbIrs extends IfsApproveBaseBean {
    private String productName;

    private String tenor;

    private BigDecimal lastQty;

    private String interestRateAdjustment;

    private String startDate;

    private String firstPeriodStartDate;

    private String endDate;

    private String couponPaymentDateReset;

    private String calculateAgency;

    private String tradeMethod;

    private static final long serialVersionUID = 1L;

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}


	public String getTenor() {
		return tenor;
	}

	public void setTenor(String tenor) {
		this.tenor = tenor;
	}

	public BigDecimal getLastQty() {
		return lastQty;
	}

	public void setLastQty(BigDecimal lastQty) {
		this.lastQty = lastQty;
	}

	public String getInterestRateAdjustment() {
		return interestRateAdjustment;
	}

	public void setInterestRateAdjustment(String interestRateAdjustment) {
		this.interestRateAdjustment = interestRateAdjustment;
	}

	@Override
    public String getStartDate() {
		return startDate;
	}

	@Override
    public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getFirstPeriodStartDate() {
		return firstPeriodStartDate;
	}

	public void setFirstPeriodStartDate(String firstPeriodStartDate) {
		this.firstPeriodStartDate = firstPeriodStartDate;
	}

	@Override
    public String getEndDate() {
		return endDate;
	}

	@Override
    public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getCouponPaymentDateReset() {
		return couponPaymentDateReset;
	}

	public void setCouponPaymentDateReset(String couponPaymentDateReset) {
		this.couponPaymentDateReset = couponPaymentDateReset;
	}

	public String getCalculateAgency() {
		return calculateAgency;
	}

	public void setCalculateAgency(String calculateAgency) {
		this.calculateAgency = calculateAgency;
	}

	public String getTradeMethod() {
		return tradeMethod;
	}

	public void setTradeMethod(String tradeMethod) {
		this.tradeMethod = tradeMethod;
	}

}