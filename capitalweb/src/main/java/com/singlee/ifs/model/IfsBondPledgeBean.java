package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * 质押券管理表
 * @Author zhangkai
 * @Description
 * @Date
 */
@Entity
@Table(name = "IFS_BOND_PLEDGE")
public class IfsBondPledgeBean implements Serializable {

    private static final long serialVersionUID = -339272511890496052L;
    private String ticketId;
    private String bondCode;
    private String port;
    private String cost;
    private String invType;
    private String descr;
    private String faceValue;
    private String cleanPrice;
    private String vdate;
    private String mdate;

    public String getTicketId() {
        return ticketId;
    }

    public void setTicketId(String ticketId) {
        this.ticketId = ticketId;
    }

    public String getBondCode() {
        return bondCode;
    }

    public void setBondCode(String bondCode) {
        this.bondCode = bondCode;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getInvType() {
        return invType;
    }

    public void setInvType(String invType) {
        this.invType = invType;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public String getFaceValue() {
        return faceValue;
    }

    public void setFaceValue(String faceValue) {
        this.faceValue = faceValue;
    }

    public String getCleanPrice() {
        return cleanPrice;
    }

    public void setCleanPrice(String cleanPrice) {
        this.cleanPrice = cleanPrice;
    }

    public String getVdate() {
        return vdate;
    }

    public void setVdate(String vdate) {
        this.vdate = vdate;
    }

    public String getMdate() {
        return mdate;
    }

    public void setMdate(String mdate) {
        this.mdate = mdate;
    }
}
