package com.singlee.ifs.model;

import javax.persistence.*;

/***
 * 交易要素  实体对象
 * @author singlee4
 *
 */
@Entity
@Table(name="IFS_Trade_PARAGROUP")
public class IfsTradeParaGroup {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT SEQ_IFS_TRDPARAM.NEXTVAL FROM DUAL")
	//分组ID
	private String groupId;
	
	//交易要素组合
	private String element;
	
	//产品代码
	private String product;
	
	//产品类型
	private String prodType;
	
	//计息规则
	private String intcalcrule;
	
	//交易员
	private String trader;
	
	//备注
	private String note;
	
	//录入时间
	private String inputTime;
	
	//最后修改时间
	private String lastTime;

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getElement() {
		return element;
	}

	public void setElement(String element) {
		this.element = element;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getProdType() {
		return prodType;
	}

	public String getIntcalcrule() {
		return intcalcrule;
	}

	public void setIntcalcrule(String intcalcrule) {
		this.intcalcrule = intcalcrule;
	}

	public void setProdType(String prodType) {
		this.prodType = prodType;
	}

	public String getTrader() {
		return trader;
	}

	public void setTrader(String trader) {
		this.trader = trader;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getInputTime() {
		return inputTime;
	}

	public void setInputTime(String inputTime) {
		this.inputTime = inputTime;
	}

	public String getLastTime() {
		return lastTime;
	}

	public void setLastTime(String lastTime) {
		this.lastTime = lastTime;
	}
}
