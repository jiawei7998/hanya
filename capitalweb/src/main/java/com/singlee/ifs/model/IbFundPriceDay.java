package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "FT_PRICE_DAILY")
public class IbFundPriceDay implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  /**
   * 基金代码
   */
  @Id
  private String fundCode;
  /**
   * 账务日期
   */
  @Id
  private String postdate;
  /**
   * 单位净值/万分收益率
   */
  private BigDecimal fundPrice;

  @Transient
  private String fundName;

  @Transient
  private int versionId;

  @Transient
  private String fundType;

  public int getVersionId() {
    return versionId;
  }

  public void setVersionId(int versionId) {
    this.versionId = versionId;
  }

  public String getFundCode() {
    return fundCode;
  }

  public void setFundCode(String fundCode) {
    this.fundCode = fundCode;
  }

  public String getPostdate() {
    return postdate;
  }

  public void setPostdate(String postdate) {
    this.postdate = postdate;
  }

  public BigDecimal getFundPrice() {
    return fundPrice;
  }

  public void setFundPrice(BigDecimal fundPrice) {
    this.fundPrice = fundPrice;
  }

  public String getFundName() {
    return fundName;
  }

  public void setFundName(String fundName) {
    this.fundName = fundName;
  }

  public String getFundType() {
    return fundType;
  }

  public void setFundType(String fundType) {
    this.fundType = fundType;
  }

}