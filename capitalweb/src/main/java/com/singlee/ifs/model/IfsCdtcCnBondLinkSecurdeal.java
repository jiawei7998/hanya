package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "IFS_CDTC_CNBONDLINK_SECURDEAL")
public class IfsCdtcCnBondLinkSecurdeal implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6611919394137641895L;
	//机构
	private String br;
	//交易号
	private String dealNo;
	//ps
	private String ps;
	//产品代码
	private String prodCode;
	//债券标识符
	private String secid;
	//债券名称
	private String secidNm;
	//cif
	private String cif;
	//客户名称
	private String custNm;
	//交易中心编号
	private String cfetsSn;
	//交易日期
	private Date dealDate;
	//起息日
	private Date vDate;
	//到期日
	private Date matDate; 
	//票面金额
	private BigDecimal faceAmt;
	//首期金额/净价金额
	private BigDecimal comProcdAmt;
	//到期金额/全价金额
	private BigDecimal matProcdAmt;
	//中债首期交割金额/债券交割金额
	private BigDecimal comProcdAmtZz;
	//中债到期交割金额
	private BigDecimal matProcdAmtZz;
	//首期交割金额/债券交割金额差额
	private BigDecimal comProcdAmtSub;
	//到期交割金额差额
	private BigDecimal matProcdAmtSub;
	//清算状态(交易状态)
	private String dealStatus;
	//交易来源
	private String dealText;
	//首期CCY 结算方式
	private String comCcysMeans;
	//到期CCY 结算方式
	private String matCcysMeans;
	//原始交易单结算方式 首期
	private String settTypeB;
	//原始交易单结算方式描述 首期
	private String settDescrB;
	//原始交易单结算方式 到期
	private String settTypeE;
	//原始交易单结算方式描述 到期
	private String settDescrE;
	private String xmlMatChid;
	//CHECK状态
	private String checkSts;
	//CHECK时间
	private Date checkTime;
	//请求编号
	private String requeStsn;
	//发送编号
	private String sendSts;
	//发送时间
	private Date sendTime;
	//发送返回的状态
	private String cnBondRetSts;
	//返回时间
	private Date cnBondRetTime;
	//债券所属账户ID
	private String acctQryAcctId;
	//结算指令标识
	private String instrId;
	//交易流水标识
	private String txFlowID;
	//指令处理状态
	private String instrSts;
	//指令处理结果返回码
	private String txRsltCd;
	//指令返回信息描述
	private String insErrInf;
	//发令方确认标识
	private String orgtrCnfrmInd;
	//对手方确认标识
	private String ctrCnfrmInd;
	//结算合同标识
	private String ctrctID;
	//合同处理状态
	private String ctrctSts;
	//合同冻结状态
	private String ctrctBlckSts;
	//合同失败原因
	private String ctrctFaildRsn;
	//查询返回时间
	private String queryRetTime;
	public String getBr() {
		return br;
	}
	public void setBr(String br) {
		this.br = br;
	}
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public String getPs() {
		return ps;
	}
	public void setPs(String ps) {
		this.ps = ps;
	}
	public String getProdCode() {
		return prodCode;
	}
	public void setProdCode(String prodCode) {
		this.prodCode = prodCode;
	}
	public String getSecid() {
		return secid;
	}
	public void setSecid(String secid) {
		this.secid = secid;
	}
	public String getSecidNm() {
		return secidNm;
	}
	public void setSecidNm(String secidNm) {
		this.secidNm = secidNm;
	}
	public String getCif() {
		return cif;
	}
	public void setCif(String cif) {
		this.cif = cif;
	}
	public String getCustNm() {
		return custNm;
	}
	public void setCustNm(String custNm) {
		this.custNm = custNm;
	}
	public String getCfetsSn() {
		return cfetsSn;
	}
	public void setCfetsSn(String cfetsSn) {
		this.cfetsSn = cfetsSn;
	}
	public Date getDealDate() {
		return dealDate;
	}
	public void setDealDate(Date dealDate) {
		this.dealDate = dealDate;
	}
	public Date getvDate() {
		return vDate;
	}
	public void setvDate(Date vDate) {
		this.vDate = vDate;
	}
	public Date getMatDate() {
		return matDate;
	}
	public void setMatDate(Date matDate) {
		this.matDate = matDate;
	}
	public BigDecimal getFaceAmt() {
		return faceAmt;
	}
	public void setFaceAmt(BigDecimal faceAmt) {
		this.faceAmt = faceAmt;
	}
	public BigDecimal getComProcdAmt() {
		return comProcdAmt;
	}
	public void setComProcdAmt(BigDecimal comProcdAmt) {
		this.comProcdAmt = comProcdAmt;
	}
	public BigDecimal getMatProcdAmt() {
		return matProcdAmt;
	}
	public void setMatProcdAmt(BigDecimal matProcdAmt) {
		this.matProcdAmt = matProcdAmt;
	}
	public BigDecimal getComProcdAmtZz() {
		return comProcdAmtZz;
	}
	public void setComProcdAmtZz(BigDecimal comProcdAmtZz) {
		this.comProcdAmtZz = comProcdAmtZz;
	}
	public BigDecimal getMatProcdAmtZz() {
		return matProcdAmtZz;
	}
	public void setMatProcdAmtZz(BigDecimal matProcdAmtZz) {
		this.matProcdAmtZz = matProcdAmtZz;
	}
	public BigDecimal getComProcdAmtSub() {
		return comProcdAmtSub;
	}
	public void setComProcdAmtSub(BigDecimal comProcdAmtSub) {
		this.comProcdAmtSub = comProcdAmtSub;
	}
	public BigDecimal getMatProcdAmtSub() {
		return matProcdAmtSub;
	}
	public void setMatProcdAmtSub(BigDecimal matProcdAmtSub) {
		this.matProcdAmtSub = matProcdAmtSub;
	}
	public String getDealStatus() {
		return dealStatus;
	}
	public void setDealStatus(String dealStatus) {
		this.dealStatus = dealStatus;
	}
	public String getDealText() {
		return dealText;
	}
	public void setDealText(String dealText) {
		this.dealText = dealText;
	}
	public String getComCcysMeans() {
		return comCcysMeans;
	}
	public void setComCcysMeans(String comCcysMeans) {
		this.comCcysMeans = comCcysMeans;
	}
	public String getMatCcysMeans() {
		return matCcysMeans;
	}
	public void setMatCcysMeans(String matCcysMeans) {
		this.matCcysMeans = matCcysMeans;
	}
	public String getSettTypeB() {
		return settTypeB;
	}
	public void setSettTypeB(String settTypeB) {
		this.settTypeB = settTypeB;
	}
	public String getSettDescrB() {
		return settDescrB;
	}
	public void setSettDescrB(String settDescrB) {
		this.settDescrB = settDescrB;
	}
	public String getSettTypeE() {
		return settTypeE;
	}
	public void setSettTypeE(String settTypeE) {
		this.settTypeE = settTypeE;
	}
	public String getSettDescrE() {
		return settDescrE;
	}
	public void setSettDescrE(String settDescrE) {
		this.settDescrE = settDescrE;
	}
	public String getXmlMatChid() {
		return xmlMatChid;
	}
	public void setXmlMatChid(String xmlMatChid) {
		this.xmlMatChid = xmlMatChid;
	}
	public String getCheckSts() {
		return checkSts;
	}
	public void setCheckSts(String checkSts) {
		this.checkSts = checkSts;
	}
	public Date getCheckTime() {
		return checkTime;
	}
	public void setCheckTime(Date checkTime) {
		this.checkTime = checkTime;
	}
	public String getRequeStsn() {
		return requeStsn;
	}
	public void setRequeStsn(String requeStsn) {
		this.requeStsn = requeStsn;
	}
	public String getSendSts() {
		return sendSts;
	}
	public void setSendSts(String sendSts) {
		this.sendSts = sendSts;
	}
	public Date getSendTime() {
		return sendTime;
	}
	public void setSendTime(Date sendTime) {
		this.sendTime = sendTime;
	}
	public String getCnBondRetSts() {
		return cnBondRetSts;
	}
	public void setCnBondRetSts(String cnBondRetSts) {
		this.cnBondRetSts = cnBondRetSts;
	}
	public Date getCnBondRetTime() {
		return cnBondRetTime;
	}
	public void setCnBondRetTime(Date cnBondRetTime) {
		this.cnBondRetTime = cnBondRetTime;
	}
	public String getAcctQryAcctId() {
		return acctQryAcctId;
	}
	public void setAcctQryAcctId(String acctQryAcctId) {
		this.acctQryAcctId = acctQryAcctId;
	}
	public String getInstrId() {
		return instrId;
	}
	public void setInstrId(String instrId) {
		this.instrId = instrId;
	}
	public String getTxFlowID() {
		return txFlowID;
	}
	public void setTxFlowID(String txFlowID) {
		this.txFlowID = txFlowID;
	}
	public String getInstrSts() {
		return instrSts;
	}
	public void setInstrSts(String instrSts) {
		this.instrSts = instrSts;
	}
	
	public String getTxRsltCd() {
		return txRsltCd;
	}
	public void setTxRsltCd(String txRsltCd) {
		this.txRsltCd = txRsltCd;
	}
	public String getInsErrInf() {
		return insErrInf;
	}
	public void setInsErrInf(String insErrInf) {
		this.insErrInf = insErrInf;
	}
	public String getOrgtrCnfrmInd() {
		return orgtrCnfrmInd;
	}
	public void setOrgtrCnfrmInd(String orgtrCnfrmInd) {
		this.orgtrCnfrmInd = orgtrCnfrmInd;
	}
	public String getCtrCnfrmInd() {
		return ctrCnfrmInd;
	}
	public void setCtrCnfrmInd(String ctrCnfrmInd) {
		this.ctrCnfrmInd = ctrCnfrmInd;
	}
	public String getCtrctID() {
		return ctrctID;
	}
	public void setCtrctID(String ctrctID) {
		this.ctrctID = ctrctID;
	}
	public String getCtrctSts() {
		return ctrctSts;
	}
	public void setCtrctSts(String ctrctSts) {
		this.ctrctSts = ctrctSts;
	}
	public String getCtrctBlckSts() {
		return ctrctBlckSts;
	}
	public void setCtrctBlckSts(String ctrctBlckSts) {
		this.ctrctBlckSts = ctrctBlckSts;
	}
	public String getCtrctFaildRsn() {
		return ctrctFaildRsn;
	}
	public void setCtrctFaildRsn(String ctrctFaildRsn) {
		this.ctrctFaildRsn = ctrctFaildRsn;
	}
	public String getQueryRetTime() {
		return queryRetTime;
	}
	public void setQueryRetTime(String queryRetTime) {
		this.queryRetTime = queryRetTime;
	}
	
	
}
