package com.singlee.ifs.model;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 内部户BEAN对象
 */
public class IfsNbhBean implements Serializable {
	
	private static final long serialVersionUID = 7997034095479329314L;

	/**
	 * 币种
	 */
	private String ccy;

	/**
	 * 核心内部户
	 */
	private String ccyacct;

	/**
	 * 经办人
	 */
	private String ioper;

	/**
	 * 经办日期
	 */
	private String idate;

	/**
	 * 本币借贷标志
	 */
	private String jiedaibz_home;

	/**
	 * 本币记账金额
	 */
	private BigDecimal jizhngje_home;

	/**
	 * 本币
	 */
	private String ccy_home;

	/**
	 * 外币借贷标志
	 */
	private String jiedaibz_foreign;

	/**
	 * 外币记账金额
	 */
	private BigDecimal jizhngje_foreign;

	/**
	 * 外币
	 */
	private String ccy_foreign;

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getCcyacct() {
		return ccyacct;
	}

	public void setCcyacct(String ccyacct) {
		this.ccyacct = ccyacct;
	}

	public String getIoper() {
		return ioper;
	}

	public void setIoper(String ioper) {
		this.ioper = ioper;
	}

	public String getIdate() {
		return idate;
	}

	public void setIdate(String idate) {
		this.idate = idate;
	}

	public String getJiedaibz_home() {
		return jiedaibz_home;
	}

	public void setJiedaibz_home(String jiedaibz_home) {
		this.jiedaibz_home = jiedaibz_home;
	}

	public BigDecimal getJizhngje_home() {
		return jizhngje_home;
	}

	public void setJizhngje_home(BigDecimal jizhngje_home) {
		this.jizhngje_home = jizhngje_home;
	}

	public String getCcy_home() {
		return ccy_home;
	}

	public void setCcy_home(String ccy_home) {
		this.ccy_home = ccy_home;
	}

	public String getJiedaibz_foreign() {
		return jiedaibz_foreign;
	}

	public void setJiedaibz_foreign(String jiedaibz_foreign) {
		this.jiedaibz_foreign = jiedaibz_foreign;
	}

	public BigDecimal getJizhngje_foreign() {
		return jizhngje_foreign;
	}

	public void setJizhngje_foreign(BigDecimal jizhngje_foreign) {
		this.jizhngje_foreign = jizhngje_foreign;
	}

	public String getCcy_foreign() {
		return ccy_foreign;
	}

	public void setCcy_foreign(String ccy_foreign) {
		this.ccy_foreign = ccy_foreign;
	}
}