package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 质押式债券信息详情
 * 
 * @author zhangcm
 * 
 * @date 2018-07-18
 */
@Entity
@Table(name = "IFS_CFETSRMB_DETAIL_CR")
public class IfsCfetsrmbDetailCr implements Serializable {
 
    private String ticketId;

    private String bondCode;

    private String bondName;

    private BigDecimal totalFaceValue;

    private BigDecimal underlyingStipType;
    
    private String invtype;

    private String costb;

    private String portb;
    
    private String dealNo;
    
    private String statcode;
    
    private String errorcode;
    
    private String seq;
    
    private String prodb;
    
    private String prodtypeb;

	private String custNo;
	private String custType;
	private String weight;
	//交易金额
	private BigDecimal parValue;
	@Transient
	private String creditBamt;
	@Transient
	private String creditProdamt;
	@Transient
	private String creditSubnm;

    private static final long serialVersionUID = 1L;

    public String getTicketId() {
        return ticketId;
    }

    public void setTicketId(String ticketId) {
        this.ticketId = ticketId == null ? null : ticketId.trim();
    }

    public String getInvtype() {
		return invtype;
	}

	public void setInvtype(String invtype) {
		this.invtype = invtype;
	}

	

	public String getCostb() {
		return costb;
	}

	public void setCostb(String costb) {
		this.costb = costb;
	}

	public String getPortb() {
		return portb;
	}

	public void setPortb(String portb) {
		this.portb = portb;
	}

	public String getBondCode() {
        return bondCode;
    }

    public void setBondCode(String bondCode) {
        this.bondCode = bondCode == null ? null : bondCode.trim();
    }

    public String getBondName() {
        return bondName;
    }

    public void setBondName(String bondName) {
        this.bondName = bondName == null ? null : bondName.trim();
    }

    public BigDecimal getTotalFaceValue() {
        return totalFaceValue;
    }

    public void setTotalFaceValue(BigDecimal totalFaceValue) {
        this.totalFaceValue = totalFaceValue;
    }

    public BigDecimal getUnderlyingStipType() {
        return underlyingStipType;
    }

    public void setUnderlyingStipType(BigDecimal underlyingStipType) {
        this.underlyingStipType = underlyingStipType;
    }

	public String getDealNo() {
		return dealNo;
	}

	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}

	public String getStatcode() {
		return statcode;
	}

	public void setStatcode(String statcode) {
		this.statcode = statcode;
	}

	public String getErrorcode() {
		return errorcode;
	}

	public void setErrorcode(String errorcode) {
		this.errorcode = errorcode;
	}

	public void setSeq(String seq) {
		this.seq = seq;
	}

	public String getSeq() {
		return seq;
	}

	public void setProdb(String prodb) {
		this.prodb = prodb;
	}

	public String getProdb() {
		return prodb;
	}

	public void setProdtypeb(String prodtypeb) {
		this.prodtypeb = prodtypeb;
	}

	public String getProdtypeb() {
		return prodtypeb;
	}

	public String getCustNo() {
		return custNo;
	}

	public void setCustNo(String custNo) {
		this.custNo = custNo;
	}

	public String getCustType() {
		return custType;
	}

	public void setCustType(String custType) {
		this.custType = custType;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public String getCreditBamt() {
		return creditBamt;
	}

	public void setCreditBamt(String creditBamt) {
		this.creditBamt = creditBamt;
	}

	public String getCreditProdamt() {
		return creditProdamt;
	}

	public void setCreditProdamt(String creditProdamt) {
		this.creditProdamt = creditProdamt;
	}

	public String getCreditSubnm() {
		return creditSubnm;
	}

	public void setCreditSubnm(String creditSubnm) {
		this.creditSubnm = creditSubnm;
	}

	public BigDecimal getParValue() {
		return parValue;
	}

	public void setParValue(BigDecimal parValue) {
		this.parValue = parValue;
	}
}