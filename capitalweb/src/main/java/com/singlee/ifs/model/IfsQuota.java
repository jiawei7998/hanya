package com.singlee.ifs.model;

import javax.persistence.Transient;
import java.io.Serializable;
import java.math.BigDecimal;

public class IfsQuota extends IfsQuotaKey implements Serializable {
	private String instId;

	private String path;

	private String lev;

	private BigDecimal quota;

	@Transient
	private String userName;
	@Transient
	private String instName;

	private static final long serialVersionUID = 1L;

	public String getInstId() {
		return instId;
	}

	public void setInstId(String instId) {
		this.instId = instId == null ? null : instId.trim();
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path == null ? null : path.trim();
	}

	public String getLev() {
		return lev;
	}

	public void setLev(String lev) {
		this.lev = lev == null ? null : lev.trim();
	}

	public BigDecimal getQuota() {
		return quota;
	}

	public void setQuota(BigDecimal quota) {
		this.quota = quota;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getInstName() {
		return instName;
	}

	public void setInstName(String instName) {
		this.instName = instName;
	}

}