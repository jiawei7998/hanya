package com.singlee.ifs.model;

import java.math.BigDecimal;

/**
 * 
 * @author Administrator
 * @apiNote 余额表
 *
 */
public class IfsSecAcct {
	//对账日期
	private String postDate;
	//债券编号
	private String secID;
	//债券名称
	private String secNm;
	//托管账户
	private String secAcct;
	//币种
	private String ccy;
	//总额（元）
	private BigDecimal amt;
	//可用金额（元）
	private BigDecimal avlAmt;
	//质押面额（元）
	private BigDecimal zyAmt;
	//已发行面额（元）
	private BigDecimal issAmt;
	private BigDecimal amt1;
	private BigDecimal amt2;
	private BigDecimal amt3;
	//总行，自贸区
	private String company;
	public String getPostDate() {
		return postDate;
	}
	public void setPostDate(String postDate) {
		this.postDate = postDate;
	}
	public String getSecID() {
		return secID;
	}
	public void setSecID(String secID) {
		this.secID = secID;
	}
	public String getSecNm() {
		return secNm;
	}
	public void setSecNm(String secNm) {
		this.secNm = secNm;
	}
	public String getSecAcct() {
		return secAcct;
	}
	public void setSecAcct(String secAcct) {
		this.secAcct = secAcct;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public BigDecimal getAmt() {
		return amt;
	}
	public void setAmt(BigDecimal amt) {
		this.amt = amt;
	}
	public BigDecimal getAvlAmt() {
		return avlAmt;
	}
	public void setAvlAmt(BigDecimal avlAmt) {
		this.avlAmt = avlAmt;
	}
	public BigDecimal getZyAmt() {
		return zyAmt;
	}
	public void setZyAmt(BigDecimal zyAmt) {
		this.zyAmt = zyAmt;
	}
	public BigDecimal getIssAmt() {
		return issAmt;
	}
	public void setIssAmt(BigDecimal issAmt) {
		this.issAmt = issAmt;
	}
	public BigDecimal getAmt1() {
		return amt1;
	}
	public void setAmt1(BigDecimal amt1) {
		this.amt1 = amt1;
	}
	public BigDecimal getAmt2() {
		return amt2;
	}
	public void setAmt2(BigDecimal amt2) {
		this.amt2 = amt2;
	}
	public BigDecimal getAmt3() {
		return amt3;
	}
	public void setAmt3(BigDecimal amt3) {
		this.amt3 = amt3;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	
	
	
}
