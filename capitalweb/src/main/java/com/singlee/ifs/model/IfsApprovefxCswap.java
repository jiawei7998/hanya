package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "IFS_APPROVEFX_CSWAP")
public class IfsApprovefxCswap extends IfsApproveBaseBean  implements Serializable{
	private static final long serialVersionUID = 1L;
	private String tradingModel;
	//交易模式
	private String tradingType;
	//交易方式
	private String startDate;
	//开始日期
	private String notionalExchange;
	//本金交换
	private String stub;
	//残段
	private String unadjustmaturityDate;
	//未调整到日期
	private String spotRate;
	//即期汇率
	private String nonVanilla;
	//是否标准协议
	private String describe;
	//描述
	private String leftDirection;
	//付款方向
	private String leftNotion;
	//名义本金
	private String leftRate;
	//固定/浮动利率
	private String leftFrequency;
	//定息周期
	private String leftRule;
	//定息规则
	private String leftBasis;
	//计息方式
	private String leftPayment;
	//付息周期
	private String leftConvention;
	//起息计算方式
	private String rightDirection;
	//付款方向
	private String rightNotion;
	//名义本金
	private String rightRate;
	//固定/浮动利率
	private String rightFrequency;
	//定息周期
	private String rightRule;
	//定息规则
	private String rightBasis;
	//计息方式
	private String rightPayment;
	//付息周期
	private String rightConvention;
	//起息计算方式



	@Override
    public String getTradingModel() {
		return tradingModel;
	}

	@Override
    public void setTradingModel(String tradingModel) {
		this.tradingModel = tradingModel == null ? null : tradingModel.trim();
	}

	public String getTradingType() {
		return tradingType;
	}

	public void setTradingType(String tradingType) {
		this.tradingType = tradingType == null ? null : tradingType.trim();
	}

	@Override
    public String getStartDate() {
		return startDate;
	}

	@Override
    public void setStartDate(String startDate) {
		this.startDate = startDate == null ? null : startDate.trim();
	}

	public String getNotionalExchange() {
		return notionalExchange;
	}

	public void setNotionalExchange(String notionalExchange) {
		this.notionalExchange = notionalExchange == null ? null : notionalExchange.trim();
	}

	public String getStub() {
		return stub;
	}

	public void setStub(String stub) {
		this.stub = stub == null ? null : stub.trim();
	}

	public String getUnadjustmaturityDate() {
		return unadjustmaturityDate;
	}

	public void setUnadjustmaturityDate(String unadjustmaturityDate) {
		this.unadjustmaturityDate = unadjustmaturityDate == null ? null : unadjustmaturityDate.trim();
	}

	public String getSpotRate() {
		return spotRate;
	}

	public void setSpotRate(String spotRate) {
		this.spotRate = spotRate == null ? null : spotRate.trim();
	}

	public String getNonVanilla() {
		return nonVanilla;
	}

	public void setNonVanilla(String nonVanilla) {
		this.nonVanilla = nonVanilla == null ? null : nonVanilla.trim();
	}

	public String getDescribe() {
		return describe;
	}

	public void setDescribe(String describe) {
		this.describe = describe == null ? null : describe.trim();
	}

	public String getLeftDirection() {
		return leftDirection;
	}

	public void setLeftDirection(String leftDirection) {
		this.leftDirection = leftDirection == null ? null : leftDirection.trim();
	}

	public String getLeftNotion() {
		return leftNotion;
	}

	public void setLeftNotion(String leftNotion) {
		this.leftNotion = leftNotion == null ? null : leftNotion.trim();
	}

	public String getLeftRate() {
		return leftRate;
	}

	public void setLeftRate(String leftRate) {
		this.leftRate = leftRate == null ? null : leftRate.trim();
	}

	public String getLeftFrequency() {
		return leftFrequency;
	}

	public void setLeftFrequency(String leftFrequency) {
		this.leftFrequency = leftFrequency == null ? null : leftFrequency.trim();
	}

	public String getLeftRule() {
		return leftRule;
	}

	public void setLeftRule(String leftRule) {
		this.leftRule = leftRule == null ? null : leftRule.trim();
	}

	public String getLeftBasis() {
		return leftBasis;
	}

	public void setLeftBasis(String leftBasis) {
		this.leftBasis = leftBasis == null ? null : leftBasis.trim();
	}

	public String getLeftPayment() {
		return leftPayment;
	}

	public void setLeftPayment(String leftPayment) {
		this.leftPayment = leftPayment == null ? null : leftPayment.trim();
	}

	public String getLeftConvention() {
		return leftConvention;
	}

	public void setLeftConvention(String leftConvention) {
		this.leftConvention = leftConvention == null ? null : leftConvention.trim();
	}

	public String getRightDirection() {
		return rightDirection;
	}

	public void setRightDirection(String rightDirection) {
		this.rightDirection = rightDirection == null ? null : rightDirection.trim();
	}

	public String getRightNotion() {
		return rightNotion;
	}

	public void setRightNotion(String rightNotion) {
		this.rightNotion = rightNotion == null ? null : rightNotion.trim();
	}

	public String getRightRate() {
		return rightRate;
	}

	public void setRightRate(String rightRate) {
		this.rightRate = rightRate == null ? null : rightRate.trim();
	}

	public String getRightFrequency() {
		return rightFrequency;
	}

	public void setRightFrequency(String rightFrequency) {
		this.rightFrequency = rightFrequency == null ? null : rightFrequency.trim();
	}

	public String getRightRule() {
		return rightRule;
	}

	public void setRightRule(String rightRule) {
		this.rightRule = rightRule == null ? null : rightRule.trim();
	}

	public String getRightBasis() {
		return rightBasis;
	}

	public void setRightBasis(String rightBasis) {
		this.rightBasis = rightBasis == null ? null : rightBasis.trim();
	}

	public String getRightPayment() {
		return rightPayment;
	}

	public void setRightPayment(String rightPayment) {
		this.rightPayment = rightPayment == null ? null : rightPayment.trim();
	}

	public String getRightConvention() {
		return rightConvention;
	}

	public void setRightConvention(String rightConvention) {
		this.rightConvention = rightConvention == null ? null : rightConvention.trim();
	}
}