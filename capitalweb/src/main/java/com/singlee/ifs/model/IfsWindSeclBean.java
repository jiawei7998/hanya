package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "IFS_WIND_SECL")
public class IfsWindSeclBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private String secName;// 债券简称
	@Id
	private String secCode;// 债券代码
	private String appraisementDate;// 估值日期
	@Transient
	private String circulatePlaceCode;// 流通标志代码
	private String circulatePlace;// 流通场所
	private BigDecimal cleanPrice;// 日终估价净价(元)
	private BigDecimal dirtyPrice;// 日终估价全价(元)
	private BigDecimal marketPrice;// 市价
	private BigDecimal ecaluatePrice;// 估价净价(中债)

	public String getSecName() {
		return secName;
	}

	public void setSecName(String secName) {
		this.secName = secName;
	}

	public String getSecCode() {
		return secCode;
	}

	public void setSecCode(String secCode) {
		this.secCode = secCode;
	}

	public String getAppraisementDate() {
		return appraisementDate;
	}

	public void setAppraisementDate(String appraisementDate) {
		this.appraisementDate = appraisementDate;
	}

	public String getCirculatePlace() {
		return circulatePlace;
	}

	public void setCirculatePlace(String circulatePlace) {
		this.circulatePlace = circulatePlace;
	}

	public BigDecimal getCleanPrice() {
		return cleanPrice;
	}

	public void setCleanPrice(BigDecimal cleanPrice) {
		this.cleanPrice = cleanPrice;
	}

	public BigDecimal getDirtyPrice() {
		return dirtyPrice;
	}

	public void setDirtyPrice(BigDecimal dirtyPrice) {
		this.dirtyPrice = dirtyPrice;
	}

	public BigDecimal getMarketPrice() {
		return marketPrice;
	}

	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}

	public BigDecimal getEcaluatePrice() {
		return ecaluatePrice;
	}

	public void setEcaluatePrice(BigDecimal ecaluatePrice) {
		this.ecaluatePrice = ecaluatePrice;
	}

	public String getCirculatePlaceCode() {
		return circulatePlaceCode;
	}

	public void setCirculatePlaceCode(String circulatePlaceCode) {
		this.circulatePlaceCode = circulatePlaceCode;
	}
}
