package com.singlee.ifs.model;

public class EcifRetMsg {

	private static final long serialVersionUID = -8126004223470289965L;

	private String code;

	private String msg;

	private String clino; // 客户号

	private String CLIENT_NAME; // 中文名

	private String GLOBAL_TYPE; // 证件类型

	private String GLOBAL_ID; // 证件号码

	private String CROSS_BORD_FLAG; // 境内外标志

	private String FIN_LICENSE_CODE; // 金融许可证

	private String NTNL_INDSTRY_TP; // 国民行业类型

	private String CTZN_ECNM_DEPT_TP; // 国民经济类型

	private String FIN_ORG_TYPE; // 金融机构类型

	private String MNGE_SCOP; // 经营范围

	private String custTelNo; // 客户建档柜员号

	private String custOrgNo; // 客户建档机构号
	
	private String CLIENT_TYPE; // 客户类型

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getClino() {
		return clino;
	}

	public void setClino(String clino) {
		this.clino = clino;
	}

	public String getCLIENT_NAME() {
		return CLIENT_NAME;
	}

	public void setCLIENT_NAME(String cLIENT_NAME) {
		CLIENT_NAME = cLIENT_NAME;
	}

	public String getGLOBAL_TYPE() {
		return GLOBAL_TYPE;
	}

	public void setGLOBAL_TYPE(String gLOBAL_TYPE) {
		GLOBAL_TYPE = gLOBAL_TYPE;
	}

	public String getGLOBAL_ID() {
		return GLOBAL_ID;
	}

	public void setGLOBAL_ID(String gLOBAL_ID) {
		GLOBAL_ID = gLOBAL_ID;
	}

	public String getFIN_LICENSE_CODE() {
		return FIN_LICENSE_CODE;
	}

	public void setFIN_LICENSE_CODE(String fIN_LICENSE_CODE) {
		FIN_LICENSE_CODE = fIN_LICENSE_CODE;
	}

	public String getCROSS_BORD_FLAG() {
		return CROSS_BORD_FLAG;
	}

	public void setCROSS_BORD_FLAG(String cROSS_BORD_FLAG) {
		CROSS_BORD_FLAG = cROSS_BORD_FLAG;
	}

	public String getFIN_ORG_TYPE() {
		return FIN_ORG_TYPE;
	}

	public void setFIN_ORG_TYPE(String fIN_ORG_TYPE) {
		FIN_ORG_TYPE = fIN_ORG_TYPE;
	}

	public String getNTNL_INDSTRY_TP() {
		return NTNL_INDSTRY_TP;
	}

	public void setNTNL_INDSTRY_TP(String nTNL_INDSTRY_TP) {
		NTNL_INDSTRY_TP = nTNL_INDSTRY_TP;
	}

	public String getCTZN_ECNM_DEPT_TP() {
		return CTZN_ECNM_DEPT_TP;
	}

	public void setCTZN_ECNM_DEPT_TP(String cTZN_ECNM_DEPT_TP) {
		CTZN_ECNM_DEPT_TP = cTZN_ECNM_DEPT_TP;
	}

	public String getMNGE_SCOP() {
		return MNGE_SCOP;
	}

	public void setMNGE_SCOP(String mNGE_SCOP) {
		MNGE_SCOP = mNGE_SCOP;
	}

	public String getCustTelNo() {
		return custTelNo;
	}

	public void setCustTelNo(String custTelNo) {
		this.custTelNo = custTelNo;
	}

	public String getCustOrgNo() {
		return custOrgNo;
	}

	public void setCustOrgNo(String custOrgNo) {
		this.custOrgNo = custOrgNo;
	}

	public String getCLIENT_TYPE() {
		return CLIENT_TYPE;
	}

	public void setCLIENT_TYPE(String cLIENT_TYPE) {
		CLIENT_TYPE = cLIENT_TYPE;
	}
}