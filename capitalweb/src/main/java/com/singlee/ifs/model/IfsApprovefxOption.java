package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;


@Entity
@Table(name = "IFS_APPROVEFX_OPTION")
public class IfsApprovefxOption extends IfsApproveBaseBean  implements Serializable {
	private static final long serialVersionUID = 1L;
	private String currencyPair;
	//货币对
	private String tradingType;
	//交易类型
	private String buyNotional;
	//名义本金方向
	private String buyNotionalAmount;
	//名义本金金额
	private String tenor;
	//期限
	private String expiryDate;
	//行权日
	private String premiumType;
	//期权类型
	private String premiumRate;
	//期权费率
	private String premiumAmount;
	//期权费金额
	private String deliveryType;
	//交割方式
	private String exerciseType;
	//执行方式
	
	private String strikePrice;
	//执行价
	private String sellNotional;
	//名义本金方向
	private String sellNotionalAmount;
	//名义本金金额
	private String spotReference;
	//即期参考汇率
	private String premiumDate;
	//期权费交付日
	private String deliveryDate;
	//交割日
	private String cutOffTime;
	//行权截止时间
	private String optionStrategy;
	//期权类型
	private String strategyId;
	//期权组合号
	
	

	public String getBuyNotionalAmount() {
		return buyNotionalAmount;
	}

	public void setBuyNotionalAmount(String buyNotionalAmount) {
		this.buyNotionalAmount = buyNotionalAmount == null ? null : buyNotionalAmount.trim();;
	}

	public String getPremiumType() {
		return premiumType;
	}

	public void setPremiumType(String premiumType) {
		this.premiumType = premiumType == null ? null : premiumType.trim();;
	}

	public String getPremiumRate() {
		return premiumRate;
	}

	public void setPremiumRate(String premiumRate) {
		this.premiumRate = premiumRate == null ? null : premiumRate.trim();;
	}

	public String getSellNotionalAmount() {
		return sellNotionalAmount;
	}

	public void setSellNotionalAmount(String sellNotionalAmount) {
		this.sellNotionalAmount = sellNotionalAmount == null ? null : sellNotionalAmount.trim();;
	}

	public String getCurrencyPair() {
		return currencyPair;
	}

	public void setCurrencyPair(String currencyPair) {
		this.currencyPair = currencyPair == null ? null : currencyPair.trim();
	}

	public String getTradingType() {
		return tradingType;
	}

	public void setTradingType(String tradingType) {
		this.tradingType = tradingType == null ? null : tradingType.trim();
	}

	public String getBuyNotional() {
		return buyNotional;
	}

	public void setBuyNotional(String buyNotional) {
		this.buyNotional = buyNotional == null ? null : buyNotional.trim();
	}

	public String getTenor() {
		return tenor;
	}

	public void setTenor(String tenor) {
		this.tenor = tenor == null ? null : tenor.trim();
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate == null ? null : expiryDate.trim();
	}

	public String getPremiumAmount() {
		return premiumAmount;
	}

	public void setPremiumAmount(String premiumAmount) {
		this.premiumAmount = premiumAmount == null ? null : premiumAmount.trim();
	}

	public String getDeliveryType() {
		return deliveryType;
	}

	public void setDeliveryType(String deliveryType) {
		this.deliveryType = deliveryType == null ? null : deliveryType.trim();
	}

	public String getExerciseType() {
		return exerciseType;
	}

	public void setExerciseType(String exerciseType) {
		this.exerciseType = exerciseType == null ? null : exerciseType.trim();
	}


	public String getStrikePrice() {
		return strikePrice;
	}

	public void setStrikePrice(String strikePrice) {
		this.strikePrice = strikePrice == null ? null : strikePrice.trim();
	}

	public String getSellNotional() {
		return sellNotional;
	}

	public void setSellNotional(String sellNotional) {
		this.sellNotional = sellNotional == null ? null : sellNotional.trim();
	}

	public String getSpotReference() {
		return spotReference;
	}

	public void setSpotReference(String spotReference) {
		this.spotReference = spotReference == null ? null : spotReference.trim();
	}

	public String getPremiumDate() {
		return premiumDate;
	}

	public void setPremiumDate(String premiumDate) {
		this.premiumDate = premiumDate == null ? null : premiumDate.trim();
	}

	public String getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate == null ? null : deliveryDate.trim();
	}

	public String getCutOffTime() {
		return cutOffTime;
	}

	public void setCutOffTime(String cutOffTime) {
		this.cutOffTime = cutOffTime == null ? null : cutOffTime.trim();
	}

	public String getOptionStrategy() {
		return optionStrategy;
	}

	public void setOptionStrategy(String optionStrategy) {
		this.optionStrategy = optionStrategy == null ? null : optionStrategy.trim();
	}

	public String getStrategyId() {
		return strategyId;
	}

	public void setStrategyId(String strategyId) {
		this.strategyId = strategyId == null ? null : strategyId.trim();
	}

}