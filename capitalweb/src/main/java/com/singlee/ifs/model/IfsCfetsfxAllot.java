package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.math.BigDecimal;

@Entity
@Table(name = "IFS_CFETSFX_ALLOT")
public class IfsCfetsfxAllot extends FxCurrenyBasebean {
	private static final long serialVersionUID = 1L;

	private String	type;
	private String	cal;
	private String	payDate;
	private String	ccy;
	private BigDecimal	amount;
	private String	rule;
	private BigDecimal	discount;
	private String	payType;
	private String	relatedTrade;
	private String	relextId;
	private String	callAccount;
	private String	cpty;
	private String	broker;
	private String  dealTransType;
	//权重
	private String  weight;
	//占用授信主体
	private String custNo;
	//审批单号
	private String applyNo;
	//产品名称
	private String applyProd;
	
	//DV01Risk
	private String DV01Risk;
	
	//止损
	private String lossLimit;
	
	//久期限
	private String longLimit;
	
	//风险程度
	private String riskDegree;
	@Transient
	private String  prdNo;
	
	
	
	public String getPrdNo() {
		return prdNo;
	}
	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getCal() {
		return cal;
	}
	public void setCal(String cal) {
		this.cal = cal;
	}
	public String getPayDate() {
		return payDate;
	}
	public void setPayDate(String payDate) {
		this.payDate = payDate;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public String getRule() {
		return rule;
	}
	public void setRule(String rule) {
		this.rule = rule;
	}
	public BigDecimal getDiscount() {
		return discount;
	}
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
	public String getPayType() {
		return payType;
	}
	public void setPayType(String payType) {
		this.payType = payType;
	}
	public String getRelatedTrade() {
		return relatedTrade;
	}
	public void setRelatedTrade(String relatedTrade) {
		this.relatedTrade = relatedTrade;
	}


	public String getRelextId() {
		return relextId;
	}
	public void setRelextId(String relextId) {
		this.relextId = relextId;
	}
	public String getCallAccount() {
		return callAccount;
	}
	public void setCallAccount(String callAccount) {
		this.callAccount = callAccount;
	}
	public String getCpty() {
		return cpty;
	}
	public void setCpty(String cpty) {
		this.cpty = cpty;
	}
	public String getBroker() {
		return broker;
	}
	public void setBroker(String broker) {
		this.broker = broker;
	}
	@Override
    public String getDealTransType() {
		return dealTransType;
	}
	@Override
    public void setDealTransType(String dealTransType) {
		this.dealTransType = dealTransType;
	}
	public String getWeight() {
		return weight;
	}
	public void setWeight(String weight) {
		this.weight = weight;
	}
	@Override
    public String getCustNo() {
		return custNo;
	}
	@Override
    public void setCustNo(String custNo) {
		this.custNo = custNo;
	}
	public String getApplyNo() {
		return applyNo;
	}
	public void setApplyNo(String applyNo) {
		this.applyNo = applyNo;
	}
	public String getApplyProd() {
		return applyProd;
	}
	public void setApplyProd(String applyProd) {
		this.applyProd = applyProd;
	}
	
	public String getDV01Risk() {
		return DV01Risk;
	}

	public void setDV01Risk(String dV01Risk) {
		this.DV01Risk = dV01Risk;
	}

	public String getLossLimit() {
		return lossLimit;
	}

	public void setLossLimit(String lossLimit) {
		this.lossLimit = lossLimit;
	}

	public String getLongLimit() {
		return longLimit;
	}

	public void setLongLimit(String longLimit) {
		this.longLimit = longLimit;
	}

	public String getRiskDegree() {
		return riskDegree;
	}

	public void setRiskDegree(String riskDegree) {
		this.riskDegree = riskDegree;
	}
	
}
