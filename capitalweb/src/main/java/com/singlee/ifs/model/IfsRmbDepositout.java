package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Author zhangkai
 * @Description
 * @Date
 */

/**
 * 存放同业
 */
@Entity
@Table(name = "IFS_RMB_DEPOSITOUT")
public class IfsRmbDepositout extends FxCurrenyBasebean implements Serializable {

	private static final long serialVersionUID = -8631594753816291031L;
	/**
	 * 交易状态
	 */
	@Id
	private String dealTransType;

	/**
	 * 交易单号
	 */
	@Id
	private String ticketId;

	/**
	 * 成交单号
	 */
	private String contractId;

	/**
	 * 审批状态
	 */
	private String approveStatus;

	/**
	 * 审批发起人
	 */
	private String sponsor;

	/**
	 * 审批发起机构
	 */
	private String sponInst;

	/**
	 * 审批开始时间
	 */
	private String aDate;

	/**
	 * 客户号
	 */
	private String custId;

	private String custName;

	private String ctype;

	private String clitype;

	/**
	 * 成交日期
	 */
	private String forDate;

	/**
	 * 起息日期
	 */
	private String firstSettlementDate;

	/**
	 * 到期日期
	 */
	private String secondSettlementDate;

	/**
	 * 币种
	 */
	private String currency;

	/**
	 * 本金(元)
	 */
	private BigDecimal amt;

	/**
	 * 利率类型
	 */
	private String rateType;

	/**
	 * 基准利率代码
	 */
	private String basisRateCode;

	/**
	 * 利率点差(千分之一)
	 */
	private BigDecimal benchmarkSpread;

	/**
	 * 利率(%)
	 */
	private BigDecimal rate;

	/**
	 * 计息基础
	 */
	private String basis;

	/**
	 * 占款期限(天)
	 */
	private String tenor;

	/**
	 * 付息频率
	 */
	private String paymentFrequency;

	/**
	 * 付息频率
	 */
	private String scheduleType;


	/**
	 * 利息付款日期
	 */
	private String intpayday;

	/**
	 * 利息日期调整规则
	 */
	private String intdaterule;

	/**
	 * 客户经理
	 */
	private String trad;

	/**
	 * 核心存款账户
	 */
	private String coreAcctNo;

	/**
	 * 核心存款账户名
	 */
	private String coreAcctName;

	/**
	 * 到期利息(元)
	 */
	private BigDecimal accuredInterest;

	/**
	 * 到期金额(元)
	 */
	private BigDecimal settlementAmount;

	/**
	 * 业务归属部门
	 */
	private String bussInstId;

	/**
	 * 备注
	 */
	private String note;

	/**
	 * 投资组合
	 */
	private String port;

	/**
	 * 成本中心
	 */
	private String cost;

	/**
	 * 产品代码
	 */
	private String product;

	/**
	 * 产品类型
	 */
	private String prodType;

	/**
	 * 付款账号
	 */
	private String selfAcccode;

	/**
	 * 付款账户名称
	 */
	private String selfAccname;

	/**
	 * 付款开户行大额行号
	 */
	private String selfBankcode;

	/**
	 * 付款开户行名称
	 */
	private String selfBankname;

	/**
	 * 收款账号
	 */
	private String partyAcccode;

	/**
	 * 收款账户名称
	 */
	private String partyAccname;

	/**
	 * 收款开户行大额行号
	 */
	private String partyBankcode;

	/**
	 * 收款开户行名称
	 */
	private String partyBankname;

	/**
	 * 利率备注
	 */
	private String rateNote;

	/**
	 * 是否开立存款证实书
	 */
	private String hasVerify;


	/**
	 * 五级分类
	 */
	private String fiveLevelClass;

	/**
	 * 归属部门
	 */
	private String attributionDept;

	/**
	 * 定价基准类型: 1-Shibor,2-Libor,3-Hibor,4-Euribor,5-LPR,
	 * 6-CHN Treasury Rate,7-存款基准利率,8-贷款基准利率,9-再贷款利率,10-其他
	 */
	private String pricingStandardType;

	/**
	 * 利率浮动频率: 1-按日,2-按周,3-按月,4-按季,5-按半年，6-按年，7-其他
	 */
	private String rateFloatFrequency;

	/**
	 * 基础利率
	 */
	private BigDecimal baseRate;

	//前置产品编号
	private String fPrdCode;

	/**
	 * 账户性质（1：投融资性，2:结算性）
	 */
	private String accountNature;

	/**
	 * 账户用途
	 */
	private String accountUse;

	/**
	 * 存款账号
	 */
	private String depositAccount;

	/**
	 * 实际占款天数
	 */
	private String occupancyDays;

	/**
	 * 首次结息日
	 */
	private String firstSettlDay;

	/**
	 * 首次付息日
	 */
	private String firstPayDay;

	/**
	 * 结息日约定
	 */
	private String settlDayAgreement;

	/**
	 * 付息日约定
	 */
	private String payDayAgreement;

	/**
	 * 提前支取预期利率（%）
	 */
	private BigDecimal advRate;

	/**
	 * 提前支取约定
	 */
	private String advAgreement;

	public String getDepositAccount() {
		return depositAccount;
	}

	public void setDepositAccount(String depositAccount) {
		this.depositAccount = depositAccount;
	}

	public String getAccountNature() {
		return accountNature;
	}

	public void setAccountNature(String accountNature) {
		this.accountNature = accountNature;
	}

	public String getAccountUse() {
		return accountUse;
	}

	public void setAccountUse(String accountUse) {
		this.accountUse = accountUse;
	}

	public String getfPrdCode() {
		return fPrdCode;
	}

	public void setfPrdCode(String fPrdCode) {
		this.fPrdCode = fPrdCode;
	}

	public String getFiveLevelClass() {
		return fiveLevelClass;
	}

	public void setFiveLevelClass(String fiveLevelClass) {
		this.fiveLevelClass = fiveLevelClass;
	}

	public String getAttributionDept() {
		return attributionDept;
	}

	public void setAttributionDept(String attributionDept) {
		this.attributionDept = attributionDept;
	}

	@Override
	public String getDealTransType() {
		return dealTransType;
	}

	@Override
	public void setDealTransType(String dealTransType) {
		this.dealTransType = dealTransType;
	}

	@Override
	public String getTicketId() {
		return ticketId;
	}

	@Override
	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	@Override
	public String getContractId() {
		return contractId;
	}

	@Override
	public void setContractId(String contractId) {
		this.contractId = contractId;
	}

	@Override
	public String getApproveStatus() {
		return approveStatus;
	}

	@Override
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}

	@Override
	public String getSponsor() {
		return sponsor;
	}

	@Override
	public void setSponsor(String sponsor) {
		this.sponsor = sponsor;
	}

	@Override
	public String getSponInst() {
		return sponInst;
	}

	@Override
	public void setSponInst(String sponInst) {
		this.sponInst = sponInst;
	}

	@Override
	public String getaDate() {
		return aDate;
	}

	@Override
	public void setaDate(String aDate) {
		this.aDate = aDate;
	}

	public String getCustId() {
		return custId;
	}

	public void setCustId(String custId) {
		this.custId = custId;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getCtype() {
		return ctype;
	}

	public void setCtype(String ctype) {
		this.ctype = ctype;
	}

	public String getClitype() {
		return clitype;
	}

	public void setClitype(String clitype) {
		this.clitype = clitype;
	}

	@Override
	public String getForDate() {
		return forDate;
	}

	@Override
	public void setForDate(String forDate) {
		this.forDate = forDate;
	}

	public String getFirstSettlementDate() {
		return firstSettlementDate;
	}

	public void setFirstSettlementDate(String firstSettlementDate) {
		this.firstSettlementDate = firstSettlementDate;
	}

	public String getSecondSettlementDate() {
		return secondSettlementDate;
	}

	public void setSecondSettlementDate(String secondSettlementDate) {
		this.secondSettlementDate = secondSettlementDate;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public BigDecimal getAmt() {
		return amt;
	}

	public void setAmt(BigDecimal amt) {
		this.amt = amt;
	}

	public String getRateType() {
		return rateType;
	}

	public void setRateType(String rateType) {
		this.rateType = rateType;
	}

	public String getBasisRateCode() {
		return basisRateCode;
	}

	public void setBasisRateCode(String basisRateCode) {
		this.basisRateCode = basisRateCode;
	}

	public BigDecimal getBenchmarkSpread() {
		return benchmarkSpread;
	}

	public void setBenchmarkSpread(BigDecimal benchmarkSpread) {
		this.benchmarkSpread = benchmarkSpread;
	}

	public BigDecimal getRate() {
		return rate;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

	public String getBasis() {
		return basis;
	}

	public void setBasis(String basis) {
		this.basis = basis;
	}

	public String getTenor() {
		return tenor;
	}

	public void setTenor(String tenor) {
		this.tenor = tenor;
	}

	public String getPaymentFrequency() {
		return paymentFrequency;
	}

	public void setPaymentFrequency(String paymentFrequency) {
		this.paymentFrequency = paymentFrequency;
	}

	public String getScheduleType() {
		return scheduleType;
	}

	public void setScheduleType(String scheduleType) {
		this.scheduleType = scheduleType;
	}

	public String getIntpayday() {
		return intpayday;
	}

	public void setIntpayday(String intpayday) {
		this.intpayday = intpayday;
	}

	public String getIntdaterule() {
		return intdaterule;
	}

	public void setIntdaterule(String intdaterule) {
		this.intdaterule = intdaterule;
	}

	public String getTrad() {
		return trad;
	}

	public void setTrad(String trad) {
		this.trad = trad;
	}

	public String getCoreAcctNo() {
		return coreAcctNo;
	}

	public void setCoreAcctNo(String coreAcctNo) {
		this.coreAcctNo = coreAcctNo;
	}

	public String getCoreAcctName() {
		return coreAcctName;
	}

	public void setCoreAcctName(String coreAcctName) {
		this.coreAcctName = coreAcctName;
	}

	public BigDecimal getAccuredInterest() {
		return accuredInterest;
	}

	public void setAccuredInterest(BigDecimal accuredInterest) {
		this.accuredInterest = accuredInterest;
	}

	public BigDecimal getSettlementAmount() {
		return settlementAmount;
	}

	public void setSettlementAmount(BigDecimal settlementAmount) {
		this.settlementAmount = settlementAmount;
	}

	public String getBussInstId() {
		return bussInstId;
	}

	public void setBussInstId(String bussInstId) {
		this.bussInstId = bussInstId;
	}

	@Override
	public String getNote() {
		return note;
	}

	@Override
	public void setNote(String note) {
		this.note = note;
	}

	@Override
	public String getPort() {
		return port;
	}

	@Override
	public void setPort(String port) {
		this.port = port;
	}

	@Override
	public String getCost() {
		return cost;
	}

	@Override
	public void setCost(String cost) {
		this.cost = cost;
	}

	@Override
	public String getProduct() {
		return product;
	}

	@Override
	public void setProduct(String product) {
		this.product = product;
	}

	@Override
	public String getProdType() {
		return prodType;
	}

	@Override
	public void setProdType(String prodType) {
		this.prodType = prodType;
	}

	public String getSelfAcccode() {
		return selfAcccode;
	}

	public void setSelfAcccode(String selfAcccode) {
		this.selfAcccode = selfAcccode;
	}

	public String getSelfAccname() {
		return selfAccname;
	}

	public void setSelfAccname(String selfAccname) {
		this.selfAccname = selfAccname;
	}

	public String getSelfBankcode() {
		return selfBankcode;
	}

	public void setSelfBankcode(String selfBankcode) {
		this.selfBankcode = selfBankcode;
	}

	public String getSelfBankname() {
		return selfBankname;
	}

	public void setSelfBankname(String selfBankname) {
		this.selfBankname = selfBankname;
	}

	public String getPartyAcccode() {
		return partyAcccode;
	}

	public void setPartyAcccode(String partyAcccode) {
		this.partyAcccode = partyAcccode;
	}

	public String getPartyAccname() {
		return partyAccname;
	}

	public void setPartyAccname(String partyAccname) {
		this.partyAccname = partyAccname;
	}

	public String getPartyBankcode() {
		return partyBankcode;
	}

	public void setPartyBankcode(String partyBankcode) {
		this.partyBankcode = partyBankcode;
	}

	public String getPartyBankname() {
		return partyBankname;
	}

	public void setPartyBankname(String partyBankname) {
		this.partyBankname = partyBankname;
	}

	public String getRateNote() {
		return rateNote;
	}

	public void setRateNote(String rateNote) {
		this.rateNote = rateNote;
	}

	public String getHasVerify() {
		return hasVerify;
	}

	public void setHasVerify(String hasVerify) {
		this.hasVerify = hasVerify;
	}

	public String getPricingStandardType() {
		return pricingStandardType;
	}

	public void setPricingStandardType(String pricingStandardType) {
		this.pricingStandardType = pricingStandardType;
	}

	public String getRateFloatFrequency() {
		return rateFloatFrequency;
	}

	public void setRateFloatFrequency(String rateFloatFrequency) {
		this.rateFloatFrequency = rateFloatFrequency;
	}

	public BigDecimal getBaseRate() {
		return baseRate;
	}

	public void setBaseRate(BigDecimal baseRate) {
		this.baseRate = baseRate;
	}

	public String getOccupancyDays() {
		return occupancyDays;
	}

	public void setOccupancyDays(String occupancyDays) {
		this.occupancyDays = occupancyDays;
	}

	public String getFirstSettlDay() {
		return firstSettlDay;
	}

	public void setFirstSettlDay(String firstSettlDay) {
		this.firstSettlDay = firstSettlDay;
	}

	public String getFirstPayDay() {
		return firstPayDay;
	}

	public void setFirstPayDay(String firstPayDay) {
		this.firstPayDay = firstPayDay;
	}

	public String getSettlDayAgreement() {
		return settlDayAgreement;
	}

	public void setSettlDayAgreement(String settlDayAgreement) {
		this.settlDayAgreement = settlDayAgreement;
	}

	public String getPayDayAgreement() {
		return payDayAgreement;
	}

	public void setPayDayAgreement(String payDayAgreement) {
		this.payDayAgreement = payDayAgreement;
	}

	public BigDecimal getAdvRate() {
		return advRate;
	}

	public void setAdvRate(BigDecimal advRate) {
		this.advRate = advRate;
	}

	public String getAdvAgreement() {
		return advAgreement;
	}

	public void setAdvAgreement(String advAgreement) {
		this.advAgreement = advAgreement;
	}
}