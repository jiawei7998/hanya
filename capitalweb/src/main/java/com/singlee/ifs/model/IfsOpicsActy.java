package com.singlee.ifs.model;

import java.util.Date;

/***
 * opics 会计分类参数表
 *
 */

public class IfsOpicsActy {
	/**
	 * 会计类型
	 */
	private String acctngtype;
	/**
	 * 会计类型英文描述
	 */
	private String acctdesc;
	/**
	 * 会计类型中文描述
	 */
	private String acctdesccn;
	
	/**
	 * 最后维护时间
	 */
	private Date lstmntdate;
	/**
	 * 操作人
	 */
	private String operator;
	/**
	 * 处理状态
	 */
	private String status;
	/**
	 * 备注
	 */
	private String remark;
	
	/**
	 * 归属类型：归属客户、归属债券、归属以上两者
	 */
	private String type;
	

	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public String getAcctngtype() {
		return acctngtype;
	}


	public void setAcctngtype(String acctngtype) {
		this.acctngtype = acctngtype;
	}


	public String getAcctdesc() {
		return acctdesc;
	}


	public void setAcctdesc(String acctdesc) {
		this.acctdesc = acctdesc;
	}


	public String getAcctdesccn() {
		return acctdesccn;
	}


	public void setAcctdesccn(String acctdesccn) {
		this.acctdesccn = acctdesccn;
	}


	public Date getLstmntdate() {
		return lstmntdate;
	}


	public void setLstmntdate(Date lstmntdate) {
		this.lstmntdate = lstmntdate;
	}


	public String getOperator() {
		return operator;
	}


	public void setOperator(String operator) {
		this.operator = operator;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public String getRemark() {
		return remark;
	}


	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	

}
