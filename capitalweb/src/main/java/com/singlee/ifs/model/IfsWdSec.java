package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "IFS_WD_SEC")
public class IfsWdSec implements Serializable{
	private static final long serialVersionUID = -354930641001419747L;
	
	private String wdsecid;
	
	private String secid;
	 //债券代码
	private String realsecid;
	
	private String issuerid;
	
	private String issuer;
	 //发行人
	private String issuesize;
	 //实际发行数量(亿元)
	private String issueprice;
	 //发行价格(元)
	private String issuedate;
	 //发行日期
	private String effdate;
	 //起息日
	private String matdate;
	 //到期日
	private String interesttype;
	 //付息方式
	private String interestbasis;
	 //浮动利率基准
	private String couponprod;
	 //息票品种
	private String bondmat;
	 //债券期限
	private String rate;
	 //票面利率
	private String freq;
	 //付息周期
	private String spread;
	 //基本利差
	private String circuplace;
	 //流通场所
	private String shortname;
	 //债券简称
	private String longname;
	 //债券全称
	private String ccy;
	 //币种
	private String isin;
	 //ISIN码
	private String bondproperty;
	 //债券性质;
	private String depositary;
	 //托管人
	private String creditrating1;
	 //债券信用评级1
	private String creditratingagency1;
	 //债券信用评级机构1
	private String creditrating2;
	 //债券信用评级2
	private String creditratingagency2;
	 //债券信用评级机构2
	private String creditrating3;
	 //债券信用评级3
	private String creditratingagency3;
	 //债券信用评级机构3
	private String issuerrating1;
	 //主体信用评级1
	private String issuerratingagency1;
	 //主体信用评级机构1
	private String issuerrating2;
	 //主体信用评级2
	private String issuerratingagency2;
	 //主体信用评级机构2
	private String issuerrating3;
	 //主体信用评级3
	private String issuerratingagency3;
	 //主体信用评级机构3
	private String issuerate;
	private String   issuetaxrate;
	private String  issuerindustry;
	private String  issuerindustry2;
	private String  issuerproperty;
	private String  issuerplat;
	private String  increasebond;
	private String  orgwdsecid;
	private String  orgsecid;
	private Date  rpGenDatetime;
	private String  updateflag;
	private String  inputdate;
	private String  inputtime;
	@Transient
	private String vDate;
	@Transient
	private String cust;//opics对应交易对手
	@Transient
	private String intpaycicle;//opics对应付息周期
	
	private String listingdate;//上市日期
	@Transient
	private String bondproperties;//opics对应证券分类
	
	
	
	public String getBondproperties() {
		return bondproperties;
	}
	public void setBondproperties(String bondproperties) {
		this.bondproperties = bondproperties;
	}
	public String getSecid() {
		return secid;
	}
	public void setSecid(String secid) {
		this.secid = secid;
	}
	public String getIssuer() {
		return issuer;
	}
	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}
	public String getIssuesize() {
		return issuesize;
	}
	public void setIssuesize(String issuesize) {
		this.issuesize = issuesize;
	}
	public String getIssueprice() {
		return issueprice;
	}
	public void setIssueprice(String issueprice) {
		this.issueprice = issueprice;
	}
	public String getIssuedate() {
		return issuedate;
	}
	public void setIssuedate(String issuedate) {
		this.issuedate = issuedate;
	}
	public String getEffdate() {
		return effdate;
	}
	public void setEffdate(String effdate) {
		this.effdate = effdate;
	}
	public String getMatdate() {
		return matdate;
	}
	public void setMatdate(String matdate) {
		this.matdate = matdate;
	}
	public String getInteresttype() {
		return interesttype;
	}
	public void setInteresttype(String interesttype) {
		this.interesttype = interesttype;
	}
	public String getInterestbasis() {
		return interestbasis;
	}
	public void setInterestbasis(String interestbasis) {
		this.interestbasis = interestbasis;
	}
	public String getCouponprod() {
		return couponprod;
	}
	public void setCouponprod(String couponprod) {
		this.couponprod = couponprod;
	}
	public String getBondmat() {
		return bondmat;
	}
	public void setBondmat(String bondmat) {
		this.bondmat = bondmat;
	}
	public String getRate() {
		return rate;
	}
	public void setRate(String rate) {
		this.rate = rate;
	}
	public String getFreq() {
		return freq;
	}
	public void setFreq(String freq) {
		this.freq = freq;
	}
	public String getSpread() {
		return spread;
	}
	public void setSpread(String spread) {
		this.spread = spread;
	}
	public String getCircuplace() {
		return circuplace;
	}
	public void setCircuplace(String circuplace) {
		this.circuplace = circuplace;
	}
	public String getShortname() {
		return shortname;
	}
	public void setShortname(String shortname) {
		this.shortname = shortname;
	}
	public String getLongname() {
		return longname;
	}
	public void setLongname(String longname) {
		this.longname = longname;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public String getIsin() {
		return isin;
	}
	public void setIsin(String isin) {
		this.isin = isin;
	}
	public String getBondproperty() {
		return bondproperty;
	}
	public void setBondproperty(String bondproperty) {
		this.bondproperty = bondproperty;
	}
	public String getDepositary() {
		return depositary;
	}
	public void setDepositary(String depositary) {
		this.depositary = depositary;
	}
	public String getCreditrating1() {
		return creditrating1;
	}
	public void setCreditrating1(String creditrating1) {
		this.creditrating1 = creditrating1;
	}
	public String getCreditratingagency1() {
		return creditratingagency1;
	}
	public void setCreditratingagency1(String creditratingagency1) {
		this.creditratingagency1 = creditratingagency1;
	}
	public String getCreditrating2() {
		return creditrating2;
	}
	public void setCreditrating2(String creditrating2) {
		this.creditrating2 = creditrating2;
	}
	public String getCreditratingagency2() {
		return creditratingagency2;
	}
	public void setCreditratingagency2(String creditratingagency2) {
		this.creditratingagency2 = creditratingagency2;
	}
	public String getCreditrating3() {
		return creditrating3;
	}
	public void setCreditrating3(String creditrating3) {
		this.creditrating3 = creditrating3;
	}
	public String getCreditratingagency3() {
		return creditratingagency3;
	}
	public void setCreditratingagency3(String creditratingagency3) {
		this.creditratingagency3 = creditratingagency3;
	}
	public String getIssuerrating1() {
		return issuerrating1;
	}
	public void setIssuerrating1(String issuerrating1) {
		this.issuerrating1 = issuerrating1;
	}
	public String getIssuerratingagency1() {
		return issuerratingagency1;
	}
	public void setIssuerratingagency1(String issuerratingagency1) {
		this.issuerratingagency1 = issuerratingagency1;
	}
	public String getIssuerrating2() {
		return issuerrating2;
	}
	public void setIssuerrating2(String issuerrating2) {
		this.issuerrating2 = issuerrating2;
	}
	public String getIssuerratingagency2() {
		return issuerratingagency2;
	}
	public void setIssuerratingagency2(String issuerratingagency2) {
		this.issuerratingagency2 = issuerratingagency2;
	}
	public String getIssuerrating3() {
		return issuerrating3;
	}
	public void setIssuerrating3(String issuerrating3) {
		this.issuerrating3 = issuerrating3;
	}
	public String getIssuerratingagency3() {
		return issuerratingagency3;
	}
	public void setIssuerratingagency3(String issuerratingagency3) {
		this.issuerratingagency3 = issuerratingagency3;
	}
	public String getWdsecid() {
		return wdsecid;
	}
	public void setWdsecid(String wdsecid) {
		this.wdsecid = wdsecid;
	}
	public String getRealsecid() {
		return realsecid;
	}
	public void setRealsecid(String realsecid) {
		this.realsecid = realsecid;
	}
	public String getIssuerid() {
		return issuerid;
	}
	public void setIssuerid(String issuerid) {
		this.issuerid = issuerid;
	}
	public String getIssuerate() {
		return issuerate;
	}
	public void setIssuerate(String issuerate) {
		this.issuerate = issuerate;
	}
	public String getIssuetaxrate() {
		return issuetaxrate;
	}
	public void setIssuetaxrate(String issuetaxrate) {
		this.issuetaxrate = issuetaxrate;
	}
	public String getIssuerindustry() {
		return issuerindustry;
	}
	public void setIssuerindustry(String issuerindustry) {
		this.issuerindustry = issuerindustry;
	}
	public String getIssuerindustry2() {
		return issuerindustry2;
	}
	public void setIssuerindustry2(String issuerindustry2) {
		this.issuerindustry2 = issuerindustry2;
	}
	public String getIssuerproperty() {
		return issuerproperty;
	}
	public void setIssuerproperty(String issuerproperty) {
		this.issuerproperty = issuerproperty;
	}
	public String getIssuerplat() {
		return issuerplat;
	}
	public void setIssuerplat(String issuerplat) {
		this.issuerplat = issuerplat;
	}
	public String getIncreasebond() {
		return increasebond;
	}
	public void setIncreasebond(String increasebond) {
		this.increasebond = increasebond;
	}
	public String getOrgwdsecid() {
		return orgwdsecid;
	}
	public void setOrgwdsecid(String orgwdsecid) {
		this.orgwdsecid = orgwdsecid;
	}
	public String getOrgsecid() {
		return orgsecid;
	}
	public void setOrgsecid(String orgsecid) {
		this.orgsecid = orgsecid;
	}
	public Date getRpGenDatetime() {
		return rpGenDatetime;
	}
	public void setRpGenDatetime(Date rpGenDatetime) {
		this.rpGenDatetime = rpGenDatetime;
	}
	public String getUpdateflag() {
		return updateflag;
	}
	public void setUpdateflag(String updateflag) {
		this.updateflag = updateflag;
	}
	public String getInputdate() {
		return inputdate;
	}
	public void setInputdate(String inputdate) {
		this.inputdate = inputdate;
	}
	public String getInputtime() {
		return inputtime;
	}
	public void setInputtime(String inputtime) {
		this.inputtime = inputtime;
	}
	public void setvDate(String vDate) {
		this.vDate = vDate;
	}
	public String getvDate() {
		return vDate;
	}
	public void setCust(String cust) {
		this.cust = cust;
	}
	public String getCust() {
		return cust;
	}
	public void setIntpaycicle(String intpaycicle) {
		this.intpaycicle = intpaycicle;
	}
	public String getIntpaycicle() {
		return intpaycicle;
	}
	public void setListingdate(String listingdate) {
		this.listingdate = listingdate;
	}
	public String getListingdate() {
		return listingdate;
	}

}
