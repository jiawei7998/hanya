package com.singlee.ifs.model;

import java.io.Serializable;

/**
 * 主要用接口的公共类对象
 * 
 * 基础状态表
 * 
 * @author shenzl
 * 
 */
public class IfsBaseBean implements Serializable {

	private static final long serialVersionUID = -961013081019078772L;
	// 成交单编号
	private String ticketId;
	// 成交日期
	private String tradDate;
	// 模块标识
	private String product;
	// 模块小类
	private String type;
	// 交易状态
	private String statCode;
	// 审批状态
	private String apprState;
	// 接入时间
	private String boodate;
	// 接入时间
	private String bootime;
	// 最后维护日期
	private String lstmndate;
}
