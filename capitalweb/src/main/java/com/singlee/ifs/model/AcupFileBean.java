package com.singlee.ifs.model;

import java.io.Serializable;

/**
 * 日终送账文件明细对象
 *
 */
public class AcupFileBean implements Serializable {
	private static final long serialVersionUID = 1L;

	// 转出新帐号
	private String outNewAcount;
	// 转出旧帐号
	private String outOldAcount;
	// 转出系统类别
	private String outType;
	// 转入新帐号
	private String inNewAcount;
	// 转入旧帐号
	private String inOldAcount;
	// 转入系统类别
	private String inType;
	// 金额
	private String amt;
	// 提示码
	private String desc;
	// 转出提示码 科目
	private String outGlno;
	// 转出机构
	private String outInstit;
	// 转入提示码 科目
	private String inGlno;
	// 转入机构
	private String inInstit;
	// CGL币种
	private String ccy;
	// 机构号
	private String br;

	public String getOutNewAcount() {
		return outNewAcount;
	}

	public void setOutNewAcount(String outNewAcount) {
		this.outNewAcount = outNewAcount;
	}

	public String getOutOldAcount() {
		return outOldAcount;
	}

	public void setOutOldAcount(String outOldAcount) {
		this.outOldAcount = outOldAcount;
	}

	public String getOutType() {
		return outType;
	}

	public void setOutType(String outType) {
		this.outType = outType;
	}

	public String getInNewAcount() {
		return inNewAcount;
	}

	public void setInNewAcount(String inNewAcount) {
		this.inNewAcount = inNewAcount;
	}

	public String getInOldAcount() {
		return inOldAcount;
	}

	public void setInOldAcount(String inOldAcount) {
		this.inOldAcount = inOldAcount;
	}

	public String getInType() {
		return inType;
	}

	public void setInType(String inType) {
		this.inType = inType;
	}

	public String getAmt() {
		return amt;
	}

	public void setAmt(String amt) {
		this.amt = amt;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getOutGlno() {
		return outGlno;
	}

	public void setOutGlno(String outGlno) {
		this.outGlno = outGlno;
	}

	public String getOutInstit() {
		return outInstit;
	}

	public void setOutInstit(String outInstit) {
		this.outInstit = outInstit;
	}

	public String getInGlno() {
		return inGlno;
	}

	public void setInGlno(String inGlno) {
		this.inGlno = inGlno;
	}

	public String getInInstit() {
		return inInstit;
	}

	public void setInInstit(String inInstit) {
		this.inInstit = inInstit;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

}
