package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "IFS_APPROVEFX_ONSPOT")
public class IfsApprovefxOnspot extends IfsApproveBaseBean  implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String forDate;
	//日期
	private String tradingModel;
	//交易模式
	private String tradingType;
	//交易方式
	private String currencyPair;
	//货币对
	private String price;
	//价格
	private String capital;
	//本金
	private String settlementAmount;
	//结算金额
	private String valueDate;
	//起息日
	
	

	public String getCapital() {
		return capital;
	}

	public void setCapital(String capital) {
		this.capital = capital;
	}

	public String getSettlementAmount() {
		return settlementAmount;
	}

	public void setSettlementAmount(String settlementAmount) {
		this.settlementAmount = settlementAmount;
	}


	public String getForDate() {
		return forDate;
	}

	public void setForDate(String forDate) {
		this.forDate = forDate == null ? null : forDate.trim();
	}


	@Override
    public String getTradingModel() {
		return tradingModel;
	}

	@Override
    public void setTradingModel(String tradingModel) {
		this.tradingModel = tradingModel == null ? null : tradingModel.trim();
	}

	public String getTradingType() {
		return tradingType;
	}

	public void setTradingType(String tradingType) {
		this.tradingType = tradingType == null ? null : tradingType.trim();
	}

	public String getCurrencyPair() {
		return currencyPair;
	}

	public void setCurrencyPair(String currencyPair) {
		this.currencyPair = currencyPair == null ? null : currencyPair.trim();
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price == null ? null : price.trim();
	}

	public String getValueDate() {
		return valueDate;
	}

	public void setValueDate(String valueDate) {
		this.valueDate = valueDate == null ? null : valueDate.trim();
	}

}