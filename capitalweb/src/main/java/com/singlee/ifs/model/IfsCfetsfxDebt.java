package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.math.BigDecimal;

@Entity
@Table(name = "IFS_CFETSFX_DEBT")
public class IfsCfetsfxDebt  extends FxCurrenyBasebean{
	private static final long serialVersionUID = 1L;
	
	private String  businessDirection;
	//買賣方向
	private String  isin;
	//ISIN
	private String  state;
	//狀態
	private String  issue;
	//發行
	private String  brokers;
	//經紀商
	private String  brokersName;
	//經紀商名稱
	private String  benchmark;
	//基準
	private String 	checkPath;
	//複核軌跡
	private BigDecimal  jnumber;
	//數量
	private String  discountRate;
	//貼現率
	private BigDecimal  capital;
	//本金
	private BigDecimal  price;
	//價格
	private BigDecimal 	profit;
	//殖利
	private BigDecimal  interest;
	//應計息
	private String  countDate;
	//計算日期
	private String  overweight;
	//加碼
	private BigDecimal  net;
	//凈
	private String 	benchmarkPrice;
	//基準/價格
	private String  benchmarkProfit;
	//基準/殖利
	private String  benchmarkDiscount;
	//基準/貼現
	private String  executionTime;
	//輸入/執行時間
//	private String  forDate;
//	//交易日期
	private String 	serialNumber;
	//序號
	private String 	ts;
	//TS交易碼
	private String 	userName;
	//用戶名稱
	private String 	customer;
	//客戶
	private String 	submissionCompany;
	//遞交者公司
	private String 	loginer;
	//登陸
	private String 	jaccount;
	//賬戶
	private String 	submission;
	//	遞交者
	@Transient
	private String  prdNo;
	

	public String getPrdNo() {
		return prdNo;
	}
	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}
	public String getBusinessDirection() {
		return businessDirection;
	}
	public void setBusinessDirection(String businessDirection) {
		this.businessDirection = businessDirection;
	}
	public String getIsin() {
		return isin;
	}
	public void setIsin(String isin) {
		this.isin = isin;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getIssue() {
		return issue;
	}
	public void setIssue(String issue) {
		this.issue = issue;
	}
	public String getBrokers() {
		return brokers;
	}
	public void setBrokers(String brokers) {
		this.brokers = brokers;
	}
	public String getBrokersName() {
		return brokersName;
	}
	public void setBrokersName(String brokersName) {
		this.brokersName = brokersName;
	}
	public String getBenchmark() {
		return benchmark;
	}
	public void setBenchmark(String benchmark) {
		this.benchmark = benchmark;
	}
	public String getCheckPath() {
		return checkPath;
	}
	public void setCheckPath(String checkPath) {
		this.checkPath = checkPath;
	}
	public BigDecimal getJnumber() {
		return jnumber;
	}
	public void setJnumber(BigDecimal jnumber) {
		this.jnumber = jnumber;
	}
	public String getDiscountRate() {
		return discountRate;
	}
	public void setDiscountRate(String discountRate) {
		this.discountRate = discountRate;
	}
	public BigDecimal getCapital() {
		return capital;
	}
	public void setCapital(BigDecimal capital) {
		this.capital = capital;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public BigDecimal getProfit() {
		return profit;
	}
	public void setProfit(BigDecimal profit) {
		this.profit = profit;
	}
	public BigDecimal getInterest() {
		return interest;
	}
	public void setInterest(BigDecimal interest) {
		this.interest = interest;
	}
	public String getCountDate() {
		return countDate;
	}
	public void setCountDate(String countDate) {
		this.countDate = countDate;
	}
	public String getOverweight() {
		return overweight;
	}
	public void setOverweight(String overweight) {
		this.overweight = overweight;
	}
	public BigDecimal getNet() {
		return net;
	}
	public void setNet(BigDecimal net) {
		this.net = net;
	}
	public String getBenchmarkPrice() {
		return benchmarkPrice;
	}
	public void setBenchmarkPrice(String benchmarkPrice) {
		this.benchmarkPrice = benchmarkPrice;
	}
	public String getBenchmarkProfit() {
		return benchmarkProfit;
	}
	public void setBenchmarkProfit(String benchmarkProfit) {
		this.benchmarkProfit = benchmarkProfit;
	}
	public String getBenchmarkDiscount() {
		return benchmarkDiscount;
	}
	public void setBenchmarkDiscount(String benchmarkDiscount) {
		this.benchmarkDiscount = benchmarkDiscount;
	}
	public String getExecutionTime() {
		return executionTime;
	}
	public void setExecutionTime(String executionTime) {
		this.executionTime = executionTime;
	}
	
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	public String getTs() {
		return ts;
	}
	public void setTs(String ts) {
		this.ts = ts;
	}
	@Override
    public String getUserName() {
		return userName;
	}
	@Override
    public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getCustomer() {
		return customer;
	}
	public void setCustomer(String customer) {
		this.customer = customer;
	}
	public String getSubmissionCompany() {
		return submissionCompany;
	}
	public void setSubmissionCompany(String submissionCompany) {
		this.submissionCompany = submissionCompany;
	}
	public String getLoginer() {
		return loginer;
	}
	public void setLoginer(String loginer) {
		this.loginer = loginer;
	}
	public String getJaccount() {
		return jaccount;
	}
	public void setJaccount(String jaccount) {
		this.jaccount = jaccount;
	}
	public String getSubmission() {
		return submission;
	}
	public void setSubmission(String submission) {
		this.submission = submission;
	}
	
	
}
