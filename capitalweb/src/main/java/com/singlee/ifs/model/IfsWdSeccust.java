package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "IFS_WD_SECCUST")
public class IfsWdSeccust implements Serializable{
	private static final long serialVersionUID = -5552204956986113465L;
	private String custid;
	private String  custnm;
	private String  pubdate;
	private String  rating;
	private String  ratingagencynm;
	private String  smtratingagency;
	private Integer  sourcetype;
	private Date  rpGenDatetime;
	@Transient
	private String vDate;
	
	public String getCustid() {
		return custid;
	}
	public void setCustid(String custid) {
		this.custid = custid;
	}
	public String getCustnm() {
		return custnm;
	}
	public void setCustnm(String custnm) {
		this.custnm = custnm;
	}
	public String getPubdate() {
		return pubdate;
	}
	public void setPubdate(String pubdate) {
		this.pubdate = pubdate;
	}
	public String getRating() {
		return rating;
	}
	public void setRating(String rating) {
		this.rating = rating;
	}
	public String getRatingagencynm() {
		return ratingagencynm;
	}
	public void setRatingagencynm(String ratingagencynm) {
		this.ratingagencynm = ratingagencynm;
	}
	public String getSmtratingagency() {
		return smtratingagency;
	}
	public void setSmtratingagency(String smtratingagency) {
		this.smtratingagency = smtratingagency;
	}
	public Integer getSourcetype() {
		return sourcetype;
	}
	public void setSourcetype(Integer sourcetype) {
		this.sourcetype = sourcetype;
	}
	public Date getRpGenDatetime() {
		return rpGenDatetime;
	}
	public void setRpGenDatetime(Date rpGenDatetime) {
		this.rpGenDatetime = rpGenDatetime;
	}
	public void setvDate(String vDate) {
		this.vDate = vDate;
	}
	public String getvDate() {
		return vDate;
	}

}
