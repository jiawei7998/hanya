package com.singlee.ifs.model;

import javax.persistence.Transient;
import java.io.Serializable;
import java.math.BigDecimal;

public class IfsCfetsfxSpt extends FxCurrenyBasebean implements Serializable {

	private static final long serialVersionUID = 1L;

    //交易单号
    private String ticketId;
      // 交易状态
    private String dealTransType;
   // 本方机构
    private String instId;
    //对方机构
    private String counterpartyInstId;
   //本方交易员
    private String dealer;
    //对方交易员
    private String counterpartyDealer;
    //日期
    private String forDate;
   // 时间
    private String forTime;
   // 交易模式
    private String tradingModel;
   // 交易方式
    private String tradingType;
   // 货币对
    private String currencyPair;

	private String ccy;

	// 交易币种
	private BigDecimal price;

	// 价格
	private String sellDirection;

	// 卖出方交易方向
	private String buyDirection;

	// 买入方交易方向
	private BigDecimal buyAmount;

	// 买入方向金额
	private BigDecimal sellAmount;

	// 卖出方向金额
	private String tenorContract;

	// 期限/合约名称
	private String valueDate;

	// 起息日
	// private String sellCurreny;
	// 清算货币
	private String bankName;

	// 开户行名称
	private String bankBic;

	// 开户行BIC CODE
	private String dueBank;

	// 收款行名称
	private String dueBankName;

	// 收款行名称BIC CODE
	private String dueBankAccount;

	// 收款行账号
	// private String buyCurreny;
	// 清算货币
	private String capitalBank;

	// 资金开户行
	private String capitalBankName;

	// 资金账户户名
	private String cnapsCode;

	// 支付系统行号
	private String capitalAccount;

	// 资金账号
	// 币种1(放重要币种)
	private String opicsccy;

	// 币种2(放次要币种)
	private String opicsctrccy;

	// 权重
	private String weight;

	// 占用授信主体
	private String custNo;

	// 审批单号
	private String applyNo;

	// 产品名称
	private String applyProd;

	// DV01Risk
	private String DV01Risk;

	// 止损
	private String lossLimit;

	// 久期限
	private String longLimit;

	// 风险程度
	private String riskDegree;

	// 升贴水点差
	private String spread;

	// 中间行名称1
	private String intermediaryBankName1;

	// 中间行BIC CODE1
	private String intermediaryBankBicCode1;

	// 中间行名称2
	private String intermediaryBankName2;

	// 中间行BIC CODE2
	private String intermediaryBankBicCode2;
	
	//价格偏离度
	private String priceDeviation;
	//净额清算路径
	private String nettingAddr;
	
	//成交汇率
	private String exchangeRate;
	
	@Transient
	private BigDecimal ccycheck;
	
	@Transient
	private BigDecimal ctrcheck;
	
	@Transient
	private BigDecimal ccyamount;
	
	@Transient
	private BigDecimal ctramount;
	
	@Transient
	private String  prdNo;

	private String  cfetscn;
	
	//购售业务种类
	private String  transKind;
	//购售用途
	private String  purposeCode;

	/**
	 * 五分类（0：正常类,1：关注类,2:次级类,3:可疑类,4:损失类）
	 */
	private String fiveLevelClass;

	/**
	 * 归属部门
	 */
	private String attributionDept;

	//结算方式
	private String deliveryType;


	//交易后确认标识
	private String cfetsCnfmIndicator;

	//前置产品编号
	private String fPrdCode;

	public String getfPrdCode() {
		return fPrdCode;
	}

	public void setfPrdCode(String fPrdCode) {
		this.fPrdCode = fPrdCode;
	}

	public String getCfetsCnfmIndicator() {
		return cfetsCnfmIndicator;
	}

	public void setCfetsCnfmIndicator(String cfetsCnfmIndicator) {
		this.cfetsCnfmIndicator = cfetsCnfmIndicator;
	}

	@Override
	public String getDeliveryType() {
		return deliveryType;
	}

	@Override
	public void setDeliveryType(String deliveryType) {
		this.deliveryType = deliveryType;
	}

	public String getFiveLevelClass() {
		return fiveLevelClass;
	}

	public void setFiveLevelClass(String fiveLevelClass) {
		this.fiveLevelClass = fiveLevelClass;
	}

	public String getAttributionDept() {
		return attributionDept;
	}

	public void setAttributionDept(String attributionDept) {
		this.attributionDept = attributionDept;
	}

	public String getCfetscn() {
		return cfetscn;
	}

	public void setCfetscn(String cfetscn) {
		this.cfetscn = cfetscn;
	}

	public String getPrdNo() {
		return prdNo;
	}

	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}

	public String getOpicsccy() {
		return opicsccy;
	}

	public void setOpicsccy(String opicsccy) {
		this.opicsccy = opicsccy;
	}

	public String getOpicsctrccy() {
		return opicsctrccy;
	}

	public void setOpicsctrccy(String opicsctrccy) {
		this.opicsctrccy = opicsctrccy;
	}

	public String getSellDirection() {
		return sellDirection;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public void setSellDirection(String sellDirection) {
		this.sellDirection = sellDirection;
	}

	public String getBuyDirection() {
		return buyDirection;
	}

	public void setBuyDirection(String buyDirection) {
		this.buyDirection = buyDirection;
	}
	
	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getBuyAmount() {
		return buyAmount;
	}

	public void setBuyAmount(BigDecimal buyAmount) {
		this.buyAmount = buyAmount;
	}

	public BigDecimal getSellAmount() {
		return sellAmount;
	}

	public void setSellAmount(BigDecimal sellAmount) {
		this.sellAmount = sellAmount;
	}

	public String getTenorContract() {
		return tenorContract;
	}

	public void setTenorContract(String tenorContract) {
		this.tenorContract = tenorContract == null ? null : tenorContract.trim();
	}

	public String getValueDate() {
		return valueDate;
	}

	public void setValueDate(String valueDate) {
		this.valueDate = valueDate == null ? null : valueDate.trim();
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName == null ? null : bankName.trim();
	}

	public String getBankBic() {
		return bankBic;
	}

	public void setBankBic(String bankBic) {
		this.bankBic = bankBic == null ? null : bankBic.trim();
	}

	public String getDueBank() {
		return dueBank;
	}

	public void setDueBank(String dueBank) {
		this.dueBank = dueBank == null ? null : dueBank.trim();
	}

	public String getDueBankName() {
		return dueBankName;
	}

	public void setDueBankName(String dueBankName) {
		this.dueBankName = dueBankName == null ? null : dueBankName.trim();
	}

	public String getDueBankAccount() {
		return dueBankAccount;
	}

	public void setDueBankAccount(String dueBankAccount) {
		this.dueBankAccount = dueBankAccount == null ? null : dueBankAccount.trim();
	}

	public String getCapitalBank() {
		return capitalBank;
	}

	public void setCapitalBank(String capitalBank) {
		this.capitalBank = capitalBank == null ? null : capitalBank.trim();
	}

	public String getCapitalBankName() {
		return capitalBankName;
	}

	public void setCapitalBankName(String capitalBankName) {
		this.capitalBankName = capitalBankName == null ? null : capitalBankName.trim();
	}

	public String getCnapsCode() {
		return cnapsCode;
	}

	public void setCnapsCode(String cnapsCode) {
		this.cnapsCode = cnapsCode == null ? null : cnapsCode.trim();
	}

	public String getCapitalAccount() {
		return capitalAccount;
	}

	public void setCapitalAccount(String capitalAccount) {
		this.capitalAccount = capitalAccount == null ? null : capitalAccount.trim();
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	@Override
	public String getCustNo() {
		return custNo;
	}

	@Override
	public void setCustNo(String custNo) {
		this.custNo = custNo;
	}

	public String getApplyNo() {
		return applyNo;
	}

	public void setApplyNo(String applyNo) {
		this.applyNo = applyNo;
	}

	public String getApplyProd() {
		return applyProd;
	}

	public void setApplyProd(String applyProd) {
		this.applyProd = applyProd;
	}

	public String getDV01Risk() {
		return DV01Risk;
	}

	public void setDV01Risk(String dV01Risk) {
		this.DV01Risk = dV01Risk;
	}

	public String getLossLimit() {
		return lossLimit;
	}

	public void setLossLimit(String lossLimit) {
		this.lossLimit = lossLimit;
	}

	public String getLongLimit() {
		return longLimit;
	}

	public void setLongLimit(String longLimit) {
		this.longLimit = longLimit;
	}

	public String getRiskDegree() {
		return riskDegree;
	}

	public void setRiskDegree(String riskDegree) {
		this.riskDegree = riskDegree;
	}

	public void setSpread(String spread) {
		this.spread = spread;
	}

	public String getSpread() {
		return spread;
	}

	@Override
	public String getIntermediaryBankName1() {
		return intermediaryBankName1;
	}

	@Override
	public void setIntermediaryBankName1(String intermediaryBankName1) {
		this.intermediaryBankName1 = intermediaryBankName1;
	}

	@Override
	public String getIntermediaryBankBicCode1() {
		return intermediaryBankBicCode1;
	}

	@Override
	public void setIntermediaryBankBicCode1(String intermediaryBankBicCode1) {
		this.intermediaryBankBicCode1 = intermediaryBankBicCode1;
	}

	@Override
	public String getIntermediaryBankName2() {
		return intermediaryBankName2;
	}

	@Override
	public void setIntermediaryBankName2(String intermediaryBankName2) {
		this.intermediaryBankName2 = intermediaryBankName2;
	}

	@Override
	public String getIntermediaryBankBicCode2() {
		return intermediaryBankBicCode2;
	}

	@Override
	public void setIntermediaryBankBicCode2(String intermediaryBankBicCode2) {
		this.intermediaryBankBicCode2 = intermediaryBankBicCode2;
	}
	public String getPriceDeviation() {
		return priceDeviation==null?"":priceDeviation;
	}

	public void setPriceDeviation(String priceDeviation) {
		this.priceDeviation = priceDeviation==null?"":priceDeviation;
	}

	public String getNettingAddr() {
		return nettingAddr;
	}

	public void setNettingAddr(String nettingAddr) {
		this.nettingAddr = nettingAddr;
	}

	public void setExchangeRate(String exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	public String getExchangeRate() {
		return exchangeRate;
	}

	public BigDecimal getCcycheck() {
		return ccycheck;
	}

	public void setCcycheck(BigDecimal ccycheck) {
		this.ccycheck = ccycheck;
	}

	public BigDecimal getCtrcheck() {
		return ctrcheck;
	}

	public void setCtrcheck(BigDecimal ctrcheck) {
		this.ctrcheck = ctrcheck;
	}

	public BigDecimal getCcyamount() {
		return ccyamount;
	}

	public void setCcyamount(BigDecimal ccyamount) {
		this.ccyamount = ccyamount;
	}

	public BigDecimal getCtramount() {
		return ctramount;
	}

	public void setCtramount(BigDecimal ctramount) {
		this.ctramount = ctramount;
	}

	public String getTransKind() {
		return transKind;
	}

	public void setTransKind(String transKind) {
		this.transKind = transKind;
	}

	public String getPurposeCode() {
		return purposeCode;
	}

	public void setPurposeCode(String purposeCode) {
		this.purposeCode = purposeCode;
	}

    @Override
	public String getTicketId() {
        return ticketId;
    }

    @Override
	public void setTicketId(String ticketId) {
        this.ticketId = ticketId;
    }

    @Override
	public String getDealTransType() {
        return dealTransType;
    }

    @Override
	public void setDealTransType(String dealTransType) {
        this.dealTransType = dealTransType;
    }

    @Override
	public String getInstId() {
        return instId;
    }

    @Override
	public void setInstId(String instId) {
        this.instId = instId;
    }

    @Override
	public String getCounterpartyInstId() {
        return counterpartyInstId;
    }

    @Override
	public void setCounterpartyInstId(String counterpartyInstId) {
        this.counterpartyInstId = counterpartyInstId;
    }

    @Override
	public String getDealer() {
        return dealer;
    }

    @Override
	public void setDealer(String dealer) {
        this.dealer = dealer;
    }

    @Override
	public String getCounterpartyDealer() {
        return counterpartyDealer;
    }

    @Override
	public void setCounterpartyDealer(String counterpartyDealer) {
        this.counterpartyDealer = counterpartyDealer;
    }

    @Override
	public String getForDate() {
        return forDate;
    }

    @Override
	public void setForDate(String forDate) {
        this.forDate = forDate;
    }

    @Override
	public String getForTime() {
        return forTime;
    }

    @Override
	public void setForTime(String forTime) {
        this.forTime = forTime;
    }

    @Override
	public String getTradingModel() {
        return tradingModel;
    }

    @Override
	public void setTradingModel(String tradingModel) {
        this.tradingModel = tradingModel;
    }

    @Override
	public String getTradingType() {
        return tradingType;
    }

    @Override
	public void setTradingType(String tradingType) {
        this.tradingType = tradingType;
    }

    @Override
	public String getCurrencyPair() {
        return currencyPair;
    }

    @Override
	public void setCurrencyPair(String currencyPair) {
        this.currencyPair = currencyPair;
    }
	
}