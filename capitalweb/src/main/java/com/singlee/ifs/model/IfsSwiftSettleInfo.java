package com.singlee.ifs.model;

import java.io.Serializable;

public class IfsSwiftSettleInfo implements Serializable{

	/**
	 * 报文清算信息
	 */
	private static final long serialVersionUID = 8200371474558702604L;

	private String msgtype;
	private String msg;
	private String sendDate;
	private String sendBank;
	private String reciverBank;
	private String amount;
	private String ccy;
	private String swiftflag;
	private String swiftStatus;
	private String tag20;
	private String tag21;
	private String swiftInst;
	public String getMsgtype() {
		return msgtype;
	}
	public void setMsgtype(String msgtype) {
		this.msgtype = msgtype;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getSendBank() {
		return sendBank;
	}
	public void setSendBank(String sendBank) {
		this.sendBank = sendBank;
	}
	public String getReciverBank() {
		return reciverBank;
	}
	public void setReciverBank(String reciverBank) {
		this.reciverBank = reciverBank;
	}
	public String getSendDate() {
		return sendDate;
	}
	public void setSendDate(String sendDate) {
		this.sendDate = sendDate;
	}
	
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public String getSwiftflag() {
		return swiftflag;
	}
	public void setSwiftflag(String swiftflag) {
		this.swiftflag = swiftflag;
	}
	public String getSwiftStatus() {
		return swiftStatus;
	}
	public void setSwiftStatus(String swiftStatus) {
		this.swiftStatus = swiftStatus;
	}
	public String getTag20() {
		return tag20;
	}
	public void setTag20(String tag20) {
		this.tag20 = tag20;
	}
	public String getTag21() {
		return tag21;
	}
	public void setTag21(String tag21) {
		this.tag21 = tag21;
	}
	public String getSwiftInst() {
		return swiftInst;
	}
	public void setSwiftInst(String swiftInst) {
		this.swiftInst = swiftInst;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	
}
