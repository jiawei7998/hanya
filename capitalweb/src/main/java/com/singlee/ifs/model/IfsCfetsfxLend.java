package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "IFS_CFETSFX_LEND")
public class IfsCfetsfxLend extends FxCurrenyBasebean implements Serializable {

	private static final long serialVersionUID = 1L;
    //交易单号
    private String ticketId;
      // 交易状态
    private String dealTransType;
   // 本方机构
    private String instId;
    //对方机构
    private String counterpartyInstId;
   //本方交易员
    private String dealer;
    //对方交易员
    private String counterpartyDealer;
 // 日期
	private String forDate;
 // 时间
	 private String forTime;
	
	private String currency;
	// 货币
	private BigDecimal rate;
	// 拆借利率
	private String direction;
	// 交易方向
	private String valueDate;
	// 起息日
	private BigDecimal occupancyDays;// factDays
	// 实际占款天数
	private BigDecimal amount;
	// 拆借金额
	private String basis;
	// 计息基准
	private String tenor;
	// 期限
	private String maturityDate;
	// 到期日
	private BigDecimal interest;
	// 应计利息
	private BigDecimal repaymentAmount;
	// 到期还款金额
	private String myAccount;
	// 本方清算账户
	private String otherAccount;
	// 对手方清算账户

	private String rateCode;
	// 权重
	private String weight;
	// 占用授信主体
	private String custNo;
	// 占用授信主体类型
	private String custType;
	// 审批单号
	private String applyNo;
	// 产品名称
	private String applyProd;

	// DV01Risk
	private String DV01Risk;

	// 止损
	private String lossLimit;

	// 久期限
	private String longLimit;

	// 风险程度
	private String riskDegree;

	private String borrowTel;

	private String borrowFax;

	private String borrowCorp;

	private String borrowAddr;

	private String removeTel;

	private String removeFax;

	private String removeCorp;

	private String removeAddr;

	private String oppoDir;

	private String borrowAccname;

	private String borrowOpenBank;

	private String borrowAccnum;

	private String borrowPsnum;

	private String removeAccname;

	private String removeOpenBank;

	private String removeAccnum;

	private String removePsnum;

	private String myUserName;

	/**
	 * 本方清算货币
	 */
	private String buyCurreny;

	/**
	 * 本方开户行名称
	 */
	private String bankName1;

	/**
	 * 本方开户行BIC CODE
	 */
	private String bankBic1;

	/**
	 * 本方收款行名称
	 */
	private String dueBank1;

	/**
	 * 本方收款行BIC CODE
	 */
	private String dueBankName1;

	/**
	 * 本方收款行账号
	 */
	private String dueBankAccount1;

	/**
	 * 本方中间行名称
	 */
	private String intermediaryBank1;

	/**
	 * 本方中间行BIC CODE
	 */
	private String intermediaryBicCode1;

	/**
	 * 对手方清算货币
	 */
	private String sellCurreny;

	/**
	 * 对手方开户行名称
	 */
	private String bankName2;

	/**
	 * 对手方开户行BIC CODE
	 */
	private String bankBic2;

	/**
	 * 对手方收款行名称
	 */
	private String dueBank2;

	/**
	 * 对手方收款行BIC CODE
	 */
	private String dueBankName2;

	/**
	 * 对手方收款行账号
	 */
	private String dueBankAccount2;

	/**
	 * 对手方中间行名称
	 */
	private String intermediaryBank2;

	/**
	 * 对手方中间行BIC CODE
	 */
	private String intermediaryBicCode2;

	// 价格偏离度
	private String priceDeviation;
	
	//参考编号    拆借生成报文
	private String refId;

	@Transient
	private String  prdNo;

	private String  cfetscn;

	/**
	 * 五级分类
	 */
	private String fiveLevelClass;

	/**
	 * 归属部门
	 */
	private String attributionDept;

	//结算方式
	private String deliveryType;

	//交易后确认标识
	private String cfetsCnfmIndicator;

	//前置产品编号
	private String fPrdCode;

	/**
	 * 交易方式
	 */
	private String  tradingType;

	/**
	 * CFETS下行期限代码
	 */
	private String  tenorCode;

	@Override
	public String getTradingType() {
		return tradingType;
	}

	@Override
	public void setTradingType(String tradingType) {
		this.tradingType = tradingType;
	}

	public String getTenorCode() {
		return tenorCode;
	}

	public void setTenorCode(String tenorCode) {
		this.tenorCode = tenorCode;
	}

	public String getfPrdCode() {
		return fPrdCode;
	}

	public void setfPrdCode(String fPrdCode) {
		this.fPrdCode = fPrdCode;
	}

	public String getCfetsCnfmIndicator() {
		return cfetsCnfmIndicator;
	}

	public void setCfetsCnfmIndicator(String cfetsCnfmIndicator) {
		this.cfetsCnfmIndicator = cfetsCnfmIndicator;
	}

	@Override
	public String getDeliveryType() {
		return deliveryType;
	}

	@Override
	public void setDeliveryType(String deliveryType) {
		this.deliveryType = deliveryType;
	}

	public String getFiveLevelClass() {
		return fiveLevelClass;
	}

	public void setFiveLevelClass(String fiveLevelClass) {
		this.fiveLevelClass = fiveLevelClass;
	}

	public String getAttributionDept() {
		return attributionDept;
	}

	public void setAttributionDept(String attributionDept) {
		this.attributionDept = attributionDept;
	}
	
	public String getCfetscn() {
		return cfetscn;
	}

	public void setCfetscn(String cfetscn) {
		this.cfetscn = cfetscn;
	}

	public String getPrdNo() {
		return prdNo;
	}

	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency == null ? null : currency.trim();
	}

	public BigDecimal getRate() {
		return rate;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction == null ? null : direction.trim();
	}

	public String getValueDate() {
		return valueDate;
	}

	public void setValueDate(String valueDate) {
		this.valueDate = valueDate == null ? null : valueDate.trim();
	}

	public String getBasis() {
		return basis;
	}

	public void setBasis(String basis) {
		this.basis = basis == null ? null : basis.trim();
	}

	public String getMaturityDate() {
		return maturityDate;
	}

	public void setMaturityDate(String maturityDate) {
		this.maturityDate = maturityDate == null ? null : maturityDate.trim();
	}

	public String getOtherAccount() {
		return otherAccount;
	}

	public void setOtherAccount(String otherAccount) {
		this.otherAccount = otherAccount == null ? null : otherAccount.trim();
	}

	public void setRateCode(String rateCode) {
		this.rateCode = rateCode;
	}

	public String getRateCode() {
		return rateCode;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	@Override
    public String getCustNo() {
		return custNo;
	}

	@Override
    public void setCustNo(String custNo) {
		this.custNo = custNo;
	}

	public String getApplyNo() {
		return applyNo;
	}

	public void setApplyNo(String applyNo) {
		this.applyNo = applyNo;
	}

	public String getApplyProd() {
		return applyProd;
	}

	public void setApplyProd(String applyProd) {
		this.applyProd = applyProd;
	}

	public String getDV01Risk() {
		return DV01Risk;
	}

	public void setDV01Risk(String dV01Risk) {
		this.DV01Risk = dV01Risk;
	}

	public String getLossLimit() {
		return lossLimit;
	}

	public void setLossLimit(String lossLimit) {
		this.lossLimit = lossLimit;
	}

	public String getLongLimit() {
		return longLimit;
	}

	public void setLongLimit(String longLimit) {
		this.longLimit = longLimit;
	}

	public String getRiskDegree() {
		return riskDegree;
	}

	public void setRiskDegree(String riskDegree) {
		this.riskDegree = riskDegree;
	}

	public String getBorrowTel() {
		return borrowTel;
	}

	public void setBorrowTel(String borrowTel) {
		this.borrowTel = borrowTel;
	}

	public String getBorrowFax() {
		return borrowFax;
	}

	public void setBorrowFax(String borrowFax) {
		this.borrowFax = borrowFax;
	}

	public String getBorrowCorp() {
		return borrowCorp;
	}

	public void setBorrowCorp(String borrowCorp) {
		this.borrowCorp = borrowCorp;
	}

	public String getBorrowAddr() {
		return borrowAddr;
	}

	public void setBorrowAddr(String borrowAddr) {
		this.borrowAddr = borrowAddr;
	}

	public String getRemoveTel() {
		return removeTel;
	}

	public void setRemoveTel(String removeTel) {
		this.removeTel = removeTel;
	}

	public String getRemoveFax() {
		return removeFax;
	}

	public void setRemoveFax(String removeFax) {
		this.removeFax = removeFax;
	}

	public String getRemoveCorp() {
		return removeCorp;
	}

	public void setRemoveCorp(String removeCorp) {
		this.removeCorp = removeCorp;
	}

	public String getRemoveAddr() {
		return removeAddr;
	}

	public void setRemoveAddr(String removeAddr) {
		this.removeAddr = removeAddr;
	}

	@Override
    public String getOppoDir() {
		return oppoDir;
	}

	@Override
    public void setOppoDir(String oppoDir) {
		this.oppoDir = oppoDir;
	}

	public String getBorrowAccname() {
		return borrowAccname;
	}

	public void setBorrowAccname(String borrowAccname) {
		this.borrowAccname = borrowAccname;
	}

	public String getBorrowOpenBank() {
		return borrowOpenBank;
	}

	public void setBorrowOpenBank(String borrowOpenBank) {
		this.borrowOpenBank = borrowOpenBank;
	}

	public String getBorrowAccnum() {
		return borrowAccnum;
	}

	public void setBorrowAccnum(String borrowAccnum) {
		this.borrowAccnum = borrowAccnum;
	}

	public String getBorrowPsnum() {
		return borrowPsnum;
	}

	public void setBorrowPsnum(String borrowPsnum) {
		this.borrowPsnum = borrowPsnum;
	}

	public String getRemoveAccname() {
		return removeAccname;
	}

	public void setRemoveAccname(String removeAccname) {
		this.removeAccname = removeAccname;
	}

	public String getRemoveOpenBank() {
		return removeOpenBank;
	}

	public void setRemoveOpenBank(String removeOpenBank) {
		this.removeOpenBank = removeOpenBank;
	}

	public String getRemoveAccnum() {
		return removeAccnum;
	}

	public void setRemoveAccnum(String removeAccnum) {
		this.removeAccnum = removeAccnum;
	}

	public String getRemovePsnum() {
		return removePsnum;
	}

	public void setRemovePsnum(String removePsnum) {
		this.removePsnum = removePsnum;
	}

	@Override
    public String getMyUserName() {
		return myUserName;
	}

	@Override
    public void setMyUserName(String myUserName) {
		this.myUserName = myUserName;
	}

	@Override
    public String getBuyCurreny() {
		return buyCurreny;
	}

	@Override
    public void setBuyCurreny(String buyCurreny) {
		this.buyCurreny = buyCurreny;
	}

	@Override
    public String getBankName1() {
		return bankName1;
	}

	@Override
    public void setBankName1(String bankName1) {
		this.bankName1 = bankName1;
	}

	@Override
    public String getBankBic1() {
		return bankBic1;
	}

	@Override
    public void setBankBic1(String bankBic1) {
		this.bankBic1 = bankBic1;
	}

	@Override
    public String getDueBank1() {
		return dueBank1;
	}

	@Override
    public void setDueBank1(String dueBank1) {
		this.dueBank1 = dueBank1;
	}

	@Override
    public String getDueBankName1() {
		return dueBankName1;
	}

	@Override
    public void setDueBankName1(String dueBankName1) {
		this.dueBankName1 = dueBankName1;
	}

	@Override
    public String getDueBankAccount1() {
		return dueBankAccount1;
	}

	@Override
    public void setDueBankAccount1(String dueBankAccount1) {
		this.dueBankAccount1 = dueBankAccount1;
	}

	public String getIntermediaryBank1() {
		return intermediaryBank1;
	}

	public void setIntermediaryBank1(String intermediaryBank1) {
		this.intermediaryBank1 = intermediaryBank1;
	}

	public String getIntermediaryBicCode1() {
		return intermediaryBicCode1;
	}

	public void setIntermediaryBicCode1(String intermediaryBicCode1) {
		this.intermediaryBicCode1 = intermediaryBicCode1;
	}

	@Override
    public String getSellCurreny() {
		return sellCurreny;
	}

	@Override
    public void setSellCurreny(String sellCurreny) {
		this.sellCurreny = sellCurreny;
	}

	@Override
    public String getBankName2() {
		return bankName2;
	}

	@Override
    public void setBankName2(String bankName2) {
		this.bankName2 = bankName2;
	}

	@Override
    public String getBankBic2() {
		return bankBic2;
	}

	@Override
    public void setBankBic2(String bankBic2) {
		this.bankBic2 = bankBic2;
	}

	@Override
    public String getDueBank2() {
		return dueBank2;
	}

	@Override
    public void setDueBank2(String dueBank2) {
		this.dueBank2 = dueBank2;
	}

	@Override
    public String getDueBankName2() {
		return dueBankName2;
	}

	@Override
    public void setDueBankName2(String dueBankName2) {
		this.dueBankName2 = dueBankName2;
	}

	@Override
    public String getDueBankAccount2() {
		return dueBankAccount2;
	}

	@Override
    public void setDueBankAccount2(String dueBankAccount2) {
		this.dueBankAccount2 = dueBankAccount2;
	}

	public String getIntermediaryBank2() {
		return intermediaryBank2;
	}

	public void setIntermediaryBank2(String intermediaryBank2) {
		this.intermediaryBank2 = intermediaryBank2;
	}

	public String getIntermediaryBicCode2() {
		return intermediaryBicCode2;
	}

	public void setIntermediaryBicCode2(String intermediaryBicCode2) {
		this.intermediaryBicCode2 = intermediaryBicCode2;
	}

	public String getPriceDeviation() {
		return priceDeviation == null ? "" : priceDeviation;
	}

	public void setPriceDeviation(String priceDeviation) {
		this.priceDeviation = priceDeviation == null ? "" : priceDeviation;
	}

	public BigDecimal getOccupancyDays() {
		return occupancyDays;
	}

	public void setOccupancyDays(BigDecimal occupancyDays) {
		this.occupancyDays = occupancyDays;
	}

	public String getTenor() {
		return tenor;
	}

	public void setTenor(String tenor) {
		this.tenor = tenor;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getRepaymentAmount() {
		return repaymentAmount;
	}

	public void setRepaymentAmount(BigDecimal repaymentAmount) {
		this.repaymentAmount = repaymentAmount;
	}

	public BigDecimal getInterest() {
		return interest;
	}

	public void setInterest(BigDecimal interest) {
		this.interest = interest;
	}

	public String getMyAccount() {
		return myAccount;
	}

	public void setMyAccount(String myAccount) {
		this.myAccount = myAccount;
	}

	public String getRefId() {
		return refId;
	}

	public void setRefId(String refId) {
		this.refId = refId;
	}

    @Override
    public String getTicketId() {
        return ticketId;
    }

    @Override
    public void setTicketId(String ticketId) {
        this.ticketId = ticketId;
    }

    @Override
    public String getDealTransType() {
        return dealTransType;
    }

    @Override
    public void setDealTransType(String dealTransType) {
        this.dealTransType = dealTransType;
    }

    @Override
    public String getInstId() {
        return instId;
    }

    @Override
    public void setInstId(String instId) {
        this.instId = instId;
    }

    @Override
    public String getCounterpartyInstId() {
        return counterpartyInstId;
    }

    @Override
    public void setCounterpartyInstId(String counterpartyInstId) {
        this.counterpartyInstId = counterpartyInstId;
    }

    @Override
    public String getDealer() {
        return dealer;
    }

    @Override
    public void setDealer(String dealer) {
        this.dealer = dealer;
    }

    @Override
    public String getCounterpartyDealer() {
        return counterpartyDealer;
    }

    @Override
    public void setCounterpartyDealer(String counterpartyDealer) {
        this.counterpartyDealer = counterpartyDealer;
    }

    @Override
    public String getForDate() {
        return forDate;
    }

    @Override
    public void setForDate(String forDate) {
        this.forDate = forDate;
    }

    @Override
    public String getForTime() {
        return forTime;
    }

    @Override
    public void setForTime(String forTime) {
        this.forTime = forTime;
    }

	public String getCustType() {
		return custType;
	}

	public void setCustType(String custType) {
		this.custType = custType;
	}
}