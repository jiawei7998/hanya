package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "IFS_CFETSRMB_BONDFWD")
public class IfsCfetsrmbBondfwd extends IfsBaseFlow implements Serializable {

	private String contractId;

	private BigDecimal price;

//	private BigDecimal amount;

	private String dealTransType;

	private String tenor;

	private String tradingProduct;

	private String ticketId;

	private String forDate;

	private String buyInst;

	private String buyTrader;

	private String buyTel;

	private String buyFax;

	private String buyCorp;

	private String buyAddr;

	private String sellInst;

	private String sellTrader;

	private String sellTel;

	private String sellFax;

	private String sellCorp;

	private String sellAddr;

	private String bondCode;

	private String bondName;

	private BigDecimal cleanPrice;

	private BigDecimal totalFaceValue;

	private BigDecimal yield;

	private BigDecimal tradeAmount;

	private BigDecimal accuredInterest;

	private BigDecimal totalAccuredInterest;

	private BigDecimal dirtyPrice;

	private BigDecimal settlementAmount;

	private String settlementMethod;

	private String settlementDate;

	private String buyAccname;

	private String buyOpbank;

	private String buyAccnum;

	private String buyPsnum;

	private String buyCaname;

	private String buyCustname;

	private String buyCanum;

	private String sellAccname;

	private String sellOpbank;

	private String sellAccnum;

	private String sellPsnum;

	private String sellCaname;

	private String sellCustname;

	private String sellCanum;

	private String invtype;

	//权重
	private String weight;

	//占用授信主体
	private String custNo;
	//占用授信主体类型
	private String custType;
	//审批单号
	private String applyNo;
	//产品名称
	private String applyProd;

	//DV01Risk
	private String Dv01Risk;

	//止损
	private String lossLimit;

	//久期限
	private String longLimit;

	//风险程度
	private String riskDegree;

	//价格偏离度
	private String priceDeviation;

	@Transient
	private BigDecimal pricecheck;

	@Transient
	private BigDecimal amtcheck;

	@Transient
	private BigDecimal opicsprice;

	@Transient
	private BigDecimal opicssettleamt;

	@Transient
	private String  prdNo;
	@Transient
	private String  cfetsno;

	private String  delaydElivind;

	private static final long serialVersionUID = 1L;

	/**
	 * 五级分类
	 */
	private String fiveLevelClass;

	/**
	 * 归属部门
	 */
	private String attributionDept;

	//前置产品编号
	private String fPrdCode;

	public String getfPrdCode() {
		return fPrdCode;
	}

	public void setfPrdCode(String fPrdCode) {
		this.fPrdCode = fPrdCode;
	}

	public String getFiveLevelClass() {
		return fiveLevelClass;
	}

	public void setFiveLevelClass(String fiveLevelClass) {
		this.fiveLevelClass = fiveLevelClass;
	}

	public String getAttributionDept() {
		return attributionDept;
	}

	public void setAttributionDept(String attributionDept) {
		this.attributionDept = attributionDept;
	}

	@Override
	public String getDelaydElivind() {
		return delaydElivind;
	}

	@Override
	public void setDelaydElivind(String delaydElivind) {
		this.delaydElivind = delaydElivind;
	}

	public BigDecimal getPrice() {
		return price;
	}

	@Override
	public String getContractId() {
		return contractId;
	}

	@Override
	public void setContractId(String contractId) {
		this.contractId = contractId;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

//	public BigDecimal getAmount() {
//		return amount;
//	}

//	public void setAmount(BigDecimal amount) {
//		this.amount = amount;
//	}

	public String getCfetsno() {
		return cfetsno;
	}

	public void setCfetsno(String cfetsno) {
		this.cfetsno = cfetsno;
	}

	public String getTenor() {
		return tenor;
	}

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	public void setTenor(String tenor) {
		this.tenor = tenor;
	}

	public String getTradingProduct() {
		return tradingProduct;
	}

	public void setTradingProduct(String tradingProduct) {
		this.tradingProduct = tradingProduct;
	}

	public String getDealTransType() {
		return dealTransType;
	}

	public void setDealTransType(String dealTransType) {
		this.dealTransType = dealTransType == null ? null : dealTransType.trim();
	}

	public String getPrdNo() {
		return prdNo;
	}

	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}

	public String getForDate() {
		return forDate;
	}

	public void setForDate(String forDate) {
		this.forDate = forDate;
	}

	public String getBuyInst() {
		return buyInst;
	}

	public void setBuyInst(String buyInst) {
		this.buyInst = buyInst == null ? null : buyInst.trim();
	}

	public String getBuyTrader() {
		return buyTrader;
	}

	public void setBuyTrader(String buyTrader) {
		this.buyTrader = buyTrader == null ? null : buyTrader.trim();
	}

	public String getBuyTel() {
		return buyTel;
	}

	public void setBuyTel(String buyTel) {
		this.buyTel = buyTel == null ? null : buyTel.trim();
	}

	public String getBuyFax() {
		return buyFax;
	}

	public void setBuyFax(String buyFax) {
		this.buyFax = buyFax == null ? null : buyFax.trim();
	}

	public String getBuyCorp() {
		return buyCorp;
	}

	public void setBuyCorp(String buyCorp) {
		this.buyCorp = buyCorp == null ? null : buyCorp.trim();
	}

	public String getBuyAddr() {
		return buyAddr;
	}

	public void setBuyAddr(String buyAddr) {
		this.buyAddr = buyAddr == null ? null : buyAddr.trim();
	}

	public String getSellInst() {
		return sellInst;
	}

	public void setSellInst(String sellInst) {
		this.sellInst = sellInst == null ? null : sellInst.trim();
	}

	public String getSellTrader() {
		return sellTrader;
	}

	public void setSellTrader(String sellTrader) {
		this.sellTrader = sellTrader == null ? null : sellTrader.trim();
	}

	public String getSellTel() {
		return sellTel;
	}

	public void setSellTel(String sellTel) {
		this.sellTel = sellTel == null ? null : sellTel.trim();
	}

	public String getSellFax() {
		return sellFax;
	}

	public void setSellFax(String sellFax) {
		this.sellFax = sellFax == null ? null : sellFax.trim();
	}

	public String getSellCorp() {
		return sellCorp;
	}

	public void setSellCorp(String sellCorp) {
		this.sellCorp = sellCorp == null ? null : sellCorp.trim();
	}

	public String getSellAddr() {
		return sellAddr;
	}

	public void setSellAddr(String sellAddr) {
		this.sellAddr = sellAddr == null ? null : sellAddr.trim();
	}

	public String getBondCode() {
		return bondCode;
	}

	public void setBondCode(String bondCode) {
		this.bondCode = bondCode == null ? null : bondCode.trim();
	}

	public String getBondName() {
		return bondName;
	}

	public void setBondName(String bondName) {
		this.bondName = bondName == null ? null : bondName.trim();
	}

	public BigDecimal getCleanPrice() {
		return cleanPrice;
	}

	public void setCleanPrice(BigDecimal cleanPrice) {
		this.cleanPrice = cleanPrice;
	}

	public BigDecimal getTotalFaceValue() {
		return totalFaceValue;
	}

	public void setTotalFaceValue(BigDecimal totalFaceValue) {
		this.totalFaceValue = totalFaceValue;
	}

	public BigDecimal getYield() {
		return yield;
	}

	public void setYield(BigDecimal yield) {
		this.yield = yield;
	}

	public BigDecimal getTradeAmount() {
		return tradeAmount;
	}

	public void setTradeAmount(BigDecimal tradeAmount) {
		this.tradeAmount = tradeAmount;
	}

	public BigDecimal getAccuredInterest() {
		return accuredInterest;
	}

	public void setAccuredInterest(BigDecimal accuredInterest) {
		this.accuredInterest = accuredInterest;
	}

	public BigDecimal getTotalAccuredInterest() {
		return totalAccuredInterest;
	}

	public void setTotalAccuredInterest(BigDecimal totalAccuredInterest) {
		this.totalAccuredInterest = totalAccuredInterest;
	}

	public BigDecimal getDirtyPrice() {
		return dirtyPrice;
	}

	public void setDirtyPrice(BigDecimal dirtyPrice) {
		this.dirtyPrice = dirtyPrice;
	}

	public BigDecimal getSettlementAmount() {
		return settlementAmount;
	}

	public void setSettlementAmount(BigDecimal settlementAmount) {
		this.settlementAmount = settlementAmount;
	}

	public String getSettlementMethod() {
		return settlementMethod;
	}

	public void setSettlementMethod(String settlementMethod) {
		this.settlementMethod = settlementMethod == null ? null : settlementMethod.trim();
	}

	public String getSettlementDate() {
		return settlementDate;
	}

	public void setSettlementDate(String settlementDate) {
		this.settlementDate = settlementDate == null ? null : settlementDate.trim();
	}

	public String getBuyAccname() {
		return buyAccname;
	}

	public void setBuyAccname(String buyAccname) {
		this.buyAccname = buyAccname == null ? null : buyAccname.trim();
	}

	public String getBuyOpbank() {
		return buyOpbank;
	}

	public void setBuyOpbank(String buyOpbank) {
		this.buyOpbank = buyOpbank == null ? null : buyOpbank.trim();
	}

	public String getBuyAccnum() {
		return buyAccnum;
	}

	public void setBuyAccnum(String buyAccnum) {
		this.buyAccnum = buyAccnum == null ? null : buyAccnum.trim();
	}

	public String getBuyPsnum() {
		return buyPsnum;
	}

	public void setBuyPsnum(String buyPsnum) {
		this.buyPsnum = buyPsnum == null ? null : buyPsnum.trim();
	}

	public String getBuyCaname() {
		return buyCaname;
	}

	public void setBuyCaname(String buyCaname) {
		this.buyCaname = buyCaname == null ? null : buyCaname.trim();
	}

	public String getBuyCustname() {
		return buyCustname;
	}

	public void setBuyCustname(String buyCustname) {
		this.buyCustname = buyCustname == null ? null : buyCustname.trim();
	}

	public String getBuyCanum() {
		return buyCanum;
	}

	public void setBuyCanum(String buyCanum) {
		this.buyCanum = buyCanum == null ? null : buyCanum.trim();
	}

	public String getSellAccname() {
		return sellAccname;
	}

	public void setSellAccname(String sellAccname) {
		this.sellAccname = sellAccname == null ? null : sellAccname.trim();
	}

	public String getSellOpbank() {
		return sellOpbank;
	}

	public void setSellOpbank(String sellOpbank) {
		this.sellOpbank = sellOpbank == null ? null : sellOpbank.trim();
	}

	public String getSellAccnum() {
		return sellAccnum;
	}

	public void setSellAccnum(String sellAccnum) {
		this.sellAccnum = sellAccnum == null ? null : sellAccnum.trim();
	}

	public String getSellPsnum() {
		return sellPsnum;
	}

	public void setSellPsnum(String sellPsnum) {
		this.sellPsnum = sellPsnum == null ? null : sellPsnum.trim();
	}

	public String getSellCaname() {
		return sellCaname;
	}

	public void setSellCaname(String sellCaname) {
		this.sellCaname = sellCaname == null ? null : sellCaname.trim();
	}

	public String getSellCustname() {
		return sellCustname;
	}

	public void setSellCustname(String sellCustname) {
		this.sellCustname = sellCustname == null ? null : sellCustname.trim();
	}

	public String getSellCanum() {
		return sellCanum;
	}

	public void setSellCanum(String sellCanum) {
		this.sellCanum = sellCanum == null ? null : sellCanum.trim();
	}

	public void setInvtype(String invtype) {
		this.invtype = invtype;
	}

	public String getInvtype() {
		return invtype;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	@Override
	public String getCustNo() {
		return custNo;
	}

	@Override
	public void setCustNo(String custNo) {
		this.custNo = custNo;
	}

	public String getApplyNo() {
		return applyNo;
	}

	public void setApplyNo(String applyNo) {
		this.applyNo = applyNo;
	}

	public String getApplyProd() {
		return applyProd;
	}

	public void setApplyProd(String applyProd) {
		this.applyProd = applyProd;
	}

	public String getDv01Risk() {
		return Dv01Risk;
	}

	public void setDv01Risk(String Dv01Risk) {
		this.Dv01Risk = Dv01Risk;
	}

	public String getLossLimit() {
		return lossLimit;
	}

	public void setLossLimit(String lossLimit) {
		this.lossLimit = lossLimit;
	}

	public String getLongLimit() {
		return longLimit;
	}

	public void setLongLimit(String longLimit) {
		this.longLimit = longLimit;
	}

	public String getRiskDegree() {
		return riskDegree;
	}

	public void setRiskDegree(String riskDegree) {
		this.riskDegree = riskDegree;
	}
	public String getPriceDeviation() {
		return priceDeviation==null?"":priceDeviation;
	}

	public void setPriceDeviation(String priceDeviation) {
		this.priceDeviation = priceDeviation==null?"":priceDeviation;
	}

	public BigDecimal getPricecheck() {
		return pricecheck;
	}

	public void setPricecheck(BigDecimal pricecheck) {
		this.pricecheck = pricecheck;
	}

	public BigDecimal getAmtcheck() {
		return amtcheck;
	}

	public void setAmtcheck(BigDecimal amtcheck) {
		this.amtcheck = amtcheck;
	}

	public BigDecimal getOpicsprice() {
		return opicsprice;
	}

	public void setOpicsprice(BigDecimal opicsprice) {
		this.opicsprice = opicsprice;
	}

	public BigDecimal getOpicssettleamt() {
		return opicssettleamt;
	}

	public void setOpicssettleamt(BigDecimal opicssettleamt) {
		this.opicssettleamt = opicssettleamt;
	}

//	public String getCfetsno() {
//		return cfetsno;
//	}
//
//	public void setCfetsno(String cfetsno) {
//		this.cfetsno = cfetsno;
//	}

	public String getCustType() {
		return custType;
	}

	public void setCustType(String custType) {
		this.custType = custType;
	}
}