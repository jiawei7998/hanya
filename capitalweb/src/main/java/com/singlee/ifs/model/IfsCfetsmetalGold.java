package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
@Entity
@Table(name = "IFS_CFETSMETAL_GOLD")
public class IfsCfetsmetalGold extends IfsCfetsmetalGoldKey implements Serializable {
	private String type;
	
    private String price;

    private String direction1;
    
    private String amount1;

    private String direction2;
    
    private String amount2;

    private String tenor1;

    private String valueDate1;

    private String direction3;
    
    private String amount3;

    private String direction4;
    
    private String amount4;

    private String tenor2;

    private String valueDate2;

    private String settleMode;
    //卖方币种
    private String sellCurreny;
    //买方币种
    private String buyCurreny;
    //点差
    private String spread;
    //数量
    private String quantity; 
    //价格
    private String prix;
    //币种1(放重要币种)
    private String opicsccy;
    //币种2(放次要币种)
	private String opicsctrccy;
	
	//权重
    private String weight;
    //占用授信主体
  	private String custNo;
  	//审批单号
  	private String applyNo;
  	//产品名称
  	private String applyProd;
  	
  //DV01Risk
	private String DV01Risk;
	
	//止损
	private String lossLimit;
	
	//久期限
	private String longLimit;
	
	//风险程度
	private String riskDegree;
	
	private String siindyn;
	
	private String swapDealno;
	
	@Transient
	private String  prdNo;
	
	
	
    public String getPrdNo() {
		return prdNo;
	}

	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}

	public String getOpicsccy() {
		return opicsccy;
	}

	public void setOpicsccy(String opicsccy) {
		this.opicsccy = opicsccy;
	}

	public String getOpicsctrccy() {
		return opicsctrccy;
	}

	public void setOpicsctrccy(String opicsctrccy) {
		this.opicsctrccy = opicsctrccy;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getPrix() {
		return prix;
	}

	public void setPrix(String prix) {
		this.prix = prix;
	}

    public String getSpread() {
		return spread;
	}

	public void setSpread(String spread) {
		this.spread = spread;
	}

	public String getSellCurreny() {
		return sellCurreny;
	}

	public void setSellCurreny(String sellCurreny) {
		this.sellCurreny = sellCurreny;
	}

	public String getBuyCurreny() {
		return buyCurreny;
	}

	public void setBuyCurreny(String buyCurreny) {
		this.buyCurreny = buyCurreny;
	}

	private static final long serialVersionUID = 1L;

    public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price == null ? null : price.trim();
    }

    public String getTenor1() {
        return tenor1;
    }

    public void setTenor1(String tenor1) {
        this.tenor1 = tenor1 == null ? null : tenor1.trim();
    }

    public String getValueDate1() {
        return valueDate1;
    }

    public void setValueDate1(String valueDate1) {
        this.valueDate1 = valueDate1;
    }

    public String getTenor2() {
        return tenor2;
    }

    public void setTenor2(String tenor2) {
        this.tenor2 = tenor2 == null ? null : tenor2.trim();
    }

    public String getValueDate2() {
        return valueDate2;
    }

    public void setValueDate2(String valueDate2) {
        this.valueDate2 = valueDate2;
    }

    public String getSettleMode() {
        return settleMode;
    }

    public void setSettleMode(String settleMode) {
        this.settleMode = settleMode == null ? null : settleMode.trim();
    }

	public String getDirection1() {
		return direction1;
	}

	public void setDirection1(String direction1) {
		this.direction1 = direction1;
	}

	public String getAmount1() {
		return amount1;
	}

	public void setAmount1(String amount1) {
		this.amount1 = amount1;
	}

	public String getDirection2() {
		return direction2;
	}

	public void setDirection2(String direction2) {
		this.direction2 = direction2;
	}

	public String getAmount2() {
		return amount2;
	}

	public void setAmount2(String amount2) {
		this.amount2 = amount2;
	}

	public String getDirection3() {
		return direction3;
	}

	public void setDirection3(String direction3) {
		this.direction3 = direction3;
	}

	public String getAmount3() {
		return amount3;
	}

	public void setAmount3(String amount3) {
		this.amount3 = amount3;
	}

	public String getDirection4() {
		return direction4;
	}

	public void setDirection4(String direction4) {
		this.direction4 = direction4;
	}

	public String getAmount4() {
		return amount4;
	}

	public void setAmount4(String amount4) {
		this.amount4 = amount4;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	@Override
    public String getCustNo() {
		return custNo;
	}

	@Override
    public void setCustNo(String custNo) {
		this.custNo = custNo;
	}

	public String getApplyNo() {
		return applyNo;
	}

	public void setApplyNo(String applyNo) {
		this.applyNo = applyNo;
	}

	public String getApplyProd() {
		return applyProd;
	}

	public void setApplyProd(String applyProd) {
		this.applyProd = applyProd;
	}
	public String getDV01Risk() {
		return DV01Risk;
	}

	public void setDV01Risk(String dV01Risk) {
		this.DV01Risk = dV01Risk;
	}

	public String getLossLimit() {
		return lossLimit;
	}

	public void setLossLimit(String lossLimit) {
		this.lossLimit = lossLimit;
	}

	public String getLongLimit() {
		return longLimit;
	}

	public void setLongLimit(String longLimit) {
		this.longLimit = longLimit;
	}

	public String getRiskDegree() {
		return riskDegree;
	}

	public void setRiskDegree(String riskDegree) {
		this.riskDegree = riskDegree;
	}

	public void setSiindyn(String siindyn) {
		this.siindyn = siindyn;
	}

	public String getSiindyn() {
		return siindyn;
	}

	public void setSwapDealno(String swapDealno) {
		this.swapDealno = swapDealno;
	}

	public String getSwapDealno() {
		return swapDealno;
	}

	
}