package com.singlee.ifs.model;

import javax.persistence.Transient;
import java.io.Serializable;

/**
 * 冲销 - 流程公共字段
 * @author lij
 *
 */
public class IfsRevFlow implements Serializable{
	private static final long serialVersionUID = 1L;
	
	//审批状态
	private String approveStatus;
	
	//审批发起人
	private String sponsor;
	
	//审批发起机构
	private String sponInst;
	
	//审批发起日期
	private String sponDate;
	
	
	//流程id
	@Transient
	private String taskId;
	
	//审批发起机构中文名称
	@Transient
	private String instName;
	
	//审批发起人中文名称
	@Transient
	private String userName;
	
	

	public String getApproveStatus() {
		return approveStatus;
	}

	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}

	public String getSponsor() {
		return sponsor;
	}

	public void setSponsor(String sponsor) {
		this.sponsor = sponsor;
	}

	public String getSponInst() {
		return sponInst;
	}

	public void setSponInst(String sponInst) {
		this.sponInst = sponInst;
	}

	public String getSponDate() {
		return sponDate;
	}

	public void setSponDate(String sponDate) {
		this.sponDate = sponDate;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getInstName() {
		return instName;
	}

	public void setInstName(String instName) {
		this.instName = instName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
