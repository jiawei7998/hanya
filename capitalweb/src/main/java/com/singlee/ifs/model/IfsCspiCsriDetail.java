package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "IFS_CSPICSRI_DETAIL")
public class IfsCspiCsriDetail implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7803992207641233423L;

	private String br;
	private String server;
	private String fedealno;
	private String seq;
	private String inoutind;
	private String product;
	private String prodtype;
	private String acctno;
	private String recordtype;
	private String payrecind;
	private String addupdelind;
	private String bic;
	private String pcc;
	private String p1;
	private String p2;
	private String p3;
	private String p4;
	
	private String cno;
	private String ccy;

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getServer() {
		return server;
	}

	public void setServer(String server) {
		this.server = server;
	}

	public String getFedealno() {
		return fedealno;
	}

	public void setFedealno(String fedealno) {
		this.fedealno = fedealno;
	}

	public String getSeq() {
		return seq;
	}

	public void setSeq(String seq) {
		this.seq = seq;
	}

	public String getInoutind() {
		return inoutind;
	}

	public void setInoutind(String inoutind) {
		this.inoutind = inoutind;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getProdtype() {
		return prodtype;
	}

	public void setProdtype(String prodtype) {
		this.prodtype = prodtype;
	}

	public String getAcctno() {
		return acctno;
	}

	public void setAcctno(String acctno) {
		this.acctno = acctno;
	}

	public String getRecordtype() {
		return recordtype;
	}

	public void setRecordtype(String recordtype) {
		this.recordtype = recordtype;
	}

	public String getPayrecind() {
		return payrecind;
	}

	public void setPayrecind(String payrecind) {
		this.payrecind = payrecind;
	}

	public String getAddupdelind() {
		return addupdelind;
	}

	public void setAddupdelind(String addupdelind) {
		this.addupdelind = addupdelind;
	}

	public String getBic() {
		return bic;
	}

	public void setBic(String bic) {
		this.bic = bic;
	}

	public String getPcc() {
		return pcc;
	}

	public void setPcc(String pcc) {
		this.pcc = pcc;
	}

	public String getP1() {
		return p1;
	}

	public void setP1(String p1) {
		this.p1 = p1;
	}

	public String getP2() {
		return p2;
	}

	public void setP2(String p2) {
		this.p2 = p2;
	}

	public String getP3() {
		return p3;
	}

	public void setP3(String p3) {
		this.p3 = p3;
	}

	public String getP4() {
		return p4;
	}

	public void setP4(String p4) {
		this.p4 = p4;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void setCno(String cno) {
		this.cno = cno;
	}

	public String getCno() {
		return cno;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getCcy() {
		return ccy;
	}

}
