package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/***
 * opics 系统seta表
 * @author yanming
 */
@Entity
@Table(name="IFS_OPICS_SETA")
public class IfsOpicsSeta implements Serializable{
	
	/**
     * 机构分支
     */
    private String br;
    
    /**
     * 清算路径
     */
    private String smeans;
    
    /**
     * 清算账户
     */
    private String sacct;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
     * 币种
     */
    private String ccy;

    /**
     * 成本中心
     */
    private String costcent;

    /**
     * 
     */
    private String genledgno;

    /**
     * 交易对手
     */
    private String cno;

    /**
     * 
     */
    private BigDecimal acctbal;

    /**
     * 最后维护时间
     */
    private Date lstmntdte;

    /**
     * 操作者
     */
    private String operator;

    /**
     * 同步状态
     */
    private String status;

    /**
     * 备注
     */
    private String remark;

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getCostcent() {
		return costcent;
	}

	public void setCostcent(String costcent) {
		this.costcent = costcent;
	}

	public String getGenledgno() {
		return genledgno;
	}

	public void setGenledgno(String genledgno) {
		this.genledgno = genledgno;
	}

	public String getCno() {
		return cno;
	}

	public void setCno(String cno) {
		this.cno = cno;
	}

	public BigDecimal getAcctbal() {
		return acctbal;
	}

	public void setAcctbal(BigDecimal acctbal) {
		this.acctbal = acctbal;
	}

	public Date getLstmntdte() {
		return lstmntdte;
	}

	public void setLstmntdte(Date lstmntdte) {
		this.lstmntdte = lstmntdte;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getSmeans() {
		return smeans;
	}

	public void setSmeans(String smeans) {
		this.smeans = smeans;
	}

	public String getSacct() {
		return sacct;
	}

	public void setSacct(String sacct) {
		this.sacct = sacct;
	}
    
}

