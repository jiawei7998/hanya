package com.singlee.ifs.model;

import java.io.Serializable;

public class LoanBean implements Serializable {

	private static final long serialVersionUID = -1240472590050776469L;
	// 交易编号
	private String custcreditid;
	// 客户编号
	private String custno;
	// 授信客户名称
	private String custname;
	// 信用主体信息
	private String customer;
	// 授信状态
	private String cntrstatus;
	// 授信标识
	private String state;
	// 币种
	private String ccy;
	// 产品类型
	private String productname;
	// 基础授信额度
	private String loanamt;
	// 用信金额
	private String amt;
	// 起始日期
	private String vdate;
	// 到期日期
	private String mdate;
	// 经办人
	private String ioper;
	// 用信部门
	private String institution;

	public String getCustcreditid() {
		return custcreditid;
	}

	public void setCustcreditid(String custcreditid) {
		this.custcreditid = custcreditid;
	}

	public String getCustno() {
		return custno;
	}

	public void setCustno(String custno) {
		this.custno = custno;
	}

	public String getCustname() {
		return custname;
	}

	public void setCustname(String custname) {
		this.custname = custname;
	}

	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public String getCntrstatus() {
		return cntrstatus;
	}

	public void setCntrstatus(String cntrstatus) {
		this.cntrstatus = cntrstatus;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getProductname() {
		return productname;
	}

	public void setProductname(String productname) {
		this.productname = productname;
	}

	public String getLoanamt() {
		return loanamt;
	}

	public void setLoanamt(String loanamt) {
		this.loanamt = loanamt;
	}

	public String getAmt() {
		return amt;
	}

	public void setAmt(String amt) {
		this.amt = amt;
	}

	public String getVdate() {
		return vdate;
	}

	public void setVdate(String vdate) {
		this.vdate = vdate;
	}

	public String getMdate() {
		return mdate;
	}

	public void setMdate(String mdate) {
		this.mdate = mdate;
	}

	public String getIoper() {
		return ioper;
	}

	public void setIoper(String ioper) {
		this.ioper = ioper;
	}

	public String getInstitution() {
		return institution;
	}

	public void setInstitution(String institution) {
		this.institution = institution;
	}
}