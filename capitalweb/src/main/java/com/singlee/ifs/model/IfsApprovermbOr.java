package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;
@Entity
@Table(name = "IFS_APPROVERMB_OR")
public class IfsApprovermbOr extends IfsApproveBaseBean {

    private String bondCode;

    private String bondName;

    private String tradingProduct;

    private String tenor;

    private BigDecimal repoRate;

    private BigDecimal totalFaceValue;

    private BigDecimal firstCleanPrice;

    private BigDecimal firstYield;

    private BigDecimal firstAccuredInterest;

    private BigDecimal secondCleanPrice;

    private BigDecimal secondYield;

    private BigDecimal secondAccuredInterest;

    private BigDecimal firstDirtyPrice;

    private BigDecimal firstSettlementAmount;

    private String firstSettlementMethod;

    private BigDecimal secondDirtyPrice;

    private BigDecimal secondSettlementAmount;

    private String secondSettlementMethod;

    private String firstSettlementDate;

    private String secondSettlementDate;

    private String occupancyDays;

    private static final long serialVersionUID = 1L;

	public String getBondCode() {
		return bondCode;
	}

	public void setBondCode(String bondCode) {
		this.bondCode = bondCode;
	}

	public String getBondName() {
		return bondName;
	}

	public void setBondName(String bondName) {
		this.bondName = bondName;
	}

	public String getTradingProduct() {
		return tradingProduct;
	}

	public void setTradingProduct(String tradingProduct) {
		this.tradingProduct = tradingProduct;
	}

	public String getTenor() {
		return tenor;
	}

	public void setTenor(String tenor) {
		this.tenor = tenor;
	}

	public BigDecimal getRepoRate() {
		return repoRate;
	}

	public void setRepoRate(BigDecimal repoRate) {
		this.repoRate = repoRate;
	}

	public BigDecimal getTotalFaceValue() {
		return totalFaceValue;
	}

	public void setTotalFaceValue(BigDecimal totalFaceValue) {
		this.totalFaceValue = totalFaceValue;
	}

	public BigDecimal getFirstCleanPrice() {
		return firstCleanPrice;
	}

	public void setFirstCleanPrice(BigDecimal firstCleanPrice) {
		this.firstCleanPrice = firstCleanPrice;
	}

	public BigDecimal getFirstYield() {
		return firstYield;
	}

	public void setFirstYield(BigDecimal firstYield) {
		this.firstYield = firstYield;
	}

	public BigDecimal getFirstAccuredInterest() {
		return firstAccuredInterest;
	}

	public void setFirstAccuredInterest(BigDecimal firstAccuredInterest) {
		this.firstAccuredInterest = firstAccuredInterest;
	}

	public BigDecimal getSecondCleanPrice() {
		return secondCleanPrice;
	}

	public void setSecondCleanPrice(BigDecimal secondCleanPrice) {
		this.secondCleanPrice = secondCleanPrice;
	}

	public BigDecimal getSecondYield() {
		return secondYield;
	}

	public void setSecondYield(BigDecimal secondYield) {
		this.secondYield = secondYield;
	}

	public BigDecimal getSecondAccuredInterest() {
		return secondAccuredInterest;
	}

	public void setSecondAccuredInterest(BigDecimal secondAccuredInterest) {
		this.secondAccuredInterest = secondAccuredInterest;
	}

	public BigDecimal getFirstDirtyPrice() {
		return firstDirtyPrice;
	}

	public void setFirstDirtyPrice(BigDecimal firstDirtyPrice) {
		this.firstDirtyPrice = firstDirtyPrice;
	}

	public BigDecimal getFirstSettlementAmount() {
		return firstSettlementAmount;
	}

	public void setFirstSettlementAmount(BigDecimal firstSettlementAmount) {
		this.firstSettlementAmount = firstSettlementAmount;
	}

	public String getFirstSettlementMethod() {
		return firstSettlementMethod;
	}

	public void setFirstSettlementMethod(String firstSettlementMethod) {
		this.firstSettlementMethod = firstSettlementMethod;
	}

	public BigDecimal getSecondDirtyPrice() {
		return secondDirtyPrice;
	}

	public void setSecondDirtyPrice(BigDecimal secondDirtyPrice) {
		this.secondDirtyPrice = secondDirtyPrice;
	}

	public BigDecimal getSecondSettlementAmount() {
		return secondSettlementAmount;
	}

	public void setSecondSettlementAmount(BigDecimal secondSettlementAmount) {
		this.secondSettlementAmount = secondSettlementAmount;
	}

	public String getSecondSettlementMethod() {
		return secondSettlementMethod;
	}

	public void setSecondSettlementMethod(String secondSettlementMethod) {
		this.secondSettlementMethod = secondSettlementMethod;
	}

	public String getFirstSettlementDate() {
		return firstSettlementDate;
	}

	public void setFirstSettlementDate(String firstSettlementDate) {
		this.firstSettlementDate = firstSettlementDate;
	}

	public String getSecondSettlementDate() {
		return secondSettlementDate;
	}

	public void setSecondSettlementDate(String secondSettlementDate) {
		this.secondSettlementDate = secondSettlementDate;
	}

	public String getOccupancyDays() {
		return occupancyDays;
	}

	public void setOccupancyDays(String occupancyDays) {
		this.occupancyDays = occupancyDays;
	}


}