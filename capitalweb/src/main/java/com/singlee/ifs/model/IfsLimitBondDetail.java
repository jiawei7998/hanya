package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "BOND_LIMIT_DETAIL")//债券限额明细
public class IfsLimitBondDetail implements Serializable{

	private static final long serialVersionUID = 1L;
	
	/**
	 * 业务流水号
	 */
	private String bondId;
	/**
	 * 交易流水号
	 */
	private String ticketId;
	/**
	 * 业务品种
	 */
	private String  prdNo;
	/**
	 * 经办人
	 */
	private String trad;
	/**
	 * 限额类型
	 */
	private String limitType;
	/**
	 * 产品
	 */
	private String prd;
	/**
	 * 产品类型
	 */
	private String prdType;
	/**
	 * 债券类型
	 */
	private String accType;
	/**
	 * 产品
	 */
	private String bondType;
	/**
	 * 币种
	 */
	private String ccy;
	/**
	 * 监测监控标志
	 */
	private String ctltype;
	/**
	 * 交易金额
	 */
	private String amt;
	/**
	 * 录入时间
	 */
	private String loadTime;
	/**
	 * 累计
	 */
	private String todayTotal;
	/**
	 * 方向
	 */
	private String direction;
	/**
	 * 序列号
	 */
	private String id;
	/**
	 * 到期日
	 */
	private String matdt;
	
	/**
	 * 债券代码
	 * @return
	 */
	private String noUse1;
	
	public String getBondId() {
		return bondId;
	}
	public void setBondId(String bondId) {
		this.bondId = bondId;
	}
	public String getTicketId() {
		return ticketId;
	}
	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}
	public String getPrdNo() {
		return prdNo;
	}
	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}
	public String getTrad() {
		return trad;
	}
	public void setTrad(String trad) {
		this.trad = trad;
	}
	public String getLimitType() {
		return limitType;
	}
	public void setLimitType(String limitType) {
		this.limitType = limitType;
	}
	public String getPrd() {
		return prd;
	}
	public void setPrd(String prd) {
		this.prd = prd;
	}
	public String getPrdType() {
		return prdType;
	}
	public void setPrdType(String prdType) {
		this.prdType = prdType;
	}
	public String getAccType() {
		return accType;
	}
	public void setAccType(String accType) {
		this.accType = accType;
	}
	public String getBondType() {
		return bondType;
	}
	public void setBondType(String bondType) {
		this.bondType = bondType;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public String getCtltype() {
		return ctltype;
	}
	public void setCtltype(String ctltype) {
		this.ctltype = ctltype;
	}
	public String getAmt() {
		return amt;
	}
	public void setAmt(String amt) {
		this.amt = amt;
	}
	public String getLoadTime() {
		return loadTime;
	}
	public void setLoadTime(String loadTime) {
		this.loadTime = loadTime;
	}
	public String getTodayTotal() {
		return todayTotal;
	}
	public void setTodayTotal(String todayTotal) {
		this.todayTotal = todayTotal;
	}
	public String getDirection() {
		return direction;
	}
	public void setDirection(String direction) {
		this.direction = direction;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getMatdt() {
		return matdt;
	}
	public void setMatdt(String matdt) {
		this.matdt = matdt;
	}
	public String getNoUse1() {
		return noUse1;
	}
	public void setNoUse1(String noUse1) {
		this.noUse1 = noUse1;
	}
	

}
