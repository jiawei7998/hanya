package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * 报文发送记录
 *
 */
@Entity
@Table(name = "IFS_ESB_MASSAGE")
public class IfsEsbMassage implements Serializable {
	private static final long serialVersionUID = 1L;

	private String service;// 服务名
	private String dealno;// 交易单号o;// 交易单号
	private String custno;// 交易对手
	private String product;// 产品
	private String producttype;// 产品类型
	private String amount;// 交易金额
	private String ccy;// 交易币种
	private String setmeans;// 报文类型
	private Date senddate;// 发送时间
	private Date recdate;// 接收时间
	// @Transient
	private String sendmsg;// 发送报文
	// @Transient
	private String recmsg;// 接收报文
	private String rescode;// 返回状态码
	private String resmsg;// 返回信息
	private String sefile;// 发送文件路径
	private String refile;// 接收文件路径

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getDealno() {
		return dealno;
	}

	public void setDealno(String dealno) {
		this.dealno = dealno;
	}

	public String getCustno() {
		return custno;
	}

	public void setCustno(String custno) {
		this.custno = custno;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getProducttype() {
		return producttype;
	}

	public void setProducttype(String producttype) {
		this.producttype = producttype;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getSetmeans() {
		return setmeans;
	}

	public void setSetmeans(String setmeans) {
		this.setmeans = setmeans;
	}

	public Date getSenddate() {
		return senddate;
	}

	public void setSenddate(Date senddate) {
		this.senddate = senddate;
	}

	public Date getRecdate() {
		return recdate;
	}

	public void setRecdate(Date recdate) {
		this.recdate = recdate;
	}

	public String getSendmsg() {
		return sendmsg;
	}

	public void setSendmsg(String sendmsg) {
		this.sendmsg = sendmsg;
	}

	public String getRecmsg() {
		return recmsg;
	}

	public void setRecmsg(String recmsg) {
		this.recmsg = recmsg;
	}

	public String getRescode() {
		return rescode;
	}

	public void setRescode(String rescode) {
		this.rescode = rescode;
	}

	public String getResmsg() {
		return resmsg;
	}

	public void setResmsg(String resmsg) {
		this.resmsg = resmsg;
	}

	public String getSefile() {
		return sefile;
	}

	public void setSefile(String sefile) {
		this.sefile = sefile;
	}

	public String getRefile() {
		return refile;
	}

	public void setRefile(String refile) {
		this.refile = refile;
	}

}
