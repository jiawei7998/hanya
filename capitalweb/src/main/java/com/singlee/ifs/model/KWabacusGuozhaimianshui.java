package com.singlee.ifs.model;

import java.math.BigDecimal;

/**
 * @author copysun
 */
public class KWabacusGuozhaimianshui {
    private BigDecimal id;

    private BigDecimal version;

    private String guozhai;

    private String zhengfuzhai;

    private String tdz;

    private String yingyeshui;

    private String createdate;

    private BigDecimal creator;

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public BigDecimal getVersion() {
        return version;
    }

    public void setVersion(BigDecimal version) {
        this.version = version;
    }

    public String getGuozhai() {
        return guozhai;
    }

    public void setGuozhai(String guozhai) {
        this.guozhai = guozhai;
    }

    public String getZhengfuzhai() {
        return zhengfuzhai;
    }

    public void setZhengfuzhai(String zhengfuzhai) {
        this.zhengfuzhai = zhengfuzhai;
    }

    public String getTdz() {
        return tdz;
    }

    public void setTdz(String tdz) {
        this.tdz = tdz;
    }

    public String getYingyeshui() {
        return yingyeshui;
    }

    public void setYingyeshui(String yingyeshui) {
        this.yingyeshui = yingyeshui;
    }

    public String getCreatedate() {
        return createdate;
    }

    public void setCreatedate(String createdate) {
        this.createdate = createdate;
    }

    public BigDecimal getCreator() {
        return creator;
    }

    public void setCreator(BigDecimal creator) {
        this.creator = creator;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", version=").append(version);
        sb.append(", guozhai=").append(guozhai);
        sb.append(", zhengfuzhai=").append(zhengfuzhai);
        sb.append(", tdz=").append(tdz);
        sb.append(", yingyeshui=").append(yingyeshui);
        sb.append(", createdate=").append(createdate);
        sb.append(", creator=").append(creator);
        sb.append("]");
        return sb.toString();
    }
}