package com.singlee.ifs.model;

import java.io.Serializable;

public class IfsCfetsrmbDpKey extends IfsBaseFlow  implements Serializable{
	private String dealTransType;

    private String ticketId;

    private static final long serialVersionUID = 1L;

    public String getDealTransType() {
        return dealTransType;
    }

    public void setDealTransType(String dealTransType) {
        this.dealTransType = dealTransType == null ? null : dealTransType.trim();
    }

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}
}
