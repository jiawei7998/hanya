package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "IFS_LIMIT_TEMPLATE_DETAIL")//外汇限额明细
public class IfsLimitTemplateDetail implements Serializable{

	private static final long serialVersionUID = 1L;
	
	/**
	 * 业务流水号
	 */
	private String limitId;
	/**
	 * 交易流水号
	 */
	private String ticketId;
	/**
	 * 业务品种
	 */
	private String  prdNo;
	/**
	 * 经办人
	 */
	private String trad;
	/**
	 * 产品
	 */
	private String prd;
	/**
	 * 产品类型
	 */
	private String prdType;
	/**
	 * 币种
	 */
	private String ccy;
	/**
	 * 金额
	 */
	private String amt;
	/**
	 * 金额折美元
	 */
	private String amtu;
	/**
	 * 限额时间
	 */
	private String loadTime;
	/**
	 * 成本中心
	 */
	private String cost;
	/**
	 * 当日最大值
	 */
	private String maxToday;
	/**
	 * 昨日敞口
	 */
	private String lastdayTotal;
	/**
	 * 当日实时总计
	 */
	private String todayTotal;
	
	/**
	 * 序列号
	 */
	private String id;
	/**
	 * 昨日敞口
	 */
	private String maxLast;
	/**
	 * 交易方向
	 */
	private String direction;
	
	
	public String getLimitId() {
		return limitId;
	}
	public void setLimitId(String limitId) {
		this.limitId = limitId;
	}
	public String getTicketId() {
		return ticketId;
	}
	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}
	public String getPrdNo() {
		return prdNo;
	}
	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}
	public String getTrad() {
		return trad;
	}
	public void setTrad(String trad) {
		this.trad = trad;
	}
	public String getPrd() {
		return prd;
	}
	public void setPrd(String prd) {
		this.prd = prd;
	}
	public String getPrdType() {
		return prdType;
	}
	public void setPrdType(String prdType) {
		this.prdType = prdType;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public String getAmt() {
		return amt;
	}
	public void setAmt(String amt) {
		this.amt = amt;
	}
	public String getAmtu() {
		return amtu;
	}
	public void setAmtu(String amtu) {
		this.amtu = amtu;
	}
	public String getLoadTime() {
		return loadTime;
	}
	public void setLoadTime(String loadTime) {
		this.loadTime = loadTime;
	}
	public String getCost() {
		return cost;
	}
	public void setCost(String cost) {
		this.cost = cost;
	}
	public String getMaxToday() {
		return maxToday;
	}
	public void setMaxToday(String maxToday) {
		this.maxToday = maxToday;
	}
	public String getLastdayTotal() {
		return lastdayTotal;
	}
	public void setLastdayTotal(String lastdayTotal) {
		this.lastdayTotal = lastdayTotal;
	}
	public String getTodayTotal() {
		return todayTotal;
	}
	public void setTodayTotal(String todayTotal) {
		this.todayTotal = todayTotal;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getMaxLast() {
		return maxLast;
	}
	public void setMaxLast(String maxLast) {
		this.maxLast = maxLast;
	}
	public String getDirection() {
		return direction;
	}
	public void setDirection(String direction) {
		this.direction = direction;
	}
	
	

}
