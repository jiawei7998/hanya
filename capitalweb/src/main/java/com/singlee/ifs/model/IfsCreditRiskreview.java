package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "IFS_CREDIT_RISKREVIEW")//单笔风险审查
public class IfsCreditRiskreview implements Serializable{

	private static final long serialVersionUID = 1L;
	
	/**
	 * 产品编码
	 */
	private String orderId;
	/**
	 * 交易类型
	 */
	private String tranType;
	/**
	 * 交易状态
	 */
	private String tranStatus;
	/**
	 * 录入柜员编码
	 */
	private String inputTelNo;
	/**
	 * 产品名称
	 */
	private String productName;
	/**
	 * 年化收益率
	 */
	private String yearRate;
	/**
	 * 投资计划
	 */
	private String investPlan;
	/**
	 * 产品到期日期
	 */
	private String prodExpDate;
	/**
	 * 审批件编号
	 */
	private String approveFlno;
	
	
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getTranType() {
		return tranType;
	}
	public void setTranType(String tranType) {
		this.tranType = tranType;
	}
	public String getTranStatus() {
		return tranStatus;
	}
	public void setTranStatus(String tranStatus) {
		this.tranStatus = tranStatus;
	}
	public String getInputTelNo() {
		return inputTelNo;
	}
	public void setInputTelNo(String inputTelNo) {
		this.inputTelNo = inputTelNo;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getYearRate() {
		return yearRate;
	}
	public void setYearRate(String yearRate) {
		this.yearRate = yearRate;
	}
	public String getInvestPlan() {
		return investPlan;
	}
	public void setInvestPlan(String investPlan) {
		this.investPlan = investPlan;
	}
	public String getProdExpDate() {
		return prodExpDate;
	}
	public void setProdExpDate(String prodExpDate) {
		this.prodExpDate = prodExpDate;
	}
	public String getApproveFlno() {
		return approveFlno;
	}
	public void setApproveFlno(String approveFlno) {
		this.approveFlno = approveFlno;
	}
	
	
}
