package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;


@Entity
@Table(name = "IFS_APPROVEFX_ALLOT")
public class IfsApprovefxAllot extends IfsApproveBaseBean implements Serializable{
	
	/**
	 * serialVersionUID:TODO(用一句话描述这个变量表示什么).
	 * @since JDK 1.6
	 */
	private static final long serialVersionUID = -9052255761807190698L;
	private String	type;
	private String	cal;
	private String	payDate;
	private String	ccy;
	private BigDecimal	amount;
	private String	rule;
	private BigDecimal	discount;
	private String	payType;
	private String	relatedTrade;
	private String	relextId;
	private String	callAccount;
	private String	cpty;
	private String	broker;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getCal() {
		return cal;
	}
	public void setCal(String cal) {
		this.cal = cal;
	}
	public String getPayDate() {
		return payDate;
	}
	public void setPayDate(String payDate) {
		this.payDate = payDate;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public String getRule() {
		return rule;
	}
	public void setRule(String rule) {
		this.rule = rule;
	}
	public BigDecimal getDiscount() {
		return discount;
	}
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
	public String getPayType() {
		return payType;
	}
	public void setPayType(String payType) {
		this.payType = payType;
	}
	public String getRelatedTrade() {
		return relatedTrade;
	}
	public void setRelatedTrade(String relatedTrade) {
		this.relatedTrade = relatedTrade;
	}
	public String getCallAccount() {
		return callAccount;
	}
	public void setCallAccount(String callAccount) {
		this.callAccount = callAccount;
	}
	public String getCpty() {
		return cpty;
	}
	public void setCpty(String cpty) {
		this.cpty = cpty;
	}
	public String getBroker() {
		return broker;
	}
	public void setBroker(String broker) {
		this.broker = broker;
	}
	public String getRelextId() {
		return relextId;
	}
	public void setRelextId(String relextId) {
		this.relextId = relextId;
	}
	
	
	
}
