package com.singlee.ifs.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @author     ：meigy
 * @date       ：Created in 2021/11/12 18:40
 * @description：${description}
 * @modified By：
 * @version:     
 */
/**
    * 交易后-接收到期未推送
    */
public class IfsCpisRecduenot implements Serializable {
    /**
    * 市场类型
    */
    private String tradeInstrument;

    /**
    * 交易类型
    */
    private String marketIndicator;

    /**
    * 类型
    */
    private String confirmType;

    /**
    * 到期未确认笔数
    */
    private String transactionNum;

    /**
    * 报文信息
    */
    private String message;

    /**
    * 更新时间
    */
    private Date updatetime;

    private static final long serialVersionUID = 1L;

    public String getTradeInstrument() {
        return tradeInstrument;
    }

    public void setTradeInstrument(String tradeInstrument) {
        this.tradeInstrument = tradeInstrument;
    }

    public String getMarketIndicator() {
        return marketIndicator;
    }

    public void setMarketIndicator(String marketIndicator) {
        this.marketIndicator = marketIndicator;
    }

    public String getConfirmType() {
        return confirmType;
    }

    public void setConfirmType(String confirmType) {
        this.confirmType = confirmType;
    }

    public String getTransactionNum() {
        return transactionNum;
    }

    public void setTransactionNum(String transactionNum) {
        this.transactionNum = transactionNum;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Date updatetime) {
        this.updatetime = updatetime;
    }
}