package com.singlee.ifs.model;


import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 期权冲销表
 * 
 * @author rongliu
 * 
 * @date 2018-08-06
 */
@Entity
@Table(name="IFS_REV_IOTD")
public class IfsRevIotd extends IfsRevFlow{
	
    /**
     * 审批单流水号
     */
	@Id
    private String ticketId;

	/**
     * 业务类型
     */
    private String dealType;

    /**
     * 冲销日期
     */
    private String adate;

    /**
     * 复核人
     */
    private String checker;

    /**
     * 分支代码
     */
    private String br;

    /**
     * 交易前端流水号
     */
    private String fedealno;

    /**
     * 序列号
     */
    private String seq;

    /**
     * IN/OUT标识
     */
    private String inoutind;

    /**
     * 服务,定义每种交易类型标志
     */
    private String server;

    /**
     * OPICS交易流水号
     */
    private String dealNo;

    /**
     * 冲销原因
     */
    private String revreason;

    private static final long serialVersionUID = 1L;

    public String getTicketId() {
        return ticketId;
    }

    public void setTicketId(String ticketId) {
        this.ticketId = ticketId == null ? null : ticketId.trim();
    }

    public String getDealType() {
        return dealType;
    }

    public void setDealType(String dealType) {
        this.dealType = dealType == null ? null : dealType.trim();
    }

    public String getAdate() {
        return adate;
    }

    public void setAdate(String adate) {
        this.adate = adate == null ? null : adate.trim();
    }

    public String getChecker() {
        return checker;
    }

    public void setChecker(String checker) {
        this.checker = checker == null ? null : checker.trim();
    }

    public String getBr() {
        return br;
    }

    public void setBr(String br) {
        this.br = br == null ? null : br.trim();
    }

    public String getFedealno() {
        return fedealno;
    }

    public void setFedealno(String fedealno) {
        this.fedealno = fedealno == null ? null : fedealno.trim();
    }

    public String getSeq() {
        return seq;
    }

    public void setSeq(String seq) {
        this.seq = seq == null ? null : seq.trim();
    }

    public String getInoutind() {
        return inoutind;
    }

    public void setInoutind(String inoutind) {
        this.inoutind = inoutind == null ? null : inoutind.trim();
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server == null ? null : server.trim();
    }

    public String getDealno() {
        return dealNo;
    }

    public void setDealno(String dealno) {
        this.dealNo = dealno == null ? null : dealno.trim();
    }

    public String getRevreason() {
        return revreason;
    }

    public void setRevreason(String revreason) {
        this.revreason = revreason == null ? null : revreason.trim();
    }

	
}