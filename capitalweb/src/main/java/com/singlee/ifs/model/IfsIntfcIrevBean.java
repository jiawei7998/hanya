package com.singlee.ifs.model;

import java.io.Serializable;

public class IfsIntfcIrevBean implements Serializable{
	private static final long serialVersionUID = 1L;
	
	//分支机构
	private String br;
	//货币对
	private String ccyp;
	//币种中文简称
	private String ccycn;
	//币种
	private String ccy;
	//汇率中间价
	private String spotrate;
	//中间价时间
	private String filetime;
	//备注
    private String remark;
	//当前日期
	private String tradedate;
	public String getBr() {
		return br;
	}
	public void setBr(String br) {
		this.br = br;
	}
	public String getCcyp() {
		return ccyp;
	}
	public void setCcyp(String ccyp) {
		this.ccyp = ccyp;
	}
	public String getCcycn() {
		return ccycn;
	}
	public void setCcycn(String ccycn) {
		this.ccycn = ccycn;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public String getSpotrate() {
		return spotrate;
	}
	public void setSpotrate(String spotrate) {
		this.spotrate = spotrate;
	}
	public String getFiletime() {
		return filetime;
	}
	public void setFiletime(String filetime) {
		this.filetime = filetime;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getTradedate() {
		return tradedate;
	}
	public void setTradedate(String tradedate) {
		this.tradedate = tradedate;
	}
	
	

}
