package com.singlee.ifs.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class IfsIntfcFiForSale implements Serializable{
	
private static final long serialVersionUID = 1L;
	
	private String svcn;
	private String ibcd;
	private String gwam;
	private String geor;
	private String opno;
	private String opics_Ref_No;
	private String security_Id;
	private String deal_Il;
	private String value_Il;
	private String sell_Nominal_Ccy;
	private BigDecimal sell_Nominal_Amt;
	private BigDecimal sell_Price;
	private String sell_Ccy;
	private BigDecimal sell_Amt;
	private String counterparty_Id;
	private String counterparty_Nm;
	private int seq_No;
	
	private String setl_Ccy;
	
	private BigDecimal setl_Amt;
	/**
	 * @return the setl_Ccy
	 */
	public String getSetl_Ccy() {
		return setl_Ccy;
	}

	/**
	 * @param setl_Ccy the setl_Ccy to set
	 */
	public void setSetl_Ccy(String setl_Ccy) {
		this.setl_Ccy = setl_Ccy;
	}

	/**
	 * @return the setl_Amt
	 */
	public BigDecimal getSetl_Amt() {
		return setl_Amt;
	}

	/**
	 * @param setl_Amt the setl_Amt to set
	 */
	public void setSetl_Amt(BigDecimal setl_Amt) {
		this.setl_Amt = setl_Amt;
	}

	/**
	 * @return the seq_No
	 */
	public int getSeq_No() {
		return seq_No;
	}

	/**
	 * @param seq_No the seq_No to set
	 */
	public void setSeq_No(int seq_No) {
		this.seq_No = seq_No;
	}

	public IfsIntfcFiForSale(){}

	/**
	 * @return the svcn
	 */
	public String getSvcn() {
		return svcn;
	}

	/**
	 * @param svcn the svcn to set
	 */
	public void setSvcn(String svcn) {
		this.svcn = svcn;
	}

	/**
	 * @return the ibcd
	 */
	public String getIbcd() {
		return ibcd;
	}

	/**
	 * @param ibcd the ibcd to set
	 */
	public void setIbcd(String ibcd) {
		this.ibcd = ibcd;
	}

	/**
	 * @return the gwam
	 */
	public String getGwam() {
		return gwam;
	}

	/**
	 * @param gwam the gwam to set
	 */
	public void setGwam(String gwam) {
		this.gwam = gwam;
	}

	/**
	 * @return the geor
	 */
	public String getGeor() {
		return geor;
	}

	/**
	 * @param geor the geor to set
	 */
	public void setGeor(String geor) {
		this.geor = geor;
	}

	/**
	 * @return the opno
	 */
	public String getOpno() {
		return opno;
	}

	/**
	 * @param opno the opno to set
	 */
	public void setOpno(String opno) {
		this.opno = opno;
	}

	/**
	 * @return the opics_Ref_No
	 */
	public String getOpics_Ref_No() {
		return opics_Ref_No;
	}

	/**
	 * @param opics_Ref_No the opics_Ref_No to set
	 */
	public void setOpics_Ref_No(String opics_Ref_No) {
		this.opics_Ref_No = opics_Ref_No;
	}

	/**
	 * @return the security_Id
	 */
	public String getSecurity_Id() {
		return security_Id;
	}

	/**
	 * @param security_Id the security_Id to set
	 */
	public void setSecurity_Id(String security_Id) {
		this.security_Id = security_Id;
	}

	/**
	 * @return the deal_Il
	 */
	public String getDeal_Il() {
		return deal_Il;
	}

	/**
	 * @param deal_Il the deal_Il to set
	 */
	public void setDeal_Il(String deal_Il) {
		this.deal_Il = deal_Il;
	}

	/**
	 * @return the value_Il
	 */
	public String getValue_Il() {
		return value_Il;
	}

	/**
	 * @param value_Il the value_Il to set
	 */
	public void setValue_Il(String value_Il) {
		this.value_Il = value_Il;
	}

	/**
	 * @return the sell_Nominal_Ccy
	 */
	public String getSell_Nominal_Ccy() {
		return sell_Nominal_Ccy;
	}

	/**
	 * @param sell_Nominal_Ccy the sell_Nominal_Ccy to set
	 */
	public void setSell_Nominal_Ccy(String sell_Nominal_Ccy) {
		this.sell_Nominal_Ccy = sell_Nominal_Ccy;
	}

	/**
	 * @return the sell_Price
	 */
	public BigDecimal getSell_Price() {
		return sell_Price;
	}

	/**
	 * @param sell_Price the sell_Price to set
	 */
	public void setSell_Price(BigDecimal sell_Price) {
		this.sell_Price = sell_Price;
	}

	/**
	 * @return the sell_Ccy
	 */
	public String getSell_Ccy() {
		return sell_Ccy;
	}

	/**
	 * @param sell_Ccy the sell_Ccy to set
	 */
	public void setSell_Ccy(String sell_Ccy) {
		this.sell_Ccy = sell_Ccy;
	}

	/**
	 * @return the sell_Amt
	 */
	public BigDecimal getSell_Amt() {
		return sell_Amt;
	}

	/**
	 * @param sell_Amt the sell_Amt to set
	 */
	public void setSell_Amt(BigDecimal sell_Amt) {
		this.sell_Amt = sell_Amt;
	}

	/**
	 * @return the counterparty_Id
	 */
	public String getCounterparty_Id() {
		return counterparty_Id;
	}

	/**
	 * @param counterparty_Id the counterparty_Id to set
	 */
	public void setCounterparty_Id(String counterparty_Id) {
		this.counterparty_Id = counterparty_Id;
	}

	/**
	 * @return the counterparty_Nm
	 */
	public String getCounterparty_Nm() {
		return counterparty_Nm;
	}

	/**
	 * @param counterparty_Nm the counterparty_Nm to set
	 */
	public void setCounterparty_Nm(String counterparty_Nm) {
		this.counterparty_Nm = counterparty_Nm;
	}

	/**
	 * @return the sell_Nominal_Amt
	 */
	public BigDecimal getSell_Nominal_Amt() {
		return sell_Nominal_Amt;
	}

	/**
	 * @param sell_Nominal_Amt the sell_Nominal_Amt to set
	 */
	public void setSell_Nominal_Amt(BigDecimal sell_Nominal_Amt) {
		this.sell_Nominal_Amt = sell_Nominal_Amt;
	}

}
