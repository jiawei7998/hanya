package com.singlee.ifs.model;

/**
 * ClassName: FxCurreny <br/>
 * Function: 清算账户信息. <br/>
 * date: 2018年6月4日 上午11:02:17 <br/>
 * 
 * @author zhangcm
 * @version
 * @since JDK 1.6
 */
public class FxCurrenyBasebean extends FxBaseBean {

	private static final long serialVersionUID = 1L;
	/* 本方清算信息 */
	private String sellCurreny;
	// 清算货币
	private String bankName1;
	// 开户行名称1
	private String bankBic1;
	// 开户行BIC CODE1
	private String bankAccount1;
	//开户行行号1
	private String dueBank1;
	// 收款行名称1
	private String dueBankName1;
	// 收款行名称BIC CODE1
	private String dueBankAccount1;
	// 收款行账号1
	private String capitalBank1;
	// 资金开户行1
	private String capitalBankName1;
	// 资金账户户名1
	private String cnapsCode1;
	// 支付系统行号1
	private String capitalAccount1;
	// 资金账号1
	
	// 中间行名称1
	private String intermediaryBankName1;
	// 中间行BIC CODE1
	private String intermediaryBankBicCode1;
	// 中间行行号1
	private String intermediaryBankAcctNo1;
	
	/* 对手方清算信息 */
	private String buyCurreny;
	// 清算货币
	private String bankName2;
	// 开户行名称2
	private String bankBic2;
	// 开户行BIC CODE2
	private String bankAccount2;
	//开户行行号1
	
	private String dueBank2;
	// 收款行名称2
	private String dueBankName2;
	// 收款行名称BIC CODE2
	private String dueBankAccount2;
	// 收款行账号
	private String capitalBank2;
	// 资金开户行2
	private String capitalBankName2;
	// 资金账户户名2
	private String cnapsCode2;
	// 支付系统行号2
	private String capitalAccount2;
	// 资金账号2
	// 中间行名称2
	private String intermediaryBankName2;

	// 中间行BIC CODE2
	private String intermediaryBankBicCode2;

	// 中间行行号2
	private String intermediaryBankAcctNo2;

	public String getSellCurreny() {
		return sellCurreny;
	}

	public void setSellCurreny(String sellCurreny) {
		this.sellCurreny = sellCurreny;
	}

	public String getBankName1() {
		return bankName1;
	}

	public void setBankName1(String bankName1) {
		this.bankName1 = bankName1;
	}

	public String getBankBic1() {
		return bankBic1;
	}

	public void setBankBic1(String bankBic1) {
		this.bankBic1 = bankBic1;
	}

	public String getDueBank1() {
		return dueBank1;
	}

	public void setDueBank1(String dueBank1) {
		this.dueBank1 = dueBank1;
	}

	public String getDueBankName1() {
		return dueBankName1;
	}

	public void setDueBankName1(String dueBankName1) {
		this.dueBankName1 = dueBankName1;
	}

	public String getDueBankAccount1() {
		return dueBankAccount1;
	}

	public void setDueBankAccount1(String dueBankAccount1) {
		this.dueBankAccount1 = dueBankAccount1;
	}

	public String getBuyCurreny() {
		return buyCurreny;
	}

	public void setBuyCurreny(String buyCurreny) {
		this.buyCurreny = buyCurreny;
	}

	public String getCapitalBank1() {
		return capitalBank1;
	}

	public void setCapitalBank1(String capitalBank1) {
		this.capitalBank1 = capitalBank1;
	}

	public String getCapitalBankName1() {
		return capitalBankName1;
	}

	public void setCapitalBankName1(String capitalBankName1) {
		this.capitalBankName1 = capitalBankName1;
	}

	public String getCnapsCode1() {
		return cnapsCode1;
	}

	public void setCnapsCode1(String cnapsCode1) {
		this.cnapsCode1 = cnapsCode1;
	}

	public String getCapitalAccount1() {
		return capitalAccount1;
	}

	public void setCapitalAccount1(String capitalAccount1) {
		this.capitalAccount1 = capitalAccount1;
	}

	public String getBankName2() {
		return bankName2;
	}

	public void setBankName2(String bankName2) {
		this.bankName2 = bankName2;
	}

	public String getBankBic2() {
		return bankBic2;
	}

	public void setBankBic2(String bankBic2) {
		this.bankBic2 = bankBic2;
	}

	public String getDueBank2() {
		return dueBank2;
	}

	public void setDueBank2(String dueBank2) {
		this.dueBank2 = dueBank2;
	}

	public String getDueBankName2() {
		return dueBankName2;
	}

	public void setDueBankName2(String dueBankName2) {
		this.dueBankName2 = dueBankName2;
	}

	public String getDueBankAccount2() {
		return dueBankAccount2;
	}

	public void setDueBankAccount2(String dueBankAccount2) {
		this.dueBankAccount2 = dueBankAccount2;
	}

	public String getCapitalBank2() {
		return capitalBank2;
	}

	public void setCapitalBank2(String capitalBank2) {
		this.capitalBank2 = capitalBank2;
	}

	public String getCapitalBankName2() {
		return capitalBankName2;
	}

	public void setCapitalBankName2(String capitalBankName2) {
		this.capitalBankName2 = capitalBankName2;
	}

	public String getCnapsCode2() {
		return cnapsCode2;
	}

	public void setCnapsCode2(String cnapsCode2) {
		this.cnapsCode2 = cnapsCode2;
	}

	public String getCapitalAccount2() {
		return capitalAccount2;
	}

	public void setCapitalAccount2(String capitalAccount2) {
		this.capitalAccount2 = capitalAccount2;
	}

	public String getIntermediaryBankName1() {
		return intermediaryBankName1;
	}

	public void setIntermediaryBankName1(String intermediaryBankName1) {
		this.intermediaryBankName1 = intermediaryBankName1;
	}

	public String getIntermediaryBankBicCode1() {
		return intermediaryBankBicCode1;
	}

	public void setIntermediaryBankBicCode1(String intermediaryBankBicCode1) {
		this.intermediaryBankBicCode1 = intermediaryBankBicCode1;
	}

	public String getIntermediaryBankAcctNo1() {
		return intermediaryBankAcctNo1;
	}

	public void setIntermediaryBankAcctNo1(String intermediaryBankAcctNo1) {
		this.intermediaryBankAcctNo1 = intermediaryBankAcctNo1;
	}

	public String getIntermediaryBankName2() {
		return intermediaryBankName2;
	}

	public void setIntermediaryBankName2(String intermediaryBankName2) {
		this.intermediaryBankName2 = intermediaryBankName2;
	}

	public String getIntermediaryBankBicCode2() {
		return intermediaryBankBicCode2;
	}

	public void setIntermediaryBankBicCode2(String intermediaryBankBicCode2) {
		this.intermediaryBankBicCode2 = intermediaryBankBicCode2;
	}

	public String getIntermediaryBankAcctNo2() {
		return intermediaryBankAcctNo2;
	}

	public void setIntermediaryBankAcctNo2(String intermediaryBankAcctNo2) {
		this.intermediaryBankAcctNo2 = intermediaryBankAcctNo2;
	}

	public String getBankAccount1() {
		return bankAccount1;
	}

	public void setBankAccount1(String bankAccount1) {
		this.bankAccount1 = bankAccount1;
	}

	public String getBankAccount2() {
		return bankAccount2;
	}

	public void setBankAccount2(String bankAccount2) {
		this.bankAccount2 = bankAccount2;
	}

}
