package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Table(name = "IFS_APPROVERMB_SLA")
public class IfsApprovermbSl extends IfsApproveBaseBean implements Serializable {
    private String forDate;

    private String borrowInst;

    private String borrowTrader;

    private String borrowTel;

    private String borrowFax;

    private String borrowCorp;

    private String borrowAddr;

    private String lendInst;

    private String lendTrader;

    private String lendTel;

    private String lendFax;

    private String lendCorp;

    private String lendAddr;

    private String underlyingSecurityId;

    private String underlyingSymbol;

    private String tenor;

    private BigDecimal underlyingQty;

    private BigDecimal price;

    private BigDecimal miscFeeType;

    private String solution;

    private String firstSettlementMethod;

    private String secondSettlementMethod;

    private String occupancyDays;

    private String firstSettlementDate;

    private String secondSettlementDate;

    private String tradingProduct;

    private String interestPaymentDate;

    private BigDecimal interestPaymentAmounts;

    private BigDecimal couponRate;

    private String marginSecuritiesId;

    private String marginSymbol;

    private BigDecimal marginAmt;

    private String marginReplacement;

    private BigDecimal marginTotalAmt;

    private String borrowAccname;

    private String borrowOpenBank;

    private String borrowAccnum;

    private String borrowPsnum;

    private String borrowCaname;

    private String borrowCustname;

    private String borrowCanum;

    private String lendAccname;

    private String lendOpenBank;

    private String lendAccnum;

    private String lendPsnum;

    private String lendCaname;

    private String lendCustname;

    private String lendCanum;
    
    private List<IfsCfetsrmbDetailSl> bondDetailsList;
    
    private String investType;
    
    //权重
    private String weight;
    
    //占用授信主体
  	private String custNo;
  	//审批单号
  	private String applyNo;
  	//产品名称
  	private String applyProd;
  //DV01Risk
	private String DV01Risk;
	
	//止损
	private String lossLimit;
	
	//久期限
	private String longLimit;
	
	//风险程度
	private String riskDegree;
	
	private String basis;
	
	private String bondCost;
  	
  	private String bondProduct;
  	
  	private String bondProdType;
  	
  	private String bondPort;
  	
  	@Transient
  	private String  prdNo;
  	
  	private BigDecimal cereitBamt;
  	 
   	private String ispayint;
  	private String iscanum;
  	private String secname;
  	private String myDir;
  	private String oppoDir;

  	
  	
    private static final long serialVersionUID = 1L;

    

    public String getSecname() {
		return secname;
	}

	public void setSecname(String secname) {
		this.secname = secname;
	}

	public String getIspayint() {
		return ispayint;
	}

	public void setIspayint(String ispayint) {
		this.ispayint = ispayint;
	}

	public String getIscanum() {
		return iscanum;
	}

	public void setIscanum(String iscanum) {
		this.iscanum = iscanum;
	}

	public String getPrdNo() {
		return prdNo;
	}

	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}

	public List<IfsCfetsrmbDetailSl> getBondDetailsList() {
		return bondDetailsList;
	}

	public void setBondDetailsList(List<IfsCfetsrmbDetailSl> bondDetailsList) {
		this.bondDetailsList = bondDetailsList;
	}

	public String getBorrowInst() {
        return borrowInst;
    }

    public void setBorrowInst(String borrowInst) {
        this.borrowInst = borrowInst == null ? null : borrowInst.trim();
    }

    public String getBorrowTrader() {
        return borrowTrader;
    }

    public void setBorrowTrader(String borrowTrader) {
        this.borrowTrader = borrowTrader == null ? null : borrowTrader.trim();
    }

    public String getBorrowTel() {
        return borrowTel;
    }

    public void setBorrowTel(String borrowTel) {
        this.borrowTel = borrowTel == null ? null : borrowTel.trim();
    }

    public String getBorrowFax() {
        return borrowFax;
    }

    public void setBorrowFax(String borrowFax) {
        this.borrowFax = borrowFax == null ? null : borrowFax.trim();
    }

    public String getBorrowCorp() {
        return borrowCorp;
    }

    public void setBorrowCorp(String borrowCorp) {
        this.borrowCorp = borrowCorp == null ? null : borrowCorp.trim();
    }

    public String getBorrowAddr() {
        return borrowAddr;
    }

    public void setBorrowAddr(String borrowAddr) {
        this.borrowAddr = borrowAddr == null ? null : borrowAddr.trim();
    }

    public String getLendInst() {
        return lendInst;
    }

    public void setLendInst(String lendInst) {
        this.lendInst = lendInst == null ? null : lendInst.trim();
    }

    public String getLendTrader() {
        return lendTrader;
    }

    public void setLendTrader(String lendTrader) {
        this.lendTrader = lendTrader == null ? null : lendTrader.trim();
    }

    public String getLendTel() {
        return lendTel;
    }

    public void setLendTel(String lendTel) {
        this.lendTel = lendTel == null ? null : lendTel.trim();
    }

    public String getLendFax() {
        return lendFax;
    }

    public void setLendFax(String lendFax) {
        this.lendFax = lendFax == null ? null : lendFax.trim();
    }

    public String getLendCorp() {
        return lendCorp;
    }

    public void setLendCorp(String lendCorp) {
        this.lendCorp = lendCorp == null ? null : lendCorp.trim();
    }

    public String getLendAddr() {
        return lendAddr;
    }

    public void setLendAddr(String lendAddr) {
        this.lendAddr = lendAddr == null ? null : lendAddr.trim();
    }

    public String getUnderlyingSecurityId() {
        return underlyingSecurityId;
    }

    public void setUnderlyingSecurityId(String underlyingSecurityId) {
        this.underlyingSecurityId = underlyingSecurityId == null ? null : underlyingSecurityId.trim();
    }

    public String getUnderlyingSymbol() {
        return underlyingSymbol;
    }

    public void setUnderlyingSymbol(String underlyingSymbol) {
        this.underlyingSymbol = underlyingSymbol == null ? null : underlyingSymbol.trim();
    }

    public BigDecimal getUnderlyingQty() {
        return underlyingQty;
    }

    public void setUnderlyingQty(BigDecimal underlyingQty) {
        this.underlyingQty = underlyingQty;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getMiscFeeType() {
        return miscFeeType;
    }

    public void setMiscFeeType(BigDecimal miscFeeType) {
        this.miscFeeType = miscFeeType;
    }

    public String getSolution() {
        return solution;
    }

    public void setSolution(String solution) {
        this.solution = solution == null ? null : solution.trim();
    }

    public String getFirstSettlementMethod() {
        return firstSettlementMethod;
    }

    public void setFirstSettlementMethod(String firstSettlementMethod) {
        this.firstSettlementMethod = firstSettlementMethod == null ? null : firstSettlementMethod.trim();
    }

    public String getSecondSettlementMethod() {
        return secondSettlementMethod;
    }

    public void setSecondSettlementMethod(String secondSettlementMethod) {
        this.secondSettlementMethod = secondSettlementMethod == null ? null : secondSettlementMethod.trim();
    }

    public String getOccupancyDays() {
        return occupancyDays;
    }

    public void setOccupancyDays(String occupancyDays) {
        this.occupancyDays = occupancyDays == null ? null : occupancyDays.trim();
    }

    public String getFirstSettlementDate() {
        return firstSettlementDate;
    }

    public void setFirstSettlementDate(String firstSettlementDate) {
        this.firstSettlementDate = firstSettlementDate == null ? null : firstSettlementDate.trim();
    }

    public String getSecondSettlementDate() {
        return secondSettlementDate;
    }

    public void setSecondSettlementDate(String secondSettlementDate) {
        this.secondSettlementDate = secondSettlementDate == null ? null : secondSettlementDate.trim();
    }

    public String getTradingProduct() {
        return tradingProduct;
    }

    public void setTradingProduct(String tradingProduct) {
        this.tradingProduct = tradingProduct == null ? null : tradingProduct.trim();
    }

    public String getInterestPaymentDate() {
        return interestPaymentDate;
    }

    public void setInterestPaymentDate(String interestPaymentDate) {
        this.interestPaymentDate = interestPaymentDate == null ? null : interestPaymentDate.trim();
    }

    public BigDecimal getInterestPaymentAmounts() {
        return interestPaymentAmounts;
    }

    public void setInterestPaymentAmounts(BigDecimal interestPaymentAmounts) {
        this.interestPaymentAmounts = interestPaymentAmounts;
    }

    public BigDecimal getCouponRate() {
        return couponRate;
    }

    public void setCouponRate(BigDecimal couponRate) {
        this.couponRate = couponRate;
    }

    public String getMarginSecuritiesId() {
        return marginSecuritiesId;
    }

    public void setMarginSecuritiesId(String marginSecuritiesId) {
        this.marginSecuritiesId = marginSecuritiesId == null ? null : marginSecuritiesId.trim();
    }

    public String getMarginSymbol() {
        return marginSymbol;
    }

    public void setMarginSymbol(String marginSymbol) {
        this.marginSymbol = marginSymbol == null ? null : marginSymbol.trim();
    }

    public BigDecimal getMarginAmt() {
        return marginAmt;
    }

    public void setMarginAmt(BigDecimal marginAmt) {
        this.marginAmt = marginAmt;
    }

    public String getMarginReplacement() {
        return marginReplacement;
    }

    public void setMarginReplacement(String marginReplacement) {
        this.marginReplacement = marginReplacement == null ? null : marginReplacement.trim();
    }

    public BigDecimal getMarginTotalAmt() {
        return marginTotalAmt;
    }

    public void setMarginTotalAmt(BigDecimal marginTotalAmt) {
        this.marginTotalAmt = marginTotalAmt;
    }

    public String getBorrowAccname() {
        return borrowAccname;
    }

    public void setBorrowAccname(String borrowAccname) {
        this.borrowAccname = borrowAccname == null ? null : borrowAccname.trim();
    }

    public String getBorrowOpenBank() {
        return borrowOpenBank;
    }

    public void setBorrowOpenBank(String borrowOpenBank) {
        this.borrowOpenBank = borrowOpenBank == null ? null : borrowOpenBank.trim();
    }

    public String getBorrowAccnum() {
        return borrowAccnum;
    }

    public void setBorrowAccnum(String borrowAccnum) {
        this.borrowAccnum = borrowAccnum == null ? null : borrowAccnum.trim();
    }

    public String getBorrowPsnum() {
        return borrowPsnum;
    }

    public void setBorrowPsnum(String borrowPsnum) {
        this.borrowPsnum = borrowPsnum == null ? null : borrowPsnum.trim();
    }

    public String getBorrowCaname() {
        return borrowCaname;
    }

    public void setBorrowCaname(String borrowCaname) {
        this.borrowCaname = borrowCaname == null ? null : borrowCaname.trim();
    }

    public String getBorrowCustname() {
        return borrowCustname;
    }

    public void setBorrowCustname(String borrowCustname) {
        this.borrowCustname = borrowCustname == null ? null : borrowCustname.trim();
    }

    public String getBorrowCanum() {
        return borrowCanum;
    }

    public void setBorrowCanum(String borrowCanum) {
        this.borrowCanum = borrowCanum == null ? null : borrowCanum.trim();
    }

    public String getLendAccname() {
        return lendAccname;
    }

    public void setLendAccname(String lendAccname) {
        this.lendAccname = lendAccname == null ? null : lendAccname.trim();
    }

    public String getLendOpenBank() {
        return lendOpenBank;
    }

    public void setLendOpenBank(String lendOpenBank) {
        this.lendOpenBank = lendOpenBank == null ? null : lendOpenBank.trim();
    }

    public String getLendAccnum() {
        return lendAccnum;
    }

    public void setLendAccnum(String lendAccnum) {
        this.lendAccnum = lendAccnum == null ? null : lendAccnum.trim();
    }

    public String getLendPsnum() {
        return lendPsnum;
    }

    public void setLendPsnum(String lendPsnum) {
        this.lendPsnum = lendPsnum == null ? null : lendPsnum.trim();
    }

    public String getLendCaname() {
        return lendCaname;
    }

    public void setLendCaname(String lendCaname) {
        this.lendCaname = lendCaname == null ? null : lendCaname.trim();
    }

    public String getLendCustname() {
        return lendCustname;
    }

    public void setLendCustname(String lendCustname) {
        this.lendCustname = lendCustname == null ? null : lendCustname.trim();
    }

    public String getLendCanum() {
        return lendCanum;
    }

    public void setLendCanum(String lendCanum) {
        this.lendCanum = lendCanum == null ? null : lendCanum.trim();
    }

	public String getForDate() {
		return forDate;
	}

	public void setForDate(String forDate) {
		this.forDate = forDate;
	}

	public String getTenor() {
		return tenor;
	}

	public void setTenor(String tenor) {
		this.tenor = tenor;
	}


	public void setInvestType(String investType) {
		this.investType = investType;
	}

	public String getInvestType() {
		return investType;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	@Override
    public String getCustNo() {
		return custNo;
	}

	@Override
    public void setCustNo(String custNo) {
		this.custNo = custNo;
	}

	public String getApplyNo() {
		return applyNo;
	}

	public void setApplyNo(String applyNo) {
		this.applyNo = applyNo;
	}

	public String getApplyProd() {
		return applyProd;
	}

	public void setApplyProd(String applyProd) {
		this.applyProd = applyProd;
	}
	
	public String getDV01Risk() {
		return DV01Risk;
	}

	public void setDV01Risk(String dV01Risk) {
		this.DV01Risk = dV01Risk;
	}

	public String getLossLimit() {
		return lossLimit;
	}

	public void setLossLimit(String lossLimit) {
		this.lossLimit = lossLimit;
	}

	public String getLongLimit() {
		return longLimit;
	}

	public void setLongLimit(String longLimit) {
		this.longLimit = longLimit;
	}

	public String getRiskDegree() {
		return riskDegree;
	}

	public void setRiskDegree(String riskDegree) {
		this.riskDegree = riskDegree;
	}

	public String getBasis() {
		return basis;
	}

	public void setBasis(String basis) {
		this.basis = basis;
	}

	public String getBondCost() {
		return bondCost;
	}

	public void setBondCost(String bondCost) {
		this.bondCost = bondCost;
	}

	public String getBondProduct() {
		return bondProduct;
	}

	public void setBondProduct(String bondProduct) {
		this.bondProduct = bondProduct;
	}

	public String getBondProdType() {
		return bondProdType;
	}

	public void setBondProdType(String bondProdType) {
		this.bondProdType = bondProdType;
	}

	public String getBondPort() {
		return bondPort;
	}

	public void setBondPort(String bondPort) {
		this.bondPort = bondPort;
	}

    public BigDecimal getCereitBamt() {
        return cereitBamt;
    }

    public void setCereitBamt(BigDecimal cereitBamt) {
        this.cereitBamt = cereitBamt;
    }

    @Override
    public String getMyDir() {
        return myDir;
    }

    @Override
    public void setMyDir(String myDir) {
        this.myDir = myDir;
    }

    @Override
    public String getOppoDir() {
        return oppoDir;
    }

    @Override
    public void setOppoDir(String oppoDir) {
        this.oppoDir = oppoDir;
    }
}