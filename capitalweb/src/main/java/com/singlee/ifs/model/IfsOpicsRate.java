package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
/**
 * OPICS对应的利率代码
 * 
 * @author lij
 * 
 * @date 2018-08-02
 */
@Entity
@Table(name="IFS_OPICS_RATE")
public class IfsOpicsRate implements Serializable {
    /**
     * 机构分支
     */
    private String br;

    /**
     * 利率代码
     */
    private String ratecode;

    /**
     * 币种
     */
    private String ccy;

    /**
     * 类型
     */
    private String ratetype;

    /**
     * 
     */
    private String varfix;

    /**
     * 来源
     */
    private String source;

    /**
     * 
     */
    private String desmat;

    /**
     * 
     */
    private BigDecimal spread8;

    /**
     * 计息基准
     */
    private String basis;

    /**
     * 英文描述
     */
    private String descr;

    /**
     * 中文描述
     */
    private String ratecodecn;

    /**
     * 
     */
    private String ratefixday;

    /**
     * 最后维护时间
     */
    private Date lstmntdate;

    /**
     * 
     */
    private String fincal;

    /**
     * 
     */
    private String ratesourceDde;

    /**
     * 
     */
    private String ratesourceIsda;

    /**
     * 操作者
     */
    private String operator;

    /**
     * 同步状态
     */
    private String status;

    /**
     * 备注
     */
    private String remark;

    private static final long serialVersionUID = 1L;

    public String getBr() {
        return br;
    }

    public void setBr(String br) {
        this.br = br == null ? null : br.trim();
    }

    public String getRatecode() {
        return ratecode;
    }

    public void setRatecode(String ratecode) {
        this.ratecode = ratecode == null ? null : ratecode.trim();
    }

    public String getCcy() {
        return ccy;
    }

    public void setCcy(String ccy) {
        this.ccy = ccy == null ? null : ccy.trim();
    }

    public String getRatetype() {
        return ratetype;
    }

    public void setRatetype(String ratetype) {
        this.ratetype = ratetype == null ? null : ratetype.trim();
    }

    public String getVarfix() {
        return varfix;
    }

    public void setVarfix(String varfix) {
        this.varfix = varfix == null ? null : varfix.trim();
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source == null ? null : source.trim();
    }

    public String getDesmat() {
        return desmat;
    }

    public void setDesmat(String desmat) {
        this.desmat = desmat == null ? null : desmat.trim();
    }

    public BigDecimal getSpread8() {
        return spread8;
    }

    public void setSpread8(BigDecimal spread8) {
        this.spread8 = spread8;
    }

    public String getBasis() {
        return basis;
    }

    public void setBasis(String basis) {
        this.basis = basis == null ? null : basis.trim();
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr == null ? null : descr.trim();
    }

    public String getRatecodecn() {
        return ratecodecn;
    }

    public void setRatecodecn(String ratecodecn) {
        this.ratecodecn = ratecodecn == null ? null : ratecodecn.trim();
    }

    public String getRatefixday() {
        return ratefixday;
    }

    public void setRatefixday(String ratefixday) {
        this.ratefixday = ratefixday == null ? null : ratefixday.trim();
    }

    public Date getLstmntdate() {
        return lstmntdate;
    }

    public void setLstmntdate(Date lstmntdate) {
        this.lstmntdate = lstmntdate;
    }

    public String getFincal() {
        return fincal;
    }

    public void setFincal(String fincal) {
        this.fincal = fincal == null ? null : fincal.trim();
    }

    public String getRatesourceDde() {
        return ratesourceDde;
    }

    public void setRatesourceDde(String ratesourceDde) {
        this.ratesourceDde = ratesourceDde == null ? null : ratesourceDde.trim();
    }

    public String getRatesourceIsda() {
        return ratesourceIsda;
    }

    public void setRatesourceIsda(String ratesourceIsda) {
        this.ratesourceIsda = ratesourceIsda == null ? null : ratesourceIsda.trim();
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator == null ? null : operator.trim();
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }
}