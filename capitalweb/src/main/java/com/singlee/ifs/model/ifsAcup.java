package com.singlee.ifs.model;

import java.io.Serializable;

public class ifsAcup implements Serializable {
	
	/**
	 * 总账对象
	 */
	private static final long serialVersionUID = 1L;
	private String setno     ;
	private String seqno     ;
	private String br        ;
	private String product   ;
	private String type      ;
	private String argno     ;
	private String dealno    ;
	private String seq       ;
	private String glno      ;
	private String costcent  ;
	private String ccy       ;
	private String ccycode   ;
	private String beind     ;
	private String code      ;
	private String qual      ;
	private String effdate   ;
	private String postdate  ;
	private String cmne      ;
	private String drcrind   ;
	private String amount    ;
	private String smeans    ;
	private String sacct     ;
	private String descr     ;
	private String intglno   ;
	private String subject   ;
	private String subbr     ;
	private String reversal  ;
	private String account   ;
	private String voucher   ;
	private String remark    ;
	private String errcode   ;
	private String errmsg    ;
	private String oper      ;
	private String note      ;
	private String retcode   ;
	private String retmsg    ;
	private String inputtime ;
	public String getSetno() {
		return setno;
	}
	public void setSetno(String setno) {
		this.setno = setno;
	}
	public String getSeqno() {
		return seqno;
	}
	public void setSeqno(String seqno) {
		this.seqno = seqno;
	}
	public String getBr() {
		return br;
	}
	public void setBr(String br) {
		this.br = br;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getArgno() {
		return argno;
	}
	public void setArgno(String argno) {
		this.argno = argno;
	}
	public String getDealno() {
		return dealno;
	}
	public void setDealno(String dealno) {
		this.dealno = dealno;
	}
	public String getSeq() {
		return seq;
	}
	public void setSeq(String seq) {
		this.seq = seq;
	}
	public String getGlno() {
		return glno;
	}
	public void setGlno(String glno) {
		this.glno = glno;
	}
	public String getCostcent() {
		return costcent;
	}
	public void setCostcent(String costcent) {
		this.costcent = costcent;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public String getCcycode() {
		return ccycode;
	}
	public void setCcycode(String ccycode) {
		this.ccycode = ccycode;
	}
	public String getBeind() {
		return beind;
	}
	public void setBeind(String beind) {
		this.beind = beind;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getQual() {
		return qual;
	}
	public void setQual(String qual) {
		this.qual = qual;
	}
	public String getEffdate() {
		return effdate;
	}
	public void setEffdate(String effdate) {
		this.effdate = effdate;
	}
	public String getPostdate() {
		return postdate;
	}
	public void setPostdate(String postdate) {
		this.postdate = postdate;
	}
	public String getCmne() {
		return cmne;
	}
	public void setCmne(String cmne) {
		this.cmne = cmne;
	}
	public String getDrcrind() {
		return drcrind;
	}
	public void setDrcrind(String drcrind) {
		this.drcrind = drcrind;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getSmeans() {
		return smeans;
	}
	public void setSmeans(String smeans) {
		this.smeans = smeans;
	}
	public String getSacct() {
		return sacct;
	}
	public void setSacct(String sacct) {
		this.sacct = sacct;
	}
	public String getDescr() {
		return descr;
	}
	public void setDescr(String descr) {
		this.descr = descr;
	}
	public String getIntglno() {
		return intglno;
	}
	public void setIntglno(String intglno) {
		this.intglno = intglno;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getSubbr() {
		return subbr;
	}
	public void setSubbr(String subbr) {
		this.subbr = subbr;
	}
	public String getReversal() {
		return reversal;
	}
	public void setReversal(String reversal) {
		this.reversal = reversal;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getVoucher() {
		return voucher;
	}
	public void setVoucher(String voucher) {
		this.voucher = voucher;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getErrcode() {
		return errcode;
	}
	public void setErrcode(String errcode) {
		this.errcode = errcode;
	}
	public String getErrmsg() {
		return errmsg;
	}
	public void setErrmsg(String errmsg) {
		this.errmsg = errmsg;
	}
	public String getOper() {
		return oper;
	}
	public void setOper(String oper) {
		this.oper = oper;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getRetcode() {
		return retcode;
	}
	public void setRetcode(String retcode) {
		this.retcode = retcode;
	}
	public String getRetmsg() {
		return retmsg;
	}
	public void setRetmsg(String retmsg) {
		this.retmsg = retmsg;
	}
	public String getInputtime() {
		return inputtime;
	}
	public void setInputtime(String inputtime) {
		this.inputtime = inputtime;
	}
	
	
}
