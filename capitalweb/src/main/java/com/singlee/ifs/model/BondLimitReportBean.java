package com.singlee.ifs.model;

import java.io.Serializable;

public class BondLimitReportBean implements Serializable{

	/**
	 * 统计拆借业务交易的报表字段
	 */
	private static final long serialVersionUID = -2827017115423221218L;
	
	//债券限额
	private String zjxe11;
	private String zjxe12;
	private String zjxe13;
	private String zjxe14;
	private String zjxe15;
	private String zjxe16;
	private String zjxe17;
	private String zjxe21;
	private String zjxe22;
	private String zjxe23;
	private String zjxe24;
	private String zjxe25;
	private String zjxe26;
	private String zjxe27;
	private String zjxe31;
	private String zjxe32;
	private String zjxe33;
	private String zjxe34;
	
	//存单限额
	private String cdxe11;
	private String cdxe12;
	private String cdxe13;
	private String cdxe14;
	private String cdxe15;
	private String cdxe16;
	private String cdxe17;
	private String cdxe21;
	private String cdxe22;
	private String cdxe23;
	private String cdxe24;
	private String cdxe25;
	private String cdxe26;
	private String cdxe27;
	private String cdxe31;
	private String cdxe32;
	private String cdxe33;
	private String cdxe34;
	public String getZjxe11() {
		return zjxe11;
	}
	public void setZjxe11(String zjxe11) {
		this.zjxe11 = zjxe11;
	}
	public String getZjxe12() {
		return zjxe12;
	}
	public void setZjxe12(String zjxe12) {
		this.zjxe12 = zjxe12;
	}
	public String getZjxe13() {
		return zjxe13;
	}
	public void setZjxe13(String zjxe13) {
		this.zjxe13 = zjxe13;
	}
	public String getZjxe14() {
		return zjxe14;
	}
	public void setZjxe14(String zjxe14) {
		this.zjxe14 = zjxe14;
	}
	public String getZjxe15() {
		return zjxe15;
	}
	public void setZjxe15(String zjxe15) {
		this.zjxe15 = zjxe15;
	}
	public String getZjxe16() {
		return zjxe16;
	}
	public void setZjxe16(String zjxe16) {
		this.zjxe16 = zjxe16;
	}
	public String getZjxe17() {
		return zjxe17;
	}
	public void setZjxe17(String zjxe17) {
		this.zjxe17 = zjxe17;
	}
	public String getZjxe21() {
		return zjxe21;
	}
	public void setZjxe21(String zjxe21) {
		this.zjxe21 = zjxe21;
	}
	public String getZjxe22() {
		return zjxe22;
	}
	public void setZjxe22(String zjxe22) {
		this.zjxe22 = zjxe22;
	}
	public String getZjxe23() {
		return zjxe23;
	}
	public void setZjxe23(String zjxe23) {
		this.zjxe23 = zjxe23;
	}
	public String getZjxe24() {
		return zjxe24;
	}
	public void setZjxe24(String zjxe24) {
		this.zjxe24 = zjxe24;
	}
	public String getZjxe25() {
		return zjxe25;
	}
	public void setZjxe25(String zjxe25) {
		this.zjxe25 = zjxe25;
	}
	public String getZjxe26() {
		return zjxe26;
	}
	public void setZjxe26(String zjxe26) {
		this.zjxe26 = zjxe26;
	}
	public String getZjxe27() {
		return zjxe27;
	}
	public void setZjxe27(String zjxe27) {
		this.zjxe27 = zjxe27;
	}
	public String getZjxe31() {
		return zjxe31;
	}
	public void setZjxe31(String zjxe31) {
		this.zjxe31 = zjxe31;
	}
	public String getZjxe32() {
		return zjxe32;
	}
	public void setZjxe32(String zjxe32) {
		this.zjxe32 = zjxe32;
	}
	public String getZjxe33() {
		return zjxe33;
	}
	public void setZjxe33(String zjxe33) {
		this.zjxe33 = zjxe33;
	}
	public String getZjxe34() {
		return zjxe34;
	}
	public void setZjxe34(String zjxe34) {
		this.zjxe34 = zjxe34;
	}
	public String getCdxe11() {
		return cdxe11;
	}
	public void setCdxe11(String cdxe11) {
		this.cdxe11 = cdxe11;
	}
	public String getCdxe12() {
		return cdxe12;
	}
	public void setCdxe12(String cdxe12) {
		this.cdxe12 = cdxe12;
	}
	public String getCdxe13() {
		return cdxe13;
	}
	public void setCdxe13(String cdxe13) {
		this.cdxe13 = cdxe13;
	}
	public String getCdxe14() {
		return cdxe14;
	}
	public void setCdxe14(String cdxe14) {
		this.cdxe14 = cdxe14;
	}
	public String getCdxe15() {
		return cdxe15;
	}
	public void setCdxe15(String cdxe15) {
		this.cdxe15 = cdxe15;
	}
	public String getCdxe16() {
		return cdxe16;
	}
	public void setCdxe16(String cdxe16) {
		this.cdxe16 = cdxe16;
	}
	public String getCdxe17() {
		return cdxe17;
	}
	public void setCdxe17(String cdxe17) {
		this.cdxe17 = cdxe17;
	}
	public String getCdxe21() {
		return cdxe21;
	}
	public void setCdxe21(String cdxe21) {
		this.cdxe21 = cdxe21;
	}
	public String getCdxe22() {
		return cdxe22;
	}
	public void setCdxe22(String cdxe22) {
		this.cdxe22 = cdxe22;
	}
	public String getCdxe23() {
		return cdxe23;
	}
	public void setCdxe23(String cdxe23) {
		this.cdxe23 = cdxe23;
	}
	public String getCdxe24() {
		return cdxe24;
	}
	public void setCdxe24(String cdxe24) {
		this.cdxe24 = cdxe24;
	}
	public String getCdxe25() {
		return cdxe25;
	}
	public void setCdxe25(String cdxe25) {
		this.cdxe25 = cdxe25;
	}
	public String getCdxe26() {
		return cdxe26;
	}
	public void setCdxe26(String cdxe26) {
		this.cdxe26 = cdxe26;
	}
	public String getCdxe27() {
		return cdxe27;
	}
	public void setCdxe27(String cdxe27) {
		this.cdxe27 = cdxe27;
	}
	public String getCdxe31() {
		return cdxe31;
	}
	public void setCdxe31(String cdxe31) {
		this.cdxe31 = cdxe31;
	}
	public String getCdxe32() {
		return cdxe32;
	}
	public void setCdxe32(String cdxe32) {
		this.cdxe32 = cdxe32;
	}
	public String getCdxe33() {
		return cdxe33;
	}
	public void setCdxe33(String cdxe33) {
		this.cdxe33 = cdxe33;
	}
	public String getCdxe34() {
		return cdxe34;
	}
	public void setCdxe34(String cdxe34) {
		this.cdxe34 = cdxe34;
	}


	
}
