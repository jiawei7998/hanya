package com.singlee.ifs.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 外汇头寸估值
 */
public class IfsNbhGatherBean implements Serializable {
	
	private static final long serialVersionUID = 3678854053681480582L;

	/**
	 * 部门
	 */
	private String br;

	/**
	 * 
	 */
	private String server;

	/**
	 * 流水号
	 */
	private String fedealno;

	/**
	 * 
	 */
	private String seq;

	/**
	 * 
	 */
	private String inoutind;

	/**
	 * 交易员
	 */
	private String trad;

	/**
	 * 
	 */
	private Date vdate;

	/**
	 * 交易对手
	 */
	private String cust;

	/**
	 * 
	 */
	private String brok;

	/**
	 * 投资组合
	 */
	private String port;

	/**
	 * 成本中心
	 */
	private String cost;

	/**
	 * 交易日期
	 */
	private Date dealdate;

	/**
	 * 交易来源
	 */
	private String dealtext;

	/**
	 * 
	 */
	private String ps;

	/**
	 * 外币币种
	 */
	private String ccy;

	/**
	 * 外币金额
	 */
	private BigDecimal ccyamt;

	/**
	 * 本币币种
	 */
	private String ctrccy;

	/**
	 * 本币金额
	 */
	private BigDecimal ctramt;

	/**
	 * 
	 */
	private String siind;

	/**
	 * 
	 */
	private String verind;

	/**
	 * 
	 */
	private String suppayind;

	/**
	 * 
	 */
	private String suprecind;

	/**
	 * 
	 */
	private String supconfind;

	/**
	 * 产品代码
	 */
	private String product;

	/**
	 * 产品类型
	 */
	private String prodtype;

	/**
	 * 
	 */
	private String authsi;

	/**
	 * OPICS处理状态
	 */
	private String localstatus;
	
	/**
	 * OPICS编号
	 */
	private String dealno;

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getServer() {
		return server;
	}

	public void setServer(String server) {
		this.server = server;
	}

	public String getFedealno() {
		return fedealno;
	}

	public void setFedealno(String fedealno) {
		this.fedealno = fedealno;
	}

	public String getSeq() {
		return seq;
	}

	public void setSeq(String seq) {
		this.seq = seq;
	}

	public String getInoutind() {
		return inoutind;
	}

	public void setInoutind(String inoutind) {
		this.inoutind = inoutind;
	}

	public String getTrad() {
		return trad;
	}

	public void setTrad(String trad) {
		this.trad = trad;
	}

	public Date getVdate() {
		return vdate;
	}

	public void setVdate(Date vdate) {
		this.vdate = vdate;
	}

	public String getCust() {
		return cust;
	}

	public void setCust(String cust) {
		this.cust = cust;
	}

	public String getBrok() {
		return brok;
	}

	public void setBrok(String brok) {
		this.brok = brok;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getCost() {
		return cost;
	}

	public void setCost(String cost) {
		this.cost = cost;
	}

	public Date getDealdate() {
		return dealdate;
	}

	public void setDealdate(Date dealdate) {
		this.dealdate = dealdate;
	}

	public String getDealtext() {
		return dealtext;
	}

	public void setDealtext(String dealtext) {
		this.dealtext = dealtext;
	}

	public String getPs() {
		return ps;
	}

	public void setPs(String ps) {
		this.ps = ps;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public BigDecimal getCcyamt() {
		return ccyamt;
	}

	public void setCcyamt(BigDecimal ccyamt) {
		this.ccyamt = ccyamt;
	}

	public String getCtrccy() {
		return ctrccy;
	}

	public void setCtrccy(String ctrccy) {
		this.ctrccy = ctrccy;
	}

	public BigDecimal getCtramt() {
		return ctramt;
	}

	public void setCtramt(BigDecimal ctramt) {
		this.ctramt = ctramt;
	}

	public String getSiind() {
		return siind;
	}

	public void setSiind(String siind) {
		this.siind = siind;
	}

	public String getVerind() {
		return verind;
	}

	public void setVerind(String verind) {
		this.verind = verind;
	}

	public String getSuppayind() {
		return suppayind;
	}

	public void setSuppayind(String suppayind) {
		this.suppayind = suppayind;
	}

	public String getSuprecind() {
		return suprecind;
	}

	public void setSuprecind(String suprecind) {
		this.suprecind = suprecind;
	}

	public String getSupconfind() {
		return supconfind;
	}

	public void setSupconfind(String supconfind) {
		this.supconfind = supconfind;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getProdtype() {
		return prodtype;
	}

	public void setProdtype(String prodtype) {
		this.prodtype = prodtype;
	}

	public String getAuthsi() {
		return authsi;
	}

	public void setAuthsi(String authsi) {
		this.authsi = authsi;
	}

	public String getLocalstatus() {
		return localstatus;
	}

	public void setLocalstatus(String localstatus) {
		this.localstatus = localstatus;
	}

	public String getDealno() {
		return dealno;
	}

	public void setDealno(String dealno) {
		this.dealno = dealno;
	}
}