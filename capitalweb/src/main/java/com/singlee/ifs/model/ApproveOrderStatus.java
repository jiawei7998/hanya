package com.singlee.ifs.model;

public class ApproveOrderStatus {
	/**
	 * 审批单状态 1:就绪
	 */
	public static String Ready = "1";
	/**
	 * 审批单状态 2:计算中
	 */
	public static String Calculating = "2";
	/**
	 * 审批单状态 3:新建
	 */
	public static String New = "3";
	/**
	 * 审批单状态 4:待审批
	 */
	public static String WaitApprove = "4";
	/**
	 * 审批单状态 5:审批中
	 */
	public static String Approving = "5";
	/**
	 * 审批单状态 6:审批通过
	 */
	public static String ApprovedPass = "6";
	/**
	 * 审批单状态 7:审批拒绝
	 */
	public static String ApprovedNoPass = "7";
	/**
	 * 审批单状态 8:注销
	 */
	public static String Cancle = "8";
	/**
	 * 审批单状态 9:执行中
	 */
	public static String Executing = "9";
	/**
	 * 审批单状态 10:执行完成
	 */
	public static String Executed = "10";
}
