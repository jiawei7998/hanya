package com.singlee.ifs.model;

import java.io.Serializable;
import java.text.DecimalFormat;

public class DataWareHouse implements Serializable {
    /**
     * 主键id
     */
    protected  Integer id;

    /**
     * 版本控制version
     */
    protected  Integer version;

    /**
     * 数据日期
     */
    protected  String data_date;

    /**
     * 源系统标志
     */
    protected  String source_sys_code;

    /**
     * 业务类型
     */
    protected  String business_type;

    /**
     * 账户号
     */
    protected  String account_id;

    /**
     * 记账机构号
     */
    protected  String org_unit_code;

    /**
     * 本金科目号
     */
    protected  String account_code;

    /**
     * 资产类型
     */
    protected  String asset_type;

    /**
     * 产品代码
     */
    protected  String product_code;

    /**
     * 业务条线
     */
    protected  String business_line_code;

    /**
     * 币种
     */
    protected  String currency_code;

    /**
     * 起息日
     */
    protected  String origination_date;

    /**
     * 到期日
     */
    protected  String maturity_date;

    /**
     * 合同日期
     */
    protected  String initial_date;

    /**
     * 处置日
     */
    protected String sell_date;

    /**
     * 票面余额
     */
    protected  Double par_balance;

    /**
     * 账面余额
     */
    protected  Double cur_balance;

    /**
     * 原始账面余额
     */
    protected  Double org_balance;

    /**
     * 原始票面余额
     */
    protected  Double org_par_balance;

    /**
     * 当前利率
     */
    protected  Double cur_net_rate;

    /**
     * 利率调整方式
     */
    protected  String adjustable_type_code;

    /**
     * 上次利率调整日期
     */
    protected  String last_reprice_date;

    /**
     * 利率调整频率
     */
    protected  String reprice_freq;

    /**
     * 利率调整频率单位
     */
    protected  String reprice_freq_mult;

    /**
     * 下次利率调整日期
     */
    protected  String next_reprice_date;

    /**
     * 付款方式
     */
    protected  String payment_type;

    /**
     * 付息方式
     */
    protected  String int_type;

    /**
     * 上次付款日
     */
    protected  String last_payment_date;

    /**
     * 付款频率
     */
    protected  String paymnet_freq;

    /**
     * 付款频率乘数
     */
    protected  String payment_freq_mult;

    /**
     * 下次付款日
     */
    protected  String next_payment_date;

    /**
     * 金额变动
     */
    protected  Double payment_amount;

    /**
     * 利息收入/支出
     */
    protected  Double interest_amount;

    /**
     * 投资收益
     */
    protected  Double invest_gain_amount;

    /**
     * 公允价值变动
     */
    protected  Double mkt_value_change_amount;

    /**
     * 利息调整
     */
    protected  Double int_adjust_amount;

    /**
     * 交易对手代码
     */
    protected  String customer_code;

    /**
     * 交易量
     */
    protected  String transaction_cnt;

    /**
     * 交易目的
     */
    protected  String transaction_purpose;

    /**
     * 票面利率
     */
    protected  String par_rate;

    /**
     * 减值计提金额
     */
    protected  Double lost_amount;

    /**
     * 利息收支科目号
     */
    protected  String interest_account_code;

    /**
     * 投资收益科目号
     */
    protected  String invest_gain_account_code;

    /**
     * 公允价值变动损益科目号
     */
    protected  String mkt_value_account_code;

    /**
     * 付息频率
     */
    protected  String paymnet_int_freq;

    /**
     * 付息频率乘数
     */
    protected  String payment_int_freq_mult;

    /**
     * 冲销标识
     */
    protected  String revflag;

    /**
     * 插入时间
     */
    protected  String intime;

    /**
     * 备注1
     */
    protected  String memo1;

    /**
     * 备注2
     */
    protected  String memo2;

    /**
     * 备注3
     */
    protected  String memo3;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getData_date() {
        return data_date;
    }

    public void setData_date(String data_date) {
        this.data_date = data_date;
    }

    public String getSource_sys_code() {
        return source_sys_code;
    }

    public void setSource_sys_code(String source_sys_code) {
        this.source_sys_code = source_sys_code;
    }

    public String getBusiness_type() {
        return business_type;
    }

    public void setBusiness_type(String business_type) {
        this.business_type = business_type;
    }

    public String getAccount_id() {
        return account_id;
    }

    public void setAccount_id(String account_id) {
        this.account_id = account_id;
    }

    public String getOrg_unit_code() {
        return org_unit_code;
    }

    public void setOrg_unit_code(String org_unit_code) {
        this.org_unit_code = org_unit_code;
    }

    public String getAccount_code() {
        return account_code;
    }

    public void setAccount_code(String account_code) {
        this.account_code = account_code;
    }

    public String getAsset_type() {
        return asset_type;
    }

    public void setAsset_type(String asset_type) {
        this.asset_type = asset_type;
    }

    public String getProduct_code() {
        return product_code;
    }

    public void setProduct_code(String product_code) {
        this.product_code = product_code;
    }

    public String getBusiness_line_code() {
        return business_line_code;
    }

    public void setBusiness_line_code(String business_line_code) {
        this.business_line_code = business_line_code;
    }

    public String getCurrency_code() {
        return currency_code;
    }

    public void setCurrency_code(String currency_code) {
        this.currency_code = currency_code;
    }

    public String getOrigination_date() {
        return origination_date;
    }

    public void setOrigination_date(String origination_date) {
        this.origination_date = origination_date;
    }

    public String getMaturity_date() {
        return maturity_date;
    }

    public void setMaturity_date(String maturity_date) {
        this.maturity_date = maturity_date;
    }

    public String getInitial_date() {
        return initial_date;
    }

    public void setInitial_date(String initial_date) {
        this.initial_date = initial_date;
    }

    public String getSell_date() {
        return sell_date;
    }

    public void setSell_date(String sell_date) {
        this.sell_date = sell_date;
    }

    public Double getPar_balance() {
        return par_balance;
    }

    public void setPar_balance(Double par_balance) {
        this.par_balance = par_balance;
    }

    public Double getCur_balance() {
        return cur_balance;
    }

    public void setCur_balance(Double cur_balance) {
        this.cur_balance = cur_balance;
    }

    public Double getOrg_balance() {
        return org_balance;
    }

    public void setOrg_balance(Double org_balance) {
        this.org_balance = org_balance;
    }

    public Double getOrg_par_balance() {
        return org_par_balance;
    }

    public void setOrg_par_balance(Double org_par_balance) {
        this.org_par_balance = org_par_balance;
    }

    public Double getCur_net_rate() {
        return cur_net_rate;
    }

    public void setCur_net_rate(Double cur_net_rate) {
        this.cur_net_rate = cur_net_rate;
    }

    public String getAdjustable_type_code() {
        return adjustable_type_code;
    }

    public void setAdjustable_type_code(String adjustable_type_code) {
        this.adjustable_type_code = adjustable_type_code;
    }

    public String getLast_reprice_date() {
        return last_reprice_date;
    }

    public void setLast_reprice_date(String last_reprice_date) {
        this.last_reprice_date = last_reprice_date;
    }

    public String getReprice_freq() {
        return reprice_freq;
    }

    public void setReprice_freq(String reprice_freq) {
        this.reprice_freq = reprice_freq;
    }

    public String getReprice_freq_mult() {
        return reprice_freq_mult;
    }

    public void setReprice_freq_mult(String reprice_freq_mult) {
        this.reprice_freq_mult = reprice_freq_mult;
    }

    public String getNext_reprice_date() {
        return next_reprice_date;
    }

    public void setNext_reprice_date(String next_reprice_date) {
        this.next_reprice_date = next_reprice_date;
    }

    public String getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(String payment_type) {
        this.payment_type = payment_type;
    }

    public String getInt_type() {
        return int_type;
    }

    public void setInt_type(String int_type) {
        this.int_type = int_type;
    }

    public String getLast_payment_date() {
        return last_payment_date;
    }

    public void setLast_payment_date(String last_payment_date) {
        this.last_payment_date = last_payment_date;
    }

    public String getPaymnet_freq() {
        return paymnet_freq;
    }

    public void setPaymnet_freq(String paymnet_freq) {
        this.paymnet_freq = paymnet_freq;
    }

    public String getPayment_freq_mult() {
        return payment_freq_mult;
    }

    public void setPayment_freq_mult(String payment_freq_mult) {
        this.payment_freq_mult = payment_freq_mult;
    }

    public String getNext_payment_date() {
        return next_payment_date;
    }

    public void setNext_payment_date(String next_payment_date) {
        this.next_payment_date = next_payment_date;
    }

    public Double getPayment_amount() {
        return payment_amount;
    }

    public void setPayment_amount(Double payment_amount) {
        this.payment_amount = payment_amount;
    }

    public Double getInterest_amount() {
        return interest_amount;
    }

    public void setInterest_amount(Double interest_amount) {
        this.interest_amount = interest_amount;
    }

    public Double getInvest_gain_amount() {
        return invest_gain_amount;
    }

    public void setInvest_gain_amount(Double invest_gain_amount) {
        this.invest_gain_amount = invest_gain_amount;
    }

    public Double getMkt_value_change_amount() {
        return mkt_value_change_amount;
    }

    public void setMkt_value_change_amount(Double mkt_value_change_amount) {
        this.mkt_value_change_amount = mkt_value_change_amount;
    }

    public Double getInt_adjust_amount() {
        return int_adjust_amount;
    }

    public void setInt_adjust_amount(Double int_adjust_amount) {
        this.int_adjust_amount = int_adjust_amount;
    }

    public String getCustomer_code() {
        return customer_code;
    }

    public void setCustomer_code(String customer_code) {
        this.customer_code = customer_code;
    }

    public String getTransaction_cnt() {
        return transaction_cnt;
    }

    public void setTransaction_cnt(String transaction_cnt) {
        this.transaction_cnt = transaction_cnt;
    }

    public String getTransaction_purpose() {
        return transaction_purpose;
    }

    public void setTransaction_purpose(String transaction_purpose) {
        this.transaction_purpose = transaction_purpose;
    }

    public String getPar_rate() {
        return par_rate;
    }

    public void setPar_rate(String par_rate) {
        this.par_rate = par_rate;
    }

    public Double getLost_amount() {
        return lost_amount;
    }

    public void setLost_amount(Double lost_amount) {
        this.lost_amount = lost_amount;
    }

    public String getInterest_account_code() {
        return interest_account_code;
    }

    public void setInterest_account_code(String interest_account_code) {
        this.interest_account_code = interest_account_code;
    }

    public String getInvest_gain_account_code() {
        return invest_gain_account_code;
    }

    public void setInvest_gain_account_code(String invest_gain_account_code) {
        this.invest_gain_account_code = invest_gain_account_code;
    }

    public String getMkt_value_account_code() {
        return mkt_value_account_code;
    }

    public void setMkt_value_account_code(String mkt_value_account_code) {
        this.mkt_value_account_code = mkt_value_account_code;
    }

    public String getPaymnet_int_freq() {
        return paymnet_int_freq;
    }

    public void setPaymnet_int_freq(String paymnet_int_freq) {
        this.paymnet_int_freq = paymnet_int_freq;
    }

    public String getPayment_int_freq_mult() {
        return payment_int_freq_mult;
    }

    public void setPayment_int_freq_mult(String payment_int_freq_mult) {
        this.payment_int_freq_mult = payment_int_freq_mult;
    }

    public String getRevflag() {
        return revflag;
    }

    public void setRevflag(String revflag) {
        this.revflag = revflag;
    }

    public String getIntime() {
        return intime;
    }

    public void setIntime(String intime) {
        this.intime = intime;
    }

    public String getMemo1() {
        return memo1;
    }

    public void setMemo1(String memo1) {
        this.memo1 = memo1;
    }

    public String getMemo2() {
        return memo2;
    }

    public void setMemo2(String memo2) {
        this.memo2 = memo2;
    }

    public String getMemo3() {
        return memo3;
    }

    public void setMemo3(String memo3) {
        this.memo3 = memo3;
    }

    public String toDWHString() {
        // ToStringBuilder sb = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);

        StringBuffer sb=new StringBuffer();

        //取消科学计数法显示
        DecimalFormat df = new DecimalFormat("0.000000000");

        sb.append("\"").append(this.data_date==null?"":this.data_date).append("\"").append(",")
                .append("\"").append(this.source_sys_code==null?"":this.source_sys_code).append("\"").append(",")
                .append("\"").append(this.business_type==null?"":this.business_type).append("\"").append(",")
                .append("\"").append(this.account_id==null?"":this.account_id).append("\"").append(",")
                .append("\"").append(this.org_unit_code==null?"":this.org_unit_code).append("\"").append(",")
                .append("\"").append(account_code==null?"":account_code).append("\"").append(",")
                .append("\"").append(this.asset_type==null?"":this.asset_type).append("\"").append(",")
                .append("\"").append(this.product_code==null?"":this.product_code).append("\"").append(",")
                .append("\"").append(this.business_line_code==null?"":this.business_line_code).append("\"").append(",")
                .append("\"").append(this.currency_code==null?"":this.currency_code).append("\"").append(",")
                .append("\"").append(this.origination_date==null?"":this.origination_date).append("\"").append(",")
                .append("\"").append(this.maturity_date==null?"":this.maturity_date).append("\"").append(",")
                .append("\"").append(this.initial_date==null?"":this.initial_date).append("\"").append(",")
                .append("\"").append(this.sell_date==null?"":this.sell_date).append("\"").append(",")
                .append(this.par_balance==null?"":df.format(this.par_balance)).append(",")
                .append(this.cur_balance==null?"":df.format(this.cur_balance)).append(",")
                .append(this.org_balance==null?"":df.format(this.org_balance)).append(",")
                .append(this.org_par_balance==null?"":df.format(this.org_par_balance)).append(",")
                .append(this.cur_net_rate==null?"":df.format(this.cur_net_rate)).append(",")
                .append("\"").append(this.adjustable_type_code==null?"":this.adjustable_type_code).append("\"").append(",")
                .append("\"").append(this.last_reprice_date==null?"":this.last_reprice_date).append("\"").append(",")
                .append("\"").append(this.reprice_freq==null?"":this.reprice_freq).append("\"").append(",")
                .append("\"").append(this.reprice_freq_mult==null?"":this.reprice_freq_mult).append("\"").append(",")
                .append("\"").append(this.next_reprice_date==null?"":this.next_reprice_date).append("\"").append(",")
                .append("\"").append(this.payment_type==null?"":this.payment_type).append("\"").append(",")
                .append("\"").append(this.int_type==null?"":this.int_type).append("\"").append(",")
                .append("\"").append(this.last_payment_date==null?"":this.last_payment_date).append("\"").append(",")
                .append("\"").append(this.paymnet_freq==null?"":this.paymnet_freq).append("\"").append(",")
                .append("\"").append(this.payment_freq_mult==null?"":this.payment_freq_mult).append("\"").append(",")
                .append("\"").append(this.next_payment_date==null?"":this.next_payment_date).append("\"").append(",")
                .append(this.payment_amount==null?"":df.format(this.payment_amount)).append(",")
                .append(this.interest_amount==null?"":df.format(this.interest_amount)).append(",")
                .append(this.invest_gain_amount==null?"":df.format(this.invest_gain_amount)).append(",")
                .append(this.mkt_value_change_amount==null?"":df.format(this.mkt_value_change_amount)).append(",")
                .append(this.int_adjust_amount==null?"":df.format(this.int_adjust_amount)).append(",")
                .append("\"").append(this.customer_code==null?"":this.customer_code).append("\"").append(",")
                .append("\"").append(this.transaction_cnt==null?"":this.transaction_cnt).append("\"").append(",")
                .append("\"").append(this.transaction_purpose==null?"":this.transaction_purpose).append("\"").append(",")
                .append("\"").append(this.par_rate==null?"":this.par_rate).append("\"").append(",")
                .append(this.lost_amount==null?"":df.format(this.lost_amount)).append(",")
                .append("\"").append(interest_account_code==null?"":interest_account_code).append("\"").append(",")
                .append("\"").append(this.invest_gain_account_code==null?"":this.invest_gain_account_code).append("\"").append(",")
                .append("\"").append(this.mkt_value_account_code==null?"":this.mkt_value_account_code).append("\"").append(",")
                .append("\"").append(this.paymnet_int_freq==null?"":this.paymnet_int_freq).append("\"").append(",")
                .append("\"").append(this.payment_int_freq_mult==null?"":this.payment_int_freq_mult).append("\"").append(",")
                .append("\"").append(this.revflag==null?"":this.revflag).append("\"");

        return sb.toString();
    }


}
