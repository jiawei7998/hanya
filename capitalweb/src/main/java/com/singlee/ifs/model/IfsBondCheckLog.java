package com.singlee.ifs.model;

import javax.persistence.*;

/***
 * 交易要素  实体对象
 * @author singlee4
 *
 */
@Entity
@Table(name="IFS_BOND_CHECKLOG")
public class IfsBondCheckLog {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT SEQ_IFS_TRDPARAM.NEXTVAL FROM DUAL")
	//分组ID
	private String id;
	
	//交易单编号啊
	private String dealno;
	
	//债券代码
	private String bndcd;
	
	//交易状态
	private String status;
	
	//操作机构
	private String operatInst;
	
	//操作人员
	private String operatUser;
	
	//录入时间
	private String inputTime;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDealno() {
		return dealno;
	}

	public void setDealno(String dealno) {
		this.dealno = dealno;
	}

	public String getBndcd() {
		return bndcd;
	}

	public void setBndcd(String bndcd) {
		this.bndcd = bndcd;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getOperatInst() {
		return operatInst;
	}

	public void setOperatInst(String operatInst) {
		this.operatInst = operatInst;
	}

	public String getOperatUser() {
		return operatUser;
	}

	public void setOperatUser(String operatUser) {
		this.operatUser = operatUser;
	}

	public String getInputTime() {
		return inputTime;
	}

	public void setInputTime(String inputTime) {
		this.inputTime = inputTime;
	}
	
}	