package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "IFS_WIND_RATE")
public class IfsWindRateBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private String rateName;
	private String tenor;
	private String opicsRateCode;
	private String type;
	private BigDecimal mid;
	private Date spotDate;// 生效日期

	public String getRateName() {
		return rateName;
	}

	public void setRateName(String rateName) {
		this.rateName = rateName;
	}

	public String getTenor() {
		return tenor;
	}

	public void setTenor(String tenor) {
		this.tenor = tenor;
	}

	public String getOpicsRateCode() {
		return opicsRateCode;
	}

	public void setOpicsRateCode(String opicsRateCode) {
		this.opicsRateCode = opicsRateCode;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public BigDecimal getMid() {
		return mid;
	}

	public void setMid(BigDecimal mid) {
		this.mid = mid;
	}

	public Date getSpotDate() {
		return spotDate;
	}

	public void setSpotDate(Date spotDate) {
		this.spotDate = spotDate;
	}

}
