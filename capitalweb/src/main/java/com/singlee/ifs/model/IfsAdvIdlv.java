package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "IFS_ADV_IDLV")
public class IfsAdvIdlv extends IfsRevFlow {

	private static final long serialVersionUID = 1L;

	@Id
	// 审批单流水号
	private String ticketId;
	// 业务类型
	private String dealType;
	// 机构
	private String br;
	// 交易前端流水号
	private String fedealno;
	// OPICS交易流水号
	private String dealno;
	// 起息日
	private String vdate;
	// 到期日
	private String mdate;
	// 提前还款日
	private String emdate;
	// 付款日期
	private String settdate;
	// 币种
	private String ccy;
	// 交易金额
	private BigDecimal dealAmt;
	// 提前还款金额
	private BigDecimal advAmt;
	// 应计利息
	private BigDecimal interestAmt;
	// 处罚金额
	private BigDecimal penaltyAmt;
	// 支付金额
	private BigDecimal payAmt;
	// 是否全部提前还款
	private String isall;
	// 是否带利息
	private String capintind;
	// I 增加本金 D 减少本金
	private String prinincdecind;
	// D到期日适用 P按比例北京 M到期适用
	private String prorateind;
	// 产品代码
	private String product;
	// 产品类型
	private String prodType;

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	public String getDealType() {
		return dealType;
	}

	public void setDealType(String dealType) {
		this.dealType = dealType;
	}

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getFedealno() {
		return fedealno;
	}

	public void setFedealno(String fedealno) {
		this.fedealno = fedealno;
	}

	public String getDealno() {
		return dealno;
	}

	public void setDealno(String dealno) {
		this.dealno = dealno;
	}

	public String getVdate() {
		return vdate;
	}

	public void setVdate(String vdate) {
		this.vdate = vdate;
	}

	public String getMdate() {
		return mdate;
	}

	public void setMdate(String mdate) {
		this.mdate = mdate;
	}

	public String getEmdate() {
		return emdate;
	}

	public void setEmdate(String emdate) {
		this.emdate = emdate;
	}

	public String getSettdate() {
		return settdate;
	}

	public void setSettdate(String settdate) {
		this.settdate = settdate;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public BigDecimal getDealAmt() {
		return dealAmt;
	}

	public void setDealAmt(BigDecimal dealAmt) {
		this.dealAmt = dealAmt;
	}

	public BigDecimal getAdvAmt() {
		return advAmt;
	}

	public void setAdvAmt(BigDecimal advAmt) {
		this.advAmt = advAmt;
	}

	public BigDecimal getInterestAmt() {
		return interestAmt;
	}

	public void setInterestAmt(BigDecimal interestAmt) {
		this.interestAmt = interestAmt;
	}

	public BigDecimal getPenaltyAmt() {
		return penaltyAmt;
	}

	public void setPenaltyAmt(BigDecimal penaltyAmt) {
		this.penaltyAmt = penaltyAmt;
	}

	public BigDecimal getPayAmt() {
		return payAmt;
	}

	public void setPayAmt(BigDecimal payAmt) {
		this.payAmt = payAmt;
	}

	public String getIsall() {
		return isall;
	}

	public void setIsall(String isall) {
		this.isall = isall;
	}

	public String getCapintind() {
		return capintind;
	}

	public void setCapintind(String capintind) {
		this.capintind = capintind;
	}

	public String getPrinincdecind() {
		return prinincdecind;
	}

	public void setPrinincdecind(String prinincdecind) {
		this.prinincdecind = prinincdecind;
	}

	public String getProrateind() {
		return prorateind;
	}

	public void setProrateind(String prorateind) {
		this.prorateind = prorateind;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getProdType() {
		return prodType;
	}

	public void setProdType(String prodType) {
		this.prodType = prodType;
	}

}