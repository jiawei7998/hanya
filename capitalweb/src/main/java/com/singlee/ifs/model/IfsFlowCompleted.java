package com.singlee.ifs.model;

import javax.persistence.Transient;

/***
 * 审批完成表
 * 
 * @author lijing
 * 
 */
public class IfsFlowCompleted {

	// 内部id
	private String ticketId;

	// 成交单编号
	private String contractId;

	// 审批状态
	private String approveStatus;

	// 产品编号
	private String  prdNo;

	// 产品名称
	private String prdName;

	// 本金
	private String amt;

	// 利率
	private String rate;

	// 成交日期
	private String tradeDate;

	// 审批发起人
	private String sponsor;

	// 客户号
	private String custNo;

	// 客户名称
	private String custName;

	// 交易类型，0：事前交易，1：正式交易
	private String tradeType;

	// 审批要素组合
	private String tradeElements;

	// 流程id
	private String flowId;

	// 流程id
	@Transient
	private String taskId;
	// 流程id
	@Transient
	private String taskName;
	// opics交易号
	@Transient
	private String dealno;

	// 显示交易要素使用
	@Transient
	private String ccyPair;
	@Transient
	private String dealPrice;
	@Transient
	private String occupyCustNo;
	@Transient
	private String weight;
	@Transient
	private String mydealer;
	@Transient
	private String dealPort;
	@Transient
	private String settmeans;
	@Transient
	private String myInstId;
	@Transient
	private String mydircn; // 本方方向
	@Transient
	private String counterpartyInstId; // 对方机构

	@Transient
	private String approveDate;//清算审批日期
	/**
	 * 净额清算状态
	 */
	private String nettingStatus;

	//起息日期
	@Transient
	private String vDate;
	//交易日期
	@Transient
	private String fDate;
	//金额2
	@Transient
	private String contraAmt;
	@Transient
	private String statcode;
	
	//从视图查询的交易金额
	@Transient
	private String vamt;
	//从视图查询的成交汇率
	@Transient
	private String vrate;
	
	//从视图查询的交易货币
	@Transient
	private String tccy;
	//从视图查询的对应货币
	@Transient
	private String occy;
	
	
	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	public String getContractId() {
		return contractId;
	}

	public void setContractId(String contractId) {
		this.contractId = contractId;
	}

	public String getApproveStatus() {
		return approveStatus;
	}

	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}

	public String getPrdNo() {
		return prdNo;
	}

	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}

	public String getPrdName() {
		return prdName;
	}

	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}

	public String getAmt() {
		return amt;
	}

	public void setAmt(String amt) {
		this.amt = amt;
	}

	public String getRate() {
		return rate;
	}

	public void setRate(String rate) {
		this.rate = rate;
	}

	public String getTradeDate() {
		return tradeDate;
	}

	public void setTradeDate(String tradeDate) {
		this.tradeDate = tradeDate;
	}

	public String getSponsor() {
		return sponsor;
	}

	public void setSponsor(String sponsor) {
		this.sponsor = sponsor;
	}

	public String getCustNo() {
		return custNo;
	}

	public void setCustNo(String custNo) {
		this.custNo = custNo;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getTradeType() {
		return tradeType;
	}

	public void setTradeType(String tradeType) {
		this.tradeType = tradeType;
	}

	public String getTradeElements() {
		return tradeElements;
	}

	public void setTradeElements(String tradeElements) {
		this.tradeElements = tradeElements;
	}

	public String getFlowId() {
		return flowId;
	}

	public void setFlowId(String flowId) {
		this.flowId = flowId;
	}

	public String getDealno() {
		return dealno;
	}

	public void setDealno(String dealno) {
		this.dealno = dealno;
	}

	public String getCcyPair() {
		return ccyPair;
	}

	public void setCcyPair(String ccyPair) {
		this.ccyPair = ccyPair;
	}

	public String getDealPrice() {
		return dealPrice;
	}

	public void setDealPrice(String dealPrice) {
		this.dealPrice = dealPrice;
	}

	public String getOccupyCustNo() {
		return occupyCustNo;
	}

	public void setOccupyCustNo(String occupyCustNo) {
		this.occupyCustNo = occupyCustNo;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public String getMydealer() {
		return mydealer;
	}

	public void setMydealer(String mydealer) {
		this.mydealer = mydealer;
	}

	public String getDealPort() {
		return dealPort;
	}

	public void setDealPort(String dealPort) {
		this.dealPort = dealPort;
	}

	public String getSettmeans() {
		return settmeans;
	}

	public void setSettmeans(String settmeans) {
		this.settmeans = settmeans;
	}

	public String getMyInstId() {
		return myInstId;
	}

	public void setMyInstId(String myInstId) {
		this.myInstId = myInstId;
	}

	public String getMydircn() {
		return mydircn;
	}

	public void setMydircn(String mydircn) {
		this.mydircn = mydircn;
	}

	public String getCounterpartyInstId() {
		return counterpartyInstId;
	}

	public void setCounterpartyInstId(String counterpartyInstId) {
		this.counterpartyInstId = counterpartyInstId;
	}

	public String getNettingStatus() {
		return nettingStatus;
	}

	public void setNettingStatus(String nettingStatus) {
		this.nettingStatus = nettingStatus;
	}

	public String getApproveDate() {
		return approveDate;
	}

	public void setApproveDate(String approveDate) {
		this.approveDate = approveDate;
	}

	public String getvDate() {
		return vDate;
	}

	public void setvDate(String vDate) {
		this.vDate = vDate;
	}

	public String getContraAmt() {
		return contraAmt;
	}

	public void setContraAmt(String contraAmt) {
		this.contraAmt = contraAmt;
	}

	public void setStatcode(String statcode) {
		this.statcode = statcode;
	}

	public String getStatcode() {
		return statcode;
	}

	public String getVamt() {
		return vamt;
	}

	public void setVamt(String vamt) {
		this.vamt = vamt;
	}

	public String getVrate() {
		return vrate;
	}

	public void setVrate(String vrate) {
		this.vrate = vrate;
	}

	public String getTccy() {
		return tccy;
	}

	public void setTccy(String tccy) {
		this.tccy = tccy;
	}

	public String getOccy() {
		return occy;
	}

	public void setOccy(String occy) {
		this.occy = occy;
	}

	public String getfDate() {
		return fDate;
	}

	public void setfDate(String fDate) {
		this.fDate = fDate;
	}
	
	

}