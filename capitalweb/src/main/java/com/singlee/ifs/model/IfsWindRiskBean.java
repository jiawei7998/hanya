package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "IFS_WIND_RISK")
public class IfsWindRiskBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private String bondCode;
	private String bondName;
	private String rdate;
	private BigDecimal jiuqi;
	private BigDecimal tuxing;
	private BigDecimal pv;
	private BigDecimal memo;

	public String getBondCode() {
		return bondCode;
	}

	public void setBondCode(String bondCode) {
		this.bondCode = bondCode;
	}

	public String getBondName() {
		return bondName;
	}

	public void setBondName(String bondName) {
		this.bondName = bondName;
	}

	public String getRdate() {
		return rdate;
	}

	public void setRdate(String rdate) {
		this.rdate = rdate;
	}

	public BigDecimal getJiuqi() {
		return jiuqi;
	}

	public void setJiuqi(BigDecimal jiuqi) {
		this.jiuqi = jiuqi;
	}

	public BigDecimal getTuxing() {
		return tuxing;
	}

	public void setTuxing(BigDecimal tuxing) {
		this.tuxing = tuxing;
	}

	public BigDecimal getPv() {
		return pv;
	}

	public void setPv(BigDecimal pv) {
		this.pv = pv;
	}

	public BigDecimal getMemo() {
		return memo;
	}

	public void setMemo(BigDecimal memo) {
		this.memo = memo;
	}

}
