package com.singlee.ifs.model;

import java.io.Serializable;

public class IfsCfetsmetalGoldKey extends IfsBaseFlow  implements Serializable {
		//交易单号（系统流水号）
		private String ticketId;
    	//交易状态
  		private String dealTransType;
  		//本方机构
  		private String InstId;
  		//对方机构
  		private String counterpartyInstId;
  		//本方交易员
  		private String dealer;
  		//对方交易员
  		private String counterpartyDealer;
  		//交易日期
  		private String forDate;
  		//交易时间
  		private String forTime;
  		//交易方式
  		private String tradingType;
  		//货币对
  		private String currencyPair;

    private static final long serialVersionUID = 1L;


    public String getInstId() {
		return InstId;
	}

	public void setInstId(String instId) {
		InstId = instId;
	}

	public String getCounterpartyInstId() {
		return counterpartyInstId;
	}

	public void setCounterpartyInstId(String counterpartyInstId) {
		this.counterpartyInstId = counterpartyInstId;
	}

	public String getDealer() {
		return dealer;
	}

	public void setDealer(String dealer) {
		this.dealer = dealer;
	}

	public String getCounterpartyDealer() {
		return counterpartyDealer;
	}

	public void setCounterpartyDealer(String counterpartyDealer) {
		this.counterpartyDealer = counterpartyDealer;
	}

	public String getForDate() {
		return forDate;
	}

	public void setForDate(String forDate) {
		this.forDate = forDate;
	}

	public String getForTime() {
		return forTime;
	}

	public void setForTime(String forTime) {
		this.forTime = forTime;
	}


	public String getTradingType() {
		return tradingType;
	}

	public void setTradingType(String tradingType) {
		this.tradingType = tradingType;
	}

	public String getCurrencyPair() {
		return currencyPair;
	}

	public void setCurrencyPair(String currencyPair) {
		this.currencyPair = currencyPair;
	}

	public String getDealTransType() {
		return dealTransType;
	}

	public void setDealTransType(String dealTransType) {
		this.dealTransType = dealTransType;
	}

	public String getTicketId() {
        return ticketId;
    }

    public void setTicketId(String ticketId) {
        this.ticketId = ticketId == null ? null : ticketId.trim();
    }
}