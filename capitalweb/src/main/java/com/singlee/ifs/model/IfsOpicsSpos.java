package com.singlee.ifs.model;

import java.io.Serializable;

/**
 * @author ：chenguo
 * @date ：Created in 2021/11/17 14:28
 * @description：${description}
 * @modified By：
 * @version:
 */
public class IfsOpicsSpos implements Serializable {
    /**
     * 债券代码
     */
    private String secid;

    /**
     * 账户类型
     */
    private String invtype;

    /**
     * 成本中心
     */
    private String cost;

    /**
     * 投资组合
     */
    private String port;

    /**
     * 持仓类别
     */
    private String acctngtype;

    /**
     * 债券名称
     */
    private String sname;

    /**
     * 起息日
     */
    private String vdate;

    /**
     * 期限
     */
    private String tenor;

    /**
     * 到期日
     */
    private String mdate;

    /**
     * 债券面值
     */
    private String prinamt;

    /**
     * 票面利率
     */
    private String couprate;

    /**
     * 买断式抵押面额(元)
     */
    private String rbamount;

    /**
     * 买断式质押面额(元)
     */
    private String vbamount;

    /**
     * 质押式抵押面额(元)
     */
    private String rpamount;

    /**
     * 质押式质押面额(元)
     */
    private String vpamount;

    /**
     * 债券借贷借入面额(元)
     */
    private String sbamount;

    /**
     * 债券借贷借入抵押面额(元)
     */
    private String lbamount;

    /**
     * 债券借贷借出面额(元)
     */
    private String slamount;

    /**
     * 债券借贷借出抵押面额(元)
     */
    private String lcamount;

    /**
     * 国库定期存款质押面额(元)
     */
    private String rdamount;

    /**
     * 常备借贷便利质押面额(元)
     */
    private String cbamount;

    /**
     * 中期借贷便利质押面额(元)
     */
    private String zqamount;

    /**
     * 押债业务质押面额(元)
     */
    private String yzamount;

    /**
     * 可用面额(元)
     */
    private String kyme;

    /**
     * 备注
     */
    private String bz;

    public String getSecid() {
        return secid;
    }

    public void setSecid(String secid) {
        this.secid = secid;
    }

    public String getInvtype() {
        return invtype;
    }

    public void setInvtype(String invtype) {
        this.invtype = invtype;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getAcctngtype() {
        return acctngtype;
    }

    public void setAcctngtype(String acctngtype) {
        this.acctngtype = acctngtype;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public String getVdate() {
        return vdate;
    }

    public void setVdate(String vdate) {
        this.vdate = vdate;
    }

    public String getTenor() {
        return tenor;
    }

    public void setTenor(String tenor) {
        this.tenor = tenor;
    }

    public String getMdate() {
        return mdate;
    }

    public void setMdate(String mdate) {
        this.mdate = mdate;
    }

    public String getPrinamt() {
        return prinamt;
    }

    public void setPrinamt(String prinamt) {
        this.prinamt = prinamt;
    }

    public String getCouprate() {
        return couprate;
    }

    public void setCouprate(String couprate) {
        this.couprate = couprate;
    }

    public String getRbamount() {
        return rbamount;
    }

    public void setRbamount(String rbamount) {
        this.rbamount = rbamount;
    }

    public String getVbamount() {
        return vbamount;
    }

    public void setVbamount(String vbamount) {
        this.vbamount = vbamount;
    }

    public String getRpamount() {
        return rpamount;
    }

    public void setRpamount(String rpamount) {
        this.rpamount = rpamount;
    }

    public String getVpamount() {
        return vpamount;
    }

    public void setVpamount(String vpamount) {
        this.vpamount = vpamount;
    }

    public String getSbamount() {
        return sbamount;
    }

    public void setSbamount(String sbamount) {
        this.sbamount = sbamount;
    }

    public String getLbamount() {
        return lbamount;
    }

    public void setLbamount(String lbamount) {
        this.lbamount = lbamount;
    }

    public String getSlamount() {
        return slamount;
    }

    public void setSlamount(String slamount) {
        this.slamount = slamount;
    }

    public String getLcamount() {
        return lcamount;
    }

    public void setLcamount(String lcamount) {
        this.lcamount = lcamount;
    }

    public String getRdamount() {
        return rdamount;
    }

    public void setRdamount(String rdamount) {
        this.rdamount = rdamount;
    }

    public String getCbamount() {
        return cbamount;
    }

    public void setCbamount(String cbamount) {
        this.cbamount = cbamount;
    }

    public String getZqamount() {
        return zqamount;
    }

    public void setZqamount(String zqamount) {
        this.zqamount = zqamount;
    }

    public String getYzamount() {
        return yzamount;
    }

    public void setYzamount(String yzamount) {
        this.yzamount = yzamount;
    }

    public String getKyme() {
        return kyme;
    }

    public void setKyme(String kyme) {
        this.kyme = kyme;
    }

    public String getBz() {
        return bz;
    }

    public void setBz(String bz) {
        this.bz = bz;
    }

    @Override
    public String toString() {
        return "IfsOpicsSpos{" +
                "secid='" + secid + '\'' +
                ", invtype='" + invtype + '\'' +
                ", cost='" + cost + '\'' +
                ", port='" + port + '\'' +
                ", acctngtype='" + acctngtype + '\'' +
                ", sname='" + sname + '\'' +
                ", vdate='" + vdate + '\'' +
                ", tenor='" + tenor + '\'' +
                ", mdate='" + mdate + '\'' +
                ", prinamt='" + prinamt + '\'' +
                ", couprate='" + couprate + '\'' +
                ", rbamount='" + rbamount + '\'' +
                ", vbamount='" + vbamount + '\'' +
                ", rpamount='" + rpamount + '\'' +
                ", vpamount='" + vpamount + '\'' +
                ", sbamount='" + sbamount + '\'' +
                ", lbamount='" + lbamount + '\'' +
                ", slamount='" + slamount + '\'' +
                ", lcamount='" + lcamount + '\'' +
                ", rdamount='" + rdamount + '\'' +
                ", cbamount='" + cbamount + '\'' +
                ", zqamount='" + zqamount + '\'' +
                ", yzamount='" + yzamount + '\'' +
                ", kyme='" + kyme + '\'' +
                ", bz='" + bz + '\'' +
                '}';
    }
}
