package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * opics对应的节假日表
 * @author lij
 *
 */
@Entity
@Table(name="IFS_OPICS_HLDY")
public class IfsOpicsHldy implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	/**
     * 日历id:存放币种
     */
    private String calendarid;

    /**
     * 是否节假日   0：否     1：是
     */
    private String holidaytype;

    /**
     * 节假日期
     */
    private Date holidate;

    /**
     * 最后维护日期
     */
    private Date lstmntdte;

    /**
     * 操作者
     */
    private String operator;

    /**
     * 同步状态
     */
    private String status;

    /**
     * 备注
     */
    private String remark;


    public String getCalendarid() {
        return calendarid;
    }

    public void setCalendarid(String calendarid) {
        this.calendarid = calendarid == null ? null : calendarid.trim();
    }

    public String getHolidaytype() {
        return holidaytype;
    }

    public void setHolidaytype(String holidaytype) {
        this.holidaytype = holidaytype == null ? null : holidaytype.trim();
    }

    public Date getHolidate() {
        return holidate;
    }

    public void setHolidate(Date holidate) {
        this.holidate = holidate;
    }
    
    public Date getLstmntdte() {
        return lstmntdte;
    }

    public void setLstmntdte(Date lstmntdte) {
        this.lstmntdte = lstmntdte;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator == null ? null : operator.trim();
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

}
