package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Author zhangkai
 * @Description 
 * @Date 
 */
/**
    * 存放同业(活期)
    */
@Entity
@Table(name = "IFS_CURRENT_DEPOSITOUT")
public class IfsCurrentDepositOut implements Serializable {
    private static final long serialVersionUID = -5689126392627362976L;
    /**
    * 交易单号
    */
    @Id
    private String ticketId;

    /**
    * 客户号
    */
    private String custId;

    /**
    * 客户名称
    */
    private String custName;

    /**
    * 客户类型
    */
    private String ctype;

    /**
    * 金融机构类型
    */
    private String clitype;

    /**
    * 成交日期
    */
    private String forDate;

    /**
    * 币种
    */
    private String currency;

    /**
    * 本金(元)
    */
    private BigDecimal amt;

    /**
    * 利率类型
    */
    private String rateType;

    /**
    * 基准利率代码
    */
    private String basisRateCode;

    /**
    * 利率点差(千分之一)
    */
    private BigDecimal benchmarkSpread;

    /**
    * 利率(%)
    */
    private BigDecimal rate;

    /**
    * 计息基础
    */
    private String basis;

    /**
    * 付息频率
    */
    private String paymentFrequency;

    /**
    * 付息类型  D-一次性还本付息  IR-按期付息
    */
    private String scheduleType;

    /**
    * 利息付款日期
    */
    private String intpayday;

    /**
    * 利息日期调整规则
    */
    private String intdaterule;

    /**
     * 利率备注
     */
    private String rateNote;

    /**
     * 是否开立存款证实书
     */
    private String hasVerify;

    /**
     * 五级分类
     */
    private String fiveLevelClass;

    /**
     * 归属部门
     */
    private String attributionDept;


    /**
    * 客户经理
    */
    private String trad;

    /**
    * 核心存款账户
    */
    private String coreAcctNo;

    /**
    * 核心存款账户名
    */
    private String coreAcctName;

    /**
    * 交易来源
    */
    private String dealSource;

    /**
    * 付款账号
    */
    private String selfAcccode;

    /**
    * 付款账户名称
    */
    private String selfAccname;

    /**
    * 付款开户行大额行号
    */
    private String selfBankcode;

    /**
    * 付款开户行名称
    */
    private String selfBankname;

    /**
    * 收款账号
    */
    private String partyAcccode;

    /**
    * 收款账户名称
    */
    private String partyAccname;

    /**
    * 收款开户行大额行号
    */
    private String partyBankcode;

    /**
    * 收款开户行名称
    */
    private String partyBankname;

    /**
     * 备注
     */
    private String note;

    public String getTicketId() {
        return ticketId;
    }

    public void setTicketId(String ticketId) {
        this.ticketId = ticketId;
    }

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getCtype() {
        return ctype;
    }

    public void setCtype(String ctype) {
        this.ctype = ctype;
    }

    public String getClitype() {
        return clitype;
    }

    public void setClitype(String clitype) {
        this.clitype = clitype;
    }

    public String getForDate() {
        return forDate;
    }

    public void setForDate(String forDate) {
        this.forDate = forDate;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public BigDecimal getAmt() {
        return amt;
    }

    public void setAmt(BigDecimal amt) {
        this.amt = amt;
    }

    public String getRateType() {
        return rateType;
    }

    public void setRateType(String rateType) {
        this.rateType = rateType;
    }

    public String getBasisRateCode() {
        return basisRateCode;
    }

    public void setBasisRateCode(String basisRateCode) {
        this.basisRateCode = basisRateCode;
    }

    public BigDecimal getBenchmarkSpread() {
        return benchmarkSpread;
    }

    public void setBenchmarkSpread(BigDecimal benchmarkSpread) {
        this.benchmarkSpread = benchmarkSpread;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public String getBasis() {
        return basis;
    }

    public void setBasis(String basis) {
        this.basis = basis;
    }

    public String getPaymentFrequency() {
        return paymentFrequency;
    }

    public void setPaymentFrequency(String paymentFrequency) {
        this.paymentFrequency = paymentFrequency;
    }

    public String getScheduleType() {
        return scheduleType;
    }

    public void setScheduleType(String scheduleType) {
        this.scheduleType = scheduleType;
    }

    public String getIntpayday() {
        return intpayday;
    }

    public void setIntpayday(String intpayday) {
        this.intpayday = intpayday;
    }

    public String getIntdaterule() {
        return intdaterule;
    }

    public void setIntdaterule(String intdaterule) {
        this.intdaterule = intdaterule;
    }

    public String getTrad() {
        return trad;
    }

    public void setTrad(String trad) {
        this.trad = trad;
    }

    public String getCoreAcctNo() {
        return coreAcctNo;
    }

    public void setCoreAcctNo(String coreAcctNo) {
        this.coreAcctNo = coreAcctNo;
    }

    public String getCoreAcctName() {
        return coreAcctName;
    }

    public void setCoreAcctName(String coreAcctName) {
        this.coreAcctName = coreAcctName;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getDealSource() {
        return dealSource;
    }

    public void setDealSource(String dealSource) {
        this.dealSource = dealSource;
    }

    public String getRateNote() {
        return rateNote;
    }

    public void setRateNote(String rateNote) {
        this.rateNote = rateNote;
    }

    public String getHasVerify() {
        return hasVerify;
    }

    public void setHasVerify(String hasVerify) {
        this.hasVerify = hasVerify;
    }

    public String getFiveLevelClass() {
        return fiveLevelClass;
    }

    public void setFiveLevelClass(String fiveLevelClass) {
        this.fiveLevelClass = fiveLevelClass;
    }

    public String getAttributionDept() {
        return attributionDept;
    }

    public void setAttributionDept(String attributionDept) {
        this.attributionDept = attributionDept;
    }

    public String getSelfAcccode() {
        return selfAcccode;
    }

    public void setSelfAcccode(String selfAcccode) {
        this.selfAcccode = selfAcccode;
    }

    public String getSelfAccname() {
        return selfAccname;
    }

    public void setSelfAccname(String selfAccname) {
        this.selfAccname = selfAccname;
    }

    public String getSelfBankcode() {
        return selfBankcode;
    }

    public void setSelfBankcode(String selfBankcode) {
        this.selfBankcode = selfBankcode;
    }

    public String getSelfBankname() {
        return selfBankname;
    }

    public void setSelfBankname(String selfBankname) {
        this.selfBankname = selfBankname;
    }

    public String getPartyAcccode() {
        return partyAcccode;
    }

    public void setPartyAcccode(String partyAcccode) {
        this.partyAcccode = partyAcccode;
    }

    public String getPartyAccname() {
        return partyAccname;
    }

    public void setPartyAccname(String partyAccname) {
        this.partyAccname = partyAccname;
    }

    public String getPartyBankcode() {
        return partyBankcode;
    }

    public void setPartyBankcode(String partyBankcode) {
        this.partyBankcode = partyBankcode;
    }

    public String getPartyBankname() {
        return partyBankname;
    }

    public void setPartyBankname(String partyBankname) {
        this.partyBankname = partyBankname;
    }
}