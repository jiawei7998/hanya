package com.singlee.ifs.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Column;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Luozb
 * @version 1.0.0
 * @ClassName IfsCoreaccount.java
 * @Description 日终对账表
 * @createTime 2021年10月18日 10:59:00
 */
public class IfsCoreaccount {
    /**
     * 新核心账号
     */
    @Column(name = "NEWNO")
    private String newno;

    /**
     * 对账日期
     */
    @Column(name = "POSTDATE")
    private String postdate;

    /**
     * 版本
     */
    @Column(name = "VERSION")
    private BigDecimal version;

    /**
     * 科目号
     */
    @Column(name = "CORENO")
    private String coreno;

    /**
     * 贷方名称
     */
    @Column(name = "DRCRNAME")
    private String drcrname;

    /**
     * 借贷标识
     */
    @Column(name = "DRCRIND")
    private String drcrind;

    /**
     * 核心余额
     */
    @Column(name = "BCOREAMT")
    private BigDecimal bcoreamt;

    /**
     * OPICS余额
     */
    @Column(name = "BOPICSAMT")
    private BigDecimal bopicsamt;

    /**
     * 余额差额
     */
    @Column(name = "BAMT")
    private BigDecimal bamt;

    /**
     * 核心发生额
     */
    @Column(name = "PCOREAMT")
    private BigDecimal pcoreamt;

    /**
     * opics发生额
     */
    @Column(name = "POPICSAMT")
    private BigDecimal popicsamt;

    /**
     * 发生额差额
     */
    @Column(name = "PAMT")
    private BigDecimal pamt;

    /**
     * 插入时间
     */
    @Column(name = "INTIME")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date intime;

    /**
     * 备注1
     */
    @Column(name = "MEMO1")
    private String memo1;

    /**
     * 备注2
     */
    @Column(name = "MEMO2")
    private String memo2;

    /**
     * 备注3
     */
    @Column(name = "MEMO3")
    private String memo3;

    @Column(name = "CREATOR")
    private BigDecimal creator;

    /**
     * 获取新核心账号
     *
     * @return NEWNO - 新核心账号
     */
    public String getNewno() {
        return newno;
    }

    /**
     * 设置新核心账号
     *
     * @param newno 新核心账号
     */
    public void setNewno(String newno) {
        this.newno = newno;
    }

    /**
     * 获取对账日期
     *
     * @return POSTDATE - 对账日期
     */
    public String getPostdate() {
        return postdate;
    }

    /**
     * 设置对账日期
     *
     * @param postdate 对账日期
     */
    public void setPostdate(String postdate) {
        this.postdate = postdate;
    }

    /**
     * 获取版本
     *
     * @return VERSION - 版本
     */
    public BigDecimal getVersion() {
        return version;
    }

    /**
     * 设置版本
     *
     * @param version 版本
     */
    public void setVersion(BigDecimal version) {
        this.version = version;
    }

    /**
     * 获取科目号
     *
     * @return CORENO - 科目号
     */
    public String getCoreno() {
        return coreno;
    }

    /**
     * 设置科目号
     *
     * @param coreno 科目号
     */
    public void setCoreno(String coreno) {
        this.coreno = coreno;
    }

    /**
     * 获取贷方名称
     *
     * @return DRCRNAME - 贷方名称
     */
    public String getDrcrname() {
        return drcrname;
    }

    /**
     * 设置贷方名称
     *
     * @param drcrname 贷方名称
     */
    public void setDrcrname(String drcrname) {
        this.drcrname = drcrname;
    }

    /**
     * 获取借贷标识
     *
     * @return DRCRIND - 借贷标识
     */
    public String getDrcrind() {
        return drcrind;
    }

    /**
     * 设置借贷标识
     *
     * @param drcrind 借贷标识
     */
    public void setDrcrind(String drcrind) {
        this.drcrind = drcrind;
    }

    /**
     * 获取核心余额
     *
     * @return BCOREAMT - 核心余额
     */
    public BigDecimal getBcoreamt() {
        return bcoreamt;
    }

    /**
     * 设置核心余额
     *
     * @param bcoreamt 核心余额
     */
    public void setBcoreamt(BigDecimal bcoreamt) {
        this.bcoreamt = bcoreamt;
    }

    /**
     * 获取OPICS余额
     *
     * @return BOPICSAMT - OPICS余额
     */
    public BigDecimal getBopicsamt() {
        return bopicsamt;
    }

    /**
     * 设置OPICS余额
     *
     * @param bopicsamt OPICS余额
     */
    public void setBopicsamt(BigDecimal bopicsamt) {
        this.bopicsamt = bopicsamt;
    }

    /**
     * 获取余额差额
     *
     * @return BAMT - 余额差额
     */
    public BigDecimal getBamt() {
        return bamt;
    }

    /**
     * 设置余额差额
     *
     * @param bamt 余额差额
     */
    public void setBamt(BigDecimal bamt) {
        this.bamt = bamt;
    }

    /**
     * 获取核心发生额
     *
     * @return PCOREAMT - 核心发生额
     */
    public BigDecimal getPcoreamt() {
        return pcoreamt;
    }

    /**
     * 设置核心发生额
     *
     * @param pcoreamt 核心发生额
     */
    public void setPcoreamt(BigDecimal pcoreamt) {
        this.pcoreamt = pcoreamt;
    }

    /**
     * 获取opics发生额
     *
     * @return POPICSAMT - opics发生额
     */
    public BigDecimal getPopicsamt() {
        return popicsamt;
    }

    /**
     * 设置opics发生额
     *
     * @param popicsamt opics发生额
     */
    public void setPopicsamt(BigDecimal popicsamt) {
        this.popicsamt = popicsamt;
    }

    /**
     * 获取发生额差额
     *
     * @return PAMT - 发生额差额
     */
    public BigDecimal getPamt() {
        return pamt;
    }

    /**
     * 设置发生额差额
     *
     * @param pamt 发生额差额
     */
    public void setPamt(BigDecimal pamt) {
        this.pamt = pamt;
    }

    /**
     * 获取插入时间
     *
     * @return INTIME - 插入时间
     */
    public Date getIntime() {
        return intime;
    }

    /**
     * 设置插入时间
     *
     * @param intime 插入时间
     */
    public void setIntime(Date intime) {
        this.intime = intime;
    }

    /**
     * 获取备注1
     *
     * @return MEMO1 - 备注1
     */
    public String getMemo1() {
        return memo1;
    }

    /**
     * 设置备注1
     *
     * @param memo1 备注1
     */
    public void setMemo1(String memo1) {
        this.memo1 = memo1;
    }

    /**
     * 获取备注2
     *
     * @return MEMO2 - 备注2
     */
    public String getMemo2() {
        return memo2;
    }

    /**
     * 设置备注2
     *
     * @param memo2 备注2
     */
    public void setMemo2(String memo2) {
        this.memo2 = memo2;
    }

    /**
     * 获取备注3
     *
     * @return MEMO3 - 备注3
     */
    public String getMemo3() {
        return memo3;
    }

    /**
     * 设置备注3
     *
     * @param memo3 备注3
     */
    public void setMemo3(String memo3) {
        this.memo3 = memo3;
    }

    /**
     * @return CREATOR
     */
    public BigDecimal getCreator() {
        return creator;
    }

    /**
     * @param creator
     */
    public void setCreator(BigDecimal creator) {
        this.creator = creator;
    }
}
