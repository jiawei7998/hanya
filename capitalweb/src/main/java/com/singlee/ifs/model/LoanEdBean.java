package com.singlee.ifs.model;

import java.io.Serializable;

public class LoanEdBean implements Serializable {

	private static final long serialVersionUID = -7444880217733940070L;
	// 交易流水号
	private String dealNo1;
	// 业务品种
	private String productCode;
	// 业务名称
	private String productName;
	// 交易对手编号
	private String custNo;
	// 交易对手名称
	private String custName;
	// 经营单位
	private String institution;
	// 起息日
	private String vdate;
	// 到期日
	private String mdate;
	// 金额
	private double amt1;
	// 录入理由
	private String reason;
	// 备注1
	private String remark1;
	// 备注2
	private String remark2;
	// 状态
	private int state;
	// 老的处理编号
	private String oldDealNo;
	// 老的金额
	private String oldAmt;
	// 操作员
	private String ioper;
	// 是否是线下手工录入的交易
	private String offLine;
	// 审批状态
	private int approveStatus;
	// 修改日期
	private String modifyDate;
	// 上送日期
	private String inputDate;
	// 上送时间
	private String inputTime;

	private String dealNo2; // 业务流水号

	private String batchTId;// 额度操作总批次

	private String batchCId;// 额度操作子批次

	private String dealType; // 交易类型

	private String vDate;// 起息日

	private String mDate; // 到期日

	private String  prdNo; // 产品代码

	private String partyId; // 参与方

	private double amt2; // 批次操作的金额

	private String ecifNo; // 客户代码

	private String batchRefNo; // 批次关联流水号

	private String batchEvent; // 批次事件

	private String batchStatus;// 批次状态

	private String batchTime;// 操作时间

	public String getDealNo1() {
		return dealNo1;
	}

	public void setDealNo1(String dealNo1) {
		this.dealNo1 = dealNo1;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getCustNo() {
		return custNo;
	}

	public void setCustNo(String custNo) {
		this.custNo = custNo;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getInstitution() {
		return institution;
	}

	public void setInstitution(String institution) {
		this.institution = institution;
	}

	public String getVdate() {
		return vdate;
	}

	public void setVdate(String vdate) {
		this.vdate = vdate;
	}

	public String getMdate() {
		return mdate;
	}

	public void setMdate(String mdate) {
		this.mdate = mdate;
	}

	public double getAmt1() {
		return amt1;
	}

	public void setAmt1(double amt1) {
		this.amt1 = amt1;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getRemark1() {
		return remark1;
	}

	public void setRemark1(String remark1) {
		this.remark1 = remark1;
	}

	public String getRemark2() {
		return remark2;
	}

	public void setRemark2(String remark2) {
		this.remark2 = remark2;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public String getOldDealNo() {
		return oldDealNo;
	}

	public void setOldDealNo(String oldDealNo) {
		this.oldDealNo = oldDealNo;
	}

	public String getOldAmt() {
		return oldAmt;
	}

	public void setOldAmt(String oldAmt) {
		this.oldAmt = oldAmt;
	}

	public String getIoper() {
		return ioper;
	}

	public void setIoper(String ioper) {
		this.ioper = ioper;
	}

	public String getOffLine() {
		return offLine;
	}

	public void setOffLine(String offLine) {
		this.offLine = offLine;
	}

	public int getApproveStatus() {
		return approveStatus;
	}

	public void setApproveStatus(int approveStatus) {
		this.approveStatus = approveStatus;
	}

	public String getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(String modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getInputDate() {
		return inputDate;
	}

	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	public String getInputTime() {
		return inputTime;
	}

	public void setInputTime(String inputTime) {
		this.inputTime = inputTime;
	}

	public String getDealNo2() {
		return dealNo2;
	}

	public void setDealNo2(String dealNo2) {
		this.dealNo2 = dealNo2;
	}

	public String getBatchTId() {
		return batchTId;
	}

	public void setBatchTId(String batchTId) {
		this.batchTId = batchTId;
	}

	public String getBatchCId() {
		return batchCId;
	}

	public void setBatchCId(String batchCId) {
		this.batchCId = batchCId;
	}

	public String getDealType() {
		return dealType;
	}

	public void setDealType(String dealType) {
		this.dealType = dealType;
	}

	public String getvDate() {
		return vDate;
	}

	public void setvDate(String vDate) {
		this.vDate = vDate;
	}

	public String getmDate() {
		return mDate;
	}

	public void setmDate(String mDate) {
		this.mDate = mDate;
	}

	public String getPrdNo() {
		return prdNo;
	}

	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}

	public String getPartyId() {
		return partyId;
	}

	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}

	public double getAmt2() {
		return amt2;
	}

	public void setAmt2(double amt2) {
		this.amt2 = amt2;
	}

	public String getEcifNo() {
		return ecifNo;
	}

	public void setEcifNo(String ecifNo) {
		this.ecifNo = ecifNo;
	}

	public String getBatchRefNo() {
		return batchRefNo;
	}

	public void setBatchRefNo(String batchRefNo) {
		this.batchRefNo = batchRefNo;
	}

	public String getBatchEvent() {
		return batchEvent;
	}

	public void setBatchEvent(String batchEvent) {
		this.batchEvent = batchEvent;
	}

	public String getBatchStatus() {
		return batchStatus;
	}

	public void setBatchStatus(String batchStatus) {
		this.batchStatus = batchStatus;
	}

	public String getBatchTime() {
		return batchTime;
	}

	public void setBatchTime(String batchTime) {
		this.batchTime = batchTime;
	}
}