package com.singlee.ifs.model;
/***
 * 字段限额 实体对象
 * @author lijing
 *
 */
public class IfsLimitColumns {
	
	/*字段限额编号*/
	private String limitId;
	
	/*产品类型*/
	private String prd;
	
	/*限额字段*/
	private String colum;
	
	/*字典值*/
	private String dictId;
	
	
	/*字典具体值*/
	private String dictId1;
	
	/*录入时间*/
	private String inputTime;
	
	/*上次修改时间*/
	private String lastTime;
	
	/*备注*/
	private String remark;
	
	/*字典值中文名称*/
	private String dictTran;
	
	/*字典具体值中文名称*/
	private String dictId1Tran;
	

	
	

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getDictTran() {
		return dictTran;
	}

	public void setDictTran(String dictTran) {
		this.dictTran = dictTran;
	}

	public String getDictId1Tran() {
		return dictId1Tran;
	}

	public void setDictId1Tran(String dictId1Tran) {
		this.dictId1Tran = dictId1Tran;
	}

	public String getLimitId() {
		return limitId;
	}

	public void setLimitId(String limitId) {
		this.limitId = limitId;
	}

	public String getPrd() {
		return prd;
	}

	public void setPrd(String prd) {
		this.prd = prd;
	}

	public String getColum() {
		return colum;
	}

	public void setColum(String colum) {
		this.colum = colum;
	}

	public String getDictId() {
		return dictId;
	}

	public void setDictId(String dictId) {
		this.dictId = dictId;
	}

	public String getInputTime() {
		return inputTime;
	}

	public void setInputTime(String inputTime) {
		this.inputTime = inputTime;
	}

	public String getLastTime() {
		return lastTime;
	}

	public void setLastTime(String lastTime) {
		this.lastTime = lastTime;
	}

	public String getDictId1() {
		return dictId1;
	}

	public void setDictId1(String dictId1) {
		this.dictId1 = dictId1;
	}
	
	
	
	

}
