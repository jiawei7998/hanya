package com.singlee.ifs.model;

import java.math.BigDecimal;

public class KYgzBean {
    private BigDecimal id;

    private BigDecimal version;

    private String dealno;

    private String invtye;

    private String sectype;

    private String seccode;

    private String secname;

    private String term;

    private String vdate;

    private String mdate;

    private BigDecimal rate;

    private BigDecimal frate;

    private String intpayrule;

    private String saeldate;

    private BigDecimal samount;

    private BigDecimal sellprice;

    private BigDecimal buyavgamt;

    private BigDecimal diffinprice;

    private String inputdate;

    private BigDecimal creator;

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public BigDecimal getVersion() {
        return version;
    }

    public void setVersion(BigDecimal version) {
        this.version = version;
    }

    public String getDealno() {
        return dealno;
    }

    public void setDealno(String dealno) {
        this.dealno = dealno;
    }

    public String getInvtye() {
        return invtye;
    }

    public void setInvtye(String invtye) {
        this.invtye = invtye;
    }

    public String getSectype() {
        return sectype;
    }

    public void setSectype(String sectype) {
        this.sectype = sectype;
    }

    public String getSeccode() {
        return seccode;
    }

    public void setSeccode(String seccode) {
        this.seccode = seccode;
    }

    public String getSecname() {
        return secname;
    }

    public void setSecname(String secname) {
        this.secname = secname;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }


    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public BigDecimal getFrate() {
        return frate;
    }

    public void setFrate(BigDecimal frate) {
        this.frate = frate;
    }

    public String getIntpayrule() {
        return intpayrule;
    }

    public void setIntpayrule(String intpayrule) {
        this.intpayrule = intpayrule;
    }


    public BigDecimal getSamount() {
        return samount;
    }

    public void setSamount(BigDecimal samount) {
        this.samount = samount;
    }

    public BigDecimal getSellprice() {
        return sellprice;
    }

    public void setSellprice(BigDecimal sellprice) {
        this.sellprice = sellprice;
    }

    public BigDecimal getBuyavgamt() {
        return buyavgamt;
    }

    public void setBuyavgamt(BigDecimal buyavgamt) {
        this.buyavgamt = buyavgamt;
    }

    public BigDecimal getDiffinprice() {
        return diffinprice;
    }

    public void setDiffinprice(BigDecimal diffinprice) {
        this.diffinprice = diffinprice;
    }

    public BigDecimal getCreator() {
        return creator;
    }

    public void setCreator(BigDecimal creator) {
        this.creator = creator;
    }

    public String getVdate() {
        return vdate;
    }

    public void setVdate(String vdate) {
        this.vdate = vdate;
    }

    public String getMdate() {
        return mdate;
    }

    public void setMdate(String mdate) {
        this.mdate = mdate;
    }

    public String getSaeldate() {
        return saeldate;
    }

    public void setSaeldate(String saeldate) {
        this.saeldate = saeldate;
    }

    public String getInputdate() {
        return inputdate;
    }

    public void setInputdate(String inputdate) {
        this.inputdate = inputdate;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", version=").append(version);
        sb.append(", dealno=").append(dealno);
        sb.append(", invtye=").append(invtye);
        sb.append(", sectype=").append(sectype);
        sb.append(", seccode=").append(seccode);
        sb.append(", secname=").append(secname);
        sb.append(", term=").append(term);
        sb.append(", vdate=").append(vdate);
        sb.append(", mdate=").append(mdate);
        sb.append(", rate=").append(rate);
        sb.append(", frate=").append(frate);
        sb.append(", intpayrule=").append(intpayrule);
        sb.append(", saeldate=").append(saeldate);
        sb.append(", samount=").append(samount);
        sb.append(", sellprice=").append(sellprice);
        sb.append(", buyavgamt=").append(buyavgamt);
        sb.append(", diffinprice=").append(diffinprice);
        sb.append(", inputdate=").append(inputdate);
        sb.append(", creator=").append(creator);
        sb.append("]");
        return sb.toString();
    }
}