package com.singlee.ifs.model;

import java.io.Serializable;
import java.math.BigDecimal;

/***
 * 交易限额
 * @author copysun
 *
 */

public class IfsTradeLimit implements Serializable{
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	private BigDecimal id;

	/**
	 * 限额类型
	 */
	private String acctngtype;

	/**
	 * 交易账户交易限额
	 */
	private BigDecimal deallmt;

	/**
	 * 交易账户浮亏止损限额(%)
	 */
	private BigDecimal lostlmt;

	/**
	 * 交易账户单支券占比限额(%)
	 */
	private BigDecimal ratelmt;

	/**
	 * 交易账户发行人主体内部评级
	 */
	private String inlevel;

	/**
	 * 交易账户发行人主体外部评级
	 */
	private String outlevel;

	/**
	 * 银行账户单支券占比限额(%)
	 */
	private BigDecimal ratelmtb;

	/**
	 * 银行账户发行人主体外部评级
	 */
	private String inlevelb;

	/**
	 * 银行账户债券债项外部评级
	 */
	private String outlevelb;

	/**
	 * 估价基点价值限额(%)
	 */
	private BigDecimal pvbp;

	/**
	 * 久期限额
	 */
	private BigDecimal duration;

	/**
	 * 创建时间
	 */
	private String createTime;


	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	public String getAcctngtype() {
		return acctngtype;
	}

	public void setAcctngtype(String acctngtype) {
		this.acctngtype = acctngtype;
	}

	public BigDecimal getDeallmt() {
		return deallmt;
	}

	public void setDeallmt(BigDecimal deallmt) {
		this.deallmt = deallmt;
	}

	public BigDecimal getLostlmt() {
		return lostlmt;
	}

	public void setLostlmt(BigDecimal lostlmt) {
		this.lostlmt = lostlmt;
	}

	public BigDecimal getRatelmt() {
		return ratelmt;
	}

	public void setRatelmt(BigDecimal ratelmt) {
		this.ratelmt = ratelmt;
	}

	public String getInlevel() {
		return inlevel;
	}

	public void setInlevel(String inlevel) {
		this.inlevel = inlevel;
	}

	public String getOutlevel() {
		return outlevel;
	}

	public void setOutlevel(String outlevel) {
		this.outlevel = outlevel;
	}

	public BigDecimal getRatelmtb() {
		return ratelmtb;
	}

	public void setRatelmtb(BigDecimal ratelmtb) {
		this.ratelmtb = ratelmtb;
	}

	public String getInlevelb() {
		return inlevelb;
	}

	public void setInlevelb(String inlevelb) {
		this.inlevelb = inlevelb;
	}

	public String getOutlevelb() {
		return outlevelb;
	}

	public void setOutlevelb(String outlevelb) {
		this.outlevelb = outlevelb;
	}

	public BigDecimal getPvbp() {
		return pvbp;
	}

	public void setPvbp(BigDecimal pvbp) {
		this.pvbp = pvbp;
	}

	public BigDecimal getDuration() {
		return duration;
	}

	public void setDuration(BigDecimal duration) {
		this.duration = duration;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
}
