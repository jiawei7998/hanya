package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;
/***
 * opics 清算方式表
 * @author lij
 *
 */
@Entity
@Table(name="IFS_OPICS_SETM")
public class IfsOpicsSetm implements Serializable{
	
	/**
     * 机构分支
     */
    private String br;

    /**
     * 清算方式
     */
    private String settlmeans;

    /**
     * 描述
     */
    private String descr;

    /**
     * 
     */
    private String checkdigit;

    /**
     * 
     */
    private String genledgno;

    /**
     * 
     */
    private String acctval;

    /**
     * 
     */
    private String valacc;

    /**
     * 最后维护时间
     */
    private Date lstmntdte;

    /**
     * 
     */
    private String extintflag;

    /**
     * 
     */
    private String offacct;

    /**
     * 
     */
    private String autoauthorize;

    /**
     * 操作者
     */
    private String operator;

    /**
     * 同步状态
     */
    private String status;

    /**
     * 备注
     */
    private String remark;

    private static final long serialVersionUID = 1L;

    public String getBr() {
        return br;
    }

    public void setBr(String br) {
        this.br = br == null ? null : br.trim();
    }

    public String getSettlmeans() {
        return settlmeans;
    }

    public void setSettlmeans(String settlmeans) {
        this.settlmeans = settlmeans == null ? null : settlmeans.trim();
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr == null ? null : descr.trim();
    }

    public String getCheckdigit() {
        return checkdigit;
    }

    public void setCheckdigit(String checkdigit) {
        this.checkdigit = checkdigit == null ? null : checkdigit.trim();
    }

    public String getGenledgno() {
        return genledgno;
    }

    public void setGenledgno(String genledgno) {
        this.genledgno = genledgno == null ? null : genledgno.trim();
    }

    public String getAcctval() {
        return acctval;
    }

    public void setAcctval(String acctval) {
        this.acctval = acctval == null ? null : acctval.trim();
    }

    public String getValacc() {
        return valacc;
    }

    public void setValacc(String valacc) {
        this.valacc = valacc == null ? null : valacc.trim();
    }

    public Date getLstmntdte() {
        return lstmntdte;
    }

    public void setLstmntdte(Date lstmntdte) {
        this.lstmntdte = lstmntdte;
    }

    public String getExtintflag() {
        return extintflag;
    }

    public void setExtintflag(String extintflag) {
        this.extintflag = extintflag == null ? null : extintflag.trim();
    }

    public String getOffacct() {
        return offacct;
    }

    public void setOffacct(String offacct) {
        this.offacct = offacct == null ? null : offacct.trim();
    }

    public String getAutoauthorize() {
        return autoauthorize;
    }

    public void setAutoauthorize(String autoauthorize) {
        this.autoauthorize = autoauthorize == null ? null : autoauthorize.trim();
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator == null ? null : operator.trim();
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

}
