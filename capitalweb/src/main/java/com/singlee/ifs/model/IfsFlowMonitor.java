package com.singlee.ifs.model;

import javax.persistence.Transient;

/***
 * 审批监控表 主要用于首页工作台
 * 
 * @author lijing
 * 
 */
public class IfsFlowMonitor {

	// 内部id
	private String ticketId;

	// 成交单编号
	private String contractId;

	// 审批状态
	private String approveStatus;

	// 产品编号
	private String  prdNo;

	// 产品名称
	private String prdName;

	// 本金
	private String amt;

	// 利率
	private String rate;

	// 成交日期
	private String tradeDate;

	// 审批发起人
	private String sponsor;

	// 客户号
	private String custNo;

	// 客户名称
	private String custName;

	/**
	 * 本方方向
	 */
	private String mydircn;

	/**
	 * 对方机构
	 */
	private String counterpartyInstId;

	// 交易类型，0：事前交易，1：正式交易
	private String tradeType;

	// 审批要素组合
	private String tradeElements;

	// 流程id（全）
	private String flowId;
	//处理结果(异常审批时)
	private String processMsg;

	// 流程id
	@Transient
	private String taskId;
	// 流程name
	@Transient
	private String taskName;
	// opics交易号
	@Transient
	private String dealno;

	// 显示交易要素使用
	@Transient
	private String ccyPair;
	@Transient
	private String dealPrice;
	@Transient
	private String occupyCustNo;
	@Transient
	private String weight;
	@Transient
	private String mydealer;
	@Transient
	private String dealPort;
	@Transient
	private String settmeans;
	@Transient
	private String myInstId;
	@Transient
	private String approveList;//审批人列表

	/**
	 * 净额清算状态
	 */
	private String nettingStatus;

	//起息日期
	@Transient
	private String vDate;
	//金额2
	@Transient
	private String contraAmt;
	
	
	@Transient
	private String approveDate;
	@Transient
	private String statcode;
	
	//新增字段
	@Transient
	private String mydir;
	@Transient
	private String pccy;
	@Transient
	private String pamt;
	@Transient
	private String rccy;
	@Transient
	private String ramt;
	
	//从视图查询的交易金额
	@Transient
	private String vamt;
	//从视图查询的成交汇率
	@Transient
	private String vrate;
	
	//从视图查询的交易货币
	@Transient
	private String tccy;
	//从视图查询的对应货币
	@Transient
	private String occy;

	//前置产品代码
	@Transient
	private String fPrdCode;

	public String getfPrdCode() {
		return fPrdCode;
	}

	public void setfPrdCode(String fPrdCode) {
		this.fPrdCode = fPrdCode;
	}

	public String getTccy() {
		return tccy;
	}

	public void setTccy(String tccy) {
		this.tccy = tccy;
	}

	public String getOccy() {
		return occy;
	}

	public void setOccy(String occy) {
		this.occy = occy;
	}

	public String getVamt() {
		return vamt;
	}

	public void setVamt(String vamt) {
		this.vamt = vamt;
	}

	public String getVrate() {
		return vrate;
	}

	public void setVrate(String vrate) {
		this.vrate = vrate;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	public String getContractId() {
		return contractId;
	}

	public void setContractId(String contractId) {
		this.contractId = contractId;
	}

	public String getApproveStatus() {
		return approveStatus;
	}

	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}

	public String getPrdNo() {
		return prdNo;
	}

	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}

	public String getPrdName() {
		return prdName;
	}

	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}

	public String getAmt() {
		return amt;
	}

	public void setAmt(String amt) {
		this.amt = amt;
	}

	public String getRate() {
		return rate;
	}

	public void setRate(String rate) {
		this.rate = rate;
	}

	public String getTradeDate() {
		return tradeDate;
	}

	public void setTradeDate(String tradeDate) {
		this.tradeDate = tradeDate;
	}

	public String getSponsor() {
		return sponsor;
	}

	public void setSponsor(String sponsor) {
		this.sponsor = sponsor;
	}

	public String getCustNo() {
		return custNo;
	}

	public void setCustNo(String custNo) {
		this.custNo = custNo;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getTradeType() {
		return tradeType;
	}

	public void setTradeType(String tradeType) {
		this.tradeType = tradeType;
	}

	public String getTradeElements() {
		return tradeElements;
	}

	public void setTradeElements(String tradeElements) {
		this.tradeElements = tradeElements;
	}

	public String getFlowId() {
		return flowId;
	}

	public void setFlowId(String flowId) {
		this.flowId = flowId;
	}

	public String getMydircn() {
		return mydircn;
	}

	public void setMydircn(String mydircn) {
		this.mydircn = mydircn;
	}

	public String getCounterpartyInstId() {
		return counterpartyInstId;
	}

	public void setCounterpartyInstId(String counterpartyInstId) {
		this.counterpartyInstId = counterpartyInstId;
	}

	public String getDealno() {
		return dealno;
	}

	public void setDealno(String dealno) {
		this.dealno = dealno;
	}

	public String getCcyPair() {
		return ccyPair;
	}

	public void setCcyPair(String ccyPair) {
		this.ccyPair = ccyPair;
	}

	public String getDealPrice() {
		return dealPrice;
	}

	public void setDealPrice(String dealPrice) {
		this.dealPrice = dealPrice;
	}

	public String getOccupyCustNo() {
		return occupyCustNo;
	}

	public void setOccupyCustNo(String occupyCustNo) {
		this.occupyCustNo = occupyCustNo;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public String getMydealer() {
		return mydealer;
	}

	public void setMydealer(String mydealer) {
		this.mydealer = mydealer;
	}

	public String getDealPort() {
		return dealPort;
	}

	public void setDealPort(String dealPort) {
		this.dealPort = dealPort;
	}

	public String getSettmeans() {
		return settmeans;
	}

	public void setSettmeans(String settmeans) {
		this.settmeans = settmeans;
	}

	public String getMyInstId() {
		return myInstId;
	}

	public void setMyInstId(String myInstId) {
		this.myInstId = myInstId;
	}

	public String getNettingStatus() {
		return nettingStatus;
	}

	public void setNettingStatus(String nettingStatus) {
		this.nettingStatus = nettingStatus;
	}

	public String getvDate() {
		return vDate;
	}

	public void setvDate(String vDate) {
		this.vDate = vDate;
	}

	public String getContraAmt() {
		return contraAmt;
	}

	public void setContraAmt(String contraAmt) {
		this.contraAmt = contraAmt;
	}

	public void setApproveDate(String approveDate) {
		this.approveDate = approveDate;
	}

	public String getApproveDate() {
		return approveDate;
	}

	public void setStatcode(String statcode) {
		this.statcode = statcode;
	}

	public String getStatcode() {
		return statcode;
	}

	public String getApproveList() {
		return approveList;
	}

	public void setApproveList(String approveList) {
		this.approveList = approveList;
	}

	public String getMydir() {
		return mydir;
	}

	public void setMydir(String mydir) {
		this.mydir = mydir;
	}

	public String getPccy() {
		return pccy;
	}

	public void setPccy(String pccy) {
		this.pccy = pccy;
	}

	public String getPamt() {
		return pamt;
	}

	public void setPamt(String pamt) {
		this.pamt = pamt;
	}

	public String getRccy() {
		return rccy;
	}

	public void setRccy(String rccy) {
		this.rccy = rccy;
	}

	public String getRamt() {
		return ramt;
	}

	public void setRamt(String ramt) {
		this.ramt = ramt;
	}

	public String getProcessMsg() {
		return processMsg;
	}

	public void setProcessMsg(String processMsg) {
		this.processMsg = processMsg;
	}

}