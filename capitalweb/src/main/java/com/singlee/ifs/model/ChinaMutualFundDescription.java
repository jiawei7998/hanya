package com.singlee.ifs.model;


import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @ClassName: IbFundInfoOuter
 * @Description:  中国共同基金基本资料
 **/
@Entity
@Table(name = "CM_FUND_DESC")
public class ChinaMutualFundDescription implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    private String OBJECT_ID;
    private String F_INFO_WINDCODE;
    private String F_INFO_FRONT_CODE;
    private String F_INFO_BACKEND_CODE;
    private String F_INFO_FULLNAME;
    private String F_INFO_NAME;
    private String F_INFO_CORP_FUNDMANAGEMENTCOMP;
    private String F_INFO_CUSTODIANBANK;
    private String F_INFO_FIRSTINVESTTYPE;
    private String F_INFO_SETUPDATE;
    private String F_INFO_MATURITYDATE;
    private BigDecimal F_ISSUE_TOTALUNIT;
    private BigDecimal F_INFO_MANAGEMENTFEERATIO;
    private BigDecimal F_INFO_CUSTODIANFEERATIO;
    private String CRNY_CODE;
    private BigDecimal F_INFO_PTMYEAR;
    private String F_ISSUE_OEF_STARTDATEINST;
    private String F_ISSUE_OEF_DNDDATEINST;
    private BigDecimal F_INFO_PARVALUE;
    private String F_INFO_TRUSTTYPE;
    private String F_INFO_TRUSTEE;
    private String F_PCHREDM_PCHSTARTDATE;
    private String F_INFO_REDMSTARTDATE;
    private BigDecimal F_INFO_MINBUYAMOUNT;
    private BigDecimal F_INFO_EXPECTEDRATEOFRETURN;
    private String F_INFO_ISSUINGPLACE;
    private String F_INFO_BENCHMARK;
    private Integer F_INFO_STATUS;
    private String F_INFO_RESTRICTEDORNOT;
    private Integer F_INFO_STRUCTUREDORNOT;
    private String F_INFO_EXCHMARKET;
    private String F_INFO_FIRSTINVESTSTYLE;
    private String F_INFO_ISSUEDATE;
    private String F_INFO_TYPE;
    private Integer F_INFO_ISINITIAL;
    private String F_INFO_PINYIN;
    private String F_INFO_INVESTSCOPE;
    private String F_INFO_INVESTOBJECT;
    private String F_INFO_INVESTCONCEPTION;
    private String F_INFO_DECISION_BASIS;
    private Integer IS_INDEXFUND;
    private String F_INFO_DELISTDATE;
    private String F_INFO_CORP_FUNDMANAGEMENTID;
    private String F_INFO_CUSTODIANBANKID;
    private BigDecimal MAX_NUM_HOLDER;
    private BigDecimal MAX_NUM_COLTARGET;
    private String INVESTSTRATEGY;
    private String RISK_RETURN;
    private BigDecimal F_PCHREDM_PCHMINAMT;
    private BigDecimal F_PCHREDM_PCHMINAMT_EX;
    private String F_INFO_LISTDATE;
    private String F_INFO_ANNDATE;
    private BigDecimal F_CLOSED_OPERATION_PERIOD;
    private BigDecimal F_CLOSED_OPERATION_INTERVAL;
    private String F_INFO_REGISTRANT;
    private String F_PERSONAL_STARTDATEIND;
    private String F_PERSONAL_ENDDATEIND;
    private String F_INFO_FUND_ID;
    private String F_SALES_SERVICE_RATE;

    private String F_INVESTMENT_AREA;
    private BigDecimal CLOSE_INSTITU_OEF_DOWN;
    private BigDecimal CLOSE_INSTITU_OEF_UP;
    private BigDecimal S_FELLOW_DISTOR;
    private String F_PERSONAL_SUBTYPE;
    private String CLOSE_INSTITU_SUBTYPE;

    private String OPDATE;
    private String OPMODE;//0（新记录）、1（更新 /更正）、2（删除）
    private String MOPDATE;

    
    public String getMOPDATE() {
        return MOPDATE;
    }

    public void setMOPDATE(String mOPDATE) {
        MOPDATE = mOPDATE;
    }

    public String getOBJECT_ID() {
        return OBJECT_ID;
    }

    public void setOBJECT_ID(String OBJECT_ID) {
        this.OBJECT_ID = OBJECT_ID;
    }

    public String getF_INFO_WINDCODE() {
        return F_INFO_WINDCODE;
    }

    public void setF_INFO_WINDCODE(String f_INFO_WINDCODE) {
        F_INFO_WINDCODE = f_INFO_WINDCODE;
    }

    public String getF_INFO_FRONT_CODE() {
        return F_INFO_FRONT_CODE;
    }

    public void setF_INFO_FRONT_CODE(String f_INFO_FRONT_CODE) {
        F_INFO_FRONT_CODE = f_INFO_FRONT_CODE;
    }

    public String getF_INFO_BACKEND_CODE() {
        return F_INFO_BACKEND_CODE;
    }

    public void setF_INFO_BACKEND_CODE(String f_INFO_BACKEND_CODE) {
        F_INFO_BACKEND_CODE = f_INFO_BACKEND_CODE;
    }

    public String getF_INFO_FULLNAME() {
        return F_INFO_FULLNAME;
    }

    public void setF_INFO_FULLNAME(String f_INFO_FULLNAME) {
        F_INFO_FULLNAME = f_INFO_FULLNAME;
    }

    public String getF_INFO_NAME() {
        return F_INFO_NAME;
    }

    public void setF_INFO_NAME(String f_INFO_NAME) {
        F_INFO_NAME = f_INFO_NAME;
    }

    public String getF_INFO_CORP_FUNDMANAGEMENTCOMP() {
        return F_INFO_CORP_FUNDMANAGEMENTCOMP;
    }

    public void setF_INFO_CORP_FUNDMANAGEMENTCOMP(String f_INFO_CORP_FUNDMANAGEMENTCOMP) {
        F_INFO_CORP_FUNDMANAGEMENTCOMP = f_INFO_CORP_FUNDMANAGEMENTCOMP;
    }

    public String getF_INFO_CUSTODIANBANK() {
        return F_INFO_CUSTODIANBANK;
    }

    public void setF_INFO_CUSTODIANBANK(String f_INFO_CUSTODIANBANK) {
        F_INFO_CUSTODIANBANK = f_INFO_CUSTODIANBANK;
    }

    public String getF_INFO_FIRSTINVESTTYPE() {
        return F_INFO_FIRSTINVESTTYPE;
    }

    public void setF_INFO_FIRSTINVESTTYPE(String f_INFO_FIRSTINVESTTYPE) {
        F_INFO_FIRSTINVESTTYPE = f_INFO_FIRSTINVESTTYPE;
    }

    public String getF_INFO_SETUPDATE() {
        return F_INFO_SETUPDATE;
    }

    public void setF_INFO_SETUPDATE(String f_INFO_SETUPDATE) {
        F_INFO_SETUPDATE = f_INFO_SETUPDATE;
    }

    public String getF_INFO_MATURITYDATE() {
        return F_INFO_MATURITYDATE;
    }

    public void setF_INFO_MATURITYDATE(String f_INFO_MATURITYDATE) {
        F_INFO_MATURITYDATE = f_INFO_MATURITYDATE;
    }

    public BigDecimal getF_ISSUE_TOTALUNIT() {
        return F_ISSUE_TOTALUNIT;
    }

    public void setF_ISSUE_TOTALUNIT(BigDecimal f_ISSUE_TOTALUNIT) {
        F_ISSUE_TOTALUNIT = f_ISSUE_TOTALUNIT;
    }

    public BigDecimal getF_INFO_MANAGEMENTFEERATIO() {
        return F_INFO_MANAGEMENTFEERATIO;
    }

    public void setF_INFO_MANAGEMENTFEERATIO(BigDecimal f_INFO_MANAGEMENTFEERATIO) {
        F_INFO_MANAGEMENTFEERATIO = f_INFO_MANAGEMENTFEERATIO;
    }

    public BigDecimal getF_INFO_CUSTODIANFEERATIO() {
        return F_INFO_CUSTODIANFEERATIO;
    }

    public void setF_INFO_CUSTODIANFEERATIO(BigDecimal f_INFO_CUSTODIANFEERATIO) {
        F_INFO_CUSTODIANFEERATIO = f_INFO_CUSTODIANFEERATIO;
    }

    public String getCRNY_CODE() {
        return CRNY_CODE;
    }

    public void setCRNY_CODE(String CRNY_CODE) {
        this.CRNY_CODE = CRNY_CODE;
    }

    public BigDecimal getF_INFO_PTMYEAR() {
        return F_INFO_PTMYEAR;
    }

    public void setF_INFO_PTMYEAR(BigDecimal f_INFO_PTMYEAR) {
        F_INFO_PTMYEAR = f_INFO_PTMYEAR;
    }

    public String getF_ISSUE_OEF_STARTDATEINST() {
        return F_ISSUE_OEF_STARTDATEINST;
    }

    public void setF_ISSUE_OEF_STARTDATEINST(String f_ISSUE_OEF_STARTDATEINST) {
        F_ISSUE_OEF_STARTDATEINST = f_ISSUE_OEF_STARTDATEINST;
    }

    public String getF_ISSUE_OEF_DNDDATEINST() {
        return F_ISSUE_OEF_DNDDATEINST;
    }

    public void setF_ISSUE_OEF_DNDDATEINST(String f_ISSUE_OEF_DNDDATEINST) {
        F_ISSUE_OEF_DNDDATEINST = f_ISSUE_OEF_DNDDATEINST;
    }

    public BigDecimal getF_INFO_PARVALUE() {
        return F_INFO_PARVALUE;
    }

    public void setF_INFO_PARVALUE(BigDecimal f_INFO_PARVALUE) {
        F_INFO_PARVALUE = f_INFO_PARVALUE;
    }

    public String getF_INFO_TRUSTTYPE() {
        return F_INFO_TRUSTTYPE;
    }

    public void setF_INFO_TRUSTTYPE(String f_INFO_TRUSTTYPE) {
        F_INFO_TRUSTTYPE = f_INFO_TRUSTTYPE;
    }

    public String getF_INFO_TRUSTEE() {
        return F_INFO_TRUSTEE;
    }

    public void setF_INFO_TRUSTEE(String f_INFO_TRUSTEE) {
        F_INFO_TRUSTEE = f_INFO_TRUSTEE;
    }

    public String getF_PCHREDM_PCHSTARTDATE() {
        return F_PCHREDM_PCHSTARTDATE;
    }

    public void setF_PCHREDM_PCHSTARTDATE(String f_PCHREDM_PCHSTARTDATE) {
        F_PCHREDM_PCHSTARTDATE = f_PCHREDM_PCHSTARTDATE;
    }

    public String getF_INFO_REDMSTARTDATE() {
        return F_INFO_REDMSTARTDATE;
    }

    public void setF_INFO_REDMSTARTDATE(String f_INFO_REDMSTARTDATE) {
        F_INFO_REDMSTARTDATE = f_INFO_REDMSTARTDATE;
    }

    public BigDecimal getF_INFO_MINBUYAMOUNT() {
        return F_INFO_MINBUYAMOUNT;
    }

    public void setF_INFO_MINBUYAMOUNT(BigDecimal f_INFO_MINBUYAMOUNT) {
        F_INFO_MINBUYAMOUNT = f_INFO_MINBUYAMOUNT;
    }

    public BigDecimal getF_INFO_EXPECTEDRATEOFRETURN() {
        return F_INFO_EXPECTEDRATEOFRETURN;
    }

    public void setF_INFO_EXPECTEDRATEOFRETURN(BigDecimal f_INFO_EXPECTEDRATEOFRETURN) {
        F_INFO_EXPECTEDRATEOFRETURN = f_INFO_EXPECTEDRATEOFRETURN;
    }

    public String getF_INFO_ISSUINGPLACE() {
        return F_INFO_ISSUINGPLACE;
    }

    public void setF_INFO_ISSUINGPLACE(String f_INFO_ISSUINGPLACE) {
        F_INFO_ISSUINGPLACE = f_INFO_ISSUINGPLACE;
    }

    public String getF_INFO_BENCHMARK() {
        return F_INFO_BENCHMARK;
    }

    public void setF_INFO_BENCHMARK(String f_INFO_BENCHMARK) {
        F_INFO_BENCHMARK = f_INFO_BENCHMARK;
    }

    public Integer getF_INFO_STATUS() {
        return F_INFO_STATUS;
    }

    public void setF_INFO_STATUS(Integer f_INFO_STATUS) {
        F_INFO_STATUS = f_INFO_STATUS;
    }

    public String getF_INFO_RESTRICTEDORNOT() {
        return F_INFO_RESTRICTEDORNOT;
    }

    public void setF_INFO_RESTRICTEDORNOT(String f_INFO_RESTRICTEDORNOT) {
        F_INFO_RESTRICTEDORNOT = f_INFO_RESTRICTEDORNOT;
    }

    public Integer getF_INFO_STRUCTUREDORNOT() {
        return F_INFO_STRUCTUREDORNOT;
    }

    public void setF_INFO_STRUCTUREDORNOT(Integer f_INFO_STRUCTUREDORNOT) {
        F_INFO_STRUCTUREDORNOT = f_INFO_STRUCTUREDORNOT;
    }

    public String getF_INFO_EXCHMARKET() {
        return F_INFO_EXCHMARKET;
    }

    public void setF_INFO_EXCHMARKET(String f_INFO_EXCHMARKET) {
        F_INFO_EXCHMARKET = f_INFO_EXCHMARKET;
    }

    public String getF_INFO_FIRSTINVESTSTYLE() {
        return F_INFO_FIRSTINVESTSTYLE;
    }

    public void setF_INFO_FIRSTINVESTSTYLE(String f_INFO_FIRSTINVESTSTYLE) {
        F_INFO_FIRSTINVESTSTYLE = f_INFO_FIRSTINVESTSTYLE;
    }

    public String getF_INFO_ISSUEDATE() {
        return F_INFO_ISSUEDATE;
    }

    public void setF_INFO_ISSUEDATE(String f_INFO_ISSUEDATE) {
        F_INFO_ISSUEDATE = f_INFO_ISSUEDATE;
    }

    public String getF_INFO_TYPE() {
        return F_INFO_TYPE;
    }

    public void setF_INFO_TYPE(String f_INFO_TYPE) {
        F_INFO_TYPE = f_INFO_TYPE;
    }

    public Integer getF_INFO_ISINITIAL() {
        return F_INFO_ISINITIAL;
    }

    public void setF_INFO_ISINITIAL(Integer f_INFO_ISINITIAL) {
        F_INFO_ISINITIAL = f_INFO_ISINITIAL;
    }

    public String getF_INFO_PINYIN() {
        return F_INFO_PINYIN;
    }

    public void setF_INFO_PINYIN(String f_INFO_PINYIN) {
        F_INFO_PINYIN = f_INFO_PINYIN;
    }

    public String getF_INFO_INVESTSCOPE() {
        return F_INFO_INVESTSCOPE;
    }

    public void setF_INFO_INVESTSCOPE(String f_INFO_INVESTSCOPE) {
        F_INFO_INVESTSCOPE = f_INFO_INVESTSCOPE;
    }

    public String getF_INFO_INVESTOBJECT() {
        return F_INFO_INVESTOBJECT;
    }

    public void setF_INFO_INVESTOBJECT(String f_INFO_INVESTOBJECT) {
        F_INFO_INVESTOBJECT = f_INFO_INVESTOBJECT;
    }

    public String getF_INFO_INVESTCONCEPTION() {
        return F_INFO_INVESTCONCEPTION;
    }

    public void setF_INFO_INVESTCONCEPTION(String f_INFO_INVESTCONCEPTION) {
        F_INFO_INVESTCONCEPTION = f_INFO_INVESTCONCEPTION;
    }

    public String getF_INFO_DECISION_BASIS() {
        return F_INFO_DECISION_BASIS;
    }

    public void setF_INFO_DECISION_BASIS(String f_INFO_DECISION_BASIS) {
        F_INFO_DECISION_BASIS = f_INFO_DECISION_BASIS;
    }

    public Integer getIS_INDEXFUND() {
        return IS_INDEXFUND;
    }

    public void setIS_INDEXFUND(Integer IS_INDEXFUND) {
        this.IS_INDEXFUND = IS_INDEXFUND;
    }

    public String getF_INFO_DELISTDATE() {
        return F_INFO_DELISTDATE;
    }

    public void setF_INFO_DELISTDATE(String f_INFO_DELISTDATE) {
        F_INFO_DELISTDATE = f_INFO_DELISTDATE;
    }

    public String getF_INFO_CORP_FUNDMANAGEMENTID() {
        return F_INFO_CORP_FUNDMANAGEMENTID;
    }

    public void setF_INFO_CORP_FUNDMANAGEMENTID(String f_INFO_CORP_FUNDMANAGEMENTID) {
        F_INFO_CORP_FUNDMANAGEMENTID = f_INFO_CORP_FUNDMANAGEMENTID;
    }

    public String getF_INFO_CUSTODIANBANKID() {
        return F_INFO_CUSTODIANBANKID;
    }

    public void setF_INFO_CUSTODIANBANKID(String f_INFO_CUSTODIANBANKID) {
        F_INFO_CUSTODIANBANKID = f_INFO_CUSTODIANBANKID;
    }

    public BigDecimal getMAX_NUM_HOLDER() {
        return MAX_NUM_HOLDER;
    }

    public void setMAX_NUM_HOLDER(BigDecimal MAX_NUM_HOLDER) {
        this.MAX_NUM_HOLDER = MAX_NUM_HOLDER;
    }

    public BigDecimal getMAX_NUM_COLTARGET() {
        return MAX_NUM_COLTARGET;
    }

    public void setMAX_NUM_COLTARGET(BigDecimal MAX_NUM_COLTARGET) {
        this.MAX_NUM_COLTARGET = MAX_NUM_COLTARGET;
    }

    public String getINVESTSTRATEGY() {
        return INVESTSTRATEGY;
    }

    public void setINVESTSTRATEGY(String INVESTSTRATEGY) {
        this.INVESTSTRATEGY = INVESTSTRATEGY;
    }

    public String getRISK_RETURN() {
        return RISK_RETURN;
    }

    public void setRISK_RETURN(String RISK_RETURN) {
        this.RISK_RETURN = RISK_RETURN;
    }

    public BigDecimal getF_PCHREDM_PCHMINAMT() {
        return F_PCHREDM_PCHMINAMT;
    }

    public void setF_PCHREDM_PCHMINAMT(BigDecimal f_PCHREDM_PCHMINAMT) {
        F_PCHREDM_PCHMINAMT = f_PCHREDM_PCHMINAMT;
    }

    public BigDecimal getF_PCHREDM_PCHMINAMT_EX() {
        return F_PCHREDM_PCHMINAMT_EX;
    }

    public void setF_PCHREDM_PCHMINAMT_EX(BigDecimal f_PCHREDM_PCHMINAMT_EX) {
        F_PCHREDM_PCHMINAMT_EX = f_PCHREDM_PCHMINAMT_EX;
    }

    public String getF_INFO_LISTDATE() {
        return F_INFO_LISTDATE;
    }

    public void setF_INFO_LISTDATE(String f_INFO_LISTDATE) {
        F_INFO_LISTDATE = f_INFO_LISTDATE;
    }

    public String getF_INFO_ANNDATE() {
        return F_INFO_ANNDATE;
    }

    public void setF_INFO_ANNDATE(String f_INFO_ANNDATE) {
        F_INFO_ANNDATE = f_INFO_ANNDATE;
    }

    public BigDecimal getF_CLOSED_OPERATION_PERIOD() {
        return F_CLOSED_OPERATION_PERIOD;
    }

    public void setF_CLOSED_OPERATION_PERIOD(BigDecimal f_CLOSED_OPERATION_PERIOD) {
        F_CLOSED_OPERATION_PERIOD = f_CLOSED_OPERATION_PERIOD;
    }

    public BigDecimal getF_CLOSED_OPERATION_INTERVAL() {
        return F_CLOSED_OPERATION_INTERVAL;
    }

    public void setF_CLOSED_OPERATION_INTERVAL(BigDecimal f_CLOSED_OPERATION_INTERVAL) {
        F_CLOSED_OPERATION_INTERVAL = f_CLOSED_OPERATION_INTERVAL;
    }

    public String getF_INFO_REGISTRANT() {
        return F_INFO_REGISTRANT;
    }

    public void setF_INFO_REGISTRANT(String f_INFO_REGISTRANT) {
        F_INFO_REGISTRANT = f_INFO_REGISTRANT;
    }

    public String getF_PERSONAL_STARTDATEIND() {
        return F_PERSONAL_STARTDATEIND;
    }

    public void setF_PERSONAL_STARTDATEIND(String f_PERSONAL_STARTDATEIND) {
        F_PERSONAL_STARTDATEIND = f_PERSONAL_STARTDATEIND;
    }

    public String getF_PERSONAL_ENDDATEIND() {
        return F_PERSONAL_ENDDATEIND;
    }

    public void setF_PERSONAL_ENDDATEIND(String f_PERSONAL_ENDDATEIND) {
        F_PERSONAL_ENDDATEIND = f_PERSONAL_ENDDATEIND;
    }

    public String getF_INFO_FUND_ID() {
        return F_INFO_FUND_ID;
    }

    public void setF_INFO_FUND_ID(String f_INFO_FUND_ID) {
        F_INFO_FUND_ID = f_INFO_FUND_ID;
    }

    public String getF_SALES_SERVICE_RATE() {
        return F_SALES_SERVICE_RATE;
    }

    public void setF_SALES_SERVICE_RATE(String f_SALES_SERVICE_RATE) {
        F_SALES_SERVICE_RATE = f_SALES_SERVICE_RATE;
    }

    public String getF_INVESTMENT_AREA() {
        return F_INVESTMENT_AREA;
    }

    public void setF_INVESTMENT_AREA(String f_INVESTMENT_AREA) {
        F_INVESTMENT_AREA = f_INVESTMENT_AREA;
    }

    public BigDecimal getCLOSE_INSTITU_OEF_DOWN() {
        return CLOSE_INSTITU_OEF_DOWN;
    }

    public void setCLOSE_INSTITU_OEF_DOWN(BigDecimal CLOSE_INSTITU_OEF_DOWN) {
        this.CLOSE_INSTITU_OEF_DOWN = CLOSE_INSTITU_OEF_DOWN;
    }

    public BigDecimal getCLOSE_INSTITU_OEF_UP() {
        return CLOSE_INSTITU_OEF_UP;
    }

    public void setCLOSE_INSTITU_OEF_UP(BigDecimal CLOSE_INSTITU_OEF_UP) {
        this.CLOSE_INSTITU_OEF_UP = CLOSE_INSTITU_OEF_UP;
    }

    public BigDecimal getS_FELLOW_DISTOR() {
        return S_FELLOW_DISTOR;
    }

    public void setS_FELLOW_DISTOR(BigDecimal s_FELLOW_DISTOR) {
        S_FELLOW_DISTOR = s_FELLOW_DISTOR;
    }

    public String getF_PERSONAL_SUBTYPE() {
        return F_PERSONAL_SUBTYPE;
    }

    public void setF_PERSONAL_SUBTYPE(String f_PERSONAL_SUBTYPE) {
        F_PERSONAL_SUBTYPE = f_PERSONAL_SUBTYPE;
    }

    public String getCLOSE_INSTITU_SUBTYPE() {
        return CLOSE_INSTITU_SUBTYPE;
    }

    public void setCLOSE_INSTITU_SUBTYPE(String CLOSE_INSTITU_SUBTYPE) {
        this.CLOSE_INSTITU_SUBTYPE = CLOSE_INSTITU_SUBTYPE;
    }

    public String getOPDATE() {
        return OPDATE;
    }

    public void setOPDATE(String OPDATE) {
        this.OPDATE = OPDATE;
    }

    public String getOPMODE() {
        return OPMODE;
    }

    public void setOPMODE(String OPMODE) {
        this.OPMODE = OPMODE;
    }

    @Override
    public String toString() {
        return "ChinaMutualFundDescription [OBJECT_ID=" + OBJECT_ID + ", F_INFO_WINDCODE=" + F_INFO_WINDCODE
                + ", F_INFO_FRONT_CODE=" + F_INFO_FRONT_CODE + ", F_INFO_BACKEND_CODE=" + F_INFO_BACKEND_CODE
                + ", F_INFO_FULLNAME=" + F_INFO_FULLNAME + ", F_INFO_NAME=" + F_INFO_NAME
                + ", F_INFO_CORP_FUNDMANAGEMENTCOMP=" + F_INFO_CORP_FUNDMANAGEMENTCOMP + ", F_INFO_CUSTODIANBANK="
                + F_INFO_CUSTODIANBANK + ", F_INFO_FIRSTINVESTTYPE=" + F_INFO_FIRSTINVESTTYPE + ", F_INFO_SETUPDATE="
                + F_INFO_SETUPDATE + ", F_INFO_MATURITYDATE=" + F_INFO_MATURITYDATE + ", F_ISSUE_TOTALUNIT="
                + F_ISSUE_TOTALUNIT + ", F_INFO_MANAGEMENTFEERATIO=" + F_INFO_MANAGEMENTFEERATIO
                + ", F_INFO_CUSTODIANFEERATIO=" + F_INFO_CUSTODIANFEERATIO + ", CRNY_CODE=" + CRNY_CODE
                + ", F_INFO_PTMYEAR=" + F_INFO_PTMYEAR + ", F_ISSUE_OEF_STARTDATEINST=" + F_ISSUE_OEF_STARTDATEINST
                + ", F_ISSUE_OEF_DNDDATEINST=" + F_ISSUE_OEF_DNDDATEINST + ", F_INFO_PARVALUE=" + F_INFO_PARVALUE
                + ", F_INFO_TRUSTTYPE=" + F_INFO_TRUSTTYPE + ", F_INFO_TRUSTEE=" + F_INFO_TRUSTEE
                + ", F_PCHREDM_PCHSTARTDATE=" + F_PCHREDM_PCHSTARTDATE + ", F_INFO_REDMSTARTDATE="
                + F_INFO_REDMSTARTDATE + ", F_INFO_MINBUYAMOUNT=" + F_INFO_MINBUYAMOUNT
                + ", F_INFO_EXPECTEDRATEOFRETURN=" + F_INFO_EXPECTEDRATEOFRETURN + ", F_INFO_ISSUINGPLACE="
                + F_INFO_ISSUINGPLACE + ", F_INFO_BENCHMARK=" + F_INFO_BENCHMARK + ", F_INFO_STATUS=" + F_INFO_STATUS
                + ", F_INFO_RESTRICTEDORNOT=" + F_INFO_RESTRICTEDORNOT + ", F_INFO_STRUCTUREDORNOT="
                + F_INFO_STRUCTUREDORNOT + ", F_INFO_EXCHMARKET=" + F_INFO_EXCHMARKET + ", F_INFO_FIRSTINVESTSTYLE="
                + F_INFO_FIRSTINVESTSTYLE + ", F_INFO_ISSUEDATE=" + F_INFO_ISSUEDATE + ", F_INFO_TYPE=" + F_INFO_TYPE
                + ", F_INFO_ISINITIAL=" + F_INFO_ISINITIAL + ", F_INFO_PINYIN=" + F_INFO_PINYIN
                + ", F_INFO_INVESTSCOPE=" + F_INFO_INVESTSCOPE + ", F_INFO_INVESTOBJECT=" + F_INFO_INVESTOBJECT
                + ", F_INFO_INVESTCONCEPTION=" + F_INFO_INVESTCONCEPTION + ", F_INFO_DECISION_BASIS="
                + F_INFO_DECISION_BASIS + ", IS_INDEXFUND=" + IS_INDEXFUND + ", F_INFO_DELISTDATE=" + F_INFO_DELISTDATE
                + ", F_INFO_CORP_FUNDMANAGEMENTID=" + F_INFO_CORP_FUNDMANAGEMENTID + ", F_INFO_CUSTODIANBANKID="
                + F_INFO_CUSTODIANBANKID + ", MAX_NUM_HOLDER=" + MAX_NUM_HOLDER + ", MAX_NUM_COLTARGET="
                + MAX_NUM_COLTARGET + ", INVESTSTRATEGY=" + INVESTSTRATEGY + ", RISK_RETURN=" + RISK_RETURN
                + ", F_PCHREDM_PCHMINAMT=" + F_PCHREDM_PCHMINAMT + ", F_PCHREDM_PCHMINAMT_EX=" + F_PCHREDM_PCHMINAMT_EX
                + ", F_INFO_LISTDATE=" + F_INFO_LISTDATE + ", F_INFO_ANNDATE=" + F_INFO_ANNDATE
                + ", F_CLOSED_OPERATION_PERIOD=" + F_CLOSED_OPERATION_PERIOD + ", F_CLOSED_OPERATION_INTERVAL="
                + F_CLOSED_OPERATION_INTERVAL + ", F_INFO_REGISTRANT=" + F_INFO_REGISTRANT
                + ", F_PERSONAL_STARTDATEIND=" + F_PERSONAL_STARTDATEIND + ", F_PERSONAL_ENDDATEIND="
                + F_PERSONAL_ENDDATEIND + ", F_INFO_FUND_ID=" + F_INFO_FUND_ID + ", F_SALES_SERVICE_RATE="
                + F_SALES_SERVICE_RATE + ", F_INVESTMENT_AREA=" + F_INVESTMENT_AREA + ", CLOSE_INSTITU_OEF_DOWN="
                + CLOSE_INSTITU_OEF_DOWN + ", CLOSE_INSTITU_OEF_UP=" + CLOSE_INSTITU_OEF_UP + ", S_FELLOW_DISTOR="
                + S_FELLOW_DISTOR + ", F_PERSONAL_SUBTYPE=" + F_PERSONAL_SUBTYPE + ", CLOSE_INSTITU_SUBTYPE="
                + CLOSE_INSTITU_SUBTYPE + ", OPDATE=" + OPDATE + ", OPMODE=" + OPMODE + ", MOPDATE=" + MOPDATE + "]";
    }
    
    
}
