package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;
@Entity
@Table(name = "IFS_APPROVERMB_CBT")
public class IfsApprovermbCbt  extends IfsApproveBaseBean {

    private String bondCode;

    private String bondName;

    private BigDecimal cleanPrice;

    private Integer totalFaceValue;

    private BigDecimal yield;

    private BigDecimal tradeAmount;

    private BigDecimal accuredInterest;

    private BigDecimal totalAccuredInterest;

    private BigDecimal dirtyPrice;

    private BigDecimal settlementAmount;

    private String settlementMethod;

    private String settlementDate;
    
    private static final long serialVersionUID = 1L;

	public String getBondCode() {
		return bondCode;
	}

	public void setBondCode(String bondCode) {
		this.bondCode = bondCode;
	}

	public String getBondName() {
		return bondName;
	}

	public void setBondName(String bondName) {
		this.bondName = bondName;
	}

	public BigDecimal getCleanPrice() {
		return cleanPrice;
	}

	public void setCleanPrice(BigDecimal cleanPrice) {
		this.cleanPrice = cleanPrice;
	}

	public Integer getTotalFaceValue() {
		return totalFaceValue;
	}

	public void setTotalFaceValue(Integer totalFaceValue) {
		this.totalFaceValue = totalFaceValue;
	}

	public BigDecimal getYield() {
		return yield;
	}

	public void setYield(BigDecimal yield) {
		this.yield = yield;
	}

	public BigDecimal getTradeAmount() {
		return tradeAmount;
	}

	public void setTradeAmount(BigDecimal tradeAmount) {
		this.tradeAmount = tradeAmount;
	}

	public BigDecimal getAccuredInterest() {
		return accuredInterest;
	}

	public void setAccuredInterest(BigDecimal accuredInterest) {
		this.accuredInterest = accuredInterest;
	}

	public BigDecimal getTotalAccuredInterest() {
		return totalAccuredInterest;
	}

	public void setTotalAccuredInterest(BigDecimal totalAccuredInterest) {
		this.totalAccuredInterest = totalAccuredInterest;
	}

	public BigDecimal getDirtyPrice() {
		return dirtyPrice;
	}

	public void setDirtyPrice(BigDecimal dirtyPrice) {
		this.dirtyPrice = dirtyPrice;
	}

	public BigDecimal getSettlementAmount() {
		return settlementAmount;
	}

	public void setSettlementAmount(BigDecimal settlementAmount) {
		this.settlementAmount = settlementAmount;
	}

	public String getSettlementMethod() {
		return settlementMethod;
	}

	public void setSettlementMethod(String settlementMethod) {
		this.settlementMethod = settlementMethod;
	}

	public String getSettlementDate() {
		return settlementDate;
	}

	public void setSettlementDate(String settlementDate) {
		this.settlementDate = settlementDate;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

  
}