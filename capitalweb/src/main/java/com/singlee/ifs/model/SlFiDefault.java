package com.singlee.ifs.model;

import javax.persistence.Column;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Luozb
 * @version 1.0.0
 * @ClassName SlFiDefault.java
 * @Description 债券违约处理
 * @createTime 2021年09月29日 16:35:00
 */
@Table(name = "OPICS47.SL_FI_DEFAULT")
public class SlFiDefault implements Serializable {
    @Column(name = "BR")
    private String br;

    @Column(name = "COST")
    private String cost;

    @Column(name = "CCY")
    private String ccy;

    @Column(name = "INVTYPE")
    private String invtype;

    @Column(name = "PORT")
    private String port;

    @Column(name = "SECID")
    private String secid;

    @Column(name = "PRINAMT")
    private BigDecimal prinamt;

    @Column(name = "QTY")
    private BigDecimal qty;

    @Column(name = "\"STATUS\"")
    private String status;

    @Column(name = "WYDATE")
    private Date wydate;

    @Column(name = "AMONT")
    private BigDecimal amont;

    @Column(name = "INT")
    private BigDecimal INT;

    @Column(name = "PINT")
    private BigDecimal pint;

    @Column(name = "FLAG")
    private String flag;

    @Column(name = "FLAG2")
    private String flag2;

    @Column(name = "INPUTDATE")
    private Date inputdate;

    /**
     * @return BR
     */
    public String getBr() {
        return br;
    }

    /**
     * @param br
     */
    public void setBr(String br) {
        this.br = br;
    }

    /**
     * @return COST
     */
    public String getCost() {
        return cost;
    }

    /**
     * @param cost
     */
    public void setCost(String cost) {
        this.cost = cost;
    }

    /**
     * @return CCY
     */
    public String getCcy() {
        return ccy;
    }

    /**
     * @param ccy
     */
    public void setCcy(String ccy) {
        this.ccy = ccy;
    }

    /**
     * @return INVTYPE
     */
    public String getInvtype() {
        return invtype;
    }

    /**
     * @param invtype
     */
    public void setInvtype(String invtype) {
        this.invtype = invtype;
    }

    /**
     * @return PORT
     */
    public String getPort() {
        return port;
    }

    /**
     * @param port
     */
    public void setPort(String port) {
        this.port = port;
    }

    /**
     * @return SECID
     */
    public String getSecid() {
        return secid;
    }

    /**
     * @param secid
     */
    public void setSecid(String secid) {
        this.secid = secid;
    }

    /**
     * @return PRINAMT
     */
    public BigDecimal getPrinamt() {
        return prinamt;
    }

    /**
     * @param prinamt
     */
    public void setPrinamt(BigDecimal prinamt) {
        this.prinamt = prinamt;
    }

    /**
     * @return QTY
     */
    public BigDecimal getQty() {
        return qty;
    }

    /**
     * @param qty
     */
    public void setQty(BigDecimal qty) {
        this.qty = qty;
    }

    /**
     * @return STATUS
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return WYDATE
     */
    public Date getWydate() {
        return wydate;
    }

    /**
     * @param wydate
     */
    public void setWydate(Date wydate) {
        this.wydate = wydate;
    }

    /**
     * @return AMONT
     */
    public BigDecimal getAmont() {
        return amont;
    }

    /**
     * @param amont
     */
    public void setAmont(BigDecimal amont) {
        this.amont = amont;
    }

    /**
     * @return INT
     */
    public BigDecimal getInt() {
        return INT;
    }

    /**
     * @param INT
     */
    public void setInt(BigDecimal INT) {
        this.INT = INT;
    }

    /**
     * @return PINT
     */
    public BigDecimal getPint() {
        return pint;
    }

    /**
     * @param pint
     */
    public void setPint(BigDecimal pint) {
        this.pint = pint;
    }

    /**
     * @return FLAG
     */
    public String getFlag() {
        return flag;
    }

    /**
     * @param flag
     */
    public void setFlag(String flag) {
        this.flag = flag;
    }

    /**
     * @return FLAG2
     */
    public String getFlag2() {
        return flag2;
    }

    /**
     * @param flag2
     */
    public void setFlag2(String flag2) {
        this.flag2 = flag2;
    }

    /**
     * @return INPUTDATE
     */
    public Date getInputdate() {
        return inputdate;
    }

    /**
     * @param inputdate
     */
    public void setInputdate(Date inputdate) {
        this.inputdate = inputdate;
    }
}
