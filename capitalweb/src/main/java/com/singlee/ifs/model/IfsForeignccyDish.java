package com.singlee.ifs.model;

import java.io.Serializable;

public class IfsForeignccyDish  implements Serializable {

	/**
	 * 总行外币平盘数据查询接口返回字段
	 */
	private static final long serialVersionUID = 1L;
	
	private String foreignccyId;  //主键ID
	private String busType;  //业务类型(1-结售汇  2-外汇买卖)
	private String bankTellerNo;  //柜员流水
	private String dealStatus;  //交易状态(0-正常 1-冲销)
	private String orgNo;  //机构号
	private String dealDate;  //成交日期
	private String vDate;  //起息日期
	private String currencyPair;  //货币对
	private String direction;  //买卖方向
	private String ccy1Amt;  //币种1金额
	private String ccy2Amt;  //币种2金额
	private String setOrgNo;  //创建机构号
	private String setPersonNo;  //创建柜员号
	private String note;  //备注
	private String ccy1;  //第一币种
	private String ccy2;  //第二币种
	private String syncStatus;  //同步opics状态，0-未同步，1-已同步
	private String fedealno;	//导入opics流水号
	private String statcode;	//opics处理状态
	private String errorcode;	//返回错误码
	private String dealno;	//opics生成编号
	
	
	public String getForeignccyId() {
		return foreignccyId;
	}
	public void setForeignccyId(String foreignccyId) {
		this.foreignccyId = foreignccyId;
	}
	public String getBusType() {
		return busType;
	}
	public void setBusType(String busType) {
		this.busType = busType;
	}
	public String getBankTellerNo() {
		return bankTellerNo;
	}
	public void setBankTellerNo(String bankTellerNo) {
		this.bankTellerNo = bankTellerNo;
	}
	public String getDealStatus() {
		return dealStatus;
	}
	public void setDealStatus(String dealStatus) {
		this.dealStatus = dealStatus;
	}
	public String getOrgNo() {
		return orgNo;
	}
	public void setOrgNo(String orgNo) {
		this.orgNo = orgNo;
	}
	public String getDealDate() {
		return dealDate;
	}
	public void setDealDate(String dealDate) {
		this.dealDate = dealDate;
	}
	public String getvDate() {
		return vDate;
	}
	public void setvDate(String vDate) {
		this.vDate = vDate;
	}
	public String getCurrencyPair() {
		return currencyPair;
	}
	public void setCurrencyPair(String currencyPair) {
		this.currencyPair = currencyPair;
	}
	public String getDirection() {
		return direction;
	}
	public void setDirection(String direction) {
		this.direction = direction;
	}
	public String getCcy1Amt() {
		return ccy1Amt;
	}
	public void setCcy1Amt(String ccy1Amt) {
		this.ccy1Amt = ccy1Amt;
	}
	public String getCcy2Amt() {
		return ccy2Amt;
	}
	public void setCcy2Amt(String ccy2Amt) {
		this.ccy2Amt = ccy2Amt;
	}
	public String getSetOrgNo() {
		return setOrgNo;
	}
	public void setSetOrgNo(String setOrgNo) {
		this.setOrgNo = setOrgNo;
	}
	public String getSetPersonNo() {
		return setPersonNo;
	}
	public void setSetPersonNo(String setPersonNo) {
		this.setPersonNo = setPersonNo;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	public String getCcy1() {
		return ccy1;
	}
	public void setCcy1(String ccy1) {
		this.ccy1 = ccy1;
	}
	public String getCcy2() {
		return ccy2;
	}
	public void setCcy2(String ccy2) {
		this.ccy2 = ccy2;
	}
	public String getSyncStatus() {
		return syncStatus;
	}
	public void setSyncStatus(String syncStatus) {
		this.syncStatus = syncStatus;
	}
	public String getStatcode() {
		return statcode;
	}
	public void setStatcode(String statcode) {
		this.statcode = statcode;
	}
	public String getErrorcode() {
		return errorcode;
	}
	public void setErrorcode(String errorcode) {
		this.errorcode = errorcode;
	}
	public String getDealno() {
		return dealno;
	}
	public void setDealno(String dealno) {
		this.dealno = dealno;
	}
	public void setFedealno(String fedealno) {
		this.fedealno = fedealno;
	}
	public String getFedealno() {
		return fedealno;
	}
	@Override
	public String toString() {
		return "IfsForeignccyDish [foreignccyId=" + foreignccyId + ", busType=" + busType + ", bankTellerNo=" + bankTellerNo + ", dealStatus=" + dealStatus + ", orgNo=" + orgNo + ", dealDate=" + dealDate + ", vDate=" + vDate + ", currencyPair="
				+ currencyPair + ", direction=" + direction + ", ccy1Amt=" + ccy1Amt + ", ccy2Amt=" + ccy2Amt + ", setOrgNo=" + setOrgNo + ", setPersonNo=" + setPersonNo + ", note=" + note + ", ccy1=" + ccy1 + ", ccy2=" + ccy2 + ", syncStatus="
				+ syncStatus + "]";
	}
	
	
}
	
	
	
