package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

@Entity
@Table(name = "IFS_COREPOSITION")
public class IfsCorePositionBean implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 前台交易号
	 */
	private String fedealNo;

	/**
	 * 第一币种
	 */
	private String ccy;

	/**
	 * 第二币种
	 */
	private String ctrCcy;

	/**
	 * 第一金额
	 */
	private String ccyAmt;

	/**
	 * 第二金额
	 */
	private String ctrAmt;

	/**
	 * 交易类型
	 */
	private String dealtype;

	/**
	 * 买卖方向
	 */
	private String psFlag;

	/**
	 * 交易日
	 */
	private String dealDate;

	/**
	 * 交割日
	 */
	private String vdate;

	/**
	 * 汇率
	 */
	private String ccyRate;

	/**
	 * 系统来源
	 */
	private String source;

	/**
	 * 导入状态
	 */
	private String status;

	/**
	 * 交易员
	 */
	private String trad;

	/**
	 * 成本中心
	 */
	private String cost;

	/**
	 * 投资组合
	 */
	private String port;

	/**
	 * 客户号
	 */
	private String cust;

	/**
	 * Server
	 */
	private String server;

	/**
	 * 更新日期
	 */
	private Timestamp updateDate;

	/**
	 * OPICS跑批日期
	 */
	private Date opicsDate;

	/**
	 * 交易摘要
	 */
	private String tranbrief;

	/**
	 * 是否冲销
	 */
	private String revFlag;

	/**
	 * 备注
	 */
	private String memo6;

	/**
	 * 区分
	 */
	private String flag;
	
	/**
     * 实时平盘转出汇率 RL_TIME_FROM_RATE
     */
    private String rlTimeFromRate;
    
    
    /**
     * 实时平盘转入汇率 RL_TIME_TO_RATE
     */
    private String rlTimeToRate;

	public String getFedealNo() {
		return fedealNo;
	}

	public void setFedealNo(String fedealNo) {
		this.fedealNo = fedealNo;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getCtrCcy() {
		return ctrCcy;
	}

	public void setCtrCcy(String ctrCcy) {
		this.ctrCcy = ctrCcy;
	}

	public String getCcyAmt() {
		return ccyAmt;
	}

	public void setCcyAmt(String ccyAmt) {
		this.ccyAmt = ccyAmt;
	}

	public String getCtrAmt() {
		return ctrAmt;
	}

	public void setCtrAmt(String ctrAmt) {
		this.ctrAmt = ctrAmt;
	}

	public String getDealtype() {
		return dealtype;
	}

	public void setDealtype(String dealtype) {
		this.dealtype = dealtype;
	}

	public String getPsFlag() {
		return psFlag;
	}

	public void setPsFlag(String psFlag) {
		this.psFlag = psFlag;
	}

	public String getDealDate() {
		return dealDate;
	}

	public void setDealDate(String dealDate) {
		this.dealDate = dealDate;
	}

	public String getVdate() {
		return vdate;
	}

	public void setVdate(String vdate) {
		this.vdate = vdate;
	}

	public String getCcyRate() {
		return ccyRate;
	}

	public void setCcyRate(String ccyRate) {
		this.ccyRate = ccyRate;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTrad() {
		return trad;
	}

	public void setTrad(String trad) {
		this.trad = trad;
	}

	public String getCost() {
		return cost;
	}

	public void setCost(String cost) {
		this.cost = cost;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getCust() {
		return cust;
	}

	public void setCust(String cust) {
		this.cust = cust;
	}

	public String getServer() {
		return server;
	}

	public void setServer(String server) {
		this.server = server;
	}

	public Timestamp getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Timestamp updateDate) {
		this.updateDate = updateDate;
	}

	public Date getOpicsDate() {
		return opicsDate;
	}

	public void setOpicsDate(Date opicsDate) {
		this.opicsDate = opicsDate;
	}

	public String getTranbrief() {
		return tranbrief;
	}

	public void setTranbrief(String tranbrief) {
		this.tranbrief = tranbrief;
	}

	public String getRevFlag() {
		return revFlag;
	}

	public void setRevFlag(String revFlag) {
		this.revFlag = revFlag;
	}

	public String getMemo6() {
		return memo6;
	}

	public void setMemo6(String memo6) {
		this.memo6 = memo6;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

    public String getRlTimeFromRate() {
        return rlTimeFromRate;
    }

    public void setRlTimeFromRate(String rlTimeFromRate) {
        this.rlTimeFromRate = rlTimeFromRate;
    }

    public String getRlTimeToRate() {
        return rlTimeToRate;
    }

    public void setRlTimeToRate(String rlTimeToRate) {
        this.rlTimeToRate = rlTimeToRate;
    }

}
