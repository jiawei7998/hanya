package com.singlee.ifs.model;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 质押债券明细
 *
 * @author zhangcm
 * @date 2018-07-19
 */
public class IfsCfetsrmbDetailSl implements Serializable {

	private static final long serialVersionUID = 1L;
	private String ticketId;
	private String marginSecuritiesId;
	private String marginSymbol;
	private BigDecimal marginAmt;
	private BigDecimal marginReplacement;
	private String invtype;
	private String costb;
	private String portb;
	private String dealNo;
	private String statcode;
	private String errorcode;
	private String seq;
	//产品
	private String prodb;
	//产品类型
	private String prodtypeb;


	/**
	 * 发行规模
	 */
	private BigDecimal issuanceScale;
	/**
	 * 评级结果
	 */
	private String bondrating;
	private BigDecimal marginRatio;

	public BigDecimal getIssuanceScale() {
		return issuanceScale;
	}

	public void setIssuanceScale(BigDecimal issuanceScale) {
		this.issuanceScale = issuanceScale;
	}

	public String getBondrating() {
		return bondrating;
	}

	public void setBondrating(String bondrating) {
		this.bondrating = bondrating;
	}

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId == null ? null : ticketId.trim();
	}

	public String getInvtype() {
		return invtype;
	}

	public void setInvtype(String invtype) {
		this.invtype = invtype;
	}

	public String getCostb() {
		return costb;
	}

	public void setCostb(String costb) {
		this.costb = costb;
	}

	public String getPortb() {
		return portb;
	}

	public void setPortb(String portb) {
		this.portb = portb;
	}

	public String getMarginSecuritiesId() {
		return marginSecuritiesId;
	}

	public void setMarginSecuritiesId(String marginSecuritiesId) {
		this.marginSecuritiesId = marginSecuritiesId == null ? null : marginSecuritiesId.trim();
	}

	public String getMarginSymbol() {
		return marginSymbol;
	}

	public void setMarginSymbol(String marginSymbol) {
		this.marginSymbol = marginSymbol == null ? null : marginSymbol.trim();
	}

	public BigDecimal getMarginAmt() {
		return marginAmt;
	}

	public void setMarginAmt(BigDecimal marginAmt) {
		this.marginAmt = marginAmt;
	}

	public BigDecimal getMarginReplacement() {
		return marginReplacement;
	}

	public void setMarginReplacement(BigDecimal marginReplacement) {
		this.marginReplacement = marginReplacement;
	}

	public String getDealNo() {
		return dealNo;
	}

	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}

	public String getStatcode() {
		return statcode;
	}

	public void setStatcode(String statcode) {
		this.statcode = statcode;
	}

	public String getErrorcode() {
		return errorcode;
	}

	public void setErrorcode(String errorcode) {
		this.errorcode = errorcode;
	}

	public String getSeq() {
		return seq;
	}

	public void setSeq(String seq) {
		this.seq = seq;
	}

	public String getProdb() {
		return prodb;
	}

	public void setProdb(String prodb) {
		this.prodb = prodb;
	}

	public String getProdtypeb() {
		return prodtypeb;
	}

	public void setProdtypeb(String prodtypeb) {
		this.prodtypeb = prodtypeb;
	}

	public BigDecimal getMarginRatio() {
		return marginRatio;
	}

	public void setMarginRatio(BigDecimal marginRatio) {
		this.marginRatio = marginRatio;
	}
}