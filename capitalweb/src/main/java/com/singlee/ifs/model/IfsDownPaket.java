package com.singlee.ifs.model;

import java.io.Serializable;
import java.util.Date;

public class IfsDownPaket implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3605757028600911574L;
	private String paketId;
	// 成交单传输状态10105
	private String paketStatus;
	private Date paketDate;
	private String paketMsg;
	private String paketChannel;

	public String getPaketId() {
		return paketId;
	}

	public void setPaketId(String paketId) {
		this.paketId = paketId;
	}

	public String getPaketStatus() {
		return paketStatus;
	}

	public void setPaketStatus(String paketStatus) {
		this.paketStatus = paketStatus;
	}

	public Date getPaketDate() {
		return paketDate;
	}

	public void setPaketDate(Date paketDate) {
		this.paketDate = paketDate;
	}

	public String getPaketMsg() {
		return paketMsg;
	}

	public void setPaketMsg(String paketMsg) {
		this.paketMsg = paketMsg;
	}

	public String getPaketChannel() {
		return paketChannel;
	}

	public void setPaketChannel(String paketChannel) {
		this.paketChannel = paketChannel;
	}

}
