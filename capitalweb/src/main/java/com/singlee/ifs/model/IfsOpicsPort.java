package com.singlee.ifs.model;

import com.singlee.capital.common.util.TreeInterface;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.Transient;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/***
 * opics 投资组合参数表
 *
 */

public class IfsOpicsPort implements Serializable, TreeInterface{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 投资组合ID
	 */
	private String portid;
	/**
	 * 投资组合父ID
	 */
	private String portpid;
	/**
	 * 部门
	 */
	private String br;
	/**
	 * 投资组合
	 */
	private String portfolio;
	/**
	 * 投资组合描述
	 */
	private String portdesc;
	/**
	 * 投资组合中文描述
	 */
	private String portdesccn;
	/**
	 * 最后修改时间
	 */
	private Date lstmntdte;
	/**
	 * 成本中心
	 */
	private String cost;
	/**
	 * 备注1
	 */
	private String text1;
	/**
	 * 备注2
	 */
	private String text2;
	/**
	 * 日期1
	 */
	private Date date1;
	/**
	 * 日期2
	 */
	private Date date2;
	/**
	 * 金额1
	 */
	private BigDecimal amount1;
	/**
	 * 金额2
	 */
	private BigDecimal amount2;
	/**
	 * 更改次数
	 */
	private String updatecounter;
	/**
	 * 操作人
	 */
	private String operator;
	/**
	 * 处理状态
	 */
	private String status;
	/**
	 * 备注
	 */
	private String remark;
	@Transient
	private List<IfsOpicsPort> children;
	
	public String getPortid() {
		return portid;
	}
	public void setPortid(String portid) {
		this.portid = portid;
	}
	public String getPortpid() {
		return portpid;
	}
	public void setPortpid(String portpid) {
		this.portpid = portpid;
	}
	public String getBr() {
		return br;
	}
	public void setBr(String br) {
		this.br = br;
	}
	public String getPortfolio() {
		return portfolio;
	}
	public void setPortfolio(String portfolio) {
		this.portfolio = portfolio;
	}
	public String getPortdesc() {
		return portdesc;
	}
	public void setPortdesc(String portdesc) {
		this.portdesc = portdesc;
	}
	public Date getLstmntdte() {
		return lstmntdte;
	}
	public void setLstmntdte(Date lstmntdte) {
		this.lstmntdte = lstmntdte;
	}
	public String getCost() {
		return cost;
	}
	public void setCost(String cost) {
		this.cost = cost;
	}
	public String getText1() {
		return text1;
	}
	public void setText1(String text1) {
		this.text1 = text1;
	}
	public String getText2() {
		return text2;
	}
	public void setText2(String text2) {
		this.text2 = text2;
	}
	public Date getDate1() {
		return date1;
	}
	public void setDate1(Date date1) {
		this.date1 = date1;
	}
	public Date getDate2() {
		return date2;
	}
	public void setDate2(Date date2) {
		this.date2 = date2;
	}
	public BigDecimal getAmount1() {
		return amount1;
	}
	public void setAmount1(BigDecimal amount1) {
		this.amount1 = amount1;
	}
	public BigDecimal getAmount2() {
		return amount2;
	}
	public void setAmount2(BigDecimal amount2) {
		this.amount2 = amount2;
	}
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	public List<IfsOpicsPort> getChildren() {
		return children;
	}
	public void setChildren(List<IfsOpicsPort> children) {
		this.children = children;
	}
	@Override
	public String getId() {
		return getPortid();
	}
	@Override
	public String getParentId() {
		return getPortpid();
	}
	@SuppressWarnings("unchecked")
	@Override
	public void children(List<? extends TreeInterface> list) {
		setChildren((List<IfsOpicsPort>) list);
		
	}
	@Override
	public List<? extends TreeInterface> children() {
		return getChildren();
	}
	@Override
	public String getText() {
		return portfolio;
	}
	@Override
	public String getValue() {
		return portpid;
	}
	@Override
	public int getSort() {
		return StringUtils.length(getPortid());
	}
	@Override
	public String getIconCls() {
		return "";
	}
	public String getPortdesccn() {
		return portdesccn;
	}
	public void setPortdesccn(String portdesccn) {
		this.portdesccn = portdesccn;
	}
	public String getUpdatecounter() {
		return updatecounter;
	}
	public void setUpdatecounter(String updatecounter) {
		this.updatecounter = updatecounter;
	}
	
}
