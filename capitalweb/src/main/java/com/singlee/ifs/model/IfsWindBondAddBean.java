package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "IFS_WIND_BOND_ADD")
public class IfsWindBondAddBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private String bondName;// 债券简称
	private String bondCode;// 债券代码
	private String bondCodeOld;// 原债券代码

	private BigDecimal faMount;// 首场发行量
	private BigDecimal zaMount;// 追加发行量
	private BigDecimal baMount;// 基本承销量
	private BigDecimal expense;// 债券承揽费

	private String zbType;// 招标方式

	private BigDecimal faceRate;// 票面利率
	private BigDecimal fxRate;// 发行利率(加权平均)
	private BigDecimal fxPrice;// 发行价格(加权平均)
	private BigDecimal fxBjRate;// 发行边际利率
	private BigDecimal fxBjPrice;// 发行边际价格
	private BigDecimal qctb;// 全场投标倍数
	private BigDecimal bjtb;// 边际投标倍数

	private String ifName;// 发行人
	private String zxLevel;// 债项评级
	private String dbName;// 担保人
	private String dbType;// 担保方式
	private String zbDate;// 招投标日期
	private String ifDate;// 发行公告日
	private String addInfo;// 投标期次
	private Date spotDate;

	public String getBondName() {
		return bondName;
	}

	public void setBondName(String bondName) {
		this.bondName = bondName;
	}

	public String getBondCode() {
		return bondCode;
	}

	public void setBondCode(String bondCode) {
		this.bondCode = bondCode;
	}

	public String getBondCodeOld() {
		return bondCodeOld;
	}

	public void setBondCodeOld(String bondCodeOld) {
		this.bondCodeOld = bondCodeOld;
	}

	public BigDecimal getFaMount() {
		return faMount;
	}

	public void setFaMount(BigDecimal faMount) {
		this.faMount = faMount;
	}

	public BigDecimal getZaMount() {
		return zaMount;
	}

	public void setZaMount(BigDecimal zaMount) {
		this.zaMount = zaMount;
	}

	public BigDecimal getBaMount() {
		return baMount;
	}

	public void setBaMount(BigDecimal baMount) {
		this.baMount = baMount;
	}

	public BigDecimal getExpense() {
		return expense;
	}

	public void setExpense(BigDecimal expense) {
		this.expense = expense;
	}

	public String getZbType() {
		return zbType;
	}

	public void setZbType(String zbType) {
		this.zbType = zbType;
	}

	public BigDecimal getFaceRate() {
		return faceRate;
	}

	public void setFaceRate(BigDecimal faceRate) {
		this.faceRate = faceRate;
	}

	public BigDecimal getFxRate() {
		return fxRate;
	}

	public void setFxRate(BigDecimal fxRate) {
		this.fxRate = fxRate;
	}

	public BigDecimal getFxPrice() {
		return fxPrice;
	}

	public void setFxPrice(BigDecimal fxPrice) {
		this.fxPrice = fxPrice;
	}

	public BigDecimal getFxBjRate() {
		return fxBjRate;
	}

	public void setFxBjRate(BigDecimal fxBjRate) {
		this.fxBjRate = fxBjRate;
	}

	public BigDecimal getFxBjPrice() {
		return fxBjPrice;
	}

	public void setFxBjPrice(BigDecimal fxBjPrice) {
		this.fxBjPrice = fxBjPrice;
	}

	public BigDecimal getQctb() {
		return qctb;
	}

	public void setQctb(BigDecimal qctb) {
		this.qctb = qctb;
	}

	public BigDecimal getBjtb() {
		return bjtb;
	}

	public void setBjtb(BigDecimal bjtb) {
		this.bjtb = bjtb;
	}

	public String getIfName() {
		return ifName;
	}

	public void setIfName(String ifName) {
		this.ifName = ifName;
	}

	public String getZxLevel() {
		return zxLevel;
	}

	public void setZxLevel(String zxLevel) {
		this.zxLevel = zxLevel;
	}

	public String getDbName() {
		return dbName;
	}

	public void setDbName(String dbName) {
		this.dbName = dbName;
	}

	public String getDbType() {
		return dbType;
	}

	public void setDbType(String dbType) {
		this.dbType = dbType;
	}

	public String getZbDate() {
		return zbDate;
	}

	public void setZbDate(String zbDate) {
		this.zbDate = zbDate;
	}

	public String getIfDate() {
		return ifDate;
	}

	public void setIfDate(String ifDate) {
		this.ifDate = ifDate;
	}

	public String getAddInfo() {
		return addInfo;
	}

	public void setAddInfo(String addInfo) {
		this.addInfo = addInfo;
	}

	public Date getSpotDate() {
		return spotDate;
	}

	public void setSpotDate(Date spotDate) {
		this.spotDate = spotDate;
	}

}
