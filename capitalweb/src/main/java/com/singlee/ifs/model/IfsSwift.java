package com.singlee.ifs.model;

import java.io.Serializable;
import java.util.Date;

/**
 * 	SWIFT清算
 * @author tlcb
 */
public class IfsSwift implements Serializable {
	
	private static final long serialVersionUID = -1714511788400565703L;

	private String br; // 部门

	private String dealNo; // 交易编号

	private String batchNo; // 流水号

	private String prodcode; // 产品代码

	private Date cDate; // 处理日期

	private String msgType; // 报文类型

	private String msg; // 报文内容

	private String leng;

	private String prodType; // 产品类型

	private Date swftcDate; // 报文处理日期

	private String iOper; // 操作员

	private String iDate;

	private String verOper;

	private String verind;

	private String verDate;

	private String vDate;

	private String fileurl;

	private String lstmntDate;

	private String settFlag; // 清算状态

	private String returncode;

	private String returnmsg;

	private String seqNo;

	private String swiftStatus;

	private String swiftMsg;

	private String settway; // 清算方式
	
	//删除操作员
	private String oper;
	
	//删除时间
	private String operDate;
	
	//复核操作员
	private String verifyOper;
	
	//复核时间
	private String verifyOperDate;
	
	private String dealDate; // 报文处理日期(发送的日期)

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getDealNo() {
		return dealNo;
	}

	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}

	public String getBatchNo() {
		return batchNo;
	}

	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}

	public String getProdcode() {
		return prodcode;
	}

	public void setProdcode(String prodcode) {
		this.prodcode = prodcode;
	}

	public Date getcDate() {
		return cDate;
	}

	public void setcDate(Date cDate) {
		this.cDate = cDate;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getLeng() {
		return leng;
	}

	public void setLeng(String leng) {
		this.leng = leng;
	}

	public String getProdType() {
		return prodType;
	}

	public void setProdType(String prodType) {
		this.prodType = prodType;
	}

	public Date getSwftcDate() {
		return swftcDate;
	}

	public void setSwftcDate(Date swftcDate) {
		this.swftcDate = swftcDate;
	}

	public String getiOper() {
		return iOper;
	}

	public void setiOper(String iOper) {
		this.iOper = iOper;
	}

	public String getiDate() {
		return iDate;
	}

	public void setiDate(String iDate) {
		this.iDate = iDate;
	}

	public String getVerOper() {
		return verOper;
	}

	public void setVerOper(String verOper) {
		this.verOper = verOper;
	}

	public String getVerind() {
		return verind;
	}

	public void setVerind(String verind) {
		this.verind = verind;
	}

	public String getVerDate() {
		return verDate;
	}

	public void setVerDate(String verDate) {
		this.verDate = verDate;
	}

	public String getvDate() {
		return vDate;
	}

	public void setvDate(String vDate) {
		this.vDate = vDate;
	}

	public String getFileurl() {
		return fileurl;
	}

	public void setFileurl(String fileurl) {
		this.fileurl = fileurl;
	}

	public String getLstmntDate() {
		return lstmntDate;
	}

	public void setLstmntDate(String lstmntDate) {
		this.lstmntDate = lstmntDate;
	}

	public String getSettFlag() {
		return settFlag;
	}

	public void setSettFlag(String settFlag) {
		this.settFlag = settFlag;
	}

	public String getReturncode() {
		return returncode;
	}

	public void setReturncode(String returncode) {
		this.returncode = returncode;
	}

	public String getReturnmsg() {
		return returnmsg;
	}

	public void setReturnmsg(String returnmsg) {
		this.returnmsg = returnmsg;
	}

	public String getSeqNo() {
		return seqNo;
	}

	public void setSeqNo(String seqNo) {
		this.seqNo = seqNo;
	}

	public String getSwiftStatus() {
		return swiftStatus;
	}

	public void setSwiftStatus(String swiftStatus) {
		this.swiftStatus = swiftStatus;
	}

	public String getSwiftMsg() {
		return swiftMsg;
	}

	public void setSwiftMsg(String swiftMsg) {
		this.swiftMsg = swiftMsg;
	}

	public String getSettway() {
		return settway;
	}

	public void setSettway(String settway) {
		this.settway = settway;
	}

	public String getOper() {
		return oper;
	}

	public void setOper(String oper) {
		this.oper = oper;
	}

	public String getOperDate() {
		return operDate;
	}

	public void setOperDate(String operDate) {
		this.operDate = operDate;
	}

	public String getVerifyOper() {
		return verifyOper;
	}

	public void setVerifyOper(String verifyOper) {
		this.verifyOper = verifyOper;
	}

	public String getVerifyOperDate() {
		return verifyOperDate;
	}

	public void setVerifyOperDate(String verifyOperDate) {
		this.verifyOperDate = verifyOperDate;
	}

	public String getDealDate() {
		return dealDate;
	}

	public void setDealDate(String dealDate) {
		this.dealDate = dealDate;
	}
}