package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
@Entity
@Table(name = "IFS_CFETSFX_ONSPOT")
public class IfsCfetsfxOnspot extends FxCurrenyBasebean  implements Serializable {
	private static final long serialVersionUID = 1L;
	
//	private String ticketId;
	//交易单号
//	private String tradeDate;
	//成交日期
//	private String dealTransType;
	//交易状态
	//private String myInstitution;
	//本方机构
//	private String counterparty;
	//对方机构
//	private String dealer;
	//本方交易员
	//private String cpDealer;
	//对方交易员
//	private String forDate;
	//日期
//	private String forTime;
	//时间
//	private String tradingModel;
	//交易模式
//	private String tradingType;
	//交易方式
//	private String currencyPair;
	//货币对
	private String price;
	//价格
	private String capital;
	//本金
	private String settlementAmount;
	//结算金额
	private String direction;
	//买卖方向
	private String valueDate;
	//起息日
//	private String sellCurreny;
	//清算货币
//	private String sellBankName;bankName1
	//开户行名称
//	private String sellBankBic;bankBic1
	//开户行BIC CODE
//	private String sellDueBank;dueBank1
	//收款行名称
//	private String sellDueBankname;dueBankName1
	//收款行名称BIC CODE
//	private String sellDueBankaccount;dueBankAccount1
	//收款行账号
//	private String buyCurreny;
	//清算货币
//	private String buyBankName;
	//开户行名称
//	private String buyBankBic;
	//开户行BIC CODE
//	private String buyDueBank;
	//收款行名称
//	private String buyDueBankname;
	//收款行名称BIC CODE
//	private String buyDueBankaccount;
	//收款行账号
	//权重
	private String weight;
	//占用授信主体
	private String custNo;
	//审批单号
	private String applyNo;
	//产品名称
	private String applyProd;
	
	//DV01Risk
	private String DV01Risk;
	
	//止损
	private String lossLimit;
	
	//久期限
	private String longLimit;
	
	//风险程度
	private String riskDegree;
	@Transient
	private String  prdNo;
	
	
	public String getPrdNo() {
		return prdNo;
	}

	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}

	public String getCapital() {
		return capital;
	}

	public void setCapital(String capital) {
		this.capital = capital;
	}

	public String getSettlementAmount() {
		return settlementAmount;
	}

	public void setSettlementAmount(String settlementAmount) {
		this.settlementAmount = settlementAmount;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price == null ? null : price.trim();
	}

	public String getValueDate() {
		return valueDate;
	}

	public void setValueDate(String valueDate) {
		this.valueDate = valueDate == null ? null : valueDate.trim();
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	@Override
    public String getCustNo() {
		return custNo;
	}

	@Override
    public void setCustNo(String custNo) {
		this.custNo = custNo;
	}

	public String getApplyNo() {
		return applyNo;
	}

	public void setApplyNo(String applyNo) {
		this.applyNo = applyNo;
	}

	public String getApplyProd() {
		return applyProd;
	}

	public void setApplyProd(String applyProd) {
		this.applyProd = applyProd;
	}
	public String getDV01Risk() {
		return DV01Risk;
	}

	public void setDV01Risk(String dV01Risk) {
		this.DV01Risk = dV01Risk;
	}

	public String getLossLimit() {
		return lossLimit;
	}

	public void setLossLimit(String lossLimit) {
		this.lossLimit = lossLimit;
	}

	public String getLongLimit() {
		return longLimit;
	}

	public void setLongLimit(String longLimit) {
		this.longLimit = longLimit;
	}

	public String getRiskDegree() {
		return riskDegree;
	}

	public void setRiskDegree(String riskDegree) {
		this.riskDegree = riskDegree;
	}

}