package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "IFS_CDTC_CNBOND_PKG")
public class IfsCdtcCnBondPkg implements Serializable{
	private String msgSn;
	private String msgFileName;
	private String msgType;
	private String msgXml;
	private Date msgTime;
	private Date msgOpdt;
	private String msgFedealno;
	public String getMsgSn() {
		return msgSn;
	}
	public void setMsgSn(String msgSn) {
		this.msgSn = msgSn;
	}
	public String getMsgFileName() {
		return msgFileName;
	}
	public void setMsgFileName(String msgFileName) {
		this.msgFileName = msgFileName;
	}
	public String getMsgType() {
		return msgType;
	}
	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}
	public String getMsgXml() {
		return msgXml;
	}
	public void setMsgXml(String msgXml) {
		this.msgXml = msgXml;
	}
	public Date getMsgTime() {
		return msgTime;
	}
	public void setMsgTime(Date msgTime) {
		this.msgTime = msgTime;
	}
	public Date getMsgOpdt() {
		return msgOpdt;
	}
	public void setMsgOpdt(Date msgOpdt) {
		this.msgOpdt = msgOpdt;
	}
	public String getMsgFedealno() {
		return msgFedealno;
	}
	public void setMsgFedealno(String msgFedealno) {
		this.msgFedealno = msgFedealno;
	}
	
	
}
