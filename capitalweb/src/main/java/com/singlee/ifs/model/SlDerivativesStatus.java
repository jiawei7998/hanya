package com.singlee.ifs.model;

import java.io.Serializable;

public class SlDerivativesStatus implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String dealno;
	private String seq;
	private String choisRefNo;
	private String choisHisNo;
	private String sendtime;
	private String issend;
	private String isresend;
	private String sendresult;
	private String errmsg;
	private String sendmsg;
	private String tablename;
	private String product;
	private String type;
	private String upcount;
	private String upown;
	private String uptime;
	private String inputtime;
	private String reversal;
	private String schdseq;
	private String verautflag;
	public String getDealno() {
		return dealno;
	}
	public void setDealno(String dealno) {
		this.dealno = dealno;
	}
	public String getSeq() {
		return seq;
	}
	public void setSeq(String seq) {
		this.seq = seq;
	}
	public String getChoisRefNo() {
		return choisRefNo;
	}
	public void setChoisRefNo(String choisRefNo) {
		this.choisRefNo = choisRefNo;
	}
	public String getChoisHisNo() {
		return choisHisNo;
	}
	public void setChoisHisNo(String choisHisNo) {
		this.choisHisNo = choisHisNo;
	}
	public String getSendtime() {
		return sendtime;
	}
	public void setSendtime(String sendtime) {
		this.sendtime = sendtime;
	}
	public String getIssend() {
		return issend;
	}
	public void setIssend(String issend) {
		this.issend = issend;
	}
	public String getIsresend() {
		return isresend;
	}
	public void setIsresend(String isresend) {
		this.isresend = isresend;
	}
	public String getSendresult() {
		return sendresult;
	}
	public void setSendresult(String sendresult) {
		this.sendresult = sendresult;
	}
	public String getErrmsg() {
		return errmsg;
	}
	public void setErrmsg(String errmsg) {
		this.errmsg = errmsg;
	}
	public String getSendmsg() {
		return sendmsg;
	}
	public void setSendmsg(String sendmsg) {
		this.sendmsg = sendmsg;
	}
	public String getTablename() {
		return tablename;
	}
	public void setTablename(String tablename) {
		this.tablename = tablename;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getUpcount() {
		return upcount;
	}
	public void setUpcount(String upcount) {
		this.upcount = upcount;
	}
	public String getUpown() {
		return upown;
	}
	public void setUpown(String upown) {
		this.upown = upown;
	}
	public String getUptime() {
		return uptime;
	}
	public void setUptime(String uptime) {
		this.uptime = uptime;
	}
	public String getInputtime() {
		return inputtime;
	}
	public void setInputtime(String inputtime) {
		this.inputtime = inputtime;
	}
	public String getReversal() {
		return reversal;
	}
	public void setReversal(String reversal) {
		this.reversal = reversal;
	}
	public String getSchdseq() {
		return schdseq;
	}
	public void setSchdseq(String schdseq) {
		this.schdseq = schdseq;
	}
	public String getVerautflag() {
		return verautflag;
	}
	public void setVerautflag(String verautflag) {
		this.verautflag = verautflag;
	}
	
}
