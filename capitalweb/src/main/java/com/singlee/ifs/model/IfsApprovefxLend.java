package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "IFS_APPROVEFX_LEND")
public class IfsApprovefxLend extends IfsApproveBaseBean  implements Serializable {
	private static final long serialVersionUID = 1L;
	private String currency;
	//货币
	private String rate;
	//拆借利率
	private String valueDate;
	//起息日
	private String occupancyDays;
	//实际占款天数
	private String amount;
	//拆借金额
	private String basis;
	//计息基准
	private String tenor;
	//期限
	private String maturityDate;
	//到期日
	

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency == null ? null : currency.trim();
	}

	public String getRate() {
		return rate;
	}

	public void setRate(String rate) {
		this.rate = rate;
	}

	public String getValueDate() {
		return valueDate;
	}

	public void setValueDate(String valueDate) {
		this.valueDate = valueDate == null ? null : valueDate.trim();
	}


	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount == null ? null : amount.trim();
	}

	public String getBasis() {
		return basis;
	}

	public void setBasis(String basis) {
		this.basis = basis == null ? null : basis.trim();
	}

	public String getTenor() {
		return tenor;
	}

	public void setTenor(String tenor) {
		this.tenor = tenor == null ? null : tenor.trim();
	}

	public String getMaturityDate() {
		return maturityDate;
	}

	public void setMaturityDate(String maturityDate) {
		this.maturityDate = maturityDate == null ? null : maturityDate.trim();
	}

	public String getOccupancyDays() {
		return occupancyDays;
	}

	public void setOccupancyDays(String occupancyDays) {
		this.occupancyDays = occupancyDays;
	}
}