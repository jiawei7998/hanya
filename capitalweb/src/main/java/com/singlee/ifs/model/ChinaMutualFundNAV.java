package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "CM_FUND_NAV")
public class ChinaMutualFundNAV implements Serializable {
	private static final long serialVersionUID = 1L;

	    private String OBJECT_ID;
	    private String F_INFO_WINDCODE;
	    private String ANN_DATE;
	    private String PRICE_DATE;
	    private BigDecimal F_NAV_UNIT;
	    private BigDecimal F_NAV_ACCUMULATED;
	    private BigDecimal F_NAV_DIVACCUMULATED;
	    private BigDecimal F_NAV_ADJFACTOR;
	    private String CRNCY_CODE;
	    private BigDecimal F_PRT_NETASSET;
	    private Integer F_ASSET_MERGEDSHARESORNOT;
	    private BigDecimal NETASSET_TOTAL;
	    private BigDecimal F_NAV_ADJUSTED;
	    private Integer IS_EXDIVIDENDDATE;
	    private BigDecimal F_NAV_DISTRIBUTION;
	    private String S_INFO_ASHARECODE;
	    private String OPDATE;
	    private String OPMODE;//0（新记录）、1（更新 /更正）、2（删除）
	    private String MOPDATE;
        public String getOBJECT_ID() {
            return OBJECT_ID;
        }
        public void setOBJECT_ID(String oBJECT_ID) {
            OBJECT_ID = oBJECT_ID;
        }
        public String getF_INFO_WINDCODE() {
            return F_INFO_WINDCODE;
        }
        public void setF_INFO_WINDCODE(String f_INFO_WINDCODE) {
            F_INFO_WINDCODE = f_INFO_WINDCODE;
        }
        public String getANN_DATE() {
            return ANN_DATE;
        }
        public void setANN_DATE(String aNN_DATE) {
            ANN_DATE = aNN_DATE;
        }
        public String getPRICE_DATE() {
            return PRICE_DATE;
        }
        public void setPRICE_DATE(String pRICE_DATE) {
            PRICE_DATE = pRICE_DATE;
        }
        public BigDecimal getF_NAV_UNIT() {
            return F_NAV_UNIT;
        }
        public void setF_NAV_UNIT(BigDecimal f_NAV_UNIT) {
            F_NAV_UNIT = f_NAV_UNIT;
        }
        public BigDecimal getF_NAV_ACCUMULATED() {
            return F_NAV_ACCUMULATED;
        }
        public void setF_NAV_ACCUMULATED(BigDecimal f_NAV_ACCUMULATED) {
            F_NAV_ACCUMULATED = f_NAV_ACCUMULATED;
        }
        public BigDecimal getF_NAV_DIVACCUMULATED() {
            return F_NAV_DIVACCUMULATED;
        }
        public void setF_NAV_DIVACCUMULATED(BigDecimal f_NAV_DIVACCUMULATED) {
            F_NAV_DIVACCUMULATED = f_NAV_DIVACCUMULATED;
        }
        public BigDecimal getF_NAV_ADJFACTOR() {
            return F_NAV_ADJFACTOR;
        }
        public void setF_NAV_ADJFACTOR(BigDecimal f_NAV_ADJFACTOR) {
            F_NAV_ADJFACTOR = f_NAV_ADJFACTOR;
        }
        public String getCRNCY_CODE() {
            return CRNCY_CODE;
        }
        public void setCRNCY_CODE(String cRNCY_CODE) {
            CRNCY_CODE = cRNCY_CODE;
        }
        public BigDecimal getF_PRT_NETASSET() {
            return F_PRT_NETASSET;
        }
        public void setF_PRT_NETASSET(BigDecimal f_PRT_NETASSET) {
            F_PRT_NETASSET = f_PRT_NETASSET;
        }
        public Integer getF_ASSET_MERGEDSHARESORNOT() {
            return F_ASSET_MERGEDSHARESORNOT;
        }
        public void setF_ASSET_MERGEDSHARESORNOT(Integer f_ASSET_MERGEDSHARESORNOT) {
            F_ASSET_MERGEDSHARESORNOT = f_ASSET_MERGEDSHARESORNOT;
        }
        public BigDecimal getNETASSET_TOTAL() {
            return NETASSET_TOTAL;
        }
        public void setNETASSET_TOTAL(BigDecimal nETASSET_TOTAL) {
            NETASSET_TOTAL = nETASSET_TOTAL;
        }
        public BigDecimal getF_NAV_ADJUSTED() {
            return F_NAV_ADJUSTED;
        }
        public void setF_NAV_ADJUSTED(BigDecimal f_NAV_ADJUSTED) {
            F_NAV_ADJUSTED = f_NAV_ADJUSTED;
        }
        public Integer getIS_EXDIVIDENDDATE() {
            return IS_EXDIVIDENDDATE;
        }
        public void setIS_EXDIVIDENDDATE(Integer iS_EXDIVIDENDDATE) {
            IS_EXDIVIDENDDATE = iS_EXDIVIDENDDATE;
        }
        public BigDecimal getF_NAV_DISTRIBUTION() {
            return F_NAV_DISTRIBUTION;
        }
        public void setF_NAV_DISTRIBUTION(BigDecimal f_NAV_DISTRIBUTION) {
            F_NAV_DISTRIBUTION = f_NAV_DISTRIBUTION;
        }
        public String getS_INFO_ASHARECODE() {
            return S_INFO_ASHARECODE;
        }
        public void setS_INFO_ASHARECODE(String s_INFO_ASHARECODE) {
            S_INFO_ASHARECODE = s_INFO_ASHARECODE;
        }
        public String getOPDATE() {
            return OPDATE;
        }
        public void setOPDATE(String oPDATE) {
            OPDATE = oPDATE;
        }
        public String getOPMODE() {
            return OPMODE;
        }
        public void setOPMODE(String oPMODE) {
            OPMODE = oPMODE;
        }
        public String getMOPDATE() {
            return MOPDATE;
        }
        public void setMOPDATE(String mOPDATE) {
            MOPDATE = mOPDATE;
        }
        public static long getSerialversionuid() {
            return serialVersionUID;
        }


}
