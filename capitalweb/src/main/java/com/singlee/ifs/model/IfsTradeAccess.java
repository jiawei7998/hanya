package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "IFS_TRADE_ACCESS")//同业授信信息登记表
public class IfsTradeAccess implements Serializable{

	private static final long serialVersionUID = 1L;
	
	/**
	 * 客户号
	 */
	private String coreClntNo;
	/**
	 * 公司名称
	 */
	private String conm;
	/**
	 * 业务类型
	 */
	private String bussType;
	/**
	 * 业务类型名称
	 */
	private String bussTpnm;
	/**
	 * 机构码
	 */
	private String branchCode;
	/**
	 * 机构名称
	 */
	private String branchName;
	/**
	 * 包含下级标志
	 */
	private String inclSubFlg;
	/**
	 * 审批日期
	 */
	private Date approvalDate;
	
	/**
     * 交易类型
     */
    private String dealType;
	
	/**
	 * 操作时间
	 */
	@Transient
	private String oprTime;
	/**
	 * 操作类型
	 */
	@Transient
	private String oprType;
	/**
	 * 操作员
	 */
	@Transient
	private String operator;
    public String getCoreClntNo() {
        return coreClntNo;
    }
    public void setCoreClntNo(String coreClntNo) {
        this.coreClntNo = coreClntNo;
    }
    public String getConm() {
        return conm;
    }
    public void setConm(String conm) {
        this.conm = conm;
    }
    public String getBussType() {
        return bussType;
    }
    public void setBussType(String bussType) {
        this.bussType = bussType;
    }
    public String getBussTpnm() {
        return bussTpnm;
    }
    public void setBussTpnm(String bussTpnm) {
        this.bussTpnm = bussTpnm;
    }
    public String getBranchCode() {
        return branchCode;
    }
    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }
    public String getBranchName() {
        return branchName;
    }
    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }
    public String getInclSubFlg() {
        return inclSubFlg;
    }
    public void setInclSubFlg(String inclSubFlg) {
        this.inclSubFlg = inclSubFlg;
    }
    public Date getApprovalDate() {
        return approvalDate;
    }
    public void setApprovalDate(Date approvalDate) {
        this.approvalDate = approvalDate;
    }
    public String getDealType() {
        return dealType;
    }
    public void setDealType(String dealType) {
        this.dealType = dealType;
    }
    public String getOprTime() {
        return oprTime;
    }
    public void setOprTime(String oprTime) {
        this.oprTime = oprTime;
    }
    public String getOprType() {
        return oprType;
    }
    public void setOprType(String oprType) {
        this.oprType = oprType;
    }
    public String getOperator() {
        return operator;
    }
    public void setOperator(String operator) {
        this.operator = operator;
    }
    public static long getSerialversionuid() {
        return serialVersionUID;
    }
    @Override
    public String toString() {
        return "IfsTradeAccess [coreClntNo=" + coreClntNo + ", conm=" + conm + ", bussType=" + bussType + ", bussTpnm="
                + bussTpnm + ", branchCode=" + branchCode + ", branchName=" + branchName + ", inclSubFlg=" + inclSubFlg
                + ", approvalDate=" + approvalDate + ", dealType=" + dealType + ", oprTime=" + oprTime + ", oprType="
                + oprType + ", operator=" + operator + "]";
    }
  
	
}
