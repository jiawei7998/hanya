package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "IFS_CUST_SETTLE")
public class IfsTrdSettle extends IfsBaseFlow implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String sn; //

	private String br; // 部门

	private String product;// 产品代码

	private String prodtype;// 产品类型

	private String dealno;// 流水号

	private String seq;// 分序号

	private String server;// 来源

	private String fedealno; //审批单号

	private String cno;// 交易对手号

	private String vdate;// 付款日期

	private String payrecind;// 收付方向

	private String ccy;// 币种

	private BigDecimal amount;// 金额

	private String cdate;

	private String ctime;

	private String setmeans;// 清算方式

	private String setacct;// 清算账户

	private String status;// 清算状态

	private String startBankid;// 清算账户

	private String payBankid;// 付款行行号

	private String payBankname;// 付款行行名

	private String payUserid;// 付款账号

	private String payUsername;// 付款账号名称

	private String recBankid;// 收款行行号

	private String recBankname;// 收款行行名

	private String recUserid;// 收款账号

	private String recUsername;// 收款账号名称

	private String yingyeBr;

	private String hvpType;// 业务种类(10-现金汇款,11-普通汇兑,12-网银支付...)

	private String hvpType1;// 汇兑类型(0-正常)

	private String pzType;// 凭证种类

	private String hurryLevel;// 加急等级

	private String remarkFy;// 附言

	private String remarkYt;

	private String dealflag;// 经办状态(0-待经办,1-已经办待签发)

	private String voidflag;// 撤销标志(0-申请撤销,1-撤销成功,2-撤销失败)

	private String settflag;// 处理状态(1-待处理,PR07-已处理)

	private String reason;// 原因

	private String ioper;// 经办人员

	private String itime;// 经办时间

	private String backtime;// 退回时间

	private String voper;// 复核人员

	private String vtime;// 复核时间

	private String retcode;// 返回码

	private String retmsg;// 返回信息

	private String userdealno;// 柜员流水号

	private String busdealno;// 业务编号

	private String inputtime;// 接口数据插入时间

	private String note;// 备注

	private String dealuser;// 当前操作用户

	private String dealtime;// 当前操作时间

	private String interfaceNo;// 接口流水

	private String dealstatus; // 清算状态(交易状态)

	private String cname; // 交易对手名称

	private String bussstatus; // 业务状态

	private String tellseqno; // 核心记账流水号
	
	private String signDate; // 签发日期
	
	private String accflag;//记账标识

	private BigDecimal handleAmt;//手续费
	private String iopername;//经办人名称
	private String vopername;//复核人名称

	public BigDecimal getHandleAmt() {
		return handleAmt;
	}

	public void setHandleAmt(BigDecimal handleAmt) {
		this.handleAmt = handleAmt;
	}

	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	@Override
    public String getBr() {
		return br;
	}

	@Override
    public void setBr(String br) {
		this.br = br;
	}

	@Override
    public String getProduct() {
		return product;
	}

	@Override
    public void setProduct(String product) {
		this.product = product;
	}

	public String getProdtype() {
		return prodtype;
	}

	public void setProdtype(String prodtype) {
		this.prodtype = prodtype;
	}

	public String getDealno() {
		return dealno;
	}

	public void setDealno(String dealno) {
		this.dealno = dealno;
	}

	public String getSeq() {
		return seq;
	}

	public void setSeq(String seq) {
		this.seq = seq;
	}

	public String getServer() {
		return server;
	}

	public void setServer(String server) {
		this.server = server;
	}

	public String getFedealno() {
		return fedealno;
	}

	public void setFedealno(String fedealno) {
		this.fedealno = fedealno;
	}

	@Override
    public String getCno() {
		return cno;
	}

	@Override
    public void setCno(String cno) {
		this.cno = cno;
	}

	public String getVdate() {
		return vdate;
	}

	public void setVdate(String vdate) {
		this.vdate = vdate;
	}

	public String getPayrecind() {
		return payrecind;
	}

	public void setPayrecind(String payrecind) {
		this.payrecind = payrecind;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getCdate() {
		return cdate;
	}

	public void setCdate(String cdate) {
		this.cdate = cdate;
	}

	public String getCtime() {
		return ctime;
	}

	public void setCtime(String ctime) {
		this.ctime = ctime;
	}

	public String getSetmeans() {
		return setmeans;
	}

	public void setSetmeans(String setmeans) {
		this.setmeans = setmeans;
	}

	public String getSetacct() {
		return setacct;
	}

	public void setSetacct(String setacct) {
		this.setacct = setacct;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStartBankid() {
		return startBankid;
	}

	public void setStartBankid(String startBankid) {
		this.startBankid = startBankid;
	}

	public String getPayBankid() {
		return payBankid;
	}

	public void setPayBankid(String payBankid) {
		this.payBankid = payBankid;
	}

	public String getPayBankname() {
		return payBankname;
	}

	public void setPayBankname(String payBankname) {
		this.payBankname = payBankname;
	}

	public String getPayUserid() {
		return payUserid;
	}

	public void setPayUserid(String payUserid) {
		this.payUserid = payUserid;
	}

	public String getPayUsername() {
		return payUsername;
	}

	public void setPayUsername(String payUsername) {
		this.payUsername = payUsername;
	}

	public String getRecBankid() {
		return recBankid;
	}

	public void setRecBankid(String recBankid) {
		this.recBankid = recBankid;
	}

	public String getRecBankname() {
		return recBankname;
	}

	public void setRecBankname(String recBankname) {
		this.recBankname = recBankname;
	}

	public String getRecUserid() {
		return recUserid;
	}

	public void setRecUserid(String recUserid) {
		this.recUserid = recUserid;
	}

	public String getRecUsername() {
		return recUsername;
	}

	public void setRecUsername(String recUsername) {
		this.recUsername = recUsername;
	}

	public String getYingyeBr() {
		return yingyeBr;
	}

	public void setYingyeBr(String yingyeBr) {
		this.yingyeBr = yingyeBr;
	}

	public String getHvpType() {
		return hvpType;
	}

	public void setHvpType(String hvpType) {
		this.hvpType = hvpType;
	}

	public String getHvpType1() {
		return hvpType1;
	}

	public void setHvpType1(String hvpType1) {
		this.hvpType1 = hvpType1;
	}

	public String getPzType() {
		return pzType;
	}

	public void setPzType(String pzType) {
		this.pzType = pzType;
	}

	public String getHurryLevel() {
		return hurryLevel;
	}

	public void setHurryLevel(String hurryLevel) {
		this.hurryLevel = hurryLevel;
	}

	public String getRemarkFy() {
		return remarkFy;
	}

	public void setRemarkFy(String remarkFy) {
		this.remarkFy = remarkFy;
	}

	public String getRemarkYt() {
		return remarkYt;
	}

	public void setRemarkYt(String remarkYt) {
		this.remarkYt = remarkYt;
	}

	public String getDealflag() {
		return dealflag;
	}

	public void setDealflag(String dealflag) {
		this.dealflag = dealflag;
	}

	public String getVoidflag() {
		return voidflag;
	}

	public void setVoidflag(String voidflag) {
		this.voidflag = voidflag;
	}

	public String getSettflag() {
		return settflag;
	}

	public void setSettflag(String settflag) {
		this.settflag = settflag;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getIoper() {
		return ioper;
	}

	public void setIoper(String ioper) {
		this.ioper = ioper;
	}

	public String getItime() {
		return itime;
	}

	public void setItime(String itime) {
		this.itime = itime;
	}

	public String getVoper() {
		return voper;
	}

	public void setVoper(String voper) {
		this.voper = voper;
	}

	public String getVtime() {
		return vtime;
	}

	public void setVtime(String vtime) {
		this.vtime = vtime;
	}

	public String getRetcode() {
		return retcode;
	}

	public void setRetcode(String retcode) {
		this.retcode = retcode;
	}

	public String getRetmsg() {
		return retmsg;
	}

	public void setRetmsg(String retmsg) {
		this.retmsg = retmsg;
	}

	public String getUserdealno() {
		return userdealno;
	}

	public void setUserdealno(String userdealno) {
		this.userdealno = userdealno;
	}

	public String getBusdealno() {
		return busdealno;
	}

	public void setBusdealno(String busdealno) {
		this.busdealno = busdealno;
	}

	public String getInputtime() {
		return inputtime;
	}

	public void setInputtime(String inputtime) {
		this.inputtime = inputtime;
	}

	@Override
    public String getNote() {
		return note;
	}

	@Override
    public void setNote(String note) {
		this.note = note;
	}

	public String getDealuser() {
		return dealuser;
	}

	public void setDealuser(String dealuser) {
		this.dealuser = dealuser;
	}

	public String getDealtime() {
		return dealtime;
	}

	public void setDealtime(String dealtime) {
		this.dealtime = dealtime;
	}

	public String getBacktime() {
		return backtime;
	}

	public void setBacktime(String backtime) {
		this.backtime = backtime;
	}

	public void setInterfaceNo(String interfaceNo) {
		this.interfaceNo = interfaceNo;
	}

	public String getInterfaceNo() {
		return interfaceNo;
	}

	public String getDealstatus() {
		return dealstatus;
	}

	public void setDealstatus(String dealstatus) {
		this.dealstatus = dealstatus;
	}

	public String getCname() {
		return cname;
	}

	public void setCname(String cname) {
		this.cname = cname;
	}

	public String getBussstatus() {
		return bussstatus;
	}

	public void setBussstatus(String bussstatus) {
		this.bussstatus = bussstatus;
	}

	public String getTellseqno() {
		return tellseqno;
	}

	public void setTellseqno(String tellseqno) {
		this.tellseqno = tellseqno;
	}

	public String getSignDate() {
		return signDate;
	}

	public void setSignDate(String signDate) {
		this.signDate = signDate;
	}

	public String getAccflag() {
		return accflag;
	}

	public void setAccflag(String accflag) {
		this.accflag = accflag;
	}

	public String getIopername() {
		return iopername;
	}

	public void setIopername(String iopername) {
		this.iopername = iopername;
	}

	public String getVopername() {
		return vopername;
	}

	public void setVopername(String vopername) {
		this.vopername = vopername;
	}
	
}