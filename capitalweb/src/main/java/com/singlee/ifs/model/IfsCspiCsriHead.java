package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "IFS_CSPICSRI_HEAD")
public class IfsCspiCsriHead implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 629204736132325618L;
	// 部门
	private String br;
	// 服务标识
	private String server;
	// 外部流水号
	private String fedealno;
	// 序列
	private String seq;
	// I-导入 O-导出
	private String inoutind;
	// 产品代码
	private String product;
	// 产品类型
	private String prodtype;
	// 币种
	private String ccy;
	// 交易对手号
	private String cno;
	// 清算方式
	private String settmeans;
	// 清算账户
	private String settaccount;
	// 标识
	private String usualid;
	// 生效日期
	private Date effdate;
	private String recordtype;
	// 收付标识
	private String payrecind;
	// 默认A-add U-update D-delete
	private String addupdelind;
	// 授权标识
	private String authind;
	// 禁止标识
	private String suppayind;
	// 禁止标识
	private String supconfind;
	private String p1;
	private String p2;
	private String p3;
	private String p4;
	private String det;
	private String r1;
	private String r2;
	private String r3;
	private String r4;
	private String r5;
	private String r6;
	// 经办人员
	private String slioper;
	
	private String slaction;
	//经办复核标识 0：待经办 1：已经办未复核 2：已复核
	private String slflag;
	//经办日期
	private Date slidate;
	//复核人员
	private String veroper;
	//复核日期
	private Date verdate;
	//同步状态，0：未同步，1：已同步
	private String status;
	//opics的statcode
	private String statcode;
	//opics错误码
	private String errorcode;
	//产品类型对应业务中文名称
	@Transient
	private String descrcn;
	//客户中文名称
	@Transient
	private String cliname;
	
	
	@Transient
	private List<IfsCspiCsriDetail> cspiCsriDetailList;

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getServer() {
		return server;
	}

	public void setServer(String server) {
		this.server = server;
	}

	public String getFedealno() {
		return fedealno;
	}

	public void setFedealno(String fedealno) {
		this.fedealno = fedealno;
	}

	public String getSeq() {
		return seq;
	}

	public void setSeq(String seq) {
		this.seq = seq;
	}

	public String getInoutind() {
		return inoutind;
	}

	public void setInoutind(String inoutind) {
		this.inoutind = inoutind;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getProdtype() {
		return prodtype;
	}

	public void setProdtype(String prodtype) {
		this.prodtype = prodtype;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getCno() {
		return cno;
	}

	public void setCno(String cno) {
		this.cno = cno;
	}

	public String getSettmeans() {
		return settmeans;
	}

	public void setSettmeans(String settmeans) {
		this.settmeans = settmeans;
	}

	public String getSettaccount() {
		return settaccount;
	}

	public void setSettaccount(String settaccount) {
		this.settaccount = settaccount;
	}

	public String getUsualid() {
		return usualid;
	}

	public void setUsualid(String usualid) {
		this.usualid = usualid;
	}

	public Date getEffdate() {
		return effdate;
	}

	public void setEffdate(Date effdate) {
		this.effdate = effdate;
	}

	public String getRecordtype() {
		return recordtype;
	}

	public void setRecordtype(String recordtype) {
		this.recordtype = recordtype;
	}

	public String getPayrecind() {
		return payrecind;
	}

	public void setPayrecind(String payrecind) {
		this.payrecind = payrecind;
	}

	public String getAddupdelind() {
		return addupdelind;
	}

	public void setAddupdelind(String addupdelind) {
		this.addupdelind = addupdelind;
	}

	public String getAuthind() {
		return authind;
	}

	public void setAuthind(String authind) {
		this.authind = authind;
	}

	public String getSuppayind() {
		return suppayind;
	}

	public void setSuppayind(String suppayind) {
		this.suppayind = suppayind;
	}

	public String getSupconfind() {
		return supconfind;
	}

	public void setSupconfind(String supconfind) {
		this.supconfind = supconfind;
	}

	public String getP1() {
		return p1;
	}

	public void setP1(String p1) {
		this.p1 = p1;
	}

	public String getP2() {
		return p2;
	}

	public void setP2(String p2) {
		this.p2 = p2;
	}

	public String getP3() {
		return p3;
	}

	public void setP3(String p3) {
		this.p3 = p3;
	}

	public String getP4() {
		return p4;
	}

	public void setP4(String p4) {
		this.p4 = p4;
	}

	public String getDet() {
		return det;
	}

	public void setDet(String det) {
		this.det = det;
	}

	public String getR1() {
		return r1;
	}

	public void setR1(String r1) {
		this.r1 = r1;
	}

	public String getR2() {
		return r2;
	}

	public void setR2(String r2) {
		this.r2 = r2;
	}

	public String getR3() {
		return r3;
	}

	public void setR3(String r3) {
		this.r3 = r3;
	}

	public String getR4() {
		return r4;
	}

	public void setR4(String r4) {
		this.r4 = r4;
	}

	public String getR5() {
		return r5;
	}

	public void setR5(String r5) {
		this.r5 = r5;
	}

	public String getR6() {
		return r6;
	}

	public void setR6(String r6) {
		this.r6 = r6;
	}

	public String getSlioper() {
		return slioper;
	}

	public void setSlioper(String slioper) {
		this.slioper = slioper;
	}

	public String getSlaction() {
		return slaction;
	}

	public void setSlaction(String slaction) {
		this.slaction = slaction;
	}

	public String getSlflag() {
		return slflag;
	}

	public void setSlflag(String slflag) {
		this.slflag = slflag;
	}

	public Date getSlidate() {
		return slidate;
	}

	public void setSlidate(Date slidate) {
		this.slidate = slidate;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void setCspiCsriDetailList(List<IfsCspiCsriDetail> cspiCsriDetailList) {
		this.cspiCsriDetailList = cspiCsriDetailList;
	}

	public List<IfsCspiCsriDetail> getCspiCsriDetailList() {
		return cspiCsriDetailList;
	}

	public void setVeroper(String veroper) {
		this.veroper = veroper;
	}

	public String getVeroper() {
		return veroper;
	}

	public void setVerdate(Date verdate) {
		this.verdate = verdate;
	}

	public Date getVerdate() {
		return verdate;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatus() {
		return status;
	}

	public void setStatcode(String statcode) {
		this.statcode = statcode;
	}

	public String getStatcode() {
		return statcode;
	}

	public void setErrorcode(String errorcode) {
		this.errorcode = errorcode;
	}

	public String getErrorcode() {
		return errorcode;
	}

	public void setDescrcn(String descrcn) {
		this.descrcn = descrcn;
	}

	public String getDescrcn() {
		return descrcn;
	}

	public void setCliname(String cliname) {
		this.cliname = cliname;
	}

	public String getCliname() {
		return cliname;
	}


}
