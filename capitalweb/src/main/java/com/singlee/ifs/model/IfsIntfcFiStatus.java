package com.singlee.ifs.model;

import java.io.Serializable;

public class IfsIntfcFiStatus implements Serializable{
	
private static final long serialVersionUID = 1L;
	
	private String br;
	
	private String dealno;
	
	private String seq;
	
	private String chois_Ref_No;
	
	private String chois_His_No;
	
	private String sendTime;
	
	private String isSend;
	
	private String isReSend;
	
	private String sendResult;
	
	private String sendMsg;
	
	private String tableName;
	
	private int upCount;
	
	private String upTime;
	
	private String upOwn;
	
	private String inputTime;
	
	private String reversal;
	
	private String errMsg;
	
	/**
	 * @return the errMsg
	 */
	public String getErrMsg() {
		return errMsg;
	}

	/**
	 * @param errMsg the errMsg to set
	 */
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

	public IfsIntfcFiStatus(){}

	/**
	 * @return the br
	 */
	public String getBr() {
		return br;
	}

	/**
	 * @param br the br to set
	 */
	public void setBr(String br) {
		this.br = br;
	}

	/**
	 * @return the dealno
	 */
	public String getDealno() {
		return dealno;
	}

	/**
	 * @param dealno the dealno to set
	 */
	public void setDealno(String dealno) {
		this.dealno = dealno;
	}

	/**
	 * @return the seq
	 */
	public String getSeq() {
		return seq;
	}

	/**
	 * @param seq the seq to set
	 */
	public void setSeq(String seq) {
		this.seq = seq;
	}

	/**
	 * @return the chois_Ref_No
	 */
	public String getChois_Ref_No() {
		return chois_Ref_No;
	}

	/**
	 * @param chois_Ref_No the chois_Ref_No to set
	 */
	public void setChois_Ref_No(String chois_Ref_No) {
		this.chois_Ref_No = chois_Ref_No;
	}

	/**
	 * @return the chois_His_No
	 */
	public String getChois_His_No() {
		return chois_His_No;
	}

	/**
	 * @param chois_His_No the chois_His_No to set
	 */
	public void setChois_His_No(String chois_His_No) {
		this.chois_His_No = chois_His_No;
	}

	/**
	 * @return the sendTime
	 */
	public String getSendTime() {
		return sendTime;
	}

	/**
	 * @param sendTime the sendTime to set
	 */
	public void setSendTime(String sendTime) {
		this.sendTime = sendTime;
	}

	/**
	 * @return the isSend
	 */
	public String getIsSend() {
		return isSend;
	}

	/**
	 * @param isSend the isSend to set
	 */
	public void setIsSend(String isSend) {
		this.isSend = isSend;
	}

	/**
	 * @return the isReSend
	 */
	public String getIsReSend() {
		return isReSend;
	}

	/**
	 * @param isReSend the isReSend to set
	 */
	public void setIsReSend(String isReSend) {
		this.isReSend = isReSend;
	}

	/**
	 * @return the sendResult
	 */
	public String getSendResult() {
		return sendResult;
	}

	/**
	 * @param sendResult the sendResult to set
	 */
	public void setSendResult(String sendResult) {
		this.sendResult = sendResult;
	}

	/**
	 * @return the sendMsg
	 */
	public String getSendMsg() {
		return sendMsg;
	}

	/**
	 * @param sendMsg the sendMsg to set
	 */
	public void setSendMsg(String sendMsg) {
		this.sendMsg = sendMsg;
	}

	/**
	 * @return the tableName
	 */
	public String getTableName() {
		return tableName;
	}

	/**
	 * @param tableName the tableName to set
	 */
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	/**
	 * @return the upCount
	 */
	public int getUpCount() {
		return upCount;
	}

	/**
	 * @param upCount the upCount to set
	 */
	public void setUpCount(int upCount) {
		this.upCount = upCount;
	}

	/**
	 * @return the upTime
	 */
	public String getUpTime() {
		return upTime;
	}

	/**
	 * @param upTime the upTime to set
	 */
	public void setUpTime(String upTime) {
		this.upTime = upTime;
	}

	/**
	 * @return the upOwn
	 */
	public String getUpOwn() {
		return upOwn;
	}

	/**
	 * @param upOwn the upOwn to set
	 */
	public void setUpOwn(String upOwn) {
		this.upOwn = upOwn;
	}

	/**
	 * @return the inputTime
	 */
	public String getInputTime() {
		return inputTime;
	}

	/**
	 * @param inputTime the inputTime to set
	 */
	public void setInputTime(String inputTime) {
		this.inputTime = inputTime;
	}

	/**
	 * @return the reversal
	 */
	public String getReversal() {
		return reversal;
	}

	/**
	 * @param reversal the reversal to set
	 */
	public void setReversal(String reversal) {
		this.reversal = reversal;
	}

}
