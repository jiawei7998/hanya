package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;


@Entity
@Table(name = "IFS_APPROVEFX_SWAP")
public class IfsApprovefxSwap extends IfsApproveBaseBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private String tradingModel;
	//交易模式
	private String tradingType;
	//交易方式
	private String currencyPair;
	//货币对
	private String fwdRate;
	//远端汇率
	private String nearValuedate;
	//近端起息日
	private String fwdPrice;
	//远端交易金额
	private String nearRate;
	//近端汇率
	private String spread;
	//点差
	private String fwdValuedate;
	//远端起息日
	private String nearPrice;
	//近端交易金额
	private String fwdReversePrice;
	//远端反向交易金额
	private String nearReversePrice;
	//近端反向交易金额
	private String CONTRACT_ID;

	public String getFwdRate() {
		return fwdRate;
	}

	public void setFwdRate(String fwdRate) {
		this.fwdRate = fwdRate;
	}

	public String getNearValuedate() {
		return nearValuedate;
	}

	public void setNearValuedate(String nearValuedate) {
		this.nearValuedate = nearValuedate;
	}


	public String getFwdPrice() {
		return fwdPrice;
	}

	public void setFwdPrice(String fwdPrice) {
		this.fwdPrice = fwdPrice;
	}

	public String getNearRate() {
		return nearRate;
	}

	public void setNearRate(String nearRate) {
		this.nearRate = nearRate;
	}

	public String getSpread() {
		return spread;
	}

	public void setSpread(String spread) {
		this.spread = spread;
	}

	public String getFwdValuedate() {
		return fwdValuedate;
	}

	public void setFwdValuedate(String fwdValuedate) {
		this.fwdValuedate = fwdValuedate;
	}

	public String getNearPrice() {
		return nearPrice;
	}

	public void setNearPrice(String nearPrice) {
		this.nearPrice = nearPrice;
	}


	@Override
    public String getTradingModel() {
		return tradingModel;
	}

	@Override
    public void setTradingModel(String tradingModel) {
		this.tradingModel = tradingModel == null ? null : tradingModel.trim();
	}

	public String getTradingType() {
		return tradingType;
	}

	public void setTradingType(String tradingType) {
		this.tradingType = tradingType == null ? null : tradingType.trim();
	}

	public String getCurrencyPair() {
		return currencyPair;
	}

	public void setCurrencyPair(String currencyPair) {
		this.currencyPair = currencyPair == null ? null : currencyPair.trim();
	}

	public String getFwdReversePrice() {
		return fwdReversePrice;
	}

	public void setFwdReversePrice(String fwdReversePrice) {
		this.fwdReversePrice = fwdReversePrice;
	}

	public String getNearReversePrice() {
		return nearReversePrice;
	}

	public void setNearReversePrice(String nearReversePrice) {
		this.nearReversePrice = nearReversePrice;
	}

	public String getCONTRACT_ID() {
		return CONTRACT_ID;
	}

	public void setCONTRACT_ID(String cONTRACT_ID) {
		CONTRACT_ID = cONTRACT_ID;
	}
}