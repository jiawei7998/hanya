package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

/**
 *
 * 2021/11/29
 * @Auther:zk
 */    
    
    /**
    * 活期同业存款
    */
@Entity
@Table(name = "IFS_CUR_DEPOSIT")
public class IfsCurDeposit {
    /**
    * 分行名称
    */
    private String branch;

    /**
    * 支行名称
    */
    private String subBranch;

    /**
    * 交易对手总部全称
    */
    private String custName;

    /**
    * 交易对手（全称）
    */
    private String sfn;

    /**
    * 交易对手地域
    */
    private String custArea;

    /**
    * 活期/定期
    */
    private String curFix;

    /**
    * 业务开始日期
    */
    private String vdate;

    /**
    * 业务结束日期
    */
    private String mdate;

    /**
    * 利率
    */
    private String rate;

    /**
    * 余额
    */
    private BigDecimal amt;

    /**
    * 记帐科目
    */
    private String subject;

    /**
    * 交易对手类型
    */
    private String custType;

    /**
     * 交易类型
     */
    private String dealType;

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getSubBranch() {
        return subBranch;
    }

    public void setSubBranch(String subBranch) {
        this.subBranch = subBranch;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getSfn() {
        return sfn;
    }

    public void setSfn(String sfn) {
        this.sfn = sfn;
    }

    public String getCustArea() {
        return custArea;
    }

    public void setCustArea(String custArea) {
        this.custArea = custArea;
    }

    public String getCurFix() {
        return curFix;
    }

    public void setCurFix(String curFix) {
        this.curFix = curFix;
    }

    public String getVdate() {
        return vdate;
    }

    public void setVdate(String vdate) {
        this.vdate = vdate;
    }

    public String getMdate() {
        return mdate;
    }

    public void setMdate(String mdate) {
        this.mdate = mdate;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public BigDecimal getAmt() {
        return amt;
    }

    public void setAmt(BigDecimal amt) {
        this.amt = amt;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getCustType() {
        return custType;
    }

    public void setCustType(String custType) {
        this.custType = custType;
    }

    public String getDealType() {
        return dealType;
    }

    public void setDealType(String dealType) {
        this.dealType = dealType;
    }
}