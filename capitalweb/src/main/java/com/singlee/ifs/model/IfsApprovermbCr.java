package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;
@Entity
@Table(name = "IFS_APPROVERMB_CR")
public class IfsApprovermbCr extends IfsApproveBaseBean {
    
    private String tenor;

    private BigDecimal repoRate;

    private BigDecimal underlyingQty;

    private BigDecimal tradeAmount;

    private BigDecimal accuredInterest;

    private BigDecimal secondSettlementAmount;

    private String firstSettlementMethod;

    private String secondSettlementMethod;

    private String firstSettlementDate;

    private String secondSettlementDate;

    private String occupancyDays;

    private String tradingProduct;

    private String bondCode;

    private String bondName;

    private BigDecimal totalFaceValue;

    private BigDecimal underlyingStipType;

    private static final long serialVersionUID = 1L;


	public String getTenor() {
		return tenor;
	}

	public void setTenor(String tenor) {
		this.tenor = tenor;
	}

	public BigDecimal getRepoRate() {
		return repoRate;
	}

	public void setRepoRate(BigDecimal repoRate) {
		this.repoRate = repoRate;
	}

	public BigDecimal getUnderlyingQty() {
		return underlyingQty;
	}

	public void setUnderlyingQty(BigDecimal underlyingQty) {
		this.underlyingQty = underlyingQty;
	}

	public BigDecimal getTradeAmount() {
		return tradeAmount;
	}

	public void setTradeAmount(BigDecimal tradeAmount) {
		this.tradeAmount = tradeAmount;
	}

	public BigDecimal getAccuredInterest() {
		return accuredInterest;
	}

	public void setAccuredInterest(BigDecimal accuredInterest) {
		this.accuredInterest = accuredInterest;
	}

	public BigDecimal getSecondSettlementAmount() {
		return secondSettlementAmount;
	}

	public void setSecondSettlementAmount(BigDecimal secondSettlementAmount) {
		this.secondSettlementAmount = secondSettlementAmount;
	}

	public String getFirstSettlementMethod() {
		return firstSettlementMethod;
	}

	public void setFirstSettlementMethod(String firstSettlementMethod) {
		this.firstSettlementMethod = firstSettlementMethod;
	}

	public String getSecondSettlementMethod() {
		return secondSettlementMethod;
	}

	public void setSecondSettlementMethod(String secondSettlementMethod) {
		this.secondSettlementMethod = secondSettlementMethod;
	}

	public String getFirstSettlementDate() {
		return firstSettlementDate;
	}

	public void setFirstSettlementDate(String firstSettlementDate) {
		this.firstSettlementDate = firstSettlementDate;
	}

	public String getSecondSettlementDate() {
		return secondSettlementDate;
	}

	public void setSecondSettlementDate(String secondSettlementDate) {
		this.secondSettlementDate = secondSettlementDate;
	}

	public String getOccupancyDays() {
		return occupancyDays;
	}

	public void setOccupancyDays(String occupancyDays) {
		this.occupancyDays = occupancyDays;
	}

	public String getTradingProduct() {
		return tradingProduct;
	}

	public void setTradingProduct(String tradingProduct) {
		this.tradingProduct = tradingProduct;
	}

	public String getBondCode() {
		return bondCode;
	}

	public void setBondCode(String bondCode) {
		this.bondCode = bondCode;
	}

	public String getBondName() {
		return bondName;
	}

	public void setBondName(String bondName) {
		this.bondName = bondName;
	}

	public BigDecimal getTotalFaceValue() {
		return totalFaceValue;
	}

	public void setTotalFaceValue(BigDecimal totalFaceValue) {
		this.totalFaceValue = totalFaceValue;
	}

	public BigDecimal getUnderlyingStipType() {
		return underlyingStipType;
	}

	public void setUnderlyingStipType(BigDecimal underlyingStipType) {
		this.underlyingStipType = underlyingStipType;
	}

  
}