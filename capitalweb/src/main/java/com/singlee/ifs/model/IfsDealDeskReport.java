package com.singlee.ifs.model;

//交易台账
public class IfsDealDeskReport {
	//业务编号
	private String ticketId;
	
	//成交单号
	private String contractId;
	
	//产品
	private String prod;
	
	//产品类型
	private String prodType;
	
	//交易号
	private String dealNo;

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	public String getContractId() {
		return contractId;
	}

	public void setContractId(String contractId) {
		this.contractId = contractId;
	}

	public String getProd() {
		return prod;
	}

	public void setProd(String prod) {
		this.prod = prod;
	}

	public String getProdType() {
		return prodType;
	}

	public void setProdType(String prodType) {
		this.prodType = prodType;
	}

	public String getDealNo() {
		return dealNo;
	}

	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	
	
}
