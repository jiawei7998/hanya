package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "IFS_WIND_BOND_LEVEL")
public class IfsWindSecLevelBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private String bondName;// 债券简称
	@Id
	private String bondCode;// 债券代码
	private String ifName;// 发行人
	private String fxZtLevel;// 发行时主体信用评级
	private String fxZtDate;// 主体信用评级时间
	private String zxZtLevel;// 最新主体信用评级
	private String fxZxLevel;// 发行时债项评级
	private String fxZxDate;// 债项评级时间
	private String zxZxLevel;// 最新债项评级
	private String ztInst;// 最新主体评级机构
	private String zxInst;// 最新债项评级机构
	private Date spotDate;

	public String getBondName() {
		return bondName;
	}

	public void setBondName(String bondName) {
		this.bondName = bondName;
	}

	public String getBondCode() {
		return bondCode;
	}

	public void setBondCode(String bondCode) {
		this.bondCode = bondCode;
	}

	public String getIfName() {
		return ifName;
	}

	public void setIfName(String ifName) {
		this.ifName = ifName;
	}

	public String getFxZtLevel() {
		return fxZtLevel;
	}

	public void setFxZtLevel(String fxZtLevel) {
		this.fxZtLevel = fxZtLevel;
	}

	public String getFxZtDate() {
		return fxZtDate;
	}

	public void setFxZtDate(String fxZtDate) {
		this.fxZtDate = fxZtDate;
	}

	public String getZxZtLevel() {
		return zxZtLevel;
	}

	public void setZxZtLevel(String zxZtLevel) {
		this.zxZtLevel = zxZtLevel;
	}

	public String getFxZxLevel() {
		return fxZxLevel;
	}

	public void setFxZxLevel(String fxZxLevel) {
		this.fxZxLevel = fxZxLevel;
	}

	public String getFxZxDate() {
		return fxZxDate;
	}

	public void setFxZxDate(String fxZxDate) {
		this.fxZxDate = fxZxDate;
	}

	public String getZxZxLevel() {
		return zxZxLevel;
	}

	public void setZxZxLevel(String zxZxLevel) {
		this.zxZxLevel = zxZxLevel;
	}

	public String getZtInst() {
		return ztInst;
	}

	public void setZtInst(String ztInst) {
		this.ztInst = ztInst;
	}

	public String getZxInst() {
		return zxInst;
	}

	public void setZxInst(String zxInst) {
		this.zxInst = zxInst;
	}

	public Date getSpotDate() {
		return spotDate;
	}

	public void setSpotDate(Date spotDate) {
		this.spotDate = spotDate;
	}

}
