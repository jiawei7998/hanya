package com.singlee.ifs.model;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 券款兑付-债券信息
 * @author Liang
 * @date 2021/10/28 16:16
 * =======================
 */
public class IfsCustDVP implements Serializable {

    private static final long serialVersionUID = 1L;
    private String cno;
    private String br;
    private String version;
    private String product;
    private String type;
    private String fedealno;
    private String dealno;
    private String vdate;
    private BigDecimal amount;
    private String ioper;
    private String voper;
    private String ccyauthoper;
    private String secauthoper;
    private String authoriza;
    private String creator;
    private String seq;
    private String cdate;
    private String payrecind;
    private String cname;

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public String getSeq() {
        return seq;
    }

    public void setSeq(String seq) {
        this.seq = seq;
    }

    public String getCdate() {
        return cdate;
    }

    public void setCdate(String cdate) {
        this.cdate = cdate;
    }

    public String getPayrecind() {
        return payrecind;
    }

    public void setPayrecind(String payrecind) {
        this.payrecind = payrecind;
    }

    public String getCno() {
        return cno;
    }

    public void setCno(String cno) {
        this.cno = cno;
    }

    public String getBr() {
        return br;
    }

    public void setBr(String br) {
        this.br = br;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFedealno() {
        return fedealno;
    }

    public void setFedealno(String fedealno) {
        this.fedealno = fedealno;
    }

    public String getDealno() {
        return dealno;
    }

    public void setDealno(String dealno) {
        this.dealno = dealno;
    }

    public String getVdate() {
        return vdate;
    }

    public void setVdate(String vdate) {
        this.vdate = vdate;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getIoper() {
        return ioper;
    }

    public void setIoper(String ioper) {
        this.ioper = ioper;
    }

    public String getVoper() {
        return voper;
    }

    public void setVoper(String voper) {
        this.voper = voper;
    }

    public String getCcyauthoper() {
        return ccyauthoper;
    }

    public void setCcyauthoper(String ccyauthoper) {
        this.ccyauthoper = ccyauthoper;
    }

    public String getSecauthoper() {
        return secauthoper;
    }

    public void setSecauthoper(String secauthoper) {
        this.secauthoper = secauthoper;
    }

    public String getAuthoriza() {
        return authoriza;
    }

    public void setAuthoriza(String authoriza) {
        this.authoriza = authoriza;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }
}
