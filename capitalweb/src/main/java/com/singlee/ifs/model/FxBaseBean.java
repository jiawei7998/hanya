/**
 * Project Name:capitalweb
 * File Name:FxBaseBean.java
 * Package Name:com.singlee.ifs.model
 * Date:2018年6月4日上午9:05:07
 * Copyright (c) 2018, chenzhou1025@126.com All Rights Reserved.
 *
*/
/**
 * Project Name:capitalweb
 * File Name:FxBaseBean.java
 * Package Name:com.singlee.ifs.model
 * Date:2018年6月4日上午9:05:07
 * Copyright (c) 2018, singlee@singlee.com.cn All Rights Reserved.
 *
 */

package com.singlee.ifs.model;


/**
 * ClassName:FxBaseBean <br/>
 * Reason:	 外币基础类. <br/>
 * Date:     2018年6月4日 上午9:05:07 <br/>
 * @author   zhangcm
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class FxBaseBean  extends IfsBaseFlow{
    private static final long serialVersionUID = 1L;
		//交易单号(流水号系统自动生成)
		private String ticketId;
		//交易状态
		private String dealTransType;
		//本方机构
		private String instId;
		//对方机构
		private String counterpartyInstId;
		//本方交易员
		private String dealer;
		//对方交易员
		private String counterpartyDealer;
		//交易日期
		private String forDate;
		//交易时间
		private String forTime;
		//交易模式
		//private String tradingModel;
		//交易方式
		private String tradingType;
		//货币对
		private String currencyPair;
		//结算方式
		private String deliveryType;

	public String getDeliveryType() {
		return deliveryType;
	}

	public void setDeliveryType(String deliveryType) {
		this.deliveryType = deliveryType;
	}

	public String getTicketId() {
			return ticketId;
		}
		public void setTicketId(String ticketId) {
			this.ticketId = ticketId;
		}
		public String getDealTransType() {
			return dealTransType;
		}
		public void setDealTransType(String dealTransType) {
			this.dealTransType = dealTransType;
		}
		
		public String getDealer() {
			return dealer;
		}
		public void setDealer(String dealer) {
			this.dealer = dealer;
		}
		public String getForDate() {
			return forDate;
		}
		public void setForDate(String forDate) {
			this.forDate = forDate;
		}
		public String getForTime() {
			return forTime;
		}
		public void setForTime(String forTime) {
			this.forTime = forTime;
		}
		/*public String getTradingModel() {
			return tradingModel;
		}
		public void setTradingModel(String tradingModel) {
			this.tradingModel = tradingModel;
		}*/
		public String getTradingType() {
			return tradingType;
		}
		public void setTradingType(String tradingType) {
			this.tradingType = tradingType;
		}
		public String getCurrencyPair() {
			return currencyPair;
		}
		public void setCurrencyPair(String currencyPair) {
			this.currencyPair = currencyPair;
		}
		
		public String getInstId() {
			return instId;
		}
		public void setInstId(String instId) {
			this.instId = instId;
		}
		public String getCounterpartyInstId() {
			return counterpartyInstId;
		}
		public void setCounterpartyInstId(String counterpartyInstId) {
			this.counterpartyInstId = counterpartyInstId;
		}
		public String getCounterpartyDealer() {
			return counterpartyDealer;
		}
		public void setCounterpartyDealer(String counterpartyDealer) {
			this.counterpartyDealer = counterpartyDealer;
		}
		
}

