package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/***
 * opics 系统国家代码表
 * @author singlee4
 *
 */
@Entity
@Table(name="IFS_OPICS_COUN")
public class IfsOpicsCoun implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8864783940067550399L;
	
	/*国家代码*/
	private String ccode;
	/*国家代码英文详情*/
	private String coun;
	/*国家代码中文详情*/
	private String councn;
	/*上次维护日期*/
	private Date lstmntdte;
	/**/
	private String sensitiveind;
	/**/
	private String treatyind;
	/*操作者*/
	private String operator;
	/*opics处理状态*/
	private String status;
	/*备注*/
	private String remark;
	
	
	public String getCcode() {
		return ccode;
	}
	public void setCcode(String ccode) {
		this.ccode = ccode;
	}
	public String getCoun() {
		return coun;
	}
	public void setCoun(String coun) {
		this.coun = coun;
	}
	public String getCouncn() {
		return councn;
	}
	public void setCouncn(String councn) {
		this.councn = councn;
	}
	public Date getLstmntdte() {
		return lstmntdte;
	}
	public void setLstmntdte(Date lstmntdte) {
		this.lstmntdte = lstmntdte;
	}
	public String getSensitiveind() {
		return sensitiveind;
	}
	public void setSensitiveind(String sensitiveind) {
		this.sensitiveind = sensitiveind;
	}
	public String getTreatyind() {
		return treatyind;
	}
	public void setTreatyind(String treatyind) {
		this.treatyind = treatyind;
	}
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	
	
	
	
	

}
