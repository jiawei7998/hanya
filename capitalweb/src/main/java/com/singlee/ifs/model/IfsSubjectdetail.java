package com.singlee.ifs.model;

import javax.persistence.Column;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Luozb
 * @version 1.0.0
 * @ClassName IfsSubjectdetail.java
 * @Description OPICS科目余额
 * @createTime 2021年10月18日 10:49:00
 */
public class IfsSubjectdetail {
    @Column(name = "NEWNO")
    private String newno;

    /**
     * 系统日期
     */
    @Column(name = "POSTDATE")
    private String postdate;

    /**
     * 版本
     */
    @Column(name = "VERSION")
    private BigDecimal version;

    /**
     * 核算类型
     */
    @Column(name = "\"TYPE\"")
    private String type;

    /**
     * 债券类型
     */
    @Column(name = "SECTYPE")
    private String sectype;

    /**
     * 科目名称
     */
    @Column(name = "SUBNAME")
    private String subname;

    /**
     * 科目号
     */
    @Column(name = "CORENO")
    private String coreno;

    /**
     * 科目名称
     */
    @Column(name = "DRCRNAME")
    private String drcrname;

    /**
     * 借贷标识
     */
    @Column(name = "DRCR")
    private String drcr;

    /**
     * 余额
     */
    @Column(name = "BALANCEAMT")
    private BigDecimal balanceamt;

    /**
     * 批前日期
     */
    @Column(name = "PREPOSTDATE")
    private String prepostdate;

    /**
     * 更新时间
     */
    @Column(name = "UTIME")
    private Date utime;

    /**
     * 备注字段1
     */
    @Column(name = "MEMO1")
    private String memo1;

    /**
     * 备注字段2
     */
    @Column(name = "MEMO2")
    private String memo2;

    /**
     * 备注字段3
     */
    @Column(name = "MEMO3")
    private String memo3;

    @Column(name = "CREATOR")
    private BigDecimal creator;

    /**
     * CGL编号
     */
    @Column(name = "CGLCODE")
    private String cglcode;

    /**
     * CGL序列号
     */
    @Column(name = "CGLSEQ")
    private String cglseq;

    /**
     * 币种
     */
    @Column(name = "CCY")
    private String ccy;

    /**
     * 账号类型
     */
    @Column(name = "SIGN")
    private String sign;

    @Column(name = "STR1")
    private String str1;

    @Column(name = "STR2")
    private String str2;

    /**
     * @return NEWNO
     */
    public String getNewno() {
        return newno;
    }

    /**
     * @param newno
     */
    public void setNewno(String newno) {
        this.newno = newno;
    }

    /**
     * 获取系统日期
     *
     * @return POSTDATE - 系统日期
     */
    public String getPostdate() {
        return postdate;
    }

    /**
     * 设置系统日期
     *
     * @param postdate 系统日期
     */
    public void setPostdate(String postdate) {
        this.postdate = postdate;
    }

    /**
     * 获取版本
     *
     * @return VERSION - 版本
     */
    public BigDecimal getVersion() {
        return version;
    }

    /**
     * 设置版本
     *
     * @param version 版本
     */
    public void setVersion(BigDecimal version) {
        this.version = version;
    }

    /**
     * 获取核算类型
     *
     * @return TYPE - 核算类型
     */
    public String getType() {
        return type;
    }

    /**
     * 设置核算类型
     *
     * @param type 核算类型
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 获取债券类型
     *
     * @return SECTYPE - 债券类型
     */
    public String getSectype() {
        return sectype;
    }

    /**
     * 设置债券类型
     *
     * @param sectype 债券类型
     */
    public void setSectype(String sectype) {
        this.sectype = sectype;
    }

    /**
     * 获取科目名称
     *
     * @return SUBNAME - 科目名称
     */
    public String getSubname() {
        return subname;
    }

    /**
     * 设置科目名称
     *
     * @param subname 科目名称
     */
    public void setSubname(String subname) {
        this.subname = subname;
    }

    /**
     * 获取科目号
     *
     * @return CORENO - 科目号
     */
    public String getCoreno() {
        return coreno;
    }

    /**
     * 设置科目号
     *
     * @param coreno 科目号
     */
    public void setCoreno(String coreno) {
        this.coreno = coreno;
    }

    /**
     * 获取科目名称
     *
     * @return DRCRNAME - 科目名称
     */
    public String getDrcrname() {
        return drcrname;
    }

    /**
     * 设置科目名称
     *
     * @param drcrname 科目名称
     */
    public void setDrcrname(String drcrname) {
        this.drcrname = drcrname;
    }

    /**
     * 获取借贷标识
     *
     * @return DRCR - 借贷标识
     */
    public String getDrcr() {
        return drcr;
    }

    /**
     * 设置借贷标识
     *
     * @param drcr 借贷标识
     */
    public void setDrcr(String drcr) {
        this.drcr = drcr;
    }

    /**
     * 获取余额
     *
     * @return BALANCEAMT - 余额
     */
    public BigDecimal getBalanceamt() {
        return balanceamt;
    }

    /**
     * 设置余额
     *
     * @param balanceamt 余额
     */
    public void setBalanceamt(BigDecimal balanceamt) {
        this.balanceamt = balanceamt;
    }

    /**
     * 获取批前日期
     *
     * @return PREPOSTDATE - 批前日期
     */
    public String getPrepostdate() {
        return prepostdate;
    }

    /**
     * 设置批前日期
     *
     * @param prepostdate 批前日期
     */
    public void setPrepostdate(String prepostdate) {
        this.prepostdate = prepostdate;
    }

    /**
     * 获取更新时间
     *
     * @return UTIME - 更新时间
     */
    public Date getUtime() {
        return utime;
    }

    /**
     * 设置更新时间
     *
     * @param utime 更新时间
     */
    public void setUtime(Date utime) {
        this.utime = utime;
    }

    /**
     * 获取备注字段1
     *
     * @return MEMO1 - 备注字段1
     */
    public String getMemo1() {
        return memo1;
    }

    /**
     * 设置备注字段1
     *
     * @param memo1 备注字段1
     */
    public void setMemo1(String memo1) {
        this.memo1 = memo1;
    }

    /**
     * 获取备注字段2
     *
     * @return MEMO2 - 备注字段2
     */
    public String getMemo2() {
        return memo2;
    }

    /**
     * 设置备注字段2
     *
     * @param memo2 备注字段2
     */
    public void setMemo2(String memo2) {
        this.memo2 = memo2;
    }

    /**
     * 获取备注字段3
     *
     * @return MEMO3 - 备注字段3
     */
    public String getMemo3() {
        return memo3;
    }

    /**
     * 设置备注字段3
     *
     * @param memo3 备注字段3
     */
    public void setMemo3(String memo3) {
        this.memo3 = memo3;
    }

    /**
     * @return CREATOR
     */
    public BigDecimal getCreator() {
        return creator;
    }

    /**
     * @param creator
     */
    public void setCreator(BigDecimal creator) {
        this.creator = creator;
    }

    /**
     * 获取CGL编号
     *
     * @return CGLCODE - CGL编号
     */
    public String getCglcode() {
        return cglcode;
    }

    /**
     * 设置CGL编号
     *
     * @param cglcode CGL编号
     */
    public void setCglcode(String cglcode) {
        this.cglcode = cglcode;
    }

    /**
     * 获取CGL序列号
     *
     * @return CGLSEQ - CGL序列号
     */
    public String getCglseq() {
        return cglseq;
    }

    /**
     * 设置CGL序列号
     *
     * @param cglseq CGL序列号
     */
    public void setCglseq(String cglseq) {
        this.cglseq = cglseq;
    }

    /**
     * 获取币种
     *
     * @return CCY - 币种
     */
    public String getCcy() {
        return ccy;
    }

    /**
     * 设置币种
     *
     * @param ccy 币种
     */
    public void setCcy(String ccy) {
        this.ccy = ccy;
    }

    /**
     * 获取账号类型
     *
     * @return SIGN - 账号类型
     */
    public String getSign() {
        return sign;
    }

    /**
     * 设置账号类型
     *
     * @param sign 账号类型
     */
    public void setSign(String sign) {
        this.sign = sign;
    }

    /**
     * @return STR1
     */
    public String getStr1() {
        return str1;
    }

    /**
     * @param str1
     */
    public void setStr1(String str1) {
        this.str1 = str1;
    }

    /**
     * @return STR2
     */
    public String getStr2() {
        return str2;
    }

    /**
     * @param str2
     */
    public void setStr2(String str2) {
        this.str2 = str2;
    }
}
