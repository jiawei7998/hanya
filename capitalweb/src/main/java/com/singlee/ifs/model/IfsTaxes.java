package com.singlee.ifs.model;

import java.io.Serializable;
import java.math.BigDecimal;

/***
 * 税费维护表
 * @author copysun
 *
 */

public class IfsTaxes implements Serializable{
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	private BigDecimal id;

	/**
	 * 上清所-现券-纯券过户
	 */
	private BigDecimal sqsBondSettlement1;

	/**
	 * 上清所-现券-DPD
	 */
	private BigDecimal sqsBondSettlement2;

	/**
	 * 上清所-质押式-单券
	 */
	private BigDecimal sqsCrSettlement2Single;

	/**
	 * 上清所-质押式-多券
	 */
	private BigDecimal sqsCrSettlement2Multiple;

	/**
	 * 上清所-买断式
	 */
	private BigDecimal sqsOrSettlement2;

	/**
	 * 上清所-远期交易
	 */
	private BigDecimal sqsForwardSettlement2;

	/**
	 * 上清所-账户维护费-A类
	 */
	private BigDecimal sqsFeeClassA;

	/**
	 * 中债-现券-纯券过户
	 */
	private BigDecimal zzBondSettlement1;

	/**
	 * 中债-现券-DPD
	 */
	private BigDecimal zzBondSettlement2;

	/**
	 * 中债-质押式回购-单券
	 */
	private BigDecimal zzCrSettlement2Single;

	/**
	 * 中债-质押式回购-多券
	 */
	private BigDecimal zzCrSettlement2Multiple;

	/**
	 * 中债-买断式回购
	 */
	private BigDecimal zzOrSettlement2;

	/**
	 * 中债-远期交易
	 */
	private BigDecimal zzForwardSettlement2;

	/**
	 * 中债-账户维护费-超过200亿
	 */
	private BigDecimal zzFeeClassFirst1;

	/**
	 * 中债-账户维护费-小于200亿
	 */
	private BigDecimal zzFeeClassFirst2;

	/**
	 * 前台税费-融资类-1天
	 */
	private BigDecimal qtsfFinancing1;

	/**
	 * 前台税费-融资类-2天以上
	 */
	private BigDecimal qtsfFinancing2;


	/**
	 * 前台税费-买卖类交易
	 */
	private BigDecimal qtsfBs;

	/**
	 * 前台税费-终端费
	 */
	private BigDecimal qtsfTerminal;

	/**
	 * 创建时间
	 */
	private String createTime;

	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	public BigDecimal getSqsBondSettlement1() {
		return sqsBondSettlement1;
	}

	public void setSqsBondSettlement1(BigDecimal sqsBondSettlement1) {
		this.sqsBondSettlement1 = sqsBondSettlement1;
	}

	public BigDecimal getSqsBondSettlement2() {
		return sqsBondSettlement2;
	}

	public void setSqsBondSettlement2(BigDecimal sqsBondSettlement2) {
		this.sqsBondSettlement2 = sqsBondSettlement2;
	}


	public BigDecimal getSqsOrSettlement2() {
		return sqsOrSettlement2;
	}

	public void setSqsOrSettlement2(BigDecimal sqsOrSettlement2) {
		this.sqsOrSettlement2 = sqsOrSettlement2;
	}

	public BigDecimal getSqsForwardSettlement2() {
		return sqsForwardSettlement2;
	}

	public void setSqsForwardSettlement2(BigDecimal sqsForwardSettlement2) {
		this.sqsForwardSettlement2 = sqsForwardSettlement2;
	}

	public BigDecimal getSqsFeeClassA() {
		return sqsFeeClassA;
	}

	public void setSqsFeeClassA(BigDecimal sqsFeeClassA) {
		this.sqsFeeClassA = sqsFeeClassA;
	}

	public BigDecimal getZzBondSettlement1() {
		return zzBondSettlement1;
	}

	public void setZzBondSettlement1(BigDecimal zzBondSettlement1) {
		this.zzBondSettlement1 = zzBondSettlement1;
	}

	public BigDecimal getZzBondSettlement2() {
		return zzBondSettlement2;
	}

	public void setZzBondSettlement2(BigDecimal zzBondSettlement2) {
		this.zzBondSettlement2 = zzBondSettlement2;
	}


	public BigDecimal getZzOrSettlement2() {
		return zzOrSettlement2;
	}

	public void setZzOrSettlement2(BigDecimal zzOrSettlement2) {
		this.zzOrSettlement2 = zzOrSettlement2;
	}

	public BigDecimal getZzForwardSettlement2() {
		return zzForwardSettlement2;
	}

	public void setZzForwardSettlement2(BigDecimal zzForwardSettlement2) {
		this.zzForwardSettlement2 = zzForwardSettlement2;
	}

	public BigDecimal getZzFeeClassFirst1() {
		return zzFeeClassFirst1;
	}

	public void setZzFeeClassFirst1(BigDecimal zzFeeClassFirst1) {
		this.zzFeeClassFirst1 = zzFeeClassFirst1;
	}

	public BigDecimal getZzFeeClassFirst2() {
		return zzFeeClassFirst2;
	}

	public void setZzFeeClassFirst2(BigDecimal zzFeeClassFirst2) {
		this.zzFeeClassFirst2 = zzFeeClassFirst2;
	}

	public BigDecimal getQtsfFinancing1() {
		return qtsfFinancing1;
	}

	public void setQtsfFinancing1(BigDecimal qtsfFinancing1) {
		this.qtsfFinancing1 = qtsfFinancing1;
	}

	public BigDecimal getQtsfFinancing2() {
		return qtsfFinancing2;
	}

	public void setQtsfFinancing2(BigDecimal qtsfFinancing2) {
		this.qtsfFinancing2 = qtsfFinancing2;
	}

	public BigDecimal getQtsfBs() {
		return qtsfBs;
	}

	public void setQtsfBs(BigDecimal qtsfBs) {
		this.qtsfBs = qtsfBs;
	}

	public BigDecimal getQtsfTerminal() {
		return qtsfTerminal;
	}

	public void setQtsfTerminal(BigDecimal qtsfTerminal) {
		this.qtsfTerminal = qtsfTerminal;
	}

	public BigDecimal getSqsCrSettlement2Single() {
		return sqsCrSettlement2Single;
	}

	public void setSqsCrSettlement2Single(BigDecimal sqsCrSettlement2Single) {
		this.sqsCrSettlement2Single = sqsCrSettlement2Single;
	}

	public BigDecimal getSqsCrSettlement2Multiple() {
		return sqsCrSettlement2Multiple;
	}

	public void setSqsCrSettlement2Multiple(BigDecimal sqsCrSettlement2Multiple) {
		this.sqsCrSettlement2Multiple = sqsCrSettlement2Multiple;
	}

	public BigDecimal getZzCrSettlement2Single() {
		return zzCrSettlement2Single;
	}

	public void setZzCrSettlement2Single(BigDecimal zzCrSettlement2Single) {
		this.zzCrSettlement2Single = zzCrSettlement2Single;
	}

	public BigDecimal getZzCrSettlement2Multiple() {
		return zzCrSettlement2Multiple;
	}

	public void setZzCrSettlement2Multiple(BigDecimal zzCrSettlement2Multiple) {
		this.zzCrSettlement2Multiple = zzCrSettlement2Multiple;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
}
