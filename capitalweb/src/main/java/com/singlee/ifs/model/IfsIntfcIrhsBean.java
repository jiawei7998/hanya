package com.singlee.ifs.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class IfsIntfcIrhsBean implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String br;
	private String ccy;
	private String ratecode;
	private String yieldcurve;
	private String desmat;
	private BigDecimal intrate_8;
	private String sourse;
	private String lstmntdte;
	public String getBr() {
		return br;
	}
	public void setBr(String br) {
		this.br = br;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public String getRatecode() {
		return ratecode;
	}
	public void setRatecode(String ratecode) {
		this.ratecode = ratecode;
	}
	
	public String getYieldcurve() {
		return yieldcurve;
	}
	public void setYieldcurve(String yieldcurve) {
		this.yieldcurve = yieldcurve;
	}
	public String getDesmat() {
		return desmat;
	}
	public void setDesmat(String desmat) {
		this.desmat = desmat;
	}
	public BigDecimal getIntrate_8() {
		return intrate_8;
	}
	public void setIntrate_8(BigDecimal intrate_8) {
		this.intrate_8 = intrate_8;
	}
	public String getSourse() {
		return sourse;
	}
	public void setSourse(String sourse) {
		this.sourse = sourse;
	}
	public String getLstmntdte() {
		return lstmntdte;
	}
	public void setLstmntdte(String lstmntdte) {
		this.lstmntdte = lstmntdte;
	}
		
	

}
