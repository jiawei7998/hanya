package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "IFS_APPROVEMETAL_GOLD")
public class IfsApprovemetalGold extends IfsApproveBaseBean {
    private static final long serialVersionUID = 1L;
	
    private String type;

    private String currencyPair;

    private String price;

    private String direction1;
    
    private String amount1;

    private String direction2;
    
    private String amount2;

    private String tenor1;

    private String valueDate1;

    private String direction3;
    
    private String amount3;

    private String direction4;
    
    private String amount4;

    private String tenor2;

    private String valueDate2;

    private String settleMode;

	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCurrencyPair() {
		return currencyPair;
	}

	public void setCurrencyPair(String currencyPair) {
		this.currencyPair = currencyPair;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getDirection1() {
		return direction1;
	}

	public void setDirection1(String direction1) {
		this.direction1 = direction1;
	}

	public String getAmount1() {
		return amount1;
	}

	public void setAmount1(String amount1) {
		this.amount1 = amount1;
	}

	public String getDirection2() {
		return direction2;
	}

	public void setDirection2(String direction2) {
		this.direction2 = direction2;
	}

	public String getAmount2() {
		return amount2;
	}

	public void setAmount2(String amount2) {
		this.amount2 = amount2;
	}

	public String getTenor1() {
		return tenor1;
	}

	public void setTenor1(String tenor1) {
		this.tenor1 = tenor1;
	}

	public String getValueDate1() {
		return valueDate1;
	}

	public void setValueDate1(String valueDate1) {
		this.valueDate1 = valueDate1;
	}

	public String getDirection3() {
		return direction3;
	}

	public void setDirection3(String direction3) {
		this.direction3 = direction3;
	}

	public String getAmount3() {
		return amount3;
	}

	public void setAmount3(String amount3) {
		this.amount3 = amount3;
	}

	public String getDirection4() {
		return direction4;
	}

	public void setDirection4(String direction4) {
		this.direction4 = direction4;
	}

	public String getAmount4() {
		return amount4;
	}

	public void setAmount4(String amount4) {
		this.amount4 = amount4;
	}

	public String getTenor2() {
		return tenor2;
	}

	public void setTenor2(String tenor2) {
		this.tenor2 = tenor2;
	}

	public String getValueDate2() {
		return valueDate2;
	}

	public void setValueDate2(String valueDate2) {
		this.valueDate2 = valueDate2;
	}

	public String getSettleMode() {
		return settleMode;
	}

	public void setSettleMode(String settleMode) {
		this.settleMode = settleMode;
	}

  }