package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "IFS_CFETSFX_CSWAP")
public class IfsCfetsfxCswap extends FxCurrenyBasebean  {
	private static final long serialVersionUID = 1L;


	private String startDate;
	//开始日期
	private String notionalExchange;
	//本金交换
	private String stub;
	//残段
	private String unadjustmaturityDate;
	//未调整到日期
	private String spotRate;
	//即期汇率
	private String nonVanilla;
	//是否标准协议
	private String describe;
	//描述
	private String leftDirection;
	//付款方向
	private String leftNotion;
	//名义本金
	private String leftRate;
	//固定/浮动
	private String leftFixedFloat;
	//固定/浮动利率
	private String leftFrequency;
	//定息周期
	private String leftRule;
	//定息规则
	private String leftBasis;
	//计息方式
	private String leftPayment;
	//付息周期
	private String leftConvention;
	//起息计算方式
	private String rightDirection;
	//付款方向
	private String rightNotion;
	//名义本金
	private String rightRate;
	//固定/浮动
	private String rightFixedFloat;
	//固定/浮动利率
	private String rightFrequency;
	//定息周期
	private String rightRule;
	//定息规则
	private String rightBasis;
	//计息方式
	private String rightPayment;
	//付息周期
	private String rightConvention;
	//起息计算方式
	
	//会计处理:trading/hedge(套期保值)
	private String accountingMethod;
	
	//本方利率曲线
	private String leftDiscountCurve;
	
	//对方利率曲线
	private String rightDiscountCurve;
	
	//本方利率代码
	private String leftRateCode;
	
	//对方利率代码
	private String rightRateCode;

	//固定/浮动方向   值为P/R/X/L
	/***
	 * P:'RECFLT/PAYFIX','PAYFIX/RECFLT'
	 * R:'RECFIX/PAYFLT','PAYFLT/RECFIX'
	 * X:'RECFIX/PAYFIX','PAYFIX/RECFIX'
	 * L:'RECFLT/PAYFLT','PAYFLT/RECFLT'
	 */
	private String fixedFloatDir;
	
	//权重
	private String  weight;
	//占用授信主体
	private String custNo;
	//审批单号
	private String applyNo;
	//产品名称
	private String applyProd;
	
	//DV01Risk
	private String DV01Risk;
	
	//止损
	private String lossLimit;
	
	//久期限
	private String longLimit;
	
	//风险程度
	private String riskDegree;
	
	@Transient
	private String  prdNo;
	
	
	public String getPrdNo() {
		return prdNo;
	}

	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}

	@Override
    public String getCustNo() {
		return custNo;
	}

	@Override
    public void setCustNo(String custNo) {
		this.custNo = custNo;
	}

	public String getApplyNo() {
		return applyNo;
	}

	public void setApplyNo(String applyNo) {
		this.applyNo = applyNo;
	}

	public String getApplyProd() {
		return applyProd;
	}

	public void setApplyProd(String applyProd) {
		this.applyProd = applyProd;
	}

	@Override
    public String getStartDate() {
		return startDate;
	}

	@Override
    public void setStartDate(String startDate) {
		this.startDate = startDate == null ? null : startDate.trim();
	}

	public String getNotionalExchange() {
		return notionalExchange;
	}

	public void setNotionalExchange(String notionalExchange) {
		this.notionalExchange = notionalExchange == null ? null : notionalExchange.trim();
	}

	public String getStub() {
		return stub;
	}

	public void setStub(String stub) {
		this.stub = stub == null ? null : stub.trim();
	}

	public String getUnadjustmaturityDate() {
		return unadjustmaturityDate;
	}

	public void setUnadjustmaturityDate(String unadjustmaturityDate) {
		this.unadjustmaturityDate = unadjustmaturityDate == null ? null : unadjustmaturityDate.trim();
	}

	public String getSpotRate() {
		return spotRate;
	}

	public void setSpotRate(String spotRate) {
		this.spotRate = spotRate == null ? null : spotRate.trim();
	}

	public String getNonVanilla() {
		return nonVanilla;
	}

	public void setNonVanilla(String nonVanilla) {
		this.nonVanilla = nonVanilla == null ? null : nonVanilla.trim();
	}

	public String getDescribe() {
		return describe;
	}

	public void setDescribe(String describe) {
		this.describe = describe == null ? null : describe.trim();
	}

	public String getLeftDirection() {
		return leftDirection;
	}

	public void setLeftDirection(String leftDirection) {
		this.leftDirection = leftDirection == null ? null : leftDirection.trim();
	}

	public String getLeftNotion() {
		return leftNotion;
	}

	public void setLeftNotion(String leftNotion) {
		this.leftNotion = leftNotion == null ? null : leftNotion.trim();
	}

	public String getLeftRate() {
		return leftRate;
	}

	public void setLeftRate(String leftRate) {
		this.leftRate = leftRate == null ? null : leftRate.trim();
	}

	public String getLeftFrequency() {
		return leftFrequency;
	}

	public void setLeftFrequency(String leftFrequency) {
		this.leftFrequency = leftFrequency == null ? null : leftFrequency.trim();
	}

	public String getLeftRule() {
		return leftRule;
	}

	public void setLeftRule(String leftRule) {
		this.leftRule = leftRule == null ? null : leftRule.trim();
	}

	public String getLeftBasis() {
		return leftBasis;
	}

	public void setLeftBasis(String leftBasis) {
		this.leftBasis = leftBasis == null ? null : leftBasis.trim();
	}

	public String getLeftPayment() {
		return leftPayment;
	}

	public void setLeftPayment(String leftPayment) {
		this.leftPayment = leftPayment == null ? null : leftPayment.trim();
	}

	public String getLeftConvention() {
		return leftConvention;
	}

	public void setLeftConvention(String leftConvention) {
		this.leftConvention = leftConvention == null ? null : leftConvention.trim();
	}

	public String getRightDirection() {
		return rightDirection;
	}

	public void setRightDirection(String rightDirection) {
		this.rightDirection = rightDirection == null ? null : rightDirection.trim();
	}

	public String getRightNotion() {
		return rightNotion;
	}

	public void setRightNotion(String rightNotion) {
		this.rightNotion = rightNotion == null ? null : rightNotion.trim();
	}

	public String getRightRate() {
		return rightRate;
	}

	public void setRightRate(String rightRate) {
		this.rightRate = rightRate == null ? null : rightRate.trim();
	}

	public String getRightFrequency() {
		return rightFrequency;
	}

	public void setRightFrequency(String rightFrequency) {
		this.rightFrequency = rightFrequency == null ? null : rightFrequency.trim();
	}

	public String getRightRule() {
		return rightRule;
	}

	public void setRightRule(String rightRule) {
		this.rightRule = rightRule == null ? null : rightRule.trim();
	}

	public String getRightBasis() {
		return rightBasis;
	}

	public void setRightBasis(String rightBasis) {
		this.rightBasis = rightBasis == null ? null : rightBasis.trim();
	}

	public String getRightPayment() {
		return rightPayment;
	}

	public void setRightPayment(String rightPayment) {
		this.rightPayment = rightPayment == null ? null : rightPayment.trim();
	}

	public String getRightConvention() {
		return rightConvention;
	}

	public void setRightConvention(String rightConvention) {
		this.rightConvention = rightConvention == null ? null : rightConvention.trim();
	}

	public String getAccountingMethod() {
		return accountingMethod;
	}

	public void setAccountingMethod(String accountingMethod) {
		this.accountingMethod = accountingMethod;
	}

	public String getLeftDiscountCurve() {
		return leftDiscountCurve;
	}

	public void setLeftDiscountCurve(String leftDiscountCurve) {
		this.leftDiscountCurve = leftDiscountCurve;
	}

	public String getRightDiscountCurve() {
		return rightDiscountCurve;
	}

	public void setRightDiscountCurve(String rightDiscountCurve) {
		this.rightDiscountCurve = rightDiscountCurve;
	}

	public String getLeftRateCode() {
		return leftRateCode;
	}

	public void setLeftRateCode(String leftRateCode) {
		this.leftRateCode = leftRateCode;
	}

	public String getRightRateCode() {
		return rightRateCode;
	}

	public void setRightRateCode(String rightRateCode) {
		this.rightRateCode = rightRateCode;
	}

	public String getFixedFloatDir() {
		return fixedFloatDir;
	}

	public void setFixedFloatDir(String fixedFloatDir) {
		this.fixedFloatDir = fixedFloatDir;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}
	
	public String getLeftFixedFloat() {
		return leftFixedFloat;
	}

	public void setLeftFixedFloat(String leftFixedFloat) {
		this.leftFixedFloat = leftFixedFloat;
	}

	public String getRightFixedFloat() {
		return rightFixedFloat;
	}

	public void setRightFixedFloat(String rightFixedFloat) {
		this.rightFixedFloat = rightFixedFloat;
	}
	public String getDV01Risk() {
		return DV01Risk;
	}

	public void setDV01Risk(String dV01Risk) {
		this.DV01Risk = dV01Risk;
	}

	public String getLossLimit() {
		return lossLimit;
	}

	public void setLossLimit(String lossLimit) {
		this.lossLimit = lossLimit;
	}

	public String getLongLimit() {
		return longLimit;
	}

	public void setLongLimit(String longLimit) {
		this.longLimit = longLimit;
	}

	public String getRiskDegree() {
		return riskDegree;
	}

	public void setRiskDegree(String riskDegree) {
		this.riskDegree = riskDegree;
	}

}