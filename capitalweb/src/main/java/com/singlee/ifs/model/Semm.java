package com.singlee.ifs.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class Semm implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String FXFIG_SVCN;
	private String FXFIG_IBCD;
	private String FXFIG_GWAM;
	private String FXFIG_GEOR;
	private String FXFIG_OPNO;
	private String FIFB10_SECURITY_ID;
	private String FIFB10_COUPON_STYLE;
	private String FIFB10_BOND_TYPE;
	private String FIFB10_ISSUE_IL;
	private String FIFB10_EXP_IL;
	private String FIFB10_ISSUE_MTD;
	private String FIFB10_ISSUE_MARKET_NM;
	private String FIFB10_ISSUE_MKT_NATION_CD;
	private String FIFB10_REPAY_PRIORITY;
	private String FIFB10_CREDIT_AGENCY;
	private BigDecimal FIFB10_TOTAL_ISSUE_AMT;
	private String FIFB10_TOTAL_ISSUE_CCY;
	private String FIFB10_REPAY_MTD;
	private String FIFB10_FST_REPAY_IL;
	private BigDecimal FIFB10_REPAY_PRICE;
	private BigDecimal FIFB10_REPAY_AMT;
	private String FIFB10_FIX_FLT_GB;
	private BigDecimal FIFB10_COUPON_SPREAD_RT;
	private String FIFB10_CAP_RT_YN;
	private BigDecimal FIFB10_CAP_RT;
	private String FIFB10_FLOOR_RT_YN;
	private BigDecimal FIFB10_FLOOR_RT;
	private String FIFB10_ADV_ARR_GB;
	private String FIFB10_INT_FREQ;
	private String FIFB10_COUPON_ADJ_YN;
	private String FIFB10_BSNS_DAY_RULE;
	private String FIFB10_ACCR_TYPE;
	private String FIFB10_SINGLE_BOTH_TYPE;
	private String FIFB10_BASE_IL;
	private String FIFB10_ISSUE_ID;
	private String FIFB10_RATE_CCY ;
	private String FIFB10_RATE_FREQ;
	public String getFXFIG_SVCN() {
		return FXFIG_SVCN;
	}
	public void setFXFIG_SVCN(String fXFIG_SVCN) {
		FXFIG_SVCN = fXFIG_SVCN;
	}
	public String getFXFIG_IBCD() {
		return FXFIG_IBCD;
	}
	public void setFXFIG_IBCD(String fXFIG_IBCD) {
		FXFIG_IBCD = fXFIG_IBCD;
	}
	public String getFXFIG_GWAM() {
		return FXFIG_GWAM;
	}
	public void setFXFIG_GWAM(String fXFIG_GWAM) {
		FXFIG_GWAM = fXFIG_GWAM;
	}
	public String getFXFIG_GEOR() {
		return FXFIG_GEOR;
	}
	public void setFXFIG_GEOR(String fXFIG_GEOR) {
		FXFIG_GEOR = fXFIG_GEOR;
	}
	public String getFXFIG_OPNO() {
		return FXFIG_OPNO;
	}
	public void setFXFIG_OPNO(String fXFIG_OPNO) {
		FXFIG_OPNO = fXFIG_OPNO;
	}
	public String getFIFB10_SECURITY_ID() {
		return FIFB10_SECURITY_ID;
	}
	public void setFIFB10_SECURITY_ID(String fIFB10_SECURITY_ID) {
		FIFB10_SECURITY_ID = fIFB10_SECURITY_ID;
	}
	public String getFIFB10_COUPON_STYLE() {
		return FIFB10_COUPON_STYLE;
	}
	public void setFIFB10_COUPON_STYLE(String fIFB10_COUPON_STYLE) {
		FIFB10_COUPON_STYLE = fIFB10_COUPON_STYLE;
	}
	public String getFIFB10_BOND_TYPE() {
		return FIFB10_BOND_TYPE;
	}
	public void setFIFB10_BOND_TYPE(String fIFB10_BOND_TYPE) {
		FIFB10_BOND_TYPE = fIFB10_BOND_TYPE;
	}
	public String getFIFB10_ISSUE_IL() {
		return FIFB10_ISSUE_IL;
	}
	public void setFIFB10_ISSUE_IL(String fIFB10_ISSUE_IL) {
		FIFB10_ISSUE_IL = fIFB10_ISSUE_IL;
	}
	public String getFIFB10_EXP_IL() {
		return FIFB10_EXP_IL;
	}
	public void setFIFB10_EXP_IL(String fIFB10_EXP_IL) {
		FIFB10_EXP_IL = fIFB10_EXP_IL;
	}
	public String getFIFB10_ISSUE_MTD() {
		return FIFB10_ISSUE_MTD;
	}
	public void setFIFB10_ISSUE_MTD(String fIFB10_ISSUE_MTD) {
		FIFB10_ISSUE_MTD = fIFB10_ISSUE_MTD;
	}
	public String getFIFB10_ISSUE_MARKET_NM() {
		return FIFB10_ISSUE_MARKET_NM;
	}
	public void setFIFB10_ISSUE_MARKET_NM(String fIFB10_ISSUE_MARKET_NM) {
		FIFB10_ISSUE_MARKET_NM = fIFB10_ISSUE_MARKET_NM;
	}
	public String getFIFB10_ISSUE_MKT_NATION_CD() {
		return FIFB10_ISSUE_MKT_NATION_CD;
	}
	public void setFIFB10_ISSUE_MKT_NATION_CD(String fIFB10_ISSUE_MKT_NATION_CD) {
		FIFB10_ISSUE_MKT_NATION_CD = fIFB10_ISSUE_MKT_NATION_CD;
	}
	public String getFIFB10_REPAY_PRIORITY() {
		return FIFB10_REPAY_PRIORITY;
	}
	public void setFIFB10_REPAY_PRIORITY(String fIFB10_REPAY_PRIORITY) {
		FIFB10_REPAY_PRIORITY = fIFB10_REPAY_PRIORITY;
	}
	public String getFIFB10_CREDIT_AGENCY() {
		return FIFB10_CREDIT_AGENCY;
	}
	public void setFIFB10_CREDIT_AGENCY(String fIFB10_CREDIT_AGENCY) {
		FIFB10_CREDIT_AGENCY = fIFB10_CREDIT_AGENCY;
	}
	public BigDecimal getFIFB10_TOTAL_ISSUE_AMT() {
		return FIFB10_TOTAL_ISSUE_AMT;
	}
	public void setFIFB10_TOTAL_ISSUE_AMT(BigDecimal fIFB10_TOTAL_ISSUE_AMT) {
		FIFB10_TOTAL_ISSUE_AMT = fIFB10_TOTAL_ISSUE_AMT;
	}
	public String getFIFB10_TOTAL_ISSUE_CCY() {
		return FIFB10_TOTAL_ISSUE_CCY;
	}
	public void setFIFB10_TOTAL_ISSUE_CCY(String fIFB10_TOTAL_ISSUE_CCY) {
		FIFB10_TOTAL_ISSUE_CCY = fIFB10_TOTAL_ISSUE_CCY;
	}
	public String getFIFB10_REPAY_MTD() {
		return FIFB10_REPAY_MTD;
	}
	public void setFIFB10_REPAY_MTD(String fIFB10_REPAY_MTD) {
		FIFB10_REPAY_MTD = fIFB10_REPAY_MTD;
	}
	public String getFIFB10_FST_REPAY_IL() {
		return FIFB10_FST_REPAY_IL;
	}
	public void setFIFB10_FST_REPAY_IL(String fIFB10_FST_REPAY_IL) {
		FIFB10_FST_REPAY_IL = fIFB10_FST_REPAY_IL;
	}
	public BigDecimal getFIFB10_REPAY_PRICE() {
		return FIFB10_REPAY_PRICE;
	}
	public void setFIFB10_REPAY_PRICE(BigDecimal fIFB10_REPAY_PRICE) {
		FIFB10_REPAY_PRICE = fIFB10_REPAY_PRICE;
	}
	public BigDecimal getFIFB10_REPAY_AMT() {
		return FIFB10_REPAY_AMT;
	}
	public void setFIFB10_REPAY_AMT(BigDecimal fIFB10_REPAY_AMT) {
		FIFB10_REPAY_AMT = fIFB10_REPAY_AMT;
	}
	public String getFIFB10_FIX_FLT_GB() {
		return FIFB10_FIX_FLT_GB;
	}
	public void setFIFB10_FIX_FLT_GB(String fIFB10_FIX_FLT_GB) {
		FIFB10_FIX_FLT_GB = fIFB10_FIX_FLT_GB;
	}
	public BigDecimal getFIFB10_COUPON_SPREAD_RT() {
		return FIFB10_COUPON_SPREAD_RT;
	}
	public void setFIFB10_COUPON_SPREAD_RT(BigDecimal fIFB10_COUPON_SPREAD_RT) {
		FIFB10_COUPON_SPREAD_RT = fIFB10_COUPON_SPREAD_RT;
	}
	public String getFIFB10_CAP_RT_YN() {
		return FIFB10_CAP_RT_YN;
	}
	public void setFIFB10_CAP_RT_YN(String fIFB10_CAP_RT_YN) {
		FIFB10_CAP_RT_YN = fIFB10_CAP_RT_YN;
	}
	public BigDecimal getFIFB10_CAP_RT() {
		return FIFB10_CAP_RT;
	}
	public void setFIFB10_CAP_RT(BigDecimal fIFB10_CAP_RT) {
		FIFB10_CAP_RT = fIFB10_CAP_RT;
	}
	public String getFIFB10_FLOOR_RT_YN() {
		return FIFB10_FLOOR_RT_YN;
	}
	public void setFIFB10_FLOOR_RT_YN(String fIFB10_FLOOR_RT_YN) {
		FIFB10_FLOOR_RT_YN = fIFB10_FLOOR_RT_YN;
	}
	public BigDecimal getFIFB10_FLOOR_RT() {
		return FIFB10_FLOOR_RT;
	}
	public void setFIFB10_FLOOR_RT(BigDecimal fIFB10_FLOOR_RT) {
		FIFB10_FLOOR_RT = fIFB10_FLOOR_RT;
	}
	public String getFIFB10_ADV_ARR_GB() {
		return FIFB10_ADV_ARR_GB;
	}
	public void setFIFB10_ADV_ARR_GB(String fIFB10_ADV_ARR_GB) {
		FIFB10_ADV_ARR_GB = fIFB10_ADV_ARR_GB;
	}
	public String getFIFB10_INT_FREQ() {
		return FIFB10_INT_FREQ;
	}
	public void setFIFB10_INT_FREQ(String fIFB10_INT_FREQ) {
		FIFB10_INT_FREQ = fIFB10_INT_FREQ;
	}
	public String getFIFB10_COUPON_ADJ_YN() {
		return FIFB10_COUPON_ADJ_YN;
	}
	public void setFIFB10_COUPON_ADJ_YN(String fIFB10_COUPON_ADJ_YN) {
		FIFB10_COUPON_ADJ_YN = fIFB10_COUPON_ADJ_YN;
	}
	public String getFIFB10_BSNS_DAY_RULE() {
		return FIFB10_BSNS_DAY_RULE;
	}
	public void setFIFB10_BSNS_DAY_RULE(String fIFB10_BSNS_DAY_RULE) {
		FIFB10_BSNS_DAY_RULE = fIFB10_BSNS_DAY_RULE;
	}
	public String getFIFB10_ACCR_TYPE() {
		return FIFB10_ACCR_TYPE;
	}
	public void setFIFB10_ACCR_TYPE(String fIFB10_ACCR_TYPE) {
		FIFB10_ACCR_TYPE = fIFB10_ACCR_TYPE;
	}
	public String getFIFB10_SINGLE_BOTH_TYPE() {
		return FIFB10_SINGLE_BOTH_TYPE;
	}
	public void setFIFB10_SINGLE_BOTH_TYPE(String fIFB10_SINGLE_BOTH_TYPE) {
		FIFB10_SINGLE_BOTH_TYPE = fIFB10_SINGLE_BOTH_TYPE;
	}
	public String getFIFB10_BASE_IL() {
		return FIFB10_BASE_IL;
	}
	public void setFIFB10_BASE_IL(String fIFB10_BASE_IL) {
		FIFB10_BASE_IL = fIFB10_BASE_IL;
	}
	public String getFIFB10_ISSUE_ID() {
		return FIFB10_ISSUE_ID;
	}
	public void setFIFB10_ISSUE_ID(String fIFB10_ISSUE_ID) {
		FIFB10_ISSUE_ID = fIFB10_ISSUE_ID;
	}
	public String getFIFB10_RATE_CCY() {
		return FIFB10_RATE_CCY;
	}
	public void setFIFB10_RATE_CCY(String fIFB10_RATE_CCY) {
		FIFB10_RATE_CCY = fIFB10_RATE_CCY;
	}
	public String getFIFB10_RATE_FREQ() {
		return FIFB10_RATE_FREQ;
	}
	public void setFIFB10_RATE_FREQ(String fIFB10_RATE_FREQ) {
		FIFB10_RATE_FREQ = fIFB10_RATE_FREQ;
	}
}
