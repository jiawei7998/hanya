package com.singlee.ifs.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class IfsIntfcFi implements Serializable{
	
	private static final long serialVersionUID = 1L;
	 
	private String server;
	
	private String inputCode;
	
	private String subjectCode;
	
	private String tradCode;
	
	private String traderCode;
	
	private String prodCode;
	
	private String prodName;
	
	private String secid;
	
	private String cno;
	
	private String dealDate;
	
	private String settDate;
	
	private String matDate;
	
	private int remainMonth;
	
	private String tranType;
	
	private String purpose;
	
	private String settleMode;
	
	private String nominalCcy;
	
	private BigDecimal nominalAmt;
	
	private BigDecimal buyPrice;
	
	private String buyCcy;
	
	private BigDecimal buyAmt;
	
	private BigDecimal fullPrice;
	
	private BigDecimal fullAmt;
	
	private BigDecimal discountAmt;
	
	private String lstCouponDate;
	
	private String nextCouponDate;
	
	private BigDecimal indexRate;
	
	private BigDecimal spreadRate;
	
	private BigDecimal totalRate;
	
	private BigDecimal intAmt;
	
	private String intCcy;
	
	private BigDecimal accrualIntAmt;
	
	private String accrualIntCcy;
	
	private String dealno;
	
	private String seq;
	
	private String longShortGb;//LONGSHORTGB
	
	private String cancelEnableYn;//CANCELENABLEYN

	private String collateRalinYn;//COLLATERALINYN
	
	private String intRcvIl;//INTRCVIL
	
	private String dealerId;//DEALERID
	
	private String setlCcy;
	
	private BigDecimal setlAmt;
	
	private String marketGb;
	
	private String inoutGb;
	
	private String counterPartyNm;
	
	private String acctIntCcy;
	
	/**
	 * 清算信息
	 */
	private String CONFIRM_YN;
	private String CONFIRM_IL;
	private String PO_YN;
	private String PO_IL;
	private String OUR_RCV_DEPO_CD;
	private String OUR_RCV_DEPO_NM;
	private String OUR_PAY_DEPO_CD;
	private String OUR_PAY_DEPO_NM;
	private String THR_RCV_DEPO_BIC;
	private String THR_RCV_DEPO_NM;
	private String CNAPS_YN;
	private String CNAPS_RCV_ACCT_NO;
	private String CNAPS_CD;
	private String CNAPS_NM;
	private String CNAPS_RCV_ACCT_NM;
		
	/**
	 * @return the cNAPS_RCV_ACCT_NM
	 */
	public String getCNAPS_RCV_ACCT_NM() {
		return CNAPS_RCV_ACCT_NM;
	}

	/**
	 * @param cnaps_rcv_acct_nm the cNAPS_RCV_ACCT_NM to set
	 */
	public void setCNAPS_RCV_ACCT_NM(String cnaps_rcv_acct_nm) {
		CNAPS_RCV_ACCT_NM = cnaps_rcv_acct_nm;
	}

	/**
	 * @return the cONFIRM_YN
	 */
	public String getCONFIRM_YN() {
		return CONFIRM_YN;
	}

	/**
	 * @param confirm_yn the cONFIRM_YN to set
	 */
	public void setCONFIRM_YN(String confirm_yn) {
		CONFIRM_YN = confirm_yn;
	}

	/**
	 * @return the cONFIRM_IL
	 */
	public String getCONFIRM_IL() {
		return CONFIRM_IL;
	}

	/**
	 * @param confirm_il the cONFIRM_IL to set
	 */
	public void setCONFIRM_IL(String confirm_il) {
		CONFIRM_IL = confirm_il;
	}

	/**
	 * @return the pO_YN
	 */
	public String getPO_YN() {
		return PO_YN;
	}

	/**
	 * @param po_yn the pO_YN to set
	 */
	public void setPO_YN(String po_yn) {
		PO_YN = po_yn;
	}

	/**
	 * @return the pO_IL
	 */
	public String getPO_IL() {
		return PO_IL;
	}

	/**
	 * @param po_il the pO_IL to set
	 */
	public void setPO_IL(String po_il) {
		PO_IL = po_il;
	}

	/**
	 * @return the oUR_RCV_DEPO_CD
	 */
	public String getOUR_RCV_DEPO_CD() {
		return OUR_RCV_DEPO_CD;
	}

	/**
	 * @param our_rcv_depo_cd the oUR_RCV_DEPO_CD to set
	 */
	public void setOUR_RCV_DEPO_CD(String our_rcv_depo_cd) {
		OUR_RCV_DEPO_CD = our_rcv_depo_cd;
	}

	/**
	 * @return the oUR_RCV_DEPO_NM
	 */
	public String getOUR_RCV_DEPO_NM() {
		return OUR_RCV_DEPO_NM;
	}

	/**
	 * @param our_rcv_depo_nm the oUR_RCV_DEPO_NM to set
	 */
	public void setOUR_RCV_DEPO_NM(String our_rcv_depo_nm) {
		OUR_RCV_DEPO_NM = our_rcv_depo_nm;
	}

	/**
	 * @return the oUR_PAY_DEPO_CD
	 */
	public String getOUR_PAY_DEPO_CD() {
		return OUR_PAY_DEPO_CD;
	}

	/**
	 * @param our_pay_depo_cd the oUR_PAY_DEPO_CD to set
	 */
	public void setOUR_PAY_DEPO_CD(String our_pay_depo_cd) {
		OUR_PAY_DEPO_CD = our_pay_depo_cd;
	}

	/**
	 * @return the oUR_PAY_DEPO_NM
	 */
	public String getOUR_PAY_DEPO_NM() {
		return OUR_PAY_DEPO_NM;
	}

	/**
	 * @param our_pay_depo_nm the oUR_PAY_DEPO_NM to set
	 */
	public void setOUR_PAY_DEPO_NM(String our_pay_depo_nm) {
		OUR_PAY_DEPO_NM = our_pay_depo_nm;
	}

	/**
	 * @return the tHR_RCV_DEPO_BIC
	 */
	public String getTHR_RCV_DEPO_BIC() {
		return THR_RCV_DEPO_BIC;
	}

	/**
	 * @param thr_rcv_depo_bic the tHR_RCV_DEPO_BIC to set
	 */
	public void setTHR_RCV_DEPO_BIC(String thr_rcv_depo_bic) {
		THR_RCV_DEPO_BIC = thr_rcv_depo_bic;
	}

	/**
	 * @return the tHR_RCV_DEPO_NM
	 */
	public String getTHR_RCV_DEPO_NM() {
		return THR_RCV_DEPO_NM;
	}

	/**
	 * @param thr_rcv_depo_nm the tHR_RCV_DEPO_NM to set
	 */
	public void setTHR_RCV_DEPO_NM(String thr_rcv_depo_nm) {
		THR_RCV_DEPO_NM = thr_rcv_depo_nm;
	}

	/**
	 * @return the cNAPS_YN
	 */
	public String getCNAPS_YN() {
		return CNAPS_YN;
	}

	/**
	 * @param cnaps_yn the cNAPS_YN to set
	 */
	public void setCNAPS_YN(String cnaps_yn) {
		CNAPS_YN = cnaps_yn;
	}

	/**
	 * @return the cNAPS_RCV_ACCT_NO
	 */
	public String getCNAPS_RCV_ACCT_NO() {
		return CNAPS_RCV_ACCT_NO;
	}

	/**
	 * @param cnaps_rcv_acct_no the cNAPS_RCV_ACCT_NO to set
	 */
	public void setCNAPS_RCV_ACCT_NO(String cnaps_rcv_acct_no) {
		CNAPS_RCV_ACCT_NO = cnaps_rcv_acct_no;
	}

	/**
	 * @return the cNAPS_CD
	 */
	public String getCNAPS_CD() {
		return CNAPS_CD;
	}

	/**
	 * @param cnaps_cd the cNAPS_CD to set
	 */
	public void setCNAPS_CD(String cnaps_cd) {
		CNAPS_CD = cnaps_cd;
	}

	/**
	 * @return the cNAPS_NM
	 */
	public String getCNAPS_NM() {
		return CNAPS_NM;
	}

	/**
	 * @param cnaps_nm the cNAPS_NM to set
	 */
	public void setCNAPS_NM(String cnaps_nm) {
		CNAPS_NM = cnaps_nm;
	}

	/**
	 * @return the acctIntCcy
	 */
	public String getAcctIntCcy() {
		return acctIntCcy;
	}

	/**
	 * @param acctIntCcy the acctIntCcy to set
	 */
	public void setAcctIntCcy(String acctIntCcy) {
		this.acctIntCcy = acctIntCcy;
	}

	/**
	 * @return the counterPartyNm
	 */
	public String getCounterPartyNm() {
		return counterPartyNm;
	}

	/**
	 * @param counterPartyNm the counterPartyNm to set
	 */
	public void setCounterPartyNm(String counterPartyNm) {
		this.counterPartyNm = counterPartyNm;
	}

	/**
	 * @return the marketGb
	 */
	public String getMarketGb() {
		return marketGb;
	}

	/**
	 * @param marketGb the marketGb to set
	 */
	public void setMarketGb(String marketGb) {
		this.marketGb = marketGb;
	}

	/**
	 * @return the inoutGb
	 */
	public String getInoutGb() {
		return inoutGb;
	}

	/**
	 * @param inoutGb the inoutGb to set
	 */
	public void setInoutGb(String inoutGb) {
		this.inoutGb = inoutGb;
	}

	/**
	 * @return the setlCcy
	 */
	public String getSetlCcy() {
		return setlCcy;
	}

	/**
	 * @param setlCcy the setlCcy to set
	 */
	public void setSetlCcy(String setlCcy) {
		this.setlCcy = setlCcy;
	}

	/**
	 * @return the setlAmt
	 */
	public BigDecimal getSetlAmt() {
		return setlAmt;
	}

	/**
	 * @param setlAmt the setlAmt to set
	 */
	public void setSetlAmt(BigDecimal setlAmt) {
		this.setlAmt = setlAmt;
	}

	public String getDealerId() {
		return dealerId;
	}

	public void setDealerId(String dealerId) {
		this.dealerId = dealerId;
	}

	/**
	 * @return the server
	 */
	public String getServer() {
		return server;
	}

	/**
	 * @param server the server to set
	 */
	public void setServer(String server) {
		this.server = server;
	}

	/**
	 * @return the inputCode
	 */
	public String getInputCode() {
		return inputCode;
	}

	/**
	 * @param inputCode the inputCode to set
	 */
	public void setInputCode(String inputCode) {
		this.inputCode = inputCode;
	}

	/**
	 * @return the subjectCode
	 */
	public String getSubjectCode() {
		return subjectCode;
	}

	/**
	 * @param subjectCode the subjectCode to set
	 */
	public void setSubjectCode(String subjectCode) {
		this.subjectCode = subjectCode;
	}

	/**
	 * @return the tradCode
	 */
	public String getTradCode() {
		return tradCode;
	}

	/**
	 * @param tradCode the tradCode to set
	 */
	public void setTradCode(String tradCode) {
		this.tradCode = tradCode;
	}

	/**
	 * @return the traderCode
	 */
	public String getTraderCode() {
		return traderCode;
	}

	/**
	 * @param traderCode the traderCode to set
	 */
	public void setTraderCode(String traderCode) {
		this.traderCode = traderCode;
	}

	/**
	 * @return the prodCode
	 */
	public String getProdCode() {
		return prodCode;
	}

	/**
	 * @param prodCode the prodCode to set
	 */
	public void setProdCode(String prodCode) {
		this.prodCode = prodCode;
	}

	/**
	 * @return the secid
	 */
	public String getSecid() {
		return secid;
	}

	/**
	 * @param secid the secid to set
	 */
	public void setSecid(String secid) {
		this.secid = secid;
	}

	/**
	 * @return the cno
	 */
	public String getCno() {
		return cno;
	}

	/**
	 * @param cno the cno to set
	 */
	public void setCno(String cno) {
		this.cno = cno;
	}

	/**
	 * @return the dealDate
	 */
	public String getDealDate() {
		return dealDate;
	}

	/**
	 * @param dealDate the dealDate to set
	 */
	public void setDealDate(String dealDate) {
		this.dealDate = dealDate;
	}

	/**
	 * @return the settDate
	 */
	public String getSettDate() {
		return settDate;
	}

	/**
	 * @param settDate the settDate to set
	 */
	public void setSettDate(String settDate) {
		this.settDate = settDate;
	}

	
	/**
	 * @return the matDate
	 */
	public String getMatDate() {
		return matDate;
	}

	/**
	 * @param matDate the matDate to set
	 */
	public void setMatDate(String matDate) {
		this.matDate = matDate;
	}

	/**
	 * @return the remainMonth
	 */
	public int getRemainMonth() {
		return remainMonth;
	}

	/**
	 * @param remainMonth the remainMonth to set
	 */
	public void setRemainMonth(int remainMonth) {
		this.remainMonth = remainMonth;
	}

	/**
	 * @return the tranType
	 */
	public String getTranType() {
		return tranType;
	}

	/**
	 * @param tranType the tranType to set
	 */
	public void setTranType(String tranType) {
		this.tranType = tranType;
	}

	/**
	 * @return the purpose
	 */
	public String getPurpose() {
		return purpose;
	}

	/**
	 * @param purpose the purpose to set
	 */
	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	/**
	 * @return the settleMode
	 */
	public String getSettleMode() {
		return settleMode;
	}

	/**
	 * @param settleMode the settleMode to set
	 */
	public void setSettleMode(String settleMode) {
		this.settleMode = settleMode;
	}

	/**
	 * @return the nominalCcy
	 */
	public String getNominalCcy() {
		return nominalCcy;
	}

	/**
	 * @param nominalCcy the nominalCcy to set
	 */
	public void setNominalCcy(String nominalCcy) {
		this.nominalCcy = nominalCcy;
	}

	/**
	 * @return the nominalAmt
	 */
	public BigDecimal getNominalAmt() {
		return nominalAmt;
	}

	/**
	 * @param nominalAmt the nominalAmt to set
	 */
	public void setNominalAmt(BigDecimal nominalAmt) {
		this.nominalAmt = nominalAmt;
	}

	/**
	 * @return the buyPrice
	 */
	public BigDecimal getBuyPrice() {
		return buyPrice;
	}

	/**
	 * @param buyPrice the buyPrice to set
	 */
	public void setBuyPrice(BigDecimal buyPrice) {
		this.buyPrice = buyPrice;
	}

	/**
	 * @return the buyCcy
	 */
	public String getBuyCcy() {
		return buyCcy;
	}

	/**
	 * @param buyCcy the buyCcy to set
	 */
	public void setBuyCcy(String buyCcy) {
		this.buyCcy = buyCcy;
	}

	/**
	 * @return the buyAmt
	 */
	public BigDecimal getBuyAmt() {
		return buyAmt;
	}

	/**
	 * @param buyAmt the buyAmt to set
	 */
	public void setBuyAmt(BigDecimal buyAmt) {
		this.buyAmt = buyAmt;
	}

	/**
	 * @return the fullPrice
	 */
	public BigDecimal getFullPrice() {
		return fullPrice;
	}

	/**
	 * @param fullPrice the fullPrice to set
	 */
	public void setFullPrice(BigDecimal fullPrice) {
		this.fullPrice = fullPrice;
	}

	/**
	 * @return the fullAmt
	 */
	public BigDecimal getFullAmt() {
		return fullAmt;
	}

	/**
	 * @param fullAmt the fullAmt to set
	 */
	public void setFullAmt(BigDecimal fullAmt) {
		this.fullAmt = fullAmt;
	}

	/**
	 * @return the discountAmt
	 */
	public BigDecimal getDiscountAmt() {
		return discountAmt;
	}

	/**
	 * @param discountAmt the discountAmt to set
	 */
	public void setDiscountAmt(BigDecimal discountAmt) {
		this.discountAmt = discountAmt;
	}

	/**
	 * @return the lstCouponDate
	 */
	public String getLstCouponDate() {
		return lstCouponDate;
	}

	/**
	 * @param lstCouponDate the lstCouponDate to set
	 */
	public void setLstCouponDate(String lstCouponDate) {
		this.lstCouponDate = lstCouponDate;
	}

	/**
	 * @return the nextCouponDate
	 */
	public String getNextCouponDate() {
		return nextCouponDate;
	}

	/**
	 * @param nextCouponDate the nextCouponDate to set
	 */
	public void setNextCouponDate(String nextCouponDate) {
		this.nextCouponDate = nextCouponDate;
	}

	/**
	 * @return the indexRate
	 */
	public BigDecimal getIndexRate() {
		return indexRate;
	}

	/**
	 * @param indexRate the indexRate to set
	 */
	public void setIndexRate(BigDecimal indexRate) {
		this.indexRate = indexRate;
	}

	/**
	 * @return the spreadRate
	 */
	public BigDecimal getSpreadRate() {
		return spreadRate;
	}

	/**
	 * @param spreadRate the spreadRate to set
	 */
	public void setSpreadRate(BigDecimal spreadRate) {
		this.spreadRate = spreadRate;
	}

	/**
	 * @return the totalRate
	 */
	public BigDecimal getTotalRate() {
		return totalRate;
	}

	/**
	 * @param totalRate the totalRate to set
	 */
	public void setTotalRate(BigDecimal totalRate) {
		this.totalRate = totalRate;
	}

	/**
	 * @return the intAmt
	 */
	public BigDecimal getIntAmt() {
		return intAmt;
	}

	/**
	 * @param intAmt the intAmt to set
	 */
	public void setIntAmt(BigDecimal intAmt) {
		this.intAmt = intAmt;
	}

	/**
	 * @return the intCcy
	 */
	public String getIntCcy() {
		return intCcy;
	}

	/**
	 * @param intCcy the intCcy to set
	 */
	public void setIntCcy(String intCcy) {
		this.intCcy = intCcy;
	}

	/**
	 * @return the accrualIntAmt
	 */
	public BigDecimal getAccrualIntAmt() {
		return accrualIntAmt;
	}

	/**
	 * @param accrualIntAmt the accrualIntAmt to set
	 */
	public void setAccrualIntAmt(BigDecimal accrualIntAmt) {
		this.accrualIntAmt = accrualIntAmt;
	}

	/**
	 * @return the accrualIntCcy
	 */
	public String getAccrualIntCcy() {
		return accrualIntCcy;
	}

	/**
	 * @param accrualIntCcy the accrualIntCcy to set
	 */
	public void setAccrualIntCcy(String accrualIntCcy) {
		this.accrualIntCcy = accrualIntCcy;
	}

	/**
	 * @return the dealno
	 */
	public String getDealno() {
		return dealno;
	}

	/**
	 * @param dealno the dealno to set
	 */
	public void setDealno(String dealno) {
		this.dealno = dealno;
	}

	/**
	 * @return the seq
	 */
	public String getSeq() {
		return seq;
	}

	/**
	 * @param seq the seq to set
	 */
	public void setSeq(String seq) {
		this.seq = seq;
	}

	/**
	 * @return the prodName
	 */
	public String getProdName() {
		return prodName;
	}

	/**
	 * @param prodName the prodName to set
	 */
	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	/**
	 * @return the longShortGb
	 */
	public String getLongShortGb() {
		return longShortGb;
	}

	/**
	 * @param longShortGb the longShortGb to set
	 */
	public void setLongShortGb(String longShortGb) {
		this.longShortGb = longShortGb;
	}

	/**
	 * @return the cancelEnableYn
	 */
	public String getCancelEnableYn() {
		return cancelEnableYn;
	}

	/**
	 * @param cancelEnableYn the cancelEnableYn to set
	 */
	public void setCancelEnableYn(String cancelEnableYn) {
		this.cancelEnableYn = cancelEnableYn;
	}

	/**
	 * @return the collateRalinYn
	 */
	public String getCollateRalinYn() {
		return collateRalinYn;
	}

	/**
	 * @param collateRalinYn the collateRalinYn to set
	 */
	public void setCollateRalinYn(String collateRalinYn) {
		this.collateRalinYn = collateRalinYn;
	}

	/**
	 * @return the intRcvIl
	 */
	public String getIntRcvIl() {
		return intRcvIl;
	}

	/**
	 * @param intRcvIl the intRcvIl to set
	 */
	public void setIntRcvIl(String intRcvIl) {
		this.intRcvIl = intRcvIl;
	}
	
}
