package com.singlee.ifs.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity	
@Table(name = "IFS_LIMIT_TEMPLATE")
public class ifsLimitTemplate implements Serializable{
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT SEQ_IFS_TEMPLATE.NEXTVAL FROM DUAL")
	private String limitId;
	private String limitType;
	private String trad;
	private String prd;
	private String prdCode;
	private	String prdType;
	private	String prdTypeCode;
	private String direction;
	private String term;
	private String dealSource;
	private String cost;
	private String ccy;
	private String ccyCode;
	private String quotaAmount;
	private String availAmount;
	private String ctlType;
	private String operateUser;
	private String itime;
	@Transient
	private String userName;
	
	//美元下线
	private String availAmountU;
	
	//美元上线
	private String quotaAmountU;
	
	//汇率
	private String rate;
	
	// 已经使用的额度，累计;
	private String totalAmount;
	
	//当日限额时间
	private String oprDayTime;
	
	@Transient
	private String maxToday;

	@Transient
	private String maxLast;
	
	@Transient
	private String lastAmount;
	
	// 操作员名字
	private String tradName;
	
	// 成本中心名字
	private String costName;
	
	// 成本中心名字
	private String limitCount;
	
	@Transient
	private String limitCountSum;

	public String getLimitId() {
		return limitId;
	}

	public void setLimitId(String limitId) {
		this.limitId = limitId;
	}

	public String getLimitType() {
		return limitType;
	}

	public void setLimitType(String limitType) {
		this.limitType = limitType;
	}

	public String getTrad() {
		return trad;
	}

	public void setTrad(String trad) {
		this.trad = trad;
	}

	public String getPrd() {
		return prd;
	}

	public void setPrd(String prd) {
		this.prd = prd;
	}

	public String getPrdCode() {
		return prdCode;
	}

	public void setPrdCode(String prdCode) {
		this.prdCode = prdCode;
	}

	public String getPrdType() {
		return prdType;
	}

	public void setPrdType(String prdType) {
		this.prdType = prdType;
	}

	public String getPrdTypeCode() {
		return prdTypeCode;
	}

	public void setPrdTypeCode(String prdTypeCode) {
		this.prdTypeCode = prdTypeCode;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public String getTerm() {
		return term;
	}

	public void setTerm(String term) {
		this.term = term;
	}

	public String getDealSource() {
		return dealSource;
	}

	public void setDealSource(String dealSource) {
		this.dealSource = dealSource;
	}

	public String getCost() {
		return cost;
	}

	public void setCost(String cost) {
		this.cost = cost;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getCcyCode() {
		return ccyCode;
	}

	public void setCcyCode(String ccyCode) {
		this.ccyCode = ccyCode;
	}

	public String getQuotaAmount() {
		return quotaAmount;
	}

	public void setQuotaAmount(String quotaAmount) {
		this.quotaAmount = quotaAmount;
	}

	public String getAvailAmount() {
		return availAmount;
	}

	public void setAvailAmount(String availAmount) {
		this.availAmount = availAmount;
	}

	public String getCtlType() {
		return ctlType;
	}

	public void setCtlType(String ctlType) {
		this.ctlType = ctlType;
	}

	public String getOperateUser() {
		return operateUser;
	}

	public void setOperateUser(String operateUser) {
		this.operateUser = operateUser;
	}

	public String getItime() {
		return itime;
	}

	public void setItime(String itime) {
		this.itime = itime;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getAvailAmountU() {
		return availAmountU;
	}

	public void setAvailAmountU(String availAmountU) {
		this.availAmountU = availAmountU;
	}

	public String getQuotaAmountU() {
		return quotaAmountU;
	}

	public void setQuotaAmountU(String quotaAmountU) {
		this.quotaAmountU = quotaAmountU;
	}

	public String getRate() {
		return rate;
	}

	public void setRate(String rate) {
		this.rate = rate;
	}

	public String getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getOprDayTime() {
		return oprDayTime;
	}

	public void setOprDayTime(String oprDayTime) {
		this.oprDayTime = oprDayTime;
	}

	public String getMaxToday() {
		return maxToday;
	}

	public void setMaxToday(String maxToday) {
		this.maxToday = maxToday;
	}

	public String getMaxLast() {
		return maxLast;
	}

	public void setMaxLast(String maxLast) {
		this.maxLast = maxLast;
	}

	public String getLastAmount() {
		return lastAmount;
	}

	public void setLastAmount(String lastAmount) {
		this.lastAmount = lastAmount;
	}

	public String getTradName() {
		return tradName;
	}

	public void setTradName(String tradName) {
		this.tradName = tradName;
	}

	public String getCostName() {
		return costName;
	}

	public void setCostName(String costName) {
		this.costName = costName;
	}

	public String getLimitCount() {
		return limitCount;
	}

	public void setLimitCount(String limitCount) {
		this.limitCount = limitCount;
	}

	public String getLimitCountSum() {
		return limitCountSum;
	}

	public void setLimitCountSum(String limitCountSum) {
		this.limitCountSum = limitCountSum;
	}
	
	
}