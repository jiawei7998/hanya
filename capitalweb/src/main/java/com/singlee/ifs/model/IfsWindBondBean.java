package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


@Entity
@Table(name = "IFS_WIND_BOND")
public class IfsWindBondBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private String bondShortName;// 债券简称
	@Id
	private String bondCode;// 债券id
	private String bondCodeOld;// 原债券代码
	private String market;// 一二级市场
	private String issueDate;// 发行日期
	private String interestTypeCode;// 付息方式代码
	private String interestType;// 付息方式
	private String basisType;// 计息方式
	private BigDecimal freq;// 付息周期（月）
	private BigDecimal rate;// 票面利率
	private String vdate;// 起息日
	private String mdate;// 到期日
	private BigDecimal term;// 期限
	private String expiryDate;// 行权日
	private BigDecimal spread;// 基本利差
	private BigDecimal issuePrice;// 发行价格（元）
	private String issuerrating;// 主体信用评级
	private String bondLongName;// 债券名称
	private String varietyCode;// 债券品种编号
	private String variety;// 债券品种
	private String circulationCode;// 流通标志代码
	private String circulation;// 流通标志
	private String custodianCode;// 债券托管行代码
	private String custodian;// 债券托管行
	private String basisCode;// 浮动利率基准代码
	private String basis;// 浮动利率基准
	@Id
	private String issuerCode;// 发行人代码
	private String issuer;// 发行人
	private String issuerShort;// 发行人简称
	private String countryCode;// 国家代码
	private String ccy;// 币种

	private String circuplace;// 流通市场
	@Transient
	private String circuplaceCode;// 流通市场
	private String rightType;// 含权方式
	private String industry;// 债券所属行业
	private BigDecimal newQty;// 最新份额
	private String maincorpid;// 组织机构代码
	private String rateType;// 利率类型
	@Transient
	private String zxInst; //评级机构
	@Transient
	private String zxZxLevel;//评级信息
	@Transient
	private String fxZxDate;//评级时间
	@Transient
	private String ztInst; //发行人评级机构
	@Transient
	private String ztZxLevel;//发行人评级信息
	@Transient
	private String fxZtDate;//发行人评级时间
	@Transient
	private String sfcs;//续发次数
	
	private Date spotDate;

	public String getBondShortName() {
		return bondShortName;
	}

	public void setBondShortName(String bondShortName) {
		this.bondShortName = bondShortName;
	}

	public String getBondCode() {
		return bondCode;
	}

	public void setBondCode(String bondCode) {
		this.bondCode = bondCode;
	}

	public String getBondCodeOld() {
		return bondCodeOld;
	}

	public void setBondCodeOld(String bondCodeOld) {
		this.bondCodeOld = bondCodeOld;
	}

	public String getMarket() {
		return market;
	}

	public void setMarket(String market) {
		this.market = market;
	}

	public String getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}

	public String getInterestTypeCode() {
		return interestTypeCode;
	}

	public void setInterestTypeCode(String interestTypeCode) {
		this.interestTypeCode = interestTypeCode;
	}

	public String getInterestType() {
		return interestType;
	}

	public void setInterestType(String interestType) {
		this.interestType = interestType;
	}

	public String getBasisType() {
		return basisType;
	}

	public void setBasisType(String basisType) {
		this.basisType = basisType;
	}

	public BigDecimal getFreq() {
		return freq;
	}

	public void setFreq(BigDecimal freq) {
		this.freq = freq;
	}

	public BigDecimal getRate() {
		return rate;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

	public String getVdate() {
		return vdate;
	}

	public void setVdate(String vdate) {
		this.vdate = vdate;
	}

	public String getMdate() {
		return mdate;
	}

	public void setMdate(String mdate) {
		this.mdate = mdate;
	}

	public BigDecimal getTerm() {
		return term;
	}

	public void setTerm(BigDecimal term) {
		this.term = term;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public BigDecimal getSpread() {
		return spread;
	}

	public void setSpread(BigDecimal spread) {
		this.spread = spread;
	}

	public BigDecimal getIssuePrice() {
		return issuePrice;
	}

	public void setIssuePrice(BigDecimal issuePrice) {
		this.issuePrice = issuePrice;
	}

	public String getIssuerrating() {
		return issuerrating;
	}

	public void setIssuerrating(String issuerrating) {
		this.issuerrating = issuerrating;
	}

	public String getBondLongName() {
		return bondLongName;
	}

	public void setBondLongName(String bondLongName) {
		this.bondLongName = bondLongName;
	}

	public String getVarietyCode() {
		return varietyCode;
	}

	public void setVarietyCode(String varietyCode) {
		this.varietyCode = varietyCode;
	}

	public String getVariety() {
		return variety;
	}

	public void setVariety(String variety) {
		this.variety = variety;
	}

	public String getCirculationCode() {
		return circulationCode;
	}

	public void setCirculationCode(String circulationCode) {
		this.circulationCode = circulationCode;
	}

	public String getCirculation() {
		return circulation;
	}

	public void setCirculation(String circulation) {
		this.circulation = circulation;
	}

	public String getCustodianCode() {
		return custodianCode;
	}

	public void setCustodianCode(String custodianCode) {
		this.custodianCode = custodianCode;
	}

	public String getCustodian() {
		return custodian;
	}

	public void setCustodian(String custodian) {
		this.custodian = custodian;
	}

	public String getBasisCode() {
		return basisCode;
	}

	public void setBasisCode(String basisCode) {
		this.basisCode = basisCode;
	}

	public String getBasis() {
		return basis;
	}

	public void setBasis(String basis) {
		this.basis = basis;
	}

	public String getIssuerCode() {
		return issuerCode;
	}

	public void setIssuerCode(String issuerCode) {
		this.issuerCode = issuerCode;
	}

	public String getIssuer() {
		return issuer;
	}

	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}

	public String getIssuerShort() {
		return issuerShort;
	}

	public void setIssuerShort(String issuerShort) {
		this.issuerShort = issuerShort;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getCircuplace() {
		return circuplace;
	}

	public void setCircuplace(String circuplace) {
		this.circuplace = circuplace;
	}

	public String getRightType() {
		return rightType;
	}

	public void setRightType(String rightType) {
		this.rightType = rightType;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public BigDecimal getNewQty() {
		return newQty;
	}

	public void setNewQty(BigDecimal newQty) {
		this.newQty = newQty;
	}

	public String getMaincorpid() {
		return maincorpid;
	}

	public void setMaincorpid(String maincorpid) {
		this.maincorpid = maincorpid;
	}

	public String getRateType() {
		return rateType;
	}

	public void setRateType(String rateType) {
		this.rateType = rateType;
	}

	public Date getSpotDate() {
		return spotDate;
	}

	public void setSpotDate(Date spotDate) {
		this.spotDate = spotDate;
	}

	public String getCircuplaceCode() {
		return circuplaceCode;
	}

	public void setCircuplaceCode(String circuplaceCode) {
		this.circuplaceCode = circuplaceCode;
	}

	public String getZxInst() {
		return zxInst;
	}

	public void setZxInst(String zxInst) {
		this.zxInst = zxInst;
	}

	public String getZxZxLevel() {
		return zxZxLevel;
	}

	public void setZxZxLevel(String zxZxLevel) {
		this.zxZxLevel = zxZxLevel;
	}

	public String getFxZxDate() {
		return fxZxDate;
	}

	public void setFxZxDate(String fxZxDate) {
		this.fxZxDate = fxZxDate;
	}

	public String getZtInst() {
		return ztInst;
	}

	public void setZtInst(String ztInst) {
		this.ztInst = ztInst;
	}

	public String getZtZxLevel() {
		return ztZxLevel;
	}

	public void setZtZxLevel(String ztZxLevel) {
		this.ztZxLevel = ztZxLevel;
	}

	public String getFxZtDate() {
		return fxZtDate;
	}

	public void setFxZtDate(String fxZtDate) {
		this.fxZtDate = fxZtDate;
	}

	public String getSfcs() {
		return sfcs;
	}

	public void setSfcs(String sfcs) {
		this.sfcs = sfcs;
	}
}
