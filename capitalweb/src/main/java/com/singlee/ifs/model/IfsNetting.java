package com.singlee.ifs.model;

import javax.persistence.Transient;
import java.io.Serializable;

/**
 * 	净额清算
 * @author tlcb
 */
public class IfsNetting implements Serializable{
	//成交单号
	private String contractId;
	//交易对手
	private String counterpartyDealer;
	//收款账号
	private String dueBankAccount1;
	//收款币种
	private String buyCurreny;
	//收款金额
	private String buyAmt;
	//付款账号
	private String dueBankAccount2;
	//付款币种
	private String sellCurreny;
	//付款金额
	private String sellAmt;
	//清算方式（净额或全额）
	private String nettingStatus;
	//业务类型
	private String businessType;
	//交割日期
	@Transient
	private String payDate;
	@Transient
	private String sumAmt;
	
	private String dealno;
	
	//付款金额
	@Transient
	private String sumAmtP;
	
	private static final long serialVersionUID = -1714511788400565703L;
	
	public String getContractId() {
		return contractId;
	}
	public void setContractId(String contractId) {
		this.contractId = contractId;
	}
	public String getCounterpartyDealer() {
		return counterpartyDealer;
	}
	public void setCounterpartyDealer(String counterpartyDealer) {
		this.counterpartyDealer = counterpartyDealer;
	}
	public String getDueBankAccount1() {
		return dueBankAccount1;
	}
	public void setDueBankAccount1(String dueBankAccount1) {
		this.dueBankAccount1 = dueBankAccount1;
	}
	public String getBuyCurreny() {
		return buyCurreny;
	}
	public void setBuyCurreny(String buyCurreny) {
		this.buyCurreny = buyCurreny;
	}
	public String getBuyAmt() {
		return buyAmt;
	}
	public void setBuyAmt(String buyAmt) {
		this.buyAmt = buyAmt;
	}
	public String getDueBankAccount2() {
		return dueBankAccount2;
	}
	public void setDueBankAccount2(String dueBankAccount2) {
		this.dueBankAccount2 = dueBankAccount2;
	}
	public String getSellCurreny() {
		return sellCurreny;
	}
	public void setSellCurreny(String sellCurreny) {
		this.sellCurreny = sellCurreny;
	}
	public String getSellAmt() {
		return sellAmt;
	}
	public void setSellAmt(String sellAmt) {
		this.sellAmt = sellAmt;
	}
	public String getNettingStatus() {
		return nettingStatus;
	}
	public void setNettingStatus(String nettingStatus) {
		this.nettingStatus = nettingStatus;
	}
	public String getBusinessType() {
		return businessType;
	}
	public void setBusinessType(String businessType) {
		this.businessType = businessType;
	}
	public String getPayDate() {
		return payDate;
	}
	public void setPayDate(String payDate) {
		this.payDate = payDate;
	}
	public String getSumAmt() {
		return sumAmt;
	}
	public void setSumAmt(String sumAmt) {
		this.sumAmt = sumAmt;
	}
	public void setDealno(String dealno) {
		this.dealno = dealno;
	}
	public String getDealno() {
		return dealno;
	}
	public String getSumAmtP() {
		return sumAmtP;
	}
	public void setSumAmtP(String sumAmtP) {
		this.sumAmtP = sumAmtP;
	}
	
}
