package com.singlee.ifs.model;

import javax.persistence.Table;

/**
 * @author ：chenguo
 * @date ：Created in 2021/9/17 10:40
 * @description：外汇远掉点
 * @modified By：
 * @version:
 */
@Table(name="IFS_FORWARD_EXCHANGE_RATE")
public class IfsForwardExchangeRateBean {

    // 货币对
    private String ccy;
    // 汇率日期
    private String postdate;
    // 即期汇率
    private String spotRate;
    // 交易日汇率
    private String period1Rate;
    // 下一交易日汇率
    private String period2Rate;
    // 三日后汇率
    private String period3Rate;
    // 一周后汇率
    private String period4Rate;
    // 两周后汇率
    private String period5Rate;
    // 一个月后汇率
    private String period6Rate;
    // 两个月后汇率
    private String period7Rate;
    // 三个月后汇率
    private String period8Rate;
    // 六个月后汇率
    private String period9Rate;
    // 九个月后汇率
    private String period10Rate;
    // 一年后汇率
    private String period11Rate;
    // 两年会汇率
    private String period12Rate;
    // 三年后汇率
    private String period13Rate;
    // 四年后汇率
    private String period14Rate;
    // 五年后汇率
    private String period15Rate;
    //汇率类型(BID：买入价；ASK、卖出价）
    private String isBuy;

    public String getCcy() {
        return ccy;
    }

    public void setCcy(String ccy) {
        this.ccy = ccy;
    }

    public String getPostdate() {
        return postdate;
    }

    public void setPostdate(String postdate) {
        this.postdate = postdate;
    }

    public String getSpotRate() {
        return spotRate;
    }

    public void setSpotRate(String spotRate) {
        this.spotRate = spotRate;
    }

    public String getPeriod1Rate() {
        return period1Rate;
    }

    public void setPeriod1Rate(String period1Rate) {
        this.period1Rate = period1Rate;
    }

    public String getPeriod2Rate() {
        return period2Rate;
    }

    public void setPeriod2Rate(String period2Rate) {
        this.period2Rate = period2Rate;
    }

    public String getPeriod3Rate() {
        return period3Rate;
    }

    public void setPeriod3Rate(String period3Rate) {
        this.period3Rate = period3Rate;
    }

    public String getPeriod4Rate() {
        return period4Rate;
    }

    public void setPeriod4Rate(String period4Rate) {
        this.period4Rate = period4Rate;
    }

    public String getPeriod5Rate() {
        return period5Rate;
    }

    public void setPeriod5Rate(String period5Rate) {
        this.period5Rate = period5Rate;
    }

    public String getPeriod6Rate() {
        return period6Rate;
    }

    public void setPeriod6Rate(String period6Rate) {
        this.period6Rate = period6Rate;
    }

    public String getPeriod7Rate() {
        return period7Rate;
    }

    public void setPeriod7Rate(String period7Rate) {
        this.period7Rate = period7Rate;
    }

    public String getPeriod8Rate() {
        return period8Rate;
    }

    public void setPeriod8Rate(String period8Rate) {
        this.period8Rate = period8Rate;
    }

    public String getPeriod9Rate() {
        return period9Rate;
    }

    public void setPeriod9Rate(String period9Rate) {
        this.period9Rate = period9Rate;
    }

    public String getPeriod10Rate() {
        return period10Rate;
    }

    public void setPeriod10Rate(String period10Rate) {
        this.period10Rate = period10Rate;
    }

    public String getPeriod11Rate() {
        return period11Rate;
    }

    public void setPeriod11Rate(String period11Rate) {
        this.period11Rate = period11Rate;
    }

    public String getPeriod12Rate() {
        return period12Rate;
    }

    public void setPeriod12Rate(String period12Rate) {
        this.period12Rate = period12Rate;
    }

    public String getPeriod13Rate() {
        return period13Rate;
    }

    public void setPeriod13Rate(String period13Rate) {
        this.period13Rate = period13Rate;
    }

    public String getPeriod14Rate() {
        return period14Rate;
    }

    public void setPeriod14Rate(String period14Rate) {
        this.period14Rate = period14Rate;
    }

    public String getPeriod15Rate() {
        return period15Rate;
    }

    public void setPeriod15Rate(String period15Rate) {
        this.period15Rate = period15Rate;
    }

    public String getIsBuy() {
        return isBuy;
    }

    public void setIsBuy(String isBuy) {
        this.isBuy = isBuy;
    }

    @Override
    public String toString() {
        return "IfsForwardExchangeRateBean{" +
                "ccy='" + ccy + '\'' +
                ", postdate='" + postdate + '\'' +
                ", spotRate='" + spotRate + '\'' +
                ", period1Rate='" + period1Rate + '\'' +
                ", period2Rate='" + period2Rate + '\'' +
                ", period3Rate='" + period3Rate + '\'' +
                ", period4Rate='" + period4Rate + '\'' +
                ", period5Rate='" + period5Rate + '\'' +
                ", period6Rate='" + period6Rate + '\'' +
                ", period7Rate='" + period7Rate + '\'' +
                ", period8Rate='" + period8Rate + '\'' +
                ", period9Rate='" + period9Rate + '\'' +
                ", period10Rate='" + period10Rate + '\'' +
                ", period11Rate='" + period11Rate + '\'' +
                ", period12Rate='" + period12Rate + '\'' +
                ", period13Rate='" + period13Rate + '\'' +
                ", period14Rate='" + period14Rate + '\'' +
                ", period15Rate='" + period15Rate + '\'' +
                ", isBuy='" + isBuy + '\'' +
                '}';
    }
}
