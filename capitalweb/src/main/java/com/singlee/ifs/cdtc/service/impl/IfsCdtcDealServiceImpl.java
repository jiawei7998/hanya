package com.singlee.ifs.cdtc.service.impl;

import com.github.pagehelper.Page;
import com.singlee.financial.common.util.ParameterUtil;
import com.singlee.financial.page.PageInfo;
import com.singlee.ifs.cdtc.mapper.IfsCdtcDealMapper;
import com.singlee.ifs.cdtc.model.IfsCdtcDeal;
import com.singlee.ifs.cdtc.service.IfsCdtcDealService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/11/29 15:21
 * @description：${description}
 * @modified By：
 * @version:   
 */
@Service
public class IfsCdtcDealServiceImpl implements IfsCdtcDealService{

    @Resource
    private IfsCdtcDealMapper ifsCdtcDealMapper;

    @Override
    public int deleteByPrimaryKey(String txid) {
        return ifsCdtcDealMapper.deleteByPrimaryKey(txid);
    }

    @Override
    public int insert(IfsCdtcDeal record) {
        return ifsCdtcDealMapper.insert(record);
    }

    @Override
    public int insertSelective(IfsCdtcDeal record) {
        return ifsCdtcDealMapper.insertSelective(record);
    }

    @Override
    public IfsCdtcDeal selectByPrimaryKey(String txid) {
        return ifsCdtcDealMapper.selectByPrimaryKey(txid);
    }

    @Override
    public int updateByPrimaryKeySelective(IfsCdtcDeal record) {
        return ifsCdtcDealMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(IfsCdtcDeal record) {
        return ifsCdtcDealMapper.updateByPrimaryKey(record);
    }

    @Override
    public PageInfo<IfsCdtcDeal> searchBondIfsCdtcDeal(Map<String, Object> map) {
        String type = (String) map.get("type");
        RowBounds rb = ParameterUtil.getRowBounds(map);
        Page<IfsCdtcDeal> page = ifsCdtcDealMapper.searchIfsCdtcDeal(map, rb);
        Page<IfsCdtcDeal> page2=null;
        for (IfsCdtcDeal ifsCdtcDeal :page){
            if (type.equals(ifsCdtcDeal.getBiztp())){
                page2.add(ifsCdtcDeal);
            }
        }
        return new PageInfo<>(page2);

    }

}
