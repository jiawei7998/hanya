package com.singlee.ifs.cdtc.service;

import com.singlee.ifs.cdtc.model.IfsCdtcSecPos;import java.util.Date;

/**
 * @author ：chenguo
 * @date ：Created in 2021/11/29 14:03
 * @description：${description}
 * @modified By：
 * @version:
 */
public interface IfsCdtcSecPosService {


    int insert(IfsCdtcSecPos record);

    int insertSelective(IfsCdtcSecPos record);

    int deleteByPrimaryKey(String secid, Date postdate);

    IfsCdtcSecPos selectByPrimaryKey(String secid, Date postdate);

    int updateByPrimaryKeySelective(IfsCdtcSecPos record);

    int updateByPrimaryKey(IfsCdtcSecPos record);
}

