package com.singlee.ifs.cdtc.model;

import java.io.Serializable;

/**
 * 债券3组件
 */
public class Bond3 implements Serializable {
    /**
     * 债券代码
     */
    private String BdId;
    /**
     * ISIN编码
     */
    private String ISIN;
    /**
     * 债券简称
     */
    private String BdShrtNm;
    /**
     * 券面总额
     */
    private String BdAmt;
    /**
     * 债券质押率
     */
    private String BdCollRate;
    /**
     * 应计利息信息
     */
    private AccruedInterestInformation AcrdIntrstInf;
    /**
     * 债券价格1
     */
    private String Pric1;
    /**
     * 债券价格2
     */
    private String Pric2;

    public String getBdId() {
        return BdId;
    }

    public void setBdId(String bdId) {
        BdId = bdId;
    }

    public String getISIN() {
        return ISIN;
    }

    public void setISIN(String ISIN) {
        this.ISIN = ISIN;
    }

    public String getBdShrtNm() {
        return BdShrtNm;
    }

    public void setBdShrtNm(String bdShrtNm) {
        BdShrtNm = bdShrtNm;
    }

    public String getBdAmt() {
        return BdAmt;
    }

    public void setBdAmt(String bdAmt) {
        BdAmt = bdAmt;
    }

    public String getBdCollRate() {
        return BdCollRate;
    }

    public void setBdCollRate(String bdCollRate) {
        BdCollRate = bdCollRate;
    }

    public AccruedInterestInformation getAcrdIntrstInf() {
        return AcrdIntrstInf;
    }

    public void setAcrdIntrstInf(AccruedInterestInformation acrdIntrstInf) {
        AcrdIntrstInf = acrdIntrstInf;
    }

    public String getPric1() {
        return Pric1;
    }

    public void setPric1(String pric1) {
        Pric1 = pric1;
    }

    public String getPric2() {
        return Pric2;
    }

    public void setPric2(String pric2) {
        Pric2 = pric2;
    }
}
