package com.singlee.ifs.cdtc.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/11/29 14:05
 * @description：${description}
 * @modified By：
 * @version:   
 */

/**
 * 指令查询
 */
public class IfsCdtcInstruct implements Serializable {
    /**
     * 结算指令标识
     */
        private String instrid;

    /**
     * 结算合同标识
     */
    private String ctrctid;

    /**
     * 付券方账户名称
     */
    private String givacctnm;

    /**
     * 付券方账户id
     */
    private String givacctid;

    /**
     * 收券方账户名称
     */
    private String takacctnm;

    /**
     * 收券方账户id
     */
    private String takacctid;

    /**
     * 业务标识号
     */
    private String txid;

    /**
     * 业务类型
     */
    private String biztp;

    /**
     * 结算方式1
     */
    private String sttlmtp1;

    /**
     * 结算方式2
     */
    private String sttlmtp2;

    /**
     * 债券数目
     */
    private BigDecimal bdcnt;

    /**
     * 债券总额
     */
    private BigDecimal aggtfaceamt;

    /**
     * 结算金额1
     */
    private BigDecimal val1;

    /**
     * 结算金额2
     */
    private BigDecimal val2;

    /**
     * 交割日1
     */
    private String dt1;

    /**
     * 交割日2
     */
    private String dt2;

    /**
     * 指令处理状态
     */
    private String instrsts;

    /**
     * 合同处理状态
     */
    private String ctrctsts;

    /**
     * 合同冻结状态
     */
    private String ctrctblcksts;

    /**
     * 最新更新时间
     */
    private String lastupdtm;

    /**
     * 发令方确认标识
     */
    private String orgtrcnfrmind;

    /**
     * 对手方确认标识
     */
    private String ctrcnfrmind;

    /**
     * 指令来源
     */
    private String instrorgn;

    /**
     * 清算时间
     */
    private String stdt;

    /**
     * 经办人员
     */
    private String ioper;

    /**
     * 经办时间
     */
    private String itime;

    /**
     * 复核人员
     */
    private String voper;

    /**
     * 复核时间
     */
    private Date vtime;

    /**
     * 报文发送处理标识
     */
    private String adealwith;

    /**
     * 处理时间
     */
    private String adtime;

    /**
     * 撤销经办人
     */
    private String rioper;

    /**
     * 撤销时间
     */
    private String ritime;

    /**
     * 撤销复核人员
     */
    private String rvoper;

    /**
     * 撤销复核时间
     */
    private String rvtime;

    /**
     * 撤销报文处理标识
     */
    private String rdealwith;

    /**
     * 撤销报文处理时间
     */
    private String rdtime;

    private String csbs001;

    private String csbs003;

    //债券1
    private List<Bond3>   Bd1;

    //债券2
    private List<Bond3>   Bd2;

    private static final long serialVersionUID = 1L;

    public String getInstrid() {
        return instrid;
    }

    public void setInstrid(String instrid) {
        this.instrid = instrid;
    }

    public String getCtrctid() {
        return ctrctid;
    }

    public void setCtrctid(String ctrctid) {
        this.ctrctid = ctrctid;
    }

    public String getGivacctnm() {
        return givacctnm;
    }

    public void setGivacctnm(String givacctnm) {
        this.givacctnm = givacctnm;
    }

    public String getGivacctid() {
        return givacctid;
    }

    public void setGivacctid(String givacctid) {
        this.givacctid = givacctid;
    }

    public String getTakacctnm() {
        return takacctnm;
    }

    public void setTakacctnm(String takacctnm) {
        this.takacctnm = takacctnm;
    }

    public String getTakacctid() {
        return takacctid;
    }

    public void setTakacctid(String takacctid) {
        this.takacctid = takacctid;
    }

    public String getTxid() {
        return txid;
    }

    public void setTxid(String txid) {
        this.txid = txid;
    }

    public String getBiztp() {
        return biztp;
    }

    public void setBiztp(String biztp) {
        this.biztp = biztp;
    }

    public String getSttlmtp1() {
        return sttlmtp1;
    }

    public void setSttlmtp1(String sttlmtp1) {
        this.sttlmtp1 = sttlmtp1;
    }

    public String getSttlmtp2() {
        return sttlmtp2;
    }

    public void setSttlmtp2(String sttlmtp2) {
        this.sttlmtp2 = sttlmtp2;
    }

    public BigDecimal getBdcnt() {
        return bdcnt;
    }

    public void setBdcnt(BigDecimal bdcnt) {
        this.bdcnt = bdcnt;
    }

    public BigDecimal getAggtfaceamt() {
        return aggtfaceamt;
    }

    public void setAggtfaceamt(BigDecimal aggtfaceamt) {
        this.aggtfaceamt = aggtfaceamt;
    }

    public BigDecimal getVal1() {
        return val1;
    }

    public void setVal1(BigDecimal val1) {
        this.val1 = val1;
    }

    public BigDecimal getVal2() {
        return val2;
    }

    public void setVal2(BigDecimal val2) {
        this.val2 = val2;
    }

    public String getDt1() {
        return dt1;
    }

    public void setDt1(String dt1) {
        this.dt1 = dt1;
    }

    public String getDt2() {
        return dt2;
    }

    public void setDt2(String dt2) {
        this.dt2 = dt2;
    }

    public String getInstrsts() {
        return instrsts;
    }

    public void setInstrsts(String instrsts) {
        this.instrsts = instrsts;
    }

    public String getCtrctsts() {
        return ctrctsts;
    }

    public void setCtrctsts(String ctrctsts) {
        this.ctrctsts = ctrctsts;
    }

    public String getCtrctblcksts() {
        return ctrctblcksts;
    }

    public void setCtrctblcksts(String ctrctblcksts) {
        this.ctrctblcksts = ctrctblcksts;
    }

    public String getLastupdtm() {
        return lastupdtm;
    }

    public void setLastupdtm(String lastupdtm) {
        this.lastupdtm = lastupdtm;
    }

    public String getOrgtrcnfrmind() {
        return orgtrcnfrmind;
    }

    public void setOrgtrcnfrmind(String orgtrcnfrmind) {
        this.orgtrcnfrmind = orgtrcnfrmind;
    }

    public String getCtrcnfrmind() {
        return ctrcnfrmind;
    }

    public void setCtrcnfrmind(String ctrcnfrmind) {
        this.ctrcnfrmind = ctrcnfrmind;
    }

    public String getInstrorgn() {
        return instrorgn;
    }

    public void setInstrorgn(String instrorgn) {
        this.instrorgn = instrorgn;
    }

    public String getStdt() {
        return stdt;
    }

    public void setStdt(String stdt) {
        this.stdt = stdt;
    }

    public String getIoper() {
        return ioper;
    }

    public void setIoper(String ioper) {
        this.ioper = ioper;
    }

    public String getItime() {
        return itime;
    }

    public void setItime(String itime) {
        this.itime = itime;
    }

    public String getVoper() {
        return voper;
    }

    public void setVoper(String voper) {
        this.voper = voper;
    }

    public Date getVtime() {
        return vtime;
    }

    public void setVtime(Date vtime) {
        this.vtime = vtime;
    }

    public String getAdealwith() {
        return adealwith;
    }

    public void setAdealwith(String adealwith) {
        this.adealwith = adealwith;
    }

    public String getAdtime() {
        return adtime;
    }

    public void setAdtime(String adtime) {
        this.adtime = adtime;
    }

    public String getRioper() {
        return rioper;
    }

    public void setRioper(String rioper) {
        this.rioper = rioper;
    }

    public String getRitime() {
        return ritime;
    }

    public void setRitime(String ritime) {
        this.ritime = ritime;
    }

    public String getRvoper() {
        return rvoper;
    }

    public void setRvoper(String rvoper) {
        this.rvoper = rvoper;
    }

    public String getRvtime() {
        return rvtime;
    }

    public void setRvtime(String rvtime) {
        this.rvtime = rvtime;
    }

    public String getRdealwith() {
        return rdealwith;
    }

    public void setRdealwith(String rdealwith) {
        this.rdealwith = rdealwith;
    }

    public String getRdtime() {
        return rdtime;
    }

    public void setRdtime(String rdtime) {
        this.rdtime = rdtime;
    }

    public String getCsbs001() {
        return csbs001;
    }

    public void setCsbs001(String csbs001) {
        this.csbs001 = csbs001;
    }

    public String getCsbs003() {
        return csbs003;
    }

    public void setCsbs003(String csbs003) {
        this.csbs003 = csbs003;
    }

    public List<Bond3> getBd1() {
        return Bd1;
    }

    public void setBd1(List<Bond3> bd1) {
        Bd1 = bd1;
    }

    public List<Bond3> getBd2() {
        return Bd2;
    }

    public void setBd2(List<Bond3> bd2) {
        Bd2 = bd2;
    }

    @Override
    public String toString() {
        return "IfsCdtcInstruct{" +
                "instrid='" + instrid + '\'' +
                ", ctrctid='" + ctrctid + '\'' +
                ", givacctnm='" + givacctnm + '\'' +
                ", givacctid='" + givacctid + '\'' +
                ", takacctnm='" + takacctnm + '\'' +
                ", takacctid='" + takacctid + '\'' +
                ", txid='" + txid + '\'' +
                ", biztp='" + biztp + '\'' +
                ", sttlmtp1='" + sttlmtp1 + '\'' +
                ", sttlmtp2='" + sttlmtp2 + '\'' +
                ", bdcnt=" + bdcnt +
                ", aggtfaceamt=" + aggtfaceamt +
                ", val1=" + val1 +
                ", val2=" + val2 +
                ", dt1='" + dt1 + '\'' +
                ", dt2='" + dt2 + '\'' +
                ", instrsts='" + instrsts + '\'' +
                ", ctrctsts='" + ctrctsts + '\'' +
                ", ctrctblcksts='" + ctrctblcksts + '\'' +
                ", lastupdtm='" + lastupdtm + '\'' +
                ", orgtrcnfrmind='" + orgtrcnfrmind + '\'' +
                ", ctrcnfrmind='" + ctrcnfrmind + '\'' +
                ", instrorgn='" + instrorgn + '\'' +
                ", stdt='" + stdt + '\'' +
                ", ioper='" + ioper + '\'' +
                ", itime='" + itime + '\'' +
                ", voper='" + voper + '\'' +
                ", vtime=" + vtime +
                ", adealwith='" + adealwith + '\'' +
                ", adtime='" + adtime + '\'' +
                ", rioper='" + rioper + '\'' +
                ", ritime='" + ritime + '\'' +
                ", rvoper='" + rvoper + '\'' +
                ", rvtime='" + rvtime + '\'' +
                ", rdealwith='" + rdealwith + '\'' +
                ", rdtime='" + rdtime + '\'' +
                ", csbs001='" + csbs001 + '\'' +
                ", csbs003='" + csbs003 + '\'' +
                ", Bd1=" + Bd1 +
                ", Bd2=" + Bd2 +
                '}';
    }
}