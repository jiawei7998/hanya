package com.singlee.ifs.cdtc.service;

import com.singlee.ifs.cdtc.model.IfsCdtcBond3;

/**
 * @author ：chenguo
 * @date ：Created in 2021/11/29 20:15
 * @description：${description}
 * @modified By：
 * @version:
 */
public interface IfsCdtcBond3Service {


    int insert(IfsCdtcBond3 record);

    int insertSelective(IfsCdtcBond3 record);

    int deleteByPrimaryKey(String bdid, String acrdintrstdt);

    IfsCdtcBond3 selectByPrimaryKey(String bdid, String acrdintrstdt);

    int updateByPrimaryKeySelective(IfsCdtcBond3 record);

    int updateByPrimaryKey(IfsCdtcBond3 record);
}


