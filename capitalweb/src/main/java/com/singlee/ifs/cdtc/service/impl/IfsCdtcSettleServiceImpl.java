package com.singlee.ifs.cdtc.service.impl;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.singlee.ifs.cdtc.mapper.IfsCdtcSettleMapper;
import com.singlee.ifs.cdtc.model.IfsCdtcSettle;
import com.singlee.ifs.cdtc.service.IfsCdtcSettleService;

/**
 * @author ：chenguo
 * @date ：Created in 2021/11/29 14:03
 * @description：${description}
 * @modified By：
 * @version:
 */
@Service
public class IfsCdtcSettleServiceImpl implements IfsCdtcSettleService {

    @Resource
    private IfsCdtcSettleMapper ifsCdtcSettleMapper;

    @Override
    public int insert(IfsCdtcSettle record) {
        return ifsCdtcSettleMapper.insert(record);
    }

    @Override
    public int insertSelective(IfsCdtcSettle record) {
        return ifsCdtcSettleMapper.insertSelective(record);
    }

    @Override
    public int deleteByPrimaryKey(String instrid) {
        return ifsCdtcSettleMapper.deleteByPrimaryKey(instrid);
    }

    @Override
    public IfsCdtcSettle selectByPrimaryKey(String instrid) {
        return ifsCdtcSettleMapper.selectByPrimaryKey(instrid);
    }

    @Override
    public int updateByPrimaryKeySelective(IfsCdtcSettle record) {
        return ifsCdtcSettleMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(IfsCdtcSettle record) {
        return ifsCdtcSettleMapper.updateByPrimaryKey(record);
    }
}

