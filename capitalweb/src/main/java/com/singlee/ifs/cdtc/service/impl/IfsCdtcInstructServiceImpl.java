package com.singlee.ifs.cdtc.service.impl;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.singlee.ifs.cdtc.model.IfsCdtcInstruct;
import com.singlee.ifs.cdtc.mapper.IfsCdtcInstructMapper;
import com.singlee.ifs.cdtc.service.IfsCdtcInstructService;

/**
 * @author ：chenguo
 * @date ：Created in 2021/11/29 14:03
 * @description：${description}
 * @modified By：
 * @version:
 */
@Service
public class IfsCdtcInstructServiceImpl implements IfsCdtcInstructService {

    @Resource
    private IfsCdtcInstructMapper ifsCdtcInstructMapper;

    @Override
    public int insert(IfsCdtcInstruct record) {
        return ifsCdtcInstructMapper.insert(record);
    }

    @Override
    public int insertSelective(IfsCdtcInstruct record) {
        return ifsCdtcInstructMapper.insertSelective(record);
    }

    @Override
    public int deleteByPrimaryKey(String instrid, String ctrctid) {
        return ifsCdtcInstructMapper.deleteByPrimaryKey(instrid, ctrctid);
    }

    @Override
    public IfsCdtcInstruct selectByPrimaryKey(String instrid, String ctrctid) {
        return ifsCdtcInstructMapper.selectByPrimaryKey(instrid, ctrctid);
    }

    @Override
    public int updateByPrimaryKeySelective(IfsCdtcInstruct record) {
        return ifsCdtcInstructMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(IfsCdtcInstruct record) {
        return ifsCdtcInstructMapper.updateByPrimaryKey(record);
    }
}

