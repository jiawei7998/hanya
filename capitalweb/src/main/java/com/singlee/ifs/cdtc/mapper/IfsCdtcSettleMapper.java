package com.singlee.ifs.cdtc.mapper;

import com.singlee.ifs.cdtc.model.IfsCdtcSettle;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author ：chenguo
 * @date ：Created in 2021/11/29 14:05
 * @description：${description}
 * @modified By：
 * @version:
 */
@Mapper
public interface IfsCdtcSettleMapper {
    /**
     * delete by primary key
     *
     * @param instrid primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(String instrid);

    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(IfsCdtcSettle record);

    /**
     * insert record to table selective
     *
     * @param record the record
     * @return insert count
     */
    int insertSelective(IfsCdtcSettle record);

    /**
     * select by primary key
     *
     * @param instrid primary key
     * @return object by primary key
     */
    IfsCdtcSettle selectByPrimaryKey(String instrid);

    /**
     * update record selective
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(IfsCdtcSettle record);

    /**
     * update record
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(IfsCdtcSettle record);
}