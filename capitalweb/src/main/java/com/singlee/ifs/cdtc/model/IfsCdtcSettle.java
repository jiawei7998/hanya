package com.singlee.ifs.cdtc.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author ：chenguo
 * @date ：Created in 2021/11/29 14:05
 * @description：${description}
 * @modified By：
 * @version:
 */
public class IfsCdtcSettle implements Serializable {
    /**
     * 结算指令标识
     */
    private String instrid;

    /**
     * 机构
     */
    private String br;

    /**
     * 交易号
     */
    private String dealno;

    /**
     * 买卖方向
     */
    private String ps;

    /**
     * 产品代码
     */
    private String prodcode;

    /**
     * 产品类型
     */
    private String prodtype;

    /**
     * 债券号
     */
    private String secid;

    /**
     * 债券名称
     */
    private String secidnm;

    /**
     * cif
     */
    private String cif;

    /**
     * 客户名称
     */
    private String custnm;

    /**
     * 交易中心编号
     */
    private String cfetssn;

    /**
     * 交易日期
     */
    private String dealdate;

    /**
     * 起息日
     */
    private String vdate;

    /**
     * 到期日
     */
    private String matdate;

    /**
     * 票面金额
     */
    private BigDecimal faceamt;

    /**
     * 首期金额/净价金额
     */
    private BigDecimal comprocdamt;

    /**
     * 到期金额/全价金额
     */
    private BigDecimal matprocdamt;

    /**
     * 中债首期交割金额/债券交割金额
     */
    private BigDecimal comprocdamtzz;

    /**
     * 中债到期交割金额
     */
    private BigDecimal matprocdamtzz;

    /**
     * 首期交割金额/债券交割金额差额
     */
    private BigDecimal comprocdamtsub;

    /**
     * 到期交割金额差额
     */
    private BigDecimal matprocdamtsub;

    /**
     * 清算状态(交易状态)
     */
    private String dealstatus;

    /**
     * 交易来源
     */
    private String dealtext;

    /**
     * 首期CCY 结算方式
     */
    private String comccysmeans;

    /**
     * 到期CCY 结算方式
     */
    private String matccysmeans;

    /**
     * 原始交易单结算方式 首期
     */
    private String setttypeB;

    /**
     * 原始交易单结算方式描述 首期
     */
    private String settdescrB;

    /**
     * 原始交易单结算方式 到期
     */
    private String setttypeE;

    /**
     * 原始交易单结算方式描述 到期
     */
    private String settdescrE;

    private String xmlmatchid;

    /**
     * CHECK状态
     */
    private String checksts;

    /**
     * CHECK时间
     */
    private Date checktime;

    /**
     * 请求编号
     */
    private String requestsn;

    /**
     * 发送编号
     */
    private String sendsts;

    /**
     * 发送时间
     */
    private String sendtime;

    /**
     * 发送返回的状态
     */
    private String cnbondretsts;

    /**
     * 返回时间
     */
    private String cnbondrettime;

    /**
     * 债券所属账户ID
     */
    private String acctqryacctid;

    /**
     * 交易流水标识
     */
    private String txflowid;

    /**
     * 指令处理状态
     */
    private String instrsts;

    /**
     * 指令处理结果返回码
     */
    private String txrsltcd;

    /**
     * 指令返回信息描述
     */
    private String inserrinf;

    /**
     * 发令方确认标识
     */
    private String orgtrcnfrmind;

    /**
     * 对手方确认标识
     */
    private String ctrcnfrmind;

    /**
     * 结算合同标识
     */
    private String ctrctid;

    /**
     * 合同处理状态
     */
    private String ctrctsts;

    /**
     * 合同冻结状态
     */
    private String ctrctblcksts;

    /**
     * 合同失败原因
     */
    private String ctrctfaildrsn;

    /**
     * 查询返回时间
     */
    private String queryrettime;

    private static final long serialVersionUID = 1L;

    public String getInstrid() {
        return instrid;
    }

    public void setInstrid(String instrid) {
        this.instrid = instrid;
    }

    public String getBr() {
        return br;
    }

    public void setBr(String br) {
        this.br = br;
    }

    public String getDealno() {
        return dealno;
    }

    public void setDealno(String dealno) {
        this.dealno = dealno;
    }

    public String getPs() {
        return ps;
    }

    public void setPs(String ps) {
        this.ps = ps;
    }

    public String getProdcode() {
        return prodcode;
    }

    public void setProdcode(String prodcode) {
        this.prodcode = prodcode;
    }

    public String getProdtype() {
        return prodtype;
    }

    public void setProdtype(String prodtype) {
        this.prodtype = prodtype;
    }

    public String getSecid() {
        return secid;
    }

    public void setSecid(String secid) {
        this.secid = secid;
    }

    public String getSecidnm() {
        return secidnm;
    }

    public void setSecidnm(String secidnm) {
        this.secidnm = secidnm;
    }

    public String getCif() {
        return cif;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    public String getCustnm() {
        return custnm;
    }

    public void setCustnm(String custnm) {
        this.custnm = custnm;
    }

    public String getCfetssn() {
        return cfetssn;
    }

    public void setCfetssn(String cfetssn) {
        this.cfetssn = cfetssn;
    }

    public String getDealdate() {
        return dealdate;
    }

    public void setDealdate(String dealdate) {
        this.dealdate = dealdate;
    }

    public String getVdate() {
        return vdate;
    }

    public void setVdate(String vdate) {
        this.vdate = vdate;
    }

    public String getMatdate() {
        return matdate;
    }

    public void setMatdate(String matdate) {
        this.matdate = matdate;
    }

    public BigDecimal getFaceamt() {
        return faceamt;
    }

    public void setFaceamt(BigDecimal faceamt) {
        this.faceamt = faceamt;
    }

    public BigDecimal getComprocdamt() {
        return comprocdamt;
    }

    public void setComprocdamt(BigDecimal comprocdamt) {
        this.comprocdamt = comprocdamt;
    }

    public BigDecimal getMatprocdamt() {
        return matprocdamt;
    }

    public void setMatprocdamt(BigDecimal matprocdamt) {
        this.matprocdamt = matprocdamt;
    }

    public BigDecimal getComprocdamtzz() {
        return comprocdamtzz;
    }

    public void setComprocdamtzz(BigDecimal comprocdamtzz) {
        this.comprocdamtzz = comprocdamtzz;
    }

    public BigDecimal getMatprocdamtzz() {
        return matprocdamtzz;
    }

    public void setMatprocdamtzz(BigDecimal matprocdamtzz) {
        this.matprocdamtzz = matprocdamtzz;
    }

    public BigDecimal getComprocdamtsub() {
        return comprocdamtsub;
    }

    public void setComprocdamtsub(BigDecimal comprocdamtsub) {
        this.comprocdamtsub = comprocdamtsub;
    }

    public BigDecimal getMatprocdamtsub() {
        return matprocdamtsub;
    }

    public void setMatprocdamtsub(BigDecimal matprocdamtsub) {
        this.matprocdamtsub = matprocdamtsub;
    }

    public String getDealstatus() {
        return dealstatus;
    }

    public void setDealstatus(String dealstatus) {
        this.dealstatus = dealstatus;
    }

    public String getDealtext() {
        return dealtext;
    }

    public void setDealtext(String dealtext) {
        this.dealtext = dealtext;
    }

    public String getComccysmeans() {
        return comccysmeans;
    }

    public void setComccysmeans(String comccysmeans) {
        this.comccysmeans = comccysmeans;
    }

    public String getMatccysmeans() {
        return matccysmeans;
    }

    public void setMatccysmeans(String matccysmeans) {
        this.matccysmeans = matccysmeans;
    }

    public String getSetttypeB() {
        return setttypeB;
    }

    public void setSetttypeB(String setttypeB) {
        this.setttypeB = setttypeB;
    }

    public String getSettdescrB() {
        return settdescrB;
    }

    public void setSettdescrB(String settdescrB) {
        this.settdescrB = settdescrB;
    }

    public String getSetttypeE() {
        return setttypeE;
    }

    public void setSetttypeE(String setttypeE) {
        this.setttypeE = setttypeE;
    }

    public String getSettdescrE() {
        return settdescrE;
    }

    public void setSettdescrE(String settdescrE) {
        this.settdescrE = settdescrE;
    }

    public String getXmlmatchid() {
        return xmlmatchid;
    }

    public void setXmlmatchid(String xmlmatchid) {
        this.xmlmatchid = xmlmatchid;
    }

    public String getChecksts() {
        return checksts;
    }

    public void setChecksts(String checksts) {
        this.checksts = checksts;
    }

    public Date getChecktime() {
        return checktime;
    }

    public void setChecktime(Date checktime) {
        this.checktime = checktime;
    }

    public String getRequestsn() {
        return requestsn;
    }

    public void setRequestsn(String requestsn) {
        this.requestsn = requestsn;
    }

    public String getSendsts() {
        return sendsts;
    }

    public void setSendsts(String sendsts) {
        this.sendsts = sendsts;
    }

    public String getSendtime() {
        return sendtime;
    }

    public void setSendtime(String sendtime) {
        this.sendtime = sendtime;
    }

    public String getCnbondretsts() {
        return cnbondretsts;
    }

    public void setCnbondretsts(String cnbondretsts) {
        this.cnbondretsts = cnbondretsts;
    }

    public String getCnbondrettime() {
        return cnbondrettime;
    }

    public void setCnbondrettime(String cnbondrettime) {
        this.cnbondrettime = cnbondrettime;
    }

    public String getAcctqryacctid() {
        return acctqryacctid;
    }

    public void setAcctqryacctid(String acctqryacctid) {
        this.acctqryacctid = acctqryacctid;
    }

    public String getTxflowid() {
        return txflowid;
    }

    public void setTxflowid(String txflowid) {
        this.txflowid = txflowid;
    }

    public String getInstrsts() {
        return instrsts;
    }

    public void setInstrsts(String instrsts) {
        this.instrsts = instrsts;
    }

    public String getTxrsltcd() {
        return txrsltcd;
    }

    public void setTxrsltcd(String txrsltcd) {
        this.txrsltcd = txrsltcd;
    }

    public String getInserrinf() {
        return inserrinf;
    }

    public void setInserrinf(String inserrinf) {
        this.inserrinf = inserrinf;
    }

    public String getOrgtrcnfrmind() {
        return orgtrcnfrmind;
    }

    public void setOrgtrcnfrmind(String orgtrcnfrmind) {
        this.orgtrcnfrmind = orgtrcnfrmind;
    }

    public String getCtrcnfrmind() {
        return ctrcnfrmind;
    }

    public void setCtrcnfrmind(String ctrcnfrmind) {
        this.ctrcnfrmind = ctrcnfrmind;
    }

    public String getCtrctid() {
        return ctrctid;
    }

    public void setCtrctid(String ctrctid) {
        this.ctrctid = ctrctid;
    }

    public String getCtrctsts() {
        return ctrctsts;
    }

    public void setCtrctsts(String ctrctsts) {
        this.ctrctsts = ctrctsts;
    }

    public String getCtrctblcksts() {
        return ctrctblcksts;
    }

    public void setCtrctblcksts(String ctrctblcksts) {
        this.ctrctblcksts = ctrctblcksts;
    }

    public String getCtrctfaildrsn() {
        return ctrctfaildrsn;
    }

    public void setCtrctfaildrsn(String ctrctfaildrsn) {
        this.ctrctfaildrsn = ctrctfaildrsn;
    }

    public String getQueryrettime() {
        return queryrettime;
    }

    public void setQueryrettime(String queryrettime) {
        this.queryrettime = queryrettime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", instrid=").append(instrid);
        sb.append(", br=").append(br);
        sb.append(", dealno=").append(dealno);
        sb.append(", ps=").append(ps);
        sb.append(", prodcode=").append(prodcode);
        sb.append(", prodtype=").append(prodtype);
        sb.append(", secid=").append(secid);
        sb.append(", secidnm=").append(secidnm);
        sb.append(", cif=").append(cif);
        sb.append(", custnm=").append(custnm);
        sb.append(", cfetssn=").append(cfetssn);
        sb.append(", dealdate=").append(dealdate);
        sb.append(", vdate=").append(vdate);
        sb.append(", matdate=").append(matdate);
        sb.append(", faceamt=").append(faceamt);
        sb.append(", comprocdamt=").append(comprocdamt);
        sb.append(", matprocdamt=").append(matprocdamt);
        sb.append(", comprocdamtzz=").append(comprocdamtzz);
        sb.append(", matprocdamtzz=").append(matprocdamtzz);
        sb.append(", comprocdamtsub=").append(comprocdamtsub);
        sb.append(", matprocdamtsub=").append(matprocdamtsub);
        sb.append(", dealstatus=").append(dealstatus);
        sb.append(", dealtext=").append(dealtext);
        sb.append(", comccysmeans=").append(comccysmeans);
        sb.append(", matccysmeans=").append(matccysmeans);
        sb.append(", setttypeB=").append(setttypeB);
        sb.append(", settdescrB=").append(settdescrB);
        sb.append(", setttypeE=").append(setttypeE);
        sb.append(", settdescrE=").append(settdescrE);
        sb.append(", xmlmatchid=").append(xmlmatchid);
        sb.append(", checksts=").append(checksts);
        sb.append(", checktime=").append(checktime);
        sb.append(", requestsn=").append(requestsn);
        sb.append(", sendsts=").append(sendsts);
        sb.append(", sendtime=").append(sendtime);
        sb.append(", cnbondretsts=").append(cnbondretsts);
        sb.append(", cnbondrettime=").append(cnbondrettime);
        sb.append(", acctqryacctid=").append(acctqryacctid);
        sb.append(", txflowid=").append(txflowid);
        sb.append(", instrsts=").append(instrsts);
        sb.append(", txrsltcd=").append(txrsltcd);
        sb.append(", inserrinf=").append(inserrinf);
        sb.append(", orgtrcnfrmind=").append(orgtrcnfrmind);
        sb.append(", ctrcnfrmind=").append(ctrcnfrmind);
        sb.append(", ctrctid=").append(ctrctid);
        sb.append(", ctrctsts=").append(ctrctsts);
        sb.append(", ctrctblcksts=").append(ctrctblcksts);
        sb.append(", ctrctfaildrsn=").append(ctrctfaildrsn);
        sb.append(", queryrettime=").append(queryrettime);
        sb.append("]");
        return sb.toString();
    }
}