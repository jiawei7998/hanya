package com.singlee.ifs.cdtc.service;

import com.singlee.ifs.cdtc.model.IfsCdtcCnbondPkg;

/**
 * @author ：chenguo
 * @date ：Created in 2021/11/29 13:19
 * @description：${description}
 * @modified By：
 * @version:
 */
public interface IfsCdtcCnbondPkgService {


    int insert(IfsCdtcCnbondPkg record);

    int insertSelective(IfsCdtcCnbondPkg record);

    int deleteByPrimaryKey(String msgsn, String msgfedealno);

    IfsCdtcCnbondPkg selectByPrimaryKey(String msgsn, String msgfedealno);

    int updateByPrimaryKeySelective(IfsCdtcCnbondPkg record);

    int updateByPrimaryKey(IfsCdtcCnbondPkg record);
}





