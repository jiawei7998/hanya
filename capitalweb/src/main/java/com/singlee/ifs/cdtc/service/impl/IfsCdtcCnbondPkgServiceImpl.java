package com.singlee.ifs.cdtc.service.impl;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.singlee.ifs.cdtc.model.IfsCdtcCnbondPkg;
import com.singlee.ifs.cdtc.mapper.IfsCdtcCnbondPkgMapper;
import com.singlee.ifs.cdtc.service.IfsCdtcCnbondPkgService;

/**
 * @author ：chenguo
 * @date ：Created in 2021/11/29 13:19
 * @description：${description}
 * @modified By：
 * @version:
 */
@Service
public class IfsCdtcCnbondPkgServiceImpl implements IfsCdtcCnbondPkgService {

    @Resource
    private IfsCdtcCnbondPkgMapper ifsCdtcCnbondPkgMapper;

    @Override
    public int insert(IfsCdtcCnbondPkg record) {
        return ifsCdtcCnbondPkgMapper.insert(record);
    }

    @Override
    public int insertSelective(IfsCdtcCnbondPkg record) {
        return ifsCdtcCnbondPkgMapper.insertSelective(record);
    }

    @Override
    public int deleteByPrimaryKey(String msgsn, String msgfedealno) {
        return ifsCdtcCnbondPkgMapper.deleteByPrimaryKey(msgsn, msgfedealno);
    }

    @Override
    public IfsCdtcCnbondPkg selectByPrimaryKey(String msgsn, String msgfedealno) {
        return ifsCdtcCnbondPkgMapper.selectByPrimaryKey(msgsn, msgfedealno);
    }

    @Override
    public int updateByPrimaryKeySelective(IfsCdtcCnbondPkg record) {
        return ifsCdtcCnbondPkgMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(IfsCdtcCnbondPkg record) {
        return ifsCdtcCnbondPkgMapper.updateByPrimaryKey(record);
    }
}





