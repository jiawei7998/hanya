package com.singlee.ifs.cdtc.mapper;

import com.singlee.ifs.cdtc.model.IfsCdtcInstruct;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author ：chenguo
 * @date ：Created in 2021/11/29 14:05
 * @description：${description}
 * @modified By：
 * @version:
 */
@Mapper
public interface IfsCdtcInstructMapper {
    /**
     * delete by primary key
     *
     * @param instrid primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(@Param("instrid") String instrid, @Param("ctrctid") String ctrctid);

    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(IfsCdtcInstruct record);

    /**
     * insert record to table selective
     *
     * @param record the record
     * @return insert count
     */
    int insertSelective(IfsCdtcInstruct record);

    /**
     * select by primary key
     *
     * @param instrid primary key
     * @return object by primary key
     */
    IfsCdtcInstruct selectByPrimaryKey(@Param("instrid") String instrid, @Param("ctrctid") String ctrctid);

    /**
     * update record selective
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(IfsCdtcInstruct record);

    /**
     * update record
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(IfsCdtcInstruct record);
}