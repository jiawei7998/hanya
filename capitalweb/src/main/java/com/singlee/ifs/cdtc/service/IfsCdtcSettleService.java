package com.singlee.ifs.cdtc.service;

import com.singlee.ifs.cdtc.model.IfsCdtcSettle;

/**
 * @author ：chenguo
 * @date ：Created in 2021/11/29 14:03
 * @description：${description}
 * @modified By：
 * @version:
 */
public interface IfsCdtcSettleService {


    int insert(IfsCdtcSettle record);

    int insertSelective(IfsCdtcSettle record);

    int deleteByPrimaryKey(String instrid);

    IfsCdtcSettle selectByPrimaryKey(String instrid);

    int updateByPrimaryKeySelective(IfsCdtcSettle record);

    int updateByPrimaryKey(IfsCdtcSettle record);
}

