package com.singlee.ifs.cdtc.mapper;

import com.singlee.ifs.cdtc.model.IfsCdtcBond3;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author ：chenguo
 * @date ：Created in 2021/11/29 20:22
 * @description：${description}
 * @modified By：
 * @version:
 */
@Mapper
public interface IfsCdtcBond3Mapper {
    /**
     * delete by primary key
     *
     * @param bdid primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(@Param("bdid") String bdid, @Param("acrdintrstdt") String acrdintrstdt);

    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(IfsCdtcBond3 record);

    /**
     * insert record to table selective
     *
     * @param record the record
     * @return insert count
     */
    int insertSelective(IfsCdtcBond3 record);

    /**
     * select by primary key
     *
     * @param bdid primary key
     * @return object by primary key
     */
    IfsCdtcBond3 selectByPrimaryKey(@Param("bdid") String bdid, @Param("acrdintrstdt") String acrdintrstdt);

    /**
     * update record selective
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(IfsCdtcBond3 record);

    /**
     * update record
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(IfsCdtcBond3 record);
}