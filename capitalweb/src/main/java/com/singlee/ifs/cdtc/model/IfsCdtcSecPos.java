package com.singlee.ifs.cdtc.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author ：chenguo
 * @date ：Created in 2021/11/29 14:05
 * @description：${description}
 * @modified By：
 * @version:
 */
public class IfsCdtcSecPos implements Serializable {
    /**
     * 债券简称
     */
    private String secid;

    /**
     * 账务日期
     */
    private Date postdate;

    /**
     * 债券代码
     */
    private String secidnm;

    /**
     * 可用科目 万元
     */
    private String ableuseamt;

    /**
     * 待付科目 万元
     */
    private String waitpayamt;

    /**
     * 质押科目 万元
     */
    private BigDecimal repozyamt;

    /**
     * 待购回科目 万元
     */
    private BigDecimal reporevamt;

    /**
     * 冻结科目
     */
    private BigDecimal frozenamt;

    /**
     * 债券余额 万元
     */
    private BigDecimal totalamt;

    /**
     * 承销额度
     */
    private BigDecimal cxamt;

    /**
     * 承销额度待付
     */
    private BigDecimal cxwaitpayamt;

    private static final long serialVersionUID = 1L;

    public String getSecid() {
        return secid;
    }

    public void setSecid(String secid) {
        this.secid = secid;
    }

    public Date getPostdate() {
        return postdate;
    }

    public void setPostdate(Date postdate) {
        this.postdate = postdate;
    }

    public String getSecidnm() {
        return secidnm;
    }

    public void setSecidnm(String secidnm) {
        this.secidnm = secidnm;
    }

    public String getAbleuseamt() {
        return ableuseamt;
    }

    public void setAbleuseamt(String ableuseamt) {
        this.ableuseamt = ableuseamt;
    }

    public String getWaitpayamt() {
        return waitpayamt;
    }

    public void setWaitpayamt(String waitpayamt) {
        this.waitpayamt = waitpayamt;
    }

    public BigDecimal getRepozyamt() {
        return repozyamt;
    }

    public void setRepozyamt(BigDecimal repozyamt) {
        this.repozyamt = repozyamt;
    }

    public BigDecimal getReporevamt() {
        return reporevamt;
    }

    public void setReporevamt(BigDecimal reporevamt) {
        this.reporevamt = reporevamt;
    }

    public BigDecimal getFrozenamt() {
        return frozenamt;
    }

    public void setFrozenamt(BigDecimal frozenamt) {
        this.frozenamt = frozenamt;
    }

    public BigDecimal getTotalamt() {
        return totalamt;
    }

    public void setTotalamt(BigDecimal totalamt) {
        this.totalamt = totalamt;
    }

    public BigDecimal getCxamt() {
        return cxamt;
    }

    public void setCxamt(BigDecimal cxamt) {
        this.cxamt = cxamt;
    }

    public BigDecimal getCxwaitpayamt() {
        return cxwaitpayamt;
    }

    public void setCxwaitpayamt(BigDecimal cxwaitpayamt) {
        this.cxwaitpayamt = cxwaitpayamt;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", secid=").append(secid);
        sb.append(", postdate=").append(postdate);
        sb.append(", secidnm=").append(secidnm);
        sb.append(", ableuseamt=").append(ableuseamt);
        sb.append(", waitpayamt=").append(waitpayamt);
        sb.append(", repozyamt=").append(repozyamt);
        sb.append(", reporevamt=").append(reporevamt);
        sb.append(", frozenamt=").append(frozenamt);
        sb.append(", totalamt=").append(totalamt);
        sb.append(", cxamt=").append(cxamt);
        sb.append(", cxwaitpayamt=").append(cxwaitpayamt);
        sb.append("]");
        return sb.toString();
    }
}