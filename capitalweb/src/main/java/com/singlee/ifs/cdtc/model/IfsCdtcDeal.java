package com.singlee.ifs.cdtc.model;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/11/29 15:21
 * @description：${description}
 * @modified By：
 * @version:   
 */
public class IfsCdtcDeal implements Serializable {
    /**
    * 业务标识号
    */
    private String txid;

    /**
    * 结算标识号
    */
    private String ctrctid;

    /**
    * 付券方帐户
    */
    private String givacctnm;

    /**
    * 收券方帐户
    */
    private String takacctnm;

    /**
    * 债券简称
    */
    private String bdshrtnm;

    /**
    * 匹配状态
    */
    private String matchingstate;

    /**
    * 确认状态
    */
    private String confirmstate;

    /**
    * 合同状态
    */
    private String ctrctsts;

    /**
    * 确认发送
    */
    private String issend;

    /**
    * 业务类型
    */
    private String biztp;

    //交割日
    private  String mdate;
    //券面总额(回购专用)
    private BigDecimal bdamt;
    //首次交割日(回购专用)
    private  String dt1;
    //到期交割日(回购专用)
    private  String dt2;
    //首期结算金额(回购专用)
    private  BigDecimal val1;
    //到期结算金额(回购专用)
    private  BigDecimal val2;
    //初期合同状态(回购专用)
    private  String ctrctsts1;
    //期末合同状态(回购专用)
    private  String ctrctsts2;

    private static final long serialVersionUID = 1L;

    public String getTxid() {
        return txid;
    }

    public void setTxid(String txid) {
        this.txid = txid;
    }

    public String getCtrctid() {
        return ctrctid;
    }

    public void setCtrctid(String ctrctid) {
        this.ctrctid = ctrctid;
    }

    public String getGivacctnm() {
        return givacctnm;
    }

    public void setGivacctnm(String givacctnm) {
        this.givacctnm = givacctnm;
    }

    public String getTakacctnm() {
        return takacctnm;
    }

    public void setTakacctnm(String takacctnm) {
        this.takacctnm = takacctnm;
    }

    public String getBdshrtnm() {
        return bdshrtnm;
    }

    public void setBdshrtnm(String bdshrtnm) {
        this.bdshrtnm = bdshrtnm;
    }

    public String getMatchingstate() {
        return matchingstate;
    }

    public void setMatchingstate(String matchingstate) {
        this.matchingstate = matchingstate;
    }

    public String getConfirmstate() {
        return confirmstate;
    }

    public void setConfirmstate(String confirmstate) {
        this.confirmstate = confirmstate;
    }

    public String getCtrctsts() {
        return ctrctsts;
    }

    public void setCtrctsts(String ctrctsts) {
        this.ctrctsts = ctrctsts;
    }

    public String getIssend() {
        return issend;
    }

    public void setIssend(String issend) {
        this.issend = issend;
    }

    public String getBiztp() {
        return biztp;
    }

    public void setBiztp(String biztp) {
        this.biztp = biztp;
    }

    public String getMdate() {
        return mdate;
    }

    public void setMdate(String mdate) {
        this.mdate = mdate;
    }

    public BigDecimal getBdamt() {
        return bdamt;
    }

    public void setBdamt(BigDecimal bdamt) {
        this.bdamt = bdamt;
    }

    public String getDt1() {
        return dt1;
    }

    public void setDt1(String dt1) {
        this.dt1 = dt1;
    }

    public String getDt2() {
        return dt2;
    }

    public void setDt2(String dt2) {
        this.dt2 = dt2;
    }

    public BigDecimal getVal1() {
        return val1;
    }

    public void setVal1(BigDecimal val1) {
        this.val1 = val1;
    }

    public BigDecimal getVal2() {
        return val2;
    }

    public void setVal2(BigDecimal val2) {
        this.val2 = val2;
    }

    public String getCtrctsts1() {
        return ctrctsts1;
    }

    public void setCtrctsts1(String ctrctsts1) {
        this.ctrctsts1 = ctrctsts1;
    }

    public String getCtrctsts2() {
        return ctrctsts2;
    }

    public void setCtrctsts2(String ctrctsts2) {
        this.ctrctsts2 = ctrctsts2;
    }

    @Override
    public String toString() {
        return "IfsCdtcDeal{" +
                "txid='" + txid + '\'' +
                ", ctrctid='" + ctrctid + '\'' +
                ", givacctnm='" + givacctnm + '\'' +
                ", takacctnm='" + takacctnm + '\'' +
                ", bdshrtnm='" + bdshrtnm + '\'' +
                ", matchingstate='" + matchingstate + '\'' +
                ", confirmstate='" + confirmstate + '\'' +
                ", ctrctsts='" + ctrctsts + '\'' +
                ", issend='" + issend + '\'' +
                ", biztp='" + biztp + '\'' +
                ", mdate='" + mdate + '\'' +
                ", bdamt=" + bdamt +
                ", dt1='" + dt1 + '\'' +
                ", dt2='" + dt2 + '\'' +
                ", val1=" + val1 +
                ", val2=" + val2 +
                ", ctrctsts1='" + ctrctsts1 + '\'' +
                ", ctrctsts2='" + ctrctsts2 + '\'' +
                '}';
    }
}