package com.singlee.ifs.cdtc.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.cdtc.model.IfsCdtcDeal;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.session.RowBounds;

import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/11/29 15:21
 * @description：${description}
 * @modified By：
 * @version:   
 */
@Mapper
public interface IfsCdtcDealMapper {
    /**
     * delete by primary key
     * @param txid primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(String txid);

    /**
     * insert record to table
     * @param record the record
     * @return insert count
     */
    int insert(IfsCdtcDeal record);

    /**
     * insert record to table selective
     * @param record the record
     * @return insert count
     */
    int insertSelective(IfsCdtcDeal record);

    /**
     * select by primary key
     * @param txid primary key
     * @return object by primary key
     */
    IfsCdtcDeal selectByPrimaryKey(String txid);

    /**
     * update record selective
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(IfsCdtcDeal record);

    /**
     * update record
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(IfsCdtcDeal record);

    Page<IfsCdtcDeal> searchIfsCdtcDeal(Map<String, Object> map, RowBounds rb);
}