package com.singlee.ifs.cdtc.service;

import com.singlee.ifs.cdtc.model.IfsCdtcInstruct;

/**
 * @author ：chenguo
 * @date ：Created in 2021/11/29 14:03
 * @description：${description}
 * @modified By：
 * @version:
 */
public interface IfsCdtcInstructService {


    int insert(IfsCdtcInstruct record);

    int insertSelective(IfsCdtcInstruct record);

    int deleteByPrimaryKey(String instrid, String ctrctid);

    IfsCdtcInstruct selectByPrimaryKey(String instrid, String ctrctid);

    int updateByPrimaryKeySelective(IfsCdtcInstruct record);

    int updateByPrimaryKey(IfsCdtcInstruct record);
}

