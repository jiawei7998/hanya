package com.singlee.ifs.cdtc.service.impl;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.singlee.ifs.cdtc.model.IfsCdtcBond3;
import com.singlee.ifs.cdtc.mapper.IfsCdtcBond3Mapper;
import com.singlee.ifs.cdtc.service.IfsCdtcBond3Service;

/**
 * @author ：chenguo
 * @date ：Created in 2021/11/29 20:15
 * @description：${description}
 * @modified By：
 * @version:
 */
@Service
public class IfsCdtcBond3ServiceImpl implements IfsCdtcBond3Service {

    @Resource
    private IfsCdtcBond3Mapper ifsCdtcBond3Mapper;

    @Override
    public int insert(IfsCdtcBond3 record) {
        return ifsCdtcBond3Mapper.insert(record);
    }

    @Override
    public int insertSelective(IfsCdtcBond3 record) {
        return ifsCdtcBond3Mapper.insertSelective(record);
    }

    @Override
    public int deleteByPrimaryKey(String bdid, String acrdintrstdt) {
        return ifsCdtcBond3Mapper.deleteByPrimaryKey(bdid, acrdintrstdt);
    }

    @Override
    public IfsCdtcBond3 selectByPrimaryKey(String bdid, String acrdintrstdt) {
        return ifsCdtcBond3Mapper.selectByPrimaryKey(bdid, acrdintrstdt);
    }

    @Override
    public int updateByPrimaryKeySelective(IfsCdtcBond3 record) {
        return ifsCdtcBond3Mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(IfsCdtcBond3 record) {
        return ifsCdtcBond3Mapper.updateByPrimaryKey(record);
    }
}


