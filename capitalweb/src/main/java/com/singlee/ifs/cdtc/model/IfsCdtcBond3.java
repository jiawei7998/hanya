package com.singlee.ifs.cdtc.model;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author ：chenguo
 * @date ：Created in 2021/11/29 20:22
 * @description：${description}
 * @modified By：
 * @version:
 */
public class IfsCdtcBond3 implements Serializable {
    /**
     * 债券代码
     */
    private String bdid;

    /**
     * 计息日
     */
    private String acrdintrstdt;

    /**
     * 交易流水标识(业务标识号)
     */
    private String txflowid;

    /**
     * ISIN编码
     */
    private String isin;

    /**
     * 债券简称
     */
    private String bdshrtnm;

    /**
     * 券面总额
     */
    private BigDecimal bdamt;

    /**
     * 债券质押率
     */
    private BigDecimal bdcollrate;

    /**
     * 净价（百元面值）
     */
    private BigDecimal pric1;

    /**
     * 全价（百元面值）
     */
    private BigDecimal pric2;

    /**
     * 应计利息
     */
    private BigDecimal acrdintrst;

    private static final long serialVersionUID = 1L;

    public String getBdid() {
        return bdid;
    }

    public void setBdid(String bdid) {
        this.bdid = bdid;
    }

    public String getAcrdintrstdt() {
        return acrdintrstdt;
    }

    public void setAcrdintrstdt(String acrdintrstdt) {
        this.acrdintrstdt = acrdintrstdt;
    }

    public String getTxflowid() {
        return txflowid;
    }

    public void setTxflowid(String txflowid) {
        this.txflowid = txflowid;
    }

    public String getIsin() {
        return isin;
    }

    public void setIsin(String isin) {
        this.isin = isin;
    }

    public String getBdshrtnm() {
        return bdshrtnm;
    }

    public void setBdshrtnm(String bdshrtnm) {
        this.bdshrtnm = bdshrtnm;
    }

    public BigDecimal getBdamt() {
        return bdamt;
    }

    public void setBdamt(BigDecimal bdamt) {
        this.bdamt = bdamt;
    }

    public BigDecimal getBdcollrate() {
        return bdcollrate;
    }

    public void setBdcollrate(BigDecimal bdcollrate) {
        this.bdcollrate = bdcollrate;
    }

    public BigDecimal getPric1() {
        return pric1;
    }

    public void setPric1(BigDecimal pric1) {
        this.pric1 = pric1;
    }

    public BigDecimal getPric2() {
        return pric2;
    }

    public void setPric2(BigDecimal pric2) {
        this.pric2 = pric2;
    }

    public BigDecimal getAcrdintrst() {
        return acrdintrst;
    }

    public void setAcrdintrst(BigDecimal acrdintrst) {
        this.acrdintrst = acrdintrst;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", bdid=").append(bdid);
        sb.append(", acrdintrstdt=").append(acrdintrstdt);
        sb.append(", txflowid=").append(txflowid);
        sb.append(", isin=").append(isin);
        sb.append(", bdshrtnm=").append(bdshrtnm);
        sb.append(", bdamt=").append(bdamt);
        sb.append(", bdcollrate=").append(bdcollrate);
        sb.append(", pric1=").append(pric1);
        sb.append(", pric2=").append(pric2);
        sb.append(", acrdintrst=").append(acrdintrst);
        sb.append("]");
        return sb.toString();
    }
}