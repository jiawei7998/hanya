package com.singlee.ifs.cdtc.controller;

import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.financial.page.PageInfo;
import com.singlee.ifs.cdtc.model.IfsCdtcDeal;
import com.singlee.ifs.cdtc.service.IfsCdtcDealService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * @author ：chenguo
 * @date ：Created in 2021/11/29 15:46
 * @description：中债业务状态
 * @modified By：
 * @version:
 */


@Controller
@RequestMapping(value = "/IfsCdtcDealController")
public class IfsCdtcDealController {

    IfsCdtcDealService ifsCdtcDealService;

    @ResponseBody
    @RequestMapping(value = "/searchIfsCdtcDeal")
    public RetMsg<PageInfo<IfsCdtcDeal>> searchIfsCdtcDeal(@RequestBody Map<String, Object> map){


        PageInfo<IfsCdtcDeal> page=ifsCdtcDealService.searchBondIfsCdtcDeal(map);
        return RetMsgHelper.ok(page);
    }


}
