package com.singlee.ifs.cdtc.service.impl;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.singlee.ifs.cdtc.model.IfsCdtcSecPos;
import com.singlee.ifs.cdtc.mapper.IfsCdtcSecPosMapper;
import com.singlee.ifs.cdtc.service.IfsCdtcSecPosService;import java.util.Date;

/**
 * @author ：chenguo
 * @date ：Created in 2021/11/29 14:03
 * @description：${description}
 * @modified By：
 * @version:
 */
@Service
public class IfsCdtcSecPosServiceImpl implements IfsCdtcSecPosService {

    @Resource
    private IfsCdtcSecPosMapper ifsCdtcSecPosMapper;

    @Override
    public int insert(IfsCdtcSecPos record) {
        return ifsCdtcSecPosMapper.insert(record);
    }

    @Override
    public int insertSelective(IfsCdtcSecPos record) {
        return ifsCdtcSecPosMapper.insertSelective(record);
    }

    @Override
    public int deleteByPrimaryKey(String secid, Date postdate) {
        return ifsCdtcSecPosMapper.deleteByPrimaryKey(secid, postdate);
    }

    @Override
    public IfsCdtcSecPos selectByPrimaryKey(String secid, Date postdate) {
        return ifsCdtcSecPosMapper.selectByPrimaryKey(secid, postdate);
    }

    @Override
    public int updateByPrimaryKeySelective(IfsCdtcSecPos record) {
        return ifsCdtcSecPosMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(IfsCdtcSecPos record) {
        return ifsCdtcSecPosMapper.updateByPrimaryKey(record);
    }
}

