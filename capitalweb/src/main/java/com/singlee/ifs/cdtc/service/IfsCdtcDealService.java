package com.singlee.ifs.cdtc.service;

import com.singlee.financial.page.PageInfo;
import com.singlee.ifs.cdtc.model.IfsCdtcDeal;

import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/11/29 15:21
 * @description：${description}
 * @modified By：
 * @version:   
 */
public interface IfsCdtcDealService{


    int deleteByPrimaryKey(String txid);

    int insert(IfsCdtcDeal record);

    int insertSelective(IfsCdtcDeal record);

    IfsCdtcDeal selectByPrimaryKey(String txid);

    int updateByPrimaryKeySelective(IfsCdtcDeal record);

    int updateByPrimaryKey(IfsCdtcDeal record);

    PageInfo<IfsCdtcDeal> searchBondIfsCdtcDeal(Map<String, Object> map) ;

}
