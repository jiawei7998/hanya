package com.singlee.ifs.cdtc.mapper;

import com.singlee.ifs.cdtc.model.IfsCdtcSecPos;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;

/**
 * @author ：chenguo
 * @date ：Created in 2021/11/29 14:05
 * @description：${description}
 * @modified By：
 * @version:
 */
@Mapper
public interface IfsCdtcSecPosMapper {
    /**
     * delete by primary key
     *
     * @param secid primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(@Param("secid") String secid, @Param("postdate") Date postdate);

    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(IfsCdtcSecPos record);

    /**
     * insert record to table selective
     *
     * @param record the record
     * @return insert count
     */
    int insertSelective(IfsCdtcSecPos record);

    /**
     * select by primary key
     *
     * @param secid primary key
     * @return object by primary key
     */
    IfsCdtcSecPos selectByPrimaryKey(@Param("secid") String secid, @Param("postdate") Date postdate);

    /**
     * update record selective
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(IfsCdtcSecPos record);

    /**
     * update record
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(IfsCdtcSecPos record);
}