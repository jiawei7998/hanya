package com.singlee.ifs.controller;

import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.financial.bean.SlDldtBean;
import com.singlee.financial.opics.IDldtCountServer;
import com.singlee.financial.page.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller
@RequestMapping(value="/ifsDldtCount")
public class ifsDldtCountController {
	@Autowired
	IDldtCountServer iDldtCountServer;
	// 分页查询
	@ResponseBody
	@RequestMapping(value = "/searchDldtCount")
	public RetMsg<PageInfo<SlDldtBean>> searchCcySwapPage(
			@RequestBody Map<String, Object> param) {
		PageInfo<SlDldtBean> page = iDldtCountServer.searchDlCount(param);
		return RetMsgHelper.ok(page);
	}
}
