package com.singlee.ifs.controller;

import com.singlee.capital.common.exception.RException;
import com.singlee.capital.credit.util.ExcelUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.financial.bean.SlIrs9Bean;
import com.singlee.financial.opics.IRefluxFileServer;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.CellStyle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 导出回流文件报表Controller
 */
@Controller
@RequestMapping(value = "/RefluxFileExportController")
public class RefluxFileExportController extends CommonController {

	@Autowired
	private IRefluxFileServer refluxFileServer;

	private static Logger logger = Logger.getLogger(RefluxFileExportController.class);

	

	/**
	 * 回流文件报表导出
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/exportRefluxFileReport")
	public void exportRefluxFileReport(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String ua = request.getHeader("User-Agent");
		String postdate = request.getParameter("postdate");
		String filename = "回流文件查询.xlsx"; // 文件名
		String encode_filename = "";
		try {
			encode_filename = URLEncoder.encode(filename, "utf-8");
		} catch (UnsupportedEncodingException e1) {
			throw new RException(e1.getMessage());
		}
		response.setContentType("application/vnd.ms-excel");
		if (ua != null) {
			response.setHeader("Content-Disposition", "attachment; filename=" + encode_filename);
		}
		response.setHeader("Connection", "close");
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("postdate", postdate);
			ExcelUtil e = downloadRefluxFileReportExcel(map);
			OutputStream ouputStream = response.getOutputStream();
			e.getWb().write(ouputStream);
			ouputStream.flush();
			ouputStream.close();
		} catch (Exception e) {
			logger.error("回流文件查询报表下载出错", e);
			throw new RException(e);
		}
	}
	/**
	 * 回流文件查询 统计Excel文件下载内容
	 */
	public ExcelUtil downloadRefluxFileReportExcel(Map<String, Object> map) throws Exception {
		ExcelUtil excel = null;
		try {
			/* 先查全部数据，再设置表格属性以及内容 */
			List<SlIrs9Bean> list=refluxFileServer.searchRefluxFileCount(map).getList();
			// 表头
			excel = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
			CellStyle center = excel.getDefaultCenterStrCellStyle();
			CellStyle left = excel.getDefaultLeftStrCellStyle();
			int sheet = 0;
			int row = 0;
			// 表格sheet名称
			excel.getWb().createSheet("回流文件查询");
			// 设置表头字段名
			excel.writeStr(sheet, row, "A", center, "编号");
			excel.writeStr(sheet, row, "B", center, "数据日期");
			excel.writeStr(sheet, row, "C", center, "批次号");
			excel.writeStr(sheet, row, "D", center, "部门");
			excel.writeStr(sheet, row, "E", center, "债券号");
			excel.writeStr(sheet, row, "F", center, "投资组合1");
			excel.writeStr(sheet, row, "G", center, "交易类");
			excel.writeStr(sheet, row, "H", center, "投资组合2");
			excel.writeStr(sheet, row, "I", center, "资产编号");
			excel.writeStr(sheet, row, "J", center, "业务条线代码");
			excel.writeStr(sheet, row, "K", center, "产品大类代码");
			excel.writeStr(sheet, row, "L", center, "业务产品类型代码");
			excel.writeStr(sheet, row, "M", center, "系统判断阶段结果");
			excel.writeStr(sheet, row, "N", center, "人工认定阶段结果");
			excel.writeStr(sheet, row, "O", center, "最终阶段判断结果");
			excel.writeStr(sheet, row, "P", center, "违约概率");
			excel.writeStr(sheet, row, "Q", center, "前瞻性调整后违约概率");
			excel.writeStr(sheet, row, "R", center, "违约损失率");
			excel.writeStr(sheet, row, "S", center, "违约风险暴露");
			excel.writeStr(sheet, row, "T", center, "本金计提金额");
			excel.writeStr(sheet, row, "U", center, "利息计提金额");
			excel.writeStr(sheet, row, "V", center, "表外计提金额");
			excel.writeStr(sheet, row, "W", center, "增提金额");
			excel.writeStr(sheet, row, "X", center, "增提比例");
			excel.writeStr(sheet, row, "Y", center, "系统计算计提金额");
			excel.writeStr(sheet, row, "Z", center, "人工认定计提金额");
			excel.writeStr(sheet, row, "AA", center, "增提前计提金额");
			excel.writeStr(sheet, row, "AB", center, "最终结果计提金额");
			excel.writeStr(sheet, row, "AC", center, "资产余额");
			excel.writeStr(sheet, row, "AD", center, "币种代码");
			excel.writeStr(sheet, row, "AE", center, "客户编号");
			excel.writeStr(sheet, row, "AF", center, "客户名称");
			excel.writeStr(sheet, row, "AG", center, "信用等级代码");
			excel.writeStr(sheet, row, "AH", center, "申请时点信用等级代码");
			excel.writeStr(sheet, row, "AI", center, "合同编号");
			excel.writeStr(sheet, row, "AJ", center, "业务所在机构编号");
			excel.writeStr(sheet, row, "AK", center, "业务所在机构名称");
			excel.writeStr(sheet, row, "AL", center, "是否前瞻性调整");
			excel.writeStr(sheet, row, "AM", center, "发放日期");
			excel.writeStr(sheet, row, "AN", center, "到期日期");
			excel.writeStr(sheet, row, "AO", center, "逾期天数");
			excel.writeStr(sheet, row, "AP", center, "资产分类结果代码");
			excel.writeStr(sheet, row, "AQ", center, "会计科目编号");
			excel.writeStr(sheet, row, "AR", center, "金融资产三分类");
			excel.writeStr(sheet, row, "AS", center, "逾期状态代码");
			excel.writeStr(sheet, row, "AT", center, "企业规模代码");
			excel.writeStr(sheet, row, "AU", center, "国标行业类型代码");
			excel.writeStr(sheet, row, "AV", center, "国标行业门类代码");
			excel.writeStr(sheet, row, "AW", center, "国标行业大类代码");
			excel.writeStr(sheet, row, "AX", center, "国标行业中类代码");
			excel.writeStr(sheet, row, "AY", center, "执行利率");
			excel.writeStr(sheet, row, "AZ", center, "来源系统");
			excel.writeStr(sheet, row, "BA", center, "创建人");
			excel.writeStr(sheet, row, "BB", center, "创建时间");
			excel.writeStr(sheet, row, "BC", center, "更新人");
			excel.writeStr(sheet, row, "BD", center, "更新时间");
			excel.writeStr(sheet, row, "BE", center, "多实体标识");
			excel.writeStr(sheet, row, "BF", center, "框架版本号");

			
		
			// 设置列宽
			excel.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(8, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(9, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(10, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(11, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(12, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(13, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(14, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(15, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(16, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(17, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(18, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(19, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(20, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(21, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(22, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(23, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(24, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(25, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(26, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(27, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(28, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(29, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(30, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(31, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(32, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(33, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(34, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(35, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(36, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(37, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(38, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(39, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(40, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(41, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(42, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(43, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(44, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(45, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(46, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(47, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(48, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(49, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(50, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(51, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(52, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(53, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(54, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(55, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(56, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(57, 20 * 256);
            for (int i = 0; i < list.size(); i++) {
            	row++;
            	SlIrs9Bean slIrsBean=list.get(i);
            	DecimalFormat df = new DecimalFormat("#0.00");
            	excel.writeStr(sheet, row, "A", center, slIrsBean.getIrs9_id());
      		    excel.writeStr(sheet, row, "B", center, slIrsBean.getData_dt());
      		    excel.writeStr(sheet, row, "C", center, slIrsBean.getBatch_id());
      			excel.writeStr(sheet, row, "D", center, slIrsBean.getBr());
      			excel.writeStr(sheet, row, "E", center, slIrsBean.getSecid());
      			excel.writeStr(sheet, row, "F", center, slIrsBean.getIrs9_cost());
      			excel.writeStr(sheet, row, "G", center, slIrsBean.getInvtype());
      			excel.writeStr(sheet, row, "H", center, slIrsBean.getPort());
      			excel.writeStr(sheet, row, "I", center, slIrsBean.getAsset_no());
      			excel.writeStr(sheet, row, "J", center, slIrsBean.getBiz_type_cd());
      			excel.writeStr(sheet, row, "K", center, slIrsBean.getProduct_level_one_cd());
      			excel.writeStr(sheet, row, "L", center, slIrsBean.getBusi_type_cd());
      			excel.writeStr(sheet, row, "M", center, slIrsBean.getStage_rslt_sys());
      			excel.writeStr(sheet, row, "N", center, slIrsBean.getStage_rslt_affirm());
      			excel.writeStr(sheet, row, "O", center, slIrsBean.getStage_rslt_final());
      			excel.writeStr(sheet, row, "P", center, slIrsBean.getPd());
      			excel.writeStr(sheet, row, "Q", center, slIrsBean.getPd_pre_adj());
      			excel.writeStr(sheet, row, "R", center, slIrsBean.getLgd());
      			excel.writeStr(sheet, row, "S", center, slIrsBean.getEad());
      			excel.writeStr(sheet, row, "T", center, slIrsBean.getEcl_prin());
      			excel.writeStr(sheet, row, "U", center, slIrsBean.getEcl_int());
      			excel.writeStr(sheet, row, "V", center, slIrsBean.getEcl_out());
      			excel.writeStr(sheet, row, "W", center, slIrsBean.getEcl_add());
      			excel.writeStr(sheet, row, "X",  center,slIrsBean.getEcl_add_ratio());
      			excel.writeStr(sheet, row, "Y", center, slIrsBean.getEcl_sys());
      			excel.writeStr(sheet, row, "Z", center, slIrsBean.getEcl_affirm());
      			excel.writeStr(sheet, row, "AA", center, slIrsBean.getEcl_bf_add());
      			excel.writeStr(sheet, row, "AB", center, slIrsBean.getEcl_final());
      			excel.writeStr(sheet, row, "AC", center, slIrsBean.getAsset_bal());
      			excel.writeStr(sheet, row, "AD", center, slIrsBean.getCurrency_cd());
      			excel.writeStr(sheet, row, "AE", center, slIrsBean.getCustomer_no());
      			excel.writeStr(sheet, row, "AF", center, slIrsBean.getCustomer_name());
      			excel.writeStr(sheet, row, "AG", center, slIrsBean.getRating_level_cd());
      			excel.writeStr(sheet, row, "AH", center, slIrsBean.getApply_rating_level_cd());
      			excel.writeStr(sheet, row, "AI", center, slIrsBean.getContract_no());
      			excel.writeStr(sheet, row, "AJ", center, slIrsBean.getOrg_no());
      			excel.writeStr(sheet, row, "AK", center, slIrsBean.getOrg_name());
      			excel.writeStr(sheet, row, "AL", center, slIrsBean.getIs_forward_adj());
      			excel.writeStr(sheet, row, "AM", center, slIrsBean.getPutout_dt());
      			excel.writeStr(sheet, row, "AN", center, slIrsBean.getMaturity());
      			excel.writeStr(sheet, row, "AO", center, slIrsBean.getOverdue_days());
      			excel.writeStr(sheet, row, "AP", center, slIrsBean.getLoan_cls_cd());
      			excel.writeStr(sheet, row, "AQ", center, slIrsBean.getAcct_subject_no());
      			excel.writeStr(sheet, row, "AR", center, slIrsBean.getAsset_three_class_cd());
      			excel.writeStr(sheet, row, "AS", center, slIrsBean.getOverdue_status_cd());
      			excel.writeStr(sheet, row, "AT", center, slIrsBean.getCustomer_size_cd());
      			excel.writeStr(sheet, row, "AU", center, slIrsBean.getIndustry_cd());
      			excel.writeStr(sheet, row, "AV", center, slIrsBean.getIndustry_level_one_cd());
      			excel.writeStr(sheet, row, "AW", center, slIrsBean.getIndustry_level_two_cd());
      			excel.writeStr(sheet, row, "AX", center, slIrsBean.getIndustry_level_three_cd());
      			excel.writeStr(sheet, row, "AY", center, slIrsBean.getExec_int_rate());
      			excel.writeStr(sheet, row, "AZ", center, slIrsBean.getSrc_sys());
      			excel.writeStr(sheet, row, "BA", center, slIrsBean.getCreate_user());
      			excel.writeStr(sheet, row, "BB", center, slIrsBean.getCreate_time());
      			excel.writeStr(sheet, row, "BC", center, slIrsBean.getUpdate_user());
      			excel.writeStr(sheet, row, "BD", center, slIrsBean.getUpdate_time());
      			excel.writeStr(sheet, row, "BE", center, slIrsBean.getTenant_id());
      			excel.writeStr(sheet, row, "BF", center, slIrsBean.getIrs9_version());
            }
			return excel;
		} catch (Exception e) {
			throw new RException(e);
		}
	}	
}