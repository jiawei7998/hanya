package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.ifs.model.IfsRevIswh;
import com.singlee.ifs.service.IfsRevIswhService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.Map;

/**
 * 
 * 互换类Controller
 * @author rongliu
 * @date 2018-08-17
 * 
 */
@Controller
@RequestMapping(value = "/IfsRevIswhController")
public class IfsRevIswhController {
	
	@Autowired
	IfsRevIswhService ifsRevIswhService;
	
	/**
	 * 查询  互换冲销     我发起的
	 * 
	 * @param params
	 * @return
	 * @author 
	 * @date 2018-08-17
	 */
	@ResponseBody
	@RequestMapping(value = "/searchRevIswhMine")
	public RetMsg<PageInfo<IfsRevIswh>> searchRevIswhMine(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsRevIswh> page = ifsRevIswhService.getRevIswhPage(params, 3);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 查询 互换冲销       待审批
	 * 
	 * @param params
	 * @return
	 * @author 
	 * @date 2018-08-17
	 */
	@ResponseBody
	@RequestMapping(value = "/searchRevIswhUnfinished")
	public RetMsg<PageInfo<IfsRevIswh>> searchRevIswhUnfinished(@RequestBody Map<String, Object> params) {
		String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse + ","
				+ DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
		String[] orderStatus = approveStatus.split(",");
		params.put("approveStatusNo", orderStatus);
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsRevIswh> page = ifsRevIswhService.getRevIswhPage(params, 1);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 查询 互换冲销       已审批
	 * 
	 * @param params
	 * @return
	 * @author 
	 * @date 2018-08-17
	 */
	@ResponseBody
	@RequestMapping(value = "/searchRevIswhFinished")
	public RetMsg<PageInfo<IfsRevIswh>> searchRevIswhFinished(@RequestBody Map<String, Object> params) {
		String approveStatus = ParameterUtil.getString(params, "approveStatus", "");
		if (StringUtil.isNullOrEmpty(approveStatus)) {
			approveStatus = DictConstants.ApproveStatus.ApprovedPass;
			approveStatus += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatus += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatus += "," + DictConstants.ApproveStatus.Verified;
			approveStatus += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			approveStatus += "," + DictConstants.ApproveStatus.Approving;

			String[] orderStatus = approveStatus.split(",");
			params.put("approveStatusNo", orderStatus);
		}
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsRevIswh> page = ifsRevIswhService.getRevIswhPage(params, 2);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 保存 互换类要素
	 */
	@ResponseBody
	@RequestMapping(value = "/saveIswhParam")
	public RetMsg<Serializable> saveIswhParam(@RequestBody IfsRevIswh entity) {
		String ticketId = entity.getTicketId();
		if (null == ticketId || "".equals(ticketId)) {// 新增
			ifsRevIswhService.insert(entity);
		} else {// 修改
			ifsRevIswhService.updateById(entity);
		}
		return RetMsgHelper.ok();
	}
	
	/***
	 * 删除 互换类要素
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteIswhParam")
	public RetMsg<Serializable> deleteIswhParam(@RequestBody Map<String, String> map) {
		String ticketId = map.get("ticketId");
		ifsRevIswhService.deleteById(ticketId);
		return RetMsgHelper.ok();
	}
	

}
