package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.dayend.service.DayendDateServiceProxy;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.ifs.model.IfsCustSettle;
import com.singlee.ifs.service.IfsCustSettleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 清算指令处理Controller
 * 
 * ClassName: IfsCustSettleController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason: TODO ADD REASON(可选). <br/>
 * date: 2018-5-30 下午07:30:00 <br/>
 * 
 * @author shenzl
 * @version
 * @since JDK 1.6
 */
@Controller
@RequestMapping(value = "/CustSettleController")
public class IfsCustSettleController extends CommonController {
	@Autowired
	private IfsCustSettleService ifsCustSettleService;

	/**
	 * 查询待经办结算主指令信息
	 * 
	 * map内参数：
	 * 
	 * @return
	 * @throws IOException
	 * @throws Exception
	 */

	@ResponseBody
	@RequestMapping(value = "/searchCustSettleInfo")
	public RetMsg<PageInfo<IfsCustSettle>> searchCustSettleInfo(@RequestBody Map<String, Object> map) {
		Page<IfsCustSettle> page = ifsCustSettleService.getCSList(map);
		return RetMsgHelper.ok(page);

	}

	@ResponseBody
	@RequestMapping(value = "/saveCustSettle")
	public RetMsg<Serializable> saveCustSettle(@RequestBody Map<String, Object> map) {
		String curDate=DayendDateServiceProxy.getInstance().getSettlementDate();
		map.put("manageDate", DateUtil.parse(curDate, "yyyy-MM-dd"));
		IfsCustSettle ifsCustSettle = ifsCustSettleService.selectById(map);
		if (ifsCustSettle == null) {
			map.put("dealFlag", "1");
			ifsCustSettleService.addCustSettle(map);
		} else {
			if ("0".equals(ifsCustSettle.getDealFlag())) {
				map.put("dealFlag", "1");
			} else {
				map.put("dealFlag", ifsCustSettle.getDealFlag());
			}
			ifsCustSettleService.updateByCno(map);
		}

		return RetMsgHelper.ok();
	}

	@ResponseBody
	@RequestMapping(value = "/deleteCustSettle")
	public RetMsg<Serializable> deleteCustSettle(@RequestBody Map<String, Object> map) {
		ifsCustSettleService.deleteSettle(map);
		return RetMsgHelper.ok();
	}
	//复核
	@ResponseBody
	@RequestMapping(value = "/confirmCustSettle")
	public RetMsg<Serializable> confirmCustSettle(@RequestBody Map<String, Object> map) {
		IfsCustSettle ifsCustSettle = ifsCustSettleService.selectById(map);
		map.put("dealFlag", "0");
		if("1".equals(ifsCustSettle.getIsdefault())){
			List<IfsCustSettle> list=ifsCustSettleService.getListByCno(map);
			for (IfsCustSettle ifsCustSettle2 : list) {
				Map<String, Object> map2=new HashMap<String, Object>();
				map2.put("seq", ifsCustSettle2.getSeq());
				map2.put("cno", ifsCustSettle2.getCno());
				map2.put("product", ifsCustSettle2.getProduct());
				map2.put("isdefault","2");
				ifsCustSettleService.updateIsdefaultById(map2);
			}
			map.put("seq", ifsCustSettle.getSeq());
			map.put("cno", ifsCustSettle.getCno());
			map.put("product", ifsCustSettle.getProduct());
			map.put("isdefault","1");
			ifsCustSettleService.updateIsdefaultById(map);
		}
		String curDate=DayendDateServiceProxy.getInstance().getSettlementDate();
		map.put("checkeDate", DateUtil.parse(curDate, "yyyy-MM-dd"));
		ifsCustSettleService.updateDealFlagById(map);
		return RetMsgHelper.ok();
	}
	
	//退回经办
	@ResponseBody
	@RequestMapping(value = "/backCustSettle")
	public RetMsg<Serializable> backCustSettle(@RequestBody Map<String, Object> map) {
		map.put("dealFlag", "2");
		String curDate=DayendDateServiceProxy.getInstance().getSettlementDate();
		map.put("checkeDate", DateUtil.parse(curDate, "yyyy-MM-dd"));
		ifsCustSettleService.updateDealFlagById(map);
		return RetMsgHelper.ok();
	}
	
	//经办
	@ResponseBody
	@RequestMapping(value = "/addCustSettle")
	public RetMsg<Serializable> addCustSettle(@RequestBody Map<String, Object> map) {
		map.put("dealFlag", "1");
		String curDate=DayendDateServiceProxy.getInstance().getSettlementDate();
		map.put("manageDate", DateUtil.parse(curDate, "yyyy-MM-dd"));
		ifsCustSettleService.updateDealFlagById(map);
		return RetMsgHelper.ok();
	}
	

}
