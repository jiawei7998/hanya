package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.ifs.model.IfsApprovermbIrs;
import com.singlee.ifs.service.IFSApproveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.io.Serializable;
import java.util.Map;

/**
 * 利率互换事前审批交易Controller
 * 
 * ClassName: IfsApprovermbIrsController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason: TODO ADD REASON(可选). <br/>
 * date: 2018-5-30 下午07:12:13 <br/>
 * 
 * @author zhangcm
 * @version
 * @since JDK 1.6
 */
@Controller
@RequestMapping(value = "/IfsApprovermbIrsController")
public class IfsApprovermbIrsController extends CommonController {
	@Autowired
	private IFSApproveService cfetsrmbIrsService;

	/**
	 * 保存成交单
	 * 
	 * @param IfsCfetsrmbIrs
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/saveIrs")
	public RetMsg<Serializable> saveIrs(@RequestBody IfsApprovermbIrs record) throws IOException {
		IfsApprovermbIrs irs = cfetsrmbIrsService.getIrsById(record);
		if (irs == null) {
			cfetsrmbIrsService.insertIrs(record);
		} else {
			cfetsrmbIrsService.updateIrs(record);
		}
		return RetMsgHelper.ok();
	}

	/**
	 * 成交单分页查询
	 * 
	 * @param IfsCfetsrmbIrs
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchCfetsrmbIrs")
	public RetMsg<PageInfo<IfsApprovermbIrs>> searchCfetsrmbIrs(@RequestBody IfsApprovermbIrs record) {
		Page<IfsApprovermbIrs> page = cfetsrmbIrsService.getIrsList(record);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 根据主键查询成交单
	 * 
	 * @param IfsCfetsrmbIrsKey
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchCfetsrmbIrsById")
	public RetMsg<IfsApprovermbIrs> searchCfetsrmbIrsById(@RequestBody IfsApprovermbIrs key) {
		IfsApprovermbIrs irs = cfetsrmbIrsService.getIrsById(key);
		return RetMsgHelper.ok(irs);
	}

	/**
	 * 根据主键查询成交单(返回值增加 流程id)
	 * 
	 * @param IfsCfetsrmbIrsKey
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchCfetsrmbIrsAndFlowIdById")
	public RetMsg<IfsApprovermbIrs> searchCfetsrmbIrsAndFlowIdById(@RequestBody Map<String, Object> key) {
		IfsApprovermbIrs irs = cfetsrmbIrsService.getIrsAndFlowIdById(key);
		return RetMsgHelper.ok(irs);
	}

	/**
	 * 删除成交单
	 * 
	 * @param IfsCfetsrmbIrsKey
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/deleteCfetsrmbIrs")
	public RetMsg<Serializable> deleteCfetsrmbIrs(@RequestBody IfsApprovermbIrs key) {
		cfetsrmbIrsService.deleteIrs(key);
		return RetMsgHelper.ok();
	}

	/**
	 * 查询我发起的
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-21
	 */
	@ResponseBody
	@RequestMapping(value = "/searchRmbIrsSwapMine")
	public RetMsg<PageInfo<IfsApprovermbIrs>> searchRmbIrsSwapMine(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsApprovermbIrs> page = cfetsrmbIrsService.getRmbIrsMinePage(params, 3);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 查询 利率互换 待审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageRmbIrsUnfinished")
	public RetMsg<PageInfo<IfsApprovermbIrs>> searchPageRmbIrsUnfinished(@RequestBody Map<String, Object> params) {
		String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse + ","
				+ DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
		String[] orderStatus = approveStatus.split(",");
		params.put("approveStatusNo", orderStatus);
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsApprovermbIrs> page = cfetsrmbIrsService.getRmbIrsMinePage(params, 1);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 查询 已审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageRmbIrsFinished")
	public RetMsg<PageInfo<IfsApprovermbIrs>> searchPageRmbIrsFinished(@RequestBody Map<String, Object> params) {
		String approveStatus = ParameterUtil.getString(params, "approveStatus", "");
		if (StringUtil.isNullOrEmpty(approveStatus)) {
			approveStatus = DictConstants.ApproveStatus.ApprovedPass;
			approveStatus += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatus += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatus += "," + DictConstants.ApproveStatus.Verified;
			approveStatus += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			// 20180502修改bug:已审批列表中增加"审批中"
			approveStatus += "," + DictConstants.ApproveStatus.Approving;

			String[] orderStatus = approveStatus.split(",");
			params.put("approveStatusNo", orderStatus);
		}
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsApprovermbIrs> page = cfetsrmbIrsService.getRmbIrsMinePage(params, 2);
		return RetMsgHelper.ok(page);
	}

	@ResponseBody
	@RequestMapping(value = "/searchPageApprovermbIrsPassed")
	public RetMsg<PageInfo<IfsApprovermbIrs>> searchPageApprovermbIrsPassed(@RequestBody Map<String, Object> params) {
		Page<IfsApprovermbIrs> page = cfetsrmbIrsService.getApprovePassedPage(params);
		return RetMsgHelper.ok(page);
	}

}
