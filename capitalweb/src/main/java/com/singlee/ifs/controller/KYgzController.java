package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.ifs.model.KYgzBean;
import com.singlee.ifs.service.KYgzBeanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Map;


@Controller
@RequestMapping(value = "/KYgzController")
public class KYgzController extends CommonController{


	@Autowired
	KYgzBeanService kYgzBeanService;
	/***
	 * 营改增列表
	 */
	@ResponseBody
	@RequestMapping(value = "/getPageList")
	public RetMsg<PageInfo<KYgzBean>> getPageList(@RequestBody Map<String, Object> map) {
		        
		Page<KYgzBean> pageList = kYgzBeanService.getPageList(map);
		return RetMsgHelper.ok(pageList);
	}

	/***
	 * 添加
	 */
	@ResponseBody
	@RequestMapping(value = "/addOrUpdate")
	public RetMsg<Serializable> addOrUpdate(@RequestBody KYgzBean kYgzBean) {
		if(kYgzBean.getId()==null){
			kYgzBeanService.insert(kYgzBean);
		}else{
			kYgzBeanService.updateByPrimaryKeySelective(kYgzBean);
		}
		return RetMsgHelper.ok("操作成功!");
	}

	/***
	 * 删除
	 */
	@ResponseBody
	@RequestMapping(value = "/delete")
	public RetMsg<Serializable> delete(@RequestBody BigDecimal id) {
		kYgzBeanService.deleteByPrimaryKey(id);
		return RetMsgHelper.ok("操作成功!");
	}





	
    
    
	
}
