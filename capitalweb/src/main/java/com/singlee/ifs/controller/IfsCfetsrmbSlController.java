package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.ifs.model.IfsCfetsrmbDetailSl;
import com.singlee.ifs.model.IfsCfetsrmbSl;
import com.singlee.ifs.model.IfsCfetsrmbSlKey;
import com.singlee.ifs.service.IFSService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Map;

/**
 * 债券借贷正式交易审批Controller
 * 
 * ClassName: IfsCfetsrmbSlController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason: TODO ADD REASON(可选). <br/>
 * date: 2018-5-30 下午07:28:45 <br/>
 * 
 * @author shenzl
 * @version
 * @since JDK 1.6
 */
@Controller
@RequestMapping(value = "/IfsCfetsrmbSlController")
public class IfsCfetsrmbSlController extends CommonController {
	@Autowired
	private IFSService cfetsrmbSlService;

	/**
	 * 保存成交单
	 * 
	 * @param IfsCfetsrmbSl
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/saveSl")
	public RetMsg<Serializable> saveSl(@RequestBody IfsCfetsrmbSl record) throws IOException {
		IfsCfetsrmbSl sl = cfetsrmbSlService.getSlById(record);
		String message   = cfetsrmbSlService.checkCust(record.getDealTransType(),record.getLendInst(),record.getCfetsno(), null, record.getProduct(), record.getProdType());
		if(StringUtil.isNotEmpty(message)) {
			return RetMsgHelper.fail(message);
		}
		if (sl == null) {
			message = cfetsrmbSlService.insertSl(record);
		} else {
			try {
				cfetsrmbSlService.updateSl(record);
				message = "修改成功";
			} catch (Exception e) {
				e.printStackTrace();
				message = "修改失败";
			}
		}
		return StringUtil.containsIgnoreCase(message, "成功")?RetMsgHelper.ok(message):RetMsgHelper.fail(message);
	}
	/**
	 * 查询质押债券详情信息
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-7-18
	 */
	@ResponseBody
	@RequestMapping(value = "/searchBondSlDetails")
	public RetMsg<PageInfo<IfsCfetsrmbDetailSl>> searchBondSlDetails(@RequestBody Map<String, Object> params) {
		Page<IfsCfetsrmbDetailSl> page = cfetsrmbSlService.searchBondSlDetails(params);
		return RetMsgHelper.ok(page);
	}
	/**
	 * 成交单分页查询
	 * 
	 * @param IfsCfetsrmbSl
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchCfetsrmbSl")
	public RetMsg<PageInfo<IfsCfetsrmbSl>> searchCfetsrmbSl(@RequestBody IfsCfetsrmbSl record) {
		Page<IfsCfetsrmbSl> page = cfetsrmbSlService.getSlList(record);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 根据主键查询成交单
	 * 
	 * @param IfsCfetsrmbSlKey
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchCfetsrmbSlById")
	public RetMsg<IfsCfetsrmbSl> searchCfetsrmbSlById(@RequestBody IfsCfetsrmbSlKey key) {
		IfsCfetsrmbSl sl = cfetsrmbSlService.getSlById(key);
		return RetMsgHelper.ok(sl);
	}

	/**
	 * 删除成交单
	 * 
	 * @param IfsCfetsrmbSlKey
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/deleteCfetsrmbSl")
	public RetMsg<Serializable> deleteCfetsrmbSl(@RequestBody IfsCfetsrmbSlKey key) {
		cfetsrmbSlService.deleteSl(key);
		return RetMsgHelper.ok();
	}

	/**
	 * 货币掉期-查询我发起的
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-21
	 */
	@ResponseBody
	@RequestMapping(value = "/searchRmbSlSwapMine")
	public RetMsg<PageInfo<IfsCfetsrmbSl>> searchRmbSlSwapMine(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsrmbSl> page = cfetsrmbSlService.getRmbSlMinePage(params, 3);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 货币掉期-查询 待审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageRmbSlUnfinished")
	public RetMsg<PageInfo<IfsCfetsrmbSl>> searchPageRmbSlUnfinished(@RequestBody Map<String, Object> params) {
		String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse + ","
				+ DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
		String[] orderStatus = approveStatus.split(",");
		params.put("approveStatusNo", orderStatus);
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsrmbSl> page = cfetsrmbSlService.getRmbSlMinePage(params, 1);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 货币掉期-查询 已审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageRmbSlFinished")
	public RetMsg<PageInfo<IfsCfetsrmbSl>> searchPageRmbSlFinished(@RequestBody Map<String, Object> params) {
		String approveStatusNo = ParameterUtil.getString(params, "approveStatus", "");
		if (StringUtil.isNullOrEmpty(approveStatusNo)) {
			approveStatusNo = DictConstants.ApproveStatus.ApprovedPass;
			approveStatusNo += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatusNo += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatusNo += "," + DictConstants.ApproveStatus.Verified;
			approveStatusNo += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			// 20180502修改bug:已审批列表中增加"审批中"
			approveStatusNo += "," + DictConstants.ApproveStatus.Approving;
			String[] orderStatus = approveStatusNo.split(",");
			params.put("approveStatusNo", orderStatus);
		}
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsrmbSl> page = cfetsrmbSlService.getRmbSlMinePage(params, 2);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 根据主键查询成交单(返回值增加 流程id)
	 * 
	 * @param
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchCfetsrmbSlAndFlowIdById")
	public RetMsg<IfsCfetsrmbSl> searchCfetsrmbSlAndFlowIdById(@RequestBody Map<String, Object> key) {
		IfsCfetsrmbSl sl = cfetsrmbSlService.getSlAndFlowIdById(key);
		return RetMsgHelper.ok(sl);
	}

	/**
	 * 勾兑时，查询表中审批通过的数据
	 * 
	 * @param
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchPageApprovermbSlPassed")
	public RetMsg<PageInfo<IfsCfetsrmbSl>> searchPageApprovermbSlPassed(@RequestBody Map<String, Object> params) {
		Page<IfsCfetsrmbSl> page = cfetsrmbSlService.getApproveSlPassedPage(params);
		return RetMsgHelper.ok(page);
	}
	
	
	//债券类mini弹框
	@ResponseBody
	@RequestMapping(value = "/getMini")
	public RetMsg<PageInfo<Map<String, Object>>> searchMiniPage(@RequestBody Map<String, Object> params) {
		Page<Map<String, Object>> page = cfetsrmbSlService.getSlMini(params);
		return RetMsgHelper.ok(page);
		
	}
	
	
	//债券类mini弹框--债券发行
	@ResponseBody
	@RequestMapping(value = "/getDpMini")
	public RetMsg<PageInfo<Map<String, Object>>> searchDpMiniPage(@RequestBody Map<String, Object> params) {
		Page<Map<String, Object>> page = cfetsrmbSlService.getDpMini(params);
		return RetMsgHelper.ok(page);
		
	}
	
	
	
	//根据ticketId查询
	@ResponseBody
	@RequestMapping(value = "/searchCfetsrmbSlByTicketId")
	public RetMsg<IfsCfetsrmbSl> searchCfetsrmbSlByTicketId(@RequestBody Map<String, Object> map) {
		IfsCfetsrmbSl sl = cfetsrmbSlService.searchCfetsrmbSlByTicketId(map);
		return RetMsgHelper.ok(sl);
	}

	//质押比例
	@ResponseBody
	@RequestMapping(value = "/getMarginRatio")
	public RetMsg<BigDecimal> getMarginRatio(@RequestBody Map<String, Object> map) {
		BigDecimal marginRatio = cfetsrmbSlService.getMarginRatio(map);
		return RetMsgHelper.ok(marginRatio);
	}
}
