package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.ifs.model.IfsRevIotd;
import com.singlee.ifs.service.IfsRevIotdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.Map;

/**
 * 
 * 期权类Controller
 * @author rongliu
 * @date 2018-08-06
 * 
 */
@Controller
@RequestMapping(value = "/IfsRevIotdController")
public class IfsRevIotdController {
	
	@Autowired
	IfsRevIotdService ifsRevIotdService;
	
	/***
	 * 分页查询 期权类要素
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageOptParam")
	public RetMsg<PageInfo<IfsRevIotd>> searchPageOptParam(@RequestBody Map<String, Object> map) {
		Page<IfsRevIotd> page = ifsRevIotdService.searchPageOptParam(map);
		return RetMsgHelper.ok(page);
	}
	
	/***
	 * 删除 期权类要素
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteOptParam")
	public RetMsg<Serializable> deleteOptParam(@RequestBody Map<String, String> map) {
		String ticketId = map.get("ticketId");
		ifsRevIotdService.deleteById(ticketId);
		return RetMsgHelper.ok();
	}

	/**
	 * 保存 期权类要素
	 */
	@ResponseBody
	@RequestMapping(value = "/saveOptParam")
	public RetMsg<Serializable> saveOptParam(@RequestBody IfsRevIotd entity) {
		String ticketId = entity.getTicketId();
		if (null == ticketId || "".equals(ticketId)) {// 新增
			ifsRevIotdService.insert(entity);
		} else {// 修改
			ifsRevIotdService.updateById(entity);
		}
		return RetMsgHelper.ok();
	}
	
/***************************************************************************************/
	
	/**
	 * 查询  期权冲销     我发起的
	 * 
	 * @param params
	 * @return
	 * @author ym
	 * @date 2018-08-13
	 */
	@ResponseBody
	@RequestMapping(value = "/searchRevIotdMine")
	public RetMsg<PageInfo<IfsRevIotd>> searchRevItodMine(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsRevIotd> page = ifsRevIotdService.getRevIotdPage(params, 3);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 查询 外汇冲销       待审批
	 * 
	 * @param params
	 * @return
	 * @author ym
	 * @date 2018-08-13
	 */
	@ResponseBody
	@RequestMapping(value = "/searchRevIotdUnfinished")
	public RetMsg<PageInfo<IfsRevIotd>> searchRevIotdUnfinished(@RequestBody Map<String, Object> params) {
		String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse + ","
				+ DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
		String[] orderStatus = approveStatus.split(",");
		params.put("approveStatusNo", orderStatus);
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsRevIotd> page = ifsRevIotdService.getRevIotdPage(params, 1);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 查询 外汇冲销      已审批
	 * 
	 * @param params
	 * @return
	 * @author ym
	 * @date 2018-08-13
	 */
	@ResponseBody
	@RequestMapping(value = "/searchRevIotdFinished")
	public RetMsg<PageInfo<IfsRevIotd>> searchRevIotdFinished(@RequestBody Map<String, Object> params) {
		String approveStatus = ParameterUtil.getString(params, "approveStatus", "");
		if (StringUtil.isNullOrEmpty(approveStatus)) {
			approveStatus = DictConstants.ApproveStatus.ApprovedPass;
			approveStatus += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatus += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatus += "," + DictConstants.ApproveStatus.Verified;
			approveStatus += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			approveStatus += "," + DictConstants.ApproveStatus.Approving;

			String[] orderStatus = approveStatus.split(",");
			params.put("approveStatusNo", orderStatus);
		}
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsRevIotd> page = ifsRevIotdService.getRevIotdPage(params, 2);
		return RetMsgHelper.ok(page);
	}

}
