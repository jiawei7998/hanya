package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.ifs.model.IfsOpicsSpos;
import com.singlee.ifs.service.IfsOpicsSposService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author ：chenguo
 * @date ：Created in 2021/11/17 13:56
 * @description：
 * @modified By：
 * @version:
 */

@Controller
@RequestMapping(value = "IfsOpicsSposController")
public class IfsOpicsSposController {


    @Resource
    IfsOpicsSposService ifsOpicsSposService;

    @ResponseBody
    @RequestMapping(value = "getSposPage")
    public RetMsg<PageInfo<IfsOpicsSpos>> getSposPage(@RequestBody  Map<String,Object> map){
        Page page=ifsOpicsSposService.getSposPage(map);
        return RetMsgHelper.ok(page);
    }

}
