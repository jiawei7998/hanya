package com.singlee.ifs.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.singlee.capital.choistrd.model.RevaluationTmaxBean;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.financial.page.PageInfo;
import com.singlee.ifs.model.SlDerivativesStatus;
import com.singlee.ifs.service.SlDerivativesStatusService;

@Controller
@RequestMapping(value = "/SlDerivativesStatusController")
public class SlDerivativesStatusController {
	
	@Autowired
	SlDerivativesStatusService slDerivativesStatusService;
	
	/**
	 * 总账查询
	 */
	@ResponseBody
	@RequestMapping(value = "/slDerivativesStatusQuery")
	public RetMsg<PageInfo<RevaluationTmaxBean>> slDerivativesStatusQuery(@RequestBody Map<String, String> params) throws Exception {
		//PageInfo<SlDerivativesStatus> page = slDerivativesStatusService.querySlDerivativesStatus(params);
		
		PageInfo<RevaluationTmaxBean> page = slDerivativesStatusService.queryAllRevaluationProc(params);
		return RetMsgHelper.ok(page);
	}
}
