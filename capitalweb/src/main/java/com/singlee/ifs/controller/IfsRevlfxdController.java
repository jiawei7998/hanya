package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.ifs.model.IfsRevIfxd;
import com.singlee.ifs.service.IfsRevlfxdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.Map;

/**
 * 
 * 外汇类Controller
 * @author rongliu
 * @date 2018-07-24
 * 
 */
@Controller
@RequestMapping(value = "/IfsRevlfxdController")
public class IfsRevlfxdController {
	
	@Autowired
	IfsRevlfxdService ifsRevlfxdService;

	/***
	 * 分页查询 外汇类要素
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageForexParam")
	public RetMsg<PageInfo<IfsRevIfxd>> searchPageForexParam(@RequestBody Map<String, Object> map) {
		Page<IfsRevIfxd> page = ifsRevlfxdService.searchPageForexParam(map);
		return RetMsgHelper.ok(page);
	}

	/***
	 * 删除 外汇类要素
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteForexParam")
	public RetMsg<Serializable> deleteForexParam(@RequestBody Map<String, String> map) {
		String ticketId = map.get("ticketId");
		ifsRevlfxdService.deleteById(ticketId);
		return RetMsgHelper.ok();
	}

	/**
	 * 保存 外汇类要素
	 */
	@ResponseBody
	@RequestMapping(value = "/saveForexParam")
	public RetMsg<Serializable> saveForexParam(@RequestBody IfsRevIfxd entity) {
		String ticketId = entity.getTicketId();
		if (null == ticketId || "".equals(ticketId)) {// 新增
			ifsRevlfxdService.insert(entity);
		} else {// 修改
			ifsRevlfxdService.updateById(entity);
		}
		return RetMsgHelper.ok();
	}
	
	/***************************************************************************************/
	
	/**
	 * 查询  外汇冲销     我发起的
	 * 
	 * @param params
	 * @return
	 * @author lij
	 * @date 2018-08-10
	 */
	@ResponseBody
	@RequestMapping(value = "/searchRevIfxdMine")
	public RetMsg<PageInfo<IfsRevIfxd>> searchRevIfxdMine(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsRevIfxd> page = ifsRevlfxdService.getRevIfxdPage(params, 3);
		return RetMsgHelper.ok(page);
	}
	
	
	/**
	 * 查询 外汇冲销       待审批
	 * 
	 * @param params
	 * @return
	 * @author lij
	 * @date 2018-08-10
	 */
	@ResponseBody
	@RequestMapping(value = "/searchRevIfxdUnfinished")
	public RetMsg<PageInfo<IfsRevIfxd>> searchRevIfxdUnfinished(@RequestBody Map<String, Object> params) {
		String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse + ","
				+ DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
		String[] orderStatus = approveStatus.split(",");
		params.put("approveStatusNo", orderStatus);
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsRevIfxd> page = ifsRevlfxdService.getRevIfxdPage(params, 1);
		return RetMsgHelper.ok(page);
	}
	
	
	/**
	 * 查询 外汇冲销      已审批
	 * 
	 * @param params
	 * @return
	 * @author lij
	 * @date 2018-08-10
	 */
	@ResponseBody
	@RequestMapping(value = "/searchRevIfxdFinished")
	public RetMsg<PageInfo<IfsRevIfxd>> searchRevIfxdFinished(@RequestBody Map<String, Object> params) {
		String approveStatus = ParameterUtil.getString(params, "approveStatus", "");
		if (StringUtil.isNullOrEmpty(approveStatus)) {
			approveStatus = DictConstants.ApproveStatus.ApprovedPass;
			approveStatus += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatus += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatus += "," + DictConstants.ApproveStatus.Verified;
			approveStatus += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			approveStatus += "," + DictConstants.ApproveStatus.Approving;

			String[] orderStatus = approveStatus.split(",");
			params.put("approveStatusNo", orderStatus);
		}
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsRevIfxd> page = ifsRevlfxdService.getRevIfxdPage(params, 2);
		return RetMsgHelper.ok(page);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
