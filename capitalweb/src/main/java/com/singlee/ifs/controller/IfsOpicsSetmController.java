package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.financial.bean.SlDealModule;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.bean.SlSetmBean;
import com.singlee.financial.opics.IStaticServer;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.SlErrors;
import com.singlee.ifs.mapper.IfsOpicsSetmMapper;
import com.singlee.ifs.model.IfsOpicsSetm;
import com.singlee.ifs.service.IfsOpicsSetmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

/***
 * 清算方式
 * 
 * @author zero
 */

@Controller
@RequestMapping(value = "/IfsOpicsSetmController")
public class IfsOpicsSetmController {
	@Autowired
	IfsOpicsSetmService ifsOpicsSetmService;

	@Autowired
	IStaticServer iStaticServer;
	
	@Resource
	IfsOpicsSetmMapper ifsOpicsSetmMapper;

	/***
	 * 分页查询
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageSetm")
	public RetMsg<PageInfo<IfsOpicsSetm>> searchPageSetm(@RequestBody Map<String, Object> map) {
		Page<IfsOpicsSetm> page = ifsOpicsSetmService.searchPageOpicsSetm(map);
		return RetMsgHelper.ok(page);
	}

	/***
	 * 根据id查询实体类
	 */
	@ResponseBody
	@RequestMapping(value = "/searchSetmById")
	public RetMsg<IfsOpicsSetm> searchSetmById(@RequestBody Map<String, String> map) {
		String settlmeans = map.get("settlmeans");
		IfsOpicsSetm entity = new IfsOpicsSetm();
		entity.setSettlmeans(settlmeans);
		entity.setBr(SlDealModule.BASE.BR);
		IfsOpicsSetm ifsOpicsSetm = ifsOpicsSetmService.searchById(entity);
		return RetMsgHelper.ok(ifsOpicsSetm);
	}

	/**
	 * 新增
	 */
	@ResponseBody
	@RequestMapping(value = "/saveOpicsSetmAdd")
	public RetMsg<Serializable> saveOpicsSetmAdd(@RequestBody IfsOpicsSetm entity) {
		entity.setBr(SlDealModule.BASE.BR);
		ifsOpicsSetmService.insert(entity);
		return RetMsgHelper.ok();
	}

	/**
	 * 修改
	 */
	@ResponseBody
	@RequestMapping(value = "/saveOpicsSetmEdit")
	public RetMsg<Serializable> saveOpicsSetmEdit(@RequestBody IfsOpicsSetm entity) {
		ifsOpicsSetmService.updateById(entity);
		return RetMsgHelper.ok();
	}

	/**
	 * 删除
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteSetm")
	public RetMsg<Serializable> deleteSetm(@RequestBody IfsOpicsSetm entity) {
		ifsOpicsSetmService.deleteById(entity);
		return RetMsgHelper.ok();
	}

	/**
	 * 同步opics表
	 * 
	 * @author rongliu
	 */
	@ResponseBody
	@RequestMapping(value = "/syncSetm")
	public RetMsg<SlOutBean> syncSetm(@RequestBody List<IfsOpicsSetm> list) {
		SlOutBean slOutBean = new SlOutBean();

		for (IfsOpicsSetm entity : list) {
			IfsOpicsSetm ifsOpicsSetm = ifsOpicsSetmService.searchById(entity);
			SlSetmBean setmBean = new SlSetmBean();
			setmBean.setBr(ifsOpicsSetm.getBr());
			setmBean.setSettlmeans(ifsOpicsSetm.getSettlmeans());
			setmBean.setDescr(ifsOpicsSetm.getDescr());
			setmBean.setCheckdigit(ifsOpicsSetm.getCheckdigit());
			setmBean.setGenledgno(ifsOpicsSetm.getGenledgno());
			setmBean.setAcctval(ifsOpicsSetm.getAcctval());
			setmBean.setValacc(ifsOpicsSetm.getValacc());
			setmBean.setLstmntdte(ifsOpicsSetm.getLstmntdte());
			setmBean.setExtintflag(ifsOpicsSetm.getExtintflag());
			setmBean.setOffacct(ifsOpicsSetm.getOffacct());
			setmBean.setAutoauthorize(ifsOpicsSetm.getAutoauthorize());
			try {
				slOutBean = iStaticServer.addSetm(setmBean);
			} catch (RemoteConnectFailureException e) {
				slOutBean.setRetCode(SlErrors.FAILED_RMI_OPICS.getErrCode());
				slOutBean.setRetMsg(SlErrors.FAILED_RMI_OPICS.getErrMsg() + ":" + e.getMessage());
				slOutBean.setRetStatus(RetStatusEnum.F);
			} catch (Exception e) {
				slOutBean.setRetCode(SlErrors.FAILED.getErrCode());
				slOutBean.setRetMsg(SlErrors.FAILED.getErrMsg() + ":" + e.getMessage());
				slOutBean.setRetStatus(RetStatusEnum.F);
			}
			if ("S".equals(String.valueOf(slOutBean.getRetStatus()))) {
				ifsOpicsSetm.setStatus("1");
				ifsOpicsSetmService.updateStatus(ifsOpicsSetm);
			}
		}
		return RetMsgHelper.ok(slOutBean);
	}

	/**
	 * 批量校准
	 * 
	 * @author rongliu
	 */
	@ResponseBody
	@RequestMapping(value = "/batchCalibration")
	public RetMsg<SlOutBean> batchCalibration(@RequestBody List<IfsOpicsSetm> list) {
		SlOutBean slOutBean = ifsOpicsSetmService.batchCalibration(list);
		return RetMsgHelper.ok(slOutBean);
	}

	/**
	 * 查询已同步的清算方式
	 */
	@ResponseBody
	@RequestMapping(value = "/searchSettmeans")
	public List<IfsOpicsSetm> searchSettmeans() {
		List<IfsOpicsSetm> result = ifsOpicsSetmMapper.searchSettmeans();
		return result;
	}
}