package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.bean.SlDealModule;
import com.singlee.financial.bean.SlSposBean;
import com.singlee.financial.opics.IBaseServer;
import com.singlee.ifs.model.IfsCfetsrmbCr;
import com.singlee.ifs.model.IfsCfetsrmbCrKey;
import com.singlee.ifs.model.IfsCfetsrmbDetailCr;
import com.singlee.ifs.service.IFSService;
import com.singlee.ifs.service.IfsOpicsTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 质押式回购正式交易审批Controller
 * 
 * ClassName: IfsCfetsrmbCrController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason: TODO ADD REASON(可选). <br/>
 * date: 2018-5-30 下午07:25:42 <br/>
 * 
 * @author lijing
 * @version
 * @since JDK 1.6
 */
@Controller
@RequestMapping(value = "/IfsCfetsrmbCrController")
public class IfsCfetsrmbCrController extends CommonController {
	@Autowired
	private IFSService cfetsrmbCrService;
	@Autowired
	IBaseServer baseServer;
	@Autowired
	IfsOpicsTypeService opicsTypeService;

	/**
	 * 保存成交单
	 * 
	 * @param IfsCfetsrmbCr
	 * @throws Exception 
	 * @throws RemoteConnectFailureException 
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/saveCr")
	public RetMsg<Serializable> saveCr(@RequestBody IfsCfetsrmbCr record) throws RemoteConnectFailureException, Exception {
		//1.检查会计类型
		String message  = cfetsrmbCrService.checkCust(record.getDealTransType(),record.getReverseInst(),record.getCfetsno(), null, record.getProduct(), record.getProdType());
		if(StringUtil.isNotEmpty(message)) {//检查失败
			return RetMsgHelper.fail(message);
		}
//		String al=opicsTypeService.getAlByType(record.getProdType(),record.getProduct());
		if("S".equals(record.getMyDir())){//检查卖出债券是否存在短头寸
			List<IfsCfetsrmbDetailCr> bondDetailsList =record.getBondDetailsList();
			for (IfsCfetsrmbDetailCr ifsCfetsrmbDetailCr : bondDetailsList) {
				Map<String,Object> map =new HashMap<String, Object>();
				map.put("br", SlDealModule.BASE.BR);
				map.put("ccy", "CNY");
				map.put("port", ifsCfetsrmbDetailCr.getPortb());
				map.put("secid",ifsCfetsrmbDetailCr.getBondCode());
				map.put("invtype",ifsCfetsrmbDetailCr.getInvtype());
				map.put("cost",ifsCfetsrmbDetailCr.getCostb());
				SlSposBean sposBean=baseServer.getSpos(map);
				BigDecimal num;
				if(sposBean==null){
					num=new BigDecimal(0);
				}else{
					num=sposBean.getTotalamt();
				}
				BigDecimal totalFaceValue=ifsCfetsrmbDetailCr.getTotalFaceValue().multiply(new BigDecimal(10000));
				int a=totalFaceValue.compareTo(num);//-1 小于，0 等于 ， 1 大于
				if(a==1){//totalFaceValue 大于  num
					message=message+"债券"+ifsCfetsrmbDetailCr.getBondCode()+"短头寸<br>";
				}
			}
		}
		if(StringUtil.isNotEmpty(message)) {//检查失败
			return RetMsgHelper.fail(message);
		}else {//检查通过
			IfsCfetsrmbCr cr = cfetsrmbCrService.getCrById(record);
			if (cr == null) {
				message = cfetsrmbCrService.insertCr(record);
			} else {
				try {
					cfetsrmbCrService.updateCr(record);
					message = "修改成功";
				} catch (Exception e) {
					e.printStackTrace();
					message = "修改失败";
				}
			}
		}
		return StringUtil.containsIgnoreCase(message, "成功")?RetMsgHelper.ok(message):RetMsgHelper.fail(message);
	}
	//不校验短头寸
	@ResponseBody
	@RequestMapping(value = "/saveCrTrue")
	public RetMsg<Serializable> saveCrTrue(@RequestBody IfsCfetsrmbCr record) throws RemoteConnectFailureException, Exception {
		//1.检查会计类型
		String message  = cfetsrmbCrService.checkCust(record.getDealTransType(),record.getReverseInst(),record.getCfetsno(), null, record.getProduct(), record.getProdType());
		if(StringUtil.isNotEmpty(message)) {//检查失败
			return RetMsgHelper.fail(message);
		}
		IfsCfetsrmbCr cr = cfetsrmbCrService.getCrById(record);
		if (cr == null) {
			message = cfetsrmbCrService.insertCr(record);
		} else {
			try {
				cfetsrmbCrService.updateCr(record);
				message = "修改成功";
			} catch (Exception e) {
				e.printStackTrace();
				message = "修改失败";
			}
		}
		return StringUtil.containsIgnoreCase(message, "成功")?RetMsgHelper.ok(message):RetMsgHelper.fail(message);
	}
	/**
	 * 成交单分页查询
	 * 
	 * @param IfsCfetsrmbCr
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchCfetsrmbCr")
	public RetMsg<PageInfo<IfsCfetsrmbCr>> searchCfetsrmbCr(@RequestBody IfsCfetsrmbCr record) {
		Page<IfsCfetsrmbCr> page = cfetsrmbCrService.getCrList(record);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 根据主键查询成交单
	 * 
	 * @param IfsCfetsrmbCrKey
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchCfetsrmbCrById")
	public RetMsg<IfsCfetsrmbCr> searchCfetsrmbCrById(@RequestBody IfsCfetsrmbCrKey key) {
		IfsCfetsrmbCr cr = cfetsrmbCrService.getCrById(key);
		return RetMsgHelper.ok(cr);
	}

	/**
	 * 删除成交单
	 * 
	 * @param IfsCfetsrmbCrKey
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/deleteCfetsrmbCr")
	public RetMsg<Serializable> deleteCfetsrmbCr(@RequestBody IfsCfetsrmbCrKey key) {
		cfetsrmbCrService.deleteCr(key);
		return RetMsgHelper.ok();
	}
	/**
	 * 查询债券详情信息
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-7-18
	 */
	@ResponseBody
	@RequestMapping(value = "/searchBondCrDetails")
	public RetMsg<PageInfo<IfsCfetsrmbDetailCr>> searchBondCrDetails(@RequestBody Map<String, Object> params) {
		Page<IfsCfetsrmbDetailCr> page = cfetsrmbCrService.searchBondCrDetails(params);
		return RetMsgHelper.ok(page);
	}
	/**
	 * 查询我发起的
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-21
	 */
	@ResponseBody
	@RequestMapping(value = "/searchRmbCrSwapMine")
	public RetMsg<PageInfo<IfsCfetsrmbCr>> searchRmbCrSwapMine(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsrmbCr> page = cfetsrmbCrService.getRmbCrMinePage(params, 3);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 查询 待审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageRmbCrUnfinished")
	public RetMsg<PageInfo<IfsCfetsrmbCr>> searchPageRmbCrUnfinished(@RequestBody Map<String, Object> params) {
		String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse + ","
				+ DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
		String[] orderStatus = approveStatus.split(",");
		params.put("approveStatusNo", orderStatus);
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsrmbCr> page = cfetsrmbCrService.getRmbCrMinePage(params, 1);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 查询 已审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageRmbCrFinished")
	public RetMsg<PageInfo<IfsCfetsrmbCr>> searchPageRmbCrFinished(@RequestBody Map<String, Object> params) {
		String approveStatusNo = ParameterUtil.getString(params, "approveStatus", "");
		if (StringUtil.isNullOrEmpty(approveStatusNo)) {
			approveStatusNo = DictConstants.ApproveStatus.ApprovedPass;
			approveStatusNo += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatusNo += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatusNo += "," + DictConstants.ApproveStatus.Verified;
			approveStatusNo += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			// 20180502修改bug:已审批列表中增加"审批中"
			approveStatusNo += "," + DictConstants.ApproveStatus.Approving;
			String[] orderStatus = approveStatusNo.split(",");
			params.put("approveStatusNo", orderStatus);
		}
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsrmbCr> page = cfetsrmbCrService.getRmbCrMinePage(params, 2);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 根据主键查询成交单(返回值增加 流程id)
	 * 
	 * @param
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchCfetsrmbCrAndFlowIdById")
	public RetMsg<IfsCfetsrmbCr> searchCfetsrmbCrAndFlowIdById(@RequestBody Map<String, Object> key) {
		IfsCfetsrmbCr cr = cfetsrmbCrService.getCrAndFlowIdById(key);
		return RetMsgHelper.ok(cr);
	}

	/**
	 * 勾兑时，查询表中审批通过的数据
	 * 
	 * @param
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchPageApprovermbCrPassed")
	public RetMsg<PageInfo<IfsCfetsrmbCr>> searchPageApprovermbCrPassed(@RequestBody Map<String, Object> params) {
		Page<IfsCfetsrmbCr> page = cfetsrmbCrService.getApproveCrPassedPage(params);
		return RetMsgHelper.ok(page);
	}
	
	//回购类mini
	@ResponseBody
	@RequestMapping(value = "/getMini")
	public RetMsg<PageInfo<Map<String, Object>>> getBondMini(@RequestBody Map<String, Object> params) {
		Page<Map<String, Object>> page = cfetsrmbCrService.getBondMini(params);
		return RetMsgHelper.ok(page);
	}
	
	// 查询
	@ResponseBody
	@RequestMapping(value = "/searchCfetsrmbCrByTicketId")
	public RetMsg<IfsCfetsrmbCr> searchCfetsrmbCrByTicketId(@RequestBody Map<String, Object> map) {
		String ticketId = map.get("ticketId").toString();
		IfsCfetsrmbCr ifsCfetsrmbCr = cfetsrmbCrService.searchCfetsrmbCrByTicketId(ticketId);
		return RetMsgHelper.ok(ifsCfetsrmbCr);
	}
	
	//根据ticketId查询详情
	@ResponseBody
	@RequestMapping(value = "/searchCrDetailByTicketId")
	public RetMsg<IfsCfetsrmbCr> searchCrDetailByTicketId(@RequestBody Map<String, Object> map) {
		IfsCfetsrmbCr cr = cfetsrmbCrService.searchCrDetailByTicketId(map);
		return RetMsgHelper.ok(cr);
	}
}
