package com.singlee.ifs.controller;

import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.financial.bean.SlGentBean;
import com.singlee.financial.opics.ISlGentServer;
import com.singlee.financial.page.PageInfo;
import com.singlee.ifs.service.IfsOpicsActyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.Map;

/**
 * 映射关系
 */
@Controller
@RequestMapping(value = "/IfsGentController")
public class IfsGentController {

	@Autowired
	IfsOpicsActyService ifsOpicsActyService;
	
	@Autowired
	ISlGentServer iSlGentServer;
	
	/**
	 * 新增时保存
	 */
	@ResponseBody
	@RequestMapping(value = "/gentAdd")
	public RetMsg<Serializable> saveGentAdd(@RequestBody SlGentBean entity) {
		iSlGentServer.addSlGent(entity);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 修改时保存
	 */
	@ResponseBody
	@RequestMapping(value = "/gentEdit")
	public RetMsg<Serializable> saveGentEdit(@RequestBody SlGentBean entity) {
		iSlGentServer.updateById(entity);
		return RetMsgHelper.ok();
	}
	
	/***
	 * 页面初始化   分页查询   展示所有数据
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageGent")
	public RetMsg<PageInfo<SlGentBean>> searchPageGent(@RequestBody Map<String, Object> map) {
		PageInfo<SlGentBean> page = iSlGentServer.searchPageGent(map);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 删除
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteSlGent")
	public RetMsg<Serializable> deleteSlGent(@RequestBody SlGentBean entity) {
		iSlGentServer.deleteSlGent(entity);
		return RetMsgHelper.ok();
	}
	
	/***
	 * 根据主键查询是否已有数据
	 */
	@ResponseBody
	@RequestMapping(value = "/searchIfsOpicsGent")
	public RetMsg<SlGentBean> searchIfsOpicsGent(@RequestBody Map<String, String> map) {
		SlGentBean entity = iSlGentServer.searchIfsOpicsGent(map);
		return RetMsgHelper.ok(entity);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
//	/***
//	 * 查询全部
//	 */
//	@ResponseBody
//	@RequestMapping(value = "/searchAllOpicsActy")
//	public Page<IfsOpicsActy> searchAllOpicsActy() {
//		Page<IfsOpicsActy> actyList = ifsOpicsActyService.searchAllOpicsActy();
//		return actyList;
//	}
//
//	/**
//	 * 根据id查询实体类
//	 */
//	@ResponseBody
//	@RequestMapping(value = "/searchActyById")
//	public RetMsg<IfsOpicsActy> searchActyById(@RequestBody Map<String, String> map) {
//		String acctngtype = map.get("acctngtype");
//		IfsOpicsActy ifsOpicsActy = ifsOpicsActyService.searchById(acctngtype);
//		return RetMsgHelper.ok(ifsOpicsActy);
//	}
}