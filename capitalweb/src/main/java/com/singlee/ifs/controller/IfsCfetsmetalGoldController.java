package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.ifs.model.IfsCfetsmetalGold;
import com.singlee.ifs.model.IfsCfetsmetalGoldKey;
import com.singlee.ifs.service.IFSService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.io.Serializable;
import java.util.Map;

/**
 * 贵金属正式交易审批Controller
 * 
 * ClassName: IfsCfetsmetalGoldController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason: TODO ADD REASON(可选). <br/>
 * date: 2018-5-30 下午07:21:18 <br/>
 * 
 * @author huanggx
 * @version
 * @since JDK 1.6
 */
@Controller
@RequestMapping(value = "/IfsCfetsmetalGoldController")
public class IfsCfetsmetalGoldController extends CommonController {
	@Autowired
	private IFSService cfetsmetalGoldService;

	/**
	 * 保存成交单
	 * 
	 * @param IfsCfetsmetalGold
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/saveGold")
	public String saveGold(@RequestBody IfsCfetsmetalGold record) throws IOException {
		IfsCfetsmetalGold gold = cfetsmetalGoldService.getGoldById(record);
		String message = "";
		if (gold == null) {
			message = cfetsmetalGoldService.insertGold(record);
		} else {
			try {
				cfetsmetalGoldService.updateGold(record);
				message = "修改成功";
			} catch (Exception e) {
				message = "修改失败";
			}
		}
		return message;
	}

	/**
	 * 成交单分页查询
	 * 
	 * @param IfsCfetsmetalGold
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchCfetsmetalGold")
	public RetMsg<PageInfo<IfsCfetsmetalGold>> searchCfetsmetalGold(@RequestBody IfsCfetsmetalGold record) {
		Page<IfsCfetsmetalGold> page = cfetsmetalGoldService.getGoldList(record);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 根据主键查询成交单
	 * 
	 * @param IfsCfetsmetalGoldKey
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchCfetsmetalGoldById")
	public RetMsg<IfsCfetsmetalGold> searchCfetsmetalGoldById(@RequestBody IfsCfetsmetalGoldKey key) {
		IfsCfetsmetalGold gold = cfetsmetalGoldService.getGoldById(key);
		return RetMsgHelper.ok(gold);
	}

	/**
	 * 根据主键关联流程查询成交单
	 * 
	 * @param IfsCfetsmetalGoldKey
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchCfetsmetalGoldAndFlowById")
	public RetMsg<IfsCfetsmetalGold> searchCfetsmetalGoldAndFlowById(@RequestBody Map<String, Object> key) {
		IfsCfetsmetalGold gold = cfetsmetalGoldService.getGoldAndFlowById(key);
		return RetMsgHelper.ok(gold);
	}

	/**
	 * 删除成交单
	 * 
	 * @param IfsCfetsmetalGoldKey
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/deleteCfetsmetalGold")
	public RetMsg<Serializable> deleteCfetsmetalGold(@RequestBody IfsCfetsmetalGoldKey key) {
		cfetsmetalGoldService.deleteGold(key);
		return RetMsgHelper.ok();
	}

	/**
	 * 查询我发起的
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-21
	 */
	@ResponseBody
	@RequestMapping(value = "/searchMetalGoldSwapMine")
	public RetMsg<PageInfo<IfsCfetsmetalGold>> searchMetalGoldSwapMine(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsmetalGold> page = cfetsmetalGoldService.getMetalGoldMinePage(params, 3);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 查询 待审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageMetalGoldUnfinished")
	public RetMsg<PageInfo<IfsCfetsmetalGold>> searchPageMetalGoldUnfinished(@RequestBody Map<String, Object> params) {
		String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse + ","
				+ DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
		String[] orderStatus = approveStatus.split(",");
		params.put("approveStatusNo", orderStatus);
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsmetalGold> page = cfetsmetalGoldService.getMetalGoldMinePage(params, 1);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 查询 已审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageMetalGoldFinished")
	public RetMsg<PageInfo<IfsCfetsmetalGold>> searchPageMetalGoldFinished(@RequestBody Map<String, Object> params) {
		String approveStatusNo = ParameterUtil.getString(params, "approveStatus", "");
		if (StringUtil.isNullOrEmpty(approveStatusNo)) {
			approveStatusNo = DictConstants.ApproveStatus.ApprovedPass;
			approveStatusNo += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatusNo += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatusNo += "," + DictConstants.ApproveStatus.Verified;
			approveStatusNo += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			// 20180502修改bug:已审批列表中增加"审批中"
			approveStatusNo += "," + DictConstants.ApproveStatus.Approving;

			String[] orderStatus = approveStatusNo.split(",");
			params.put("approveStatusNo", orderStatus);
		}
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsmetalGold> page = cfetsmetalGoldService.getMetalGoldMinePage(params, 2);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 勾兑时，查询表中审批通过的数据
	 * 
	 * @param
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchPageApprovemetalGoldPassed")
	public RetMsg<PageInfo<IfsCfetsmetalGold>> searchPageApprovemetalGoldPassed(@RequestBody Map<String, Object> params) {
		Page<IfsCfetsmetalGold> page = cfetsmetalGoldService.getApprovemetalGoldPassedPage(params);
		return RetMsgHelper.ok(page);
	}
	
	// 查询
	@ResponseBody
	@RequestMapping(value = "/searchGold")
	public RetMsg<IfsCfetsmetalGold> searchGold(@RequestBody Map<String, String> map) {
		IfsCfetsmetalGold page = cfetsmetalGoldService.getGoldByIdAndType(map);
		return RetMsgHelper.ok(page);
	}
	
	// 查询
	@ResponseBody
	@RequestMapping(value = "/searchMetalGold")
	public RetMsg<IfsCfetsmetalGold> searchMetalGold(@RequestBody Map<String, Object> map) {
		IfsCfetsmetalGold gold = cfetsmetalGoldService.searchMetalGold(map);
		return RetMsgHelper.ok(gold);
	}
}
