package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.financial.bean.BtchQryRslt;
import com.singlee.financial.bean.ManualSettleDetail;
import com.singlee.financial.cdtc.CdtcManualSettleServer;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/***
 * 中债直联——指令查询
 * 
 */
@Controller
@RequestMapping(value = "/IfsCdtcInstrBatchController")
public class IfsCdtcInstrBatchController {
	@Autowired
	private CdtcManualSettleServer cdtcManualSettleServer;
	private static List<BtchQryRslt> instrBtchQry = new ArrayList<BtchQryRslt>();
	private static Date lastTime;
	static {
		// 暂无中债环境,模拟数据
		for (int i = 0; i < 20; i++) {
			BtchQryRslt b = new BtchQryRslt();
			initCtrct(b);
			instrBtchQry.add(b);
		}
	}

	/**
	 * 指令批量查询——列表
	 * 
	 */
	@ResponseBody
	@RequestMapping(value = "/cdtcInstrBatchQuery")
	public RetMsg<PageInfo<BtchQryRslt>> cdtcInstrBatchQuery(@RequestBody Map<String, Object> map) {
		long time = 0;
		if (lastTime != null) {
			time = (System.currentTimeMillis() - lastTime.getTime()) / 1000;
		}
		if (lastTime == null || time > 2*60) {
//			try {
//				cdtcManualSettleServer.getManualSettleListQuery(map);
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
			// 根据参数，对list进行分页
			RowBounds rb = ParameterUtil.getRowBounds(map);
			Page<BtchQryRslt> page = new Page<BtchQryRslt>();
			page.setTotal(instrBtchQry.size());
			page.setPages((int) Math.ceil(instrBtchQry.size() / (double) rb.getLimit()));
			int end = (rb.getOffset() + rb.getLimit()) > instrBtchQry.size() ? instrBtchQry.size()
					: (rb.getOffset() + rb.getLimit());
			page.addAll(instrBtchQry.subList(rb.getOffset(), end));
			lastTime = new Date();
			return RetMsgHelper.ok(page);
		} else {
			StringBuffer info = new StringBuffer("距离上一次查询不到两分钟，不支持再次查询。还需等待").append(120 - time).append("秒");
			RetMsg<PageInfo<BtchQryRslt>> retMsg = new RetMsg<PageInfo<BtchQryRslt>>("error", info.toString(),
					info.toString(), null);
			return retMsg;
		}
	}

	/**
	 * 指令查询——详情
	 * 
	 */
	@ResponseBody
	@RequestMapping(value = "/cdtcInstrDetail")
	public RetMsg<ManualSettleDetail> cdtcInstrDetail(@RequestBody Map<String, Object> map) {
//		Pair<SmtOutBean, ManualSettleDetail> manualSettleDetailQuery = null;
//		try {
//			manualSettleDetailQuery = cdtcManualSettleServer.getManualSettleDetailQuery(map);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		ManualSettleDetail right = manualSettleDetailQuery.getRight();
//		return RetMsgHelper.ok(right);
		// 模拟数据
		ManualSettleDetail mDetail = new ManualSettleDetail();
		getMDetail(mDetail);
		return RetMsgHelper.ok(mDetail);
	}

	private void getMDetail(ManualSettleDetail mDetail) {
		// 结算指令标识 Exact12NumericText
		mDetail.setInstrId(getRandomNumber(12));
		// 业务类型 BusinessTypeCode
		mDetail.setBizTp("BT" + String.format("%02d", new Random().nextInt(28)));
		// -----------------------
		mDetail.setBizTpcode(mDetail.getBizTp());
		// 处理结果状态 Max8Text
		mDetail.setTxRsltCd("BJ000000");
		// 指令处理状态 InstructionStatusCode
		mDetail.setInstrSts("IS" + String.format("%02d", new Random().nextInt(9)));
		// -----------------------
		mDetail.setInstrStscode(mDetail.getInstrSts());
		// 对手方确认标识
		mDetail.setCtrCnfrmInd("IC" + String.format("%02d", new Random().nextInt(3)));
		// -----------------------
		mDetail.setCtrCnfrmIndcode(mDetail.getCtrCnfrmIndcode());
		// 发令方确认标识 InstructionConfirmIndicatorCode
		mDetail.setOrgtrCnfrmInd("IC" + String.format("%02d", new Random().nextInt(3)));
		// -----------------------
		mDetail.setOrgtrCnfrmIndcode(mDetail.getOrgtrCnfrmInd());
		// 指令来源
		mDetail.setInstrOrgn("IO" + String.format("%02d", new Random().nextInt(5)));
		// -----------------------
		mDetail.setInstrOrgncode(mDetail.getInstrOrgn());
		// 业务标识号 Max20Text
		mDetail.setTxId(getRandomNumber(10));
		// 结算金额1（基本债券：净价金额）（质押式回购：首期金额）CommonCurrencyAndAmount
		mDetail.setVal1(getRandomNumber(17));
		// 结算金额2 （基本债券：全价金额）（质押式回购：到期金额）CommonCurrencyAndAmount
		mDetail.setVal2(getRandomNumber(17));
		// 交割日1 （基本债券：交割日期）（质押式回购：首期交割日期）ISODate YYYY-MM-DD
		mDetail.setDt1(new SimpleDateFormat("YYYY-MM-dd").format(new Date()));
		// 交割日2 （质押式回购：到期交割日期）ISODate YYYY-MM-DD
		mDetail.setDt2(new SimpleDateFormat("YYYY-MM-dd").format(new Date()));
		// 结算方式1 （基本债券：结算方式）（质押式回购：首期结算方式）BondSettlementType2Code
		mDetail.setSttlmTp1("ST" + String.format("%02d", new Random().nextInt(4)));
		// -----------------------
		mDetail.setSttlmTp1code(mDetail.getSttlmTp1());
		// 结算方式2 （质押式回购：到期结算方式）BondSettlementType2Code
		mDetail.setSttlmTp2("ST" + String.format("%02d", new Random().nextInt(4)));
		// -----------------------
		mDetail.setSttlmTp2code(mDetail.getSttlmTp2());
		// 收券方账户id （基本债券：买入方债券账号）（质押式回购：逆回购方账号）
		mDetail.setTakAcctId(getRandomNumber(9));
		// 收券方账户名称 （基本债券：买入方简称）（质押式回购：逆回购方简称）
		mDetail.setTakAcctNm(getRandomName(5));
		// 付券方账户id （基本债券：卖出方债券账号）（质押式回购：正回购方账号）
		mDetail.setGivAcctId(getRandomNumber(9));
		// 付券方账户名称 （基本债券：卖出方简称）（质押式回购：正回购方简称）
		mDetail.setGivAcctNm(getRandomName(5));
		// 操作员
		mDetail.setChckr(getRandomName(5));
		// 操作员
		mDetail.setOprtr(getRandomName(5));
		mDetail.setOprtr(getRandomName(5));
		//合同标识
		mDetail.setCtrctId(getRandomNumber(9));
		//合同处理状态
		mDetail.setCtrctSts("CS"+String.format("%02d", new Random().nextInt(15)));
		//合同冻结状态
		mDetail.setCtrctBlckSts("CB"+String.format("%02d", new Random().nextInt(2)));
		//合同失败原因
		mDetail.setCtrctFaildRsn(getRandomName(9));
		//合同更新时间
		mDetail.setCtrctUpdate("2020-12-22");
		//合同结果返回码 Max8Text
		mDetail.setCtrctQryRsltCd(getRandomNumber(8));
		//合同结果返回信息
		mDetail.setCtrctQryRsltInf(getRandomName(10));
		/***
		 * 以下是[质押式回购]特有
		 */
		//回购利率
		mDetail.setRate(getRandomNumber(8));
		//全面金额    
		mDetail.setAggtFaceAmt(getRandomNumber(8));
		/***
		 * 以下是[基本债券]特有
		 */
		//净价（百元面值）
		mDetail.setPric1(getRandomNumber(8));
		//全价（百元面值）
		mDetail.setPric2(getRandomNumber(8));
		//债券总额
		mDetail.setBdAmt(getRandomNumber(8));
		/***
		 * 以下是[买断式回购]特有
		 */
		//担保方式
		mDetail.setGrteTp("DT"+String.format("%02d", new Random().nextInt(2)));
		//逆回购方保证券
		mDetail.setNBdId(getRandomNumber(10));
		//逆回购方保证券名
		mDetail.setNBdShrtNm(getRandomName(10));
		//逆回购方保证券额
		mDetail.setNBdAmt(getRandomNumber(10));
		//正回购方保证券
		mDetail.setZBdId(getRandomNumber(10));
		//正回购方保证券名
		mDetail.setZBdShrtNm(getRandomName(10));
		//正回购方保证券额
		mDetail.setZBdAmt(getRandomNumber(10));
//		private List<CdtcZLSecurity> secList;//债券信息列表
	}

	// 模拟数据，初始化返回报文
	private static void initCtrct(BtchQryRslt btchQryRslt) {
		// 结算指令标识 Exact12NumericText
		btchQryRslt.setInstrId(getRandomNumber(12));
		// 结算合同标识 Exact9NumericText
		btchQryRslt.setCtrctId(getRandomNumber(9));
		// 付券方账户名称
		btchQryRslt.setGivAcctNm(getRandomNumber(5));
		;
		// 付券方账户id
		btchQryRslt.setGivAcctId(getRandomNumber(9));
		// 收券方账户名称
		btchQryRslt.setTakAcctNm(getRandomName(5));
		// 收券方账户id
		btchQryRslt.setTakAcctId(getRandomNumber(9));
		// 业务标识号 Max20Text
		btchQryRslt.setTxId(getRandomNumber(10));
		// 业务类型 BusinessTypeCode
		btchQryRslt.setBizTp("BT" + String.format("%02d", new Random().nextInt(28)));
		// 结算方式1 BondSettlementType2Code
		btchQryRslt.setSttlmTp1("ST" + String.format("%02d", new Random().nextInt(4)));
		// 结算方式2 BondSettlementType2Code
		btchQryRslt.setSttlmTp2("ST" + String.format("%02d", new Random().nextInt(4)));
		// 债券数目 Max5NumericText
		btchQryRslt.setBdCnt(Integer.valueOf(getRandomNumber(5)));
		// 债券总额 FaceCurrencyAndAmount
		btchQryRslt.setAggtFaceAmt(new BigDecimal(getRandomNumber(12)));
		// 结算金额1 CommonCurrencyAndAmount
		btchQryRslt.setVal1(new BigDecimal(getRandomNumber(17)));
		// 结算金额2
		btchQryRslt.setVal2(new BigDecimal(getRandomNumber(17)));
		// 交割日1 ISODate YYYY-MM-DD
		btchQryRslt.setDt1(new SimpleDateFormat("YYYY-MM-dd").format(new Date()));
		// 交割日2 ISODate YYYY-MM-DD
		btchQryRslt.setDt2(new SimpleDateFormat("YYYY-MM-dd").format(new Date()));
		// 指令处理状态 InstructionStatusCode
		btchQryRslt.setInstrSts("IS" + String.format("%02d", new Random().nextInt(9)));
		// 合同处理状态
		btchQryRslt.setCtrctSts("CS" + String.format("%02d", new Random().nextInt(15)));
		// 合同冻结状态 ContractBlockStatusCode
		btchQryRslt.setCtrctBlckSts("CB" + String.format("%02d", new Random().nextInt(2)));
		// 最新更新时间 ISODateTime YYYY-MM-DDThh:mm:ss.sss
		btchQryRslt.setLastUpdTm(new SimpleDateFormat("YYYY-MM-dd'T'hh:mm:ss.sss").format(new Date()));
		// 发令方确认标识 InstructionConfirmIndicatorCode
		btchQryRslt.setOrgtrCnfrmInd("IC" + String.format("%02d", new Random().nextInt(3)));
		// 对手方确认标识 InstructionConfirmIndicatorCode
		btchQryRslt.setCtrCnfrmInd("IC" + String.format("%02d", new Random().nextInt(3)));
		// 指令来源
		btchQryRslt.setInstrOrgn("IO" + String.format("%02d", new Random().nextInt(5)));

		// 清算时间
		btchQryRslt.setStdt(new SimpleDateFormat("YYYY-MM-dd'T'hh:mm:ss.sss").format(new Date()));
		// 经办人员
		btchQryRslt.setIOper(getRandomName(5));
		// 经办时间
		btchQryRslt.setITime(new Date());
		// 复核人员
		btchQryRslt.setVOper(getRandomName(5));
		// 复核时间
		btchQryRslt.setVTime(new Date());
		// 报文发送处理标识
		btchQryRslt.setADealWith(getRandomNumber(2));
		// 处理时间
		btchQryRslt.setADTime(new Date());
		// 撤销经办人
		btchQryRslt.setRIOper(getRandomName(5));
		// 撤销时间
		btchQryRslt.setRITime(new Date());
		// 撤销复核人员
		btchQryRslt.setRVOper(getRandomName(5));
		// 撤销复核时间
		btchQryRslt.setRVTime(new Date());
		// 撤销报文处理标识
		btchQryRslt.setRDealWith(getRandomNumber(2));
		// 撤销处理时间
		btchQryRslt.setRDTime(new Date());
		btchQryRslt.setCSBS001(getRandomName(3));
		btchQryRslt.setCSBS003(getRandomName(3));
	}

	public static String getRandomNumber(int len) {
		String source = "0123456789";
		Random r = new Random();
		StringBuilder rs = new StringBuilder();
		for (int j = 0; j < len; j++) {
			rs.append(source.charAt(r.nextInt(10)));
		}
		return rs.toString();
	}

	public static String getRandomName(int len) {
		int min = len, max = len;
		String name;
		char[] nameChar;
		// 名字最长为max个,最短为min个
		int nameLength = (int) (Math.random() * (max - min + 1)) + min;
		nameChar = new char[nameLength];
		// 生成大写首字母
		nameChar[0] = (char) (Math.random() * 26 + 65);
		for (int i = 1; i < nameLength; i++) {
			nameChar[i] = (char) (Math.random() * 26 + 97);
		}
		name = new String(nameChar);
		return name;
	}
}
