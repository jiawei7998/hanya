package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.github.pagehelper.StringUtil;
import com.singlee.capital.base.util.FileManage;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.UploadedFile;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.credit.util.ExcelUtil;
import com.singlee.capital.eu.service.EdCustManangeService;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.bean.*;
import com.singlee.financial.esb.tlcb.IFxscServer;
import com.singlee.financial.esb.tlcb.IQueryServer;
import com.singlee.financial.esb.tlcb.ITyzrServer;
import com.singlee.financial.opics.IAcupServer;
import com.singlee.financial.opics.IBaseServer;
import com.singlee.ifs.mapper.*;
import com.singlee.ifs.model.*;
import com.singlee.ifs.service.IfsLimitService;
import com.singlee.ifs.service.IfsOpicsBondService;
import jxl.Sheet;
import jxl.Workbook;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.LogManager;
import org.apache.poi.ss.usermodel.CellStyle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 限额处理Controller
 * 
 * ClassName: IfsLimitController <br/>
 * date: 2018-5-30 下午07:37:22 <br/>
 * 
 * @author yangfan
 * @version
 * @since JDK 1.6
 */
@Controller
@RequestMapping(value = "IfsLimitController")
public class IfsLimitController extends CommonController {
	@Autowired
	IfsLimitService ifsLimitService;

	@Autowired
	IFxscServer fxscServer;

	@Autowired
	ITyzrServer tyzrServer;

	@Autowired
	IfsLimitConditionMapper ifsLimitConditionMapper;

	@Autowired
	IQueryServer queryServer; // 对公客户查询

	@Autowired
	IAcupServer acupServer;

	// 注入流程监控表
	@Autowired
	IfsFlowMonitorMapper ifsFlowMonitorMapper;

	// 外币拆借
	@Autowired
	IfsCfetsfxLendMapper ifsCfetsfxLendMapper;

	// 现劵买卖
	@Autowired
	IfsCfetsrmbCbtMapper ifsCfetsrmbCbtMapper;

	// 质押式回购
	@Autowired
	IfsCfetsrmbCrMapper ifsCfetsrmbCrMapper;

	// 买断式回购
	@Autowired
	IfsCfetsrmbOrMapper ifsCfetsrmbOrMapper;

	// 人民币即期
	@Autowired
	IfsCfetsfxSptMapper ifsCfetsfxSptMapper;

	@Autowired
	IfsOpicsSettleManageMapper ifsOpicsSettleManageMapper;

	@Autowired
	private IfsLimitTemplateMapper ifslimittemplatemapper;

	@Autowired
	IfsCfetsfxSwapMapper ifsCfetsfxSwapMapper;

	@Autowired
	IBaseServer baseServer;

	@Autowired
	IfsOpicsBondMapper ifsOpicsBondMapper;

	@Autowired
	BondLimitTemplateMapper bondLimitTemplateMapper;

	@Autowired
	EdCustManangeService edCustManangeService;
	
	@Autowired
	IfsOpicsBondService ifsOpicsBondService;
	
	@Autowired
	IBaseServer iBaseServer;

	@ResponseBody
	@RequestMapping(value = "/searchPageLimit")
	public RetMsg<PageInfo<IfsLimitCondition>> searchPageLimit(@RequestBody Map<String, Object> map) {
		Page<IfsLimitCondition> page = ifsLimitService.searchPageLimit(map);
		return RetMsgHelper.ok(page);
	}

	/***
	 * 删除
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteLimit")
	public RetMsg<Serializable> deleteLimit(@RequestBody Map<String, String> map) {
		String id = map.get("id");
		ifsLimitService.deleteById(id);
		return RetMsgHelper.ok();
	}

	/**
	 * 新增
	 */
	@RequestMapping(value = "/LimitConditionAdd")
	@ResponseBody
	public RetMsg<Serializable> LimitConditionAdd(@RequestBody IfsLimitCondition entity) throws RException {
		ifsLimitService.insert(entity);
		return RetMsgHelper.ok();
	}

	/**
	 * 保存 修改
	 */
	@ResponseBody
	@RequestMapping(value = "/saveLimitConditionEdit")
	public RetMsg<Serializable> saveLimitConditionEdit(@RequestBody IfsLimitCondition entity) {
		String id = entity.getId();
		if (null == id || "".equals(id)) {
			return RetMsgHelper.simple("cNo-Error", "修改时请选择id!");
		}
		ifsLimitService.update(entity);
		return RetMsgHelper.ok();
	}

	@ResponseBody
	@RequestMapping(value = "/searchIfsLimitCondition")
	public RetMsg<IfsLimitCondition> searchIfsLimitCondition(@RequestBody Map<String, String> map) {
		String id = map.get("id");
		IfsLimitCondition entity = ifsLimitService.searchIfsLimitCondition(id);
		return RetMsgHelper.ok(entity);
	}

	// 根据 交易员和产品 查出结果
	@RequestMapping(value = "/searchResult")
	public RetMsg<List<Map<String, Object>>> searchResult(@RequestBody Map<String, String> map) {
		List<Map<String, Object>> list = ifsLimitService.searchResult(map);
		return RetMsgHelper.ok(list);
	}

	/***
	 * 
	 * searchTraderTotalAndAvlAmt:(这里用一句话描述这个方法的作用). <br/>
	 * //20180531新增：查询交易员的总授信额度、总可用额度
	 * 
	 * @author lij
	 * @param map
	 * @return
	 * @since JDK 1.6
	 */
	@ResponseBody
	@RequestMapping(value = "/searchTraderTotalAndAvlAmt")
	RetMsg<PageInfo<List<Map<String, Object>>>> searchTraderTotalAndAvlAmt(@RequestBody Map<String, Object> map) {
		Page<List<Map<String, Object>>> page = ifsLimitService.searchTraderTotalAndAvlAmt(map);
		return RetMsgHelper.ok(page);
	}

	/***
	 * 
	 * searchTraderEdu:(这里用一句话描述这个方法的作用). <br/>
	 * 查询交易员的分授信额度、分可用额度
	 * 
	 * @author lij
	 * @param map
	 * @return
	 * @since JDK 1.6
	 */
	@ResponseBody
	@RequestMapping(value = "/searchTraderEdu")
	RetMsg<PageInfo<IfsLimitCondition>> searchTraderEdu(@RequestBody Map<String, Object> map) {
		Page<IfsLimitCondition> page = ifsLimitService.searchTraderEdu(map);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 新增
	 */
	@RequestMapping(value = "/LimitTemplateAdd")
	@ResponseBody
	public RetMsg<Serializable> LimitTemplateAdd(@RequestBody Map<String, Object> map) throws RException {
		map.put("operateUser", SlSessionHelper.getUserId());
		map.put("itime", DateUtil.getCurrentDateAsString());
		map.put("totalAmount", "0");
		ifsLimitService.insertLimit(map);
		return RetMsgHelper.ok();
	}

	/**
	 * 查询外汇限额信息
	 * 
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchTemplateLimit")
	public RetMsg<PageInfo<ifsLimitTemplate>> searchTemplateLimit(@RequestBody Map<String, Object> map) {
		Page<ifsLimitTemplate> page = ifsLimitService.searchTemplateLimit(map);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 保存 修改
	 */
	@ResponseBody
	@RequestMapping(value = "/LimitTemplateEdit")
	public RetMsg<Serializable> LimitTemplateEdit(@RequestBody Map<String, Object> map) {
		map.put("operateUser", SlSessionHelper.getUserId());
		map.put("itime", DateUtil.getCurrentDateAsString());
		ifsLimitService.updateLimit(map);
		return RetMsgHelper.ok();
	}

	/***
	 * 删除
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteIfsLimit")
	public RetMsg<Serializable> deleteIfsLimit(@RequestBody Map<String, Object> map) {
		ifsLimitService.deleteIfsLimit(map);
		return RetMsgHelper.ok();
	}

	/**
	 * 保存债券和存单发行限额
	 */
	@ResponseBody
	@RequestMapping(value = "/insertBond")
	public RetMsg<Serializable> insertBond(@RequestBody Map<String, Object> map) {
		map.put("operateUser", SlSessionHelper.getUserId());
		map.put("itime", DateUtil.getCurrentDateAsString());
		map.put("totalAmoumt", "0");
		ifsLimitService.insertBond(map);
		return RetMsgHelper.ok();
	}

	/**
	 * 查询债券和存单发行限额信息
	 * 
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchTemplateBond")
	public RetMsg<PageInfo<BondTemplate>> searchTemplateBond(@RequestBody Map<String, Object> map) {
		map.put("startVdate", ParameterUtil.getString(map, "startVdate", "").substring(0, 10));
		Page<BondTemplate> page = ifsLimitService.searchTemplateBond(map);
		return RetMsgHelper.ok(page);
	}

	/***
	 * 删除
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteIfsBond")
	public RetMsg<Serializable> deleteIfsBond(@RequestBody Map<String, Object> map) {
		ifsLimitService.deleteIfsBond(map);
		return RetMsgHelper.ok();
	}

	/**
	 * 保存 修改
	 */
	@ResponseBody
	@RequestMapping(value = "/BondTemplateEdit")
	public RetMsg<Serializable> BondTemplateEdit(@RequestBody Map<String, Object> map) {
		map.put("operateUser", SlSessionHelper.getUserId());
		map.put("itime", DateUtil.getCurrentDateAsString());
		ifsLimitService.updateBond(map);
		return RetMsgHelper.ok();
	}

	@ResponseBody
	@RequestMapping(value = "/searchCostList")
	public RetMsg<List<IfsOpicsCost>> searchCostList(@RequestBody Map<String, Object> params) {
		return RetMsgHelper.ok(ifsLimitService.searchCostList(params));
	}

	/***
	 * 分页查询 信用风险
	 */
	@ResponseBody
	@RequestMapping(value = "/searchAllProd")
	public RetMsg<PageInfo<PdAttrInfoArray>> searchAllProd(@RequestBody Map<String, Object> map) {
		Page<PdAttrInfoArray> page = ifsLimitService.searchAllProd(map);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 调用同业信用风险审查接口查询产品属性信息
	 */
	@ResponseBody
	@RequestMapping(value = "/searchProdInfo")
	public RetMsg<List<Map<String, Object>>> searchProdInfo(@RequestBody Map<String, String> map) {
		map.put("userName", SlSessionHelper.getUserId()); // 传入当前登录用户
		SlIfsBean ifsBean = fxscServer.TY004(map);
		List<PdAttrInfoArray> list = ifsBean.getPD_ATTR_INFO_ARRAY();
		List<Map<String, Object>> listToMap = new ArrayList<Map<String, Object>>();
		Map<String, Object> mapAdd = new HashMap<String, Object>();
		String yearRate = "";
		String approveFlNo = "";
		NumberFormat nf = NumberFormat.getPercentInstance();
		if (list != null && "000000".equals(ifsBean.getRetCode())) { // 交易成功
			for (int i = 0; i < list.size(); i++) {
				String orderId = list.get(i).getORDER_ID();
				String tranType = list.get(i).getTRAN_TYPE();
				String tranStatus = list.get(i).getTRAN_STATUS();
				String inputNo = list.get(i).getINPT_TELLER_NO();
				String prodName = list.get(i).getPRODUCT_NAME();
				yearRate = list.get(i).getYLD_RATE();
				String investPlan = list.get(i).getIVTNT_PLN();
				String expDate = list.get(i).getPD_EXP_DT();
				approveFlNo = list.get(i).getAPPROVE_FL_NO();
				if (yearRate != null && yearRate.contains("%")) {
					try {
						Number m = nf.parse(yearRate);
						yearRate = m.toString();
					} catch (ParseException e) {
						e.printStackTrace();
					}
				}
				mapAdd.put("oRDER_ID", orderId);
				mapAdd.put("tRAN_TYPE", tranType);
				mapAdd.put("tRAN_STATUS", tranStatus);
				mapAdd.put("iNPT_TELLER_NO", inputNo);
				mapAdd.put("pRODUCT_NAME", prodName);
				mapAdd.put("yLD_RATE", yearRate);
				mapAdd.put("iVTNT_PLN", investPlan);
				mapAdd.put("pD_EXP_DT", expDate);
				mapAdd.put("aPPROVE_FL_NO", approveFlNo);
				listToMap.add(mapAdd);
			}
			List<PdAttrInfoArray> applist = ifsLimitService
					.searchProd(approveFlNo);
			if (applist.size() == 0) { // 数据不同则插入
				ifsLimitService.insertCredit(mapAdd); // 交易成功后将每次查询出的不同的数据插入到表中
			}
		}
		mapAdd.put("retCode", ifsBean.getRetCode());
		mapAdd.put("retMsg", ifsBean.getRetMsg());
		listToMap.add(mapAdd);
		return RetMsgHelper.ok(listToMap);
	}

	/***
	 * 分页查询 同业准入
	 */
	@ResponseBody
	@RequestMapping(value = "/searchAllBuss")
	public RetMsg<PageInfo<BussVrtyInfoArray>> searchAllBuss(
			@RequestBody Map<String, Object> map) {
		Page<BussVrtyInfoArray> page = ifsLimitService.searchAllBuss(map);
		return RetMsgHelper.ok(page);
	}

	/***
	 * 分页查询 同业准入历史明细
	 */
	@ResponseBody
	@RequestMapping(value = "/searchLocalDetail")
	public RetMsg<PageInfo<IfsTradeAccess>> searchLocalDetail(
			@RequestBody Map<String, Object> map) {
		Page<IfsTradeAccess> page = ifsLimitService.searchLocalDetail(map);
		return RetMsgHelper.ok(page);
	}

	/***
	 * 分页查询 同业准入(黑名单)
	 */
	@ResponseBody
	@RequestMapping(value = "/searchAllBussBk")
	public RetMsg<PageInfo<BussVrtyInfoArray>> searchAllBussBk(
			@RequestBody Map<String, Object> map) {
		Page<BussVrtyInfoArray> page = ifsLimitService.searchAllBussBk(map);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 调用同业机构准入信息查询接口查询同业金融机构信息
	 */
	@ResponseBody
	@RequestMapping(value = "/searchBussInfo")
	public RetMsg<List<Map<String, Object>>> searchBussInfo(
			@RequestBody Map<String, String> map) {
		map.put("userName", SlSessionHelper.getUserId()); // 传入当前登录用户
		SlIfsBean ifsBean = tyzrServer.TY005(map);

		List<BussVrtyInfoArray> list = ifsBean.getBUSS_VRTY_INFO_ARRAY();
		List<Map<String, Object>> listToMap = new ArrayList<Map<String, Object>>();
		Map<String, Object> mapAdd = new HashMap<String, Object>();
		String nowDate = ifsOpicsSettleManageMapper.getCurDate();
		if (list != null && "000000".equals(ifsBean.getRetCode())) { // 交易成功
			for (int i = 0; i < list.size(); i++) {
				String bussType = list.get(i).getBUSS_TYPE(); // 业务类型
				mapAdd.put("bUSS_TYPE", mapping(bussType).get("type"));
				mapAdd.put("bUSS_TP_NM", mapping(bussType).get("name"));
				String coreClntNo = ifsBean.getCORE_CLNT_NO();
				String coNm = ifsBean.getCO_NM();
				String branchCode = ifsBean.getBRANCH_CODE();
				String branchName = ifsBean.getBRANCH_NAME();
				String inclSubFlg = ifsBean.getINCL_SUB_FLG();
				Date appDate = ifsBean.getAPPROVAL_DATE();
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				String date = sdf.format(appDate);
				mapAdd.put("cORE_CLNT_NO", coreClntNo);
				if(coNm.contains(".")){
					mapAdd.put("cO_NM", coNm.substring(coNm.indexOf(".") + 1, coNm.length()));
				} else {
					mapAdd.put("cO_NM", coNm);
				}
				mapAdd.put("bRANCH_CODE", branchCode);
				mapAdd.put("bRANCH_NAME", branchName);
				mapAdd.put("iNCL_SUB_FLG", inclSubFlg);
				mapAdd.put("aPPROVAL_DATE", date);
				if(mapping(bussType).get("type") != null && !"".equals(mapping(bussType).get("type"))){
					/* 根据客户号和业务类型名称查出来有相同的数据，则删除本地的 */
					ifslimittemplatemapper.deleteByEcifNo(mapAdd);
					/* 删除本地数据后，再插入同业的数据 */
					ifsLimitService.insertBussType(mapAdd); // 交易成功后将每次查询出的不同的数据插入到表中
				}
				listToMap.add(mapAdd);
			}
		}
		mapAdd.put("operator", SlSessionHelper.getUserId());
		mapAdd.put("oprTime", nowDate);
		mapAdd.put("oprType", "新增");
		mapAdd.put("retCode", ifsBean.getRetCode());
		mapAdd.put("retMsg", ifsBean.getRetMsg());
		listToMap.add(mapAdd);
		return RetMsgHelper.ok(listToMap);
	}
	
	/**
	 * 信贷产品编码映射OPICS编码
	 */
	public Map<String,Object> mapping(String bussType) {
		Map<String,Object> map = new HashMap<String,Object>();
		if("0202100".equals(bussType) || "0202101".equals(bussType)){ // 分销买入/卖出
			map.put("type", "443");
			map.put("name", "现券买卖");
		} else if("0122100".equals(bussType)){ // 同业拆出
			map.put("type", "444");
			map.put("name", "信用拆借");
		} else if("1301000".equals(bussType) || "1301001".equals(bussType)){ // 外汇即期买入/卖出
			map.put("type", "411");
			map.put("name", "外汇即期");
		} else if("0123101".equals(bussType) || "0123100".equals(bussType)){ // 债券质押式正回购/逆回购
			map.put("type", "446");
			map.put("name", "质押式回购");
		} else if("0220101".equals(bussType) || "0220100".equals(bussType)){ // 债券买断式正回购/逆回购
			map.put("type", "442");
			map.put("name", "买断式回购");
		} else if("0156101".equals(bussType)){ // 系统内拆借
			map.put("type", "438");
			map.put("name", "外币拆借");
		} else if("0000021".equals(bussType)){ // 债券发行
			map.put("type", "451");
			map.put("name", "外币债");
		} else if("1401100".equals(bussType) || "1401101".equals(bussType)){ // 外汇远期买入/卖出
			map.put("type", "391");
			map.put("name", "外汇远期");
		} else if("1501100".equals(bussType)){ // 外汇掉期买/卖首期
			map.put("type", "432");
			map.put("name", "外汇掉期");
		} else if("1701001".equals(bussType) || "1701000".equals(bussType)){ // 外汇期权买入/卖出
			map.put("type", "433");
			map.put("name", "外汇期权");
		} else if("0151101".equals(bussType)){ // 结构性存款
			map.put("type", "400");
			map.put("name", "结构衍生");
		} else if("0500100".equals(bussType) || "0500101".equals(bussType)){ // 支付浮动/固定
			map.put("type", "441");
			map.put("name", "利率互换");
		} else if("2000400".equals(bussType)){ // 债券借贷置换
			map.put("type", "445");
			map.put("name", "债券借贷");
		}
		return map;
	}

	/**
	 * 客户准入 查询本地数据
	 */
	@ResponseBody
	@RequestMapping(value = "/searchLocalInfo")
	public RetMsg<PageInfo<SlIfsBean>> searchLocalInfo(@RequestBody Map<String, String> map) {
		map.put("pageIndex", "1");
		map.put("pageSize", "10");
		Page<SlIfsBean> page = ifsLimitService.searchLocalInfo(map);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 客户准入 查询本地数据(黑名单)
	 */
	@ResponseBody
	@RequestMapping(value = "/searchLocalInfoBk")
	public List<SlIfsBean> searchLocalInfoBk(
			@RequestBody Map<String, String> map) {
		List<SlIfsBean> list = ifsLimitService.searchLocalInfoBk(map);
		return list;
	}

	/**
	 * 客户准入 新增时保存
	 */
	@ResponseBody
	@RequestMapping(value = "/bussInfoAdd")
	public RetMsg<Serializable> bussInfoAdd(@RequestBody Map<String, Object> map) {
		Map<String, Object> upmap = new HashMap<String, Object>();
		String coreno = map.get("cORE_CLNT_NO").toString();
		upmap.put("cORE_CLNT_NO", coreno);
		upmap.put("cO_NM", map.get("cO_NM"));
		upmap.put("bRANCH_CODE", map.get("bRANCH_CODE"));
		upmap.put("bRANCH_NAME", map.get("bRANCH_NAME"));
		upmap.put("iNCL_SUB_FLG", map.get("iNCL_SUB_FLG"));
		upmap.put("aPPROVAL_DATE", map.get("aPPROVAL_DATE"));
		String tp = map.get("bUSS_TYPE").toString();
		String tpNm = map.get("bUSS_TP_NM").toString();
		String[] type = tp.split(",");
		String[] typeNm = tpNm.split(",");
		String nowDate = ifsOpicsSettleManageMapper.getCurDate();
		for (int i = 0; i < type.length; i++) {
			upmap.put("coreno", coreno);
			upmap.put("bUSS_TYPE", type[i]);
			upmap.put("bUSS_TP_NM", typeNm[i]);
			List<SlIfsBean> list = ifsLimitService.queryBussInfo(upmap);
			if (list.size() == 0) { // 若无此条数据，则插入
				upmap.put("operator", SlSessionHelper.getUserId());
				upmap.put("oprTime", nowDate);
				upmap.put("oprType", "新增");
				ifsLimitService.insertBussType(upmap);
			}
		}
		return RetMsgHelper.ok();
	}

	/**
	 * 客户准入 新增时保存(黑名单)
	 */
	@ResponseBody
	@RequestMapping(value = "/bussInfoAddBk")
	public RetMsg<Serializable> bussInfoAddBk(
			@RequestBody Map<String, Object> map) {
		Map<String, Object> upmap = new HashMap<String, Object>();
		String coreno = map.get("cORE_CLNT_NO").toString();
		upmap.put("cORE_CLNT_NO", coreno);
		upmap.put("cO_NM", map.get("cO_NM"));
		upmap.put("bRANCH_CODE", map.get("bRANCH_CODE"));
		upmap.put("bRANCH_NAME", map.get("bRANCH_NAME"));
		upmap.put("iNCL_SUB_FLG", map.get("iNCL_SUB_FLG"));
		upmap.put("aPPROVAL_DATE", map.get("aPPROVAL_DATE"));
		String tp = map.get("bUSS_TYPE").toString();
		String tpNm = map.get("bUSS_TP_NM").toString();
		String[] type = tp.split(",");
		String[] typeNm = tpNm.split(",");
		for (int i = 0; i < type.length; i++) {
			upmap.put("coreno", coreno);
			upmap.put("bUSS_TYPE", type[i]);
			upmap.put("bUSS_TP_NM", typeNm[i]);
			List<SlIfsBean> list = ifsLimitService.queryBussInfoBk(upmap);
			if (list.size() == 0) { // 若无此条数据，则插入
				ifsLimitService.insertBussTypeBk(upmap);
			}
		}
		return RetMsgHelper.ok();
	}

	/**
	 * 客户准入 修改时保存
	 */
	@ResponseBody
	@RequestMapping(value = "/bussInfoEdit")
	public RetMsg<Serializable> bussInfoEdit(@RequestBody Map<String, Object> map) {
		String nowDate = ifsOpicsSettleManageMapper.getCurDate();
		map.put("operator", SlSessionHelper.getUserId());
		map.put("oprTime", nowDate);
		map.put("oprType", "修改");

		/* 查业务类型编号 */
		Map<String, Object> upmap = new HashMap<String, Object>();
		upmap.put("coreno", map.get("cORE_CLNT_NO"));
		upmap.put("bUSS_TP_NM", map.get("bUSS_TP_NM"));
		List<SlIfsBean> list = ifsLimitService.queryBussInfo(upmap);

		map.put("bUSS_TYPE", list.get(0).getBUSS_TYPE());
		ifsLimitService.updateByEcifNo(map);
		return RetMsgHelper.ok();
	}

	/**
	 * 客户准入 修改时保存(黑名单)
	 */
	@ResponseBody
	@RequestMapping(value = "/bussInfoEditBk")
	public RetMsg<Serializable> bussInfoEditBk(@RequestBody Map<String, Object> map) {
		ifsLimitService.updateByEcifNoBk(map);
		return RetMsgHelper.ok();
	}

	/**
	 * 客户准入 删除
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteBussInfo")
	public RetMsg<Serializable> deleteBussInfo(@RequestBody Map<String, Object> map) {
		String nowDate = ifsOpicsSettleManageMapper.getCurDate();
		map.put("operator", SlSessionHelper.getUserId());
		map.put("oprTime", nowDate);
		map.put("oprType", "删除");
		ifsLimitService.deleteByEcifNo(map);
		return RetMsgHelper.ok();
	}

	/**
	 * 客户准入 删除(黑名单)
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteBussInfoBk")
	public RetMsg<Serializable> deleteBussInfoBk(@RequestBody Map<String, Object> map) {
		ifsLimitService.deleteByEcifNoBk(map);
		return RetMsgHelper.ok();
	}

	/**
	 * 客户准入 Excel上传准入客户信息
	 */
	@ResponseBody
	@RequestMapping(value = "/importBussByExcel", method = RequestMethod.POST, produces = "text/plain;charset=UTF-8")
	public String importBussByExcel(MultipartHttpServletRequest request,HttpServletResponse response) {
		// 1.文件获取
		List<UploadedFile> uploadedFileList = null;
		String ret = "导入失败";
		Workbook wb = null;
		List<IfsTradeAccess> slIfsBean = new ArrayList<IfsTradeAccess>();
		try {
			uploadedFileList = FileManage.requestExtractor(request);
			// 2.校验并处理成excel
			List<Workbook> excelList = new LinkedList<Workbook>();
			for (UploadedFile uploadedFile : uploadedFileList) {
				if (!"xls".equals(uploadedFile.getType())) {
					return ret = "上传文件类型错误,仅允许上传xls格式文件";
				}
				Workbook book = Workbook.getWorkbook(new ByteArrayInputStream(
						uploadedFile.getBytes()));
				excelList.add(book);
			}
			if (excelList.size() != 1) {
				return ret = "只能处理一份excel";
			}
			wb = excelList.get(0);
			Sheet[] sheets = wb.getSheets();// 获得卡片sheet数量
			for (Sheet sheet : sheets) {
				int rowCount = sheet.getRows();// 行数
				IfsTradeAccess ifsOpicsBond = null;
				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
				String bussTpNm = "";
				
				for (int i = 1; i < rowCount; i++) { // rows=1开始 第二行开始读取，第一行为列名
					bussTpNm = StringUtils.trimToEmpty(sheet.getCell(2, i)
							.getContents());
					ifsOpicsBond = new IfsTradeAccess();
					ifsOpicsBond.setCoreClntNo(StringUtils.trimToEmpty(sheet
							.getCell(0, i).getContents()));// A
					ifsOpicsBond.setConm(StringUtils.trimToEmpty(sheet
							.getCell(1, i).getContents()));// B
					ifsOpicsBond.setBussTpnm(bussTpNm);// C
					ifsOpicsBond.setBussType(StringUtils.trimToEmpty(sheet.getCell(3, i)
                            .getContents()));// D
					ifsOpicsBond.setDealType(StringUtils.trimToEmpty(sheet.getCell(4, i)
                            .getContents()));// D
					ifsOpicsBond.setBranchName(StringUtils.trimToEmpty(sheet
							.getCell(5, i).getContents()));// D
					String date = StringUtils.trimToEmpty(
							sheet.getCell(6, i).getContents());
					ifsOpicsBond.setApprovalDate(sdf.parse(date));// E
					/* 将业务类型编码插入到表中 */
					ifsOpicsBond.setBussType(ifslimittemplatemapper
							.queryPrdNo(bussTpNm));
					slIfsBean.add(ifsOpicsBond);
				}
			}
			ret = ifsLimitService.doBussExcelPropertyToDataBase(slIfsBean);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RException(e);
		} finally {
			if (null != wb) {
                wb.close();
            }
		}
		return ret;// 返回前台展示配置
	}

	/**
	 * 风险审查 新增时保存
	 */
	@ResponseBody
	@RequestMapping(value = "/singleLimitAdd")
	public RetMsg<Serializable> singleLimitAdd(@RequestBody Map<String, Object> map) {
		Map<String, Object> upmap = new HashMap<String, Object>();
		upmap.put("aPPROVE_FL_NO", map.get("aPPROVE_FL_NO"));
		upmap.put("oRDER_ID", map.get("oRDER_ID"));
		upmap.put("tRAN_TYPE", map.get("tRAN_TYPE"));
		upmap.put("tRAN_STATUS", map.get("tRAN_STATUS"));
		upmap.put("iNPT_TELLER_NO", map.get("iNPT_TELLER_NO"));
		upmap.put("pRODUCT_NAME", map.get("pRODUCT_NAME"));
		upmap.put("yLD_RATE", map.get("yLD_RATE"));
		upmap.put("iVTNT_PLN", map.get("iVTNT_PLN"));
		upmap.put("pD_EXP_DT", map.get("pD_EXP_DT"));
		ifsLimitService.insertCredit(upmap);
		return RetMsgHelper.ok();
	}

	/**
	 * 风险审查 修改时保存
	 */
	@ResponseBody
	@RequestMapping(value = "/singleLimitEdit")
	public RetMsg<Serializable> singleLimitEdit(
			@RequestBody Map<String, Object> map) {
		ifsLimitService.updateByAppNo(map);
		return RetMsgHelper.ok();
	}

	/**
	 * 风险审查 删除
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteSingleLimit")
	public RetMsg<Serializable> deleteSingleLimit(@RequestBody Map<String, Object> map) {
		ifsLimitService.deleteByAppNo(map);
		return RetMsgHelper.ok();
	}

	/**
	 * 风险审查 Excel上传同业客户信息
	 */
	@ResponseBody
	@RequestMapping(value = "/importSingleByExcel", method = RequestMethod.POST, produces = "text/plain;charset=UTF-8")
	public String importSingleByExcel(MultipartHttpServletRequest request,HttpServletResponse response) {
		// 1.文件获取
		List<UploadedFile> uploadedFileList = null;
		String ret = "导入失败";
		Workbook wb = null;
		List<PdAttrInfoArray> list = new ArrayList<PdAttrInfoArray>();
		try {
			uploadedFileList = FileManage.requestExtractor(request);
			// 2.校验并处理成excel
			List<Workbook> excelList = new LinkedList<Workbook>();
			for (UploadedFile uploadedFile : uploadedFileList) {
				if (!"xls".equals(uploadedFile.getType())) {
					return ret = "上传文件类型错误,仅允许上传xls格式文件";
				}
				Workbook book = Workbook.getWorkbook(new ByteArrayInputStream(
						uploadedFile.getBytes()));
				excelList.add(book);
			}
			if (excelList.size() != 1) {
				return ret = "只能处理一份excel";
			}
			wb = excelList.get(0);
			Sheet[] sheets = wb.getSheets();// 获得卡片sheet数量
			for (Sheet sheet : sheets) {
				int rowCount = sheet.getRows();// 行数
				PdAttrInfoArray pdAttrInfo = null;
				for (int i = 1; i < rowCount; i++) { // rows=1开始 第二行开始读取，第一行为列名
					pdAttrInfo = new PdAttrInfoArray();
					pdAttrInfo.setAPPROVE_FL_NO(StringUtils.trimToEmpty(sheet
							.getCell(0, i).getContents()));// A
					pdAttrInfo.setORDER_ID(StringUtils.trimToEmpty(sheet
							.getCell(1, i).getContents()));// B
					pdAttrInfo.setTRAN_TYPE(StringUtils.trimToEmpty(sheet
							.getCell(2, i).getContents()));// C
					pdAttrInfo.setTRAN_STATUS(StringUtils.trimToEmpty(sheet
							.getCell(3, i).getContents()));// D
					pdAttrInfo.setINPT_TELLER_NO(StringUtils.trimToEmpty(sheet
							.getCell(4, i).getContents()));// E
					pdAttrInfo.setPRODUCT_NAME(StringUtils.trimToEmpty(sheet
							.getCell(5, i).getContents()));// F
					String rate = StringUtils.trimToEmpty(sheet.getCell(6, i)
							.getContents());
					pdAttrInfo.setYLD_RATE(rate);// G
					pdAttrInfo.setIVTNT_PLN(StringUtils.trimToEmpty(sheet
							.getCell(7, i).getContents()));// H
					String date = StringUtils.trimToEmpty(
							sheet.getCell(8, i).getContents())
							.replace("/", "-");
					pdAttrInfo.setPD_EXP_DT(date);// I
					list.add(pdAttrInfo);
				}
			}
			ret = ifsLimitService.doSIngleExcelPropertyToDataBase(list);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RException(e);
		} finally {
			if (null != wb) {
                wb.close();
            }
		}
		return ret; // 返回前台展示配置
	}

	/***
	 * 风险审查 查询单笔
	 */
	@ResponseBody
	@RequestMapping(value = "/searchProd")
	public String searchProd(@RequestBody String approveFlNo) {
		String flag = "1"; // 表中有此编号
		List<PdAttrInfoArray> list = ifsLimitService.searchProd(approveFlNo);
		if (list.size() == 0) { // 表中无此编号
			return flag = "0";
		}
		return flag;
	}

	/***
	 * 客户准入 查询单笔
	 */
	@ResponseBody
	@RequestMapping(value = "/searchBuss")
	public Map<String, Object> searchBuss(@RequestBody String ecifno) {
		Map<String, Object> map = new HashMap<String, Object>();
		/*
		 * String flag = "1"; // 表中有此客户号 List<BussVrtyInfoArray> list =
		 * ifsLimitService.searchBuss(ecifno); if (list.size() == 0) { //
		 * 表中无此客户号 flag = "0"; } map.put("flag", flag);
		 */
		/*
		 * 根据客户号查询客户名称
		 */
		SlCustBean slCustBean = new SlCustBean("01", SlDealModule.CUST.SERVER,
				SlDealModule.CUST.TAG, SlDealModule.CUST.DETAIL);
		slCustBean.setEcifNo(ecifno);
		slCustBean.setCa5(SlSessionHelper.getUserId());
		SlCustBean result = queryServer.ecif0014(slCustBean);
		String name = result.getCliname();
		map.put("name", name);
		return map;
	}

	/**
	 * 客户准入 导出Excel
	 */
	@ResponseBody
	@RequestMapping(value = "/exportExcel")
	public void exportExcel(HttpServletRequest request,
			HttpServletResponse response) {
		String ua = request.getHeader("User-Agent");
		String filename = "客户准入信息.xlsx"; // 文件名
		String encode_filename = "";
		try {
			encode_filename = URLEncoder.encode(filename, "utf-8");
		} catch (UnsupportedEncodingException e1) {
			throw new RException(e1.getMessage());
		}
		response.setContentType("application/vnd.ms-excel");
		if (ua != null && ua.indexOf("Firefox/") >= 0) {
			response.setHeader("Content-Disposition", "attachment; filename*="
					+ encode_filename);
		} else {
			response.setHeader("Content-Disposition", "attachment; filename="
					+ encode_filename);
		}
		response.setHeader("Connection", "close");
		try {
			ExcelUtil e = downloadExcel();
			OutputStream ouputStream = response.getOutputStream();
			e.getWb().write(ouputStream);
			ouputStream.flush();
			ouputStream.close();
		} catch (Exception e) {
			throw new RException(e);
		}
	};

	/**
	 * 客户准入 Excel文件下载内容
	 */
	public ExcelUtil downloadExcel() throws Exception {
		SlIfsBean bean = new SlIfsBean();
		List<SlIfsBean> list = ifsLimitService.searchAllData(); // 查数据
		ExcelUtil e = null;
		try {
			// 表头
			e = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
			CellStyle center = e.getDefaultCenterStrCellStyle();
			int sheet = 0;
			int row = 0;
			// 表格sheet名称
			e.getWb().createSheet("客户准入表");
			// 设置表头字段名
			e.writeStr(sheet, row, "A", center, "核心客户号");
			e.writeStr(sheet, row, "B", center, "客户名称");
			e.writeStr(sheet, row, "C", center, "业务类型名称");
			e.writeStr(sheet, row, "D", center, "机构名称");
			e.writeStr(sheet, row, "E", center, "包含下级标志");
			e.writeStr(sheet, row, "F", center, "审批日期");
			// 设置列宽
			e.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
			for (int i = 0; i < list.size(); i++) {
				bean = list.get(i);
				row++;
				// 插入数据
				e.writeStr(sheet, row, "A", center, bean.getCORE_CLNT_NO());
				e.writeStr(sheet, row, "B", center, bean.getCO_NM());
				e.writeStr(sheet, row, "C", center, bean.getBUSS_TP_NM());
				e.writeStr(sheet, row, "D", center, bean.getBRANCH_NAME());
				e.writeStr(sheet, row, "E", center, bean.getINCL_SUB_FLG());
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				String date = sdf.format(bean.getAPPROVAL_DATE());
				e.writeStr(sheet, row, "F", center, date);
			}
			return e;
		} catch (Exception e1) {
			throw new RException(e1);
		}
	}

	/**
	 * 风险审查 查询本地数据
	 */
	@ResponseBody
	@RequestMapping(value = "/searchLocalData")
	public List<PdAttrInfoArray> searchLocalData(
			@RequestBody Map<String, String> map) {
		List<PdAttrInfoArray> list = ifsLimitService.searchLocalData(map);
		return list;
	}

	/**
	 * 风险审查 导出Excel
	 */
	@ResponseBody
	@RequestMapping(value = "/exportExcelData")
	public void exportExcelData(HttpServletRequest request,
			HttpServletResponse response) {
		String ua = request.getHeader("User-Agent");
		String filename = "风险审查信息.xlsx"; // 文件名
		String encode_filename = "";
		try {
			encode_filename = URLEncoder.encode(filename, "utf-8");
		} catch (UnsupportedEncodingException e1) {
			throw new RException(e1.getMessage());
		}
		response.setContentType("application/vnd.ms-excel");
		if (ua != null && ua.indexOf("Firefox/") >= 0) {
			response.setHeader("Content-Disposition", "attachment; filename*="
					+ encode_filename);
		} else {
			response.setHeader("Content-Disposition", "attachment; filename="
					+ encode_filename);
		}
		response.setHeader("Connection", "close");
		try {
			ExcelUtil e = downloadExcelData();
			OutputStream ouputStream = response.getOutputStream();
			e.getWb().write(ouputStream);
			ouputStream.flush();
			ouputStream.close();
		} catch (Exception e) {
			throw new RException(e);
		}
	};

	/**
	 * 风险审查 Excel文件下载内容
	 */
	public ExcelUtil downloadExcelData() throws Exception {
		PdAttrInfoArray bean = new PdAttrInfoArray();
		List<PdAttrInfoArray> list = ifsLimitService.searchAllDataRisk(); // 查数据
		ExcelUtil e = null;
		try {
			// 表头
			e = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
			CellStyle center = e.getDefaultCenterStrCellStyle();
			int sheet = 0;
			int row = 0;
			// 表格sheet名称
			e.getWb().createSheet("客户准入表");
			// 设置表头字段名
			e.writeStr(sheet, row, "A", center, "审批件编号");
			e.writeStr(sheet, row, "B", center, "审批单号");
			e.writeStr(sheet, row, "C", center, "交易类型");
			e.writeStr(sheet, row, "D", center, "交易状态");
			e.writeStr(sheet, row, "E", center, "经办员");
			e.writeStr(sheet, row, "F", center, "产品名称");
			e.writeStr(sheet, row, "G", center, "年化收益率");
			e.writeStr(sheet, row, "H", center, "投资计划");
			e.writeStr(sheet, row, "I", center, "有效期截止日");
			// 设置列宽
			e.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
			for (int i = 0; i < list.size(); i++) {
				bean = list.get(i);
				row++;
				// 插入数据
				e.writeStr(sheet, row, "A", center, bean.getAPPROVE_FL_NO());
				e.writeStr(sheet, row, "B", center, bean.getORDER_ID());
				e.writeStr(sheet, row, "C", center, bean.getTrantp());
				e.writeStr(sheet, row, "D", center, bean.getTranst());
				e.writeStr(sheet, row, "E", center, bean.getINPT_TELLER_NO());
				e.writeStr(sheet, row, "F", center, bean.getPRODUCT_NAME());
				e.writeStr(sheet, row, "G", center, bean.getYLD_RATE()
						.toString());
				e.writeStr(sheet, row, "H", center, bean.getIVTNT_PLN());
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				String date = sdf.format(bean.getPD_EXP_DT());
				e.writeStr(sheet, row, "I", center, date);
			}
			return e;
		} catch (Exception e1) {
			throw new RException(e1);
		}
	}

	/***
	 * 查询汇率
	 */
	@ResponseBody
	@RequestMapping(value = "/searchRate")
	public String searchRate() {
		Map<String, String> mapRate = new HashMap<String, String>();
		mapRate.put("ccy", "USD");
		mapRate.put("br", "01");// 机构号，送01
		BigDecimal dayRate = acupServer.queryRate(mapRate);
		if (dayRate == null || "".equals(dayRate)) {
			JY.raise("美元汇率未找到");
		}
		return dayRate + "";
	}

	/**
	 * 计算（价格异常检测偏离度使用）
	 * 
	 */
	@ResponseBody
	@RequestMapping(value = "/queryRhisIntrate")
	public String queryRhisIntrate(@RequestBody Map<String, String> map) {
		String result = edCustManangeService.queryRhisIntrate(map);
		return result;
	}

	@ResponseBody
	@RequestMapping(value = "/searchLimitDetail")
	public RetMsg<PageInfo<IfsLimitTemplateDetail>> getTdEdCustDetailPage(
			@RequestBody Map<String, Object> map) {
		Page<IfsLimitTemplateDetail> list = ifsLimitService
				.searchAllTemplateDetail(map);
		return RetMsgHelper.ok(list);
	}

	@ResponseBody
	@RequestMapping(value = "/searchAllBondDetail")
	public RetMsg<PageInfo<IfsLimitBondDetail>> searchAllBondDetail(
			@RequestBody Map<String, Object> map) {
		map.put("loadTime", ParameterUtil.getString(map, "loadTime", "")
				.substring(0, 10));
		Page<IfsLimitBondDetail> list = ifsLimitService
				.searchAllBondDetail(map);
		return RetMsgHelper.ok(list);
	}

	// 给定一个日期型字符串，返回加减n天后的日期型字符串
	public String nDaysAfterOneDateString(String basicDate, int n) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date tmpDate = null;
		try {
			tmpDate = df.parse(basicDate);
		} catch (Exception e) {
			// 日期型字符串格式错误
		}
		long nDay = (tmpDate.getTime() / (24 * 60 * 60 * 1000) + 1 + n)
				* (24 * 60 * 60 * 1000);
		tmpDate.setTime(nDay);
		return df.format(tmpDate);
	}

	/**
	 * 获取估值损益
	 */
	@ResponseBody
	@RequestMapping(value = "/getLossLimit")
	public String getLossLimit(@RequestBody Map<String, String> map) {
		String effDate = ifsOpicsSettleManageMapper.getCurDate();
		String dealno = ParameterUtil.getString(map, "dealno", "");
		if ("".equals(dealno) || "null".equals(dealno) || null == dealno) {// 为空时opics转为0去查询，返回
			dealno = "0";
		}
		map.put("dealno", dealno);
		effDate = nDaysAfterOneDateString(effDate, -1);// 获取上一日日期
		map.put("postdate", effDate);
		BigDecimal mapValueLast = acupServer.queryValue(map);
		return mapValueLast + "";
	}

	/**
	 * 获取估值损益
	 */
	@ResponseBody
	@RequestMapping(value = "/getLossLimitForWap")
	public String getLossLimitForWap(@RequestBody Map<String, String> map) {
		String effDate = ifsOpicsSettleManageMapper.getCurDate();
		String dealno = ParameterUtil.getString(map, "dealno", "");
		String serialNo = ParameterUtil.getString(map, "ticketId", "");
		if ("".equals(dealno) || "null".equals(dealno) || null == dealno) {// 为空时opics转为0去查询，返回
			dealno = "0";
		}
		map.put("dealno", dealno);
		effDate = nDaysAfterOneDateString(effDate, -1);// 获取上一日日期
		map.put("postdate", effDate);
		BigDecimal mapValueLast = acupServer.queryValue(map);// 获取近端的估值

		IfsCfetsfxSwap ifsCfetsfxSwap = new IfsCfetsfxSwap();
		ifsCfetsfxSwap = ifsCfetsfxSwapMapper.getrmswap(serialNo);// 人命币掉期获取远端opics账号
		String dealno2 = ifsCfetsfxSwap.getFarDealNo();// 远端opics编号
		if ("".equals(dealno2) || dealno2 == null) {
			dealno2 = Integer.parseInt(dealno) + 1 + "";// 远端交易的opics账号，获取近端交易的opicsNo加1
		}

		map.put("dealno", dealno2);
		BigDecimal mapValueLast2 = acupServer.queryValue(map);// 获取近端的估值
		mapValueLast = mapValueLast.add(mapValueLast2);// 估值为近端值加上远端的估值
		return mapValueLast + "";
	}

	/**
	 * 获取报表
	 * 
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getBondInvestmentLimit")
	public RetMsg<List<BondLimitReportBean>> getBondInvestmentLimit(@RequestBody Map<String, Object> map) {
		map.put("bondFlag", "1");
		String oprDate = ParameterUtil.getString(map, "oprDate", "");
		if ("".equals(oprDate) || null == oprDate) {
			oprDate = ifsOpicsSettleManageMapper.getCurDate();
		}
		map.put("oprDate", oprDate.substring(0, 10));
		Page<InvestLimtlog> list = ifsLimitService.getBondInvestmentLimit(map);
		
		BondLimitReportBean bean = new BondLimitReportBean();
		List<BondLimitReportBean> beanList = new ArrayList<BondLimitReportBean>();
		bean.setZjxe11("0.00");
		bean.setZjxe12("0.00");
		bean.setZjxe13("0.00");
		bean.setZjxe14("0.00");
		bean.setZjxe15("0.00");
		bean.setZjxe16("0.00");
		bean.setZjxe17("0.00");
		bean.setZjxe21("0.00");
		bean.setZjxe22("0.00");
		bean.setZjxe23("0.00");
		bean.setZjxe24("0.00");
		bean.setZjxe25("0.00");
		bean.setZjxe26("0.00");
		bean.setZjxe27("0.00");
		bean.setZjxe31("0.00");
		bean.setZjxe32("0.00");
		bean.setZjxe33("0.00");
		bean.setZjxe34("0.00");
		for(int i=0;i<list.size();i++){
			String invtype = list.get(i).getInvtype();
			DecimalFormat df = new DecimalFormat("#0.00");//科学计数法转正常数据
			if("A".equals(invtype)){
				bean.setZjxe21(list.get(i).getInvestLimit()==null?"0":df.format(Double.parseDouble(list.get(i).getInvestLimit())));
				bean.setZjxe22(list.get(i).getCreditInvestLimit()==null?"0":df.format(Double.parseDouble(list.get(i).getCreditInvestLimit())));
				bean.setZjxe23(list.get(i).getLowLevelLimit()==null?"0":df.format(Double.parseDouble(list.get(i).getLowLevelLimit())));
				bean.setZjxe24(list.get(i).getDuration()==null?"0":df.format(Double.parseDouble(list.get(i).getDuration())));
				bean.setZjxe25(list.get(i).getDv01()==null?"0":df.format(Double.parseDouble(list.get(i).getDv01())));
				bean.setZjxe26(list.get(i).getConvexityYear()==null?"0":df.format(Double.parseDouble(list.get(i).getConvexityYear())));
				bean.setZjxe27(list.get(i).getConvexityMonth()==null?"0":df.format(Double.parseDouble(list.get(i).getConvexityMonth())));
			}else if("T".equals(invtype)){
				bean.setZjxe11(list.get(i).getInvestLimit()==null?"0":df.format(Double.parseDouble(list.get(i).getInvestLimit())));
				bean.setZjxe12(list.get(i).getCreditInvestLimit()==null?"0":df.format(Double.parseDouble(list.get(i).getCreditInvestLimit())));
				bean.setZjxe13(list.get(i).getLowLevelLimit()==null?"0":df.format(Double.parseDouble(list.get(i).getLowLevelLimit())));
				bean.setZjxe14(list.get(i).getDuration()==null?"0":df.format(Double.parseDouble(list.get(i).getDuration())));
				bean.setZjxe15(list.get(i).getDv01()==null?"0":df.format(Double.parseDouble(list.get(i).getDv01())));
				bean.setZjxe16(list.get(i).getConvexityYear()==null?"0":df.format(Double.parseDouble(list.get(i).getConvexityYear())));
				bean.setZjxe17(list.get(i).getConvexityMonth()==null?"0":df.format(Double.parseDouble(list.get(i).getConvexityMonth())));
			}else if("H".equals(invtype)){
				bean.setZjxe31(list.get(i).getInvestLimit()==null?"0":df.format(Double.parseDouble(list.get(i).getInvestLimit())));
				bean.setZjxe32(list.get(i).getCreditInvestLimit()==null?"0":df.format(Double.parseDouble(list.get(i).getCreditInvestLimit())));
				bean.setZjxe33(list.get(i).getLowLevelLimit()==null?"0":df.format(Double.parseDouble(list.get(i).getLowLevelLimit())));
				bean.setZjxe34(list.get(i).getDuration()==null?"0":df.format(Double.parseDouble(list.get(i).getDuration())));
			}
		}
		beanList.add(bean);
		
		return RetMsgHelper.ok(beanList);
	}


	/**
	 * 获取存单报表
	 * 
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getCdInvestmentLimit")
	public RetMsg<List<BondLimitReportBean>> getCdInvestmentLimit(@RequestBody Map<String, Object> map) {
		map.put("bondFlag", "2");
		String oprDate = ParameterUtil.getString(map, "oprDate", "");
		if ("".equals(oprDate) || null == oprDate) {
			oprDate = ifsOpicsSettleManageMapper.getCurDate();
		}
		map.put("oprDate", oprDate.substring(0, 10));
		Page<InvestLimtlog> list = ifsLimitService.getBondInvestmentLimit(map);
		
		BondLimitReportBean bean = new BondLimitReportBean();
		List<BondLimitReportBean> beanList = new ArrayList<BondLimitReportBean>();
		bean.setCdxe11("0.00");
		bean.setCdxe12("0.00");
		bean.setCdxe13("0.00");
		bean.setCdxe14("0.00");
		bean.setCdxe15("0.00");
		bean.setCdxe16("0.00");
		bean.setCdxe17("0.00");
		bean.setCdxe21("0.00");
		bean.setCdxe22("0.00");
		bean.setCdxe23("0.00");
		bean.setCdxe24("0.00");
		bean.setCdxe25("0.00");
		bean.setCdxe26("0.00");
		bean.setCdxe27("0.00");
		bean.setCdxe31("0.00");
		bean.setCdxe32("0.00");
		bean.setCdxe33("0.00");
		bean.setCdxe34("0.00");
		for(int i=0;i<list.size();i++){
			String invtype = list.get(i).getInvtype();
			DecimalFormat df = new DecimalFormat("#0.00");//科学计数法转正常数据
			if("A".equals(invtype)){
				bean.setCdxe21(list.get(i).getInvestLimit()==null?"0":df.format(Double.parseDouble(list.get(i).getInvestLimit())));
				bean.setCdxe22(list.get(i).getCreditInvestLimit()==null?"0":df.format(Double.parseDouble(list.get(i).getCreditInvestLimit())));
				bean.setCdxe23(list.get(i).getLowLevelLimit()==null?"0":df.format(Double.parseDouble(list.get(i).getLowLevelLimit())));
				bean.setCdxe24(list.get(i).getDuration()==null?"0":df.format(Double.parseDouble(list.get(i).getDuration())));
				bean.setCdxe25(list.get(i).getDv01()==null?"0":df.format(Double.parseDouble(list.get(i).getDv01())));
				bean.setCdxe26(list.get(i).getConvexityYear()==null?"0":df.format(Double.parseDouble(list.get(i).getConvexityYear())));
				bean.setCdxe27(list.get(i).getConvexityMonth()==null?"0":df.format(Double.parseDouble(list.get(i).getConvexityMonth())));
			}else if("T".equals(invtype)){
				bean.setCdxe11(list.get(i).getInvestLimit()==null?"0":df.format(Double.parseDouble(list.get(i).getInvestLimit())));
				bean.setCdxe12(list.get(i).getCreditInvestLimit()==null?"0":df.format(Double.parseDouble(list.get(i).getCreditInvestLimit())));
				bean.setCdxe13(list.get(i).getLowLevelLimit()==null?"0":df.format(Double.parseDouble(list.get(i).getLowLevelLimit())));
				bean.setCdxe14(list.get(i).getDuration()==null?"0":df.format(Double.parseDouble(list.get(i).getDuration())));
				bean.setCdxe15(list.get(i).getDv01()==null?"0":df.format(Double.parseDouble(list.get(i).getDv01())));
				bean.setCdxe16(list.get(i).getConvexityYear()==null?"0":df.format(Double.parseDouble(list.get(i).getConvexityYear())));
				bean.setCdxe17(list.get(i).getConvexityMonth()==null?"0":df.format(Double.parseDouble(list.get(i).getConvexityMonth())));
			}else if("H".equals(invtype)){
				bean.setCdxe31(list.get(i).getInvestLimit()==null?"0":df.format(Double.parseDouble(list.get(i).getInvestLimit())));
				bean.setCdxe32(list.get(i).getCreditInvestLimit()==null?"0":df.format(Double.parseDouble(list.get(i).getCreditInvestLimit())));
				bean.setCdxe33(list.get(i).getLowLevelLimit()==null?"0":df.format(Double.parseDouble(list.get(i).getLowLevelLimit())));
				bean.setCdxe34(list.get(i).getDuration()==null?"0":df.format(Double.parseDouble(list.get(i).getDuration())));
			}
		}
		beanList.add(bean);
		
		return RetMsgHelper.ok(beanList);
	}

	/**
	 * 获取外币报表
	 * 
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getForeignLimit")
	public RetMsg<PageInfo<ForeignLimitLog>> getForeignLimit(
			@RequestBody Map<String, Object> map) {
		String oprDate = ParameterUtil.getString(map, "oprDate", "");
		if ("".equals(oprDate) || null == oprDate) {
			oprDate = ifsOpicsSettleManageMapper.getCurDate();
		}
		map.put("oprDate", oprDate.substring(0, 10));
		Page<ForeignLimitLog> list = ifsLimitService.getForeignLimit(map);
		return RetMsgHelper.ok(list);
	}

	/**
	 * 更新债券存单限额
	 * 
	 * @throws Exception
	 * @throws RemoteConnectFailureException
	 */
	@SuppressWarnings("deprecation")
	@ResponseBody
	@RequestMapping(value = "/updateBondLimitDetail")
	public void updateBondLimitDetail() throws RemoteConnectFailureException,Exception {
		String nowDate = ifsOpicsSettleManageMapper.getCurDate();// 获取今日日期
		
		Map<String,String> brpsMap=new HashMap<String, String>();
		brpsMap.put("br", "01");
		String lastDate=acupServer.getAcupDate(brpsMap);	// 获取上一个跑批日期

		// opics中获取存量债券信息
		Map<String, Object> opicsMap = new HashMap<String, Object>();
		// opicsMap.put("postdate",nowDate);
		opicsMap.put("postdate", lastDate);
		List<SlBondPositionBalanceBean> list = baseServer.getBondPositionBalanceExport(opicsMap);

		// 删除当日明细
		bondLimitTemplateMapper.deleteIntoBondDetail(nowDate);
		for (SlBondPositionBalanceBean slBondPositionBalanceBean : list) {

			try {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("today", nowDate);// 当前日期
				map.put("lastDate", lastDate);// 昨日日期

				if (StringUtil.isNotEmpty(slBondPositionBalanceBean.getFacevalue())) {// 券面金额
					map.put("amt", slBondPositionBalanceBean.getFacevalue());
				}
				if (StringUtil.isNotEmpty(slBondPositionBalanceBean.getInvtype())) {// 账户类型H，T，A
					map.put("invtype", slBondPositionBalanceBean.getInvtype());
				}
				if (StringUtil.isNotEmpty(slBondPositionBalanceBean.getSecid())) {// 债券id
					map.put("bondId", slBondPositionBalanceBean.getSecid().trim());
					map.put("bondCode", slBondPositionBalanceBean.getSecid().trim());

					// 获取，产品product，产品类型prodtype，债券类型bondproperties,券到期日期matdt
					List<IfsOpicsBond> bondList = new ArrayList<IfsOpicsBond>();
					bondList = ifsOpicsBondMapper.duedateBondListQry(map);
					for (int i = 0; i < bondList.size(); i++) {
						map.put("prd", bondList.get(i).getProduct());// 产品
						map.put("prdType", bondList.get(i).getProdtype());// 产品类型
						map.put("bondProperties", bondList.get(i).getBondproperties());// 债券类型
						map.put("matdt", bondList.get(i).getMatdt());// 券到期日
					}
				}

				// 获取限额条件
				List<BondTemplate> bondTemplate = bondLimitTemplateMapper.queryOccupyBond(map);// 查询债券
				for (int i = 0; i < bondTemplate.size(); i++) {
					map.put("settccy", "CNY");// 币种
					map.put("prdNo", "443");// 产品
					map.put("ctlType", bondTemplate.get(i).getCtlType());// 1监测，2监控
					map.put("limitType", bondTemplate.get(i).getLimitType());// 1单笔，2累计
					map.put("bondId", bondTemplate.get(i).getBondId());// 限额类型

					bondLimitTemplateMapper.insertIntoBondDetail(map);// 今日明细增加
				}
			} catch (Exception e) {
				LogManager.getLogger(LogManager.CONFIGURATOR_CLASS_KEY).info(
						"===========债券占用额度异常========");
			}
		}
	}
	
	/**
	 * 获取债券投资业务限额管理
	 * 
	 * @param
	 * @return
	 * @throws Exception 
	 * @throws RemoteConnectFailureException 
	 */
	@ResponseBody
	@RequestMapping(value = "/updateSDInverstReport")
	public void updateSDInverstReport() throws RemoteConnectFailureException, Exception{
		String nowDate = ifsOpicsSettleManageMapper.getCurDate();// 获取今日日期
		Map<String,String> brpsMap=new HashMap<String, String>();
		brpsMap.put("br", "01");
		String lastDate=acupServer.getAcupDate(brpsMap);	// 获取上一个跑批日期
		
		for(int j=1;j<3;j++){
			String bondFlag = j+"";//1是债券，2是存单
			
			//清除昨日数据
			HashMap<String, Object> deleteMap=new HashMap<String, Object>();
			deleteMap.put("oprDate", lastDate);
			deleteMap.put("bondFlag", bondFlag);//1债券，2存单
			deleteMap.put("detailFlag", "1");//是否为明细，0明细，1总计
			bondLimitTemplateMapper.deleteExistBond(deleteMap);
			
			// opics中获取存量债券信息
			Map<String, Object> opicsMap = new HashMap<String, Object>();
			// opicsMap.put("postdate",nowDate);
			opicsMap.put("postdate", lastDate);
			
			double totalAmtT = 0;//交易账户投资限额
			double creditAmtT = 0;//交易账户信用债投资限额
			double lowLevelLimitT = 0;//交易账户信用债AA及以下
			double totalAmtA = 0;//可供出售用户投资限额
			double creditAmtA = 0;//可供出售用户信用债投资限额
			double lowLevelLimitA = 0;//可供出售用户信用债AA及以下
			double totalAmtH = 0;//持有到期账户投资限额
			double creditAmtH = 0;//持有到期账户信用债投资限额
			double lowLevelLimitH = 0;//持有到期账户信用债AA及以下
			
			List<SlBondPositionBalanceBean> list = baseServer.getBondPositionBalanceExport(opicsMap);
			for (SlBondPositionBalanceBean slBondPositionBalanceBean : list) {
				
				try {
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("today", nowDate);// 当前日期
					map.put("lastDate", lastDate);// 昨日日期
					double amt = 0;
					String invtype = "";
					String bondCode = "";

					if (StringUtil.isNotEmpty(slBondPositionBalanceBean.getFacevalue())) {// 券面金额
						map.put("amt", slBondPositionBalanceBean.getFacevalue());
						amt = Double.parseDouble(slBondPositionBalanceBean.getFacevalue());
					}else{
						continue;
					}
					if (StringUtil.isNotEmpty(slBondPositionBalanceBean.getInvtype())) {// 账户类型H，T，A
						map.put("invtype", slBondPositionBalanceBean.getInvtype());
						invtype = slBondPositionBalanceBean.getInvtype();
					}else{
						continue;
					}
					if (StringUtil.isNotEmpty(slBondPositionBalanceBean.getSecid())) {// 债券代码
						map.put("bondId", slBondPositionBalanceBean.getSecid().trim());
						map.put("bondCode", slBondPositionBalanceBean.getSecid().trim());
						bondCode = slBondPositionBalanceBean.getSecid().trim();
					}else{
						continue;
					}
					
					IfsOpicsBond ifsOpicsBond = ifsOpicsBondService.searchOneById(bondCode);
					String prodtype = "";//债券类型,债券还是存单
					String bondProperties = "";//债券分类
					String bondRating = "";//评级类型
					int resultBond = 14;//债券等级
					
					if (StringUtil.isNotEmpty(ifsOpicsBond.getProdtype())) {//债券类型,mei
						prodtype = ifsOpicsBond.getProdtype();
						if("1".equals(bondFlag)){//债券1，存单2
							if(!"SD".equals(prodtype)){//只对债券进行限额校验
								continue;
							}
						}else if("2".equals(bondFlag)){
							if(!"CD".equals(prodtype)){//只对存单进行限额校验
								continue;
							}
						}else{
							continue;
						}
						
					}else{
						continue;
					}
					if (StringUtil.isNotEmpty(ifsOpicsBond.getBondproperties())) {//债券分类
						bondProperties = ifsOpicsBond.getBondproperties();
					}
					if (StringUtil.isNotEmpty(ifsOpicsBond.getBondrating())) {//评级类型
						bondRating = ifsOpicsBond.getBondrating();
						resultBond = Integer.parseInt(bondRating);
					}
					
					if("T".equals(invtype)){//交易账户
						totalAmtT = amt + totalAmtT;
						if("SE-QYZ,SE-GSZ,SE-ZP,SE-DR,SE-CDR,SE-DXGJ,".contains(bondProperties)){//信用债限额
							creditAmtT = creditAmtT + amt;
							if(!"".equals(bondRating)){
								resultBond = Integer.parseInt(bondRating);
							}
							if("T".equals(invtype)){//A类型账户账户类型，信用债等级为AA级及以下
								if(resultBond > 3){
									lowLevelLimitT = amt + lowLevelLimitT;
								}
							}
						}
					}else if("A".equals(invtype)){//可供出售账户
						totalAmtA = amt + totalAmtA;
						if("SE-QYZ,SE-GSZ,SE-ZP,SE-DR,SE-CDR,SE-DXGJ,".contains(bondProperties)){//信用债限额
							creditAmtA = creditAmtA + amt;
							if(!"".equals(bondRating)){
								resultBond = Integer.parseInt(bondRating);
							}
							if("T".equals(invtype)){//A类型账户账户类型，信用债等级为AA-级及以下
								if(resultBond > 4){
									lowLevelLimitA = amt + lowLevelLimitA;
								}
							}
						}
					}else if("H".equals(invtype)){//持有到期账户
						totalAmtH = amt + totalAmtH;
						if("SE-QYZ,SE-GSZ,SE-ZP,SE-DR,SE-CDR,SE-DXGJ,".contains(bondProperties)){//信用债限额
							creditAmtH = creditAmtH + amt;
							if(!"".equals(bondRating)){
								resultBond = Integer.parseInt(bondRating);
							}
							if("T".equals(invtype)){//A类型账户账户类型，信用债等级为AA-级及以下
								if(resultBond > 4){
									lowLevelLimitH = amt + lowLevelLimitH;
								}
							}
						}
					}
				}catch(Exception e) {
					LogManager.getLogger(LogManager.CONFIGURATOR_CLASS_KEY).info("===========债券限额报表异常========");
				}
			}
			
			//获取risk数据
			List<String> tt = new ArrayList<String>(Arrays.asList("A", "T", "H"));
			Map<String, Object> marketMap = new HashMap<String, Object>();
			for (String invtype : tt){
				String codeType = "";
				if("A".equals(invtype)){
					codeType = "SEN_AFS";
				}else if("T".equals(invtype)){
					codeType = "SEN_TB";
				}
				
				marketMap.put("code", codeType);
				String duration = "0";
				String convexity = "0";
				String delta = "0";
				try {
					SlCommonBean slCommonBean = new SlCommonBean();
					slCommonBean= iBaseServer.queryMarketData(marketMap);
					if(slCommonBean != null){
						duration = slCommonBean.getDuration();//久期
						convexity = slCommonBean.getConvexity();//止损
						delta = slCommonBean.getDelta();//DV01
					}
				} catch (RemoteConnectFailureException e) {
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				String lastMonthDay = lastDate.substring(0,8)+"01";
				lastMonthDay = nDaysAfterOneDateString(lastMonthDay,-1);
				String lastMonth = lastMonthDay.substring(0,7);
				Map<String, Object> mapBondQry = new HashMap<String, Object>();
				mapBondQry.put("lastMonth", lastMonth);//上一个月的日期
				mapBondQry.put("invtype", invtype);//操作类型
				mapBondQry.put("bondFlag", bondFlag);//债券投资1，存单投资2
				String convexityMonth = "0";
				String monthZ = bondLimitTemplateMapper.qryLastMonthLimit(mapBondQry);
				if("".equals(monthZ) || null == monthZ){
					convexityMonth = convexity;
				}else{
					convexityMonth = Double.parseDouble(convexity)-Double.parseDouble(monthZ)+"";
				}
				
				
				//存储数据
				Map<String, Object> bondLimitMap = new HashMap<String, Object>();
				if("A".equals(invtype)){
					bondLimitMap.put("investLimit", totalAmtA+"");//投资金额
					bondLimitMap.put("creditInvestLimit", creditAmtA+"");//信用债金额
					bondLimitMap.put("lowLevelLimit", lowLevelLimitA+"");//AA-级及以下金额
				}else if("T".equals(invtype)){
					bondLimitMap.put("investLimit", totalAmtT+"");//投资金额
					bondLimitMap.put("creditInvestLimit", creditAmtT+"");//信用债金额
					bondLimitMap.put("lowLevelLimit", lowLevelLimitT+"");//AA级及以下金额
				}else if("H".equals(invtype)){
					bondLimitMap.put("investLimit", totalAmtH+"");//投资金额
					bondLimitMap.put("creditInvestLimit", creditAmtH+"");//信用债金额
					bondLimitMap.put("lowLevelLimit", lowLevelLimitH+"");//AA-级及以下金额
				}
				
				bondLimitMap.put("invtype", invtype);//账户类型 A T H
				bondLimitMap.put("duration", duration);//久期限额
				bondLimitMap.put("convexityYear", convexity);//止损年
				bondLimitMap.put("convexityMonth", convexityMonth);//止损月
				bondLimitMap.put("dv01", delta);//DV01
				bondLimitMap.put("bondFlag", bondFlag);//债券类型,1债券，2存单
				bondLimitMap.put("todayFlag","1");//0是明细，1是总计
				bondLimitMap.put("oprTime", lastDate);//上一日时间
				
				//加入每日报表总计
				bondLimitTemplateMapper.insertBondInvestLimit(bondLimitMap);
			}
		}
	}
	
	
	/**
     * 客户准入模板导出
     */
    @ResponseBody
    @RequestMapping(value = "/templateExcel")
    public void templateExcel(HttpServletRequest request,
            HttpServletResponse response) {
        String ua = request.getHeader("User-Agent");
        String filename = "客户准入模板.xls"; // 文件名
        String encode_filename = "";
        try {
            encode_filename = URLEncoder.encode(filename, "utf-8");
        } catch (UnsupportedEncodingException e1) {
            throw new RException(e1.getMessage());
        }
        response.setContentType("application/vnd.ms-excel");
        if (ua != null && ua.indexOf("Firefox/") >= 0) {
            response.setHeader("Content-Disposition", "attachment; filename*="
                    + encode_filename);
        } else {
            response.setHeader("Content-Disposition", "attachment; filename="
                    + encode_filename);
        }
        response.setHeader("Connection", "close");
        try {
            ExcelUtil e = templateDoExcel();
            OutputStream ouputStream = response.getOutputStream();
            e.getWb().write(ouputStream);
            ouputStream.flush();
            ouputStream.close();
        } catch (Exception e) {
            throw new RException(e);
        }
    };

    /**
     * 客户准入 Excel模板文件
     */
    public ExcelUtil templateDoExcel() throws Exception {
//        SlIfsBean bean = new SlIfsBean();
//        List<SlIfsBean> list = ifsLimitService.searchAllData(); // 查数据
        ExcelUtil e = null;
        try {
            // 表头
            e = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
            CellStyle center = e.getDefaultCenterStrCellStyle();
            int sheet = 0;
            int row = 0;
            // 表格sheet名称
            e.getWb().createSheet("客户准入模板");
            // 设置表头字段名
            e.writeStr(sheet, row, "A", center, "客户号");
            e.writeStr(sheet, row, "B", center, "客户名称");
            e.writeStr(sheet, row, "C", center, "业务类型名称");
            e.writeStr(sheet, row, "D", center, "业务类型");
            e.writeStr(sheet, row, "E", center, "交易类型");
            e.writeStr(sheet, row, "F", center, "机构名称");
//            e.writeStr(sheet, row, "G", center, "包含下级标志");
            e.writeStr(sheet, row, "G", center, "审批日期");
            // 设置列宽
            e.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
            e.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
            e.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
            e.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
            e.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
            e.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
            e.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
//            for (int i = 0; i < list.size(); i++) {
//                bean = list.get(i);
//                row++;
//                // 插入数据
//                e.writeStr(sheet, row, "A", center,"客户号" );
//                e.writeStr(sheet, row, "B", center, );
//                e.writeStr(sheet, row, "C", center, );
//                e.writeStr(sheet, row, "D", center, );
//                e.writeStr(sheet, row, "E", center, );
//                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//                String date = sdf.format(bean.getAPPROVAL_DATE());
//                e.writeStr(sheet, row, "F", center, date);
//            }
            return e;
        } catch (Exception e1) {
            throw new RException(e1);
        }
    }

//String str = HrbReports.getReportModel(report);//获取模板    File file = new File(localPath + localFileName);

	/**
	 * 节假日模板导出
	 */
	@ResponseBody
	@RequestMapping(value = "/hldyExcel")
	public void hldyExcel(HttpServletRequest request,
							  HttpServletResponse response) {
		OutputStream out = null;
		FileInputStream in = null;

		try {
			String fileName = "MarketHoliday_模板";
			// 读取模板
			String excelPath = request.getSession().getServletContext().getRealPath("standard/hdly/MarketHoliday.xls");

			fileName = URLEncoder.encode(fileName, "UTF-8");

			response.reset();
			// 追加时间
			response.addHeader("Content-Disposition", "attachment;filename="
					+ fileName + ".xls");
			response.setContentType("application/octet-stream;charset=UTF-8");

			out = response.getOutputStream();
			in = new FileInputStream(excelPath);

			byte[] b = new byte[1024];
			int len;

			while ((len = in.read(b)) > 0) {
				response.getOutputStream().write(b, 0, len);
			}
			out.flush();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (null != in) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				in = null;
			}
			if (null != out) {
				try {
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				out = null;
			}
		}
	};






}