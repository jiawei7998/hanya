package com.singlee.ifs.controller;


import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.hrbreport.util.HrbReportUtils;
import com.singlee.ifs.model.IfsCorePositionBean;
import com.singlee.ifs.service.IfsFxHxImportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/IfsFxHxImportController")
public class IfsFxHxImportController extends CommonController{

	
	
	@Autowired
	IfsFxHxImportService ifsFxHxImportService;
	/***
	 * 核心 分发平台数据
	 */
	@ResponseBody
	@RequestMapping(value = "/searchFxHxMsg")
	public RetMsg<PageInfo<IfsCorePositionBean>> searchFxHxMsg(@RequestBody Map<String, Object> map) {
		        
		 List<IfsCorePositionBean>list =ifsFxHxImportService.getAlList(map);
		 int pageNumber=(int)map.get("pageNumber");
		 int pageSize=(int)map.get("pageSize");
		 Page<IfsCorePositionBean> page = HrbReportUtils.producePage(list, pageNumber, pageSize);
		 
		return RetMsgHelper.ok(page);
	}



	
    
    
	
}
