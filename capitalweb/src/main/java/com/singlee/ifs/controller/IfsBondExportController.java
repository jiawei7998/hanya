package com.singlee.ifs.controller;

import com.alibaba.fastjson.JSON;
import com.singlee.capital.base.util.FileManage;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.UploadedFile;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.credit.util.ExcelUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.financial.bean.SlBondHoldBean;
import com.singlee.financial.bean.SlBondTradeBean;
import com.singlee.financial.opics.IBondHoldServer;
import com.singlee.financial.opics.IBondTradeServer;
import com.singlee.ifs.service.IfsBondPledgeService;

import jxl.Sheet;
import jxl.Workbook;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.CellStyle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * 导出债券交易报表Controller
 */
@Controller
@RequestMapping(value = "/IfsBondExportController")
public class IfsBondExportController extends CommonController {

	@Autowired
	private IBondTradeServer ibondTradeServer;
	
	@Autowired
	private IBondHoldServer ibondHoldServer;
	
	@Autowired
	private IfsBondPledgeService ifsBondPledgeService;
	
	
	private static Logger logger = Logger.getLogger(IfsBondExportController.class);

	

	/**
	 * 债券交易报表导出
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/exportBondTradeReport")
	public void exportBondTradeReport(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String ua = request.getHeader("User-Agent");
		String br = request.getParameter("br");//部门
		String dealno = request.getParameter("dealno");//交易单号
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");
		String filename = "债券交易查询.xlsx"; // 文件名
		String encode_filename = "";
		try {
			encode_filename = URLEncoder.encode(filename, "utf-8");
		} catch (UnsupportedEncodingException e1) {
			throw new RException(e1.getMessage());
		}
		response.setContentType("application/vnd.ms-excel");
		if (ua != null) {
			response.setHeader("Content-Disposition", "attachment; filename=" + encode_filename);
		}
		response.setHeader("Connection", "close");
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("br", br);
			map.put("dealno", dealno);
			map.put("startDate", startDate);
			map.put("endDate", endDate);
			ExcelUtil e = downloadBondTradeReportExcel(map);
			OutputStream ouputStream = response.getOutputStream();
			e.getWb().write(ouputStream);
			ouputStream.flush();
			ouputStream.close();
		} catch (Exception e) {
			logger.error("债券交易查询报表下载出错", e);
			throw new RException(e);
		}
	}
	/**
	 * 债券交易查询  统计Excel文件下载内容
	 */
	public ExcelUtil downloadBondTradeReportExcel(Map<String, Object> map) throws Exception {
		ExcelUtil excel = null;
		try {
			/* 先查全部数据，再设置表格属性以及内容 */
			List<SlBondTradeBean> list=ibondTradeServer.searchBondTradeCount(map).getList();
			// 表头
			excel = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
			CellStyle center = excel.getDefaultCenterStrCellStyle();
			CellStyle left = excel.getDefaultLeftStrCellStyle();
			int sheet = 0;
			int row = 0;
			// 表格sheet名称
			excel.getWb().createSheet("债券交易查询");
			// 设置表头字段名
			excel.writeStr(sheet, row, "A", center, "交易单号");
			excel.writeStr(sheet, row, "B", center, "状态");
			excel.writeStr(sheet, row, "C", center, "交易对手");
			excel.writeStr(sheet, row, "D", center, "交易序号");
			excel.writeStr(sheet, row, "E", center, "账户");
			excel.writeStr(sheet, row, "F", center, "交易日");
			excel.writeStr(sheet, row, "G", center, "债券类型");
			excel.writeStr(sheet, row, "H", center, "市场");
			excel.writeStr(sheet, row, "I", center, "代码");
			excel.writeStr(sheet, row, "J", center, "名称");
			excel.writeStr(sheet, row, "K", center, "方向");
			excel.writeStr(sheet, row, "L", center, "清算方式");
			excel.writeStr(sheet, row, "M", center, "结算方式");
			excel.writeStr(sheet, row, "N", center, "净价(元)");
			excel.writeStr(sheet, row, "O", center, "结算日");
			excel.writeStr(sheet, row, "P", center, "收益率(%)");
			excel.writeStr(sheet, row, "Q", center, "全价(元)");
			excel.writeStr(sheet, row, "R", center, "成交面额(万元)");
			excel.writeStr(sheet, row, "S", center, "成交金额(元)");
			excel.writeStr(sheet, row, "T", center, "现金流(元)");
			excel.writeStr(sheet, row, "U", center, "净价金额(元)");
			excel.writeStr(sheet, row, "V", center, "利息(元)");
			excel.writeStr(sheet, row, "W", center, "发起人");
			excel.writeStr(sheet, row, "X", center, "备注");
		
			// 设置列宽
			excel.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(8, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(9, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(10, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(11, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(12, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(13, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(14, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(15, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(16, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(17, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(18, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(19, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(20, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(21, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(22, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(23, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(24, 20 * 256);
            for (int i = 0; i < list.size(); i++) {
            	row++;
            	SlBondTradeBean bondTradeBean=list.get(i);
            	DecimalFormat df = new DecimalFormat("#0.00");
            	excel.writeStr(sheet, row, "A", center, bondTradeBean.getDealno());
      		    excel.writeStr(sheet, row, "B", center, bondTradeBean.getStatus());
      		    excel.writeStr(sheet, row, "C", center, bondTradeBean.getCmne());
      			excel.writeStr(sheet, row, "D", center, bondTradeBean.getSeq());
      			excel.writeStr(sheet, row, "E", center, bondTradeBean.getInvtype());
      			excel.writeStr(sheet, row, "F", center, bondTradeBean.getDealdate().substring(0,10));//交易日
      			excel.writeStr(sheet, row, "G", center, bondTradeBean.getAcctngtype());
      			excel.writeStr(sheet, row, "H", center, bondTradeBean.getMarket());
      			excel.writeStr(sheet, row, "I", center, bondTradeBean.getSecid());
      			excel.writeStr(sheet, row, "J", left, bondTradeBean.getDescr());
      			excel.writeStr(sheet, row, "K", center, bondTradeBean.getPs());
      			excel.writeStr(sheet, row, "L", center, bondTradeBean.getMethod());
      			excel.writeStr(sheet, row, "M", center, bondTradeBean.getDvp());
      			excel.writeStr(sheet, row, "N", center, bondTradeBean.getPrice()==null?"":bondTradeBean.getPrice().setScale(4, BigDecimal.ROUND_HALF_UP).toString());//净价(元)
      			excel.writeStr(sheet, row, "O", center, bondTradeBean.getSettdate().substring(0,10));//结算日
      			excel.writeStr(sheet, row, "P", center, df.format(Double.valueOf(bondTradeBean.getYield_8())));//收益率%
      			excel.writeStr(sheet, row, "Q", center, bondTradeBean.getPrice_8()==null?"":bondTradeBean.getPrice_8().setScale(4, BigDecimal.ROUND_HALF_UP).toString());//全价(元)
      			excel.writeStr(sheet, row, "R", center, bondTradeBean.getFaceamt()==null?"":bondTradeBean.getFaceamt().divide(new BigDecimal("10000")).setScale(0, BigDecimal.ROUND_HALF_UP).toString());//成交面额(万元)
      			excel.writeStr(sheet, row, "S", center, bondTradeBean.getSettamt()==null?"":bondTradeBean.getSettamt().setScale(2, BigDecimal.ROUND_HALF_UP).toString());//成交金额(元)
      			excel.writeStr(sheet, row, "T", center, bondTradeBean.getCashsettamt()==null?"":bondTradeBean.getCashsettamt().setScale(2, BigDecimal.ROUND_HALF_UP).toString());//现金流(元)
      			excel.writeStr(sheet, row, "U", center, bondTradeBean.getCostamt()==null?"":bondTradeBean.getCostamt().setScale(0, BigDecimal.ROUND_HALF_UP).toString());//净价金额(元)
      			excel.writeStr(sheet, row, "V", center, bondTradeBean.getPurchintamt()==null?"":bondTradeBean.getPurchintamt().setScale(0, BigDecimal.ROUND_HALF_UP).toString());//利息(元)
      			excel.writeStr(sheet, row, "W", center, bondTradeBean.getTrad());
      			excel.writeStr(sheet, row, "X",  left,bondTradeBean.getDealtext());
            }
			return excel;
		} catch (Exception e) {
			throw new RException(e);
		}
	}
	
	
	
	
	
	
	
	/**
	 * 债券持仓报表导出
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/exportBondHoldReport")
	public void exportBondHoldReport(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String ua = request.getHeader("User-Agent");
		String br = request.getParameter("br");//部门
		String filename = "债券持仓查询.xlsx"; // 文件名
		String encode_filename = "";
		try {
			encode_filename = URLEncoder.encode(filename, "utf-8");
		} catch (UnsupportedEncodingException e1) {
			throw new RException(e1.getMessage());
		}
		response.setContentType("application/vnd.ms-excel");
		if (ua != null) {
			response.setHeader("Content-Disposition", "attachment; filename=" + encode_filename);
		}
		response.setHeader("Connection", "close");
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("br", br);
			ExcelUtil e = downloadBondHoldReportExcel(map);
			OutputStream ouputStream = response.getOutputStream();
			e.getWb().write(ouputStream);
			ouputStream.flush();
			ouputStream.close();
		} catch (Exception e) {
			logger.error("债券持仓查询报表下载出错", e);
			throw new RException(e);
		}
	}
	/**
	 * 债券持仓查询  统计Excel文件下载内容
	 */
	public ExcelUtil downloadBondHoldReportExcel(Map<String, Object> map) throws Exception {
		ExcelUtil excel = null;
		try {
			/* 先查全部数据，再设置表格属性以及内容 */
			List<SlBondHoldBean> list=ibondHoldServer.searchBondHoldCount(map).getList();
			// 表头
			excel = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
			CellStyle center = excel.getDefaultCenterStrCellStyle();
			CellStyle left = excel.getDefaultLeftStrCellStyle();
			int sheet = 0;
			int row = 0;
			// 表格sheet名称
			excel.getWb().createSheet("债券持仓查询");
			// 设置表头字段名
			excel.writeStr(sheet, row, "A", center, "机构");
			excel.writeStr(sheet, row, "B", center, "部门");
			excel.writeStr(sheet, row, "C", center, "债券类型");
			excel.writeStr(sheet, row, "D", center, "债券代码");
			excel.writeStr(sheet, row, "E", center, "债券名称");
			excel.writeStr(sheet, row, "F", center, "账户分类");
			excel.writeStr(sheet, row, "G", center, "发行人");
			excel.writeStr(sheet, row, "H", center, "剩余年限");
			excel.writeStr(sheet, row, "I", center, "付息剩余天数");
			excel.writeStr(sheet, row, "J", center, "付息方式");
			excel.writeStr(sheet, row, "K", center, "下个付息日");
			excel.writeStr(sheet, row, "L", center, "年限");
			excel.writeStr(sheet, row, "M", center, "债券信用级别");
			excel.writeStr(sheet, row, "N", center, "含权类");
			excel.writeStr(sheet, row, "O", center, "行权日");
			excel.writeStr(sheet, row, "P", center, "行权剩余年限");
			excel.writeStr(sheet, row, "Q", center, "起息日");
			excel.writeStr(sheet, row, "R", center, "到期日");
			excel.writeStr(sheet, row, "S", center, "利率方式");
			excel.writeStr(sheet, row, "T", center, "债券主体评级");
			excel.writeStr(sheet, row, "U", center, "票面利率(%)");
			excel.writeStr(sheet, row, "V", center, "持仓面额(元)");
			excel.writeStr(sheet, row, "W", center, "已质押面额");
			excel.writeStr(sheet, row, "X", center, "全价成本(元)");
			excel.writeStr(sheet, row, "Y", center, "净价成本(元)");
			excel.writeStr(sheet, row, "Z", center, "公允价值调整");
			excel.writeStr(sheet, row, "AA", center, "账面余额(元)");
			excel.writeStr(sheet, row, "AB", center, "应收利息");
			excel.writeStr(sheet, row, "AC", center, "本金");
			excel.writeStr(sheet, row, "AD", center, "百元净价成本");
			excel.writeStr(sheet, row, "AE", center, "百元全价成本");
			excel.writeStr(sheet, row, "AF", center, "到期收益率");
			excel.writeStr(sheet, row, "AG", center, "百元净价估值");
			excel.writeStr(sheet, row, "AH", center, "百元全价估值");
			excel.writeStr(sheet, row, "AI", center, "净价浮盈(元)");
			excel.writeStr(sheet, row, "AJ", center, "浮动盈亏比");
			
			
		
			// 设置列宽
			excel.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(8, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(9, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(10, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(11, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(12, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(13, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(14, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(15, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(16, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(17, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(18, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(19, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(20, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(21, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(22, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(23, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(24, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(25, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(26, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(27, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(28, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(29, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(30, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(31, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(32, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(33, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(34, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(35, 20 * 256);
			excel.getSheetAt(sheet).setColumnWidth(36, 20 * 256);
            for (int i = 0; i < list.size(); i++) {
            	row++;
            	SlBondHoldBean bondHoldBean=list.get(i);
            	excel.writeStr(sheet, row, "A", center, bondHoldBean.getBr());
            	excel.writeStr(sheet, row, "B", center, bondHoldBean.getPort());
            	excel.writeStr(sheet, row, "C", center, bondHoldBean.getAcctngtype());
      		    excel.writeStr(sheet, row, "D", center, bondHoldBean.getSecid());
      		    excel.writeStr(sheet, row, "E", left, bondHoldBean.getDescr());
      			excel.writeStr(sheet, row, "F", center, bondHoldBean.getInvtype());
      			excel.writeStr(sheet, row, "G", center, bondHoldBean.getCmne());
      			excel.writeStr(sheet, row, "H", center, bondHoldBean.getTenor()==null?"":bondHoldBean.getTenor().setScale(2, BigDecimal.ROUND_HALF_UP).toString());//剩余年限
      			excel.writeStr(sheet, row, "I", center, bondHoldBean.getIntday());
      			excel.writeStr(sheet, row, "J", center, bondHoldBean.getIntpaycycle());
      			excel.writeStr(sheet, row, "K", center, bondHoldBean.getIpaydate().substring(0,10));//下个付息日
      			excel.writeStr(sheet, row, "L", center, bondHoldBean.getYears());
      			excel.writeStr(sheet, row, "M", center, bondHoldBean.getRating());
      			excel.writeStr(sheet, row, "N", center, bondHoldBean.getWeightedclass());
      			excel.writeStr(sheet, row, "O", center, bondHoldBean.getExercisedate()==null?"":bondHoldBean.getExercisedate().substring(0,10));//行权日
      			excel.writeStr(sheet, row, "P", center, bondHoldBean.getExerciseyears()==null?"":bondHoldBean.getExerciseyears().setScale(2, BigDecimal.ROUND_HALF_UP).toString());//行权剩余年限
      			excel.writeStr(sheet, row, "Q", center, bondHoldBean.getVdate().substring(0,10));//起息日
      			excel.writeStr(sheet, row, "R", center, bondHoldBean.getMdate().substring(0,10));//到期日
      			excel.writeStr(sheet, row, "S", center, bondHoldBean.getRatecode());
      			excel.writeStr(sheet, row, "T", center, bondHoldBean.getSubjectrating());
      			excel.writeStr(sheet, row, "U", center, bondHoldBean.getCouprate_8()==null?"":bondHoldBean.getCouprate_8().setScale(2, BigDecimal.ROUND_HALF_UP).toString());//票面利率
      			excel.writeStr(sheet, row, "V", center, bondHoldBean.getPrinamt()==null?"":bondHoldBean.getPrinamt().setScale(2, BigDecimal.ROUND_HALF_UP).toString());//持仓面额(元)
      			excel.writeStr(sheet, row, "W", center, bondHoldBean.getPledgedfaceamt()==null?"":bondHoldBean.getPledgedfaceamt().setScale(2, BigDecimal.ROUND_HALF_UP).toString());//已质押面额
      			excel.writeStr(sheet, row, "X", center, bondHoldBean.getFullcost()==null?"":bondHoldBean.getFullcost().setScale(4, BigDecimal.ROUND_HALF_UP).toString());//全价成本(元)
      			excel.writeStr(sheet, row, "Y", center, bondHoldBean.getNetcost()==null?"":bondHoldBean.getNetcost().setScale(4, BigDecimal.ROUND_HALF_UP).toString());//净价成本(元)
      			excel.writeStr(sheet, row, "Z",  center,bondHoldBean.getUnamortamt()==null?"":bondHoldBean.getUnamortamt().setScale(2, BigDecimal.ROUND_HALF_UP).toString());//公允价值调整
      			excel.writeStr(sheet, row, "AA", center, bondHoldBean.getSettavgcost()==null?"":bondHoldBean.getSettavgcost().setScale(4, BigDecimal.ROUND_HALF_UP).toString());//账面余额(元)
      			excel.writeStr(sheet, row, "AB", center, bondHoldBean.getAccr_intamt()==null?"":bondHoldBean.getAccr_intamt().setScale(2, BigDecimal.ROUND_HALF_UP).toString());//应收利息
      			excel.writeStr(sheet, row, "AC", center, bondHoldBean.getAmt()==null?"":bondHoldBean.getAmt().setScale(4, BigDecimal.ROUND_HALF_UP).toString());//本金
      			excel.writeStr(sheet, row, "AD", center, bondHoldBean.getUnitcost()==null?"":bondHoldBean.getUnitcost().setScale(4, BigDecimal.ROUND_HALF_UP).toString());//百元净价成本
      			excel.writeStr(sheet, row, "AE", center, bondHoldBean.getNetpricecost()==null?"":bondHoldBean.getNetpricecost().setScale(4, BigDecimal.ROUND_HALF_UP).toString());//百元全价成本
      			excel.writeStr(sheet, row, "AF", center, bondHoldBean.getTclsgprice_8());//到期收益率
      			excel.writeStr(sheet, row, "AG", center, bondHoldBean.getSclsgprice_8()==null?"":bondHoldBean.getSclsgprice_8().setScale(4, BigDecimal.ROUND_HALF_UP).toString());//百元净价估值
      			excel.writeStr(sheet, row, "AH", center, bondHoldBean.getSclsgprice2_8()==null?"":bondHoldBean.getSclsgprice2_8().setScale(4, BigDecimal.ROUND_HALF_UP).toString());//百元全价估值
      			excel.writeStr(sheet, row, "AI", center, bondHoldBean.getDiff_amt()==null?"":bondHoldBean.getDiff_amt().setScale(4, BigDecimal.ROUND_HALF_UP).toString());//净价浮盈(元)
      			excel.writeStr(sheet, row, "AJ", center, bondHoldBean.getFloatamt());//浮动盈亏比
            }
			return excel;
		} catch (Exception e) {
			throw new RException(e);
		}
	}
	
	@RequestMapping(value="/importBondOpicsExcel")
	public String importBondOpicsExcel(MultipartHttpServletRequest request, HttpServletResponse response) throws IOException{
		String message = "导入成功";
		Workbook wb = null;
		try{
			List<UploadedFile> uploadedFileList = FileManage.requestExtractor(request);
			List<Workbook> excelList = new LinkedList<Workbook>();
			for (UploadedFile uploadedFile : uploadedFileList) {
				JY.ensure("xls".equals(uploadedFile.getType()), "上传文件类型错误,仅允许上传xls格式文件.");
				Workbook book = Workbook.getWorkbook(new ByteArrayInputStream(uploadedFile.getBytes()));
				excelList.add(book);
			}
			JY.require(excelList.size() == 1, "只能处理一份excel");
			wb = excelList.get(0);
			Sheet[] sheets = wb.getSheets();//获得卡片sheet数量
			
			//update by zl 2019-08-20 从第一页开始读取sheet
//			for (int i = 0; i < sheets.length; i++) { // 循环解析产品的会计配置

			Map<String, Object> mapParam = new HashMap<String, Object>();

			mapParam = ifsBondPledgeService.saveReadBondExcelImportToOpicsService(sheets[0]);
//			}
			message = "excel共计"+mapParam.get("rows")+"支债券,成功导入了"+mapParam.get("count")+"支债券";	
		}catch (Exception e) {
			message = "导入失败";
			// TODO: handle exception
			e.printStackTrace();
			return e.toString();
		}finally{
			if(null != wb)
				wb.close();
		}
		return message;
	}
	
}