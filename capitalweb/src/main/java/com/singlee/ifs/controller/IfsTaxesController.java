package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.ifs.model.IfsTaxes;
import com.singlee.ifs.service.IfsTaxesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.List;
import java.util.Map;


@Controller
@RequestMapping(value = "/IfsTaxesController")
public class IfsTaxesController {
	@Autowired
	IfsTaxesService ifsTaxesService;

	/***
	 * 分页查询
	 */
	@ResponseBody
	@RequestMapping(value = "/getDataPage")
	public RetMsg<PageInfo<IfsTaxes>> getDataPage(@RequestBody Map<String, Object> map) {
		map.put("startDate","".equals(map.get("startDate"))?"":map.get("startDate").toString().substring(0,10));
		map.put("endDate","".equals(map.get("endDate"))?"":map.get("endDate").toString().substring(0,10));

		Page<IfsTaxes> page = ifsTaxesService.getDataPage(map);
		return RetMsgHelper.ok(page);
	}

	/***
	 * 添加
	 */
	@ResponseBody
	@RequestMapping(value = "/addData",method = RequestMethod.POST)
	public RetMsg<Serializable> addData( @RequestBody List<Map<String,Object>> list) {

		if(list.size()>0){
			if(list.get(0).get("id")==null){
				ifsTaxesService.addData(list);
			}else{
				ifsTaxesService.update(list);
			}
		}
		return RetMsgHelper.ok("保存成功!");
	}

	/***
	 * 添加
	 */
	@ResponseBody
	@RequestMapping(value = "/delData",method = RequestMethod.POST)
	public RetMsg<Serializable> delData( @RequestBody List<Map<String,Object>> list) {
		ifsTaxesService.delData(list);
		return RetMsgHelper.ok("删除成功!");
	}

}
