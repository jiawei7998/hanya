package com.singlee.ifs.controller;

import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.financial.bean.SlCashFlowBean;
import com.singlee.financial.opics.ICashFlowServer;
import com.singlee.financial.page.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;


/**
 * 现金流查询
 * 
 * @author fengll
 * 
 */
@Controller
@RequestMapping(value = "/CashFlowStatementController")
public class CashFlowStatementController {
	@Autowired
	private ICashFlowServer CashFlowStatementServer;
	
	/***
	 * 分页现金流展示
	 */
	@ResponseBody
	@RequestMapping(value = "/searchSceId")
	public RetMsg<PageInfo<SlCashFlowBean>> searchSceId(@RequestBody Map<String, String> map) {
		PageInfo<SlCashFlowBean> page = CashFlowStatementServer.getCashFlowList(map);
		return  RetMsgHelper.ok(page);
	}
	
}
