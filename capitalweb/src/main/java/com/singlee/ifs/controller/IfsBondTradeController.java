package com.singlee.ifs.controller;

import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.financial.bean.SlBondTradeBean;
import com.singlee.financial.opics.IBondTradeServer;
import com.singlee.financial.page.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/***
 *债券报表
 * 
 */
@Controller
@RequestMapping(value = "/IfsBondTradeController")
public class IfsBondTradeController {
	
	@Autowired
	IBondTradeServer bondTradeServer;
	
	
	/**
	 * 债券交易查询
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/searchBondTradeCount")
	public RetMsg<PageInfo<SlBondTradeBean>> searchBondTradeCount(@RequestBody Map<String, Object> map) throws RemoteConnectFailureException, Exception {
		PageInfo<SlBondTradeBean> page = bondTradeServer.searchBondTradeCount(map);
		return RetMsgHelper.ok(page);
	}
	
	
}
