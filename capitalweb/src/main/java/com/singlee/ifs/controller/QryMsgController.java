package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.github.pagehelper.StringUtil;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.financial.cfets.cpis.IImixCPISManageServer;
import com.singlee.financial.model.*;
import com.singlee.financial.pojo.TradeSettlsBean;
import com.singlee.ifs.mapper.CPISMessageMapper;
import com.singlee.ifs.mapper.TdQryMsgMapper;
import com.singlee.ifs.mapper.TtFxBasicMapper;
import com.singlee.ifs.model.TdQryMsg;
import com.singlee.ifs.model.TtFxBasic;
import com.singlee.ifs.service.QryMsgService;
import com.singlee.ifs.service.TradeSettlsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.awt.*;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.*;


@Controller
@RequestMapping(value = "/QryMsgController")
public class QryMsgController {
	
	
	@Autowired
	private QryMsgService qryMsgService;

	@Autowired
	TradeSettlsService tradeSettlsService;
	
	//外汇即期
	@ResponseBody
	@RequestMapping(value = "/searchQryMsg")
	public RetMsg<PageInfo<TdQryMsg>> searchQryMsg(@RequestBody Map<String, Object> params) {
		Page<TdQryMsg> page = qryMsgService.searchQryMsg(params);
		return RetMsgHelper.ok(page);
	}
	//未发送报文交易
	@ResponseBody
	@RequestMapping(value = "/searchNoSendDeal")
	public RetMsg<PageInfo<TdQryMsg>> searchNoSendDeal(@RequestBody Map<String, Object> params) {
		Page<TdQryMsg> page = qryMsgService.searchNoSendDeal(params);
		return RetMsgHelper.ok(page);
	}

	//发送确认报文
	@ResponseBody
	@RequestMapping(value = "/sendConfirmMessage")
	public RetMsg<Serializable> sendConfirmMessage(@RequestBody Map<String, Object> param) throws Exception {
		StringBuffer sb = new StringBuffer();
		int total = ParameterUtil.getInt(param,"total",0);
		sb.append("交易后报文总共发送："+String.valueOf(total)+"\n");
		int success = 0;

		CPISMessageMapper cpisMessageMapper = SpringContextHolder.getBean(CPISMessageMapper.class);
		TtFxBasicMapper ttFxBasicMapper = SpringContextHolder.getBean(TtFxBasicMapper.class);
		TdQryMsgMapper tdQryMsgMapper = SpringContextHolder.getBean(TdQryMsgMapper.class);
		IImixCPISManageServer iImixCPISManageServer = SpringContextHolder.getBean("IImixCPISManageServer");

		String promptMsg="";//提示信息
		String marketIndicator = ParameterUtil.getString(param, "marketIndicator", "");//市场标识
		String tradeDate = ParameterUtil.getString(param, "tradeDate", "");//日期
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		if("".equals(tradeDate)){
			tradeDate=sdf.format(new Date());//默认当前日期
		}
		String execID = ParameterUtil.getString(param, "execID", "");//交易主键
		String sendAgain=ParameterUtil.getString(param, "sendAgain", "");//1:重发标志

		String myInst= SystemProperties.cfetsInstId;//本方机构21码

		try {
			Map<String, Object> dateMap = new HashMap<String, Object>();
			dateMap.put("cDate", tradeDate);//界面日期，没有默认当前日期
//			dateMap.put("cDate", cpisMessageMapper.getCurrentDate());//系统时间
			if("".equals(execID)){
				dateMap.put("execID", execID);//成交单号
			}else{
				dateMap.put("execID", execID.split(","));//成交单号
			}
//			dateMap.put("userId", SlSessionHelper.getUserId());//登录账户，需求，报文发送不是交易员发送的
			dateMap.put("sendAgain", sendAgain);
			Map<String, Object> params = new HashMap<String, Object>();
			Map<String, String> typeMap = new HashMap<String, String>();
			typeMap.put("interAccount_type", "213");//中间行账号
			typeMap.put("interBank_type", "209");//中间行名称
			typeMap.put("interBicCode_type", "211");//中间行BICCODE
			typeMap.put("opAccount_type", "207");//开户行账号
			typeMap.put("opBank_type", "110");//开户行名称
			typeMap.put("opBicCode_type", "138");//开户行BICCODE
			typeMap.put("reAccount_type", "15");//收款行账号
			typeMap.put("reBank_type", "23");//收款行名称
			typeMap.put("reBicCode_type", "16");//收款行BICCODE
			if("21".equals(marketIndicator)){
				//外币拆借
				List<SendForeignFxmmModel> fxmmList = cpisMessageMapper.searchForeignFxmmMessage(dateMap);
				for (SendForeignFxmmModel sendForeignFxmmModel : fxmmList) {
					params.put("ticketId", sendForeignFxmmModel.getDealNo());


					TradeSettlsBean tradeSettlsBean = tradeSettlsService.getTradeSettls(params);
					tradeSettlsBean.setFullName("");//本方中文全称
					tradeSettlsBean.setcFullName("");//对手方中文全称
					sendForeignFxmmModel.setTradeSettlsBean(tradeSettlsBean);

					//add by lzb
					sendForeignFxmmModel.setMarketIndicator("21");
					sendForeignFxmmModel.setExecID(sendForeignFxmmModel.getExecId());
					sendForeignFxmmModel.setPartyID_2(sendForeignFxmmModel.getPartyCfetsNo());
					boolean flag = iImixCPISManageServer.confirmCPISTransaction(sendForeignFxmmModel);
					if (flag) {
						Map<String, Object> msgDetail = new HashMap<String, Object>();
						msgDetail.put("dealNo", sendForeignFxmmModel.getDealNo());
						msgDetail.put("execID", sendForeignFxmmModel.getExecID());
						msgDetail.put("confirmID", sendForeignFxmmModel.getConfirmID());
						msgDetail.put("br", sendForeignFxmmModel.getBr());
						msgDetail.put("sendState", "103");
						msgDetail.put("tradeDate", tradeDate);
						msgDetail.put("marketIndicator", marketIndicator);
						msgDetail.put("execType","0"); //成交状态0-正常(外汇),4-撤销
						msgDetail.put("affirmStatus", "INI");//校验状态，默认是会员上传，目的防止接收报文有误导致无线定时发送确认。
						List<TdQryMsg> recList = tdQryMsgMapper.searchQryMsg(msgDetail);
						if (!(recList!=null && recList.size()>0)) {
							tdQryMsgMapper.insert(msgDetail);
							success++;
						}
					}
				}
			}else if("12".equals(marketIndicator)||"14".equals(marketIndicator)){
				//外汇即期，外汇远期
				List<SendForeignForwardSpotModel> forwardSpotModels;
				//注释外汇即期：买入和卖出金额需要根据交易方向转换，此转换放到查询方法中
				if("12".equals(marketIndicator)){
					forwardSpotModels = cpisMessageMapper.searchForeignSptMessage(dateMap);
				}else{
					forwardSpotModels = cpisMessageMapper.searchForeignFwdMessage(dateMap);
					if("1".equals(sendAgain) && forwardSpotModels.size()==0){
						forwardSpotModels = cpisMessageMapper.searchForeignSptMessage(dateMap);
					}
				}
				for (SendForeignForwardSpotModel sendForeignForwardSpotModel : forwardSpotModels) {
					params.put("ticketId", sendForeignForwardSpotModel.getDealNo());

					TradeSettlsBean tradeSettlsBean = tradeSettlsService.getTradeSettls(params);
					if(tradeSettlsBean == null){
						tradeSettlsBean = new TradeSettlsBean();
					}
					tradeSettlsBean.setFullName("");//本方中文全称
					tradeSettlsBean.setcFullName("");//对手方中文全称
					sendForeignForwardSpotModel.setTradeSettlsBean(tradeSettlsBean);

					sendForeignForwardSpotModel.setMarketIndicator(marketIndicator);
					sendForeignForwardSpotModel.setExecID(sendForeignForwardSpotModel.getExecId());
					sendForeignForwardSpotModel.setPartyID_2(sendForeignForwardSpotModel.getPartyCFETSNO());

					boolean flag = iImixCPISManageServer.confirmCPISTransaction(sendForeignForwardSpotModel);
					if (flag) {
						Map<String, Object> msgDetail = new HashMap<String, Object>();
						msgDetail.put("dealNo", sendForeignForwardSpotModel.getDealNo());
						msgDetail.put("execID", sendForeignForwardSpotModel.getExecID());
						msgDetail.put("confirmID", sendForeignForwardSpotModel.getConfirmID());
						msgDetail.put("br", sendForeignForwardSpotModel.getBr());
						msgDetail.put("sendState", "103");
						msgDetail.put("tradeDate", tradeDate);
						msgDetail.put("execType","F"); //成交状态F-正常(外汇),4-撤销
						msgDetail.put("marketIndicator", marketIndicator);
						msgDetail.put("affirmStatus", "INI");//校验状态，默认是会员上传，目的防止接收报文有误导致无线定时发送确认。
						List<TdQryMsg> recList = tdQryMsgMapper.searchQryMsg(msgDetail);
						if (!(recList!=null && recList.size()>0)) {
							tdQryMsgMapper.insert(msgDetail);
							success++;
						}
					}
				}
			}else if("11".equals(marketIndicator)){
				//外汇掉期
				List<SendForeignSwapModel> sendForeignSwapModels = cpisMessageMapper.searchForeignSwapMessage(dateMap);
				for (SendForeignSwapModel sendForeignSwapModel : sendForeignSwapModels) {
					params.put("ticketId", sendForeignSwapModel.getDealNo());


					TradeSettlsBean tradeSettlsBean = tradeSettlsService.getTradeSettls(params);
					tradeSettlsBean.setFullName("");//本方中文全称
					tradeSettlsBean.setcFullName("");//对手方中文全称
					sendForeignSwapModel.setTradeSettlsBean(tradeSettlsBean);

					sendForeignSwapModel.setMarketIndicator(marketIndicator);
					sendForeignSwapModel.setExecID(sendForeignSwapModel.getExecId());
					sendForeignSwapModel.setPartyID_2(sendForeignSwapModel.getPartyID_2());

					boolean flag = iImixCPISManageServer.confirmCPISTransaction(sendForeignSwapModel);
					if (flag) {
						Map<String, Object> msgDetail = new HashMap<String, Object>();
						msgDetail.put("dealNo", sendForeignSwapModel.getDealNo());
						msgDetail.put("execID", sendForeignSwapModel.getExecID());
						msgDetail.put("confirmID", sendForeignSwapModel.getConfirmID());
						msgDetail.put("br", sendForeignSwapModel.getBr());
						msgDetail.put("sendState", "103");
						msgDetail.put("tradeDate", tradeDate);
						msgDetail.put("execType","F"); //成交状态F-正常(外汇),4-撤销
						msgDetail.put("marketIndicator", marketIndicator);
						msgDetail.put("affirmStatus", "INI");//校验状态，默认是会员上传，目的防止接收报文有误导致无线定时发送确认。
						List<TdQryMsg> recList = tdQryMsgMapper.searchQryMsg(msgDetail);
						if (!(recList!=null && recList.size()>0)) {
							tdQryMsgMapper.insert(msgDetail);
							success++;
						}
					}
				}
			} else{
				promptMsg="不在确认报文之内";
			}
		} catch (Exception e) {
			e.printStackTrace();
			sb.append("报文发送失败");
		} finally {

		}
		return RetMsgHelper.ok(sb.toString());
	}

	//发送确认报文
	public RetMsg<Serializable> sendConfirmMessageOld(@RequestBody Map<String, Object> param) throws Exception {
		StringBuffer sb = new StringBuffer();
		int total = ParameterUtil.getInt(param,"total",0);
		sb.append("交易后报文总共发送："+String.valueOf(total)+"\n");
		int success = 0;

		 CPISMessageMapper cpisMessageMapper = SpringContextHolder.getBean(CPISMessageMapper.class);
		 TtFxBasicMapper ttFxBasicMapper = SpringContextHolder.getBean(TtFxBasicMapper.class);
		 TdQryMsgMapper tdQryMsgMapper = SpringContextHolder.getBean(TdQryMsgMapper.class);
		 IImixCPISManageServer iImixCPISManageServer = SpringContextHolder.getBean("IImixCPISManageServer");

		String promptMsg="";//提示信息
		String marketIndicator = ParameterUtil.getString(param, "marketIndicator", "");//市场标识
		String tradeDate = ParameterUtil.getString(param, "tradeDate", "");//日期
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		if("".equals(tradeDate)){
			tradeDate=sdf.format(new Date());//默认当前日期
		}
		String execID = ParameterUtil.getString(param, "execID", "");//交易主键
		String sendAgain=ParameterUtil.getString(param, "sendAgain", "");//1:重发标志
		
		String myInst= SystemProperties.cfetsInstId;//本方机构21码
		
		try {
			Map<String, Object> dateMap = new HashMap<String, Object>();
			dateMap.put("cDate", tradeDate);//界面日期，没有默认当前日期
//			dateMap.put("cDate", cpisMessageMapper.getCurrentDate());//系统时间
			if("".equals(execID)){
				dateMap.put("execID", execID);//成交单号
			}else{
				dateMap.put("execID", execID.split(","));//成交单号
			}
//			dateMap.put("userId", SlSessionHelper.getUserId());//登录账户，需求，报文发送不是交易员发送的
			dateMap.put("sendAgain", sendAgain);
			Map<String, Object> params = new HashMap<String, Object>();
			Map<String, String> typeMap = new HashMap<String, String>();
			typeMap.put("interAccount_type", "213");//中间行账号
			typeMap.put("interBank_type", "209");//中间行名称
			typeMap.put("interBicCode_type", "211");//中间行BICCODE
			typeMap.put("opAccount_type", "207");//开户行账号
			typeMap.put("opBank_type", "110");//开户行名称
			typeMap.put("opBicCode_type", "138");//开户行BICCODE
			typeMap.put("reAccount_type", "15");//收款行账号
			typeMap.put("reBank_type", "23");//收款行名称
			typeMap.put("reBicCode_type", "16");//收款行BICCODE
			if("21".equals(marketIndicator)){
				//外币拆借
				List<SendForeignFxmmModel> fxmmList = cpisMessageMapper.searchForeignFxmmMessage(dateMap);
				for (SendForeignFxmmModel sendForeignFxmmModel : fxmmList) {
					params.put("ticketId", sendForeignFxmmModel.getDealNo());
					List<AgencyBasicInformationModel> list1 = new ArrayList<AgencyBasicInformationModel>();//本方机构清算信息
					List<AgencyBasicInformationModel> list2 = new ArrayList<AgencyBasicInformationModel>();//对手方机构清算信息
					List<TtFxBasic> fxlist1 = ttFxBasicMapper.searchMyInstBasicFXMM(params);
					list1 = fun1(list1, fxlist1, typeMap);
					List<TtFxBasic> fxlist2 = ttFxBasicMapper.searchMyInstBasicFXMMOpponet(params);
					list2 = fun1(list2, fxlist2, typeMap);
					//添加其他信息
					String packmsg = ttFxBasicMapper.searchMyInstBasicFXMMOther(params);
					list1=funOther(list1,packmsg,myInst,sendForeignFxmmModel.getPartyCfetsNo(),"5,124,139",marketIndicator);
					list2=funOther(list2,packmsg,sendForeignFxmmModel.getPartyCfetsNo(),myInst,"5,124,139",marketIndicator);
					TradeSettlsBean settlsBean = new TradeSettlsBean();

					sendForeignFxmmModel.setList1(list1);

					sendForeignFxmmModel.setList2(list2);


					//add by lzb
					sendForeignFxmmModel.setMarketIndicator("21");
					sendForeignFxmmModel.setExecID(sendForeignFxmmModel.getExecId());
					sendForeignFxmmModel.setPartyID_2(sendForeignFxmmModel.getPartyCfetsNo());
					boolean flag = iImixCPISManageServer.confirmCPISTransaction(sendForeignFxmmModel);
					if (flag) {
						Map<String, Object> msgDetail = new HashMap<String, Object>();
						msgDetail.put("dealNo", sendForeignFxmmModel.getDealNo());
						msgDetail.put("execID", sendForeignFxmmModel.getExecID());
						msgDetail.put("confirmID", sendForeignFxmmModel.getConfirmID());
						msgDetail.put("br", sendForeignFxmmModel.getBr());
						msgDetail.put("sendState", "103");
						msgDetail.put("tradeDate", tradeDate);
						msgDetail.put("marketIndicator", marketIndicator);
						msgDetail.put("execType","0"); //成交状态0-正常(外汇),4-撤销
						msgDetail.put("affirmStatus", "INI");//校验状态，默认是会员上传，目的防止接收报文有误导致无线定时发送确认。
						List<TdQryMsg> recList = tdQryMsgMapper.searchQryMsg(msgDetail);
						if (!(recList!=null && recList.size()>0)) {
							tdQryMsgMapper.insert(msgDetail);
							success++;
						}
					}
				}
			}else if("12".equals(marketIndicator)||"14".equals(marketIndicator)){
				//外汇即期，外汇远期
				List<SendForeignForwardSpotModel> forwardSpotModels;
				//注释外汇即期：买入和卖出金额需要根据交易方向转换，此转换放到查询方法中
				if("12".equals(marketIndicator)){
					forwardSpotModels = cpisMessageMapper.searchForeignSptMessage(dateMap);
				}else{
					forwardSpotModels = cpisMessageMapper.searchForeignFwdMessage(dateMap);
					if("1".equals(sendAgain) && forwardSpotModels.size()==0){
						forwardSpotModels = cpisMessageMapper.searchForeignSptMessage(dateMap);
					}
				}
				for (SendForeignForwardSpotModel sendForeignForwardSpotModel : forwardSpotModels) {
					params.put("ticketId", sendForeignForwardSpotModel.getDealNo());
					Map<String, String> typeMap1 = new HashMap<String, String>();
					typeMap1.put("opBank_type", "110");//开户行名称
					typeMap1.put("reAccount_type", "15");//收款行账号
					typeMap1.put("reBank_type", "23");//收款行名称
					typeMap1.put("reBicCode_type", "16");//收款行BICCODE
					List<AgencyBasicInformationModel> list1 = new ArrayList<AgencyBasicInformationModel>();//本方机构清算信息
					List<AgencyBasicInformationModel> list2 = new ArrayList<AgencyBasicInformationModel>();//对手方机构清算信息
					//卖出币种为外币时对手方清算信息处理
					String soldCurrency = sendForeignForwardSpotModel.getCurrency2();//卖出币种
					List<TtFxBasic> fxlist2;
					if("12".equals(marketIndicator)){
						fxlist2 = ttFxBasicMapper.searchMyInstBasicFXSPTOpponet(params);
					}else{
						fxlist2 = ttFxBasicMapper.searchMyInstBasicFXFWDOpponet(params);
					}
					if (!"CNY".equals(soldCurrency)) {
						list2 = fun1(list2, fxlist2, typeMap);
					}else {//人民币
						list2 = fun1(list2, fxlist2, typeMap1);
					}
					//买入币种为人民币时清算机构信息处理
					List<TtFxBasic> fxlist1;
					String packmsg="";//添加其他信息
					if("12".equals(marketIndicator)){
						packmsg = ttFxBasicMapper.searchMyInstBasicFXSPTOther(params);
						fxlist1 = ttFxBasicMapper.searchMyInstBasicFXSPT(params);
					}else{
						packmsg = ttFxBasicMapper.searchMyInstBasicFXFWDOther(params);
						fxlist1 = ttFxBasicMapper.searchMyInstBasicFXFWD(params);
					}
					if ("CNY".equals(sendForeignForwardSpotModel.getCurrency1())) {
						list1 = fun1(list1, fxlist1, typeMap1);
					}else {
						list1 = fun1(list1, fxlist1, typeMap);
					}
					try {
						Map<String, Object> msgDetail1 = new HashMap<String, Object>();
						msgDetail1.put("dealNo", sendForeignForwardSpotModel.getDealNo());
						List<TdQryMsg> recList1 = tdQryMsgMapper.searchQryMsg(msgDetail1);
						String str1="124,139";
						String str2="124,145";
						if (recList1!=null && recList1.size()>0) {
							if("1".equals(recList1.get(0).getJobnum())){
								str1="124,145";
								str2="124,139";
							}
						}
						list1=funOther(list1,packmsg,myInst,sendForeignForwardSpotModel.getPartyCFETSNO(),str1,marketIndicator);
						list2=funOther(list2,packmsg,sendForeignForwardSpotModel.getPartyCFETSNO(),myInst,str2,marketIndicator);
					} catch (Exception e) {
						e.printStackTrace();
					}
					sendForeignForwardSpotModel.setList1(list1);
					sendForeignForwardSpotModel.setList2(list2);

					if("P".equals(sendForeignForwardSpotModel.getSide()))

					sendForeignForwardSpotModel.setMarketIndicator(marketIndicator);
					sendForeignForwardSpotModel.setExecID(sendForeignForwardSpotModel.getExecId());
					sendForeignForwardSpotModel.setPartyID_2(sendForeignForwardSpotModel.getPartyCFETSNO());

					boolean flag = iImixCPISManageServer.confirmCPISTransaction(sendForeignForwardSpotModel);
					if (flag) {
						Map<String, Object> msgDetail = new HashMap<String, Object>();
						msgDetail.put("dealNo", sendForeignForwardSpotModel.getDealNo());
						msgDetail.put("execID", sendForeignForwardSpotModel.getExecID());
						msgDetail.put("confirmID", sendForeignForwardSpotModel.getConfirmID());
						msgDetail.put("br", sendForeignForwardSpotModel.getBr());
						msgDetail.put("sendState", "103");
						msgDetail.put("tradeDate", tradeDate);
						msgDetail.put("execType","F"); //成交状态F-正常(外汇),4-撤销
						msgDetail.put("marketIndicator", marketIndicator);
						msgDetail.put("affirmStatus", "INI");//校验状态，默认是会员上传，目的防止接收报文有误导致无线定时发送确认。
						List<TdQryMsg> recList = tdQryMsgMapper.searchQryMsg(msgDetail);
						if (!(recList!=null && recList.size()>0)) {
							tdQryMsgMapper.insert(msgDetail);
							success++;
						}
					}
				}
			}else if("11".equals(marketIndicator)){
				//外汇掉期
				List<SendForeignSwapModel> sendForeignSwapModels = cpisMessageMapper.searchForeignSwapMessage(dateMap);
				for (SendForeignSwapModel sendForeignSwapModel : sendForeignSwapModels) {
					params.put("ticketId", sendForeignSwapModel.getDealNo());


					TradeSettlsBean tradeSettlsBean = tradeSettlsService.getTradeSettls(params);
					tradeSettlsBean.setFullName("");//本方中文全称
					tradeSettlsBean.setcFullName("");//对手方中文全称
					sendForeignSwapModel.setTradeSettlsBean(tradeSettlsBean);

					sendForeignSwapModel.setMarketIndicator(marketIndicator);
					sendForeignSwapModel.setExecID(sendForeignSwapModel.getExecId());
					sendForeignSwapModel.setPartyID_2(sendForeignSwapModel.getPartyID_2());

					boolean flag = iImixCPISManageServer.confirmCPISTransaction(sendForeignSwapModel);
					if (flag) {
						Map<String, Object> msgDetail = new HashMap<String, Object>();
						msgDetail.put("dealNo", sendForeignSwapModel.getDealNo());
						msgDetail.put("execID", sendForeignSwapModel.getExecID());
						msgDetail.put("confirmID", sendForeignSwapModel.getConfirmID());
						msgDetail.put("br", sendForeignSwapModel.getBr());
						msgDetail.put("sendState", "103");
						msgDetail.put("tradeDate", tradeDate);
						msgDetail.put("execType","F"); //成交状态F-正常(外汇),4-撤销
						msgDetail.put("marketIndicator", marketIndicator);
						msgDetail.put("affirmStatus", "INI");//校验状态，默认是会员上传，目的防止接收报文有误导致无线定时发送确认。
						List<TdQryMsg> recList = tdQryMsgMapper.searchQryMsg(msgDetail);
						if (!(recList!=null && recList.size()>0)) {
							tdQryMsgMapper.insert(msgDetail);
							success++;
						}
					}
				}
			} else{
				promptMsg="不在确认报文之内";
			}
		} catch (Exception e) {
			e.printStackTrace();
			sb.append("报文发送失败");
		} finally {
			
		}
		return RetMsgHelper.ok(sb.toString());
	}

	/**
	 * 报文查询
	 * @param params
	 * @return
	 * @throws AWTException
	 */
	@ResponseBody
	@RequestMapping(value = "/searchQryConfirmMsg")
	public RetMsg<Serializable> searchQryConfirmMsg(@RequestBody  Map<String, Object> params) throws AWTException {
		IImixCPISManageServer iImixCPISManageServer = SpringContextHolder.getBean("IImixCPISManageServer");

		//发送查询报文
		QueryModel queryModel = new QueryModel();
		BeanUtil.populate(queryModel, params);
		String queryRequestId = "" + System.currentTimeMillis();
		queryModel.setQueryRequestID(queryRequestId);
		queryModel.setQueryStartNumber("0");
		boolean status = iImixCPISManageServer.confirmCPISDetail(queryModel);
		RetMsg<Serializable> ret = new RetMsg<Serializable>(RetMsgHelper.codeOk, "发送查询报文成功!");
		if (!status)  {
			ret = new RetMsg<Serializable>(RetMsgHelper.codeOk, "发送查询报文失败!");
		}
		return ret;
	}
//	
//	private static List<AgencyBasicInformationModel> fun(List<AgencyBasicInformationModel> basicList,String swiftText, String str, String code1, String code2){
//		String resStr = SwiftMessageUtil.fun(swiftText, str);
//		AgencyBasicInformationModel basicModel;
//		if (resStr != null && StringUtil.isNotEmpty(resStr)) {
//			resStr = resStr.replace("\r", "");
//			if (resStr.contains("\n")) {
//				String[] strs = resStr.split("\n");
//				if (strs[0] != null && !strs[0].equals("")) {
//					basicModel = new AgencyBasicInformationModel();
//					basicModel.setPartySubID(strs[0]);
//					basicModel.setPartySubIDType(code1);
//					basicList.add(basicModel);
//				}
//				if (strs[1] != null && !strs[1].equals("")) {
//					basicModel = new AgencyBasicInformationModel();
//					basicModel.setPartySubID(strs[1]);
//					basicModel.setPartySubIDType(code2);
//					basicList.add(basicModel);
//				}
//			}else {
//				basicModel = new AgencyBasicInformationModel();
//				basicModel.setPartySubID(resStr);
//				basicModel.setPartySubIDType(code2);
//				basicList.add(basicModel);
//			}
//		}
//		return basicList;
//	}
	
	
	/**
	 * 
	 * @param basiclist
	 * @param param
	 * @param codeMap
	 * @return
	 */
	private List<AgencyBasicInformationModel> fun1(List<AgencyBasicInformationModel> basiclist, List<TtFxBasic> fxlist,
			Map<String, String> codeMap){
		if (fxlist != null && fxlist.size() > 0) {
			for (TtFxBasic ttFxBasic : fxlist) {
				if (StringUtil.isNotEmpty(ttFxBasic.getInterAccount())) {
					AgencyBasicInformationModel informationModel = new AgencyBasicInformationModel();
					informationModel.setPartySubID(ttFxBasic.getInterAccount());
					informationModel.setPartySubIDType(codeMap.get("interAccount_type"));
					basiclist.add(informationModel);
				}
				if (StringUtil.isNotEmpty(ttFxBasic.getInterBank())) {
					AgencyBasicInformationModel informationModel = new AgencyBasicInformationModel();
					informationModel.setPartySubID(ttFxBasic.getInterBank());
					informationModel.setPartySubIDType(codeMap.get("interBank_type"));
					basiclist.add(informationModel);
				}
				if (StringUtil.isNotEmpty(ttFxBasic.getInterBicCode())) {
					AgencyBasicInformationModel informationModel = new AgencyBasicInformationModel();
					informationModel.setPartySubID(ttFxBasic.getInterBicCode());
					informationModel.setPartySubIDType(codeMap.get("interBicCode_type"));
					basiclist.add(informationModel);
				}
				if (StringUtil.isNotEmpty(ttFxBasic.getOpAccount())) {
					AgencyBasicInformationModel informationModel = new AgencyBasicInformationModel();
					informationModel.setPartySubID(ttFxBasic.getOpAccount());
					informationModel.setPartySubIDType(codeMap.get("opAccount_type"));
					basiclist.add(informationModel);
				}
				if (StringUtil.isNotEmpty(ttFxBasic.getOpBank())) {
					AgencyBasicInformationModel informationModel = new AgencyBasicInformationModel();
					informationModel.setPartySubID(ttFxBasic.getOpBank());
					informationModel.setPartySubIDType(codeMap.get("opBank_type"));
					basiclist.add(informationModel);
				}
				if (StringUtil.isNotEmpty(ttFxBasic.getOpBicCode())) {
					AgencyBasicInformationModel informationModel = new AgencyBasicInformationModel();
					informationModel.setPartySubID(ttFxBasic.getOpBicCode());
					informationModel.setPartySubIDType(codeMap.get("opBicCode_type"));
					basiclist.add(informationModel);
				}
				if (StringUtil.isNotEmpty(ttFxBasic.getReAccount())) {
					AgencyBasicInformationModel informationModel = new AgencyBasicInformationModel();
					informationModel.setPartySubID(ttFxBasic.getReAccount());
					informationModel.setPartySubIDType(codeMap.get("reAccount_type"));
					basiclist.add(informationModel);
				}
				if (StringUtil.isNotEmpty(ttFxBasic.getReBank())) {
					AgencyBasicInformationModel informationModel = new AgencyBasicInformationModel();
					informationModel.setPartySubID(ttFxBasic.getReBank());
					informationModel.setPartySubIDType(codeMap.get("reBank_type"));
					basiclist.add(informationModel);
				}
				if (StringUtil.isNotEmpty(ttFxBasic.getReBicCode())) {
					AgencyBasicInformationModel informationModel = new AgencyBasicInformationModel();
					informationModel.setPartySubID(ttFxBasic.getReBicCode());
					informationModel.setPartySubIDType(codeMap.get("reBicCode_type"));
					basiclist.add(informationModel);
				}
			}
		}
		return basiclist;
	}
	private List<AgencyBasicInformationModel> funOther(List<AgencyBasicInformationModel> basiclist,
			String packmsg,String myInst,String operInst,String values,String marketIndicator){
		int myInt = packmsg.indexOf(myInst);
		JY.require(myInt>-1,"本方机构21位机构码+"+myInst+"错误");
		int operInt = packmsg.indexOf(operInst);
		JY.require(operInt>-1,"对手方机构21位机构码"+operInst+"错误");
		if(myInt<operInt){
			packmsg=packmsg.substring(myInt,operInt);
		}else{
			packmsg=packmsg.substring(myInt);
		}
		String[] valuesStr = values.split(",");//eg:124,5,139
		for(int i=0;i<valuesStr.length;i++){
			int valueInt = packmsg.indexOf("803="+valuesStr[i]);
			if(valueInt!=-1){
				String valueMsg= packmsg.substring(0,valueInt);
				valueMsg = valueMsg.substring(valueMsg.lastIndexOf("523=")+4);
				AgencyBasicInformationModel informationModel = new AgencyBasicInformationModel();
				informationModel.setPartySubID(valueMsg);
				informationModel.setPartySubIDType(valuesStr[i]);
				basiclist.add(informationModel);
			}
		}
//		if("21".contains(marketIndicator)){
//			//外币拆借
//			//TODO 外币拆借机构中文名称需要本地维护
//			IfsOpicsCust cust= ifsOpicsCustMapper.searchByCfetsno(myInst);//myInst相对本方：赋值泰隆即泰隆21机构码，赋值其它银行，值为其他银行21机构码
//			if(cust!=null){
//				AgencyBasicInformationModel informationModel = new AgencyBasicInformationModel();
//				informationModel.setPartySubID(cust.getCliname());
//				informationModel.setPartySubIDType("124");
//				basiclist.add(informationModel);
//			}
//		}
		return basiclist;
	}
}
