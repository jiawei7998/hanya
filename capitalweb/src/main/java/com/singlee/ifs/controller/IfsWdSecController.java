package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.base.util.FileManage;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.UploadedFile;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.hrbextra.wind.mapper.CmFincomeMapper;
import com.singlee.hrbextra.wind.model.CmFincome;
import com.singlee.ifs.mapper.IbFundPriceDayMapper;
import com.singlee.ifs.mapper.IfsWdFundAllMapper;
import com.singlee.ifs.mapper.IfsWdFundMapper;
import com.singlee.ifs.model.*;
import com.singlee.ifs.service.IfsWdFundAllService;
import com.singlee.ifs.service.IfsWdFundService;
import com.singlee.ifs.service.IfsWdSecService;
import com.singlee.refund.mapper.CFtFundaccountInfoMapper;
import com.singlee.refund.mapper.CFtInfoMapper;
import com.singlee.refund.mapper.FtFundManagerMapper;
import com.singlee.refund.model.CFtFundaccountInfo;
import com.singlee.refund.model.CFtInfo;
import com.singlee.refund.model.FtFundManager;
import jxl.Sheet;
import jxl.Workbook;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.*;

@Controller
@RequestMapping(value = "/IfsWdSecController")
public class IfsWdSecController extends CommonController{
	@Autowired
	private IfsWdSecService ifsWdSecService;
	
	@Autowired
	IfsWdFundService ifsWdFundService;
	@Autowired
	IfsWdFundAllService ifsWdFundAllService;
	
	@Autowired
	IfsWdFundAllMapper ifsWdFundAllMapper;
	
	@Autowired
	CFtInfoMapper  cFtInfoMapper;
	@Autowired
	CFtFundaccountInfoMapper cFtFundaccountInfoMapper;
	
	@Autowired
	IfsWdFundMapper ifsWdFundMapper;
	
	@Autowired
	CmFincomeMapper cmFincomeMapper;
	
	@Autowired
	IbFundPriceDayMapper ibFundPriceDayMapper;

    @Autowired
    FtFundManagerMapper ftFundManagerMapper;
	/***
	 * 债券信息 分页查询 
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageWdSec")
	public RetMsg<PageInfo<IfsWdSec>> searchPageWdSec(@RequestBody Map<String, Object> map) {
		Page<IfsWdSec> page = ifsWdSecService.getWdSecList(map);
		return RetMsgHelper.ok(page);
	}

	/***
	 * 债券信息映射 分页查询 
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageWdOpicsSecmap")
	public RetMsg<PageInfo<IfsWdOpicsSecmap>> searchPageWdOpicsSecmap(@RequestBody Map<String, Object> map) {
		Page<IfsWdOpicsSecmap> page = ifsWdSecService.getWdOpicsSecmapList(map);
		return RetMsgHelper.ok(page);
	}
	
	
	/**
	 * 新增/修改
	 */
	@ResponseBody
	@RequestMapping(value = "/saveWdOpicsMap")
	public RetMsg<Serializable> saveWdOpicsMap(@RequestBody IfsWdOpicsSecmap entity) {
		String mapid = entity.getMapid();
		if (null == mapid || "".equals(mapid)) {// 新增
			ifsWdSecService.insert(entity);
		} else {// 修改
			ifsWdSecService.updateById(entity);
		}
		return RetMsgHelper.ok();
	}
	
	/**
	 * 删除
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteWdOpicsMap")
	public RetMsg<Serializable> deleteWdOpicsMap(@RequestBody IfsWdOpicsSecmap entity) {
		String mapid = entity.getMapid();
		ifsWdSecService.delete(mapid);
		return RetMsgHelper.ok();
	}
	
	
	/***
	 * 发行人信息 分页查询 
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageWdSeccust")
	public RetMsg<PageInfo<IfsWdSeccust>> searchPageWdSeccust(@RequestBody Map<String, Object> map) {
		Page<IfsWdSeccust> page = ifsWdSecService.getWdSeccustList(map);
		return RetMsgHelper.ok(page);
	}
	
	   /**
     * 哈尔滨  新万得
     * 债券信息 分页查询 
     * @param map
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/searchPageWdBond")
    public RetMsg<PageInfo<IfsWindBondBean>> searchPageWdBond(@RequestBody Map<String, Object> map) {
        Page<IfsWindBondBean> page = ifsWdSecService.getIfsWindBondList(map);
        return RetMsgHelper.ok(page);
    }
	
	
	/**
	 * 哈尔滨  新万得
	 * 利率信息 分页查询 
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageWdRate")
	public RetMsg<PageInfo<IfsWindRateBean>> searchPageWdRate(@RequestBody Map<String, Object> map) {
		Page<IfsWindRateBean> page = ifsWdSecService.getIfsWindRateList(map);
		return RetMsgHelper.ok(page);
	}
	
	/**
     * 哈尔滨  新万得
     * 收盘价信息 分页查询 
     * @param map
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/searchPageWdSecl")
    public RetMsg<PageInfo<IfsWindSeclBean>> searchPageWdSecl(@RequestBody Map<String, Object> map) {
        Page<IfsWindSeclBean> page = ifsWdSecService.getIfsWindSeclList(map);
        return RetMsgHelper.ok(page);
    }
    
    /**
     * 哈尔滨  新万得
     * 利率曲线 分页查询 
     * @param map
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/searchPageWdYc")
    public RetMsg<PageInfo<IfsWindYcBean>> searchPageWdYc(@RequestBody Map<String, Object> map) {
        Page<IfsWindYcBean> page = ifsWdSecService.getIfsWindYcList(map);
        return RetMsgHelper.ok(page);
    }
    
    /**
     * 哈尔滨  新万得
     * 债券额外信息 分页查询 
     * @param map
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/searchPageWdBondAdd")
    public RetMsg<PageInfo<IfsWindBondAddBean>> searchPageWdBondAdd(@RequestBody Map<String, Object> map) {
        Page<IfsWindBondAddBean> page = ifsWdSecService.getIfsWindBondAddList(map);
        return RetMsgHelper.ok(page);
    }
    
    /**
     * 哈尔滨  新万得
     * 债券评级 分页查询 
     * @param map
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/searchPageWdSecLevel")
    public RetMsg<PageInfo<IfsWindSecLevelBean>> searchPageWdSecLevel(@RequestBody Map<String, Object> map) {
        Page<IfsWindSecLevelBean> page = ifsWdSecService.getIfsWindSecLevelList(map);
        return RetMsgHelper.ok(page);
    }
    
    /**
     * 哈尔滨  新万得
     * 债券风险 分页查询 
     * @param map
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/searchPageWdRisk")
    public RetMsg<PageInfo<IfsWindRiskBean>> searchPageWdRisk(@RequestBody Map<String, Object> map) {
        Page<IfsWindRiskBean> page = ifsWdSecService.getIfsWindRiskList(map);
        return RetMsgHelper.ok(page);
    }
    
    /**
     * 哈尔滨  新万得
     * 基金基本信息 分页查询 
     * @param map
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/searchFundList")
    public RetMsg<PageInfo<ChinaMutualFundNAV>> searchFundList(@RequestBody Map<String, Object> map) {
        Page<ChinaMutualFundNAV> page = ifsWdFundService.getWdFundList(map);
        return RetMsgHelper.ok(page);
   }
    
    /**
     * 哈尔滨  新万得
     * 中国共同基金基本资料 分页查询 
     * @param map
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/searchFundAllList")
    public RetMsg<PageInfo<ChinaMutualFundDescription>> searchFundAllList(@RequestBody Map<String, Object> map) {
        Page<ChinaMutualFundDescription> page = ifsWdFundAllService.getWdFundAllList(map);
        return RetMsgHelper.ok(page);
    }
    
    /**
     * 哈尔滨  新万得
     * 基金信息同步（基金净值、基金信息、发行人）
     * @param map
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/updateFund")
    public RetMsg<Serializable> updateFund(@RequestBody Map<String, Object> map) {

        //先更新基金账号信息表(拿到所有的基金公司)
        List<String> compList=ifsWdFundAllMapper.getComp();
      Map<String, Object> repMap=new HashMap<>();
      
      CFtFundaccountInfo cFtFundaccountInfo=new CFtFundaccountInfo();
        for (String comp : compList) {
            repMap.put("managcompnm", comp);
           
            //查询基金账号是否存在
            CFtFundaccountInfo fundaccount=  cFtFundaccountInfoMapper.selectFund(repMap);
           if(fundaccount==null) {

               FtFundManager ftFundManager=new FtFundManager();
               //基金管理人名称
               ftFundManager.setManagcompnm(comp);
               //判断基金管理人信息是否存在
               FtFundManager FtFundManagers=ftFundManagerMapper.selectOne(ftFundManager);
               String ManagCompNm="";
               if(FtFundManagers!=null){
                   //基金管理人编号从已有的基金管理人信息里拿
                    ManagCompNm=FtFundManagers.getManagcomp();

               }else {
                   //序列生成的数字
                   ManagCompNm = cFtInfoMapper.getNm();
                   //基金管理人编号
                   ftFundManager.setManagcomp(ManagCompNm);
                   ftFundManagerMapper.insert(ftFundManager);
               }
               //基金账号表
               //基金管理人编码
               cFtFundaccountInfo.setManagcomp(ManagCompNm);
               //基金管理人名称
               cFtFundaccountInfo.setManagcompnm(comp);
               //插入基金账号表************************************************************************************
              cFtFundaccountInfoMapper.insert(cFtFundaccountInfo);

           }
        }
        
        //基金基本信息
        CFtInfo cFtInfo=new CFtInfo();
        IbFundPriceDay ibFundPriceDay=new IbFundPriceDay();
        CmFincome cmFincome=new CmFincome();
        
        List<ChinaMutualFundDescription> getFundAllList=ifsWdFundAllMapper.getFundAllList(map);
      
     for(ChinaMutualFundDescription  FundDescrip :getFundAllList ) {
         
         String fundcode=FundDescrip.getF_INFO_WINDCODE().substring(0,6);
         if("债券型".equals(FundDescrip.getF_INFO_FIRSTINVESTTYPE())|| "货币市场型".equals(FundDescrip.getF_INFO_FIRSTINVESTTYPE())) {
                   
         CFtInfo  Key=   cFtInfoMapper.selectByPrimaryKey(fundcode);
         if(Key==null){
             
         cFtInfo.setFundCode(fundcode);
         cFtInfo.setFundName(FundDescrip.getF_INFO_NAME());
         cFtInfo.setFundFullName(FundDescrip.getF_INFO_FULLNAME());
         // 基金类型
         if("债券型".equals(FundDescrip.getF_INFO_FIRSTINVESTTYPE())) {
             cFtInfo.setFundType("802");
             cFtInfo.setCurrencyType("2");
              
         }else if ("货币市场型".equals(FundDescrip.getF_INFO_FIRSTINVESTTYPE())) {
             cFtInfo.setFundType("801");
             cFtInfo.setCurrencyType("1");
        }
         // 投资目标分类
         cFtInfo.setInvType(FundDescrip.getF_INFO_TYPE());
          // 成立日期
         cFtInfo.setEstDate(FundDescrip.getF_INFO_SETUPDATE());
        //到期日期
         String maDate=FundDescrip.getF_INFO_MATURITYDATE()==null?"":FundDescrip.getF_INFO_MATURITYDATE();
         cFtInfo.setMaDate(maDate);
          // 存续期限 
//         Object o=FundDescrip.getF_INFO_PTMYEAR();
//         cFtInfo.setTerm(o.toString());
         // 管理人名称 
         cFtInfo.setManagCompNm( FundDescrip.getF_INFO_CORP_FUNDMANAGEMENTCOMP());
         repMap.put("managcompnm", FundDescrip.getF_INFO_CORP_FUNDMANAGEMENTCOMP().trim());
         CFtFundaccountInfo fundaccounts=  cFtFundaccountInfoMapper.selectFund(repMap);
         if(null!=fundaccounts) {
             String managComp=fundaccounts.getManagcomp();
             // 基金管理人编号
             cFtInfo.setManagComp(managComp);
         }
         else {
             JY.raise("请先维护基金管理人信息");
         }
      // 基金托管人
         cFtInfo.setCustodian(FundDescrip.getF_INFO_CUSTODIANBANK());
      // 基金经理
         cFtInfo.setManagerMen(FundDescrip.getF_INFO_CORP_FUNDMANAGEMENTID());
//      // 信息披露者
//         cFtInfo.setInfoDiscloser(FundDescrip);
//      // 发行方式
//         cFtInfo.setIssueType(FundDescrip);
//      // 发行协调人
//         cFtInfo.setDistCoordinator(FundDescrip.getF_ISSUE_TOTALUNIT());
//      // 基金总份额
//         cFtInfo.setTotalQty(FundDescrip.getF_ISSUE_TOTALUNIT());
         // 流通份额
         cFtInfo.setTrueQty(FundDescrip.getF_ISSUE_TOTALUNIT());
         // 币种代码
         cFtInfo.setCcy(FundDescrip.getCRNY_CODE());
      // 管理费率%
         cFtInfo.setManagRate(FundDescrip.getF_INFO_MANAGEMENTFEERATIO());
      // 托管费率%
         cFtInfo.setTrustRate(FundDescrip.getF_INFO_CUSTODIANFEERATIO());
      // 申购费率
      //   cFtInfo.setApplyRate(FundDescrip);
      // 赎回费率
    //     cFtInfo.setRedeemRate(FundDescrip);
      // 份额结转方式
//         cFtInfo.setCoverMode(FundDescrip);
//      // 份额结转日期
//         cFtInfo.setCoverDay(FundDescrip);
//      // 备注信息
//         cFtInfo.setRemark(FundDescrip);
//      // 基金经理联系方式
//         cFtInfo.setManagerMenPhone(FundDescrip);

//         cFtInfo.setPartyAcccode(FundDescrip);
//         cFtInfo.setPartyAccname(FundDescrip);
//         cFtInfo.setPartyBankcode(FundDescrip);
//         cFtInfo.setPartyBankname(FundDescrip);
      // 起息规则
//         cFtInfo.setvType(vType);
//         
//      // 分红方式
//         cFtInfo.setBonusWay(FundDescrip);
//         
//         cFtInfo.setBondType(FundDescrip);
//         cFtInfo.setManagCname(FundDescrip);
         
         //先查询 为null则新增
         String fundCode=cFtInfo.getFundCode();
         CFtInfo CFtInfo=  cFtInfoMapper.selectByPrimaryKey(fundCode);
//         cFtInfoMapper.deleteByPrimaryKey(fundCode);
         if(CFtInfo==null) {
             //插入基金信息表**********************************************************************************
             cFtInfoMapper.insert(cFtInfo); 
         }
         
         }
     }
    }
        return RetMsgHelper.ok();
    }


    /**
     * 基金信息模板导出
     */
    @ResponseBody
    @RequestMapping(value = "/cFundInfoExcel")
    public void cFundInfoExcel(HttpServletRequest request,
                                 HttpServletResponse response) {
        OutputStream out = null;
        FileInputStream in = null;

        try {
            String fileName = "基金信息模板";
            // 读取模板
            String excelPath = request.getSession().getServletContext().getRealPath("standard/Ifs/wind/基金信息.xls");

            fileName = URLEncoder.encode(fileName, "UTF-8");

            response.reset();
            // 追加时间
            response.addHeader("Content-Disposition", "attachment;filename="
                    + fileName + ".xls");
            response.setContentType("application/octet-stream;charset=UTF-8");

            out = response.getOutputStream();
            in = new FileInputStream(excelPath);

            byte[] b = new byte[1024];
            int len;

            while ((len = in.read(b)) > 0) {
                response.getOutputStream().write(b, 0, len);
            }
            out.flush();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != in) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                in = null;
            }
            if (null != out) {
                try {
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                out = null;
            }
        }

    }




    /**
     * 市场数据 Excel上传外汇远掉点信息
     */
    @ResponseBody
    @RequestMapping(value = "/ImportFundByExcel", method = RequestMethod.POST, produces = "text/plain;charset=UTF-8")
    public String ImportFundByExcel(MultipartHttpServletRequest request, HttpServletResponse response) {
        // 1.文件获取
        List<UploadedFile> uploadedFileList = null;
        String ret = "导入失败";
        Workbook wb = null;
        List<CFtInfo> fundBean = new ArrayList<>();
        try {
            uploadedFileList = FileManage.requestExtractor(request);
            // 2.校验并处理成excel
            List<Workbook> excelList = new LinkedList<Workbook>();
            for (UploadedFile uploadedFile : uploadedFileList) {
                if (!"xls".equals(uploadedFile.getType())) {
                    return ret = "上传文件类型错误,仅允许上传xls格式文件";
                }
                Workbook book = Workbook.getWorkbook(new ByteArrayInputStream(
                        uploadedFile.getBytes()));
                excelList.add(book);
            }
            if (excelList.size() != 1) {
                return ret = "只能处理一份excel";
            }
            wb = excelList.get(0);
            Sheet[] sheets = wb.getSheets();// 获得卡片sheet数量
            String postdate= DateUtil.getCurrentDateAsString("yyyy-MM-dd");
            for (Sheet sheet : sheets) {
                int rowCount = sheet.getRows();// 行数



                for (int i = 1; i < rowCount; i++) { // 从第二行开始
                    CFtInfo cFtInfo = new CFtInfo();

                    if(StringUtils.trimToEmpty(sheet.getCell(1, i).getContents()).equals("")){
                        continue;
                    }


                    cFtInfo.setFundCode(StringUtils.trimToEmpty(sheet
                            .getCell(0, i).getContents()));// A
                    cFtInfo.setFundName(StringUtils.trimToEmpty(sheet
                            .getCell(1, i).getContents()));// B
                    cFtInfo.setFundFullName(StringUtils.trimToEmpty(sheet
                            .getCell(2, i).getContents()));// C
                    cFtInfo.setFundType(StringUtils.trimToEmpty(sheet
                            .getCell(3, i).getContents()));// D
                    cFtInfo.setBonusWay(StringUtils.trimToEmpty(sheet
                            .getCell(4, i).getContents()));// E
                    cFtInfo.setCcy(StringUtils.trimToEmpty(sheet
                            .getCell(5, i).getContents()));// F
                    cFtInfo.setvType(StringUtils.trimToEmpty(sheet
                            .getCell(6, i).getContents()));// G
                    cFtInfo.setManagerMen(StringUtils.trimToEmpty(sheet
                            .getCell(7, i).getContents()));// H
                    cFtInfo.setManagerMenPhone(StringUtils.trimToEmpty(sheet
                            .getCell(8, i).getContents()));// I
                    cFtInfo.setCoverMode(StringUtils.trimToEmpty(sheet
                            .getCell(9, i).getContents()));// J
                    cFtInfo.setCoverDay(StringUtils.trimToEmpty(sheet
                            .getCell(10, i).getContents()));// K
                    cFtInfo.setManagComp(StringUtils.trimToEmpty(sheet
                            .getCell(11, i).getContents()));// L
                    cFtInfo.setManagCompNm(StringUtils.trimToEmpty(sheet
                            .getCell(12, i).getContents()));// M
                    cFtInfo.setPartyAcccode(StringUtils.trimToEmpty(sheet
                            .getCell(13, i).getContents()));// N
                    cFtInfo.setPartyAccname(StringUtils.trimToEmpty(sheet
                            .getCell(14, i).getContents()));// O
                    cFtInfo.setPartyBankcode(StringUtils.trimToEmpty(sheet
                            .getCell(15, i).getContents()));// P
                    cFtInfo.setPartyBankname(StringUtils.trimToEmpty(sheet
                            .getCell(16, i).getContents()));// Q

                    cFtInfo.setRiskType(StringUtils.trimToEmpty(sheet
                            .getCell(17, i).getContents()));// R
                    cFtInfo.setInvType(StringUtils.trimToEmpty(sheet
                            .getCell(18, i).getContents()));// S
                    cFtInfo.setEstDate(StringUtils.trimToEmpty(sheet
                            .getCell(19, i).getContents()));// T
                    cFtInfo.setCustodian(StringUtils.trimToEmpty(sheet
                            .getCell(20, i).getContents()));// U
                    cFtInfo.setMaDate(StringUtils.trimToEmpty(sheet
                            .getCell(21, i).getContents()));// V
                    cFtInfo.setIssueType(StringUtils.trimToEmpty(sheet
                            .getCell(22, i).getContents()));// W
                    cFtInfo.setInfoDiscloser(StringUtils.trimToEmpty(sheet
                            .getCell(23, i).getContents()));// X
                    cFtInfo.setTotalQty(new BigDecimal(StringUtils.trimToEmpty(sheet
                            .getCell(24, i).getContents())));// Y
                    cFtInfo.setDistCoordinator(StringUtils.trimToEmpty(sheet
                            .getCell(25, i).getContents()));// Z
                    cFtInfo.setManagRate(new BigDecimal(StringUtils.trimToEmpty(sheet
                            .getCell(26, i).getContents())));// AA
                    cFtInfo.setTrueQty(new BigDecimal(StringUtils.trimToEmpty(sheet
                            .getCell(27, i).getContents())));// AB
                    cFtInfo.setApplyRate(new BigDecimal(StringUtils.trimToEmpty(sheet
                            .getCell(28, i).getContents())));// AC
                    cFtInfo.setTrustRate(new BigDecimal(StringUtils.trimToEmpty(sheet
                            .getCell(29, i).getContents())));// AD
                    cFtInfo.setRedeemRate(new BigDecimal(StringUtils.trimToEmpty(sheet
                            .getCell(30, i).getContents())));// AE
                    cFtInfo.setTerm(StringUtils.trimToEmpty(sheet
                            .getCell(31, i).getContents()));// AF
//                    cFtInfo.setRemark(StringUtils.trimToEmpty(sheet
//                            .getCell(32, i).getContents()));// AG

                    fundBean.add(cFtInfo);
                }
            }
            //插入基金信息表
           ret = ifsWdFundService.doFundExcelToData(fundBean);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RException(e);
        } finally {
            if (null != wb) {
                wb.close();
            }
        }
        return ret;// 返回前台展示配置
    }









}
