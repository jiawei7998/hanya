package com.singlee.ifs.controller;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.read.listener.PageReadListener;
import com.github.pagehelper.Page;
import com.singlee.capital.base.util.FileManage;
import com.singlee.capital.chois.util.ZhongZhaiUtil;
import com.singlee.capital.choistrd.model.OpicsSecm;
import com.singlee.capital.choistrd.model.Secl;
import com.singlee.capital.choistrd.model.getSecl;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.UploadedFile;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.PropertiesUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.hrbextra.wind.WindSeclService;
import com.singlee.hrbreport.util.HrbReportUtils;
import com.singlee.ifs.mapper.IfsIntfcIselMapper;
import com.singlee.ifs.model.IfsIntfcIselBean;
import com.singlee.ifs.service.IfsIntfcIselService;

import jxl.Sheet;
import jxl.Workbook;

@Controller
@RequestMapping(value = "/IfsIntfcIselController")
public class IfsIntfcIselController extends CommonController{
	@Autowired
	IfsIntfcIselService ifsIntfcIselService;
	@Autowired
	IfsIntfcIselMapper ifsIntfcIselMapper;
	private static Logger logManager = LoggerFactory.getLogger(IfsIntfcIselController.class);
	
	private ZhongZhaiUtil zhongZhaiUtil;
	/***
     * 收盘价信息分页查询
     */
    @ResponseBody
    @RequestMapping(value = "/searchIsel")
    public RetMsg<PageInfo<IfsIntfcIselBean>> searchIrev(@RequestBody Map<String, Object> map) {

        List<IfsIntfcIselBean> list = ifsIntfcIselService.getlistIsel(map);
        int pageNumber = (int) map.get("pageNumber");
        int pageSize = (int) map.get("pageSize");
        Page<IfsIntfcIselBean> page = HrbReportUtils.producePage(list, pageNumber, pageSize);

        return RetMsgHelper.ok(page);
    }
    
    /**
     * 市场数据 Excel上传外汇远掉点信息
     */
    @ResponseBody
    @RequestMapping(value = "/ImportIselByExcel", method = RequestMethod.POST, produces = "text/plain;charset=UTF-8")
    public String ImportIselByExcel(MultipartHttpServletRequest request, HttpServletResponse response) {
    	// 1.文件获取
        List<UploadedFile> uploadedFileList = null;
        String ret = "导入失败";
        Workbook wb = null;
        List<IfsIntfcIselBean> irevBean = new ArrayList<>();
        try {
            uploadedFileList = FileManage.requestExtractor(request);
            // 2.校验并处理成excel
            List<Workbook> excelList = new LinkedList<Workbook>();
            for (UploadedFile uploadedFile : uploadedFileList) {
                if (!"xls".equals(uploadedFile.getType())) {
                    return ret = "上传文件类型错误,仅允许上传xls格式文件";
                }
                Workbook book = Workbook.getWorkbook(new ByteArrayInputStream(
                        uploadedFile.getBytes()));
                excelList.add(book);
            }
            if (excelList.size() != 1) {
                return ret = "只能处理一份excel";
            }
            wb = excelList.get(0);
            Sheet sheet = wb.getSheet(0);// 获得第一个sheet页
            String lstmntdte= DateUtil.getCurrentDateAsString("yyyy-MM-dd");
            
            int rowCount = sheet.getRows();// 行数
            IfsIntfcIselBean IfsIntfcIselbean = null;

            for (int i = 1; i <= rowCount-1; i++) { // rows=6开始 第七行开始读取，第一行为列名，第二行为汇率类型
            	IfsIntfcIselbean = new IfsIntfcIselBean();                    
                
            	IfsIntfcIselbean.setBr("01");
            	IfsIntfcIselbean.setDescr(StringUtils.trimToEmpty(sheet.getCell(0, i).getContents()));//A
            	IfsIntfcIselbean.setSecid(StringUtils.trimToEmpty(sheet.getCell(1, i).getContents()));//B
            	IfsIntfcIselbean.setClsgprice_8(StringUtils.trimToEmpty(sheet.getCell(2, i).getContents()));//C
            	IfsIntfcIselbean.setAskprice_8("0");
            	IfsIntfcIselbean.setBidprice_8("0");
            	IfsIntfcIselbean.setRemark("");
            	IfsIntfcIselbean.setLstmntdte(StringUtils.trimToEmpty(lstmntdte));
                
                
                irevBean.add(IfsIntfcIselbean);
            }
            ret = ifsIntfcIselService.doBussExcelPropertyToDataBase(irevBean);
            //插入inth表和isel
            String flag=  ifsIntfcIselService.dataBaseToIsel(lstmntdte);
            if("SUCCESS".equals(flag)){
                ret=ret+"且当天债券收盘价数据同步至OPICS成功";
            }else {
                ret=ret+"且当天债券收盘价数据同步至OPICS失败";
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw new RException(e);
        } finally {
            if (null != wb) {
                wb.close();
            }
        }
        return ret;// 返回前台展示配置
    }

    
    /**
     * 债券收盘价自动导入  
     * lizg
     * 债券估值.xlsx
     * @throws Exception 
     */
   @ResponseBody
   @RequestMapping(value = "/ImportIselByExcelByZz")
   public String ImportIselByExcelByZz() throws Exception {
	//1.文件下载   
	   boolean flag =false;
	   String ret = "导入失败";
	   //flag=zhongZhaiUtil.sftp();
	   
	  // if(flag==true) {
		// 1.文件获取
		    Date date = new Date();
			SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
			SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			calendar.add(Calendar.DATE, -1);
			String sdate = null;
			sdate = format.format(calendar.getTime());
			String lstmntdte= DateUtil.getCurrentDateAsString("yyyy-MM-dd");
	           
			PropertiesConfiguration stdtmsgSysValue;
			stdtmsgSysValue = PropertiesUtil.parseFile("sftp.properties");
			String zsLocalPath=null;
			zsLocalPath= stdtmsgSysValue.getString("zsLocalPath");
			
			File file = new File(zsLocalPath + "//"+sdate+"//" + "zhongzhaiguzhi"+sdate+".xlsx");
			file.setReadable(true);
			file.setWritable(true);
			if (!file.exists()) {
				logManager.info("========" + file + ":文件不存在=========");
	            
			}else {
				 
			   
			       List<IfsIntfcIselBean> irevBean = new ArrayList<>();
			       try {
			           
			           // 2.校验并处理成excel
			          
			           InputStream is = new FileInputStream(file);
			           
			           EasyExcel.read(file, getSecl.class, new PageReadListener<getSecl>(dataList -> {
			        	   IfsIntfcIselBean IfsIntfcIselbean = null;
					        String secid=null;
					        String secid1=null;
					        String clsgprice=null;
					        String clsgprice1=null;
					        String dcq=null;//代偿期
					        String dcq1=null;//代偿期     
					        Map<String, Object> map =new HashMap<String, Object>();
			        	   for (getSecl demoData : dataList) {
			               	//System.out.println("读取到一条数据{}"+ JSON.toJSONString(demoData));
			               	
				           	IfsIntfcIselbean = new IfsIntfcIselBean();                    
				               if(StringUtils.trimToEmpty(demoData.getLpath()).equals("银行间债券市场")) {
				            	   System.out.println("债券号:"+ demoData.getSecid());
					                secid=StringUtils.trimToEmpty(demoData.getSecid());
						        	clsgprice=StringUtils.trimToEmpty(demoData.getClprice());
						        	   
				            	   
				            	   IfsIntfcIselbean.setBr("01");
				                  	IfsIntfcIselbean.setDescr(StringUtils.trimToEmpty(demoData.getSecname()));
				                  	IfsIntfcIselbean.setSecid(StringUtils.trimToEmpty(demoData.getSecid()));
				                  	IfsIntfcIselbean.setClsgprice_8(clsgprice);
				                  	IfsIntfcIselbean.setAskprice_8("0");
				                  	IfsIntfcIselbean.setBidprice_8("0");
				                  	IfsIntfcIselbean.setRemark("");
				                  	IfsIntfcIselbean.setLstmntdte(StringUtils.trimToEmpty(lstmntdte));  
				                      irevBean.add(IfsIntfcIselbean);
				                     
				                      map.put("secid", secid);
				                      List<OpicsSecm> secls=ifsIntfcIselService.selectByPrimaryKey(map);
				                      if(secls.size()>0) {
				                    	  if (ifsIntfcIselMapper.selectIntfcIsel(IfsIntfcIselbean) != null){
				                      		//若存在，先删除再添加
				                      		
				                      		ifsIntfcIselMapper.deleteIntfcIsel(IfsIntfcIselbean);
				                      	}
				                      	ifsIntfcIselMapper.insertIntfcIsel(IfsIntfcIselbean);
				                      }		                      
				                     
			               }
			              }
			           })).sheet().doRead();
			           
			          
			          
			           //插入inth表和isel
			           String flag1=  ifsIntfcIselService.dataBaseToIsel(lstmntdte);
			           if("SUCCESS".equals(flag1)){
			               ret=ret+"且当天债券收盘价数据同步至OPICS成功";
			           }else {
			               ret=ret+"且当天债券收盘价数据同步至OPICS失败";
			           }

			       } catch (Exception e) {
			           e.printStackTrace();
			           throw new RException(e);
			       } finally {
			           
			       }
			}
	      
	     
	          
	           
	  // }
	   
   	
       return ret;// 返回前台展示配置
   }

}
