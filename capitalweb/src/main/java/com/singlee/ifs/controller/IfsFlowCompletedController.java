package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.ifs.model.IfsFlowCompleted;
import com.singlee.ifs.service.IfsFlowCompletedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * 查询流程审批完成Controller
 * 
 * ClassName: IfsFlowCompletedController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason: TODO ADD REASON(可选). <br/>
 * date: 2018-5-30 下午07:54:52 <br/>
 * 
 * @author shenzl
 * @version
 * @since JDK 1.6
 */
@Controller
@RequestMapping(value = "/IfsFlowCompletedController")
public class IfsFlowCompletedController {
	@Autowired
	IfsFlowCompletedService ifsFlowCompletedService;

	/***
	 * 分页查询 "审批完成"的记录
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageFlowCompleted")
	public RetMsg<PageInfo<IfsFlowCompleted>> searchPageFlowCompleted(@RequestBody Map<String, Object> map) {
		String approveStatusNo = ParameterUtil.getString(map, "approveStatus", "");
		if (StringUtil.isNullOrEmpty(approveStatusNo)) {
			approveStatusNo = DictConstants.ApproveStatus.ApprovedPass;
			approveStatusNo += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatusNo += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatusNo += "," + DictConstants.ApproveStatus.Verified;
			approveStatusNo += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			approveStatusNo += "," + DictConstants.ApproveStatus.Approving;
			String[] orderStatus = approveStatusNo.split(",");
			map.put("approveStatusNo", orderStatus);
		}
		
		Page<IfsFlowCompleted> page = ifsFlowCompletedService.searchPageFlowCompleted(map);
		return RetMsgHelper.ok(page);
	}

}
