package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.ifs.model.IfsQuota;
import com.singlee.ifs.model.IfsQuotaKey;
import com.singlee.ifs.service.IfsQuotaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.Map;

/**
 * 用户限额
 * 
 * @author
 *
 */
@Controller
@RequestMapping(value = "/IfsQuotaController")
public class IfsQuotaController extends CommonController {

	@Autowired
	private IfsQuotaService ifsQuotaService;

	/**
	 * 分页查询
	 * 
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchIfsQuotaList")
	public RetMsg<PageInfo<IfsQuota>> searchIfsQuotaList(@RequestBody Map<String, Object> params) {

		Page<IfsQuota> page = ifsQuotaService.searchIfsQuotaList(params);
		return RetMsgHelper.ok(page);

	}

	/**
	 * 保存或者修改
	 * 
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/saveOrUpdateIfsQuota")
	public RetMsg<Serializable> saveOrUpdateIfsQuota(@RequestBody Map<String, Object> map) {
		IfsQuota ifsQuota = new IfsQuota();
		BeanUtil.populate(ifsQuota, map);
		int result = ifsQuotaService.saveOrUpdateIfsQuota(ifsQuota);
		if (result > 0) {
			return RetMsgHelper.ok(true);
		} else {
			return RetMsgHelper.ok(false);
		}
	}

	/**
	 * 删除
	 * 
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteIfsQuota")
	public RetMsg<Serializable> deleteIfsQuota(@RequestBody Map<String, Object> params) {

		IfsQuotaKey key = new IfsQuotaKey();
		key.setUserId((String) params.get("userId"));
		key.setProduct((String) params.get("product"));
		key.setProdType((String) params.get("prodType"));
		key.setCost((String) params.get("cost"));

		int result = ifsQuotaService.deleteIfsQuota(key);
		if (result > 0) {
			return RetMsgHelper.ok();
		} else {
			return RetMsgHelper.simple("error.common.0001", "操作失败！");
		}

	}

}
