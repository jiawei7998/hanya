package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.mapper.TaSysParamMapper;
import com.singlee.capital.system.model.TaSysParam;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.bean.SlNupdBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.bean.SlSettleBean;
import com.singlee.financial.esb.dlcb.ISlCnapsServer;
import com.singlee.financial.esb.dlcb.ISlNupdServer;
import com.singlee.financial.esb.hbcb.bean.common.ReqMsgHead;
import com.singlee.financial.esb.hbcb.bean.common.RequestHeader;
import com.singlee.financial.esb.hbcb.model.EsbOutBean;
import com.singlee.financial.esb.hbcb.model.MS5702Bean;
import com.singlee.financial.esb.hbcb.service.S003003990MS5702Service;
import com.singlee.ifs.mapper.IfsEsbMassageMapper;
import com.singlee.ifs.mapper.IfsTrdSettleMapper;
import com.singlee.ifs.model.IfsEsbMassage;
import com.singlee.ifs.model.IfsTrdSettle;
import com.singlee.ifs.service.IfsTrdSettleService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import tk.mybatis.mapper.entity.Example;

import java.io.IOException;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 清算信息Controller
 * 
 * ClassName: IfsTrdSettleControllers <br/>
 * date: 2018-5-30 下午07:52:39 <br/>
 * 
 * @author yubao
 * @version
 * @since JDK 1.6
 */
@Controller
@RequestMapping(value = "/TrdSettleController")
public class IfsTrdSettleControllers extends CommonController {

	@Autowired
	private IfsTrdSettleService trdsettleservice;

	@Autowired
	ISlCnapsServer iSlCnapsServer;
	
	@Autowired
	ISlNupdServer iSlNupdServer;
	
	@Autowired
	private IfsTrdSettleMapper trdsettlemapper;
	
	@Autowired
	private IfsEsbMassageMapper ifsEsbMassageMapper;
	
	@Autowired
	S003003990MS5702Service s003003990MS5702Service;
	
	@Autowired
	private TaSysParamMapper taSysParamMapper;

	/**
	 * 查询待经办结算主指令信息
	 * 
	 * map内参数：
	 * 
	 * @return
	 * @throws IOException
	 * @throws Exception
	 */

	@ResponseBody
	@RequestMapping(value = "/searchSubmitSettleInstList")
	public RetMsg<PageInfo<IfsTrdSettle>> searchSubmitSettleInstList(
			@RequestBody Map<String, Object> param) {
		Map<String, Object> map = new HashMap<String, Object>();
		String dealNo_s = ParameterUtil.getString(param, "dealNo_s", "");
		String cname = ParameterUtil.getString(param, "cname", "");
		String product_s = ParameterUtil.getString(param, "product_s", "");
//		String vdate = ParameterUtil.getString(param, "vdate", ""); // 结算日期
		String startDate = ParameterUtil.getString(param, "startDate", ""); //
		String endDate = ParameterUtil.getString(param, "endDate", ""); //
		String pageSize = ParameterUtil.getString(param, "pageSize", null);
		String pageNumber = ParameterUtil.getString(param, "pageNumber", null);
		//自贸区改造加br
		String br = ParameterUtil.getString(param, "br", null);
		
		map.put("dealno", dealNo_s);
		map.put("cname", cname);
		map.put("prodtype", product_s);
		map.put("startDate", startDate);
		map.put("endDate", endDate);
		map.put("pageSize", pageSize);
		map.put("pageNumber", pageNumber);
		map.put("dealflag", "0");
		map.put("br", br);
		Page<IfsTrdSettle> page = trdsettleservice.getTrdSettleList(map);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 提交经办
	 * 
	 * map内参数：
	 * 
	 * @return
	 * @throws IOException
	 * @throws Exception
	 */
	@RequestMapping(value = "/submitSettle")
	@ResponseBody
	public RetMsg<Serializable> submitSettle(
			@RequestBody Map<String, Object> params) {
		try {
			return trdsettleservice.submitSettle(params);
		} catch (Exception e) {
			// log.error("验证大额支付信复核状态出错", e);
			return RetMsgHelper.simple("999999", "经办出错:" + e.getMessage());
		}
	}

	/**
	 * 提交签发
	 * 
	 * map内参数：
	 * 
	 * @return
	 * @throws IOException
	 * @throws Exception
	 */
	@RequestMapping(value = "/verifySettle")
	@ResponseBody
	public RetMsg<Serializable> verifySettle(
			@RequestBody HashMap<String, Object> param) {
		RetMsg<Serializable> ret = new RetMsg<Serializable>(
				RetMsgHelper.codeOk, "复核成功");
		try {
			IfsTrdSettle trdSttle = trdsettleservice
					.queryTrdSettleByDealNo(param);
			if (StringUtils.isBlank(trdSttle.getPayUserid())
					|| StringUtils.isBlank(trdSttle.getPayUsername())
					|| StringUtils.isBlank(trdSttle.getPayBankid())
					|| StringUtils.isBlank(trdSttle.getPayBankname())
					|| StringUtils.isBlank(trdSttle.getRecUserid())
					|| StringUtils.isBlank(trdSttle.getRecUsername())
					|| StringUtils.isBlank(trdSttle.getPayBankid())
					|| StringUtils.isBlank(trdSttle.getRecBankname())) {
				ret.setCode("999999");
				ret.setDesc("您所需要确认的往账交易信息:<br>账号:" + trdSttle.getRecUserid()
						+ "<br>户名:" + trdSttle.getRecUsername() + "<br>行号:"
						+ trdSttle.getRecUserid() + "<br>行名:"
						+ trdSttle.getRecBankname()
						+ "<br><html><font color=red>金额:"
						+ trdSttle.getAmount().doubleValue()
						+ "</font></html><br>清算信息空缺！");
				return ret;
			}

		} catch (Exception e) {
			return RetMsgHelper.simple("000010", "复核出错:" + e.getMessage());
		}
		IfsTrdSettle trdSettle1 = new IfsTrdSettle();
		trdSettle1.setDealno(param.get("dealno").toString());
		trdSettle1.setDealflag("A");
		trdSettle1.setVoidflag("0");
		trdSettle1.setSettflag("0");
		trdSettle1.setDealuser(SlSessionHelper.getUserId());
		trdSettle1.setDealtime(DateUtil.getCurrentDateTimeAsString());
		try {
			RetMsg<Serializable> retmsg = trdsettleservice
					.validSettleState(trdSettle1);
			if (RetMsgHelper.isOk(retmsg) == false) {
				return retmsg;
			}
			trdSettle1 = (IfsTrdSettle) retmsg.getObj();
			trdSettle1.setVoper(SlSessionHelper.getUserId());

			trdSettle1.setVtime(DateUtil.getCurrentDateTimeAsString());
		} catch (Exception e) {
			trdSettle1.setRetcode("000020");
			trdSettle1.setRetmsg("复核出错:" + e.getMessage());
			trdSettle1.setDealflag("C");
		}

		trdSettle1.setDealflag("F");
		trdSettle1.setVoidflag("0");
		trdSettle1.setSettflag("1");
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("dealno", trdSettle1.getDealno());
		params.put("dealflag", trdSettle1.getDealflag());
		params.put("voidflag", trdSettle1.getVoidflag());
		params.put("settflag", trdSettle1.getSettflag());
		params.put("voper", trdSettle1.getVoper());
		params.put("vtime", trdSettle1.getVtime());
		try {
			if (trdsettleservice.updateTrdSettle(params) > 0) {
				trdSettle1.setRetcode(RetMsgHelper.codeOk);
				trdSettle1.setRetmsg("您所确认签发的往账交易信息:<br>账号:"
						+ trdSettle1.getRecUserid() + "<br>户名:"
						+ trdSettle1.getRecUsername() + "<br>行号:"
						+ trdSettle1.getRecBankid() + "<br>行名:"
						+ trdSettle1.getRecBankname()
						+ "<br><html><font color=red>金额:"
						+ trdSettle1.getAmount().abs().doubleValue()
						+ "</font></html><br>签发处理成功，请稍后查询业务状态！");
				trdSettle1.setDealflag("F");
			} else {
				trdSettle1.setRetcode("999999");
				trdSettle1.setRetmsg("前置返回状态异常,请与管理员确认！");
				trdSettle1.setDealflag("C");
			}
		} catch (Exception e) {
			trdSettle1.setRetcode("000070");
			trdSettle1.setDealflag("C");
		}

		return ret;
	}

	/**
	 * 查询待复核结算主指令信息
	 * 
	 * map内参数：
	 * 
	 * @return
	 * @throws IOException
	 * @throws Exception
	 */
	@RequestMapping(value = "/searchVerifySettleInstList")
	@ResponseBody
	public RetMsg<PageInfo<IfsTrdSettle>> searchVerifySettleInstList(@RequestBody HashMap<String, Object> paramMap) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String dealNo_s = ParameterUtil.getString(paramMap, "dealo_s", "");
		String cname = ParameterUtil.getString(paramMap, "cname", "");
		String product_s = ParameterUtil.getString(paramMap, "product_s", "");
		String dealstatus = ParameterUtil.getString(paramMap, "dealstatus", "");
		//String signDate = ParameterUtil.getString(paramMap, "signDate", ""); // 签发日期
		int searchType = ParameterUtil.getInt(paramMap, "searchType", -1);
		String pageSize = ParameterUtil.getString(paramMap, "pageSize", null);
		String pageNumber = ParameterUtil.getString(paramMap, "pageNumber",null);
		String startDate=ParameterUtil.getString(paramMap, "startDate",null);//结算日期区间
		String endDate=ParameterUtil.getString(paramMap, "endDate",null);
		//自贸区改造加br
		String br = ParameterUtil.getString(paramMap, "br", null);
		map.put("dealno", dealNo_s);
		map.put("cname", cname);
		map.put("prodtype", product_s);
		map.put("dealstatus", dealstatus);
		//map.put("signDate", signDate);
		map.put("searchType", searchType);
		map.put("pageSize", pageSize);
		map.put("pageNumber", pageNumber);
		map.put("startDate", startDate);
		map.put("endDate", endDate);
		map.put("br", br);
		String approveType = ParameterUtil.getString(paramMap, "approveType", "");//处理状态
		String status = "";
		if("0".equals(approveType)){ //待处理
			status = "1";
		} else { //已处理
			status = "PR07";
		}
		map.put("settflag", status);
		map.put("dealflag", "1");//已经办
		Page<IfsTrdSettle> retList = trdsettleservice.getTrdSettleList(map);
		return RetMsgHelper.ok(retList);
	}

	@RequestMapping(value = "/queryTdTrdSettleByDealNo")
	@ResponseBody
	public RetMsg<IfsTrdSettle> queryTdTrdSettleByDealNo(@RequestBody HashMap<String, Object> paramMap) throws Exception {
		IfsTrdSettle tdTrdSettle = trdsettleservice.queryTrdSettleByDealNo(paramMap);
		return RetMsgHelper.ok(tdTrdSettle);
	}

	/**
	 * 分页查询IfsTrdSettle
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/queryAllTrdSettle")
	@ResponseBody
	public RetMsg<PageInfo<IfsTrdSettle>> queryAllTrdSettle(@RequestBody HashMap<String, Object> paramMap) throws Exception {
		Page<IfsTrdSettle> page= trdsettleservice.queryAllTrdSettle(paramMap);
		return RetMsgHelper.ok(page);
	}
	/**
	 * 查询rmb支出
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/queryRMBTrdSettle")
	@ResponseBody
	public RetMsg<PageInfo<IfsTrdSettle>> queryRMBTrdSettle(@RequestBody HashMap<String, Object> paramMap) throws Exception {
		Page<IfsTrdSettle> page= trdsettleservice.queryRMBTrdSettle(paramMap);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 查询外币收取
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/queryFXTrdSettle")
	@ResponseBody
	public RetMsg<PageInfo<IfsTrdSettle>> queryFXTrdSettle(@RequestBody HashMap<String, Object> paramMap) throws Exception {
		Page<IfsTrdSettle> page= trdsettleservice.queryFXTrdSettle(paramMap);
		return RetMsgHelper.ok(page);
	}


	@RequestMapping(value = "/backSettle")
	@ResponseBody
	public RetMsg<Serializable> backSettle(@RequestBody HashMap<String, Object> paramMap) {
		try {
			return trdsettleservice.backSettle(paramMap);
		} catch (Exception e) {
			// log.error("验证大额支付信复核状态出错", e);
			return RetMsgHelper.simple("999999", "经办出错:" + e.getMessage());
		}
	}

	/**
	 * 签发
	 * @param param
	 * @return
	 */
	@RequestMapping(value = "/sendSettle")
	@ResponseBody
	public RetMsg<Serializable> sendSettle(@RequestBody HashMap<String, Object> param) {
		RetMsg<Serializable> ret = new RetMsg<Serializable>(RetMsgHelper.codeOk, "签发成功");
		try {
			IfsTrdSettle trdSettle = trdsettleservice.queryTrdSettleByDealNo(param);
			
			if("PR07".equals(trdSettle.getSettflag())){
				ret.setDesc("该笔业务已被处理");
				return ret;
			}
			//超时查询，为true则不发，false则发送
			boolean flag=trdsettleservice.sendCheck(param);
			JY.info("这笔交易的状态为（）："+flag+"······");
	        System.out.println(flag);
			if(flag==false) {

			MS5702Bean ms5702Bean = new MS5702Bean();
			RequestHeader requestHeader =new RequestHeader();
			ReqMsgHead reqMsgHead = new ReqMsgHead();
			com.singlee.financial.esb.hbcb.bean.s003003990MS5702.RequestBody requestBody = new com.singlee.financial.esb.hbcb.bean.s003003990MS5702.RequestBody(); 
			
			Map<String, Object> sysMap = new HashMap<String, Object>();
			sysMap.put("p_type", "ESBMS5702");
			List<TaSysParam> sysList = taSysParamMapper.selectTaSysParam(sysMap);
			sysMap.put("p_type", "ESB");
			List<TaSysParam> sysListCopy = taSysParamMapper.selectTaSysParam(sysMap);
			sysList.addAll(sysListCopy);
			for (TaSysParam taSysParam : sysList) {
				if("ChnlNo".equals(taSysParam.getP_code())) {
					requestHeader.setChnlNo(taSysParam.getP_value().trim());//渠道号
					reqMsgHead.setChanid(taSysParam.getP_value().trim());//发起渠道
				}else if("TlrNo".equals(taSysParam.getP_code())) {
					requestHeader.setTlrNo(taSysParam.getP_value().trim());//柜员号
					reqMsgHead.setTlno(taSysParam.getP_value().trim());//受理柜员
				}else if("Syscd".equals(taSysParam.getP_code())) {
					requestBody.setSyscd(taSysParam.getP_value().trim());//通道
				}else if("Procuctflag".equals(taSysParam.getP_code())) {
					requestBody.setProcuctflag(taSysParam.getP_value().trim());//强制执行标志 大额通道关闭时是否继续交易
				}else if("Paytyp".equals(taSysParam.getP_code())) {
					requestBody.setPaytyp(taSysParam.getP_value().trim());//支付类型 区别客户发起和金融机构发起
				}else if("Feemod".equals(taSysParam.getP_code())) {
					requestBody.setFeemod(taSysParam.getP_value().trim());//手续费收费方式
				}else if("Priority".equals(taSysParam.getP_code())) {
					requestBody.setPriority(taSysParam.getP_value().trim());//优先级
				}else if("Ctgypurpcd".equals(taSysParam.getP_code())) {
					requestBody.setCtgypurpcd(taSysParam.getP_value().trim());//业务种类
				}else if("Actflg".equals(taSysParam.getP_code())) {
					requestBody.setActflg(taSysParam.getP_value().trim());//现转标识
				}
			}
			
			requestHeader.setReqTm(new SimpleDateFormat("yyyyMMdd").format(new Date()));//请求方交易时间戳
			requestHeader.setReqSeqNo(trdSettle.getDealno());//请求方流水号
			requestHeader.setBrchNo("00100");//机构号
			requestBody.setTxtpcd(trdSettle.getHvpType());//业务类型(10-现金汇款,11-普通汇兑,12-网银支付)
			requestBody.setTrftrno(trdSettle.getPayUserid());//转账账号 现金交易时，上送0
			requestBody.setTrftrnm(trdSettle.getPayUsername());//转账户名 现金交易时，上送0
			requestBody.setDbtrno(trdSettle.getPayUserid());//付款账户账号 付款人账号 现金交易时不上送值
			requestBody.setDbtrnm(trdSettle.getPayUsername());//付款账户名称 付款人名称 现金交易时不上送值
			requestBody.setCdtrno(trdSettle.getRecUserid());//收款账户账号 收款人账号
			requestBody.setCdtrnm(trdSettle.getRecUsername());//收款账户名称 收款人名称
			requestBody.setCdtbranchid(trdSettle.getRecBankid());//收款行行号
			requestBody.setCcy(trdSettle.getCcy());//币种
			requestBody.setAmt(new DecimalFormat("0.00").format(trdSettle.getAmount()));//交易金额 金额
			reqMsgHead.setChandt(trdSettle.getCdate().substring(0,10).replace("-", ""));//原渠道日期
			reqMsgHead.setChanflow(StringUtils.replace(UUID.randomUUID().toString(), "-", ""));//渠道流水号
			reqMsgHead.setBizdate(trdSettle.getCdate().substring(0,10).replace("-", ""));//交易日期
			reqMsgHead.setSbno("0" + trdSettle.getBr() + "00");// 交易发起机构号
			
			ms5702Bean.setReqMsgHead(reqMsgHead);
			ms5702Bean.setRequestBody(requestBody);
			ms5702Bean.setRequestHeader(requestHeader);
			EsbOutBean outBean = s003003990MS5702Service.send(ms5702Bean);
			//保存报文
			IfsEsbMassage ifsEsbMassage = new IfsEsbMassage();
			ifsEsbMassage.setDealno(trdSettle.getFedealno().trim());
			ifsEsbMassage.setCustno(trdSettle.getCno().trim());
			ifsEsbMassage.setProduct(trdSettle.getProduct().trim());
			ifsEsbMassage.setProducttype(trdSettle.getProdtype().trim());
			List<IfsEsbMassage> select = ifsEsbMassageMapper.select(ifsEsbMassage);
			if(select.size() == 1) {
				ifsEsbMassage = select.get(0);
				ifsEsbMassage.setSenddate(new Date());
				ifsEsbMassage.setRecdate(new Date());
				ifsEsbMassage.setSendmsg(outBean.getSendmsg());
				ifsEsbMassage.setRecmsg(outBean.getRecmsg());
				ifsEsbMassage.setRescode(outBean.getRetCode());
				ifsEsbMassage.setResmsg(outBean.getRetMsg());
				Example example = new Example(IfsEsbMassage.class);
				example.createCriteria().andEqualTo("dealno",ifsEsbMassage.getDealno())
										.andEqualTo("custno",ifsEsbMassage.getCustno())
										.andEqualTo("product",ifsEsbMassage.getProduct())
										.andEqualTo("producttype",ifsEsbMassage.getProducttype())
										.andEqualTo("service",ifsEsbMassage.getService());
				ifsEsbMassageMapper.updateByExample(ifsEsbMassage, example);
			}else if(select.size() == 0) {
				ifsEsbMassage.setAmount(trdSettle.getAmount().toString());
				ifsEsbMassage.setCcy(trdSettle.getCcy());
				ifsEsbMassage.setSetmeans("CNAPS");
				ifsEsbMassage.setService("S003003990MS5702");
				ifsEsbMassage.setSenddate(new Date());
				ifsEsbMassage.setRecdate(new Date());
				ifsEsbMassage.setSendmsg(outBean.getSendmsg());
				ifsEsbMassage.setRecmsg(outBean.getRecmsg());
				ifsEsbMassage.setRescode(outBean.getRetCode());
				ifsEsbMassage.setResmsg(outBean.getRetMsg());
				ifsEsbMassageMapper.insert(ifsEsbMassage);
			}
			
			ret.setCode(outBean.getRetCode());
			ret.setDesc(outBean.getRetMsg());
			
			Map<String,Object> map = new HashMap<String,Object>();
			String interfaceno = outBean.getClientNo(); // 二代返回查询交易流水号（用于查询交易）
			String tellseqno = outBean.getTellSeqNo(); // 二代返回核心流水号（用于核心账务查询）
			
			map.put("fedealno", ParameterUtil.getString(param,"fedealno",""));
			map.put("product", ParameterUtil.getString(param,"product",""));
			map.put("prodtype", ParameterUtil.getString(param,"prodtype",""));
			map.put("payrecind", ParameterUtil.getString(param,"payrecind",""));
			if("003003AAAAAAA".equals(outBean.getRetCode())){ //签发成功
				map.put("setTFlag", "PR07"); //已处理
				Date day = new Date();
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String signDate = df.format(day);
				map.put("signDate", signDate); // 更新签发日期
				map.put("voper", SlSessionHelper.getUserId());
				map.put("vtime", signDate);
				trdsettlemapper.updateFlag(map); // 更新处理状态为已处理
			}
			
			//只要通讯，就更新流水号
			if(outBean.getRetMsg() != null || !"".equals(outBean.getRetMsg())){
				map.put("interfaceno", interfaceno);
				if(tellseqno != null && tellseqno !=""){
					map.put("tellseqno", tellseqno);
				} else {
					map.put("tellseqno", "");
				}
				map.put("note", outBean.getRetMsg());
				trdsettlemapper.updateSeqno(map); // 更新流水号
			}
			
			return ret;
		}
			else {
			    return RetMsgHelper.simple("000010", "该单号已发送:" );
            }
		} catch (Exception e) {
			return RetMsgHelper.simple("000010", "签发出错:" + e.getMessage());
		}
		
	}

	/**
	 * 交易状态查询
	 */
	@RequestMapping(value = "/querySettle")
	@ResponseBody
	public RetMsg<Serializable> querySettle(@RequestBody HashMap<String, Object> param) {
		RetMsg<Serializable> ret = new RetMsg<Serializable>(RetMsgHelper.codeOk, "查询成功");
		try {
			List<IfsTrdSettle> list=trdsettlemapper.getIfsTrdSettleList();
			if(list.size()==0||list==null){
				 ret.setDesc("没有需要更新处理状态的数据");
			}else{
				for (IfsTrdSettle ifsTrdSettle : list) {
					SlSettleBean settleBean = new SlSettleBean();
					settleBean.setVdate(ifsTrdSettle.getSignDate().substring(0,10).replace("-", ""));
					settleBean.setSeq(ifsTrdSettle.getInterfaceNo());
					//0829通过本行MQ机构代号对应opic部门数据字典，将server存为机构号
					settleBean.setServer(ifsTrdSettle.getServer());
					SlOutBean outBean = iSlCnapsServer.search(settleBean);
					/**
					 * 交易成功后，更新清算状态
					 */
					Map<String,Object> map = new HashMap<String, Object>();
					if("000000".equals(outBean.getRetCode())){ 
						String dealStatus = outBean.getClientNo(); // 交易状态 0-失败 （1）   1-成功 （0）  2 处理中（3）
						dealStatus="1".equals(dealStatus)?"0":"0".equals(dealStatus)?"1":"2".equals(dealStatus)?"3":dealStatus;
						map.put("dealStatus", dealStatus); // 将清算状态更新到IFS_CUST_SETTLE表中
					}
					map.put("fedealno", ifsTrdSettle.getFedealno());
					map.put("interfaceNo", ifsTrdSettle.getInterfaceNo());
					map.put("note",outBean.getRetMsg());
					trdsettlemapper.updateDealStatus(map);
				}
			}
			return ret;
		} catch (Exception e) {
			return RetMsgHelper.simple("000010", "查询出错:" + e.getMessage());
		}
	}
	
	/**
	 * 检查流水号是否为空
	 * @param param
	 * @return
	 */
	@RequestMapping(value = "/checkSettle")
	@ResponseBody
	public String checkSettle(@RequestBody HashMap<String, Object> map) {
		String bussno = "";
		if(map.get("fedealno") != null){
			if(trdsettlemapper.querySerialNo(map) == null){
				return bussno;
			} else {
				bussno = trdsettlemapper.querySerialNo(map);
			}
		} else {
			return "F"; // 若为1，则表明流水号为空，不能进行签发
		}
		return bussno;
	}
	/**
	 * 发送日间账务
	 * @param param
	 * @return
	 */
	@RequestMapping(value = "/sendAccount")
	@ResponseBody
	public RetMsg<Serializable> sendAccount(@RequestBody HashMap<String, Object> param) {
		RetMsg<Serializable> ret = new RetMsg<Serializable>(RetMsgHelper.codeOk, "签发成功");
		try {
			String fedealno=ParameterUtil.getString(param,"fedealno","");
			IfsTrdSettle trdSettle = trdsettleservice.queryTrdSettleByDealNo(param);
			SlNupdBean slNupdBeanP=new SlNupdBean();
			slNupdBeanP.setSetNo(trdSettle.getDealno());
			slNupdBeanP.setAccount(trdSettle.getPayUserid());
			slNupdBeanP.setCcyCode("156");
			slNupdBeanP.setAmount(String.valueOf(trdSettle.getAmount()));
			slNupdBeanP.setDrcrind("1");//借贷方向
			List<SlNupdBean> list=new ArrayList<SlNupdBean>();
			list.add(slNupdBeanP);//贷方
			SlNupdBean slNupdBeanR=new SlNupdBean();
			slNupdBeanR.setSetNo(trdSettle.getDealno());
			slNupdBeanR.setAccount(trdSettle.getRecUserid());
			slNupdBeanR.setCcyCode("156");
			slNupdBeanR.setAmount(String.valueOf(trdSettle.getAmount()));
			slNupdBeanR.setDrcrind("0");//借贷方向
			list.add(slNupdBeanR);//借方
			SlOutBean outBean=iSlNupdServer.nupdSend(list);
			//ret.setCode(outBean.getRetCode());
			ret.setDesc(outBean.getRetMsg());
			if("AAAAAAAAAA".equals(outBean.getRetCode())){ //记账成功
				Map<String,Object> map=new HashMap<String, Object>();
				map.put("accflag", "1"); //已记账
				map.put("fedealno", fedealno); 
				trdsettlemapper.updateAccFlag(map); // 更新处理状态为已记账
			}
			//只要通讯，就更新流水号
			//if(outBean.getRetMsg() != null || !"".equals(outBean.getRetMsg())){
				//map.put("interfaceno", interfaceno);
				//if(tellseqno != null && tellseqno !=""){
			//		map.put("tellseqno", tellseqno);
			//	} else {
			//		map.put("tellseqno", "");
			//	}
				//trdsettlemapper.updateSeqno(map); // 更新流水号
			//}
			return ret;
		} catch (Exception e) {
			return RetMsgHelper.simple("000010", "发送出错:" + e.getMessage());
		}
	}
	/**
	 * 大额清算（人员）交易统计报表
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchCnapsDealReport")
	public RetMsg<PageInfo<IfsTrdSettle>> searchCnapsDealReport(@RequestBody Map<String, Object> map){
		Page<IfsTrdSettle> page=trdsettleservice.getTrdSettleCount(map);
		return RetMsgHelper.ok(page);
	}
	
	// 查询
	@ResponseBody
	@RequestMapping(value = "/getSettleCnaps")
	public RetMsg<IfsTrdSettle> getSettleCnaps(@RequestBody Map<String, String> map) {
		String fedealno = map.get("fedealno");
		IfsTrdSettle page = trdsettleservice.getSettleCnaps(fedealno);
		return RetMsgHelper.ok(page);
	}

}