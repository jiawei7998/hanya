package com.singlee.ifs.controller;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.StringUtil;
import com.singlee.capital.base.service.TaMessageService;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.spring.json.FastJsonUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.eu.service.EdCustManangeService;
import com.singlee.capital.flow.service.FlowOtherService;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.util.DateUtil;
import com.singlee.hrbcap.util.TradeConstants;
import com.singlee.ifs.baseUtil.IfsFlowApprove;
import com.singlee.ifs.mapper.*;
import com.singlee.ifs.model.*;
import com.singlee.ifs.service.IfsOpicsBondService;
import com.singlee.ifs.service.IfsOpicsSettleManageService;
import com.singlee.refund.mapper.*;
import com.singlee.refund.model.*;
import com.singlee.slbpm.mapper.TtFlowSerialMapMapper;
import com.singlee.slbpm.pojo.bo.ProcDefInfo;
import com.singlee.slbpm.pojo.bo.TtFlowSerialMap;
import com.singlee.slbpm.service.*;
import org.activiti.engine.TaskService;
import org.activiti.engine.task.Task;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.io.Serializable;
import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 审批流程相关控制器
 *
 * ClassName: IfsFlowController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason: TODO ADD REASON(可选). <br/>
 * date: 2018-5-30 下午07:33:45 <br/>
 *
 * @author lijing
 * @version
 * @since JDK 1.6
 */
@Controller
@RequestMapping(value = "/IfsFlowController", method = RequestMethod.POST)
public class IfsFlowController extends CommonController {

	// 最大线程池数量=运行程序机器的2*cpu核心数+1
	private static final ExecutorService executorService = Executors.newFixedThreadPool(2*Runtime.getRuntime().availableProcessors()+1);
//	private ExecutorService executorService = Executors.newCachedThreadPool();
	private static final Logger logManager = LoggerFactory.getLogger(IfsFlowController.class);
	@Autowired
	IfsCfetsfxLendMapper ifsCfetsfxLendMapper;
	@Autowired
	IfsFlowMonitorMapper ifsFlowMonitorMapper;
	//人民币即期
	@Autowired
	IfsCfetsfxSptMapper ifsCfetsfxSptMapper;
	//人民币即期 事前
	@Autowired
	IfsApprovefxSptMapper ifsApprovefxSptMapper;
	//人民币远期
	@Autowired
	IfsCfetsfxFwdMapper ifsCfetsfxFwdMapper;
	//人命币外币掉期
	@Autowired
	IfsCfetsfxSwapMapper ifsCfetsfxSwapMapper;
	//人民币期权
	@Autowired
	IfsCfetsfxOptionMapper ifsCfetsfxOptionMapper;
	//利率互换
	@Autowired
	IfsCfetsrmbIrsMapper ifsCfetsrmbIrsMapper;
	//买断式回购
	@Autowired
	IfsCfetsrmbOrMapper ifsCfetsrmbOrMapper;
	//现劵买卖
	@Autowired
	IfsCfetsrmbCbtMapper ifsCfetsrmbCbtMapper;
	//债券远期
	@Autowired
	IfsCfetsrmbBondfwdMapper ifsCfetsrmbBondfwdMapper;
	//债券借贷
	@Autowired
	IfsCfetsrmbSlMapper ifsCfetsrmbSlMapper;
	@Autowired
	IfsCfetsrmbDetailSlMapper ifsCfetsrmbDetailSlMapper;
	//质押式回购
	@Autowired
	IfsCfetsrmbCrMapper ifsCfetsrmbCrMapper;
	@Autowired
	IfsCfetsrmbDetailCrMapper ifsCfetsrmbDetailCrMapper;
	//存单发行
	@Autowired
	IfsCfetsrmbDpMapper ifsCfetsrmbDpMapper;
	@Autowired
	IfsCfetsrmbDpOfferMapper ifsCfetsrmbDpOfferMapper;
	//黄金即远掉
	@Autowired
	IfsCfetsmetalGoldMapper ifsCfetsmetalGoldMapper;
	//同业存款
	@Autowired
	IfsApprovermbDepositMapper ifsApprovermbDepositMapper;
	//基金信息
	@Autowired
	CFtInfoMapper cFtInfoMapper;
	//同业存放(外币)
	@Autowired
	IfsFxDepositInMapper ifsFxDepositInMapper;
	//存放同业(外币)
	@Autowired
	IfsFxDepositoutMapper ifsFxDepositoutMapper;
	//同业存放
	@Autowired
	IfsRmbDepositinMapper ifsRmbDepositinMapper;
	//存放同业
	@Autowired
	IfsRmbDepositoutMapper ifsRmbDepositoutMapper;
	@Autowired
	IfsOpicsCustMapper ifsOpicsCustMapper;
	@Autowired
	IfsOpicsBondService ifsOpicsBondService;
	//用于校验Opics 清算路径
	@Autowired
	IfsOpicsSettleManageService ifsOpicsSettleManageService;
	//外汇冲销
	@Autowired
	IfsRevIfxdMapper ifsRevlfxdMapper;
	//回购冲销
	@Autowired
	IfsRevBredMapper ifsRevBredMapper;
	//互换冲销
	@Autowired
	IfsRevIswhMapper ifsRevIswhMapper;
	//债券冲销
	@Autowired
	IfsRevIsldMapper ifsRevIsldMapper;
	/** 审批流程定义接口 */
	@Autowired
	private FlowDefService flowDefService;
	/** 审批流程查询接口 */
	@Autowired
	private FlowQueryService flowQueryService;
	/** 审批流程日志接口 */
	@Autowired
	private FlowLogService flowLogService;
	/** 审批流程操作接口 */
	@Autowired
	private FlowOpService flowOpService;
	/** 审批流程其他接口 */
	@Autowired
	private FlowService flowService;
	/** Activiti任务接口 */
	@Autowired
	private TaskService taskService;
	/** capital内的流程附加接口service */
	@Autowired
	private FlowOtherService flowOtherService;
	/** 待办消息管理 */
	@Autowired
	private TaMessageService taMessageService;
	/** 流程和外部流水号关系Dao */
	@Autowired
	private TtFlowSerialMapMapper ttFlowSerialMapMapper;
	@Autowired
	private EdCustManangeService edCustManangeService;
	/** 流程审批前预检查 */
	@Autowired
	private IfsFlowApprove ifsFlowApprove;
	//信用拆借
	@Autowired
	private IfsCfetsrmbIboMapper cfetsrmbIboMapper;
	//债券借贷事前审批
	@Autowired
	private IfsApprovermbSlMapper  ifsapprovermbslmapper;
	//申购
	@Autowired
	private CFtAfpMapper cFtAfpMapper;
	//申购确认
	@Autowired
	private CFtAfpConfMapper cFtAfpConfMapper;
	//赎回
	@Autowired
	private CFtRdpMapper cFtRdpMapper;
	//赎回确认
	@Autowired
	private CFtRdpConfMapper cFtRdpConfMapper;
	//现金分红
	@Autowired
	private CFtReddMapper cFtReddMapper;
	//红利再投
	@Autowired
	private CFtReinMapper cFtReinMapper;
	//拆借冲销
	@Autowired
	private IfsRevIrvvMapper ifsRevIrvvMapper;

	/**
	 * 获取可用的流程列表，主要用于提交审批时获取能够选择的审批列表 map 参数： serial_no 外部序号【非空】通常为业务审批单号
	 * flow_type 流程类型【非空】
	 *
	 * @return
	 */
	@RequestMapping(value = "/getAvailableFlowList")
	@ResponseBody
	public List<ProcDefInfo> getAvailableFlowList(@RequestBody Map<String, String> paramMap) throws Exception {

		String serial_no = ParameterUtil.getString(paramMap, "serial_no", "");
		String flow_type = ParameterUtil.getString(paramMap, "flow_type", "");
		String product_type = ParameterUtil.getString(paramMap, "product_type", "");
		String flow_belong = "";
//		if (StringUtil.isEmpty(serial_no) || StringUtil.isEmpty(flow_type)) {
//			throw new RException("参数不能为空");
//		}
		flow_belong = SlSessionHelper.getInstitutionId();
		List<ProcDefInfo> list = new ArrayList<ProcDefInfo>();
		list = flowDefService.listProcDef("", flow_type, "1", product_type, flow_belong);
		return list;
	}

	/**
	 * 批量提交审批【支持多线程】
	 *
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/approveBatch")
	@ResponseBody
	public RetMsg<?> approveBatchCommit(@RequestBody Map<String, String> paramMap) {
		//批量提交条件
		List<IfsFlowApproveBean> approveBeanLst = JSON.parseArray(ParameterUtil.getString(paramMap, "batchParams", null), IfsFlowApproveBean.class);
		List<String> failNum = new ArrayList<String>();
		int jumpNum = 0;
		StringBuffer failMsg = new StringBuffer();
		// 循环处理
		//开启线程计数器
		final CountDownLatch downLatch = new CountDownLatch(approveBeanLst.size());
		String userId = SlSessionHelper.getUser().getUserId();//获取当前用户
		logManager.info(String.format("%s用户在%s操作了IfsFlowController.approveBatchCommit -> 批量提交审批【支持多线程】方法]",userId, DateUtil.getDateTime()));
		long start = System.currentTimeMillis();
		// 循环检查当前状态
		for (IfsFlowApproveBean approveBean : approveBeanLst) {
			if (!StringUtils.equals(approveBean.getOrderStatus(), "3")) {// 新建状态
				jumpNum += 1;
				continue;
			}
			Runnable runnable = new Runnable() {
				@Override
				public void run() {
					//设置线程优先级 高于默认JOB的线程
					Thread.currentThread().setPriority(6);
					JY.info(String.format("当前线程[%s],优先级[%s]：开始处理单号[%s]的交易,交易处理人[%s]....", Thread.currentThread().getName(),Thread.currentThread().getPriority(), approveBean.getSerialNo(),userId));
					try {
						// 查询是否处在已审批中
						List<Map<String, Object>> listLog = flowLogService.getFlowlogBySerialNo(approveBean.getSerialNo());
						if (null != listLog) {// 查询存在审批日志
							Map<String, Object> map = listLog.get(listLog.size() - 1);// 取最后一个
							String taskId = (String) map.get("TaskId");// 任务节点ID
							// 1.检查TASK任务是否存在
							Task task = taskService.createTaskQuery().taskId(taskId).singleResult();
							// 2.根据流程实例ID找到业务映射记录
							TtFlowSerialMap flowSerMap = ttFlowSerialMapMapper.get(null, approveBean.getSerialNo(), null,Long.valueOf(task.getProcessInstanceId()));
							String dealNo = flowSerMap.getSerial_no();// 业务编号
							// 3.检查交易对手、债券信息
							Map<String, Object> paramBeforeCheck = new HashMap<String, Object>();
							paramBeforeCheck.put("ticketId", dealNo);// ticketId 不会重复
							String approveBeforeCheckMsg = ifsFlowApprove.approveBeforeCheck(paramBeforeCheck);
							if (!"".equals(approveBeforeCheckMsg)) {
								JY.raise(approveBeforeCheckMsg);
							}
							// 4.判断当前是否是发起节点
							IfsFlowMonitor ifsFlowMonitor = ifsFlowMonitorMapper.getEntityById(dealNo);
							if (ifsFlowMonitor != null) {
								if (userId.equals(ifsFlowMonitor.getSponsor())) {
									// TODO 4.1重新发起的重新占用额度
									// TODO 4.2限额
								} else {
									// TODO 4.3正式交易将预占用转换成占用明细
									// TODO 4.4预占用先不用
									// String status = edCustManangeService.changeOccupyType(dealNo);
								}
							}
							// 流程审批操作,属于再次审批
							flowOpService.completePersonalTask(userId, taskId,approveBean.getSerialNo(), DictConstants.FlowCompleteType.Continue,"", "", "");
						} else {
							// 1.不存在审批日志查询流程，并获取默认流程
							List<ProcDefInfo> listProcDef = flowDefService.listProcDef("", approveBean.getFlowType(),DictConstants.YesNo.YES, approveBean.getProductType(),SlSessionHelper.getInstitutionId());
							if (null == listProcDef) {
								JY.raise("流程匹配失败.匹配到的审批流程有 多笔,请检查流程机构产品挂靠", "系统提示");
							}
							// 获取第一个流程Id
							String flowId = listProcDef.get(0).getFlow_id();
							// 2.提交前校验（交易对手信息，债券信息）
							Map<String, Object> paramBeforeCheck = new HashMap<String, Object>();
							paramBeforeCheck.put("ticketId", approveBean.getSerialNo());
							paramBeforeCheck.put("prdNo", approveBean.getProductType());
							String approveBeforeCheckMsg = ifsFlowApprove.approveBeforeCheck(paramBeforeCheck);
							if (!"".equals(approveBeforeCheckMsg)) {
								JY.raise(approveBeforeCheckMsg);
							}
							// TODO 3.产品统一占用额度接口
							// TODO 4.限额
							// TODO 5.敏感度限额
							// 6.提交流程
							flowOpService.startNewFlowInstance(userId, flowId,approveBean.getFlowType(), approveBean.getSerialNo(), "", false, false, "",approveBean.getCallBackService(), "", approveBean.getProductType());
							// 发送消息提醒【无用,已有monitor替代】
							/*
							Map<String, Object> messageMap = new HashMap<String, Object>();
							messageMap.put("callBackService", approveBean.getCallBackService());// 回调Service
							messageMap.put("serial_no", approveBean.getSerialNo());// 交易编号
							messageMap.put("flow_type", approveBean.getFlowType());// 流程类型
							messageMap.put("flow_id", approveBean.getFlowId());// 流程ID
							messageMap.put("user_id", SlSessionHelper.getUserId());
							messageMap.put("type", "approveCommit");
							taMessageService.insertTaMessage(messageMap);*/

						}
					} catch (Exception e) {
						failNum.add(approveBean.getSerialNo());
						failMsg.append("审批单号[" + approveBean.getSerialNo() + "]=>" +  (e.getCause() == null ? e.getMessage():e.getCause())  + "<br>");
						JY.error("审批失败,审批单号[" + approveBean.getSerialNo() + "]=><br>", e);
					}finally {
						//不关心成功与失败计数器减1
						downLatch.countDown();
					}
				}
			};
			executorService.execute(runnable);//执行线程
		}
		try {
			downLatch.await();//唤醒等待
		} catch (InterruptedException e) {
			JY.error("发起人提交审批异常InterruptedException", e);
		}
		long end = System.currentTimeMillis();
		return RetMsgHelper.ok(String.format("一共提交%s笔，其中成功%s笔，失败%s笔，跳过%s笔，消耗的时间为:%s（分钟）；<br> 处理信息：%s", approveBeanLst.size(),approveBeanLst.size() - failNum.size() , failNum.size(), jumpNum,(end-start)/1000/60, failMsg));
	}

	/**
	 * 发起审批【单独发起审批，不支持批量，已由/approveBatch替代】
	 *
	 * @param paramMap
	 *            map参数： serial_no 外部流水号【必输】 flow_type 流程类型【必输】 flow_id 流程id【必输】
	 *            字典项000043 审批流程类型 is_pass_with_pre_auth 业务审批是否使用预授权通过，0：否 1：是
	 * @return
	 * @throws Exception
	 */
	@Deprecated
	@RequestMapping(value = "/approveCommit")
	@ResponseBody
	public RetMsg<?> approveCommit(@RequestBody Map<String, String> paramMap) throws Exception {
		String serial_no = ParameterUtil.getString(paramMap, "serial_no", "");
		String flow_type = ParameterUtil.getString(paramMap, "flow_type", "");
		String flow_id = ParameterUtil.getString(paramMap, "flow_id", "");
		String specific_user = ParameterUtil.getString(paramMap, "specific_user", "");
		String approve_msg = ParameterUtil.getString(paramMap, "approve_msg", "");
		String user_id = this.getThreadLocalUserId();
		logManager.info(String.format("%s用户在%s操作了IfsFlowController.approveCommit -> 发起审批【单独发起审批，不支持批量，已由/approveBatch替代】,审批单号[%s]",user_id, DateUtil.getDateTime(),serial_no));
		String callBackFun = ParameterUtil.getString(paramMap, "callBackFun", "");
		String prd_no = ParameterUtil.getString(paramMap, "product_type", "");
		String limit_level = "";
		String callBackService = null;

		//提交前校验（交易对手信息，债券信息）
		Map<String,Object> paramBeforeCheck=new HashMap<String, Object>();
		paramBeforeCheck.put("ticketId", serial_no);
		paramBeforeCheck.put("prdNo", prd_no);
		paramBeforeCheck.put("flow_type", flow_type);
		String approveBeforeCheckMsg = approveBeforeCheck(paramBeforeCheck);
		if(!"".equals(approveBeforeCheckMsg)){
			JY.raise(approveBeforeCheckMsg);
		}

		//回调服务
		callBackService = ParameterUtil.getString(paramMap, "callBackService", callBackFun);

		// 提交流程
		flowOpService.startNewFlowInstance(user_id, flow_id, flow_type, serial_no, specific_user, false, false, limit_level, callBackService, approve_msg, prd_no);

		// 发送消息提醒[由ifs_monitor表替代]

		/*Map<String, Object> messageMap = new HashMap<String, Object>();
		messageMap.put("callBackService", callBackService);// 回调Service
		messageMap.put("serial_no", serial_no);// 交易编号
		messageMap.put("flow_type", flow_type);// 流程类型
		messageMap.put("flow_id", flow_id);// 流程ID
		messageMap.put("user_id", SlSessionHelper.getUserId());
		messageMap.put("type", "approveCommit");
		taMessageService.insertTaMessage(messageMap);*/


		return RetMsgHelper.ok();

	}

	/**
	 * 取审批日志
	 *
	 * @return map 参数： serial_no 外部流水号
	 * @throws IOException
	 * @throws Exception
	 */
	@RequestMapping(value = "/approveLog")
	@ResponseBody
	public RetMsg<List<Map<String, Object>>> approveLog(@RequestBody Map<String, String> paramMap) throws Exception {
		String serial_no = ParameterUtil.getString(paramMap, "serial_no", "");
		logManager.info(String.format("%s用户在%s操作了IfsFlowController.approveLog -> 取审批日志,审批单号[%s]",this.getThreadLocalUserId(), DateUtil.getDateTime(),serial_no));
//		if (StringUtil.isEmpty(serial_no)) {
//			throw new RException("查询日志出错参数不能为空");
//		}
		// 2017/6/14 - yangyang - 从流程引擎获取事件日志
		// List<ApproveLogVo> list = flowLogService.searchFlowLog("", "",serial_no, "", "", "",false);
		List<Map<String, Object>> list = flowLogService.getFlowlogBySerialNo(serial_no);
		return RetMsgHelper.ok(list);
	}

	/**
	 * 批量通过/拒绝审批【批量操作】
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/approveBatchPass")
	@ResponseBody
	public RetMsg<Serializable> approveBatchPass(@RequestBody Map<String, String> paramMap) throws Exception {
		String user_id = this.getThreadLocalUser().getUserId();
		logManager.info(String.format("%s用户在%s操作了IfsFlowController.approveBatchPass -> 批量通过/拒绝审批【批量操作】",user_id, DateUtil.getDateTime()));
		List<String> failNum = new ArrayList<String>();
		StringBuffer failMsg = new StringBuffer();
		// 批量通过提交条件
		List<IfsFlowApproveBean> approveBeanLst = JSON.parseArray(ParameterUtil.getString(paramMap, "batchParams", null), IfsFlowApproveBean.class);
		//开启线程计数器
		final CountDownLatch downLatch = new CountDownLatch(approveBeanLst.size());
		long start = System.currentTimeMillis();
		// 循环检查当前状态
		for (IfsFlowApproveBean approveBean : approveBeanLst) {

			Runnable runnable = new Runnable() {
				@Override
				public void run() {
					try {
						// 开启审批批量执行,设置线程优先级 高于默认JOB的线程
						Thread.currentThread().setPriority(6);
						JY.info(String.format("当前线程[%s],优先级[%s]：开始处理单号[%s]的交易....", Thread.currentThread().getName(),Thread.currentThread().getPriority(),approveBean.getSerialNo()));
						// 检查清算路径是否存在【复用了approveBean的属性，未增加新的属性】
						if (StringUtils.equals(approveBean.getOrderStatus(),"待结算") && StringUtils.equals(approveBean.getProductType(),"sl")){
							String retStr = ifsOpicsSettleManageService.checkSearchSettInfoAll(approveBean.getSerialNo(),approveBean.getProductType());
							if (StringUtils.isNotBlank(retStr)){//非空
								failMsg.append(retStr);//记录错误信息
								failNum.add(approveBean.getSerialNo());
								return ;
							}
						}
						// 1.1校验任务是否存在
						Task task = taskService.createTaskQuery().taskId(approveBean.getTaskId()).singleResult();
						// 1.2根据流程实例ID找到业务映射记录
						TtFlowSerialMap flowSerMap = ttFlowSerialMapMapper.get(null, approveBean.getSerialNo(), null, Long.valueOf(task.getProcessInstanceId()));
						String dealNo = flowSerMap.getSerial_no();
						// 1.3提交前校验（交易对手信息，债券信息）
						//提交前校验（交易对手信息，债券信息）
						if(!"99".equals(approveBean.getOpType())){//退回发起
							Map<String,Object> paramBeforeCheck=new HashMap<String, Object>();
							paramBeforeCheck.put("ticketId", dealNo);
							paramBeforeCheck.put("prdNo", flowSerMap.getPrd_no());
							paramBeforeCheck.put("flow_type", flowSerMap.getFlow_type());
							String approveBeforeCheckMsg = approveBeforeCheck(paramBeforeCheck);
							if(!"".equals(approveBeforeCheckMsg)){
								JY.raise(approveBeforeCheckMsg);
							}
						}
						//开始处理审批
						if ("2".equalsIgnoreCase(approveBean.getOpType())) {
							List<String> userList = Arrays.asList(approveBean.getSpecificUser().split(","));
							flowService.invitationWithTask(approveBean.getTaskId(), userList);
						} else {
							//泰隆的逻辑
							/*IfsFlowMonitor ifsFlowMonitor = ifsFlowMonitorMapper.getEntityById(dealNo);
							if(ifsFlowMonitor!=null){
								if( user_id.equals(ifsFlowMonitor.getSponsor())){
									//重新发起的重新占用额度
									List<EdInParams> creditList =  new ArrayList<EdInParams>();
									creditList = ifsCfetsfxLendMapper.searchAllInfo(dealNo);
									for(int i=0;i<creditList.size();i++){
										paramMap.put("product_type", creditList.get(i).getPrdNo());
									}
									paramMap.put("serial_no", dealNo);
									edCustManangeService.chooseOccupy(paramMap);
									//限额
									String prdNo = ParameterUtil.getString(paramMap, "product_type", "");//交易产品
									if("411".equals(prdNo)){
										//411外汇即期,391外汇远期,432外汇掉期,431货币掉期,440外汇对即期.433人民币期权TODO，需要的添加，并增加视图V_FOREIGNEXCHANGE_LIMIT
										paramMap.put("code", "1");//外汇限额
										edCustManangeService.limitForeignExchange(paramMap);
									}
									if("443".equals(prdNo)){
										//443现劵买卖，442买断式回购,446质押式回购,445债券借贷
										paramMap.put("code", "0");//债券限额

										String inFlag = "0";////作为判断标识符，为0时是审批台发起，为1为清算后发起
										edCustManangeService.limitBondExchange(paramMap,inFlag);
									}
								}else{
									//正式交易将预占用转换成占用明细
									//TODO预占用先不用
									//String status = edCustManangeService.changeOccupyType(dealNo);
								}
							}*/
							// 1.5审批完成
							flowOpService.completePersonalTask(user_id, approveBean.getTaskId(),approveBean.getSerialNo(), approveBean.getOpType(), approveBean.getSpecificUser(), approveBean.getMsg(), approveBean.getBackTo());
						}
					} catch (Exception e) {
						failNum.add(approveBean.getSerialNo());
						failMsg.append("审批单号[" + approveBean.getSerialNo() + "]=>" + (e.getCause() == null ? e.getMessage():e.getCause())  + "<br>");
						JY.error(String.format("审批失败任务节点[%s]",approveBean.getTaskId()), e);
					}finally {
						//不关心成功与失败计数器减1
						downLatch.countDown();
					}
				}
			};
			executorService.execute(runnable);//执行线程
		}//循环结束
		try {
			downLatch.await();//唤醒等待
		} catch (InterruptedException e) {
			JY.error("批量流程审批异常InterruptedException", e);
		}finally {
			//防止线程池无
//			executorService.shutdown();
		}
		long end = System.currentTimeMillis();
		return RetMsgHelper.ok(String.format("一共提交%s笔，其中成功%s笔，失败%s笔；消耗的时间为:%s（分钟）；<br> 处理信息：%s", approveBeanLst.size(),
				approveBeanLst.size() - failNum.size() , failNum.size(),(end-start)/1000/60, failMsg));
	}


	/**
	 * 批量通过/拒绝审批【单笔操作,已由/approveBatchPass替代】
	 * 审批操作 map参数 complete_type 操作类型【非空】[-1:不通过 0：继续 1：通过] task_id 任务id【非空】 msg
	 * 审批信息
	 *
	 * @return
	 * @throws IOException
	 * @throws Exception
	 */
	@Deprecated
	@RequestMapping(value = "/approve")
	@ResponseBody
	public RetMsg<Serializable> approve(@RequestBody Map<String, String> paramMap) throws Exception {
		String user_id = this.getThreadLocalUser().getUserId();
		String task_id = ParameterUtil.getString(paramMap, "task_id", "");
		String op_type = ParameterUtil.getString(paramMap, "op_type", "");
		String msg = ParameterUtil.getString(paramMap, "msg", "");
		String specific_user = ParameterUtil.getString(paramMap, "specific_user", "");
		String back_to = ParameterUtil.getString(paramMap, "back_to", "");
		HashMap<String, Object> data = new HashMap<String, Object>();
		String productData = ParameterUtil.getString(paramMap, "data", "");
		String serial_no = ParameterUtil.getString(paramMap, "serial_no", "");
		logManager.info(String.format("%s用户在%s操作了IfsFlowController.approve -> 批量通过/拒绝审批【单笔操作,已由/approveBatchPass替代】,审批单号[%s]",user_id, DateUtil.getDateTime(),serial_no));
		if (StringUtil.isEmpty(user_id) || StringUtil.isEmpty(task_id) || StringUtil.isEmpty(op_type)) {
			throw new RException("参数不能为空");
		}
		// 校验任务是否存在
		Task task = taskService.createTaskQuery().taskId(task_id).singleResult();
		// 根据流程实例ID找到业务映射记录
		TtFlowSerialMap flowSerMap = ttFlowSerialMapMapper.get(null, serial_no, null, Long.valueOf(task.getProcessInstanceId()));
		String dealNo = flowSerMap.getSerial_no();
		// "2"为审批跳转操作

		//提交前校验（交易对手信息，债券信息）
		if(!"99".equals(op_type)){//退回发起
			Map<String,Object> paramBeforeCheck=new HashMap<String, Object>();
			paramBeforeCheck.put("ticketId", dealNo);
			paramBeforeCheck.put("prdNo", flowSerMap.getPrd_no());
			paramBeforeCheck.put("flow_type", flowSerMap.getFlow_type());
			String approveBeforeCheckMsg = approveBeforeCheck(paramBeforeCheck);
			if(!"".equals(approveBeforeCheckMsg)){
				JY.raise(approveBeforeCheckMsg);
			}
		}
		if ("2".equalsIgnoreCase(op_type)) {
			List<String> userList = Arrays.asList(specific_user.split(","));
			flowService.invitationWithTask(task_id, userList);
		} else {
			if (StringUtil.isNotEmpty(productData)) {
				data = (HashMap) FastJsonUtil.parseObject(productData, Map.class);
				flowOtherService.completeAndSave(user_id, task_id, op_type, specific_user, msg, back_to, data);
			} else {

				/*IfsFlowMonitor ifsFlowMonitor = ifsFlowMonitorMapper.getEntityById(dealNo);
				if(ifsFlowMonitor!=null){
					if( user_id.equals(ifsFlowMonitor.getSponsor())){
						//重新发起的重新占用额度
						List<EdInParams> creditList =  new ArrayList<EdInParams>();
						creditList = ifsCfetsfxLendMapper.searchAllInfo(dealNo);
						for(int i=0;i<creditList.size();i++){
							paramMap.put("product_type", creditList.get(i).getPrdNo());
						}
						paramMap.put("serial_no", dealNo);
						edCustManangeService.chooseOccupy(paramMap);
						//限额
						String prdNo = ParameterUtil.getString(paramMap, "product_type", "");//交易产品
						if("411".equals(prdNo)){
							//411外汇即期,391外汇远期,432外汇掉期,431货币掉期,440外汇对即期.433人民币期权TODO，需要的添加，并增加视图V_FOREIGNEXCHANGE_LIMIT
							paramMap.put("code", "1");//外汇限额
							edCustManangeService.limitForeignExchange(paramMap);
						}
						if("443".equals(prdNo)){
							//443现劵买卖，442买断式回购,446质押式回购,445债券借贷
							paramMap.put("code", "0");//债券限额

							String inFlag = "0";////作为判断标识符，为0时是审批台发起，为1为清算后发起
							edCustManangeService.limitBondExchange(paramMap,inFlag);
						}
					}else{
						//正式交易将预占用转换成占用明细
						//TODO预占用先不用
						//String status = edCustManangeService.changeOccupyType(dealNo);
					}
				}*/

				flowOpService.completePersonalTask(user_id, task_id,dealNo, op_type, specific_user, msg, back_to);
			}
		}
		// 发送消息提醒
		/*Map<String, Object> messageMap = new HashMap<String, Object>();
		messageMap.put("task_id", task_id);// 环节ID
		messageMap.put("user_id", SlSessionHelper.getUserId());
		messageMap.put("type", "approve");
		messageMap.put("serial_no", dealNo);// 交易编号
		taMessageService.insertTaMessage(messageMap);*/
		return RetMsgHelper.ok();
	}

	/**
	 * 获取指定的唯一一条流程定义的基本属性 map 参数： serial_no 外部序号 若有该参数，则忽略其他参数，其他参数可为空，否则其他参数必输
	 * flow_id 流程id 【与serial_no不同时为空】 version 版本号 【与serial_no不同时为空】
	 *
	 * @return
	 */
	@RequestMapping(value = "/getOneFlowDefineBaseInfo")
	@ResponseBody
	public RetMsg<ProcDefInfo> getOneFlowDefineBaseInfo(@RequestBody Map<String, String> paramMap) throws Exception {
		String serial_no = ParameterUtil.getString(paramMap, "serial_no", "");
		String flow_id = ParameterUtil.getString(paramMap, "flow_id", "");
		String version = ParameterUtil.getString(paramMap, "version", "");
		if (StringUtil.isEmpty(serial_no)) {
			if (StringUtil.isEmpty(flow_id) || StringUtil.isEmpty(version)) {
				throw new RException("参数错误");
			}
		} else {
			TtFlowSerialMap flowSerialMap = flowQueryService.getFlowSerialMap(serial_no);
			if (null == flowSerialMap) {
				throw new RException("未找到对应流程");
			}
			flow_id = flowSerialMap.getFlow_id();
			version = flowSerialMap.getVersion();
		}
		return RetMsgHelper.ok(flowQueryService.getFlowDefine(flow_id, version));
	}

	/**
	 * 通过外部流水号获取流程定义ID和流程实例ID
	 *
	 * @param paramMap
	 *            serial_no 外部流水号(审批单号)
	 *
	 * @return Map<String, String> serial_no 外部流水号（审批单号） flow_id 流程定义ID
	 *         instance_id 流程实例ID
	 *
	 * @throws Exception
	 * @author 杨阳
	 */
	@RequestMapping(value = "/getFlowIdAndInstanceIdBySerialNo")
	@ResponseBody
	public RetMsg<?> getFlowIdAndInstanceIdBySerialNo(@RequestBody Map<String, String> paramMap) throws Exception {
		String serial_no = ParameterUtil.getString(paramMap, "serial_no", "");
		Map<String, String> map = flowQueryService.getFlowIdAndInstanceIdBySerialNo(serial_no);
		return RetMsgHelper.ok(map);
	}

	/** 撤回/召回
	 */
	@RequestMapping(value = "/recall")
	@ResponseBody
	public RetMsg<Serializable> recall(@RequestBody Map<String,Object> param) throws Exception {
		List<String> serialNoLst = JSON.parseArray(ParameterUtil.getString(param, "serial_nos", ""), String.class);
		logManager.info(String.format("%s用户在%s操作了IfsFlowController.recall -> 撤回/召回,审批单号[%s]",this.getThreadLocalUser().getUserId(), DateUtil.getDateTime(),serialNoLst));
		int failNum = 0;
		for (String serialNo : serialNoLst) {
			try {
				flowOpService.recall(serialNo);
			} catch (Exception e) {
				failNum += 1;
			}
		}
		return RetMsgHelper.ok(String.format("一共撤回%s笔,其中成功%s笔,失败%s笔。", serialNoLst.size(), serialNoLst.size() - failNum, failNum));
	}

	/** 发起人撤销
	 */
	@RequestMapping(value = "/reback")
	@ResponseBody
	public RetMsg<Serializable> reback(@RequestBody Map<String,Object> param) throws Exception {
		List<String> serialNoLst = JSON.parseArray(ParameterUtil.getString(param, "serial_nos", ""), String.class);
		logManager.info(String.format("%s用户在%s操作了IfsFlowController.recall -> 发起人撤销,审批单号[%s]",this.getThreadLocalUser().getUserId(), DateUtil.getDateTime(),serialNoLst));
		int failNum = 0;
		for (String serialNo : serialNoLst) {
			try {
				flowOpService.ApproveCancel(serialNo);
			} catch (Exception e) {
				failNum += 1;
			}
		}
		return RetMsgHelper.ok(String.format("一共撤销[不要]%s笔,其中成功%s笔,失败%s笔。", serialNoLst.size(), serialNoLst.size() - failNum, failNum));
	}

	/**
	 * 单笔提交前检查交易对手和债券信息
	 * @param param
	 * @return
	 */
	public String approveBeforeCheck(Map<String,Object> param){
		String msg = "";//校验信息返回
		String ticketId = ParameterUtil.getString(param, "ticketId", "");//交易审批单号
		String prdno = ParameterUtil.getString(param, "prdNo", "");//产品类型
		String flow_type = ParameterUtil.getString(param, "flow_type", "");

		if ("".equals(ticketId)) {
			msg = "审批单号不存在";
			return msg;
		}

		if ("".equals(prdno) || TradeConstants.ProductCode.CURRENCY_FUND.equals(prdno) || TradeConstants.ProductCode.BOND_FUND.equals(prdno) || TradeConstants.ProductCode.SPEC_FUND.equals(prdno)) {
			//退回发起会为空，从监控表中取
			IfsFlowMonitor ifsFlowMonitor = ifsFlowMonitorMapper.getPrdNoById(ticketId);
			if (ifsFlowMonitor != null) {
				prdno = ifsFlowMonitor.getPrdNo();
			}
		}
		if("4".equals(flow_type)) {
			logManager.info("==========提交正式审批信息参数值prdno=[" + prdno + "],ticketId=[" + ticketId + "]=================");
			if (!"".equals(prdno)) {
				if ("rmbspt".equals(prdno) || "411".equals(prdno) || "外汇即期".equals(prdno)) {
					//外汇即期
					IfsCfetsfxSpt ifsCfetsfxSpt = ifsCfetsfxSptMapper.getSpot(ticketId);
					if (ifsCfetsfxSpt != null) {
						msg = checkCust(ifsCfetsfxSpt.getCounterpartyInstId(), ticketId);
					} else {
						msg = "[" + ticketId + "]交易不存在";
					}
				} else if ("rmbfwd".equals(prdno) || "391".equals(prdno) || "外汇远期".equals(prdno)) {
					//外汇远期
					IfsCfetsfxFwd ifsCfetsfxFwd = ifsCfetsfxFwdMapper.getCredit(ticketId);
					if (ifsCfetsfxFwd != null) {
						msg = checkCust(ifsCfetsfxFwd.getCounterpartyInstId(), ticketId);
					} else {
						msg = "[" + ticketId + "]交易不存在";
					}
				} else if ("rmbswap".equals(prdno) || "432".equals(prdno) || "外汇掉期".equals(prdno)) {
					//外汇掉期
					IfsCfetsfxSwap ifsCfetsfxSwap = ifsCfetsfxSwapMapper.getrmswap(ticketId);
					if (ifsCfetsfxSwap != null) {
						msg = checkCust(ifsCfetsfxSwap.getCounterpartyInstId(), ticketId);
					} else {
						msg = "[" + ticketId + "]交易不存在";
					}
				} else if ("ccyLending".equals(prdno) || "438".equals(prdno) || "外币拆借".equals(prdno)) {
					//外币拆借
					IfsCfetsfxLend ifsCfetsfxLend = ifsCfetsfxLendMapper.searchCcyLending(ticketId);
					if (ifsCfetsfxLend != null) {
						msg = checkCust(ifsCfetsfxLend.getCounterpartyInstId(), ticketId);
					} else {
						msg = "[" + ticketId + "]交易不存在";
					}
				} else if ("ccyDepositIn".equals(prdno) || "436".equals(prdno) || "同业存放(外币)".equals(prdno)) {
					//同业存放(外币)
					IfsFxDepositIn ifsFxDepositIn = ifsFxDepositInMapper.searchByTicketId(ticketId);
					if (ifsFxDepositIn != null) {
						msg = checkCust(ifsFxDepositIn.getCustId(), ticketId);
					} else {
						msg = "[" + ticketId + "]交易不存在";
					}
				}else if ("ccyDepositout".equals(prdno) || "437".equals(prdno) || "存放同业(外币)".equals(prdno)) {
					//存放同业(外币)
					IfsFxDepositout ifsFxDepositout = ifsFxDepositoutMapper.searchByTicketId(ticketId);
					if (ifsFxDepositout != null) {
						msg = checkCust(ifsFxDepositout.getCustId(), ticketId);
					} else {
						msg = "[" + ticketId + "]交易不存在";
					}
				} else if ("rmbDepositIn".equals(prdno) || "790".equals(prdno) || "同业存放".equals(prdno)) {
					//同业存放
					IfsRmbDepositin ifsRmbDepositin = ifsRmbDepositinMapper.searchByTicketId(ticketId);
					if (ifsRmbDepositin != null) {
						msg = checkCust(ifsRmbDepositin.getCustId(), ticketId);
					} else {
						msg = "[" + ticketId + "]交易不存在";
					}
				} else if ("rmbDepositOut".equals(prdno) || "791".equals(prdno) || "存放同业".equals(prdno)) {
					//存放同业
					IfsRmbDepositout ifsRmbDepositout = ifsRmbDepositoutMapper.searchByTicketId(ticketId);
					if (ifsRmbDepositout != null) {
						msg = checkCust(ifsRmbDepositout.getCustId(), ticketId);
					} else {
						msg = "[" + ticketId + "]交易不存在";
					}
				} else if ("ibo".equals(prdno) || "444".equals(prdno) || "信用拆借".equals(prdno)
						|| "tyibo".equals(prdno) || "544".equals(prdno) || "同业借款".equals(prdno)
						|| "fxtyibo".equals(prdno) || "435".equals(prdno) || "同业拆放".equals(prdno)) {
					//信用拆借
					IfsCfetsrmbIbo ifsCfetsrmbIbo = cfetsrmbIboMapper.searchCredLo(ticketId);
					if (ifsCfetsrmbIbo != null) {
						msg = checkCust(ifsCfetsrmbIbo.getRemoveInst(), ticketId);
					} else {
						msg = "[" + ticketId + "]交易不存在";
					}
				} else if ("800".equals(prdno) || "同业存款(线上)".equals(prdno) || "799".equals(prdno)|| "同业存款(线下)".equals(prdno)) {
					//同业存款
					IfsApprovermbDeposit ifsApprovermbDeposit = ifsApprovermbDepositMapper.selectByPrimaryKey(ticketId);
					if (ifsApprovermbDeposit != null) {
						msg = checkCust(ifsApprovermbDeposit.getCounterpartyInstId(), ticketId);
					} else {
						msg = "[" + ticketId + "]交易不存在";
					}
				} else if ("cbt".equals(prdno) || "443".equals(prdno) || "现券买卖".equals(prdno)
						|| "debt".equals(prdno) || "451".equals(prdno)
						|| "mbs".equals(prdno) || "471".equals(prdno) || "提前偿还".equals(prdno)) {
					//现券买卖、外币债
					IfsCfetsrmbCbt ifsCfetsrmbCbt = ifsCfetsrmbCbtMapper.searchIfsBondTra(ticketId);
					if (ifsCfetsrmbCbt != null) {
						msg = checkCust(ifsCfetsrmbCbt.getSellInst(), ticketId);
						//客户校验不通过，债券就没必要校验了
						if ("".equals(msg)) {
							msg = checkBond(ifsCfetsrmbCbt.getBondCode(), ticketId);
						}
					} else {
						msg = "[" + ticketId + "]交易不存在";

					}
				} else if ("sl".equals(prdno) || "445".equals(prdno) || "债券借贷".equals(prdno)) {
					String slString = ticketId.substring(0, 2);
					//债券借贷事前审批
					if ("SL".equals(slString)) {
						IfsApprovermbSl ifsCfetsrmbSl = ifsapprovermbslmapper.searchBondLon(ticketId);
						if (ifsCfetsrmbSl != null) {
							msg = checkCust(ifsCfetsrmbSl.getLendInst(), ticketId);
							//客户校验不通过，债券就没必要校验了
							if ("".equals(msg)) {
								//标的券
								msg = checkBond(ifsCfetsrmbSl.getUnderlyingSecurityId(), ticketId);
								if ("".equals(msg)) {
									//质押券
									List<IfsCfetsrmbDetailSl> list = ifsCfetsrmbDetailSlMapper.searchBondByTicketId(ticketId);
									for (IfsCfetsrmbDetailSl ifsCfetsrmbDetailSl : list) {
										msg = checkBond(ifsCfetsrmbDetailSl.getMarginSecuritiesId(), ticketId);
										if (!"".equals(msg)) {
											break;
										}
									}
								}
							}
						} else {
							msg = "[" + ticketId + "]交易不存在";
						}
					} else {
						//债券借贷
						IfsCfetsrmbSl ifsCfetsrmbSl = ifsCfetsrmbSlMapper.searchBondLon(ticketId);
						if (ifsCfetsrmbSl != null) {
							msg = checkCust(ifsCfetsrmbSl.getLendInst(), ticketId);
							//客户校验不通过，债券就没必要校验了
							if ("".equals(msg)) {
								//标的券
								msg = checkBond(ifsCfetsrmbSl.getUnderlyingSecurityId(), ticketId);
								if ("".equals(msg)) {
									//质押券
									List<IfsCfetsrmbDetailSl> list = ifsCfetsrmbDetailSlMapper.searchBondByTicketId(ticketId);
									for (IfsCfetsrmbDetailSl ifsCfetsrmbDetailSl : list) {
										msg = checkBond(ifsCfetsrmbDetailSl.getMarginSecuritiesId(), ticketId);
										if (!"".equals(msg)) {
											break;
										}
									}
								}
							}
						} else {
							msg = "[" + ticketId + "]交易不存在";
						}
					}
				} else if ("dp".equals(prdno) || "452".equals(prdno) || "债券发行".equals(prdno)
						|| "dpcd".equals(prdno) || "存单发行".equals(prdno) || "425".equals(prdno)) {
					//债券发行，循环判断
					List<IfsCfetsrmbDpOffer> list = ifsCfetsrmbDpOfferMapper.searchOfferList(ticketId);
					for (IfsCfetsrmbDpOffer ifsCfetsrmbDpOffer : list) {
						msg = checkCust(ifsCfetsrmbDpOffer.getLendInst(), ticketId);
						if (!"".equals(msg)) {
							break;
						}
					}
					//客户校验不通过，债券就没必要校验了
					if ("".equals(msg)) {
						//校验债券
						Map<String, String> map = new HashMap<String, String>();
						map.put("ticketId", ticketId);
						//					map.put("branchId", DictConstants.Branch.ID);
						IfsCfetsrmbDp ifsCfetsrmbDp = ifsCfetsrmbDpMapper.selectById(map);
						if (ifsCfetsrmbDp != null) {
							msg = checkBond(ifsCfetsrmbDp.getDepositCode(), ticketId);
						} else {
							msg = "[" + ticketId + "]交易不存在";
						}
					}
				} else if ("cr".equals(prdno) || "446".equals(prdno) || "质押式回购".equals(prdno)
						|| "467".equals(prdno) || "CRGD".equals(prdno) || "国库定期存款".equals(prdno)
						|| "468".equals(prdno) || "CRJY".equals(prdno) || "交易所回购".equals(prdno)
						|| "469".equals(prdno) || "CRCB".equals(prdno) || "常备借贷便利".equals(prdno)
						|| "470".equals(prdno) || "CRZQ".equals(prdno) || "中期借贷便利".equals(prdno)
						|| "472".equals(prdno) || "CRZD".equals(prdno) || "线下押券".equals(prdno)) {
					//质押式回购 国库定期存款 交易所回购  常备借贷便利  中期借贷便利
					IfsCfetsrmbCr ifsCfetsrmbCr = ifsCfetsrmbCrMapper.searchPleDge(ticketId);
					if (ifsCfetsrmbCr != null) {
						msg = checkCust(ifsCfetsrmbCr.getReverseInst(), ticketId);
						//客户校验不通过，债券就没必要校验了
						if ("".equals(msg)) {
							//质押券
							List<IfsCfetsrmbDetailCr> list = ifsCfetsrmbDetailCrMapper.searchBondByTicketId(ticketId);
							for (IfsCfetsrmbDetailCr ifsCfetsrmbDetailCr : list) {
								msg = checkBond(ifsCfetsrmbDetailCr.getBondCode(), ticketId);
								if (!"".equals(msg)) {
									break;
								}
							}
						}
					} else {
						msg = "[" + ticketId + "]交易不存在";
					}
				} else if ("or".equals(prdno) || "442".equals(prdno) || "买断式回购".equals(prdno)) {
					//买断式回购
					IfsCfetsrmbOr ifsCfetsrmbOr = ifsCfetsrmbOrMapper.searchCfetsrmbOrByTicketId(ticketId);
					if (ifsCfetsrmbOr != null) {
						//校验客户信息
						msg = checkCust(ifsCfetsrmbOr.getReverseInst(), ticketId);
						//客户校验不通过，债券就没必要校验了
						if ("".equals(msg)) {
							msg = checkBond(ifsCfetsrmbOr.getBondCode(), ticketId);
						}
					} else {
						msg = "[" + ticketId + "]交易不存在";
					}
				} else if ("irs".equals(prdno) || "441".equals(prdno) || "利率互换".equals(prdno) || "irsDerive".equals(prdno)) {
					//利率互换,结构衍生
					Map<String, String> map = new HashMap<String, String>();
					map.put("ticketId", ticketId);
					//				map.put("branchId", DictConstants.Branch.ID);
					IfsCfetsrmbIrs IfsCfetsrmbIrs = ifsCfetsrmbIrsMapper.selectById(map);
					if (IfsCfetsrmbIrs != null) {
						msg = checkCust(IfsCfetsrmbIrs.getFloatInst(), ticketId);
					} else {
						msg = "[" + ticketId + "]交易不存在";
					}
				} else if ("BFWD".equals(prdno) || "447".equals(prdno) || "债券远期".equals(prdno)) {
					//债券远期
					IfsCfetsrmbBondfwd ifsCfetsrmbBondfwd = ifsCfetsrmbBondfwdMapper.selectById(ticketId);
					if (ifsCfetsrmbBondfwd != null) {
						msg = checkCust(ifsCfetsrmbBondfwd.getCno(), ticketId);
						//客户校验不通过，债券就没必要校验了
						if ("".equals(msg)) {
							msg = checkBond(ifsCfetsrmbBondfwd.getBondCode(), ticketId);
						}
					} else {
						msg = "[" + ticketId + "]交易不存在";
					}
				} else if ("goldspt".equals(prdno) || "450".equals(prdno) || "黄金即期".equals(prdno)) {
					//黄金即期
					IfsCfetsmetalGold ifsCfetsmetalGold = ifsCfetsmetalGoldMapper.searchGoldspt(ticketId);
					if (ifsCfetsmetalGold != null) {
						msg = checkCust(ifsCfetsmetalGold.getCounterpartyInstId(), ticketId);
					} else {
						msg = "[" + ticketId + "]交易不存在";
					}
				} else if ("goldfwd".equals(prdno) || "448".equals(prdno) || "黄金远期".equals(prdno)) {
					//黄金远期
					IfsCfetsmetalGold ifsCfetsmetalGold = ifsCfetsmetalGoldMapper.searchGoldspt(ticketId);
					if (ifsCfetsmetalGold != null) {
						msg = checkCust(ifsCfetsmetalGold.getCounterpartyInstId(), ticketId);
					} else {
						msg = "[" + ticketId + "]交易不存在";
					}
				} else if ("goldswap".equals(prdno) || "449".equals(prdno) || "黄金掉期".equals(prdno)) {
					//黄金掉期
					IfsCfetsmetalGold ifsCfetsmetalGold = ifsCfetsmetalGoldMapper.searchGoldspt(ticketId);
					if (ifsCfetsmetalGold != null) {
						msg = checkCust(ifsCfetsmetalGold.getCounterpartyInstId(), ticketId);
					} else {
						msg = "[" + ticketId + "]交易不存在";
					}
				} else if ("666".equals(prdno) || "667".equals(prdno)) {
					//额度占用
					return msg;
				} else if ("rmbopt".equals(prdno) || "433".equals(prdno)) {
					//外汇期权
					IfsCfetsfxOption ifsCfetsfxOption = ifsCfetsfxOptionMapper.searchOption(ticketId);
					if (ifsCfetsfxOption != null) {
						msg = checkCust(ifsCfetsfxOption.getCounterpartyInstId(), ticketId);
					} else {
						msg = "[" + ticketId + "]交易不存在";
					}
				} else {
					msg = "[" + ticketId + "]交易不存在";
				}
			} else {
				msg = "产品类型不存在";
			}
		}else if("40".equals(flow_type)) {
			logManager.info("==========提交事前审批信息参数值prdno=["+prdno+"],ticketId=["+ticketId+"]=================");
			if(!"".equals(prdno)){
				if("rmbspt".equals(prdno)||"411".equals(prdno)||"外汇即期".equals(prdno)){
					//外汇即期
					IfsApprovefxSpt ifsApprovefxSpt = ifsApprovefxSptMapper.getSpot(ticketId);

					if(ifsApprovefxSpt!=null){
						msg=checkCust(ifsApprovefxSpt.getCounterpartyInstId(),ticketId);
					}else{
						msg="["+ticketId+"]交易不存在";
					}
				}else if("sl".equals(prdno)||"445".equals(prdno)||"债券借贷事前审批".equals(prdno)){
					IfsApprovermbSl ifsApprovermbSl =ifsapprovermbslmapper.searchBondLon(ticketId);
					if(ifsApprovermbSl!=null){
						msg=checkCust(ifsApprovermbSl.getLendInst(),ticketId);
						//客户校验不通过，债券就没必要校验了
						if("".equals(msg)){
							//标的券
							msg=checkBond(ifsApprovermbSl.getUnderlyingSecurityId(),ticketId);
							if("".equals(msg)){
								//质押券
								List<IfsCfetsrmbDetailSl> list= ifsCfetsrmbDetailSlMapper.searchBondByTicketId(ticketId);
								for(IfsCfetsrmbDetailSl ifsCfetsrmbDetailSl: list){
									msg=checkBond(ifsCfetsrmbDetailSl.getMarginSecuritiesId(),ticketId);
									if(!"".equals(msg)){
										break;
									}
								}
							}
						}else{
							msg="["+ticketId+"]交易不存在";
						}
					}
				}else{
					msg="["+ticketId+"]交易不存在";
				}
			}else{
				msg="产品类型不存在";
			}
		}else if("30".equals(flow_type)) {
			logManager.info("==========提交基金申购审批信息参数值prdno=["+prdno+"],ticketId=["+ticketId+"]=================");
			if(!"".equals(prdno)) {
				if ("货币基金申购".equals(prdno) || TradeConstants.TrdType.CURY_FUND_AFP.equals(prdno) || "801".equals(prdno) ||
						"债券基金申购".equals(prdno) || TradeConstants.TrdType.BD_FUND_AFP.equals(prdno) || "802".equals(prdno) ||
						"专户基金申购".equals(prdno) || TradeConstants.TrdType.FUND_AFP.equals(prdno) ||"803".equals(prdno) ) {
					//基金申购
					CFtAfp cFtAfp = cFtAfpMapper.selectByPrimaryKey(ticketId);
					if (cFtAfp != null) {
						msg = checkFund(cFtAfp.getFundCode(), ticketId);

					} else {
						msg = "[" + ticketId + "]交易不存在";
					}
				} else {
					msg = "[" + ticketId + "]交易不存在";
				}
			} else {
				msg = "产品类型不存在";
			}
		}else if("31".equals(flow_type)) {
			logManager.info("==========提交基金申购确认审批信息参数值prdno=["+prdno+"],ticketId=["+ticketId+"]=================");
			if(!"".equals(prdno)) {
				if ("货币基金申购份额确认".equals(prdno) || TradeConstants.TrdType.CURY_FUND_AFPCONF.equals(prdno) || "801".equals(prdno) ||
						"债券基金申购份额确认".equals(prdno) || TradeConstants.TrdType.BD_FUND_AFPCONF.equals(prdno) || "802".equals(prdno) ||
						"专户基金申购份额确认".equals(prdno) || TradeConstants.TrdType.FUND_AFPCONF.equals(prdno) || "803".equals(prdno)) {
					//基金申购确认
					CFtAfpConf cFtAfpConf = cFtAfpConfMapper.selectByPrimaryKey(ticketId);
					if (cFtAfpConf != null) {
						msg = checkFund(cFtAfpConf.getFundCode(), ticketId);

					} else {
						msg = "[" + ticketId + "]交易不存在";
					}
				} else {
					msg = "[" + ticketId + "]交易不存在";
				}
			} else {
				msg = "产品类型不存在";
			}
		}else if("32".equals(flow_type)) {
			logManager.info("==========提交基金赎回审批信息参数值prdno=["+prdno+"],ticketId=["+ticketId+"]=================");
			if(!"".equals(prdno)) {
				if ("货币基金赎回".equals(prdno) || TradeConstants.TrdType.CURY_FUND_RDP.equals(prdno) || "801".equals(prdno) ||
						"债券基金赎回".equals(prdno) || TradeConstants.TrdType.BD_FUND_RDP.equals(prdno) || "802".equals(prdno) ||
						"专户基金赎回".equals(prdno) || TradeConstants.TrdType.FUND_RDP.equals(prdno) || "803".equals(prdno)) {
					//基金赎回
					CFtRdp cFtRdp = cFtRdpMapper.selectByPrimaryKey(ticketId);
					if (cFtRdp != null) {
						msg = checkFund(cFtRdp.getFundCode(), ticketId);

					} else {
						msg = "[" + ticketId + "]交易不存在";
					}
				} else {
					msg = "[" + ticketId + "]交易不存在";
				}
			} else {
				msg = "产品类型不存在";
			}
		}else if("33".equals(flow_type)) {
			logManager.info("==========提交基金赎回确认审批信息参数值prdno=["+prdno+"],ticketId=["+ticketId+"]=================");
			if(!"".equals(prdno)) {
				if ("货币基金赎回份额确认".equals(prdno) || TradeConstants.TrdType.CURY_FUND_RDPCONF.equals(prdno) || "801".equals(prdno) ||
						"债券基金赎回份额确认".equals(prdno) || TradeConstants.TrdType.BD_FUND_RDPCONF.equals(prdno) || "802".equals(prdno) ||
						"专户基金赎回份额确认".equals(prdno) || TradeConstants.TrdType.FUND_RDPCONF.equals(prdno) || "803".equals(prdno)) {
					//基金赎回
					CFtRdpConf cFtRdpConf = cFtRdpConfMapper.selectByPrimaryKey(ticketId);
					if (cFtRdpConf != null) {
						msg = checkFund(cFtRdpConf.getFundCode(), ticketId);

					} else {
						msg = "[" + ticketId + "]交易不存在";
					}
				} else {
					msg = "[" + ticketId + "]交易不存在";
				}
			} else {
				msg = "产品类型不存在";
			}
		}else if("34".equals(flow_type)) {
			if(!"".equals(prdno)) {
				if ("货币基金现金分红".equals(prdno) || "债券基金现金分红".equals(prdno) ||"现金分红".equals(prdno) || "专户基金现金分红".equals(prdno) || TradeConstants.TrdType.FUND_REDD.equals(prdno)) {
					//基金现金分红
					CFtRedd cFtRedd = cFtReddMapper.selectByPrimaryKey(ticketId);
					if (cFtRedd != null) {
						msg = checkFund(cFtRedd.getFundCode(), ticketId);

					} else {
						msg = "[" + ticketId + "]交易不存在";
					}
				} else if ("货币基金红利再投".equals(prdno) || "债券基金红利再投".equals(prdno) ||"红利再投".equals(prdno) ||"专户基金红利再投".equals(prdno) || TradeConstants.TrdType.FUND_REIN.equals(prdno)) {
					//基金红利再投
					CFtRein cFtRein = cFtReinMapper.selectByPrimaryKey(ticketId);
					if (cFtRein != null) {
						msg = checkFund(cFtRein.getFundCode(), ticketId);

					} else {
						msg = "[" + ticketId + "]交易不存在";
					}
				} else {
					msg = "[" + ticketId + "]交易不存在";
				}
			} else {
				msg = "产品类型不存在";
			}
		}else if("99".equals(flow_type)){
			if(!"".equals(prdno)) {
				if("411".equals(prdno)|| "391".equals(prdno) ||"432".equals(prdno)){
					//外汇类冲销
					IfsRevIfxd ifsRevIfxd = ifsRevlfxdMapper.getOneById(ticketId);
					if(ifsRevIfxd != null){}else {
						msg = "[" + ticketId + "]交易不存在";
					}
				} else if("442".equals(prdno) || "446".equals(prdno) ||"467".equals(prdno) ||"468".equals(prdno)|| "469".equals(prdno)|| "470".equals(prdno)|| "472".equals(prdno) ){
					//回购类冲销
					IfsRevBred ifsRevBred = ifsRevBredMapper.getOneByPrimaryKey(ticketId);
					if(ifsRevBred != null){}else {
						msg = "[" + ticketId + "]交易不存在";
					}
				} else if("444".equals(prdno) || "544".equals(prdno) ||"438".equals(prdno) ||"800".equals(prdno)|| "799".equals(prdno)){
					//拆借类冲销
					IfsRevIrvv ifsRevIrvv = ifsRevIrvvMapper.getOneById(ticketId);
					if(ifsRevIrvv != null){}else {
						msg = "[" + ticketId + "]交易不存在";
					}
				} else if("441".equals(prdno)){
					//互换类冲销
					IfsRevIswh ifsRevIswh = ifsRevIswhMapper.getOneById(ticketId);
					if(ifsRevIswh != null){}else {
						msg = "[" + ticketId + "]交易不存在";
					}
				} else if("443".equals(prdno)|| "445".equals(prdno) ||"452".equals(prdno) ||"425".equals(prdno)){
					//债券类冲销
					IfsRevIsld ifsRevIsld = ifsRevIsldMapper.getOneById(ticketId);
					if(ifsRevIsld != null){}else {
						msg = "[" + ticketId + "]交易不存在";
					}
				} else {
					msg = "[" + ticketId + "]交易不存在";
				}
			} else {
				msg = "产品类型不存在";
			}
		}else {
			msg = "审批类型不存在";
		}
		return msg;
	}

	private String checkCust(String cno,String ticketId){
		String msg="";
		logManager.info("==========提交审批客户信息检查cno=["+cno+"],ticketId=["+ticketId+"]=================");
		if("".equals(cno)){
			msg="交易本身没有交易对手";
		}else{
			IfsOpicsCust ifsOpicsCust =  ifsOpicsCustMapper.searchById(cno);
			if(ifsOpicsCust!=null){
				msg=checkCustDetail(ifsOpicsCust);
			}else{
				msg="交易对手["+cno+"]不存在";
			}
		}
		if(!"".equals(msg)){
			msg="["+ticketId+"]"+msg;
		}
		return msg;
	}

	/**
	 * 前后台审批流程编号转化
	 * @param flow_type
	 * @return
	 */
	private String changeFlowType(String flow_type){
		if("0".equals(flow_type)){
			return "40";
		}else if("1".equals(flow_type)){
			return "4";
		}
		return flow_type;
	}

	private String checkBond(String bndcd,String ticketId){
		String msg="";
		logManager.info("==========提交审批债券信息检查bndcd=["+bndcd+"],ticketId=["+ticketId+"]=================");
		if("".equals(bndcd)){
			msg="交易本身没有债券";
		}else{
			IfsOpicsBond ifsOpicsBond = ifsOpicsBondService.searchOneById(bndcd);
			if(ifsOpicsBond!=null){
				msg=checkBondDetail(ifsOpicsBond);
			}else{
				msg="的["+bndcd+"]债券信息不存在";
			}
		}
		if(!"".equals(msg)){
			msg="["+ticketId+"]这笔交易的"+msg;
		}
		return msg;
	}

	private String checkCustDetail(IfsOpicsCust ifsOpicsCust){
		String msg="";
		if(StringUtils.isEmpty(ifsOpicsCust.getClitype())){
			msg="客户类型为空";
		}else if(StringUtils.isEmpty(ifsOpicsCust.getCliname())){
			msg="客户中文名称为空";
		}else if(StringUtils.isEmpty(ifsOpicsCust.getFlag())){
			msg="境内外标识为空";
		}else if(StringUtils.isEmpty(ifsOpicsCust.getCno())){
			msg="交易对手编号为空";
		}else if(StringUtils.isEmpty(ifsOpicsCust.getCname())){
			msg="交易对手代码为空";
		}else if(StringUtils.isEmpty(ifsOpicsCust.getCfn())){
			msg="交易对手全称为空";
		}else if(StringUtils.isEmpty(ifsOpicsCust.getSn())){
			msg="交易对手简称为空";
		}else if(StringUtils.isEmpty(ifsOpicsCust.getSic())){
			msg="工业标准代码为空";
		}else if(StringUtils.isEmpty(ifsOpicsCust.getUccode())){
			msg="国家代码为空";
		}else if(StringUtils.isEmpty(ifsOpicsCust.getCcode())){
			msg="城市代码为空";
		}else if(StringUtils.isEmpty(ifsOpicsCust.getLocation())){
			msg="所在城市为空";
		}else if(StringUtils.isEmpty(ifsOpicsCust.getAcctngtype())){
			msg="会计类型为空";
		}else if(StringUtils.isEmpty(ifsOpicsCust.getCtype())){
			msg="交易对手类型为空";
		}else if(StringUtils.isEmpty(ifsOpicsCust.getCreditsub())){
			msg="授信占用主体为空";
		}else if(StringUtils.isEmpty(ifsOpicsCust.getCreditsubno())){
			msg="授信占用主体编号为空";
		}
		if(StringUtils.isEmpty(ifsOpicsCust.getCno())){
			msg="交易对手编号为空";
		}else if(StringUtils.isEmpty(ifsOpicsCust.getCname())){
			msg="交易对手代码为空";
		}else if(StringUtils.isEmpty(ifsOpicsCust.getCfn())){
			msg="交易对手全称为空";
		}else if(StringUtils.isEmpty(ifsOpicsCust.getSn())){
			msg="交易对手简称为空";
		}else if(StringUtils.isEmpty(ifsOpicsCust.getSic())){
			msg="工业标准代码为空";
		}else if(StringUtils.isEmpty(ifsOpicsCust.getUccode())){
			msg="国家代码为空";
		}else if(StringUtils.isEmpty(ifsOpicsCust.getCcode())){
			msg="城市代码为空";
		}else if(StringUtils.isEmpty(ifsOpicsCust.getLocation())){
			msg="所在城市为空";
		}else if(StringUtils.isEmpty(ifsOpicsCust.getAcctngtype())){
			msg="会计类型为空";
		}else if(StringUtils.isEmpty(ifsOpicsCust.getCtype())){
			msg="交易对手类型为空";
		}

		return msg;
	}

	private String checkBondDetail(IfsOpicsBond ifsOpicsBond){
		String msg="";
		if(StringUtils.isEmpty(ifsOpicsBond.getBndcd())){
			msg="债券代码为空";
		}else if(StringUtils.isEmpty(ifsOpicsBond.getDescr())){
			msg="债券描述为空";
		}else if(StringUtils.isEmpty(ifsOpicsBond.getBndnm_en())){
			msg="债券简称为空";
		}else if(StringUtils.isEmpty(ifsOpicsBond.getBndnm_cn())){
			msg="债券全称为空";
		}else if(StringUtils.isEmpty(ifsOpicsBond.getProduct())){
			msg="债券产品代码为空";
		}else if(StringUtils.isEmpty(ifsOpicsBond.getProdtype())){
			msg="债券产品类型为空";
		}else if(StringUtils.isEmpty(ifsOpicsBond.getIntcalcrule())){
			msg="债券计息规则为空";
		}else if(StringUtils.isEmpty(ifsOpicsBond.getAcctngtype())){
			msg="债券会计类型为空";
		}else if(StringUtils.isEmpty(ifsOpicsBond.getCcy())){
			msg="债券货币为空";
		}else if(StringUtils.isEmpty(ifsOpicsBond.getSecunit())){
			msg="债券股权类型为空";
		}else if(StringUtils.isEmpty(ifsOpicsBond.getDenom())){
			msg="债券单价为空";
		}else if(StringUtils.isEmpty(ifsOpicsBond.getIssuer())){
			msg="债券发行人为空";
		}else if(StringUtils.isEmpty(ifsOpicsBond.getPubdt())){
			msg="债券发行日期为空";
		}else if(StringUtils.isEmpty(ifsOpicsBond.getBondproperties())){
			msg="债券标准行业代码为空";
		}else if(StringUtils.isEmpty(ifsOpicsBond.getSettccy())){
			msg="债券结算币种为空";
		}else if(StringUtils.isEmpty(ifsOpicsBond.getIssudt())){
			msg="债券起息日为空";
		}else if(StringUtils.isEmpty(ifsOpicsBond.getMatdt())){
			msg="债券到期日为空";
		}else if(StringUtils.isEmpty(ifsOpicsBond.getIssprice()==null?"":ifsOpicsBond.getIssprice().toString())){
			msg="债券面值为空";
		}else if(StringUtils.isEmpty(ifsOpicsBond.getIntpaymethod())){
			msg="债券利息支付规则为空";
		}else if(StringUtils.isEmpty(ifsOpicsBond.getIntenddaterule())){
			msg="债券利息结束日规则为空";
		}
		else if(StringUtils.isEmpty(ifsOpicsBond.getZtInst())){
			msg="主体评级机构为空";
		}else if(StringUtils.isEmpty(ifsOpicsBond.getZxZtLevel())){
			msg="主体评级级别为空";
		}
		else if(StringUtils.isEmpty(ifsOpicsBond.getZxInst())){
			msg="债项评级机构为空";
		}else if(StringUtils.isEmpty(ifsOpicsBond.getZxZxLevel())){
			msg="债项评级级别为空";
		}
//		else if(StringUtils.isEmpty(ifsOpicsBond.getBondratingclass())){
//			msg="债券评级类型为空";
//		}else if(StringUtils.isEmpty(ifsOpicsBond.getBondratingagency())){
//			msg="债券评级机构为空";
//		}else if(StringUtils.isEmpty(ifsOpicsBond.getBondrating())){
//			msg="债券评级结果为空";
//		}
		else if(StringUtils.isEmpty(ifsOpicsBond.getAccountno())){
			msg="债券托管机构为空";
		}else if(StringUtils.isEmpty(ifsOpicsBond.getBasis())){
			msg="债券计息方式为空";
		}else if(StringUtils.isEmpty(ifsOpicsBond.getParagraphfirstplan())){
			msg="债券第一个息票日期为空";
		}else if(StringUtils.isEmpty(ifsOpicsBond.getRateType())){
			msg="债券利率类型为空";
		}else if(StringUtils.isEmpty(ifsOpicsBond.getSlPubMethod())){
			msg="债券发行方式为空";
		}
//		else if(StringUtils.isEmpty(ifsOpicsBond.getMarketDate())){
//			msg="债券上市日为空";
//		}else if(StringUtils.isEmpty(ifsOpicsBond.getBr())){
//			msg="债券部门为空";
//		}
		return msg;
	}

	private String checkFund(String fundCode,String ticketId){
		String msg="";
		logManager.info("==========提交审批基金信息检查fundCode=["+fundCode+"],ticketId=["+ticketId+"]=================");
		if("".equals(fundCode)){
			msg="交易本身没有基金";
		}else{
			CFtInfo info = cFtInfoMapper.selectByPrimaryKey(fundCode);
			if(info==null){
				msg="的["+fundCode+"]基金信息不存在";
			}
		}
		if(!"".equals(msg)){
			msg="["+ticketId+"]这笔交易的"+msg;
		}
		return msg;
	}
}
