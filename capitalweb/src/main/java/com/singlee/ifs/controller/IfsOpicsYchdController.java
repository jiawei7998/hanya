package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.opics.IStaticServer;
import com.singlee.ifs.model.IfsOpicsYchd;
import com.singlee.ifs.service.IfsOpicsYchdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

/***
 * 利率曲线
 * @author lij
 *
 */
@Controller
@RequestMapping(value = "/IfsOpicsYchdController")
public class IfsOpicsYchdController {
	
	@Autowired
	IfsOpicsYchdService ifsOpicsYchdService;
	
	@Autowired
	IStaticServer iStaticServer;
	
	
	/***
	 * 分页查询
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageYchd")
	public RetMsg<PageInfo<IfsOpicsYchd>> searchPageYchd(@RequestBody Map<String, Object> map) {
		Page<IfsOpicsYchd> page = ifsOpicsYchdService.searchPageOpicsYchd(map);
		return RetMsgHelper.ok(page);
	}
	
	
	
	/**
	 * 批量校准 全部校准
	 */
	@ResponseBody
	@RequestMapping(value = "/batchCalibration2")
	public RetMsg<SlOutBean> batchCalibration2(@RequestBody Map<String, Object> map) {
		
		SlOutBean slOutBean = ifsOpicsYchdService.batchCalibration("2",null);
		return RetMsgHelper.ok(slOutBean);
	}
	
	
	/**
	 * 批量校准  选中行校准
	 */
	@ResponseBody
	@RequestMapping(value = "/batchCalibration1")
	public RetMsg<SlOutBean> batchCalibration1(@RequestBody List<IfsOpicsYchd> list) {
		
		SlOutBean slOutBean = ifsOpicsYchdService.batchCalibration("1",list);
		return RetMsgHelper.ok(slOutBean);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
