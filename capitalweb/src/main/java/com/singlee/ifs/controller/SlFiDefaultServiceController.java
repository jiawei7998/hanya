package com.singlee.ifs.controller;

import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.financial.page.PageInfo;
import com.singlee.ifs.model.SlFiDefault;
import com.singlee.ifs.service.SlFiDefaultService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.Map;

/**
 * @author Luozb
 * @version 1.0.0
 * @ClassName SlFiDefaultServiceController.java
 * @Description 债券违约控制层
 * @createTime 2021年09月29日 17:02:00
 */
@Controller
@RequestMapping(value = "/SlFiDefaultServiceController")
public class SlFiDefaultServiceController extends CommonController {
    @Autowired
    SlFiDefaultService slFiDefaultService;

    /*/**
     * @title searchPageSlFi
     * @description 分页查询
     * @author  Luozb
     * @updateTime 2021/9/29 0029 17:08
     * @throws
     */
    @ResponseBody
    @RequestMapping(value = "/searchPageSlFi")
    public RetMsg<PageInfo<SlFiDefault>> searchPageSlFi(@RequestBody Map<String, Object> map) {
        PageInfo<SlFiDefault> page = slFiDefaultService.searchPageSlFi(map);
        return RetMsgHelper.ok(page);
    }

    /**
     * 新增
     */
    @ResponseBody
    @RequestMapping(value = "/saveSlFiAdd")
    public RetMsg<Serializable> saveOpicsSetmAdd(@RequestBody SlFiDefault entity) {
        String msg = slFiDefaultService.insert(entity);
        if (msg.equals("成功")) {

            return RetMsgHelper.ok();
        }        return RetMsgHelper.ok(msg);
    }
    /**
     * 修改
     */
    @ResponseBody
    @RequestMapping(value = "/saveSlFiEdit")
    public RetMsg<Serializable> saveSlFiEdit(@RequestBody SlFiDefault entity) {
        slFiDefaultService.updateById(entity);
        return RetMsgHelper.ok();
    }

    /**
     * 删除
     */
    @ResponseBody
    @RequestMapping(value = "/deleteSetm")
    public RetMsg<Serializable> deleteSetm(@RequestBody SlFiDefault entity) {
        entity.setStatus("0");
        slFiDefaultService.deleteById(entity);
        return RetMsgHelper.ok();
    }

}
