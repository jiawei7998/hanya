package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.ifs.model.*;
import com.singlee.ifs.service.IFSService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Map;

/**
 * 人民币远期正式交易审批Controller
 * 
 * ClassName: IfsForeignController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason: TODO ADD REASON(可选). <br/>
 * date: 2018-5-30 下午07:35:59 <br/>
 * 
 * @author shenzl
 * @version
 * @since JDK 1.6
 */
@Controller
@RequestMapping(value = "/IfsForeignController")
public class IfsForeignController {
	@Autowired
	private IFSService iFSService;
	private static Logger logger = LoggerFactory.getLogger(IfsForeignController.class);

	// 分页查询
	@ResponseBody
	@RequestMapping(value = "/searchCreditEdit")
	public RetMsg<PageInfo<IfsCfetsfxFwd>> searchForm(@RequestBody Map<String, Object> map) {
		Page<IfsCfetsfxFwd> page = iFSService.getCreditEdit(map);
		return RetMsgHelper.ok(page);
	}

	// 查询
	@ResponseBody
	@RequestMapping(value = "/searchCredit")
	public RetMsg<IfsCfetsfxFwd> searchFormEdit(@RequestBody Map<String, String> map) {
		String ticketId = map.get("ticketId");
		IfsCfetsfxFwd page = iFSService.getCredit(ticketId);
		return RetMsgHelper.ok(page);
	}

	// 增加
	@ResponseBody
	@RequestMapping(value = "/addCreditEdit")
	public RetMsg<Serializable> addForm(@RequestBody IfsCfetsfxFwd map) {
		String message  = iFSService.checkCust(map.getDealTransType(),map.getCounterpartyInstId(), null,map.getCfetscn(), map.getProduct(), map.getProdType());
		if(StringUtil.isNotEmpty(message)) {
			return RetMsgHelper.fail(message);
		}
		message = iFSService.addCreditCondition(map);
		return StringUtil.containsIgnoreCase(message, "保存成功")?RetMsgHelper.ok(message):RetMsgHelper.fail(message);
	}

	// 修改
	@ResponseBody
	@RequestMapping(value = "/editCreditEdit")
	public RetMsg<Serializable> editFormEdit(@RequestBody IfsCfetsfxFwd map) {
		String message = iFSService.checkCust(map.getDealTransType(),map.getCounterpartyInstId(), null,map.getCfetscn(), map.getProduct(), map.getProdType());
		if(StringUtil.isNotEmpty(message)) {
			return RetMsgHelper.fail(message);
		}
		iFSService.editCredit(map);
		return RetMsgHelper.ok();
	}
	// 修改（批量要素更新）
	@ResponseBody
	@RequestMapping(value = "/editCreditEditUpdate")
	public RetMsg<Serializable> editCreditEditUpdate(@RequestBody IfsCfetsfxFwd map) {
		IfsCfetsfxFwd ifsCfetsfxFwd = iFSService.getCredit(map.getTicketId());
		ifsCfetsfxFwd.setPort(map.getPort());
		ifsCfetsfxFwd.setCost(map.getCost());
		ifsCfetsfxFwd.setProduct(map.getProduct());
		ifsCfetsfxFwd.setProdType(map.getProdType());
		iFSService.editCredit(ifsCfetsfxFwd);
		return RetMsgHelper.ok();
	}

	// 删除
	@ResponseBody
	@RequestMapping(value = "/deleteCreditEdit")
	public RetMsg<Serializable> deleteFormEdit(@RequestBody Map<String, String> map) {
		String ticketId = map.get("ticketid");
		iFSService.deleteCredit(ticketId);
		return RetMsgHelper.ok();
	}

	/**
	 * 根据主键查询成交单(返回值增加 流程id)
	 * 
	 * @param
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchFwdAndFlowIdById")
	public RetMsg<IfsCfetsfxFwd> searchFwdAndFlowIdById(@RequestBody Map<String, Object> key) {
		IfsCfetsfxFwd fwd = iFSService.getFwdAndFlowIdById(key);
		return RetMsgHelper.ok(fwd);
	}

	/**
	 * 外汇远期-查询我发起的
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-21
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageFxFwdMine")
	public RetMsg<PageInfo<IfsCfetsfxFwd>> searchPageFxFwdMine(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsfxFwd> page = iFSService.getFxFwdMinePage(params, 3);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 人民币外汇远期-查询 待审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageFxFwdUnfinished")
	public RetMsg<PageInfo<IfsCfetsfxFwd>> searchPageFxFwdUnfinished(@RequestBody Map<String, Object> params) {
		String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse + ","
				+ DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
		String[] orderStatus = approveStatus.split(",");
		params.put("approveStatusNo", orderStatus);
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsfxFwd> page = iFSService.getFxFwdMinePage(params, 1);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 人民币外汇远期-查询 已审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageFxFwdFinished")
	public RetMsg<PageInfo<IfsCfetsfxFwd>> searchPageFxFwdFinished(@RequestBody Map<String, Object> params) {
		String approveStatusNo = ParameterUtil.getString(params, "approveStatus", "");
		if (StringUtil.isNullOrEmpty(approveStatusNo)) {
			approveStatusNo = DictConstants.ApproveStatus.ApprovedPass;
			approveStatusNo += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatusNo += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatusNo += "," + DictConstants.ApproveStatus.Verified;
			approveStatusNo += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			approveStatusNo += "," + DictConstants.ApproveStatus.Approving;
			String[] orderStatus = approveStatusNo.split(",");
			params.put("approveStatusNo", orderStatus);
		}
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsfxFwd> page = iFSService.getFxFwdMinePage(params, 2);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 勾兑时，查询表中审批通过的数据
	 * 
	 * @param
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchPageApprovefxOptionPassed")
	public RetMsg<PageInfo<IfsCfetsfxOption>> searchPageApprovefxOptionPassed(@RequestBody Map<String, Object> params) {
		Page<IfsCfetsfxOption> page = iFSService.getApprovefxOptionPassedPage(params);
		return RetMsgHelper.ok(page);
	}

	/*
	 * 外汇掉期
	 */
	// 分页查询
	@ResponseBody
	@RequestMapping(value = "/searchrmswappage")
	public RetMsg<PageInfo<IfsCfetsfxSwap>> searchFormswapPage(@RequestBody Map<String, Object> map) {
		Page<IfsCfetsfxSwap> page = iFSService.getswapPage(map);
		return RetMsgHelper.ok(page);
	}

	// 查询
	@ResponseBody
	@RequestMapping(value = "/searchrmswap")
	public RetMsg<IfsCfetsfxSwap> searchFormswap(@RequestBody Map<String, Object> map) {
		String ticketId = map.get("ticketId").toString();
		IfsCfetsfxSwap page = iFSService.getrmswap(ticketId);
		return RetMsgHelper.ok(page);
	}

	// 增加
	@ResponseBody
	@RequestMapping(value = "/addrmswap")
	public RetMsg<Serializable> addrmswap(HttpServletRequest request, @RequestBody IfsCfetsfxSwap map) {
		String message  = iFSService.checkCust(map.getDealTransType(),map.getCounterpartyInstId(),null,map.getCfetscn(), map.getProduct(), map.getProdType());
		if(StringUtil.isNotEmpty(message)) {
			return RetMsgHelper.fail(message);
		}
		message = iFSService.addrmswap(map);
		return StringUtil.containsIgnoreCase(message, "保存成功")?RetMsgHelper.ok(message):RetMsgHelper.fail(message);
	}

	// 修改
	@ResponseBody
	@RequestMapping(value = "/editrmswap")
	public RetMsg<Serializable> editrmswap(@RequestBody IfsCfetsfxSwap map) {
		String message = iFSService.checkCust(map.getDealTransType(),map.getCounterpartyInstId(),null,map.getCfetscn(), map.getProduct(), map.getProdType());
		if(StringUtil.isNotEmpty(message)) {
			return RetMsgHelper.fail(message);
		}
		iFSService.editrmswap(map);
		return RetMsgHelper.ok();
	}

	// 删除
	@ResponseBody
	@RequestMapping(value = "/deletermswap")
	public RetMsg<Serializable> deletermswap(@RequestBody Map<String, String> map) {
		String ticketId = map.get("ticketid");
		iFSService.deletermswap(ticketId);
		return RetMsgHelper.ok();
	}

	/**
	 * 根据主键查询成交单(返回值增加 流程id)
	 * 
	 * @param
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchRmbSwapAndFlowIdById")
	public RetMsg<IfsCfetsfxSwap> searchRmbSwapAndFlowIdById(@RequestBody Map<String, Object> key) {
		IfsCfetsfxSwap swap = iFSService.getRmbSwapAndFlowIdById(key);
		return RetMsgHelper.ok(swap);
	}

	/**
	 * 人民币外汇掉期-查询我发起的
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-21
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageFxSwapMine")
	public RetMsg<PageInfo<IfsCfetsfxSwap>> searchPageFxSwapMine(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsfxSwap> page = iFSService.getFxSwapMinePage(params, 3);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 人民币外汇掉期-查询 待审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageFxSwapUnfinished")
	public RetMsg<PageInfo<IfsCfetsfxSwap>> searchPageFxSwapUnfinished(@RequestBody Map<String, Object> params) {
		String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse + ","
				+ DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
		String[] orderStatus = approveStatus.split(",");
		params.put("approveStatusNo", orderStatus);
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsfxSwap> page = iFSService.getFxSwapMinePage(params, 1);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 人民币外汇掉期-查询 已审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageFxSwapFinished")
	public RetMsg<PageInfo<IfsCfetsfxSwap>> searchPageFxSwapFinished(@RequestBody Map<String, Object> params) {
		String approveStatusNo = ParameterUtil.getString(params, "approveStatus", "");
		if (StringUtil.isNullOrEmpty(approveStatusNo)) {
			approveStatusNo = DictConstants.ApproveStatus.ApprovedPass;
			approveStatusNo += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatusNo += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatusNo += "," + DictConstants.ApproveStatus.Verified;
			approveStatusNo += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			approveStatusNo += "," + DictConstants.ApproveStatus.Approving;
			String[] orderStatus = approveStatusNo.split(",");
			params.put("approveStatusNo", orderStatus);
		}
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsfxSwap> page = iFSService.getFxSwapMinePage(params, 2);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 勾兑时，查询表中审批通过的数据
	 * 
	 * @param
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchPageApprovefxSptPassed")
	public RetMsg<PageInfo<IfsCfetsfxSpt>> searchPageApprovefxSptPassed(@RequestBody Map<String, Object> params) {
		Page<IfsCfetsfxSpt> page = iFSService.getApprovefxSptPassedPage(params);
		return RetMsgHelper.ok(page);
	}

	/*
	 * 外汇即期
	 */
	// 分页查询
	@ResponseBody
	@RequestMapping(value = "/searchSpotPage")
	public RetMsg<PageInfo<IfsCfetsfxSpt>> searchFormSpotPage(@RequestBody Map<String, Object> map) {
		Page<IfsCfetsfxSpt> page = iFSService.getSpotPage(map);
		return RetMsgHelper.ok(page);
	}

	// 查询
	@ResponseBody
	@RequestMapping(value = "/searchSpot")
	public RetMsg<IfsCfetsfxSpt> searchFormSpot(@RequestBody Map<String, Object> map) {
		String ticketId = map.get("ticketId").toString();
		IfsCfetsfxSpt page = iFSService.getSpot(ticketId);
		return RetMsgHelper.ok(page);
	}

	// 增加
	@ResponseBody
	@RequestMapping(value = "/addSpot")
	public RetMsg<Serializable> addSpot(@RequestBody IfsCfetsfxSpt map) {
		String message = iFSService.checkCust(map.getDealTransType(),map.getCounterpartyInstId(), null,map.getCfetscn(), map.getProduct(), map.getProdType());
		if(StringUtil.isNotEmpty(message)) {
			return RetMsgHelper.fail(message);
		}
		message = iFSService.addSpot(map);
		return StringUtil.containsIgnoreCase(message, "保存成功")?RetMsgHelper.ok(message):RetMsgHelper.fail(message);
	}

	// 修改
	@ResponseBody
	@RequestMapping(value = "/editSpot")
	public RetMsg<Serializable> editSpot(@RequestBody IfsCfetsfxSpt map) {
		String message = iFSService.checkCust(map.getDealTransType(),map.getCounterpartyInstId(),null,map.getCfetscn(), map.getProduct(), map.getProdType());
		if(StringUtil.isNotEmpty(message)) {
			return RetMsgHelper.fail(message);
		}
		//修改原始数据
		iFSService.editSpot(map);
		return RetMsgHelper.ok();
	}

	// 删除
	@ResponseBody
	@RequestMapping(value = "/deleteSpot")
	public RetMsg<Serializable> deleteSpot(@RequestBody Map<String, String> map) {
		String ticketId = map.get("ticketid");
		iFSService.deleteSpot(ticketId);
		return RetMsgHelper.ok();
	}

	/**
	 * 根据主键查询成交单(返回值增加 流程id)
	 * 
	 * @param
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchSpotAndFlowIdById")
	public RetMsg<IfsCfetsfxSpt> searchSpotAndFlowIdById(@RequestBody Map<String, Object> key) {
		IfsCfetsfxSpt spot = iFSService.getSpotAndFlowIdById(key);
		return RetMsgHelper.ok(spot);
	}

	/**
	 * 外汇即期-查询我发起的
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-21
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageSpotMine")
	public RetMsg<PageInfo<IfsCfetsfxSpt>> searchPageSpotMine(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsfxSpt> page = iFSService.getSpotMinePage(params, 3);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 人民币外汇即期-查询 待审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageSpotUnfinished")
	public RetMsg<PageInfo<IfsCfetsfxSpt>> searchPageSpotUnfinished(@RequestBody Map<String, Object> params) {
		String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse + ","
				+ DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
		String[] orderStatus = approveStatus.split(",");
		params.put("approveStatusNo", orderStatus);
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsfxSpt> page = iFSService.getSpotMinePage(params, 1);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 人民币外汇即期-查询 已审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageSpotFinished")
	public RetMsg<PageInfo<IfsCfetsfxSpt>> searchPageSpotFinished(@RequestBody Map<String, Object> params) {
		String approveStatusNo = ParameterUtil.getString(params, "approveStatus", "");
		if (StringUtil.isNullOrEmpty(approveStatusNo)) {
			approveStatusNo = DictConstants.ApproveStatus.ApprovedPass;
			approveStatusNo += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatusNo += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatusNo += "," + DictConstants.ApproveStatus.Verified;
			approveStatusNo += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			approveStatusNo += "," + DictConstants.ApproveStatus.Approving;
			String[] orderStatus = approveStatusNo.split(",");
			params.put("approveStatusNo", orderStatus);
		}
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsfxSpt> page = iFSService.getSpotMinePage(params, 2);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 勾兑时，查询表中审批通过的数据
	 * 
	 * @param
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchPageApprovefxFwdPassed")
	public RetMsg<PageInfo<IfsCfetsfxFwd>> searchPageApprovefxFwdPassed(@RequestBody Map<String, Object> params) {
		Page<IfsCfetsfxFwd> page = iFSService.getApprovefxFwdPassedPage(params);
		return RetMsgHelper.ok(page);
	}

	/*
	 * 人民币期权
	 */
	// 分页查询
	@ResponseBody
	@RequestMapping(value = "/searchOptionPage")
	public RetMsg<PageInfo<IfsCfetsfxOption>> searchOptionPage(@RequestBody Map<String, Object> map) {
		Page<IfsCfetsfxOption> page = iFSService.searchOptionPage(map);
		return RetMsgHelper.ok(page);
	}

	// 查询
	@ResponseBody
	@RequestMapping(value = "/searchOption")
	public RetMsg<IfsCfetsfxOption> searchOption(@RequestBody Map<String, Object> map) {
		String ticketId = map.get("ticketId").toString();
		IfsCfetsfxOption page = iFSService.searchOption(ticketId);
		return RetMsgHelper.ok(page);
	}

	// 增加
	@ResponseBody
	@RequestMapping(value = "/addOption")
	public String addOption(HttpServletRequest request, @RequestBody IfsCfetsfxOption map) {
		String message = iFSService.addOption(map);
		return message;
	}

	// 修改
	@ResponseBody
	@RequestMapping(value = "/editOption")
	public RetMsg<Serializable> editOption(@RequestBody IfsCfetsfxOption map) {
		iFSService.editOption(map);
		return RetMsgHelper.ok();
	}

	// 删除

	@ResponseBody
	@RequestMapping(value = "/deleteOption")
	public RetMsg<Serializable> deleteOption(@RequestBody Map<String, String> map) {
		String ticketId = map.get("ticketid");
		iFSService.deleteOption(ticketId);
		return RetMsgHelper.ok();
	}

	/**
	 * 人民币期权-查询我发起的
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-21
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageOptionMine")
	public RetMsg<PageInfo<IfsCfetsfxOption>> searchPageOptionMine(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsfxOption> page = iFSService.getOptionMinePage(params, 3);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 人民币期权-查询 待审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageOptionUnfinished")
	public RetMsg<PageInfo<IfsCfetsfxOption>> searchPageOptionUnfinished(@RequestBody Map<String, Object> params) {
		String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse + ","
				+ DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
		String[] orderStatus = approveStatus.split(",");
		params.put("approveStatusNo", orderStatus);
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsfxOption> page = iFSService.getOptionMinePage(params, 1);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 人民币期权-查询 已审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageOptionFinished")
	public RetMsg<PageInfo<IfsCfetsfxOption>> searchPageOptionFinished(@RequestBody Map<String, Object> params) {
		String approveStatusNo = ParameterUtil.getString(params, "approveStatus", "");
		if (StringUtil.isNullOrEmpty(approveStatusNo)) {
			approveStatusNo = DictConstants.ApproveStatus.ApprovedPass;
			approveStatusNo += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatusNo += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatusNo += "," + DictConstants.ApproveStatus.Verified;
			approveStatusNo += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			approveStatusNo += "," + DictConstants.ApproveStatus.Approving;
			String[] orderStatus = approveStatusNo.split(",");
			params.put("approveStatusNo", orderStatus);
		}
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsfxOption> page = iFSService.getOptionMinePage(params, 2);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 根据主键查询成交单(返回值增加 流程id)
	 * 
	 * @param
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchRmbOptAndFlowIdById")
	public RetMsg<IfsCfetsfxOption> searchRmbOptAndFlowIdById(@RequestBody Map<String, Object> key) {
		IfsCfetsfxOption opt = iFSService.searchRmbOptAndFlowIdById(key);
		return RetMsgHelper.ok(opt);
	}

	/**
	 * 根据主键查询成交单(返回值增加 流程id)
	 * 
	 * @param
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchRmbCcylendingAndFlowIdById")
	public RetMsg<IfsCfetsfxLend> searchRmbCcylendingAndFlowIdById(@RequestBody Map<String, Object> key) {
		IfsCfetsfxLend ccylend = iFSService.searchRmbCcylendingAndFlowIdById(key);
		return RetMsgHelper.ok(ccylend);
	}

	/**
	 * 根据主键查询成交单(返回值增加 流程id)
	 * 
	 * @param
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchRmbCcypaAndFlowIdById")
	public RetMsg<IfsCfetsfxAllot> searchRmbCcypaAndFlowIdById(@RequestBody Map<String, Object> key) {
		IfsCfetsfxAllot allot = iFSService.searchRmbCcypaAndFlowIdById(key);
		return RetMsgHelper.ok(allot);
	}

	/**
	 * 根据主键查询成交单(返回值增加 流程id)
	 * 
	 * @param
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchRmbSpotAndFlowIdById")
	public RetMsg<IfsCfetsfxOnspot> searchRmbSpotAndFlowIdById(@RequestBody Map<String, Object> key) {
		IfsCfetsfxOnspot onsport = iFSService.searchRmbSpotAndFlowIdById(key);
		return RetMsgHelper.ok(onsport);
	}

	/**
	 * 勾兑时，查询表中审批通过的数据
	 * 
	 * @param
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchPageApprovefxSwapPassed")
	public RetMsg<PageInfo<IfsCfetsfxSwap>> searchPageApprovefxSwapPassed(@RequestBody Map<String, Object> params) {
		Page<IfsCfetsfxSwap> page = iFSService.getApprovefxSwapPassedPage(params);
		return RetMsgHelper.ok(page);
		
	}
	
	
	//外汇类mini弹框
	@ResponseBody
	@RequestMapping(value = "/searchMiniPage")
	public RetMsg<PageInfo<Map<String, Object>>> searchMiniPage(@RequestBody Map<String, Object> params) {
		Page<Map<String, Object>> page = iFSService.getMini(params);
		return RetMsgHelper.ok(page);
	}
	
	//期权类mini弹框
	@ResponseBody
	@RequestMapping(value = "/searchOptMiniPage")
	public RetMsg<PageInfo<Map<String, Object>>> searchOptMiniPage(@RequestBody Map<String, Object> params) {
		Page<Map<String, Object>> page = iFSService.getOptMini(params);
		return RetMsgHelper.ok(page);
	}
	
	
	//根据ticketId查询详情--外汇远期
	@ResponseBody
	@RequestMapping(value = "/searchFwdDetailByTicketId")
	public RetMsg<IfsCfetsfxFwd> searchFwdDetailByTicketId(@RequestBody Map<String, Object> map) {
		IfsCfetsfxFwd fwd = iFSService.searchFwdDetailByTicketId(map);
		return RetMsgHelper.ok(fwd);
	}
	
	//根据ticketId查询详情--外币拆借
	@ResponseBody
	@RequestMapping(value = "/searchLendDetailByTicketId")
	public RetMsg<IfsCfetsfxLend> searchLendDetailByTicketId(@RequestBody Map<String, Object> map) {
		IfsCfetsfxLend lend = iFSService.searchLendDetailByTicketId(map);
		return RetMsgHelper.ok(lend);
	}
	
	//根据ticketId查询详情--外汇期权
	@ResponseBody
	@RequestMapping(value = "/searchOptionDetailByTicketId")
	public RetMsg<IfsCfetsfxOption> searchOptionDetailByTicketId(@RequestBody Map<String, Object> map) {
		IfsCfetsfxOption option = iFSService.searchOptionDetailByTicketId(map);
		return RetMsgHelper.ok(option);
	}
	
	//根据ticketId查询详情--外汇即期
	@ResponseBody
	@RequestMapping(value = "/searchSptDetailByTicketId")
	public RetMsg<IfsCfetsfxSpt> searchSptDetailByTicketId(@RequestBody Map<String, Object> map) {
		IfsCfetsfxSpt spt = iFSService.searchSptDetailByTicketId(map);
		return RetMsgHelper.ok(spt);
	}
	
	//根据ticketId查询详情--外汇掉期
	@ResponseBody
	@RequestMapping(value = "/searchSwapDetailByTicketId")
	public RetMsg<IfsCfetsfxSwap> searchSwapDetailByTicketId(@RequestBody Map<String, Object> map) {
		IfsCfetsfxSwap swap = iFSService.searchSwapDetailByTicketId(map);
		return RetMsgHelper.ok(swap);
	}
	
	//上传成交单附件
	@ResponseBody
	@RequestMapping(value = "/uploadFile", method = RequestMethod.POST, produces = "text/html;charset=UTF-8")
	public RetMsg<Serializable> uploadFile(MultipartHttpServletRequest request) {
		OutputStream out = null;
		InputStream in = null;
		try {
			logger.info("成交单附件上传...");
			MultipartFile mf = request.getFile("Fdata");
			if (null == mf) {
				return RetMsgHelper.simple("000010", "上传文件为空");
			}
			in = mf.getInputStream();
			String ticketId=request.getParameter("ticketId");
			String savePath = SystemProperties.upload_url+ticketId.substring(0, 8);
			File folder = new File(savePath);
			if (!folder.exists()) {
				folder.mkdirs();
			}
			//原文件名
			String filename = mf.getOriginalFilename();
			//后缀
			String prefix=filename.substring(filename.lastIndexOf(".")+1);
			if(!"pdf".equals(prefix)){
				return RetMsgHelper.simple("000010", "文件不是pdf格式");
			}
			//新文件名
			String newfilename=ticketId+"."+prefix;
			logger.info("上传文件路径:" + savePath + newfilename);
			out = new FileOutputStream(new File(savePath, newfilename));
			int length = 0;
			byte[] buf = new byte[1024];
			while ((length = in.read(buf)) != -1) {
				// in.read(buf) 每次读到的数据存放在 buf 数组中
				// 在 buf 数组中 取出数据 写到 （输出流）磁盘上
				out.write(buf, 0, length); 
			}
		} catch (IOException e) {
			logger.info("更新异常：" + e.getMessage());
			return RetMsgHelper.simple("000010", "更新异常:" + e.getMessage());
		} finally {
			try {
				if (null != in) {
					in.close();
				}
			} catch (IOException e) {
			}
			try {
				if (null != out) {
					out.close();
				}
			} catch (IOException e) {
			}
		}
		return RetMsgHelper.ok();
	}
	//下载成交单附件
	@RequestMapping(value = "/downloadFile/{ticketId}", method = RequestMethod.GET)
	public void downloadFile(@PathVariable String ticketId,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException, Throwable {
		String fileName = ticketId+".pdf";
		String path = SystemProperties.upload_url+ticketId.substring(0, 8)+"/"+fileName;
		File file=new File(path);
		if(!file.exists()){
			request.setAttribute("message", "您要下载的成交单附件不存在！");
			request.getRequestDispatcher("/standard/message.jsp").forward(request, response);
			return;
		}
		InputStream in=null;
		OutputStream out=null;
		try {
			response.setHeader("content-disposition", "attachment;filename="+fileName);
			
			//获取要下载的文件的绝对路径
			in=new FileInputStream(path);
            int len = 0;
             //创建数据缓冲区
             byte[] buffer = new byte[1024];
              //通过response对象获取OutputStream流
             out = response.getOutputStream();
              //将FileInputStream流写入到buffer缓冲区
              while ((len = in.read(buffer)) > 0) {//in.read(byte[] b)最多读入b.length个字节 在碰到流的结尾时 返回-1
              //使用OutputStream将缓冲区的数据输出到客户端浏览器
                  out.write(buffer,0,len);
             }
          }catch (FileNotFoundException e) {
             e.printStackTrace();
          }finally{
             in.close();
             out.close();
         }       
	}
	
}
