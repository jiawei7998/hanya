package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.ifs.model.IfsApprovemetalGold;
import com.singlee.ifs.service.IFSApproveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.io.Serializable;
import java.util.Map;

/**
 * 贵金属事前审批交易Controller
 * 
 * ClassName: IfsApprovemetalGoldController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason: TODO ADD REASON(可选). <br/>
 * date: 2018-5-30 下午07:08:32 <br/>
 * 
 * @author zhangcm
 * @version
 * @since JDK 1.6
 */
@Controller
@RequestMapping(value = "/IfsApprovemetalGoldController")
public class IfsApprovemetalGoldController extends CommonController {
	@Autowired
	private IFSApproveService cfetsmetalGoldService;

	/**
	 * 保存成交单
	 * 
	 * @param IfsCfetsmetalGold
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/saveGold")
	public RetMsg<Serializable> saveGold(@RequestBody IfsApprovemetalGold record) throws IOException {
		IfsApprovemetalGold gold = cfetsmetalGoldService.getGoldById(record);
		if (gold == null) {
			cfetsmetalGoldService.insertGold(record);
		} else {
			cfetsmetalGoldService.updateGold(record);
		}
		return RetMsgHelper.ok();
	}

	/**
	 * 成交单分页查询
	 * 
	 * @param IfsCfetsmetalGold
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchCfetsmetalGold")
	public RetMsg<PageInfo<IfsApprovemetalGold>> searchCfetsmetalGold(@RequestBody IfsApprovemetalGold record) {
		Page<IfsApprovemetalGold> page = cfetsmetalGoldService.getGoldList(record);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 根据主键查询成交单
	 * 
	 * @param IfsCfetsmetalGoldKey
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchCfetsmetalGoldById")
	public RetMsg<IfsApprovemetalGold> searchCfetsmetalGoldById(@RequestBody IfsApprovemetalGold key) {
		IfsApprovemetalGold gold = cfetsmetalGoldService.getGoldById(key);
		return RetMsgHelper.ok(gold);
	}

	/**
	 * 删除成交单
	 * 
	 * @param IfsCfetsmetalGoldKey
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/deleteCfetsmetalGold")
	public RetMsg<Serializable> deleteCfetsmetalGold(@RequestBody IfsApprovemetalGold key) {
		cfetsmetalGoldService.deleteGold(key);
		return RetMsgHelper.ok();
	}

	/**
	 * 查询我发起的
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-21
	 */
	@ResponseBody
	@RequestMapping(value = "/searchMetalGoldSwapMine")
	public RetMsg<PageInfo<IfsApprovemetalGold>> searchMetalGoldSwapMine(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsApprovemetalGold> page = cfetsmetalGoldService.getMetalGoldMinePage(params, 3);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 查询 待审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageMetalGoldUnfinished")
	public RetMsg<PageInfo<IfsApprovemetalGold>> searchPageMetalGoldUnfinished(@RequestBody Map<String, Object> params) {
		String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse + ","
				+ DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
		String[] orderStatus = approveStatus.split(",");
		params.put("approveStatusNo", orderStatus);
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsApprovemetalGold> page = cfetsmetalGoldService.getMetalGoldMinePage(params, 1);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 查询 已审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageMetalGoldFinished")
	public RetMsg<PageInfo<IfsApprovemetalGold>> searchPageMetalGoldFinished(@RequestBody Map<String, Object> params) {
		String approveStatusNo = ParameterUtil.getString(params, "approveStatus", "");
		if (StringUtil.isNullOrEmpty(approveStatusNo)) {
			approveStatusNo = DictConstants.ApproveStatus.ApprovedPass;
			approveStatusNo += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatusNo += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatusNo += "," + DictConstants.ApproveStatus.Verified;
			approveStatusNo += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			// 20180502修改bug:已审批列表中增加"审批中"
			approveStatusNo += "," + DictConstants.ApproveStatus.Approving;

			String[] orderStatus = approveStatusNo.split(",");
			params.put("approveStatusNo", orderStatus);
		}
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsApprovemetalGold> page = cfetsmetalGoldService.getMetalGoldMinePage(params, 2);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 根据主键关联流程查询成交单
	 * 
	 * @param IfsCfetsmetalGoldKey
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchCfetsmetalGoldAndFlowById")
	public RetMsg<IfsApprovemetalGold> searchCfetsmetalGoldAndFlowById(@RequestBody Map<String, Object> key) {
		IfsApprovemetalGold gold = cfetsmetalGoldService.getGoldAndFlowById(key);
		return RetMsgHelper.ok(gold);
	}

	/**
	 * 勾兑时，查询表中审批通过的数据
	 * 
	 * @param
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchPageApprovemetalGoldPassed")
	public RetMsg<PageInfo<IfsApprovemetalGold>> searchPageApprovemetalGoldPassed(@RequestBody Map<String, Object> params) {
		Page<IfsApprovemetalGold> page = cfetsmetalGoldService.getApprovemetalGoldPassedPage(params);
		return RetMsgHelper.ok(page);
	}
}
