package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.dict.service.DictionaryGetService;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.bean.SlSwiftBean;
import com.singlee.financial.opics.ISwiftServer;
import com.singlee.generated.model.field.Field21;
import com.singlee.ifs.baseUtil.SingleeFTPClient;
import com.singlee.ifs.baseUtil.SingleeSFTPClient;
import com.singlee.ifs.mapper.*;
import com.singlee.ifs.model.IfsSwift;
import com.singlee.ifs.service.IfsSwiftService;
import com.singlee.swift.model.SwiftMessage;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.File;
import java.io.PrintWriter;
import java.io.Serializable;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * SWIFT报文处理 Controller
 * 
 * ClassName: IfsSwiftController <br/>
 * date: 2018-5-30 下午07:48:45 <br/>
 * 
 * @author yubao
 * @version
 * @since JDK 1.6
 */
@Controller
@RequestMapping(value = "/IfsSwiftController")
public class IfsSwiftController {

	@Autowired
	private IfsSwiftService ifsSwiftService;

	@Autowired
	IfsSwiftMapper ifsSwiftMapper;

	@Autowired
	IfsCfetsfxSptMapper cfetsfxSptMapper;

	@Autowired
	IfsCfetsfxFwdMapper cfetsfxFwdMapper;

	@Autowired
	IfsCfetsfxSwapMapper cfetsfxSwapMapper;

	@Autowired
	IfsCfetsfxOptionMapper cfetsfxOptionMapper;

	@Autowired
	IfsCfetsfxLendMapper cfetsfxLendMapper;

	@Autowired
	IfsCfetsrmbIboMapper cfetsrmbIboMapper;

	@Autowired
	IfsCfetsrmbIrsMapper cfetsrmbIrsMapper;
	
	@Autowired
	ISwiftServer swiftServer;
	
	@Autowired
	DictionaryGetService dictionaryGetService;

	private static Logger logManager = LoggerFactory.getLogger(IfsSwiftController.class);

	@ResponseBody
	@RequestMapping(value = "/editHandling")
	public RetMsg<PageInfo<IfsSwift>> editHandling(@RequestBody Map<String, Object> map) {
		Page<IfsSwift> page = ifsSwiftService.getHandling(map);
		return RetMsgHelper.ok(page);
	}

	@ResponseBody
	@RequestMapping(value = "/verifyHandling")
	public RetMsg<PageInfo<IfsSwift>> verifyHandling(@RequestBody Map<String, Object> map) {
		Page<IfsSwift> page = ifsSwiftService.getVerifyHandling(map);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 退回到经办状态
	 */
	@ResponseBody
	@RequestMapping(value = "/backHandle")
	public String backHandle(@RequestBody Map<String, Object> map) {
		String msg = "";
		ParameterUtil.getString(map, "batchNO", "");
		String settflag = ifsSwiftMapper.searchSettflag(map);
		if ("1".equals(settflag)) { // 已发送不允许退回经办
			msg = "1"; // 不允许退回
		} else {
			msg = "0"; // 退回
		}
		 if ("0".equals(msg)) { // 退回经办
		//ifsSwiftMapper.backHandle(map);// 改变经办复核状态
		map.put("verind", map.get("type").toString());
		map.put("veroper", SlSessionHelper.getUserId());// 记录复核信息
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		map.put("verdate", df.format(new Date())); // 更新报文处理日期
		map.put("sendFlag", "2");
		ifsSwiftMapper.submitSwift(map);
		 }
		return msg;
	}

	/**
	 * 报文发送
	 */
	@ResponseBody
	@RequestMapping(value = "/sendMsg")
	public RetMsg<Serializable> sendMsg(@RequestBody Map<String, String> map) throws Exception {
		String br = map.get("br"); //机构号
		String batchNo = map.get("batchNo"); // 流水号
		batchNo.substring(0, batchNo.length() - 1);
		batchNo = "(" + batchNo + ")";
		// 返回前台信息
		StringBuffer content = new StringBuffer();
		// 查询swift报文
		Map<String, Object> mapB = new HashMap<String, Object>();
		mapB.put("batchNo", batchNo);
		mapB.put("br", br);
		
		// 获取需要查询的报文数据
		List<SlSwiftBean> swiftList = ifsSwiftMapper.getMsg(mapB);
		if("1".equals(swiftList.get(0).getSettFlag())){
			content=content.append("该笔报文已被处理");
			return RetMsgHelper.ok(content.toString()); 
		}
		
		/**
		 * settFlag:清算状态 1:已发送 2:未发送 3:发送异常
		 */
		for (SlSwiftBean swiftBean : swiftList) {
			// 清算状态为'未发送才能发送.其他跳过
			if (!"2".equals(swiftBean.getSettFlag()) && !"6".equals(swiftBean.getSettFlag())) {
				continue;
			}
			String fileName = MessageFormat.format("{0}{1}{2}{3}{4}{5}", swiftBean.getBr().trim(), swiftBean.getDealNo().trim(),
					swiftBean.getProdCode().trim(), swiftBean.getMsgType().trim(), swiftBean.getBatchNo().trim(), ".out");
			logManager.info("====SWIFT发送文件名"+fileName+"=====");
			// 上传文件备份
			String path = MessageFormat.format("{0}{1}", SystemProperties.swiftPath, "in/");
			File file = new File(path , fileName);
			if (!file.getParentFile().exists()) {
				file.getParentFile().mkdirs();
			}
			PrintWriter pw =new PrintWriter(file);
			pw.write(swiftBean.getMsg());
			pw.flush();
			pw.close();
			/* 文件上传 */
			logManager.info("====SWIFT发送"+fileName+"=====");	
			//初始化FTP
			SingleeSFTPClient sftp = new SingleeSFTPClient(SystemProperties.swiftIp, SystemProperties.swiftPort, 
					SystemProperties.swiftUserName, SystemProperties.swiftPassword);
			//开始连接
			sftp.connect();
			//上传文件  
			boolean flag = sftp.uploadFile(SystemProperties.swiftCatalog + "in/", fileName, path, fileName);
			
			Map<String, Object> upmap = new HashMap<String, Object>();
			upmap.put("batchNo", swiftBean.getBatchNo());
			upmap.put("br", swiftBean.getBr());
			String dealDate=DateUtil.format(new Date() , "yyyy-MM-dd HH:mm:ss");
			if (flag) { // 发送成功
				content.append(swiftBean.getDealNo() + "_" + swiftBean.getMsgType() + "报文发送成功<br/>");
				upmap.put("flg", "1");
				upmap.put("dealDate", dealDate); // 更新报文处理日期
				ifsSwiftMapper.updateSettFlag(upmap); // 发送成功后将清算状态改为1-已发送
				upmap.put("verind", "2");
				upmap.put("veroper", SlSessionHelper.getUserId());
				upmap.put("verdate", dealDate);
				ifsSwiftMapper.submitSwift(upmap);
			} else {
				//upmap.put("flg", "3");//发送失败进入重新发送队列 BY WZT 20191031
				upmap.put("flg", "2");//发送失败进入重新发送队列
				upmap.put("dealDate", dealDate); // 更新报文处理日期
				content.append(swiftBean.getDealNo() + "_" + swiftBean.getMsgType() + "报文发送失败 <br/>");
				ifsSwiftMapper.updateSettFlag(upmap); // 发送失败时将清算状态设为3-发送超时
			}
		}
		return RetMsgHelper.ok(content.toString());
	}

	/**
	 * 手动取回
	 */
	@ResponseBody
	@RequestMapping(value = "/manualGetbackSwift")
	public RetMsg<Serializable> manualGetbackSwift(@RequestBody Map<String, String> map) throws Exception {
		String forDate = ParameterUtil.getString(map, "forDate", "");
		String br = map.get("br");
		String dealno = map.get("dealno");
		String product = map.get("product");
		String prodtype = map.get("prodtype");
		// 查询swift报文
		Map<String, Object> map2 = new HashMap<String, Object>();
		map2.put("br", br);
		map2.put("dealno", dealno);
		map2.put("prodCode", product);
		map2.put("type", prodtype);// 因为202等其他报文这个值opics是空值
		map2.put("cdate", forDate);
		List<SlSwiftBean> swiftBeans = swiftServer.searchSwiftBySendflag(map2);
		for (SlSwiftBean slSwiftBean : swiftBeans) {
			//判断是否为  外汇  , 202类型报文
			slSwiftBean.setMsg(slSwiftBean.getMsg().replaceFirst("\r\n", ""));
			if("FXD".equals(slSwiftBean.getProdCode().trim())&"202".equals(slSwiftBean.getMsgType().trim())){
				SwiftMessage swift=SwiftMessage.parse(slSwiftBean.getMsg());
				String field=dictionaryGetService.getTaDictByCodeAndKey("SwiftTag21", "202").getDict_value();
				swift.getBlock4().getTagByName(Field21.NAME).setValue(field);//设置报文21域的值
				slSwiftBean.setMsg(swift.toMT().message());
			}
			try {
				ifsSwiftMapper.insertSwiftData(slSwiftBean);
			} catch (Exception e){
//				return RetMsgHelper.simple("RuntimeException",e.getMessage(),null);
				JY.raiseRException(e.getMessage());
			}

			Map<String, Object> map3 = new HashMap<String, Object>();
			map3.put("sendflag", "1");
			map3.put("br", StringUtils.trimToEmpty(slSwiftBean.getBr()));
			map3.put("dealno", StringUtils.trimToEmpty(slSwiftBean.getDealNo()));
			map3.put("batchno", StringUtils.trimToEmpty(slSwiftBean.getBatchNo()));
			// map3.put("seq", StringUtils.trimToEmpty(slSwiftBean.getseq()));
			map3.put("prodcode", StringUtils.trimToEmpty(slSwiftBean.getProdCode()));
			map3.put("cdate", slSwiftBean.getCdate());
			// map3.put("ctime",
			// StringUtils.trimToEmpty(slSwiftBean.getCtime()));
			map3.put("msgtype", StringUtils.trimToEmpty(slSwiftBean.getMsgType()));
			// map3.put("msgseq", StringUtils.trimToEmpty(slSwiftBean.getM));
			// map3.put("msgstatus",
			// StringUtils.trimToEmpty(slSwiftBean.getBr()));

			swiftServer.updateSendflag(map3);
		}

		/*
		 * String forDate = ParameterUtil.getString(map, "forDate", "");
		 * if(StringUtil.isEmpty(forDate)){ CommonSettleManager settleManager =
		 * new CommonSettleManager(); String br = map.get("br"); String dealno =
		 * map.get("dealno"); String product = map.get("product"); String
		 * prodtype = map.get("prodtype"); settleManager.addSwift(br, dealno,
		 * product, prodtype); }else{ CommonSettleManager settleManager = new
		 * CommonSettleManager();
		 * 
		 * Map<String, Object> riskMap = new HashMap<String, Object>();
		 * riskMap.put("forDate", forDate); //外汇即期 List<IfsCfetsfxSpt> listSpt2
		 * = cfetsfxSptMapper.searchSwiftNotExistByDate(riskMap); for
		 * (IfsCfetsfxSpt ifsCfetsfxSpt : listSpt2) { String br = "01"; String
		 * dealno = StringUtils.trimToEmpty(ifsCfetsfxSpt.getDealNo()); String
		 * product = StringUtils.trimToEmpty(ifsCfetsfxSpt.getProduct()); String
		 * prodtype = StringUtils.trimToEmpty(ifsCfetsfxSpt.getProdType());
		 * logManager
		 * .info("==========开始执行外汇即期swift dealno ="+dealno+"=============");
		 * settleManager.addSwift(br, dealno, product,prodtype); } //外汇远期
		 * List<IfsCfetsfxFwd> listFwd2 =
		 * cfetsfxFwdMapper.searchSwiftNotExistByDate(riskMap); for
		 * (IfsCfetsfxFwd ifsCfetsfxFwd : listFwd2) { String br = "01"; String
		 * dealno = StringUtils.trimToEmpty(ifsCfetsfxFwd.getDealNo()); String
		 * product = StringUtils.trimToEmpty(ifsCfetsfxFwd.getProduct()); String
		 * prodtype = StringUtils.trimToEmpty(ifsCfetsfxFwd.getProdType());
		 * logManager
		 * .info("==========开始执行外汇远期swift dealno ="+dealno+"=============");
		 * settleManager.addSwift(br, dealno, product,prodtype); } //外汇掉期
		 * List<IfsCfetsfxSwap> listSwap2 =
		 * cfetsfxSwapMapper.searchSwiftNotExistByDate(riskMap); for
		 * (IfsCfetsfxSwap ifsCfetsfxSwap : listSwap2) { String br = "01";
		 * String dealno = StringUtils.trimToEmpty(ifsCfetsfxSwap.getDealNo());
		 * String product =
		 * StringUtils.trimToEmpty(ifsCfetsfxSwap.getProduct()); String prodtype
		 * = StringUtils.trimToEmpty(ifsCfetsfxSwap.getProdType());
		 * logManager.info
		 * ("==========开始执行 外汇掉期swift dealno ="+dealno+"=============");
		 * settleManager.addSwift(br, dealno, product,prodtype); } //外汇期权
		 * List<IfsCfetsfxOption> listOption2 =
		 * cfetsfxOptionMapper.searchSwiftNotExistByDate(riskMap); for
		 * (IfsCfetsfxOption ifsCfetsfxOption : listOption2) { String br = "01";
		 * String dealno =
		 * StringUtils.trimToEmpty(ifsCfetsfxOption.getDealNo()); String product
		 * = StringUtils.trimToEmpty(ifsCfetsfxOption.getProduct()); String
		 * prodtype = StringUtils.trimToEmpty(ifsCfetsfxOption.getProdType());
		 * logManager
		 * .info("==========开始执行 外汇期权swift dealno ="+dealno+"=============");
		 * settleManager.addSwift(br, dealno, product,prodtype); } //外币拆借
		 * List<IfsCfetsfxLend> listLend2 =
		 * cfetsfxLendMapper.searchSwiftNotExistByDate(riskMap); for
		 * (IfsCfetsfxLend ifsCfetsfxLend : listLend2) { String br = "01";
		 * String dealno = StringUtils.trimToEmpty(ifsCfetsfxLend.getDealNo());
		 * String product =
		 * StringUtils.trimToEmpty(ifsCfetsfxLend.getProduct()); String prodtype
		 * = StringUtils.trimToEmpty(ifsCfetsfxLend.getProdType());
		 * logManager.info
		 * ("==========开始增加 外币拆借swift dealno ="+dealno+"=============");
		 * settleManager.addSwift(br, dealno, product,prodtype); } //信用拆借
		 * List<IfsCfetsrmbIbo> listIbo2 =
		 * cfetsrmbIboMapper.searchSwiftNotExistByDate(riskMap); for
		 * (IfsCfetsrmbIbo ifsCfetsrmbIbo : listIbo2) { String br = "01"; String
		 * dealno = StringUtils.trimToEmpty(ifsCfetsrmbIbo.getDealNo()); String
		 * product = StringUtils.trimToEmpty(ifsCfetsrmbIbo.getProduct());
		 * String prodtype =
		 * StringUtils.trimToEmpty(ifsCfetsrmbIbo.getProdType());
		 * logManager.info
		 * ("==========开始执行 信用拆借swift dealno ="+dealno+"=============");
		 * settleManager.addSwift(br, dealno, product,prodtype); } //利率互换
		 * List<IfsCfetsrmbIrs> listIrs2 =
		 * cfetsrmbIrsMapper.searchSwiftNotExistByDate(riskMap); for
		 * (IfsCfetsrmbIrs ifsCfetsrmbIrs : listIrs2) { String br = "01"; String
		 * dealno = StringUtils.trimToEmpty(ifsCfetsrmbIrs.getDealNo()); String
		 * product = StringUtils.trimToEmpty(ifsCfetsrmbIrs.getProduct());
		 * String prodtype =
		 * StringUtils.trimToEmpty(ifsCfetsrmbIrs.getProdType());
		 * logManager.info
		 * ("==========开始执行 利率互换swift dealno ="+dealno+"=============");
		 * settleManager.addSwift(br, dealno, product,prodtype); }
		 * 
		 * }
		 */
		return RetMsgHelper.ok();

	}

	/**
	 * 删除经办
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteSwift")
	public RetMsg<Serializable> deleteSwift(@RequestBody Map<String, Object> map) {
		// ifsSwiftService.deleteSwift(map);
		String oper = SlSessionHelper.getUserId();
		Date day = new Date();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String operDate = df.format(day);
		map.put("oper", oper);
		map.put("operDate", operDate);

		String row = ParameterUtil.getString(map, "row", "");
		for (String batchNo : row.split(",")) {
			map.put("batchNo", batchNo);
			ifsSwiftService.updateSwiftDelete(map);
		}
		return RetMsgHelper.ok();
	}

	/**
	 * 删除复合
	 */
	@ResponseBody
	@RequestMapping(value = "/updateSwiftComfirm")
	public RetMsg<Serializable> updateSwiftComfirm(@RequestBody Map<String, Object> map) {
		String sendFlag = ParameterUtil.getString(map, "sendFlag", "");// 复合状态，5同意，6拒绝
		String verifyOper = SlSessionHelper.getUserId();
		Date day = new Date();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String verifyOperDate = df.format(day);
		map.put("verifyOper", verifyOper);
		map.put("verifyOperDate", verifyOperDate);
		map.put("sendFlag", sendFlag);

		String row = ParameterUtil.getString(map, "row", "");
		for (String batchNo : row.split(",")) {
			map.put("batchNo", batchNo);
			ifsSwiftService.updateSwiftComfirm(map);
		}
		return RetMsgHelper.ok();
	}

	/**
	 * 复合驳回
	 */
	@ResponseBody
	@RequestMapping(value = "/returnSwiftComfirm")
	public RetMsg<Serializable> returnSwiftComfirm(@RequestBody Map<String, Object> map) {
		String sendFlag = ParameterUtil.getString(map, "sendFlag", "");// 复合状态，5同意，6拒绝
		String verifyOper = SlSessionHelper.getUserId();
		Date day = new Date();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String verifyOperDate = df.format(day);
		map.put("verifyOper", verifyOper);
		map.put("verifyOperDate", verifyOperDate);
		map.put("sendFlag", sendFlag);

		String row = ParameterUtil.getString(map, "row", "");
		for (String batchNo : row.split(",")) {
			map.put("batchNo", batchNo);
			ifsSwiftService.updateSwiftComfirm(map);
		}
		return RetMsgHelper.ok();
	}

	/**
	 * swift 发送经办
	 * 
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/subSwiftSettle")
	@ResponseBody
	public RetMsg<Serializable> subSwiftSettle(@RequestBody Map<String, Object> map) {
		RetMsg<Serializable> ret = new RetMsg<Serializable>(RetMsgHelper.codeOk, "经办成功");
		try {
			Map<String, Object> param = new HashMap<String, Object>();
			String batchNo = ParameterUtil.getString(map, "batchNo", ""); // 流水号
			String BR = ParameterUtil.getString(map, "br", ""); // 机构号
			IfsSwift ifsSwift = ifsSwiftMapper.querySwiftByBatchNo(map);
			ret.setObj(ifsSwift);
			if (!"0".equals(ifsSwift.getVerind()) && !StringUtils.isEmpty(ifsSwift.getiOper())) {// 已经办不能再经办
				ret.setCode("999999");
				ret.setDesc("您所确认的SWIFT交易信息:<br>账号:" + ifsSwift.getDealNo() + "</font></html><br>已经被: " + ifsSwift.getiOper() + "经办处理！<br>经办时间: "
						+ ifsSwift.getiDate());
				return ret;
			}
			param.put("batchNo", batchNo);
			param.put("br", BR);
			param.put("ioper", SlSessionHelper.getUserId());
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String dealDate = df.format(new Date());
			param.put("idate", dealDate); // 经办日期
			param.put("verind", "1");
			ifsSwiftMapper.submitSwift(param);// 更新经办复核状态
			return ret;
		} catch (Exception e) {
			return RetMsgHelper.simple("999999", "经办出错:" + e.getMessage());
		}
	}

	/**
	 * 查询报文状态
	 * 
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/searchSwiftStatu")
	@ResponseBody
	public RetMsg<Serializable> searchSwiftStatu(@RequestBody Map<String, Object> map) {
		RetMsg<Serializable> ret = new RetMsg<Serializable>(RetMsgHelper.codeOk, "查询成功");
		try {
			String date = DateUtil.format(new Date(),"yyyyMMdd");
			logManager.info("====SWIFT远程备份文件名"+date+"=====");
			/** 本地备份路径 **/
			String localFile = MessageFormat.format("{0}{1}", SystemProperties.swiftPath, "ack");
			String remotePath = MessageFormat.format("{0}/{1}", SystemProperties.swiftCatalog + "/OPICS/INPUT", "ACK");
			// 下载文件
			logManager.info("====SWIFT处理状态更新=====");
			boolean flg = SingleeFTPClient.downFile(SystemProperties.swiftIp, SystemProperties.swiftPort, SystemProperties.swiftUserName,
					SystemProperties.swiftPassword, remotePath, localFile, date,"ifsSwiftServiceImpl");
			if(!flg){
				ret.setDesc("查询出错或无ack文件");
			}
			return ret;
		} catch (Exception e) {
			return RetMsgHelper.simple("000010", "查询出错:" + e.getMessage());
		}
	}
	/**
	 * 生成299报文
	 * 
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/swiftBondCreat")
	@ResponseBody
	public RetMsg<Serializable> swiftBondCreat(@RequestBody Map<String, Object> map) {
		try {
			return ifsSwiftService.swiftBondCreat(map);
		} catch (Exception e) {
			return RetMsgHelper.simple("999999", "报文生成出错:" + e.getMessage());
		}
	}
	/**
	 * 查询299报文
	 * 
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/swiftBondSearch")
	@ResponseBody
	public Map<String,Object> swiftBondSearch(@RequestBody Map<String, Object> map) {
			return ifsSwiftService.getSwiftBond(map);
	}
}