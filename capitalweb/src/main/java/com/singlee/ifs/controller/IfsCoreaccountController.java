package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.ifs.model.IfsCoreaccount;
import com.singlee.ifs.service.IfsCoreaccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * @author Luozb
 * @version 1.0.0
 * @ClassName IfsCoreaccountController.java
 * @Description 日终对账
 * @createTime 2021年10月18日 11:03:00
 */
@Controller
@RequestMapping(value = "/IfsCoreaccountController")
public class IfsCoreaccountController extends CommonController {

    @Autowired
    IfsCoreaccountService ifsCoreaccountService;
    /**
     * 日终对账查询
     * @param map
     * @return
     *
     *
     */
    @ResponseBody
    @RequestMapping(value = "/searchpage")
    public RetMsg<PageInfo<IfsCoreaccount>> searchpage(@RequestBody Map<String, Object> map) {
        Page<IfsCoreaccount> page = ifsCoreaccountService.getList(map);
        return RetMsgHelper.ok(page);
    }
}
