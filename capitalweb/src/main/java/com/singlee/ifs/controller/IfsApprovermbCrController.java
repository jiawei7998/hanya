package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.ifs.model.IfsApprovermbCr;
import com.singlee.ifs.service.IFSApproveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.io.Serializable;
import java.util.Map;

/**
 * 质押式回购事前审批交易Controller
 * 
 * ClassName: IfsApprovermbCrController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason: TODO ADD REASON(可选). <br/>
 * date: 2018-5-30 下午07:10:04 <br/>
 * 
 * @author zhangcm
 * @version
 * @since JDK 1.6
 */
@Controller
@RequestMapping(value = "/IfsApprovermbCrController")
public class IfsApprovermbCrController extends CommonController {
	@Autowired
	private IFSApproveService cfetsrmbCrService;

	/**
	 * 保存成交单
	 * 
	 * @param IfsCfetsrmbCr
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/saveCr")
	public RetMsg<Serializable> saveCr(@RequestBody IfsApprovermbCr record) throws IOException {
		IfsApprovermbCr cr = cfetsrmbCrService.getCrById(record);
		if (cr == null) {
			cfetsrmbCrService.insertCr(record);
		} else {
			cfetsrmbCrService.updateCr(record);
		}
		return RetMsgHelper.ok();
	}

	/**
	 * 成交单分页查询
	 * 
	 * @param IfsCfetsrmbCr
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchCfetsrmbCr")
	public RetMsg<PageInfo<IfsApprovermbCr>> searchCfetsrmbCr(@RequestBody IfsApprovermbCr record) {
		Page<IfsApprovermbCr> page = cfetsrmbCrService.getCrList(record);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 根据主键查询成交单
	 * 
	 * @param IfsCfetsrmbCrKey
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchCfetsrmbCrById")
	public RetMsg<IfsApprovermbCr> searchCfetsrmbCrById(@RequestBody IfsApprovermbCr key) {
		IfsApprovermbCr cr = cfetsrmbCrService.getCrById(key);
		return RetMsgHelper.ok(cr);
	}

	/**
	 * 删除成交单
	 * 
	 * @param IfsCfetsrmbCrKey
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/deleteCfetsrmbCr")
	public RetMsg<Serializable> deleteCfetsrmbCr(@RequestBody IfsApprovermbCr key) {
		cfetsrmbCrService.deleteCr(key);
		return RetMsgHelper.ok();
	}

	/**
	 * 查询我发起的
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-21
	 */
	@ResponseBody
	@RequestMapping(value = "/searchRmbCrSwapMine")
	public RetMsg<PageInfo<IfsApprovermbCr>> searchRmbCrSwapMine(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsApprovermbCr> page = cfetsrmbCrService.getRmbCrMinePage(params, 3);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 查询 待审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageRmbCrUnfinished")
	public RetMsg<PageInfo<IfsApprovermbCr>> searchPageRmbCrUnfinished(@RequestBody Map<String, Object> params) {
		String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse + ","
				+ DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
		String[] orderStatus = approveStatus.split(",");
		params.put("approveStatusNo", orderStatus);
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsApprovermbCr> page = cfetsrmbCrService.getRmbCrMinePage(params, 1);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 查询 已审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageRmbCrFinished")
	public RetMsg<PageInfo<IfsApprovermbCr>> searchPageRmbCrFinished(@RequestBody Map<String, Object> params) {
		String approveStatusNo = ParameterUtil.getString(params, "approveStatus", "");
		if (StringUtil.isNullOrEmpty(approveStatusNo)) {
			approveStatusNo = DictConstants.ApproveStatus.ApprovedPass;
			approveStatusNo += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatusNo += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatusNo += "," + DictConstants.ApproveStatus.Verified;
			approveStatusNo += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			// 20180502修改bug:已审批列表中增加"审批中"
			approveStatusNo += "," + DictConstants.ApproveStatus.Approving;
			String[] orderStatus = approveStatusNo.split(",");
			params.put("approveStatusNo", orderStatus);
		}
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsApprovermbCr> page = cfetsrmbCrService.getRmbCrMinePage(params, 2);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 根据主键查询成交单(返回值增加 流程id)
	 * 
	 * @param
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchCfetsrmbCrAndFlowIdById")
	public RetMsg<IfsApprovermbCr> searchCfetsrmbCrAndFlowIdById(@RequestBody Map<String, Object> key) {
		IfsApprovermbCr cr = cfetsrmbCrService.getCrAndFlowIdById(key);
		return RetMsgHelper.ok(cr);
	}

	/**
	 * 勾兑时，查询表中审批通过的数据
	 * 
	 * @param
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchPageApprovermbCrPassed")
	public RetMsg<PageInfo<IfsApprovermbCr>> searchPageApprovermbCrPassed(@RequestBody Map<String, Object> params) {
		Page<IfsApprovermbCr> page = cfetsrmbCrService.getApproveCrPassedPage(params);
		return RetMsgHelper.ok(page);
	}
}
