package com.singlee.ifs.controller;

import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.financial.bean.SlCnapsBean;
import com.singlee.financial.esb.hsbank.ICnapsServer;
import com.singlee.financial.page.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;
@Controller
@RequestMapping(value = "/CnapsController")
public class IfsOpicsCnapsController {
	@Autowired
	ICnapsServer  cnapsServer;
	/**
	 * 大额清算查询
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchCnapsList")
	public RetMsg<PageInfo<SlCnapsBean>> searchCnapsList(@RequestBody Map<String, Object> params) {
		PageInfo<SlCnapsBean> page = cnapsServer.getAcupList(params);
		return RetMsgHelper.ok(page);
		 
	}
}
