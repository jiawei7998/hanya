package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.credit.util.ExcelUtil;
import com.singlee.capital.dayend.model.TtDayendDate;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.dict.ExternalDictConstants;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.bean.*;
import com.singlee.financial.common.util.DateUtil;
import com.singlee.financial.esb.dlcb.IAcupGetServer;
import com.singlee.financial.esb.dlcb.IAcupSendServer;
import com.singlee.financial.esb.hsbank.ICoreServer;
import com.singlee.financial.esb.tlcb.IFileServer;
import com.singlee.financial.esb.tlcb.ITyjzdjServer;
import com.singlee.financial.esb.tlcb.ITyjztjcxServer;
import com.singlee.financial.opics.IAcupServer;
import com.singlee.financial.opics.IBaseServer;
import com.singlee.financial.opics.IFxdServer;
import com.singlee.financial.page.PageInfo;
import com.singlee.financial.pojo.component.PsEnum;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.FxCurrencyBean;
import com.singlee.financial.pojo.trade.InstitutionBean;
import com.singlee.financial.pojo.trade.SlErrors;
import com.singlee.ifs.mapper.IfsOpicsBondMapper;
import com.singlee.ifs.model.IfsBatchStep;
import com.singlee.ifs.model.IfsNbhBean;
import com.singlee.ifs.model.IfsNbhGatherBean;
import com.singlee.ifs.service.IFSService;
import com.singlee.ifs.service.IfsOpicsAcupOperService;
import com.singlee.ifs.service.IfsOpicsAcupService;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.CellStyle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.lang.management.ManagementFactory;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * OPICS总账业务处理Controller
 *
 * ClassName: IfsOpicsAcupController <br/>
 * Function: 主要处理OPICS日终批后操作 <br/>
 * Reason: 提供OPICS接口操作. <br/>
 * date: 2018-5-30 下午07:02:46 <br/>
 *
 * @author SINGLEE
 * @version
 * @since JDK 1.6
 */
@Controller
@RequestMapping(value = "/IfsOpicsAcupController")
public class IfsOpicsAcupController extends CommonController {

	private static Logger logger = Logger.getLogger(IfsOpicsAcupController.class);

	/**
	 * OPICS服务接口
	 *
	 */
	@Autowired
	private IAcupServer acupServer;

	@Autowired
	private IBaseServer baseServer;

	@Autowired
	private IfsOpicsAcupService opicsAcupService;

	@Autowired
	IFileServer iFileServer;

	@Autowired
	private ICoreServer coreServer;

	@Autowired
	private ITyjzdjServer tyjzdjServer;

	@Autowired
	private IAcupSendServer acupSendServer;

	@Autowired
	private IAcupGetServer acupGetServer;

	@Autowired
	private ITyjztjcxServer tyjztjcxServer;

	@Autowired
	IFSService ifsServvice;

	@Autowired
	DayendDateService dayendDateService;

	@Autowired
	IfsOpicsBondMapper ifsOpicsBondMapper;

	@Autowired
	IFxdServer fxdServer;

	@Autowired
	private IFSService ifsService; // 正式交易台

	@Autowired
	IfsOpicsAcupOperService ifsOpicsAcupOperService;

	/**
	 * 总账数据生成
	 *
	 */
	@ResponseBody
	@RequestMapping(value = "/generateData")
	public RetMsg<SlOutBean> generateData(@RequestBody Map<String, String> params) {
		SlOutBean msg = new SlOutBean();
		try {
			msg = acupServer.callAcupProc(params);
		} catch (RemoteConnectFailureException e) {
			msg.setRetCode(SlErrors.FAILED_RMI_OPICS.getErrCode());
			msg.setRetMsg(SlErrors.FAILED_RMI_OPICS.getErrMsg() + ":" + e.getMessage());
			msg.setRetStatus(RetStatusEnum.F);
		} catch (Exception e) {
			msg.setRetCode(SlErrors.FAILED.getErrCode());
			msg.setRetMsg(SlErrors.FAILED.getErrMsg() + ":" + e.getMessage());
			msg.setRetStatus(RetStatusEnum.F);
		}
		return RetMsgHelper.ok(msg);
	}

	/**
	 * 总账自动发送(哈尔滨使用)
	 *
	 */
	@ResponseBody
	@RequestMapping(value = "/autoSendAcup")
	public RetMsg<PageInfo<IfsBatchStep>> autoSendAcup(@RequestBody Map<String, String> map) throws Exception {

		PageInfo<IfsBatchStep> page = opicsAcupService.autoSendAcup(map);

		return RetMsgHelper.ok(page);
	}

	/**
	 * 总账数据生成(哈尔滨使用)
	 *
	 */
	@ResponseBody
	@RequestMapping(value = "/createAcup")
	public RetMsg<SlOutBean> createAcup(@RequestBody Map<String, String> params) {
		SlOutBean msg = new SlOutBean();
		try {
			msg = acupServer.createAcupProc(params);
		} catch (RemoteConnectFailureException e) {
			msg.setRetCode(SlErrors.FAILED_RMI_OPICS.getErrCode());
			msg.setRetMsg(SlErrors.FAILED_RMI_OPICS.getErrMsg() + ":" + e.getMessage() + "\n");
			msg.setRetStatus(RetStatusEnum.F);
		} catch (Exception e) {
			msg.setRetCode(SlErrors.FAILED.getErrCode());
			msg.setRetMsg(SlErrors.FAILED.getErrMsg() + ":" + e.getMessage() + "\n");
			msg.setRetStatus(RetStatusEnum.F);
		}
		return RetMsgHelper.ok(msg);
	}

	/**
	 * 检查总账是否已经上送   生成（哈尔滨使用）
	 *
	 */
	@ResponseBody
	@RequestMapping(value = "/checkSendFlag")
	public RetMsg<SlOutBean> checkSendFlag(@RequestBody Map<String, String> params) {
		SlOutBean msg = new SlOutBean();
		try {
			msg = acupServer.checkSendFlag(params.get("postdate"));
		} catch (RemoteConnectFailureException e) {
			msg.setRetCode(SlErrors.FAILED_RMI_OPICS.getErrCode());
			msg.setRetMsg(SlErrors.FAILED_RMI_OPICS.getErrMsg() + ":" + e.getMessage());
			msg.setRetStatus(RetStatusEnum.F);
		} catch (Exception e) {
			msg.setRetCode(SlErrors.FAILED.getErrCode());
			msg.setRetMsg(SlErrors.FAILED.getErrMsg() + ":" + e.getMessage());
			msg.setRetStatus(RetStatusEnum.F);
		}
		return RetMsgHelper.ok(msg);
	}

	/**
	 * 总账文件生成,上送至综合前置，通知核心记账(哈尔滨使用)
	 *
	 */
	@ResponseBody
	@RequestMapping(value = "/createDlAcupFile")
	public RetMsg<SlOutBean> createDlAcupFile(@RequestBody SlAcupBean acupBean) {
		acupBean.setUsername(SlSessionHelper.getUserId());
		SlOutBean msg = new SlOutBean();
		try {
			//msg = acupSendServer.acupSend(acupBean);

			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("coreDate",acupBean.getEffDate());
			msg = ifsOpicsAcupOperService.acupSend(parameters);

		} catch (Exception e) {
			msg.setRetCode(SlErrors.FAILED_RMI_OPICS.getErrCode());
			msg.setRetMsg(SlErrors.FAILED_RMI_OPICS.getErrMsg() + ":" + "Cannot connect to Hessian");
			msg.setRetStatus(RetStatusEnum.F);
			logger.info(e);
		}
		return new RetMsg<SlOutBean>(msg.getRetCode(), msg.getRetMsg());
	}

	/**
	 * 总账取回(哈尔滨使用)
	 *
	 */
	@ResponseBody
	@RequestMapping(value = "/getDlAcupResultPath")
	public RetMsg<SlOutBean> getDlAcupResultPath(@RequestBody SlAcupBean acupBean) {
		// 调用总账取回接口
		acupBean.setUsername(SlSessionHelper.getUserId());
		//SlOutBean msg  = acupGetServer.acupGet(acupBean);

		SlOutBean msg = null;
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("coreDate",acupBean.getEffDate());

		try {
			msg = ifsOpicsAcupOperService.acupGet(parameters);
		} catch (RemoteConnectFailureException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if ("success".equals(msg.getClientNo())) { // 取回成功后，更新日期
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date opicsDate = new Date();
			try {
				/* 取OPICS系统时间 */
				opicsDate = baseServer.getOpicsSysDate("01");
				Map<String, String> map2 = new HashMap<String, String>();
				map2.put("RETCODE", "");
				map2.put("RETMSG", "");
			} catch (Exception e) {
				logger.info(e);
			}
			String date = sdf.format(opicsDate); // 转成字符串格式
			TtDayendDate tdd = this.dayendDateService.getTtDayendDate();
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("newStatus", ExternalDictConstants.DayendDateStatus_Normal);
			map.put("newDate", date);
			map.put("curDate", tdd.getCurDate());
			map.put("status", tdd.getStatus());
			map.put("version", Integer.valueOf(tdd.getVersion()));
			/* 更新TT_DAYEND_DATE表日期 */
			dayendDateService.updateTtDayendDate(map);

		}
		return new RetMsg<SlOutBean>(msg.getRetCode(), msg.getRetMsg());
	}

	/**
	 * 总账文件生成,上送至ESB，通知核心记账
	 *
	 */
	@ResponseBody
	@RequestMapping(value = "/sendCore")
	public RetMsg<SlOutBean> sendCore(@RequestBody Map<String, String> params) {
		SlOutBean msg = coreServer.sendCore(params);
		return RetMsgHelper.ok(msg);
	}

	/**
	 * 总账取回
	 *
	 */
	@ResponseBody
	@RequestMapping(value = "/getAcupResultPath")
	public RetMsg<SlOutBean> reviceCoreResult(@RequestBody Map<String, String> map) {
		// 调用总账发送接口
		SlOutBean msg = coreServer.reviceCoreResult(map);
		return RetMsgHelper.ok(msg);
	}

	/**
	 * 总账文件生成,上送至ESB，通知核心记账(泰隆)
	 *
	 */
	@ResponseBody
	@RequestMapping(value = "/createTlAcupFile")
	public RetMsg<SlOutBean> createTlAcupFile(@RequestBody SlAcupBean acupBean) {
		acupBean.setUsername(SlSessionHelper.getUserId());
		SlOutBean msg = tyjzdjServer.w060(acupBean);
		return new RetMsg<SlOutBean>(msg.getRetCode(), msg.getRetMsg());
	}

	/**
	 * 总账取回(TLBANK)
	 *
	 */
	@ResponseBody
	@RequestMapping(value = "/getTlAcupResultPath")
	public RetMsg<SlOutBean> getTlAcupResultPath(@RequestBody SlAcupBean acupBean) {
		// 调用总账发送接口
		acupBean.setUsername(SlSessionHelper.getUserId());
		SlOutBean msg = tyjztjcxServer.w061(acupBean);
		if ("success".equals(msg.getClientNo())) { // 取回成功后，更新日期
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date opicsDate = new Date();
			try {
				/* 取OPICS系统时间 */
				opicsDate = baseServer.getOpicsSysDate("01");
				Map<String, String> map2 = new HashMap<String, String>();
				map2.put("RETCODE", "");
				map2.put("RETMSG", "");
				baseServer.callSdInvestHistory(map2);
				baseServer.callCdInvestHistory(map2);
				baseServer.callSdProfitLoss(map2);
				baseServer.callCdProfitLoss(map2);
			} catch (Exception e) {
				logger.info(e);
			}
			String date = sdf.format(opicsDate); // 转成字符串格式
			TtDayendDate tdd = this.dayendDateService.getTtDayendDate();
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("newStatus", ExternalDictConstants.DayendDateStatus_Normal);
			map.put("newDate", date);
			map.put("curDate", tdd.getCurDate());
			map.put("status", tdd.getStatus());
			map.put("version", Integer.valueOf(tdd.getVersion()));
			/* 更新TT_DAYEND_DATE表日期 */
			dayendDateService.updateTtDayendDate(map);

			/**
			 * 调用shell脚本
			 */
			String filepath = SystemProperties.shpath; // 读取配置文件
			StringBuffer sb = new StringBuffer();
			BufferedReader br = null;
			String name = ManagementFactory.getRuntimeMXBean().getName();
			String pid = name.split("@")[0];
			try {
				logger.info("Starting to exec{" + filepath + "}. PID is: " + pid);
				Process process = null;
				ProcessBuilder pb = new ProcessBuilder("/bin/bash", "-c", filepath);
				pb.environment();
				pb.redirectErrorStream(true);
				process = pb.start();
				if (process != null) {
					br = new BufferedReader(new InputStreamReader(process.getInputStream()), 1024);
					process.waitFor();
				} else {
					logger.info("There is no PID found.");
				}
				sb.append("Ending exec right now, the result is：\n");
				String line = null;
				while (br != null && (line = br.readLine()) != null) {
					sb.append(line).append("\n");
				}
			} catch (Exception ioe) {
				sb.append("Error occured when exec cmd：\n").append(ioe.getMessage()).append("\n");
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						logger.info(e);
					}
				}
			}
			logger.info(sb.toString());
		}
		return new RetMsg<SlOutBean>(msg.getRetCode(), msg.getRetMsg());
	}

	/**
	 * 错账查询
	 *
	 */
	@ResponseBody
	@RequestMapping(value = "/searchAcupList")
	public RetMsg<PageInfo<SlAcupBean>> searchAcupList(@RequestBody Map<String, String> params) {
		// 查询全部
		PageInfo<SlAcupBean> page = null;
		try {
			page = acupServer.getAcupList(params);
		} catch (RemoteConnectFailureException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return RetMsgHelper.ok(page);
	}

	/**
	 * 错账查询
	 *
	 */
	@ResponseBody
	@RequestMapping(value = "/searchAcupById")
	public RetMsg<List<SlAcupBean>> searchAcupById(@RequestBody Map<String, String> params) {
		// 调用总账发送接口
		List<SlAcupBean> list = null;
		try {
			list = acupServer.getAcupById(params);
		} catch (RemoteConnectFailureException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return RetMsgHelper.ok(list);
	}

	/**
	 * 查询日期并同步日期
	 *
	 */
	@ResponseBody
	@RequestMapping(value = "/acupOpicsDate")
	public String queryAcupDate(@RequestBody Map<String, String> params) {
		try {
			Date postdate = baseServer.getOpicsSysDate(params.get("br"));
			TtDayendDate tdd = this.dayendDateService.getTtDayendDate();

			if (postdate.equals(DateUtil.parse(tdd.getCurDate()))) {// 相等则不进行更新
				return tdd.getCurDate();
			}
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("newDate", DateUtil.format(postdate));
			map.put("curDate", tdd.getCurDate());
			map.put("status", tdd.getStatus());
			map.put("version", Integer.valueOf(tdd.getVersion()));
			/* 更新TT_DAYEND_DATE表日期 */
			dayendDateService.updateTtDayendDate(map);
			return DateUtil.format(postdate);

		} catch (RemoteConnectFailureException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

	@ResponseBody
	@RequestMapping(value = "/acupOpicsBr")
	public RetMsg<List<Map<String, String>>> queryBrs(@RequestBody Map<String, String> params) {
		List<Map<String, String>> map = acupServer.getOpicsBr(params);
		return RetMsgHelper.ok(map);
	}

	/**
	 * 查询日期(泰隆使用)
	 *
	 */
	@ResponseBody
	@RequestMapping(value = "/acupFileMaker")
	public String acupFileMaker(@RequestBody Map<String, String> params) {
		try {
			return acupServer.getAcupDate(params);
		} catch (Exception e) {
			logger.info(e);
		}
		return "";
	}

	@ResponseBody
	@RequestMapping(value = "/sendValid")
	public RetMsg<SlOutSonBean> sendValid(@RequestBody Map<String, String> params) {
		SlOutSonBean msg = new SlOutSonBean();
		try {
			msg = acupServer.sendValid(params);
		} catch (RemoteConnectFailureException e) {
			msg.setRetCode(SlErrors.FAILED_RMI_OPICS.getErrCode());
			msg.setRetMsg(SlErrors.FAILED_RMI_OPICS.getErrMsg() + ":" + e.getMessage());
			msg.setRetStatus(RetStatusEnum.F);
		} catch (Exception e) {
			msg.setRetCode(SlErrors.FAILED.getErrCode());
			msg.setRetMsg(SlErrors.FAILED.getErrMsg() + ":" + e.getMessage());
			msg.setRetStatus(RetStatusEnum.F);
		}
		return RetMsgHelper.ok(msg);
	}

	@ResponseBody
	@RequestMapping(value = "/queryOperaRec")
	public RetMsg<SlOutSonBean> queryOperaRec(@RequestBody Map<String, String> params) {
		SlOutSonBean msg = new SlOutSonBean();
		try {
			List<SlAcupRecord> list = acupServer.queryOperaRec(params);
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put("records", list);
			msg.setRetCode("error.common.0000");
			msg.setRetStatus(RetStatusEnum.S);
			msg.setRetMsg("查询成功");
			msg.setDataMap(dataMap);
		} catch (RemoteConnectFailureException e) {
			msg.setRetCode(SlErrors.FAILED_RMI_OPICS.getErrCode());
			msg.setRetMsg(SlErrors.FAILED_RMI_OPICS.getErrMsg() + ":" + e.getMessage());
			msg.setRetStatus(RetStatusEnum.F);
		} catch (Exception e) {
			msg.setRetCode(SlErrors.FAILED.getErrCode());
			msg.setRetMsg(SlErrors.FAILED.getErrMsg() + ":" + e.getMessage());
			msg.setRetStatus(RetStatusEnum.F);
		}
		return RetMsgHelper.ok(msg);
	}

	/**
	 * 总账查询
	 */
	@ResponseBody
	@RequestMapping(value = "/ledgerQuery")
	public RetMsg<PageInfo<SlAcupBean>> ledgerQuery(@RequestBody Map<String, String> params) throws Exception {

		String PREVBRANPRCDATE = acupServer.getAcupDate(params);
		// 如果没有过账日期,设置一个opics的上一总账日期为过账日期
		if ("".equals(params.get("startDate"))) {
			params.put("startDate", PREVBRANPRCDATE);
		}
		if ("".equals(params.get("endDate"))) {
			params.put("endDate", PREVBRANPRCDATE);
		}

		PageInfo<SlAcupBean> page = acupServer.getLedgerList(params);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 总账发送日志查询(大连使用)
	 *
	 */
	@ResponseBody
	@RequestMapping(value = "/selectIfsBatchStep")
	public RetMsg<PageInfo<IfsBatchStep>> selectIfsBatchStep(@RequestBody Map<String, String> map) throws Exception {

		PageInfo<IfsBatchStep> page = opicsAcupService.selectIfsBatchStep(map);

		return RetMsgHelper.ok(page);
	}

	/**
	 * 总账按照套号保存(大连使用)
	 */
	@ResponseBody
	@RequestMapping(value = "/saveAcupDetail")
	public RetMsg<SlOutBean> saveAcupDetail(@RequestBody List<AcupSendBean> acupBeans) throws Exception {
		RetMsg<SlOutBean> page = opicsAcupService.saveAcupDetail(acupBeans);
		return page;
	}

	/**
	 * 总账重发(大连使用)
	 */
	@ResponseBody
	@RequestMapping(value = "/retrysendAcup")
	public RetMsg<SlOutBean> retrysendAcup(@RequestBody Map<String, String> params) throws Exception {
		RetMsg<SlOutBean> page = opicsAcupService.retrysendAcup(params);
		return page;
	}

	/**
	 * 日期校验(大连使用)
	 *
	 */
	@ResponseBody
	@RequestMapping(value = "/dateCheck")
	public String dateCheck(@RequestBody Map<String, String> params) {
		try {
			return opicsAcupService.dateCheck(params);
		} catch (Exception e) {
			logger.info(e);
		}
		return "";
	}

	/**
	 * OPICS自动跑批
	 *
	 */
	@ResponseBody
	@RequestMapping(value = "/opicsBsys")
	public RetMsg<SlOutBean> opicsBsys(@RequestBody Map<String, String> params) {
		SlOutBean msg = new SlOutBean();
		try {
			String opicsBsysMsg = acupServer.callOpicsBsys();
			msg.setRetCode("info");
			msg.setRetMsg(opicsBsysMsg);
		} catch (RemoteConnectFailureException e) {
			msg.setRetCode(SlErrors.FAILED_RMI_OPICS.getErrCode());
			msg.setRetMsg(SlErrors.FAILED_RMI_OPICS.getErrMsg() + ":" + e.getMessage() + "\n");
			msg.setRetStatus(RetStatusEnum.F);
		} catch (Exception e) {
			msg.setRetCode(SlErrors.FAILED.getErrCode());
			msg.setRetMsg(SlErrors.FAILED.getErrMsg() + ":" + e.getMessage() + "\n");
			msg.setRetStatus(RetStatusEnum.F);
		}
		return RetMsgHelper.ok(msg);
	}

	/**
	 * 导出Excel
	 */
	@ResponseBody
	@RequestMapping(value = "/exportExcel")
	public void exportExcel(HttpServletRequest request, HttpServletResponse response) {
		String ua = request.getHeader("User-Agent");
		String filename = "OPICS日终总账.xlsx"; // 文件名
		String encode_filename = "";
		try {
			encode_filename = URLEncoder.encode(filename, "utf-8");
		} catch (UnsupportedEncodingException e1) {
			throw new RException(e1.getMessage());
		}
		response.setContentType("application/vnd.ms-excel");
		if (ua != null && ua.indexOf("Firefox/") >= 0) {
			response.setHeader("Content-Disposition", "attachment; filename*=" + encode_filename);
		} else {
			response.setHeader("Content-Disposition", "attachment; filename=" + encode_filename);
		}
		response.setHeader("Connection", "close");
		try {
			String setno = request.getParameter("setno");
			String seetstatus = request.getParameter("seetstatus");
			String startDate = request.getParameter("startDate");
			String endDate = request.getParameter("endDate");
			String br = request.getParameter("br");

			Map<String, String> map = new HashMap<String, String>();
			map.put("setno", setno);
			map.put("seetstatus", seetstatus);
			map.put("startDate", startDate);
			map.put("endDate", endDate);
			map.put("br", br);
			ExcelUtil e = downloadExcel(map); // Excel文件下载内容
			OutputStream ouputStream = response.getOutputStream();
			e.getWb().write(ouputStream);
			ouputStream.flush();
			ouputStream.close();
		} catch (Exception e) {
			logger.info(e);
			throw new RException(e);
		}
	};

	/**
	 * Excel文件下载内容
	 */
	public ExcelUtil downloadExcel(Map<String, String> map) throws Exception {
		SlAcupBean bean = new SlAcupBean();
		List<SlAcupBean> acupList = acupServer.getAllData(map); // 查数据
		ExcelUtil e = null;
		try {
			// 表头
			e = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
			CellStyle center = e.getDefaultCenterStrCellStyle();

			int sheet = 0;
			int row = 0;

			// 表格sheet名称
			e.getWb().createSheet("日终总账表");

			// 设置表头字段名
			e.writeStr(sheet, row, "A", center, "处理序号");
			e.writeStr(sheet, row, "B", center, "套号");
			e.writeStr(sheet, row, "C", center, "部门");
			e.writeStr(sheet, row, "D", center, "顺序号");
			e.writeStr(sheet, row, "E", center, "交易号");
			e.writeStr(sheet, row, "F", center, "产品代码");
			e.writeStr(sheet, row, "G", center, "产品类型");
			e.writeStr(sheet, row, "H", center, "核心返回码");
			e.writeStr(sheet, row, "I", center, "核心返回信息");
			e.writeStr(sheet, row, "J", center, "opics科目号");
			e.writeStr(sheet, row, "K", center, "币种");
			e.writeStr(sheet, row, "L", center, "生效日期");
			e.writeStr(sheet, row, "M", center, "过账日期");
			e.writeStr(sheet, row, "N", center, "英文简称");
			e.writeStr(sheet, row, "O", center, "借贷标志");
			e.writeStr(sheet, row, "P", center, "金额");
			e.writeStr(sheet, row, "Q", center, "结算方式");
			e.writeStr(sheet, row, "R", center, "结算账号");
			e.writeStr(sheet, row, "S", center, "核心科目号");
			e.writeStr(sheet, row, "T", center, "核心账号");
			e.writeStr(sheet, row, "U", center, "记账网点");
			e.writeStr(sheet, row, "V", center, "argno");
			e.writeStr(sheet, row, "W", center, "成本中心");
			e.writeStr(sheet, row, "X", center, "beind");
			e.writeStr(sheet, row, "Y", center, "code");
			e.writeStr(sheet, row, "Z", center, "qual");
			e.writeStr(sheet, row, "AA", center, "descr");
			e.writeStr(sheet, row, "AB", center, "ccyCode");
			e.writeStr(sheet, row, "AC", center, "reversal");
			e.writeStr(sheet, row, "AD", center, "account");
			e.writeStr(sheet, row, "AE", center, "核心流水号");
			e.writeStr(sheet, row, "AF", center, "科目号");
			e.writeStr(sheet, row, "AG", center, "note");
			e.writeStr(sheet, row, "AH", center, "errCode");
			e.writeStr(sheet, row, "AI", center, "errMsg");
			e.writeStr(sheet, row, "AJ", center, "科目名称");
			e.writeStr(sheet, row, "AK", center, "dealflag");
			e.writeStr(sheet, row, "AL", center, "inputtime");

			// 设置列宽
			e.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(8, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(9, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(10, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(11, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(12, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(13, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(14, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(15, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(16, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(17, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(18, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(19, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(20, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(21, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(22, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(23, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(24, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(25, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(26, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(27, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(28, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(29, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(30, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(31, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(32, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(33, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(34, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(35, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(36, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(37, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(38, 20 * 256);

			for (int i = 0; i < acupList.size(); i++) {
				bean = acupList.get(i);
				row++;
				Map<String, String> amap = new HashMap<String, String>();
				// 插入数据
				e.writeStr(sheet, row, "A", center, bean.getSeqNo());
				e.writeStr(sheet, row, "B", center, bean.getSetNo());
				e.writeStr(sheet, row, "C", center, bean.getBr());
				e.writeStr(sheet, row, "D", center, bean.getSeq());
				e.writeStr(sheet, row, "E", center, bean.getDealno());
				e.writeStr(sheet, row, "F", center, bean.getProduct());
				e.writeStr(sheet, row, "G", center, bean.getType());
				e.writeStr(sheet, row, "H", center, bean.getRetCode());
				e.writeStr(sheet, row, "I", center, bean.getRetMsg());
				e.writeStr(sheet, row, "I", center, bean.getRetMsg());
				e.writeStr(sheet, row, "J", center, bean.getGlno());
				e.writeStr(sheet, row, "K", center, bean.getCcy());
				e.writeStr(sheet, row, "L", center, bean.getEffDate().substring(0, 10));
				e.writeStr(sheet, row, "M", center, bean.getPostDate().substring(0, 10));
				e.writeStr(sheet, row, "N", center, bean.getCmne());
				amap.put("dictId", "DebitCreditFlag");
				amap.put("dictKey", bean.getDrcrind());
				DecimalFormat df = new DecimalFormat("#0.00");
				e.writeStr(sheet, row, "O", center, ifsService.queryDictValue(amap));
				e.writeStr(sheet, row, "P", center, df.format(Double.valueOf(bean.getAmount())));
				e.writeStr(sheet, row, "Q", center, bean.getSmeans());
				e.writeStr(sheet, row, "R", center, bean.getSacct());
				e.writeStr(sheet, row, "S", center, bean.getIntglno());
				e.writeStr(sheet, row, "T", center, bean.getSubject());
				e.writeStr(sheet, row, "U", center, bean.getSubbr());
				e.writeStr(sheet, row, "V", center, bean.getArgno());
				e.writeStr(sheet, row, "W", center, bean.getCostcent());
				e.writeStr(sheet, row, "X", center, bean.getBeind());
				e.writeStr(sheet, row, "Y", center, bean.getCode());
				e.writeStr(sheet, row, "Z", center, bean.getQual());
				e.writeStr(sheet, row, "AA", center, bean.getDescr());
				e.writeStr(sheet, row, "AB", center, bean.getCcyCode());
				e.writeStr(sheet, row, "AC", center, bean.getReversal());
				e.writeStr(sheet, row, "AD", center, bean.getAccount());
				e.writeStr(sheet, row, "AE", center, bean.getVoucher());
				e.writeStr(sheet, row, "AF", center, bean.getRemark());
				e.writeStr(sheet, row, "AG", center, bean.getNote());
				e.writeStr(sheet, row, "AH", center, bean.getErrCode());
				e.writeStr(sheet, row, "AI", center, bean.getErrMsg());
				e.writeStr(sheet, row, "AJ", center, bean.getOper());
				e.writeStr(sheet, row, "AK", center, bean.getDealflag());
				e.writeStr(sheet, row, "AL", center, bean.getInputtime());
			}
			return e;
		} catch (Exception e1) {
			logger.info(e1);
			throw new RException(e1);
		}
	}

	/**
	 * 内部户 新增保存
	 */
	@ResponseBody
	@RequestMapping(value = "/nbhAdd")
	public RetMsg<Serializable> nbhAdd(@RequestBody Map<String, Object> map) {
		ifsServvice.nbhAdd(map);
		return RetMsgHelper.ok("error.common.0000"); // 正常返回
	}

	/**
	 * 内部户 修改保存
	 */
	@ResponseBody
	@RequestMapping(value = "/nbhEdit")
	public RetMsg<Serializable> nbhEdit(@RequestBody IfsNbhBean entity) {
		ifsServvice.updateById(entity);
		return RetMsgHelper.ok();
	}

	/***
	 * 分页查询 内部户
	 */
	@ResponseBody
	@RequestMapping(value = "/searchData")
	public RetMsg<com.singlee.capital.common.pojo.page.PageInfo<IfsNbhBean>> searchData(
			@RequestBody IfsNbhBean entity) {
		Page<IfsNbhBean> page = ifsServvice.searchData(entity);
		return RetMsgHelper.ok(page);
	}

	/***
	 * 删除 内部户
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteNbh")
	public RetMsg<Serializable> deleteNbh(@RequestBody Map<String, String> map) {
		ifsServvice.deleteNbh(map);
		return RetMsgHelper.ok();
	}

	/***
	 * 外汇头寸估值 分页查询
	 */
	@ResponseBody
	@RequestMapping(value = "/searchNbhPage")
	public RetMsg<com.singlee.capital.common.pojo.page.PageInfo<IfsNbhGatherBean>> searchNbhPage(
			@RequestBody Map<String, Object> map) {
		Page<IfsNbhGatherBean> page = ifsServvice.searchNbhPage(map);
		return RetMsgHelper.ok(page);
	}

	/***
	 * 外汇头寸估值 删除
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteNbhAcct")
	public RetMsg<Serializable> deleteNbhAcct(@RequestBody Map<String, String> map) {
		ifsServvice.deleteNbhAcct(map);
		return RetMsgHelper.ok();
	}

	/**
	 * 外汇头寸估值 新增保存
	 */
	@ResponseBody
	@RequestMapping(value = "/nbhGatherAdd")
	public RetMsg<Serializable> nbhGatherAdd(@RequestBody IfsNbhGatherBean entity) {
		// 设置流水号
		HashMap<String, Object> map = new HashMap<String, Object>();
		String fedealno = ifsOpicsBondMapper.getTradeId(map);
		entity.setFedealno(fedealno);
		ifsServvice.nbhGatherAdd(entity);
		return RetMsgHelper.ok("error.common.0000"); // 正常返回
	}

	/**
	 * 外汇头寸估值 修改保存
	 */
	@ResponseBody
	@RequestMapping(value = "/nbhGatherEdit")
	public RetMsg<Serializable> nbhGatherEdit(@RequestBody IfsNbhGatherBean entity) {
		// 根据fedealno查询实体类
		IfsNbhGatherBean gatherBean = ifsServvice.searchNbhPageSingle(entity.getFedealno());
		String localStatus = gatherBean.getLocalstatus();
		entity.setLocalstatus(localStatus);
		ifsServvice.updateByGatherId(entity);
		return RetMsgHelper.ok();
	}

	/**
	 * 外汇头寸估值 同步至OPICS
	 */
	@ResponseBody
	@RequestMapping(value = "/syncNbh")
	public RetMsg<SlOutBean> syncNbh(@RequestBody IfsNbhGatherBean entity) {
		SlFxSptFwdBean slFxSptFwdBean = new SlFxSptFwdBean("01", SlDealModule.FXD.SERVER, SlDealModule.FXD.TAG,
				SlDealModule.FXD.DETAIL);
		FxCurrencyBean currencyInfo = new FxCurrencyBean();
		currencyInfo.setAmt(entity.getCcyamt()); // 外币金额
		currencyInfo.setCcy(entity.getCcy()); // 外币币种
		currencyInfo.setContraAmt(entity.getCtramt()); // 本币金额
		currencyInfo.setContraCcy(entity.getCtrccy()); // 本币币种
		slFxSptFwdBean.setCurrencyInfo(currencyInfo);

		InstitutionBean contraPatryInstitutionInfo = new InstitutionBean();
		InstitutionBean institutionInfo = new InstitutionBean();
		contraPatryInstitutionInfo.setInstitutionId(entity.getCust()); // 设置交易对手
		institutionInfo.setTradeId(entity.getTrad()); // 设置交易员
		slFxSptFwdBean.setContraPatryInstitutionInfo(contraPatryInstitutionInfo);
		slFxSptFwdBean.setInstitutionInfo(institutionInfo);

		SlExternalBean externalBean = new SlExternalBean();
		externalBean.setPort(entity.getPort()); // 设置投资组合
		externalBean.setCost(entity.getCost()); // 设置成本中心
		externalBean.setBroker(SlDealModule.BROKER); // 设置BROK
		externalBean.setDealtext(entity.getDealtext()); // 设置来源DEALTEXT
		externalBean.setSiind(SlDealModule.FXD.SIIND); // 设置SIIND
		externalBean.setVerind(SlDealModule.FXD.VERIND); // 设置VERIND
		externalBean.setSuppayind(SlDealModule.FXD.SUPPAYIND); // 设置SUPPAYIND
		externalBean.setSuprecind(SlDealModule.FXD.SUPRECIND); // 设置SUPRECIND
		externalBean.setSupconfind(SlDealModule.FXD.SUPCONFIND); // 设置SUPCONFIND
		externalBean.setProdcode(entity.getProduct()); // 设置产品代码
		externalBean.setProdtype(entity.getProdtype()); // 设置产品类型
		externalBean.setAuthsi(SlDealModule.FXD.AUTHSI); // 设置AUTHSI
		slFxSptFwdBean.setExternalBean(externalBean);

		// 设置流水号
		/** 设置Fedealno和Inoutind */
		SlInthBean inthBean = slFxSptFwdBean.getInthBean();
		inthBean.setFedealno(entity.getFedealno());
		slFxSptFwdBean.setInthBean(inthBean);

		// 设置DEALDATE
		slFxSptFwdBean.setValueDate(entity.getVdate()); // 设置VDATE
		slFxSptFwdBean.setDealDate(entity.getDealdate());
		if ("P".equals(entity.getPs())) {
			slFxSptFwdBean.setPs(PsEnum.P);
		} else {
			slFxSptFwdBean.setPs(PsEnum.S);
		}
		SlOutBean result = new SlOutBean();
		try {
			// 插入OPICS的IFXD表
			result = fxdServer.fxSptFwd(slFxSptFwdBean);
		} catch (RemoteConnectFailureException e) {
			result.setRetCode(SlErrors.FAILED_RMI_OPICS.getErrCode());
			result.setRetMsg(SlErrors.FAILED_RMI_OPICS.getErrMsg() + ":" + e.getMessage());
			result.setRetStatus(RetStatusEnum.F);
		} catch (Exception e) {
			result.setRetCode(SlErrors.FAILED.getErrCode());
			result.setRetMsg(SlErrors.FAILED.getErrMsg() + ":" + e.getMessage());
			result.setRetStatus(RetStatusEnum.F);
		}
		if ("S".equals(String.valueOf(result.getRetStatus()))) {
			entity.setLocalstatus("1"); // 已同步
			ifsServvice.updateStatus(entity);
		}
		if (!result.getRetStatus().equals(RetStatusEnum.S)) {
			JY.raise("插入opics表失败......");
		}
		return RetMsgHelper.ok(result);
	}

	/**
	 * 刷新OPICS处理状态
	 */
	@ResponseBody
	@RequestMapping(value = "/refresh")
	public String refresh(@RequestBody IfsNbhGatherBean entity) {
		String flag = "";
		try {
			//
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("fedealno", entity.getFedealno());
			String dealno = fxdServer.queryDealno(map);
			if (dealno != null) {
				entity.setDealno(dealno); // OPICS编号
				entity.setLocalstatus("2"); // 同步成功
				ifsServvice.updateStatus(entity);
				flag = "2";
			} else {
				entity.setLocalstatus("3"); // 同步失败
				ifsServvice.updateStatus(entity);
				flag = "3"; // 同步失败
			}
		} catch (Exception e) {
			JY.raise("同步OPICS表失败......");
			entity.setLocalstatus("3"); // 同步失败
			ifsServvice.updateStatus(entity);
			e.printStackTrace();
		}
		return flag;
	}

	/**
	 * 导出历史送账记录Excel
	 */
	@ResponseBody
	@RequestMapping(value = "/exportHistory")
	public void exportHistory(HttpServletRequest request, HttpServletResponse response) {
		String ua = request.getHeader("User-Agent");
		String filename = "OPICS历史送账记录.xlsx"; // 文件名
		String encode_filename = "";
		try {
			encode_filename = URLEncoder.encode(filename, "utf-8");
		} catch (UnsupportedEncodingException e1) {
			throw new RException(e1.getMessage());
		}
		response.setContentType("application/vnd.ms-excel");
		if (ua != null && ua.indexOf("Firefox/") >= 0) {
			response.setHeader("Content-Disposition", "attachment; filename*=" + encode_filename);
		} else {
			response.setHeader("Content-Disposition", "attachment; filename=" + encode_filename);
		}
		response.setHeader("Connection", "close");
		try {
			String setno = request.getParameter("setno");
			String seetstatus = request.getParameter("seetstatus");
			String postDate = request.getParameter("postDate");
			Map<String, String> map = new HashMap<String, String>();
			map.put("setno", setno);
			map.put("seetstatus", seetstatus);
			map.put("postDate", postDate);
			ExcelUtil e = downloadHistoryExcel(map);
			OutputStream ouputStream = response.getOutputStream();
			e.getWb().write(ouputStream);
			ouputStream.flush();
			ouputStream.close();
		} catch (Exception e) {
			throw new RException(e);
		}
	};

	/**
	 * Excel文件下载历史总账
	 */
	public ExcelUtil downloadHistoryExcel(Map<String, String> map) throws Exception {
		SlAcupBean bean = new SlAcupBean();
		List<SlAcupBean> acupList = acupServer.getAllHistoryData(map); // 查总账历史数据
		ExcelUtil e = null;
		try {
			// 表头
			e = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
			CellStyle center = e.getDefaultCenterStrCellStyle();

			int sheet = 0;
			int row = 0;

			// 表格sheet名称
			e.getWb().createSheet("日终历史总账表");

			// 设置表头字段名
			e.writeStr(sheet, row, "A", center, "处理序号");
			e.writeStr(sheet, row, "B", center, "套号");
			e.writeStr(sheet, row, "C", center, "部门");
			e.writeStr(sheet, row, "D", center, "顺序号");
			e.writeStr(sheet, row, "E", center, "交易号");
			e.writeStr(sheet, row, "F", center, "产品代码");
			e.writeStr(sheet, row, "G", center, "产品类型");
			e.writeStr(sheet, row, "H", center, "核心返回码");
			e.writeStr(sheet, row, "I", center, "核心返回信息");
			e.writeStr(sheet, row, "J", center, "opics科目号");
			e.writeStr(sheet, row, "K", center, "币种");
			e.writeStr(sheet, row, "L", center, "生效日期");
			e.writeStr(sheet, row, "M", center, "过账日期");
			e.writeStr(sheet, row, "N", center, "英文简称");
			e.writeStr(sheet, row, "O", center, "借贷标志");
			e.writeStr(sheet, row, "P", center, "金额");
			e.writeStr(sheet, row, "Q", center, "结算方式");
			e.writeStr(sheet, row, "R", center, "结算账号");
			e.writeStr(sheet, row, "S", center, "核心科目号");
			e.writeStr(sheet, row, "T", center, "核心账号");
			e.writeStr(sheet, row, "U", center, "记账网点");
			e.writeStr(sheet, row, "V", center, "argno");
			e.writeStr(sheet, row, "W", center, "成本中心");
			e.writeStr(sheet, row, "X", center, "beind");
			e.writeStr(sheet, row, "Y", center, "code");
			e.writeStr(sheet, row, "Z", center, "qual");
			e.writeStr(sheet, row, "AA", center, "descr");
			e.writeStr(sheet, row, "AB", center, "ccyCode");
			e.writeStr(sheet, row, "AC", center, "reversal");
			e.writeStr(sheet, row, "AD", center, "account");
			e.writeStr(sheet, row, "AE", center, "voucher");
			e.writeStr(sheet, row, "AF", center, "remark");
			e.writeStr(sheet, row, "AG", center, "note");
			e.writeStr(sheet, row, "AH", center, "errCode");
			e.writeStr(sheet, row, "AI", center, "errMsg");
			e.writeStr(sheet, row, "AJ", center, "操作员");
			e.writeStr(sheet, row, "AK", center, "dealflag");
			e.writeStr(sheet, row, "AL", center, "inputtime");

			// 设置列宽
			e.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(8, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(9, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(10, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(11, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(12, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(13, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(14, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(15, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(16, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(17, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(18, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(19, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(20, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(21, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(22, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(23, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(24, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(25, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(26, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(27, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(28, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(29, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(30, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(31, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(32, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(33, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(34, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(35, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(36, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(37, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(38, 20 * 256);

			for (int i = 0; i < acupList.size(); i++) {
				bean = acupList.get(i);
				row++;
				// 插入数据
				e.writeStr(sheet, row, "A", center, bean.getSeqNo());
				e.writeStr(sheet, row, "B", center, bean.getSetNo());
				e.writeStr(sheet, row, "C", center, bean.getBr());
				e.writeStr(sheet, row, "D", center, bean.getSeq());
				e.writeStr(sheet, row, "E", center, bean.getDealno());
				e.writeStr(sheet, row, "F", center, bean.getProduct());
				e.writeStr(sheet, row, "G", center, bean.getType());
				e.writeStr(sheet, row, "H", center, bean.getRetCode());
				e.writeStr(sheet, row, "I", center, bean.getRetMsg());
				e.writeStr(sheet, row, "J", center, bean.getGlno());
				e.writeStr(sheet, row, "K", center, bean.getCcy());
				e.writeStr(sheet, row, "L", center, bean.getEffDate());
				e.writeStr(sheet, row, "M", center, bean.getPostDate());
				e.writeStr(sheet, row, "N", center, bean.getCmne());
				e.writeStr(sheet, row, "O", center, bean.getDrcrind());
				e.writeStr(sheet, row, "P", center, bean.getAmount());
				e.writeStr(sheet, row, "Q", center, bean.getSmeans());
				e.writeStr(sheet, row, "R", center, bean.getSacct());
				e.writeStr(sheet, row, "S", center, bean.getIntglno());
				e.writeStr(sheet, row, "T", center, bean.getSubject());
				e.writeStr(sheet, row, "U", center, bean.getSubbr());
				e.writeStr(sheet, row, "V", center, bean.getArgno());
				e.writeStr(sheet, row, "W", center, bean.getCostcent());
				e.writeStr(sheet, row, "X", center, bean.getBeind());
				e.writeStr(sheet, row, "Y", center, bean.getCode());
				e.writeStr(sheet, row, "Z", center, bean.getQual());
				e.writeStr(sheet, row, "AA", center, bean.getDescr());
				e.writeStr(sheet, row, "AB", center, bean.getCcyCode());
				e.writeStr(sheet, row, "AC", center, bean.getReversal());
				e.writeStr(sheet, row, "AD", center, bean.getAccount());
				e.writeStr(sheet, row, "AE", center, bean.getVoucher());
				e.writeStr(sheet, row, "AF", center, bean.getRemark());
				e.writeStr(sheet, row, "AG", center, bean.getNote());
				e.writeStr(sheet, row, "AH", center, bean.getErrCode());
				e.writeStr(sheet, row, "AI", center, bean.getErrMsg());
				e.writeStr(sheet, row, "AJ", center, bean.getOper());
				e.writeStr(sheet, row, "AK", center, bean.getDealflag());
				e.writeStr(sheet, row, "AL", center, bean.getInputtime());
			}
			return e;
		} catch (Exception e1) {
			throw new RException(e1);
		}
	}
}