package com.singlee.ifs.controller;

import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.credit.util.ExcelUtil;
import com.singlee.financial.bean.DldtRevaluation;
import com.singlee.financial.bean.SlSecmRedemption;
import com.singlee.financial.bean.SlSecmRevaluation;
import com.singlee.financial.opics.IDldtCountServer;
import com.singlee.financial.opics.ISecurServer;
import com.singlee.financial.page.PageInfo;
import com.singlee.ifs.mapper.IfsNettingMapper;
import com.singlee.ifs.model.IfsNetting;
import com.singlee.ifs.service.IfsNettingService;
import org.apache.poi.ss.usermodel.CellStyle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 净额清算处理 Controller
 * 
 * ClassName: IfsSwiftController <br/>
 * date: 2018-5-30 下午07:48:45 <br/>
 * 
 * @author yubao
 * @version
 * @since JDK 1.6
 */
@Controller
@RequestMapping(value = "/IfsNettingController")
public class IfsNettingController {

	@Autowired
	private IfsNettingService ifsNettingService;
	
	@Autowired
	ISecurServer securServer;

	@Autowired
	IDldtCountServer dldtServer;
	
	@Autowired
	IfsNettingMapper ifsNettingMapper;
	
	@ResponseBody
	@RequestMapping(value = "/queryNettingList")
	public RetMsg<PageInfo<IfsNetting>> queryNettingList(@RequestBody Map<String, Object> map) {
		PageInfo<IfsNetting> page = ifsNettingService.queryNettingList(map);
		return RetMsgHelper.ok(page);
	}
	
	@ResponseBody
	@RequestMapping(value = "/queryNettingSum")
	public List<IfsNetting> queryNettingSum(@RequestBody Map<String, Object> map) {
		List<IfsNetting> list = ifsNettingService.queryNettingSum(map);
		return list;
	}
	
	/**
	 * 付款金额统计
	 * @param map
	 * @return
	 */
	
	@ResponseBody
	@RequestMapping(value = "/queryNettingSumPay")
	public List<IfsNetting> queryNettingSumPay(@RequestBody Map<String, Object> map) {
		List<IfsNetting> list = ifsNettingService.queryNettingSumPay(map);
		return list;
	}
	
	/**
	 * 查询还本付息记录
	 * @param map
	 * @return
	 * @throws Exception 
	 * @throws RemoteConnectFailureException 
	 */
	@ResponseBody
	@RequestMapping(value = "/querySecurServerList")
	public RetMsg<PageInfo<SlSecmRedemption>> querySecurServerList(@RequestBody Map<String, Object> map) throws RemoteConnectFailureException, Exception {
		PageInfo<SlSecmRedemption> page = securServer.getRedemption(map);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 导出Excel
	 */
	@ResponseBody
	@RequestMapping(value = "/exportExcel")
	public void exportExcel(HttpServletRequest request, HttpServletResponse response) {
		String ua = request.getHeader("User-Agent");
		String filename = "净额清算报表.xlsx"; // 文件名
		String encode_filename = "";
		try {
			encode_filename = URLEncoder.encode(filename, "utf-8");
		} catch (UnsupportedEncodingException e1) {
			throw new RException(e1.getMessage());
		}
		response.setContentType("application/vnd.ms-excel");
		if (ua != null && ua.indexOf("Firefox/") >= 0) {
			response.setHeader("Content-Disposition", "attachment; filename*=" + encode_filename);
		} else {
			response.setHeader("Content-Disposition", "attachment; filename=" + encode_filename);
		}
		response.setHeader("Connection", "close");
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("sysDate", request.getParameter("sysDate"));
			map.put("payDate", request.getParameter("payDate"));
			map.put("businessType", request.getParameter("businessType"));
			//收款金额统计
			map.put("UsdSumAmt", request.getParameter("UsdSumAmt"));
			map.put("EurSumAmt", request.getParameter("EurSumAmt"));
			map.put("JpySumAmt", request.getParameter("JpySumAmt"));
			map.put("GbpSumAmt", request.getParameter("GbpSumAmt"));
			map.put("HkdSumAmt", request.getParameter("HkdSumAmt"));
			map.put("CnySumAmt", request.getParameter("CnySumAmt"));
			
			//付款金额统计
			map.put("UsdSumAmtP", request.getParameter("UsdSumAmtP"));
			map.put("EurSumAmtP", request.getParameter("EurSumAmtP"));
			map.put("JpySumAmtP", request.getParameter("JpySumAmtP"));
			map.put("GbpSumAmtP", request.getParameter("GbpSumAmtP"));
			map.put("HkdSumAmtP", request.getParameter("HkdSumAmtP"));
			map.put("CnySumAmtP", request.getParameter("CnySumAmtP"));
			
			//收付款金额统计
			map.put("UsdSumAmtD", request.getParameter("UsdSumAmtD"));
			map.put("EurSumAmtD", request.getParameter("EurSumAmtD"));
			map.put("JpySumAmtD", request.getParameter("JpySumAmtD"));
			map.put("GbpSumAmtD", request.getParameter("GbpSumAmtD"));
			map.put("HkdSumAmtD", request.getParameter("HkdSumAmtD"));
			map.put("CnySumAmtD", request.getParameter("CnySumAmtD"));
			
			ExcelUtil e = downloadExcel(map);
			OutputStream ouputStream = response.getOutputStream();
			e.getWb().write(ouputStream);
			ouputStream.flush();
			ouputStream.close();
		} catch (Exception e) {
			throw new RException(e);
		}
	};

	/**
	 * Excel文件下载内容
	 */
	public ExcelUtil downloadExcel(Map<String, Object> map) throws Exception {
		IfsNetting bean = new IfsNetting();
		List<IfsNetting> ifsNetting = ifsNettingMapper.queryNettingData(map); // 查数据
		ExcelUtil e = null;
		try {
			// 表头
			e = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
			CellStyle center = e.getDefaultCenterStrCellStyle();

			int sheet = 0;
			int row = 0;

			// 表格sheet名称
			e.getWb().createSheet("净额清算报表");

			// 设置表头字段名
			//设置第一行名称
			e.writeStr(sheet, row, "A", center, "");
			e.writeStr(sheet, row, "B", center, "美元");
			e.writeStr(sheet, row, "C", center, "欧元");
			e.writeStr(sheet, row, "D", center, "日元");
			e.writeStr(sheet, row, "E", center, "英镑");
			e.writeStr(sheet, row, "F", center, "港元");
			e.writeStr(sheet, row, "G", center, "人民币");
			
			//收款金额
			e.writeStr(sheet, row+1, "A", center, "收款金额");
			e.writeStr(sheet, row+1, "B", center, map.get("UsdSumAmt")==null?"":map.get("UsdSumAmt").toString());
			e.writeStr(sheet, row+1, "C", center, map.get("EurSumAmt")==null?"":map.get("EurSumAmt").toString());
			e.writeStr(sheet, row+1, "D", center, map.get("JpySumAmt")==null?"":map.get("JpySumAmt").toString());
			e.writeStr(sheet, row+1, "E", center, map.get("GbpSumAmt")==null?"":map.get("GbpSumAmt").toString());
			e.writeStr(sheet, row+1, "F", center, map.get("HkdSumAmt")==null?"":map.get("HkdSumAmt").toString());
			e.writeStr(sheet, row+1, "G", center, map.get("CnySumAmt")==null?"":map.get("CnySumAmt").toString());
			
			row = row+1;
			//付款金额
			e.writeStr(sheet, row+1, "A", center, "付款金额");
			e.writeStr(sheet, row+1, "B", center, map.get("UsdSumAmtP")==null?"":map.get("UsdSumAmtP").toString());
			e.writeStr(sheet, row+1, "C", center, map.get("EurSumAmtP")==null?"":map.get("EurSumAmtP").toString());
			e.writeStr(sheet, row+1, "D", center, map.get("JpySumAmtP")==null?"":map.get("JpySumAmtP").toString());
			e.writeStr(sheet, row+1, "E", center, map.get("GbpSumAmtP")==null?"":map.get("GbpSumAmtP").toString());
			e.writeStr(sheet, row+1, "F", center, map.get("HkdSumAmtP")==null?"":map.get("HkdSumAmtP").toString());
			e.writeStr(sheet, row+1, "G", center, map.get("CnySumAmtP")==null?"":map.get("CnySumAmtP").toString());
			
			row = row+1;
			//首付款差值统计
			e.writeStr(sheet, row+1, "A", center, "首付款差值");
			e.writeStr(sheet, row+1, "B", center, map.get("UsdSumAmtD")==null?"":map.get("UsdSumAmtD").toString());
			e.writeStr(sheet, row+1, "C", center, map.get("EurSumAmtD")==null?"":map.get("EurSumAmtD").toString());
			e.writeStr(sheet, row+1, "D", center, map.get("JpySumAmtD")==null?"":map.get("JpySumAmtD").toString());
			e.writeStr(sheet, row+1, "E", center, map.get("GbpSumAmtD")==null?"":map.get("GbpSumAmtD").toString());
			e.writeStr(sheet, row+1, "F", center, map.get("HkdSumAmtD")==null?"":map.get("HkdSumAmtD").toString());
			e.writeStr(sheet, row+1, "G", center, map.get("CnySumAmtD")==null?"":map.get("CnySumAmtD").toString());
			
			
			row = row+2;
			//净额清算明细
			e.writeStr(sheet, row+1, "A", center, "OPICS编号");
			e.writeStr(sheet, row+1, "B", center, "成交单号");
			e.writeStr(sheet, row+1, "C", center, "业务类型");
			e.writeStr(sheet, row+1, "D", center, "交易对手");
			e.writeStr(sheet, row+1, "E", center, "收款账号");
			e.writeStr(sheet, row+1, "F", center, "收款币种");
			e.writeStr(sheet, row+1, "G", center, "收款金额");
			e.writeStr(sheet, row+1, "H", center, "付款账号");
			e.writeStr(sheet, row+1, "I", center, "付款币种");
			e.writeStr(sheet, row+1, "J", center, "付款金额");
			e.writeStr(sheet, row+1, "K", center, "清算方式");
			
			// 设置列宽
			e.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(8, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(9, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(10, 20 * 256);

			for (int i = 0; i < ifsNetting.size(); i++) {
				bean = ifsNetting.get(i);
				row++;
				// 插入数据
				e.writeStr(sheet, row+1, "A", center, bean.getDealno());
				e.writeStr(sheet, row+1, "B", center, bean.getContractId());
				e.writeStr(sheet, row+1, "C", center, bean.getBusinessType());
				e.writeStr(sheet, row+1, "D", center, bean.getCounterpartyDealer());
				e.writeStr(sheet, row+1, "E", center, bean.getDueBankAccount1());
				e.writeStr(sheet, row+1, "F", center, bean.getBuyCurreny());
				e.writeStr(sheet, row+1, "G", center, bean.getBuyAmt());
				e.writeStr(sheet, row+1, "H", center, bean.getDueBankAccount2());
				e.writeStr(sheet, row+1, "I", center, bean.getSellCurreny());
				e.writeStr(sheet, row+1, "J", center, bean.getSellAmt());
				e.writeStr(sheet, row+1, "K", center, bean.getNettingStatus());
			}
			return e;
		} catch (Exception e1) {
			throw new RException(e1);
		}
	}
	
	
	
	/**
	 * 债券重定价提醒
	 * @param map
	 * @return
	 * @throws Exception 
	 * @throws RemoteConnectFailureException 
	 */
	@ResponseBody
	@RequestMapping(value = "/queryBondRevaluationList")
	public RetMsg<PageInfo<SlSecmRevaluation>> queryBondRevaluationList(@RequestBody Map<String, Object> map) throws RemoteConnectFailureException, Exception {
		PageInfo<SlSecmRevaluation> page = securServer.getPageSecmRevaluation(map);
		return RetMsgHelper.ok(page);
	}

	/**
	 *浮动拆借定息提醒
	 *
	 */
	@ResponseBody
	@RequestMapping(value = "/queryINORevaluation")
	public RetMsg<PageInfo<DldtRevaluation>> queryINORevaluation(@RequestBody Map<String, Object> map){
		PageInfo<DldtRevaluation> page = dldtServer.searchDldtRev(map);
		return RetMsgHelper.ok(page);
	}
}