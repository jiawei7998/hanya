package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.ifs.model.IfsRevIsld;
import com.singlee.ifs.service.IfsIsldService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.Map;

/**
 * 
 * 债券类Controller
 * @author lishichao
 * @date 2018-08-17
 * 
 */


@Controller
@RequestMapping(value = "/IfsIsldController")
public class IfsIsldController {

	@Autowired
	IfsIsldService ifsIsldService;

	/***
	 * 删除 债券类要素
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteIsldParam")
	public RetMsg<Serializable> deleteForexParam(@RequestBody Map<String, String> map) {
		String ticketId = map.get("ticketId");
		ifsIsldService.deleteById(ticketId);
		return RetMsgHelper.ok();
	}

	/**
	 * 保存 债券类要素
	 */
	@ResponseBody
	@RequestMapping(value = "/saveIsldParam")
	public RetMsg<Serializable> saveForexParam(@RequestBody IfsRevIsld entity) {
		String ticketId = entity.getTicketId();
		if (null == ticketId || "".equals(ticketId)) {// 新增
			ifsIsldService.insert(entity);
		} else {// 修改
			ifsIsldService.updateById(entity);
		}
		return RetMsgHelper.ok();
	}
	
	/***************************************************************************************/
	
	/**
	 * 查询  债券冲销     我发起的
	 * 
	 * @param params
	 * @return
	 * @author lij
	 * @date 2018-08-10
	 */
	@ResponseBody
	@RequestMapping(value = "/searchIsldMine")
	public RetMsg<PageInfo<IfsRevIsld>> searchRevIfxdMine(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsRevIsld> page = ifsIsldService.getRevIsldPage(params, 3);
		return RetMsgHelper.ok(page);
	}
	
	
	/**
	 * 查询 债券冲销       待审批
	 * 
	 * @param params
	 * @return
	 * @author lij
	 * @date 2018-08-10
	 */
	@ResponseBody
	@RequestMapping(value = "/searchIsldUnfinished")
	public RetMsg<PageInfo<IfsRevIsld>> searchRevIfxdUnfinished(@RequestBody Map<String, Object> params) {
		String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse + ","
				+ DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
		String[] orderStatus = approveStatus.split(",");
		params.put("approveStatusNo", orderStatus);
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsRevIsld> page = ifsIsldService.getRevIsldPage(params, 1);
		return RetMsgHelper.ok(page);
	}
	
	
	/**
	 * 查询 债券冲销      已审批
	 * 
	 * @param params
	 * @return
	 * @author lij
	 * @date 2018-08-10
	 */
	@ResponseBody
	@RequestMapping(value = "/searchIsldFinished")
	public RetMsg<PageInfo<IfsRevIsld>> searchRevIfxdFinished(@RequestBody Map<String, Object> params) {
		String approveStatus = ParameterUtil.getString(params, "approveStatus", "");
		if (StringUtil.isNullOrEmpty(approveStatus)) {
			approveStatus = DictConstants.ApproveStatus.ApprovedPass;
			approveStatus += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatus += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatus += "," + DictConstants.ApproveStatus.Verified;
			approveStatus += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			approveStatus += "," + DictConstants.ApproveStatus.Approving;

			String[] orderStatus = approveStatus.split(",");
			params.put("approveStatusNo", orderStatus);
		}
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsRevIsld> page = ifsIsldService.getRevIsldPage(params, 2);
		return RetMsgHelper.ok(page);
	}
}
