package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.ifs.model.IfsTradeParaGroup;
import com.singlee.ifs.service.IfsTradeElementParamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.Map;

/**
 * 交易要素COntroller
 * 
 * ClassName: IfsTradeElementParamController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason: TODO ADD REASON(可选). <br/>
 * date: 2018-5-30 下午07:49:42 <br/>
 * 
 * @author shenzl
 * @version
 * @since JDK 1.6
 */
@Controller
@RequestMapping(value = "/IfsTradeElementParamController")
public class IfsTradeElementParamController {
	@Autowired
	IfsTradeElementParamService ifsTradeParamService;

	/***
	 * 分页查询 交易要素
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageTradeParam")
	public RetMsg<PageInfo<IfsTradeParaGroup>> searchPageTradeParam(@RequestBody Map<String, Object> map) {
		Page<IfsTradeParaGroup> page = ifsTradeParamService.searchPageOpicsTrdParam(map);
		return RetMsgHelper.ok(page);
	}

	/***
	 * 删除 交易要素
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteTradeParam")
	public RetMsg<Serializable> deleteTradeParam(@RequestBody Map<String, String> map) {
		String groupId = map.get("groupId");
		ifsTradeParamService.deleteById(groupId);
		return RetMsgHelper.ok();
	}

	/**
	 * 保存 交易要素
	 */
	@ResponseBody
	@RequestMapping(value = "/saveTradeParam")
	public RetMsg<Serializable> saveTradeParam(@RequestBody IfsTradeParaGroup entity) {
		String groupId = entity.getGroupId();
		if (null == groupId || "".equals(groupId)) {// 新增
			ifsTradeParamService.insert(entity);
		} else {// 修改
			ifsTradeParamService.updateById(entity);
		}

		return RetMsgHelper.ok();
	}
}
