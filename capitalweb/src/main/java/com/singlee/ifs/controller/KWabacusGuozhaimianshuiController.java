package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.ifs.model.KWabacusGuozhaimianshui;
import com.singlee.ifs.service.KWabacusGuozhaimianshuiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Map;


@Controller
@RequestMapping(value = "/KWabacusGuozhaimianshuiController")
public class KWabacusGuozhaimianshuiController extends CommonController{


	@Autowired
	KWabacusGuozhaimianshuiService kWabacusGuozhaimianshuiService;
	/***
	 * 生成国债免税数据
	 */
	@ResponseBody
	@RequestMapping(value = "/getPageList")
	public RetMsg<PageInfo<KWabacusGuozhaimianshui>> getPageList(@RequestBody Map<String, Object> map) {
		        
		Page<KWabacusGuozhaimianshui> pageList = kWabacusGuozhaimianshuiService.getPageList(map);
		return RetMsgHelper.ok(pageList);
	}

	/***
	 * 添加
	 */
	@ResponseBody
	@RequestMapping(value = "/addOrUpdate")
	public RetMsg<Serializable> addOrUpdate(@RequestBody KWabacusGuozhaimianshui kWabacusGuozhaimianshui) {
		if(kWabacusGuozhaimianshui.getId()==null){
			kWabacusGuozhaimianshuiService.insert(kWabacusGuozhaimianshui);
		}else{
			kWabacusGuozhaimianshuiService.updateByPrimaryKeySelective(kWabacusGuozhaimianshui);
		}
		return RetMsgHelper.ok("操作成功!");
	}

	/***
	 * 删除
	 */
	@ResponseBody
	@RequestMapping(value = "/delete")
	public RetMsg<Serializable> delete(@RequestBody BigDecimal id) {
		kWabacusGuozhaimianshuiService.deleteByPrimaryKey(id);
		return RetMsgHelper.ok("操作成功!");
	}





	
    
    
	
}
