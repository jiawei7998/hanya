package com.singlee.ifs.controller;

import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.ifs.service.IfsCdtcTestConnectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.Map;

/**
 * @author Luozb
 * @version 1.0.0
 * @ClassName IfsCdtcTestConnectController.java
 * @Description 中债测试报文连通性
 * @createTime 2021年11月23日 16:27:00
 */
@Controller
@RequestMapping(value = "/IfsCdtcTestConnectController")
public class IfsCdtcTestConnectController extends CommonController {

    @Autowired
    IfsCdtcTestConnectService ifsCdtcTestConnectService;

    @ResponseBody
    @RequestMapping(value = "/createHertMessageXml")
    public RetMsg<Serializable> createHertMessageXml(
            @RequestBody Map<String, Object> map) {
        String result = ifsCdtcTestConnectService.createHertMessageXml(map);
        return RetMsgHelper.ok(result);
    }

    @ResponseBody
    @RequestMapping(value = "/sendHertMessageXml")
    public RetMsg<Serializable> sendHertMessageXml(
            @RequestBody Map<String, Object> map) {
        String result = ifsCdtcTestConnectService.sendHertMessageXml(map);
        return RetMsgHelper.ok(result);
    }
}
