package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.opics.IBaseServer;
import com.singlee.ifs.model.IfsCfetsrmbCbt;
import com.singlee.ifs.model.IfsCfetsrmbCbtKey;
import com.singlee.ifs.service.IFSService;
import com.singlee.ifs.service.IfsOpicsTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.Map;

/**
 * 现券买卖正式交易审批Controller
 * 
 * ClassName: IfsCfetsrmbCbtController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason: TODO ADD REASON(可选). <br/>
 * date: 2018-5-30 下午07:23:24 <br/>
 * 
 * @author lijing
 * @version
 * @since JDK 1.6
 */
@Controller
@RequestMapping(value = "/IfsCfetsrmbCbtController")
public class IfsCfetsrmbCbtController extends CommonController {
	@Autowired
	private IFSService cfetsrmbCbtService;
	@Autowired
	IBaseServer baseServer;
	@Autowired
	IfsOpicsTypeService opicsTypeService;

	/**
	 * 保存成交单
	 * 
	 * @param IfsCfetsrmbCbt
	 * @throws Exception 
	 * @throws RemoteConnectFailureException 
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/saveCbt")
	public RetMsg<Serializable> saveCbt(@RequestBody IfsCfetsrmbCbt record) throws RemoteConnectFailureException, Exception {
		//1.检查会计类型
		String message = cfetsrmbCbtService.checkCust(record.getDealTransType(), record.getSellInst(),record.getCfetsno(), null, record.getProduct(), record.getProdType());
		if(StringUtil.isNotEmpty(message)) {//检查失败
			return RetMsgHelper.fail(message);
		}
		IfsCfetsrmbCbt cbt = cfetsrmbCbtService.getCbtById(record);
		if (cbt == null) {
			message = cfetsrmbCbtService.insertCbt(record);
		} else {
			try {
				cfetsrmbCbtService.updateCbt(record);
				message = "修改成功";
				RetMsgHelper.ok(message);
			} catch (Exception e) {
				e.printStackTrace();
				message = "修改失败";
				return RetMsgHelper.fail(message);
			}
		}
		return StringUtil.containsIgnoreCase(message, "成功")?RetMsgHelper.ok(message):RetMsgHelper.fail(message);
	}

	/**
	 * 成交单分页查询
	 * 
	 * @param IfsCfetsrmbCbt
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchCfetsrmbCbt")
	public RetMsg<PageInfo<IfsCfetsrmbCbt>> searchCfetsrmbCbt(@RequestBody IfsCfetsrmbCbt record) {
		Page<IfsCfetsrmbCbt> page = cfetsrmbCbtService.getCbtList(record);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 根据主键查询成交单
	 * 
	 * @param IfsCfetsrmbCbtKey
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchCfetsrmbCbtById")
	public RetMsg<IfsCfetsrmbCbt> searchCfetsrmbCbtById(@RequestBody IfsCfetsrmbCbtKey key) {
		IfsCfetsrmbCbt cbt = cfetsrmbCbtService.getCbtById(key);
		return RetMsgHelper.ok(cbt);
	}

	/**
	 * 根据主键查询成交单(返回值增加 流程id)
	 * 
	 * @param
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchCfetsrmbCbtAndFlowIdById")
	public RetMsg<IfsCfetsrmbCbt> searchCfetsrmbCbtAndFlowIdById(@RequestBody Map<String, Object> key) {
		IfsCfetsrmbCbt cbt = cfetsrmbCbtService.getCbtAndFlowIdById(key);
		return RetMsgHelper.ok(cbt);
	}

	/**
	 * 删除成交单
	 * 
	 * @param IfsCfetsrmbCbtKey
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/deleteCfetsrmbCbt")
	public RetMsg<Serializable> deleteCfetsrmbCbt(@RequestBody IfsCfetsrmbCbtKey key) {
		cfetsrmbCbtService.deleteCbt(key);
		return RetMsgHelper.ok();
	}

	/**
	 * 查询我发起的
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-21
	 */
	@ResponseBody
	@RequestMapping(value = "/searchRmbCbtSwapMine")
	public RetMsg<PageInfo<IfsCfetsrmbCbt>> searchRmbCbtSwapMine(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsrmbCbt> page = cfetsrmbCbtService.getRmbCbtMinePage(params, 3);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 查询 待审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageRmbCbtUnfinished")
	public RetMsg<PageInfo<IfsCfetsrmbCbt>> searchPageRmbCbtUnfinished(@RequestBody Map<String, Object> params) {
		String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse + ","
				+ DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
		String[] orderStatus = approveStatus.split(",");
		params.put("approveStatusNo", orderStatus);
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsrmbCbt> page = cfetsrmbCbtService.getRmbCbtMinePage(params, 1);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 查询 已审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageRmbCbtFinished")
	public RetMsg<PageInfo<IfsCfetsrmbCbt>> searchPageRmbCbtFinished(@RequestBody Map<String, Object> params) {
		String approveStatus = ParameterUtil.getString(params, "approveStatus", "");
		if (StringUtil.isNullOrEmpty(approveStatus)) {
			approveStatus = DictConstants.ApproveStatus.ApprovedPass;
			approveStatus += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatus += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatus += "," + DictConstants.ApproveStatus.Verified;
			approveStatus += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			// 20180502修改bug:已审批列表中增加"审批中"
			approveStatus += "," + DictConstants.ApproveStatus.Approving;

			String[] orderStatus = approveStatus.split(",");
			params.put("approveStatusNo", orderStatus);
		}
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsrmbCbt> page = cfetsrmbCbtService.getRmbCbtMinePage(params, 2);
		return RetMsgHelper.ok(page);
	}

	@ResponseBody
	@RequestMapping(value = "/searchPageApprovermbCbtPassed")
	public RetMsg<PageInfo<IfsCfetsrmbCbt>> searchPageApprovermbCbtPassed(@RequestBody Map<String, Object> params) {
		Page<IfsCfetsrmbCbt> page = cfetsrmbCbtService.getApproveCbtPassedPage(params);
		return RetMsgHelper.ok(page);
	}
	
	//根据ticketId查询
	@ResponseBody
	@RequestMapping(value = "/searchCfetsrmbCbtByTicketId")
	public RetMsg<IfsCfetsrmbCbt> searchCfetsrmbCbtByTicketId(@RequestBody Map<String, Object> map) {
		IfsCfetsrmbCbt cbt = cfetsrmbCbtService.searchCfetsrmbCbtByTicketId(map);
		return RetMsgHelper.ok(cbt);
	}
}
