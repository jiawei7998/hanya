package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.base.util.FileManage;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.UploadedFile;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.bean.SlHldyInfo;
import com.singlee.financial.common.util.DateUtil;
import com.singlee.financial.opics.IHldyDbAndFileServer;
import com.singlee.financial.opics.IStaticServer;
import com.singlee.ifs.baseUtil.SingleeExcelUtil;
import com.singlee.ifs.model.IfsOpicsHldy;
import com.singlee.ifs.service.IfsOpicsHldyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;

/***
 * 节假日
 *
 * @author lij
 *
 */
@Controller
@RequestMapping(value = "/IfsOpicsHldyController")
public class IfsOpicsHldyController {

    @Autowired
    IfsOpicsHldyService ifsOpicsHldyService;
    @Autowired
    IHldyDbAndFileServer dbAndFileServer;

    @Autowired
    IStaticServer iStaticServer;

    /***
     * 分页查询
     */
    @ResponseBody
    @RequestMapping(value = "/searchPageHldy")
    public RetMsg<PageInfo<IfsOpicsHldy>> searchPageHldy(@RequestBody Map<String, Object> map) {
        Page<IfsOpicsHldy> page = ifsOpicsHldyService.searchPageOpicsHldy(map);
        return RetMsgHelper.ok(page);
    }

    /**
     * 批量校准 全部校准
     */
    @ResponseBody
    @RequestMapping(value = "/batchCalibration2")
    public RetMsg<Serializable> batchCalibration2(@RequestBody Map<String, Object> map) {

        ifsOpicsHldyService.batchCalibration("2", null);
        return RetMsgHelper.ok();
    }

    /**
     * 批量校准 选中行校准
     */
    @ResponseBody
    @RequestMapping(value = "/batchCalibration1")
    public RetMsg<Serializable> batchCalibration1(@RequestBody List<IfsOpicsHldy> list) {

        ifsOpicsHldyService.batchCalibration("1", list);
        return RetMsgHelper.ok();
    }

    /**
     * 新增节假日
     *
     * @param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/insertHldy")
    public RetMsg<Serializable> insertHldy(@RequestBody String[] str) {
        String mgs = null;
        for (int i = 0; i < str.length; i++) {
            IfsOpicsHldy ifsOpicsHldy = new IfsOpicsHldy();

            try {
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                String calDate1 = str[i];
                Date date = format.parse(calDate1);
                ifsOpicsHldy.setCalendarid("CNY");
                ifsOpicsHldy.setHolidate(date);
                ifsOpicsHldy.setHolidaytype("1");// 设置是否为节假日：是
                ifsOpicsHldy.setStatus("0");// 设置同步状态：未同步
                ifsOpicsHldy.setOperator(SlSessionHelper.getUserId());
                ifsOpicsHldy.setLstmntdte(new Date());

                int count = ifsOpicsHldyService.queryDateById(ifsOpicsHldy);

                if (count == 0) {
                    mgs = ifsOpicsHldyService.insert(ifsOpicsHldy);

                } else {
                    return RetMsgHelper.simple("您选中的日期已设置为节假日日期!");
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }// end for
        if (mgs == null || "000".equalsIgnoreCase(mgs)) {
            return RetMsgHelper.ok();
        } else {
            return RetMsgHelper.simple("001", mgs);
        }
    }

    /***
     * 取消节假日，即：删除数据
     *
     * @param str
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/deleteHldy")
    public RetMsg<Serializable> deleteHldy(@RequestBody String[] str) {
        for (int i = 0; i < str.length; i++) {
            IfsOpicsHldy ifsOpicsHldy = new IfsOpicsHldy();
            try {

                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                String calDate1 = str[i];
                Date date = format.parse(calDate1);
                ifsOpicsHldy.setCalendarid("CNY");
                ifsOpicsHldy.setHolidate(date);
                ifsOpicsHldy.setHolidaytype("1");// 设置是否为节假日：是
                ifsOpicsHldy.setOperator(SlSessionHelper.getUserId());
                ifsOpicsHldy.setLstmntdte(new Date());

                int count = ifsOpicsHldyService.queryDateById(ifsOpicsHldy);

                if (count != 0) {
                    IfsOpicsHldy ifsOpicsHldy1 = ifsOpicsHldyService.searchEntityById(ifsOpicsHldy);
                    if (ifsOpicsHldy1 != null) {
                        if ("1".equals(ifsOpicsHldy1.getStatus())) {// 已同步的数据就不要删除
                            return RetMsgHelper.simple("已同步的节假日期不可以取消设置!");

                        } else {// 未同步的数据就可以删除
                            ifsOpicsHldyService.delete(ifsOpicsHldy);
                        }

                    }

                } else {
                    return RetMsgHelper.simple("您选中的日期不是节假日日期!");
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }
        return RetMsgHelper.ok();
    }

    @ResponseBody
    @RequestMapping(value = "/getCalendarByMonth")
    public RetMsg<List<IfsOpicsHldy>> getCalendarByMonth(@RequestBody Map<String, Object> map) throws Exception {
        List<IfsOpicsHldy> list = ifsOpicsHldyService.getCalendarByMonth(map);
        return RetMsgHelper.ok(list);
    }

    /***
     * 根据id 查询已同步的数据
     *
     * @param entity
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getCalendarById")
    public RetMsg<IfsOpicsHldy> getCalendarById(@RequestBody IfsOpicsHldy entity) {
        IfsOpicsHldy ifsOpicsHldy = new IfsOpicsHldy();
        entity.setStatus("1");
        entity.setCalendarid("CNY");
        entity.setHolidaytype("1");
        ifsOpicsHldy = ifsOpicsHldyService.searchEntityById(entity);
        /*
         * map.put("status", "1"); map.put("calendarid", "CNY"); map.put("holidaytype", "1");
         */

        // ifsOpicsHldy = ifsOpicsHldyService.queryEntityById(map);

        return RetMsgHelper.ok(ifsOpicsHldy);
    }

    @ResponseBody
    @RequestMapping(value = "/importHildyExcel")
    public String importHildyExcel(MultipartHttpServletRequest request, HttpServletResponse response) {
        String ret = "导入成功";
        //获取传入输入流
        InputStream is = null;
        //提取文件
        List<UploadedFile> uploadedFileList = null;
        try {
            uploadedFileList = FileManage.requestExtractor(request);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //解析假日文件
        for (UploadedFile uploadedFile : uploadedFileList) {
            JY.require("xlsx".equals(uploadedFile.getType()) || "xls".equals(uploadedFile.getType()), "上传文件类型错误,仅允许上传xls,xlsx格式文件.");
            is = new ByteArrayInputStream((uploadedFile.getBytes()));
            //调用解析
            List<SlHldyInfo> hildy = SingleeExcelUtil.readDataByIndex(is, uploadedFile.getType());
            //开始判断数据
            //循环处理数据
            for (SlHldyInfo hldyInfo : hildy) {
                String ccy = hldyInfo.getCcy();
                List<Date> hildyLst = hldyInfo.getHilDayLst();
                List<Date> adj = hldyInfo.getAdjDayLst();
                //获取50年的周六周末
                Calendar cal = Calendar.getInstance();
                cal.setTime(hildyLst.get(0));
                int year = cal.get(Calendar.YEAR);
                List<Date> weekday = DateUtil.getWeekDay(year, year + 50, ccy);// 默认取传入年数据
                //开始处理日期[hldy表中只存假日，如果本来就是假日，则需要剔除为工作日]
                HashSet<Date> hildySet = new HashSet<>();
                //复制周末假日
                hildySet.addAll(weekday);
                //复制特殊假日
                hildySet.addAll(hildyLst);
                //判断是是否包含关系
                if (!hildyLst.containsAll(adj)) {
                    return "调休日期不在假日范围内,请检查调休日期是否正常!";
                }
                for (Date adjDt : adj) {
                    Calendar clr = Calendar.getInstance();
                    clr.setTime(adjDt);
                    int yr = cal.get(Calendar.YEAR);
                    if (yr != year) {
                        return "调休日期年份不为同一年份,请检查调休日期是否正常!";
                    }
                }
            }
            //调用opics客制化Api/假日/调休日
            try {
                dbAndFileServer.addHldyProcess(hildy);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (null != is) {
                    try {
                        is.close();
                    } catch (IOException e) {
                        ret = e.getMessage();
                    }
                }
            }
        }
        return ret;
    }


//    @ResponseBody
//    @RequestMapping(value = "/importHildyWord")
//    @Deprecated
//    public String importHildyWord(MultipartHttpServletRequest request, HttpServletResponse response) {
//        String ret = "导入失败";
//        try {
//            List<UploadedFile> uploadedFileList = FileManage.requestExtractor(request);
//            Iterator var3 = uploadedFileList.iterator();
//            HashMap<String, List<Date>> hldyJson = new HashMap<String, List<Date>>();
//            String year = "";
//            while (var3.hasNext()) {
//                UploadedFile uploadedFile = (UploadedFile) var3.next();
//                JY.require("docx".equals(uploadedFile.getType()) || "doc".equals(uploadedFile.getType()), "上传文件类型错误,仅允许上传doc,docx格式文件.");
//                String fileNm = uploadedFile.getName();
//                year = fileNm.substring(uploadedFile.getName().indexOf("年") - 4, uploadedFile.getName().indexOf("年") + 1);
//                InputStream fis = new ByteArrayInputStream((uploadedFile.getBytes()));
//                XWPFDocument xdoc = new XWPFDocument(fis);
//                XWPFWordExtractor extractor = new XWPFWordExtractor(xdoc);
//                String doc1 = extractor.getText().replaceAll("（", "(").replaceAll("）", ")").replaceAll("：", ":");
//                String[] dtLine = doc1.split("\n");
//                // 处理
//                for (int i = 0; i < dtLine.length; i++) {
//                    if (!(dtLine[i].contains("月") && dtLine[i].contains("日"))) {
//                        continue;
//                    }
//                    String ccy = dtLine[i].split(":")[0];
//
//
//                    String[] dt = dtLine[i].split(":")[1].split("、");
//                    // 处理排序
//                    List<String> dates = new ArrayList<String>();
//                    List<Date> daysNew = new ArrayList<Date>();
//                    for (int j = 0; j < dt.length; j++) {
//                        String tmpDt = year + dt[j];
//                        SimpleDateFormat dateFormat = new SimpleDateFormat(getDateFormat(tmpDt), Locale.CHINA);
//                        Date cfsDte = dateFormat.parse(tmpDt);
//                        dates.add(DateUtil.format(cfsDte));// 交易中心下发日期
//                    }
//                    for (Date defDte : getWeekendInYear(ccy)) {
//                        String defDteStr = DateUtil.format(defDte);
//                        if (!dates.contains(defDteStr)) {// 判断非重复记录
//                            dates.add(defDteStr);// 默认周六周日
//                        }
//                    }
//                    for (String date : dates) {
//                        daysNew.add(DateUtils.parseDate(date, "yyyy-MM-dd"));
//                    }
//
//
//                    String s = "[a-zA-Z0-9]";
//                    Pattern pattern = Pattern.compile(s);
//                    Matcher m = pattern.matcher(ccy);
//                    StringBuffer sb = new StringBuffer();
//                    while (m.find()) {
//                        sb.append(m.group());
//                    }
//                    String ccyNew = sb.toString();
//                    if (StringUtils.isNotEmpty(ccyNew)) {
//                        hldyJson.put(ccyNew, daysNew);
//                    } else {
//                        hldyJson.put(ccy, daysNew);
//                    }
//
//                }
//                System.out.println(JSON.toJSON(hldyJson));
//                dbAndFileServer.addHldyProcess(hldyJson);
//                ret = "上传成功";
//                fis.close();
//            }
//        } catch (ParseException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (Exception exception) {
//            exception.printStackTrace();
//        }
//        return ret;
//    }

    /**
     * 常规自动日期格式识别
     *
     * @param str 时间字符串
     * @return Date
     * @author dc
     */
    private static String getDateFormat(String str) {
        boolean year = false;
        Pattern pattern = Pattern.compile("^[-\\+]?[\\d]*$");
        if (pattern.matcher(str.substring(0, 4)).matches()) {
            year = true;
        }
        StringBuilder sb = new StringBuilder();
        int index = 0;
        if (!year) {
            if (str.contains("月") || str.contains("-") || str.contains("/")) {
                if (Character.isDigit(str.charAt(0))) {
                    index = 1;
                }
            } else {
                index = 3;
            }
        }
        for (int i = 0; i < str.length(); i++) {
            char chr = str.charAt(i);
            if (Character.isDigit(chr)) {
                if (index == 0) {
                    sb.append("y");
                }
                if (index == 1) {
                    sb.append("M");
                }
                if (index == 2) {
                    sb.append("d");
                }
                if (index == 3) {
                    sb.append("H");
                }
                if (index == 4) {
                    sb.append("m");
                }
                if (index == 5) {
                    sb.append("s");
                }
                if (index == 6) {
                    sb.append("S");
                }
            } else {
                if (i > 0) {
                    char lastChar = str.charAt(i - 1);
                    if (Character.isDigit(lastChar)) {
                        index++;
                    }
                }
                sb.append(chr);
            }
        }
        return sb.toString();
    }

    /**
     * 获取周六周日
     *
     * @return
     */
    public static List<Date> getWeekendInYear(String ccy) {
        List<Date> list = new ArrayList<Date>();
        Calendar c = Calendar.getInstance();
        c.set(c.get(Calendar.YEAR), 0, 1);
        Calendar c2 = Calendar.getInstance();
        c2.set(c.get(Calendar.YEAR) + 1, 0, 1);
        while (c.compareTo(c2) < 0) {

            if (ccy.contains("AED") || ccy.contains("SAR")) {// 阿联酋迪拉姆（AED）和沙特里亚尔（SAR）的周末为周五、周六
                if (c.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY
                        || c.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
                    list.add(c.getTime());
                }
            } else {
                if (c.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY
                        || c.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
                    list.add(c.getTime());
                }
            }
            // 日期+1
            c.add(Calendar.DATE, 1);
        }
        return list;
    }
}
