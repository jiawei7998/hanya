package com.singlee.ifs.controller;

import com.singlee.financial.bean.SlProperties;
import com.singlee.financial.opics.IPropertiesServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;



@Controller
@RequestMapping(value = "/IpropertiesController")
public class IpropertiesController {
	
	
	@Autowired
	private IPropertiesServer propertiesServer;
	
	@ResponseBody
	@RequestMapping(value = "/editProperties")
	public void editProperties(@RequestBody List<SlProperties> properties) throws Exception{
		for(int i=0; i<properties.size(); i++){
			propertiesServer.operateProperties(properties.get(i));
		}
	}
	
	
	@ResponseBody
	@RequestMapping(value = "/getPropertiesList")
	public List<SlProperties> getPropertiesList() throws Exception{
		List<SlProperties> msg= propertiesServer.getProperties();
		return msg;
	}
}
