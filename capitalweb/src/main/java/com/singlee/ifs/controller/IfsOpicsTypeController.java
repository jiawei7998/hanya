package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.bean.SlProdTypeBean;
import com.singlee.financial.opics.IStaticServer;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.SlErrors;
import com.singlee.ifs.mapper.IfsOpicsTypeMapper;
import com.singlee.ifs.model.IfsOpicsType;
import com.singlee.ifs.service.IfsOpicsTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 
 * 产品类型Controller
 * 
 * ClassName: IfsOpicsTypeController <br/>
 * date: 2018-5-30 下午07:43:38 <br/>
 * 
 * @author zhengfl
 * @version
 * @since JDK 1.6
 */
@Controller
@RequestMapping(value = "/IfsOpicsTypeController")
public class IfsOpicsTypeController {
	
	@Autowired
	IfsOpicsTypeService ifsOpicsTypeService;
	
	@Autowired
	IStaticServer iNonParamServer;
	
	@Resource
	IfsOpicsTypeMapper ifsOpicsTypeMapper;

	/***
	 * 分页查询
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageType")
	public RetMsg<PageInfo<IfsOpicsType>> searchPageType(@RequestBody Map<String, Object> map) {
		Page<IfsOpicsType> page = ifsOpicsTypeService.searchPageOpicsType(map);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 新增
	 */
	@ResponseBody
	@RequestMapping(value = "/saveOpicsTypeAdd")
	public RetMsg<Serializable> saveOpicsTypeAdd(@RequestBody IfsOpicsType entity) {
		ifsOpicsTypeService.insert(entity);
		return RetMsgHelper.ok();
	}

	/**
	 * 修改
	 */
	@ResponseBody
	@RequestMapping(value = "/saveOpicsTypeEdit")
	public RetMsg<Serializable> saveOpicsTypeEdit(@RequestBody IfsOpicsType entity) {
		ifsOpicsTypeService.updateById(entity);
		return RetMsgHelper.ok();
	}

	/**
	 * 删除
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteType")
	public RetMsg<Serializable> deleteType(@RequestBody Map<String, Object> map) {
		ifsOpicsTypeService.deleteById(map);
		return RetMsgHelper.ok();
	}

	/**
	 * 同步到opics表
	 */
	@ResponseBody
	@RequestMapping(value = "/syncType")
	public RetMsg<SlOutBean> syncType(@RequestBody Map<String, String> map) {
		SlOutBean slOutBean = new SlOutBean();

		IfsOpicsType ifsOpicsType = ifsOpicsTypeService.searchById(map);
		SlProdTypeBean prodTypeBean = new SlProdTypeBean();
		prodTypeBean.setAl(ifsOpicsType.getAl());
		prodTypeBean.setDescr(ifsOpicsType.getDescr());
		Date lstmntdte = DateUtil.parse(DateUtil.format(ifsOpicsType.getLstmntdte()));
		prodTypeBean.setLstmntdte(lstmntdte);
		prodTypeBean.setProdCode(ifsOpicsType.getProdcode());
		prodTypeBean.setProdType(ifsOpicsType.getType());

		try {
			slOutBean = iNonParamServer.addProdType(prodTypeBean);
		} catch (RemoteConnectFailureException e) {
			slOutBean.setRetCode(SlErrors.FAILED_RMI_OPICS.getErrCode());
			slOutBean.setRetMsg(SlErrors.FAILED_RMI_OPICS.getErrMsg() + ":" + e.getMessage());
			slOutBean.setRetStatus(RetStatusEnum.F);
		} catch (Exception e) {
			slOutBean.setRetCode(SlErrors.FAILED.getErrCode());
			slOutBean.setRetMsg(SlErrors.FAILED.getErrMsg() + ":" + e.getMessage());
			slOutBean.setRetStatus(RetStatusEnum.F);
		}
		if ("S".equals(String.valueOf(slOutBean.getRetStatus()))) {
			ifsOpicsType.setStatus("1");
			ifsOpicsTypeService.updateStatus(ifsOpicsType);
		}
		return RetMsgHelper.ok(slOutBean);
	}

	/**
	 * 批量校准
	 */
	@ResponseBody
	@RequestMapping(value = "/batchCalibration")
	public RetMsg<SlOutBean> batchCalibration(@RequestBody Map<String, String> map) {
		SlOutBean slOutBean = new SlOutBean();
		String type = String.valueOf(map.get("type"));
		String prodcode;
		String prodtype;
		String[] prodcodes;
		String[] prodtypes;
		if ("1".equals(type)) {
			prodcode = map.get("prodcode");
			prodtype = map.get("prodtype");
			prodcodes = prodcode.split(",");
			prodtypes = prodtype.split(",");
		} else {
			prodcodes = null;
			prodtypes = null;
		}
		try {
			slOutBean = ifsOpicsTypeService.batchCalibration(type, prodcodes, prodtypes);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return RetMsgHelper.ok(slOutBean);
	}
	
	/**
	 * 查询已同步的产品类型
	 */
	@ResponseBody
	@RequestMapping(value = "/searchProdtype")
	public List<IfsOpicsType> searchProdtype() {
		List<IfsOpicsType> result = ifsOpicsTypeMapper.searchProdtype();
		return result;
	}
}