package com.singlee.ifs.controller;

import com.github.pagehelper.StringUtil;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.dayend.service.DayendDateServiceProxy;
import com.singlee.financial.bean.*;
import com.singlee.financial.opics.IAcupServer;
import com.singlee.financial.opics.IBaseServer;
import com.singlee.financial.opics.IFxdhServer;
import com.singlee.financial.opics.ISpshServer;
import com.singlee.financial.page.PageInfo;
import com.singlee.ifs.model.*;
import com.singlee.ifs.service.IFSService;
import com.singlee.ifs.service.IfsOpicsActyService;
import com.singlee.ifs.service.IfsOpicsBondService;
import com.singlee.ifs.service.IfsOpicsCustService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;
import java.util.*;

/***
 *报表
 * 
 */
@Controller
@RequestMapping(value = "/IfsReportController")
public class IfsReportController {
	
	@Autowired
	IBaseServer baseServer;
	@Autowired
	IfsOpicsBondService ifsOpicsBondService;
	@Autowired
	IfsOpicsCustService ifsOpicsCustService;
	@Autowired
	IfsOpicsActyService ifsOpicsActyService;
	@Autowired
	IAcupServer acupServer;
	@Autowired
	private IFSService iFSService;
	@Autowired
	private IFxdhServer fxdhServer;
	@Autowired
	private ISpshServer spshServer;
	
	/***
	 * 分页查询
	 * @throws Exception 
	 * @throws RemoteConnectFailureException 
	 */
	@ResponseBody
	@RequestMapping(value = "/searchBondPositionBalanceList")
	public RetMsg<PageInfo<SlBondPositionBalanceBean>> searchBondPositionBalanceList(@RequestBody Map<String, Object> map) throws RemoteConnectFailureException, Exception {
		PageInfo<SlBondPositionBalanceBean> page = baseServer.getBondPositionBalance(map);
		List<SlBondPositionBalanceBean> list=page.getList();
		List<SlBondPositionBalanceBean> newList=new ArrayList<SlBondPositionBalanceBean>();
		for (SlBondPositionBalanceBean slBondPositionBalanceBean : list) {
			SlBondPositionBalanceBean bean=new SlBondPositionBalanceBean();
			bean=slBondPositionBalanceBean;
			if(StringUtil.isNotEmpty(slBondPositionBalanceBean.getIssuer())){
				IfsOpicsCust cust=ifsOpicsCustService.searchIfsOpicsCust(slBondPositionBalanceBean.getIssuer().trim());
				if (cust != null) {
					if(StringUtil.isNotEmpty(cust.getCliname())){
						bean.setIssuer(cust.getCliname());
					}
					bean.setSubjectrating(cust.getRatresult1());
				}
			}
			if(StringUtil.isNotEmpty(slBondPositionBalanceBean.getSecid())){
				IfsOpicsBond bond=ifsOpicsBondService.searchOneById(slBondPositionBalanceBean.getSecid().trim());
				if (bond != null) {
					bean.setBondtrating(bond.getBondrating());
				}
			}
			if(StringUtil.isNotEmpty(slBondPositionBalanceBean.getAcctngtype())){
				IfsOpicsActy acty=ifsOpicsActyService.searchById(slBondPositionBalanceBean.getAcctngtype().trim());
				if (acty != null) {
					if (StringUtil.isNotEmpty(acty.getAcctdesccn())) {
						bean.setAcctngtype(acty.getAcctdesccn());
					}
				}
			}
			newList.add(bean);
		}
		PageInfo<SlBondPositionBalanceBean> page2=new PageInfo<SlBondPositionBalanceBean>();
		page2.setList(newList);
		page2.setTotal(page.getTotal());
		return RetMsgHelper.ok(page2);
	}

	
	
	@ResponseBody
	@RequestMapping(value = "/searchBondInvestmentHistoryList")
	public RetMsg<PageInfo<SlSdCdInvestHistoryBean>> searchBondInvestmentHistoryList(@RequestBody Map<String, Object> map) throws RemoteConnectFailureException, Exception {
		PageInfo<SlSdCdInvestHistoryBean> page = baseServer.getSdInvestHistory(map);
		return RetMsgHelper.ok(page);
	}
	
	
	@ResponseBody
	@RequestMapping(value = "/searchCdInvestmentHistoryList")
	public RetMsg<PageInfo<SlSdCdInvestHistoryBean>> searchCdInvestmentHistoryList(@RequestBody Map<String, Object> map) throws RemoteConnectFailureException, Exception {
		PageInfo<SlSdCdInvestHistoryBean> page = baseServer.getCdInvestHistory(map);
		return RetMsgHelper.ok(page);
	}
	
	
	@ResponseBody
	@RequestMapping(value = "/searchBondProfitLossList")
	public RetMsg<List<SlSdCdProfitAndLossBean>> searchBondProfitLossList(@RequestBody Map<String, Object> map) throws RemoteConnectFailureException, Exception {
		List<SlSdCdProfitAndLossBean> page = baseServer.getSdProfitAndLoss(map);
		return RetMsgHelper.ok(page);
	}
	
	@ResponseBody
	@RequestMapping(value = "/searchCdProfitLossList")
	public RetMsg<List<SlSdCdProfitAndLossBean>> searchCdProfitLossList(@RequestBody Map<String, Object> map) throws RemoteConnectFailureException, Exception {
		List<SlSdCdProfitAndLossBean> page = baseServer.getCdProfitAndLoss(map);
		return RetMsgHelper.ok(page);
	}
	
	@ResponseBody
	@RequestMapping(value = "/searchCdIssueDayBalanceList")
	public RetMsg<PageInfo<SlCdIssueBalanceBean>> searchCdIssueDayBalanceList(@RequestBody Map<String, Object> map) throws RemoteConnectFailureException, Exception {
		Map<String,String> opicsMap=new HashMap<String, String>();
		opicsMap.put("br", "01");
		String opicsdate=acupServer.getAcupDate(opicsMap);
		String datadate=(String)map.get("datadate");
		if(datadate==null|| "".equals(datadate)){
			map.put("datadate", opicsdate);
		}
		PageInfo<SlCdIssueBalanceBean> page = baseServer.getCdIssueBalance(map);
		List<SlCdIssueBalanceBean> list=page.getList();
		List<SlCdIssueBalanceBean> newList=new ArrayList<SlCdIssueBalanceBean>();
		for (SlCdIssueBalanceBean slCdIssueBalanceBean : list) {
			SlCdIssueBalanceBean bean=new SlCdIssueBalanceBean();
			bean=slCdIssueBalanceBean;
			if(StringUtil.isNotEmpty(slCdIssueBalanceBean.getSubname())){
				IfsOpicsCust cust = ifsOpicsCustService.searchIfsOpicsCust(slCdIssueBalanceBean.getSubname().trim());
				if (cust != null) {
					if (StringUtil.isNotEmpty(cust.getCliname())) {
						bean.setSubname(cust.getCliname());
					}
				}
			}
			if(StringUtil.isNotEmpty(slCdIssueBalanceBean.getSecid())){
				IfsOpicsBond bond = ifsOpicsBondService.searchOneById(slCdIssueBalanceBean.getSecid().trim());
				if (bond != null) {
					bean.setSecidnm(bond.getBndnm_en());
				}
			}
			/*if(StringUtil.isNotEmpty(slCdIssueBalanceBean.getInsttype())){
				IfsOpicsActy acty = ifsOpicsActyService.searchById(slCdIssueBalanceBean.getInsttype().trim());
				if (acty != null) {
					if (StringUtil.isNotEmpty(acty.getAcctdesccn())) {
						bean.setInsttype(acty.getAcctdesccn());
					}
				}
			}*/
			newList.add(bean);
		}
		PageInfo<SlCdIssueBalanceBean> page2=new PageInfo<SlCdIssueBalanceBean>();
		page2.setList(newList);
		page2.setTotal(page.getTotal());
		return RetMsgHelper.ok(page2);
	}
	
	/**
	 * 外汇交易业务校验
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/searchSptCheck")
	public RetMsg<PageInfo<IfsCfetsfxSpt>> searchSptCheck(@RequestBody Map<String, Object> map) throws RemoteConnectFailureException, Exception {
		PageInfo<IfsCfetsfxSpt> page=iFSService.searchSptCheck(map);
		List<IfsCfetsfxSpt> list=page.getList();
		List<IfsCfetsfxSpt> newList=new ArrayList<IfsCfetsfxSpt>();
		for (IfsCfetsfxSpt ifsCfetsfxSpt : list) {
			IfsCfetsfxSpt spt=new IfsCfetsfxSpt();
			spt=ifsCfetsfxSpt;
			Map<String, Object> opicsmap=new HashMap<String, Object>();
			opicsmap.put("dealno", ifsCfetsfxSpt.getDealNo());
			SlFxdhBean fxdhBean=fxdhServer.getFxdhBean(opicsmap);
			if(fxdhBean!=null){
				spt.setCcyamount(fxdhBean.getCcyamt());
				spt.setCtramount(fxdhBean.getCtramt());
				spt.setCcycheck(ifsCfetsfxSpt.getBuyAmount().subtract(fxdhBean.getCcyamt()));
				spt.setCtrcheck(ifsCfetsfxSpt.getSellAmount().subtract(fxdhBean.getCtramt()));
			}else{
				spt.setCcyamount(new BigDecimal(0));
				spt.setCtramount(new BigDecimal(0));
				spt.setCcycheck(ifsCfetsfxSpt.getBuyAmount().subtract(new BigDecimal(0)));
				spt.setCtrcheck(ifsCfetsfxSpt.getSellAmount().subtract(new BigDecimal(0)));
			}
			newList.add(spt);
		}
		PageInfo<IfsCfetsfxSpt> page2=new PageInfo<IfsCfetsfxSpt>();
		page2.setList(newList);
		page2.setTotal(page.getTotal());
		return RetMsgHelper.ok(page2);
	}
	
	/**
	 * 现券交易类业务校验
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/searchCbtCheck")
	public RetMsg<PageInfo<IfsCfetsrmbCbt>> searchCbtCheck(@RequestBody Map<String, Object> map) throws RemoteConnectFailureException, Exception {
		PageInfo<IfsCfetsrmbCbt> page=iFSService.searchCbtCheck(map);
		List<IfsCfetsrmbCbt> list=page.getList();
		List<IfsCfetsrmbCbt> newList=new ArrayList<IfsCfetsrmbCbt>();
		for (IfsCfetsrmbCbt ifsCfetsrmbCbt : list) {
			IfsCfetsrmbCbt cbt=new IfsCfetsrmbCbt();
			cbt=ifsCfetsrmbCbt;
			Map<String, Object> opicsmap=new HashMap<String, Object>();
			opicsmap.put("dealno", ifsCfetsrmbCbt.getDealNo());
			SlSpshBean spshBean=spshServer.getSpshBean(opicsmap);
			if(spshBean!=null){
				cbt.setOpicsprice(spshBean.getPrice_8());
				cbt.setOpicssettleamt(spshBean.getSettamt());
				cbt.setPricecheck(ifsCfetsrmbCbt.getCleanPrice().subtract(spshBean.getPrice_8()));
				cbt.setAmtcheck(ifsCfetsrmbCbt.getSettlementAmount().subtract(spshBean.getSettamt()));
			}else{
				cbt.setOpicsprice(new BigDecimal(0));
				cbt.setOpicssettleamt(new BigDecimal(0));
				cbt.setPricecheck(ifsCfetsrmbCbt.getCleanPrice().subtract(new BigDecimal(0)));
				cbt.setAmtcheck(ifsCfetsrmbCbt.getSettlementAmount().subtract(new BigDecimal(0)));
			}
			newList.add(cbt);
		}
		PageInfo<IfsCfetsrmbCbt> page2=new PageInfo<IfsCfetsrmbCbt>();
		page2.setList(newList);
		page2.setTotal(page.getTotal());
		return RetMsgHelper.ok(page2);
	}
	
	/**
	 * 资金往来类业务校验
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/searchCapitalCheck")
	public RetMsg<PageInfo<IfsCfetsrmbOr>> searchCapitalCheck(@RequestBody Map<String, Object> map) throws RemoteConnectFailureException, Exception {
		PageInfo<IfsCfetsrmbOr> page=iFSService.searchCapitalCheck(map);
		List<IfsCfetsrmbOr> list=page.getList();
		List<IfsCfetsrmbOr> newList=new ArrayList<IfsCfetsrmbOr>();
		for (IfsCfetsrmbOr ifsCfetsrmbOr : list) {
			IfsCfetsrmbOr or=new IfsCfetsrmbOr();
			or=ifsCfetsrmbOr;
			Map<String, Object> opicsmap=new HashMap<String, Object>();
			opicsmap.put("dealno", ifsCfetsrmbOr.getDealNo());
			SlRprhDldtBean rprhDldtBean=spshServer.getRprhDldtBean(opicsmap);
			if(rprhDldtBean!=null){
				or.setComamount(rprhDldtBean.getComamt());
				or.setMatamount(rprhDldtBean.getMatamt());
				or.setComcheck(ifsCfetsrmbOr.getFirstSettlementAmount().subtract(rprhDldtBean.getComamt()));
				or.setMatcheck(ifsCfetsrmbOr.getSecondSettlementAmount().subtract(rprhDldtBean.getMatamt()));
			}else{
				or.setComamount(new BigDecimal(0));
				or.setMatamount(new BigDecimal(0));
				or.setComcheck(ifsCfetsrmbOr.getFirstSettlementAmount().subtract(new BigDecimal(0)));
				or.setMatcheck(ifsCfetsrmbOr.getSecondSettlementAmount().subtract(new BigDecimal(0)));
			}
			newList.add(or);
		}
		PageInfo<IfsCfetsrmbOr> page2=new PageInfo<IfsCfetsrmbOr>();
		page2.setList(newList);
		page2.setTotal(page.getTotal());
		return RetMsgHelper.ok(page2);
	}
	
	/**
	 * 外汇交易业务校验汇总
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/searchSptCheckTotal")
	public RetMsg<IfsCfetsfxSpt> searchSptCheckTotal(@RequestBody Map<String, Object> map) throws RemoteConnectFailureException, Exception {
		List<IfsCfetsfxSpt> list=iFSService.searchSptCheckList(map);
		List<IfsCfetsfxSpt> newList=new ArrayList<IfsCfetsfxSpt>();
		for (IfsCfetsfxSpt ifsCfetsfxSpt : list) {
			IfsCfetsfxSpt spt=new IfsCfetsfxSpt();
			spt=ifsCfetsfxSpt;
			Map<String, Object> opicsmap=new HashMap<String, Object>();
			opicsmap.put("dealno", ifsCfetsfxSpt.getDealNo());
			SlFxdhBean fxdhBean=fxdhServer.getFxdhBean(opicsmap);
			if(fxdhBean!=null){
				spt.setCcyamount(fxdhBean.getCcyamt());
				spt.setCtramount(fxdhBean.getCtramt());
				spt.setCcycheck(ifsCfetsfxSpt.getBuyAmount().subtract(fxdhBean.getCcyamt()));
				spt.setCtrcheck(ifsCfetsfxSpt.getSellAmount().subtract(fxdhBean.getCtramt()));
			}else{
				spt.setCcyamount(new BigDecimal(0));
				spt.setCtramount(new BigDecimal(0));
				spt.setCcycheck(ifsCfetsfxSpt.getBuyAmount().subtract(new BigDecimal(0)));
				spt.setCtrcheck(ifsCfetsfxSpt.getSellAmount().subtract(new BigDecimal(0)));
			}
			newList.add(spt);
		}
		IfsCfetsfxSpt fxspt=new IfsCfetsfxSpt();
		BigDecimal ccycheck=new BigDecimal(0);
		BigDecimal ctrcheck=new BigDecimal(0);
		BigDecimal ccyamount=new BigDecimal(0);
		BigDecimal ctramount=new BigDecimal(0);
		BigDecimal buyAmount=new BigDecimal(0);
		BigDecimal sellAmount=new BigDecimal(0);
		
		
		for (IfsCfetsfxSpt ifsCfetsfxSpt : newList) {
			ccycheck=ccycheck.add(ifsCfetsfxSpt.getCcycheck());
			ctrcheck=ctrcheck.add(ifsCfetsfxSpt.getCtrcheck());
			ccyamount=ccyamount.add(ifsCfetsfxSpt.getCcyamount());
			ctramount=ctramount.add(ifsCfetsfxSpt.getCtramount());
			buyAmount=buyAmount.add(ifsCfetsfxSpt.getBuyAmount());
			sellAmount=sellAmount.add(ifsCfetsfxSpt.getSellAmount());
		}
		fxspt.setCcycheck(ccycheck);
		fxspt.setCtrcheck(ctrcheck);
		fxspt.setCcyamount(ccyamount);
		fxspt.setCtramount(ctramount);
		fxspt.setBuyAmount(buyAmount);
		fxspt.setSellAmount(sellAmount);
		return RetMsgHelper.ok(fxspt);
	}
	
	/**
	 * 现券交易类业务校验汇总
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/searchCbtCheckTotal")
	public RetMsg<IfsCfetsrmbCbt> searchCbtCheckTotal(@RequestBody Map<String, Object> map) throws RemoteConnectFailureException, Exception {
		List<IfsCfetsrmbCbt> list=iFSService.searchCbtCheckList(map);
		List<IfsCfetsrmbCbt> newList=new ArrayList<IfsCfetsrmbCbt>();
		for (IfsCfetsrmbCbt ifsCfetsrmbCbt : list) {
			IfsCfetsrmbCbt cbt=new IfsCfetsrmbCbt();
			cbt=ifsCfetsrmbCbt;
			Map<String, Object> opicsmap=new HashMap<String, Object>();
			opicsmap.put("dealno", ifsCfetsrmbCbt.getDealNo());
			SlSpshBean spshBean=spshServer.getSpshBean(opicsmap);
			if(spshBean!=null){
				cbt.setOpicsprice(spshBean.getPrice_8());
				cbt.setOpicssettleamt(spshBean.getSettamt());
				cbt.setPricecheck(ifsCfetsrmbCbt.getCleanPrice().subtract(spshBean.getPrice_8()));
				cbt.setAmtcheck(ifsCfetsrmbCbt.getSettlementAmount().subtract(spshBean.getSettamt()));
			}else{
				cbt.setOpicsprice(new BigDecimal(0));
				cbt.setOpicssettleamt(new BigDecimal(0));
				cbt.setPricecheck(ifsCfetsrmbCbt.getCleanPrice().subtract(new BigDecimal(0)));
				cbt.setAmtcheck(ifsCfetsrmbCbt.getSettlementAmount().subtract(new BigDecimal(0)));
			}
			newList.add(cbt);
		}
		IfsCfetsrmbCbt rmbCbt=new IfsCfetsrmbCbt();
		BigDecimal pricecheck=new BigDecimal(0);
		BigDecimal amtcheck=new BigDecimal(0);
		BigDecimal opicsprice=new BigDecimal(0);
		BigDecimal opicssettleamt=new BigDecimal(0);
		BigDecimal cleanPrice=new BigDecimal(0);
		BigDecimal settlementAmount=new BigDecimal(0);
		
		for (IfsCfetsrmbCbt ifsCfetsrmbCbt : newList) {
			pricecheck=pricecheck.add(ifsCfetsrmbCbt.getPricecheck());
			amtcheck=amtcheck.add(ifsCfetsrmbCbt.getAmtcheck());
			opicsprice=opicsprice.add(ifsCfetsrmbCbt.getOpicsprice());
			opicssettleamt=opicssettleamt.add(ifsCfetsrmbCbt.getOpicssettleamt());
			cleanPrice=cleanPrice.add(ifsCfetsrmbCbt.getCleanPrice());
			settlementAmount=settlementAmount.add(ifsCfetsrmbCbt.getSettlementAmount());
		}
		rmbCbt.setPricecheck(pricecheck);
		rmbCbt.setAmtcheck(amtcheck);
		rmbCbt.setOpicsprice(opicsprice);
		rmbCbt.setOpicssettleamt(opicssettleamt);
		rmbCbt.setCleanPrice(cleanPrice);
		rmbCbt.setSettlementAmount(settlementAmount);
		return RetMsgHelper.ok(rmbCbt);
	}
	
	/**
	 * 资金往来类业务校验汇总
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/searchCapitalCheckTotal")
	public RetMsg<IfsCfetsrmbOr> searchCapitalCheckTotal(@RequestBody Map<String, Object> map) throws RemoteConnectFailureException, Exception {
		List<IfsCfetsrmbOr> list=iFSService.searchCapitalCheckList(map);
		List<IfsCfetsrmbOr> newList=new ArrayList<IfsCfetsrmbOr>();
		for (IfsCfetsrmbOr ifsCfetsrmbOr : list) {
			IfsCfetsrmbOr or=new IfsCfetsrmbOr();
			or=ifsCfetsrmbOr;
			Map<String, Object> opicsmap=new HashMap<String, Object>();
			opicsmap.put("dealno", ifsCfetsrmbOr.getDealNo());
			SlRprhDldtBean rprhDldtBean=spshServer.getRprhDldtBean(opicsmap);
			if(rprhDldtBean!=null){
				or.setComamount(rprhDldtBean.getComamt());
				or.setMatamount(rprhDldtBean.getMatamt());
				or.setComcheck(ifsCfetsrmbOr.getFirstSettlementAmount().subtract(rprhDldtBean.getComamt()));
				or.setMatcheck(ifsCfetsrmbOr.getSecondSettlementAmount().subtract(rprhDldtBean.getMatamt()));
			}else{
				or.setComamount(new BigDecimal(0));
				or.setMatamount(new BigDecimal(0));
				or.setComcheck(ifsCfetsrmbOr.getFirstSettlementAmount().subtract(new BigDecimal(0)));
				or.setMatcheck(ifsCfetsrmbOr.getSecondSettlementAmount().subtract(new BigDecimal(0)));
			}
			newList.add(or);
		}
		IfsCfetsrmbOr rmbOr=new IfsCfetsrmbOr();
		BigDecimal comcheck=new BigDecimal(0);
		BigDecimal matcheck=new BigDecimal(0);
		BigDecimal comamount=new BigDecimal(0);
		BigDecimal matamount=new BigDecimal(0);
		BigDecimal firstSettlementAmount=new BigDecimal(0);
		BigDecimal secondSettlementAmount=new BigDecimal(0);
		for (IfsCfetsrmbOr ifsCfetsrmbOr : newList) {
			comcheck=comcheck.add(ifsCfetsrmbOr.getComcheck());
			matcheck=matcheck.add(ifsCfetsrmbOr.getMatcheck());
			comamount=comamount.add(ifsCfetsrmbOr.getComamount());
			matamount=matamount.add(ifsCfetsrmbOr.getMatamount());
			firstSettlementAmount=firstSettlementAmount.add(ifsCfetsrmbOr.getFirstSettlementAmount());
			secondSettlementAmount=secondSettlementAmount.add(ifsCfetsrmbOr.getSecondSettlementAmount());
		}
		rmbOr.setComcheck(comcheck);
		rmbOr.setMatcheck(matcheck);
		rmbOr.setComamount(comamount);
		rmbOr.setMatamount(matamount);
		rmbOr.setFirstSettlementAmount(firstSettlementAmount);
		rmbOr.setSecondSettlementAmount(secondSettlementAmount);
		return RetMsgHelper.ok(rmbOr);
	}
	/**
	 * 回购统计报表
	 */
	@ResponseBody
	@RequestMapping(value = "/searchRepoReportList")
	public RetMsg<List<IfsRepoReportBean>> searchRepoReportList(@RequestBody Map<String, Object> map) {
		String year=(String)map.get("year");
		if(year==null|| "".equals(year)){
			String curDate=DayendDateServiceProxy.getInstance().getSettlementDate();
			year=curDate.substring(0, 4);
			map.put("year", year);
		}
		List<IfsRepoReportBean> list = iFSService.searchRepoReportList(map);
		List<IfsRepoReportBean> newList=new ArrayList<IfsRepoReportBean>();
		String[] arr={year+"01",year+"02",year+"03",year+"04",year+"05",year+"06",
				year+"07",year+"08",year+"09",year+"10",year+"11",year+"12"};
		for (String month : arr) {
			IfsRepoReportBean bean=new IfsRepoReportBean();
			bean.setYear(year);
			bean.setMonth(month);
			String sdatestr=month+"01";
			Date sdate=DateUtil.parse(sdatestr, "yyyyMMdd");
			Date edate=DateUtil.getEndDateTimeOfMonth(sdate);
			bean.setSdate(DateUtil.format(sdate));
			bean.setEdate(DateUtil.format(edate));
			bean.setPnum("0");
			bean.setPamt(new BigDecimal(0));
			bean.setPavg(new BigDecimal(0));
			bean.setSnum("0");
			bean.setSamt(new BigDecimal(0));
			bean.setSavg(new BigDecimal(0));
			for (IfsRepoReportBean ifsRepoReportBean : list) {
				if(ifsRepoReportBean.getMonth().equalsIgnoreCase(DateUtil.format(sdate).substring(0, 7))){
					bean.setPnum(ifsRepoReportBean.getPnum());
					bean.setPamt(ifsRepoReportBean.getPamt()==null?new BigDecimal(0):ifsRepoReportBean.getPamt().setScale(4, BigDecimal.ROUND_HALF_UP));
					bean.setPavg(ifsRepoReportBean.getPavg()==null?new BigDecimal(0):ifsRepoReportBean.getPavg().setScale(4, BigDecimal.ROUND_HALF_UP));
					bean.setSnum(ifsRepoReportBean.getSnum());
					bean.setSamt(ifsRepoReportBean.getSamt()==null?new BigDecimal(0):ifsRepoReportBean.getSamt().setScale(4, BigDecimal.ROUND_HALF_UP));
					bean.setSavg(ifsRepoReportBean.getSavg()==null?new BigDecimal(0):ifsRepoReportBean.getSavg().setScale(4, BigDecimal.ROUND_HALF_UP));
				}
			}
			newList.add(bean);
		}
		//合计
		IfsRepoReportBean totalBean=iFSService.searchRepoReportListTotal(map);
		IfsRepoReportBean bean=new IfsRepoReportBean();
		if(totalBean!=null){
			bean.setPnum(totalBean.getPnum());
			bean.setPamt(totalBean.getPamt()==null?new BigDecimal(0):totalBean.getPamt().setScale(4, BigDecimal.ROUND_HALF_UP));
			bean.setSnum(totalBean.getSnum());
			bean.setSamt(totalBean.getSamt()==null?new BigDecimal(0):totalBean.getSamt().setScale(4, BigDecimal.ROUND_HALF_UP));
		}else{
			bean.setPnum("0");
			bean.setPamt(new BigDecimal(0));
			bean.setSnum("0");
			bean.setSamt(new BigDecimal(0));
		}
		newList.add(bean);
		return RetMsgHelper.ok(newList);
	}
	
	
	@ResponseBody
	@RequestMapping(value = "/searchIboReportList")
	public RetMsg<List<IfsIboReportBean>> searchIboReportList(@RequestBody Map<String, Object> map) {
		String year=(String)map.get("year");
		if(year==null|| "".equals(year)){
			String curDate=DayendDateServiceProxy.getInstance().getSettlementDate();
			year=curDate.substring(0, 4);
			map.put("year", year);
		}
		List<IfsIboReportBean> list = iFSService.searchIboReportList(map);
		List<IfsIboReportBean> newList=new ArrayList<IfsIboReportBean>();
		String[] arr={year+"01",year+"02",year+"03",year+"04",year+"05",year+"06",
				year+"07",year+"08",year+"09",year+"10",year+"11",year+"12"};
		for (String month : arr) {
			IfsIboReportBean bean=new IfsIboReportBean();
			bean.setYear(year);
			bean.setMonth(month);
			String sdatestr=month+"01";
			Date sdate=DateUtil.parse(sdatestr, "yyyyMMdd");
			Date edate=DateUtil.getEndDateTimeOfMonth(sdate);
			bean.setSdate(DateUtil.format(sdate));
			bean.setEdate(DateUtil.format(edate));
			bean.setPnum("0");
			bean.setPamt(new BigDecimal(0));
			bean.setPavg(new BigDecimal(0));
			bean.setSnum("0");
			bean.setSamt(new BigDecimal(0));
			bean.setSavg(new BigDecimal(0));
			for (IfsIboReportBean ifsIboReportBean : list) {
				if(ifsIboReportBean.getMonth().equalsIgnoreCase(DateUtil.format(sdate).substring(0, 7))){
					bean.setPnum(ifsIboReportBean.getPnum());
					bean.setPamt(ifsIboReportBean.getPamt()==null?new BigDecimal(0):ifsIboReportBean.getPamt().setScale(4, BigDecimal.ROUND_HALF_UP));
					bean.setPavg(ifsIboReportBean.getPavg()==null?new BigDecimal(0):ifsIboReportBean.getPavg().setScale(4, BigDecimal.ROUND_HALF_UP));
					bean.setSnum(ifsIboReportBean.getSnum());
					bean.setSamt(ifsIboReportBean.getSamt()==null?new BigDecimal(0):ifsIboReportBean.getSamt().setScale(4, BigDecimal.ROUND_HALF_UP));
					bean.setSavg(ifsIboReportBean.getSavg()==null?new BigDecimal(0):ifsIboReportBean.getSavg().setScale(4, BigDecimal.ROUND_HALF_UP));
				}
			}
			newList.add(bean);
		}
		//合计
		IfsIboReportBean totalBean=iFSService.searchIboReportListTotal(map);
		IfsIboReportBean bean=new IfsIboReportBean();
		if(totalBean!=null){
			bean.setPnum(totalBean.getPnum());
			bean.setPamt(totalBean.getPamt()==null?new BigDecimal(0):totalBean.getPamt().setScale(4, BigDecimal.ROUND_HALF_UP));
			bean.setSnum(totalBean.getSnum());
			bean.setSamt(totalBean.getSamt()==null?new BigDecimal(0):totalBean.getSamt().setScale(4, BigDecimal.ROUND_HALF_UP));
		}else{
			bean.setPnum("0");
			bean.setPamt(new BigDecimal(0));
			bean.setSnum("0");
			bean.setSamt(new BigDecimal(0));
		}
		newList.add(bean);
		return RetMsgHelper.ok(newList);
	}
	
	@ResponseBody
	@RequestMapping(value = "/searchCbtReportList")
	public RetMsg<List<IfsCbtReportBean>> searchCbtReportList(@RequestBody Map<String, Object> map) {
		String year=(String)map.get("year");
		if(year==null|| "".equals(year)){
			String curDate=DayendDateServiceProxy.getInstance().getSettlementDate();
			year=curDate.substring(0, 4);
			map.put("year", year);
		}
		List<IfsCbtReportBean> list = iFSService.searchCbtReportList(map);
		List<IfsCbtReportBean> newList=new ArrayList<IfsCbtReportBean>();
		String[] arr={year+"01",year+"02",year+"03",year+"04",year+"05",year+"06",
				year+"07",year+"08",year+"09",year+"10",year+"11",year+"12"};
		for (String month : arr) {
			IfsCbtReportBean bean=new IfsCbtReportBean();
			bean.setYear(year);
			bean.setMonth(month);
			String sdatestr=month+"01";
			Date sdate=DateUtil.parse(sdatestr, "yyyyMMdd");
			Date edate=DateUtil.getEndDateTimeOfMonth(sdate);
			bean.setSdate(DateUtil.format(sdate));
			bean.setEdate(DateUtil.format(edate));
			bean.setPnum("0");
			bean.setPamt(new BigDecimal(0));
			bean.setPavg(new BigDecimal(0));
			bean.setSnum("0");
			bean.setSamt(new BigDecimal(0));
			bean.setSavg(new BigDecimal(0));
			bean.setTnum("0");
			bean.setTamt(new BigDecimal(0));
			for (IfsCbtReportBean ifsCbtReportBean : list) {
				if(ifsCbtReportBean.getMonth().equalsIgnoreCase(DateUtil.format(sdate).substring(0, 7))){
					bean.setPnum(ifsCbtReportBean.getPnum());
					bean.setPamt(ifsCbtReportBean.getPamt()==null?new BigDecimal(0):ifsCbtReportBean.getPamt().setScale(4, BigDecimal.ROUND_HALF_UP));
					//bean.setPavg(ifsCbtReportBean.getPavg()==null?new BigDecimal(0):ifsCbtReportBean.getPavg().setScale(4, BigDecimal.ROUND_HALF_UP));
					bean.setSnum(ifsCbtReportBean.getSnum());
					bean.setSamt(ifsCbtReportBean.getSamt()==null?new BigDecimal(0):ifsCbtReportBean.getSamt().setScale(4, BigDecimal.ROUND_HALF_UP));
					//bean.setSavg(ifsCbtReportBean.getSavg()==null?new BigDecimal(0):ifsCbtReportBean.getSavg().setScale(4, BigDecimal.ROUND_HALF_UP));
					bean.setTnum(ifsCbtReportBean.getTnum());
					bean.setTamt(ifsCbtReportBean.getTamt()==null?new BigDecimal(0):ifsCbtReportBean.getTamt().setScale(4, BigDecimal.ROUND_HALF_UP));
				}
			}
			newList.add(bean);
		}
		//合计
		IfsCbtReportBean totalBean=iFSService.searchCbtReportListTotal(map);
		IfsCbtReportBean bean=new IfsCbtReportBean();
		if(totalBean!=null){
			bean.setPnum(totalBean.getPnum());
			bean.setPamt(totalBean.getPamt()==null?new BigDecimal(0):totalBean.getPamt().setScale(4, BigDecimal.ROUND_HALF_UP));
			bean.setSnum(totalBean.getSnum());
			bean.setSamt(totalBean.getSamt()==null?new BigDecimal(0):totalBean.getSamt().setScale(4, BigDecimal.ROUND_HALF_UP));
			bean.setTnum(totalBean.getTnum());
			bean.setTamt(totalBean.getTamt()==null?new BigDecimal(0):totalBean.getTamt().setScale(4, BigDecimal.ROUND_HALF_UP));
		}else{
			bean.setPnum("0");
			bean.setPamt(new BigDecimal(0));
			bean.setSnum("0");
			bean.setSamt(new BigDecimal(0));
			bean.setTnum("0");
			bean.setTamt(new BigDecimal(0));
		}
		newList.add(bean);
		return RetMsgHelper.ok(newList);
	}
	
	
}
