package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.ifs.model.IfsAdvIdlv;
import com.singlee.ifs.service.IFSService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.Map;

@Controller
@RequestMapping(value = "/IfsAdvIdlvController")
public class IfsAdvIdlvController {
	
	@Autowired
	private IFSService iFSService;

	/***
	 * 分页查询
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageIdlv")
	public RetMsg<PageInfo<IfsAdvIdlv>> searchPageIdlv(@RequestBody Map<String, Object> map) {
		Page<IfsAdvIdlv> page = iFSService.searchPageIdlv(map);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 查询 拆借冲销 我发起的
	 */
	@ResponseBody
	@RequestMapping(value = "/searchIdlvMine")
	public RetMsg<PageInfo<IfsAdvIdlv>> searchIdlvMine(@RequestBody Map<String, Object> map) {
		map.put("isActive", DictConstants.YesNo.YES);
		map.put("userId", SlSessionHelper.getUserId());
		Page<IfsAdvIdlv> page = iFSService.searchIdlv(map, 3);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 查询 拆借冲销 待审批
	 * 
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchIdlvUnfinished")
	public RetMsg<PageInfo<IfsAdvIdlv>> searchIdlvUnfinished(@RequestBody Map<String, Object> map) {
		String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + ","
				+ DictConstants.ApproveStatus.VerifyRefuse + "," + DictConstants.ApproveStatus.WaitVerify + ","
				+ DictConstants.ApproveStatus.Verified;
		String[] orderStatus = approveStatus.split(",");
		map.put("approveStatusNo", orderStatus);
		map.put("isActive", DictConstants.YesNo.YES);
		map.put("userId", SlSessionHelper.getUserId());
		Page<IfsAdvIdlv> page = iFSService.searchIdlv(map, 1);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 查询 拆借冲销 已审批
	 * 
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchIdlvFinished")
	public RetMsg<PageInfo<IfsAdvIdlv>> searchIdlvFinished(@RequestBody Map<String, Object> map) {
		String approveStatus = ParameterUtil.getString(map, "approveStatus", "");
		if (StringUtil.isNullOrEmpty(approveStatus)) {
			approveStatus = DictConstants.ApproveStatus.ApprovedPass;
			approveStatus += "," + DictConstants.ApproveStatus.ApprovedNoPass + ","
					+ DictConstants.ApproveStatus.Cancle;
			approveStatus += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatus += "," + DictConstants.ApproveStatus.Verified;
			approveStatus += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			approveStatus += "," + DictConstants.ApproveStatus.Approving;

			String[] orderStatus = approveStatus.split(",");
			map.put("approveStatusNo", orderStatus);
		}
		map.put("isActive", DictConstants.YesNo.YES);
		map.put("userId", SlSessionHelper.getUserId());
		Page<IfsAdvIdlv> page = iFSService.searchIdlv(map, 2);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 删除
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteIdlv")
	public RetMsg<Serializable> deleteIdlv(@RequestBody Map<String, String> map) {
		String ticketId = map.get("ticketId");
		iFSService.deleteIdlvById(ticketId);
		return RetMsgHelper.ok();
	}

	/**
	 * 保存 拆借类要素
	 */
	@ResponseBody
	@RequestMapping(value = "/saveOrUpdateIdlv")
	public RetMsg<Serializable> saveOrUpdateIdlv(@RequestBody IfsAdvIdlv idlv) {
		String ticketId = idlv.getTicketId();
		if (null == ticketId || "".equals(ticketId)) {// 新增
			iFSService.insertIdlv(idlv);
		} else {// 修改
			iFSService.updateIdlvById(idlv);
		}
		return RetMsgHelper.ok();
	}

}
