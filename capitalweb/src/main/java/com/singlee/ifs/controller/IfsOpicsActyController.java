package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.bean.SlActyBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.opics.IStaticServer;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.SlErrors;
import com.singlee.ifs.model.IfsOpicsActy;
import com.singlee.ifs.service.IfsOpicsActyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * OPICS的会计类型
 * 
 * ClassName: IfsOpicsActyController <br/>
 * date: 2018-5-30 下午07:53:27 <br/>
 * 
 * @author zhengfl
 * @version
 * @since JDK 1.6
 */
@Controller
@RequestMapping(value = "/IfsOpicsActyController")
public class IfsOpicsActyController {

	@Autowired
	IfsOpicsActyService ifsOpicsActyService;

	@Autowired
	IStaticServer iStaticServer;


	/***
	 * 查询列表
	 */
	@ResponseBody
	@RequestMapping(value = "/actyList")
	public RetMsg<List<IfsOpicsActy>> actyList(@RequestBody Map<String, Object> map) {
		List<IfsOpicsActy> actyList = ifsOpicsActyService.actyList(map);
		return RetMsgHelper.ok(actyList);
	}

	/***
	 * 分页查询
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageActy")
	public RetMsg<PageInfo<IfsOpicsActy>> searchPageActy(@RequestBody Map<String, Object> map) {
		Page<IfsOpicsActy> page = ifsOpicsActyService.searchPageOpicsActy(map);
		return RetMsgHelper.ok(page);
	}

	/***
	 * 查询全部
	 */
	@ResponseBody
	@RequestMapping(value = "/searchAllOpicsActy")
	public Page<IfsOpicsActy> searchAllOpicsActy() {
		Page<IfsOpicsActy> actyList = ifsOpicsActyService.searchAllOpicsActy();
		return actyList;
	}
	
	/**
	 * 新增
	 */
	@ResponseBody
	@RequestMapping(value = "/saveOpicsActyAdd")
	public RetMsg<Serializable> saveOpicsActyAdd(@RequestBody IfsOpicsActy entity) {
		entity.setOperator(SlSessionHelper.getUserId());
		ifsOpicsActyService.insert(entity);
		return RetMsgHelper.ok();
	}

	/**
	 * 修改
	 */
	@ResponseBody
	@RequestMapping(value = "/saveOpicsActyEdit")
	public RetMsg<Serializable> saveOpicsActyEdit(@RequestBody IfsOpicsActy entity) {
		String acctngType = entity.getAcctngtype();
		IfsOpicsActy opicsActy = ifsOpicsActyService.searchById(acctngType);
		String status = opicsActy.getStatus();
		entity.setStatus(status);
		entity.setOperator(SlSessionHelper.getUserId());
		ifsOpicsActyService.updateById(entity);
		return RetMsgHelper.ok();
	}

	/**
	 * 删除
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteActy")
	public RetMsg<Serializable> deleteActy(@RequestBody Map<String, String> map) {
		String acctngtype = map.get("acctngtype");
		ifsOpicsActyService.deleteById(acctngtype);
		return RetMsgHelper.ok();
	}

	/**
	 * 根据id查询实体类
	 */
	@ResponseBody
	@RequestMapping(value = "/searchActyById")
	public RetMsg<IfsOpicsActy> searchActyById(@RequestBody Map<String, String> map) {
		String acctngtype = map.get("acctngtype");
		IfsOpicsActy ifsOpicsActy = ifsOpicsActyService.searchById(acctngtype);
		return RetMsgHelper.ok(ifsOpicsActy);
	}

	/**
	 * 同步至OPICS表
	 */
	@ResponseBody
	@RequestMapping(value = "/syncActy")
	public RetMsg<SlOutBean> syncActy(@RequestBody Map<String, String> map) {
		String acctngtype = map.get("acctngtype");
		IfsOpicsActy ifsOpicsActy = ifsOpicsActyService.searchById(acctngtype);
		SlActyBean actyBean = new SlActyBean();
		actyBean.setAcctDesc(ifsOpicsActy.getAcctdesc());
		actyBean.setAcctngType(ifsOpicsActy.getAcctngtype());
		actyBean.setLstmntdate(ifsOpicsActy.getLstmntdate());
		SlOutBean outBean = new SlOutBean();
		try {
			outBean = iStaticServer.addActy(actyBean);
		} catch (RemoteConnectFailureException e) {
			outBean.setRetCode(SlErrors.FAILED_RMI_OPICS.getErrCode());
			outBean.setRetMsg(SlErrors.FAILED_RMI_OPICS.getErrMsg() + ":" + e.getMessage());
			outBean.setRetStatus(RetStatusEnum.F);
		}catch (Exception e) {
			outBean.setRetCode(SlErrors.FAILED.getErrCode());
			outBean.setRetMsg(SlErrors.FAILED.getErrMsg() + ":" + e.getMessage());
			outBean.setRetStatus(RetStatusEnum.F);
		}
		if("S".equals(String.valueOf(outBean.getRetStatus()))){
			ifsOpicsActy.setStatus("1"); //设为已同步
			ifsOpicsActyService.updateStatus(ifsOpicsActy);
		}
		return RetMsgHelper.ok(outBean);
	}

	/**
	 * 批量校准信息
	 */
	@ResponseBody
	@RequestMapping(value = "/batchCalibration")
	public RetMsg<SlOutBean> verify(@RequestBody Map<String, String> map) {
		SlOutBean slOutBean = new SlOutBean();
		String type = String.valueOf(map.get("type"));
		String acctngtype = "";
		String[] acctngtypes;
		if ("1".equals(type)) { // 选中记录校准
			acctngtype = map.get("acctngtype");
			acctngtypes = acctngtype.split(",");
		} else { // 全部校准
			acctngtypes = null;
		}
		try{
			slOutBean = ifsOpicsActyService.batchCalibration(type, acctngtypes);
		} catch (Exception e){
			e.printStackTrace();
		}
		return RetMsgHelper.ok(slOutBean);
	}
}