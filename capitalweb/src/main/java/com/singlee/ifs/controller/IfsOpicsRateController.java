package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.opics.IStaticServer;
import com.singlee.ifs.model.IfsOpicsRate;
import com.singlee.ifs.service.IfsOpicsRateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/***
 * 利率代码
 * @author lij
 *
 */
@Controller
@RequestMapping(value = "/IfsOpicsRateController")
public class IfsOpicsRateController {
	@Autowired
	IfsOpicsRateService ifsOpicsRateService;
	
	@Autowired
	IStaticServer iStaticServer;
	
	
	/***
	 * 分页查询
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageRate")
	public RetMsg<PageInfo<IfsOpicsRate>> searchPageRate(@RequestBody Map<String, Object> map) {
		Page<IfsOpicsRate> page = ifsOpicsRateService.searchPageOpicsRate(map);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 批量校准
	 */
	@ResponseBody
	@RequestMapping(value = "/batchCalibration")
	public RetMsg<SlOutBean> batchCalibration(@RequestBody Map<String, String> map) {
		String type = String.valueOf(map.get("type"));
		String ratecode;
		String br;
		String[] ratecodes;
		String[] brs;
		if("1".equals(type)){//选中记录校准
			ratecode = map.get("ratecode");
			ratecodes = ratecode.split(",");
			br = map.get("br");
			brs = br.split(",");
			
		}else{//全部校准
			ratecodes=null;
			brs=null;
		}
		SlOutBean slOutBean = ifsOpicsRateService.batchCalibration(type,ratecodes,brs);
		return RetMsgHelper.ok(slOutBean);
	}

}
