package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.bean.SlSicoBean;
import com.singlee.financial.opics.IStaticServer;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.SlErrors;
import com.singlee.ifs.model.IfsOpicsSico;
import com.singlee.ifs.service.IfsOpicsSicoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/***
 * 工业标准码
 * 
 * @author lij
 * 
 */
@Controller
@RequestMapping(value = "/IfsOpicsSicoController")
public class IfsOpicsSicoController {
	@Autowired
	IfsOpicsSicoService ifsOpicsSicoService;

	@Autowired
	IStaticServer iStaticServer;

	/***
	 * 分页查询
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageSico")
	public RetMsg<PageInfo<IfsOpicsSico>> searchPageSico(@RequestBody Map<String, Object> map) {
		Page<IfsOpicsSico> page = ifsOpicsSicoService.searchPageOpicsSico(map);
		return RetMsgHelper.ok(page);
	}

	/***
	 * 根据id查询实体类
	 */
	@ResponseBody
	@RequestMapping(value = "/searchSicoById")
	public RetMsg<IfsOpicsSico> searchSicoById(@RequestBody Map<String, String> map) {
		String sic = map.get("sic");
		IfsOpicsSico ifsOpicsSico = ifsOpicsSicoService.searchById(sic);
		return RetMsgHelper.ok(ifsOpicsSico);
	}

	/**
	 * 新增
	 */
	@ResponseBody
	@RequestMapping(value = "/saveOpicsSicoAdd")
	public RetMsg<Serializable> saveOpicsSicoAdd(@RequestBody IfsOpicsSico entity) {
		ifsOpicsSicoService.insert(entity);
		return RetMsgHelper.ok();
	}

	/**
	 * 修改
	 */
	@ResponseBody
	@RequestMapping(value = "/saveOpicsSicoEdit")
	public RetMsg<Serializable> saveOpicsSicoEdit(@RequestBody IfsOpicsSico entity) {
		ifsOpicsSicoService.updateById(entity);
		return RetMsgHelper.ok();
	}

	/**
	 * 删除
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteSico")
	public RetMsg<Serializable> deleteSico(@RequestBody Map<String, String> map) {
		String sic = map.get("sic");
		ifsOpicsSicoService.deleteById(sic);
		return RetMsgHelper.ok();
	}

	/**
	 * 同步opics表
	 */
	@ResponseBody
	@RequestMapping(value = "/syncSico")
	public RetMsg<SlOutBean> syncSico(@RequestBody Map<String, String> map) {
		SlOutBean slOutBean = new SlOutBean();

		String sic = map.get("sic");
		String[] sics = sic.split(",");
		for (int i = 0; i < sics.length; i++) {
			IfsOpicsSico ifsOpicsSico = ifsOpicsSicoService.searchById(sics[i]);
			SlSicoBean sicoBean = new SlSicoBean();
			sicoBean.setSic(sics[i]);
			sicoBean.setSd(ifsOpicsSico.getSd());
			sicoBean.setLstmntdte(ifsOpicsSico.getLstmntdte());
			try {
				slOutBean = iStaticServer.addSico(sicoBean);
			} catch (RemoteConnectFailureException e) {
				slOutBean.setRetCode(SlErrors.FAILED_RMI_OPICS.getErrCode());
				slOutBean.setRetMsg(SlErrors.FAILED_RMI_OPICS.getErrMsg() + ":" + e.getMessage());
				slOutBean.setRetStatus(RetStatusEnum.F);
			} catch (Exception e) {
				slOutBean.setRetCode(SlErrors.FAILED.getErrCode());
				slOutBean.setRetMsg(SlErrors.FAILED.getErrMsg() + ":" + e.getMessage());
				slOutBean.setRetStatus(RetStatusEnum.F);
			}
			if ("S".equals(String.valueOf(slOutBean.getRetStatus()))) {
				ifsOpicsSico.setStatus("1");
				ifsOpicsSicoService.updateStatus(ifsOpicsSico);
			}

		}

		return RetMsgHelper.ok(slOutBean);
	}

	/**
	 * 批量校准
	 */
	@ResponseBody
	@RequestMapping(value = "/batchCalibration")
	public RetMsg<SlOutBean> batchCalibration(@RequestBody Map<String, String> map) {
		String type = String.valueOf(map.get("type"));
		String sic;
		String[] sics;
		if ("1".equals(type)) {// 选中记录校准
			sic = map.get("sic");
			sics = sic.split(",");
		} else {// 全部校准
			sics = null;
		}
		SlOutBean slOutBean = ifsOpicsSicoService.batchCalibration(type, sics);
		return RetMsgHelper.ok(slOutBean);
	}

	/***
	 * 查询全部工业标准代码
	 */
	@ResponseBody
	@RequestMapping(value = "/searchAllOpicsSico")
	public List<IfsOpicsSico> searchAllOpicsSico() {
		List<IfsOpicsSico> result = ifsOpicsSicoService.searchAllOpicsSico();
		return result;
	}

	/***
	 * 查询全部工业标准代码(下拉框)
	 */
	@ResponseBody
	@RequestMapping(value = "/searchAllOpicsSicoDict")
	public List<IfsOpicsSico> searchAllOpicsSicoDict() {
		List<IfsOpicsSico> result = ifsOpicsSicoService.searchAllOpicsSicoDict();
		return result;
	}

}
