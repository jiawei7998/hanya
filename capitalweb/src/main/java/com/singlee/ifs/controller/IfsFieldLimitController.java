package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.ifs.model.IfsLimitColumns;
import com.singlee.ifs.model.IfsOpicsProd;
import com.singlee.ifs.service.IfsFieldLimitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 产品字段限额
 * 
 * ClassName: IfsFieldLimitController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason: TODO ADD REASON(可选). <br/>
 * date: 2018-5-30 下午07:32:00 <br/>
 * 
 * @author lijing
 * @version
 * @since JDK 1.6
 */
@Controller
@RequestMapping(value = "/IfsFieldLimitController")
public class IfsFieldLimitController{
	@Autowired
	IfsFieldLimitService ifsFieldLimitService;

	/***
	 * 分页查询 产品字段限额
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageFieldLimit")
	public RetMsg<PageInfo<IfsLimitColumns>> searchPageFieldLimit(@RequestBody Map<String, Object> map) {
		Page<IfsLimitColumns> page = ifsFieldLimitService.searchPageFieldLimit(map);
		return RetMsgHelper.ok(page);
	}

	/***
	 * 删除 产品字段限额
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteFieldLimit")
	public RetMsg<Serializable> deleteFieldLimit(@RequestBody Map<String, String> map) {
		String limitId = map.get("limitId");
		ifsFieldLimitService.deleteById(limitId);
		return RetMsgHelper.ok();
	}

	/**
	 * 保存 产品字段限额 新增
	 */
	@ResponseBody
	@RequestMapping(value = "/saveFieldLimitAdd")
	public RetMsg<Serializable> saveFieldLimitAdd(@RequestBody Map<String, Object> map) {
		String limitId = "NO" + DateUtil.getCurrentCompactDateTimeAsString() + Integer.toString((int) ((Math.random() * 9 + 1) * 100));
		map.put("limitId", limitId);
		ifsFieldLimitService.insert(map);
		return RetMsgHelper.ok();
	}

	/**
	 * 保存 产品字段限额 修改
	 */
	@ResponseBody
	@RequestMapping(value = "/saveFieldLimitEdit")
	public RetMsg<Serializable> saveFieldLimitEdit(@RequestBody Map<String, Object> map) {
		ifsFieldLimitService.updateById(map);
		return RetMsgHelper.ok();
	}

	/***
	 * 查询 产品表的结构
	 */
	@ResponseBody
	@RequestMapping(value = "/searchColumns")
	public RetMsg<List<Map<String, Object>>> searchColumns(@RequestBody Map<String, Object> map) {
		List<Map<String, Object>> list = ifsFieldLimitService.searchColumns(map);
		return RetMsgHelper.ok(list);
	}

	/***
	 * 查询 产品表
	 */
	@ResponseBody
	@RequestMapping(value = "/searchTables")
	public RetMsg<List<Map<String, Object>>> searchTables(@RequestBody Map<String, Object> map) {
		List<Map<String, Object>> list = ifsFieldLimitService.searchTables(map);
		return RetMsgHelper.ok(list);
	}

	/***
	 * 查询 字典表
	 */
	@ResponseBody
	@RequestMapping(value = "/searchDict")
	public RetMsg<List<Map<String, Object>>> searchDict(@RequestBody Map<String, Object> map) {
		List<Map<String, Object>> list = ifsFieldLimitService.searchDict(map);
		return RetMsgHelper.ok(list);
	}

	/***
	 * 根据产品类型 查询限额字段以及字典值（对返回值进行拼接，拼接成限额条件）
	 */
	@ResponseBody
	@RequestMapping(value = "/searchLimitFieldAndDict")
	public RetMsg<Serializable> searchLimitFieldAndDict(@RequestBody Map<String, Object> map) {
		String str = ifsFieldLimitService.searchLimitFieldAndDict(map);
		return RetMsgHelper.ok(str);
	}

	/***
	 * 根据产品类型 查询出所有
	 */
	@ResponseBody
	@RequestMapping(value = "/searchAllByPrd")
	public RetMsg<List<Map<String, Object>>> searchAllByPrd(@RequestBody Map<String, Object> map) {
		List<Map<String, Object>> list = ifsFieldLimitService.searchAllByPrd(map);
		return RetMsgHelper.ok(list);
	}

	// 根据 产品 限额字段 字典值 select
	@ResponseBody
	@RequestMapping(value = "/proColuDic")
	public RetMsg<List<Map<String, Object>>> proColuDic(@RequestBody Map<String, Object> map) {
		List<Map<String, Object>> list = ifsFieldLimitService.proColuDic(map);
		return RetMsgHelper.ok(list);

	}

	// 根据限额字段去查询数据
	@ResponseBody
	@RequestMapping(value = "/searchColum")
	public RetMsg<List<Map<String, Object>>> searchColum(@RequestBody Map<String, Object> map) {
		List<Map<String, Object>> list = ifsFieldLimitService.searchColum(map);
		return RetMsgHelper.ok(list);

	}

	// 限额条件 根据产品 查询出所有的 限额字段(colum)

	@RequestMapping(value = "/selectColum")
	public RetMsg<List<Map<String, Object>>> selectColum(@RequestBody Map<String, String> map) {
		List<Map<String, Object>> list = ifsFieldLimitService.selectColum(map);
		return RetMsgHelper.ok(list);
	}
	
	//查询所有的产品信息
	@ResponseBody
	@RequestMapping(value = "/searchPrdInfo")
	public RetMsg<PageInfo<IfsOpicsProd>> searchPrdInfo(@RequestBody Map<String, Object> map) {
		Page<IfsOpicsProd> page = ifsFieldLimitService.searchPrdInfo(map);
		return RetMsgHelper.ok(page);
	}
	
	//查询所有的产品类型信息
	@ResponseBody
	@RequestMapping(value = "/searchTypeInfo")
	public RetMsg<PageInfo<Map<String, Object>>> searchTypeInfo(@RequestBody Map<String, Object> map) {
		Page<Map<String, Object>> page = ifsFieldLimitService.searchTypeInfo(map);
		return RetMsgHelper.ok(page);
	}
	
	//查询所有的币种信息
	@ResponseBody
	@RequestMapping(value = "/searchCcyInfo")
	public RetMsg<PageInfo<Map<String, Object>>> searchCcyInfo(@RequestBody Map<String, Object> map) {
		Page<Map<String, Object>> page = ifsFieldLimitService.searchCcyInfo(map);
		return RetMsgHelper.ok(page);
	}

	//查询债券类型
	@ResponseBody
	@RequestMapping(value = "/searchBondInfo")
	public RetMsg<PageInfo<Map<String, Object>>> searchBondInfo(@RequestBody Map<String, Object> map) {
		Page<Map<String, Object>> page = ifsFieldLimitService.searchBondInfo(map);
		return RetMsgHelper.ok(page);
	}
	
	//查询所有账户类型信息
	@ResponseBody
	@RequestMapping(value = "/searchAccTypeInfo")
	public RetMsg<PageInfo<Map<String, Object>>> searchAccTypeInfo(@RequestBody Map<String, Object> map) {
		Page<Map<String, Object>> page = ifsFieldLimitService.searchAccTypeInfo(map);
		return RetMsgHelper.ok(page);
	}
	
	//查询所有用户名
	@ResponseBody
	@RequestMapping(value = "/searchTradInfo")
	public RetMsg<PageInfo<Map<String, Object>>> searchTradInfo(@RequestBody Map<String, Object> map) {
		Page<Map<String, Object>> page = ifsFieldLimitService.searchTradInfo(map);
		return RetMsgHelper.ok(page);
	}
	
	//查询所有用户名
	@ResponseBody
	@RequestMapping(value = "/searchCostInfo")
	public RetMsg<PageInfo<Map<String, Object>>> searchCostInfo(@RequestBody Map<String, Object> map) {
		Page<Map<String, Object>> page = ifsFieldLimitService.searchCostInfo(map);
		return RetMsgHelper.ok(page);
	}
	
}
