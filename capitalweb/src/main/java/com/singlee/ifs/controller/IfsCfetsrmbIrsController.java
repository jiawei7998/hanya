package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.ifs.model.IfsCfetsrmbIrs;
import com.singlee.ifs.model.IfsCfetsrmbIrsKey;
import com.singlee.ifs.service.IFSService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.io.Serializable;
import java.util.Map;

/**
 * 利率互换正式交易审批Controller
 * 
 * ClassName: IfsCfetsrmbIrsController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason: TODO ADD REASON(可选). <br/>
 * date: 2018-5-30 下午07:27:45 <br/>
 * 
 * @author lijing
 * @version
 * @since JDK 1.6
 */
@Controller
@RequestMapping(value = "/IfsCfetsrmbIrsController")
public class IfsCfetsrmbIrsController extends CommonController {
	@Autowired
	private IFSService cfetsrmbIrsService;

	/**
	 * 保存成交单
	 * 
	 * @param IfsCfetsrmbIrs
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/saveIrs")
	public RetMsg<Serializable> saveIrs(@RequestBody IfsCfetsrmbIrs record) throws IOException {
		IfsCfetsrmbIrs irs = cfetsrmbIrsService.getIrsById(record);
		String message  = cfetsrmbIrsService.checkCust(record.getDealTransType(),record.getFloatInst(),record.getCfetsno(), null, record.getProduct(), record.getProdType());
		if(StringUtil.isNotEmpty(message)) {
			return RetMsgHelper.fail(message);
		}
		
		if (irs == null) {
			message = cfetsrmbIrsService.insertIrs(record);
		} else {
			try {
				cfetsrmbIrsService.updateIrs(record);
				message = "修改成功";
			} catch (Exception e) {
				e.printStackTrace();
				message = "修改失败";
			}
		}
		return StringUtil.containsIgnoreCase(message, "成功")?RetMsgHelper.ok(message):RetMsgHelper.fail(message);
	}

	/**
	 * 成交单分页查询
	 * 
	 * @param IfsCfetsrmbIrs
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchCfetsrmbIrs")
	public RetMsg<PageInfo<IfsCfetsrmbIrs>> searchCfetsrmbIrs(@RequestBody IfsCfetsrmbIrs record) {
		Page<IfsCfetsrmbIrs> page = cfetsrmbIrsService.getIrsList(record);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 根据主键查询成交单
	 * 
	 * @param IfsCfetsrmbIrsKey
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchCfetsrmbIrsById")
	public RetMsg<IfsCfetsrmbIrs> searchCfetsrmbIrsById(@RequestBody IfsCfetsrmbIrsKey key) {
		IfsCfetsrmbIrs irs = cfetsrmbIrsService.getIrsById(key);
		return RetMsgHelper.ok(irs);
	}

	/**
	 * 根据主键查询成交单(返回值增加 流程id)
	 * 
	 * @param IfsCfetsrmbIrsKey
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchCfetsrmbIrsAndFlowIdById")
	public RetMsg<IfsCfetsrmbIrs> searchCfetsrmbIrsAndFlowIdById(@RequestBody Map<String, Object> key) {
		IfsCfetsrmbIrs irs = cfetsrmbIrsService.getIrsAndFlowIdById(key);
		return RetMsgHelper.ok(irs);
	}

	/**
	 * 删除成交单
	 * 
	 * @param IfsCfetsrmbIrsKey
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/deleteCfetsrmbIrs")
	public RetMsg<Serializable> deleteCfetsrmbIrs(@RequestBody IfsCfetsrmbIrsKey key) {
		cfetsrmbIrsService.deleteIrs(key);
		return RetMsgHelper.ok();
	}

	/**
	 * 查询我发起的
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-21
	 */
	@ResponseBody
	@RequestMapping(value = "/searchRmbIrsSwapMine")
	public RetMsg<PageInfo<IfsCfetsrmbIrs>> searchRmbIrsSwapMine(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsrmbIrs> page = cfetsrmbIrsService.getRmbIrsMinePage(params, 3);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 查询 利率互换 待审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageRmbIrsUnfinished")
	public RetMsg<PageInfo<IfsCfetsrmbIrs>> searchPageRmbIrsUnfinished(@RequestBody Map<String, Object> params) {
		String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse + ","
				+ DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
		String[] orderStatus = approveStatus.split(",");
		params.put("approveStatusNo", orderStatus);
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsrmbIrs> page = cfetsrmbIrsService.getRmbIrsMinePage(params, 1);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 查询 已审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageRmbIrsFinished")
	public RetMsg<PageInfo<IfsCfetsrmbIrs>> searchPageRmbIrsFinished(@RequestBody Map<String, Object> params) {
		String approveStatus = ParameterUtil.getString(params, "approveStatus", "");
		if (StringUtil.isNullOrEmpty(approveStatus)) {
			approveStatus = DictConstants.ApproveStatus.ApprovedPass;
			approveStatus += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatus += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatus += "," + DictConstants.ApproveStatus.Verified;
			approveStatus += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			// 20180502修改bug:已审批列表中增加"审批中"
			approveStatus += "," + DictConstants.ApproveStatus.Approving;

			String[] orderStatus = approveStatus.split(",");
			params.put("approveStatusNo", orderStatus);
		}
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsrmbIrs> page = cfetsrmbIrsService.getRmbIrsMinePage(params, 2);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 勾兑时，查询表中审批通过的数据
	 * 
	 * @param
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchPageApprovermbIrsPassed")
	public RetMsg<PageInfo<IfsCfetsrmbIrs>> searchPageApprovermbIrsPassed(@RequestBody Map<String, Object> params) {
		Page<IfsCfetsrmbIrs> page = cfetsrmbIrsService.getApprovePassedPage(params);
		return RetMsgHelper.ok(page);
	}
	
	
	/**
	 * 存续管理台  互换类 mini弹框
	 * 
	 * @param params
	 * @return
	 * @author rongliu
	 * @date 2018-08-17
	 */
	@ResponseBody
	@RequestMapping(value = "/searchIswhMiniPage")
	public RetMsg<PageInfo<Map<String, Object>>> searchIswhMiniPage(@RequestBody Map<String, Object> params) {
		Page<Map<String, Object>> page = cfetsrmbIrsService.getIswhMini(params);
		return RetMsgHelper.ok(page);
	}
	
	
	// 根据id查询利率互换元素
	@ResponseBody
	@RequestMapping(value = "/searchIrsByTicketId")
	public RetMsg<IfsCfetsrmbIrs> searchIrsByTicketId(@RequestBody Map<String, Object> map) {
		String ticketId = map.get("ticketId").toString();
		IfsCfetsrmbIrs ifsCfetsrmbIrs = cfetsrmbIrsService.searchIrsByTicketId(ticketId);
		return RetMsgHelper.ok(ifsCfetsrmbIrs);
	}
	
	//根据ticketId查询详情
	@ResponseBody
	@RequestMapping(value = "/searchIrsDetailByTicketId")
	public RetMsg<IfsCfetsrmbIrs> searchIrsDetailByTicketId(@RequestBody Map<String, Object> map) {
		IfsCfetsrmbIrs irs = cfetsrmbIrsService.searchIrsDetailByTicketId(map);
		return RetMsgHelper.ok(irs);
	}
}
