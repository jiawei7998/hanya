package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.bean.SlSetaBean;
import com.singlee.financial.opics.IStaticServer;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.SlErrors;
import com.singlee.ifs.model.IfsOpicsSeta;
import com.singlee.ifs.service.IfsOpicsSetaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.Map;

/***
 * SETA
 * 
 * @author yanming
 * 
 */

@Controller
@RequestMapping(value = "/IfsOpicsSetaController")
public class IfsOpicsSetaController {

	@Autowired
	IfsOpicsSetaService ifsOpicsSetaService;
	@Autowired
	IStaticServer iStaticServer;

	/***
	 * 分页查询
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageSeta")
	public RetMsg<PageInfo<IfsOpicsSeta>> searchPageSeta(@RequestBody Map<String, Object> map) {
		Page<IfsOpicsSeta> page = ifsOpicsSetaService.searchPageOpicsSeta(map);
		return RetMsgHelper.ok(page);
	}

	/***
	 * 分页查询
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageSetaMini")
	public RetMsg<PageInfo<IfsOpicsSeta>> searchPageSetaMini(@RequestBody Map<String, Object> map) {
		Page<IfsOpicsSeta> page = ifsOpicsSetaService.searchPageOpicsSetaMini(map);
		return RetMsgHelper.ok(page);
	}

	/***
	 * 根据id查询实体类
	 */
	@ResponseBody
	@RequestMapping(value = "/searchSetaById")
	public RetMsg<IfsOpicsSeta> searchSetaById(@RequestBody Map<String, String> map) {
		IfsOpicsSeta ifsOpicsSeta = ifsOpicsSetaService.searchById(map);
		return RetMsgHelper.ok(ifsOpicsSeta);
	}

	/**
	 * 新增
	 */
	@ResponseBody
	@RequestMapping(value = "/saveOpicsSetaAdd")
	public RetMsg<Serializable> saveOpicsSetaAdd(@RequestBody IfsOpicsSeta entity) {
		ifsOpicsSetaService.insert(entity);
		return RetMsgHelper.ok();
	}

	/**
	 * 修改
	 */
	@ResponseBody
	@RequestMapping(value = "/saveOpicsSetaEdit")
	public RetMsg<Serializable> saveOpicsSetaEdit(@RequestBody IfsOpicsSeta entity) {
		ifsOpicsSetaService.updateById(entity);
		return RetMsgHelper.ok();
	}

	/**
	 * 删除
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteSeta")
	public RetMsg<Serializable> deleteSeta(@RequestBody Map<String, String> map) {
		String smeans = map.get("smeans");
		ifsOpicsSetaService.deleteById(smeans);
		return RetMsgHelper.ok();
	}

	/***
	 * 同步SETA表
	 */
	@ResponseBody
	@RequestMapping(value = "/syncSeta")
	public RetMsg<SlOutBean> syncSeta(@RequestBody Map<String, String> map) {
		SlOutBean slOutBean = new SlOutBean();

		String smeans = map.get("smeans");
		String[] smeanss = smeans.split(",");
		for (int i = 0; i < smeanss.length; i++) {
			IfsOpicsSeta ifsOpicsSeta = ifsOpicsSetaService.searchById(map);
			SlSetaBean setaBean = new SlSetaBean();
			setaBean.setSmeans(smeanss[i]);
			setaBean.setBr(ifsOpicsSeta.getBr());
			setaBean.setSacct(ifsOpicsSeta.getSacct());
			setaBean.setCcy(ifsOpicsSeta.getCcy());
			setaBean.setLstmntdte(ifsOpicsSeta.getLstmntdte());
			try {
				slOutBean = iStaticServer.addSeta(setaBean);
			} catch (RemoteConnectFailureException e) {
				slOutBean.setRetCode(SlErrors.FAILED_RMI_OPICS.getErrCode());
				slOutBean.setRetMsg(SlErrors.FAILED_RMI_OPICS.getErrMsg() + ":" + e.getMessage());
				slOutBean.setRetStatus(RetStatusEnum.F);
			} catch (Exception e) {
				slOutBean.setRetCode(SlErrors.FAILED.getErrCode());
				slOutBean.setRetMsg(SlErrors.FAILED.getErrMsg() + ":" + e.getMessage());
				slOutBean.setRetStatus(RetStatusEnum.F);
			}
			if ("S".equals(String.valueOf(slOutBean.getRetStatus()))) {
				ifsOpicsSeta.setStatus("1");
				ifsOpicsSetaService.updateStatus(ifsOpicsSeta);
			}
		}
		return RetMsgHelper.ok(slOutBean);
	}

	/**
	 * 批量校准
	 */
	@ResponseBody
	@RequestMapping(value = "/batchCalibration")
	public RetMsg<SlOutBean> batchCalibration(@RequestBody Map<String, String> map) {
		String type = String.valueOf(map.get("type"));
		String smeans;
		String sacct;
		String[] smeanss;
		String[] saccts;
		if ("1".equals(type)) {
			smeans = map.get("smeans");
			sacct = map.get("sacct");
			smeanss = smeans.split(",");
			saccts = sacct.split(",");
		} else {
			smeanss = null;
			saccts = null;
		}
		SlOutBean slOutBean = ifsOpicsSetaService.batchCalibration(type, smeanss, saccts);
		return RetMsgHelper.ok(slOutBean);

	}
}