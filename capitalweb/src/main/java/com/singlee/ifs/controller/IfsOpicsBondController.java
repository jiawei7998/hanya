package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.base.util.FileManage;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.UploadedFile;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.dict.model.TaDictVo;
import com.singlee.capital.dict.service.DictionaryGetService;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.bean.SlDealModule;
import com.singlee.financial.bean.SlInthBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.bean.SlSecmBean;
import com.singlee.financial.opics.IAcupServer;
import com.singlee.financial.opics.IExternalServer;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.ifs.mapper.IfsWdOpicsSecmapMapper;
import com.singlee.ifs.mapper.IfsWindSecLevelMapper;
import com.singlee.ifs.model.IfsBondCheckLog;
import com.singlee.ifs.model.IfsOpicsBond;
import com.singlee.ifs.model.IfsWdOpicsSecmap;
import com.singlee.ifs.model.IfsWindSecLevelBean;
import com.singlee.ifs.service.IfsBondCheckLogService;
import com.singlee.ifs.service.IfsBondPledgeService;
import com.singlee.ifs.service.IfsOpicsBondService;
import jxl.Sheet;
import jxl.Workbook;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * OPICS债券基本信息导入
 * <p>
 * ClassName: IfsOpicsBondController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason: TODO ADD REASON(可选). <br/>
 * date: 2018-5-30 下午07:37:55 <br/>
 *
 * @author zhangcm
 * @since JDK 1.6
 */
@Controller
@RequestMapping(value = "/IfsOpicsBondController")
public class IfsOpicsBondController {
    @Autowired
    IfsOpicsBondService ifsOpicsBondService;
    @Autowired
    IfsBondCheckLogService ifsBondCheckLogService;
    @Autowired
    IExternalServer externalServer;
    @Autowired
    IAcupServer acupServer;
    @Autowired
    DictionaryGetService dictionaryGetService;
    @Autowired
	private IfsBondPledgeService ifsBondPledgeService;

    @Autowired
    IfsWdOpicsSecmapMapper wdOpicsSecmapMapper;
    @Autowired
    IfsWindSecLevelMapper ifsWindSecLevelMapper;


    /***
     * 分页债券补录信息查询 交易要素
     */
    @ResponseBody
    @RequestMapping(value = "/searchPageBondTrd")
    public RetMsg<PageInfo<IfsOpicsBond>> searchPageBondTrd(@RequestBody Map<String, Object> map) {
        Page<IfsOpicsBond> page = ifsOpicsBondService.searchPageBondTrd(map);
        return RetMsgHelper.ok(page);
    }

    /***
     * 分页债券复核查询
     */
    @ResponseBody
    @RequestMapping(value = "/searchPageCheckTrd")
    public RetMsg<PageInfo<IfsOpicsBond>> searchPageCheckTrd(@RequestBody Map<String, Object> map) {
        Page<IfsOpicsBond> page = ifsOpicsBondService.searchPageCheckTrd(map);
        return RetMsgHelper.ok(page);
    }

    /**
     * 根据主键查询成交单(返回值增加 流程id)
     */
    @ResponseBody
    @RequestMapping(value = "/searchOneById")
    public IfsOpicsBond searchOneById(@RequestBody Map<String, Object> key) {
        String bndcd = key.get("bndcd").toString();
        IfsOpicsBond ifsOpicsBond = ifsOpicsBondService.searchOneById(bndcd);
        return ifsOpicsBond;
    }

    /***
     * 删除
     */
    @ResponseBody
    @RequestMapping(value = "/delete")
    public RetMsg<Serializable> delete(@RequestBody Map<String, String> map) {
        String bndcd = map.get("bndcd");
        ifsOpicsBondService.deleteById(bndcd);
        return RetMsgHelper.ok();
    }

    @ResponseBody
    @RequestMapping(value = "/isExist")
    public RetMsg<IfsOpicsBond> isExist(@RequestBody Map<String, String> map) {
        String bndcd = map.get("bndcd");
        SlSecmBean secmBean = null;
        try {
            secmBean = externalServer.getSemmById(bndcd);
        } catch (RemoteConnectFailureException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        IfsOpicsBond opicsBond = new IfsOpicsBond();
        if (secmBean != null) {
            // 发行日期
            Date issdate = secmBean.getIssdate();
            opicsBond.setPubdt(DateUtil.format(issdate, "yyyy-MM-dd"));
            // 起息日
            Date vdate = secmBean.getVdate();
            opicsBond.setIssudt(DateUtil.format(vdate, "yyyy-MM-dd"));
            // 到期日
            Date mdate = secmBean.getMdate();
            opicsBond.setMatdt(DateUtil.format(mdate, "yyyy-MM-dd"));
            opicsBond.setDescr(secmBean.getDescr());
            opicsBond.setProduct(secmBean.getProduct());
            opicsBond.setProdtype(secmBean.getProdtype());
            opicsBond.setAccountno(secmBean.getSecsacct());
            opicsBond.setAcctngtype(secmBean.getAcctngtype());
            opicsBond.setBasis(secmBean.getBasis());
            opicsBond.setCcy(secmBean.getCcy());
            opicsBond.setSettccy(secmBean.getSettccy());
            opicsBond.setSecunit(secmBean.getSecunit());
            opicsBond.setDenom(secmBean.getDenom());
            opicsBond.setIntcalcrule(secmBean.getIntcalcrule());
            opicsBond.setIssuer(secmBean.getIssuer());
            opicsBond.setCouponrate(secmBean.getCouprate());
            opicsBond.setIntpaymethod(secmBean.getIntpayrule());
            opicsBond.setIntenddaterule(secmBean.getIntenddaterule());
            opicsBond.setIntpaycircle(secmBean.getIntpaycycle());
            opicsBond.setBndcd(secmBean.getSecid());
        }

        return RetMsgHelper.ok(opicsBond);
    }

    /***
     * 查询债券上次付息日
     */
    @ResponseBody
    @RequestMapping(value = "/queryBondPayDate")
    public String queryBondPayDate(@RequestBody Map<String, Object> map) {
        Date date = acupServer.queryDate(map);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            String dateStr = sdf.format(date);
            return dateStr;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    /**
     * 保存 和修改
     */
    @ResponseBody
    @RequestMapping(value = "/save")
    public String save(@RequestBody IfsOpicsBond entity) {
        /*
         * String id = DateUtil.getCurrentCompactDateTimeAsString() + Integer.toString((int) ((Math.random() * 9 + 1) * 100)); IfsBondCheckLog checkLog = new IfsBondCheckLog();
         * checkLog.setBndcd(entity.getBndcd()); checkLog.setId(id); checkLog.setInputTime(DateUtil.getCurrentDateTimeAsString());
         * checkLog.setOperatInst(SlSessionHelper.getInstitution().getInstName()); checkLog.setOperatUser(SlSessionHelper.getUserId());
         */
        String bancd = entity.getBndcd();
        IfsOpicsBond ifsOpicsBond = ifsOpicsBondService.searchOneById(bancd);
        String message = "";
        entity.setBr("01");//不管那个过来的数据都默认01
        if (null == ifsOpicsBond) {// 新增
            /* checkLog.setStatus(entity.getStatus()); */
            message = ifsOpicsBondService.insert(entity);
            /*
             * if ("保存成功".equals(message)) { ifsBondCheckLogService.insert(checkLog); }
             */
        } else {// 修改
            ifsOpicsBondService.updateById(entity);
            /*
             * checkLog.setStatus("E"); ifsBondCheckLogService.insert(checkLog);
             */
            message = "信息修改成功";
        }
        return message;
    }

    /**
     * 复核
     */
    @ResponseBody
    @RequestMapping(value = "/check")
    public RetMsg<Serializable> check(@RequestBody IfsOpicsBond entity) {
        RetMsg<Serializable> ret = new RetMsg<Serializable>(RetMsgHelper.codeOk, "复核成功");
        try {
            String id = DateUtil.getCurrentCompactDateTimeAsString() + (int) ((Math.random() * 9 + 1) * 100);
            IfsBondCheckLog checkLog = new IfsBondCheckLog();
            checkLog.setBndcd(entity.getBndcd());
            checkLog.setDealno(entity.getDealno());
            checkLog.setId(id);
            checkLog.setInputTime(DateUtil.getCurrentDateTimeAsString());
            checkLog.setOperatInst(SlSessionHelper.getInstitution().getInstName());
            checkLog.setOperatUser(SlSessionHelper.getUserId());
            checkLog.setStatus(entity.getStatus());

            SlSecmBean secmBean = new SlSecmBean("01", SlDealModule.SECUR.SERVER, SlDealModule.SECUR.TAG, SlDealModule.SECUR.DETAIL);
            secmBean.setSecid(entity.getBndcd());
            secmBean.setProduct(entity.getProduct());
            secmBean.setProdtype(entity.getProdtype());
            secmBean.setCcy(entity.getCcy());
            secmBean.setSectype("");
            secmBean.setTenor("99");
            secmBean.setAcctngtype(entity.getAcctngtype());
            secmBean.setSecunit(entity.getSecunit());
            secmBean.setDenom(entity.getDenom());
            secmBean.setIssdate(DateUtil.parse(entity.getPubdt(), "yyyy-MM-dd"));
            secmBean.setIssuer(entity.getIssuer());
            secmBean.setPricetol(null);
            secmBean.setPricedecs("12");
            secmBean.setSettdays(entity.getSettdays());
            secmBean.setParamt("100");
            secmBean.setSettccy(entity.getSettccy());
            secmBean.setDescr(entity.getDescr());
            secmBean.setVdate(DateUtil.parse(entity.getIssudt(), "yyyy-MM-dd"));
            secmBean.setMdate(DateUtil.parse(entity.getMatdt(), "yyyy-MM-dd"));
            secmBean.setCouprate(entity.getCouponrate());
            secmBean.setBasis(entity.getBasis());
            secmBean.setIntpaycycle(entity.getIntpaycircle());
            secmBean.setIntcalcrule(entity.getIntcalcrule());
            secmBean.setSpreadRate("0");
            secmBean.setRedempamt("100");
            secmBean.setRatecode(null);
            secmBean.setRatedecs("8");
            secmBean.setRatetol(null);
            if (entity.getIntcalcrule().endsWith("DIS")) {
                secmBean.setRaterndrule("");
            } else {
                secmBean.setRaterndrule("U");
            }
            secmBean.setIntpayrule(entity.getIntpaymethod());
            secmBean.setRepocollpct(null);
            secmBean.setSecsacct(entity.getAccountno());
            secmBean.setTaxind(null);
            secmBean.setDcpriceind("C");
            secmBean.setIntenddaterule(entity.getIntenddaterule());
            secmBean.setCashdecs("8");
            secmBean.setAidecs("12");
            secmBean.setSecmsic("");
            secmBean.setFirstipaydate(null);
            secmBean.setCoupenddate(null);
            secmBean.setPricerndrule("U");

            SlInthBean inthBean = secmBean.getInthBean();
            inthBean.setFedealno(entity.getDealno());
            secmBean.setInthBean(inthBean);
            SlOutBean outBean = externalServer.addSemm(secmBean);
            if (outBean.getRetStatus().equals(RetStatusEnum.S)) {
                ifsOpicsBondService.updateById(entity);
                ifsBondCheckLogService.insert(checkLog);

                Map<String, Object> map = new HashMap<String, Object>();
                map.put("bndcd", entity.getBndcd());
                map.put("satacode", "-1");
                ifsOpicsBondService.updateStatcodeById(map);
            } else {
                ret.setCode(outBean.getRetCode());
                ret.setDesc(outBean.getRetMsg());
            }
        } catch (Exception e) {
            return RetMsgHelper.simple("000010", "复核出错:" + e.getMessage());
        }

        return ret;

    }

    /**
     * Excel上传债券信息
     */
    @ResponseBody
    @RequestMapping(value = "/importBondByExcel", method = RequestMethod.POST, produces = "text/plain;charset=UTF-8")
    public String importBondByExcel(MultipartHttpServletRequest request, HttpServletResponse response) {
    	String message = "导入成功";
		Workbook wb = null;
		try{
			List<UploadedFile> uploadedFileList = FileManage.requestExtractor(request);
			List<Workbook> excelList = new LinkedList<Workbook>();
			for (UploadedFile uploadedFile : uploadedFileList) {
				JY.ensure("xls".equals(uploadedFile.getType()), "上传文件类型错误,仅允许上传xls格式文件.");
				Workbook book = Workbook.getWorkbook(new ByteArrayInputStream(uploadedFile.getBytes()));
				excelList.add(book);
			}
			JY.require(excelList.size() == 1, "只能处理一份excel");
			wb = excelList.get(0);
			Sheet[] sheets = wb.getSheets();//获得卡片sheet数量
			
			//update by zl 2019-08-20 从第一页开始读取sheet
//			for (int i = 0; i < sheets.length; i++) { // 循环解析产品的会计配置

			Map<String, Object> mapParam = new HashMap<String, Object>();

			mapParam = ifsBondPledgeService.saveReadBondExcelImportToOpicsService(sheets[0]);
//			}
			message = "excel共计"+mapParam.get("rows")+"支债券,成功导入了"+mapParam.get("count")+"支债券";	
		}catch (Exception e) {
			message = "导入失败";
			// TODO: handle exception
			e.printStackTrace();
			return e.toString();
		}finally{
			if(null != wb)
				wb.close();
		}
		return message;
    }

    /**
     * 同步
     */
    @ResponseBody
    @RequestMapping(value = "/syncOpicsBond")
    public RetMsg<Serializable> syncOpicsBond(@RequestBody Map<String, String> map) {
        RetMsg<Serializable> ret = new RetMsg<Serializable>("000000", "发送成功");
        try {
            String bndcd = map.get("bndcd");
            SlSecmBean slSecmBean = externalServer.getSemmById(bndcd);
            if (slSecmBean != null) {
                return RetMsgHelper.simple("000010", "该债券信息已存在，请批量校准！");
            }
            IfsOpicsBond entity = ifsOpicsBondService.searchOneById(bndcd);
            SlSecmBean secmBean = new SlSecmBean(entity.getBr(), SlDealModule.SECUR.SERVER, SlDealModule.SECUR.TAG, SlDealModule.SECUR.DETAIL);
            secmBean.setSecid(entity.getBndcd());
            secmBean.setProduct(entity.getProduct());
            secmBean.setProdtype(entity.getProdtype());
            secmBean.setCcy(entity.getCcy());
            secmBean.setSectype("");
            secmBean.setTenor("99");
            secmBean.setAcctngtype(entity.getAcctngtype());
            secmBean.setSecunit(entity.getSecunit());
            secmBean.setDenom(entity.getDenom());
            secmBean.setIssdate(DateUtil.parse(entity.getPubdt(), "yyyy-MM-dd"));
            secmBean.setIssuer(entity.getIssuer().trim());
            secmBean.setIssueprice(entity.getIssprice());
            secmBean.setPricedecs("12");
            secmBean.setSettdays(entity.getSettdays());
            secmBean.setParamt("100");
            secmBean.setSettccy(entity.getSettccy());
            secmBean.setDescr(entity.getDescr());
            secmBean.setVdate(DateUtil.parse(entity.getIssudt(), "yyyy-MM-dd"));
            secmBean.setMdate(DateUtil.parse(entity.getMatdt(), "yyyy-MM-dd"));
            //固定利率
            secmBean.setCouprate(entity.getCouponrate());
            secmBean.setBasis(entity.getBasis());
            secmBean.setIntpaycycle(entity.getIntpaycircle());
            secmBean.setIntcalcrule(entity.getIntcalcrule());
            //浮动点差
            secmBean.setSpreadRate(entity.getBasicspread());
            secmBean.setRatecode(entity.getFloatratemark());
            secmBean.setRedempamt("100");
            secmBean.setRatedecs("8");
            if (entity.getIntcalcrule() != null && !"".equals(entity.getIntcalcrule())) {
                if ("DIS".equals(entity.getIntcalcrule())) {
                    secmBean.setRaterndrule("");
                } else {
                    secmBean.setRaterndrule("U");
                }
                if ("MBS".equals(entity.getIntcalcrule())) {
                    secmBean.setDelaydays(entity.getSettdays());
                    secmBean.setFirstipaydate(DateUtil.parse(entity.getParagraphfirstplan()));
                    secmBean.setPrepmtmodel("ABS");
                } else {
                    secmBean.setDelaydays("");
                }
            }
            secmBean.setIntpayrule(entity.getIntpaymethod());
            secmBean.setSecsacct(entity.getAccountno());
            secmBean.setDcpriceind("C");
            secmBean.setIntenddaterule(entity.getIntenddaterule());
            secmBean.setCashdecs("8");
            secmBean.setAidecs("12");
            secmBean.setSecmsic(entity.getBondproperties());
            secmBean.setPricerndrule("U");

            SlInthBean inthBean = secmBean.getInthBean();
            inthBean.setFedealno(entity.getDealno());
            secmBean.setInthBean(inthBean);

            SlOutBean outBean = externalServer.addSemm(secmBean);
            if (outBean.getRetStatus().equals(RetStatusEnum.S)) {
                entity.setStatus("B");
                ifsOpicsBondService.updateById(entity);

                Map<String, Object> map2 = new HashMap<String, Object>();
                map2.put("bndcd", entity.getBndcd());
                map2.put("satacode", "-1");
                ifsOpicsBondService.updateStatcodeById(map2);
            } else {
                ret.setCode(outBean.getRetCode());
                ret.setDesc(outBean.getRetMsg());
            }
        } catch (Exception e) {
            e.printStackTrace();
            return RetMsgHelper.simple("000010", "发送出错:" + e.getMessage());
        }
        return ret;
    }

    /**
     * 批量校准信息
     */
    @ResponseBody
    @RequestMapping(value = "/batchCalibration")
    public RetMsg<SlOutBean> verify(@RequestBody Map<String, String> map) {
        SlOutBean slOutBean = new SlOutBean();
        String type = String.valueOf(map.get("type"));
        String bndcd = "";
        String[] bndcds;
        if ("1".equals(type)) { // 选中记录校准
            bndcd = map.get("bndcd");
            bndcds = bndcd.split(",");
        } else if ("2".equals(type)) {
            //校准输入值
            bndcd = map.get("bndcd");
            bndcds = new String[]{bndcd};
            type = "1";
        } else { // 全部校准
            bndcds = null;
        }
        try {
            slOutBean = ifsOpicsBondService.batchCalibration(type, bndcds);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return RetMsgHelper.ok(slOutBean);
    }

    /**
     * 通过字典value查询key
     */
    @ResponseBody
    @RequestMapping(value = "/queryKey")
    public String queryKey(@RequestBody Map<String, String> map) {
        String code = map.get("code");
        String value = map.get("value");
        String key = null;
        List<TaDictVo> list = dictionaryGetService.getTaDictByCode(code);
        for (TaDictVo taDictVo : list) {
            if (taDictVo.getDict_value().substring(0, 4).equalsIgnoreCase(value.substring(0, 4))) {
                key = taDictVo.getDict_key();
            }
        }

        return key;
    }

    /**
     * 通过映射表映射
     *
     * @param map
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/queryKeyMapping")
    public IfsWdOpicsSecmap queryKeyMapping(@RequestBody Map<String, String> map) {
        IfsWdOpicsSecmap queryKeyMapping = wdOpicsSecmapMapper.queryKeyMapping(map);
        return queryKeyMapping;
    }


    @ResponseBody
    @RequestMapping(value = "/queryKeyLevel")
    public IfsWindSecLevelBean queryKeyLevel(@RequestBody Map<String, String> map) {
        IfsWindSecLevelBean wslb = ifsWindSecLevelMapper.selectByPrimaryKey(ParameterUtil.getString(map, "secid", ""));
        if (wslb != null) {
            List<IfsWdOpicsSecmap> typelist = wdOpicsSecmapMapper.queryTypeList();
            for (IfsWdOpicsSecmap ifsWdOpicsSecmap : typelist) {
                if (wslb.getZtInst() != null && wslb.getZtInst().equals(ifsWdOpicsSecmap.getWdid())) {
                    wslb.setZtInst(ifsWdOpicsSecmap.getOpicsid());// 主体评级机构
                }
                if(wslb.getZxZtLevel() != null && wslb.getZxZtLevel().equals(ifsWdOpicsSecmap.getWdid())){
                    wslb.setZxZtLevel(ifsWdOpicsSecmap.getOpicsid());// 主体评级级别
                }
                if (wslb.getZxInst() != null && wslb.getZxInst().equals(ifsWdOpicsSecmap.getWdid())) {
                    wslb.setZxInst(ifsWdOpicsSecmap.getOpicsid());// 债项评级机构
                }
                if(wslb.getZxZxLevel() != null && wslb.getZxZxLevel().equals(ifsWdOpicsSecmap.getWdid())){
                    wslb.setZxZxLevel(ifsWdOpicsSecmap.getOpicsid());// 债项评级级别
                }
            }
        }
        return wslb;
    }
}
