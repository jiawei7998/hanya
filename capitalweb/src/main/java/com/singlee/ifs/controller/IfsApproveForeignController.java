package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.ifs.model.*;
import com.singlee.ifs.service.IFSApproveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.Map;

/**
 * 
 * 人民币期权事前审批交易Controller
 * 
 * ClassName: IfsApproveForeignController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason: TODO ADD REASON(可选). <br/>
 * date: 2018-5-30 下午07:07:28 <br/>
 * 
 * @author zhangcm
 * @version
 * @since JDK 1.6
 */
@Controller
@RequestMapping(value = "/IfsApproveForeignController")
public class IfsApproveForeignController  extends CommonController {
	@Autowired
	private IFSApproveService iFSApproveService;

	/* 人民币期权 */

	// 分页查询
	@ResponseBody
	@RequestMapping(value = "/searchOptionPage")
	public RetMsg<PageInfo<IfsApprovefxOption>> searchOptionPage(@RequestBody Map<String, Object> map) {
		Page<IfsApprovefxOption> page = iFSApproveService.searchOptionPage(map);
		return RetMsgHelper.ok(page);
	}

	// 查询
	@ResponseBody
	@RequestMapping(value = "/searchOption")
	public RetMsg<IfsApprovefxOption> searchOption(@RequestBody Map<String, Object> map) {
		String ticketId = map.get("ticketId").toString();
		IfsApprovefxOption page = iFSApproveService.searchOption(ticketId);
		return RetMsgHelper.ok(page);
	}

	// 增加
	@ResponseBody
	@RequestMapping(value = "/addOption")
	public RetMsg<Serializable> addOption(HttpServletRequest request, @RequestBody IfsApprovefxOption map) {
		iFSApproveService.addOption(map);
		return RetMsgHelper.ok();
	}

	// 修改
	@ResponseBody
	@RequestMapping(value = "/editOption")
	public RetMsg<Serializable> editOption(@RequestBody IfsApprovefxOption map) {
		iFSApproveService.editOption(map);
		return RetMsgHelper.ok();
	}

	// 删除

	@ResponseBody
	@RequestMapping(value = "/deleteOption")
	public RetMsg<Serializable> deleteOption(@RequestBody Map<String, String> map) {
		String ticketId = map.get("ticketid");
		iFSApproveService.deleteOption(ticketId);
		return RetMsgHelper.ok();
	}

	/**
	 * 人民币期权-查询我发起的
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-21
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageOptionMine")
	public RetMsg<PageInfo<IfsApprovefxOption>> searchPageOptionMine(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsApprovefxOption> page = iFSApproveService.getOptionMinePage(params, 3);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 人民币期权-查询 待审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageOptionUnfinished")
	public RetMsg<PageInfo<IfsApprovefxOption>> searchPageOptionUnfinished(@RequestBody Map<String, Object> params) {
		String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse + ","
				+ DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
		String[] orderStatus = approveStatus.split(",");
		params.put("approveStatusNo", orderStatus);
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsApprovefxOption> page = iFSApproveService.getOptionMinePage(params, 1);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 人民币期权-查询 已审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageOptionFinished")
	public RetMsg<PageInfo<IfsApprovefxOption>> searchPageOptionFinished(@RequestBody Map<String, Object> params) {
		String approveStatusNo = ParameterUtil.getString(params, "approveStatus", "");
		if (StringUtil.isNullOrEmpty(approveStatusNo)) {
			approveStatusNo = DictConstants.ApproveStatus.ApprovedPass;
			approveStatusNo += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatusNo += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatusNo += "," + DictConstants.ApproveStatus.Verified;
			approveStatusNo += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			approveStatusNo += "," + DictConstants.ApproveStatus.Approving;
			String[] orderStatus = approveStatusNo.split(",");
			params.put("approveStatusNo", orderStatus);
		}
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsApprovefxOption> page = iFSApproveService.getOptionMinePage(params, 2);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 勾兑时，查询表中审批通过的数据
	 * 
	 * @param
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchPageApprovefxOptionPassed")
	public RetMsg<PageInfo<IfsApprovefxOption>> searchPageApprovefxOptionPassed(@RequestBody Map<String, Object> params) {
		Page<IfsApprovefxOption> page = iFSApproveService.getApprovefxOptionPassedPage(params);
		return RetMsgHelper.ok(page);
	}

	/* 外汇即期 */

	// 分页查询
	@ResponseBody
	@RequestMapping(value = "/searchSpotPage")
	public RetMsg<PageInfo<IfsApprovefxSpt>> searchFormSpotPage(@RequestBody Map<String, Object> map) {
		Page<IfsApprovefxSpt> page = iFSApproveService.getSpotPage(map);
		return RetMsgHelper.ok(page);
	}

	// 查询
	@ResponseBody
	@RequestMapping(value = "/searchSpot")
	public RetMsg<IfsApprovefxSpt> searchFormSpot(@RequestBody Map<String, Object> map) {
		String ticketId = map.get("ticketId").toString();
		IfsApprovefxSpt page = iFSApproveService.getSpot(ticketId);
		return RetMsgHelper.ok(page);
	}

	// 增加
	@ResponseBody
	@RequestMapping(value = "/addSpot")
	public RetMsg<Serializable> addSpot(HttpServletRequest request, @RequestBody IfsApprovefxSpt map) {
		iFSApproveService.addSpot(map);
		return RetMsgHelper.ok();
	}

	// 修改
	@ResponseBody
	@RequestMapping(value = "/editSpot")
	public RetMsg<Serializable> editSpot(@RequestBody IfsApprovefxSpt map) {
		iFSApproveService.editSpot(map);
		return RetMsgHelper.ok();
	}

	// 删除
	@ResponseBody
	@RequestMapping(value = "/deleteSpot")
	public RetMsg<Serializable> deleteSpot(@RequestBody Map<String, String> map) {
		String ticketId = map.get("ticketid");
		iFSApproveService.deleteSpot(ticketId);
		return RetMsgHelper.ok();
	}

	/**
	 * 根据主键查询成交单(返回值增加 流程id)
	 * 
	 * @param
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchSpotAndFlowIdById")
	public RetMsg<IfsApprovefxSpt> searchSpotAndFlowIdById(@RequestBody Map<String, Object> key) {
		IfsApprovefxSpt spot = iFSApproveService.getSpotAndFlowIdById(key);
		return RetMsgHelper.ok(spot);
	}

	/**
	 * 外汇即期-查询我发起的
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-21
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageSpotMine")
	public RetMsg<PageInfo<IfsApprovefxSpt>> searchPageSpotMine(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsApprovefxSpt> page = iFSApproveService.getSpotMinePage(params, 3);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 人民币外汇即期-查询 待审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageSpotUnfinished")
	public RetMsg<PageInfo<IfsApprovefxSpt>> searchPageSpotUnfinished(@RequestBody Map<String, Object> params) {
		String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse + ","
				+ DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
		String[] orderStatus = approveStatus.split(",");
		params.put("approveStatusNo", orderStatus);
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsApprovefxSpt> page = iFSApproveService.getSpotMinePage(params, 1);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 人民币外汇即期-查询 已审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageSpotFinished")
	public RetMsg<PageInfo<IfsApprovefxSpt>> searchPageSpotFinished(@RequestBody Map<String, Object> params) {
		String approveStatusNo = ParameterUtil.getString(params, "approveStatus", "");
		if (StringUtil.isNullOrEmpty(approveStatusNo)) {
			approveStatusNo = DictConstants.ApproveStatus.ApprovedPass;
			approveStatusNo += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatusNo += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatusNo += "," + DictConstants.ApproveStatus.Verified;
			approveStatusNo += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			approveStatusNo += "," + DictConstants.ApproveStatus.Approving;
			String[] orderStatus = approveStatusNo.split(",");
			params.put("approveStatusNo", orderStatus);
		}
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsApprovefxSpt> page = iFSApproveService.getSpotMinePage(params, 2);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 勾兑时，查询表中审批通过的数据
	 * 
	 * @param
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchPageApprovefxSptPassed")
	public RetMsg<PageInfo<IfsApprovefxSpt>> searchPageApprovefxSptPassed(@RequestBody Map<String, Object> params) {
		Page<IfsApprovefxSpt> page = iFSApproveService.getApprovefxSptPassedPage(params);
		return RetMsgHelper.ok(page);
	}

	/* 外汇远期 */

	// 分页查询
	@ResponseBody
	@RequestMapping(value = "/searchCreditEdit")
	public RetMsg<PageInfo<IfsApprovefxFwd>> searchForm(@RequestBody Map<String, Object> map) {
		Page<IfsApprovefxFwd> page = iFSApproveService.getCreditEdit(map);
		return RetMsgHelper.ok(page);
	}

	// 查询
	@ResponseBody
	@RequestMapping(value = "/searchCredit")
	public RetMsg<IfsApprovefxFwd> searchFormEdit(@RequestBody Map<String, String> map) {
		String ticketId = map.get("ticketId");
		IfsApprovefxFwd page = iFSApproveService.getCredit(ticketId);
		return RetMsgHelper.ok(page);
	}

	// 增加
	@ResponseBody
	@RequestMapping(value = "/addCreditEdit")
	public RetMsg<Serializable> addForm(HttpServletRequest request, @RequestBody IfsApprovefxFwd map) {
		iFSApproveService.addCreditCondition(map);
		return RetMsgHelper.ok();
	}

	// 修改
	@ResponseBody
	@RequestMapping(value = "/editCreditEdit")
	public RetMsg<Serializable> editFormEdit(@RequestBody IfsApprovefxFwd map) {
		iFSApproveService.editCredit(map);
		return RetMsgHelper.ok();
	}

	// 删除
	@ResponseBody
	@RequestMapping(value = "/deleteCreditEdit")
	public RetMsg<Serializable> deleteFormEdit(@RequestBody Map<String, String> map) {
		String ticketId = map.get("ticketid");
		iFSApproveService.deleteCredit(ticketId);
		return RetMsgHelper.ok();
	}

	/**
	 * 根据主键查询成交单(返回值增加 流程id)
	 * 
	 * @param
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchFwdAndFlowIdById")
	public RetMsg<IfsApprovefxFwd> searchFwdAndFlowIdById(@RequestBody Map<String, Object> key) {
		IfsApprovefxFwd fwd = iFSApproveService.getFwdAndFlowIdById(key);
		return RetMsgHelper.ok(fwd);
	}

	/**
	 * 外汇远期-查询我发起的
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-21
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageFxFwdMine")
	public RetMsg<PageInfo<IfsApprovefxFwd>> searchPageFxFwdMine(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsApprovefxFwd> page = iFSApproveService.getFxFwdMinePage(params, 3);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 人民币外汇远期-查询 待审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageFxFwdUnfinished")
	public RetMsg<PageInfo<IfsApprovefxFwd>> searchPageFxFwdUnfinished(@RequestBody Map<String, Object> params) {
		String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse + ","
				+ DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
		String[] orderStatus = approveStatus.split(",");
		params.put("approveStatusNo", orderStatus);
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsApprovefxFwd> page = iFSApproveService.getFxFwdMinePage(params, 1);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 人民币外汇远期-查询 已审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageFxFwdFinished")
	public RetMsg<PageInfo<IfsApprovefxFwd>> searchPageFxFwdFinished(@RequestBody Map<String, Object> params) {
		String approveStatusNo = ParameterUtil.getString(params, "approveStatus", "");
		if (StringUtil.isNullOrEmpty(approveStatusNo)) {
			approveStatusNo = DictConstants.ApproveStatus.ApprovedPass;
			approveStatusNo += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatusNo += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatusNo += "," + DictConstants.ApproveStatus.Verified;
			approveStatusNo += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			approveStatusNo += "," + DictConstants.ApproveStatus.Approving;
			String[] orderStatus = approveStatusNo.split(",");
			params.put("approveStatusNo", orderStatus);
		}
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsApprovefxFwd> page = iFSApproveService.getFxFwdMinePage(params, 2);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 勾兑时，查询表中审批通过的数据
	 * 
	 * @param
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchPageApprovefxFwdPassed")
	public RetMsg<PageInfo<IfsApprovefxFwd>> searchPageApprovefxFwdPassed(@RequestBody Map<String, Object> params) {
		Page<IfsApprovefxFwd> page = iFSApproveService.getApprovefxFwdPassedPage(params);
		return RetMsgHelper.ok(page);
	}

	/* 外汇掉期 */

	// 分页查询
	@ResponseBody
	@RequestMapping(value = "/searchrmswappage")
	public RetMsg<PageInfo<IfsApprovefxSwap>> searchFormswapPage(@RequestBody Map<String, Object> map) {
		Page<IfsApprovefxSwap> page = iFSApproveService.getswapPage(map);
		return RetMsgHelper.ok(page);
	}

	// 查询
	@ResponseBody
	@RequestMapping(value = "/searchrmswap")
	public RetMsg<IfsApprovefxSwap> searchFormswap(@RequestBody Map<String, Object> map) {
		String ticketId = map.get("ticketId").toString();
		IfsApprovefxSwap page = iFSApproveService.getrmswap(ticketId);
		return RetMsgHelper.ok(page);
	}

	// 增加
	@ResponseBody
	@RequestMapping(value = "/addrmswap")
	public RetMsg<Serializable> addrmswap(@RequestBody IfsApprovefxSwap map) {
		iFSApproveService.addrmswap(map);
		return RetMsgHelper.ok();
	}
	
	
	// 修改
	@ResponseBody
	@RequestMapping(value = "/editrmswap")
	public RetMsg<Serializable> editrmswap(@RequestBody IfsApprovefxSwap map) {
		iFSApproveService.editrmswap(map);
		return RetMsgHelper.ok();
	}

	// 删除
	@ResponseBody
	@RequestMapping(value = "/deletermswap")
	public RetMsg<Serializable> deletermswap(@RequestBody Map<String, String> map) {
		String ticketId = map.get("ticketid");
		iFSApproveService.deletermswap(ticketId);
		return RetMsgHelper.ok();
	}

	/**
	 * 根据主键查询成交单(返回值增加 流程id)
	 * 
	 * @param
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchRmbSwapAndFlowIdById")
	public RetMsg<IfsApprovefxSwap> searchRmbSwapAndFlowIdById(@RequestBody Map<String, Object> key) {
		IfsApprovefxSwap swap = iFSApproveService.getRmbSwapAndFlowIdById(key);
		return RetMsgHelper.ok(swap);
	}

	/**
	 * 人民币外汇掉期-查询我发起的
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-21
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageFxSwapMine")
	public RetMsg<PageInfo<IfsApprovefxSwap>> searchPageFxSwapMine(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsApprovefxSwap> page = iFSApproveService.getFxSwapMinePage(params, 3);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 人民币外汇掉期-查询 待审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageFxSwapUnfinished")
	public RetMsg<PageInfo<IfsApprovefxSwap>> searchPageFxSwapUnfinished(@RequestBody Map<String, Object> params) {
		String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse + ","
				+ DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
		String[] orderStatus = approveStatus.split(",");
		params.put("approveStatusNo", orderStatus);
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsApprovefxSwap> page = iFSApproveService.getFxSwapMinePage(params, 1);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 人民币外汇掉期-查询 已审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageFxSwapFinished")
	public RetMsg<PageInfo<IfsApprovefxSwap>> searchPageFxSwapFinished(@RequestBody Map<String, Object> params) {
		String approveStatusNo = ParameterUtil.getString(params, "approveStatus", "");
		if (StringUtil.isNullOrEmpty(approveStatusNo)) {
			approveStatusNo = DictConstants.ApproveStatus.ApprovedPass;
			approveStatusNo += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatusNo += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatusNo += "," + DictConstants.ApproveStatus.Verified;
			approveStatusNo += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			approveStatusNo += "," + DictConstants.ApproveStatus.Approving;
			String[] orderStatus = approveStatusNo.split(",");
			params.put("approveStatusNo", orderStatus);
		}
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsApprovefxSwap> page = iFSApproveService.getFxSwapMinePage(params, 2);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 根据主键查询成交单(返回值增加 流程id)
	 * 
	 * @param
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchRmbOptAndFlowIdById")
	public RetMsg<IfsApprovefxOption> searchRmbOptAndFlowIdById(@RequestBody Map<String, Object> key) {
		IfsApprovefxOption opt = iFSApproveService.searchRmbOptAndFlowIdById(key);
		return RetMsgHelper.ok(opt);
	}

	/**
	 * 根据主键查询成交单(返回值增加 流程id)
	 * 
	 * @param
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchRmbCcylendingAndFlowIdById")
	public RetMsg<IfsApprovefxLend> searchRmbCcylendingAndFlowIdById(@RequestBody Map<String, Object> key) {
		IfsApprovefxLend ccylend = iFSApproveService.searchRmbCcylendingAndFlowIdById(key);
		return RetMsgHelper.ok(ccylend);
	}

	/**
	 * 根据主键查询成交单(返回值增加 流程id)
	 * 
	 * @param
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchRmbCcypaAndFlowIdById")
	public RetMsg<IfsApprovefxAllot> searchRmbCcypaAndFlowIdById(@RequestBody Map<String, Object> key) {
		IfsApprovefxAllot allot = iFSApproveService.searchRmbCcypaAndFlowIdById(key);
		return RetMsgHelper.ok(allot);
	}

	/**
	 * 根据主键查询成交单(返回值增加 流程id)
	 * 
	 * @param
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchRmbSpotAndFlowIdById")
	public RetMsg<IfsApprovefxOnspot> searchRmbSpotAndFlowIdById(@RequestBody Map<String, Object> key) {
		IfsApprovefxOnspot onsport = iFSApproveService.searchRmbSpotAndFlowIdById(key);
		return RetMsgHelper.ok(onsport);
	}

	/**
	 * 勾兑时，查询表中审批通过的数据
	 * 
	 * @param
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchPageApprovefxSwapPassed")
	public RetMsg<PageInfo<IfsApprovefxSwap>> searchPageApprovefxSwapPassed(@RequestBody Map<String, Object> params) {
		Page<IfsApprovefxSwap> page = iFSApproveService.getApprovefxSwapPassedPage(params);
		return RetMsgHelper.ok(page);
	}
}
