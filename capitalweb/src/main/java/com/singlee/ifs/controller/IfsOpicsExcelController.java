package com.singlee.ifs.controller;

import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import java.io.*;

/**
 * OPICS EXCEL Controller
 * 
 * @author zc
 * @version
 * @since JDK 1.6
 */
@Controller
@RequestMapping(value = "/IfsOpicsExcelController")
public class IfsOpicsExcelController {

	private static Logger logger = LoggerFactory.getLogger(IfsOpicsExcelController.class);

	@ResponseBody
	@RequestMapping(value = "/uploadRate", method = RequestMethod.POST, produces = "text/html;charset=UTF-8")
	public RetMsg<Serializable> uploadRate(MultipartHttpServletRequest request) {
		OutputStream out = null;
		InputStream in = null;
		try {
			logger.info("rateFeed文件上传...");
			MultipartFile mf = request.getFile("Fdata");
			if (null == mf) {
				return RetMsgHelper.simple("000010", "上传文件为空");
			}
			in = mf.getInputStream();
			String savePath = "C:/DOCS/";
			File folder = new File(savePath);
			if (!folder.exists()) {
				folder.mkdirs();
			}
			String filename = mf.getOriginalFilename();
			logger.info("上传文件路径:" + savePath + filename);
			out = new FileOutputStream(new File(savePath, filename));
			int length = 0;
			byte[] buf = new byte[102400];
			while ((length = in.read(buf)) != -1) {
				// in.read(buf) 每次读到的数据存放在 buf 数组中
				// 在 buf 数组中 取出数据 写到 （输出流）磁盘上
				out.write(buf, 0, length); 
			}
		} catch (IOException e) {
			logger.info("更新异常：" + e.getMessage());
			return RetMsgHelper.simple("000010", "更新异常:" + e.getMessage());
		} finally {
			try {
				if (null != in) {
					in.close();
				}
			} catch (IOException e) {
			}
			try {
				if (null != out) {
					out.close();
				}
			} catch (IOException e) {
			}
		}
		return RetMsgHelper.ok();
	}
}
