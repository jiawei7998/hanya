package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.ifs.model.IfsCfetgoldLend;
import com.singlee.ifs.service.IFSService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.Map;

/**
 * 黄金拆借正式交易审批Controller
 * 
 * ClassName: IfsGoldLendController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason: TODO ADD REASON(可选). <br/>
 * date: 2018-5-30 下午07:36:32 <br/>
 * 
 * @author shenzl
 * @version
 * @since JDK 1.6
 */
@Controller
@RequestMapping(value = "/IfsGoldLendController")
public class IfsGoldLendController {
	@Autowired
	private IFSService iFSService;

	// 分页查询
	@ResponseBody
	@RequestMapping(value = "/searchGoldLendPage")
	public RetMsg<PageInfo<IfsCfetgoldLend>> searchGoldLendPage(@RequestBody Map<String, Object> map) {
		Page<IfsCfetgoldLend> page = iFSService.searchGoldLendPage(map);
		return RetMsgHelper.ok(page);
	}

	// 删除
	@ResponseBody
	@RequestMapping(value = "/deleteGoldLend")
	public RetMsg<Serializable> deleteGoldLend(@RequestBody Map<String, Object> map) {
		String ticketid = map.get("ticketid").toString();
		iFSService.deleteGoldLend(ticketid);
		return RetMsgHelper.ok();
	}

	// 新增
	@ResponseBody
	@RequestMapping(value = "/addGoldLend")
	public String addCcySwap(@RequestBody IfsCfetgoldLend map) {
		String message = iFSService.addGoldLend(map);
		return message;
	}

	// 修改
	@ResponseBody
	@RequestMapping(value = "/editGoldLend")
	public RetMsg<Serializable> editCcySwap(@RequestBody IfsCfetgoldLend map) {
		iFSService.editGoldLend(map);
		return RetMsgHelper.ok();
	}

	// 查询
	@ResponseBody
	@RequestMapping(value = "/searchGoldLend")
	public RetMsg<IfsCfetgoldLend> searchCcySwap(@RequestBody Map<String, Object> map) {
		String ticketid = map.get("ticketId").toString();
		IfsCfetgoldLend gold1 = iFSService.searchGoldLend(ticketid);
		return RetMsgHelper.ok(gold1);
	}

	/**
	 * 黄金拆借-查询我发起的
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-21
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageGoldLendMine")
	public RetMsg<PageInfo<IfsCfetgoldLend>> searchPageGoldLendMine(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetgoldLend> page = iFSService.getGoldLendMinePage(params, 3);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 黄金拆借-查询 待审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageGoldLendUnfinished")
	public RetMsg<PageInfo<IfsCfetgoldLend>> searchPageGoldLendUnfinished(@RequestBody Map<String, Object> params) {
		String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse + ","
				+ DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
		String[] orderStatus = approveStatus.split(",");
		params.put("approveStatusNo", orderStatus);
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetgoldLend> page = iFSService.getGoldLendMinePage(params, 1);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 黄金拆借-查询 已审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageGoldLendFinished")
	public RetMsg<PageInfo<IfsCfetgoldLend>> searchPageGoldLendFinished(@RequestBody Map<String, Object> params) {
		String approveStatusNo = ParameterUtil.getString(params, "approveStatus", "");
		if (StringUtil.isNullOrEmpty(approveStatusNo)) {
			approveStatusNo = DictConstants.ApproveStatus.ApprovedPass;
			approveStatusNo += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatusNo += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatusNo += "," + DictConstants.ApproveStatus.Verified;
			approveStatusNo += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			approveStatusNo += "," + DictConstants.ApproveStatus.Approving;
			String[] orderStatus = approveStatusNo.split(",");
			params.put("approveStatusNo", orderStatus);
		}
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetgoldLend> page = iFSService.getGoldLendMinePage(params, 2);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 根据主键关联流程查询成交单
	 * 
	 * @param Key
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchCfetsGoldLendAndFlowById")
	public RetMsg<IfsCfetgoldLend> searchCfetsmetalGoldAndFlowById(@RequestBody Map<String, Object> key) {
		IfsCfetgoldLend gold = iFSService.getGoldLendAndFlowById(key);
		return RetMsgHelper.ok(gold);
	}

	/**
	 * 勾兑时，查询表中审批通过的数据
	 * 
	 * @param
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchPageApprovegoldLendPassed")
	public RetMsg<PageInfo<IfsCfetgoldLend>> searchPageApprovegoldLendPassed(@RequestBody Map<String, Object> params) {
		Page<IfsCfetgoldLend> page = iFSService.getApproveGoldLendPassedPage(params);
		return RetMsgHelper.ok(page);
	}
}
