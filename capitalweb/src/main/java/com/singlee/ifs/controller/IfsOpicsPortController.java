package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.bean.SlPortBean;
import com.singlee.financial.opics.IStaticServer;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.SlErrors;
import com.singlee.ifs.model.IfsOpicsPort;
import com.singlee.ifs.service.IfsOpicsPortService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * OPICS投资组合Controller
 * 
 * ClassName: IfsOpicsPortController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason: TODO ADD REASON(可选). <br/>
 * date: 2018-5-30 下午07:41:51 <br/>
 * 
 * @author zhengfl
 * @version
 * @since JDK 1.6
 */
@Controller
@RequestMapping(value = "/IfsOpicsPortController")
public class IfsOpicsPortController {
	@Autowired
	IfsOpicsPortService ifsOpicsPortService;
	@Autowired
	IStaticServer iNonParamServer;

	/***
	 * 分页查询
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPagePort")
	public RetMsg<PageInfo<IfsOpicsPort>> searchPagePort(@RequestBody Map<String, Object> map) {
		Page<IfsOpicsPort> page = ifsOpicsPortService.searchPageOpicsPort(map);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 新增
	 */
	@ResponseBody
	@RequestMapping(value = "/saveOpicsPortAdd")
	public RetMsg<Serializable> saveOpicsPortAdd(@RequestBody IfsOpicsPort entity) {
		ifsOpicsPortService.insert(entity);
		return RetMsgHelper.ok();
	}

	/**
	 * 修改
	 */
	@ResponseBody
	@RequestMapping(value = "/saveOpicsPortEdit")
	public RetMsg<Serializable> saveOpicsPortEdit(@RequestBody IfsOpicsPort entity) {
		ifsOpicsPortService.updateById(entity);
		return RetMsgHelper.ok();
	}

	/**
	 * 删除
	 */
	@ResponseBody
	@RequestMapping(value = "/deletePort")
	public RetMsg<Serializable> deletePort(@RequestBody Map<String, Object> map) {
		ifsOpicsPortService.deleteById(map);
		return RetMsgHelper.ok();
	}

	@ResponseBody
	@RequestMapping(value = "/searchPort")
	public RetMsg<List<IfsOpicsPort>> searchPort(@RequestBody HashMap<String, Object> map) throws RException {
		List<IfsOpicsPort> list = ifsOpicsPortService.searchPort(map);
		return RetMsgHelper.ok(list);
	}

	/**
	 * 同步到opics表
	 */
	@ResponseBody
	@RequestMapping(value = "/syncPort")
	public RetMsg<SlOutBean> syncPort(@RequestBody Map<String, String> map) {
		SlOutBean slOutBean = new SlOutBean();
		IfsOpicsPort ifsOpicsPort = ifsOpicsPortService.searchById(map);
		SlPortBean portBean = new SlPortBean();
		portBean.setAmount1(ifsOpicsPort.getAmount1());
		portBean.setAmount2(ifsOpicsPort.getAmount2());
		portBean.setBr(ifsOpicsPort.getBr());
		portBean.setCost(ifsOpicsPort.getCost());
		portBean.setDate1(ifsOpicsPort.getDate1());
		portBean.setDate2(ifsOpicsPort.getDate2());
		// 转换成yyyy-MM-dd日期格式
		Date lstmntdte = DateUtil.parse(DateUtil.format(ifsOpicsPort.getLstmntdte()));
		portBean.setLstmntdte(lstmntdte);
		portBean.setPortdesc(ifsOpicsPort.getPortdesc());
		portBean.setPortfolio(ifsOpicsPort.getPortfolio());
		portBean.setText1(ifsOpicsPort.getText1());
		portBean.setText2(ifsOpicsPort.getText2());
		portBean.setUpdateCounter(ifsOpicsPort.getUpdatecounter());

		try {
			slOutBean = iNonParamServer.addPort(portBean);
		} catch (RemoteConnectFailureException e) {
			slOutBean.setRetCode(SlErrors.FAILED_RMI_OPICS.getErrCode());
			slOutBean.setRetMsg(SlErrors.FAILED_RMI_OPICS.getErrMsg() + ":" + e.getMessage());
			slOutBean.setRetStatus(RetStatusEnum.F);
		} catch (Exception e) {
			slOutBean.setRetCode(SlErrors.FAILED.getErrCode());
			slOutBean.setRetMsg(SlErrors.FAILED.getErrMsg() + ":" + e.getMessage());
			slOutBean.setRetStatus(RetStatusEnum.F);
		}
		if ("S".equals(String.valueOf(slOutBean.getRetStatus()))) {
			ifsOpicsPort.setStatus("1");
			ifsOpicsPortService.updateStatus(ifsOpicsPort);

		}
		return RetMsgHelper.ok(slOutBean);
	}

	/**
	 * 批量校准
	 */
	@ResponseBody
	@RequestMapping(value = "/batchCalibration")
	public RetMsg<SlOutBean> batchCalibration(@RequestBody Map<String, String> map) {
		SlOutBean slOutBean = new SlOutBean();
		String type = String.valueOf(map.get("type"));
		String br;
		String portfolio;
		String[] brs;
		String[] portfolios;
		if ("1".equals(type)) {
			br = map.get("br");
			portfolio = map.get("portfolio");
			brs = br.split(",");
			portfolios = portfolio.split(",");
		} else {
			brs = null;
			portfolios = null;
		}
		try {
			slOutBean = ifsOpicsPortService.batchCalibration(type, brs, portfolios);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return RetMsgHelper.ok(slOutBean);

	}
	
	/***
	 * 查询全部
	 */
	@ResponseBody
	@RequestMapping(value = "/searchAllOpicsPort")
	public List<IfsOpicsPort> searchAllOpicsPort() {
		List<IfsOpicsPort> page = ifsOpicsPortService.searchAllOpicsPort();
		return page;
	}
}
