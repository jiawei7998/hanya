package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.ifs.model.IfsCfetsFlowUser;
import com.singlee.ifs.service.IFSService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping(value = "/IfsCfetsFlowUserManageController")
public class IfsCfetsFlowUserManageController {

	@Autowired
	private IFSService cfetsFlowUser;
	
	
	/**
	 * 成交单分页查询
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchCfetsFlowUserManage")
	public RetMsg<PageInfo<IfsCfetsFlowUser>> searchCfetsFlowUserManage(@RequestBody IfsCfetsFlowUser record) {
		Page<IfsCfetsFlowUser> page = cfetsFlowUser.searchCfetsFlowUserManage(record);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 根据主键查询成交单(返回值增加 流程id)
	 * */
	@ResponseBody
	@RequestMapping(value = "/queryCfetsFlowUserById")
	public RetMsg<IfsCfetsFlowUser> queryCfetsFlowUserById(@RequestBody Map<String, Object> key) {
		IfsCfetsFlowUser dp = cfetsFlowUser.queryCfetsFlowUserById(key);
		return RetMsgHelper.ok(dp);
	}
	/**
	 * 新增
	 */
	@ResponseBody
	@RequestMapping(value = "/AddFlowUser")
	public RetMsg<Serializable> AddFlowUser(@RequestBody Map<String, Object> map) {
		String action=map.get("action").toString();
		if("add".equals(action)){
			IfsCfetsFlowUser flowUser = cfetsFlowUser.queryCfetsFlowUserById(map);
			if(flowUser==null){
				cfetsFlowUser.insertFlowUser(map);
			}
		}else if("edit".equals(action)){
			Map<String, Object> map1=new HashMap<String, Object>();
			map1.put("userId", map.get("userId"));
			map1.put("productType", map.get("productType"));
			IfsCfetsFlowUser flowUser = cfetsFlowUser.queryCfetsFlowUserById(map1);
			if(flowUser!=null){
				cfetsFlowUser.updateFlowUser(map);
			}
		}
		return RetMsgHelper.ok();
	}
	/**
	 * 删除
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteFlowUser")
	public RetMsg<Serializable> deleteFlowUser(@RequestBody Map<String, Object> map) {
		cfetsFlowUser.deleteFlowUser(map);
		return RetMsgHelper.ok();
	}
	
}
