package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.ifs.model.IfsDealDeskReport;
import com.singlee.ifs.service.IFSService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller
@RequestMapping(value = "/IfsDealDeskReportController")
public class IfsDealDeskReportController {
	
	@Autowired
	private IFSService iFSService;
	
	/**
	 * 交易台账
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageDealDesk")
	public RetMsg<PageInfo<IfsDealDeskReport>> searchPageDealDesk(@RequestBody Map<String, Object> params) {
		//TODO 只能查看我参与的交易
		params.put("flag", "1");//本人发起
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsDealDeskReport> page = iFSService.searchPageDealDesk(params);
		if(page.size()==0){
			params.put("flag", "2");//审批人
			page = iFSService.searchPageDealDesk(params);
		}
		return RetMsgHelper.ok(page);
	}
}
