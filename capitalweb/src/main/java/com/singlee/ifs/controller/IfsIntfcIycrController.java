package com.singlee.ifs.controller;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.github.pagehelper.Page;
import com.singlee.capital.base.util.FileManage;
import com.singlee.capital.chois.util.LutouUtil;
import com.singlee.capital.choistrd.model.SlIntfcGent;
import com.singlee.capital.choistrd.model.SlIycrGent;
import com.singlee.capital.choistrd.model.SlMktStatus;
import com.singlee.capital.choistrd.service.SlIntfcGentService;
import com.singlee.capital.choistrd.service.impl.MktTmaxCommand;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.UploadedFile;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.PropertiesUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.hrbreport.util.HrbReportUtils;
import com.singlee.ifs.model.IfsIntfcIycrBean;
import com.singlee.ifs.service.IfsIntfcIycrService;

import jxl.Sheet;
import jxl.Workbook;

@Controller
@RequestMapping(value = "/IfsIntfcIycrController")
public class IfsIntfcIycrController extends CommonController {
	
	@Autowired
	IfsIntfcIycrService ifsIntfcIycrService;
	
	@Autowired
	SlIntfcGentService slIntfcGentService;
	
	@Autowired
	MktTmaxCommand mktTmaxCommand;
	
	@Autowired
	IfsIntfcIrevController ifsIntfcIrevController;
	private static Logger logManager = LoggerFactory.getLogger(IfsIntfcIycrController.class);
	
	/***
     * 牌价信息分页查询
     */
    @ResponseBody
    @RequestMapping(value = "/searchIycr")
    public RetMsg<PageInfo<IfsIntfcIycrBean>> searchIycr(@RequestBody Map<String, Object> map) {

        List<IfsIntfcIycrBean> list = ifsIntfcIycrService.getlistIycr(map);
        int pageNumber = (int) map.get("pageNumber");
        int pageSize = (int) map.get("pageSize");
        Page<IfsIntfcIycrBean> page = HrbReportUtils.producePage(list, pageNumber, pageSize);

        return RetMsgHelper.ok(page);
    }
    
    
    /**
     * 市场数据 Excel上传外汇远掉点信息
     */
    @ResponseBody
    @RequestMapping(value = "/ImportIycrByExcel", method = RequestMethod.POST, produces = "text/plain;charset=UTF-8")
    public String ImportIycrByExcel(MultipartHttpServletRequest request, HttpServletResponse response) {
        // 1.文件获取
        List<UploadedFile> uploadedFileList = null;
        String ret = "导入失败";
        Workbook wb = null;
        List<IfsIntfcIycrBean> iycrBean = new ArrayList<>();
        try {
            uploadedFileList = FileManage.requestExtractor(request);
            // 2.校验并处理成excel
            List<Workbook> excelList = new LinkedList<Workbook>();
            for (UploadedFile uploadedFile : uploadedFileList) {
                if (!"xls".equals(uploadedFile.getType())) {
                    return ret = "上传文件类型错误,仅允许上传xls格式文件";
                }
                Workbook book = Workbook.getWorkbook(new ByteArrayInputStream(
                        uploadedFile.getBytes()));
                excelList.add(book);
            }
            if (excelList.size() != 1) {
                return ret = "只能处理一份excel";
            }
            wb = excelList.get(0);
            Sheet[] sheets = wb.getSheets();// 获得卡片sheet数量
            String lstmntdte= DateUtil.getCurrentDateAsString("yyyy-MM-dd");
            for (Sheet sheet : sheets) {
            	IfsIntfcIycrBean ifsIntfcIycrBean = null;
            	if(sheet.getName().equals("MKTDATA")) {
            		int rowCount = sheet.getRows();// 行数
            		for (int i = 3; i < rowCount; i++) { // rows=1开始 第二行开始读取，第一行为列名
            			if(StringUtils.trimToEmpty(sheet.getCell(22, i).getContents()).equals("")){
                            continue;
                        }
            			ifsIntfcIycrBean = new IfsIntfcIycrBean();
                        ifsIntfcIycrBean.setLstmntdte(lstmntdte);
                        ifsIntfcIycrBean.setBr("01");
                        ifsIntfcIycrBean.setCcy(StringUtils.trimToEmpty(sheet.getCell(15, i).getContents()));//P
                        ifsIntfcIycrBean.setYieldcurve(StringUtils.trimToEmpty(sheet
                                .getCell(24, i).getContents()));
                        ifsIntfcIycrBean.setMtyend(StringUtils.trimToEmpty(sheet
                                .getCell(23, i).getContents()));
                        ifsIntfcIycrBean.setRatecode(StringUtils.trimToEmpty(sheet
                                .getCell(22, i).getContents()));
                        ifsIntfcIycrBean.setBidrate_8("0");
                        ifsIntfcIycrBean.setMidrate_8(StringUtils.trimToEmpty(sheet
                                .getCell(9, i).getContents()));
                        ifsIntfcIycrBean.setOfferrate_8("0");
                        ifsIntfcIycrBean.setTradedate(StringUtils.trimToEmpty(sheet
                                .getCell(8, i).getContents()));
                        ifsIntfcIycrBean.setRic(StringUtils.trimToEmpty(sheet
                                .getCell(2, i).getContents()));
                        ifsIntfcIycrBean.setDefined_identifier2(StringUtils.trimToEmpty(sheet
                                .getCell(16, i).getContents()));
                        ifsIntfcIycrBean.setDefined_identifier(StringUtils.trimToEmpty(sheet
                                .getCell(15, i).getContents()));
                        ifsIntfcIycrBean.setSourse("MKTDATA");
                        ifsIntfcIycrBean.setRemark("");
                        
                        iycrBean.add(ifsIntfcIycrBean);
                        
            		}
            	}else if(sheet.getName().equals("BONDDATA")) {
            		int rowCount = sheet.getRows();// 行数
            		for (int i = 1; i < rowCount; i++) { // rows=1开始 第二行开始读取，第一行为列名
                         ifsIntfcIycrBean = new IfsIntfcIycrBean();
                         ifsIntfcIycrBean.setLstmntdte(lstmntdte);
                         ifsIntfcIycrBean.setBr("01");
                         ifsIntfcIycrBean.setCcy(StringUtils.trimToEmpty(sheet.getCell(6, i).getContents()).substring(0, 3));//G
                         ifsIntfcIycrBean.setYieldcurve(StringUtils.trimToEmpty(sheet
                                 .getCell(8, i).getContents()));
                         ifsIntfcIycrBean.setMtyend(StringUtils.trimToEmpty(sheet
                                 .getCell(7, i).getContents()));
                         ifsIntfcIycrBean.setRatecode(StringUtils.trimToEmpty(sheet
                                 .getCell(6, i).getContents()));
                         ifsIntfcIycrBean.setBidrate_8("0");
                         ifsIntfcIycrBean.setMidrate_8(StringUtils.trimToEmpty(sheet
                                 .getCell(4, i).getContents()));
                         ifsIntfcIycrBean.setOfferrate_8("0");
                         ifsIntfcIycrBean.setTradedate(StringUtils.trimToEmpty(sheet
                                 .getCell(0, i).getContents()));
                         ifsIntfcIycrBean.setRic(StringUtils.trimToEmpty(sheet
                                 .getCell(1, i).getContents()));
                         ifsIntfcIycrBean.setDefined_identifier2(StringUtils.trimToEmpty(sheet
                                 .getCell(2, i).getContents()));
                         ifsIntfcIycrBean.setDefined_identifier(StringUtils.trimToEmpty(sheet
                                 .getCell(3, i).getContents()));
                         ifsIntfcIycrBean.setSourse("BONDDATA");
                         ifsIntfcIycrBean.setRemark("");
                         
                         iycrBean.add(ifsIntfcIycrBean);
                     }
            	}else if(sheet.getName().equals("CRS")) {
            		int rowCount1 = 7;// 行数
            		for (int i = 3; i < rowCount1; i++) { // rows=1开始 第二行开始读取，第一行为列名
                        /*if(StringUtils.trimToEmpty(sheet.getCell(1, i).getContents()).equals("")){
                            continue;
                        }*/
                        ifsIntfcIycrBean = new IfsIntfcIycrBean();
                        ifsIntfcIycrBean.setLstmntdte(lstmntdte);
                        
                        ifsIntfcIycrBean.setBr("01");
                        ifsIntfcIycrBean.setCcy("USD");//G
                        ifsIntfcIycrBean.setYieldcurve("USDCRSMM"+StringUtils.trimToEmpty(sheet
                                .getCell(0, i).getContents()));
                        ifsIntfcIycrBean.setMtyend(StringUtils.trimToEmpty(sheet
                                .getCell(0, i).getContents()));
                        ifsIntfcIycrBean.setRatecode("USDCRSMM");
                        ifsIntfcIycrBean.setBidrate_8("0");
                        ifsIntfcIycrBean.setMidrate_8(StringUtils.trimToEmpty(sheet
                                .getCell(4, i).getContents()));
                        ifsIntfcIycrBean.setOfferrate_8("0");
                        ifsIntfcIycrBean.setTradedate("");
                        ifsIntfcIycrBean.setRic("");
                        ifsIntfcIycrBean.setDefined_identifier2("");
                        ifsIntfcIycrBean.setDefined_identifier("");
                        ifsIntfcIycrBean.setSourse("CRS");
                        ifsIntfcIycrBean.setRemark("");
                        
                        iycrBean.add(ifsIntfcIycrBean);
                    }
            		
            		int rowCount = 14;// 行数
            		for (int i = 7; i < rowCount; i++) { // rows=1开始 第二行开始读取，第一行为列名
                         /*if(StringUtils.trimToEmpty(sheet.getCell(1, i).getContents()).equals("")){
                             continue;
                         }*/
                         ifsIntfcIycrBean = new IfsIntfcIycrBean();
                         ifsIntfcIycrBean.setLstmntdte(lstmntdte);
                         
                         ifsIntfcIycrBean.setBr("01");
                         ifsIntfcIycrBean.setCcy("USD");//G
                         ifsIntfcIycrBean.setYieldcurve("USD3MCRS"+StringUtils.trimToEmpty(sheet
                                 .getCell(0, i).getContents()));
                         ifsIntfcIycrBean.setMtyend(StringUtils.trimToEmpty(sheet
                                 .getCell(0, i).getContents()));
                         ifsIntfcIycrBean.setRatecode("USD3MCRS");
                         ifsIntfcIycrBean.setBidrate_8("0");
                         ifsIntfcIycrBean.setMidrate_8(StringUtils.trimToEmpty(sheet
                                 .getCell(4, i).getContents()));
                         ifsIntfcIycrBean.setOfferrate_8("0");
                         ifsIntfcIycrBean.setTradedate("");
                         ifsIntfcIycrBean.setRic("");
                         ifsIntfcIycrBean.setDefined_identifier2("");
                         ifsIntfcIycrBean.setDefined_identifier("");
                         ifsIntfcIycrBean.setSourse("CRS");
                         ifsIntfcIycrBean.setRemark("");
                         
                         iycrBean.add(ifsIntfcIycrBean);
                     }
            	}
            	
            }
            ret = ifsIntfcIycrService.doBussExcelPropertyToDataBase(iycrBean);
            //插入inth表和iycr
            String flag=  ifsIntfcIycrService.dataBaseToIycr(lstmntdte);
          //插入inth表和irhs
            String flag1=  ifsIntfcIycrService.dataBaseToIrhs(lstmntdte);
            
            if("SUCCESS".equals(flag)&&"SUCCESS".equals(flag1)){
                ret=ret+"且当天收益率曲线数据同步至OPICS成功";
            }else {
                ret=ret+"且当天收益率曲线数据同步至OPICS失败";
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw new RException(e);
        } finally {
            if (null != wb) {
                wb.close();
            }
        }
        return ret;// 返回前台展示配置
    }
    public void getMuH() throws Exception {
    	List<IfsIntfcIycrBean> iycrBean = new ArrayList<>();
        String ret = "导入失败";
       
	     String lstmntdte= DateUtil.getCurrentDateAsString("yyyy-MM-dd");
	     String makeIl= DateUtil.getCurrentDateAsString("yyyy/MM/dd");
	     //String makeIl= "2023/08/30";
	     IfsIntfcIycrBean ifsIntfcIycrBean = null;
	     ifsIntfcIycrBean = new IfsIntfcIycrBean();
         ifsIntfcIycrBean.setLstmntdte(lstmntdte);
         ifsIntfcIycrBean.setBr("01");
	   //母行数据
	     Map<String, Object> kmap =new HashMap<String, Object>();
	     kmap.put("makeIl", makeIl);
	     List<SlMktStatus> slMkts=slIntfcGentService.queryMkt(kmap);
	     
	     if(slMkts.size()>0) {
	    	 for(SlMktStatus mkt : slMkts) {
	    		 System.out.println("CCY-----"+mkt.getCcy());
	    		 System.out.println("TYPE-----"+mkt.getRtType());
	    		 System.out.println("MTYEND-----"+mkt.getMtyend());
	    		 if(!mkt.getRtType().trim().equals("BSBY") ) {
	    			 if(!mkt.getCcy().trim().equals("CAD")) {
		    			 ifsIntfcIycrBean.setYieldcurve(mkt.getCcy()+"MMKT"+mkt.getMtyend());                 
	                     ifsIntfcIycrBean.setRatecode(mkt.getCcy()+"MMKT");
	                     ifsIntfcIycrBean.setCcy(mkt.getCcy());//G
	                     ifsIntfcIycrBean.setMtyend(mkt.getMtyend());
	                     ifsIntfcIycrBean.setBidrate_8("0");
			             ifsIntfcIycrBean.setMidrate_8(mkt.getPrice());
			             ifsIntfcIycrBean.setOfferrate_8("0");
			             ifsIntfcIycrBean.setTradedate(mkt.getMakeIl());
			             ifsIntfcIycrBean.setRic("");
			             ifsIntfcIycrBean.setDefined_identifier2("");
			             ifsIntfcIycrBean.setDefined_identifier("");
			             ifsIntfcIycrBean.setSourse("MKTDATA");
			             ifsIntfcIycrBean.setRemark("");
			                
			                iycrBean.add(ifsIntfcIycrBean);
			                ret = ifsIntfcIycrService.doBussExcelPropertyToDataBase(iycrBean);
			   	    	 
	    			 }
	    		 }
	    		
		           
	    	 }
	    	//插入inth表和iycr
	            String flag=  ifsIntfcIycrService.dataBaseToIycr(lstmntdte);
	          //插入inth表和irhs
	            String flag1=  ifsIntfcIycrService.dataBaseToIrhs(lstmntdte); 
	     }
    }
    public void getMktdata() throws Exception {
    	List<IfsIntfcIycrBean> iycrBean = new ArrayList<>();
        String ret = "导入失败";
        Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		//calendar.add(Calendar.DATE, -1);
		String sdate = null;
		sdate = format.format(calendar.getTime());
		//sdate = "20231122";
		PropertiesConfiguration stdtmsgSysValue;
		stdtmsgSysValue = PropertiesUtil.parseFile("sftp.properties");
		String lsLocalPath=null;
		String name=null;
		lsLocalPath= stdtmsgSysValue.getString("lsLocalPath");
		name= stdtmsgSysValue.getString("MKTDATA_NEW");
		
		 InputStreamReader isr = null;
	     BufferedReader br = null;
	     String lstmntdte= DateUtil.getCurrentDateAsString("yyyy-MM-dd");
	     String makeIl= DateUtil.getCurrentDateAsString("yyyy/MM/dd");
	     IfsIntfcIycrBean ifsIntfcIycrBean = null;
	     ifsIntfcIycrBean = new IfsIntfcIycrBean();
         ifsIntfcIycrBean.setLstmntdte(lstmntdte);
         ifsIntfcIycrBean.setBr("01");
	   //母行数据
	    /* Map<String, Object> kmap =new HashMap<String, Object>();
	     kmap.put("makeIl", makeIl);
	     List<SlMktStatus> slMkts=slIntfcGentService.queryMkt(kmap);
	     
	     if(slMkts.size()>0) {
	    	 for(SlMktStatus mkt : slMkts) {
	    		 System.out.println("CCY-----"+mkt.getCcy());
	    		 System.out.println("TYPE-----"+mkt.getRtType());
	    		 if(!mkt.getRtType().trim().equals("BSBY") ) {
	    			 if(!mkt.getCcy().trim().equals("CAD")) {
		    			 ifsIntfcIycrBean.setYieldcurve(mkt.getCcy()+"MMKT"+mkt.getMtyend());                 
	                     ifsIntfcIycrBean.setRatecode(mkt.getCcy()+"MMKT");
	                     ifsIntfcIycrBean.setCcy(mkt.getCcy());//G
	                     ifsIntfcIycrBean.setMtyend(mkt.getMtyend());
	                     ifsIntfcIycrBean.setBidrate_8("0");
			             ifsIntfcIycrBean.setMidrate_8(mkt.getPrice());
			             ifsIntfcIycrBean.setOfferrate_8("0");
			             ifsIntfcIycrBean.setTradedate(mkt.getMakeIl());
			             ifsIntfcIycrBean.setRic("");
			             ifsIntfcIycrBean.setDefined_identifier2("");
			             ifsIntfcIycrBean.setDefined_identifier("");
			             ifsIntfcIycrBean.setSourse("MKTDATA");
			             ifsIntfcIycrBean.setRemark("");
			                
			                iycrBean.add(ifsIntfcIycrBean);
			                
	    			 }
	    		 }
	    		
		            //插入inth表和iycr
		           // String flag=  ifsIntfcIycrService.dataBaseToIycr(lstmntdte);
		          //插入inth表和irhs
		           // String flag1=  ifsIntfcIycrService.dataBaseToIrhs(lstmntdte);
	    	 }
	    	 ret = ifsIntfcIycrService.doBussExcelPropertyToDataBase(iycrBean);
	            
	     }*/
	     
	     
	   //MKTDATA
	     File[] list = new File(lsLocalPath).listFiles();
	     for(File file : list)
	        {
	           if(file.isFile())
	           {
	               if (file.getName().contains(name+sdate+".csv")) {
	                   // 就输出该文件的绝对路径
	                   System.out.println(file.getName());
	                   File mkdata = new File(lsLocalPath + file.getName());
	          		 mkdata.setReadable(true);
	          		 mkdata.setWritable(true);
	          		 if (!mkdata.exists()) {
	          				logManager.info("========" + mkdata + ":文件不存在=========");
	          	            
	          		}else {
	          				try {
	          		            isr = new InputStreamReader(new FileInputStream(mkdata), "UTF-8");
	          		            br = new BufferedReader(isr);
	          		        } catch (Exception e) {
	          		            e.printStackTrace();
	          		        }
	          		        String line = "";
	          		        ArrayList<String> records = new ArrayList<>();
	          		        String bidrate_8=null;
	          		        String midrate_8=null;
	          		        try {
	          		        	
	          		            while ((line = br.readLine()) != null) {
	          		            	String[] names = line.split(",(?=([^\\\"]*\\\"[^\\\"]*\\\")*[^\\\"]*$)",-1);
	          		            	if(names[0].equals("REUTERS MKT DATA") || names[0].equals("") || names[0].equals("Instrument ID")) {
	          		                	logManager.info("========names:" + names + "=========");
	          		                } else {

	          		                	System.out.println(line);
		          		                System.out.println("names[0]:"+names[0].trim());
		          		                System.out.println("names[9]:"+names[9].trim());
		          		                System.out.println("names[14]:"+names[14].trim());
		          		                System.out.println("names[17]:"+names[17].trim());
		          		                if(names[4].trim().equals(" ") || names[4].trim()==null) {
		          		                	System.out.println("names[4]:"+"null");
		          		                }else {
		          		                	System.out.println("names[4]:"+names[4].trim());
		          		                }
	          		                Map<String, Object> map = new HashMap<String, Object>();
	          		        		map.put("operid", names[2].trim());
	          		                List<SlIntfcGent> gents = slIntfcGentService.queryGent(map);
	          		                
	          		                Map<String, Object> iycrmap = new HashMap<String, Object>();
	          		                iycrmap.put("ric", names[2].trim());
	          		                List<SlIycrGent> iycrgents = slIntfcGentService.queryIycrGent(iycrmap);
	          		                
	          		               
	          		                if(gents.size()>0) {
	          		                	for(SlIntfcGent intfcGent:gents) {
	          		                		
	          		                		 ifsIntfcIycrBean.setYieldcurve(StringUtils.trimToEmpty(intfcGent.getPart2()));
	          		                       
	          		                        ifsIntfcIycrBean.setRatecode(StringUtils.trimToEmpty(intfcGent.getObject()));
	          		                    }
	          		                	
	          		                	 if(iycrgents.size()>0) {
	  	          		            	   for(SlIycrGent iycrGent:iycrgents) {
	  	          		            		   ifsIntfcIycrBean.setMtyend(StringUtils.trimToEmpty(iycrGent.getMtyend()));
	  	          		            		 ifsIntfcIycrBean.setCcy(StringUtils.trimToEmpty(iycrGent.getCcy()));//G
	  		          	                       
	  	          		            	   }
	  	          		               }
	  	          		               
	  	          		                bidrate_8=names[17].trim().replace("\"", "").replace(",", "");
	  	          		                midrate_8=names[9].trim().replace("\"", "").replace(",", "");
	  	          		                ifsIntfcIycrBean.setBidrate_8(StringUtils.trimToEmpty(bidrate_8));
	  	          		                ifsIntfcIycrBean.setMidrate_8(StringUtils.trimToEmpty(midrate_8));
	  	          		                ifsIntfcIycrBean.setOfferrate_8("0");
	  	          		                ifsIntfcIycrBean.setTradedate(StringUtils.trimToEmpty(names[14].trim()));
	  	          		                ifsIntfcIycrBean.setRic(StringUtils.trimToEmpty(names[2].trim()));
	  	          		                ifsIntfcIycrBean.setDefined_identifier2(StringUtils.trimToEmpty(names[0].trim()));
	  	          		                ifsIntfcIycrBean.setDefined_identifier(StringUtils.trimToEmpty(names[1].trim()));
	  	          		                ifsIntfcIycrBean.setSourse("MKTDATA");
	  	          		                ifsIntfcIycrBean.setRemark("");
	  	          		                
	  	          		                iycrBean.add(ifsIntfcIycrBean);
	  	          		                records.add(line);
	  	          		            ret = ifsIntfcIycrService.doBussExcelPropertyToDataBase(iycrBean);
	  	          		           	
	          		                }
	          		              
	          		              if(names[2].trim().equals("CNY=")) {
		  	          		            Map<String, Object> insMap1 =new HashMap<String, Object>();
			  	          		        insMap1.put("makeIl", lstmntdte);
			  							insMap1.put("rtType","SOFR");
			  							insMap1.put("ccy", "USD");
			  							insMap1.put("mtyend", "HD");
			  							insMap1.put("price", names[7].trim().replace("\"", "").replace(",", ""));
			  							List<SlMktStatus> slMkts1=slIntfcGentService.queryMkt(insMap1);
			  							//slMkt.setMtyend("1D");
			  							if(slMkts1.size()>0) {
			  								slIntfcGentService.deleteMkt(insMap1);
			  								
			  							}
			  							slIntfcGentService.insertMkt(insMap1);	
		  	          		         }
	          		            }
	          		            	
	          		          }
	          		         
          	 				  
	          		            //插入inth表和iycr
	          		            String flag=  ifsIntfcIycrService.dataBaseToIycr(lstmntdte);
	          		          //插入inth表和irhs
	          		            String flag1=  ifsIntfcIycrService.dataBaseToIrhs(lstmntdte);
	          		            
	          		            if("SUCCESS".equals(flag)&&"SUCCESS".equals(flag1)){
	          		                ret=ret+"且当天收益率曲线数据同步至OPICS成功";
	          		            }else {
	          		                ret=ret+"且当天收益率曲线数据同步至OPICS失败";
	          		            }
	          		            System.out.println("csv表格读取行数：" + records.size());
	          		        } catch (IOException e) {
	          		            e.printStackTrace();
	          		        }
	          			}
	               }
	               
	    
	   
	           }
	        }
    }
    //收益率曲线
    public void getSyl() throws Exception {
    	List<IfsIntfcIycrBean> iycrBean = new ArrayList<>();
        String ret = "导入失败";
        Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		//calendar.add(Calendar.DATE, -1);
		String sdate = null;
		sdate = format.format(calendar.getTime());
		//sdate ="20230830";
        PropertiesConfiguration stdtmsgSysValue;
		stdtmsgSysValue = PropertiesUtil.parseFile("sftp.properties");
		String sylpath=null;
		String sylname=null;
		sylpath= stdtmsgSysValue.getString("sylLocalPath");
		sylname= stdtmsgSysValue.getString("sylname");
		
		 
	     String lstmntdte= DateUtil.getCurrentDateAsString("yyyy-MM-dd");
	     IfsIntfcIycrBean ifsIntfcIycrBean = null;
	     ifsIntfcIycrBean = new IfsIntfcIycrBean();
         ifsIntfcIycrBean.setLstmntdte(lstmntdte);
         ifsIntfcIycrBean.setBr("01");
    	//收益率曲线
	     File syl = new File(sylpath + "//" + "收益率曲线"+sdate+".xlsx");
	     syl.setReadable(true);
	     syl.setWritable(true);
	     
	     if (!syl.exists()) {
				logManager.info("========" + syl + ":文件不存在=========");
	            
		}else {
			// Workbook wb = null; 
			List<Workbook> excelList = new LinkedList<Workbook>();
			
	           InputStream is = new FileInputStream(syl);
	          // Workbook book = Workbook.getWorkbook(is);
	           XSSFWorkbook wb=new XSSFWorkbook(is);
	           XSSFSheet sheet=wb.getSheetAt(1);
	          // excelList.add(book);
	          
	           if (excelList.size() != 1) {
	        	   logManager.info("只能处理一份excel");
	           }
	           //wb = excelList.get(0);
	           //Sheet sheet = wb.getSheet(1);// 获得第一个sheet页
	           
	           
	           //int rowCount = sheet.getRows();// 行数
	           String mtyend=null;
	           String yieldcurve=null;
	           
	         //USD  
	          /* for (int i = 3; i <= 18; i++) {
	        	   if(StringUtils.trimToEmpty(sheet.getRow(i).getCell(2).toString()).equals("ON")) {
	        		   mtyend="1D";
	        		   yieldcurve="USDMMKT1D";
	        		   
	        		   ifsIntfcIycrBean.setYieldcurve(yieldcurve);
	                   
	        		   ifsIntfcIycrBean.setMtyend(StringUtils.trimToEmpty(mtyend));
	        	   }else {
	        		   ifsIntfcIycrBean.setYieldcurve("USDMMKT"+StringUtils.trimToEmpty(sheet.getRow(i).getCell(2).toString()));
	                   
	        		   ifsIntfcIycrBean.setMtyend(StringUtils.trimToEmpty(sheet.getRow(i).getCell(2).toString()));
	        	   }
	        	  // ifsIntfcIycrBean.setYieldcurve("USDMMKT");
                   
                   ifsIntfcIycrBean.setRatecode("USDMMKT");
                   ifsIntfcIycrBean.setCcy("USD");//G
                   
	               
	               // ifsIntfcIycrBean.setMtyend(StringUtils.trimToEmpty("1D"));
	                
	                ifsIntfcIycrBean.setBidrate_8("0");
	                ifsIntfcIycrBean.setMidrate_8(StringUtils.trimToEmpty(sheet.getRow(i).getCell(5).toString()));
	                ifsIntfcIycrBean.setOfferrate_8("0");
	                ifsIntfcIycrBean.setTradedate(lstmntdte);
	                ifsIntfcIycrBean.setRic(StringUtils.trimToEmpty(sheet.getRow(i).getCell(4).toString()));
	                ifsIntfcIycrBean.setDefined_identifier2(StringUtils.trimToEmpty(sheet.getRow(i).getCell(4).toString()));
	                ifsIntfcIycrBean.setDefined_identifier("");
	                ifsIntfcIycrBean.setSourse("MKTDATA");
	                ifsIntfcIycrBean.setRemark("");
	                
	                iycrBean.add(ifsIntfcIycrBean);
	                ret = ifsIntfcIycrService.doBussExcelPropertyToDataBase(iycrBean);
	           }*/
	         
	            //插入inth表和iycr
	           /* String flag=  ifsIntfcIycrService.dataBaseToIycr(lstmntdte);
	          //插入inth表和irhs
	            String flag1=  ifsIntfcIycrService.dataBaseToIrhs(lstmntdte);
	            
	            if("SUCCESS".equals(flag)&&"SUCCESS".equals(flag1)){
	                ret=ret+"且当天收益率曲线数据同步至OPICS成功";
	            }else {
	                ret=ret+"且当天收益率曲线数据同步至OPICS失败";
	            }*/
	            
	            //CNY
	            for (int i = 3; i <= 18; i++) {
		        	   if(StringUtils.trimToEmpty(sheet.getRow(i).getCell(2).toString()).equals("ON")) {
		        		   mtyend="1D";
		        		   yieldcurve="CNYMMKT1D";
		        		   
		        		   ifsIntfcIycrBean.setYieldcurve(yieldcurve);
		                   
		        		   ifsIntfcIycrBean.setMtyend(StringUtils.trimToEmpty(mtyend));
		        	   }else {
		        		   ifsIntfcIycrBean.setYieldcurve("CNYMMKT"+StringUtils.trimToEmpty(sheet.getRow(i).getCell(2).toString()));
		                   
		        		   ifsIntfcIycrBean.setMtyend(StringUtils.trimToEmpty(sheet.getRow(i).getCell(2).toString()));
		        	   }
		        	  // ifsIntfcIycrBean.setYieldcurve("USDMMKT");
	                   
	                   ifsIntfcIycrBean.setRatecode("CNYMMKT");
	                   ifsIntfcIycrBean.setCcy("CNY");//G
	                   
		               
		               // ifsIntfcIycrBean.setMtyend(StringUtils.trimToEmpty("1D"));
		                String mid=Double.toString(Double.parseDouble(sheet.getRow(i).getCell(10).toString().replace("%",""))*100);
		                ifsIntfcIycrBean.setBidrate_8("0");
		                ifsIntfcIycrBean.setMidrate_8(StringUtils.trimToEmpty(mid));
		                ifsIntfcIycrBean.setOfferrate_8("0");
		                ifsIntfcIycrBean.setTradedate(lstmntdte);
		                ifsIntfcIycrBean.setRic(StringUtils.trimToEmpty(sheet.getRow(i).getCell(7).toString()));
		                ifsIntfcIycrBean.setDefined_identifier2(StringUtils.trimToEmpty(sheet.getRow(i).getCell(7).toString()));
		                ifsIntfcIycrBean.setDefined_identifier("");
		                ifsIntfcIycrBean.setSourse("MKTDATA");
		                ifsIntfcIycrBean.setRemark("");
		                
		                iycrBean.add(ifsIntfcIycrBean);
		                
		                ret = ifsIntfcIycrService.doBussExcelPropertyToDataBase(iycrBean);
		           }
		         
		            //插入inth表和iycr
		            String flag2=  ifsIntfcIycrService.dataBaseToIycr(lstmntdte);
		          //插入inth表和irhs
		            String flag3=  ifsIntfcIycrService.dataBaseToIrhs(lstmntdte);
		            
		            if("SUCCESS".equals(flag2)&&"SUCCESS".equals(flag3)){
		                ret=ret+"且当天收益率曲线数据同步至OPICS成功";
		            }else {
		                ret=ret+"且当天收益率曲线数据同步至OPICS失败";
		            }
	        	
		}
    }
    
    public void getCfx() throws Exception {
    	 List<IfsIntfcIycrBean> iycrBean = new ArrayList<>();
         String ret = "导入失败";
         Date date = new Date();
 		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
 		Calendar calendar = Calendar.getInstance();
 		calendar.setTime(date);
 		//calendar.add(Calendar.DATE, -1);
 		String sdate = null;
 		sdate = format.format(calendar.getTime());
 		//sdate = "20230830";
         PropertiesConfiguration stdtmsgSysValue;
 		stdtmsgSysValue = PropertiesUtil.parseFile("sftp.properties");
 		String lsLocalPath=null;
 		String name=null;
 		lsLocalPath= stdtmsgSysValue.getString("lsLocalPath");
 		name= stdtmsgSysValue.getString("CFX_NEW");
 		
 		 InputStreamReader isr = null;
 	     BufferedReader br = null;
 	     String lstmntdte= DateUtil.getCurrentDateAsString("yyyy-MM-dd");
 	     
 	    File[] list = new File(lsLocalPath).listFiles();
 	   for(File file : list)
       {
 		  if(file.isFile())
          {
 			 if (file.getName().contains(name+sdate+".csv")) {
 				 //CFX
 		         File bonddata = new File(lsLocalPath + file.getName());
 		         bonddata.setReadable(true);
 		         bonddata.setWritable(true);
 		         
 		        
 		         if (!bonddata.exists()) {
 		 			logManager.info("========" + bonddata + ":文件不存在=========");
 		             
 		 		}else {
 		 			try {
 		 	            isr = new InputStreamReader(new FileInputStream(bonddata), "UTF-8");
 		 	            br = new BufferedReader(isr);
 		 	        } catch (Exception e) {
 		 	            e.printStackTrace();
 		 	        }
 		 	        String line = "";
 		 	        ArrayList<String> records = new ArrayList<>();
 		 	        try {
 		 	        	IfsIntfcIycrBean ifsIntfcIycrBean = null;
 		 	            while ((line = br.readLine()) != null) {
 		 	            	String[] names = line.split(",",-1);
 		 	            	
 		 	                System.out.println(line);
 		 	                System.out.println("names[0]:"+names[0].trim());
 		 	                System.out.println("names[1]:"+names[1].trim());
 		 	                System.out.println("names[2]:"+names[2].trim());
 		 	                System.out.println("names[3]:"+names[3].trim());
 		 	              if(names[4].trim().equals(" ") || names[4].trim()==null) {
 			                	System.out.println("names[4]:"+"null");
 			                }else {
 			                	System.out.println("names[4]:"+names[4].trim());
 			                }
 			                
 			                if(names[0].equals("Instrument ID")) {
 			                	logManager.info("========names:" + names + "=========");
 			                }else {
 			                
 			                Map<String, Object> map = new HashMap<String, Object>();
 			        		map.put("operid", names[0].trim());
 			                List<SlIntfcGent> gents = slIntfcGentService.queryGent(map);
 			                ifsIntfcIycrBean = new IfsIntfcIycrBean();
 			                ifsIntfcIycrBean.setLstmntdte(lstmntdte);
 			                ifsIntfcIycrBean.setBr("01");
 			                if(gents.size()>0) {
 			                	for(SlIntfcGent intfcGent:gents) {
 			                		
 			                		 ifsIntfcIycrBean.setYieldcurve(StringUtils.trimToEmpty(intfcGent.getPart2()));
 			                       
 			                        ifsIntfcIycrBean.setRatecode(StringUtils.trimToEmpty(intfcGent.getObject()));
 			                    }
 			                	
 			                	ifsIntfcIycrBean.setCcy("CNY");//G
 	 			                
 	 			               Map<String, Object> iycrmap = new HashMap<String, Object>();
 	     		                iycrmap.put("ric", names[0].trim());
 	     		                List<SlIycrGent> iycrgents = slIntfcGentService.queryIycrGent(iycrmap);
 	     		               
 	     		                if(iycrgents.size()>0) {
 	      		            	   for(SlIycrGent iycrGent:iycrgents) {
 	      		            		   ifsIntfcIycrBean.setMtyend(StringUtils.trimToEmpty(iycrGent.getMtyend()));
 	      		            	   }
 	      		               }
 	 			                ifsIntfcIycrBean.setBidrate_8(StringUtils.trimToEmpty(names[4].trim()));
 	 			                ifsIntfcIycrBean.setMidrate_8(StringUtils.trimToEmpty(names[4].trim()));
 	 			                ifsIntfcIycrBean.setOfferrate_8("0");
 	 			                ifsIntfcIycrBean.setTradedate(StringUtils.trimToEmpty(names[5].trim()));
 	 			                ifsIntfcIycrBean.setRic(StringUtils.trimToEmpty(names[0].trim()));
 	 			                ifsIntfcIycrBean.setDefined_identifier2("");
 	 			                ifsIntfcIycrBean.setDefined_identifier(StringUtils.trimToEmpty(names[1].trim()));
 	 			                ifsIntfcIycrBean.setSourse("CFX");
 	 			                ifsIntfcIycrBean.setRemark("");
 	 			                
 	 			                iycrBean.add(ifsIntfcIycrBean);
 	 			                records.add(line);
 			                }
 			               ret = ifsIntfcIycrService.doBussExcelPropertyToDataBase(iycrBean);
 			            }
 			           
 		 	           }
 		 	           
 		 	            //插入inth表和iycr
 		 	            String flag=  ifsIntfcIycrService.dataBaseToIycr(lstmntdte);
 		 	          //插入inth表和irhs
 		 	            String flag1=  ifsIntfcIycrService.dataBaseToIrhs(lstmntdte);
 		 	            
 		 	            if("SUCCESS".equals(flag)&&"SUCCESS".equals(flag1)){
 		 	                ret=ret+"且当天收益率曲线数据同步至OPICS成功";
 		 	            }else {
 		 	                ret=ret+"且当天收益率曲线数据同步至OPICS失败";
 		 	            }
 		 	            System.out.println("csv表格读取行数：" + records.size());
 		 	        } catch (IOException e) {
 		 	            e.printStackTrace();
 		 	        }
 		 		}
 			 }
          }
       }
 	 
    }
    
    public void getBondata() throws Exception {
   	 List<IfsIntfcIycrBean> iycrBean = new ArrayList<>();
        String ret = "导入失败";
        Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		//calendar.add(Calendar.DATE, -1);
		String sdate = null;
		sdate = format.format(calendar.getTime());
		//sdate = "20231122";
		PropertiesConfiguration stdtmsgSysValue;
		stdtmsgSysValue = PropertiesUtil.parseFile("sftp.properties");
		String lsLocalPath=null;
		String name=null;
		lsLocalPath= stdtmsgSysValue.getString("lsLocalPath");
		name= stdtmsgSysValue.getString("BONDDATA_NEW");
		
		 InputStreamReader isr = null;
	     BufferedReader br = null;
	     String lstmntdte= DateUtil.getCurrentDateAsString("yyyy-MM-dd");
	     
	    File[] list = new File(lsLocalPath).listFiles();
	   for(File file : list)
      {
		  if(file.isFile())
         {
			 if (file.getName().contains(name+sdate+".csv")) {
				 //BONDDATA
		         File bonddata = new File(lsLocalPath + file.getName());
		         bonddata.setReadable(true);
		         bonddata.setWritable(true);
		         
		        
		         if (!bonddata.exists()) {
		 			logManager.info("========" + bonddata + ":文件不存在=========");
		             
		 		}else {
		 			try {
		 	            isr = new InputStreamReader(new FileInputStream(bonddata), "UTF-8");
		 	            br = new BufferedReader(isr);
		 	        } catch (Exception e) {
		 	            e.printStackTrace();
		 	        }
		 	        String line = "";
		 	        ArrayList<String> records = new ArrayList<>();
		 	        try {
		 	        	IfsIntfcIycrBean ifsIntfcIycrBean = null;
		 	            while ((line = br.readLine()) != null) {
		 	            	String[] names = line.split(",",-1);
		 	            	
		 	                System.out.println(line);
		 	                System.out.println("names[0]:"+names[0].trim());
		 	                System.out.println("names[1]:"+names[1].trim());
		 	                System.out.println("names[2]:"+names[2].trim());
		 	                System.out.println("names[3]:"+names[3].trim());
		 	                if(names[4].trim().equals(" ") || names[4].trim()==null) {
		 	                	System.out.println("names[4]:"+"null");
		 	                }else {
		 	                	System.out.println("names[4]:"+names[4].trim());
		 	                }
		 	                
		 	                if(names[0].equals("Trade Date")) {
		 	                	logManager.info("========names:" + names + "=========");
		 	                }else {
		 	                	
		 	                
		 	                Map<String, Object> map = new HashMap<String, Object>();
		 	        		map.put("operid", names[1].trim());
		 	                List<SlIntfcGent> gents = slIntfcGentService.queryGent(map);
		 	                ifsIntfcIycrBean = new IfsIntfcIycrBean();
		 	                ifsIntfcIycrBean.setLstmntdte(lstmntdte);
		 	                ifsIntfcIycrBean.setBr("01");
		 	                if(gents.size()>0) {
		 	                	for(SlIntfcGent intfcGent:gents) {
		 	                		if(intfcGent.getPart2().substring(0, 2).equals("CP") || intfcGent.getPart2().substring(0, 2).equals("CB") || intfcGent.getPart2().substring(0, 2).equals("CR")|| intfcGent.getPart2().substring(0, 2).equals("AB") || intfcGent.getPart2().substring(0, 2).equals("LC")) {
		 	                			//intfcGent.setPart2("CNY");
		 	                			ifsIntfcIycrBean.setCcy("CNY");//G
		 	                		}else {
		 	                			ifsIntfcIycrBean.setCcy(StringUtils.trimToEmpty(intfcGent.getPart2()).substring(0, 3));
		 	                		}
		 	                		
		 	                        ifsIntfcIycrBean.setYieldcurve(StringUtils.trimToEmpty(intfcGent.getPart2()));
		 	                       
		 	                        ifsIntfcIycrBean.setRatecode(StringUtils.trimToEmpty(intfcGent.getObject()));
		 	                    }
		 	                }
		 	               Map<String, Object> iycrmap = new HashMap<String, Object>();
     		                iycrmap.put("ric", names[1].trim());
     		                List<SlIycrGent> iycrgents = slIntfcGentService.queryIycrGent(iycrmap);
     		                
     		               if(iycrgents.size()>0) {
      		            	   for(SlIycrGent iycrGent:iycrgents) {
      		            		   ifsIntfcIycrBean.setMtyend(StringUtils.trimToEmpty(iycrGent.getMtyend()));
      		            	   }
      		               }
      		               
		 	                ifsIntfcIycrBean.setBidrate_8("0");
		 	                ifsIntfcIycrBean.setMidrate_8(StringUtils.trimToEmpty(names[4].trim()));
		 	                ifsIntfcIycrBean.setOfferrate_8("0");
		 	                ifsIntfcIycrBean.setTradedate(StringUtils.trimToEmpty(names[0].trim()));
		 	                ifsIntfcIycrBean.setRic(StringUtils.trimToEmpty(names[1].trim()));
		 	                ifsIntfcIycrBean.setDefined_identifier2(StringUtils.trimToEmpty(names[2].trim()));
		 	                ifsIntfcIycrBean.setDefined_identifier(StringUtils.trimToEmpty(names[3].trim()));
		 	                ifsIntfcIycrBean.setSourse("BONDDATA");
		 	                ifsIntfcIycrBean.setRemark("");
		 	                
		 	                iycrBean.add(ifsIntfcIycrBean);
		 	                records.add(line);
		 	            }
		 	               
		 	           }
		 	           ret = ifsIntfcIycrService.doBussExcelPropertyToDataBase(iycrBean);
		 	            //插入inth表和iycr
	 	              String flag=  ifsIntfcIycrService.dataBaseToIycr(lstmntdte);
		 	          //插入inth表和irhs
		 	            String flag1=  ifsIntfcIycrService.dataBaseToIrhs(lstmntdte);
		 	            
		 	            if("SUCCESS".equals(flag)&&"SUCCESS".equals(flag1)){
		 	                ret=ret+"且当天收益率曲线数据同步至OPICS成功";
		 	            }else {
		 	                ret=ret+"且当天收益率曲线数据同步至OPICS失败";
		 	            }
		 	            System.out.println("csv表格读取行数：" + records.size());
		 	        } catch (IOException e) {
		 	            e.printStackTrace();
		 	        }
		 		}
			 }
         }
      }
	 
   }
    
    public void getCrs() throws Exception {
    	List<IfsIntfcIycrBean> iycrBean = new ArrayList<>();
        String ret = "导入失败";
        Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		//calendar.add(Calendar.DATE, -1);
		String sdate = null;
		sdate = format.format(calendar.getTime());
		//sdate = "20230830";
        PropertiesConfiguration stdtmsgSysValue;
		stdtmsgSysValue = PropertiesUtil.parseFile("sftp.properties");
		String lsLocalPath=null;
		String name=null;
		name= stdtmsgSysValue.getString("MKTDATA_NEW");
		lsLocalPath= stdtmsgSysValue.getString("lsLocalPath");
		
		 InputStreamReader isr = null;
	     BufferedReader br = null;
	     String lstmntdte= DateUtil.getCurrentDateAsString("yyyy/MM/dd");
	     String lstmntdte1= DateUtil.getCurrentDateAsString("yyyy-MM-dd");
	     Map<String, Object> map =new HashMap<String, Object>();
	     map.put("makeIl", lstmntdte);
	     map.put("mtyend", "3M");
	     map.put("ccy", "USD");
	     map.put("rtType", "SOFR");
	     List<SlMktStatus> mktStatus= slIntfcGentService.queryMkt(map);
	     
	     Map<String, Object> map1 =new HashMap<String, Object>();
	     map1.put("makeIl", lstmntdte1);
	     map1.put("mtyend", "HD");
	     map1.put("ccy", "USD");
	     map.put("rtType", "SOFR");
	     List<SlMktStatus> mktStatus1= slIntfcGentService.queryMkt(map1);
	     File[] list = new File(lsLocalPath).listFiles();
	 	   for(File file : list)
	       {
	 		  if(file.isFile())
	          {
	 			 if (file.getName().contains(name+sdate+".csv")) {
	 				 //CRS
	 				 File crs = new File(lsLocalPath + file.getName());
	 				 crs.setReadable(true);
	 				 crs.setWritable(true);
	 				 if (!crs.exists()) {
	 						logManager.info("========" + crs + ":文件不存在=========");
	 			            
	 				}else {
	 						try {
	 				            isr = new InputStreamReader(new FileInputStream(crs), "UTF-8");
	 				            br = new BufferedReader(isr);
	 				        } catch (Exception e) {
	 				            e.printStackTrace();
	 				        }
	 				        String line = "";
	 				        ArrayList<String> records = new ArrayList<>();
	 				        try {
	 				        	IfsIntfcIycrBean ifsIntfcIycrBean = null;
	 				        	
	 				        	String rate=null;
	 				            while ((line = br.readLine()) != null) {
	 				            	String[] names = line.split(",(?=([^\\\"]*\\\"[^\\\"]*\\\")*[^\\\"]*$)",-1);
	 				            	
	 				                System.out.println(line);
	 				                
	 				               
	 				                if(names[0].equals("REUTERS MKT DATA") || names[0].equals("") || names[0].equals("Instrument ID")) {
	 				                	logManager.info("========names:" + names + "=========");
	 				                }else {
	 				                	String h194=null;
	 				                	String j84=null;
	 				                	String h36=null;
	 				                	String h37=null;
	 				                	String h40=null;
	 				                	String h42=null;
	 				                	String fixRate;
	 				                	BigDecimal crsRate;
	 				                	
	 				                	if(mktStatus1.size()>0) {
	 				                		for(SlMktStatus slMktStatus : mktStatus1) {
		 				                		h194=slMktStatus.getPrice();
		 				                	}
	 				                	}
	 				                	
	 				                	if(mktStatus.size()>0) {
	 				                		for(SlMktStatus slMktStatus : mktStatus) {
		 				                		j84=slMktStatus.getPrice();
		 				                	}
	 				                		
	 				                   	if(names[2].trim().equals("CNYON=")) {
	 				                		h36=names[7].trim().replace("\"", "").replace(",", "");
	 				                		fixRate=Double.toString(Double.parseDouble(h194)+Double.parseDouble(h36)/10000);
	 				                		crsRate=new BigDecimal(((Double.parseDouble(fixRate)/Double.parseDouble(h194))*(1+Double.parseDouble(j84)/360/100)-1)*360*100);
	 				                	
	 				                 		   ifsIntfcIycrBean = new IfsIntfcIycrBean();
	 							               ifsIntfcIycrBean.setLstmntdte(lstmntdte);
	 							               ifsIntfcIycrBean.setBr("01");
	 							               ifsIntfcIycrBean.setYieldcurve(StringUtils.trimToEmpty("USDCRSMM1D"));
	 							                       
	 							                ifsIntfcIycrBean.setRatecode(StringUtils.trimToEmpty("USDCRSMM"));
	 							               
	 							                ifsIntfcIycrBean.setCcy("USD");//G
	 						                       
	 							              
	 							            	ifsIntfcIycrBean.setMtyend(StringUtils.trimToEmpty("1D"));
	 							            
	 							                
	 							                ifsIntfcIycrBean.setBidrate_8(StringUtils.trimToEmpty(names[17].trim().replace("\"", "").replace(",", "")));
	 							                ifsIntfcIycrBean.setMidrate_8(StringUtils.trimToEmpty(crsRate.toString()));
	 							                ifsIntfcIycrBean.setOfferrate_8("0");
	 							                ifsIntfcIycrBean.setTradedate(StringUtils.trimToEmpty(names[14].trim()));
	 							                ifsIntfcIycrBean.setRic(StringUtils.trimToEmpty(names[2].trim()));
	 							                ifsIntfcIycrBean.setDefined_identifier2(StringUtils.trimToEmpty(names[0].trim()));
	 							                ifsIntfcIycrBean.setDefined_identifier(StringUtils.trimToEmpty(names[1].trim()));
	 							                ifsIntfcIycrBean.setSourse("CRS");
	 							                ifsIntfcIycrBean.setRemark("");
	 							                
	 							                iycrBean.add(ifsIntfcIycrBean);
	 							                records.add(line);
	 				                	}else if(names[2].trim().equals("CNYSW=")) {
	 				                		h37=names[7].trim().replace("\"", "").replace(",", "");
	 				                		fixRate=Double.toString(Double.parseDouble(h194)+Double.parseDouble(h37)/10000);
	 				                		crsRate=new BigDecimal(((Double.parseDouble(fixRate)/Double.parseDouble(h194))*(1+Double.parseDouble(j84)*7/360/100)-1)*360*100/7);
	 					                	
	 				                		 ifsIntfcIycrBean = new IfsIntfcIycrBean();
	 							               ifsIntfcIycrBean.setLstmntdte(lstmntdte);
	 							               ifsIntfcIycrBean.setBr("01");
	 							               ifsIntfcIycrBean.setYieldcurve(StringUtils.trimToEmpty("USDCRSMM1W"));
	 							                       
	 							                ifsIntfcIycrBean.setRatecode(StringUtils.trimToEmpty("USDCRSMM"));
	 							               
	 							                ifsIntfcIycrBean.setCcy("USD");//G
	 						                       
	 							              
	 							            	ifsIntfcIycrBean.setMtyend(StringUtils.trimToEmpty("1W"));
	 							            
	 							                
	 							                ifsIntfcIycrBean.setBidrate_8(StringUtils.trimToEmpty(names[17].trim().trim().replace("\"", "").replace(",", "")));
	 							                ifsIntfcIycrBean.setMidrate_8(StringUtils.trimToEmpty(crsRate.toString()));
	 							                ifsIntfcIycrBean.setOfferrate_8("0");
	 							                ifsIntfcIycrBean.setTradedate(StringUtils.trimToEmpty(names[14].trim()));
	 							                ifsIntfcIycrBean.setRic(StringUtils.trimToEmpty(names[2].trim()));
	 							                ifsIntfcIycrBean.setDefined_identifier2(StringUtils.trimToEmpty(names[0].trim()));
	 							                ifsIntfcIycrBean.setDefined_identifier(StringUtils.trimToEmpty(names[1].trim()));
	 							                ifsIntfcIycrBean.setSourse("CRS");
	 							                ifsIntfcIycrBean.setRemark("");
	 							                
	 							                iycrBean.add(ifsIntfcIycrBean);
	 							                records.add(line);
	 				                	}else if(names[2].trim().equals("CNY1M=")) {
	 				                		h42=names[7].trim().replace("\"", "").replace(",", "");
	 				                		fixRate=Double.toString(Double.parseDouble(h194)+Double.parseDouble(h42)/10000);
	 				                		crsRate=new BigDecimal(((Double.parseDouble(fixRate)/Double.parseDouble(h194))*(1+Double.parseDouble(j84)/12/100)-1)*12*100);
	 					                	
	 				                		 ifsIntfcIycrBean = new IfsIntfcIycrBean();
	 							               ifsIntfcIycrBean.setLstmntdte(lstmntdte);
	 							               ifsIntfcIycrBean.setBr("01");
	 							               ifsIntfcIycrBean.setYieldcurve(StringUtils.trimToEmpty("USDCRSMM1M"));
	 							                       
	 							                ifsIntfcIycrBean.setRatecode(StringUtils.trimToEmpty("USDCRSMM"));
	 							               
	 							                ifsIntfcIycrBean.setCcy("USD");//G
	 						                       
	 							              
	 							            	ifsIntfcIycrBean.setMtyend(StringUtils.trimToEmpty("1M"));
	 							            
	 							                
	 							                ifsIntfcIycrBean.setBidrate_8(StringUtils.trimToEmpty(names[17].trim().trim().replace("\"", "").replace(",", "")));
	 							                ifsIntfcIycrBean.setMidrate_8(StringUtils.trimToEmpty(crsRate.toString()));
	 							                ifsIntfcIycrBean.setOfferrate_8("0");
	 							                ifsIntfcIycrBean.setTradedate(StringUtils.trimToEmpty(names[14].trim()));
	 							                ifsIntfcIycrBean.setRic(StringUtils.trimToEmpty(names[2].trim()));
	 							                ifsIntfcIycrBean.setDefined_identifier2(StringUtils.trimToEmpty(names[0].trim()));
	 							                ifsIntfcIycrBean.setDefined_identifier(StringUtils.trimToEmpty(names[1].trim()));
	 							                ifsIntfcIycrBean.setSourse("CRS");
	 							                ifsIntfcIycrBean.setRemark("");
	 							                
	 							                iycrBean.add(ifsIntfcIycrBean);
	 							                records.add(line);
	 				                	}else if(names[2].trim().equals("CNY3M=")) {
	 				                		h40=names[7].trim().replace("\"", "").replace(",", "");
	 				                		fixRate=Double.toString(Double.parseDouble(h194)+Double.parseDouble(h40)/10000);
	 				                		crsRate=new BigDecimal(((Double.parseDouble(fixRate)/Double.parseDouble(h194))*(1+Double.parseDouble(j84)/4/100)-1)*4*100);
	 				                		 ifsIntfcIycrBean = new IfsIntfcIycrBean();
	 							               ifsIntfcIycrBean.setLstmntdte(lstmntdte);
	 							               ifsIntfcIycrBean.setBr("01");
	 							               ifsIntfcIycrBean.setYieldcurve(StringUtils.trimToEmpty("USDCRSMM3M"));
	 							                       
	 							                ifsIntfcIycrBean.setRatecode(StringUtils.trimToEmpty("USDCRSMM"));
	 							               
	 							                ifsIntfcIycrBean.setCcy("USD");//G
	 						                       
	 							              
	 							            	ifsIntfcIycrBean.setMtyend(StringUtils.trimToEmpty("3M"));
	 							            
	 							                
	 							                ifsIntfcIycrBean.setBidrate_8(StringUtils.trimToEmpty(names[17].trim().trim().replace("\"", "").replace(",", "")));
	 							                ifsIntfcIycrBean.setMidrate_8(StringUtils.trimToEmpty(crsRate.toString()));
	 							                ifsIntfcIycrBean.setOfferrate_8("0");
	 							                ifsIntfcIycrBean.setTradedate(StringUtils.trimToEmpty(names[14].trim()));
	 							                ifsIntfcIycrBean.setRic(StringUtils.trimToEmpty(names[2].trim()));
	 							                ifsIntfcIycrBean.setDefined_identifier2(StringUtils.trimToEmpty(names[0].trim()));
	 							                ifsIntfcIycrBean.setDefined_identifier(StringUtils.trimToEmpty(names[1].trim()));
	 							                ifsIntfcIycrBean.setSourse("CRS");
	 							                ifsIntfcIycrBean.setRemark("");
	 							                
	 							                iycrBean.add(ifsIntfcIycrBean);
	 							                records.add(line);
	 				                		}
	 				                	ret = ifsIntfcIycrService.doBussExcelPropertyToDataBase(iycrBean);
	 				                		
	 				                	}

	 				            
	 				                	
	 				                	
	 				                	
	 					                if(names[2].trim().equals("CNUSSO6M=")) {
	 					                	
	 					                
	 						               
	 						                ifsIntfcIycrBean = new IfsIntfcIycrBean();
	 						                ifsIntfcIycrBean.setLstmntdte(lstmntdte);
	 						                ifsIntfcIycrBean.setBr("01");
	 						               ifsIntfcIycrBean.setYieldcurve(StringUtils.trimToEmpty("USD3MCRS6M"));
	 						                       
	 						                ifsIntfcIycrBean.setRatecode(StringUtils.trimToEmpty("USD3MCRS"));
	 						               
	 						                ifsIntfcIycrBean.setCcy("USD");
	 					                       
	 						              
	 						            	ifsIntfcIycrBean.setMtyend(StringUtils.trimToEmpty("6M"));
	 						            
	 						                
	 						                ifsIntfcIycrBean.setBidrate_8(StringUtils.trimToEmpty(names[17].trim().replace("\"", "").replace(",", "")));
	 						                ifsIntfcIycrBean.setMidrate_8(StringUtils.trimToEmpty(names[7].trim().replace("\"", "").replace(",", "")));
	 						                ifsIntfcIycrBean.setOfferrate_8("0");
	 						                ifsIntfcIycrBean.setTradedate(StringUtils.trimToEmpty(names[14].trim()));
	 						                ifsIntfcIycrBean.setRic(StringUtils.trimToEmpty(names[2].trim()));
	 						                ifsIntfcIycrBean.setDefined_identifier2(StringUtils.trimToEmpty(names[0].trim()));
	 						                ifsIntfcIycrBean.setDefined_identifier(StringUtils.trimToEmpty(names[1].trim()));
	 						                ifsIntfcIycrBean.setSourse("CRS");
	 						                ifsIntfcIycrBean.setRemark("");
	 						                
	 						                iycrBean.add(ifsIntfcIycrBean);
	 						                records.add(line);
	 						            }else if(names[2].trim().equals("CNUSSO9M=")) {
	 					                	
	 					                
	 						               
	 						                ifsIntfcIycrBean = new IfsIntfcIycrBean();
	 						                ifsIntfcIycrBean.setLstmntdte(lstmntdte);
	 						                ifsIntfcIycrBean.setBr("01");
	 						               ifsIntfcIycrBean.setYieldcurve(StringUtils.trimToEmpty("USD3MCRS9M"));
	 						                       
	 						                ifsIntfcIycrBean.setRatecode(StringUtils.trimToEmpty("USD3MCRS"));
	 						               
	 						                ifsIntfcIycrBean.setCcy("USD");//G
	 					                       
	 						              
	 						            	ifsIntfcIycrBean.setMtyend(StringUtils.trimToEmpty("9M"));
	 						            
	 						                
	 						                ifsIntfcIycrBean.setBidrate_8(StringUtils.trimToEmpty(names[17].trim().replace("\"", "").replace(",", "")));
	 						                ifsIntfcIycrBean.setMidrate_8(StringUtils.trimToEmpty(names[7].trim().replace("\"", "").replace(",", "")));
	 						                ifsIntfcIycrBean.setOfferrate_8("0");
	 						                ifsIntfcIycrBean.setTradedate(StringUtils.trimToEmpty(names[14].trim()));
	 						                ifsIntfcIycrBean.setRic(StringUtils.trimToEmpty(names[2].trim()));
	 						                ifsIntfcIycrBean.setDefined_identifier2(StringUtils.trimToEmpty(names[0].trim()));
	 						                ifsIntfcIycrBean.setDefined_identifier(StringUtils.trimToEmpty(names[1].trim()));
	 						                ifsIntfcIycrBean.setSourse("CRS");
	 						                ifsIntfcIycrBean.setRemark("");
	 						                
	 						                iycrBean.add(ifsIntfcIycrBean);
	 						                records.add(line);
	 						            }else if(names[2].trim().equals("CNUSSO1Y=")) {
	 					                	
	 					                
	 						               
	 						                ifsIntfcIycrBean = new IfsIntfcIycrBean();
	 						                ifsIntfcIycrBean.setLstmntdte(lstmntdte);
	 						                ifsIntfcIycrBean.setBr("01");
	 						               ifsIntfcIycrBean.setYieldcurve(StringUtils.trimToEmpty("USD3MCRS1Y"));
	 						                       
	 						                ifsIntfcIycrBean.setRatecode(StringUtils.trimToEmpty("USD3MCRS"));
	 						               
	 						                ifsIntfcIycrBean.setCcy("USD");//G
	 					                       
	 						              
	 						            	ifsIntfcIycrBean.setMtyend(StringUtils.trimToEmpty("1Y"));
	 						            
	 						                
	 						                ifsIntfcIycrBean.setBidrate_8(StringUtils.trimToEmpty(names[17].trim().replace("\"", "").replace(",", "")));
	 						                ifsIntfcIycrBean.setMidrate_8(StringUtils.trimToEmpty(names[7].trim().replace("\"", "").replace(",", "")));
	 						                ifsIntfcIycrBean.setOfferrate_8("0");
	 						                ifsIntfcIycrBean.setTradedate(StringUtils.trimToEmpty(names[14].trim()));
	 						                ifsIntfcIycrBean.setRic(StringUtils.trimToEmpty(names[2].trim()));
	 						                ifsIntfcIycrBean.setDefined_identifier2(StringUtils.trimToEmpty(names[0].trim()));
	 						                ifsIntfcIycrBean.setDefined_identifier(StringUtils.trimToEmpty(names[1].trim()));
	 						                ifsIntfcIycrBean.setSourse("CRS");
	 						                ifsIntfcIycrBean.setRemark("");
	 						                
	 						                iycrBean.add(ifsIntfcIycrBean);
	 						                records.add(line);
	 						            }else if(names[2].trim().equals("CNUSSO2Y=")) {
	 					                	
	 					                
	 						               
	 						                ifsIntfcIycrBean = new IfsIntfcIycrBean();
	 						                ifsIntfcIycrBean.setLstmntdte(lstmntdte);
	 						                ifsIntfcIycrBean.setBr("01");
	 						               ifsIntfcIycrBean.setYieldcurve(StringUtils.trimToEmpty("USD3MCRS2Y"));
	 						                       
	 						                ifsIntfcIycrBean.setRatecode(StringUtils.trimToEmpty("USD3MCRS"));
	 						               
	 						                ifsIntfcIycrBean.setCcy("USD");//G
	 					                       
	 						              
	 						            	ifsIntfcIycrBean.setMtyend(StringUtils.trimToEmpty("2Y"));
	 						            
	 						                
	 						                ifsIntfcIycrBean.setBidrate_8(StringUtils.trimToEmpty(names[17].trim().replace("\"", "").replace(",", "")));
	 						                ifsIntfcIycrBean.setMidrate_8(StringUtils.trimToEmpty(names[7].trim().replace("\"", "").replace(",", "")));
	 						                ifsIntfcIycrBean.setOfferrate_8("0");
	 						                ifsIntfcIycrBean.setTradedate(StringUtils.trimToEmpty(names[14].trim()));
	 						                ifsIntfcIycrBean.setRic(StringUtils.trimToEmpty(names[2].trim()));
	 						                ifsIntfcIycrBean.setDefined_identifier2(StringUtils.trimToEmpty(names[0].trim()));
	 						                ifsIntfcIycrBean.setDefined_identifier(StringUtils.trimToEmpty(names[1].trim()));
	 						                ifsIntfcIycrBean.setSourse("CRS");
	 						                ifsIntfcIycrBean.setRemark("");
	 						                
	 						                iycrBean.add(ifsIntfcIycrBean);
	 						                records.add(line);
	 						            }else if(names[2].trim().equals("CNUSSO3Y=")) {
	 					                	
	 					                
	 						               
	 						                ifsIntfcIycrBean = new IfsIntfcIycrBean();
	 						                ifsIntfcIycrBean.setLstmntdte(lstmntdte);
	 						                ifsIntfcIycrBean.setBr("01");
	 						               ifsIntfcIycrBean.setYieldcurve(StringUtils.trimToEmpty("USD3MCRS3Y"));
	 						                       
	 						                ifsIntfcIycrBean.setRatecode(StringUtils.trimToEmpty("USD3MCRS"));
	 						               
	 						                ifsIntfcIycrBean.setCcy("USD");//G
	 					                       
	 						              
	 						            	ifsIntfcIycrBean.setMtyend(StringUtils.trimToEmpty("3Y"));
	 						            
	 						                
	 						                ifsIntfcIycrBean.setBidrate_8(StringUtils.trimToEmpty(names[17].trim().replace("\"", "").replace(",", "")));
	 						                ifsIntfcIycrBean.setMidrate_8(StringUtils.trimToEmpty(names[7].trim().replace("\"", "").replace(",", "")));
	 						                ifsIntfcIycrBean.setOfferrate_8("0");
	 						                ifsIntfcIycrBean.setTradedate(StringUtils.trimToEmpty(names[14].trim()));
	 						                ifsIntfcIycrBean.setRic(StringUtils.trimToEmpty(names[2].trim()));
	 						                ifsIntfcIycrBean.setDefined_identifier2(StringUtils.trimToEmpty(names[0].trim()));
	 						                ifsIntfcIycrBean.setDefined_identifier(StringUtils.trimToEmpty(names[1].trim()));
	 						                ifsIntfcIycrBean.setSourse("CRS");
	 						                ifsIntfcIycrBean.setRemark("");
	 						                
	 						                iycrBean.add(ifsIntfcIycrBean);
	 						                records.add(line);
	 						            }else if(names[2].trim().equals("CNUSSO4Y=")) {
	 					                	
	 					                
	 						               
	 						                ifsIntfcIycrBean = new IfsIntfcIycrBean();
	 						                ifsIntfcIycrBean.setLstmntdte(lstmntdte);
	 						                ifsIntfcIycrBean.setBr("01");
	 						               ifsIntfcIycrBean.setYieldcurve(StringUtils.trimToEmpty("USD3MCRS4Y"));
	 						                       
	 						                ifsIntfcIycrBean.setRatecode(StringUtils.trimToEmpty("USD3MCRS"));
	 						               
	 						                ifsIntfcIycrBean.setCcy("USD");//G
	 					                       
	 						              
	 						            	ifsIntfcIycrBean.setMtyend(StringUtils.trimToEmpty("4Y"));
	 						            
	 						                
	 						                ifsIntfcIycrBean.setBidrate_8(StringUtils.trimToEmpty(names[17].trim().replace("\"", "").replace(",", "")));
	 						                ifsIntfcIycrBean.setMidrate_8(StringUtils.trimToEmpty(names[7].trim().replace("\"", "").replace(",", "")));
	 						                ifsIntfcIycrBean.setOfferrate_8("0");
	 						                ifsIntfcIycrBean.setTradedate(StringUtils.trimToEmpty(names[14].trim()));
	 						                ifsIntfcIycrBean.setRic(StringUtils.trimToEmpty(names[2].trim()));
	 						                ifsIntfcIycrBean.setDefined_identifier2(StringUtils.trimToEmpty(names[0].trim()));
	 						                ifsIntfcIycrBean.setDefined_identifier(StringUtils.trimToEmpty(names[1].trim()));
	 						                ifsIntfcIycrBean.setSourse("CRS");
	 						                ifsIntfcIycrBean.setRemark("");
	 						                
	 						                iycrBean.add(ifsIntfcIycrBean);
	 						                records.add(line);
	 						            }else if(names[2].trim().equals("CNUSSO5Y=")) {
	 					                	
	 					                
	 						               
	 						                ifsIntfcIycrBean = new IfsIntfcIycrBean();
	 						                ifsIntfcIycrBean.setLstmntdte(lstmntdte);
	 						                ifsIntfcIycrBean.setBr("01");
	 						               ifsIntfcIycrBean.setYieldcurve(StringUtils.trimToEmpty("USD3MCRS5Y"));
	 						                       
	 						                ifsIntfcIycrBean.setRatecode(StringUtils.trimToEmpty("USD3MCRS"));
	 						               
	 						                ifsIntfcIycrBean.setCcy("USD");//G
	 					                       
	 						              
	 						            	ifsIntfcIycrBean.setMtyend(StringUtils.trimToEmpty("5Y"));
	 						            
	 						                
	 						                ifsIntfcIycrBean.setBidrate_8(StringUtils.trimToEmpty(names[17].trim().replace("\"", "").replace(",", "")));
	 						                ifsIntfcIycrBean.setMidrate_8(StringUtils.trimToEmpty(names[7].trim().replace("\"", "").replace(",", "")));
	 						                ifsIntfcIycrBean.setOfferrate_8("0");
	 						                ifsIntfcIycrBean.setTradedate(StringUtils.trimToEmpty(names[14].trim()));
	 						                ifsIntfcIycrBean.setRic(StringUtils.trimToEmpty(names[2].trim()));
	 						                ifsIntfcIycrBean.setDefined_identifier2(StringUtils.trimToEmpty(names[0].trim()));
	 						                ifsIntfcIycrBean.setDefined_identifier(StringUtils.trimToEmpty(names[1].trim()));
	 						                ifsIntfcIycrBean.setSourse("CRS");
	 						                ifsIntfcIycrBean.setRemark("");
	 						                
	 						                iycrBean.add(ifsIntfcIycrBean);
	 						                records.add(line);
	 						            }
	 					               ret = ifsIntfcIycrService.doBussExcelPropertyToDataBase(iycrBean);
	 						          }
	 				              
	 				            }
	 				          
	 						            //插入inth表和iycr
	 						            String flag=  ifsIntfcIycrService.dataBaseToIycr(lstmntdte);
	 						          //插入inth表和irhs
	 						            String flag1=  ifsIntfcIycrService.dataBaseToIrhs(lstmntdte);
	 						            
	 						            if("SUCCESS".equals(flag)&&"SUCCESS".equals(flag1)){
	 						                ret=ret+"且当天收益率曲线数据同步至OPICS成功";
	 						            }else {
	 						                ret=ret+"且当天收益率曲线数据同步至OPICS失败";
	 						            }
	 						            System.out.println("csv表格读取行数：" + records.size());
	 						        } catch (IOException e) {
	 						            e.printStackTrace();
	 						        }
	 						        
	 					}
	 			 }
	          }
	       }	  
	    
    }
    /**
     * 自动获取市场数据 Excel上传外汇远掉点信息
     * @throws Exception 
     */
    @ResponseBody
    @RequestMapping(value = "/ImportLuIycrByExcel")
    public void ImportLuIycrByExcel() throws Exception {
    	LutouUtil lutouUtil=new LutouUtil();
    	//下载模板
    	Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		//calendar.add(Calendar.DATE, -1);
		String sdate = null;
		sdate = format.format(calendar.getTime());
		//sdate = "20230830";
		//readCsvByBufferedReader("C:\\Users\\hspcadmin\\Desktop\\韩亚银行OPICS升级\\二期文档\\市场数据新模板\\HANA_IntradaySample.csv");
		//////sdate = format.format(calendar.getTime());
		lutouUtil.sftp("MKTDATA_NEW"+sdate+".csv");
		lutouUtil.sftp("BONDDATA_NEW"+sdate+".csv");
		lutouUtil.sftp("CFX_NEW"+sdate+".csv");
		
		mktTmaxCommand.executeHostVisitAction();
		getMuH();
    	//导入数据
		this.getMktdata();
		
    	this.getBondata();
    	this.getCfx();
    	this.getCrs();
    	
		lutouUtil.smbGet();
		this.getSyl();
    	
 	
    }
    /**
     * 自动获取市场数据 Excel上传外汇汇率
     * @throws Exception 
     */
    @ResponseBody
    @RequestMapping(value = "/ImportLuIrevByExcel")
    public void ImportLuIrevByExcel() throws Exception {
    	//下载模板
    	
    	ifsIntfcIrevController.ImportLIrevByExcel();
    	
    	
    }
}
