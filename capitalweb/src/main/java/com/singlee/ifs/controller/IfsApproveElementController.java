package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.ifs.model.IfsApproveElement;
import com.singlee.ifs.service.IfsApproveElementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 产品表产品要素Controller
 * 
 * ClassName: IfsApproveElementController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason: TODO ADD REASON(可选). <br/>
 * date: 2018-5-30 下午07:06:45 <br/>
 * 
 * @author zhangcm
 * @version
 * @since JDK 1.6
 */
@Controller
@RequestMapping(value = "IfsApproveElementController")
public class IfsApproveElementController {

	@Autowired
	IfsApproveElementService ifsLimitService;

	@ResponseBody
	@RequestMapping(value = "/searchPageLimit")
	public RetMsg<PageInfo<IfsApproveElement>> searchPageLimit(@RequestBody Map<String, Object> map) {
		Page<IfsApproveElement> page = ifsLimitService.searchPageLimit(map);
		return RetMsgHelper.ok(page);
	}

	/***
	 * 删除
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteLimit")
	public RetMsg<Serializable> deleteLimit(@RequestBody Map<String, String> map) {
		String id = map.get("id");
		ifsLimitService.deleteById(id);
		return RetMsgHelper.ok();
	}

	/**
	 * 新增
	 */
	@RequestMapping(value = "/LimitConditionAdd")
	@ResponseBody
	public RetMsg<Serializable> LimitConditionAdd(@RequestBody IfsApproveElement entity) throws RException {
		ifsLimitService.insert(entity);

		return RetMsgHelper.ok();
	}

	/**
	 * 保存 修改
	 */
	@ResponseBody
	@RequestMapping(value = "/saveLimitConditionEdit")
	public RetMsg<Serializable> saveLimitConditionEdit(@RequestBody IfsApproveElement entity) {
		String id = entity.getId();

		if (null == id || "".equals(id)) {
			return RetMsgHelper.simple("cNo-Error", "修改时请选择id!");

		}

		ifsLimitService.update(entity);
		return RetMsgHelper.ok();
	}

	@ResponseBody
	@RequestMapping(value = "/searchIfsLimitCondition")
	public RetMsg<IfsApproveElement> searchIfsLimitCondition(@RequestBody Map<String, String> map) {
		String id = map.get("id");
		IfsApproveElement entity = ifsLimitService.searchIfsLimitCondition(id);
		return RetMsgHelper.ok(entity);
	}

	/***
	 * 查询 产品表的结构
	 */
	@ResponseBody
	@RequestMapping(value = "/searchColumns")
	public RetMsg<List<Map<String, Object>>> searchColumns(@RequestBody Map<String, Object> map) {
		List<Map<String, Object>> list = ifsLimitService.searchColumns(map);
		return RetMsgHelper.ok(list);
	}

	/***
	 * 查询 产品表
	 */
	@ResponseBody
	@RequestMapping(value = "/searchTables")
	public RetMsg<List<Map<String, Object>>> searchTables(@RequestBody Map<String, Object> map) {
		List<Map<String, Object>> list = ifsLimitService.searchTables(map);
		return RetMsgHelper.ok(list);
	}

}
