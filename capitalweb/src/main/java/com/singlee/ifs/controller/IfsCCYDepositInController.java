package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.ifs.model.IfsFxDepositIn;
import com.singlee.ifs.service.IFSService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.Map;

/**
 * @Author zhangkai
 * @Description
 * @Date
 */
@Controller
@RequestMapping(value="/IfsCCYDepositInController")
public class IfsCCYDepositInController {

    @Autowired
    private IFSService iFSService;

    /**
     * 同业存放(外币)-查询我发起的
     *
     * @param params
     *            - 请求参数
     * @return
     * @author
     * @date 2018-3-21
     */
    @ResponseBody
    @RequestMapping(value = "/searchPageCcyDepositInMine")
    public RetMsg<PageInfo<IfsFxDepositIn>> searchPageCcyDepositInMine(@RequestBody Map<String, Object> params) {
        params.put("isActive", DictConstants.YesNo.YES);
        params.put("userId", SlSessionHelper.getUserId());
        Page<IfsFxDepositIn> page = iFSService.getDepositInFXMinePage(params, 3);
        return RetMsgHelper.ok(page);
    }

    /**
     * 同业存放(外币)-查询 待审批
     *
     * @param params
     *            - 请求参数
     * @return
     * @author
     * @date 2018-3-23
     */
    @ResponseBody
    @RequestMapping(value = "/searchPageCcyDepositInUnfinished")
    public RetMsg<PageInfo<IfsFxDepositIn>> searchPageCcyDepositInUnfinished(@RequestBody Map<String, Object> params) {
        String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse + ","
                + DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
        String[] orderStatus = approveStatus.split(",");
        params.put("approveStatusNo", orderStatus);
        params.put("isActive", DictConstants.YesNo.YES);
        params.put("userId", SlSessionHelper.getUserId());
        Page<IfsFxDepositIn> page = iFSService.getDepositInFXMinePage(params, 1);
        return RetMsgHelper.ok(page);
    }

    /**
     * 同业存放(外币)-查询 已审批
     *
     * @param params
     *            - 请求参数
     * @return
     * @author
     * @date 2018-3-23
     */
    @ResponseBody
    @RequestMapping(value = "/searchPageCcyDepositInFinished")
    public RetMsg<PageInfo<IfsFxDepositIn>> searchPageCcyDepositInFinished(@RequestBody Map<String, Object> params) {
        String approveStatusNo = ParameterUtil.getString(params, "approveStatus", "");
        if (StringUtil.isNullOrEmpty(approveStatusNo)) {
            approveStatusNo = DictConstants.ApproveStatus.ApprovedPass;
            approveStatusNo += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
            approveStatusNo += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
            approveStatusNo += "," + DictConstants.ApproveStatus.Verified;
            approveStatusNo += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
            approveStatusNo += "," + DictConstants.ApproveStatus.Approving;
            String[] orderStatus = approveStatusNo.split(",");
            params.put("approveStatusNo", orderStatus);
        }
        params.put("isActive", DictConstants.YesNo.YES);
        params.put("userId", SlSessionHelper.getUserId());
        Page<IfsFxDepositIn> page = iFSService.getDepositInFXMinePage(params, 2);
        return RetMsgHelper.ok(page);
    }

    // 查询一条
    @ResponseBody
    @RequestMapping(value = "/searchCcyDepositIn")
    public RetMsg<PageInfo<IfsFxDepositIn>> searchCcyDepositIn(@RequestBody Map<String, Object> params) {
        Page<IfsFxDepositIn> page = iFSService.searchCcyDepositIn(params);
        return RetMsgHelper.ok(page);
    }

    // 根据ticketid查询
    @ResponseBody
    @RequestMapping(value = "/searchByTicketId")
    public RetMsg<IfsFxDepositIn> searchByTicketId(@RequestBody Map<String, Object> params) {
        IfsFxDepositIn ifsFxDepositIn = iFSService.searchByTicketId(params);
        return RetMsgHelper.ok(ifsFxDepositIn);
    }

    // 根据ticketid查询
    @ResponseBody
    @RequestMapping(value = "/searchDepositInFXFlowByTicketId")
    public RetMsg<IfsFxDepositIn> searchDepositInFXFlowByTicketId(@RequestBody Map<String, Object> params) {
        IfsFxDepositIn ifsFxDepositIn = iFSService.searchDepositInFXFlowByTicketId(params);
        return RetMsgHelper.ok(ifsFxDepositIn);
    }


    // 新增
    @ResponseBody
    @RequestMapping(value = "/addCcyDepositIn")
    public RetMsg<Serializable> addCcyDepositIn(@RequestBody IfsFxDepositIn record) {
        //校验交易对手
        //String message = iFSService.checkCust(record.getDealTransType(),record.getCounterpartyInstId(),null,record.getCfetscn(), record.getProduct(), record.getProdType());
//        if(StringUtil.isNotEmpty(message)) {
//            return RetMsgHelper.fail(message);
//        }
        String message = iFSService.addCcyDepositIn(record);
        return StringUtil.containsIgnoreCase(message, "保存成功")?RetMsgHelper.ok(message):RetMsgHelper.fail(message);
    }

    // 修改
    @ResponseBody
    @RequestMapping(value = "/editCcyDepositIn")
    public RetMsg<Serializable> editCcyDepositIn(@RequestBody IfsFxDepositIn record) {
//        String message = iFSService.checkCust(map.getDealTransType(),map.getCounterpartyInstId(),null,map.getCfetscn(), map.getProduct(), map.getProdType());
//        if(StringUtil.isNotEmpty(message)) {
//            return RetMsgHelper.fail(message);
//        }
        iFSService.editCcyDepositIn(record);
        return RetMsgHelper.ok();
    }

    //删除
    @ResponseBody
    @RequestMapping(value = "/deleteCcyDepositIn")
    public RetMsg<Serializable> deleteCcyDepositIn(@RequestBody Map<String, Object> map) {
        String ticketid = map.get("ticketid").toString();
        iFSService.deleteCcyDepositIn(ticketid);
        return RetMsgHelper.ok();
    }

}
