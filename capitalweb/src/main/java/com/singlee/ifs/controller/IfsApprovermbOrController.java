package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.ifs.model.IfsApprovermbOr;
import com.singlee.ifs.service.IFSApproveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.io.Serializable;
import java.util.Map;

/**
 * 买断式回购事前审批交易Controller
 * 
 * ClassName: IfsApprovermbOrController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason: TODO ADD REASON(可选). <br/>
 * date: 2018-5-30 下午07:12:31 <br/>
 * 
 * @author zhangcm
 * @version
 * @since JDK 1.6
 */
@Controller
@RequestMapping(value = "/IfsApprovermbOrController")
public class IfsApprovermbOrController extends CommonController {
	@Autowired
	private IFSApproveService cfetsrmbOrService;

	/**
	 * 保存成交单
	 * 
	 * @param IfsCfetsrmbOr
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/saveOr")
	public RetMsg<Serializable> saveOr(@RequestBody IfsApprovermbOr record) throws IOException {
		IfsApprovermbOr or = cfetsrmbOrService.getOrById(record);
		if (or == null) {
			cfetsrmbOrService.insertOr(record);
		} else {
			cfetsrmbOrService.updateOr(record);
		}
		return RetMsgHelper.ok();
	}

	/**
	 * 成交单分页查询
	 * 
	 * @param IfsCfetsrmbOr
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchCfetsrmbOr")
	public RetMsg<PageInfo<IfsApprovermbOr>> searchCfetsrmbOr(@RequestBody IfsApprovermbOr record) {
		Page<IfsApprovermbOr> page = cfetsrmbOrService.getOrList(record);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 根据主键查询成交单
	 * 
	 * @param IfsCfetsrmbOrKey
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchCfetsrmbOrById")
	public RetMsg<IfsApprovermbOr> searchCfetsrmbOrById(@RequestBody IfsApprovermbOr key) {
		IfsApprovermbOr or = cfetsrmbOrService.getOrById(key);
		return RetMsgHelper.ok(or);
	}

	/**
	 * 根据主键查询成交单(返回值增加 流程id)
	 * 
	 * @param
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchCfetsrmbOrAndFlowIdById")
	public RetMsg<IfsApprovermbOr> searchCfetsrmbOrAndFlowIdById(@RequestBody Map<String, Object> key) {
		IfsApprovermbOr irs = cfetsrmbOrService.getOrAndFlowIdById(key);
		return RetMsgHelper.ok(irs);
	}

	/**
	 * 删除成交单
	 * 
	 * @param IfsCfetsrmbOrKey
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/deleteCfetsrmbOr")
	public RetMsg<Serializable> deleteCfetsrmbOr(@RequestBody IfsApprovermbOr key) {
		cfetsrmbOrService.deleteOr(key);
		return RetMsgHelper.ok();
	}

	/**
	 * 查询我发起的
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-21
	 */
	@ResponseBody
	@RequestMapping(value = "/searchRmbOrSwapMine")
	public RetMsg<PageInfo<IfsApprovermbOr>> searchRmbOrSwapMine(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsApprovermbOr> page = cfetsrmbOrService.getRmbOrMinePage(params, 3);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 查询 待审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageRmbOrUnfinished")
	public RetMsg<PageInfo<IfsApprovermbOr>> searchPageRmbOrUnfinished(@RequestBody Map<String, Object> params) {
		String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse + ","
				+ DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
		String[] orderStatus = approveStatus.split(",");
		params.put("approveStatusNo", orderStatus);
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsApprovermbOr> page = cfetsrmbOrService.getRmbOrMinePage(params, 1);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 查询 已审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageRmbOrFinished")
	public RetMsg<PageInfo<IfsApprovermbOr>> searchPageRmbOrFinished(@RequestBody Map<String, Object> params) {
		String approveStatus = ParameterUtil.getString(params, "approveStatus", "");
		if (StringUtil.isNullOrEmpty(approveStatus)) {
			approveStatus = DictConstants.ApproveStatus.ApprovedPass;
			approveStatus += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatus += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatus += "," + DictConstants.ApproveStatus.Verified;
			approveStatus += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			// 20180502修改bug:已审批列表中增加"审批中"
			approveStatus += "," + DictConstants.ApproveStatus.Approving;

			String[] orderStatus = approveStatus.split(",");
			params.put("approveStatusNo", orderStatus);
		}
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsApprovermbOr> page = cfetsrmbOrService.getRmbOrMinePage(params, 2);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 勾兑时，查询表中审批通过的数据
	 * 
	 * @param
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchPageApprovermbOrPassed")
	public RetMsg<PageInfo<IfsApprovermbOr>> searchPageApprovermbOrPassed(@RequestBody Map<String, Object> params) {
		Page<IfsApprovermbOr> page = cfetsrmbOrService.getApproveOrPassedPage(params);
		return RetMsgHelper.ok(page);
	}
}
