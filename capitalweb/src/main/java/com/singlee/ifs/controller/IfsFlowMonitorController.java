package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.ifs.model.IfsFlowMonitor;
import com.singlee.ifs.service.IfsFlowMonitorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * 查询流程监控信息Controller
 * 
 * ClassName: IfsFlowMonitorController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason: TODO ADD REASON(可选). <br/>
 * date: 2018-5-30 下午07:55:31 <br/>
 * 
 * @author zhangcm
 * @version
 * @since JDK 1.6
 */
@Controller
@RequestMapping(value = "/IfsFlowMonitorController")
public class IfsFlowMonitorController {
	@Autowired
	IfsFlowMonitorService ifsFlowMonitorService;

	/***
	 * 分页查询 "审批中"的记录
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageFlowMonitor")
	public RetMsg<PageInfo<IfsFlowMonitor>> searchPageFlowMonitor(@RequestBody Map<String, Object> map) {
		Page<IfsFlowMonitor> page = ifsFlowMonitorService.searchPageFlowMonitor(map);
		return RetMsgHelper.ok(page);
	}
	
	/***
	 * 分页查询 "审批中及审批完成"的记录
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageAllFlowMonitor")
	public RetMsg<PageInfo<IfsFlowMonitor>> searchPageAllFlowMonitor(@RequestBody Map<String, Object> map) {
		Page<IfsFlowMonitor> page = ifsFlowMonitorService.searchPageAllFlowMonitor(map);
		return RetMsgHelper.ok(page);
	}
	@ResponseBody
	@RequestMapping(value="/dealCount")
	public RetMsg<PageInfo<IfsFlowMonitor>> dealCount(@RequestBody Map<String, Object> map){
		Page<IfsFlowMonitor> page = ifsFlowMonitorService.searchDealCount(map);
		return  RetMsgHelper.ok(page);
	}

}
