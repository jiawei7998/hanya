package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.ifs.model.IfsApprovermbCbt;
import com.singlee.ifs.service.IFSApproveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.io.Serializable;
import java.util.Map;

/**
 * 现券买卖事前审批交易Controller
 * 
 * ClassName: IfsApprovermbCbtController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason: TODO ADD REASON(可选). <br/>
 * date: 2018-5-30 下午07:09:04 <br/>
 * 
 * @author zhangcm
 * @version
 * @since JDK 1.6
 */
@Controller
@RequestMapping(value = "/IfsApprovermbCbtController")
public class IfsApprovermbCbtController extends CommonController {
	@Autowired
	private IFSApproveService cfetsrmbCbtService;

	/**
	 * 保存成交单
	 * 
	 * @param IfsApprovermbCbt
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/saveCbt")
	public RetMsg<Serializable> saveCbt(@RequestBody IfsApprovermbCbt record) throws IOException {
		IfsApprovermbCbt cbt = cfetsrmbCbtService.getCbtById(record);
		if (cbt == null) {
			cfetsrmbCbtService.insertCbt(record);
		} else {
			cfetsrmbCbtService.updateCbt(record);
		}
		return RetMsgHelper.ok();
	}

	/**
	 * 成交单分页查询
	 * 
	 * @param IfsApprovermbCbt
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchCfetsrmbCbt")
	public RetMsg<PageInfo<IfsApprovermbCbt>> searchCfetsrmbCbt(@RequestBody IfsApprovermbCbt record) {
		Page<IfsApprovermbCbt> page = cfetsrmbCbtService.getCbtList(record);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 根据主键查询成交单
	 * 
	 * @param IfsApprovermbCbt
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchCfetsrmbCbtById")
	public RetMsg<IfsApprovermbCbt> searchCfetsrmbCbtById(@RequestBody IfsApprovermbCbt key) {
		IfsApprovermbCbt cbt = cfetsrmbCbtService.getCbtById(key);
		return RetMsgHelper.ok(cbt);
	}

	/**
	 * 根据主键查询成交单(返回值增加 流程id)
	 * 
	 * @param
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchCfetsrmbCbtAndFlowIdById")
	public RetMsg<IfsApprovermbCbt> searchCfetsrmbCbtAndFlowIdById(@RequestBody Map<String, Object> key) {
		IfsApprovermbCbt cbt = cfetsrmbCbtService.getCbtAndFlowIdById(key);
		return RetMsgHelper.ok(cbt);
	}

	/**
	 * 删除成交单
	 * 
	 * @param IfsApprovermbCbt
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/deleteCfetsrmbCbt")
	public RetMsg<Serializable> deleteCfetsrmbCbt(@RequestBody IfsApprovermbCbt key) {
		cfetsrmbCbtService.deleteCbt(key);
		return RetMsgHelper.ok();
	}

	/**
	 * 查询我发起的
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-21
	 */
	@ResponseBody
	@RequestMapping(value = "/searchRmbCbtSwapMine")
	public RetMsg<PageInfo<IfsApprovermbCbt>> searchRmbCbtSwapMine(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsApprovermbCbt> page = cfetsrmbCbtService.getRmbCbtMinePage(params, 3);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 查询 待审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageRmbCbtUnfinished")
	public RetMsg<PageInfo<IfsApprovermbCbt>> searchPageRmbCbtUnfinished(@RequestBody Map<String, Object> params) {
		String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse + ","
				+ DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
		String[] orderStatus = approveStatus.split(",");
		params.put("approveStatusNo", orderStatus);
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsApprovermbCbt> page = cfetsrmbCbtService.getRmbCbtMinePage(params, 1);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 查询 已审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageRmbCbtFinished")
	public RetMsg<PageInfo<IfsApprovermbCbt>> searchPageRmbCbtFinished(@RequestBody Map<String, Object> params) {
		String approveStatus = ParameterUtil.getString(params, "approveStatus", "");
		if (StringUtil.isNullOrEmpty(approveStatus)) {
			approveStatus = DictConstants.ApproveStatus.ApprovedPass;
			approveStatus += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatus += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatus += "," + DictConstants.ApproveStatus.Verified;
			approveStatus += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			// 20180502修改bug:已审批列表中增加"审批中"
			approveStatus += "," + DictConstants.ApproveStatus.Approving;

			String[] orderStatus = approveStatus.split(",");
			params.put("approveStatusNo", orderStatus);
		}
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsApprovermbCbt> page = cfetsrmbCbtService.getRmbCbtMinePage(params, 2);
		return RetMsgHelper.ok(page);
	}

	@ResponseBody
	@RequestMapping(value = "/searchPageApprovermbCbtPassed")
	public RetMsg<PageInfo<IfsApprovermbCbt>> searchPageApprovermbCbtPassed(@RequestBody Map<String, Object> params) {
		Page<IfsApprovermbCbt> page = cfetsrmbCbtService.getApproveCbtPassedPage(params);
		return RetMsgHelper.ok(page);
	}
}
