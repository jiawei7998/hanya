package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.mapper.TaUserMapper;
import com.singlee.ifs.model.IfsCustDVP;
import com.singlee.ifs.service.IfsCustDVPService;
import com.singlee.slbpm.service.FlowLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * @author Liang
 * @date 2021/11/1 16:30
 * =======================
 */
@Controller
@RequestMapping(value = "/IfsCustDVPController")
public class IfsCustDVPController {

    @Autowired
    private IfsCustDVPService ifsCustDVPService;

    /** 审批流程日志接口 */
    @Autowired
    private FlowLogService flowLogService;

    @Autowired
    TaUserMapper taUserMapper;

    /**
     * 分页查询IfsCustDVP
     * @param map
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/searchCustDVPInfo")
    public RetMsg<PageInfo<IfsCustDVP>> searchCustDVPInfo(@RequestBody Map<String, Object> map) throws Exception  {
        Page<IfsCustDVP> page =ifsCustDVPService.selectCustDVPByCno(map);
        return RetMsgHelper.ok(page);

    }

}
