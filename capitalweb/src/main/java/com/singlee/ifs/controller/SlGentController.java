package com.singlee.ifs.controller;

import java.io.Serializable;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.financial.bean.SlGentBean;
import com.singlee.financial.bean.SlIntfcGent;
import com.singlee.financial.page.PageInfo;
import com.singlee.ifs.service.SlGentService;

@Controller
@RequestMapping(value = "/SlGentController")
public class SlGentController {

	@Autowired
	SlGentService slGentService;
	
	/**
	 * 参数查询
	 */
	@ResponseBody
	@RequestMapping(value = "/slGentQuery")
	public RetMsg<PageInfo<SlIntfcGent>> slGentQuery(@RequestBody Map<String, String> params) throws Exception {
		//PageInfo<SlDerivativesStatus> page = slDerivativesStatusService.querySlDerivativesStatus(params);
		
		PageInfo<SlIntfcGent> page = slGentService.querySlGent(params);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 新增时保存
	 */
	@ResponseBody
	@RequestMapping(value = "/gentAdd")
	public RetMsg<Serializable> saveGentAdd(@RequestBody SlIntfcGent entity) {
		slGentService.gentAdd(entity);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 修改时保存
	 */
	@ResponseBody
	@RequestMapping(value = "/gentEdit")
	public RetMsg<Serializable> saveGentEdit(@RequestBody SlIntfcGent entity) {
		slGentService.gentEdit(entity);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 删除
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteSlGent")
	public RetMsg<Serializable> deleteSlGent(@RequestBody SlIntfcGent entity) {
		slGentService.deleteSlGent(entity);
		return RetMsgHelper.ok();
	}
}
