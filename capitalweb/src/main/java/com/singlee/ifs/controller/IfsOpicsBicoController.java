package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.financial.bean.SlBicoBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.opics.IStaticServer;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.SlErrors;
import com.singlee.ifs.model.IfsOpicsBico;
import com.singlee.ifs.service.IfsOpicsBicoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

/***
 * SWIFT码
 * 
 * @author lij
 * 
 */
@Controller
@RequestMapping(value = "/IfsOpicsBicoController")
public class IfsOpicsBicoController {
	@Autowired
	IfsOpicsBicoService ifsOpicsBicoService;

	@Autowired
	IStaticServer iStaticServer;

	/***
	 * 分页查询
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageBico")
	public RetMsg<PageInfo<IfsOpicsBico>> searchPageBico(@RequestBody Map<String, Object> map) {
		Page<IfsOpicsBico> page = ifsOpicsBicoService.searchPageOpicsBico(map);
		return RetMsgHelper.ok(page);
	}

	/***
	 * 根据id查询实体类
	 */
	@ResponseBody
	@RequestMapping(value = "/searchBicoById")
	public RetMsg<IfsOpicsBico> searchBicoById(@RequestBody Map<String, String> map) {
		String bic = map.get("bic");
		IfsOpicsBico ifsOpicsBico = ifsOpicsBicoService.searchById(bic);
		return RetMsgHelper.ok(ifsOpicsBico);
	}

	/**
	 * 新增
	 */
	@ResponseBody
	@RequestMapping(value = "/saveOpicsBicoAdd")
	public RetMsg<Serializable> saveOpicsBicoAdd(@RequestBody IfsOpicsBico entity) {
		ifsOpicsBicoService.insert(entity);
		return RetMsgHelper.ok();
	}

	/**
	 * 修改
	 */
	@ResponseBody
	@RequestMapping(value = "/saveOpicsBicoEdit")
	public RetMsg<Serializable> saveOpicsBicoEdit(@RequestBody IfsOpicsBico entity) {
		ifsOpicsBicoService.updateById(entity);
		return RetMsgHelper.ok();
	}

	/**
	 * 删除
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteBico")
	public RetMsg<Serializable> deleteBico(@RequestBody Map<String, String> map) {
		String bic = map.get("bic");
		ifsOpicsBicoService.deleteById(bic);
		return RetMsgHelper.ok();
	}

	/**
	 * 同步opics表
	 */
	@ResponseBody
	@RequestMapping(value = "/syncBico")
	public RetMsg<SlOutBean> syncBico(@RequestBody Map<String, String> map) {
		SlOutBean outBean = new SlOutBean();

		String bic = map.get("bic");
		String[] bics = bic.split(",");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		for (int i = 0; i < bics.length; i++) {
			IfsOpicsBico ifsOpicsBico = ifsOpicsBicoService.searchById(bics[i]);
			SlBicoBean bicoBean = new SlBicoBean();
			bicoBean.setBic(bics[i]);
			bicoBean.setSn(ifsOpicsBico.getSn());
			bicoBean.setLstmntdte(sdf.format(ifsOpicsBico.getLstmntdte()));

			try {
				outBean = iStaticServer.addBico(bicoBean);
			} catch (RemoteConnectFailureException e) {
				outBean.setRetCode(SlErrors.FAILED_RMI_OPICS.getErrCode());
				outBean.setRetMsg(SlErrors.FAILED_RMI_OPICS.getErrMsg() + ":" + e.getMessage());
				outBean.setRetStatus(RetStatusEnum.F);
			} catch (Exception e) {
				outBean.setRetCode(SlErrors.FAILED.getErrCode());
				outBean.setRetMsg(SlErrors.FAILED.getErrMsg() + ":" + e.getMessage());
				outBean.setRetStatus(RetStatusEnum.F);
			}
			if ("S".equals(String.valueOf(outBean.getRetStatus()))) {
				ifsOpicsBico.setStatus("1");
				ifsOpicsBicoService.updateStatus(ifsOpicsBico);
			}
		}
		return RetMsgHelper.ok(outBean);
	}

	/**
	 * 批量校准
	 */
	@ResponseBody
	@RequestMapping(value = "/batchCalibration")
	public RetMsg<SlOutBean> batchCalibration(@RequestBody Map<String, String> map) {
		String type = String.valueOf(map.get("type"));
		String bic;
		String[] bics;
		if ("1".equals(type)) {// 选中记录校准
			bic = map.get("bic");
			bics = bic.split(",");
		} else {// 全部校准
			bics = null;
		}
		SlOutBean outBean = ifsOpicsBicoService.batchCalibration(type, bics);
		return RetMsgHelper.ok(outBean);
	}

	/***
	 * 查询全部swift码
	 */
	@ResponseBody
	@RequestMapping(value = "/searchAllOpicsBico")
	public List<IfsOpicsBico> searchAllOpicsBico() {
		List<IfsOpicsBico> result = ifsOpicsBicoService.searchAllOpicsBico();
		return result;
	}

}
