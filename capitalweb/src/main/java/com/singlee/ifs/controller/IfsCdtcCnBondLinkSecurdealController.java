package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.financial.cdtc.CdtcBtchQryServer;
import com.singlee.ifs.model.IfsCdtcCnBondLinkSecurdeal;
import com.singlee.ifs.service.IfsCdtcCnBondLinkSecurdealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/***
 * 中债直联——综合查询
 * 
 */
@Controller
@RequestMapping(value = "/IfsCdtcCnBondLinkSecurdealController")
public class IfsCdtcCnBondLinkSecurdealController {
	@Autowired
	private IfsCdtcCnBondLinkSecurdealService cnBondLinkSecurdealService;
	@Autowired
	private CdtcBtchQryServer cdtcBatchServer;

	/**
	 * 综合查询——列表
	 * 
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageCnBondLinkSecurdeal")
	public RetMsg<PageInfo<IfsCdtcCnBondLinkSecurdeal>> searchPageCnBondLinkSecurdeal(
			@RequestBody Map<String, Object> map) {
		Page<IfsCdtcCnBondLinkSecurdeal> page = cnBondLinkSecurdealService.searchPageCnBondLinkSecurdeal(map);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 综合查询——详情
	 * 
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageCnBondLinkSecurdealDetail")
	public RetMsg<IfsCdtcCnBondLinkSecurdeal> searchPageCnBondLinkSecurdealDetail(
			@RequestBody Map<String, Object> map) {
		String dealNo = (String) map.get("dealNo");
		IfsCdtcCnBondLinkSecurdeal ifsCdtcCnBondLinkSecurdeal = cnBondLinkSecurdealService.queryByDealNo(dealNo);
		return RetMsgHelper.ok(ifsCdtcCnBondLinkSecurdeal);
	}
}
