package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.financial.bean.SlCounBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.opics.IStaticServer;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.SlErrors;
import com.singlee.ifs.model.IfsOpicsCoun;
import com.singlee.ifs.service.IfsOpicsCounService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Map;

/***
 * 国家代码1
 * 
 * @author lij
 * 
 */
@Controller
@RequestMapping(value = "/IfsOpicsCounController")
public class IfsOpicsCounController {
	@Autowired
	IfsOpicsCounService ifsOpicsCounService;

	@Autowired
	IStaticServer iStaticServer;

	/***
	 * 分页查询
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageCoun")
	public RetMsg<PageInfo<IfsOpicsCoun>> searchPageCoun(@RequestBody Map<String, Object> map) {
		Page<IfsOpicsCoun> page = ifsOpicsCounService.searchPageOpicsCoun(map);
		return RetMsgHelper.ok(page);
	}

	/***
	 * 根据id查询实体类
	 */
	@ResponseBody
	@RequestMapping(value = "/searchCounById")
	public RetMsg<IfsOpicsCoun> searchCounById(@RequestBody Map<String, String> map) {
		String ccode = map.get("ccode");
		IfsOpicsCoun ifsOpicsCoun = ifsOpicsCounService.searchById(ccode);
		return RetMsgHelper.ok(ifsOpicsCoun);
	}

	/**
	 * 新增
	 */
	@ResponseBody
	@RequestMapping(value = "/saveOpicsCounAdd")
	public RetMsg<Serializable> saveOpicsCounAdd(@RequestBody IfsOpicsCoun entity) {
		ifsOpicsCounService.insert(entity);
		return RetMsgHelper.ok();
	}

	/**
	 * 修改
	 */
	@ResponseBody
	@RequestMapping(value = "/saveOpicsCounEdit")
	public RetMsg<Serializable> saveOpicsCounEdit(@RequestBody IfsOpicsCoun entity) {
		ifsOpicsCounService.updateById(entity);
		return RetMsgHelper.ok();
	}

	/**
	 * 删除
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteCoun")
	public RetMsg<Serializable> deleteCoun(@RequestBody Map<String, String> map) {
		String ccode = map.get("ccode");
		ifsOpicsCounService.deleteById(ccode);
		return RetMsgHelper.ok();
	}

	/**
	 * 同步opics表
	 */
	@ResponseBody
	@RequestMapping(value = "/syncCoun")
	public RetMsg<SlOutBean> syncCoun(@RequestBody Map<String, String> map) {
		SlOutBean slOutBean = new SlOutBean();

		String ccode = map.get("ccode");
		String[] ccodes = ccode.split(",");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		for (int i = 0; i < ccodes.length; i++) {
			IfsOpicsCoun ifsOpicsCoun = ifsOpicsCounService.searchById(ccodes[i]);
			SlCounBean counBean = new SlCounBean();
			counBean.setCcode(ccodes[i]);
			counBean.setCoun(ifsOpicsCoun.getCoun());
			counBean.setLstmntdte(sdf.format(ifsOpicsCoun.getLstmntdte()));
			try {
				slOutBean = iStaticServer.addCoun(counBean);
			} catch (RemoteConnectFailureException e) {
				slOutBean.setRetCode(SlErrors.FAILED_RMI_OPICS.getErrCode());
				slOutBean.setRetMsg(SlErrors.FAILED_RMI_OPICS.getErrMsg() + ":" + e.getMessage());
				slOutBean.setRetStatus(RetStatusEnum.F);
			} catch (Exception e) {
				slOutBean.setRetCode(SlErrors.FAILED.getErrCode());
				slOutBean.setRetMsg(SlErrors.FAILED.getErrMsg() + ":" + e.getMessage());
				slOutBean.setRetStatus(RetStatusEnum.F);
			}

			if ("S".equals(String.valueOf(slOutBean.getRetStatus()))) {
				ifsOpicsCoun.setStatus("1");
				ifsOpicsCounService.updateStatus(ifsOpicsCoun);
			}

		}

		return RetMsgHelper.ok(slOutBean);
	}

	/**
	 * 批量校准
	 */
	@ResponseBody
	@RequestMapping(value = "/batchCalibration")
	public RetMsg<SlOutBean> batchCalibration(@RequestBody Map<String, String> map) {
		SlOutBean slOutBean = new SlOutBean();
		String type = String.valueOf(map.get("type"));
		String ccode;
		String[] ccodes;
		if ("1".equals(type)) {// 选中记录校准
			ccode = map.get("ccode");
			ccodes = ccode.split(",");
		} else {// 全部校准
			ccodes = null;
		}
		try {
			slOutBean = ifsOpicsCounService.batchCalibration(type, ccodes);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return RetMsgHelper.ok(slOutBean);
	}

	/***
	 * 查询全部
	 */
	@ResponseBody
	@RequestMapping(value = "/searchAllOpicsCoun")
	public Page<IfsOpicsCoun> searchAllOpicsCoun() {
		Page<IfsOpicsCoun> page = ifsOpicsCounService.searchAllOpicsCoun();
		return page;
	}

}
