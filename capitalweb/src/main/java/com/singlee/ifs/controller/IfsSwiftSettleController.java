package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.ifs.model.IfsSwiftSettleInfo;
import com.singlee.ifs.service.IfsSwiftSettleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * SWIFT清算报文处理 Controller
 * 
 * 
 * @author 
 * @version
 * @since JDK 1.6
 */
@Controller
@RequestMapping(value = "/IfsSwiftSettleController")
public class IfsSwiftSettleController {

	@Autowired
	private IfsSwiftSettleService ifsSwiftSettleService;

	@ResponseBody
	@RequestMapping(value = "/getSettleInfo")
	public RetMsg<PageInfo<IfsSwiftSettleInfo>> getSetleInfo(@RequestBody Map<String, Object> map) {
		Page<IfsSwiftSettleInfo> page = ifsSwiftSettleService.getSetleInfo(map);
		return RetMsgHelper.ok(page);
	}
}