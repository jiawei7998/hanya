package com.singlee.ifs.controller;

import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.MathUtil;
import com.singlee.capital.dict.model.TaDictVo;
import com.singlee.capital.dict.service.impl.DictionaryGetServiceImpl;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.financial.bean.*;
import com.singlee.financial.esb.dlcb.ICreateGTPFileServer;
import com.singlee.financial.opics.IBaseServer;
import com.singlee.financial.opics.IFxdServer;
import com.singlee.financial.pojo.component.PsEnum;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.FxCurrencyBean;
import com.singlee.financial.pojo.trade.InstitutionBean;
import com.singlee.ifs.mapper.IfsForeignCcyMapper;
import com.singlee.ifs.model.IfsForeignccyDish;
import com.singlee.ifs.service.IfsForeignCcyService;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/IfsJobManualManageController")
public class IfsJobManualManageController extends CommonController {

    
    @Autowired
    private IBaseServer baseServer;
    
    @Autowired
    private IFxdServer fxdServer ;
    
    @Autowired
    private IfsForeignCcyMapper ccyMapper;
    
    @Autowired
    private DictionaryGetServiceImpl dictionary;
    
    @Autowired
    private IfsForeignCcyService foreignCcyService;
    
    @Autowired
    private ICreateGTPFileServer createGTPFileServer;

    private static Logger logger = Logger.getLogger(IfsJobManualManageController.class);

    /**
     * 代客交易取回
     * @throws Exception 
     * @throws RemoteConnectFailureException 
     */
    @ResponseBody
    @RequestMapping(value = "/queryForeignCcy")
    public RetMsg<SlOutBean> queryForeignCcy(@RequestBody Map<String, String> params) throws RemoteConnectFailureException, Exception {
    	logger.info("==================开始手工处理==============================");
    	Map<String, Object> map=new HashMap<String, Object>();
    	Date sysDate=baseServer.getOpicsSysDate("01");
    	if(""!=params.get("queryDate") && null != params.get("queryDate")){
	    String queryDate =   params.get("queryDate").split("T")[0].replace("-", "");
	        map.put("queryDate",queryDate);
        }else {
        	 map.put("queryDate",DateUtil.getCurrentDateAsString("yyyyMMdd"));
		}
		SlOutBean result =  foreignCcyService.queryForeignCcy(map);
		String retCode = result.getRetCode();
		if(	(null != retCode && "AAAAAAAAAA".equals(retCode))){
			logger.info("==================核心取回外汇代客查询成功==============================");
		}else {
			logger.info("==================核心取回外汇代客查询失败==============================");
		}
		
		
		
		
		logger.info("********** 外汇代客发送opics开始***********");
		Map<String, Object> map3=new HashMap<String, Object>();
		//Date dealDate=baseServer.getOpicsSysDate("01");
		//map3.put("dealDate", DateUtil.format(sysDate));
		//map3.put("dealDate", "2018-01-16");
		List<IfsForeignccyDish> list=ccyMapper.searchDishList(map3);
		List<TaDictVo> dictList=dictionary.getTaDictByCode("AMTFLAG");
		if(dictList!=null&&dictList.size()>0){
			String amt=dictList.get(0).getDict_value();
			for (IfsForeignccyDish bean : list) {
				String curTime=DateUtil.getCurrentTimeAsString();
				String hour=curTime.substring(0,2);
				if((Double.valueOf(hour)>=18&&!MathUtil.isZero(Double.valueOf(bean.getCcy2Amt())))||Math.abs(Double.valueOf(bean.getCcy2Amt()))>=Double.valueOf(amt)){//晚上六点以后或者金额达到一定数量
					SlFxSptFwdBean slFxSptFwdBean = new SlFxSptFwdBean("01", SlDealModule.FXD.SERVER, SlDealModule.FXD.TAG, SlDealModule.FXD.DETAIL);
					//交易日期、起息日
					Map<String, Object> mapHldy=new HashMap<String, Object>();
					mapHldy.put("vDate", bean.getDealDate().substring(0, 10));
					mapHldy.put("ccy1", bean.getCcy1());
					//mapHldy.put("ccy1","CNY");//0905改为查CNY的节假日
					slFxSptFwdBean.setDealDate(DateUtil.parse(ccyMapper.searchDate(mapHldy), "yyyy-MM-dd"));
					slFxSptFwdBean.setValueDate(DateUtil.parse(ccyMapper.searchDate(mapHldy), "yyyy-MM-dd"));
					//交易对手 
					InstitutionBean contraPatryInstitutionInfo = new InstitutionBean();
					contraPatryInstitutionInfo.setInstitutionId("100001");
					slFxSptFwdBean.setContraPatryInstitutionInfo(contraPatryInstitutionInfo);
					//本方交易员
					InstitutionBean institutionInfo = new InstitutionBean();
					if("5".equals(bean.getBusType())){//5-自身结售汇税金结转
						institutionInfo.setTradeId("ZSSJ");
					}else if("6".equals(bean.getBusType())){//6-自身结售汇未结转利润分配
						institutionInfo.setTradeId("ZSLR");
					}else{//1-代客结售汇,2-代客外汇买卖
						institutionInfo.setTradeId("SING");
					}
					slFxSptFwdBean.setInstitutionInfo(institutionInfo);
					//金额和币种 
	    			FxCurrencyBean currencyInfo = new FxCurrencyBean();	
	    			//买卖方向
					String ps = bean.getDirection();
	    			if(Double.valueOf(bean.getCcy2Amt())<0){//冲销交易
	    				currencyInfo.setCcy(bean.getCcy1()); // 主要币种
						currencyInfo.setContraCcy(bean.getCcy2()); // 次要币种
						currencyInfo.setAmt(BigDecimal.valueOf(MathUtil.roundWithNaN(Math.abs(Double.valueOf(bean.getCcy1Amt())),2))); // 主要币种金额
						currencyInfo.setContraAmt(BigDecimal.valueOf(MathUtil.roundWithNaN(Math.abs(Double.valueOf(bean.getCcy2Amt())),2))); // 次要币种金额
						//调换方向
						if (ps.endsWith("P")) {
							slFxSptFwdBean.setPs(PsEnum.S);
						}
						if (ps.endsWith("S")) {
							slFxSptFwdBean.setPs(PsEnum.P);
						}
	    			}else{
	    				currencyInfo.setCcy(bean.getCcy1()); // 主要币种
						currencyInfo.setContraCcy(bean.getCcy2()); // 次要币种
						currencyInfo.setAmt(BigDecimal.valueOf(MathUtil.roundWithNaN(Double.valueOf(bean.getCcy1Amt()),2))); // 主要币种金额
						currencyInfo.setContraAmt(BigDecimal.valueOf(MathUtil.roundWithNaN(Double.valueOf(bean.getCcy2Amt()),2))); // 次要币种金额
						if (ps.endsWith("P")) {
							slFxSptFwdBean.setPs(PsEnum.P);
						}
						if (ps.endsWith("S")) {
							slFxSptFwdBean.setPs(PsEnum.S);
						}
	    			}
					//点差
					currencyInfo.setPoints(BigDecimal.valueOf(0));
					slFxSptFwdBean.setCurrencyInfo(currencyInfo);
					
					//设置流水号
					HashMap<String, Object> hashMap = new HashMap<String, Object>();
					String fedealno = ccyMapper.getFedealno(hashMap);
					//SlExternalBean
					SlExternalBean externalBean = new SlExternalBean();
					if("1".equals(bean.getBusType())){//1-代客结售汇
						externalBean.setCost("1010111000");//即期代客-结售汇
					}else if("2".equals(bean.getBusType())){//2-代客外汇买卖
						externalBean.setCost("1010211000");//即期代客-外汇
					}else if("5".equals(bean.getBusType())){//5-自身结售汇税金结转
						externalBean.setCost("1050115010");//即期自身-税金结转
					}else if("6".equals(bean.getBusType())){//6-自身结售汇未结转利润分配
						externalBean.setCost("1050115020");//即期自身-未分配利润
					}
					
					externalBean.setProdcode("FXD");
					externalBean.setProdtype("FX");
					if("5".equals(bean.getBusType())|| "6".equals(bean.getBusType())){//5-自身结售汇税金结转,6-自身结售汇未结转利润分配
						externalBean.setPort("FXZS");
					}else{//1-代客结售汇,2-代客外汇买卖
						externalBean.setPort("FXDK");
					}
					externalBean.setBroker(SlDealModule.BROKER);
					externalBean.setCustrefno(fedealno);
					externalBean.setDealtext(fedealno);
					externalBean.setAuthsi(SlDealModule.FXD.AUTHSI);
					externalBean.setSiind(SlDealModule.FXD.SIIND);
					externalBean.setSupconfind(SlDealModule.FXD.SUPCONFIND);
					externalBean.setSuppayind(SlDealModule.FXD.SUPPAYIND);
					externalBean.setSuprecind(SlDealModule.FXD.SUPRECIND);
					externalBean.setVerind(SlDealModule.FXD.VERIND);
					slFxSptFwdBean.setExternalBean(externalBean);
					//SlInthBean
					SlInthBean inthBean = slFxSptFwdBean.getInthBean();
					inthBean.setFedealno(fedealno);
					inthBean.setLstmntdate(sysDate);
					slFxSptFwdBean.setInthBean(inthBean);
					
					// 插入opics表
					SlOutBean result2 = fxdServer.fxSptFwd(slFxSptFwdBean);// 用注入的方式调用opics相关方法

					if (result2.getRetStatus().equals(RetStatusEnum.S)) {
						logger.info("插入opics表成功......");
						String id=bean.getForeignccyId();
						String[] ids =  id.split(",");
						for (int i = 0; i < ids.length; i++) {
							bean.setSyncStatus("1");
							bean.setForeignccyId(ids[i]);
							bean.setFedealno(fedealno);
							ccyMapper.updateSyncStatus(bean);
						}
					} else {
						logger.info("插入opics表失败......");
					}
				}
			}
		}else{
			logger.info("*********字典项未设置金额参数**********");
		}
		logger.info("********** 外汇代客发送opics结束***********");
		logger.info("*********更新外汇代客opics返回状态**********");
		List<IfsForeignccyDish> synclist=ccyMapper.searchSyncDishList();
		for (IfsForeignccyDish ifsForeignccyDish : synclist) {
			SlInthBean inthBean = new SlInthBean();
			inthBean.setServer(SlDealModule.FXD.SERVER);
			inthBean.setTag(SlDealModule.FXD.TAG);
			inthBean.setBr("01");
			inthBean.setFedealno(ifsForeignccyDish.getFedealno());
			// 正常情况下单笔模块只会查询到一笔数据
			SlInthBean slInthBean = baseServer.getInthStatus(inthBean);
			// 根据fedealno回查更新状态
			if (slInthBean != null) {
				Map<String, Object> map2 = new HashMap<String, Object>();
				map2.put("fedealno", slInthBean.getFedealno());
				map2.put("statcode", slInthBean.getStatcode());
				map2.put("errorcode", slInthBean.getErrorcode());
				map2.put("dealno", slInthBean.getDealno());
				if("-4".equals(StringUtils.trimToEmpty(slInthBean.getStatcode()))){
					map2.put("syncStatus", "2");
				}else if("-6".equals(StringUtils.trimToEmpty(slInthBean.getStatcode()))){
					map2.put("syncStatus", "3");
				}
				ccyMapper.updateByFedealno(map2);
			}
		}
		logger.info("==================手工处理任务结束==============================");
	
        return RetMsgHelper.ok(result);
    }
    
    @ResponseBody
    @RequestMapping(value = "/createGTPFile")
    public RetMsg<SlOutBean> createGTPFile(@RequestBody Map<String, String> params) throws RemoteConnectFailureException, Exception {
  
       logger.info("==================开始执行文件处理==============================");
		
		Map<String, Object> map=new HashMap<String, Object>();
		//根据OPICS系统日期查询
		Date sysDate=baseServer.getOpicsSysDate("01");
		String date=DateUtil.format(sysDate, "yyyyMMdd");
		//map.put("queryDate",date);
		if(""!=params.get("queryDate") && null != params.get("queryDate")){
		    String queryDate =   params.get("queryDate").split("T")[0].replace("-", "");
		        map.put("queryDate",queryDate);
	     }else {
	        	 map.put("queryDate",date);
	     }
		SlOutBean result =  createGTPFileServer.createGTPFile(map);
		String retCode = result.getRetCode();
		if(	null != retCode && "s".equals(retCode)){
			logger.info("==================数据仓库文件生成成功==============================");
		}else {
			logger.info("==================数据仓库文件生成失败==============================");
		}	
		logger.info("==================文件任务结束==============================");
    	 
		return RetMsgHelper.ok(result);
    }
   
  
    
    

   
}