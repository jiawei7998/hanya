package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.ifs.model.IfsApprovermbIbo;
import com.singlee.ifs.service.IFSApproveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.io.Serializable;
import java.util.Map;

/**
 * 信用拆借事前审批交易Controller
 * 
 * ClassName: IfsApprovermbIboController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason: TODO ADD REASON(可选). <br/>
 * date: 2018-5-30 下午07:10:30 <br/>
 * 
 * @author zhangcm
 * @version
 * @since JDK 1.6
 */
@Controller
@RequestMapping(value = "/IfsApprovermbIboController")
public class IfsApprovermbIboController extends CommonController {
	@Autowired
	private IFSApproveService cfetsrmbIboService;

	/**
	 * 保存成交单
	 * 
	 * @param IfsCfetsrmbIbo
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/saveIbo")
	public RetMsg<Serializable> saveIbo(@RequestBody IfsApprovermbIbo record) throws IOException {
		IfsApprovermbIbo ibo = cfetsrmbIboService.getIboById(record);
		if (ibo == null) {
			cfetsrmbIboService.insertIbo(record);
		} else {
			cfetsrmbIboService.updateIbo(record);
		}
		return RetMsgHelper.ok();
	}

	/**
	 * 成交单分页查询
	 * 
	 * @param IfsCfetsrmbIbo
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchCfetsrmbIbo")
	public RetMsg<PageInfo<IfsApprovermbIbo>> searchCfetsrmbIbo(@RequestBody IfsApprovermbIbo record) {
		Page<IfsApprovermbIbo> page = cfetsrmbIboService.getIboList(record);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 根据主键查询成交单
	 * 
	 * @param IfsCfetsrmbIboKey
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchCfetsrmbIboById")
	public RetMsg<IfsApprovermbIbo> searchCfetsrmbIboById(@RequestBody IfsApprovermbIbo key) {
		IfsApprovermbIbo ibo = cfetsrmbIboService.getIboById(key);
		return RetMsgHelper.ok(ibo);
	}

	/**
	 * 删除成交单
	 * 
	 * @param IfsCfetsrmbIboKey
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/deleteCfetsrmbIbo")
	public RetMsg<Serializable> deleteCfetsrmbIbo(@RequestBody IfsApprovermbIbo key) {
		cfetsrmbIboService.deleteIbo(key);
		return RetMsgHelper.ok();
	}

	/**
	 * 查询我发起的
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-21
	 */
	@ResponseBody
	@RequestMapping(value = "/searchRmbIboSwapMine")
	public RetMsg<PageInfo<IfsApprovermbIbo>> searchRmbIboSwapMine(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsApprovermbIbo> page = cfetsrmbIboService.getRmbIboMinePage(params, 3);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 查询 待审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageRmbIboUnfinished")
	public RetMsg<PageInfo<IfsApprovermbIbo>> searchPageRmbIboUnfinished(@RequestBody Map<String, Object> params) {
		String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse + ","
				+ DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
		String[] orderStatus = approveStatus.split(",");
		params.put("approveStatusNo", orderStatus);
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsApprovermbIbo> page = cfetsrmbIboService.getRmbIboMinePage(params, 1);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 查询 已审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageRmbIboFinished")
	public RetMsg<PageInfo<IfsApprovermbIbo>> searchPageRmbIboFinished(@RequestBody Map<String, Object> params) {
		String approveStatusNo = ParameterUtil.getString(params, "approveStatus", "");
		if (StringUtil.isNullOrEmpty(approveStatusNo)) {
			approveStatusNo = DictConstants.ApproveStatus.ApprovedPass;
			approveStatusNo += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatusNo += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatusNo += "," + DictConstants.ApproveStatus.Verified;
			approveStatusNo += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			// 20180502修改bug:已审批列表中增加"审批中"
			approveStatusNo += "," + DictConstants.ApproveStatus.Approving;
			String[] orderStatus = approveStatusNo.split(",");
			params.put("approveStatusNo", orderStatus);
		}
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsApprovermbIbo> page = cfetsrmbIboService.getRmbIboMinePage(params, 2);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 根据主键查询成交单(返回值增加 流程id)
	 * 
	 * @param
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchCfetsrmbIboAndFlowIdById")
	public RetMsg<IfsApprovermbIbo> searchCfetsrmbIboAndFlowIdById(@RequestBody Map<String, Object> key) {
		IfsApprovermbIbo ibo = cfetsrmbIboService.getIboAndFlowIdById(key);
		return RetMsgHelper.ok(ibo);
	}

	/**
	 * 勾兑时，查询表中审批通过的数据
	 * 
	 * @param
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchPageApprovermbIboPassed")
	public RetMsg<PageInfo<IfsApprovermbIbo>> searchPageApprovermbIboPassed(@RequestBody Map<String, Object> params) {
		Page<IfsApprovermbIbo> page = cfetsrmbIboService.getApproveIboPassedPage(params);
		return RetMsgHelper.ok(page);
	}
}
