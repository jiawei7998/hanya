package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.ifs.model.IfsRevIrvv;
import com.singlee.ifs.service.IFSService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.Map;

/**
 * 模块冲销单Controller
 * 
 * @author fangck
 * 
 * @date 2018-07-24
 * @version
 * @since JDK 1.6
 */
@Controller
@RequestMapping(value = "/IfsRevIrvvController")
public class IfsRevIrvvController {
	@Autowired
	private IFSService iFSService;
	

	/***
	 * 分页查询
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageIrvv")
	public RetMsg<PageInfo<IfsRevIrvv>> searchForm(@RequestBody Map<String, Object> map) {
		Page<IfsRevIrvv> page = iFSService.searchPageIrvv(map);
		return RetMsgHelper.ok(page);	
	}
	/**
	 * 查询 拆借冲销  我发起的
	 */
	@ResponseBody
	@RequestMapping(value = "/searchIrvvMine")
	public RetMsg<PageInfo<IfsRevIrvv>> searchMine(@RequestBody Map<String, Object> map) {
		map.put("isActive", DictConstants.YesNo.YES);
		map.put("userId", SlSessionHelper.getUserId());
		Page<IfsRevIrvv> page = iFSService.searchIrvv(map,3);
		return RetMsgHelper.ok(page);	
	}

	/**
	 * 查询 拆借冲销  待审批
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchIrvvUnfinished")
	public RetMsg<PageInfo<IfsRevIrvv>> searchUnfinished(@RequestBody Map<String, Object> map) {
		String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + ","
				+ DictConstants.ApproveStatus.VerifyRefuse + "," + DictConstants.ApproveStatus.WaitVerify + "," 
				+ DictConstants.ApproveStatus.Verified;
		String[] orderStatus = approveStatus.split(",");
		map.put("approveStatusNo", orderStatus);
		map.put("isActive", DictConstants.YesNo.YES);
		map.put("userId", SlSessionHelper.getUserId());
		Page<IfsRevIrvv> page = iFSService.searchIrvv(map,1);
		return RetMsgHelper.ok(page);	
	}
	
	/**
	 * 查询 拆借冲销  已审批
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchIrvvFinished")
	public RetMsg<PageInfo<IfsRevIrvv>> searchFinished(@RequestBody Map<String, Object> map) {
		String approveStatus = ParameterUtil.getString(map, "approveStatus", "");
		if (StringUtil.isNullOrEmpty(approveStatus)) {
			approveStatus = DictConstants.ApproveStatus.ApprovedPass;
			approveStatus += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatus += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatus += "," + DictConstants.ApproveStatus.Verified;
			approveStatus += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			approveStatus += "," + DictConstants.ApproveStatus.Approving;

			String[] orderStatus = approveStatus.split(",");
			map.put("approveStatusNo", orderStatus);
		}
		map.put("isActive", DictConstants.YesNo.YES);
		map.put("userId", SlSessionHelper.getUserId());
		Page<IfsRevIrvv> page = iFSService.searchIrvv(map,2);
		return RetMsgHelper.ok(page);	
	}
	
	
	/**
	 * 删除
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteIrvv")
	public RetMsg<Serializable> deleteProd(@RequestBody Map<String, String> map) {
		String ticketId = map.get("ticketId");
		iFSService.deleteById(ticketId);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 保存 拆借类要素
	 */
	@ResponseBody
	@RequestMapping(value = "/saveLendParam")
	public RetMsg<Serializable> saveForexParam(@RequestBody IfsRevIrvv entity) {
		String ticketId = entity.getTicketId();
		if (null == ticketId || "".equals(ticketId)) {// 新增
			iFSService.insert(entity);
		} else {// 修改
			iFSService.updateById(entity);
		}
		return RetMsgHelper.ok();
	}

	/*@ResponseBody
	@RequestMapping(value = "/searchProd")
	public RetMsg<List<IfsOpicsProd>> searchProd(@RequestBody HashMap<String, Object> map) throws RException {
		List<IfsOpicsProd> list = iFSService.searchProd(map);
		return RetMsgHelper.ok(list);
	}*/
}
