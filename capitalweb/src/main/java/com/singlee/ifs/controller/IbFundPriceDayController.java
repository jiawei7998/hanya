package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.base.util.FileManage;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.UploadedFile;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.spring.mybatis.BatchDao;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.hrbextra.wind.model.CmFincome;
import com.singlee.hrbextra.wind.service.CmFincomeService;
import com.singlee.ifs.model.ChinaMutualFundNAV;
import com.singlee.ifs.model.IbFundPriceDay;
import com.singlee.ifs.service.IbFundPriceDayService;
import com.singlee.ifs.service.IfsWdFundService;
import com.singlee.refund.service.CFtInfoService;
import jxl.Sheet;
import jxl.Workbook;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author ：chenguo
 * @date ：Created in 2021/10/14 16:12
 * @description：
 * @modified By：
 * @version:
 */

@Controller
@RequestMapping(value = "/IbFundPriceDayController")
public class IbFundPriceDayController {

	@Autowired
	IbFundPriceDayService ibFundPriceDayService;
	@Autowired
	IfsWdFundService ifsWdFundService;
	@Autowired
	CmFincomeService cmFincomeService;
	@Autowired
	BatchDao batchDao;
	@Autowired
	CFtInfoService cFtInfoService;


	/***
	 * 分页查询
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPagePriceDay")
	public RetMsg<PageInfo<IbFundPriceDay>> searchPagePriceDay(@RequestBody Map<String, Object> map) {
		String fundCode = String.valueOf(map.get("fundCode"));
//        CFtInfo cFtInfo =cFtInfoService.searchCFtInfoByFundCode(fundCode);
//        if (cFtInfo==null){
//            map.put("fundCode","");
//        }
		Page<IbFundPriceDay> page = ibFundPriceDayService.getFundPricePage(map);
		return RetMsgHelper.ok(page);
	}


	/***
	 * 债券信息 分页查询
	 */
	@ResponseBody
	@RequestMapping(value = "/deletePriceDay")
	public RetMsg<Serializable> deletePriceDay(@RequestBody IbFundPriceDay ibFundPriceDay) {
		ibFundPriceDayService.delete(ibFundPriceDay);
		return RetMsgHelper.ok();
	}


	/***
	 * 手工同步wind每日基金净值和万份收益到FT_PRICE_DAILY表中
	 */
	@ResponseBody
	@RequestMapping(value = "/dataToPriceDaily")
	public RetMsg<Serializable> dataToPriceDaily(@RequestBody Map<String, Object> map) {
		Date postdate = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		String annDate = sdf.format(postdate);
		map.put("annDate", annDate);
		List<IbFundPriceDay> priceList = new ArrayList<>();
		//查询今天的基金净值信息
		List<ChinaMutualFundNAV> listNav = ifsWdFundService.getWdFundAll(map);
		if (listNav.size() > 0) {
			for (ChinaMutualFundNAV chinaMutualFundNAV : listNav) {
				String date = chinaMutualFundNAV.getANN_DATE().trim();
				StringBuffer sb = new StringBuffer(date);
				sb.insert(6, "-");
				sb.insert(4, "-");


				IbFundPriceDay ibFundPriceDay = new IbFundPriceDay();
				ibFundPriceDay.setFundPrice(chinaMutualFundNAV.getF_NAV_UNIT());//单位净值/万分收益率
				ibFundPriceDay.setFundCode(chinaMutualFundNAV.getF_INFO_WINDCODE());//基金代码
				ibFundPriceDay.setPostdate(String.valueOf(sb));//账务日期
				priceList.add(ibFundPriceDay);
			}
		}
		//查询今天的基金万份收益
		List<CmFincome> listCm = cmFincomeService.getCmFincomeList(map);
		if (listCm.size() > 0) {
			for (CmFincome cmFincome : listCm) {
				IbFundPriceDay ibFundPriceDay = new IbFundPriceDay();
				String dateAnn = cmFincome.getANN_DATE().trim();
				StringBuffer sbd = new StringBuffer(dateAnn);
				sbd.insert(6, "-");
				sbd.insert(4, "-");
				ibFundPriceDay.setFundPrice(cmFincome.getF_INFO_UNITYIELD());//单位净值/万分收益率
				ibFundPriceDay.setFundCode(cmFincome.getS_INFO_WINDCODE());//基金代码
				ibFundPriceDay.setPostdate(String.valueOf(sbd));//账务日期
				priceList.add(ibFundPriceDay);
			}
		}
		if (priceList.size() > 0) {
			batchDao.batch("com.singlee.ifs.mapper.IbFundPriceDayMapper.delete", priceList);
			batchDao.batch("com.singlee.ifs.mapper.IbFundPriceDayMapper.insert", priceList);
		}

		return RetMsgHelper.ok();
	}

	/**
	 * 节假日模板导出
	 */
	@ResponseBody
	@RequestMapping(value = "/PriceDailyExcel")
	public void PriceDailyExcel(HttpServletRequest request,
	                            HttpServletResponse response) {
		OutputStream out = null;
		FileInputStream in = null;

		try {
			String fileName = "基金净值收益率模板";
			// 读取模板
			String excelPath = request.getSession().getServletContext().getRealPath("standard/PriceDailyExcel/PriceDaily.xls");

			fileName = URLEncoder.encode(fileName, "UTF-8");

			response.reset();
			// 追加时间
			response.addHeader("Content-Disposition", "attachment;filename="
					+ fileName + ".xls");
			response.setContentType("application/octet-stream;charset=UTF-8");

			out = response.getOutputStream();
			in = new FileInputStream(excelPath);

			byte[] b = new byte[1024];
			int len;

			while ((len = in.read(b)) > 0) {
				response.getOutputStream().write(b, 0, len);
			}
			out.flush();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (null != in) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				in = null;
			}
			if (null != out) {
				try {
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				out = null;
			}
		}
	}


    /**
	 * Excel上传基金净值收益率信息
	 */
	@ResponseBody
	@RequestMapping(value = "/ImportPriceDailyByExcel", method = RequestMethod.POST, produces = "text/plain;charset=UTF-8")
	public String ImportPriceDailyByExcel(MultipartHttpServletRequest request, HttpServletResponse response) {
		// 1.文件获取
		List<UploadedFile> uploadedFileList = null;
		String ret = "导入失败";
		Workbook wb = null;
		List<IbFundPriceDay> fundBean = new ArrayList<>();
		try {
			uploadedFileList = FileManage.requestExtractor(request);
			// 2.校验并处理成excel
			List<Workbook> excelList = new LinkedList<Workbook>();
			for (UploadedFile uploadedFile : uploadedFileList) {
				if (!"xls".equals(uploadedFile.getType())) {
					return ret = "上传文件类型错误,仅允许上传xls格式文件";
				}
				Workbook book = Workbook.getWorkbook(new ByteArrayInputStream(
						uploadedFile.getBytes()));
				excelList.add(book);
			}
			if (excelList.size() != 1) {
				return ret = "只能处理一份excel";
			}
			wb = excelList.get(0);
			Sheet[] sheets = wb.getSheets();// 获得卡片sheet数量
			String postdate = DateUtil.getCurrentDateAsString("yyyy-MM-dd");
			for (Sheet sheet : sheets) {
				int rowCount = sheet.getRows();// 行数


				for (int i = 1; i < rowCount; i++) { // 从第二行开始
					IbFundPriceDay ibFundPriceDay = new IbFundPriceDay();

					if (StringUtils.trimToEmpty(sheet.getCell(1, i).getContents()).equals("")) {
						continue;
					}

//
					ibFundPriceDay.setFundCode(StringUtils.trimToEmpty(sheet
							.getCell(0, i).getContents()));// A
					ibFundPriceDay.setFundPrice(new BigDecimal(StringUtils.trimToEmpty(sheet
							.getCell(1, i).getContents())));// B
					ibFundPriceDay.setPostdate(StringUtils.trimToEmpty(sheet
							.getCell(2, i).getContents()));// C

					fundBean.add(ibFundPriceDay);
				}
			}
			//插入基金信息表
			ret = ibFundPriceDayService.doExcelToData(fundBean);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RException(e);
		} finally {
			if (null != wb) {
				wb.close();
			}
		}
		return ret;// 返回前台展示配置
	}


}
