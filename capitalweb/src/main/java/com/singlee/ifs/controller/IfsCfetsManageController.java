package com.singlee.ifs.controller;

import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.financial.cfets.fx.IImixFxManageServer;
import com.singlee.financial.cfets.rmb.IImixRmbManageServer;
import com.singlee.ifs.baseUtil.SingleeJschUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.lang.management.ManagementFactory;

@Controller
@RequestMapping(value = "/IfsCfetsManageController")
public class IfsCfetsManageController extends CommonController {

	@Autowired
	private IImixFxManageServer iImixFxManageServer;

	@Autowired
	private IImixRmbManageServer IImixRmbManageServer;

	private static Logger logger = Logger.getLogger(IfsCfetsManageController.class);

	/**
	 * 启动
	 */
	@ResponseBody
	@RequestMapping(value = "/startFxImixSession")
	public RetMsg<Boolean> startFxImixSession() throws IOException {
		return RetMsgHelper.ok(iImixFxManageServer.startImixSession());
	}

	/**
	 * 停止
	 */
	@ResponseBody
	@RequestMapping(value = "/stopFxImixSession")
	public RetMsg<Boolean> stopFxImixSession() throws IOException {
		return RetMsgHelper.ok(iImixFxManageServer.stopImixSession());
	}

	/**
	 * 启动
	 */
	@ResponseBody
	@RequestMapping(value = "/startRmbImixSession")
	public RetMsg<Boolean> startRmbImixSession() throws IOException {
		return RetMsgHelper.ok(IImixRmbManageServer.startImixSession());
	}

	/**
	 * 停止
	 */
	@ResponseBody
	@RequestMapping(value = "/stopRmbImixSession")
	public RetMsg<Boolean> stopRmbImixSession() throws IOException {
		return RetMsgHelper.ok(IImixRmbManageServer.stopImixSession());
	}

	/**
	 * 启动外币客户端
	 */
	@ResponseBody
	@RequestMapping(value = "/startCstpFxClient")
	public RetMsg<Serializable> startCstpFxClient() throws IOException {
		String host = SystemProperties.cstpVmIp;
		String user = SystemProperties.cstpVmUsername;
		String psw = SystemProperties.cstpVmPassword;
		int port = SystemProperties.cstpVmPort;
		// 外币
		String cstpFxHome = SystemProperties.cstpFxHome;
		String cstpFxAgentStart = SystemProperties.cstpFxAgentStart;
		String cstpFxClientStart = SystemProperties.cstpFxClientStart;
		StringBuffer commSb = new StringBuffer();
		if (!"".equals(cstpFxAgentStart)) {
			commSb.append(cstpFxHome + ";" + cstpFxAgentStart + "  ");// 启动外币AGENT
		}
		if (!"".equals(cstpFxClientStart)) {
			commSb.append(cstpFxHome + ";" + cstpFxClientStart);// 启动CLIENT
		}
		logger.debug("CSTP IMIX START COMMAND ===>" + commSb);
		return RetMsgHelper.ok(SingleeJschUtil.exec(host, user, psw, port, commSb.toString()));
	}

	/**
	 * 启动本币客户端
	 */
	@ResponseBody
	@RequestMapping(value = "/startCstpRmbClient")
	public RetMsg<Serializable> startCstpRmbClient() throws IOException {
		String host = SystemProperties.cstpVmIp;
		String user = SystemProperties.cstpVmUsername;
		String psw = SystemProperties.cstpVmPassword;
		int port = SystemProperties.cstpVmPort;
		// 本币
		String cstpRmbHome = SystemProperties.cstpRmbHome;
		String cstpRmbAgentStart = SystemProperties.cstpRmbAgentStart;
		String cstpRmbClientStart = SystemProperties.cstpRmbClientStart;
		StringBuffer commSb = new StringBuffer();
		if (!"".equals(cstpRmbAgentStart)) {
			commSb.append(cstpRmbHome + ";" + cstpRmbAgentStart + "  ");// 启动外币AGENT
		}
		if (!"".equals(cstpRmbClientStart)) {
			commSb.append(cstpRmbHome + ";" + cstpRmbClientStart);// 启动CLIENT
		}
		logger.debug("CSTP IMIX START COMMAND ===>" + commSb);
		return RetMsgHelper.ok(SingleeJschUtil.exec(host, user, psw, port, commSb.toString()));
	}

	/**
	 * 启动交易后确认
	 */
	@ResponseBody
	@RequestMapping(value = "/startCPISClient")
	public RetMsg<Serializable> startCPISClient() throws IOException{
		String host = SystemProperties.cstpVmIp;
		String user = SystemProperties.cstpVmUsername;
		String psw = SystemProperties.cstpVmPassword;
		int port = SystemProperties.cstpVmPort;
		StringBuffer commSb = new StringBuffer();

		//交易后确认
		String cpisClientHome = SystemProperties.cpisClientHome;
		String cpisClientStart = SystemProperties.cpisClientStart;
		if (!"".equals(cpisClientStart)) {
			commSb.append(cpisClientHome + ";" + cpisClientStart);// 启动CLIENT
		}
		logger.debug("CPIS IMIX START COMMAND ===>" + commSb);

		return RetMsgHelper.ok(SingleeJschUtil.exec(host, user, psw, port, commSb.toString()));
	}

	/**
	 * 停止交易后确认
	 */
	@ResponseBody
	@RequestMapping(value = "/stopCpisClient")
	public RetMsg<Serializable> stopCpisClient() throws IOException{
		String host = SystemProperties.cstpVmIp;
		String user = SystemProperties.cstpVmUsername;
		String psw = SystemProperties.cstpVmPassword;
		int port = SystemProperties.cstpVmPort;

		// 交易后确认
		String cpisStop = SystemProperties.cpisStop;
		logger.debug("CSTP IMIX STOP COMMAND ===>" + cpisStop);
		return RetMsgHelper.ok(SingleeJschUtil.exec(host, user, psw, port, cpisStop));
	}

	/**
	 * 停止外币客户端
	 */
	@ResponseBody
	@RequestMapping(value = "/stopCstpFxClient")
	public RetMsg<Serializable> stopCstpFxClient() throws IOException {
		String host = SystemProperties.cstpVmIp;
		String user = SystemProperties.cstpVmUsername;
		String psw = SystemProperties.cstpVmPassword;
		int port = SystemProperties.cstpVmPort;
		// 外币
		String cstpFxStop = SystemProperties.cstpFxStop;
		logger.debug("CSTP IMIX STOP COMMAND ===>" + cstpFxStop);
		return RetMsgHelper.ok(SingleeJschUtil.exec(host, user, psw, port, cstpFxStop));
	}

	/**
	 * 停止本币客户端
	 */
	@ResponseBody
	@RequestMapping(value = "/stopCstpRmbClient")
	public RetMsg<Serializable> stopCstpRmbClient() throws IOException {
		String host = SystemProperties.cstpVmIp;
		String user = SystemProperties.cstpVmUsername;
		String psw = SystemProperties.cstpVmPassword;
		int port = SystemProperties.cstpVmPort;
		// 本币
		String cstpRmbStop = SystemProperties.cstpRmbStop;
		logger.debug("CSTP IMIX STOP COMMAND ===>" + cstpRmbStop);
		return RetMsgHelper.ok(SingleeJschUtil.exec(host, user, psw, port, cstpRmbStop));
	}

	/**
	 * 查询外币客户端是否停止
	 */
	@ResponseBody
	@RequestMapping(value = "/getCstpFxStatus")
	public RetMsg<Serializable> getCstpFxStatus() throws IOException {
		String host = SystemProperties.cstpVmIp;
		String user = SystemProperties.cstpVmUsername;
		String psw = SystemProperties.cstpVmPassword;
		int port = SystemProperties.cstpVmPort;
		String cstpFxConsole = SystemProperties.cstpFxConsole;
		logger.debug("CSTP IMIX STATUS COMMAND ===>" + cstpFxConsole);
		return RetMsgHelper.ok(SingleeJschUtil.exec(host, user, psw, port, cstpFxConsole));
	}

	/**
	 * 查询外币客户端是否停止
	 */
	@ResponseBody
	@RequestMapping(value = "/getCstpRmbStatus")
	public RetMsg<Serializable> getCstpRmbStatus() throws IOException {
		String host = SystemProperties.cstpVmIp;
		String user = SystemProperties.cstpVmUsername;
		String psw = SystemProperties.cstpVmPassword;
		int port = SystemProperties.cstpVmPort;
		String cstpRmbConsole = SystemProperties.cstpRmbConsole;
		logger.debug("CSTP IMIX STATUS COMMAND ===>" + cstpRmbConsole);
		return RetMsgHelper.ok(SingleeJschUtil.exec(host, user, psw, port, cstpRmbConsole));
	}

	/**
	 * 查询交易后确认客户端是否停止
	 */
	@ResponseBody
	@RequestMapping(value = "/getCpisStatus")
	public RetMsg<Serializable> getCpisStatus() throws IOException {
		String host = SystemProperties.cstpVmIp;
		String user = SystemProperties.cstpVmUsername;
		String psw = SystemProperties.cstpVmPassword;
		int port = SystemProperties.cstpVmPort;
		String cpisConsole = SystemProperties.cpisConsole;
		logger.debug("CSTP IMIX STATUS COMMAND ===>" + cpisConsole);
		return RetMsgHelper.ok(SingleeJschUtil.exec(host, user, psw, port, cpisConsole));
	}


	@ResponseBody
	@RequestMapping(value = "/startPort")
	public RetMsg<Boolean> startPort() throws IOException {
		/**
		 * 调用shell脚本
		 */
		String filepath = SystemProperties.cstpStartPath; // 读取配置文件(启动)
		StringBuffer sb = new StringBuffer();
		BufferedReader br = null;
		String name = ManagementFactory.getRuntimeMXBean().getName();
		String pid = name.split("@")[0];
		String line = "";
		try {
			logger.info("Starting to exec{" + filepath + "}. PID is: " + pid);
			Process process = null;
			ProcessBuilder pb = new ProcessBuilder("/bin/bash", "-c", filepath);
			pb.environment();
			pb.redirectErrorStream(true);
			process = pb.start();
			if (process != null) {
				br = new BufferedReader(new InputStreamReader(process.getInputStream()), 1024);
				process.waitFor();
			} else {
				logger.info("There is no PID found.");
			}
			sb.append("Ending exec right now, the result is：\n");
			while (br != null && (line = br.readLine()) != null) {
				sb.append(line).append("\n");
			}
		} catch (Exception ioe) {
			sb.append("Error occured when exec cmd：\n").append(ioe.getMessage()).append("\n");
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					logger.info(e);
				}
			}
		}
		logger.info(sb.toString());
		return RetMsgHelper.ok(true);
	}

	@ResponseBody
	@RequestMapping(value = "/stopPort")
	public RetMsg<Boolean> stopPort() throws IOException {
		/**
		 * 调用shell脚本
		 */
		String filepath = SystemProperties.cstpStopPath; // 读取配置文件(停止)
		StringBuffer sb = new StringBuffer();
		BufferedReader br = null;
		String name = ManagementFactory.getRuntimeMXBean().getName();
		String pid = name.split("@")[0];
		String line = "";
		try {
			logger.info("Starting to exec{" + filepath + "}. PID is: " + pid);
			Process process = null;
			ProcessBuilder pb = new ProcessBuilder("/bin/bash", "-c", filepath);
			pb.environment();
			pb.redirectErrorStream(true);
			process = pb.start();
			if (process != null) {
				br = new BufferedReader(new InputStreamReader(process.getInputStream()), 1024);
				process.waitFor();
			} else {
				logger.info("There is no PID found.");
			}
			sb.append("Ending exec right now, the result is：\n");
			while (br != null && (line = br.readLine()) != null) {
				sb.append(line).append("\n");
			}
		} catch (Exception ioe) {
			sb.append("Error occured when exec cmd：\n").append(ioe.getMessage()).append("\n");
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					logger.info(e);
				}
			}
		}
		logger.info(sb.toString());
		return RetMsgHelper.ok(true);
	}



}