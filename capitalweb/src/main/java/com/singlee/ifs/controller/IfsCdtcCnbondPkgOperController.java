package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.Pair;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.financial.bean.AbstractSlIntfcCnbondPkgId;
import com.singlee.financial.bean.CnbondPkgDetailBean;
import com.singlee.financial.bean.SmtOutBean;
import com.singlee.financial.cdtc.CdtcCnbondPkgOperServer;
import com.singlee.financial.pojo.component.RetStatusEnum;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

/***
 * 中债直联——结算报文查询
 * 
 */
@Controller
@RequestMapping(value = "/IfsCdtcCnbondPkgOperController")
public class IfsCdtcCnbondPkgOperController {
	@Autowired
	private CdtcCnbondPkgOperServer cdtcCnbondPkgOperServer;
	private Logger logger = LoggerFactory.getLogger(IfsCdtcCnbondPkgOperController.class);

	/**
	 * 结算报文查询——列表
	 * 
	 */
	@ResponseBody
	@RequestMapping(value = "/cdtcCnbondPkgOperQry")
	public RetMsg<PageInfo<AbstractSlIntfcCnbondPkgId>> cdtcCnbondPkgOperQry(@RequestBody Map<String, String> map) {
		Pair<SmtOutBean, List<AbstractSlIntfcCnbondPkgId>> queryCdtcCnbondPkg = null;
		try {
			queryCdtcCnbondPkg = cdtcCnbondPkgOperServer.queryCdtcCnbondPkg(map);
		} catch (Exception e) {
			logger.info("结算报文查询失败", e);
		}

		SmtOutBean left = queryCdtcCnbondPkg.getLeft();
		Page<AbstractSlIntfcCnbondPkgId> page = new Page<AbstractSlIntfcCnbondPkgId>();
		// 查询结算报文成功
		if (left.getRetStatus().equals(RetStatusEnum.S)) {
			// 对结算报文进行分页
			RowBounds rb = ParameterUtil.getRowBounds(map);
			List<AbstractSlIntfcCnbondPkgId> right = queryCdtcCnbondPkg.getRight();
			int end = (rb.getOffset() + rb.getLimit()) > right.size() ? right.size() : (rb.getOffset() + rb.getLimit());
			page.setPages((int) Math.ceil(right.size() / (double) rb.getLimit()));
			page.addAll(right.subList(rb.getOffset(), end));
			page.setTotal(right.size());
		} else {
			// 查询结算报文失败
			logger.info("结算报文查询失败", left);
		}

		return RetMsgHelper.ok(page);
	}

	/**
	 * 结算报文查询——详情
	 * 
	 */
	@ResponseBody
	@RequestMapping(value = "/cdtcCnbondPkgOperDetail")
	public RetMsg<CnbondPkgDetailBean> cdtcCnbondPkgOperDetail(@RequestBody AbstractSlIntfcCnbondPkgId okg) {
		Pair<SmtOutBean, CnbondPkgDetailBean> cnbondPkgDetail = null;
		try {
			cnbondPkgDetail = cdtcCnbondPkgOperServer.getCnbondPkgDetail(okg);
		} catch (Exception e) {
			logger.info("结算报文详情查询失败", e);
		}
		return RetMsgHelper.ok(cnbondPkgDetail.getRight());
	}

}
