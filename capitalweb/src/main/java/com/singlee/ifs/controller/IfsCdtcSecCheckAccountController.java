package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.financial.bean.CdtcAcctTotalBean;
import com.singlee.financial.cdtc.CdtcSecCheckAccountServer;
import com.singlee.ifs.service.IfsSecAcctService;
import org.apache.ibatis.session.RowBounds;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 债券总对账单
 * 
 */
@Controller
@RequestMapping(value = "/IfsCdtcSecCheckAccountController")
public class IfsCdtcSecCheckAccountController {
	private static Logger logger = Logger.getLogger(IfsCdtcSecCheckAccountController.class);
	@Autowired
	private CdtcSecCheckAccountServer cdtcSecCheckAccountServer;
	@Autowired
	private IfsSecAcctService ifsSecAcctService;

	/**
	 *  查询债券总对账单
	 * 
	 */
	@ResponseBody
	@RequestMapping(value = "/cdtcSecCheckAccount")
	public RetMsg<PageInfo<CdtcAcctTotalBean>> cdtcSecCheckAccount(@RequestBody Map<String, Object> map) {
		map.put("acctText","CDC");
		map.put("secAcct", "CDC");
		String queryDate = (String) map.get("queryDate");
		if (queryDate != null && !"".equals(queryDate)) {
			try {
				queryDate = new SimpleDateFormat("yyyyMMdd")
						.format(new SimpleDateFormat("yyyy-MM-dd").parse(queryDate));
			} catch (ParseException e) {
				logger.info("queryDate日期格式转换失败", e);
			}
			map.put("queryDate", queryDate);
		}
		List<CdtcAcctTotalBean> secCheckAccount=new ArrayList<CdtcAcctTotalBean>();
//		secCheckAccount = cdtcSecCheckAccountServer.getSecCheckAccount(map);
		CdtcAcctTotalBean acctTotal=new CdtcAcctTotalBean();
		acctTotal.setSecidNm("国债20201201");
		acctTotal.setSecid("2020101201");
		acctTotal.setAbleUseAmt(new BigDecimal("88888888"));
		acctTotal.setWaitPayAmt(new BigDecimal("231231"));
		acctTotal.setRepoZYAmt(new BigDecimal("54643"));
		acctTotal.setRepoRevAmt(new BigDecimal("1000"));
		acctTotal.setFrozenAmt(new BigDecimal("190"));
		acctTotal.setTotalAmt(new BigDecimal("21312321"));
		acctTotal.setCxAmt(new BigDecimal("2131"));
		acctTotal.setCxWaitPayAmt(new BigDecimal("3131"));
		secCheckAccount.add(acctTotal);
		
		CdtcAcctTotalBean acctTotal2=new CdtcAcctTotalBean();
		acctTotal2.setSecidNm("浙债20201201");
		acctTotal2.setSecid("2020101201");
		acctTotal2.setAbleUseAmt(new BigDecimal("66666666666"));
		acctTotal2.setWaitPayAmt(new BigDecimal("112331"));
		acctTotal2.setRepoZYAmt(new BigDecimal("321343"));
		acctTotal2.setRepoRevAmt(new BigDecimal("51000"));
		acctTotal2.setFrozenAmt(new BigDecimal("1190"));
		acctTotal2.setTotalAmt(new BigDecimal("21312321"));
		acctTotal2.setCxAmt(new BigDecimal("21131"));
		acctTotal2.setCxWaitPayAmt(new BigDecimal("1131"));
		secCheckAccount.add(acctTotal2);
		
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<CdtcAcctTotalBean> page = new Page<CdtcAcctTotalBean>();
		page.setTotal(secCheckAccount.size());
		page.setPages((int) Math.ceil(secCheckAccount.size() / (double) rb.getLimit()));
		int end = (rb.getOffset() + rb.getLimit()) > secCheckAccount.size() ? secCheckAccount.size()
				: (rb.getOffset() + rb.getLimit());
		page.addAll(secCheckAccount.subList(rb.getOffset(), end));
		return RetMsgHelper.ok(page);

	}
}
