package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.ifs.model.IfsCdtcCnBondLinkSecurdeal;
import com.singlee.ifs.service.IfsCdtcCnBondLinkSecurdealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/***
 * 中债直联——异常交易查询
 * 
 */
@Controller
@RequestMapping(value = "/IfsCdtcErrorTradeController")
public class IfsCdtcErrorTradeController {
	@Autowired
	private IfsCdtcCnBondLinkSecurdealService cnBondLinkSecurdealService;

	/**
	 * 异常交易查询——列表
	 * 
	 */
	@ResponseBody
	@RequestMapping(value = "/cdtcErrorTradeQuery")
	public RetMsg<PageInfo<IfsCdtcCnBondLinkSecurdeal>> cdtcErrorTradeQuery(@RequestBody Map<String, Object> map) {
		map.put("ctrctSts", "CS11");
		Page<IfsCdtcCnBondLinkSecurdeal> page = cnBondLinkSecurdealService.searchPageCnBondLinkSecurdeal(map);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 异常交易查询——详情
	 * 
	 */
	@ResponseBody
	@RequestMapping(value = "/cdtcErrorTradeDetail")
	public RetMsg<IfsCdtcCnBondLinkSecurdeal> cdtcErrorTradeDetail(@RequestBody Map<String, Object> map) {
		String dealNo = (String) map.get("dealNo");
		IfsCdtcCnBondLinkSecurdeal ifsCdtcCnBondLinkSecurdeal = cnBondLinkSecurdealService.queryByDealNo(dealNo);
		return RetMsgHelper.ok(ifsCdtcCnBondLinkSecurdeal);
	}
}
