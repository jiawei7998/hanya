package com.singlee.ifs.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.credit.util.ExcelUtil;
import com.singlee.capital.dayend.service.DayendDateServiceProxy;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.service.UserService;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.bean.*;
import com.singlee.financial.opics.*;
import com.singlee.ifs.mapper.IfsFlowMonitorMapper;
import com.singlee.ifs.mapper.IfsOpicsCustMapper;
import com.singlee.ifs.mapper.IfsOpicsSettleManageMapper;
import com.singlee.ifs.mapper.IfsSwiftMapper;
import com.singlee.ifs.model.*;
import com.singlee.ifs.service.*;
import com.singlee.refund.model.*;
import com.singlee.refund.service.*;
import com.singlee.slbpm.mapper.TtFlowSerialMapMapper;
import com.singlee.slbpm.pojo.bo.TtFlowSerialMap;
import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import org.activiti.engine.HistoryService;
import org.activiti.engine.history.HistoricTaskInstance;
import org.apache.ibatis.session.RowBounds;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.CellStyle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 导出报表Controller
 */
@Controller
@RequestMapping(value = "/IfsExportController")
public class IfsExportController extends CommonController {

    private static Logger logger = Logger.getLogger(IfsReportController.class);

    @Autowired
    IfsFlowMonitorMapper ifsFlowMonitorMapper; // 审批台

    @Autowired
    private IFSService ifsService; // 正式交易台

    @Autowired
    IfsSwiftMapper ifsSwiftMapper;

    @Autowired
    IfsFlowMonitorService ifsFlowMonitorService;

    @Autowired
    IfsFlowCompletedService ifsFlowCompletedService;

    @Autowired
    IBaseServer baseServer;

    @Autowired
    IfsOpicsBondService ifsOpicsBondService;

    @Autowired
    IfsOpicsCustService ifsOpicsCustService;

    @Autowired
    IfsOpicsActyService ifsOpicsActyService;

    @Autowired
    IfsOpicsSettleManageMapper ifsOpicsSettleManageMapper;

    @Autowired
    IfsLimitService ifsLimitService;

    @Resource
    IfsOpicsCustMapper ifsOpicsCustMapper;


    @Autowired
    private IfsCfetsrmbBondfwdService ifsCfetsrmbBondfwdService;
    @Autowired
    private CFtAfpService cFtAfpService;
    @Autowired
    private CFtAfpConfService cFtAfpConfService;
    @Autowired
    private CFtRdpService cFtRdpService;
    @Autowired
    private CFtRdpConfService cFtRdpConfService;
    @Autowired
    private CFtReddService cFtReddService;
    @Autowired
    private CFtReinService cFtReinService;


    @Autowired
    private QryTotalDealService qryTotalDealService;
    @Autowired
    private IFxdhServer fxdhServer;
    @Autowired
    private ISpshServer spshServer;
    @Autowired
    IAcupServer acupServer;
    @Autowired
    IExposureServer exposureServer;
    @Autowired
    TtFlowSerialMapMapper ttFlowSerialMapMapper;

    @Autowired
    HistoryService historyService;

    @Autowired
    UserService userService;

    @Autowired
    IfsTrdSettleService trdsettleservice;

    @Autowired
    IDldtCountServer iDldtCountServer;

    @Autowired
    ISwapServer swapServer;

    /**
     * 导出审批台交易Excel
     */
    @ResponseBody
    @RequestMapping(value = "/exportExcel")
    public void exportExcel(HttpServletRequest request, HttpServletResponse response) {
        String ua = request.getHeader("User-Agent");
        String approveType = request.getParameter("approveType"); // 审批列表
        String contractId = request.getParameter("contractId"); // 成交单编号
        String tradeType = request.getParameter("tradeType"); // 操作类型
        String prdName = request.getParameter("prdName"); // 产品名称
        String custName = request.getParameter("custName"); // 客户名称
        String tradeDate = request.getParameter("tradeDate"); // 成交日期
        String approveDate = request.getParameter("approveDate"); // 审批日期
        String nettingStatus = request.getParameter("nettingStatus"); // 净额清算状态
        String dealno = request.getParameter("dealno"); // opics编号

        String filename = "审批台报表.xlsx"; // 文件名
        String encode_filename = "";
        try {
            encode_filename = URLEncoder.encode(filename, "utf-8");
        } catch (UnsupportedEncodingException e1) {
            throw new RException(e1.getMessage());
        }
        response.setContentType("application/vnd.ms-excel");
        if (ua != null) {
            response.setHeader("Content-Disposition", "attachment; filename=" + encode_filename);
        }
        response.setHeader("Connection", "close");
        try {
            String userId = request.getParameter("userId");
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("approveType", approveType);
            map.put("userId", userId);
            map.put("contractId", contractId);
            map.put("tradeType", tradeType);
            map.put("prdName", prdName);
            map.put("custName", custName);
            map.put("tradeDate", tradeDate);
            map.put("approveDate", approveDate);
            map.put("nettingStatus", nettingStatus);
            map.put("dealno", dealno);
            ExcelUtil e = downloadExcel(map);
            OutputStream ouputStream = response.getOutputStream();
            e.getWb().write(ouputStream);
            ouputStream.flush();
            ouputStream.close();
        } catch (Exception e) {
            throw new RException(e);
        }
    }

    ;

    /**
     * Excel文件下载内容
     */
    public ExcelUtil downloadExcel(Map<String, Object> map) throws Exception {
        IfsFlowMonitor bean = new IfsFlowMonitor();
        IfsFlowCompleted bean2 = new IfsFlowCompleted();
        ExcelUtil e = null;
        try {
            /* 先查数据，再设置表格属性以及内容 */
            Page<IfsFlowMonitor> data = new Page<IfsFlowMonitor>();
            Page<IfsFlowCompleted> data2 = new Page<IfsFlowCompleted>();
            String approveType = map.get("approveType").toString();
            map.put("pageNumber", "1");
            map.put("pageSize", "999");
            if ("approve".equals(approveType)) {
                data = ifsFlowMonitorService.searchPageFlowMonitor(map);
            } else if ("finished".equals(approveType)) {
                data2 = ifsFlowCompletedService.searchPageFlowCompleted(map);
            } else {
                data = ifsFlowMonitorService.searchPageAllFlowMonitor(map);
            }
            // 表头
            e = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
            CellStyle center = e.getDefaultCenterStrCellStyle();
            int sheet = 0;
            int row = 0;
            // 表格sheet名称
            e.getWb().createSheet("审批台报表");
            // 设置表头字段名
            e.writeStr(sheet, row, "A", center, "审批单编号");
            e.writeStr(sheet, row, "B", center, "审批状态");
            e.writeStr(sheet, row, "C", center, "产品名称");
            e.writeStr(sheet, row, "D", center, "货币/货币对");
            e.writeStr(sheet, row, "E", center, "本方方向");
            e.writeStr(sheet, row, "F", center, "交易货币");
            e.writeStr(sheet, row, "G", center, "交易金额");
            e.writeStr(sheet, row, "H", center, "成交汇率");
            e.writeStr(sheet, row, "I", center, "起息日期");
            e.writeStr(sheet, row, "J", center, "交易日期");
            e.writeStr(sheet, row, "K", center, "对方机构");
            e.writeStr(sheet, row, "L", center, "审批日期");
            e.writeStr(sheet, row, "M", center, "成交单编号");
            e.writeStr(sheet, row, "N", center, "opics处理状态");
            e.writeStr(sheet, row, "O", center, "opics编号");
            e.writeStr(sheet, row, "P", center, "审批发起人");
            // 设置列宽
            e.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
            e.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
            e.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
            e.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
            e.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
            e.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
            e.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
            e.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
            e.getSheetAt(sheet).setColumnWidth(8, 20 * 256);
            e.getSheetAt(sheet).setColumnWidth(9, 20 * 256);
            e.getSheetAt(sheet).setColumnWidth(10, 20 * 256);
            e.getSheetAt(sheet).setColumnWidth(11, 20 * 256);
            e.getSheetAt(sheet).setColumnWidth(12, 20 * 256);
            e.getSheetAt(sheet).setColumnWidth(13, 20 * 256);
            e.getSheetAt(sheet).setColumnWidth(14, 20 * 256);
            e.getSheetAt(sheet).setColumnWidth(15, 20 * 256);

            Map<String, String> upmap = new HashMap<String, String>();
            if ("finished".equals(approveType)) {
                for (int i = 0; i < data2.size(); i++) {
                    bean2 = data2.get(i);
                    row++;
                    upmap.put("dictId", "statcode");
                    upmap.put("dictKey", bean2.getStatcode());
                    String statcode = ifsService.queryDictValue(upmap); // 取数据字典中的value值
                    DecimalFormat df = new DecimalFormat("#,##0.00000000"); //利率格式化
                    // 插入数据
                    e.writeStr(sheet, row, "A", center, bean2.getTicketId());
                    e.writeStr(sheet, row, "B", center, bean2.getTaskName());
                    e.writeStr(sheet, row, "C", center, bean2.getPrdName());
                    e.writeStr(sheet, row, "D", center, bean2.getCcyPair());
                    e.writeStr(sheet, row, "E", center, bean2.getMydircn());
                    e.writeStr(sheet, row, "F", center, bean2.getTccy()); //交易货币
                    e.writeStr(sheet, row, "G", center, bean2.getVamt());
                    e.writeStr(sheet, row, "H", center,
                            bean2.getVrate() != null && !"".equals(bean2.getVrate()) ? df.format(Double.valueOf(bean2.getVrate())) : bean2.getVrate());
                    e.writeStr(sheet, row, "I", center, bean2.getvDate());
                    e.writeStr(sheet, row, "J", center, bean2.getTradeDate());
                    e.writeStr(sheet, row, "K", center, bean2.getCustName());
                    e.writeStr(sheet, row, "L", center, bean2.getApproveDate());
                    e.writeStr(sheet, row, "M", center, bean2.getContractId());
                    e.writeStr(sheet, row, "N", center, statcode);
                    e.writeStr(sheet, row, "O", center, bean2.getDealno());
                    e.writeStr(sheet, row, "P", center, bean2.getSponsor());
                }
            } else {
                for (int i = 0; i < data.size(); i++) {
                    bean = data.get(i);
                    row++;
                    upmap.put("dictId", "statcode");
                    upmap.put("dictKey", bean.getStatcode());
                    String statcode = ifsService.queryDictValue(upmap); // 取数据字典中的value值
                    DecimalFormat df = new DecimalFormat("#,##0.00000000");//利率格式化
                    // 插入数据
                    e.writeStr(sheet, row, "A", center, bean.getTicketId());
                    e.writeStr(sheet, row, "B", center, bean.getTaskName());
                    e.writeStr(sheet, row, "C", center, bean.getPrdName());
                    e.writeStr(sheet, row, "D", center, bean.getCcyPair());
                    e.writeStr(sheet, row, "E", center, bean.getMydircn());
                    e.writeStr(sheet, row, "F", center, bean.getTccy()); //交易货币
                    e.writeStr(sheet, row, "G", center, bean.getVamt());
                    e.writeStr(sheet, row, "H", center,
                            bean.getVrate() != null && !"".equals(bean.getVrate()) ? df.format(Double.valueOf(bean.getVrate())) : bean.getVrate());
                    e.writeStr(sheet, row, "I", center, bean.getvDate());
                    e.writeStr(sheet, row, "J", center, bean.getTradeDate());
                    e.writeStr(sheet, row, "K", center, bean.getCustName());
                    e.writeStr(sheet, row, "L", center, bean.getApproveDate());
                    e.writeStr(sheet, row, "M", center, bean.getContractId());
                    e.writeStr(sheet, row, "N", center, statcode);
                    e.writeStr(sheet, row, "O", center, bean.getDealno());
                    e.writeStr(sheet, row, "P", center, bean.getSponsor());
                }
            }
            return e;
        } catch (Exception e1) {
            throw new RException(e1);
        }
    }


    /**
     * 导出正式交易台交易Excel
     */
    @ResponseBody
    @RequestMapping(value = "/exportTradeExcel1")
    public void exportTradeExcel1(HttpServletRequest request, HttpServletResponse response) throws Exception {
        //获取request对象的参数
        BufferedReader streamReader = new BufferedReader(new InputStreamReader(request.getInputStream(), "UTF-8"));
        StringBuilder responseStrBuilder = new StringBuilder();
        String inputStr;
        while ((inputStr = streamReader.readLine()) != null) {
            responseStrBuilder.append(inputStr);
        }
        JSONObject jsonObject = JSONObject.parseObject(responseStrBuilder.toString());

        ServletOutputStream out = response.getOutputStream();
        //获取名称
        String fileName = jsonObject.get("fileName").toString(); // 文件名称
        JSONArray dataMap = (JSONArray) jsonObject.get("dataMap"); // 文件数据
        JSONArray rowsHear = (JSONArray) jsonObject.get("columns");
        //声明输出流
        response.reset();//清空输出流
        response.setCharacterEncoding("UTF-8");//设置相应内容的编码格式
        response.setHeader("Content-Disposition", "attachment;filename=" + fileName + ".xls");
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Cache-Control", "no-cache");
        response.setContentType("application/vnd.ms-excel");//定义输出类型

        try {
            // 获得开始时间
            // long start = System.currentTimeMillis();
            // 创建Excel工作薄
            WritableWorkbook workbook = Workbook.createWorkbook(out);
            // 添加第一个工作表并设置第一个Sheet的名字
            WritableSheet sheet = workbook.createSheet(jsonObject.get("fileName").toString(), 0);
            Label label;
            //拼装hear名称
            for (int i = 0; i < rowsHear.size(); i++) {
                label = new Label(i, 0, ((JSONObject) rowsHear.get(i)).get("header").toString());
                sheet.addCell(label);
            }
            //写出数据
            for (int i = 1; i < dataMap.size(); i++) {
                JSONObject jsonObj = (JSONObject) dataMap.get(i);
                for (int k = 1; k < jsonObj.size(); k++) {
                    for (int j = 0; j < rowsHear.size(); j++) {
                        JSONObject keyFiled = (JSONObject) rowsHear.get(j);
                        if (null == keyFiled.get("field")) {
                            continue;
                        }
                        //获取值
                        if (null == jsonObj.get(keyFiled.get("field"))) {
                            continue;
                        }
                        System.out.println("当前查询的key:" + keyFiled + "==>" + jsonObj.get(keyFiled.get("field")));
                        label = new Label(j, i, jsonObj.get(keyFiled.get("field")) + "");
                        sheet.addCell(label);
                    }
                }
            }
            // 写入数据
            workbook.write();
            out.flush();
            // 关闭文件
            workbook.close();
            out.close();
            // long end = System.currentTimeMillis();
            //System.out.println("----完成该操作共用的时间是:"+(end-start)/1000);
        } catch (Exception e) {
            // System.out.println("---出现异常---");
            e.printStackTrace();
        }
    }

    /**
     * 导出正式交易台交易Excel
     */
    @ResponseBody
    @RequestMapping(value = "/exportTradeExcel")
    public void exportTradeExcel(HttpServletRequest request, HttpServletResponse response) {
        String prdName = request.getParameter("prdName"); // 交易名
        String approveType = request.getParameter("approveType"); // 审批列表
        String contractId = request.getParameter("contractId"); // 成交单编号
        String approveStatus = request.getParameter("approveStatus"); // 审批状态
        String currencyPair = request.getParameter("currencyPair"); // 货币对
        String forDate = request.getParameter("forDate"); // 成交日期
        String port = request.getParameter("port"); // 投资组合
        String startDate = request.getParameter("startDate"); // 起始日期
        String endDate = request.getParameter("endDate"); // 结束日期
        String branchId = request.getParameter("branchId");

        /*审批发起日期*/
        String startDay = request.getParameter("startDay");
        String endDay = request.getParameter("endDay");
        String ticketId = request.getParameter("ticketId"); // 审批单编号
        String deal = request.getParameter("deal"); // 审批单编号
        String sponInst = request.getParameter("sponInst"); // 审批发起机构

        String dealNo = request.getParameter("dealNo"); // dealNo

        String filename = ""; // 文件名
        if ("债券发行".equals(prdName)) {
            filename = "债券发行报表.xlsx";
        } else if ("利率互换".equals(prdName)) {
            filename = "利率互换报表.xlsx";
        } else if ("买断式回购".equals(prdName)) {
            filename = "买断式回购报表.xlsx";
        } else if ("现券买卖".equals(prdName)) {
            filename = "现券买卖报表.xlsx";
        } else if ("信用拆借".equals(prdName)) {
            filename = "信用拆借报表.xlsx";
        } else if ("债券借贷".equals(prdName)) {
            filename = "债券借贷报表.xlsx";
        } else if ("质押式回购".equals(prdName)) {
            filename = "质押式回购报表.xlsx";
        } else if ("黄金拆借".equals(prdName)) {
            filename = "黄金拆借报表.xlsx";
        } else if ("外币债".equals(prdName)) {
            filename = "外币债报表.xlsx";
        } else if ("货币掉期".equals(prdName)) {
            filename = "货币掉期报表.xlsx";
        } else if ("外汇掉期".equals(prdName)) {
            filename = "外汇掉期报表.xlsx";
        } else if ("人民币期权".equals(prdName)) {
            filename = "人民币期权报表.xlsx";
        } else if ("外币拆借".equals(prdName)) {
            filename = "外币拆借报表.xlsx";
        } else if ("外币头寸调拨".equals(prdName)) {
            filename = "外币头寸调拨报表.xlsx";
        } else if ("外汇远期".equals(prdName)) {
            filename = "外汇远期报表.xlsx";
        } else if ("外汇即期".equals(prdName)) {
            filename = "外汇即期报表.xlsx";
        } else if ("黄金即期".equals(prdName)) {
            filename = "黄金即期报表.xlsx";
        } else if ("黄金远期".equals(prdName)) {
            filename = "黄金远期报表.xlsx";
        } else if ("黄金掉期".equals(prdName)) {
            filename = "黄金掉期报表.xlsx";
        } else if ("债券远期".equals(prdName)) {
            filename = "债券远期报表.xlsx";
        } else if ("基金申购".equals(prdName)) {
            filename = "基金申购报表.xlsx";
        } else if ("基金申购确认".equals(prdName)) {
            filename = "基金申购确认报表.xlsx";
        } else if ("基金赎回".equals(prdName)) {
            filename = "基金赎回报表.xlsx";
        } else if ("基金赎回确认".equals(prdName)) {
            filename = "基金赎回确认报表.xlsx";
        } else if ("现金分红".equals(prdName)) {
            filename = "基金现金分红报表.xlsx";
        } else if ("红利再投".equals(prdName)) {
            filename = "基金红利再投报表.xlsx";
        }

        String ua = request.getHeader("User-Agent");
        String encode_filename = "";
        try {
            encode_filename = URLEncoder.encode(filename, "utf-8");
        } catch (UnsupportedEncodingException e1) {
            throw new RException(e1.getMessage());
        }
        response.setContentType("application/vnd.ms-excel");
        if (ua != null) {
            response.setHeader("Content-Disposition", "attachment; filename=" + encode_filename);
        }
        response.setHeader("Connection", "close");
        try {
            Map<String, String> map = new HashMap<String, String>();
            map.put("approveType", approveType);
            map.put("prdName", prdName);
            map.put("contractId", contractId);
            map.put("approveStatus", approveStatus);
            map.put("currencyPair", currencyPair);
            map.put("startDate", startDate);
            map.put("endDate", endDate);
            map.put("port", port);
            map.put("startDay", startDay);
            map.put("endDay", endDay);
            map.put("ticketId", ticketId);
            map.put("deal", deal);
            map.put("branchId", branchId);
            map.put("sponInst", sponInst);
            if (forDate != null && forDate != "" && forDate.length() >= 10) {
                map.put("forDate", forDate.substring(0, 10));
            } else {
                map.put("forDate", "");
            }
            map.put("branchId", branchId);
            map.put("dealNo", dealNo);
            ExcelUtil e = downloadTradeExcel(map);
            OutputStream ouputStream = response.getOutputStream();
            e.getWb().write(ouputStream);
            ouputStream.flush();
            ouputStream.close();
        } catch (Exception e) {
            throw new RException(e);
        }
    }

    ;

    /**
     * 正式交易台 Excel文件下载内容
     */
    public ExcelUtil downloadTradeExcel(Map<String, String> map) throws Exception {
        String startDay = "";
        String endDay = "";
        if (!StringUtil.isNullOrEmpty(map.get("startDay"))) {
            startDay = map.get("startDay");
        }
        if (!StringUtil.isNullOrEmpty(map.get("endDay"))) {
            endDay = map.get("endDay");
        }
        String ticketId = "";
        if (!StringUtil.isNullOrEmpty(map.get("ticketId"))) {
            ticketId = map.get("ticketId");
        }
        String sponInst = "";
        if (!StringUtil.isNullOrEmpty(map.get("sponInst"))) {
            sponInst = map.get("sponInst");
        }
        ExcelUtil excel = null;
        try {
            String prdName = map.get("prdName");
            String branchId = map.get("branchId");
            if ("债券发行".equals(prdName)) {
                /* 先查数据，再设置表格属性以及内容 */
                Page<IfsCfetsrmbDp> data = new Page<IfsCfetsrmbDp>();
                String approveType = map.get("approveType");
                Map<String, Object> params = new HashMap<String, Object>();
                params.put("contractId", map.get("contractId")); // 成交单编号
                params.put("forDate", map.get("forDate")); // 成交日期
                params.put("approveStatus", map.get("approveStatus")); // 审批状态
                params.put("branchId", branchId);
                params.put("pageNumber", "1");
                params.put("pageSize", "999");
                params.put("startDay", startDay);
                params.put("endDay", endDay);
                params.put("ticketId", ticketId);
                params.put("sponInst", sponInst);
                if (!StringUtil.isNullOrEmpty(map.get("deal"))) { // 总交易查询导出
                    params.put("isActive", DictConstants.YesNo.YES);
                    params.put("userId", SlSessionHelper.getUserId());
                    data = qryTotalDealService.searchTotalDealZQFX(params, 3);
                } else { // 正式交易台查询导出
                    if ("mine".equals(approveType)) {
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        data = ifsService.getRmbDpMinePage(params, 3);
                    } else if ("approve".equals(approveType)) {
                        String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + ","
                                + DictConstants.ApproveStatus.VerifyRefuse + "," + DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
                        String[] orderStatus = approveStatus.split(",");
                        params.put("approveStatusNo", orderStatus);
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        data = ifsService.getRmbDpMinePage(params, 1);
                    } else {
                        String approveStatusNo = ParameterUtil.getString(params, "approveStatus", "");
                        if (StringUtil.isNullOrEmpty(approveStatusNo)) {
                            approveStatusNo = DictConstants.ApproveStatus.ApprovedPass;
                            approveStatusNo += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
                            approveStatusNo += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
                            approveStatusNo += "," + DictConstants.ApproveStatus.Verified;
                            approveStatusNo += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
                            // 20180502修改bug:已审批列表中增加"审批中"
                            approveStatusNo += "," + DictConstants.ApproveStatus.Approving;
                            String[] orderStatus = approveStatusNo.split(",");
                            params.put("approveStatusNo", orderStatus);
                        }
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        data = ifsService.getRmbDpMinePage(params, 2);
                    }
                }
                // 表头
                excel = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
                CellStyle center = excel.getDefaultCenterStrCellStyle();
                int sheet = 0;
                int row = 0;
                // 表格sheet名称
                excel.getWb().createSheet("债券发行报表");
                // 设置表头字段名
                excel.writeStr(sheet, row, "A", center, "审批单编号");
                excel.writeStr(sheet, row, "B", center, "成交单编号");
                excel.writeStr(sheet, row, "C", center, "成交日期");
                excel.writeStr(sheet, row, "D", center, "审批状态");
                excel.writeStr(sheet, row, "E", center, "债券代码");
                excel.writeStr(sheet, row, "F", center, "债券简称");
                excel.writeStr(sheet, row, "G", center, "债券期限");
                excel.writeStr(sheet, row, "H", center, "息票类型");
                excel.writeStr(sheet, row, "I", center, "认购量总额");
                excel.writeStr(sheet, row, "J", center, "应缴金额总额");
                excel.writeStr(sheet, row, "K", center, "发行日");
                excel.writeStr(sheet, row, "L", center, "起息日");
                excel.writeStr(sheet, row, "M", center, "到期日");
                excel.writeStr(sheet, row, "N", center, "发行价格");
                excel.writeStr(sheet, row, "O", center, "参考收益率(%)");
                excel.writeStr(sheet, row, "P", center, "计息基准");
                excel.writeStr(sheet, row, "Q", center, "审批发起人");
                excel.writeStr(sheet, row, "R", center, "审批发起时间");
                // 设置列宽
                excel.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(8, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(9, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(10, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(11, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(12, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(13, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(14, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(15, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(16, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(17, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(18, 20 * 256);
                IfsCfetsrmbDp bean = new IfsCfetsrmbDp();
                Map<String, String> upmap = new HashMap<String, String>();
                Map<String, String> approveMap = new HashMap<String, String>();
                for (int i = 0; i < data.size(); i++) {
                    bean = data.get(i);
                    row++;
                    // 取数据字典中的value值
                    upmap.put("dictId", "Basis");
                    upmap.put("dictKey", bean.getBasis());
                    String basis = ifsService.queryDictValue(upmap);  // 计息基准

                    approveMap.put("dictId", "ApproveStatus");
                    approveMap.put("dictKey", bean.getApproveStatus());
                    String approveStatus = ifsService.queryDictValue(approveMap); // 审批状态
                    // 插入数据
                    excel.writeStr(sheet, row, "A", center, bean.getTicketId());
                    excel.writeStr(sheet, row, "B", center, bean.getContractId());
                    excel.writeStr(sheet, row, "C", center, bean.getForDate());
                    if (!StringUtil.isNullOrEmpty(map.get("deal"))) {
                        excel.writeStr(sheet, row, "D", center, approveStatus);
                    } else {
                        excel.writeStr(sheet, row, "D", center, bean.getTaskName());
                    }
                    excel.writeStr(sheet, row, "E", center, bean.getDepositCode());
                    excel.writeStr(sheet, row, "F", center, bean.getDepositName());
                    excel.writeStr(sheet, row, "G", center, bean.getDepositTerm() == null ? "" : bean.getDepositTerm().toString());
                    excel.writeStr(sheet, row, "H", center, bean.getInterestType());
                    excel.writeStr(sheet, row, "I", center, bean.getOfferAmount());
                    excel.writeStr(sheet, row, "J", center, bean.getShouldPayMoney());
                    excel.writeStr(sheet, row, "K", center, bean.getPublishDate());
                    excel.writeStr(sheet, row, "L", center, bean.getValueDate());
                    excel.writeStr(sheet, row, "M", center, bean.getDuedate());
                    excel.writeStr(sheet, row, "N", center, bean.getPublishPrice() == null ? "" : bean.getPublishPrice().toString());
                    excel.writeStr(sheet, row, "O", center, bean.getBenchmarkcurvename() == null ? "" : bean.getBenchmarkcurvename().toString());
                    excel.writeStr(sheet, row, "P", center, basis);
                    excel.writeStr(sheet, row, "Q", center, bean.getSponsor());
                    excel.writeStr(sheet, row, "R", center, bean.getaDate());
                }
            } else if ("利率互换".equals(prdName)) {
                /* 先查数据，再设置表格属性以及内容 */
                Page<IfsCfetsrmbIrs> data = new Page<IfsCfetsrmbIrs>();
                String approveType = map.get("approveType");
                Map<String, Object> params = new HashMap<String, Object>();
                params.put("contractId", map.get("contractId")); // 成交单编号
                params.put("forDate", map.get("forDate")); // 成交日期
                params.put("approveStatus", map.get("approveStatus")); // 审批状态
                params.put("branchId", branchId);
                params.put("pageNumber", "1");
                params.put("pageSize", "999");
                params.put("prdFlag", "0"); // 利率互换为0
                params.put("startDay", startDay);
                params.put("endDay", endDay);
                params.put("ticketId", ticketId);
                params.put("sponInst", sponInst);
                if (!StringUtil.isNullOrEmpty(map.get("deal"))) { // 总交易查询导出
                    params.put("isActive", DictConstants.YesNo.YES);
                    params.put("userId", SlSessionHelper.getUserId());
                    data = qryTotalDealService.searchTotalDealLLHH(params, 3);
                } else { // 正式交易台查询导出
                    if ("mine".equals(approveType)) {
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        data = ifsService.getRmbIrsMinePage(params, 3);
                    } else if ("approve".equals(approveType)) {
                        String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + ","
                                + DictConstants.ApproveStatus.VerifyRefuse + "," + DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
                        String[] orderStatus = approveStatus.split(",");
                        params.put("approveStatusNo", orderStatus);
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        data = ifsService.getRmbIrsMinePage(params, 1);
                    } else {
                        String approveStatus = ParameterUtil.getString(params, "approveStatus", "");
                        if (StringUtil.isNullOrEmpty(approveStatus)) {
                            approveStatus = DictConstants.ApproveStatus.ApprovedPass;
                            approveStatus += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
                            approveStatus += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
                            approveStatus += "," + DictConstants.ApproveStatus.Verified;
                            approveStatus += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
                            // 20180502修改bug:已审批列表中增加"审批中"
                            approveStatus += "," + DictConstants.ApproveStatus.Approving;
                            String[] orderStatus = approveStatus.split(",");
                            params.put("approveStatusNo", orderStatus);
                        }
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        data = ifsService.getRmbIrsMinePage(params, 2);
                    }
                }
                // 表头
                excel = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
                CellStyle center = excel.getDefaultCenterStrCellStyle();
                int sheet = 0;
                int row = 0;
                // 表格sheet名称
                excel.getWb().createSheet("利率互换报表");
                // 设置表头字段名
                excel.writeStr(sheet, row, "A", center, "审批单号");
                excel.writeStr(sheet, row, "B", center, "成交单号");
                excel.writeStr(sheet, row, "C", center, "opics单号");
                excel.writeStr(sheet, row, "D", center, "本方方向");
                excel.writeStr(sheet, row, "E", center, "对手方机构名称");
                excel.writeStr(sheet, row, "F", center, "名义本金(万元)");
                excel.writeStr(sheet, row, "G", center, "利率代码");
                excel.writeStr(sheet, row, "H", center, "起息日");
                excel.writeStr(sheet, row, "I", center, "到期日");
                excel.writeStr(sheet, row, "J", center, "产品名称");
                excel.writeStr(sheet, row, "K", center, "固定利率支付方机构");
                excel.writeStr(sheet, row, "L", center, "计算机构");
                excel.writeStr(sheet, row, "M", center, "清算方式");
                excel.writeStr(sheet, row, "N", center, "成交日期");
                excel.writeStr(sheet, row, "O", center, "审批状态");
                excel.writeStr(sheet, row, "P", center, "审批发起人");
                excel.writeStr(sheet, row, "Q", center, "审批发起机构");
                excel.writeStr(sheet, row, "R", center, "审批发起时间");
                // 设置列宽
                excel.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(8, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(9, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(10, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(11, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(12, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(13, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(14, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(15, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(16, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(17, 20 * 256);
                IfsCfetsrmbIrs bean = new IfsCfetsrmbIrs();
                Map<String, String> upmap = new HashMap<String, String>();
                Map<String, String> upmap2 = new HashMap<String, String>();
                Map<String, String> upmap3 = new HashMap<String, String>();
                Map<String, String> approveMap = new HashMap<String, String>();
                for (int i = 0; i < data.size(); i++) {
                    bean = data.get(i);
                    row++;
                    upmap.put("dictId", "calInst");
                    upmap.put("dictKey", bean.getCalculateAgency());
                    String calculateAgency = ifsService.queryDictValue(upmap); // 取数据字典中的value值
                    upmap2.put("dictId", "CleanMethoed");
                    upmap2.put("dictKey", bean.getTradeMethod());
                    String tradeMethod = ifsService.queryDictValue(upmap2); // 取数据字典中的value值
                    upmap3.put("dictId", "irsing");
                    upmap3.put("dictKey", bean.getMyDir());
                    String myDir = ifsService.queryDictValue(upmap3); // 取数据字典中的value值
                    approveMap.put("dictId", "ApproveStatus");
                    approveMap.put("dictKey", bean.getApproveStatus());
                    String approveStatus = ifsService.queryDictValue(approveMap); // 审批状态
                    // 插入数据
                    excel.writeStr(sheet, row, "A", center, bean.getTicketId());
                    excel.writeStr(sheet, row, "B", center, bean.getContractId());
                    excel.writeStr(sheet, row, "C", center, bean.getDealNo());
                    excel.writeStr(sheet, row, "D", center, myDir); // 本方方向
                    excel.writeStr(sheet, row, "E", center, bean.getFloatInst());
                    BigDecimal lastQty = bean.getLastQty();
                    excel.writeStr(sheet, row, "F", center, lastQty == null ? "" : lastQty.toString());
                    excel.writeStr(sheet, row, "G", center, bean.getRateCode());
                    excel.writeStr(sheet, row, "H", center, bean.getStartDate());
                    excel.writeStr(sheet, row, "I", center, bean.getEndDate());
                    excel.writeStr(sheet, row, "J", center, bean.getProductName());
                    excel.writeStr(sheet, row, "K", center, bean.getFixedInst());
                    excel.writeStr(sheet, row, "L", center, calculateAgency); // 计算机构
                    excel.writeStr(sheet, row, "M", center, tradeMethod); // 清算方式
                    excel.writeStr(sheet, row, "N", center, bean.getForDate());
                    if (!StringUtil.isNullOrEmpty(map.get("deal"))) {
                        excel.writeStr(sheet, row, "O", center, approveStatus);
                    } else {
                        excel.writeStr(sheet, row, "O", center, bean.getTaskName());
                    }
                    excel.writeStr(sheet, row, "P", center, bean.getSponsor());
                    excel.writeStr(sheet, row, "Q", center, bean.getSponInst());
                    excel.writeStr(sheet, row, "R", center, bean.getaDate());
                }
            } else if ("买断式回购".equals(prdName)) {
                /* 先查数据，再设置表格属性以及内容 */
                Page<IfsCfetsrmbOr> data = new Page<IfsCfetsrmbOr>();
                String approveType = map.get("approveType");
                Map<String, Object> params = new HashMap<String, Object>();
                params.put("contractId", map.get("contractId")); // 成交单编号
                params.put("forDate", map.get("forDate")); // 成交日期
                params.put("approveStatus", map.get("approveStatus")); // 审批状态
                params.put("branchId", branchId);
                params.put("pageNumber", "1");
                params.put("pageSize", "999");
                params.put("startDay", startDay);
                params.put("endDay", endDay);
                params.put("ticketId", ticketId);
                params.put("sponInst", sponInst);
                if (!StringUtil.isNullOrEmpty(map.get("deal"))) { // 总交易查询导出
                    params.put("isActive", DictConstants.YesNo.YES);
                    params.put("userId", SlSessionHelper.getUserId());
                    data = qryTotalDealService.searchTotalDealMDSHG(params, 3);
                } else { // 正式交易台查询导出
                    if ("mine".equals(approveType)) {
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        data = ifsService.getRmbOrMinePage(params, 3);
                    } else if ("approve".equals(approveType)) {
                        String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + ","
                                + DictConstants.ApproveStatus.VerifyRefuse + "," + DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
                        String[] orderStatus = approveStatus.split(",");
                        params.put("approveStatusNo", orderStatus);
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        data = ifsService.getRmbOrMinePage(params, 1);
                    } else {
                        String approveStatus = ParameterUtil.getString(params, "approveStatus", "");
                        if (StringUtil.isNullOrEmpty(approveStatus)) {
                            approveStatus = DictConstants.ApproveStatus.ApprovedPass;
                            approveStatus += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
                            approveStatus += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
                            approveStatus += "," + DictConstants.ApproveStatus.Verified;
                            approveStatus += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
                            // 20180502修改bug:已审批列表中增加"审批中"
                            approveStatus += "," + DictConstants.ApproveStatus.Approving;
                            String[] orderStatus = approveStatus.split(",");
                            params.put("approveStatusNo", orderStatus);
                        }
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        data = ifsService.getRmbOrMinePage(params, 2);
                    }
                }
                // 表头
                excel = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
                CellStyle center = excel.getDefaultCenterStrCellStyle();
                int sheet = 0;
                int row = 0;
                // 表格sheet名称
                excel.getWb().createSheet("买断式回购报表");
                // 设置表头字段名
                excel.writeStr(sheet, row, "A", center, "审批单编号");
                excel.writeStr(sheet, row, "B", center, "成交单编号");
                excel.writeStr(sheet, row, "C", center, "opics交易号");
                excel.writeStr(sheet, row, "D", center, "审批状态");
                excel.writeStr(sheet, row, "E", center, "本方方向");
                excel.writeStr(sheet, row, "F", center, "对手方机构名称");
                excel.writeStr(sheet, row, "G", center, "交易金额(万元)");
                excel.writeStr(sheet, row, "H", center, "回购利率(%)");
                excel.writeStr(sheet, row, "I", center, "起息日");
                excel.writeStr(sheet, row, "J", center, "到期结算日");
                excel.writeStr(sheet, row, "K", center, "期限");
                excel.writeStr(sheet, row, "L", center, "实际占款天数");
                excel.writeStr(sheet, row, "M", center, "到期结算金额(元)");
                excel.writeStr(sheet, row, "N", center, "计息基准");
                excel.writeStr(sheet, row, "O", center, "审批发起人");
                excel.writeStr(sheet, row, "P", center, "审批发起时间");
                // 设置列宽
                excel.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(8, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(9, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(10, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(11, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(12, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(13, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(14, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(15, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(16, 20 * 256);
                IfsCfetsrmbOr bean = new IfsCfetsrmbOr();
                Map<String, String> upmap = new HashMap<String, String>();
                Map<String, String> upmap2 = new HashMap<String, String>();
                Map<String, String> approveMap = new HashMap<String, String>();
                for (int i = 0; i < data.size(); i++) {
                    bean = data.get(i);
                    row++;
                    approveMap.put("dictId", "ApproveStatus");
                    approveMap.put("dictKey", bean.getApproveStatus());
                    String approveStatus = ifsService.queryDictValue(approveMap); // 审批状态
                    upmap.put("dictId", "repoing");
                    upmap.put("dictKey", bean.getMyDir());
                    String myDir = ifsService.queryDictValue(upmap); // 取数据字典中的value值
                    upmap2.put("dictId", "calIntDays");
                    upmap2.put("dictKey", bean.getBasis());
                    String basis = ifsService.queryDictValue(upmap2); // 取数据字典中的value值
                    // 插入数据
                    excel.writeStr(sheet, row, "A", center, bean.getTicketId());
                    excel.writeStr(sheet, row, "B", center, bean.getContractId());
                    excel.writeStr(sheet, row, "C", center, bean.getDealNo());
                    if (!StringUtil.isNullOrEmpty(map.get("deal"))) {
                        excel.writeStr(sheet, row, "D", center, approveStatus);
                    } else {
                        excel.writeStr(sheet, row, "D", center, bean.getTaskName());
                    }
                    excel.writeStr(sheet, row, "E", center, myDir); // 本方方向
                    excel.writeStr(sheet, row, "F", center, bean.getReverseInst());
                    excel.writeStr(sheet, row, "G", center, bean.getTotalFaceValue() == null ? "" : bean.getTotalFaceValue().toString());
                    excel.writeStr(sheet, row, "H", center, bean.getRepoRate() == null ? "" : bean.getRepoRate().toString());
                    excel.writeStr(sheet, row, "I", center, bean.getForDate());
                    excel.writeStr(sheet, row, "J", center, bean.getSecondSettlementDate());
                    excel.writeStr(sheet, row, "K", center, bean.getTenor());
                    excel.writeStr(sheet, row, "L", center, bean.getOccupancyDays() == null ? "" : bean.getOccupancyDays().toString());
                    excel.writeStr(sheet, row, "M", center, bean.getSecondSettlementAmount() == null ? "" : bean.getSecondSettlementAmount().toString());
                    excel.writeStr(sheet, row, "N", center, basis); // 计息基准
                    excel.writeStr(sheet, row, "O", center, bean.getPositiveTrader());
                    excel.writeStr(sheet, row, "P", center, bean.getaDate());
                }
            } else if ("现券买卖".equals(prdName)) {
                /* 先查数据，再设置表格属性以及内容 */
                Page<IfsCfetsrmbCbt> data = new Page<IfsCfetsrmbCbt>();
                String approveType = map.get("approveType");
                Map<String, Object> params = new HashMap<String, Object>();
                params.put("contractId", map.get("contractId")); // 成交单编号
                params.put("forDate", map.get("forDate")); // 成交日期
                params.put("approveStatus", map.get("approveStatus")); // 审批状态
                params.put("branchId", branchId);
                params.put("pageNumber", "1");
                params.put("pageSize", "999");
                params.put("startDay", startDay);
                params.put("endDay", endDay);
                params.put("ticketId", ticketId);
                params.put("sponInst", sponInst);
                if (!StringUtil.isNullOrEmpty(map.get("deal"))) { // 总交易查询导出
                    params.put("isActive", DictConstants.YesNo.YES);
                    params.put("userId", SlSessionHelper.getUserId());
                    data = qryTotalDealService.searchTotalDealXJMM(params, 3);
                } else { // 正式交易台查询导出
                    if ("mine".equals(approveType)) {
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        data = ifsService.getRmbCbtMinePage(params, 3);
                    } else if ("approve".equals(approveType)) {
                        String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + ","
                                + DictConstants.ApproveStatus.VerifyRefuse + "," + DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
                        String[] orderStatus = approveStatus.split(",");
                        params.put("approveStatusNo", orderStatus);
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        data = ifsService.getRmbCbtMinePage(params, 1);
                    } else {
                        String approveStatus = ParameterUtil.getString(params, "approveStatus", "");
                        if (StringUtil.isNullOrEmpty(approveStatus)) {
                            approveStatus = DictConstants.ApproveStatus.ApprovedPass;
                            approveStatus += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
                            approveStatus += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
                            approveStatus += "," + DictConstants.ApproveStatus.Verified;
                            approveStatus += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
                            // 20180502修改bug:已审批列表中增加"审批中"
                            approveStatus += "," + DictConstants.ApproveStatus.Approving;
                            String[] orderStatus = approveStatus.split(",");
                            params.put("approveStatusNo", orderStatus);
                        }
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        data = ifsService.getRmbCbtMinePage(params, 2);
                    }
                }
                // 表头
                excel = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
                CellStyle center = excel.getDefaultCenterStrCellStyle();
                int sheet = 0;
                int row = 0;
                // 表格sheet名称
                excel.getWb().createSheet("现券买卖报表");
                // 设置表头字段名
                excel.writeStr(sheet, row, "A", center, "审批单号");
                excel.writeStr(sheet, row, "B", center, "成交单号");
                excel.writeStr(sheet, row, "C", center, "opics单号");
                excel.writeStr(sheet, row, "D", center, "债券简称");
                excel.writeStr(sheet, row, "E", center, "投资类型");
                excel.writeStr(sheet, row, "F", center, "本方方向");
                excel.writeStr(sheet, row, "G", center, "对手方机构名称");
                excel.writeStr(sheet, row, "H", center, "交易净价");
                excel.writeStr(sheet, row, "I", center, "到期收益率(%)");
                excel.writeStr(sheet, row, "J", center, "交易金额(元)");
                excel.writeStr(sheet, row, "K", center, "交易日");
                excel.writeStr(sheet, row, "L", center, "结算金额(元");
                excel.writeStr(sheet, row, "M", center, "结算日");
                excel.writeStr(sheet, row, "N", center, "券面总额(万元)");
                excel.writeStr(sheet, row, "O", center, "结算方式");
                excel.writeStr(sheet, row, "P", center, "买入方机构");
                excel.writeStr(sheet, row, "Q", center, "审批状态");
                excel.writeStr(sheet, row, "R", center, "审批发起人");
                excel.writeStr(sheet, row, "S", center, "审批发起机构");
                excel.writeStr(sheet, row, "T", center, "审批发起时间");
                // 设置列宽
                excel.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(8, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(9, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(10, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(11, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(12, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(13, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(14, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(15, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(16, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(17, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(18, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(19, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(20, 20 * 256);
                IfsCfetsrmbCbt bean = new IfsCfetsrmbCbt();
                Map<String, String> upmap = new HashMap<String, String>();
                Map<String, String> upmap2 = new HashMap<String, String>();
                Map<String, String> approveMap = new HashMap<String, String>();
                for (int i = 0; i < data.size(); i++) {
                    bean = data.get(i);
                    row++;
                    upmap.put("dictId", "trading");
                    upmap.put("dictKey", bean.getMyDir());
                    String myDir = ifsService.queryDictValue(upmap); // 取数据字典中的value值

                    upmap2.put("dictId", "SettlementMethod");
                    upmap2.put("dictKey", bean.getSettlementMethod());
                    String settlementMethod = ifsService.queryDictValue(upmap2); // 取数据字典中的value值

                    approveMap.put("dictId", "ApproveStatus");
                    approveMap.put("dictKey", bean.getApproveStatus());
                    String approveStatus = ifsService.queryDictValue(approveMap); // 审批状态
                    // 插入数据
                    excel.writeStr(sheet, row, "A", center, bean.getTicketId());
                    excel.writeStr(sheet, row, "B", center, bean.getContractId());
                    excel.writeStr(sheet, row, "C", center, bean.getDealNo());
                    excel.writeStr(sheet, row, "D", center, bean.getBondName());
                    excel.writeStr(sheet, row, "E", center, bean.getInvtype());
                    excel.writeStr(sheet, row, "F", center, myDir); // 本方方向
                    excel.writeStr(sheet, row, "G", center, bean.getSellInst());
                    excel.writeStr(sheet, row, "H", center, bean.getCleanPrice() == null ? "" : bean.getCleanPrice().toString());
                    excel.writeStr(sheet, row, "I", center, bean.getYield() == null ? "" : bean.getYield().toString());
                    excel.writeStr(sheet, row, "J", center, bean.getTradeAmount() == null ? "" : bean.getTradeAmount().toString());
                    excel.writeStr(sheet, row, "K", center, bean.getForDate());
                    excel.writeStr(sheet, row, "L", center, bean.getSettlementAmount() == null ? "" : bean.getSettlementAmount().toString());
                    excel.writeStr(sheet, row, "M", center, bean.getSettlementDate());
                    excel.writeStr(sheet, row, "N", center, bean.getTotalFaceValue() == null ? "" : bean.getTotalFaceValue().toString());
                    excel.writeStr(sheet, row, "O", center, settlementMethod); // 结算方式
                    excel.writeStr(sheet, row, "P", center, bean.getBuyInst());
                    if (!StringUtil.isNullOrEmpty(map.get("deal"))) {
                        excel.writeStr(sheet, row, "Q", center, approveStatus);
                    } else {
                        excel.writeStr(sheet, row, "Q", center, bean.getTaskName());
                    }
                    excel.writeStr(sheet, row, "R", center, bean.getSponsor());
                    excel.writeStr(sheet, row, "S", center, bean.getSponInst());
                    excel.writeStr(sheet, row, "T", center, bean.getaDate());
                }
            } else if ("信用拆借".equals(prdName)) {
                /* 先查数据，再设置表格属性以及内容 */
                Page<IfsCfetsrmbIbo> data = new Page<IfsCfetsrmbIbo>();
                String approveType = map.get("approveType");
                Map<String, Object> params = new HashMap<String, Object>();
                params.put("contractId", map.get("contractId")); // 成交单编号
                params.put("forDate", map.get("forDate")); // 成交日期
                params.put("approveStatus", map.get("approveStatus")); // 审批状态
                params.put("branchId", branchId);
                params.put("pageNumber", "1");
                params.put("pageSize", "999");
                params.put("startDay", startDay);
                params.put("endDay", endDay);
                params.put("ticketId", ticketId);
                params.put("sponInst", sponInst);
                if (!StringUtil.isNullOrEmpty(map.get("deal"))) { // 总交易查询导出
                    params.put("isActive", DictConstants.YesNo.YES);
                    params.put("userId", SlSessionHelper.getUserId());
                    data = qryTotalDealService.searchTotalDealXYCJ(params, 3);
                } else { // 正式交易台查询导出
                    if ("mine".equals(approveType)) {
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        data = ifsService.getRmbIboMinePage(params, 3);
                    } else if ("approve".equals(approveType)) {
                        String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + ","
                                + DictConstants.ApproveStatus.VerifyRefuse + "," + DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
                        String[] orderStatus = approveStatus.split(",");
                        params.put("approveStatusNo", orderStatus);
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        data = ifsService.getRmbIboMinePage(params, 1);
                    } else {
                        String approveStatusNo = ParameterUtil.getString(params, "approveStatus", "");
                        if (StringUtil.isNullOrEmpty(approveStatusNo)) {
                            approveStatusNo = DictConstants.ApproveStatus.ApprovedPass;
                            approveStatusNo += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
                            approveStatusNo += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
                            approveStatusNo += "," + DictConstants.ApproveStatus.Verified;
                            approveStatusNo += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
                            // 20180502修改bug:已审批列表中增加"审批中"
                            approveStatusNo += "," + DictConstants.ApproveStatus.Approving;
                            String[] orderStatus = approveStatusNo.split(",");
                            params.put("approveStatusNo", orderStatus);
                        }
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        data = ifsService.getRmbIboMinePage(params, 2);
                    }
                }
                // 表头
                excel = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
                CellStyle center = excel.getDefaultCenterStrCellStyle();
                int sheet = 0;
                int row = 0;
                // 表格sheet名称
                excel.getWb().createSheet("信用拆借报表");
                // 设置表头字段名
                excel.writeStr(sheet, row, "A", center, "审批单编号");
                excel.writeStr(sheet, row, "B", center, "成交单编号");
                excel.writeStr(sheet, row, "C", center, "opics交易号");
                excel.writeStr(sheet, row, "D", center, "审批状态");
                excel.writeStr(sheet, row, "E", center, "本方方向");
                excel.writeStr(sheet, row, "F", center, "对手方机构名称");
                excel.writeStr(sheet, row, "G", center, "拆借金额(万元)");
                excel.writeStr(sheet, row, "H", center, "拆借利率");
                excel.writeStr(sheet, row, "I", center, "起息日");
                excel.writeStr(sheet, row, "J", center, "到期结算日");
                excel.writeStr(sheet, row, "K", center, "期限");
                excel.writeStr(sheet, row, "L", center, "实际占款天数");
                excel.writeStr(sheet, row, "M", center, "到期结算金额(元)");
                excel.writeStr(sheet, row, "N", center, "计息基准");
                excel.writeStr(sheet, row, "O", center, "审批发起人");
                excel.writeStr(sheet, row, "P", center, "审批发起时间");
                // 设置列宽
                excel.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(8, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(9, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(10, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(11, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(12, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(13, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(14, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(15, 20 * 256);
                IfsCfetsrmbIbo bean = new IfsCfetsrmbIbo();
                Map<String, String> upmap = new HashMap<String, String>();
                Map<String, String> upmap2 = new HashMap<String, String>();
                Map<String, String> approveMap = new HashMap<String, String>();
                for (int i = 0; i < data.size(); i++) {
                    bean = data.get(i);
                    row++;
                    approveMap.put("dictId", "ApproveStatus");
                    approveMap.put("dictKey", bean.getApproveStatus());
                    String approveStatus = ifsService.queryDictValue(approveMap); // 审批状态
                    upmap.put("dictId", "lending");
                    upmap.put("dictKey", bean.getMyDir());
                    String myDir = ifsService.queryDictValue(upmap); // 取数据字典中的value值
                    upmap2.put("dictId", "calIntDays");
                    upmap2.put("dictKey", bean.getBasis());
                    String basis = ifsService.queryDictValue(upmap2); // 取数据字典中的value值
                    // 插入数据
                    excel.writeStr(sheet, row, "A", center, bean.getTicketId());
                    excel.writeStr(sheet, row, "B", center, bean.getContractId());
                    excel.writeStr(sheet, row, "C", center, bean.getDealNo());
                    if (!StringUtil.isNullOrEmpty(map.get("deal"))) {
                        excel.writeStr(sheet, row, "D", center, approveStatus);
                    } else {
                        excel.writeStr(sheet, row, "D", center, bean.getTaskName());
                    }
                    excel.writeStr(sheet, row, "E", center, myDir); // 本方方向
                    excel.writeStr(sheet, row, "F", center, bean.getRemoveInst());
                    excel.writeStr(sheet, row, "G", center, bean.getAmt() == null ? "" : bean.getAmt().toString());
                    excel.writeStr(sheet, row, "H", center, bean.getRate() == null ? "" : bean.getRate().toString());
                    excel.writeStr(sheet, row, "I", center, bean.getFirstSettlementDate());
                    excel.writeStr(sheet, row, "J", center, bean.getSecondSettlementDate());
                    excel.writeStr(sheet, row, "K", center, bean.getTenor());
                    excel.writeStr(sheet, row, "L", center, bean.getOccupancyDays() == null ? "" : bean.getOccupancyDays().toString());
                    excel.writeStr(sheet, row, "M", center, bean.getSettlementAmount() == null ? "" : bean.getSettlementAmount().toString());
                    excel.writeStr(sheet, row, "N", center, basis); // 计息基准
                    excel.writeStr(sheet, row, "O", center, bean.getBorrowTrader());
                    excel.writeStr(sheet, row, "P", center, bean.getaDate());
                }
            } else if ("债券借贷".equals(prdName)) {
                /* 先查数据，再设置表格属性以及内容 */
                Page<IfsCfetsrmbSl> data = new Page<IfsCfetsrmbSl>();
                String approveType = map.get("approveType");
                Map<String, Object> params = new HashMap<String, Object>();
                params.put("contractId", map.get("contractId")); // 成交单编号
                params.put("forDate", map.get("forDate")); // 成交日期
                params.put("approveStatus", map.get("approveStatus")); // 审批状态
                params.put("branchId", branchId);
                params.put("pageNumber", "1");
                params.put("pageSize", "999");
                params.put("startDay", startDay);
                params.put("endDay", endDay);
                params.put("ticketId", ticketId);
                params.put("sponInst", sponInst);
                if (!StringUtil.isNullOrEmpty(map.get("deal"))) { // 总交易查询导出
                    params.put("isActive", DictConstants.YesNo.YES);
                    params.put("userId", SlSessionHelper.getUserId());
                    data = qryTotalDealService.searchTotalDealZJJD(params, 3);
                } else { // 正式交易台查询导出
                    if ("mine".equals(approveType)) {
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        data = ifsService.getRmbSlMinePage(params, 3);
                    } else if ("approve".equals(approveType)) {
                        String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + ","
                                + DictConstants.ApproveStatus.VerifyRefuse + "," + DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
                        String[] orderStatus = approveStatus.split(",");
                        params.put("approveStatusNo", orderStatus);
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        data = ifsService.getRmbSlMinePage(params, 1);
                    } else {
                        String approveStatusNo = ParameterUtil.getString(params, "approveStatus", "");
                        if (StringUtil.isNullOrEmpty(approveStatusNo)) {
                            approveStatusNo = DictConstants.ApproveStatus.ApprovedPass;
                            approveStatusNo += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
                            approveStatusNo += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
                            approveStatusNo += "," + DictConstants.ApproveStatus.Verified;
                            approveStatusNo += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
                            // 20180502修改bug:已审批列表中增加"审批中"
                            approveStatusNo += "," + DictConstants.ApproveStatus.Approving;
                            String[] orderStatus = approveStatusNo.split(",");
                            params.put("approveStatusNo", orderStatus);
                        }
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        data = ifsService.getRmbSlMinePage(params, 2);
                    }
                }
                // 表头
                excel = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
                CellStyle center = excel.getDefaultCenterStrCellStyle();
                int sheet = 0;
                int row = 0;
                // 表格sheet名称
                excel.getWb().createSheet("债券借贷报表");
                // 设置表头字段名
                excel.writeStr(sheet, row, "A", center, "审批单号");
                excel.writeStr(sheet, row, "B", center, "成交单号");
                excel.writeStr(sheet, row, "C", center, "opics单号");
                excel.writeStr(sheet, row, "D", center, "交易品种");
                excel.writeStr(sheet, row, "E", center, "标的债券简称");
                excel.writeStr(sheet, row, "F", center, "标的债券券面总额(万元)");
                excel.writeStr(sheet, row, "G", center, "本方方向");
                excel.writeStr(sheet, row, "H", center, "融出方机构");
                excel.writeStr(sheet, row, "I", center, "首次结算日");
                excel.writeStr(sheet, row, "J", center, "到期结算日");
                excel.writeStr(sheet, row, "K", center, "首次结算方式");
                excel.writeStr(sheet, row, "L", center, "到期结算方式");
                excel.writeStr(sheet, row, "M", center, "融入方机构");
                excel.writeStr(sheet, row, "N", center, "审批状态");
                excel.writeStr(sheet, row, "O", center, "成交日期");
                excel.writeStr(sheet, row, "P", center, "审批发起人");
                excel.writeStr(sheet, row, "Q", center, "审批发起机构");
                excel.writeStr(sheet, row, "R", center, "审批发起时间");
                // 设置列宽
                excel.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(8, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(9, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(10, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(11, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(12, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(13, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(14, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(15, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(16, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(17, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(18, 20 * 256);
                IfsCfetsrmbSl bean = new IfsCfetsrmbSl();
                Map<String, String> upmap = new HashMap<String, String>();
                Map<String, String> approveMap = new HashMap<String, String>();
                for (int i = 0; i < data.size(); i++) {
                    bean = data.get(i);
                    row++;
                    upmap.put("dictId", "SlTrading");
                    upmap.put("dictKey", bean.getMyDir());
                    String myDir = ifsService.queryDictValue(upmap); // 取数据字典中的value值
                    approveMap.put("dictId", "ApproveStatus");
                    approveMap.put("dictKey", bean.getApproveStatus());
                    String approveStatus = ifsService.queryDictValue(approveMap); // 审批状态
                    // 插入数据
                    excel.writeStr(sheet, row, "A", center, bean.getTicketId());
                    excel.writeStr(sheet, row, "B", center, bean.getContractId());
                    excel.writeStr(sheet, row, "C", center, bean.getDealNo());
                    excel.writeStr(sheet, row, "D", center, bean.getTradingProduct());
                    excel.writeStr(sheet, row, "E", center, bean.getUnderlyingSymbol());
                    excel.writeStr(sheet, row, "F", center, bean.getUnderlyingQty() == null ? "" : bean.getUnderlyingQty().toString());
                    excel.writeStr(sheet, row, "G", center, myDir); // 本方方向
                    excel.writeStr(sheet, row, "H", center, bean.getLendInst());
                    excel.writeStr(sheet, row, "I", center, bean.getFirstSettlementDate());
                    excel.writeStr(sheet, row, "J", center, bean.getSecondSettlementDate());
                    excel.writeStr(sheet, row, "K", center, bean.getFirstSettlementMethod());
                    excel.writeStr(sheet, row, "L", center, bean.getSecondSettlementMethod());
                    excel.writeStr(sheet, row, "M", center, bean.getBorrowInst());
                    if (!StringUtil.isNullOrEmpty(map.get("deal"))) {
                        excel.writeStr(sheet, row, "N", center, approveStatus);
                    } else {
                        excel.writeStr(sheet, row, "N", center, bean.getTaskName());
                    }
                    excel.writeStr(sheet, row, "O", center, bean.getForDate());
                    excel.writeStr(sheet, row, "P", center, bean.getSponsor());
                    excel.writeStr(sheet, row, "Q", center, bean.getSponInst());
                    excel.writeStr(sheet, row, "R", center, bean.getaDate());
                }
            } else if ("质押式回购".equals(prdName)) {
                /* 先查数据，再设置表格属性以及内容 */
                Page<IfsCfetsrmbCr> data = new Page<IfsCfetsrmbCr>();
                String approveType = map.get("approveType");
                Map<String, Object> params = new HashMap<String, Object>();
                params.put("contractId", map.get("contractId")); // 成交单编号
                params.put("forDate", map.get("forDate")); // 成交日期
                params.put("approveStatus", map.get("approveStatus")); // 审批状态
                params.put("branchId", branchId);
                params.put("pageNumber", "1");
                params.put("pageSize", "999");
                params.put("startDay", startDay);
                params.put("endDay", endDay);
                params.put("ticketId", ticketId);
                params.put("sponInst", sponInst);
                if (!StringUtil.isNullOrEmpty(map.get("deal"))) { // 总交易查询导出
                    params.put("isActive", DictConstants.YesNo.YES);
                    params.put("userId", SlSessionHelper.getUserId());
                    data = qryTotalDealService.searchTotalDealZYSHG(params, 3);
                } else { // 正式交易台查询导出
                    if ("mine".equals(approveType)) {
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        data = ifsService.getRmbCrMinePage(params, 3);
                    } else if ("approve".equals(approveType)) {
                        String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + ","
                                + DictConstants.ApproveStatus.VerifyRefuse + "," + DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
                        String[] orderStatus = approveStatus.split(",");
                        params.put("approveStatusNo", orderStatus);
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        data = ifsService.getRmbCrMinePage(params, 1);
                    } else {
                        String approveStatusNo = ParameterUtil.getString(params, "approveStatus", "");
                        if (StringUtil.isNullOrEmpty(approveStatusNo)) {
                            approveStatusNo = DictConstants.ApproveStatus.ApprovedPass;
                            approveStatusNo += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
                            approveStatusNo += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
                            approveStatusNo += "," + DictConstants.ApproveStatus.Verified;
                            approveStatusNo += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
                            // 20180502修改bug:已审批列表中增加"审批中"
                            approveStatusNo += "," + DictConstants.ApproveStatus.Approving;
                            String[] orderStatus = approveStatusNo.split(",");
                            params.put("approveStatusNo", orderStatus);
                        }
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        data = ifsService.getRmbCrMinePage(params, 2);
                    }
                }
                // 表头
                excel = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
                CellStyle center = excel.getDefaultCenterStrCellStyle();
                int sheet = 0;
                int row = 0;
                // 表格sheet名称
                excel.getWb().createSheet("质押式回购报表");
                // 设置表头字段名
                excel.writeStr(sheet, row, "A", center, "审批单编号");
                excel.writeStr(sheet, row, "B", center, "成交单编号");
                excel.writeStr(sheet, row, "C", center, "opics交易号");
                excel.writeStr(sheet, row, "D", center, "审批状态");
                excel.writeStr(sheet, row, "E", center, "本方方向");
                excel.writeStr(sheet, row, "F", center, "对手方机构名称");
                excel.writeStr(sheet, row, "G", center, "交易金额");
                excel.writeStr(sheet, row, "H", center, "回购利率(%)");
                excel.writeStr(sheet, row, "I", center, "起息日");
                excel.writeStr(sheet, row, "J", center, "到期结算日");
                excel.writeStr(sheet, row, "K", center, "期限");
                excel.writeStr(sheet, row, "L", center, "实际占款天数");
                excel.writeStr(sheet, row, "M", center, "到期结算金额(元)");
                excel.writeStr(sheet, row, "N", center, "计息基准");
                excel.writeStr(sheet, row, "O", center, "审批发起人");
                excel.writeStr(sheet, row, "P", center, "审批发起时间");
                // 设置列宽
                excel.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(8, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(9, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(10, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(11, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(12, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(13, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(14, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(15, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(16, 20 * 256);
                IfsCfetsrmbCr bean = new IfsCfetsrmbCr();
                Map<String, String> upmap = new HashMap<String, String>();
                Map<String, String> approveMap = new HashMap<String, String>();
                for (int i = 0; i < data.size(); i++) {
                    bean = data.get(i);
                    row++;
                    approveMap.put("dictId", "ApproveStatus");
                    approveMap.put("dictKey", bean.getApproveStatus());
                    String approveStatus = ifsService.queryDictValue(approveMap); // 审批状态
                    upmap.put("dictId", "repoing");
                    upmap.put("dictKey", bean.getMyDir());
                    String myDir = ifsService.queryDictValue(upmap); // 取数据字典中的value值
                    upmap.put("dictId", "calIntDays");
                    upmap.put("dictKey", bean.getBasis());
                    String basis = ifsService.queryDictValue(upmap); // 取数据字典中的value值
                    // 插入数据
                    excel.writeStr(sheet, row, "A", center, bean.getTicketId());
                    excel.writeStr(sheet, row, "B", center, bean.getContractId());
                    excel.writeStr(sheet, row, "C", center, bean.getDealNo());
                    if (!StringUtil.isNullOrEmpty(map.get("deal"))) {
                        excel.writeStr(sheet, row, "D", center, approveStatus);
                    } else {
                        excel.writeStr(sheet, row, "D", center, bean.getTaskName());
                    }
                    excel.writeStr(sheet, row, "E", center, myDir); // 本方方向
                    excel.writeStr(sheet, row, "F", center, bean.getReverseInst());
                    excel.writeStr(sheet, row, "G", center, bean.getTradeAmount() == null ? "" : bean.getTradeAmount().toString());
                    excel.writeStr(sheet, row, "H", center, bean.getRepoRate().toString());
                    excel.writeStr(sheet, row, "I", center, bean.getForDate());
                    excel.writeStr(sheet, row, "J", center, bean.getSecondSettlementDate());
                    excel.writeStr(sheet, row, "K", center, bean.getTenor());
                    excel.writeStr(sheet, row, "L", center, bean.getOccupancyDays() == null ? "" : bean.getOccupancyDays().toString());
                    excel.writeStr(sheet, row, "M", center, bean.getSecondSettlementAmount() == null ? "" : bean.getSecondSettlementAmount().toString());
                    excel.writeStr(sheet, row, "N", center, basis); // 计息基准
                    excel.writeStr(sheet, row, "O", center, bean.getPositiveTrader());
                    excel.writeStr(sheet, row, "P", center, bean.getaDate());
                }
            } else if ("黄金拆借".equals(prdName)) {
            } else if ("外币债".equals(prdName)) {
                /* 先查数据，再设置表格属性以及内容 */
                Page<IfsCfetsrmbCbt> data = new Page<IfsCfetsrmbCbt>();
                String approveType = map.get("approveType");
                Map<String, Object> params = new HashMap<String, Object>();
                params.put("contractId", map.get("contractId")); // 成交单编号
                params.put("forDate", map.get("forDate")); // 成交日期
                params.put("approveStatus", map.get("approveStatus")); // 审批状态
                params.put("branchId", branchId);
                params.put("pageNumber", "1");
                params.put("pageSize", "999");
                if ("mine".equals(approveType)) {
                    params.put("isActive", DictConstants.YesNo.YES);
                    params.put("userId", SlSessionHelper.getUserId());
                    data = ifsService.getRmbCbtMinePage(params, 3);
                } else if ("approve".equals(approveType)) {
                    String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + ","
                            + DictConstants.ApproveStatus.VerifyRefuse + "," + DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
                    String[] orderStatus = approveStatus.split(",");
                    params.put("approveStatusNo", orderStatus);
                    params.put("isActive", DictConstants.YesNo.YES);
                    params.put("userId", SlSessionHelper.getUserId());
                    data = ifsService.getRmbCbtMinePage(params, 1);
                } else {
                    String approveStatus = ParameterUtil.getString(params, "approveStatus", "");
                    if (StringUtil.isNullOrEmpty(approveStatus)) {
                        approveStatus = DictConstants.ApproveStatus.ApprovedPass;
                        approveStatus += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
                        approveStatus += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
                        approveStatus += "," + DictConstants.ApproveStatus.Verified;
                        approveStatus += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
                        // 20180502修改bug:已审批列表中增加"审批中"
                        approveStatus += "," + DictConstants.ApproveStatus.Approving;
                        String[] orderStatus = approveStatus.split(",");
                        params.put("approveStatusNo", orderStatus);
                    }
                    params.put("isActive", DictConstants.YesNo.YES);
                    params.put("userId", SlSessionHelper.getUserId());
                    data = ifsService.getRmbCbtMinePage(params, 2);
                }
                // 表头
                excel = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
                CellStyle center = excel.getDefaultCenterStrCellStyle();
                int sheet = 0;
                int row = 0;
                // 表格sheet名称
                excel.getWb().createSheet("外币债报表");
                // 设置表头字段名
                excel.writeStr(sheet, row, "A", center, "opics交易号");
                excel.writeStr(sheet, row, "B", center, "债券名称");
                excel.writeStr(sheet, row, "C", center, "券面总额(万元)");
                excel.writeStr(sheet, row, "D", center, "到期收益率(%)");
                excel.writeStr(sheet, row, "E", center, "交易金额(元)");
                excel.writeStr(sheet, row, "F", center, "结算金额(元)");
                excel.writeStr(sheet, row, "G", center, "结算方式");
                excel.writeStr(sheet, row, "H", center, "结算日");
                excel.writeStr(sheet, row, "I", center, "买入方机构");
                excel.writeStr(sheet, row, "J", center, "卖出方机构");
                excel.writeStr(sheet, row, "K", center, "成交单编号");
                excel.writeStr(sheet, row, "L", center, "审批状态");
                excel.writeStr(sheet, row, "M", center, "成交日期");
                excel.writeStr(sheet, row, "N", center, "审批发起人");
                excel.writeStr(sheet, row, "O", center, "审批发起机构");
                excel.writeStr(sheet, row, "P", center, "审批发起时间");
                excel.writeStr(sheet, row, "Q", center, "审批单号");
                // 设置列宽
                excel.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(8, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(9, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(10, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(11, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(12, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(13, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(14, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(15, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(16, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(17, 20 * 256);
                IfsCfetsrmbCbt bean = new IfsCfetsrmbCbt();
                Map<String, String> upmap = new HashMap<String, String>();
                for (int i = 0; i < data.size(); i++) {
                    bean = data.get(i);
                    row++;
                    upmap.put("dictId", "SettlementMethod");
                    upmap.put("dictKey", bean.getSettlementMethod());
                    String settlementMethod = ifsService.queryDictValue(upmap); // 取数据字典中的value值
                    // 插入数据
                    excel.writeStr(sheet, row, "A", center, bean.getDealNo());
                    excel.writeStr(sheet, row, "B", center, bean.getBondName());
                    excel.writeStr(sheet, row, "C", center, bean.getTotalFaceValue() == null ? "" : bean.getTotalFaceValue().toString());
                    excel.writeStr(sheet, row, "D", center, bean.getYield() == null ? "" : bean.getYield().toString());
                    excel.writeStr(sheet, row, "E", center, bean.getTradeAmount() == null ? "" : bean.getTradeAmount().toString());
                    excel.writeStr(sheet, row, "F", center, bean.getSettlementAmount() == null ? "" : bean.getSettlementAmount().toString());
                    excel.writeStr(sheet, row, "G", center, settlementMethod); // 结算方式
                    excel.writeStr(sheet, row, "H", center, bean.getSettlementDate());
                    excel.writeStr(sheet, row, "I", center, bean.getBuyInst());
                    excel.writeStr(sheet, row, "J", center, bean.getSellInst());
                    excel.writeStr(sheet, row, "K", center, bean.getContractId());
                    excel.writeStr(sheet, row, "L", center, bean.getTaskName());
                    excel.writeStr(sheet, row, "M", center, bean.getForDate());
                    excel.writeStr(sheet, row, "N", center, bean.getSponsor());
                    excel.writeStr(sheet, row, "O", center, bean.getSponInst());
                    excel.writeStr(sheet, row, "P", center, bean.getaDate());
                    excel.writeStr(sheet, row, "Q", center, bean.getTicketId());
                }
            } else if ("货币掉期".equals(prdName)) {
            } else if ("外汇掉期".equals(prdName)) {
                /* 先查数据，再设置表格属性以及内容 */
                Page<IfsCfetsfxSwap> data = new Page<IfsCfetsfxSwap>();
                String approveType = map.get("approveType");
                Map<String, Object> params = new HashMap<String, Object>();
                params.put("contractId", map.get("contractId")); // 成交单编号
                params.put("approveStatus", map.get("approveStatus")); // 审批状态
                params.put("currencyPair", map.get("currencyPair")); // 货币对
                params.put("branchId", branchId);
                params.put("pageNumber", "1");
                params.put("pageSize", "999");
                params.put("startDay", startDay);
                params.put("endDay", endDay);
                params.put("ticketId", ticketId);
                params.put("sponInst", sponInst);
                params.put("dealNo", map.get("dealNo"));
                if (map.get("startDate") != null && map.get("startDate").length() >= 10) {
                    params.put("startDate", map.get("startDate").substring(0, 10));
                } else {
                    params.put("startDate", map.get("startDate"));
                }
                if (map.get("endDate") != null && map.get("endDate").length() >= 10) {
                    params.put("endDate", map.get("endDate").substring(0, 10));
                } else {
                    params.put("endDate", map.get("endDate"));
                }
                if (!StringUtil.isNullOrEmpty(map.get("deal"))) { // 总交易查询导出
                    params.put("isActive", DictConstants.YesNo.YES);
                    params.put("userId", SlSessionHelper.getUserId());
                    data = qryTotalDealService.searchTotalDealRMBWBDQ(params, 3);
                } else { // 正式交易台查询导出
                    if ("mine".equals(approveType)) {
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        data = ifsService.getFxSwapMinePage(params, 3);
                    } else if ("approve".equals(approveType)) {
                        String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + ","
                                + DictConstants.ApproveStatus.VerifyRefuse + "," + DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
                        String[] orderStatus = approveStatus.split(",");
                        params.put("approveStatusNo", orderStatus);
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        data = ifsService.getFxSwapMinePage(params, 1);
                    } else {
                        String approveStatusNo = ParameterUtil.getString(params, "approveStatus", "");
                        if (StringUtil.isNullOrEmpty(approveStatusNo)) {
                            approveStatusNo = DictConstants.ApproveStatus.ApprovedPass;
                            approveStatusNo += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
                            approveStatusNo += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
                            approveStatusNo += "," + DictConstants.ApproveStatus.Verified;
                            approveStatusNo += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
                            approveStatusNo += "," + DictConstants.ApproveStatus.Approving;
                            String[] orderStatus = approveStatusNo.split(",");
                            params.put("approveStatusNo", orderStatus);
                        }
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        data = ifsService.getFxSwapMinePage(params, 2);
                    }
                }
                // 表头
                excel = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
                CellStyle center = excel.getDefaultCenterStrCellStyle();
                int sheet = 0;
                int row = 0;
                // 表格sheet名称
                excel.getWb().createSheet("外汇掉期报表");
                // 设置表头字段名
                excel.writeStr(sheet, row, "A", center, "审批单号");
                excel.writeStr(sheet, row, "B", center, "opics交易号");
                excel.writeStr(sheet, row, "C", center, "交易来源");
                excel.writeStr(sheet, row, "D", center, "货币对");
                excel.writeStr(sheet, row, "E", center, "交易方向");
                excel.writeStr(sheet, row, "F", center, "近端汇率");
                excel.writeStr(sheet, row, "G", center, "买入金额(近端)");
                excel.writeStr(sheet, row, "H", center, "卖出金额(近端)");
                excel.writeStr(sheet, row, "I", center, "起息日(近端)");
                excel.writeStr(sheet, row, "J", center, "起息日(远端)");
                excel.writeStr(sheet, row, "K", center, "对方机构");
                excel.writeStr(sheet, row, "L", center, "成交单编号");
                excel.writeStr(sheet, row, "M", center, "审批状态");
                excel.writeStr(sheet, row, "N", center, "交易日期");
                excel.writeStr(sheet, row, "O", center, "审批发起人");
                excel.writeStr(sheet, row, "P", center, "审批发起机构");
                excel.writeStr(sheet, row, "Q", center, "审批发起时间");
                // 设置列宽
                excel.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(8, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(9, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(10, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(11, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(12, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(13, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(14, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(15, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(16, 20 * 256);
                IfsCfetsfxSwap bean = new IfsCfetsfxSwap();
                Map<String, String> upmap = new HashMap<String, String>();
                Map<String, String> approveMap = new HashMap<String, String>();
                Map<String, String> tradeSourceMap = new HashMap<String, String>();
                for (int i = 0; i < data.size(); i++) {
                    bean = data.get(i);
                    row++;

                    tradeSourceMap.put("dictId", "TradeSource");
                    tradeSourceMap.put("dictKey", bean.getDealSource());
                    String dealSource = ifsService.queryDictValue(tradeSourceMap); // 交易来源

                    upmap.put("dictId", "Swaptrading");
                    upmap.put("dictKey", bean.getDirection());
                    String direction = ifsService.queryDictValue(upmap); // 交易方向

                    approveMap.put("dictId", "ApproveStatus");
                    approveMap.put("dictKey", bean.getApproveStatus());
                    String approveStatus = ifsService.queryDictValue(approveMap); // 审批状态
                    DecimalFormat df = new DecimalFormat("#,##0.00000000");//利率格式化
                    // 插入数据
                    excel.writeStr(sheet, row, "A", center, bean.getTicketId());
                    excel.writeStr(sheet, row, "B", center, bean.getDealNo());
                    excel.writeStr(sheet, row, "C", center, dealSource);
                    excel.writeStr(sheet, row, "D", center, bean.getCurrencyPair());
                    excel.writeStr(sheet, row, "E", center, direction);
                    excel.writeStr(sheet, row, "F", center, df.format(Double.valueOf(bean.getNearRate())));
                    excel.writeStr(sheet, row, "G", center, bean.getNearPrice());
                    excel.writeStr(sheet, row, "H", center, bean.getNearReversePrice());
                    excel.writeStr(sheet, row, "I", center, bean.getNearValuedate());
                    excel.writeStr(sheet, row, "J", center, bean.getFwdValuedate());
                    excel.writeStr(sheet, row, "K", center, bean.getCounterpartyInstId());
                    excel.writeStr(sheet, row, "L", center, bean.getContractId());
                    if (!StringUtil.isNullOrEmpty(map.get("deal"))) {
                        excel.writeStr(sheet, row, "M", center, approveStatus);
                    } else {
                        excel.writeStr(sheet, row, "M", center, bean.getTaskName());
                    }
                    excel.writeStr(sheet, row, "N", center, bean.getForDate());
                    excel.writeStr(sheet, row, "O", center, bean.getSponsor());
                    excel.writeStr(sheet, row, "P", center, bean.getSponInst());
                    excel.writeStr(sheet, row, "Q", center, bean.getaDate());
                }
            } else if ("人民币期权".equals(prdName)) {
                /* 先查数据，再设置表格属性以及内容 */
                Page<IfsCfetsfxOption> data = new Page<IfsCfetsfxOption>();
                String approveType = map.get("approveType");
                Map<String, Object> params = new HashMap<String, Object>();
                params.put("contractId", map.get("contractId")); // 成交单编号
                params.put("approveStatus", map.get("approveStatus")); // 审批状态
                params.put("currencyPair", map.get("currencyPair")); // 货币对
                params.put("branchId", branchId);
                params.put("pageNumber", "1");
                params.put("pageSize", "999");
                params.put("startDay", startDay);
                params.put("endDay", endDay);
                params.put("ticketId", ticketId);
                params.put("sponInst", sponInst);
                if (map.get("startDate") != null && map.get("startDate").length() >= 10) {
                    params.put("startDate", map.get("startDate").substring(0, 10));
                } else {
                    params.put("startDate", map.get("startDate"));
                }
                if (map.get("endDate") != null && map.get("endDate").length() >= 10) {
                    params.put("endDate", map.get("endDate").substring(0, 10));
                } else {
                    params.put("endDate", map.get("endDate"));
                }
                if (!StringUtil.isNullOrEmpty(map.get("deal"))) { // 总交易查询导出
                    params.put("isActive", DictConstants.YesNo.YES);
                    params.put("userId", SlSessionHelper.getUserId());
                    data = qryTotalDealService.searchTotalDealRMBQQ(params, 3);
                } else { // 正式交易台查询导出
                    if ("mine".equals(approveType)) {
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        data = ifsService.getOptionMinePage(params, 3);
                    } else if ("approve".equals(approveType)) {
                        String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + ","
                                + DictConstants.ApproveStatus.VerifyRefuse + "," + DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
                        String[] orderStatus = approveStatus.split(",");
                        params.put("approveStatusNo", orderStatus);
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        data = ifsService.getOptionMinePage(params, 1);
                    } else {
                        String approveStatusNo = ParameterUtil.getString(params, "approveStatus", "");
                        if (StringUtil.isNullOrEmpty(approveStatusNo)) {
                            approveStatusNo = DictConstants.ApproveStatus.ApprovedPass;
                            approveStatusNo += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
                            approveStatusNo += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
                            approveStatusNo += "," + DictConstants.ApproveStatus.Verified;
                            approveStatusNo += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
                            approveStatusNo += "," + DictConstants.ApproveStatus.Approving;
                            String[] orderStatus = approveStatusNo.split(",");
                            params.put("approveStatusNo", orderStatus);
                        }
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        data = ifsService.getOptionMinePage(params, 2);
                    }
                }

                // 表头
                excel = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
                CellStyle center = excel.getDefaultCenterStrCellStyle();
                int sheet = 0;
                int row = 0;
                // 表格sheet名称
                excel.getWb().createSheet("人民币期权报表");
                // 设置表头字段名
                excel.writeStr(sheet, row, "A", center, "opics交易号");
                excel.writeStr(sheet, row, "B", center, "交易来源");
                excel.writeStr(sheet, row, "C", center, "成交单编号");
                excel.writeStr(sheet, row, "D", center, "审批状态");
                excel.writeStr(sheet, row, "E", center, "本方机构");
                excel.writeStr(sheet, row, "F", center, "对方机构");
                excel.writeStr(sheet, row, "G", center, "交易日期");
                excel.writeStr(sheet, row, "H", center, "交易方向");
                excel.writeStr(sheet, row, "I", center, "名义本金（买）");
                excel.writeStr(sheet, row, "J", center, "名义本金（卖）");
                excel.writeStr(sheet, row, "K", center, "货币对");
                excel.writeStr(sheet, row, "L", center, "执行价");
                excel.writeStr(sheet, row, "M", center, "行权日");
                excel.writeStr(sheet, row, "N", center, "交割日");
                excel.writeStr(sheet, row, "O", center, "交割方式");
                excel.writeStr(sheet, row, "P", center, "执行方式");
                excel.writeStr(sheet, row, "Q", center, "审批发起人");
                excel.writeStr(sheet, row, "R", center, "审批发起机构");
                excel.writeStr(sheet, row, "S", center, "审批发起时间");
                excel.writeStr(sheet, row, "T", center, "审批单号");
                // 设置列宽
                excel.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(8, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(9, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(10, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(11, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(12, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(13, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(14, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(15, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(16, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(17, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(18, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(19, 20 * 256);
                IfsCfetsfxOption bean = new IfsCfetsfxOption();
                Map<String, String> upmap = new HashMap<String, String>();
                Map<String, String> upmap2 = new HashMap<String, String>();
                Map<String, String> tradeSourceMap = new HashMap<String, String>();
                Map<String, String> approveMap = new HashMap<String, String>();
                for (int i = 0; i < data.size(); i++) {
                    bean = data.get(i);
                    row++;

                    tradeSourceMap.put("dictId", "TradeSource");
                    tradeSourceMap.put("dictKey", bean.getDealSource());
                    String dealSource = ifsService.queryDictValue(tradeSourceMap); // 交易来源

                    approveMap.put("dictId", "ApproveStatus");
                    approveMap.put("dictKey", bean.getApproveStatus());
                    String approveStatus = ifsService.queryDictValue(approveMap); // 审批状态

                    upmap.put("dictId", "trading");
                    upmap.put("dictKey", bean.getDirection());
                    String direction = ifsService.queryDictValue(upmap); // 交易方向

                    upmap2.put("dictId", "DeliveryType");
                    upmap2.put("dictKey", bean.getDeliveryType());
                    String deliveryType = ifsService.queryDictValue(upmap2); // 交割方式

                    // 插入数据
                    excel.writeStr(sheet, row, "A", center, bean.getDealNo());
                    excel.writeStr(sheet, row, "B", center, dealSource);
                    excel.writeStr(sheet, row, "C", center, bean.getContractId());
                    if (!StringUtil.isNullOrEmpty(map.get("deal"))) {
                        excel.writeStr(sheet, row, "D", center, approveStatus);
                    } else {
                        excel.writeStr(sheet, row, "D", center, bean.getTaskName());
                    }
                    excel.writeStr(sheet, row, "E", center, bean.getInstId());
                    excel.writeStr(sheet, row, "F", center, bean.getCounterpartyInstId());
                    excel.writeStr(sheet, row, "G", center, bean.getForDate());
                    excel.writeStr(sheet, row, "H", center, direction);
                    excel.writeStr(sheet, row, "I", center, bean.getBuyNotional());
                    excel.writeStr(sheet, row, "J", center, bean.getSellNotional());
                    excel.writeStr(sheet, row, "K", center, bean.getCurrencyPair());
                    excel.writeStr(sheet, row, "L", center, bean.getStrikePrice());
                    excel.writeStr(sheet, row, "M", center, bean.getExpiryDate());
                    excel.writeStr(sheet, row, "N", center, bean.getDeliveryDate());
                    excel.writeStr(sheet, row, "O", center, deliveryType);
                    excel.writeStr(sheet, row, "P", center, bean.getExerciseType());
                    excel.writeStr(sheet, row, "Q", center, bean.getSponsor());
                    excel.writeStr(sheet, row, "R", center, bean.getSponInst());
                    excel.writeStr(sheet, row, "S", center, bean.getaDate());
                    excel.writeStr(sheet, row, "T", center, bean.getTicketId());
                }
            } else if ("外币拆借".equals(prdName)) {
                /* 先查数据，再设置表格属性以及内容 */
                Page<IfsCfetsfxLend> data = new Page<IfsCfetsfxLend>();
                String approveType = map.get("approveType");
                Map<String, Object> params = new HashMap<String, Object>();
                params.put("contractId", map.get("contractId")); // 成交单编号
                params.put("forDate", map.get("forDate")); // 成交日期
                params.put("approveStatus", map.get("approveStatus")); // 审批状态
                params.put("branchId", branchId);
                params.put("pageNumber", "1");
                params.put("pageSize", "999");
                params.put("startDay", startDay);
                params.put("endDay", endDay);
                params.put("ticketId", ticketId);
                params.put("sponInst", sponInst);
                params.put("dealNo", map.get("dealNo"));
                if (map.get("startDate") != null && map.get("startDate").length() >= 10) {
                    params.put("startDate", map.get("startDate").substring(0, 10));
                } else {
                    params.put("startDate", map.get("startDate"));
                }
                if (map.get("endDate") != null && map.get("endDate").length() >= 10) {
                    params.put("endDate", map.get("endDate").substring(0, 10));
                } else {
                    params.put("endDate", map.get("endDate"));
                }
                if (!StringUtil.isNullOrEmpty(map.get("deal"))) { // 总交易查询导出
                    params.put("isActive", DictConstants.YesNo.YES);
                    params.put("userId", SlSessionHelper.getUserId());
                    data = qryTotalDealService.searchTotalDealWBCQ(params, 3);
                } else { // 正式交易台查询导出
                    if ("mine".equals(approveType)) {
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        data = ifsService.getCcyLendingMinePage(params, 3);
                    } else if ("approve".equals(approveType)) {
                        String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + ","
                                + DictConstants.ApproveStatus.VerifyRefuse + "," + DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
                        String[] orderStatus = approveStatus.split(",");
                        params.put("approveStatusNo", orderStatus);
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        data = ifsService.getCcyLendingMinePage(params, 1);
                    } else {
                        String approveStatusNo = ParameterUtil.getString(params, "approveStatus", "");
                        if (StringUtil.isNullOrEmpty(approveStatusNo)) {
                            approveStatusNo = DictConstants.ApproveStatus.ApprovedPass;
                            approveStatusNo += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
                            approveStatusNo += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
                            approveStatusNo += "," + DictConstants.ApproveStatus.Verified;
                            approveStatusNo += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
                            approveStatusNo += "," + DictConstants.ApproveStatus.Approving;
                            String[] orderStatus = approveStatusNo.split(",");
                            params.put("approveStatusNo", orderStatus);
                        }
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        data = ifsService.getCcyLendingMinePage(params, 2);
                    }
                }

                // 表头
                excel = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
                CellStyle center = excel.getDefaultCenterStrCellStyle();
                int sheet = 0;
                int row = 0;
                // 表格sheet名称
                excel.getWb().createSheet("外币拆借报表");
                // 设置表头字段名
                excel.writeStr(sheet, row, "A", center, "opics交易号");
                excel.writeStr(sheet, row, "B", center, "交易来源");
                excel.writeStr(sheet, row, "C", center, "货币");
                excel.writeStr(sheet, row, "D", center, "拆借金额");
                excel.writeStr(sheet, row, "E", center, "交易方向");
                excel.writeStr(sheet, row, "F", center, "起息日");
                excel.writeStr(sheet, row, "G", center, "到期日");
                excel.writeStr(sheet, row, "H", center, "成交单编号");
                excel.writeStr(sheet, row, "I", center, "成交日期");
                excel.writeStr(sheet, row, "J", center, "审批状态");
                excel.writeStr(sheet, row, "K", center, "本方机构");
                excel.writeStr(sheet, row, "L", center, "对手方机构");
                excel.writeStr(sheet, row, "M", center, "交易日期");
                excel.writeStr(sheet, row, "N", center, "审批发起人");
                excel.writeStr(sheet, row, "O", center, "审批发起机构");
                excel.writeStr(sheet, row, "P", center, "审批发起时间");
                excel.writeStr(sheet, row, "Q", center, "审批单号");
                // 设置列宽
                excel.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(8, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(9, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(10, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(11, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(12, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(13, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(14, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(15, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(16, 20 * 256);
                IfsCfetsfxLend bean = new IfsCfetsfxLend();
                Map<String, String> upmap = new HashMap<String, String>();
                Map<String, String> tradeSourceMap = new HashMap<String, String>();
                Map<String, String> approveMap = new HashMap<String, String>();
                for (int i = 0; i < data.size(); i++) {
                    bean = data.get(i);
                    row++;

                    tradeSourceMap.put("dictId", "TradeSource");
                    tradeSourceMap.put("dictKey", bean.getDealSource());
                    String dealSource = ifsService.queryDictValue(tradeSourceMap); // 交易来源

                    upmap.put("dictId", "lending");
                    upmap.put("dictKey", bean.getDirection());
                    String direction = ifsService.queryDictValue(upmap); // 交易方向

                    approveMap.put("dictId", "ApproveStatus");
                    approveMap.put("dictKey", bean.getApproveStatus());
                    String approveStatus = ifsService.queryDictValue(approveMap); // 审批状态

                    // 插入数据
                    excel.writeStr(sheet, row, "A", center, bean.getDealNo());
                    excel.writeStr(sheet, row, "B", center, dealSource);
                    excel.writeStr(sheet, row, "C", center, bean.getCurrency());
                    excel.writeDouble(sheet, row, "D", center, bean.getAmount().doubleValue());
                    excel.writeStr(sheet, row, "E", center, direction);
                    excel.writeStr(sheet, row, "F", center, bean.getValueDate());
                    excel.writeStr(sheet, row, "G", center, bean.getMaturityDate());
                    excel.writeStr(sheet, row, "H", center, bean.getContractId());
                    excel.writeStr(sheet, row, "I", center, bean.getForDate());
                    if (!StringUtil.isNullOrEmpty(map.get("deal"))) {
                        excel.writeStr(sheet, row, "J", center, approveStatus);
                    } else {
                        excel.writeStr(sheet, row, "J", center, bean.getTaskName());
                    }
                    excel.writeStr(sheet, row, "K", center, bean.getInstId());
                    excel.writeStr(sheet, row, "L", center, bean.getCounterpartyInstId());
                    excel.writeStr(sheet, row, "M", center, bean.getForDate());
                    excel.writeStr(sheet, row, "N", center, bean.getSponsor());
                    excel.writeStr(sheet, row, "O", center, bean.getSponInst());
                    excel.writeStr(sheet, row, "P", center, bean.getaDate());
                    excel.writeStr(sheet, row, "Q", center, bean.getTicketId());
                }
            } else if ("外币头寸调拨".equals(prdName)) {
            } else if ("外汇远期".equals(prdName)) {
                /* 先查数据，再设置表格属性以及内容 */
                Page<IfsCfetsfxFwd> data = new Page<IfsCfetsfxFwd>();
                String approveType = map.get("approveType");
                Map<String, Object> params = new HashMap<String, Object>();
                params.put("contractId", map.get("contractId")); // 成交单编号
                params.put("approveStatus", map.get("approveStatus")); // 审批状态
                params.put("currencyPair", map.get("currencyPair")); // 货币对
                params.put("branchId", branchId);
                params.put("pageNumber", "1");
                params.put("pageSize", "999");
                params.put("startDay", startDay);
                params.put("endDay", endDay);
                params.put("ticketId", ticketId);
                params.put("sponInst", sponInst);
                params.put("dealNo", map.get("dealNo"));
                if (map.get("startDate") != null && map.get("startDate").length() >= 10) {
                    params.put("startDate", map.get("startDate").substring(0, 10));
                } else {
                    params.put("startDate", map.get("startDate"));
                }
                if (map.get("endDate") != null && map.get("endDate").length() >= 10) {
                    params.put("endDate", map.get("endDate").substring(0, 10));
                } else {
                    params.put("endDate", map.get("endDate"));
                }

                if (!StringUtil.isNullOrEmpty(map.get("deal"))) { // 总交易查询导出
                    params.put("isActive", DictConstants.YesNo.YES);
                    params.put("userId", SlSessionHelper.getUserId());
                    data = qryTotalDealService.searchTotalDealRMBWBYQ(params, 3);
                } else { // 正式交易台查询导出
                    if ("mine".equals(approveType)) {
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        data = ifsService.getFxFwdMinePage(params, 3);
                    } else if ("approve".equals(approveType)) {
                        String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + ","
                                + DictConstants.ApproveStatus.VerifyRefuse + "," + DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
                        String[] orderStatus = approveStatus.split(",");
                        params.put("approveStatusNo", orderStatus);
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        data = ifsService.getFxFwdMinePage(params, 1);
                    } else {
                        String approveStatusNo = ParameterUtil.getString(params, "approveStatus", "");
                        if (StringUtil.isNullOrEmpty(approveStatusNo)) {
                            approveStatusNo = DictConstants.ApproveStatus.ApprovedPass;
                            approveStatusNo += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
                            approveStatusNo += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
                            approveStatusNo += "," + DictConstants.ApproveStatus.Verified;
                            approveStatusNo += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
                            approveStatusNo += "," + DictConstants.ApproveStatus.Approving;
                            String[] orderStatus = approveStatusNo.split(",");
                            params.put("approveStatusNo", orderStatus);
                        }
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        data = ifsService.getFxFwdMinePage(params, 2);
                    }
                }

                // 表头
                excel = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
                CellStyle center = excel.getDefaultCenterStrCellStyle();
                int sheet = 0;
                int row = 0;
                // 表格sheet名称
                excel.getWb().createSheet("外汇远期报表");
                // 设置表头字段名
                excel.writeStr(sheet, row, "A", center, "审批单号");
                excel.writeStr(sheet, row, "B", center, "opics交易号");
                excel.writeStr(sheet, row, "C", center, "交易来源");
                excel.writeStr(sheet, row, "D", center, "货币对");
                excel.writeStr(sheet, row, "E", center, "交易金额");
                excel.writeStr(sheet, row, "F", center, "成交汇率");
                excel.writeStr(sheet, row, "G", center, "交易方向");
                excel.writeStr(sheet, row, "H", center, "成交单编号");
                excel.writeStr(sheet, row, "I", center, "审批状态");
                excel.writeStr(sheet, row, "J", center, "对方机构");
                excel.writeStr(sheet, row, "K", center, "交易日期");
                excel.writeStr(sheet, row, "L", center, "起息日");
                excel.writeStr(sheet, row, "M", center, "审批发起人");
                excel.writeStr(sheet, row, "N", center, "审批发起机构");
                excel.writeStr(sheet, row, "O", center, "审批发起时间");
                // 设置列宽
                excel.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(8, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(9, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(10, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(11, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(12, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(13, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(14, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(15, 20 * 256);
                IfsCfetsfxFwd bean = new IfsCfetsfxFwd();
                Map<String, String> upmap = new HashMap<String, String>();
                Map<String, String> tradeSourceMap = new HashMap<String, String>();
                Map<String, String> approveMap = new HashMap<String, String>();
                for (int i = 0; i < data.size(); i++) {
                    bean = data.get(i);
                    row++;
                    upmap.put("dictId", "trading");
                    upmap.put("dictKey", bean.getBuyDirection());
                    String buyDirection = ifsService.queryDictValue(upmap); // 交易方向

                    tradeSourceMap.put("dictId", "TradeSource");
                    tradeSourceMap.put("dictKey", bean.getDealSource());
                    String dealSource = ifsService.queryDictValue(tradeSourceMap); // 交易来源

                    approveMap.put("dictId", "ApproveStatus");
                    approveMap.put("dictKey", bean.getApproveStatus());
                    String approveStatus = ifsService.queryDictValue(approveMap); // 审批状态
                    DecimalFormat df = new DecimalFormat("#,##0.00000000");//利率格式化
                    // 插入数据
                    excel.writeStr(sheet, row, "A", center, bean.getTicketId());
                    excel.writeStr(sheet, row, "B", center, bean.getDealNo());
                    excel.writeStr(sheet, row, "C", center, dealSource);
                    excel.writeStr(sheet, row, "D", center, bean.getCurrencyPair());
                    excel.writeStr(sheet, row, "E", center, bean.getBuyAmount() == null ? "" : bean.getBuyAmount().toString());
                    excel.writeStr(sheet, row, "F", center, df.format(Double.valueOf(bean.getExchangeRate())));
                    excel.writeStr(sheet, row, "G", center, buyDirection);
                    excel.writeStr(sheet, row, "H", center, bean.getContractId());
                    if (!StringUtil.isNullOrEmpty(map.get("deal"))) {
                        excel.writeStr(sheet, row, "I", center, approveStatus);
                    } else {
                        excel.writeStr(sheet, row, "I", center, bean.getTaskName());
                    }
                    excel.writeStr(sheet, row, "J", center, bean.getCounterpartyInstId());
                    excel.writeStr(sheet, row, "K", center, bean.getForDate());
                    excel.writeStr(sheet, row, "L", center, bean.getValueDate());
                    excel.writeStr(sheet, row, "M", center, bean.getSponsor());
                    excel.writeStr(sheet, row, "N", center, bean.getSponInst());
                    excel.writeStr(sheet, row, "O", center, bean.getaDate());
                }
            } else if ("外汇即期".equals(prdName)) {
                /* 先查数据，再设置表格属性以及内容 */
                Page<IfsCfetsfxSpt> data = new Page<IfsCfetsfxSpt>();
                String approveType = map.get("approveType");
                Map<String, Object> params = new HashMap<String, Object>();
                params.put("contractId", map.get("contractId")); // 成交单编号
                params.put("approveStatus", map.get("approveStatus")); // 审批状态
                params.put("port", map.get("port")); // 投资组合
                params.put("branchId", branchId);
                params.put("pageNumber", "1");
                params.put("pageSize", "999");
                params.put("startDay", startDay);
                params.put("endDay", endDay);
                params.put("ticketId", ticketId);
                params.put("sponInst", sponInst);
                params.put("dealNo", map.get("dealNo"));
                if (map.get("startDate") != null && map.get("startDate").length() >= 10) {
                    params.put("startDate", map.get("startDate").substring(0, 10));
                } else {
                    params.put("startDate", map.get("startDate"));
                }
                if (map.get("endDate") != null && map.get("endDate").length() >= 10) {
                    params.put("endDate", map.get("endDate").substring(0, 10));
                } else {
                    params.put("endDate", map.get("endDate"));
                }

                if (!StringUtil.isNullOrEmpty(map.get("deal"))) { // 总交易查询导出
                    params.put("isActive", DictConstants.YesNo.YES);
                    params.put("userId", SlSessionHelper.getUserId());
                    data = qryTotalDealService.searchTotalDealRMBWBJQ(params, 3);
                } else { // 正式交易台查询导出
                    if ("mine".equals(approveType)) {
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        data = ifsService.getSpotMinePage(params, 3);
                    } else if ("approve".equals(approveType)) {
                        String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + ","
                                + DictConstants.ApproveStatus.VerifyRefuse + "," + DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
                        String[] orderStatus = approveStatus.split(",");
                        params.put("approveStatusNo", orderStatus);
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        data = ifsService.getSpotMinePage(params, 1);
                    } else {
                        String approveStatusNo = ParameterUtil.getString(params, "approveStatus", "");
                        if (StringUtil.isNullOrEmpty(approveStatusNo)) {
                            approveStatusNo = DictConstants.ApproveStatus.ApprovedPass;
                            approveStatusNo += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
                            approveStatusNo += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
                            approveStatusNo += "," + DictConstants.ApproveStatus.Verified;
                            approveStatusNo += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
                            approveStatusNo += "," + DictConstants.ApproveStatus.Approving;
                            String[] orderStatus = approveStatusNo.split(",");
                            params.put("approveStatusNo", orderStatus);
                        }
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        data = ifsService.getSpotMinePage(params, 2);
                    }
                }

                // 表头
                excel = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
                CellStyle center = excel.getDefaultCenterStrCellStyle();
                int sheet = 0;
                int row = 0;
                // 表格sheet名称
                excel.getWb().createSheet("外汇即期报表");
                // 设置表头字段名
                excel.writeStr(sheet, row, "A", center, "审批单号");
                excel.writeStr(sheet, row, "B", center, "opics交易号");
                excel.writeStr(sheet, row, "C", center, "交易来源");
                excel.writeStr(sheet, row, "D", center, "货币对");
                excel.writeStr(sheet, row, "E", center, "交易方向");
                excel.writeStr(sheet, row, "F", center, "投资组合");
                excel.writeStr(sheet, row, "G", center, "交易金额");
                excel.writeStr(sheet, row, "H", center, "成交汇率");
                excel.writeStr(sheet, row, "I", center, "成交单编号");
                excel.writeStr(sheet, row, "J", center, "审批状态");
                excel.writeStr(sheet, row, "K", center, "对方机构");
                excel.writeStr(sheet, row, "L", center, "起息日");
                excel.writeStr(sheet, row, "M", center, "交易日期");
                excel.writeStr(sheet, row, "N", center, "审批发起人");
                excel.writeStr(sheet, row, "O", center, "审批发起机构");
                excel.writeStr(sheet, row, "P", center, "审批发起时间");
                // 设置列宽
                excel.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(8, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(9, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(10, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(11, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(12, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(13, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(14, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(15, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(16, 20 * 256);
                IfsCfetsfxSpt bean = new IfsCfetsfxSpt();
                Map<String, String> upmap = new HashMap<String, String>();
                Map<String, String> upmap2 = new HashMap<String, String>();
                Map<String, String> upmap3 = new HashMap<String, String>();
                for (int i = 0; i < data.size(); i++) {
                    bean = data.get(i);
                    row++;

                    upmap.put("dictId", "trading");
                    upmap.put("dictKey", bean.getBuyDirection());
                    String buyDirection = ifsService.queryDictValue(upmap); // 交易方向

                    upmap2.put("dictId", "TradeSource");
                    upmap2.put("dictKey", bean.getDealSource());
                    String dealSource = ifsService.queryDictValue(upmap2); // 交易来源

                    upmap3.put("dictId", "ApproveStatus");
                    upmap3.put("dictKey", bean.getApproveStatus());
                    String approveStatus = ifsService.queryDictValue(upmap3); // 审批状态
                    DecimalFormat df = new DecimalFormat("#,##0.00000000");//利率格式化
                    // 插入数据
                    excel.writeStr(sheet, row, "A", center, bean.getTicketId());
                    excel.writeStr(sheet, row, "B", center, bean.getDealNo());
                    excel.writeStr(sheet, row, "C", center, dealSource);
                    excel.writeStr(sheet, row, "D", center, bean.getCurrencyPair());
                    excel.writeStr(sheet, row, "E", center, buyDirection);
                    excel.writeStr(sheet, row, "F", center, bean.getPort());
                    excel.writeStr(sheet, row, "G", center, bean.getBuyAmount() == null ? "" : bean.getBuyAmount().toString());
                    excel.writeStr(sheet, row, "H", center, df.format(Double.valueOf(bean.getExchangeRate())));
                    excel.writeStr(sheet, row, "I", center, bean.getContractId());
                    if (!StringUtil.isNullOrEmpty(map.get("deal"))) {
                        excel.writeStr(sheet, row, "J", center, approveStatus);
                    } else {
                        excel.writeStr(sheet, row, "J", center, bean.getTaskName());
                    }
                    excel.writeStr(sheet, row, "K", center, bean.getCounterpartyInstId());
                    excel.writeStr(sheet, row, "L", center, bean.getValueDate());
                    excel.writeStr(sheet, row, "M", center, bean.getForDate());
                    excel.writeStr(sheet, row, "N", center, bean.getSponsor());
                    excel.writeStr(sheet, row, "O", center, bean.getSponInst());
                    excel.writeStr(sheet, row, "P", center, bean.getaDate());
                }
            } else if ("黄金即期".equals(prdName)) {
                /* 先查数据，再设置表格属性以及内容 */
                Page<IfsCfetsmetalGold> data = new Page<IfsCfetsmetalGold>();
                String approveType = map.get("approveType");
                Map<String, Object> params = new HashMap<String, Object>();
                params.put("contractId", map.get("contractId")); // 成交单编号
                params.put("approveStatus", map.get("approveStatus")); // 审批状态
                //params.put("port", map.get("port")); // 投资组合
                params.put("branchId", branchId);
                params.put("pageNumber", "1");
                params.put("pageSize", "999");
                params.put("startDay", startDay);
                params.put("endDay", endDay);
                params.put("ticketId", ticketId);
                params.put("sponInst", sponInst);
                params.put("dealNo", map.get("dealNo"));
                if (map.get("startDate") != null && map.get("startDate").length() >= 10) {
                    params.put("startDate", map.get("startDate").substring(0, 10));
                } else {
                    params.put("startDate", map.get("startDate"));
                }
                if (map.get("endDate") != null && map.get("endDate").length() >= 10) {
                    params.put("endDate", map.get("endDate").substring(0, 10));
                } else {
                    params.put("endDate", map.get("endDate"));
                }

                if (!StringUtil.isNullOrEmpty(map.get("deal"))) { // 总交易查询导出
                    params.put("isActive", DictConstants.YesNo.YES);
                    params.put("userId", SlSessionHelper.getUserId());
                    params.put("type", "spt");
                    data = qryTotalDealService.searchTotalDealHJ(params, 3);
                } else { // 正式交易台查询导出
                    if ("mine".equals(approveType)) {
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        params.put("type", "spt");
                        data = ifsService.getMetalGoldMinePage(params, 3);
                    } else if ("approve".equals(approveType)) {
                        String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + ","
                                + DictConstants.ApproveStatus.VerifyRefuse + "," + DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
                        String[] orderStatus = approveStatus.split(",");
                        params.put("approveStatusNo", orderStatus);
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        params.put("type", "spt");
                        data = ifsService.getMetalGoldMinePage(params, 1);
                    } else {
                        String approveStatusNo = ParameterUtil.getString(params, "approveStatus", "");
                        if (StringUtil.isNullOrEmpty(approveStatusNo)) {
                            approveStatusNo = DictConstants.ApproveStatus.ApprovedPass;
                            approveStatusNo += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
                            approveStatusNo += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
                            approveStatusNo += "," + DictConstants.ApproveStatus.Verified;
                            approveStatusNo += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
                            approveStatusNo += "," + DictConstants.ApproveStatus.Approving;
                            String[] orderStatus = approveStatusNo.split(",");
                            params.put("approveStatusNo", orderStatus);
                        }
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        params.put("type", "spt");
                        data = ifsService.getMetalGoldMinePage(params, 2);
                    }
                }

                // 表头
                excel = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
                CellStyle center = excel.getDefaultCenterStrCellStyle();
                int sheet = 0;
                int row = 0;
                // 表格sheet名称
                excel.getWb().createSheet("黄金即期报表");
                // 设置表头字段名
                excel.writeStr(sheet, row, "A", center, "审批单编号");
                excel.writeStr(sheet, row, "B", center, "成交单编号");
                excel.writeStr(sheet, row, "C", center, "opics交易号");
                excel.writeStr(sheet, row, "D", center, "交易日期");
                excel.writeStr(sheet, row, "E", center, "交易来源");
                excel.writeStr(sheet, row, "F", center, "货币对");
                excel.writeStr(sheet, row, "G", center, "数量");
                excel.writeStr(sheet, row, "H", center, "价格");
                excel.writeStr(sheet, row, "I", center, "金额");
                excel.writeStr(sheet, row, "J", center, "交易方向");
                excel.writeStr(sheet, row, "K", center, "起息日");
                excel.writeStr(sheet, row, "L", center, "审批状态");
                excel.writeStr(sheet, row, "M", center, "本方机构");
                excel.writeStr(sheet, row, "N", center, "对手方机构");
                excel.writeStr(sheet, row, "O", center, "审批发起人");
                excel.writeStr(sheet, row, "P", center, "审批发起机构");
                excel.writeStr(sheet, row, "Q", center, "审批发起时间");
                // 设置列宽
                excel.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(8, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(9, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(10, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(11, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(12, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(13, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(14, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(15, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(16, 20 * 256);
                IfsCfetsmetalGold bean = new IfsCfetsmetalGold();
                Map<String, String> upmap = new HashMap<String, String>();
                Map<String, String> upmap2 = new HashMap<String, String>();
                Map<String, String> upmap3 = new HashMap<String, String>();
                for (int i = 0; i < data.size(); i++) {
                    bean = data.get(i);
                    row++;

                    upmap.put("dictId", "trading");
                    upmap.put("dictKey", bean.getDirection1());
                    String direction1 = ifsService.queryDictValue(upmap); // 交易方向

                    upmap2.put("dictId", "TradeSource");
                    upmap2.put("dictKey", bean.getDealSource());
                    String dealSource = ifsService.queryDictValue(upmap2); // 交易来源

                    upmap3.put("dictId", "ApproveStatus");
                    upmap3.put("dictKey", bean.getApproveStatus());
                    String approveStatus = ifsService.queryDictValue(upmap3); // 审批状态

                    // 插入数据
                    excel.writeStr(sheet, row, "A", center, bean.getTicketId());
                    excel.writeStr(sheet, row, "B", center, bean.getContractId());
                    excel.writeStr(sheet, row, "C", center, bean.getDealNo());
                    excel.writeStr(sheet, row, "D", center, bean.getForDate());
                    excel.writeStr(sheet, row, "E", center, dealSource);
                    excel.writeStr(sheet, row, "F", center, bean.getCurrencyPair());
                    excel.writeStr(sheet, row, "G", center, bean.getQuantity());
                    excel.writeStr(sheet, row, "H", center, bean.getPrix());
                    excel.writeStr(sheet, row, "I", center, bean.getAmount1());
                    excel.writeStr(sheet, row, "J", center, direction1);
                    excel.writeStr(sheet, row, "K", center, bean.getValueDate1());
                    if (!StringUtil.isNullOrEmpty(map.get("deal"))) {
                        excel.writeStr(sheet, row, "L", center, approveStatus);
                    } else {
                        excel.writeStr(sheet, row, "L", center, bean.getTaskName());
                    }
                    excel.writeStr(sheet, row, "M", center, bean.getInstId());
                    excel.writeStr(sheet, row, "N", center, bean.getCounterpartyInstId());
                    excel.writeStr(sheet, row, "O", center, bean.getSponsor());
                    excel.writeStr(sheet, row, "P", center, bean.getSponInst());
                    excel.writeStr(sheet, row, "Q", center, bean.getaDate());
                }
            } else if ("黄金远期".equals(prdName)) {
                /* 先查数据，再设置表格属性以及内容 */
                Page<IfsCfetsmetalGold> data = new Page<IfsCfetsmetalGold>();
                String approveType = map.get("approveType");
                Map<String, Object> params = new HashMap<String, Object>();
                params.put("contractId", map.get("contractId")); // 成交单编号
                params.put("approveStatus", map.get("approveStatus")); // 审批状态
                //params.put("port", map.get("port")); // 投资组合
                params.put("branchId", branchId);
                params.put("pageNumber", "1");
                params.put("pageSize", "999");
                params.put("startDay", startDay);
                params.put("endDay", endDay);
                params.put("ticketId", ticketId);
                params.put("sponInst", sponInst);
                params.put("dealNo", map.get("dealNo"));
                if (map.get("startDate") != null && map.get("startDate").length() >= 10) {
                    params.put("startDate", map.get("startDate").substring(0, 10));
                } else {
                    params.put("startDate", map.get("startDate"));
                }
                if (map.get("endDate") != null && map.get("endDate").length() >= 10) {
                    params.put("endDate", map.get("endDate").substring(0, 10));
                } else {
                    params.put("endDate", map.get("endDate"));
                }

                if (!StringUtil.isNullOrEmpty(map.get("deal"))) { // 总交易查询导出
                    params.put("isActive", DictConstants.YesNo.YES);
                    params.put("userId", SlSessionHelper.getUserId());
                    params.put("type", "fwd");
                    data = qryTotalDealService.searchTotalDealHJ(params, 3);
                } else { // 正式交易台查询导出
                    if ("mine".equals(approveType)) {
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        params.put("type", "fwd");
                        data = ifsService.getMetalGoldMinePage(params, 3);
                    } else if ("approve".equals(approveType)) {
                        String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + ","
                                + DictConstants.ApproveStatus.VerifyRefuse + "," + DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
                        String[] orderStatus = approveStatus.split(",");
                        params.put("approveStatusNo", orderStatus);
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        params.put("type", "fwd");
                        data = ifsService.getMetalGoldMinePage(params, 1);
                    } else {
                        String approveStatusNo = ParameterUtil.getString(params, "approveStatus", "");
                        if (StringUtil.isNullOrEmpty(approveStatusNo)) {
                            approveStatusNo = DictConstants.ApproveStatus.ApprovedPass;
                            approveStatusNo += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
                            approveStatusNo += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
                            approveStatusNo += "," + DictConstants.ApproveStatus.Verified;
                            approveStatusNo += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
                            approveStatusNo += "," + DictConstants.ApproveStatus.Approving;
                            String[] orderStatus = approveStatusNo.split(",");
                            params.put("approveStatusNo", orderStatus);
                        }
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        params.put("type", "fwd");
                        data = ifsService.getMetalGoldMinePage(params, 2);
                    }
                }

                // 表头
                excel = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
                CellStyle center = excel.getDefaultCenterStrCellStyle();
                int sheet = 0;
                int row = 0;
                // 表格sheet名称
                excel.getWb().createSheet("黄金远期报表");
                // 设置表头字段名
                excel.writeStr(sheet, row, "A", center, "审批单编号");
                excel.writeStr(sheet, row, "B", center, "成交单编号");
                excel.writeStr(sheet, row, "C", center, "opics交易号");
                excel.writeStr(sheet, row, "D", center, "交易日期");
                excel.writeStr(sheet, row, "E", center, "交易来源");
                excel.writeStr(sheet, row, "F", center, "货币对");
                excel.writeStr(sheet, row, "G", center, "数量");
                excel.writeStr(sheet, row, "H", center, "价格");
                excel.writeStr(sheet, row, "I", center, "金额");
                excel.writeStr(sheet, row, "J", center, "交易方向");
                excel.writeStr(sheet, row, "K", center, "起息日");
                excel.writeStr(sheet, row, "L", center, "审批状态");
                excel.writeStr(sheet, row, "M", center, "本方机构");
                excel.writeStr(sheet, row, "N", center, "对手方机构");
                excel.writeStr(sheet, row, "O", center, "审批发起人");
                excel.writeStr(sheet, row, "P", center, "审批发起机构");
                excel.writeStr(sheet, row, "Q", center, "审批发起时间");
                // 设置列宽
                excel.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(8, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(9, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(10, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(11, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(12, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(13, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(14, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(15, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(16, 20 * 256);
                IfsCfetsmetalGold bean = new IfsCfetsmetalGold();
                Map<String, String> upmap = new HashMap<String, String>();
                Map<String, String> upmap2 = new HashMap<String, String>();
                Map<String, String> upmap3 = new HashMap<String, String>();
                for (int i = 0; i < data.size(); i++) {
                    bean = data.get(i);
                    row++;

                    upmap.put("dictId", "trading");
                    upmap.put("dictKey", bean.getDirection1());
                    String direction1 = ifsService.queryDictValue(upmap); // 交易方向

                    upmap2.put("dictId", "TradeSource");
                    upmap2.put("dictKey", bean.getDealSource());
                    String dealSource = ifsService.queryDictValue(upmap2); // 交易来源

                    upmap3.put("dictId", "ApproveStatus");
                    upmap3.put("dictKey", bean.getApproveStatus());
                    String approveStatus = ifsService.queryDictValue(upmap3); // 审批状态

                    // 插入数据
                    excel.writeStr(sheet, row, "A", center, bean.getTicketId());
                    excel.writeStr(sheet, row, "B", center, bean.getContractId());
                    excel.writeStr(sheet, row, "C", center, bean.getDealNo());
                    excel.writeStr(sheet, row, "D", center, bean.getForDate());
                    excel.writeStr(sheet, row, "E", center, dealSource);
                    excel.writeStr(sheet, row, "F", center, bean.getCurrencyPair());
                    excel.writeStr(sheet, row, "G", center, bean.getQuantity());
                    excel.writeStr(sheet, row, "H", center, bean.getPrix());
                    excel.writeStr(sheet, row, "I", center, bean.getAmount1());
                    excel.writeStr(sheet, row, "J", center, direction1);
                    excel.writeStr(sheet, row, "K", center, bean.getValueDate1());
                    if (!StringUtil.isNullOrEmpty(map.get("deal"))) {
                        excel.writeStr(sheet, row, "L", center, approveStatus);
                    } else {
                        excel.writeStr(sheet, row, "L", center, bean.getTaskName());
                    }
                    excel.writeStr(sheet, row, "M", center, bean.getInstId());
                    excel.writeStr(sheet, row, "N", center, bean.getCounterpartyInstId());
                    excel.writeStr(sheet, row, "O", center, bean.getSponsor());
                    excel.writeStr(sheet, row, "P", center, bean.getSponInst());
                    excel.writeStr(sheet, row, "Q", center, bean.getaDate());
                }
            } else if ("黄金掉期".equals(prdName)) {
                /* 先查数据，再设置表格属性以及内容 */
                Page<IfsCfetsmetalGold> data = new Page<IfsCfetsmetalGold>();
                String approveType = map.get("approveType");
                Map<String, Object> params = new HashMap<String, Object>();
                params.put("contractId", map.get("contractId")); // 成交单编号
                params.put("approveStatus", map.get("approveStatus")); // 审批状态
                //params.put("port", map.get("port")); // 投资组合
                params.put("branchId", branchId);
                params.put("pageNumber", "1");
                params.put("pageSize", "999");
                params.put("startDay", startDay);
                params.put("endDay", endDay);
                params.put("ticketId", ticketId);
                params.put("sponInst", sponInst);
                params.put("dealNo", map.get("dealNo"));
                if (map.get("startDate") != null && map.get("startDate").length() >= 10) {
                    params.put("startDate", map.get("startDate").substring(0, 10));
                } else {
                    params.put("startDate", map.get("startDate"));
                }
                if (map.get("endDate") != null && map.get("endDate").length() >= 10) {
                    params.put("endDate", map.get("endDate").substring(0, 10));
                } else {
                    params.put("endDate", map.get("endDate"));
                }

                if (!StringUtil.isNullOrEmpty(map.get("deal"))) { // 总交易查询导出
                    params.put("isActive", DictConstants.YesNo.YES);
                    params.put("userId", SlSessionHelper.getUserId());
                    params.put("type", "swap");
                    data = qryTotalDealService.searchTotalDealHJ(params, 3);
                } else { // 正式交易台查询导出
                    if ("mine".equals(approveType)) {
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        params.put("type", "swap");
                        data = ifsService.getMetalGoldMinePage(params, 3);
                    } else if ("approve".equals(approveType)) {
                        String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + ","
                                + DictConstants.ApproveStatus.VerifyRefuse + "," + DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
                        String[] orderStatus = approveStatus.split(",");
                        params.put("approveStatusNo", orderStatus);
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        params.put("type", "swap");
                        data = ifsService.getMetalGoldMinePage(params, 1);
                    } else {
                        String approveStatusNo = ParameterUtil.getString(params, "approveStatus", "");
                        if (StringUtil.isNullOrEmpty(approveStatusNo)) {
                            approveStatusNo = DictConstants.ApproveStatus.ApprovedPass;
                            approveStatusNo += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
                            approveStatusNo += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
                            approveStatusNo += "," + DictConstants.ApproveStatus.Verified;
                            approveStatusNo += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
                            approveStatusNo += "," + DictConstants.ApproveStatus.Approving;
                            String[] orderStatus = approveStatusNo.split(",");
                            params.put("approveStatusNo", orderStatus);
                        }
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        params.put("type", "swap");
                        data = ifsService.getMetalGoldMinePage(params, 2);
                    }
                }

                // 表头
                excel = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
                CellStyle center = excel.getDefaultCenterStrCellStyle();
                int sheet = 0;
                int row = 0;
                // 表格sheet名称
                excel.getWb().createSheet("黄金掉期报表");
                // 设置表头字段名
                excel.writeStr(sheet, row, "A", center, "审批单编号");
                excel.writeStr(sheet, row, "B", center, "成交单编号");
                excel.writeStr(sheet, row, "C", center, "opics交易号");
                excel.writeStr(sheet, row, "D", center, "远端交易号");
                excel.writeStr(sheet, row, "E", center, "交易日期");
                excel.writeStr(sheet, row, "F", center, "交易来源");
                excel.writeStr(sheet, row, "G", center, "货币对");
                excel.writeStr(sheet, row, "H", center, "数量");
                excel.writeStr(sheet, row, "I", center, "交易方向");
                excel.writeStr(sheet, row, "J", center, "近端价格");
                excel.writeStr(sheet, row, "K", center, "近端金额(即)");
                excel.writeStr(sheet, row, "L", center, "起息日(即)");
                excel.writeStr(sheet, row, "M", center, "远端价格");
                excel.writeStr(sheet, row, "N", center, "远端金额(远)");
                excel.writeStr(sheet, row, "O", center, "起息日(远)");
                excel.writeStr(sheet, row, "P", center, "审批状态");
                excel.writeStr(sheet, row, "Q", center, "本方机构");
                excel.writeStr(sheet, row, "R", center, "对手方机构");
                excel.writeStr(sheet, row, "S", center, "审批发起人");
                excel.writeStr(sheet, row, "T", center, "审批发起机构");
                excel.writeStr(sheet, row, "U", center, "审批发起时间");
                // 设置列宽
                excel.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(8, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(9, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(10, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(11, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(12, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(13, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(14, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(15, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(16, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(17, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(18, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(19, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(20, 20 * 256);
                IfsCfetsmetalGold bean = new IfsCfetsmetalGold();
                Map<String, String> upmap = new HashMap<String, String>();
                Map<String, String> upmap2 = new HashMap<String, String>();
                Map<String, String> upmap3 = new HashMap<String, String>();
                for (int i = 0; i < data.size(); i++) {
                    bean = data.get(i);
                    row++;

                    upmap.put("dictId", "trading");
                    upmap.put("dictKey", bean.getDirection1());
                    String direction1 = ifsService.queryDictValue(upmap); // 交易方向

                    upmap2.put("dictId", "TradeSource");
                    upmap2.put("dictKey", bean.getDealSource());
                    String dealSource = ifsService.queryDictValue(upmap2); // 交易来源

                    upmap3.put("dictId", "ApproveStatus");
                    upmap3.put("dictKey", bean.getApproveStatus());
                    String approveStatus = ifsService.queryDictValue(upmap3); // 审批状态

                    // 插入数据
                    excel.writeStr(sheet, row, "A", center, bean.getTicketId());
                    excel.writeStr(sheet, row, "B", center, bean.getContractId());
                    excel.writeStr(sheet, row, "C", center, bean.getDealNo());
                    excel.writeStr(sheet, row, "D", center, bean.getSwapDealno());
                    excel.writeStr(sheet, row, "E", center, bean.getForDate());
                    excel.writeStr(sheet, row, "F", center, dealSource);
                    excel.writeStr(sheet, row, "G", center, bean.getCurrencyPair());
                    excel.writeStr(sheet, row, "H", center, bean.getQuantity());
                    excel.writeStr(sheet, row, "I", center, direction1);
                    excel.writeStr(sheet, row, "J", center, bean.getPrix());
                    excel.writeStr(sheet, row, "K", center, bean.getAmount1());
                    excel.writeStr(sheet, row, "L", center, bean.getValueDate1());
                    excel.writeStr(sheet, row, "M", center, bean.getTenor2());
                    excel.writeStr(sheet, row, "N", center, bean.getAmount2());
                    excel.writeStr(sheet, row, "O", center, bean.getValueDate2());
                    if (!StringUtil.isNullOrEmpty(map.get("deal"))) {
                        excel.writeStr(sheet, row, "P", center, approveStatus);
                    } else {
                        excel.writeStr(sheet, row, "P", center, bean.getTaskName());
                    }
                    excel.writeStr(sheet, row, "Q", center, bean.getInstId());
                    excel.writeStr(sheet, row, "R", center, bean.getCounterpartyInstId());
                    excel.writeStr(sheet, row, "S", center, bean.getSponsor());
                    excel.writeStr(sheet, row, "T", center, bean.getSponInst());
                    excel.writeStr(sheet, row, "U", center, bean.getaDate());
                }
            } else if ("债券远期".equals(prdName)) {
                /* 先查数据，再设置表格属性以及内容 */
                Page<IfsCfetsrmbBondfwd> data = new Page<IfsCfetsrmbBondfwd>();
                String approveType = map.get("approveType");
                Map<String, Object> params = new HashMap<String, Object>();
                params.put("contractId", map.get("contractId")); // 成交单编号
                params.put("forDate", map.get("forDate")); // 成交日期
                params.put("approveStatus", map.get("approveStatus")); // 审批状态
                params.put("branchId", branchId);
                params.put("pageNumber", "1");
                params.put("pageSize", "999");
                params.put("startDay", startDay);
                params.put("endDay", endDay);
                params.put("ticketId", ticketId);
                params.put("sponInst", sponInst);
                if (!StringUtil.isNullOrEmpty(map.get("deal"))) { // 总交易查询导出
                    params.put("isActive", DictConstants.YesNo.YES);
                    params.put("userId", SlSessionHelper.getUserId());
                    data = qryTotalDealService.searchTotalDealZQYQ(params, 3);
                } else { // 正式交易台查询导出
                    if ("mine".equals(approveType)) {
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        data = ifsCfetsrmbBondfwdService.searchICBFPage(params, 3);
                    } else if ("approve".equals(approveType)) {
                        String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + ","
                                + DictConstants.ApproveStatus.VerifyRefuse + "," + DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
                        String[] orderStatus = approveStatus.split(",");
                        params.put("approveStatusNo", orderStatus);
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        data = ifsCfetsrmbBondfwdService.searchICBFPage(params, 1);
                    } else {
                        String approveStatus = ParameterUtil.getString(params, "approveStatus", "");
                        if (StringUtil.isNullOrEmpty(approveStatus)) {
                            approveStatus = DictConstants.ApproveStatus.ApprovedPass;
                            approveStatus += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
                            approveStatus += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
                            approveStatus += "," + DictConstants.ApproveStatus.Verified;
                            approveStatus += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
                            // 20180502修改bug:已审批列表中增加"审批中"
                            approveStatus += "," + DictConstants.ApproveStatus.Approving;
                            String[] orderStatus = approveStatus.split(",");
                            params.put("approveStatusNo", orderStatus);
                        }
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        data = ifsCfetsrmbBondfwdService.searchICBFPage(params, 2);
                    }
                }
                // 表头
                excel = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
                CellStyle center = excel.getDefaultCenterStrCellStyle();
                int sheet = 0;
                int row = 0;
                // 表格sheet名称
                excel.getWb().createSheet("债券远期报表");
                // 设置表头字段名
                excel.writeStr(sheet, row, "A", center, "审批单号");
                excel.writeStr(sheet, row, "B", center, "成交单号");
                excel.writeStr(sheet, row, "C", center, "opics单号");
                excel.writeStr(sheet, row, "D", center, "债券简称");
                excel.writeStr(sheet, row, "E", center, "投资类型");
                excel.writeStr(sheet, row, "F", center, "本方方向");
                excel.writeStr(sheet, row, "G", center, "对手方机构名称");
                excel.writeStr(sheet, row, "H", center, "交易日");
                excel.writeStr(sheet, row, "I", center, "价格");
                excel.writeStr(sheet, row, "J", center, "金额(元");
                excel.writeStr(sheet, row, "K", center, "结算日");
                excel.writeStr(sheet, row, "L", center, "结算方式");
                excel.writeStr(sheet, row, "M", center, "买入方机构");
                excel.writeStr(sheet, row, "N", center, "审批状态");
                excel.writeStr(sheet, row, "O", center, "审批发起人");
                excel.writeStr(sheet, row, "P", center, "审批发起机构");
                excel.writeStr(sheet, row, "Q", center, "审批发起时间");

                // 设置列宽
                excel.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(8, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(9, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(10, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(11, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(12, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(13, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(14, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(15, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(16, 20 * 256);

                IfsCfetsrmbBondfwd bean = new IfsCfetsrmbBondfwd();
                Map<String, String> upmap = new HashMap<String, String>();
                Map<String, String> upmap2 = new HashMap<String, String>();
                Map<String, String> approveMap = new HashMap<String, String>();
                for (int i = 0; i < data.size(); i++) {
                    bean = data.get(i);
                    row++;
                    upmap.put("dictId", "trading");
                    upmap.put("dictKey", bean.getMyDir());
                    String myDir = ifsService.queryDictValue(upmap); // 取数据字典中的value值

                    upmap2.put("dictId", "SettlementMethod");
                    upmap2.put("dictKey", bean.getSettlementMethod());
                    String settlementMethod = ifsService.queryDictValue(upmap2); // 取数据字典中的value值

                    approveMap.put("dictId", "ApproveStatus");
                    approveMap.put("dictKey", bean.getApproveStatus());
                    String approveStatus = ifsService.queryDictValue(approveMap); // 审批状态

                    // 插入数据
                    excel.writeStr(sheet, row, "A", center, bean.getTicketId());
                    excel.writeStr(sheet, row, "B", center, bean.getContractId());
                    excel.writeStr(sheet, row, "C", center, bean.getDealNo());
                    excel.writeStr(sheet, row, "D", center, bean.getBondName());
                    excel.writeStr(sheet, row, "E", center, bean.getInvtype());
                    excel.writeStr(sheet, row, "F", center, myDir); // 本方方向
                    excel.writeStr(sheet, row, "G", center, bean.getSellInst());
                    excel.writeStr(sheet, row, "H", center, bean.getForDate());
                    excel.writeStr(sheet, row, "I", center, bean.getPrice() == null ? "" : bean.getPrice().toString());
                    excel.writeStr(sheet, row, "J", center, bean.getSettlementAmount() == null ? "" : bean.getSettlementAmount().toString());
                    excel.writeStr(sheet, row, "K", center, bean.getSettlementDate());
                    excel.writeStr(sheet, row, "L", center, settlementMethod); // 结算方式
                    excel.writeStr(sheet, row, "M", center, bean.getBuyInst());
                    if (!StringUtil.isNullOrEmpty(map.get("deal"))) {
                        excel.writeStr(sheet, row, "N", center, approveStatus);
                    } else {
                        excel.writeStr(sheet, row, "N", center, bean.getTaskName());
                    }
                    excel.writeStr(sheet, row, "O", center, bean.getSponsor());
                    excel.writeStr(sheet, row, "P", center, bean.getSponInst());
                    excel.writeStr(sheet, row, "Q", center, bean.getaDate());
                }
            } else if ("基金申购".equals(prdName)) {
                /* 先查数据，再设置表格属性以及内容 */
                Page<CFtAfp> data = new Page<CFtAfp>();
                String approveType = map.get("approveType");
                Map<String, Object> params = new HashMap<String, Object>();
//				params.put("contractId", map.get("contractId")); // 成交单编号
//				params.put("forDate", map.get("forDate")); // 成交日期
                params.put("approveStatus", map.get("approveStatus")); // 审批状态
                params.put("branchId", branchId);
                params.put("pageNumber", "1");
                params.put("pageSize", "999");
//				params.put("startDay", startDay);
//				params.put("endDay", endDay);
                params.put("dealNo", ticketId);//审批单号
                params.put("sponInst", sponInst);
                if (!StringUtil.isNullOrEmpty(map.get("deal"))) { // 总交易查询导出
                    params.put("isActive", DictConstants.YesNo.YES);
                    params.put("userId", SlSessionHelper.getUserId());
                    data = qryTotalDealService.searchTotalDealJJSGANDFH(params, 3);
                } else { // 正式交易台查询导出
                    if ("mine".equals(approveType)) {
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        data = cFtAfpService.searchCFtAfpList(params, 3);
                    } else if ("approve".equals(approveType)) {
                        String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + ","
                                + DictConstants.ApproveStatus.VerifyRefuse + "," + DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
                        String[] orderStatus = approveStatus.split(",");
                        params.put("approveStatusNo", orderStatus);
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        data = cFtAfpService.searchCFtAfpList(params, 1);
                    } else {
                        String approveStatus = ParameterUtil.getString(params, "approveStatus", "");
                        if (StringUtil.isNullOrEmpty(approveStatus)) {
                            approveStatus = DictConstants.ApproveStatus.ApprovedPass;
                            approveStatus += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
                            approveStatus += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
                            approveStatus += "," + DictConstants.ApproveStatus.Verified;
                            approveStatus += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
                            // 20180502修改bug:已审批列表中增加"审批中"
                            approveStatus += "," + DictConstants.ApproveStatus.Approving;
                            String[] orderStatus = approveStatus.split(",");
                            params.put("approveStatusNo", orderStatus);
                        }
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        data = cFtAfpService.searchCFtAfpList(params, 2);
                    }
                }
                // 表头
                excel = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
                CellStyle center = excel.getDefaultCenterStrCellStyle();
                int sheet = 0;
                int row = 0;
                // 表格sheet名称
                excel.getWb().createSheet("基金申购报表");
                // 设置表头字段名
                excel.writeStr(sheet, row, "A", center, "交易编号");
                excel.writeStr(sheet, row, "B", center, "基金全称");
                excel.writeStr(sheet, row, "C", center, "基金管理人名称");
                excel.writeStr(sheet, row, "D", center, "基金经理");
                excel.writeStr(sheet, row, "E", center, "基金币种");
                excel.writeStr(sheet, row, "F", center, "申购份额(份)");
                excel.writeStr(sheet, row, "G", center, "申购基金(元)");
                excel.writeStr(sheet, row, "F", center, "单位净值(元)");
                excel.writeStr(sheet, row, "I", center, "交易日期");
                excel.writeStr(sheet, row, "J", center, "会计类型");
                excel.writeStr(sheet, row, "K", center, "审批状态");
                excel.writeStr(sheet, row, "L", center, "审批发起人");
                excel.writeStr(sheet, row, "M", center, "审批发起机构");
                excel.writeStr(sheet, row, "N", center, "审批发起时间");
                // 设置列宽
                excel.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(1, 30 * 256);
                excel.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(8, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(9, 35 * 256);
                excel.getSheetAt(sheet).setColumnWidth(10, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(11, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(12, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(13, 20 * 256);

                CFtAfp bean = new CFtAfp();
                Map<String, String> upmap = new HashMap<String, String>();
                Map<String, String> upmap2 = new HashMap<String, String>();
                Map<String, String> approveMap = new HashMap<String, String>();
                for (int i = 0; i < data.size(); i++) {
                    bean = data.get(i);
                    row++;

                    upmap.put("dictId", "Currency");
                    upmap.put("dictKey", bean.getCcy());
                    String ccy = ifsService.queryDictValue(upmap); // 取数据字典中的value值

                    upmap2.put("dictId", "AccThreeSubject");
                    upmap2.put("dictKey", bean.getInvType());
                    String invType = ifsService.queryDictValue(upmap2); // 取数据字典中的value值

                    approveMap.put("dictId", "ApproveStatus");
                    approveMap.put("dictKey", bean.getApproveStatus());
                    String approveStatus = ifsService.queryDictValue(approveMap); // 审批状态

                    // 插入数据
                    excel.writeStr(sheet, row, "A", center, bean.getDealNo());
                    excel.writeStr(sheet, row, "B", center, bean.getFundFullName());
                    excel.writeStr(sheet, row, "C", center, bean.getCname());
                    excel.writeStr(sheet, row, "D", center, bean.getContact());
                    excel.writeStr(sheet, row, "E", center, ccy);
                    excel.writeStr(sheet, row, "F", center, bean.getShareAmt() == null ? "" : bean.getShareAmt().toString());
                    excel.writeStr(sheet, row, "G", center, bean.getAmt() == null ? "" : bean.getAmt().toString());
                    excel.writeStr(sheet, row, "H", center, bean.getPrice() == null ? "" : bean.getPrice().toString());
                    excel.writeStr(sheet, row, "I", center, bean.getTdate());
                    excel.writeStr(sheet, row, "J", center, invType);
                    if (!StringUtil.isNullOrEmpty(map.get("deal"))) {
                        excel.writeStr(sheet, row, "K", center, approveStatus);
                    } else {
                        excel.writeStr(sheet, row, "K", center, bean.getTaskName());
                    }
                    excel.writeStr(sheet, row, "L", center, bean.getSponsor());
                    excel.writeStr(sheet, row, "M", center, bean.getSponInstName());
                    excel.writeStr(sheet, row, "N", center, bean.getaDate());

                }

            } else if ("基金申购确认".equals(prdName)) {
                /* 先查数据，再设置表格属性以及内容 */
                Page<CFtAfpConf> data = new Page<CFtAfpConf>();
                String approveType = map.get("approveType");
                Map<String, Object> params = new HashMap<String, Object>();
                params.put("approveStatus", map.get("approveStatus")); // 审批状态
                params.put("branchId", branchId);
                params.put("pageNumber", "1");
                params.put("pageSize", "999");
                params.put("dealNo", ticketId);//审批单号
                params.put("sponInst", sponInst);
                if (!StringUtil.isNullOrEmpty(map.get("deal"))) { // 总交易查询导出
                    params.put("isActive", DictConstants.YesNo.YES);
                    params.put("userId", SlSessionHelper.getUserId());
                    data = qryTotalDealService.searchTotalDealJJSGQR(params, 3);
                } else { // 正式交易台查询导出
                    if ("mine".equals(approveType)) {
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        data = cFtAfpConfService.searchCFtAfpConfList(params, 3);
                    } else if ("approve".equals(approveType)) {
                        String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + ","
                                + DictConstants.ApproveStatus.VerifyRefuse + "," + DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
                        String[] orderStatus = approveStatus.split(",");
                        params.put("approveStatusNo", orderStatus);
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        data = cFtAfpConfService.searchCFtAfpConfList(params, 1);
                    } else {
                        String approveStatus = ParameterUtil.getString(params, "approveStatus", "");
                        if (StringUtil.isNullOrEmpty(approveStatus)) {
                            approveStatus = DictConstants.ApproveStatus.ApprovedPass;
                            approveStatus += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
                            approveStatus += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
                            approveStatus += "," + DictConstants.ApproveStatus.Verified;
                            approveStatus += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
                            // 20180502修改bug:已审批列表中增加"审批中"
                            approveStatus += "," + DictConstants.ApproveStatus.Approving;
                            String[] orderStatus = approveStatus.split(",");
                            params.put("approveStatusNo", orderStatus);
                        }
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        data = cFtAfpConfService.searchCFtAfpConfList(params, 2);
                    }
                }
                // 表头
                excel = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
                CellStyle center = excel.getDefaultCenterStrCellStyle();
                int sheet = 0;
                int row = 0;
                // 表格sheet名称
                excel.getWb().createSheet("基金申购确认报表");
                // 设置表头字段名
                excel.writeStr(sheet, row, "A", center, "关联编号");
                excel.writeStr(sheet, row, "B", center, "交易编号");
                excel.writeStr(sheet, row, "C", center, "基金全称");
                excel.writeStr(sheet, row, "D", center, "基金管理人名称");
                excel.writeStr(sheet, row, "E", center, "基金经理");
                excel.writeStr(sheet, row, "F", center, "基金币种");
                excel.writeStr(sheet, row, "G", center, "申购份额(份)");
                excel.writeStr(sheet, row, "H", center, "申购基金(元)");
                excel.writeStr(sheet, row, "I", center, "单位净值(元)");
                excel.writeStr(sheet, row, "J", center, "手续费(元)");
                excel.writeStr(sheet, row, "K", center, "交易日期");
                excel.writeStr(sheet, row, "L", center, "会计类型");
                excel.writeStr(sheet, row, "M", center, "审批状态");
                excel.writeStr(sheet, row, "N", center, "审批发起人");
                excel.writeStr(sheet, row, "O", center, "审批发起机构");
                excel.writeStr(sheet, row, "P", center, "审批发起时间");
                // 设置列宽
                excel.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(2, 30 * 256);
                excel.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(8, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(9, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(10, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(11, 35 * 256);
                excel.getSheetAt(sheet).setColumnWidth(12, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(13, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(14, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(15, 20 * 256);
                CFtAfpConf bean = new CFtAfpConf();
                Map<String, String> upmap = new HashMap<String, String>();
                Map<String, String> upmap2 = new HashMap<String, String>();
                Map<String, String> approveMap = new HashMap<String, String>();
                for (int i = 0; i < data.size(); i++) {
                    bean = data.get(i);
                    row++;

                    upmap.put("dictId", "Currency");
                    upmap.put("dictKey", bean.getCcy());
                    String ccy = ifsService.queryDictValue(upmap); // 取数据字典中的value值

                    upmap2.put("dictId", "AccThreeSubject");
                    upmap2.put("dictKey", bean.getInvType());
                    String invType = ifsService.queryDictValue(upmap2); // 取数据字典中的value值

                    approveMap.put("dictId", "ApproveStatus");
                    approveMap.put("dictKey", bean.getApproveStatus());
                    String approveStatus = ifsService.queryDictValue(approveMap); // 审批状态

                    // 插入数据
                    excel.writeStr(sheet, row, "A", center, bean.getRefNo());
                    excel.writeStr(sheet, row, "B", center, bean.getDealNo());
                    excel.writeStr(sheet, row, "C", center, bean.getFundFullName());
                    excel.writeStr(sheet, row, "D", center, bean.getCname());
                    excel.writeStr(sheet, row, "E", center, bean.getContact());
                    excel.writeStr(sheet, row, "F", center, ccy);
                    excel.writeStr(sheet, row, "G", center, bean.getShareAmt() == null ? "" : bean.getShareAmt().toString());
                    excel.writeStr(sheet, row, "H", center, bean.getAmt() == null ? "" : bean.getAmt().toString());
                    excel.writeStr(sheet, row, "I", center, bean.getPrice() == null ? "" : bean.getPrice().toString());
                    excel.writeStr(sheet, row, "J", center, bean.getHandleAmt() == null ? "" : bean.getHandleAmt().toString());
                    excel.writeStr(sheet, row, "K", center, bean.getTdate());
                    excel.writeStr(sheet, row, "L", center, invType);
                    if (!StringUtil.isNullOrEmpty(map.get("deal"))) {
                        excel.writeStr(sheet, row, "M", center, approveStatus);
                    } else {
                        excel.writeStr(sheet, row, "M", center, bean.getTaskName());
                    }
                    excel.writeStr(sheet, row, "N", center, bean.getSponsor());
                    excel.writeStr(sheet, row, "O", center, bean.getSponInstName());
                    excel.writeStr(sheet, row, "P", center, bean.getaDate());
                }

            } else if ("基金赎回".equals(prdName)) {
                /* 先查数据，再设置表格属性以及内容 */
                Page<CFtRdp> data = new Page<CFtRdp>();
                String approveType = map.get("approveType");
                Map<String, Object> params = new HashMap<String, Object>();
                params.put("approveStatus", map.get("approveStatus")); // 审批状态
                params.put("branchId", branchId);
                params.put("pageNumber", "1");
                params.put("pageSize", "999");
                params.put("dealNo", ticketId);//审批单号
                params.put("sponInst", sponInst);
                if (!StringUtil.isNullOrEmpty(map.get("deal"))) { // 总交易查询导出
                    params.put("isActive", DictConstants.YesNo.YES);
                    params.put("userId", SlSessionHelper.getUserId());
                    data = qryTotalDealService.searchTotalDealJJSH(params, 3);
                } else { // 正式交易台查询导出
                    if ("mine".equals(approveType)) {
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        data = cFtRdpService.searchCFtRdpList(params, 3);
                    } else if ("approve".equals(approveType)) {
                        String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + ","
                                + DictConstants.ApproveStatus.VerifyRefuse + "," + DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
                        String[] orderStatus = approveStatus.split(",");
                        params.put("approveStatusNo", orderStatus);
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        data = cFtRdpService.searchCFtRdpList(params, 1);
                    } else {
                        String approveStatus = ParameterUtil.getString(params, "approveStatus", "");
                        if (StringUtil.isNullOrEmpty(approveStatus)) {
                            approveStatus = DictConstants.ApproveStatus.ApprovedPass;
                            approveStatus += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
                            approveStatus += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
                            approveStatus += "," + DictConstants.ApproveStatus.Verified;
                            approveStatus += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
                            // 20180502修改bug:已审批列表中增加"审批中"
                            approveStatus += "," + DictConstants.ApproveStatus.Approving;
                            String[] orderStatus = approveStatus.split(",");
                            params.put("approveStatusNo", orderStatus);
                        }
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        data = cFtRdpService.searchCFtRdpList(params, 2);
                    }
                }

                // 表头
                excel = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
                CellStyle center = excel.getDefaultCenterStrCellStyle();
                int sheet = 0;
                int row = 0;
                // 表格sheet名称
                excel.getWb().createSheet("基金赎回报表");
                // 设置表头字段名
                excel.writeStr(sheet, row, "A", center, "交易编号");
                excel.writeStr(sheet, row, "B", center, "基金全称");
                excel.writeStr(sheet, row, "C", center, "基金管理人名称");
                excel.writeStr(sheet, row, "D", center, "基金经理");
                excel.writeStr(sheet, row, "E", center, "基金币种");
                excel.writeStr(sheet, row, "F", center, "回款日期");
                excel.writeStr(sheet, row, "G", center, "赎回日期");
                excel.writeStr(sheet, row, "H", center, "赎回份额（份）");
                excel.writeStr(sheet, row, "I", center, "会计类型");
                excel.writeStr(sheet, row, "J", center, "审批状态");
                excel.writeStr(sheet, row, "K", center, "审批发起人");
                excel.writeStr(sheet, row, "L", center, "审批发起机构");
                excel.writeStr(sheet, row, "M", center, "审批发起时间");
                // 设置列宽
                excel.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(1, 30 * 256);
                excel.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(8, 35 * 256);
                excel.getSheetAt(sheet).setColumnWidth(9, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(10, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(11, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(12, 20 * 256);

                CFtRdp bean = new CFtRdp();
                Map<String, String> upmap = new HashMap<String, String>();
                Map<String, String> upmap2 = new HashMap<String, String>();
                Map<String, String> approveMap = new HashMap<String, String>();
                for (int i = 0; i < data.size(); i++) {
                    bean = data.get(i);
                    row++;

                    upmap.put("dictId", "Currency");
                    upmap.put("dictKey", bean.getCcy());
                    String ccy = ifsService.queryDictValue(upmap); // 取数据字典中的value值

                    upmap2.put("dictId", "AccThreeSubject");
                    upmap2.put("dictKey", bean.getInvType());
                    String invType = ifsService.queryDictValue(upmap2); // 取数据字典中的value值

                    approveMap.put("dictId", "ApproveStatus");
                    approveMap.put("dictKey", bean.getApproveStatus());
                    String approveStatus = ifsService.queryDictValue(approveMap); // 审批状态

                    // 插入数据
                    excel.writeStr(sheet, row, "A", center, bean.getDealNo());
                    excel.writeStr(sheet, row, "B", center, bean.getFundFullName());
                    excel.writeStr(sheet, row, "C", center, bean.getCname());
                    excel.writeStr(sheet, row, "D", center, bean.getContact());
                    excel.writeStr(sheet, row, "E", center, ccy);
                    excel.writeStr(sheet, row, "F", center, bean.getTdate());
                    excel.writeStr(sheet, row, "G", center, bean.getVdate());
                    excel.writeStr(sheet, row, "H", center, bean.getShareAmt() == null ? "" : bean.getShareAmt().toString());
                    excel.writeStr(sheet, row, "I", center, invType);
                    if (!StringUtil.isNullOrEmpty(map.get("deal"))) {
                        excel.writeStr(sheet, row, "J", center, approveStatus);
                    } else {
                        excel.writeStr(sheet, row, "J", center, bean.getTaskName());
                    }
                    excel.writeStr(sheet, row, "K", center, bean.getSponsor());
                    excel.writeStr(sheet, row, "L", center, bean.getSponInstName());
                    excel.writeStr(sheet, row, "M", center, bean.getaDate());
                }
            } else if ("基金赎回确认".equals(prdName)) {
                /* 先查数据，再设置表格属性以及内容 */
                Page<CFtRdpConf> data = new Page<CFtRdpConf>();
                String approveType = map.get("approveType");
                Map<String, Object> params = new HashMap<String, Object>();
                params.put("approveStatus", map.get("approveStatus")); // 审批状态
                params.put("branchId", branchId);
                params.put("pageNumber", "1");
                params.put("pageSize", "999");
                params.put("dealNo", ticketId);//审批单号
                params.put("sponInst", sponInst);
                if (!StringUtil.isNullOrEmpty(map.get("deal"))) { // 总交易查询导出
                    params.put("isActive", DictConstants.YesNo.YES);
                    params.put("userId", SlSessionHelper.getUserId());
                    data = qryTotalDealService.searchTotalDealJJSHQR(params, 3);
                } else { // 正式交易台查询导出
                    if ("mine".equals(approveType)) {
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        data = cFtRdpConfService.searchCFtRdpConfList(params, 3);
                    } else if ("approve".equals(approveType)) {
                        String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + ","
                                + DictConstants.ApproveStatus.VerifyRefuse + "," + DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
                        String[] orderStatus = approveStatus.split(",");
                        params.put("approveStatusNo", orderStatus);
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        data = cFtRdpConfService.searchCFtRdpConfList(params, 1);
                    } else {
                        String approveStatus = ParameterUtil.getString(params, "approveStatus", "");
                        if (StringUtil.isNullOrEmpty(approveStatus)) {
                            approveStatus = DictConstants.ApproveStatus.ApprovedPass;
                            approveStatus += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
                            approveStatus += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
                            approveStatus += "," + DictConstants.ApproveStatus.Verified;
                            approveStatus += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
                            // 20180502修改bug:已审批列表中增加"审批中"
                            approveStatus += "," + DictConstants.ApproveStatus.Approving;
                            String[] orderStatus = approveStatus.split(",");
                            params.put("approveStatusNo", orderStatus);
                        }
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        data = cFtRdpConfService.searchCFtRdpConfList(params, 2);
                    }
                }
                // 表头
                excel = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
                CellStyle center = excel.getDefaultCenterStrCellStyle();
                int sheet = 0;
                int row = 0;
                // 表格sheet名称
                excel.getWb().createSheet("基金赎回确认报表");
                // 设置表头字段名
                excel.writeStr(sheet, row, "A", center, "关联编号");
                excel.writeStr(sheet, row, "B", center, "交易编号");
                excel.writeStr(sheet, row, "C", center, "基金全称");
                excel.writeStr(sheet, row, "D", center, "基金管理人名称");
                excel.writeStr(sheet, row, "E", center, "基金经理");
                excel.writeStr(sheet, row, "F", center, "基金币种");
                excel.writeStr(sheet, row, "G", center, "回款日期");
                excel.writeStr(sheet, row, "H", center, "赎回日期");
                excel.writeStr(sheet, row, "I", center, "赎回份额(份)");
                excel.writeStr(sheet, row, "J", center, "赎回金额(元)");
                excel.writeStr(sheet, row, "K", center, "单位净值(元)");
                excel.writeStr(sheet, row, "L", center, "赎回红利(元)");
                excel.writeStr(sheet, row, "M", center, "手续费(元)");
                excel.writeStr(sheet, row, "N", center, "会计类型");
                excel.writeStr(sheet, row, "O", center, "审批状态");
                excel.writeStr(sheet, row, "P", center, "审批发起人");
                excel.writeStr(sheet, row, "Q", center, "审批发起机构");
                excel.writeStr(sheet, row, "R", center, "审批发起时间");
                // 设置列宽
                excel.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(2, 30 * 256);
                excel.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(8, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(9, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(10, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(11, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(12, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(13, 35 * 256);
                excel.getSheetAt(sheet).setColumnWidth(14, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(15, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(16, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(17, 20 * 256);

                CFtRdpConf bean = new CFtRdpConf();
                Map<String, String> upmap = new HashMap<String, String>();
                Map<String, String> upmap2 = new HashMap<String, String>();
                Map<String, String> approveMap = new HashMap<String, String>();
                for (int i = 0; i < data.size(); i++) {
                    bean = data.get(i);
                    row++;

                    upmap.put("dictId", "Currency");
                    upmap.put("dictKey", bean.getCcy());
                    String ccy = ifsService.queryDictValue(upmap); // 取数据字典中的value值

                    upmap2.put("dictId", "AccThreeSubject");
                    upmap2.put("dictKey", bean.getInvType());
                    String invType = ifsService.queryDictValue(upmap2); // 取数据字典中的value值

                    approveMap.put("dictId", "ApproveStatus");
                    approveMap.put("dictKey", bean.getApproveStatus());
                    String approveStatus = ifsService.queryDictValue(approveMap); // 审批状态

                    // 插入数据
                    excel.writeStr(sheet, row, "A", center, bean.getRefNo());
                    excel.writeStr(sheet, row, "B", center, bean.getDealNo());
                    excel.writeStr(sheet, row, "C", center, bean.getFundFullName());
                    excel.writeStr(sheet, row, "D", center, bean.getCname());
                    excel.writeStr(sheet, row, "E", center, bean.getContact());
                    excel.writeStr(sheet, row, "F", center, ccy);
                    excel.writeStr(sheet, row, "G", center, bean.getTdate());
                    excel.writeStr(sheet, row, "H", center, bean.getVdate());
                    excel.writeStr(sheet, row, "I", center, bean.getShareAmt() == null ? "" : bean.getShareAmt().toString());
                    excel.writeStr(sheet, row, "J", center, bean.getAmt() == null ? "" : bean.getAmt().toString());
                    excel.writeStr(sheet, row, "K", center, bean.getPrice() == null ? "" : bean.getPrice().toString());
                    excel.writeStr(sheet, row, "L", center, bean.getIntamt() == null ? "" : bean.getIntamt().toString());
                    excel.writeStr(sheet, row, "M", center, bean.getHandleAmt() == null ? "" : bean.getHandleAmt().toString());
                    excel.writeStr(sheet, row, "N", center, invType);
                    if (!StringUtil.isNullOrEmpty(map.get("deal"))) {
                        excel.writeStr(sheet, row, "O", center, approveStatus);
                    } else {
                        excel.writeStr(sheet, row, "O", center, bean.getTaskName());
                    }
                    excel.writeStr(sheet, row, "P", center, bean.getSponsor());
                    excel.writeStr(sheet, row, "Q", center, bean.getSponInstName());
                    excel.writeStr(sheet, row, "R", center, bean.getaDate());
                }
            } else if ("现金分红".equals(prdName)) {
                /* 先查数据，再设置表格属性以及内容 */
                Page<CFtRedd> data = new Page<CFtRedd>();
                String approveType = map.get("approveType");
                Map<String, Object> params = new HashMap<String, Object>();
                params.put("approveStatus", map.get("approveStatus")); // 审批状态
                params.put("branchId", branchId);
                params.put("pageNumber", "1");
                params.put("pageSize", "999");
                params.put("dealNo", ticketId);//审批单号
                params.put("sponInst", sponInst);
                if (!StringUtil.isNullOrEmpty(map.get("deal"))) { // 总交易查询导出
                    params.put("isActive", DictConstants.YesNo.YES);
                    params.put("userId", SlSessionHelper.getUserId());
                    data = qryTotalDealService.searchTotalDealJJXJFH(params, 3);
                } else { // 正式交易台查询导出
                    if ("mine".equals(approveType)) {
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        data = cFtReddService.searchCFtReddList(params, 3);
                    } else if ("approve".equals(approveType)) {
                        String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + ","
                                + DictConstants.ApproveStatus.VerifyRefuse + "," + DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
                        String[] orderStatus = approveStatus.split(",");
                        params.put("approveStatusNo", orderStatus);
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        data = cFtReddService.searchCFtReddList(params, 1);
                    } else {
                        String approveStatus = ParameterUtil.getString(params, "approveStatus", "");
                        if (StringUtil.isNullOrEmpty(approveStatus)) {
                            approveStatus = DictConstants.ApproveStatus.ApprovedPass;
                            approveStatus += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
                            approveStatus += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
                            approveStatus += "," + DictConstants.ApproveStatus.Verified;
                            approveStatus += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
                            // 20180502修改bug:已审批列表中增加"审批中"
                            approveStatus += "," + DictConstants.ApproveStatus.Approving;
                            String[] orderStatus = approveStatus.split(",");
                            params.put("approveStatusNo", orderStatus);
                        }
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        data = cFtReddService.searchCFtReddList(params, 2);
                    }
                }
                // 表头
                excel = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
                CellStyle center = excel.getDefaultCenterStrCellStyle();
                int sheet = 0;
                int row = 0;
                // 表格sheet名称
                excel.getWb().createSheet("基金现金分红报表");
                // 设置表头字段名
                excel.writeStr(sheet, row, "A", center, "交易编号");
                excel.writeStr(sheet, row, "B", center, "基金全称");
                excel.writeStr(sheet, row, "C", center, "基金管理人名称");
                excel.writeStr(sheet, row, "D", center, "基金经理");
                excel.writeStr(sheet, row, "E", center, "基金币种");
                excel.writeStr(sheet, row, "F", center, "分红时刻持仓(份)");
                excel.writeStr(sheet, row, "G", center, "分红金额(元)");
                excel.writeStr(sheet, row, "H", center, "单位净值(元)");
                excel.writeStr(sheet, row, "I", center, "宣告日期");
                excel.writeStr(sheet, row, "J", center, "分红日期");
                excel.writeStr(sheet, row, "K", center, "会计类型");
                excel.writeStr(sheet, row, "L", center, "审批状态");
                excel.writeStr(sheet, row, "M", center, "审批发起人");
                excel.writeStr(sheet, row, "N", center, "审批发起机构");
                excel.writeStr(sheet, row, "O", center, "审批发起时间");
                // 设置列宽
                excel.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(1, 30 * 256);
                excel.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(8, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(9, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(10, 35 * 256);
                excel.getSheetAt(sheet).setColumnWidth(11, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(12, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(13, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(14, 20 * 256);
                CFtRedd bean = new CFtRedd();
                Map<String, String> upmap = new HashMap<String, String>();
                Map<String, String> upmap2 = new HashMap<String, String>();
                Map<String, String> approveMap = new HashMap<String, String>();
                for (int i = 0; i < data.size(); i++) {
                    bean = data.get(i);
                    row++;

                    upmap.put("dictId", "Currency");
                    upmap.put("dictKey", bean.getCcy());
                    String ccy = ifsService.queryDictValue(upmap); // 取数据字典中的value值

                    upmap2.put("dictId", "AccThreeSubject");
                    upmap2.put("dictKey", bean.getInvType());
                    String invType = ifsService.queryDictValue(upmap2); // 取数据字典中的value值

                    approveMap.put("dictId", "ApproveStatus");
                    approveMap.put("dictKey", bean.getApproveStatus());
                    String approveStatus = ifsService.queryDictValue(approveMap); // 审批状态

                    // 插入数据
                    excel.writeStr(sheet, row, "A", center, bean.getDealNo());
                    excel.writeStr(sheet, row, "B", center, bean.getFundFullName());
                    excel.writeStr(sheet, row, "C", center, bean.getCname());
                    excel.writeStr(sheet, row, "D", center, bean.getContact());
                    excel.writeStr(sheet, row, "E", center, ccy);
                    excel.writeStr(sheet, row, "F", center, bean.getBshareAmt() == null ? "" : bean.getBshareAmt().toString());
                    excel.writeStr(sheet, row, "G", center, bean.getAmt() == null ? "" : bean.getAmt().toString());
                    excel.writeStr(sheet, row, "H", center, bean.getPrice() == null ? "" : bean.getPrice().toString());
                    excel.writeStr(sheet, row, "I", center, bean.getTdate());
                    excel.writeStr(sheet, row, "J", center, bean.getVdate());
                    excel.writeStr(sheet, row, "K", center, invType);
                    if (!StringUtil.isNullOrEmpty(map.get("deal"))) {
                        excel.writeStr(sheet, row, "L", center, approveStatus);
                    } else {
                        excel.writeStr(sheet, row, "L", center, bean.getTaskName());
                    }
                    excel.writeStr(sheet, row, "M", center, bean.getSponsor());
                    excel.writeStr(sheet, row, "N", center, bean.getSponInstName());
                    excel.writeStr(sheet, row, "O", center, bean.getaDate());
                }

            } else if ("红利再投".equals(prdName)) {
                /* 先查数据，再设置表格属性以及内容 */
                Page<CFtRein> data = new Page<CFtRein>();
                String approveType = map.get("approveType");
                Map<String, Object> params = new HashMap<String, Object>();
                params.put("approveStatus", map.get("approveStatus")); // 审批状态
                params.put("branchId", branchId);
                params.put("pageNumber", "1");
                params.put("pageSize", "999");
                params.put("dealNo", ticketId);//审批单号
                params.put("sponInst", sponInst);
                if (!StringUtil.isNullOrEmpty(map.get("deal"))) { // 总交易查询导出
                    params.put("isActive", DictConstants.YesNo.YES);
                    params.put("userId", SlSessionHelper.getUserId());
                    data = qryTotalDealService.searchTotalDealJJHLZT(params, 3);
                } else { // 正式交易台查询导出
                    if ("mine".equals(approveType)) {
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        data = cFtReinService.searchCFtReinList(params, 3);
                    } else if ("approve".equals(approveType)) {
                        String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + ","
                                + DictConstants.ApproveStatus.VerifyRefuse + "," + DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
                        String[] orderStatus = approveStatus.split(",");
                        params.put("approveStatusNo", orderStatus);
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        data = cFtReinService.searchCFtReinList(params, 1);
                    } else {
                        String approveStatus = ParameterUtil.getString(params, "approveStatus", "");
                        if (StringUtil.isNullOrEmpty(approveStatus)) {
                            approveStatus = DictConstants.ApproveStatus.ApprovedPass;
                            approveStatus += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
                            approveStatus += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
                            approveStatus += "," + DictConstants.ApproveStatus.Verified;
                            approveStatus += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
                            // 20180502修改bug:已审批列表中增加"审批中"
                            approveStatus += "," + DictConstants.ApproveStatus.Approving;
                            String[] orderStatus = approveStatus.split(",");
                            params.put("approveStatusNo", orderStatus);
                        }
                        params.put("isActive", DictConstants.YesNo.YES);
                        params.put("userId", SlSessionHelper.getUserId());
                        data = cFtReinService.searchCFtReinList(params, 2);
                    }
                }
                // 表头
                excel = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
                CellStyle center = excel.getDefaultCenterStrCellStyle();
                int sheet = 0;
                int row = 0;
                // 表格sheet名称
                excel.getWb().createSheet("基金红利再投报表");
                // 设置表头字段名
                excel.writeStr(sheet, row, "A", center, "交易编号");
                excel.writeStr(sheet, row, "B", center, "基金全称");
                excel.writeStr(sheet, row, "C", center, "基金管理人名称");
                excel.writeStr(sheet, row, "D", center, "基金经理");
                excel.writeStr(sheet, row, "E", center, "基金币种");
                excel.writeStr(sheet, row, "F", center, "转投份额(份)");
                excel.writeStr(sheet, row, "G", center, "转投金额(元)");
                excel.writeStr(sheet, row, "H", center, "转投时间");
                excel.writeStr(sheet, row, "I", center, "会计类型");
                excel.writeStr(sheet, row, "J", center, "审批状态");
                excel.writeStr(sheet, row, "K", center, "审批发起人");
                excel.writeStr(sheet, row, "L", center, "审批发起机构");
                excel.writeStr(sheet, row, "M", center, "审批发起时间");
                // 设置列宽
                excel.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(1, 30 * 256);
                excel.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(8, 35 * 256);
                excel.getSheetAt(sheet).setColumnWidth(9, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(10, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(11, 20 * 256);
                excel.getSheetAt(sheet).setColumnWidth(12, 20 * 256);
                CFtRein bean = new CFtRein();
                Map<String, String> upmap = new HashMap<String, String>();
                Map<String, String> upmap2 = new HashMap<String, String>();
                Map<String, String> approveMap = new HashMap<String, String>();
                for (int i = 0; i < data.size(); i++) {
                    bean = data.get(i);
                    row++;

                    upmap.put("dictId", "Currency");
                    upmap.put("dictKey", bean.getCcy());
                    String ccy = ifsService.queryDictValue(upmap); // 取数据字典中的value值

                    upmap2.put("dictId", "AccThreeSubject");
                    upmap2.put("dictKey", bean.getInvType());
                    String invType = ifsService.queryDictValue(upmap2); // 取数据字典中的value值

                    approveMap.put("dictId", "ApproveStatus");
                    approveMap.put("dictKey", bean.getApproveStatus());
                    String approveStatus = ifsService.queryDictValue(approveMap); // 审批状态

                    // 插入数据
                    excel.writeStr(sheet, row, "A", center, bean.getDealNo());
                    excel.writeStr(sheet, row, "B", center, bean.getFundFullName());
                    excel.writeStr(sheet, row, "C", center, bean.getCname());
                    excel.writeStr(sheet, row, "D", center, bean.getContact());
                    excel.writeStr(sheet, row, "E", center, ccy);
                    excel.writeStr(sheet, row, "F", center, bean.getEshareAmt() == null ? "" : bean.getEshareAmt().toString());
                    excel.writeStr(sheet, row, "G", center, bean.geteAmt() == null ? "" : bean.geteAmt().toString());
                    excel.writeStr(sheet, row, "H", center, bean.getVdate());
                    excel.writeStr(sheet, row, "I", center, invType);
                    if (!StringUtil.isNullOrEmpty(map.get("deal"))) {
                        excel.writeStr(sheet, row, "J", center, approveStatus);
                    } else {
                        excel.writeStr(sheet, row, "J", center, bean.getTaskName());
                    }
                    excel.writeStr(sheet, row, "K", center, bean.getSponsor());
                    excel.writeStr(sheet, row, "L", center, bean.getSponInstName());
                    excel.writeStr(sheet, row, "M", center, bean.getaDate());

                }
            }

            //
            return excel;
        } catch (Exception e) {
            throw new RException(e);
        }
    }

    /**
     * swift报文导出Excel
     */
    @ResponseBody
    @RequestMapping(value = "/exportSwiftMsg")
    public void exportSwiftMsg(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String ua = request.getHeader("User-Agent");
        String dealNo = request.getParameter("dealNo"); // OPICS编号
        String msgType = request.getParameter("msgType"); // 报文类型
        String settway = request.getParameter("settway"); // 清算方式
        String settFlag = request.getParameter("settFlag"); // 清算状态
        String swftcDate = request.getParameter("swftcDate"); // 报文生成日期
        String dealDate = request.getParameter("dealDate"); // 报文处理日期
        String verind = request.getParameter("verind"); // 流程处理状态
        String filename = "swift报表.xlsx"; // 文件名
        String encode_filename = "";
        try {
            encode_filename = URLEncoder.encode(filename, "utf-8");
        } catch (UnsupportedEncodingException e1) {
            throw new RException(e1.getMessage());
        }
        response.setContentType("application/vnd.ms-excel");
        if (ua != null) {
            response.setHeader("Content-Disposition", "attachment; filename=" + encode_filename);
        }
        response.setHeader("Connection", "close");
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("dealNo", dealNo);
            map.put("msgType", msgType);
            map.put("settway", settway);
            map.put("settFlag", settFlag);
            map.put("swftcDate", swftcDate);
            map.put("dealDate", dealDate);
            map.put("verind", verind);
            ExcelUtil e = downloadSwiftExcel(map);
            OutputStream ouputStream = response.getOutputStream();
            e.getWb().write(ouputStream);
            ouputStream.flush();
            ouputStream.close();
        } catch (Exception e) {
            throw new RException(e);
        }
    }

    /**
     * swift报文下载内容
     */
    public ExcelUtil downloadSwiftExcel(Map<String, Object> map) throws Exception {
        ExcelUtil excel = null;
        try {
            /* 先查数据，再设置表格属性以及内容 */
            List<SlSwiftBean> data = ifsSwiftMapper.getMessage(map);
            SlSwiftBean bean = new SlSwiftBean();
            // 表头
            excel = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
            CellStyle center = excel.getDefaultCenterStrCellStyle();
            int sheet = 0;
            int row = 0;
            // 表格sheet名称
            excel.getWb().createSheet("swift报表");
            // 设置表头字段名
            excel.writeStr(sheet, row, "A", center, "OPICS编号");
            excel.writeStr(sheet, row, "B", center, "流水号");
            excel.writeStr(sheet, row, "C", center, "产品代码");
            excel.writeStr(sheet, row, "D", center, "报文类型");
            excel.writeStr(sheet, row, "E", center, "报文内容");
            // 设置列宽
            excel.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(4, 100 * 256);
            for (int i = 0; i < data.size(); i++) {
                bean = data.get(i);
                row++;
                // 插入数据
                excel.writeStr(sheet, row, "A", center, bean.getDealNo());
                excel.writeStr(sheet, row, "B", center, bean.getBatchNo());
                excel.writeStr(sheet, row, "C", center, bean.getProdCode());
                excel.writeStr(sheet, row, "D", center, bean.getMsgType());
                excel.writeStr(sheet, row, "E", center, bean.getMsg());
            }
            return excel;
        } catch (Exception e) {
            throw new RException(e);
        }
    }

    /**
     * 导出债券持仓余额Excel
     */
    @ResponseBody
    @RequestMapping(value = "/exportCcyeExcel")
    public void exportCcyeExcel(HttpServletRequest request, HttpServletResponse response) {
        String ua = request.getHeader("User-Agent");
        String secid = request.getParameter("secid"); // 债券代码
        String postdate = request.getParameter("postdate");
        String type = request.getParameter("type");
        String filename;
        if (type != null && !"".equals(type)) {
            filename = "债券持仓余额表.xlsx"; // 文件名
        } else {
            filename = "发行类持仓余额表.xlsx"; // 文件名
        }

        String encode_filename = "";
        try {
            encode_filename = URLEncoder.encode(filename, "utf-8");
        } catch (UnsupportedEncodingException e1) {
            throw new RException(e1.getMessage());
        }
        response.setContentType("application/vnd.ms-excel");
        if (ua != null && ua.indexOf("Firefox/") >= 0) {
            response.setHeader("Content-Disposition", "attachment; filename*=" + encode_filename);
        } else {
            response.setHeader("Content-Disposition", "attachment; filename=" + encode_filename);
        }
        response.setHeader("Connection", "close");
        try {

            Map<String, Object> map = new HashMap<String, Object>();
            map.put("secid", secid);
            map.put("postdate", postdate);
            map.put("type", type);
            ExcelUtil e = downloadCcyeExcel(map);
            OutputStream ouputStream = response.getOutputStream();
            e.getWb().write(ouputStream);
            ouputStream.flush();
            ouputStream.close();
        } catch (Exception e) {
            logger.info(e);
            throw new RException(e);
        }
    }

    ;

    /**
     * 债券持仓余额Excel文件下载内容
     */
    public ExcelUtil downloadCcyeExcel(Map<String, Object> map) throws Exception {
        SlBondPositionBalanceBean bean2 = new SlBondPositionBalanceBean();
        List<SlBondPositionBalanceBean> list = baseServer.getBondPositionBalanceExport(map);
        List<SlBondPositionBalanceBean> newList = new ArrayList<SlBondPositionBalanceBean>();
        for (SlBondPositionBalanceBean slBondPositionBalanceBean : list) {
            SlBondPositionBalanceBean bean = new SlBondPositionBalanceBean();
            bean = slBondPositionBalanceBean;
            if (StringUtil.isNotEmpty(slBondPositionBalanceBean.getIssuer())) {
                IfsOpicsCust cust = ifsOpicsCustService.searchIfsOpicsCust(slBondPositionBalanceBean.getIssuer().trim());
                if (cust != null) {
                    if (StringUtil.isNotEmpty(cust.getCliname())) {
                        bean.setIssuer(cust.getCliname());
                    }
                    bean.setSubjectrating(cust.getRatresult1());
                }
            }
            if (StringUtil.isNotEmpty(slBondPositionBalanceBean.getSecid())) {
                IfsOpicsBond bond = ifsOpicsBondService.searchOneById(slBondPositionBalanceBean.getSecid().trim());
                if (bond != null) {
                    bean.setBondtrating(bond.getBondrating());
                }
            }
            if (StringUtil.isNotEmpty(slBondPositionBalanceBean.getAcctngtype())) {
                IfsOpicsActy acty = ifsOpicsActyService.searchById(slBondPositionBalanceBean.getAcctngtype().trim());
                if (acty != null) {
                    if (StringUtil.isNotEmpty(acty.getAcctdesccn())) {
                        bean.setAcctngtype(acty.getAcctdesccn());
                    }
                }
            }
            newList.add(bean);
        }
        ExcelUtil eutil = null;
        try {
            // 表头
            eutil = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
            CellStyle center = eutil.getDefaultCenterStrCellStyle();
            int sheet = 0;
            int row = 0;
            // 表格sheet名称
            if (map.get("type") != null && !"".equals(map.get("type").toString())) {
                eutil.getWb().createSheet("债券持仓余额表");
            } else {
                eutil.getWb().createSheet("发行类持仓余额表");
            }

            // 设置表头字段名
            eutil.writeStr(sheet, row, "A", center, "债券代码");
            eutil.writeStr(sheet, row, "B", center, "发行人");
            eutil.writeStr(sheet, row, "C", center, "债券类型");
            eutil.writeStr(sheet, row, "D", center, "所属账户");
            eutil.writeStr(sheet, row, "E", center, "组合分类");
            eutil.writeStr(sheet, row, "F", center, "账户类型");
            eutil.writeStr(sheet, row, "G", center, "下一付息日");
            eutil.writeStr(sheet, row, "H", center, "券面价值");
            eutil.writeStr(sheet, row, "I", center, "摊余成本");
            eutil.writeStr(sheet, row, "J", center, "本日计提");
            eutil.writeStr(sheet, row, "K", center, "截止本日计提");
            eutil.writeStr(sheet, row, "L", center, "未摊余额");
            eutil.writeStr(sheet, row, "M", center, "本日摊销");
            eutil.writeStr(sheet, row, "N", center, "单位成本");
            eutil.writeStr(sheet, row, "O", center, "止损净价");
            eutil.writeStr(sheet, row, "P", center, "公允价格");
            eutil.writeStr(sheet, row, "Q", center, "券面");
            eutil.writeStr(sheet, row, "R", center, "公允价值");
            eutil.writeStr(sheet, row, "S", center, "实际收益率");
            eutil.writeStr(sheet, row, "T", center, "估值损益");
            eutil.writeStr(sheet, row, "U", center, "起息日");
            eutil.writeStr(sheet, row, "V", center, "到期日");
            eutil.writeStr(sheet, row, "W", center, "票面利率");
            eutil.writeStr(sheet, row, "X", center, "剩余期限");
            eutil.writeStr(sheet, row, "Y", center, "利率类型");
            eutil.writeStr(sheet, row, "Z", center, "付息频率");
            eutil.writeStr(sheet, row, "AA", center, "主体评级");
            eutil.writeStr(sheet, row, "AB", center, "债项评级");
            eutil.writeStr(sheet, row, "AC", center, "数据日期");
            // 设置列宽
            eutil.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(8, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(9, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(10, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(11, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(12, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(13, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(14, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(15, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(16, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(17, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(18, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(19, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(20, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(21, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(22, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(23, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(24, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(25, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(26, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(27, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(28, 20 * 256);

            Map<String, String> upmap = new HashMap<String, String>();
            for (int i = 0; i < newList.size(); i++) {
                bean2 = newList.get(i);
                row++;
                DecimalFormat df = new DecimalFormat("#,##0.0000");
                // 插入数据
                eutil.writeStr(sheet, row, "A", center, bean2.getSecid());
                eutil.writeStr(sheet, row, "B", center, bean2.getIssuer());
                eutil.writeStr(sheet, row, "C", center, bean2.getAcctngtype());
                eutil.writeStr(sheet, row, "D", center, bean2.getCost());
                eutil.writeStr(sheet, row, "E", center, bean2.getPort());
                eutil.writeStr(sheet, row, "F", center, bean2.getInvtype());
                eutil.writeStr(sheet, row, "G", center, bean2.getIpaydate() == null ? "" : bean2.getIpaydate().substring(0, 10));
                eutil.writeStr(sheet, row, "H", center, bean2.getParamt());
                eutil.writeStr(sheet, row, "I", center, bean2.getAmortamt());
                eutil.writeStr(sheet, row, "J", center, bean2.getTdyintamt());
                eutil.writeStr(sheet, row, "K", center, bean2.getAccrintamt());
                eutil.writeStr(sheet, row, "L", center, bean2.getUnamortamt());
                eutil.writeStr(sheet, row, "M", center, bean2.getTdyamortamt());
                if (bean2.getUnitcost() != null) {
                    eutil.writeStr(sheet, row, "N", center, df.format(Double.valueOf(String.valueOf(bean2.getUnitcost()))));
                } else {
                    eutil.writeStr(sheet, row, "N", center, bean2.getUnitcost());
                }
                if (bean2.getCutpoint() != null) {
                    eutil.writeStr(sheet, row, "O", center, df.format(Double.valueOf(String.valueOf(bean2.getCutpoint()))));
                } else {
                    eutil.writeStr(sheet, row, "O", center, bean2.getCutpoint());
                }
                eutil.writeStr(sheet, row, "P", center, bean2.getClsgprice());
                eutil.writeStr(sheet, row, "Q", center, bean2.getFacevalue());
                eutil.writeStr(sheet, row, "R", center, bean2.getFairvalue());
                eutil.writeStr(sheet, row, "S", center, bean2.getRealRate());
                eutil.writeStr(sheet, row, "T", center, bean2.getDiffamt());
                eutil.writeStr(sheet, row, "U", center, bean2.getVdate() == null ? "" : bean2.getVdate().substring(0, 10));
                eutil.writeStr(sheet, row, "V", center, bean2.getMdate() == null ? "" : bean2.getMdate().substring(0, 10));
                eutil.writeStr(sheet, row, "W", center, bean2.getCouprate());
                if (bean2.getYearstomaturity() != null) {
                    eutil.writeStr(sheet, row, "X", center, df.format(Double.valueOf(String.valueOf(bean2.getYearstomaturity()))));
                } else {
                    eutil.writeStr(sheet, row, "X", center, bean2.getYearstomaturity());
                }
                eutil.writeStr(sheet, row, "Y", center, bean2.getRatetype());
                eutil.writeStr(sheet, row, "Z", center, bean2.getIntpaycycle());
                eutil.writeStr(sheet, row, "AA", center, bean2.getSubjectrating());
                upmap.put("dictId", "Rating");
                upmap.put("dictKey", bean2.getBondtrating());
                String rating = ifsService.queryDictValue(upmap); // 取数据字典中的value值
                eutil.writeStr(sheet, row, "AB", center, rating);
                eutil.writeStr(sheet, row, "AC", center, bean2.getPostdate() == null ? "" : bean2.getPostdate().substring(0, 10));
            }
            return eutil;
        } catch (Exception e) {
            logger.info(e);
            throw new RException(e);
        }
    }


    /**
     * 导出债券投资损益Excel
     */
    @ResponseBody
    @RequestMapping(value = "/exportTzsyExcel")
    public void exportTzsyExcel(HttpServletRequest request, HttpServletResponse response) {
        String ua = request.getHeader("User-Agent");
        String filename = "债券投资损益历史表.xlsx"; // 文件名
        String encode_filename = "";
        try {
            encode_filename = URLEncoder.encode(filename, "utf-8");
        } catch (UnsupportedEncodingException e1) {
            throw new RException(e1.getMessage());
        }
        response.setContentType("application/vnd.ms-excel");
        if (ua != null && ua.indexOf("Firefox/") >= 0) {
            response.setHeader("Content-Disposition", "attachment; filename*=" + encode_filename);
        } else {
            response.setHeader("Content-Disposition", "attachment; filename=" + encode_filename);
        }
        response.setHeader("Connection", "close");
        try {
            String inputdate = request.getParameter("inputdate"); // 数据日期
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("inputdate", inputdate);
            ExcelUtil e = downloadTzsyExcel(map);
            OutputStream ouputStream = response.getOutputStream();
            e.getWb().write(ouputStream);
            ouputStream.flush();
            ouputStream.close();
        } catch (Exception e) {
            logger.info(e);
            throw new RException(e);
        }
    }

    ;

    /**
     * 债券投资损益Excel文件下载内容
     */
    @SuppressWarnings("static-access")
    public ExcelUtil downloadTzsyExcel(Map<String, Object> map) throws Exception {
        SlSdCdInvestHistoryBean bean = new SlSdCdInvestHistoryBean();
        List<SlSdCdInvestHistoryBean> list = baseServer.getSdInvestHistoryExport(map); // 查数据
        ExcelUtil eutil = null;
        try {
            // 表头
            eutil = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
            CellStyle center = eutil.getDefaultCenterStrCellStyle();
            int sheet = 0;
            int row = 0;
            // 表格sheet名称
            eutil.getWb().createSheet("债券投资损益历史表");
            // 设置表头字段名
            eutil.writeStr(sheet, row, "A", center, "日期");
            eutil.writeStr(sheet, row, "B", center, "总投资");
            eutil.writeStr(sheet, row, "C", center, "总投资");
            eutil.writeStr(sheet, row, "D", center, "总投资");
            eutil.writeStr(sheet, row, "E", center, "总投资");
            eutil.writeStr(sheet, row, "F", center, "总投资");
            eutil.writeStr(sheet, row, "G", center, "总投资");
            eutil.writeStr(sheet, row, "H", center, "持有到期投资");
            eutil.writeStr(sheet, row, "I", center, "持有到期投资");
            eutil.writeStr(sheet, row, "J", center, "持有到期投资");
            eutil.writeStr(sheet, row, "K", center, "持有到期投资");
            eutil.writeStr(sheet, row, "L", center, "持有到期投资");
            eutil.writeStr(sheet, row, "M", center, "持有到期投资");
            eutil.writeStr(sheet, row, "N", center, "可供出售投资");
            eutil.writeStr(sheet, row, "O", center, "可供出售投资");
            eutil.writeStr(sheet, row, "P", center, "可供出售投资");
            eutil.writeStr(sheet, row, "Q", center, "可供出售投资");
            eutil.writeStr(sheet, row, "R", center, "可供出售投资");
            eutil.writeStr(sheet, row, "S", center, "可供出售投资");
            eutil.writeStr(sheet, row, "T", center, "交易类投资");
            eutil.writeStr(sheet, row, "U", center, "交易类投资");
            eutil.writeStr(sheet, row, "V", center, "交易类投资");
            eutil.writeStr(sheet, row, "W", center, "交易类投资");
            eutil.writeStr(sheet, row, "X", center, "交易类投资");
            eutil.writeStr(sheet, row, "Y", center, "交易类投资");

            row = row + 1;
            eutil.writeStr(sheet, row, "A", center, "日期");
            eutil.writeStr(sheet, row, "B", center, "券面合计");
            eutil.writeStr(sheet, row, "C", center, "投资收益合计");
            eutil.writeStr(sheet, row, "D", center, "加权收益率合计");
            eutil.writeStr(sheet, row, "E", center, "久期");
            eutil.writeStr(sheet, row, "F", center, "DV01");
            eutil.writeStr(sheet, row, "G", center, "估值损益");
            eutil.writeStr(sheet, row, "H", center, "券面合计");
            eutil.writeStr(sheet, row, "I", center, "投资收益合计");
            eutil.writeStr(sheet, row, "J", center, "加权收益率合计");
            eutil.writeStr(sheet, row, "K", center, "久期");
            eutil.writeStr(sheet, row, "L", center, "DV01");
            eutil.writeStr(sheet, row, "M", center, "估值损益");
            eutil.writeStr(sheet, row, "N", center, "券面合计");
            eutil.writeStr(sheet, row, "O", center, "投资收益合计");
            eutil.writeStr(sheet, row, "P", center, "加权收益率合计");
            eutil.writeStr(sheet, row, "Q", center, "久期");
            eutil.writeStr(sheet, row, "R", center, "DV01");
            eutil.writeStr(sheet, row, "S", center, "估值损益");
            eutil.writeStr(sheet, row, "T", center, "券面合计");
            eutil.writeStr(sheet, row, "U", center, "投资收益合计");
            eutil.writeStr(sheet, row, "V", center, "加权收益率合计");
            eutil.writeStr(sheet, row, "W", center, "久期");
            eutil.writeStr(sheet, row, "X", center, "DV01");
            eutil.writeStr(sheet, row, "Y", center, "估值损益");
            // 合并单元格
            eutil.cellRange(0, 1, 0, 0, eutil.getSheetAt(0), eutil.getWb());
            eutil.cellRange(0, 0, 1, 6, eutil.getSheetAt(0), eutil.getWb());
            eutil.cellRange(0, 0, 7, 12, eutil.getSheetAt(0), eutil.getWb());
            eutil.cellRange(0, 0, 13, 18, eutil.getSheetAt(0), eutil.getWb());
            eutil.cellRange(0, 0, 19, 24, eutil.getSheetAt(0), eutil.getWb());
            // 设置列宽
            eutil.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(8, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(9, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(10, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(11, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(12, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(13, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(14, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(15, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(16, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(17, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(18, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(19, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(20, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(21, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(22, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(23, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(24, 20 * 256);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            for (int i = 0; i < list.size(); i++) {
                bean = list.get(i);
                row++;
                // 插入数据
                eutil.writeStr(sheet, row, "A", center, bean.getInputdate() == null ? "" : sdf.format(bean.getInputdate()));
                eutil.writeStr(sheet, row, "B", center, bean.getTotalFacevalue());
                eutil.writeStr(sheet, row, "C", center, bean.getTotalInvestIncome());
                eutil.writeStr(sheet, row, "D", center, bean.getTotalWeightRate());
                eutil.writeStr(sheet, row, "E", center, bean.getTotalDuration());
                eutil.writeStr(sheet, row, "F", center, bean.getTotalDv01());
                eutil.writeStr(sheet, row, "G", center, bean.getTotalGainsLosses());
                eutil.writeStr(sheet, row, "H", center, bean.gethFacevalue());
                eutil.writeStr(sheet, row, "I", center, bean.gethInvestIncome());
                eutil.writeStr(sheet, row, "J", center, bean.gethWeightRate());
                eutil.writeStr(sheet, row, "K", center, bean.gethDuration());
                eutil.writeStr(sheet, row, "L", center, bean.gethDv01());
                eutil.writeStr(sheet, row, "M", center, bean.gethGainsLosses());
                eutil.writeStr(sheet, row, "N", center, bean.getaFacevalue());
                eutil.writeStr(sheet, row, "O", center, bean.getaInvestIncome());
                eutil.writeStr(sheet, row, "P", center, bean.getaWeightRate());
                eutil.writeStr(sheet, row, "Q", center, bean.getaDuration());
                eutil.writeStr(sheet, row, "R", center, bean.getaDv01());
                eutil.writeStr(sheet, row, "S", center, bean.getaGainsLosses());
                eutil.writeStr(sheet, row, "T", center, bean.gettFacevalue());
                eutil.writeStr(sheet, row, "U", center, bean.gettInvestIncome());
                eutil.writeStr(sheet, row, "V", center, bean.gettWeightRate());
                eutil.writeStr(sheet, row, "W", center, bean.gettDuration());
                eutil.writeStr(sheet, row, "X", center, bean.gettDv01());
                eutil.writeStr(sheet, row, "Y", center, bean.gettGainsLosses());
            }
            return eutil;
        } catch (Exception e) {
            logger.info(e);
            throw new RException(e);
        }
    }

    /**
     * 导出存单投资损益历史Excel
     */
    @ResponseBody
    @RequestMapping(value = "/exportCdsyExcel")
    public void exportCdsyExcel(HttpServletRequest request, HttpServletResponse response) {
        String ua = request.getHeader("User-Agent");
        String filename = "存单投资损益历史表.xlsx"; // 文件名
        String encode_filename = "";
        try {
            encode_filename = URLEncoder.encode(filename, "utf-8");
        } catch (UnsupportedEncodingException e1) {
            throw new RException(e1.getMessage());
        }
        response.setContentType("application/vnd.ms-excel");
        if (ua != null && ua.indexOf("Firefox/") >= 0) {
            response.setHeader("Content-Disposition", "attachment; filename*=" + encode_filename);
        } else {
            response.setHeader("Content-Disposition", "attachment; filename=" + encode_filename);
        }
        response.setHeader("Connection", "close");
        try {
            String inputdate = request.getParameter("inputdate"); // 数据日期
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("inputdate", inputdate);
            ExcelUtil e = downloadCdsyExcel(map);
            OutputStream ouputStream = response.getOutputStream();
            e.getWb().write(ouputStream);
            ouputStream.flush();
            ouputStream.close();
        } catch (Exception e) {
            logger.info(e);
            throw new RException(e);
        }
    }

    ;

    /**
     * 存单投资损益Excel文件下载内容
     */
    @SuppressWarnings("static-access")
    public ExcelUtil downloadCdsyExcel(Map<String, Object> map) throws Exception {
        SlSdCdInvestHistoryBean bean = new SlSdCdInvestHistoryBean();
        List<SlSdCdInvestHistoryBean> list = baseServer.getCdInvestHistoryExport(map); // 查数据
        ExcelUtil eutil = null;
        try {
            // 表头
            eutil = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
            CellStyle center = eutil.getDefaultCenterStrCellStyle();
            int sheet = 0;
            int row = 0;
            // 表格sheet名称
            eutil.getWb().createSheet("存单投资损益历史表");
            // 设置表头字段名
            eutil.writeStr(sheet, row, "A", center, "日期");
            eutil.writeStr(sheet, row, "B", center, "总投资");
            eutil.writeStr(sheet, row, "C", center, "总投资");
            eutil.writeStr(sheet, row, "D", center, "总投资");
            eutil.writeStr(sheet, row, "E", center, "总投资");
            eutil.writeStr(sheet, row, "F", center, "总投资");
            eutil.writeStr(sheet, row, "G", center, "总投资");
            eutil.writeStr(sheet, row, "H", center, "持有到期投资");
            eutil.writeStr(sheet, row, "I", center, "持有到期投资");
            eutil.writeStr(sheet, row, "J", center, "持有到期投资");
            eutil.writeStr(sheet, row, "K", center, "持有到期投资");
            eutil.writeStr(sheet, row, "L", center, "持有到期投资");
            eutil.writeStr(sheet, row, "M", center, "持有到期投资");
            eutil.writeStr(sheet, row, "N", center, "可供出售投资");
            eutil.writeStr(sheet, row, "O", center, "可供出售投资");
            eutil.writeStr(sheet, row, "P", center, "可供出售投资");
            eutil.writeStr(sheet, row, "Q", center, "可供出售投资");
            eutil.writeStr(sheet, row, "R", center, "可供出售投资");
            eutil.writeStr(sheet, row, "S", center, "可供出售投资");
            eutil.writeStr(sheet, row, "T", center, "交易类投资");
            eutil.writeStr(sheet, row, "U", center, "交易类投资");
            eutil.writeStr(sheet, row, "V", center, "交易类投资");
            eutil.writeStr(sheet, row, "W", center, "交易类投资");
            eutil.writeStr(sheet, row, "X", center, "交易类投资");
            eutil.writeStr(sheet, row, "Y", center, "交易类投资");
            row = row + 1;
            eutil.writeStr(sheet, row, "A", center, "日期");
            eutil.writeStr(sheet, row, "B", center, "券面合计");
            eutil.writeStr(sheet, row, "C", center, "投资收益合计");
            eutil.writeStr(sheet, row, "D", center, "加权收益率合计");
            eutil.writeStr(sheet, row, "E", center, "久期");
            eutil.writeStr(sheet, row, "F", center, "DV01");
            eutil.writeStr(sheet, row, "G", center, "估值损益");
            eutil.writeStr(sheet, row, "H", center, "券面合计");
            eutil.writeStr(sheet, row, "I", center, "投资收益合计");
            eutil.writeStr(sheet, row, "J", center, "加权收益率合计");
            eutil.writeStr(sheet, row, "K", center, "久期");
            eutil.writeStr(sheet, row, "L", center, "DV01");
            eutil.writeStr(sheet, row, "M", center, "估值损益");
            eutil.writeStr(sheet, row, "N", center, "券面合计");
            eutil.writeStr(sheet, row, "O", center, "投资收益合计");
            eutil.writeStr(sheet, row, "P", center, "加权收益率合计");
            eutil.writeStr(sheet, row, "Q", center, "久期");
            eutil.writeStr(sheet, row, "R", center, "DV01");
            eutil.writeStr(sheet, row, "S", center, "估值损益");
            eutil.writeStr(sheet, row, "T", center, "券面合计");
            eutil.writeStr(sheet, row, "U", center, "投资收益合计");
            eutil.writeStr(sheet, row, "V", center, "加权收益率合计");
            eutil.writeStr(sheet, row, "W", center, "久期");
            eutil.writeStr(sheet, row, "X", center, "DV01");
            eutil.writeStr(sheet, row, "Y", center, "估值损益");
            // 合并单元格
            eutil.cellRange(0, 1, 0, 0, eutil.getSheetAt(0), eutil.getWb());
            eutil.cellRange(0, 0, 1, 6, eutil.getSheetAt(0), eutil.getWb());
            eutil.cellRange(0, 0, 7, 12, eutil.getSheetAt(0), eutil.getWb());
            eutil.cellRange(0, 0, 13, 18, eutil.getSheetAt(0), eutil.getWb());
            eutil.cellRange(0, 0, 19, 24, eutil.getSheetAt(0), eutil.getWb());
            // 设置列宽
            eutil.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(8, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(9, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(10, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(11, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(12, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(13, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(14, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(15, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(16, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(17, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(18, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(19, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(20, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(21, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(22, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(23, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(24, 20 * 256);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            for (int i = 0; i < list.size(); i++) {
                bean = list.get(i);
                row++;
                // 插入数据
                eutil.writeStr(sheet, row, "A", center, bean.getInputdate() == null ? "" :
                        sdf.format(bean.getInputdate()));
                eutil.writeStr(sheet, row, "B", center, bean.getTotalFacevalue());
                eutil.writeStr(sheet, row, "C", center, bean.getTotalInvestIncome());
                eutil.writeStr(sheet, row, "D", center, bean.getTotalWeightRate());
                eutil.writeStr(sheet, row, "E", center, bean.getTotalDuration());
                eutil.writeStr(sheet, row, "F", center, bean.getTotalDv01());
                eutil.writeStr(sheet, row, "G", center, bean.getTotalGainsLosses());
                eutil.writeStr(sheet, row, "H", center, bean.gethFacevalue());
                eutil.writeStr(sheet, row, "I", center, bean.gethInvestIncome());
                eutil.writeStr(sheet, row, "J", center, bean.gethWeightRate());
                eutil.writeStr(sheet, row, "K", center, bean.gethDuration());
                eutil.writeStr(sheet, row, "L", center, bean.gethDv01());
                eutil.writeStr(sheet, row, "M", center, bean.gethGainsLosses());
                eutil.writeStr(sheet, row, "N", center, bean.getaFacevalue());
                eutil.writeStr(sheet, row, "O", center, bean.getaInvestIncome());
                eutil.writeStr(sheet, row, "P", center, bean.getaWeightRate());
                eutil.writeStr(sheet, row, "Q", center, bean.getaDuration());
                eutil.writeStr(sheet, row, "R", center, bean.getaDv01());
                eutil.writeStr(sheet, row, "S", center, bean.getaGainsLosses());
                eutil.writeStr(sheet, row, "T", center, bean.gettFacevalue());
                eutil.writeStr(sheet, row, "U", center, bean.gettInvestIncome());
                eutil.writeStr(sheet, row, "V", center, bean.gettWeightRate());
                eutil.writeStr(sheet, row, "W", center, bean.gettDuration());
                eutil.writeStr(sheet, row, "X", center, bean.gettDv01());
                eutil.writeStr(sheet, row, "Y", center, bean.gettGainsLosses());
            }
            return eutil;
        } catch (Exception e) {
            logger.info(e);
            throw new RException(e);
        }
    }

    /**
     * 导出债券持仓盈亏Excel
     */
    @ResponseBody
    @RequestMapping(value = "/exportZqccykExcel")
    public void exportZqccykExcel(HttpServletRequest request, HttpServletResponse response) {
        String ua = request.getHeader("User-Agent");
        String filename = "债券持仓盈亏统计表.xlsx"; // 文件名
        String encode_filename = "";
        try {
            encode_filename = URLEncoder.encode(filename, "utf-8");
        } catch (UnsupportedEncodingException e1) {
            throw new RException(e1.getMessage());
        }
        response.setContentType("application/vnd.ms-excel");
        if (ua != null && ua.indexOf("Firefox/") >= 0) {
            response.setHeader("Content-Disposition", "attachment; filename*=" + encode_filename);
        } else {
            response.setHeader("Content-Disposition", "attachment; filename=" + encode_filename);
        }
        response.setHeader("Connection", "close");
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            ExcelUtil e = downloadZqccykExcel(map);
            OutputStream ouputStream = response.getOutputStream();
            e.getWb().write(ouputStream);
            ouputStream.flush();
            ouputStream.close();
        } catch (Exception e) {
            logger.info(e);
            throw new RException(e);
        }
    }

    ;

    /**
     * 债券持仓盈亏Excel文件下载内容
     */
    @SuppressWarnings("static-access")
    public ExcelUtil downloadZqccykExcel(Map<String, Object> map) throws Exception {
        SlSdCdProfitAndLossBean bean = new SlSdCdProfitAndLossBean();
        List<SlSdCdProfitAndLossBean> list = baseServer.getSdProfitAndLoss(map); // 查数据
        ExcelUtil eutil = null;
        try {
            // 表头
            eutil = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
            CellStyle center = eutil.getDefaultCenterStrCellStyle();
            int sheet = 0;
            int row = 0;
            // 表格sheet名称
            eutil.getWb().createSheet("债券持仓盈亏统计表");
            // 设置表头字段名
            eutil.writeStr(sheet, row, "A", center, "会计分类");
            eutil.writeStr(sheet, row, "B", center, "债券分类");
            eutil.writeStr(sheet, row, "C", center, "名义本金(亿)");
            eutil.writeStr(sheet, row, "D", center, "名义本金(亿)");
            eutil.writeStr(sheet, row, "E", center, "账面价值(亿)");
            eutil.writeStr(sheet, row, "F", center, "账面价值(亿)");
            eutil.writeStr(sheet, row, "G", center, "久期");
            eutil.writeStr(sheet, row, "H", center, "久期");
            eutil.writeStr(sheet, row, "I", center, "持有到期收益率%");
            eutil.writeStr(sheet, row, "J", center, "持有到期收益率%");
            eutil.writeStr(sheet, row, "K", center, "估值损益");
            eutil.writeStr(sheet, row, "L", center, "估值损益");
            eutil.writeStr(sheet, row, "M", center, "日损益");
            eutil.writeStr(sheet, row, "N", center, "月损益");
            eutil.writeStr(sheet, row, "O", center, "年损益");
            row = row + 1;
            eutil.writeStr(sheet, row, "A", center, "会计分类");
            eutil.writeStr(sheet, row, "B", center, "债券分类");
            eutil.writeStr(sheet, row, "C", center, "当前");
            eutil.writeStr(sheet, row, "D", center, "年初");
            eutil.writeStr(sheet, row, "E", center, "当前");
            eutil.writeStr(sheet, row, "F", center, "年初");
            eutil.writeStr(sheet, row, "G", center, "当前");
            eutil.writeStr(sheet, row, "H", center, "年初");
            eutil.writeStr(sheet, row, "I", center, "当前");
            eutil.writeStr(sheet, row, "J", center, "年初");
            eutil.writeStr(sheet, row, "K", center, "当前");
            eutil.writeStr(sheet, row, "L", center, "年初");
            eutil.writeStr(sheet, row, "M", center, "日损益");
            eutil.writeStr(sheet, row, "N", center, "月损益");
            eutil.writeStr(sheet, row, "O", center, "年损益");
            // 合并单元格
            eutil.cellRange(0, 1, 0, 0, eutil.getSheetAt(0), eutil.getWb());
            eutil.cellRange(0, 1, 1, 1, eutil.getSheetAt(0), eutil.getWb());
            eutil.cellRange(0, 0, 2, 3, eutil.getSheetAt(0), eutil.getWb());
            eutil.cellRange(0, 0, 4, 5, eutil.getSheetAt(0), eutil.getWb());
            eutil.cellRange(0, 0, 6, 7, eutil.getSheetAt(0), eutil.getWb());
            eutil.cellRange(0, 0, 8, 9, eutil.getSheetAt(0), eutil.getWb());
            eutil.cellRange(0, 0, 10, 11, eutil.getSheetAt(0), eutil.getWb());
            eutil.cellRange(0, 1, 12, 12, eutil.getSheetAt(0), eutil.getWb());
            eutil.cellRange(0, 1, 13, 13, eutil.getSheetAt(0), eutil.getWb());
            eutil.cellRange(0, 1, 14, 14, eutil.getSheetAt(0), eutil.getWb());
            // 设置列宽
            eutil.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(8, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(9, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(10, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(11, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(12, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(13, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(14, 20 * 256);
            for (int i = 0; i < list.size(); i++) {
                bean = list.get(i);
                row++;
                // 插入数据
                eutil.writeStr(sheet, row, "A", center, bean.getInvtype() != null && !"".equals(bean.getInvtype()) ? bean.getInvtype() : "总计");
                eutil.writeStr(sheet, row, "B", center, bean.getAcctngtype() != null && !"".equals(bean.getAcctngtype()) ? getAcctngtype(bean.getAcctngtype()) : "小计");
                eutil.writeStr(sheet, row, "C", center, bean.getPrinamt());
                eutil.writeStr(sheet, row, "D", center, bean.getYearprinamt());
                eutil.writeStr(sheet, row, "E", center, bean.getTdbookvalue());
                eutil.writeStr(sheet, row, "F", center, bean.getYearbookvalue());
                eutil.writeStr(sheet, row, "G", center, bean.getDuration());
                eutil.writeStr(sheet, row, "H", center, bean.getYearduation());
                eutil.writeStr(sheet, row, "I", center, bean.getRate());
                eutil.writeStr(sheet, row, "J", center, bean.getYearrate());
                eutil.writeStr(sheet, row, "K", center, bean.getDiffamt());
                eutil.writeStr(sheet, row, "L", center, bean.getYeardiffamt());
                eutil.writeStr(sheet, row, "M", center, bean.getDayprofitloss());
                eutil.writeStr(sheet, row, "N", center, bean.getMonthprofitloss());
                eutil.writeStr(sheet, row, "O", center, bean.getYearprofitloss());
            }
            return eutil;
        } catch (Exception e) {
            logger.info(e);
            throw new RException(e);
        }
    }

    /**
     * 根据英文ID获取债券分类中文名
     *
     * @param acctngtype
     * @return
     */
    private String getAcctngtype(String acctngtype) {
        String typeName = "";
        if ("GZ".equals(acctngtype)) {
            typeName = "国债";
        } else if ("ZCXYH".equals(acctngtype)) {
            typeName = "政策性银行债";
        } else if ("QTJRJG".equals(acctngtype)) {
            typeName = "其他金融债";
        } else if ("DFZFZ".equals(acctngtype)) {
            typeName = "地方政府债";
        } else if ("YP".equals(acctngtype)) {
            typeName = "央票";
        } else if ("JRZ".equals(acctngtype)) {
            typeName = "金融债";
        } else if ("DR".equals(acctngtype)) {
            typeName = "短期融资券";
        } else if ("QYZ".equals(acctngtype)) {
            typeName = "企业债";
        } else if ("GSZ".equals(acctngtype)) {
            typeName = "公司债";
        } else if ("ZP".equals(acctngtype)) {
            typeName = "中票";
        }
        return typeName;
    }

    /**
     * 导出债券投资限额Excel
     */
    @ResponseBody
    @RequestMapping(value = "/exportZqtzxeExcel")
    public void exportZqtzxeExcel(HttpServletRequest request, HttpServletResponse response) {
        String ua = request.getHeader("User-Agent");
        String filename = "债券投资业务限管理执行表.xlsx"; // 文件名
        String encode_filename = "";
        try {
            encode_filename = URLEncoder.encode(filename, "utf-8");
        } catch (UnsupportedEncodingException e1) {
            throw new RException(e1.getMessage());
        }
        response.setContentType("application/vnd.ms-excel");
        if (ua != null && ua.indexOf("Firefox/") >= 0) {
            response.setHeader("Content-Disposition", "attachment; filename*=" + encode_filename);
        } else {
            response.setHeader("Content-Disposition", "attachment; filename=" + encode_filename);
        }
        response.setHeader("Connection", "close");
        try {
            String oprDate = request.getParameter("oprDate"); // 限额日期
            String reportDetail = request.getParameter("reportDetail"); // 报表详情
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("oprDate", oprDate);
            map.put("reportDetail", reportDetail);
            ExcelUtil e = downloadZqtzxeExcel(map);
            OutputStream ouputStream = response.getOutputStream();
            e.getWb().write(ouputStream);
            ouputStream.flush();
            ouputStream.close();
        } catch (Exception e) {
            logger.info(e);
            throw new RException(e);
        }
    }

    ;

    /**
     * 债券投资限额Excel文件下载内容
     */
    @SuppressWarnings("static-access")
    public ExcelUtil downloadZqtzxeExcel(Map<String, Object> map) throws Exception {
		/*InvestLimtlog bean = new InvestLimtlog();
		map.put("bondFlag", "1");
		String oprDate = ParameterUtil.getString(map, "oprDate", "");
		if ("".equals(oprDate) || null == oprDate) {
			oprDate = ifsOpicsSettleManageMapper.getCurDate();
		}
		map.put("oprDate", oprDate.substring(0, 10));
		Page<InvestLimtlog> list = ifsLimitService.getBondInvestmentLimit(map); // 查数据
*/
        String reportDetail = ParameterUtil.getString(map, "reportDetail", "");//获取页面传来数据
        String[] array = reportDetail.split(",");

        ExcelUtil eutil = null;
        try {
            // 表头
            eutil = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
            CellStyle center = eutil.getDefaultAllCenterStrCellStyle();
            CellStyle left = eutil.getDefaultLeftStrCellStyle();//字体左对齐
            int sheet = 0;
            int row = 0;
            // 表格sheet名称
            eutil.getWb().createSheet("债券投资业务限管理执行表");
            System.out.println(array);

            eutil.writeStr(sheet, row, "A", center, "账户类型");
            eutil.writeStr(sheet, row, "B", center, "债券限额类型");
            eutil.writeStr(sheet, row, "C", center, "限额指标");
            eutil.writeStr(sheet, row, "D", center, "当前值(元)");
            //交易账户
            row = row + 1;
            eutil.writeStr(sheet, row, "A", center, "交易账户");
            eutil.writeStr(sheet, row, "B", center, "交易限额");
            eutil.writeStr(sheet, row, "C", left, "交易账户投资限额");
            eutil.writeStr(sheet, row, "D", left, array[0]);
            row = row + 1;
            eutil.writeStr(sheet, row, "A", center, "交易账户");
            eutil.writeStr(sheet, row, "B", center, "交易限额");
            eutil.writeStr(sheet, row, "C", left, "交易账户信用债投资限额");
            eutil.writeStr(sheet, row, "D", left, array[1]);
            row = row + 1;
            eutil.writeStr(sheet, row, "A", center, "交易账户");
            eutil.writeStr(sheet, row, "B", center, "交易限额");
            eutil.writeStr(sheet, row, "C", left, "交易账户信用债AA级及以下债券余额");
            eutil.writeStr(sheet, row, "D", left, array[2]);
            row = row + 1;
            eutil.writeStr(sheet, row, "A", center, "交易账户");
            eutil.writeStr(sheet, row, "B", center, "风险限额");
            eutil.writeStr(sheet, row, "C", left, "交易账户债券久期限额");
            eutil.writeStr(sheet, row, "D", left, array[3]);
            row = row + 1;
            eutil.writeStr(sheet, row, "A", center, "交易账户");
            eutil.writeStr(sheet, row, "B", center, "风险限额");
            eutil.writeStr(sheet, row, "C", left, "交易账户DV01限额");
            eutil.writeStr(sheet, row, "D", left, array[4]);
            row = row + 1;
            eutil.writeStr(sheet, row, "A", center, "交易账户");
            eutil.writeStr(sheet, row, "B", center, "止损限额");
            eutil.writeStr(sheet, row, "C", left, "交易账户止损限额(年)");
            eutil.writeStr(sheet, row, "D", left, array[5]);
            row = row + 1;
            eutil.writeStr(sheet, row, "A", center, "交易账户");
            eutil.writeStr(sheet, row, "B", center, "止损限额");
            eutil.writeStr(sheet, row, "C", left, "交易账户止损限额(月)");
            eutil.writeStr(sheet, row, "D", left, array[6]);

            //可供出售账户
            row = row + 1;
            eutil.writeStr(sheet, row, "A", center, "可供出售账户");
            eutil.writeStr(sheet, row, "B", center, "交易限额");
            eutil.writeStr(sheet, row, "C", left, "可供出售账户投资限额");
            eutil.writeStr(sheet, row, "D", left, array[7]);
            row = row + 1;
            eutil.writeStr(sheet, row, "A", center, "可供出售账户");
            eutil.writeStr(sheet, row, "B", center, "交易限额");
            eutil.writeStr(sheet, row, "C", left, "可供出售账户信用债投资限额");
            eutil.writeStr(sheet, row, "D", left, array[8]);
            row = row + 1;
            eutil.writeStr(sheet, row, "A", center, "可供出售账户");
            eutil.writeStr(sheet, row, "B", center, "交易限额");
            eutil.writeStr(sheet, row, "C", left, "可供出售账户信用债AA-级及以下债券余额");
            eutil.writeStr(sheet, row, "D", left, array[9]);
            row = row + 1;
            eutil.writeStr(sheet, row, "A", center, "可供出售账户");
            eutil.writeStr(sheet, row, "B", center, "风险限额");
            eutil.writeStr(sheet, row, "C", left, "可供出售账户债券久期限额");
            eutil.writeStr(sheet, row, "D", left, array[10]);
            row = row + 1;
            eutil.writeStr(sheet, row, "A", center, "可供出售账户");
            eutil.writeStr(sheet, row, "B", center, "风险限额");
            eutil.writeStr(sheet, row, "C", left, "交易账户DV01限额");
            eutil.writeStr(sheet, row, "D", left, array[11]);
            row = row + 1;
            eutil.writeStr(sheet, row, "A", center, "可供出售账户");
            eutil.writeStr(sheet, row, "B", center, "止损限额");
            eutil.writeStr(sheet, row, "C", left, "交易账户止损限额(年)");
            eutil.writeStr(sheet, row, "D", left, array[12]);
            row = row + 1;
            eutil.writeStr(sheet, row, "A", center, "可供出售账户");
            eutil.writeStr(sheet, row, "B", center, "止损限额");
            eutil.writeStr(sheet, row, "C", left, "交易账户止损限额(月)");
            eutil.writeStr(sheet, row, "D", left, array[13]);
            //持有至到期账户
            row = row + 1;
            eutil.writeStr(sheet, row, "A", center, "持有至到期账户");
            eutil.writeStr(sheet, row, "B", center, "交易限额");
            eutil.writeStr(sheet, row, "C", left, "持有至到期账户投资限额");
            eutil.writeStr(sheet, row, "D", left, array[14]);
            row = row + 1;
            eutil.writeStr(sheet, row, "A", center, "持有至到期账户");
            eutil.writeStr(sheet, row, "B", center, "交易限额");
            eutil.writeStr(sheet, row, "C", left, "持有至到期账户信用债投资限额");
            eutil.writeStr(sheet, row, "D", left, array[15]);
            row = row + 1;
            eutil.writeStr(sheet, row, "A", center, "持有至到期账户");
            eutil.writeStr(sheet, row, "B", center, "交易限额");
            eutil.writeStr(sheet, row, "C", left, "持有至到期账户信用债AA-级及以下债券余额");
            eutil.writeStr(sheet, row, "D", left, array[16]);
            row = row + 1;
            eutil.writeStr(sheet, row, "A", center, "持有至到期账户");
            eutil.writeStr(sheet, row, "B", center, "风险限额");
            eutil.writeStr(sheet, row, "C", left, "持有至到期账户债券久期限额");
            eutil.writeStr(sheet, row, "D", left, array[17]);

            // 合并单元格
            eutil.cellRange(1, 7, 0, 0, eutil.getSheetAt(0), eutil.getWb());
            eutil.cellRange(1, 3, 1, 1, eutil.getSheetAt(0), eutil.getWb());
            eutil.cellRange(4, 5, 1, 1, eutil.getSheetAt(0), eutil.getWb());
            eutil.cellRange(6, 7, 1, 1, eutil.getSheetAt(0), eutil.getWb());

            // 合并单元格
            eutil.cellRange(8, 14, 0, 0, eutil.getSheetAt(0), eutil.getWb());
            eutil.cellRange(8, 10, 1, 1, eutil.getSheetAt(0), eutil.getWb());
            eutil.cellRange(11, 12, 1, 1, eutil.getSheetAt(0), eutil.getWb());
            eutil.cellRange(13, 14, 1, 1, eutil.getSheetAt(0), eutil.getWb());

            // 合并单元格
            eutil.cellRange(15, 18, 0, 0, eutil.getSheetAt(0), eutil.getWb());
            eutil.cellRange(15, 16, 1, 1, eutil.getSheetAt(0), eutil.getWb());
            eutil.cellRange(17, 18, 1, 1, eutil.getSheetAt(0), eutil.getWb());

            // 设置列宽
            eutil.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(2, 35 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(3, 20 * 256);

            return eutil;
        } catch (Exception e) {
            logger.info(e);
            throw new RException(e);
        }
    }

    /**
     * 导出同业存单投资业务限额管理执行Excel
     */
    @ResponseBody
    @RequestMapping(value = "/exportTyCdxeExcel")
    public void exportTyCdxeExcel(HttpServletRequest request, HttpServletResponse response) {
        String ua = request.getHeader("User-Agent");
        String filename = "同业存单投资业务限额管理执行表.xlsx"; // 文件名
        String encode_filename = "";
        try {
            encode_filename = URLEncoder.encode(filename, "utf-8");
        } catch (UnsupportedEncodingException e1) {
            throw new RException(e1.getMessage());
        }
        response.setContentType("application/vnd.ms-excel");
        if (ua != null && ua.indexOf("Firefox/") >= 0) {
            response.setHeader("Content-Disposition", "attachment; filename*=" + encode_filename);
        } else {
            response.setHeader("Content-Disposition", "attachment; filename=" + encode_filename);
        }
        response.setHeader("Connection", "close");
        try {
            String oprDate = request.getParameter("oprDate"); // 限额日期
            String reportDetail = request.getParameter("reportDetail"); // 报表详情
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("oprDate", oprDate);
            map.put("reportDetail", reportDetail);
            ExcelUtil e = downloadTyCdxeExcel(map);
            OutputStream ouputStream = response.getOutputStream();
            e.getWb().write(ouputStream);
            ouputStream.flush();
            ouputStream.close();
        } catch (Exception e) {
            logger.info(e);
            throw new RException(e);
        }
    }

    ;

    /**
     * 同业存单投资限额Excel文件下载内容
     */
    @SuppressWarnings("static-access")
    public ExcelUtil downloadTyCdxeExcel(Map<String, Object> map) throws Exception {
		/*InvestLimtlog bean = new InvestLimtlog();
		map.put("bondFlag", "2");
		String oprDate = ParameterUtil.getString(map, "oprDate", "");
		if ("".equals(oprDate) || null == oprDate) {
			oprDate = ifsOpicsSettleManageMapper.getCurDate();
		}
		map.put("oprDate", oprDate.substring(0, 10));
		Page<InvestLimtlog> list = ifsLimitService.getBondInvestmentLimit(map); // 查数据
*/
        String reportDetail = ParameterUtil.getString(map, "reportDetail", "");//获取页面传来数据
        String[] array = reportDetail.split(",");

        ExcelUtil eutil = null;
        try {
            // 表头
            eutil = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
            CellStyle center = eutil.getDefaultAllCenterStrCellStyle();
            CellStyle left = eutil.getDefaultLeftStrCellStyle();//字体左对齐
            int sheet = 0;
            int row = 0;
            // 表格sheet名称
            eutil.getWb().createSheet("同业存单投资业务限额管理执行表");
            System.out.println(array);

            eutil.writeStr(sheet, row, "A", center, "账户类型");
            eutil.writeStr(sheet, row, "B", center, "同业存单限额类型");
            eutil.writeStr(sheet, row, "C", center, "限额指标");
            eutil.writeStr(sheet, row, "D", center, "当前值(元)");
            //交易账户
            row = row + 1;
            eutil.writeStr(sheet, row, "A", center, "交易账户");
            eutil.writeStr(sheet, row, "B", center, "交易限额");
            eutil.writeStr(sheet, row, "C", left, "交易账户投资限额");
            eutil.writeStr(sheet, row, "D", left, array[0]);
            row = row + 1;
            eutil.writeStr(sheet, row, "A", center, "交易账户");
            eutil.writeStr(sheet, row, "B", center, "交易限额");
            eutil.writeStr(sheet, row, "C", left, "交易账户信用债AA级及以下债券余额");
            eutil.writeStr(sheet, row, "D", left, array[1]);
            row = row + 1;
            eutil.writeStr(sheet, row, "A", center, "交易账户");
            eutil.writeStr(sheet, row, "B", center, "风险限额");
            eutil.writeStr(sheet, row, "C", left, "交易账户债券久期限额");
            eutil.writeStr(sheet, row, "D", left, array[2]);
            row = row + 1;
            eutil.writeStr(sheet, row, "A", center, "交易账户");
            eutil.writeStr(sheet, row, "B", center, "风险限额");
            eutil.writeStr(sheet, row, "C", left, "交易账户DV01限额");
            eutil.writeStr(sheet, row, "D", left, array[3]);
            row = row + 1;
            eutil.writeStr(sheet, row, "A", center, "交易账户");
            eutil.writeStr(sheet, row, "B", center, "止损限额");
            eutil.writeStr(sheet, row, "C", left, "交易账户止损限额(年)");
            eutil.writeStr(sheet, row, "D", left, array[4]);
            row = row + 1;
            eutil.writeStr(sheet, row, "A", center, "交易账户");
            eutil.writeStr(sheet, row, "B", center, "止损限额");
            eutil.writeStr(sheet, row, "C", left, "交易账户止损限额(月)");
            eutil.writeStr(sheet, row, "D", left, array[5]);

            //可供出售账户
            row = row + 1;
            eutil.writeStr(sheet, row, "A", center, "可供出售账户");
            eutil.writeStr(sheet, row, "B", center, "交易限额");
            eutil.writeStr(sheet, row, "C", left, "可供出售账户投资限额");
            eutil.writeStr(sheet, row, "D", left, array[6]);
            row = row + 1;
            eutil.writeStr(sheet, row, "A", center, "可供出售账户");
            eutil.writeStr(sheet, row, "B", center, "交易限额");
            eutil.writeStr(sheet, row, "C", left, "可供出售账户信用债AA-级及以下债券余额");
            eutil.writeStr(sheet, row, "D", left, array[7]);
            row = row + 1;
            eutil.writeStr(sheet, row, "A", center, "可供出售账户");
            eutil.writeStr(sheet, row, "B", center, "风险限额");
            eutil.writeStr(sheet, row, "C", left, "可供出售账户债券久期限额");
            eutil.writeStr(sheet, row, "D", left, array[8]);
            row = row + 1;
            eutil.writeStr(sheet, row, "A", center, "可供出售账户");
            eutil.writeStr(sheet, row, "B", center, "风险限额");
            eutil.writeStr(sheet, row, "C", left, "交易账户DV01限额");
            eutil.writeStr(sheet, row, "D", left, array[9]);
            row = row + 1;
            eutil.writeStr(sheet, row, "A", center, "可供出售账户");
            eutil.writeStr(sheet, row, "B", center, "止损限额");
            eutil.writeStr(sheet, row, "C", left, "交易账户止损限额(年)");
            eutil.writeStr(sheet, row, "D", left, array[10]);
            row = row + 1;
            eutil.writeStr(sheet, row, "A", center, "可供出售账户");
            eutil.writeStr(sheet, row, "B", center, "止损限额");
            eutil.writeStr(sheet, row, "C", left, "交易账户止损限额(月)");
            eutil.writeStr(sheet, row, "D", left, array[11]);
            //持有至到期账户
            row = row + 1;
            eutil.writeStr(sheet, row, "A", center, "持有至到期账户");
            eutil.writeStr(sheet, row, "B", center, "交易限额");
            eutil.writeStr(sheet, row, "C", left, "持有至到期账户投资限额");
            eutil.writeStr(sheet, row, "D", left, array[12]);
            row = row + 1;
            eutil.writeStr(sheet, row, "A", center, "持有至到期账户");
            eutil.writeStr(sheet, row, "B", center, "交易限额");
            eutil.writeStr(sheet, row, "C", left, "持有至到期账户信用债AA-级及以下债券余额");
            eutil.writeStr(sheet, row, "D", left, array[13]);
            row = row + 1;
            eutil.writeStr(sheet, row, "A", center, "持有至到期账户");
            eutil.writeStr(sheet, row, "B", center, "风险限额");
            eutil.writeStr(sheet, row, "C", left, "持有至到期账户债券久期限额");
            eutil.writeStr(sheet, row, "D", left, array[14]);

            // 合并单元格
            eutil.cellRange(1, 6, 0, 0, eutil.getSheetAt(0), eutil.getWb());
            eutil.cellRange(1, 2, 1, 1, eutil.getSheetAt(0), eutil.getWb());
            eutil.cellRange(3, 4, 1, 1, eutil.getSheetAt(0), eutil.getWb());
            eutil.cellRange(5, 6, 1, 1, eutil.getSheetAt(0), eutil.getWb());

            // 合并单元格
            eutil.cellRange(7, 12, 0, 0, eutil.getSheetAt(0), eutil.getWb());
            eutil.cellRange(7, 8, 1, 1, eutil.getSheetAt(0), eutil.getWb());
            eutil.cellRange(9, 10, 1, 1, eutil.getSheetAt(0), eutil.getWb());
            eutil.cellRange(11, 12, 1, 1, eutil.getSheetAt(0), eutil.getWb());

            // 合并单元格
            eutil.cellRange(13, 15, 0, 0, eutil.getSheetAt(0), eutil.getWb());
            eutil.cellRange(13, 14, 1, 1, eutil.getSheetAt(0), eutil.getWb());

            // 设置列宽
            eutil.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(2, 35 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(3, 20 * 256);

            return eutil;
        } catch (Exception e) {
            logger.info(e);
            throw new RException(e);
        }
    }

    /**
     * 外币限额Excel
     */
    @ResponseBody
    @RequestMapping(value = "/exportWbxeExcel")
    public void exportWbxeExcel(HttpServletRequest request, HttpServletResponse response) {
        String ua = request.getHeader("User-Agent");
        String filename = "外币限额表.xlsx"; // 文件名
        String encode_filename = "";
        try {
            encode_filename = URLEncoder.encode(filename, "utf-8");
        } catch (UnsupportedEncodingException e1) {
            throw new RException(e1.getMessage());
        }
        response.setContentType("application/vnd.ms-excel");
        if (ua != null && ua.indexOf("Firefox/") >= 0) {
            response.setHeader("Content-Disposition", "attachment; filename*=" + encode_filename);
        } else {
            response.setHeader("Content-Disposition", "attachment; filename=" + encode_filename);
        }
        response.setHeader("Connection", "close");
        try {
            String oprDate = request.getParameter("oprDate"); // 限额日期
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("oprDate", oprDate);
            ExcelUtil e = downloadWbxeExcel(map);
            OutputStream ouputStream = response.getOutputStream();
            e.getWb().write(ouputStream);
            ouputStream.flush();
            ouputStream.close();
        } catch (Exception e) {
            logger.info(e);
            throw new RException(e);
        }
    }

    ;

    /**
     * 外币限额Excel文件下载内容
     */
    public ExcelUtil downloadWbxeExcel(Map<String, Object> map) throws Exception {
        ForeignLimitLog bean = new ForeignLimitLog();
        String oprDate = ParameterUtil.getString(map, "oprDate", "");
        if ("".equals(oprDate) || null == oprDate) {
            oprDate = ifsOpicsSettleManageMapper.getCurDate();
        }
        map.put("oprDate", oprDate.substring(0, 10));
        Page<ForeignLimitLog> list = ifsLimitService.getForeignLimit(map); // 查数据
        ExcelUtil eutil = null;
        try {
            // 表头
            eutil = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
            CellStyle center = eutil.getDefaultCenterStrCellStyle();
            int sheet = 0;
            int row = 0;
            // 表格sheet名称
            eutil.getWb().createSheet("外币限额表");
            // 设置表头字段名
            eutil.writeStr(sheet, row, "A", center, "限额类型");
            eutil.writeStr(sheet, row, "B", center, "USD");
            eutil.writeStr(sheet, row, "C", center, "EUR");
            eutil.writeStr(sheet, row, "D", center, "JPY");
            eutil.writeStr(sheet, row, "E", center, "HKD");
            eutil.writeStr(sheet, row, "F", center, "GBP");
            eutil.writeStr(sheet, row, "G", center, "总计");
            // 设置列宽
            eutil.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
            for (int i = 0; i < list.size(); i++) {
                bean = list.get(i);
                row++;
                // 插入数据
                eutil.writeStr(sheet, row, "A", center, bean.getForeignLimitType());
                eutil.writeStr(sheet, row, "B", center, bean.getUsd());
                eutil.writeStr(sheet, row, "C", center, bean.getEur());
                eutil.writeStr(sheet, row, "D", center, bean.getJpy());
                eutil.writeStr(sheet, row, "E", center, bean.getHkd());
                eutil.writeStr(sheet, row, "F", center, bean.getGbp());
                eutil.writeStr(sheet, row, "G", center, bean.getTotal());
            }
            return eutil;
        } catch (Exception e) {
            logger.info(e);
            throw new RException(e);
        }
    }

    /**
     * 导出存单发行每日余额Excel
     */
    @ResponseBody
    @RequestMapping(value = "/exportCdIssueDayBalance")
    public void exportCdIssueDayBalance(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String ua = request.getHeader("User-Agent");
        String secid = request.getParameter("secid");
        String datadate = request.getParameter("datadate");
        String filename = "同业存单发行每日余额报表.xlsx"; // 文件名
        String encode_filename = "";
        try {
            encode_filename = URLEncoder.encode(filename, "utf-8");
        } catch (UnsupportedEncodingException e1) {
            throw new RException(e1.getMessage());
        }
        response.setContentType("application/vnd.ms-excel");
        if (ua != null) {
            response.setHeader("Content-Disposition", "attachment; filename=" + encode_filename);
        }
        response.setHeader("Connection", "close");
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("secid", secid);
            map.put("datadate", datadate);
            ExcelUtil e = downloadCdIssueDayBalanceExcel(map);
            OutputStream ouputStream = response.getOutputStream();
            e.getWb().write(ouputStream);
            ouputStream.flush();
            ouputStream.close();
        } catch (Exception e) {
            throw new RException(e);
        }
    }

    /**
     * 存单发行每日余额Excel文件下载内容
     */
    public ExcelUtil downloadCdIssueDayBalanceExcel(Map<String, Object> map) throws Exception {
        ExcelUtil excel = null;
        try {
            /* 先查数据，再设置表格属性以及内容 */
            Map<String, String> opicsMap = new HashMap<String, String>();
            opicsMap.put("br", "01");
            String opicsdate = acupServer.getAcupDate(opicsMap);
            String datadate = (String) map.get("datadate");
            if (datadate == null || "".equals(datadate)) {
                map.put("datadate", opicsdate);
            }
            List<SlCdIssueBalanceBean> list = baseServer.getCdIssueBalanceExport(map);
            List<SlCdIssueBalanceBean> newList = new ArrayList<SlCdIssueBalanceBean>();
            for (SlCdIssueBalanceBean slCdIssueBalanceBean : list) {
                SlCdIssueBalanceBean bean = new SlCdIssueBalanceBean();
                bean = slCdIssueBalanceBean;
                if (StringUtil.isNotEmpty(slCdIssueBalanceBean.getSubname())) {
                    IfsOpicsCust cust = ifsOpicsCustService.searchIfsOpicsCust(slCdIssueBalanceBean.getSubname().trim());
                    if (cust != null) {
                        if (StringUtil.isNotEmpty(cust.getCliname())) {
                            bean.setSubname(cust.getCliname());
                        }
                    }
                }
                if (StringUtil.isNotEmpty(slCdIssueBalanceBean.getSecid())) {
                    IfsOpicsBond bond = ifsOpicsBondService.searchOneById(slCdIssueBalanceBean.getSecid().trim());
                    if (bond != null) {
                        bean.setSecidnm(bond.getBndnm_en());
                    }
                }
				/*if(StringUtil.isNotEmpty(slCdIssueBalanceBean.getInsttype())){
					IfsOpicsActy acty = ifsOpicsActyService.searchById(slCdIssueBalanceBean.getInsttype().trim());
					if (acty != null) {
						if (StringUtil.isNotEmpty(acty.getAcctdesccn())) {
							bean.setInsttype(acty.getAcctdesccn());
						}
					}
				}*/

                newList.add(bean);
            }
            SlCdIssueBalanceBean cdBean = new SlCdIssueBalanceBean();
            // 表头
            excel = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
            CellStyle center = excel.getDefaultCenterStrCellStyle();
            int sheet = 0;
            int row = 0;
            // 表格sheet名称
            excel.getWb().createSheet("同业存单发行每日余额报表");
            // 设置表头字段名
            excel.writeStr(sheet, row, "A", center, "数据日期");
            excel.writeStr(sheet, row, "B", center, "机构号");
            excel.writeStr(sheet, row, "C", center, "存单代码");
            excel.writeStr(sheet, row, "D", center, "存单名称");
            excel.writeStr(sheet, row, "E", center, "认购人名称");
            excel.writeStr(sheet, row, "F", center, "所属账户分类");
            excel.writeStr(sheet, row, "G", center, "核心科目号");
            excel.writeStr(sheet, row, "H", center, "产品性质");
            excel.writeStr(sheet, row, "I", center, "产品分类");
            excel.writeStr(sheet, row, "J", center, "账面余额");
            excel.writeStr(sheet, row, "K", center, "起息日期");
            excel.writeStr(sheet, row, "L", center, "到期日期");
            excel.writeStr(sheet, row, "M", center, "面值");
            excel.writeStr(sheet, row, "N", center, "利息调整");
            // 设置列宽
            excel.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(8, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(9, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(10, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(11, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(12, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(13, 20 * 256);
            for (int i = 0; i < newList.size(); i++) {
                cdBean = newList.get(i);
                row++;
                // 插入数据
                DecimalFormat df = new DecimalFormat("#,##0.0000");
                excel.writeStr(sheet, row, "A", center, cdBean.getDatadate() != null ? cdBean.getDatadate().substring(0, 10) : cdBean.getDatadate());
                excel.writeStr(sheet, row, "B", center, cdBean.getInst());
                excel.writeStr(sheet, row, "C", center, cdBean.getSecid());
                excel.writeStr(sheet, row, "D", center, cdBean.getSecidnm());
                excel.writeStr(sheet, row, "E", center, cdBean.getSubname());
                excel.writeStr(sheet, row, "F", center, cdBean.getInsttype());
                excel.writeStr(sheet, row, "G", center, cdBean.getSubjectno());
                excel.writeStr(sheet, row, "H", center, cdBean.getProdtype());
                excel.writeStr(sheet, row, "I", center, cdBean.getProduct());
                excel.writeStr(sheet, row, "J", center, cdBean.getBookbalance() != null && !"".equals(cdBean.getBookbalance()) ? df.format(Double.valueOf(cdBean.getBookbalance()))
                        : cdBean.getBookbalance());
                excel.writeStr(sheet, row, "K", center, cdBean.getVdate() != null ? cdBean.getVdate().substring(0, 10) : cdBean.getVdate());
                excel.writeStr(sheet, row, "L", center, cdBean.getMdate() != null ? cdBean.getMdate().substring(0, 10) : cdBean.getMdate());
                excel.writeStr(sheet, row, "M", center, cdBean.getFacevalue() != null && !"".equals(cdBean.getFacevalue()) ? df.format(Double.valueOf(cdBean.getFacevalue()))
                        : cdBean.getFacevalue());
                excel.writeStr(sheet, row, "N", center, cdBean.getInterestadj() != null && !"".equals(cdBean.getInterestadj()) ? df.format(Double.valueOf(cdBean.getInterestadj()))
                        : cdBean.getInterestadj());
            }
            return excel;
        } catch (Exception e) {
            throw new RException(e);
        }
    }


    /**
     * 导出存单持仓盈亏统计表Excel
     */
    @ResponseBody
    @RequestMapping(value = "/exportCdProfitLoss")
    public void exportCdProfitLoss(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String ua = request.getHeader("User-Agent");
        String filename = "存单持仓盈亏统计报表.xlsx"; // 文件名
        String encode_filename = "";
        try {
            encode_filename = URLEncoder.encode(filename, "utf-8");
        } catch (UnsupportedEncodingException e1) {
            throw new RException(e1.getMessage());
        }
        response.setContentType("application/vnd.ms-excel");
        if (ua != null) {
            response.setHeader("Content-Disposition", "attachment; filename=" + encode_filename);
        }
        response.setHeader("Connection", "close");
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            ExcelUtil e = downloadCdProfitLossExcel(map);
            OutputStream ouputStream = response.getOutputStream();
            e.getWb().write(ouputStream);
            ouputStream.flush();
            ouputStream.close();
        } catch (Exception e) {
            throw new RException(e);
        }
    }

    /**
     * 存单持仓盈亏统计表Excel文件下载内容
     */
    @SuppressWarnings("static-access")
    public ExcelUtil downloadCdProfitLossExcel(Map<String, Object> map) throws Exception {
        ExcelUtil excel = null;
        try {
            /* 先查数据，再设置表格属性以及内容 */
            List<SlSdCdProfitAndLossBean> list = baseServer.getCdProfitAndLoss(map);
            SlSdCdProfitAndLossBean cdBean = new SlSdCdProfitAndLossBean();
            // 表头
            excel = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
            CellStyle center = excel.getDefaultCenterStrCellStyle();
            int sheet = 0;
            int row = 0;
            // 表格sheet名称
            excel.getWb().createSheet("存单持仓盈亏统计表");
            // 设置表头字段名
            excel.writeStr(sheet, row, "A", center, "会计分类");
            excel.writeStr(sheet, row, "B", center, "债券分类");
            excel.writeStr(sheet, row, "C", center, "名义本金(亿)");
            excel.writeStr(sheet, row, "D", center, "名义本金(亿)");
            excel.writeStr(sheet, row, "E", center, "账面价值(亿)");
            excel.writeStr(sheet, row, "F", center, "账面价值(亿)");
            excel.writeStr(sheet, row, "G", center, "久期");
            excel.writeStr(sheet, row, "H", center, "久期");
            excel.writeStr(sheet, row, "I", center, "持有到期收益率%");
            excel.writeStr(sheet, row, "J", center, "持有到期收益率%");
            excel.writeStr(sheet, row, "K", center, "估值损益");
            excel.writeStr(sheet, row, "L", center, "估值损益");
            excel.writeStr(sheet, row, "M", center, "日损益");
            excel.writeStr(sheet, row, "N", center, "月损益");
            excel.writeStr(sheet, row, "O", center, "年损益");
            row = row + 1;
            excel.writeStr(sheet, row, "A", center, "会计分类");
            excel.writeStr(sheet, row, "B", center, "债券分类");
            excel.writeStr(sheet, row, "C", center, "当前");
            excel.writeStr(sheet, row, "D", center, "年初");
            excel.writeStr(sheet, row, "E", center, "当前");
            excel.writeStr(sheet, row, "F", center, "年初");
            excel.writeStr(sheet, row, "G", center, "当前");
            excel.writeStr(sheet, row, "H", center, "年初");
            excel.writeStr(sheet, row, "I", center, "当前");
            excel.writeStr(sheet, row, "J", center, "年初");
            excel.writeStr(sheet, row, "K", center, "当前");
            excel.writeStr(sheet, row, "L", center, "年初");
            excel.writeStr(sheet, row, "M", center, "日损益");
            excel.writeStr(sheet, row, "N", center, "月损益");
            excel.writeStr(sheet, row, "O", center, "年损益");
            // 合并单元格
            excel.cellRange(0, 1, 0, 0, excel.getSheetAt(0), excel.getWb());
            excel.cellRange(0, 1, 1, 1, excel.getSheetAt(0), excel.getWb());
            excel.cellRange(0, 0, 2, 3, excel.getSheetAt(0), excel.getWb());
            excel.cellRange(0, 0, 4, 5, excel.getSheetAt(0), excel.getWb());
            excel.cellRange(0, 0, 6, 7, excel.getSheetAt(0), excel.getWb());
            excel.cellRange(0, 0, 8, 9, excel.getSheetAt(0), excel.getWb());
            excel.cellRange(0, 0, 10, 11, excel.getSheetAt(0), excel.getWb());
            excel.cellRange(0, 1, 12, 12, excel.getSheetAt(0), excel.getWb());
            excel.cellRange(0, 1, 13, 13, excel.getSheetAt(0), excel.getWb());
            excel.cellRange(0, 1, 14, 14, excel.getSheetAt(0), excel.getWb());
            // 设置列宽
            excel.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(8, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(9, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(10, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(11, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(12, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(13, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(14, 20 * 256);
            for (int i = 0; i < list.size(); i++) {
                cdBean = list.get(i);
                row++;
                // 插入数据
                DecimalFormat df = new DecimalFormat("#,##0.0000");
                excel.writeStr(sheet, row, "A", center, cdBean.getInvtype() != null && !"".equals(cdBean.getInvtype()) ? cdBean.getInvtype() : "合计");
                excel.writeStr(sheet, row, "B", center, cdBean.getAcctngtype());
                excel.writeStr(sheet, row, "C", center, cdBean.getPrinamt() != null && !"".equals(cdBean.getPrinamt()) ? df.format(Double.valueOf(cdBean.getPrinamt()))
                        : cdBean.getPrinamt());
                excel.writeStr(sheet, row, "D", center, cdBean.getYearprinamt() != null && !"".equals(cdBean.getYearprinamt()) ?
                        df.format(Double.valueOf(cdBean.getYearprinamt())) : cdBean.getYearprinamt());
                excel.writeStr(sheet, row, "E", center, cdBean.getTdbookvalue() != null && !"".equals(cdBean.getTdbookvalue()) ?
                        df.format(Double.valueOf(cdBean.getTdbookvalue())) : cdBean.getTdbookvalue());
                excel.writeStr(sheet, row, "F", center, cdBean.getYearbookvalue() != null && !"".equals(cdBean.getYearbookvalue()) ?
                        df.format(Double.valueOf(cdBean.getYearbookvalue())) : cdBean.getYearbookvalue());
                excel.writeStr(sheet, row, "G", center, cdBean.getDuration() != null && !"".equals(cdBean.getDuration()) ?
                        df.format(Double.valueOf(cdBean.getDuration())) : cdBean.getDuration());
                excel.writeStr(sheet, row, "H", center, cdBean.getYearduation() != null && !"".equals(cdBean.getYearduation()) ?
                        df.format(Double.valueOf(cdBean.getYearduation())) : cdBean.getYearduation());
                excel.writeStr(sheet, row, "I", center,
                        cdBean.getRate() != null && !"".equals(cdBean.getRate()) ? df.format(Double.valueOf(cdBean.getRate())) : cdBean.getRate());
                excel.writeStr(sheet, row, "J", center,
                        cdBean.getYearrate() != null && !"".equals(cdBean.getYearrate()) ? df.format(Double.valueOf(cdBean.getYearrate())) : cdBean.getYearrate());
                excel.writeStr(sheet, row, "K", center, cdBean.getDiffamt() != null && !"".equals(cdBean.getDiffamt()) ? df.format(Double.valueOf(cdBean.getDiffamt()))
                        : cdBean.getDiffamt());
                excel.writeStr(sheet, row, "L", center, cdBean.getYeardiffamt() != null && !"".equals(cdBean.getYeardiffamt()) ?
                        df.format(Double.valueOf(cdBean.getYeardiffamt())) : cdBean.getYeardiffamt());
                excel.writeStr(sheet, row, "M", center, cdBean.getDayprofitloss() != null && !"".equals(cdBean.getDayprofitloss()) ?
                        df.format(Double.valueOf(cdBean.getDayprofitloss())) : cdBean.getDayprofitloss());
                excel.writeStr(sheet, row, "N", center, cdBean.getMonthprofitloss() != null && !"".equals(cdBean.getMonthprofitloss()) ?
                        df.format(Double.valueOf(cdBean.getMonthprofitloss())) : cdBean.getMonthprofitloss());
                excel.writeStr(sheet, row, "O", center, cdBean.getYearprofitloss() != null && !"".equals(cdBean.getYearprofitloss()) ?
                        df.format(Double.valueOf(cdBean.getYearprofitloss())) : cdBean.getYearprofitloss());
            }
            return excel;
        } catch (Exception e) {
            throw new RException(e);
        }
    }

    /**
     * 导出客户信息Excel
     */
    @ResponseBody
    @RequestMapping(value = "/exportCustExcel")
    public void exportCustExcel(HttpServletRequest request, HttpServletResponse response) {
        String ua = request.getHeader("User-Agent");
        String filename = "客户信息表.xlsx"; // 文件名
        String encode_filename = "";
        try {
            encode_filename = URLEncoder.encode(filename, "utf-8");
        } catch (UnsupportedEncodingException e1) {
            throw new RException(e1.getMessage());
        }
        response.setContentType("application/vnd.ms-excel");
        if (ua != null) {
            response.setHeader("Content-Disposition", "attachment; filename=" + encode_filename);
        }
        response.setHeader("Connection", "close");
        try {
            String cno = request.getParameter("cno"); // 交易对手编号
            String cname = request.getParameter("cname"); // 交易对手代码
            String cliname = request.getParameter("cliname"); // 中文名称
            String localstatus = request.getParameter("localstatus"); // 处理状态
            String ctype = request.getParameter("ctype"); // 交易对手类型
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("cno", cno);
            map.put("cname", cname);
            map.put("cliname", cliname);
            map.put("localstatus", localstatus);
            map.put("ctype", ctype);
            ExcelUtil e = downloadCustExcel(map);
            OutputStream ouputStream = response.getOutputStream();
            e.getWb().write(ouputStream);
            ouputStream.flush();
            ouputStream.close();
        } catch (Exception e) {
            logger.info(e);
            throw new RException(e);
        }
    }

    ;

    /**
     * 客户信息Excel文件下载内容
     */
    public ExcelUtil downloadCustExcel(Map<String, Object> map) throws Exception {
        IfsOpicsCust bean = new IfsOpicsCust();
        List<IfsOpicsCust> result = ifsOpicsCustMapper.searchListOpicsCust(map); // 查数据
        ExcelUtil eUtil = null;
        try {
            // 表头
            eUtil = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
            CellStyle center = eUtil.getDefaultCenterStrCellStyle();

            int sheet = 0;
            int row = 0;

            // 表格sheet名称
            eUtil.getWb().createSheet("客户信息表");

            // 设置表头字段名
            eUtil.writeStr(sheet, row, "A", center, "交易对手编号");
            eUtil.writeStr(sheet, row, "B", center, "交易对手代码");
            eUtil.writeStr(sheet, row, "C", center, "交易对手简称");
            eUtil.writeStr(sheet, row, "D", center, "客户中文名称");
            eUtil.writeStr(sheet, row, "E", center, "交易对手类型");
            eUtil.writeStr(sheet, row, "F", center, "核心客户号");
            eUtil.writeStr(sheet, row, "G", center, "处理状态");
            eUtil.writeStr(sheet, row, "H", center, "证件类型");
            eUtil.writeStr(sheet, row, "I", center, "证件号码");
            eUtil.writeStr(sheet, row, "J", center, "客户建档柜员号");
            eUtil.writeStr(sheet, row, "K", center, "客户建档机构号");
            eUtil.writeStr(sheet, row, "L", center, "同业客户类型");
            eUtil.writeStr(sheet, row, "M", center, "注册地区");
            eUtil.writeStr(sheet, row, "N", center, "注册国家");
            eUtil.writeStr(sheet, row, "O", center, "SWIFT编码");
            eUtil.writeStr(sheet, row, "P", center, "国家代码");
            eUtil.writeStr(sheet, row, "Q", center, "城市代码");
            eUtil.writeStr(sheet, row, "R", center, "工业标准代码");
            eUtil.writeStr(sheet, row, "S", center, "交易对手全称");
            eUtil.writeStr(sheet, row, "T", center, "公司地址");
            eUtil.writeStr(sheet, row, "U", center, "邮编");
            eUtil.writeStr(sheet, row, "V", center, "维护更新日期");
            eUtil.writeStr(sheet, row, "W", center, "纳税人编号");
            eUtil.writeStr(sheet, row, "X", center, "会计类型");
            eUtil.writeStr(sheet, row, "Y", center, "所在城市");
            eUtil.writeStr(sheet, row, "Z", center, "评级类型");
            eUtil.writeStr(sheet, row, "AA", center, "外部评级公司");
            eUtil.writeStr(sheet, row, "AB", center, "外部评级结果");
            eUtil.writeStr(sheet, row, "AC", center, "内部评级结果");
            eUtil.writeStr(sheet, row, "AD", center, "行业类型");
            eUtil.writeStr(sheet, row, "AE", center, "CFETS机构编码");
            eUtil.writeStr(sheet, row, "AF", center, "CFETS机构简称");
            eUtil.writeStr(sheet, row, "AG", center, "授信占用主体");
            eUtil.writeStr(sheet, row, "AH", center, "客户类型");

            // 设置列宽
            eUtil.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
            eUtil.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
            eUtil.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
            eUtil.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
            eUtil.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
            eUtil.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
            eUtil.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
            eUtil.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
            eUtil.getSheetAt(sheet).setColumnWidth(8, 20 * 256);
            eUtil.getSheetAt(sheet).setColumnWidth(9, 20 * 256);
            eUtil.getSheetAt(sheet).setColumnWidth(10, 20 * 256);
            eUtil.getSheetAt(sheet).setColumnWidth(11, 20 * 256);
            eUtil.getSheetAt(sheet).setColumnWidth(12, 20 * 256);
            eUtil.getSheetAt(sheet).setColumnWidth(13, 20 * 256);
            eUtil.getSheetAt(sheet).setColumnWidth(14, 20 * 256);
            eUtil.getSheetAt(sheet).setColumnWidth(15, 20 * 256);
            eUtil.getSheetAt(sheet).setColumnWidth(16, 20 * 256);
            eUtil.getSheetAt(sheet).setColumnWidth(17, 20 * 256);
            eUtil.getSheetAt(sheet).setColumnWidth(18, 20 * 256);
            eUtil.getSheetAt(sheet).setColumnWidth(19, 20 * 256);
            eUtil.getSheetAt(sheet).setColumnWidth(20, 20 * 256);
            eUtil.getSheetAt(sheet).setColumnWidth(21, 20 * 256);
            eUtil.getSheetAt(sheet).setColumnWidth(22, 20 * 256);
            eUtil.getSheetAt(sheet).setColumnWidth(23, 20 * 256);
            eUtil.getSheetAt(sheet).setColumnWidth(24, 20 * 256);
            eUtil.getSheetAt(sheet).setColumnWidth(25, 20 * 256);
            eUtil.getSheetAt(sheet).setColumnWidth(26, 20 * 256);
            eUtil.getSheetAt(sheet).setColumnWidth(27, 20 * 256);
            eUtil.getSheetAt(sheet).setColumnWidth(28, 20 * 256);
            eUtil.getSheetAt(sheet).setColumnWidth(29, 20 * 256);
            eUtil.getSheetAt(sheet).setColumnWidth(30, 20 * 256);
            eUtil.getSheetAt(sheet).setColumnWidth(31, 20 * 256);
            eUtil.getSheetAt(sheet).setColumnWidth(32, 20 * 256);
            eUtil.getSheetAt(sheet).setColumnWidth(33, 20 * 256);
            eUtil.getSheetAt(sheet).setColumnWidth(34, 20 * 256);

            /*数据字典传值*/
            Map<String, String> ctypeMap = new HashMap<String, String>();
            Map<String, String> localstatusMap = new HashMap<String, String>();
            Map<String, String> gtypeMap = new HashMap<String, String>();
            Map<String, String> fintypeMap = new HashMap<String, String>();
            Map<String, String> flagMap = new HashMap<String, String>();
            Map<String, String> regplaceMap = new HashMap<String, String>();
            Map<String, String> ratypeMap = new HashMap<String, String>();
            Map<String, String> indtypeMap = new HashMap<String, String>();
            Map<String, String> clitypeMap = new HashMap<String, String>();
            for (int i = 0; i < result.size(); i++) {
                bean = result.get(i);
                row++;

                /*取数据字典中的value值*/
                ctypeMap.put("dictId", "CType");
                ctypeMap.put("dictKey", bean.getCtype());
                String ctype = ifsService.queryDictValue(ctypeMap); // 交易对手类型

                localstatusMap.put("dictId", "DealStatus");
                localstatusMap.put("dictKey", bean.getLocalstatus());
                String localstatus = ifsService.queryDictValue(localstatusMap); // 处理状态

                gtypeMap.put("dictId", "GType");
                gtypeMap.put("dictKey", bean.getGtype());
                String gtype = ifsService.queryDictValue(gtypeMap); // 证件类型

                fintypeMap.put("dictId", "newFintype");
                fintypeMap.put("dictKey", bean.getFintype());
                String fintype = ifsService.queryDictValue(fintypeMap); // 同业客户类型

                flagMap.put("dictId", "YesNo");
                flagMap.put("dictKey", bean.getFlag());
                String flag = ifsService.queryDictValue(flagMap); // 境内外标志

                regplaceMap.put("dictId", "newRegplace");
                regplaceMap.put("dictKey", bean.getRegplace());
                String regplace = ifsService.queryDictValue(regplaceMap); // 注册国家

                ratypeMap.put("dictId", "ratype");
                ratypeMap.put("dictKey", bean.getRatype());
                String ratype = ifsService.queryDictValue(ratypeMap); // 评级类型

                indtypeMap.put("dictId", "industtype");
                indtypeMap.put("dictKey", bean.getIndtype());
                String indtype = ifsService.queryDictValue(indtypeMap); // 行业类型

                clitypeMap.put("dictId", "clitype");
                clitypeMap.put("dictKey", bean.getClitype());
                String clitype = ifsService.queryDictValue(clitypeMap); // 客户类型

                // 插入数据
                eUtil.writeStr(sheet, row, "A", center, bean.getCno());
                eUtil.writeStr(sheet, row, "B", center, bean.getCname());
                eUtil.writeStr(sheet, row, "C", center, bean.getSn());
                eUtil.writeStr(sheet, row, "D", center, bean.getCliname());
                eUtil.writeStr(sheet, row, "E", center, ctype);
                eUtil.writeStr(sheet, row, "F", center, bean.getClino());
                eUtil.writeStr(sheet, row, "G", center, localstatus);
                eUtil.writeStr(sheet, row, "H", center, gtype);
                eUtil.writeStr(sheet, row, "I", center, bean.getGid());
                eUtil.writeStr(sheet, row, "J", center, bean.getTellerno());
                eUtil.writeStr(sheet, row, "K", center, bean.getOrgno());
                eUtil.writeStr(sheet, row, "L", center, fintype);
                eUtil.writeStr(sheet, row, "M", center, bean.getLicense());
                eUtil.writeStr(sheet, row, "N", center, regplace);
                eUtil.writeStr(sheet, row, "O", center, bean.getBic());
                eUtil.writeStr(sheet, row, "P", center, bean.getUccode());
                eUtil.writeStr(sheet, row, "Q", center, bean.getCcode());
                eUtil.writeStr(sheet, row, "R", center, bean.getSic());
                eUtil.writeStr(sheet, row, "S", center, bean.getCfn());
                eUtil.writeStr(sheet, row, "T", center, bean.getCa());
                eUtil.writeStr(sheet, row, "U", center, bean.getCpost());
                eUtil.writeStr(sheet, row, "V", center, bean.getLstmntdate());
                eUtil.writeStr(sheet, row, "W", center, bean.getTaxid());
                eUtil.writeStr(sheet, row, "X", center, bean.getAcctngtype());
                eUtil.writeStr(sheet, row, "Y", center, bean.getLocation());
                eUtil.writeStr(sheet, row, "Z", center, ratype);
                eUtil.writeStr(sheet, row, "AA", center, bean.getExtrainfo());
                eUtil.writeStr(sheet, row, "AB", center, bean.getRatresult1());
                eUtil.writeStr(sheet, row, "AC", center, bean.getRatresult2());
                eUtil.writeStr(sheet, row, "AD", center, indtype);
                eUtil.writeStr(sheet, row, "AE", center, bean.getCfetsno());
                eUtil.writeStr(sheet, row, "AF", center, bean.getCfetscn());
                eUtil.writeStr(sheet, row, "AG", center, bean.getCreditsubnm());
                eUtil.writeStr(sheet, row, "AH", center, clitype);
            }
            return eUtil;
        } catch (Exception e) {
            logger.info(e);
            throw new RException(e);
        }
    }

    /**
     * 导出回购统计报表Excel
     */
    @ResponseBody
    @RequestMapping(value = "/exportRepoReportExcel")
    public void exportRepoReportExcel(HttpServletRequest request, HttpServletResponse response) {
        String ua = request.getHeader("User-Agent");
        String year = request.getParameter("year");
        String filename = "回购统计报表.xlsx"; // 文件名
        String encode_filename = "";
        try {
            encode_filename = URLEncoder.encode(filename, "utf-8");
        } catch (UnsupportedEncodingException e1) {
            throw new RException(e1.getMessage());
        }
        response.setContentType("application/vnd.ms-excel");
        if (ua != null && ua.indexOf("Firefox/") >= 0) {
            response.setHeader("Content-Disposition", "attachment; filename*=" + encode_filename);
        } else {
            response.setHeader("Content-Disposition", "attachment; filename=" + encode_filename);
        }
        response.setHeader("Connection", "close");
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("year", year);
            ExcelUtil e = downloadRepoReportExcel(map);
            OutputStream ouputStream = response.getOutputStream();
            e.getWb().write(ouputStream);
            ouputStream.flush();
            ouputStream.close();
        } catch (Exception e) {
            logger.info(e);
            throw new RException(e);
        }
    }

    ;

    /**
     * 回购统计报表Excel文件下载内容
     */
    @SuppressWarnings("static-access")
    public ExcelUtil downloadRepoReportExcel(Map<String, Object> map) throws Exception {
        String year = (String) map.get("year");
        if (year == null || "".equals(year)) {
            String curDate = DayendDateServiceProxy.getInstance().getSettlementDate();
            year = curDate.substring(0, 4);
            map.put("year", year);
        }
        List<IfsRepoReportBean> list = ifsService.searchRepoReportList(map);
        List<IfsRepoReportBean> newList = new ArrayList<IfsRepoReportBean>();
        String[] arr = {year + "01", year + "02", year + "03", year + "04", year + "05", year + "06",
                year + "07", year + "08", year + "09", year + "10", year + "11", year + "12"};
        for (String month : arr) {
            IfsRepoReportBean bean = new IfsRepoReportBean();
            bean.setYear(year);
            bean.setMonth(month);
            String sdatestr = month + "01";
            Date sdate = DateUtil.parse(sdatestr, "yyyyMMdd");
            Date edate = DateUtil.getEndDateTimeOfMonth(sdate);
            bean.setSdate(DateUtil.format(sdate));
            bean.setEdate(DateUtil.format(edate));
            bean.setPnum("0");
            bean.setPamt(new BigDecimal(0));
            bean.setPavg(new BigDecimal(0));
            bean.setSnum("0");
            bean.setSamt(new BigDecimal(0));
            bean.setSavg(new BigDecimal(0));
            for (IfsRepoReportBean ifsRepoReportBean : list) {
                if (ifsRepoReportBean.getMonth().equalsIgnoreCase(DateUtil.format(sdate).substring(0, 7))) {
                    bean.setPnum(ifsRepoReportBean.getPnum());
                    bean.setPamt(ifsRepoReportBean.getPamt() == null ? new BigDecimal(0) : ifsRepoReportBean.getPamt().setScale(4, BigDecimal.ROUND_HALF_UP));
                    bean.setPavg(ifsRepoReportBean.getPavg() == null ? new BigDecimal(0) : ifsRepoReportBean.getPavg().setScale(4, BigDecimal.ROUND_HALF_UP));
                    bean.setSnum(ifsRepoReportBean.getSnum());
                    bean.setSamt(ifsRepoReportBean.getSamt() == null ? new BigDecimal(0) : ifsRepoReportBean.getSamt().setScale(4, BigDecimal.ROUND_HALF_UP));
                    bean.setSavg(ifsRepoReportBean.getSavg() == null ? new BigDecimal(0) : ifsRepoReportBean.getSavg().setScale(4, BigDecimal.ROUND_HALF_UP));
                }
            }
            newList.add(bean);
        }
        //合计
        IfsRepoReportBean totalBean = ifsService.searchRepoReportListTotal(map);
        IfsRepoReportBean bean = new IfsRepoReportBean();
        if (totalBean != null) {
            bean.setPnum(totalBean.getPnum());
            bean.setPamt(totalBean.getPamt() == null ? new BigDecimal(0) : totalBean.getPamt().setScale(4, BigDecimal.ROUND_HALF_UP));
            bean.setSnum(totalBean.getSnum());
            bean.setSamt(totalBean.getSamt() == null ? new BigDecimal(0) : totalBean.getSamt().setScale(4, BigDecimal.ROUND_HALF_UP));
        } else {
            bean.setPnum("0");
            bean.setPamt(new BigDecimal(0));
            bean.setSnum("0");
            bean.setSamt(new BigDecimal(0));
        }
        newList.add(bean);
        ExcelUtil eutil = null;
        try {
            // 表头
            eutil = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
            CellStyle center = eutil.getDefaultCenterStrCellStyle();
            int sheet = 0;
            int row = 0;
            // 表格sheet名称
            eutil.getWb().createSheet("回购统计报表");
            // 设置表头字段名
            eutil.writeStr(sheet, row, "A", center, "月份");
            eutil.writeStr(sheet, row, "B", center, "起止时间");
            eutil.writeStr(sheet, row, "C", center, "起止时间");
            eutil.writeStr(sheet, row, "D", center, "正回购");
            eutil.writeStr(sheet, row, "E", center, "正回购");
            eutil.writeStr(sheet, row, "F", center, "正回购");
            eutil.writeStr(sheet, row, "G", center, "逆回购");
            eutil.writeStr(sheet, row, "H", center, "逆回购");
            eutil.writeStr(sheet, row, "I", center, "逆回购");
            row = row + 1;
            eutil.writeStr(sheet, row, "A", center, "月份");
            eutil.writeStr(sheet, row, "B", center, "起");
            eutil.writeStr(sheet, row, "C", center, "止");
            eutil.writeStr(sheet, row, "D", center, "金额(亿)");
            eutil.writeStr(sheet, row, "E", center, "加权价格");
            eutil.writeStr(sheet, row, "F", center, "笔数");
            eutil.writeStr(sheet, row, "G", center, "金额(亿)");
            eutil.writeStr(sheet, row, "H", center, "加权价格");
            eutil.writeStr(sheet, row, "I", center, "笔数");
            // 合并单元格
            eutil.cellRange(0, 1, 0, 0, eutil.getSheetAt(0), eutil.getWb());
            eutil.cellRange(0, 0, 1, 2, eutil.getSheetAt(0), eutil.getWb());
            eutil.cellRange(0, 0, 3, 5, eutil.getSheetAt(0), eutil.getWb());
            eutil.cellRange(0, 0, 6, 8, eutil.getSheetAt(0), eutil.getWb());
            // 设置列宽
            eutil.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(8, 20 * 256);
            for (int i = 0; i < newList.size(); i++) {
                bean = newList.get(i);
                row++;
                // 插入数据
                eutil.writeStr(sheet, row, "A", center, bean.getMonth() != null && !"".equals(bean.getMonth()) ? bean.getMonth() : "合计");
                eutil.writeStr(sheet, row, "B", center, bean.getSdate());
                eutil.writeStr(sheet, row, "C", center, bean.getEdate());
                eutil.writeStr(sheet, row, "D", center, bean.getSamt() == null ? String.valueOf(0) : String.valueOf(bean.getSamt()));
                eutil.writeStr(sheet, row, "E", center, bean.getSavg() == null ? String.valueOf(0) : String.valueOf(bean.getSavg()));
                eutil.writeStr(sheet, row, "F", center, bean.getSnum());
                eutil.writeStr(sheet, row, "G", center, bean.getPamt() == null ? String.valueOf(0) : String.valueOf(bean.getPamt()));
                eutil.writeStr(sheet, row, "H", center, bean.getPavg() == null ? String.valueOf(0) : String.valueOf(bean.getPavg()));
                eutil.writeStr(sheet, row, "I", center, bean.getPnum());
            }
            return eutil;
        } catch (Exception e) {
            logger.info(e);
            throw new RException(e);
        }
    }

    /**
     * 导出拆借统计报表Excel
     */
    @ResponseBody
    @RequestMapping(value = "/exportIboReportExcel")
    public void exportIboReportExcel(HttpServletRequest request, HttpServletResponse response) {
        String ua = request.getHeader("User-Agent");
        String year = request.getParameter("year");
        String filename = "拆借统计报表.xlsx"; // 文件名
        String encode_filename = "";
        try {
            encode_filename = URLEncoder.encode(filename, "utf-8");
        } catch (UnsupportedEncodingException e1) {
            throw new RException(e1.getMessage());
        }
        response.setContentType("application/vnd.ms-excel");
        if (ua != null && ua.indexOf("Firefox/") >= 0) {
            response.setHeader("Content-Disposition", "attachment; filename*=" + encode_filename);
        } else {
            response.setHeader("Content-Disposition", "attachment; filename=" + encode_filename);
        }
        response.setHeader("Connection", "close");
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("year", year);
            ExcelUtil e = downloadIboReportExcel(map);
            OutputStream ouputStream = response.getOutputStream();
            e.getWb().write(ouputStream);
            ouputStream.flush();
            ouputStream.close();
        } catch (Exception e) {
            logger.info(e);
            throw new RException(e);
        }
    }

    ;

    /**
     * 拆借统计报表Excel文件下载内容
     */
    @SuppressWarnings("static-access")
    public ExcelUtil downloadIboReportExcel(Map<String, Object> map) throws Exception {
        String year = (String) map.get("year");
        if (year == null || "".equals(year)) {
            String curDate = DayendDateServiceProxy.getInstance().getSettlementDate();
            year = curDate.substring(0, 4);
            map.put("year", year);
        }
        List<IfsIboReportBean> list = ifsService.searchIboReportList(map);
        List<IfsIboReportBean> newList = new ArrayList<IfsIboReportBean>();
        String[] arr = {year + "01", year + "02", year + "03", year + "04", year + "05", year + "06",
                year + "07", year + "08", year + "09", year + "10", year + "11", year + "12"};
        for (String month : arr) {
            IfsIboReportBean bean = new IfsIboReportBean();
            bean.setYear(year);
            bean.setMonth(month);
            String sdatestr = month + "01";
            Date sdate = DateUtil.parse(sdatestr, "yyyyMMdd");
            Date edate = DateUtil.getEndDateTimeOfMonth(sdate);
            bean.setSdate(DateUtil.format(sdate));
            bean.setEdate(DateUtil.format(edate));
            bean.setPnum("0");
            bean.setPamt(new BigDecimal(0));
            bean.setPavg(new BigDecimal(0));
            bean.setSnum("0");
            bean.setSamt(new BigDecimal(0));
            bean.setSavg(new BigDecimal(0));
            for (IfsIboReportBean ifsIboReportBean : list) {
                if (ifsIboReportBean.getMonth().equalsIgnoreCase(DateUtil.format(sdate).substring(0, 7))) {
                    bean.setPnum(ifsIboReportBean.getPnum());
                    bean.setPamt(ifsIboReportBean.getPamt() == null ? new BigDecimal(0) : ifsIboReportBean.getPamt().setScale(4, BigDecimal.ROUND_HALF_UP));
                    bean.setPavg(ifsIboReportBean.getPavg() == null ? new BigDecimal(0) : ifsIboReportBean.getPavg().setScale(4, BigDecimal.ROUND_HALF_UP));
                    bean.setSnum(ifsIboReportBean.getSnum());
                    bean.setSamt(ifsIboReportBean.getSamt() == null ? new BigDecimal(0) : ifsIboReportBean.getSamt().setScale(4, BigDecimal.ROUND_HALF_UP));
                    bean.setSavg(ifsIboReportBean.getSavg() == null ? new BigDecimal(0) : ifsIboReportBean.getSavg().setScale(4, BigDecimal.ROUND_HALF_UP));
                }
            }
            newList.add(bean);
        }
        //合计
        IfsIboReportBean totalBean = ifsService.searchIboReportListTotal(map);
        IfsIboReportBean bean = new IfsIboReportBean();
        if (totalBean != null) {
            bean.setPnum(totalBean.getPnum());
            bean.setPamt(totalBean.getPamt() == null ? new BigDecimal(0) : totalBean.getPamt().setScale(4, BigDecimal.ROUND_HALF_UP));
            bean.setSnum(totalBean.getSnum());
            bean.setSamt(totalBean.getSamt() == null ? new BigDecimal(0) : totalBean.getSamt().setScale(4, BigDecimal.ROUND_HALF_UP));
        } else {
            bean.setPnum("0");
            bean.setPamt(new BigDecimal(0));
            bean.setSnum("0");
            bean.setSamt(new BigDecimal(0));
        }
        newList.add(bean);

        ExcelUtil eutil = null;
        try {
            // 表头
            eutil = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
            CellStyle center = eutil.getDefaultCenterStrCellStyle();
            int sheet = 0;
            int row = 0;
            // 表格sheet名称
            eutil.getWb().createSheet("拆借统计报表");
            // 设置表头字段名
            eutil.writeStr(sheet, row, "A", center, "月份");
            eutil.writeStr(sheet, row, "B", center, "起止时间");
            eutil.writeStr(sheet, row, "C", center, "起止时间");
            eutil.writeStr(sheet, row, "D", center, "人民币拆入");
            eutil.writeStr(sheet, row, "E", center, "人民币拆入");
            eutil.writeStr(sheet, row, "F", center, "人民币拆入");
            eutil.writeStr(sheet, row, "G", center, "人民币拆出");
            eutil.writeStr(sheet, row, "H", center, "人民币拆出");
            eutil.writeStr(sheet, row, "I", center, "人民币拆出");
            row = row + 1;
            eutil.writeStr(sheet, row, "A", center, "月份");
            eutil.writeStr(sheet, row, "B", center, "起");
            eutil.writeStr(sheet, row, "C", center, "止");
            eutil.writeStr(sheet, row, "D", center, "金额(亿)");
            eutil.writeStr(sheet, row, "E", center, "加权价格");
            eutil.writeStr(sheet, row, "F", center, "笔数");
            eutil.writeStr(sheet, row, "G", center, "金额(亿)");
            eutil.writeStr(sheet, row, "H", center, "加权价格");
            eutil.writeStr(sheet, row, "I", center, "笔数");
            // 合并单元格
            eutil.cellRange(0, 1, 0, 0, eutil.getSheetAt(0), eutil.getWb());
            eutil.cellRange(0, 0, 1, 2, eutil.getSheetAt(0), eutil.getWb());
            eutil.cellRange(0, 0, 3, 5, eutil.getSheetAt(0), eutil.getWb());
            eutil.cellRange(0, 0, 6, 8, eutil.getSheetAt(0), eutil.getWb());
            // 设置列宽
            eutil.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(8, 20 * 256);
            for (int i = 0; i < newList.size(); i++) {
                bean = newList.get(i);
                row++;
                // 插入数据
                eutil.writeStr(sheet, row, "A", center, bean.getMonth() != null && !"".equals(bean.getMonth()) ? bean.getMonth() : "合计");
                eutil.writeStr(sheet, row, "B", center, bean.getSdate());
                eutil.writeStr(sheet, row, "C", center, bean.getEdate());
                eutil.writeStr(sheet, row, "D", center, bean.getPamt() == null ? String.valueOf(0) : String.valueOf(bean.getPamt()));
                eutil.writeStr(sheet, row, "E", center, bean.getPavg() == null ? String.valueOf(0) : String.valueOf(bean.getPavg()));
                eutil.writeStr(sheet, row, "F", center, bean.getPnum());
                eutil.writeStr(sheet, row, "G", center, bean.getSamt() == null ? String.valueOf(0) : String.valueOf(bean.getSamt()));
                eutil.writeStr(sheet, row, "H", center, bean.getSavg() == null ? String.valueOf(0) : String.valueOf(bean.getSavg()));
                eutil.writeStr(sheet, row, "I", center, bean.getSnum());
            }
            return eutil;
        } catch (Exception e) {
            logger.info(e);
            throw new RException(e);
        }
    }


    /**
     * 导出现券买卖统计报表Excel
     */
    @ResponseBody
    @RequestMapping(value = "/exportCbtReportExcel")
    public void exportCbtReportExcel(HttpServletRequest request, HttpServletResponse response) {
        String ua = request.getHeader("User-Agent");
        String year = request.getParameter("year");
        String filename = "现券买卖统计报表.xlsx"; // 文件名
        String encode_filename = "";
        try {
            encode_filename = URLEncoder.encode(filename, "utf-8");
        } catch (UnsupportedEncodingException e1) {
            throw new RException(e1.getMessage());
        }
        response.setContentType("application/vnd.ms-excel");
        if (ua != null && ua.indexOf("Firefox/") >= 0) {
            response.setHeader("Content-Disposition", "attachment; filename*=" + encode_filename);
        } else {
            response.setHeader("Content-Disposition", "attachment; filename=" + encode_filename);
        }
        response.setHeader("Connection", "close");
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("year", year);
            ExcelUtil e = downloadCbtReportExcel(map);
            OutputStream ouputStream = response.getOutputStream();
            e.getWb().write(ouputStream);
            ouputStream.flush();
            ouputStream.close();
        } catch (Exception e) {
            logger.info(e);
            throw new RException(e);
        }
    }

    ;

    /**
     * 现券买卖统计报表Excel文件下载内容
     */
    @SuppressWarnings("static-access")
    public ExcelUtil downloadCbtReportExcel(Map<String, Object> map) throws Exception {

        String year = (String) map.get("year");
        if (year == null || "".equals(year)) {
            String curDate = DayendDateServiceProxy.getInstance().getSettlementDate();
            year = curDate.substring(0, 4);
            map.put("year", year);
        }
        List<IfsCbtReportBean> list = ifsService.searchCbtReportList(map);
        List<IfsCbtReportBean> newList = new ArrayList<IfsCbtReportBean>();
        String[] arr = {year + "01", year + "02", year + "03", year + "04", year + "05", year + "06",
                year + "07", year + "08", year + "09", year + "10", year + "11", year + "12"};
        for (String month : arr) {
            IfsCbtReportBean bean = new IfsCbtReportBean();
            bean.setYear(year);
            bean.setMonth(month);
            String sdatestr = month + "01";
            Date sdate = DateUtil.parse(sdatestr, "yyyyMMdd");
            Date edate = DateUtil.getEndDateTimeOfMonth(sdate);
            bean.setSdate(DateUtil.format(sdate));
            bean.setEdate(DateUtil.format(edate));
            bean.setPnum("0");
            bean.setPamt(new BigDecimal(0));
            bean.setPavg(new BigDecimal(0));
            bean.setSnum("0");
            bean.setSamt(new BigDecimal(0));
            bean.setSavg(new BigDecimal(0));
            bean.setTnum("0");
            bean.setTamt(new BigDecimal(0));
            for (IfsCbtReportBean ifsCbtReportBean : list) {
                if (ifsCbtReportBean.getMonth().equalsIgnoreCase(DateUtil.format(sdate).substring(0, 7))) {
                    bean.setPnum(ifsCbtReportBean.getPnum());
                    bean.setPamt(ifsCbtReportBean.getPamt() == null ? new BigDecimal(0) : ifsCbtReportBean.getPamt().setScale(4, BigDecimal.ROUND_HALF_UP));
                    //bean.setPavg(ifsCbtReportBean.getPavg()==null?new BigDecimal(0):ifsCbtReportBean.getPavg().setScale(4, BigDecimal.ROUND_HALF_UP));
                    bean.setSnum(ifsCbtReportBean.getSnum());
                    bean.setSamt(ifsCbtReportBean.getSamt() == null ? new BigDecimal(0) : ifsCbtReportBean.getSamt().setScale(4, BigDecimal.ROUND_HALF_UP));
                    //bean.setSavg(ifsCbtReportBean.getSavg()==null?new BigDecimal(0):ifsCbtReportBean.getSavg().setScale(4, BigDecimal.ROUND_HALF_UP));
                    bean.setTnum(ifsCbtReportBean.getTnum());
                    bean.setTamt(ifsCbtReportBean.getTamt() == null ? new BigDecimal(0) : ifsCbtReportBean.getTamt().setScale(4, BigDecimal.ROUND_HALF_UP));
                }
            }
            newList.add(bean);
        }
        //合计
        IfsCbtReportBean totalBean = ifsService.searchCbtReportListTotal(map);
        IfsCbtReportBean bean = new IfsCbtReportBean();
        if (totalBean != null) {
            bean.setPnum(totalBean.getPnum());
            bean.setPamt(totalBean.getPamt() == null ? new BigDecimal(0) : totalBean.getPamt().setScale(4, BigDecimal.ROUND_HALF_UP));
            bean.setSnum(totalBean.getSnum());
            bean.setSamt(totalBean.getSamt() == null ? new BigDecimal(0) : totalBean.getSamt().setScale(4, BigDecimal.ROUND_HALF_UP));
            bean.setTnum(totalBean.getTnum());
            bean.setTamt(totalBean.getTamt() == null ? new BigDecimal(0) : totalBean.getTamt().setScale(4, BigDecimal.ROUND_HALF_UP));
        } else {
            bean.setPnum("0");
            bean.setPamt(new BigDecimal(0));
            bean.setSnum("0");
            bean.setSamt(new BigDecimal(0));
            bean.setTnum("0");
            bean.setTamt(new BigDecimal(0));
        }
        newList.add(bean);

        ExcelUtil eutil = null;
        try {
            // 表头
            eutil = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
            CellStyle center = eutil.getDefaultCenterStrCellStyle();
            int sheet = 0;
            int row = 0;
            // 表格sheet名称
            eutil.getWb().createSheet("现券买卖统计报表");
            // 设置表头字段名
            eutil.writeStr(sheet, row, "A", center, "月份");
            eutil.writeStr(sheet, row, "B", center, "起止时间");
            eutil.writeStr(sheet, row, "C", center, "起止时间");
            eutil.writeStr(sheet, row, "D", center, "债券买入");
            eutil.writeStr(sheet, row, "E", center, "债券买入");
            eutil.writeStr(sheet, row, "F", center, "债券卖出");
            eutil.writeStr(sheet, row, "G", center, "债券卖出");
            eutil.writeStr(sheet, row, "H", center, "合计");
            eutil.writeStr(sheet, row, "I", center, "合计");
            row = row + 1;
            eutil.writeStr(sheet, row, "A", center, "月份");
            eutil.writeStr(sheet, row, "B", center, "起");
            eutil.writeStr(sheet, row, "C", center, "止");
            eutil.writeStr(sheet, row, "D", center, "劵面(亿)");
            eutil.writeStr(sheet, row, "E", center, "笔数");
            eutil.writeStr(sheet, row, "F", center, "劵面(亿)");
            eutil.writeStr(sheet, row, "G", center, "笔数");
            eutil.writeStr(sheet, row, "H", center, "金额");
            eutil.writeStr(sheet, row, "I", center, "笔数");
            // 合并单元格
            eutil.cellRange(0, 1, 0, 0, eutil.getSheetAt(0), eutil.getWb());
            eutil.cellRange(0, 0, 1, 2, eutil.getSheetAt(0), eutil.getWb());
            eutil.cellRange(0, 0, 3, 4, eutil.getSheetAt(0), eutil.getWb());
            eutil.cellRange(0, 0, 5, 6, eutil.getSheetAt(0), eutil.getWb());
            eutil.cellRange(0, 0, 7, 8, eutil.getSheetAt(0), eutil.getWb());
            // 设置列宽
            eutil.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(8, 20 * 256);
            for (int i = 0; i < newList.size(); i++) {
                bean = newList.get(i);
                row++;
                // 插入数据
                eutil.writeStr(sheet, row, "A", center, bean.getMonth() != null && !"".equals(bean.getMonth()) ? bean.getMonth() : "合计");
                eutil.writeStr(sheet, row, "B", center, bean.getSdate());
                eutil.writeStr(sheet, row, "C", center, bean.getEdate());
                eutil.writeStr(sheet, row, "D", center, bean.getPamt() == null ? String.valueOf(0) : String.valueOf(bean.getPamt()));
                eutil.writeStr(sheet, row, "E", center, bean.getPnum());
                eutil.writeStr(sheet, row, "F", center, bean.getSamt() == null ? String.valueOf(0) : String.valueOf(bean.getSamt()));
                eutil.writeStr(sheet, row, "G", center, bean.getSnum());
                eutil.writeStr(sheet, row, "H", center, bean.getTamt() == null ? String.valueOf(0) : String.valueOf(bean.getTamt()));
                eutil.writeStr(sheet, row, "I", center, bean.getTnum());
            }
            return eutil;
        } catch (Exception e) {
            logger.info(e);
            throw new RException(e);
        }
    }

    /**
     * 导出资金往来类业务校验Excel
     */
    @ResponseBody
    @RequestMapping(value = "/exportCapitalCheckExcel")
    public void exportCapitalCheckExcel(HttpServletRequest request, HttpServletResponse response) {
        String ua = request.getHeader("User-Agent");
        String forDate = request.getParameter("forDate");
        String contractId = request.getParameter("contractId");
        String dealNo = request.getParameter("dealNo");
        String filename = "资金往来类业务校验报表.xlsx"; // 文件名
        String encode_filename = "";
        try {
            encode_filename = URLEncoder.encode(filename, "utf-8");
        } catch (UnsupportedEncodingException e1) {
            throw new RException(e1.getMessage());
        }
        response.setContentType("application/vnd.ms-excel");
        if (ua != null && ua.indexOf("Firefox/") >= 0) {
            response.setHeader("Content-Disposition", "attachment; filename*=" + encode_filename);
        } else {
            response.setHeader("Content-Disposition", "attachment; filename=" + encode_filename);
        }
        response.setHeader("Connection", "close");
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("forDate", forDate);
            map.put("contractId", contractId);
            map.put("dealNo", dealNo);
            ExcelUtil e = downloadCapitalCheckExcel(map);
            OutputStream ouputStream = response.getOutputStream();
            e.getWb().write(ouputStream);
            ouputStream.flush();
            ouputStream.close();
        } catch (Exception e) {
            logger.info(e);
            throw new RException(e);
        }
    }

    ;

    /**
     * 资金往来类业务校验Excel文件下载内容
     */
    public ExcelUtil downloadCapitalCheckExcel(Map<String, Object> map) throws Exception {
        List<IfsCfetsrmbOr> list = ifsService.searchCapitalCheckList(map);
        List<IfsCfetsrmbOr> newList = new ArrayList<IfsCfetsrmbOr>();
        for (IfsCfetsrmbOr ifsCfetsrmbOr : list) {
            IfsCfetsrmbOr or = new IfsCfetsrmbOr();
            or = ifsCfetsrmbOr;
            Map<String, Object> opicsmap = new HashMap<String, Object>();
            opicsmap.put("dealno", ifsCfetsrmbOr.getDealNo());
            SlRprhDldtBean rprhDldtBean = spshServer.getRprhDldtBean(opicsmap);
            if (rprhDldtBean != null) {
                or.setComamount(rprhDldtBean.getComamt());
                or.setMatamount(rprhDldtBean.getMatamt());
                or.setComcheck(ifsCfetsrmbOr.getFirstSettlementAmount().subtract(rprhDldtBean.getComamt()));
                or.setMatcheck(ifsCfetsrmbOr.getSecondSettlementAmount().subtract(rprhDldtBean.getMatamt()));
            } else {
                or.setComamount(new BigDecimal(0));
                or.setMatamount(new BigDecimal(0));
                or.setComcheck(ifsCfetsrmbOr.getFirstSettlementAmount().subtract(new BigDecimal(0)));
                or.setMatcheck(ifsCfetsrmbOr.getSecondSettlementAmount().subtract(new BigDecimal(0)));
            }
            newList.add(or);
        }
        //合计
        IfsCfetsrmbOr rmbOr = new IfsCfetsrmbOr();
        BigDecimal comcheck = new BigDecimal(0);
        BigDecimal matcheck = new BigDecimal(0);
        BigDecimal comamount = new BigDecimal(0);
        BigDecimal matamount = new BigDecimal(0);
        BigDecimal firstSettlementAmount = new BigDecimal(0);
        BigDecimal secondSettlementAmount = new BigDecimal(0);
        for (IfsCfetsrmbOr ifsCfetsrmbOr : newList) {
            comcheck = comcheck.add(ifsCfetsrmbOr.getComcheck());
            matcheck = matcheck.add(ifsCfetsrmbOr.getMatcheck());
            comamount = comamount.add(ifsCfetsrmbOr.getComamount());
            matamount = matamount.add(ifsCfetsrmbOr.getMatamount());
            firstSettlementAmount = firstSettlementAmount.add(ifsCfetsrmbOr.getFirstSettlementAmount());
            secondSettlementAmount = secondSettlementAmount.add(ifsCfetsrmbOr.getSecondSettlementAmount());
        }
        rmbOr.setComcheck(comcheck);
        rmbOr.setMatcheck(matcheck);
        rmbOr.setComamount(comamount);
        rmbOr.setMatamount(matamount);
        rmbOr.setFirstSettlementAmount(firstSettlementAmount);
        rmbOr.setSecondSettlementAmount(secondSettlementAmount);
        newList.add(rmbOr);


        ExcelUtil eutil = null;
        try {
            // 表头
            eutil = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
            CellStyle center = eutil.getDefaultCenterStrCellStyle();
            int sheet = 0;
            int row = 0;
            // 表格sheet名称
            eutil.getWb().createSheet("资金往来类业务校验报表");
            // 设置表头字段名
            eutil.writeStr(sheet, row, "A", center, "成交日期");
            eutil.writeStr(sheet, row, "B", center, "业务类型");
            eutil.writeStr(sheet, row, "C", center, "成交单号");
            eutil.writeStr(sheet, row, "D", center, "OPICS单号");
            eutil.writeStr(sheet, row, "E", center, "首期校验");
            eutil.writeStr(sheet, row, "F", center, "到期校验");
            eutil.writeStr(sheet, row, "G", center, "本地首期结算金额");
            eutil.writeStr(sheet, row, "H", center, "本地到期结算金额");
            eutil.writeStr(sheet, row, "I", center, "OPICS首期结算金额");
            eutil.writeStr(sheet, row, "J", center, "OPICS到期结算金额");


            // 设置列宽
            eutil.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(8, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(9, 20 * 256);
            for (int i = 0; i < newList.size(); i++) {
                rmbOr = newList.get(i);
                row++;
                // 插入数据
                eutil.writeStr(sheet, row, "A", center, rmbOr.getForDate() != null && !"".equals(rmbOr.getForDate()) ? rmbOr.getForDate() : "合计");
                eutil.writeStr(sheet, row, "B", center, rmbOr.getProduct());
                eutil.writeStr(sheet, row, "C", center, rmbOr.getContractId());
                eutil.writeStr(sheet, row, "D", center, rmbOr.getDealNo());
                eutil.writeStr(sheet, row, "E", center, rmbOr.getComcheck() == null ? String.valueOf(0) : String.valueOf(rmbOr.getComcheck()));
                eutil.writeStr(sheet, row, "F", center, rmbOr.getMatcheck() == null ? String.valueOf(0) : String.valueOf(rmbOr.getMatcheck()));
                eutil.writeStr(sheet, row, "G", center, rmbOr.getFirstSettlementAmount() == null ? String.valueOf(0) : String.valueOf(rmbOr.getFirstSettlementAmount()));
                eutil.writeStr(sheet, row, "H", center, rmbOr.getSecondSettlementAmount() == null ? String.valueOf(0) : String.valueOf(rmbOr.getSecondSettlementAmount()));
                eutil.writeStr(sheet, row, "I", center, rmbOr.getComamount() == null ? String.valueOf(0) : String.valueOf(rmbOr.getComamount()));
                eutil.writeStr(sheet, row, "J", center, rmbOr.getMatamount() == null ? String.valueOf(0) : String.valueOf(rmbOr.getMatamount()));
            }
            return eutil;
        } catch (Exception e) {
            logger.info(e);
            throw new RException(e);
        }
    }


    /**
     * 导出现券交易类业务校验Excel
     */
    @ResponseBody
    @RequestMapping(value = "/exportCbtCheckExcel")
    public void exportCbtCheckExcel(HttpServletRequest request, HttpServletResponse response) {
        String ua = request.getHeader("User-Agent");
        String forDate = request.getParameter("forDate");
        String contractId = request.getParameter("contractId");
        String dealNo = request.getParameter("dealNo");
        String filename = "现券交易类业务校验报表.xlsx"; // 文件名
        String encode_filename = "";
        try {
            encode_filename = URLEncoder.encode(filename, "utf-8");
        } catch (UnsupportedEncodingException e1) {
            throw new RException(e1.getMessage());
        }
        response.setContentType("application/vnd.ms-excel");
        if (ua != null && ua.indexOf("Firefox/") >= 0) {
            response.setHeader("Content-Disposition", "attachment; filename*=" + encode_filename);
        } else {
            response.setHeader("Content-Disposition", "attachment; filename=" + encode_filename);
        }
        response.setHeader("Connection", "close");
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("forDate", forDate);
            map.put("contractId", contractId);
            map.put("dealNo", dealNo);
            ExcelUtil e = downloadCbtCheckExcel(map);
            OutputStream ouputStream = response.getOutputStream();
            e.getWb().write(ouputStream);
            ouputStream.flush();
            ouputStream.close();
        } catch (Exception e) {
            logger.info(e);
            throw new RException(e);
        }
    }

    ;

    /**
     * 现券交易类业务校验Excel文件下载内容
     */
    public ExcelUtil downloadCbtCheckExcel(Map<String, Object> map) throws Exception {
        List<IfsCfetsrmbCbt> list = ifsService.searchCbtCheckList(map);
        List<IfsCfetsrmbCbt> newList = new ArrayList<IfsCfetsrmbCbt>();
        for (IfsCfetsrmbCbt ifsCfetsrmbCbt : list) {
            IfsCfetsrmbCbt cbt = new IfsCfetsrmbCbt();
            cbt = ifsCfetsrmbCbt;
            Map<String, Object> opicsmap = new HashMap<String, Object>();
            opicsmap.put("dealno", ifsCfetsrmbCbt.getDealNo());
            SlSpshBean spshBean = spshServer.getSpshBean(opicsmap);
            if (spshBean != null) {
                cbt.setOpicsprice(spshBean.getPrice_8());
                cbt.setOpicssettleamt(spshBean.getSettamt());
                cbt.setPricecheck(ifsCfetsrmbCbt.getCleanPrice().subtract(spshBean.getPrice_8()));
                cbt.setAmtcheck(ifsCfetsrmbCbt.getSettlementAmount().subtract(spshBean.getSettamt()));
            } else {
                cbt.setOpicsprice(new BigDecimal(0));
                cbt.setOpicssettleamt(new BigDecimal(0));
                cbt.setPricecheck(ifsCfetsrmbCbt.getCleanPrice().subtract(new BigDecimal(0)));
                cbt.setAmtcheck(ifsCfetsrmbCbt.getSettlementAmount().subtract(new BigDecimal(0)));
            }
            newList.add(cbt);
        }
        //合计
        IfsCfetsrmbCbt rmbCbt = new IfsCfetsrmbCbt();
        BigDecimal pricecheck = new BigDecimal(0);
        BigDecimal amtcheck = new BigDecimal(0);
        BigDecimal opicsprice = new BigDecimal(0);
        BigDecimal opicssettleamt = new BigDecimal(0);
        BigDecimal cleanPrice = new BigDecimal(0);
        BigDecimal settlementAmount = new BigDecimal(0);

        for (IfsCfetsrmbCbt ifsCfetsrmbCbt : newList) {
            pricecheck = pricecheck.add(ifsCfetsrmbCbt.getPricecheck());
            amtcheck = amtcheck.add(ifsCfetsrmbCbt.getAmtcheck());
            opicsprice = opicsprice.add(ifsCfetsrmbCbt.getOpicsprice());
            opicssettleamt = opicssettleamt.add(ifsCfetsrmbCbt.getOpicssettleamt());
            cleanPrice = cleanPrice.add(ifsCfetsrmbCbt.getCleanPrice());
            settlementAmount = settlementAmount.add(ifsCfetsrmbCbt.getSettlementAmount());
        }
        rmbCbt.setPricecheck(pricecheck);
        rmbCbt.setAmtcheck(amtcheck);
        rmbCbt.setOpicsprice(opicsprice);
        rmbCbt.setOpicssettleamt(opicssettleamt);
        rmbCbt.setCleanPrice(cleanPrice);
        rmbCbt.setSettlementAmount(settlementAmount);
        newList.add(rmbCbt);

        ExcelUtil eutil = null;
        try {
            // 表头
            eutil = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
            CellStyle center = eutil.getDefaultCenterStrCellStyle();
            int sheet = 0;
            int row = 0;
            // 表格sheet名称
            eutil.getWb().createSheet("现券交易类业务校验报表");
            // 设置表头字段名
            eutil.writeStr(sheet, row, "A", center, "成交日期");
            eutil.writeStr(sheet, row, "B", center, "业务类型");
            eutil.writeStr(sheet, row, "C", center, "成交单号");
            eutil.writeStr(sheet, row, "D", center, "OPICS单号");
            eutil.writeStr(sheet, row, "E", center, "净价校验");
            eutil.writeStr(sheet, row, "F", center, "结算金额校验");
            eutil.writeStr(sheet, row, "G", center, "本地交易净价");
            eutil.writeStr(sheet, row, "H", center, "本地结算金额");
            eutil.writeStr(sheet, row, "I", center, "OPICS交易净价");
            eutil.writeStr(sheet, row, "J", center, "OPICS结算金额");


            // 设置列宽
            eutil.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(8, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(9, 20 * 256);
            for (int i = 0; i < newList.size(); i++) {
                rmbCbt = newList.get(i);
                row++;
                // 插入数据
                eutil.writeStr(sheet, row, "A", center, rmbCbt.getForDate() != null && !"".equals(rmbCbt.getForDate()) ? rmbCbt.getForDate() : "合计");
                eutil.writeStr(sheet, row, "B", center, rmbCbt.getProduct());
                eutil.writeStr(sheet, row, "C", center, rmbCbt.getContractId());
                eutil.writeStr(sheet, row, "D", center, rmbCbt.getDealNo());
                eutil.writeStr(sheet, row, "E", center, rmbCbt.getPricecheck() == null ? String.valueOf(0) : String.valueOf(rmbCbt.getPricecheck()));
                eutil.writeStr(sheet, row, "F", center, rmbCbt.getAmtcheck() == null ? String.valueOf(0) : String.valueOf(rmbCbt.getAmtcheck()));
                eutil.writeStr(sheet, row, "G", center, rmbCbt.getCleanPrice() == null ? String.valueOf(0) : String.valueOf(rmbCbt.getCleanPrice()));
                eutil.writeStr(sheet, row, "H", center, rmbCbt.getSettlementAmount() == null ? String.valueOf(0) : String.valueOf(rmbCbt.getSettlementAmount()));
                eutil.writeStr(sheet, row, "I", center, rmbCbt.getOpicsprice() == null ? String.valueOf(0) : String.valueOf(rmbCbt.getOpicsprice()));
                eutil.writeStr(sheet, row, "J", center, rmbCbt.getOpicssettleamt() == null ? String.valueOf(0) : String.valueOf(rmbCbt.getOpicssettleamt()));
            }
            return eutil;
        } catch (Exception e) {
            logger.info(e);
            throw new RException(e);
        }
    }


    /**
     * 导出外汇交易业务校验Excel
     */
    @ResponseBody
    @RequestMapping(value = "/exportSptCheckExcel")
    public void exportSptCheckExcel(HttpServletRequest request, HttpServletResponse response) {
        String ua = request.getHeader("User-Agent");
        String forDate = request.getParameter("forDate");
        String contractId = request.getParameter("contractId");
        String dealNo = request.getParameter("dealNo");
        String filename = "外汇交易业务校验报表.xlsx"; // 文件名
        String encode_filename = "";
        try {
            encode_filename = URLEncoder.encode(filename, "utf-8");
        } catch (UnsupportedEncodingException e1) {
            throw new RException(e1.getMessage());
        }
        response.setContentType("application/vnd.ms-excel");
        if (ua != null && ua.indexOf("Firefox/") >= 0) {
            response.setHeader("Content-Disposition", "attachment; filename*=" + encode_filename);
        } else {
            response.setHeader("Content-Disposition", "attachment; filename=" + encode_filename);
        }
        response.setHeader("Connection", "close");
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("forDate", forDate);
            map.put("contractId", contractId);
            map.put("dealNo", dealNo);
            ExcelUtil e = downloadSptCheckExcel(map);
            OutputStream ouputStream = response.getOutputStream();
            e.getWb().write(ouputStream);
            ouputStream.flush();
            ouputStream.close();
        } catch (Exception e) {
            logger.info(e);
            throw new RException(e);
        }
    }

    ;

    /**
     * 外汇交易业务校验Excel文件下载内容
     */
    public ExcelUtil downloadSptCheckExcel(Map<String, Object> map) throws Exception {
        List<IfsCfetsfxSpt> list = ifsService.searchSptCheckList(map);
        List<IfsCfetsfxSpt> newList = new ArrayList<IfsCfetsfxSpt>();
        for (IfsCfetsfxSpt ifsCfetsfxSpt : list) {
            IfsCfetsfxSpt spt = new IfsCfetsfxSpt();
            spt = ifsCfetsfxSpt;
            Map<String, Object> opicsmap = new HashMap<String, Object>();
            opicsmap.put("dealno", ifsCfetsfxSpt.getDealNo());
            SlFxdhBean fxdhBean = fxdhServer.getFxdhBean(opicsmap);
            if (fxdhBean != null) {
                spt.setCcyamount(fxdhBean.getCcyamt());
                spt.setCtramount(fxdhBean.getCtramt());
                spt.setCcycheck(ifsCfetsfxSpt.getBuyAmount().subtract(fxdhBean.getCcyamt()));
                spt.setCtrcheck(ifsCfetsfxSpt.getSellAmount().subtract(fxdhBean.getCtramt()));
            } else {
                spt.setCcyamount(new BigDecimal(0));
                spt.setCtramount(new BigDecimal(0));
                spt.setCcycheck(ifsCfetsfxSpt.getBuyAmount().subtract(new BigDecimal(0)));
                spt.setCtrcheck(ifsCfetsfxSpt.getSellAmount().subtract(new BigDecimal(0)));
            }
            newList.add(spt);
        }
        //合计
        IfsCfetsfxSpt fxspt = new IfsCfetsfxSpt();
        BigDecimal ccycheck = new BigDecimal(0);
        BigDecimal ctrcheck = new BigDecimal(0);
        BigDecimal ccyamount = new BigDecimal(0);
        BigDecimal ctramount = new BigDecimal(0);
        BigDecimal buyAmount = new BigDecimal(0);
        BigDecimal sellAmount = new BigDecimal(0);


        for (IfsCfetsfxSpt ifsCfetsfxSpt : newList) {
            ccycheck = ccycheck.add(ifsCfetsfxSpt.getCcycheck());
            ctrcheck = ctrcheck.add(ifsCfetsfxSpt.getCtrcheck());
            ccyamount = ccyamount.add(ifsCfetsfxSpt.getCcyamount());
            ctramount = ctramount.add(ifsCfetsfxSpt.getCtramount());
            buyAmount = buyAmount.add(ifsCfetsfxSpt.getBuyAmount());
            sellAmount = sellAmount.add(ifsCfetsfxSpt.getSellAmount());
        }
        fxspt.setCcycheck(ccycheck);
        fxspt.setCtrcheck(ctrcheck);
        fxspt.setCcyamount(ccyamount);
        fxspt.setCtramount(ctramount);
        fxspt.setBuyAmount(buyAmount);
        fxspt.setSellAmount(sellAmount);
        newList.add(fxspt);

        ExcelUtil eutil = null;
        try {
            // 表头
            eutil = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
            CellStyle center = eutil.getDefaultCenterStrCellStyle();
            int sheet = 0;
            int row = 0;
            // 表格sheet名称
            eutil.getWb().createSheet("外汇交易业务校验报表");
            // 设置表头字段名
            eutil.writeStr(sheet, row, "A", center, "成交日期");
            eutil.writeStr(sheet, row, "B", center, "业务类型");
            eutil.writeStr(sheet, row, "C", center, "成交单号");
            eutil.writeStr(sheet, row, "D", center, "OPICS单号");
            eutil.writeStr(sheet, row, "E", center, "CCY校验");
            eutil.writeStr(sheet, row, "F", center, "CTR校验");
            eutil.writeStr(sheet, row, "G", center, "本地CCY AMOUNT");
            eutil.writeStr(sheet, row, "H", center, "本地CTR AMOUNT");
            eutil.writeStr(sheet, row, "I", center, "CCY AMOUNT");
            eutil.writeStr(sheet, row, "J", center, "CTR AMOUNT");


            // 设置列宽
            eutil.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(8, 20 * 256);
            eutil.getSheetAt(sheet).setColumnWidth(9, 20 * 256);
            for (int i = 0; i < newList.size(); i++) {
                fxspt = newList.get(i);
                row++;
                // 插入数据
                eutil.writeStr(sheet, row, "A", center, fxspt.getForDate() != null && !"".equals(fxspt.getForDate()) ? fxspt.getForDate() : "合计");
                eutil.writeStr(sheet, row, "B", center, fxspt.getProduct());
                eutil.writeStr(sheet, row, "C", center, fxspt.getContractId());
                eutil.writeStr(sheet, row, "D", center, fxspt.getDealNo());
                eutil.writeStr(sheet, row, "E", center, fxspt.getCcycheck() == null ? String.valueOf(0) : String.valueOf(fxspt.getCcycheck()));
                eutil.writeStr(sheet, row, "F", center, fxspt.getCtrcheck() == null ? String.valueOf(0) : String.valueOf(fxspt.getCtrcheck()));
                eutil.writeStr(sheet, row, "G", center, fxspt.getBuyAmount() == null ? String.valueOf(0) : String.valueOf(fxspt.getBuyAmount()));
                eutil.writeStr(sheet, row, "H", center, fxspt.getSellAmount() == null ? String.valueOf(0) : String.valueOf(fxspt.getSellAmount()));
                eutil.writeStr(sheet, row, "I", center, fxspt.getCcyamount() == null ? String.valueOf(0) : String.valueOf(fxspt.getCcyamount()));
                eutil.writeStr(sheet, row, "J", center, fxspt.getCtramount() == null ? String.valueOf(0) : String.valueOf(fxspt.getCtramount()));
            }
            return eutil;
        } catch (Exception e) {
            logger.info(e);
            throw new RException(e);
        }
    }


    /*********************************************************************************************/
    @ResponseBody
    @RequestMapping(value = "/exportFxExposureExcel")
    public void exportFxExposureExcel(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String ua = request.getHeader("User-Agent");
        String trad = request.getParameter("trad");
        String port = request.getParameter("port");
        String ccy = request.getParameter("ccy");
        String br = request.getParameter("br");  //自贸区加br

        String filename = "即期外汇敞口查询.xlsx"; // 文件名
        String encode_filename = "";
        try {
            encode_filename = URLEncoder.encode(filename, "utf-8");
        } catch (UnsupportedEncodingException e1) {
            throw new RException(e1.getMessage());
        }
        response.setContentType("application/vnd.ms-excel");
        if (ua != null) {
            response.setHeader("Content-Disposition", "attachment; filename=" + encode_filename);
        }
        response.setHeader("Connection", "close");
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("trad", trad);
            map.put("port", port);
            map.put("ccy", ccy);
            map.put("br", br);//自贸区加br
            ExcelUtil e = downloadFxExposureExcel(map);
            OutputStream ouputStream = response.getOutputStream();
            e.getWb().write(ouputStream);
            ouputStream.flush();
            ouputStream.close();
        } catch (Exception e) {
            throw new RException(e);
        }
    }


    /**
     * 外汇敞口Excel文件下载内容
     */
    public ExcelUtil downloadFxExposureExcel(Map<String, Object> map) throws Exception {
        ExcelUtil excel = null;
        try {
            /* 先查数据，再设置表格属性以及内容 */

            List<SlExposureBean> list = exposureServer.getFxExposure(map).getList();
            SlExposureBean bean = new SlExposureBean();
            // 表头
            excel = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
            CellStyle center = excel.getDefaultCenterStrCellStyle();
            int sheet = 0;
            int row = 0;
            // 表格sheet名称
            excel.getWb().createSheet("即期外汇敞口查询");
            // 设置表头字段名
            excel.writeStr(sheet, row, "A", center, "部门");
            excel.writeStr(sheet, row, "B", center, "敞口类型");
            excel.writeStr(sheet, row, "C", center, "投资组合");
            excel.writeStr(sheet, row, "D", center, "交易员");
            excel.writeStr(sheet, row, "E", center, "币种");
            excel.writeStr(sheet, row, "F", center, "币种1金额");
            excel.writeStr(sheet, row, "G", center, "币种2金额");
            excel.writeStr(sheet, row, "H", center, "成本汇率");
            excel.writeStr(sheet, row, "I", center, "市场汇率");
            excel.writeStr(sheet, row, "J", center, "折美元浮动金额");
            // 设置列宽
            excel.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(8, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(9, 20 * 256);

            for (int i = 0; i < list.size(); i++) {
                bean = list.get(i);
                row++;
                // 插入数据
                excel.writeStr(sheet, row, "A", center, bean.getBr());
                excel.writeStr(sheet, row, "B", center, bean.getProd());
                excel.writeStr(sheet, row, "C", center, bean.getPort());
                excel.writeStr(sheet, row, "D", center, bean.getTrad());
                excel.writeStr(sheet, row, "E", center, bean.getCcy());
                excel.writeStr(sheet, row, "F", center, bean.getCcy1amt() == null ? "" : bean.getCcy1amt().setScale(4, BigDecimal.ROUND_HALF_UP).toString());
                excel.writeStr(sheet, row, "G", center, bean.getCcy2amt() == null ? "" : bean.getCcy2amt().setScale(4, BigDecimal.ROUND_HALF_UP).toString());
                excel.writeStr(sheet, row, "H", center, bean.getBidprice() == null ? "" : bean.getBidprice().setScale(8, BigDecimal.ROUND_HALF_UP).toString());
                excel.writeStr(sheet, row, "I", center, bean.getRmbprice() == null ? "" : bean.getRmbprice().setScale(8, BigDecimal.ROUND_HALF_UP).toString());
                excel.writeStr(sheet, row, "J", center, bean.getUsdamt() == null ? "" : bean.getUsdamt().setScale(4, BigDecimal.ROUND_HALF_UP).toString());

            }
            return excel;
        } catch (Exception e) {
            throw new RException(e);
        }
    }

    /**
     * 导出历史即期外汇敞口Excel文件
     */
    @ResponseBody
    @RequestMapping(value = "/exportFxExposureHistoryExcel")
    public void exportFxExposureHistoryExcel(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String ua = request.getHeader("User-Agent");
        String postdate = request.getParameter("postdate");
        String br = request.getParameter("br");  //自贸区加br

        String filename = "历史即期外汇敞口查询.xlsx"; // 文件名
        String encode_filename = "";
        try {
            encode_filename = URLEncoder.encode(filename, "utf-8");
        } catch (UnsupportedEncodingException e1) {
            throw new RException(e1.getMessage());
        }
        response.setContentType("application/vnd.ms-excel");
        if (ua != null) {
            response.setHeader("Content-Disposition", "attachment; filename=" + encode_filename);
        }
        response.setHeader("Connection", "close");
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("postdate", postdate);
            map.put("br", br);//自贸区加br
            ExcelUtil e = downloadFxExposureHistoryExcel(map);
            OutputStream ouputStream = response.getOutputStream();
            e.getWb().write(ouputStream);
            ouputStream.flush();
            ouputStream.close();
        } catch (Exception e) {
            throw new RException(e);
        }
    }

    /**
     * 历史外汇敞口Excel文件下载内容
     */
    public ExcelUtil downloadFxExposureHistoryExcel(Map<String, Object> map) throws Exception {
        ExcelUtil excel = null;
        try {
            /* 先查数据，再设置表格属性以及内容 */

            List<SlExposureBean> list = exposureServer.getFxExposureHistory(map).getList();
            SlExposureBean bean = new SlExposureBean();
            // 表头
            excel = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
            CellStyle center = excel.getDefaultCenterStrCellStyle();
            int sheet = 0;
            int row = 0;
            // 表格sheet名称
            excel.getWb().createSheet("历史即期外汇敞口查询");
            // 设置表头字段名
            excel.writeStr(sheet, row, "A", center, "部门");
            excel.writeStr(sheet, row, "B", center, "敞口类型");
            excel.writeStr(sheet, row, "C", center, "投资组合");
            excel.writeStr(sheet, row, "D", center, "交易员");
            excel.writeStr(sheet, row, "E", center, "币种");
            excel.writeStr(sheet, row, "F", center, "币种1金额");
            excel.writeStr(sheet, row, "G", center, "币种2金额");
            excel.writeStr(sheet, row, "H", center, "成本汇率");
            excel.writeStr(sheet, row, "I", center, "市场汇率");
            excel.writeStr(sheet, row, "J", center, "折美元浮动金额");
            // 设置列宽
            excel.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(8, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(9, 20 * 256);

            for (int i = 0; i < list.size(); i++) {
                bean = list.get(i);
                row++;
                // 插入数据
                excel.writeStr(sheet, row, "A", center, bean.getBr());
                excel.writeStr(sheet, row, "B", center, bean.getProd());
                excel.writeStr(sheet, row, "C", center, bean.getPort());
                excel.writeStr(sheet, row, "D", center, bean.getTrad());
                excel.writeStr(sheet, row, "E", center, bean.getCcy());
                excel.writeStr(sheet, row, "F", center, bean.getCcy1amt() == null ? "" : bean.getCcy1amt().setScale(4, BigDecimal.ROUND_HALF_UP).toString());
                excel.writeStr(sheet, row, "G", center, bean.getCcy2amt() == null ? "" : bean.getCcy2amt().setScale(4, BigDecimal.ROUND_HALF_UP).toString());
                excel.writeStr(sheet, row, "H", center, bean.getBidprice() == null ? "" : bean.getBidprice().setScale(8, BigDecimal.ROUND_HALF_UP).toString());
                excel.writeStr(sheet, row, "I", center, bean.getRmbprice() == null ? "" : bean.getRmbprice().setScale(8, BigDecimal.ROUND_HALF_UP).toString());
                excel.writeStr(sheet, row, "J", center, bean.getUsdamt() == null ? "" : bean.getUsdamt().setScale(4, BigDecimal.ROUND_HALF_UP).toString());

            }
            return excel;
        } catch (Exception e) {
            throw new RException(e);
        }
    }

    /**
     * 导出结售汇敞口Excel
     */
    @ResponseBody
    @RequestMapping(value = "/exportFxSettleExposureExcel")
    public void exportFxSettleExposureExcel(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String ua = request.getHeader("User-Agent");
        String trad = request.getParameter("trad");
        String port = request.getParameter("port");
        String ccy = request.getParameter("ccy");
        String br = request.getParameter("br");  //自贸区加br

        String filename = "即期结售汇敞口查询.xlsx"; // 文件名
        String encode_filename = "";
        try {
            encode_filename = URLEncoder.encode(filename, "utf-8");
        } catch (UnsupportedEncodingException e1) {
            throw new RException(e1.getMessage());
        }
        response.setContentType("application/vnd.ms-excel");
        if (ua != null) {
            response.setHeader("Content-Disposition", "attachment; filename=" + encode_filename);
        }
        response.setHeader("Connection", "close");
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("trad", trad);
            map.put("port", port);
            map.put("ccy", ccy);
            map.put("br", br);//自贸区加br
            ExcelUtil e = downloadFxSettleExposureExcel(map);
            OutputStream ouputStream = response.getOutputStream();
            e.getWb().write(ouputStream);
            ouputStream.flush();
            ouputStream.close();
        } catch (Exception e) {
            throw new RException(e);
        }
    }

    /**
     * 结售汇敞口Excel文件下载内容
     */
    public ExcelUtil downloadFxSettleExposureExcel(Map<String, Object> map) throws Exception {
        ExcelUtil excel = null;
        try {
            /* 先查数据，再设置表格属性以及内容 */

            List<SlExposureBean> list = exposureServer.getFxSettleExposure(map).getList();
            SlExposureBean bean = new SlExposureBean();
            // 表头
            excel = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
            CellStyle center = excel.getDefaultCenterStrCellStyle();
            int sheet = 0;
            int row = 0;
            // 表格sheet名称
            excel.getWb().createSheet("即期结售汇敞口查询");
            // 设置表头字段名
            excel.writeStr(sheet, row, "A", center, "部门");
            excel.writeStr(sheet, row, "B", center, "敞口类型");
            excel.writeStr(sheet, row, "C", center, "投资组合");
            excel.writeStr(sheet, row, "D", center, "交易员");
            excel.writeStr(sheet, row, "E", center, "币种");
            excel.writeStr(sheet, row, "F", center, "外币金额");
            /*excel.writeStr(sheet, row, "G", center, "人民币金额");*/
            excel.writeStr(sheet, row, "G", center, "预警状态");
            excel.writeStr(sheet, row, "H", center, "成本汇率");
            excel.writeStr(sheet, row, "I", center, "市场汇率");
            excel.writeStr(sheet, row, "J", center, "浮动损益(CNY)");
            // 设置列宽
            excel.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(8, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(9, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(10, 20 * 256);
            for (int i = 0; i < list.size(); i++) {
                bean = list.get(i);
                row++;
                // 插入数据
                //DecimalFormat df = new DecimalFormat("#,##0.0000");
                excel.writeStr(sheet, row, "A", center, bean.getBr());
                excel.writeStr(sheet, row, "B", center, bean.getProd());
                excel.writeStr(sheet, row, "C", center, bean.getPort());
                excel.writeStr(sheet, row, "D", center, bean.getTrad());
                excel.writeStr(sheet, row, "E", center, bean.getCcy());
                excel.writeStr(sheet, row, "F", center, bean.getAmt() == null ? "" : bean.getAmt().setScale(4, BigDecimal.ROUND_HALF_UP).toString());
                /*excel.writeStr(sheet, row, "G", center, bean.getCcy2amt()==null?"":bean.getCcy2amt().setScale(4, BigDecimal.ROUND_HALF_UP).toString());*/
                excel.writeStr(sheet, row, "G", center, bean.getCe());
                excel.writeStr(sheet, row, "H", center, bean.getBidprice() == null ? "" : bean.getBidprice().setScale(8, BigDecimal.ROUND_HALF_UP).toString());
                excel.writeStr(sheet, row, "I", center, bean.getPrice() == null ? "" : bean.getPrice().setScale(8, BigDecimal.ROUND_HALF_UP).toString());
                excel.writeStr(sheet, row, "J", center, bean.getPlamt() == null ? "" : bean.getPlamt().setScale(4, BigDecimal.ROUND_HALF_UP).toString());

            }
            return excel;
        } catch (Exception e) {
            throw new RException(e);
        }
    }


    /**
     * 历史导出结售汇敞口Excel
     */
    @ResponseBody
    @RequestMapping(value = "/exportFxSettleExposureHistoryExcel")
    public void exportFxSettleExposureHistoryExcel(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String ua = request.getHeader("User-Agent");
        String postdate = request.getParameter("postdate");
        String br = request.getParameter("br");  //自贸区加br

        String filename = "历史即期结售汇敞口查询.xlsx"; // 文件名
        String encode_filename = "";
        try {
            encode_filename = URLEncoder.encode(filename, "utf-8");
        } catch (UnsupportedEncodingException e1) {
            throw new RException(e1.getMessage());
        }
        response.setContentType("application/vnd.ms-excel");
        if (ua != null) {
            response.setHeader("Content-Disposition", "attachment; filename=" + encode_filename);
        }
        response.setHeader("Connection", "close");
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("postdate", postdate);
            map.put("br", br);//自贸区加br
            ExcelUtil e = downloadFxSettleExposureHExcelHistory(map);
            OutputStream ouputStream = response.getOutputStream();
            e.getWb().write(ouputStream);
            ouputStream.flush();
            ouputStream.close();
        } catch (Exception e) {
            throw new RException(e);
        }
    }

    /**
     * 结售汇敞口Excel文件下载内容
     */
    public ExcelUtil downloadFxSettleExposureHExcelHistory(Map<String, Object> map) throws Exception {
        ExcelUtil excel = null;
        try {
            /* 先查数据，再设置表格属性以及内容 */

            List<SlExposureBean> list = exposureServer.getFxSettleExposureHistory(map).getList();
            SlExposureBean bean = new SlExposureBean();
            // 表头
            excel = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
            CellStyle center = excel.getDefaultCenterStrCellStyle();
            int sheet = 0;
            int row = 0;
            // 表格sheet名称
            excel.getWb().createSheet("历史即期结售汇敞口查询");
            // 设置表头字段名
            excel.writeStr(sheet, row, "A", center, "部门");
            excel.writeStr(sheet, row, "B", center, "敞口类型");
            excel.writeStr(sheet, row, "C", center, "投资组合");
            excel.writeStr(sheet, row, "D", center, "交易员");
            excel.writeStr(sheet, row, "E", center, "币种");
            excel.writeStr(sheet, row, "F", center, "外币金额");
            /*excel.writeStr(sheet, row, "G", center, "人民币金额");*/
            excel.writeStr(sheet, row, "G", center, "预警状态");
            excel.writeStr(sheet, row, "H", center, "成本汇率");
            excel.writeStr(sheet, row, "I", center, "市场汇率");
            excel.writeStr(sheet, row, "J", center, "浮动损益(CNY)");
            // 设置列宽
            excel.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(8, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(9, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(10, 20 * 256);
            for (int i = 0; i < list.size(); i++) {
                bean = list.get(i);
                row++;
                // 插入数据
                //DecimalFormat df = new DecimalFormat("#,##0.0000");
                excel.writeStr(sheet, row, "A", center, bean.getBr());
                excel.writeStr(sheet, row, "B", center, bean.getProd());
                excel.writeStr(sheet, row, "C", center, bean.getPort());
                excel.writeStr(sheet, row, "D", center, bean.getTrad());
                excel.writeStr(sheet, row, "E", center, bean.getCcy());
                excel.writeStr(sheet, row, "F", center, bean.getAmt() == null ? "" : bean.getAmt().setScale(4, BigDecimal.ROUND_HALF_UP).toString());
                /*excel.writeStr(sheet, row, "G", center, bean.getCcy2amt()==null?"":bean.getCcy2amt().setScale(4, BigDecimal.ROUND_HALF_UP).toString());*/
                excel.writeStr(sheet, row, "G", center, bean.getCe());
                excel.writeStr(sheet, row, "H", center, bean.getBidprice() == null ? "" : bean.getBidprice().setScale(8, BigDecimal.ROUND_HALF_UP).toString());
                excel.writeStr(sheet, row, "I", center, bean.getPrice() == null ? "" : bean.getPrice().setScale(8, BigDecimal.ROUND_HALF_UP).toString());
                excel.writeStr(sheet, row, "J", center, bean.getPlamt() == null ? "" : bean.getPlamt().setScale(4, BigDecimal.ROUND_HALF_UP).toString());

            }
            return excel;
        } catch (Exception e) {
            throw new RException(e);
        }
    }


    /**
     * 导出现金流Excel
     */
    @ResponseBody
    @RequestMapping(value = "/exportFxCashFlowExcel")
    public void exportFxCashFlowExcel(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String ua = request.getHeader("User-Agent");
        String port = request.getParameter("port");
        String ccy = request.getParameter("ccy");
        String prod = request.getParameter("prod");
        String br = request.getParameter("br");  //自贸区加br

        String filename = "现金流.xlsx"; // 文件名
        String encode_filename = "";
        try {
            encode_filename = URLEncoder.encode(filename, "utf-8");
        } catch (UnsupportedEncodingException e1) {
            throw new RException(e1.getMessage());
        }
        response.setContentType("application/vnd.ms-excel");
        if (ua != null) {
            response.setHeader("Content-Disposition", "attachment; filename=" + encode_filename);
        }
        response.setHeader("Connection", "close");
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("ccy", ccy);
            map.put("port", port);
            map.put("prod", prod);
            map.put("br", br);//自贸区加br
            ExcelUtil e = downloadFxCashFlowExcel(map);
            OutputStream ouputStream = response.getOutputStream();
            e.getWb().write(ouputStream);
            ouputStream.flush();
            ouputStream.close();
        } catch (Exception e) {
            throw new RException(e);
        }
    }

    /**
     * 现金流Excel文件下载内容
     */
    public ExcelUtil downloadFxCashFlowExcel(Map<String, Object> map) throws Exception {
        ExcelUtil excel = null;
        try {
            /* 先查数据，再设置表格属性以及内容 */

            List<SlFxCashFlowBean> list = fxdhServer.searchFxCashFlowPage(map).getList();
            SlFxCashFlowBean bean = new SlFxCashFlowBean();
            // 表头
            excel = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
            CellStyle center = excel.getDefaultCenterStrCellStyle();
            int sheet = 0;
            int row = 0;
            // 表格sheet名称
            excel.getWb().createSheet("现金流");
            // 设置表头字段名
            excel.writeStr(sheet, row, "A", center, "部门");
            excel.writeStr(sheet, row, "B", center, "产品类型");
            excel.writeStr(sheet, row, "C", center, "投资组合");
            excel.writeStr(sheet, row, "D", center, "起息日");
            excel.writeStr(sheet, row, "E", center, "币种1");
            excel.writeStr(sheet, row, "F", center, "币种1金额");
            excel.writeStr(sheet, row, "G", center, "币种2");
            excel.writeStr(sheet, row, "H", center, "币种2金额");

            // 设置列宽
            excel.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(7, 20 * 256);

            for (int i = 0; i < list.size(); i++) {
                bean = list.get(i);
                row++;
                // 插入数据
                excel.writeStr(sheet, row, "A", center, bean.getBr());
                excel.writeStr(sheet, row, "B", center, bean.getProd());
                excel.writeStr(sheet, row, "C", center, bean.getPort());
                excel.writeStr(sheet, row, "D", center, bean.getVdate() == null ? "" : bean.getVdate().substring(0, 10));
                excel.writeStr(sheet, row, "E", center, bean.getCcy1());
                excel.writeStr(sheet, row, "F", center, bean.getCcy1amt() == null ? "" : bean.getCcy1amt().setScale(4, BigDecimal.ROUND_HALF_UP).toString());
                excel.writeStr(sheet, row, "G", center, bean.getCcy2());
                excel.writeStr(sheet, row, "H", center, bean.getCcy2amt() == null ? "" : bean.getCcy2amt().setScale(4, BigDecimal.ROUND_HALF_UP).toString());
            }
            return excel;
        } catch (Exception e) {
            throw new RException(e);
        }
    }

    /**
     * 交易审批统计报表
     *
     * @param request
     * @param response
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "/exportDealAppCount")
    public void exportDealAppCount(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String ua = request.getHeader("User-Agent");
        String oper = request.getParameter("oper");
        String opername = request.getParameter("opername");
        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        String userId = request.getParameter("userId");
        String pageNumber = request.getParameter("pageNumber");
        String pageSize = request.getParameter("pageSize");
        String br = request.getParameter("br");  //自贸区加br

        String filename = "交易审批统计.xlsx"; // 文件名
        String encode_filename = "";
        try {
            encode_filename = URLEncoder.encode(filename, "utf-8");
        } catch (UnsupportedEncodingException e1) {
            throw new RException(e1.getMessage());
        }
        response.setContentType("application/vnd.ms-excel");
        if (ua != null) {
            response.setHeader("Content-Disposition", "attachment; filename=" + encode_filename);
        }
        response.setHeader("Connection", "close");
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("oper", oper);
            map.put("opername", opername);
            map.put("startDate", startDate);
            map.put("endDate", endDate);
            map.put("userId", userId);
            map.put("pageNumber", pageNumber);
            map.put("pageSize", pageSize);
            map.put("br", br);//自贸区加br
            ExcelUtil e = downloadDealAppCountExcel(map);
            OutputStream ouputStream = response.getOutputStream();
            e.getWb().write(ouputStream);
            ouputStream.flush();
            ouputStream.close();
        } catch (Exception e) {
            throw new RException(e);
        }
    }

    /**
     * 交易审批统计Excel文件下载内容
     */
    public ExcelUtil downloadDealAppCountExcel(Map<String, Object> map) throws Exception {
        ExcelUtil excel = null;
        try {
            /* 先查数据，再设置表格属性以及内容 */
            RowBounds rb = ParameterUtil.getRowBounds(map);
            map.put("isAdmin", 1);//查询所有审批中，审批完成交易
            List<IfsFlowMonitor> applist = ifsFlowMonitorMapper.searchPageAllFlowMonitor(map, rb);
            String opername = ParameterUtil.getString(map, "opername", "");
            String oper = ParameterUtil.getString(map, "oper", "");
            // 表头
            excel = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
            CellStyle center = excel.getDefaultCenterStrCellStyle();
            CellStyle left = excel.getDefaultLeftStrCellStyle();
            int sheet = 0;
            int row = 0;
            int count = 0;
            // 表格sheet名称
            excel.getWb().createSheet("交易审批统计");
            // 设置表头字段名
            if (!StringUtil.isEmpty(opername)) {
                excel.writeStr(sheet, row, "A", center, "审批人");
                excel.writeStr(sheet, row, "B", center, opername);
                excel.writeStr(sheet, row, "C", center, "审批交易总笔数");
                row = row + 2;
            }
            excel.writeStr(sheet, row, "A", center, "审批单编号");
            excel.writeStr(sheet, row, "B", center, "成交单编号");
            excel.writeStr(sheet, row, "C", center, "交易日期");
            excel.writeStr(sheet, row, "D", center, "起息日");
            excel.writeStr(sheet, row, "E", center, "方向");
            excel.writeStr(sheet, row, "F", center, "交易货币");
            excel.writeStr(sheet, row, "G", center, "金额1");
            excel.writeStr(sheet, row, "H", center, "对应货币");
            excel.writeStr(sheet, row, "I", center, "金额2");
            excel.writeStr(sheet, row, "J", center, "审批状态");
            excel.writeStr(sheet, row, "K", center, "opics处理状态");
            excel.writeStr(sheet, row, "L", center, "opics编号");
            excel.writeStr(sheet, row, "M", center, "产品名称");
            excel.writeStr(sheet, row, "N", center, "审批发起人");
            excel.writeStr(sheet, row, "O", center, "审批人列表");
            // 设置列宽
            excel.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(8, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(9, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(10, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(11, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(12, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(13, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(14, 80 * 256);
            for (int i = 0; i < applist.size(); i++) {
                IfsFlowMonitor ifsFlowMonitor = applist.get(i);
                TtFlowSerialMap flowMap = ttFlowSerialMapMapper.get(null, ifsFlowMonitor.getTicketId(), null, 0);
                String ulist = null;
                boolean f = false;
                if (flowMap != null) {
                    List<HistoricTaskInstance> hislist = historyService.createHistoricTaskInstanceQuery().processInstanceId(String.valueOf(flowMap.getExecution())).orderByTaskCreateTime().asc().list();
                    for (HistoricTaskInstance his : hislist) {//选择审批人是否存在审批列表
                        TaUser user = userService.getUserById(his.getAssignee());
                        if (user != null) {
                            ulist = StringUtil.isEmpty(ulist) ? user.getUserName() : ulist + " - " + user.getUserName();
                            if ((StringUtil.isNotEmpty(his.getAssignee()) & oper.equals(his.getAssignee())) || StringUtil.isEmpty(opername)) {
                                f = true;
                            }
                        }
                    }
                    if (f) {
                        row++;
                        ifsFlowMonitor.setApproveList(ulist);
                        excel.writeStr(sheet, row, "A", center, ifsFlowMonitor.getTicketId());
                        excel.writeStr(sheet, row, "B", left, ifsFlowMonitor.getContractId());
                        excel.writeStr(sheet, row, "C", center, ifsFlowMonitor.getTradeDate());// 交易日期
                        excel.writeStr(sheet, row, "D", center, ifsFlowMonitor.getvDate()); // 起息日
                        excel.writeStr(sheet, row, "E", center, ifsFlowMonitor.getMydircn()); // 反向
                        excel.writeStr(sheet, row, "F", center, ifsFlowMonitor.getTccy()); // 交易货币
                        excel.writeStr(sheet, row, "G", center, ifsFlowMonitor.getPamt()); // 金额1
                        excel.writeStr(sheet, row, "H", left, ifsFlowMonitor.getOccy()); // 对应货币
                        excel.writeStr(sheet, row, "I", center, ifsFlowMonitor.getRamt()); // 金额2
                        excel.writeStr(sheet, row, "J", center, ifsFlowMonitor.getTaskName());
                        Map<String, String> amap = new HashMap<String, String>();
                        amap.put("dictId", "statcode");
                        amap.put("dictKey", ifsFlowMonitor.getStatcode());
                        excel.writeStr(sheet, row, "K", center, ifsService.queryDictValue(amap));
                        excel.writeStr(sheet, row, "L", center, ifsFlowMonitor.getDealno());
                        excel.writeStr(sheet, row, "M", center, ifsFlowMonitor.getPrdName());
                        excel.writeStr(sheet, row, "N", center, ifsFlowMonitor.getSponsor());
                        excel.writeStr(sheet, row, "O", left, ifsFlowMonitor.getApproveList());
                        count++;
                    }
                }
            }
            if (!StringUtil.isEmpty(opername)) {
                excel.writeStr(sheet, 0, "D", center, String.valueOf(count));
            }
            return excel;
        } catch (Exception e) {
            throw new RException(e);
        }
    }

    /**
     * 大额清算统计
     *
     * @param request
     * @param response
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "/exportCnapsCount")
    public void exportCnapsCount(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String ua = request.getHeader("User-Agent");
        String oper = request.getParameter("oper");
        String opername = request.getParameter("opername");
        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        String br = request.getParameter("br");  //自贸区加br

        String filename = "大额清算交易统计.xlsx"; // 文件名
        String encode_filename = "";
        try {
            encode_filename = URLEncoder.encode(filename, "utf-8");
        } catch (UnsupportedEncodingException e1) {
            throw new RException(e1.getMessage());
        }
        response.setContentType("application/vnd.ms-excel");
        if (ua != null) {
            response.setHeader("Content-Disposition", "attachment; filename=" + encode_filename);
        }
        response.setHeader("Connection", "close");
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("oper", oper);
            map.put("opername", opername);
            map.put("startDate", startDate);
            map.put("endDate", endDate);
            map.put("br", br);//自贸区加br
            ExcelUtil e = downloadCnapsCountExcel(map);
            OutputStream ouputStream = response.getOutputStream();
            e.getWb().write(ouputStream);
            ouputStream.flush();
            ouputStream.close();
        } catch (Exception e) {
            throw new RException(e);
        }
    }

    /**
     * 大额清算统计Excel文件下载内容
     */
    public ExcelUtil downloadCnapsCountExcel(Map<String, Object> map) throws Exception {
        ExcelUtil excel = null;
        try {
            String opername = ParameterUtil.getString(map, "opername", "");
            String oper = ParameterUtil.getString(map, "oper", "");
            /* 先查数据，再设置表格属性以及内容 */
            List<IfsTrdSettle> list = trdsettleservice.getTrdSettleCount(map);
            // 表头
            excel = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
            CellStyle center = excel.getDefaultCenterStrCellStyle();
            int sheet = 0;
            int row = 0;
            int icount = 0;
            int vcount = 0;
            // 表格sheet名称
            excel.getWb().createSheet("大额清算统计");
            // 设置表头字段名
            if (!StringUtil.isEmpty(opername)) {
                excel.writeStr(sheet, row, "A", center, "经办/复核人");
                excel.writeStr(sheet, row, "B", center, opername);
                excel.writeStr(sheet, row, "C", center, "经办交易笔数");
                excel.writeStr(sheet, row, "D", center, String.valueOf(list.size()));
                excel.writeStr(sheet, row, "E", center, "复核交易笔数");
                excel.writeStr(sheet, row, "F", center, String.valueOf(list.size()));
                row = row + 2;
            }
            excel.writeStr(sheet, row, "A", center, "流水号");
            excel.writeStr(sheet, row, "B", center, "产品");
            excel.writeStr(sheet, row, "C", center, "处理状态");
            excel.writeStr(sheet, row, "D", center, "清算状态");
            excel.writeStr(sheet, row, "E", center, "结算金额");
            excel.writeStr(sheet, row, "F", center, "付款人账号");
            excel.writeStr(sheet, row, "G", center, "付款人名称");
            excel.writeStr(sheet, row, "H", center, "收款人账号");
            excel.writeStr(sheet, row, "I", center, "收款人名称");
            excel.writeStr(sheet, row, "J", center, "经办人名称");
            excel.writeStr(sheet, row, "K", center, "经办时间");
            excel.writeStr(sheet, row, "L", center, "复核人名称");
            excel.writeStr(sheet, row, "M", center, "复核时间");
            // 设置列宽
            excel.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(8, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(9, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(10, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(11, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(12, 20 * 256);
            for (int i = 0; i < list.size(); i++) {
                row++;
                IfsTrdSettle ifsTrdSettle = list.get(i);
                Map<String, String> upmap = new HashMap<String, String>();
                Map<String, String> amap = new HashMap<String, String>();
                upmap.put("dictId", "rbstatus");
                upmap.put("dictKey", ifsTrdSettle.getSettflag());//处理状态
                amap.put("dictId", "handleflag");
                amap.put("dictKey", ifsTrdSettle.getDealstatus());//清算状态
                excel.writeStr(sheet, row, "A", center, ifsTrdSettle.getDealno());
                excel.writeStr(sheet, row, "B", center, ifsTrdSettle.getProduct());
                excel.writeStr(sheet, row, "C", center, ifsService.queryDictValue(upmap));
                excel.writeStr(sheet, row, "D", center, ifsService.queryDictValue(amap));
                excel.writeStr(sheet, row, "E", center, ifsTrdSettle.getAmount() == null ? "" : ifsTrdSettle.getAmount().setScale(4, BigDecimal.ROUND_HALF_UP).toString());
                excel.writeStr(sheet, row, "F", center, ifsTrdSettle.getPayBankid());
                excel.writeStr(sheet, row, "G", center, ifsTrdSettle.getPayBankname());
                excel.writeStr(sheet, row, "H", center, ifsTrdSettle.getRecUserid());
                excel.writeStr(sheet, row, "I", center, ifsTrdSettle.getRecUsername());
                excel.writeStr(sheet, row, "J", center, ifsTrdSettle.getIopername());
                excel.writeStr(sheet, row, "K", center, ifsTrdSettle.getItime());
                excel.writeStr(sheet, row, "L", center, ifsTrdSettle.getVopername());
                excel.writeStr(sheet, row, "M", center, ifsTrdSettle.getVtime());
                if (oper.equals(ifsTrdSettle.getIoper())) {
                    icount++;
                }
                if (oper.equals(ifsTrdSettle.getVoper())) {
                    vcount++;
                }
            }
            if (!StringUtil.isEmpty(opername)) {
                excel.writeStr(sheet, 0, "D", center, String.valueOf(icount));
                excel.writeStr(sheet, 0, "F", center, String.valueOf(vcount));
            }
            return excel;
        } catch (Exception e) {
            throw new RException(e);
        }
    }

    /**
     * 拆出外币 报表导出
     *
     * @param request
     * @param response
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "/exportDldtCount")
    public void exportDldtCount(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String ua = request.getHeader("User-Agent");
        String br = request.getParameter("br");//部门
        String cust = request.getParameter("cust");//部门
        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        String filename = "拆出外币统计.xlsx"; // 文件名
        String encode_filename = "";
        try {
            encode_filename = URLEncoder.encode(filename, "utf-8");
        } catch (UnsupportedEncodingException e1) {
            throw new RException(e1.getMessage());
        }
        response.setContentType("application/vnd.ms-excel");
        if (ua != null) {
            response.setHeader("Content-Disposition", "attachment; filename=" + encode_filename);
        }
        response.setHeader("Connection", "close");
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("startDate", startDate);
            map.put("endDate", endDate);
            map.put("br", br);
            map.put("cust", cust);
            ExcelUtil e = downloadDldtCountExcel(map);
            OutputStream ouputStream = response.getOutputStream();
            e.getWb().write(ouputStream);
            ouputStream.flush();
            ouputStream.close();
        } catch (Exception e) {
            throw new RException(e);
        }
    }

    /**
     * 拆出外币统计Excel文件下载内容
     */
    public ExcelUtil downloadDldtCountExcel(Map<String, Object> map) throws Exception {
        ExcelUtil excel = null;
        try {
            //String br=ParameterUtil.getString(map, "br", "");
            /* 先查数据，再设置表格属性以及内容 */
            List<SlDldtBean> list = iDldtCountServer.searchDlCountlis(map);
            // 表头
            excel = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
            CellStyle center = excel.getDefaultCenterStrCellStyle();
            int sheet = 0;
            int row = 0;
            // 表格sheet名称
            excel.getWb().createSheet("拆出外币统计");
            // 设置表头字段名
            excel.writeStr(sheet, row, "A", center, "交易对手");
            excel.writeStr(sheet, row, "B", center, "证券代码");
            excel.writeStr(sheet, row, "C", center, "证券名称");
            excel.writeStr(sheet, row, "D", center, "交易日");
            excel.writeStr(sheet, row, "E", center, "结算日");
            excel.writeStr(sheet, row, "F", center, "到期日");
            excel.writeStr(sheet, row, "G", center, "拆借期限(天)");
            excel.writeStr(sheet, row, "H", center, "拆借利率");
            excel.writeStr(sheet, row, "I", center, "拆出金额");
            excel.writeStr(sheet, row, "J", center, "到期还款额");
            excel.writeStr(sheet, row, "K", center, "币种");

            // 设置列宽
            excel.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(8, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(9, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(10, 20 * 256);
            for (int i = 0; i < list.size(); i++) {
                row++;
                SlDldtBean slDldtBean = list.get(i);
                DecimalFormat df = new DecimalFormat("#0.00");
                excel.writeStr(sheet, row, "A", center, slDldtBean.getCmne());
                excel.writeStr(sheet, row, "B", center, slDldtBean.getSecid());
                excel.writeStr(sheet, row, "C", center, slDldtBean.getDescr());
                excel.writeStr(sheet, row, "D", center, slDldtBean.getDealDate().substring(0, 10));
                excel.writeStr(sheet, row, "E", center, slDldtBean.getvDate().substring(0, 10));
                excel.writeStr(sheet, row, "F", center, slDldtBean.getmDate().substring(0, 10));
                excel.writeStr(sheet, row, "G", center, slDldtBean.getTerm());
                excel.writeStr(sheet, row, "H", center, slDldtBean.getIntRate());
                excel.writeStr(sheet, row, "I", center, df.format(Double.valueOf(slDldtBean.getCcyAmt())));
                excel.writeStr(sheet, row, "J", center, df.format(Double.valueOf(slDldtBean.getAmount())));
                excel.writeStr(sheet, row, "K", center, slDldtBean.getCcy());
            }
            return excel;
        } catch (Exception e) {
            throw new RException(e);
        }
    }

    /**
     * 货币互换头寸敞口 报表导出
     *
     * @param request
     * @param response
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "/exportCurSwapCount")
    public void exportCurSwapCount(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String ua = request.getHeader("User-Agent");
        String br = request.getParameter("br");//部门
        String trad = request.getParameter("trad");//交易员
        String ccy = request.getParameter("ccy");//币种
        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        String filename = "货币互换头寸敞口.xlsx"; // 文件名
        String encode_filename = "";
        try {
            encode_filename = URLEncoder.encode(filename, "utf-8");
        } catch (UnsupportedEncodingException e1) {
            throw new RException(e1.getMessage());
        }
        response.setContentType("application/vnd.ms-excel");
        if (ua != null) {
            response.setHeader("Content-Disposition", "attachment; filename=" + encode_filename);
        }
        response.setHeader("Connection", "close");
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("br", br);
            map.put("trad", trad);
            map.put("ccy", ccy);
            map.put("startDate", startDate);
            map.put("endDate", endDate);
            ExcelUtil e = downloadCurSwapCountExcel(map);
            OutputStream ouputStream = response.getOutputStream();
            e.getWb().write(ouputStream);
            ouputStream.flush();
            ouputStream.close();
        } catch (Exception e) {
            throw new RException(e);
        }
    }

    /**
     * 货币互换头寸敞口  统计Excel文件下载内容
     */
    public ExcelUtil downloadCurSwapCountExcel(Map<String, Object> map) throws Exception {
        ExcelUtil excel = null;
        try {
            //String br=ParameterUtil.getString(map, "br", "");
            /* 先查数据，再设置表格属性以及内容 */
            List<SlSwapCurBean> list = swapServer.getSwapCrsCount(map);
            // 表头
            excel = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
            CellStyle center = excel.getDefaultCenterStrCellStyle();
            int sheet = 0;
            int row = 0;
            // 表格sheet名称
            excel.getWb().createSheet("货币互换头寸敞口统计");
            // 设置表头字段名
            excel.writeStr(sheet, row, "A", center, "机构");
            excel.writeStr(sheet, row, "B", center, "交易号");
            excel.writeStr(sheet, row, "C", center, "产品");
            excel.writeStr(sheet, row, "D", center, "产品类型");
            excel.writeStr(sheet, row, "E", center, "产品中心号");
            excel.writeStr(sheet, row, "F", center, "交易对手");
            excel.writeStr(sheet, row, "G", center, "交易员");
            excel.writeStr(sheet, row, "H", center, "交易日期");
            excel.writeStr(sheet, row, "I", center, "起息日");
            excel.writeStr(sheet, row, "J", center, "到期日");
            excel.writeStr(sheet, row, "K", center, "互换币种");
            excel.writeStr(sheet, row, "L", center, "交易方向");
            excel.writeStr(sheet, row, "M", center, "利率代码");
            excel.writeStr(sheet, row, "N", center, "本金交割方式");
            excel.writeStr(sheet, row, "O", center, "期初交割日");
            excel.writeStr(sheet, row, "P", center, "币种1");
            excel.writeStr(sheet, row, "Q", center, "币种1金额");
            excel.writeStr(sheet, row, "R", center, "币种2");
            excel.writeStr(sheet, row, "S", center, "币种2金额");
            excel.writeStr(sheet, row, "T", center, "期末交割日");
            excel.writeStr(sheet, row, "U", center, "币种1");
            excel.writeStr(sheet, row, "V", center, "币种1金额");
            excel.writeStr(sheet, row, "W", center, "币种2");
            excel.writeStr(sheet, row, "X", center, "币种2金额");

            // 设置列宽
            excel.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(8, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(9, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(10, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(11, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(12, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(13, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(14, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(15, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(16, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(17, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(18, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(19, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(20, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(21, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(22, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(23, 20 * 256);
            for (int i = 0; i < list.size(); i++) {
                row++;
                SlSwapCurBean swapBean = list.get(i);
                DecimalFormat df = new DecimalFormat("#0.00");
                excel.writeStr(sheet, row, "A", center, swapBean.getBr());
                excel.writeStr(sheet, row, "B", center, swapBean.getDealno());
                excel.writeStr(sheet, row, "C", center, swapBean.getProduct());
                excel.writeStr(sheet, row, "D", center, swapBean.getProdtype());
                excel.writeStr(sheet, row, "E", center, swapBean.getCost());
                excel.writeStr(sheet, row, "F", center, swapBean.getCmne());
                excel.writeStr(sheet, row, "G", center, swapBean.getTrad());
                excel.writeStr(sheet, row, "H", center, swapBean.getDealdate().substring(0, 10));
                excel.writeStr(sheet, row, "I", center, swapBean.getSettledate().substring(0, 10));
                excel.writeStr(sheet, row, "J", center, swapBean.getMatdate().substring(0, 10));
                excel.writeStr(sheet, row, "K", center, swapBean.getCurrency());
                excel.writeStr(sheet, row, "L", center, swapBean.getSide());
                excel.writeStr(sheet, row, "M", center, swapBean.getRatecode());
                excel.writeStr(sheet, row, "N", center, swapBean.getPrinciple());
                excel.writeStr(sheet, row, "O", center, swapBean.getInitexchdate().substring(0, 10));
                excel.writeStr(sheet, row, "P", center, swapBean.getIccy1());
                excel.writeStr(sheet, row, "Q", center, df.format(Double.valueOf(swapBean.getInitexchamtccy1())));
                excel.writeStr(sheet, row, "R", center, swapBean.getIccy2());
                excel.writeStr(sheet, row, "S", center, df.format(Double.valueOf(swapBean.getInitexchamtccy2())));
                excel.writeStr(sheet, row, "T", center, swapBean.getFinexchdate().substring(0, 10));
                excel.writeStr(sheet, row, "U", center, swapBean.getFccy1());
                excel.writeStr(sheet, row, "V", center, df.format(Double.valueOf(swapBean.getFinexchamtccy1())));
                excel.writeStr(sheet, row, "W", center, swapBean.getFccy2());
                excel.writeStr(sheet, row, "X", center, df.format(Double.valueOf(swapBean.getFinexchamtccy2())));
            }
            return excel;
        } catch (Exception e) {
            throw new RException(e);
        }
    }

    @ResponseBody
    @RequestMapping(value = "/exportFxForwardExposureExcel")
    public void exportFxForwardExposureExcel(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String ua = request.getHeader("User-Agent");
        String trad = request.getParameter("trad");
        String port = request.getParameter("port");
        String ccy = request.getParameter("ccy");
        String br = request.getParameter("br");  //自贸区加br

        String filename = "远期外汇敞口查询.xlsx"; // 文件名
        String encode_filename = "";
        try {
            encode_filename = URLEncoder.encode(filename, "utf-8");
        } catch (UnsupportedEncodingException e1) {
            throw new RException(e1.getMessage());
        }
        response.setContentType("application/vnd.ms-excel");
        if (ua != null) {
            response.setHeader("Content-Disposition", "attachment; filename=" + encode_filename);
        }
        response.setHeader("Connection", "close");
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("trad", trad);
            map.put("port", port);
            map.put("ccy", ccy);
            map.put("br", br);//自贸区加br
            ExcelUtil e = downloadFxForwardExposureExcel(map);
            OutputStream ouputStream = response.getOutputStream();
            e.getWb().write(ouputStream);
            ouputStream.flush();
            ouputStream.close();
        } catch (Exception e) {
            throw new RException(e);
        }
    }

    /**
     * 远期外汇敞口Excel文件下载内容
     */
    public ExcelUtil downloadFxForwardExposureExcel(Map<String, Object> map) throws Exception {
        ExcelUtil excel = null;
        try {
            /* 先查数据，再设置表格属性以及内容 */

            List<SlExposureBean> list = exposureServer.getFxForwardExposure(map).getList();
            SlExposureBean bean = new SlExposureBean();
            // 表头
            excel = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
            CellStyle center = excel.getDefaultCenterStrCellStyle();
            int sheet = 0;
            int row = 0;
            // 表格sheet名称
            excel.getWb().createSheet("远期外汇敞口查询");
            // 设置表头字段名
            excel.writeStr(sheet, row, "A", center, "部门");
            excel.writeStr(sheet, row, "B", center, "敞口类型");
            excel.writeStr(sheet, row, "C", center, "投资组合");
            excel.writeStr(sheet, row, "D", center, "交易员");
            excel.writeStr(sheet, row, "E", center, "币种");
            excel.writeStr(sheet, row, "F", center, "币种1金额");
            excel.writeStr(sheet, row, "G", center, "币种2金额");
            excel.writeStr(sheet, row, "H", center, "成本汇率");
            excel.writeStr(sheet, row, "I", center, "市场汇率");
            excel.writeStr(sheet, row, "J", center, "折美元浮动金额");
            // 设置列宽
            excel.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(8, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(9, 20 * 256);

            for (int i = 0; i < list.size(); i++) {
                bean = list.get(i);
                row++;
                // 插入数据
                excel.writeStr(sheet, row, "A", center, bean.getBr());
                excel.writeStr(sheet, row, "B", center, bean.getProd());
                excel.writeStr(sheet, row, "C", center, bean.getPort());
                excel.writeStr(sheet, row, "D", center, bean.getTrad());
                excel.writeStr(sheet, row, "E", center, bean.getCcy());
                excel.writeStr(sheet, row, "F", center, bean.getCcy1amt() == null ? "" : bean.getCcy1amt().setScale(4, BigDecimal.ROUND_HALF_UP).toString());
                excel.writeStr(sheet, row, "G", center, bean.getCcy2amt() == null ? "" : bean.getCcy2amt().setScale(4, BigDecimal.ROUND_HALF_UP).toString());
                excel.writeStr(sheet, row, "H", center, bean.getBidprice() == null ? "" : bean.getBidprice().setScale(8, BigDecimal.ROUND_HALF_UP).toString());
                excel.writeStr(sheet, row, "I", center, bean.getRmbprice() == null ? "" : bean.getRmbprice().setScale(8, BigDecimal.ROUND_HALF_UP).toString());
                excel.writeStr(sheet, row, "J", center, bean.getUsdamt() == null ? "" : bean.getUsdamt().setScale(4, BigDecimal.ROUND_HALF_UP).toString());

            }
            return excel;
        } catch (Exception e) {
            throw new RException(e);
        }
    }

    /**
     * 导出远期结售汇敞口Excel
     */
    @ResponseBody
    @RequestMapping(value = "/exportFxForwardSettleExposureExcel")
    public void exportFxForwardSettleExposureExcel(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String ua = request.getHeader("User-Agent");
        String trad = request.getParameter("trad");
        String port = request.getParameter("port");
        String ccy = request.getParameter("ccy");
        String br = request.getParameter("br");  //自贸区加br

        String filename = "远期结售汇敞口查询.xlsx"; // 文件名
        String encode_filename = "";
        try {
            encode_filename = URLEncoder.encode(filename, "utf-8");
        } catch (UnsupportedEncodingException e1) {
            throw new RException(e1.getMessage());
        }
        response.setContentType("application/vnd.ms-excel");
        if (ua != null) {
            response.setHeader("Content-Disposition", "attachment; filename=" + encode_filename);
        }
        response.setHeader("Connection", "close");
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("trad", trad);
            map.put("port", port);
            map.put("ccy", ccy);
            map.put("br", br);//自贸区加br
            ExcelUtil e = downloadFxForwardSettleExposureExcel(map);
            OutputStream ouputStream = response.getOutputStream();
            e.getWb().write(ouputStream);
            ouputStream.flush();
            ouputStream.close();
        } catch (Exception e) {
            throw new RException(e);
        }
    }

    /**
     * 远期结售汇敞口Excel文件下载内容
     */
    public ExcelUtil downloadFxForwardSettleExposureExcel(Map<String, Object> map) throws Exception {
        ExcelUtil excel = null;
        try {
            /* 先查数据，再设置表格属性以及内容 */

            List<SlExposureBean> list = exposureServer.getFxForwardSettleExposure(map).getList();
            SlExposureBean bean = new SlExposureBean();
            // 表头
            excel = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
            CellStyle center = excel.getDefaultCenterStrCellStyle();
            int sheet = 0;
            int row = 0;
            // 表格sheet名称
            excel.getWb().createSheet("远期结售汇敞口查询");
            // 设置表头字段名
            excel.writeStr(sheet, row, "A", center, "部门");
            excel.writeStr(sheet, row, "B", center, "敞口类型");
            excel.writeStr(sheet, row, "C", center, "投资组合");
            excel.writeStr(sheet, row, "D", center, "交易员");
            excel.writeStr(sheet, row, "E", center, "币种");
            excel.writeStr(sheet, row, "F", center, "外币金额");
            /*excel.writeStr(sheet, row, "G", center, "人民币金额");*/
            excel.writeStr(sheet, row, "G", center, "预警状态");
            excel.writeStr(sheet, row, "H", center, "成本汇率");
            excel.writeStr(sheet, row, "I", center, "市场汇率");
            excel.writeStr(sheet, row, "J", center, "浮动损益(CNY)");
            // 设置列宽
            excel.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(8, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(9, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(10, 20 * 256);
            for (int i = 0; i < list.size(); i++) {
                bean = list.get(i);
                row++;
                // 插入数据
                //DecimalFormat df = new DecimalFormat("#,##0.0000");
                excel.writeStr(sheet, row, "A", center, bean.getBr());
                excel.writeStr(sheet, row, "B", center, bean.getProd());
                excel.writeStr(sheet, row, "C", center, bean.getPort());
                excel.writeStr(sheet, row, "D", center, bean.getTrad());
                excel.writeStr(sheet, row, "E", center, bean.getCcy());
                excel.writeStr(sheet, row, "F", center, bean.getAmt() == null ? "" : bean.getAmt().setScale(4, BigDecimal.ROUND_HALF_UP).toString());
                /*excel.writeStr(sheet, row, "G", center, bean.getCcy2amt()==null?"":bean.getCcy2amt().setScale(4, BigDecimal.ROUND_HALF_UP).toString());*/
                excel.writeStr(sheet, row, "G", center, bean.getCe());
                excel.writeStr(sheet, row, "H", center, bean.getBidprice() == null ? "" : bean.getBidprice().setScale(8, BigDecimal.ROUND_HALF_UP).toString());
                excel.writeStr(sheet, row, "I", center, bean.getPrice() == null ? "" : bean.getPrice().setScale(8, BigDecimal.ROUND_HALF_UP).toString());
                excel.writeStr(sheet, row, "J", center, bean.getPlamt() == null ? "" : bean.getPlamt().setScale(4, BigDecimal.ROUND_HALF_UP).toString());

            }
            return excel;
        } catch (Exception e) {
            throw new RException(e);
        }
    }

    @ResponseBody
    @RequestMapping(value = "/exportFxSwapExposureExcel")
    public void exportFxSwapExposureExcel(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String ua = request.getHeader("User-Agent");
        String trad = request.getParameter("trad");
        String port = request.getParameter("port");
        String ccy = request.getParameter("ccy");
        String br = request.getParameter("br");  //自贸区加br

        String filename = "掉期外汇敞口查询.xlsx"; // 文件名
        String encode_filename = "";
        try {
            encode_filename = URLEncoder.encode(filename, "utf-8");
        } catch (UnsupportedEncodingException e1) {
            throw new RException(e1.getMessage());
        }
        response.setContentType("application/vnd.ms-excel");
        if (ua != null) {
            response.setHeader("Content-Disposition", "attachment; filename=" + encode_filename);
        }
        response.setHeader("Connection", "close");
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("trad", trad);
            map.put("port", port);
            map.put("ccy", ccy);
            map.put("br", br);//自贸区加br
            ExcelUtil e = downloadFxSwapExposureExcel(map);
            OutputStream ouputStream = response.getOutputStream();
            e.getWb().write(ouputStream);
            ouputStream.flush();
            ouputStream.close();
        } catch (Exception e) {
            throw new RException(e);
        }
    }

    /**
     * 掉期外汇敞口Excel文件下载内容
     */
    public ExcelUtil downloadFxSwapExposureExcel(Map<String, Object> map) throws Exception {
        ExcelUtil excel = null;
        try {
            /* 先查数据，再设置表格属性以及内容 */

            List<SlExposureBean> list = exposureServer.getFxSwapExposure(map).getList();
            SlExposureBean bean = new SlExposureBean();
            // 表头
            excel = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
            CellStyle center = excel.getDefaultCenterStrCellStyle();
            int sheet = 0;
            int row = 0;
            // 表格sheet名称
            excel.getWb().createSheet("即期外汇敞口查询");
            // 设置表头字段名
            excel.writeStr(sheet, row, "A", center, "部门");
            excel.writeStr(sheet, row, "B", center, "敞口类型");
            excel.writeStr(sheet, row, "C", center, "投资组合");
            excel.writeStr(sheet, row, "D", center, "交易员");
            excel.writeStr(sheet, row, "E", center, "币种");
            excel.writeStr(sheet, row, "F", center, "币种1金额");
            excel.writeStr(sheet, row, "G", center, "币种2金额");
            excel.writeStr(sheet, row, "H", center, "成本汇率");
            excel.writeStr(sheet, row, "I", center, "市场汇率");
            excel.writeStr(sheet, row, "J", center, "折美元浮动金额");
            // 设置列宽
            excel.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(8, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(9, 20 * 256);

            for (int i = 0; i < list.size(); i++) {
                bean = list.get(i);
                row++;
                // 插入数据
                excel.writeStr(sheet, row, "A", center, bean.getBr());
                excel.writeStr(sheet, row, "B", center, bean.getProd());
                excel.writeStr(sheet, row, "C", center, bean.getPort());
                excel.writeStr(sheet, row, "D", center, bean.getTrad());
                excel.writeStr(sheet, row, "E", center, bean.getCcy());
                excel.writeStr(sheet, row, "F", center, bean.getCcy1amt() == null ? "" : bean.getCcy1amt().setScale(4, BigDecimal.ROUND_HALF_UP).toString());
                excel.writeStr(sheet, row, "G", center, bean.getCcy2amt() == null ? "" : bean.getCcy2amt().setScale(4, BigDecimal.ROUND_HALF_UP).toString());
                excel.writeStr(sheet, row, "H", center, bean.getBidprice() == null ? "" : bean.getBidprice().setScale(8, BigDecimal.ROUND_HALF_UP).toString());
                excel.writeStr(sheet, row, "I", center, bean.getRmbprice() == null ? "" : bean.getRmbprice().setScale(8, BigDecimal.ROUND_HALF_UP).toString());
                excel.writeStr(sheet, row, "J", center, bean.getUsdamt() == null ? "" : bean.getUsdamt().setScale(4, BigDecimal.ROUND_HALF_UP).toString());

            }
            return excel;
        } catch (Exception e) {
            throw new RException(e);
        }
    }

    /**
     * 导出掉期结售汇敞口Excel
     */
    @ResponseBody
    @RequestMapping(value = "/exportFxSwapSettleExposureExcel")
    public void exportFxSwapSettleExposureExcel(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String ua = request.getHeader("User-Agent");
        String trad = request.getParameter("trad");
        String port = request.getParameter("port");
        String ccy = request.getParameter("ccy");
        String br = request.getParameter("br");  //自贸区加br

        String filename = "掉期结售汇敞口查询.xlsx"; // 文件名
        String encode_filename = "";
        try {
            encode_filename = URLEncoder.encode(filename, "utf-8");
        } catch (UnsupportedEncodingException e1) {
            throw new RException(e1.getMessage());
        }
        response.setContentType("application/vnd.ms-excel");
        if (ua != null) {
            response.setHeader("Content-Disposition", "attachment; filename=" + encode_filename);
        }
        response.setHeader("Connection", "close");
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("trad", trad);
            map.put("port", port);
            map.put("ccy", ccy);
            map.put("br", br);//自贸区加br
            ExcelUtil e = downloadFxSwapSettleExposureExcel(map);
            OutputStream ouputStream = response.getOutputStream();
            e.getWb().write(ouputStream);
            ouputStream.flush();
            ouputStream.close();
        } catch (Exception e) {
            throw new RException(e);
        }
    }

    /**
     * 掉期结售汇敞口Excel文件下载内容
     */
    public ExcelUtil downloadFxSwapSettleExposureExcel(Map<String, Object> map) throws Exception {
        ExcelUtil excel = null;
        try {
            /* 先查数据，再设置表格属性以及内容 */

            List<SlExposureBean> list = exposureServer.getFxSwapSettleExposure(map).getList();
            SlExposureBean bean = new SlExposureBean();
            // 表头
            excel = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
            CellStyle center = excel.getDefaultCenterStrCellStyle();
            int sheet = 0;
            int row = 0;
            // 表格sheet名称
            excel.getWb().createSheet("掉期结售汇敞口查询");
            // 设置表头字段名
            excel.writeStr(sheet, row, "A", center, "部门");
            excel.writeStr(sheet, row, "B", center, "敞口类型");
            excel.writeStr(sheet, row, "C", center, "投资组合");
            excel.writeStr(sheet, row, "D", center, "交易员");
            excel.writeStr(sheet, row, "E", center, "币种");
            excel.writeStr(sheet, row, "F", center, "外币金额");
            /*excel.writeStr(sheet, row, "G", center, "人民币金额");*/
            excel.writeStr(sheet, row, "G", center, "预警状态");
            excel.writeStr(sheet, row, "H", center, "成本汇率");
            excel.writeStr(sheet, row, "I", center, "市场汇率");
            excel.writeStr(sheet, row, "J", center, "浮动损益(CNY)");
            // 设置列宽
            excel.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(8, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(9, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(10, 20 * 256);
            for (int i = 0; i < list.size(); i++) {
                bean = list.get(i);
                row++;
                // 插入数据
                //DecimalFormat df = new DecimalFormat("#,##0.0000");
                excel.writeStr(sheet, row, "A", center, bean.getBr());
                excel.writeStr(sheet, row, "B", center, bean.getProd());
                excel.writeStr(sheet, row, "C", center, bean.getPort());
                excel.writeStr(sheet, row, "D", center, bean.getTrad());
                excel.writeStr(sheet, row, "E", center, bean.getCcy());
                excel.writeStr(sheet, row, "F", center, bean.getAmt() == null ? "" : bean.getAmt().setScale(4, BigDecimal.ROUND_HALF_UP).toString());
                /*excel.writeStr(sheet, row, "G", center, bean.getCcy2amt()==null?"":bean.getCcy2amt().setScale(4, BigDecimal.ROUND_HALF_UP).toString());*/
                excel.writeStr(sheet, row, "G", center, bean.getCe());
                excel.writeStr(sheet, row, "H", center, bean.getBidprice() == null ? "" : bean.getBidprice().setScale(8, BigDecimal.ROUND_HALF_UP).toString());
                excel.writeStr(sheet, row, "I", center, bean.getPrice() == null ? "" : bean.getPrice().setScale(8, BigDecimal.ROUND_HALF_UP).toString());
                excel.writeStr(sheet, row, "J", center, bean.getPlamt() == null ? "" : bean.getPlamt().setScale(4, BigDecimal.ROUND_HALF_UP).toString());

            }
            return excel;
        } catch (Exception e) {
            throw new RException(e);
        }
    }


    /**
     * 债券头寸查询 报表导出
     *
     * @param request
     * @param response
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "/exportBondCashReport")
    public void exportBondCashReport(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String ua = request.getHeader("User-Agent");
        String br = request.getParameter("br");//部门
        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        String filename = "债券头寸查询.xlsx"; // 文件名
        String encode_filename = "";
        try {
            encode_filename = URLEncoder.encode(filename, "utf-8");
        } catch (UnsupportedEncodingException e1) {
            throw new RException(e1.getMessage());
        }
        response.setContentType("application/vnd.ms-excel");
        if (ua != null) {
            response.setHeader("Content-Disposition", "attachment; filename=" + encode_filename);
        }
        response.setHeader("Connection", "close");
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("br", br);
            map.put("startDate", startDate);
            map.put("endDate", endDate);
            ExcelUtil e = downloadBondCashReportExcel(map);
            OutputStream ouputStream = response.getOutputStream();
            e.getWb().write(ouputStream);
            ouputStream.flush();
            ouputStream.close();
        } catch (Exception e) {
            throw new RException(e);
        }
    }

    /**
     * 债券头寸查询  统计Excel文件下载内容
     */
    public ExcelUtil downloadBondCashReportExcel(Map<String, Object> map) throws Exception {
        ExcelUtil excel = null;
        try {
            //String br=ParameterUtil.getString(map, "br", "");
            /* 先查数据，再设置表格属性以及内容 */
            List<SlBondCashBean> list = exposureServer.searchBondCashCount(map).getList();
            // 表头
            excel = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
            CellStyle center = excel.getDefaultCenterStrCellStyle();
            int sheet = 0;
            int row = 0;
            // 表格sheet名称
            excel.getWb().createSheet("债券头寸查询");
            // 设置表头字段名
            excel.writeStr(sheet, row, "A", center, "债券ISIN");
            excel.writeStr(sheet, row, "B", center, "成本中心");
            excel.writeStr(sheet, row, "C", center, "投资组合");
            excel.writeStr(sheet, row, "D", center, "资产类型");
            excel.writeStr(sheet, row, "E", center, "收款日");
            excel.writeStr(sheet, row, "F", center, "票面价值");
            excel.writeStr(sheet, row, "G", center, "成本");
            excel.writeStr(sheet, row, "H", center, "计提");
            excel.writeStr(sheet, row, "I", center, "截止上个付息日利息");
            excel.writeStr(sheet, row, "J", center, "未摊销金额");
            excel.writeStr(sheet, row, "K", center, "成本价");
            excel.writeStr(sheet, row, "L", center, "收盘价");
            excel.writeStr(sheet, row, "M", center, "面值");
            excel.writeStr(sheet, row, "N", center, "市场价值");
            excel.writeStr(sheet, row, "O", center, "估值");
            excel.writeStr(sheet, row, "P", center, "起息日");
            excel.writeStr(sheet, row, "Q", center, "到期日");
            excel.writeStr(sheet, row, "R", center, "票面利率");
            excel.writeStr(sheet, row, "S", center, "到期收益率");
            excel.writeStr(sheet, row, "T", center, "计息规则");
            excel.writeStr(sheet, row, "U", center, "付息频率");

            // 设置列宽
            excel.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(8, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(9, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(10, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(11, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(12, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(13, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(14, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(15, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(16, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(17, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(18, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(19, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(20, 20 * 256);
            excel.getSheetAt(sheet).setColumnWidth(21, 20 * 256);
            for (int i = 0; i < list.size(); i++) {
                row++;
                SlBondCashBean bondCashBean = list.get(i);
                DecimalFormat df = new DecimalFormat("#0.00");
                excel.writeStr(sheet, row, "A", center, bondCashBean.getSecid());
                excel.writeStr(sheet, row, "B", center, bondCashBean.getCost());
                excel.writeStr(sheet, row, "C", center, bondCashBean.getPort());
                excel.writeStr(sheet, row, "D", center, bondCashBean.getInvtype());
                excel.writeStr(sheet, row, "E", center, bondCashBean.getIpaydate().substring(0, 10)); //收款日
                excel.writeStr(sheet, row, "F", center, bondCashBean.getParamt());
                excel.writeStr(sheet, row, "G", center, bondCashBean.getAmortamt());
                excel.writeStr(sheet, row, "H", center, df.format(Double.valueOf(bondCashBean.getTdyintamt())));//计提
                excel.writeStr(sheet, row, "I", center, df.format(Double.valueOf(bondCashBean.getAccrintamt())));//截止上个付息日利息
                excel.writeStr(sheet, row, "J", center, bondCashBean.getUnamortamt());
                excel.writeStr(sheet, row, "K", center, df.format(Double.valueOf(bondCashBean.getUnitcost())));//成本价
                excel.writeStr(sheet, row, "L", center, bondCashBean.getClsgprice());
                excel.writeStr(sheet, row, "M", center, bondCashBean.getFacevalue()); //面值
                excel.writeStr(sheet, row, "N", center, bondCashBean.getFairvalue());//市场价值
                excel.writeStr(sheet, row, "O", center, bondCashBean.getDiffamt()); //估值
                excel.writeStr(sheet, row, "P", center, bondCashBean.getVdate().substring(0, 10)); //起息日
                excel.writeStr(sheet, row, "Q", center, bondCashBean.getMdate().substring(0, 10)); //到期日
                excel.writeStr(sheet, row, "R", center, df.format(Double.valueOf(bondCashBean.getCouprate())));// 票面利率
                excel.writeStr(sheet, row, "S", center, df.format(Double.valueOf(bondCashBean.getYearstomaturity()))); //到期收益率
                excel.writeStr(sheet, row, "T", center, bondCashBean.getRatetype()); //计息规则
                excel.writeStr(sheet, row, "U", center, bondCashBean.getIntpaycycle()); //付息频率
            }
            return excel;
        } catch (Exception e) {
            throw new RException(e);
        }
    }

}