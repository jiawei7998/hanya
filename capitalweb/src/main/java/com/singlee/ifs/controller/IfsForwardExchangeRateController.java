package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.base.util.FileManage;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.UploadedFile;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.ifs.model.IfsForwardExchangeRateBean;
import com.singlee.ifs.service.IfsForwardExchangeRateService;
import jxl.Sheet;
import jxl.Workbook;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author cg
 * @version 1.0.0
 * @ClassName IfsForwardExchangeRateController.java
 * @Description 市场数据
 * @createTime 2021-09-17
 */
@Controller
@RequestMapping(value = "IfsForwardExchangeRateController")
public class IfsForwardExchangeRateController extends CommonController {

    @Autowired
    IfsForwardExchangeRateService ifsForwardExchangeRateService;

    /**
     * @title 分页查询
     * @description
     * @author  cg
     * @updateTime 2021-09-17
     * @throws
     */
    @ResponseBody
    @RequestMapping(value = "/searchPageRate")
    public RetMsg<PageInfo<IfsForwardExchangeRateBean>> searchPageRate(@RequestBody Map<String, Object> map) {
        Page<IfsForwardExchangeRateBean> page = ifsForwardExchangeRateService.searchPageRate(map);
        return RetMsgHelper.ok(page);
    }

    /**
     * 市场数据 Excel上传外汇远掉点信息
     */
    @ResponseBody
    @RequestMapping(value = "/ImportRateByExcel", method = RequestMethod.POST, produces = "text/plain;charset=UTF-8")
    public String ImportRateByExcel(MultipartHttpServletRequest request, HttpServletResponse response) {
        // 1.文件获取
        List<UploadedFile> uploadedFileList = null;
        String ret = "导入失败";
        Workbook wb = null;
        List<IfsForwardExchangeRateBean> rateBean = new ArrayList<>();
        try {
            uploadedFileList = FileManage.requestExtractor(request);
            // 2.校验并处理成excel
            List<Workbook> excelList = new LinkedList<Workbook>();
            for (UploadedFile uploadedFile : uploadedFileList) {
                if (!"xls".equals(uploadedFile.getType())) {
                    return ret = "上传文件类型错误,仅允许上传xls格式文件";
                }
                Workbook book = Workbook.getWorkbook(new ByteArrayInputStream(
                        uploadedFile.getBytes()));
                excelList.add(book);
            }
            if (excelList.size() != 1) {
                return ret = "只能处理一份excel";
            }
            wb = excelList.get(0);
            Sheet[] sheets = wb.getSheets();// 获得卡片sheet数量
            String postdate= DateUtil.getCurrentDateAsString("yyyy-MM-dd");
            for (Sheet sheet : sheets) {
                int rowCount = sheet.getRows();// 行数
                IfsForwardExchangeRateBean IfsForwardExchangeRate = null;


                for (int i = 2; i < rowCount-2; i++) { // rows=2开始 第三行开始读取，第一行为列名，第二行为汇率类型
                   if("".equals(StringUtils.trimToEmpty(sheet.getCell(0, i).getContents()))||"CCY".equals(StringUtils.trimToEmpty(sheet.getCell(0, i).getContents()))){
                       continue;
                   }


                    if(StringUtils.trimToEmpty(sheet.getCell(1, i).getContents()).equals("")){
                        continue;
                    }
                    IfsForwardExchangeRate = new IfsForwardExchangeRateBean();
                    IfsForwardExchangeRate.setPostdate(postdate);

                    IfsForwardExchangeRate.setCcy(StringUtils.trimToEmpty(sheet
                            .getCell(0, i).getContents())+"/CNY");// A
                    IfsForwardExchangeRate.setSpotRate(StringUtils.trimToEmpty(sheet
                            .getCell(1, i).getContents()));// B
                    IfsForwardExchangeRate.setPeriod1Rate(StringUtils.trimToEmpty(sheet
                            .getCell(2, i).getContents()));// C
                    IfsForwardExchangeRate.setPeriod2Rate(StringUtils.trimToEmpty(sheet
                            .getCell(3, i).getContents()));// D
                    IfsForwardExchangeRate.setPeriod3Rate(StringUtils.trimToEmpty(sheet
                            .getCell(4, i).getContents()));// E
                    IfsForwardExchangeRate.setPeriod4Rate(StringUtils.trimToEmpty(sheet
                            .getCell(5, i).getContents()));// F
                    IfsForwardExchangeRate.setPeriod5Rate(StringUtils.trimToEmpty(sheet
                            .getCell(6, i).getContents()));// G
                    IfsForwardExchangeRate.setPeriod6Rate(StringUtils.trimToEmpty(sheet
                            .getCell(7, i).getContents()));// H
                    IfsForwardExchangeRate.setPeriod7Rate(StringUtils.trimToEmpty(sheet
                            .getCell(8, i).getContents()));// I
                    IfsForwardExchangeRate.setPeriod8Rate(StringUtils.trimToEmpty(sheet
                            .getCell(9, i).getContents()));// J
                    IfsForwardExchangeRate.setPeriod9Rate(StringUtils.trimToEmpty(sheet
                            .getCell(10, i).getContents()));// K
                    IfsForwardExchangeRate.setPeriod10Rate(StringUtils.trimToEmpty(sheet
                            .getCell(11, i).getContents()));// L
                    IfsForwardExchangeRate.setPeriod11Rate(StringUtils.trimToEmpty(sheet
                            .getCell(12, i).getContents()));// M
                    IfsForwardExchangeRate.setPeriod12Rate(StringUtils.trimToEmpty(sheet
                            .getCell(13, i).getContents()));// N
                    IfsForwardExchangeRate.setPeriod13Rate(StringUtils.trimToEmpty(sheet
                            .getCell(14, i).getContents()));// O
                    IfsForwardExchangeRate.setPeriod14Rate(StringUtils.trimToEmpty(sheet
                            .getCell(15, i).getContents()));// P
                    IfsForwardExchangeRate.setPeriod15Rate(StringUtils.trimToEmpty(sheet
                            .getCell(16, i).getContents()));// Q
                    /* 将汇率类型插入到表中 */
                    if(i<=12){
                        //BID买入价
                        IfsForwardExchangeRate.setIsBuy("BID");
                    }else if (i>=14){
                        //ASK卖出价
                        IfsForwardExchangeRate.setIsBuy("ASK");
                    }

                    rateBean.add(IfsForwardExchangeRate);
                }
            }
            ret = ifsForwardExchangeRateService.doBussExcelPropertyToDataBase(rateBean);
            //插入inth表和irev
            String flag=  ifsForwardExchangeRateService.dataBaseToIrev(postdate);
            if("插入成功".equals(flag)){
                ret=ret+"且当天远掉点数据同步至OPICS成功";
            }else {
                ret=ret+"且当天远掉点数据同步至OPICS失败";
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw new RException(e);
        } finally {
            if (null != wb) {
                wb.close();
            }
        }
        return ret;// 返回前台展示配置
    }


    /**
     * 节假日模板导出
     */
    @ResponseBody
    @RequestMapping(value = "/forwardRateExcel")
    public void forwardRateExcel(HttpServletRequest request,
                          HttpServletResponse response) {
        OutputStream out = null;
        FileInputStream in = null;

        try {
            String fileName = "外汇远掉点模板";
            // 读取模板
            String excelPath = request.getSession().getServletContext().getRealPath("standard/forwardRate/forwardRate.xls");

            fileName = URLEncoder.encode(fileName, "UTF-8");

            response.reset();
            // 追加时间
            response.addHeader("Content-Disposition", "attachment;filename="
                    + fileName + ".xls");
            response.setContentType("application/octet-stream;charset=UTF-8");

            out = response.getOutputStream();
            in = new FileInputStream(excelPath);

            byte[] b = new byte[1024];
            int len;

            while ((len = in.read(b)) > 0) {
                response.getOutputStream().write(b, 0, len);
            }
            out.flush();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != in) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                in = null;
            }
            if (null != out) {
                try {
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                out = null;
            }
        }

    }

    }
