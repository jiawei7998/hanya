package com.singlee.ifs.controller;


import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.ifs.model.IfsEsbMassage;
import com.singlee.ifs.service.IfsEsbMassageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;
@Controller
@RequestMapping(value = "/IfsEsbMessageController")
public class IfsEsbMessageController extends CommonController{
	
    @Autowired
    IfsEsbMassageService ifsEsbMassageService;
    /**
     * esb报文状态
     * 分页查询 
     * @param map
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/searchEsbList")
    public RetMsg<PageInfo<IfsEsbMassage>> searchEsbList(@RequestBody Map<String, Object> map) {
        Page<IfsEsbMassage> page = ifsEsbMassageService.getEsbMassagesList(map);
        return RetMsgHelper.ok(page);
    }
    
}
