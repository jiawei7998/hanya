package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.ifs.model.IfsLimitTotal;
import com.singlee.ifs.service.IfsLimitTotalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.Map;

/**
 * @author Luozb
 * @version 1.0.0
 * @ClassName IfsLimitTotalController.java
 * @Description 限额
 * @createTime 2021年09月09日 16:10:00
 */
@Controller
@RequestMapping(value = "IfsLimitTotalController")
public class IfsLimitTotalController extends CommonController {

    @Autowired
    IfsLimitTotalService ifsLimitTotalService;

    /**
     * @title 分页查询
     * @description
     * @author  Luozb
     * @updateTime 2021/9/9 0009 16:52
     * @throws
     */
    @ResponseBody
    @RequestMapping(value = "/searchPageLimit")
    public RetMsg<PageInfo<IfsLimitTotal>> searchPageLimit(@RequestBody Map<String, Object> map) {
        Page<IfsLimitTotal> page = ifsLimitTotalService.searchPageLimit(map);
        return RetMsgHelper.ok(page);
    }

    @ResponseBody
    @RequestMapping(value = "/deleteLimit")
    public RetMsg<Serializable> deleteLimit(@RequestBody Map<String, String> map) {
        String limitId = map.get("limitId");
        ifsLimitTotalService.deleteByLimitId(limitId);
        return RetMsgHelper.ok();
    }

    /**
     * 新增
     */
    @RequestMapping(value = "/LimitConditionAdd")
    @ResponseBody
    public RetMsg<Serializable> LimitConditionAdd(@RequestBody IfsLimitTotal entity) throws RException {
        entity.setOperateUser(SlSessionHelper.getUserId());
        entity.setItime(DateUtil.getCurrentDateAsString());
        entity.setTotalAmount("0");
        ifsLimitTotalService.insert(entity);
        return RetMsgHelper.ok();
    }

    /**
     * 修改
     */
    @ResponseBody
    @RequestMapping(value = "/saveLimitConditionEdit")
    public RetMsg<Serializable> saveLimitConditionEdit(@RequestBody IfsLimitTotal entity) {
        String limitId = entity.getLimitId();
        entity.setOperateUser(SlSessionHelper.getUserId());
        entity.setItime(DateUtil.getCurrentDateAsString());
        ifsLimitTotalService.update(entity);
        return RetMsgHelper.ok();
    }
}
