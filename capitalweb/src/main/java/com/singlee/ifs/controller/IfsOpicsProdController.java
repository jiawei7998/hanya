package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.bean.SlProdBean;
import com.singlee.financial.opics.IStaticServer;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.SlErrors;
import com.singlee.ifs.mapper.IfsOpicsProdMapper;
import com.singlee.ifs.model.IfsOpicsProd;
import com.singlee.ifs.service.IfsOpicsProdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * OPICS产品代码Controller
 * 
 * ClassName: IfsOpicsProdController <br/>
 * date: 2018-5-30 下午07:43:06 <br/>
 * 
 * @author zhengfl
 * @version
 * @since JDK 1.6
 */
@Controller
@RequestMapping(value = "/IfsOpicsProdController")
public class IfsOpicsProdController {
	
	@Autowired
	IfsOpicsProdService ifsOpicsProdService;
	
	@Autowired
	IStaticServer iNonParamServer;
	
	@Resource
	IfsOpicsProdMapper ifsOpicsProdMapper;

	/***
	 * 分页查询
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageProd")
	public RetMsg<PageInfo<IfsOpicsProd>> searchPageProd(@RequestBody Map<String, Object> map) {
		Page<IfsOpicsProd> page = ifsOpicsProdService.searchPageOpicsProd(map);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 保存 产品 新增
	 */
	@ResponseBody
	@RequestMapping(value = "/saveOpicsProdAdd")
	public RetMsg<Serializable> saveOpicsProdAdd(@RequestBody IfsOpicsProd entity) {
		ifsOpicsProdService.insert(entity);
		return RetMsgHelper.ok();
	}

	/**
	 * 保存 产品 修改
	 */
	@ResponseBody
	@RequestMapping(value = "/saveOpicsProdEdit")
	public RetMsg<Serializable> saveOpicsProdEdit(@RequestBody IfsOpicsProd entity) {
		ifsOpicsProdService.updateById(entity);
		return RetMsgHelper.ok();
	}

	/**
	 * 删除
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteProd")
	public RetMsg<Serializable> deleteProd(@RequestBody Map<String, String> map) {
		String pcode = map.get("pcode");
		ifsOpicsProdService.deleteById(pcode);
		return RetMsgHelper.ok();
	}

	@ResponseBody
	@RequestMapping(value = "/searchProd")
	public RetMsg<List<IfsOpicsProd>> searchProd(@RequestBody HashMap<String, Object> map) throws RException {
		List<IfsOpicsProd> list = ifsOpicsProdService.searchProd(map);
		return RetMsgHelper.ok(list);
	}

	/**
	 * 同步到opics表
	 */
	@ResponseBody
	@RequestMapping(value = "/syncProd")
	public RetMsg<SlOutBean> syncProd(@RequestBody Map<String, String> map) {
		SlOutBean slOutBean = new SlOutBean();
		String pcode = map.get("pcode");
		IfsOpicsProd ifsOpicsProd = ifsOpicsProdService.searchById(pcode);
		SlProdBean prodBean = new SlProdBean();
		Date lstmntdte = DateUtil.parse(DateUtil.format(ifsOpicsProd.getLstmntdte()));
		prodBean.setLstmntdte(lstmntdte);
		prodBean.setPcode(ifsOpicsProd.getPcode());
		prodBean.setPdesc(ifsOpicsProd.getPdesc());
		prodBean.setSys(ifsOpicsProd.getSys());
		try {
			slOutBean = iNonParamServer.addProd(prodBean);
		} catch (RemoteConnectFailureException e) {
			slOutBean.setRetCode(SlErrors.FAILED_RMI_OPICS.getErrCode());
			slOutBean.setRetMsg(SlErrors.FAILED_RMI_OPICS.getErrMsg() + ":" + e.getMessage());
			slOutBean.setRetStatus(RetStatusEnum.F);
		} catch (Exception e) {
			slOutBean.setRetCode(SlErrors.FAILED.getErrCode());
			slOutBean.setRetMsg(SlErrors.FAILED.getErrMsg() + ":" + e.getMessage());
			slOutBean.setRetStatus(RetStatusEnum.F);
		}
		if ("S".equals(String.valueOf(slOutBean.getRetStatus()))) {
			ifsOpicsProd.setStatus("1");
			ifsOpicsProdService.updateStatus(ifsOpicsProd);
		}
		return RetMsgHelper.ok(slOutBean);
	}

	/**
	 * 批量校准信息
	 */
	@ResponseBody
	@RequestMapping(value = "/batchCalibration")
	public RetMsg<SlOutBean> verify(@RequestBody Map<String, String> map) {
		SlOutBean slOutBean = new SlOutBean();
		String type = String.valueOf(map.get("type"));
		String pcode = "";
		String[] pcodes;
		if ("1".equals(type)) { // 选中记录校准
			pcode = map.get("pcode");
			pcodes = pcode.split(",");
		} else { // 全部校准
			pcodes = null;
		}
		try {
			slOutBean = ifsOpicsProdService.batchCalibration(type, pcodes);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return RetMsgHelper.ok(slOutBean);
	}
	
	/**
	 * 查询已同步的产品代码
	 */
	@ResponseBody
	@RequestMapping(value = "/searchProdcode")
	public List<IfsOpicsProd> searchProdcode() {
		List<IfsOpicsProd> result = ifsOpicsProdMapper.searchProdcode();
		return result;
	}
}