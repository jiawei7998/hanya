package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.financial.bean.SlSwiftBean;
import com.singlee.hrbreport.util.HrbReportUtils;
import com.singlee.ifs.mapper.SwiftMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;


/**
 * 查看job是否执行成功
 * 
 * @author
 * 
 */
@Controller
@RequestMapping(value = "/jobRunController")
public class jobRunController {

	
	   @Autowired
	    SwiftMapper swiftMapper;
	
	/***
	 * 查询未发送的swift报文
	 */
	@ResponseBody
	@RequestMapping(value = "/searchSwift")
	public RetMsg<PageInfo<SlSwiftBean>> searchSwift(@RequestBody Map<String, Object> map) {
		int pageNum=(int)map.get("pageNum");
		int pageSize=(int)map.get("pageSize");
	    
		List<SlSwiftBean> list=swiftMapper.searchSwiftBySendflag(map);
		
		Page<SlSwiftBean> page= HrbReportUtils.producePage(list, pageNum, pageSize);
		return  RetMsgHelper.ok(page);
	}
	
}
