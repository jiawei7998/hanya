package com.singlee.ifs.controller;

import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.financial.bean.SlIrs9Bean;
import com.singlee.financial.opics.IRefluxFileServer;
import com.singlee.financial.page.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/***
 *回流文件报表
 * 
 */
@Controller
@RequestMapping(value = "/IrsRefluxController")
public class IrsRefluxController {
	
	@Autowired
	IRefluxFileServer refluxFileServer;
	
	
	/**
	 * 债券持仓查询
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/searchRefluxFileCount")
	public RetMsg<PageInfo<SlIrs9Bean>> searchRefluxFileCount(@RequestBody Map<String, Object> map) throws RemoteConnectFailureException, Exception {
		PageInfo<SlIrs9Bean> page = refluxFileServer.searchRefluxFileCount(map);
		return RetMsgHelper.ok(page);
	}
	
	
}
