package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.ifs.model.IfsApprovermbSl;
import com.singlee.ifs.model.IfsCfetsrmbDetailSl;
import com.singlee.ifs.service.IFSApproveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.io.Serializable;
import java.util.Map;

/**
 * 债券借贷事前审批交易Controller
 * 
 * ClassName: IfsApprovermbSlController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason: TODO ADD REASON(可选). <br/>
 * date: 2018-5-30 下午07:13:03 <br/>
 * 
 * @author zhangcm
 * @version
 * @since JDK 1.6
 */
@Controller
@RequestMapping(value = "/IfsApprovermbSlController")
public class IfsApprovermbSlController extends CommonController {
	@Autowired
	private IFSApproveService cfetsrmbSlService;

	/**
	 * 保存成交单
	 * 
	 * @param IfsApprovermbSl
	 * 
	 * */	
	@ResponseBody
	@RequestMapping(value = "/saveSl")
	public RetMsg<Serializable> saveSl(@RequestBody IfsApprovermbSl record) throws IOException {
		IfsApprovermbSl sl = cfetsrmbSlService.getSlById(record);
		String message = "";
		if (sl == null) {
			message = cfetsrmbSlService.insertSl(record);
		} else {
			try {
				cfetsrmbSlService.updateSl(record);
				message = "修改成功";
			} catch (Exception e) {
				e.printStackTrace();
				message = "修改失败";
			}
		}
		return StringUtil.containsIgnoreCase(message, "成功")? RetMsgHelper.ok(message):RetMsgHelper.fail(message);
	}
	

	/**
	 * 查询质押债券详情信息
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-7-18
	 */
	@ResponseBody
	@RequestMapping(value = "/searchBondSlDetails")
	public RetMsg<PageInfo<IfsCfetsrmbDetailSl>> searchBondSlDetails(@RequestBody Map<String, Object> params) {
		Page<IfsCfetsrmbDetailSl> page = cfetsrmbSlService.searchBondSlDetails(params);
		return RetMsgHelper.ok(page);
	}
	/**
	 * 成交单分页查询
	 * 
	 * @param IfsApprovermbSl
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchCfetsrmbSl")
	public RetMsg<PageInfo<IfsApprovermbSl>> searchCfetsrmbSl(@RequestBody IfsApprovermbSl record) {
		Page<IfsApprovermbSl> page = cfetsrmbSlService.getSlList(record);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 根据主键查询成交单
	 * 
	 * @param IfsApprovermbSl
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchCfetsrmbSlById")
	public RetMsg<IfsApprovermbSl> searchCfetsrmbSlById(@RequestBody IfsApprovermbSl key) {
		IfsApprovermbSl sl = cfetsrmbSlService.getSlById(key);
		return RetMsgHelper.ok(sl);
	}

	/**
	 * 删除成交单
	 * 
	 * @param IfsApprovermbSl
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/deleteCfetsrmbSl")
	public RetMsg<Serializable> deleteCfetsrmbSl(@RequestBody IfsApprovermbSl key) {
		cfetsrmbSlService.deleteSl(key);
		return RetMsgHelper.ok();
	}

	/**
	 * 货币掉期-查询我发起的
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-21
	 */
	@ResponseBody
	@RequestMapping(value = "/searchRmbSlSwapMine")
	public RetMsg<PageInfo<IfsApprovermbSl>> searchRmbSlSwapMine(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsApprovermbSl> page = cfetsrmbSlService.getRmbSlMinePage(params, 3);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 货币掉期-查询 待审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageRmbSlUnfinished")
	public RetMsg<PageInfo<IfsApprovermbSl>> searchPageRmbSlUnfinished(@RequestBody Map<String, Object> params) {
		String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse + ","
				+ DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
		String[] orderStatus = approveStatus.split(",");
		params.put("approveStatusNo", orderStatus);
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsApprovermbSl> page = cfetsrmbSlService.getRmbSlMinePage(params, 1);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 货币掉期-查询 已审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageRmbSlFinished")
	public RetMsg<PageInfo<IfsApprovermbSl>> searchPageRmbSlFinished(@RequestBody Map<String, Object> params) {
		String approveStatusNo = ParameterUtil.getString(params, "approveStatus", "");
		if (StringUtil.isNullOrEmpty(approveStatusNo)) {
			approveStatusNo = DictConstants.ApproveStatus.ApprovedPass;
			approveStatusNo += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatusNo += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatusNo += "," + DictConstants.ApproveStatus.Verified;
			approveStatusNo += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			// 20180502修改bug:已审批列表中增加"审批中"
			approveStatusNo += "," + DictConstants.ApproveStatus.Approving;
			String[] orderStatus = approveStatusNo.split(",");
			params.put("approveStatusNo", orderStatus);
		}
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsApprovermbSl> page = cfetsrmbSlService.getRmbSlMinePage(params, 2);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 根据主键查询成交单(返回值增加 流程id)
	 * 
	 * @param
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchCfetsrmbSlAndFlowIdById")
	public RetMsg<IfsApprovermbSl> searchCfetsrmbSlAndFlowIdById(@RequestBody Map<String, Object> key) {
		IfsApprovermbSl sl = cfetsrmbSlService.getSlAndFlowIdById(key);
		return RetMsgHelper.ok(sl);
	}

	/**
	 * 勾兑时，查询表中审批通过的数据
	 * 
	 * @param
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchPageApprovermbSlPassed")
	public RetMsg<PageInfo<IfsApprovermbSl>> searchPageApprovermbSlPassed(@RequestBody Map<String, Object> params) {
		Page<IfsApprovermbSl> page = cfetsrmbSlService.getApproveSlPassedPage(params);
		return RetMsgHelper.ok(page);
	}
	
	
	//债券类mini弹框
	@ResponseBody
	@RequestMapping(value = "/getMini")
	public RetMsg<PageInfo<Map<String, Object>>> searchMiniPage(@RequestBody Map<String, Object> params) {
		Page<Map<String, Object>> page = cfetsrmbSlService.getSlMini(params);
		return RetMsgHelper.ok(page);
		
	}
	
	
//	//债券类mini弹框--债券发行
//	@ResponseBody
//	@RequestMapping(value = "/getDpMini")
//	public RetMsg<PageInfo<Map<String, Object>>> searchDpMiniPage(@RequestBody Map<String, Object> params) {
//		Page<Map<String, Object>> page = cfetsrmbSlService.getDpMini(params);
//		return RetMsgHelper.ok(page);
//		
//	}
//	
//	
//	
//	//根据ticketId查询
//	@ResponseBody
//	@RequestMapping(value = "/searchCfetsrmbSlByTicketId")
//	public RetMsg<IfsApprovermbSl> searchCfetsrmbSlByTicketId(@RequestBody Map<String, Object> map) {
//		IfsApprovermbSl sl = cfetsrmbSlService.searchCfetsrmbSlByTicketId(map);
//		return RetMsgHelper.ok(sl);
//	}
}

