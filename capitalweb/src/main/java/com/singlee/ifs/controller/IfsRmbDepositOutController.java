package com.singlee.ifs.controller;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.Page;
import com.singlee.capital.cashflow.model.parent.CashflowCapital;
import com.singlee.capital.cashflow.model.parent.CashflowInterest;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.ifs.model.IfsRmbDepositout;
import com.singlee.ifs.service.IfsRmbDepOutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @Author zhangkai
 * @Description
 * @Date
 */
@Controller
@RequestMapping(value="/IfsRmbDepositOutController")
public class IfsRmbDepositOutController {

    @Autowired
    IfsRmbDepOutService iFSService;

    /**
     * 同业存放(外币)-查询我发起的
     *
     * @param params
     *            - 请求参数
     * @return
     * @author
     * @date 2018-3-21
     */
    @ResponseBody
    @RequestMapping(value = "/searchPageRmbDepositOutMine")
    public RetMsg<PageInfo<IfsRmbDepositout>> searchPageRmbDepositOutMine(@RequestBody Map<String, Object> params) {
        params.put("isActive", DictConstants.YesNo.YES);
        params.put("userId", SlSessionHelper.getUserId());
        Page<IfsRmbDepositout> page = iFSService.getDepositOutRmbMinePage(params, 3);
        return RetMsgHelper.ok(page);
    }

    /**
     * 同业存放(外币)-查询 待审批
     *
     * @param params
     *            - 请求参数
     * @return
     * @author
     * @date 2018-3-23
     */
    @ResponseBody
    @RequestMapping(value = "/searchPageRmbDepositOutUnfinished")
    public RetMsg<PageInfo<IfsRmbDepositout>> searchPageRmbDepositOutUnfinished(@RequestBody Map<String, Object> params) {
        String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse + ","
                + DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
        String[] orderStatus = approveStatus.split(",");
        params.put("approveStatusNo", orderStatus);
        params.put("isActive", DictConstants.YesNo.YES);
        params.put("userId", SlSessionHelper.getUserId());
        Page<IfsRmbDepositout> page = iFSService.getDepositOutRmbMinePage(params, 1);
        return RetMsgHelper.ok(page);
    }

    /**
     * 同业存放(外币)-查询 已审批
     *
     * @param params
     *            - 请求参数
     * @return
     * @author
     * @date 2018-3-23
     */
    @ResponseBody
    @RequestMapping(value = "/searchPageRmbDepositOutFinished")
    public RetMsg<PageInfo<IfsRmbDepositout>> searchPageRmbDepositOutFinished(@RequestBody Map<String, Object> params) {
        String approveStatusNo = ParameterUtil.getString(params, "approveStatus", "");
        if (StringUtil.isNullOrEmpty(approveStatusNo)) {
            approveStatusNo = DictConstants.ApproveStatus.ApprovedPass;
            approveStatusNo += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
            approveStatusNo += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
            approveStatusNo += "," + DictConstants.ApproveStatus.Verified;
            approveStatusNo += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
            approveStatusNo += "," + DictConstants.ApproveStatus.Approving;
            String[] orderStatus = approveStatusNo.split(",");
            params.put("approveStatusNo", orderStatus);
        }
        params.put("isActive", DictConstants.YesNo.YES);
        params.put("userId", SlSessionHelper.getUserId());
        Page<IfsRmbDepositout> page = iFSService.getDepositOutRmbMinePage(params, 2);
        return RetMsgHelper.ok(page);
    }

    // 查询一条
    @ResponseBody
    @RequestMapping(value = "/searchRmbDepositOut")
    public RetMsg<PageInfo<IfsRmbDepositout>> searchRmbDepositOut(@RequestBody Map<String, Object> params) {
        Page<IfsRmbDepositout> page = iFSService.searchRmbDepositOut(params);
        return RetMsgHelper.ok(page);
    }

    // 根据ticketid查询
    @ResponseBody
    @RequestMapping(value = "/searchByTicketId")
    public RetMsg<IfsRmbDepositout> searchByTicketId(@RequestBody Map<String, Object> params) {
        IfsRmbDepositout ifsRmbDepositOut = iFSService.searchDepositOutRmbByTicketId(params);
        return RetMsgHelper.ok(ifsRmbDepositOut);
    }

    // 根据ticketid查询
    @ResponseBody
    @RequestMapping(value = "/searchFlowDepositOutRmbByTicketId")
    public RetMsg<IfsRmbDepositout> searchFlowByTicketId(@RequestBody Map<String, Object> params) {
        IfsRmbDepositout ifsRmbDepositOut = iFSService.searchFlowDepositOutRmbByTicketId(params);
        return RetMsgHelper.ok(ifsRmbDepositOut);
    }


    // 新增
    @ResponseBody
    @RequestMapping(value = "/addRmbDepositOut")
    public RetMsg<Serializable> addRmbDepositOut(@RequestBody IfsRmbDepositout record) {
        String message = iFSService.addRmbDepositOut(record);
        return StringUtil.containsIgnoreCase(message, "保存成功")?RetMsgHelper.ok(message):RetMsgHelper.fail(message);
    }

    // 修改
    @ResponseBody
    @RequestMapping(value = "/editRmbDepositOut")
    public RetMsg<Serializable> editRmbDepositOut(@RequestBody IfsRmbDepositout record) {
        iFSService.editRmbDepositOut(record);
        return RetMsgHelper.ok();
    }

    //删除
    @ResponseBody
    @RequestMapping(value = "/deleteRmbDepositOut")
    public RetMsg<Serializable> deleteRmbDepositOut(@RequestBody Map<String, Object> map) {
        String ticketid = map.get("ticketid").toString();
        iFSService.deleteRmbDepositOut(ticketid);
        return RetMsgHelper.ok();
    }

    //本金现金流查询
    @ResponseBody
    @RequestMapping(value = "/searchAmt")
    public RetMsg<List<CashflowCapital>> searchAmt(@RequestBody Map<String, Object> map) {
//        String ticketid = map.get("ticketid").toString();
//        iFSService.deleteRmbDepositOut(ticketid);
        List<CashflowCapital> capitalCashFlow = iFSService.searchAmt(map);
        return RetMsgHelper.ok(capitalCashFlow);
    }

    //利息现金流查询
    @ResponseBody
    @RequestMapping(value = "/searchInt")
    public RetMsg<List<CashflowInterest>> searchInt(@RequestBody Map<String, Object> map) {
//        String ticketid = map.get("ticketid").toString();
//        iFSService.deleteRmbDepositOut(ticketid);
        List<CashflowInterest> intCashFlow = iFSService.searchInt(map);
        return RetMsgHelper.ok(intCashFlow);
    }

    //根据传入的本金计划生成利息计划
    @ResponseBody
    @RequestMapping(value = "/getInterest")
    public RetMsg<List<CashflowInterest>> getInterest(@RequestBody Map<String,Object> map){
        List<CashflowInterest> objects = iFSService.getInterestByCapital(map);
        return RetMsgHelper.ok(objects);
    }

    //保存自定义的现金流
    @ResponseBody
    @RequestMapping(value = "/saveCashFlow")
    public RetMsg<Serializable> saveCashFlow(@RequestBody Map<String,Object> map){
        iFSService.saveCashFlow(map);
        return RetMsgHelper.ok();
    }

}
