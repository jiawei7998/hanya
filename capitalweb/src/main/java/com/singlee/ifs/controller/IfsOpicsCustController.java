package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.eu.service.EdCustManangeService;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.bean.SlCommonBean;
import com.singlee.financial.bean.SlCustBean;
import com.singlee.financial.bean.SlDealModule;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.esb.dlcb.IQueryCustServer;
import com.singlee.financial.esb.tlcb.IBuildServer;
import com.singlee.financial.esb.tlcb.IMaintainServer;
import com.singlee.financial.esb.tlcb.IQueryServer;
import com.singlee.financial.opics.IAcupServer;
import com.singlee.financial.opics.IBaseServer;
import com.singlee.financial.opics.IExternalServer;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.SlErrors;
import com.singlee.financial.wks.bean.CustWksBean;
import com.singlee.financial.wks.intfc.CustomService;
import com.singlee.ifs.model.EcifRetMsg;
import com.singlee.ifs.model.IfsOpicsCust;
import com.singlee.ifs.service.IfsForeignCcyService;
import com.singlee.ifs.service.IfsOpicsCustService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * OPICS交易对手Controller
 * 
 * ClassName: IfsOpicsCustController <br/>
 * date: 2018-5-30 下午07:40:39 <br/>
 * 
 * @author zhengfl
 * @version
 * @since JDK 1.6
 */
@Controller
@RequestMapping(value = "/IfsOpicsCustController")
public class IfsOpicsCustController {

	@Autowired
	IfsOpicsCustService ifsOpicsCustService;

	@Autowired
	IExternalServer externalServer;

	@Autowired
	IBuildServer buildServer; // 对公客户新建

	@Autowired
	IMaintainServer maintainServer; // 对公客户维护

	@Autowired
	IQueryServer queryServer; // 对公客户查询

	@Autowired
	IBaseServer iBaseServer;

	@Autowired
	IAcupServer acupServer;

	@Autowired
	IQueryCustServer queryCustServer; // 同业客户查询

	@Autowired
	IfsForeignCcyService foreignCcyServer; // 外汇代客查询

	@Autowired
	EdCustManangeService edCustManangeService;

	@Autowired
	CustomService customService;

	/***
	 * 分页查询 交易对手
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageOpicsCust")
	public RetMsg<PageInfo<IfsOpicsCust>> searchPageOpicsCust(@RequestBody Map<String, Object> map) {
		Page<IfsOpicsCust> page = ifsOpicsCustService.searchPageOpicsCust(map);
		return RetMsgHelper.ok(page);
	}

	
	/***
     * 查询某一条 交易对手信息
     */
    @ResponseBody
    @RequestMapping(value = "/searchSingle")
    public List<Map<String,Object>> searchSingle(@RequestBody Map<String, Object> map) {
        List<Map<String,Object>> list = ifsOpicsCustService.searchSingleAll(map);
        return list;
    }

    
	
	
	/***
	 * 删除 交易要素
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteOpicsCust")
	public RetMsg<Serializable> deleteOpicsCust(@RequestBody Map<String, String> map) {
		String cno = map.get("cno");
		ifsOpicsCustService.deleteById(cno);
		return RetMsgHelper.ok();
	}

	/**
	 * 保存 交易要素 新增
	 */
	@ResponseBody
	@RequestMapping(value = "/saveOpicsCustAdd")
	public RetMsg<Serializable> saveOpicsCustAdd(@RequestBody IfsOpicsCust entity) {
		String cno = entity.getCno(); // 交易对手编号
//		if (null == cno || "".equals(cno)) {
//			return RetMsgHelper.simple("cNo-Error", "新增时没有交易对手编号");
//		}
		/**
		 * 查询有数据，则说明此客户号对应的交易对手已存在，则提示"客户已存在"
		 */
		/*List<IfsOpicsCust> list = ifsOpicsCustService.searchSingle(entity);
		if (list.size() != 0) {
			return RetMsgHelper.ok("0");
		}*/
		ifsOpicsCustService.insert(entity);
		return RetMsgHelper.ok("error.common.0000"); // 正常返回
	}

	/**
	 * 对公客户信息新建
	 */
	@ResponseBody
	@RequestMapping(value = "/buildCust")
	public RetMsg<EcifRetMsg> OpicsCustAdd(@RequestBody IfsOpicsCust entity) {
		EcifRetMsg msg = new EcifRetMsg();
		try {
			SlCustBean slCustBean = new SlCustBean("01", SlDealModule.CUST.SERVER, SlDealModule.CUST.TAG, SlDealModule.CUST.DETAIL);
			slCustBean.setGtype(entity.getGtype());
			slCustBean.setGid(entity.getGid());
			slCustBean.setCliname(entity.getCliname());
			slCustBean.setCrossBordFlag(entity.getFlag());
			slCustBean.setFinLicenseCode(entity.getLicense());
			slCustBean.setFinOrgType(entity.getFintype());
			slCustBean.setMngeScop(entity.getRegplace());
			slCustBean.setCustTelNo(entity.getTellerno());
			slCustBean.setCustOrgNo(entity.getOrgno());
			slCustBean.setCa4(entity.getClitype()); // 客户类型(0-同业;2-对公)
			slCustBean.setCa5(SlSessionHelper.getUserId()); // 泰隆要求传入当前用户
			SlOutBean result = buildServer.ecif0031(slCustBean);
			msg.setCode(result.getRetCode());
			msg.setClino(result.getClientNo());
			msg.setMsg(result.getRetMsg());
		} catch (Exception e) {
			e.getStackTrace();
		}
		return RetMsgHelper.ok(msg);
	}

	/**
	 * 对公客户信息查询
	 */
	@ResponseBody
	@RequestMapping(value = "/queryCustInfo")
	public RetMsg<EcifRetMsg> OpicsCustQuery(@RequestBody String clino) {
		EcifRetMsg msg = new EcifRetMsg();
		try {
			SlCustBean slCustBean = new SlCustBean("01", SlDealModule.CUST.SERVER, SlDealModule.CUST.TAG, SlDealModule.CUST.DETAIL);
			slCustBean.setCa5(SlSessionHelper.getUserId());
			slCustBean.setEcifNo(clino);
			// 查数据 20300731
			// Map<String, Object> map=new HashMap<String, Object>();
			// map.put("queryDate","20300731");
			// foreignCcyServer.queryForeignCcy(map);

			SlCustBean result = queryCustServer.searchOneCust(slCustBean);
			msg.setCode(result.getRetCode());
			msg.setMsg(result.getRetMsg());
			msg.setCLIENT_NAME(result.getCliname());
			msg.setGLOBAL_TYPE(result.getGtype());
			msg.setGLOBAL_ID(result.getGid());
			msg.setFIN_LICENSE_CODE(result.getFinLicenseCode());
			msg.setCROSS_BORD_FLAG(result.getCrossBordFlag());
			msg.setFIN_ORG_TYPE(result.getFinOrgType());
			msg.setNTNL_INDSTRY_TP(result.getNtnlIndstryTp());
			msg.setCTZN_ECNM_DEPT_TP(result.getCtznEcnmDeptTp());
			msg.setMNGE_SCOP(result.getMngeScop());
			msg.setCustTelNo(result.getCustTelNo());
			msg.setCustOrgNo(result.getCustOrgNo());
			msg.setCLIENT_TYPE(result.getCa1());
		} catch (Exception e) {
			e.getStackTrace();
		}
		return RetMsgHelper.ok(msg);
	}

	/**
	 * 保存 交易要素 修改
	 */
	@ResponseBody
	@RequestMapping(value = "/saveOpicsCustEdit")
	public RetMsg<Serializable> saveOpicsCustEdit(@RequestBody IfsOpicsCust entity) {
		String cno = entity.getCno();
		if (null == cno || "".equals(cno)) {
			return RetMsgHelper.simple("cNo-Error", "修改时没有交易对手编号!");
		}
		IfsOpicsCust opicsCust = ifsOpicsCustService.searchIfsOpicsCust(cno);
		String localStatus = opicsCust.getLocalstatus();
		entity.setLocalstatus(localStatus);
		ifsOpicsCustService.updateById(entity);
		/**
		 * 查询有数据，则说明此客户号对应的交易对手已存在，则提示"客户已存在"
		 */
		// List<IfsOpicsCust> list = ifsOpicsCustService.searchSingle(entity);
		// if (list.size() != 0) {
		// return RetMsgHelper.ok("0");
		// }
		return RetMsgHelper.ok("error.common.0000"); // 正常返回
	}

	/***
	 * 根据交易对手编号查询实体类
	 */
	@ResponseBody
	@RequestMapping(value = "/searchIfsOpicsCust")
	public RetMsg<IfsOpicsCust> searchIfsOpicsCust(@RequestBody Map<String, String> map) {
		String cno = map.get("cno");
		IfsOpicsCust entity = ifsOpicsCustService.searchIfsOpicsCust(cno);
		return RetMsgHelper.ok(entity);
	}

	/***
	 * 客户信息同步
	 */
	@ResponseBody
	@RequestMapping(value = "/syncCust")
	public RetMsg<SlOutBean> syncOpicsCust(@RequestBody Map<String, String> map) {
		String cno = map.get("cno");
		IfsOpicsCust entity = ifsOpicsCustService.searchIfsOpicsCust(cno);
		SlCustBean custBean = new SlCustBean("01", SlDealModule.CUST.SERVER, SlDealModule.CUST.TAG, SlDealModule.CUST.DETAIL);

		custBean.getInthBean().setFedealno(entity.getFedealno());
		custBean.setCno(cno);
		custBean.setCtype(entity.getCtype());
		custBean.setCcode(entity.getCcode());
		custBean.setCa1(entity.getCa());
		custBean.setUccode(entity.getUccode());
		custBean.setAcctngtype(entity.getAcctngtype());
		custBean.setGtype(entity.getGtype());
		custBean.setGid(entity.getGid());
		custBean.setCliname(entity.getCliname());
		custBean.setCpost(entity.getCpost());
		custBean.setTaxid(entity.getTaxid());
		custBean.setSic(entity.getSic());
		custBean.setSn(entity.getSn());
		custBean.setCfn1(entity.getCfn());
		custBean.setCmne(entity.getCname());
		custBean.setCa4(entity.getLocation());
		custBean.setBic(entity.getBic());
		SlOutBean outBean = new SlOutBean();
		try {
//			customService
			outBean = externalServer.addCust(custBean);
			if ("S".equals(String.valueOf(outBean.getRetStatus()))) {
				entity.setLocalstatus("1"); // 已同步
				entity.setStatcode("-1");
				ifsOpicsCustService.updateStatus(entity);
			}
		} catch (RemoteConnectFailureException e) {
			outBean.setRetCode(SlErrors.FAILED_RMI_OPICS.getErrCode());
			outBean.setRetMsg(SlErrors.FAILED_RMI_OPICS.getErrMsg() + ":" + e.getMessage());
			outBean.setRetStatus(RetStatusEnum.F);
		} catch (Exception e) {
			outBean.setRetCode(SlErrors.FAILED.getErrCode());
			outBean.setRetMsg(SlErrors.FAILED.getErrMsg() + ":" + e.getMessage());
			outBean.setRetStatus(RetStatusEnum.F);
		}

		return RetMsgHelper.ok(outBean);
	}

	/***
	 * 客户信息同步(直插)
	 */
	@ResponseBody
	@RequestMapping(value = "/syncCustD")
	public RetMsg<SlOutBean> syncOpicsCustD(@RequestBody Map<String, String> map) {
		String cno = map.get("cno");
		IfsOpicsCust entity = ifsOpicsCustService.searchIfsOpicsCust(cno);
		CustWksBean custBean = new CustWksBean();
//		SlCustBean custBean = new SlCustBean("01", SlDealModule.CUST.SERVER, SlDealModule.CUST.TAG, SlDealModule.CUST.DETAIL);

		custBean.setCustId(entity.getCno());
		custBean.setCustCode(entity.getCname());//对手简称
		custBean.setSwiftCode(entity.getBic());//swift code
		custBean.setCustEshortname(entity.getSn());//简称
		custBean.setCustSmallKind(entity.getSic());//工业代码
		custBean.setCustEfullname(entity.getCfn());//全称
		custBean.setCustCountry(entity.getCcode());//国家代码
		custBean.setCustArea(entity.getCa());//国家代码
		custBean.setCustCategory(entity.getCtype());//客户类型
		custBean.setAcctngtype(entity.getAcctngtype());//会计类型

		SlOutBean outBean = new SlOutBean();
		try {
			outBean = customService.saveEntry(custBean);
//			outBean = externalServer.addCust(custBean);
			if ("S".equals(String.valueOf(outBean.getRetStatus()))) {
				entity.setLocalstatus("1"); // 已同步
				entity.setStatcode("-1");
				ifsOpicsCustService.updateStatus(entity);
			}
		} catch (RemoteConnectFailureException e) {
			outBean.setRetCode(SlErrors.FAILED_RMI_OPICS.getErrCode());
			outBean.setRetMsg(SlErrors.FAILED_RMI_OPICS.getErrMsg() + ":" + e.getMessage());
			outBean.setRetStatus(RetStatusEnum.F);
		} catch (Exception e) {
			outBean.setRetCode(SlErrors.FAILED.getErrCode());
			outBean.setRetMsg(SlErrors.FAILED.getErrMsg() + ":" + e.getMessage());
			outBean.setRetStatus(RetStatusEnum.F);
		}

		return RetMsgHelper.ok(outBean);
	}

	/**
	 * 批量校准信息
	 */
	@ResponseBody
	@RequestMapping(value = "/batchCalibration")
	public RetMsg<SlOutBean> verify(@RequestBody Map<String, String> map) {
		SlOutBean outBean = new SlOutBean();
		String type = String.valueOf(map.get("type"));
		String cno = "";
		String[] cnos;
		if ("1".equals(type)) { // 选中记录校准
			cno = map.get("cno");
			cnos = cno.split(",");
		} else { // 全部校准
			cnos = null;
		}
		try {
			outBean = ifsOpicsCustService.batchCalibration(type, cnos);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return RetMsgHelper.ok(outBean);
	}

	/**
	 * 更新客户信息
	 */
	@ResponseBody
	@RequestMapping(value = "/updateCustInfo")
	public String updateCustInfo(@RequestBody String clino) {
		String msg = "";
		try {
			SlCustBean slCustBean = new SlCustBean("01", SlDealModule.CUST.SERVER, SlDealModule.CUST.TAG, SlDealModule.CUST.DETAIL);
			slCustBean.setEcifNo(clino);
			slCustBean.setCa5(SlSessionHelper.getUserId());
			SlCustBean result = queryCustServer.searchOneCust(slCustBean);
			ifsOpicsCustService.updateCustInfo(result);
			msg = result.getRetCode();
		} catch (Exception e) {
			e.getStackTrace();
		}
		return msg;
	}

	/**
	 * 对公客户信息维护
	 */
	@ResponseBody
	@RequestMapping(value = "/alterCust")
	public RetMsg<EcifRetMsg> alterCust(@RequestBody IfsOpicsCust entity) {
		EcifRetMsg msg = new EcifRetMsg();
		try {
			SlCustBean slCustBean = new SlCustBean("01", SlDealModule.CUST.SERVER, SlDealModule.CUST.TAG, SlDealModule.CUST.DETAIL);
			slCustBean.setEcifNo(entity.getClino());
			slCustBean.setCa4(entity.getClitype()); // 客户类型(0-同业;1-其他;2-对公)
			slCustBean.setCliname(entity.getCliname()); // 客户中文名称
			slCustBean.setCrossBordFlag(entity.getFlag()); // 境内外标志
			// slCustBean.setGtype(entity.getGtype()); // 证件类型
			// slCustBean.setGid(entity.getGid()); //证件号码
			slCustBean.setCustTelNo(entity.getTellerno()); // 客户建档柜员号
			slCustBean.setCustOrgNo(entity.getOrgno()); // 客户建档机构号
			slCustBean.setFinOrgType(entity.getFintype()); // 金融机构类型
			slCustBean.setFinLicenseCode(entity.getLicense()); // 客户金融许可证
			slCustBean.setMngeScop(entity.getRegplace()); // 注册地/经营所在地
			slCustBean.setCa5(SlSessionHelper.getUserId()); // 泰隆要求传入当前用户
			SlOutBean result = maintainServer.ecif0032(slCustBean);
			msg.setCode(result.getRetCode());
			msg.setClino(result.getClientNo());
			msg.setMsg(result.getRetMsg());
		} catch (Exception e) {
			e.getStackTrace();
		}
		return RetMsgHelper.ok(msg);
	}

	/**
	 * 查询获取DV01，久期限，止损值查询
	 * 
	 * @throws Exception
	 * @throws RemoteConnectFailureException
	 */
	@ResponseBody
	@RequestMapping(value = "/queryMarketData")
	public RetMsg<SlCommonBean> queryMarketData(@RequestBody Map<String, Object> map) throws RemoteConnectFailureException, Exception {

		SlCommonBean slCommonBean = new SlCommonBean();
		slCommonBean = edCustManangeService.queryMarketData(map);
		return RetMsgHelper.ok(slCommonBean);
	}

	/**
	 * 查询外汇止损值总和
	 * 
	 * @throws Exception
	 * @throws RemoteConnectFailureException
	 */
	@ResponseBody
	@RequestMapping(value = "/queryFxdSum")
	public String queryFxdSum(@RequestBody Map<String, Object> map) throws RemoteConnectFailureException, Exception {

		String fxdSum = edCustManangeService.queryFxdSum(map);
		return fxdSum;
	}

	/***
	 * 经办客户信息
	 */
	@ResponseBody
	@RequestMapping(value = "/sendCust")
	public RetMsg<Serializable> sendCust(@RequestBody IfsOpicsCust entity) {
		entity.setIoper(SlSessionHelper.getUserId());
		ifsOpicsCustService.sendCust(entity);
		return RetMsgHelper.ok();
	}



	/***
	 * 复核
	 */
	@ResponseBody
	@RequestMapping(value = "/checkCust")
	public RetMsg<Serializable> checkCust(@RequestBody IfsOpicsCust entity) {
		entity.setReviewer(SlSessionHelper.getUserId());
		ifsOpicsCustService.sendCust(entity);
		return RetMsgHelper.ok();
	}




}