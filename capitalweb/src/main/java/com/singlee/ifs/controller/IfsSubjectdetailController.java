package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.ifs.model.IfsSubjectdetail;
import com.singlee.ifs.service.IfsSubjectdetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.Map;

//opics科目余额
@Controller
@RequestMapping(value = "/IfsSubjectdetailController")
public class IfsSubjectdetailController extends CommonController {

    @Autowired
    IfsSubjectdetailService ifsSubjectdetailService;
    /**
     * opics科目余额查询
     * @param map
     * @return
     *
     *
     */
    @ResponseBody
    @RequestMapping(value = "/searchpage")
    public RetMsg<PageInfo<IfsSubjectdetail>> searchpage(@RequestBody Map<String, Object> map) {
        Page<IfsSubjectdetail> page = ifsSubjectdetailService.getList(map);
        return RetMsgHelper.ok(page);
    }


    /**
     * opics科目余额修改
     * @param map
     * @return
     *
     *
     */
    @ResponseBody
    @RequestMapping(value = "/updateOpicsBalance")
    public RetMsg<Serializable> updateOpicsBalance(@RequestBody  Map<String, Object> map) {
        String postdate=map.get("postdate")==null?"":map.get("postdate").toString().substring(0,10);
        String prepostdate=map.get("prepostdate")==null?"":map.get("prepostdate").toString().substring(0,10);
        map.put("postdate",postdate);
        map.put("prepostdate",prepostdate);
        ifsSubjectdetailService.updateOpicsBalance(map);
        return RetMsgHelper.ok();
    }

    /**
     * opics科目余额添加
     * @param map
     * @return
     *
     *
     */
    @ResponseBody
    @RequestMapping(value = "/addOpicsBalance")
    public RetMsg<Serializable> addOpicsBalance(@RequestBody  Map<String, Object> map) {
        String postdate=map.get("postdate")==null?"":map.get("postdate").toString().substring(0,10);
        String prepostdate=map.get("prepostdate")==null?"":map.get("prepostdate").toString().substring(0,10);
        map.put("postdate",postdate);
        map.put("prepostdate",prepostdate);
        ifsSubjectdetailService.addOpicsBalance(map);
        return RetMsgHelper.ok();
    }
}
