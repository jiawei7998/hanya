package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.bean.SlDealModule;
import com.singlee.financial.bean.SlSposBean;
import com.singlee.financial.opics.IBaseServer;
import com.singlee.ifs.model.IfsCfetsrmbOr;
import com.singlee.ifs.model.IfsCfetsrmbOrKey;
import com.singlee.ifs.service.IFSService;
import com.singlee.ifs.service.IfsOpicsTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * 买断式回购正式交易审批Controller
 * 
 * ClassName: IfsCfetsrmbOrController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason: TODO ADD REASON(可选). <br/>
 * date: 2018-5-30 下午07:28:08 <br/>
 * 
 * @author lijing
 * @version
 * @since JDK 1.6
 */
@Controller
@RequestMapping(value = "/IfsCfetsrmbOrController")
public class IfsCfetsrmbOrController extends CommonController {
	@Autowired
	private IFSService cfetsrmbOrService;
	@Autowired
	IBaseServer baseServer;
	@Autowired
	IfsOpicsTypeService opicsTypeService;

	/**
	 * 保存成交单
	 * 
	 * @param IfsCfetsrmbOr
	 * @throws Exception 
	 * @throws RemoteConnectFailureException 
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/saveOr")
	public RetMsg<Serializable> saveOr(@RequestBody IfsCfetsrmbOr record) throws RemoteConnectFailureException, Exception {
		String message = cfetsrmbOrService.checkCust(record.getDealTransType(),record.getReverseInst(),record.getCfetsno(), null, record.getProduct(), record.getProdType());
		if(StringUtil.isNotEmpty(message)) {
			return RetMsgHelper.fail(message);
		}
//		String al=opicsTypeService.getAlByType(record.getProdType(),record.getProduct());
		if("S".equals(record.getMyDir())){
			Map<String,Object> map =new HashMap<String, Object>();
			map.put("br", SlDealModule.BASE.BR);
			map.put("ccy", "CNY");
			map.put("port", record.getBondPort());
			map.put("secid",record.getBondCode());
			map.put("invtype",record.getInvtype());
			map.put("cost",record.getBondCost());
			SlSposBean sposBean=baseServer.getSpos(map);
			BigDecimal num;
			if(sposBean==null){
				num=new BigDecimal(0);
			}else{
				num=sposBean.getTotalamt();
			}
			BigDecimal totalFaceValue=record.getTotalFaceValue().multiply(new BigDecimal(10000));
			int a=totalFaceValue.compareTo(num);//-1 小于，0 等于 ， 1 大于
			if(a==1){//totalFaceValue 大于  num
				message=message+"债券"+record.getBondCode()+"短头寸<br>";
			}
		}
		if(StringUtil.isNotEmpty(message)) {//检查失败
			return RetMsgHelper.fail(message);
		}else {//检查通过
			IfsCfetsrmbOr or = cfetsrmbOrService.getOrById(record);
			if (or == null) {
				message = cfetsrmbOrService.insertOr(record);
			} else {
				try {
					cfetsrmbOrService.updateOr(record);
					message = "修改成功";
				} catch (Exception e) {
					e.printStackTrace();
					message = "修改失败";
				}
			}
		}
		return StringUtil.containsIgnoreCase(message, "成功")?RetMsgHelper.ok(message):RetMsgHelper.fail(message);
	}
	//不校验短头寸
	@ResponseBody
	@RequestMapping(value = "/saveOrTrue")
	public RetMsg<Serializable> saveOrTrue(@RequestBody IfsCfetsrmbOr record) throws RemoteConnectFailureException, Exception {
		String message = "";
		IfsCfetsrmbOr or = cfetsrmbOrService.getOrById(record);
		if (or == null) {
			message = cfetsrmbOrService.insertOr(record);
		} else {
			try {
				cfetsrmbOrService.updateOr(record);
				message = "修改成功";
			} catch (Exception e) {
				e.printStackTrace();
				message = "修改失败";
			}
		}
		return StringUtil.containsIgnoreCase(message, "成功")?RetMsgHelper.ok(message):RetMsgHelper.fail(message);
	}
	/**
	 * 成交单分页查询
	 * 
	 * @param IfsCfetsrmbOr
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchCfetsrmbOr")
	public RetMsg<PageInfo<IfsCfetsrmbOr>> searchCfetsrmbOr(@RequestBody IfsCfetsrmbOr record) {
		Page<IfsCfetsrmbOr> page = cfetsrmbOrService.getOrList(record);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 根据主键查询成交单
	 * 
	 * @param IfsCfetsrmbOrKey
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchCfetsrmbOrById")
	public RetMsg<IfsCfetsrmbOr> searchCfetsrmbOrById(@RequestBody IfsCfetsrmbOrKey key) {
		IfsCfetsrmbOr or = cfetsrmbOrService.getOrById(key);
		return RetMsgHelper.ok(or);
	}

	/**
	 * 根据主键查询成交单(返回值增加 流程id)
	 * 
	 * @param
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchCfetsrmbOrAndFlowIdById")
	public RetMsg<IfsCfetsrmbOr> searchCfetsrmbOrAndFlowIdById(@RequestBody Map<String, Object> key) {
		IfsCfetsrmbOr irs = cfetsrmbOrService.getOrAndFlowIdById(key);
		return RetMsgHelper.ok(irs);
	}

	/**
	 * 删除成交单
	 * 
	 * @param IfsCfetsrmbOrKey
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/deleteCfetsrmbOr")
	public RetMsg<Serializable> deleteCfetsrmbOr(@RequestBody IfsCfetsrmbOrKey key) {
		cfetsrmbOrService.deleteOr(key);
		return RetMsgHelper.ok();
	}

	/**
	 * 根据主键查询成交单
	 * 
	 * @param IfsCfetsrmbOrKey
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/selectById")
	public RetMsg<IfsCfetsrmbOr> selectById(@RequestBody Map<String, String> map) {
		IfsCfetsrmbOr or = cfetsrmbOrService.selectById(map);
		return RetMsgHelper.ok(or);
	}
	
	/**
	 * 查询我发起的
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-21
	 */
	@ResponseBody
	@RequestMapping(value = "/searchRmbOrSwapMine")
	public RetMsg<PageInfo<IfsCfetsrmbOr>> searchRmbOrSwapMine(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsrmbOr> page = cfetsrmbOrService.getRmbOrMinePage(params, 3);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 查询 待审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageRmbOrUnfinished")
	public RetMsg<PageInfo<IfsCfetsrmbOr>> searchPageRmbOrUnfinished(@RequestBody Map<String, Object> params) {
		String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse + ","
				+ DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
		String[] orderStatus = approveStatus.split(",");
		params.put("approveStatusNo", orderStatus);
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsrmbOr> page = cfetsrmbOrService.getRmbOrMinePage(params, 1);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 查询 已审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageRmbOrFinished")
	public RetMsg<PageInfo<IfsCfetsrmbOr>> searchPageRmbOrFinished(@RequestBody Map<String, Object> params) {
		String approveStatus = ParameterUtil.getString(params, "approveStatus", "");
		if (StringUtil.isNullOrEmpty(approveStatus)) {
			approveStatus = DictConstants.ApproveStatus.ApprovedPass;
			approveStatus += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatus += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatus += "," + DictConstants.ApproveStatus.Verified;
			approveStatus += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			// 20180502修改bug:已审批列表中增加"审批中"
			approveStatus += "," + DictConstants.ApproveStatus.Approving;

			String[] orderStatus = approveStatus.split(",");
			params.put("approveStatusNo", orderStatus);
		}
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsrmbOr> page = cfetsrmbOrService.getRmbOrMinePage(params, 2);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 勾兑时，查询表中审批通过的数据
	 * 
	 * @param
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchPageApprovermbOrPassed")
	public RetMsg<PageInfo<IfsCfetsrmbOr>> searchPageApprovermbOrPassed(@RequestBody Map<String, Object> params) {
		Page<IfsCfetsrmbOr> page = cfetsrmbOrService.getApproveOrPassedPage(params);
		return RetMsgHelper.ok(page);
	}
	
	//根据ticketId查询
	@ResponseBody
	@RequestMapping(value = "/searchCfetsrmbOrByTicketId")
	public RetMsg<IfsCfetsrmbOr> searchCfetsrmbOrByTicketId(@RequestBody Map<String, Object> map) {
		String ticketId = map.get("ticketId").toString();
		IfsCfetsrmbOr or = cfetsrmbOrService.searchCfetsrmbOrByTicketId(ticketId);
		return RetMsgHelper.ok(or);
	}
	
	//根据ticketId查询详情
	@ResponseBody
	@RequestMapping(value = "/searchOrDetailByTicketId")
	public RetMsg<IfsCfetsrmbOr> searchOrDetailByTicketId(@RequestBody Map<String, Object> map) {
		IfsCfetsrmbOr or = cfetsrmbOrService.searchOrDetailByTicketId(map);
		return RetMsgHelper.ok(or);
	}
}
