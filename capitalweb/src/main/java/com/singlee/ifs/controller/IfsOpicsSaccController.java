package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.bean.SlSaccBean;
import com.singlee.financial.opics.IStaticServer;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.SlErrors;
import com.singlee.ifs.model.IfsOpicsSacc;
import com.singlee.ifs.service.IfsOpicsSaccService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * SACC
 * 
 * @author yanming
 */

@Controller
@RequestMapping(value = "/IfsOpicsSaccController")
public class IfsOpicsSaccController {

	@Autowired
	IfsOpicsSaccService ifsOpicsSaccService;

	@Autowired
	IStaticServer iStaticServer;

	/***
	 * 分页查询
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageSacc")
	public RetMsg<PageInfo<IfsOpicsSacc>> searchPageSacc(@RequestBody Map<String, Object> map) {
		Page<IfsOpicsSacc> page = ifsOpicsSaccService.searchPageOpicsSacc(map);
		return RetMsgHelper.ok(page);
	}

	/***
	 * 根据id查询实体类
	 */
	@ResponseBody
	@RequestMapping(value = "/searchById")
	public RetMsg<IfsOpicsSacc> searchById(@RequestBody Map<String, String> map) {
		IfsOpicsSacc ifsOpicsSacc = ifsOpicsSaccService.searchById(map);
		return RetMsgHelper.ok(ifsOpicsSacc);
	}

	/**
	 * 新增
	 */
	@ResponseBody
	@RequestMapping(value = "/saveOpicsSaccAdd")
	public RetMsg<Serializable> saveOpicsSaccAdd(@RequestBody IfsOpicsSacc entity) {
		ifsOpicsSaccService.insert(entity);
		return RetMsgHelper.ok();
	}

	/**
	 * 修改
	 */
	@ResponseBody
	@RequestMapping(value = "/saveOpicsSaccEdit")
	public RetMsg<Serializable> saveOpicsSaccEdit(@RequestBody IfsOpicsSacc entity) {
		ifsOpicsSaccService.updateById(entity);
		return RetMsgHelper.ok();
	}

	/**
	 * 删除
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteSacc")
	public RetMsg<Serializable> deleteSacc(@RequestBody Map<String, String> map) {
		ifsOpicsSaccService.deleteById(map);
		return RetMsgHelper.ok();
	}

	/**
	 * 同步opics表
	 */
	@ResponseBody
	@RequestMapping(value = "/syncSacc")
	public RetMsg<SlOutBean> syncSacc(@RequestBody Map<String, String> map) {
		SlOutBean slOutBean = new SlOutBean();

		String accountno = map.get("accountno");
		String[] accountnos = accountno.split(",");
		String br = map.get("br");
		String[] brs = br.split(",");

		for (int i = 0; i < accountnos.length; i++) {
			HashMap<String, String> hashmap = new HashMap<String, String>();
			hashmap.put("accountno", accountnos[i]);
			hashmap.put("br", brs[i]);

			IfsOpicsSacc ifsOpicsSacc = ifsOpicsSaccService.searchById(hashmap);
			SlSaccBean saccBean = new SlSaccBean();
			saccBean.setBr(ifsOpicsSacc.getBr());
			saccBean.setAccountno(accountnos[i]);
			saccBean.setTrad(ifsOpicsSacc.getTrad());
			saccBean.setOpendate(ifsOpicsSacc.getOpendate());
			saccBean.setAccttitle(ifsOpicsSacc.getAccttitle());
			saccBean.setLstmntdate(ifsOpicsSacc.getLstmntdate());
			saccBean.setStmtcycle(ifsOpicsSacc.getStmtcycle());
			saccBean.setStmtdayrule(ifsOpicsSacc.getStmtdayrule());
			saccBean.setShortposind(ifsOpicsSacc.getShortposind());
			try {
				slOutBean = iStaticServer.addSacc(saccBean);
			} catch (RemoteConnectFailureException e) {
				slOutBean.setRetCode(SlErrors.FAILED_RMI_OPICS.getErrCode());
				slOutBean.setRetMsg(SlErrors.FAILED_RMI_OPICS.getErrMsg() + ":" + e.getMessage());
				slOutBean.setRetStatus(RetStatusEnum.F);
			} catch (Exception e) {
				slOutBean.setRetCode(SlErrors.FAILED.getErrCode());
				slOutBean.setRetMsg(SlErrors.FAILED.getErrMsg() + ":" + e.getMessage());
				slOutBean.setRetStatus(RetStatusEnum.F);
			}
			if ("S".equals(String.valueOf(slOutBean.getRetStatus()))) {
				ifsOpicsSacc.setStatus("1");
				ifsOpicsSaccService.updateStatus(ifsOpicsSacc);
			}
		}
		return RetMsgHelper.ok(slOutBean);
	}

	/**
	 * 批量校准
	 */
	@ResponseBody
	@RequestMapping(value = "/batchCalibration")
	public RetMsg<SlOutBean> batchCalibration(@RequestBody Map<String, String> map) {
		String type = String.valueOf(map.get("type"));
		String accountno;
		String br;
		String[] accountnos;
		String[] brs;
		if ("1".equals(type)) {
			accountno = map.get("accountno");
			accountnos = accountno.split(",");
			br = map.get("br");
			brs = br.split(",");

		} else {
			accountnos = null;
			brs = null;
		}
		SlOutBean slOutBean = ifsOpicsSaccService.batchCalibration(type, accountnos, brs);
		return RetMsgHelper.ok(slOutBean);
	}

}
