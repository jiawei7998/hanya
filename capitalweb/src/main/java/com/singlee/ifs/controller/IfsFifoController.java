package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.financial.bean.SlFifoBean;
import com.singlee.financial.opics.IFifoServer;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;
@Controller
@RequestMapping(value = "/IfsFifoController")
public class IfsFifoController extends CommonController{

	
	
	@Autowired
	IFifoServer iFifoServer;
	/***
	 * 生产FIFO表数据
	 */
	@ResponseBody
	@RequestMapping(value = "/searchSecm")
	public RetMsg<PageInfo<SlFifoBean>> searchSecm(@RequestBody Map<String, Object> map) {
		        
	    List<SlFifoBean>  list =iFifoServer.getAllList(map);
		 int pageNumber=(int)map.get("pageNumber");
		 int  pageSize=(int)map.get("pageSize");
		 Page<SlFifoBean> page = HrbReportUtils.producePage(list, pageNumber, pageSize);
		 
		return RetMsgHelper.ok(page);
	}


	
    
    
	
}
