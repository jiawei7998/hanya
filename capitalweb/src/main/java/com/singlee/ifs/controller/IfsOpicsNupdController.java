package com.singlee.ifs.controller;

import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.financial.bean.SlNupdBean;
import com.singlee.financial.esb.hsbank.INupdServer;
import com.singlee.financial.page.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

/**
 * OPICS总账业务处理Controller
 * 
 * ClassName: IfsOpicsAcupController <br/>
 * Function: 主要处理OPICS日终批后操作 <br/>
 * Reason: 提供OPICS接口操作. <br/>
 * date: 2018-5-30 下午07:02:46 <br/>
 * 
 * @author Fll
 * @version
 * @since JDK 1.6
 */
@Controller
@RequestMapping(value = "/IfsOpicsNupdController")
public class IfsOpicsNupdController extends CommonController {

	@Autowired
	INupdServer nupdServer;

	/**
	 * 往来账查询查询
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchNupdList")
	public RetMsg<PageInfo<SlNupdBean>> searchNupdList(@RequestBody Map<String, String> params) {
		PageInfo<SlNupdBean> page = nupdServer.getNupdRetMsg(params);
		return RetMsgHelper.ok(page);
	}
	
	@ResponseBody
	@RequestMapping(value = "/searchNupdById")
	public RetMsg<List<SlNupdBean>> searchNupdById(@RequestBody Map<String, String> params) {
		List<SlNupdBean> list = nupdServer.getNupdById(params);
		return RetMsgHelper.ok(list);
	}
}
