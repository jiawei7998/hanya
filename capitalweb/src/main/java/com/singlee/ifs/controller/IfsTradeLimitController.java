package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.ifs.model.IfsTradeLimit;
import com.singlee.ifs.service.IfsTradeLimitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.List;
import java.util.Map;


/**
 * @author copysun
 */
@Controller
@RequestMapping(value = "/IfsTradeLimitController")
public class IfsTradeLimitController {

	@Autowired
	IfsTradeLimitService ifsTradeLimitService;

	/***
	 * 分页查询
	 */
	@ResponseBody
	@RequestMapping(value = "/getDataPage")
	public RetMsg<PageInfo<IfsTradeLimit>> getDataPage(@RequestBody Map<String, Object> map) {

		Page<IfsTradeLimit> page = ifsTradeLimitService.getDataPage(map);
		return RetMsgHelper.ok(page);
	}

	/***
	 * 添加
	 */
	@ResponseBody
	@RequestMapping(value = "/addData",method = RequestMethod.POST)
	public RetMsg<Serializable> addData( @RequestBody Map<String,Object> map) {

		if(map.get("id")==null){
			ifsTradeLimitService.addData(map);
		}else{
			ifsTradeLimitService.update(map);
		}
		return RetMsgHelper.ok("保存成功!");
	}

	/***
	 * 添加
	 */
	@ResponseBody
	@RequestMapping(value = "/delData",method = RequestMethod.POST)
	public RetMsg<Serializable> delData( @RequestBody List<Map<String,Object>> list) {
		ifsTradeLimitService.delData(list);
		return RetMsgHelper.ok("删除成功!");
	}

}
