package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.ifs.model.IfsForeignccyDish;
import com.singlee.ifs.service.IfsForeignCcyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;


@Controller
@RequestMapping(value = "/IfsForeignccyDishController")
public class IfsForeignccyDishController extends CommonController {
	@Autowired
	private IfsForeignCcyService ccyService;

	/**
	 * 分页查询
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchPageDish")
	public RetMsg<PageInfo<IfsForeignccyDish>> searchPageDish(@RequestBody Map<String, Object> map) {
		Page<IfsForeignccyDish> page = ccyService.searchPageDish(map);
		return RetMsgHelper.ok(page);
	}

}
