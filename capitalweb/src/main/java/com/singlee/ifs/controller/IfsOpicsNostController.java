package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.financial.bean.SlNostBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.opics.IStaticServer;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.SlErrors;
import com.singlee.ifs.model.IfsOpicsNost;
import com.singlee.ifs.service.IfsOpicsNostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.Map;

/***
 * 清算路径
 * 
 * @author lij
 * 
 */
@Controller
@RequestMapping(value = "/IfsOpicsNostController")
public class IfsOpicsNostController {

	@Autowired
	IfsOpicsNostService ifsOpicsNostService;

	@Autowired
	IStaticServer iStaticServer;

	/***
	 * 分页查询
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageNost")
	public RetMsg<PageInfo<IfsOpicsNost>> searchPageNost(@RequestBody Map<String, Object> map) {
		Page<IfsOpicsNost> page = ifsOpicsNostService.searchPageOpicsNost(map);
		return RetMsgHelper.ok(page);
	}

	/***
	 * 根据id查询实体类
	 */
	@ResponseBody
	@RequestMapping(value = "/searchNostById")
	public RetMsg<IfsOpicsNost> searchNostById(@RequestBody Map<String, String> map) {
		String nos = map.get("nos");
		IfsOpicsNost ifsOpicsNost = ifsOpicsNostService.searchNostById(nos);
		return RetMsgHelper.ok(ifsOpicsNost);
	}

	/**
	 * 新增
	 */
	@ResponseBody
	@RequestMapping(value = "/saveOpicsNostAdd")
	public RetMsg<Serializable> saveOpicsNostAdd(@RequestBody IfsOpicsNost entity) {
		ifsOpicsNostService.insert(entity);
		return RetMsgHelper.ok();
	}

	/**
	 * 修改
	 */
	@ResponseBody
	@RequestMapping(value = "/saveOpicsNostEdit")
	public RetMsg<Serializable> saveOpicsNostEdit(@RequestBody IfsOpicsNost entity) {
		ifsOpicsNostService.updateById(entity);
		return RetMsgHelper.ok();
	}

	/**
	 * 删除
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteNost")
	public RetMsg<Serializable> deleteNost(@RequestBody Map<String, String> map) {
		String nos = map.get("nos");
		ifsOpicsNostService.deleteById(nos);
		return RetMsgHelper.ok();
	}

	/**
	 * 同步
	 */
	@ResponseBody
	@RequestMapping(value = "/syncNost")
	public RetMsg<SlOutBean> syncNost(@RequestBody Map<String, String> map) {
		SlOutBean slOutBean = new SlOutBean();
		String nos = map.get("nos");
		String[] noss = nos.split(",");
		for (int i = 0; i < noss.length; i++) {
			IfsOpicsNost ifsOpicsNost = ifsOpicsNostService.searchNostById(noss[i]);
			SlNostBean nostBean = new SlNostBean();
			nostBean.setNos(noss[i]);
			nostBean.setBr(ifsOpicsNost.getBr());
			nostBean.setCcy(ifsOpicsNost.getCcy());
			nostBean.setCost(ifsOpicsNost.getCost());
			nostBean.setCust(ifsOpicsNost.getCust());
			nostBean.setLstmntdte(ifsOpicsNost.getLstmntdte());

			try {
				slOutBean = iStaticServer.addNost(nostBean);
			} catch (RemoteConnectFailureException e) {
				slOutBean.setRetCode(SlErrors.FAILED_RMI_OPICS.getErrCode());
				slOutBean.setRetMsg(SlErrors.FAILED_RMI_OPICS.getErrMsg() + ":" + e.getMessage());
				slOutBean.setRetStatus(RetStatusEnum.F);
			} catch (Exception e) {
				slOutBean.setRetCode(SlErrors.FAILED.getErrCode());
				slOutBean.setRetMsg(SlErrors.FAILED.getErrMsg() + ":" + e.getMessage());
				slOutBean.setRetStatus(RetStatusEnum.F);
			}
			if ("S".equals(String.valueOf(slOutBean.getRetStatus()))) {
				ifsOpicsNost.setStatus("1");
				ifsOpicsNostService.updateStatus(ifsOpicsNost);
			}

		}

		return RetMsgHelper.ok(slOutBean);

	}

	/**
	 * 批量校准
	 */
	@ResponseBody
	@RequestMapping(value = "/batchCalibration")
	public RetMsg<SlOutBean> batchCalibration(@RequestBody Map<String, String> map) {
		String type = String.valueOf(map.get("type"));
		String nos;
		String[] noss;
		if ("1".equals(type)) {
			nos = map.get("nos");
			noss = nos.split(",");
		} else {
			noss = null;
		}
		SlOutBean slOutBean = ifsOpicsNostService.batchCalibration(type, noss);
		return RetMsgHelper.ok(slOutBean);

	}

}
