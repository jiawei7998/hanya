package com.singlee.ifs.controller;

import com.alibaba.fastjson.JSON;
import com.singlee.capital.base.mapper.TaDeskMenuMapper;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.session.SessionService;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.financial.opics.IBaseServer;
import com.singlee.financial.opics.ISecurServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * 用于检查消息发送消息类
 */
@Controller
@RequestMapping(value = "IfsMessageTopicController")
public class IfsMessageTopicController {
    @Autowired
    TaDeskMenuMapper deskMenuMapper;
    @Autowired
    SessionService sessionService;
    @Autowired
    IBaseServer baseServer;
    @Autowired
    ISecurServer securServer;

    //还本收息提醒
    @ResponseBody
    @RequestMapping(value = "/sendhbsxtx")
    public RetMsg<Serializable> sendhbsxtx(@RequestBody Map<String, Object> params) {
        //map中需要有角色Id和菜单Id
        String roleIdStr = ParameterUtil.getString(params, "roleIds", null);
        params.put("roleIds", JSON.parseArray(roleIdStr, String.class));
        String userId = ParameterUtil.getString(params, "userId", null);
        if (deskMenuMapper.getTaRoleByModuleId(params).size() > 0) {
            try {
                String postdate = DateUtil.format(baseServer.getOpicsSysDate("01"), "yyyy-MM-dd");
                Map<String, Object> paramMap = new HashMap<>();
                paramMap.put("br", "01");
                paramMap.put("startDate", postdate);
                paramMap.put("endDate",DateUtil.dateAdjust(postdate,7));
                sessionService.sendTopic(userId, "<a href='javascript:void(0);' onclick='tipshbsxtx();'>你未来7天有" + securServer.getRedemption(paramMap).getSize() + "笔【还本付息业务】<br>需要在opics中进行经办复核!<a>");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return RetMsgHelper.ok();
    }
}
