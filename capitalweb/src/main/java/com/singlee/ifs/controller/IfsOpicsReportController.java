package com.singlee.ifs.controller;

import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.financial.report.IReportServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * 报表处理Controller
 * 
 * ClassName: IfsOpicsReportController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason: TODO ADD REASON(可选). <br/>
 * date: 2018-5-30 下午07:48:07 <br/>
 * 
 * @author shenzl
 * @version
 * @since JDK 1.6
 */
@Controller
@RequestMapping(value = "/IfsOpicsReportController")
public class IfsOpicsReportController {
	@Autowired
	IReportServer reportServer;

	@ResponseBody
	@RequestMapping(value = "/getSqlfromFile")
	public RetMsg<Serializable> getSqlfromFile(@RequestBody Map<String, Object> map) throws RemoteConnectFailureException, Exception {
		return RetMsgHelper.ok(reportServer.getSqlfromFile(map.get("path").toString()));
	}

	@ResponseBody
	@RequestMapping(value = "/runSql")
	public List<LinkedHashMap<String, Object>> runSql(@RequestBody Map<String, Object> map) throws RemoteConnectFailureException, Exception {
		return reportServer.runSql(map.get("sql").toString());
	}

	@ResponseBody
	@RequestMapping(value = "/getUserTabComments")
	public List<String> getUserTabComments(@RequestBody Map<String, Object> map) throws RemoteConnectFailureException, Exception {
		return reportServer.getUserTabComments(map.get("tabId").toString());
	}

	@ResponseBody
	@RequestMapping(value = "/getUserTabColums")
	public List<String> getUserTabColums(@RequestBody Map<String, Object> map) throws RemoteConnectFailureException, Exception {
		return reportServer.getUserTabColums(map.get("tableId").toString());
	}
}
