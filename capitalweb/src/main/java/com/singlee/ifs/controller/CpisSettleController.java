package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.financial.pojo.TradeSettlsBean;
import com.singlee.ifs.service.TradeSettlsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * @author Luozb
 * @version 1.0.0
 * @ClassName CpisSptFwdSettleController.java
 * @Description 清算信息
 * @createTime 2021年11月27日 15:06:00
 */
public class CpisSettleController extends CommonController {

    @Autowired
    TradeSettlsService tradeSettlsService;
    /***
     * 分页查询-即期远期查询
     */
    @ResponseBody
    @RequestMapping(value = "/searchSptFwdSettle")
    public RetMsg<PageInfo<TradeSettlsBean>> searchSptFwdSettle(@RequestBody Map<String, Object> map) {
        Page<TradeSettlsBean> page = tradeSettlsService.searchSptFwdSettle(map);
        return RetMsgHelper.ok(page);
    }

    /***
     * 分页查询--掉期查询
     */
    @ResponseBody
    @RequestMapping(value = "/searchSptFwdSettle")
    public RetMsg<PageInfo<TradeSettlsBean>> searchSwapSettle(@RequestBody Map<String, Object> map) {
        Page<TradeSettlsBean> page = tradeSettlsService.searchSwapSettle(map);
        return RetMsgHelper.ok(page);
    }
}
