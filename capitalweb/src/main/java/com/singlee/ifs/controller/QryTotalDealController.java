package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.hrbcap.util.TradeConstants;
import com.singlee.ifs.model.*;
import com.singlee.ifs.service.QryTotalDealService;
import com.singlee.refund.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller
@RequestMapping(value = "/QryTotalDealController")
public class QryTotalDealController {

	@Autowired
	private QryTotalDealService qryTotalDealService;

	// 外汇即期
	@ResponseBody
	@RequestMapping(value = "/searchTotalDealRMBWBJQ")
	public RetMsg<PageInfo<IfsCfetsfxSpt>> searchTotalDealRMBWBJQ(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsfxSpt> page = qryTotalDealService.searchTotalDealRMBWBJQ(params, 3);
		return RetMsgHelper.ok(page);
	}

	// 外汇远期
	@ResponseBody
	@RequestMapping(value = "/searchTotalDealRMBWBYQ")
	public RetMsg<PageInfo<IfsCfetsfxFwd>> searchTotalDealRMBWBYQ(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsfxFwd> page = qryTotalDealService.searchTotalDealRMBWBYQ(params, 3);
		return RetMsgHelper.ok(page);
	}

	// 外汇掉期
	@ResponseBody
	@RequestMapping(value = "/searchTotalDealRMBWBDQ")
	public RetMsg<PageInfo<IfsCfetsfxSwap>> searchTotalDealRMBWBDQ(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsfxSwap> page = qryTotalDealService.searchTotalDealRMBWBDQ(params, 3);
		return RetMsgHelper.ok(page);
	}

	// 外币拆借
	@ResponseBody
	@RequestMapping(value = "/searchTotalDealWBCQ")
	public RetMsg<PageInfo<IfsCfetsfxLend>> searchTotalDealWBCQ(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsfxLend> page = qryTotalDealService.searchTotalDealWBCQ(params, 3);
		return RetMsgHelper.ok(page);
	}

	// 信用拆借
	@ResponseBody
	@RequestMapping(value = "/searchTotalDealXYCJ")
	public RetMsg<PageInfo<IfsCfetsrmbIbo>> searchTotalDealXYCJ(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		String[] prodtype = { "CC", "CR" };
		params.put("prodtype", prodtype);
		Page<IfsCfetsrmbIbo> page = qryTotalDealService.searchTotalDealXYCJ(params, 3);
		return RetMsgHelper.ok(page);
	}

	// 同业借款
	@ResponseBody
	@RequestMapping(value = "/searchTotalDealTYJK")
	public RetMsg<PageInfo<IfsCfetsrmbIbo>> searchTotalDealTYJK(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		String[] prodtype = { "CT" };
		params.put("prodtype", prodtype);
		Page<IfsCfetsrmbIbo> page = qryTotalDealService.searchTotalDealXYCJ(params, 3);
		return RetMsgHelper.ok(page);
	}

	// 同业存款
	@ResponseBody
	@RequestMapping(value = "/searchTotalDealTYCK")
	public RetMsg<PageInfo<IfsApprovermbDeposit>> searchTotalDealTYCK(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsApprovermbDeposit> page = qryTotalDealService.searchTotalDealTYCK(params, 3);
		return RetMsgHelper.ok(page);
	}

	// 现券买卖
	@ResponseBody
	@RequestMapping(value = "/searchTotalDealXJMM")
	public RetMsg<PageInfo<IfsCfetsrmbCbt>> searchTotalDealXJMM(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		String[] costcent = { "8300002001" };
		params.put("cost", costcent);
		params.put("costind", null);
		Page<IfsCfetsrmbCbt> page = qryTotalDealService.searchTotalDealXJMM(params, 3);
		return RetMsgHelper.ok(page);
	}

	// 债券远期
	@ResponseBody
	@RequestMapping(value = "/searchTotalDealZQYQ")
	public RetMsg<PageInfo<IfsCfetsrmbBondfwd>> searchTotalDealZQYQ(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsrmbBondfwd> page = qryTotalDealService.searchTotalDealZQYQ(params, 3);
		return RetMsgHelper.ok(page);
	}

	// 债券借贷
	@ResponseBody
	@RequestMapping(value = "/searchTotalDealZJJD")
	public RetMsg<PageInfo<IfsCfetsrmbSl>> searchTotalDealZJJD(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsrmbSl> page = qryTotalDealService.searchTotalDealZJJD(params, 3);
		return RetMsgHelper.ok(page);
	}

	// 债券发行
	@ResponseBody
	@RequestMapping(value = "/searchTotalDealZQFX")
	public RetMsg<PageInfo<IfsCfetsrmbDp>> searchTotalDealZQFX(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		params.put("instrumentType", "SE");
		Page<IfsCfetsrmbDp> page = qryTotalDealService.searchTotalDealZQFX(params, 3);
		return RetMsgHelper.ok(page);
	}

	// 存单发行
	@ResponseBody
	@RequestMapping(value = "/searchTotalDealCDFX")
	public RetMsg<PageInfo<IfsCfetsrmbDp>> searchTotalDealCDFX(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		params.put("instrumentType", "CD");
		Page<IfsCfetsrmbDp> page = qryTotalDealService.searchTotalDealZQFX(params, 3);
		return RetMsgHelper.ok(page);
	}

	// MBS
	@ResponseBody
	@RequestMapping(value = "/searchTotalDealMBS")
	public RetMsg<PageInfo<IfsCfetsrmbCbt>> searchTotalDealMBS(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		String[] costcent = { "8300002001" };
		params.put("cost", costcent);
		params.put("costind", "MBS");
		Page<IfsCfetsrmbCbt> page = qryTotalDealService.searchTotalDealXJMM(params, 3);
		return RetMsgHelper.ok(page);
	}

	// 质押式回购
	@ResponseBody
	@RequestMapping(value = "/searchTotalDealZYSHG")
	public RetMsg<PageInfo<IfsCfetsrmbCr>> searchTotalDealZYSHG(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		String[] prodtype = { "VP", "RP" };
		params.put("prodtype", prodtype);
		Page<IfsCfetsrmbCr> page = qryTotalDealService.searchTotalDealZYSHG(params, 3);
		return RetMsgHelper.ok(page);
	}

	// 买断式回购
	@ResponseBody
	@RequestMapping(value = "/searchTotalDealMDSHG")
	public RetMsg<PageInfo<IfsCfetsrmbOr>> searchTotalDealMDSHG(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsrmbOr> page = qryTotalDealService.searchTotalDealMDSHG(params, 3);
		return RetMsgHelper.ok(page);
	}

	// 国库定期存款
	@ResponseBody
	@RequestMapping(value = "/searchTotalDealGKDQCK")
	public RetMsg<PageInfo<IfsCfetsrmbCr>> searchTotalDealGKDQCK(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		String[] prodtype = { "GD" };
		params.put("prodtype", prodtype);
		Page<IfsCfetsrmbCr> page = qryTotalDealService.searchTotalDealZYSHG(params, 3);
		return RetMsgHelper.ok(page);
	}

	// 交易所回购
	@ResponseBody
	@RequestMapping(value = "/searchTotalDealJYSHG")
	public RetMsg<PageInfo<IfsCfetsrmbCr>> searchTotalDealJYSHG(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		String[] prodtype = { "JY" };
		params.put("prodtype", prodtype);
		Page<IfsCfetsrmbCr> page = qryTotalDealService.searchTotalDealZYSHG(params, 3);
		return RetMsgHelper.ok(page);
	}

	// 常备借贷便利
	@ResponseBody
	@RequestMapping(value = "/searchTotalDealCBJDBL")
	public RetMsg<PageInfo<IfsCfetsrmbCr>> searchTotalDealCBJDBL(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		String[] prodtype = { "CB" };
		params.put("prodtype", prodtype);
		Page<IfsCfetsrmbCr> page = qryTotalDealService.searchTotalDealZYSHG(params, 3);
		return RetMsgHelper.ok(page);
	}

	// 中期借贷便利
	@ResponseBody
	@RequestMapping(value = "/searchTotalDealZQJDBL")
	public RetMsg<PageInfo<IfsCfetsrmbCr>> searchTotalDealZQJDBL(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		String[] prodtype = { "ZQ" };
		params.put("prodtype", prodtype);
		Page<IfsCfetsrmbCr> page = qryTotalDealService.searchTotalDealZYSHG(params, 3);
		return RetMsgHelper.ok(page);
	}

	// 线下押券
	@ResponseBody
	@RequestMapping(value = "/searchTotalDealZXZDK")
	public RetMsg<PageInfo<IfsCfetsrmbCr>> searchTotalDealZXZDK(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		String[] prodtype = { "ZD" };
		params.put("prodtype", prodtype);
		Page<IfsCfetsrmbCr> page = qryTotalDealService.searchTotalDealZYSHG(params, 3);
		return RetMsgHelper.ok(page);
	}

	// 利率互换
	@ResponseBody
	@RequestMapping(value = "/searchTotalDealLLHH")
	public RetMsg<PageInfo<IfsCfetsrmbIrs>> searchTotalDealLLHH(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsrmbIrs> page = qryTotalDealService.searchTotalDealLLHH(params, 3);
		return RetMsgHelper.ok(page);
	}

	// 基金申购
	@ResponseBody
	@RequestMapping(value = "/searchTotalDealJJSG")
	public RetMsg<PageInfo<CFtAfp>> searchTotalDealJJSG(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		params.put("type", TradeConstants.TrdType.FUND_AFP);
		String id=params.get("id").toString().substring(0,3);
		params.put("id",id);
		Page<CFtAfp> page = qryTotalDealService.searchTotalDealJJSGANDFH(params, 1);
		return RetMsgHelper.ok(page);
	}

	// 基金申购确认
	@ResponseBody
	@RequestMapping(value = "/searchTotalDealJJSGQR")
	public RetMsg<PageInfo<CFtAfpConf>> searchTotalDealJJSGQR(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		params.put("type", TradeConstants.TrdType.FUND_AFPCONF);
		params.put("type", TradeConstants.TrdType.FUND_AFP);
		String id=params.get("id").toString().substring(0,3);
		params.put("id",id);
		Page<CFtAfpConf> page = qryTotalDealService.searchTotalDealJJSGQR(params, 3);
		return RetMsgHelper.ok(page);
	}

	// 基金赎回
	@ResponseBody
	@RequestMapping(value = "/searchTotalDealJJSH")
	public RetMsg<PageInfo<CFtRdp>> searchTotalDealJJSH(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		params.put("type", TradeConstants.TrdType.FUND_RDP);
		params.put("type", TradeConstants.TrdType.FUND_AFP);
		String id=params.get("id").toString().substring(0,3);
		params.put("id",id);
		Page<CFtRdp> page = qryTotalDealService.searchTotalDealJJSH(params, 3);
		return RetMsgHelper.ok(page);
	}

	// 基金赎回确认
	@ResponseBody
	@RequestMapping(value = "/searchTotalDealJJSHQR")
	public RetMsg<PageInfo<CFtRdpConf>> searchTotalDealJJSHQR(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		params.put("type", TradeConstants.TrdType.FUND_RDPCONF);
		params.put("type", TradeConstants.TrdType.FUND_AFP);
		String id=params.get("id").toString().substring(0,3);
		params.put("id",id);
		Page<CFtRdpConf> page = qryTotalDealService.searchTotalDealJJSHQR(params, 3);
		return RetMsgHelper.ok(page);
	}

	// 基金现金分红
	@ResponseBody
	@RequestMapping(value = "/searchTotalDealJJXJFH")
	public RetMsg<PageInfo<CFtRedd>> searchTotalDealJJXJFH(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		params.put("type", TradeConstants.TrdType.FUND_REDD);
		Page<CFtRedd> page = qryTotalDealService.searchTotalDealJJXJFH(params, 3);
		return RetMsgHelper.ok(page);
	}

	// 基金红利再投
	@ResponseBody
	@RequestMapping(value = "/searchTotalDealJJHLZT")
	public RetMsg<PageInfo<CFtRein>> searchTotalDealJJHLZT(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		params.put("type", TradeConstants.TrdType.FUND_REIN);
		Page<CFtRein> page = qryTotalDealService.searchTotalDealJJHLZT(params, 3);
		return RetMsgHelper.ok(page);
	}

	// 黄金即期
	@ResponseBody
	@RequestMapping(value = "/searchTotalDealHJJQ")
	public RetMsg<PageInfo<IfsCfetsmetalGold>> searchTotalDealHJJQ(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		params.put("type", "spt");
		Page<IfsCfetsmetalGold> page = qryTotalDealService.searchTotalDealHJ(params, 3);
		return RetMsgHelper.ok(page);
	}

	// 黄金远期
	@ResponseBody
	@RequestMapping(value = "/searchTotalDealHJYQ")
	public RetMsg<PageInfo<IfsCfetsmetalGold>> searchTotalDealHJYQ(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		params.put("type", "fwd");
		Page<IfsCfetsmetalGold> page = qryTotalDealService.searchTotalDealHJ(params, 3);
		return RetMsgHelper.ok(page);
	}

	// 黄金掉期
	@ResponseBody
	@RequestMapping(value = "/searchTotalDealHJDQ")
	public RetMsg<PageInfo<IfsCfetsmetalGold>> searchTotalDealHJDQ(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		params.put("type", "swap");
		Page<IfsCfetsmetalGold> page = qryTotalDealService.searchTotalDealHJ(params, 3);
		return RetMsgHelper.ok(page);
	}

	// 货币掉期
	@ResponseBody
	@RequestMapping(value = "/searchTotalDealHBDQ")
	public RetMsg<PageInfo<IfsCfetsfxCswap>> searchTotalDealHBDQ(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsfxCswap> page = qryTotalDealService.searchTotalDealHBDQ(params, 3);
		return RetMsgHelper.ok(page);
	}

	// 人民币期权
	@ResponseBody
	@RequestMapping(value = "/searchTotalDealRMBQQ")
	public RetMsg<PageInfo<IfsCfetsfxOption>> searchTotalDealRMBQQ(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsfxOption> page = qryTotalDealService.searchTotalDealRMBQQ(params, 3);
		return RetMsgHelper.ok(page);
	}

	// 外币头寸调拨
	@ResponseBody
	@RequestMapping(value = "/searchTotalDealWBTCTB")
	public RetMsg<PageInfo<IfsCfetsfxAllot>> searchTotalDealWBTCTB(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsfxAllot> page = qryTotalDealService.searchTotalDealWBTCTB(params, 3);
		return RetMsgHelper.ok(page);
	}

	// 外汇对即期
	@ResponseBody
	@RequestMapping(value = "/searchTotalDealWBDJQ")
	public RetMsg<PageInfo<IfsCfetsfxOnspot>> searchTotalDealWBDJQ(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsfxOnspot> page = qryTotalDealService.searchTotalDealWBDJQ(params, 3);
		return RetMsgHelper.ok(page);
	}

	//债券借贷事前审批
	@ResponseBody
	@RequestMapping(value = "/searchTotalDealZQJDSQ")
	public RetMsg<PageInfo<IfsApprovermbSl>> searchTotalDealZQJDSQ(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsApprovermbSl> page = qryTotalDealService.searchTotalDealZQJDSQ(params, 3);
		return RetMsgHelper.ok(page);
	}

}
