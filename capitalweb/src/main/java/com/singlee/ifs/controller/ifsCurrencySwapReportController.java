package com.singlee.ifs.controller;

import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.financial.bean.SlSwapCurBean;
import com.singlee.financial.opics.ISwapServer;
import com.singlee.financial.page.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller
@RequestMapping(value="/ifsCurSwapCount")
public class ifsCurrencySwapReportController {
	@Autowired
	ISwapServer swapServer;
	// 分页查询
	@ResponseBody
	@RequestMapping(value = "/searchCurSwapCount")
	public RetMsg<PageInfo<SlSwapCurBean>> searchCcySwapPage(
			@RequestBody Map<String, Object> param) throws RemoteConnectFailureException, Exception {
		PageInfo<SlSwapCurBean> page = swapServer.getSwapCrsExpose(param);
		return RetMsgHelper.ok(page);
	}
}
