package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.financial.bean.SlCostBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.opics.IStaticServer;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.SlErrors;
import com.singlee.ifs.model.IfsOpicsCost;
import com.singlee.ifs.service.IfsOpicsCostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * OPICS成本中心Controller
 * 
 * ClassName: IfsOpicsCostController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason: TODO ADD REASON(可选). <br/>
 * date: 2018-5-30 下午07:38:53 <br/>
 * 
 * @author zhengfl
 * @version
 * @since JDK 1.6
 */
@Controller
@RequestMapping(value = "/IfsOpicsCostController")
public class IfsOpicsCostController {
	@Autowired
	IfsOpicsCostService ifsOpicsCostService;
	@Autowired
	IStaticServer iStaticServer;

	/***
	 * 分页查询
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageCost")
	public RetMsg<PageInfo<IfsOpicsCost>> searchPageCost(@RequestBody Map<String, Object> map) {
		Page<IfsOpicsCost> page = ifsOpicsCostService.searchPageOpicsCost(map);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 新增
	 */
	@ResponseBody
	@RequestMapping(value = "/saveOpicsCostAdd")
	public RetMsg<Serializable> saveOpicsCostAdd(@RequestBody IfsOpicsCost entity) {
		ifsOpicsCostService.insert(entity);
		return RetMsgHelper.ok();
	}

	/**
	 * 修改
	 */
	@ResponseBody
	@RequestMapping(value = "/saveOpicsCostEdit")
	public RetMsg<Serializable> saveOpicsCostEdit(@RequestBody IfsOpicsCost entity) {
		ifsOpicsCostService.updateById(entity);
		return RetMsgHelper.ok();
	}

	/**
	 * 删除
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteCost")
	public RetMsg<Serializable> deleteProd(@RequestBody Map<String, String> map) {
		String costcent = map.get("costcent");
		ifsOpicsCostService.deleteById(costcent);
		return RetMsgHelper.ok();
	}

	@ResponseBody
	@RequestMapping(value = "/searchCost")
	public RetMsg<List<IfsOpicsCost>> searchCost(@RequestBody HashMap<String, Object> map) throws RException {
		List<IfsOpicsCost> list = ifsOpicsCostService.searchCost(map);
		return RetMsgHelper.ok(list);
	}

	/**
	 * 同步opics表
	 */
	@ResponseBody
	@RequestMapping(value = "/syncCost")
	public RetMsg<SlOutBean> syncCost(@RequestBody Map<String, String> map) {
		SlOutBean outBean = new SlOutBean();
		String costcent = map.get("costcent");
		String[] costcents = costcent.split(",");
		for (int i = 0; i < costcents.length; i++) {
			IfsOpicsCost ifsOpicsCost = ifsOpicsCostService.searchById(costcents[i]);
			SlCostBean costBean = new SlCostBean();
			costBean.setCostCent(costcents[i]);
			costBean.setBusunit(ifsOpicsCost.getBusunit());
			costBean.setCostDesc(ifsOpicsCost.getCostdesc());
			costBean.setLstmntdte(DateUtil.format(ifsOpicsCost.getLstmntdte(), "yyyy-MM-dd"));
			costBean.setOrgunit(ifsOpicsCost.getOrgunit());
			try {
				outBean = iStaticServer.addCost(costBean);
			} catch (RemoteConnectFailureException e) {
				outBean.setRetCode(SlErrors.FAILED_RMI_OPICS.getErrCode());
				outBean.setRetMsg(SlErrors.FAILED_RMI_OPICS.getErrMsg() + ":" + e.getMessage());
				outBean.setRetStatus(RetStatusEnum.F);
			} catch (Exception e) {
				outBean.setRetCode(SlErrors.FAILED.getErrCode());
				outBean.setRetMsg(SlErrors.FAILED.getErrMsg() + ":" + e.getMessage());
				outBean.setRetStatus(RetStatusEnum.F);
			}

			if ("S".equals(String.valueOf(outBean.getRetStatus()))) {
				ifsOpicsCost.setStatus("1");
				ifsOpicsCostService.updateStatus(ifsOpicsCost);
			}
		}
		return RetMsgHelper.ok(outBean);
	}

	/**
	 * 批量校准信息
	 */
	@ResponseBody
	@RequestMapping(value = "/batchCalibration")
	public RetMsg<SlOutBean> verify(@RequestBody Map<String, String> map) {
		SlOutBean slOutBean = new SlOutBean();
		String type = String.valueOf(map.get("type"));
		String costcent = "";
		String[] costcents;
		if ("1".equals(type)) { // 选中记录校准
			costcent = map.get("costcent");
			costcents = costcent.split(",");
		} else { // 全部校准
			costcents = null;
		}
		try {
			slOutBean = ifsOpicsCostService.batchCalibration(type, costcents);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return RetMsgHelper.ok(slOutBean);
	}

}
