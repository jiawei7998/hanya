package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.ifs.baseUtil.CfetsRmbService;
import com.singlee.ifs.model.IfsCfetsBeforeDeal;
import com.singlee.ifs.service.IFSService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.Map;

@Controller
@RequestMapping(value = "/IfsCfetsBeforeDealCrotroller")
public class IfsCfetsBeforeDealCrotroller {

	@Autowired
	private IFSService cfetsBeforeDeal;
	@Autowired
	private CfetsRmbService cfetsService;
	/**
	 * 成交单分页查询
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchCfetsBeforeDeal")
	public RetMsg<PageInfo<IfsCfetsBeforeDeal>> searchCfetsBeforeDeal(@RequestBody IfsCfetsBeforeDeal record) {
		Page<IfsCfetsBeforeDeal> page = cfetsBeforeDeal.searchCfetsBeforeDeal(record);
		return RetMsgHelper.ok(page);
	}
	/**
	 * 根据主键查询成交单(返回值增加 流程id)
	 * */
	@ResponseBody
	@RequestMapping(value = "/queryCfetsBeforeDealById")
	public RetMsg<IfsCfetsBeforeDeal> queryCfetsBeforeDealById(@RequestBody Map<String, Object> key) {
		IfsCfetsBeforeDeal dp = cfetsBeforeDeal.queryCfetsBeforeDealById(key);
		return RetMsgHelper.ok(dp);
	}
	
	/**
	 * 新增
	 */
	@ResponseBody
	@RequestMapping(value = "/AddBeforeDeal")
	public RetMsg<Serializable> AddBeforeDeal(@RequestBody Map<String, Object> map) {
		String action=map.get("action").toString();
		if("add".equals(action)){
			map.put("ticketId",cfetsService.getTicketId("SQJY"));
			map.put("status","0");//新增
			cfetsBeforeDeal.insertBeforeDeal(map);
		}else if("edit".equals(action)){
			map.put("status","0");//修改
			cfetsBeforeDeal.updateBeforeDeal(map);
		}else if("commit".equals(action)){
			map.put("status","1");//提交
			cfetsBeforeDeal.updateBeforeDealStatus(map);
		}else if("check".equals(action)){
			map.put("status","3");//复核
			cfetsBeforeDeal.updateBeforeDealStatus(map);
		}else if("confirm".equals(action)){
			map.put("status","3");//确认(无)
			cfetsBeforeDeal.updateBeforeDealStatus(map);
		}
		return RetMsgHelper.ok();
	}
	/**
	 * 删除
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteBeforeDeal")
	public RetMsg<Serializable> deleteBeforeDeal(@RequestBody Map<String, Object> map) {
		cfetsBeforeDeal.deleteBeforeDeal(map);
		return RetMsgHelper.ok();
	}
}
