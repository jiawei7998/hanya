package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.ifs.model.*;
import com.singlee.ifs.service.IFSApproveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.Map;

/**
 * 货币掉期事前审批交易Controller
 * 
 * ClassName: IfsApproveCurrencyController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason: TODO ADD REASON(可选). <br/>
 * date: 2018-5-30 下午07:05:07 <br/>
 * 
 * @author zhangcm
 * @version
 * @since JDK 1.6
 */
@Controller
@RequestMapping("IfsApproveCurrencyController")
public class IfsApproveCurrencyController {
	@Autowired
	private IFSApproveService iFSApproveService;

	// 分页查询
	@ResponseBody
	@RequestMapping(value = "/searchCcySwapPage")
	public RetMsg<PageInfo<IfsApprovefxCswap>> searchCcySwapPage(@RequestBody Map<String, Object> map) {
		Page<IfsApprovefxCswap> page = iFSApproveService.searchCcySwapPage(map);
		return RetMsgHelper.ok(page);
	}

	// 删除
	@ResponseBody
	@RequestMapping(value = "/deleteCcySwap")
	public RetMsg<Serializable> deleteCcySwap(@RequestBody Map<String, Object> map) {
		String ticketid = map.get("ticketid").toString();
		iFSApproveService.deleteCcySwap(ticketid);
		return RetMsgHelper.ok();
	}

	// 新增
	@ResponseBody
	@RequestMapping(value = "/addCcySwap")
	public RetMsg<Serializable> addCcySwap(@RequestBody IfsApprovefxCswap map) {
		iFSApproveService.addCcySwap(map);
		return RetMsgHelper.ok();
	}

	// 修改
	@ResponseBody
	@RequestMapping(value = "/editCcySwap")
	public RetMsg<Serializable> editCcySwap(@RequestBody IfsApprovefxCswap map) {
		iFSApproveService.editCcySwap(map);
		return RetMsgHelper.ok();
	}

	// 查询
	@ResponseBody
	@RequestMapping(value = "/searchCcySwap")
	public RetMsg<IfsApprovefxCswap> searchCcySwap(@RequestBody Map<String, Object> map) {
		String ticketid = map.get("ticketId").toString();
		IfsApprovefxCswap IfsApprovefxCswap = iFSApproveService.searchCcySwap(ticketid);
		return RetMsgHelper.ok(IfsApprovefxCswap);
	}

	/**
	 * 根据主键查询成交单(返回值增加 流程id)
	 * 
	 * @param
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchCcySwapAndFlowIdById")
	public RetMsg<IfsApprovefxCswap> searchCcySwapAndFlowIdById(@RequestBody Map<String, Object> key) {
		IfsApprovefxCswap ccySwap = iFSApproveService.getCcySwapAndFlowIdById(key);
		return RetMsgHelper.ok(ccySwap);
	}

	/**
	 * 货币掉期-查询我发起的
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-21
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageCcySwapMine")
	public RetMsg<PageInfo<IfsApprovefxCswap>> searchPageCcySwapMine(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsApprovefxCswap> page = iFSApproveService.getCcySwapMinePage(params, 3);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 货币掉期-查询 待审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageCcySwapUnfinished")
	public RetMsg<PageInfo<IfsApprovefxCswap>> searchPageCcySwapUnfinished(@RequestBody Map<String, Object> params) {
		String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse + ","
				+ DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
		String[] orderStatus = approveStatus.split(",");
		params.put("approveStatusNo", orderStatus);
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsApprovefxCswap> page = iFSApproveService.getCcySwapMinePage(params, 1);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 货币掉期-查询 已审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageCcySwapFinished")
	public RetMsg<PageInfo<IfsApprovefxCswap>> searchPageCcySwapFinished(@RequestBody Map<String, Object> params) {
		String approveStatusNo = ParameterUtil.getString(params, "approveStatus", "");
		if (StringUtil.isNullOrEmpty(approveStatusNo)) {
			approveStatusNo = DictConstants.ApproveStatus.ApprovedPass;
			approveStatusNo += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatusNo += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatusNo += "," + DictConstants.ApproveStatus.Verified;
			approveStatusNo += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			approveStatusNo += "," + DictConstants.ApproveStatus.Approving;
			String[] orderStatus = approveStatusNo.split(",");
			params.put("approveStatusNo", orderStatus);
		}
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsApprovefxCswap> page = iFSApproveService.getCcySwapMinePage(params, 2);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 勾兑时，查询表中审批通过的数据
	 * 
	 * @param
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchPageApprovefxCswapPassed")
	public RetMsg<PageInfo<IfsApprovefxCswap>> searchPageApprovefxCswapPassed(@RequestBody Map<String, Object> params) {
		Page<IfsApprovefxCswap> page = iFSApproveService.getApprovefxCswapPassedPage(params);
		return RetMsgHelper.ok(page);
	}

	/* 外币拆借 */

	// 分页查询
	@ResponseBody
	@RequestMapping(value = "/searchCcyLendingPage")
	public RetMsg<PageInfo<IfsApprovefxLend>> searchCcyLendingPage(@RequestBody Map<String, Object> map) {
		Page<IfsApprovefxLend> page = iFSApproveService.searchCcyLendingPage(map);
		return RetMsgHelper.ok(page);
	}

	// 删除
	@ResponseBody
	@RequestMapping(value = "/deleteCcyLending")
	public RetMsg<Serializable> deleteCcyLending(@RequestBody Map<String, Object> map) {
		String ticketid = map.get("ticketid").toString();
		iFSApproveService.deleteCcyLending(ticketid);
		return RetMsgHelper.ok();
	}

	// 新增
	@ResponseBody
	@RequestMapping(value = "/addCcyLending")
	public RetMsg<Serializable> addCcyLending(@RequestBody IfsApprovefxLend map) {
		iFSApproveService.addCcyLending(map);
		return RetMsgHelper.ok();
	}

	// 修改
	@ResponseBody
	@RequestMapping(value = "/editCcyLending")
	public RetMsg<Serializable> editCcyLending(@RequestBody IfsApprovefxLend map) {
		iFSApproveService.editCcyLending(map);
		return RetMsgHelper.ok();
	}

	// 查询
	@ResponseBody
	@RequestMapping(value = "/searchCcyLending")
	public RetMsg<IfsApprovefxLend> searchCcyLending(@RequestBody Map<String, Object> map) {
		String ticketid = map.get("ticketId").toString();
		IfsApprovefxLend IfsApprovefxLend = iFSApproveService.searchCcyLending(ticketid);
		return RetMsgHelper.ok(IfsApprovefxLend);
	}

	/**
	 * 外币拆借-查询我发起的
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-21
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageCcyLendingMine")
	public RetMsg<PageInfo<IfsApprovefxLend>> searchPageCcyLendingMine(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsApprovefxLend> page = iFSApproveService.getCcyLendingMinePage(params, 3);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 外币拆借-查询 待审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageCcyLendingUnfinished")
	public RetMsg<PageInfo<IfsApprovefxLend>> searchPageCcyLendingUnfinished(@RequestBody Map<String, Object> params) {
		String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse + ","
				+ DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
		String[] orderStatus = approveStatus.split(",");
		params.put("approveStatusNo", orderStatus);
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsApprovefxLend> page = iFSApproveService.getCcyLendingMinePage(params, 1);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 外币拆借-查询 已审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageCcyLendingFinished")
	public RetMsg<PageInfo<IfsApprovefxLend>> searchPageCcyLendingFinished(@RequestBody Map<String, Object> params) {
		String approveStatusNo = ParameterUtil.getString(params, "approveStatus", "");
		if (StringUtil.isNullOrEmpty(approveStatusNo)) {
			approveStatusNo = DictConstants.ApproveStatus.ApprovedPass;
			approveStatusNo += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatusNo += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatusNo += "," + DictConstants.ApproveStatus.Verified;
			approveStatusNo += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			approveStatusNo += "," + DictConstants.ApproveStatus.Approving;
			String[] orderStatus = approveStatusNo.split(",");
			params.put("approveStatusNo", orderStatus);
		}
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsApprovefxLend> page = iFSApproveService.getCcyLendingMinePage(params, 2);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 勾兑时，查询表中审批通过的数据
	 * 
	 * @param
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchPageApprovefxLendPassed")
	public RetMsg<PageInfo<IfsApprovefxLend>> searchPageApprovefxLendPassed(@RequestBody Map<String, Object> params) {
		Page<IfsApprovefxLend> page = iFSApproveService.getApprovefxLendPassedPage(params);
		return RetMsgHelper.ok(page);
	}

	/* 外币头寸调拨 */

	// 分页查询
	@ResponseBody
	@RequestMapping(value = "/searchIfsAllocatePage")
	public RetMsg<PageInfo<IfsApprovefxAllot>> searchIfsAllocatePage(@RequestBody Map<String, Object> map) {
		Page<IfsApprovefxAllot> page = iFSApproveService.searchIfsAllocatePage(map);
		return RetMsgHelper.ok(page);
	}

	// 删除
	@ResponseBody
	@RequestMapping(value = "/deleteIfsAllocate")
	public RetMsg<Serializable> deleteIfsAllocate(@RequestBody Map<String, Object> map) {
		String ticketid = map.get("ticketid").toString();
		iFSApproveService.deleteIfsAllocate(ticketid);
		return RetMsgHelper.ok();
	}

	// 新增
	@ResponseBody
	@RequestMapping(value = "/addIfsAllocate")
	public RetMsg<Serializable> addIfsAllocate(@RequestBody IfsApprovefxAllot map) {
		iFSApproveService.addIfsAllocate(map);
		return RetMsgHelper.ok();
	}

	// 修改
	@ResponseBody
	@RequestMapping(value = "/editIfsAllocate")
	public RetMsg<Serializable> editIfsAllocate(@RequestBody IfsApprovefxAllot map) {
		iFSApproveService.editIfsAllocate(map);
		return RetMsgHelper.ok();
	}

	// 查询
	@ResponseBody
	@RequestMapping(value = "/searchIfsAllocate")
	public RetMsg<IfsApprovefxAllot> searchIfsAllocate(@RequestBody Map<String, Object> map) {
		String ticketid = map.get("ticketId").toString();
		IfsApprovefxAllot IfsApprovefxAllot = iFSApproveService.searchIfsAllocate(ticketid);
		return RetMsgHelper.ok(IfsApprovefxAllot);
	}

	/**
	 * 外币头寸调拨-查询我发起的
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-21
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageAllocateMine")
	public RetMsg<PageInfo<IfsApprovefxAllot>> searchPageAllocateMine(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsApprovefxAllot> page = iFSApproveService.getAllocateMinePage(params, 3);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 外币头寸调拨-查询 待审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageAllocateUnfinished")
	public RetMsg<PageInfo<IfsApprovefxAllot>> searchPageAllocateUnfinished(@RequestBody Map<String, Object> params) {
		String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse + ","
				+ DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
		String[] orderStatus = approveStatus.split(",");
		params.put("approveStatusNo", orderStatus);
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsApprovefxAllot> page = iFSApproveService.getAllocateMinePage(params, 1);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 外币头寸调拨-查询 已审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageAllocateFinished")
	public RetMsg<PageInfo<IfsApprovefxAllot>> searchPageAllocateFinished(@RequestBody Map<String, Object> params) {
		String approveStatusNo = ParameterUtil.getString(params, "approveStatus", "");
		if (StringUtil.isNullOrEmpty(approveStatusNo)) {
			approveStatusNo = DictConstants.ApproveStatus.ApprovedPass;
			approveStatusNo += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatusNo += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatusNo += "," + DictConstants.ApproveStatus.Verified;
			approveStatusNo += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			approveStatusNo += "," + DictConstants.ApproveStatus.Approving;
			String[] orderStatus = approveStatusNo.split(",");
			params.put("approveStatusNo", orderStatus);
		}
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsApprovefxAllot> page = iFSApproveService.getAllocateMinePage(params, 2);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 勾兑时，查询表中审批通过的数据
	 * 
	 * @param
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchPageApprovefxAllotPassed")
	public RetMsg<PageInfo<IfsApprovefxAllot>> searchPageApprovefxAllotPassed(@RequestBody Map<String, Object> params) {
		Page<IfsApprovefxAllot> page = iFSApproveService.getApprovefxAllotPassedPage(params);
		return RetMsgHelper.ok(page);
	}

	/* 外汇对即期 */

	// 分页查询
	@ResponseBody
	@RequestMapping(value = "/searchCcyOnSpotPage")
	public RetMsg<PageInfo<IfsApprovefxOnspot>> searchCcyOnSpotPage(@RequestBody Map<String, Object> map) {
		Page<IfsApprovefxOnspot> page = iFSApproveService.searchCcyOnSpotPage(map);
		return RetMsgHelper.ok(page);
	}

	// 删除
	@ResponseBody
	@RequestMapping(value = "/deleteCcyOnSpot")
	public RetMsg<Serializable> deleteCcyOnSpot(@RequestBody Map<String, Object> map) {
		String ticketid = map.get("ticketid").toString();
		iFSApproveService.deleteCcyOnSpot(ticketid);
		return RetMsgHelper.ok();
	}

	// 新增
	@ResponseBody
	@RequestMapping(value = "/addCcyOnSpot")
	public RetMsg<Serializable> addCcyOnSpot(@RequestBody IfsApprovefxOnspot map) {
		iFSApproveService.addCcyOnSpot(map);
		return RetMsgHelper.ok();
	}

	// 修改
	@ResponseBody
	@RequestMapping(value = "/editCcyOnSpot")
	public RetMsg<Serializable> editCcyOnSpot(@RequestBody IfsApprovefxOnspot map) {
		iFSApproveService.editCcyOnSpot(map);
		return RetMsgHelper.ok();
	}

	// 查询
	@ResponseBody
	@RequestMapping(value = "/searchCcyOnSpot")
	public RetMsg<IfsApprovefxOnspot> searchCcyOnSpot(@RequestBody Map<String, Object> map) {
		String ticketid = map.get("ticketId").toString();
		IfsApprovefxOnspot IfsApprovefxOnspot = iFSApproveService.searchCcyOnSpot(ticketid);
		return RetMsgHelper.ok(IfsApprovefxOnspot);
	}

	/**
	 * 外汇对即期-查询我发起的
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-21
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageCcyOnSpotMine")
	public RetMsg<PageInfo<IfsApprovefxOnspot>> searchPageCcyOnSpotMine(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsApprovefxOnspot> page = iFSApproveService.getCcyOnSpotMinePage(params, 3);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 外汇对即期-查询 待审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageCcyOnSpotUnfinished")
	public RetMsg<PageInfo<IfsApprovefxOnspot>> searchPageCcyOnSpotUnfinished(@RequestBody Map<String, Object> params) {
		String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse + ","
				+ DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
		String[] orderStatus = approveStatus.split(",");
		params.put("approveStatusNo", orderStatus);
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsApprovefxOnspot> page = iFSApproveService.getCcyOnSpotMinePage(params, 1);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 外汇对即期-查询 已审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageCcyOnSpotFinished")
	public RetMsg<PageInfo<IfsApprovefxOnspot>> searchPageCcyOnSpotFinished(@RequestBody Map<String, Object> params) {
		String approveStatusNo = ParameterUtil.getString(params, "approveStatus", "");
		if (StringUtil.isNullOrEmpty(approveStatusNo)) {
			approveStatusNo = DictConstants.ApproveStatus.ApprovedPass;
			approveStatusNo += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatusNo += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatusNo += "," + DictConstants.ApproveStatus.Verified;
			approveStatusNo += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			approveStatusNo += "," + DictConstants.ApproveStatus.Approving;
			String[] orderStatus = approveStatusNo.split(",");
			params.put("approveStatusNo", orderStatus);
		}
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsApprovefxOnspot> page = iFSApproveService.getCcyOnSpotMinePage(params, 2);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 勾兑时，查询表中审批通过的数据
	 * 
	 * @param
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchPageApproveOnspotPassed")
	public RetMsg<PageInfo<IfsApprovefxOnspot>> searchPageApproveOnspotPassed(@RequestBody Map<String, Object> params) {
		Page<IfsApprovefxOnspot> page = iFSApproveService.getApproveOnspotPassedPage(params);
		return RetMsgHelper.ok(page);
	}

	/* 外币债 */

	// 分页查询
	@ResponseBody
	@RequestMapping(value = "/searchIfsDebtPage")
	public RetMsg<PageInfo<IfsApprovefxDebt>> searchIfsDebtPage(@RequestBody Map<String, Object> map) {
		Page<IfsApprovefxDebt> page = iFSApproveService.searchIfsDebtPage(map);
		return RetMsgHelper.ok(page);
	}

	// 删除
	@ResponseBody
	@RequestMapping(value = "/deleteIfsDebt")
	public RetMsg<Serializable> deleteIfsDebt(@RequestBody Map<String, Object> map) {
		String ticketid = map.get("ticketid").toString();
		iFSApproveService.deleteIfsDebt(ticketid);
		return RetMsgHelper.ok();
	}

	// 新增
	@ResponseBody
	@RequestMapping(value = "/addIfsDebt")
	public RetMsg<Serializable> addIfsDebt(@RequestBody IfsApprovefxDebt map) {
		iFSApproveService.addIfsDebt(map);
		return RetMsgHelper.ok();
	}

	// 修改
	@ResponseBody
	@RequestMapping(value = "/editIfsDebt")
	public RetMsg<Serializable> editIfsDebt(@RequestBody IfsApprovefxDebt map) {
		iFSApproveService.editIfsDebt(map);
		return RetMsgHelper.ok();
	}

	// 查询
	@ResponseBody
	@RequestMapping(value = "/searchIfsDebt")
	public RetMsg<IfsApprovefxDebt> searchIfsDebt(@RequestBody Map<String, Object> map) {
		String ticketid = map.get("ticketId").toString();
		IfsApprovefxDebt IfsApprovefxDebt = iFSApproveService.searchIfsDebt(ticketid);
		return RetMsgHelper.ok(IfsApprovefxDebt);
	}

	/**
	 * 外币债-查询我发起的
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-21
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageIfsDebtMine")
	public RetMsg<PageInfo<IfsApprovefxDebt>> searchPageIfsDebtMine(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsApprovefxDebt> page = iFSApproveService.getDebtMinePage(params, 3);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 外币债-查询 待审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageIfsDebtUnfinished")
	public RetMsg<PageInfo<IfsApprovefxDebt>> searchPageIfsDebtUnfinished(@RequestBody Map<String, Object> params) {
		String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse + ","
				+ DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
		String[] orderStatus = approveStatus.split(",");
		params.put("approveStatusNo", orderStatus);
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsApprovefxDebt> page = iFSApproveService.getDebtMinePage(params, 1);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 外币债-查询 已审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageIfsDebtFinished")
	public RetMsg<PageInfo<IfsApprovefxDebt>> searchPageIfsDebtFinished(@RequestBody Map<String, Object> params) {
		String approveStatusNo = ParameterUtil.getString(params, "approveStatus", "");
		if (StringUtil.isNullOrEmpty(approveStatusNo)) {
			approveStatusNo = DictConstants.ApproveStatus.ApprovedPass;
			approveStatusNo += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatusNo += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatusNo += "," + DictConstants.ApproveStatus.Verified;
			approveStatusNo += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			approveStatusNo += "," + DictConstants.ApproveStatus.Approving;
			String[] orderStatus = approveStatusNo.split(",");
			params.put("approveStatusNo", orderStatus);
		}
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsApprovefxDebt> page = iFSApproveService.getDebtMinePage(params, 2);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 根据主键关联流程查询成交单
	 * 
	 * @param Key
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchCurrencyAndFlowById")
	public RetMsg<IfsApprovefxDebt> searchCurrencyAndFlowById(@RequestBody Map<String, Object> key) {
		IfsApprovefxDebt gold = iFSApproveService.getDebtAndFlowById(key);
		return RetMsgHelper.ok(gold);
	}

	/**
	 * 勾兑时，查询表中审批通过的数据
	 * 
	 * @param
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchPageApprovefxDebtPassed")
	public RetMsg<PageInfo<IfsApprovefxDebt>> searchPageApprovefxDebtPassed(@RequestBody Map<String, Object> params) {
		Page<IfsApprovefxDebt> page = iFSApproveService.getApprovefxDebtPassedPage(params);
		return RetMsgHelper.ok(page);
	}
}
