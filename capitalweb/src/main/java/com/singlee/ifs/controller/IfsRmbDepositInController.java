package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.ifs.model.IfsRmbDepositin;
import com.singlee.ifs.service.IFSService;
import com.singlee.ifs.service.IfsRmbDepositInService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.Map;

/**
 * @Author zhangkai
 * @Description
 * @Date
 */
@Controller
@RequestMapping(value="/IfsRmbDepositInController")
public class IfsRmbDepositInController {

    @Autowired
    IFSService iFSService;
    @Autowired
    IfsRmbDepositInService ifsRmbDepositInService;

    /**
     * 同业存放(外币)-查询我发起的
     *
     * @param params
     *            - 请求参数
     * @return
     * @author
     * @date 2018-3-21
     */
    @ResponseBody
    @RequestMapping(value = "/searchPageCcyDepositInMine")
    public RetMsg<PageInfo<IfsRmbDepositin>> searchPageCcyDepositInMine(@RequestBody Map<String, Object> params) {
        params.put("isActive", DictConstants.YesNo.YES);
        params.put("userId", SlSessionHelper.getUserId());
        Page<IfsRmbDepositin> page = iFSService.getDepositInRmbMinePage(params, 3);
        return RetMsgHelper.ok(page);
    }

    /**
     * 同业存放(外币)-查询 待审批
     *
     * @param params
     *            - 请求参数
     * @return
     * @author
     * @date 2018-3-23
     */
    @ResponseBody
    @RequestMapping(value = "/searchPageCcyDepositInUnfinished")
    public RetMsg<PageInfo<IfsRmbDepositin>> searchPageCcyDepositInUnfinished(@RequestBody Map<String, Object> params) {
        String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse + ","
                + DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
        String[] orderStatus = approveStatus.split(",");
        params.put("approveStatusNo", orderStatus);
        params.put("isActive", DictConstants.YesNo.YES);
        params.put("userId", SlSessionHelper.getUserId());
        Page<IfsRmbDepositin> page = iFSService.getDepositInRmbMinePage(params, 1);
        return RetMsgHelper.ok(page);
    }

    /**
     * 同业存放(外币)-查询 已审批
     *
     * @param params
     *            - 请求参数
     * @return
     * @author
     * @date 2018-3-23
     */
    @ResponseBody
    @RequestMapping(value = "/searchPageCcyDepositInFinished")
    public RetMsg<PageInfo<IfsRmbDepositin>> searchPageCcyDepositInFinished(@RequestBody Map<String, Object> params) {
        String approveStatusNo = ParameterUtil.getString(params, "approveStatus", "");
        if (StringUtil.isNullOrEmpty(approveStatusNo)) {
            approveStatusNo = DictConstants.ApproveStatus.ApprovedPass;
            approveStatusNo += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
            approveStatusNo += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
            approveStatusNo += "," + DictConstants.ApproveStatus.Verified;
            approveStatusNo += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
            approveStatusNo += "," + DictConstants.ApproveStatus.Approving;
            String[] orderStatus = approveStatusNo.split(",");
            params.put("approveStatusNo", orderStatus);
        }
        params.put("isActive", DictConstants.YesNo.YES);
        params.put("userId", SlSessionHelper.getUserId());
        Page<IfsRmbDepositin> page = iFSService.getDepositInRmbMinePage(params, 2);
        return RetMsgHelper.ok(page);
    }

    // 查询一条
    @ResponseBody
    @RequestMapping(value = "/searchCcyDepositIn")
    public RetMsg<PageInfo<IfsRmbDepositin>> searchCcyDepositIn(@RequestBody Map<String, Object> params) {
        Page<IfsRmbDepositin> page = iFSService.searchRmbDepositIn(params);
        return RetMsgHelper.ok(page);
    }

    // 根据ticketid查询
    @ResponseBody
    @RequestMapping(value = "/searchByTicketId")
    public RetMsg<IfsRmbDepositin> searchByTicketId(@RequestBody Map<String, Object> params) {
        IfsRmbDepositin ifsRmbDepositin = iFSService.searchDepositInRmbByTicketId(params);
        return RetMsgHelper.ok(ifsRmbDepositin);
    }

    // 根据ticketid查询
    @ResponseBody
    @RequestMapping(value = "/searchFlowByTicketId")
    public RetMsg<IfsRmbDepositin> searchFlowByTicketId(@RequestBody Map<String, Object> params) {
        IfsRmbDepositin ifsRmbDepositin = iFSService.searchFlowDepositInRmbByTicketId(params);
        return RetMsgHelper.ok(ifsRmbDepositin);
    }


    // 新增
    @ResponseBody
    @RequestMapping(value = "/addRmbDepositIn")
    public RetMsg<Serializable> addRmbDepositIn(@RequestBody IfsRmbDepositin record) {
        String message = iFSService.addRmbDepositIn(record);
        return StringUtil.containsIgnoreCase(message, "保存成功")?RetMsgHelper.ok(message):RetMsgHelper.fail(message);
    }

    // 修改
    @ResponseBody
    @RequestMapping(value = "/editRmbDepositIn")
    public RetMsg<Serializable> editRmbDepositIn(@RequestBody IfsRmbDepositin record) {
        iFSService.editRmbDepositIn(record);
        return RetMsgHelper.ok();
    }

    //删除
    @ResponseBody
    @RequestMapping(value = "/deleteRmbDepositIn")
    public RetMsg<Serializable> deleteRmbDepositIn(@RequestBody Map<String, Object> map) {
        String ticketid = map.get("ticketid").toString();
        iFSService.deleteRmbDepositIn(ticketid);
        return RetMsgHelper.ok();
    }

    //提前还款分页查询
    @ResponseBody
    @RequestMapping(value = "/selectAdjustDepositPage")
    public RetMsg<PageInfo<IfsRmbDepositin>> selectAdjustDepositPage(@RequestBody Map<String, Object> map){
        Page<IfsRmbDepositin> ifsRmbDepositins = ifsRmbDepositInService.selectAdjustDepositPage(map);
        return RetMsgHelper.ok(ifsRmbDepositins);
    }

    //更新偿还状态
    @ResponseBody
    @RequestMapping(value = "/updateDepositAdjust")
    public void updateDepositAdjust(){
        ifsRmbDepositInService.updateDepositAdjust();
    }

}