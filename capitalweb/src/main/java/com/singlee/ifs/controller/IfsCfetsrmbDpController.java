package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.ifs.model.IfsCfetsrmbDp;
import com.singlee.ifs.model.IfsCfetsrmbDpKey;
import com.singlee.ifs.model.IfsCfetsrmbDpOffer;
import com.singlee.ifs.service.IFSService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.io.Serializable;
import java.util.Map;

/**
 * 存单发行正式交易审批Controller
 * 
 * ClassName: IfsCfetsrmbSlController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason: TODO ADD REASON(可选). <br/>
 * date: 2018-5-30 下午07:28:45 <br/>
 * 
 * @author xuqq
 * @version
 * @since JDK 1.6
 */
@Controller
@RequestMapping(value = "/IfsCfetsrmbDpController")
public class IfsCfetsrmbDpController extends CommonController{

	@Autowired
	private IFSService cfetsrmbDpService;
	
	/**
	 * 保存成交单
	 *
	 * @param record
	 *
	 * */
	@ResponseBody
	@RequestMapping(value = "/saveDp")
	public RetMsg<Serializable> saveDp(@RequestBody IfsCfetsrmbDp record) throws IOException {
		IfsCfetsrmbDp dp = cfetsrmbDpService.getDpById(record);
		String message = "";
		if (dp == null) {
			message = cfetsrmbDpService.insertDp(record);
		} else {
			try {
				cfetsrmbDpService.updateDp(record);
				message = "修改成功";
			} catch (Exception e) {
				e.printStackTrace();
				message = "修改失败";
			}
		}
		return StringUtil.containsIgnoreCase(message, "成功")?RetMsgHelper.ok(message):RetMsgHelper.fail(message);
	}
	/**
	 * 查询存单认购详情信息
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-7-18
	 */
	@ResponseBody
	@RequestMapping(value = "/searchOfferDpDetails")
	public RetMsg<PageInfo<IfsCfetsrmbDpOffer>> searchOfferDpDetails(@RequestBody Map<String, Object> params) {
		Page<IfsCfetsrmbDpOffer> page = cfetsrmbDpService.searchOfferDpDetails(params);
		return RetMsgHelper.ok(page);
	}
	/**
	 * 删除成交单
	 * 
	 * @param IfsCfetsrmbSlKey
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/deleteCfetsrmbDp")
	public RetMsg<Serializable> deleteCfetsrmbDp(@RequestBody IfsCfetsrmbDpKey key) {
		cfetsrmbDpService.deleteDp(key);
		return RetMsgHelper.ok();
	}
	/**
	 * 存单发行-查询我发起的
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-21
	 */
	@ResponseBody
	@RequestMapping(value = "/searchRmbDpSwapMine")
	public RetMsg<PageInfo<IfsCfetsrmbDp>> searchRmbDpSwapMine(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsrmbDp> page = cfetsrmbDpService.getRmbDpMinePage(params, 3);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 存单发行-查询 待审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageRmbDpUnfinished")
	public RetMsg<PageInfo<IfsCfetsrmbDp>> searchPageRmbDpUnfinished(@RequestBody Map<String, Object> params) {
		String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse + ","
				+ DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
		String[] orderStatus = approveStatus.split(",");
		params.put("approveStatusNo", orderStatus);
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsrmbDp> page = cfetsrmbDpService.getRmbDpMinePage(params, 1);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 存单发行-查询 已审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageRmbDpFinished")
	public RetMsg<PageInfo<IfsCfetsrmbDp>> searchPageRmbDpFinished(@RequestBody Map<String, Object> params) {
		String approveStatusNo = ParameterUtil.getString(params, "approveStatus", "");
		if (StringUtil.isNullOrEmpty(approveStatusNo)) {
			approveStatusNo = DictConstants.ApproveStatus.ApprovedPass;
			approveStatusNo += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatusNo += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatusNo += "," + DictConstants.ApproveStatus.Verified;
			approveStatusNo += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			// 20180502修改bug:已审批列表中增加"审批中"
			approveStatusNo += "," + DictConstants.ApproveStatus.Approving;
			String[] orderStatus = approveStatusNo.split(",");
			params.put("approveStatusNo", orderStatus);
		}
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsrmbDp> page = cfetsrmbDpService.getRmbDpMinePage(params, 2);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 根据主键查询成交单(返回值增加 流程id)
	 * 
	 * @param
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchCfetsrmbDpAndFlowIdById")
	public RetMsg<IfsCfetsrmbDp> searchCfetsrmbDpAndFlowIdById(@RequestBody Map<String, Object> key) {
		IfsCfetsrmbDp dp = cfetsrmbDpService.getDpAndFlowIdById(key);
		return RetMsgHelper.ok(dp);
	}
	
	//根据ticketId查询
	@ResponseBody
	@RequestMapping(value = "/searchCfetsrmbDpByTicketId")
	public RetMsg<IfsCfetsrmbDp> searchCfetsrmbDpByTicketId(@RequestBody Map<String, Object> map) {
		IfsCfetsrmbDp dp = cfetsrmbDpService.searchCfetsrmbDpByTicketId(map);
		return RetMsgHelper.ok(dp);
	}
}
