package com.singlee.ifs.controller;


import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.ifs.model.IfsCreditRiskreview;
import com.singlee.ifs.service.IfsOpicsRiskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * 单笔风险审查Controller
 * @version
 * @since JDK 1.6
 */
@Controller
@RequestMapping(value = "/IfsOpicsRiskController")
public class IfsOpicsRiskController {

	@Autowired
	IfsOpicsRiskService ifsOpicsRiskService;

	/***
	 * 分页查询 审查单号
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageOpicsRisk")
	public RetMsg<PageInfo<IfsCreditRiskreview>> searchPageOpicsRisk(@RequestBody Map<String, Object> map) {
		Page<IfsCreditRiskreview> page = ifsOpicsRiskService.searchPageOpicsRisk(map);
		return RetMsgHelper.ok(page);
	}
}