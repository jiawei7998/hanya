package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.ifs.model.IfsBlendSuccess;
import com.singlee.ifs.service.IfsBlendSuccessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.Map;

/**
 * 勾兑处理Controller
 * 
 * ClassName: IfsBlendSuccessController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason: TODO ADD REASON(可选). <br/>
 * date: 2018-5-30 下午07:14:31 <br/>
 * 
 * @author zhangcm
 * @version
 * @since JDK 1.6
 */
@Controller
@RequestMapping(value = "IfsBlendSuccessController")
public class IfsBlendSuccessController {

	@Autowired
	IfsBlendSuccessService ifsBlendSuccessService;

	@ResponseBody
	@RequestMapping(value = "/searchPageSuccess")
	public RetMsg<PageInfo<IfsBlendSuccess>> searchPageSuccess(@RequestBody Map<String, Object> map) {
		Page<IfsBlendSuccess> page = ifsBlendSuccessService.searchPageSuccess(map);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 新增
	 */
	@RequestMapping(value = "/blendSuccessAdd")
	@ResponseBody
	public RetMsg<Serializable> blendSuccessAdd(@RequestBody IfsBlendSuccess entity) throws RException {
		ifsBlendSuccessService.insert(entity);

		return RetMsgHelper.ok();
	}

	/***
	 * 删除（撤销）
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteData")
	public RetMsg<Serializable> deleteData(@RequestBody Map<String, String> map) {
		String id = map.get("id");
		ifsBlendSuccessService.deleteById(id);
		return RetMsgHelper.ok();
	}
}
