/**
 * 
 * Project Name:capitalweb File Name:IfsOpicsSettleManageController.java Package Name:com.singlee.ifs.controller
 * Date:2018-9-7下午03:57:34 Copyright (c) 2018, chenzhou1025@126.com All Rights Reserved.
 *
 */

package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.github.pagehelper.StringUtil;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.financial.bean.*;
import com.singlee.financial.opics.IBaseServer;
import com.singlee.financial.opics.ICspiCsriSdvpServer;
import com.singlee.financial.opics.ISettleManagerServer;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.SlErrors;
import com.singlee.ifs.mapper.*;
import com.singlee.ifs.model.*;
import com.singlee.ifs.service.IfsOpicsSettleManageService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.*;

/**
 * ClassName:IfsOpicsSettleManageController <br/>
 * Date: 2018-9-7 下午03:57:34 <br/>
 * 
 * @author zhengfl
 * @version
 * @since JDK 1.6
 * @see
 */
@Controller
@RequestMapping(value = "/IfsOpicsSettleManageController")
public class IfsOpicsSettleManageController extends CommonController {

    @Autowired
    private IfsOpicsSettleManageService opicsSettleManageService;

    @Autowired
    ISettleManagerServer settleManagerServer;

    @Autowired
    ICspiCsriSdvpServer iCspiCsriSdvpServer;

    @Autowired
    IBaseServer baseServer;// 查询opics基础类

    @Autowired
    private IfsOpicsSettleManageMapper ifsOpicsSettleManageMapper;
    @Autowired
    private IfsOpicsSettleManageService ifsOpicsSettleManageService;

    // 人民币即期
    @Autowired
    IfsCfetsfxSptMapper ifsCfetsfxSptMapper;
    // 人民币远期
    @Autowired
    IfsCfetsfxFwdMapper ifsCfetsfxFwdMapper;
    // 人命币外币掉期
    @Autowired
    IfsCfetsfxSwapMapper ifsCfetsfxSwapMapper;
    // 人民币期权
    @Autowired
    IfsCfetsfxOptionMapper ifsCfetsfxOptionMapper;

    // 外币拆借
    @Autowired
    IfsCfetsfxLendMapper ifsCfetsfxLendMapper;

    @ResponseBody
    @RequestMapping(value = "/saveCspiAndCsriAdd")
    public RetMsg<Serializable> saveCspiAndCsriAdd(@RequestBody IfsCspiCsriHead record) {
        // 判断记录是否已存在
        int num = opicsSettleManageService.getCspiCsriById(record);
        if (num > 0) {
            return RetMsgHelper.simple("000010", "该交易对手已添加此业务默认清算路径，如有变化请找到该笔记录并修改！");
        } else {
            opicsSettleManageService.saveCspiAndCsri(record);
            return RetMsgHelper.ok();
        }
    }

    @ResponseBody
    @RequestMapping(value = "/saveCspiAndCsriEdit")
    public RetMsg<Serializable> saveCspiAndCsriEdit(@RequestBody IfsCspiCsriHead record) {
        opicsSettleManageService.updateCspiAndCsri(record);
        return RetMsgHelper.ok();
    }

    /***
     * 分页查询
     */
    @ResponseBody
    @RequestMapping(value = "/searchPageCspiAndCsri")
    public RetMsg<PageInfo<IfsCspiCsriHead>> searchPageCspiAndCsri(@RequestBody Map<String, Object> map) {
        Page<IfsCspiCsriHead> page = opicsSettleManageService.searchPageCspiAndCsri(map);
        return RetMsgHelper.ok(page);
    }

    /***
     * 明细列表查询
     */
    @ResponseBody
    @RequestMapping(value = "/searchCspiCsriDetails")
    public RetMsg<List<IfsCspiCsriDetail>> searchCspiCsriDetails(@RequestBody Map<String, Object> map) {
        List<IfsCspiCsriDetail> page = opicsSettleManageService.searchCspiCsriDetails(map);
        return RetMsgHelper.ok(page);
    }

    /***
     * 明细列表查询
     */
    @ResponseBody
    @RequestMapping(value = "/searchPageCspiAndCsriDetails")
    public RetMsg<PageInfo<IfsCspiCsriDetail>> searchPageCspiAndCsriDetails(@RequestBody Map<String, Object> map) {
        Page<IfsCspiCsriDetail> page = opicsSettleManageService.searchPageCspiAndCsriDetails(map);
        return RetMsgHelper.ok(page);
    }

    /***
     * 经办复核
     */
    @ResponseBody
    @RequestMapping(value = "/updateSlflag")
    public RetMsg<Serializable> updateSlflag(@RequestBody Map<String, String> map) throws Exception {
        String fedealno = map.get("fedealno");
        String userid = map.get("userid");
        String slflag = map.get("slflag");
        String curDate = opicsSettleManageService.getCurDate();
        Date date = DateUtil.parse(curDate, "yyyy-MM-dd");
        opicsSettleManageService.updateSlflag(fedealno, userid, date, slflag);
        return RetMsgHelper.ok();
    }

    /**
     * 删除
     */
    @ResponseBody
    @RequestMapping(value = "/deleteCspiAndCsri")
    public RetMsg<Serializable> deleteCspiAndCsri(@RequestBody Map<String, Object> map) {
        opicsSettleManageService.deleteByFedealno(map);
        return RetMsgHelper.ok();
    }

    /***
     * SDVP分页查询
     */
    @ResponseBody
    @RequestMapping(value = "/searchPageSdvp")
    public RetMsg<PageInfo<IfsSdvpHead>> searchPageSdvp(@RequestBody Map<String, Object> map) {
        Page<IfsSdvpHead> page = opicsSettleManageService.searchPageSdvp(map);
        return RetMsgHelper.ok(page);
    }

    /***
     * SDVP明细查询
     */
    @ResponseBody
    @RequestMapping(value = "/searchSdvpDetails")
    public RetMsg<List<IfsSdvpDetail>> searchSdvpDetails(@RequestBody Map<String, Object> map) {
        List<IfsSdvpDetail> page = opicsSettleManageService.searchSdvpDetails(map);
        return RetMsgHelper.ok(page);
    }

    /***
     * 明细分页查询
     */
    @ResponseBody
    @RequestMapping(value = "/searchPageSdvpDetails")
    public RetMsg<PageInfo<IfsSdvpDetail>> searchPageSdvpDetails(@RequestBody Map<String, Object> map) {
        Page<IfsSdvpDetail> page = opicsSettleManageService.searchPageSdvpDetails(map);
        return RetMsgHelper.ok(page);
    }

    @ResponseBody
    @RequestMapping(value = "/saveSdvpAdd")
    public RetMsg<Serializable> saveSdvpAdd(@RequestBody IfsSdvpHead record) {
        // 判断记录是否已存在
        int num = opicsSettleManageService.getSdvpById(record);
        if (num > 0) {
            return RetMsgHelper.simple("000010", "该交易对手已添加此业务默认清算路径，如有变化请找到该笔记录并修改！");
        } else {
            opicsSettleManageService.saveSdvp(record);
            return RetMsgHelper.ok();
        }
    }

    @ResponseBody
    @RequestMapping(value = "/saveSdvpEdit")
    public RetMsg<Serializable> saveSdvpEdit(@RequestBody IfsSdvpHead record) {
        opicsSettleManageService.updateSdvp(record);
        return RetMsgHelper.ok();
    }

    /***
     * 经办复核
     */
    @ResponseBody
    @RequestMapping(value = "/updateSdvpSlflag")
    public RetMsg<Serializable> updateSdvpSlflag(@RequestBody Map<String, String> map) throws Exception {
        String fedealno = map.get("fedealno");
        String userid = map.get("userid");
        String slflag = map.get("slflag");
        String curDate = opicsSettleManageService.getCurDate();
        Date date = DateUtil.parse(curDate, "yyyy-MM-dd");
        opicsSettleManageService.updateSdvpSlflag(fedealno, userid, date, slflag);
        return RetMsgHelper.ok();
    }

    /**
     * 删除
     */
    @ResponseBody
    @RequestMapping(value = "/deleteSdvp")
    public RetMsg<Serializable> deleteSdvp(@RequestBody Map<String, Object> map) {
        opicsSettleManageService.deleteSdvpByFedealno(map);
        return RetMsgHelper.ok();
    }

    /**
     * 复核并同步到opics表
     */
    @ResponseBody
    @RequestMapping(value = "/syncCspiCsri")
    public RetMsg<SlOutBean> syncCspiCsri(@RequestBody Map<String, String> map) {
        // 复核
        String fedealno = map.get("fedealno");
        String userid = map.get("userid");
        String slflag = map.get("slflag");
        String curDate = opicsSettleManageService.getCurDate();
        Date date = DateUtil.parse(curDate, "yyyy-MM-dd");

        // 同步
        SlOutBean slOutBean = new SlOutBean();
        IfsCspiCsriHead cspiCsriHead = opicsSettleManageService.searchCspiCsriHeadById(fedealno);

        try {
            Map<String, Object> cMap = new HashMap<String, Object>();
            cMap.put("br", SlDealModule.BASE.BR);
            cMap.put("cno", cspiCsriHead.getCno());
            cMap.put("ccy", cspiCsriHead.getCcy());
            cMap.put("product", cspiCsriHead.getProduct());
            cMap.put("prodtype", cspiCsriHead.getProdtype());

            SlIinsBean iinsBean;
            if ("P".equals(cspiCsriHead.getPayrecind())) {// CSPI
                // 查询CSPI是否已存在
                if (settleManagerServer.searchCspiById(cMap) <= 0) {
                    cspiCsriHead.setAddupdelind("A");
                } else {
                    cspiCsriHead.setAddupdelind("U");
                }

                iinsBean = new SlIinsBean(SlDealModule.BASE.BR, SlDealModule.SETTLE.SERVER_CSPI,
                    SlDealModule.SETTLE.TAG_CSPI, SlDealModule.SETTLE.DETAIL_IINS);
                // 头表信息,插入第一条
                iinsBean.setPayrecind(cspiCsriHead.getPayrecind());
                iinsBean.setCcy(cspiCsriHead.getCcy());
                iinsBean.setCno(cspiCsriHead.getCno());
                iinsBean.setProduct(cspiCsriHead.getProduct());
                iinsBean.setProdtype(cspiCsriHead.getProdtype());
                iinsBean.setSettaccount(cspiCsriHead.getSettaccount());
                iinsBean.setSettmeans(cspiCsriHead.getSettmeans());
                iinsBean.setP1(cspiCsriHead.getP1());
                iinsBean.setP2(cspiCsriHead.getP2());
                iinsBean.setP3(cspiCsriHead.getP3());
                iinsBean.setP4(cspiCsriHead.getP4());
                iinsBean.setR1(cspiCsriHead.getR1());
                iinsBean.setR2(cspiCsriHead.getR2());
                iinsBean.setR3(cspiCsriHead.getR3());
                iinsBean.setR4(cspiCsriHead.getR4());
                iinsBean.setR5(cspiCsriHead.getR5());
                iinsBean.setR6(cspiCsriHead.getR6());
                iinsBean.setAddupdelind(cspiCsriHead.getAddupdelind());
                iinsBean.setAuthind("Y");
                iinsBean.setSupconfind("N");
                iinsBean.setSuppayind("N");
                iinsBean.setSuprecind("N");

                SlInthBean inthBean = iinsBean.getInthBean();
                inthBean.setFedealno(fedealno);
                inthBean.setSeq("0");
                inthBean.setStatcode("-1");
                inthBean.setLstmntdate(baseServer.getOpicsSysDate("01"));
                iinsBean.setInthBean(inthBean);
                slOutBean = settleManagerServer.addSlIinsBean(iinsBean);
                if ("S".equals(String.valueOf(slOutBean.getRetStatus()))) {
                    Map<String, Object> detailmap = new HashMap<String, Object>();
                    detailmap.put("fedealno", fedealno);
                    List<IfsCspiCsriDetail> list = opicsSettleManageService.searchCspiCsriDetails(detailmap);
                    for (int i = 0; i < list.size(); i++) {
                        iinsBean = new SlIinsBean(SlDealModule.BASE.BR, SlDealModule.SETTLE.SERVER_CSPI,
                            SlDealModule.SETTLE.TAG_CSPI, SlDealModule.SETTLE.DETAIL_IINS);
                        IfsCspiCsriDetail ifsCspiCsriDetail = list.get(i);
                        // 头表信息
                        iinsBean.setPayrecind(cspiCsriHead.getPayrecind());
                        iinsBean.setCcy(cspiCsriHead.getCcy());
                        iinsBean.setCno(cspiCsriHead.getCno());
                        iinsBean.setProduct(cspiCsriHead.getProduct());
                        iinsBean.setProdtype(cspiCsriHead.getProdtype());
                        iinsBean.setSettaccount(cspiCsriHead.getSettaccount());
                        iinsBean.setSettmeans(cspiCsriHead.getSettmeans());
                        iinsBean.setAuthind("Y");
                        iinsBean.setSupconfind("N");
                        iinsBean.setSuppayind("N");
                        iinsBean.setSuprecind("N");
                        iinsBean.setPcc(ifsCspiCsriDetail.getPcc());
                        iinsBean.setAcctno(ifsCspiCsriDetail.getAcctno());
                        iinsBean.setBic(ifsCspiCsriDetail.getBic());
                        iinsBean.setP1(ifsCspiCsriDetail.getP1());
                        iinsBean.setP2(ifsCspiCsriDetail.getP2());
                        iinsBean.setP3(ifsCspiCsriDetail.getP3());
                        iinsBean.setP4(ifsCspiCsriDetail.getP4());
                        inthBean = iinsBean.getInthBean();
                        inthBean.setFedealno(fedealno);
                        inthBean.setSeq(String.valueOf(i + 1));
                        inthBean.setStatcode("-1");
                        inthBean.setLstmntdate(baseServer.getOpicsSysDate("01"));
                        iinsBean.setInthBean(inthBean);
                        slOutBean = settleManagerServer.addSlIinsBean(iinsBean);
                    } // end for

                } // end if
            } else {// CSRI
                // 查询CSRI是否已存在
                if (settleManagerServer.searchCsriById(cMap) <= 0) {
                    cspiCsriHead.setAddupdelind("A");
                } else {
                    cspiCsriHead.setAddupdelind("U");
                }

                iinsBean = new SlIinsBean(SlDealModule.BASE.BR, SlDealModule.SETTLE.SERVER_CSRI,
                    SlDealModule.SETTLE.TAG_CSRI, SlDealModule.SETTLE.DETAIL_IINS);
                Map<String, Object> detailmap = new HashMap<String, Object>();
                detailmap.put("fedealno", fedealno);
                // LIST里只有一条数据
                List<IfsCspiCsriDetail> list = opicsSettleManageService.searchCspiCsriDetails(detailmap);
                if (list.size() > 0) {
                    IfsCspiCsriDetail ifsCspiCsriDetail = list.get(0);
                    if (ifsCspiCsriDetail != null) {
                        iinsBean.setBic(ifsCspiCsriDetail.getBic());
                        iinsBean.setP1(ifsCspiCsriDetail.getP1());
                        iinsBean.setP2(ifsCspiCsriDetail.getP2());
                        iinsBean.setP3(ifsCspiCsriDetail.getP3());
                        iinsBean.setP4(ifsCspiCsriDetail.getP4());
                    }
                }
                // 头表信息
                iinsBean.setPayrecind(cspiCsriHead.getPayrecind());
                iinsBean.setCcy(cspiCsriHead.getCcy());
                iinsBean.setCno(cspiCsriHead.getCno());
                iinsBean.setProduct(cspiCsriHead.getProduct());
                iinsBean.setProdtype(cspiCsriHead.getProdtype());
                iinsBean.setSettaccount(cspiCsriHead.getSettaccount());
                iinsBean.setSettmeans(cspiCsriHead.getSettmeans());
                iinsBean.setAddupdelind(cspiCsriHead.getAddupdelind());
                iinsBean.setAuthind("Y");
                iinsBean.setSupconfind("N");
                iinsBean.setSuppayind("N");
                iinsBean.setSuprecind("N");
                SlInthBean inthBean = iinsBean.getInthBean();
                inthBean.setFedealno(fedealno);
                inthBean.setSeq("0");
                inthBean.setStatcode("-1");
                inthBean.setLstmntdate(baseServer.getOpicsSysDate("01"));
                iinsBean.setInthBean(inthBean);
                slOutBean = settleManagerServer.addSlIinsBean(iinsBean);
            }

        } catch (RemoteConnectFailureException e) {
            slOutBean.setRetCode(SlErrors.FAILED_RMI_OPICS.getErrCode());
            slOutBean.setRetMsg(SlErrors.FAILED_RMI_OPICS.getErrMsg() + ":" + e.getMessage());
            slOutBean.setRetStatus(RetStatusEnum.F);
        } catch (Exception e) {
            slOutBean.setRetCode(SlErrors.FAILED.getErrCode());
            slOutBean.setRetMsg(SlErrors.FAILED.getErrMsg() + ":" + e.getMessage());
            slOutBean.setRetStatus(RetStatusEnum.F);
        }
        if ("S".equals(String.valueOf(slOutBean.getRetStatus()))) {
            Map<String, Object> opicsmap = new HashMap<String, Object>();
            opicsmap.put("fedealno", fedealno);
            opicsmap.put("statcode", "-1");
            opicsmap.put("errorcode", "");
            opicsSettleManageService.updateCspiCsriByFedealno(opicsmap);
            cspiCsriHead.setStatus("1");
            opicsSettleManageService.updateSyncStatus(cspiCsriHead);
            opicsSettleManageService.updateSlflag(fedealno, userid, date, slflag);
        }
        return RetMsgHelper.ok(slOutBean);
    }

    /**
     * SDVP同步到opics表
     */
    @ResponseBody
    @RequestMapping(value = "/syncSdvp")
    public RetMsg<SlOutBean> syncSdvp(@RequestBody Map<String, String> map) {
        String fedealno = map.get("fedealno");
        String userid = map.get("userid");
        String slflag = map.get("slflag");
        String curDate = opicsSettleManageService.getCurDate();
        Date date = DateUtil.parse(curDate, "yyyy-MM-dd");

        SlOutBean slOutBean = new SlOutBean();
        IfsSdvpHead sdvpHead = opicsSettleManageService.searchSdvpHeadById(fedealno);
        try {
            Map<String, Object> cMap = new HashMap<String, Object>();
            cMap.put("br", SlDealModule.BASE.BR);
            cMap.put("cno", sdvpHead.getCno());
            cMap.put("ccy", sdvpHead.getCcy());
            cMap.put("product", sdvpHead.getProduct());
            cMap.put("prodtype", sdvpHead.getProdtype());
            cMap.put("safekeepacct", sdvpHead.getSafekeepacct());
            cMap.put("delrecind", sdvpHead.getDelrecind());
            // 查询SDVP是否已存在
            if (settleManagerServer.searchSdvpById(cMap) <= 0) {
                sdvpHead.setAddupddel("A");
            } else {
                sdvpHead.setAddupddel("U");
            }

            SlIdriBean idriBean;
            idriBean = new SlIdriBean(SlDealModule.BASE.BR, SlDealModule.SETTLE.SERVER_SDVP,
                SlDealModule.SETTLE.TAG_SDVP, SlDealModule.SETTLE.DETAIL_IDRI);
            // 头表信息
            idriBean.setDelrecind(sdvpHead.getDelrecind());
            idriBean.setCcy(sdvpHead.getCcy());
            idriBean.setCno(sdvpHead.getCno());
            idriBean.setProdtype(sdvpHead.getProdtype());
            idriBean.setProduct(sdvpHead.getProduct());
            idriBean.setSettmeans(sdvpHead.getSettmeans());
            idriBean.setSettacct(sdvpHead.getSettacct());
            idriBean.setSafekeepacct(sdvpHead.getSafekeepacct());
            idriBean.setC1(sdvpHead.getC1());
            idriBean.setC2(sdvpHead.getC2());
            idriBean.setC3(sdvpHead.getC3());
            idriBean.setC4(sdvpHead.getC4());
            idriBean.setC5(sdvpHead.getC5());
            idriBean.setC6(sdvpHead.getC6());
            idriBean.setAc1(sdvpHead.getAc1());
            idriBean.setAc2(sdvpHead.getAc2());
            idriBean.setAc3(sdvpHead.getAc3());
            idriBean.setAc4(sdvpHead.getAc4());
            idriBean.setAc5(sdvpHead.getAc5());
            idriBean.setAc6(sdvpHead.getAc6());
            SlInthBean inthBean = idriBean.getInthBean();
            inthBean.setFedealno(fedealno);
            inthBean.setSeq("0");
            inthBean.setStatcode("-1");
            idriBean.setInthBean(inthBean);
            idriBean.setAddupddel(sdvpHead.getAddupddel());
            idriBean.setAuthind("Y");
            idriBean.setSupccyind("N");
            idriBean.setSupconfind("N");
            idriBean.setSupsecind("N");
            slOutBean = settleManagerServer.addSlIdriBean(idriBean);
            if ("S".equals(String.valueOf(slOutBean.getRetStatus()))) {
                Map<String, Object> detailmap = new HashMap<String, Object>();
                detailmap.put("fedealno", fedealno);
                List<IfsSdvpDetail> list = opicsSettleManageService.searchSdvpDetails(detailmap);
                for (int i = 0; i < list.size(); i++) {
                    IfsSdvpDetail ifsSdvpDetail = list.get(i);
                    idriBean = new SlIdriBean(SlDealModule.BASE.BR, SlDealModule.SETTLE.SERVER_SDVP,
                        SlDealModule.SETTLE.TAG_SDVP, SlDealModule.SETTLE.DETAIL_IDRI);
                    idriBean.setDelrecind(sdvpHead.getDelrecind());
                    idriBean.setCcy(sdvpHead.getCcy());
                    idriBean.setCno(sdvpHead.getCno());
                    idriBean.setProdtype(sdvpHead.getProdtype());
                    idriBean.setProduct(sdvpHead.getProduct());
                    idriBean.setSettmeans(sdvpHead.getSettmeans());
                    idriBean.setSettacct(sdvpHead.getSettacct());
                    idriBean.setSafekeepacct(sdvpHead.getSafekeepacct());
                    idriBean.setDcc(ifsSdvpDetail.getDcc());
                    idriBean.setAcctno(ifsSdvpDetail.getAcctno());
                    idriBean.setBic(ifsSdvpDetail.getBic());
                    idriBean.setC1(ifsSdvpDetail.getC1());
                    idriBean.setC2(ifsSdvpDetail.getC2());
                    idriBean.setC3(ifsSdvpDetail.getC3());
                    idriBean.setC4(ifsSdvpDetail.getC4());
                    inthBean = idriBean.getInthBean();
                    inthBean.setFedealno(fedealno);
                    inthBean.setSeq(String.valueOf(i + 1));
                    inthBean.setStatcode("-1");
                    idriBean.setInthBean(inthBean);
                    idriBean.setAuthind("Y");
                    idriBean.setSupccyind("N");
                    idriBean.setSupconfind("N");
                    idriBean.setSupsecind("N");
                    slOutBean = settleManagerServer.addSlIdriBean(idriBean);
                }
            }

        } catch (RemoteConnectFailureException e) {
            slOutBean.setRetCode(SlErrors.FAILED_RMI_OPICS.getErrCode());
            slOutBean.setRetMsg(SlErrors.FAILED_RMI_OPICS.getErrMsg() + ":" + e.getMessage());
            slOutBean.setRetStatus(RetStatusEnum.F);
        } catch (Exception e) {
            slOutBean.setRetCode(SlErrors.FAILED.getErrCode());
            slOutBean.setRetMsg(SlErrors.FAILED.getErrMsg() + ":" + e.getMessage());
            slOutBean.setRetStatus(RetStatusEnum.F);
        }
        if ("S".equals(String.valueOf(slOutBean.getRetStatus()))) {
            Map<String, Object> opicsmap = new HashMap<String, Object>();
            opicsmap.put("fedealno", fedealno);
            opicsmap.put("statcode", "-1");
            opicsmap.put("errorcode", "");
            opicsSettleManageService.updateSdvpByFedealno(opicsmap);
            sdvpHead.setStatus("1");
            opicsSettleManageService.updateSdvpSyncStatus(sdvpHead);
            opicsSettleManageService.updateSdvpSlflag(fedealno, userid, date, slflag);
        }
        return RetMsgHelper.ok(slOutBean);
    }

    /***
     * 打开界面时，根据流水号查询产品代码、类型和币种等信息，再根据这些数据查询清算路径
     * 
     * @throws Exception
     * @throws RemoteConnectFailureException
     */
    @ResponseBody
    @RequestMapping(value = "/searchProdLimit")
    public IfsSearchSettleBean searchProdLimit(@RequestBody String ticketId)
        throws RemoteConnectFailureException, Exception {
        /* 查询交易对手、产品代码、类型和币种等信息 */
        IfsSearchSettleBean settleBean = ifsOpicsSettleManageMapper.searchProdLimit(ticketId);
        String product = settleBean.getProduct();
        String type = settleBean.getProdtype();
        String cno = settleBean.getCno();
        String pccy = settleBean.getPccy();
        String rccy = settleBean.getRccy();

        /* 查询付款ifs_cspicsri_head清算路径 */
        Map<String, Object> pmap = new HashMap<String, Object>();
        pmap.put("cno", cno);
        pmap.put("ccy", pccy);
        pmap.put("product", product);
        pmap.put("type", type);
        SlCspiCsriBean phead = iCspiCsriSdvpServer.searchpaycspicsrihead(pmap); // 付款
        if(phead==null){
            pmap.put("product", "DEFSI");
            pmap.put("type", "DF");
            phead= iCspiCsriSdvpServer.searchpaycspicsrihead(pmap); // 付款
        }
        /* 查询收款ifs_cspicsri_head清算路径 */
        Map<String, Object> rmap = new HashMap<String, Object>();
        rmap.put("cno", cno);
        rmap.put("ccy", rccy);
        rmap.put("product", product);
        rmap.put("type", type);
        SlCspiCsriBean rhead = iCspiCsriSdvpServer.searchreccspicsrihead(rmap); // 收款
        if(rhead==null){
        	rmap.put("product", "DEFSI");
        	rmap.put("type", "DF");
            rhead = iCspiCsriSdvpServer.searchreccspicsrihead(rmap); // 收款
        }

        List<SlCspiCsriBean> list = new ArrayList<SlCspiCsriBean>();
        if (phead != null) {
            list.add(phead);
            /* 查询付款列表ifs_cspicsri_detail内容 */
            String br = phead.getBr();
            Map<String, Object> upmap = new HashMap<String, Object>();
            upmap.put("br", br);
            upmap.put("cno", cno);
            upmap.put("ccy", pccy);
            upmap.put("product", StringUtils.trimToEmpty(phead.getProduct()));
            upmap.put("type", StringUtils.trimToEmpty(phead.getType()));
            List<SlCspiCsriDetailBean> paydetailList = iCspiCsriSdvpServer.searchcspicsridetail(upmap); // 付款明细
            if (paydetailList != null) {
                settleBean.setPayDetailList(paydetailList);
            }
        }
        if (rhead != null) {
            list.add(rhead);
            /* 查询收款列表ifs_cspicsri_detail内容 */
            String br2 = rhead.getBr();
            Map<String, Object> upmap2 = new HashMap<String, Object>();
            upmap2.put("br", br2);
            upmap2.put("cno", cno);
            upmap2.put("ccy", rccy);
            upmap2.put("product", StringUtils.trimToEmpty(rhead.getProduct()));
            upmap2.put("type", StringUtils.trimToEmpty(rhead.getType()));
            List<SlCspiCsriDetailBean> recdetailList = iCspiCsriSdvpServer.searchcsridetail(upmap2); // 收款明细
            if (!(StringUtil.isEmpty(recdetailList.get(0).getBic()) && StringUtil.isEmpty(recdetailList.get(0).getC1())
                && StringUtil.isEmpty(recdetailList.get(0).getC2()) && StringUtil.isEmpty(recdetailList.get(0).getC3())
                && StringUtil.isEmpty(recdetailList.get(0).getC4()))) {
                settleBean.setRecDetailList(recdetailList);
            }
        }
        if (list != null) {
            settleBean.setList(list); // 将从ifs_cspicsri_head表中查询出来的值插入
        }
        return settleBean;
    }

    /***
     * 托管账户 查询清算路径
     */
    @ResponseBody
    @RequestMapping(value = "/searchSettInfo")
    public IfsSearchSettleBean searchSettInfo(@RequestBody String ticketId)
        throws RemoteConnectFailureException, Exception {
        /* 查询交易对手、产品代码、类型和币种等信息 */
        IfsSearchSettleBean settleBean = ifsOpicsSettleManageMapper.searchProdLimit(ticketId);
        String product = settleBean.getProduct();
        String prodtype = settleBean.getProdtype();
        String cno = settleBean.getCno();
        String pccy = settleBean.getPccy();
        String rccy = settleBean.getRccy();
        String safekeepacct = settleBean.getSafekeepacct(); // 托管账户

        String mydircn = settleBean.getMydircn();
        String prdname = settleBean.getPrdname();
        settleBean.setMydircn(mydircn); // 本方方向
        settleBean.setPrdname(prdname); // 产品名称

        List<SlSdvpBean> headsList = new ArrayList<SlSdvpBean>();
        /* 查询付款清算路径 */
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("cno", cno);
        map.put("ccy", pccy);
        map.put("product", product);
        map.put("type", prodtype);
        map.put("safekeepacct", safekeepacct);
        SlSdvpBean pheadsList = iCspiCsriSdvpServer.searchpaysdvphead(map); // 查付款
        if (pheadsList != null) {
            headsList.add(pheadsList);
            String br = pheadsList.getBr();
            /* 查询列表ifs_sdvp_detail内容 */
            Map<String, Object> upmap = new HashMap<String, Object>();
            upmap.put("br", br);
            upmap.put("product", product);
            upmap.put("prodtype", prodtype);
            upmap.put("cno", cno);
            upmap.put("ccy", pccy);
            upmap.put("safekeepacct", safekeepacct);
            upmap.put("delrecind", pheadsList.getDelrecind());
            List<SlSdvpDetailBean> payDetailList = iCspiCsriSdvpServer.searchsdvpdetail(upmap); // 付款明细
            if (payDetailList != null) {
                settleBean.setPayIfsSdvpDetail(payDetailList);
            }
        }

        /* 查询收款清算路径 */
        Map<String, Object> map2 = new HashMap<String, Object>();
        map2.put("cno", cno);
        map2.put("ccy", rccy);
        map2.put("product", product);
        map2.put("prodtype", prodtype);
        map2.put("safekeepacct", safekeepacct);
        SlSdvpBean headsList2 = iCspiCsriSdvpServer.searchrecsdvphead(map2); // 查收款
        if (headsList2 != null) {
            headsList.add(headsList2);
            String br = headsList2.getBr();
            /* 查询列表ifs_sdvp_detail内容 */
            Map<String, Object> upmap = new HashMap<String, Object>();
            upmap.put("br", br);
            upmap.put("product", product);
            upmap.put("prodtype", prodtype);
            upmap.put("cno", cno);
            upmap.put("ccy", rccy);
            upmap.put("safekeepacct", safekeepacct);
            upmap.put("delrecind", headsList2.getDelrecind());
            List<SlSdvpDetailBean> recDetailList = iCspiCsriSdvpServer.searchsdvpdetail(upmap); // 收款明细
            if (recDetailList != null) {
                settleBean.setRecIfsSdvpDetail(recDetailList);
            }
        }
        if (headsList != null) {
            settleBean.setIfsSdvpHead(headsList); // 将从ifs_sdvp_head表中查询出来的值插入
        }
        return settleBean;
    }

    /**
     * CSPI/CSRI 手动启动定时任务
     */
    @ResponseBody
    @RequestMapping(value = "/checkSyncStatus")
    public RetMsg<Serializable> checkSyncStatus(@RequestBody Map<String, String> hmap) throws Exception {
        String type = String.valueOf(hmap.get("type"));
        String fedealno;
        String[] fedealnos;
        if ("1".equals(type)) {// 选中记录校准
            fedealno = hmap.get("fedealno");
            fedealnos = fedealno.split(",");
            for (String string : fedealnos) {
                IfsCspiCsriHead ifsCspiCsriHead = ifsOpicsSettleManageMapper.searchCspiCsriHeadById(string);
                if (ifsCspiCsriHead != null) {
                    SlInthBean inthBean = new SlInthBean();
                    if ("P".equals(ifsCspiCsriHead.getPayrecind())) {
                        inthBean.setServer(SlDealModule.SETTLE.SERVER_CSPI);
                        inthBean.setTag(SlDealModule.SETTLE.TAG_CSPI);
                    } else {
                        inthBean.setServer(SlDealModule.SETTLE.SERVER_CSRI);
                        inthBean.setTag(SlDealModule.SETTLE.TAG_CSRI);
                    }
                    inthBean.setBr(SlDealModule.BASE.BR);
                    inthBean.setFedealno(ifsCspiCsriHead.getFedealno());
                    // 只会查询到一笔数据
                    SlInthBean slInthBean = baseServer.getInthStatus(inthBean);
                    if (slInthBean != null) {
                        Map<String, Object> map = new HashMap<String, Object>();
                        map.put("fedealno", slInthBean.getFedealno());
                        map.put("statcode", slInthBean.getStatcode());
                        map.put("errorcode", slInthBean.getErrorcode());
                        ifsOpicsSettleManageMapper.updateCspiCsriByFedealno(map);
                        if (StringUtil.isNotEmpty(slInthBean.getStatcode())) {
                            if ("-4".equals(StringUtils.trimToEmpty(slInthBean.getStatcode()))) {
                                ifsCspiCsriHead.setStatus("2");
                            } else if ("-6".equals(StringUtils.trimToEmpty(slInthBean.getStatcode()))) {
                                ifsCspiCsriHead.setStatus("3");
                            }
                            ifsOpicsSettleManageMapper.updateSyncStatus(ifsCspiCsriHead);
                        }
                    }
                }
            }

        } else {// 全部校准
            // 查询全部已同步未确定是否成功的
            List<IfsCspiCsriHead> listCspiCsri = ifsOpicsSettleManageMapper.getSyncCspiCsriList();
            for (IfsCspiCsriHead ifsCspiCsriHead : listCspiCsri) {
                SlInthBean inthBean = new SlInthBean();
                if ("P".equals(ifsCspiCsriHead.getPayrecind())) {
                    inthBean.setServer(SlDealModule.SETTLE.SERVER_CSPI);
                    inthBean.setTag(SlDealModule.SETTLE.TAG_CSPI);
                } else {
                    inthBean.setServer(SlDealModule.SETTLE.SERVER_CSRI);
                    inthBean.setTag(SlDealModule.SETTLE.TAG_CSRI);
                }
                inthBean.setBr(SlDealModule.BASE.BR);
                inthBean.setFedealno(ifsCspiCsriHead.getFedealno());
                // 只会查询到一笔数据
                SlInthBean slInthBean = baseServer.getInthStatus(inthBean);
                if (slInthBean != null) {
                    Map<String, Object> map = new HashMap<String, Object>();
                    map.put("fedealno", slInthBean.getFedealno());
                    map.put("statcode", slInthBean.getStatcode());
                    map.put("errorcode", slInthBean.getErrorcode());
                    ifsOpicsSettleManageMapper.updateCspiCsriByFedealno(map);
                }
            }
            // 查询全部已同步已确定是否成功的
            List<IfsCspiCsriHead> listSCspiCsris = ifsOpicsSettleManageMapper.getSyncSCspiCsriList();
            for (IfsCspiCsriHead ifsCspiCsriHead : listSCspiCsris) {
                SlInthBean inthBean = new SlInthBean();
                if ("P".equals(ifsCspiCsriHead.getPayrecind())) {
                    inthBean.setServer(SlDealModule.SETTLE.SERVER_CSPI);
                    inthBean.setTag(SlDealModule.SETTLE.TAG_CSPI);
                } else {
                    inthBean.setServer(SlDealModule.SETTLE.SERVER_CSRI);
                    inthBean.setTag(SlDealModule.SETTLE.TAG_CSRI);
                }
                inthBean.setBr(SlDealModule.BASE.BR);
                inthBean.setFedealno(ifsCspiCsriHead.getFedealno());
                // 只会查询到一笔数据
                SlInthBean slInthBean = baseServer.getInthStatus(inthBean);
                if (slInthBean != null) {
                    if ("-4".equals(StringUtils.trimToEmpty(slInthBean.getStatcode()))) {
                        ifsCspiCsriHead.setStatus("2");
                    } else {
                        ifsCspiCsriHead.setStatus("3");
                    }
                    ifsOpicsSettleManageMapper.updateSyncStatus(ifsCspiCsriHead);
                }
            }
        }
        return RetMsgHelper.ok("error.common.0000");
    }

    /**
     * SDVP 手动启动定时任务
     */
    @ResponseBody
    @RequestMapping(value = "/checkSvdpStatus")
    public RetMsg<Serializable> checkSvdpStatus(@RequestBody Map<String, String> hmap) throws Exception {
        String type = String.valueOf(hmap.get("type"));
        String fedealno;
        String[] fedealnos;
        if ("1".equals(type)) {// 选中记录校准
            fedealno = hmap.get("fedealno");
            fedealnos = fedealno.split(",");
            for (String string : fedealnos) {
                IfsSdvpHead ifsSdvpHead = ifsOpicsSettleManageMapper.searchSdvpHeadById(string);
                SlInthBean inthBean = new SlInthBean();
                inthBean.setServer(SlDealModule.SETTLE.SERVER_SDVP);
                inthBean.setTag(SlDealModule.SETTLE.TAG_SDVP);
                inthBean.setBr(SlDealModule.BASE.BR);
                inthBean.setFedealno(ifsSdvpHead.getFedealno());
                // 只会查询到一笔数据
                SlInthBean slInthBean = baseServer.getInthStatus(inthBean);
                if (slInthBean != null) {
                    Map<String, Object> map = new HashMap<String, Object>();
                    map.put("fedealno", slInthBean.getFedealno());
                    map.put("statcode", slInthBean.getStatcode());
                    map.put("errorcode", slInthBean.getErrorcode());
                    ifsOpicsSettleManageMapper.updateSdvpByFedealno(map);
                    if (StringUtil.isNotEmpty(slInthBean.getStatcode())) {
                        if ("-4".equals(StringUtils.trimToEmpty(slInthBean.getStatcode()))) {
                            ifsSdvpHead.setStatus("2");
                        } else if ("-6".equals(StringUtils.trimToEmpty(slInthBean.getStatcode()))) {
                            ifsSdvpHead.setStatus("3");
                        }
                        ifsOpicsSettleManageMapper.updateSdvpSyncStatus(ifsSdvpHead);
                    }
                }
            }
        } else {// 全部校准
            // 查询全部已同步未确定是否成功的
            List<IfsSdvpHead> listSdvp = ifsOpicsSettleManageMapper.getSyncSdvp();
            for (IfsSdvpHead ifsSdvpHead : listSdvp) {
                SlInthBean inthBean = new SlInthBean();
                inthBean.setServer(SlDealModule.SETTLE.SERVER_SDVP);
                inthBean.setTag(SlDealModule.SETTLE.TAG_SDVP);
                inthBean.setBr(SlDealModule.BASE.BR);
                inthBean.setFedealno(ifsSdvpHead.getFedealno());
                // 只会查询到一笔数据
                SlInthBean slInthBean = baseServer.getInthStatus(inthBean);
                if (slInthBean != null) {
                    Map<String, Object> map = new HashMap<String, Object>();
                    map.put("fedealno", slInthBean.getFedealno());
                    map.put("statcode", slInthBean.getStatcode());
                    map.put("errorcode", slInthBean.getErrorcode());
                    ifsOpicsSettleManageMapper.updateSdvpByFedealno(map);
                }
            }
            // 查询全部已同步已确定是否成功的
            List<IfsSdvpHead> listSSdvp = ifsOpicsSettleManageMapper.getSyncSSdvpList();
            for (IfsSdvpHead ifsSdvpHead : listSSdvp) {
                SlInthBean inthBean = new SlInthBean();
                inthBean.setServer(SlDealModule.SETTLE.SERVER_SDVP);
                inthBean.setTag(SlDealModule.SETTLE.TAG_SDVP);
                inthBean.setBr(SlDealModule.BASE.BR);
                inthBean.setFedealno(ifsSdvpHead.getFedealno());
                // 只会查询到一笔数据
                SlInthBean slInthBean = baseServer.getInthStatus(inthBean);
                if (slInthBean != null) {
                    if ("-4".equals(StringUtils.trimToEmpty(slInthBean.getStatcode()))) {
                        ifsSdvpHead.setStatus("2");
                    } else {
                        ifsSdvpHead.setStatus("3");
                    }
                    ifsOpicsSettleManageMapper.updateSdvpSyncStatus(ifsSdvpHead);
                }
            }
        }
        return RetMsgHelper.ok("error.common.0000");
    }

    /***
     * 托管账户 查询清算路径（存单多笔的）
     */
    @ResponseBody
    @RequestMapping(value = "/searchSettInfoList")
    public List<IfsSearchSettleBean> searchSettInfoList(@RequestBody String ticketId) {
        /* 查询交易对手、产品代码、类型和币种等信息 */
        List<IfsSearchSettleBean> settleBeanList = ifsOpicsSettleManageMapper.searchProdLimitList(ticketId);
        return settleBeanList;
    }

    /***
     * 托管账户 查询清算路径（存单多笔的）
     */
    @ResponseBody
    @RequestMapping(value = "/searchSettInfoListDetail")
    public IfsSearchSettleBean searchSettInfoListDetail(@RequestBody Map<String, String> smap)
        throws RemoteConnectFailureException, Exception {
        /* 查询交易对手、产品代码、类型和币种等信息 */
        IfsSearchSettleBean settleBean = ifsOpicsSettleManageMapper.searchSettInfoListDetail(smap);
        String product = settleBean.getProduct();
        String prodtype = settleBean.getProdtype();
        String cno = settleBean.getCno();
        String pccy = settleBean.getPccy();
        String rccy = settleBean.getRccy();
        String safekeepacct = settleBean.getSafekeepacct();

        List<SlSdvpBean> headsList = new ArrayList<SlSdvpBean>();
        /* 查询付款清算路径 */
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("cno", cno);
        map.put("ccy", pccy);
        map.put("product", product);
        map.put("type", prodtype);
        map.put("safekeepacct", safekeepacct);
        SlSdvpBean headsList1 = iCspiCsriSdvpServer.searchpaysdvphead(map); // 查付款
        if (headsList1 != null) {
            headsList.add(headsList1);
            String br = headsList1.getBr();
            /* 查询列表ifs_sdvp_detail内容 */
            Map<String, Object> upmap = new HashMap<String, Object>();
            upmap.put("br", br);
            upmap.put("product", product);
            upmap.put("prodtype", prodtype);
            upmap.put("cno", cno);
            upmap.put("ccy", pccy);
            upmap.put("safekeepacct", safekeepacct);
            upmap.put("delrecind", headsList1.getDelrecind());
            List<SlSdvpDetailBean> payDetailList = iCspiCsriSdvpServer.searchsdvpdetail(upmap); // 付款明细
            if (payDetailList != null) {
                settleBean.setPayIfsSdvpDetail(payDetailList);
            }
        }

        /* 查询收款清算路径 */
        Map<String, Object> map2 = new HashMap<String, Object>();
        map2.put("cno", cno);
        map2.put("ccy", rccy);
        map2.put("product", product);
        map2.put("prodtype", prodtype);
        map2.put("safekeepacct", safekeepacct);
        SlSdvpBean headsList2 = iCspiCsriSdvpServer.searchrecsdvphead(map2); // 查收款
        if (headsList2 != null) {
            headsList.add(headsList2);
            String br = headsList1.getBr();
            /* 查询列表ifs_sdvp_detail内容 */
            Map<String, Object> upmap = new HashMap<String, Object>();
            upmap.put("br", br);
            upmap.put("product", product);
            upmap.put("prodtype", prodtype);
            upmap.put("cno", cno);
            upmap.put("ccy", rccy);
            upmap.put("safekeepacct", safekeepacct);
            upmap.put("delrecind", headsList2.getDelrecind());
            List<SlSdvpDetailBean> recDetailList = iCspiCsriSdvpServer.searchsdvpdetail(upmap); // 收款明细
            if (recDetailList != null) {
                settleBean.setRecIfsSdvpDetail(recDetailList);
            }
        }

        if (headsList != null) {
            settleBean.setIfsSdvpHead(headsList); // 将从ifs_sdvp_head表中查询出来的值插入
        }
        return settleBean;
    }

    /***
     * 校验：托管账户 查询清算路径（存单多笔的）
     */
    @ResponseBody
    @RequestMapping(value = "/checkSearchSettInfo")
    public String checkSearchSettInfo(@RequestBody String ticketId) {
        String msg = "";
        List<IfsSearchSettleBean> settleBeanList = ifsOpicsSettleManageMapper.searchProdLimitList(ticketId);
        for (IfsSearchSettleBean issb : settleBeanList) {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("cno", issb.getCno());
            map.put("ccy", issb.getPccy());
            map.put("product", issb.getProduct());
            map.put("prodtype", issb.getProdtype());
            map.put("safekeepacct", issb.getSafekeepacct());
            List<IfsSdvpHead> headsList = ifsOpicsSettleManageMapper.searchSettInfoList(map);
            if (headsList.size() == 0) {
                msg = "交易对手为【" + issb.getCno() + "】的付款路径没有！";
                break;
            }
            for (IfsSdvpHead ish : headsList) {
                String settmeans = ish.getSettmeans() == null ? "" : ish.getSettmeans();
                String settacct = ish.getSettacct() == null ? "" : ish.getSettacct();
                if ("".equals(settmeans) || "".equals(settacct)) {
                    msg = "交易对手为【" + issb.getCno() + "】的付款路径没有！";
                    break;
                }
            }
            IfsSdvpHead heads = ifsOpicsSettleManageMapper.recsearchSettInfo(map);
            if (heads == null) {
                msg = "交易对手为【" + issb.getCno() + "】的收款路径没有！";
                break;
            } else {
                String settmeans = heads.getSettmeans() == null ? "" : heads.getSettmeans();
                String settacct = heads.getSettacct() == null ? "" : heads.getSettacct();
                if ("".equals(settmeans) || "".equals(settacct)) {
                    msg = "交易对手为【" + issb.getCno() + "】的收款路径没有！";
                    break;
                }
            }
            if (!"".equals(msg)) {
                break;
            }
        }
        return msg;
    }

    // 所有产品校验清算路径
    @ResponseBody
    @RequestMapping(value = "/checkSearchSettInfoAll")
    public Map<String, String> checkSearchSettInfoAll(@RequestBody Map<String, String> smap) throws RemoteConnectFailureException, Exception {
        String ticketId = smap.get("ticketId").toString();
        String prdNo = smap.get("prdNo").toString();
        String msg = ifsOpicsSettleManageService.checkSearchSettInfoAll(prdNo, ticketId);
        Map<String, String> map = new HashMap<String, String>();
        map.put("msg", msg);
        map.put("indexVal", smap.get("indexVal").toString());
        return map;
    }

    // 单笔校验：外汇需要校验其他信息（付外币才校验）
    @ResponseBody
    @RequestMapping(value = "/checkFxFlagSinglee")
    public String checkFxFlagSinglee(@RequestBody Map<String, Object> map) {
        String ticketId = map.get("ticketId").toString();
        String prdNo = map.get("prdNo").toString();
        String msg = ""; //checkFxFlag(ticketId, prdNo);
        return msg;
    }

    // 外汇需要校验其他信息（付外币才校验）
    private String checkFxFlag(String ticketId, String prdNo) {
        String msg = "";
        // 校验四个值，设置变量
        Boolean checkflag = false;// 判断是够需要校验(付外币校验)
        String BEL = "";// 收款行bic code
        String BER = "";// 收款行账号
        String AWL = "";// 开户行bic code
        String INL = "";// 中间行bic code
        // 获取交易对应的值
        if ("rmbspt".equals(prdNo)) {
            // 外汇即期
            IfsCfetsfxSpt ifsCfetsfxSpt = ifsCfetsfxSptMapper.getSpot(ticketId);
            if (ifsCfetsfxSpt != null) {
                // 判断净额清算:0不是
                if (!"1".equals(ifsCfetsfxSpt.getNettingStatus() == null ? "" : ifsCfetsfxSpt.getNettingStatus())) {
                    // 付外币
                    String currencyPair = ifsCfetsfxSpt.getCurrencyPair();
                    if (("P".equalsIgnoreCase(ifsCfetsfxSpt.getBuyDirection())
                        && !"CNY".equalsIgnoreCase(currencyPair.substring(4, 7)))
                        || ("S".equalsIgnoreCase(ifsCfetsfxSpt.getBuyDirection())
                            && !"CNY".equalsIgnoreCase(currencyPair.substring(0, 3)))) {
                        checkflag = true;
                        AWL = ifsCfetsfxSpt.getBankBic2();
                        BEL = ifsCfetsfxSpt.getDueBankName2();
                        BER = ifsCfetsfxSpt.getCapitalAccount2();
                        INL = ifsCfetsfxSpt.getIntermediaryBankBicCode2();
                    }
                }
            } else {
                msg = "【" + ticketId + "】交易不存在";
            }
        } else if ("rmbfwd".equals(prdNo)) {
            // 外汇远期
            IfsCfetsfxFwd ifsCfetsfxFwd = ifsCfetsfxFwdMapper.getCredit(ticketId);
            if (ifsCfetsfxFwd != null) {
                if (!"1".equals(ifsCfetsfxFwd.getNettingStatus() == null ? "" : ifsCfetsfxFwd.getNettingStatus())) {
                    // 付外币
                    String currencyPair = ifsCfetsfxFwd.getCurrencyPair();
                    if (("P".equalsIgnoreCase(ifsCfetsfxFwd.getBuyDirection())
                        && !"CNY".equalsIgnoreCase(currencyPair.substring(4, 7)))
                        || ("S".equalsIgnoreCase(ifsCfetsfxFwd.getBuyDirection())
                            && !"CNY".equalsIgnoreCase(currencyPair.substring(0, 3)))) {
                        checkflag = true;
                        AWL = ifsCfetsfxFwd.getBankBic2();
                        BEL = ifsCfetsfxFwd.getDueBankName2();
                        BER = ifsCfetsfxFwd.getCapitalAccount2();
                        INL = ifsCfetsfxFwd.getIntermediaryBankBicCode2();
                    }
                }
            } else {
                msg = "【" + ticketId + "】交易不存在";
            }
        } else if ("rmbswap".equals(prdNo)) {
            // 外汇掉期
            IfsCfetsfxSwap ifsCfetsfxSwap = ifsCfetsfxSwapMapper.getrmswap(ticketId);
            if (ifsCfetsfxSwap != null) {
                if (!"1".equals(ifsCfetsfxSwap.getNettingStatus() == null ? "" : ifsCfetsfxSwap.getNettingStatus())) {
                    // 付外币
                    String currencyPair = ifsCfetsfxSwap.getCurrencyPair();
                    if (("P".equalsIgnoreCase(ifsCfetsfxSwap.getDirection())
                        && !"CNY".equalsIgnoreCase(currencyPair.substring(4, 7)))
                        || ("S".equalsIgnoreCase(ifsCfetsfxSwap.getDirection())
                            && !"CNY".equalsIgnoreCase(currencyPair.substring(0, 3)))) {
                        checkflag = true;
                        AWL = ifsCfetsfxSwap.getBankBic2();
                        BEL = ifsCfetsfxSwap.getDueBankName2();
                        BER = ifsCfetsfxSwap.getDueBankAccount2();
                        INL = ifsCfetsfxSwap.getIntermediaryBankBicCode2();
                    }
                }
            } else {
                msg = "【" + ticketId + "】交易不存在";
            }
        } else if ("rmbopt".equals(prdNo)) {
            // 外汇期权
            IfsCfetsfxOption ifsCfetsfxOption = ifsCfetsfxOptionMapper.searchOption(ticketId);
            if (ifsCfetsfxOption != null) {
                if (!"1"
                    .equals(ifsCfetsfxOption.getNettingStatus() == null ? "" : ifsCfetsfxOption.getNettingStatus())) {
                    // 付外币
                    String currencyPair = ifsCfetsfxOption.getCurrencyPair();
                    if (("P".equalsIgnoreCase(ifsCfetsfxOption.getDirection())
                        && !"CNY".equalsIgnoreCase(currencyPair.substring(4, 7)))
                        || ("S".equalsIgnoreCase(ifsCfetsfxOption.getDirection())
                            && !"CNY".equalsIgnoreCase(currencyPair.substring(0, 3)))) {
                        checkflag = true;
                        AWL = ifsCfetsfxOption.getBankBic2();
                        BEL = ifsCfetsfxOption.getDueBankName2();
                        BER = ifsCfetsfxOption.getCapitalAccount2();
                        INL = ifsCfetsfxOption.getIntermediaryBankBicCode2();
                    }
                }
            } else {
                msg = "【" + ticketId + "】交易不存在";
            }
        }
        if (BEL == null && BER == null && AWL == null && INL == null) {
            checkflag = false;
        }
        if (checkflag) {
            // 比较
            IfsSearchSettleBean settleBean = ifsOpicsSettleManageMapper.searchProdLimit(ticketId);
            Map<String, Object> map2 = new HashMap<String, Object>();
            map2.put("cno", settleBean.getCno());
            map2.put("ccy", settleBean.getPccy());
            map2.put("product", settleBean.getProduct());
            map2.put("prodtype", settleBean.getProdtype());
            IfsCspiCsriHead headsList2 = ifsOpicsSettleManageMapper.searchcspicsri(map2); // 付款明细
            if (headsList2 != null) {
                Map<String, Object> upmap2 = new HashMap<String, Object>();
                upmap2.put("br", headsList2.getBr());
                upmap2.put("server", headsList2.getServer());
                upmap2.put("fedealno", headsList2.getFedealno());
                upmap2.put("inoutind", headsList2.getInoutind());
                List<IfsCspiCsriDetail> paydetailList = ifsOpicsSettleManageMapper.searchcspicsridetail(upmap2);
                String BEL1 = "";// 收款行bic code
                String BER1 = "";// 收款行账号
                String AWL1 = "";// 开户行bic code
                String INL1 = "";// 中间行bic code
                for (int j = 0; j < paydetailList.size(); j++) {
                    IfsCspiCsriDetail ifsCspiCsriDetail = paydetailList.get(j);
                    if ("BE".equals(ifsCspiCsriDetail.getPcc() == null ? "" : ifsCspiCsriDetail.getPcc().trim())) {
                        BEL1 = ifsCspiCsriDetail.getBic() == null ? "" : ifsCspiCsriDetail.getBic().trim();
                        BER1 = ifsCspiCsriDetail.getAcctno() == null ? ""
                            : ifsCspiCsriDetail.getAcctno().trim().substring(1);
                    } else if ("AW"
                        .equals(ifsCspiCsriDetail.getPcc() == null ? "" : ifsCspiCsriDetail.getPcc().trim())) {
                        AWL1 = ifsCspiCsriDetail.getBic() == null ? "" : ifsCspiCsriDetail.getBic().trim();
                    } else if ("IN"
                        .equals(ifsCspiCsriDetail.getPcc() == null ? "" : ifsCspiCsriDetail.getPcc().trim())) {
                        INL1 = ifsCspiCsriDetail.getBic() == null ? "" : ifsCspiCsriDetail.getBic().trim();
                    }
                }
                if (BEL != null) {
                    if (!BEL.equals(BEL1)) {
                        msg = "【" + ticketId + "】的【收款行bic code】不正确";
                    }
                }
                if (BER != null) {
                    if (!BER.equals(BER1)) {
                        msg = "【" + ticketId + "】的【收款行账号】不正确";
                    }
                }
                if (AWL != null) {
                    if (!AWL.equals(AWL1)) {
                        msg = "【" + ticketId + "】的【开户行bic code】不正确";
                    }
                }
                if (INL != null) {
                    if (!INL.equals(INL1)) {
                        msg = "【" + ticketId + "】的【中间行bic code】不正确";
                    }
                }
            } else {
                msg = "【" + ticketId + "】的清算详情没有";
            }
        }
        return msg;
    }

    /***
     * 打开界面时，根据流水号查询产品代码、类型和币种等信息，再根据这些数据查询清算路径-------外币掉期
     * 
     * @throws Exception
     * @throws RemoteConnectFailureException
     */
    @ResponseBody
    @RequestMapping(value = "/searchFxSwapSettle")
    public IfsSearchSettleBean searchFxSwapSettle(@RequestBody String ticketId)
        throws RemoteConnectFailureException, Exception {
        /* 查询交易对手、产品代码、类型和币种等信息 */
        IfsSearchSettleBean settleBean = ifsOpicsSettleManageMapper.searchProdLimit(ticketId);
        String product = settleBean.getProduct();
        String prodtype = settleBean.getProdtype();
        String cno = settleBean.getCno();
        String pccy = settleBean.getPccy();
        String rccy = settleBean.getRccy();

        /* 近端付款清算路径 */
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("cno", cno);
        map.put("ccy", pccy);
        map.put("product", product);
        map.put("type", prodtype);
        SlCspiCsriBean headsList1 = iCspiCsriSdvpServer.searchpaycspicsrihead(map); // 近端付款
        if(headsList1==null){
        	 map.put("product", "DEFSI");
             map.put("type", "DF");
             headsList1 = iCspiCsriSdvpServer.searchpaycspicsrihead(map); // 近端付款
        }

        /* 近端收款清算路径 */
        Map<String, Object> map2 = new HashMap<String, Object>();
        map2.put("cno", cno);
        map2.put("ccy", rccy);
        map2.put("product", product);
        map2.put("type", prodtype);
        SlCspiCsriBean headsList2 = iCspiCsriSdvpServer.searchreccspicsrihead(map2); // 近端收款
        if(headsList2==null){
        	map2.put("product", "DEFSI");
        	map2.put("type", "DF");
        	headsList2 = iCspiCsriSdvpServer.searchreccspicsrihead(map2); // 近端收款
        }

        /* 远端付款清算路径 */
        Map<String, Object> map3 = new HashMap<String, Object>();
        map3.put("cno", cno);
        map3.put("ccy", rccy);
        map3.put("product", product);
        map3.put("type", prodtype);
        SlCspiCsriBean headsList3 = iCspiCsriSdvpServer.searchpaycspicsrihead(map3); // 远端付款
        if(headsList3==null){
        	map3.put("product", "DEFSI");
        	map3.put("type", "DF");
        	headsList3 = iCspiCsriSdvpServer.searchpaycspicsrihead(map3); // 远端付款
        }

        /* 远端收款清算路径 */
        Map<String, Object> map4 = new HashMap<String, Object>();
        map4.put("cno", cno);
        map4.put("ccy", pccy);
        map4.put("product", product);
        map4.put("type", prodtype);
        SlCspiCsriBean headsList4 = iCspiCsriSdvpServer.searchreccspicsrihead(map4); // 远端收款
        if(headsList4==null){
        	map4.put("product", "DEFSI");
        	map4.put("type", "DF");
        	headsList4 = iCspiCsriSdvpServer.searchreccspicsrihead(map4); // 远端收款
        }

        List<SlCspiCsriBean> list = new ArrayList<SlCspiCsriBean>();
        if (headsList1 != null) {
            list.add(headsList1);

            /* 查询近端付款明细CPCI内容 */
            String br = headsList1.getBr();
            Map<String, Object> upmap = new HashMap<String, Object>();
            upmap.put("br", br);
            upmap.put("cno", cno);
            upmap.put("ccy", pccy);
            upmap.put("product", StringUtils.trimToEmpty(headsList1.getProduct()));
            upmap.put("type", StringUtils.trimToEmpty(headsList1.getType()));
            List<SlCspiCsriDetailBean> closepaydetailList = iCspiCsriSdvpServer.searchcspicsridetail(upmap);
            if (closepaydetailList != null) {
                settleBean.setPayDetailList(closepaydetailList);
            }
        }
        if (headsList2 != null) {
            list.add(headsList2);

            /* 查询近端收款明细CPCI内容 */
            String br2 = headsList2.getBr();
            Map<String, Object> upmap2 = new HashMap<String, Object>();
            upmap2.put("br", br2);
            upmap2.put("cno", cno);
            upmap2.put("ccy", rccy);
            upmap2.put("product", StringUtils.trimToEmpty(headsList2.getProduct()));
            upmap2.put("type", StringUtils.trimToEmpty(headsList2.getType()));
            List<SlCspiCsriDetailBean> closerecdetailList = iCspiCsriSdvpServer.searchcsridetail(upmap2);
            if (!(StringUtil.isEmpty(closerecdetailList.get(0).getBic())
                && StringUtil.isEmpty(closerecdetailList.get(0).getC1())
                && StringUtil.isEmpty(closerecdetailList.get(0).getC2())
                && StringUtil.isEmpty(closerecdetailList.get(0).getC3())
                && StringUtil.isEmpty(closerecdetailList.get(0).getC4()))) {
                settleBean.setRecDetailList(closerecdetailList);
            }

        }
        if (headsList3 != null) {
            list.add(headsList3);

            /* 查询远端付款明细CPCI内容 */
            String br = headsList3.getBr();
            Map<String, Object> upmap3 = new HashMap<String, Object>();
            upmap3.put("br", br);
            upmap3.put("cno", cno);
            upmap3.put("ccy", rccy);
            upmap3.put("product", StringUtils.trimToEmpty(headsList3.getProduct()));
            upmap3.put("type", StringUtils.trimToEmpty(headsList3.getType()));
            List<SlCspiCsriDetailBean> remotepaydetailList = iCspiCsriSdvpServer.searchcspicsridetail(upmap3);
            if (remotepaydetailList != null) {
                settleBean.setRemotePayDetailList(remotepaydetailList);
            }
        }
        if (headsList4 != null) {
            list.add(headsList4);

            /* 查询远端收款明细CPCI内容 */
            String br4 = headsList4.getBr();
            Map<String, Object> upmap4 = new HashMap<String, Object>();
            upmap4.put("br", br4);
            upmap4.put("cno", cno);
            upmap4.put("ccy", pccy);
            upmap4.put("product", StringUtils.trimToEmpty(headsList4.getProduct()));
            upmap4.put("type", StringUtils.trimToEmpty(headsList4.getType()));
            List<SlCspiCsriDetailBean> remoterecdetailList = iCspiCsriSdvpServer.searchcsridetail(upmap4);
            if (!(StringUtil.isEmpty(remoterecdetailList.get(0).getBic())
                && StringUtil.isEmpty(remoterecdetailList.get(0).getC1())
                && StringUtil.isEmpty(remoterecdetailList.get(0).getC2())
                && StringUtil.isEmpty(remoterecdetailList.get(0).getC3())
                && StringUtil.isEmpty(remoterecdetailList.get(0).getC4()))) {
                settleBean.setRemoteRecDetailList(remoterecdetailList);
            }
        }
        if (list != null) {
            settleBean.setList(list); // 将从ifs_cspicsri_head表中查询出来的值插入
        }
        return settleBean;
    }
}