package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.ifs.model.IfsCfetsrmbBondfwd;
import com.singlee.ifs.service.IFSService;
import com.singlee.ifs.service.IfsCfetsrmbBondfwdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.Map;

/**
 * 债券远期
 *
 */
@Controller
@RequestMapping(value = "/IfsCfetsrmbBondfwdController")
public class IfsCfetsrmbBondfwdController extends CommonController {

	@Autowired
	private IfsCfetsrmbBondfwdService ifsCfetsrmbBondfwdService;

	@Autowired
	private IFSService cfetsrmbBondfwdService;
	/**
	 * 分页查询  我发起的
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchCfetsrmbBondfwdMine")
	public RetMsg<PageInfo<IfsCfetsrmbBondfwd>> searchCfetsrmbBondfwdMine(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsrmbBondfwd> page = ifsCfetsrmbBondfwdService.searchICBFPage(params,3);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 分页查询  待审批
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchCfetsrmbBondfwdUnfinished")
	public RetMsg<PageInfo<IfsCfetsrmbBondfwd>> searchCfetsrmbBondfwdUnfinished(@RequestBody Map<String, Object> params) {
		String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse + ","
				+ DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
		String[] orderStatus = approveStatus.split(",");
		params.put("approveStatusNo", orderStatus);
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsrmbBondfwd> page = ifsCfetsrmbBondfwdService.searchICBFPage(params,1);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 分页查询  已审批
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchCfetsrmbBondfwdFinished")
	public RetMsg<PageInfo<IfsCfetsrmbBondfwd>> searchCfetsrmbBondfwdFinished(@RequestBody Map<String, Object> params) {
		String approveStatus = ParameterUtil.getString(params, "approveStatus", "");
		if (StringUtil.isNullOrEmpty(approveStatus)) {
			approveStatus = DictConstants.ApproveStatus.ApprovedPass;
			approveStatus += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatus += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatus += "," + DictConstants.ApproveStatus.Verified;
			approveStatus += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			approveStatus += "," + DictConstants.ApproveStatus.Approving;
			String[] orderStatus = approveStatus.split(",");
			params.put("approveStatusNo", orderStatus);
		}
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsrmbBondfwd> page = ifsCfetsrmbBondfwdService.searchICBFPage(params,2);
		return RetMsgHelper.ok(page);
	}
	
	@ResponseBody
	@RequestMapping(value = "/searchCfetsrmbBondfwdAndFlowIdById")
	public RetMsg<IfsCfetsrmbBondfwd> searchCfetsrmbBondfwdAndFlowIdById(@RequestBody Map<String, Object> params) {
		IfsCfetsrmbBondfwd obj = ifsCfetsrmbBondfwdService.searchICBF(params);
		return RetMsgHelper.ok(obj);	
	}

	@ResponseBody
	@RequestMapping(value = "/searchCfetsrmbBondfwdByTicketId")
	public RetMsg<IfsCfetsrmbBondfwd> searchCfetsrmbBondfwdByTicketId(@RequestBody Map<String, Object> params) {
		IfsCfetsrmbBondfwd obj = ifsCfetsrmbBondfwdService.getIfsCfetsrmbBondfwdByTicketId(params);
		return RetMsgHelper.ok(obj);
	}

	/**
	 * 保存
	 * @param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/saveCfetsrmbBondfwd")
	public RetMsg<Serializable> saveCfetsrmbBondfwd(@RequestBody IfsCfetsrmbBondfwd record) {
		//1.检查会计类型
		String message = cfetsrmbBondfwdService.checkCust(record.getDealTransType(), record.getSellInst(),record.getCfetsno(), null, record.getProduct(), record.getProdType());
		if(StringUtil.isNotEmpty(message)) {//检查失败
			return RetMsgHelper.fail(message);
		}
		IfsCfetsrmbBondfwd bondfwd=ifsCfetsrmbBondfwdService.getBondById(record);
		if(bondfwd==null){
			ifsCfetsrmbBondfwdService.saveICBF(record);
			message="添加成功";
			RetMsgHelper.ok(message);
		}else{
			try {
				ifsCfetsrmbBondfwdService.updateICBF(record);
				message="修改成功";
				RetMsgHelper.ok(message);
			}catch (Exception e) {
				e.printStackTrace();
				message = "修改失败";
				return RetMsgHelper.fail(message);
			}
		}
		return StringUtil.containsIgnoreCase(message, "成功")?RetMsgHelper.ok(message):RetMsgHelper.fail(message);
	}
	
	/**
	 * 修改
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/updateCfetsrmbBondfwd")
	public RetMsg<Serializable> updateCfetsrmbBondfwd(@RequestBody Map<String, Object> params) {
		IfsCfetsrmbBondfwd ifsCfetsrmbBondfwd = new IfsCfetsrmbBondfwd();
		BeanUtil.populate(ifsCfetsrmbBondfwd, params);
		int result = ifsCfetsrmbBondfwdService.updateICBF(ifsCfetsrmbBondfwd);
		if(result > 0) {
			return RetMsgHelper.ok();
		}else {
			return RetMsgHelper.simple("error.common.0001", "操作失败！");
		}
	}
	
	/**
	 * 删除
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteCfetsrmbBondfwd")
	public RetMsg<Serializable> deleteCfetsrmbBondfwd(@RequestBody Map<String, Object> params) {
		IfsCfetsrmbBondfwd ifsCfetsrmbBondfwd = new IfsCfetsrmbBondfwd();
		BeanUtil.populate(ifsCfetsrmbBondfwd, params);
		int result = ifsCfetsrmbBondfwdService.deleteICBF(ifsCfetsrmbBondfwd);
		if(result > 0) {
			return RetMsgHelper.ok();
		}else {
			return RetMsgHelper.simple("error.common.0001", "操作失败！");
		}
	}
}
