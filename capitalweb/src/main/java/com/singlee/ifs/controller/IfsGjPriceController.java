package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.hrbreport.util.HrbReportUtils;
import com.singlee.ifs.model.FileNameList;
import com.singlee.ifs.service.ListPriceGetService;
import com.singlee.ifs.service.ListPriceJobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/IfsGjPriceController")
public class IfsGjPriceController extends CommonController {
    @Autowired
    ListPriceJobService listPriceJobService;
    @Autowired
    ListPriceGetService listPriceGetService;

    /***
     * 牌价信息分页查询
     */
    @ResponseBody
    @RequestMapping(value = "/searchPrice")
    public RetMsg<PageInfo<FileNameList>> searchPrice(@RequestBody Map<String, Object> map) {

        List<FileNameList> list = listPriceJobService.getlist(map);
        int pageNumber = (int) map.get("pageNumber");
        int pageSize = (int) map.get("pageSize");
        Page<FileNameList> page = HrbReportUtils.producePage(list, pageNumber, pageSize);

        return RetMsgHelper.ok(page);
    }

    /***
     * 牌价信息获取
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "/getPrice")
    public RetMsg<Serializable> getPrice(@RequestBody Map<String, Object> map) throws Exception {
        boolean flag = listPriceGetService.getPrice();
        return RetMsgHelper.ok(flag);
    }
}
