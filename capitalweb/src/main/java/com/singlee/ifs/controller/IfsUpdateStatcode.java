package com.singlee.ifs.controller;

import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.financial.bean.SlInthBean;
import com.singlee.financial.opics.IBaseServer;
import com.singlee.ifs.mapper.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/IfsUpdateStatcode")
public class IfsUpdateStatcode extends CommonController {
	@Autowired
	private IBaseServer baseServer;
	@Autowired
	private IfsCfetsfxSptMapper ifsCfetsfxSptMapper;
	@Autowired
	private IfsCfetsfxFwdMapper ifsCfetsfxFwdMapper;
	@Autowired
	private IfsCfetsfxSwapMapper ifsCfetsfxSwapMapper;
	@Autowired
	private IfsCfetsrmbIboMapper ifsCfetsrmbIboMapper;
	@Autowired
	private IfsCfetsfxOptionMapper ifsCfetsfxOptionMapper;
	@Autowired
	private IfsCfetsmetalGoldMapper ifsCfetsmetalGoldMapper;
	@Autowired
	private IfsCfetsfxCswapMapper ifsCfetsfxCswapMapper;
	@Autowired
	IfsCfetsrmbIrsMapper ifsCfetsrmbIrsMapper;
	@Autowired
	private IfsCfetsrmbCbtMapper ifsCfetsrmbCbtMapper;
	@Autowired
	private IfsCfetsrmbSlMapper ifsCfetsrmbSlMapper;
	@Autowired
	IfsCfetsrmbCrMapper ifsCfetsrmbCrMapper;
	@Autowired
	IfsCfetsrmbOrMapper ifsCfetsrmbOrMapper;

	IfsCfetsrmbDpMapper ifsCfetsrmbDpMapper;

	@ResponseBody
	@RequestMapping(value = "/queryStatcode")
	public RetMsg<Serializable> queryStatcode(@RequestBody Map<String, Object> map)
			throws RemoteConnectFailureException, Exception {
		List<Map<String, String>> list = (List<Map<String, String>>) map.get("queryParams");
		for (Map<String, String> mapTp : list) {
			SlInthBean inthBean = new SlInthBean();
			inthBean.setFedealno(mapTp.get("fedealno"));
			inthBean.setSeq("0");
			SlInthBean ret = baseServer.getInthStatus(inthBean);
			// 更新状态
			String prdName = (String) map.get("type");
			if (ret == null) {
				return RetMsgHelper.ok();
			}
			// 查询条件
			Map<String, Object> queryMap = new HashMap<String, Object>();
			queryMap.put("statcode", ret.getStatcode());
			queryMap.put("errorcode", ret.getErrorcode());
			queryMap.put("dealno", ret.getDealno());
			queryMap.put("fedealno", ret.getFedealno());
			if (StringUtils.equals(prdName, "外汇即期")) {
				ifsCfetsfxSptMapper.updateCfetsfxSptByFedealno(queryMap);
			} else if (StringUtils.equals(prdName, "外汇远期")) {
				ifsCfetsfxFwdMapper.updateCfetsfxFwdByFedealno(queryMap);
			} else if (StringUtils.equals(prdName, "外汇掉期")) {
				ifsCfetsfxSwapMapper.updateCfetsfxSwapByFedealno(queryMap);
			} else if (StringUtils.equals(prdName, "信用拆借")) {
				ifsCfetsrmbIboMapper.updateCfetsIboByFedealno(queryMap);
			} else if (StringUtils.equals(prdName, "黄金即期") || StringUtils.equals(prdName, "黄金远期")
					|| StringUtils.equals(prdName, "黄金掉期")) {
				ifsCfetsmetalGoldMapper.updateStatcodeByTicketId(queryMap);
			} else if (StringUtils.equals(prdName, "外汇期权")) {
				ifsCfetsfxOptionMapper.updateCfetsfxOptionByFedealno(queryMap);
			} else if (StringUtils.equals(prdName, "货币掉期")) {
				ifsCfetsfxCswapMapper.updateCfetsfxCswapByFedealno(queryMap);
			} else if (StringUtils.equals(prdName, "利率互换")) {
				ifsCfetsrmbIrsMapper.updateCfetsrmbIrsByFedealno(queryMap);
			} else if (StringUtils.equals(prdName, "现券买卖") || StringUtils.equals(prdName, "外币债")) {
				ifsCfetsrmbCbtMapper.updateCfetsrmbCbtByFedealno(queryMap);
			} else if (StringUtils.equals(prdName, "债券借贷")) {
				ifsCfetsrmbSlMapper.updateCfetsrmbSlByFedealno(queryMap);
			} else if (StringUtils.equals(prdName, "质押式回购")) {
				ifsCfetsrmbCrMapper.updateCfetsrmbCrByFedealno(queryMap);
			} else if (StringUtils.equals(prdName, "买断式回购")) {
				ifsCfetsrmbOrMapper.updateCfetsrmbOrByFedealno(queryMap);
			}
		}
		return RetMsgHelper.ok();
	}

}
