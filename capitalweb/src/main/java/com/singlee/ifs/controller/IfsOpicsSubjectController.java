package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.financial.bean.SlCoreSubject;
import com.singlee.financial.esb.dlcb.ISubjectAcupServer;
import com.singlee.ifs.model.IfsOpicsKCoreKm;
import com.singlee.ifs.service.IfsOpicsGlnoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.Map;

/**
 * OPICS记账科目Controller
 * 
 * ClassName: IfsOpicsSubjectController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason: TODO ADD REASON(可选). <br/>
 * date: 2019-7-17 下午07:38:53 <br/>
 * 
 * @author zhuxl
 * @version
 * @since JDK 1.6
 */
@Controller
@RequestMapping(value = "/IfsOpicsSubjectController")
public class IfsOpicsSubjectController {
	@Autowired
	ISubjectAcupServer subjectAcupServer;
	@Autowired
	IfsOpicsGlnoService ifsOpicsGlnoService;

	/***
	 * 分页查询
	 */
//	@ResponseBody
//	@RequestMapping(value = "/searchPageSubject")
//	public RetMsg<PageInfo<SlCoreSubject>> searchPageSubject(@RequestBody Map<String, Object> map) {
//		PageInfo<SlCoreSubject> page = subjectAcupServer.searchPageOpicsSubject(map);
//		return RetMsgHelper.ok(page);
//	}

	/**
	 * 新增
	 */
	@ResponseBody
	@RequestMapping(value = "/saveCoreSubjectAdd")
	public RetMsg<Serializable> saveCoreSubjectAdd(@RequestBody SlCoreSubject entity) {
		subjectAcupServer.insert(entity);
		return RetMsgHelper.ok();
	}

	/**
	 * 修改
	 */
	@ResponseBody
	@RequestMapping(value = "/saveCoreSubjectEdit")
	public RetMsg<Serializable> saveCoreSubjectEdit(@RequestBody SlCoreSubject entity) {
		subjectAcupServer.updateById(entity);
		return RetMsgHelper.ok();
	}

	/**
	 * 删除
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteSubject")
	public RetMsg<Serializable> deleteSubject(@RequestBody Map<String, String> map) {
		String acctno = map.get("acctno");
		subjectAcupServer.deleteById(acctno);
		return RetMsgHelper.ok();
	}
	
	@ResponseBody
	@RequestMapping(value = "/searchPageGlno")
	public RetMsg<PageInfo<IfsOpicsKCoreKm>> searchPageGlno(@RequestBody Map<String, Object> map) {
		Page<IfsOpicsKCoreKm> page = ifsOpicsGlnoService.searchPageOpicsGlno(map);
		return RetMsgHelper.ok(page);
	}
}
