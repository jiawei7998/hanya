package com.singlee.ifs.controller;

import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.financial.bean.SlBondCashBean;
import com.singlee.financial.bean.SlExposureBean;
import com.singlee.financial.bean.SlFxCashFlowBean;
import com.singlee.financial.bean.SlFxdhBean;
import com.singlee.financial.opics.IExposureServer;
import com.singlee.financial.opics.IFxdhServer;
import com.singlee.financial.page.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

/***
 *敞口
 * 
 */
@Controller
@RequestMapping(value = "/IfsExposureController")
public class IfsExposureController {
	
	@Autowired
	IExposureServer exposureServer;
	@Autowired
	IFxdhServer fxdhServer;
	/**
	 * 即期外汇敞口
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/searchFxExposure")
	public RetMsg<PageInfo<SlExposureBean>> searchFxExposure(@RequestBody Map<String, Object> map) throws RemoteConnectFailureException, Exception {
		PageInfo<SlExposureBean> page = exposureServer.getFxExposure(map);
		return RetMsgHelper.ok(page);
	}
	/**
	 * 历史即期外汇敞口
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/searchFxExposureHistory")
	public RetMsg<PageInfo<SlExposureBean>> searchFxExposureHistory(@RequestBody Map<String, Object> map) throws RemoteConnectFailureException, Exception {
		PageInfo<SlExposureBean> page = exposureServer.getFxExposureHistory(map);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 即期结售汇敞口
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/searchFxSettleExposure")
	public RetMsg<PageInfo<SlExposureBean>> searchFxSettleExposure(@RequestBody Map<String, Object> map) throws RemoteConnectFailureException, Exception {
		PageInfo<SlExposureBean> page = exposureServer.getFxSettleExposure(map);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 历史即期结售汇敞口
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/searchFxSettleExposureHistory")
	public RetMsg<PageInfo<SlExposureBean>> searchFxSettleExposureHistory(@RequestBody Map<String, Object> map) throws RemoteConnectFailureException, Exception {
		PageInfo<SlExposureBean> page = exposureServer.getFxSettleExposureHistory(map);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 远期外汇敞口
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/searchFxForwardExposure")
	public RetMsg<PageInfo<SlExposureBean>> searchFxForwardExposure(@RequestBody Map<String, Object> map) throws RemoteConnectFailureException, Exception {
		PageInfo<SlExposureBean> page = exposureServer.getFxForwardExposure(map);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 远期结售汇敞口
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/searchFxForwardSettleExposure")
	public RetMsg<PageInfo<SlExposureBean>> searchFxForwardSettleExposure(@RequestBody Map<String, Object> map) throws RemoteConnectFailureException, Exception {
		PageInfo<SlExposureBean> page = exposureServer.getFxForwardSettleExposure(map);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 掉期外汇敞口
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/searchFxSwapExposure")
	public RetMsg<PageInfo<SlExposureBean>> searchFxSwapExposure(@RequestBody Map<String, Object> map) throws RemoteConnectFailureException, Exception {
		PageInfo<SlExposureBean> page = exposureServer.getFxSwapExposure(map);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 掉期结售汇敞口
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/searchFxSwapSettleExposure")
	public RetMsg<PageInfo<SlExposureBean>> searchFxSwapSettleExposure(@RequestBody Map<String, Object> map) throws RemoteConnectFailureException, Exception {
		PageInfo<SlExposureBean> page = exposureServer.getFxSwapSettleExposure(map);
		return RetMsgHelper.ok(page);
	}
	
	
	/**
	 * 外汇现金流
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/searchFxCashFlow")
	public RetMsg<PageInfo<SlFxCashFlowBean>> searchFxCashFlow(@RequestBody Map<String, Object> map) throws RemoteConnectFailureException, Exception {
		PageInfo<SlFxCashFlowBean> page = fxdhServer.searchFxCashFlowPage(map);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 债券头寸查询
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/searchBondCashCount")
	public RetMsg<PageInfo<SlBondCashBean>> searchBondCashCount(@RequestBody Map<String, Object> map) throws RemoteConnectFailureException, Exception {
		PageInfo<SlBondCashBean> page = exposureServer.searchBondCashCount(map);
		return RetMsgHelper.ok(page);
	}
	
	
	@ResponseBody
	@RequestMapping(value = "/getFxCashFlowDetail")
	public RetMsg<List<SlFxdhBean>> getFxCashFlowDetail(@RequestBody Map<String, Object> map) throws RemoteConnectFailureException, Exception {
		List<SlFxdhBean> list=fxdhServer.getFxdhList(map);
		return RetMsgHelper.ok(list);
	}
	
	
}
