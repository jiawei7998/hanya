package com.singlee.ifs.controller;

import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.credit.pojo.SecurServerBean;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.ifs.mapper.IfsOpicsSettleManageMapper;
import com.singlee.ifs.service.IfsSecurServerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.Map;


/**
 * 限额处理Controller
 * 
 * ClassName: IfsLimitController <br/>
 * date: 2018-5-30 下午07:37:22 <br/>
 * 
 * @author yangfan
 * @version
 * @since JDK 1.6
 */
@Controller
@RequestMapping(value = "IfsSecurServerController")
public class ifsSecurServerController extends CommonController {
	
	@Autowired
	IfsSecurServerService ifsSecurServerService;
	
	@Autowired
	IfsOpicsSettleManageMapper ifsOpicsSettleManageMapper;


	@ResponseBody
	@RequestMapping(value = "/getAdjAmtList")
	public RetMsg<SecurServerBean> getAdjAmtList(@RequestBody Map<String, Object> map) {
		SecurServerBean securServerBean = ifsSecurServerService.getAdjAmtList(map);
		return RetMsgHelper.ok(securServerBean);
	}

	/**
	 * 保存 修改
	 */
	@ResponseBody
	@RequestMapping(value = "/saveByDealNo")
	public RetMsg<Serializable> saveByDealNo(@RequestBody Map<String, Object> map) {
		
		String oprTime = ifsOpicsSettleManageMapper.getCurDate();
		map.put("oprTime", oprTime);
		String operator = SlSessionHelper.getUserId();
		map.put("operator", operator);
		map.put("status", "经办");
		String existFlag = ParameterUtil.getString(map, "existFlag", "");
		if("1".equals(existFlag)){//existFlag：0数据库不存在数据，新增，1数据库存在数据修改
			ifsSecurServerService.updateByDealNo(map);
		}else{
			map.put("existFlag", "1");
			ifsSecurServerService.insertAdjAmtList(map);
		}
		return RetMsgHelper.ok();
	}

	@ResponseBody
	@RequestMapping(value = "/insertAdjAmtList")
	public RetMsg<Serializable> insertAdjAmtList(@RequestBody Map<String, Object> map) {
		ifsSecurServerService.insertAdjAmtList(map);
		return RetMsgHelper.ok();
	}


	@ResponseBody
	@RequestMapping(value = "/deleteByDealNo")
	public RetMsg<Serializable> deleteByDealNo(@RequestBody String dealno) {
		ifsSecurServerService.deleteByDealNo(dealno);
		return RetMsgHelper.ok();
	}

	
		
}