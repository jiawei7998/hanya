package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.ifs.model.IfsFxDepositout;
import com.singlee.ifs.service.IFSService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.Map;

/**
 * @Author zhangkai
 * @Description
 * @Date
 */
@Controller
@RequestMapping(value="/IfsCCYDepositOutController")
public class IfsCCYDepositOutController {

    @Autowired
    private IFSService iFSService;

    /**
     * 存放同业(外币)-查询我发起的
     *
     * @param params
     *            - 请求参数
     * @return
     * @author
     * @date 2018-3-21
     */
    @ResponseBody
    @RequestMapping(value = "/searchPageCcyDepositOutMine")
    public RetMsg<PageInfo<IfsFxDepositout>> searchPageCcyDepositOutMine(@RequestBody Map<String, Object> params) {
        params.put("isActive", DictConstants.YesNo.YES);
        params.put("userId", SlSessionHelper.getUserId());
        Page<IfsFxDepositout> page = iFSService.getDepositoutFXMinePage(params, 3);
        return RetMsgHelper.ok(page);
    }

    /**
     * 同业存放(外币)-查询 待审批
     *
     * @param params
     *            - 请求参数
     * @return
     * @author
     * @date 2018-3-23
     */
    @ResponseBody
    @RequestMapping(value = "/searchPageCcyDepositoutUnfinished")
    public RetMsg<PageInfo<IfsFxDepositout>> searchPageCcyDepositoutUnfinished(@RequestBody Map<String, Object> params) {
        String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse + ","
                + DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
        String[] orderStatus = approveStatus.split(",");
        params.put("approveStatusNo", orderStatus);
        params.put("isActive", DictConstants.YesNo.YES);
        params.put("userId", SlSessionHelper.getUserId());
        Page<IfsFxDepositout> page = iFSService.getDepositoutFXMinePage(params, 1);
        return RetMsgHelper.ok(page);
    }

    /**
     * 同业存放(外币)-查询 已审批
     *
     * @param params
     *            - 请求参数
     * @return
     * @author
     * @date 2018-3-23
     */
    @ResponseBody
    @RequestMapping(value = "/searchPageCcyDepositoutFinished")
    public RetMsg<PageInfo<IfsFxDepositout>> searchPageCcyDepositoutFinished(@RequestBody Map<String, Object> params) {
        String approveStatusNo = ParameterUtil.getString(params, "approveStatus", "");
        if (StringUtil.isNullOrEmpty(approveStatusNo)) {
            approveStatusNo = DictConstants.ApproveStatus.ApprovedPass;
            approveStatusNo += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
            approveStatusNo += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
            approveStatusNo += "," + DictConstants.ApproveStatus.Verified;
            approveStatusNo += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
            approveStatusNo += "," + DictConstants.ApproveStatus.Approving;
            String[] orderStatus = approveStatusNo.split(",");
            params.put("approveStatusNo", orderStatus);
        }
        params.put("isActive", DictConstants.YesNo.YES);
        params.put("userId", SlSessionHelper.getUserId());
        Page<IfsFxDepositout> page = iFSService.getDepositoutFXMinePage(params, 2);
        return RetMsgHelper.ok(page);
    }

    // 查询一条
    @ResponseBody
    @RequestMapping(value = "/searchCcyDepositout")
    public RetMsg<PageInfo<IfsFxDepositout>> searchCcyDepositout(@RequestBody Map<String, Object> params) {
        Page<IfsFxDepositout> page = iFSService.searchCcyDepositout(params);
        return RetMsgHelper.ok(page);
    }

    // 根据ticketid查询
    @ResponseBody
    @RequestMapping(value = "/searchByTicketId")
    public RetMsg<IfsFxDepositout> searchByTicketId(@RequestBody Map<String, Object> params) {
        IfsFxDepositout ifsFxDepositout = iFSService.searchDepositoutFXByTicketId(params);
        return RetMsgHelper.ok(ifsFxDepositout);
    }

    // 根据ticketid查询
    @ResponseBody
    @RequestMapping(value = "/searchFlowByTicketId")
    public RetMsg<IfsFxDepositout> searchFlowByTicketId(@RequestBody Map<String, Object> params) {
        IfsFxDepositout ifsFxDepositout = iFSService.searchFlowDepositoutFXByTicketId(params);
        return RetMsgHelper.ok(ifsFxDepositout);
    }


    // 新增
    @ResponseBody
    @RequestMapping(value = "/addCcyDepositout")
    public RetMsg<Serializable> addCcyDepositout(@RequestBody IfsFxDepositout record) {
        String message = iFSService.addCcyDepositout(record);
        return StringUtil.containsIgnoreCase(message, "保存成功")?RetMsgHelper.ok(message):RetMsgHelper.fail(message);
    }

    // 修改
    @ResponseBody
    @RequestMapping(value = "/editCcyDepositout")
    public RetMsg<Serializable> editCcyDepositout(@RequestBody IfsFxDepositout record) {
        iFSService.editCcyDepositout(record);
        return RetMsgHelper.ok();
    }

    //删除
    @ResponseBody
    @RequestMapping(value = "/deleteCcyDepositout")
    public RetMsg<Serializable> deleteCcyDepositout(@RequestBody Map<String, Object> map) {
        String ticketid = map.get("ticketid").toString();
        iFSService.deleteCcyDepositout(ticketid);
        return RetMsgHelper.ok();
    }
}
