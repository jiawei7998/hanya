package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.ifs.model.IfsBondPledgeBean;
import com.singlee.ifs.service.IfsBondPledgeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author zhangkai
 * @Description
 * @Date
 */
@Controller
@RequestMapping("/IfsBondPledgeController")
public class IfsBondPledgeController extends CommonController {

    @Autowired
    IfsBondPledgeService ifsBondPledgeService;

    /**
     * 分页查询
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/searchBondPledge")
    public RetMsg<PageInfo<IfsBondPledgeBean>> searchBondPledge(@RequestBody Map<String, Object> paramMap){
        Page<IfsBondPledgeBean> ifsBondPledgeBeans = ifsBondPledgeService.selectIfsCfetsrmbCrList(paramMap);
        return RetMsgHelper.ok(ifsBondPledgeBeans);
    }

    /**
     * 删除
     * @param key
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/deleteCfetsrmbCr")
    public RetMsg<Serializable> deleteCfetsrmbCr(@RequestBody IfsBondPledgeBean ifsBondPledgeBean){
        ifsBondPledgeService.deleteByPrimaryKey(ifsBondPledgeBean);
        return RetMsgHelper.ok();
    }

    /**
     * 新增
     * @param ifsBondPledgeBean
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/saveBondPLldge")
    public RetMsg<Serializable> saveBondPLldge(@RequestBody IfsBondPledgeBean ifsBondPledgeBean){
        //数据准备
        HashMap<String, Object> map = new HashMap<>();
        map.put("ticketId",ifsBondPledgeBean.getTicketId());
        map.put("bondCode",ifsBondPledgeBean.getBondCode());
        map.put("port",ifsBondPledgeBean.getPort());
        map.put("cost",ifsBondPledgeBean.getCost());
        map.put("invType",ifsBondPledgeBean.getInvType());

        IfsBondPledgeBean result = ifsBondPledgeService.selectBondPledgeByPrimaryKey(map);

        if(result == null){
            ifsBondPledgeService.insert(ifsBondPledgeBean);
        }else{
            return RetMsgHelper.simple("此交易质押券已存在");
        }
        return RetMsgHelper.ok();
    }

    /**
     * 修改
     * @param ifsBondPledgeBean
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/updateBondPLldge")
    public RetMsg<Serializable> updateBondPLldge(@RequestBody IfsBondPledgeBean ifsBondPledgeBean){
        ifsBondPledgeService.updateByPrimaryKey(ifsBondPledgeBean);
        return RetMsgHelper.ok();
    }
}
