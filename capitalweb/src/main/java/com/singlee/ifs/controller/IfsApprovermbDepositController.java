package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.ifs.model.IfsApprovermbDeposit;
import com.singlee.ifs.service.IfsApprovermbDepositService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.io.Serializable;
import java.util.Map;

/**
 * 同业存款
 */
@Controller
@RequestMapping(value = "/IfsApprovermbDepositController")
public class IfsApprovermbDepositController extends CommonController {
    
    @Autowired
    IfsApprovermbDepositService ifsApprovermbDepositService;

    /**
     * 删除成交单
     *
     *
     * */
    @ResponseBody
    @RequestMapping(value = "/deleteCfetsrmbDeposit")
    public RetMsg<Serializable> deleteCfetsrmbDeposit(@RequestBody IfsApprovermbDeposit key) {
        ifsApprovermbDepositService.deleteDeposit(key);
        return RetMsgHelper.ok();
    }
    @ResponseBody
    @RequestMapping(value = "/searchDepositByDealNo")
    public RetMsg<?> searchDepositByDealNo(@RequestBody Map<String, Object> map){
        return RetMsgHelper.ok(ifsApprovermbDepositService.getDepositById(map));
    }

    /**
     * 查询我发起的
     *
     * @param params
     *            - 请求参数
     * @return
     * @author
     * @date 2018-3-21
     */
    @ResponseBody
    @RequestMapping(value = "/searchRmbDepositMine")
    public RetMsg<PageInfo<IfsApprovermbDeposit>> searchRmbDepositMine(@RequestBody Map<String, Object> params) {
        params.put("isActive", DictConstants.YesNo.YES);
        params.put("userId", SlSessionHelper.getUserId());
        Page<IfsApprovermbDeposit> page = ifsApprovermbDepositService.getRmbDepositMinePage(params, 3);
        return RetMsgHelper.ok(page);
    }

    /**
     * 查询 待审批
     *
     * @param params
     *            - 请求参数
     * @return
     * @author
     * @date 2018-3-23
     */
    @ResponseBody
    @RequestMapping(value = "/searchPageRmbDepositUnfinished")
    public RetMsg<PageInfo<IfsApprovermbDeposit>> searchPageRmbDepositUnfinished(@RequestBody Map<String, Object> params) {
        String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse + ","
                + DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
        String[] orderStatus = approveStatus.split(",");
        params.put("approveStatusNo", orderStatus);
        params.put("isActive", DictConstants.YesNo.YES);
        params.put("userId", SlSessionHelper.getUserId());
        Page<IfsApprovermbDeposit> page = ifsApprovermbDepositService.getRmbDepositMinePage(params, 1);
        return RetMsgHelper.ok(page);
    }

    /**
     * 查询 已审批
     *
     * @param params
     *            - 请求参数
     * @return
     * @author
     * @date 2018-3-23
     */
    @ResponseBody
    @RequestMapping(value = "/searchPageRmbDepositFinished")
    public RetMsg<PageInfo<IfsApprovermbDeposit>> searchPageRmbDepositFinished(@RequestBody Map<String, Object> params) {
        String approveStatus = ParameterUtil.getString(params, "approveStatus", "");
        if (StringUtil.isNullOrEmpty(approveStatus)) {
            approveStatus = DictConstants.ApproveStatus.ApprovedPass;
            approveStatus += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
            approveStatus += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
            approveStatus += "," + DictConstants.ApproveStatus.Verified;
            approveStatus += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
            approveStatus += "," + DictConstants.ApproveStatus.Approving;
            String[] orderStatus = approveStatus.split(",");
            params.put("approveStatusNo", orderStatus);
        }
        params.put("isActive", DictConstants.YesNo.YES);
        params.put("userId", SlSessionHelper.getUserId());
        Page<IfsApprovermbDeposit> page = ifsApprovermbDepositService.getRmbDepositMinePage(params, 2);
        return RetMsgHelper.ok(page);
    }

    /**
     * 保存成交单
     * @param deposit
     * @return
     * @throws IOException
     */
    @ResponseBody
    @RequestMapping(value = "/saveDeposit")
    public RetMsg<Serializable> saveDeposit(@RequestBody IfsApprovermbDeposit deposit) throws IOException {
        String message = "";
        IfsApprovermbDeposit old = ifsApprovermbDepositService.getDepositById(deposit);
        if(old == null){
            message = ifsApprovermbDepositService.insertDeposit(deposit);
        }else{
            try {
                ifsApprovermbDepositService.updateDeposit(deposit);
                message = "修改成功";
            } catch (Exception e) {
                e.printStackTrace();
                message = "修改失败";
            }
        }
        ifsApprovermbDepositService.generateCashFlow(deposit);

        return StringUtil.containsIgnoreCase(message, "成功")? RetMsgHelper.ok(message):RetMsgHelper.fail(message);
    }


    //根据ticketId查询详情
    @ResponseBody
    @RequestMapping(value = "/searchApprovermbDepositByTicketId")
    public RetMsg<IfsApprovermbDeposit> searchApproveRmbDepositByTicketId(@RequestBody Map<String, Object> map) {
        String ticketId= (String) map.get("ticketId");
        IfsApprovermbDeposit amd = ifsApprovermbDepositService.searchApproveRmbDepositByTicketId(ticketId);
        return RetMsgHelper.ok(amd);
    }
}
