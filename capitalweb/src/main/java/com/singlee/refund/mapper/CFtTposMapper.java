package com.singlee.refund.mapper;

import com.github.pagehelper.Page;
import com.singlee.refund.model.CFtTpos;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

public interface CFtTposMapper {
    int deleteByPrimaryKey(Map<String, Object> map);

    int insert(CFtTpos record);

    int insertSelective(CFtTpos record);

    CFtTpos selectByMap(Map<String, Object> map);
    
    Page<CFtTpos> searchCFtTposPage(Map<String, Object> map,RowBounds rowBounds);
    
    Page<CFtTpos>  getTposList(Map<String, Object> params, RowBounds rowBounds);

	List<CFtTpos>  getTposListExcel(Map<String, Object> params);
	
	CFtTpos geCFtTposByContion(Map<String, Object> params);
	
	/**
	 * 获取基金持仓列表
	 * @param params
	 * @return
	 */
	List<CFtTpos> getListCFtTpos(Map<String, Object> params);

	List<CFtTpos> getHandleFundCode(Map<String, Object> params);
	
	void updateTopRdd(Map<String, Object> params);

	void updateTopUnplAmt(Map<String, Object> params);

	void updateYUnplAmtZero(Map<String, Object> params);

	void updateRevalAmt(CFtTpos ftTpos);

	/**
	 *  对账之后更新持仓信息
	 * @param params
	 */
	void updateCheckAfter(Map<String, Object> params);

    int updateByPrimaryKeySelective(CFtTpos record);

    int updateByPrimaryKey(CFtTpos record);
    
    int updateQtyByDealNo(Map<String, Object> params);

}