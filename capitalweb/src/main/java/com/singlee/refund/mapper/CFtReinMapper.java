package com.singlee.refund.mapper;

import java.util.LinkedHashMap;
import java.util.Map;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;
import com.github.pagehelper.Page;
import com.singlee.refund.model.CFtRein;

public interface CFtReinMapper {
	
    int deleteByPrimaryKey(String dealNo);

    int insert(CFtRein record);

    int insertSelective(CFtRein record);

    CFtRein selectByPrimaryKey(String dealNo);
    
    CFtRein selectByMap(Map<String, Object> map);

    CFtRein selectCFtReinById(Map<String, Object> map);
    
    //我发起
    Page<CFtRein> searchCFtReinPageMine(Map<String, Object> map,RowBounds rowBounds);
    
    //已审批
    Page<CFtRein> searchCFtReinPageFinished(Map<String, Object> map,RowBounds rowBounds);
    
    //待审批
    Page<CFtRein> searchCFtReinPageUnfinished(Map<String, Object> map,RowBounds rowBounds);
    
    /**
   	 * 总交易查询（红利再投）
   	 * @param map
   	 */
   	Page<CFtRein> searchTotalDealHLZT(Map<String, Object> params,RowBounds rowBounds);

    int updateByPrimaryKeySelective(CFtRein record);

    int updateByPrimaryKey(CFtRein record);
    
    int updateCFtReinByDealNo(String dealNo, String status,String date);
    
    LinkedHashMap<String, Object> searchProperty(@Param("sqll")String sqll);
}