package com.singlee.refund.mapper;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;
import com.github.pagehelper.Page;
import com.singlee.refund.model.CFtAfpConf;
import com.singlee.refund.model.CFtRdp;

public interface CFtRdpMapper {
	
    int deleteByPrimaryKey(String dealNo);

    int insert(CFtRdp record);

    int insertSelective(CFtRdp record);

    CFtRdp selectByPrimaryKey(String dealNo);

    CFtRdp selectCFtRdp(Map<String, Object> map);

    CFtRdp selectCFtRdpById(Map<String, Object> map);
    
    Page<CFtRdp> searchCFtRdpPageMine(Map<String, Object> map,RowBounds rowBounds);
    
    Page<CFtRdp> searchCFtRdpPageUnfinished(Map<String, Object> map,RowBounds rowBounds);
    
    Page<CFtRdp> searchCFtRdpPageFinished(Map<String, Object> map,RowBounds rowBounds);
    
    /**
   	 * 总交易查询（基金赎回）
   	 * @param map
   	 */
   	Page<CFtRdp> searchTotalDealJJSH(Map<String, Object> params,RowBounds rowBounds);

    int updateByPrimaryKeySelective(CFtRdp record);

    int updateByPrimaryKey(CFtRdp record);
    
    int updateCFtRdpByDealNo(String dealNo, String status,String date);
    
    LinkedHashMap<String, Object> searchProperty(@Param("sqll")String sqll);
}