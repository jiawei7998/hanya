package com.singlee.refund.mapper;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;
import com.github.pagehelper.Page;
import com.singlee.refund.model.CFtAfpConf;

public interface CFtAfpConfMapper {
    int deleteByPrimaryKey(String dealNo);

    int insert(CFtAfpConf record);

    int insertSelective(CFtAfpConf record);

    CFtAfpConf selectByPrimaryKey(String dealNo);
    
    CFtAfpConf selectCFtAfpConfByDealNo(Map<String,Object> map);

    CFtAfpConf selectCFtAfpConfById(Map<String,Object> map);
    
    List<CFtAfpConf> selectListByFundCode(String fundCode);
    
    Page<CFtAfpConf> searchCFtAfpConfPage(Map<String, Object> map,RowBounds rowBounds);
    
    Page<CFtAfpConf> searchCFtAfpConfPageMine(Map<String, Object> map,RowBounds rowBounds);
    
    Page<CFtAfpConf> searchCFtAfpConfPageUnfinished(Map<String, Object> map,RowBounds rowBounds);
    
    Page<CFtAfpConf> searchCFtAfpConfPageFinished(Map<String, Object> map,RowBounds rowBounds);
    
    /**
   	 * 总交易查询（基金申购确认）
   	 * @param map
   	 */
   	Page<CFtAfpConf> searchTotalDealJJSGQR(Map<String, Object> params,RowBounds rowBounds);

    int updateByPrimaryKeySelective(CFtAfpConf record);

    int updateByPrimaryKey(CFtAfpConf record);
    
    int updateAfpConfByDealNo(String dealNo, String status,String date);
    
    LinkedHashMap<String, Object> searchProperty(@Param("sqll")String sqll);
}