package com.singlee.refund.mapper;

import com.github.pagehelper.Page;
import com.singlee.refund.model.CFtInfo;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

public interface CFtInfoMapper {

	int deleteByPrimaryKey(String fundCode);

	int insert(CFtInfo record);

	int insertSelective(CFtInfo record);

	CFtInfo selectByPrimaryKey(String fundCode);

	List<CFtInfo> selectListBymanagcomp(String managcomp);

	Page<CFtInfo> searchCFtInfoPage(Map<String, Object> map, RowBounds rowBounds);

	int updateByPrimaryKeySelective(CFtInfo record);

	int updateByPrimaryKey(CFtInfo record);

	String getNm();

	Integer getCount(Map<String, Object> map);

}