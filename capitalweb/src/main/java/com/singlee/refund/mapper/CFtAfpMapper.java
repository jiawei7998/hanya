package com.singlee.refund.mapper;

import com.github.pagehelper.Page;
import com.singlee.refund.model.CFtAfp;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public interface CFtAfpMapper  {
	
    int deleteByPrimaryKey(String dealNo);

    int insert(CFtAfp record);

    int insertSelective(CFtAfp record);

    CFtAfp selectByPrimaryKey(String dealNo);
    
    CFtAfp selectBydealno(String dealNo);

    CFtAfp searchCFtAfpById(Map<String,Object> map);
    
    CFtAfp selectTaskIdByPrimaryKey(Map<String,Object> map);
    
    List<CFtAfp> selectListByFundCode(String fundCode);
    
    Page<CFtAfp> searchCFtAfpPage(Map<String, Object> map,RowBounds rowBounds);
    
    Page<CFtAfp> searchCFtAfpPageMine(Map<String, Object> map,RowBounds rowBounds);
    
    Page<CFtAfp> searchCFtAfpPageUnfinished(Map<String, Object> map,RowBounds rowBounds);
    
    Page<CFtAfp> searchCFtAfpPageFinished(Map<String, Object> map,RowBounds rowBounds);
    
    /**
	 * 总交易查询（基金申购和复核）
	 * @param map
	 */
	Page<CFtAfp> searchTotalDealJJSGANDFH(Map<String, Object> params,RowBounds rowBounds);

    int updateByPrimaryKeySelective(CFtAfp record);

    int updateByPrimaryKey(CFtAfp record);
    
    int updateAfpByID(String dealNo, String status,String date);
    
    LinkedHashMap<String, Object> searchProperty(@Param("sqll")String sqll);
}