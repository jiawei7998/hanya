package com.singlee.refund.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.refund.model.FtpManage;

public interface FtpManageMapper {
	
    int deleteByPrimaryKey(Short id);

    int insert(FtpManage record);

    int insertSelective(FtpManage record);

    FtpManage selectByPrimaryKey(Short id);
    
    Page<FtpManage> searchFtpManagePage(Map<String, Object> map,RowBounds rowBounds);

    int updateByPrimaryKeySelective(FtpManage record);

    int updateByPrimaryKey(FtpManage record);
}