package com.singlee.refund.mapper;

import java.util.LinkedHashMap;
import java.util.Map;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;
import com.github.pagehelper.Page;
import com.singlee.refund.model.CFtRedd;

public interface CFtReddMapper {
    int deleteByPrimaryKey(String dealNo);

    int insert(CFtRedd record);

    int insertSelective(CFtRedd record);

    CFtRedd selectByPrimaryKey(String dealNo);
    
    CFtRedd selectByMap(Map<String, Object> map);

    CFtRedd selectCFtReddById(Map<String, Object> map);
    
    //我发起
    Page<CFtRedd> searchCFtReddPageMine(Map<String, Object> map,RowBounds rowBounds);
    
    //已审批
    Page<CFtRedd> searchCFtReddPageFinished(Map<String, Object> map,RowBounds rowBounds);
    
    //待审批
    Page<CFtRedd> searchCFtReddPageUnfinished(Map<String, Object> map,RowBounds rowBounds);
    
    /**
   	 * 总交易查询（现金分红）
   	 * @param map
   	 */
   	Page<CFtRedd> searchTotalDealXJFH(Map<String, Object> params,RowBounds rowBounds);

    int updateByPrimaryKeySelective(CFtRedd record);

    int updateByPrimaryKey(CFtRedd record);
    
    int updateCFtReddByDealNo(String dealNo, String status,String date);
    
    LinkedHashMap<String, Object> searchProperty(@Param("sqll")String sqll);
}