package com.singlee.refund.mapper;

import com.singlee.capital.common.spring.mybatis.MyMapper;
import com.singlee.refund.model.FtFundManager;

public interface FtFundManagerMapper  extends MyMapper<FtFundManager> {

}