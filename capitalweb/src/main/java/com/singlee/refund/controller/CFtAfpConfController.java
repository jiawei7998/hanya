package com.singlee.refund.controller;

import java.io.Serializable;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.refund.model.CFtAfp;
import com.singlee.refund.model.CFtAfpConf;
import com.singlee.refund.service.CFtAfpConfService;

/**
 * 基金申购确认
 *
 */
@Controller
@RequestMapping(value = "/CFtAfpConfController")
public class CFtAfpConfController extends CommonController {

	@Autowired
	private CFtAfpConfService cFtAfpConfService;
	
	/**
	 * 我发起的
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchCFtAfpConfListMine")
	public RetMsg<PageInfo<CFtAfpConf>> searchCFtAfpConfListMine(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<CFtAfpConf> page = cFtAfpConfService.searchCFtAfpConfList(params,3);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 待审批的
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchCFtAfpConfListUnfinished")
	public RetMsg<PageInfo<CFtAfpConf>> searchCFtAfpConfListUnfinished(@RequestBody Map<String, Object> params) {
		String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse + ","
				+ DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
		String[] orderStatus = approveStatus.split(",");
		params.put("approveStatusNo", orderStatus);
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<CFtAfpConf> page = cFtAfpConfService.searchCFtAfpConfList(params,1);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 已审批的
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchCFtAfpConfListFinished")
	public RetMsg<PageInfo<CFtAfpConf>> searchCFtAfpConfListFinished(@RequestBody Map<String, Object> params) {
		String approveStatus = ParameterUtil.getString(params, "approveStatus", "");
		if (StringUtil.isNullOrEmpty(approveStatus)) {
			approveStatus = DictConstants.ApproveStatus.ApprovedPass;
			approveStatus += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatus += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatus += "," + DictConstants.ApproveStatus.Verified;
			approveStatus += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			approveStatus += "," + DictConstants.ApproveStatus.Approving;
			String[] orderStatus = approveStatus.split(",");
			params.put("approveStatusNo", orderStatus);
		}
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<CFtAfpConf> page = cFtAfpConfService.searchCFtAfpConfList(params,2);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 单条查询
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchCFtAfpConfByDealNo")
	public RetMsg<CFtAfpConf> searchCFtAfpConfByDealNo(@RequestBody Map<String, Object> map) {
		CFtAfpConf cFtAfpConf = cFtAfpConfService.searchCFtAfpConfByMap(map);
		return RetMsgHelper.ok(cFtAfpConf);
	}

	@ResponseBody
	@RequestMapping(value = "/searchCFtAfpConfById")
	public RetMsg<CFtAfpConf> searchCFtAfpConfById(@RequestBody Map<String, Object> map) {
		CFtAfpConf cFtAfpConf = cFtAfpConfService.searchCFtAfpConfById(map);
		return RetMsgHelper.ok(cFtAfpConf);
	}
	
	/**
	 * 保存或者修改
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/saveOrUpdateCFtAfpConf")
	public RetMsg<Serializable> saveOrUpdateCFtAfpConf(@RequestBody Map<String,Object> map){
		CFtAfpConf cFtAfpConf=new CFtAfpConf();
		BeanUtil.populate(cFtAfpConf, map);
		int result = cFtAfpConfService.saveOrUpdateCFtAfpConf(cFtAfpConf);
		if(result > 0) {
			return RetMsgHelper.ok();
		}else {
			return RetMsgHelper.simple("error.common.0001", "操作失败！");
		}
	}
	
	/**
	 * 删除
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteCFtAfpConf")
	public RetMsg<Serializable> deleteCFtAfpConf(@RequestBody Map<String, Object> params) {
		String dealNo=(String) params.get("dealNo");
		if(dealNo == null) {
			return RetMsgHelper.ok("删除失败！参数错误！");
		}else {
			CFtAfpConf obj = cFtAfpConfService.searchCFtAfpConfByDealNo(dealNo);
			if("3".equals(obj.getApproveStatus())) {
				int result = cFtAfpConfService.deleteCFtAfpConf(dealNo);
				if(result > 0) {
					return RetMsgHelper.ok();
				}else {
					return RetMsgHelper.ok("删除失败！");
				}
			}else {
				return RetMsgHelper.ok("该审批状态下不允许删除！");
			}
		}
		
		
		
	}
	
}
