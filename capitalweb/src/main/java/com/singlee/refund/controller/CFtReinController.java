package com.singlee.refund.controller;

import java.io.Serializable;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.refund.model.CFtRein;
import com.singlee.refund.service.CFtReinService;

/**
 * 红利再投
 *
 */
@Controller
@RequestMapping(value = "/CFtReinController")
public class CFtReinController extends CommonController {

	@Autowired
	private CFtReinService cFtReinService;
	
	/**
	 * 分页查询  我发起的
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchCFtReinListMine")
	public RetMsg<PageInfo<CFtRein>> searchCFtReinListMine(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<CFtRein> page = cFtReinService.searchCFtReinList(params,3);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 分页查询  待审批
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchCFtReinListUnfinished")
	public RetMsg<PageInfo<CFtRein>> searchCFtReinListUnfinished(@RequestBody Map<String, Object> params) {
		String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse + ","
				+ DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
		String[] orderStatus = approveStatus.split(",");
		params.put("approveStatusNo", orderStatus);
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<CFtRein> page = cFtReinService.searchCFtReinList(params,1);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 分页查询  已审批
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchCFtReinListFinished")
	public RetMsg<PageInfo<CFtRein>> searchCFtReinListFinished(@RequestBody Map<String, Object> params) {
		String approveStatus = ParameterUtil.getString(params, "approveStatus", "");
		if (StringUtil.isNullOrEmpty(approveStatus)) {
			approveStatus = DictConstants.ApproveStatus.ApprovedPass;
			approveStatus += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatus += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatus += "," + DictConstants.ApproveStatus.Verified;
			approveStatus += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			approveStatus += "," + DictConstants.ApproveStatus.Approving;
			String[] orderStatus = approveStatus.split(",");
			params.put("approveStatusNo", orderStatus);
		}
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<CFtRein> page = cFtReinService.searchCFtReinList(params,2);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 单条查询
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchCFtReinByDealNo")
	public RetMsg<CFtRein> searchCFtReinByDealNo(@RequestBody Map<String, Object> map) {
		CFtRein cFtRein = cFtReinService.selectCFtReinByMap(map);
		return RetMsgHelper.ok(cFtRein);
	}

	@ResponseBody
	@RequestMapping(value = "/searchCFtReinById")
	public RetMsg<CFtRein> searchCFtReinById(@RequestBody Map<String, Object> map) {
		CFtRein cFtRein = cFtReinService.selectCFtReinById(map);
		return RetMsgHelper.ok(cFtRein);
	}
	
	/**
	 * 保存或者修改
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/saveOrUpdateCFtRein")
	public RetMsg<Serializable> saveOrUpdateCFtRein(@RequestBody Map<String,Object> map){
		CFtRein cFtRein=new CFtRein();
		BeanUtil.populate(cFtRein, map);
		int result = cFtReinService.saveOrUpdateCFtRein(cFtRein);
		if(result > 0) {
			return RetMsgHelper.ok(true);
		}else {
			return RetMsgHelper.simple("error.common.0001", "操作失败！");
		}
	}
	
	/**
	 * 删除
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteCFtRein")
	public RetMsg<Serializable> deleteCFtRein(@RequestBody Map<String, Object> params) {
		String dealNo=(String) params.get("dealNo");
		if(dealNo == null) {
			return RetMsgHelper.ok("删除失败！参数错误！");
		}else {
			CFtRein obj = cFtReinService.selectCFtReinByDealNo(dealNo);
			if("3".equals(obj.getApproveStatus())) {
				int result = cFtReinService.deleteCFtRein(dealNo);
				if(result > 0) {
					return RetMsgHelper.ok();
				}else {
					return RetMsgHelper.ok("删除失败！");
				}
			}else {
				return RetMsgHelper.ok("该审批状态下不允许删除！");
			}
		}
	}
	
}
