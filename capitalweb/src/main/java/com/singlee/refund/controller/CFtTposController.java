package com.singlee.refund.controller;

import java.util.Date;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.financial.common.util.DataUtils;
import com.singlee.refund.model.CFtRedd;
import com.singlee.refund.model.CFtTpos;
import com.singlee.refund.service.CFtTposService;

/**
 * 基金持仓
 *
 */
@Controller
@RequestMapping(value = "/CFtTposController")
public class CFtTposController extends CommonController {

	@Autowired
	private CFtTposService cFtTposService;

	/**
	 * 分页查询
	 * 
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchCFtTposList")
	public RetMsg<PageInfo<CFtTpos>> searchCFtTposList(@RequestBody Map<String, Object> params) {
		Page<CFtTpos> page = cFtTposService.searchCFtTposList(params);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 单条持仓详情
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchCFtTposByMap")
	public RetMsg<CFtTpos> searchCFtTposByMap(@RequestBody Map<String, Object> map) {
		map.put("postdate", DataUtils.formatDate(new Date(), 1, "yyyy-MM-dd"));
		CFtTpos cFtTpos = cFtTposService.searchCFtTposByMap(map);
		return RetMsgHelper.ok(cFtTpos);
	}
	
}
