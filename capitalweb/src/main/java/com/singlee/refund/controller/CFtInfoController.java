package com.singlee.refund.controller;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.refund.mapper.CFtAfpConfMapper;
import com.singlee.refund.mapper.CFtAfpMapper;
import com.singlee.refund.model.CFtAfp;
import com.singlee.refund.model.CFtInfo;
import com.singlee.refund.service.CFtInfoService;

/**
 * 基金基础信息
 *
 */
@Controller
@RequestMapping(value = "/CFtInfoController")
public class CFtInfoController extends CommonController {

	@Autowired
	private CFtInfoService cFtInfoService;
	@Autowired
	private CFtAfpMapper cFtAfpMapper;

	/**
	 * 分页查询
	 * 
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchCFtInfoList")
	public RetMsg<PageInfo<CFtInfo>> searchCFtInfoList(@RequestBody Map<String, Object> params) {
		Page<CFtInfo> page = cFtInfoService.searchCFtInfoList(params);
		return RetMsgHelper.ok(page);
	}
	
	
	/**
	 * 保存或者修改
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/saveOrUpdateCFtInfo")
	public RetMsg<Serializable> saveOrUpdateCFtInfo(@RequestBody Map<String,Object> map){
		CFtInfo fundInfo=new CFtInfo();
		BeanUtil.populate(fundInfo, map);
		int result = cFtInfoService.saveOrUpdateCFtInfo(fundInfo);
		if(result > 0) {
			return RetMsgHelper.ok();
		}else {
			if(result == -99) {
				return RetMsgHelper.simple("error.common.0001", "操作失败！该基金申购确认成功，不可修改分红方式或基货类型！");
			}else {
				return RetMsgHelper.simple("error.common.0001", "操作失败！");
			}
			
		}
	}

	@ResponseBody
	@RequestMapping(value = "/deleteCFtInfo")
	public RetMsg<Serializable> deleteCFtInfo(@RequestBody Map<String, Object> params) {
		String fundCode = (String)params.get("fundCode");
		if(fundCode == null || "".equals(fundCode)) {
			return RetMsgHelper.ok("删除失败！参数错误！");
		}
		List<CFtAfp> list = cFtAfpMapper.selectListByFundCode(fundCode);
		if(list.size() > 0) {
			return RetMsgHelper.ok("删除失败！该基金已参与交易！");
		}
		int result = cFtInfoService.deleteCFtInfo(fundCode);
		if(result > 0) {
			return RetMsgHelper.ok();
		}else {
			return RetMsgHelper.ok("删除失败！");
		}
	}
}
