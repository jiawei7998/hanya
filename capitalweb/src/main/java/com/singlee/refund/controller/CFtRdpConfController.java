package com.singlee.refund.controller;

import java.io.Serializable;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.refund.model.CFtRdpConf;
import com.singlee.refund.service.CFtRdpConfService;

/**
 * 基金赎回确认
 *
 */
@Controller
@RequestMapping(value = "/CFtRdpConfController")
public class CFtRdpConfController extends CommonController {

	@Autowired
	private CFtRdpConfService cFtRdpConfService;
	
	/**
	 * 我发起的
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchCFtRdpConfListMine")
	public RetMsg<PageInfo<CFtRdpConf>> searchCFtRdpConfListMine(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<CFtRdpConf> page = cFtRdpConfService.searchCFtRdpConfList(params,3);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 待审批的
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchCFtRdpConfListUnfinished")
	public RetMsg<PageInfo<CFtRdpConf>> searchCFtRdpConfListUnfinished(@RequestBody Map<String, Object> params) {
		String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse + ","
				+ DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
		String[] orderStatus = approveStatus.split(",");
		params.put("approveStatusNo", orderStatus);
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<CFtRdpConf> page = cFtRdpConfService.searchCFtRdpConfList(params,1);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 已审批的
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchCFtRdpConfListFinished")
	public RetMsg<PageInfo<CFtRdpConf>> searchCFtRdpConfListFinished(@RequestBody Map<String, Object> params) {
		String approveStatus = ParameterUtil.getString(params, "approveStatus", "");
		if (StringUtil.isNullOrEmpty(approveStatus)) {
			approveStatus = DictConstants.ApproveStatus.ApprovedPass;
			approveStatus += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatus += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatus += "," + DictConstants.ApproveStatus.Verified;
			approveStatus += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			approveStatus += "," + DictConstants.ApproveStatus.Approving;
			String[] orderStatus = approveStatus.split(",");
			params.put("approveStatusNo", orderStatus);
		}
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<CFtRdpConf> page = cFtRdpConfService.searchCFtRdpConfList(params,2);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 单条查询
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchCFtRdpConfByDealNo")
	public RetMsg<CFtRdpConf> searchCFtRdpConfByDealNo(@RequestBody Map<String, Object> map) {
		CFtRdpConf cFtRdpConf = cFtRdpConfService.searchCFtRdpConfByMap(map);
		return RetMsgHelper.ok(cFtRdpConf);
	}

	@ResponseBody
	@RequestMapping(value = "/searchCFtRdpConfById")
	public RetMsg<CFtRdpConf> searchCFtRdpConfById(@RequestBody Map<String, Object> map) {
		CFtRdpConf cFtRdpConf = cFtRdpConfService.selectCFtRdpConfById(map);
		return RetMsgHelper.ok(cFtRdpConf);
	}
	
	/**
	 * 保存或者修改
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/saveOrUpdateCFtRdpConf")
	public RetMsg<Serializable> saveOrUpdateCFtRdpConf(@RequestBody Map<String,Object> map){
		CFtRdpConf cFtRdpConf=new CFtRdpConf();
		BeanUtil.populate(cFtRdpConf, map);
		int result = cFtRdpConfService.saveOrUpdateCFtRdpConf(cFtRdpConf);
		if(result > 0) {
			return RetMsgHelper.ok();
		}else {
			return RetMsgHelper.simple("error.common.0001", "操作失败！");
		}
	}
	
	/**
	 * 删除
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteCFtRdpConf")
	public RetMsg<Serializable> deleteCFtRdpConf(@RequestBody Map<String, Object> params) {
		String dealNo=(String) params.get("dealNo");
		if(dealNo == null) {
			return RetMsgHelper.ok("删除失败！参数错误！");
		}else {
			CFtRdpConf obj = cFtRdpConfService.searchCFtRdpConfByDealNo(dealNo);
			if("3".equals(obj.getApproveStatus())) {
				int result = cFtRdpConfService.deleteCFtRdpConf(dealNo);
				if(result > 0) {
					return RetMsgHelper.ok();
				}else {
					return RetMsgHelper.ok("删除失败！");
				}
			}else {
				return RetMsgHelper.ok("该审批状态下不允许删除！");
			}
		}
		
		
	}
	
}
