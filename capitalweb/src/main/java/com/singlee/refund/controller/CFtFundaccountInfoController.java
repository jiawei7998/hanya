package com.singlee.refund.controller;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.singlee.capital.common.util.JY;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.refund.mapper.CFtInfoMapper;
import com.singlee.refund.model.CFtFundaccountInfo;
import com.singlee.refund.model.CFtInfo;
import com.singlee.refund.service.CFtFundaccountInfoService;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletResponse;

/**
 * 基金账号信息
 *
 */
@Controller
@RequestMapping(value = "/CFtFundaccountInfoController")
public class CFtFundaccountInfoController extends CommonController {

	@Autowired
	private CFtFundaccountInfoService cFtFundaccountInfoService;
	@Autowired
	private CFtInfoMapper cFtInfoMapper;

	/**
	 * 分页查询
	 * 
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchCFtFundaccountInfoList")
	public RetMsg<PageInfo<CFtFundaccountInfo>> searchCFtFundaccountInfoList(@RequestBody Map<String, Object> params) {
		Page<CFtFundaccountInfo> page = cFtFundaccountInfoService.searchCFtFundaccountInfoList(params);
		return RetMsgHelper.ok(page);
	}
	
	
	/**
	 * 保存或者修改
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/saveOrUpdateCFtFundaccountInfo", method = RequestMethod.POST, produces = "text/plain;charset=UTF-8")
	public String saveOrUpdateCFtFundaccountInfo(MultipartHttpServletRequest request,  HttpServletResponse response){

		StringBuffer sb = new StringBuffer();
		try {
			cFtFundaccountInfoService.saveOrUpdateCFtFundaccountInfo(request);
		} catch (Exception e) {
			JY.error(e);
			sb.append(e.getMessage());
		}
		return StringUtils.isEmpty(sb.toString()) ? "保存成功" : sb.toString();
	}

	@ResponseBody
	@RequestMapping(value = "/deleteCFtFundaccountInfo")
	public RetMsg<Serializable> deleteCFtFundaccountInfo(@RequestBody Map<String, Object> params) {
		String managcomp = (String)params.get("managcomp");
		if(managcomp == null || "".equals(managcomp)) {
			return RetMsgHelper.ok("删除失败！参数错误！");
		}
		List<CFtInfo> list = cFtInfoMapper.selectListBymanagcomp(managcomp);
		if(list.size() > 0) {
			return RetMsgHelper.ok("删除失败！该基金管理人信息被使用！");
		}
		int result = cFtFundaccountInfoService.deleteCFtFundaccountInfo(managcomp);
		if(result > 0) {
			return RetMsgHelper.ok();
		}else {
			return RetMsgHelper.ok("删除失败！");
		}
	}
}
