package com.singlee.refund.controller;

import java.io.Serializable;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.refund.model.CFtRdp;
import com.singlee.refund.service.CFtRdpService;

/**
 * 基金赎回
 *
 */
@Controller
@RequestMapping(value = "/CFtRdpController")
public class CFtRdpController extends CommonController {

	@Autowired
	private CFtRdpService cFtRdpService;
	
	/**
	 * 分页查询  我发起的
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchCFtRdpListMine")
	public RetMsg<PageInfo<CFtRdp>> searchCFtRdpListMine(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<CFtRdp> page = cFtRdpService.searchCFtRdpList(params,3);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 分页查询  待审批
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchCFtRdpListUnfinished")
	public RetMsg<PageInfo<CFtRdp>> searchCFtRdpListUnfinished(@RequestBody Map<String, Object> params) {
		String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse + ","
				+ DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
		String[] orderStatus = approveStatus.split(",");
		params.put("approveStatusNo", orderStatus);
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<CFtRdp> page = cFtRdpService.searchCFtRdpList(params,1);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 分页查询  已审批
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchCFtRdpListFinished")
	public RetMsg<PageInfo<CFtRdp>> searchCFtRdpListFinished(@RequestBody Map<String, Object> params) {
		String approveStatus = ParameterUtil.getString(params, "approveStatus", "");
		if (StringUtil.isNullOrEmpty(approveStatus)) {
			approveStatus = DictConstants.ApproveStatus.ApprovedPass;
			approveStatus += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatus += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatus += "," + DictConstants.ApproveStatus.Verified;
			approveStatus += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			approveStatus += "," + DictConstants.ApproveStatus.Approving;
			String[] orderStatus = approveStatus.split(",");
			params.put("approveStatusNo", orderStatus);
		}
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<CFtRdp> page = cFtRdpService.searchCFtRdpList(params,2);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 单条查询
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchCFtRdpByDealNo")
	public RetMsg<CFtRdp> searchCFtRdpByDealNo(@RequestBody Map<String, Object> map) {
		CFtRdp cFtRdp = cFtRdpService.selectByMap(map);
		return RetMsgHelper.ok(cFtRdp);
	}

	@ResponseBody
	@RequestMapping(value = "/searchCFtRdpById")
	public RetMsg<CFtRdp> searchCFtRdpById(@RequestBody Map<String, Object> map) {
		CFtRdp cFtRdp = cFtRdpService.selectCFtRdpById(map);
		return RetMsgHelper.ok(cFtRdp);
	}
	
	/**
	 * 根据主键单条查询
	 * @param
	 * @return
	 */
	   @ResponseBody
	    @RequestMapping(value = "/selectBydealNo")
	public RetMsg<CFtRdp> selectBydealNo(@RequestBody String dealNo){
	    CFtRdp cFtRdp = cFtRdpService.selectBydealNo(dealNo);
	    return RetMsgHelper.ok(cFtRdp);
	}
	
	/**
	 * 保存或者修改
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/saveOrUpdateCFtRdp")
	public RetMsg<Serializable> saveOrUpdateCFtRdp(@RequestBody Map<String,Object> map){
		CFtRdp cFtRdp=new CFtRdp();
		BeanUtil.populate(cFtRdp, map);
		int result = cFtRdpService.saveOrUpdateCFtRdp(cFtRdp);
		if(result > 0) {
			return RetMsgHelper.ok();
		}else {
			return RetMsgHelper.simple("error.common.0001", "操作失败！");
		}
	}
	
	/**
	 * 删除
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteCFtRdp")
	public RetMsg<Serializable> deleteCFtRdp(@RequestBody Map<String, Object> params) {
		String dealNo=(String) params.get("dealNo");
		if(dealNo == null) {
			return RetMsgHelper.ok("删除失败！参数错误！");
		}else {
			CFtRdp obj = cFtRdpService.selectBydealNo(dealNo);
			if("3".equals(obj.getApproveStatus())) {
				int result = cFtRdpService.deleteCFtRdp(dealNo);
				if(result > 0) {
					return RetMsgHelper.ok();
				}else {
					return RetMsgHelper.ok("删除失败！");
				}
			}else {
				return RetMsgHelper.ok("该审批状态下不允许删除！");
			}
		}
	}
	
}
