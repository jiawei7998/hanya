package com.singlee.refund.controller;

import java.io.Serializable;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.refund.model.CFtAfp;
import com.singlee.refund.model.CFtRdp;
import com.singlee.refund.service.CFtAfpService;

/**
 * 基金申购
 *
 */
@Controller
@RequestMapping(value = "/CFtAfpController")
public class CFtAfpController extends CommonController {

	@Autowired
	private CFtAfpService cFtAfpService;
	
	/**
	 * 分页查询  我发起的
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchCFtAfpListMine")
	public RetMsg<PageInfo<CFtAfp>> searchCFtAfpListMine(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<CFtAfp> page = cFtAfpService.searchCFtAfpList(params,3);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 分页查询  待审批
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchCFtAfpListUnfinished")
	public RetMsg<PageInfo<CFtAfp>> searchCFtAfpListUnfinished(@RequestBody Map<String, Object> params) {
		String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse + ","
				+ DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
		String[] orderStatus = approveStatus.split(",");
		params.put("approveStatusNo", orderStatus);
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<CFtAfp> page = cFtAfpService.searchCFtAfpList(params,1);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 分页查询  已审批
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchCFtAfpListFinished")
	public RetMsg<PageInfo<CFtAfp>> searchCFtAfpListFinished(@RequestBody Map<String, Object> params) {
		String approveStatus = ParameterUtil.getString(params, "approveStatus", "");
		if (StringUtil.isNullOrEmpty(approveStatus)) {
			approveStatus = DictConstants.ApproveStatus.ApprovedPass;
			approveStatus += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatus += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatus += "," + DictConstants.ApproveStatus.Verified;
			approveStatus += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			approveStatus += "," + DictConstants.ApproveStatus.Approving;
			String[] orderStatus = approveStatus.split(",");
			params.put("approveStatusNo", orderStatus);
		}
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<CFtAfp> page = cFtAfpService.searchCFtAfpList(params,2);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 单条查询
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchCFtAfpByDealNo")
	public RetMsg<CFtAfp> searchCFtAfpByDealNo(@RequestBody Map<String, Object> map) {
		CFtAfp cFtAfp = cFtAfpService.selectTaskIdByMap(map);
		return RetMsgHelper.ok(cFtAfp);
	}


	@ResponseBody
	@RequestMapping(value = "/searchCFtAfpById")
	public RetMsg<CFtAfp> searchCFtAfpById(@RequestBody Map<String, Object> map) {
		CFtAfp cFtAfp = cFtAfpService.searchCFtAfpById(map);
		return RetMsgHelper.ok(cFtAfp);
	}
	
	
	/**
     * 单条查询
     * @param map
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/searchAfpByDealNo")
    public RetMsg<CFtAfp> selectAfpByDealNo(@RequestBody String dealno) {
        CFtAfp cFtAfp = cFtAfpService.selectTaskIdByDealNo(dealno);
        return RetMsgHelper.ok(cFtAfp);
    }
	
	/**
	 * 保存或者修改
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/saveOrUpdateCFtAfp")
	public RetMsg<Serializable> saveOrUpdateCFtAfp(@RequestBody Map<String,Object> map){
		CFtAfp cFtAfp=new CFtAfp();
		BeanUtil.populate(cFtAfp, map);
		int result = cFtAfpService.saveOrUpdateCFtAfp(cFtAfp);
		if(result > 0) {
			return RetMsgHelper.ok();
		}else {
			return RetMsgHelper.simple("error.common.0001", "操作失败！");
		}
	}
	
	/**
	 * 删除
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteCFtAfp")
	public RetMsg<Serializable> deleteCFtAfp(@RequestBody Map<String, Object> params) {
		String dealNo=(String) params.get("dealNo");
		if(dealNo == null) {
			return RetMsgHelper.ok("删除失败！参数错误！");
		}else {
			CFtAfp obj = cFtAfpService.selectTaskIdByDealNo(dealNo);
			if("3".equals(obj.getApproveStatus())) {
				int result = cFtAfpService.deleteCFtAfp(dealNo);
				if(result > 0) {
					return RetMsgHelper.ok();
				}else {
					return RetMsgHelper.ok("删除失败！");
				}
			}else {
				return RetMsgHelper.ok("删除失败！该审批状态下不允许删除！");
			}
		}
	}
	
}
