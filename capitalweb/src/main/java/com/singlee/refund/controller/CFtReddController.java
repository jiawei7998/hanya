package com.singlee.refund.controller;

import java.io.Serializable;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.refund.model.CFtRedd;
import com.singlee.refund.service.CFtReddService;

/**
 * 现金分红
 *
 */
@Controller
@RequestMapping(value = "/CFtReddController")
public class CFtReddController extends CommonController {

	@Autowired
	private CFtReddService cFtReddService;
	
	/**
	 * 分页查询  我发起的
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchCFtReddListMine")
	public RetMsg<PageInfo<CFtRedd>> searchCFtReddListMine(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<CFtRedd> page = cFtReddService.searchCFtReddList(params,3);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 分页查询  待审批
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchCFtReddListUnfinished")
	public RetMsg<PageInfo<CFtRedd>> searchCFtReddListUnfinished(@RequestBody Map<String, Object> params) {
		String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse + ","
				+ DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
		String[] orderStatus = approveStatus.split(",");
		params.put("approveStatusNo", orderStatus);
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<CFtRedd> page = cFtReddService.searchCFtReddList(params,1);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 分页查询  已审批
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchCFtReddListFinished")
	public RetMsg<PageInfo<CFtRedd>> searchCFtReddListFinished(@RequestBody Map<String, Object> params) {
		String approveStatus = ParameterUtil.getString(params, "approveStatus", "");
		if (StringUtil.isNullOrEmpty(approveStatus)) {
			approveStatus = DictConstants.ApproveStatus.ApprovedPass;
			approveStatus += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatus += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatus += "," + DictConstants.ApproveStatus.Verified;
			approveStatus += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			approveStatus += "," + DictConstants.ApproveStatus.Approving;
			String[] orderStatus = approveStatus.split(",");
			params.put("approveStatusNo", orderStatus);
		}
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<CFtRedd> page = cFtReddService.searchCFtReddList(params,2);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 单条查询
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchCFtReddByDealNo")
	public RetMsg<CFtRedd> searchCFtReddByDealNo(@RequestBody Map<String, Object> map) {
		CFtRedd cFtRedd = cFtReddService.selectCFtReddByMap(map);
		return RetMsgHelper.ok(cFtRedd);
	}

	@ResponseBody
	@RequestMapping(value = "/searchCFtReddById")
	public RetMsg<CFtRedd> searchCFtReddById(@RequestBody Map<String, Object> map) {
		CFtRedd cFtRedd = cFtReddService.selectCFtReddById(map);
		return RetMsgHelper.ok(cFtRedd);
	}
	
	/**
	 * 保存或者修改
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/saveOrUpdateCFtRedd")
	public RetMsg<Serializable> saveOrUpdateCFtRedd(@RequestBody Map<String,Object> map){
		CFtRedd cFtRedd=new CFtRedd();
		BeanUtil.populate(cFtRedd, map);
		int result = cFtReddService.saveOrUpdateCFtRedd(cFtRedd);
		if(result > 0) {
			return RetMsgHelper.ok(true);
		}else {
			return RetMsgHelper.simple("error.common.0001", "操作失败！");
		}
	}
	
	/**
	 * 删除
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteCFtRedd")
	public RetMsg<Serializable> deleteCFtRedd(@RequestBody Map<String, Object> params) {
		String dealNo=(String) params.get("dealNo");
		if(dealNo == null) {
			return RetMsgHelper.ok("删除失败！参数错误！");
		}else {
			CFtRedd obj = cFtReddService.selectCFtReddByDealNo(dealNo);
			if("3".equals(obj.getApproveStatus())) {
				int result = cFtReddService.deleteCFtRedd(dealNo);
				if(result > 0) {
					return RetMsgHelper.ok();
				}else {
					return RetMsgHelper.ok("删除失败！");
				}
			}else {
				return RetMsgHelper.ok("该审批状态下不允许删除！");
			}
		}
	}
	
}
