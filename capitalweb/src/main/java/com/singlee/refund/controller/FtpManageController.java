package com.singlee.refund.controller;

import java.io.Serializable;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.refund.model.FtpManage;
import com.singlee.refund.service.FtpManageService;

/**
 * FTP价格管理
 *
 */
@Controller
@RequestMapping(value = "/FtpManageController")
public class FtpManageController extends CommonController {

	@Autowired
	private FtpManageService ftpManageService;
	
	/**
	 * 分页查询
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchFtpManageList")
	public RetMsg<PageInfo<FtpManage>> searchFtpManageList(@RequestBody Map<String, Object> params) {
		Page<FtpManage> page = ftpManageService.searchFtpManageList(params);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 保存或者修改
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/saveOrUpdateFtpManage")
	public RetMsg<Serializable> saveOrUpdateFtpManage(@RequestBody Map<String,Object> map){
		FtpManage ftpManage=new FtpManage();
		BeanUtil.populate(ftpManage, map);
		int result = ftpManageService.saveOrUpdateFtpManage(ftpManage);
		if(result > 0) {
			return RetMsgHelper.ok(true);
		}else {
			return RetMsgHelper.ok(false);
		}
	}
	
	/**
	 * 删除
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteFtpManage")
	public RetMsg<Serializable> deleteFtpManage(@RequestBody Map<String, Short> params) {
		int result = ftpManageService.deleteFtpManage(params.get("id"));
		if(result > 0) {
			return RetMsgHelper.ok(true);
		}else {
			return RetMsgHelper.ok(false);
		}
	}
	
}
