package com.singlee.refund.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "FT_INFO")
public class CFtInfo implements Serializable {

	// 基金代码
	private String fundCode;
	// 基金简称
	private String fundName;
	// 基金全称
	private String fundFullName;
	// 基金类型
	private String fundType;
	// 投资目标分类
	private String invType;
	// 成立日期
	private String estDate;
	// 存续期限
	private String term;
	// 基金管理人
	private String managComp;
	// 基金托管人
	private String custodian;
	// 基金经理
	private String managerMen;
	// 信息披露者
	private String infoDiscloser;
	// 发行方式
	private String issueType;
	// 发行协调人
	private String distCoordinator;
	// 基金总份额
	private BigDecimal totalQty;
	// 流通份额
	private BigDecimal trueQty;
	// 币种代码
	private String ccy;
	// 管理费率%
	private BigDecimal managRate;
	// 托管费率%
	private BigDecimal trustRate;
	// 申购费率
	private BigDecimal applyRate;
	// 赎回费率
	private BigDecimal redeemRate;
	// 份额结转方式
	private String coverMode;
	// 份额结转日期
	private String coverDay;
	// 备注信息
	private String remark;
	// 基金经理联系方式
	private String managerMenPhone;
	// 管理人名称
	private String managCompNm;

	private String partyAcccode;

	private String partyAccname;

	private String partyBankcode;

	private String partyBankname;
	// 起息规则
	private String vType;

	private String currencyType;
	// 分红方式
	private String bonusWay;
	// 债券基金估值类型 3-成本法 4-市值法
	private String bondType;
	//到期日期
	private String maDate;
	//风险类型
	private String riskType;

	//基金净值类型（0：净值型，1：非净值型）
    private String netWorthType;

    //资管产品类型
    private String manageProductType;

	/**
	 * 最早可赎回日期
	 */
	private String earliestRedemptionDate;
	@Transient
	private String managCname;

	private static final long serialVersionUID = 1L;

	public String getEarliestRedemptionDate() {
		return earliestRedemptionDate;
	}

	public void setEarliestRedemptionDate(String earliestRedemptionDate) {
		this.earliestRedemptionDate = earliestRedemptionDate;
	}

	public String getManageProductType() {
		return manageProductType;
	}

	public void setManageProductType(String manageProductType) {
		this.manageProductType = manageProductType;
	}

	public String getFundCode() {
		return fundCode;
	}

	public void setFundCode(String fundCode) {
		this.fundCode = fundCode;
	}

	public String getFundName() {
		return fundName;
	}

	public void setFundName(String fundName) {
		this.fundName = fundName;
	}

	public String getFundFullName() {
		return fundFullName;
	}

	public void setFundFullName(String fundFullName) {
		this.fundFullName = fundFullName;
	}

	public String getFundType() {
		return fundType;
	}

	public void setFundType(String fundType) {
		this.fundType = fundType;
	}

	public String getInvType() {
		return invType;
	}

	public void setInvType(String invType) {
		this.invType = invType;
	}

	public String getEstDate() {
		return estDate;
	}

	public void setEstDate(String estDate) {
		this.estDate = estDate;
	}

	public String getTerm() {
		return term;
	}

	public void setTerm(String term) {
		this.term = term;
	}

	public String getManagComp() {
		return managComp;
	}

	public void setManagComp(String managComp) {
		this.managComp = managComp;
	}

	public String getCustodian() {
		return custodian;
	}

	public void setCustodian(String custodian) {
		this.custodian = custodian;
	}

	public String getManagerMen() {
		return managerMen;
	}

	public void setManagerMen(String managerMen) {
		this.managerMen = managerMen;
	}

	public String getInfoDiscloser() {
		return infoDiscloser;
	}

	public void setInfoDiscloser(String infoDiscloser) {
		this.infoDiscloser = infoDiscloser;
	}

	public String getIssueType() {
		return issueType;
	}

	public void setIssueType(String issueType) {
		this.issueType = issueType;
	}

	public String getDistCoordinator() {
		return distCoordinator;
	}

	public void setDistCoordinator(String distCoordinator) {
		this.distCoordinator = distCoordinator;
	}

	public BigDecimal getTotalQty() {
		return totalQty;
	}

	public void setTotalQty(BigDecimal totalQty) {
		this.totalQty = totalQty;
	}

	public BigDecimal getTrueQty() {
		return trueQty;
	}

	public void setTrueQty(BigDecimal trueQty) {
		this.trueQty = trueQty;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public BigDecimal getManagRate() {
		return managRate;
	}

	public void setManagRate(BigDecimal managRate) {
		this.managRate = managRate;
	}

	public BigDecimal getTrustRate() {
		return trustRate;
	}

	public void setTrustRate(BigDecimal trustRate) {
		this.trustRate = trustRate;
	}

	public BigDecimal getApplyRate() {
		return applyRate;
	}

	public void setApplyRate(BigDecimal applyRate) {
		this.applyRate = applyRate;
	}

	public BigDecimal getRedeemRate() {
		return redeemRate;
	}

	public void setRedeemRate(BigDecimal redeemRate) {
		this.redeemRate = redeemRate;
	}

	public String getCoverMode() {
		return coverMode;
	}

	public void setCoverMode(String coverMode) {
		this.coverMode = coverMode;
	}

	public String getCoverDay() {
		return coverDay;
	}

	public void setCoverDay(String coverDay) {
		this.coverDay = coverDay;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getManagerMenPhone() {
		return managerMenPhone;
	}

	public void setManagerMenPhone(String managerMenPhone) {
		this.managerMenPhone = managerMenPhone;
	}

	public String getManagCompNm() {
		return managCompNm;
	}

	public void setManagCompNm(String managCompNm) {
		this.managCompNm = managCompNm;
	}

	public String getPartyAcccode() {
		return partyAcccode;
	}

	public void setPartyAcccode(String partyAcccode) {
		this.partyAcccode = partyAcccode;
	}

	public String getPartyAccname() {
		return partyAccname;
	}

	public void setPartyAccname(String partyAccname) {
		this.partyAccname = partyAccname;
	}

	public String getPartyBankcode() {
		return partyBankcode;
	}

	public void setPartyBankcode(String partyBankcode) {
		this.partyBankcode = partyBankcode;
	}

	public String getPartyBankname() {
		return partyBankname;
	}

	public void setPartyBankname(String partyBankname) {
		this.partyBankname = partyBankname;
	}

	public String getvType() {
		return vType;
	}

	public void setvType(String vType) {
		this.vType = vType;
	}

	public String getCurrencyType() {
		return currencyType;
	}

	public void setCurrencyType(String currencyType) {
		this.currencyType = currencyType;
	}

	public String getBonusWay() {
		return bonusWay;
	}

	public void setBonusWay(String bonusWay) {
		this.bonusWay = bonusWay;
	}

	public String getBondType() {
		return bondType;
	}

	public void setBondType(String bondType) {
		this.bondType = bondType;
	}

	public String getMaDate() {
		return maDate;
	}

	public void setMaDate(String maDate) {
		this.maDate = maDate;
	}

	public String getRiskType() {
		return riskType;
	}

	public void setRiskType(String riskType) {
		this.riskType = riskType;
	}

	public String getManagCname() {
		return managCname;
	}

	public void setManagCname(String managCname) {
		this.managCname = managCname;
	}

	public String getNetWorthType() {
		return netWorthType;
	}

	public void setNetWorthType(String netWorthType) {
		this.netWorthType = netWorthType;
	}

	@Override
	public String toString() {
		return "CFtInfo{" +
				"fundCode='" + fundCode + '\'' +
				", fundName='" + fundName + '\'' +
				", fundFullName='" + fundFullName + '\'' +
				", fundType='" + fundType + '\'' +
				", invType='" + invType + '\'' +
				", estDate='" + estDate + '\'' +
				", term='" + term + '\'' +
				", managComp='" + managComp + '\'' +
				", custodian='" + custodian + '\'' +
				", managerMen='" + managerMen + '\'' +
				", infoDiscloser='" + infoDiscloser + '\'' +
				", issueType='" + issueType + '\'' +
				", distCoordinator='" + distCoordinator + '\'' +
				", totalQty=" + totalQty +
				", trueQty=" + trueQty +
				", ccy='" + ccy + '\'' +
				", managRate=" + managRate +
				", trustRate=" + trustRate +
				", applyRate=" + applyRate +
				", redeemRate=" + redeemRate +
				", coverMode='" + coverMode + '\'' +
				", coverDay='" + coverDay + '\'' +
				", remark='" + remark + '\'' +
				", managerMenPhone='" + managerMenPhone + '\'' +
				", managCompNm='" + managCompNm + '\'' +
				", partyAcccode='" + partyAcccode + '\'' +
				", partyAccname='" + partyAccname + '\'' +
				", partyBankcode='" + partyBankcode + '\'' +
				", partyBankname='" + partyBankname + '\'' +
				", vType='" + vType + '\'' +
				", currencyType='" + currencyType + '\'' +
				", bonusWay='" + bonusWay + '\'' +
				", bondType='" + bondType + '\'' +
				", maDate='" + maDate + '\'' +
				", riskType='" + riskType + '\'' +
				", netWorthType='" + netWorthType + '\'' +
				", managCname='" + managCname + '\'' +
				'}';
	}
}