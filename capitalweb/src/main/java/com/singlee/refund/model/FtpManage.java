package com.singlee.refund.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "FTP_MANAGE")
public class FtpManage implements Serializable {
	
	//唯一Id
    private Short id;
    //生效日期
    private String effectiveDate;
    //曲线类型
    private String ftpType;
    //一天   
    private BigDecimal oneDay;
    //七天
    private BigDecimal sevenDay;
    //十四天
    private BigDecimal fourteenDay;
    //一个月
    private BigDecimal oneMonth;
    //三个月
    private BigDecimal threeMonth;
    //六个月
    private BigDecimal sixMonth;
    //九个月
    private BigDecimal nineMonth;
    //一年
    private BigDecimal oneYear;
    //两年
    private BigDecimal twoYear;
    //三年
    private BigDecimal threeYear;
    //四年
    private BigDecimal fourYear;
    //五年
    private BigDecimal fiveYear;
    //六年
    private BigDecimal sixYear;
    //七年
    private BigDecimal sevenYear;
    //八年
    private BigDecimal eightYear;
    //九年
    private BigDecimal nineeYear;
    //十年
    private BigDecimal tenYear;
    //十五年
    private BigDecimal fifteenYear;
    //二十年
    private BigDecimal twentyYear;
    //三十年
    private BigDecimal thirtyYear;
    //是否生效
    private String yesOrNo;
    //操作时间
    private String operTime;

    private static final long serialVersionUID = 1L;

    public Short getId() {
        return id;
    }

    public void setId(Short id) {
        this.id = id;
    }

    public String getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(String effectiveDate) {
        this.effectiveDate = effectiveDate == null ? null : effectiveDate.trim();
    }

    public String getFtpType() {
        return ftpType;
    }

    public void setFtpType(String ftpType) {
        this.ftpType = ftpType == null ? null : ftpType.trim();
    }

    public BigDecimal getOneDay() {
        return oneDay;
    }

    public void setOneDay(BigDecimal oneDay) {
        this.oneDay = oneDay;
    }

    public BigDecimal getSevenDay() {
        return sevenDay;
    }

    public void setSevenDay(BigDecimal sevenDay) {
        this.sevenDay = sevenDay;
    }

    public BigDecimal getFourteenDay() {
        return fourteenDay;
    }

    public void setFourteenDay(BigDecimal fourteenDay) {
        this.fourteenDay = fourteenDay;
    }

    public BigDecimal getOneMonth() {
        return oneMonth;
    }

    public void setOneMonth(BigDecimal oneMonth) {
        this.oneMonth = oneMonth;
    }

    public BigDecimal getThreeMonth() {
        return threeMonth;
    }

    public void setThreeMonth(BigDecimal threeMonth) {
        this.threeMonth = threeMonth;
    }

    public BigDecimal getSixMonth() {
        return sixMonth;
    }

    public void setSixMonth(BigDecimal sixMonth) {
        this.sixMonth = sixMonth;
    }

    public BigDecimal getNineMonth() {
        return nineMonth;
    }

    public void setNineMonth(BigDecimal nineMonth) {
        this.nineMonth = nineMonth;
    }

    public BigDecimal getOneYear() {
        return oneYear;
    }

    public void setOneYear(BigDecimal oneYear) {
        this.oneYear = oneYear;
    }

    public BigDecimal getTwoYear() {
        return twoYear;
    }

    public void setTwoYear(BigDecimal twoYear) {
        this.twoYear = twoYear;
    }

    public BigDecimal getThreeYear() {
        return threeYear;
    }

    public void setThreeYear(BigDecimal threeYear) {
        this.threeYear = threeYear;
    }

    public BigDecimal getFourYear() {
        return fourYear;
    }

    public void setFourYear(BigDecimal fourYear) {
        this.fourYear = fourYear;
    }

    public BigDecimal getFiveYear() {
        return fiveYear;
    }

    public void setFiveYear(BigDecimal fiveYear) {
        this.fiveYear = fiveYear;
    }

    public BigDecimal getSixYear() {
        return sixYear;
    }

    public void setSixYear(BigDecimal sixYear) {
        this.sixYear = sixYear;
    }

    public BigDecimal getSevenYear() {
        return sevenYear;
    }

    public void setSevenYear(BigDecimal sevenYear) {
        this.sevenYear = sevenYear;
    }

    public BigDecimal getEightYear() {
        return eightYear;
    }

    public void setEightYear(BigDecimal eightYear) {
        this.eightYear = eightYear;
    }

    public BigDecimal getNineeYear() {
        return nineeYear;
    }

    public void setNineeYear(BigDecimal nineeYear) {
        this.nineeYear = nineeYear;
    }

    public BigDecimal getTenYear() {
        return tenYear;
    }

    public void setTenYear(BigDecimal tenYear) {
        this.tenYear = tenYear;
    }

    public BigDecimal getFifteenYear() {
        return fifteenYear;
    }

    public void setFifteenYear(BigDecimal fifteenYear) {
        this.fifteenYear = fifteenYear;
    }

    public BigDecimal getTwentyYear() {
        return twentyYear;
    }

    public void setTwentyYear(BigDecimal twentyYear) {
        this.twentyYear = twentyYear;
    }

    public BigDecimal getThirtyYear() {
        return thirtyYear;
    }

    public void setThirtyYear(BigDecimal thirtyYear) {
        this.thirtyYear = thirtyYear;
    }

    public String getYesOrNo() {
        return yesOrNo;
    }

    public void setYesOrNo(String yesOrNo) {
        this.yesOrNo = yesOrNo == null ? null : yesOrNo.trim();
    }

    public String getOperTime() {
        return operTime;
    }

    public void setOperTime(String operTime) {
        this.operTime = operTime == null ? null : operTime.trim();
    }
}