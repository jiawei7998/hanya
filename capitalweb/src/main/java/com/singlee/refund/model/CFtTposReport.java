package com.singlee.refund.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Transient;

public class CFtTposReport implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String fundCode;// 基金代码
	private String fundName;// 基金名称
	private String sponinst;// 机构号
	private String sponinstName;// 机构名称
	private String managCompNm;// 基金公司
	private String ccy;// 币种
	private String currencyType;// 债（货）基类型
	private BigDecimal cost;// 持仓余额
	private int holdingDay;// 持仓天数
	private BigDecimal avgCost;// 平均持仓余额
	private BigDecimal priceGrow;// 净值增长
	private BigDecimal intAmt;// 分红
	private BigDecimal unpAmt;// 累计转投金额
	private BigDecimal income;// 区间收益--万元
	private BigDecimal yield;// 区间收益率
	private BigDecimal ftpCost;// FTP成本
	private BigDecimal avgFtpCost;// 平均FTP成本
	private BigDecimal ftpProfit;// FTP利润
	private BigDecimal ftpProfitRate;// FTP利润率
	private BigDecimal weight;// 风险权重
	private BigDecimal riskExpAmt;// 风险资产
	private BigDecimal avgUtQty;// 平均持仓份额
	private BigDecimal utQty;// 持仓份额

	@Transient
	private String afpDate;// 首次申购日期
	
	public static long getSerialVersionUID() {
        return serialVersionUID;
    }

	public String getFundCode() {
		return fundCode;
	}

	public void setFundCode(String fundCode) {
		this.fundCode = fundCode;
	}

	public String getFundName() {
		return fundName;
	}

	public void setFundName(String fundName) {
		this.fundName = fundName;
	}

	public String getSponinst() {
		return sponinst;
	}

	public void setSponinst(String sponinst) {
		this.sponinst = sponinst;
	}

	public String getSponinstName() {
		return sponinstName;
	}

	public void setSponinstName(String sponinstName) {
		this.sponinstName = sponinstName;
	}

	public String getManagCompNm() {
		return managCompNm;
	}

	public void setManagCompNm(String managCompNm) {
		this.managCompNm = managCompNm;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getCurrencyType() {
		return currencyType;
	}

	public void setCurrencyType(String currencyType) {
		this.currencyType = currencyType;
	}

	public BigDecimal getCost() {
		return cost;
	}

	public void setCost(BigDecimal cost) {
		this.cost = cost;
	}

	public int getHoldingDay() {
		return holdingDay;
	}

	public void setHoldingDay(int holdingDay) {
		this.holdingDay = holdingDay;
	}

	public BigDecimal getAvgCost() {
		return avgCost;
	}

	public void setAvgCost(BigDecimal avgCost) {
		this.avgCost = avgCost;
	}

	public BigDecimal getPriceGrow() {
		return priceGrow;
	}

	public void setPriceGrow(BigDecimal priceGrow) {
		this.priceGrow = priceGrow;
	}

	public BigDecimal getIntAmt() {
		return intAmt;
	}

	public void setIntAmt(BigDecimal intAmt) {
		this.intAmt = intAmt;
	}

	public BigDecimal getUnpAmt() {
		return unpAmt;
	}

	public void setUnpAmt(BigDecimal unpAmt) {
		this.unpAmt = unpAmt;
	}

	public BigDecimal getIncome() {
		return income;
	}

	public void setIncome(BigDecimal income) {
		this.income = income;
	}

	public BigDecimal getYield() {
		return yield;
	}

	public void setYield(BigDecimal yield) {
		this.yield = yield;
	}

	public BigDecimal getFtpCost() {
		return ftpCost;
	}

	public void setFtpCost(BigDecimal ftpCost) {
		this.ftpCost = ftpCost;
	}

	public BigDecimal getAvgFtpCost() {
		return avgFtpCost;
	}

	public void setAvgFtpCost(BigDecimal avgFtpCost) {
		this.avgFtpCost = avgFtpCost;
	}

	public BigDecimal getFtpProfit() {
		return ftpProfit;
	}

	public void setFtpProfit(BigDecimal ftpProfit) {
		this.ftpProfit = ftpProfit;
	}

	public BigDecimal getFtpProfitRate() {
		return ftpProfitRate;
	}

	public void setFtpProfitRate(BigDecimal ftpProfitRate) {
		this.ftpProfitRate = ftpProfitRate;
	}

	public BigDecimal getWeight() {
		return weight;
	}

	public void setWeight(BigDecimal weight) {
		this.weight = weight;
	}

	public BigDecimal getRiskExpAmt() {
		return riskExpAmt;
	}

	public void setRiskExpAmt(BigDecimal riskExpAmt) {
		this.riskExpAmt = riskExpAmt;
	}

	public BigDecimal getAvgUtQty() {
		return avgUtQty;
	}

	public void setAvgUtQty(BigDecimal avgUtQty) {
		this.avgUtQty = avgUtQty;
	}

	public BigDecimal getUtQty() {
		return utQty;
	}

	public void setUtQty(BigDecimal utQty) {
		this.utQty = utQty;
	}

	public String getAfpDate() {
		return afpDate;
	}

	public void setAfpDate(String afpDate) {
		this.afpDate = afpDate;
	}

}