package com.singlee.refund.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "FT_TPOS")
public class CFtTpos implements Serializable {

	private String prdNo;// 产品代码
	private String invtype;// 四分类
	private String fundCode;// 基金代码

	@Transient
	private String fundName;
	@Transient
	private String fundType;
	@Transient
	private String currencyType;// 普通还是净值

	private String cNo;// 客户编号
	private String ccy;// 币种
	private String sponInst;// 机构号

	private BigDecimal utQty;// 持仓份额
	private BigDecimal utAmt;// 赎回成本
	private BigDecimal ncvAmt;// 累计已转l投金额（货币基金）
	private BigDecimal yield8;// 万份收益率（货币基金）净值（债券基金）
	private BigDecimal shAmt;// 累计已分红金额（债券基金）
	private String postdate;// 账务日期

	private BigDecimal revalAmt;// 估值

	private BigDecimal yUnplAmt;// 累计未付收益
	private BigDecimal tUnplAmt;// 累计总估值
	private BigDecimal newQty;// 赎回份额-NEW
	private BigDecimal appQty;// 赎回份额-APP

	private BigDecimal cost;// 基金投资成本（不包含未确认）
	private BigDecimal ucCost;// 基金未确认成本
	private BigDecimal afpAmt;// 累计申购金额
	private BigDecimal handleAmt;// 累计手续费（债券基金）
	private BigDecimal rdpIntAmt;// 赎回累计红利

	private String isAssmt;// 是否估值
	private String afpDate;// 首次申购日

	@Transient
	private String cName;
	@Transient
	private String managComp;
	@Transient
	private String managCompNm;
	@Transient
	private String sponInstName;
	@Transient
	private String coverMode;
	@Transient
	private String coverDay;
	@Transient
	private String bonusWay;// 分红方式
	@Transient
	private String investAmt;
	@Transient
	private BigDecimal ftpPrice;
	@Transient
	private BigDecimal ftpAcup;

	@Transient
	private String fundFullName;// 基金全称
	@Transient
	private String estDate;// 成立日期
	@Transient
	private BigDecimal totalQty;// 基金规模
	@Transient
	private String managerMen;// 基金经理
	@Transient
	private String managerMenPhone;// 基金经理联系方式

	private static final long serialVersionUID = 1L;

	public String getPrdNo() {
		return prdNo;
	}

	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}

	public String getInvtype() {
		return invtype;
	}

	public void setInvtype(String invtype) {
		this.invtype = invtype;
	}

	public String getFundCode() {
		return fundCode;
	}

	public void setFundCode(String fundCode) {
		this.fundCode = fundCode;
	}

	public String getFundName() {
		return fundName;
	}

	public void setFundName(String fundName) {
		this.fundName = fundName;
	}

	public String getFundType() {
		return fundType;
	}

	public void setFundType(String fundType) {
		this.fundType = fundType;
	}

	public String getCurrencyType() {
		return currencyType;
	}

	public void setCurrencyType(String currencyType) {
		this.currencyType = currencyType;
	}

	public String getcNo() {
		return cNo;
	}

	public void setcNo(String cNo) {
		this.cNo = cNo;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getSponInst() {
		return sponInst;
	}

	public void setSponInst(String sponInst) {
		this.sponInst = sponInst;
	}

	public BigDecimal getUtQty() {
		return utQty;
	}

	public void setUtQty(BigDecimal utQty) {
		this.utQty = utQty;
	}

	public BigDecimal getUtAmt() {
		return utAmt;
	}

	public void setUtAmt(BigDecimal utAmt) {
		this.utAmt = utAmt;
	}

	public BigDecimal getNcvAmt() {
		return ncvAmt;
	}

	public void setNcvAmt(BigDecimal ncvAmt) {
		this.ncvAmt = ncvAmt;
	}

	public BigDecimal getYield8() {
		return yield8;
	}

	public void setYield8(BigDecimal yield8) {
		this.yield8 = yield8;
	}

	public BigDecimal getShAmt() {
		return shAmt;
	}

	public void setShAmt(BigDecimal shAmt) {
		this.shAmt = shAmt;
	}

	public String getPostdate() {
		return postdate;
	}

	public void setPostdate(String postdate) {
		this.postdate = postdate;
	}

	public BigDecimal getRevalAmt() {
		return revalAmt;
	}

	public void setRevalAmt(BigDecimal revalAmt) {
		this.revalAmt = revalAmt;
	}

	public BigDecimal getyUnplAmt() {
		return yUnplAmt;
	}

	public void setyUnplAmt(BigDecimal yUnplAmt) {
		this.yUnplAmt = yUnplAmt;
	}

	public BigDecimal gettUnplAmt() {
		return tUnplAmt;
	}

	public void settUnplAmt(BigDecimal tUnplAmt) {
		this.tUnplAmt = tUnplAmt;
	}

	public BigDecimal getNewQty() {
		return newQty;
	}

	public void setNewQty(BigDecimal newQty) {
		this.newQty = newQty;
	}

	public BigDecimal getAppQty() {
		return appQty;
	}

	public void setAppQty(BigDecimal appQty) {
		this.appQty = appQty;
	}

	public BigDecimal getCost() {
		return cost;
	}

	public void setCost(BigDecimal cost) {
		this.cost = cost;
	}

	public BigDecimal getUcCost() {
		return ucCost;
	}

	public void setUcCost(BigDecimal ucCost) {
		this.ucCost = ucCost;
	}

	public BigDecimal getAfpAmt() {
		return afpAmt;
	}

	public void setAfpAmt(BigDecimal afpAmt) {
		this.afpAmt = afpAmt;
	}

	public BigDecimal getHandleAmt() {
		return handleAmt;
	}

	public void setHandleAmt(BigDecimal handleAmt) {
		this.handleAmt = handleAmt;
	}

	public BigDecimal getRdpIntAmt() {
		return rdpIntAmt;
	}

	public void setRdpIntAmt(BigDecimal rdpIntAmt) {
		this.rdpIntAmt = rdpIntAmt;
	}

	public String getIsAssmt() {
		return isAssmt;
	}

	public void setIsAssmt(String isAssmt) {
		this.isAssmt = isAssmt;
	}

	public String getAfpDate() {
		return afpDate;
	}

	public void setAfpDate(String afpDate) {
		this.afpDate = afpDate;
	}

	public String getcName() {
		return cName;
	}

	public void setcName(String cName) {
		this.cName = cName;
	}

	public String getManagComp() {
		return managComp;
	}

	public void setManagComp(String managComp) {
		this.managComp = managComp;
	}

	public String getManagCompNm() {
		return managCompNm;
	}

	public void setManagCompNm(String managCompNm) {
		this.managCompNm = managCompNm;
	}

	public String getSponInstName() {
		return sponInstName;
	}

	public void setSponInstName(String sponInstName) {
		this.sponInstName = sponInstName;
	}

	public String getCoverMode() {
		return coverMode;
	}

	public void setCoverMode(String coverMode) {
		this.coverMode = coverMode;
	}

	public String getCoverDay() {
		return coverDay;
	}

	public void setCoverDay(String coverDay) {
		this.coverDay = coverDay;
	}

	public String getBonusWay() {
		return bonusWay;
	}

	public void setBonusWay(String bonusWay) {
		this.bonusWay = bonusWay;
	}

	public String getInvestAmt() {
		return investAmt;
	}

	public void setInvestAmt(String investAmt) {
		this.investAmt = investAmt;
	}

	public BigDecimal getFtpPrice() {
		return ftpPrice;
	}

	public void setFtpPrice(BigDecimal ftpPrice) {
		this.ftpPrice = ftpPrice;
	}

	public BigDecimal getFtpAcup() {
		return ftpAcup;
	}

	public void setFtpAcup(BigDecimal ftpAcup) {
		this.ftpAcup = ftpAcup;
	}

	public String getFundFullName() {
		return fundFullName;
	}

	public void setFundFullName(String fundFullName) {
		this.fundFullName = fundFullName;
	}

	public String getEstDate() {
		return estDate;
	}

	public void setEstDate(String estDate) {
		this.estDate = estDate;
	}

	public BigDecimal getTotalQty() {
		return totalQty;
	}

	public void setTotalQty(BigDecimal totalQty) {
		this.totalQty = totalQty;
	}

	public String getManagerMen() {
		return managerMen;
	}

	public void setManagerMen(String managerMen) {
		this.managerMen = managerMen;
	}

	public String getManagerMenPhone() {
		return managerMenPhone;
	}

	public void setManagerMenPhone(String managerMenPhone) {
		this.managerMenPhone = managerMenPhone;
	}

}