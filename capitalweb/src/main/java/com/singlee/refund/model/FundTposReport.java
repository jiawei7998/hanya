package com.singlee.refund.model;

import java.io.Serializable;

public class FundTposReport implements Serializable {

	private static final long serialVersionUID = 1L;

	//@ExcelProperty("货币市场基金份额投资（资产方）")
	private String sumQty;

	//@ExcelProperty("货币市场基金存放款项（负债方）")
	private String sumAmt;

	//@ExcelProperty("货币市场基金持有的同业存单（负债方）")
	private String sum;

	public String getSumQty() {
		return sumQty;
	}

	public void setSumQty(String sumQty) {
		this.sumQty = sumQty;
	}

	public String getSumAmt() {
		return sumAmt;
	}

	public void setSumAmt(String sumAmt) {
		this.sumAmt = sumAmt;
	}

	public String getSum() {
		return sum;
	}

	public void setSum(String sum) {
		this.sum = sum;
	}

}
