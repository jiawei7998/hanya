package com.singlee.refund.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "FT_FUNDACCOUNT_INFO")
public class CFtFundaccountInfo implements Serializable {
	
	//基金管理人编号
    @Id
    private String managcomp;
    //基金管理人简称
    private String managcompnm;
    //基金管理人全称
    private String managcompfnm;
    //基金账号
    private String fundaccount;
    //交易账号
    private String fundtradeaccount;
    //户名
    private String fundaccountname;
    //开户银行名称
    private String fundbankname;
    //人行支付系统行号
    private String rhbankno;
    //银行账号
    private String fundbankno;
    //影像上传
    private byte[] movie;

    private static final long serialVersionUID = 1L;

    public String getManagcomp() {
        return managcomp;
    }

    public void setManagcomp(String managcomp) {
        this.managcomp = managcomp == null ? null : managcomp.trim();
    }

    public String getManagcompnm() {
        return managcompnm;
    }

    public void setManagcompnm(String managcompnm) {
        this.managcompnm = managcompnm == null ? null : managcompnm.trim();
    }

    public String getManagcompfnm() {
        return managcompfnm;
    }

    public void setManagcompfnm(String managcompfnm) {
        this.managcompfnm = managcompfnm;
    }

    public String getFundaccount() {
        return fundaccount;
    }

    public void setFundaccount(String fundaccount) {
        this.fundaccount = fundaccount == null ? null : fundaccount.trim();
    }

    public String getFundtradeaccount() {
        return fundtradeaccount;
    }

    public void setFundtradeaccount(String fundtradeaccount) {
        this.fundtradeaccount = fundtradeaccount == null ? null : fundtradeaccount.trim();
    }

    public String getFundaccountname() {
        return fundaccountname;
    }

    public void setFundaccountname(String fundaccountname) {
        this.fundaccountname = fundaccountname == null ? null : fundaccountname.trim();
    }

    public String getFundbankname() {
        return fundbankname;
    }

    public void setFundbankname(String fundbankname) {
        this.fundbankname = fundbankname == null ? null : fundbankname.trim();
    }

    public String getRhbankno() {
        return rhbankno;
    }

    public void setRhbankno(String rhbankno) {
        this.rhbankno = rhbankno == null ? null : rhbankno.trim();
    }

    public String getFundbankno() {
        return fundbankno;
    }

    public void setFundbankno(String fundbankno) {
        this.fundbankno = fundbankno == null ? null : fundbankno.trim();
    }

    public byte[] getMovie() {
        return movie;
    }

    public void setMovie(byte[] movie) {
        this.movie = movie;
    }
}