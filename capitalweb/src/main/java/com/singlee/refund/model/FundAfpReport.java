package com.singlee.refund.model;

import java.io.Serializable;

public class FundAfpReport implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//@ExcelIgnore
	private String dealNo;
	
	//@ExcelProperty("工具代码")
	private String fundCode;

	//@ExcelProperty("工具简称")
	private String fundName;

	//@ExcelProperty("申购机构")
	private String afpInst;

	//@ExcelProperty("申购机构类型")
	private String afpInstType;

	//@ExcelProperty("工具类型")
	private String fundType;

	//@ExcelProperty("币种")
	private String ccy;

	//@ExcelProperty("发行日")
	private String releaseDate;

	//@ExcelProperty("缴款日")
	private String payMentDate;

	//@ExcelProperty("到期日")
	private String secondSettlementDate;

	//@ExcelProperty("起息日")
	private String firstSettlementDate;

	//@ExcelProperty("流通范围")
	private String trueRange;

	//@ExcelProperty("发行方式")
	private String issueType;

	//@ExcelProperty("招标方式")
	private String biddingType;

	//@ExcelProperty("期限")
	private String term;

	//@ExcelProperty("票息类型")
	private String couponType;

	//@ExcelProperty("实际发行量（亿元）")
	private String totalQty;

	//@ExcelProperty("成交量（亿元）")
	private String trueQty;

	//@ExcelProperty("投标倍数")
	private String biddingMul;

	//@ExcelProperty("发行价格（元）")
	private String pric;

	//@ExcelProperty("参考收益率")
	private String referRate;

	//@ExcelProperty("票面利率（%）")
	private String faceRate;

	//@ExcelProperty("基准利率")
	private String baseRate;

	//@ExcelProperty("利差（BP）")
	private String spread;

	//@ExcelProperty("提交用户")
	private String userName;

	//@ExcelProperty("电话")
	private String phone;
	
	public String getDealNo() {
		return dealNo;
	}

	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}

	public String getFundCode() {
		return fundCode;
	}

	public void setFundCode(String fundCode) {
		this.fundCode = fundCode;
	}

	public String getFundName() {
		return fundName;
	}

	public void setFundName(String fundName) {
		this.fundName = fundName;
	}

	public String getAfpInst() {
		return afpInst;
	}

	public void setAfpInst(String afpInst) {
		this.afpInst = afpInst;
	}

	public String getAfpInstType() {
		return afpInstType;
	}

	public void setAfpInstType(String afpInstType) {
		this.afpInstType = afpInstType;
	}

	public String getFundType() {
		return fundType;
	}

	public void setFundType(String fundType) {
		this.fundType = fundType;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}

	public String getPayMentDate() {
		return payMentDate;
	}

	public void setPayMentDate(String payMentDate) {
		this.payMentDate = payMentDate;
	}

	public String getSecondSettlementDate() {
		return secondSettlementDate;
	}

	public void setSecondSettlementDate(String secondSettlementDate) {
		this.secondSettlementDate = secondSettlementDate;
	}

	public String getFirstSettlementDate() {
		return firstSettlementDate;
	}

	public void setFirstSettlementDate(String firstSettlementDate) {
		this.firstSettlementDate = firstSettlementDate;
	}

	public String getTrueRange() {
		return trueRange;
	}

	public void setTrueRange(String trueRange) {
		this.trueRange = trueRange;
	}

	public String getIssueType() {
		return issueType;
	}

	public void setIssueType(String issueType) {
		this.issueType = issueType;
	}

	public String getBiddingType() {
		return biddingType;
	}

	public void setBiddingType(String biddingType) {
		this.biddingType = biddingType;
	}

	public String getTerm() {
		return term;
	}

	public void setTerm(String term) {
		this.term = term;
	}

	public String getCouponType() {
		return couponType;
	}

	public void setCouponType(String couponType) {
		this.couponType = couponType;
	}

	public String getTotalQty() {
		return totalQty;
	}

	public void setTotalQty(String totalQty) {
		this.totalQty = totalQty;
	}

	public String getTrueQty() {
		return trueQty;
	}

	public void setTrueQty(String trueQty) {
		this.trueQty = trueQty;
	}

	public String getBiddingMul() {
		return biddingMul;
	}

	public void setBiddingMul(String biddingMul) {
		this.biddingMul = biddingMul;
	}

	public String getPric() {
		return pric;
	}

	public void setPric(String pric) {
		this.pric = pric;
	}

	public String getReferRate() {
		return referRate;
	}

	public void setReferRate(String referRate) {
		this.referRate = referRate;
	}

	public String getFaceRate() {
		return faceRate;
	}

	public void setFaceRate(String faceRate) {
		this.faceRate = faceRate;
	}

	public String getBaseRate() {
		return baseRate;
	}

	public void setBaseRate(String baseRate) {
		this.baseRate = baseRate;
	}

	public String getSpread() {
		return spread;
	}

	public void setSpread(String spread) {
		this.spread = spread;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

}
