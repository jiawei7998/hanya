package com.singlee.refund.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.singlee.ifs.model.IfsBaseFlow;

@Entity
@Table(name = "FT_REIN")
public class CFtRein  extends IfsBaseFlow implements Serializable {

	// 交易编号
	private String dealNo;
	// 产品名称
	private String prdNo;
	// 单据类型
	private String dealType;
	// 审批时间
	private String aDate;
	// 审批发起人
	private String sponsor;
	// 审批发起机构
	private String sponInst;
	// 交易对手
	private String cno;
	// 交易对手名称
	private String cname;
	// 联系人
	private String contact;
	// 联系电话
	private String contactPhone;
	// 基金代码
	private String fundCode;
	// 基金币种
	private String ccy;
	// 转投日期
	private String vdate;
	// 转投份额
	private BigDecimal eshareAmt;
	// 转投红利
	private BigDecimal eAmt;
	// 单位净值
	private BigDecimal price;
	// 会计类型
	private String invType;
	// 审批状态
	private String approveStatus;
	//交易事由
	private String remark;

	@Transient
	private String fundFullName;
	@Transient
	private String taskId;
	@Transient
	private String sponInstName;
	@Transient
	private String taskName;// 审批状态
	@Transient
	private String fundName;
	@Transient
	private BigDecimal totalQty;
	@Transient
	private String estDate;
	@Transient
	private String userName;


	private static final long serialVersionUID = 1L;

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
    public String getDealNo() {
		return dealNo;
	}

	@Override
    public void setDealNo(String dealNo) {
		this.dealNo = dealNo == null ? null : dealNo.trim();
	}

	public String getPrdNo() {
		return prdNo;
	}

	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo == null ? null : prdNo.trim();
	}

	public String getDealType() {
		return dealType;
	}

	public void setDealType(String dealType) {
		this.dealType = dealType == null ? null : dealType.trim();
	}

	@Override
    public String getaDate() {
		return aDate;
	}

	@Override
    public void setaDate(String aDate) {
		this.aDate = aDate;
	}

	@Override
    public String getSponsor() {
		return sponsor;
	}

	@Override
    public void setSponsor(String sponsor) {
		this.sponsor = sponsor == null ? null : sponsor.trim();
	}

	@Override
    public String getSponInst() {
		return sponInst;
	}

	@Override
    public void setSponInst(String sponInst) {
		this.sponInst = sponInst;
	}

	@Override
    public String getCno() {
		return cno;
	}

	@Override
    public void setCno(String cno) {
		this.cno = cno == null ? null : cno.trim();
	}

	public String getCname() {
		return cname;
	}

	public void setCname(String cname) {
		this.cname = cname == null ? null : cname.trim();
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact == null ? null : contact.trim();
	}

	public String getContactPhone() {
		return contactPhone;
	}

	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone == null ? null : contactPhone.trim();
	}

	public String getFundCode() {
		return fundCode;
	}

	public void setFundCode(String fundCode) {
		this.fundCode = fundCode == null ? null : fundCode.trim();
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy == null ? null : ccy.trim();
	}

	public String getVdate() {
		return vdate;
	}

	public void setVdate(String vdate) {
		this.vdate = vdate == null ? null : vdate.trim();
	}

	public BigDecimal getEshareAmt() {
		return eshareAmt;
	}

	public void setEshareAmt(BigDecimal eshareAmt) {
		this.eshareAmt = eshareAmt;
	}

	public BigDecimal geteAmt() {
		return eAmt;
	}

	public void seteAmt(BigDecimal eAmt) {
		this.eAmt = eAmt;
	}

	public String getInvType() {
		return invType;
	}

	public void setInvType(String invType) {
		this.invType = invType == null ? null : invType.trim();
	}

	@Override
    public String getApproveStatus() {
		return approveStatus;
	}

	@Override
    public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}

	public String getFundFullName() {
		return fundFullName;
	}

	public void setFundFullName(String fundFullName) {
		this.fundFullName = fundFullName;
	}

	@Override
    public String getTaskId() {
		return taskId;
	}

	@Override
    public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getSponInstName() {
		return sponInstName;
	}

	public void setSponInstName(String sponInstName) {
		this.sponInstName = sponInstName;
	}

	@Override
    public String getTaskName() {
		return taskName;
	}

	@Override
    public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public String getFundName() {
		return fundName;
	}

	public void setFundName(String fundName) {
		this.fundName = fundName;
	}

	public BigDecimal getTotalQty() {
		return totalQty;
	}

	public void setTotalQty(BigDecimal totalQty) {
		this.totalQty = totalQty;
	}

	public String getEstDate() {
		return estDate;
	}

	public void setEstDate(String estDate) {
		this.estDate = estDate;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	@Override
	public String getUserName() {
		return userName;
	}

	@Override
	public void setUserName(String userName) {
		this.userName = userName;
	}
}