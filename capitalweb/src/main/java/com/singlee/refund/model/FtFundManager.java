package com.singlee.refund.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "FT_FUND_MANAGER")
public class FtFundManager implements Serializable {


    //基金管理人编号
    @Id
    private String managcomp;
    //基金管理人名称
    private String managcompnm;

    public String getManagcomp() {
        return managcomp;
    }

    public void setManagcomp(String managcomp) {
        this.managcomp = managcomp;
    }

    public String getManagcompnm() {
        return managcompnm;
    }

    public void setManagcompnm(String managcompnm) {
        this.managcompnm = managcompnm;
    }

    @Override
    public String toString() {
        return "FtFundManager{" +
                "managcomp='" + managcomp + '\'' +
                ", managcompnm='" + managcompnm + '\'' +
                '}';
    }
}