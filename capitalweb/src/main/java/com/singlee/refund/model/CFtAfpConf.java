package com.singlee.refund.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.singlee.ifs.model.IfsBaseFlow;

@Entity
@Table(name = "FT_AFP_CONF")
public class CFtAfpConf  extends IfsBaseFlow implements Serializable {

	// 交易编号
	private String dealNo;
	// 关联编号
	private String refNo;
	// 产品名称
	private String prdNo;
	// 单据类型
	private String dealType;
	// 审批时间
	private String aDate;
	// 审批发起人
	private String sponsor;
	// 审批发起机构
	private String sponInst;
	// 交易对手
	private String cno;
	// 交易对手名称
	private String cname;
	// 联系人
	private String contact;
	// 联系电话
	private String contactPhone;
	// 基金代码
	private String fundCode;
	// 基金币种
	private String ccy;
	// 确认份额
	private BigDecimal shareAmt;
	// 确认金额
	private BigDecimal amt;
	// 单位净值
	private BigDecimal price;
	// 扣款日期
	private String tdate;
	// 起息日期
	private String vdate;
	// FTP价格
	private BigDecimal ftpprice;
	// 会计类型
	private String invType;
	// 平台投向
	private String platformInv;
	// 最终投向
	private String eventuallyInv;
	// 备注
	private String remark;
	// 本方帐号
	private String selfAcccode;
	// 本方帐号名称
	private String selfAccname;
	// 本方开户行行号
	private String selfBankcode;
	// 本方开户行名称
	private String selfBankname;
	// 对方帐号
	private String partyAcccode;
	// 对方帐号名称
	private String partyAccname;
	// 对方开户行行号
	private String partyBankcode;
	// 对方开户行名称
	private String partyBankname;
	// 审批状态
	private String approveStatus;
	// 是否确认完成
	private String isConfirm;
	// 手续费
	private BigDecimal handleAmt;
	// 剩余可赎回金额
	private BigDecimal reShareAmt;
	// 是否赎回完毕
	private String reEndFlag;

	private String currencyType;
	// ftp参考曲线
	private String ftpflag;
	// 是否直销 0-否 1-是
	private String isDirect;

	private BigDecimal handleRate;// 费率
	private String trem;// 期限
	private BigDecimal yield;// 万份收益
	private BigDecimal sevendayRate;// 七日年化收益率

	/**
	 * 实际记账金额
	 */
	private BigDecimal actualAmountReceived;
	@Transient
	private String fundFullName;
	@Transient
	private String taskId;
	@Transient
	private String sponInstName;
	@Transient
	private String taskName;// 审批状态
	@Transient
	private BigDecimal atpShareAmt;// 申购份额
	@Transient
	private BigDecimal atpAmt;// 申购金额
	@Transient
	private String fundName;
	@Transient
	private BigDecimal totalQty;
	@Transient
	private String estDate;
	@Transient
	private String userName;

	@Transient
	private static final long serialVersionUID = 1L;

	public String getRefNo() {
		return refNo;
	}

	public void setRefNo(String refNo) {
		this.refNo = refNo == null ? null : refNo.trim();
	}

	@Override
    public String getDealNo() {
		return dealNo;
	}

	@Override
    public void setDealNo(String dealNo) {
		this.dealNo = dealNo == null ? null : dealNo.trim();
	}

	public String getPrdNo() {
		return prdNo;
	}

	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo == null ? null : prdNo.trim();
	}

	public String getDealType() {
		return dealType;
	}

	public void setDealType(String dealType) {
		this.dealType = dealType == null ? null : dealType.trim();
	}

	@Override
    public String getaDate() {
		return aDate;
	}

	@Override
    public void setaDate(String aDate) {
		this.aDate = aDate;
	}

	@Override
    public String getSponsor() {
		return sponsor;
	}

	@Override
    public void setSponsor(String sponsor) {
		this.sponsor = sponsor == null ? null : sponsor.trim();
	}

	@Override
    public String getSponInst() {
		return sponInst;
	}

	@Override
    public void setSponInst(String sponInst) {
		this.sponInst = sponInst;
	}

	@Override
    public String getCno() {
		return cno;
	}

	@Override
    public void setCno(String cno) {
		this.cno = cno == null ? null : cno.trim();
	}

	public String getCname() {
		return cname;
	}

	public void setCname(String cname) {
		this.cname = cname == null ? null : cname.trim();
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact == null ? null : contact.trim();
	}

	public String getContactPhone() {
		return contactPhone;
	}

	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone == null ? null : contactPhone.trim();
	}

	public String getFundCode() {
		return fundCode;
	}

	public void setFundCode(String fundCode) {
		this.fundCode = fundCode == null ? null : fundCode.trim();
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy == null ? null : ccy.trim();
	}

	public BigDecimal getShareAmt() {
		return shareAmt;
	}

	public void setShareAmt(BigDecimal shareAmt) {
		this.shareAmt = shareAmt;
	}

	public BigDecimal getAmt() {
		return amt;
	}

	public void setAmt(BigDecimal amt) {
		this.amt = amt;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getTdate() {
		return tdate;
	}

	public void setTdate(String tdate) {
		this.tdate = tdate == null ? null : tdate.trim();
	}

	public String getVdate() {
		return vdate;
	}

	public void setVdate(String vdate) {
		this.vdate = vdate == null ? null : vdate.trim();
	}

	public BigDecimal getFtpprice() {
		return ftpprice;
	}

	public void setFtpprice(BigDecimal ftpprice) {
		this.ftpprice = ftpprice;
	}

	public String getInvType() {
		return invType;
	}

	public void setInvType(String invType) {
		this.invType = invType == null ? null : invType.trim();
	}

	public String getPlatformInv() {
		return platformInv;
	}

	public void setPlatformInv(String platformInv) {
		this.platformInv = platformInv == null ? null : platformInv.trim();
	}

	public String getEventuallyInv() {
		return eventuallyInv;
	}

	public void setEventuallyInv(String eventuallyInv) {
		this.eventuallyInv = eventuallyInv == null ? null : eventuallyInv.trim();
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark == null ? null : remark.trim();
	}

	public String getSelfAcccode() {
		return selfAcccode;
	}

	public void setSelfAcccode(String selfAcccode) {
		this.selfAcccode = selfAcccode == null ? null : selfAcccode.trim();
	}

	public String getSelfAccname() {
		return selfAccname;
	}

	public void setSelfAccname(String selfAccname) {
		this.selfAccname = selfAccname == null ? null : selfAccname.trim();
	}

	public String getSelfBankcode() {
		return selfBankcode;
	}

	public void setSelfBankcode(String selfBankcode) {
		this.selfBankcode = selfBankcode == null ? null : selfBankcode.trim();
	}

	public String getSelfBankname() {
		return selfBankname;
	}

	public void setSelfBankname(String selfBankname) {
		this.selfBankname = selfBankname == null ? null : selfBankname.trim();
	}

	public String getPartyAcccode() {
		return partyAcccode;
	}

	public void setPartyAcccode(String partyAcccode) {
		this.partyAcccode = partyAcccode == null ? null : partyAcccode.trim();
	}

	public String getPartyAccname() {
		return partyAccname;
	}

	public void setPartyAccname(String partyAccname) {
		this.partyAccname = partyAccname == null ? null : partyAccname.trim();
	}

	public String getPartyBankcode() {
		return partyBankcode;
	}

	public void setPartyBankcode(String partyBankcode) {
		this.partyBankcode = partyBankcode == null ? null : partyBankcode.trim();
	}

	public String getPartyBankname() {
		return partyBankname;
	}

	public void setPartyBankname(String partyBankname) {
		this.partyBankname = partyBankname == null ? null : partyBankname.trim();
	}

	@Override
    public String getApproveStatus() {
		return approveStatus;
	}

	@Override
    public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus == null ? null : approveStatus.trim();
	}

	public String getIsConfirm() {
		return isConfirm;
	}

	public void setIsConfirm(String isConfirm) {
		this.isConfirm = isConfirm == null ? null : isConfirm.trim();
	}

	public BigDecimal getHandleAmt() {
		return handleAmt;
	}

	public void setHandleAmt(BigDecimal handleAmt) {
		this.handleAmt = handleAmt;
	}

	public BigDecimal getReShareAmt() {
		return reShareAmt;
	}

	public void setReShareAmt(BigDecimal reShareAmt) {
		this.reShareAmt = reShareAmt;
	}

	public String getReEndFlag() {
		return reEndFlag;
	}

	public void setReEndFlag(String reEndFlag) {
		this.reEndFlag = reEndFlag == null ? null : reEndFlag.trim();
	}

	public String getCurrencyType() {
		return currencyType;
	}

	public void setCurrencyType(String currencyType) {
		this.currencyType = currencyType == null ? null : currencyType.trim();
	}

	public String getFtpflag() {
		return ftpflag;
	}

	public void setFtpflag(String ftpflag) {
		this.ftpflag = ftpflag == null ? null : ftpflag.trim();
	}

	public String getIsDirect() {
		return isDirect;
	}

	public void setIsDirect(String isDirect) {
		this.isDirect = isDirect == null ? null : isDirect.trim();
	}

	public String getFundFullName() {
		return fundFullName;
	}

	public void setFundFullName(String fundFullName) {
		this.fundFullName = fundFullName;
	}

	@Override
    public String getTaskId() {
		return taskId;
	}

	@Override
    public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getSponInstName() {
		return sponInstName;
	}

	public void setSponInstName(String sponInstName) {
		this.sponInstName = sponInstName;
	}

	@Override
    public String getTaskName() {
		return taskName;
	}

	@Override
    public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public BigDecimal getAtpShareAmt() {
		return atpShareAmt;
	}

	public void setAtpShareAmt(BigDecimal atpShareAmt) {
		this.atpShareAmt = atpShareAmt;
	}

	public BigDecimal getAtpAmt() {
		return atpAmt;
	}

	public void setAtpAmt(BigDecimal atpAmt) {
		this.atpAmt = atpAmt;
	}

	public String getFundName() {
		return fundName;
	}

	public void setFundName(String fundName) {
		this.fundName = fundName;
	}

	public BigDecimal getTotalQty() {
		return totalQty;
	}

	public void setTotalQty(BigDecimal totalQty) {
		this.totalQty = totalQty;
	}

	public String getEstDate() {
		return estDate;
	}

	public void setEstDate(String estDate) {
		this.estDate = estDate;
	}

	public BigDecimal getHandleRate() {
		return handleRate;
	}

	public void setHandleRate(BigDecimal handleRate) {
		this.handleRate = handleRate;
	}

	public String getTrem() {
		return trem;
	}

	public void setTrem(String trem) {
		this.trem = trem;
	}

	public BigDecimal getYield() {
		return yield;
	}

	public void setYield(BigDecimal yield) {
		this.yield = yield;
	}

	public BigDecimal getSevendayRate() {
		return sevendayRate;
	}

	public void setSevendayRate(BigDecimal sevendayRate) {
		this.sevendayRate = sevendayRate;
	}

	@Override
	public String getUserName() {
		return userName;
	}

	@Override
	public void setUserName(String userName) {
		this.userName = userName;
	}

	public BigDecimal getActualAmountReceived() {
		return actualAmountReceived;
	}

	public void setActualAmountReceived(BigDecimal actualAmountReceived) {
		this.actualAmountReceived = actualAmountReceived;
	}
}