package com.singlee.refund.service.impl;


import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.refund.mapper.CFtAfpConfMapper;
import com.singlee.refund.mapper.CFtInfoMapper;
import com.singlee.refund.model.CFtAfpConf;
import com.singlee.refund.model.CFtInfo;
import com.singlee.refund.service.CFtInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service
@Transactional(value = "transactionManager", rollbackFor = Exception.class)
public class CFtTposServiceImpl implements CFtInfoService {

	@Autowired
	private CFtInfoMapper cFtInfoMapper;
	@Autowired
	private CFtAfpConfMapper cFtAfpConfMapper;

	@Override
	public Page<CFtInfo> searchCFtInfoList(Map<String, Object> map) {
		return cFtInfoMapper.searchCFtInfoPage(map, ParameterUtil.getRowBounds(map));
	}

	@Override
	public CFtInfo searchCFtInfoByFundCode(String fundCode) {
		return cFtInfoMapper.selectByPrimaryKey(fundCode);
	}

	@Override
	public int saveOrUpdateCFtInfo(CFtInfo fundInfo) {
		CFtInfo obj = cFtInfoMapper.selectByPrimaryKey(fundInfo.getFundCode());
		if(obj == null) {
			return cFtInfoMapper.insertSelective(fundInfo);
		}else{
			List<CFtAfpConf> list = cFtAfpConfMapper.selectListByFundCode(fundInfo.getFundCode());
			if(list.size() > 0 && (!obj.getBonusWay().equals(fundInfo.getBonusWay()))) {//|| !obj.getCurrencyType().equals(fundInfo.getCurrencyType())
				return -99;
			}
			return cFtInfoMapper.updateByPrimaryKeySelective(fundInfo);
		}
	}

	@Override
	public int deleteCFtInfo(String fundCode) {
		return cFtInfoMapper.deleteByPrimaryKey(fundCode);
	}

	@Override
	public Integer getCount(Map<String, Object> map) {
		return cFtInfoMapper.getCount(map);
	}

}