package com.singlee.refund.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.refund.model.CFtRdp;

/**
 * 基金赎回
 *
 */
public interface CFtRdpService {

	/**
	 * 分页查询
	 * @param params
	 * @param isFinished
	 * @return
	 */
	Page<CFtRdp> searchCFtRdpList(Map<String, Object> params,Integer isFinished);
	
	/**
	 * 单条查询
	 * @param dealNo
	 * @return
	 */
	CFtRdp selectBydealNo(String dealNo);
	
	/**
	 * 单条查询
	 * @param map
	 * @return
	 */
	CFtRdp selectByMap(Map<String,Object> map);

	CFtRdp selectCFtRdpById(Map<String,Object> map);
	
	/**
	 * 保存或者修改
	 * @param cFtRdp
	 * @return
	 */
	int saveOrUpdateCFtRdp(CFtRdp cFtRdp);
	
	/**
	 * 删除
	 * @param dealNo
	 * @return
	 */
	int deleteCFtRdp(String dealNo);
}