package com.singlee.refund.service;

import java.util.Map;
import com.github.pagehelper.Page;
import com.singlee.refund.model.CFtRein;

/**
 * 红利再投
 *
 */
public interface CFtReinService {

	/**
	 * 分页查询
	 * @param params
	 * @param isFinished
	 * @return
	 */
	Page<CFtRein> searchCFtReinList(Map<String, Object> params,Integer isFinished);
	
	/**
	 * 单条查询
	 * @param dealNo
	 * @return
	 */
	CFtRein selectCFtReinByDealNo(String dealNo);
	
	/**
	 * 单条查询
	 * @param map
	 * @return
	 */
	CFtRein selectCFtReinByMap(Map<String,Object> map);

	CFtRein selectCFtReinById(Map<String,Object> map);
	
	/**
	 * 保存或者修改
	 * @param cFtRein
	 * @return
	 */
	int saveOrUpdateCFtRein(CFtRein cFtRein);
	
	/**
	 * 删除
	 * @param dealNo
	 * @return
	 */
	int deleteCFtRein(String dealNo);
	
}