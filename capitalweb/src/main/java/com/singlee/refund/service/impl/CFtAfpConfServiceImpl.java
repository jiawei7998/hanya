package com.singlee.refund.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.github.pagehelper.Page;
import com.singlee.refund.model.CFtAfpConf;
import com.singlee.refund.service.CFtAfpConfService;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.hrbcap.util.TradeConstants;
import com.singlee.ifs.mapper.IFSMapper;
import com.singlee.refund.mapper.CFtAfpConfMapper;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class CFtAfpConfServiceImpl implements CFtAfpConfService {

	@Autowired
	private CFtAfpConfMapper cFtAfpConfMapper;
	@Resource
	IFSMapper ifsMapper;

	@Override
	public Page<CFtAfpConf> searchCFtAfpConfList(Map<String, Object> params,Integer isFinished) {
		Page<CFtAfpConf> page = new Page<CFtAfpConf>();
		if (isFinished == 1) {// 待审批
			page = cFtAfpConfMapper.searchCFtAfpConfPageUnfinished(params, ParameterUtil.getRowBounds(params));
		} else if (isFinished == 2) {// 已审批
			page = cFtAfpConfMapper.searchCFtAfpConfPageFinished(params, ParameterUtil.getRowBounds(params));
		} else {// 我发起的
			page = cFtAfpConfMapper.searchCFtAfpConfPageMine(params, ParameterUtil.getRowBounds(params));
		}
		return page;
	}

	@Override
	public CFtAfpConf searchCFtAfpConfByDealNo(String dealNo) {
		return cFtAfpConfMapper.selectByPrimaryKey(dealNo);
	}

	@Override
	public CFtAfpConf searchCFtAfpConfByMap(Map<String,Object> map) {
		return cFtAfpConfMapper.selectCFtAfpConfByDealNo(map);
	}

	@Override
	public CFtAfpConf searchCFtAfpConfById(Map<String, Object> map) {
		return cFtAfpConfMapper.selectCFtAfpConfById(map);
	}

	@Override
	public int saveOrUpdateCFtAfpConf(CFtAfpConf cFtAfpConf) {
		CFtAfpConf obj = cFtAfpConfMapper.selectByPrimaryKey(cFtAfpConf.getDealNo());
		if (obj == null) {
			cFtAfpConf.setDealNo(getTicketId(TradeConstants.TrdType.FUND_AFPCONF));
			cFtAfpConf.setApproveStatus(DictConstants.ApproveStatus.New);
			return cFtAfpConfMapper.insertSelective(cFtAfpConf);
		} else {
			return cFtAfpConfMapper.updateByPrimaryKeySelective(cFtAfpConf);
		}
	}

	@Override
	public int deleteCFtAfpConf(String dealNo) {
		return cFtAfpConfMapper.deleteByPrimaryKey(dealNo);
	}
	
	//获取交易编号
	public String getTicketId(String str) {
		HashMap<String, Object> hashMap = new HashMap<String, Object>();
		// 前缀
		hashMap.put("sStr", "");
		// 贸易类型 正式交易
		hashMap.put("trdType", "2");
		String ticketId = ifsMapper.getTradeId(hashMap);
		return ticketId;
	}

}