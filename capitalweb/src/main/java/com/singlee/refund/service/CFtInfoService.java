package com.singlee.refund.service;

import com.github.pagehelper.Page;
import com.singlee.refund.model.CFtInfo;

import java.util.Map;

/**
 * 基金基础信息
 */
public interface CFtInfoService {

	/**
	 * 分页获取列表
	 *
	 * @param map
	 * @return
	 */
	Page<CFtInfo> searchCFtInfoList(Map<String, Object> map);

	/**
	 * 单条查询
	 *
	 * @param fundCode
	 * @return
	 */
	CFtInfo searchCFtInfoByFundCode(String fundCode);

	/**
	 * 保存或修改
	 *
	 * @param map
	 * @return
	 */
	int saveOrUpdateCFtInfo(CFtInfo fundInfo);

	/**
	 * 删除
	 *
	 * @param fundCode
	 * @return
	 */
	int deleteCFtInfo(String fundCode);


	Integer getCount(Map<String, Object> map);
}