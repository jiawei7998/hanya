package com.singlee.refund.service.impl;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.github.pagehelper.Page;
import com.singlee.refund.model.CFtRdpConf;
import com.singlee.refund.service.CFtRdpConfService;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.hrbcap.util.TradeConstants;
import com.singlee.ifs.mapper.IFSMapper;
import com.singlee.refund.mapper.CFtRdpConfMapper;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class CFtRdpConfServiceImpl implements CFtRdpConfService {

	@Autowired
	private CFtRdpConfMapper cFtRdpConfMapper;
	@Resource
	IFSMapper ifsMapper;

	@Override
	public Page<CFtRdpConf> searchCFtRdpConfList(Map<String, Object> params, Integer isFinished) {
		Page<CFtRdpConf> page = new Page<CFtRdpConf>();
		if (isFinished == 1) {// 待审批
			page = cFtRdpConfMapper.searchCFtRdpConfPageUnfinished(params, ParameterUtil.getRowBounds(params));
		} else if (isFinished == 2) {// 已审批
			page = cFtRdpConfMapper.searchCFtRdpConfPageFinished(params, ParameterUtil.getRowBounds(params));
		} else {// 我发起的
			page = cFtRdpConfMapper.searchCFtRdpConfPageMine(params, ParameterUtil.getRowBounds(params));
		}
		return page;
	}

	@Override
	public CFtRdpConf searchCFtRdpConfByDealNo(String dealNo) {
		return cFtRdpConfMapper.selectByPrimaryKey(dealNo);
	}

	@Override
	public CFtRdpConf searchCFtRdpConfByMap(Map<String, Object> map) {
		return cFtRdpConfMapper.selectCFtRdpConf(map);
	}

	@Override
	public CFtRdpConf selectCFtRdpConfById(Map<String, Object> map) {
		return cFtRdpConfMapper.selectCFtRdpConfById(map);
	}

	@Override
	public int saveOrUpdateCFtRdpConf(CFtRdpConf cFtRdpConf) {
		CFtRdpConf obj = cFtRdpConfMapper.selectByPrimaryKey(cFtRdpConf.getDealNo());
		if(obj == null) {
			cFtRdpConf.setDealNo(getTicketId(TradeConstants.TrdType.FUND_RDPCONF));
			cFtRdpConf.setApproveStatus(DictConstants.ApproveStatus.New);
			return cFtRdpConfMapper.insertSelective(cFtRdpConf);
		}else {
			return cFtRdpConfMapper.updateByPrimaryKeySelective(cFtRdpConf);
		}
	}

	@Override
	public int deleteCFtRdpConf(String dealNo) {
		return cFtRdpConfMapper.deleteByPrimaryKey(dealNo);
	}

	
	//获取交易编号
	public String getTicketId(String str) {
		HashMap<String, Object> hashMap = new HashMap<String, Object>();
		// 前缀
		hashMap.put("sStr", "");
		// 贸易类型 正式交易
		hashMap.put("trdType", "2");
		String ticketId = ifsMapper.getTradeId(hashMap);
		return ticketId;
	}

}