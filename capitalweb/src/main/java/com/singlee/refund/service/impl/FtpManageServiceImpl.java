package com.singlee.refund.service.impl;

import java.text.SimpleDateFormat;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.refund.mapper.FtpManageMapper;
import com.singlee.refund.model.FtpManage;
import com.singlee.refund.service.FtpManageService;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class FtpManageServiceImpl implements FtpManageService {

	@Autowired
	private FtpManageMapper ftpManageMapper;

	@Override
	public Page<FtpManage> searchFtpManageList(Map<String, Object> params) {
		return ftpManageMapper.searchFtpManagePage(params, ParameterUtil.getRowBounds(params));
	}

	@Override
	public FtpManage searchFtpManageByid(Short id) {
		return ftpManageMapper.selectByPrimaryKey(id);
	}

	@Override
	public int saveOrUpdateFtpManage(FtpManage ftpManage) {
		FtpManage obj = ftpManageMapper.selectByPrimaryKey(ftpManage.getId());
		if(obj == null) {
			SimpleDateFormat format =new SimpleDateFormat("yyyy-MM-dd");
			ftpManage.setOperTime(format.format(System.currentTimeMillis()));
			return ftpManageMapper.insertSelective(ftpManage);
		}else {
			return ftpManageMapper.updateByPrimaryKeySelective(ftpManage);
		}
	}

	@Override
	public int deleteFtpManage(Short id) {
		return ftpManageMapper.deleteByPrimaryKey(id);
	}
}