package com.singlee.refund.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.hrbcap.util.TradeConstants;
import com.singlee.ifs.mapper.IFSMapper;
import com.singlee.refund.mapper.CFtAfpMapper;
import com.singlee.refund.model.CFtAfp;
import com.singlee.refund.service.CFtAfpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class CFtAfpServiceImpl implements CFtAfpService {

	@Autowired
	private CFtAfpMapper cFtAfpMapper;
	@Resource
	IFSMapper ifsMapper;

	@Override
	public Page<CFtAfp> searchCFtAfpList(Map<String, Object> params,Integer isFinished) {
		Page<CFtAfp> page = new Page<CFtAfp>();
		if (isFinished == 1) {// 待审批
			page = cFtAfpMapper.searchCFtAfpPageUnfinished(params, ParameterUtil.getRowBounds(params));
		} else if (isFinished == 2) {// 已审批
			page = cFtAfpMapper.searchCFtAfpPageFinished(params, ParameterUtil.getRowBounds(params));
		} else {// 我发起的
			page = cFtAfpMapper.searchCFtAfpPageMine(params, ParameterUtil.getRowBounds(params));
		}
		return page;
	}

	@Override
	public CFtAfp selectTaskIdByDealNo(String dealNo) {
		return cFtAfpMapper.selectByPrimaryKey(dealNo);
	}

	@Override
	public CFtAfp searchCFtAfpById(Map<String, Object> map) {
		return cFtAfpMapper.searchCFtAfpById(map);
	}

	@Override
	public CFtAfp selectTaskIdByMap(Map<String,Object> map) {
		return cFtAfpMapper.selectTaskIdByPrimaryKey(map);
	}

	@Override
	public int saveOrUpdateCFtAfp(CFtAfp cFtAfp) {
		int i;
		CFtAfp obj = cFtAfpMapper.selectByPrimaryKey(cFtAfp.getDealNo());
		if (obj == null) {
			cFtAfp.setDealNo(getTicketId(TradeConstants.TrdType.FUND_AFP));
			cFtAfp.setApproveStatus(DictConstants.ApproveStatus.New);
			i=cFtAfpMapper.insertSelective(cFtAfp);
		} else {
			i=cFtAfpMapper.updateByPrimaryKeySelective(cFtAfp);

		}
		// 组装map
		Map<String, Object> callMap = new HashMap<String, Object>();
		callMap.put("serial_no", cFtAfp.getDealNo());
		callMap.put("status", "6");
		callMap.put("prdNo", cFtAfp.getPrdNo());

		return i;
	}

	@Override
	public int deleteCFtAfp(String dealNo) {
		return cFtAfpMapper.deleteByPrimaryKey(dealNo);
	}
	
	//获取交易编号
	public String getTicketId(String str) {
		HashMap<String, Object> hashMap = new HashMap<String, Object>();
		// 前缀
		hashMap.put("sStr", "");
		// 贸易类型 正式交易
		hashMap.put("trdType", "2");
		String ticketId = ifsMapper.getTradeId(hashMap);
		return ticketId;
	}

}