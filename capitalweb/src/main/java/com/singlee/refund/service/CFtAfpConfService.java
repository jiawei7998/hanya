package com.singlee.refund.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.refund.model.CFtAfpConf;

/**
 * 基金申购确认
 *
 */
public interface CFtAfpConfService {

	/**
	 * 分页查询
	 * @param params
	 * @param isFinished
	 * @return
	 */
	Page<CFtAfpConf> searchCFtAfpConfList(Map<String, Object> params,Integer isFinished);
	
	/**
	 * 单条查询
	 * @param dealNo
	 * @return
	 */
	CFtAfpConf searchCFtAfpConfByDealNo(String dealNo);
	
	/**
	 * 单条查询
	 * @param map
	 * @return
	 */
	CFtAfpConf searchCFtAfpConfByMap(Map<String,Object> map);

	CFtAfpConf searchCFtAfpConfById(Map<String,Object> map);
	
	/**
	 * 保存或者修改
	 * @param cFtAfpConf
	 * @return
	 */
	int saveOrUpdateCFtAfpConf(CFtAfpConf cFtAfpConf);
	
	/**
	 * 删除
	 * @param dealNo
	 * @return
	 */
	int deleteCFtAfpConf(String dealNo);
}