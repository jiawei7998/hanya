package com.singlee.refund.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.refund.model.CFtRdpConf;

/**
 * 基金赎回确认
 *
 */
public interface CFtRdpConfService {

	/**
	 * 分页查询
	 * @param params
	 * @param isFinished
	 * @return
	 */
	Page<CFtRdpConf> searchCFtRdpConfList(Map<String, Object> params,Integer isFinished);
	
	/**
	 * 单条查询
	 * @param dealNo
	 * @return
	 */
	CFtRdpConf searchCFtRdpConfByDealNo(String dealNo);
	
	/**
	 * 单条查询
	 * @param dealNo
	 * @return
	 */
	CFtRdpConf searchCFtRdpConfByMap(Map<String,Object> map);

	CFtRdpConf selectCFtRdpConfById(Map<String,Object> map);
	
	/**
	 * 保存或者修改
	 * @param cFtRdpConf
	 * @return
	 */
	int saveOrUpdateCFtRdpConf(CFtRdpConf cFtRdpConf);
	
	/**
	 * 删除
	 * @param dealNo
	 * @return
	 */
	int deleteCFtRdpConf(String dealNo);
}