package com.singlee.refund.service;

import java.util.Map;
import com.github.pagehelper.Page;
import com.singlee.refund.model.CFtRedd;

/**
 * 现金分红
 *
 */
public interface CFtReddService {

	/**
	 * 分页查询
	 * @param params
	 * @param isFinished
	 * @return
	 */
	Page<CFtRedd> searchCFtReddList(Map<String, Object> params,Integer isFinished);
	
	/**
	 * 单条查询
	 * @param dealNo
	 * @return
	 */
	CFtRedd selectCFtReddByDealNo(String dealNo);
	
	/**
	 * 单条查询
	 * @param map
	 * @return
	 */
	CFtRedd selectCFtReddByMap(Map<String,Object> map);

	CFtRedd selectCFtReddById(Map<String,Object> map);
	
	/**
	 * 保存或者修改
	 * @param cFtRedd
	 * @return
	 */
	int saveOrUpdateCFtRedd(CFtRedd cFtRedd);
	
	/**
	 * 删除
	 * @param dealNo
	 * @return
	 */
	int deleteCFtRedd(String dealNo);
	
}