package com.singlee.refund.service.impl;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.github.pagehelper.Page;
import com.singlee.refund.model.CFtRein;
import com.singlee.refund.service.CFtReinService;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.hrbcap.util.TradeConstants;
import com.singlee.ifs.mapper.IFSMapper;
import com.singlee.refund.mapper.CFtReinMapper;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class CFtReinServiceImpl implements CFtReinService {

	@Autowired
	private CFtReinMapper cFtReinMapper;
	@Resource
	IFSMapper ifsMapper;

	@Override
	public Page<CFtRein> searchCFtReinList(Map<String, Object> params, Integer isFinished) {
		Page<CFtRein> page = new Page<CFtRein>();
		if (isFinished == 1) {// 待审批
			page = cFtReinMapper.searchCFtReinPageUnfinished(params, ParameterUtil.getRowBounds(params));
		} else if (isFinished == 2) {// 已审批
			page = cFtReinMapper.searchCFtReinPageFinished(params, ParameterUtil.getRowBounds(params));
		} else {// 我发起的
			page = cFtReinMapper.searchCFtReinPageMine(params, ParameterUtil.getRowBounds(params));
		}
		return page;
	}

	@Override
	public CFtRein selectCFtReinByDealNo(String dealNo) {
		return cFtReinMapper.selectByPrimaryKey(dealNo);
	}

	@Override
	public CFtRein selectCFtReinByMap(Map<String, Object> map) {
		return cFtReinMapper.selectByMap(map);
	}

	@Override
	public CFtRein selectCFtReinById(Map<String, Object> map) {
		return cFtReinMapper.selectCFtReinById(map);
	}

	@Override
	public int saveOrUpdateCFtRein(CFtRein cFtRein) {
		CFtRein obj = cFtReinMapper.selectByPrimaryKey(cFtRein.getDealNo());
		if(obj == null) {
			cFtRein.setDealNo(getTicketId(TradeConstants.TrdType.FUND_REIN));
			cFtRein.setApproveStatus(DictConstants.ApproveStatus.New);
			return cFtReinMapper.insertSelective(cFtRein);
		}else {
			return cFtReinMapper.updateByPrimaryKeySelective(cFtRein);
		}
	}

	@Override
	public int deleteCFtRein(String dealNo) {
		return cFtReinMapper.deleteByPrimaryKey(dealNo);
	}
	
	//获取交易编号
	public String getTicketId(String str) {
		HashMap<String, Object> hashMap = new HashMap<String, Object>();
		// 前缀
		hashMap.put("sStr", "");
		// 贸易类型 正式交易
		hashMap.put("trdType", "2");
		String ticketId = ifsMapper.getTradeId(hashMap);
		return ticketId;
	}

}