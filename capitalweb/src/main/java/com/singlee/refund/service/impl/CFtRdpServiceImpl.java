package com.singlee.refund.service.impl;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.github.pagehelper.Page;
import com.singlee.refund.model.CFtAfp;
import com.singlee.refund.model.CFtRdp;
import com.singlee.refund.service.CFtRdpService;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.hrbcap.util.TradeConstants;
import com.singlee.ifs.mapper.IFSMapper;
import com.singlee.refund.mapper.CFtRdpMapper;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class CFtRdpServiceImpl implements CFtRdpService {

	@Autowired
	private CFtRdpMapper cFtRdpMapper;
	@Resource
	IFSMapper ifsMapper;

	@Override
	public Page<CFtRdp> searchCFtRdpList(Map<String, Object> params, Integer isFinished) {
		Page<CFtRdp> page = new Page<CFtRdp>();
		if (isFinished == 1) {// 待审批
			page = cFtRdpMapper.searchCFtRdpPageUnfinished(params, ParameterUtil.getRowBounds(params));
		} else if (isFinished == 2) {// 已审批
			page = cFtRdpMapper.searchCFtRdpPageFinished(params, ParameterUtil.getRowBounds(params));
		} else {// 我发起的
			page = cFtRdpMapper.searchCFtRdpPageMine(params, ParameterUtil.getRowBounds(params));
		}
		return page;
	}

	@Override
	public CFtRdp selectBydealNo(String dealNo) {
		return cFtRdpMapper.selectByPrimaryKey(dealNo);
	}

	@Override
	public CFtRdp selectByMap(Map<String, Object> map) {
		return cFtRdpMapper.selectCFtRdp(map);
	}

	@Override
	public CFtRdp selectCFtRdpById(Map<String, Object> map) {
		return cFtRdpMapper.selectCFtRdpById(map);
	}

	@Override
	public int saveOrUpdateCFtRdp(CFtRdp cFtRdp) {
		CFtRdp obj = cFtRdpMapper.selectByPrimaryKey(cFtRdp.getDealNo());
		if(obj == null) {
			cFtRdp.setDealNo(getTicketId(TradeConstants.TrdType.FUND_RDP));
			cFtRdp.setApproveStatus(DictConstants.ApproveStatus.New);
			return cFtRdpMapper.insertSelective(cFtRdp);
		}else {
			return cFtRdpMapper.updateByPrimaryKeySelective(cFtRdp);
		}
	}

	@Override
	public int deleteCFtRdp(String dealNo) {
		return cFtRdpMapper.deleteByPrimaryKey(dealNo);
	}
	
	//获取交易编号
	public String getTicketId(String str) {
		HashMap<String, Object> hashMap = new HashMap<String, Object>();
		// 前缀
		hashMap.put("sStr", "");
		// 贸易类型 正式交易
		hashMap.put("trdType", "2");
		String ticketId = ifsMapper.getTradeId(hashMap);
		return ticketId;
	}
}