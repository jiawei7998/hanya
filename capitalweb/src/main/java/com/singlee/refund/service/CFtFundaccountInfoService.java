package com.singlee.refund.service;

import java.util.Map;
import com.github.pagehelper.Page;
import com.singlee.refund.model.CFtFundaccountInfo;
import org.springframework.web.multipart.MultipartHttpServletRequest;

/**
 * 基金账号信息
 *
 */
public interface CFtFundaccountInfoService {

	/**
	 * 分页获取列表
	 * @param map
	 * @return
	 */
	Page<CFtFundaccountInfo> searchCFtFundaccountInfoList(Map<String, Object> map);
	
	/**
	 * 单条查询
	 * @param id
	 * @return
	 */
	CFtFundaccountInfo searchCFtFundaccountInfoById(String id);
	
	/**
	 * 保存或修改
	 * @return
	 */
	void saveOrUpdateCFtFundaccountInfo(MultipartHttpServletRequest request) throws Exception;
	
	/**
	 * 删除
	 * @param id
	 * @return
	 */
	int deleteCFtFundaccountInfo(String id);
}