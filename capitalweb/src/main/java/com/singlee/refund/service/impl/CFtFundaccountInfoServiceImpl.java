package com.singlee.refund.service.impl;


import java.util.List;
import java.util.Map;

import com.singlee.capital.base.util.FileManage;
import com.singlee.capital.common.pojo.UploadedFile;
import com.singlee.capital.common.util.JY;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.refund.service.CFtFundaccountInfoService;
import com.singlee.refund.mapper.CFtFundaccountInfoMapper;
import com.singlee.refund.model.CFtFundaccountInfo;
import org.springframework.web.multipart.MultipartHttpServletRequest;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class CFtFundaccountInfoServiceImpl implements CFtFundaccountInfoService {
	
	@Autowired
	private CFtFundaccountInfoMapper cFtFundaccountInfoMapper;

	@Override
	public Page<CFtFundaccountInfo> searchCFtFundaccountInfoList(Map<String, Object> map) {
		return cFtFundaccountInfoMapper.searchCFtFundaccountInfoPage(map, ParameterUtil.getRowBounds(map));
	}

	@Override
	public CFtFundaccountInfo searchCFtFundaccountInfoById(String id) {
		return cFtFundaccountInfoMapper.selectByPrimaryKey(id);
	}

	@Override
	public void saveOrUpdateCFtFundaccountInfo(MultipartHttpServletRequest request) throws Exception {
		// 1.文件获取
		List<UploadedFile> uploadedFileList = FileManage.requestExtractor(request);
		String managcomp = request.getParameter("managcomp");
		CFtFundaccountInfo obj = cFtFundaccountInfoMapper.selectByPrimaryKey(managcomp);
		boolean flag = false;
		if(obj == null){
			flag = true;
			obj = new CFtFundaccountInfo();
		}
		obj.setManagcomp(managcomp);
		obj.setManagcompnm(request.getParameter("managcompnm"));
		obj.setManagcompfnm(request.getParameter("managcompfnm"));
		obj.setFundaccount(request.getParameter("fundaccount"));
		obj.setFundtradeaccount(request.getParameter("fundtradeaccount"));
		obj.setFundaccountname(request.getParameter("fundaccountname"));
		obj.setFundbankname(request.getParameter("fundbankname"));
		obj.setRhbankno(request.getParameter("rhbankno"));
		obj.setFundbankno(request.getParameter("fundbankno"));

		if(uploadedFileList!=null && uploadedFileList.size()>0){
			UploadedFile uploadedFile =uploadedFileList.get(0);
			if(StringUtils.isNotEmpty(uploadedFile.getFullname())){
				if(!"pdf".equals(uploadedFile.getType())){
					JY.raiseRException("上传文件类型错误,仅允许上传pdf格式文件!");
				}
				obj.setMovie(uploadedFile.getBytes());
			}
		}
		if(flag){
			cFtFundaccountInfoMapper.insertSelective(obj);
		}else{
			cFtFundaccountInfoMapper.updateByPrimaryKeySelective(obj);
		}
	}

	@Override
	public int deleteCFtFundaccountInfo(String id) {
		return cFtFundaccountInfoMapper.deleteByPrimaryKey(id);
	}
}