package com.singlee.refund.service.impl;


import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.refund.service.CFtTposService;
import com.singlee.refund.mapper.CFtTposMapper;
import com.singlee.refund.model.CFtTpos;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class CFtInfoServiceImpl implements CFtTposService {
	
	@Autowired
	private CFtTposMapper cFtTposMapper;

	@Override
	public Page<CFtTpos> searchCFtTposList(Map<String, Object> map) {
		return cFtTposMapper.searchCFtTposPage(map, ParameterUtil.getRowBounds(map));
	}

	@Override
	public CFtTpos searchCFtTposByMap(Map<String, Object> map) {
		return cFtTposMapper.selectByMap(map);
	}
	
}