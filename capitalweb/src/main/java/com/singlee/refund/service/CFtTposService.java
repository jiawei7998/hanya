package com.singlee.refund.service;

import com.github.pagehelper.Page;
import com.singlee.refund.model.CFtTpos;

import java.util.Map;

/**
 * 基金持仓
 *
 */
public interface CFtTposService {

	/**
	 * 分页获取列表
	 * @param map
	 * @return
	 */
	Page<CFtTpos> searchCFtTposList(Map<String, Object> map);
	
	/**
	 * 单条持仓详情
	 * @param map
	 * @return
	 */
	CFtTpos searchCFtTposByMap(Map<String, Object> map);
}