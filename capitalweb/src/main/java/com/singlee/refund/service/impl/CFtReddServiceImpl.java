package com.singlee.refund.service.impl;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.github.pagehelper.Page;
import com.singlee.refund.model.CFtRedd;
import com.singlee.refund.service.CFtReddService;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.hrbcap.util.TradeConstants;
import com.singlee.ifs.mapper.IFSMapper;
import com.singlee.refund.mapper.CFtReddMapper;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class CFtReddServiceImpl implements CFtReddService {

	@Autowired
	private CFtReddMapper cFtReddMapper;
	@Resource
	IFSMapper ifsMapper;

	@Override
	public Page<CFtRedd> searchCFtReddList(Map<String, Object> params, Integer isFinished) {
		Page<CFtRedd> page = new Page<CFtRedd>();
		if (isFinished == 1) {// 待审批
			page = cFtReddMapper.searchCFtReddPageUnfinished(params, ParameterUtil.getRowBounds(params));
		} else if (isFinished == 2) {// 已审批
			page = cFtReddMapper.searchCFtReddPageFinished(params, ParameterUtil.getRowBounds(params));
		} else {// 我发起的
			page = cFtReddMapper.searchCFtReddPageMine(params, ParameterUtil.getRowBounds(params));
		}
		return page;
	}

	@Override
	public CFtRedd selectCFtReddByDealNo(String dealNo) {
		return cFtReddMapper.selectByPrimaryKey(dealNo);
	}

	@Override
	public CFtRedd selectCFtReddByMap(Map<String, Object> map) {
		return cFtReddMapper.selectByMap(map);
	}

	@Override
	public CFtRedd selectCFtReddById(Map<String, Object> map) {
		return cFtReddMapper.selectCFtReddById(map);
	}

	@Override
	public int saveOrUpdateCFtRedd(CFtRedd cFtRedd) {
		CFtRedd obj = cFtReddMapper.selectByPrimaryKey(cFtRedd.getDealNo());
		if(obj == null) {
			cFtRedd.setDealNo(getTicketId(TradeConstants.TrdType.FUND_REDD));
			cFtRedd.setApproveStatus(DictConstants.ApproveStatus.New);
			return cFtReddMapper.insertSelective(cFtRedd);
		}else {
			return cFtReddMapper.updateByPrimaryKeySelective(cFtRedd);
		}
	}

	@Override
	public int deleteCFtRedd(String dealNo) {
		return cFtReddMapper.deleteByPrimaryKey(dealNo);
	}
	
	//获取交易编号
	public String getTicketId(String str) {
		HashMap<String, Object> hashMap = new HashMap<String, Object>();
		// 前缀
		hashMap.put("sStr", "");
		// 贸易类型 正式交易
		hashMap.put("trdType", "2");
		String ticketId = ifsMapper.getTradeId(hashMap);
		return ticketId;
	}

}