package com.singlee.refund.service;

import java.util.Map;
import com.github.pagehelper.Page;
import com.singlee.refund.model.FtpManage;

/**
 * FTP价格管理
 *
 */
public interface FtpManageService {

	/**
	 * 分页查询
	 * @param params
	 * @return
	 */
	Page<FtpManage> searchFtpManageList(Map<String, Object> params);
	
	/**
	 * 单条查询
	 * @param id
	 * @return
	 */
	FtpManage searchFtpManageByid(Short id);
	
	/**
	 * 保存或者修改
	 * @param ftpManage
	 * @return
	 */
	int saveOrUpdateFtpManage(FtpManage ftpManage);
	
	/**
	 * 删除
	 * @param id
	 * @return
	 */
	int deleteFtpManage(Short id);
}