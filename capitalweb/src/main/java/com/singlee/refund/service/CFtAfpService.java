package com.singlee.refund.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.refund.model.CFtAfp;

/**
 * 基金申购
 *
 */
public interface CFtAfpService {

	/**
	 * 分页查询
	 * @param params
	 * @param isFinished
	 * @return
	 */
	Page<CFtAfp> searchCFtAfpList(Map<String, Object> params,Integer isFinished);
	
	/**
	 * 单条查询
	 * @param dealNo
	 * @return
	 */
	CFtAfp selectTaskIdByDealNo(String dealNo);


	CFtAfp searchCFtAfpById(Map<String,Object> map);
	
	/**
	 * 单条查询
	 * @param map
	 * @return
	 */
	CFtAfp selectTaskIdByMap(Map<String,Object> map);
	
	/**
	 * 保存或者修改
	 * @param cFtAfp
	 * @return
	 */
	int saveOrUpdateCFtAfp(CFtAfp cFtAfp);
	
	/**
	 * 删除
	 * @param dealNo
	 * @return
	 */
	int deleteCFtAfp(String dealNo);
}