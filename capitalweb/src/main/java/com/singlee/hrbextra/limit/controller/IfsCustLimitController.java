package com.singlee.hrbextra.limit.controller;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.bean.BussVrtyInfoArray;
import com.singlee.financial.bean.SlCustBean;
import com.singlee.financial.bean.SlDealModule;
import com.singlee.financial.bean.SlIfsBean;
import com.singlee.hrbextra.credit.mapper.HrbCreditPrdRelationMapper;
import com.singlee.hrbextra.credit.model.HrbCreditCustSub;
import com.singlee.hrbextra.limit.service.IfsCustLimitService;
import com.singlee.ifs.mapper.IfsOpicsSettleManageMapper;
import com.singlee.ifs.model.IfsOpicsCust;
import com.singlee.ifs.model.IfsTradeAccess;


@Controller
@RequestMapping(value = "IfsCustLimitController")
public class IfsCustLimitController {
    
    @Autowired
    IfsOpicsSettleManageMapper ifsOpicsSettleManageMapper;
    @Autowired
     HrbCreditPrdRelationMapper hrbCreditPrdRelationMapper;
    
    @Autowired
    IfsCustLimitService ifsCustLimitService;
    
    /**
     * 客户准入 删除
     */
    @ResponseBody
    @RequestMapping(value = "/deleteBussInfo")
    public RetMsg<Serializable> deleteBussInfo(@RequestBody Map<String, Object> map) {
        String nowDate = ifsOpicsSettleManageMapper.getCurDate();
        map.put("operator", SlSessionHelper.getUserId());
        map.put("oprTime", nowDate);
        map.put("oprType", "删除");
        ifsCustLimitService.deleteByEcifNo(map);
        return RetMsgHelper.ok();
    }
    
    /**
     * 客户准入 删除某个客户所有准入业务
     */
    @ResponseBody
    @RequestMapping(value = "/deleteAll")
    public RetMsg<Serializable> deleteAll(@RequestBody Map<String, Object> map) {
        String nowDate = ifsOpicsSettleManageMapper.getCurDate();
        map.put("operator", SlSessionHelper.getUserId());
        map.put("oprTime", nowDate);
        map.put("oprType", "删除");
        ifsCustLimitService.deleteByAll(map);
        return RetMsgHelper.ok();
    }
    
    
    
    /**
     * 客户准入 查询本地数据
     */
    @ResponseBody
    @RequestMapping(value = "/searchLocalInfo")
    public RetMsg<PageInfo<IfsTradeAccess>> searchLocalInfo(@RequestBody Map<String, String> map) {
        map.put("pageIndex", "1");
        map.put("pageSize", "10");
        Page<IfsTradeAccess> page = ifsCustLimitService.searchLocalInfo(map);
        return RetMsgHelper.ok(page);
    }
    
    /***
     * 查询所有已经准入的客户
     */
    @ResponseBody
    @RequestMapping(value = "/searchAllBuss")
    public RetMsg<PageInfo<IfsTradeAccess>> searchAllBuss(
            @RequestBody Map<String, Object> map) {
        Page<IfsTradeAccess> page = ifsCustLimitService.searchAllBuss(map);
        return RetMsgHelper.ok(page);
    }

    
    /***
     * 查询某个客户被准入的业务类型
     */
    @ResponseBody
    @RequestMapping(value = "/searchAllBussTpnm")
    public RetMsg<PageInfo<IfsTradeAccess>> searchAllBussTpnm(@RequestBody Map<String, Object> map) {
        Page<IfsTradeAccess> page = ifsCustLimitService.searchAllBussTpnm(map);
        return RetMsgHelper.ok(page);
    }
    
    /**
     * 客户准入 新增时保存
     */
    @ResponseBody
    @RequestMapping(value = "/bussInfoAdd")
    public RetMsg<Serializable> bussInfoAdd(@RequestBody Map<String, Object> map) {
        Map<String, Object> upmap = new HashMap<String, Object>();
        String coreClntNo = map.get("coreClntNo").toString();
        upmap.put("coreClntNo", coreClntNo);
        upmap.put("conm", map.get("conm"));
        upmap.put("branchCode", map.get("branchCode"));
        upmap.put("branchName", map.get("branchName"));
        upmap.put("inclSubFlg", map.get("inclSubFlg"));
        upmap.put("approvalDate", map.get("approvalDate"));
        upmap.put("dealType", map.get("dealType"));
        String tp = map.get("bussType").toString();
        String tpNm = map.get("bussTpnm").toString();
        String[] type = tp.split(",");
        String[] typeNm = tpNm.split(",");
        String nowDate = ifsOpicsSettleManageMapper.getCurDate();
        for (int i = 0; i < type.length; i++) {
            upmap.put("coreClntNo", coreClntNo);
            upmap.put("bussType", type[i]);
            upmap.put("bussTpnm", typeNm[i]);
            List<IfsTradeAccess> list = ifsCustLimitService.queryBussInfo(upmap);
            if (list.size() == 0) { // 若无此条数据，则插入
                upmap.put("operator", SlSessionHelper.getUserId());
                upmap.put("oprTime", nowDate);
                upmap.put("oprType", "新增");
                ifsCustLimitService.insertBussType(upmap);
            }else{
                return RetMsgHelper.ok(typeNm[i] + "-业务类型已存在！");
            }
        }
        return RetMsgHelper.ok("保存成功！");
    }
   
    
    /**
     * 客户准入 修改时保存
     */
    @ResponseBody
    @RequestMapping(value = "/bussInfoEdit")
    public RetMsg<Serializable> bussInfoEdit(@RequestBody Map<String, Object> map) {
        String nowDate = ifsOpicsSettleManageMapper.getCurDate();
        map.put("operator", SlSessionHelper.getUserId());
        map.put("oprTime", nowDate);
        map.put("oprType", "修改");

        /* 查业务类型编号 */
//        Map<String, Object> upmap = new HashMap<String, Object>();
//        upmap.put("coreClntNo", map.get("coreClntNo"));
//        upmap.put("bussTpnm", map.get("bussTpnm"));
//        List<IfsTradeAccess> list = ifsCustLimitService.queryBussInfo(upmap);

//        map.put("bussType", list.get(0).get("bussType"));
        ifsCustLimitService.updateByEcifNo(map);
        return RetMsgHelper.ok();
    }

    /***
     * 客户准入 查询单笔
     */
    @ResponseBody
    @RequestMapping(value = "/searchBuss")
    public Map<String, Object> searchBuss(@RequestBody Map<String, Object> map) {
        Map<String, Object> remap = ifsCustLimitService.searchBuss(map);
        
        return remap;
    }
    
    /***
     * 客户准入 查询单笔
     */
    @ResponseBody
    @RequestMapping(value = "/searchAllCust")
    public RetMsg<PageInfo<IfsOpicsCust>> searchAllCust(@RequestBody Map<String, Object> map) {
        Page<IfsOpicsCust> page = ifsCustLimitService.searchAllCust(map);
        
        return RetMsgHelper.ok(page);
    }
    
    
    /***
     * 判断交易对手是否已经客户准入
     */
    @ResponseBody
    @RequestMapping(value = "/searchLimit")
    public RetMsg<Serializable> searchLimit(@RequestBody Map<String, Object> map) {
//      if(map.get("prdName")==null) {
//          String prdName=ifsCustLimitService.searchPrdname(map);
//          map.put("prdName", prdName);
//      }
        
        String cno = ParameterUtil.getString(map, "cno", "");
        String prdNo = ParameterUtil.getString(map, "prdNo", "");
        String delType = ParameterUtil.getString(map, "dealType", "");
        
        List<IfsTradeAccess> remap = ifsCustLimitService.searchLimit(map);
        if(remap!=null) {
            for(IfsTradeAccess custmap : remap) {
            //机构号和业务类型都相同时则为真
               if( cno.equals(custmap.getCoreClntNo())&& prdNo.equals(custmap.getBussType())){
                  
                  return RetMsgHelper.ok();
               }
            }
        }
        return RetMsgHelper.simple("该交易对手没有进行客户准入，请重新选择");
      
        
       
    }

    /***
     * 判断交易对手的授信主体是否有额度
     */
    @ResponseBody
    @RequestMapping(value = "/searchCredit")
    public RetMsg<Serializable> searchCredit(@RequestBody Map<String, Object> map) {

        List<HrbCreditCustSub> remap = hrbCreditPrdRelationMapper.getCustOne(map);
        if(remap!=null) {
            
                return RetMsgHelper.ok();
           
        }
        return RetMsgHelper.simple("该交易对手没有额度，请重新选择");
      
        
       
    }
    
}