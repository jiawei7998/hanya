package com.singlee.hrbextra.limit.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.BussVrtyInfoArray;
import com.singlee.financial.bean.SlIfsBean;
import com.singlee.ifs.model.IfsOpicsCust;
import com.singlee.ifs.model.IfsTradeAccess;

public interface IfsCustLimitMapper {
	
    
    /**
     * 同业准入  查询本地数据
     */
    Page<IfsTradeAccess> searchLocalInfo(Map<String, String> map, RowBounds rb);
    
    
    /**
     * 查询所有已经准入的客户
     */
    Page<IfsTradeAccess> searchAllBuss(Map<String, Object> map, RowBounds rb);
    
    
    
    /**
     * 查询某个客户被准入的业务类型
     */
    Page<IfsTradeAccess> searchAllBussTpnm(Map<String, Object> map, RowBounds rb);
    /**
     * 根据ECIF客户号及业务类型名称删除某一条数据
     */
    void deleteByEcifNo(Map<String, Object> map);
    
    
    /**
     * 根据ECIF客户号删除某个客户所有准入业务
     */
    void deleteAll(Map<String, Object> map);
    
    //查询所有客户号
    Page<IfsOpicsCust> searchAllCust(Map<String, Object> map, RowBounds rb);
    
    /**
     *准入名单信息有修改时进行增加日志数据
     */
    public void insertOperateDetail(Map<String,Object> map);
    
    
    /**
     * 将查询出的数据插入ifs_trade_access表中
     */
    public void insertBussType(Map<String,Object> map);
    
    
    /**
     * 同业准入 查询是否有此条记录
     */
    List<IfsTradeAccess> queryBussInfo(Map<String, Object> map);
    
    
    /**
     * 查询所有客户准入名单
     */
    List<IfsTradeAccess> searchLimit(Map<String, Object> map);
    
    
    /**
     * 根据ECIF客户号更新某一条数据
     */
    void updateByEcifNo(Map<String, Object> map);
    
    public Map<String,Object> searchCust(Map<String,Object> map);
    
    
    public String  searchPrdname(Map<String,Object> map);
}