package com.singlee.hrbextra.limit.serviceImpl;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.financial.bean.BussVrtyInfoArray;
import com.singlee.financial.bean.SlIfsBean;
import com.singlee.hrbextra.limit.mapper.IfsCustLimitMapper;
import com.singlee.hrbextra.limit.service.IfsCustLimitService;
import com.singlee.ifs.model.IfsOpicsCust;
import com.singlee.ifs.model.IfsTradeAccess;


@Service(value = "IfsCustLimitServiceImpl")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsCustLimitServiceImpl implements IfsCustLimitService {

    @Autowired
    private IfsCustLimitMapper ifsCustLimitMapper;

    
    /**
     * 同业准入 查询本地数据
     */
    @Override
    public Page<IfsTradeAccess> searchLocalInfo(Map<String, String> map) {
        RowBounds rb = ParameterUtil.getRowBounds(map);
        Page<IfsTradeAccess> page = ifsCustLimitMapper.searchLocalInfo(map,rb);
        return page;
    }
    
    /**
     * 查询所有已经准入的客户
     */
    @Override
    public Page<IfsTradeAccess> searchAllBuss(Map<String, Object> map) {
        RowBounds rb = ParameterUtil.getRowBounds(map);
        Page<IfsTradeAccess> result = ifsCustLimitMapper.searchAllBuss(map, rb);
        return result;
    }
    
    
    /**
     *查询某个客户被准入的业务类型
     */
    @Override
    public Page<IfsTradeAccess> searchAllBussTpnm(Map<String, Object> map) {
        RowBounds rb = ParameterUtil.getRowBounds(map);
        Page<IfsTradeAccess> result = ifsCustLimitMapper.searchAllBussTpnm(map, rb);
        return result;
    }
    /**
     * 根据客户号去删除某一条记录
     */
    @Override
    public void deleteByEcifNo(Map<String, Object> map) {
        if(map.get("branchCode")==null) {
            map.put("branchCode", "null");
        }
        ifsCustLimitMapper.insertOperateDetail(map);
        ifsCustLimitMapper.deleteByEcifNo(map);
    }

   
    /**
     * 插入业务种类信息表
     */
    @Override
    public void insertBussType(Map<String, Object> map) {
        if(map.get("branchCode")==null) {
            map.put("branchCode", "null");
        }
        ifsCustLimitMapper.insertBussType(map);
        ifsCustLimitMapper.insertOperateDetail(map);
    }

    @Override
    public List<IfsTradeAccess> queryBussInfo(Map<String, Object> map) {
        return ifsCustLimitMapper.queryBussInfo(map);
    }

    /**
     * 根据客户号去更新某一条记录
     */
    @Override
    public void updateByEcifNo(Map<String, Object> map) {
        if(map.get("branchCode")==null) {
            map.put("branchCode", "null");
        }
        ifsCustLimitMapper.updateByEcifNo(map);
        ifsCustLimitMapper.insertOperateDetail(map);
    }

    @Override
    public Map<String, Object> searchBuss(Map<String, Object> map) {
       return ifsCustLimitMapper.searchCust(map);
       
    }

    @Override
    public Page<IfsOpicsCust> searchAllCust(Map<String, Object> map) {
        RowBounds rb = ParameterUtil.getRowBounds(map);
        return ifsCustLimitMapper.searchAllCust(map, rb);

    }

    @Override
    public List<IfsTradeAccess> searchLimit(Map<String, Object> map) {
        // TODO Auto-generated method stub
        return ifsCustLimitMapper.searchLimit(map);
    }

    @Override
    public String searchPrdname(Map<String, Object> map) {
        String prdName =ifsCustLimitMapper.searchPrdname(map);
        return prdName;
    }

    
    @Override
    public void deleteByAll(Map<String, Object> map) {
        if(map.get("branchCode")==null) {
            map.put("branchCode", "null");
        }
        if(map.get("bussTpnm")==null) {
            map.put("bussTpnm", "all");
        }
        ifsCustLimitMapper.insertOperateDetail(map);
        ifsCustLimitMapper.deleteAll(map);
        
    }
  
	
	}
