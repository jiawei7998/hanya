package com.singlee.hrbextra.limit.service;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.BussVrtyInfoArray;
import com.singlee.financial.bean.SlIfsBean;
import com.singlee.ifs.model.IfsOpicsCust;
import com.singlee.ifs.model.IfsTradeAccess;

public interface IfsCustLimitService {
    
    /**
     * 同业准入 查询本地数据
     */
    Page<IfsTradeAccess> searchLocalInfo(Map<String, String> map);
    
    

    /**
     * 查询所有已经准入的客户
     */
    Page<IfsTradeAccess> searchAllBuss(Map<String, Object> map);    
    
      //查询某个客户被准入的业务类型
    Page<IfsTradeAccess> searchAllBussTpnm(Map<String, Object> map);    
    /**
     * 根据客户号删除某一条数据
     */
    void deleteByEcifNo(Map<String, Object> map);
    
    
    /**
     * 删除某个客户所有准入业务
     */
    void deleteByAll(Map<String, Object> map);
    
    
    /**
     * 插入业务种类信息表
     */
    public void insertBussType(Map<String, Object> map);
    
    
    /**
     * 同业准入 查询是否有此条记录
     */
    List<IfsTradeAccess> queryBussInfo(Map<String, Object> map);

    
    /**
     * 根据ECIF客户号更新某一条数据
     */
    void updateByEcifNo(Map<String, Object> map);
    
    //通过客户号返回客户名称
    public Map<String,Object> searchBuss(Map<String,Object> map) ;
    
    
    Page<IfsOpicsCust> searchAllCust(Map<String, Object> map);
    
   
    List<IfsTradeAccess> searchLimit(Map<String, Object> map);
    
    String searchPrdname(Map<String, Object> map);
    
}