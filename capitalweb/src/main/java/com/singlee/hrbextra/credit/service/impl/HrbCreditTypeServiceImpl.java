package com.singlee.hrbextra.credit.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.hrbextra.credit.mapper.HrbCreditTypeMapper;
import com.singlee.hrbextra.credit.model.HrbCreditType;
import com.singlee.hrbextra.credit.service.HrbCreditTypeService;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class HrbCreditTypeServiceImpl implements HrbCreditTypeService {

    @Autowired
    HrbCreditTypeMapper hrbCreditTypeMapper;

    @Override
    public Page<HrbCreditType> queryCrediyTypePage(Map<String, Object> map) {
        return hrbCreditTypeMapper.queryCrediyTypePage(map, ParameterUtil.getRowBounds(map));
    }
    
    @Override
    public List<HrbCreditType> queryAllCrediyType(Map<String, Object> map) {
        return hrbCreditTypeMapper.queryAllCrediyType(map);
    }
    
    
    @Override
    public String insertCrediyType(HrbCreditType type) {
        type.setOperator(SlSessionHelper.getUserId()); 
        type.setOpTime(DateUtil.getCurrentDateAsString());
        
        String result = "ok";
        Map<String,Object> map = new HashMap<String,Object>();
        map.put("creType", type.getCreType());
        map.put("creTypeName", type.getCreTypeName());
        String count = countCreditType(map);
        if(count != null && Integer.parseInt(count) > 0) {
            return "error";
        }
        hrbCreditTypeMapper.insert(type);
        return result;
    }
    
    @Override
    public String countCreditType(Map<String,Object> map) {
        return hrbCreditTypeMapper.countCreditType(map);
    }
    
    @Override
    public void updateCrediyType(HrbCreditType type) {
        type.setOperator(SlSessionHelper.getUserId()); 
        type.setOpTime(DateUtil.getCurrentDateAsString());
        hrbCreditTypeMapper.updateByPrimaryKeySelective(type);
        return;
    }

    @Override
    public void deleteCrediyType(HrbCreditType type) {
        hrbCreditTypeMapper.delete(type);
    }
    
}
