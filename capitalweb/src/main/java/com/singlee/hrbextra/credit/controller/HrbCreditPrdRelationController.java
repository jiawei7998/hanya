package com.singlee.hrbextra.credit.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.hrbextra.credit.model.HrbCreditCustProd;
import com.singlee.hrbextra.credit.service.HrbCreditPrdRelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value="/HrbCreditPrdRelationController")
public class HrbCreditPrdRelationController  extends CommonController  {
    @Autowired
    HrbCreditPrdRelationService hrbCreditPrdRelationService;
    /**
     * 额度查询界面
     * @param map
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getHrbCreditQueryPage")
    public RetMsg<PageInfo<HrbCreditCustProd>> getHrbCreditQueryPage(@RequestBody Map<String, Object> map) {
        Page<HrbCreditCustProd> list = hrbCreditPrdRelationService.getHrbCreditQueryPage(map);
        return RetMsgHelper.ok(list);
    }

    /**
     * 额度查询界面
     * @param map
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getHrbCreditQueryList")
    public RetMsg<List<HrbCreditCustProd>> getHrbCreditQueryList(@RequestBody Map<String, Object> map) {
        List<HrbCreditCustProd> list = hrbCreditPrdRelationService.getHrbCreditQueryList(map);
        return RetMsgHelper.ok(list);
    }
    
    
    @ResponseBody
    @RequestMapping(value = "/saveInfo")
    public RetMsg<Serializable> saveInfo(@RequestBody HrbCreditCustProd hrbCreditCustProd) {
        String result = hrbCreditPrdRelationService.saveInfo(hrbCreditCustProd);
        if(result != null && "error".equals(result)) {
            return RetMsgHelper.simple("产品额度已存在");
        }
        return RetMsgHelper.ok();
    }
    
    @ResponseBody
    @RequestMapping(value = "/deleteInfo")
    public RetMsg<Serializable> deleteInfo(@RequestBody HrbCreditCustProd hrbCreditCustProd) {
        hrbCreditPrdRelationService.deleteInfo(hrbCreditCustProd);
        return RetMsgHelper.ok();
    }
}
