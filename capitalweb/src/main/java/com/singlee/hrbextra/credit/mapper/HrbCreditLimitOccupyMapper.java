package com.singlee.hrbextra.credit.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;

import com.singlee.financial.bean.LoanBean;
import com.singlee.hrbextra.credit.model.HrbCreditLimitOccupy;

import tk.mybatis.mapper.common.Mapper;

public interface HrbCreditLimitOccupyMapper extends Mapper<HrbCreditLimitOccupy> {

	/**
	 * 得到额度占用审批列表
	 */
	Page<HrbCreditLimitOccupy> getCreditDealListApprove(Map<String, Object> map, RowBounds rowBounds);

	Page<HrbCreditLimitOccupy> getCreditDealListFinish(Map<String, Object> map, RowBounds rowBounds);

	Page<HrbCreditLimitOccupy> getCreditDealListMine(Map<String, Object> map, RowBounds rowBounds);

	void deleteByDealNo(Map<String, Object> map);

	void statusChange(Map<String, Object> status);

	HrbCreditLimitOccupy getCreditDealByDealNo(String dealNo);
	
	HrbCreditLimitOccupy getCreditDealAllByDealNo(String dealNo);

	Page<HrbCreditLimitOccupy> getCreditDealAll(Map<String, Object> map, RowBounds rowBounds);

	Page<HrbCreditLimitOccupy> queryCreditReleaseDeal(Map<String, Object> map, RowBounds rowBounds);

	HrbCreditLimitOccupy queryCreditReleaseDealByDealno(Map<String, Object> map);

	void updateByMap(Map<String, Object> map);
	
	/**
     *获取该客户某个业务剩余的占用额度和已经释放的额度
     
     * @return
     */
    List<HrbCreditLimitOccupy> getBalance(Map<String, Object> map);
    
    
  
	/**
	 * 取明细台账
	 */
	List<LoanBean> getDetailEd();
	
	//交易金额增加权重，金额值为 金额*权重/100
	HrbCreditLimitOccupy getCreditDealByDealNoAddWeight(String dealNo);
	
	//由手工释放由新交易流水号查询到老交易流水号
	public String getTicketIdOld (@Param("serial_no")String serial_no);
	
	
	public void OccupyInsert(Map<String,Object> map);
}