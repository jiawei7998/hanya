package com.singlee.hrbextra.credit.controller;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;


import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.hrbextra.credit.mapper.HrbCreditPrdRelationMapper;
import com.singlee.hrbextra.credit.model.HrbCreditCustProd;
import com.singlee.hrbextra.credit.model.HrbCreditCustSub;
import com.singlee.hrbextra.credit.model.HrbCreditLimitOccupy;
import com.singlee.hrbextra.credit.service.HrbCreditLimitOccupyService;

@Controller
@RequestMapping(value = "/HrbCreditLimitOccupyController")
public class HrbCreditLimitOccupyController extends CommonController {

	@Autowired
	HrbCreditLimitOccupyService limitOccupy;
	@Autowired
	HrbCreditPrdRelationMapper hrbCreditPrdRelationMapper;
	
	/**
	 * 新增一笔额度占用审批
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/addHrbCreditLimitOccupy")
	public RetMsg<Serializable> addHrbCreditLimitOccupy(@RequestBody Map<String,Object> map) {
		
		limitOccupy.addHrbCreditLimitOccupy(map);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 得到发起的额度占用审批列表
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/getCreditDealList")
	public RetMsg<PageInfo<HrbCreditLimitOccupy>> getCreditDealList(@RequestBody Map<String,Object> map) {
		map.put("userId", SlSessionHelper.getUserId());
		Page<HrbCreditLimitOccupy> list = limitOccupy.getCreditDealListMine(map);
		return RetMsgHelper.ok(list);
	}
	
	/**
	 * 得到待审批额度占用审批列表
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/getCreditDealWaitApprove")
	public RetMsg<PageInfo<HrbCreditLimitOccupy>> getCreditDealWaitApprove(@RequestBody Map<String,Object> map) {
		map.put("userId", SlSessionHelper.getUserId());
		Page<HrbCreditLimitOccupy> list = limitOccupy.getCreditDealListApprove(map);
		return RetMsgHelper.ok(list);
	}
	
	/**
	 * 得到审批完成额度占用审批列表
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/getCreditDealApprovedPass")
	public RetMsg<PageInfo<HrbCreditLimitOccupy>> getCreditDealApprovedPass(@RequestBody Map<String,Object> map) {
		map.put("userId", SlSessionHelper.getUserId());
		Page<HrbCreditLimitOccupy> list = limitOccupy.getCreditDealListFinish(map);
		return RetMsgHelper.ok(list);
	}
	
	
	/**
	 * 删除一笔额度占用审批
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteCreditDeal")
	public RetMsg<Serializable> deleteCreditDeal(@RequestBody Map<String,Object> map) {
		limitOccupy.deleteCreditDeal(map);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 得到所有占用审批列表
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/getCreditDealAll")
	public RetMsg<PageInfo<HrbCreditLimitOccupy>> getCreditDealAll(@RequestBody Map<String,Object> map) {
		Page<HrbCreditLimitOccupy> list = limitOccupy.getCreditDealAll(map);
		return RetMsgHelper.ok(list);
	}
	
	/**
	 * 修改额度占用审批
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/updateCreditDeal")
	public RetMsg<Serializable> updateCreditDeal(@RequestBody Map<String,Object> map) {
		limitOccupy.updateCreditDeal(map);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 查询所有有余额的交易
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/queryCreditReleaseDeal")
	public RetMsg<PageInfo<HrbCreditLimitOccupy>> queryCreditReleaseDeal(@RequestBody Map<String,Object> map) {
		Page<HrbCreditLimitOccupy> list = limitOccupy.queryCreditReleaseDeal(map,ParameterUtil.getRowBounds(map));
		return RetMsgHelper.ok(list);
	}
	
	/**
	 * 查询需要释放额度的交易
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/queryCreditReleaseDealByDealno")
	public RetMsg<HrbCreditLimitOccupy> queryCreditReleaseDealByDealno(@RequestBody Map<String,Object> map) {
		HrbCreditLimitOccupy bean = limitOccupy.queryCreditReleaseDealByDealno(map);
		return RetMsgHelper.ok(bean);
	}
	
	/**
	 * 查询需要释放额度的交易
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/getCreditDealByDealNo")
	public RetMsg<HrbCreditLimitOccupy> getCreditDealByDealNo(@RequestBody Map<String,Object> map) {
		HrbCreditLimitOccupy bean = limitOccupy.getCreditDealByDealNo(String.valueOf(map.get("dealNo")));
		return RetMsgHelper.ok(bean);
	}
	
	
	   /**
     * 根据授信类型、币种、客户号、业务号来查询产品额度详情和授信主体的总额和余额
     * @param map
     */
    @ResponseBody
    @RequestMapping(value = "/checklimit")
    public RetMsg< List<HrbCreditCustSub>> checklimit(@RequestBody Map<String,Object> map) {
		Object[] bondCodes = ParameterUtil.getList(map, "bondCodes");
		String custName = ParameterUtil.getString(map, "custName", null);

		if (null != bondCodes && bondCodes.length > 0) {
			String[] stringArray = Arrays.copyOf(bondCodes, bondCodes.length, String[].class);
			map.put("bondCodes", stringArray);
		}

		List<HrbCreditCustSub> bean = null;

		if (null != custName && !"".equals(custName)) {
			bean = hrbCreditPrdRelationMapper.getCustOne(map);
		} else {
			bean = hrbCreditPrdRelationMapper.getCustOneBySecm(map);
		}
		return RetMsgHelper.ok(bean);
    }
	
}
