package com.singlee.hrbextra.credit.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.hrbextra.credit.mapper.HrbCreditCustMapper;
import com.singlee.hrbextra.credit.model.HrbCreditCustQuota;
import com.singlee.hrbextra.credit.service.HrbCreditCustService;
import com.singlee.hrbnewport.service.CommonExportService;
import com.singlee.hrbreport.service.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class HrbCreditCustServiceImpl implements HrbCreditCustService,ReportService, CommonExportService {
    @Autowired
    HrbCreditCustMapper hrbCreditCustMapper;
    
    @Override
    public Page<HrbCreditCustQuota> getHrbCreditQueryPage(Map<String, Object> map) {
        String customer = ParameterUtil.getString(map, "customer", "");
        String ecifNum = ParameterUtil.getString(map, "ecifNum", "");
        map.put("customer", customer.trim());
        map.put("ecifNum",ecifNum.trim());
        return hrbCreditCustMapper.getHrbCreditQueryPage(map, ParameterUtil.getRowBounds(map));
    }
    
    @Override
    public String insertOrUpdateCreditCust(HrbCreditCustQuota creditCust) {
        creditCust.setOperator(SlSessionHelper.getUserId()); 
        creditCust.setOpTime(DateUtil.getCurrentDateAsString());
        
      //通过CustCreditId是否有值判断是insert还是update
        String result = "";
        if(StringUtil.isNullOrEmpty(creditCust.getCustCreditId())) {
            result = insertCreditCust(creditCust);
        }else {
            result = updateCreditCust(creditCust);
        }
        return result;
    }
    
    private String insertCreditCust(HrbCreditCustQuota creditCust) {
      //根据 客户号 币种 额度种类 查询,若查到数据则不再插入
        Map<String,Object> map = new HashMap<String,Object>();
        map.put("ecifNum", creditCust.getEcifNum());
        map.put("creditType", creditCust.getCreditType());
//        map.put("currency", creditCust.getCurrency());
        String count = countCustCreditById(map);
        if(count != null && Integer.parseInt(count) > 0) {
            return "error";
        }
        hrbCreditCustMapper.insert(creditCust);
        return "ok";
    }
    
    private String updateCreditCust(HrbCreditCustQuota creditCust) {
        hrbCreditCustMapper.updateByPrimaryKeySelective(creditCust);
        return "ok";
    }
    
    @Override
    public void deleteCreditCust(HrbCreditCustQuota creditCust) {
        hrbCreditCustMapper.delete(creditCust);
    }
    
    @Override
    public String countCustCreditById(Map<String, Object> map) {
        return hrbCreditCustMapper.countCustCreditById(map);
    }

    @Override
    public Map<String, Object> getReportMap(Map<String, String> paramer, Date postdate) {
        return null;
    }

    @Override
    public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
        List<HrbCreditCustQuota> list= null;
        list=hrbCreditCustMapper.getHrbCreditQueryList(paramer);
        Map<String, Object> map = new HashMap<>();
        map.put("list",list);
        return map;
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String, Object> result = new HashMap<String, Object>();
        List<HrbCreditCustQuota> hrbCreditQueryList = hrbCreditCustMapper.getHrbCreditQueryList(map);
        result.put("list",hrbCreditQueryList);
        return result;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}
