package com.singlee.hrbextra.credit.mapper;

import com.github.pagehelper.Page;
import com.singlee.hrbextra.credit.model.HrbCreditCustProd;
import com.singlee.hrbextra.credit.model.HrbCreditCustSub;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

public interface HrbCreditPrdRelationMapper extends Mapper<HrbCreditCustProd>  {
    Page<HrbCreditCustProd> getHrbCreditQueryPage(Map<String, Object> map, RowBounds rowBounds);
    
    List<HrbCreditCustProd> getHrbCreditQueryList(Map<String, Object> map);
    
    //根据授信类型、币种、客户号、业务号来查询产品额度详情
    List<HrbCreditCustProd> getHrbCreditQueryOne(Map<String, Object> map);
    
  //根据授信类型、币种、客户号、业务号来查询产品额度详情和授信主体的总额和余额
    List<HrbCreditCustSub> getCustOne(Map<String, Object> map);

    List<HrbCreditCustSub> getCustOneBySecm(Map<String, Object> map);

    HrbCreditCustProd getHrbCreditCustProd(Map<String, Object> map);
    
    String countProdById(Map<String, Object> map);

    void deleteCreditCustProd(HrbCreditCustProd hrbCreditCustProd);
}
