package com.singlee.hrbextra.credit.controller;

import java.io.Serializable;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.hrbextra.credit.model.HrbCreditCustQuota;
import com.singlee.hrbextra.credit.service.HrbCreditCustService;

@Controller
@RequestMapping(value="/hrbCreditCustController")
public class HrbCreditCustController extends CommonController {
    @Autowired
    HrbCreditCustService hrbCreditCustService;
    /**
     * 额度查询界面
     * @param map
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getHrbCreditQueryPage")
    public RetMsg<PageInfo<HrbCreditCustQuota>> getHrbCreditQueryPage(@RequestBody Map<String, Object> map) {
        Page<HrbCreditCustQuota> list = hrbCreditCustService.getHrbCreditQueryPage(map);
        return RetMsgHelper.ok(list);
    }
    
    
    /**
     * _添加和修改客户额度信息
     * @param map
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/insertAndUpdate")
    public RetMsg<Serializable> insertAndUpdate(@RequestBody HrbCreditCustQuota creditCust) {
        String result = hrbCreditCustService.insertOrUpdateCreditCust(creditCust);
        
        if(result != null && "error".equals(result)) {
            return RetMsgHelper.simple("额度已存在或者同一授信主体同一额度类型不能有多个币种的授信信息");
        }
        return RetMsgHelper.ok();
    }
    
    /**
     * _删除选中的额度
     * @param creditCust
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/deleteCreditCust")
    public RetMsg<Serializable> deleteCreditCust(@RequestBody HrbCreditCustQuota creditCust) {
        hrbCreditCustService.deleteCreditCust(creditCust);
        return RetMsgHelper.ok();
    }
    
}
