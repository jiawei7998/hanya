package com.singlee.hrbextra.credit.service;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.hrbextra.credit.model.HrbCreditType;

public interface HrbCreditTypeService {

    Page<HrbCreditType> queryCrediyTypePage(Map<String, Object> map);
    
    List<HrbCreditType> queryAllCrediyType(Map<String, Object> map);
    
    String insertCrediyType(HrbCreditType type);
    
    void updateCrediyType(HrbCreditType type);
    
    void deleteCrediyType(HrbCreditType type);
    
    String countCreditType(Map<String,Object> map);
    
}
