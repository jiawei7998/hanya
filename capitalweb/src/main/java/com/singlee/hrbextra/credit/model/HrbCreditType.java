package com.singlee.hrbextra.credit.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * _额度种类表
 * @author zk
 */
@Entity
@Table(name="TD_ED_TYPE")
public class HrbCreditType implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 额度种类编码
     */
    @Id
    private String creType;
    /**
     * 额度种类
     */
    private String creTypeName;
    /**
     * 操作员
     */
    private String operator;
    /**
     * 操作时间
     */
    private String opTime;
    public String getCreType() {
        return creType;
    }
    public void setCreType(String creType) {
        this.creType = creType;
    }
    public String getCreTypeName() {
        return creTypeName;
    }
    public void setCreTypeName(String creTypeName) {
        this.creTypeName = creTypeName;
    }
    public String getOperator() {
        return operator;
    }
    public void setOperator(String operator) {
        this.operator = operator;
    }
    public String getOpTime() {
        return opTime;
    }
    public void setOpTime(String opTime) {
        this.opTime = opTime;
    }
    
    
}
