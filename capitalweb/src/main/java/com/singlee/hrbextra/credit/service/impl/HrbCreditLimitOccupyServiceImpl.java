package com.singlee.hrbextra.credit.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.approve.mapper.TrdOrderMapper;
import com.singlee.capital.common.util.*;
import com.singlee.capital.credit.dict.DictConstants;
import com.singlee.capital.credit.mapper.TcProductCreditMapper;
import com.singlee.capital.credit.model.TcProductCredit;
import com.singlee.capital.eu.mapper.TdEdCustBatchStatusMapper;
import com.singlee.capital.eu.mapper.TdEdDealLogMapper;
import com.singlee.capital.eu.model.TdEdCustBatchStatus;
import com.singlee.capital.eu.model.TdEdDealLog;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.opics.IAcupServer;
import com.singlee.hrbextra.credit.mapper.HrbCreditCustMapper;
import com.singlee.hrbextra.credit.mapper.HrbCreditLimitOccupyMapper;
import com.singlee.hrbextra.credit.mapper.HrbCreditPrdRelationMapper;
import com.singlee.hrbextra.credit.model.HrbCreditCustProd;
import com.singlee.hrbextra.credit.model.HrbCreditCustQuota;
import com.singlee.hrbextra.credit.model.HrbCreditLimitOccupy;
import com.singlee.hrbextra.credit.service.HrbCreditCustService;
import com.singlee.hrbextra.credit.service.HrbCreditLimitOccupyService;
import com.singlee.hrbextra.credit.service.HrbCreditPrdRelationService;
import com.singlee.ifs.mapper.IfsFlowMonitorMapper;
import com.singlee.ifs.mapper.IfsOpicsCustMapper;
import com.singlee.ifs.model.IfsOpicsCust;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional(value = "transactionManager", rollbackFor = Exception.class)
public class HrbCreditLimitOccupyServiceImpl implements HrbCreditLimitOccupyService {

	@Autowired
	HrbCreditLimitOccupyMapper LimitOccupyMapper;

	@Autowired
	IfsOpicsCustMapper ifsOpicsCustMapper;
	@Autowired
	HrbCreditLimitOccupyMapper hrbCreditLimitOccupyMapper;

	@Autowired
	HrbCreditPrdRelationMapper hrbCreditPrdRelationMapper;

	@Autowired
	HrbCreditPrdRelationService hrbCreditPrdRelationService;

	@Autowired
	HrbCreditCustService hrbCreditCustService;

	@Autowired
	private TrdOrderMapper approveManageDao;
	@Autowired
	TdEdDealLogMapper tdDealLogMapper;

	@Autowired
	IfsFlowMonitorMapper ifsFlowMonitorMapper;

	@Autowired
	TcProductCreditMapper tcProductCreditMapper;

	@Autowired
	TdEdCustBatchStatusMapper tdedCustBatchStatusMapper;

	@Autowired
	HrbCreditCustMapper hrbCreditCustMapper;
	@Autowired
	IAcupServer acupServer;

	// 手工额度占用
	@Override
	public void addHrbCreditLimitOccupy(Map<String, Object> map) {
		map.put("approveStatus", com.singlee.slbpm.dict.DictConstants.ApproveStatus.New);
		String trdtype = DictConstants.CREDIT_TYPE.USED;
		HashMap<String, Object> hashMap = new HashMap<String, Object>();
		hashMap.put("trdtype", trdtype);
		if("1".equals(map.get("isoccupy"))){
			String order_id = approveManageDao.getOrderId(hashMap);
			map.put("dealNo", order_id);
		}else if("0".equals(map.get("isoccupy"))){
			map.put("oldDealNo", map.get("oldDealNo"));
			map.put("dealNo", map.get("oldDealNo")+"_R");
			map.put("creditsubno", map.get("custNo"));
		}

		/* map.put("inputDate", DateUtil.getCurrentDateAsString()); */
		map.put("inputTime", DateUtil.getCurrentTimeAsString());
		map.put("modifyDate", DateUtil.getCurrentDateTimeAsString());
//		map.put("isoccupy","isoccupy");	
//		map.put("balance",map.get("amt"));
//		map.put("released",0);

		LimitOccupyMapper.insert(mapTocreditDeal(map));
	}

	// 判断占用额度是否超额

	/**
	 * map转换成需要的TdOverDueConfirm对象
	 * 
	 * @param map
	 * @return
	 */
	public HrbCreditLimitOccupy mapTocreditDeal(Map<String, Object> map) {
		HrbCreditLimitOccupy limitOccupy = new HrbCreditLimitOccupy();
		try {
			BeanUtil.populate(limitOccupy, map);
		} catch (Exception e) {
			JY.raise(e.getMessage());
		}
		return limitOccupy;
	}

	/**
	 * 得到额度占用审批列表
	 */
	@Override
	public Page<HrbCreditLimitOccupy> getCreditDealListApprove(Map<String, Object> map) {
		Page<HrbCreditLimitOccupy> page = LimitOccupyMapper.getCreditDealListApprove(map,
				ParameterUtil.getRowBounds(map));
		return page;
	}

	/**
	 * 得到额度占用审批列表
	 */
	@Override
	public Page<HrbCreditLimitOccupy> getCreditDealListFinish(Map<String, Object> map) {
		return LimitOccupyMapper.getCreditDealListFinish(map, ParameterUtil.getRowBounds(map));
	}

	/**
	 * 得到额度占用审批列表
	 */
	@Override
	public Page<HrbCreditLimitOccupy> getCreditDealListMine(Map<String, Object> map) {
		return LimitOccupyMapper.getCreditDealListMine(map, ParameterUtil.getRowBounds(map));
	}

	@Override
	public void deleteCreditDeal(Map<String, Object> map) {
		LimitOccupyMapper.deleteByPrimaryKey(map.get("dealNo"));
	}

	@Override
	public void updateCreditDeal(Map<String, Object> map) {
		map.put("modifyDate", DateUtil.getCurrentDateTimeAsString());
		LimitOccupyMapper.updateByMap(map);
	}

	@Override
	public void statusChange(Map<String, Object> status) {
		status.put("modifyDate", DateUtil.getCurrentDateTimeAsString());
		LimitOccupyMapper.statusChange(status);
	}

	@Override
	public HrbCreditLimitOccupy getCreditDealByDealNo(String serial_no) {
		return LimitOccupyMapper.getCreditDealByDealNo(serial_no);
	}

	@Override
	public Page<HrbCreditLimitOccupy> getCreditDealAll(Map<String, Object> map) {
		return LimitOccupyMapper.getCreditDealAll(map, ParameterUtil.getRowBounds(map));
	}

	@Override
	public Page<HrbCreditLimitOccupy> queryCreditReleaseDeal(Map<String, Object> map, RowBounds rowBounds) {
		return LimitOccupyMapper.queryCreditReleaseDeal(map, rowBounds);
	}

	@Override
	public HrbCreditLimitOccupy queryCreditReleaseDealByDealno(Map<String, Object> map) {
		return LimitOccupyMapper.queryCreditReleaseDealByDealno(map);
	}

	@Override
	public HrbCreditLimitOccupy getCreditDealByDealNoAddWeight(String serial_no) {
		return LimitOccupyMapper.getCreditDealByDealNoAddWeight(serial_no);
	}

	// 总表和产品表的占用和释放
	@Override
	public void AmtChange(Map<String, Object> map) {

		HrbCreditLimitOccupy hrbCredit = new HrbCreditLimitOccupy();
		if ("0".equals(map.get("OldNew"))) {
			// 获取当前占用详情
			hrbCredit = hrbCreditLimitOccupyMapper.getCreditDealAllByDealNo((String) map.get("serial_no"));
			hrbCredit.setCustNo(hrbCredit.getCreditsubno());
			hrbCredit.setCustName(hrbCredit.getCreditsubnm());
			BigDecimal divide = new BigDecimal(hrbCredit.getWeight()).divide(new BigDecimal("100"), 2, RoundingMode.HALF_UP);
			hrbCredit.setAmt(hrbCredit.getAmt().multiply(divide));
			BigDecimal changeAmt = ccyChange(hrbCredit.getAmt(), hrbCredit.getCurrency(), (String) map.get("currencyChange"));
			hrbCredit.setAmt(changeAmt);
			map.put("creTypeName", hrbCredit.getCreTypeName());

		} else {
			String amt = map.get("amt").toString();
			hrbCredit.setAmt(new BigDecimal(amt).setScale(2, RoundingMode.HALF_UP));//取两位精度  四舍五入
			hrbCredit.setCustNo((String) map.get("custNo"));
			hrbCredit.setCustName((String) map.get("custName"));
			hrbCredit.setProductCode((String) map.get("productCode"));
			hrbCredit.setProductName((String) map.get("productName"));
			hrbCredit.setCurrency((String) map.get("currency"));
			hrbCredit.setCreTypeName((String) map.get("creTypeName"));
			int isoccupy = Integer.parseInt((String) map.get("isoccupy"));
			hrbCredit.setIsoccupy(isoccupy);
			int state = Integer.parseInt((String) map.get("state"));
			hrbCredit.setState(state);
			hrbCredit.setVdate((String) map.get("vdate"));
			hrbCredit.setMdate((String) map.get("mdate"));
			hrbCredit.setInstitution((String) map.get("institution"));
			hrbCredit.setIoper((String) map.get("ioper"));
			hrbCredit.setInputTime((String) map.get("inputTime"));
			hrbCredit.setModifyDate((String) map.get("modifyDate"));
			hrbCredit.setOffLine((String) map.get("offLine"));
			hrbCredit.setWeight((String) map.get("weight"));
			hrbCredit.setDealNo((String) map.get("dealNo"));
			String approveStatus = com.singlee.slbpm.dict.DictConstants.ApproveStatus.ApprovedPass;
			int approveStatuss = Integer.parseInt(approveStatus);
			hrbCredit.setApproveStatus(approveStatuss);
		}

		int isoccipy = hrbCredit.getIsoccupy();
		// 额度占用操作
		Map<String, Object> remap = new HashMap<>();
		remap.put("custName", hrbCredit.getCustNo());
//		remap.put("currency", map.get("currency"));
		remap.put("prodType", hrbCredit.getProductCode());
		remap.put("creTypeName", map.get("creTypeName"));
		//加锁处理FOR UPDATE
		HrbCreditCustQuota hccq = hrbCreditCustMapper.getHrbCreditCustQuota(remap);
		if (null == hccq) {
			JY.raiseRException("未找到" + hrbCredit.getCustNo() + "授信主体下" + map.get("creTypeName") + "额度类型的总额度");
		}
		remap.put("custCreditId", hccq.getCustCreditId());
		HrbCreditCustProd prod = hrbCreditPrdRelationMapper.getHrbCreditCustProd(remap);
		if (null == prod) {
			JY.raiseRException("未找到" + hrbCredit.getCustNo() + "授信主体下" + map.get("creTypeName") + "额度类型" + hrbCredit.getProductCode() + "产品的总额度");
		}
		HrbCreditCustProd hrbCreditCustProd = prod;
		String ProLoanAmt = prod.getProLoanAmt();
		String OldAmt = prod.getProFrozenAmt();
		String ProFrozenAmt;
		if (isoccipy == 1) {

			/**
			 * 修改占用明细
			 */
			// 占用额度（已经占用的额度+本次释放的额度）
			ProFrozenAmt = new BigDecimal(OldAmt).add(hrbCredit.getAmt()).toString();
			hrbCreditCustProd.setProFrozenAmt(ProFrozenAmt);
			// 剩余额度（授信额度-已用额度）
			String ProAvlAmt = new BigDecimal(ProLoanAmt).subtract(new BigDecimal(ProFrozenAmt)).toString();
			if(new BigDecimal(ProAvlAmt).doubleValue()<0){
				JY.error("本次交易金额超过了该授信主体该产品的授权金额，请重新确认");
				JY.raiseRException("本次交易金额超过了该授信主体该产品的授权金额，请重新确认");
			}
			hrbCreditCustProd.setProAvlAmt(ProAvlAmt);
			// 一、每次额度占用审批结束之后对产品额度表进行更新（TD_ED_CUST_PROD）
			hrbCreditPrdRelationService.saveInfo(hrbCreditCustProd);
			/**
			 * 修改占用总表
			 */
			HrbCreditCustQuota creditCust = hccq;
			//占用额度（已经占用的额度+本次释放的额度）
			String custFrozenAmt = new BigDecimal(creditCust.getFrozenAmt()).add(hrbCredit.getAmt()).toString();
			creditCust.setFrozenAmt(custFrozenAmt);
			//剩余额度（授信额度-已用额度）
			String custAvlAmt = new BigDecimal(creditCust.getLoanAmt()).subtract(new BigDecimal(custFrozenAmt)).toString();
			if(new BigDecimal(custAvlAmt).doubleValue()<0){
				JY.error("本次交易金额超过了该授信主体的总授权金额，请重新确认");
				JY.raiseRException("本次交易金额超过了该授信主体的总授权金额，请重新确认");
			}
			creditCust.setAvlAmt(custAvlAmt);
			// 插入修改后的数据到总表
			hrbCreditCustService.insertOrUpdateCreditCust(creditCust);

			//创建日志
			Map<String, Object> log = new HashMap<>();
			log.put("dealNo", hrbCredit.getDealNo());
			// vdate,mdate
			log.put("vDate", hrbCredit.getVdate());
			log.put("mDate", hrbCredit.getMdate());
			// 产品名称
			log.put("prdNo", hrbCredit.getProductName());
			// 客户编号
			log.put("ecifNo", hrbCredit.getCustNo());
			// 产品编号
			log.put("productCode", hrbCredit.getProductCode());
			// 审批日期
			log.put("postDate", new Date());
			// 额度操作金额
			log.put("edOpAmt", hrbCredit.getAmt());
			// 客户唯一主键
			log.put("custCreditId", creditCust.getCustCreditId());
			// 客户可用授信额度总额（剩余可用）
			log.put("avlAmtEcif", custAvlAmt);
			// 授信产品可用授信额度（产品表的剩余可用）
			log.put("avlAmtCredit", ProAvlAmt);
			// 客户授信额度总额（总表的客户授信额度总额）
			log.put("loanAmtEcif", creditCust.getLoanAmt());
			// 授信产品额度总额（产品表的授信总额）
			log.put("loanAmtCredit", ProLoanAmt);
			// 额度操作类型(1:占用，0：释放)
			log.put("edOpType", hrbCredit.getIsoccupy());
			// 额度种类
			log.put("creditId", hrbCredit.getCreTypeName());

			log.put("currency", hrbCredit.getCurrency());

			log.put("custName", hrbCredit.getCustName());
			
			log.put("isActive", "1");
			log.put("dealType", "1");//额度占用
			TdEdDealLog dealLog = tdDealLogMapper.selectDealLog(hrbCredit.getDealNo());
			if(null == dealLog) {
				tdDealLogMapper.InsertDealLog(log);
			}else {
				tdDealLogMapper.deleteDealLog(hrbCredit.getDealNo());
				tdDealLogMapper.InsertDealLog(log);
			}
		}
		// 额度释放操作
		else {

			/**
			 * 修改占用明细
			 */
			// 占用额度（已经占用的额度-本次释放的额度）
			ProFrozenAmt = new BigDecimal(OldAmt).subtract(hrbCredit.getAmt()).toString();
			if(new BigDecimal(ProFrozenAmt).doubleValue()<0){
				JY.error("本次交易金额超过了该授信主体该产品已占用的金额，请重新确认");
				JY.raiseRException("本次交易金额超过了该授信主体该产品已占用的金额，请重新确认");
			}
			hrbCreditCustProd.setProFrozenAmt(ProFrozenAmt);
			// 剩余额度（授信额度-已用额度）
			String ProAvlAmt = new BigDecimal(ProLoanAmt).subtract(new BigDecimal(ProFrozenAmt)).toString();
			hrbCreditCustProd.setProAvlAmt(ProAvlAmt);
			// 一、每次额度占用审批结束之后对产品额度表进行更新（TD_ED_CUST_PROD）
			hrbCreditPrdRelationService.saveInfo(hrbCreditCustProd);

			/**
			 * 修改占用总表
			 */
			HrbCreditCustQuota creditCust = hccq;
			//占用额度（已经占用的额度-本次释放的额度）
			String custFrozenAmt = new BigDecimal(creditCust.getFrozenAmt()).subtract(hrbCredit.getAmt()).toString();
			if(new BigDecimal(custFrozenAmt).doubleValue()<0){
				JY.error("本次交易金额超过了该授信主体已占用的金额，请重新确认");
				JY.raiseRException("本次交易金额超过了该授信主体已占用的金额，请重新确认");
			}
			creditCust.setFrozenAmt(custFrozenAmt);
			//剩余额度（授信额度-已用额度）
			String custAvlAmt = new BigDecimal(creditCust.getLoanAmt()).subtract(new BigDecimal(custFrozenAmt)).toString();
			creditCust.setAvlAmt(custAvlAmt);
			// 插入修改后的数据到总表
			hrbCreditCustService.insertOrUpdateCreditCust(creditCust);


			Map<String, Object> log = new HashMap<>();
			log.put("dealNo", hrbCredit.getDealNo());
			// vdate,mdate暂时不用
//             log.put("vDate", value);
//             log.put("mDate", value);
			// 产品名称
			log.put("prdNo", hrbCredit.getProductName());
			// 客户编号
			log.put("ecifNo", hrbCredit.getCustNo());
			// 产品编号
			log.put("productCode", hrbCredit.getProductCode());
			// 审批日期
			log.put("postDate", new Date());
			// 额度操作金额
			log.put("edOpAmt", hrbCredit.getAmt());
			// 客户唯一主键
			log.put("custCreditId", creditCust.getCustCreditId());
			// 客户可用授信额度总额（剩余可用）
			log.put("avlAmtEcif", custAvlAmt);
			// 授信产品可用授信额度（产品表的剩余可用）
			log.put("avlAmtCredit", ProAvlAmt);
			// 客户授信额度总额（总表的客户授信额度总额）
			log.put("loanAmtEcif", creditCust.getLoanAmt());
			// 授信产品额度总额（产品表的授信总额）
			log.put("loanAmtCredit", ProLoanAmt);
			// 额度操作类型(1:占用，0：释放)
			log.put("edOpType", hrbCredit.getIsoccupy());
			// 额度授信种类
			log.put("creditId", hrbCredit.getCreTypeName());

			log.put("currency", hrbCredit.getCurrency());

			log.put("custName", hrbCredit.getCustName());
			
			log.put("isActive", "1");
			log.put("dealType", "2");//额度释放
			
			tdDealLogMapper.updateNoUseDealStatus(hrbCredit.getDealNo().replace("_R", ""));
			TdEdDealLog dealLog = tdDealLogMapper.selectDealLog(hrbCredit.getDealNo());
			if(null == dealLog) {
				tdDealLogMapper.InsertDealLog(log);
			}else {
				tdDealLogMapper.deleteDealLog(hrbCredit.getDealNo());
				tdDealLogMapper.InsertDealLog(log);
			}
		}

	}

	//额度占用（自动）
	@Override
	public void LimitOccupyInsert(Map<String, Object> map) {
		Map<String, Object> remap = new HashMap<>();
		String custno = (String) map.get("custNo");
		String productCode = (String) map.get("productCode");
		IfsOpicsCust ifsOpicsCust = ifsOpicsCustMapper.searchIfsOpicsCust(custno);
		if (ifsOpicsCust == null ) {
			JY.raiseRException("未找到" + custno + "授信主体");
		}

		TcProductCredit tcProductCredit = tcProductCreditMapper.selectCreditProductOne(map);
		if (tcProductCredit == null ) {
			JY.raiseRException("未找到" + productCode + "产品类型");
		}

		//组装占用明细表
		remap.put("custNo", ifsOpicsCust.getCno());
		remap.put("custName", ifsOpicsCust.getSn());
		remap.put("productCode", productCode);
		remap.put("productName", tcProductCredit.getProductName());
		remap.put("currency", map.get("currency"));
		remap.put("creTypeName", map.get("custType"));
		remap.put("amt", map.get("amt"));
		remap.put("isoccupy", "1");//占用标识
		remap.put("state", "2");
		remap.put("vdate", map.get("vdate"));
		remap.put("mdate", map.get("mdate"));
		remap.put("institution", map.get("institution"));
		remap.put("ioper", SlSessionHelper.getUserId());
		remap.put("inputTime", DateUtil.getCurrentTimeAsString());
		remap.put("modifyDate", DateUtil.getCurrentDateTimeAsString());
		// N:自动，Y：人工
		remap.put("offLine", "N");
		remap.put("weight", map.get("weight"));
		// 审批单号和交易单号一致，并且直接跳过审批
		remap.put("dealNo", map.get("serial_no"));
		remap.put("approveStatus", com.singlee.slbpm.dict.DictConstants.ApproveStatus.ApprovedPass);
		remap.put("OldNew", "1");
		HrbCreditLimitOccupy tecbs = LimitOccupyMapper.getCreditDealByDealNo((String)remap.get("dealNo"));
		if(null == tecbs) {
			// 插入本次交易信息到额度占用中
			LimitOccupyMapper.OccupyInsert(remap);
		}else {
			LimitOccupyMapper.deleteByPrimaryKey((String)remap.get("dealNo"));
			LimitOccupyMapper.OccupyInsert(remap);
		}
		// 完成总表和产品表的占用
		AmtChange(remap);
	}

	// 额度释放（自动）
	@Override
	public void LimitReleaseInsert(Map<String, Object> map) {
		//组装释放明细表
		Map<String, Object> remap = new HashMap<>();
		remap.put("custNo", map.get("custNo"));
		remap.put("custName", map.get("custName"));
		remap.put("productCode", map.get("productCode"));
		remap.put("productName", map.get("productName"));
		remap.put("currency", map.get("currency"));
		remap.put("creTypeName", map.get("creTypeName"));
		remap.put("amt", map.get("amt"));
		remap.put("isoccupy", "0");//释放标识
		remap.put("state", "2");
		remap.put("vdate", map.get("vdate"));
		remap.put("mdate", map.get("mdate"));
		remap.put("institution", map.get("institution"));
		remap.put("ioper", SlSessionHelper.getUserId());
		remap.put("inputTime", DateUtil.getCurrentTimeAsString());
		remap.put("modifyDate", DateUtil.getCurrentDateTimeAsString());
		// N:自动，Y：人工
		remap.put("offLine", "N");
		remap.put("weight", map.get("weight"));

		// 生产审批单号，并且直接跳过审批
		String trdtype = DictConstants.CREDIT_TYPE.USED;
		remap.put("dealNo", map.get("dealNo")+ "_R");
		remap.put("approveStatus", com.singlee.slbpm.dict.DictConstants.ApproveStatus.ApprovedPass);
		remap.put("OldNew", "1");
		HrbCreditLimitOccupy tecbs = LimitOccupyMapper.getCreditDealByDealNo((String)remap.get("dealNo"));
		if(null == tecbs) {
			// 插入本次交易信息到额度占用中
			LimitOccupyMapper.OccupyInsert(remap);
		}else {
			LimitOccupyMapper.deleteByPrimaryKey((String)remap.get("dealNo"));
			LimitOccupyMapper.OccupyInsert(remap);
		}

		// 完成总表和产品表的占用
		AmtChange(remap);

	}

	@Override
	public void LimitReleaseInsertByDealno(String dealno) {
		Map<String, Object> remp = new HashMap<>();
		remp.put("dealNo", dealno);
		// 根据交易号拿到这个占用的全部信息
		List<TdEdCustBatchStatus> list2 = tdedCustBatchStatusMapper.selectEdCustLogForDealNoWithList(remp);
		Map<String, Object> map = new HashMap<>();
		//没有查到占用信息的不做释放
		if (list2 != null && list2.size() > 0) {
			map.put("custName", list2.get(0).getCnName());
			map.put("custNo", list2.get(0).getEcifNo());
			map.put("productCode", list2.get(0).getPrdNo());
			map.put("productName", list2.get(0).getPrdName());
			map.put("currency", list2.get(0).getCurrency());
			map.put("amt", list2.get(0).getEdOpAmt());
			map.put("institution", "999999999");
			map.put("creTypeName", list2.get(0).getCreditId());
			map.put("weight", list2.get(0).getWeight());
			map.put("dealNo", dealno);
			LimitReleaseInsert(map);
		}
	}

	@Override
	public List<HrbCreditCustProd> OccupyAmt(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return null;
	}

	public BigDecimal ccyChange(BigDecimal amt, String vccy,String mccy) {

		/**
		 * vccy币种转CNY
		 */
		BigDecimal toCNYRate = new BigDecimal("1");
		//获取汇率
		if (!"CNY".equals(vccy)) {
			Map<String, String> mapRate = new HashMap<String, String>();
			mapRate.put("ccy", vccy);
			mapRate.put("br", "01");// 机构号，送01
			toCNYRate = acupServer.queryRate(mapRate);
			if (toCNYRate == null || "".equals(toCNYRate)) {
				JY.raise("货币" + vccy + "不存在汇率");
			}
		}
		//获取ccy  兑  CNY的乘除关系
		String toCNYRateType = acupServer.queryRateCCType("01", vccy, "M");
		if (!StringUtil.isEmptyString(toCNYRateType)) {
			if ("M".equals(toCNYRateType)) {
				amt = amt.multiply(toCNYRate);
			} else {
				amt = amt.divide(toCNYRate, 4, RoundingMode.HALF_UP);
			}
		} else {
			JY.raise("货币" + vccy + "不存在汇率计算方式");
		}

		/**
		 *CNY转mccy
		 */
		BigDecimal dayRate = new BigDecimal("1");
		//获取汇率
		if (!"CNY".equals(mccy)) {
			Map<String, String> mapRate = new HashMap<String, String>();
			mapRate.put("ccy", mccy);
			mapRate.put("br", "01");// 机构号，送01
			dayRate = acupServer.queryRate(mapRate);
			if (dayRate == null || "".equals(dayRate)) {
				JY.raise("货币" + mccy + "不存在汇率");
			}
		}
		//获取mccy  兑  CNY的乘除关系
		String rateCCType = acupServer.queryRateCCType("01", mccy, "M");
		if (!StringUtil.isEmptyString(rateCCType)) {
			if ("M".equals(rateCCType)) {
				amt = amt.divide(dayRate, 4, RoundingMode.HALF_UP);
			} else {
				amt = amt.multiply(dayRate);
			}
		} else {
			JY.raise("货币" + mccy + "不存在汇率计算方式");
		}
		return amt;
	}
}
