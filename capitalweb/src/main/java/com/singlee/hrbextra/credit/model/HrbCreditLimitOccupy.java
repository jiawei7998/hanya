package com.singlee.hrbextra.credit.model;


import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * 额度占用情况表
 *
 * @author SINGLEE
 */

@Table(name = "TD_CUST_CREDIT_DEAL")
public class HrbCreditLimitOccupy implements Serializable {


    private static final long serialVersionUID = 1L;
    @Id
    // 交易流水号
    private String dealNo;
    // 业务品种
    private String productCode;
    // 业务名称
    private String productName;
    // 交易对手编号
    private String custNo;
    // 交易对手名称
    private String custName;
    // 经营单位
    private String institution;
    // 占用日
    private String vdate;
    // 到期日
    private String mdate;
    // 金额
    private BigDecimal amt;
    // 录入理由
    private String reason;
    // 备注1
    private String remark1;
    // 备注2
    private String remark2;
    // 状态
    private int isoccupy;
    //是否为额度占用
    private int state;
    // 操作员
    private String ioper;
    // 是否是线下手工录入的交易
    private String offLine;
    // 审批状态
    private int approveStatus;
    // 修改日期
    private String modifyDate;
    // 上送日期
    private String inputDate;
    // 上送时间
    private String inputTime;
    //权重
    private String weight;
    //授信主体名称
    private String creditsubnm;
    //授信主体编号
    private String creditsubno;

    // 币种
    private String currency;
    //额度类型
    private String creTypeName;
    //老单号
    private String oldDealNo;


    //剩余占用额度
    private String balance;
    //已释放额度
    private String released;
    @Transient
    private String taskId;

    @Transient
    private String sponInstName;// 20180529新加
    @Transient
    private String userName;

    public String getDealNo() {
        return dealNo;
    }

    public void setDealNo(String dealNo) {
        this.dealNo = dealNo;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getCustNo() {
        return custNo;
    }

    public void setCustNo(String custNo) {
        this.custNo = custNo;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getInstitution() {
        return institution;
    }

    public void setInstitution(String institution) {
        this.institution = institution;
    }

    public String getVdate() {
        return vdate;
    }

    public void setVdate(String vdate) {
        this.vdate = vdate;
    }

    public String getMdate() {
        return mdate;
    }

    public void setMdate(String mdate) {
        this.mdate = mdate;
    }

    public BigDecimal getAmt() {
        return amt;
    }

    public void setAmt(BigDecimal amt) {
        this.amt = amt;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getRemark1() {
        return remark1;
    }

    public void setRemark1(String remark1) {
        this.remark1 = remark1;
    }

    public String getRemark2() {
        return remark2;
    }

    public void setRemark2(String remark2) {
        this.remark2 = remark2;
    }

    public int getIsoccupy() {
        return isoccupy;
    }

    public void setIsoccupy(int isoccupy) {
        this.isoccupy = isoccupy;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getIoper() {
        return ioper;
    }

    public void setIoper(String ioper) {
        this.ioper = ioper;
    }

    public String getOffLine() {
        return offLine;
    }

    public void setOffLine(String offLine) {
        this.offLine = offLine;
    }

    public int getApproveStatus() {
        return approveStatus;
    }

    public void setApproveStatus(int approveStatus) {
        this.approveStatus = approveStatus;
    }

    public String getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(String modifyDate) {
        this.modifyDate = modifyDate;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getInputTime() {
        return inputTime;
    }

    public void setInputTime(String inputTime) {
        this.inputTime = inputTime;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCreTypeName() {
        return creTypeName;
    }

    public void setCreTypeName(String creTypeName) {
        this.creTypeName = creTypeName;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getReleased() {
        return released;
    }

    public void setReleased(String released) {
        this.released = released;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getSponInstName() {
        return sponInstName;
    }

    public void setSponInstName(String sponInstName) {
        this.sponInstName = sponInstName;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public String getCreditsubnm() {
        return creditsubnm;
    }

    public void setCreditsubnm(String creditsubnm) {
        this.creditsubnm = creditsubnm;
    }

    public String getCreditsubno() {
        return creditsubno;
    }

    public void setCreditsubno(String creditsubno) {
        this.creditsubno = creditsubno;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getOldDealNo() {
        return oldDealNo;
    }

    public void setOldDealNo(String oldDealNo) {
        this.oldDealNo = oldDealNo;
    }

    @Override
    public String toString() {
        return "HrbCreditLimitOccupy [dealNo=" + dealNo + ", productCode=" + productCode + ", productName="
                + productName + ", custNo=" + custNo + ", custName=" + custName + ", institution=" + institution
                + ", vdate=" + vdate + ", mdate=" + mdate + ", amt=" + amt + ", reason=" + reason + ", remark1="
                + remark1 + ", remark2=" + remark2 + ", isoccupy=" + isoccupy + ", state=" + state + ", ioper="
                + ioper + ", offLine=" + offLine + ", approveStatus=" + approveStatus + ", modifyDate=" + modifyDate
                + ", inputDate=" + inputDate + ", inputTime=" + inputTime + ", weight=" + weight + ", creditsubnm="
                + creditsubnm + ", creditsubno=" + creditsubno + ", currency=" + currency + ", creTypeName="
                + creTypeName + ", balance=" + balance + ", released=" + released + ", taskId=" + taskId
                + ", sponInstName=" + sponInstName + "]";
    }


}
