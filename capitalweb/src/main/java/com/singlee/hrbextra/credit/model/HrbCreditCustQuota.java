package com.singlee.hrbextra.credit.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 客户总额度表
 * @author zk
 *
 */
@Entity
@Table(name="TD_ED_CUST_QUOTA")
public class HrbCreditCustQuota implements Serializable{
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT SEQ_ED_CUST_ID.NEXTVAL FROM DUAL")
    private String custCreditId;
    /**
     * opics客户号
     */
    private String ecifNum;
    /**
     * 客户名称
     */
    private String customer;
    /**
     * 额度品种
     */
    private String creditType;
    /**
     * 币种
     */
    private String currency;
    /**
     * 授信额度
     */
    private String loanAmt;
    /**
     * 可用金额
     */
    private String avlAmt;
    /**
     * 冻结额度
     */
    private String frozenAmt;
    /**
     * 起始日
     */
    private String dueDate;
    /**
     * 到期日
     */
    private String dueDateLast;
    /**
     * 操作员
     */
    private String operator;
    /**
     * 操作时间
     */
    private String opTime;
    
    public String getDueDate() {
        return dueDate;
    }
    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }
    public String getDueDateLast() {
        return dueDateLast;
    }
    public void setDueDateLast(String dueDateLast) {
        this.dueDateLast = dueDateLast;
    }
    public String getCustCreditId() {
        return custCreditId;
    }
    public void setCustCreditId(String custCreditId) {
        this.custCreditId = custCreditId;
    }
    public String getEcifNum() {
        return ecifNum;
    }
    public void setEcifNum(String ecifNum) {
        this.ecifNum = ecifNum;
    }
    public String getCustomer() {
        return customer;
    }
    public void setCustomer(String customer) {
        this.customer = customer;
    }
    public String getCreditType() {
        return creditType;
    }
    public void setCreditType(String creditType) {
        this.creditType = creditType;
    }
    public String getCurrency() {
        return currency;
    }
    public void setCurrency(String currency) {
        this.currency = currency;
    }
    public String getLoanAmt() {
        return loanAmt;
    }
    public void setLoanAmt(String loanAmt) {
        this.loanAmt = loanAmt;
    }
    public String getAvlAmt() {
        return avlAmt;
    }
    public void setAvlAmt(String avlAmt) {
        this.avlAmt = avlAmt;
    }
    public String getFrozenAmt() {
        return frozenAmt;
    }
    public void setFrozenAmt(String frozenAmt) {
        this.frozenAmt = frozenAmt;
    }
    public String getOperator() {
        return operator;
    }
    public void setOperator(String operator) {
        this.operator = operator;
    }
    public String getOpTime() {
        return opTime;
    }
    public void setOpTime(String opTime) {
        this.opTime = opTime;
    }
    @Override
    public String toString() {
        return "HrbCreditCustQuota [custCreditId=" + custCreditId + ", ecifNum=" + ecifNum + ", customer=" + customer
                + ", creditType=" + creditType + ", currency=" + currency + ", loanAmt=" + loanAmt + ", avlAmt="
                + avlAmt + ", frozenAmt=" + frozenAmt + ", dueDate=" + dueDate + ", dueDateLast=" + dueDateLast
                + ", operator=" + operator + ", opTime=" + opTime + "]";
    }
    
}
