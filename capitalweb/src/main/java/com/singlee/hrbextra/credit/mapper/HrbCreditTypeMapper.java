package com.singlee.hrbextra.credit.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.hrbextra.credit.model.HrbCreditType;

import tk.mybatis.mapper.common.Mapper;

public interface HrbCreditTypeMapper  extends Mapper<HrbCreditType> {
    
    Page<HrbCreditType> queryCrediyTypePage(Map<String, Object> map, RowBounds rowBounds);
    
    List<HrbCreditType> queryAllCrediyType(Map<String, Object> map);
    
    String countCreditType(Map<String, Object> map);
}
