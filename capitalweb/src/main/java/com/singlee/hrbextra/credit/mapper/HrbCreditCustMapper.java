package com.singlee.hrbextra.credit.mapper;

import com.github.pagehelper.Page;
import com.singlee.hrbextra.credit.model.HrbCreditCustQuota;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

public interface HrbCreditCustMapper extends Mapper<HrbCreditCustQuota> {
    Page<HrbCreditCustQuota> getHrbCreditQueryPage(Map<String, Object> map, RowBounds rowBounds);
    
    String countCustCreditById(Map<String, Object> map);

    /**
     * 加锁FOR UPDATE
     * @param map
     * @return
     */
    HrbCreditCustQuota getHrbCreditCustQuota(Map<String, Object> map);

    /**
     * 单条查询不加锁
     * @param map
     * @return
     */
    HrbCreditCustQuota selectHrbCreditCustQuota(Map<String, Object> map);

    List<HrbCreditCustQuota> getHrbCreditQueryList(Map<String, Object> map);
}
