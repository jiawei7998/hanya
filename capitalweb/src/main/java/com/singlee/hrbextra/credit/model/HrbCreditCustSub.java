package com.singlee.hrbextra.credit.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 授信主体信息查询
 * @author zk
 *
 */

public class HrbCreditCustSub implements Serializable{
    
    private static final long serialVersionUID = 1L;
    private String prodCreditId;
    
    /**
     * 客户额度唯一 编号
     */
    private String custCreditId;
    /**
     * opics客户号
     */
    private String custNum;
    /**
     * 产品类型
     */
    private String prodType;
    
    /**
     * 产品名称
     */
    private String prodName;
    /**
     * 授信额度
     */
    private String proLoanAmt;
    /**
     * 可用金额
     */
    private String proAvlAmt;
    /**
     * 已用额度
     */
    private String proFrozenAmt;
    /**
     * 操作员
     */
    private String operator;
    /**
     * 操作时间
     */
    private String opTime;
    
    /**
     * 授信主体授信总额
     */
    private String loanAmt;
    /**
     * 授信主体授信剩余额度
     */
    private String avlAmt;

    /**
     * 债券id
      */
    private String bondCode;
   
    /**
     * 授信主体名称
     */
    private String creditsubnm;
    public String getProdCreditId() {
        return prodCreditId;
    }
    public void setProdCreditId(String prodCreditId) {
        this.prodCreditId = prodCreditId;
    }
    public String getCustCreditId() {
        return custCreditId;
    }
    public void setCustCreditId(String custCreditId) {
        this.custCreditId = custCreditId;
    }
    public String getCustNum() {
        return custNum;
    }
    public void setCustNum(String custNum) {
        this.custNum = custNum;
    }
    public String getProdType() {
        return prodType;
    }
    public void setProdType(String prodType) {
        this.prodType = prodType;
    }
    public String getProdName() {
        return prodName;
    }
    public void setProdName(String prodName) {
        this.prodName = prodName;
    }
    public String getProLoanAmt() {
        return proLoanAmt;
    }
    public void setProLoanAmt(String proLoanAmt) {
        this.proLoanAmt = proLoanAmt;
    }
    public String getProAvlAmt() {
        return proAvlAmt;
    }
    public void setProAvlAmt(String proAvlAmt) {
        this.proAvlAmt = proAvlAmt;
    }
    public String getProFrozenAmt() {
        return proFrozenAmt;
    }
    public void setProFrozenAmt(String proFrozenAmt) {
        this.proFrozenAmt = proFrozenAmt;
    }
    public String getOperator() {
        return operator;
    }
    public void setOperator(String operator) {
        this.operator = operator;
    }
    public String getOpTime() {
        return opTime;
    }
    public void setOpTime(String opTime) {
        this.opTime = opTime;
    }
    public String getLoanAmt() {
        return loanAmt;
    }
    public void setLoanAmt(String loanAmt) {
        this.loanAmt = loanAmt;
    }
    public String getAvlAmt() {
        return avlAmt;
    }
    public void setAvlAmt(String avlAmt) {
        this.avlAmt = avlAmt;
    }
    public String getCreditsubnm() {
        return creditsubnm;
    }
    public void setCreditsubnm(String creditsubnm) {
        this.creditsubnm = creditsubnm;
    }

    public String getBondCode() {
        return bondCode;
    }

    public void setBondCode(String bondCode) {
        this.bondCode = bondCode;
    }

    @Override
    public String toString() {
        return "HrbCreditCustSub [prodCreditId=" + prodCreditId + ", custCreditId=" + custCreditId + ", custNum="
                + custNum + ", prodType=" + prodType + ", prodName=" + prodName + ", proLoanAmt=" + proLoanAmt
                + ", proAvlAmt=" + proAvlAmt + ", proFrozenAmt=" + proFrozenAmt + ", operator=" + operator + ", opTime="
                + opTime + ", loanAmt=" + loanAmt + ", avlAmt=" + avlAmt + ", creditsubnm=" + creditsubnm + "]";
    }
   
    
}
