package com.singlee.hrbextra.credit.controller;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.hrbextra.credit.model.HrbCreditType;
import com.singlee.hrbextra.credit.service.HrbCreditTypeService;

@Controller
@RequestMapping("/HrbCreditTypeController")
public class HrbCreditTypeController  extends CommonController  {

    @Autowired
    HrbCreditTypeService hrbCreditTypeService;
    
    @ResponseBody
    @RequestMapping("/queryCrediyTypePage")
    public RetMsg<PageInfo<HrbCreditType>> queryCrediyTypePage(@RequestBody Map<String, Object> map){
        Page<HrbCreditType> queryCrediyTypePage = hrbCreditTypeService.queryCrediyTypePage(map);
        return RetMsgHelper.ok(queryCrediyTypePage);
    }
    
    @ResponseBody
    @RequestMapping("/queryAllCrediyType")
    public List<HrbCreditType> queryAllCrediyType(@RequestBody Map<String, Object> map){
        List<HrbCreditType> CrediyTypeList = hrbCreditTypeService.queryAllCrediyType(map);
        return CrediyTypeList;
    }
    
    @ResponseBody
    @RequestMapping("/insertCrediyType")
    public RetMsg<Serializable> insertCrediyType(@RequestBody HrbCreditType type){
        String result = hrbCreditTypeService.insertCrediyType(type);
        if(result != null && "error".equals(result)) {
            return RetMsgHelper.simple("额度代码已存在");
        }
        return RetMsgHelper.ok();
    }
    
    @ResponseBody
    @RequestMapping("/updateCrediyType")
    public RetMsg<Serializable> updateCrediyType(@RequestBody HrbCreditType type){
        hrbCreditTypeService.updateCrediyType(type);
        return RetMsgHelper.ok();
    }
    
    @ResponseBody
    @RequestMapping("/deleteCrediyType")
    public RetMsg<Serializable> deleteCrediyType(@RequestBody HrbCreditType type){
        hrbCreditTypeService.deleteCrediyType(type);
        return RetMsgHelper.ok();
    }
    
}
