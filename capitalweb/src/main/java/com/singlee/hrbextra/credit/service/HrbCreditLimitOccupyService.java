package com.singlee.hrbextra.credit.service;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.hrbextra.credit.model.HrbCreditCustProd;
import com.singlee.hrbextra.credit.model.HrbCreditLimitOccupy;



public interface HrbCreditLimitOccupyService {

	void addHrbCreditLimitOccupy(Map<String,Object> map);

	void deleteCreditDeal(Map<String, Object> map);

	void updateCreditDeal(Map<String, Object> map);

	void statusChange(Map<String,Object> approveMap);

	HrbCreditLimitOccupy getCreditDealByDealNo(String serial_no);

	Page<HrbCreditLimitOccupy> getCreditDealAll(Map<String, Object> map);
	
	Page<HrbCreditLimitOccupy> queryCreditReleaseDeal(Map<String, Object> map,RowBounds rowBounds);

	HrbCreditLimitOccupy queryCreditReleaseDealByDealno(Map<String, Object> map);

	Page<HrbCreditLimitOccupy> getCreditDealListFinish(Map<String, Object> map);

	Page<HrbCreditLimitOccupy> getCreditDealListApprove(Map<String, Object> map);

	Page<HrbCreditLimitOccupy> getCreditDealListMine(Map<String, Object> map);
	
	/**
	 * 获取手手工占用详情，金额为 金额*权重/100
	 */
	HrbCreditLimitOccupy getCreditDealByDealNoAddWeight(String dealNo);

	void AmtChange(Map<String,Object> map);
	
	//自动额度占用
	 void  LimitOccupyInsert(Map<String,Object> map);
	
	//自动额度释放
	void  LimitReleaseInsert(Map<String,Object> map);
	
	//根据单号自动释放
	void  LimitReleaseInsertByDealno(String dealno);
	
     List<HrbCreditCustProd> OccupyAmt(Map<String,Object> map);
}
