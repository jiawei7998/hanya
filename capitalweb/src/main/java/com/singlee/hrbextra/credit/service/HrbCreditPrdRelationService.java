package com.singlee.hrbextra.credit.service;

import com.github.pagehelper.Page;
import com.singlee.hrbextra.credit.model.HrbCreditCustProd;

import java.util.List;
import java.util.Map;

public interface HrbCreditPrdRelationService {

    Page<HrbCreditCustProd> getHrbCreditQueryPage(Map<String, Object> map);
    
    String saveInfo(HrbCreditCustProd hrbCreditCustProd);
    
    void deleteInfo(HrbCreditCustProd hrbCreditCustProd);
    
    String countProdById(Map<String, Object> map);

    List<HrbCreditCustProd> getHrbCreditQueryList(Map<String, Object> map);
}
