package com.singlee.hrbextra.credit.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.hrbextra.credit.mapper.HrbCreditPrdRelationMapper;
import com.singlee.hrbextra.credit.model.HrbCreditCustProd;
import com.singlee.hrbextra.credit.service.HrbCreditPrdRelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class HrbCreditPrdRelationServiceImpl implements HrbCreditPrdRelationService {
    @Autowired
    HrbCreditPrdRelationMapper hrbCreditPrdRelationMapper;
    
    @Override
    public Page<HrbCreditCustProd> getHrbCreditQueryPage(Map<String, Object> map) {
        return hrbCreditPrdRelationMapper.getHrbCreditQueryPage(map, ParameterUtil.getRowBounds(map));
    }
    
    @Override
    public String saveInfo(HrbCreditCustProd hrbCreditCustProd) {
        
        hrbCreditCustProd.setOperator(SlSessionHelper.getUserId()); 
        hrbCreditCustProd.setOpTime(DateUtil.getCurrentDateAsString());
        
      //通过ProdCreditId是否有值判断是insert还是update
        String result = "";
        deleteCreditCustProd(hrbCreditCustProd);
        result = insertCreditCustProd(hrbCreditCustProd);


        
        return result;
    }

    private void deleteCreditCustProd(HrbCreditCustProd hrbCreditCustProd) {
        hrbCreditPrdRelationMapper.deleteCreditCustProd(hrbCreditCustProd);
    }


    private String updateCreditCustProd(HrbCreditCustProd hrbCreditCustProd) {
        hrbCreditPrdRelationMapper.updateByPrimaryKeySelective(hrbCreditCustProd);
        return "ok";
    }

    private String insertCreditCustProd(HrbCreditCustProd hrbCreditCustProd) {
        Map<String,Object> map = new HashMap<String,Object>();
        map.put("custCreditId", hrbCreditCustProd.getCustCreditId());
        map.put("custNum", hrbCreditCustProd.getCustNum());
        map.put("prodType", hrbCreditCustProd.getProdType());
        String count = countProdById(map);
        if(count != null && Integer.parseInt(count) > 0) {
            return "error";
        }
        hrbCreditPrdRelationMapper.insert(hrbCreditCustProd);
        return "ok";
    }

    @Override
    public void deleteInfo(HrbCreditCustProd hrbCreditCustProd) {
        hrbCreditPrdRelationMapper.delete(hrbCreditCustProd);
    }
    
    @Override
    public String countProdById(Map<String, Object> map) {
        return hrbCreditPrdRelationMapper.countProdById(map);
    }

    @Override
    public List<HrbCreditCustProd> getHrbCreditQueryList(Map<String, Object> map) {
        return hrbCreditPrdRelationMapper.getHrbCreditQueryList(map);
    }
}
