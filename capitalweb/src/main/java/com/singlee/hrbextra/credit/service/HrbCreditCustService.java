package com.singlee.hrbextra.credit.service;

import com.github.pagehelper.Page;
import com.singlee.hrbextra.credit.model.HrbCreditCustQuota;

import java.util.Map;

public interface HrbCreditCustService {

    Page<HrbCreditCustQuota> getHrbCreditQueryPage(Map<String, Object> map);
    
    String insertOrUpdateCreditCust(HrbCreditCustQuota creditCust);
    
    void deleteCreditCust(HrbCreditCustQuota creditCust);
    
    String countCustCreditById(Map<String,Object> map);

    Map<String, Object> getReportExcel(Map<String, Object> paramer) ;
}
