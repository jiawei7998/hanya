package com.singlee.hrbextra.wind.mapper;

import com.github.pagehelper.Page;
import com.singlee.hrbextra.wind.model.CmFincome;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

public interface CmFincomeMapper extends Mapper<CmFincome> {
    Page<CmFincome> getCmFincomePage(Map<String, Object> map, RowBounds rowBounds);

    List<CmFincome> getCmFincomeList(Map<String, Object> map);

    void deleCmFincome(CmFincome cmFincome);

}