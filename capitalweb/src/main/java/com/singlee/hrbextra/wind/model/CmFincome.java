package com.singlee.hrbextra.wind.model;

import javax.persistence.Column;
import javax.persistence.Table;
import java.math.BigDecimal;

/**
    * 货币基金万分收益
    */
@Table(name = "CM_FINCOME")
public class CmFincome {
    /**
     * 对象 ID
     */
    @Column(name = "OBJECT_ID")
    private String OBJECT_ID;

    /**
     * Wind 代码
     */
    @Column(name = "S_INFO_WINDCODE")
    private String S_INFO_WINDCODE;

    /**
     * 起始日期
     */
    @Column(name = "F_INFO_BGNDATE")
    private String F_INFO_BGNDATE;

    /**
     * 截止日期
     */
    @Column(name = "F_INFO_ENDDATE")
    private String F_INFO_ENDDATE;

    /**
     * 每万份 基金单 位收益
     */
    @Column(name = "F_INFO_UNITYIELD")
    private BigDecimal F_INFO_UNITYIELD;

    /**
     * 最近七 日收益 所算的年资产收益率
     */
    @Column(name = "F_INFO_YEARLYROE")
    private BigDecimal F_INFO_YEARLYROE;

    /**
     * 公告日期
     */
    @Column(name = "ANN_DATE")
    private String ANN_DATE;

    @Column(name = "OPDATE")
    private String OPDATE;

    @Column(name = "OPMODE")
    private String OPMODE;

    @Column(name = "MOPDATE")
    private String MOPDATE;
    /**
     * 操作时间
     */
    private String updateTime;

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 获取对象 ID
     *
     * @return OBJECT_ID - 对象 ID
     */
    public String getOBJECT_ID() {
        return OBJECT_ID;
    }

    /**
     * 设置对象 ID
     *
     * @param OBJECT_ID 对象 ID
     */
    public void setOBJECT_ID(String OBJECT_ID) {
        this.OBJECT_ID = OBJECT_ID;
    }

    /**
     * 获取Wind 代码
     *
     * @return S_INFO_WINDCODE - Wind 代码
     */
    public String getS_INFO_WINDCODE() {
        return S_INFO_WINDCODE;
    }

    /**
     * 设置Wind 代码
     *
     * @param S_INFO_WINDCODE Wind 代码
     */
    public void setS_INFO_WINDCODE(String S_INFO_WINDCODE) {
        this.S_INFO_WINDCODE = S_INFO_WINDCODE;
    }

    /**
     * 获取起始日期
     *
     * @return F_INFO_BGNDATE - 起始日期
     */
    public String getF_INFO_BGNDATE() {
        return F_INFO_BGNDATE;
    }

    /**
     * 设置起始日期
     *
     * @param F_INFO_BGNDATE 起始日期
     */
    public void setF_INFO_BGNDATE(String F_INFO_BGNDATE) {
        this.F_INFO_BGNDATE = F_INFO_BGNDATE;
    }

    /**
     * 获取截止日期
     *
     * @return F_INFO_ENDDATE - 截止日期
     */
    public String getF_INFO_ENDDATE() {
        return F_INFO_ENDDATE;
    }

    /**
     * 设置截止日期
     *
     * @param F_INFO_ENDDATE 截止日期
     */
    public void setF_INFO_ENDDATE(String F_INFO_ENDDATE) {
        this.F_INFO_ENDDATE = F_INFO_ENDDATE;
    }

    /**
     * 获取每万份 基金单 位收益
     *
     * @return F_INFO_UNITYIELD - 每万份 基金单 位收益
     */
    public BigDecimal getF_INFO_UNITYIELD() {
        return F_INFO_UNITYIELD;
    }

    /**
     * 设置每万份 基金单 位收益
     *
     * @param F_INFO_UNITYIELD 每万份 基金单 位收益
     */
    public void setF_INFO_UNITYIELD(BigDecimal F_INFO_UNITYIELD) {
        this.F_INFO_UNITYIELD = F_INFO_UNITYIELD;
    }

    /**
     * 获取最近七 日收益 所算的年资产收益率
     *
     * @return F_INFO_YEARLYROE - 最近七 日收益 所算的年资产收益率
     */
    public BigDecimal getF_INFO_YEARLYROE() {
        return F_INFO_YEARLYROE;
    }

    /**
     * 设置最近七 日收益 所算的年资产收益率
     *
     * @param F_INFO_YEARLYROE 最近七 日收益 所算的年资产收益率
     */
    public void setF_INFO_YEARLYROE(BigDecimal F_INFO_YEARLYROE) {
        this.F_INFO_YEARLYROE = F_INFO_YEARLYROE;
    }

    /**
     * 获取公告日期
     *
     * @return ANN_DATE - 公告日期
     */
    public String getANN_DATE() {
        return ANN_DATE;
    }

    /**
     * 设置公告日期
     *
     * @param ANN_DATE 公告日期
     */
    public void setANN_DATE(String ANN_DATE) {
        this.ANN_DATE = ANN_DATE;
    }

    /**
     * @return OPDATE
     */
    public String getOPDATE() {
        return OPDATE;
    }

    /**
     * @param OPDATE
     */
    public void setOPDATE(String OPDATE) {
        this.OPDATE = OPDATE;
    }

    /**
     * @return OPMODE
     */
    public String getOPMODE() {
        return OPMODE;
    }

    /**
     * @param OPMODE
     */
    public void setOPMODE(String OPMODE) {
        this.OPMODE = OPMODE;
    }

    /**
     * @return MOPDATE
     */
    public String getMOPDATE() {
        return MOPDATE;
    }

    /**
     * @param MOPDATE
     */
    public void setMOPDATE(String MOPDATE) {
        this.MOPDATE = MOPDATE;
    }
}