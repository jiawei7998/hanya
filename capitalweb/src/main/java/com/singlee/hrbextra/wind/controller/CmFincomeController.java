package com.singlee.hrbextra.wind.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.hrbextra.wind.model.CmFincome;
import com.singlee.hrbextra.wind.service.CmFincomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller
@RequestMapping("/CmFincomeController")
public class CmFincomeController extends CommonController {
    @Autowired
    CmFincomeService cmFincomeService;

    @ResponseBody
    @RequestMapping(value = "/getCmFincomePage")
    public RetMsg<PageInfo<CmFincome>> getCmFincomePage(@RequestBody Map<String, Object> params){
        Page<CmFincome> cmFincomepage = cmFincomeService.getCmFincomePage(params);
        return RetMsgHelper.ok(cmFincomepage);
    }
}
