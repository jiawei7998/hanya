package com.singlee.hrbextra.wind;

import com.singlee.capital.common.spring.mybatis.BatchDao;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.credit.util.ExcelUtil;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.financial.marketdata.intfc.MarketDataTransServer;
import com.singlee.hrbextra.wind.Util.WindUtils;
import com.singlee.ifs.mapper.IfsOpicsBondMapper;
import com.singlee.ifs.mapper.IfsWdOpicsSecmapMapper;
import com.singlee.ifs.mapper.IfsWindBondMapper;
import com.singlee.ifs.mapper.IfsWindSecLevelMapper;
import com.singlee.ifs.model.IfsOpicsBond;
import com.singlee.ifs.model.IfsWdOpicsSecmap;
import com.singlee.ifs.model.IfsWindBondBean;
import com.singlee.ifs.model.IfsWindSecLevelBean;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 万得债券信息数据
 *
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class WindBondService {

	@Autowired
	BatchDao batchDao;
	@Autowired
	MarketDataTransServer marketDataTransServer;
	@Autowired
	IfsWindBondMapper ifsWindBondMapper;
	@Autowired
	IfsOpicsBondMapper ifsOpicsBondMapper;
	@Autowired
	IfsWdOpicsSecmapMapper wdOpicsSecmapMapper;
	@Autowired
	IfsWindSecLevelMapper ifsWindSecLevelMapper;


	private static Logger logManager = LoggerFactory.getLogger(WindBondService.class);

	public Boolean run() throws Exception {



		logManager.info("==================在本地创建文件夹==============================");

		Date spotDate = new Date();
		String postDate = new SimpleDateFormat("yyyyMMdd HH").format(spotDate);
		//本地文件文件保存目录
		String filePath = SystemProperties.marketdataTargetpath;
		//远程下载文件名
		String fileName = "";
		if ("08".equals(postDate.substring(9))||"09".equals(postDate.substring(9))) {
			fileName = SystemProperties.marketdataLocalfilenameSecm + "0830" ;
		} else if ("10".equals(postDate.substring(9))||"11".equals(postDate.substring(9))) {
			fileName = SystemProperties.marketdataLocalfilenameSecm + "1030";
		} else if ("14".equals(postDate.substring(9))||"15".equals(postDate.substring(9))) {
			fileName = SystemProperties.marketdataLocalfilenameSecm + "1430";

		}
		Map<String ,Object> map=new HashMap<>();
		//远程文件名(不含文件后缀名)
		map.put("fileName",fileName);
		map.put("postDate",postDate);
		// 下载文件 （远程下载目录、远程文件名、本地保存目录、本地保存文件名）
		boolean flag=WindUtils.sftp(map);

		logManager.info("=================="+flag+"==============================");



		if(flag) {

			logManager.info("==================开始执行 读取Excel==============================");
			// 组装文件名
			// 读取文件
			String fullName= (String) map.get("fullName");
			//本地文件绝对路径
			String excel_location = filePath + postDate.substring(0, 8) + "/" + fullName;
			logManager.debug("开始读取文件：" + excel_location);
			File file = new File(excel_location);
			if (!file.exists()) {
				logManager.info("========" + excel_location + ":文件不存在=========");
				return true;
			}
			// 解析结果集
			List<IfsWindBondBean> wdbs = new ArrayList<IfsWindBondBean>();
			InputStream is = new FileInputStream(file);
			ExcelUtil excelUtil = new ExcelUtil(is);
			Sheet sheet = excelUtil.getSheetAt(0);
			int rowNum = sheet.getLastRowNum();
			// 开始读取数据
			for (int i = 1; i <= rowNum; i++) {
				IfsWindBondBean wdb = new IfsWindBondBean();
				Row row = sheet.getRow(i);
				wdb = readExcel(row, wdb, spotDate);
				wdbs.add(wdb);
			}
			logManager.info("==================执行完成 读取Excel==============================");

			logManager.info("==================开始执行 批量处理前置==============================");
			try {
				List<IfsWindBondBean> Ilist=new ArrayList<>();

				List<IfsWindBondBean> Ulist=new ArrayList<>();

				for (IfsWindBondBean wdb : wdbs) {
					IfsWindBondBean record = new IfsWindBondBean();
					record.setBondCode(wdb.getBondCode());
					record.setIssuerCode(wdb.getIssuerCode());
					logManager.info("=====债券ID:" + wdb.getBondCode() + ",债券发行人ID:" + wdb.getIssuerCode() + "=====");
					IfsWindBondBean selectOne = ifsWindBondMapper.selectOne(record);//债券代码和债券发行人
					if (selectOne == null) {
						ifsWindBondMapper.insertSelective(wdb);
					} else {
						Example example = new Example(IfsWindBondBean.class);
						example.createCriteria().andEqualTo("bondCode", wdb.getBondCode()).andEqualTo("issuerCode", wdb.getIssuerCode());
						ifsWindBondMapper.updateByExampleSelective(wdb, example);
					}
				}

			} catch (Exception e) {
				logManager.error("债券信息数据批量插入前置异常!", e);
			}
			logManager.info("==================执行完成 批量处理前置==============================");

//			logManager.info("==================开始执行 批量处理OPICS==============================");
//			try {
//				//wind映射OPICS 类型
				List<IfsWdOpicsSecmap> wosmlist = wdOpicsSecmapMapper.queryTypeList();
//
//				//wind映射OPICS 发行人
//				List<IfsWdOpicsSecmap> custlist = wdOpicsSecmapMapper.queryCustList();
//				for (IfsWdOpicsSecmap ifsWdOpicsSecmap : custlist) {
//
//					IfsWindBondBean rw = new IfsWindBondBean();
//					rw.setIssuer(ifsWdOpicsSecmap.getWdid().trim());
//					List<IfsWindBondBean> selectList = ifsWindBondMapper.selectByExample(rw);
//					for (IfsWindBondBean ifsWindBondBean : selectList) {
//						IfsWindSecLevelBean wb = ifsWindSecLevelMapper.selectByPrimaryKey(ifsWindBondBean.getBondCode());
//
//						IfsOpicsBond iob = generateIfsOpicsBond(ifsWindBondBean, wosmlist, wb);
//						iob.setIssuer(ifsWdOpicsSecmap.getOpicsid().trim());// 发行机构
//						iob.setIssuername(ifsWdOpicsSecmap.getOpicsname().trim());// 机构名称
//
//						IfsOpicsBond iobselect = ifsOpicsBondMapper.searchByIdAndAcct(iob.getBndcd(), iob.getIssuer());
//						if (iobselect == null) {
//							ifsOpicsBondMapper.insert(iob);
//						} else {
//							ifsOpicsBondMapper.updateByIdAndAcct(iob);
//						}
//					}
//				}
//
//
//			} catch (Exception e) {
//				logManager.error("债券信息数据批量插入OPICS异常!", e);
//			}
//			logManager.info("==================执行完成 批量处理OPICS==============================");

		return true;
		}
		else {
			logManager.info("==================文件下载失败，请重试==============================");

			return false;
		}
	}

	/**
	 * 解析表格
	 * 
	 * @param row
	 * @param wdb
	 * @return
	 */
	public static IfsWindBondBean readExcel(Row row, IfsWindBondBean wdb, Date spotDate) {
		wdb.setBondShortName(WindUtils.getString(row, 0));
		wdb.setBondCode(WindUtils.getString(row, 1));
		wdb.setBondCodeOld(WindUtils.getString(row, 2));
		wdb.setMarket(WindUtils.getString(row, 3));
		wdb.setIssueDate(WindUtils.getString(row, 4));
		wdb.setInterestTypeCode(WindUtils.getString(row, 5));
		wdb.setInterestType(WindUtils.getString(row, 6));
		wdb.setBasisType(WindUtils.getString(row, 7));
		wdb.setFreq(WindUtils.NBigDecimal(WindUtils.getString(row, 8)));
		wdb.setRate(WindUtils.NBigDecimal(WindUtils.getString(row, 9)));
		wdb.setVdate(WindUtils.getString(row, 10));
		wdb.setMdate(WindUtils.getString(row, 11));
		wdb.setTerm(WindUtils.NBigDecimal(WindUtils.getString(row, 12)));
		wdb.setExpiryDate(WindUtils.getString(row, 13));
		wdb.setSpread(WindUtils.NBigDecimal(WindUtils.getString(row, 14)));
		wdb.setIssuePrice(WindUtils.NBigDecimal(WindUtils.getString(row, 15)));
		wdb.setIssuerrating(WindUtils.getString(row, 16));
		wdb.setBondLongName(WindUtils.getString(row, 17));
		wdb.setVarietyCode(WindUtils.getString(row, 18));
		wdb.setVariety(WindUtils.getString(row, 19));
		wdb.setCirculationCode(WindUtils.getString(row, 20));
		wdb.setCirculation(WindUtils.getString(row, 21));
		wdb.setCustodianCode(WindUtils.getString(row, 22));
		wdb.setCustodian(WindUtils.getString(row, 23));
		wdb.setBasisCode(WindUtils.getString(row, 24));
		wdb.setBasis(WindUtils.getString(row, 25));
		wdb.setIssuerCode(WindUtils.getString(row, 26));
		wdb.setIssuer(WindUtils.getString(row, 27));
		wdb.setIssuerShort(WindUtils.getString(row, 28));
		wdb.setCountryCode(WindUtils.getString(row, 29));
		wdb.setCcy(WindUtils.getString(row, 30));
		wdb.setCircuplace(WindUtils.getString(row, 31));
		wdb.setRightType(WindUtils.getString(row, 32));
		wdb.setIndustry(WindUtils.getString(row, 33));
		wdb.setNewQty(WindUtils.NBigDecimal(WindUtils.getString(row, 34)));
		wdb.setMaincorpid(WindUtils.getString(row, 35));
		wdb.setRateType(WindUtils.getString(row, 36));
		wdb.setSpotDate(spotDate);
		return wdb;
	}

//	/**
//	 * 格式化数据
//	 *
//	 * @param row
//	 * @return
//	 */
//	public static String WindUtils.getString(Row row, int i) {
//		if (i == 8 || i == 9 || i == 12 || i == 14 || i == 15 || i == 34) {
//			if ("".equals(row.getCell(i).WindUtils.getStringCellValue()) || null == row.getCell(i).WindUtils.getStringCellValue()) {
//				return "0";
//			} else {
//				return row.getCell(i).WindUtils.getStringCellValue().trim();
//			}
//		} else {
//			if ("".equals(row.getCell(i).WindUtils.getStringCellValue()) || null == row.getCell(i).WindUtils.getStringCellValue()) {
//				return "";
//			} else {
//				return row.getCell(i).WindUtils.getStringCellValue().trim();
//			}
//		}
//	}

	/**
	 * 
	 * @param time
	 * @param num
	 * @return
	 */
	public static Date monthAddNum(Date time, Integer num) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(time);
		calendar.add(Calendar.MONTH, num);
		Date newTime = calendar.getTime();
		return newTime;
	}
	
	/**
	 * 获取首次付息日
	 * @param iob
	 *
	 */
	public static void GenerateDatil(IfsOpicsBond iob, IfsWindBondBean ifsWindBondBean, List<IfsWdOpicsSecmap> wosmlist,IfsWindSecLevelBean wb) {
		Date qxdate = ifsWindBondBean.getVdate() == null ? null : DateUtil.parse(ifsWindBondBean.getVdate(), "yyyyMMdd"); // 万得起息日
		Date fxdate = ifsWindBondBean.getIssueDate() == null ? null : DateUtil.parse(ifsWindBondBean.getIssueDate(), "yyyyMMdd"); // 万得发行日
		if (fxdate == null || qxdate == null) {
			iob.setIssudt(DateUtil.format(qxdate));// 正常起息日
		} else {// 续发 起息日小于发行日 将起息日改为发行日
			iob.setIssudt(qxdate.compareTo(fxdate) == -1 ? DateUtil.format(fxdate) : DateUtil.format(qxdate));
		}
		
		if (ifsWindBondBean.getMdate() != null) {
			String dqString = ifsWindBondBean.getMdate();
			iob.setMatdt(dqString.substring(0, 4) + "-" + dqString.substring(4, 6) + "-" + dqString.substring(6, 8));// 到期日
		}
		if (ifsWindBondBean.getIssueDate() != null) {// 发行日期
			String dqString = ifsWindBondBean.getIssueDate();
			iob.setPubdt(dqString.substring(0, 4) + "-" + dqString.substring(4, 6) + "-" + dqString.substring(6, 8));// 到期日
		}
		
		String freq = ifsWindBondBean.getFreq().toString();// 付息周期
		if ("1".equals(freq)) {
			iob.setIntpaycircle("M");
		} else if ("3".equals(freq)) {
			iob.setIntpaycircle("Q");
		} else if ("6".equals(freq)) {
			iob.setIntpaycircle("S");
		} else if ("12".equals(freq) || "24".equals(freq)) {
			iob.setIntpaycircle("A");
		} else {
			iob.setIntpaycircle(null);
		}

		if(iob.getIssudt() != null && iob.getMatdt() !=null && freq!=null && !"0".equals(freq)) {
			// 计算首次付息日
			String vString = iob.getIssudt().replace("-", "");
			String mString = iob.getMatdt().replace("-", "");
			Date vdated = DateUtil.parse(iob.getIssudt());
			Date mdated = DateUtil.parse(iob.getMatdt());
			int num = Integer.parseInt(freq);
			if (!vString.substring(4).equals(mString.substring(4))) {
				Boolean flag = true;
				Date first = mdated;
				do {
					first = monthAddNum(first, -num);
					flag = first.compareTo(vdated) == -1 ? false : true;
				} while (flag);
				iob.setParagraphfirstplan(DateUtil.format(monthAddNum(first, num)));
			} else {
				iob.setParagraphfirstplan(DateUtil.format(monthAddNum(vdated, num)));
			}
		}
		
		// 转化证券分类
		for (IfsWdOpicsSecmap ifsWdOpicsSecmap : wosmlist) {
			if (ifsWindBondBean.getVarietyCode().equals(ifsWdOpicsSecmap.getWdid())) {
				iob.setBondproperties(ifsWdOpicsSecmap.getOpicsid());
			}
		}

		// 转化债券评级
		iob.setBondratingclass("1");// 评级类别
		for (IfsWdOpicsSecmap ifsWdOpicsSecmap : wosmlist) {
			
			if (wb !=null && wb.getZxInst() !=null && wb.getZxInst().equals(ifsWdOpicsSecmap.getWdid())) {
				iob.setBondratingagency(ifsWdOpicsSecmap.getOpicsid());// 评级机构
			}
			
			if (wb !=null && wb.getZxZtLevel() !=null && wb.getZxZtLevel().equals(ifsWdOpicsSecmap.getWdid())) {
				iob.setBondrating(ifsWdOpicsSecmap.getOpicsid());//评级
			}
		}
		
		// 利率类型
		String rateType = ifsWindBondBean.getRateType().trim();
		if ("浮动利率".equals(rateType)) {
			iob.setRateType("DEPO");
			iob.setFloatratemark(ifsWindBondBean.getBasisCode());// 浮动利率基准
		} else if ("固定利率".equals(rateType)) {
			iob.setRateType("FIXED");
			iob.setCouponrate(ifsWindBondBean.getRate().toString());// 票面利率(%)
		} else if ("累进利率".equals(rateType)) {
			iob.setRateType("OPTSE");
			if(!StringUtil.isEmptyString(ifsWindBondBean.getRate().toString())) {
				iob.setCouponrate(ifsWindBondBean.getRate().toString());// 票面利率(%)
			}else {
				iob.setFloatratemark(ifsWindBondBean.getBasisCode());// 浮动利率基准
			}
		}
		
		iob.setProduct("SECUR");// 商品
		// 商品类型
		if ("IAM".equals(ifsWindBondBean.getInterestTypeCode())) {
			iob.setProdtype("IM"); // gai
		} else {
			iob.setProdtype("SD"); // gai
		}
	}

	/**
	 * 组装 债券前置实体
	 * 
	 * @param ifsWindBondBean
	 *
	 * @return
	 * @throws ParseException
	 */
	public static IfsOpicsBond generateIfsOpicsBond(IfsWindBondBean ifsWindBondBean, List<IfsWdOpicsSecmap> wosmlist,IfsWindSecLevelBean wb) {
		// 前置对象
		IfsOpicsBond iob = new IfsOpicsBond();
		GenerateDatil(iob, ifsWindBondBean, wosmlist, wb);
		iob.setStatus("A");// 交易状态
		iob.setBr("1");// 部门
		iob.setCcy(ifsWindBondBean.getCcy());// 债券币种
		iob.setSecunit("FMT");// 债券单元
		iob.setDenom("10000");// 一手的量
		iob.setDescr(ifsWindBondBean.getBondShortName()); // 描述
		iob.setSpreadrate_8(Float.parseFloat(ifsWindBondBean.getSpread().toString()));// 点差
		iob.setIntenddaterule("D");// 到期付息规则
		iob.setBndnm_cn(ifsWindBondBean.getBondLongName());// 全称
		iob.setBndnm_en(ifsWindBondBean.getBondShortName());// 简称
		iob.setBndcd(ifsWindBondBean.getBondCode());// 债券代码
		iob.setAccountno(ifsWindBondBean.getCustodianCode());// 托管账户
		// iob.setIssuevol();//实际发行量
		iob.setIssprice(ifsWindBondBean.getIssuePrice());// 发行价
		iob.setIntpaymethod("D");// 付息方式
		iob.setBondperiod(ifsWindBondBean.getTerm().toString());// 债券期限
		iob.setBasicspread(ifsWindBondBean.getSpread().toString());// 基本利差
		iob.setSettccy(ifsWindBondBean.getCcy());// 清算币种
		iob.setParamt("100");// 票面
		iob.setBasis(ifsWindBondBean.getBasisType());// 计息基础
		iob.setIntcalcrule(ifsWindBondBean.getInterestTypeCode());// 计息规则
		iob.setUserid("admin");// 录入用户id
		iob.setInputdate(DateUtil.format(new Date()));// 录入日期
		// iob.setAcctngtype(sib.getAcctngtype()); 会计类型
		iob.setSettdays("0");// 结算天数
		iob.setMarketDate(iob.getPubdt());// 上市日
		// iob.setSlPubMethod(slPubMethod);
		return iob;
	}



}
