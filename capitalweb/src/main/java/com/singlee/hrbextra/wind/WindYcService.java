package com.singlee.hrbextra.wind;

import com.singlee.capital.common.spring.mybatis.BatchDao;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.credit.util.ExcelUtil;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.marketdata.bean.YieldCurveIndex;
import com.singlee.financial.marketdata.intfc.MarketDataTransServer;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.hrbextra.wind.Util.WindUtils;
import com.singlee.ifs.mapper.IfsWindYcMapper;
import com.singlee.ifs.model.IfsWindYcBean;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 万得收益率曲线数据
 *
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class WindYcService {

	@Autowired
	BatchDao batchDao;
	@Autowired
	MarketDataTransServer marketDataTransServer;
	@Autowired
	IfsWindYcMapper ifsWindYcMapper;


	private static Logger logManager = LoggerFactory.getLogger(WindYcService.class);

	public Boolean run() throws Exception {
		logManager.info("==================开始执行 读取Excel==============================");


		logManager.info("==================在本地创建文件夹==============================");

		Date spotDate = new Date();
		String postDate = new SimpleDateFormat("yyyyMMdd").format(spotDate);
		//本地文件文件保存目录
		String filePath = SystemProperties.marketdataTargetpath;
		//远程下载文件名
		String fileName = SystemProperties.marketdataLocalfilenameYc + ".xlsx";
		Map<String ,Object> map=new HashMap<>();
		//远程文件名(不含文件后缀名)
		map.put("fileName",fileName);
		map.put("postDate",postDate);
		// 下载文件 （远程下载目录、远程文件名、本地保存目录、本地保存文件名）
		boolean flag=WindUtils.sftp(map);

		logManager.info("=================="+flag+"==============================");

		if(flag) {
			String fullName= (String) map.get("fullName");
			//本地文件绝对路径
			String excel_location = filePath + postDate + "/" + fullName;

			// 读取文件
			logManager.debug("开始读取文件：" + excel_location);
			File file = new File(excel_location);
			if (!file.exists()) {
				logManager.info("========" + excel_location + ":文件不存在=========");
				return true;
			}
			// 解析结果集
			List<IfsWindYcBean> wybs = new ArrayList<IfsWindYcBean>();

			InputStream is = new FileInputStream(file);
			ExcelUtil excelUtil = new ExcelUtil(is);
			Sheet sheet = excelUtil.getSheetAt(0);
			int rowNum = sheet.getLastRowNum();
			// 开始读取数据
			for (int i = 1; i <= rowNum; i++) {
				IfsWindYcBean wyb = new IfsWindYcBean();
				Row row = sheet.getRow(i);
				wyb = readExcel(row, wyb, spotDate);
				wybs.add(wyb);
			}
			logManager.info("==================执行完成 读取Excel==============================");

			logManager.info("==================开始执行 批量处理前置==============================");
			try {
				if (wybs.size() > 0) {
					batchDao.batch("com.singlee.ifs.mapper.IfsWindYcMapper.insert", wybs);
				}
			} catch (Exception e) {
				logManager.error("收益率曲线数据批量插入前置异常!", e);
			}
			logManager.info("==================执行完成 批量处理前置==============================");

			logManager.info("==================开始执行 批量处理OPICS==============================");
			try {
				List<YieldCurveIndex> yciLst = new ArrayList<YieldCurveIndex>();
				Example example = new Example(IfsWindYcBean.class);
				Criteria criteria = example.createCriteria();
				criteria.andCondition("SPOT_DATE = to_date('" + DateUtil.format(spotDate, "yyyy-MM-dd HH:mm:ss") + "','yyyy-mm-dd HH24:MI:SS')");
				List<IfsWindYcBean> selectList = ifsWindYcMapper.selectByExample(example);
				for (IfsWindYcBean ifsWindYcBean : selectList) {
					YieldCurveIndex yci = new YieldCurveIndex();
					//yci.setRateType(ifsWindYcBean.getRateType());
					yci.setCcy(ifsWindYcBean.getCcy());
					yci.setYieldCurveName(ifsWindYcBean.getContribRate());
					yci.setSpotDate(ifsWindYcBean.getSpotDate());
					yci.setDesignatedMaturity(ifsWindYcBean.getTenor());
					yci.setMaturityDate(ifsWindYcBean.getSpotDate());
					yci.setRate(ifsWindYcBean.getMid());
					yci.setLstmntdate(ifsWindYcBean.getSpotDate());
					yciLst.add(yci);
				}
				SlOutBean result = marketDataTransServer.saveYieldData(yciLst, 1);
				if (!result.getRetStatus().equals(RetStatusEnum.S)) {
					logManager.error("收益率曲线数据批量插入OPICS异常!");
				}
			} catch (Exception e) {
				logManager.error("收益率曲线数据批量插入OPICS异常!", e);
			}
			logManager.info("==================执行完成 批量处理OPICS==============================");
			return true;
		}else{
			logManager.info("==================文件下载失败，请重试==============================");

			return false;
		}
	}

	/**
	 * 解析表格
	 * 
	 * @param row
	 * @param wyb
	 * @return
	 */
	public static IfsWindYcBean readExcel(Row row, IfsWindYcBean wyb, Date spotDate) {
		wyb.setYieldCurveDesc(WindUtils.getString(row, 0));
		wyb.setContribRate(WindUtils.getString(row, 1));
		wyb.setCcy(WindUtils.getString(row, 2));
		wyb.setRateType(WindUtils.getString(row, 3).toUpperCase());
		wyb.setTenor(WindUtils.getString(row, 4));
		wyb.setMid(WindUtils.NBigDecimal(WindUtils.getString(row, 5)));
		wyb.setSpotDate(spotDate);
		return wyb;
	}

//	/**
//	 * 格式化数据
//	 *
//	 * @param row
//	 * @return
//	 */
//	public static String WindUtils.getString(Row row, int i) {
//		if (i == 5) {
//			if (row.getCell(i) != null&&row.getCell(i).equals("")) {
////				if (i == 2) {
////					NumberFormat instance = NumberFormat.getInstance();
////					instance.setGroupingUsed(false);
////					String format = instance.format(row.getCell(i).getCellFormula());
//				String format =row.getCell(i).WindUtils.getStringCellValue().trim();
//					return String.valueOf(format);
////				}else {
////					return String.valueOf(row.getCell(i).getNumericCellValue());
////				}
//			}
//			else {
//				return "0";
//			}
//		} else {
//			if ("".equals(row.getCell(i).WindUtils.getStringCellValue()) || null == row.getCell(i).WindUtils.getStringCellValue()) {
//				return "";
//			} else {
//				return row.getCell(i).WindUtils.getStringCellValue().trim();
//			}
//		}
//	}




}
