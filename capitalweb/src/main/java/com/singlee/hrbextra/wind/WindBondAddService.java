package com.singlee.hrbextra.wind;

import com.singlee.capital.common.spring.mybatis.BatchDao;
import com.singlee.capital.credit.util.ExcelUtil;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.hrbextra.wind.Util.WindUtils;
import com.singlee.ifs.mapper.IfsWindBondAddMapper;
import com.singlee.ifs.model.IfsWindBondAddBean;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 万得债券追加数据
 *
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class WindBondAddService {

	@Autowired
	BatchDao batchDao;
	@Autowired
	IfsWindBondAddMapper ifsWindBondAddMapper;

	
	private static Logger logManager = LoggerFactory.getLogger(WindBondAddService.class);

	public Boolean run() throws Exception {

		logManager.info("==================开始执行 读取Excel==============================");

		logManager.info("==================在本地创建文件夹==============================");

		Date spotDate = new Date();
		String postDate = new SimpleDateFormat("yyyyMMdd").format(spotDate);
		//本地文件文件保存目录
		String filePath = SystemProperties.marketdataTargetpath;
		//远程下载文件名
		String fileName = SystemProperties.marketdataLocalfilenameBondAdd;
		logManager.info("==================需要下载的文件名为："+fileName+"==============================");

		//远程文件下载目录
//		String fPath = SystemProperties.marketdataFundPath+postDate.substring(0,8)+"/";
		//本地文件生成目录
		String excel_location_path = filePath + postDate.substring(0, 8);
		Map<String ,Object> map=new HashMap<>();
		//远程文件名(不含文件后缀名)
		map.put("fileName",fileName);
		map.put("postDate",postDate);
		// 下载文件 （
		boolean flag=WindUtils.sftp(map);
		logManager.info("=================="+flag+"==============================");
		if(flag) {
			String fullName= (String) map.get("fullName");
			//本地文件绝对路径
			String excel_location = filePath + postDate + "/" + fullName;
			// 读取文件
			logManager.debug("开始读取文件：" + excel_location);
			File file = new File(excel_location);
			if (!file.exists()) {
				logManager.info("========" + excel_location + ":文件不存在=========");
				return true;
			}
			// 解析结果集
			List<IfsWindBondAddBean> wbabs = new ArrayList<IfsWindBondAddBean>();
			InputStream is = new FileInputStream(file);
			ExcelUtil excelUtil = new ExcelUtil(is);
			Sheet sheet = excelUtil.getSheetAt(0);
			int rowNum = sheet.getLastRowNum();
			// 开始读取数据
			for (int i = 1; i <= rowNum; i++) {
				IfsWindBondAddBean wbab = new IfsWindBondAddBean();
				Row row = sheet.getRow(i);
				wbab = readExcel(row, wbab, spotDate);
				wbabs.add(wbab);
			}
			logManager.info("==================执行完成 读取Excel==============================");

			logManager.info("==================开始执行 批量处理前置==============================");
			try {
				for (IfsWindBondAddBean wbab : wbabs) {
					IfsWindBondAddBean record = new IfsWindBondAddBean();
					record.setBondCode(wbab.getBondCode().trim());
					record.setAddInfo(wbab.getAddInfo().trim());
					logManager.info("=====债券ID:" + wbab.getBondCode() + ",投标期次:" + wbab.getAddInfo() + "=====");
					List<IfsWindBondAddBean> select = ifsWindBondAddMapper.select(record);
					if (null == select) {
						ifsWindBondAddMapper.insertSelective(wbab);
					} else {
						Example example = new Example(IfsWindBondAddBean.class);
						example.createCriteria().andEqualTo("bondCode", wbab.getBondCode()).andEqualTo("addInfo", wbab.getAddInfo());
						ifsWindBondAddMapper.updateByExample(wbab, example);
					}
				}
			} catch (Exception e) {
				logManager.error("债券追加数据批量插入前置异常!", e);
			}
			logManager.info("==================执行完成 批量处理前置==============================");

			return true;

		}else {
			logManager.info("==================文件下载失败，请重试==============================");

			return false;
		}
	}

	/**
	 * 解析表格
	 * 
	 * @param row
	 *
	 * @return
	 */
	public static IfsWindBondAddBean readExcel(Row row, IfsWindBondAddBean wbab, Date spotDate) {
		wbab.setBondName(WindUtils.getString(row, 0));
		wbab.setBondCode(WindUtils.getString(row, 1));
		wbab.setBondCodeOld(WindUtils.getString(row, 2));
		wbab.setFaMount(WindUtils.NBigDecimal(WindUtils.getString(row, 3)));
		wbab.setZaMount(WindUtils.NBigDecimal(WindUtils.getString(row, 4)));
		wbab.setBaMount(WindUtils.NBigDecimal(WindUtils.getString(row, 5)));
		wbab.setExpense(WindUtils.NBigDecimal(WindUtils.getString(row, 6)));
		wbab.setZbType(WindUtils.getString(row, 7));
		wbab.setFaceRate(WindUtils.NBigDecimal(WindUtils.getString(row, 8)));
		wbab.setFxRate(WindUtils.NBigDecimal(WindUtils.getString(row, 9)));
		wbab.setFxPrice(WindUtils.NBigDecimal(WindUtils.getString(row, 10)));
		wbab.setFxBjRate(WindUtils.NBigDecimal(WindUtils.getString(row, 11)));
		wbab.setFxBjPrice(WindUtils.NBigDecimal(WindUtils.getString(row, 12)));
		wbab.setQctb(WindUtils.NBigDecimal(WindUtils.getString(row, 13)));
		wbab.setBjtb(WindUtils.NBigDecimal(WindUtils.getString(row, 14)));
		wbab.setIfName(WindUtils.getString(row, 15));
		wbab.setZxLevel(WindUtils.getString(row, 16));
		wbab.setDbName(WindUtils.getString(row, 17));
		wbab.setDbType(WindUtils.getString(row, 18));
		wbab.setZbDate(WindUtils.getString(row, 19));
		wbab.setIfDate(WindUtils.getString(row, 20));
		wbab.setAddInfo(WindUtils.getString(row, 21));
		wbab.setSpotDate(spotDate);
		return wbab;
	}

//	/**
//	 * 格式换数据
//	 *
//	 * @param row
//	 * @return
//	 */
//	public static String WindUtils.getString(Row row, int i) {
//
//		if (row.getCell(i)==null){
//			return "0";
//		}
//		row.getCell(i).setCellType(CellType.STRING);
//		return 	row.getCell(i).WindUtils.getStringCellValue().trim();
//
//	}

}
