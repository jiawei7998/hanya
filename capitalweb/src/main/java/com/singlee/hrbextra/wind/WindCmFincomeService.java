package com.singlee.hrbextra.wind;

import com.singlee.capital.common.spring.mybatis.BatchDao;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.hrbextra.wind.Util.WindUtils;
import com.singlee.hrbextra.wind.mapper.CmFincomeMapper;
import com.singlee.hrbextra.wind.model.CmFincome;
import com.singlee.ifs.baseUtil.SingleeSFTPClient;
import com.singlee.ifs.file.entity.FileName;
import com.singlee.ifs.file.mapper.FileNamemapper;
import com.singlee.ifs.model.ChinaMutualFundNAV;
import com.singlee.ifs.model.IbFundPriceDay;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class WindCmFincomeService {
    @Autowired
    BatchDao batchDao;
    @Autowired
    CmFincomeMapper cmFincomeMapper;
    @Autowired
    FileNamemapper fileNamemapper;


   private static Logger logManager = LoggerFactory.getLogger(WindCmFincomeService.class);
//
//    private FileNamemapper fileNamemapper = SpringContextHolder.getBean(FileNamemapper.class);
    public Boolean run() throws Exception {
        logManager.info("==================在本地创建文件夹==============================");
        Date spotDate = new Date();
        String postDate = new SimpleDateFormat("yyyyMMdd").format(spotDate);
        //本地文件文件保存目录(压缩文件)
        String fileGZPath = SystemProperties.marketdataLocalfundInComeGZpath;
        //本地文件文件保存目录(解压后文件)
        String filePath = SystemProperties.marketdataLocalfundInComepath;
        //远程下载文件夹名
       String fileName = SystemProperties.marketdataWindfundInCome;

       // String fileName="/appdata/was/interface/windFund/CMoneyMarketFIncome/";

        File filedir = new File(fileGZPath);
        //创建文件夹
        if (!filedir.exists()) {
            filedir.mkdirs();
        }
        File filedirPath = new File(filePath);
        //创建文件夹
        if (!filedirPath.exists()) {
            filedirPath.mkdirs();
        }
        JY.info("==================开始执行 准备连接sftp下载万得债券文件==============================");
        // 初始化FTP
        SingleeSFTPClient sftp = new SingleeSFTPClient(SystemProperties.windsftpId, SystemProperties.windsftpPort, SystemProperties.windsftpUser, SystemProperties.windsftpPasswd);
        // 开始连接
        sftp.connect();
        // 下载文件 （远程下载目录、本地保存目录、下载文件格式、下载文件格式）
        List<String> filenames = sftp.batchDownLoadFile( fileName,fileGZPath, null, ".gz", false);
        sftp.disconnect();
        List<FileName> FileList = new ArrayList<>();
        List<String> files = fileNamemapper.queryAllByLimit();
        //如果数据库里没有数据，则全部解析，全部入表（第一次执行）
        if (files.size() != filenames.size()) {
            for (int i = 0; i < filenames.size(); i++) {
                //表里没有的文件名才操作
                if (!files.contains(filenames.get(i))) {
                    //绝对路径
                    String filename = filenames.get(i);
                    //解压前文件名
                    String realName=WindUtils.getFullName(filename);
                    FileName FileBean = new FileName();
                    FileBean.setFilename(filename);
                    FileBean.setInputdate(postDate);
                    FileList.add(FileBean);
                    JY.info("==================开始解压文件==============================");
                    //解压文件的绝对地址
                    String FileNamePath = fileGZPath + realName;
                    JY.info("==================解压文件绝对路径为：" + FileNamePath);
//                    //解压后文件名
                    String reFilename = WindUtils.getFileName(realName);
                    //解压后文件的绝对路径为
                    String OutFileNamePath = filePath + reFilename;
                    JY.info("==================解压后文件绝对路径为：" + OutFileNamePath);
                    boolean flag = WindUtils.doUncompressFile(FileNamePath, OutFileNamePath);
                    if (flag) {
                        JY.info("=========开始解压文件=========");
                        boolean intoFlag = intoDate(OutFileNamePath);
                        if (!intoFlag) {
                            JY.info("=========解析文件插入失败=========");
                            return false;
                        }
                    }
                }
            }
            batchDao.batch("com.singlee.ifs.file.mapper.FileNamemapper.insertall", FileList);
        } else {
            JY.info("=========没有文件需要更新=========");
        }
        return true;

    }

    public boolean intoDate(String fileName) {

        // 解析结果集
        List<CmFincome> lists = new ArrayList<CmFincome>();
        List<IbFundPriceDay> priceList = new ArrayList<IbFundPriceDay>();

        Document doc = null;
        SAXReader saxReader = new SAXReader();

        try {
            // 开始读取数据
            Class<CmFincome> cls = CmFincome.class;
            doc = saxReader.read(fileName);
            Element root = doc.getRootElement();
            List<Element> list = root.elements("Product");

            for (Element e : list) {
                List<Element> li = e.elements();
                Class<?> cl = (Class<?>) Class.forName(cls.getName());
                Object ob = cl.newInstance();
                IbFundPriceDay ibFundPriceDay=new IbFundPriceDay();
                for (Element element2 : li) {
                    String name = element2.getName();
                    final Field[] declaredFields = ChinaMutualFundNAV.class.getDeclaredFields();
                    for (Field fiels:declaredFields) {
                        String cName = fiels.getName();

                        if (name.equals(cName)){
                            String value = element2.getText();
                            Field field = ob.getClass().getDeclaredField(name);
                            field.setAccessible(true);
                            WindUtils.convertValue(field, ob, value);
                        }

                    }

                }
                CmFincome cmFincome = (CmFincome) ob;

                if (cmFincome.getS_INFO_WINDCODE()!=null&&cmFincome.getANN_DATE()!=null){


                String dateAnn=cmFincome.getANN_DATE().trim();
                StringBuffer sbd = new StringBuffer(dateAnn);
                sbd.insert(6,"-");
                sbd.insert(4,"-");
                ibFundPriceDay.setFundPrice(cmFincome.getF_INFO_UNITYIELD());//单位净值/万分收益率
                ibFundPriceDay.setFundCode(cmFincome.getS_INFO_WINDCODE());//基金代码
                ibFundPriceDay.setPostdate(String.valueOf(sbd));//账务日期

                cmFincome.setUpdateTime(DateUtil.getCurrentDateTimeAsString());
                lists.add(cmFincome);
                priceList.add(ibFundPriceDay);
                }
            }
            logManager.info("==================执行完成 读取Excel==============================");
            logManager.info("==================开始执行 批量处理前置==============================");
            if (lists.size() > 0) {
                batchDao.batch("com.singlee.hrbextra.wind.mapper.CmFincomeMapper.deleCmFincome", lists);
                batchDao.batch("com.singlee.hrbextra.wind.mapper.CmFincomeMapper.insert", lists);
            }

            logManager.info("==================万分收益表插入完成，开始插入万份收益表表==============================");

            if (priceList.size() > 0) {
                batchDao.batch("com.singlee.ifs.mapper.IbFundPriceDayMapper.delete", priceList);
                batchDao.batch("com.singlee.ifs.mapper.IbFundPriceDayMapper.insert", priceList);
            }
        } catch (DocumentException e) {
            e.printStackTrace();
            logManager.error("DocumentException异常：",e);
            return false;
        } catch (InstantiationException e) {
            e.printStackTrace();
            logManager.error("InstantiationException异常：",e);
            return false;
        } catch (NoSuchFieldException e) {
            logManager.error("NoSuchFieldException异常：",e);
            e.printStackTrace();
            return false;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            logManager.error("IllegalAccessException异常：",e);
            return false;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            logManager.error("ClassNotFoundException异常：",e);
            return false;
        } catch (Exception exception) {
            exception.printStackTrace();
            logManager.error("Exception异常：",exception);
            return false;
        }
        return true;
    }


}
