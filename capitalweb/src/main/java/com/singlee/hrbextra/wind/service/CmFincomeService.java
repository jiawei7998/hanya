package com.singlee.hrbextra.wind.service;

import com.github.pagehelper.Page;
import com.singlee.hrbextra.wind.model.CmFincome;

import java.util.List;
import java.util.Map;

public interface CmFincomeService {

    Page<CmFincome> getCmFincomePage(Map<String, Object> map);

    List<CmFincome> getCmFincomeList(Map<String, Object> map);

}
