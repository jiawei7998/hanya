package com.singlee.hrbextra.wind;

import com.singlee.capital.common.spring.mybatis.BatchDao;
import com.singlee.capital.credit.util.ExcelUtil;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.hrbextra.wind.Util.WindUtils;
import com.singlee.ifs.mapper.IfsWindRiskMapper;
import com.singlee.ifs.model.IfsWindRiskBean;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 万得风险数据
 *
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class WindRiskService {

	@Autowired
	BatchDao batchDao;
	@Autowired
	IfsWindRiskMapper ifsWindRiskMapper;


	private static Logger logManager = LoggerFactory.getLogger(WindRiskService.class);

	public Boolean run() throws Exception {
		logManager.info("==================开始执行 读取Excel==============================");
		// 组装文件名

		logManager.info("==================在本地创建文件夹==============================");
		Date spotDate = new Date();
		String postDate = new SimpleDateFormat("yyyyMMdd").format(spotDate);
		//本地文件文件保存目录
		String filePath = SystemProperties.marketdataTargetpath;
		//远程下载文件名
		String fileName = SystemProperties.marketdataLocalfilenameRisk;
		Map<String ,Object> map=new HashMap<>();
		//远程文件名(不含文件后缀名)
		map.put("fileName",fileName);
		map.put("postDate",postDate);
		// 下载文件 （远程下载目录、远程文件名、本地保存目录、本地保存文件名）
		boolean flag=WindUtils.sftp(map);

		if(flag) {
        String fullName= (String) map.get("fullName");
        //本地文件绝对路径
	    String excel_location = filePath + postDate + "/" + fullName;
		// 读取文件
		logManager.debug("开始读取文件：" + excel_location);
		File file = new File(excel_location);
		if (!file.exists()) {
			logManager.info("========" + excel_location + ":文件不存在=========");
			return true;
		}
		// 解析结果集
		List<IfsWindRiskBean> wrbs = new ArrayList<IfsWindRiskBean>();

		InputStream is = new FileInputStream(file);
		ExcelUtil excelUtil = new ExcelUtil(is);
		Sheet sheet = excelUtil.getSheetAt(0);
		int rowNum = sheet.getLastRowNum();
		// 开始读取数据
		for (int i = 1; i <= rowNum; i++) {
			IfsWindRiskBean wrb = new IfsWindRiskBean();
			Row row = sheet.getRow(i);
			wrb = readExcel(row, wrb);
			wrbs.add(wrb);
		}
		logManager.info("==================执行完成 读取Excel==============================");

		logManager.info("==================开始执行 批量处理前置==============================");
		try {
			for (IfsWindRiskBean wrb : wrbs) {
				IfsWindRiskBean record= new IfsWindRiskBean();
				record.setBondCode(wrb.getBondCode().trim());
				record.setRdate(wrb.getRdate().trim());
				logManager.info("=====债券ID:" + wrb.getBondCode() + ",日期:" + wrb.getRdate() + "=====");
				List<IfsWindRiskBean> select = ifsWindRiskMapper.select(record);
				if(null == select) {
					ifsWindRiskMapper.insertSelective(wrb);
				}else {
					Example example = new Example(IfsWindRiskBean.class);
					example.createCriteria().andEqualTo("bondCode", wrb.getBondCode()).andEqualTo("rdate", wrb.getRdate());
					ifsWindRiskMapper.updateByExample(wrb, example);
				}
			}
		} catch (Exception e) {
			logManager.error("风险数据批量插入前置异常!", e);
		}
		logManager.info("==================执行完成 批量处理前置==============================");

		return true;
		}else
		{
			logManager.info("==================文件下载失败，请重试==============================");
			logManager.info("=================="+flag+"==============================");

			return false;
		}

	}

	/**
	 * 解析表格
	 *
	 * @param row
	 * @return
	 */
	public static IfsWindRiskBean readExcel(Row row, IfsWindRiskBean wrb) {
		wrb.setBondCode(WindUtils.getString(row, 0));
		wrb.setBondName(WindUtils.getString(row, 1));
		wrb.setRdate(WindUtils.getString(row, 2));
		wrb.setJiuqi(WindUtils.NBigDecimal(WindUtils.getString(row, 3)));
		wrb.setTuxing(WindUtils.NBigDecimal(WindUtils.getString(row, 4)));
		wrb.setPv(WindUtils.NBigDecimal(WindUtils.getString(row, 5)));
		wrb.setMemo(WindUtils.NBigDecimal(WindUtils.getString(row, 6)));
		return wrb;
	}

//	/**
//	 * 格式化数据
//	 *
//	 * @param row
//	 * @return
//	 */
//	public static String WindUtils.getString(Row row, int i) {
//		if (i == 2 || i == 3 || i == 4 || i == 5 || i == 6) {
//			if (row.getCell(i) != null) {
//				if (i == 2) {
//					NumberFormat instance = NumberFormat.getInstance();
//					instance.setGroupingUsed(false);
//					String format = instance.format(row.getCell(i).getNumericCellValue());
//					return String.valueOf(format);
//				} else {
//					return String.valueOf(row.getCell(i).getNumericCellValue());
//				}
//			} else {
//				return "0";
//			}
//		} else {
//			if ("".equals(row.getCell(i).WindUtils.getStringCellValue()) || null == row.getCell(i).WindUtils.getStringCellValue()) {
//				return "";
//			} else {
//				return row.getCell(i).WindUtils.getStringCellValue().trim();
//			}
//		}
//	}
}
