package com.singlee.hrbextra.wind.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbextra.wind.mapper.CmFincomeMapper;
import com.singlee.hrbextra.wind.model.CmFincome;
import com.singlee.hrbextra.wind.service.CmFincomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 货币基金万分收益
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class CmFincomeServiceImpl implements CmFincomeService {
    @Autowired
    CmFincomeMapper cmFincomeMapper;

    @Override
    public Page<CmFincome> getCmFincomePage(Map<String, Object> map) {
        return cmFincomeMapper.getCmFincomePage(map, ParameterUtil.getRowBounds(map));
    }

    @Override
    public List<CmFincome> getCmFincomeList(Map<String, Object> map) {
        List<CmFincome> list =cmFincomeMapper.getCmFincomeList(map);
        return list;
    }


}
