package com.singlee.hrbextra.wind.Util;

import com.singlee.capital.common.util.JY;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.ifs.baseUtil.SingleeSFTPClient;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;

import java.io.*;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.zip.GZIPInputStream;

/**
 * 万得工具类
 */
public class WindUtils {
    /**
     * 反射设置实体不同类型字段的值 <暂时只支持 日期 字符串 boolean Integer值设置 待扩建>
     *
     * @param field
     * @param obj
     * @param value
     * @throws Exception
     */
    public static void convertValue(Field field, Object obj, String value)
            throws Exception {
        SimpleDateFormat sim = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        if ("class java.lang.Integer".equals(field.getGenericType().toString())) {
            field.set(obj, Integer.parseInt(value));
        } else if ("boolean".equals(field.getGenericType().toString())) {
            field.set(obj, Boolean.parseBoolean(value));
        } else if ("class java.util.Date".equals(
                field.getGenericType().toString())) {
            field.set(obj, sim.parse(value));
        } else if ("class java.math.BigDecimal".equals(
                field.getGenericType().toString())) {
            field.set(obj, new BigDecimal(value));
        } else {
            field.set(obj, value);
        }

    }

    /**
     * 下载单个文件
     *chenguo 20211119
     * @return
     */
    public static boolean sftp(Map<String ,Object> map) {
        String postDate= (String) map.get("postDate");
        String fileName= (String) map.get("fileName");
        //本地文件文件保存目录
        String filePath = SystemProperties.marketdataTargetpath + postDate.substring(0, 8) + "/";
        //远程文件下载目录
       String fPath = SystemProperties.marketdataFundPath + postDate.substring(0, 8) + "/";


        JY.info("==================远程下载文件名为"+fileName+"==============================");
        JY.info("==================在本地创建文件夹，路径为：" + filePath + "==============================");
        File filedir = new File(filePath);
        //创建文件夹
        if (!filedir.exists()) {
            filedir.mkdirs();
        }
        //远程下载目录
        map.put("fPath",fPath);
        //本地保存目录
        map.put("filePath",filePath);


        JY.info("==================开始执行 准备连接sftp下载万得债券文件==============================");
        // 初始化FTP
         SingleeSFTPClient sftp = new SingleeSFTPClient("130.1.10.10", 22, "qindejin@was@130.1.14.66", "1234Abcd");
        // 开始连接
        sftp.connect();
        // 下载文件 （远程下载目录、远程文件名、本地保存目录）
        boolean flag = sftp.downloadFile(map);
        JY.info("==================" + flag + "==============================");
        sftp.disconnect();

        return flag;
    }


    //解压文件（压缩文件绝对路劲，解压后文件绝对路劲）
    public  static boolean doUncompressFile(String inFileName,String outFileName){
        try {
            if (!getExtension(inFileName).equalsIgnoreCase("gz")) {
                JY.info("文件格式必须是.gz结尾");
                return  false;
            }
            JY.info("====================解压文件绝对位置为:"+inFileName);
            JY.info("====================解压中====================");
            GZIPInputStream in = null;
            try {
                in = new GZIPInputStream(new FileInputStream(inFileName));
            } catch(FileNotFoundException e) {
                JY.info("压缩文件" + inFileName+"没有找到");
                return  false;
            }
            JY.info("Open the output file.");
            //解压文件绝对地址
//            String outFileName = getFileName(inFileName);
            JY.info("===================解压后文件的绝对路径为："+outFileName);
//            outFileName
            FileOutputStream out = null;

            try {
                out = new FileOutputStream(outFileName);
            } catch (FileNotFoundException e) {
                JY.info("================Could not write to file. " + outFileName);
                return  false;
            }
            System.out.println("===================从压缩文件传输字节到输出文件========================");
            byte[] buf = new byte[1024];
            int len;
            while((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }

            JY.info("Closing the file and stream");
            in.close();
            out.close();

        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        return true;
        
    }

    //获取文件格式（.gz）
    public static String getExtension(String f) {
        String ext = "";
        int i = f.lastIndexOf('.');

        if (i > 0 &&  i < f.length() - 1) {
            ext = f.substring(i+1);
        }
        return ext;
    }

    //获取解压后文件名称
    public static String getFileName(String f) {
        String fname = "";
        int i = f.lastIndexOf('.');

        if (i > 0 &&  i < f.length() - 1) {
            fname = f.substring(0,i);
        }
        return fname;
    }
    //获取远程绝对路径的文件名
    public static String getFullName(String f) {
        String ext = "";
        int i = f.lastIndexOf('/');

        if (i > 0 &&  i < f.length() - 1) {
            ext = f.substring(i+1);
        }
        return ext;
    }


    public  static String  getString(Row row, int i){
        if (row.getCell(i)==null){
            return "0";
        }
        row.getCell(i).setCellType(CellType.STRING);
        String name=row.getCell(i).getStringCellValue().trim();
        return 	name.trim();

    }

    public  static  BigDecimal  NBigDecimal(String str){
        if (str.equals("")){
            str="0";
        }
        return new BigDecimal(str);
    }

}
