package com.singlee.hrbextra.wind;

import com.singlee.capital.common.spring.mybatis.BatchDao;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.credit.util.ExcelUtil;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.marketdata.bean.RateHistoryInformation;
import com.singlee.financial.marketdata.intfc.MarketDataTransServer;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.hrbextra.wind.Util.WindUtils;
import com.singlee.ifs.mapper.IfsWindRateMapper;
import com.singlee.ifs.model.IfsWindRateBean;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 *  万得产品利率数据
 *
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class WindRateService {

	@Autowired
	BatchDao batchDao;
	@Autowired
	IfsWindRateMapper ifsWindRateMapper;
	@Autowired
	MarketDataTransServer marketDataTransServer;

	
	private static Logger logManager = LoggerFactory.getLogger(WindRateService.class);

	public Boolean run() throws Exception {

		// 组装文件名

		logManager.info("==================在本地创建文件夹==============================");

		Date spotDate = new Date();
		String postDate = new SimpleDateFormat("yyyyMMdd").format(spotDate);
		//本地文件文件保存目录
		String filePath = SystemProperties.marketdataTargetpath;
		//远程下载文件名
		String fileName = SystemProperties.marketdataLocalfilenameRate;

		//远程文件下载目录
//		String fPath = SystemProperties.marketdataFundPath+postDate.substring(0,8)+"/";
		//本地文件生成目录
		String excel_location_path = filePath + postDate.substring(0, 8);
		Map<String ,Object> map=new HashMap<>();
		//远程文件名(不含文件后缀名)
		map.put("fileName",fileName);
		map.put("postDate",postDate);
		// 下载文件 （远程下载目录、远程文件名、本地保存目录、本地保存文件名）
		boolean flag=WindUtils.sftp(map);


		logManager.info("=================="+flag+"==============================");

		if(flag) {

			logManager.info("==================开始执行 读取Excel==============================");
			String fullName= (String) map.get("fullName");
			//本地文件绝对路径
			String excel_location = filePath + postDate + "/" + fullName;
			// 读取文件
			logManager.debug("开始读取文件：" + excel_location);
			File file = new File(excel_location);
			if (!file.exists()) {
				logManager.info("========" + excel_location + ":文件不存在=========");
				return true;
			}
			// 解析结果集
			List<IfsWindRateBean> wrbs = new ArrayList<IfsWindRateBean>();

			InputStream is = new FileInputStream(file);
			ExcelUtil excelUtil = new ExcelUtil(is);
			Sheet sheet = excelUtil.getSheetAt(0);
			int rowNum = sheet.getLastRowNum();
			// 开始读取数据
			for (int i = 1; i <= rowNum; i++) {
				IfsWindRateBean wrb = new IfsWindRateBean();
				Row row = sheet.getRow(i);
				wrb = readExcel(row, wrb, spotDate);
				wrbs.add(wrb);
			}
			logManager.info("==================执行完成 读取Excel==============================");

			logManager.info("==================开始执行 批量处理前置==============================");
			try {
				if (wrbs.size() > 0) {
					batchDao.batch("com.singlee.ifs.mapper.IfsWindRateMapper.insert", wrbs);
				}
			} catch (Exception e) {
				logManager.error("重定利率数据批量插入前置异常!", e);
			}
			logManager.info("==================执行完成 批量处理前置==============================");

			logManager.info("==================开始执行 批量处理OPICS==============================");
			try {
				List<RateHistoryInformation> rhiLst = new ArrayList<RateHistoryInformation>();
				Example example = new Example(IfsWindRateBean.class);
				Criteria criteria = example.createCriteria();
				criteria.andCondition("SPOT_DATE = to_date('" + DateUtil.format(spotDate, "yyyy-MM-dd HH:mm:ss") + "','yyyy-mm-dd HH24:MI:SS')");
				List<IfsWindRateBean> selectList = ifsWindRateMapper.selectByExample(example);
				for (IfsWindRateBean ifsWindRateBean : selectList) {
					RateHistoryInformation rhi = new RateHistoryInformation();
					rhi.setBr("01");
					rhi.setSpotDate(ifsWindRateBean.getSpotDate());
					rhi.setLstmntdate(ifsWindRateBean.getSpotDate());
					rhi.setRateCode(ifsWindRateBean.getOpicsRateCode());
					rhi.setIntRate(ifsWindRateBean.getMid());
					rhiLst.add(rhi);
				}
				SlOutBean result = marketDataTransServer.saveRateData(rhiLst, 1);
				if (!result.getRetStatus().equals(RetStatusEnum.S)) {
					logManager.error("重定利率数据批量插入OPICS异常!");
				}
			} catch (Exception e) {
				logManager.error("重定利率数据批量插入OPICS异常!", e);
			}
			logManager.info("==================执行完成 批量处理OPICS==============================");

			return true;
		}else
		{
			logManager.info("==================文件下载失败，请重试==============================");

			return false;

		}
	}

	/**
	 * 解析表格
	 * 
	 * @param row
	 * @return
	 */
	public static IfsWindRateBean readExcel(Row row, IfsWindRateBean wrb, Date spotDate) {
		wrb.setRateName(WindUtils.getString(row, 0));
		wrb.setTenor(WindUtils.getString(row, 1));
		wrb.setOpicsRateCode(WindUtils.getString(row, 2));
		wrb.setType(WindUtils.getString(row, 3));
		wrb.setMid(WindUtils.NBigDecimal(WindUtils.getString(row, 4)));
		wrb.setSpotDate(spotDate);
		return wrb;
	}

//	/**
//	 * 格式换数据
//	 *
//	 * @param row
//	 * @return
//	 */
//	public static String WindUtils.getString(Row row, int i) {
//		if (i == 4) {
//			return String.valueOf(row.getCell(i).getNumericCellValue());
//		} else {
//			if ("".equals(row.getCell(i).WindUtils.getStringCellValue()) || null == row.getCell(i).WindUtils.getStringCellValue()) {
//				return "";
//			} else {
//				return row.getCell(i).WindUtils.getStringCellValue().trim();
//			}
//		}
//	}
}
