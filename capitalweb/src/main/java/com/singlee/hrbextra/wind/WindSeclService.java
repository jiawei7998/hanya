package com.singlee.hrbextra.wind;

import com.singlee.capital.common.spring.mybatis.BatchDao;
import com.singlee.capital.credit.util.ExcelUtil;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.marketdata.bean.BondMarketPrices;
import com.singlee.financial.marketdata.intfc.MarketDataTransServer;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.hrbextra.wind.Util.WindUtils;
import com.singlee.ifs.mapper.IfsWindSeclMapper;
import com.singlee.ifs.model.IfsWindSeclBean;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 万得债券收盘价数据
 */
@Service
@Transactional(value = "transactionManager", rollbackFor = Exception.class)
public class WindSeclService {

    @Autowired
    BatchDao batchDao;
    @Autowired
    MarketDataTransServer marketDataTransServer;
    @Autowired
    IfsWindSeclMapper ifsWindSeclMapper;


    private static Logger logManager = LoggerFactory.getLogger(WindSeclService.class);

    public Boolean run() throws Exception {
        logManager.info("==================开始执行 读取Excel==============================");
        // 组装文件名

        Date spotDate = new Date();
        String postDate = new SimpleDateFormat("yyyyMMdd").format(spotDate);
        //本地文件文件保存目录
        String filePath = SystemProperties.marketdataTargetpath;
        //远程下载文件名
        String fileName = SystemProperties.marketdataLocalfilenameBond;
        Map<String ,Object> map=new HashMap<>();
        //远程文件名(不含文件后缀名)
        map.put("fileName",fileName);
        map.put("postDate",postDate);
        // 下载文件 （远程下载目录、远程文件名、本地保存目录、本地保存文件名）
        boolean flag = WindUtils.sftp(map);

        logManager.info("==================" + flag + "==============================");

        if (flag) {
            String fullName= (String) map.get("fullName");
            //本地文件绝对路径
            String excel_location = filePath + postDate + "/" + fullName;
            // 读取文件
            logManager.debug("开始读取文件：" + excel_location);
            File file = new File(excel_location);
            if (!file.exists()) {
                logManager.info("========" + excel_location + ":文件不存在=========");
                return true;
            }
            // 结果集
            List<IfsWindSeclBean> wsbs = new ArrayList<IfsWindSeclBean>();

            InputStream is = new FileInputStream(file);
            ExcelUtil excelUtil = new ExcelUtil(is);
            Sheet sheet = excelUtil.getSheetAt(0);
            int rowNum = sheet.getLastRowNum();
            // 遍历每行数据
            for (int i = 1; i <= rowNum; i++) {
                IfsWindSeclBean wsb = new IfsWindSeclBean();
                Row row = sheet.getRow(i);
                wsb = readExcel(row, wsb);
                wsbs.add(wsb);
            }
            logManager.info("==================执行完成 读取Excel==============================");

            logManager.info("==================开始执行 批量处理前置==============================");
            try {
                for (IfsWindSeclBean wsb : wsbs) {
                    logManager.info("=====前置系统导入------》债券ID:" + wsb.getSecCode() + "=====");
                    IfsWindSeclBean selectByPrimaryKey = ifsWindSeclMapper.selectByPrimaryKey(wsb.getSecCode());
                    if (selectByPrimaryKey == null) {
                        ifsWindSeclMapper.insertSelective(wsb);
                    } else {
                        ifsWindSeclMapper.updateByPrimaryKeySelective(wsb);
                    }

                }
            } catch (Exception e) {
                logManager.error("债券收盘价数据批量插入前置异常!", e);
            }
//			logManager.info("==================执行完成 批量处理前置==============================");

            logManager.info("==================开始执行 批量处理OPICS==============================");
            try {
                //查询所有
                List<IfsWindSeclBean> selectList = ifsWindSeclMapper.selectIfsWindSeclLst(new HashMap<>());

                Map<String, BondMarketPrices> bmpLst = new HashMap<String, BondMarketPrices>();
                for (IfsWindSeclBean ifswsb : selectList) {
                    logManager.info("=====OPICS系统导入------》债券ID:" + ifswsb.getSecCode() + "=====流通场所" + ifswsb.getCirculatePlaceCode());
                    BondMarketPrices bmp = new BondMarketPrices();
                    //处理银行间与非银行间债券ID
                    bmp.setBondCode(StringUtils.equals(ifswsb.getCirculatePlaceCode(), "IB") ? ifswsb.getSecCode() : ifswsb.getSecCode() + "." + ifswsb.getCirculatePlaceCode());
                    bmp.setCleanPrice(ifswsb.getCleanPrice());
                    bmp.setSpotDate(new SimpleDateFormat("yyyyMMdd").parse(ifswsb.getAppraisementDate()));
                    bmpLst.put(ifswsb.getSecCode(), bmp);
                }
                SlOutBean result = marketDataTransServer.savePriceData(bmpLst);
                if (!result.getRetStatus().equals(RetStatusEnum.S)) {
                    logManager.error("债券收盘价数据批量插入OPICS异常!");
                }
            } catch (Exception e) {
                logManager.error("债券收盘价数据批量插入OPICS异常!");
            }
            logManager.info("==================执行完成 批量处理OPICS==============================");

            return true;
        } else {
            logManager.info("==================文件下载失败，请重试==============================");

            return false;
        }
    }

    /**
     * 解析表格
     *
     * @param row
     * @param wsb
     * @return
     */
    public static IfsWindSeclBean readExcel(Row row, IfsWindSeclBean wsb) {
        wsb.setSecName(WindUtils.getString(row, 0));
        wsb.setSecCode(WindUtils.getString(row, 1));
        wsb.setAppraisementDate(String.valueOf(WindUtils.getString(row, 2)));
        wsb.setCirculatePlace(WindUtils.getString(row, 3));
        wsb.setCleanPrice(WindUtils.NBigDecimal(WindUtils.getString(row, 4)));
        wsb.setDirtyPrice(WindUtils.NBigDecimal(WindUtils.getString(row, 5)));
        wsb.setMarketPrice(WindUtils.NBigDecimal(WindUtils.getString(row, 6)));
        wsb.setEcaluatePrice(WindUtils.NBigDecimal(WindUtils.getString(row, 7)));
        return wsb;
    }

//    /**
//     * 格式化数据
//     *
//     * @param row
//     * @param i
//     * @return
//     */
//    public static String WindUtils.getString(Row row, int i) {
//        if (i == 2 || i == 4 || i == 5 || i == 6 || i == 7) {
//            if (row.getCell(i) != null) {
//                if (i == 2) {
//                    NumberFormat instance = NumberFormat.getInstance();
//                    instance.setGroupingUsed(false);
//                    String format = instance.format(row.getCell(i).getNumericCellValue());
//                    return String.valueOf(format);
//                } else {
//                    return String.valueOf(row.getCell(i).getNumericCellValue());
//                }
//            } else {
//                return "0";
//            }
//        } else {
//            if ("".equals(row.getCell(i).WindUtils.getStringCellValue()) || null == row.getCell(i).WindUtils.getStringCellValue()) {
//                return "";
//            } else {
//                return row.getCell(i).WindUtils.getStringCellValue().trim();
//            }
//        }
//    }
}
