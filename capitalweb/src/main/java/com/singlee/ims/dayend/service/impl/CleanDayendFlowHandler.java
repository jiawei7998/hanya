package com.singlee.ims.dayend.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.singlee.capital.base.service.AttachService;
import com.singlee.capital.base.service.AttachTreeTemplateService;
import com.singlee.capital.common.session.SlSession;
import com.singlee.capital.trade.service.AttachTreeMapService;

/**
 * 清除附件数据
 * 
 * @author x230i
 * 
 */
@Service("cleanDayendFlowHandler")
public class CleanDayendFlowHandler{
	@Autowired
	AttachService attachService;
	@Autowired
	AttachTreeMapService attachTreeMapService;
	@Autowired
	AttachTreeTemplateService attachTreeTemplateService;
	
	public void execute(SlSession session) {
		// 清除附件中没有归属的 
		attachService.clearAttach();
		attachTreeMapService.clearAttachTreeMap();
		// 清除没有关联类型的附件树模版
		attachTreeTemplateService.clearAttachTreeTemplate();
	}
}
