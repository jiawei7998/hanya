package com.singlee.ims.approve.service.impl;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.TypeReference;
import com.joyard.jc.dataservice.ExternalDictConstants;
import com.joyard.jc.service.CalendarService;
import com.joyard.jc.time.Calendar;
import com.joyard.jc.time.Date;
import com.joyard.jc.util.QLUtils;
import com.singlee.capital.acc.model.TtAccOutCash;
import com.singlee.capital.acc.service.AccOutCashService;
import com.singlee.capital.approve.mapper.TrdOrderMapper;
import com.singlee.capital.approve.model.TtTrdOrder;
import com.singlee.capital.approve.service.ApproveServiceListener;
import com.singlee.capital.cashflow.model.TtCashflowInterest;
import com.singlee.capital.common.spring.json.FastJsonUtil;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.MathUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.trade.model.TdLiabilitiesDuration;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.trade.model.TtTrdTrade;
import com.singlee.capital.trade.service.AdvanceMaturityService;
import com.singlee.capital.trade.service.ApproveCancelService;
import com.singlee.capital.trade.service.CashFlowChangeService;
import com.singlee.capital.trade.service.InterestSettleService;
import com.singlee.capital.trade.service.LiabilitiesDurationService;
import com.singlee.capital.trade.service.MulResaleService;
import com.singlee.capital.trade.service.ProductApproveService;
import com.singlee.capital.trade.service.RateChangeService;
import com.singlee.capital.trade.service.TradeExtendService;
import com.singlee.capital.trade.service.TradeInterfaceService;
import com.singlee.capital.trade.service.TradeOffsetService;
import com.singlee.capital.trust.model.TtTrdTrust;

@Service("approveCustomServiceImpl")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class ApproveCustomServiceImpl implements ApproveServiceListener {
	@Autowired
	private ProductApproveService productApproveService;
	@Autowired
	private AdvanceMaturityService advanceMaturityService;
	@Autowired
	private RateChangeService rateChangeService;
	@Autowired
	private CashFlowChangeService cashFlowChangeService;
	@Autowired
	private TradeExtendService tradeExtendService;
	@Autowired
	private MulResaleService mulResaleService;
	@Autowired
	private ApproveCancelService approveCancelService;
	@Autowired
	private TradeOffsetService tradeOffsetService;
	@Autowired
	private AccOutCashService accOutCashService;
	@Resource
	private TradeInterfaceService tradeInterfaceService;
	@Autowired
	private LiabilitiesDurationService liabilitiesDurationService;
	@Autowired
	InterestSettleService interestSettleService;
	@Autowired
	TrdOrderMapper trdOrderMapper;
	/** 业务日 */
	@Autowired
	private DayendDateService dayendDateService;
	@Override
	public Object searchApproveList(HashMap<String, Object> map, int pageNum, int pageSize) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void selectApproveInfo(HashMap<String, Object> map) {
		// TODO Auto-generated method stub

	}
	/**
	 * 设置自定业务审批单信息
	 * @param map 传入参数
	 * @param ttTrdOrder 审批单对象
	 * @return
	 */
	private void setCustomOrderInfo(Map<String,Object> map,TtTrdOrder ttTrdOrder){
		if (DictConstants.TrdType.Custom.equals(ttTrdOrder.getTrdtype()) || DictConstants.TrdType.CustomMultiResale.equals(ttTrdOrder.getTrdtype())) {
			ttTrdOrder.setI_code(ttTrdOrder.getOrder_id());
		}
		else{
			//icode赋值原交易流水（页面refNo）
			ttTrdOrder.setI_code(ParameterUtil.getString(map, "refNo", ""));
		}
		ttTrdOrder.setA_type(DictConstants.AType.IBMoney);
		ttTrdOrder.setM_type(DictConstants.MarketType.Bank);
		ttTrdOrder.setI_name(ParameterUtil.getString(map, "conTitle", ""));
		String businessDate = dayendDateService.getSettlementDate();  //当前账务日期
		if (DictConstants.TrdType.Custom.equals(ttTrdOrder.getTrdtype())) {
			//自定义业务首期审批单日期去交易日期
			ttTrdOrder.setOrder_date(ParameterUtil.getString(map, "aDate", businessDate));
			//金额信息
			ttTrdOrder.setTotalamount(ParameterUtil.getDouble(map, "amt", 0.0));
			ttTrdOrder.setTotalcount(ttTrdOrder.getTotalamount());
			ttTrdOrder.setPreorder_amount(ttTrdOrder.getTotalamount());
			ttTrdOrder.setUsed_amount(0);
			ttTrdOrder.setRemain_amount(ttTrdOrder.getTotalamount());
			ttTrdOrder.setAiamount(ParameterUtil.getDouble(map, "intAmt", 0));
			//结算金额判断收息方式：先收息减去利息 mod by lihb 20170213
			String intType = ParameterUtil.getString(map, "intType", "");
			if(!StringUtil.isNullOrEmpty(intType) && DictConstants.intType.chargeBefore.equals(intType)) {
				ttTrdOrder.setFst_settle_amount(MathUtil.round(ttTrdOrder.getTotalamount() - ttTrdOrder.getAiamount(),2));
			} else {
				ttTrdOrder.setFst_settle_amount(MathUtil.round(ttTrdOrder.getTotalamount() + ttTrdOrder.getAiamount(),2));
			}
			ttTrdOrder.setEnd_settle_amount(ParameterUtil.getDouble(map, "mAmt", 0));
			//清算路径信息
			ttTrdOrder.setSelf_bankacccode(ParameterUtil.getString(map, "selfAccCode", ""));
			ttTrdOrder.setSelf_bankaccname(ParameterUtil.getString(map, "selfAccName", ""));
			ttTrdOrder.setSelf_bankcode(ParameterUtil.getString(map, "selfBankCode", ""));
			ttTrdOrder.setSelf_bankname(ParameterUtil.getString(map, "selfBankName", ""));
			//获取本方外部资金账户ID
			if(!StringUtil.isNullOrEmpty(ttTrdOrder.getSelf_bankacccode())){
				List<TtAccOutCash> accs = accOutCashService.getAcc(ttTrdOrder.getSelf_bankacccode(), ttTrdOrder.getSelf_bankcode());
				if(accs != null && accs.size() > 0) {
					ttTrdOrder.setSelf_outcashaccid(accs.get(0).getAccid());
				}
			}
			ttTrdOrder.setParty_bankacccode(ParameterUtil.getString(map, "partyAccCode", ""));
			ttTrdOrder.setParty_bankaccname(ParameterUtil.getString(map, "partyAccName", ""));
			ttTrdOrder.setParty_bankcode(ParameterUtil.getString(map, "partyBankCode", ""));
			ttTrdOrder.setParty_bankname(ParameterUtil.getString(map, "partyBankName", ""));
			//其他信息
			ttTrdOrder.setYtm(ParameterUtil.getDouble(map, "contractRate", 0.0));
			ttTrdOrder.setTerm_days(ParameterUtil.getDouble(map, "term", 0.0));
			//起息日
			String valueDate = ParameterUtil.getString(map, "vDate", "");
			//划款日
			String tDate = ParameterUtil.getString(map, "tDate", "");
			//结算日期如果有划款日，则为划款日，如果没有，则为起息日,如果还没有，则审批开始日期
			if(!StringUtil.isNullOrEmpty(tDate)) {
				ttTrdOrder.setFst_settle_date(tDate);
			} else if(!StringUtil.isNullOrEmpty(valueDate)) {
				ttTrdOrder.setFst_settle_date(valueDate);
			} else {
				ttTrdOrder.setFst_settle_date(ttTrdOrder.getOrder_date());
			}
			ttTrdOrder.setEnd_settle_date(ParameterUtil.getString(map, "mDate", ""));
			ttTrdOrder.setSelf_traderid(ParameterUtil.getString(map, "sponsor", ttTrdOrder.getOperator_id()));
			ttTrdOrder.setParty_id(ParameterUtil.getString(map, "party_id", ""));
			ttTrdOrder.setParty_type(DictConstants.PartType.Outider);
		}
		else if(DictConstants.TrdType.CustomAdvanceMaturity.equals(ttTrdOrder.getTrdtype())){
			ttTrdOrder.setOrder_date(ParameterUtil.getString(map, "amDate", businessDate));
			//金额信息
			ttTrdOrder.setTotalamount(ParameterUtil.getDouble(map, "amAmt", 0.0));
			ttTrdOrder.setYtm(ParameterUtil.getDouble(map, "amRate", 0.0));
			ttTrdOrder.setTotalcount(ttTrdOrder.getTotalamount());
			ttTrdOrder.setPreorder_amount(ttTrdOrder.getTotalamount());
			ttTrdOrder.setUsed_amount(0);
			ttTrdOrder.setRemain_amount(ttTrdOrder.getTotalamount());
			ttTrdOrder.setAiprice(ParameterUtil.getDouble(map, "amInt", 0.0));
			ttTrdOrder.setAiamount(ParameterUtil.getDouble(map, "actualInt", 0.0));
			ttTrdOrder.setFst_settle_amount(MathUtil.round(ttTrdOrder.getTotalamount() + ttTrdOrder.getAiamount(),2));
			//其他信息
			ttTrdOrder.setFst_settle_date(ttTrdOrder.getOrder_date());
			//发起人取操作人，即session
			ttTrdOrder.setSelf_traderid(ttTrdOrder.getOperator_id());
			ttTrdOrder.setParty_type(DictConstants.PartType.Outider);
			//获取原业务核实交易单信息
			JY.require(!StringUtil.isNullOrEmpty(ttTrdOrder.getI_code()), "原业务核实交易单号为空！请确认");
			TtTrdTrade oldTrade = tradeInterfaceService.selectTrdTrade(ttTrdOrder.getI_code(),0);
			JY.require(oldTrade!=null, "找不到原业务核实交易单，原业务核实交易单号:" + ttTrdOrder.getI_code());
			setOldOrderInfo(ttTrdOrder,oldTrade);
		}
		else if(DictConstants.TrdType.CustomCashFlowChange.equals(ttTrdOrder.getTrdtype())){
			ttTrdOrder.setOrder_date(ParameterUtil.getString(map, "dealDate", businessDate));
			String interestStr =ParameterUtil.getString(map, "interestCashFlowList", "");
			if(StringUtil.isNotEmpty(interestStr)){
				List<TtCashflowInterest> cashflowInterests = FastJsonUtil.parseObject(ParameterUtil.getString(map, "interestCashFlowList", ""), new TypeReference<List<TtCashflowInterest>>() {});
				//金额信息
				double totalAiAmount = 0;
				for(TtCashflowInterest cashflowInterest : cashflowInterests){
					Calendar calendar = CalendarService.getInstance().getIBCalender();
					Date payMentDate =calendar.adjust(new Date(cashflowInterest.getTheoryPaymentDate()), QLUtils.toConvention(ExternalDictConstants.DateAdjustment_Following));
					JY.require(payMentDate!=null && (!payMentDate.isNull()), "根据理论支付日 %s计算实际支付日失败!",cashflowInterest.getTheoryPaymentDate());
					if(payMentDate.toString().equals(businessDate)){
						totalAiAmount +=cashflowInterest.getReceivableInterest();
						if(DictConstants.SettleCashInstBussType.RECEIVE.equals(cashflowInterest.getPayDirection())){
							ttTrdOrder.setAiprice(-1);
						}
					}
				}
				ttTrdOrder.setAiamount(MathUtil.round(totalAiAmount,2));
			}
			ttTrdOrder.setFst_settle_amount(ttTrdOrder.getAiamount());
			//其他信息
			ttTrdOrder.setFst_settle_date(MathUtil.compare(ttTrdOrder.getAiamount(), 0, 2)>0?businessDate:ttTrdOrder.getOrder_date());
			//发起人取操作人，即session
			ttTrdOrder.setSelf_traderid(ttTrdOrder.getOperator_id());
			ttTrdOrder.setParty_id(ParameterUtil.getString(map, "party_id", ""));
			ttTrdOrder.setParty_type(DictConstants.PartType.Outider);
		}
		else if(DictConstants.TrdType.CustomRateChange.equals(ttTrdOrder.getTrdtype())){
			ttTrdOrder.setOrder_date(ParameterUtil.getString(map, "effectDate", businessDate));
			//金额信息
			ttTrdOrder.setTotalcount(ttTrdOrder.getTotalamount());
			ttTrdOrder.setPreorder_amount(ttTrdOrder.getTotalamount());
			ttTrdOrder.setUsed_amount(0);
			ttTrdOrder.setRemain_amount(ttTrdOrder.getTotalamount());
			ttTrdOrder.setFst_settle_amount(ttTrdOrder.getTotalamount());
			//其他信息
			ttTrdOrder.setFst_settle_date(ttTrdOrder.getOrder_date());
			//发起人取操作人，即session
			ttTrdOrder.setSelf_traderid(ttTrdOrder.getOperator_id());
			ttTrdOrder.setParty_id(ParameterUtil.getString(map, "party_id", ""));
			ttTrdOrder.setParty_type(DictConstants.PartType.Outider);
		}
		else if(DictConstants.TrdType.CustomTradeExtend.equals(ttTrdOrder.getTrdtype())){
			ttTrdOrder.setOrder_date(ParameterUtil.getString(map, "expDate", businessDate));
			ttTrdOrder.setTerm_days(ParameterUtil.getDouble(map, "term", 0));
			//金额信息
			ttTrdOrder.setTotalamount(ParameterUtil.getDouble(map, "settleAmt", 0));
			ttTrdOrder.setPreorder_amount(ParameterUtil.getDouble(map, "settleAmt", 0));
			ttTrdOrder.setUsed_amount(0);
			ttTrdOrder.setRemain_amount(ParameterUtil.getDouble(map, "surplusAmt", 0));
			ttTrdOrder.setFst_settle_amount(ParameterUtil.getDouble(map, "settleAmt", 0));
			ttTrdOrder.setYtm(ParameterUtil.getDouble(map, "expRate", 0));
			//其他信息
			ttTrdOrder.setFst_settle_date(ttTrdOrder.getOrder_date());
			//发起人取操作人，即session
			ttTrdOrder.setSelf_traderid(ttTrdOrder.getOperator_id());
			ttTrdOrder.setParty_id(ParameterUtil.getString(map, "party_id", ""));
			ttTrdOrder.setParty_type(DictConstants.PartType.Outider);
		}
		else if(DictConstants.TrdType.CustomMultiResale.equals(ttTrdOrder.getTrdtype())){
			ttTrdOrder.setOrder_date(ParameterUtil.getString(map, "resDate", businessDate));
			//金额信息
			ttTrdOrder.setTotalamount(ParameterUtil.getDouble(map, "resAmt", 0));
			ttTrdOrder.setTotalcount(ttTrdOrder.getTotalamount());
			ttTrdOrder.setNetamount(ttTrdOrder.getTotalamount()+ParameterUtil.getDouble(map, "transferIncome", 0));
			ttTrdOrder.setAiamount(ParameterUtil.getDouble(map, "resInterest", 0));
			ttTrdOrder.setFullamount(MathUtil.round(ttTrdOrder.getNetamount() + ttTrdOrder.getAiamount(),2));
			ttTrdOrder.setFst_settle_amount(ttTrdOrder.getFullamount());
			
			ttTrdOrder.setPreorder_amount(ttTrdOrder.getTotalamount());
			ttTrdOrder.setUsed_amount(0);
			ttTrdOrder.setRemain_amount(ttTrdOrder.getTotalamount());
			
			//其他信息
			ttTrdOrder.setFst_settle_date(ttTrdOrder.getOrder_date());
			//发起人取操作人，即session
			ttTrdOrder.setSelf_traderid(ttTrdOrder.getOperator_id());
			ttTrdOrder.setParty_id(ParameterUtil.getString(map, "party_id", ""));
			ttTrdOrder.setParty_type(DictConstants.PartType.Outider);
		}
		else if(DictConstants.TrdType.ApproveCancel.equals(ttTrdOrder.getTrdtype())){
			ttTrdOrder.setOrder_date(ParameterUtil.getString(map, "cancelDate", businessDate));
			//金额信息
			ttTrdOrder.setTotalamount(ParameterUtil.getDouble(map, "originalAmt", 0.0));
			ttTrdOrder.setTotalcount(ttTrdOrder.getTotalamount());
			ttTrdOrder.setPreorder_amount(ttTrdOrder.getTotalamount());
			ttTrdOrder.setUsed_amount(0);
			ttTrdOrder.setRemain_amount(ttTrdOrder.getTotalamount());
			ttTrdOrder.setFst_settle_amount(ttTrdOrder.getTotalamount());
			//其他信息
			ttTrdOrder.setFst_settle_date(ttTrdOrder.getOrder_date());
			//发起人取操作人，即session
			ttTrdOrder.setSelf_traderid(ttTrdOrder.getOperator_id());
			ttTrdOrder.setParty_id(ParameterUtil.getString(map, "party_id", ""));
			ttTrdOrder.setParty_type(DictConstants.PartType.Outider);
		}

		else if(DictConstants.TrdType.AccountTradeOffset.equals(ttTrdOrder.getTrdtype())){
			ttTrdOrder.setOrder_date(ParameterUtil.getString(map, "offsetDate", businessDate));
			//金额信息
			ttTrdOrder.setTotalamount(ParameterUtil.getDouble(map, "originalAmt", 0.0));
			ttTrdOrder.setTotalcount(ttTrdOrder.getTotalamount());
			ttTrdOrder.setPreorder_amount(ttTrdOrder.getTotalamount());
			ttTrdOrder.setUsed_amount(0);
			ttTrdOrder.setRemain_amount(ttTrdOrder.getTotalamount());
			//其他信息
			ttTrdOrder.setFst_settle_date(ttTrdOrder.getOrder_date());
			//发起人取操作人，即session
			ttTrdOrder.setSelf_traderid(ttTrdOrder.getOperator_id());
			ttTrdOrder.setParty_id(ParameterUtil.getString(map, "party_id", ""));
			ttTrdOrder.setParty_type(DictConstants.PartType.Outider);
			//获取原业务核实交易单信息
			JY.require(!StringUtil.isNullOrEmpty(ttTrdOrder.getI_code()), "原业务核实交易单号为空！请确认");
			TtTrdTrade oldTrade = tradeInterfaceService.selectTrdTrade(ttTrdOrder.getI_code(),0);
			JY.require(oldTrade!=null, "找不到原业务核实交易单，原业务核实交易单号:" + ttTrdOrder.getI_code());
			setOldOrderInfo(ttTrdOrder,oldTrade);
			//原交易本金
			JY.require(MathUtil.isEqual(oldTrade.getTotalamount(), ttTrdOrder.getTotalamount(), 2, BigDecimal.ROUND_HALF_UP), "冲销金额与原交易本金不符，请确认!");
			//原交易利息金额
			ttTrdOrder.setAiamount(oldTrade.getAiamount());
			ttTrdOrder.setFst_settle_amount(oldTrade.getFst_settle_amount());
		}
		else if(DictConstants.TrdType.CustomLiabilitiesDuration.equals(ttTrdOrder.getTrdtype())){
			ttTrdOrder.setOrder_date(ParameterUtil.getString(map, "ldDate", businessDate));
			//金额信息
			ttTrdOrder.setTotalamount(ParameterUtil.getDouble(map, "ldAmt", 0.0));
			ttTrdOrder.setYtm(ParameterUtil.getDouble(map, "ldRate", 0.0));
			ttTrdOrder.setTotalcount(ttTrdOrder.getTotalamount());
			ttTrdOrder.setPreorder_amount(ttTrdOrder.getTotalamount());
			ttTrdOrder.setUsed_amount(0);
			ttTrdOrder.setRemain_amount(ttTrdOrder.getTotalamount());
			ttTrdOrder.setFst_settle_amount(MathUtil.round(ttTrdOrder.getTotalamount() + ttTrdOrder.getAiamount(),2));
			//其他信息
			ttTrdOrder.setFst_settle_date(ttTrdOrder.getOrder_date());
			ttTrdOrder.setSelf_traderid(ParameterUtil.getString(map, "sponsor", ttTrdOrder.getOperator_id()));
			ttTrdOrder.setParty_type(DictConstants.PartType.Outider);
		}
	}
	/**
	 * 设置原交易信息到新业务审批单：提前支取、交易冲销使用
	 * @param ttTrdOrder
	 */
	private void setOldOrderInfo(TtTrdOrder ttTrdOrder,TtTrdTrade oldTrade){
		ttTrdOrder.setParty_id(oldTrade.getParty_id());
		ttTrdOrder.setI_name(oldTrade.getI_name());
		ttTrdOrder.setSelf_bankacccode(oldTrade.getSelf_bankacccode());
		ttTrdOrder.setSelf_bankaccname(oldTrade.getSelf_bankaccname());
		ttTrdOrder.setSelf_bankcode(oldTrade.getSelf_bankcode());
		ttTrdOrder.setSelf_bankname(oldTrade.getSelf_bankname());
		ttTrdOrder.setSelf_incashaccid(oldTrade.getSelf_incashaccid());
		ttTrdOrder.setSelf_outcashaccid(oldTrade.getSelf_outcashaccid());
		ttTrdOrder.setSelf_insecuaccid(oldTrade.getSelf_insecuaccid());
		ttTrdOrder.setSelf_outsecuaccid(oldTrade.getSelf_outsecuaccid());
		ttTrdOrder.setParty_bankacccode(oldTrade.getParty_bankacccode());
		ttTrdOrder.setParty_bankaccname(oldTrade.getParty_bankaccname());
		ttTrdOrder.setParty_bankcode(oldTrade.getParty_bankcode());
		ttTrdOrder.setParty_bankname(oldTrade.getParty_bankname());
	}
	@Override
	public void beforeSave(HashMap<String, Object> map) {
		//初始化固定参数，后续增加后从页面传入
		map.put("self_insecuaccid", "401");
		//转换交易对手
		String partyId = ParameterUtil.getString(map, "cNo", "");
		//map.put("party_id", StringUtil.isNotEmpty(partyId)?partyId:ParameterUtil.getString(map, "cNo", ""));
		map.put("party_id", partyId);
		//转换order_id
		String orderId = ParameterUtil.getString(map, "dealNo", "");
		map.put("order_id", orderId);
	}

	@Override
	public void saveApprove(HashMap<String, Object> map, TtTrdOrder ttTrdOrder, boolean isAdd) {
		//设置自定义业务审批单信息
		setCustomOrderInfo(map,ttTrdOrder);
		//将业务单号放进map
		map.put("dealNo", ttTrdOrder.getOrder_id());
		map.put("iCode", ttTrdOrder.getI_code());
		map.put("aType", ttTrdOrder.getA_type());
		map.put("mType", ttTrdOrder.getM_type());

		//更新老版本数据状态
		//mainMapper.updateMainStatus(main);
		
		//新增最新版本的数据
		//int oldMaxVersion = mainMapper.selectMaxVersion(main);
		//main.setVersion(oldMaxVersion+1);
		map.put("dealType", DictConstants.DealType.Approve);
		if(isAdd){
			if (DictConstants.TrdType.Custom.equals(ttTrdOrder.getTrdtype())) {
				productApproveService.createProductApprove(map);
			} else if (DictConstants.TrdType.CustomAdvanceMaturity.equals(ttTrdOrder.getTrdtype())) {
				advanceMaturityService.createAdvanceMaturity(map);
			} else if (DictConstants.TrdType.CustomCashFlowChange.equals(ttTrdOrder.getTrdtype())){
				cashFlowChangeService.createCashFlowChange(map);
			} else if (DictConstants.TrdType.CustomRateChange.equals(ttTrdOrder.getTrdtype())){
				rateChangeService.createRateChange(map);
			} else if (DictConstants.TrdType.CustomInterestSettleConfirm.equals(ttTrdOrder.getTrdtype())){
				interestSettleService.createInterestSettle(map);
			}
			else if (DictConstants.TrdType.CustomTradeExtend.equals(ttTrdOrder.getTrdtype())){
				tradeExtendService.createTradeExtend(map);
			}
			else if (DictConstants.TrdType.CustomMultiResale.equals(ttTrdOrder.getTrdtype())){
				mulResaleService.createMulResale(map);
			}
			else if (DictConstants.TrdType.ApproveCancel.equals(ttTrdOrder.getTrdtype())){
				approveCancelService.createApproveCancel(map);
			}
			else if (DictConstants.TrdType.AccountTradeOffset.equals(ttTrdOrder.getTrdtype())){
				tradeOffsetService.createTradeOffset(map);
			}
			else if (DictConstants.TrdType.CustomLiabilitiesDuration.equals(ttTrdOrder.getTrdtype())){
				liabilitiesDurationService.createLiabilitiesDuration(map);
			}
			else if (DictConstants.TrdType.LiabilitiesDepositRegular.equals(ttTrdOrder.getTrdtype())){
				productApproveService.createLiabilitiesProductApprove(map);
			}
			else if (DictConstants.TrdType.AbssetForEnterpriceCredit.equals(ttTrdOrder.getTrdtype())){
				productApproveService.createCrmsProductApprove(map);
			}
		} else{
			TtTrdOrder tempTrdOrder = trdOrderMapper.selectOrderForOrderId(BeanUtil.beanToHashMap(ttTrdOrder));
			if(null != tempTrdOrder && !StringUtils.equalsIgnoreCase(tempTrdOrder.getOrder_status(), DictConstants.ApproveStatus.New)){
				JY.raise("当前业务非新建状态，不允许修改!");
			}
			if (DictConstants.TrdType.Custom.equals(ttTrdOrder.getTrdtype())) {
				productApproveService.updateProductApprove(map);
			} else if (DictConstants.TrdType.CustomAdvanceMaturity.equals(ttTrdOrder.getTrdtype())) {
				advanceMaturityService.updateAdvanceMaturity(map);
			} else if (DictConstants.TrdType.CustomCashFlowChange.equals(ttTrdOrder.getTrdtype())){
				cashFlowChangeService.updateCashFlowChange(map);
			} else if (DictConstants.TrdType.CustomRateChange.equals(ttTrdOrder.getTrdtype())){
				rateChangeService.updateRateChange(map);
			}else if (DictConstants.TrdType.CustomInterestSettleConfirm.equals(ttTrdOrder.getTrdtype())){
				interestSettleService.updateInterestSettle(map);
			}
			else if (DictConstants.TrdType.CustomTradeExtend.equals(ttTrdOrder.getTrdtype())){
				tradeExtendService.updateTradeExtend(map);
			}
			else if (DictConstants.TrdType.CustomMultiResale.equals(ttTrdOrder.getTrdtype())){
				mulResaleService.updateMulResale(map);
			}
			else if (DictConstants.TrdType.ApproveCancel.equals(ttTrdOrder.getTrdtype())){
				approveCancelService.updateApproveCancel(map);
			}
			else if (DictConstants.TrdType.AccountTradeOffset.equals(ttTrdOrder.getTrdtype())){
				tradeOffsetService.updateTradeOffset(map);
			}
			else if (DictConstants.TrdType.CustomLiabilitiesDuration.equals(ttTrdOrder.getTrdtype())){
				liabilitiesDurationService.updateLiabilitiesDuration(map);
			}
			else if (DictConstants.TrdType.LiabilitiesDepositRegular.equals(ttTrdOrder.getTrdtype())){
				productApproveService.updateLiabilitiesProductApprove(map);
			}
			else if (DictConstants.TrdType.AbssetForEnterpriceCredit.equals(ttTrdOrder.getTrdtype())){
				productApproveService.updateCrmsProductApprove(map);
			}
		}
			
		//更新现金流
//		Map<String,Object> cmap=new HashMap<String,Object>();
//		cmap.put("iCode", ttTrdOrder.getI_code());
//		cmap.put("aType", ttTrdOrder.getA_type());
//		cmap.put("mType", ttTrdOrder.getM_type());
//		ttCashflowInterestMapper.deleteCfInterst(cmap);
//		if(intList!=null && intList.size()>0){
//			batchDao.batch("com.singlee.capital.cashflow.mapper.TtCashflowInterestMapper.insert", intList);
//		}
//		ttCashflowCapitalMapper.deleteCfCapital(cmap);
//		if(capList!=null && capList.size()>0){
//			batchDao.batch("com.singlee.capital.cashflow.mapper.TtCashflowCapitalMapper.insert", capList);
//		}
		
		
	}
	
	
	@Override
	public void createTrade(HashMap<String, Object> map, TtTrdTrade ttTrdTrade) {
		// TODO Auto-generated method stub

	}

	@Override
	public void createdTradeHandle(TtTrdTrade trd, String folder) {
		// TODO Auto-generated method stub

	}

	@Override
	public void createTrust(HashMap<String, Object> map, TtTrdTrust ttTrdTrust) {
		// TODO Auto-generated method stub

	}

	@Override
	public void cancelApprove(TtTrdOrder ttTrdOrder) {
		// TODO Auto-generated method stub

	}

	@Override
	public void noPassApprove(TtTrdOrder ttTrdOrder) {
		// TODO Auto-generated method stub

	}
	/**
	 * 获得正在使用生效的交易数据，包含参与方等一些列子表
	 */
	@Override
	public boolean need2Trade(TtTrdOrder ttTrdOrder) {
		if(DictConstants.TrdType.Custom.equals(ttTrdOrder.getTrdtype())){
			TdProductApproveMain main = productApproveService.getProductApproveActivated(ttTrdOrder.getOrder_id());
			String prdProp = main.getProduct().getPrdProp();
			String prdTerm = main.getProduct().getPrdTerm();
			JY.require(StringUtils.isNotEmpty(prdProp), "产品 %s 的产品性质没有取到，请确认！", main.getProduct().getPrdName());
			
			if(DictConstants.prdProp.assets.equals(prdProp)) { //资产
				if (DictConstants.prdTerm.current.equals(prdTerm)) {
					return false;
				} else {
					return true;
				}
			}else if(DictConstants.prdProp.liablities.equals(prdProp)){ //负债
				return false;
			}else if(DictConstants.prdProp.quota.equals(prdProp)){
				return false;
			}else if(DictConstants.prdProp.intermediateIncome.equals(prdProp)){ //中间业务收入
				return false;
			}
		}
		else if(DictConstants.TrdType.CustomLiabilitiesDuration.equals(ttTrdOrder.getTrdtype())){
			return false;
		}
		else{
			return true;
		}
		return false;
	}

	@Override
	public void notNeedTradeProcess(TtTrdOrder ttTrdOrder) {
		//负债产品
		if(DictConstants.TrdType.CustomLiabilitiesDuration.equals(ttTrdOrder.getTrdtype())){
			TdLiabilitiesDuration liab = liabilitiesDurationService.getLiabilitiesDurationById(ttTrdOrder.getOrder_id());
			JY.require(liab!=null, "单号 %s 负债存续期不存在!",ttTrdOrder.getOrder_id());
			TdProductApproveMain main = productApproveService.getProductApproveActivated(liab.getRefNo());
			String prdProp = main.getProduct().getPrdProp();
			JY.require(StringUtils.isNotEmpty(prdProp), "产品 %s 的产品性质没有取到，请确认！", main.getProduct().getPrdName());
			if(DictConstants.prdProp.liablities.equals(prdProp)){ //负债
				main.setAmt(liab.getLdAmt());
				main.setContractRate(liab.getLdRate());
				main.setmDate(liab.getLdDate());
				main.setActuralRate(liab.getLdRate());
				//到期利息
				double mInterest = productApproveService.computeMInterest(main);
				main.setmInt(mInterest);
				//期限
				int term;
				try {
					term = productApproveService.computeTimeLimit(main);
				} catch (ParseException e) {
                   term =0;
				}
				main.setTerm(term);
				//到期金额
				main.setmAmt(MathUtil.round(main.getAmt()+main.getmInt(),2));
				productApproveService.updateProductApproveMainVersion(main);
			}
		}
		
	}

}
