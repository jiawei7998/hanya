package com.singlee.derivative.mapper;


import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.capital.common.spring.mybatis.MyMapper;
import com.singlee.derivative.model.JSYQMA;
import com.singlee.derivative.model.FDSETR;

public interface SwapTaskMapper extends MyMapper<FDSETR>{
	/**
	 * 
	 * @param FDSETR
	 */
	 void insertintoma(FDSETR FDSETR);
	 void updatetr(FDSETR FDSETR);
	 void insertintoMa(Map<String, Object> map);
	 void updatetr(Map<String, Object> map);
	 void updatema(Map<String, Object> map);
	 
	 public String selectfromtrAD(FDSETR FDSETR);
	/**
	 * 分页查询产品事件
	 * @param map
	 * @return
	 */
	public Page<FDSETR> selectfromtr(Map<String, Object> map);
	/**
	 * 分页查询产品事件
	 * @param map
	 * @return
	 */
	public Page<FDSETR> selectfromtrTS(Map<String, Object> map);
	/**
	 * 分页查询产品事件
	 * @param map
	 * @return
	 */
	public Page<FDSETR> selectfromtrWM(Map<String, Object> map);
	/**
	 * 分页查询产品事件
	 * @param map
	 * @return
	 */
	public Page<FDSETR> selectfromtrWV(Map<String, Object> map);
	/**
	 * 分页查询产品事件
	 * @param map
	 * @return
	 */
	public Page<FDSETR> selectfromtrEF(Map<String, Object> map);
	public Page<FDSETR> selectfrommaYQ(Map<String, Object> map);
	public Page<FDSETR> selectfrommaDL(Map<String, Object> map);
	public Page<FDSETR> selectfrommaYD(Map<String, Object> map);
	public int deletetr(String  jsyqNo);
}