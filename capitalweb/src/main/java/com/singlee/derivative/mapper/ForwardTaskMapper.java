package com.singlee.derivative.mapper;


import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.capital.common.spring.mybatis.MyMapper;
import com.singlee.derivative.model.JSYQMA;
import com.singlee.derivative.model.LTYQAD;

public interface ForwardTaskMapper extends MyMapper<LTYQAD>{
	/**
	 * 
	 * @param ltyqad
	 */
	 void insertintoma(LTYQAD ltyqad);
	 void updatetr(LTYQAD ltyqad);
	 void insertintoMa(Map<String, Object> map);
	 void updatetr(Map<String, Object> map);
	 void updatema(Map<String, Object> map);
	 
	 public String selectfromtrAD(LTYQAD ltyqad);
	/**
	 * 分页查询产品事件
	 * @param map
	 * @return
	 */
	public Page<LTYQAD> selectfromtr(Map<String, Object> map);
	/**
	 * 分页查询产品事件
	 * @param map
	 * @return
	 */
	public Page<LTYQAD> selectfromtrTS(Map<String, Object> map);
	/**
	 * 分页查询产品事件
	 * @param map
	 * @return
	 */
	public Page<LTYQAD> selectfromtrWM(Map<String, Object> map);
	/**
	 * 分页查询产品事件
	 * @param map
	 * @return
	 */
	public Page<LTYQAD> selectfromtrWV(Map<String, Object> map);
	/**
	 * 分页查询产品事件
	 * @param map
	 * @return
	 */
	public Page<LTYQAD> selectfromtrEF(Map<String, Object> map);
	public Page<LTYQAD> selectfrommaYQ(Map<String, Object> map);
	public Page<LTYQAD> selectfrommaDL(Map<String, Object> map);
	public int deletetr(String  jsyqNo);
}