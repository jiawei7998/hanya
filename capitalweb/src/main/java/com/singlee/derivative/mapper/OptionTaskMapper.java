package com.singlee.derivative.mapper;


import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.capital.common.spring.mybatis.MyMapper;
import com.singlee.derivative.model.JSYQMA;
import com.singlee.derivative.model.COPTTR;

public interface OptionTaskMapper extends MyMapper<COPTTR>{
	/**
	 * 
	 * @param COPTTR
	 */
	 void insertintoma(COPTTR COPTTR);
	 void updatetr(COPTTR COPTTR);
	 void insertintoMa(Map<String, Object> map);
	 void updatetr(Map<String, Object> map);
	 void updatema(Map<String, Object> map);
	 
	 public String selectfromtrAD(COPTTR COPTTR);
	/**
	 * 分页查询产品事件
	 * @param map
	 * @return
	 */
	public Page<COPTTR> selectfromtr(Map<String, Object> map);
	/**
	 * 分页查询产品事件
	 * @param map
	 * @return
	 */
	public Page<COPTTR> selectfromtrTS(Map<String, Object> map);
	/**
	 * 分页查询产品事件
	 * @param map
	 * @return
	 */
	public Page<COPTTR> selectfromtrWM(Map<String, Object> map);
	/**
	 * 分页查询产品事件
	 * @param map
	 * @return
	 */
	public Page<COPTTR> selectfromtrWV(Map<String, Object> map);
	/**
	 * 分页查询产品事件
	 * @param map
	 * @return
	 */
	public Page<COPTTR> selectfromtrEF(Map<String, Object> map);
	public Page<COPTTR> selectfrommaYQ(Map<String, Object> map);
	public Page<COPTTR> selectfrommaDL(Map<String, Object> map);
	public Page<COPTTR> selectfrommaYD(Map<String, Object> map);
	public int deletetr(String  jsyqNo);
}