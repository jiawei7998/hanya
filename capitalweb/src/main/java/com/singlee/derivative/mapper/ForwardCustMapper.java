package com.singlee.derivative.mapper;


import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.common.spring.mybatis.MyMapper;
import com.singlee.derivative.model.CUACTR;
import com.singlee.derivative.model.DUEL;
import com.singlee.derivative.model.LTYQAD;

public interface ForwardCustMapper extends MyMapper<LTYQAD>{
	

	/**
	 * 分页查询产品事件
	 * @param map
	 * @return
	 */
	public Page<CUACTR> searchCustomerAccount(Map<String, Object> map);
	
	/**
	 * 获取递增Id
	 * 
	 * @return
	 */
	public String getSequenceId();
	
}