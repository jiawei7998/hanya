package com.singlee.derivative.mapper;


import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.common.spring.mybatis.MyMapper;
import com.singlee.derivative.model.CUACTR;
import com.singlee.derivative.model.DUEL;
import com.singlee.derivative.model.FDQVMA;
import com.singlee.derivative.model.LTYQAD;

public interface ForwardInqunoMapper extends MyMapper<LTYQAD>{
	

	
	
	/**
	 * 获取递增Id
	 * 
	 * @return
	 */
	public String getSequenceId();
	

	/**
	 * 分页查询产品事件
	 * @param map
	 * @return
	 */
	public Page<FDQVMA> searchFdqvno(Map<String, Object> map);
	
	
}