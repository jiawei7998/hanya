package com.singlee.derivative.mapper;


import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.github.pagehelper.Page;
import com.singlee.capital.common.spring.mybatis.MyMapper;
import com.singlee.derivative.model.CUACTR;
import com.singlee.derivative.model.DUEL;
import com.singlee.derivative.model.FDQVMA;
import com.singlee.derivative.model.FDQVTR;

public interface ForwardInquMapper extends MyMapper<FDQVTR>{
	
	/**
	 * 
	 * @param FDQVTR
	 */
	 void insertintotrQV(FDQVTR FDQVTR);
	 void UpdatetrQV(FDQVTR FDQVTR);
	 void UpdatetrQVS(FDQVTR FDQVTR);
	 void insertintoMaQV(Map<String, Object> map);
	 void updatetrQV(Map<String, Object> map);
	 void updatemaQV(Map<String, Object> map);
	 
	 public String selectfromtrQV(FDQVTR FDQVTR);
	 public Page<FDQVMA> selectfrommaQV(Map<String, Object> map);

	/**
	 * 分页查询产品事件
	 * @param map
	 * @return
	 */
	public Page<FDQVTR> selectfromtrQV(Map<String, Object> map);
	/**
	 * 分页查询产品事件
	 * @param map
	 * @return
	 */
	public Page<FDQVTR> selectfromtrTS(Map<String, Object> map);
	/**
	 * 分页查询产品事件
	 * @param map
	 * @return
	 */
	public Page<FDQVTR> selectfromtrWM(Map<String, Object> map);
	/**
	 * 分页查询产品事件
	 * @param map
	 * @return
	 */
	public Page<FDQVTR> selectfromtrWV(Map<String, Object> map);
	/**
	 * 分页查询产品事件
	 * @param map
	 * @return
	 */
	public Page<FDQVTR> selectfromtrEF(Map<String, Object> map);
	public int deletetrQV(String  FDQVNO);
	
	
}