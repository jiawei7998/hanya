package com.singlee.derivative.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.derivative.model.FDQVMA;
import com.singlee.derivative.model.FDQVTR;
import com.singlee.derivative.model.LTYQAD;
import com.singlee.derivative.service.InquiryService;



@Controller
@RequestMapping(value = "/InquiryController")
public class InquiryController  extends CommonController{
	@Autowired
	private InquiryService inquiryservice;
	
	@ResponseBody
	@RequestMapping(value = "/save")
	public RetMsg<Serializable> Savedata(HttpServletRequest request,@RequestBody FDQVTR param) throws IOException {
		//获取询价类型：远期 、掉期、期权、利率互换
		
		String fdqvno=inquiryservice.InsertintotrQV(param);
		return  RetMsgHelper.ok(fdqvno);
	}
	
	@ResponseBody
	@RequestMapping(value = "/query")
    public RetMsg<PageInfo<FDQVTR>> showAllTbkProductEventPaged(@RequestBody Map<String,Object> map) {
		Page<FDQVTR> page = inquiryservice.SelectfromtrQV(map);
		return RetMsgHelper.ok(page);
		
	}
	
	@ResponseBody
	@RequestMapping(value = "/searchPageTempStorage")
	public RetMsg<PageInfo<FDQVTR>> searchPageTempStorage(@RequestBody Map<String,Object> map) {
	    Page<FDQVTR> page = inquiryservice.SelectfromtrTS(map);
		return RetMsgHelper.ok(page);
			
	}
	
	@ResponseBody
	@RequestMapping(value = "/searchPageOperate")
	public RetMsg<PageInfo<FDQVMA>> searchPageOperate(@RequestBody Map<String,Object> map) {
	    Page<FDQVMA> page = inquiryservice.SelectfrommaQV(map);
		return RetMsgHelper.ok(page);
			
	}
	@ResponseBody
	@RequestMapping(value = "/searchPageWaitModify")
    public RetMsg<PageInfo<FDQVTR>> searchPageWaitModify(@RequestBody Map<String,Object> map) {
		Page<FDQVTR> page = inquiryservice.SelectfromtrWM(map);
		return RetMsgHelper.ok(page);
	}		
	@ResponseBody
	@RequestMapping(value = "/searchPageWaitVerify")
	public RetMsg<PageInfo<FDQVTR>> searchPageWaitVverify(@RequestBody Map<String,Object> map) {
		Page<FDQVTR> page = inquiryservice.SelectfromtrWV(map);
		
		return RetMsgHelper.ok(page);

}
	@ResponseBody
	@RequestMapping(value = "/searchPageEfficient")
	public RetMsg<PageInfo<FDQVTR>>searchPageEfficient(@RequestBody Map<String,Object> map) {
		Page<FDQVTR> page = inquiryservice.SelectfromtrEF(map);
		return RetMsgHelper.ok(page);

}
	@ResponseBody
	@RequestMapping(value = "/commit")
	public RetMsg<Serializable> updatetr(@RequestBody FDQVTR map) throws IOException  {

		inquiryservice.UpdatetrQV(map);
		return RetMsgHelper.ok();
	}
	
	
	@ResponseBody
	@RequestMapping(value = "/checkTradeTr")
	public RetMsg<Serializable>  updatetrCheck(@RequestBody Map<String,Object> map) throws IOException  {

		inquiryservice.UpdatetrQV(map);
		inquiryservice.InsertintoMaQV(map);
		
		
		return RetMsgHelper.ok();
	}
	

	@ResponseBody
	@RequestMapping(value = "/refuse")
	public RetMsg<Serializable> refusetr(@RequestBody FDQVTR map) throws IOException  {

		inquiryservice.UpdatetrQV(map);
		return RetMsgHelper.ok();
	}
	@ResponseBody
	@RequestMapping(value = "/delete")
	public RetMsg<Serializable> deletetr(@RequestBody String[] fDQVNO) throws IOException  {

		inquiryservice.DeletetrQV(fDQVNO);
		return RetMsgHelper.ok();
	}
	
}


