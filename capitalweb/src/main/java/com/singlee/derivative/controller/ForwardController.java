package com.singlee.derivative.controller;

import java.io.IOException;
import java.io.Serializable;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.derivative.model.CUACTR;
import com.singlee.derivative.model.FDQVMA;
import com.singlee.derivative.model.JSYQMA;
import com.singlee.derivative.model.LTYQAD;
import com.singlee.derivative.service.FwdService;
import com.github.pagehelper.Page;
import com.github.pagehelper.StringUtil;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.counterparty.model.CounterPartyVo;



@Controller
@RequestMapping(value = "/ForwardController")
public class ForwardController  extends CommonController{
	@Autowired
	private FwdService fwdservice;
	
	@ResponseBody
	@RequestMapping(value = "/save")
	public RetMsg<Serializable> Savedata(HttpServletRequest request,@RequestBody LTYQAD param) throws IOException {
		//Format format=new SimpleDateFormat("yyyyMMddHHmmss");
		//param.setJsyqNo("JSYQ"+format.format(new Date()));
		String jsyqno=fwdservice.Insertintoma(param);
		return  RetMsgHelper.ok(jsyqno);
	}
	
	@ResponseBody
	@RequestMapping(value = "/query")
    public RetMsg<PageInfo<LTYQAD>> showAllTbkProductEventPaged(@RequestBody Map<String,Object> map) {
		Page<LTYQAD> page = fwdservice.Selectfromtr(map);
		return RetMsgHelper.ok(page);
		
	}

	@ResponseBody
	@RequestMapping(value = "/searchPageTempStorage")
	public RetMsg<PageInfo<LTYQAD>> searchPageTempStorage(@RequestBody Map<String,Object> map) {
	    Page<LTYQAD> page = fwdservice.SelectfromtrTS(map);
		return RetMsgHelper.ok(page);
			
	}
	@ResponseBody
	@RequestMapping(value = "/searchPageWaitModify")
    public RetMsg<PageInfo<LTYQAD>> searchPageWaitModify(@RequestBody Map<String,Object> map) {
		Page<LTYQAD> page = fwdservice.SelectfromtrWM(map);
		return RetMsgHelper.ok(page);
	}		
	@ResponseBody
	@RequestMapping(value = "/searchPageWaitVerify")
	public RetMsg<PageInfo<LTYQAD>> searchPageWaitVverify(@RequestBody Map<String,Object> map) {
		Page<LTYQAD> page = fwdservice.SelectfromtrWV(map);
		return RetMsgHelper.ok(page);

}
	@ResponseBody
	@RequestMapping(value = "/searchPageEfficient")
	public RetMsg<PageInfo<LTYQAD>>searchPageEfficient(@RequestBody Map<String,Object> map) {
		Page<LTYQAD> page = fwdservice.SelectfromtrEF(map);
		return RetMsgHelper.ok(page);

}
	@ResponseBody
	@RequestMapping(value = "/searchCustomerAccount")
	public RetMsg<PageInfo<CUACTR>> searchCustomerAccount(@RequestBody Map<String,Object> map) throws IOException  {

		Page<CUACTR>  page = fwdservice.searchCustomerAccount(map);
		return RetMsgHelper.ok(page);
	}
	
	@ResponseBody
	@RequestMapping(value = "/searchPageOperate")
	public RetMsg<PageInfo<LTYQAD>> searchPageOperate(@RequestBody Map<String,Object> map) {
	    Page<LTYQAD> page = fwdservice.SelectfrommaYQ(map);
		return RetMsgHelper.ok(page);
			
	}
	
	@ResponseBody
	@RequestMapping(value = "/commit")
	public RetMsg<Serializable> updatetr(@RequestBody LTYQAD map) throws IOException  {

		fwdservice.Updatetr(map);
		return RetMsgHelper.ok();
	}
	
	
	@ResponseBody
	@RequestMapping(value = "/checkTradeTr")
	public RetMsg<Serializable>  updatetrCheck(@RequestBody Map<String,Object> map) throws IOException  {

		fwdservice.Updatetr(map);
		if("A".equals(map.get("newStatCd"))){
		fwdservice.InsertintoMa(map);
		}
		if("Z".equals(map.get("newStatCd"))){
		fwdservice.Updatema(map);
		}
		
		
		return RetMsgHelper.ok();
	}
	

	@ResponseBody
	@RequestMapping(value = "/refuse")
	public RetMsg<Serializable> refusetr(@RequestBody LTYQAD map) throws IOException  {

		fwdservice.Updatetr(map);
		return RetMsgHelper.ok();
	}
	
	@ResponseBody
	@RequestMapping(value = "/searchfdqvno")
	public RetMsg<PageInfo<FDQVMA>> searchFdqvno( @RequestBody Map<String,Object> map) throws IOException  {
 
		Page<FDQVMA>  page=fwdservice.searchFdqvno(map);
		return RetMsgHelper.ok(page);
	}
	
	@ResponseBody
	@RequestMapping(value = "/delete")
	public RetMsg<Serializable> deletetr(@RequestBody String[] jsyqNo) throws IOException  {

		fwdservice.Deletetr(jsyqNo);
		return RetMsgHelper.ok();
	}
	
}


