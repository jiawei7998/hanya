package com.singlee.derivative.controller;

import java.io.IOException;
import java.io.Serializable;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.derivative.model.CUACTR;
import com.singlee.derivative.model.FDQVMA;
import com.singlee.derivative.model.JSYQMA;
import com.singlee.derivative.model.COPTTR;
import com.singlee.derivative.service.OptionService;

import com.github.pagehelper.Page;
import com.github.pagehelper.StringUtil;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.counterparty.model.CounterPartyVo;



@Controller
@RequestMapping(value = "/OptionController")
public class OptionController  extends CommonController{
	@Autowired
	private OptionService optionservice;
	
	@ResponseBody
	@RequestMapping(value = "/save")
	public RetMsg<Serializable> Savedata(HttpServletRequest request,@RequestBody COPTTR param) throws IOException {
		//Format format=new SimpleDateFormat("yyyyMMddHHmmss");
		//param.setJsyqNo("JSYQ"+format.format(new Date()));
		String jsyqno=optionservice.Insertintoma(param);
		return  RetMsgHelper.ok(jsyqno);
	}
	
	@ResponseBody
	@RequestMapping(value = "/query")
    public RetMsg<PageInfo<COPTTR>> showAllTbkProductEventPaged(@RequestBody Map<String,Object> map) {
		Page<COPTTR> page = optionservice.Selectfromtr(map);
		return RetMsgHelper.ok(page);
		
	}

	@ResponseBody
	@RequestMapping(value = "/searchPageTempStorage")
	public RetMsg<PageInfo<COPTTR>> searchPageTempStorage(@RequestBody Map<String,Object> map) {
	    Page<COPTTR> page = optionservice.SelectfromtrTS(map);
		return RetMsgHelper.ok(page);
			
	}
	@ResponseBody
	@RequestMapping(value = "/searchPageWaitModify")
    public RetMsg<PageInfo<COPTTR>> searchPageWaitModify(@RequestBody Map<String,Object> map) {
		Page<COPTTR> page = optionservice.SelectfromtrWM(map);
		return RetMsgHelper.ok(page);
	}		
	@ResponseBody
	@RequestMapping(value = "/searchPageWaitVerify")
	public RetMsg<PageInfo<COPTTR>> searchPageWaitVverify(@RequestBody Map<String,Object> map) {
		Page<COPTTR> page = optionservice.SelectfromtrWV(map);
		return RetMsgHelper.ok(page);

}
	@ResponseBody
	@RequestMapping(value = "/searchPageEfficient")
	public RetMsg<PageInfo<COPTTR>>searchPageEfficient(@RequestBody Map<String,Object> map) {
		Page<COPTTR> page = optionservice.SelectfromtrEF(map);
		return RetMsgHelper.ok(page);

}
	@ResponseBody
	@RequestMapping(value = "/searchCustomerAccount")
	public RetMsg<PageInfo<CUACTR>> searchCustomerAccount(@RequestBody Map<String,Object> map) throws IOException  {

		Page<CUACTR>  page = optionservice.searchCustomerAccount(map);
		return RetMsgHelper.ok(page);
	}
	
	@ResponseBody
	@RequestMapping(value = "/searchPageOperate")
	public RetMsg<PageInfo<COPTTR>> searchPageOperate(@RequestBody Map<String,Object> map) {
	    Page<COPTTR> page = optionservice.SelectfrommaYQ(map);
		return RetMsgHelper.ok(page);
			
	}
	
	@ResponseBody
	@RequestMapping(value = "/commit")
	public RetMsg<Serializable> updatetr(@RequestBody COPTTR map) throws IOException  {

		optionservice.Updatetr(map);
		return RetMsgHelper.ok();
	}
	
	
	@ResponseBody
	@RequestMapping(value = "/checkTradeTr")
	public RetMsg<Serializable>  updatetrCheck(@RequestBody Map<String,Object> map) throws IOException  {

		optionservice.Updatetr(map);
		if("A".equals(map.get("newStatCd"))){
		optionservice.InsertintoMa(map);
		}
		if("Z".equals(map.get("newStatCd"))){
		optionservice.Updatema(map);
		}
		
		
		return RetMsgHelper.ok();
	}
	

	@ResponseBody
	@RequestMapping(value = "/refuse")
	public RetMsg<Serializable> refusetr(@RequestBody COPTTR map) throws IOException  {

		optionservice.Updatetr(map);
		return RetMsgHelper.ok();
	}
	
	@ResponseBody
	@RequestMapping(value = "/searchfdqvno")
	public RetMsg<PageInfo<FDQVMA>> searchFdqvno( @RequestBody Map<String,Object> map) throws IOException  {
 
		Page<FDQVMA>  page=optionservice.searchFdqvno(map);
		return RetMsgHelper.ok(page);
	}
	
	@ResponseBody
	@RequestMapping(value = "/delete")
	public RetMsg<Serializable> deletetr(@RequestBody String[] COPTNO) throws IOException  {

		optionservice.Deletetr(COPTNO);
		return RetMsgHelper.ok();
	}
	
}


