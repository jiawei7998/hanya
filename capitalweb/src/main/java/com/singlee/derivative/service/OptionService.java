package com.singlee.derivative.service;


import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.cap.bookkeeping.model.TbkDealBalance;
import com.singlee.capital.counterparty.model.CounterPartyVo;
import com.singlee.derivative.model.CUACTR;
import com.singlee.derivative.model.FDQVMA;
import com.singlee.derivative.model.JSYQMA;
import com.singlee.derivative.model.COPTTR;

/**
 * 数据保存到表中
 * 
 * @param data
 */
public interface OptionService {
	
    //第一次保存按钮调用，插入到交易表中
	public String Insertintoma( COPTTR param);
	//复核通过调用，插入到主表中
	public void InsertintoMa( Map<String, Object> param);
	//暂存队列修改后再次保存或者提交时或者拒绝时调用，更新交易表状态
	public void Updatetr( COPTTR param);
	//暂存队列修改后再次保存或者提交时或者拒绝时调用，更新交易表状态，
	public void Updatetr( Map<String, Object> param);
	//展期更新主表交割日调用
	public void Updatema( Map<String, Object> param);
	//LIST页面查询时调用
    public Page<COPTTR> Selectfromtr(Map<String, Object> map);
    //经办队列查询调用
    public Page<COPTTR> SelectfrommaYQ(Map<String, Object> map);
    //暂存队列查询调用
    public Page<COPTTR> SelectfromtrTS(Map<String, Object> map);
    //待修改队列查询调用
    public Page<COPTTR> SelectfromtrWM(Map<String, Object> map);
    //待复核队列查询调用
    public Page<COPTTR> SelectfromtrWV(Map<String, Object> map);
    //生效队列查询调用
    public Page<COPTTR> SelectfromtrEF(Map<String, Object> map);
    //页面点击帐号选择按钮查询客户帐号调用
	public Page<CUACTR> searchCustomerAccount(Map<String, Object> map);
	//页面点击询价编号查询适用的询价业务编号调用
	public Page<FDQVMA> searchFdqvno(Map<String, Object> map);
	//页面点击删除根据业务编号删除暂存队列中的交易
	public void Deletetr( String[] jsyqNo);
   
	
}
