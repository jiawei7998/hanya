package com.singlee.derivative.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.common.log.AutoLogMethod;
import com.singlee.derivative.mapper.ForwardCustMapper;
import com.singlee.derivative.mapper.ForwardInquMapper;
import com.singlee.derivative.mapper.ForwardInqunoMapper;
import com.singlee.derivative.mapper.ForwardTaskMapper;
import com.singlee.derivative.model.CUACTR;
import com.singlee.derivative.model.FDQVMA;
import com.singlee.derivative.model.FDQVTR;
import com.singlee.derivative.model.JSYQMA;
import com.singlee.derivative.model.LTYQAD;
import com.singlee.derivative.service.FwdService;
import com.singlee.derivative.service.InquiryService;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class InquiryServiceImpl implements InquiryService {


	@Autowired
	private ForwardInquMapper forwardInquMapper;
	@Autowired
	private ForwardInqunoMapper forwardInqunoMapper;
	
	

	@Override 
	public String InsertintotrQV(FDQVTR param) {
		String inqType = param.getInqType();
		String fdqvNo="";
		if(param.getFDQVNO()==""|| "".equals(param.getFDQVNO())){
			if("FDQV".equals(inqType)){//远期
				fdqvNo = "FDQV"+new SimpleDateFormat("yyyyMMdd").format(new Date())+ forwardInqunoMapper.getSequenceId();
			}else if("SPQV".equals(inqType)){//掉期
				fdqvNo = "SPQV"+new SimpleDateFormat("yyyyMMdd").format(new Date())+ forwardInqunoMapper.getSequenceId();
			}else if("OPQV".equals(inqType)){//期权
				fdqvNo = "OPQV"+new SimpleDateFormat("yyyyMMdd").format(new Date())+ forwardInqunoMapper.getSequenceId();
			}else if("IRQV".equals(inqType)){//利率互换
				fdqvNo = "IRQV"+new SimpleDateFormat("yyyyMMdd").format(new Date())+ forwardInqunoMapper.getSequenceId();
			}
			param.setFDQVNO(fdqvNo);
		}else{
			fdqvNo=param.getFDQVNO();
		}
		 String result= forwardInquMapper.selectfromtrQV(param);
		 if("0".equals(result)){
			 forwardInquMapper.insertintotrQV(param);
		 }else{
			 if(param.getSTATCD()==""|| "".equals(param.getSTATCD())){
				 forwardInquMapper.UpdatetrQV(param);
			 }
			 else{
				 forwardInquMapper.UpdatetrQVS(param);
			 }
		 }
		 
		 return fdqvNo;
		
	}
	

	@Override
	public void UpdatetrQV(FDQVTR param) {
		// TODO Auto-generated method stub
		forwardInquMapper.UpdatetrQV(param);
		
	}
	@Override
	public void UpdatetrQV(Map<String, Object> param) {
		// TODO Auto-generated method stub
		forwardInquMapper.updatetrQV(param);
		
	}
	
	@Override
    public Page<FDQVTR> SelectfromtrQV(
			Map<String, Object> map) {
		
		Page<FDQVTR> result = forwardInquMapper.selectfromtrQV(map);
		return result;
	}
	
	@Override
    public Page<FDQVMA> SelectfrommaQV(
			Map<String, Object> map) {
		
		Page<FDQVMA> result = forwardInquMapper.selectfrommaQV(map);
		return result;
	}
	@Override
	public void InsertintoMaQV(Map<String, Object> param) {
		// TODO Auto-generated method stub
		if("C".equals(param.get("STATCD"))){
			forwardInquMapper.updatemaQV(param);
		}else{
		forwardInquMapper.insertintoMaQV(param);
		}
	}
	
	
	
	@Override
    public Page<FDQVTR> SelectfromtrTS(
			Map<String, Object> map) {
		
		Page<FDQVTR> result = forwardInquMapper.selectfromtrTS(map);
		return result;
	}
	@Override
    public Page<FDQVTR> SelectfromtrWM(
			Map<String, Object> map) {
		
		Page<FDQVTR> result = forwardInquMapper.selectfromtrWM(map);
		return result;
	}
	@Override
    public Page<FDQVTR> SelectfromtrWV(
			Map<String, Object> map) {
		
		Page<FDQVTR> result = forwardInquMapper.selectfromtrWV(map);
		return result;
	}
	@Override
    public Page<FDQVTR> SelectfromtrEF(
			Map<String, Object> map) {
		
		Page<FDQVTR> result = forwardInquMapper.selectfromtrEF(map);
		return result;
	}

	@Override
	@AutoLogMethod(value="删除自定义字段")
	public void DeletetrQV(String[] fDQVNO) {
		for (int i = 0; i < fDQVNO.length; i++) {
			forwardInquMapper.deletetrQV(fDQVNO[i]);
		}
	}
}

