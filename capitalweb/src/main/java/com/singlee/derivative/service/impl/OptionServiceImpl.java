package com.singlee.derivative.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.common.log.AutoLogMethod;
import com.singlee.derivative.mapper.ForwardCustMapper;
import com.singlee.derivative.mapper.ForwardInqunoMapper;
import com.singlee.derivative.mapper.OptionTaskMapper;

import com.singlee.derivative.model.CUACTR;
import com.singlee.derivative.model.FDQVMA;
import com.singlee.derivative.model.JSYQMA;
import com.singlee.derivative.model.COPTTR;
import com.singlee.derivative.service.FwdService;
import com.singlee.derivative.service.OptionService;
import com.singlee.derivative.service.SwapService;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class OptionServiceImpl implements  OptionService {

	@Autowired
	private OptionTaskMapper optionTaskMapper;
	
	@Autowired
	private ForwardCustMapper forwardCustMapper;
	@Autowired
	private ForwardInqunoMapper forwardInqunoMapper;

	@Override 
	public String Insertintoma(COPTTR param) {
		// TODO Auto-generated method stub
		String coptNo="";
		if(param.getCoptNo()==""|| "".equals(param.getCoptNo())){
		  coptNo = "COPT"+new SimpleDateFormat("yyyyMMdd").format(new Date())+ forwardCustMapper.getSequenceId();
		
		  param.setCoptNo(coptNo);
		  }
		else{
			coptNo=param.getCoptNo();
		}
		 String result= optionTaskMapper.selectfromtrAD(param);
         if("0".equals(result)){
			 optionTaskMapper.insertintoma(param);
		 }else{
			 optionTaskMapper.updatetr(param);
		 }
		 
		 return coptNo;
		
	}
	

	@Override
	public void Updatetr(COPTTR param) {
		// TODO Auto-generated method stub
	
		optionTaskMapper.updatetr(param);
		
	}

	@Override
	public void Updatema(Map<String, Object> param) {
		// TODO Auto-generated method stub
		
		optionTaskMapper.updatema(param);
	}
		
	
	@Override
	public void Updatetr(Map<String, Object> param) {
		// TODO Auto-generated method stub
	
		optionTaskMapper.updatetr(param);
		
	}

	@Override
	public void InsertintoMa(Map<String, Object> param) {
		// TODO Auto-generated method stub
		optionTaskMapper.insertintoMa(param);
		
	}
	
	
	@Override
     public Page<COPTTR> Selectfromtr(
			Map<String, Object> map) {
		
		Page<COPTTR> result = optionTaskMapper.selectfromtr(map);
		return result;
	}
	@Override
    public Page<COPTTR> SelectfrommaYQ(
			Map<String, Object> map) {
		Page<COPTTR> result=null;
		if("C".equals(map.get("newStatCd"))){
		 result = optionTaskMapper.selectfrommaDL(map);
		}
		else if("Y".equals(map.get("newStatCd"))){
		result = optionTaskMapper.selectfrommaYD(map);
		}
		else {
		result = optionTaskMapper.selectfrommaYQ(map);
			
			}
		
		return result;
	}
	
	@Override
    public Page<COPTTR> SelectfromtrTS(
			Map<String, Object> map) {
		
		Page<COPTTR> result = optionTaskMapper.selectfromtrTS(map);
		return result;
	}
	@Override
    public Page<COPTTR> SelectfromtrWM(
			Map<String, Object> map) {
		
		Page<COPTTR> result = optionTaskMapper.selectfromtrWM(map);
		return result;
	}
	@Override
    public Page<COPTTR> SelectfromtrWV(
			Map<String, Object> map) {
		
		Page<COPTTR> result = optionTaskMapper.selectfromtrWV(map);
		return result;
	}
	@Override
    public Page<COPTTR> SelectfromtrEF(
			Map<String, Object> map) {
		
		Page<COPTTR> result = optionTaskMapper.selectfromtrEF(map);
		return result;
	}
	@Override
    public Page<CUACTR> searchCustomerAccount(
			Map<String, Object> map) {
		
		Page<CUACTR> result = forwardCustMapper.searchCustomerAccount(map);
		return result;
	}
	@Override
    public Page<FDQVMA> searchFdqvno(
			Map<String, Object> map) {
		
		Page<FDQVMA> result = forwardInqunoMapper.searchFdqvno(map);
		return result;
	}
	@Override
	@AutoLogMethod(value="删除自定义字段")
	public void Deletetr(String[] Fdseno) {
		for (int i = 0; i < Fdseno.length; i++) {
			optionTaskMapper.deletetr(Fdseno[i]);
		}
	}

}

