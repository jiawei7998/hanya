package com.singlee.derivative.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.common.log.AutoLogMethod;
import com.singlee.derivative.mapper.ForwardCustMapper;
import com.singlee.derivative.mapper.ForwardInqunoMapper;
import com.singlee.derivative.mapper.SwapTaskMapper;
import com.singlee.derivative.model.CUACTR;
import com.singlee.derivative.model.FDQVMA;
import com.singlee.derivative.model.JSYQMA;
import com.singlee.derivative.model.FDSETR;
import com.singlee.derivative.service.FwdService;
import com.singlee.derivative.service.SwapService;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class SwapServiceImpl implements  SwapService {

	@Autowired
	private SwapTaskMapper swapTaskMapper;
	
	@Autowired
	private ForwardCustMapper forwardCustMapper;
	@Autowired
	private ForwardInqunoMapper forwardInqunoMapper;

	@Override 
	public String Insertintoma(FDSETR param) {
		// TODO Auto-generated method stub
		String Fdseno="";
		if(param.getFdseNo()==""|| "".equals(param.getFdseNo())){
		  Fdseno = "FDSE"+new SimpleDateFormat("yyyyMMdd").format(new Date())+ forwardCustMapper.getSequenceId();
		
		  param.setFdseNo(Fdseno);
		  }
		else{
			Fdseno=param.getFdseNo();
		}
		 String result= swapTaskMapper.selectfromtrAD(param);
         if("0".equals(result)){
			 swapTaskMapper.insertintoma(param);
		 }else{
			 swapTaskMapper.updatetr(param);
		 }
		 
		 return Fdseno;
		
	}
	

	@Override
	public void Updatetr(FDSETR param) {
		// TODO Auto-generated method stub
	
		swapTaskMapper.updatetr(param);
		
	}

	@Override
	public void Updatema(Map<String, Object> param) {
		// TODO Auto-generated method stub
		
		swapTaskMapper.updatema(param);
	}
		
	
	@Override
	public void Updatetr(Map<String, Object> param) {
		// TODO Auto-generated method stub
	
		swapTaskMapper.updatetr(param);
		
	}

	@Override
	public void InsertintoMa(Map<String, Object> param) {
		// TODO Auto-generated method stub
		swapTaskMapper.insertintoMa(param);
		
	}
	
	
	@Override
     public Page<FDSETR> Selectfromtr(
			Map<String, Object> map) {
		
		Page<FDSETR> result = swapTaskMapper.selectfromtr(map);
		return result;
	}
	@Override
    public Page<FDSETR> SelectfrommaYQ(
			Map<String, Object> map) {
		Page<FDSETR> result=null;
		if("C".equals(map.get("newStatCd"))){
		 result = swapTaskMapper.selectfrommaDL(map);
		}
		else if("Y".equals(map.get("newStatCd"))){
		result = swapTaskMapper.selectfrommaYD(map);
		}
		else {
		result = swapTaskMapper.selectfrommaYQ(map);
			
			}
		
		return result;
	}
	
	@Override
    public Page<FDSETR> SelectfromtrTS(
			Map<String, Object> map) {
		
		Page<FDSETR> result = swapTaskMapper.selectfromtrTS(map);
		return result;
	}
	@Override
    public Page<FDSETR> SelectfromtrWM(
			Map<String, Object> map) {
		
		Page<FDSETR> result = swapTaskMapper.selectfromtrWM(map);
		return result;
	}
	@Override
    public Page<FDSETR> SelectfromtrWV(
			Map<String, Object> map) {
		
		Page<FDSETR> result = swapTaskMapper.selectfromtrWV(map);
		return result;
	}
	@Override
    public Page<FDSETR> SelectfromtrEF(
			Map<String, Object> map) {
		
		Page<FDSETR> result = swapTaskMapper.selectfromtrEF(map);
		return result;
	}
	@Override
    public Page<CUACTR> searchCustomerAccount(
			Map<String, Object> map) {
		
		Page<CUACTR> result = forwardCustMapper.searchCustomerAccount(map);
		return result;
	}
	@Override
    public Page<FDQVMA> searchFdqvno(
			Map<String, Object> map) {
		
		Page<FDQVMA> result = forwardInqunoMapper.searchFdqvno(map);
		return result;
	}
	@Override
	@AutoLogMethod(value="删除自定义字段")
	public void Deletetr(String[] Fdseno) {
		for (int i = 0; i < Fdseno.length; i++) {
			swapTaskMapper.deletetr(Fdseno[i]);
		}
	}

}

