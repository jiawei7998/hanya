package com.singlee.derivative.service;


import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.cap.bookkeeping.model.TbkDealBalance;
import com.singlee.capital.counterparty.model.CounterPartyVo;
import com.singlee.derivative.model.CUACTR;
import com.singlee.derivative.model.FDQVMA;
import com.singlee.derivative.model.FDQVTR;
import com.singlee.derivative.model.JSYQMA;
import com.singlee.derivative.model.LTYQAD;

/**
 * 数据保存到表中
 * 
 * @param data
 */
public interface InquiryService {
	

	public String InsertintotrQV(FDQVTR param);
	public void UpdatetrQV( FDQVTR param); 
	public void DeletetrQV( String[] fDQVNO);
	public void UpdatetrQV( Map<String, Object> param);
	public void InsertintoMaQV( Map<String, Object> param);
	public Page<FDQVTR> SelectfromtrQV(Map<String, Object> map);
	public Page<FDQVMA> SelectfrommaQV(Map<String, Object> map);
	    public Page<FDQVTR> SelectfromtrTS(Map<String, Object> map);
	    public Page<FDQVTR> SelectfromtrWM(Map<String, Object> map);
	    public Page<FDQVTR> SelectfromtrWV(Map<String, Object> map);
	    public Page<FDQVTR> SelectfromtrEF(Map<String, Object> map);
	
}
