package com.singlee.derivative.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.common.log.AutoLogMethod;
import com.singlee.derivative.mapper.ForwardCustMapper;
import com.singlee.derivative.mapper.ForwardInqunoMapper;
import com.singlee.derivative.mapper.ForwardTaskMapper;
import com.singlee.derivative.model.CUACTR;
import com.singlee.derivative.model.FDQVMA;
import com.singlee.derivative.model.JSYQMA;
import com.singlee.derivative.model.LTYQAD;
import com.singlee.derivative.service.FwdService;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class FwdServiceImpl implements  FwdService {


	@Autowired
	private ForwardTaskMapper forwardTaskMapper;
	@Autowired
	private ForwardCustMapper forwardCustMapper;
	@Autowired
	private ForwardInqunoMapper forwardInqunoMapper;

	@Override 
	public String Insertintoma(LTYQAD param) {
		// TODO Auto-generated method stub
		String jsyqNo="";
		if(param.getJsyqNo()==""|| "".equals(param.getJsyqNo())){
		  jsyqNo = "JSYQ"+new SimpleDateFormat("yyyyMMdd").format(new Date())+ forwardCustMapper.getSequenceId();
		
		  param.setJsyqNo(jsyqNo);
		  }
		else{
			jsyqNo=param.getJsyqNo();
		}
		 String result= forwardTaskMapper.selectfromtrAD(param);
         if("0".equals(result)){
			 forwardTaskMapper.insertintoma(param);
		 }else{
			 forwardTaskMapper.updatetr(param);
		 }
		 
		 return jsyqNo;
		
	}
	

	@Override
	public void Updatetr(LTYQAD param) {
		// TODO Auto-generated method stub
		String wbamts=param.getwBamts();
		if("".equals(param.getWbamBl())){
			param.setWbamBl(wbamts);
		}
		forwardTaskMapper.updatetr(param);
		
	}

	@Override
	public void Updatema(Map<String, Object> param) {
		// TODO Auto-generated method stub
		
		forwardTaskMapper.updatema(param);
	}
		
	
	@Override
	public void Updatetr(Map<String, Object> param) {
		// TODO Auto-generated method stub
		Object wbamts=param.get("wBamts");
		if("".equals(param.get("wbamBl"))){
			param.put("wbamBl",wbamts);
		}
		forwardTaskMapper.updatetr(param);
		
	}

	@Override
	public void InsertintoMa(Map<String, Object> param) {
		// TODO Auto-generated method stub
		forwardTaskMapper.insertintoMa(param);
		
	}
	
	
	@Override
     public Page<LTYQAD> Selectfromtr(
			Map<String, Object> map) {
		
		Page<LTYQAD> result = forwardTaskMapper.selectfromtr(map);
		return result;
	}
	@Override
    public Page<LTYQAD> SelectfrommaYQ(
			Map<String, Object> map) {
		Page<LTYQAD> result=null;
		if("C".equals(map.get("newStatCd"))){
		 result = forwardTaskMapper.selectfrommaDL(map);
		}
		else{
		result = forwardTaskMapper.selectfrommaYQ(map);
			
			}
		
		return result;
	}
	
	@Override
    public Page<LTYQAD> SelectfromtrTS(
			Map<String, Object> map) {
		
		Page<LTYQAD> result = forwardTaskMapper.selectfromtrTS(map);
		return result;
	}
	@Override
    public Page<LTYQAD> SelectfromtrWM(
			Map<String, Object> map) {
		
		Page<LTYQAD> result = forwardTaskMapper.selectfromtrWM(map);
		return result;
	}
	@Override
    public Page<LTYQAD> SelectfromtrWV(
			Map<String, Object> map) {
		
		Page<LTYQAD> result = forwardTaskMapper.selectfromtrWV(map);
		return result;
	}
	@Override
    public Page<LTYQAD> SelectfromtrEF(
			Map<String, Object> map) {
		
		Page<LTYQAD> result = forwardTaskMapper.selectfromtrEF(map);
		return result;
	}
	@Override
    public Page<CUACTR> searchCustomerAccount(
			Map<String, Object> map) {
		
		Page<CUACTR> result = forwardCustMapper.searchCustomerAccount(map);
		return result;
	}
	@Override
    public Page<FDQVMA> searchFdqvno(
			Map<String, Object> map) {
		
		Page<FDQVMA> result = forwardInqunoMapper.searchFdqvno(map);
		return result;
	}
	@Override
	@AutoLogMethod(value="删除自定义字段")
	public void Deletetr(String[] jsyqNo) {
		for (int i = 0; i < jsyqNo.length; i++) {
			forwardTaskMapper.deletetr(jsyqNo[i]);
		}
	}

}

