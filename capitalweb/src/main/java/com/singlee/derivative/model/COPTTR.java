package com.singlee.derivative.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class COPTTR implements Serializable {
	 private String coptpk;

	    private String utchno;

	    private String inerno;

	    private String brchno;

	    private String coptNo;

	    private String dlDate;

	    private String custNo;
	    
	    private String custAd;
	    
	    private String custNm;

	    private String qabank;

	    private String qainac;

	    private String trAmts;

	    private String exerDt;

	    private String wbamdo;

	    private String wbambl;

	    private String coptst;

	    private String statcd;

	    private String upvaam;

	    private String contrt;

	    private String bankFr;

	    private String custFr;

	    private String bankPr;

	    private String custPr;

	    private String upDown;

	    private String yucyNo;

	    private String mdcyNo;

	    private String mdacNo;

	    private String faCtrt;

	    private String gnlsAm;

	    private String jqRate;

	    private String sigMa;

	    private String ukDate;

	    private String jsyqTp;
	    
	    private String userSt;
	    
	    private String chckSt;
	    
	    private String granSt;
	    

    public String getCoptpk() {
			return coptpk;
		}


		public void setCoptpk(String coptpk) {
			this.coptpk = coptpk;
		}


		public String getUtchno() {
			return utchno;
		}


		public void setUtchno(String utchno) {
			this.utchno = utchno;
		}


		public String getInerno() {
			return inerno;
		}


		public void setInerno(String inerno) {
			this.inerno = inerno;
		}


		public String getBrchno() {
			return brchno;
		}


		public void setBrchno(String brchno) {
			this.brchno = brchno;
		}


		public String getCoptNo() {
			return coptNo;
		}


		public void setCoptNo(String coptNo) {
			this.coptNo = coptNo;
		}


		public String getDlDate() {
			return dlDate;
		}


		public void setDlDate(String dlDate) {
			this.dlDate = dlDate;
		}


		public String getCustNo() {
			return custNo;
		}


		public void setCustNo(String custNo) {
			this.custNo = custNo;
		}


		public String getCustAd() {
			return custAd;
		}


		public void setCustAd(String custAd) {
			this.custAd = custAd;
		}


		public String getCustNm() {
			return custNm;
		}


		public void setCustNm(String custNm) {
			this.custNm = custNm;
		}


		public String getQabank() {
			return qabank;
		}


		public void setQabank(String qabank) {
			this.qabank = qabank;
		}


		public String getQainac() {
			return qainac;
		}


		public void setQainac(String qainac) {
			this.qainac = qainac;
		}


		public String getTrAmts() {
			return trAmts;
		}


		public void setTrAmts(String trAmts) {
			this.trAmts = trAmts;
		}


		public String getExerDt() {
			return exerDt;
		}


		public void setExerDt(String exerDt) {
			this.exerDt = exerDt;
		}


		public String getWbamdo() {
			return wbamdo;
		}


		public void setWbamdo(String wbamdo) {
			this.wbamdo = wbamdo;
		}


		public String getWbambl() {
			return wbambl;
		}


		public void setWbambl(String wbambl) {
			this.wbambl = wbambl;
		}


		public String getCoptst() {
			return coptst;
		}


		public void setCoptst(String coptst) {
			this.coptst = coptst;
		}


		public String getStatcd() {
			return statcd;
		}


		public void setStatcd(String statcd) {
			this.statcd = statcd;
		}


		public String getUpvaam() {
			return upvaam;
		}


		public void setUpvaam(String upvaam) {
			this.upvaam = upvaam;
		}


		public String getContrt() {
			return contrt;
		}


		public void setContrt(String contrt) {
			this.contrt = contrt;
		}


		public String getBankFr() {
			return bankFr;
		}


		public void setBankFr(String bankFr) {
			this.bankFr = bankFr;
		}


		public String getCustFr() {
			return custFr;
		}


		public void setCustFr(String custFr) {
			this.custFr = custFr;
		}


		public String getBankPr() {
			return bankPr;
		}


		public void setBankPr(String bankPr) {
			this.bankPr = bankPr;
		}


		public String getCustPr() {
			return custPr;
		}


		public void setCustPr(String custPr) {
			this.custPr = custPr;
		}


		public String getUpDown() {
			return upDown;
		}


		public void setUpDown(String upDown) {
			this.upDown = upDown;
		}


		public String getYucyNo() {
			return yucyNo;
		}


		public void setYucyNo(String yucyNo) {
			this.yucyNo = yucyNo;
		}


		public String getMdcyNo() {
			return mdcyNo;
		}


		public void setMdcyNo(String mdcyNo) {
			this.mdcyNo = mdcyNo;
		}


		public String getMdacNo() {
			return mdacNo;
		}


		public void setMdacNo(String mdacNo) {
			this.mdacNo = mdacNo;
		}


	


	


		public String getFaCtrt() {
			return faCtrt;
		}


		public void setFaCtrt(String faCtrt) {
			this.faCtrt = faCtrt;
		}


		public String getGnlsAm() {
			return gnlsAm;
		}


		public void setGnlsAm(String gnlsAm) {
			this.gnlsAm = gnlsAm;
		}


		public String getJqRate() {
			return jqRate;
		}


		public void setJqRate(String jqRate) {
			this.jqRate = jqRate;
		}


		public String getSigMa() {
			return sigMa;
		}


		public void setSigMa(String sigMa) {
			this.sigMa = sigMa;
		}


		public String getUkDate() {
			return ukDate;
		}


		public void setUkDate(String ukDate) {
			this.ukDate = ukDate;
		}


		public String getJsyqTp() {
			return jsyqTp;
		}


		public void setJsyqTp(String jsyqTp) {
			this.jsyqTp = jsyqTp;
		}


		public String getUserSt() {
			return userSt;
		}


		public void setUserSt(String userSt) {
			this.userSt = userSt;
		}


		public String getChckSt() {
			return chckSt;
		}


		public void setChckSt(String chckSt) {
			this.chckSt = chckSt;
		}


		public String getGranSt() {
			return granSt;
		}


		public void setGranSt(String granSt) {
			this.granSt = granSt;
		}


	private static final long serialVersionUID = 1L;

    
}