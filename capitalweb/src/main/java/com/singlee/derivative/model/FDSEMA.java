package com.singlee.derivative.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * LTYQAD     
 * @author   YANGLIU
 * Create Time  2013-01-21 15:10:08
 */
/**
 * 登记
 */
@Entity
@Table(name="ITS_FDSEMA")
public class FDSEMA implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5673855701376759250L;
	@Id
	private String tranId;
	private String fdseId;
	private String utchNo;
	private String inerNo; 
	private String brchNo; 
	private String dlDate; 
	private String rFerno; 
	private String ltPrid; 
	private String fdseNo; 
	private String custNo; 
	private String custNm; 
	private String custAd; 
	private String trAmts; 
	private String bycyNo; 
	private String slcyNo; 
	private String spotFr; 
	private String spotDt; 
	private String spotSt; 
	private String forwFr; 
	private String forwDt; 
	private String forwSt; 
	private String qtcyNo; 
	private String qTamts; 
	private String crqtNo; 
	private String qaRate; 
	private String mgRate; 
	private String mgAcno; 
	private String suAcno; 
	private String mgAcam; 
	private String mgAccy; 
	private String mgAcn2; 
	private String suAcn2; 
	private String mgAcc2; 
	private String mgAca2; 
	private String mgAcn3; 
	private String suAcn3; 
	private String mgAcc3; 
	private String mgAca3; 
	private String trmCd1; 
	private String trmCd2; 
	private String trmCd3; 
	private String iNstac; 
	private String iNstc1; 
	private String iNstc2; 
	private String tranMo; 
	private String slacNo; 
	private String byacNo; 
	private String fdseTp; 
	private String wBamdo; 
	private String wBambl; 
    private String statCd; 
	private String trAmt1; 
	private String trAmt2; 
	private String trAmt3; 
	private String trAmt4; 
	private String spRate; 
	private String foRate; 
	private String gNlsam; 
	private String oForwd; 
	private String oForwf; 
	private String oSpotd; 
	public String getTranId() {
		return tranId;
	}
	public void setTranId(String tranId) {
		this.tranId = tranId;
	}
	public String getFdseId() {
		return fdseId;
	}
	public void setFdseId(String fdseId) {
		this.fdseId = fdseId;
	}
	public String getUtchNo() {
		return utchNo;
	}
	public void setUtchNo(String utchNo) {
		this.utchNo = utchNo;
	}
	public String getInerNo() {
		return inerNo;
	}
	public void setInerNo(String inerNo) {
		this.inerNo = inerNo;
	}
	public String getBrchNo() {
		return brchNo;
	}
	public void setBrchNo(String brchNo) {
		this.brchNo = brchNo;
	}
	public String getDlDate() {
		return dlDate;
	}
	public void setDlDate(String dlDate) {
		this.dlDate = dlDate;
	}
	public String getrFerno() {
		return rFerno;
	}
	public void setrFerno(String rFerno) {
		this.rFerno = rFerno;
	}
	public String getLtPrid() {
		return ltPrid;
	}
	public void setLtPrid(String ltPrid) {
		this.ltPrid = ltPrid;
	}
	public String getFdseNo() {
		return fdseNo;
	}
	public void setFdseNo(String fdseNo) {
		this.fdseNo = fdseNo;
	}
	public String getCustNo() {
		return custNo;
	}
	public void setCustNo(String custNo) {
		this.custNo = custNo;
	}
	public String getCustNm() {
		return custNm;
	}
	public void setCustNm(String custNm) {
		this.custNm = custNm;
	}
	public String getCustAd() {
		return custAd;
	}
	public void setCustAd(String custAd) {
		this.custAd = custAd;
	}
	public String getTrAmts() {
		return trAmts;
	}
	public void setTrAmts(String trAmts) {
		this.trAmts = trAmts;
	}
	public String getBycyNo() {
		return bycyNo;
	}
	public void setBycyNo(String bycyNo) {
		this.bycyNo = bycyNo;
	}
	public String getSlcyNo() {
		return slcyNo;
	}
	public void setSlcyNo(String slcyNo) {
		this.slcyNo = slcyNo;
	}
	public String getSpotFr() {
		return spotFr;
	}
	public void setSpotFr(String spotFr) {
		this.spotFr = spotFr;
	}
	public String getSpotDt() {
		return spotDt;
	}
	public void setSpotDt(String spotDt) {
		this.spotDt = spotDt;
	}
	public String getSpotSt() {
		return spotSt;
	}
	public void setSpotSt(String spotSt) {
		this.spotSt = spotSt;
	}
	public String getForwFr() {
		return forwFr;
	}
	public void setForwFr(String forwFr) {
		this.forwFr = forwFr;
	}
	public String getForwDt() {
		return forwDt;
	}
	public void setForwDt(String forwDt) {
		this.forwDt = forwDt;
	}
	public String getForwSt() {
		return forwSt;
	}
	public void setForwSt(String forwSt) {
		this.forwSt = forwSt;
	}
	public String getQtcyNo() {
		return qtcyNo;
	}
	public void setQtcyNo(String qtcyNo) {
		this.qtcyNo = qtcyNo;
	}
	public String getqTamts() {
		return qTamts;
	}
	public void setqTamts(String qTamts) {
		this.qTamts = qTamts;
	}
	public String getCrqtNo() {
		return crqtNo;
	}
	public void setCrqtNo(String crqtNo) {
		this.crqtNo = crqtNo;
	}
	public String getQaRate() {
		return qaRate;
	}
	public void setQaRate(String qaRate) {
		this.qaRate = qaRate;
	}
	public String getMgRate() {
		return mgRate;
	}
	public void setMgRate(String mgRate) {
		this.mgRate = mgRate;
	}
	public String getMgAcno() {
		return mgAcno;
	}
	public void setMgAcno(String mgAcno) {
		this.mgAcno = mgAcno;
	}
	public String getSuAcno() {
		return suAcno;
	}
	public void setSuAcno(String suAcno) {
		this.suAcno = suAcno;
	}
	public String getMgAcam() {
		return mgAcam;
	}
	public void setMgAcam(String mgAcam) {
		this.mgAcam = mgAcam;
	}
	public String getMgAccy() {
		return mgAccy;
	}
	public void setMgAccy(String mgAccy) {
		this.mgAccy = mgAccy;
	}
	public String getMgAcn2() {
		return mgAcn2;
	}
	public void setMgAcn2(String mgAcn2) {
		this.mgAcn2 = mgAcn2;
	}
	public String getSuAcn2() {
		return suAcn2;
	}
	public void setSuAcn2(String suAcn2) {
		this.suAcn2 = suAcn2;
	}
	public String getMgAcc2() {
		return mgAcc2;
	}
	public void setMgAcc2(String mgAcc2) {
		this.mgAcc2 = mgAcc2;
	}
	public String getMgAca2() {
		return mgAca2;
	}
	public void setMgAca2(String mgAca2) {
		this.mgAca2 = mgAca2;
	}
	public String getMgAcn3() {
		return mgAcn3;
	}
	public void setMgAcn3(String mgAcn3) {
		this.mgAcn3 = mgAcn3;
	}
	public String getSuAcn3() {
		return suAcn3;
	}
	public void setSuAcn3(String suAcn3) {
		this.suAcn3 = suAcn3;
	}
	public String getMgAcc3() {
		return mgAcc3;
	}
	public void setMgAcc3(String mgAcc3) {
		this.mgAcc3 = mgAcc3;
	}
	public String getMgAca3() {
		return mgAca3;
	}
	public void setMgAca3(String mgAca3) {
		this.mgAca3 = mgAca3;
	}
	public String getTrmCd1() {
		return trmCd1;
	}
	public void setTrmCd1(String trmCd1) {
		this.trmCd1 = trmCd1;
	}
	public String getTrmCd2() {
		return trmCd2;
	}
	public void setTrmCd2(String trmCd2) {
		this.trmCd2 = trmCd2;
	}
	public String getTrmCd3() {
		return trmCd3;
	}
	public void setTrmCd3(String trmCd3) {
		this.trmCd3 = trmCd3;
	}
	public String getiNstac() {
		return iNstac;
	}
	public void setiNstac(String iNstac) {
		this.iNstac = iNstac;
	}
	public String getiNstc1() {
		return iNstc1;
	}
	public void setiNstc1(String iNstc1) {
		this.iNstc1 = iNstc1;
	}
	public String getiNstc2() {
		return iNstc2;
	}
	public void setiNstc2(String iNstc2) {
		this.iNstc2 = iNstc2;
	}
	public String getTranMo() {
		return tranMo;
	}
	public void setTranMo(String tranMo) {
		this.tranMo = tranMo;
	}
	public String getSlacNo() {
		return slacNo;
	}
	public void setSlacNo(String slacNo) {
		this.slacNo = slacNo;
	}
	public String getByacNo() {
		return byacNo;
	}
	public void setByacNo(String byacNo) {
		this.byacNo = byacNo;
	}
	public String getFdseTp() {
		return fdseTp;
	}
	public void setFdseTp(String fdseTp) {
		this.fdseTp = fdseTp;
	}
	public String getwBamdo() {
		return wBamdo;
	}
	public void setwBamdo(String wBamdo) {
		this.wBamdo = wBamdo;
	}
	public String getwBambl() {
		return wBambl;
	}
	public void setwBambl(String wBambl) {
		this.wBambl = wBambl;
	}
	public String getStatCd() {
		return statCd;
	}
	public void setStatCd(String statCd) {
		this.statCd = statCd;
	}
	public String getTrAmt1() {
		return trAmt1;
	}
	public void setTrAmt1(String trAmt1) {
		this.trAmt1 = trAmt1;
	}
	public String getTrAmt2() {
		return trAmt2;
	}
	public void setTrAmt2(String trAmt2) {
		this.trAmt2 = trAmt2;
	}
	public String getTrAmt3() {
		return trAmt3;
	}
	public void setTrAmt3(String trAmt3) {
		this.trAmt3 = trAmt3;
	}
	public String getTrAmt4() {
		return trAmt4;
	}
	public void setTrAmt4(String trAmt4) {
		this.trAmt4 = trAmt4;
	}
	public String getSpRate() {
		return spRate;
	}
	public void setSpRate(String spRate) {
		this.spRate = spRate;
	}
	public String getFoRate() {
		return foRate;
	}
	public void setFoRate(String foRate) {
		this.foRate = foRate;
	}
	public String getgNlsam() {
		return gNlsam;
	}
	public void setgNlsam(String gNlsam) {
		this.gNlsam = gNlsam;
	}
	public String getoForwd() {
		return oForwd;
	}
	public void setoForwd(String oForwd) {
		this.oForwd = oForwd;
	}
	public String getoForwf() {
		return oForwf;
	}
	public void setoForwf(String oForwf) {
		this.oForwf = oForwf;
	}
	public String getoSpotd() {
		return oSpotd;
	}
	public void setoSpotd(String oSpotd) {
		this.oSpotd = oSpotd;
	}
	public String getoSpotf() {
		return oSpotf;
	}
	public void setoSpotf(String oSpotf) {
		this.oSpotf = oSpotf;
	}
	public String getFdqvNo() {
		return fdqvNo;
	}
	public void setFdqvNo(String fdqvNo) {
		this.fdqvNo = fdqvNo;
	}
	public String getYbcyNo() {
		return ybcyNo;
	}
	public void setYbcyNo(String ybcyNo) {
		this.ybcyNo = ybcyNo;
	}
	public String getYscyNo() {
		return yscyNo;
	}
	public void setYscyNo(String yscyNo) {
		this.yscyNo = yscyNo;
	}
	public String getoSprat() {
		return oSprat;
	}
	public void setoSprat(String oSprat) {
		this.oSprat = oSprat;
	}
	public String getoForat() {
		return oForat;
	}
	public void setoForat(String oForat) {
		this.oForat = oForat;
	}
	public String getoTram1() {
		return oTram1;
	}
	public void setoTram1(String oTram1) {
		this.oTram1 = oTram1;
	}
	public String getoTram2() {
		return oTram2;
	}
	public void setoTram2(String oTram2) {
		this.oTram2 = oTram2;
	}
	public String getoTram3() {
		return oTram3;
	}
	public void setoTram3(String oTram3) {
		this.oTram3 = oTram3;
	}
	public String getoTram4() {
		return oTram4;
	}
	public void setoTram4(String oTram4) {
		this.oTram4 = oTram4;
	}
	private String oSpotf; 
	private String fdqvNo; 
	private String ybcyNo; 
	private String yscyNo; 
	private String oSprat; 
	private String oForat; 
	private String oTram1; 
	private String oTram2; 
	private String oTram3; 
	private String oTram4;	
	
	

}