package com.singlee.derivative.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * LTYQAD     
 * @author   YANGLIU
 * Create Time  2013-01-21 15:10:08
 */
/**
 * 登记
 */
@Entity
@Table(name="ITS_JSYQMA")
public class JSYQMA implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5673855701376759250L;
	@Id
	/**机构号*/
	private String brchNo;
	/**机构名称*/
	private String brchNm;
	/**业务编号*/
	private String jsyqNo;
	/**客户代码*/
	private String custNo;
	/**客户名称*/
	private String custNm;
	/**客户地址 */
	private String custAd;
	/**结汇/售汇*/
	private String jsyqTp;
	/**原币种*/
	private String yucyNo;
	/**原账号*/
	private String yuacNo;
	/**目的币种*/
	private String mdcyNo;
	/**目的账号*/
	private String mdacNo;
	/**交割总金额（外币*/
	private String wBamts;
	/**询价业务编号*/
	private String fdQvno;
	/**我行报客户汇率*/
	private String myRate;
	/**总行平盘汇率*/
	private String exRate;
	/**交割方式*/
	private String jgType;
	/**固定交割日*/
	private String jgDate;
	/**择期交割日开始日*/
	private String jgDatf;
	
	/**择期交割日到期日*/
	private String jgDatt;
	/**占用额度币种*/
	private String qtCyno;
	/**金额*/
	private String qTamts;
	/**冻结编号一*/
	private String mgAcno;
	/**保证金账号*/
	private String suAcno;
	/**币种*/
	private String mgAccy;
	/**金额*/
	private String mgAcam;
	/**冻结编号二*/
	private String mgAcn2;
	/**保证金账号*/
	private String suAcn2;
	/**币种*/
	private String mgAcc2;
	/**金额*/
	private String mgAca2;
	/**冻结编号三*/
	private String mgAcn3;
	/**保证金账号*/
	private String suAcn3;
	/**币种*/
	private String mgAcc3;
	/**金额*/
	private String mgAca3;
	/**经办状态码*/
	
	
	
	
	public String getBrchNo() {
		return brchNo;
	}
	public void setBrchNo(String brchNo) {
		this.brchNo = brchNo;
	}
	public String getBrchNm() {
		return brchNm;
	}
	public void setBrchNm(String brchNm) {
		this.brchNm = brchNm;
	}
	
	public String getJsyqNo() {
		return jsyqNo;
	}
	public void setJsyqNo(String jsyqNo) {
		this.jsyqNo = jsyqNo;
	}
	public String getCustNo() {
		return custNo;
	}
	public void setCustNo(String custNo) {
		this.custNo = custNo;
	}
	public String getCustNm() {
		return custNm;
	}
	public void setCustNm(String custNm) {
		this.custNm = custNm;
	}
	public String getCustAd() {
		return custAd;
	}
	public void setCustAd(String custAd) {
		this.custAd = custAd;
	}
	public String getJsyqTp() {
		return jsyqTp;
	}
	public void setJsyqTp(String jsyqTp) {
		this.jsyqTp = jsyqTp;
	}
	public String getYucyNo() {
		return yucyNo;
	}
	public void setYucyNo(String yucyNo) {
		this.yucyNo = yucyNo;
	}
	public String getYuacNo() {
		return yuacNo;
	}
	public void setYuacNo(String yuacNo) {
		this.yuacNo = yuacNo;
	}
	public String getMdcyNo() {
		return mdcyNo;
	}
	public void setMdcyNo(String mdcyNo) {
		this.mdcyNo = mdcyNo;
	}
	public String getMdacNo() {
		return mdacNo;
	}
	public void setMdacNo(String mdacNo) {
		this.mdacNo = mdacNo;
	}
	public String getwBamts() {
		return wBamts;
	}
	public void setwBamts(String wBamts) {
		this.wBamts = wBamts;
	}
	public String getFdQvno() {
		return fdQvno;
	}
	public void setFdQvno(String fdQvno) {
		this.fdQvno = fdQvno;
	}
	public String getMyRate() {
		return myRate;
	}
	public void setMyRate(String myRate) {
		this.myRate = myRate;
	}
	public String getExRate() {
		return exRate;
	}
	public void setExRate(String exRate) {
		this.exRate = exRate;
	}
	public String getJgType() {
		return jgType;
	}
	public void setJgType(String jgType) {
		this.jgType = jgType;
	}
	public String getJgDate() {
		return jgDate;
	}
	public void setJgDate(String jgDate) {
		this.jgDate = jgDate;
	}
	public String getJgDatf() {
		return jgDatf;
	}
	public void setJgDatf(String jgDatf) {
		this.jgDatf = jgDatf;
	}
	public String getJgDatt() {
		return jgDatt;
	}
	public void setJgDatt(String jgDatt) {
		this.jgDatt = jgDatt;
	}
	public String getQtCyno() {
		return qtCyno;
	}
	public void setQtCyno(String qtCyno) {
		this.qtCyno = qtCyno;
	}
	public String getqTamts() {
		return qTamts;
	}
	public void setqTamts(String qTamts) {
		this.qTamts = qTamts;
	}
	public String getMgAcno() {
		return mgAcno;
	}
	public void setMgAcno(String mgAcno) {
		this.mgAcno = mgAcno;
	}
	public String getSuAcno() {
		return suAcno;
	}
	public void setSuAcno(String suAcno) {
		this.suAcno = suAcno;
	}
	public String getMgAccy() {
		return mgAccy;
	}
	public void setMgAccy(String mgAccy) {
		this.mgAccy = mgAccy;
	}
	public String getMgAcam() {
		return mgAcam;
	}
	public void setMgAcam(String mgAcam) {
		this.mgAcam = mgAcam;
	}
	public String getMgAcn2() {
		return mgAcn2;
	}
	public void setMgAcn2(String mgAcn2) {
		this.mgAcn2 = mgAcn2;
	}
	public String getSuAcn2() {
		return suAcn2;
	}
	public void setSuAcn2(String suAcn2) {
		this.suAcn2 = suAcn2;
	}
	public String getMgAcc2() {
		return mgAcc2;
	}
	public void setMgAcc2(String mgAcc2) {
		this.mgAcc2 = mgAcc2;
	}
	public String getMgAca2() {
		return mgAca2;
	}
	public void setMgAca2(String mgAca2) {
		this.mgAca2 = mgAca2;
	}
	public String getMgAcn3() {
		return mgAcn3;
	}
	public void setMgAcn3(String mgAcn3) {
		this.mgAcn3 = mgAcn3;
	}
	public String getSuAcn3() {
		return suAcn3;
	}
	public void setSuAcn3(String suAcn3) {
		this.suAcn3 = suAcn3;
	}
	public String getMgAcc3() {
		return mgAcc3;
	}
	public void setMgAcc3(String mgAcc3) {
		this.mgAcc3 = mgAcc3;
	}
	public String getMgAca3() {
		return mgAca3;
	}
	public void setMgAca3(String mgAca3) {
		this.mgAca3 = mgAca3;
	}
}
