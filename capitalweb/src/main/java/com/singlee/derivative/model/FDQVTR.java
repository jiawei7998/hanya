package com.singlee.derivative.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;


/**
 * FDQVTR
 * 询价     
 * @author   YANGLIU
 * Create Time  2018-03-01 16:16:08
 */
/**
 * 询价交易表
 */
@Entity
@Table(name="ITS_FDQVTR")
public class FDQVTR implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5673855701376759250L;

	/**外键*/
	private String TRANID  ;
	/**主键*/
	private String  FDQVID  ;
	/**机构号*/
	private String BRCHNO  ;
	/**交易日期*/
	private String   DLDATE ;
	/**交易流水*/
	private String  RFERNO ;
	/**询价业务编号*/
	private String   FDQVNO  ;
	/**询价日期*/
	private String  FDQVDT  ;
	/**处理码*/
	private String LTPRID  ;
	/**合同号*/
	private String CRCONO  ;
	

	/**业务编号*/
	private String JSYQNO  ;
	/**币种*/
	private String   CUCYNO ;
	/**金额*/
	private String TRAMTS ;
	/**客户号*/
	private String  CUSTNO  ;
	/**客户名称*/
	private String   CUNAME;
	/**实际汇率*/
	private String  EXRATE ;
	/**询价汇率*/
	private String MYRATE ;
	/**备注*/
	private String  REMARK ;
	/**经办柜员*/
	private String  USERID ;
	/**经办日期*/
	private String  USERDT ;
	/**经办状态*/
	private String USERST ;
	/**复核柜员*/
	private String CHCKID ;
	/**经复核日期*/
	private String  CHCKDT ;
	/**复核状态*/
	private String CHCKST ;
	/**授权ID*/
	private String GRANID ;
	/**授权日期*/
	private String  GRANDT;
	/**授权状态*/
	private String  GRANST ;
	/**交易状态*/
	private String STATCD ;
	/**询价方向*/
	private String  FDQVTP ;
	/**结汇/售汇*/
	private String  JSYQTP ;
	/**询价类型*/
	private String JSXJTP ;
	/**远期汇率期限*/
	private String  TIMETP ;
	/**掉期平盘汇率*/
	private String  JDRATE ;
	/**近端实际汇率*/
	private String SPOTFR ;
	/**远端实际汇率*/
	private String  FORWFR ;
	/**近端対客汇率*/
	private String  SPRATE ;
	/**远端対客汇率*/
	private String FORATE ;
	/**合同汇率*/
	private String  CONTRT ;
	/**总行平盘期权费率*/
	private String  BANKFR ;
	/**总行平盘期权费*/
	private String  BANKPR ;
	/**对客期权费率*/
	private String CUSTFR ;
	/**对客期权费*/
	private String CUSTPR ;
	/**看涨、看跌*/
	private String  UPDOWN ;
	/**期权询价类型*/
	private String  JSCOTP ;
	/**产品名称*/
	private String PRNAME ;
	/**利率互换类型*/
	private String  LTISTP ;
	/**计息方式（付）*/
	private String FORMU1 ;
	/**固定/浮动利息（付）*/
	private String FTYPE1 ;
	/**利率（付）*/
	private String IRATE1 ;
	/**浮动利率基准曲线（付）*/
	private String ICURVE1 ;
	/**浮动利率周期（付）*/
	private String BAND1   ;
	/**天数偏移（付）*/
	private String  OFFSET1 ;
	/**利率点差(付)*/
	private String  IPIPS1 ;
	/**计息天数（付）*/
	private String  DAYCV1 ;
	/**付息频率*/
	private String PAFREQ ;
	/**利率重置频率（付）*/
	private String RTFRE1;
	/**计息方式（收）*/
	private String FORMU2;
	/**固定/浮动利息（收）*/
	private String  FTYPE2 ;
	/**利率（收）*/
	private String  IRATE2 ;
	/**浮动利率基准曲线（收）*/
	private String ICURVE2 ;
	/**浮动利率周期（收）*/
	private String BAND2 ;
	/**天数偏移（收）*/
	private String OFFSET2 ;
	/**利率点差(收)*/
	private String IPIPS2 ;
	/**计息天数（收）*/
	private String  DAYCV2 ;
	/**收息频率*/
	private String  REFREQ  ;
	/**利率重置频率（收）*/
	private String  RTFRE2 ;
	/***/
	private String  TRAMTS1 ;
	/**即期汇率*/
	private String JQRATE  ;
	/**询价类型：远期FDQV、掉期SPQV、期权OPQV、利率互换IRQV*/
	@Transient
	private String inqType;
	
	public String getInqType() {
		return inqType;
	}
	public void setInqType(String inqType) {
		this.inqType = inqType;
	}
	public String getTRANID() {
		return TRANID;
	}
	public void setTRANID(String tRANID) {
		TRANID = tRANID;
	}
	public String getFDQVID() {
		return FDQVID;
	}
	public void setFDQVID(String fDQVID) {
		FDQVID = fDQVID;
	}
	public String getBRCHNO() {
		return BRCHNO;
	}
	public void setBRCHNO(String bRCHNO) {
		BRCHNO = bRCHNO;
	}
	public String getDLDATE() {
		return DLDATE;
	}
	public void setDLDATE(String dLDATE) {
		DLDATE = dLDATE;
	}
	public String getRFERNO() {
		return RFERNO;
	}
	public void setRFERNO(String rFERNO) {
		RFERNO = rFERNO;
	}
	public String getFDQVNO() {
		return FDQVNO;
	}
	public void setFDQVNO(String fDQVNO) {
		FDQVNO = fDQVNO;
	}
	public String getFDQVDT() {
		return FDQVDT;
	}
	public void setFDQVDT(String fDQVDT) {
		FDQVDT = fDQVDT;
	}
	public String getLTPRID() {
		return LTPRID;
	}
	public void setLTPRID(String lTPRID) {
		LTPRID = lTPRID;
	}
	public String getJSYQNO() {
		return JSYQNO;
	}
	public void setJSYQNO(String jSYQNO) {
		JSYQNO = jSYQNO;
	}
	public String getCUCYNO() {
		return CUCYNO;
	}
	public void setCUCYNO(String cUCYNO) {
		CUCYNO = cUCYNO;
	}
	public String getTRAMTS() {
		return TRAMTS;
	}
	public void setTRAMTS(String tRAMTS) {
		TRAMTS = tRAMTS;
	}
	public String getCUSTNO() {
		return CUSTNO;
	}
	public void setCUSTNO(String cUSTNO) {
		CUSTNO = cUSTNO;
	}
	public String getCUNAME() {
		return CUNAME;
	}
	public void setCUNAME(String cUNAME) {
		CUNAME = cUNAME;
	}
	public String getEXRATE() {
		return EXRATE;
	}
	public void setEXRATE(String eXRATE) {
		EXRATE = eXRATE;
	}
	public String getMYRATE() {
		return MYRATE;
	}
	public void setMYRATE(String mYRATE) {
		MYRATE = mYRATE;
	}
	public String getREMARK() {
		return REMARK;
	}
	public void setREMARK(String rEMARK) {
		REMARK = rEMARK;
	}
	public String getUSERID() {
		return USERID;
	}
	public void setUSERID(String uSERID) {
		USERID = uSERID;
	}
	public String getUSERDT() {
		return USERDT;
	}
	public void setUSERDT(String uSERDT) {
		USERDT = uSERDT;
	}
	public String getUSERST() {
		return USERST;
	}
	public void setUSERST(String userSt) {
		USERST = userSt;
	}
	public String getCHCKID() {
		return CHCKID;
	}
	public void setCHCKID(String cHCKID) {
		CHCKID = cHCKID;
	}
	public String getCHCKDT() {
		return CHCKDT;
	}
	public void setCHCKDT(String cHCKDT) {
		CHCKDT = cHCKDT;
	}
	public String getCHCKST() {
		return CHCKST;
	}
	public void setCHCKST(String chckSt) {
		CHCKST = chckSt;
	}
	public String getGRANID() {
		return GRANID;
	}
	public void setGRANID(String gRANID) {
		GRANID = gRANID;
	}
	public String getGRANDT() {
		return GRANDT;
	}
	public void setGRANDT(String gRANDT) {
		GRANDT = gRANDT;
	}
	public String getGRANST() {
		return GRANST;
	}
	public void setGRANST(String granSt) {
		GRANST = granSt;
	}
	public String getSTATCD() {
		return STATCD;
	}
	public void setSTATCD(String statCd) {
		STATCD = statCd;
	}
	public String getFDQVTP() {
		return FDQVTP;
	}
	public void setFDQVTP(String fDQVTP) {
		FDQVTP = fDQVTP;
	}
	public String getJSYQTP() {
		return JSYQTP;
	}
	public void setJSYQTP(String jSYQTP) {
		JSYQTP = jSYQTP;
	}
	public String getJSXJTP() {
		return JSXJTP;
	}
	public void setJSXJTP(String jSXJTP) {
		JSXJTP = jSXJTP;
	}
	public String getTIMETP() {
		return TIMETP;
	}
	public void setTIMETP(String tIMETP) {
		TIMETP = tIMETP;
	}
	public String getJDRATE() {
		return JDRATE;
	}
	public void setJDRATE(String jDRATE) {
		JDRATE = jDRATE;
	}
	public String getSPOTFR() {
		return SPOTFR;
	}
	public void setSPOTFR(String sPOTFR) {
		SPOTFR = sPOTFR;
	}
	public String getFORWFR() {
		return FORWFR;
	}
	public void setFORWFR(String fORWFR) {
		FORWFR = fORWFR;
	}
	public String getSPRATE() {
		return SPRATE;
	}
	public void setSPRATE(String sPRATE) {
		SPRATE = sPRATE;
	}
	public String getFORATE() {
		return FORATE;
	}
	public void setFORATE(String fORATE) {
		FORATE = fORATE;
	}
	public String getCONTRT() {
		return CONTRT;
	}
	public void setCONTRT(String cONTRT) {
		CONTRT = cONTRT;
	}
	public String getBANKFR() {
		return BANKFR;
	}
	public void setBANKFR(String bANKFR) {
		BANKFR = bANKFR;
	}
	public String getBANKPR() {
		return BANKPR;
	}
	public void setBANKPR(String bANKPR) {
		BANKPR = bANKPR;
	}
	public String getCUSTFR() {
		return CUSTFR;
	}
	public void setCUSTFR(String cUSTFR) {
		CUSTFR = cUSTFR;
	}
	public String getCUSTPR() {
		return CUSTPR;
	}
	public void setCUSTPR(String cUSTPR) {
		CUSTPR = cUSTPR;
	}
	public String getUPDOWN() {
		return UPDOWN;
	}
	public void setUPDOWN(String uPDOWN) {
		UPDOWN = uPDOWN;
	}
	public String getJSCOTP() {
		return JSCOTP;
	}
	public void setJSCOTP(String jSCOTP) {
		JSCOTP = jSCOTP;
	}
	public String getPRNAME() {
		return PRNAME;
	}
	public void setPRNAME(String pRNAME) {
		PRNAME = pRNAME;
	}
	public String getLTISTP() {
		return LTISTP;
	}
	public void setLTISTP(String lTISTP) {
		LTISTP = lTISTP;
	}
	public String getFORMU1() {
		return FORMU1;
	}
	public void setFORMU1(String fORMU1) {
		FORMU1 = fORMU1;
	}
	public String getFTYPE1() {
		return FTYPE1;
	}
	public void setFTYPE1(String fTYPE1) {
		FTYPE1 = fTYPE1;
	}
	public String getIRATE1() {
		return IRATE1;
	}
	public void setIRATE1(String iRATE1) {
		IRATE1 = iRATE1;
	}
	public String getICURVE1() {
		return ICURVE1;
	}
	public void setICURVE1(String iCURVE1) {
		ICURVE1 = iCURVE1;
	}
	public String getBAND1() {
		return BAND1;
	}
	public void setBAND1(String bAND1) {
		BAND1 = bAND1;
	}
	public String getOFFSET1() {
		return OFFSET1;
	}
	public void setOFFSET1(String oFFSET1) {
		OFFSET1 = oFFSET1;
	}
	public String getIPIPS1() {
		return IPIPS1;
	}
	public void setIPIPS1(String iPIPS1) {
		IPIPS1 = iPIPS1;
	}
	public String getDAYCV1() {
		return DAYCV1;
	}
	public void setDAYCV1(String dAYCV1) {
		DAYCV1 = dAYCV1;
	}
	public String getPAFREQ() {
		return PAFREQ;
	}
	public void setPAFREQ(String pAFREQ) {
		PAFREQ = pAFREQ;
	}
	public String getRTFRE1() {
		return RTFRE1;
	}
	public void setRTFRE1(String rTFRE1) {
		RTFRE1 = rTFRE1;
	}
	public String getFORMU2() {
		return FORMU2;
	}
	public void setFORMU2(String fORMU2) {
		FORMU2 = fORMU2;
	}
	public String getFTYPE2() {
		return FTYPE2;
	}
	public void setFTYPE2(String fTYPE2) {
		FTYPE2 = fTYPE2;
	}
	public String getIRATE2() {
		return IRATE2;
	}
	public void setIRATE2(String iRATE2) {
		IRATE2 = iRATE2;
	}
	public String getICURVE2() {
		return ICURVE2;
	}
	public void setICURVE2(String iCURVE2) {
		ICURVE2 = iCURVE2;
	}
	public String getBAND2() {
		return BAND2;
	}
	public void setBAND2(String bAND2) {
		BAND2 = bAND2;
	}
	public String getOFFSET2() {
		return OFFSET2;
	}
	public void setOFFSET2(String oFFSET2) {
		OFFSET2 = oFFSET2;
	}
	public String getIPIPS2() {
		return IPIPS2;
	}
	public void setIPIPS2(String iPIPS2) {
		IPIPS2 = iPIPS2;
	}
	public String getDAYCV2() {
		return DAYCV2;
	}
	public void setDAYCV2(String dAYCV2) {
		DAYCV2 = dAYCV2;
	}
	public String getREFREQ() {
		return REFREQ;
	}
	public void setREFREQ(String rEFREQ) {
		REFREQ = rEFREQ;
	}
	public String getRTFRE2() {
		return RTFRE2;
	}
	public void setRTFRE2(String rTFRE2) {
		RTFRE2 = rTFRE2;
	}
	public String getTRAMTS1() {
		return TRAMTS1;
	}
	public void setTRAMTS1(String tRAMTS1) {
		TRAMTS1 = tRAMTS1;
	}
	public String getJQRATE() {
		return JQRATE;
	}
	public void setJQRATE(String jQRATE) {
		JQRATE = jQRATE;
	}
	
	public String getCRCONO() {
		return CRCONO;
	}
	public void setCRCONO(String cRCONO) {
		CRCONO = cRCONO;
	}
	
	


	
	 
	
}
