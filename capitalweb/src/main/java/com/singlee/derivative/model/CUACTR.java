package com.singlee.derivative.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * LTYQAD     
 * @author   YANGLIU
 * Create Time  2013-01-21 15:10:08
 */
/**
 * 登记
 */
@Entity
@Table(name="ITS_CUACTR")
public class CUACTR implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5673855701376759250L;
	@Id

	

	public String getTranId() {
		return tranId;
	}
	public void setTranId(String tranId) {
		this.tranId = tranId;
	}
	public String getUtchNo() {
		return utchNo;
	}
	public void setUtchNo(String utchNo) {
		this.utchNo = utchNo;
	}
	public String getInerNo() {
		return inerNo;
	}
	public void setInerNo(String inerNo) {
		this.inerNo = inerNo;
	}
	public String getBrchNo() {
		return brchNo;
	}
	public void setBrchNo(String brchNo) {
		this.brchNo = brchNo;
	}
	public String getCustNo() {
		return custNo;
	}
	public void setCustNo(String custNo) {
		this.custNo = custNo;
	}
	public String getAcType() {
		return acType;
	}
	public void setAcType(String acType) {
		this.acType = acType;
	}
	public String getCucyNo() {
		return cucyNo;
	}
	public void setCucyNo(String cucyNo) {
		this.cucyNo = cucyNo;
	}
	public String getCucyCd() {
		return cucyCd;
	}
	public void setCucyCd(String cucyCd) {
		this.cucyCd = cucyCd;
	}
	public String getItemNo() {
		return itemNo;
	}
	public void setItemNo(String itemNo) {
		this.itemNo = itemNo;
	}
	public String getItemCd() {
		return itemCd;
	}
	public void setItemCd(String itemCd) {
		this.itemCd = itemCd;
	}
	public String getAccoNo() {
		return accoNo;
	}
	public void setAccoNo(String accoNo) {
		this.accoNo = accoNo;
	}
	public String getCsExtg() {
		return csExtg;
	}
	public void setCsExtg(String csExtg) {
		this.csExtg = csExtg;
	}
	public String getAccoTp() {
		return accoTp;
	}
	public void setAccoTp(String accoTp) {
		this.accoTp = accoTp;
	}
	public String getStatCd() {
		return statCd;
	}
	public void setStatCd(String statCd) {
		this.statCd = statCd;
	}
	public String getCustMa() {
		return custMa;
	}
	public void setCustMa(String custMa) {
		this.custMa = custMa;
	}
	public String getAutoJh() {
		return autoJh;
	}
	public void setAutoJh(String autoJh) {
		this.autoJh = autoJh;
	}
	public String getJhAcco() {
		return jhAcco;
	}
	public void setJhAcco(String jhAcco) {
		this.jhAcco = jhAcco;
	}
	public String getJhTerm() {
		return jhTerm;
	}
	public void setJhTerm(String jhTerm) {
		this.jhTerm = jhTerm;
	}
	public String getTempCl() {
		return tempCl;
	}
	public void setTempCl(String tempCl) {
		this.tempCl = tempCl;
	}
	public String getKhFlag() {
		return khFlag;
	}
	public void setKhFlag(String khFlag) {
		this.khFlag = khFlag;
	}
	public String getKhType() {
		return khType;
	}
	public void setKhType(String khType) {
		this.khType = khType;
	}
	public String getKhPert() {
		return khPert;
	}
	public void setKhPert(String khPert) {
		this.khPert = khPert;
	}
	public String getKhCuno() {
		return khCuno;
	}
	public void setKhCuno(String khCuno) {
		this.khCuno = khCuno;
	}
	public String getKhAcco() {
		return khAcco;
	}
	public void setKhAcco(String khAcco) {
		this.khAcco = khAcco;
	}
	public String getStDate() {
		return stDate;
	}
	public void setStDate(String stDate) {
		this.stDate = stDate;
	}
	public String getSubSac() {
		return subSac;
	}
	public void setSubSac(String subSac) {
		this.subSac = subSac;
	}
	public String getAcctId() {
		return acctId;
	}
	public void setAcctId(String acctId) {
		this.acctId = acctId;
	}
	public String getCaamFg() {
		return caamFg;
	}
	public void setCaamFg(String caamFg) {
		this.caamFg = caamFg;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getYucyNo() {
		return yucyNo;
	}
	public void setYucyNo(String yucyNo) {
		this.yucyNo = yucyNo;
	}
	public String getMdcyNo() {
		return mdcyNo;
	}
	public void setMdcyNo(String mdcyNo) {
		this.mdcyNo = mdcyNo;
	}
	

	/**id*/
	private String tranId;
	/**单证中心*/
	private String utchNo;
	/**辖内号*/
	private String inerNo;
	/**机构号*/
	private String brchNo;
	/**客户号*/
	private String custNo;
	/**帐号类型*/
	private String acType;
	/**币种编号*/
	private String cucyNo;
	/**币种缩写*/
	private String cucyCd;
	/**科目*/
	private String itemNo;
	
	/**科目代码*/
	private String itemCd;
	/**帐号*/
	private String accoNo;
	/**钞汇标志*/
	private String csExtg;
	/**帐号类型*/
	private String accoTp;
	/**状态*/
	private String statCd;
	/**客户经理*/
	private String custMa;
	/**是否见汇即结*/
	private String autoJh;
	
	/**见汇即结入账的人民币帐号*/
	private String jhAcco;
	/**见汇即结期限*/
	private String jhTerm;
	/**临时客户*/
	private String tempCl;
	/**是否需要收镇府结
	 * 汇费用是否需要收镇府结汇费用*/
	private String khFlag ;
	/**扣划种类*/
	private String khType;
	/**扣划金额/比例（
	 * 万分之一）*/
	private String khPert;
	/**镇府扣划客户号*/
	private String khCuno;
	/**镇府的账号*/
	private String khAcco;
	
	/**启用日期*/
	private String stDate;
	/**子户号*/
	private String subSac;
	/**账户ID*/
	private String acctId;
	/**免收内扣手续费标志*/
	private String caamFg;
	/**原币种*/
	private String yucyNo ;
	


	/**目的币种*/
	private String mdcyNo;
	
	 
	
}
