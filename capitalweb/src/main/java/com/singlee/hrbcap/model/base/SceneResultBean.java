package com.singlee.hrbcap.model.base;

import java.io.Serializable;

/**
 * 场景Bean
 * @author 
 *
 */
public class SceneResultBean implements Serializable{
	private static final long serialVersionUID = 1L;

	private String groupId ;
	private String prdNo ;
	private String prdName ;
	private String eventId ;
	private String eventName ;
	private String dimensionId ;
	private String dimensionName ;
	private String dimensionValue ;
	private String dimensionDesc ;
	private String dimensionIdValue ;
	private String dbId ;
	private String dimensionKeys ;
	private String dimensionValues ;
	private String dimensionDescs ;
	public String getGroupId() {
		return groupId;
	}
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	public String getPrdNo() {
		return prdNo;
	}
	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}
	public String getPrdName() {
		return prdName;
	}
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	public String getEventId() {
		return eventId;
	}
	public void setEventId(String eventId) {
		this.eventId = eventId;
	}
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public String getDimensionId() {
		return dimensionId;
	}
	public void setDimensionId(String dimensionId) {
		this.dimensionId = dimensionId;
	}
	public String getDimensionValue() {
		return dimensionValue;
	}
	public void setDimensionValue(String dimensionValue) {
		this.dimensionValue = dimensionValue;
	}
	public String getDimensionIdValue() {
		return dimensionIdValue;
	}
	public void setDimensionIdValue(String dimensionIdValue) {
		this.dimensionIdValue = dimensionIdValue;
	}
	public String getDimensionKeys() {
		return dimensionKeys;
	}
	public void setDimensionKeys(String dimensionKeys) {
		this.dimensionKeys = dimensionKeys;
	}
	public String getDimensionValues() {
		return dimensionValues;
	}
	public void setDimensionValues(String dimensionValues) {
		this.dimensionValues = dimensionValues;
	}
	public String getDimensionName() {
		return dimensionName;
	}
	public void setDimensionName(String dimensionName) {
		this.dimensionName = dimensionName;
	}
	public String getDimensionDesc() {
		return dimensionDesc;
	}
	public void setDimensionDesc(String dimensionDesc) {
		this.dimensionDesc = dimensionDesc;
	}
	public String getDimensionDescs() {
		return dimensionDescs;
	}
	public void setDimensionDescs(String dimensionDescs) {
		this.dimensionDescs = dimensionDescs;
	}
	public String getDbId() {
		return dbId;
	}
	public void setDbId(String dbId) {
		this.dbId = dbId;
	}
	@Override
	public String toString() {
		return "SceneResultBean [groupId=" + groupId + ", prdNo=" + prdNo
				+ ", prdName=" + prdName + ", eventId=" + eventId
				+ ", eventName=" + eventName + ", dimensionId=" + dimensionId
				+ ", dimensionName=" + dimensionName + ", dimensionValue="
				+ dimensionValue + ", dimensionDesc=" + dimensionDesc
				+ ", dimensionIdValue=" + dimensionIdValue + ", dbId=" + dbId
				+ ", dimensionKeys=" + dimensionKeys + ", dimensionValues="
				+ dimensionValues + ", dimensionDescs=" + dimensionDescs + "]";
	}
	
	
	

	
}
