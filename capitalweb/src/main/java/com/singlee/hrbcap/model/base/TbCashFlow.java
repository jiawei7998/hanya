package com.singlee.hrbcap.model.base;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name="TB_CASH_FLOW")
public class TbCashFlow implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * 流水号
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT TO_CHAR(SYSDATE,'YYYYMMDD')||LPAD(SEQ_CASH_FLOW.NEXTVAL,8,'0') FROM DUAL")
    private String sn;

    /**
     * 交易单号
     */
    private String dealNo;

    /**
     * 现金流类型
     */
    private String cftype;

    /**
     * 金额
     */
    private BigDecimal amt;

    /**
     * 利息
     */
    private BigDecimal rpi;

    /**
     * 总金额
     */
    private BigDecimal tamt;

    /**
     * 发生日期
     */
    private String occudate;

    /**
     * 开始日期
     */
    private String startdate;

    /**
     * 结束日期
     */
    private String enddate;

    /**
     * 操作时间
     */
    private String opertime;
    /**
     * 发起机构
     */
    private String sponinst;
    //交易对手
    private String cust;
    //交易对手名称
    private String cname;
    //基金代码
    private String fundCode;
    //基金名称
    private String fundName;
    //交易日期
    private String tdate;

    public String getTdate() {
        return tdate;
    }

    public void setTdate(String tdate) {
        this.tdate = tdate;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public String getCust() {
        return cust;
    }

    public void setCust(String cust) {
        this.cust = cust;
    }

    public String getFundCode() {
        return fundCode;
    }

    public void setFundCode(String fundCode) {
        this.fundCode = fundCode;
    }

    public String getFundName() {
        return fundName;
    }

    public void setFundName(String fundName) {
        this.fundName = fundName;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getDealNo() {
        return dealNo;
    }

    public void setDealNo(String dealNo) {
        this.dealNo = dealNo;
    }

    public String getCftype() {
        return cftype;
    }

    public void setCftype(String cftype) {
        this.cftype = cftype;
    }

    public BigDecimal getAmt() {
        return amt;
    }

    public void setAmt(BigDecimal amt) {
        this.amt = amt;
    }

    public BigDecimal getRpi() {
        return rpi;
    }

    public void setRpi(BigDecimal rpi) {
        this.rpi = rpi;
    }

    public BigDecimal getTamt() {
        return tamt;
    }

    public void setTamt(BigDecimal tamt) {
        this.tamt = tamt;
    }

    public String getOccudate() {
        return occudate;
    }

    public void setOccudate(String occudate) {
        this.occudate = occudate;
    }

    public String getStartdate() {
        return startdate;
    }

    public void setStartdate(String startdate) {
        this.startdate = startdate;
    }

    public String getEnddate() {
        return enddate;
    }

    public void setEnddate(String enddate) {
        this.enddate = enddate;
    }

    public String getOpertime() {
        return opertime;
    }

    public void setOpertime(String opertime) {
        this.opertime = opertime;
    }

    public String getSponinst() {
        return sponinst;
    }

    public void setSponinst(String sponinst) {
        this.sponinst = sponinst;
    }
}
