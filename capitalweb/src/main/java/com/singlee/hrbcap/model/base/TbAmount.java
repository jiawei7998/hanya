package com.singlee.hrbcap.model.base;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/*
 * 金额指针表
 */
@Entity
@Table(name = "TB_AMOUNT")
public class TbAmount implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	/**
	 * 金额代码
	 */
	private String amountId;
	/**
	 * 金额名称
	 */
	private String amountName;
	/**
	 * 金额公式
	 */
	private String amountFormula;
	/**
	 * 金额描述
	 */
	private String amountDesc;

	public String getAmountId() {
		return amountId;
	}

	public void setAmountId(String amountId) {
		this.amountId = amountId;
	}

	public String getAmountName() {
		return amountName;
	}

	public void setAmountName(String amountName) {
		this.amountName = amountName;
	}

	public String getAmountFormula() {
		return amountFormula;
	}

	public void setAmountFormula(String amountFormula) {
		this.amountFormula = amountFormula;
	}

	public String getAmountDesc() {
		return amountDesc;
	}

	public void setAmountDesc(String amountDesc) {
		this.amountDesc = amountDesc;
	}

}
