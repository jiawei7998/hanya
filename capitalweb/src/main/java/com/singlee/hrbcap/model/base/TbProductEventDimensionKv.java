package com.singlee.hrbcap.model.base;

import java.io.Serializable;

/**
 * 产品事件 与 维度取值关联
 * @author louhuanqing
 *
 */
public class TbProductEventDimensionKv implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private  String groupId;
	/**
	 * 产品代码
	 */
	private String prdNo;
	/**
	 * 事件代码
	 */
	private String eventId;
	/**
	 * 维度键
	 */
	private String dimensionId;
	/**
	 * 维度值
	 */
	private String dimensionValue;
	/**
	 * 维度值id
	 */
	private String dimensionValueId;

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getPrdNo() {
		return prdNo;
	}

	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}

	public String getEventId() {
		return eventId;
	}

	public void setEventId(String eventId) {
		this.eventId = eventId;
	}

	public String getDimensionId() {
		return dimensionId;
	}

	public void setDimensionId(String dimensionId) {
		this.dimensionId = dimensionId;
	}

	public String getDimensionValue() {
		return dimensionValue;
	}

	public void setDimensionValue(String dimensionValue) {
		this.dimensionValue = dimensionValue;
	}

	public String getDimensionValueId() {
		return dimensionValueId;
	}

	public void setDimensionValueId(String dimensionValueId) {
		this.dimensionValueId = dimensionValueId;
	}

	@Override
	public String toString() {
		return "TbProductEventDimensionKv [groupId=" + groupId + ", prdNo="
				+ prdNo + ", eventId=" + eventId + ", dimensionId="
				+ dimensionId + ", dimensionValue=" + dimensionValue
				+ ", dimensionValueId=" + dimensionValueId + "]";
	}

	
	
	
}
