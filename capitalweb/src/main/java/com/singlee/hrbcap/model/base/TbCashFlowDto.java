package com.singlee.hrbcap.model.base;

import java.io.Serializable;

/**
 * @author copysun
 */
public class TbCashFlowDto extends TbCashFlow implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;


    /**
     * 对方帐号
     */
    private String partyAccCode;

    /**
     * 对方帐号名称
     */
    private String partyAccName;

    /**
     * 对方开户行行号
     */
    private String partyBankCode;

    public String getPartyAccCode() {
        return partyAccCode;
    }

    public void setPartyAccCode(String partyAccCode) {
        this.partyAccCode = partyAccCode;
    }

    public String getPartyAccName() {
        return partyAccName;
    }

    public void setPartyAccName(String partyAccName) {
        this.partyAccName = partyAccName;
    }

    public String getPartyBankCode() {
        return partyBankCode;
    }

    public void setPartyBankCode(String partyBankCode) {
        this.partyBankCode = partyBankCode;
    }


}
