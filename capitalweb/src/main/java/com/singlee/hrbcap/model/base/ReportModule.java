package com.singlee.hrbcap.model.base;

import java.util.List;

public class ReportModule {
	private String id;
	private String text;
	private List<ReportModule> children;
	private String pid;
	private String type;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public List<ReportModule> getChildren() {
		return children;
	}
	public void setChildren(List<ReportModule> children) {
		this.children = children;
	}
	public String getPid() {
		return pid;
	}
	public void setPid(String pid) {
		this.pid = pid;
	}
	@Override
	public String toString() {
		return "ReportModule [id=" + id + ", text=" + text + ", children="
				+ children + ", pid=" + pid + "]";
	}
	

}
