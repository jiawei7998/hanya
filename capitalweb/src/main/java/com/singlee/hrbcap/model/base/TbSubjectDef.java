package com.singlee.hrbcap.model.base;

import com.singlee.capital.common.util.TreeInterface;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.List;

/*
 * 科目表
 */
@Entity
@Table(name = "TB_SUBJECT_DEF")
public class TbSubjectDef implements Serializable, TreeInterface {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3501925335597732909L;
	/*
	 * 所属账套id
	 */
	private String taskId;
	/**
	 * 所属账套名称
	 */
	@Transient
	private String taskName;
	/*
	 * 科目号
	 */
	@Id
	private String subjCode;
	/*
	 * 科目名称
	 */
	private String subjName;
	/*
	 * 上级科目号
	 */
	private String parentSubjCode;
	/**
	 * 上级科目名称
	 */
	@Transient
	private String parentSubjName;

	/*
	 * 科目类型
	 */
	private String subjType;
	/*
	 * 科目性质
	 */
	private String subjNature;
	/*
	 * 余额借贷方向
	 */
	private String debitCredit;
	/*
	 * 余额收付方向
	 */
	private String payRecive;
	/*
	 * 收支方向[收入/支出],损益类科目必选
	 */
	private String direction;
	
	/**
	 * 数据来源 1本地 2接口
	 */
	private String source;
	

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}
	@Transient
	private List<TbSubjectDef> children;

	
	public List<TbSubjectDef> getChildren() {
		return children;
	}

	public void setChildren(List<TbSubjectDef> children) {
		this.children = children;
	}

	public String getParentSubjName() {
		return parentSubjName;
	}

	public void setParentSubjName(String parentSubjName) {
		this.parentSubjName = parentSubjName;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getSubjCode() {
		return subjCode;
	}

	public void setSubjCode(String subjCode) {
		this.subjCode = subjCode;
	}

	public String getSubjName() {
		return subjName;
	}

	public void setSubjName(String subjName) {
		this.subjName = subjName;
	}

	public String getParentSubjCode() {
		return parentSubjCode;
	}

	public void setParentSubjCode(String parentSubjCode) {
		this.parentSubjCode = parentSubjCode;
	}

	public String getSubjType() {
		return subjType;
	}

	public void setSubjType(String subjType) {
		this.subjType = subjType;
	}

	public String getDebitCredit() {
		return debitCredit;
	}

	public void setDebitCredit(String debitCredit) {
		this.debitCredit = debitCredit;
	}

	public String getPayRecive() {
		return payRecive;
	}

	public void setPayRecive(String payRecive) {
		this.payRecive = payRecive;
	}

	public String getSubjNature() {
		return subjNature;
	}

	public void setSubjNature(String subjNature) {
		this.subjNature = subjNature;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	@Override
	public String getId() {
		return getSubjCode();
	}

	@Override
	public String getParentId() {
		return getParentSubjCode();
	}

	@Override
	@SuppressWarnings("unchecked")
	public void children(List<? extends TreeInterface> list) {
		setChildren((List<TbSubjectDef>) list);
	}

	@Override
	public List<? extends TreeInterface> children() {
		return getChildren();
	}

	@Override
	public String getText() {
		return subjName;
	}

	@Override
	public String getValue() {
		return subjCode;
	}

	@Override
	public int getSort() {
		return StringUtils.length(getSubjCode());
	}
	/**
	 * 暂时未返回图标
	 */
	@Override
	public String getIconCls() {
		return null;
	}

}
