package com.singlee.hrbcap.model.base;

import java.io.Serializable;

/**
 * 产品事件维度取值关系组 对应的分录表
 * @author louhuanqing
 *
 */
public class TbProductEventSceneAcup implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 产品事件维度取值组
	 */
	private String groupId;
	/**
	 * 借贷关系
	 */
	private String drcrInd;
	/**
	 * 科目号
	 */
	private String subject;
	/**
	 * 科目描述
	 */
	private String subjDesc;
	/**
	 * 金额表达式
	 */
	private String subjAmt;
	/**
	 * 科目转换
	 */
	private String subjTrans;
	/**
	 * 科目序号
	 */
	private int subjSeq;
	/**
	 * 科目序列2
	 */
	private String subjCom;

	public String getSubjCom() {
		return subjCom;
	}

	public void setSubjCom(String subjCom) {
		this.subjCom = subjCom;
	}

	public int getSubjSeq() {
		return subjSeq;
	}
	public void setSubjSeq(int subjSeq) {
		this.subjSeq = subjSeq;
	}
	public String getGroupId() {
		return groupId;
	}
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	public String getDrcrInd() {
		return drcrInd;
	}
	public void setDrcrInd(String drcrInd) {
		this.drcrInd = drcrInd;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getSubjDesc() {
		return subjDesc;
	}
	public void setSubjDesc(String subjDesc) {
		this.subjDesc = subjDesc;
	}
	public String getSubjAmt() {
		return subjAmt;
	}
	public void setSubjAmt(String subjAmt) {
		this.subjAmt = subjAmt;
	}
	public String getSubjTrans() {
		return subjTrans;
	}
	public void setSubjTrans(String subjTrans) {
		this.subjTrans = subjTrans;
	}
	@Override
	public String toString() {
		return "TbProductEventSceneAcup [groupId=" + groupId + ", drcrInd=" + drcrInd + ", subject=" + subject
				+ ", subjDesc=" + subjDesc + ", subjAmt=" + subjAmt + ", subjTrans=" + subjTrans + "]";
	}
	
}
