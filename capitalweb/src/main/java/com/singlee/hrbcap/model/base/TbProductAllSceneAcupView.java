package com.singlee.hrbcap.model.base;

import java.io.Serializable;

/**
 * 产品事件维度取值关系组 对应的分录表
 * @author louhuanqing
 *
 */
public class TbProductAllSceneAcupView implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 产品事件维度取值组
	 */
	private String groupId;
	
	private String prdNo;
	
	private String prdName;
	
	private String eventId;
	
	private String eventName;
	
	private String dimensionKeys;
	
	private String dimensionValues;
	
	private String dimensionDescs;
	
	/**
	 * 借贷关系
	 */
	private String drcrInd;
	/**
	 * 科目号
	 */
	private String subject;
	/**
	 * 科目描述
	 */
	private String subjDesc;
	/**
	 * 金额表达式
	 */
	private String subjAmt;
	/**
	 * 科目序号
	 */
	private int subjSeq;

	private String subjTrans;

	private String subjCom;

	public String getSubjCom() {
		return subjCom;
	}

	public void setSubjCom(String subjCom) {
		this.subjCom = subjCom;
	}

	public int getSubjSeq() {
		return subjSeq;
	}
	public void setSubjSeq(int subjSeq) {
		this.subjSeq = subjSeq;
	}
	public String getGroupId() {
		return groupId;
	}
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	public String getDrcrInd() {
		return drcrInd;
	}
	public void setDrcrInd(String drcrInd) {
		this.drcrInd = drcrInd;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getSubjDesc() {
		return subjDesc;
	}
	public void setSubjDesc(String subjDesc) {
		this.subjDesc = subjDesc;
	}
	public String getSubjAmt() {
		return subjAmt;
	}
	public void setSubjAmt(String subjAmt) {
		this.subjAmt = subjAmt;
	}
	public String getPrdNo() {
		return prdNo;
	}
	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}
	public String getPrdName() {
		return prdName;
	}
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	public String getEventId() {
		return eventId;
	}
	public void setEventId(String eventId) {
		this.eventId = eventId;
	}
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public String getDimensionKeys() {
		return dimensionKeys;
	}
	public void setDimensionKeys(String dimensionKeys) {
		this.dimensionKeys = dimensionKeys;
	}
	public String getDimensionValues() {
		return dimensionValues;
	}
	public void setDimensionValues(String dimensionValues) {
		this.dimensionValues = dimensionValues;
	}
	public String getDimensionDescs() {
		return dimensionDescs;
	}
	public void setDimensionDescs(String dimensionDescs) {
		this.dimensionDescs = dimensionDescs;
	}

	public String getSubjTrans() {
		return subjTrans;
	}

	public void setSubjTrans(String subjTrans) {
		this.subjTrans = subjTrans;
	}
}
