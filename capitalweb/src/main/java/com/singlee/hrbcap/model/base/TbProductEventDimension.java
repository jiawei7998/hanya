package com.singlee.hrbcap.model.base;

import javax.persistence.Id;
import java.io.Serializable;

/**
 * 产品事件 与 维度的关联
 * @author louhuanqing
 *
 */
public class TbProductEventDimension implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 产品代码
	 */
	@Id
	private String prdNo;
	/**
	 * 事件代码
	 */
	@Id
	private String eventId;
	/**
	 * 维度键
	 */
	@Id
	private String dimensionId;
	
	public String getPrdNo() {
		return prdNo;
	}

	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}

	public String getEventId() {
		return eventId;
	}

	public void setEventId(String eventId) {
		this.eventId = eventId;
	}

	public String getDimensionId() {
		return dimensionId;
	}

	public void setDimensionId(String dimensionId) {
		this.dimensionId = dimensionId;
	}

	@Override
	public String toString() {
		return "TbProductEventDimension [prdNo=" + prdNo + ", eventId=" + eventId + ", dimensionId=" + dimensionId
				+ "]";
	}

	
}
