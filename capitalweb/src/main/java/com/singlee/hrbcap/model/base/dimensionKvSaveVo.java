package com.singlee.hrbcap.model.base;

import java.io.Serializable;
import java.util.List;

/**
 * 维度值树保存对象
 * @author 
 *
 */
public class dimensionKvSaveVo implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String prdNo ;
	private String eventId ;
	private List<TbDimensionValue> dimensionKvList;//菜单数组
	private List<SceneResultBean> sceneResultBeanList;
	public String getPrdNo() {
		return prdNo;
	}
	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}
	public String getEventId() {
		return eventId;
	}
	public void setEventId(String eventId) {
		this.eventId = eventId;
	}
	public List<TbDimensionValue> getDimensionKvList() {
		return dimensionKvList;
	}
	public void setDimensionKvList(List<TbDimensionValue> dimensionKvList) {
		this.dimensionKvList = dimensionKvList;
	}
	public List<SceneResultBean> getSceneResultBeanList() {
		return sceneResultBeanList;
	}
	public void setSceneResultBeanList(List<SceneResultBean> sceneResultBeanList) {
		this.sceneResultBeanList = sceneResultBeanList;
	}

	

	

	
}
