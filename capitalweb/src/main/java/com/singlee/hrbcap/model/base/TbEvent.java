package com.singlee.hrbcap.model.base;

import javax.persistence.Id;
import java.io.Serializable;

/**
 * TB_EVENT 会计事件表
 * @author louhuanqing
 *
 */
public class TbEvent implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 会计事件ID
	 */
	@Id
	private String eventId;
	/**
	 * 事件名称
	 */
	private String eventName;
	/**
	 * 事件描述
	 */
	private String eventDesc;
	public String getEventId() {
		return eventId;
	}
	public void setEventId(String eventId) {
		this.eventId = eventId;
	}
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public String getEventDesc() {
		return eventDesc;
	}
	public void setEventDesc(String eventDesc) {
		this.eventDesc = eventDesc;
	}
	@Override
	public String toString() {
		return "TbEvent [eventId=" + eventId + ", eventName=" + eventName + ", eventDesc=" + eventDesc + "]";
	}
	
	
}
