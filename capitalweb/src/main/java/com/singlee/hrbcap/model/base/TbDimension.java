package com.singlee.hrbcap.model.base;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * 会计维度表
 * @author louhuanqing
 *
 */
public class TbDimension implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT SEQ_TB_DiMENSION.NEXTVAL FROM DUAL")
	private String dbId;
	/**
	 * 会计维度ID
	 */
	private String dimensionId;
	/**
	 * 会计维度名称
	 */
	private String dimensionName;
	/**
	 * 会计维度排序
	 */
	private String dimensionSort;
	/**
	 * 是否可用
	 */
	private String isActive;
	
	/**
	 * 备注
	 * @return
	 */
	private String remark;
	
	public String getDbId() {
		return dbId;
	}
	public void setDbId(String dbId) {
		this.dbId = dbId;
	}
	public String getDimensionId() {
		return dimensionId;
	}
	public void setDimensionId(String dimensionId) {
		this.dimensionId = dimensionId;
	}
	public String getDimensionName() {
		return dimensionName;
	}
	public void setDimensionName(String dimensionName) {
		this.dimensionName = dimensionName;
	}
	public String getDimensionSort() {
		return dimensionSort;
	}
	public void setDimensionSort(String dimensionSort) {
		this.dimensionSort = dimensionSort;
	}
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	@Override
	public String toString() {
		return "TbdDimension [dbId=" + dbId + ", dimensionId=" + dimensionId + ", dimensionName=" + dimensionName
				+ ", dimensionSort=" + dimensionSort + ", isActive=" + isActive + "]";
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}

	
}
