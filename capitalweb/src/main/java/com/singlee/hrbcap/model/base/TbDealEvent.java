package com.singlee.hrbcap.model.base;

import java.io.Serializable;

public class TbDealEvent implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1000011111L;

	/**
     * 交易单号
     */
    private String dealNo;

    /**
     * 事件id
     */
    private String eventId;

    /**
     * 保本类型
     */
    private String preservationType;

    /**
     * 投资类型
     */
    private String acType;

    /**
     * 产品类型
     */
    private String prdNo;

    /**
     * 出账状态 1-出账成功 0-未出账 
     */
    private String accStatus;

    /**
     * 业务日期
     */
    private String postdate;

    /**
     * 交易单号
     * @return DEAL_NO 交易单号
     */
    public String getDealNo() {
        return dealNo;
    }

    /**
     * 交易单号
     * @param dealNo 交易单号
     */
    public void setDealNo(String dealNo) {
        this.dealNo = dealNo == null ? null : dealNo.trim();
    }

    /**
     * 事件id
     * @return EVENT_ID 事件id
     */
    public String getEventId() {
        return eventId;
    }

    /**
     * 事件id
     * @param eventId 事件id
     */
    public void setEventId(String eventId) {
        this.eventId = eventId == null ? null : eventId.trim();
    }

    /**
     * 保本类型
     * @return PRESERVATION_TYPE 保本类型
     */
    public String getPreservationType() {
        return preservationType;
    }

    /**
     * 保本类型
     * @param preservationType 保本类型
     */
    public void setPreservationType(String preservationType) {
        this.preservationType = preservationType == null ? null : preservationType.trim();
    }

    /**
     * 投资类型
     * @return AC_TYPE 投资类型
     */
    public String getAcType() {
        return acType;
    }

    /**
     * 投资类型
     * @param acType 投资类型
     */
    public void setAcType(String acType) {
        this.acType = acType == null ? null : acType.trim();
    }

    /**
     * 产品类型
     * @return PRD_NO 产品类型
     */
    public String getPrdNo() {
        return prdNo;
    }

    /**
     * 产品类型
     * @param prdNo 产品类型
     */
    public void setPrdNo(String prdNo) {
        this.prdNo = prdNo == null ? null : prdNo.trim();
    }

    /**
     * 出账状态 1-出账成功 0-未出账 
     * @return ACC_STATUS 出账状态 1-出账成功 0-未出账 
     */
    public String getAccStatus() {
        return accStatus;
    }

    /**
     * 出账状态 1-出账成功 0-未出账 
     * @param accStatus 出账状态 1-出账成功 0-未出账 
     */
    public void setAccStatus(String accStatus) {
        this.accStatus = accStatus == null ? null : accStatus.trim();
    }

    /**
     * 业务日期
     * @return POSTDATE 业务日期
     */
    public String getPostdate() {
        return postdate;
    }

    /**
     * 业务日期
     * @param postdate 业务日期
     */
    public void setPostdate(String postdate) {
        this.postdate = postdate == null ? null : postdate.trim();
    }
}