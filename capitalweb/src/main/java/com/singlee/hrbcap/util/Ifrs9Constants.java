package com.singlee.hrbcap.util;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * 会计事件代码（2018）
 * 会计入口设定
 * 判定多冲少补
 * @author Administrator
 *
 */
public class Ifrs9Constants {
	
	/* 计息基础 */
	public static final String A360 = "A360";
	public static final String A365 = "A365";
	/**
	 * 基金计算FTP成本计息基础
	 */
	public static final BigDecimal FTP365 = new BigDecimal("365");
	/* 核算方式 */
	public static final String ACTYPE1 = "1"; //摊余方式
	public static final String ACTYPE2 = "2"; //公允方式
	
	public static final String CCY1 = "ccy";  //币种代码1
	
	public static final String CNY = "CNY";  //人民币
	
	public static final String CNYPRODUCT = "1210";  //本币产品
	
	/**
	 * 会计事件2018
	 * @author Administrator
	 *
	 */
	public final static class Ifrs9EventCode{
		
		
		/**
		 * 起息日分录
		 */
		public static final String Event_VDATE = "VDATE";
		/**
		 * 确认分录
		 */
		public static final String Event_VERIFY = "VERIFY";
		/**
		 * 计提分录
		 */
		public static final String Event_DINT = "DINT";
		/**
		 * 收付息分录
		 */
		public static final String Event_RINT = "RINT";
		/**
		 * 计还本分录
		 */
		public static final String Event_RAMT = "RAMT";
		/**
		 * 资产转让分录
		 */
		public static final String Event_SDATE = "SDATE";
		/**
		 * 交易结清分录
		 */
		public static final String Event_SETT = "SETT";
		/**
		 * 每日估值分录
		 */
		public static final String Event_ASSMT = "ASSMT";
		
		/**
		 * 到期后收息分录
		 */
		public static final String Event_EINT = "EINT";
		/**
		 * 到期日分录
		 */
		public static final String Event_MDATE = "MDATE";
		/**
		 * 资产转让分录
		 */
		public static final String Event_RESALE = "RESALE";
		/**
		 * 资产转让收息分录
		 */
		public static final String Event_RESALEINT = "RESALEINT";
		/**
		 * 收息确认分录
		 */
		public static final String Event_VERIFYINT = "VERIFYINT";
		/**
		 * 到期确认分录
		 */
		public static final String Event_VERIFYMINT = "VERIFYMINT";
		/**
		 * 分红
		 */
		public static final String Event_BOND = "BOND";
		/**
		 * 红利再投分录
		 */
		public static final String Event_DRT = "DRT";
		/***
		 * 放款审批
		 * 基金放款，投资业务放款
		 */
		public static final String Event_0101 = "0101";
		/***
		 * 红利再投
		 */
		public static final String Event_0102 = "0102";
		/***
		 * 现金分红
		 */
		public static final String Event_0103 = "0103";
		/***
		 * 赎回-来账分拣
		 */
		public static final String Event_0104 = "0104";
		
		/**
		 * 来账分拣
		 */
		public static final String Event_0105 = "0105";
		
		/**
		 * 外币投资
		 */
		public static final String Event_0106 = "0106";
		/**
		 * 每日计提
		 */
		public static final String Event_0201 = "0201";
		/**
		 * 每日摊销
		 */
		public static final String Event_0202 = "0202";
		/**
		 * 续存本金
		 */
		public static final String Event_0204 = "0204";
		/**
		 * 提前还款
		 */
		public static final String Event_0301 = "0301";
		/**
		 * 利率调整
		 */
		public static final String Event_0501 = "0501";
		
		/**
		 * 逾期确认
		 */
		public static final String Event_0401 = "0401";
		/**
		 * 逾期计提
		 */
		public static final String Event_0402 = "0402";
		/**
		 * 逾期90天
		 */
		public static final String Event_0403 = "0403";
		/**
		 * 逾期来账
		 */
		public static final String Event_0404 = "0404";
		/**
		 * 预结息
		 */
		public static final String Event_0701 = "0701";
		/**
		 * 结息确认
		 */
		public static final String Event_0702 = "0702";
		
		/**
		 * 费用每日计提
		 */
		public static final String Event_0801 = "0801";
		/**
		 * 费用每日摊销
		 */
		public static final String Event_0802 = "0802";
		
		/**
		 * 中收费用来账分拣
		 */
		public static final String Event_0803 = "0803";
		/**
		 * 0109	申购确认
		 */
		public static final String Event_0109 = "0109";
		/**
		 *基金估值 
		 */
		public static final String Event_0107 = "0107";
		
	}
	
	/**收息重定分录类型*/
	public static final String EVENTARR= Ifrs9EventCode.Event_MDATE+","+ Ifrs9EventCode.Event_RINT+","+ Ifrs9EventCode.Event_VERIFYINT+","+ Ifrs9EventCode.Event_RESALE;
	
	public static final String EVENTMARR= Ifrs9EventCode.Event_MDATE;
	
	public static final String EVENTRARR= Ifrs9EventCode.Event_RINT+","+ Ifrs9EventCode.Event_VERIFYINT;

	public final static class CheckType{
		public static final String AMT = "amt";//本金
		public static final String RPI = "rpi";//利息
	}
	
	public final static class AcupType{
		public static final String AMTCHECK = "1";//本金
		public static final String RPICHECK = "2";//利息
		public static final String AMTRPICHECK = "3";//本息已核对
		public static final String NONECHECK = "0";//本息均未核对
	}
	
	public static Map<String,String> amtNameMap=new HashMap<String,String>();
	static{
		amtNameMap.put("amt_IntAdj", "利息调整");
		amtNameMap.put("am_IntAdj", "提前还款后收息应收息调整");
		amtNameMap.put("reval_fund_amt", "净值型产品估值");
		amtNameMap.put("adj_ByRateChage_Interest", "利率变更引起的调整");
	}
	
	/**
	 * 会计引擎
	 * @author Administrator
	 *
	 */
	public final static class AcupEngine{
		/**
		 * 并行处理
		 */
		public static final String ALL = "2";//全部
		/**
		 * IFRS9
		 */
		public static final String IFRS = "1";//I9
		/**
		 * IAS39四分类
		 */
		public static final String IASC = "0";//四分类
	}
	
	
	public final static class InvType3{
		public static final String AC = "1";
		public static final String OCI = "2";
		public static final String PL = "3";
	}
	
	/**
	 * 科目事件分类
	 */
	public final static class Event{
		//计提分录
		public static final String[] EVENTINT = {Ifrs9EventCode.Event_DINT, Ifrs9EventCode.Event_VERIFYINT};
		//收息分录
		public static final String[] EVENTRINT = {Ifrs9EventCode.Event_RINT, Ifrs9EventCode.Event_MDATE,
				Ifrs9EventCode.Event_RESALEINT, Ifrs9EventCode.Event_RESALE, Ifrs9EventCode.Event_BOND};
	}
	/**
	 * 现金流类型
	 * @author guomeijuan
	 *
	 */
	public final static class CashFlow{
		
		/**
		 * 起息日金额
		 */
		public static final String VDATE = "1";
		
		/**
		 * 收息日金额
		 */
		public static final String RPI = "2";
		
		/**
		 * 付息日金额
		 */
		public static final String PAYRPI = "3";
		
		/**
		 * 到期日金额
		 */
		public static final String MDATE = "4";
		/**
		 * 基金确认差额
		 */
		public static final String VERIFY= "5";

		/**
		 * 红利再投金额
		 */
		public static final String DRT= "6";
	}
	/**
	 * 基金现金流交易类型
	 * @author guomeijuan
	 *
	 */
	public final static class CashFlowType{

		/**
		 * 货基申购
		 */
		public static final String FundType = "1";

		/**
		 * 货基申购确认
		 */
		public static final String FundTypeACK = "2";
		/**
		 * 货基赎回确认
		 */
		public static final String FundTypeBack = "7";

		/**
		 * 债基申购
		 */
		public static final String BondFundType = "3";

		/**
		 * 债基申购确认
		 */
		public static final String BondFundTypeACK = "4";

		/**
		 * 债基赎回确认
		 */
		public static final String BondFundTypeBack = "8";

		/**
		 * 专基申购
		 */
		public static final String CashFundType = "5";

		/**
		 * 专基申购确认
		 */
		public static final String CashFundTypeACK = "6";

		/**
		 * 专基赎回确认
		 */
		public static final String CashFundTypeBack = "9";
		/**
		 * 债基分红
		 */
		public static final String BondFundTypeShare = "10";
		/**
		 * 专基分红
		 */
		public static final String CashFundTypeShare = "11";
		/**
		 * 货基分红
		 */
		public static final String FundTypeShare = "13";
		/**
		 * 货基红利再投
		 */
		public static final String FundTypeAgain = "12";
		/**
		 * 债基红利再投
		 */
		public static final String BondFundTypeAgain = "14";
		/**
		 * 专基红利再投
		 */
		public static final String CashFundTypeAgain = "15";

	}
}
