package com.singlee.hrbcap.util;

import com.singlee.capital.common.util.JY;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 日期工具类
 * @author Gaolb
 */

public class DateUtils {
	
	public final static String FMT_DATE_DEFAULT = "yyyy-MM-dd";
	public final static String FMT_TIME_DEFAULT = "HH:mm:ss";
	public final static String FMT_DATETIME_DEFAULT = "yyyy-MM-dd HH:mm:ss";
	
	public final static char[] weekNumbers = {'一','二','三','四','五','六','日'};
	
	
	
	
	public static Date getUtilDate(String dateStr, String format) throws ParseException {
		if (dateStr == null || dateStr.length() == 0) {
            return null;
        }
		format = format == null ? FMT_DATETIME_DEFAULT : format;
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.parse(dateStr);
	}
	
	public static String getAfterTreatingDate(String date, String method, int days) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat(FMT_DATE_DEFAULT);
		Date handleDate = sdf.parse(date);
		Calendar cal = Calendar.getInstance();
		cal.setTime(handleDate);
		//加
		if ("add".equals(method)) {
			cal.add(Calendar.DATE, days);
			return new SimpleDateFormat(FMT_DATE_DEFAULT).format(cal.getTime());
		//减
		} else if ("subtract".equals(method)) {
			cal.add(Calendar.DATE, -days);
			return new SimpleDateFormat(FMT_DATE_DEFAULT).format(cal.getTime());
		} else {
			return date;
		}
	}
	
	/***
	 * 第一个日期大 返回true
	 * 第一个日期小于等于第二个日期  返回false
	 * @param fdate
	 * @param sdate
	 * @return
	 * @throws ParseException
	 */
	public static boolean dateCompare(String fdate, String sdate) throws ParseException {
		SimpleDateFormat dateFormat = new SimpleDateFormat(FMT_DATE_DEFAULT); 
		Date fdateTime = dateFormat.parse(fdate);   
		Date sdateTime = dateFormat.parse(sdate);  
		int i = fdateTime.compareTo(sdateTime);   
		//第一个日期大
        if (i > 0) {
        	return true;
        //第二个日期大
        } else {
        	return false;
        }
	}
	
	
	/***
	 * 根据传入的月份和日期返回当年的年月日，即返回例如 ： 2020-03-31
	 * @param month
	 * @param day
	 * @return
	 */
	public static String getDateByMonthAndDay(String month,String day) {
		String date = "";
		if(month.length()!=2 || day.length()!=2) {
			JY.raise("传入的月份或日期不是2位数！请检查！");
		}
		Calendar cl = Calendar.getInstance();
		int year = cl.get(Calendar.YEAR);
		date = String.valueOf(year)+"-"+month+"-"+day;
		return date;
	}
	
	
	
	public static String formatDateTime() {
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("HHmmss"); 
		return dateFormat.format(date);
	}
	
	public static String formatDateString() {
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd"); 
		return dateFormat.format(date);
	}

	
	public static String formatDateTimeByFormat(String format) {
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat(format); 
		return dateFormat.format(date);
	}
	
	public static String getDate() {
		Date now = Calendar.getInstance().getTime();
		return new SimpleDateFormat("ddMMyyyy").format(now);
	}
	
}
