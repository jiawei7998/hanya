package com.singlee.hrbcap.util;

import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.MathUtil;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.hrbcap.model.acup.TbEntry;
import com.singlee.hrbcap.model.base.TbManualEntry;
import org.apache.commons.lang.time.DateUtils;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * 记账常用工具类
 * @author Libo
 *
 */
public class BookkeepingUtil {

	/**
	 * 调整并校验分录   主要调整以下内容：红蓝字、轧差校验，去掉0的分录
	 * @param entryList
	 */
	public static void adjustAndCheckEntryList(List<TbEntry> entryList){
		
		double dAmt = 0;
		double cAmt = 0;
		for (int i = 0; i < entryList.size(); i++) {
			TbEntry entry = entryList.get(i);
			//计算借贷总额
			if(DictConstants.DebitCreditFlag.Debit.equals(entry.getDebitCreditFlag())){
				dAmt = dAmt + entry.getValue();
			}else if(DictConstants.DebitCreditFlag.Credit.equals(entry.getDebitCreditFlag())){
				cAmt = cAmt + entry.getValue();
			}
		}
		//校验借贷平衡
		if(!MathUtil.isEqual(dAmt, cAmt,2,BigDecimal.ROUND_HALF_UP)){
			for(TbEntry entry : entryList){
				entry.setState(DictConstants.EntryState.ABNORMAL);
				entry.setErrCode("01");
				entry.setErrMsg("分录校验失败，借贷不平，差额：" + Math.abs(dAmt-cAmt));
			}
		}
		// 分录金额全部为0时,不去除为0的分录
		if(!MathUtil.isZero(dAmt) && !MathUtil.isZero(cAmt)){
			//去除值为0的分录
//			for(TbEntry entry : entryList){
//				if(MathUtil.isZero(entry.getValue())){
//					entryList.remove(entry);
//				}
//			}
			
			for(int i= 0; i < entryList.size() ; i++){
				if(MathUtil.isZero(entryList.get(i).getValue())){
					entryList.remove(i);
					i--;
				}
			}
		}
	}
	

	/**
	 * 校验分录 
	 * @param entryList
	 */
	public static void CheckManualEntryList(List<TbManualEntry> entryList){
				
		//计算借贷总额
		double dAmt = 0;
		double cAmt = 0;	
		for(TbManualEntry entry : entryList){
			if(DictConstants.DebitCreditFlag.Debit.equals(entry.getDebitCreditFlag())){
				dAmt = dAmt + entry.getValue();
			}else if(DictConstants.DebitCreditFlag.Credit.equals(entry.getDebitCreditFlag())){
				cAmt = cAmt + entry.getValue();
			}
		}
		//校验借贷平衡
		if(!MathUtil.isEqual(dAmt, cAmt,5,BigDecimal.ROUND_DOWN)){
			throw new RException("分录校验失败，借贷不平，差额：" + Math.abs(dAmt-cAmt));
		}
	}

	
	/**
	 * 判断两个对象是否相等
	 * @param obj1
	 * @param obj2
	 * @return
	 */
	public static boolean equals(Object obj1, Object obj2){
		if(obj1 == null && obj2 == null){
			return true;
		}
		if(obj1 == null || obj2 == null){
			return false;
		}
		return obj1.equals(obj2);
	}

	/**
	 * 判断是否为一年的最后一天
	 * @param date
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public static boolean isLastDayOfYear(String date){
		Date date1 = DateUtil.parse(date);
		Date date2 = DateUtils.addDays(date1, 1);
		if(date1.getYear() == date2.getYear()) {
            return false;
        } else {
            return true;
        }
		
	}
	
	public static String getErrorInfoFromException(Exception e) {
		StringWriter sw = null; PrintWriter pw = null;
		try {
			sw = new StringWriter();
			pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			return "\r\n" + sw.toString() + "\r\n";
		} catch (Exception e2) {
			return "bad getErrorInfoFromException";
		}finally{
			
			try {
				if(null != sw) {
                    sw.close();
                }
				if(null != pw) {
                    pw.close();
                }
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		
		}
	}

}
