package com.singlee.hrbcap.util;
/**
 * 首期变量控制器
 * @author SINGLEE
 *
 */
public class TradeConstants {
    /**
     * 产品代码
     */
    public final static class ProductCode{
		/***货币基金**/
        public static final String CURRENCY_FUND = "801";
        /***债券基金****/
        public static final String BOND_FUND = "802";
		/***专户基金****/
		public static final String SPEC_FUND = "803";

		/***外汇即期****/
		public static final String RMBSPT = "411";
		/***外汇远期****/
		public static final String RMBFWD = "391";
		/***外汇掉期***/
		public static final String RMBSWAP = "432";
		/***外币拆借**/
		public static final String CCYLENDING = "438";

		/***同业拆放(对俄)****/
		public static final String FXTYIBO = "435";
		/***同业存放(对俄)****/
		public static final String FXDEPIN = "436";
		/***存放同业(对俄)****/
		public static final String FXDEPOUT = "437";

		/***同业存放****/
		public static final String RMBDEPIN = "790";
		/***存放同业****/
		public static final String RMBDEPOUT = "791";

		/***同业存款**/
		public static final String IB_deposit = "800";
		/***同业存款-线下**/
		public static final String IB_deposit_offline = "799";

		/***信用拆借****/
        public static final String IBO = "444";
		/***同业借款****/
        public static final String TYIBO = "544";


		/***现券买卖****/
		public static final String CBT = "443";
		/***债券借贷****/
        public static final String SL = "445";
		/***债券发行****/
        public static final String DP = "452";
		/***存单发行****/
        public static final String DPCD = "425";
		/***提前偿还****/
        public static final String MBS = "471";

		/***质押式回购****/
        public static final String CR = "446";
		/***买断式回购****/
        public static final String OR = "442";
		/***国库定期存款****/
        public static final String CRGD = "467";
		/***交易所回购****/
        public static final String CRJY = "468";
		/***常备借贷便利****/
        public static final String CRCB = "469";
		/***中期借贷便利****/
        public static final String CRZQ = "470";
		/***线下押券****/
        public static final String CRZD = "472";
		/***利率互换****/
        public static final String IRS = "441";
		/***债券远期****/
		public static final String BFWD = "447";
    }
    
    /**
     * 操作类型
     */
    public final static class TrdType{
		/***货币基金申购**/
		public static final String CURY_FUND_AFP = "CURYAFP";
		/***货币基金申购确认**/
		public static final String CURY_FUND_AFPCONF = "CURYAFPCON";
		/***货币基金赎回**/
		public static final String CURY_FUND_RDP = "CURYRDP";
		/***货币基金赎回确认**/
		public static final String CURY_FUND_RDPCONF = "CURYRDPCON";

		/***债券基金申购**/
		public static final String BD_FUND_AFP = "BDAFP";
		/***债券基金申购确认**/
		public static final String BD_FUND_AFPCONF = "BDAFPCON";
		/***债券基金赎回**/
		public static final String BD_FUND_RDP = "BDRDP";
		/***债券基金赎回确认**/
		public static final String BD_FUND_RDPCONF = "BDRDPCON";

    	/***基金申购**/
    	public static final String FUND_AFP = "AFP";
    	/***基金申购确认**/
    	public static final String FUND_AFPCONF = "AFPCON";
    	/***基金赎回**/
    	public static final String FUND_RDP = "RDP";
    	/***基金赎回确认**/
    	public static final String FUND_RDPCONF = "RDPCON";

    	/***基金现金分红**/
    	public static final String FUND_REDD = "REDD";
    	/***基金红利再投**/
    	public static final String FUND_REIN = "REIN";
    }
	/**
	 * 操作类型
	 */
	public final static class CurrencyType{
		/***非净值型**/
		public static final String FJZX="1";
		/***净值型**/
		public static final String JZX="2";
	}

	/**
	 * cfets市场标识
	 */
	public final static class MarketIndicator{
		public static final String ALL = "0";
		// 信用拆借
		public static final String INTER_BANK_OFFERING = "1";
		// 买断式回购
		public static final String OUTRIGHT_REPO = "10";
		// 外汇掉期
		public static final String FXSWP = "11";
		// 外汇即期
		public static final String FXSPT = "12";
		// 外汇远期
		public static final String FXFOW = "14";
		// 利率互换
		public static final String INTEREST_RATE_SWAP = "2";
		// 现券买卖
		public static final String CASH_BOND = "4";
		// 同业借款
		public static final String INTER_BANK_BORROWING = "41";
		// 标准债券远期
		public static final String STANDARD_BOND_FORWARD = "43";
		// 债券远期
		public static final String BOND_FORWARD = "5";
		// 同业存款
		public static final String IBD = "53";
		// 债券借贷
		public static final String SECURITY_LENDING = "8";
		// 质押式回购
		public static final String COLLATERAL_REPO = "9";
	}
	
	
	/**
     * 交易类型
     */
	public final static class Dealtype{
	    //冲销
	   public static final String WRITE_OFF = "2";
	   //正常交易
	   public static final String NORMAL = "1";
	
	}

	/**
	 * 限额类型
	 */
	public final static class LimitType{
		//单笔限额
		public static final String SINGLE_LIMIT  = "1";
		//累计限额
		public static final String CUMULATIVE_LIMIT = "2";
	}
}
