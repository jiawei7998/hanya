package com.singlee.hrbcap.mapper;

import com.singlee.hrbcap.model.base.TbProductEventDimension;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 产品事件 与 维度的组合关系
 * @author louhuanqing
 *
 */
public interface TbProductEventDimensionMapper extends Mapper<TbProductEventDimension>{
	/**
	 * 根据 prdNo,eventId获得排序的产品维度列表
	 * @param map
	 * @return
	 */
	List<TbProductEventDimension> getTbProductEventDimesions(Map<String, Object> map);
}
