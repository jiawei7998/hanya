package com.singlee.hrbcap.mapper;


import com.singlee.hrbcap.model.base.SceneResultBean;
import com.singlee.hrbcap.model.base.TbProductAllSceneAcupView;
import com.singlee.hrbcap.model.base.TbProductEventDimensionKv;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 产品事件 与 维度键值关系
 * @author louhuanqing
 *
 */
public interface TbProductEventDimensionKvMapper extends Mapper<TbProductEventDimensionKv>{
	/**
	 * 获得产品事件的场景组合ID
	 * @param map
	 * @return
	 */
	String getTbProductEventDimensionKvGroupId(Map<String, Object> map);
	
	public List<TbProductEventDimensionKv> getDataForProductAndEvent(Map<String, Object> map);
	/**
	 * 获取所有场景
	 * @return
	 */
	public List<SceneResultBean> getSceneResultList(Map<String, Object> map);
	/**
	 * 删除所选维度组合
	 */
	void deleteTbProductEventDimensionKv(Map<String, Object> map);
	
	/**
	 * Excel的配置文件导入完成后，需要更新场景值组合表的ID
	 */
	void updateProductEventDimensionKvId(Map<String, Object> map);
	
	
	
	/***删除分录配置**start********/
	void deleteTbProductEventSceneAcupByPrdNo(Map<String, Object> map);
	void deleteTbProductEventDimensionKvByPrdNo(Map<String, Object> map);
	void deleteTbProductEventDimensionByPrdNo(Map<String, Object> map);
	/***删除分录配置**end********/
	
	/**
	 * 获得所有场景分录
	 * @param map
	 * @return
	 */
	List<TbProductAllSceneAcupView> selectAllSceneForProdNo(Map<String, Object> map);
	/**
	 * 获得所有场景zl
	 * @param map
	 * @return
	 */
	List<SceneResultBean> getSceneResult(Map<String, Object> map);
	SceneResultBean getDataForProductAndEventAndDem (Map<String, Object> map);
	/***清理规则基础表**start********/
	void deleteTbDimension();
	void deleteTbDimensionValue();
	void deleteTbEvent();
	void deleteTbProductEvent(Map<String, Object> map);
	/***清理规则基础表**end********/
}
