package com.singlee.hrbcap.mapper;


import com.singlee.hrbcap.model.base.TbDealEvent;

import java.util.Map;


public interface TbDealEventMapper {

    /**
     *
     * @zhangcm 2019-08-07
     */
    int insert(TbDealEvent record);

    TbDealEvent selectByDealNoAndEvent(Map<String, Object> map);
}