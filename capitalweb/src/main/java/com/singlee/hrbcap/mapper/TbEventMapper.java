package com.singlee.hrbcap.mapper;

import com.github.pagehelper.Page;
import com.singlee.hrbcap.model.base.TbEvent;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 会计事件数据库操作类
 * @author louhuanqing
 *
 */
public interface TbEventMapper extends Mapper<TbEvent>{
	public Page<TbEvent> getTbEventPage(Map<String, Object> map, RowBounds rb);
	public List<TbEvent> getTbEventPage(Map<String, Object> map);
	
	/**
	 * 根据eventId获得事件
	 * @param eventId
	 * @return
	 */
	TbEvent getTbEventByEventId(Map<String, Object> map);
	String getGroupId();
	/**
	 * 锁定事件驱动表
	 * @param map
	 * @return
	 */
	public List<TbEvent> lockTbEvent(Map<String, Object> map);
	/**
	 * 检查事件中是否有当前事件代码
	 * @param amountId
	 * @return
	 */
	public Integer checkTbEventByEventId(String amountId);
	/**
	 * 获得产品项下已分配的事件
	 * @param params
	 * @return
	 */
	public List<TbEvent> getTbEventByPrdNoYes(Map<String, Object> map);
	
	/**
	 * 获得产品项下未分配的事件
	 * @param params
	 * @return
	 */
	public List<TbEvent> getTbEventByPrdNoNo(Map<String, Object> map);
	/**
	 * 查询下拉事件
	 * @param params
	 * @return
	 */
	public List<TbEvent> searchEvent(Map<String, Object> params);
	
	
	/**
	 * 根据dealNo查询TbEntry表中不重复的下拉事件
	 * @param params
	 * @return
	 */
	public List<TbEvent> searchDistinctEventByDealNo(Map<String, Object> params);
	
	
}
