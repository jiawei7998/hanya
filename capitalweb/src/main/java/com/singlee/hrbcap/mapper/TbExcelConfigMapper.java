package com.singlee.hrbcap.mapper;

import com.singlee.hrbcap.model.base.TbExcelConfig;
import tk.mybatis.mapper.common.Mapper;

/**
 * excel配置表操作类
 * @author louhuanqing
 *
 */
public interface TbExcelConfigMapper extends Mapper<TbExcelConfig>{

}
