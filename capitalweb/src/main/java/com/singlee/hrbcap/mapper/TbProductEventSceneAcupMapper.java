package com.singlee.hrbcap.mapper;

import com.singlee.hrbcap.model.base.TbProductEventSceneAcup;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 产品事件维度取值组与会计分录关系表
 * @author louhuanqing
 *
 */
public interface TbProductEventSceneAcupMapper extends Mapper<TbProductEventSceneAcup>{
	/**
	 * 根据场景获得分录
	 * @param map
	 * @return
	 */
	List<TbProductEventSceneAcup> getTbProductEventSceneAcupsByGroupId(Map<String, Object> map);
	/**
	 * 根据场景和借贷方向获得分录
	 * @param map
	 * @return
	 */
	List<TbProductEventSceneAcup>  getTbProductEventSceneAcupsByGroupIdAndDrcrInd(Map<String, Object> map);
	/**
	 * 更新场景信息
	 * @param TbProductEventSceneAcup
	 * @return
	 */
	public void updateTbProductEventSceneAcup(TbProductEventSceneAcup tbProductEventSceneAcup);
	/**
	 * 新增场景科目信息
	 * @param TbProductEventSceneAcup
	 * @return
	 */
	public void insertTbProductEventSceneAcup(TbProductEventSceneAcup tbProductEventSceneAcup);
	/** 删除场景分录配置
	 * 
	 * @param TbProductEventSceneAcup
	 * @return
	 */
	public void deleteTbProductEventSceneAcup(TbProductEventSceneAcup tbProductEventSceneAcup);
	/**
	 * 科目-场景绑定
	 * 
	 * @param map
	 * @return
	 */
    public void bundSceneSubject(Map<String,Object> map);
	/**
	 * 科目-场景解除绑定
	 * 
	 * @param map
	 * @return
	 */
	public void untieSceneSubject(Map<String,Object> map);
	/**
	 * 查询已存在记录数
	 * 
	 * @param TbProductEventSceneAcup
	 * @return
	 */
	public int getCount (TbProductEventSceneAcup tbProductEventSceneAcup);
	
	/**
	 * 根据科目管理更新场景分录科目名称
	 * @param tbProductEventSceneAcup
	 */
	public void updateSceneAcupSubjDescBySubject(TbProductEventSceneAcup tbProductEventSceneAcup);
	
	/**
	 * 当前场景下最大排序
	 * 
	 * @param map
	 * @return
	 */
	public int getSubjSeq(Map<String,Object> map);
}
