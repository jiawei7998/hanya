package com.singlee.hrbcap.mapper;

import com.singlee.hrbcap.model.base.TbProductEvent;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

/**
 * 产品事件关联表
 * @author louhuanqing
 *
 */
public interface TbProductEventMapper extends Mapper<TbProductEvent>{
	/**
	 * 根据prdNo 和 eventId获得数据
	 * @param map
	 * @return
	 */
	TbProductEvent getTbProductEvent(Map<String, Object> map);
	
	public void deleteTbProductEventByProNo(Map<String, Object> map);
}
