package com.singlee.hrbcap.mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.common.spring.mybatis.MyMapper;
import com.singlee.hrbcap.model.base.TbCashFlow;
import com.singlee.hrbcap.model.base.TbCashFlowDto;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

public interface TbCashFlowMapper extends MyMapper<TbCashFlow> {
    /**
     * 查询现金流信息
     * @param map
     * @return
     */
    public Page<TbCashFlow> queryCashFlow(Map<String, Object> map, RowBounds rowBounds);

    public Page<TbCashFlowDto> getCashFlow(Map<String, Object> map, RowBounds rowBounds);
    /**
     * 根据收息勾对金额更新金额
     * @param map
     */
    public void updateCashFlowByDate(Map<String, Object> map);

    /**
     * sendODS查询现金流信息所有数据
     *
     */
    public List<TbCashFlow> queryCashFlow();
}
