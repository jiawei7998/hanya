package com.singlee.hrbcap.mapper;

import com.github.pagehelper.Page;
import com.singlee.hrbcap.model.base.TbAmount;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 金额操作类
 * @author louhuanqing
 *
 */
public interface TbAmountMapper extends Mapper<TbAmount>{

	public Page<TbAmount> getTbAmountList(Map<String, Object> map, RowBounds rb);
	/**
	 * 获得所有金额指针
	 * 
	 * @param amountId
	 * @return
	 */
    public List<TbAmount> getTbAmountList();
	/**
	 * 检查金额指针中是否有当前指针代码
	 * 
	 * @param amountId
	 * @return
	 */
	public Integer checkTbAmountById(String amountId);

	public Integer queryName(String amountName);
	
}
