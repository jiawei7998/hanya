package com.singlee.hrbcap.mapper;

import com.github.pagehelper.Page;
import com.singlee.hrbcap.model.acup.TbEntry;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 会计分录明细操作类
 * @author Administrator
 *
 */
public interface TbEntryMapper extends Mapper<TbEntry>{
	/**		生成新的流水号 	 */
	public String getNewTbEntryFlowNo();
	/**		生成调账套号 	 */
	public String getAdjustTbEntryFlowNo();
	/**
  	 * 407对账查询同业清算详情		//美元
  	 * @param map
  	 * @return
  	 */
  	public Page<TbEntry>  selectByBkkpgOrgIdAndPostDateAndUsd(Map<String,Object> map,RowBounds rb);
  	public Page<TbEntry>  selectByBkkpgOrgIdAndPostDateAndUsd(Map<String,Object> map);//为新柜面新增方法
  	
  	/**
	 * 查询分录流水
	 * @param map
	 * @param rb
	 * @return
	 */
	Page<Map<String, Object>> searchEntryFlow(Map<String, Object> map, RowBounds rb);
	
	/**
	 * 查询出入账
	 * @param map
	 * @param rb
	 * @return
	 */
	Page<Map<String, Object>> selectOutAndInBookkeeping(Map<String, Object> map, RowBounds rb);
	
	List<Map<String, Object>> selectOutAndInBookkeeping(Map<String, Object> map);
	
	/**
   * 查询  
   * @param map
   * map参数    
   */
	public List<TbEntry>  selectByMap(Map<String,Object> map);
  
	public Page<List<TbEntry>>  selectByMap(Map<String,Object> map, RowBounds rb);
	
	/**
	   * 待发送
	   * @param map
	   * map参数    
	   */
	public Page<List<TbEntry>>  selectByMapSend(Map<String,Object> map,RowBounds rb);
	

	/**
   * 
   * @param taskId
   * @return
   */
  	public Integer checkBookkeepingEntryByTaskId(String taskId);
  	
  	/**
  	 * 查询需要送帐的套号
  	 * @param map
  	 * @return
  	 */
  	public List<TbEntry> selectEntryFlowId(Map<String, Object> map);
  	
  	/**
     * 查询需要送帐的信息
     * @param map
     * @return
     */
  	public List<TbEntry> selectFundEntry(Map<String, Object> map);

	/**
	 *
	 * @param entry
	 */
	public void updateEntry(TbEntry entry);
  	
  	/**
  	 * 更新总账送帐状态
  	 * @param entry
  	 */
  	public void updateEntryState(TbEntry entry);

	/**
	 * 更新总账送帐状态
	 * @param entry
	 */
	public void updateEntryRetState(TbEntry entry);
  
  	
  	public List<TbEntry>  selectByTime(Map<String,Object> map);
  	
  	/**
  	 * 407对账查询同业清算详情		//人民币
  	 * @param map
  	 * @return
  	 */
  	public Page<TbEntry>  selectByBkkpgOrgIdAndPostDateAndCcy(Map<String,Object> map,RowBounds rb);
  	
  	/**
  	 * 根据机构，产品查询
  	 */
  	public List<TbEntry>	selectByBkkpgOrgIdAndPrdNo(Map<String,Object> map);
  	
	/**
	 * 查询数据信息
	 */
	public Page<TbEntry>  searchPageData(Map<String,Object> map, RowBounds rb);
	
	
	/**
  	 * 407对账查询
  	 */
	public Page<TbEntry>  searchTbEntryBySubjectSum(Map<String,Object> map);
	
	/**
  	 * 查询所有需要发核心的账务
  	 */
  	public List<TbEntry>  queryTbEntry(Map<String,Object> map);
  	
  	/**
  	 * 查询所有需要发核心的账务ID
  	 */
  	public List<TbEntry>  queryTbEntryFlowId(Map<String,Object> map);
  	
  	/**
  	 * 查询辖内往来账户的信息
  	 */
  	public String  queryTbEntryAccount(Map<String,Object> map);
  	/**
  	 * 
  	 * @Title: queryTbEntryAccountResult   
  	 * @Description: 查询需要更新的账务信息
  	 * @author: tmk
  	 * @date: 2019年8月27日 下午6:42:51
  	 * @param map
  	 * @return
  	 */
  	public List<TbEntry>  queryTbEntryAccountResult(Map<String,Object> map);
  	
  	/**
  	 * 根据交易单号查询账套信息
  	 */
  	public String  queryTbAccount(Map<String,Object> map);
  	
  	/***
  	 * 根据dealNo、event查询会计分录模板
  	 * @param map
  	 * @return
  	 */
  	public List<TbEntry>  queryTbEntryTemplateByDealNoAndEvent(Map<String,Object> map);
  	/***
  	 * 根据dealNo、event，查询账务日期最新、套号（FLOW_ID）最大的会计分录
  	 * @param map
  	 * @return
  	 */
  	public List<TbEntry>  queryTbEntryByDealNoAndEventNew(Map<String,Object> map);
  	
  	/**
  	 * 查询需发送的活期结息账务
  	 * @param  prdNo产品类型    eventId账务类型  postDate账务日期 sendFlag 未发送/发送失败状态  
  	 */
  	public List<TbEntry> queryTbEntryForCurrent(Map<String,Object> map);
  	
  	/**
  	 * 查询同业存放需要发核心的账务
  	 */
  	public Page<TbEntry>  queryTbEntry(Map<String,Object> map, RowBounds rb);
  	
  	/**
  	 * 根据条件更新分录发送状态
  	 * @param map
  	 */
  	void updateEntrySendFlag(Map<String,Object> map);
  	
  	/**
  	 * sendODS查询账务表所有数据
  	 * @param
  	 */
  	public List<TbEntry> getAllTbEntry();

	/**
	 * 查询所有需发送账务得业务品种
	 * @param map
	 */
	public List<String> getEntryPrdNos(Map<String,Object> map);

	/**
	 * 批量更新发账结果
	 * @param list
	 */
	public void postBatchUpdate(List<TbEntry> list);

	/**
	 * 锁表
	 * @param flowId
	 */
	public void lockTableByFlowId(@Param("flowId") String flowId);
	/**
	 * 核对当日账务是否已存在
	 * @param map
	 * @return
	 */
	public List<TbEntry> checkDailyAcup(Map<String,Object> map);

	/**
	 * 删除临时账务表中数据
	 */
	public void deleteTbEntryT();

	/**
	 * 将需要发送得账务放入临时表中
	 */
	public void insertNeedEntryToT(Map<String, Object> param);
	/**
	 * 查询临时表所有需要发核心的账务
	 */
	public List<TbEntry>  queryTbEntryT(Map<String,Object> map);
	/**
	 * 查询临时表所有需要发核心的账务ID
	 */
	public List<TbEntry>  queryTbEntryFlowIdT(Map<String,Object> map);

	public List<TbEntry>  getBusyFlowIds(Map<String,Object> map);

	/**
	 *获取前置日终账
	 * @param map
	 * @return
	 */
	public List<TbEntry>  getTbEntryAcup(Map<String,Object> map);

	/**
	 *
	 * @param map
	 * @return
	 */
	public Page<TbEntry>  getTbEntryGroupByFlowId(Map<String,Object> map,RowBounds rb);

	/**
	 *
	 * @param map
	 * @return
	 */
	public Page<TbEntry>  getTbEntryByFlowId(Map<String,Object> map,RowBounds rb);
}
