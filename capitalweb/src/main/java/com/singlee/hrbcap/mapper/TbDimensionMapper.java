package com.singlee.hrbcap.mapper;

import com.github.pagehelper.Page;
import com.singlee.hrbcap.model.base.TbDimension;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

/**
 * 会计维度表
 * @author louhuanqing
 *
 */
public interface TbDimensionMapper extends Mapper<TbDimension>{
	/**
	 * 根据维度ID获得唯一维度信息
	 * @param dimensionId
	 * @return
	 */
	TbDimension geTbDimensionById(Map<String, Object> map);
	
	public Page<TbDimension> getTbDimensionPage(Map<String, Object> map, RowBounds rb);
	
	/**
	 * 根据DbId获得事件
	 * @param DbId
	 * @return
	 */
	TbDimension getTbDimensionByDbId(Map<String, Object> map);
	String getGroupId();

	/**
	 * 检查事件中是否有当前事件代码
	 * @param amountId
	 * @return
	 */
	public Integer checkTbDimensionByDbId(String dbId);
}
