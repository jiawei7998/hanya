package com.singlee.hrbcap.mapper;

import com.github.pagehelper.Page;
import com.singlee.hrbcap.model.base.SceneResultBean;
import com.singlee.hrbcap.model.base.TbDimensionValue;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 维度值表
 * @author louhuanqing
 *
 */
public interface TbDimensionValueMapper extends Mapper<TbDimensionValue>{
	/**
	 * 根据维度ID获得记录
	 * @param dimensionId
	 * @return
	 */
	TbDimensionValue geTbDimensionValueById(Map<String, Object> map);
	
	
	public Page<TbDimensionValue> getTbDimensionValuePage(Map<String, Object> map, RowBounds rb);
	public List<TbDimensionValue> getTbDimensionValueList(Map<String, Object> map);
	/**
	 * 获取所有维度
	 * @return
	 */
	public List<TbDimensionValue> getTbDimensionValueListOrder();
	/**
	 * 按产品事件获取维度
	 * @param map
	 * @return
	 */
	public List<SceneResultBean> getTbDimensionValueListByPE(Map<String, Object> map);
	/**
	 * 根据DbId获得事件
	 * @param DbId
	 * @return
	 */
	TbDimensionValue getTbDimensionValueByDbId(Map<String, Object> map);
	String getGroupId();

	/**
	 * 检查事件中是否有当前事件代码
	 * @param amountId
	 * @return
	 */
	public Integer checkTbDimensionValueByDbId(String dbId);
	
}
