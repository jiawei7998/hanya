package com.singlee.hrbcap.mapper;

import com.github.pagehelper.Page;
import com.singlee.hrbcap.model.base.TbSubjectDef;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

/*
 * 科目表Mapper层  201805
 */
public interface TbSubjectDefMapper extends Mapper<TbSubjectDef> {
	/*
	 * 遍历科目表
	 */
	public Page<TbSubjectDef> getTbSubjectDefList(Map<String,Object> map);
	
	public Page<TbSubjectDef> getTbSubjectDefList(Map<String,Object> map, RowBounds rb);

	public void deleteTbSubjectDefByTaskId(String taskId);
	
	/*
	 * 遍历科目表
	 */
	public List<TbSubjectDef> queryTbSubjectDefList(Map<String,Object> map);

	/*
	 * 查询条件科目及上级科目
	 */
	public List<TbSubjectDef> getTbSubjectAndParentList(Map<String,Object> map);
	
	/**
	 * 根据科目号 精确查询
	 * @param map
	 * @return
	 */
	public Page<TbSubjectDef> getTbSubjectDefSame(Map<String,Object> map);
	
	/**
	 * 根据科目号 精确查询
	 * @param map
	 * @return
	 */
	public List<TbSubjectDef> getTbSubjectDefDistinctFirst(Map<String,Object> map);
	/**
	 * 根据科目号 更新科目名称
	 * @param tbsubjectdef
	 */
	public void  updateSubjName (TbSubjectDef tbsubjectdef);
}
