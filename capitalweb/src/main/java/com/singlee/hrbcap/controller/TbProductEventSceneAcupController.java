package com.singlee.hrbcap.controller;


import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.hrbcap.model.base.SceneResultBean;
import com.singlee.hrbcap.model.base.TbProductAllSceneAcupView;
import com.singlee.hrbcap.model.base.TbProductEventSceneAcup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/TbProductEventSceneAcupController")
public class TbProductEventSceneAcupController extends CommonController {
	@Autowired
	private com.singlee.hrbcap.service.TbProductEventSceneAcupService TbProductEventSceneAcupService;

	@ResponseBody
	@RequestMapping(value = "/selectAllSceneForProdNo")
	public RetMsg<List<TbProductAllSceneAcupView>> selectAllSceneForProdNo(@RequestBody Map<String, Object> map){
		
		List<TbProductAllSceneAcupView> sceneResultList = TbProductEventSceneAcupService.getAllSceneResultList(map);
		return RetMsgHelper.ok(sceneResultList);
	}
	
	
	
	@ResponseBody
	@RequestMapping(value = "/getSceneResultList")
	public RetMsg<List<SceneResultBean>> getSceneResultList(){
		
		List<SceneResultBean> sceneResultList = TbProductEventSceneAcupService.getSceneResultList();
		return RetMsgHelper.ok(sceneResultList);
	}

	@ResponseBody
	@RequestMapping(value = "/getSceneAcupListByGroupId")
	public RetMsg<List<TbProductEventSceneAcup>> getSceneAcupListByGroupId(@RequestBody Map<String, Object> map){
		
		List<TbProductEventSceneAcup> sceneResultList = TbProductEventSceneAcupService.getTbProductEventSceneAcupsByGroupId(map);
		return RetMsgHelper.ok(sceneResultList);
	}
	
	/**
	 * 修改场景分录信息
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/updateTbProductEventSceneAcup")
	public RetMsg<Serializable> updateTbProductEventSceneAcup(@RequestBody TbProductEventSceneAcup tbProductEventSceneAcup) {
		TbProductEventSceneAcupService.updateTbProductEventSceneAcup(tbProductEventSceneAcup);
		return RetMsgHelper.ok();
	}
	/**
	 * 新增场景分录信息
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/insertTbProductEventSceneAcup")
	public RetMsg<Serializable> insertTbProductEventSceneAcup(@RequestBody TbProductEventSceneAcup tbProductEventSceneAcup){
		TbProductEventSceneAcupService.insertTbProductEventSceneAcup(tbProductEventSceneAcup);
		return RetMsgHelper.ok();
	}
	/**
	 * 删除场景分录信息
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/deleteTbProductEventSceneAcup")
	public RetMsg<Serializable> deleteTbProductEventSceneAcup(@RequestBody TbProductEventSceneAcup tbProductEventSceneAcup){
		TbProductEventSceneAcupService.deleteTbProductEventSceneAcup(tbProductEventSceneAcup);
		return RetMsgHelper.ok();
	}
	/**
	 * 场景分录信息更改保存
	 * @param map
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@ResponseBody
	@RequestMapping(value="/saveChange")
	public RetMsg<Serializable> saveChange(@RequestBody Map<String, Object> map){
		Map<String,Object> leftMap=(Map<String, Object>) map.get("contentL");
		Map<String,Object> rightMap=(Map<String, Object>) map.get("contentR");
		if(!rightMap.isEmpty()){
			TbProductEventSceneAcupService.untieSceneSubject(rightMap);	
		}
		if(!leftMap.isEmpty()) {
			TbProductEventSceneAcupService.bundSceneSubject(leftMap);
		}
		return RetMsgHelper.ok();
	}
}
