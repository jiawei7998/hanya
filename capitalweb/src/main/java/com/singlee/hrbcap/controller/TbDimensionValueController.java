package com.singlee.hrbcap.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.hrbcap.model.base.TbDimensionValue;
import com.singlee.hrbcap.service.TbDimensionValueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/TbDimensionValueController")
public class TbDimensionValueController extends CommonController {
	@Autowired
	private TbDimensionValueService tbDimensionValueService;

	/**
	 * 查询会计维度返回列表
	 * @param params
	 * @return
	 */
     
     
	@ResponseBody
	@RequestMapping(value = "/searchTbDimensionValuePage")
	public RetMsg<PageInfo<TbDimensionValue>> searchTbDimensionValuePage(@RequestBody Map<String, Object> params) {
		Page<TbDimensionValue> page = tbDimensionValueService.getTbDimensionValuePage(params, ParameterUtil.getRowBounds(params));
		return RetMsgHelper.ok(page);
	}

	
	@ResponseBody
	@RequestMapping(value = "/searchTbDimensionValueList")
	public RetMsg<List<TbDimensionValue>> searchTbDimensionValueList(@RequestBody Map<String,Object> map){
		List<TbDimensionValue> page  = tbDimensionValueService.getTbDimensionValueList(map);
//		page = TreeUtil.mergeChildrenList(page, "-");
		return RetMsgHelper.ok(page);
	}
	/**
	 * 添加会计维度代码
	 * @param TbDimensionValue
	 * @return
	 */
	
	@ResponseBody
	@RequestMapping(value = "/createTbDimensionValue")
	public RetMsg<Serializable> createTbDimensionValue(@RequestBody TbDimensionValue TbDimensionValue) {
		tbDimensionValueService.createTbDimensionValue(TbDimensionValue);
		return RetMsgHelper.ok();
	}
	/**
	 * 修改会计维度代码 
	 * 
	 * @param acctCode
	 */
	@ResponseBody
	@RequestMapping(value = "/updateTbDimensionValue")
	public RetMsg<Serializable> updateTbDimensionValue(@RequestBody TbDimensionValue TbDimensionValue) {
		tbDimensionValueService.updateTbDimensionValue(TbDimensionValue);
		return RetMsgHelper.ok();
	}

	/**
	 * 删除会计维度代码，删除条件为dbId
	 * 
	 * @param seq
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteTbDimensionValue")
	public RetMsg<Serializable> deleteTbDimensionValue(@RequestBody HashMap<String,Object> map) {
		tbDimensionValueService.deleteTbDimensionValue(ParameterUtil.getString(map, "dbId", ""));
		return RetMsgHelper.ok();
	}
	
}
