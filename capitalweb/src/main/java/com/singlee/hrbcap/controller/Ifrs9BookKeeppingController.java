package com.singlee.hrbcap.controller;

import com.github.pagehelper.Page;
import com.singlee.cap.bookkeeping.model.QueryBalanceModel;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.hrbcap.model.acup.TbDealBalance;
import com.singlee.hrbcap.service.TbDealBalanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller
@RequestMapping(value = "/Ifrs9BookkeepingController")
public class Ifrs9BookKeeppingController {

	@Autowired
	TbDealBalanceService dealBalanceService;
	/**
	 * 根据信息查询需要调账交易的科目余额
	 * 
	 * @param tbkAmount
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/queryDealBal")
	public RetMsg<PageInfo<TbDealBalance>> queryDealBal(@RequestBody Map<String, Object> params){
		Page<TbDealBalance> page = dealBalanceService.queryEntrySum(params);
		return RetMsgHelper.ok(page);
	}
	/**
	 * 基金科目余额查询
	 * @param tbkAmount
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/queryFundBal")
	public RetMsg<PageInfo<QueryBalanceModel>> queryFundBal(@RequestBody Map<String, Object> params){
		Page<QueryBalanceModel> page = dealBalanceService.queryFundBal(params);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 产品科目余额查询
	 * @param tbkAmount
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/queryProductBal")
	public RetMsg<PageInfo<QueryBalanceModel>> queryProductBal(@RequestBody Map<String, Object> params){
		Page<QueryBalanceModel> page = dealBalanceService.queryProductBal(params);
		return RetMsgHelper.ok(page);
	}
}
