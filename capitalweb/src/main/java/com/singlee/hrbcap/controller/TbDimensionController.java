package com.singlee.hrbcap.controller;

import com.github.pagehelper.PageInfo;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.hrbcap.model.base.TbDimension;
import com.singlee.hrbcap.service.TbDimensionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping(value = "/TbDimensionController")
public class TbDimensionController extends CommonController {
	@Autowired
	private TbDimensionService tbDimensionService;

	/**
	 * 查询会计维度返回列表
	 * @param params
	 * @return
	 */
     
     
	@ResponseBody
	@RequestMapping(value = "/searchTbDimensionPage")
	public RetMsg<PageInfo<TbDimension>> searchTbDimensionPage(@RequestBody Map<String, Object> params) {
		return RetMsgHelper.ok(tbDimensionService.getTbDimensionPage(params));
	}
	/**
	 * 添加会计维度代码
	 * @param TbDimension
	 * @return
	 */
	
	@ResponseBody
	@RequestMapping(value = "/createTbDimension")
	public RetMsg<Serializable> createTbDimension(@RequestBody TbDimension tbDimension) {
		tbDimensionService.createTbDimension(tbDimension);
		return RetMsgHelper.ok();
	}
	/**
	 * 修改会计维度代码 
	 * 
	 * @param acctCode
	 */
	@ResponseBody
	@RequestMapping(value = "/updateTbDimension")
	public RetMsg<Serializable> updateTbDimension(@RequestBody TbDimension tbDimension) {
		tbDimensionService.updateTbDimension(tbDimension);
		return RetMsgHelper.ok();
	}

	/**
	 * 删除会计维度代码，删除条件为dbId
	 * 
	 * @param seq
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteTbDimension")
	public RetMsg<Serializable> deleteTbDimension(@RequestBody HashMap<String,Object> map) {
		tbDimensionService.deleteTbDimension(ParameterUtil.getString(map, "dbId", ""));
		return RetMsgHelper.ok();
	}
	
}
