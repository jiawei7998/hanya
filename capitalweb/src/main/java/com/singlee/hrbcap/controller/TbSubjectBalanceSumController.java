package com.singlee.hrbcap.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.hrbcap.model.acup.TbSubjectBalanceSum;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/*
 * 科目表controller层
 */
@Controller
@RequestMapping(value = "/TbSubjectBalanceSumController")
public class TbSubjectBalanceSumController extends CommonController {
	/*
	 * 科目表service层
	 */
	@Autowired
	private com.singlee.hrbcap.service.TbSubjectBalanceSumService TbSubjectBalanceSumService;
	/*
	 * 查询科目信息
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageTbSubjectBalanceSum")
	public RetMsg<PageInfo<TbSubjectBalanceSum>> searchPageTbSubjectBalanceSum(@RequestBody Map<String,Object> allMap) throws RException {
		RowBounds rowBounds = ParameterUtil.getRowBounds(allMap);
		Page<TbSubjectBalanceSum> page = TbSubjectBalanceSumService.selectByMap(allMap, rowBounds);
		return RetMsgHelper.ok(page);
	}
	
}
