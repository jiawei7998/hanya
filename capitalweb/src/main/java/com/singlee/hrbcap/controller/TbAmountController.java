package com.singlee.hrbcap.controller;

import com.github.pagehelper.PageInfo;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.hrbcap.model.base.TbAmount;
import com.singlee.hrbcap.service.TbAmountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping(value = "/TbAmountController")
public class TbAmountController extends CommonController {
	@Autowired
	private TbAmountService tbAmountService;
	/**
	 * 查询金额返回列表
	 * 
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchAmountPointerPage")
	public RetMsg<PageInfo<TbAmount>> searchAmountPointerPage(@RequestBody Map<String, Object> params) {
		return RetMsgHelper.ok(tbAmountService.getTbAmountList(params));
	}
	
	/**
	 * 添加金额代码
	 * 
	 * @param TbAmount
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/createAmount")
	public RetMsg<Serializable> createAmount(@RequestBody TbAmount tbkAmount) {
		tbAmountService.createAmount(tbkAmount);
		return RetMsgHelper.ok();
	}

	/**
	 * 修改金额代码
	 * 
	 * @param tbkAmount
	 */
	@ResponseBody
	@RequestMapping(value = "/updateAmount")
	public RetMsg<Serializable> updateAmount(@RequestBody TbAmount tbkAmount) {
		tbAmountService.updateAmount(tbkAmount);
		return RetMsgHelper.ok();
	}

	/**
	 * 删除金额代码
	 * 
	 * @param amountId
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteAmount")
	public RetMsg<Serializable> deleteAmount(@RequestBody HashMap<String, Object> map) {
		tbAmountService.deleteAmount(ParameterUtil.getString(map, "amountId", ""));
		return RetMsgHelper.ok();
	}
}
