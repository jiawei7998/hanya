package com.singlee.hrbcap.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.interfacex.qdb.util.Reports;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.hrbcap.model.acup.TbEntry;
import com.singlee.hrbcap.service.TbEntryService;
import com.singlee.hrbcap.service.TbEventService;
import net.sf.jxls.transformer.XLSTransformer;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 * 分录controller层
 */
@Controller
@RequestMapping(value = "/TbEntryController")
public class TbEntryController extends CommonController {
	/*
	 * 分录表service层
	 */
	@Autowired
	private TbEntryService bookkeepingEntryService;
	@Autowired
	private TbEventService tbEventService;
	public final static Log logger = LogFactory.getLog(com.singlee.hrbcap.controller.TbEntryController.class);
	/*
	 * 查询分录信息
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageEntries")
	public RetMsg<PageInfo<List<TbEntry>>> searchPageEntries(@RequestBody Map<String,Object> allMap) throws RException {
		Page<List<TbEntry>> page = bookkeepingEntryService.getEntryList(allMap, ParameterUtil.getRowBounds(allMap));
		return RetMsgHelper.ok(page);
	}

	
	/*
     * 查询待发送分录信息
     */
    @ResponseBody
    @RequestMapping(value = "/searchPageSend")
    public RetMsg<PageInfo<List<TbEntry>>> searchPageSend(@RequestBody Map<String,Object> allMap) throws RException {
        Page<List<TbEntry>> page = bookkeepingEntryService.getEntrySendList(allMap, ParameterUtil.getRowBounds(allMap));
        return RetMsgHelper.ok(page);
    }

	/**
	 * 去重查询套账
	 * @param allMap
	 * @return
	 * @throws RException
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageTbEntryGroupByFlowId")
	public RetMsg<PageInfo<TbEntry>> searchPageTbEntryGroupByFlowId(@RequestBody Map<String,Object> allMap) throws RException {
		Page<TbEntry> page = bookkeepingEntryService.getTbEntryGroupByFlowId(allMap, ParameterUtil.getRowBounds(allMap));
		return RetMsgHelper.ok(page);
	}

	/**
	 * 按套号查询详细帐
	 * @param allMap
	 * @return
	 * @throws RException
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageTbEntryByFlowId")
	public RetMsg<PageInfo<TbEntry>> searchPageTbEntryByFlowId(@RequestBody Map<String,Object> allMap) throws RException {
		Page<TbEntry> page = bookkeepingEntryService.getTbEntryByFlowId(allMap, ParameterUtil.getRowBounds(allMap));
		return RetMsgHelper.ok(page);
	}

	
	/*
	 * 全量下载分录信息
	 */
	@ResponseBody
	@RequestMapping(value = "/downLoadEntries")
	public RetMsg<Serializable> downLoadEntries(@RequestBody Map<String,Object> allMap) throws RException {
		RetMsg<Object> retMsg=bookkeepingEntryService.downLoadEntry(allMap);
		return RetMsgHelper.simple(retMsg.getCode(),retMsg.getDesc());
	}

	/*
	 * 查询数据信息
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageData")
	public RetMsg<PageInfo<TbEntry>> searchPageData(@RequestBody Map<String,Object> allMap) throws RException {
		Page<TbEntry> page = bookkeepingEntryService.searchPageData(allMap, ParameterUtil.getRowBounds(allMap));
		return RetMsgHelper.ok(page);
	}

	/*
	 * 手工调账发送核心
	 */
	@ResponseBody
	@RequestMapping(value = "/sendCore")
    RetMsg<Serializable> sendCore(@RequestBody Map<String,Object> map) throws Exception{
		bookkeepingEntryService.sendCore(map);
		return RetMsgHelper.ok();
	}

	/*
	 * 查询下拉事件
	 */
	@ResponseBody
	@RequestMapping(value = "/searchEvent")
    RetMsg<List<Map<String, String>>> searchEvent(@RequestBody Map<String,Object> map){
		List<Map<String, String>> list2 = tbEventService.searchEvent(map);
		return RetMsgHelper.ok(list2);
	}
	/*
	 * 根据dealNo查询TbEntry表中不重复的下拉事件
	 */
	@ResponseBody
	@RequestMapping(value = "/searchDistinctEventByDealNo")
    RetMsg<List<Map<String, String>>> searchDistinctEventByDealNo(@RequestBody Map<String,Object> map){
		List<Map<String, String>> list2 = tbEventService.searchDistinctEventByDealNo(map);
		return RetMsgHelper.ok(list2);
	}
	/*
	 * 根据dealNo、event查询会计分录模板
	 */
	@ResponseBody
	@RequestMapping(value = "/queryTbEntryTemplateByDealNoAndEvent")
    RetMsg<List<TbEntry>> queryTbEntryTemplateByDealNoAndEvent(@RequestBody Map<String,Object> map){
		List<TbEntry> list2 = bookkeepingEntryService.queryTbEntryTemplateByDealNoAndEvent(map);
		return RetMsgHelper.ok(list2);
	}
	/*
	 * 查询同业存放徐发送数据信息
	 */
	@ResponseBody
	@RequestMapping(value = "/searchTycfEntry")
	public RetMsg<PageInfo<TbEntry>> searchTycfEntry(@RequestBody Map<String,Object> map) throws RException {
		Page<TbEntry> page = bookkeepingEntryService.searchTycfEntry(map, ParameterUtil.getRowBounds(map));
		return RetMsgHelper.ok(page);
	}


	/**
	 * EXCEL报表导出
	 */
	@ResponseBody
	@RequestMapping(value="/exportExcel")
	public void exportExcel(HttpServletRequest request, HttpServletResponse response){
		InputStream is = null;
		OutputStream out = null;
		try {
			@SuppressWarnings("unchecked")
			Map<String, Object> param = requestParamToMap(request);
			logger.info("request转map："+param);
			String reportDate = DateUtil.getCurrentCompactDateTimeAsString();
			/***
			 * excelName需要在前台传，格式为xxx.xls：
			 */
			String excelName = String.valueOf(param.get("excelName"));
			Reports report = Reports.getReportsByFileName(excelName);
			JY.require(report!=null, "缺少报表名称或报表名称未配置！");
			if(report==null){
				logger.info("缺少报表名称或报表名称未配置！");
			}else{
				logger.info("报表名称为："+report.getDesc());
			}


			String ua = request.getHeader("User-Agent");
			//文件名格式转换
			String encode_filename = "";
			encode_filename = URLEncoder.encode(report.getDesc()+ reportDate + ".xls", "UTF-8");
			logger.info("编码后的文件名称为："+encode_filename);

			if (ua != null && ua.indexOf("Firefox/") >= 0) {
				response.setHeader("Content-Disposition", "attachment; filename*=" + encode_filename);
			} else {
				response.setHeader("Content-Disposition", "attachment; filename=" + encode_filename);
			}
			response.setHeader("Connection", "close");


			String str = Reports.getReportModel(report);//获取模板
			logger.info("导出模板所在位置："+str);
			is = new FileInputStream(str);
			XLSTransformer transformer = new XLSTransformer();
			//获取要导出的数据
			Map<String, Object> data = bookkeepingEntryService.getExportList(param);
			logger.info("要导出的数据为："+data);

			Workbook wb = transformer.transformXLS(is, data);
			out = response.getOutputStream();
			wb.write(out);
			out.flush();

		}catch (UnsupportedEncodingException e) {
			JY.raise(e.getMessage());
			logger.error("导出文件名编码异常："+e.getMessage());
		}catch (Exception e) {
			JY.raise(e.getMessage());
			logger.error("EXCEL报表导出异常，异常信息："+e.getMessage());
		}finally{
			if(is!= null){
				try {
					is.close();
				} catch (IOException e) {
					JY.raise(e.getMessage());
					logger.error("输入流InputStream关闭异常");
				}
			}
			if(out!= null){
				try {
					out.close();
				} catch (IOException e) {
					JY.raise(e.getMessage());
					logger.error("输出流OutputStream关闭异常");
				}
			}
		}
	}

	/**
	 * 读取request中的参数,转换为map对象
	 *
	 * @param request
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private HashMap requestParamToMap(HttpServletRequest request) {
		HashMap parameters = new HashMap();
		Enumeration<String> Enumeration = request.getParameterNames();
		while (Enumeration.hasMoreElements()) {
			String paramName = (String) Enumeration.nextElement();
			String paramValue = request.getParameter(paramName);
			// 形成键值对应的map
			if (!StringUtils.isEmpty(paramValue)) {
				parameters.put(paramName, paramValue);
			}
		}
		return parameters;
	}
}
