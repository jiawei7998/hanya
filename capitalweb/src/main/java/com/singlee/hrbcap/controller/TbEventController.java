package com.singlee.hrbcap.controller;

import com.github.pagehelper.PageInfo;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.hrbcap.model.base.TbEvent;
import com.singlee.hrbcap.service.TbEventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/TbEventController")
public class TbEventController extends CommonController {
	@Autowired
	private TbEventService tbEventService;

	/**
	 * 查询会计事件返回列表
	 * @param params
	 * @return
	 */
     
     
	@ResponseBody
	@RequestMapping(value = "/searchTbEventPage")
	public RetMsg<PageInfo<TbEvent>> searchTbEventPage(@RequestBody Map<String, Object> params) {
		return RetMsgHelper.ok(tbEventService.getTbEventPage(params));
	}
	
	@ResponseBody
	@RequestMapping(value = "/searchTbEventAllList")
	public RetMsg<List<TbEvent>> searchTbEventAllList(@RequestBody Map<String, Object> params) {
		List<TbEvent> page = tbEventService.getTbEventAllList(params);
		return RetMsgHelper.ok(page);
	}
	/**
	 * 添加会计事件代码
	 * @param TbEvent
	 * @return
	 */
	
	@ResponseBody
	@RequestMapping(value = "/createTbEvent")
	public RetMsg<Serializable> createTbEvent(@RequestBody TbEvent TbEvent) {
		tbEventService.createTbEvent(TbEvent);
		return RetMsgHelper.ok();
	}
	/**
	 * 修改会计事件代码 
	 * 
	 * @param acctCode
	 */
	@ResponseBody
	@RequestMapping(value = "/updateTbEvent")
	public RetMsg<Serializable> updateTbEvent(@RequestBody TbEvent TbEvent) {
		tbEventService.updateTbEvent(TbEvent);
		return RetMsgHelper.ok();
	}

	/**
	 * 删除会计事件代码，删除条件为eventId
	 * 
	 * @param seq
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteTbEvent")
	public RetMsg<Serializable> deleteTbEvent(@RequestBody HashMap<String,Object> map) {
		tbEventService.deleteTbEvent(ParameterUtil.getString(map, "eventId", ""));
		return RetMsgHelper.ok();
	}
	/**
	 * 
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getProductEvents")
	public RetMsg<List<TbEvent>> getProductEvents(@RequestBody Map<String, Object> map) {
		return RetMsgHelper.ok(tbEventService.getProductEvents(map));
	}
	
}
