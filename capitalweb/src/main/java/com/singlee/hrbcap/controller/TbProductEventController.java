package com.singlee.hrbcap.controller;

import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.hrbcap.service.TbProductEventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.Map;

@Controller
@RequestMapping(value = "/TbProductEventController")
public class TbProductEventController extends CommonController {
	@Autowired
	private TbProductEventService tbProductEventService;

	/**
	 * 查询会计事件返回列表
	 * @param params
	 * @return
	 */
     
	@ResponseBody
	@RequestMapping(value = "/saveTbProductEvent")
	public RetMsg<Serializable> saveTbProductEvent(@RequestBody Map<String,Object> map) {
		String [] eventid = map.get("eventId").toString().split(",");
		tbProductEventService.deleteTbProductEventByProNo(map);
		for(int i=0;i<eventid.length;i++){
			map.put("eventId", eventid[i]);
			tbProductEventService.addTbProductEventService(map);  //循环插入 
		}
		return RetMsgHelper.ok();
		
	}
	
}
