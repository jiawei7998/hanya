package com.singlee.hrbcap.controller;

import com.github.pagehelper.StringUtil;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.hrbcap.model.base.TbProductEventDimension;
import com.singlee.hrbcap.service.TbProductEventDimensionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/TbProductEventDimensionController")
public class TbProductEventDimensionController extends CommonController {
	@Autowired
	private TbProductEventDimensionService tbProductEventDimensionService;

	@ResponseBody
	@RequestMapping(value = "/searchTbProductEventDimensionList")
	public RetMsg<List<TbProductEventDimension>> searchTbProductEventDimensionList(@RequestBody Map<String,Object> map){
		List<TbProductEventDimension> page  = tbProductEventDimensionService.getTbProductEventDimensionList(map);
		return RetMsgHelper.ok(page);
	}
	
	@RequestMapping(value = "/setTbProductEventDimension")
	@ResponseBody
	public RetMsg<Serializable> setTbProductEventDimension(@RequestBody Map<String,Object> paramMap) {
		String prdNo = ParameterUtil.getString(paramMap, "prdNo", "");
		String eventId = ParameterUtil.getString(paramMap, "eventId", "");
		String dimensionIds = ParameterUtil.getString(paramMap, "dimensionIds", "");
		JY.require(StringUtil.isNotEmpty(eventId), "事件不得为空.");
		tbProductEventDimensionService.setTbProductEventDimension(prdNo,eventId,dimensionIds);
		return RetMsgHelper.ok();
	}
}
