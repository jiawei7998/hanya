package com.singlee.hrbcap.controller;

import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.pojo.RoleModule;
import com.singlee.hrbcap.model.base.SceneResultBean;
import com.singlee.hrbcap.model.base.TbProductEventDimensionKv;
import com.singlee.hrbcap.model.base.dimensionKvSaveVo;
import com.singlee.hrbcap.service.TbProductEventDimensionKvService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/TbProductEventDimensionKvController")
public class TbProductEventDimensionKvController extends CommonController {
	@Autowired
	private TbProductEventDimensionKvService tbProductEventDimensionKvService;


	
	@ResponseBody
	@RequestMapping(value = "/searchAllModule")
	public RetMsg<List<RoleModule>> searchAllModule(){
		List<RoleModule> retList = tbProductEventDimensionKvService.searchModuleForReport();
		return RetMsgHelper.ok(retList);
	}
	@ResponseBody
	@RequestMapping(value = "/searchTbDimensionValue")
	public RetMsg<List<SceneResultBean>> searchTbDimensionValue(@RequestBody Map<String,Object> map){
		List<SceneResultBean> retList = tbProductEventDimensionKvService.searchTbDimensionValue(map);
		return RetMsgHelper.ok(retList);
	}
	
	@ResponseBody
	@RequestMapping(value = "/getDataForProductAndEvent")
	public RetMsg<List<TbProductEventDimensionKv>> getDataForProductAndEvent(@RequestBody Map<String,Object> map){
		
		List<TbProductEventDimensionKv> retList = tbProductEventDimensionKvService.getDataForProductAndEvent(map);
		return RetMsgHelper.ok(retList);
	}
	@ResponseBody
	@RequestMapping(value = "/getSceneResultList")
	public RetMsg<List<SceneResultBean>> getSceneResultList(@RequestBody Map<String,Object> map){
		
		List<SceneResultBean> sceneResultList = tbProductEventDimensionKvService.getSceneResultList(map);
		return RetMsgHelper.ok(sceneResultList);
	}
	@RequestMapping(value = "/setTbProductEventDimension")
	@ResponseBody
	public RetMsg<Serializable> setTbProductEventDimension(@RequestBody dimensionKvSaveVo dimensionKvSaveVo) {

		tbProductEventDimensionKvService.saveTbProductEventDimensionKv(dimensionKvSaveVo);
		return RetMsgHelper.ok();
	}

	@ResponseBody
	@RequestMapping(value = "/deleteTbProductEventDimensionKv")
	public RetMsg<Serializable> deleteTbProductEventDimensionKv(@RequestBody Map<String, Object> map) {
		tbProductEventDimensionKvService.deleteTbProductEventDimensionKv(map);
		return RetMsgHelper.ok();
	}
	
	
}
