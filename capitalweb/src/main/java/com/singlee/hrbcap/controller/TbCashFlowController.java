package com.singlee.hrbcap.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.hrbcap.model.base.TbCashFlowDto;
import com.singlee.hrbcap.service.TbCashFlowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * @author Liang
 * @date 2021/10/15 13:13
 * =======================
 */
@Controller
@RequestMapping(value = "/TbCashFlowController")
public class TbCashFlowController extends CommonController {

    @Autowired
    private TbCashFlowService tbCashFlowService;

    /*
     * 查询数据信息
     */
    @ResponseBody
    @RequestMapping(value = "/searchPageData")
    public RetMsg<PageInfo<TbCashFlowDto>> searchPageData(@RequestBody Map<String,Object> map) throws RException {
        Page<TbCashFlowDto> page = tbCashFlowService.getCashFlow(map);
        return RetMsgHelper.ok(page);
    }

}
