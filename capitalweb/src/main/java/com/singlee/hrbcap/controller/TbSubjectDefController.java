package com.singlee.hrbcap.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.hrbcap.model.base.TbSubjectDef;
import com.singlee.hrbcap.service.TbSubjectDefService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/*
 * 科目表controller层
 */
@Controller
@RequestMapping(value = "/TbSubjectDefController")
public class TbSubjectDefController extends CommonController {
	/*
	 * 科目表service层
	 */
	@Autowired
	private TbSubjectDefService tbSubjectDefService;

	/*
	 * 查询科目树信息
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageTbSubjectDef")
	public RetMsg<List<TbSubjectDef>> searchPageTbSubjectDef(@RequestBody Map<String, Object> allMap)
			throws RException {
		List<TbSubjectDef> list = tbSubjectDefService.getTbSubjectDefList(allMap);
		return RetMsgHelper.ok(list);
	}
	
	@ResponseBody
	@RequestMapping(value = "/searchPageTbSubjectDefForTree")
	public RetMsg<List<TbSubjectDef>> searchPageTbSubjectDefForTree(@RequestBody Map<String, Object> allMap)
			throws RException {
		List<TbSubjectDef> list = tbSubjectDefService.getTbSubjectDefListForTree(allMap);
		return RetMsgHelper.ok(list);
	}
	
	/*
	 * 查询科目信息
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageSubject")
	public RetMsg<PageInfo<TbSubjectDef>> searchPageSubject(@RequestBody Map<String, Object> allMap)
			throws RException {
		Page<TbSubjectDef> page = tbSubjectDefService.getTbSubjectDefPageList(allMap);
		return RetMsgHelper.ok(page);
	}


	/*
	 * 修改科目信息
	 */
	@ResponseBody
	@RequestMapping(value = "/updateTbSubjectDef")
	public RetMsg<Serializable> updateTbSubjectDef(@RequestBody TbSubjectDef tTbSubjectDef) throws RException {
		tbSubjectDefService.updateTbSubjectDef(tTbSubjectDef);
		return RetMsgHelper.ok();
	}

	/*
	 * 添加科目信息
	 */
	@ResponseBody
	@RequestMapping(value = "/saveTbSubjectDef")
	public RetMsg<Serializable> saveTbSubjectDef(@RequestBody TbSubjectDef tTbSubjectDef) throws RException {
		tbSubjectDefService.creatTbSubjectDef(tTbSubjectDef);
		return RetMsgHelper.ok();
	}

	/*
	 * 删除科目信息
	 */
	@ResponseBody
	@RequestMapping(value = "/delTbSubjectDef")
	public RetMsg<Serializable> delTbSubjectDef(@RequestBody String[] dbIds) throws RException {
		tbSubjectDefService.delTbSubjectDef(dbIds);
		return RetMsgHelper.ok();
	}
}
