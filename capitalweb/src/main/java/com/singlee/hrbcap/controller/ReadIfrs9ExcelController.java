package com.singlee.hrbcap.controller;

import com.singlee.capital.base.util.FileManage;
import com.singlee.capital.common.pojo.UploadedFile;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.hrbcap.model.base.TbExcelConfig;
import com.singlee.hrbcap.service.ReadIfrs9ExcelService;
import com.singlee.hrbcap.service.TbExcelConfigService;
import jxl.Sheet;
import jxl.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value="/ReadIfrs9ExcelController")
public class ReadIfrs9ExcelController extends CommonController {
	@Autowired
	private TbExcelConfigService excelConfigService;
	@Autowired
	private ReadIfrs9ExcelService readIfrs9ExcelService;
	@ResponseBody
	@RequestMapping(value="/importIfrs9Excel")
	public String importIfrs9Excel(MultipartHttpServletRequest request, HttpServletResponse response){
		String message = "导入成功";
		Workbook wb = null;
		try{
			List<UploadedFile> uploadedFileList = FileManage.requestExtractor(request);
			List<Workbook> excelList = new LinkedList<Workbook>();
			for (UploadedFile uploadedFile : uploadedFileList) {
				JY.ensure("xls".equals(uploadedFile.getType()), "上传文件类型错误,仅允许上传xls格式文件.");
				Workbook book = Workbook.getWorkbook(new ByteArrayInputStream(uploadedFile.getBytes()));
				excelList.add(book);
			}
			JY.require(excelList.size() == 1, "只能处理一份excel");
			wb = excelList.get(0);
			Sheet[] sheets = wb.getSheets();//获得卡片sheet数量
			
			//add by zl 2019-08-20 删除规则基础表数据
			readIfrs9ExcelService.deleteAllConfig();
			//end
			//update by zl 2019-08-20 从第一页开始读取sheet
			for(int i=0;i<sheets.length;i++){ //循环解析产品的会计配置
				//每个sheet页的第一列第三行就是产品号
				String prdNo = StringUtil.trimToNull(sheets[i].getCell(0, 2).getContents());
				if(null != prdNo) {
					TbExcelConfig excelConfig = excelConfigService.geTbExcelConfig(prdNo);
					if(null != excelConfig){
						Map<String, Object> mapParam=new HashMap<String, Object>();
						mapParam.put("prdNo", prdNo);
						readIfrs9ExcelService.deleteAllConfigByPrdNo(mapParam);
						readIfrs9ExcelService.readIfrs9ExcelTransferToDataBaseService(sheets[i], excelConfig.getProductIdCol(), excelConfig.getBeginReadRow(),
							excelConfig.getBeginDimesionCol(), excelConfig.getEndDimesionCol());
					}
				}
			}
			readIfrs9ExcelService.updateProductEventDimensionKvId(new HashMap<String, Object>());
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return e.toString();
		}finally{
			if(null != wb) {
                wb.close();
            }
		} 
		return message;
	}
}
