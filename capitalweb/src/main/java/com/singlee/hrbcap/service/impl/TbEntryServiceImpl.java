package com.singlee.hrbcap.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.mapper.TaSysParamMapper;
import com.singlee.capital.system.model.TaSysParam;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.esb.hbcb.bean.common.ExchangeHeader;
import com.singlee.financial.esb.hbcb.bean.common.MasterHeader;
import com.singlee.financial.esb.hbcb.bean.common.RequestHeader;
import com.singlee.financial.esb.hbcb.bean.s100001001084015.ReqDetail1;
import com.singlee.financial.esb.hbcb.bean.s100001001084015.RequestBody;
import com.singlee.financial.esb.hbcb.model.EsbOutBean;
import com.singlee.financial.esb.hbcb.model.S84015Bean;
import com.singlee.financial.esb.hbcb.service.S100001001084015Service;
import com.singlee.financial.esb.hbcb.util.EsbSend;
import com.singlee.hrbcap.mapper.TbEntryMapper;
import com.singlee.hrbcap.model.acup.TbEntry;
import com.singlee.hrbcap.service.TbEntryService;
import com.singlee.ifs.mapper.IfsEsbMassageMapper;
import com.singlee.ifs.model.IfsEsbMassage;
import com.singlee.refund.mapper.CFtAfpConfMapper;
import net.sf.saxon.functions.Collection;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service("tbEntryService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TbEntryServiceImpl implements TbEntryService {

    @Autowired
    private Ifrs9QueryDownLoadServer Ifrs9QueryDownLoadServer;
    public static final Map<String, Map<String, Object>> downLoadParamMap = new HashMap<String, Map<String, Object>>();

    @Autowired
    private TbEntryMapper bookkeepingEntryMapper;
    
    @Autowired
    private TaSysParamMapper taSysParamMapper;
    
    @Autowired
    private S100001001084015Service s100001001084015Service;
    
    
    
    @Autowired
    private CFtAfpConfMapper cFtAfpMapper;
    
    @Autowired
    private IfsEsbMassageMapper ifsEsbMassageMapper;
    
    
   

    @Override
    public void save(TbEntry entry) {
        // TODO Auto-generated method stub
        bookkeepingEntryMapper.insert(entry);
    }

    @Override
    public void save(List<TbEntry> entryList) {
        // TODO Auto-generated method stub
        if (entryList == null) {
            return;
        }
        for (TbEntry entry : entryList) {
            if (BigDecimal.ZERO.compareTo(BigDecimal.valueOf(entry.getValue())) != 0) {
                //当value大于0时再进入账务表
                save(entry);
            }
        }
    }


    @Override
    public Page<List<TbEntry>> getEntryList(Map<String, Object> map, RowBounds rb) {

        //字符串判空调整
        if ("".equals(ParameterUtil.getString(map, "eventId", "")) &&
                !"9001".equals(ParameterUtil.getString(map, "eventId", ""))) {

            map.put("instId", SlSessionHelper.getInstitutionId(SlSessionHelper.getSlSession()));

        }

        return bookkeepingEntryMapper.selectByMap(map, rb);
    }
    
    
    
    
    @Override
    public Page<List<TbEntry>> getEntrySendList(Map<String, Object> map, RowBounds rb) {

        //字符串判空调整
        if ("".equals(ParameterUtil.getString(map, "eventId", "")) &&
                !"9001".equals(ParameterUtil.getString(map, "eventId", ""))) {

            map.put("instId", SlSessionHelper.getInstitutionId(SlSessionHelper.getSlSession()));

        }

        return bookkeepingEntryMapper.selectByMapSend(map, rb);
    }
    
    

    @Override
    public List<TbEntry> getDealEntryList(Map<String, Object> map) {
        return bookkeepingEntryMapper.selectByMap(map);
    }

    @Override
    public List<TbEntry> selectEntryFlowId(Map<String, Object> map) {
        return bookkeepingEntryMapper.selectEntryFlowId(map);
    }

    @Override
    public void updateEntryState(TbEntry entry) {
        bookkeepingEntryMapper.updateEntryState(entry);
    }

    @Override
    public void updateEntryRetState(TbEntry entry) {
        bookkeepingEntryMapper.updateEntryRetState(entry);
    }

    @Override
    public List<TbEntry> selectByTimeService(Map<String, Object> map) {
        return bookkeepingEntryMapper.selectByTime(map);
    }

    @Override
    public RetMsg<Object> downLoadEntry(Map<String, Object> map) {
        RetMsg<Object> retMsg = new RetMsg<Object>("error.common.0000", "正在下载，请耐心等待...");
        if (!downLoadParamMap.containsKey("TbEntryParam")) {
            //map.put("filePath", "/share_ifbm/DownLoadDocment/"+SlSessionHelper.getUserId(SlSessionHelper.getSlSession())+"/TbkEntry/");
            map.put("filePath", "");
            map.put("DownLoadUser", SlSessionHelper.getUser(SlSessionHelper.getSlSession()).getUserName());
            downLoadParamMap.put("TbEntryParam", map);
            this.createThread();
        } else {
            retMsg = new RetMsg<Object>("error.common.0000", "已有用户【<span style='color:red;'>" + downLoadParamMap.get("TbEntryParam").get("DownLoadUser") + "</span>】的下载任务，请稍后再试...");
        }
        return retMsg;
    }

    protected Thread entryThread = null;

    public void createThread() {
        try {
            if (entryThread != null) {
                System.out.println("===========entryThread.getState():" + entryThread.getState().toString() + "================");
                entryThread = null;
                System.gc();
            }
                entryThread = new Thread(Ifrs9QueryDownLoadServer);
                System.out.println("===========createThread entryThread.getState():" + entryThread.getState().toString() + "================");
                entryThread.start();
                System.out.println("===========entryThread.start() entryThread.getState():" + entryThread.getState().toString() + "================");
        } catch (Exception e) {
            downLoadParamMap.remove("TbEntryParam");
            e.printStackTrace();
        }
    }

    @Override
    public Page<TbEntry> searchPageData(Map<String, Object> map, RowBounds rb) {
        return bookkeepingEntryMapper.searchPageData(map, rb);
    }

    @Override
    public void sendCore(Map<String, Object> map) throws Exception {
        String flowId = ParameterUtil.getString(map, "flowId", null);
        if(StringUtil.isNullOrEmpty(flowId)){
            return;
        }
        List<TbEntry> Tblist = bookkeepingEntryMapper.getTbEntryByFlowId(map,ParameterUtil.getRowBounds(map));
        if (null == Tblist || Tblist.size() == 0) {
            return ;
        }

        // 开始组装数据
        S84015Bean s84015Bean = new S84015Bean();
        Map<String, Object> sysMap = new HashMap<String, Object>();
        sysMap.put("p_type", "ESB");
        List<TaSysParam> sysList = taSysParamMapper.selectTaSysParam(sysMap);
        // 请求头
        RequestHeader requestHeader = new RequestHeader();
        ExchangeHeader exchangeHeader = new ExchangeHeader();
        MasterHeader masterHeader = new MasterHeader();
        // 请求体
        RequestBody requestBody = new RequestBody();
        //组装报文基础信息
        GenerateS84015Bean(requestBody, requestHeader, sysList);
        requestHeader.setBrchNo("00100");// 机构号
        requestHeader.setUUID(EsbSend.createUUID());
        requestHeader.setReqTm(String.valueOf(System.currentTimeMillis()));// 请求方交易时间戳
        requestHeader.setExchangeHeader(exchangeHeader);
        requestHeader.setMasterHeader(masterHeader);

        List<ReqDetail1> detailist = new ArrayList<>();
        ReqDetail1 detail1 = null;
        TbEntry entry = new TbEntry();
        entry.setValue(0);
        //开始筛选借贷对
        for (int i = 1; i < Tblist.size(); i+=2) {
            detail1 = new ReqDetail1();
            List<TbEntry> list = new ArrayList<TbEntry>();
            for (TbEntry tbEntry : Tblist) {
                if(tbEntry.getFlowSeq() == i || tbEntry.getFlowSeq() == (i+1)){
                    list.add(tbEntry);
                }
            }
            //检查借贷是否平衡
            if(list.size() == 2 && list.get(0).getValue() == list.get(1).getValue()){
                for (TbEntry tbEntry : list) {
                    if("D".equals(tbEntry.getDebitCreditFlag())){
                        GenerateAcupDR(detail1, tbEntry.getSubjCode(), "00100");
                    }else{
                        GenerateAcupCR(detail1, tbEntry.getSubjCode(), "00100");
                    }

                    entry.setFlowId(tbEntry.getFlowId());
                    entry.setCcy(tbEntry.getCcy());
                    entry.setValue(entry.getValue()+tbEntry.getValue());
                    entry.setFundCode(tbEntry.getFundCode());
                    entry.setPrdNo(tbEntry.getPrdNo());
                    entry.setEventId(tbEntry.getEventId());
                }
                detail1.setPromptNum(StringUtil.isEmpty(detail1.getPromptNum()) ? "" : detail1.getPromptNum());// O 提示码   新科目号
                detail1.setAmt(new DecimalFormat("0.00").format(list.get(0).getValue()));
            }
            detailist.add(detail1);
        }

        if(detailist.size() < 1){
            return;
        }

        s84015Bean.setReqDetail1(detailist);
        s84015Bean.setRequestBody(requestBody);
        s84015Bean.setRequestHeader(requestHeader);
        EsbOutBean outBean = s100001001084015Service.send(s84015Bean);

        if ("0020010000".equals(outBean.getRetCode())) {// 发送成功
            entry.setSendFlag("3");//发送成功
        }else{
            entry.setSendFlag("2");//发送失败
        }

        entry.setRetMsg(outBean.getRetMsg());
        entry.setRetCode(outBean.getRetCode());
        updateEntryRetState(entry);// 更新处理状态为已成功发送

        // 保存报文
        IfsEsbMassage ifsEsbMassage = new IfsEsbMassage();
        ifsEsbMassage.setDealno(entry.getFlowId().trim());
        ifsEsbMassage.setCustno(entry.getFundCode().trim());
        ifsEsbMassage.setProduct(entry.getPrdNo().trim());
        ifsEsbMassage.setProducttype(entry.getEventId().trim());
        List<IfsEsbMassage> select = ifsEsbMassageMapper.select(ifsEsbMassage);

        if (select.size() == 1) {
            ifsEsbMassage = select.get(0);
            ifsEsbMassage.setSenddate(new Date());
            ifsEsbMassage.setRecdate(new Date());
            ifsEsbMassage.setSendmsg(outBean.getSendmsg());
            ifsEsbMassage.setRecmsg(outBean.getRecmsg());
            ifsEsbMassage.setRescode(outBean.getRetCode());
            ifsEsbMassage.setResmsg(outBean.getRetMsg());
            Example example = new Example(IfsEsbMassage.class);
            example.createCriteria().andEqualTo("dealno", ifsEsbMassage.getDealno())
                    .andEqualTo("custno", ifsEsbMassage.getCustno())
                    .andEqualTo("product", ifsEsbMassage.getProduct())
                    .andEqualTo("producttype", ifsEsbMassage.getProducttype())
                    .andEqualTo("service",ifsEsbMassage.getService());
            ifsEsbMassageMapper.updateByExample(ifsEsbMassage, example);
        } else if (select.size() == 0) {
            ifsEsbMassage.setAmount(String.valueOf(entry.getValue()));
            ifsEsbMassage.setCcy(entry.getCcy());
            ifsEsbMassage.setSetmeans("NOS");
            ifsEsbMassage.setService("S100001001084015");
            ifsEsbMassage.setSenddate(new Date());
            ifsEsbMassage.setRecdate(new Date());
            ifsEsbMassage.setSendmsg(outBean.getSendmsg());
            ifsEsbMassage.setRecmsg(outBean.getRecmsg());
            ifsEsbMassage.setRescode(outBean.getRetCode());
            ifsEsbMassage.setResmsg(outBean.getRetMsg());
            ifsEsbMassageMapper.insert(ifsEsbMassage);
        }
    }

    /**
     * 处理调用核心记账接口相应逻辑
     * @param entryList
     * @param result
     */
    public void handlerSendCoreResult(List<TbEntry> entryList, Map<String, String> result) {
        //如果核心记账成功，修改状态码
        TbEntry te=new TbEntry();
//        if(result.containsKey("RET_CODE") && Constant.SendCoreFlag.equals(result.get("RET_CODE"))){
//            te.setFlowId(entryList.get(0).getFlowId());
//            te.setSendFlag(Constant.SendCoreSuccessFlag);//设置执行状态
//            te.setRetCode(result.get("RET_CODE"));//设置成功编号
//            te.setRetMsg(result.get("RET_MSG"));//设置成功消息
//            te.setQzDate(result.get("TRAN_DATE"));//设置发送时间
//            te.setQzDealno(result.get("RET_SEQ_NO"));//设置核心的响应流水号  前置流水号
//            bookkeepingEntryMapper.updateEntryState(te);
//        }else{
//            te.setFlowId(entryList.get(0).getFlowId());
//            te.setSendFlag(Constant.SendCoreFailFlag);
//            te.setErrCode(result.get("ERROR_CODE"));
//            te.setErrMsg(result.get("ERROR_MSG"));
//            te.setQzDate(result.get("TRAN_DATE"));//设置发送时间
//            te.setQzDealno(result.get("RET_SEQ_NO"));//设置核心的响应流水号
//            bookkeepingEntryMapper.updateEntryState(te);
//        }


    }

    /***
     * 根据dealNo、event查询会计分录模板
     * @param map
     * @return
     */
    @Override
    public List<TbEntry> queryTbEntryTemplateByDealNoAndEvent(Map<String, Object> map) {
        List<TbEntry> list = bookkeepingEntryMapper.queryTbEntryByDealNoAndEventNew(map);
        List<TbEntry> list1 = null;
        if(null != list && list.size() != 0) {
            TbEntry tbEntry = list.get(0);
            map.put("flowId", tbEntry.getFlowId());
            list1 = bookkeepingEntryMapper.queryTbEntryTemplateByDealNoAndEvent(map);
        }

        return list1;
    }

    /**
     * 	查询需发送的同业存放账务
     */
    @Override
    public Page<TbEntry> searchTycfEntry(Map<String, Object> map, RowBounds rb) {
        List<String> list = new ArrayList<String>();
        String sendFlag = ParameterUtil.getString(map, "sendFlag", "");
        String eventArr = ParameterUtil.getString(map, "eventArr", "");
        if(StringUtil.isNotEmpty(sendFlag)) {//保留原有逻辑
            list.add(sendFlag);
        }else{//如果发送标志为空，那么就使用默认配置
//            list=Constant.SendHeXinFlag1;
        }
        map.put("sendFlagOrg", list);
        if(StringUtil.isNotEmpty(eventArr)) {//保留原有逻辑
        	map.put("eventArr", eventArr.split(","));
        }
        return bookkeepingEntryMapper.queryTbEntry(map, rb);
    }

    /***
     * 导出查询
     */
    @Override
    public Map<String, Object> getExportList(Map<String, Object> map) {
        if ("".equals(ParameterUtil.getString(map, "eventId", "")) &&
                !"9001".equals(ParameterUtil.getString(map, "eventId", ""))) {

            map.put("instId", SlSessionHelper.getInstitutionId(SlSessionHelper.getSlSession()));
        }
        List<TbEntry> dataList = bookkeepingEntryMapper.selectByMap(map);
        map.put("dataList", dataList);
        return map;
    }

    @Override
    public Page<TbEntry> getTbEntryGroupByFlowId(Map<String, Object> map, RowBounds rb) {
        return bookkeepingEntryMapper.getTbEntryGroupByFlowId(map, rb);
    }

    @Override
    public Page<TbEntry> getTbEntryByFlowId(Map<String, Object> map, RowBounds rb) {
        return bookkeepingEntryMapper.getTbEntryByFlowId(map, rb);
    }


    /**
     * 组装DR
     * @param detail1
     * @param intglno
     * @param subbr
     */
    public static void GenerateAcupDR(ReqDetail1 detail1, String intglno, String subbr) {
        int length = intglno.trim().length();
        if (length > 2) {// BGL
            detail1.setTfroutAcctNo("0"+StringUtil.stringFormat(intglno, "0", 17, "L"));
            detail1.setTfroutAcctNoSys("BGL");
        } else if (length == 2) {// CGL
            
            detail1.setPromptNum(StringUtil.isEmpty(detail1.getPromptNum()) ? intglno : "");// 两个账号都为CGL 该字段为空
            detail1.setTfroutBrch(subbr);
            detail1.setTfroutProCode(intglno);
        }
    }
    
    /**
     * 组装CR
     * @param detail1
     * @param intglno
     * @param subbr
     */
    public static void GenerateAcupCR(ReqDetail1 detail1, String intglno, String subbr) {
        int length = intglno.trim().length();
        if (length > 2) {// BGL
            detail1.setTfrinAcctNo("0"+StringUtil.stringFormat(intglno, "0", 17, "L"));
            detail1.setTfrinAcctNoSys("BGL");
        } else if (length == 2) {
            detail1.setTfrInProCode(intglno);
            detail1.setPromptNum(StringUtil.isEmpty(detail1.getPromptNum()) ? intglno : "");// 两个账号都为CGL 该字段为空
            detail1.setTfrInBrch(subbr);
        }
    }

    /**
     * 初始化请求报文
     * @param requestBody
     * @param requestHeader
     * @param sysList
     */
    public static void GenerateS84015Bean(RequestBody requestBody, RequestHeader requestHeader,List<TaSysParam> sysList) {
        for (TaSysParam taSysParam : sysList) {
            if ("VerNo".equals(taSysParam.getP_code())) {
                // 版本号 非必输
                requestHeader.setVerNo(taSysParam.getP_value().trim());
            } else if ("ReqSysCd".equals(taSysParam.getP_code())) {
                // 请求方系统代码 非必输
                requestHeader.setReqSysCd(taSysParam.getP_value().trim());
            } else if ("TxnTyp".equals(taSysParam.getP_code())) {
                // 交易类型 非必输
                requestHeader.setTxnTyp(taSysParam.getP_value().trim());
            } else if ("TxnMod".equals(taSysParam.getP_code())) {
                // 交易模式 非必输
                requestHeader.setTxnMod(taSysParam.getP_value().trim());
            } else if ("TerminalType".equals(taSysParam.getP_code())) {
                // 终端类型 输入有效值：0，1，2 0：柜员前端和ESB
                requestHeader.setTerminalType(taSysParam.getP_value().trim());
            } else if ("Flag1".equals(taSysParam.getP_code())) {
                // 标识1 0-正常交易 1-倒退日交易 2-正常授权交易 3-倒退日授权交易 4,5,6,7ATM/POS交易使用 倒退日交易见详细的各个模块交易说明
                requestHeader.setFlag1(taSysParam.getP_value().trim());
            } else if ("Flag4".equals(taSysParam.getP_code())) {
                // 标识4 渠道标识： 0-柜面 5-ESB（表示渠道交易）
                requestHeader.setFlag4(taSysParam.getP_value().trim());
            } else if ("ChnlNo".equals(taSysParam.getP_code())) {
                // 渠道号
                requestHeader.setChnlNo(taSysParam.getP_value().trim());
                // 渠道编号
                requestBody.setChannelNo(taSysParam.getP_value().trim());
            } else if ("TrmNo".equals(taSysParam.getP_code())) {
                // 终端号 柜员的终端号是通过银行内部分配的；渠道的终端号为“000”。
                requestHeader.setTrmNo(taSysParam.getP_value().trim());
            } else if ("TlrNo".equals(taSysParam.getP_code())) {
                // 柜员号
                requestHeader.setTlrNo(taSysParam.getP_value().trim());
            } else if ("BancsSeqNo".equals(taSysParam.getP_code())) {
                // 流水号 输入填0，输出为bancs的流水号. 多金融组合交易的流水号是一样的
                requestHeader.setBancsSeqNo(taSysParam.getP_value().trim());
            } else if ("SupervisorID".equals(taSysParam.getP_code())) {
                // 复核或授权时输入主管id号，复核或授权的时候才有效，其他建议填”0000000”。主管ID只提供给柜面前端使用，其他外围系统的授权处理在各自的业务系统
                requestHeader.setSupervisorID(taSysParam.getP_value().trim());
            }
        }
    
    
    
    }
}
