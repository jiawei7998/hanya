package com.singlee.hrbcap.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.JY;
import com.singlee.hrbcap.model.base.TbDimensionValue;
import com.singlee.hrbcap.service.TbDimensionValueService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/*
 * 会计事件表serviceIMpl
 */
@Service("tbDimensionValueService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TbDimensionValueServiceImpl implements TbDimensionValueService {
	@Autowired
	private com.singlee.hrbcap.mapper.TbDimensionValueMapper TbDimensionValueMapper;
	
	@Override
	public Page<TbDimensionValue> getTbDimensionValuePage(Map<String, Object> map, RowBounds rb) {
		return TbDimensionValueMapper.getTbDimensionValuePage(map, rb);
	}
	
	@Override
	public List<TbDimensionValue> getTbDimensionValueList(Map<String, Object> map) {
		return TbDimensionValueMapper.getTbDimensionValueList(map);
	}
	@Override
	public void createTbDimensionValue(TbDimensionValue TbDimensionValue) {
		if (TbDimensionValueMapper.checkTbDimensionValueByDbId(TbDimensionValue.getDbId()) == 0) {
			TbDimensionValueMapper.insert(TbDimensionValue);
		} else {
			JY.raiseRException(("DBID已经存在"));
		}
		
	}

	@Override
	public void updateTbDimensionValue(TbDimensionValue TbDimensionValue) {
		if (TbDimensionValueMapper.checkTbDimensionValueByDbId(TbDimensionValue.getDbId()) == 0) {
			JY.raiseRException(("DBID不存在"));
		} else {
			TbDimensionValueMapper.updateByPrimaryKey(TbDimensionValue);
		}
	}

	@Override
	public void deleteTbDimensionValue(String DbId) {
		
		TbDimensionValueMapper.deleteByPrimaryKey(DbId);
	}


	

	


}
