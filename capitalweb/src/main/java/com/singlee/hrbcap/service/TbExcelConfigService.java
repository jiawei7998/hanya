package com.singlee.hrbcap.service;


import com.singlee.hrbcap.model.base.TbExcelConfig;

public interface TbExcelConfigService {
	/**
	 * 根据产品获得配置
	 * @param prdNo
	 * @return
	 */
	TbExcelConfig geTbExcelConfig(String prdNo);
}
