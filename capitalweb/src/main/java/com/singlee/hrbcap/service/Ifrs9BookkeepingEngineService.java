package com.singlee.hrbcap.service;


import com.singlee.cap.accounting.model.InstructionPackage;
import com.singlee.capital.common.pojo.Pair;
import com.singlee.hrbcap.model.acup.TbEntry;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * IFRS9会计引擎驱动入口
 * @author louhuanqing
 *
 */
public interface Ifrs9BookkeepingEngineService {
	/**
	 * 指令入口
	 * @param instructionPackage
	 * @return
	 */
	public Pair<String,List<TbEntry>> generateEntryIFRS9Instruction(InstructionPackage instructionPackage, String flowId);
	/**
	 * 协同会计处理入口
	 * @param entryList
	 * @return
	 */
	public String generateEntryIFRS9Entrance(InstructionPackage instructionPackage,String flowId);
	/**
	 * 预先处理
	 * @param instructionPackage
	 */
	public void dealPretreatment(InstructionPackage instructionPackage);
	
	/**
	 * 根据分录更新余额
	 * @param entryList
	 */
	public void updateBookkeepingBalance(List<TbEntry> entryList);
	
	/**
	 * 通过分录更新科目余额
	 * @param entry
	 */
	public void updateBookkeepingBalance(TbEntry entry);
	
	/**
	 * 更新科目余额汇总
	 * @param entry
	 */
	public void updateTbkSubjectBalanceSum(TbEntry entry);
	
	/**
	 * 会计场景 预校验
	 * @param instructionPackage
	 */
	public void checkInfoSceneEntry(InstructionPackage instructionPackage);
	
	/**
	 * @author guomeijuan 20191025
	 * 计算当天利息
	 * 计算公式：amt*rate*(endDate-beginDate)/basis-amt*rate*(endDate-beginDate-1)/basis
	 * @param beginDate
	 * @param endDate
	 * @param params
	 * @return
	 */
	public BigDecimal calcInt(String beginDate,String endDate,Map<String,Object> params); 
	
	/**
	 * @author guomeijuan 20191025
	 * 生成起息日至账务日期的利息帐以及收息帐
	 * @param params 出账参数，必须包含单号dealNo、利率rate、计息基础basis、起息日期vdate、到期日期mdate、本金amt以及账务所需的必要的参数
	 * @return SYS_TOTAL_INT 未收利息总和  ACT_TOTAL_INT 已收利息总和
	 */
	public Map<String,Object> generatePreInt(Map<String,Object> params);
	
	/**系统内资金往来
	 * 生成起息日至账务日期的利息帐以及收息帐
	 * @param params 出账参数，必须包含单号dealNo、利率rate、计息基础basis、起息日期vdate、到期日期mdate、本金amt以及账务所需的必要的参数
	 * @return SYS_TOTAL_INT 未收利息总和  ACT_TOTAL_INT 已收利息总和
	 */
	public Map<String,Object> generatePreXTNInt(Map<String,Object> params);
	
	/***
	 * 倒起息-负债定期使用
	 * 生成起息日至账务日期的利息账以及收息账
	 * 
	 * @param params
	 * @return
	 */
	public Map<String,Object> generatePreIntForDepositDq(Map<String,Object> params);
	
	/***
	 * 倒起息-负债活期使用
	 * 生成起息日至账务日期的利息账以及收息账
	 * 
	 * @param params
	 * @return
	 */
	public Map<String,Object> generatePreIntForDepositHq(Map<String,Object> params);

	/***
	 * 倒起息-行内资金转移使用
	 * 生成起息日至账务日期的利息账以及到期账
	 * 注：行内资金转移无收付息计划
	 * @param params
	 * @return
	 */
	public Map<String,Object> generatePreIntForHnzjzy(Map<String,Object> params);
	/**
	 * 
	 * @param beginDate  起息日
	 * @param endDate    到期日
	 * @param postDate   账务日期
	 * @param amt        收息金额
	 * @return
	 */
	public BigDecimal calculateIntBefore(String beginDate,String endDate,String postDate,BigDecimal amt);
	/**
	 * 获取日期区间利息
	 * @param beginDate  起息日
	 * @param endDate    到期日
	 * @param postDate   账务日期
	 * @param amt        收息金额
	 * @return
	 */
	public BigDecimal calcIntervalInt(String beginDate, String endDate, Map<String, Object> params);
	

}