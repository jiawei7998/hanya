package com.singlee.hrbcap.service;

import com.github.pagehelper.Page;
import com.singlee.hrbcap.model.base.TbDimensionValue;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

public interface TbDimensionValueService {
	/**
	 * 分页
	 * @param map
	 * @param rb
	 * @return
	 */
	public Page<TbDimensionValue> getTbDimensionValuePage(Map<String,Object> map, RowBounds rb);
	
	public List<TbDimensionValue> getTbDimensionValueList(Map<String,Object> map);
		
	/**
	 * 添加会计维度代码
	 * 
	 * @param acctCode
	 */
	public void createTbDimensionValue(TbDimensionValue tbDimension);

	/**
	 * 修改会计维度代码
	 * 
	 * @param acctCode
	 */
	public void updateTbDimensionValue(TbDimensionValue tbDimension);

	/**
	 * 删除会计维度代码，删除条件为aKey
	 * 
	 * @param seq
	 */
	public void deleteTbDimensionValue(String eventId);
	
	
}
