package com.singlee.hrbcap.service.impl;

import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.dayend.service.DayendDateServiceProxy;
import com.singlee.capital.duration.service.DurationConstants;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.hrbcap.service.TbCashFlowService;
import com.singlee.hrbcap.util.Ifrs9Constants;
import com.singlee.hrbcap.util.TradeConstants;
import com.singlee.ifs.mapper.IfsApprovermbDepositMapper;
import com.singlee.ifs.mapper.IfsOpicsCustMapper;
import com.singlee.refund.mapper.CFtInfoMapper;
import com.singlee.refund.mapper.CFtTposMapper;
import com.singlee.refund.model.*;
import com.singlee.refund.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 *  清算完成后生成成本帐
 * 20191023
 */
@Service("HrbCapSettleOrderCallback")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class HrbCapSettleOrderCallback {
    @Autowired
    private DayendDateService dayendDateService;
    @Autowired
    CFtAfpService cFtAfpService;
    @Autowired
    CFtAfpConfService cFtAfpConfService;
    @Autowired
    CFtRdpConfService cFtRdpConfService;
    @Autowired
    CFtReinService fundReinService;
    @Autowired
    HrbAccountCallback hrbAccountCallback;
    @Autowired
    TbCashFlowService tbCashFlowService;
    @Autowired
    CFtReddService fundReddService;
    @Autowired
    CFtTposMapper ftTposMapper;
    @Autowired
    IfsApprovermbDepositMapper ifsApprovermbDepositMapper;
    @Autowired
    IfsOpicsCustMapper custMapper;// 用于查询 对方机构
    @Autowired
    CFtInfoMapper cFtInfoMapper;

    /**
     * 清算完成生成成本帐
     * @param map
     */
    public void generateAcup(Map<String, Object> map) {
        JY.info("********************************HrbCapSettleOrderCallback:成本帐开始生成**************************************");
        /**
         * 1.获取原交易
         */
        Map<String, Object> acupMap = new HashMap<String, Object>();
        String dealNo = ParameterUtil.getString(map, "serial_no", "");
        String prdNo = ParameterUtil.getString(map, "prdNo", "");

        //acupInfoParam 账务所需参数
        Map<String, Object> acupInfoParam = new HashMap<String, Object>();
        String postDate = dayendDateService.getDayendDate();
        acupInfoParam.put("postDate", postDate);

        acupMap.put("dealNo", dealNo);

        acupInfoParam.put("prdNo", prdNo);
        acupInfoParam.put("eventId", Ifrs9Constants.Ifrs9EventCode.Event_VDATE);
        acupInfoParam.put("ccy", DictConstants.Currency.CNY);
        acupInfoParam.put("dealNo", dealNo);
        //amtParam 金额参数
        Map<String, Object> amtParam = new HashMap<String, Object>();
        /**
         * 2.根据取到的交易单号获取原交易信息
         */
        if (TradeConstants.ProductCode.CURRENCY_FUND.equals(prdNo)
                || TradeConstants.ProductCode.BOND_FUND.equals(prdNo)||
                TradeConstants.ProductCode.SPEC_FUND.equals(prdNo)) {//货币基金，债券基金,专户基金
            JY.info("*******************************"+dealNo+"基金申购开始生成*******************************");

            //获取基金交易
            CFtAfp ftAfp = cFtAfpService.selectTaskIdByDealNo(dealNo);
            JY.require(ftAfp!=null,"交易不存在，请确认单号是否存在");
            acupInfoParam.put("dealNo", ftAfp.getDealNo());
            //插入基金代码   交易对手 交易对手名称
            acupInfoParam.put("fundCode",ftAfp.getFundCode());
            acupInfoParam.put("cust",ftAfp.getCno());
            acupInfoParam.put("cname",ftAfp.getCname());
            acupInfoParam.put("tdate",ftAfp.getTdate());

            amtParam.put(DurationConstants.AccType.LOAN_AMT, ftAfp.getAmt());
            acupInfoParam.put("tbAccountType", ftAfp.getInvType());

            acupInfoParam.put("sponinst",ftAfp.getSponInst());
            acupInfoParam.put("instaffiliation",ftAfp.getSponInst());
            acupInfoParam.put("qzDealno",ftAfp.getDealNo());

        }
        String flowId=hrbAccountCallback.packageAcupParams(acupInfoParam, amtParam);
        JY.info("********************************"+dealNo+":账务生成成功**************************************");
        //保存成本现金流
        tbCashFlowService.saveCashFlow(acupInfoParam,amtParam);

    }
    /**
     * 生成到期账务
     * @param
     */
    public void generateTerminationAcup(Map<String, Object> map) {
        JY.info("********************************HrbCapSettleOrderCallback:到期账务开始生成**************************************");
        //acupInfoParam 账务所需参数
        Map<String,Object> acupInfoParam = new HashMap<String, Object>();
        //amtParam 金额参数 需要传配置的表达式需要的参数
        Map<String,Object> amtParam = new HashMap<String, Object>();
        String dealNo = ParameterUtil.getString(map, "serial_no", "");
        JY.require(StringUtil.isNotBlank(dealNo), "serial_no为空");
        String prdNo = ParameterUtil.getString(map, "prdNo", "");
        JY.require(StringUtil.isNotBlank(prdNo), "prdNo为空");
        String trdType = ParameterUtil.getString(map, "trdType", "");//交易类型
        acupInfoParam.put("postDate", dayendDateService.getDayendDate());

        if(TradeConstants.TrdType.FUND_RDPCONF.equals(trdType)){//基金赎回账务
            JY.info("*******************************"+dealNo+"基金赎回账务开始生成*******************************");
            //1、获取赎回确认信息
            CFtRdpConf ftRdpConf = cFtRdpConfService.searchCFtRdpConfByDealNo(dealNo);
            acupInfoParam.put("dealNo", ftRdpConf.getDealNo());
            acupInfoParam.put("fundCode",ftRdpConf.getFundCode());
            acupInfoParam.put("sponinst",ftRdpConf.getSponInst());
            acupInfoParam.put("instaffiliation",ftRdpConf.getSponInst());
            acupInfoParam.put("qzDealno", ftRdpConf.getDealNo());
            //插入基金代码  基金名称  交易对手
            acupInfoParam.put("fundCode",ftRdpConf.getFundCode());
            acupInfoParam.put("cust",ftRdpConf.getCno());
            acupInfoParam.put("cname",ftRdpConf.getCname());
            acupInfoParam.put("tdate",ftRdpConf.getTdate());

            //2、获取持仓信息
            map.clear();
            map.put("fundCode",ftRdpConf.getFundCode());
            map.put("prdNo",prdNo);
            map.put("sponinst",ftRdpConf.getSponInst());
            map.put("invtype",ftRdpConf.getInvType());
            map.put("postdate", DayendDateServiceProxy.getInstance().getSettlementDate());

            CFtTpos ftTpos = ftTposMapper.geCFtTposByContion(map);


            //赎回成本
            BigDecimal rdpAmt = ftRdpConf.getAmt();
            //赎回红利
            BigDecimal intAmt = ftRdpConf.getIntamt();

            amtParam.put(DurationConstants.AccType.LOAN_AMT,rdpAmt);
            amtParam.put(DurationConstants.AccType.AMT_INTEREST, intAmt);

            if(TradeConstants.ProductCode.CURRENCY_FUND.equals(prdNo)){//货币基金
                //基金基本信息
                CFtInfo cFtInfo= cFtInfoMapper.selectByPrimaryKey(ftRdpConf.getFundCode());
                //净值类型
                acupInfoParam.put("priceType", cFtInfo.getNetWorthType());
            }

            if(DictConstants.YesNo.YES.equals(ftRdpConf.getIsConfirm())) {//全部赎回
                acupInfoParam.put("rdpType","1");
                //获取前一日估值
                amtParam.put(DurationConstants.AccType.LOAN_ASSMT_LAST,ftTpos.getRevalAmt());
                JY.info("*******************************"+dealNo+"全部赎回--本金"+rdpAmt+",分红"+intAmt+"冲减估值"+ftTpos.getRevalAmt()+"*******************************");

            }else{//部分赎回
                acupInfoParam.put("rdpType","0");
                JY.info("*******************************"+dealNo+"部分赎回--本金"+rdpAmt+",分红"+intAmt+"*******************************");
            }

            acupInfoParam.put("prdNo", ftRdpConf.getPrdNo());
            acupInfoParam.put("eventId",Ifrs9Constants.Ifrs9EventCode.Event_MDATE);
            acupInfoParam.put("ccy", ftTpos.getCcy());

            acupInfoParam.put("acupInst", ftRdpConf.getSponInst());
            acupInfoParam.put("tbAccountType",ftRdpConf.getInvType());

        }

        hrbAccountCallback.packageAcupParams(acupInfoParam, amtParam);
        JY.info("********************************HrbCapSettleOrderCallback:到期账务账务生成成功**************************************");
        //保存现金流
        tbCashFlowService.saveCashFlow(acupInfoParam,amtParam);
    }

    /**
     *  分红账务
     */
    public void generateBOND(Map<String, Object> map){
        JY.info("********************************HrbCapSettleOrderCallback:分红账务开始生成**************************************");
        //acupInfoParam 账务所需参数
        Map<String,Object> acupInfoParam = new HashMap<String, Object>();
        //amtParam 金额参数 需要传配置的表达式需要的参数
        Map<String,Object> amtParam = new HashMap<String, Object>();

        acupInfoParam.put("postDate", dayendDateService.getDayendDate());
        String dealNo = ParameterUtil.getString(map, "serial_no", "");
        JY.require(StringUtil.isNotBlank(dealNo), "serial_no为空");
        String prdNo = ParameterUtil.getString(map, "prdNo", "");
        JY.require(StringUtil.isNotBlank(prdNo), "prdNo为空");
        /**
         * 1.根据单号取分红账务
         */
        CFtRedd ftRedd = fundReddService.selectCFtReddByDealNo(dealNo);
        //账务参数中必填字段dealNo、sponinst、ccy、eventId、prdNo/ 其他的为维度参数：tbAccountTye、CurrencyType、LaunchInst
        acupInfoParam.put("dealNo", ftRedd.getDealNo());
        acupInfoParam.put("tbAccountType", ftRedd.getInvType());
        acupInfoParam.put("eventId",Ifrs9Constants.Ifrs9EventCode.Event_BOND);
        acupInfoParam.put("prdNo", ftRedd.getPrdNo());
        if(TradeConstants.ProductCode.SPEC_FUND.equals(prdNo)){//专户基金
            //基金基本信息
            CFtInfo cFtInfo= cFtInfoMapper.selectByPrimaryKey(ftRedd.getFundCode());
            //保本类型
            acupInfoParam.put("riskType", cFtInfo.getRiskType());
        }
        if(TradeConstants.ProductCode.CURRENCY_FUND.equals(prdNo)){//货币基金
            //基金基本信息
            CFtInfo cFtInfo= cFtInfoMapper.selectByPrimaryKey(ftRedd.getFundCode());
            //净值类型
            acupInfoParam.put("priceType", cFtInfo.getNetWorthType());
            //分红类型
            acupInfoParam.put("BonusWay",cFtInfo.getBonusWay());
        }

        acupInfoParam.put("sponinst",ftRedd.getSponInst());
        acupInfoParam.put("instaffiliation",ftRedd.getSponInst());
        acupInfoParam.put("ccy", ftRedd.getCcy());
        acupInfoParam.put("qzDealno", ftRedd.getDealNo());
        //插入基金代码  基金名称  交易对手
        acupInfoParam.put("fundCode",ftRedd.getFundCode());
        acupInfoParam.put("cust",ftRedd.getCno());
        acupInfoParam.put("cname",ftRedd.getCname());
        acupInfoParam.put("tdate",ftRedd.getTdate());
        //分红金额
        amtParam.put(DurationConstants.AccType.LOAN_BOND,ftRedd.getAmt());

        hrbAccountCallback.packageAcupParams(acupInfoParam, amtParam);
        JY.info("********************************HrbCapSettleOrderCallback:分红账务开始生成**************************************");
        //保存现金流
        tbCashFlowService.saveCashFlow(acupInfoParam,amtParam);
    }

    /**
     *  红利再投账务
     */
    public void generateDRTAcup(Map<String, Object> map){
        JY.info("********************************HrbCapSettleOrderCallback:红利再投账务开始生成**************************************");
        //acupInfoParam 账务所需参数
        Map<String,Object> acupInfoParam = new HashMap<String, Object>();
        //amtParam 金额参数 需要传配置的表达式需要的参数
        Map<String,Object> amtParam = new HashMap<String, Object>();

        acupInfoParam.put("postDate", dayendDateService.getDayendDate());

        String dealNo = ParameterUtil.getString(map, "serial_no", "");
        JY.require(StringUtil.isNotBlank(dealNo), "serial_no为空");
        String prdNo = ParameterUtil.getString(map, "prdNo", "");
        JY.require(StringUtil.isNotBlank(prdNo), "prdNo为空");
        /**
         * 1.根据单号取红利再投信息
         */
        CFtRein ftRein = fundReinService.selectCFtReinByDealNo(dealNo);
        //账务参数中必填字段dealNo、sponinst、ccy、eventId、prdNo/ 其他的为维度参数：tbAccountType、CurrencyType、LaunchInst
        acupInfoParam.put("dealNo", ftRein.getDealNo());
        acupInfoParam.put("tbAccountType", ftRein.getInvType());
        acupInfoParam.put("eventId",Ifrs9Constants.Ifrs9EventCode.Event_DRT);

        acupInfoParam.put("prdNo", ftRein.getPrdNo());
        acupInfoParam.put("sponinst",ftRein.getSponInst());
        acupInfoParam.put("instaffiliation",ftRein.getSponInst());
        acupInfoParam.put("ccy", ftRein.getCcy());
        //插入基金代码  基金名称  交易对手
        acupInfoParam.put("fundCode",ftRein.getFundCode());
        acupInfoParam.put("cust",ftRein.getCno());
        acupInfoParam.put("cname",ftRein.getCname());
        acupInfoParam.put("tdate",ftRein.getVdate());
        acupInfoParam.put("qzDealno", ftRein.getDealNo());


        //红利再投金额
        amtParam.put(DurationConstants.AccType.LOAN_DRT,ftRein.geteAmt());

        hrbAccountCallback.packageAcupParams(acupInfoParam, amtParam);
        JY.info("********************************HrbCapSettleOrderCallback:红利再投账务生成成功**************************************");
        //保存现金流
        tbCashFlowService.saveCashFlow(acupInfoParam,amtParam);

    }

    /**
     * //申购确认完成后，需要根据确认金额与申购金额作比较，如果确认金额小于申购金额，则需要冲掉差值
     * @param map
     */
    public boolean generateVerifyAcup(Map<String, Object> map) {
        JY.info("********************************HrbCapSettleOrderCallback:申购确认账务开始生成**************************************");
        //acupInfoParam 账务所需参数
        Map<String,Object> acupInfoParam = new HashMap<String, Object>();
        acupInfoParam.put("postDate", dayendDateService.getDayendDate());
        //amtParam 金额参数 需要传配置的表达式需要的参数
        Map<String,Object> amtParam = new HashMap<String, Object>();


        /**
         * 1.根据确认单号取申购确认信息
         */
        String serial_no = ParameterUtil.getString(map, "serial_no", "");
        JY.require(StringUtil.isNotBlank(serial_no), "serial_no为空");
        
        String refNo = ParameterUtil.getString(map, "refNo", "");
        JY.require(StringUtil.isNotBlank(refNo), "refNo为空");

        CFtAfpConf ftAfpConf = cFtAfpConfService.searchCFtAfpConfByDealNo(serial_no);
        map.put("dealNo", ftAfpConf.getRefNo());
        //获取申购复核金额
        CFtAfp ftAfp = cFtAfpService.selectTaskIdByDealNo(ftAfpConf.getRefNo());

        //账务参数中必填字段dealNo、sponinst、ccy、eventId、prdNo/ 其他的为维度参数：tbAccountType、CurrencyType、LaunchInst
        acupInfoParam.put("dealNo", ftAfp.getDealNo());
        acupInfoParam.put("tbAccountType", ftAfp.getInvType());
        acupInfoParam.put("eventId",Ifrs9Constants.Ifrs9EventCode.Event_VERIFY);
        acupInfoParam.put("prdNo", ftAfp.getPrdNo());

        acupInfoParam.put("sponinst",ftAfp.getSponInst());
        acupInfoParam.put("instaffiliation",ftAfp.getSponInst());
        acupInfoParam.put("ccy", ftAfp.getCcy());
        acupInfoParam.put("qzDealno", serial_no);

        //插入基金代码  基金名称  交易对手
        acupInfoParam.put("fundCode",ftAfp.getFundCode());
        acupInfoParam.put("cust",ftAfp.getCno());
        acupInfoParam.put("cname",ftAfp.getCname());
        acupInfoParam.put("tdate",ftAfp.getTdate());

        if(ftAfp.getAmt().equals(ftAfpConf.getAmt().add(ftAfpConf.getHandleAmt()))&&!TradeConstants.ProductCode.BOND_FUND.equals(ftAfp.getPrdNo())){
            return false;
        }
        //申购金额
        amtParam.put(DurationConstants.AccType.LOAN_AMT,ftAfp.getAmt());
        //申购确认费用
        amtParam.put(DurationConstants.AccType.FEE,ftAfpConf.getHandleAmt()==null?0:ftAfpConf.getHandleAmt());
        //申购确认金额
        amtParam.put(DurationConstants.AccType.LOAN_VAMT,ftAfpConf.getAmt());

        hrbAccountCallback.packageAcupParams(acupInfoParam, amtParam);
        JY.info("********************************HrbCapSettleOrderCallback:申购确认账务生成成功**************************************");
        //保存现金流
        tbCashFlowService.saveCashFlow(acupInfoParam,amtParam);

        if(ftAfp.getAmt().equals(ftAfpConf.getAmt().add(ftAfpConf.getHandleAmt()))){
            return false;
        }
        return true;
    }
}
