package com.singlee.hrbcap.service;

import java.util.Map;

public interface TbProductEventService {
	public void addTbProductEventService (Map<String, Object> map);
	
	public void deleteTbProductEventByProNo(Map<String, Object> map);

}
