package com.singlee.hrbcap.service;

import com.github.pagehelper.PageInfo;
import com.singlee.hrbcap.model.base.TbEvent;

import java.util.List;
import java.util.Map;

public interface TbEventService {
	/**
	 * 分页
	 * @param map
	 * @param rb
	 * @return
	 */
	public PageInfo<TbEvent> getTbEventPage(Map<String,Object> map);
	public List<TbEvent> getTbEventAllList(Map<String,Object> map);	
	/**
	 * 添加会计事件代码
	 * 
	 * @param acctCode
	 */
	public void createTbEvent(TbEvent TbEvent);

	/**
	 * 修改会计事件代码
	 * 
	 * @param acctCode
	 */
	public void updateTbEvent(TbEvent TbEvent);

	/**
	 * 删除会计事件代码，删除条件为aKey
	 * 
	 * @param seq
	 */
	public void deleteTbEvent(String eventId);
	
	/**
	 * 获得产品项下的事件
	 * @param map
	 * @return
	 */
	List<TbEvent> getProductEvents(Map<String, Object> map);
	/**
	 * 查询下拉事件
	 * @param params
	 * @return
	 */
	public List<Map<String, String>> searchEvent(Map<String, Object> params);
	/**
	 * 根据dealNo查询TbEntry表中不重复的下拉事件
	 * @param params
	 * @return
	 */
	public List<Map<String, String>> searchDistinctEventByDealNo(Map<String, Object> params);

}
