package com.singlee.hrbcap.service;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.hrbcap.model.base.TbCashFlow;
import com.singlee.hrbcap.model.base.TbCashFlowDto;

import java.util.Map;

public interface TbCashFlowService {
    /**
     * 保存现金流信息
     * @param acupInfoParam 账务参数
     * @param amtParam 账务金额参数
     */
    public void saveCashFlow(Map<String, Object> acupInfoParam, Map<String, Object> amtParam);

    public Page<TbCashFlowDto> getCashFlow(Map<String, Object> map);
    /**
     * 查询现金流信息
     * @param map
     * @return
     */
    public PageInfo<TbCashFlow> queryCashFlow(Map<String, Object> map);
}
