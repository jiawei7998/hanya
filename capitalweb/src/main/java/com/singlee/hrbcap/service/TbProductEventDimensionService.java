package com.singlee.hrbcap.service;


import com.singlee.hrbcap.model.base.TbProductEventDimension;

import java.util.List;
import java.util.Map;

public interface TbProductEventDimensionService {

	public List<TbProductEventDimension> getTbProductEventDimensionList(Map<String,Object> map);
	
	public void setTbProductEventDimension(String prdNo,String eventId,String dimensionIds);

}
