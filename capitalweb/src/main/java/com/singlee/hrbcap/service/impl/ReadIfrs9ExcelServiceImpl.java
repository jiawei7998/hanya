package com.singlee.hrbcap.service.impl;

import com.singlee.capital.common.pojo.Pair;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.dict.model.TaDictVo;
import com.singlee.capital.dict.service.DictionaryGetService;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.hrbcap.mapper.*;
import com.singlee.hrbcap.model.base.*;
import com.singlee.hrbcap.service.ReadIfrs9ExcelService;
import jxl.Sheet;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("readIfrs9ExcelService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class ReadIfrs9ExcelServiceImpl implements ReadIfrs9ExcelService {
	@Autowired
	private TbProductEventDimensionKvMapper productEventDimensionKvMapper;
	@Autowired
	private TbEventMapper eventMapper;
	@Autowired
	private DictionaryGetService dictionaryGetService;
	@Autowired
	private TbDimensionMapper dimensionMapper;
	@Autowired
	private TbDimensionValueMapper dimensionValueMapper;

	@Autowired
	private TbProductEventSceneAcupMapper productEventSceneAcupMapper;
	@Autowired
	private TbProductEventMapper productEventMapper;
	@Autowired
	private TbProductEventDimensionMapper productEventDimensionMapper;
	
	private static Logger accountLog = LoggerFactory.getLogger("ACCOUNT");
	
	@Override
	public void readIfrs9ExcelTransferToDataBaseService(Sheet sheet,int productIdCol,int beginReadRow,int beginDimesionCol ,int endDimesionCol) {
		// TODO Auto-generated method stub
		String sheetName  = sheet.getName();//获得sheet的名称
		accountLog.info("当前解析的配置页："+sheetName);
		int rowCount = sheet.getRows();
		//默认读取产品代码位 productIdCol=0 beginReadRow=2
		String prdNo = sheet.getCell(productIdCol, beginReadRow+1).getContents();//产品代码prdNo
		String eventId = "";
		int beginRow = 0;//设定读取事件追溯最小的行
		List<Pair<String, String>> pairs = new ArrayList<Pair<String,String>>();
		Pair<String, String> pair = null;
		StringBuffer rowKey = new StringBuffer();
		rowKey.append(StringUtil.trimToEmpty(prdNo));
		String rowKeyOld = "";//用于标记行的KEY唯一区别的值
		String groupId = "";//组别
		int dimensionRow = beginReadRow;//标记第几列是说明行 beginReadRow=2
		Map<String, Object> map = new HashMap<String, Object>();
		if(rowCount>0) {
			//读取列头
			int colCount = sheet.getColumns();
			//处理当前页面产品的维度 tb_dimension  beginDimesionCol=4 endDimesionCol=5
			for(int j=beginDimesionCol;j<=endDimesionCol;j++) {
				String desc = sheet.getCell(j, dimensionRow).getContents();
				TaDictVo taDictVo = dictionaryGetService.getTaDictByCodeAndValue("dimension", desc);
				String dimensionId = (taDictVo==null?desc:taDictVo.getDict_key());
				TbDimension dimension  = new TbDimension();
				dimension.setDimensionId(dimensionId);
				dimension.setDimensionName(desc);
				dimension.setIsActive(DictConstants.YesNo.YES);
				dimension.setDimensionSort(this.eventMapper.getGroupId());
				map.put("dimensionId", dimensionId);
				if(null == this.dimensionMapper.geTbDimensionById(map)) {
					dimensionMapper.insert(dimension);
				}
			}
			//开始准备导入数据  根据每一页面产品设置的读取EXCEL的开始行
			//第二行为会计配置的列头
			for(int i =beginReadRow+1;i<rowCount;i++){//第三行开始 beginReadRow=2
				//循环列头
				for(int j=0; j<colCount; j++){
					//开始读取配置栏位
					if(j==productIdCol+2) {//事件代码 productIdCol=0
						//先判定第三列是否配置了相应的事件代码
						if(!StringUtil.checkEmptyNull(sheet.getCell(j, i).getContents())) { beginRow = i;}
						eventId = getExcelValue(j,i,sheet,beginRow);
						rowKey.append(eventId);
						//操作事件表 tb_event
						map.put("eventId", eventId);
						if(null==this.eventMapper.getTbEventByEventId(map)) {
							TbEvent event = new TbEvent();
							event.setEventDesc(getExcelValue(j+1,i,sheet,beginRow));
							event.setEventId(StringUtil.trimToEmpty(eventId));
							event.setEventName(getExcelValue(j+1, i, sheet, beginRow));
							this.eventMapper.insert(event);
						}
						//保存产品与事件的关系表
						TbProductEvent productEvent = new TbProductEvent();
						productEvent.setEventId(eventId);
						productEvent.setPrdNo(prdNo);
						map.put("prdNo", prdNo);
						map.put("eventId", eventId);
						if(null == this.productEventMapper.getTbProductEvent(map)) {
							this.productEventMapper.insert(productEvent);
						}
					}
					if(j>=beginDimesionCol && j<=endDimesionCol)//锁定读取维度key-value的起始列-结束列
					{
						pair = new Pair<String, String>();
						//维度值处理
						String desc = StringUtil.trimToEmpty(sheet.getCell(j, dimensionRow).getContents());
						//查看字典的翻译字段
						TaDictVo taDictVo = dictionaryGetService.getTaDictByCodeAndValue("dimension", desc);
						String dimensionId = (taDictVo==null?desc:taDictVo.getDict_key());
						pair.setLeft(dimensionId);//键
						String dimensionDesc = StringUtil.trimToEmpty(getExcelValue(j,i,sheet,beginRow));
						if(!"".equals(dimensionDesc)) {
						TaDictVo taDictValue = dictionaryGetService.getTaDictByCodeAndValue(dimensionId, dimensionDesc);
						String dimensionValue = (taDictValue==null?dimensionDesc:taDictValue.getDict_key());
						pair.setRight(dimensionValue);//值
						pairs.add(pair);
						rowKey.append(StringUtil.trimToEmpty(getExcelValue(j,i,sheet,beginRow)));
						//键值保存 tb_dimension_value
						TbDimensionValue value = new TbDimensionValue();
						value.setDimensionDesc(dimensionDesc);
						value.setDimensionId(dimensionId);
						value.setDimensionValue(dimensionValue);
						value.setIsActive(DictConstants.YesNo.YES);
						map.put("dimensionId", dimensionId);
						map.put("dimensionValue", dimensionValue);
						if(null == this.dimensionValueMapper.geTbDimensionValueById(map)) {
							this.dimensionValueMapper.insert(value);
						}
						}
						
					}
					
				}
				
				if("".equals(rowKeyOld) || !StringUtils.endsWithIgnoreCase(rowKeyOld, rowKey.toString()))
				{
					rowKeyOld = rowKey.toString();
					
					groupId = prdNo+"."+eventId+"."+this.eventMapper.getGroupId();
					//存储TbProductEventDimensionKv  产品事件维度取值关系组
					TbProductEventDimensionKv kv = null; TbProductEventDimension productEventDimension = null;
					for(int p = 0;p<pairs.size();p++) {
						kv = new TbProductEventDimensionKv();
						productEventDimension = new TbProductEventDimension();
						kv.setDimensionId(pairs.get(p).getLeft());
						productEventDimension.setDimensionId(pairs.get(p).getLeft());
						kv.setDimensionValue(pairs.get(p).getRight());
						kv.setEventId(eventId);
						productEventDimension.setEventId(eventId);
						kv.setPrdNo(prdNo);
						productEventDimension.setPrdNo(prdNo);
						kv.setGroupId(groupId);
						this.productEventDimensionKvMapper.insert(kv);
						if(null == this.productEventDimensionMapper.selectByPrimaryKey(productEventDimension)) {
							this.productEventDimensionMapper.insert(productEventDimension);
						}
					}
				}else {
					//rowKeyOld 与 rowKey.toString() 想等时，则会计分录属于同一组
					rowKeyOld = rowKey.toString();
					pairs.clear();
				}
				rowKey.delete(0,rowKey.length());
				//会计分录组 tb_product_event_scene_acup
				TbProductEventSceneAcup acup = new TbProductEventSceneAcup();
				int countAcupRow = endDimesionCol+1;//会计分录起始列 beginDimesionCol=5
				acup.setGroupId(groupId);
				acup.setDrcrInd(StringUtil.trimToEmpty(sheet.getCell(countAcupRow, i).getContents()));
				acup.setSubjTrans(StringUtil.trimToEmpty(sheet.getCell(countAcupRow+1, i).getContents()).replace("-", ""));
				acup.setSubjCom(StringUtil.trimToEmpty(sheet.getCell(countAcupRow+2, i).getContents()).replace("-", ""));
				acup.setSubject(StringUtil.trimToEmpty(sheet.getCell(countAcupRow+3, i).getContents()).replace("-", ""));
				acup.setSubjDesc(StringUtil.trimToEmpty(sheet.getCell(countAcupRow+4, i).getContents()));
				acup.setSubjSeq(Integer.parseInt(StringUtil.trimToEmpty(sheet.getCell(countAcupRow+5, i)==null?"99" : "".equals(sheet.getCell(countAcupRow+5, i).getContents())?"99":sheet.getCell(countAcupRow+5, i).getContents())));
				acup.setSubjAmt(StringUtil.trimToEmpty(sheet.getCell(countAcupRow+6, i)==null?"":sheet.getCell(countAcupRow+6, i).getContents()));
				this.productEventSceneAcupMapper.insert(acup);
			}
		}
	}


	/**
	 * 读取Excel配置项数据
	 * @param col 列
	 * @param row 行
	 * @param sheet 页
	 * @param beginRow 顶位
	 * @return
	 */
	public static String getExcelValue(int col,int row,Sheet sheet,int beginRow) {
		String cellValue = null;
		for(int i=row;i >=  beginRow;i--) {
			cellValue = sheet.getCell(col, i).getContents();
			if(!StringUtil.checkEmptyNull(cellValue)) {
				break;
			}
		}
		return cellValue;
	}
	@Override
	public void updateProductEventDimensionKvId(Map<String, Object> map) {
		// TODO Auto-generated method stub
		this.productEventDimensionKvMapper.updateProductEventDimensionKvId(map);
	}
	@Override
	public void deleteAllConfigByPrdNo(Map<String, Object> map) {
		
		//add by zl 2019-08-20 删除产品事件表数据
		this.productEventDimensionKvMapper.deleteTbProductEvent(map);
		//end
		this.productEventDimensionKvMapper.deleteTbProductEventSceneAcupByPrdNo(map);
		this.productEventDimensionKvMapper.deleteTbProductEventDimensionKvByPrdNo(map);
		this.productEventDimensionKvMapper.deleteTbProductEventDimensionByPrdNo(map);
	}


	@Override
	public void deleteAllConfig() {
		//add by zl 2019-08-20 删除基础数据表
		this.productEventDimensionKvMapper.deleteTbDimension();
		this.productEventDimensionKvMapper.deleteTbDimensionValue();
		this.productEventDimensionKvMapper.deleteTbEvent();
		//end
	}

}
