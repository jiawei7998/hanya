package com.singlee.hrbcap.service;

import com.github.pagehelper.PageInfo;
import com.singlee.hrbcap.model.base.TbDimension;

import java.util.Map;

public interface TbDimensionService {
	/**
	 * 分页
	 * @param map
	 * @param rb
	 * @return
	 */
	public PageInfo<TbDimension> getTbDimensionPage(Map<String,Object> map);
		
	/**
	 * 添加会计维度代码
	 * 
	 * @param acctCode
	 */
	public void createTbDimension(TbDimension tbDimension);

	/**
	 * 修改会计维度代码
	 * 
	 * @param acctCode
	 */
	public void updateTbDimension(TbDimension tbDimension);

	/**
	 * 删除会计维度代码，删除条件为aKey
	 * 
	 * @param seq
	 */
	public void deleteTbDimension(String eventId);
	
	
}
