
package com.singlee.hrbcap.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.TreeUtil;
import com.singlee.hrbcap.mapper.TbProductEventSceneAcupMapper;
import com.singlee.hrbcap.mapper.TbSubjectDefMapper;
import com.singlee.hrbcap.model.base.TbProductEventSceneAcup;
import com.singlee.hrbcap.model.base.TbSubjectDef;
import com.singlee.hrbcap.service.TbSubjectDefService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/*
 * 科目表serviceIMple
 */
@Service("tbSubjectDefService")
/*
 * 事物
 */
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TbSubjectDefServiceImpl implements TbSubjectDefService {
	/**
	 * 不能用缓存，不然在查询相应套账下的科目信息时不会弹出提示“没有查询到该账套相关的科目信息”
	 */
	/*
	 * 科目Mapper映射
	 */
	@Autowired
	private TbSubjectDefMapper tTbSubjectDefMapper;
	
	@Autowired
	private TbProductEventSceneAcupMapper tbProductEventSceneAcupMapper;
	
	/*
	 * 科目列表
	 */
	// @Cacheable(value = cacheName, key = "'whole'")
	@Override
	public List<TbSubjectDef> getTbSubjectDefList(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<TbSubjectDef> page = tTbSubjectDefMapper.getTbSubjectDefList(map, rb);
		//return TreeUtil.mergeChildrenList(page.getResult(), "-1");
		return page;
	}
	/**
	 * 获取科目树形结构201805
	 */
	@Override
    public List<TbSubjectDef> getTbSubjectDefListForTree(Map<String, Object> map){
		map.put("pageSize", 99999);
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<TbSubjectDef> page = tTbSubjectDefMapper.getTbSubjectDefList(map, rb);
		return TreeUtil.mergeChildrenList(page.getResult(), "-1");
	}
	/*
	 * 添加科目
	 */
	@Override
	public TbSubjectDef creatTbSubjectDef(TbSubjectDef tTbSubjectdef) {
		tTbSubjectDefMapper.insert(tTbSubjectdef);
		return null;
	}

	/*
	 * 删除科目
	 */
	@Override
	public void delTbSubjectDef(String[] dbIds) {
		for (int i = 0; i < dbIds.length; i++) {
			tTbSubjectDefMapper.deleteByPrimaryKey(dbIds[i]);
		}

	}

	@Override
	public Page<TbSubjectDef> getTbSubjectDefPageList(Map<String, Object> map) {
		return tTbSubjectDefMapper.getTbSubjectDefList(map, ParameterUtil.getRowBounds(map));
	}
	
	@Override
	public List<TbSubjectDef> selectAll() {
		return tTbSubjectDefMapper.selectAll();
	}

	@Override
	public List<TbSubjectDef> getTbSubjectAndParentList(
			Map<String, Object> map) {
		return tTbSubjectDefMapper.getTbSubjectAndParentList(map);
	}

	@Override
	public Page<TbSubjectDef> getTbSubjectDefSameService(
			Map<String, Object> map) {
		return tTbSubjectDefMapper.getTbSubjectDefSame(map);
	}
	@Override
	public TbSubjectDef updateTbSubjectDef(TbSubjectDef tTbSubjectdef) {
		int i=tTbSubjectDefMapper.updateByPrimaryKey(tTbSubjectdef);
		if(i>0){
			TbProductEventSceneAcup sceneAcup=new TbProductEventSceneAcup();
			sceneAcup.setSubjDesc(tTbSubjectdef.getSubjName());
			sceneAcup.setSubject(tTbSubjectdef.getSubjCode());
			tbProductEventSceneAcupMapper.updateSceneAcupSubjDescBySubject(sceneAcup);
		}
		return null;
	}

}
