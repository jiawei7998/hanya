package com.singlee.hrbcap.service;


import com.github.pagehelper.Page;
import com.singlee.hrbcap.model.base.TbSubjectDef;

import java.util.List;
import java.util.Map;

/*
 *科目表service层
 */
public interface TbSubjectDefService {
	/*
	 * 遍历科目
	 */
	public List<TbSubjectDef> getTbSubjectDefList(Map<String,Object> map);
	/*
	 * 添加科目
	 */
	public TbSubjectDef creatTbSubjectDef(TbSubjectDef tTbSubjectdef);
	
	/*
	 * 修改科目
	 */
	public TbSubjectDef updateTbSubjectDef(TbSubjectDef tTbSubjectdef);
	/*
	 * 删除科目
	 */
	public void delTbSubjectDef(String[] dbIds);
	/*
	 * 遍历科目
	 */
	public Page<TbSubjectDef> getTbSubjectDefPageList(Map<String, Object> map);
	
	List<TbSubjectDef> selectAll();
	
	/*
	 * 查询条件科目及上级科目
	 */
	public List<TbSubjectDef> getTbSubjectAndParentList(Map<String,Object> map);
	
	/**
	 * 根据科目号 精确查询
	 * @param map
	 * @return
	 */
	public Page<TbSubjectDef> getTbSubjectDefSameService(Map<String,Object> map);
	
	/**
	 * 树形展示的科目定义 201805
	 * @param map
	 * @return
	 */
	public List<TbSubjectDef> getTbSubjectDefListForTree(Map<String, Object> map);
}
