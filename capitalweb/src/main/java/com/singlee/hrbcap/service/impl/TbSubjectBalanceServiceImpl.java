package com.singlee.hrbcap.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.hrbcap.mapper.TbSubjectBalanceMapper;
import com.singlee.hrbcap.model.acup.TbEntry;
import com.singlee.hrbcap.model.acup.TbSubjectBalance;
import com.singlee.hrbcap.service.TbSubjectBalanceService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 记账余额的服务类
 * @author Libo
 *
 */
@Service("tbSubjectBalanceService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TbSubjectBalanceServiceImpl implements TbSubjectBalanceService {

	@Autowired
	private TbSubjectBalanceMapper bookkeepingBalanceMapper;
	/**
	 * 根据分录查找相应的余额，如查询不到，则创建新的并返回
	 * @param taskId
	 * @param date
	 * @param entry
	 */
	@Override
	public TbSubjectBalance findOrCreateByEntry(TbEntry entry){
		TbSubjectBalance balance = new TbSubjectBalance();
		balance.setTaskId(entry.getTaskId());
		balance.setBeginDate(entry.getPostDate());
		balance.setSubjCode(entry.getSubjCode());
		balance.setAcctNo(entry.getCoreAcctCode());
		balance.setOrgNo(entry.getBkkpgOrgId());
		balance.setCcy(entry.getCcy());
		balance.setBkkpgOrgId(entry.getBkkpgOrgId());
		List<TbSubjectBalance> balanceList = bookkeepingBalanceMapper.selectByMap(BeanUtil.beanToHashMap(balance),new RowBounds());
		if(balanceList != null && balanceList.size()>0){
			return balanceList.get(0);
		}
		
		balance.setEndDate(entry.getPostDate());
		balance.setUpdateTime(DateUtil.getCurrentDateTimeAsString());
		balance.setSubjName(entry.getSubjName());
		bookkeepingBalanceMapper.insert(balance);
		
		return balance;
		
	}
	
	/**
	 * 更新余额，只更新四个数字余额（借、贷、支、付）
	 * @param balance
	 */
	@Override
	public void updateBalance(TbSubjectBalance balance){
		bookkeepingBalanceMapper.updateByPrimaryKey(balance);
	}

	@Override
	public Page<TbSubjectBalance> selectByMap(Map<String, Object> map,
			RowBounds rb) {
		return bookkeepingBalanceMapper.selectByMap(map, rb);
	}
	
	@Override
	public List<TbSubjectBalance> selectByMap(Map<String, Object> map) {
		return bookkeepingBalanceMapper.selectByMap(map);
	}

	@Override
	public void insert(TbSubjectBalance balance) {
		bookkeepingBalanceMapper.insert(balance);
	}
}
