package com.singlee.hrbcap.service;

import com.singlee.capital.system.pojo.RoleModule;
import com.singlee.hrbcap.model.base.SceneResultBean;
import com.singlee.hrbcap.model.base.TbProductEventDimensionKv;
import com.singlee.hrbcap.model.base.dimensionKvSaveVo;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Map;

public interface TbProductEventDimensionKvService {

	/**
	 * 查询报表资源信息
	 * @return
	 */
	List<RoleModule> searchModuleForReport();
	/**
	 * 获取维度码值
	 * @param params
	 * @return
	 */
	public List<SceneResultBean> searchTbDimensionValue(Map<String, Object> params);
	
	
	/**
	 * 传入产品和事件编号返回列表
	 * @param map
	 * @return
	 */
	public List<TbProductEventDimensionKv> getDataForProductAndEvent(Map<String, Object> map);
	/**
	 * 保存所选
	 * @param dimensionKvSaveVo
	 */
	public void saveTbProductEventDimensionKv(dimensionKvSaveVo dimensionKvSaveVo);
	/**
	 * 删除所选
	 * @param map
	 */
	public void deleteTbProductEventDimensionKv(@RequestBody Map<String, Object> map);
	
	public List<SceneResultBean> getSceneResultList(Map<String, Object> map);

}
