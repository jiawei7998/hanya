package com.singlee.hrbcap.service.impl;

import com.singlee.capital.common.util.ParentChildUtil;
import com.singlee.hrbcap.mapper.TbProductEventMapper;
import com.singlee.hrbcap.model.base.TbProductEvent;
import com.singlee.hrbcap.service.TbProductEventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

/*
 * 会计事件表serviceIMpl
 */
@Service("tbProductEventService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TbProductEventServiceImpl implements TbProductEventService {
	@Autowired
	private TbProductEventMapper tbProductEventMapper;
	
	@Override
	public void deleteTbProductEventByProNo(Map<String, Object> map) {
		tbProductEventMapper.deleteTbProductEventByProNo(map);
		
	}
	
	@Override
	public void addTbProductEventService(Map<String, Object> map) {
		TbProductEvent tpe = new TbProductEvent();
		ParentChildUtil.HashMapToClass(map, tpe);
		tbProductEventMapper.insert(tpe);
	}


}
