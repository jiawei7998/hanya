package com.singlee.hrbcap.service;


import com.github.pagehelper.Page;
import com.singlee.hrbcap.model.base.TbManualEntry;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/**
 * 记账分录服务接口
 * @author shiting
 *
 */
public interface TbManualEntryService {
	
	/**  获取新的流水号   */
	public String getNewFlowNo();

	/**
	 * 查询分录（分页）
	 * @author 
	 */
	public Page<TbManualEntry> getEntryList(Map<String,Object> map, RowBounds rb);

	/**  保存分录   */
	public void save(TbManualEntry entry);
	
	/**
	 * 查询分录
	 * @author huqin
	 */
	public List<TbManualEntry> getDealEntryList(Map<String,Object> map);
	

	/**  保存分录   */
	public void update(TbManualEntry entry);
	
	public void createYearEntry();
	public void createYearEntry2();
	/**
	 * 保存手工记账分录
	 * @param date		日期
	 * @param taskId	任务ID
	 * @param bookkeepingOrgId  记账机构
	 * @param entryList		分录列表
	 * @return
	 */
	public void saveManualEntry(Map<String,Object> params, @Param("entryList")List<TbManualEntry> entryList);
	
	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 产品审批主表对象列表
	 * @author Hunter
	 * @date 2016-8-9
	 */
	public Page<TbManualEntry> getManualEntryList(Map<String, Object> params, RowBounds rb);
	
	public Page<TbManualEntry> getManualEntryListHand(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 根据请求参数查询分页列表 (我发起的)
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 产品审批主表对象列表
	 * @author Hunter
	 * @date 2016-8-9
	 */
	public Page<TbManualEntry> getManualEntryListMine(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 根据请求参数查询分页列表 (已经审批)
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 产品审批主表对象列表
	 * @author Hunter
	 * @date 2016-8-9
	 */
	public Page<TbManualEntry> getManualEntryFinishList(Map<String, Object> params, RowBounds rb);
	
	public TbManualEntry selectManualEntryVo(Map<String, Object> map);
	
	/**
	 * 年终结算发送T24
	 * @param params
	 */
	public void sendYearendInterfaceToT24(String flowId);
	
	public void comfirYearendEntry(Map<String, Object> params);
	
	/**
	 * 查询当年需要发送的年结分录Id
	 * */
	public String selectYearendEntryFlowId(Map<String, Object> params);

	public void deleteManualEntry(Map<String, Object> params);
}
