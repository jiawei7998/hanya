package com.singlee.hrbcap.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.hrbcap.mapper.TbEventMapper;
import com.singlee.hrbcap.model.base.TbEvent;
import com.singlee.hrbcap.service.TbEventService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 * 会计事件表serviceIMpl
 */
@Service("tbEventService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TbEventServiceImpl implements TbEventService {
	@Autowired
	private TbEventMapper tbEventMapper;
	
	@Override
	public PageInfo<TbEvent> getTbEventPage(Map<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<TbEvent> page = tbEventMapper.getTbEventPage(map, rowBounds);
		return new PageInfo<TbEvent>(page);
	}
	

	@Override
	public void createTbEvent(TbEvent TbEvent) {
		if (tbEventMapper.checkTbEventByEventId(TbEvent.getEventId()) == 0) {
			tbEventMapper.insert(TbEvent);
		} else {
			JY.raiseRException(("金额代码已经存在"));
		}
		
	}

	@Override
	public void updateTbEvent(TbEvent TbEvent) {
		if (tbEventMapper.checkTbEventByEventId(TbEvent.getEventId()) == 0) {
			JY.raiseRException(("会计分录事件代码不存在"));
		} else {
			tbEventMapper.updateByPrimaryKey(TbEvent);
		}
	}

	@Override
	public void deleteTbEvent(String eventId) {
		
		tbEventMapper.deleteByPrimaryKey(eventId);
	}

	
	@Override
	public List<TbEvent> getProductEvents(Map<String, Object> map) {
		// TODO Auto-generated method stub
		String type = ParameterUtil.getString(map, "type", "");
		List<TbEvent> events  =new ArrayList<TbEvent>();
		if(type.equals(DictConstants.YesNo.YES)) {
            events = tbEventMapper.getTbEventByPrdNoYes(map);
        } else {
            events = tbEventMapper.getTbEventByPrdNoNo(map);
        }
		return events;
	}
	@Override
	public List<TbEvent> getTbEventAllList(Map<String, Object> map) {
		return tbEventMapper.getTbEventPage(map);
	}


	@Override
	public List<Map<String, String>> searchEvent(Map<String, Object> params) {
		List<TbEvent> list = tbEventMapper.searchEvent(params);
		List<Map<String, String>> list2 = new ArrayList<Map<String, String>>();
    	for(TbEvent bean: list) {
    		Map<String, String> map1 = new HashMap<String, String>();
    		map1.put("id", bean.getEventId());
    		map1.put("text", bean.getEventName());
    		list2.add(map1);
    	}
		return list2;
	}

	/***
	 * 根据dealNo查询TbEntry表中不重复的下拉事件
	 */
	@Override
	public List<Map<String, String>> searchDistinctEventByDealNo(Map<String, Object> params) {
		List<TbEvent> list = tbEventMapper.searchDistinctEventByDealNo(params);
		List<Map<String, String>> list2 = new ArrayList<Map<String, String>>();
    	for(TbEvent bean: list) {
    		Map<String, String> map1 = new HashMap<String, String>();
    		map1.put("id", bean.getEventId());
    		map1.put("text", bean.getEventName());
    		list2.add(map1);
    	}
		return list2;
	}
}
