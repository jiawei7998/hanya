package com.singlee.hrbcap.service.impl;


import com.singlee.cap.accounting.model.InstructionPackage;
import com.singlee.cap.accounting.model.TacTask;
import com.singlee.cap.accounting.service.AccountingTaskService;
import com.singlee.capital.base.cal.LogManager;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.Pair;
import com.singlee.capital.common.spring.mybatis.BatchDao;
import com.singlee.capital.common.util.*;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.dict.service.DictionaryGetService;
import com.singlee.capital.eu.util.CollectionUtils;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.hrbcap.mapper.*;
import com.singlee.hrbcap.model.acup.TbDealBalance;
import com.singlee.hrbcap.model.acup.TbEntry;
import com.singlee.hrbcap.model.acup.TbSubjectBalance;
import com.singlee.hrbcap.model.acup.TbSubjectBalanceSum;
import com.singlee.hrbcap.model.base.*;
import com.singlee.hrbcap.service.*;
import com.singlee.hrbcap.util.BookkeepingUtil;
import com.singlee.hrbcap.util.Ifrs9Constants;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import java.math.BigDecimal;
import java.util.*;

@Service("ifrs9BookkeepingEngineService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class Ifrs9BookkeepingEngineServiceImpl implements Ifrs9BookkeepingEngineService {

	@Autowired
	TbProductEventMapper productEventMapper;
	@Autowired
	TbProductEventDimensionMapper productEventDimensionMapper;
	@Autowired
	TbProductEventDimensionKvMapper productEventDimensionKvMapper;
	@Autowired
	TbProductEventSceneAcupMapper productEventSceneAcupMapper;
	@Autowired
	TbAmountMapper amountMapper;
	@Autowired
	TbEntryMapper entryMapper;
	@Autowired
    DayendDateService dayendDateService;
	@Autowired
	TbEventMapper eventMapper;
	@Autowired
	TbDealBalanceService dealBalanceService;
	@Autowired
	TbSubjectBalanceService subjectBalanceService;
	@Autowired
	TbSubjectDefService tbSubjectDefService;
	@Autowired
	TbSubjectBalanceSumService subjectBalanceSumService;
	@Autowired 
	TbEntryService entryService;
	/*@Autowired
	private ProductService productService;*/
	@Autowired
	private AccountingTaskService accountingTaskService;
	@Autowired
    DictionaryGetService dictionaryGetService;
	@Autowired
	TbDimensionValueMapper tbDimensionValueMapper;

	@Autowired
	TbEntryMapper  tbEntryMapper;
	@Autowired
    BatchDao batchDao;

	
	private static Logger accountLog = LoggerFactory.getLogger("ACCOUNT");
	
	@Override
	public void dealPretreatment(InstructionPackage instructionPackage) {
		//adjFlag +  - 
		//从 amtMap中获得所有需要判定 + - 冲减的金额
		Iterator<String> amtIterator = Ifrs9Constants.amtNameMap.keySet().iterator();
		String adjFlag = "+";
		while(amtIterator.hasNext()){
			String amtKeyString = amtIterator.next();
			Double amtValue = ParameterUtil.getDouble(instructionPackage.getAmtMap(),amtKeyString,0);
//			if(amtValue.compareTo(new Double(0)) >= 0){
//				adjFlag = "+";
//			}else
//			{
//				adjFlag = "-";
//			}
			//float比较漏洞修改 llf 20190809
			if(BigDecimal.valueOf(amtValue).compareTo(BigDecimal.valueOf(0))>=0) {
				adjFlag = "+";
			}else {
				adjFlag = "-";
			}
		}
		instructionPackage.getInfoMap().put("adjFlag", adjFlag);
	}
	
	/**
	 * guomeijuan  20191023
	 * tips:instructionPackage中包含两部分参数：infoMap，AmtMap
	 *      infoMap传的参数（维度参数）：eventId会计事件
	 *                       prdNo：产品编号
	 *                       instaffiliation记账机构（如果有记账机构，必须传）
	 *                       sponinst：交易机构（选传）
	 *                       ccy:币种（必填）
	 *                       others：如果有科目转换规则表达式，则需要传递表达式中需要的参数【如：key1与key2在map中获取，由key1与key2拼接或转换得到的科目，则需传递key1、key2】
	 *      AmtMap参数说明：需传分录对应的金额表达式对应的value
	 */
	@Override
	public Pair<String,List<TbEntry>> generateEntryIFRS9Instruction(InstructionPackage instructionPackage, String flowId) {
		// TODO Auto-generated method stub
		Pair<String,List<TbEntry>> pair = new Pair<String, List<TbEntry>>();
		/*if(!productService.checkIsAcctAcup(instructionPackage.getInfoMap()))
		{
			pair.setLeft("000000");
			pair.setRight(null);
			return pair;
		}*/
		//返回的会计分录列表
		List<TbEntry> entryList = new ArrayList<TbEntry>();
		LogManager.getLogger(LogManager.MODEL_BOOKKEEPING).info("引擎输入参数："+instructionPackage.toString());
		Map<String, Object> paramsMap = new HashMap<String, Object>();
		JY.require(null != instructionPackage, "请指定会计引擎入口参数包： instructionPackage！");
		JY.require(null != instructionPackage.getInfoMap(), "请指定会计引擎的业务信息：InfoMap！");
		JY.require(null != instructionPackage.getAmtMap(), "请指定会计引擎的金额信息：AmtMap！");
		//获得事件入口  eventId   instructionPackage.getInfoMap为业务信息
		String eventId = ParameterUtil.getString(instructionPackage.getInfoMap(), "eventId", null);
		JY.require(null != eventId, "请指定InfoMap的会计事件代码eventId!");
		String prdNo  =  ParameterUtil.getString(instructionPackage.getInfoMap(), "prdNo", null);
		JY.require(null != prdNo, "请指定InfoMap的会计产品代码prdNo!");
		//检查产品是否存在该事件
		paramsMap.put("eventId", eventId);
		paramsMap.put("prdNo", prdNo);
		TbProductEvent productEvent = productEventMapper.getTbProductEvent(paramsMap);
		if(null == productEvent) 
		{
			LogManager.getLogger(LogManager.MODEL_BATCH).info("当前产品："+prdNo+"没有对应的事件:"+eventId);
			JY.raise("当前产品："+prdNo+"没有对应的事件:"+eventId);
			return null;
		}
		//根据事件和产品确定其维度范围
		List<TbProductEventDimension> dimensions = this.productEventDimensionMapper.getTbProductEventDimesions(paramsMap);
		if(dimensions.size()<=0) {
			LogManager.getLogger(LogManager.MODEL_BATCH).info("当前产品："+prdNo+",事件:"+eventId+"无会计维度！");
			JY.raise("当前产品："+prdNo+",事件:"+eventId+"无会计维度！");
			return null;
		}
		/**
		 * 预处理，增加部分动态维度逻辑
		 */
		dealPretreatment(instructionPackage);
		//循环会计维度获取值
		StringBuffer dimensionKeys = new StringBuffer();
		StringBuffer dimensionValues = new StringBuffer();
		StringBuffer dimensionValueNames = new StringBuffer();
		for(TbProductEventDimension dimension : dimensions) {
			if(null == ParameterUtil.getString(instructionPackage.getInfoMap(),dimension.getDimensionId(),null)) {
				LogManager.getLogger(LogManager.MODEL_BATCH).info("当前产品："+prdNo+",事件:"+eventId+"维度:"+dimension.getDimensionId()+"不存在业务信息Map数据中！");
				JY.raise("当前产品："+prdNo+",事件:"+eventId+"维度:"+dimension.getDimensionId()+"不存在！");
				return null;
			}
			dimensionKeys.append(dimension.getDimensionId()).append(",");
			dimensionValues.append(ParameterUtil.getString(instructionPackage.getInfoMap(),dimension.getDimensionId(),".")).append(",");
			try {
				String	dimensionId =dimension.getDimensionId();
				String dimensionValue= ParameterUtil.getString(instructionPackage.getInfoMap(),dimension.getDimensionId(),".");
				Map<String, Object> dvMap = new HashMap<String, Object>();
				dvMap.put("dimensionId",dimensionId);
				dvMap.put("dimensionValue",dimensionValue);
				
				dimensionValueNames.append(tbDimensionValueMapper.geTbDimensionValueById(dvMap).getDimensionDesc()).append(",");
			} catch (Exception e) {
				accountLog.info("获取维度码值名称出错",e);
			}
		
		}
		String key = dimensionKeys.toString();String value = dimensionValues.toString();
		paramsMap.put("dimensionKeys", key.substring(0, key.length()-1));
		paramsMap.put("dimensionValues", value.substring(0, value.length()-1));
		//循环会计场景获得groupId
		String groupId = productEventDimensionKvMapper.getTbProductEventDimensionKvGroupId(paramsMap);
		if(null == groupId) {
			LogManager.getLogger(LogManager.MODEL_BATCH).info("未查到[dimensionKeys："+ ParameterUtil.getString(paramsMap,"dimensionKeys","")+"],[dimensionValues :"+ ParameterUtil.getString(paramsMap,"dimensionValues","")+"]的IFRS9会计场景 ");
			JY.raise("未查到[dimensionKeys："+ ParameterUtil.getString(paramsMap,"dimensionKeys","")+"],[dimensionValues :"+ ParameterUtil.getString(paramsMap,"dimensionValues","")+"]的IFRS9会计场景 ");
			return null;
		}
		//通过场景获得会计分录
		paramsMap.put("groupId", groupId);
		List<TbProductEventSceneAcup> acups = this.productEventSceneAcupMapper.getTbProductEventSceneAcupsByGroupId(paramsMap);
		
		//String postDate = dayendDateService.getDayendDate();//账务日期改为以下方式获取
		//edit by 20200319 账务日期的获取改为传进来的值，若没传，则获取当前账务日期
		String postDate = ParameterUtil.getString(instructionPackage.getInfoMap(), "postDate", null);
		postDate = StringUtil.isNullOrEmpty(postDate)==true?dayendDateService.getDayendDate():postDate;
		
		
		//根据条件获取帐套信息
		TacTask task = new TacTask();
		task.setBkMain(","+ ParameterUtil.getString(instructionPackage.getInfoMap(),"prdNo","")+",");
		task.setInstId(","+ ParameterUtil.getString(instructionPackage.getInfoMap(),"sponinst","")+",");
		List<TacTask> taskList = accountingTaskService.selectEntryTaskList(BeanUtil.beanToMap(task));
		if(taskList != null && taskList.size() > 0){
			task = taskList.get(0);
		}else{
			taskList = accountingTaskService.selectEntryTaskList(null);
			task = taskList.get(0);
		}
		instructionPackage.getInfoMap().put("taskId", task.getTaskId());
		
		//获得会计流水号
		String flowNo = (null != StringUtil.trimToNull(flowId)) ? flowId:entryMapper.getNewTbEntryFlowNo();
		for(TbProductEventSceneAcup acup:acups) {
			
			TbEntry entry = new TbEntry();
			
			//自动映射同名属性
			try {
				//BeanUtils.populate(entry, instructionPackage.getInfoMap());
				ParentChildUtil.HashMapToClass(instructionPackage.getInfoMap(), entry);
			} catch (Exception e) {
				//JY.raise(BookkeepingUtil.getErrorInfoFromException(e));
				JY.require(false, BookkeepingUtil.getErrorInfoFromException(e));
			}
			entry.setFlowId(flowNo);
			entry.setEventId(eventId);
			/**
			 * 针对从界面获取的表达式取值进行解析  Exp:acctNo
			 */
			if(!StringUtil.checkEmptyNull(acup.getSubject()) && acup.getSubject().startsWith("Exp:"))
			{
				//开始组装脚本引擎的表达式
				String subject = "";
				try {
					//组装科目/账号表达式  infoMap 带入的bean里面获取数据  : 冒号之后为截取的属性
					Map<String, Object> infoMap = new HashMap<String, Object>();
					infoMap.put("infoMap", instructionPackage.getInfoMap());
					String formula = "infoMap.get(\""+acup.getSubject().substring(acup.getSubject().indexOf(":")+1, acup.getSubject().trim().length())+"\")";
					subject = calcBkIndexSubject(infoMap, formula); //获取表达式对应的值
				} catch (Exception e) {
					LogManager.getLogger(LogManager.MODEL_BATCH).info(acup.getSubject()+",会计科目表达式找不到！"+ExceptionUtils.getFullStackTrace(e));
					JY.raise(acup.getSubject()+",会计科目表达式找不到！");
					return null;
				}
				entry.setSubjCode(subject);
			}else if(!StringUtil.checkEmptyNull(acup.getSubject()) && acup.getSubject().startsWith("JSEx:")) {
				/**
				 * 适用于表达式 key1+key2+固定值；key1与key2在map中获取，最后拼接固定值
				 */
				//开始组装脚本引擎的表达式
				String subject = "";
				//组装科目/账号表达式  infoMap 带入的bean里面获取数据  : 冒号之后为截取的属性
				Map<String, Object> infoMap = instructionPackage.getInfoMap();
				String formula = acup.getSubject().substring(acup.getSubject().indexOf(":")+1, acup.getSubject().trim().length());
				subject = calcSubject(infoMap, formula); //获取表达式对应的值
				entry.setSubjCode(subject);
			}else if(!StringUtil.checkEmptyNull(acup.getSubject()) && acup.getSubject().startsWith("AC:")) {
				/**
				 * 适用于表达式 key；key在map中获取
				 */
				//开始组装脚本引擎的表达式
				String subject = "";
				//组装科目/账号表达式  infoMap 带入的bean里面获取数据  : 冒号之后为截取的属性
				Map<String, Object> infoMap = instructionPackage.getInfoMap();
				String formula = acup.getSubject().substring(acup.getSubject().indexOf(":")+1, acup.getSubject().trim().length());
				JY.require(!"".equals(formula), "未从表达式"+acup.getSubject()+"中取到相应的key！");
				subject =  ParameterUtil.getString(infoMap,formula,"");//获取表达式对应的值
				JY.require(!"".equals(subject), "未从infoMap"+"中取到相应的"+formula+"值！");
				entry.setSubjCode(subject);
			}else{
				entry.setSubjCode(acup.getSubject());
			}
			///////////////////////////////////////////////////////////////////////////
			if(!StringUtil.checkEmptyNull(acup.getSubjDesc()) && acup.getSubjDesc().startsWith("Exn:"))
			{
				//开始组装脚本引擎的表达式
				String subjectName = "";
				try {
					//组装科目/账号表达式  infoMap 带入的bean里面获取数据  : 冒号之后为截取的属性
					Map<String, Object> infoMap = new HashMap<String, Object>();
					infoMap.put("infoMap", instructionPackage.getInfoMap());
					String formula = "infoMap.get(\""+acup.getSubjDesc().substring(acup.getSubjDesc().indexOf(":")+1, acup.getSubjDesc().trim().length())+"\")";
					subjectName = calcBkIndexSubject(infoMap, formula); //获取表达式对应的值
				} catch (Exception e) {
					LogManager.getLogger(LogManager.MODEL_BATCH).info(acup.getSubjDesc()+",会计科目表达式找不到！"+ExceptionUtils.getFullStackTrace(e));
					JY.raise(acup.getSubjDesc()+",会计科目表达式找不到！");
					return null;
				}
				entry.setSubjName(subjectName);
			}else{
				entry.setSubjName(acup.getSubjDesc());
			}	
			
			entry.setPostDate(postDate);
			entry.setFlowSeq(acup.getSubjSeq());//序号
			entry.setSetno(acup.getGroupId());//组别
			entry.setSponInst(ParameterUtil.getString(instructionPackage.getInfoMap(),"sponinst","NULL"));
			entry.setBkkpgOrgId(ParameterUtil.getString(instructionPackage.getInfoMap(),"instaffiliation","NULL"));
			entry.setState(DictConstants.EntryState.NORMAL);
			
			entry.setSendFlag(DictConstants.EntrySendFlag.UnSend);
			entry.setUpdateTime(DateUtil.getCurrentDateTimeAsString());
			
			entry.setTaskId(task.getTaskId());
			
			//开始轮询会计配置，获得金额表达式，进行运算
			Map<String, Object> calMap = new HashMap<String, Object>();
			calMap.put("amtMap", instructionPackage.getAmtMap());
			double amtValue = 0;
			try {
				//查询金额表达式
				TbAmount amount = amountMapper.selectByPrimaryKey(acup.getSubjAmt());
				JY.require(null != amount, acup.getSubjAmt()+",金额表达式找不到！");
				amtValue = calcBkIndex(calMap, amount.getAmountFormula()); //计算
				
			} catch (Exception e) {
				LogManager.getLogger(LogManager.MODEL_BATCH).info(acup.getSubjAmt()+",金额表达式找不到！"+ExceptionUtils.getFullStackTrace(e));
				return null;
			}
			/**
			 * 预处理 
			 * FCND  标记  金额为正
			   FDNC
			 */
			entry.setRedFlag("00");//默认所有账务都记蓝字
			
			if ("FCNC".equalsIgnoreCase(acup.getDrcrInd())){
				acup.setDrcrInd(DictConstants.DrcrInd.CR);
				entry.setValue(MathUtil.round(amtValue, 2));
				//redFlag红蓝字【00-蓝字,01-红字】
				if(BigDecimal.valueOf(amtValue).compareTo(BigDecimal.ZERO) < 0){
					entry.setRedFlag("01");
				}
			}else{
				entry.setValue(MathUtil.round(Math.abs(amtValue), 2));
			}
			
			if(acup.getDrcrInd().equalsIgnoreCase(DictConstants.DrcrInd.FCND))
			{
				acup.setDrcrInd(BigDecimal.valueOf(amtValue).compareTo(BigDecimal.ZERO)>=0? DictConstants.DrcrInd.CR: DictConstants.DrcrInd.DR);
			}else if(acup.getDrcrInd().equalsIgnoreCase(DictConstants.DrcrInd.FDNC))
			{
				acup.setDrcrInd(BigDecimal.valueOf(amtValue).compareTo(BigDecimal.ZERO)>=0? DictConstants.DrcrInd.DR: DictConstants.DrcrInd.CR);
			}else if((DictConstants.DrcrInd.DR.equals(acup.getDrcrInd()) ||
					DictConstants.DrcrInd.CR.equals(acup.getDrcrInd()))
					&&BigDecimal.valueOf(amtValue).compareTo(BigDecimal.ZERO) < 0) {
				//如果是其他类型的科目，则根据金额正负，判断红蓝标志，负数为红字
				entry.setRedFlag("01");
				entry.setValue(MathUtil.round(amtValue, 2));
			}
			
			entry.setDebitCreditFlag(acup.getDrcrInd().substring(0,1));//DR 截取为 D    CR  截取为 C 

			entryList.add(entry);
			
		}
		//检查借贷是否平衡
		BookkeepingUtil.adjustAndCheckEntryList(entryList);
		pair.setLeft(flowId);
		pair.setRight(entryList);
		return pair;
	}

	private static ScriptEngineManager manager = new ScriptEngineManager(); 
	private static ScriptEngine engine;
	/**
	 * 计算记账指标
	 * @param map
	 * @param expression
	 * @return
	 */
	public static double calcBkIndex(Map<String, Object> map, String expression){
		if(engine == null) {
            engine =  manager.getEngineByName("js");
        }
		
		for(String key:map.keySet()){
			engine.put(key, map.get(key));
		}
		try {
			Object val = engine.eval(expression);
			//解决+不能计算问题
			BigDecimal result = BigDecimal.ZERO;
			if(expression.contains("+")){
				String[] exp = expression.split("\\+");
				for(int i = 0; i < exp.length; i++){
					String ex = exp[i];
					result = result.add(new BigDecimal(engine.eval(ex).toString()));
				}
				return result.doubleValue();
			}
			if(val != null){
				return Double.parseDouble(val.toString());
			}
		} catch (Exception e) {
			//e.printStackTrace();
			throw new RException(e);
		}
		return 0;
	}
	
	public static String calcBkIndexSubject(Map<String, Object> map, String expression){
		if(engine == null) {
            engine =  manager.getEngineByName("js");
        }
		
		for(String key:map.keySet()){
			engine.put(key, map.get(key));
		}
		try {
			Object val = engine.eval(expression);
			if(val != null){
				return String.valueOf(val.toString());
			}
		} catch (Exception e) {
			//e.printStackTrace();
			throw new RException(e);
		}
		return "Exp:Unabled";
	}
	
	@Override
	public String generateEntryIFRS9Entrance(InstructionPackage instructionPackage,String flowId) {
		// TODO Auto-generated method stub
		//获得事件入口  eventId   instructionPackage.getInfoMap为业务信息
		//insert into TA_DICT (DICT_ID, DICT_NAME, DICT_MODULE, DICT_DESC, DICT_PARENTID, DICT_KEY, DICT_VALUE, DICT_LEVEL, DICT_SORT, IS_ACTIVE)
		//values ('IFRS9', 'IFRS9会计引擎控制器', 'IFRS9', 'IFRS9会计引擎控制器', '-', 'OPEN', '1', '1', 1, 1);
		//判断净值型理财不需除会计分录
		if((dictionaryGetService.getTaDictByCodeAndKey("IFRS9", "OPEN").getDict_value().equals(DictConstants.YesNo.NO))|| "1092".equals(instructionPackage.getInfoMap().get("prdNo"))) {
            return DictConstants.YesNo.NO;
        }
		
		String eventId = ParameterUtil.getString(instructionPackage.getInfoMap(), "eventId", null);
		JY.require(null != eventId, "请指定InfoMap的会计事件代码eventId!");
		Map<String, Object> paramsMap = new HashMap<String, Object>();
		paramsMap.put("eventId", eventId);
		//行级锁
		eventMapper.lockTbEvent(paramsMap);
		Pair<String,List<TbEntry>> pair =  generateEntryIFRS9Instruction(instructionPackage,flowId);
		CollectionUtils.sort(pair.getRight(), false, "flowSeq");
		if(null != pair.getRight() && pair.getRight().size()> 0) {
			// 生成交易科目余额
			// 生成科目明细余额
			// 生成科目汇总余额
			updateBookkeepingBalance(pair.getRight());
			entryService.save(pair.getRight());
			flowId = pair.getRight().get(0).getFlowId();
		}
		return flowId;//返回套号
	}
	
	/**
	 * 根据分录更新余额
	 * @param entryList
	 */
	@Override
	public void updateBookkeepingBalance(List<TbEntry> entryList){
		for(TbEntry entry : entryList){
			if(entry.getDealNo() != null && !"".equals(entry.getDealNo())){
				updateDealEntryBal(entry);
			}
			updateBookkeepingBalance(entry);
			updateTbkSubjectBalanceSum(entry);
		}
	}
	/**
	 * 更新交易的科目余额
	 * @param entryList
	 */
	public void updateDealEntryBal(TbEntry entry){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("dealNo", entry.getDealNo());
		map.put("subjCode", entry.getSubjCode());
		List<TbDealBalance> list = dealBalanceService.selectByMap(map);
		TbDealBalance balance = null;
		if(list.size() > 0){
			balance = list.get(0);
			if(DictConstants.DebitCreditFlag.Debit.equals(entry.getDebitCreditFlag())){
				balance.setDebitValue(balance.getDebitValue() + entry.getValue());
			}else if(DictConstants.DebitCreditFlag.Credit.equals(entry.getDebitCreditFlag())){
				balance.setCreditValue(balance.getCreditValue() + entry.getValue());
			}else if(DictConstants.DebitCreditFlag.Pay.equals(entry.getDebitCreditFlag())){
				balance.setPayValue(balance.getPayValue() + entry.getValue());
			}else if(DictConstants.DebitCreditFlag.Receive.equals(entry.getDebitCreditFlag())){
				balance.setReceiveValue(balance.getReceiveValue() + entry.getValue());
			}
			dealBalanceService.updateBalance(balance);
		}else{
			balance = new TbDealBalance();
			balance.setTaskId(entry.getTaskId());
			balance.setDealNo(entry.getDealNo());
			balance.setBkkpgOrgId(entry.getBkkpgOrgId());
			balance.setCcy(entry.getCcy());
			balance.setSubjCode(entry.getSubjCode());
			balance.setSubjName(entry.getSubjName());
			if(DictConstants.DebitCreditFlag.Debit.equals(entry.getDebitCreditFlag())){
				balance.setDebitValue(entry.getValue());
			}else if(DictConstants.DebitCreditFlag.Credit.equals(entry.getDebitCreditFlag())){
				balance.setCreditValue(entry.getValue());
			}else if(DictConstants.DebitCreditFlag.Pay.equals(entry.getDebitCreditFlag())){
				balance.setPayValue(entry.getValue());
			}else if(DictConstants.DebitCreditFlag.Receive.equals(entry.getDebitCreditFlag())){
				balance.setReceiveValue(entry.getValue());
			}
			balance.setLstDate(DateUtil.getCurrentDateTimeAsString());
			dealBalanceService.insert(balance);
		}
	}

	/**
	 * 通过分录更新科目余额
	 * @param entry
	 */
	@Override
	public void updateBookkeepingBalance(TbEntry entry){
		
		TbSubjectBalance balance = subjectBalanceService.findOrCreateByEntry(entry);
		if(DictConstants.DebitCreditFlag.Debit.equals(entry.getDebitCreditFlag())){
			balance.setDebitChg(balance.getDebitChg() + entry.getValue());
			balance.setDebitValue(balance.getDebitValue() + entry.getValue());
		}else if(DictConstants.DebitCreditFlag.Credit.equals(entry.getDebitCreditFlag())){
			balance.setCreditChg(balance.getCreditChg() + entry.getValue());
			balance.setCreditValue(balance.getCreditValue() + entry.getValue());
		}else if(DictConstants.DebitCreditFlag.Pay.equals(entry.getDebitCreditFlag())){
			balance.setPayChg(balance.getPayChg() + entry.getValue());
			balance.setPayValue(balance.getPayValue() + entry.getValue());
		}else if(DictConstants.DebitCreditFlag.Receive.equals(entry.getDebitCreditFlag())){
			balance.setReceiveChg(balance.getReceiveChg() + entry.getValue());
			balance.setReceiveValue(balance.getReceiveValue() + entry.getValue());
		}else{
			throw new RException("分录借贷方向错误：" + entry);
		}
		
		if(DictConstants.DebitCreditFlag.Debit.equals(balance.getBalanceDirect())){
			balance.setDebitValue(balance.getDebitValue() - balance.getCreditValue());
			balance.setCreditValue(0);
		}else if(DictConstants.DebitCreditFlag.Credit.equals(balance.getBalanceDirect())){
			balance.setCreditValue(balance.getCreditValue() - balance.getDebitValue());
			balance.setDebitValue(0);
		}else if(DictConstants.DebitCreditFlag.Receive.equals(balance.getBalanceDirect())){
			balance.setReceiveValue(balance.getReceiveValue() - balance.getPayValue());
			balance.setPayValue(0);
		}else if(DictConstants.DebitCreditFlag.Pay.equals(balance.getBalanceDirect())){
			balance.setPayValue(balance.getPayValue() - balance.getReceiveValue());
			balance.setReceiveValue(0);
		}else{
			balance.setDebitValue(balance.getDebitValue() - balance.getCreditValue());
			balance.setCreditValue(0);
			balance.setPayValue(balance.getPayValue() - balance.getReceiveValue());
			balance.setReceiveValue(0);
			
			if(balance.getDebitValue() < 0){
				balance.setCreditValue(-balance.getDebitValue());
				balance.setDebitValue(0);
			}
			if(balance.getPayValue() < 0 ){
				balance.setReceiveValue(-balance.getPayValue());
				balance.setPayValue(0);
			}
		}
		
		if (StringUtil.isEmpty(balance.getDbId())) {
			subjectBalanceService.insert(balance);
		} else {
			subjectBalanceService.updateBalance(balance);
		}
	}
	/**
	 * 更新科目余额汇总
	 * @param entry
	 */
	@Override
	public void updateTbkSubjectBalanceSum(TbEntry entry){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("taskId", entry.getTaskId());
		map.put("subjCode", entry.getSubjCode());
		List<TbSubjectDef> defList = tbSubjectDefService.getTbSubjectAndParentList(map);
		for (TbSubjectDef tbkSubjectDef : defList) {
			Map<String, Object> balMap = new HashMap<String, Object>();
			balMap.put("taskId", entry.getTaskId());
			balMap.put("bkkpgOrgId", entry.getBkkpgOrgId());
			balMap.put("ccy", entry.getCcy());
			balMap.put("subjCode", tbkSubjectDef.getSubjCode());
			List<TbSubjectBalanceSum> list = subjectBalanceSumService.selectByMap(balMap);
			TbSubjectBalanceSum balance = null;
			if(list.size() > 0){
				balance = list.get(0);
				if(tbkSubjectDef.getDebitCredit() != null && !"".equals(tbkSubjectDef.getDebitCredit())){
					if(tbkSubjectDef.getDebitCredit().equals(DictConstants.debitCredit.Debit)){
						if(DictConstants.DebitCreditFlag.Debit.equals(entry.getDebitCreditFlag())){
							balance.setDebitValue(balance.getDebitValue() + entry.getValue());
							balance.setDebitChg(balance.getDebitChg() + entry.getValue());
						}else if(DictConstants.DebitCreditFlag.Credit.equals(entry.getDebitCreditFlag())){
							balance.setDebitValue(balance.getDebitValue() - entry.getValue());
							balance.setCreditChg(balance.getCreditChg() + entry.getValue());
						}
					}else if(tbkSubjectDef.getDebitCredit().equals(DictConstants.debitCredit.Credit)){
						if(DictConstants.DebitCreditFlag.Debit.equals(entry.getDebitCreditFlag())){
							balance.setCreditValue(balance.getCreditValue() - entry.getValue());
							balance.setDebitChg(balance.getDebitChg() + entry.getValue());
						}else if(DictConstants.DebitCreditFlag.Credit.equals(entry.getDebitCreditFlag())){
							balance.setCreditValue(balance.getCreditValue() + entry.getValue());
							balance.setCreditChg(balance.getCreditChg() + entry.getValue());
						}
					}else{
						if(DictConstants.DebitCreditFlag.Debit.equals(entry.getDebitCreditFlag())){
							balance.setDebitValue(balance.getDebitValue() + entry.getValue());
							balance.setDebitChg(balance.getDebitChg() + entry.getValue());
						}else if(DictConstants.DebitCreditFlag.Credit.equals(entry.getDebitCreditFlag())){
							balance.setDebitValue(balance.getDebitValue() - entry.getValue());
							balance.setCreditChg(balance.getCreditChg() + entry.getValue());
						}
					}
				}else{
					if(DictConstants.DebitCreditFlag.Debit.equals(entry.getDebitCreditFlag())){
						balance.setDebitValue(balance.getDebitValue() + entry.getValue());
						balance.setDebitChg(balance.getDebitChg() + entry.getValue());
					}else if(DictConstants.DebitCreditFlag.Credit.equals(entry.getDebitCreditFlag())){
						balance.setDebitValue(balance.getDebitValue() - entry.getValue());
						balance.setCreditChg(balance.getCreditChg() + entry.getValue());
					}
				}
					
				if(tbkSubjectDef.getPayRecive() != null && !"".equals(tbkSubjectDef.getPayRecive())){
					if(tbkSubjectDef.getPayRecive().equals(DictConstants.payRecive.P)){
						if(DictConstants.DebitCreditFlag.Pay.equals(entry.getDebitCreditFlag())){
							balance.setPayChg(balance.getPayChg()+entry.getValue());
							balance.setPayValue(balance.getPayValue() + entry.getValue());
						}else if(DictConstants.DebitCreditFlag.Receive.equals(entry.getDebitCreditFlag())){
							balance.setPayChg(balance.getPayChg() - entry.getValue());
							balance.setReceiveChg(balance.getReceiveChg() + entry.getValue());
						}
					}else{
						if(DictConstants.DebitCreditFlag.Pay.equals(entry.getDebitCreditFlag())){
							balance.setPayChg(balance.getPayChg()+entry.getValue());
							balance.setPayValue(balance.getPayValue() + entry.getValue());
						}else if(DictConstants.DebitCreditFlag.Receive.equals(entry.getDebitCreditFlag())){
							balance.setPayChg(balance.getPayChg() - entry.getValue());
							balance.setReceiveChg(balance.getReceiveChg() + entry.getValue());
						}
					}
				}else{
					if(DictConstants.DebitCreditFlag.Pay.equals(entry.getDebitCreditFlag())){
						balance.setPayChg(balance.getPayChg()+entry.getValue());
						balance.setPayValue(balance.getPayValue() + entry.getValue());
					}else if(DictConstants.DebitCreditFlag.Receive.equals(entry.getDebitCreditFlag())){
						balance.setPayChg(balance.getPayChg() - entry.getValue());
						balance.setReceiveChg(balance.getReceiveChg() + entry.getValue());
					}
				}
				balance.setUpdateTime(DateUtil.getCurrentDateTimeAsString());
				subjectBalanceSumService.updateBalance(BeanUtil.beanToMap(balance));
			}else{
				balance = new TbSubjectBalanceSum();
				balance.setTaskId(entry.getTaskId());
				balance.setCcy(entry.getCcy());
				balance.setBkkpgOrgId(entry.getBkkpgOrgId());
				balance.setSubjCode(tbkSubjectDef.getSubjCode());
				balance.setSubjName(tbkSubjectDef.getSubjName());
				if(tbkSubjectDef.getDebitCredit() != null && !"".equals(tbkSubjectDef.getDebitCredit())){
					if(tbkSubjectDef.getDebitCredit().equals(DictConstants.debitCredit.Debit)){
						if(DictConstants.DebitCreditFlag.Debit.equals(entry.getDebitCreditFlag())){
							balance.setDebitValue(balance.getDebitValue() + entry.getValue());
							balance.setDebitChg(balance.getDebitChg() + entry.getValue());
						}else if(DictConstants.DebitCreditFlag.Credit.equals(entry.getDebitCreditFlag())){
							balance.setDebitValue(balance.getDebitValue() - entry.getValue());
							balance.setCreditChg(balance.getCreditChg() + entry.getValue());
						}
					}else if(tbkSubjectDef.getDebitCredit().equals(DictConstants.debitCredit.Credit)){
						if(DictConstants.DebitCreditFlag.Debit.equals(entry.getDebitCreditFlag())){
							balance.setCreditValue(balance.getCreditValue() - entry.getValue());
							balance.setDebitChg(balance.getDebitChg() + entry.getValue());
						}else if(DictConstants.DebitCreditFlag.Credit.equals(entry.getDebitCreditFlag())){
							balance.setCreditValue(balance.getCreditValue() + entry.getValue());
							balance.setCreditChg(balance.getCreditChg() + entry.getValue());
						}
					}else{
						if(DictConstants.DebitCreditFlag.Debit.equals(entry.getDebitCreditFlag())){
							balance.setDebitValue(balance.getDebitValue() + entry.getValue());
							balance.setDebitChg(balance.getDebitChg() + entry.getValue());
						}else if(DictConstants.DebitCreditFlag.Credit.equals(entry.getDebitCreditFlag())){
							balance.setDebitValue(balance.getDebitValue() - entry.getValue());
							balance.setCreditChg(balance.getCreditChg() + entry.getValue());
						}
					}
				}else{
					if(DictConstants.DebitCreditFlag.Debit.equals(entry.getDebitCreditFlag())){
						balance.setDebitValue(balance.getDebitValue() + entry.getValue());
						balance.setDebitChg(balance.getDebitChg() + entry.getValue());
					}else if(DictConstants.DebitCreditFlag.Credit.equals(entry.getDebitCreditFlag())){
						balance.setDebitValue(balance.getDebitValue() - entry.getValue());
						balance.setCreditChg(balance.getCreditChg() + entry.getValue());
					}
				}
					
				if(tbkSubjectDef.getPayRecive() != null && !"".equals(tbkSubjectDef.getPayRecive())){
					if(tbkSubjectDef.getPayRecive().equals(DictConstants.payRecive.P)){
						if(DictConstants.DebitCreditFlag.Pay.equals(entry.getDebitCreditFlag())){
							balance.setPayChg(balance.getPayChg()+entry.getValue());
							balance.setPayValue(balance.getPayValue() + entry.getValue());
						}else if(DictConstants.DebitCreditFlag.Receive.equals(entry.getDebitCreditFlag())){
							balance.setPayChg(balance.getPayChg() - entry.getValue());
							balance.setReceiveChg(balance.getReceiveChg() + entry.getValue());
						}
					}else{
						if(DictConstants.DebitCreditFlag.Pay.equals(entry.getDebitCreditFlag())){
							balance.setPayChg(balance.getPayChg()+entry.getValue());
							balance.setPayValue(balance.getPayValue() + entry.getValue());
						}else if(DictConstants.DebitCreditFlag.Receive.equals(entry.getDebitCreditFlag())){
							balance.setPayChg(balance.getPayChg() - entry.getValue());
							balance.setReceiveChg(balance.getReceiveChg() + entry.getValue());
						}
					}
				}else{
					if(DictConstants.DebitCreditFlag.Pay.equals(entry.getDebitCreditFlag())){
						balance.setPayChg(balance.getPayChg()+entry.getValue());
						balance.setPayValue(balance.getPayValue() + entry.getValue());
					}else if(DictConstants.DebitCreditFlag.Receive.equals(entry.getDebitCreditFlag())){
						balance.setPayChg(balance.getPayChg() - entry.getValue());
						balance.setReceiveChg(balance.getReceiveChg() + entry.getValue());
					}
				}
				balance.setUpdateTime(DateUtil.getCurrentDateTimeAsString());
				subjectBalanceSumService.insert(balance);
			}
		}
	}
	@Override
	public void checkInfoSceneEntry(InstructionPackage instructionPackage) {
		// TODO Auto-generated method stub
		LogManager.getLogger(LogManager.MODEL_BOOKKEEPING).info("引擎输入参数 校验："+instructionPackage.toString());
		Map<String, Object> paramsMap = new HashMap<String, Object>();
		JY.require(null != instructionPackage.getInfoMap(), "请指定会计引擎的业务信息：InfoMap！");
		JY.require(null != instructionPackage.getAmtMap(), "请指定会计引擎的金额信息：AmtMap！");
		//获得事件入口  eventId   instructionPackage.getInfoMap为业务信息
		String eventId = ParameterUtil.getString(instructionPackage.getInfoMap(), "eventId", null);
		JY.require(null != eventId, "交易编号为："+ ParameterUtil.getString(instructionPackage.getInfoMap(),"dealNo","")+"的交易，未找到对应的IFRS9会计事件代码");
		
		String prdNo  =  ParameterUtil.getString(instructionPackage.getInfoMap(), "prdNo", null);
		JY.require(null != prdNo, "请指定InfoMap的会计产品代码prdNo!");
		//检查产品是否存在该事件
		paramsMap.put("eventId", eventId);
		paramsMap.put("prdNo", prdNo);
		TbProductEvent productEvent = productEventMapper.getTbProductEvent(paramsMap);
		JY.require(null != productEvent, "当前产品："+prdNo+"没有对应的事件:"+eventId);
		//根据事件和产品确定其维度范围
		List<TbProductEventDimension> dimensions = this.productEventDimensionMapper.getTbProductEventDimesions(paramsMap);
		JY.require(dimensions.size()>0, "当前产品："+prdNo+",事件:"+eventId+"无会计维度！");
		/**
		 * 预处理，增加部分动态维度逻辑
		 */
		dealPretreatment(instructionPackage);
		//循环会计维度获取值
		StringBuffer dimensionKeys = new StringBuffer();StringBuffer dimensionValues = new StringBuffer();
		for(TbProductEventDimension dimension : dimensions) {
			JY.require(null != ParameterUtil.getString(instructionPackage.getInfoMap(),dimension.getDimensionId(),null),
					"当前产品："+prdNo+",事件:"+eventId+"维度:"+dimension.getDimensionId()+"不存在业务信息Map数据中！");
			dimensionKeys.append(dimension.getDimensionId()).append(",");
			dimensionValues.append(ParameterUtil.getString(instructionPackage.getInfoMap(),dimension.getDimensionId(),".")).append(",");
		}
		String key = dimensionKeys.toString();String value = dimensionValues.toString();
		paramsMap.put("dimensionKeys", key.substring(0, key.length()-1));
		paramsMap.put("dimensionValues", value.substring(0, value.length()-1));
		//循环会计场景获得groupId
		String groupId = productEventDimensionKvMapper.getTbProductEventDimensionKvGroupId(paramsMap);
		JY.require(null != groupId,"该笔交易未找到[dimensionKeys："+ ParameterUtil.getString(paramsMap,"dimensionKeys","")
				+"],[dimensionValues :"+ ParameterUtil.getString(paramsMap,"dimensionValues","")+"]对应的IFRS9会计场景，请检查会计三分类、收息方式等交易要素 ");
		//通过场景获得会计分录
		paramsMap.put("groupId", groupId);
		List<TbProductEventSceneAcup> acups = this.productEventSceneAcupMapper.getTbProductEventSceneAcupsByGroupId(paramsMap);
		if(acups == null || acups.size()<=0){
			throw new RException("该笔交易未找到对应的IFRS9分录科目配置，请检查交易的客户类型或者再保理类型等交易要素！");
		}
	}
	
	/**
	 * @author guomeijuan 20191023
	 * @param map 
	 * @param expression key1+key2+固定值；key1与key2在map中获取，最后拼接固定值
	 * @return 实际科目
	 */
	public static String calcSubject(Map<String, Object> map, String expression){
		JY.require(!"".equals(expression), "表达式不能为空！");
		JY.require(expression.indexOf("+")>-1, "表达式不满足规则：key1+key2+固定值，实际表达式为"+expression);
		String[] expressionArr = expression.split("\\+");
		JY.require(expressionArr.length==3, "表达式不满足规则：key1+key2+固定值，实际表达式为"+expression);
		String result = ParameterUtil.getString(map, expressionArr[0], "ERROR");
		JY.require(result.indexOf("ERROR")<0, "map中获取value错误，key为"+expressionArr[0]);
		result = result + ParameterUtil.getString(map, expressionArr[1], "ERROR");
		JY.require(result.indexOf("ERROR")<0, "map中获取value错误，key为"+expressionArr[1]);
		result = result + expressionArr[2];
		return result;
	}
	
	/***
	 * 倒起息-负债定期使用
	 * 生成起息日至账务日期的利息账以及付息账
	 * 备注：完全倒起息（即交易到期日<=当前账务日期）需要出到期日分录，
	 * 不需要出到期日当天计提分录；需要出到期日FTP成本、到期日FTP利润，
	 * 不需要出到期日当天计提FTP成本与FTP利润
	 */
	@Override
	public Map<String,Object> generatePreIntForDepositDq(Map<String,Object> params){
		//amtParam 金额参数 需要传配置的表达式需要的参数
		Map<String,Object> amtParam = new HashMap<String, Object>();
		return params;
	}
	
	
	
	
	/***
	 * 倒起息-负债活期使用
	 * 生成起息日至账务日期的利息账以及付息账
	 * 备注：完全倒起息（即交易到期日<=当前账务日期）需要出到期日分录，
	 * 不需要出到期日当天计提分录；需要出到期日FTP成本、到期日FTP利润，
	 * 不需要出到期日当天计提FTP成本与FTP利润
	 */
	@Override
	public Map<String,Object> generatePreIntForDepositHq(Map<String,Object> params){
		Map<String,Object> amtParam = new HashMap<String, Object>();
		return params;
	}
	
	
	
	
	/***
	 * 倒起息-行内资金转移使用
	 * 生成起息日至账务日期的利息账以及到期账
	 * 注：行内资金转移无收付息计划
	 * @param params
	 * @return
	 */
	@Override
	public Map<String,Object> generatePreIntForHnzjzy(Map<String,Object> params){
		Map<String,Object> amtParam = new HashMap<String, Object>();
		return params;
	}
	

	/**
	 * T+N,T+M倒起息
	 * 若账务日期大于出款日 小于起息日 （出款日-当前账务日期计提）
	 * 若账务日期大于出款日  大于等于起息日  小于到期日 （出款日--起息日）
	 * 若账务日期大于到期日  小于回款日 （出款日--起息日    到期日-账务日）
	 * 若账务日期大于等于回款日（出款日--起息日    到期日-回款日）
	 *@param startDate   计提开始日期（出款日/到期日）
	 *@param flag 是否T+N起息 -0  是否T+N到期-1 
	 */

	
	/**
	 * @author guomeijuan 20191025
	 * 生成起息日至账务日期的利息帐以及收息帐
	 * @param params 出账参数，必须包含单号dealNo、利率rate、计息基础basis、起息日期vdate、到期日期mdate、本金amt以及账务所需的必要的参数
	 * @return SYS_TOTAL_INT 未收利息总和  ACT_TOTAL_INT 已收利息总和
	 * 备注：完全倒起息（即交易到期日<=当前账务日期）需要出到期日分录，不需要出计提分录；需要出到期日FTP成本、到期日FTP利润，不需要出到期日当天计提FTP成本与FTP利润
	 */
	@Override
	public Map<String,Object> generatePreInt(Map<String,Object> params) {
		//amtParam 金额参数 需要传配置的表达式需要的参数
		Map<String,Object> amtParam = new HashMap<String, Object>();
		return params;
	}

	 
	@Override
	public BigDecimal calcInt(String beginDate, String endDate, Map<String, Object> params) {
		//本金
		String amt = ParameterUtil.getString(params, "amt", "");
		JY.require(!StringUtil.checkEmptyNull(amt), "缺少必要的参数amt");
		//利率
		String rate = ParameterUtil.getString(params, "rate", "");
		JY.require(!StringUtil.checkEmptyNull(rate), "缺少必要的参数rate");
		//计息基础
		String basis = ParameterUtil.getString(params, "basis", "");
		JY.require(!StringUtil.checkEmptyNull(basis), "缺少必要的参数basis");
		int intBasis = Ifrs9Constants.A360.equals(basis)?360:365;
		//计算结果
		int days = 0;
		days = DateUtil.daysBetween(beginDate, endDate);
		
		//1.本金*利率/100*期限/计息基础
		BigDecimal result = new BigDecimal(amt).multiply(new BigDecimal(rate)).divide(new BigDecimal("100")).multiply(BigDecimal.valueOf(days+1))
				.divide(BigDecimal.valueOf(intBasis),2,BigDecimal.ROUND_HALF_UP);
		
		if(days > 0) {
			result = result.subtract(new BigDecimal(amt).multiply(new BigDecimal(rate)).divide(new BigDecimal("100"))
					.multiply(BigDecimal.valueOf(days)).divide(BigDecimal.valueOf(intBasis),2,BigDecimal.ROUND_HALF_UP));
		}
		/*BigDecimal result = calculate(BigDecimal.valueOf(amt),BigDecimal.valueOf(rate),intBasis,days+1);
		
		if(days > 0) {
			BigDecimal result1 = calculate(BigDecimal.valueOf(amt),BigDecimal.valueOf(rate),intBasis,days);
			result = result.subtract(result1);
		}*/
		
		
		return result;
	}
	/**
	 * 计算区间利息
	 * @param beginDate
	 * @param endDate
	 * @param params
	 * @return
	 */
	@Override
	public BigDecimal calcIntervalInt(String beginDate, String endDate, Map<String, Object> params) {
		//本金
		String amt = ParameterUtil.getString(params, "amt", "");
		JY.require(!StringUtil.checkEmptyNull(amt), "缺少必要的参数amt");
		//利率
		String rate = ParameterUtil.getString(params, "rate", "");
		JY.require(!StringUtil.checkEmptyNull(rate), "缺少必要的参数rate");
		//计息基础
		String basis = ParameterUtil.getString(params, "basis", "");
		JY.require(!StringUtil.checkEmptyNull(basis), "缺少必要的参数basis");
		int intBasis = Ifrs9Constants.A360.equals(basis)?360:365;
		//计算结果
		int days = 0;
		days = DateUtil.daysBetween(beginDate, endDate);
		
		//1.本金*利率/100*期限/计息基础
		BigDecimal result = new BigDecimal(amt).multiply(new BigDecimal(rate)).divide(new BigDecimal("100")).multiply(BigDecimal.valueOf(days))
				.divide(BigDecimal.valueOf(intBasis),2,BigDecimal.ROUND_HALF_UP);
		return result;
	}
	
	/***
	 * 计算利息
	 * @param amt  本金
	 * @param rate 利率（带百分比）
	 * @param intBasis 计息基础（360/365）
	 * @param days 期限
	 * @return
	 */
	public BigDecimal calculate(BigDecimal amt,BigDecimal rate,int intBasis,int days) {
		BigDecimal result = BigDecimal.ZERO;
		BigDecimal multiplyData = amt.multiply(rate).multiply(BigDecimal.valueOf(days));
		BigDecimal divideData = BigDecimal.valueOf(100).multiply(BigDecimal.valueOf(intBasis));
		result = multiplyData.divide(divideData,2,BigDecimal.ROUND_HALF_UP);
		return result;
	}
	/**
	 * 计算摊销利息
	 * 起息日   到期日   账务日期   收息金额
	 */
	@Override
	public BigDecimal calculateIntBefore(String beginDate,String endDate,String postDate,BigDecimal amt) {
		BigDecimal result=BigDecimal.ZERO;
		//计算结果
		int days = 0;
		int term = 0;
		term = DateUtil.daysBetween(beginDate, endDate);
		days= DateUtil.daysBetween(postDate, endDate);

		//1.本金*利率/100*期限/计息基础
		 result = amt.divide(BigDecimal.valueOf(term),2,BigDecimal.ROUND_HALF_UP).multiply(BigDecimal.valueOf(days));
		
		if(days > 0) {
			result = result.subtract(amt.divide(BigDecimal.valueOf(term),2,BigDecimal.ROUND_HALF_UP).multiply(BigDecimal.valueOf(days-1)));
		}
		return result;
	}
    /**
     * 系统内资金往来倒起息
     * */
	@Override
	public Map<String, Object> generatePreXTNInt(Map<String, Object> params) {
		return params;
	}
}
