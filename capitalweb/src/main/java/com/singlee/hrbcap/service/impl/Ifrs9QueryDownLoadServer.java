package com.singlee.hrbcap.service.impl;


import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.dict.model.TaDictVo;
import com.singlee.capital.dict.service.DictionaryGetService;
import com.singlee.capital.dict.service.TaDictVoService;
import com.singlee.capital.reports.util.ExcelUtil;
import com.singlee.capital.reports.util.ReportType;
import com.singlee.capital.reports.util.ReportUtils;
import com.singlee.hrbcap.model.acup.TbEntry;
import com.singlee.hrbcap.service.TbEntryService;
import org.apache.poi.ss.usermodel.CellStyle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class Ifrs9QueryDownLoadServer implements Runnable {

	@Autowired
	private TbEntryService tbEntryService;
	
	@Autowired
	private DictionaryGetService dictionaryGetService;
	
	@Autowired
	private TaDictVoService taDictVoService;
	
	@Override
	public void run() {
		FileOutputStream writer = null ;
		ByteArrayOutputStream os =null;
		try {
			
			while (com.singlee.hrbcap.service.impl.TbEntryServiceImpl.downLoadParamMap.containsKey("TbEntryParam")) {
				os = new ByteArrayOutputStream();
				System.out.println("startDownLoadDate:"+ DateUtil.getCurrentCompactDateTimeAsString());
				ExcelUtil e= new ExcelUtil(ReportUtils.getReportModelFilePath(ReportType.EntriesQuery.getFileName()));
				Map<String, Object> map= com.singlee.hrbcap.service.impl.TbEntryServiceImpl.downLoadParamMap.get("TbEntryParam");
				Map<String, Object> params=new HashMap<String, Object>();
				params.put("dict_id", "downloadPageSize");
				TaDictVo taDictVo=taDictVoService.getTaDict(params, ParameterUtil.getRowBounds(map));
				if(null!=taDictVo){
					map.put("pageSize", taDictVo.getDict_key());
				}else{
					map.put("pageSize", 150000);
				}
				Page<List<TbEntry>> list = tbEntryService.getEntryList(map, ParameterUtil.getRowBounds(map));
				CellStyle center = e.getDefaultCenterStrCellStyle();
				int row = 1;
				for(int j=0;j<=list.size()/65531;j++){//excel 单个sheet导出最大行为 65535  65531
					e.getSheetAt(j).setColumnWidth(0, 20 * 256);
					e.getSheetAt(j).setColumnWidth(1, 20 * 256);
					e.getSheetAt(j).setColumnWidth(2, 20 * 256);
					e.getSheetAt(j).setColumnWidth(3, 20 * 256);
					e.getSheetAt(j).setColumnWidth(4, 20 * 256);
					e.getSheetAt(j).setColumnWidth(5, 20 * 256);
					e.getSheetAt(j).setColumnWidth(6, 20 * 256);
					e.getSheetAt(j).setColumnWidth(7, 20 * 256);
					e.getSheetAt(j).setColumnWidth(8, 20 * 256);
					e.getSheetAt(j).setColumnWidth(9, 20 * 256);
					e.getSheetAt(j).setDefaultRowHeightInPoints(20);
					e.getSheetAt(j).getWorkbook().setSheetName(j, "会计分录查询"+(j+1));
				}
				for(int i=0;i<list.size();i++){
					int j = i/65531;
					int z=row+2+i;
					if(j>0) {
                        z=i-65531*j;
                    }
					e.writeStr(j, z,"A", center,((TbEntry)list.get(i)).getFlowId());
					e.writeStr(j, z,"B", center,((TbEntry)list.get(i)).getFlowSeq()+"");
					e.writeStr(j, z,"C", center,((TbEntry)list.get(i)).getTaskName());
					e.writeStr(j, z,"D", center,((TbEntry)list.get(i)).getPostDate());
					e.writeStr(j, z,"E", center,((TbEntry)list.get(i)).getDealNo());			
					e.writeStr(j, z,"F", center,((TbEntry)list.get(i)).getPrdName());
					e.writeStr(j, z,"G", center,((TbEntry)list.get(i)).getSponInst());
					e.writeStr(j, z,"H", center,((TbEntry)list.get(i)).getBkkpgOrgId());
					e.writeStr(j, z,"I", center,((TbEntry)list.get(i)).getEventName());
					e.writeStr(j, z,"J", center,((TbEntry)list.get(i)).getCcy()== null?"":dictionaryGetService.getTaDictByCodeAndKey("Currency",((TbEntry)list.get(i)).getCcy()).getDict_value());
					e.writeStr(j, z,"K", center,((TbEntry)list.get(i)).getDebitCreditFlag()== null?"":dictionaryGetService.getTaDictByCodeAndKey("DebitCreditFlag",((TbEntry)list.get(i)).getDebitCreditFlag()).getDict_value());
					e.writeStr(j, z,"L", center,((TbEntry)list.get(i)).getSubjCode());
					e.writeStr(j, z,"M", center,((TbEntry)list.get(i)).getSubjName());
					e.writeDouble(j, z,"N", e.getDefaultDouble2CellStyle(),((TbEntry)list.get(i)).getValue());
					e.writeStr(j, z,"O", center,((TbEntry)list.get(i)).getState() == null?"":dictionaryGetService.getTaDictByCodeAndKey("EntryState",((TbEntry)list.get(i)).getState()).getDict_value());
					e.writeStr(j, z,"P", center,((TbEntry)list.get(i)).getErrMsg());
					e.writeStr(j, z,"Q", center,((TbEntry)list.get(i)).getUpdateTime());
				}
				e.setForceRecalculation();
				e.getWb().write(os);
				byte[] content = os.toByteArray();
				String filePath=map.get("filePath").toString();
				File pathFile = new File(filePath);
				if(!pathFile.exists()) {
					pathFile.mkdirs();
				}
				File writefile = new File(filePath+ ReportType.EntriesQuery.getDesc()+ DateUtil.getCurrentCompactDateTimeAsString()+".xls");
				writefile.setWritable(true, false);
				writefile.setExecutable(true, false);//设置可执行权限  
				writefile.setReadable(true, false);
				writefile.delete();
				writer = new FileOutputStream(writefile);
				writer.write(content);
				
				com.singlee.hrbcap.service.impl.TbEntryServiceImpl.downLoadParamMap.remove("TbEntryParam");
				content=null;
				System.out.println(writefile.getPath());
				System.out.println("endDownLoadDate:"+ DateUtil.getCurrentCompactDateTimeAsString());
			}
		} catch (Exception e) {
			com.singlee.hrbcap.service.impl.TbEntryServiceImpl.downLoadParamMap.remove("TbEntryParam");
			e.printStackTrace();
		}finally{
			com.singlee.hrbcap.service.impl.TbEntryServiceImpl.downLoadParamMap.remove("TbEntryParam");
			try {
				if(writer != null) {
                    writer.close();
                }
				if(os!=null) {
                    os.close();
                }
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
