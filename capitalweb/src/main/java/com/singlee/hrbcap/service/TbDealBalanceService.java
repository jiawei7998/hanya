package com.singlee.hrbcap.service;


import com.github.pagehelper.Page;
import com.singlee.cap.bookkeeping.model.QueryBalanceModel;
import com.singlee.hrbcap.model.acup.TbDealBalance;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/**
 * 交易科目余额的服务接口
 * @author shiting
 *
 */
public interface TbDealBalanceService {
	
	void insert(TbDealBalance balance);
	/**
	 * 更新余额，只更新四个数字余额（借、贷、支、付）
	 * @param balance
	 */
	public void updateBalance(TbDealBalance balance);
	
	public Page<TbDealBalance> selectByMap(Map<String,Object> map,RowBounds rb);

	public List<TbDealBalance> selectByMap(Map<String, Object> map);
	
	/**
	 * 基金科目余额查询
	 * @param map
	 * @return
	 */
	public Page<QueryBalanceModel> queryFundBal(Map<String,Object> map);
	
	/**
	 * 产品科目余额查询
	 * @param map
	 * @return
	 */
	public Page<QueryBalanceModel> queryProductBal(Map<String,Object> map);
	
	/**
	 * 查询需要调账交易的账务汇总信息
	 * @param dealNo
	 */
	public Page<TbDealBalance> queryEntrySum(Map<String, Object> map);
}