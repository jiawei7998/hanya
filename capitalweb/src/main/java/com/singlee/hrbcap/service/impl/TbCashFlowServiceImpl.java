package com.singlee.hrbcap.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.*;
import com.singlee.capital.duration.service.DurationConstants;
import com.singlee.fund.mapper.FtInfoMapper;
import com.singlee.hrbcap.mapper.TbCashFlowMapper;
import com.singlee.hrbcap.model.base.TbCashFlow;
import com.singlee.hrbcap.model.base.TbCashFlowDto;
import com.singlee.hrbcap.service.TbCashFlowService;
import com.singlee.hrbcap.util.Ifrs9Constants;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Map;
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TbCashFlowServiceImpl implements TbCashFlowService {
    @Autowired
    TbCashFlowMapper tbCashFlowMapper;
    @Autowired
    FtInfoMapper infoMapper;

    @Override
    public void saveCashFlow(Map<String, Object> acupInfoParam, Map<String, Object> amtParam) {
        String postDate = ParameterUtil.getString(acupInfoParam, "postDate", "");
        JY.require(StringUtil.notNullOrEmpty(postDate), "保存现金流，缺少必要的参数postDate");
        String sponinst = ParameterUtil.getString(acupInfoParam, "sponinst", "");
        String dealNo = ParameterUtil.getString(acupInfoParam, "dealNo", "");
        JY.require(StringUtil.notNullOrEmpty(dealNo), "保存现金流，缺少必要的参数dealNo");
        String prdNo = ParameterUtil.getString(acupInfoParam, "prdNo", "");
        JY.require(StringUtil.notNullOrEmpty(prdNo), "保存现金流，缺少必要的参数prdNo");

        String cust= ParameterUtil.getString(acupInfoParam, "cust", "");
        String cname= ParameterUtil.getString(acupInfoParam, "cname", "");
        String fundCode= ParameterUtil.getString(acupInfoParam, "fundCode", "");
        String tdate= ParameterUtil.getString(acupInfoParam, "tdate", "");

        JY.info("************************************dealNo:"+dealNo+"开始保存现金流*****************************");
        String cashFlowDealNo = ParameterUtil.getString(acupInfoParam, "cashFlowDealNo", "");
        String eventId = ParameterUtil.getString(acupInfoParam, "eventId", "");
        JY.require(StringUtil.notNullOrEmpty(eventId), "保存现金流，缺少必要的参数eventId");
        TbCashFlow bean = new TbCashFlow();
        String fundName="";
        BigDecimal amt = BigDecimal.ZERO;
        if(Ifrs9Constants.Ifrs9EventCode.Event_VDATE.equals(eventId)) {
            //起息帐
            amt = new BigDecimal(ParameterUtil.getString(amtParam, DurationConstants.AccType.LOAN_AMT, "0"));
            if ("801".equals(prdNo)){
                bean.setCftype(Ifrs9Constants.CashFlowType.FundType);
            }else if ("802".equals(prdNo)){
                bean.setCftype(Ifrs9Constants.CashFlowType.BondFundType);
            }else if ("803".equals(prdNo)){
                bean.setCftype(Ifrs9Constants.CashFlowType.BondFundType);
            }else {
                bean.setCftype(Ifrs9Constants.CashFlow.VDATE);
            }
            bean.setTamt(amt);

        }else if(Ifrs9Constants.Ifrs9EventCode.Event_VERIFYINT.equals(eventId)||
                Ifrs9Constants.Ifrs9EventCode.Event_RINT.equals(eventId)) {
            //收息帐--收息勾对帐
            amt = new BigDecimal(ParameterUtil.getString(amtParam, DurationConstants.AccType.INCOME_INT, "0"));
            bean.setCftype(Ifrs9Constants.CashFlow.RPI);//默认收息
            bean.setTamt(amt);
        }else if(Ifrs9Constants.Ifrs9EventCode.Event_VERIFY.equals(eventId)) {
            //基金确认帐
            amt = new BigDecimal(ParameterUtil.getString(amtParam, DurationConstants.AccType.LOAN_AMT, "0"));
            BigDecimal fee = new BigDecimal(ParameterUtil.getString(amtParam, DurationConstants.AccType.FEE, "0"));
            BigDecimal loan_vamt = new BigDecimal(ParameterUtil.getString(amtParam, DurationConstants.AccType.LOAN_VAMT, "0"));
            if ("801".equals(prdNo)){
                bean.setCftype(Ifrs9Constants.CashFlowType.FundTypeACK);
            }else if ("802".equals(prdNo)){
                bean.setCftype(Ifrs9Constants.CashFlowType.BondFundTypeACK);
            }else if ("803".equals(prdNo)){
                bean.setCftype(Ifrs9Constants.CashFlowType.CashFundTypeACK);
            }else {
                bean.setCftype(Ifrs9Constants.CashFlow.VERIFY);
            }
            bean.setTamt(amt.subtract(fee).subtract(loan_vamt));
        }else if(Ifrs9Constants.Ifrs9EventCode.Event_BOND.equals(eventId)||
                Ifrs9Constants.Ifrs9EventCode.Event_DRT.equals(eventId)) {
            //分红或再投
            BigDecimal incomeInt = null;
            if (Ifrs9Constants.Ifrs9EventCode.Event_BOND.equals(eventId)){
                incomeInt=new BigDecimal(ParameterUtil.getString(amtParam, DurationConstants.AccType.LOAN_BOND, "0"));
            }else if (Ifrs9Constants.Ifrs9EventCode.Event_DRT.equals(eventId)){
                incomeInt= new BigDecimal(ParameterUtil.getString(amtParam, DurationConstants.AccType.LOAN_DRT, "0"));
            }
            if ("801".equals(prdNo)){
                if (Ifrs9Constants.Ifrs9EventCode.Event_BOND.equals(eventId)){
                    bean.setCftype(Ifrs9Constants.CashFlowType.FundTypeShare);
                }else{
                    bean.setCftype(Ifrs9Constants.CashFlowType.FundTypeAgain);
                }
            }else if ("802".equals(prdNo)){
                if (Ifrs9Constants.Ifrs9EventCode.Event_BOND.equals(eventId)){
                    bean.setCftype(Ifrs9Constants.CashFlowType.BondFundTypeShare);
                }else{
                    bean.setCftype(Ifrs9Constants.CashFlowType.BondFundTypeAgain);
                }
            }else if ("803".equals(prdNo)){
                if (Ifrs9Constants.Ifrs9EventCode.Event_BOND.equals(eventId)){
                    bean.setCftype(Ifrs9Constants.CashFlowType.CashFundTypeShare);
                }else{
                    bean.setCftype(Ifrs9Constants.CashFlowType.CashFundTypeAgain);
                }
            }else {
                bean.setCftype(Ifrs9Constants.CashFlow.RPI);
            }
            bean.setTamt(incomeInt);
        }else if(Ifrs9Constants.Ifrs9EventCode.Event_MDATE.equals(eventId)) {
            //到期现金流
            amt = new BigDecimal(ParameterUtil.getString(amtParam, DurationConstants.AccType.LOAN_AMT, "0"));
            BigDecimal incomeInt = new BigDecimal(ParameterUtil.getString(amtParam, DurationConstants.AccType.INCOME_INT, "0"));
            //基金赎回时，红利为AMT_INTEREST
            incomeInt = incomeInt.add(new BigDecimal(ParameterUtil.getString(amtParam, DurationConstants.AccType.AMT_INTEREST, "0")));
            if ("801".equals(prdNo)){
                bean.setCftype(Ifrs9Constants.CashFlowType.FundTypeBack);
            }else if ("802".equals(prdNo)){
                bean.setCftype(Ifrs9Constants.CashFlowType.BondFundTypeBack);
            }else if ("803".equals(prdNo)){
                bean.setCftype(Ifrs9Constants.CashFlowType.CashFundTypeBack);
            }else {
                bean.setCftype(Ifrs9Constants.CashFlow.MDATE);
            }
            bean.setTamt(amt.add(incomeInt));
        }
        if(Ifrs9Constants.Ifrs9EventCode.Event_VERIFYINT.equals(eventId)) {
            //收息勾对时更新
            bean.setDealNo(dealNo);
            bean.setOccudate(postDate);
            bean.setSponinst(sponinst);
            tbCashFlowMapper.updateCashFlowByDate(ParentChildUtil.ClassToHashMap(bean));
        }else {
            bean.setDealNo(dealNo);
            bean.setOccudate(postDate);
            bean.setOpertime(DateUtil.getCurrentDateTimeAsString());
            bean.setSponinst(sponinst);
            bean.setTdate(tdate);
            bean.setCust(cust);
            bean.setFundCode(fundCode);
            if (fundCode!=null&&fundCode.length()>0){
                fundName=infoMapper.selectFundNameByFundCode(fundCode);
            }
            bean.setFundName(fundName);
            bean.setCname(cname);
            if(BigDecimal.ZERO.compareTo(bean.getTamt()) != 0) {
                tbCashFlowMapper.insert(bean);
            }
        }

        JY.info("************************************dealNo:"+dealNo+"保存现金流成功*****************************");
    }

    @Override
    public PageInfo<TbCashFlow> queryCashFlow(Map<String, Object> map) {

        RowBounds rowBounds = ParameterUtil.getRowBounds(map);
        Page<TbCashFlow> page = tbCashFlowMapper.queryCashFlow(map, rowBounds);
        return new PageInfo<TbCashFlow>(page);
    }

    @Override
    public Page<TbCashFlowDto> getCashFlow(Map<String, Object> map) {

        RowBounds rowBounds = ParameterUtil.getRowBounds(map);
        Page<TbCashFlowDto> page = tbCashFlowMapper.getCashFlow(map, rowBounds);
//        Page<TbCashFlow> page = tbCashFlowMapper.queryCashFlow(map, rowBounds);
        return page;
    }
}
