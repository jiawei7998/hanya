package com.singlee.hrbcap.service.impl;

import com.singlee.capital.system.pojo.RoleModule;
import com.singlee.hrbcap.model.base.SceneResultBean;
import com.singlee.hrbcap.model.base.TbDimensionValue;
import com.singlee.hrbcap.model.base.TbProductEventDimensionKv;
import com.singlee.hrbcap.model.base.dimensionKvSaveVo;
import com.singlee.hrbcap.service.TbProductEventDimensionKvService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 * 会计事件表serviceIMpl
 */
@Service("tbProductEventDimensionKvService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TbProductEventDimensionKvServiceImpl implements TbProductEventDimensionKvService {
	@Autowired
	private com.singlee.hrbcap.mapper.TbProductEventDimensionKvMapper TbProductEventDimensionKvMapper;
	@Autowired
	private com.singlee.hrbcap.mapper.TbDimensionValueMapper TbDimensionValueMapper;
	
	private static Logger accountLog = LoggerFactory.getLogger("ACCOUNT");
	

	@Override
	public List<TbProductEventDimensionKv> getDataForProductAndEvent(Map<String, Object> map){
		
		List<TbProductEventDimensionKv> pdkList =TbProductEventDimensionKvMapper.getDataForProductAndEvent(map);
//		List<String> pdkIdList =new ArrayList<String>();
//		for(int i=0;i<pdkList.size();i++){
//			TbProductEventDimensionKv pdk =pdkList.get(i);
//		
//			if(i==0|| !pdkList.get(i).getDimensionId().equals(pdkList.get(i-1).getDimensionId())){
//			pdkIdList.add(pdk.getDimensionId());
//			}
//			pdkIdList.add(pdk.getDimensionValueId());
//		}
		
		return pdkList;
	}
	@Override
	public List<SceneResultBean> getSceneResultList(Map<String, Object> map){
		
		return TbProductEventDimensionKvMapper.getSceneResultList(map);
	}
	/**
	 * 删除所选
	 * @param map
	 */
	@Override
	public void deleteTbProductEventDimensionKv(@RequestBody Map<String, Object> map){
		TbProductEventDimensionKvMapper.deleteTbProductEventDimensionKv(map);
	}
	
	@Override
	public void saveTbProductEventDimensionKv(dimensionKvSaveVo dimensionKvSaveVo) {
		
		String prdNo = dimensionKvSaveVo.getPrdNo();
		String eventId = dimensionKvSaveVo.getEventId();
		// 维度码值List
		List<TbDimensionValue> dimensionKvList = dimensionKvSaveVo.getDimensionKvList();
		
		//把维度码值list转换成以维度为key的map
		Map<String, List<TbDimensionValue>> vomap = new HashMap<String, List<TbDimensionValue>>();		
		if (dimensionKvList != null) {			
			for (int i = 0; i < dimensionKvList.size(); i++) {				
				TbDimensionValue value = dimensionKvList.get(i);
				if(vomap.get(value.getDimensionId())==null){
					 List<TbDimensionValue> list1 = new ArrayList<TbDimensionValue>();	
					 list1.add(value);
					 vomap.put(value.getDimensionId(), list1);	
				}else{
					 List<TbDimensionValue> list1 =vomap.get(value.getDimensionId());
					 list1.add(value);
					 vomap.put(value.getDimensionId(), list1);	
					 
				}
						
				}		
			}
		//Map转换成List<List<>>
//		List<List<TbDimensionValue>> resultList = new ArrayList(vomap.values());
		
		//解析成多个维度
		List<List<TbDimensionValue>> tdvList = new ArrayList<List<TbDimensionValue>>();
		 for(String key:vomap.keySet())
	        {
			 List<TbDimensionValue> list2 = vomap.get(key);	
			 tdvList.add(list2);
	        }

		
		// 删除产品事件下的维度
		TbProductEventDimensionKv tbProductEvDim_delete = new TbProductEventDimensionKv();
		tbProductEvDim_delete.setPrdNo(prdNo);
		tbProductEvDim_delete.setEventId(eventId);
		try {
			TbProductEventDimensionKvMapper.delete(tbProductEvDim_delete);
		} catch (Exception e) {
			accountLog.info(e.getMessage());
		}
		
		TbProductEventDimensionKv ProEventDimKv = new TbProductEventDimensionKv();
		
		ProEventDimKv.setPrdNo(prdNo);
		ProEventDimKv.setEventId(eventId);
		
		// 声明 用于笛卡尔积操作生成的结果数组descartesList
		List<List<TbDimensionValue>> descartesList = new ArrayList<List<TbDimensionValue>>();
				
				// 调用笛卡尔积方法
		descartes(tdvList, descartesList, 0, new ArrayList<TbDimensionValue>());
		
		for(int i = 0; i < descartesList.size(); i++) {
			List<TbDimensionValue> descartesList1 = descartesList.get(i); // descartesList1 一维数据
			String groupId = prdNo + "." + eventId;
			// 把同一个分类的组合代码的pValue拼接用于生成subId
			for (int j = 0; j < descartesList1.size(); j++) {
				TbDimensionValue descartesList2 = descartesList1.get(j); // descartesList2 二维数据
				groupId += "." + descartesList2.getDbId();
			}
			// 循环把同一个分类下的维度信息添加到数据库
			for (int j = 0; j < descartesList1.size(); j++) {
				TbDimensionValue descartesList2 = descartesList1.get(j);

				// 添加
				ProEventDimKv.setGroupId(groupId);
				ProEventDimKv.setDimensionValueId(descartesList2.getDbId());
				ProEventDimKv.setDimensionId(descartesList2.getDimensionId());
				ProEventDimKv.setDimensionValue(descartesList2.getDimensionValue());
				TbProductEventDimensionKvMapper.insert(ProEventDimKv);
			}
		}
	    
			
	}
	

	// 笛卡尔积 
	// dimvalue 需要进行笛卡尔积操作的数组
	// result 结果输出的数组
	// layer 层数  取值一般为0
	// curList 默认new一个ArrayList来方便中间操作
	private void descartes(List<List<TbDimensionValue>> dimvalue, List<List<TbDimensionValue>> result, int layer, List<TbDimensionValue> curList) {  
        if (layer < dimvalue.size() - 1) {  
            if (dimvalue.get(layer).size() == 0) {  
                descartes(dimvalue, result, layer + 1, curList);  
            } else {  
                for (int i = 0; i < dimvalue.get(layer).size(); i++) {  
                    List<TbDimensionValue> list = new ArrayList<TbDimensionValue>(curList);  
                    list.add(dimvalue.get(layer).get(i));  
                    descartes(dimvalue, result, layer + 1, list);  
                }  
            }  
        } else if (layer == dimvalue.size() - 1) {  
            if (dimvalue.get(layer).size() == 0) {  
                result.add(curList);  
            } else {  
                for (int i = 0; i < dimvalue.get(layer).size(); i++) {  
                    List<TbDimensionValue> list = new ArrayList<TbDimensionValue>(curList);  
                    list.add(dimvalue.get(layer).get(i));  
                    result.add(list);  
                }  
            }  
        }  
    }
 
	@Override
	public List<SceneResultBean> searchTbDimensionValue(Map<String, Object> params) {
		//获取所有维度
//		return TbDimensionValueMapper.getTbDimensionValueListOrder();
		//按产品事件获取维度
		return TbDimensionValueMapper.getTbDimensionValueListByPE(params);
	}
	@Override
	public List<RoleModule> searchModuleForReport() {

		List<TbDimensionValue> orignial =  TbDimensionValueMapper.getTbDimensionValueListOrder();
		List<RoleModule> ReportRoots = new ArrayList<RoleModule>();
		RoleModule ReportModule = new RoleModule();
		ReportModule.setText("维度树");
		ReportModule.setChildren(createModleList(orignial));
		ReportRoots.add(ReportModule);
		return ReportRoots;
	}
	
	/**
	 * 根据查询出的树结构生成TreeModuleBo对象
	 * @param hashList
	 * @param parentTreeModule
	 * @return
	 */
	private List<RoleModule> createModleList(List<TbDimensionValue> hashList){
		List<RoleModule> ReportRoots = new ArrayList<RoleModule>();
	
		for(int i=0;i<hashList.size();i++){
			
			if(i==0|| !hashList.get(i).getDimensionId().equals(hashList.get(i-1).getDimensionId())){
				
				RoleModule parentModule =	toParentModuleBo(hashList.get(i));
				RoleModule childrenModule =toChildrenModuleBo(hashList.get(i));
				parentModule.getChildren().add(childrenModule);
				ReportRoots.add(parentModule);
				
			}else {
				RoleModule childrenModule =toChildrenModuleBo(hashList.get(i));
				ReportRoots.get(ReportRoots.size()-1).getChildren().add(childrenModule);	
				
				}
			}
		for(RoleModule i: ReportRoots){
			System.out.println("数列："+i);
		}
		
		return ReportRoots;
	
	}
	/**
	 * 将Reports对象转换为TreeModuleBo对象
	 * @param Reports
	 * @return
	 */
	private RoleModule toParentModuleBo(TbDimensionValue Report){
		RoleModule ReportModule = new RoleModule();
		List<RoleModule> list = new ArrayList<RoleModule>();
		ReportModule.setId(Report.getDimensionId());
		ReportModule.setText(Report.getDimensionDesc());
		ReportModule.setChildren(list);
		ReportModule.setPid("0");
		return ReportModule;
	}
	
	private RoleModule toChildrenModuleBo(TbDimensionValue Report){
		RoleModule ReportModule = new RoleModule();
		
		ReportModule.setId(Report.getDbId());
		ReportModule.setText(Report.getDimensionValue());
		
		ReportModule.setPid(Report.getDimensionId());
		return ReportModule;
	}

}
