package com.singlee.hrbcap.service;

import com.github.pagehelper.Page;
import com.singlee.hrbcap.model.acup.TbEntry;
import com.singlee.hrbcap.model.acup.TbSubjectBalanceSum;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;


/**
 * 记账余额的服务接口
 * @author shiting
 *
 */
public interface TbSubjectBalanceSumService {
	
	/**
	 * 根据分录查找相应的余额，如查询不到，则创建新的并返回
	 * @param entry
	 */
	public TbSubjectBalanceSum findOrCreateByEntry(TbEntry entry);
	
	void insert(TbSubjectBalanceSum balance);
	/**
	 * 更新余额，只更新四个数字余额（借、贷、支、付）
	 * @param balance
	 */
	public void updateBalance(Map<String, Object> map);
	
	public Page<TbSubjectBalanceSum> selectByMap(Map<String,Object> map,RowBounds rb);

	List<TbSubjectBalanceSum> selectByMap(Map<String, Object> map);

	/**
	 * 更新上日科目余额
	 * @param balance
	 */
	public void updateBeforeBalance();
	public List<TbSubjectBalanceSum> getAllDiffListService(Map<String,Object> map);

}
