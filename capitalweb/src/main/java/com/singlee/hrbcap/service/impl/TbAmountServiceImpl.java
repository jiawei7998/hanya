
package com.singlee.hrbcap.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbcap.mapper.TbAmountMapper;
import com.singlee.hrbcap.model.base.TbAmount;
import com.singlee.hrbcap.service.TbAmountService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service("tbAmountService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TbAmountServiceImpl implements TbAmountService {
	@Autowired
	private TbAmountMapper tbAmountMapper;

	@Override
	public PageInfo<TbAmount> getTbAmountList(Map<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<TbAmount> page = tbAmountMapper.getTbAmountList(map, rowBounds);
		return new PageInfo<TbAmount>(page);
	}

	@Override
	public TbAmount getAmountById(String amountId) {
		return tbAmountMapper.selectByPrimaryKey(amountId);//
	}
	@Override
	public List<TbAmount> getAllTbAmount() {
		return tbAmountMapper.selectAll();//
	}
	@Override
	public void createAmount(TbAmount tbkAmount) {
		if (tbAmountMapper.checkTbAmountById(tbkAmount.getAmountId()) == 0) {
			tbAmountMapper.insert(tbkAmount);
		} else {
			JY.raiseRException("金额代码已经存在");
		}

	}

	@Override
	public void updateAmount(TbAmount tbkAmount) {
		if (tbAmountMapper.checkTbAmountById(tbkAmount.getAmountId()) == 0) {
			JY.raiseRException("金额代码不存在");
		} else {
			tbAmountMapper.updateByPrimaryKey(tbkAmount);
		}
	}

	@Override
	public void deleteAmount(String amountId) {
		tbAmountMapper.deleteByPrimaryKey(amountId);
	}

}
