package com.singlee.hrbcap.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.system.service.InstitutionService;
import com.singlee.hrbcap.mapper.TbSubjectBalanceSumMapper;
import com.singlee.hrbcap.model.acup.TbEntry;
import com.singlee.hrbcap.model.acup.TbSubjectBalanceSum;
import com.singlee.hrbcap.service.TbSubjectBalanceSumService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 记账余额的服务类
 * @author shiting
 *
 */
@Service("tbSubjectBalanceSumService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TbSubjectBalanceServiceSumImpl implements TbSubjectBalanceSumService {

	@Autowired
	private TbSubjectBalanceSumMapper tbBalanceSumMapper;
	@Autowired
    DayendDateService dateService;
	@Autowired
    InstitutionService institutionService;
	
	/**
	 * 根据分录查找相应的余额，如查询不到，则创建新的并返回
	 * @param taskId
	 * @param date
	 * @param entry
	 */
	@Override
	public TbSubjectBalanceSum findOrCreateByEntry(TbEntry entry){
		TbSubjectBalanceSum balance = new TbSubjectBalanceSum();
		balance.setTaskId(entry.getTaskId());
		balance.setSubjCode(entry.getSubjCode());
		balance.setCcy(entry.getCcy());
		balance.setBkkpgOrgId(entry.getBkkpgOrgId());
		List<TbSubjectBalanceSum> balanceList = tbBalanceSumMapper.selectByMap(BeanUtil.beanToHashMap(balance),new RowBounds());
		if(balanceList != null && balanceList.size()>0){
			return balanceList.get(0);
		}
		
		balance.setUpdateTime(DateUtil.getCurrentDateTimeAsString());
		balance.setSubjName(entry.getSubjName());
		tbBalanceSumMapper.insert(balance);
		
		return balance;
		
	}
	
	/**
	 * 更新余额，只更新四个数字余额（借、贷、支、付）
	 * @param balance
	 */
	@Override
	public void updateBalance(Map<String, Object> map){
		tbBalanceSumMapper.updateBalance(map);
	}

	@Override
	public Page<TbSubjectBalanceSum> selectByMap(Map<String, Object> map,
			RowBounds rb) {
		return tbBalanceSumMapper.selectByMap(map, rb);
	}
	
	@Override
	public List<TbSubjectBalanceSum> selectByMap(Map<String, Object> map) {
		return tbBalanceSumMapper.selectSumList(map);
	}

	@Override
	public void insert(TbSubjectBalanceSum balance) {
		tbBalanceSumMapper.insert(balance);
	}


	@Override
	public List<TbSubjectBalanceSum> getAllDiffListService(
			Map<String, Object> map) {
		return tbBalanceSumMapper.getAllDiffList(map);
	}


	@Override
	public void updateBeforeBalance() {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("postDate", dateService.getDayendDate());
		tbBalanceSumMapper.copySubjectBalSum(map);
		tbBalanceSumMapper.updateBeginValue();
	}
}
