package com.singlee.hrbcap.service;


import com.singlee.hrbcap.model.base.SceneResultBean;
import com.singlee.hrbcap.model.base.TbProductAllSceneAcupView;
import com.singlee.hrbcap.model.base.TbProductEventSceneAcup;

import java.util.List;
import java.util.Map;

public interface TbProductEventSceneAcupService {


	/**
	 * 获取所有场景
	 * @param map
	 * @return
	 */
	public List<SceneResultBean> getSceneResultList();
	/**
	 * 获得产品所有会计场景
	 * @return
	 */
	public List<TbProductAllSceneAcupView>  getAllSceneResultList(Map<String, Object> map);
	/**
	 * 使用GroupId查询
	 * @param map
	 * @return
	 */
	public List<TbProductEventSceneAcup> getTbProductEventSceneAcupsByGroupId(Map<String, Object> map);
	

	
	/**
	 * 更新场景信息
	 * @param TbProductEventSceneAcup
	 * @return
	 */
	public void updateTbProductEventSceneAcup(TbProductEventSceneAcup TbProductEventSceneAcup);
	/**
	 * 新增场景科目信息
	 * @param TbProductEventSceneAcup
	 * @return
	 */
	public void insertTbProductEventSceneAcup(TbProductEventSceneAcup TbProductEventSceneAcup);
	/** 删除场景分录配置
	 * 
	 * @param TbProductEventSceneAcup
	 * @return
	 */
	public void deleteTbProductEventSceneAcup(TbProductEventSceneAcup TbProductEventSceneAcup);
	/**
	 * 科目-场景绑定
	 * 
	 * @param map
	 * @return
	 */
    public void bundSceneSubject(Map<String,Object> map);
	/**
	 * 科目-场景解除绑定
	 * 
	 * @param map
	 * @return
	 */
	public void untieSceneSubject(Map<String,Object> map);
	/**
	 * 查询已存在记录数
	 * 
	 * @param params
	 * @return
	 */
	public int getCount (TbProductEventSceneAcup TbProductEventSceneAcup);
}
