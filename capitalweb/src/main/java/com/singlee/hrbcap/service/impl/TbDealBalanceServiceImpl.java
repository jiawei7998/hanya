package com.singlee.hrbcap.service.impl;

import com.github.pagehelper.Page;
import com.singlee.cap.bookkeeping.model.QueryBalanceModel;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbcap.mapper.Ifrs9QueryBalanceMapper;
import com.singlee.hrbcap.mapper.TbDealBalanceMapper;
import com.singlee.hrbcap.model.acup.TbDealBalance;
import com.singlee.hrbcap.service.TbDealBalanceService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 记账余额的服务类
 * @author Libo
 *
 */
@Service("tbDealBalanceService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TbDealBalanceServiceImpl implements TbDealBalanceService {

	@Autowired
	private TbDealBalanceMapper dealBalanceMapper;
	
	@Autowired
	private Ifrs9QueryBalanceMapper queryBalanceMapper;
	
	/**
	 * 更新余额，只更新四个数字余额（借、贷、支、付）
	 * @param balance
	 */
	@Override
	public void updateBalance(TbDealBalance balance){
		dealBalanceMapper.updateBalance(BeanUtil.beanToMap(balance));
	}

	@Override
	public Page<TbDealBalance> selectByMap(Map<String, Object> map,
			RowBounds rb) {
		return dealBalanceMapper.selectByMap(map, rb);
	}
	
	@Override
	public List<TbDealBalance> selectByMap(Map<String, Object> map) {
		return dealBalanceMapper.selectByMap(map);
	}

	@Override
	public void insert(TbDealBalance balance) {
		dealBalanceMapper.insert(balance);
	}

	@Override
	public Page<QueryBalanceModel> queryFundBal(Map<String, Object> map) {
		return queryBalanceMapper.queryFundBal(map, ParameterUtil.getRowBounds(map));
	}

	@Override
	public Page<QueryBalanceModel> queryProductBal(Map<String, Object> map) {
		return queryBalanceMapper.queryProductBal(map, ParameterUtil.getRowBounds(map));
	}

	@Override
	public Page<TbDealBalance> queryEntrySum(Map<String, Object> map) {
		return dealBalanceMapper.selectByMap(map, ParameterUtil.getRowBounds(map));
	}

}
