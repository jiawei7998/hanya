package com.singlee.hrbcap.service;

import com.github.pagehelper.PageInfo;
import com.singlee.hrbcap.model.base.TbAmount;

import java.util.List;
import java.util.Map;

public interface TbAmountService {
	public PageInfo<TbAmount> getTbAmountList(Map<String, Object> map);

	public List<TbAmount> getAllTbAmount();
	
	public TbAmount getAmountById(String amountId);

	/**
	 * 添加金额代码
	 * 
	 * @param tbAmount
	 */
	public void createAmount(TbAmount tbAmount);

	/**
	 * 修改金额指针代码
	 * 
	 * @param TbAmount
	 */
	public void updateAmount(TbAmount tbAmount);

	/**
	 * 删除金额代码
	 * 
	 * @param amountId
	 */
	public void deleteAmount(String amountId);

}
