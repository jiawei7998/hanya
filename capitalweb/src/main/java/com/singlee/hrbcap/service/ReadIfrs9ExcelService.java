package com.singlee.hrbcap.service;

import jxl.Sheet;

import java.util.Map;

/**
 *  读取EXCEL会计配置表服务类
 * @author louhuanqing
 *
 */
public interface ReadIfrs9ExcelService {
	/**
	 *  根据每一页进行会计解析
	 * @param sheet（参考货币基金）
	 * @param productIdCol 产品代码列 0
	 * @param beginReadRow 读取数据列 2
	 * @param beginDimesionCol 维度起时列 4
	 * @param endDimesionCol 维度结束列 5
	 */
	void readIfrs9ExcelTransferToDataBaseService(Sheet sheet,int productIdCol,int beginReadRow,int beginDimesionCol ,int endDimesionCol);
	
	/**
	 * Excel的配置文件导入完成后，需要更新场景值组合表的ID
	 */
	void updateProductEventDimensionKvId(Map<String, Object> map);
	
	void deleteAllConfigByPrdNo(Map<String, Object> map);
	/**
	 * 清理规则基础表数据
	 */
	void deleteAllConfig(); 
	
	
}
