package com.singlee.hrbcap.service.impl;


import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbcap.mapper.TbDimensionMapper;
import com.singlee.hrbcap.model.base.TbDimension;
import com.singlee.hrbcap.service.TbDimensionService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

/*
 * 会计事件表serviceIMpl
 */
@Service("tbDimensionService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TbDimensionServiceImpl implements TbDimensionService {
	@Autowired
	private TbDimensionMapper tbDimensionMapper;
	
	@Override
	public PageInfo<TbDimension> getTbDimensionPage(Map<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<TbDimension> page = tbDimensionMapper.getTbDimensionPage(map, rowBounds);
		return new PageInfo<TbDimension>(page);
	}
	

	@Override
	public void createTbDimension(TbDimension TbDimension) {
		if (tbDimensionMapper.checkTbDimensionByDbId(TbDimension.getDbId()) == 0) {
			tbDimensionMapper.insert(TbDimension);
		} else {
			JY.raiseRException(("DBID已经存在"));
		}
		
	}

	@Override
	public void updateTbDimension(TbDimension TbDimension) {
		if (tbDimensionMapper.checkTbDimensionByDbId(TbDimension.getDbId()) == 0) {
			JY.raiseRException(("DBID不存在"));
		} else {
			tbDimensionMapper.updateByPrimaryKey(TbDimension);
		}
	}

	@Override
	public void deleteTbDimension(String DbId) {
		
		tbDimensionMapper.deleteByPrimaryKey(DbId);
	}

	


}
