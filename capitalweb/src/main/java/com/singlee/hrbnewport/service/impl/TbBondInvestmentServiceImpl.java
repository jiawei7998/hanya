package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbnewport.mapper.TbBondInvestmentMapper;
import com.singlee.hrbnewport.model.TbBondInvestment;
import com.singlee.hrbnewport.service.TbBondInvestmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TbBondInvestmentServiceImpl implements TbBondInvestmentService{

    @Autowired
    private TbBondInvestmentMapper tbBondInvestmentMapper;


    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String, Object> result = new HashMap<String, Object>();
        String queryDate = ParameterUtil.getString(map,"queryDate", DateUtil.getCurrentDateAsString());
        result.put("queryDate",queryDate);
        List<TbBondInvestment> list=tbBondInvestmentMapper.getBondInvestment();
        result.put("list",list);
        return result;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }

    @Override
    public List<TbBondInvestment> selectReport(Map<String, String> map) {
        tbBondInvestmentMapper.bondInvestmentCreate(map);
        return tbBondInvestmentMapper.getBondInvestment();
    }
}
