package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.hrbnewport.mapper.IfsReportCshbnwtzZbbbjtctqMapper;
import com.singlee.hrbnewport.model.IfsReportCshbnwtzZbbbjtctq;
import com.singlee.hrbnewport.service.IfsReportCshbnwtzZbbbjtctqReportService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author Administrator
 * <p>
 * 表六 表内外投资业务资本拨备计提情况表（穿透前）
 */
@Service
public class IfsReportCshbnwtzZbbbjtctqReportServiceImpl implements IfsReportCshbnwtzZbbbjtctqReportService {


    @Autowired
    private IfsReportCshbnwtzZbbbjtctqMapper ifsReportCshbnwtzZbbbjtctqMapper;

    private static Logger logger = Logger.getLogger(IfsReportCshbnwtzZbbbjtctqReportServiceImpl.class);


    @Override
    public Page<IfsReportCshbnwtzZbbbjtctq> searchIfsReportCshbnwtzZbbbjtctqPage(Map<String, String> map) {
        int pageNumber = ParameterUtil.getInt(map, "pageNumber", 10);
        int pageSize = ParameterUtil.getInt(map, "pageSize", 1);
        List<IfsReportCshbnwtzZbbbjtctq> ifsReportCshbnwtzZbbbjtctqList = ifsReportCshbnwtzZbbbjtctqMapper.selectAll();
        return HrbReportUtils.producePage(ifsReportCshbnwtzZbbbjtctqList, pageNumber, pageSize);
    }

    @Override
    public SlOutBean ifsReportCshbnwtzZbbbjtctqCreate(Map<String, String> map) {
        SlOutBean msg = new SlOutBean(RetStatusEnum.S, "error.common.0000", "【城商行表内外投资业务和同业交易对手情况表-表内外投资业务资本拨备计提情况表（穿透前）】生成成功！");
        ifsReportCshbnwtzZbbbjtctqMapper.ifsReportCshbnwtzZbbbjtctqCreate(map);
        String RETCODE = map.get("RETCODE");
        if (!"999".equals(RETCODE)) {
            logger.info("【城商行表内外投资业务和同业交易对手情况表-表内外投资业务资本拨备计提情况表（穿透前）】生成失败!");
            return new SlOutBean(RetStatusEnum.F, RETCODE, "【城商行表内外投资业务和同业交易对手情况表-表内外投资业务资本拨备计提情况表（穿透前）】生成失败!");
        }
        msg.setRetCode("999");
        msg.setRetMsg("【城商行表内外投资业务和同业交易对手情况表-表内外投资业务资本拨备计提情况表（穿透前）】生成成功！");
        logger.info("【城商行表内外投资业务和同业交易对手情况表-表内外投资业务资本拨备计提情况表（穿透前）】生成成功！");
        return msg;
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        String queryDate = ParameterUtil.getString(map,"queryDate","");
        queryDate = DateUtil.format(DateUtil.parse(queryDate), "yyyy年MM月dd日");
        List<IfsReportCshbnwtzZbbbjtctq> ifsReportCshbnwtzZbbbjtctqList = ifsReportCshbnwtzZbbbjtctqMapper.selectAll();
        Map<String, Object> rustmap = new HashMap<>();
        rustmap.put("list",ifsReportCshbnwtzZbbbjtctqList);
        rustmap.put("hz","");//汇总
        rustmap.put("sjsd",queryDate);//数据时点
        return rustmap;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}