package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.mapper.IfsReportTytzZfBtyccMapper;
import com.singlee.hrbnewport.model.IfsReportTytzZfBtycc;
import com.singlee.hrbnewport.service.IfsReportTytzZfBtyccService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 16:22
 * @description：${description}
 * @modified By：
 * @version:   
 */
@Service
public class IfsReportTytzZfBtyccServiceImpl implements IfsReportTytzZfBtyccService{

    @Resource
    private IfsReportTytzZfBtyccMapper ifsReportTytzZfBtyccMapper;

    @Override
    public int insert(IfsReportTytzZfBtycc record) {
        return ifsReportTytzZfBtyccMapper.insert(record);
    }

    @Override
    public int insertSelective(IfsReportTytzZfBtycc record) {
        return ifsReportTytzZfBtyccMapper.insertSelective(record);
    }

    @Override
    public List<IfsReportTytzZfBtycc> getDate(Map<String, Object> map) {
        ifsReportTytzZfBtyccMapper.call(map);
        return ifsReportTytzZfBtyccMapper.getDate(map);
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String,Object> mapEnd=new HashMap<>();
        List<IfsReportTytzZfBtycc> list=ifsReportTytzZfBtyccMapper.getDate(map);
        mapEnd.put("list",list);
        return mapEnd;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}
