package com.singlee.hrbnewport.service;

import com.singlee.hrbnewport.model.IfsReportTytzBtc;

import java.util.List;
import java.util.Map;

/**
 * @author ：chenguo
 * @date ：Created in 2021/12/3 15:27
 * @description：${description}
 * @modified 本币-同业存放
 * @version:
 */
public interface IfsReportTytzBtcService extends CommonExportService {

    List<IfsReportTytzBtc> getDate(Map<String,Object> map);
}

