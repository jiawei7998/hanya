package com.singlee.hrbnewport.service;

import com.singlee.hrbnewport.model.IfsReportZyzctfGbm;

import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/8 20:17
 * @description：${description}
 * @modified By：
 * @version:   
 */
public interface IfsReportZyzctfGbmService  extends CommonExportService{


    int insert(IfsReportZyzctfGbm record);

    int insertSelective(IfsReportZyzctfGbm record);


    List<Map<String,Object>> getDate(Map<String,Object> map);

}
