package com.singlee.hrbnewport.service;

import com.singlee.hrbnewport.model.IfsReportDfzfzu;

import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 20:52
 * @description：${description}
 * @modified By：
 * @version:   
 */
public interface IfsReportDfzfzuService extends CommonExportService{


    int insert(IfsReportDfzfzu record);

    int insertSelective(IfsReportDfzfzu record);

    List<IfsReportDfzfzu> getDate(Map<String,Object> map);

}
