package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbnewport.mapper.IfsReportG1402Mapper;
import com.singlee.hrbnewport.model.IfsReportCdPosit;
import com.singlee.hrbnewport.model.IfsReportG1402;
import com.singlee.hrbnewport.service.IfsReportG1402Service;
import javafx.util.Pair;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * G1402-大额风险暴露
 * 2021/11/25
 * @Auther:zk
 */
@Service
public class IfsReportG1402ServiceImpl implements IfsReportG1402Service {

    @Autowired
    IfsReportG1402Mapper ifsReportG1402Mapper;

    /******************************************报表查询方法***********************************************/
    /**
     * 生成数据
     * @param map
     */
    @Override
    public void creatG1402(Map<String, Object> map) {
        Object queryDate = map.get("queryDate");
        map.put("datadate", queryDate!=null&&!"".equals(queryDate.toString().trim()) ?
                queryDate.toString().trim() : DateUtil.getCurrentDateAsString());
        ifsReportG1402Mapper.createG1402Report(map);
    }

    /**
     * 分页查询
     * @param map
     * @return
     */
    @Override
    public Page<IfsReportG1402> searchIfsReportG1402(Map<String, Object> map) {
        RowBounds rowBounds = ParameterUtil.getRowBounds(map);
        return ifsReportG1402Mapper.searchAllPage(map,rowBounds);
    }

    /******************************************报表导出方法***********************************************/
    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        HashMap<String, Object> result = new HashMap<>();
        creatG1402(map);
        List<IfsReportG1402> ifsReportG1402s = searchIfsReportG1402List(map);
        result.put("list",ifsReportG1402s);
        return result;
    }

    private List<IfsReportG1402> searchIfsReportG1402List(Map<String, Object> map) {
        return ifsReportG1402Mapper.searchAll(map);
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}
