package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.hrbnewport.mapper.IfsReportS6301Mapper;
import com.singlee.hrbnewport.model.IfsReportS6301;
import com.singlee.hrbnewport.service.IfsReportS6301ReportService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author Administrator
 * <p>
 * S6301 大中小微型企业贷款情况表
 */
@Service
public class IfsReportS6301ReportServiceImpl implements IfsReportS6301ReportService {


    @Autowired
    private IfsReportS6301Mapper ifsReportS6301Mapper;

    private static Logger logger = Logger.getLogger(IfsReportS6301ReportServiceImpl.class);


    @Override
    public Page<IfsReportS6301> searchIfsReportS6301Page(Map<String, String> map) {
        int pageNumber = ParameterUtil.getInt(map, "pageNumber", 10);
        int pageSize = ParameterUtil.getInt(map, "pageSize", 1);
        List<IfsReportS6301> ifsReportS6301List = ifsReportS6301Mapper.selectAll();
        return HrbReportUtils.producePage(ifsReportS6301List, pageNumber, pageSize);
    }

    @Override
    public SlOutBean ifsReportS6301Create(Map<String, String> map) {
        SlOutBean msg = new SlOutBean(RetStatusEnum.S, "error.common.0000", "【S6301-大中小微型企业贷款情况表】生成成功！");
        ifsReportS6301Mapper.ifsReportS6301Create(map);
        String RETCODE = map.get("RETCODE");
        if (!"999".equals(RETCODE)) {
            logger.info("【S6301-大中小微型企业贷款情况表】生成失败!");
            return new SlOutBean(RetStatusEnum.F, RETCODE, "【S6301-大中小微型企业贷款情况表】生成失败!");
        }
        msg.setRetCode("999");
        msg.setRetMsg("【S6301-大中小微型企业贷款情况表】生成成功！");
        logger.info("【S6301-大中小微型企业贷款情况表】生成成功！");
        return msg;
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        String queryDate = ParameterUtil.getString(map,"queryDate","");
        queryDate = DateUtil.format(DateUtil.parse(queryDate), "yyyy年MM月dd日");
        List<IfsReportS6301> ifsReportS6301List = ifsReportS6301Mapper.selectAll();
        Map<String, Object> rustmap = new HashMap<>();
        rustmap.put("list",ifsReportS6301List);
        rustmap.put("bskj","境内汇总数据");
        rustmap.put("bbrq",queryDate);
        return rustmap;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}