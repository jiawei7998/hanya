package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbnewport.mapper.*;
import com.singlee.hrbnewport.model.*;
import com.singlee.hrbnewport.service.YjInnerbankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 【106号文-银监局】同业业务管理报告
 * 2021/12/8
 * @Auther:zk
 */
@Service
public class YjInnerbankServiceImpl implements YjInnerbankService {

    @Autowired
    YjInnerbankCdIssMapper yjInnerbankCdIssMapper;
    @Autowired
    YjInnerbankDepositMapper yjInnerbankDepositMapper;
    @Autowired
    YjInnerbankCdTposMapper yjInnerbankCdTposMapper;
    @Autowired
    YjInnerbankFundMapper yjInnerbankFundMapper;
    @Autowired
    YjInnerbankSecTposMapper yjInnerbankSecTposMapper;
    @Autowired
    YjInnerbankRvrepoMapper yjInnerbankRvrepoMapper;
    @Autowired
    YjInnerbankRepoMapper yjInnerbankRepoMapper;

    /**************************分页查询****************************************/
    @Override
    public Page<YjInnerbankRepo> searchRepoAllPage(Map<String, Object> param) {
        Object temDate = param.get("queryDate");
        String queryDate = temDate!=null&&!"".equals(temDate.toString().trim())?temDate.toString().trim(): DateUtil.getCurrentDateAsString();
        param.put("queryDate", queryDate);
        yjInnerbankRepoMapper.createData(param);
        Page<YjInnerbankRepo> yjInnerbankRepos = yjInnerbankRepoMapper.searchAllPage(param, ParameterUtil.getRowBounds(param));
        return yjInnerbankRepos;
    }

    @Override
    public Page<YjInnerbankRvrepo> searchRvrepoAllPage(Map<String, Object> param) {
        Object temDate = param.get("queryDate");
        String queryDate = temDate!=null&&!"".equals(temDate.toString().trim())?temDate.toString().trim(): DateUtil.getCurrentDateAsString();
        param.put("queryDate", queryDate);
        yjInnerbankRvrepoMapper.createData(param);
        Page<YjInnerbankRvrepo> yjInnerbankRvrepos = yjInnerbankRvrepoMapper.searchAllPage(param, ParameterUtil.getRowBounds(param));
        return yjInnerbankRvrepos;
    }

    @Override
    public Page<YjInnerbankSecTpos> searchSecTposAllPage(Map<String, Object> param) {
        Object temDate = param.get("queryDate");
        String queryDate = temDate!=null&&!"".equals(temDate.toString().trim())?temDate.toString().trim(): DateUtil.getCurrentDateAsString();
        param.put("queryDate", queryDate);
        yjInnerbankSecTposMapper.createData(param);
        Page<YjInnerbankSecTpos> yjInnerbankSecTpos = yjInnerbankSecTposMapper.searchAllPage(param, ParameterUtil.getRowBounds(param));
        return yjInnerbankSecTpos;
    }

    @Override
    public Page<YjInnerbankFund> searchFundAllPage(Map<String, Object> param) {
        Object temDate = param.get("queryDate");
        String queryDate = temDate!=null&&!"".equals(temDate.toString().trim())?temDate.toString().trim(): DateUtil.getCurrentDateAsString();
        param.put("queryDate", queryDate);
        yjInnerbankFundMapper.createData(param);
        Page<YjInnerbankFund> yjInnerbankFunds = yjInnerbankFundMapper.searchAllPage(param, ParameterUtil.getRowBounds(param));
        return yjInnerbankFunds;
    }

    @Override
    public Page<YjInnerbankCdTpos> searchCdTposAllPage(Map<String, Object> param) {
        Object temDate = param.get("queryDate");
        String queryDate = temDate!=null&&!"".equals(temDate.toString().trim())?temDate.toString().trim(): DateUtil.getCurrentDateAsString();
        param.put("queryDate", queryDate);
        yjInnerbankCdTposMapper.createData(param);
        Page<YjInnerbankCdTpos> yjInnerbankCdTpos = yjInnerbankCdTposMapper.searchAllPage(param, ParameterUtil.getRowBounds(param));
        return yjInnerbankCdTpos;
    }

    @Override
    public Page<YjInnerbankDeposit> searchDepositAllPage(Map<String, Object> param) {
        Object temDate = param.get("queryDate");
        String queryDate = temDate!=null&&!"".equals(temDate.toString().trim())?temDate.toString().trim(): DateUtil.getCurrentDateAsString();
        param.put("queryDate", queryDate);
        yjInnerbankDepositMapper.createData(param);
        Page<YjInnerbankDeposit> yjInnerbankDeposits = yjInnerbankDepositMapper.searchAllPage(param, ParameterUtil.getRowBounds(param));
        return yjInnerbankDeposits;
    }

    @Override
    public Page<YjInnerbankCdIss> searchCdIssAllPage(Map<String, Object> param) {
        Object temDate = param.get("queryDate");
        String queryDate = temDate!=null&&!"".equals(temDate.toString().trim())?temDate.toString().trim(): DateUtil.getCurrentDateAsString();
        param.put("queryDate", queryDate);
        yjInnerbankCdIssMapper.createData(param);
        Page<YjInnerbankCdIss> yjInnerbankCdIsses = yjInnerbankCdIssMapper.searchAllPage(param, ParameterUtil.getRowBounds(param));
        return yjInnerbankCdIsses;
    }


    /***************************报表导出****************************************/

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        HashMap<String, Object> result = new HashMap<>();
        String sheetType = ParameterUtil.getString(map, "sheetType", "FUND");
        if("FUND".equals(sheetType)){
            List<YjInnerbankFund> yjInnerbankFunds = searchFundAll(map);
            result.put("list",yjInnerbankFunds);
        }if("REPO".equals(sheetType)){
            List<YjInnerbankRepo> yjInnerbankRepos = searchRepoAll(map);
            result.put("list",yjInnerbankRepos);
        }if("REVREPO".equals(sheetType)){
            List<YjInnerbankRvrepo> yjInnerbankRvrepos = searchRvrepoAll(map);
            result.put("list",yjInnerbankRvrepos);
        }if("CDTPOS".equals(sheetType)){
            List<YjInnerbankCdTpos> yjInnerbankCdTpos = searchCdTposAll(map);
            result.put("list",yjInnerbankCdTpos);
        }if("SEC".equals(sheetType)){
            List<YjInnerbankSecTpos> yjInnerbankSecTpos = searchSecTposAll(map);
            result.put("list",yjInnerbankSecTpos);
        }if("CDISS".equals(sheetType)){
            List<YjInnerbankCdIss> yjInnerbankCdIsses = searchCdIssAll(map);
            result.put("list",yjInnerbankCdIsses);
        }if("CT".equals(sheetType)||"TC".equals(sheetType)||"CC".equals(sheetType)||"CR".equals(sheetType)){
            List<YjInnerbankDeposit> yjInnerbankDeposits = searchDepositAll(map);
            result.put("list",yjInnerbankDeposits);
        }
        String queryDate = ParameterUtil.getString(map,"queryDate", DateUtil.getCurrentDateAsString());
        result.put("year",queryDate.substring(0,4));
        result.put("month",queryDate.substring(5,7));
        return result;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }



    private List<YjInnerbankRepo> searchRepoAll(Map<String, Object> param) {
        Object temDate = param.get("queryDate");
        String queryDate = temDate!=null&&!"".equals(temDate.toString().trim())?temDate.toString().trim(): DateUtil.getCurrentDateAsString();
        param.put("queryDate", queryDate);
        yjInnerbankRepoMapper.createData(param);
        List<YjInnerbankRepo> yjInnerbankRepos = yjInnerbankRepoMapper.searchAll(param);
        return yjInnerbankRepos;
    }

    private List<YjInnerbankRvrepo> searchRvrepoAll(Map<String, Object> param) {
        Object temDate = param.get("queryDate");
        String queryDate = temDate!=null&&!"".equals(temDate.toString().trim())?temDate.toString().trim(): DateUtil.getCurrentDateAsString();
        param.put("queryDate", queryDate);
        yjInnerbankRvrepoMapper.createData(param);
        List<YjInnerbankRvrepo> yjInnerbankRvrepos = yjInnerbankRvrepoMapper.searchAll(param);
        return yjInnerbankRvrepos;
    }

    private List<YjInnerbankSecTpos> searchSecTposAll(Map<String, Object> param) {
        Object temDate = param.get("queryDate");
        String queryDate = temDate!=null&&!"".equals(temDate.toString().trim())?temDate.toString().trim(): DateUtil.getCurrentDateAsString();
        param.put("queryDate", queryDate);
        yjInnerbankSecTposMapper.createData(param);
        List<YjInnerbankSecTpos> yjInnerbankSecTpos = yjInnerbankSecTposMapper.searchAll(param);
        return yjInnerbankSecTpos;
    }

    private List<YjInnerbankFund> searchFundAll(Map<String, Object> param) {
        Object temDate = param.get("queryDate");
        String queryDate = temDate!=null&&!"".equals(temDate.toString().trim())?temDate.toString().trim(): DateUtil.getCurrentDateAsString();
        param.put("queryDate", queryDate);
        yjInnerbankFundMapper.createData(param);
        List<YjInnerbankFund> yjInnerbankFunds = yjInnerbankFundMapper.searchAll(param);
        return yjInnerbankFunds;
    }

    private List<YjInnerbankCdTpos> searchCdTposAll(Map<String, Object> param) {
        Object temDate = param.get("queryDate");
        String queryDate = temDate!=null&&!"".equals(temDate.toString().trim())?temDate.toString().trim(): DateUtil.getCurrentDateAsString();
        param.put("queryDate", queryDate);
        yjInnerbankCdTposMapper.createData(param);
        List<YjInnerbankCdTpos> yjInnerbankCdTpos = yjInnerbankCdTposMapper.searchAll(param);
        return yjInnerbankCdTpos;
    }

    private List<YjInnerbankDeposit> searchDepositAll(Map<String, Object> param) {
        Object temDate = param.get("queryDate");
        String queryDate = temDate!=null&&!"".equals(temDate.toString().trim())?temDate.toString().trim(): DateUtil.getCurrentDateAsString();
        param.put("queryDate", queryDate);
        yjInnerbankDepositMapper.createData(param);
        List<YjInnerbankDeposit> yjInnerbankDeposits = yjInnerbankDepositMapper.searchAll(param);
        return yjInnerbankDeposits;
    }

    private List<YjInnerbankCdIss> searchCdIssAll(Map<String, Object> param) {
        Object temDate = param.get("queryDate");
        String queryDate = temDate!=null&&!"".equals(temDate.toString().trim())?temDate.toString().trim(): DateUtil.getCurrentDateAsString();
        param.put("queryDate", queryDate);
        yjInnerbankCdIssMapper.createData(param);
        List<YjInnerbankCdIss> yjInnerbankCdIsses = yjInnerbankCdIssMapper.searchAll(param);
        return yjInnerbankCdIsses;
    }

}
