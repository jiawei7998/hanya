package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.model.TbBondProfit;

import java.util.Map;

public interface TbBondProfitService extends CommonExportService{

    Page<TbBondProfit> selectReport(Map<String,String > map);

}
