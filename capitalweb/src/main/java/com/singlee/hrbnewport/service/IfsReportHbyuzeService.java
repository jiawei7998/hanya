package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.model.IfsReportHbyuze;

import java.util.Map;

public interface IfsReportHbyuzeService extends CommonExportService{


    Page<IfsReportHbyuze> selectReport(Map<String,String > map);

}
