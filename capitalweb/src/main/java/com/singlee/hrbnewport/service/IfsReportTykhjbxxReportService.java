package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportTykhjbxx;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;


/**
 * @author Administrator
 * <p>
 * 同业客户-基本信息表
 */
@Service
public interface IfsReportTykhjbxxReportService extends CommonExportService {

    Page<IfsReportTykhjbxx> searchIfsReportTykhjbxxPage(@RequestBody Map<String, String> map);

    SlOutBean ifsReportTykhjbxxCreate(Map<String, String> map);
}
 