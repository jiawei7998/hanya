package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.hrbnewport.mapper.IfsReportCkhyhtyhlhcffzJwjgckbMapper;
import com.singlee.hrbnewport.model.IfsReportCkhyhtyhlhcffzJwjgckb;
import com.singlee.hrbnewport.service.IfsReportCkhyhtyhlhcffzJwjgckbReportService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author Administrator
 * <p>
 * 存款含银行同业和联行存放负债 境外机构存款表
 */
@Service
public class IfsReportCkhyhtyhlhcffzJwjgckbReportServiceImpl implements IfsReportCkhyhtyhlhcffzJwjgckbReportService {


    @Autowired
    private IfsReportCkhyhtyhlhcffzJwjgckbMapper ifsReportCkhyhtyhlhcffzJwjgckbMapper;

    private static Logger logger = Logger.getLogger(IfsReportCkhyhtyhlhcffzJwjgckbReportServiceImpl.class);


    @Override
    public Page<IfsReportCkhyhtyhlhcffzJwjgckb> searchIfsReportCkhyhtyhlhcffzJwjgckbPage(Map<String, String> map) {
        int pageNumber = ParameterUtil.getInt(map, "pageNumber", 10);
        int pageSize = ParameterUtil.getInt(map, "pageSize", 1);
        List<IfsReportCkhyhtyhlhcffzJwjgckb> ifsReportCkhyhtyhlhcffzJwjgckbList = ifsReportCkhyhtyhlhcffzJwjgckbMapper.selectAll();
        return HrbReportUtils.producePage(ifsReportCkhyhtyhlhcffzJwjgckbList, pageNumber, pageSize);
    }

    @Override
    public SlOutBean ifsReportCkhyhtyhlhcffzJwjgckbCreate(Map<String, String> map) {
        SlOutBean msg = new SlOutBean(RetStatusEnum.S, "error.common.0000", "【存款含银行同业和联行存放负债 境外机构存款表】生成成功！");
        ifsReportCkhyhtyhlhcffzJwjgckbMapper.ifsReportCkhyhtyhlhcffzJwjgckbCreate(map);
        String RETCODE = map.get("RETCODE");
        if (!"999".equals(RETCODE)) {
            logger.info("【存款含银行同业和联行存放负债 境外机构存款表】生成失败!");
            return new SlOutBean(RetStatusEnum.F, RETCODE, "【存款含银行同业和联行存放负债 境外机构存款表】生成失败!");
        }
        msg.setRetCode("999");
        msg.setRetMsg("【存款含银行同业和联行存放负债 境外机构存款表】生成成功！");
        logger.info("【存款含银行同业和联行存放负债 境外机构存款表】生成成功！");
        return msg;
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        List<IfsReportCkhyhtyhlhcffzJwjgckb> ifsReportCkhyhtyhlhcffzJwjgckbList = ifsReportCkhyhtyhlhcffzJwjgckbMapper.selectAll();
        Map<String, Object> rustmap = new HashMap<>();
        rustmap.put("list",ifsReportCkhyhtyhlhcffzJwjgckbList);
        return rustmap;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}