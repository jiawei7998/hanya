package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.hrbnewport.mapper.IfsReportG3101Mapper;
import com.singlee.hrbnewport.model.IfsReportG3101;
import com.singlee.hrbnewport.model.IfsReportJrscbywglZqzy;
import com.singlee.hrbnewport.service.IfsReportG3101ReportService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author Administrator
 * <p>
 * G31投资业务情况表-底层资产投资情况
 */
@Service
public class IfsReportG3101ReportServiceImpl implements IfsReportG3101ReportService {


    @Autowired
    private IfsReportG3101Mapper ifsReportG3101Mapper;

    private static Logger logger = Logger.getLogger(IfsReportG3101ReportServiceImpl.class);


    @Override
    public Page<IfsReportG3101> searchIfsReportG3101Page(Map<String, String> map) {
        int pageNumber = ParameterUtil.getInt(map, "pageNumber", 10);
        int pageSize = ParameterUtil.getInt(map, "pageSize", 1);
        List<IfsReportG3101> ifsReportG3101List = ifsReportG3101Mapper.selectAll();
        for (IfsReportG3101 ifsReportG3101 : ifsReportG3101List) {
            String[] productList = ifsReportG3101.getProduct().split("_");
            if (productList.length > 0) {
                ifsReportG3101.setProduct(productList[0]);
            }
            if (productList.length > 1) {
                ifsReportG3101.setProducto(productList[1]);
            }
        }
        return HrbReportUtils.producePage(ifsReportG3101List, pageNumber, pageSize);
    }

    @Override
    public SlOutBean ifsReportG3101Create(Map<String, String> map) {
        SlOutBean msg = new SlOutBean(RetStatusEnum.S, "error.common.0000", "【G31-投资业务情况表-底层资产投资情况】生成成功！");
        ifsReportG3101Mapper.ifsReportG3101Create(map);
        String RETCODE = map.get("RETCODE");
        if (!"999".equals(RETCODE)) {
            logger.info("【G31-投资业务情况表-底层资产投资情况】生成失败!");
            return new SlOutBean(RetStatusEnum.F, RETCODE, "【G31-投资业务情况表-底层资产投资情况】生成失败!");
        }
        msg.setRetCode("999");
        msg.setRetMsg("【G31-投资业务情况表-底层资产投资情况】生成成功！");
        logger.info("【G31-投资业务情况表-底层资产投资情况】生成成功！");
        return msg;
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        String queryDate = ParameterUtil.getString(map,"queryDate","");
        queryDate = DateUtil.format(DateUtil.parse(queryDate), "yyyy年MM月dd日");
        List<IfsReportG3101> ifsReportG3101List = ifsReportG3101Mapper.selectByProductExcel();
        Map<String, Object> rustmap = new HashMap<>();
        rustmap.put("list",ifsReportG3101List);
        rustmap.put("tbjg","哈尔滨银行股份有限公司");
        rustmap.put("bbrq",queryDate);
        return rustmap;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}