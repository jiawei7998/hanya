package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportCshbnwtzTyjyds;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;


/**
 * @author Administrator
 * <p>
 * 表七 同业交易对手情况表
 */
@Service
public interface IfsReportCshbnwtzTyjydsReportService extends CommonExportService {

    Page<IfsReportCshbnwtzTyjyds> searchIfsReportCshbnwtzTyjydsPage(@RequestBody Map<String, String> map);

    SlOutBean ifsReportCshbnwtzTyjydsCreate(Map<String, String> map);
}
 