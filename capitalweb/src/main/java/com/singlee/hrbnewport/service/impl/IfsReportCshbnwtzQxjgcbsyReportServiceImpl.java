package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.hrbnewport.mapper.IfsReportCshbnwtzQxjgcbsyMapper;
import com.singlee.hrbnewport.model.IfsReportCshbnwtzQxjgcbsy;
import com.singlee.hrbnewport.service.IfsReportCshbnwtzQxjgcbsyReportService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author Administrator
 * <p>
 * 表四 表内外投资业务期限结构及成本收益表
 */
@Service
public class IfsReportCshbnwtzQxjgcbsyReportServiceImpl implements IfsReportCshbnwtzQxjgcbsyReportService {


    @Autowired
    private IfsReportCshbnwtzQxjgcbsyMapper ifsReportCshbnwtzQxjgcbsyMapper;

    private static Logger logger = Logger.getLogger(IfsReportCshbnwtzQxjgcbsyReportServiceImpl.class);


    @Override
    public Page<IfsReportCshbnwtzQxjgcbsy> searchIfsReportCshbnwtzQxjgcbsyPage(Map<String, String> map) {
        int pageNumber = ParameterUtil.getInt(map, "pageNumber", 10);
        int pageSize = ParameterUtil.getInt(map, "pageSize", 1);
        List<IfsReportCshbnwtzQxjgcbsy> ifsReportCshbnwtzQxjgcbsyList = ifsReportCshbnwtzQxjgcbsyMapper.selectAll();
        return HrbReportUtils.producePage(ifsReportCshbnwtzQxjgcbsyList, pageNumber, pageSize);
    }

    @Override
    public SlOutBean ifsReportCshbnwtzQxjgcbsyCreate(Map<String, String> map) {
        SlOutBean msg = new SlOutBean(RetStatusEnum.S, "error.common.0000", "【城商行表内外投资业务和同业交易对手情况表-表内外投资业务期限结构及成本收益表】生成成功！");
        ifsReportCshbnwtzQxjgcbsyMapper.ifsReportCshbnwtzQxjgcbsyCreate(map);
        String RETCODE = map.get("RETCODE");
        if (!"999".equals(RETCODE)) {
            logger.info("【城商行表内外投资业务和同业交易对手情况表-表内外投资业务期限结构及成本收益表】生成失败!");
            return new SlOutBean(RetStatusEnum.F, RETCODE, "【城商行表内外投资业务和同业交易对手情况表-表内外投资业务期限结构及成本收益表】生成失败!");
        }
        msg.setRetCode("999");
        msg.setRetMsg("【城商行表内外投资业务和同业交易对手情况表-表内外投资业务期限结构及成本收益表】生成成功！");
        logger.info("【城商行表内外投资业务和同业交易对手情况表-表内外投资业务期限结构及成本收益表】生成成功！");
        return msg;
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        String queryDate = ParameterUtil.getString(map,"queryDate","");
        queryDate = DateUtil.format(DateUtil.parse(queryDate), "yyyy年MM月dd日");
        List<IfsReportCshbnwtzQxjgcbsy> ifsReportCshbnwtzQxjgcbsyList = ifsReportCshbnwtzQxjgcbsyMapper.selectAll();
        Map<String, Object> rustmap = new HashMap<>();
        rustmap.put("list",ifsReportCshbnwtzQxjgcbsyList);
        rustmap.put("hz","");//汇总
        rustmap.put("sjsd",queryDate);//数据时点
        return rustmap;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}