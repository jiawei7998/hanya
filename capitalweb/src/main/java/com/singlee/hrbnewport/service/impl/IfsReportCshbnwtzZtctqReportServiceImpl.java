package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.hrbnewport.mapper.IfsReportCshbnwtzZtctqMapper;
import com.singlee.hrbnewport.model.IfsReportCshbnwtzZtctq;
import com.singlee.hrbnewport.service.IfsReportCshbnwtzZtctqReportService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author Administrator
 * <p>
 * 表一 表内外投资业务总体情况表（穿透前）
 */
@Service
public class IfsReportCshbnwtzZtctqReportServiceImpl implements IfsReportCshbnwtzZtctqReportService {


    @Autowired
    private IfsReportCshbnwtzZtctqMapper ifsReportCshbnwtzZtctqMapper;

    private static Logger logger = Logger.getLogger(IfsReportCshbnwtzZtctqReportServiceImpl.class);


    @Override
    public Page<IfsReportCshbnwtzZtctq> searchIfsReportCshbnwtzZtctqPage(Map<String, String> map) {
        int pageNumber = ParameterUtil.getInt(map, "pageNumber", 10);
        int pageSize = ParameterUtil.getInt(map, "pageSize", 1);
        List<IfsReportCshbnwtzZtctq> ifsReportCshbnwtzZtctqList = ifsReportCshbnwtzZtctqMapper.selectAll();
        for (IfsReportCshbnwtzZtctq ifsReportCshbnwtzZtctq : ifsReportCshbnwtzZtctqList) {
            String[] s = ifsReportCshbnwtzZtctq.getTztype().split("_");
            if (s.length > 0) {
                ifsReportCshbnwtzZtctq.setTztype(s[0]);
            }
            if (s.length > 1) {
                ifsReportCshbnwtzZtctq.setTztypeo(s[1]);
            }
        }
        return HrbReportUtils.producePage(ifsReportCshbnwtzZtctqList, pageNumber, pageSize);
    }

    @Override
    public SlOutBean ifsReportCshbnwtzZtctqCreate(Map<String, String> map) {
        SlOutBean msg = new SlOutBean(RetStatusEnum.S, "error.common.0000", "【城商行表内外投资业务和同业交易对手情况表-表内外投资业务总体情况表（穿透前）】生成成功！");
        ifsReportCshbnwtzZtctqMapper.ifsReportCshbnwtzZtctqCreate(map);
        String RETCODE = map.get("RETCODE");
        if (!"999".equals(RETCODE)) {
            logger.info("【城商行表内外投资业务和同业交易对手情况表-表内外投资业务总体情况表（穿透前）】生成失败!");
            return new SlOutBean(RetStatusEnum.F, RETCODE, "【城商行表内外投资业务和同业交易对手情况表-表内外投资业务总体情况表（穿透前）】生成失败!");
        }
        msg.setRetCode("999");
        msg.setRetMsg("【城商行表内外投资业务和同业交易对手情况表-表内外投资业务总体情况表（穿透前）】生成成功！");
        logger.info("【城商行表内外投资业务和同业交易对手情况表-表内外投资业务总体情况表（穿透前）】生成成功！");
        return msg;
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        String queryDate = ParameterUtil.getString(map,"queryDate","");
        queryDate = DateUtil.format(DateUtil.parse(queryDate), "yyyy年MM月dd日");
        List<IfsReportCshbnwtzZtctq> ifsReportCshbnwtzZtctqList = ifsReportCshbnwtzZtctqMapper.selectAll();
        Map<String, Object> rustmap = new HashMap<>();
        rustmap.put("list",ifsReportCshbnwtzZtctqList);
        rustmap.put("tbjg","哈尔滨银行股份有限公司");//填报单位
        rustmap.put("sjsd",queryDate);//数据时点
        return rustmap;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}