package com.singlee.hrbnewport.service;

import com.singlee.hrbnewport.model.IfsReportRatetdfx;

import java.util.List;
import java.util.Map;

public interface IfsReportRatetdfxService extends CommonExportService{

    List<IfsReportRatetdfx> selectReport(Map<String,String > map);

}
