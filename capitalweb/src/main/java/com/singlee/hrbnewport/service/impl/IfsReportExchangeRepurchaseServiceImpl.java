package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbnewport.mapper.IfsReportExchangeRepurchaseMapper;
import com.singlee.hrbnewport.service.IfsReportExchangeRepurchaseService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class IfsReportExchangeRepurchaseServiceImpl implements IfsReportExchangeRepurchaseService{

    @Resource
    private IfsReportExchangeRepurchaseMapper ifsReportExchangeRepurchaseMapper;


    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        String queryDate=map.get("queryDate").toString();
        Map<String, Object> result = new HashMap<String, Object>(16);
        ifsReportExchangeRepurchaseMapper.generateData(map);
        List<Map<String, Object>> mapList = ifsReportExchangeRepurchaseMapper.getExportPageData(map);
        result.put("list",mapList);
        result.put("instName","哈尔滨银行股份有限公司");
        result.put("year",queryDate.substring(0,4));
        result.put("month",queryDate.substring(5,7));
        result.put("day",queryDate.substring(8,10));
        return result;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        ifsReportExchangeRepurchaseMapper.generateData(map);
        return ifsReportExchangeRepurchaseMapper.getExportPageData(map, ParameterUtil.getRowBounds(map));
    }
}
