package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.model.IfsReportDepositInvestorReport;

import java.util.Map;

/**
 * 申购信息查询结果
 * 2021/12/6
 * @Auther:zk
 */
public interface IfsReportDepositInvestorReportService extends CommonExportService{

    Page<IfsReportDepositInvestorReport> selectAllReport(Map<String , Object> param);

    void createReport(Map<String , Object> param);

}
