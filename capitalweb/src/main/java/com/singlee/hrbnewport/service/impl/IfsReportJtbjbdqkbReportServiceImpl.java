package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.hrbnewport.mapper.IfsReportJtbjbdqkbMapper;
import com.singlee.hrbnewport.model.IfsReportJtbjbdqkb;
import com.singlee.hrbnewport.service.IfsReportJtbjbdqkbReportService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author Administrator
 * <p>
 * 计提本金变动情况报表
 */
@Service
public class IfsReportJtbjbdqkbReportServiceImpl implements IfsReportJtbjbdqkbReportService {


    @Autowired
    private IfsReportJtbjbdqkbMapper ifsReportJtbjbdqkbMapper;

    private static Logger logger = Logger.getLogger(IfsReportJtbjbdqkbReportServiceImpl.class);


    @Override
    public Page<IfsReportJtbjbdqkb> searchIfsReportJtbjbdqkbPage(Map<String, String> map) {
        int pageNumber = ParameterUtil.getInt(map, "pageNumber", 10);
        int pageSize = ParameterUtil.getInt(map, "pageSize", 1);
        List<IfsReportJtbjbdqkb> ifsReportJtbjbdqkbList = ifsReportJtbjbdqkbMapper.selectAll();
        return HrbReportUtils.producePage(ifsReportJtbjbdqkbList, pageNumber, pageSize);
    }

    @Override
    public SlOutBean ifsReportJtbjbdqkbCreate(Map<String, String> map) {
        SlOutBean msg = new SlOutBean(RetStatusEnum.S, "error.common.0000", "【计提本金变动情况报表】生成成功！");
        ifsReportJtbjbdqkbMapper.ifsReportJtbjbdqkbCreate(map);
        String RETCODE = map.get("RETCODE");
        if (!"999".equals(RETCODE)) {
            logger.info("【计提本金变动情况报表】生成失败!");
            return new SlOutBean(RetStatusEnum.F, RETCODE, "【计提本金变动情况报表】生成失败!");
        }
        msg.setRetCode("999");
        msg.setRetMsg("【计提本金变动情况报表】生成成功！");
        logger.info("【计提本金变动情况报表】生成成功！");
        return msg;
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        List<IfsReportJtbjbdqkb> ifsReportJtbjbdqkbList = ifsReportJtbjbdqkbMapper.selectAll();
        Map<String, Object> rustmap = new HashMap<>();
        rustmap.put("list",ifsReportJtbjbdqkbList);
        return rustmap;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}