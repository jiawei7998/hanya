package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbnewport.mapper.IfsReportCdPositionMapper;
import com.singlee.hrbnewport.model.IfsReportCdPosit;
import com.singlee.hrbnewport.model.IfsReportCdPosition;
import com.singlee.hrbnewport.service.IfsReportCdPositionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 同业存单
 * 2021/12/8
 * @Auther:zk
 */
@Service
public class IfsReportCdPositionServiceImpl implements IfsReportCdPositionService {

    @Autowired
    IfsReportCdPositionMapper ifsReportCdPositionMapper;

    /**********************页面查询******************************/
    @Override
    public Page<IfsReportCdPosition> searchAllPage(Map<String, Object> map) {
        Object temDate = map.get("queryDate");
        String queryDate = temDate!=null&&!"".equals(temDate.toString().trim())?temDate.toString().trim(): DateUtil.getCurrentDateAsString().substring(0,7);
        map.put("queryDate", queryDate);
        String[] split = queryDate.split("-");
        map.put("mon",split[1]);

        ifsReportCdPositionMapper.createData(map);
        Page<IfsReportCdPosition> ifsReportCdPositions = ifsReportCdPositionMapper.searchAllPage(map, ParameterUtil.getRowBounds(map));
        return ifsReportCdPositions;
    }
    /**********************报表导出******************************/
    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        HashMap<String, Object> result = new HashMap<>();
        List<IfsReportCdPosition> ifsReportCdPositions = searchAll(map);
        result.put("list",ifsReportCdPositions);
        return result;
    }

    private List<IfsReportCdPosition> searchAll(Map<String, Object> map) {
        Object temDate = map.get("queryDate");
        String queryDate = temDate!=null&&!"".equals(temDate.toString().trim())?temDate.toString().trim(): DateUtil.getCurrentDateAsString().substring(0,7);
        map.put("queryDate", queryDate);
        String[] split = queryDate.split("-");
        map.put("mon",split[1]);

        ifsReportCdPositionMapper.createData(map);
        List<IfsReportCdPosition> ifsReportCdPositions = ifsReportCdPositionMapper.searchAll(map);
        return ifsReportCdPositions;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}
