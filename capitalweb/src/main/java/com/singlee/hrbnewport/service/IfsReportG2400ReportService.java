package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportG2400;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;


/**
 * @author Administrator
 * <p>
 * G24最大百家金融机构同业融入情况表
 */
@Service
public interface IfsReportG2400ReportService extends CommonExportService {

    Page<IfsReportG2400> searchIfsReportG2400Page(@RequestBody Map<String, String> map);

    SlOutBean ifsReportG2400Create(Map<String, String> map);
}
 