package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.model.IfsReportImportantCheck;

import java.util.Map;

public interface IfsReportImportantCheckService extends CommonExportService{


    Page<IfsReportImportantCheck> selectReport(Map<String,String > map);

}
