package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.model.TbRmbDeal;

import java.util.Map;

public interface TbRmbDealService extends CommonExportService{

    Page<TbRmbDeal> selectReport(Map<String,String > map);


}
