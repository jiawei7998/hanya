package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportCshbnwtzZbbbjtctq;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;


/**
 * @author Administrator
 * <p>
 * 表六 表内外投资业务资本拨备计提情况表（穿透前）
 */
@Service
public interface IfsReportCshbnwtzZbbbjtctqReportService extends CommonExportService {

    Page<IfsReportCshbnwtzZbbbjtctq> searchIfsReportCshbnwtzZbbbjtctqPage(@RequestBody Map<String, String> map);

    SlOutBean ifsReportCshbnwtzZbbbjtctqCreate(Map<String, String> map);
}
 