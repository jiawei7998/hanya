package com.singlee.hrbnewport.service;

import com.singlee.hrbnewport.model.IfsReportTytzWtc;

import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/3 15:37
 * @description：${description}
 * @modified By：
 * @version:   
 */
public interface IfsReportTytzWtcService extends CommonExportService{


    int insert(IfsReportTytzWtc record);

    int insertSelective(IfsReportTytzWtc record);

    List<IfsReportTytzWtc> getDate(Map<String,Object> map);
}
