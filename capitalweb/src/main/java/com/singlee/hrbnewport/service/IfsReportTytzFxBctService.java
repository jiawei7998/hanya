package com.singlee.hrbnewport.service;

import com.singlee.hrbnewport.model.IfsReportTytzFxBct;

import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 16:21
 * @description：${description}
 * @modified By：
 * @version:   
 */
public interface IfsReportTytzFxBctService extends CommonExportService{


    int insert(IfsReportTytzFxBct record);

    int insertSelective(IfsReportTytzFxBct record);

    List<IfsReportTytzFxBct> getDate(Map<String,Object> map);

}
