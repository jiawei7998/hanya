package com.singlee.hrbnewport.service;

import com.singlee.hrbnewport.model.TbHoldBond;

import java.util.List;
import java.util.Map;

public interface TbHoldBondService extends CommonExportService{


    List<TbHoldBond> selectReport(Map<String,String > map);
}
