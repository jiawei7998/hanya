package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbnewport.mapper.*;
import com.singlee.hrbnewport.model.*;
import com.singlee.hrbnewport.service.IfsReportTytzFxBtcService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 16:21
 * @description：${description}
 * @modified By：
 * @version:   
 */
@Service
public class IfsReportTytzFxBtcServiceImpl implements IfsReportTytzFxBtcService{

    @Resource
    private IfsReportTytzFxBtcMapper ifsReportTytzFxBtcMapper;

    @Resource
    private IfsReportTytzFxBctMapper ifsReportTytzFxBctMapper;

    @Resource
    private IfsReportTytzFxBtyccMapper ifsReportTytzFxBtyccMapper;

    @Resource
    private IfsReportTytzFxBtycrMapper ifsReportTytzFxBtycrMapper;

    @Resource
    private IfsReportTytzFxWtcMapper ifsReportTytzFxWtcMapper;

    @Resource
    private IfsReportTytzFxWctMapper ifsReportTytzFxWctMapper;

    @Resource
    private IfsReportTytzFxWtyccMapper ifsReportTytzFxWtyccMapper;

    @Resource
    private IfsReportTytzFxWtycrMapper ifsReportTytzFxWtycrMapper;

    @Override
    public int insert(IfsReportTytzFxBtc record) {
        return ifsReportTytzFxBtcMapper.insert(record);
    }

    @Override
    public int insertSelective(IfsReportTytzFxBtc record) {
        return ifsReportTytzFxBtcMapper.insertSelective(record);
    }

    @Override
    public List<IfsReportTytzFxBtc> getDate(Map<String, Object> map) {
        ifsReportTytzFxBtcMapper.call(map);
        return ifsReportTytzFxBtcMapper.getDate(map);
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String,Object> mapEnd=new HashMap<>();
        String sheetType= ParameterUtil.getString(map,"sheetType","");
        if(sheetType.equals( "BTYCJ" ) ) {
            //本币-同业拆入
            List<IfsReportTytzFxBtycr> list=ifsReportTytzFxBtycrMapper.getDate(map);
            mapEnd.put("list",list);

        }
        if(sheetType.equals( "BTYCC" ) ) {
            //本币-同业拆出
            List<IfsReportTytzFxBtycc> list=ifsReportTytzFxBtyccMapper.getDate(map);
            mapEnd.put("list",list);
        }
        if(sheetType.equals( "WTYCF" ) ) {
            //外币-同业存放
            List<IfsReportTytzFxWtc> list=ifsReportTytzFxWtcMapper.getDate(map);
            mapEnd.put("list",list);
        }
        if(sheetType.equals( "WCFTY" ) ) {
            //外币-存放同业
            List<IfsReportTytzFxWct> list=ifsReportTytzFxWctMapper.getDate(map);
            mapEnd.put("list",list);
        }
        if(sheetType.equals( "WTYCR" ) ) {
            //外币-同业拆入
            List<IfsReportTytzFxWtycr> list=ifsReportTytzFxWtycrMapper.getDate(map);
            mapEnd.put("list",list);
        }
        if(sheetType.equals( "WCFTY2" ) ) {
            //外币-同业拆出

            List<IfsReportTytzFxWtycc> list=ifsReportTytzFxWtyccMapper.getDate(map);
            mapEnd.put("list",list);
        }

        if(sheetType.equals( "BTYCF" ) ) {
            //本币-同业存放

            List<IfsReportTytzFxBtc> list=ifsReportTytzFxBtcMapper.getDate(map);
            mapEnd.put("list",list);

        }
        if(sheetType.equals( "BCFTY" ) ) {
            //本币-存放同业

            List<IfsReportTytzFxBct> list=ifsReportTytzFxBctMapper.getDate(map);
            mapEnd.put("list",list);

        }

        return mapEnd;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}
