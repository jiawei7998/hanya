package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.model.IfsReportG14Detail;
import org.apache.ibatis.session.RowBounds;

import java.util.Map;

/**
 * 2021/12/7
 *
 * @Auther:zk
 */
public interface IfsReportG14DetailSrevice extends CommonExportService {

    Page<IfsReportG14Detail> searchAllPage(Map<String,Object> param);

}
