package com.singlee.hrbnewport.service;

import com.singlee.hrbnewport.model.IfsReportTytzWtycc;

import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/3 15:37
 * @description：${description}
 * @modified By：
 * @version:   
 */
public interface IfsReportTytzWtyccService extends CommonExportService{


    int insert(IfsReportTytzWtycc record);

    int insertSelective(IfsReportTytzWtycc record);

    List<IfsReportTytzWtycc> getDate(Map<String,Object> map);

}
