package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.hrbnewport.mapper.IfsReportEstateComMapper;
import com.singlee.hrbnewport.model.IfsReportEstateCom;
import com.singlee.hrbnewport.service.EstateComSecService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * S67_房地产融资风险检测表
 * @author zk
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class EstateComSecServiceImpl implements EstateComSecService {

    @Autowired
    IfsReportEstateComMapper ifsReportEstateComMapper;


    @Override
    public Page<IfsReportEstateCom> selectAllReport(Map<String , Object> param) {
        int pageNum = (int)param.get("pageNum");
        int pageSize = (int)param.get("pageSize");
        List<IfsReportEstateCom> ifsReportEstateComs = ifsReportEstateComMapper.selectAllReport();
        return HrbReportUtils.producePage(ifsReportEstateComs,pageNum,pageSize);
    }

    @Override
    public void createEstate(Map<String , Object> param) {

        Object queryDate = param.get("queryDate");
        param.put("queryDate", queryDate!=null&&!"".equals(queryDate.toString().trim()) ?
                queryDate.toString().trim() : DateUtil.getCurrentDateAsString());

        ifsReportEstateComMapper.creatReport(param);
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Object temDate = map.get("queryDate");
        String queryDate = temDate!=null&&!"".equals(temDate.toString().trim())?temDate.toString().trim(): DateUtil.getCurrentDateAsString();
        map.put("queryDate", queryDate);
        ifsReportEstateComMapper.creatReport(map);

        Map<String, Object> result = ifsReportEstateComMapper.selectResult();
        result.put("instName","哈尔滨银行股份有限公司");
        result.put("year",queryDate.substring(0,4));
        result.put("month",queryDate.substring(5,7));
        result.put("day",queryDate.substring(8,10));
        return result;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
    
}
