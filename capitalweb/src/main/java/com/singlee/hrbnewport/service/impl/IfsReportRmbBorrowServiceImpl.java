package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.mapper.IfsReportRmbBorrowMapper;
import com.singlee.hrbnewport.service.IfsReportRmbBorrowService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class IfsReportRmbBorrowServiceImpl implements IfsReportRmbBorrowService{

    @Resource
    private IfsReportRmbBorrowMapper ifsReportRmbBorrowMapper;


    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String, Object> result = new HashMap<String, Object>();
        ifsReportRmbBorrowMapper.generateData(map);
        List<Map<String, Object>> mapList = ifsReportRmbBorrowMapper.getData(map);
        for(int i =0;i<mapList.size();i++){
            result.put("map",mapList.get(i));
        }
        return result;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        ifsReportRmbBorrowMapper.generateData(map);
        return ifsReportRmbBorrowMapper.getData(map);
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}
