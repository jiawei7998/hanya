package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.model.DealdetailPubFund;
import org.apache.poi.ss.formula.functions.T;

import java.util.Map;

/**
 * 业务明细_人行
 * @author zk
 *
 */
public interface DealDetailService extends CommonExportService {

    Page<T> searchDetailPage(Map<String,Object> map);
}
