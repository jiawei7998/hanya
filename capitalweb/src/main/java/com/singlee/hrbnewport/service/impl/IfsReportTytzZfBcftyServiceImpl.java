package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.mapper.IfsReportTytzZfBcftyMapper;
import com.singlee.hrbnewport.model.IfsReportTytzZfBcfty;
import com.singlee.hrbnewport.service.IfsReportTytzZfBcftyService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 16:22
 * @description：${description}
 * @modified By：外币-存放同业
 * @version:   
 */
@Service
public class IfsReportTytzZfBcftyServiceImpl implements IfsReportTytzZfBcftyService{

    @Resource
    private IfsReportTytzZfBcftyMapper ifsReportTytzZfBcftyMapper;

    @Override
    public int insert(IfsReportTytzZfBcfty record) {
        return ifsReportTytzZfBcftyMapper.insert(record);
    }

    @Override
    public int insertSelective(IfsReportTytzZfBcfty record) {
        return ifsReportTytzZfBcftyMapper.insertSelective(record);
    }

    @Override
    public List<IfsReportTytzZfBcfty> getDate(Map<String, Object> map) {
        ifsReportTytzZfBcftyMapper.call(map);
        return ifsReportTytzZfBcftyMapper.getDate(map);
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String,Object> mapEnd=new HashMap<>();
        List<IfsReportTytzZfBcfty> list=ifsReportTytzZfBcftyMapper.getDate(map);
        mapEnd.put("list",list);
        return mapEnd;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}
