package com.singlee.hrbnewport.service.impl;


import com.github.pagehelper.Page;
import com.singlee.hrbnewport.mapper.PositionReportMapper;
import com.singlee.hrbnewport.service.PositionReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author Administrator
 *哈尔滨银行资金头寸日报表
 */
@Service
public class PositionReportServiceImpl implements PositionReportService {

	
	@Autowired
	private PositionReportMapper positionReportMapper;


    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {

        String queryDate=map.get("queryDate").toString();
        Map<String, Object> result = new HashMap<String, Object>(16);
        positionReportMapper.generateExportData(map);
        List<Map<String, Object>> mapList = positionReportMapper.getExportData(map);
        for (int i = 0; i < mapList.size(); i++) {
            result.put("map"+(i+1),mapList.get(i));
        }
        result.put("instName","哈尔滨银行股份有限公司");
        result.put("year",queryDate.substring(0,4));
        result.put("month",queryDate.substring(5,7));
        result.put("day",queryDate.substring(8,10));
        return result;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        positionReportMapper.generateExportData(map);
        return positionReportMapper.getExportData(map);
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }


}
