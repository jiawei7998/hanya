package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.model.IfsReportHbzsy;

import java.util.Map;

public interface IfsReportHbzsyService extends CommonExportService{


    Page<IfsReportHbzsy> selectReport(Map<String,String > map);

}
