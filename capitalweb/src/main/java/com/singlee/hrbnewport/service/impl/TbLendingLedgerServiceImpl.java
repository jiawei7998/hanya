package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbnewport.mapper.TbLendingLedgerMapper;
import com.singlee.hrbnewport.model.TbLendingLedger;
import com.singlee.hrbnewport.service.TbLendingLedgerService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TbLendingLedgerServiceImpl implements TbLendingLedgerService{

    @Autowired
    private TbLendingLedgerMapper tbLendingLedgerMapper;

    @Override
    public Page<TbLendingLedger> selectReport(Map<String, String> map) {
        RowBounds rb= ParameterUtil.getRowBounds(map);
        tbLendingLedgerMapper.lendingReportCreate(map);
        return tbLendingLedgerMapper.getLending(rb);
    }


    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String, Object> result = new HashMap<String, Object>();
        String startDate = ParameterUtil.getString(map,"startDate", DateUtil.getCurrentDateAsString());
        String endDate = ParameterUtil.getString(map,"endDate", DateUtil.getCurrentDateAsString());
        result.put("startDate",startDate);
        result.put("endDate",endDate);
        List<TbLendingLedger> list=tbLendingLedgerMapper.getLending();
        result.put("list",list);
        return result;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }


}
