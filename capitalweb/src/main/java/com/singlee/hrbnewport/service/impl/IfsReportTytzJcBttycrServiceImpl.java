package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.mapper.IfsReportTytzJcBttycrMapper;
import com.singlee.hrbnewport.model.IfsReportTytzJcBttycr;
import com.singlee.hrbnewport.service.IfsReportTytzJcBttycrService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ：chenguo
 * @date ：Created in 2021/12/6 16:20
 * @description：${description}
 * @modified By：
 * @version:
 */
@Service
public class IfsReportTytzJcBttycrServiceImpl implements IfsReportTytzJcBttycrService {

    @Resource
    private IfsReportTytzJcBttycrMapper ifsReportTytzJcBttycrMapper;

    @Override
    public int insert(IfsReportTytzJcBttycr record) {
        return ifsReportTytzJcBttycrMapper.insert(record);
    }

    @Override
    public int insertSelective(IfsReportTytzJcBttycr record) {
        return ifsReportTytzJcBttycrMapper.insertSelective(record);
    }

    @Override
    public List<IfsReportTytzJcBttycr> getDate(Map<String, Object> map) {
        ifsReportTytzJcBttycrMapper.call(map);
        return ifsReportTytzJcBttycrMapper.getDate(map);
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String,Object> mapEnd=new HashMap<>();
        List<IfsReportTytzJcBttycr> list=ifsReportTytzJcBttycrMapper.getDate(map);
        mapEnd.put("list",list);
        return mapEnd;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}

