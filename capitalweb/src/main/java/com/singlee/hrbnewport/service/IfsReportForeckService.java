package com.singlee.hrbnewport.service;

import com.singlee.hrbnewport.model.IfsReportForeck;

import java.util.List;
import java.util.Map;

public interface IfsReportForeckService extends CommonExportService{

    List<IfsReportForeck> selectReport(Map<String,String > map);

}
