package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbnewport.mapper.IfsForexZyckMapper;
import com.singlee.hrbnewport.model.IfsForexZyck;
import com.singlee.hrbnewport.service.IfsForexZyckService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class IfsForexZyckServiceImpl implements IfsForexZyckService{

    @Autowired
    private IfsForexZyckMapper ifsForexZyckMapper;


    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String, Object> result = new HashMap<String, Object>();
        String edate = ParameterUtil.getString(map,"queryDate", DateUtil.getCurrentDateAsString());
        String sdate = ParameterUtil.getString(map,"sdate", DateUtil.getCurrentDateAsString());
        String unit = ParameterUtil.getString(map,"unit", "");
        String ccytype = ParameterUtil.getString(map,"ccytype", "");
        result.put("sdate",sdate);
        result.put("edate",edate);
        result.put("unit",unit);
        result.put("ccytype",ccytype);
        List<IfsForexZyck> list=ifsForexZyckMapper.getForexZyck();
        result.put("list",list);
        return result;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<IfsForexZyck> selectReport(Map<String, String> map) {
        RowBounds rb = ParameterUtil.getRowBounds(map);
        ifsForexZyckMapper.ReportForexZyckCreate(map);
        return ifsForexZyckMapper.getForexZyck(rb);
    }
}
