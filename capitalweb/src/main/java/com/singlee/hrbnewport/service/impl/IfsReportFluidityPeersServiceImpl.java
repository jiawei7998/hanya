package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.mapper.IfsReportFluidityPeersMapper;
import com.singlee.hrbnewport.service.IfsReportFluidityPeersService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class IfsReportFluidityPeersServiceImpl implements IfsReportFluidityPeersService{

    @Resource
    private IfsReportFluidityPeersMapper ifsReportFluidityPeersMapper;

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String, Object> result = new HashMap<String, Object>();
        String queryDate=map.get("queryDate").toString();
        ifsReportFluidityPeersMapper.generateData(map);
        List<Map<String, Object>> mapList = ifsReportFluidityPeersMapper.getData(map);
        for (int i = 0; i < mapList.size(); i++) {
            result.put("map"+(i+1),mapList.get(i));
        }
        result.put("instName","哈尔滨银行股份有限公司");
        result.put("year",queryDate.substring(0,4));
        result.put("month",queryDate.substring(5,7));
        result.put("day",queryDate.substring(8,10));
        return result;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        ifsReportFluidityPeersMapper.generateData(map);
        return ifsReportFluidityPeersMapper.getData(map);
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}
