package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.hrbnewport.mapper.IfsReportG2200Mapper;
import com.singlee.hrbnewport.model.IfsReportG2200;
import com.singlee.hrbnewport.service.IfsReportG2200ReportService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author Administrator
 * <p>
 * G22流动性比例监测表
 */
@Service
public class IfsReportG2200ReportServiceImpl implements IfsReportG2200ReportService {


    @Autowired
    private IfsReportG2200Mapper ifsReportG2200Mapper;

    private static Logger logger = Logger.getLogger(IfsReportG2200ReportServiceImpl.class);


    @Override
    public Page<IfsReportG2200> searchIfsReportG2200Page(Map<String, String> map) {
        int pageNumber = ParameterUtil.getInt(map, "pageNumber", 10);
        int pageSize = ParameterUtil.getInt(map, "pageSize", 1);
        List<IfsReportG2200> ifsReportG2200List = ifsReportG2200Mapper.selectAll();
        return HrbReportUtils.producePage(ifsReportG2200List, pageNumber, pageSize);
    }

    @Override
    public SlOutBean ifsReportG2200Create(Map<String, String> map) {
        SlOutBean msg = new SlOutBean(RetStatusEnum.S, "error.common.0000", "【G22-流动性比例监测表】生成成功！");
        ifsReportG2200Mapper.ifsReportG2200Create(map);
        String RETCODE = map.get("RETCODE");
        if (!"999".equals(RETCODE)) {
            logger.info("【G22-流动性比例监测表】生成失败!");
            return new SlOutBean(RetStatusEnum.F, RETCODE, "【G22-流动性比例监测表】生成失败!");
        }
        msg.setRetCode("999");
        msg.setRetMsg("【G22-流动性比例监测表】生成成功！");
        logger.info("【G22-流动性比例监测表】生成成功！");
        return msg;
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        String queryDate = ParameterUtil.getString(map,"queryDate","");
        queryDate = DateUtil.format(DateUtil.parse(queryDate), "yyyy年MM月dd日");
        List<IfsReportG2200> ifsReportG2200List = ifsReportG2200Mapper.selectByProductExcel();
        Map<String, Object> rustmap = new HashMap<>();
        rustmap.put("list",ifsReportG2200List);
        rustmap.put("tbjg","哈尔滨银行股份有限公司");
        rustmap.put("bbrq",queryDate);
        return rustmap;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}