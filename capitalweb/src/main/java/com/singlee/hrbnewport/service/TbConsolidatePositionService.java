package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.model.TbConsolidatePosition;

import java.util.Map;

public interface TbConsolidatePositionService extends CommonExportService{

    Page<TbConsolidatePosition> selectReport(Map<String,String > map);


}
