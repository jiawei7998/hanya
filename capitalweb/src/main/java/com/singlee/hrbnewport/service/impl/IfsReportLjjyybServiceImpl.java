package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbnewport.mapper.IfsReportLjjyybMapper;
import com.singlee.hrbnewport.service.IfsReportLjjyybService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class IfsReportLjjyybServiceImpl implements IfsReportLjjyybService{

    @Resource
    private IfsReportLjjyybMapper ifsReportLjjyybMapper;


    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String, Object> result = new HashMap<String, Object>();
        String queryDate = ParameterUtil.getString(map,"queryDate", DateUtil.getCurrentDateAsString());
        ifsReportLjjyybMapper.generateData(map);
        List<Map<String, Object>> mapList = ifsReportLjjyybMapper.getData(map);
        for(int i =0;i<mapList.size();i++){
            result.put("map"+(i+1),mapList.get(i));
        }
        result.put("instName","哈尔滨银行股份有限公司");
        result.put("year",queryDate.substring(0,4));
        result.put("month",queryDate.substring(5,7));
        result.put("day",queryDate.substring(8,10));
        return result;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        ifsReportLjjyybMapper.generateData(map);
        return ifsReportLjjyybMapper.getData(map);
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}
