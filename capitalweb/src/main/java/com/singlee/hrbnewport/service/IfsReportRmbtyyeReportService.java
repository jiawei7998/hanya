package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportRmbtyye;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;


/**
 * @author Administrator
 * <p>
 * 人民币同业余额
 */
@Service
public interface IfsReportRmbtyyeReportService extends CommonExportService {

    Page<IfsReportRmbtyye> searchIfsReportRmbtyyePage(@RequestBody Map<String, String> map);

    SlOutBean ifsReportRmbtyyeCreate(Map<String, String> map);
}
 