package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.hrbnewport.mapper.IfsReportDffrjrjgydjcbeeMapper;
import com.singlee.hrbnewport.model.IfsReportDffrjrjgydjcbee;
import com.singlee.hrbnewport.service.IfsReportDffrjrjgydjcbeeReportService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author Administrator
 * <p>
 * 地方法人金融机构月度监测表（表二）
 */
@Service
public class IfsReportDffrjrjgydjcbeeReportServiceImpl implements IfsReportDffrjrjgydjcbeeReportService {


    @Autowired
    private IfsReportDffrjrjgydjcbeeMapper ifsReportDffrjrjgydjcbeeMapper;

    private static Logger logger = Logger.getLogger(IfsReportDffrjrjgydjcbeeReportServiceImpl.class);


    @Override
    public Page<IfsReportDffrjrjgydjcbee> searchIfsReportDffrjrjgydjcbeePage(Map<String, String> map) {
        int pageNumber = ParameterUtil.getInt(map, "pageNumber", 10);
        int pageSize = ParameterUtil.getInt(map, "pageSize", 1);
        List<IfsReportDffrjrjgydjcbee> ifsReportDffrjrjgydjcbeeList = ifsReportDffrjrjgydjcbeeMapper.selectAll();
        return HrbReportUtils.producePage(ifsReportDffrjrjgydjcbeeList, pageNumber, pageSize);
    }

    @Override
    public SlOutBean ifsReportDffrjrjgydjcbeeCreate(Map<String, String> map) {
        SlOutBean msg = new SlOutBean(RetStatusEnum.S, "error.common.0000", "【地方法人金融机构月度监测表（表二）】生成成功！");
        ifsReportDffrjrjgydjcbeeMapper.ifsReportDffrjrjgydjcbeeCreate(map);
        String RETCODE = map.get("RETCODE");
        if (!"999".equals(RETCODE)) {
            logger.info("【地方法人金融机构月度监测表（表二）】生成失败!");
            return new SlOutBean(RetStatusEnum.F, RETCODE, "【地方法人金融机构月度监测表（表二）】生成失败!");
        }
        msg.setRetCode("999");
        msg.setRetMsg("【地方法人金融机构月度监测表（表二）】生成成功！");
        logger.info("【地方法人金融机构月度监测表（表二）】生成成功！");
        return msg;
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        String queryDate = ParameterUtil.getString(map,"queryDate","");
        queryDate = DateUtil.format(DateUtil.parse(queryDate), "yyyy年MM月dd日");
        List<IfsReportDffrjrjgydjcbee> ifsReportDffrjrjgydjcbeeList = ifsReportDffrjrjgydjcbeeMapper.selectAll();
        Map<String, Object> rustmap = new HashMap<>();
        rustmap.put("list",ifsReportDffrjrjgydjcbeeList);
        rustmap.put("tbdw","哈尔滨银行股份有限公司");
        rustmap.put("sjrq",queryDate);
        return rustmap;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}