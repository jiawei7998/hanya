package com.singlee.hrbnewport.service;

import com.singlee.hrbnewport.model.IfsReportTytzJcBtc;

import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 16:20
 * @description：${description}
 * @modified By：
 * @version:   
 */
public interface IfsReportTytzJcBtcService extends CommonExportService{


    int insert(IfsReportTytzJcBtc record);

    int insertSelective(IfsReportTytzJcBtc record);

    List<IfsReportTytzJcBtc> getDate(Map<String,Object> map);
}
