package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbnewport.mapper.IfsReportForezyjyxeMapper;
import com.singlee.hrbnewport.model.IfsReportForezyjyxe;
import com.singlee.hrbnewport.service.IfsReportForezyjyxeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class IfsReportForezyjyxeServiceImpl implements IfsReportForezyjyxeService{

    @Autowired
    private IfsReportForezyjyxeMapper ifsReportForezyjyxeMapper;


    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String, Object> result = new HashMap<String, Object>();
        String edate = ParameterUtil.getString(map,"queryDate", DateUtil.getCurrentDateAsString());
        String sdate = ParameterUtil.getString(map,"sdate", DateUtil.getCurrentDateAsString());
        String wd1 = ParameterUtil.getString(map,"wd1", "1000");
        String wd2 = ParameterUtil.getString(map,"wd2", "1500");
        String wd3 = ParameterUtil.getString(map,"wd3", "2000");
        result.put("edate",edate);
        result.put("sdate",sdate);
        result.put("wd1",wd1);
        result.put("wd2",wd2);
        result.put("wd3",wd3);
        List<IfsReportForezyjyxe> list=ifsReportForezyjyxeMapper.getForeZyjyxe();
        result.put("list",list);
        return result;

    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }

    @Override
    public List<IfsReportForezyjyxe> selectReport(Map<String, String> map) {
        ifsReportForezyjyxeMapper.reportForeckCreate(map);
        return ifsReportForezyjyxeMapper.getForeZyjyxe();
    }
}
