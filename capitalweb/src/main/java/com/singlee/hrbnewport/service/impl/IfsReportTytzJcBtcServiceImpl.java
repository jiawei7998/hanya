package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbnewport.mapper.*;
import com.singlee.hrbnewport.model.*;
import com.singlee.hrbnewport.service.IfsReportTytzJcBtcService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 16:20
 * @description：${description}
 * @modified By：
 * @version:   
 */
@Service
public class IfsReportTytzJcBtcServiceImpl implements IfsReportTytzJcBtcService{

    @Resource
    private IfsReportTytzJcBtcMapper ifsReportTytzJcBtcMapper;

    @Resource
    private IfsReportTytzJcBctMapper ifsReportTytzJcBctMapper;

    @Resource
    private IfsReportTytzJcBtyccMapper ifsReportTytzJcBtyccMapper;

    @Resource
    private IfsReportTytzJcBttycrMapper ifsReportTytzJcBtycrMapper;

    @Resource
    private IfsReportTytzJcWtcMapper ifsReportTytzJcWtcMapper;

    @Resource
    private IfsReportTytzJcWctMapper ifsReportTytzJcWctMapper;

    @Resource
    private IfsReportTytzJcWcftyMapper ifsReportTytzJcWtyccMapper;

    @Resource
    private IfsReportTytzJcWtycrMapper ifsReportTytzJcWtycrMapper;


    @Override
    public int insert(IfsReportTytzJcBtc record) {
        return ifsReportTytzJcBtcMapper.insert(record);
    }

    @Override
    public int insertSelective(IfsReportTytzJcBtc record) {
        return ifsReportTytzJcBtcMapper.insertSelective(record);
    }

    @Override
    public List<IfsReportTytzJcBtc> getDate(Map<String, Object> map) {
        ifsReportTytzJcBtcMapper.call(map);
        return ifsReportTytzJcBtcMapper.getDate(map);
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String,Object> mapEnd=new HashMap<>();
        String sheetType= ParameterUtil.getString(map,"sheetType","");
        if(sheetType.equals( "BTYCJ" ) ) {
            //本币-同业拆入
            List<IfsReportTytzJcBttycr> list=ifsReportTytzJcBtycrMapper.getDate(map);
            mapEnd.put("list",list);

        }
        if(sheetType.equals( "BTYCC" ) ) {
            //本币-同业拆出
            List<IfsReportTytzJcBtycc> list=ifsReportTytzJcBtyccMapper.getDate(map);
            mapEnd.put("list",list);
        }
        if(sheetType.equals( "WTYCF" ) ) {
            //外币-同业存放
            List<IfsReportTytzJcWtc> list=ifsReportTytzJcWtcMapper.getDate(map);
            mapEnd.put("list",list);
        }
        if(sheetType.equals( "WCFTY" ) ) {
            //外币-存放同业
            List<IfsReportTytzJcWct> list=ifsReportTytzJcWctMapper.getDate(map);
            mapEnd.put("list",list);
        }
        if(sheetType.equals( "WTYCR" ) ) {
            //外币-同业拆入
            List<IfsReportTytzJcWtycr> list=ifsReportTytzJcWtycrMapper.getDate(map);
            mapEnd.put("list",list);
        }
        if(sheetType.equals( "WCFTY2" ) ) {
            //外币-同业拆出

            List<IfsReportTytzJcWcfty> list=ifsReportTytzJcWtyccMapper.getDate(map);
            mapEnd.put("list",list);
        }

        if(sheetType.equals( "BTYCF" ) ) {
            //本币-同业存放

            List<IfsReportTytzJcBtc> list=ifsReportTytzJcBtcMapper.getDate(map);
            mapEnd.put("list",list);

        }
        if(sheetType.equals( "BCFTY" ) ) {
            //本币-存放同业

            List<IfsReportTytzJcBct> list=ifsReportTytzJcBctMapper.getDate(map);
            mapEnd.put("list",list);

        }

        return mapEnd;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}
