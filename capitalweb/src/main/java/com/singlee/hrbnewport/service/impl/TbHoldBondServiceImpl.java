package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbnewport.mapper.TbHoldBondMapper;
import com.singlee.hrbnewport.model.TbHoldBond;
import com.singlee.hrbnewport.service.TbHoldBondService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TbHoldBondServiceImpl implements TbHoldBondService{

    @Autowired
    private TbHoldBondMapper tbHoldBondMapper;

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String, Object> result = new HashMap<String, Object>();
        String queryDate = ParameterUtil.getString(map,"queryDate", DateUtil.getCurrentDateAsString());
        List<TbHoldBond> list=tbHoldBondMapper.getHoldBond();
        result.put("list",list);
        result.put("queryDate",queryDate);
        return result;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }

    @Override
    public List<TbHoldBond> selectReport(Map<String, String> map) {
        tbHoldBondMapper.holdBond2Create(map);
        return tbHoldBondMapper.getHoldBond();
    }
}
