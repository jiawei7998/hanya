package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.mapper.IfsReportTytzJcWctMapper;
import com.singlee.hrbnewport.model.IfsReportTytzJcWct;
import com.singlee.hrbnewport.service.IfsReportTytzJcWctService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 16:38
 * @description：${description}
 * @modified By：
 * @version:   
 */
@Service
public class IfsReportTytzJcWctServiceImpl implements IfsReportTytzJcWctService{

    @Resource
    private IfsReportTytzJcWctMapper ifsReportTytzJcWctMapper;

    @Override
    public int insert(IfsReportTytzJcWct record) {
        return ifsReportTytzJcWctMapper.insert(record);
    }

    @Override
    public int insertSelective(IfsReportTytzJcWct record) {
        return ifsReportTytzJcWctMapper.insertSelective(record);
    }

    @Override
    public List<IfsReportTytzJcWct> getDate(Map<String, Object> map) {
        ifsReportTytzJcWctMapper.call(map);
        return ifsReportTytzJcWctMapper.getDate(map);
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String,Object> mapEnd=new HashMap<>();
        List<IfsReportTytzJcWct> list=ifsReportTytzJcWctMapper.getDate(map);
        mapEnd.put("list",list);
        return mapEnd;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}
