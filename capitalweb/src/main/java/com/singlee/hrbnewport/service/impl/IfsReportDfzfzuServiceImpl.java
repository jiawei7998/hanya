package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.mapper.IfsReportDfzfzuMapper;
import com.singlee.hrbnewport.model.IfsReportDfzfzu;
import com.singlee.hrbnewport.service.IfsReportDfzfzuService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 20:52
 * @description：${description}
 * @modified By：
 * @version:   
 */
@Service
public class IfsReportDfzfzuServiceImpl implements IfsReportDfzfzuService{

    @Resource
    private IfsReportDfzfzuMapper ifsReportDfzfzuMapper;

    @Override
    public int insert(IfsReportDfzfzu record) {
        return ifsReportDfzfzuMapper.insert(record);
    }

    @Override
    public int insertSelective(IfsReportDfzfzu record) {
        return ifsReportDfzfzuMapper.insertSelective(record);
    }

    @Override
    public List<IfsReportDfzfzu> getDate(Map<String, Object> map) {
        ifsReportDfzfzuMapper.call(map);
        return ifsReportDfzfzuMapper.getDate(map);
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String,Object> mapEnd=new HashMap<>();
        List<IfsReportDfzfzu> list=ifsReportDfzfzuMapper.getDate(map);
        mapEnd.put("list",list);
        return mapEnd;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}
