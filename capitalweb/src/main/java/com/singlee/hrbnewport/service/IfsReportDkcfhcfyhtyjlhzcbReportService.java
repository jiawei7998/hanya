package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportDkcfhcfyhtyjlhzcb;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;


/**
 * @author Administrator
 * <p>
 * 贷款拆放含拆放银行同业及联行资产表
 */
@Service
public interface IfsReportDkcfhcfyhtyjlhzcbReportService extends CommonExportService {

    Page<IfsReportDkcfhcfyhtyjlhzcb> searchIfsReportDkcfhcfyhtyjlhzcbPage(@RequestBody Map<String, String> map);

    SlOutBean ifsReportDkcfhcfyhtyjlhzcbCreate(Map<String, String> map);
}
 