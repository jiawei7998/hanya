package com.singlee.hrbnewport.service;

import com.singlee.hrbnewport.model.IfsReportTytzFxWtycr;

import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 16:21
 * @description：${description}
 * @modified By：
 * @version:   
 */
public interface IfsReportTytzFxWtycrService extends CommonExportService{


    int insert(IfsReportTytzFxWtycr record);

    int insertSelective(IfsReportTytzFxWtycr record);


    List<IfsReportTytzFxWtycr> getDate(Map<String,Object> map);

}
