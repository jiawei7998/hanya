package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.model.RvRepoPladgeVO;

import java.util.Map;
/**
 * _买入返售质押
 * @author zk
 */
public interface RvRepoPladgeService extends CommonExportService {
    
//    public Page<RvRepoPladgeVO> searchRevRepoPage(Map<String , Object> param);
    
//    public List<RvRepoPladgeVO> searchRevRepoList(Map<String , Object> param);

    void createRevRepo(Map<String , Object> map);

    public Page<RvRepoPladgeVO> searchRevRepoPladgePage(Map<String , Object> param);
    
}
