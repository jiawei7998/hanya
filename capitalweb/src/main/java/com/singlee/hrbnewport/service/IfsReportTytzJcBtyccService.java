package com.singlee.hrbnewport.service;

import com.singlee.hrbnewport.model.IfsReportTytzJcBtycc;

import java.util.List;
import java.util.Map;

/**
 * @author ：chenguo
 * @date ：Created in 2021/12/6 16:20
 * @description：${description}
 * @modified By：
 * @version:
 */
public interface IfsReportTytzJcBtyccService  extends CommonExportService{


    int insert(IfsReportTytzJcBtycc record);

    int insertSelective(IfsReportTytzJcBtycc record);

    List<IfsReportTytzJcBtycc> getDate(Map<String,Object> map);

}

