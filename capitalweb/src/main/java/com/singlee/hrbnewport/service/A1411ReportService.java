package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.model.A1411ReportVO;

import java.util.Map;

public interface A1411ReportService extends CommonExportService {

    public Page<A1411ReportVO> selectA1411Report(Map<String,String > map);
}
