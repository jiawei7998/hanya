package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.model.TbRateSwap;

import java.util.Map;

public interface TbRateSwapService extends CommonExportService{

    Page<TbRateSwap> selectReport(Map<String,String > map);

}
