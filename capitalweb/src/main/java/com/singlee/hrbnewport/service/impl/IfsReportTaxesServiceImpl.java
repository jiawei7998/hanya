package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.mapper.IfsReportTaxesMapper;
import com.singlee.hrbnewport.service.IfsReportTaxesService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class IfsReportTaxesServiceImpl implements IfsReportTaxesService{

    @Resource
    private IfsReportTaxesMapper ifsReportTaxesMapper;


    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String, Object> result = new HashMap<String, Object>();
        ifsReportTaxesMapper.generateData(map);
        List<Map<String, Object>> mapList = ifsReportTaxesMapper.getData(map);
        for(int i =0;i<mapList.size();i++){
            result.put("map"+(i+1),mapList.get(i));
        }
        return result;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        ifsReportTaxesMapper.generateData(map);
        return ifsReportTaxesMapper.getData(map);
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}
