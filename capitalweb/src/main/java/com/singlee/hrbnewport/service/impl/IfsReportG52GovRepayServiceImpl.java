package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbnewport.mapper.IfsReportG52GovRepayMapper;
import com.singlee.hrbnewport.model.IfsReportG52GovRepay;
import com.singlee.hrbnewport.service.IfsReportG52GovRepayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * G52地方政府融资平台及支出责任债务持有情况统计表
 * 2021/12/8
 * @Auther:zk
 */
@Service
public class IfsReportG52GovRepayServiceImpl implements IfsReportG52GovRepayService {

    @Autowired
    IfsReportG52GovRepayMapper ifsReportG52GovRepayMapper;

    /****************************页面查询**************************************/
    @Override
    public Page<IfsReportG52GovRepay> searchAllPage(Map<String, Object> map) {
        Object temDate = map.get("queryDate");
        String queryDate = temDate!=null&&!"".equals(temDate.toString().trim())?temDate.toString().trim(): DateUtil.getCurrentDateAsString();
        map.put("queryDate", queryDate);
        ifsReportG52GovRepayMapper.createData(map);
        Page<IfsReportG52GovRepay> ifsReportG52GovRepays = ifsReportG52GovRepayMapper.searchAllPage(map, ParameterUtil.getRowBounds(map));
        return ifsReportG52GovRepays;
    }

    /****************************报表导出**************************************/
    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        HashMap<String, Object> result = new HashMap<>();
        Object temDate = map.get("queryDate");
        String queryDate = temDate!=null&&!"".equals(temDate.toString().trim())?temDate.toString().trim(): DateUtil.getCurrentDateAsString();
        map.put("queryDate", queryDate);
        ifsReportG52GovRepayMapper.createData(map);
        Page<IfsReportG52GovRepay> ifsReportG52GovRepays = ifsReportG52GovRepayMapper.searchAllPage(map, ParameterUtil.getRowBounds(map));
        List<IfsReportG52GovRepay> par = ifsReportG52GovRepays.getResult();
        for(int i =0;i<par.size();i++){
            IfsReportG52GovRepay ifsReportG52GovRepay = par.get(i);
            HashMap<String, Object> paramer = BeanUtil.beanToHashMap(ifsReportG52GovRepay);

            result.put("map"+ ParameterUtil.getString(paramer,"seq",""),paramer);
        }
        return result;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }

}
