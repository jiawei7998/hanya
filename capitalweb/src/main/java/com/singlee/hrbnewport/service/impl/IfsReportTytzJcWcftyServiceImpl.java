package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.mapper.IfsReportTytzJcWcftyMapper;
import com.singlee.hrbnewport.model.IfsReportTytzJcWcfty;
import com.singlee.hrbnewport.service.IfsReportTytzJcWcftyService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 16:38
 * @description：${description}
 * @modified By：
 * @version:   
 */
@Service
public class IfsReportTytzJcWcftyServiceImpl implements IfsReportTytzJcWcftyService{

    @Resource
    private IfsReportTytzJcWcftyMapper ifsReportTytzJcWcftyMapper;

    @Override
    public int insert(IfsReportTytzJcWcfty record) {
        return ifsReportTytzJcWcftyMapper.insert(record);
    }

    @Override
    public int insertSelective(IfsReportTytzJcWcfty record) {
        return ifsReportTytzJcWcftyMapper.insertSelective(record);
    }

    @Override
    public List<IfsReportTytzJcWcfty> getDate(Map<String, Object> map) {
        ifsReportTytzJcWcftyMapper.call(map);
        return ifsReportTytzJcWcftyMapper.getDate(map);
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String,Object> mapEnd=new HashMap<>();
        List<IfsReportTytzJcWcfty> list=ifsReportTytzJcWcftyMapper.getDate(map);
        mapEnd.put("list",list);
        return mapEnd;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}
