package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbnewport.mapper.A1411ReportMapper;
import com.singlee.hrbnewport.model.A1411ReportVO;
import com.singlee.hrbnewport.service.A1411ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Liang
 * @date 2021/11/23 9:06
 * =======================
 */
@Service
@Transactional(value = "transactionManager", rollbackFor = Exception.class)
public class A1411ReportServiceImpl implements A1411ReportService {

    @Autowired
    private A1411ReportMapper a1411ReportMapper;

    @Override
    public Page<A1411ReportVO> selectA1411Report(Map<String, String> map) {
        Page<A1411ReportVO> result = null;
        //调用存储过程生成数据
        a1411ReportMapper.A1411ReportCreate(map);
        if ("999".equals(map.get("RETCODE"))){
            JY.info( map.get("RETMSG"));
            result = a1411ReportMapper.getA1411Report();
        }else if ("100".equals(map.get("RETCODE"))){
            JY.info(map.get("RETMSG"));
        }else {
            JY.raise("生成数据失败，请检查再执行！");
        }
        return result;
    }



    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String, Object> result = new HashMap<String, Object>();
        String queryDate = ParameterUtil.getString(map,"queryDate", DateUtil.getCurrentDateAsString());
        List<A1411ReportVO> list = a1411ReportMapper.getA1411Report();
        for(int i =0;i<list.size();i++){
            result.put("list"+ list.get(i).getId(),list.get(i));
        }
        result.put("instName","哈尔滨银行股份有限公司");
        result.put("year",queryDate.substring(0,4));
        result.put("month",queryDate.substring(5,7));
//        result.put("day",queryDate.substring(8,10));
        return result;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }

}
