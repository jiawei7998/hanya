package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.hrbnewport.mapper.IfsReportWzqyMapper;
import com.singlee.hrbnewport.model.IfsReportWzqy;
import com.singlee.hrbnewport.service.IfsReportWzqyReportService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author Administrator
 * <p>
 * 外债签约信息报送表
 */
@Service
public class IfsReportWzqyReportServiceImpl implements IfsReportWzqyReportService {


    @Autowired
    private IfsReportWzqyMapper ifsReportWzqyMapper;

    private static Logger logger = Logger.getLogger(IfsReportWzqyReportServiceImpl.class);


    @Override
    public Page<IfsReportWzqy> searchIfsReportWzqyPage(Map<String, String> map) {
        int pageNumber = ParameterUtil.getInt(map, "pageNumber", 10);
        int pageSize = ParameterUtil.getInt(map, "pageSize", 1);
        List<IfsReportWzqy> ifsReportWzqyList = ifsReportWzqyMapper.selectAll();
        return HrbReportUtils.producePage(ifsReportWzqyList, pageNumber, pageSize);
    }

    @Override
    public SlOutBean ifsReportWzqyCreate(Map<String, String> map) {
        SlOutBean msg = new SlOutBean(RetStatusEnum.S, "error.common.0000", "【外债签约信息报送表】生成成功！");
        ifsReportWzqyMapper.ifsReportWzqyCreate(map);
        String RETCODE = map.get("RETCODE");
        if (!"999".equals(RETCODE)) {
            logger.info("【外债签约信息报送表】生成失败!");
            return new SlOutBean(RetStatusEnum.F, RETCODE, "【外债签约信息报送表】生成失败!");
        }
        msg.setRetCode("999");
        msg.setRetMsg("【外债签约信息报送表】生成成功！");
        logger.info("【外债签约信息报送表】生成成功！");
        return msg;
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        List<IfsReportWzqy> ifsReportWzqyList = ifsReportWzqyMapper.selectAll();
        Map<String, Object> rustmap = new HashMap<>();
        rustmap.put("list",ifsReportWzqyList);
        return rustmap;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}