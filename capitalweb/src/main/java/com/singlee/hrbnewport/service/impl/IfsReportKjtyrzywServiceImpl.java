package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.mapper.IfsReportKjtyrzywMapper;
import com.singlee.hrbnewport.model.IfsReportKjtyrzyw;
import com.singlee.hrbnewport.service.IfsReportKjtyrzywService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 20:50
 * @description：${description}
 * @modified By：
 * @version:   
 */
@Service
public class IfsReportKjtyrzywServiceImpl implements IfsReportKjtyrzywService{

    @Resource
    private IfsReportKjtyrzywMapper ifsReportKjtyrzywMapper;

    @Override
    public int insert(IfsReportKjtyrzyw record) {
        return ifsReportKjtyrzywMapper.insert(record);
    }

    @Override
    public int insertSelective(IfsReportKjtyrzyw record) {
        return ifsReportKjtyrzywMapper.insertSelective(record);
    }

    @Override
    public List<IfsReportKjtyrzyw> getDate(Map<String, Object> map) {
        List<IfsReportKjtyrzyw> list2=  ifsReportKjtyrzywMapper.getDate(map);
        if (list2==null||list2.size()==0){
            ifsReportKjtyrzywMapper.call(map);
            return ifsReportKjtyrzywMapper.getDate(map);
        }
        return  list2;
    }

    @Override
    public void upDate(IfsReportKjtyrzyw record) {
        ifsReportKjtyrzywMapper.updateByPrimaryKey(record);
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String,Object> mapEnd=new HashMap<>();
        List<IfsReportKjtyrzyw> list=ifsReportKjtyrzywMapper.getDate(map);
        mapEnd.put("list",list);
        return mapEnd;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}
