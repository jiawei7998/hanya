package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.hrbnewport.mapper.IfsReportTykhjbxxMapper;
import com.singlee.hrbnewport.model.IfsReportTykhjbxx;
import com.singlee.hrbnewport.service.IfsReportTykhjbxxReportService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author Administrator
 * <p>
 * 同业客户-基本信息表
 */
@Service
public class IfsReportTykhjbxxReportServiceImpl implements IfsReportTykhjbxxReportService {


    @Autowired
    private IfsReportTykhjbxxMapper ifsReportTykhjbxxMapper;

    private static Logger logger = Logger.getLogger(IfsReportTykhjbxxReportServiceImpl.class);


    @Override
    public Page<IfsReportTykhjbxx> searchIfsReportTykhjbxxPage(Map<String, String> map) {
        int pageNumber = ParameterUtil.getInt(map, "pageNumber", 10);
        int pageSize = ParameterUtil.getInt(map, "pageSize", 1);
        List<IfsReportTykhjbxx> ifsReportTykhjbxxList = ifsReportTykhjbxxMapper.selectAll();
        return HrbReportUtils.producePage(ifsReportTykhjbxxList, pageNumber, pageSize);
    }

    @Override
    public SlOutBean ifsReportTykhjbxxCreate(Map<String, String> map) {
        SlOutBean msg = new SlOutBean(RetStatusEnum.S, "error.common.0000", "【同业客户-基本信息表】生成成功！");
        ifsReportTykhjbxxMapper.ifsReportTykhjbxxCreate(map);
        String RETCODE = map.get("RETCODE");
        if (!"999".equals(RETCODE)) {
            logger.info("【同业客户-基本信息表】生成失败!");
            return new SlOutBean(RetStatusEnum.F, RETCODE, "【同业客户-基本信息表】生成失败!");
        }
        msg.setRetCode("999");
        msg.setRetMsg("【同业客户-基本信息表】生成成功！");
        logger.info("【同业客户-基本信息表】生成成功！");
        return msg;
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        List<IfsReportTykhjbxx> ifsReportTykhjbxxList = ifsReportTykhjbxxMapper.selectAll();
        Map<String, Object> rustmap = new HashMap<>();
        rustmap.put("list",ifsReportTykhjbxxList);
        return rustmap;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}