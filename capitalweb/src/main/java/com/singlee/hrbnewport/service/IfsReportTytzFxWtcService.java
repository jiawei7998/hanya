package com.singlee.hrbnewport.service;

import com.singlee.hrbnewport.model.IfsReportTytzFxWtc;

import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 16:21
 * @description：${description}
 * @modified By：
 * @version:   
 */
public interface IfsReportTytzFxWtcService extends CommonExportService{


    int insert(IfsReportTytzFxWtc record);

    int insertSelective(IfsReportTytzFxWtc record);
    List<IfsReportTytzFxWtc> getDate(Map<String,Object> map);

}
