package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportJrscbywglYwfzqx;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;


/**
 * @author Administrator
 * <p>
 * 金融市场业务负债期限结构表
 */
@Service
public interface IfsReportJrscbywglYwfzqxReportService extends CommonExportService {

    Page<IfsReportJrscbywglYwfzqx> searchIfsReportJrscbywglYwfzqxPage(@RequestBody Map<String, String> map);

    SlOutBean ifsReportJrscbywglYwfzqxCreate(Map<String, String> map);
}
 