package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbnewport.mapper.IfsReportDepositInvestorReportMapper;
import com.singlee.hrbnewport.model.IfsReportDepositInvestorReport;
import com.singlee.hrbnewport.service.IfsReportDepositInvestorReportService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 申购信息查询结果
 * 2021/12/6
 * @Auther:zk
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsReportDepositInvestorReportServiceImpl implements IfsReportDepositInvestorReportService {
    @Autowired
    IfsReportDepositInvestorReportMapper ifsReportDepositInvestorReportMapper;

    /************************************查询方法******************************************/
    @Override
    public Page<IfsReportDepositInvestorReport> selectAllReport(Map<String, Object> param) {
        RowBounds rowBounds = ParameterUtil.getRowBounds(param);
        Page<IfsReportDepositInvestorReport> ifsReportDepositInvestorReports = ifsReportDepositInvestorReportMapper.selectAllReport(param,rowBounds);
        return ifsReportDepositInvestorReports;
    }

    @Override
    public void createReport(Map<String, Object> param) {
        Object temDate = param.get("queryDate");
        String queryDate = temDate!=null&&!"".equals(temDate.toString().trim())?temDate.toString().trim(): DateUtil.getCurrentDateAsString();
        param.put("queryDate", queryDate);
        ifsReportDepositInvestorReportMapper.creatReport(param);
    }

    /************************************导出方法******************************************/
    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Object temDate = map.get("queryDate");
        String queryDate = temDate!=null&&!"".equals(temDate.toString().trim())?temDate.toString().trim(): DateUtil.getCurrentDateAsString();
        map.put("queryDate", queryDate);
        ifsReportDepositInvestorReportMapper.creatReport(map);

        HashMap<String, Object> stringObjectHashMap = new HashMap<>();
        List<IfsReportDepositInvestorReport> result = ifsReportDepositInvestorReportMapper.selectResult();
        stringObjectHashMap.put("results",result);
        return stringObjectHashMap;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}
