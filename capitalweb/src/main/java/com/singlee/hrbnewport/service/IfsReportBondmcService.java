package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.model.IfsReportBondmc;

import java.util.Map;

public interface IfsReportBondmcService extends CommonExportService{


    Page<IfsReportBondmc> selectReport(Map<String,String > map);

}
