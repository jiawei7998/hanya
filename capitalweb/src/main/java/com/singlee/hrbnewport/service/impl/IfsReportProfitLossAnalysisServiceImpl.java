package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbnewport.mapper.IfsReportProfitLossAnalysisMapper;
import com.singlee.hrbnewport.service.IfsReportProfitLossAnalysisService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class IfsReportProfitLossAnalysisServiceImpl implements IfsReportProfitLossAnalysisService{

    @Resource
    private IfsReportProfitLossAnalysisMapper ifsReportProfitLossAnalysisMapper;


    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String, Object> result = new HashMap<String, Object>();
        ifsReportProfitLossAnalysisMapper.generateData(map);
        List<Map<String, Object>> mapList = ifsReportProfitLossAnalysisMapper.getData(map);
        result.put("list",mapList);
        return result;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        ifsReportProfitLossAnalysisMapper.generateData(map);
        return ifsReportProfitLossAnalysisMapper.getData(map, ParameterUtil.getRowBounds(map));
    }
}
