package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;

import java.util.Map;

/**
 * 人行-同业业务管理报告
 * 2021/12/9
 * @Auther:zk
 */
public interface RhInnerbankService extends CommonExportService {
    public Page<Map<String, Object>> searchAllPage(Map<String, Object> map);
}
