package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.mapper.IfsReportKjjbsrxxMapper;
import com.singlee.hrbnewport.model.IfsReportKjjbsrxx;
import com.singlee.hrbnewport.service.IfsReportKjjbsrxxService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 20:50
 * @description：${description}
 * @modified By：
 * @version:   
 */
@Service
public class IfsReportKjjbsrxxServiceImpl implements IfsReportKjjbsrxxService{

    @Resource
    private IfsReportKjjbsrxxMapper ifsReportKjjbsrxxMapper;

    @Override
    public int insert(IfsReportKjjbsrxx record) {
        return ifsReportKjjbsrxxMapper.insert(record);
    }

    @Override
    public int insertSelective(IfsReportKjjbsrxx record) {
        return ifsReportKjjbsrxxMapper.insertSelective(record);
    }

    @Override
    public List<IfsReportKjjbsrxx> getDate(Map<String, Object> map) {
        ifsReportKjjbsrxxMapper.call(map);
        return ifsReportKjjbsrxxMapper.getDate(map);
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String,Object> mapEnd=new HashMap<>();
        List<IfsReportKjjbsrxx> list=ifsReportKjjbsrxxMapper.getDate(map);
        mapEnd.put("list",list);
        return mapEnd;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}
