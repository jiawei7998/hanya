package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.hrbnewport.mapper.IfsReportDkcfhcfyhtyjlhzcbMapper;
import com.singlee.hrbnewport.model.IfsReportDkcfhcfyhtyjlhzcb;
import com.singlee.hrbnewport.service.IfsReportDkcfhcfyhtyjlhzcbReportService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author Administrator
 * <p>
 * 贷款拆放含拆放银行同业及联行资产表
 */
@Service
public class IfsReportDkcfhcfyhtyjlhzcbReportServiceImpl implements IfsReportDkcfhcfyhtyjlhzcbReportService {


    @Autowired
    private IfsReportDkcfhcfyhtyjlhzcbMapper ifsReportDkcfhcfyhtyjlhzcbMapper;

    private static Logger logger = Logger.getLogger(IfsReportDkcfhcfyhtyjlhzcbReportServiceImpl.class);


    @Override
    public Page<IfsReportDkcfhcfyhtyjlhzcb> searchIfsReportDkcfhcfyhtyjlhzcbPage(Map<String, String> map) {
        int pageNumber = ParameterUtil.getInt(map, "pageNumber", 10);
        int pageSize = ParameterUtil.getInt(map, "pageSize", 1);
        List<IfsReportDkcfhcfyhtyjlhzcb> ifsReportDkcfhcfyhtyjlhzcbList = ifsReportDkcfhcfyhtyjlhzcbMapper.selectAll();
        return HrbReportUtils.producePage(ifsReportDkcfhcfyhtyjlhzcbList, pageNumber, pageSize);
    }

    @Override
    public SlOutBean ifsReportDkcfhcfyhtyjlhzcbCreate(Map<String, String> map) {
        SlOutBean msg = new SlOutBean(RetStatusEnum.S, "error.common.0000", "【贷款拆放含拆放银行同业及联行资产表】生成成功！");
        ifsReportDkcfhcfyhtyjlhzcbMapper.ifsReportDkcfhcfyhtyjlhzcbCreate(map);
        String RETCODE = map.get("RETCODE");
        if (!"999".equals(RETCODE)) {
            logger.info("【贷款拆放含拆放银行同业及联行资产表】生成失败!");
            return new SlOutBean(RetStatusEnum.F, RETCODE, "【贷款拆放含拆放银行同业及联行资产表】生成失败!");
        }
        msg.setRetCode("999");
        msg.setRetMsg("【贷款拆放含拆放银行同业及联行资产表】生成成功！");
        logger.info("【贷款拆放含拆放银行同业及联行资产表】生成成功！");
        return msg;
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        List<IfsReportDkcfhcfyhtyjlhzcb> ifsReportDkcfhcfyhtyjlhzcbList = ifsReportDkcfhcfyhtyjlhzcbMapper.selectAll();
        Map<String, Object> rustmap = new HashMap<>();
        rustmap.put("list",ifsReportDkcfhcfyhtyjlhzcbList);
        return rustmap;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}