package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbnewport.mapper.IfsReportG14DetailMapper;
import com.singlee.hrbnewport.model.IfsReportG1402;
import com.singlee.hrbnewport.model.IfsReportG14Detail;
import com.singlee.hrbnewport.service.IfsReportG14DetailSrevice;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 2021/12/7
 *
 * @Auther:zk
 */
@Service
public class IfsReportG14DetailServiceImpl implements IfsReportG14DetailSrevice{

    @Autowired
    IfsReportG14DetailMapper ifsReportG14DetailMapper;

    /***********************查询方法***************************/

    @Override
    public Page<IfsReportG14Detail> searchAllPage(Map<String, Object> param) {
        Object temDate = param.get("queryDate");
        String queryDate = temDate!=null&&!"".equals(temDate.toString().trim())?temDate.toString().trim(): DateUtil.getCurrentDateAsString();
        param.put("queryDate", queryDate);
        ifsReportG14DetailMapper.createData(param);
        return ifsReportG14DetailMapper.searchAllPage(param, ParameterUtil.getRowBounds(param));
    }

    /*********************导出报表*******************************/

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        HashMap<String, Object> result = new HashMap<>();
        List<IfsReportG14Detail> ifsReportG14Details = searchAll(map);
        result.put("list",ifsReportG14Details);
        return result;
    }

    private List<IfsReportG14Detail> searchAll(Map<String, Object> param) {
        Object temDate = param.get("queryDate");
        String queryDate = temDate!=null&&!"".equals(temDate.toString().trim())?temDate.toString().trim(): DateUtil.getCurrentDateAsString();
        param.put("queryDate", queryDate);
        ifsReportG14DetailMapper.createData(param);
        return ifsReportG14DetailMapper.searchAll(param);
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}
