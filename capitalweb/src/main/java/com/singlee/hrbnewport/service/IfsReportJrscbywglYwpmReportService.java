package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportJrscbywglYwpm;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;


/**
 * @author Administrator
 * <p>
 * 金融市场业务排名情况表（当月）（累计）
 */
@Service
public interface IfsReportJrscbywglYwpmReportService extends CommonExportService {

    Page<IfsReportJrscbywglYwpm> searchIfsReportJrscbywglYwpmPage(@RequestBody Map<String, String> map);

    SlOutBean ifsReportJrscbywglYwpmCreate(Map<String, String> map);
}
 