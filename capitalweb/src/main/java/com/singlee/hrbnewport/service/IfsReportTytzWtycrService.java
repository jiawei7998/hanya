package com.singlee.hrbnewport.service;

import com.singlee.hrbnewport.model.IfsReportTytzWtycr;

import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/3 15:37
 * @description：${description}
 * @modified By：
 * @version:   
 */
public interface IfsReportTytzWtycrService extends CommonExportService{


    int insert(IfsReportTytzWtycr record);

    int insertSelective(IfsReportTytzWtycr record);
    List<IfsReportTytzWtycr> getDate(Map<String,Object> map);

}
