package com.singlee.hrbnewport.service;

import com.singlee.hrbnewport.model.IfsReportTytzZfWtc;

import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 16:22
 * @description：${description}
 * @modified By：
 * @version:   
 */
public interface IfsReportTytzZfWtcService extends CommonExportService{


    int insert(IfsReportTytzZfWtc record);

    int insertSelective(IfsReportTytzZfWtc record);

    List<IfsReportTytzZfWtc> getDate(Map<String,Object> map);

}
