package com.singlee.hrbnewport.service;

import com.singlee.hrbnewport.model.IfsReportBondtraded;

import java.util.List;
import java.util.Map;

public interface IfsReportBondtradedService extends CommonExportService{

    List<IfsReportBondtraded> selectReport(Map<String,String > map);
}
