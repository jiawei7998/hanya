package com.singlee.hrbnewport.service;

import com.singlee.hrbnewport.model.IfsReportTytzZfWct;

import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 16:22
 * @description：${description}
 * @modified By：
 * @version:   
 */
public interface IfsReportTytzZfWctService extends CommonExportService{


    int insert(IfsReportTytzZfWct record);

    int insertSelective(IfsReportTytzZfWct record);


    List<IfsReportTytzZfWct> getDate(Map<String,Object> map);
}
