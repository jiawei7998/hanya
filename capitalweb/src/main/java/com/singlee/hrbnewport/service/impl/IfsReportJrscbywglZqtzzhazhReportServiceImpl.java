package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.hrbnewport.mapper.IfsReportJrscbywglZqtzzhazhMapper;
import com.singlee.hrbnewport.model.IfsReportJrscbywglZqtzzhazh;
import com.singlee.hrbnewport.service.IfsReportJrscbywglZqtzzhazhReportService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author Administrator
 * <p>
 * 债券投资账户结构表(按会计账户）
 */
@Service
public class IfsReportJrscbywglZqtzzhazhReportServiceImpl implements IfsReportJrscbywglZqtzzhazhReportService {


    @Autowired
    private IfsReportJrscbywglZqtzzhazhMapper ifsReportJrscbywglZqtzzhazhMapper;

    private static Logger logger = Logger.getLogger(IfsReportJrscbywglZqtzzhazhReportServiceImpl.class);


    @Override
    public Page<IfsReportJrscbywglZqtzzhazh> searchIfsReportJrscbywglZqtzzhazhPage(Map<String, String> map) {
        int pageNumber = ParameterUtil.getInt(map, "pageNumber", 10);
        int pageSize = ParameterUtil.getInt(map, "pageSize", 1);
        List<IfsReportJrscbywglZqtzzhazh> ifsReportJrscbywglZqtzzhazhList = ifsReportJrscbywglZqtzzhazhMapper.selectAll();
        return HrbReportUtils.producePage(ifsReportJrscbywglZqtzzhazhList, pageNumber, pageSize);
    }

    @Override
    public SlOutBean ifsReportJrscbywglZqtzzhazhCreate(Map<String, String> map) {
        SlOutBean msg = new SlOutBean(RetStatusEnum.S, "error.common.0000", "【金融市场部业务管理报表-债券投资账户结构表(按会计账户）】生成成功！");
        ifsReportJrscbywglZqtzzhazhMapper.ifsReportJrscbywglZqtzzhazhCreate(map);
        String RETCODE = map.get("RETCODE");
        if (!"999".equals(RETCODE)) {
            logger.info("【金融市场部业务管理报表-债券投资账户结构表(按会计账户）】生成失败!");
            return new SlOutBean(RetStatusEnum.F, RETCODE, "【金融市场部业务管理报表-债券投资账户结构表(按会计账户）】生成失败!");
        }
        msg.setRetCode("999");
        msg.setRetMsg("【金融市场部业务管理报表-债券投资账户结构表(按会计账户）】生成成功！");
        logger.info("【金融市场部业务管理报表-债券投资账户结构表(按会计账户）】生成成功！");
        return msg;
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        String queryDate = ParameterUtil.getString(map,"queryDate","");
        queryDate = DateUtil.format(DateUtil.parse(queryDate), "yyyy年MM月dd日");
        List<IfsReportJrscbywglZqtzzhazh> ifsReportJrscbywglZqtzzhazhList = ifsReportJrscbywglZqtzzhazhMapper.selectAll();
        Map<String, Object> rustmap = new HashMap<>();
        rustmap.put("list",ifsReportJrscbywglZqtzzhazhList);
        rustmap.put("bbrq",queryDate);
        return rustmap;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}