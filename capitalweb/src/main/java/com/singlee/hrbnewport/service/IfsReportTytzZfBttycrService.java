package com.singlee.hrbnewport.service;

import com.singlee.hrbnewport.model.IfsReportTytzZfBttycr;

import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 16:22
 * @description：${description}
 * @modified By：
 * @version:   
 */
public interface IfsReportTytzZfBttycrService extends CommonExportService{


    int insert(IfsReportTytzZfBttycr record);

    int insertSelective(IfsReportTytzZfBttycr record);

    List<IfsReportTytzZfBttycr> getDate(Map<String,Object> map);

}
