package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportS6301;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;


/**
 * @author Administrator
 * <p>
 * S6301 大中小微型企业贷款情况表
 */
@Service
public interface IfsReportS6301ReportService extends CommonExportService {

    Page<IfsReportS6301> searchIfsReportS6301Page(@RequestBody Map<String, String> map);

    SlOutBean ifsReportS6301Create(Map<String, String> map);
}
 