package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.mapper.FundExportMapper;
import com.singlee.hrbnewport.service.FundExportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 202XXXXXFund
 * @author cg
 *
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class FundExportServiceImpl implements FundExportService {

	@Autowired
	private FundExportMapper fundExportMapper;


	@Override
	public Map<String, Object> getExportData(Map<String, Object> map) {
		String queryDate=map.get("queryDate").toString();
		Map<String, Object> result = new HashMap<String, Object>(16);
		fundExportMapper.generateData(map);
		List<Map<String, Object>> mapList = fundExportMapper.getData(map);
		for (int i = 0; i < mapList.size(); i++) {
			result.put("map",mapList.get(i));
		}
		result.put("instName","哈尔滨银行股份有限公司");
		result.put("year",queryDate.substring(0,4));
		result.put("month",queryDate.substring(5,7));
		result.put("day",queryDate.substring(8,10));
		return result;
	}

	@Override
	public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
		fundExportMapper.generateData(map);
		return fundExportMapper.getData(map);
	}

	@Override
	public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
		return null;
	}
}
