package com.singlee.hrbnewport.service;

import com.singlee.hrbnewport.model.IfsReportTytzJcWct;

import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 16:38
 * @description：${description}
 * @modified By：
 * @version:   
 */
public interface IfsReportTytzJcWctService  extends CommonExportService{


    int insert(IfsReportTytzJcWct record);

    int insertSelective(IfsReportTytzJcWct record);

    List<IfsReportTytzJcWct> getDate(Map<String,Object> map);

}
