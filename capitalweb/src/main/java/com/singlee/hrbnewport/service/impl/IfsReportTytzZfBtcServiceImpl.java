package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbnewport.mapper.*;
import com.singlee.hrbnewport.model.*;
import com.singlee.hrbnewport.service.IfsReportTytzZfBtcService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 16:22
 * @description：${description}
 * @modified By：
 * @version:   
 */
@Service
public class IfsReportTytzZfBtcServiceImpl implements IfsReportTytzZfBtcService{

    @Resource
    private IfsReportTytzZfBtcMapper ifsReportTytzZfBtcMapper;

    @Resource
    private IfsReportTytzZfBctMapper ifsReportTytzZfBctMapper;

    @Resource
    private IfsReportTytzZfBtyccMapper ifsReportTytzZfBtyccMapper;

    @Resource
    private IfsReportTytzZfBttycrMapper ifsReportTytzZfBtycrMapper;

    @Resource
    private IfsReportTytzZfWtcMapper ifsReportTytzZfWtcMapper;

    @Resource
    private IfsReportTytzZfWctMapper ifsReportTytzZfWctMapper;

    @Resource
    private IfsReportTytzZfBcftyMapper ifsReportTytzZfWtyccMapper;

    @Resource
    private IfsReportTytzZfWtycrMapper ifsReportTytzZfWtycrMapper;


    @Override
    public int insert(IfsReportTytzZfBtc record) {
        return ifsReportTytzZfBtcMapper.insert(record);
    }

    @Override
    public int insertSelective(IfsReportTytzZfBtc record) {
        return ifsReportTytzZfBtcMapper.insertSelective(record);
    }

    @Override
    public List<IfsReportTytzZfBtc> getDate(Map<String, Object> map) {
        ifsReportTytzZfBtcMapper.call(map);
        return ifsReportTytzZfBtcMapper.getDate(map);
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String,Object> mapEnd=new HashMap<>();
        String sheetType= ParameterUtil.getString(map,"sheetType","");
        if(sheetType.equals( "BTYCJ" ) ) {
            //本币-同业拆入
            List<IfsReportTytzZfBttycr> list=ifsReportTytzZfBtycrMapper.getDate(map);
            mapEnd.put("list",list);

        }
        if(sheetType.equals( "BTYCC" ) ) {
            //本币-同业拆出
            List<IfsReportTytzZfBtycc> list=ifsReportTytzZfBtyccMapper.getDate(map);
            mapEnd.put("list",list);
        }
        if(sheetType.equals( "WTYCF" ) ) {
            //外币-同业存放
            List<IfsReportTytzZfWtc> list=ifsReportTytzZfWtcMapper.getDate(map);
            mapEnd.put("list",list);
        }
        if(sheetType.equals( "WCFTY" ) ) {
            //外币-存放同业
            List<IfsReportTytzZfWct> list=ifsReportTytzZfWctMapper.getDate(map);
            mapEnd.put("list",list);
        }
        if(sheetType.equals( "WTYCR" ) ) {
            //外币-同业拆入
            List<IfsReportTytzZfWtycr> list=ifsReportTytzZfWtycrMapper.getDate(map);
            mapEnd.put("list",list);
        }
        if(sheetType.equals( "WCFTY2" ) ) {
            //外币-同业拆出

            List<IfsReportTytzZfBcfty> list=ifsReportTytzZfWtyccMapper.getDate(map);
            mapEnd.put("list",list);
        }

        if(sheetType.equals( "BTYCF" ) ) {
            //本币-同业存放

            List<IfsReportTytzZfBtc> list=ifsReportTytzZfBtcMapper.getDate(map);
            mapEnd.put("list",list);

        }
        if(sheetType.equals( "BCFTY" ) ) {
            //本币-存放同业

            List<IfsReportTytzZfBct> list=ifsReportTytzZfBctMapper.getDate(map);
            mapEnd.put("list",list);

        }
        return mapEnd;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}
