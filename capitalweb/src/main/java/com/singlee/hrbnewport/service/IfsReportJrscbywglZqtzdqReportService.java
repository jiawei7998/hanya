package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportJrscbywglZqtzdq;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;


/**
 * @author Administrator
 * <p>
 * 债券投资到期结构表
 */
@Service
public interface IfsReportJrscbywglZqtzdqReportService extends CommonExportService {

    Page<IfsReportJrscbywglZqtzdq> searchIfsReportJrscbywglZqtzdqPage(@RequestBody Map<String, String> map);

    SlOutBean ifsReportJrscbywglZqtzdqCreate(Map<String, String> map);
}
 