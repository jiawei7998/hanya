package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.mapper.IfsReportTytzBtyccMapper;
import com.singlee.hrbnewport.model.IfsReportTytzBtycc;
import com.singlee.hrbnewport.service.IfsReportTytzBtyccService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ：chenguo
 * @date ：Created in 2021/12/3 15:27
 * @description：${description}
 * @modified By：
 * @version:
 */
@Service
public class IfsReportTytzBtyccServiceImpl implements IfsReportTytzBtyccService {

    @Resource
    private IfsReportTytzBtyccMapper ifsReportTytzBtyccMapper;

    @Override
    public int insert(IfsReportTytzBtycc record) {
        return ifsReportTytzBtyccMapper.insert(record);
    }

    @Override
    public int insertSelective(IfsReportTytzBtycc record) {
        return ifsReportTytzBtyccMapper.insertSelective(record);
    }

    @Override
    public List<IfsReportTytzBtycc> getDate(Map<String, Object> map) {
        ifsReportTytzBtyccMapper.call(map);
        return ifsReportTytzBtyccMapper.getDate(map);
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String,Object> mapEnd=new HashMap<>();
        List<IfsReportTytzBtycc> list=ifsReportTytzBtyccMapper.getDate(map);
        mapEnd.put("list",list);
        return mapEnd;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}

