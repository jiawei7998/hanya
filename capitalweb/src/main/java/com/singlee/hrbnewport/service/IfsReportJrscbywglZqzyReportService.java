package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportJrscbywglZqzy;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;


/**
 * @author Administrator
 * <p>
 * 债券质押情况表
 */
@Service
public interface IfsReportJrscbywglZqzyReportService extends CommonExportService {

    Page<IfsReportJrscbywglZqzy> searchIfsReportJrscbywglZqzyPage(@RequestBody Map<String, String> map);

    SlOutBean ifsReportJrscbywglZqzyCreate(Map<String, String> map);
}
 