package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.hrbnewport.mapper.IfsReportJrscbywglYwjyltjMapper;
import com.singlee.hrbnewport.model.IfsReportJrscbywglYwjyltj;
import com.singlee.hrbnewport.service.IfsReportJrscbywglYwjyltjReportService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author Administrator
 * <p>
 * 业务交易量统计表
 */
@Service
public class IfsReportJrscbywglYwjyltjReportServiceImpl implements IfsReportJrscbywglYwjyltjReportService {


    @Autowired
    private IfsReportJrscbywglYwjyltjMapper ifsReportJrscbywglYwjyltjMapper;

    private static Logger logger = Logger.getLogger(IfsReportJrscbywglYwjyltjReportServiceImpl.class);


    @Override
    public Page<IfsReportJrscbywglYwjyltj> searchIfsReportJrscbywglYwjyltjPage(Map<String, String> map) {
        int pageNumber = ParameterUtil.getInt(map, "pageNumber", 10);
        int pageSize = ParameterUtil.getInt(map, "pageSize", 1);
        List<IfsReportJrscbywglYwjyltj> ifsReportJrscbywglYwjyltjList = ifsReportJrscbywglYwjyltjMapper.selectAll();
        return HrbReportUtils.producePage(ifsReportJrscbywglYwjyltjList, pageNumber, pageSize);
    }

    @Override
    public SlOutBean ifsReportJrscbywglYwjyltjCreate(Map<String, String> map) {
        SlOutBean msg = new SlOutBean(RetStatusEnum.S, "error.common.0000", "【金融市场部业务管理报表-业务交易量统计表】生成成功！");
        ifsReportJrscbywglYwjyltjMapper.ifsReportJrscbywglYwjyltjCreate(map);
        String RETCODE = map.get("RETCODE");
        if (!"999".equals(RETCODE)) {
            logger.info("【金融市场部业务管理报表-业务交易量统计表】生成失败!");
            return new SlOutBean(RetStatusEnum.F, RETCODE, "【金融市场部业务管理报表-业务交易量统计表】生成失败!");
        }
        msg.setRetCode("999");
        msg.setRetMsg("【金融市场部业务管理报表-业务交易量统计表】生成成功！");
        logger.info("【金融市场部业务管理报表-业务交易量统计表】生成成功！");
        return msg;
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        String queryDate = ParameterUtil.getString(map,"queryDate","");
        queryDate = DateUtil.format(DateUtil.parse(queryDate), "yyyy年MM月dd日");
        List<IfsReportJrscbywglYwjyltj> ifsReportJrscbywglYwjyltjList = ifsReportJrscbywglYwjyltjMapper.selectAll();
        Map<String, Object> rustmap = new HashMap<>();
        rustmap.put("list",ifsReportJrscbywglYwjyltjList);
        rustmap.put("bbrq",queryDate);
        return rustmap;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}