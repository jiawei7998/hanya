package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.calendar.util.CalendarUtil;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.hrbnewport.mapper.*;
import com.singlee.hrbnewport.model.DealdetailPubFund;
import com.singlee.hrbreport.mapper.DealDetailMapper;
import com.singlee.hrbreport.model.MutualFundsVO;
import com.singlee.hrbreport.model.RepoDealDetailVO;
import com.singlee.hrbreport.model.SecDetailVO;
import com.singlee.hrbreport.model.SecPurchaseVO;
import com.singlee.hrbnewport.service.DealDetailService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * _业务明细_人行
 * @author zk
 *
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class DealDetailServiceImpl implements DealDetailService {

    @Autowired
    DealdetailPubFundMapper dealdetailPubFundMapper;//业务明细-公募基金
    @Autowired
    DealdetailSpeFundMapper dealdetailSpeFundMapper;//业明细-基金管理公司及子公司专户产品
    @Autowired
    DealdetailCdSubMapper dealdetailCdSubMapper;// 业务明细-同业存单申购信息
    @Autowired
    DealdetailCdTposMapper dealdetailCdTposMapper;//业务明细-投资同业存单明细
    @Autowired
    DealdetailRvreposecSpvMapper dealdetailRvreposecSpvMapper;//业务明细-向境内交易及结算类金融机构买入返售债券
    @Autowired
    DealdetailReposecSpvMapper dealdetailReposecSpvMapper;//业务明细-向境内交易及结算类金融机构卖出回购债券
    @Autowired
    DealdetailRvreposecInsureMapper dealdetailRvreposecInsureMapper;//业务明细-向境内保险业金融机构买入返售债券
    @Autowired
    DealdetailReposecInsureMapper dealdetailReposecInsureMapper;//业务明细-向境内保险业金融机构卖出回购债券
    @Autowired
    DealdetailRvreposecSettleMapper dealdetailRvreposecSettleMapper;//业务明细-向境内交易及结算类金融机构买入返售债券
    @Autowired
    DealdetailRvreposecBankMapper dealdetailRvreposecBankMapper;//业务明细-向境内银行业存款类金融机构买入返售债券
    @Autowired
    DealdetailReposecBankMapper dealdetailReposecBankMapper;//业务明细-向境内银行业存款类金融机构卖出回购债券
    @Autowired
    DealdetailRvreposecStockMapper dealdetailRvreposecStockMapper;//业务明细-向境内证券业金融机构买入返售债券
    @Autowired
    DealdetailReposecStockMapper dealdetailReposecStockMapper;//业务明细-向境内证券业金融机构卖出回购债券
    @Autowired
    DealdetailReposecCentbankMapper dealdetailReposecCentbankMapper;//业务明细-向境内交易及结算类金融机构卖出回购债券
    @Autowired
    DealdetailMoneyMapper dealdetailMoneyMapper;//业务明细-基金管理公司及子公司专户产品

    DealDetailBaseMapper dealDetailBaseMapper;

    /******************************************报表查询方法***********************************************/
    @Override
    public Page<T> searchDetailPage(Map<String, Object> map) {
        String sheetType = ParameterUtil.getString(map, "sheetType", "pub_fund");
        Object temDate = map.get("queryDate");
        String queryDate = temDate!=null&&!"".equals(temDate.toString().trim())?temDate.toString().trim(): DateUtil.getCurrentDateAsString();
        map.put("DATADATE", queryDate);

        switch (sheetType){
            case "pub_fund":
                dealDetailBaseMapper = dealdetailPubFundMapper;
                break;
            case "spe_fund":
                dealDetailBaseMapper = dealdetailSpeFundMapper;
                break;
            case "cd_sub":
                dealDetailBaseMapper = dealdetailCdSubMapper;
                break;
            case "cd_tpos":
                dealDetailBaseMapper = dealdetailCdTposMapper;
                break;
            case "rvrepo_spv":
                dealDetailBaseMapper = dealdetailRvreposecSpvMapper;
                break;
            case "repo_spv":
                dealDetailBaseMapper = dealdetailReposecSpvMapper;
                break;
            case "rvrepo_insure":
                dealDetailBaseMapper = dealdetailRvreposecInsureMapper;
                break;
            case "repo_insure":
                dealDetailBaseMapper = dealdetailReposecInsureMapper;
                break;
            case "rvrepo_settle":
                dealDetailBaseMapper = dealdetailRvreposecSettleMapper;
                break;
            case "rvrepo_bank":
                dealDetailBaseMapper = dealdetailRvreposecBankMapper;
                break;
            case "repo_bank":
                dealDetailBaseMapper = dealdetailReposecBankMapper;
                break;
            case "rvrepo_stock":
                dealDetailBaseMapper = dealdetailRvreposecStockMapper;
                break;
            case "repo_stock":
                dealDetailBaseMapper = dealdetailReposecStockMapper;
                break;
            case "repo_centbank":
                dealDetailBaseMapper = dealdetailReposecCentbankMapper;
                break;
            case "money":
                dealDetailBaseMapper = dealdetailMoneyMapper;
                break;
        }
        dealDetailBaseMapper.createData(map);
        Page<T> ts = dealDetailBaseMapper.searAllPage(map,ParameterUtil.getRowBounds(map));
        return ts;
    }

    /******************************************报表导出方法***********************************************/
    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        HashMap<String, Object> result = new HashMap<>();
        Object temDate = map.get("queryDate");
        String queryDate = temDate!=null&&!"".equals(temDate.toString().trim())?temDate.toString().trim(): DateUtil.getCurrentDateAsString();
        map.put("queryDate", queryDate);
        String sheetType = ParameterUtil.getString(map, "sheetType", "pub_fund");
        List<T> list;
        switch (sheetType){
            case "pub_fund":
                dealdetailPubFundMapper.createData(map);
                list = dealdetailPubFundMapper.searAll(map);

                result.put("list",list);
                break;
            case "spe_fund":
                dealdetailSpeFundMapper.createData(map);
                list = dealdetailSpeFundMapper.searAll(map);

                result.put("list",list);
                break;
            case "cd_sub":
                dealdetailCdSubMapper.createData(map);
                list = dealdetailCdSubMapper.searAll(map);

                result.put("list",list);
                break;
            case "cd_tpos":
                dealdetailCdTposMapper.createData(map);
                list = dealdetailCdTposMapper.searAll(map);

                result.put("list",list);
                break;
            case "rvrepo_spv":
                dealdetailRvreposecSpvMapper.createData(map);
                list = dealdetailRvreposecSpvMapper.searAll(map);

                result.put("list",list);
                break;
            case "repo_spv":
                dealdetailReposecSpvMapper.createData(map);
                list = dealdetailReposecSpvMapper.searAll(map);

                result.put("list",list);
                break;
            case "rvrepo_insure":
                dealdetailRvreposecInsureMapper.createData(map);
                list = dealdetailRvreposecInsureMapper.searAll(map);

                result.put("list",list);
                break;
            case "repo_insure":
                dealdetailReposecInsureMapper.createData(map);
                list = dealdetailReposecInsureMapper.searAll(map);

                result.put("list",list);
                break;
            case "rvrepo_settle":
                dealdetailRvreposecSettleMapper.createData(map);
                list = dealdetailRvreposecSettleMapper.searAll(map);

                result.put("list",list);
                break;
            case "rvrepo_bank":
                dealdetailRvreposecBankMapper.createData(map);
                list = dealdetailRvreposecBankMapper.searAll(map);

                result.put("list",list);
                break;
            case "repo_bank":
                dealdetailReposecBankMapper.createData(map);
                list = dealdetailReposecBankMapper.searAll(map);

                result.put("list",list);
                break;
            case "rvrepo_stock":
                dealdetailRvreposecStockMapper.createData(map);
                list = dealdetailRvreposecStockMapper.searAll(map);

                result.put("list",list);
                break;
            case "repo_stock":
                dealdetailReposecStockMapper.createData(map);
                list = dealdetailReposecStockMapper.searAll(map);

                result.put("list",list);
                break;
            case "repo_centbank":
                dealdetailReposecCentbankMapper.createData(map);
                list = dealdetailReposecCentbankMapper.searAll(map);

                result.put("list",list);
                break;
            case "money":
                dealdetailMoneyMapper.createData(map);
                list = dealdetailMoneyMapper.searAll(map);

                result.put("list",list);
                break;
        }
        return result;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}
