package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.model.IfsReportCurAsset;
import com.singlee.hrbnewport.model.IfsReportG52GovRepay;

import java.util.Map;

/**
 * 哈尔滨银行优质流动性资产台账
 * 2021/12/9
 * @Auther:zk
 */
public interface IfsReportCurAssetService extends CommonExportService {

    Page<IfsReportCurAsset> searchAllPage(Map<String, Object> map);

}
