package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.model.IfsReportCdPosit;
import com.singlee.hrbnewport.model.IfsReportCdPosition;

import java.util.Map;

/**
 * 同 业 存 单
 * 2021/12/9
 *
 * @Auther:zk
 */
public interface IfsReportCdPositService extends CommonExportService {
    Page<IfsReportCdPosit> searchAllPage(Map<String, Object> map);
}
