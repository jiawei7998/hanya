package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbnewport.mapper.G01ReportMapper;
import com.singlee.hrbnewport.model.G01ReportVO;
import com.singlee.hrbnewport.service.G01ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * G01 资产负债项目统计表
 * @author cg
 *
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class G01ReportServiceImpl implements G01ReportService {
	
	@Autowired
	private G01ReportMapper G1;

	@Override
	public Page<G01ReportVO> getG01Report(Map<String, String> map) {
		Page<G01ReportVO> result= null;
		//调用存储过程生成数据
		G1.G01ReportCreate(map);
		if ("999".equals(map.get("RETCODE"))){
			JY.info(map.get("RETMSG"));
			result= G1.getG01Report();
		}else if ("100".equals(map.get("RETCODE"))){
			JY.info(map.get("RETMSG"));
		}else {
			JY.raise("生成数据失败，请检查再执行！");
		}
		return result;
	}

	@Override
	public Map<String, Object> getExportData(Map<String, Object> map) {
		Map<String, Object> result = new HashMap<String, Object>();
		String queryDate = ParameterUtil.getString(map,"queryDate", DateUtil.getCurrentDateAsString());
		map.put("queryDate",queryDate);
		List<G01ReportVO> list=G1.getG01Report();
		for (int i = 0; i <list.size() ; i++) {
			result.put("list"+ i,list.get(i));
		}
        result.put("instName","哈尔滨银行股份有限公司");
		result.put("year",queryDate.substring(0,4));
		result.put("month",queryDate.substring(5,7));
//		result.put("day",queryDate.substring(8,10));
		return result;

	}

	@Override
	public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
		return null;
	}

	@Override
	public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
		return null;
	}
}
