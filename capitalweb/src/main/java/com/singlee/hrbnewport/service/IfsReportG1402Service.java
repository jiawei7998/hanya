package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.model.IfsReportG1402;

import java.util.Map;

/**
 * G1402-大额风险暴露
 * 2021/11/25
 * @Auther:zk
 */
public interface  IfsReportG1402Service extends CommonExportService{

    void creatG1402(Map<String , Object> map);

    Page<IfsReportG1402> searchIfsReportG1402(Map<String,Object> map);
}
