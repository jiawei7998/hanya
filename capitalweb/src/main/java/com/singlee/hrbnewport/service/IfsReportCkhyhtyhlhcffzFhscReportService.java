package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportCkhyhtyhlhcffzFhsc;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;


/**
 * @author Administrator
 * <p>
 * 存款含银行同业和联行存放负债 分行上传表
 */
@Service
public interface IfsReportCkhyhtyhlhcffzFhscReportService extends CommonExportService {

    Page<IfsReportCkhyhtyhlhcffzFhsc> searchIfsReportCkhyhtyhlhcffzFhscPage(@RequestBody Map<String, String> map);

    SlOutBean ifsReportCkhyhtyhlhcffzFhscCreate(Map<String, String> map);
}
 