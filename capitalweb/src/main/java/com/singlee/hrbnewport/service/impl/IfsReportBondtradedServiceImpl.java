package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbnewport.mapper.IfsReportBondtradedMapper;
import com.singlee.hrbnewport.model.IfsReportBondtraded;
import com.singlee.hrbnewport.service.IfsReportBondtradedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class IfsReportBondtradedServiceImpl implements IfsReportBondtradedService{

    @Autowired
    private IfsReportBondtradedMapper ifsReportBondtradedMapper;


    @Override
    public List<IfsReportBondtraded> selectReport(Map<String, String> map) {
        ifsReportBondtradedMapper.bondtradedCreate(map);
        return ifsReportBondtradedMapper.getBondtraded();
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String, Object> result = new HashMap<String, Object>();
        String queryDate = ParameterUtil.getString(map,"queryDate", DateUtil.getCurrentDateAsString());
        List<IfsReportBondtraded> list=ifsReportBondtradedMapper.getBondtraded();
        result.put("list",list);
        result.put("queryDate",queryDate);
        return result;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}
