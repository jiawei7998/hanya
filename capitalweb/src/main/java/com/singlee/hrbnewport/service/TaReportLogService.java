package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.model.TaReportLog;

import java.util.Map;

public interface TaReportLogService {


	int insert(TaReportLog record);

	int insertSelective(TaReportLog record);

	Page<Map<String, Object>> getPageList(Map<String, Object> map);

}

