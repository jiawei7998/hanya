package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.mapper.IfsReportTytzFxWtcMapper;
import com.singlee.hrbnewport.model.IfsReportTytzFxWtc;
import com.singlee.hrbnewport.service.IfsReportTytzFxWtcService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 16:21
 * @description：${description}
 * @modified By：
 * @version:   
 */
@Service
public class IfsReportTytzFxWtcServiceImpl implements IfsReportTytzFxWtcService{

    @Resource
    private IfsReportTytzFxWtcMapper ifsReportTytzFxWtcMapper;

    @Override
    public int insert(IfsReportTytzFxWtc record) {
        return ifsReportTytzFxWtcMapper.insert(record);
    }

    @Override
    public int insertSelective(IfsReportTytzFxWtc record) {
        return ifsReportTytzFxWtcMapper.insertSelective(record);
    }

    @Override
    public List<IfsReportTytzFxWtc> getDate(Map<String, Object> map) {
        ifsReportTytzFxWtcMapper.call(map);
        return ifsReportTytzFxWtcMapper.getDate(map);
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String,Object> mapEnd=new HashMap<>();
        List<IfsReportTytzFxWtc> list=ifsReportTytzFxWtcMapper.getDate(map);
        mapEnd.put("list",list);
        return mapEnd;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}
