package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.mapper.IfsReportTytzWtyccMapper;
import com.singlee.hrbnewport.model.IfsReportTytzWtycc;
import com.singlee.hrbnewport.service.IfsReportTytzWtyccService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/3 15:37
 * @description：${description}
 * @modified By：
 * @version:   
 */
@Service
public class IfsReportTytzWtyccServiceImpl implements IfsReportTytzWtyccService{

    @Resource
    private IfsReportTytzWtyccMapper ifsReportTytzWtyccMapper;

    @Override
    public int insert(IfsReportTytzWtycc record) {
        return ifsReportTytzWtyccMapper.insert(record);
    }

    @Override
    public int insertSelective(IfsReportTytzWtycc record) {
        return ifsReportTytzWtyccMapper.insertSelective(record);
    }

    @Override
    public List<IfsReportTytzWtycc> getDate(Map<String, Object> map) {
        ifsReportTytzWtyccMapper.call(map);
        return ifsReportTytzWtyccMapper.getDate(map);
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String,Object> mapEnd=new HashMap<>();
        List<IfsReportTytzWtycc> list=ifsReportTytzWtyccMapper.getDate(map);
        mapEnd.put("list",list);
        return mapEnd;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}
