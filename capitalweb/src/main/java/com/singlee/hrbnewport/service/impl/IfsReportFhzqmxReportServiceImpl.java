package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.hrbnewport.mapper.IfsReportFhzqmxMapper;
import com.singlee.hrbnewport.model.IfsReportFhzqmx;
import com.singlee.hrbnewport.service.IfsReportFhzqmxReportService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author Administrator
 * <p>
 * 分行债券明细表
 */
@Service
public class IfsReportFhzqmxReportServiceImpl implements IfsReportFhzqmxReportService {


    @Autowired
    private IfsReportFhzqmxMapper ifsReportFhzqmxMapper;

    private static Logger logger = Logger.getLogger(IfsReportFhzqmxReportServiceImpl.class);


    @Override
    public Page<IfsReportFhzqmx> searchIfsReportFhzqmxPage(Map<String, String> map) {
        int pageNumber = ParameterUtil.getInt(map, "pageNumber", 10);
        int pageSize = ParameterUtil.getInt(map, "pageSize", 1);
        List<IfsReportFhzqmx> ifsReportFhzqmxList = ifsReportFhzqmxMapper.selectAll();
        return HrbReportUtils.producePage(ifsReportFhzqmxList, pageNumber, pageSize);
    }

    @Override
    public SlOutBean ifsReportFhzqmxCreate(Map<String, String> map) {
        SlOutBean msg = new SlOutBean(RetStatusEnum.S, "error.common.0000", "【分行债券明细表】生成成功！");
        ifsReportFhzqmxMapper.ifsReportFhzqmxCreate(map);
        String RETCODE = map.get("RETCODE");
        if (!"999".equals(RETCODE)) {
            logger.info("【分行债券明细表】生成失败!");
            return new SlOutBean(RetStatusEnum.F, RETCODE, "【分行债券明细表】生成失败!");
        }
        msg.setRetCode("999");
        msg.setRetMsg("【分行债券明细表】生成成功！");
        logger.info("【分行债券明细表】生成成功！");
        return msg;
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        List<IfsReportFhzqmx> ifsReportFhzqmxList = ifsReportFhzqmxMapper.selectAll();
        Map<String, Object> rustmap = new HashMap<>();
        rustmap.put("list",ifsReportFhzqmxList);
        return rustmap;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}