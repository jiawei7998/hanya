package com.singlee.hrbnewport.service;

import com.singlee.hrbnewport.model.TbBondInvestment;

import java.util.List;
import java.util.Map;

public interface TbBondInvestmentService extends CommonExportService{


    List<TbBondInvestment> selectReport(Map<String,String > map);

}
