package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.hrbnewport.mapper.IfsReportCshmyyhzyjgzbqkbMapper;
import com.singlee.hrbnewport.model.IfsReportCshmyyhzyjgzbqkb;
import com.singlee.hrbnewport.service.IfsReportCshmyyhzyjgzbqkbReportService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author Administrator
 * <p>
 * 城商行民营银行主要监管指标情况表
 */
@Service
public class IfsReportCshmyyhzyjgzbqkbReportServiceImpl implements IfsReportCshmyyhzyjgzbqkbReportService {


    @Autowired
    private IfsReportCshmyyhzyjgzbqkbMapper ifsReportCshmyyhzyjgzbqkbMapper;

    private static Logger logger = Logger.getLogger(IfsReportCshmyyhzyjgzbqkbReportServiceImpl.class);


    @Override
    public Page<IfsReportCshmyyhzyjgzbqkb> searchIfsReportCshmyyhzyjgzbqkbPage(Map<String, String> map) {
        int pageNumber = ParameterUtil.getInt(map, "pageNumber", 10);
        int pageSize = ParameterUtil.getInt(map, "pageSize", 1);
        List<IfsReportCshmyyhzyjgzbqkb> ifsReportCshmyyhzyjgzbqkbList = ifsReportCshmyyhzyjgzbqkbMapper.selectAll();
        return HrbReportUtils.producePage(ifsReportCshmyyhzyjgzbqkbList, pageNumber, pageSize);
    }

    @Override
    public SlOutBean ifsReportCshmyyhzyjgzbqkbCreate(Map<String, String> map) {
        SlOutBean msg = new SlOutBean(RetStatusEnum.S, "error.common.0000", "【城商行民营银行主要监管指标情况表】生成成功！");
        ifsReportCshmyyhzyjgzbqkbMapper.ifsReportCshmyyhzyjgzbqkbCreate(map);
        String RETCODE = map.get("RETCODE");
        if (!"999".equals(RETCODE)) {
            logger.info("【城商行民营银行主要监管指标情况表】生成失败!");
            return new SlOutBean(RetStatusEnum.F, RETCODE, "【城商行民营银行主要监管指标情况表】生成失败!");
        }
        msg.setRetCode("999");
        msg.setRetMsg("【城商行民营银行主要监管指标情况表】生成成功！");
        logger.info("【城商行民营银行主要监管指标情况表】生成成功！");
        return msg;
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        List<IfsReportCshmyyhzyjgzbqkb> ifsReportCshmyyhzyjgzbqkbList = ifsReportCshmyyhzyjgzbqkbMapper.selectAll();
        Map<String, Object> rustmap = new HashMap<>();
        rustmap.put("list",ifsReportCshmyyhzyjgzbqkbList);
        return rustmap;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}