package com.singlee.hrbnewport.service;

import com.singlee.hrbnewport.model.IfsReportTywlzhye;

import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 20:51
 * @description：${description}
 * @modified By：
 * @version:   
 */
public interface IfsReportTywlzhyeService extends CommonExportService{


    int insert(IfsReportTywlzhye record);

    int insertSelective(IfsReportTywlzhye record);

    List<IfsReportTywlzhye> getDate(Map<String,Object> map);

    void  upDate(IfsReportTywlzhye record);

}
