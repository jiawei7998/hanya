package com.singlee.hrbnewport.service;

import com.singlee.hrbnewport.model.IfsReportKjjbsrxx;

import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 20:50
 * @description：${description}
 * @modified By：
 * @version:   
 */
public interface IfsReportKjjbsrxxService extends CommonExportService{


    int insert(IfsReportKjjbsrxx record);

    int insertSelective(IfsReportKjjbsrxx record);

    List<IfsReportKjjbsrxx> getDate(Map<String,Object> map);

}
