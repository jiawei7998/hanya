package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportJrscbywglZqtzsy;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;


/**
 * @author Administrator
 * <p>
 * 债券投资收益结构表
 */
@Service
public interface IfsReportJrscbywglZqtzsyReportService extends CommonExportService {

    Page<IfsReportJrscbywglZqtzsy> searchIfsReportJrscbywglZqtzsyPage(@RequestBody Map<String, String> map);

    SlOutBean ifsReportJrscbywglZqtzsyCreate(Map<String, String> map);
}
 