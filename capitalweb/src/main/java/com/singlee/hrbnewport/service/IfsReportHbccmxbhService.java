package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.model.IfsReportHbccmxbh;

import java.util.Map;

public interface IfsReportHbccmxbhService extends CommonExportService{


    Page<IfsReportHbccmxbh> selectReport(Map<String,String > map);

}
