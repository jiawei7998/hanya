package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.hrbnewport.mapper.IfsReportJrscbywglZqtzsyMapper;
import com.singlee.hrbnewport.model.IfsReportJrscbywglZqtzsy;
import com.singlee.hrbnewport.service.IfsReportJrscbywglZqtzsyReportService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * @author Administrator
 * <p>
 * 债券投资收益结构表
 */
@Service
public class IfsReportJrscbywglZqtzsyReportServiceImpl implements IfsReportJrscbywglZqtzsyReportService {


    @Autowired
    private IfsReportJrscbywglZqtzsyMapper ifsReportJrscbywglZqtzsyMapper;

    private static Logger logger = Logger.getLogger(IfsReportJrscbywglZqtzsyReportServiceImpl.class);


    @Override
    public Page<IfsReportJrscbywglZqtzsy> searchIfsReportJrscbywglZqtzsyPage(Map<String, String> map) {
        int pageNumber = ParameterUtil.getInt(map, "pageNumber", 10);
        int pageSize = ParameterUtil.getInt(map, "pageSize", 1);
        List<IfsReportJrscbywglZqtzsy> ifsReportJrscbywglZqtzsyList = ifsReportJrscbywglZqtzsyMapper.selectAll();
        return HrbReportUtils.producePage(ifsReportJrscbywglZqtzsyList, pageNumber, pageSize);
    }

    @Override
    public SlOutBean ifsReportJrscbywglZqtzsyCreate(Map<String, String> map) {
        SlOutBean msg = new SlOutBean(RetStatusEnum.S, "error.common.0000", "【金融市场部业务管理报表-债券投资收益结构表】生成成功！");
        ifsReportJrscbywglZqtzsyMapper.ifsReportJrscbywglZqtzsyCreate(map);
        String RETCODE = map.get("RETCODE");
        if (!"999".equals(RETCODE)) {
            logger.info("【金融市场部业务管理报表-债券投资收益结构表】生成失败!");
            return new SlOutBean(RetStatusEnum.F, RETCODE, "【金融市场部业务管理报表-债券投资收益结构表】生成失败!");
        }
        msg.setRetCode("999");
        msg.setRetMsg("【金融市场部业务管理报表-债券投资收益结构表】生成成功！");
        logger.info("【金融市场部业务管理报表-债券投资收益结构表】生成成功！");
        return msg;
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        String queryDate = ParameterUtil.getString(map,"queryDate","");
        queryDate = DateUtil.format(DateUtil.parse(queryDate), "yyyy年MM月dd日");
        List<IfsReportJrscbywglZqtzsy> ifsReportJrscbywglZqtzsyList = ifsReportJrscbywglZqtzsyMapper.selectAll();
        Map<String, Object> rustmap = new HashMap<>();
        Map<String, List<IfsReportJrscbywglZqtzsy>> listMap = ifsReportJrscbywglZqtzsyList.stream().collect(Collectors.groupingBy(IfsReportJrscbywglZqtzsy::getJytype));
        for (String s : listMap.keySet()) {
            if("交易性类".equals(s)){
                rustmap.put("listT",listMap.get(s));
            }
            if("可供出售类".equals(s)){
                rustmap.put("listA",listMap.get(s));
            }
            if("持有至到期类".equals(s)){
                rustmap.put("listH",listMap.get(s));
            }
        }
        rustmap.put("bbrq",queryDate);
        return rustmap;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}