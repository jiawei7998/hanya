package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbnewport.mapper.*;
import com.singlee.hrbnewport.model.*;
import com.singlee.hrbnewport.service.IfsReportTytzBtcService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ：chenguo
 * @date ：Created in 2021/12/3 15:27
 * @description：${description}
 * @modified By：本币-同业存放
 * @version:
 */
@Service
public class IfsReportTytzBtcServiceImpl implements IfsReportTytzBtcService {

    @Resource
    private IfsReportTytzBtcMapper ifsReportTytzBtcMapper;

    @Resource
    private IfsReportTytzBctMapper ifsReportTytzBctMapper;

    @Resource
    private IfsReportTytzBtyccMapper ifsReportTytzBtyccMapper;

    @Resource
    private IfsReportTytzBtycrMapper ifsReportTytzBtycrMapper;

    @Resource
    private IfsReportTytzWtcMapper ifsReportTytzWtcMapper;

    @Resource
    private IfsReportTytzWctMapper ifsReportTytzWctMapper;

    @Resource
    private IfsReportTytzWtyccMapper ifsReportTytzWtyccMapper;

    @Resource
    private IfsReportTytzWtycrMapper ifsReportTytzWtycrMapper;



    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        String sheetType= ParameterUtil.getString(map,"sheetType","");
        Map<String,Object> mapEnd=new HashMap<>();

        if(sheetType.equals( "BTYCJ" ) ) {
            //本币-同业拆入
            List<IfsReportTytzBtycr> list=ifsReportTytzBtycrMapper.getDate(map);
            mapEnd.put("list",list);

        }
        if(sheetType.equals( "BTYCC" ) ) {
            //本币-同业拆出
            List<IfsReportTytzBtycc> list=ifsReportTytzBtyccMapper.getDate(map);
            mapEnd.put("list",list);
        }
        if(sheetType.equals( "WTYCF" ) ) {
            //外币-同业存放
            List<IfsReportTytzWtc> list=ifsReportTytzWtcMapper.getDate(map);
            mapEnd.put("list",list);
        }
        if(sheetType.equals( "WCFTY" ) ) {
            //外币-存放同业
            List<IfsReportTytzWct> list=ifsReportTytzWctMapper.getDate(map);
            mapEnd.put("list",list);
        }
        if(sheetType.equals( "WTYCR" ) ) {
            //外币-同业拆入
            List<IfsReportTytzWtycr> list=ifsReportTytzWtycrMapper.getDate(map);
            mapEnd.put("list",list);
        }
        if(sheetType.equals( "WCFTY2" ) ) {
            //外币-同业拆出

            List<IfsReportTytzWtycc> list=ifsReportTytzWtyccMapper.getDate(map);
            mapEnd.put("list",list);
        }

        if(sheetType.equals( "BTYCF" ) ) {
            //本币-同业存放

            List<IfsReportTytzBtc> list=ifsReportTytzBtcMapper.getDateBtc(map);
            mapEnd.put("list",list);

        }
        if(sheetType.equals( "BCFTY" ) ) {
            //本币-存放同业

            List<IfsReportTytzBct> list=ifsReportTytzBctMapper.getDateBct(map);
            mapEnd.put("list",list);

        }

        return mapEnd;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
     return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }

    @Override
    public List<IfsReportTytzBtc> getDate(Map<String, Object> map) {
        ifsReportTytzBtcMapper.callBtc(map);
        return ifsReportTytzBtcMapper.getDateBtc(map);
    }
}



