package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.hrbnewport.mapper.IfsReportG3301Mapper;
import com.singlee.hrbnewport.model.IfsReportG3301;
import com.singlee.hrbnewport.service.IfsReportG3301ReportService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * @author Administrator
 * <p>
 * G3301银行账簿利率风险计量报表
 */
@Service
public class IfsReportG3301ReportServiceImpl implements IfsReportG3301ReportService {


    @Autowired
    private IfsReportG3301Mapper ifsReportG3301Mapper;

    private static Logger logger = Logger.getLogger(IfsReportG3301ReportServiceImpl.class);


    @Override
    public Page<IfsReportG3301> searchIfsReportG3301Page(Map<String, String> map) {
        int pageNumber = ParameterUtil.getInt(map, "pageNumber", 10);
        int pageSize = ParameterUtil.getInt(map, "pageSize", 1);
        List<IfsReportG3301> ifsReportG3301List = ifsReportG3301Mapper.selectAll();
        return HrbReportUtils.producePage(ifsReportG3301List, pageNumber, pageSize);
    }

    @Override
    public SlOutBean ifsReportG3301Create(Map<String, String> map) {
        SlOutBean msg = new SlOutBean(RetStatusEnum.S, "error.common.0000", "【G3301银行账簿利率风险计量报表】生成成功！");
        ifsReportG3301Mapper.ifsReportG3301Create(map);
        String RETCODE = map.get("RETCODE");
        if (!"999".equals(RETCODE)) {
            logger.info("【G3301银行账簿利率风险计量报表】生成失败!");
            return new SlOutBean(RetStatusEnum.F, RETCODE, "【G3301银行账簿利率风险计量报表】生成失败!");
        }
        msg.setRetCode("999");
        msg.setRetMsg("【G3301银行账簿利率风险计量报表】生成成功！");
        logger.info("【G3301银行账簿利率风险计量报表】生成成功！");
        return msg;
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        String queryDate = ParameterUtil.getString(map,"queryDate","");
        queryDate = DateUtil.format(DateUtil.parse(queryDate), "yyyy年MM月dd日");
        List<IfsReportG3301> ifsReportG3301List = ifsReportG3301Mapper.selectAll();
        Map<String, Object> rustmap = new HashMap<>();
        for (IfsReportG3301 ifsReportG3301 : ifsReportG3301List) {
            rustmap.put("map"+getsuff(ifsReportG3301.getProduct()),ifsReportG3301);
        }
        rustmap.put("bskj","境内汇总数据");
        rustmap.put("bbrq",queryDate);
        return rustmap;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }

    private String getsuff(String colum){
        String regEx="[^0-9]";
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(colum);
        colum = m.replaceAll("").trim();
        return colum;
    }
}