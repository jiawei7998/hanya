package com.singlee.hrbnewport.service;

import com.singlee.hrbnewport.model.IfsReportTytzBtycc;

import java.util.List;
import java.util.Map;

/**
 * @author ：chenguo
 * @date ：Created in 2021/12/3 15:27
 * @description：${description}
 * @modified By：
 * @version:
 */
public interface IfsReportTytzBtyccService  extends CommonExportService {


    int insert(IfsReportTytzBtycc record);

    int insertSelective(IfsReportTytzBtycc record);

    List<IfsReportTytzBtycc> getDate(Map<String,Object> map);
}

