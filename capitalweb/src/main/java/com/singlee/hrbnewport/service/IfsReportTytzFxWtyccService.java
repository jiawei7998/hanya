package com.singlee.hrbnewport.service;

import com.singlee.hrbnewport.model.IfsReportTytzFxWtycc;

import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 16:21
 * @description：${description}
 * @modified By：
 * @version:   
 */
public interface IfsReportTytzFxWtyccService extends CommonExportService{


    int insert(IfsReportTytzFxWtycc record);

    int insertSelective(IfsReportTytzFxWtycc record);


    List<IfsReportTytzFxWtycc> getDate(Map<String,Object> map);

}
