package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.mapper.IfsReportTytzFxBtycrMapper;
import com.singlee.hrbnewport.model.IfsReportTytzFxBtycr;
import com.singlee.hrbnewport.service.IfsReportTytzFxBtycrService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 16:33
 * @description：${description}
 * @modified By：
 * @version:   
 */
@Service
public class IfsReportTytzFxBtycrServiceImpl implements IfsReportTytzFxBtycrService{

    @Resource
    private IfsReportTytzFxBtycrMapper ifsReportTytzFxBtycrMapper;

    @Override
    public int insert(IfsReportTytzFxBtycr record) {
        return ifsReportTytzFxBtycrMapper.insert(record);
    }

    @Override
    public int insertSelective(IfsReportTytzFxBtycr record) {
        return ifsReportTytzFxBtycrMapper.insertSelective(record);
    }

    @Override
    public List<IfsReportTytzFxBtycr> getDate(Map<String, Object> map) {
        ifsReportTytzFxBtycrMapper.call(map);
        return ifsReportTytzFxBtycrMapper.getDate(map);
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String,Object> mapEnd=new HashMap<>();
        List<IfsReportTytzFxBtycr> list=ifsReportTytzFxBtycrMapper.getDate(map);
        mapEnd.put("list",list);
        return mapEnd;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}
