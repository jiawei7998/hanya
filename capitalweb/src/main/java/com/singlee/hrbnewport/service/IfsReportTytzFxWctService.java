package com.singlee.hrbnewport.service;

import com.singlee.hrbnewport.model.IfsReportTytzFxWct;

import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 16:21
 * @description：${description}
 * @modified By：
 * @version:   
 */
public interface IfsReportTytzFxWctService extends CommonExportService{


    int insert(IfsReportTytzFxWct record);

    int insertSelective(IfsReportTytzFxWct record);

    List<IfsReportTytzFxWct> getDate(Map<String,Object> map);
}
