package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbnewport.mapper.G21NewReportMapper;
import com.singlee.hrbnewport.service.G21NewReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * G21 流动性期限缺口统计表
 * @author cg
 *
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class G21NewReportServiceImpl implements G21NewReportService {

	@Autowired
    private G21NewReportMapper g21NewReportMapper;

	@Override
	public Map<String, Object> getExportData(Map<String, Object> map) {
		Map<String, Object> result = new HashMap<String, Object>();
		String queryDate = ParameterUtil.getString(map,"queryDate", DateUtil.getCurrentDateAsString());
		map.put("queryDate",queryDate);
		g21NewReportMapper.generateG21Data(map);
		List<Map<String, Object>> par = g21NewReportMapper.getG21Data(map);
		for(int i =0;i<par.size();i++){
			Map<String, Object> paramer = par.get(i);
			result.put("map"+ ParameterUtil.getString(paramer,"no",""),paramer);
		}
		result.put("instName","哈尔滨银行股份有限公司");
		result.put("year",queryDate.substring(0,4));
		result.put("month",queryDate.substring(5,7));
		result.put("day",queryDate.substring(8,10));
		return result;
	}

	@Override
	public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
		g21NewReportMapper.generateG21Data(map);
		return g21NewReportMapper.getG21Data(map).stream().limit(46).collect(Collectors.toList());
	}

	@Override
	public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
		return null;
	}

}
