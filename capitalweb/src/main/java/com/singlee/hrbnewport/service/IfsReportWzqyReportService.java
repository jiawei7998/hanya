package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportWzqy;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;


/**
 * @author Administrator
 * <p>
 * 外债签约信息报送表
 */
@Service
public interface IfsReportWzqyReportService extends CommonExportService {

    Page<IfsReportWzqy> searchIfsReportWzqyPage(@RequestBody Map<String, String> map);

    SlOutBean ifsReportWzqyCreate(Map<String, String> map);
}
 