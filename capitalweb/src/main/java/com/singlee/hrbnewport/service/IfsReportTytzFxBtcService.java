package com.singlee.hrbnewport.service;

import com.singlee.hrbnewport.model.IfsReportTytzFxBtc;

import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 16:21
 * @description：${description}
 * @modified By：
 * @version:   
 */
public interface IfsReportTytzFxBtcService extends CommonExportService{


    int insert(IfsReportTytzFxBtc record);

    int insertSelective(IfsReportTytzFxBtc record);

    List<IfsReportTytzFxBtc> getDate(Map<String,Object> map);

}
