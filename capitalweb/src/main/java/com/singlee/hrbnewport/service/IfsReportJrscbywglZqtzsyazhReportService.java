package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportJrscbywglZqtzsyazh;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;


/**
 * @author Administrator
 * <p>
 * 债券投资收益结构表（按会计账户）
 */
@Service
public interface IfsReportJrscbywglZqtzsyazhReportService extends CommonExportService {

    Page<IfsReportJrscbywglZqtzsyazh> searchIfsReportJrscbywglZqtzsyazhPage(@RequestBody Map<String, String> map);

    SlOutBean ifsReportJrscbywglZqtzsyazhCreate(Map<String, String> map);
}
 