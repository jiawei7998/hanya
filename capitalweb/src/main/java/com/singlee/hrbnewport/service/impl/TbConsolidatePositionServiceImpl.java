package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbnewport.mapper.TbConsolidatePositionMapper;
import com.singlee.hrbnewport.model.TbConsolidatePosition;
import com.singlee.hrbnewport.service.TbConsolidatePositionService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TbConsolidatePositionServiceImpl implements TbConsolidatePositionService{

    @Autowired
    private TbConsolidatePositionMapper tbConsolidatePositionMapper;


    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String, Object> result = new HashMap<String, Object>();
        Map<String, String> map1=new HashMap<>();
        Map<String, String> map2=new HashMap<>();
        String queryDate = ParameterUtil.getString(map,"queryDate", DateUtil.getCurrentDateAsString());
        String dealtype = ParameterUtil.getString(map,"sheetType", "");
        List<TbConsolidatePosition> list1 = new ArrayList<TbConsolidatePosition>();
        List<TbConsolidatePosition> list2 = new ArrayList<TbConsolidatePosition>();
        if("fund".equals(dealtype)){
            map1.put("fundside","pay");
            map1.put("dealtype",dealtype);
            map2.put("fundside","sell");
            map2.put("dealtype",dealtype);
            list1=tbConsolidatePositionMapper.getConsolidatePosition1(map1);
            list2=tbConsolidatePositionMapper.getConsolidatePosition1(map2);
        }else{
            map1.put("ps","P");
            map1.put("invtype",dealtype);
            map2.put("ps","S");
            map2.put("invtype",dealtype);
            list1=tbConsolidatePositionMapper.getConsolidatePosition(map1);
            list2=tbConsolidatePositionMapper.getConsolidatePosition(map2);
        }
        result.put("list1",list1);
        result.put("list2",list2);
//        result.put("instName","哈尔滨银行股份有限公司");
        result.put("year",queryDate.substring(0,4));
        result.put("month",queryDate.substring(5,7));
//        result.put("day",queryDate.substring(8,10));
        return result;

    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<TbConsolidatePosition> selectReport(Map<String, String> map) {
        Map<String, String> mapEnd = new HashMap<String, String >();
        //分页
        RowBounds rb = ParameterUtil.getRowBounds(map);
        //投资类型
        String busType=map.get("dealtype");
        //基金投资方向
        String fundside = map.get("fundside");
        //债券方向
        String ps=map.get("ps");
        if ("fund".equals(busType)){
            mapEnd.put("bustype","2");
            mapEnd.put("fundside",fundside);
        }else {
            mapEnd.put("bustype","1");
            mapEnd.put("invtype",busType);
            mapEnd.put("ps",ps);
        }
        mapEnd.put("datadate",map.get("queryMonth"));
        //调用存储过程生成数据
        tbConsolidatePositionMapper.ConsolidatePositionCreate(mapEnd);
        Page<TbConsolidatePosition> result= null;
        if ("fund".equals(busType)){
            result=tbConsolidatePositionMapper.getConsolidatePosition1(mapEnd,rb);
        }else{
            result=tbConsolidatePositionMapper.getConsolidatePosition(mapEnd,rb);
        }
        return result;
    }
}
