package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.mapper.IfsReportTytzZfWtycrMapper;
import com.singlee.hrbnewport.model.IfsReportTytzZfWtycr;
import com.singlee.hrbnewport.service.IfsReportTytzZfWtycrService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 16:22
 * @description：${description}
 * @modified By：
 * @version:   
 */
@Service
public class IfsReportTytzZfWtycrServiceImpl implements IfsReportTytzZfWtycrService{

    @Resource
    private IfsReportTytzZfWtycrMapper ifsReportTytzZfWtycrMapper;

    @Override
    public int insert(IfsReportTytzZfWtycr record) {
        return ifsReportTytzZfWtycrMapper.insert(record);
    }

    @Override
    public int insertSelective(IfsReportTytzZfWtycr record) {
        return ifsReportTytzZfWtycrMapper.insertSelective(record);
    }

    @Override
    public List<IfsReportTytzZfWtycr> getDate(Map<String, Object> map) {
        ifsReportTytzZfWtycrMapper.call(map);
        return ifsReportTytzZfWtycrMapper.getDate(map);
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String,Object> mapEnd=new HashMap<>();
        List<IfsReportTytzZfWtycr> list=ifsReportTytzZfWtycrMapper.getDate(map);
        mapEnd.put("list",list);
        return mapEnd;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}
