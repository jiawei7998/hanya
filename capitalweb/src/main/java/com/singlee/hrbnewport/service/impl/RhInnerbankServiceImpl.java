package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbnewport.mapper.*;
import com.singlee.hrbnewport.service.RhInnerbankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 人行-同业业务管理报告
 * 2021/12/9
 * @Auther:zk
 */
@Service
public class RhInnerbankServiceImpl implements RhInnerbankService {

    @Autowired
    RhInnerbankCdIssMapper rhInnerbankCdIssMapper;
    @Autowired
    RhInnerbankCdTposMapper rhInnerbankCdTposMapper;
    @Autowired
    RhInnerbankDepositMapper rhInnerbankDepositMapper;
    @Autowired
    RhInnerbankRepoMapper rhInnerbankRepoMapper;
    @Autowired
    RhInnerbankSecTposMapper rhInnerbankSecTposMapper;
    @Autowired
    RhInnerbankRvrepoMapper rhInnerbankRvrepoMapper;


    /***********************页面查询*****************************/
    @Override
    public Page<Map<String, Object>> searchAllPage(Map<String, Object> map) {
        String sheetType = ParameterUtil.getString(map, "sheetType", "pub_fund");
        Object temDate = map.get("queryDate");
        String queryDate = temDate!=null&&!"".equals(temDate.toString().trim())?temDate.toString().trim(): DateUtil.getCurrentDateAsString();
        map.put("queryDate", queryDate);
        Page<Map<String, Object>> result = null;
        switch (sheetType){
            case "1":
                rhInnerbankCdIssMapper.createData(map);
                result = rhInnerbankCdIssMapper.searchAllPage(map,ParameterUtil.getRowBounds(map));
                break;
            case "2":
                rhInnerbankCdTposMapper.createData(map);
                result = rhInnerbankCdTposMapper.searchAllPage(map,ParameterUtil.getRowBounds(map));
                break;
            case "3":
                map.put("dealDir","CT");
                rhInnerbankDepositMapper.createData(map);
                result = rhInnerbankDepositMapper.searchAllPage(map,ParameterUtil.getRowBounds(map));
                break;
            case "4":
                map.put("dealDir","TC");
                rhInnerbankDepositMapper.createData(map);
                result = rhInnerbankDepositMapper.searchAllPage(map,ParameterUtil.getRowBounds(map));
                break;
            case "5":
                rhInnerbankRepoMapper.createData(map);
                result = rhInnerbankRepoMapper.searchAllPage(map,ParameterUtil.getRowBounds(map));
                break;
            case "6":
                rhInnerbankSecTposMapper.createData(map);
                result = rhInnerbankSecTposMapper.searchAllPage(map,ParameterUtil.getRowBounds(map));
                break;
            case "7":
                rhInnerbankRvrepoMapper.createData(map);
                result = rhInnerbankRvrepoMapper.searchAllPage(map,ParameterUtil.getRowBounds(map));
                break;
        }
        return result;
    }

    /***********************导出方法*****************************/
    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        HashMap<String, Object> resultmap = new HashMap<>();
        String sheetType = ParameterUtil.getString(map, "sheetType", "1");
        List<Map<String, Object>> result = null;
        switch (sheetType){
            case "1":
                rhInnerbankCdIssMapper.createData(map);
                result = rhInnerbankCdIssMapper.searchAll(map);
                break;
            case "2":
                rhInnerbankCdTposMapper.createData(map);
                result = rhInnerbankCdTposMapper.searchAll(map);
                break;
            case "3":
                map.put("dealDir","CT");
                rhInnerbankDepositMapper.createData(map);
                result = rhInnerbankDepositMapper.searchAll(map);
                break;
            case "4":
                map.put("dealDir","TC");
                rhInnerbankDepositMapper.createData(map);
                result = rhInnerbankDepositMapper.searchAll(map);
                break;
            case "5":
                rhInnerbankRepoMapper.createData(map);
                result = rhInnerbankRepoMapper.searchAll(map);
                break;
            case "6":
                rhInnerbankSecTposMapper.createData(map);
                result = rhInnerbankSecTposMapper.searchAll(map);
                break;
            case "7":
                rhInnerbankRvrepoMapper.createData(map);
                result = rhInnerbankRvrepoMapper.searchAll(map);
                break;
        }
        String queryDate = ParameterUtil.getString(map,"queryDate", DateUtil.getCurrentDateAsString());
        resultmap.put("year",queryDate.substring(0,4));
        resultmap.put("month",queryDate.substring(5,7));
        resultmap.put("list",result);
        return resultmap;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}
