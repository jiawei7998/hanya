package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.hrbnewport.mapper.IfsReportZqtzywmsdsdjbMapper;
import com.singlee.hrbnewport.model.IfsReportZqtzywmsdsdjb;
import com.singlee.hrbnewport.service.IfsReportZqtzywmsdsdjbReportService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author Administrator
 * <p>
 * 债券投资业务免所得税登记表
 */
@Service
public class IfsReportZqtzywmsdsdjbReportServiceImpl implements IfsReportZqtzywmsdsdjbReportService {


    @Autowired
    private IfsReportZqtzywmsdsdjbMapper ifsReportZqtzywmsdsdjbMapper;

    private static Logger logger = Logger.getLogger(IfsReportZqtzywmsdsdjbReportServiceImpl.class);


    @Override
    public Page<IfsReportZqtzywmsdsdjb> searchIfsReportZqtzywmsdsdjbPage(Map<String, String> map) {
        int pageNumber = ParameterUtil.getInt(map, "pageNumber", 10);
        int pageSize = ParameterUtil.getInt(map, "pageSize", 1);
        List<IfsReportZqtzywmsdsdjb> ifsReportZqtzywmsdsdjbList = ifsReportZqtzywmsdsdjbMapper.selectAll();
        return HrbReportUtils.producePage(ifsReportZqtzywmsdsdjbList, pageNumber, pageSize);
    }

    @Override
    public SlOutBean ifsReportZqtzywmsdsdjbCreate(Map<String, String> map) {
        SlOutBean msg = new SlOutBean(RetStatusEnum.S, "error.common.0000", "【债券投资业务免所得税登记表】生成成功！");
        ifsReportZqtzywmsdsdjbMapper.ifsReportZqtzywmsdsdjbCreate(map);
        String RETCODE = map.get("RETCODE");
        if (!"999".equals(RETCODE)) {
            logger.info("【债券投资业务免所得税登记表】生成失败!");
            return new SlOutBean(RetStatusEnum.F, RETCODE, "【债券投资业务免所得税登记表】生成失败!");
        }
        msg.setRetCode("999");
        msg.setRetMsg("【债券投资业务免所得税登记表】生成成功！");
        logger.info("【债券投资业务免所得税登记表】生成成功！");
        return msg;
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        List<IfsReportZqtzywmsdsdjb> ifsReportZqtzywmsdsdjbList = ifsReportZqtzywmsdsdjbMapper.selectAll();
        Map<String, Object> rustmap = new HashMap<>();
        rustmap.put("list",ifsReportZqtzywmsdsdjbList);
        return rustmap;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}