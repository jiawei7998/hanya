package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.mapper.IfsReportZdsjgljxMapper;
import com.singlee.hrbnewport.model.IfsReportZdsjgljx;
import com.singlee.hrbnewport.service.IfsReportZdsjgljxService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 20:52
 * @description：${description}
 * @modified By：
 * @version:   
 */
@Service
public class IfsReportZdsjgljxServiceImpl implements IfsReportZdsjgljxService{

    @Resource
    private IfsReportZdsjgljxMapper ifsReportZdsjgljxMapper;

    @Override
    public int insert(IfsReportZdsjgljx record) {
        return ifsReportZdsjgljxMapper.insert(record);
    }

    @Override
    public int insertSelective(IfsReportZdsjgljx record) {
        return ifsReportZdsjgljxMapper.insertSelective(record);
    }

    @Override
    public List<IfsReportZdsjgljx> getDate(Map<String, Object> map) {
        ifsReportZdsjgljxMapper.call(map);
        return ifsReportZdsjgljxMapper.getDate(map);

    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String,Object> mapEnd=new HashMap<>();
        List<IfsReportZdsjgljx> list=ifsReportZdsjgljxMapper.getDate(map);
        mapEnd.put("list",list);
        return mapEnd;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}
