package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportCkhyhtyhlhcffzJwjgckb;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;


/**
 * @author Administrator
 * <p>
 * 存款含银行同业和联行存放负债 境外机构存款表
 */
@Service
public interface IfsReportCkhyhtyhlhcffzJwjgckbReportService extends CommonExportService {

    Page<IfsReportCkhyhtyhlhcffzJwjgckb> searchIfsReportCkhyhtyhlhcffzJwjgckbPage(@RequestBody Map<String, String> map);

    SlOutBean ifsReportCkhyhtyhlhcffzJwjgckbCreate(Map<String, String> map);
}
 