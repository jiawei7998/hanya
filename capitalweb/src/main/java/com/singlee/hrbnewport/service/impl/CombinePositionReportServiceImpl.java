package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbnewport.mapper.CombinePositionReportMapper;
import com.singlee.hrbnewport.model.CombinePositionVO;
import com.singlee.hrbnewport.service.CombinePositionReportService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Liang
 * @date 2021/11/17 13:12
 * =======================
 */
@Service
@Transactional(value = "transactionManager", rollbackFor = Exception.class)
public class CombinePositionReportServiceImpl implements CombinePositionReportService {

    @Autowired
    private CombinePositionReportMapper combinePositionReportMapper;

    @Override
    public Page<CombinePositionVO> selectBondReport(Map<String, String> map) {
        Map<String, Object> mapEnd = new HashMap<String, Object>();
        //分页
        RowBounds rb = ParameterUtil.getRowBounds(map);
        //投资类型
        String busType=map.get("dealtype");
        //基金投资方向
        String fundside = map.get("fundside");
        //债券方向
        String ps=map.get("ps");
        if ("S".equals(ps)){
            mapEnd.put("dealtext","REDEMPTION");
        }
        //查询时间
        String postdate=map.get("queryMonth");
        mapEnd.put("busType",busType);
        mapEnd.put("ps",ps);
        mapEnd.put("postdate",postdate);

        Page<CombinePositionVO> result=null;
        if ("I".equals(busType)){
            result=combinePositionReportMapper.selectCDBond(mapEnd,rb);
        }else if ("fund".equals(busType)&&"pay".equals(fundside)){
            result=combinePositionReportMapper.queryFundPay(mapEnd,rb);
        }else if ("fund".equals(busType)&&"sell".equals(fundside)){
            result=combinePositionReportMapper.queryFundSell(mapEnd,rb);
        }else {
            result=combinePositionReportMapper.selectBond(mapEnd,rb);
        }
        return result;
    }


    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {

        
        return null;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }


}
