package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.mapper.IfsReportTytzWctMapper;
import com.singlee.hrbnewport.model.IfsReportTytzWct;
import com.singlee.hrbnewport.service.IfsReportTytzWctService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ：chenguo
 * @date ：Created in 2021/12/3 15:28
 * @description：${description}
 * @modified By：
 * @version:
 */
@Service
public class IfsReportTytzWctServiceImpl implements IfsReportTytzWctService {

    @Resource
    private IfsReportTytzWctMapper ifsReportTytzWctMapper;

    @Override
    public int insert(IfsReportTytzWct record) {
        return ifsReportTytzWctMapper.insert(record);
    }

    @Override
    public int insertSelective(IfsReportTytzWct record) {
        return ifsReportTytzWctMapper.insertSelective(record);
    }

    @Override
    public List<IfsReportTytzWct> getDate(Map<String, Object> map) {
        ifsReportTytzWctMapper.call(map);
        return ifsReportTytzWctMapper.getDate(map);
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String,Object> mapEnd=new HashMap<>();
        List<IfsReportTytzWct> list=ifsReportTytzWctMapper.getDate(map);
        mapEnd.put("list",list);
        return mapEnd;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}

