package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.financial.common.util.ParameterUtil;
import com.singlee.hrbnewport.mapper.TbZyzjbalanceMapper;
import com.singlee.hrbnewport.model.TbZyzjbalance;
import com.singlee.hrbnewport.service.TbZyzjbalanceService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TbZyzjbalanceServiceImpl implements TbZyzjbalanceService{

    @Resource
    private TbZyzjbalanceMapper tbZyzjbalanceMapper;


    @Override
    public Page<TbZyzjbalance> selectReport(Map<String, String> map) {
        RowBounds rb= ParameterUtil.getRowBounds(map);
        tbZyzjbalanceMapper.ZYZJBALANCEReportCreate(map);
        Page<TbZyzjbalance> result=tbZyzjbalanceMapper.getTbZYZJBALANCE(rb);
        return result;
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String, Object> result = new HashMap<String, Object>();
        String queryDate = com.singlee.capital.common.util.ParameterUtil.getString(map,"queryDate", DateUtil.getCurrentDateAsString());
        map.put("queryDate",queryDate);
        List<TbZyzjbalance> list=tbZyzjbalanceMapper.getTbZYZJBALANCE2();
        result.put("list",list);
//        result.put("instName","哈尔滨银行股份有限公司");
        result.put("year",queryDate.substring(0,4));
        result.put("month",queryDate.substring(5,7));
        result.put("day",queryDate.substring(8,10));
        return result;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}
