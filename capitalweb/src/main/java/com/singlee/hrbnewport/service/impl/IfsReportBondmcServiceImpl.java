package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbnewport.mapper.IfsReportBondmcMapper;
import com.singlee.hrbnewport.model.IfsReportBondmc;
import com.singlee.hrbnewport.service.IfsReportBondmcService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class IfsReportBondmcServiceImpl implements IfsReportBondmcService{

    @Autowired
    private IfsReportBondmcMapper ifsReportBondmcMapper;


    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String, Object> result = new HashMap<String, Object>();
        String edate = ParameterUtil.getString(map,"queryDate", DateUtil.getCurrentDateAsString());
        String sdate = ParameterUtil.getString(map,"sdate", DateUtil.getCurrentDateAsString());
        List<IfsReportBondmc> list=ifsReportBondmcMapper.getBondMC();
        result.put("sdate",sdate);
        result.put("edate",edate);
        result.put("list",list);
        return result;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<IfsReportBondmc> selectReport(Map<String, String> map) {
        RowBounds rb = ParameterUtil.getRowBounds(map);
        ifsReportBondmcMapper.ReportBondMCCreate(map);
        return ifsReportBondmcMapper.getBondMC(rb);
    }
}
