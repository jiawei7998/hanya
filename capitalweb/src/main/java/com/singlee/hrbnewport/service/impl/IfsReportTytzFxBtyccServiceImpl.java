package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.mapper.IfsReportTytzFxBtyccMapper;
import com.singlee.hrbnewport.model.IfsReportTytzFxBtycc;
import com.singlee.hrbnewport.service.IfsReportTytzFxBtyccService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 16:33
 * @description：${description}
 * @modified By：
 * @version:   
 */
@Service
public class IfsReportTytzFxBtyccServiceImpl implements IfsReportTytzFxBtyccService{

    @Resource
    private IfsReportTytzFxBtyccMapper ifsReportTytzFxBtyccMapper;

    @Override
    public int insert(IfsReportTytzFxBtycc record) {
        return ifsReportTytzFxBtyccMapper.insert(record);
    }

    @Override
    public int insertSelective(IfsReportTytzFxBtycc record) {
        return ifsReportTytzFxBtyccMapper.insertSelective(record);
    }

    @Override
    public List<IfsReportTytzFxBtycc> getDate(Map<String, Object> map) {
        ifsReportTytzFxBtyccMapper.call(map);
        return ifsReportTytzFxBtyccMapper.getDate(map);
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String,Object> mapEnd=new HashMap<>();
        List<IfsReportTytzFxBtycc> list=ifsReportTytzFxBtyccMapper.getDate(map);
        mapEnd.put("list",list);
        return mapEnd;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}
