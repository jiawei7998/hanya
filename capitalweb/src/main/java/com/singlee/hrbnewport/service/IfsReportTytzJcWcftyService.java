package com.singlee.hrbnewport.service;

import com.singlee.hrbnewport.model.IfsReportTytzJcWcfty;

import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 16:38
 * @description：${description}
 * @modified By：
 * @version:   
 */
public interface IfsReportTytzJcWcftyService extends CommonExportService{


    int insert(IfsReportTytzJcWcfty record);

    int insertSelective(IfsReportTytzJcWcfty record);

    List<IfsReportTytzJcWcfty> getDate(Map<String,Object> map);
}
