package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.mapper.IfsReportTytzWtycrMapper;
import com.singlee.hrbnewport.model.IfsReportTytzWtycr;
import com.singlee.hrbnewport.service.IfsReportTytzWtycrService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/3 15:37
 * @description：${description}
 * @modified By：
 * @version:   
 */
@Service
public class IfsReportTytzWtycrServiceImpl implements IfsReportTytzWtycrService{

    @Resource
    private IfsReportTytzWtycrMapper ifsReportTytzWtycrMapper;

    @Override
    public int insert(IfsReportTytzWtycr record) {
        return ifsReportTytzWtycrMapper.insert(record);
    }

    @Override
    public int insertSelective(IfsReportTytzWtycr record) {
        return ifsReportTytzWtycrMapper.insertSelective(record);
    }

    @Override
    public List<IfsReportTytzWtycr> getDate(Map<String, Object> map) {
        ifsReportTytzWtycrMapper.call(map);
        return ifsReportTytzWtycrMapper.getDate(map);
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String,Object> mapEnd=new HashMap<>();
        List<IfsReportTytzWtycr> list=ifsReportTytzWtycrMapper.getDate(map);
        mapEnd.put("list",list);
        return mapEnd;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}
