package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbnewport.mapper.IfsReportZyzctfGbmMapper;
import com.singlee.hrbnewport.model.IfsReportZyzctfGbm;
import com.singlee.hrbnewport.service.IfsReportZyzctfGbmService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/8 20:17
 * @description：${description}
 * @modified By：
 * @version:   
 */
@Service
public class IfsReportZyzctfGbmServiceImpl implements IfsReportZyzctfGbmService{

    @Resource
    private IfsReportZyzctfGbmMapper ifsReportZyzctfGbmMapper;

    @Override
    public int insert(IfsReportZyzctfGbm record) {
        return ifsReportZyzctfGbmMapper.insert(record);
    }

    @Override
    public int insertSelective(IfsReportZyzctfGbm record) {
        return ifsReportZyzctfGbmMapper.insertSelective(record);
    }

    @Override
    public List<Map<String, Object>> getDate(Map<String, Object> map) {
        String sheetType = ParameterUtil.getString(map, "dealType", "");
        ifsReportZyzctfGbmMapper.call(map);
        List<Map<String, Object>> list=null;
        if (sheetType.equals("FZ")){

            list= ifsReportZyzctfGbmMapper.getDate3(map);
        }else if (sheetType.equals("GBM")){

           list= ifsReportZyzctfGbmMapper.getDate(map);
        }else if (sheetType.equals("KH")){
            list= ifsReportZyzctfGbmMapper.getDate2(map);
        }

          return list;
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String, Object> mapEnd = new HashMap<>();
        String sheetType = ParameterUtil.getString(map, "sheetType", "");
        if (sheetType.equals("FZ")){
            List<Map<String, Object>> list= ifsReportZyzctfGbmMapper.getDate3(map);
            for (int i = 0; i < list.size(); i++) {
                Map<String, Object> paramer = list.get(i);
                mapEnd.put("mapEnd" + ParameterUtil.getString(paramer, "PX", ""), paramer);
            }
        }else if (sheetType.equals("GBM")){
            List<Map<String, Object>> list= ifsReportZyzctfGbmMapper.getDate(map);
            for (int i = 0; i < list.size(); i++) {
                Map<String, Object> paramer = list.get(i);
                mapEnd.put("mapEnd" + ParameterUtil.getString(paramer, "PX", ""), paramer);
            }
        }else if (sheetType.equals("KH")){
            List<Map<String, Object>> list= ifsReportZyzctfGbmMapper.getDate2(map);
            for (int i = 0; i < list.size(); i++) {
                Map<String, Object> paramer = list.get(i);
                mapEnd.put("mapEnd" + ParameterUtil.getString(paramer, "PX", ""), paramer);
            }
        }
        return mapEnd;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}
