package com.singlee.hrbnewport.service;

import com.singlee.hrbnewport.model.IfsReportZdsjgljx;

import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 20:52
 * @description：${description}
 * @modified By：
 * @version:   
 */
public interface IfsReportZdsjgljxService extends CommonExportService{


    int insert(IfsReportZdsjgljx record);

    int insertSelective(IfsReportZdsjgljx record);

    List<IfsReportZdsjgljx> getDate(Map<String,Object> map);

}
