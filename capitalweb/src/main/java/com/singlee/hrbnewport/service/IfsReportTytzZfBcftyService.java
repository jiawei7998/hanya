package com.singlee.hrbnewport.service;

import com.singlee.hrbnewport.model.IfsReportTytzZfBcfty;

import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 16:22
 * @description：${description}
 * @modified By：
 * @version:   
 */
public interface IfsReportTytzZfBcftyService extends CommonExportService{


    int insert(IfsReportTytzZfBcfty record);

    int insertSelective(IfsReportTytzZfBcfty record);


    List<IfsReportTytzZfBcfty> getDate(Map<String,Object> map);

}
