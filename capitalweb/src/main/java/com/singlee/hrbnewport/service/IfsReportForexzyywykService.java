package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.model.IfsReportForexzyywyk;

import java.util.Map;

public interface IfsReportForexzyywykService extends CommonExportService{


    Page<IfsReportForexzyywyk> selectReport(Map<String,String > map);

}
