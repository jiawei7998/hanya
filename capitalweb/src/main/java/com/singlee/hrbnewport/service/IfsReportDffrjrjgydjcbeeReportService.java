package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportDffrjrjgydjcbee;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;


/**
 * @author Administrator
 * <p>
 * 地方法人金融机构月度监测表（表二）
 */
@Service
public interface IfsReportDffrjrjgydjcbeeReportService extends CommonExportService {

    Page<IfsReportDffrjrjgydjcbee> searchIfsReportDffrjrjgydjcbeePage(@RequestBody Map<String, String> map);

    SlOutBean ifsReportDffrjrjgydjcbeeCreate(Map<String, String> map);
}
 