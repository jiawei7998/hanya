package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportDkcfhcfyhtyjlhzcbAgbabz;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;


/**
 * @author Administrator
 * <p>
 * 贷款拆放含拆放银行同业及联行资产表 按国别按币种
 */
@Service
public interface IfsReportDkcfhcfyhtyjlhzcbAgbabzReportService extends CommonExportService {

    Page<IfsReportDkcfhcfyhtyjlhzcbAgbabz> searchIfsReportDkcfhcfyhtyjlhzcbAgbabzPage(@RequestBody Map<String, String> map);

    SlOutBean ifsReportDkcfhcfyhtyjlhzcbAgbabzCreate(Map<String, String> map);
}
 