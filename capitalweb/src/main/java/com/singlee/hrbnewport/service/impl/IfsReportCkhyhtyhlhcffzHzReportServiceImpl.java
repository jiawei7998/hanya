package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.hrbnewport.mapper.IfsReportCkhyhtyhlhcffzHzMapper;
import com.singlee.hrbnewport.model.IfsReportCkhyhtyhlhcffzHz;
import com.singlee.hrbnewport.service.IfsReportCkhyhtyhlhcffzHzReportService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author Administrator
 * <p>
 * 存款含银行同业和联行存放负债 汇总
 */
@Service
public class IfsReportCkhyhtyhlhcffzHzReportServiceImpl implements IfsReportCkhyhtyhlhcffzHzReportService {


    @Autowired
    private IfsReportCkhyhtyhlhcffzHzMapper ifsReportCkhyhtyhlhcffzHzMapper;

    private static Logger logger = Logger.getLogger(IfsReportCkhyhtyhlhcffzHzReportServiceImpl.class);


    @Override
    public Page<IfsReportCkhyhtyhlhcffzHz> searchIfsReportCkhyhtyhlhcffzHzPage(Map<String, String> map) {
        int pageNumber = ParameterUtil.getInt(map, "pageNumber", 10);
        int pageSize = ParameterUtil.getInt(map, "pageSize", 1);
        List<IfsReportCkhyhtyhlhcffzHz> ifsReportCkhyhtyhlhcffzHzList = ifsReportCkhyhtyhlhcffzHzMapper.selectAll();
        return HrbReportUtils.producePage(ifsReportCkhyhtyhlhcffzHzList, pageNumber, pageSize);
    }

    @Override
    public SlOutBean ifsReportCkhyhtyhlhcffzHzCreate(Map<String, String> map) {
        SlOutBean msg = new SlOutBean(RetStatusEnum.S, "error.common.0000", "【存款含银行同业和联行存放负债 汇总】生成成功！");
        ifsReportCkhyhtyhlhcffzHzMapper.ifsReportCkhyhtyhlhcffzHzCreate(map);
        String RETCODE = map.get("RETCODE");
        if (!"999".equals(RETCODE)) {
            logger.info("【存款含银行同业和联行存放负债 汇总】生成失败!");
            return new SlOutBean(RetStatusEnum.F, RETCODE, "【存款含银行同业和联行存放负债 汇总】生成失败!");
        }
        msg.setRetCode("999");
        msg.setRetMsg("【存款含银行同业和联行存放负债 汇总】生成成功！");
        logger.info("【存款含银行同业和联行存放负债 汇总】生成成功！");
        return msg;
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        List<IfsReportCkhyhtyhlhcffzHz> ifsReportCkhyhtyhlhcffzHzList = ifsReportCkhyhtyhlhcffzHzMapper.selectAll();
        Map<String, Object> rustmap = new HashMap<>();
        rustmap.put("list",ifsReportCkhyhtyhlhcffzHzList);
        return rustmap;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}