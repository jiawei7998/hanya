package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.hrbnewport.mapper.IfsReportG2400Mapper;
import com.singlee.hrbnewport.model.IfsReportG2400;
import com.singlee.hrbnewport.service.IfsReportG2400ReportService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author Administrator
 * <p>
 * G24最大百家金融机构同业融入情况表
 */
@Service
public class IfsReportG2400ReportServiceImpl implements IfsReportG2400ReportService {


    @Autowired
    private IfsReportG2400Mapper ifsReportG2400Mapper;

    private static Logger logger = Logger.getLogger(IfsReportG2400ReportServiceImpl.class);


    @Override
    public Page<IfsReportG2400> searchIfsReportG2400Page(Map<String, String> map) {
        int pageNumber = ParameterUtil.getInt(map, "pageNumber", 10);
        int pageSize = ParameterUtil.getInt(map, "pageSize", 1);
        List<IfsReportG2400> ifsReportG2400List = ifsReportG2400Mapper.selectAll();
        return HrbReportUtils.producePage(ifsReportG2400List, pageNumber, pageSize);
    }

    @Override
    public SlOutBean ifsReportG2400Create(Map<String, String> map) {
        SlOutBean msg = new SlOutBean(RetStatusEnum.S, "error.common.0000", "【G24-最大百家金融机构同业融入情况表】生成成功！");
        ifsReportG2400Mapper.ifsReportG2400Create(map);
        String RETCODE = map.get("RETCODE");
        if (!"999".equals(RETCODE)) {
            logger.info("【G24最大百家金融机构同业融入情况表】生成失败!");
            return new SlOutBean(RetStatusEnum.F, RETCODE, "【G24-最大百家金融机构同业融入情况表】生成失败!");
        }
        msg.setRetCode("999");
        msg.setRetMsg("【G24-最大百家金融机构同业融入情况表】生成成功！");
        logger.info("【G24-最大百家金融机构同业融入情况表】生成成功！");
        return msg;
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        String queryDate = ParameterUtil.getString(map,"queryDate","");
        queryDate = DateUtil.format(DateUtil.parse(queryDate), "yyyy年MM月dd日");
        List<IfsReportG2400> ifsReportG2400List = ifsReportG2400Mapper.selectAll();
        Map<String, Object> rustmap = new HashMap<>();
        rustmap.put("list",ifsReportG2400List);
        rustmap.put("tbjg","哈尔滨银行股份有限公司");
        rustmap.put("bbrq",queryDate);
        return rustmap;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}