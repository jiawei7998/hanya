package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.model.TbDealInformation;

import java.util.Map;

public interface TbDealInformationService extends CommonExportService{

    Page<TbDealInformation> selectReport(Map<String,String > map);


}
