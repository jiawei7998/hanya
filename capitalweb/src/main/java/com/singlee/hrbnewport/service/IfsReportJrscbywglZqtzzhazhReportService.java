package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportJrscbywglZqtzzhazh;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;


/**
 * @author Administrator
 * <p>
 * 债券投资账户结构表(按会计账户）
 */
@Service
public interface IfsReportJrscbywglZqtzzhazhReportService extends CommonExportService {

    Page<IfsReportJrscbywglZqtzzhazh> searchIfsReportJrscbywglZqtzzhazhPage(@RequestBody Map<String, String> map);

    SlOutBean ifsReportJrscbywglZqtzzhazhCreate(Map<String, String> map);
}
 