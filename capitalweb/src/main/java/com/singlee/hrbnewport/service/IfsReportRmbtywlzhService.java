package com.singlee.hrbnewport.service;

import com.singlee.hrbnewport.model.IfsReportRmbtywlzh;

import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 20:51
 * @description：${description}
 * @modified By：
 * @version:   
 */
public interface IfsReportRmbtywlzhService extends CommonExportService{




    List<IfsReportRmbtywlzh> getDate(Map<String,Object> map);

    void  upDate(IfsReportRmbtywlzh record);

}

