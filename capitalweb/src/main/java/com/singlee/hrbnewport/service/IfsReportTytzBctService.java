package com.singlee.hrbnewport.service;

import com.singlee.hrbnewport.model.IfsReportTytzBct;

import java.util.List;
import java.util.Map;

/**
 * @author ：chenguo
 * @date ：Created in 2021/12/3 15:27
 * @description：${description}
 * @modified By：
 * @version:
 */
public interface IfsReportTytzBctService  extends CommonExportService{


    int insert(IfsReportTytzBct record);

    int insertSelective(IfsReportTytzBct record);

    List<IfsReportTytzBct>  getDate(Map<String,Object> map);

//    void call(Map<String,Object> map);

}

