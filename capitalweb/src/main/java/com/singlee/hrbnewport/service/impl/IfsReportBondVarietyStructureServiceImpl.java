package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.mapper.IfsReportBondVarietyStructureMapper;
import com.singlee.hrbnewport.service.IfsReportBondVarietyStructureService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class IfsReportBondVarietyStructureServiceImpl implements IfsReportBondVarietyStructureService{

    @Resource
    private IfsReportBondVarietyStructureMapper ifsReportBondVarietyStructureMapper;


    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String, Object> result = new HashMap<String, Object>();
        ifsReportBondVarietyStructureMapper.generateData(map);
        List<Map<String, Object>> mapList = ifsReportBondVarietyStructureMapper.getData(map);
        for(int i =0;i<mapList.size();i++){
            result.put("map"+(i+1),mapList.get(i));
        }
        return result;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        ifsReportBondVarietyStructureMapper.generateData(map);
        return ifsReportBondVarietyStructureMapper.getData(map);
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}
