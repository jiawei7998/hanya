package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportJrscbywglZcfz;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;


/**
 * @author Administrator
 * <p>
 * 资产负债数据表
 */
@Service
public interface IfsReportJrscbywglZcfzReportService extends CommonExportService {

    Page<IfsReportJrscbywglZcfz> searchIfsReportJrscbywglZcfzPage(@RequestBody Map<String, String> map);

    SlOutBean ifsReportJrscbywglZcfzCreate(Map<String, String> map);
}
 