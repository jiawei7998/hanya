package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.mapper.IfsReportTytzJcBtyccMapper;
import com.singlee.hrbnewport.model.IfsReportTytzJcBtycc;
import com.singlee.hrbnewport.service.IfsReportTytzJcBtyccService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ：chenguo
 * @date ：Created in 2021/12/6 16:20
 * @description：${description}
 * @modified By：
 * @version:
 */
@Service
public class IfsReportTytzJcBtyccServiceImpl implements IfsReportTytzJcBtyccService {

    @Resource
    private IfsReportTytzJcBtyccMapper ifsReportTytzJcBtyccMapper;

    @Override
    public int insert(IfsReportTytzJcBtycc record) {
        return ifsReportTytzJcBtyccMapper.insert(record);
    }

    @Override
    public int insertSelective(IfsReportTytzJcBtycc record) {
        return ifsReportTytzJcBtyccMapper.insertSelective(record);
    }

    @Override
    public List<IfsReportTytzJcBtycc> getDate(Map<String, Object> map) {
        ifsReportTytzJcBtyccMapper.call(map);
        return ifsReportTytzJcBtyccMapper.getDate(map);
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String,Object> mapEnd=new HashMap<>();
        List<IfsReportTytzJcBtycc> list=ifsReportTytzJcBtyccMapper.getDate(map);
        mapEnd.put("list",list);
        return mapEnd;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}

