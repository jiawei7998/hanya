package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportCkhyhtyhlhcffzHz;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;


/**
 * @author Administrator
 * <p>
 * 存款含银行同业和联行存放负债 汇总
 */
@Service
public interface IfsReportCkhyhtyhlhcffzHzReportService extends CommonExportService {

    Page<IfsReportCkhyhtyhlhcffzHz> searchIfsReportCkhyhtyhlhcffzHzPage(@RequestBody Map<String, String> map);

    SlOutBean ifsReportCkhyhtyhlhcffzHzCreate(Map<String, String> map);
}
 