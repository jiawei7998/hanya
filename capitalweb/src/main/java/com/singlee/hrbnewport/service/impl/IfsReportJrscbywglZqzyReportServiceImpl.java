package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.hrbnewport.mapper.IfsReportJrscbywglZqzyMapper;
import com.singlee.hrbnewport.model.IfsReportJrscbywglZqzy;
import com.singlee.hrbnewport.service.IfsReportJrscbywglZqzyReportService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author Administrator
 * <p>
 * 债券质押情况表
 */
@Service
public class IfsReportJrscbywglZqzyReportServiceImpl implements IfsReportJrscbywglZqzyReportService {


    @Autowired
    private IfsReportJrscbywglZqzyMapper ifsReportJrscbywglZqzyMapper;

    private static Logger logger = Logger.getLogger(IfsReportJrscbywglZqzyReportServiceImpl.class);


    @Override
    public Page<IfsReportJrscbywglZqzy> searchIfsReportJrscbywglZqzyPage(Map<String, String> map) {
        int pageNumber = ParameterUtil.getInt(map, "pageNumber", 10);
        int pageSize = ParameterUtil.getInt(map, "pageSize", 1);
        List<IfsReportJrscbywglZqzy> ifsReportJrscbywglZqzyList = ifsReportJrscbywglZqzyMapper.selectAll();
        for (IfsReportJrscbywglZqzy ifsReportJrscbywglZqzy : ifsReportJrscbywglZqzyList) {
            String[] productList = ifsReportJrscbywglZqzy.getProduct().split("_");
            if (productList.length > 0) {
                ifsReportJrscbywglZqzy.setProduct(productList[0]);
            }
            if (productList.length > 1) {
                ifsReportJrscbywglZqzy.setProducto(productList[1]);
            }
        }
        return HrbReportUtils.producePage(ifsReportJrscbywglZqzyList, pageNumber, pageSize);
    }

    @Override
    public SlOutBean ifsReportJrscbywglZqzyCreate(Map<String, String> map) {
        SlOutBean msg = new SlOutBean(RetStatusEnum.S, "error.common.0000", "【金融市场部业务管理报表-债券质押情况表】生成成功！");
        ifsReportJrscbywglZqzyMapper.ifsReportJrscbywglZqzyCreate(map);
        String RETCODE = map.get("RETCODE");
        if (!"999".equals(RETCODE)) {
            logger.info("【金融市场部业务管理报表-债券质押情况表】生成失败!");
            return new SlOutBean(RetStatusEnum.F, RETCODE, "【金融市场部业务管理报表-债券质押情况表】生成失败!");
        }
        msg.setRetCode("999");
        msg.setRetMsg("【金融市场部业务管理报表-债券质押情况表】生成成功！");
        logger.info("【金融市场部业务管理报表-债券质押情况表】生成成功！");
        return msg;
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        String queryDate = ParameterUtil.getString(map,"queryDate","");
        queryDate = DateUtil.format(DateUtil.parse(queryDate), "yyyy年MM月dd日");
        List<IfsReportJrscbywglZqzy> ifsReportJrscbywglZqzyList = ifsReportJrscbywglZqzyMapper.selectAll();
        Map<String, Object> rustmap = new HashMap<>();
        rustmap.put("list",ifsReportJrscbywglZqzyList);
        rustmap.put("bbrq",queryDate);
        return rustmap;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}