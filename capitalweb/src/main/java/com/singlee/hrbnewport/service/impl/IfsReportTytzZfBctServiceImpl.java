package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.mapper.IfsReportTytzZfBctMapper;
import com.singlee.hrbnewport.model.IfsReportTytzZfBct;
import com.singlee.hrbnewport.service.IfsReportTytzZfBctService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 16:22
 * @description：${description}
 * @modified By：
 * @version:   
 */
@Service
public class IfsReportTytzZfBctServiceImpl implements IfsReportTytzZfBctService{

    @Resource
    private IfsReportTytzZfBctMapper ifsReportTytzZfBctMapper;

    @Override
    public int insert(IfsReportTytzZfBct record) {
        return ifsReportTytzZfBctMapper.insert(record);
    }

    @Override
    public int insertSelective(IfsReportTytzZfBct record) {
        return ifsReportTytzZfBctMapper.insertSelective(record);
    }

    @Override
    public List<IfsReportTytzZfBct> getDate(Map<String, Object> map) {
        ifsReportTytzZfBctMapper.call(map);
        return ifsReportTytzZfBctMapper.getDate(map);
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String,Object> mapEnd=new HashMap<>();
        List<IfsReportTytzZfBct> list=ifsReportTytzZfBctMapper.getDate(map);
        mapEnd.put("list",list);
        return mapEnd;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}
