package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.model.TbZyzjbalance;

import java.util.Map;

public interface TbZyzjbalanceService extends CommonExportService{


    Page<TbZyzjbalance> selectReport(Map<String,String > map);

}
