package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportG3101;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;


/**
 * @author Administrator
 * <p>
 * G31投资业务情况表 底层资产投资情况
 */
@Service
public interface IfsReportG3101ReportService extends CommonExportService {

    Page<IfsReportG3101> searchIfsReportG3101Page(@RequestBody Map<String, String> map);

    SlOutBean ifsReportG3101Create(Map<String, String> map);
}
 