package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.hrbnewport.mapper.IfsReportRmbtyyeMapper;
import com.singlee.hrbnewport.model.IfsReportRmbtyye;
import com.singlee.hrbnewport.service.IfsReportRmbtyyeReportService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author Administrator
 * <p>
 * 人民币同业余额
 */
@Service
public class IfsReportRmbtyyeReportServiceImpl implements IfsReportRmbtyyeReportService {


    @Autowired
    private IfsReportRmbtyyeMapper ifsReportRmbtyyeMapper;

    private static Logger logger = Logger.getLogger(IfsReportRmbtyyeReportServiceImpl.class);


    @Override
    public Page<IfsReportRmbtyye> searchIfsReportRmbtyyePage(Map<String, String> map) {
        int pageNumber = ParameterUtil.getInt(map, "pageNumber", 10);
        int pageSize = ParameterUtil.getInt(map, "pageSize", 1);
        List<IfsReportRmbtyye> ifsReportRmbtyyeList = ifsReportRmbtyyeMapper.selectAll();
        return HrbReportUtils.producePage(ifsReportRmbtyyeList, pageNumber, pageSize);
    }

    @Override
    public SlOutBean ifsReportRmbtyyeCreate(Map<String, String> map) {
        SlOutBean msg = new SlOutBean(RetStatusEnum.S, "error.common.0000", "【人民币同业余额】生成成功！");
        ifsReportRmbtyyeMapper.ifsReportRmbtyyeCreate(map);
        String RETCODE = map.get("RETCODE");
        if (!"999".equals(RETCODE)) {
            logger.info("【人民币同业余额】生成失败!");
            return new SlOutBean(RetStatusEnum.F, RETCODE, "【人民币同业余额】生成失败!");
        }
        msg.setRetCode("999");
        msg.setRetMsg("【人民币同业余额】生成成功！");
        logger.info("【人民币同业余额】生成成功！");
        return msg;
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        List<IfsReportRmbtyye> ifsReportRmbtyyeList = ifsReportRmbtyyeMapper.selectAll();
        Map<String, Object> rustmap = new HashMap<>();
        rustmap.put("list",ifsReportRmbtyyeList);
        return rustmap;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}