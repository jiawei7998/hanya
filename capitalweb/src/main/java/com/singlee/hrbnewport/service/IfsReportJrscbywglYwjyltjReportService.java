package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportJrscbywglYwjyltj;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;


/**
 * @author Administrator
 * <p>
 * 业务交易量统计表
 */
@Service
public interface IfsReportJrscbywglYwjyltjReportService extends CommonExportService {

    Page<IfsReportJrscbywglYwjyltj> searchIfsReportJrscbywglYwjyltjPage(@RequestBody Map<String, String> map);

    SlOutBean ifsReportJrscbywglYwjyltjCreate(Map<String, String> map);
}
 