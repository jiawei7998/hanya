package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.model.IfsReportG52GovRepay;
import com.singlee.hrbnewport.service.impl.IfsReportG52GovRepayServiceImpl;

import java.util.Map;

/**
 * 2021/12/8
 *
 * @Auther:zk
 */
public interface IfsReportG52GovRepayService extends CommonExportService  {

    public Page<IfsReportG52GovRepay> searchAllPage(Map<String, Object> map);

}
