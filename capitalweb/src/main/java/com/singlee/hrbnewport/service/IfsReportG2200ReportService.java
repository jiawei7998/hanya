package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportG2200;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;


/**
 * @author Administrator
 * <p>
 * G22流动性比例监测表
 */
@Service
public interface IfsReportG2200ReportService extends CommonExportService {

    Page<IfsReportG2200> searchIfsReportG2200Page(@RequestBody Map<String, String> map);

    SlOutBean ifsReportG2200Create(Map<String, String> map);
}
 