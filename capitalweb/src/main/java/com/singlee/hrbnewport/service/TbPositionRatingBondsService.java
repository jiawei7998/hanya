package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.model.TbPositionRatingBonds;

import java.util.Map;

public interface TbPositionRatingBondsService extends CommonExportService{


    Page<TbPositionRatingBonds> selectReport(Map<String,String > map);

}
