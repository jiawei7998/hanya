package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportG3301;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;


/**
 * @author Administrator
 * <p>
 * G3301银行账簿利率风险计量报表
 */
@Service
public interface IfsReportG3301ReportService extends CommonExportService {

    Page<IfsReportG3301> searchIfsReportG3301Page(@RequestBody Map<String, String> map);

    SlOutBean ifsReportG3301Create(Map<String, String> map);
}
 