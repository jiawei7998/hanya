package com.singlee.hrbnewport.service;

import com.singlee.hrbnewport.model.IfsReportTytzJcBttycr;

import java.util.List;
import java.util.Map;

/**
 * @author ：chenguo
 * @date ：Created in 2021/12/6 16:20
 * @description：${description}
 * @modified By：
 * @version:
 */
public interface IfsReportTytzJcBttycrService extends CommonExportService {


    int insert(IfsReportTytzJcBttycr record);

    int insertSelective(IfsReportTytzJcBttycr record);

    List<IfsReportTytzJcBttycr> getDate(Map<String,Object> map);

}

