package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.mapper.IfsReportTytzJcWtcMapper;
import com.singlee.hrbnewport.model.IfsReportTytzJcWtc;
import com.singlee.hrbnewport.service.IfsReportTytzJcWtcService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ：chenguo
 * @date ：Created in 2021/12/6 16:20
 * @description：${description}
 * @modified By：
 * @version:
 */
@Service
public class IfsReportTytzJcWtcServiceImpl implements IfsReportTytzJcWtcService {

    @Resource
    private IfsReportTytzJcWtcMapper ifsReportTytzJcWtcMapper;

    @Override
    public int insert(IfsReportTytzJcWtc record) {
        return ifsReportTytzJcWtcMapper.insert(record);
    }

    @Override
    public int insertSelective(IfsReportTytzJcWtc record) {
        return ifsReportTytzJcWtcMapper.insertSelective(record);
    }

    @Override
    public List<IfsReportTytzJcWtc> getDate(Map<String, Object> map) {
        ifsReportTytzJcWtcMapper.call(map);
        return ifsReportTytzJcWtcMapper.getDate(map);
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String,Object> mapEnd=new HashMap<>();
        List<IfsReportTytzJcWtc> list=ifsReportTytzJcWtcMapper.getDate(map);
        mapEnd.put("list",list);
        return mapEnd;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}

