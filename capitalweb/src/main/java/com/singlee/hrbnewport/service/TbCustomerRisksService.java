package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.model.TbCustomerRisks;

import java.util.Map;

public interface TbCustomerRisksService extends CommonExportService{

    Page<TbCustomerRisks> selectReport(Map<String,String > map);


}
