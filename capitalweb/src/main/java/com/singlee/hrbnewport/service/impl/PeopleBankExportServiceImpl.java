package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbnewport.mapper.PeopleBankExportMapper;
import com.singlee.hrbnewport.service.PeopleBankExportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 人行利率报备系统
 *
 * @author copysun
 */
@Service
@Transactional(value = "transactionManager", rollbackFor = Exception.class)
public class PeopleBankExportServiceImpl implements PeopleBankExportService {

	@Autowired
	private PeopleBankExportMapper peopleBankExportMapper;


	@Override
	public Map<String, Object> getExportData(Map<String, Object> map) {
		String queryDate=map.get("queryDate").toString();
		String type=map.get("type").toString();
		Map<String, Object> result = new HashMap<String, Object>(16);
		List<Map<String,Object>> mapList=new ArrayList<>();
		peopleBankExportMapper.generateData(map);
		if("1".equals(type)){
			mapList=peopleBankExportMapper.getCustPage(map);
		}else if("2".equals(type)){
			mapList=peopleBankExportMapper.getInterbankDepositsBasePage(map);
		}else if("3".equals(type)){
			mapList=peopleBankExportMapper.getInterbankDepositsBalancePage(map);
		}else if("4".equals(type)){
			mapList=peopleBankExportMapper.getInterbankDepositsAmountPage(map);
		}else if("5".equals(type)){
			mapList=peopleBankExportMapper.getInterbankBorrowBasePage(map);
		}else if("6".equals(type)){
			mapList=peopleBankExportMapper.getInterbankBorrowBalancePage(map);
		}else if("7".equals(type)){
			mapList=peopleBankExportMapper.getInterbankBorrowAmountPage(map);
		}else if("8".equals(type)){
			mapList=peopleBankExportMapper.getRprhBasePage(map);
		}else if("9".equals(type)){
			mapList=peopleBankExportMapper.getRprhBalancePage(map);
		}else{
			mapList=peopleBankExportMapper.getRprhAmountPage(map);
		}
		result.put("list",mapList);
		result.put("instName","哈尔滨银行股份有限公司");
		result.put("year",queryDate.substring(0,4));
		result.put("month",queryDate.substring(5,7));
		result.put("day",queryDate.substring(8,10));
		return result;
	}

	@Override
	public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
		return null;
	}

	@Override
	public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
		String type = map.get("type").toString();
		peopleBankExportMapper.generateData(map);
		if ("1".equals(type)) {
			//对公客户信息表
			return peopleBankExportMapper.getCustPage(map, ParameterUtil.getRowBounds(map));
		} else if ("2".equals(type)) {
			//同业存款基础信息表
			return peopleBankExportMapper.getInterbankDepositsBasePage(map, ParameterUtil.getRowBounds(map));
		} else if ("3".equals(type)) {
			//同业存款余额信息表
			return peopleBankExportMapper.getInterbankDepositsBalancePage(map, ParameterUtil.getRowBounds(map));
		} else if ("4".equals(type)) {
			//同业存款发生额信息表
			return peopleBankExportMapper.getInterbankDepositsAmountPage(map, ParameterUtil.getRowBounds(map));
		} else if ("5".equals(type)) {
			//同业借贷基础信息表
			return peopleBankExportMapper.getInterbankBorrowBasePage(map, ParameterUtil.getRowBounds(map));
		} else if ("6".equals(type)) {
			//同业借贷余额表
			return peopleBankExportMapper.getInterbankBorrowBalancePage(map, ParameterUtil.getRowBounds(map));
		} else if ("7".equals(type)) {
			//同业借贷发生额信息表
			return peopleBankExportMapper.getInterbankBorrowAmountPage(map, ParameterUtil.getRowBounds(map));
		} else if ("8".equals(type)) {
			//买入返售及卖出回购基础信息表
			return peopleBankExportMapper.getRprhBasePage(map, ParameterUtil.getRowBounds(map));
		} else if ("9".equals(type)) {
			//买入返售及卖出回购余额信息表
			return peopleBankExportMapper.getRprhBalancePage(map, ParameterUtil.getRowBounds(map));
		} else if ("10".equals(type)) {
			//买入返售及卖出回购发生额信息表
			return peopleBankExportMapper.getRprhAmountPage(map, ParameterUtil.getRowBounds(map));
		}
		return null;
	}
}
