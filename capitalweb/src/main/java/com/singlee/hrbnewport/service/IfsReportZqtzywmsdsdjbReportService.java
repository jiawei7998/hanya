package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportZqtzywmsdsdjb;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;


/**
 * @author Administrator
 * <p>
 * 债券投资业务免所得税登记表
 */
@Service
public interface IfsReportZqtzywmsdsdjbReportService extends CommonExportService {

    Page<IfsReportZqtzywmsdsdjb> searchIfsReportZqtzywmsdsdjbPage(@RequestBody Map<String, String> map);

    SlOutBean ifsReportZqtzywmsdsdjbCreate(Map<String, String> map);
}
 