package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.mapper.IfsReportTytzFxBctMapper;
import com.singlee.hrbnewport.model.IfsReportTytzFxBct;
import com.singlee.hrbnewport.service.IfsReportTytzFxBctService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 16:21
 * @description：${description}
 * @modified By：
 * @version:   
 */
@Service
public class IfsReportTytzFxBctServiceImpl implements IfsReportTytzFxBctService{

    @Resource
    private IfsReportTytzFxBctMapper ifsReportTytzFxBctMapper;

    @Override
    public int insert(IfsReportTytzFxBct record) {
        return ifsReportTytzFxBctMapper.insert(record);
    }

    @Override
    public int insertSelective(IfsReportTytzFxBct record) {
        return ifsReportTytzFxBctMapper.insertSelective(record);
    }

    @Override
    public List<IfsReportTytzFxBct> getDate(Map<String, Object> map) {
        ifsReportTytzFxBctMapper.call(map);
        return ifsReportTytzFxBctMapper.getDate(map);
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String,Object> mapEnd=new HashMap<>();
        List<IfsReportTytzFxBct> list=ifsReportTytzFxBctMapper.getDate(map);
        mapEnd.put("list",list);
        return mapEnd;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}
