package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.model.*;
import org.apache.ibatis.session.RowBounds;

import java.util.Map;

/**
 * 2021/12/8
 *
 * @Auther:zk
 */
public interface YjInnerbankService extends CommonExportService{

    Page<YjInnerbankRepo> searchRepoAllPage(Map<String,Object> param);

    Page<YjInnerbankRvrepo> searchRvrepoAllPage(Map<String,Object> param);

    Page<YjInnerbankSecTpos> searchSecTposAllPage(Map<String,Object> param);

    Page<YjInnerbankFund> searchFundAllPage(Map<String,Object> param);

    Page<YjInnerbankCdTpos> searchCdTposAllPage(Map<String,Object> param);

    Page<YjInnerbankDeposit> searchDepositAllPage(Map<String,Object> param);

    Page<YjInnerbankCdIss> searchCdIssAllPage(Map<String,Object> param);
}
