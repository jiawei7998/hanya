package com.singlee.hrbnewport.service.impl;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.*;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.hrbnewport.mapper.IfsReportXzqtzMapper;
import com.singlee.hrbnewport.model.IfsReportXzqtz;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.singlee.hrbnewport.mapper.NewBondInvestReportMapper;
import com.singlee.hrbnewport.service.NewBondInvestReportService;


/**
 * @author Administrator
 * 新债券投资情况表
 */
@Service
public class NewBondInvestReportServiceImpl implements NewBondInvestReportService {


    @Autowired
    private NewBondInvestReportMapper bondInvest;
    @Autowired
    private IfsReportXzqtzMapper ifsReportXzqtzMapper;

    private static Logger logger = Logger.getLogger(NewBondInvestReportServiceImpl.class);


    public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
        Map<String, Object> result = new HashMap<>();
        Map<String, Object> mapEnd = createData(paramer);
        result.put("mapEnd", mapEnd);
        return result;
    }

    @Override
    public Page<IfsReportXzqtz> searchIfsReportXzqtzPage(Map<String, String> map) {
        int pageNumber = ParameterUtil.getInt(map, "pageNumber", 10);
        int pageSize = ParameterUtil.getInt(map, "pageSize", 1);
        List<IfsReportXzqtz> ifsReportXzqtzList = ifsReportXzqtzMapper.selectAll();
        for (IfsReportXzqtz ifsReportXzqtz : ifsReportXzqtzList) {
            if ("合计".equals(ifsReportXzqtz.getQz())) {
                ifsReportXzqtz.setProduct("合计");
            } else {
                ifsReportXzqtz.setProduct("债券投资");
            }
            String[] s = ifsReportXzqtz.getQz().split("_");
            if (s.length > 0) {
                ifsReportXzqtz.setQz(s[0]);
            }
            if (s.length > 1) {
                ifsReportXzqtz.setQzo(s[1]);
            }
            if (s.length > 2) {
                ifsReportXzqtz.setQzt(s[2]);
            }
        }
        return HrbReportUtils.producePage(ifsReportXzqtzList, pageNumber, pageSize);
    }

    @Override
    public SlOutBean ifsReportXzqtzCreate(Map<String, String> map) {
        SlOutBean msg = new SlOutBean(RetStatusEnum.S, "error.common.0000", "【新债券投资情况表】生成成功！");
        ifsReportXzqtzMapper.ifsReportXzqtzCreate(map);
        String RETCODE = map.get("RETCODE");
        if (!"999".equals(RETCODE)) {
            logger.info("【新债券投资情况表】生成失败!");
            return new SlOutBean(RetStatusEnum.F, RETCODE, "【新债券投资情况表】生成失败!");
        }
        msg.setRetCode("999");
        msg.setRetMsg("【新债券投资情况表】生成成功！");
        logger.info("【新债券投资情况表】生成成功！");
        return msg;
    }

    public Map<String, Object> createData(Map<String, Object> paramer) {
        Map<String, Object> mapEnd = new HashMap<>();
        try {
            //国债
            List<Map<String, Object>> listgz = bondInvest.getSql1(paramer);
            //地方政府债
            List<Map<String, Object>> listdfzf = bondInvest.getSql12(paramer);
            //央票
            List<Map<String, Object>> listyp = bondInvest.getSql2(paramer);

            List<Map<String, Object>> listzcx = bondInvest.getSql3(paramer);
            //商业银行债   3月以内
            List<Map<String, Object>> listsy3n = bondInvest.getSql4(paramer);
            //商业银行债   不含次级 3月以上
            List<Map<String, Object>> listsy3w = bondInvest.getSql5(paramer);
            //	次级债  政策性银行发行
            List<Map<String, Object>> listcjzzcx = bondInvest.getSql6(paramer);
            //次级债  商业银行发行
            List<Map<String, Object>> listcjzsy = bondInvest.getSql7(paramer);
            //其他金融机构发行的债券
            List<Map<String, Object>> listqtjr = bondInvest.getSql8(paramer);
            //公用企业债
            List<Map<String, Object>> listqyz = bondInvest.getSql9(paramer);
            //其他债
            List<Map<String, Object>> listqt = bondInvest.getSql10(paramer);
            Map<String, Object> map0 = getData(listgz, 0.0);
            mapEnd.put("map0t7", map0.get("t7"));
            mapEnd.put("map0t8", map0.get("t8"));
            mapEnd.put("map0t11", map0.get("t11"));
            mapEnd.put("map0t12", map0.get("t12"));

            Map<String, Object> map12 = getData(listdfzf, 0.0);
            mapEnd.put("map12t7", map12.get("t7"));
            mapEnd.put("map12t8", map12.get("t8"));
            mapEnd.put("map12t11", map12.get("t11"));
            mapEnd.put("map12t12", map12.get("t12"));

            Map<String, Object> map1 = getData(listyp, 0.0);
            mapEnd.put("map1t7", map1.get("t7"));
            mapEnd.put("map1t8", map1.get("t8"));
            mapEnd.put("map1t11", map1.get("t11"));
            mapEnd.put("map1t12", map1.get("t12"));

            Map<String, Object> map2 = getData(listzcx, 0.0);
            mapEnd.put("map2t7", map2.get("t7"));
            mapEnd.put("map2t8", map2.get("t8"));
            mapEnd.put("map2t11", map2.get("t11"));
            mapEnd.put("map2t12", map2.get("t12"));

            Map<String, Object> map3 = getData(listsy3n, 0.2);
            mapEnd.put("map3t7", map3.get("t7"));
            mapEnd.put("map3t8", map3.get("t8"));
            mapEnd.put("map3t11", map3.get("t11"));
            mapEnd.put("map3t12", map3.get("t12"));

            Map<String, Object> map4 = getData(listsy3w, 0.25);
            mapEnd.put("map4t7", map4.get("t7"));
            mapEnd.put("map4t8", map4.get("t8"));
            mapEnd.put("map4t11", map4.get("t11"));
            mapEnd.put("map4t12", map4.get("t12"));

            Map<String, Object> map5 = getData(listcjzzcx, 1.0);
            mapEnd.put("map5t7", map5.get("t7"));
            mapEnd.put("map5t8", map5.get("t8"));
            mapEnd.put("map5t11", map5.get("t11"));
            mapEnd.put("map5t12", map5.get("t12"));

            Map<String, Object> map6 = getData(listcjzsy, 1.0);
            mapEnd.put("map6t7", map6.get("t7"));
            mapEnd.put("map6t8", map6.get("t8"));
            mapEnd.put("map6t11", map6.get("t11"));
            mapEnd.put("map6t12", map6.get("t12"));

            Map<String, Object> map7 = getData(null, 0.0);
            mapEnd.put("map7t7", map7.get("t7"));
            mapEnd.put("map7t8", map7.get("t8"));
            mapEnd.put("map7t11", map7.get("t11"));
            mapEnd.put("map7t12", map7.get("t12"));

            Map<String, Object> map8 = getData(null, 1.0);
            mapEnd.put("map8t7", map8.get("t7"));
            mapEnd.put("map8t8", map8.get("t8"));
            mapEnd.put("map8t11", map8.get("t11"));
            mapEnd.put("map8t12", map8.get("t12"));

            Map<String, Object> map9 = getData(listqtjr, 1.0);
            mapEnd.put("map9t7", map9.get("t7"));
            mapEnd.put("map9t8", map9.get("t8"));
            mapEnd.put("map9t11", map9.get("t11"));
            mapEnd.put("map9t12", map9.get("t12"));
            //公用企业债
            Map<String, Object> map10 = getData(listqyz, 0.2);
            mapEnd.put("map10t7", map10.get("t7"));
            mapEnd.put("map10t8", map10.get("t8"));
            mapEnd.put("map10t11", map10.get("t11"));
            mapEnd.put("map10t12", map10.get("t12"));
            Map<String, Object> map11 = getData(listqt, 1.0);

//            BigDecimal t6Date = nullto0(map11.get("t6").toString()).subtract(nullto0(map0.get("t6").toString()).add(nullto0(map1.get("t6").toString()))
//                    .add(nullto0(map2.get("t6").toString())).add(nullto0(map12.get("t6").toString())).add(nullto0((map3.get("t6").toString())).add(nullto0(map4.get("t6").toString()))
//                    .add(nullto0(map6.get("t6").toString())).add(nullto0(map7.get("t6").toString()))
//                    .add(nullto0(map8.get("t6").toString())).add(nullto0(map9.get("t6").toString())).add(nullto0(map10.get("t6").toString()))));
            BigDecimal t7Date = nullto0(map11.get("t7").toString()).subtract(nullto0(map0.get("t7").toString()).add(nullto0(map1.get("t7").toString()))
                    .add(nullto0(map2.get("t7").toString())).add(nullto0(map3.get("t7").toString())).add(nullto0(map4.get("t7").toString()))
                    .add(nullto0(map6.get("t7").toString())).add(nullto0(map12.get("t7").toString())).add(nullto0(map7.get("t7").toString()))
                    .add(nullto0(map8.get("t7").toString())).add(nullto0(map9.get("t7").toString())).add(nullto0(map10.get("t7").toString())));
            BigDecimal t8Date = nullto0(map11.get("t8").toString()).subtract(nullto0(map0.get("t8").toString()).add(nullto0(map1.get("t8").toString()))
                    .add(nullto0(map2.get("t8").toString())).add(nullto0(map3.get("t8").toString())).add(nullto0(map4.get("t8").toString()))
                    .add(nullto0(map6.get("t8").toString())).add(nullto0(map12.get("t8").toString())).add(nullto0(map7.get("t8").toString()))
                    .add(nullto0(map8.get("t8").toString())).add(nullto0(map9.get("t8").toString())).add(nullto0(map10.get("t8").toString())));
//            BigDecimal t9Date = nullto0( map11.get("t9")).subtract(nullto0( map0.get("t9")).add(nullto0( map1.get("t9")))
//                    .add(nullto0( map2.get("t9"))).add(nullto0( map3.get("t9"))).add(nullto0( map4.get("t9")))
//                    .add(nullto0( map6.get("t9"))).add(nullto0( map12.get("t9"))).add(nullto0( map7.get("t9")))
//                    .add(nullto0( map8.get("t9"))).add(nullto0( map9.get("t9"))).add(nullto0( map10.get("t9"))));
//            BigDecimal t10Date = nullto0( map11.get("t10")).subtract(nullto0( map0.get("t10")).add(nullto0( map1.get("t10")))
//                    .add(nullto0( map2.get("t10"))).add(nullto0( map3.get("t10"))).add(nullto0( map4.get("t10")))
//                    .add(nullto0( map6.get("t10"))).add(nullto0( map12.get("t10"))).add(nullto0( map7.get("t10")))
//                    .add(nullto0( map8.get("t10"))).add(nullto0( map9.get("t10"))).add(nullto0( map10.get("t10"))));
            BigDecimal t11Date = nullto0(map11.get("t11").toString()).subtract(nullto0(map0.get("t11").toString()).add(nullto0(map1.get("t11").toString()))
                    .add(nullto0(map2.get("t11").toString())).add(nullto0(map3.get("t11").toString())).add(nullto0(map4.get("t11").toString()))
                    .add(nullto0(map6.get("t11").toString())).add(nullto0(map12.get("t11").toString())).add(nullto0(map7.get("t11").toString()))
                    .add(nullto0(map8.get("t11").toString())).add(nullto0(map9.get("t11").toString())).add(nullto0(map10.get("t11").toString())));
            BigDecimal t12Date = nullto0(map11.get("t12").toString()).subtract(nullto0(map0.get("t12").toString()).add(nullto0(map1.get("t12").toString()))
                    .add(nullto0(map2.get("t12").toString())).add(nullto0(map3.get("t12").toString())).add(nullto0(map4.get("t12").toString()))
                    .add(nullto0(map6.get("t12").toString())).add(nullto0(map12.get("t12").toString())).add(nullto0(map7.get("t12").toString()))
                    .add(nullto0(map8.get("t12").toString())).add(nullto0(map9.get("t12").toString())).add(nullto0(map10.get("t12").toString())));

            mapEnd.put("map11t7", t7Date.toString());
            mapEnd.put("map11t8", t8Date.toString());
            mapEnd.put("map11t11", t11Date.toString());
            mapEnd.put("map11t12", t12Date.toString());

            Map<String, Object> map13 = getData(listqt, 1.0);
            mapEnd.put("map13t7", map13.get("t7"));
            mapEnd.put("map13t8", map13.get("t8"));
            mapEnd.put("map13t11", map13.get("t11"));
            mapEnd.put("map13t12", map13.get("t12").toString());

        } catch (Exception e) {
            e.printStackTrace();
        } finally {

        }
        return mapEnd;
    }

    public BigDecimal nullto0(String s) {
        if ("".equals(s)) {
            return new BigDecimal("0.0");
        } else {
            String data = s.replaceAll(",", "");
            return new BigDecimal(data);
        }
    }

    public Map<String, Object> getData(List<Map<String, Object>> list, Double qz) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("t6", "0.0000");
        map.put("t7", "0.0000");
        map.put("t8", "0.0000");
        map.put("t9", "0.0000");
        map.put("t10", "0.0000");
        map.put("t11", "0.0000");
        map.put("t12", "0.0000");
        DecimalFormat df = new DecimalFormat("0.0000");
        if (list != null && list.size() > 0) {
            for (Map<String, Object> m : list) {
                if ("1".equals((String) m.get("COST"))) {//交易账户
//					map.put("t6", m.get("MZ"));
                    map.put("t7", m.get("YE"));
                    map.put("t8", m.get("LX"));
                    map.put("t9", df.format(Double.parseDouble(m.get("YE").toString()) * qz));
                } else {//银行账户
//					map.put("t10",m.get("MZ"));
                    map.put("t11", m.get("YE"));
                    map.put("t12", m.get("LX"));
                }
            }
        }
        return map;
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        List<IfsReportXzqtz> ifsReportXzqtzList = ifsReportXzqtzMapper.selectAll();
        Map<String, Object> rustmap = new HashMap<>();
        rustmap.put("list",ifsReportXzqtzList);
        return rustmap;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}