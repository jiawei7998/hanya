package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbnewport.mapper.IfsReportD02Mapper;
import com.singlee.hrbnewport.service.IfsReportD02Service;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class IfsReportD02ServiceImpl implements IfsReportD02Service{

    @Resource
    private IfsReportD02Mapper ifsReportD02Mapper;


    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String, Object> result = new HashMap<String, Object>();
        ifsReportD02Mapper.generateData(map);
        List<Map<String, Object>> mapList = ifsReportD02Mapper.getData(map);
        for(int i =0;i<mapList.size();i++){
            result.put("map",mapList.get(i));
        }
        return result;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        ifsReportD02Mapper.generateData(map);
        return ifsReportD02Mapper.getData(map, ParameterUtil.getRowBounds(map));
    }
}
