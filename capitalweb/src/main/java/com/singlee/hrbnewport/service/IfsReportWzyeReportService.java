package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportWzye;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;


/**
 * @author Administrator
 * <p>
 * 外债余额
 */
@Service
public interface IfsReportWzyeReportService extends CommonExportService {

    Page<IfsReportWzye> searchIfsReportWzyePage(@RequestBody Map<String, String> map);

    SlOutBean ifsReportWzyeCreate(Map<String, String> map);
}
 