package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.model.CombinePositionVO;

import java.util.Map;

public interface CombinePositionReportService extends CommonExportService {

    public Page<CombinePositionVO> selectBondReport(Map<String,String > map);
}
