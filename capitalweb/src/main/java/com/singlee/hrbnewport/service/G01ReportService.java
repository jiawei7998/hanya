package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.model.G01ReportVO;

import java.util.Map;

/**
 * G01 资产负债项目统计表
 * @author cg
 *
 */
public interface G01ReportService  extends CommonExportService{



    public Page<G01ReportVO> getG01Report(Map<String, String> map);
}
