package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbnewport.mapper.IfsReportForeckMapper;
import com.singlee.hrbnewport.model.IfsReportForeck;
import com.singlee.hrbnewport.service.IfsReportForeckService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class IfsReportForeckServiceImpl implements IfsReportForeckService{

    @Autowired
    private IfsReportForeckMapper ifsReportForeckMapper;


    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String, Object> result = new HashMap<String, Object>();
        String queryDate = ParameterUtil.getString(map,"queryDate", DateUtil.getCurrentDateAsString());
        String unit = ParameterUtil.getString(map,"textValue", "元");
        String ccytype = ParameterUtil.getString(map,"ccytype", "CNY");
        map.put("queryDate",queryDate);
        List<IfsReportForeck> list=ifsReportForeckMapper.getForeck();
        result.put("list",list);
        result.put("unit",unit);
        result.put("ccytype",ccytype);
        result.put("year",queryDate.substring(0,4));
        result.put("month",queryDate.substring(5,7));
		result.put("day",queryDate.substring(8,10));
        return result;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }

    @Override
    public List<IfsReportForeck> selectReport(Map<String, String> map) {
        ifsReportForeckMapper.ReportForeckCreate(map);
        return ifsReportForeckMapper.getForeck();
    }
}
