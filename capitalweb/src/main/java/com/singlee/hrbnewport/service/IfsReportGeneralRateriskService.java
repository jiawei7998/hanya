package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.model.IfsReportGeneralRaterisk;

import java.util.Map;

public interface IfsReportGeneralRateriskService extends CommonExportService{


    Page<IfsReportGeneralRaterisk> selectReport(Map<String,String > map);

}
