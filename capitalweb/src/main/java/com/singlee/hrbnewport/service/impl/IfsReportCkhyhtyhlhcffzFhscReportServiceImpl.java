package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.hrbnewport.mapper.IfsReportCkhyhtyhlhcffzFhscMapper;
import com.singlee.hrbnewport.model.IfsReportCkhyhtyhlhcffzFhsc;
import com.singlee.hrbnewport.service.IfsReportCkhyhtyhlhcffzFhscReportService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author Administrator
 * <p>
 * 存款含银行同业和联行存放负债 分行上传表
 */
@Service
public class IfsReportCkhyhtyhlhcffzFhscReportServiceImpl implements IfsReportCkhyhtyhlhcffzFhscReportService {


    @Autowired
    private IfsReportCkhyhtyhlhcffzFhscMapper ifsReportCkhyhtyhlhcffzFhscMapper;

    private static Logger logger = Logger.getLogger(IfsReportCkhyhtyhlhcffzFhscReportServiceImpl.class);


    @Override
    public Page<IfsReportCkhyhtyhlhcffzFhsc> searchIfsReportCkhyhtyhlhcffzFhscPage(Map<String, String> map) {
        int pageNumber = ParameterUtil.getInt(map, "pageNumber", 10);
        int pageSize = ParameterUtil.getInt(map, "pageSize", 1);
        List<IfsReportCkhyhtyhlhcffzFhsc> ifsReportCkhyhtyhlhcffzFhscList = ifsReportCkhyhtyhlhcffzFhscMapper.selectAll();
        return HrbReportUtils.producePage(ifsReportCkhyhtyhlhcffzFhscList, pageNumber, pageSize);
    }

    @Override
    public SlOutBean ifsReportCkhyhtyhlhcffzFhscCreate(Map<String, String> map) {
        SlOutBean msg = new SlOutBean(RetStatusEnum.S, "error.common.0000", "【Ck存款含银行同业和联行存放负债 分行上传表】生成成功！");
        ifsReportCkhyhtyhlhcffzFhscMapper.ifsReportCkhyhtyhlhcffzFhscCreate(map);
        String RETCODE = map.get("RETCODE");
        if (!"999".equals(RETCODE)) {
            logger.info("【Ck存款含银行同业和联行存放负债 分行上传表】生成失败!");
            return new SlOutBean(RetStatusEnum.F, RETCODE, "【Ck存款含银行同业和联行存放负债 分行上传表】生成失败!");
        }
        msg.setRetCode("999");
        msg.setRetMsg("【Ck存款含银行同业和联行存放负债 分行上传表】生成成功！");
        logger.info("【Ck存款含银行同业和联行存放负债 分行上传表】生成成功！");
        return msg;
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        List<IfsReportCkhyhtyhlhcffzFhsc> ifsReportCkhyhtyhlhcffzFhscList = ifsReportCkhyhtyhlhcffzFhscMapper.selectAll();
        Map<String, Object> rustmap = new HashMap<>();
        rustmap.put("list",ifsReportCkhyhtyhlhcffzFhscList);
        return rustmap;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}