package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbnewport.mapper.IfsReportPvMapper;
import com.singlee.hrbnewport.service.IfsReportPvService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class IfsReportPvServiceImpl implements IfsReportPvService{

    @Autowired
    IfsReportPvMapper ifsReportPvMapper;

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String, Object> result = new HashMap<String, Object>();
        List<Map<String, Object>> mapList = ifsReportPvMapper.getData(map);
        result.put("list",mapList);
        return result;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        ifsReportPvMapper.generateData(map);
        return ifsReportPvMapper.getData(map, ParameterUtil.getRowBounds(map));
    }
}
