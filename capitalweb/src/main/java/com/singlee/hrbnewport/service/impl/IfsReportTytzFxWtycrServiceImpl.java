package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.mapper.IfsReportTytzFxWtycrMapper;
import com.singlee.hrbnewport.model.IfsReportTytzFxWtycr;
import com.singlee.hrbnewport.service.IfsReportTytzFxWtycrService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 16:21
 * @description：${description}
 * @modified By：
 * @version:   
 */
@Service
public class IfsReportTytzFxWtycrServiceImpl implements IfsReportTytzFxWtycrService{

    @Resource
    private IfsReportTytzFxWtycrMapper ifsReportTytzFxWtycrMapper;

    @Override
    public int insert(IfsReportTytzFxWtycr record) {
        return ifsReportTytzFxWtycrMapper.insert(record);
    }

    @Override
    public int insertSelective(IfsReportTytzFxWtycr record) {
        return ifsReportTytzFxWtycrMapper.insertSelective(record);
    }

    @Override
    public List<IfsReportTytzFxWtycr> getDate(Map<String, Object> map) {
        ifsReportTytzFxWtycrMapper.call(map);
        return ifsReportTytzFxWtycrMapper.getDate(map);
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String,Object> mapEnd=new HashMap<>();
        List<IfsReportTytzFxWtycr> list=ifsReportTytzFxWtycrMapper.getDate(map);
        mapEnd.put("list",list);
        return mapEnd;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}
