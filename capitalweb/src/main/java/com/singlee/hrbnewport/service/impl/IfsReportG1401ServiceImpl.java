package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbnewport.mapper.IfsReportG1401Mapper;
import com.singlee.hrbnewport.model.IfsReportCdPosit;
import com.singlee.hrbnewport.model.IfsReportG1401;
import com.singlee.hrbnewport.model.IfsReportG52GovRepay;
import com.singlee.hrbnewport.service.IfsReportG1401Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * G1401大额风险暴露总体情况
 * 2021/12/7
 * @Auther:zk
 */
@Service
public class IfsReportG1401ServiceImpl implements IfsReportG1401Service {

    @Autowired
    IfsReportG1401Mapper ifsReportG1401Mapper;

    /**********************查询方法*****************************/
    @Override
    public Page<IfsReportG1401> searchAllPage(Map<String, Object> map) {
        Object temDate = map.get("queryDate");
        String queryDate = temDate!=null&&!"".equals(temDate.toString().trim())?temDate.toString().trim(): DateUtil.getCurrentDateAsString();
        map.put("queryDate", queryDate);
        ifsReportG1401Mapper.createData(map);
        return ifsReportG1401Mapper.searchAllPage(map, ParameterUtil.getRowBounds(map));
    }

    /**********************导出方法****************************/
    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        HashMap<String, Object> result = new HashMap<>();
        Page<IfsReportG1401> ifsReportG1401s = searchAllPage(map);
        List<IfsReportG1401> result1 = ifsReportG1401s.getResult();
        for (IfsReportG1401 ifsReportG1401 : result1) {
            HashMap<String, Object> paramer = BeanUtil.beanToHashMap(ifsReportG1401);
            result.put("map"+ ParameterUtil.getString(paramer,"sortFiled",""),paramer);
        }
        return result;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}
