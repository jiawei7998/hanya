package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.model.IfsReportG1405;

import java.util.Map;

/**
 * G1405-大额风险暴露
 * by zk
 */
public interface IfsReportG1405Service extends CommonExportService{

    void creatG1405(Map<String , Object> map);

    Page<IfsReportG1405> searchIfsReportG1405(Map<String,Object> map);

}
