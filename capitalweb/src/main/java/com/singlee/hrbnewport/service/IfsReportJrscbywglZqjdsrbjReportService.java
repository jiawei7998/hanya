package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportJrscbywglZqjdsrbj;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;


/**
 * @author Administrator
 * <p>
 * 债券借贷收入比较情况表
 */
@Service
public interface IfsReportJrscbywglZqjdsrbjReportService extends CommonExportService {

    Page<IfsReportJrscbywglZqjdsrbj> searchIfsReportJrscbywglZqjdsrbjPage(@RequestBody Map<String, String> map);

    SlOutBean ifsReportJrscbywglZqjdsrbjCreate(Map<String, String> map);
}
 