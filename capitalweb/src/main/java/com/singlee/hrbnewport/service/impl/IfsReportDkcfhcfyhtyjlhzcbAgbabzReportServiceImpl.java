package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.hrbnewport.mapper.IfsReportDkcfhcfyhtyjlhzcbAgbabzMapper;
import com.singlee.hrbnewport.model.IfsReportDkcfhcfyhtyjlhzcbAgbabz;
import com.singlee.hrbnewport.service.IfsReportDkcfhcfyhtyjlhzcbAgbabzReportService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author Administrator
 * <p>
 * 贷款拆放含拆放银行同业及联行资产表 按国别按币种
 */
@Service
public class IfsReportDkcfhcfyhtyjlhzcbAgbabzReportServiceImpl implements IfsReportDkcfhcfyhtyjlhzcbAgbabzReportService {


    @Autowired
    private IfsReportDkcfhcfyhtyjlhzcbAgbabzMapper ifsReportDkcfhcfyhtyjlhzcbAgbabzMapper;

    private static Logger logger = Logger.getLogger(IfsReportDkcfhcfyhtyjlhzcbAgbabzReportServiceImpl.class);


    @Override
    public Page<IfsReportDkcfhcfyhtyjlhzcbAgbabz> searchIfsReportDkcfhcfyhtyjlhzcbAgbabzPage(Map<String, String> map) {
        int pageNumber = ParameterUtil.getInt(map, "pageNumber", 10);
        int pageSize = ParameterUtil.getInt(map, "pageSize", 1);
        List<IfsReportDkcfhcfyhtyjlhzcbAgbabz> ifsReportDkcfhcfyhtyjlhzcbAgbabzList = ifsReportDkcfhcfyhtyjlhzcbAgbabzMapper.selectAll();
        return HrbReportUtils.producePage(ifsReportDkcfhcfyhtyjlhzcbAgbabzList, pageNumber, pageSize);
    }

    @Override
    public SlOutBean ifsReportDkcfhcfyhtyjlhzcbAgbabzCreate(Map<String, String> map) {
        SlOutBean msg = new SlOutBean(RetStatusEnum.S, "error.common.0000", "【贷款拆放含拆放银行同业及联行资产表 按国别按币种】生成成功！");
        ifsReportDkcfhcfyhtyjlhzcbAgbabzMapper.ifsReportDkcfhcfyhtyjlhzcbAgbabzCreate(map);
        String RETCODE = map.get("RETCODE");
        if (!"999".equals(RETCODE)) {
            logger.info("【贷款拆放含拆放银行同业及联行资产表 按国别按币种】生成失败!");
            return new SlOutBean(RetStatusEnum.F, RETCODE, "【贷款拆放含拆放银行同业及联行资产表 按国别按币种】生成失败!");
        }
        msg.setRetCode("999");
        msg.setRetMsg("【贷款拆放含拆放银行同业及联行资产表 按国别按币种】生成成功！");
        logger.info("【贷款拆放含拆放银行同业及联行资产表 按国别按币种】生成成功！");
        return msg;
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        List<IfsReportDkcfhcfyhtyjlhzcbAgbabz> ifsReportDkcfhcfyhtyjlhzcbAgbabzList = ifsReportDkcfhcfyhtyjlhzcbAgbabzMapper.selectAll();
        Map<String, Object> rustmap = new HashMap<>();
        rustmap.put("list",ifsReportDkcfhcfyhtyjlhzcbAgbabzList);
        return rustmap;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}