package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.hrbnewport.mapper.IfsReportJrscbywglZcfzMapper;
import com.singlee.hrbnewport.model.IfsReportJrscbywglZcfz;
import com.singlee.hrbnewport.service.IfsReportJrscbywglZcfzReportService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author Administrator
 * <p>
 * 资产负债数据表
 */
@Service
public class IfsReportJrscbywglZcfzReportServiceImpl implements IfsReportJrscbywglZcfzReportService {


    @Autowired
    private IfsReportJrscbywglZcfzMapper ifsReportJrscbywglZcfzMapper;

    private static Logger logger = Logger.getLogger(IfsReportJrscbywglZcfzReportServiceImpl.class);


    @Override
    public Page<IfsReportJrscbywglZcfz> searchIfsReportJrscbywglZcfzPage(Map<String, String> map) {
        int pageNumber = ParameterUtil.getInt(map, "pageNumber", 10);
        int pageSize = ParameterUtil.getInt(map, "pageSize", 1);
        List<IfsReportJrscbywglZcfz> ifsReportJrscbywglZcfzList = ifsReportJrscbywglZcfzMapper.selectAll();
        for (IfsReportJrscbywglZcfz ifsReportJrscbywglZcfz : ifsReportJrscbywglZcfzList) {
            String[] zcList = ifsReportJrscbywglZcfz.getZc().split("_");
            if (zcList.length > 0) {ifsReportJrscbywglZcfz.setZc(zcList[0]);}
            if (zcList.length > 1) {ifsReportJrscbywglZcfz.setZco(zcList[1]);}
            if(null != ifsReportJrscbywglZcfz.getFz()){
                String[] fzList = ifsReportJrscbywglZcfz.getFz().split("_");
                if (fzList.length > 0) {ifsReportJrscbywglZcfz.setFz(fzList[0]);}
                if (fzList.length > 1) {ifsReportJrscbywglZcfz.setFzo(fzList[1]);}
            }
        }
        return HrbReportUtils.producePage(ifsReportJrscbywglZcfzList, pageNumber, pageSize);
    }

    @Override
    public SlOutBean ifsReportJrscbywglZcfzCreate(Map<String, String> map) {
        SlOutBean msg = new SlOutBean(RetStatusEnum.S, "error.common.0000", "【金融市场部业务管理报表-资产负债数据表】生成成功！");
        ifsReportJrscbywglZcfzMapper.ifsReportJrscbywglZcfzCreate(map);
        String RETCODE = map.get("RETCODE");
        if (!"999".equals(RETCODE)) {
            logger.info("【金融市场部业务管理报表-资产负债数据表】生成失败!");
            return new SlOutBean(RetStatusEnum.F, RETCODE, "【金融市场部业务管理报表-资产负债数据表】生成失败!");
        }
        msg.setRetCode("999");
        msg.setRetMsg("【金融市场部业务管理报表-资产负债数据表】生成成功！");
        logger.info("【金融市场部业务管理报表-资产负债数据表】生成成功！");
        return msg;
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        String queryDate = ParameterUtil.getString(map,"queryDate","");
        queryDate = DateUtil.format(DateUtil.parse(queryDate), "yyyy年MM月dd日");
        List<IfsReportJrscbywglZcfz> ifsReportJrscbywglZcfzList = ifsReportJrscbywglZcfzMapper.selectAll();
        Map<String, Object> rustmap = new HashMap<>();
        rustmap.put("list",ifsReportJrscbywglZcfzList);
        rustmap.put("bbrq",queryDate);
        return rustmap;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}