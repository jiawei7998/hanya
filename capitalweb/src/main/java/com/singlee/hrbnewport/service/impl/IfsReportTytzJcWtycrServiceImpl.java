package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.mapper.IfsReportTytzJcWtycrMapper;
import com.singlee.hrbnewport.model.IfsReportTytzJcWtycr;
import com.singlee.hrbnewport.service.IfsReportTytzJcWtycrService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ：chenguo
 * @date ：Created in 2021/12/6 16:21
 * @description：${description}
 * @modified By：
 * @version:
 */
@Service
public class IfsReportTytzJcWtycrServiceImpl implements IfsReportTytzJcWtycrService {

    @Resource
    private IfsReportTytzJcWtycrMapper ifsReportTytzJcWtycrMapper;

    @Override
    public int insert(IfsReportTytzJcWtycr record) {
        return ifsReportTytzJcWtycrMapper.insert(record);
    }

    @Override
    public int insertSelective(IfsReportTytzJcWtycr record) {
        return ifsReportTytzJcWtycrMapper.insertSelective(record);
    }

    @Override
    public List<IfsReportTytzJcWtycr> getDate(Map<String, Object> map) {
        ifsReportTytzJcWtycrMapper.call(map);
        return ifsReportTytzJcWtycrMapper.getDate(map);
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String,Object> mapEnd=new HashMap<>();
        List<IfsReportTytzJcWtycr> list=ifsReportTytzJcWtycrMapper.getDate(map);
        mapEnd.put("list",list);
        return mapEnd;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}

