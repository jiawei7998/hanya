package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.hrbnewport.mapper.RvRepoPladgeMapper;
import com.singlee.hrbnewport.model.RvRepoPladgeVO;
import com.singlee.hrbnewport.service.RvRepoPladgeService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 买入返售质押
 * @author zk
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class RvRepoPladgeServiceImpl implements RvRepoPladgeService {

    @Autowired
    RvRepoPladgeMapper rvRepoPladgeMapper;


    /******************************************2021.11.8新增方法*******************************************************************/
    @Override
    public void createRevRepo(Map<String , Object> param) {
        Object queryDate = param.get("queryDate");
        param.put("queryDate", queryDate!=null&&!"".equals(queryDate.toString().trim()) ?
                queryDate.toString().trim() : DateUtil.getCurrentDateAsString());
        Date queryDay = DateUtil.parse(param.get("queryDate").toString());
        Map<String, String> seasonBegEnd = HrbReportUtils.getSeasonBegEnd(queryDay);
        param.put("startDate", seasonBegEnd.get("begin"));
        param.put("endDate", seasonBegEnd.get("end"));
        rvRepoPladgeMapper.createRevRepo(param);
    }

    @Override
    public Page<RvRepoPladgeVO> searchRevRepoPladgePage(Map<String, Object> param) {
        //获取页码
        int pageNum = (int)param.get("pageNum");
        int pageSize = (int)param.get("pageSize");
        List<RvRepoPladgeVO> rvRepoPladgeVOS = rvRepoPladgeMapper.searchRevRepoPladgeAll();
        return HrbReportUtils.producePage(rvRepoPladgeVOS, pageNum, pageSize);
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Object temDate = map.get("queryDate");
        String queryDate = temDate!=null&&!"".equals(temDate.toString().trim())?temDate.toString().trim(): DateUtil.getCurrentDateAsString();
        Date queryDay = DateUtil.parse(queryDate);
        Map<String, String> seasonBegEnd = HrbReportUtils.getSeasonBegEnd(queryDay);
        map.put("startDate", seasonBegEnd.get("begin"));
        map.put("endDate", seasonBegEnd.get("end"));
        map.put("queryDate", queryDate);
        rvRepoPladgeMapper.createRevRepo(map);

        List<RvRepoPladgeVO> rvRepoPladgeVOS = rvRepoPladgeMapper.searchRevRepoPladgeAll();

        Map<String, Object> result = new HashMap<>();
        for (RvRepoPladgeVO rvRepoPladgeVO : rvRepoPladgeVOS) {
            String mapSuff = getsuff(rvRepoPladgeVO.getColum());
            result.put("map"+mapSuff,rvRepoPladgeVO);
        }
        return result;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }

    /******************************************工具方法*******************************************************************/
    /**
     * 根据colum字段中的表填编号拼接map的后缀
     * 输入：&emsp;&emsp;4.1.1对我国政策性银行的债权(0%)
     * 输出：11
     * @param colum
     * @return
     */
    private String getsuff(String colum){
        String[] split = colum.split("\\.");
        StringBuffer sb = new StringBuffer();
        for (int i = 1; i < split.length; i++) {
            sb.append(split[i].toCharArray()[0]);
        }
        return sb.toString();
    }
    
}
