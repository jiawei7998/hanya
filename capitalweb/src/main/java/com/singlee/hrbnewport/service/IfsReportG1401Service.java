package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.model.IfsReportG1401;

import java.util.Map;

/**
 * 2021/12/7
 *
 * @Auther:zk
 */
public interface IfsReportG1401Service extends CommonExportService {

    Page<IfsReportG1401> searchAllPage(Map<String,Object> map);

}
