package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.mapper.IfsReportTytzWtcMapper;
import com.singlee.hrbnewport.model.IfsReportTytzWtc;
import com.singlee.hrbnewport.service.IfsReportTytzWtcService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/3 15:37
 * @description：${description}
 * @modified By：
 * @version:   
 */
@Service
public class IfsReportTytzWtcServiceImpl implements IfsReportTytzWtcService{

    @Resource
    private IfsReportTytzWtcMapper ifsReportTytzWtcMapper;

    @Override
    public int insert(IfsReportTytzWtc record) {
        return ifsReportTytzWtcMapper.insert(record);
    }

    @Override
    public int insertSelective(IfsReportTytzWtc record) {
        return ifsReportTytzWtcMapper.insertSelective(record);
    }

    @Override
    public List<IfsReportTytzWtc> getDate(Map<String, Object> map) {
        ifsReportTytzWtcMapper.call(map);
        return ifsReportTytzWtcMapper.getDate(map);
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String,Object> mapEnd=new HashMap<>();
        List<IfsReportTytzWtc> list=ifsReportTytzWtcMapper.getDate(map);
        mapEnd.put("list",list);
        return mapEnd;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}
