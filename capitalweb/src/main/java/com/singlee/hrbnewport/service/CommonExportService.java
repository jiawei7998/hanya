package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author copysun
 */
@Service
public interface CommonExportService {

	 Map<String,Object> getExportData(Map<String,Object> map);

	 List<Map<String,Object>> getExportListData(Map<String,Object> map);

	 Page<Map<String,Object>> getExportPageData(Map<String,Object> map);
}
