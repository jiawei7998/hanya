package com.singlee.hrbnewport.service;

import com.singlee.hrbnewport.model.IfsReportRmbtyzhxx;

import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 20:25
 * @description：${description}
 * @modified By：
 * @version:   
 */
public interface IfsReportRmbtyzhxxService extends CommonExportService{


    int insert(IfsReportRmbtyzhxx record);

    int insertSelective(IfsReportRmbtyzhxx record);


    List<IfsReportRmbtyzhxx> getDate(Map<String,Object> map);

}
