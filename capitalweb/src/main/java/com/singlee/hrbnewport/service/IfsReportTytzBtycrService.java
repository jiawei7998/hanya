package com.singlee.hrbnewport.service;

import com.singlee.hrbnewport.model.IfsReportTytzBtc;
import com.singlee.hrbnewport.model.IfsReportTytzBtycr;

import java.util.List;
import java.util.Map;

/**
 * @author ：chenguo
 * @date ：Created in 2021/12/3 15:27
 * @description：${description}
 * @modified By：
 * @version:
 */
public interface IfsReportTytzBtycrService extends  CommonExportService{


    int insert(IfsReportTytzBtycr record);

    int insertSelective(IfsReportTytzBtycr record);

    List<IfsReportTytzBtycr> getDate(Map<String,Object> map);
}

