package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbnewport.mapper.IfsReportBondPositionMapper;
import com.singlee.hrbnewport.service.IfsReportBondPositionService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class IfsReportBondPositionServiceImpl implements IfsReportBondPositionService{

    @Resource
    private IfsReportBondPositionMapper ifsReportBondPositionMapper;


    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String, Object> result = new HashMap<String, Object>(16);
        ifsReportBondPositionMapper.generateData(map);
        List<Map<String, Object>> mapList = ifsReportBondPositionMapper.getData(map);
        result.put("list",mapList);
        return result;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        ifsReportBondPositionMapper.generateData(map);
        return ifsReportBondPositionMapper.getData(map, ParameterUtil.getRowBounds(map));
    }
}
