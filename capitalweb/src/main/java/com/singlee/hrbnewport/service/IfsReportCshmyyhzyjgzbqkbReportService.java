package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportCshmyyhzyjgzbqkb;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;


/**
 * @author Administrator
 * <p>
 * 城商行民营银行主要监管指标情况表
 */
@Service
public interface IfsReportCshmyyhzyjgzbqkbReportService extends CommonExportService {

    Page<IfsReportCshmyyhzyjgzbqkb> searchIfsReportCshmyyhzyjgzbqkbPage(@RequestBody Map<String, String> map);

    SlOutBean ifsReportCshmyyhzyjgzbqkbCreate(Map<String, String> map);
}
 