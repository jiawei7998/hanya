package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbnewport.mapper.IfsReportRmbgsywMapper;
import com.singlee.hrbnewport.model.IfsReportRmbgsyw;
import com.singlee.hrbnewport.service.IfsReportRmbgsywService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ：chenguo
 * @date ：Created in 2021/12/6 20:51
 * @description：${description}
 * @modified By：
 * @version:
 */
@Service
public class IfsReportRmbgsywServiceImpl implements IfsReportRmbgsywService {

    @Resource
    private IfsReportRmbgsywMapper ifsReportRmbgsywMapper;

    @Override
    public int insert(IfsReportRmbgsyw record) {
        return ifsReportRmbgsywMapper.insert(record);
    }

    @Override
    public int insertSelective(IfsReportRmbgsyw record) {
        return ifsReportRmbgsywMapper.insertSelective(record);
    }

    @Override
    public List<IfsReportRmbgsyw> getDate(Map<String, Object> map) {
        String postdate = ParameterUtil.getString(map, "queryDate", "");
        List<IfsReportRmbgsyw> list2 = ifsReportRmbgsywMapper.getDate(map);
        if (list2 == null || list2.size() == 0) {
            ifsReportRmbgsywMapper.call(map);
            return ifsReportRmbgsywMapper.getDate(map);
        }
        return list2;
    }

    @Override
    public void upDate(IfsReportRmbgsyw record) {
        ifsReportRmbgsywMapper.updateByPrimaryKey(record);
    }

    //数据处理
    @Override
    public List<IfsReportRmbgsyw> getBaseDate(Map<String, Object> map) {
        Map<String, Object> Swapmap = new HashMap<>();
        List<IfsReportRmbgsyw> list2 = ifsReportRmbgsywMapper.getDate(map);
        if (list2 == null || list2.size() == 0) {
            List<IfsReportRmbgsyw> ifsReportRmbgsywList = new ArrayList<>();
            List<Map<String, Object>> list = ifsReportRmbgsywMapper.getBaseDate(map);
            if (list != null && list.size() > 0) {
                for (Map<String, Object> remap : list) {
                    IfsReportRmbgsyw ifsReportRmbgsyw = new IfsReportRmbgsyw();
                    //申报号码
                    //操作类型
                    //变更/撤销原因
                    //申报日期
                    ifsReportRmbgsyw.setSbrq(ParameterUtil.getString(remap, "queryDate", ""));
                    String ccode = ParameterUtil.getString(remap, "CCODE", "");
                    //境内
                    if (ccode.equals("CN")) {
                        //境内机构代码
                        ifsReportRmbgsyw.setJnjgdm(ParameterUtil.getString(remap, "OUTNO", ""));
                        //境内机构名称
                        ifsReportRmbgsyw.setJnjgmc(ParameterUtil.getString(remap, "OUTNAME", ""));
                        //境内机构类型
                        ifsReportRmbgsyw.setJnjglx("银行");
                    } else {
                        //境外主体类型
                        ifsReportRmbgsyw.setJnztlx("银行");
                        //境外主体代码
                        ifsReportRmbgsyw.setJwztdm(ParameterUtil.getString(remap, "OUTNO", ""));
                        //境外主体名称
                        ifsReportRmbgsyw.setJwztmc(ParameterUtil.getString(remap, "OUTNAME", ""));
                    }
                    //人民币账户账号
                    ifsReportRmbgsyw.setRmbzhzh(ParameterUtil.getString(remap, "ACCTNO", ""));
                    //人民币跨境购售类型
                    if (ParameterUtil.getString(remap, "PS", "").equals("P")) {
                        ifsReportRmbgsyw.setRmbkjgslx("1");//购买
                    } else {
                        ifsReportRmbgsyw.setRmbkjgslx("2");//出售
                    }
                    //购售用途
                    ifsReportRmbgsyw.setGsyt("货物贸易");
                    //业务品种
                    // ywlx
                    String dealno = ParameterUtil.getString(remap, "DEALNO", "");
                    //交易类型
                    if (!ParameterUtil.getString(remap, "SWAPDEAL", "").trim().equals("0")) {
                        //外汇掉期
                        ifsReportRmbgsyw.setJxlx("0003");
                        Swapmap.put("dealno", dealno);
                        String farnearind = ParameterUtil.getString(remap, "FARNEARIND", "");
                        //远端
                        if (farnearind.equals("F")) {
                            Swapmap.put("farnearind", "N");
                            //远端买入(名义本金)币种
                            ifsReportRmbgsyw.setDqydmrbz(ParameterUtil.getString(remap, "FCCY", ""));
                            //远端买入(名义本金)金额
                            ifsReportRmbgsyw.setDqydmrje(ParameterUtil.getString(remap, "CCYAMT", ""));
                            //远端交割日
                            ifsReportRmbgsyw.setDqydjgr(ParameterUtil.getString(remap, "VDATE", ""));
                            //远端交易价格
                            ifsReportRmbgsyw.setDqydmrje(ParameterUtil.getString(remap, "RATE", ""));
                            //远端卖出(名义本金)币种
                            ifsReportRmbgsyw.setDqydmrje(ParameterUtil.getString(remap, "CTRCCY", ""));
                            //远端卖出(名义本金)金额
                            ifsReportRmbgsyw.setDqydmrje(ParameterUtil.getString(remap, "CTRAMT", ""));
                            //近端信息
                            Map<String, Object> result = ifsReportRmbgsywMapper.getFxdh(Swapmap);
                            //近端交割日
                            ifsReportRmbgsyw.setDqjdjgr(ParameterUtil.getString(result, "VDATE", ""));
                            //近端交易价格
                            ifsReportRmbgsyw.setDqjdjyjg(ParameterUtil.getString(result, "CCYRATE_8", ""));

                        } else {
                            Swapmap.put("farnearind", "F");
                            Map<String, Object> result = ifsReportRmbgsywMapper.getFxdh(Swapmap);
                            //远端买入(名义本金)币种
                            ifsReportRmbgsyw.setDqydmrbz(ParameterUtil.getString(result, "CCY", ""));
                            //远端买入(名义本金)金额
                            ifsReportRmbgsyw.setDqydmrje(ParameterUtil.getString(result, "CCYAMT", ""));
                            //远端交割日
                            ifsReportRmbgsyw.setDqydjgr(ParameterUtil.getString(result, "VDATE", ""));
                            //远端交易价格
                            ifsReportRmbgsyw.setDqydmrje(ParameterUtil.getString(result, "CCYRATE_8", ""));
                            //远端卖出(名义本金)币种
                            ifsReportRmbgsyw.setDqydmrje(ParameterUtil.getString(result, "CTRCCY", ""));
                            //远端卖出(名义本金)金额
                            ifsReportRmbgsyw.setDqydmrje(ParameterUtil.getString(result, "CTRAMT", ""));
                            //近端交割日
                            ifsReportRmbgsyw.setDqjdjgr(ParameterUtil.getString(remap, "VDATE", ""));
                            //近端交易价格
                            ifsReportRmbgsyw.setDqjdjyjg(ParameterUtil.getString(remap, "RATE", ""));
                        }
                        //掉期明细操作类型
                        //掉期序号
                        String SwapId = ifsReportRmbgsywMapper.getSwapId();
                        ifsReportRmbgsyw.setDqxh(SwapId);
                        //掉期交易日
                        ifsReportRmbgsyw.setDqjyr(ParameterUtil.getString(remap, "DEALDATE", ""));
                    } else if (ParameterUtil.getString(remap, "SPOTFWDIND", "").equals("S")) {
                        //外汇即期
                        ifsReportRmbgsyw.setJxlx("0001");
                        //即期明细操作类型
                        //即期序号
                        String SpotId = ifsReportRmbgsywMapper.getSpotId();
                        ifsReportRmbgsyw.setJqxh(SpotId);
                        //即期买入币种
                        ifsReportRmbgsyw.setJqbz(ParameterUtil.getString(remap, "FCCY", ""));
                        //即期买入金额
                        ifsReportRmbgsyw.setJqje(ParameterUtil.getString(remap, "CCYAMT", ""));
                        //即期价格
                        ifsReportRmbgsyw.setJqjg(ParameterUtil.getString(remap, "RATE", ""));
                        //即期卖出币种
                        ifsReportRmbgsyw.setJqmcbz(ParameterUtil.getString(remap, "CTRCCY", ""));
                        //即期卖出金额
                        ifsReportRmbgsyw.setJqmcje(ParameterUtil.getString(remap, "CTRAMT", ""));
                        //即期交易日
                        ifsReportRmbgsyw.setJyr(ParameterUtil.getString(remap, "DEALDATE", ""));
                        //即期结算日
                        ifsReportRmbgsyw.setJsr(ParameterUtil.getString(remap, "VDATE", ""));
                    } else {
                        //外汇远期
                        ifsReportRmbgsyw.setJxlx("0002");
                        //远期明细操作类型
                        //远期序号
                        String FwdindId = ifsReportRmbgsywMapper.getFwdindId();
                        ifsReportRmbgsyw.setJqxh(FwdindId);
                        //远期买入币种
                        ifsReportRmbgsyw.setYqmrbz(ParameterUtil.getString(remap, "FCCY", ""));
                        //远期买入金额
                        ifsReportRmbgsyw.setYqmrje(ParameterUtil.getString(remap, "CCYAMT", ""));
                        //远期价格
                        ifsReportRmbgsyw.setYqjg(ParameterUtil.getString(remap, "RATE", ""));
                        //远期卖出币种
                        ifsReportRmbgsyw.setYqmcbz(ParameterUtil.getString(remap, "CTRCCY", ""));
                        //远期卖出金额
                        ifsReportRmbgsyw.setYqmcje(ParameterUtil.getString(remap, "CTRAMT", ""));
                        //远期交易日
                        ifsReportRmbgsyw.setYqjyr(ParameterUtil.getString(remap, "DEALDATE", ""));
                        //远期到期日
                        ifsReportRmbgsyw.setYqdqr(ParameterUtil.getString(remap, "VDATE", ""));
                        //远期结算日
                        ifsReportRmbgsyw.setYqjsr(ParameterUtil.getString(remap, "VDATE", ""));
                    }
                    //业务种类
                    //银行业务编号
                    ifsReportRmbgsyw.setYhywbh(dealno);
                    //交易附言
                    //账务日期

                    ifsReportRmbgsyw.setPostdate(ParameterUtil.getString(map, "queryDate", ""));
                    ifsReportRmbgsywList.add(ifsReportRmbgsyw);
                    IfsReportRmbgsyw Rmbgg = ifsReportRmbgsywMapper.selectOne(ifsReportRmbgsyw);
                    if (Rmbgg == null) {
                        //插入表
                        ifsReportRmbgsywMapper.insert(ifsReportRmbgsyw);
                    }
                }
                return ifsReportRmbgsywMapper.getDate(map);
            } else {
                return null;
            }
        }
        return list2;

    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String, Object> mapEnd = new HashMap<>();
        List<IfsReportRmbgsyw> list = ifsReportRmbgsywMapper.getDate(map);
        mapEnd.put("list", list);
        return mapEnd;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}
