package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.model.TbLendingLedger;

import java.util.Map;

public interface TbLendingLedgerService extends CommonExportService{

    Page<TbLendingLedger> selectReport(Map<String,String > map);


}
