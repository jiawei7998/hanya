package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportCshbnwtzJczc;
import com.singlee.hrbreport.service.ReportService;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;


/**
 * @author Administrator
 * <p>
 * 表三 表内外投资业务基础资产情况表
 */
@Service
public interface IfsReportCshbnwtzJczcReportService extends CommonExportService {

    Page<IfsReportCshbnwtzJczc> searchIfsReportCshbnwtzJczcPage(@RequestBody Map<String, String> map);

    SlOutBean ifsReportCshbnwtzJczcCreate(Map<String, String> map);
}
 