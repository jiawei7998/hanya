package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.model.TbTradingAccountBondinvest;

import java.util.Map;

public interface TbTradingAccountBondinvestService extends CommonExportService{

    Page<TbTradingAccountBondinvest> selectReport(Map<String,String > map);
}
