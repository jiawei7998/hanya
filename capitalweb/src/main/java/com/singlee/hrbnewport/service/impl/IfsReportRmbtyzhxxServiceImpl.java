package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.mapper.IfsReportRmbtyzhxxMapper;
import com.singlee.hrbnewport.model.IfsReportRmbtyzhxx;
import com.singlee.hrbnewport.service.IfsReportRmbtyzhxxService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 20:25
 * @description：${description}
 * @modified By：
 * @version:   
 */
@Service
public class IfsReportRmbtyzhxxServiceImpl implements IfsReportRmbtyzhxxService{

    @Resource
    private IfsReportRmbtyzhxxMapper ifsReportRmbtyzhxxMapper;

    @Override
    public int insert(IfsReportRmbtyzhxx record) {
        return ifsReportRmbtyzhxxMapper.insert(record);
    }

    @Override
    public int insertSelective(IfsReportRmbtyzhxx record) {
        return ifsReportRmbtyzhxxMapper.insertSelective(record);
    }

    @Override
    public List<IfsReportRmbtyzhxx> getDate(Map<String, Object> map) {
        ifsReportRmbtyzhxxMapper.call(map);
        return ifsReportRmbtyzhxxMapper.getDate(map);
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String,Object> mapEnd=new HashMap<>();
        List<IfsReportRmbtyzhxx> list=ifsReportRmbtyzhxxMapper.getDate(map);
        mapEnd.put("list",list);
        return mapEnd;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}
