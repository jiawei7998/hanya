package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.model.IfsReportCdPosition;

import java.util.Map;

/**
 * 同业存单
 * 2021/12/8
 * @Auther:zk
 */
public interface IfsReportCdPositionService extends CommonExportService {
    Page<IfsReportCdPosition> searchAllPage(Map<String, Object> map);
}
