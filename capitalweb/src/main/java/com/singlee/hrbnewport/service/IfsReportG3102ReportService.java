package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportG3102;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;


/**
 * @author Administrator
 * <p>
 * G31投资业务情况表 非底层资产投资情况
 */
@Service
public interface IfsReportG3102ReportService extends CommonExportService {

    Page<IfsReportG3102> searchIfsReportG3102Page(@RequestBody Map<String, String> map);

    SlOutBean ifsReportG3102Create(Map<String, String> map);
}
 