package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbnewport.mapper.IfsReportG1405Mapper;
import com.singlee.hrbnewport.model.IfsReportCdPosit;
import com.singlee.hrbnewport.model.IfsReportG1405;
import com.singlee.hrbnewport.service.IfsReportG1405Service;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * G1405-大额风险暴露
 * by zk
 */
@Service
public class IfsReportG1405ServiceImpl implements IfsReportG1405Service {

    @Autowired
    IfsReportG1405Mapper ifsReportG1405Mapper;

    /******************************************报表查询方法***********************************************/
    @Override
    public void creatG1405(Map<String , Object> param) {
        Object queryDate = param.get("queryDate");
        param.put("datadate", queryDate!=null&&!"".equals(queryDate.toString().trim()) ?
                queryDate.toString().trim() : DateUtil.getCurrentDateAsString());
        ifsReportG1405Mapper.createG1405Report(param);
    }

    @Override
    public Page<IfsReportG1405> searchIfsReportG1405(Map<String, Object> map) {
        RowBounds rowBounds = ParameterUtil.getRowBounds(map);
        Page<IfsReportG1405> ifsReportG1405s = ifsReportG1405Mapper.searchAllPage(map, rowBounds);
        return ifsReportG1405s;
    }

    /******************************************报表导出方法***********************************************/
    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        HashMap<String, Object> result = new HashMap<>();
//        creatG1405(map);
        List<IfsReportG1405> ifsReportG1405s = searchIfsReportG1405List(map);
        result.put("list",ifsReportG1405s);
        return result;
    }

    private List<IfsReportG1405> searchIfsReportG1405List(Map<String, Object> map) {
        return ifsReportG1405Mapper.searchAll(map);
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}
