package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.mapper.IfsReportTytzJcBctMapper;
import com.singlee.hrbnewport.model.IfsReportTytzJcBct;
import com.singlee.hrbnewport.service.IfsReportTytzJcBctService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 16:20
 * @description：${description}
 * @modified By：
 * @version:   
 */
@Service
public class IfsReportTytzJcBctServiceImpl implements IfsReportTytzJcBctService{

    @Resource
    private IfsReportTytzJcBctMapper ifsReportTytzJcBctMapper;

    @Override
    public int insert(IfsReportTytzJcBct record) {
        return ifsReportTytzJcBctMapper.insert(record);
    }

    @Override
    public int insertSelective(IfsReportTytzJcBct record) {
        return ifsReportTytzJcBctMapper.insertSelective(record);
    }

    @Override
    public List<IfsReportTytzJcBct> getDate(Map<String, Object> map) {
        ifsReportTytzJcBctMapper.call(map);
        return ifsReportTytzJcBctMapper.getDate(map);
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String,Object> mapEnd=new HashMap<>();
        List<IfsReportTytzJcBct> list=ifsReportTytzJcBctMapper.getDate(map);
        mapEnd.put("list",list);
        return mapEnd;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}
