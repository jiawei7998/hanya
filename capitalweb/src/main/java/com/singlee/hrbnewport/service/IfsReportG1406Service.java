package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.model.IfsReportG1405;
import com.singlee.hrbnewport.model.IfsReportG1406;

import java.util.Map;

/**
 * G1406-大额风险暴露
 * by zk
 */
public interface IfsReportG1406Service extends CommonExportService{
    void creatG1406(Map<String , Object> map);

    Page<IfsReportG1406> searchIfsReportG1406(Map<String,Object> map);
}
