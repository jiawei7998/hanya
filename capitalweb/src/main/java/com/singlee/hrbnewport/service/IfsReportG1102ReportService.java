package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportG1102;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;


/**
 * @author Administrator
 * <p>
 * G1102资产质量及准备金表
 */
@Service
public interface IfsReportG1102ReportService extends CommonExportService {

    Page<IfsReportG1102> searchIfsReportG1102Page(@RequestBody Map<String, String> map);

    SlOutBean ifsReportG1102Create(Map<String, String> map);
}
 