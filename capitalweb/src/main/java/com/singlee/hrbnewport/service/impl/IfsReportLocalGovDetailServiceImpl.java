package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbnewport.mapper.IfsReportLocalGovDetailAddMapper;
import com.singlee.hrbnewport.mapper.IfsReportLocalGovDetailExpireMapper;
import com.singlee.hrbnewport.mapper.IfsReportLocalGovDetailMapper;
import com.singlee.hrbnewport.service.IfsReportLocalGovDetailService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author copysun
 */
@Service
public class IfsReportLocalGovDetailServiceImpl implements IfsReportLocalGovDetailService{

    @Resource
    private IfsReportLocalGovDetailMapper ifsReportLocalGovDetailMapper;
    @Resource
    private IfsReportLocalGovDetailAddMapper ifsReportLocalGovDetailAddMapper;
    @Resource
    private IfsReportLocalGovDetailExpireMapper ifsReportLocalGovDetailExpireMapper;
    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        String queryDate=map.get("queryDate").toString();
        String type=map.get("type").toString();
        Map<String, Object> result = new HashMap<String, Object>(16);

        //地方政府明细表
        if("1".equals(type)){
            ifsReportLocalGovDetailMapper.generateData(map);
            List<Map<String, Object>> mapList =ifsReportLocalGovDetailMapper.getData(map);
            result.put("list1",mapList);
        }else{
            ifsReportLocalGovDetailAddMapper.generateData(map);
            ifsReportLocalGovDetailExpireMapper.generateData(map);
            List<Map<String, Object>> mapList1 =ifsReportLocalGovDetailAddMapper.getData(map);
            List<Map<String, Object>> mapList2 =ifsReportLocalGovDetailExpireMapper.getData(map);
            result.put("list1",mapList1);
            result.put("list2",mapList2);
        }
        result.put("instName","哈尔滨银行股份有限公司");
        result.put("year",queryDate.substring(0,4));
        result.put("month",queryDate.substring(5,7));
        result.put("day",queryDate.substring(8,10));
        return result;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        String type=map.get("type").toString();
        if("1".equals(type)){
            ifsReportLocalGovDetailMapper.generateData(map);
            return ifsReportLocalGovDetailMapper.getData(map, ParameterUtil.getRowBounds(map));
        }
        if("2".equals(type)){
            ifsReportLocalGovDetailAddMapper.generateData(map);
            return ifsReportLocalGovDetailAddMapper.getData(map, ParameterUtil.getRowBounds(map));
        }
        if("3".equals(type)){
            ifsReportLocalGovDetailExpireMapper.generateData(map);
            return ifsReportLocalGovDetailExpireMapper.getData(map, ParameterUtil.getRowBounds(map));
        }

        return null;
    }
}
