package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbnewport.mapper.IfsReportHbyuzeMapper;
import com.singlee.hrbnewport.model.IfsReportHbyuze;
import com.singlee.hrbnewport.service.IfsReportHbyuzeService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class IfsReportHbyuzeServiceImpl implements IfsReportHbyuzeService{

    @Resource
    private IfsReportHbyuzeMapper ifsReportHbyuzeMapper;




    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String, Object> result = new HashMap<String, Object>();
        String queryDate = ParameterUtil.getString(map,"queryDate", DateUtil.getCurrentDateAsString());
        map.put("queryDate",queryDate);
        List<IfsReportHbyuze> list=ifsReportHbyuzeMapper.getIfsReportHbyuze();
        result.put("list",list);
//        result.put("instName","哈尔滨银行股份有限公司");
        result.put("year",queryDate.substring(0,4));
        result.put("month",queryDate.substring(5,7));
        result.put("day",queryDate.substring(8,10));
        return result;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<IfsReportHbyuze> selectReport(Map<String, String> map) {
        RowBounds rb = ParameterUtil.getRowBounds(map);
        ifsReportHbyuzeMapper.ifsReportHbyuzeCreate(map);

        return ifsReportHbyuzeMapper.getIfsReportHbyuze(rb);
    }
}
