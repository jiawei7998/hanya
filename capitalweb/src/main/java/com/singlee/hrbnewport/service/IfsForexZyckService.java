package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.model.IfsForexZyck;

import java.util.Map;

public interface IfsForexZyckService extends CommonExportService{


    Page<IfsForexZyck> selectReport(Map<String,String > map);

}
