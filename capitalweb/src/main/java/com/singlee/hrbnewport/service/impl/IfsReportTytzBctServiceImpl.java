package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.mapper.IfsReportTytzBctMapper;
import com.singlee.hrbnewport.model.IfsReportTytzBct;
import com.singlee.hrbnewport.service.IfsReportTytzBctService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ：chenguo
 * @date ：Created in 2021/12/3 15:27
 * @description：${description}
 * @modified By：本币-存放同业
 * @version:
 */
@Service
public class IfsReportTytzBctServiceImpl implements IfsReportTytzBctService {

    @Resource
    private IfsReportTytzBctMapper ifsReportTytzBctMapper;

    @Override
    public int insert(IfsReportTytzBct record) {
        return ifsReportTytzBctMapper.insert(record);
    }

    @Override
    public int insertSelective(IfsReportTytzBct record) {
        return ifsReportTytzBctMapper.insertSelective(record);
    }

    @Override
    public List<IfsReportTytzBct> getDate(Map<String, Object> map) {
        ifsReportTytzBctMapper.callBct(map);
        return ifsReportTytzBctMapper.getDateBct(map);
    }


    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String,Object> mapEnd=new HashMap<>();
        List<IfsReportTytzBct> list=ifsReportTytzBctMapper.getDateBct(map);
        mapEnd.put("list",list);
        return mapEnd;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}

