package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.hrbnewport.mapper.IfsReportG1102Mapper;
import com.singlee.hrbnewport.model.IfsReportG1102;
import com.singlee.hrbnewport.service.IfsReportG1102ReportService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author Administrator
 * <p>
 * G1102资产质量及准备金表
 */
@Service
public class IfsReportG1102ReportServiceImpl implements IfsReportG1102ReportService {


    @Autowired
    private IfsReportG1102Mapper ifsReportG1102Mapper;

    private static Logger logger = Logger.getLogger(IfsReportG1102ReportServiceImpl.class);


    @Override
    public Page<IfsReportG1102> searchIfsReportG1102Page(Map<String, String> map) {
        int pageNumber = ParameterUtil.getInt(map, "pageNumber", 10);
        int pageSize = ParameterUtil.getInt(map, "pageSize", 1);
        List<IfsReportG1102> ifsReportG1102List = ifsReportG1102Mapper.selectAll();
        return HrbReportUtils.producePage(ifsReportG1102List, pageNumber, pageSize);
    }

    @Override
    public SlOutBean ifsReportG1102Create(Map<String, String> map) {
        SlOutBean msg = new SlOutBean(RetStatusEnum.S, "error.common.0000", "【G1102-资产质量及准备金表】生成成功！");
        ifsReportG1102Mapper.ifsReportG1102Create(map);
        String RETCODE = map.get("RETCODE");
        if (!"999".equals(RETCODE)) {
            logger.info("【G1102-资产质量及准备金表】生成失败!");
            return new SlOutBean(RetStatusEnum.F, RETCODE, "【G1102-资产质量及准备金表】生成失败!");
        }
        msg.setRetCode("999");
        msg.setRetMsg("【G1102-资产质量及准备金表】生成成功！");
        logger.info("【G1102-资产质量及准备金表】生成成功！");
        return msg;
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        List<IfsReportG1102> ifsReportG1102List = ifsReportG1102Mapper.selectAll();
        Map<String, Object> rustmap = new HashMap<>();
        rustmap.put("list",ifsReportG1102List);
        return rustmap;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}