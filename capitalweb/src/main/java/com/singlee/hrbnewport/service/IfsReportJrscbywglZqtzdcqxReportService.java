package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportJrscbywglZqtzdcqx;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;


/**
 * @author Administrator
 * <p>
 * 债券投资待偿期限结构表
 */
@Service
public interface IfsReportJrscbywglZqtzdcqxReportService extends CommonExportService {

    Page<IfsReportJrscbywglZqtzdcqx> searchIfsReportJrscbywglZqtzdcqxPage(@RequestBody Map<String, String> map);

    SlOutBean ifsReportJrscbywglZqtzdcqxCreate(Map<String, String> map);
}
 