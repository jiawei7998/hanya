package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.hrbnewport.mapper.RhSalesReportMapper;
import com.singlee.hrbnewport.service.RhSalesReportService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Liang
 * @date 2021/11/9 14:21
 * =======================
 */
@Service
@Transactional(value = "transactionManager", rollbackFor = Exception.class)
public class RhSalesReportServiceImpl implements RhSalesReportService {
    @Autowired
    private RhSalesReportMapper rhSalesReportMapper;


    public Page<Map<String, Object>> getrhSalesReportMap(Map<String, String> map)  {

        String dealtype = map.get("dealtype");
        //分页要素
        RowBounds rb = ParameterUtil.getRowBounds(map);
        String datadate= map.get("queryDate");
        if (StringUtil.isNullOrEmpty(datadate)){
            JY.raise("查询条件为空！");
        }
        datadate=datadate.replace("-", "");

        HashMap<String, Object> mapEnd = new HashMap<String, Object>();
        mapEnd.put("DATADATE", datadate);
        mapEnd.put("BUSINESSTYPE", dealtype);
        Page<Map<String, Object>> page = null;
        //调用存储过程生成数据
        rhSalesReportMapper.RHJSReportCreate(mapEnd);
        //判断存储古过程是否执行成功
        if ("999".equals(mapEnd.get("RETCODE"))){
            JY.info(mapEnd.get("RETMSG").toString());
        }else if("100".equals(mapEnd.get("RETCODE"))){
            JY.raise(mapEnd.get("RETMSG").toString());
        }else {
            JY.raise("生成数据失败，请检查再执行！");
        }
        try{
            if ("1".equals(dealtype)) {
                page= rhSalesReportMapper.sql1(rb);
            } else if ("2".equals(dealtype)) {
                page = rhSalesReportMapper.sql2(rb);
            } else if("3".equals(dealtype)){
                page=rhSalesReportMapper.sql3(rb);
            }else if ("4".equals(dealtype)){
                page=rhSalesReportMapper.sql4(rb);
            }else if ("5".equals(dealtype)){
                page=rhSalesReportMapper.sql5(rb);
            }else if ("6".equals(dealtype)){
                page=rhSalesReportMapper.sql6(rb);
            }else if ("7".equals(dealtype)){
                page=rhSalesReportMapper.sql7(rb);
            }else if ("8".equals(dealtype)){
                page=rhSalesReportMapper.sql8(rb);
            }else if ("9".equals(dealtype)){
                page=rhSalesReportMapper.sql9(rb);
            }else if ("10".equals(dealtype)){
                page=rhSalesReportMapper.sql10(rb);
            }else if ("11".equals(dealtype)){
                page=rhSalesReportMapper.sql11(rb);
            }
        }catch (Exception e){
            JY.raise("查询数据异常！");
        }

        return page;
    }


    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        List<Map<String, Object>> page = new ArrayList<Map<String, Object>>();
        Map<String, Object> result = new HashMap<String, Object>();
        String dealtype = ParameterUtil.getString(map,"sheetType","");
        if ("1".equals(dealtype)) {
            page= rhSalesReportMapper.sql1();
        } else if ("2".equals(dealtype)) {
            page = rhSalesReportMapper.sql2();
        } else if("3".equals(dealtype)){
            page=rhSalesReportMapper.sql3();
        }else if ("4".equals(dealtype)){
            page=rhSalesReportMapper.sql4();
        }else if ("5".equals(dealtype)){
            page=rhSalesReportMapper.sql5();
        }else if ("6".equals(dealtype)){
            page=rhSalesReportMapper.sql6();
        }else if ("7".equals(dealtype)){
            page=rhSalesReportMapper.sql7();
        }else if ("8".equals(dealtype)){
            page=rhSalesReportMapper.sql8();
        }else if ("9".equals(dealtype)){
            page=rhSalesReportMapper.sql9();
        }else if ("10".equals(dealtype)){
            page=rhSalesReportMapper.sql10();
        }else if ("11".equals(dealtype)){
            page=rhSalesReportMapper.sql11();
        }
        String queryDate = com.singlee.capital.common.util.ParameterUtil.getString(map,"queryDate", DateUtil.getCurrentDateAsString());
        map.put("queryDate",queryDate);
        result.put("list",page);
//        result.put("instName","哈尔滨银行股份有限公司");
        result.put("year",queryDate.substring(0,4));
        result.put("month",queryDate.substring(5,7));
//        result.put("day",queryDate.substring(8,10));
        return result;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}
