package com.singlee.hrbnewport.service;

import com.singlee.hrbnewport.model.IfsReportTytzFxBtycc;

import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 16:33
 * @description：${description}
 * @modified By：
 * @version:   
 */
public interface IfsReportTytzFxBtyccService extends CommonExportService{


    int insert(IfsReportTytzFxBtycc record);

    int insertSelective(IfsReportTytzFxBtycc record);


    List<IfsReportTytzFxBtycc> getDate(Map<String,Object> map);

}
