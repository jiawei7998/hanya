package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbnewport.mapper.IfsReportPeoplebankQuarterlyMapper;
import com.singlee.hrbnewport.service.IfsReportPeoplebankQuarterlyService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class IfsReportPeoplebankQuarterlyServiceImpl implements IfsReportPeoplebankQuarterlyService{

    @Resource
    private IfsReportPeoplebankQuarterlyMapper ifsReportPeoplebankQuarterlyMapper;

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String, Object> result = new HashMap<String, Object>();
        ifsReportPeoplebankQuarterlyMapper.generateData(map);
        List<Map<String, Object>> mapList = ifsReportPeoplebankQuarterlyMapper.getData(map);
        for(int i =0;i<mapList.size();i++){
            result.put("map",mapList.get(i));
        }
        return result;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        ifsReportPeoplebankQuarterlyMapper.generateData(map);
        return ifsReportPeoplebankQuarterlyMapper.getData(map, ParameterUtil.getRowBounds(map));
    }
}

