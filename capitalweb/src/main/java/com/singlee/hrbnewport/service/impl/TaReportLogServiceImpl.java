package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbnewport.mapper.TaReportLogMapper;
import com.singlee.hrbnewport.model.TaReportLog;
import com.singlee.hrbnewport.service.TaReportLogService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;

@Service
public class TaReportLogServiceImpl implements TaReportLogService {

	@Resource
	private TaReportLogMapper taReportLogMapper;

	@Override
	public int insert(TaReportLog record) {
		return taReportLogMapper.insert(record);
	}

	@Override
	public int insertSelective(TaReportLog record) {
		return taReportLogMapper.insertSelective(record);
	}

	@Override
	public Page<Map<String, Object>> getPageList(Map<String, Object> map) {
		return taReportLogMapper.getPageList(map, ParameterUtil.getRowBounds(map));
	}

}

