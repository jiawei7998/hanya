package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportXzqtz;
import com.singlee.hrbreport.service.ReportService;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;


/**
 * @author Administrator
 * 新债券投资情况表
 */
@Service
public interface NewBondInvestReportService extends CommonExportService {

    Page<IfsReportXzqtz> searchIfsReportXzqtzPage(@RequestBody Map<String, String> map);

    SlOutBean ifsReportXzqtzCreate(Map<String, String> map);
}
 