package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.model.TbZyzjinfo;

import java.util.Map;

public interface TbZyzjinfoService extends CommonExportService{


    Page<TbZyzjinfo> selectReport(Map<String,String > map);

}
