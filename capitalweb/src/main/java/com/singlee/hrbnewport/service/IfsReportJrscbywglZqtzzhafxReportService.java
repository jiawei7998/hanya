package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportJrscbywglZqtzzhafx;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;


/**
 * @author Administrator
 * <p>
 * 债券投资账户结构表按风险账户
 */
@Service
public interface IfsReportJrscbywglZqtzzhafxReportService extends CommonExportService {

    Page<IfsReportJrscbywglZqtzzhafx> searchIfsReportJrscbywglZqtzzhafxPage(@RequestBody Map<String, String> map);

    SlOutBean ifsReportJrscbywglZqtzzhafxCreate(Map<String, String> map);
}
 