package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportFhzqmx;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;


/**
 * @author Administrator
 * <p>
 * 分行债券明细表
 */
@Service
public interface IfsReportFhzqmxReportService extends CommonExportService {

    Page<IfsReportFhzqmx> searchIfsReportFhzqmxPage(@RequestBody Map<String, String> map);

    SlOutBean ifsReportFhzqmxCreate(Map<String, String> map);
}
 