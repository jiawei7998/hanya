package com.singlee.hrbnewport.service;

import com.singlee.hrbnewport.model.IfsReportTytzZfBct;

import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 16:22
 * @description：${description}
 * @modified By：
 * @version:   
 */
public interface IfsReportTytzZfBctService extends CommonExportService{


    int insert(IfsReportTytzZfBct record);

    int insertSelective(IfsReportTytzZfBct record);

    List<IfsReportTytzZfBct> getDate(Map<String,Object> map);

}
