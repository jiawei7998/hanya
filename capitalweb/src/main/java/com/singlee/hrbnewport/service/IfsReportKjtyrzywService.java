package com.singlee.hrbnewport.service;

import com.singlee.hrbnewport.model.IfsReportKjtyrzyw;

import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 20:50
 * @description：${description}
 * @modified By：
 * @version:   
 */
public interface IfsReportKjtyrzywService extends CommonExportService{


    int insert(IfsReportKjtyrzyw record);

    int insertSelective(IfsReportKjtyrzyw record);

    List<IfsReportKjtyrzyw> getDate(Map<String,Object> map);

    void  upDate(IfsReportKjtyrzyw record);

}
