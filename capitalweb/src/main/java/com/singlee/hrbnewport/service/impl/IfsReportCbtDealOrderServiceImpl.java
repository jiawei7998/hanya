package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbnewport.mapper.IfsReportCbtDealOrderMapper;
import com.singlee.hrbnewport.service.IfsReportCbtDealOrderService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class IfsReportCbtDealOrderServiceImpl implements IfsReportCbtDealOrderService{

    @Resource
    private IfsReportCbtDealOrderMapper ifsReportCbtDealOrderMapper;


    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String, Object> result = new HashMap<String, Object>();
        ifsReportCbtDealOrderMapper.generateData(map);
        List<Map<String, Object>> mapList = ifsReportCbtDealOrderMapper.getData(map);
        result.put("list",mapList);
        return result;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        ifsReportCbtDealOrderMapper.generateData(map);
        return ifsReportCbtDealOrderMapper.getData(map, ParameterUtil.getRowBounds(map));
    }
}
