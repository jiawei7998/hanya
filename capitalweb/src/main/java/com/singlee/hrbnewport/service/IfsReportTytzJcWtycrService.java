package com.singlee.hrbnewport.service;

import com.singlee.hrbnewport.model.IfsReportTytzJcWtycr;

import java.util.List;
import java.util.Map;

/**
 * @author ：chenguo
 * @date ：Created in 2021/12/6 16:21
 * @description：${description}
 * @modified By：
 * @version:
 */
public interface IfsReportTytzJcWtycrService extends CommonExportService  {


    int insert(IfsReportTytzJcWtycr record);

    int insertSelective(IfsReportTytzJcWtycr record);

    List<IfsReportTytzJcWtycr> getDate(Map<String,Object> map);

}

