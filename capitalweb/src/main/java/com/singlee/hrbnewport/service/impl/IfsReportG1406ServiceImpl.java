package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbnewport.mapper.IfsReportG1406Mapper;
import com.singlee.hrbnewport.model.IfsReportCdPosition;
import com.singlee.hrbnewport.model.IfsReportG1406;
import com.singlee.hrbnewport.service.IfsReportG1406Service;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * G1406-大额风险暴露
 * by zk
 */
@Service
public class IfsReportG1406ServiceImpl implements IfsReportG1406Service {

    @Autowired
    IfsReportG1406Mapper ifsReportG1406Mapper;

    /****************************************页面查询********************************************************/
    @Override
    public void creatG1406(Map<String, Object> param) {
        Object queryDate = param.get("queryDate");
        param.put("datadate", queryDate!=null&&!"".equals(queryDate.toString().trim()) ?
                queryDate.toString().trim() : DateUtil.getCurrentDateAsString());
        ifsReportG1406Mapper.createG1406Report(param);
    }

    @Override
    public Page<IfsReportG1406> searchIfsReportG1406(Map<String, Object> map) {
        RowBounds rowBounds = ParameterUtil.getRowBounds(map);
        Page<IfsReportG1406> ifsReportG1406s = ifsReportG1406Mapper.searchAllPage(map, rowBounds);
        return ifsReportG1406s;
    }

    /****************************************报表导出********************************************************/
    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        HashMap<String, Object> result = new HashMap<>();
//        creatG1406(map);
        List<IfsReportG1406> ifsReportG1406s = searchIfsReportG1406List(map);
        result.put("list",ifsReportG1406s);
        return result;
    }

    private List<IfsReportG1406> searchIfsReportG1406List(Map<String, Object> map) {
        return ifsReportG1406Mapper.searchAll(map);
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}
