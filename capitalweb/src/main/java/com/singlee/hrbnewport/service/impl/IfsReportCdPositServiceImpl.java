package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbnewport.mapper.IfsReportCdPositMapper;
import com.singlee.hrbnewport.model.IfsReportCdPosit;
import com.singlee.hrbnewport.service.IfsReportCdPositService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *  同 业 存 单
 * 2021/12/9
 * @Auther:zk
 */
@Service
public class IfsReportCdPositServiceImpl implements IfsReportCdPositService {

    @Autowired
    IfsReportCdPositMapper ifsReportCdPositMapper;

    /**********************页面查询******************************/
    @Override
    public Page<IfsReportCdPosit> searchAllPage(Map<String, Object> map) {
        Object temDate = map.get("queryDate");
        String queryDate = temDate!=null&&!"".equals(temDate.toString().trim())?temDate.toString().trim(): DateUtil.getCurrentDateAsString().substring(0,4);
        map.put("queryDate", queryDate);
        ifsReportCdPositMapper.createData(map);
        Page<IfsReportCdPosit> ifsReportCdPosits = ifsReportCdPositMapper.searchAllPage(map, ParameterUtil.getRowBounds(map));
        return ifsReportCdPosits;
    }
    /**********************报表导出******************************/
    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        HashMap<String, Object> result = new HashMap<>();
        List<IfsReportCdPosit> ifsReportCdPosits = searchAll(map);
        result.put("list",ifsReportCdPosits);
        return result;
    }

    private List<IfsReportCdPosit> searchAll(Map<String, Object> map) {
        Object temDate = map.get("queryDate");
        String queryDate = temDate!=null&&!"".equals(temDate.toString().trim())?temDate.toString().trim(): DateUtil.getCurrentDateAsString().substring(0,4);
        map.put("queryDate", queryDate);
        ifsReportCdPositMapper.createData(map);
        List<IfsReportCdPosit> ifsReportCdPosits = ifsReportCdPositMapper.searchAll(map);
        return ifsReportCdPosits;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}
