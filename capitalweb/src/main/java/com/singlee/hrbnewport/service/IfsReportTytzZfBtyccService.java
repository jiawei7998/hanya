package com.singlee.hrbnewport.service;

import com.singlee.hrbnewport.model.IfsReportTytzZfBtycc;

import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 16:22
 * @description：${description}
 * @modified By：
 * @version:   
 */
public interface IfsReportTytzZfBtyccService  extends CommonExportService {


    int insert(IfsReportTytzZfBtycc record);

    int insertSelective(IfsReportTytzZfBtycc record);

    List<IfsReportTytzZfBtycc> getDate(Map<String,Object> map);

}
