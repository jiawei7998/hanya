package com.singlee.hrbnewport.service;

import com.singlee.hrbnewport.model.IfsReportTytzBtc;
import com.singlee.hrbnewport.model.IfsReportTytzWct;

import java.util.List;
import java.util.Map;

/**
 * @author ：chenguo
 * @date ：Created in 2021/12/3 15:28
 * @description：${description}
 * @modified By：
 * @version:
 */
public interface IfsReportTytzWctService extends  CommonExportService{


    int insert(IfsReportTytzWct record);

    int insertSelective(IfsReportTytzWct record);

    List<IfsReportTytzWct> getDate(Map<String,Object> map);
}

