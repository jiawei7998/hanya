package com.singlee.hrbnewport.service;

import com.singlee.hrbnewport.model.IfsReportTytzZfBtc;

import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 16:22
 * @description：${description}
 * @modified By：
 * @version:   
 */
public interface IfsReportTytzZfBtcService extends CommonExportService{


    int insert(IfsReportTytzZfBtc record);

    int insertSelective(IfsReportTytzZfBtc record);

    List<IfsReportTytzZfBtc> getDate(Map<String,Object> map);

}
