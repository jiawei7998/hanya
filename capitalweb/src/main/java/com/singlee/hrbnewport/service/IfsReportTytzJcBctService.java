package com.singlee.hrbnewport.service;

import com.singlee.hrbnewport.model.IfsReportTytzJcBct;

import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 16:20
 * @description：${description}
 * @modified By：
 * @version:   
 */
public interface IfsReportTytzJcBctService extends CommonExportService{


    int insert(IfsReportTytzJcBct record);

    int insertSelective(IfsReportTytzJcBct record);

    List<IfsReportTytzJcBct> getDate(Map<String,Object> map);

}
