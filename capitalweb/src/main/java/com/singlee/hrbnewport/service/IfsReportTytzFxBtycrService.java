package com.singlee.hrbnewport.service;

import com.singlee.hrbnewport.model.IfsReportTytzFxBtycr;

import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 16:33
 * @description：${description}
 * @modified By：
 * @version:   
 */
public interface IfsReportTytzFxBtycrService extends CommonExportService{


    int insert(IfsReportTytzFxBtycr record);

    int insertSelective(IfsReportTytzFxBtycr record);

    List<IfsReportTytzFxBtycr> getDate(Map<String,Object> map);

}
