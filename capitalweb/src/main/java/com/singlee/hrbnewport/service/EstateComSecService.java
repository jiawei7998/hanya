package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.model.IfsReportEstateCom;

import java.util.Map;

/**
 * _房地产融资风险检测表
 * @author zk
 */
public interface EstateComSecService extends CommonExportService {

    Page<IfsReportEstateCom> selectAllReport(Map<String , Object> param);

    void createEstate(Map<String , Object> param);

}
