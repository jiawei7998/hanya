package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.mapper.IfsReportTytzBtycrMapper;
import com.singlee.hrbnewport.model.IfsReportTytzBtycr;
import com.singlee.hrbnewport.service.IfsReportTytzBtycrService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ：chenguo
 * @date ：Created in 2021/12/3 15:27
 * @description：${description}
 * @modified By：
 * @version:
 */
@Service
public class IfsReportTytzBtycrServiceImpl implements IfsReportTytzBtycrService {

    @Resource
    private IfsReportTytzBtycrMapper ifsReportTytzBtycrMapper;

    @Override
    public int insert(IfsReportTytzBtycr record) {
        return ifsReportTytzBtycrMapper.insert(record);
    }

    @Override
    public int insertSelective(IfsReportTytzBtycr record) {
        return ifsReportTytzBtycrMapper.insertSelective(record);
    }

    @Override
    public List<IfsReportTytzBtycr> getDate(Map<String, Object> map) {
        ifsReportTytzBtycrMapper.call(map);

        return ifsReportTytzBtycrMapper.getDate(map);
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String,Object> mapEnd=new HashMap<>();
        List<IfsReportTytzBtycr> list=ifsReportTytzBtycrMapper.getDate(map);
        mapEnd.put("list",list);
        return mapEnd;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}

