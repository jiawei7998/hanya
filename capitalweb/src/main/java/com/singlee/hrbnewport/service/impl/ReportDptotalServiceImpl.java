package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbnewport.mapper.ReportDptotalMapper;
import com.singlee.hrbnewport.service.CommonExportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Liang
 * @date 2021/11/6 18:27
 * =======================
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class ReportDptotalServiceImpl implements CommonExportService {

    @Autowired
    private ReportDptotalMapper reportDptotalMapper;



    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String, Object> result = new HashMap<String, Object>();
        String queryDate = ParameterUtil.getString(map,"queryDate", DateUtil.getCurrentDateAsString());
        map.put("queryDate",queryDate);
        List<Map<String, Object>> list=reportDptotalMapper.getReportDptotal();
        for (int i = 0; i <list.size() ; i++) {
            Map<String, Object> paramer = list.get(i);
            result.put("map"+ i,paramer);
        }
//        result.put("instName","哈尔滨银行股份有限公司");
        result.put("year",queryDate.substring(0,4));
//        result.put("month",queryDate.substring(5,7));
        return result;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        Page<Map<String, Object>> result =null;
        //调用存储过程生成数据
        reportDptotalMapper.DptotalCreate(map);
        if ("999".equals(map.get("RETCODE"))) {
            result = reportDptotalMapper.getReportDptotal();
        }else {
            JY.raise("生成数据失败，请检查再执行！");
        }
        return result;
    }
}
