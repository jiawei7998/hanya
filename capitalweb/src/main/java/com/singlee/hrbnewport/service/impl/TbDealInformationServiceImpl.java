package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.financial.common.util.ParameterUtil;
import com.singlee.hrbnewport.mapper.TbDealInformationMapper;
import com.singlee.hrbnewport.model.TbDealInformation;
import com.singlee.hrbnewport.service.TbDealInformationService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TbDealInformationServiceImpl implements TbDealInformationService{

    @Autowired
    private TbDealInformationMapper tbDealInformationMapper;


    @Override
    public Page<TbDealInformation> selectReport(Map<String, String> map) {

        RowBounds rb= ParameterUtil.getRowBounds(map);
        //调用存储过程生成数据
        tbDealInformationMapper.ZJJYXXReportCreate(map);
        return tbDealInformationMapper.getTbDealInformation(rb);

    }

    /**
     * 导出报表
     * @param map
     * @return
     */
    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String, Object> result = new HashMap<String, Object>();
        String queryDate = com.singlee.capital.common.util.ParameterUtil.getString(map,"queryDate", DateUtil.getCurrentDateAsString());
        map.put("queryDate",queryDate);
        List<TbDealInformation> entry=tbDealInformationMapper.getTbDealInformation();
        result.put("list",entry);
//        result.put("instName","哈尔滨银行股份有限公司");
        result.put("year",queryDate.substring(0,4));
        result.put("month",queryDate.substring(5,7));
        result.put("day",queryDate.substring(8,10));
        return result;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}
