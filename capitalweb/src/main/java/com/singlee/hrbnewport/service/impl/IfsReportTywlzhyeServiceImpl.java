package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.mapper.IfsReportTywlzhyeMapper;
import com.singlee.hrbnewport.model.IfsReportTywlzhye;
import com.singlee.hrbnewport.service.IfsReportTywlzhyeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 20:51
 * @description：${description}
 * @modified By：
 * @version:   
 */
@Service
public class IfsReportTywlzhyeServiceImpl implements IfsReportTywlzhyeService{

    @Resource
    private IfsReportTywlzhyeMapper ifsReportTywlzhyeMapper;

    @Override
    public int insert(IfsReportTywlzhye record) {
        return ifsReportTywlzhyeMapper.insert(record);
    }

    @Override
    public int insertSelective(IfsReportTywlzhye record) {
        return ifsReportTywlzhyeMapper.insertSelective(record);
    }

    @Override
    public List<IfsReportTywlzhye> getDate(Map<String, Object> map) {
        List<IfsReportTywlzhye> list2=  ifsReportTywlzhyeMapper.getDate(map);
        if (list2==null||list2.size()==0){
            ifsReportTywlzhyeMapper.call(map);
            return ifsReportTywlzhyeMapper.getDate(map);
        }
        return  list2;
    }

    @Override
    public void upDate(IfsReportTywlzhye record) {
        ifsReportTywlzhyeMapper.updateByPrimaryKey(record);
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String,Object> mapEnd=new HashMap<>();
        List<IfsReportTywlzhye> list=ifsReportTywlzhyeMapper.getDate(map);
        mapEnd.put("list",list);
        return mapEnd;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}
