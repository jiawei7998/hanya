package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.mapper.IfsReportRmbtywlzhMapper;
import com.singlee.hrbnewport.model.IfsReportRmbtywlzh;
import com.singlee.hrbnewport.service.IfsReportRmbtywlzhService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 20:51
 * @description：${description}
 * @modified By：
 * @version:   
 */
@Service
public class IfsReportRmbtywlzhServiceImpl implements IfsReportRmbtywlzhService{

    @Resource
    private IfsReportRmbtywlzhMapper ifsReportRmbtywlzhMapper;



    @Override
    public List<IfsReportRmbtywlzh> getDate(Map<String, Object> map) {
        List<IfsReportRmbtywlzh> list2 = ifsReportRmbtywlzhMapper.getDate(map);
        if (list2 == null || list2.size() == 0) {
            ifsReportRmbtywlzhMapper.call(map);
            return ifsReportRmbtywlzhMapper.getDate(map);
        }
        return list2;
    }

    @Override
    public void upDate(IfsReportRmbtywlzh record) {
        ifsReportRmbtywlzhMapper.updateByPrimaryKey(record);
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String,Object> mapEnd=new HashMap<>();
        List<IfsReportRmbtywlzh> list=ifsReportRmbtywlzhMapper.getDate(map);
        mapEnd.put("list",list);
        return mapEnd;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}
