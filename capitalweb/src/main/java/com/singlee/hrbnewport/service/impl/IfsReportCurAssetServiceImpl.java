package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbnewport.mapper.IfsReportCurAssetMapper;
import com.singlee.hrbnewport.model.IfsReportCurAsset;
import com.singlee.hrbnewport.service.IfsReportCurAssetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 哈尔滨银行优质流动性资产台账
 * 2021/12/9
 * @Auther:zk
 */
@Service
public class IfsReportCurAssetServiceImpl implements IfsReportCurAssetService {

    @Autowired
    IfsReportCurAssetMapper ifsReportCurAssetMapper;

    /**********************页面查询******************************/
    @Override
    public Page<IfsReportCurAsset> searchAllPage(Map<String, Object> map) {
        Object temDate = map.get("queryDate");
        String queryDate = temDate!=null&&!"".equals(temDate.toString().trim())?temDate.toString().trim(): DateUtil.getCurrentDateAsString();
        map.put("queryDate", queryDate);

        ifsReportCurAssetMapper.createData(map);
        Page<IfsReportCurAsset> ifsReportCurAssets = ifsReportCurAssetMapper.searchAllPage(map, ParameterUtil.getRowBounds(map));
        return ifsReportCurAssets;
    }
    /**********************报表导出******************************/
    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        HashMap<String, Object> result = new HashMap<>();
        List<IfsReportCurAsset> ifsReportCurAssets = searchAll(map);
        result.put("list", ifsReportCurAssets);
        String queryDate = ParameterUtil.getString(map,"queryDate", DateUtil.getCurrentDateAsString());
        result.put("year",queryDate.substring(0,4));
        result.put("month",queryDate.substring(5,7));
        result.put("day",queryDate.substring(8,10));
        return result;
    }

    private List<IfsReportCurAsset> searchAll(Map<String, Object> map) {
        Object temDate = map.get("queryDate");
        String queryDate = temDate!=null&&!"".equals(temDate.toString().trim())?temDate.toString().trim(): DateUtil.getCurrentDateAsString();
        map.put("queryDate", queryDate);

        ifsReportCurAssetMapper.createData(map);
        List<IfsReportCurAsset> ifsReportCurAssets = ifsReportCurAssetMapper.searchAll(map);
        return ifsReportCurAssets;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}
