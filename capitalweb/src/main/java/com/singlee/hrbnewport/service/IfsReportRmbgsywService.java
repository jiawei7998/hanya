package com.singlee.hrbnewport.service;

import com.singlee.hrbnewport.model.IfsReportRmbgsyw;

import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 20:51
 * @description：${description}
 * @modified By：
 * @version:   
 */
public interface IfsReportRmbgsywService extends CommonExportService{


    int insert(IfsReportRmbgsyw record);

    int insertSelective(IfsReportRmbgsyw record);


    List<IfsReportRmbgsyw> getDate(Map<String,Object> map);

    void  upDate(IfsReportRmbgsyw record);


    List<IfsReportRmbgsyw> getBaseDate(Map<String,Object> map);
}
