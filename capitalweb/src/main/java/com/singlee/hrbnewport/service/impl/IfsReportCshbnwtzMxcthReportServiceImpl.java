package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.hrbnewport.mapper.IfsReportCshbnwtzMxcthMapper;
import com.singlee.hrbnewport.model.IfsReportCshbnwtzMxcth;
import com.singlee.hrbnewport.service.IfsReportCshbnwtzMxcthReportService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author Administrator
 * <p>
 * 表二 表内外投资业务明细表（穿透后）
 */
@Service
public class IfsReportCshbnwtzMxcthReportServiceImpl implements IfsReportCshbnwtzMxcthReportService {


    @Autowired
    private IfsReportCshbnwtzMxcthMapper ifsReportCshbnwtzMxcthMapper;

    private static Logger logger = Logger.getLogger(IfsReportCshbnwtzMxcthReportServiceImpl.class);


    @Override
    public Page<IfsReportCshbnwtzMxcth> searchIfsReportCshbnwtzMxcthPage(Map<String, String> map) {
        int pageNumber = ParameterUtil.getInt(map, "pageNumber", 10);
        int pageSize = ParameterUtil.getInt(map, "pageSize", 1);
        List<IfsReportCshbnwtzMxcth> ifsReportCshbnwtzMxcthList = ifsReportCshbnwtzMxcthMapper.selectAll();
        return HrbReportUtils.producePage(ifsReportCshbnwtzMxcthList, pageNumber, pageSize);
    }

    @Override
    public SlOutBean ifsReportCshbnwtzMxcthCreate(Map<String, String> map) {
        SlOutBean msg = new SlOutBean(RetStatusEnum.S, "error.common.0000", "【城商行表内外投资业务和同业交易对手情况表-表内外投资业务明细表（穿透后）】生成成功！");
        ifsReportCshbnwtzMxcthMapper.ifsReportCshbnwtzMxcthCreate(map);
        String RETCODE = map.get("RETCODE");
        if (!"999".equals(RETCODE)) {
            logger.info("【城商行表内外投资业务和同业交易对手情况表-表内外投资业务明细表（穿透后）】生成失败!");
            return new SlOutBean(RetStatusEnum.F, RETCODE, "【城商行表内外投资业务和同业交易对手情况表-表内外投资业务明细表（穿透后）】生成失败!");
        }
        msg.setRetCode("999");
        msg.setRetMsg("【城商行表内外投资业务和同业交易对手情况表-表内外投资业务明细表（穿透后）】生成成功！");
        logger.info("【城商行表内外投资业务和同业交易对手情况表-表内外投资业务明细表（穿透后）】生成成功！");
        return msg;
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        String queryDate = ParameterUtil.getString(map,"queryDate","");
        queryDate = DateUtil.format(DateUtil.parse(queryDate), "yyyy年MM月dd日");
        List<IfsReportCshbnwtzMxcth> ifsReportCshbnwtzMxcthList = ifsReportCshbnwtzMxcthMapper.selectAll();
        Map<String, Object> rustmap = new HashMap<>();
        rustmap.put("list",ifsReportCshbnwtzMxcthList);
        rustmap.put("hz","");//汇总
        rustmap.put("sjsd",queryDate);//数据时点
        return rustmap;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}