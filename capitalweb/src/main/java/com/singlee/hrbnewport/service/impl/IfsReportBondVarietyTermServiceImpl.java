package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.mapper.IfsReportBondVarietyTermMapper;
import com.singlee.hrbnewport.service.IfsReportBondVarietyTermService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class IfsReportBondVarietyTermServiceImpl implements IfsReportBondVarietyTermService{

    @Resource
    private IfsReportBondVarietyTermMapper ifsReportBondVarietyTermMapper;


    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String, Object> result = new HashMap<String, Object>();
        ifsReportBondVarietyTermMapper.generateData(map);
        List<Map<String, Object>> mapList = ifsReportBondVarietyTermMapper.getData(map);
        for(int i =0;i<mapList.size();i++){
            result.put("map"+(i+1),mapList.get(i));
        }
        return result;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        ifsReportBondVarietyTermMapper.generateData(map);
        return ifsReportBondVarietyTermMapper.getData(map);

    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}
