package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.mapper.IfsReportTytzFxWctMapper;
import com.singlee.hrbnewport.model.IfsReportTytzFxWct;
import com.singlee.hrbnewport.service.IfsReportTytzFxWctService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 16:21
 * @description：${description}
 * @modified By：
 * @version:   
 */
@Service
public class IfsReportTytzFxWctServiceImpl implements IfsReportTytzFxWctService{

    @Resource
    private IfsReportTytzFxWctMapper ifsReportTytzFxWctMapper;

    @Override
    public int insert(IfsReportTytzFxWct record) {
        return ifsReportTytzFxWctMapper.insert(record);
    }

    @Override
    public int insertSelective(IfsReportTytzFxWct record) {
        return ifsReportTytzFxWctMapper.insertSelective(record);
    }

    @Override
    public List<IfsReportTytzFxWct> getDate(Map<String, Object> map) {
        ifsReportTytzFxWctMapper.call(map);
        return ifsReportTytzFxWctMapper.getDate(map);
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String,Object> mapEnd=new HashMap<>();
        List<IfsReportTytzFxWct> list=ifsReportTytzFxWctMapper.getDate(map);
        mapEnd.put("list",list);
        return mapEnd;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}
