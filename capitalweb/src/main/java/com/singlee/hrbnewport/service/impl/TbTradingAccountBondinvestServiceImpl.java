package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbnewport.mapper.TbTradingAccountBondinvestMapper;
import com.singlee.hrbnewport.model.TbTradingAccountBondinvest;
import com.singlee.hrbnewport.service.TbTradingAccountBondinvestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TbTradingAccountBondinvestServiceImpl implements TbTradingAccountBondinvestService{

    @Autowired
    private TbTradingAccountBondinvestMapper tbTradingAccountBondinvestMapper;


    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String, Object> result = new HashMap<String, Object>();
        String edate = ParameterUtil.getString(map,"queryDate", DateUtil.getCurrentDateAsString());
        String sdate = ParameterUtil.getString(map,"sdate", DateUtil.getCurrentDateAsString());
        List<TbTradingAccountBondinvest> list=tbTradingAccountBondinvestMapper.getAccountBondInvest();
        result.put("list",list);
        result.put("sdate",sdate);
        result.put("edate",edate);
        return result;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<TbTradingAccountBondinvest> selectReport(Map<String, String> map) {
        tbTradingAccountBondinvestMapper.accountBondInvestCreate(map);
        return tbTradingAccountBondinvestMapper.getAccountBondInvest();
    }
}
