package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportCshbnwtzQxjgcbsy;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;


/**
 * @author Administrator
 * <p>
 * 表四 表内外投资业务期限结构及成本收益表
 */
@Service
public interface IfsReportCshbnwtzQxjgcbsyReportService extends CommonExportService {

    Page<IfsReportCshbnwtzQxjgcbsy> searchIfsReportCshbnwtzQxjgcbsyPage(@RequestBody Map<String, String> map);

    SlOutBean ifsReportCshbnwtzQxjgcbsyCreate(Map<String, String> map);
}
 