package com.singlee.hrbnewport.service;

import com.singlee.hrbnewport.model.IfsReportTytzJcWtc;

import java.util.List;
import java.util.Map;

/**
 * @author ：chenguo
 * @date ：Created in 2021/12/6 16:20
 * @description：${description}
 * @modified By：
 * @version:
 */
public interface IfsReportTytzJcWtcService  extends CommonExportService {


    int insert(IfsReportTytzJcWtc record);

    int insertSelective(IfsReportTytzJcWtc record);

    List<IfsReportTytzJcWtc> getDate(Map<String,Object> map);

}

