package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.mapper.IfsReportTytzZfWctMapper;
import com.singlee.hrbnewport.model.IfsReportTytzZfWct;
import com.singlee.hrbnewport.service.IfsReportTytzZfWctService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 16:22
 * @description：${description}
 * @modified By：
 * @version:   
 */
@Service
public class IfsReportTytzZfWctServiceImpl implements IfsReportTytzZfWctService{

    @Resource
    private IfsReportTytzZfWctMapper ifsReportTytzZfWctMapper;

    @Override
    public int insert(IfsReportTytzZfWct record) {
        return ifsReportTytzZfWctMapper.insert(record);
    }

    @Override
    public int insertSelective(IfsReportTytzZfWct record) {
        return ifsReportTytzZfWctMapper.insertSelective(record);
    }

    @Override
    public List<IfsReportTytzZfWct> getDate(Map<String, Object> map) {
        ifsReportTytzZfWctMapper.call(map);
        return ifsReportTytzZfWctMapper.getDate(map);
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String,Object> mapEnd=new HashMap<>();
        List<IfsReportTytzZfWct> list=ifsReportTytzZfWctMapper.getDate(map);
        mapEnd.put("list",list);
        return mapEnd;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}
