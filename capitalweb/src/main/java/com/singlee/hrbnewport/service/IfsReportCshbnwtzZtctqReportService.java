package com.singlee.hrbnewport.service;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportCshbnwtzZtctq;
import com.singlee.hrbreport.service.ReportService;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;


/**
 * @author Administrator
 * <p>
 * 表一 表内外投资业务总体情况表（穿透前）
 */
@Service
public interface IfsReportCshbnwtzZtctqReportService extends CommonExportService {

    Page<IfsReportCshbnwtzZtctq> searchIfsReportCshbnwtzZtctqPage(@RequestBody Map<String, String> map);

    SlOutBean ifsReportCshbnwtzZtctqCreate(Map<String, String> map);
}
 