package com.singlee.hrbnewport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.mapper.IfsReportTytzZfWtcMapper;
import com.singlee.hrbnewport.model.IfsReportTytzZfWtc;
import com.singlee.hrbnewport.service.IfsReportTytzZfWtcService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 16:22
 * @description：${description}
 * @modified By：
 * @version:   
 */
@Service
public class IfsReportTytzZfWtcServiceImpl implements IfsReportTytzZfWtcService{

    @Resource
    private IfsReportTytzZfWtcMapper ifsReportTytzZfWtcMapper;

    @Override
    public int insert(IfsReportTytzZfWtc record) {
        return ifsReportTytzZfWtcMapper.insert(record);
    }

    @Override
    public int insertSelective(IfsReportTytzZfWtc record) {
        return ifsReportTytzZfWtcMapper.insertSelective(record);
    }

    @Override
    public List<IfsReportTytzZfWtc> getDate(Map<String, Object> map) {
        ifsReportTytzZfWtcMapper.call(map);
        return ifsReportTytzZfWtcMapper.getDate(map);
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String,Object> mapEnd=new HashMap<>();
        List<IfsReportTytzZfWtc> list=ifsReportTytzZfWtcMapper.getDate(map);
        mapEnd.put("list",list);
        return mapEnd;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}
