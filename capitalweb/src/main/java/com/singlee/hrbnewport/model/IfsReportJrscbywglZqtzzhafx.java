package com.singlee.hrbnewport.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

/**
 * 债券投资账户结构表按风险账户
 */
@Table(name = "IFS_REPORT_JRSCBYWGL_ZQTZZHAFX")
public class IfsReportJrscbywglZqtzzhafx implements Serializable {
    /**
     * 债券类型
     */
    @Column(name = "ZQTYPE")
    private String zqtype;

    /**
     * 原始期限
     */
    @Column(name = "YSQX")
    private String ysqx;

    /**
     * 交易账户_余额
     */
    @Column(name = "JYZH_YE")
    private BigDecimal jyzhYe;

    /**
     * 交易账户_占比
     */
    @Column(name = "JYZH_ZB")
    private BigDecimal jyzhZb;

    /**
     * 银行账户_余额
     */
    @Column(name = "YHZH_YE")
    private BigDecimal yhzhYe;

    /**
     * 银行账户_占比
     */
    @Column(name = "YHZH_ZB")
    private BigDecimal yhzhZb;

    /**
     * 合计_余额
     */
    @Column(name = "HJ_YE")
    private BigDecimal hjYe;

    /**
     * 合计_占比
     */
    @Column(name = "HJ_ZB")
    private BigDecimal hjZb;

    private static final long serialVersionUID = 1L;

    /**
     * 获取债券类型
     *
     * @return ZQTYPE - 债券类型
     */
    public String getZqtype() {
        return zqtype;
    }

    /**
     * 设置债券类型
     *
     * @param zqtype 债券类型
     */
    public void setZqtype(String zqtype) {
        this.zqtype = zqtype;
    }

    /**
     * 获取原始期限
     *
     * @return YSQX - 原始期限
     */
    public String getYsqx() {
        return ysqx;
    }

    /**
     * 设置原始期限
     *
     * @param ysqx 原始期限
     */
    public void setYsqx(String ysqx) {
        this.ysqx = ysqx;
    }

    /**
     * 获取交易账户_余额
     *
     * @return JYZH_YE - 交易账户_余额
     */
    public BigDecimal getJyzhYe() {
        return jyzhYe;
    }

    /**
     * 设置交易账户_余额
     *
     * @param jyzhYe 交易账户_余额
     */
    public void setJyzhYe(BigDecimal jyzhYe) {
        this.jyzhYe = jyzhYe;
    }

    /**
     * 获取交易账户_占比
     *
     * @return JYZH_ZB - 交易账户_占比
     */
    public BigDecimal getJyzhZb() {
        return jyzhZb;
    }

    /**
     * 设置交易账户_占比
     *
     * @param jyzhZb 交易账户_占比
     */
    public void setJyzhZb(BigDecimal jyzhZb) {
        this.jyzhZb = jyzhZb;
    }

    /**
     * 获取银行账户_余额
     *
     * @return YHZH_YE - 银行账户_余额
     */
    public BigDecimal getYhzhYe() {
        return yhzhYe;
    }

    /**
     * 设置银行账户_余额
     *
     * @param yhzhYe 银行账户_余额
     */
    public void setYhzhYe(BigDecimal yhzhYe) {
        this.yhzhYe = yhzhYe;
    }

    /**
     * 获取银行账户_占比
     *
     * @return YHZH_ZB - 银行账户_占比
     */
    public BigDecimal getYhzhZb() {
        return yhzhZb;
    }

    /**
     * 设置银行账户_占比
     *
     * @param yhzhZb 银行账户_占比
     */
    public void setYhzhZb(BigDecimal yhzhZb) {
        this.yhzhZb = yhzhZb;
    }

    /**
     * 获取合计_余额
     *
     * @return HJ_YE - 合计_余额
     */
    public BigDecimal getHjYe() {
        return hjYe;
    }

    /**
     * 设置合计_余额
     *
     * @param hjYe 合计_余额
     */
    public void setHjYe(BigDecimal hjYe) {
        this.hjYe = hjYe;
    }

    /**
     * 获取合计_占比
     *
     * @return HJ_ZB - 合计_占比
     */
    public BigDecimal getHjZb() {
        return hjZb;
    }

    /**
     * 设置合计_占比
     *
     * @param hjZb 合计_占比
     */
    public void setHjZb(BigDecimal hjZb) {
        this.hjZb = hjZb;
    }
}