package com.singlee.hrbnewport.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author ：meigy
 * @date ：Created in 2021/11/11 13:32
 * @description：${description}
 * @modified By：
 * @version:
 */

/**
 * 表内外投资业务基础资产情况表
 */
@Entity
@Table(name = "IFS_REPORT_CSHBNWTZ_JCZC")
public class IfsReportCshbnwtzJczc implements Serializable {
    /**
     * 底层项目_可以实时统计基础资产的产品
     */
    private String product;

    /**
     * 表外理财投资产品形式及余额_债券_本季末数额
     */
    private String bwZqBjmAmt;

    /**
     * 表外理财投资产品形式及余额_债券_上季末数额
     */
    private String bwZqSjmAmt;

    /**
     * 表外理财投资产品形式及余额_非标准化债权类投资_本季末数额
     */
    private String bwFbzzqBjmAmt;

    /**
     * 表外理财投资产品形式及余额_非标准化债权类投资_上季末数额
     */
    private String bwFbzzqSjmAmt;

    /**
     * 表外理财投资产品形式及余额_其他（请列明）_本季末数额
     */
    private String bwQtBjmAmt;

    /**
     * 表外理财投资产品形式及余额_其他（请列明）_上季末数额
     */
    private String bwQtSjmAmt;

    /**
     * 表外理财投资产品形式及余额_总计_本季末数额
     */
    private String bwZjBjmAmt;

    /**
     * 表外理财投资产品形式及余额_总计_上季末数额
     */
    private String bwZjSjmAmt;

    /**
     * 表外理财投资产品形式及余额_备注
     */
    private String bwBz;

    /**
     * 表内资金投资产品形式及余额_债券_本季末数额
     */
    private String bnZqBjmAmt;

    /**
     * 表内资金投资产品形式及余额_债券_上季末数额
     */
    private String bnZqSjmAmt;

    /**
     * 表内资金投资产品形式及余额_非标准化债权类投资_本季末数额
     */
    private String bnFbzzqBjmAmt;

    /**
     * 表内资金投资产品形式及余额_非标准化债权类投资_上季末数额
     */
    private String bnFbzzqSjmAmt;

    /**
     * 表内资金投资产品形式及余额_其他（请列明）_本季末数额
     */
    private String bnQtBjmAmt;

    /**
     * 表内资金投资产品形式及余额_其他（请列明）_上季末数额
     */
    private String bnQtSjmAmt;

    /**
     * 表内资金投资产品形式及余额_总计_本季末数额
     */
    private String bnZjBjmAmt;

    /**
     * 表内资金投资产品形式及余额_总计_上季末数额
     */
    private String bnZjSjmAmt;

    /**
     * 表外理财投资产品形式及余额_备注
     */
    private String bnBz;

    private String parentProduct;

    private static final long serialVersionUID = 1L;

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getBwZqBjmAmt() {
        return bwZqBjmAmt;
    }

    public void setBwZqBjmAmt(String bwZqBjmAmt) {
        this.bwZqBjmAmt = bwZqBjmAmt;
    }

    public String getBwZqSjmAmt() {
        return bwZqSjmAmt;
    }

    public void setBwZqSjmAmt(String bwZqSjmAmt) {
        this.bwZqSjmAmt = bwZqSjmAmt;
    }

    public String getBwFbzzqBjmAmt() {
        return bwFbzzqBjmAmt;
    }

    public void setBwFbzzqBjmAmt(String bwFbzzqBjmAmt) {
        this.bwFbzzqBjmAmt = bwFbzzqBjmAmt;
    }

    public String getBwFbzzqSjmAmt() {
        return bwFbzzqSjmAmt;
    }

    public void setBwFbzzqSjmAmt(String bwFbzzqSjmAmt) {
        this.bwFbzzqSjmAmt = bwFbzzqSjmAmt;
    }

    public String getBwQtBjmAmt() {
        return bwQtBjmAmt;
    }

    public void setBwQtBjmAmt(String bwQtBjmAmt) {
        this.bwQtBjmAmt = bwQtBjmAmt;
    }

    public String getBwQtSjmAmt() {
        return bwQtSjmAmt;
    }

    public void setBwQtSjmAmt(String bwQtSjmAmt) {
        this.bwQtSjmAmt = bwQtSjmAmt;
    }

    public String getBwZjBjmAmt() {
        return bwZjBjmAmt;
    }

    public void setBwZjBjmAmt(String bwZjBjmAmt) {
        this.bwZjBjmAmt = bwZjBjmAmt;
    }

    public String getBwZjSjmAmt() {
        return bwZjSjmAmt;
    }

    public void setBwZjSjmAmt(String bwZjSjmAmt) {
        this.bwZjSjmAmt = bwZjSjmAmt;
    }

    public String getBwBz() {
        return bwBz;
    }

    public void setBwBz(String bwBz) {
        this.bwBz = bwBz;
    }

    public String getBnZqBjmAmt() {
        return bnZqBjmAmt;
    }

    public void setBnZqBjmAmt(String bnZqBjmAmt) {
        this.bnZqBjmAmt = bnZqBjmAmt;
    }

    public String getBnZqSjmAmt() {
        return bnZqSjmAmt;
    }

    public void setBnZqSjmAmt(String bnZqSjmAmt) {
        this.bnZqSjmAmt = bnZqSjmAmt;
    }

    public String getBnFbzzqBjmAmt() {
        return bnFbzzqBjmAmt;
    }

    public void setBnFbzzqBjmAmt(String bnFbzzqBjmAmt) {
        this.bnFbzzqBjmAmt = bnFbzzqBjmAmt;
    }

    public String getBnFbzzqSjmAmt() {
        return bnFbzzqSjmAmt;
    }

    public void setBnFbzzqSjmAmt(String bnFbzzqSjmAmt) {
        this.bnFbzzqSjmAmt = bnFbzzqSjmAmt;
    }

    public String getBnQtBjmAmt() {
        return bnQtBjmAmt;
    }

    public void setBnQtBjmAmt(String bnQtBjmAmt) {
        this.bnQtBjmAmt = bnQtBjmAmt;
    }

    public String getBnQtSjmAmt() {
        return bnQtSjmAmt;
    }

    public void setBnQtSjmAmt(String bnQtSjmAmt) {
        this.bnQtSjmAmt = bnQtSjmAmt;
    }

    public String getBnZjBjmAmt() {
        return bnZjBjmAmt;
    }

    public void setBnZjBjmAmt(String bnZjBjmAmt) {
        this.bnZjBjmAmt = bnZjBjmAmt;
    }

    public String getBnZjSjmAmt() {
        return bnZjSjmAmt;
    }

    public void setBnZjSjmAmt(String bnZjSjmAmt) {
        this.bnZjSjmAmt = bnZjSjmAmt;
    }

    public String getBnBz() {
        return bnBz;
    }

    public void setBnBz(String bnBz) {
        this.bnBz = bnBz;
    }

    public String getParentProduct() {
        return parentProduct;
    }

    public void setParentProduct(String parentProduct) {
        this.parentProduct = parentProduct;
    }
}