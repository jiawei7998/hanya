package com.singlee.hrbnewport.model;

/**
 *
 * 2021/12/6
 * @Auther:zk
 */    
    
/**
    * 业务明细-基金管理公司及子公司专户产品
    */
public class DealdetailMoney {
    /**
    * 理财管理人
    */
    private String fundManager;

    /**
    * 理财全称
    */
    private String fundName;

    /**
    * 理财登记代码
    */
    private String fundCode;

    /**
    * 交易方向
    */
    private String dealDir;

    /**
    * 交易余额（万元）
    */
    private String amt;

    /**
    * 申购日
    */
    private String purchDate;

    public String getFundManager() {
        return fundManager;
    }

    public void setFundManager(String fundManager) {
        this.fundManager = fundManager;
    }

    public String getFundName() {
        return fundName;
    }

    public void setFundName(String fundName) {
        this.fundName = fundName;
    }

    public String getFundCode() {
        return fundCode;
    }

    public void setFundCode(String fundCode) {
        this.fundCode = fundCode;
    }

    public String getDealDir() {
        return dealDir;
    }

    public void setDealDir(String dealDir) {
        this.dealDir = dealDir;
    }

    public String getAmt() {
        return amt;
    }

    public void setAmt(String amt) {
        this.amt = amt;
    }

    public String getPurchDate() {
        return purchDate;
    }

    public void setPurchDate(String purchDate) {
        this.purchDate = purchDate;
    }
}