package com.singlee.hrbnewport.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

/**
 * S6301 大中小微型企业贷款情况表
 */
@Table(name = "IFS_REPORT_S6301")
public class IfsReportS6301 implements Serializable {
    /**
     * 项目
     */
    @Column(name = "PRODUCT")
    private String product;

    /**
     * 大型企业
     */
    @Column(name = "DXQY")
    private BigDecimal dxqy;

    /**
     * 中型企业
     */
    @Column(name = "ZXQY")
    private BigDecimal zxqy;

    /**
     * 小型企业
     */
    @Column(name = "XXQY")
    private BigDecimal xxqy;

    /**
     * 微型企业
     */
    @Column(name = "WXQY")
    private BigDecimal wxqy;

    /**
     * 个人经营性贷款
     */
    @Column(name = "GRJYXDK")
    private BigDecimal grjyxdk;

    /**
     * 其中：个体工商户贷款
     */
    @Column(name = "QZGTGSHDK")
    private BigDecimal qzgtgshdk;

    /**
     * 其中：小微企业主贷款
     */
    @Column(name = "QZXWQYZDK")
    private BigDecimal qzxwqyzdk;

    @Column(name = "PARENT_PRODUCT")
    private String parentProduct;

    private static final long serialVersionUID = 1L;

    /**
     * 获取项目
     *
     * @return PRODUCT - 项目
     */
    public String getProduct() {
        return product;
    }

    /**
     * 设置项目
     *
     * @param product 项目
     */
    public void setProduct(String product) {
        this.product = product;
    }

    /**
     * 获取大型企业
     *
     * @return DXQY - 大型企业
     */
    public BigDecimal getDxqy() {
        return dxqy;
    }

    /**
     * 设置大型企业
     *
     * @param dxqy 大型企业
     */
    public void setDxqy(BigDecimal dxqy) {
        this.dxqy = dxqy;
    }

    /**
     * 获取中型企业
     *
     * @return ZXQY - 中型企业
     */
    public BigDecimal getZxqy() {
        return zxqy;
    }

    /**
     * 设置中型企业
     *
     * @param zxqy 中型企业
     */
    public void setZxqy(BigDecimal zxqy) {
        this.zxqy = zxqy;
    }

    /**
     * 获取小型企业
     *
     * @return XXQY - 小型企业
     */
    public BigDecimal getXxqy() {
        return xxqy;
    }

    /**
     * 设置小型企业
     *
     * @param xxqy 小型企业
     */
    public void setXxqy(BigDecimal xxqy) {
        this.xxqy = xxqy;
    }

    /**
     * 获取微型企业
     *
     * @return WXQY - 微型企业
     */
    public BigDecimal getWxqy() {
        return wxqy;
    }

    /**
     * 设置微型企业
     *
     * @param wxqy 微型企业
     */
    public void setWxqy(BigDecimal wxqy) {
        this.wxqy = wxqy;
    }

    /**
     * 获取个人经营性贷款
     *
     * @return GRJYXDK - 个人经营性贷款
     */
    public BigDecimal getGrjyxdk() {
        return grjyxdk;
    }

    /**
     * 设置个人经营性贷款
     *
     * @param grjyxdk 个人经营性贷款
     */
    public void setGrjyxdk(BigDecimal grjyxdk) {
        this.grjyxdk = grjyxdk;
    }

    /**
     * 获取其中：个体工商户贷款
     *
     * @return QZGTGSHDK - 其中：个体工商户贷款
     */
    public BigDecimal getQzgtgshdk() {
        return qzgtgshdk;
    }

    /**
     * 设置其中：个体工商户贷款
     *
     * @param qzgtgshdk 其中：个体工商户贷款
     */
    public void setQzgtgshdk(BigDecimal qzgtgshdk) {
        this.qzgtgshdk = qzgtgshdk;
    }

    /**
     * 获取其中：小微企业主贷款
     *
     * @return QZXWQYZDK - 其中：小微企业主贷款
     */
    public BigDecimal getQzxwqyzdk() {
        return qzxwqyzdk;
    }

    /**
     * 设置其中：小微企业主贷款
     *
     * @param qzxwqyzdk 其中：小微企业主贷款
     */
    public void setQzxwqyzdk(BigDecimal qzxwqyzdk) {
        this.qzxwqyzdk = qzxwqyzdk;
    }

    /**
     * @return PARENT_PRODUCT
     */
    public String getParentProduct() {
        return parentProduct;
    }

    /**
     * @param parentProduct
     */
    public void setParentProduct(String parentProduct) {
        this.parentProduct = parentProduct;
    }
}