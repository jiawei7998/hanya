package com.singlee.hrbnewport.model;

import java.math.BigDecimal;

/**
 *
 * 2021/12/6
 * @Auther:zk
 */    
    
/**
    * 申购信息查询结果
    */
public class IfsReportDepositInvestorReport {
    /**
    * 工具代码
    */
    private String depositCode;

    /**
    * 工具简称
    */
    private String depositName;

    /**
    * 申购机构
    */
    private String lendInst;

    /**
    * 申购机构类型
    */
    private String finType;

    /**
    * 工具类型
    */
    private String prodtype;

    /**
    * 币种
    */
    private String currency;

    /**
    * 发行日
    */
    private String publishDate;

    /**
    * 缴款日
    */
    private String payDate;

    /**
    * 到期日
    */
    private String duedate;

    /**
    * 起息日
    */
    private String valueDate;

    /**
    * 流通范围
    */
    private String circulation;

    /**
    * 发行方式
    */
    private String issMethod;

    /**
    * 招标方式
    */
    private String inviMethod;

    /**
    * 期限
    */
    private String depositTerm;

    /**
    * 票息类型
    */
    private String interestType;

    /**
    * 实际发行量(亿元)
    */
    private BigDecimal actualAmount;

    /**
    * 成交量(亿元)
    */
    private BigDecimal totalTenderSuccessAmt;

    /**
    * 投标倍数
    */
    private String tenderMultiples;

    /**
    * 发行价格(元)
    */
    private BigDecimal publishPrice;

    /**
    * 参考收益率
    */
    private BigDecimal compareRate;

    /**
    * 票面利率(%)
    */
    private BigDecimal faceRate;

    /**
    * 基准利率
    */
    private BigDecimal baseRate;

    /**
    * 利差(BP)
    */
    private BigDecimal spread;

    /**
    * 提交用户
    */
    private String lendTrad;

    /**
    * 电话
    */
    private String lendTel;

    /**
    * 科目字段
    */
    private String subject;

    /**
    * 存款协议代码
    */
    private String depositSubId;

    /**
    * 利息调整
    */
    private BigDecimal interestAdj;

    /**
    * 账面
    */
    private BigDecimal faceAmt;

    public String getDepositCode() {
        return depositCode;
    }

    public void setDepositCode(String depositCode) {
        this.depositCode = depositCode;
    }

    public String getDepositName() {
        return depositName;
    }

    public void setDepositName(String depositName) {
        this.depositName = depositName;
    }

    public String getLendInst() {
        return lendInst;
    }

    public void setLendInst(String lendInst) {
        this.lendInst = lendInst;
    }

    public String getFinType() {
        return finType;
    }

    public void setFinType(String finType) {
        this.finType = finType;
    }

    public String getProdtype() {
        return prodtype;
    }

    public void setProdtype(String prodtype) {
        this.prodtype = prodtype;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(String publishDate) {
        this.publishDate = publishDate;
    }

    public String getPayDate() {
        return payDate;
    }

    public void setPayDate(String payDate) {
        this.payDate = payDate;
    }

    public String getDuedate() {
        return duedate;
    }

    public void setDuedate(String duedate) {
        this.duedate = duedate;
    }

    public String getValueDate() {
        return valueDate;
    }

    public void setValueDate(String valueDate) {
        this.valueDate = valueDate;
    }

    public String getCirculation() {
        return circulation;
    }

    public void setCirculation(String circulation) {
        this.circulation = circulation;
    }

    public String getIssMethod() {
        return issMethod;
    }

    public void setIssMethod(String issMethod) {
        this.issMethod = issMethod;
    }

    public String getInviMethod() {
        return inviMethod;
    }

    public void setInviMethod(String inviMethod) {
        this.inviMethod = inviMethod;
    }

    public String getDepositTerm() {
        return depositTerm;
    }

    public void setDepositTerm(String depositTerm) {
        this.depositTerm = depositTerm;
    }

    public String getInterestType() {
        return interestType;
    }

    public void setInterestType(String interestType) {
        this.interestType = interestType;
    }

    public BigDecimal getActualAmount() {
        return actualAmount;
    }

    public void setActualAmount(BigDecimal actualAmount) {
        this.actualAmount = actualAmount;
    }

    public BigDecimal getTotalTenderSuccessAmt() {
        return totalTenderSuccessAmt;
    }

    public void setTotalTenderSuccessAmt(BigDecimal totalTenderSuccessAmt) {
        this.totalTenderSuccessAmt = totalTenderSuccessAmt;
    }

    public String getTenderMultiples() {
        return tenderMultiples;
    }

    public void setTenderMultiples(String tenderMultiples) {
        this.tenderMultiples = tenderMultiples;
    }

    public BigDecimal getPublishPrice() {
        return publishPrice;
    }

    public void setPublishPrice(BigDecimal publishPrice) {
        this.publishPrice = publishPrice;
    }

    public BigDecimal getCompareRate() {
        return compareRate;
    }

    public void setCompareRate(BigDecimal compareRate) {
        this.compareRate = compareRate;
    }

    public BigDecimal getFaceRate() {
        return faceRate;
    }

    public void setFaceRate(BigDecimal faceRate) {
        this.faceRate = faceRate;
    }

    public BigDecimal getBaseRate() {
        return baseRate;
    }

    public void setBaseRate(BigDecimal baseRate) {
        this.baseRate = baseRate;
    }

    public BigDecimal getSpread() {
        return spread;
    }

    public void setSpread(BigDecimal spread) {
        this.spread = spread;
    }

    public String getLendTrad() {
        return lendTrad;
    }

    public void setLendTrad(String lendTrad) {
        this.lendTrad = lendTrad;
    }

    public String getLendTel() {
        return lendTel;
    }

    public void setLendTel(String lendTel) {
        this.lendTel = lendTel;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDepositSubId() {
        return depositSubId;
    }

    public void setDepositSubId(String depositSubId) {
        this.depositSubId = depositSubId;
    }

    public BigDecimal getInterestAdj() {
        return interestAdj;
    }

    public void setInterestAdj(BigDecimal interestAdj) {
        this.interestAdj = interestAdj;
    }

    public BigDecimal getFaceAmt() {
        return faceAmt;
    }

    public void setFaceAmt(BigDecimal faceAmt) {
        this.faceAmt = faceAmt;
    }

    @Override
    public String toString() {
        return "IfsReportDepositInvestorReport{" +
                "depositCode='" + depositCode + '\'' +
                ", depositName='" + depositName + '\'' +
                ", lendInst='" + lendInst + '\'' +
                ", finType='" + finType + '\'' +
                ", prodtype='" + prodtype + '\'' +
                ", currency='" + currency + '\'' +
                ", publishDate='" + publishDate + '\'' +
                ", payDate='" + payDate + '\'' +
                ", duedate='" + duedate + '\'' +
                ", valueDate='" + valueDate + '\'' +
                ", circulation='" + circulation + '\'' +
                ", issMethod='" + issMethod + '\'' +
                ", inviMethod='" + inviMethod + '\'' +
                ", depositTerm='" + depositTerm + '\'' +
                ", interestType='" + interestType + '\'' +
                ", actualAmount=" + actualAmount +
                ", totalTenderSuccessAmt=" + totalTenderSuccessAmt +
                ", tenderMultiples='" + tenderMultiples + '\'' +
                ", publishPrice=" + publishPrice +
                ", compareRate=" + compareRate +
                ", faceRate=" + faceRate +
                ", baseRate=" + baseRate +
                ", spread=" + spread +
                ", lendTrad='" + lendTrad + '\'' +
                ", lendTel='" + lendTel + '\'' +
                ", subject='" + subject + '\'' +
                ", depositSubId='" + depositSubId + '\'' +
                ", interestAdj=" + interestAdj +
                ", faceAmt=" + faceAmt +
                '}';
    }
}