package com.singlee.hrbnewport.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

/**
 * G24最大百家金融机构同业融入情况表
 */
@Table(name = "IFS_REPORT_G2400")
public class IfsReportG2400 implements Serializable {
    /**
     * 机构类型
     */
    @Column(name = "JGLX")
    private String jglx;

    /**
     * 金融机构名称
     */
    @Column(name = "JRJGMC")
    private String jrjgmc;

    /**
     * 金融机构代码
     */
    @Column(name = "JRJGDM")
    private String jrjgdm;

    /**
     * 同业拆放-其中：同业拆借
     */
    @Column(name = "CFTY_CJ")
    private BigDecimal cftyCj;

    /**
     * 同业拆放-其中：同业借款
     */
    @Column(name = "CFTY_JK")
    private BigDecimal cftyJk;

    /**
     * 同业存放
     */
    @Column(name = "TYCF")
    private BigDecimal tycf;

    /**
     * 卖出回购
     */
    @Column(name = "MCHG")
    private BigDecimal mchg;

    /**
     * 委托方同业代付
     */
    @Column(name = "WTFTYDF")
    private BigDecimal wtftydf;

    /**
     * 同业融入合计（D+G+H+I）
     */
    @Column(name = "TYRRHJ")
    private BigDecimal tyrrhj;

    /**
     * 扣除结算性同业存款后的融入余额（D+G+H+I-M）
     */
    @Column(name = "KCJSXYE")
    private BigDecimal kcjsxye;

    /**
     * 占负债合计的比重(%)
     */
    @Column(name = "FZHJBZ")
    private BigDecimal fzhjbz;

    /**
     * 扣除项：结算性同业存款
     */
    @Column(name = "KCX_JSXTYCK")
    private BigDecimal kcxJsxtyck;

    /**
     * 附注：发行的同业存单
     */
    @Column(name = "BZ_FXTYCD")
    private BigDecimal bzFxtycd;

    private static final long serialVersionUID = 1L;

    /**
     * 获取机构类型
     *
     * @return JGLX - 机构类型
     */
    public String getJglx() {
        return jglx;
    }

    /**
     * 设置机构类型
     *
     * @param jglx 机构类型
     */
    public void setJglx(String jglx) {
        this.jglx = jglx;
    }

    /**
     * 获取金融机构名称
     *
     * @return JRJGMC - 金融机构名称
     */
    public String getJrjgmc() {
        return jrjgmc;
    }

    /**
     * 设置金融机构名称
     *
     * @param jrjgmc 金融机构名称
     */
    public void setJrjgmc(String jrjgmc) {
        this.jrjgmc = jrjgmc;
    }

    /**
     * 获取金融机构代码
     *
     * @return JRJGDM - 金融机构代码
     */
    public String getJrjgdm() {
        return jrjgdm;
    }

    /**
     * 设置金融机构代码
     *
     * @param jrjgdm 金融机构代码
     */
    public void setJrjgdm(String jrjgdm) {
        this.jrjgdm = jrjgdm;
    }

    /**
     * 获取同业拆放-其中：同业拆借
     *
     * @return CFTY_CJ - 同业拆放-其中：同业拆借
     */
    public BigDecimal getCftyCj() {
        return cftyCj;
    }

    /**
     * 设置同业拆放-其中：同业拆借
     *
     * @param cftyCj 同业拆放-其中：同业拆借
     */
    public void setCftyCj(BigDecimal cftyCj) {
        this.cftyCj = cftyCj;
    }

    /**
     * 获取同业拆放-其中：同业借款
     *
     * @return CFTY_JK - 同业拆放-其中：同业借款
     */
    public BigDecimal getCftyJk() {
        return cftyJk;
    }

    /**
     * 设置同业拆放-其中：同业借款
     *
     * @param cftyJk 同业拆放-其中：同业借款
     */
    public void setCftyJk(BigDecimal cftyJk) {
        this.cftyJk = cftyJk;
    }

    /**
     * 获取同业存放
     *
     * @return TYCF - 同业存放
     */
    public BigDecimal getTycf() {
        return tycf;
    }

    /**
     * 设置同业存放
     *
     * @param tycf 同业存放
     */
    public void setTycf(BigDecimal tycf) {
        this.tycf = tycf;
    }

    /**
     * 获取卖出回购
     *
     * @return MCHG - 卖出回购
     */
    public BigDecimal getMchg() {
        return mchg;
    }

    /**
     * 设置卖出回购
     *
     * @param mchg 卖出回购
     */
    public void setMchg(BigDecimal mchg) {
        this.mchg = mchg;
    }

    /**
     * 获取委托方同业代付
     *
     * @return WTFTYDF - 委托方同业代付
     */
    public BigDecimal getWtftydf() {
        return wtftydf;
    }

    /**
     * 设置委托方同业代付
     *
     * @param wtftydf 委托方同业代付
     */
    public void setWtftydf(BigDecimal wtftydf) {
        this.wtftydf = wtftydf;
    }

    /**
     * 获取同业融入合计（D+G+H+I）
     *
     * @return TYRRHJ - 同业融入合计（D+G+H+I）
     */
    public BigDecimal getTyrrhj() {
        return tyrrhj;
    }

    /**
     * 设置同业融入合计（D+G+H+I）
     *
     * @param tyrrhj 同业融入合计（D+G+H+I）
     */
    public void setTyrrhj(BigDecimal tyrrhj) {
        this.tyrrhj = tyrrhj;
    }

    /**
     * 获取扣除结算性同业存款后的融入余额（D+G+H+I-M）
     *
     * @return KCJSXYE - 扣除结算性同业存款后的融入余额（D+G+H+I-M）
     */
    public BigDecimal getKcjsxye() {
        return kcjsxye;
    }

    /**
     * 设置扣除结算性同业存款后的融入余额（D+G+H+I-M）
     *
     * @param kcjsxye 扣除结算性同业存款后的融入余额（D+G+H+I-M）
     */
    public void setKcjsxye(BigDecimal kcjsxye) {
        this.kcjsxye = kcjsxye;
    }

    /**
     * 获取占负债合计的比重(%)
     *
     * @return FZHJBZ - 占负债合计的比重(%)
     */
    public BigDecimal getFzhjbz() {
        return fzhjbz;
    }

    /**
     * 设置占负债合计的比重(%)
     *
     * @param fzhjbz 占负债合计的比重(%)
     */
    public void setFzhjbz(BigDecimal fzhjbz) {
        this.fzhjbz = fzhjbz;
    }

    /**
     * 获取扣除项：结算性同业存款
     *
     * @return KCX_JSXTYCK - 扣除项：结算性同业存款
     */
    public BigDecimal getKcxJsxtyck() {
        return kcxJsxtyck;
    }

    /**
     * 设置扣除项：结算性同业存款
     *
     * @param kcxJsxtyck 扣除项：结算性同业存款
     */
    public void setKcxJsxtyck(BigDecimal kcxJsxtyck) {
        this.kcxJsxtyck = kcxJsxtyck;
    }

    /**
     * 获取附注：发行的同业存单
     *
     * @return BZ_FXTYCD - 附注：发行的同业存单
     */
    public BigDecimal getBzFxtycd() {
        return bzFxtycd;
    }

    /**
     * 设置附注：发行的同业存单
     *
     * @param bzFxtycd 附注：发行的同业存单
     */
    public void setBzFxtycd(BigDecimal bzFxtycd) {
        this.bzFxtycd = bzFxtycd;
    }
}