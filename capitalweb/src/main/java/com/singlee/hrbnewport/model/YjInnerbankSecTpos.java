package com.singlee.hrbnewport.model;

import java.math.BigDecimal;

/**
 *
 * 2021/12/8
 * @Auther:zk
 */    
    
/**
    * 同业业务管理报告-债券投资
    */
public class YjInnerbankSecTpos {
    /**
    * 序号
    */
    private String seq;

    /**
    * 债券代码
    */
    private String secid;

    /**
    * 债券名称
    */
    private String secName;

    /**
    * 起息日
    */
    private String vdate;

    /**
    * 期限
    */
    private String tenor;

    /**
    * 到期日
    */
    private String mdate;

    /**
    * 债券面值
    */
    private BigDecimal faceAmt;

    /**
    * 票面利率
    */
    private BigDecimal faceRate;

    /**
    * 是否质押
    */
    private BigDecimal isPledge;

    /**
    * 债券类型
    */
    private String secType;

    public String getSeq() {
        return seq;
    }

    public void setSeq(String seq) {
        this.seq = seq;
    }

    public String getSecid() {
        return secid;
    }

    public void setSecid(String secid) {
        this.secid = secid;
    }

    public String getSecName() {
        return secName;
    }

    public void setSecName(String secName) {
        this.secName = secName;
    }

    public String getVdate() {
        return vdate;
    }

    public void setVdate(String vdate) {
        this.vdate = vdate;
    }

    public String getTenor() {
        return tenor;
    }

    public void setTenor(String tenor) {
        this.tenor = tenor;
    }

    public String getMdate() {
        return mdate;
    }

    public void setMdate(String mdate) {
        this.mdate = mdate;
    }

    public BigDecimal getFaceAmt() {
        return faceAmt;
    }

    public void setFaceAmt(BigDecimal faceAmt) {
        this.faceAmt = faceAmt;
    }

    public BigDecimal getFaceRate() {
        return faceRate;
    }

    public void setFaceRate(BigDecimal faceRate) {
        this.faceRate = faceRate;
    }

    public BigDecimal getIsPledge() {
        return isPledge;
    }

    public void setIsPledge(BigDecimal isPledge) {
        this.isPledge = isPledge;
    }

    public String getSecType() {
        return secType;
    }

    public void setSecType(String secType) {
        this.secType = secType;
    }
}