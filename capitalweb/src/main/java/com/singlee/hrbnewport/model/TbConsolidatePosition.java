package com.singlee.hrbnewport.model;

import java.math.BigDecimal;

/**
    * 合并持仓持仓明细变化-债券
    */
public class TbConsolidatePosition {
    /**
    * 投资类型
    */
    private String investorType;

    /**
    * 账户类型
    */
    private String bustype;

    /**
    * 债券类型
    */
    private String acctdesc;

    /**
    * 债券发行人
    */
    private String issuer;

    /**
    * 债券代码
    */
    private String secid;

    /**
    * 债券名称
    */
    private String descr;

    /**
    * 投资组合
    */
    private String port;

    /**
    * 起息日
    */
    private String vdate;

    /**
    * 期限
    */
    private String teno;

    /**
    * 到期日
    */
    private String mdate;

    /**
    * 债券面值
    */
    private BigDecimal faceamt;

    /**
    * 票面利率
    */
    private BigDecimal couprate;

    /**
    * 账面价值（元）
    */
    private BigDecimal settamt;

    private String ps;

    public String getInvestorType() {
        return investorType;
    }

    public void setInvestorType(String investorType) {
        this.investorType = investorType == null ? null : investorType.trim();
    }

    public String getBustype() {
        return bustype;
    }

    public void setBustype(String bustype) {
        this.bustype = bustype == null ? null : bustype.trim();
    }


    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr == null ? null : descr.trim();
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port == null ? null : port.trim();
    }

    public String getVdate() {
        return vdate;
    }

    public void setVdate(String vdate) {
        this.vdate = vdate == null ? null : vdate.trim();
    }

    public String getTeno() {
        return teno;
    }

    public void setTeno(String teno) {
        this.teno = teno == null ? null : teno.trim();
    }

    public String getMdate() {
        return mdate;
    }

    public void setMdate(String mdate) {
        this.mdate = mdate == null ? null : mdate.trim();
    }

    public BigDecimal getFaceamt() {
        return faceamt;
    }

    public void setFaceamt(BigDecimal faceamt) {
        this.faceamt = faceamt;
    }

    public BigDecimal getCouprate() {
        return couprate;
    }

    public void setCouprate(BigDecimal couprate) {
        this.couprate = couprate;
    }

    public BigDecimal getSettamt() {
        return settamt;
    }

    public void setSettamt(BigDecimal settamt) {
        this.settamt = settamt;
    }

    public String getAcctdesc() {
        return acctdesc;
    }

    public void setAcctdesc(String acctdesc) {
        this.acctdesc = acctdesc;
    }

    public String getIssuer() {
        return issuer;
    }

    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }

    public String getSecid() {
        return secid;
    }

    public void setSecid(String secid) {
        this.secid = secid;
    }

    public String getPs() {
        return ps;
    }

    public void setPs(String ps) {
        this.ps = ps;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", investorType=").append(investorType);
        sb.append(", bustype=").append(bustype);
        sb.append(", acctdesc=").append(acctdesc);
        sb.append(", issuer=").append(issuer);
        sb.append(", secid=").append(secid);
        sb.append(", descr=").append(descr);
        sb.append(", port=").append(port);
        sb.append(", vdate=").append(vdate);
        sb.append(", teno=").append(teno);
        sb.append(", mdate=").append(mdate);
        sb.append(", faceamt=").append(faceamt);
        sb.append(", couprate=").append(couprate);
        sb.append(", settamt=").append(settamt);
        sb.append(", ps=").append(ps);
        sb.append("]");
        return sb.toString();
    }
}