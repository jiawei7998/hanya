package com.singlee.hrbnewport.model;

/**
 *
 * 2021/12/6
 * @Auther:zk
 */    
    
/**
    * 业务明细-向境内交易及结算类金融机构买入返售债券
    */
public class DealdetailRvreposecSettle {
    /**
    * 交易对手
    */
    private String cust;

    /**
    * 收起交割日
    */
    private String vdate;

    /**
    * 期限
    */
    private String tenor;

    /**
    * 到期交割日
    */
    private String mdate;

    /**
    * 利率（%）
    */
    private String rate;

    /**
    * 首期结算额
    */
    private String amt;

    public String getCust() {
        return cust;
    }

    public void setCust(String cust) {
        this.cust = cust;
    }

    public String getVdate() {
        return vdate;
    }

    public void setVdate(String vdate) {
        this.vdate = vdate;
    }

    public String getTenor() {
        return tenor;
    }

    public void setTenor(String tenor) {
        this.tenor = tenor;
    }

    public String getMdate() {
        return mdate;
    }

    public void setMdate(String mdate) {
        this.mdate = mdate;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getAmt() {
        return amt;
    }

    public void setAmt(String amt) {
        this.amt = amt;
    }
}