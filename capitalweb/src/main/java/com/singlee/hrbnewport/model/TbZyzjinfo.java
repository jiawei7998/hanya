package com.singlee.hrbnewport.model;

  /**
    * 自营资金交易信息表
    */
public class TbZyzjinfo {
    /**
    * 采集日期
    */
    private String cjrq;

    /**
    * 阶段开始日
    */
    private String jdksr;

    /**
    * 阶段结束日
    */
    private String jdjsr;

    /**
    * 银行机构代码
    */
    private String yxjgdm;

    /**
    * 金融许可证号
    */
    private String jrxkzh;

    /**
    * 内部机构号
    */
    private String nbjgh;

    /**
    * 银行机构名称
    */
    private String yxjgmc;

    /**
    * 行内归属部门
    */
    private String hnssbm;

    /**
    * 账户类型
    */
    private String zhlx;

    /**
    * 交易编号
    */
    private String jybh;

    /**
    * 业务中类
    */
    private String ywzl;

    /**
    * 业务小类
    */
    private String ywxl;

    /**
    * 银行产品
    */
    private String yhcp;

    /**
    * 币种
    */
    private String bz;

    /**
    * 交易对手编号
    */
    private String jydsbh;

    /**
    * 交易对手金融机构许可证号
    */
    private String jydsjrjgxkzh;

    /**
    * 组织机构代码
    */
    private String zzjgdm;

    /**
    * 纳税识别码
    */
    private String nssbm;

    /**
    * 交易对手名称
    */
    private String jydsmc;

    /**
    * 交易对手国家
    */
    private String jydsgj;

    /**
    * 交易对手行业
    */
    private String jydshy;

    /**
    * 交易对手评级
    */
    private String jydspj;

    /**
    * 交易对手评级机构
    */
    private String jydspjjg;

    /**
    * 基础资产编号
    */
    private String jczcbh;

    /**
    * 基础资产名称
    */
    private String jczcmc;

    /**
    * 基础资产评级
    */
    private String jczcpj;

    /**
    * 基础资产评级机构
    */
    private String jczcpjjg;

    /**
    * 基础资产客户编号
    */
    private String jczckhbh;

    /**
    * 基础资产客户名称
    */
    private String jczckhmc;

    /**
    * 基础资产客户国家
    */
    private String jczckhgj;

    /**
    * 基础资产客户评级
    */
    private String jczckhpj;

    /**
    * 基础客户评级机构
    */
    private String jckhpjjg;

    /**
    * 基础资产或客户行业
    */
    private String jczchkhhy;

    /**
    * 最终投向类型
    */
    private String zztxlx;

    /**
    * 最终投向行业
    */
    private String zztxhy;

    /**
    * 成交日期
    */
    private String chjrq;

    /**
    * 成交时间
    */
    private String cjsj;

    /**
    * 交割日期
    */
    private String jgrq;

    /**
    * 起息日
    */
    private String qxr;

    /**
    * 到期日
    */
    private String dqr;

    /**
    * 交易方向
    */
    private String jyfx;

    /**
    * 成交面值
    */
    private String cjmz;

    /**
    * 合同金额
    */
    private String cjje;

    /**
    * 年化利率
    */
    private String nhll;

    /**
    * 审批人
    */
    private String spr;

    /**
    * 交易员
    */
    private String jyy;

    /**
    * 本方清算账号
    */
    private String bfqszh;

    /**
    * 本方清算行号
    */
    private String bfqshh;

    /**
    * 对方清算账号
    */
    private String dfqszh;

    /**
    * 对方清算行号
    */
    private String dfqshh;

    public String getCjrq() {
        return cjrq;
    }

    public void setCjrq(String cjrq) {
        this.cjrq = cjrq == null ? null : cjrq.trim();
    }

    public String getJdksr() {
        return jdksr;
    }

    public void setJdksr(String jdksr) {
        this.jdksr = jdksr == null ? null : jdksr.trim();
    }

    public String getJdjsr() {
        return jdjsr;
    }

    public void setJdjsr(String jdjsr) {
        this.jdjsr = jdjsr == null ? null : jdjsr.trim();
    }

    public String getYxjgdm() {
        return yxjgdm;
    }

    public void setYxjgdm(String yxjgdm) {
        this.yxjgdm = yxjgdm == null ? null : yxjgdm.trim();
    }

    public String getJrxkzh() {
        return jrxkzh;
    }

    public void setJrxkzh(String jrxkzh) {
        this.jrxkzh = jrxkzh == null ? null : jrxkzh.trim();
    }

    public String getNbjgh() {
        return nbjgh;
    }

    public void setNbjgh(String nbjgh) {
        this.nbjgh = nbjgh == null ? null : nbjgh.trim();
    }

    public String getYxjgmc() {
        return yxjgmc;
    }

    public void setYxjgmc(String yxjgmc) {
        this.yxjgmc = yxjgmc == null ? null : yxjgmc.trim();
    }

    public String getHnssbm() {
        return hnssbm;
    }

    public void setHnssbm(String hnssbm) {
        this.hnssbm = hnssbm == null ? null : hnssbm.trim();
    }

    public String getZhlx() {
        return zhlx;
    }

    public void setZhlx(String zhlx) {
        this.zhlx = zhlx == null ? null : zhlx.trim();
    }

    public String getJybh() {
        return jybh;
    }

    public void setJybh(String jybh) {
        this.jybh = jybh == null ? null : jybh.trim();
    }

    public String getYwzl() {
        return ywzl;
    }

    public void setYwzl(String ywzl) {
        this.ywzl = ywzl == null ? null : ywzl.trim();
    }

    public String getYwxl() {
        return ywxl;
    }

    public void setYwxl(String ywxl) {
        this.ywxl = ywxl == null ? null : ywxl.trim();
    }

    public String getYhcp() {
        return yhcp;
    }

    public void setYhcp(String yhcp) {
        this.yhcp = yhcp == null ? null : yhcp.trim();
    }

    public String getBz() {
        return bz;
    }

    public void setBz(String bz) {
        this.bz = bz == null ? null : bz.trim();
    }

    public String getJydsbh() {
        return jydsbh;
    }

    public void setJydsbh(String jydsbh) {
        this.jydsbh = jydsbh == null ? null : jydsbh.trim();
    }

    public String getJydsjrjgxkzh() {
        return jydsjrjgxkzh;
    }

    public void setJydsjrjgxkzh(String jydsjrjgxkzh) {
        this.jydsjrjgxkzh = jydsjrjgxkzh == null ? null : jydsjrjgxkzh.trim();
    }

    public String getZzjgdm() {
        return zzjgdm;
    }

    public void setZzjgdm(String zzjgdm) {
        this.zzjgdm = zzjgdm == null ? null : zzjgdm.trim();
    }

    public String getNssbm() {
        return nssbm;
    }

    public void setNssbm(String nssbm) {
        this.nssbm = nssbm == null ? null : nssbm.trim();
    }

    public String getJydsmc() {
        return jydsmc;
    }

    public void setJydsmc(String jydsmc) {
        this.jydsmc = jydsmc == null ? null : jydsmc.trim();
    }

    public String getJydsgj() {
        return jydsgj;
    }

    public void setJydsgj(String jydsgj) {
        this.jydsgj = jydsgj == null ? null : jydsgj.trim();
    }

    public String getJydshy() {
        return jydshy;
    }

    public void setJydshy(String jydshy) {
        this.jydshy = jydshy == null ? null : jydshy.trim();
    }

    public String getJydspj() {
        return jydspj;
    }

    public void setJydspj(String jydspj) {
        this.jydspj = jydspj == null ? null : jydspj.trim();
    }

    public String getJydspjjg() {
        return jydspjjg;
    }

    public void setJydspjjg(String jydspjjg) {
        this.jydspjjg = jydspjjg == null ? null : jydspjjg.trim();
    }

    public String getJczcbh() {
        return jczcbh;
    }

    public void setJczcbh(String jczcbh) {
        this.jczcbh = jczcbh == null ? null : jczcbh.trim();
    }

    public String getJczcmc() {
        return jczcmc;
    }

    public void setJczcmc(String jczcmc) {
        this.jczcmc = jczcmc == null ? null : jczcmc.trim();
    }

    public String getJczcpj() {
        return jczcpj;
    }

    public void setJczcpj(String jczcpj) {
        this.jczcpj = jczcpj == null ? null : jczcpj.trim();
    }

    public String getJczcpjjg() {
        return jczcpjjg;
    }

    public void setJczcpjjg(String jczcpjjg) {
        this.jczcpjjg = jczcpjjg == null ? null : jczcpjjg.trim();
    }

    public String getJczckhbh() {
        return jczckhbh;
    }

    public void setJczckhbh(String jczckhbh) {
        this.jczckhbh = jczckhbh == null ? null : jczckhbh.trim();
    }

    public String getJczckhmc() {
        return jczckhmc;
    }

    public void setJczckhmc(String jczckhmc) {
        this.jczckhmc = jczckhmc == null ? null : jczckhmc.trim();
    }

    public String getJczckhgj() {
        return jczckhgj;
    }

    public void setJczckhgj(String jczckhgj) {
        this.jczckhgj = jczckhgj == null ? null : jczckhgj.trim();
    }

    public String getJczckhpj() {
        return jczckhpj;
    }

    public void setJczckhpj(String jczckhpj) {
        this.jczckhpj = jczckhpj == null ? null : jczckhpj.trim();
    }

    public String getJckhpjjg() {
        return jckhpjjg;
    }

    public void setJckhpjjg(String jckhpjjg) {
        this.jckhpjjg = jckhpjjg == null ? null : jckhpjjg.trim();
    }

    public String getJczchkhhy() {
        return jczchkhhy;
    }

    public void setJczchkhhy(String jczchkhhy) {
        this.jczchkhhy = jczchkhhy == null ? null : jczchkhhy.trim();
    }

    public String getZztxlx() {
        return zztxlx;
    }

    public void setZztxlx(String zztxlx) {
        this.zztxlx = zztxlx == null ? null : zztxlx.trim();
    }

    public String getZztxhy() {
        return zztxhy;
    }

    public void setZztxhy(String zztxhy) {
        this.zztxhy = zztxhy == null ? null : zztxhy.trim();
    }

    public String getChjrq() {
        return chjrq;
    }

    public void setChjrq(String chjrq) {
        this.chjrq = chjrq == null ? null : chjrq.trim();
    }

    public String getCjsj() {
        return cjsj;
    }

    public void setCjsj(String cjsj) {
        this.cjsj = cjsj == null ? null : cjsj.trim();
    }

    public String getJgrq() {
        return jgrq;
    }

    public void setJgrq(String jgrq) {
        this.jgrq = jgrq == null ? null : jgrq.trim();
    }

    public String getQxr() {
        return qxr;
    }

    public void setQxr(String qxr) {
        this.qxr = qxr == null ? null : qxr.trim();
    }

    public String getDqr() {
        return dqr;
    }

    public void setDqr(String dqr) {
        this.dqr = dqr == null ? null : dqr.trim();
    }

    public String getJyfx() {
        return jyfx;
    }

    public void setJyfx(String jyfx) {
        this.jyfx = jyfx == null ? null : jyfx.trim();
    }

    public String getCjmz() {
        return cjmz;
    }

    public void setCjmz(String cjmz) {
        this.cjmz = cjmz == null ? null : cjmz.trim();
    }

    public String getCjje() {
        return cjje;
    }

    public void setCjje(String cjje) {
        this.cjje = cjje == null ? null : cjje.trim();
    }

    public String getNhll() {
        return nhll;
    }

    public void setNhll(String nhll) {
        this.nhll = nhll == null ? null : nhll.trim();
    }

    public String getSpr() {
        return spr;
    }

    public void setSpr(String spr) {
        this.spr = spr == null ? null : spr.trim();
    }

    public String getJyy() {
        return jyy;
    }

    public void setJyy(String jyy) {
        this.jyy = jyy == null ? null : jyy.trim();
    }

    public String getBfqszh() {
        return bfqszh;
    }

    public void setBfqszh(String bfqszh) {
        this.bfqszh = bfqszh == null ? null : bfqszh.trim();
    }

    public String getBfqshh() {
        return bfqshh;
    }

    public void setBfqshh(String bfqshh) {
        this.bfqshh = bfqshh == null ? null : bfqshh.trim();
    }

    public String getDfqszh() {
        return dfqszh;
    }

    public void setDfqszh(String dfqszh) {
        this.dfqszh = dfqszh == null ? null : dfqszh.trim();
    }

    public String getDfqshh() {
        return dfqshh;
    }

    public void setDfqshh(String dfqshh) {
        this.dfqshh = dfqshh == null ? null : dfqshh.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", cjrq=").append(cjrq);
        sb.append(", jdksr=").append(jdksr);
        sb.append(", jdjsr=").append(jdjsr);
        sb.append(", yxjgdm=").append(yxjgdm);
        sb.append(", jrxkzh=").append(jrxkzh);
        sb.append(", nbjgh=").append(nbjgh);
        sb.append(", yxjgmc=").append(yxjgmc);
        sb.append(", hnssbm=").append(hnssbm);
        sb.append(", zhlx=").append(zhlx);
        sb.append(", jybh=").append(jybh);
        sb.append(", ywzl=").append(ywzl);
        sb.append(", ywxl=").append(ywxl);
        sb.append(", yhcp=").append(yhcp);
        sb.append(", bz=").append(bz);
        sb.append(", jydsbh=").append(jydsbh);
        sb.append(", jydsjrjgxkzh=").append(jydsjrjgxkzh);
        sb.append(", zzjgdm=").append(zzjgdm);
        sb.append(", nssbm=").append(nssbm);
        sb.append(", jydsmc=").append(jydsmc);
        sb.append(", jydsgj=").append(jydsgj);
        sb.append(", jydshy=").append(jydshy);
        sb.append(", jydspj=").append(jydspj);
        sb.append(", jydspjjg=").append(jydspjjg);
        sb.append(", jczcbh=").append(jczcbh);
        sb.append(", jczcmc=").append(jczcmc);
        sb.append(", jczcpj=").append(jczcpj);
        sb.append(", jczcpjjg=").append(jczcpjjg);
        sb.append(", jczckhbh=").append(jczckhbh);
        sb.append(", jczckhmc=").append(jczckhmc);
        sb.append(", jczckhgj=").append(jczckhgj);
        sb.append(", jczckhpj=").append(jczckhpj);
        sb.append(", jckhpjjg=").append(jckhpjjg);
        sb.append(", jczchkhhy=").append(jczchkhhy);
        sb.append(", zztxlx=").append(zztxlx);
        sb.append(", zztxhy=").append(zztxhy);
        sb.append(", chjrq=").append(chjrq);
        sb.append(", cjsj=").append(cjsj);
        sb.append(", jgrq=").append(jgrq);
        sb.append(", qxr=").append(qxr);
        sb.append(", dqr=").append(dqr);
        sb.append(", jyfx=").append(jyfx);
        sb.append(", cjmz=").append(cjmz);
        sb.append(", cjje=").append(cjje);
        sb.append(", nhll=").append(nhll);
        sb.append(", spr=").append(spr);
        sb.append(", jyy=").append(jyy);
        sb.append(", bfqszh=").append(bfqszh);
        sb.append(", bfqshh=").append(bfqshh);
        sb.append(", dfqszh=").append(dfqszh);
        sb.append(", dfqshh=").append(dfqshh);
        sb.append("]");
        return sb.toString();
    }
}