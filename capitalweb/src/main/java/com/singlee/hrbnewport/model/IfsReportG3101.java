package com.singlee.hrbnewport.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

/**
 * G31投资业务情况表 底层资产投资情况
 */
@Table(name = "IFS_REPORT_G3101")
public class IfsReportG3101 implements Serializable {
    /**
     * 项目
     */
    @Column(name = "PRODUCT")
    private String product;

    /**
     * 穿透前_期末余额
     */
    @Column(name = "CTQ_QMYE")
    private BigDecimal ctqQmye;

    /**
     * 穿透前_投资收入（年初至报告期末数）
     */
    @Column(name = "CTQ_TZSR")
    private BigDecimal ctqTzsr;

    /**
     * 因持有非底层资产而间接持有_期末余额
     */
    @Column(name = "YCYFDCZCJJCY_QMYE")
    private BigDecimal ycyfdczcjjcyQmye;

    /**
     * 穿透后_期末余额
     */
    @Column(name = "CTH_MQYE")
    private BigDecimal cthMqye;

    @Column(name = "PARENT_PRODUCT")
    private String parentProduct;

    @Transient
    private String producto;

    private static final long serialVersionUID = 1L;

    /**
     * 获取项目
     *
     * @return PRODUCT - 项目
     */
    public String getProduct() {
        return product;
    }

    /**
     * 设置项目
     *
     * @param product 项目
     */
    public void setProduct(String product) {
        this.product = product;
    }

    /**
     * 获取穿透前_期末余额
     *
     * @return CTQ_QMYE - 穿透前_期末余额
     */
    public BigDecimal getCtqQmye() {
        return ctqQmye;
    }

    /**
     * 设置穿透前_期末余额
     *
     * @param ctqQmye 穿透前_期末余额
     */
    public void setCtqQmye(BigDecimal ctqQmye) {
        this.ctqQmye = ctqQmye;
    }

    /**
     * 获取穿透前_投资收入（年初至报告期末数）
     *
     * @return CTQ_TZSR - 穿透前_投资收入（年初至报告期末数）
     */
    public BigDecimal getCtqTzsr() {
        return ctqTzsr;
    }

    /**
     * 设置穿透前_投资收入（年初至报告期末数）
     *
     * @param ctqTzsr 穿透前_投资收入（年初至报告期末数）
     */
    public void setCtqTzsr(BigDecimal ctqTzsr) {
        this.ctqTzsr = ctqTzsr;
    }

    /**
     * 获取因持有非底层资产而间接持有_期末余额
     *
     * @return YCYFDCZCJJCY_QMYE - 因持有非底层资产而间接持有_期末余额
     */
    public BigDecimal getYcyfdczcjjcyQmye() {
        return ycyfdczcjjcyQmye;
    }

    /**
     * 设置因持有非底层资产而间接持有_期末余额
     *
     * @param ycyfdczcjjcyQmye 因持有非底层资产而间接持有_期末余额
     */
    public void setYcyfdczcjjcyQmye(BigDecimal ycyfdczcjjcyQmye) {
        this.ycyfdczcjjcyQmye = ycyfdczcjjcyQmye;
    }

    /**
     * 获取穿透后_期末余额
     *
     * @return CTH_MQYE - 穿透后_期末余额
     */
    public BigDecimal getCthMqye() {
        return cthMqye;
    }

    /**
     * 设置穿透后_期末余额
     *
     * @param cthMqye 穿透后_期末余额
     */
    public void setCthMqye(BigDecimal cthMqye) {
        this.cthMqye = cthMqye;
    }

    /**
     * @return PARENT_PRODUCT
     */
    public String getParentProduct() {
        return parentProduct;
    }

    /**
     * @param parentProduct
     */
    public void setParentProduct(String parentProduct) {
        this.parentProduct = parentProduct;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }
}