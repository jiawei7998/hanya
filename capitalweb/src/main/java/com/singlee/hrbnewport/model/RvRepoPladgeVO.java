package com.singlee.hrbnewport.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

/**
 * _买入返售质押
 * @author zk
 */
@Entity
@Table(name="IFS_REPORT_VREPO_PLEDGE")
public class RvRepoPladgeVO {
    private String colum; //名称
    private String pladgeSec; //买入返售债券
    private BigDecimal gov; //中央政府
    private BigDecimal centerBank; //央行
    private BigDecimal policeBank; //政策性银行
    private BigDecimal pubDepartment; //公共部门实体
    private BigDecimal comBank; //商业银行

    public String getColum() {
        return colum;
    }
    public void setColum(String colum) {
        this.colum = colum;
    }
    public String getPladgeSec() {
        return pladgeSec;
    }
    public void setPladgeSec(String pladgeSec) {
        this.pladgeSec = pladgeSec;
    }
    public BigDecimal getGov() {
        return gov;
    }
    public void setGov(BigDecimal gov) {
        this.gov = gov;
    }
    public BigDecimal getCenterBank() {
        return centerBank;
    }
    public void setCenterBank(BigDecimal centerBank) {
        this.centerBank = centerBank;
    }
    public BigDecimal getPoliceBank() {
        return policeBank;
    }
    public void setPoliceBank(BigDecimal policeBank) {
        this.policeBank = policeBank;
    }
    public BigDecimal getPubDepartment() {
        return pubDepartment;
    }
    public void setPubDepartment(BigDecimal pubDepartment) {
        this.pubDepartment = pubDepartment;
    }
    public BigDecimal getComBank() {
        return comBank;
    }
    public void setComBank(BigDecimal comBank) {
        this.comBank = comBank;
    }

    @Override
    public String toString() {
        return "RvRepoPladgeVO [colum=" + colum + ", pladgeSec=" + pladgeSec + ", gov=" + gov + ", centerBank="
                + centerBank + ", policeBank=" + policeBank + ", pubDepartment=" + pubDepartment + ", comBank="
                + comBank + "]";
    }
}
