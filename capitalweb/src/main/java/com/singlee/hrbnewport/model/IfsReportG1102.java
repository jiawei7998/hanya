package com.singlee.hrbnewport.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

/**
 * G1102资产质量及准备金表
 */
@Table(name = "IFS_REPORT_G1102")
public class IfsReportG1102 implements Serializable {
    /**
     * 项目
     */
    @Column(name = "PRODUCT")
    private String product;

    /**
     * 各项资产
     */
    @Column(name = "GXZC")
    private BigDecimal gxzc;

    /**
     * 正常资产
     */
    @Column(name = "ZCZC")
    private BigDecimal zczc;

    /**
     * 正常类
     */
    @Column(name = "ZCL")
    private BigDecimal zcl;

    /**
     * 关注类
     */
    @Column(name = "GZL")
    private BigDecimal gzl;

    /**
     * 不良资产
     */
    @Column(name = "BLZC")
    private BigDecimal blzc;

    /**
     * 次级类
     */
    @Column(name = "CJL")
    private BigDecimal cjl;

    /**
     * 可疑类
     */
    @Column(name = "KYL")
    private BigDecimal kyl;

    /**
     * 损失类
     */
    @Column(name = "SSL")
    private BigDecimal ssl;

    /**
     * 逾期资产
     */
    @Column(name = "YQZC")
    private BigDecimal yqzc;

    @Column(name = "PARENT_PRODUCT")
    private String parentProduct;

    private static final long serialVersionUID = 1L;

    /**
     * 获取项目
     *
     * @return PRODUCT - 项目
     */
    public String getProduct() {
        return product;
    }

    /**
     * 设置项目
     *
     * @param product 项目
     */
    public void setProduct(String product) {
        this.product = product;
    }

    /**
     * 获取各项资产
     *
     * @return GXZC - 各项资产
     */
    public BigDecimal getGxzc() {
        return gxzc;
    }

    /**
     * 设置各项资产
     *
     * @param gxzc 各项资产
     */
    public void setGxzc(BigDecimal gxzc) {
        this.gxzc = gxzc;
    }

    /**
     * 获取正常资产
     *
     * @return ZCZC - 正常资产
     */
    public BigDecimal getZczc() {
        return zczc;
    }

    /**
     * 设置正常资产
     *
     * @param zczc 正常资产
     */
    public void setZczc(BigDecimal zczc) {
        this.zczc = zczc;
    }

    /**
     * 获取正常类
     *
     * @return ZCL - 正常类
     */
    public BigDecimal getZcl() {
        return zcl;
    }

    /**
     * 设置正常类
     *
     * @param zcl 正常类
     */
    public void setZcl(BigDecimal zcl) {
        this.zcl = zcl;
    }

    /**
     * 获取关注类
     *
     * @return GZL - 关注类
     */
    public BigDecimal getGzl() {
        return gzl;
    }

    /**
     * 设置关注类
     *
     * @param gzl 关注类
     */
    public void setGzl(BigDecimal gzl) {
        this.gzl = gzl;
    }

    /**
     * 获取不良资产
     *
     * @return BLZC - 不良资产
     */
    public BigDecimal getBlzc() {
        return blzc;
    }

    /**
     * 设置不良资产
     *
     * @param blzc 不良资产
     */
    public void setBlzc(BigDecimal blzc) {
        this.blzc = blzc;
    }

    /**
     * 获取次级类
     *
     * @return CJL - 次级类
     */
    public BigDecimal getCjl() {
        return cjl;
    }

    /**
     * 设置次级类
     *
     * @param cjl 次级类
     */
    public void setCjl(BigDecimal cjl) {
        this.cjl = cjl;
    }

    /**
     * 获取可疑类
     *
     * @return KYL - 可疑类
     */
    public BigDecimal getKyl() {
        return kyl;
    }

    /**
     * 设置可疑类
     *
     * @param kyl 可疑类
     */
    public void setKyl(BigDecimal kyl) {
        this.kyl = kyl;
    }

    /**
     * 获取损失类
     *
     * @return SSL - 损失类
     */
    public BigDecimal getSsl() {
        return ssl;
    }

    /**
     * 设置损失类
     *
     * @param ssl 损失类
     */
    public void setSsl(BigDecimal ssl) {
        this.ssl = ssl;
    }

    /**
     * 获取逾期资产
     *
     * @return YQZC - 逾期资产
     */
    public BigDecimal getYqzc() {
        return yqzc;
    }

    /**
     * 设置逾期资产
     *
     * @param yqzc 逾期资产
     */
    public void setYqzc(BigDecimal yqzc) {
        this.yqzc = yqzc;
    }

    /**
     * @return PARENT_PRODUCT
     */
    public String getParentProduct() {
        return parentProduct;
    }

    /**
     * @param parentProduct
     */
    public void setParentProduct(String parentProduct) {
        this.parentProduct = parentProduct;
    }
}