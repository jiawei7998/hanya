package com.singlee.hrbnewport.model;

import javax.persistence.Id;
import java.io.Serializable;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 20:51
 * @description：${description}
 * @modified By：
 * @version:   
 */
/**
    * 2105同业往来账户余额信息
    */
public class IfsReportTywlzhye implements Serializable {
    /**
    * 申报号码
    */
    private String sbhm;

    /**
    * 操作类型
    */
    private String czlx;

    /**
    * 变更/撤销原因
    */
    private String bgyy;

    /**
    * 银行机构代码
    */
    private String yhjgdm;

    /**
    * 余额对应日期
    */
    private String yedyrq;

    /**
    * 是否联行及附属机构往来
    */
    private String zjwl;

    /**
    * 明细操作类型
    */
    private String mxczlx;

    /**
    * 主账户账号
    */
    private String zhhzh;

    /**
    * 币种
    */
    private String bz;

    /**
    * 主账户日终余额
    */
    private String zhhrzye;

    /**
    * 其中一年以上余额
    */
    private String qzynysye;

    /**
    * 明细操作类型
    */
    private String zhhmxczlx;

    /**
    * 子账户账号
    */
    private String zzhzh;

    /**
    * 币种
    */
    private String zzhbz;

    /**
    * 子账户日终余额
    */
    private String zzhye;

    /**
    * 其中一年以上余额
    */
    private String ynys;

    /**
     * 账务日期
     */
    @Id
    private String postdate;

    private static final long serialVersionUID = 1L;

    public String getSbhm() {
        return sbhm;
    }

    public void setSbhm(String sbhm) {
        this.sbhm = sbhm;
    }

    public String getCzlx() {
        return czlx;
    }

    public void setCzlx(String czlx) {
        this.czlx = czlx;
    }

    public String getBgyy() {
        return bgyy;
    }

    public void setBgyy(String bgyy) {
        this.bgyy = bgyy;
    }

    public String getYhjgdm() {
        return yhjgdm;
    }

    public void setYhjgdm(String yhjgdm) {
        this.yhjgdm = yhjgdm;
    }

    public String getYedyrq() {
        return yedyrq;
    }

    public void setYedyrq(String yedyrq) {
        this.yedyrq = yedyrq;
    }

    public String getZjwl() {
        return zjwl;
    }

    public void setZjwl(String zjwl) {
        this.zjwl = zjwl;
    }

    public String getMxczlx() {
        return mxczlx;
    }

    public void setMxczlx(String mxczlx) {
        this.mxczlx = mxczlx;
    }

    public String getZhhzh() {
        return zhhzh;
    }

    public void setZhhzh(String zhhzh) {
        this.zhhzh = zhhzh;
    }

    public String getBz() {
        return bz;
    }

    public void setBz(String bz) {
        this.bz = bz;
    }

    public String getZhhrzye() {
        return zhhrzye;
    }

    public void setZhhrzye(String zhhrzye) {
        this.zhhrzye = zhhrzye;
    }

    public String getQzynysye() {
        return qzynysye;
    }

    public void setQzynysye(String qzynysye) {
        this.qzynysye = qzynysye;
    }

    public String getZhhmxczlx() {
        return zhhmxczlx;
    }

    public void setZhhmxczlx(String zhhmxczlx) {
        this.zhhmxczlx = zhhmxczlx;
    }

    public String getZzhzh() {
        return zzhzh;
    }

    public void setZzhzh(String zzhzh) {
        this.zzhzh = zzhzh;
    }

    public String getZzhbz() {
        return zzhbz;
    }

    public void setZzhbz(String zzhbz) {
        this.zzhbz = zzhbz;
    }

    public String getZzhye() {
        return zzhye;
    }

    public void setZzhye(String zzhye) {
        this.zzhye = zzhye;
    }

    public String getYnys() {
        return ynys;
    }

    public void setYnys(String ynys) {
        this.ynys = ynys;
    }

    public String getPostdate() {
        return postdate;
    }

    public void setPostdate(String postdate) {
        this.postdate = postdate;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", sbhm=").append(sbhm);
        sb.append(", czlx=").append(czlx);
        sb.append(", bgyy=").append(bgyy);
        sb.append(", yhjgdm=").append(yhjgdm);
        sb.append(", yedyrq=").append(yedyrq);
        sb.append(", zjwl=").append(zjwl);
        sb.append(", mxczlx=").append(mxczlx);
        sb.append(", zhhzh=").append(zhhzh);
        sb.append(", bz=").append(bz);
        sb.append(", zhhrzye=").append(zhhrzye);
        sb.append(", qzynysye=").append(qzynysye);
        sb.append(", zhhmxczlx=").append(zhhmxczlx);
        sb.append(", zzhzh=").append(zzhzh);
        sb.append(", zzhbz=").append(zzhbz);
        sb.append(", zzhye=").append(zzhye);
        sb.append(", ynys=").append(ynys);
        sb.append("]");
        return sb.toString();
    }
}