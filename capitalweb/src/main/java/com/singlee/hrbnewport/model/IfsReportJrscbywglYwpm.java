package com.singlee.hrbnewport.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

/**
 * 金融市场业务排名情况表（当月）（累计）
 */
@Table(name = "IFS_REPORT_JRSCBYWGL_YWPM")
public class IfsReportJrscbywglYwpm implements Serializable {
    /**
     * 月份
     */
    @Column(name = "YF")
    private String yf;

    /**
     * 现券_交易量
     */
    @Column(name = "XQ_JYL")
    private BigDecimal xqJyl;

    /**
     * 现券_城商行排名
     */
    @Column(name = "XQ_CSHPM")
    private BigDecimal xqCshpm;

    /**
     * 现券_全市场排名
     */
    @Column(name = "XQ_QSCPM")
    private BigDecimal xqQscpm;

    /**
     * 质押式回购_交易量
     */
    @Column(name = "ZYS_JYL")
    private BigDecimal zysJyl;

    /**
     * 质押式回购_城商行排名
     */
    @Column(name = "ZYS_CSHPM")
    private BigDecimal zysCshpm;

    /**
     * 质押式回购_全市场排名
     */
    @Column(name = "ZYS_QSCPM")
    private BigDecimal zysQscpm;

    /**
     * 买断式回购_交易量
     */
    @Column(name = "MDS_JYL")
    private BigDecimal mdsJyl;

    /**
     * 买断式回购_城商行排名
     */
    @Column(name = "MDS_CSHPM")
    private BigDecimal mdsCshpm;

    /**
     * 买断式回购_全市场排名
     */
    @Column(name = "MDS_QSCPM")
    private BigDecimal mdsQscpm;

    /**
     * 债券借贷_交易量
     */
    @Column(name = "ZQJD_JYL")
    private BigDecimal zqjdJyl;

    /**
     * 债券借贷_城商行排名
     */
    @Column(name = "ZQJD_CSHPM")
    private BigDecimal zqjdCshpm;

    /**
     * 债券借贷_全市场排名
     */
    @Column(name = "ZQJD_QSCPM")
    private BigDecimal zqjdQscpm;

    /**
     * 总体_总交易量
     */
    @Column(name = "ZT_ZJYL")
    private BigDecimal ztZjyl;

    /**
     * 总体_城商行排名
     */
    @Column(name = "ZT_CSHPM")
    private BigDecimal ztCshpm;

    /**
     * 总体_全市场排名
     */
    @Column(name = "ZT_QSCPM")
    private BigDecimal ztQscpm;

    /**
     * 市场_城商行排名
     */
    @Column(name = "SC_CSHPM")
    private BigDecimal scCshpm;

    /**
     * 市场_全市场排名
     */
    @Column(name = "SC_QSCPM")
    private BigDecimal scQscpm;

    private static final long serialVersionUID = 1L;

    /**
     * 获取月份
     *
     * @return YF - 月份
     */
    public String getYf() {
        return yf;
    }

    /**
     * 设置月份
     *
     * @param yf 月份
     */
    public void setYf(String yf) {
        this.yf = yf;
    }

    /**
     * 获取现券_交易量
     *
     * @return XQ_JYL - 现券_交易量
     */
    public BigDecimal getXqJyl() {
        return xqJyl;
    }

    /**
     * 设置现券_交易量
     *
     * @param xqJyl 现券_交易量
     */
    public void setXqJyl(BigDecimal xqJyl) {
        this.xqJyl = xqJyl;
    }

    /**
     * 获取现券_城商行排名
     *
     * @return XQ_CSHPM - 现券_城商行排名
     */
    public BigDecimal getXqCshpm() {
        return xqCshpm;
    }

    /**
     * 设置现券_城商行排名
     *
     * @param xqCshpm 现券_城商行排名
     */
    public void setXqCshpm(BigDecimal xqCshpm) {
        this.xqCshpm = xqCshpm;
    }

    /**
     * 获取现券_全市场排名
     *
     * @return XQ_QSCPM - 现券_全市场排名
     */
    public BigDecimal getXqQscpm() {
        return xqQscpm;
    }

    /**
     * 设置现券_全市场排名
     *
     * @param xqQscpm 现券_全市场排名
     */
    public void setXqQscpm(BigDecimal xqQscpm) {
        this.xqQscpm = xqQscpm;
    }

    /**
     * 获取质押式回购_交易量
     *
     * @return ZYS_JYL - 质押式回购_交易量
     */
    public BigDecimal getZysJyl() {
        return zysJyl;
    }

    /**
     * 设置质押式回购_交易量
     *
     * @param zysJyl 质押式回购_交易量
     */
    public void setZysJyl(BigDecimal zysJyl) {
        this.zysJyl = zysJyl;
    }

    /**
     * 获取质押式回购_城商行排名
     *
     * @return ZYS_CSHPM - 质押式回购_城商行排名
     */
    public BigDecimal getZysCshpm() {
        return zysCshpm;
    }

    /**
     * 设置质押式回购_城商行排名
     *
     * @param zysCshpm 质押式回购_城商行排名
     */
    public void setZysCshpm(BigDecimal zysCshpm) {
        this.zysCshpm = zysCshpm;
    }

    /**
     * 获取质押式回购_全市场排名
     *
     * @return ZYS_QSCPM - 质押式回购_全市场排名
     */
    public BigDecimal getZysQscpm() {
        return zysQscpm;
    }

    /**
     * 设置质押式回购_全市场排名
     *
     * @param zysQscpm 质押式回购_全市场排名
     */
    public void setZysQscpm(BigDecimal zysQscpm) {
        this.zysQscpm = zysQscpm;
    }

    /**
     * 获取买断式回购_交易量
     *
     * @return MDS_JYL - 买断式回购_交易量
     */
    public BigDecimal getMdsJyl() {
        return mdsJyl;
    }

    /**
     * 设置买断式回购_交易量
     *
     * @param mdsJyl 买断式回购_交易量
     */
    public void setMdsJyl(BigDecimal mdsJyl) {
        this.mdsJyl = mdsJyl;
    }

    /**
     * 获取买断式回购_城商行排名
     *
     * @return MDS_CSHPM - 买断式回购_城商行排名
     */
    public BigDecimal getMdsCshpm() {
        return mdsCshpm;
    }

    /**
     * 设置买断式回购_城商行排名
     *
     * @param mdsCshpm 买断式回购_城商行排名
     */
    public void setMdsCshpm(BigDecimal mdsCshpm) {
        this.mdsCshpm = mdsCshpm;
    }

    /**
     * 获取买断式回购_全市场排名
     *
     * @return MDS_QSCPM - 买断式回购_全市场排名
     */
    public BigDecimal getMdsQscpm() {
        return mdsQscpm;
    }

    /**
     * 设置买断式回购_全市场排名
     *
     * @param mdsQscpm 买断式回购_全市场排名
     */
    public void setMdsQscpm(BigDecimal mdsQscpm) {
        this.mdsQscpm = mdsQscpm;
    }

    /**
     * 获取债券借贷_交易量
     *
     * @return ZQJD_JYL - 债券借贷_交易量
     */
    public BigDecimal getZqjdJyl() {
        return zqjdJyl;
    }

    /**
     * 设置债券借贷_交易量
     *
     * @param zqjdJyl 债券借贷_交易量
     */
    public void setZqjdJyl(BigDecimal zqjdJyl) {
        this.zqjdJyl = zqjdJyl;
    }

    /**
     * 获取债券借贷_城商行排名
     *
     * @return ZQJD_CSHPM - 债券借贷_城商行排名
     */
    public BigDecimal getZqjdCshpm() {
        return zqjdCshpm;
    }

    /**
     * 设置债券借贷_城商行排名
     *
     * @param zqjdCshpm 债券借贷_城商行排名
     */
    public void setZqjdCshpm(BigDecimal zqjdCshpm) {
        this.zqjdCshpm = zqjdCshpm;
    }

    /**
     * 获取债券借贷_全市场排名
     *
     * @return ZQJD_QSCPM - 债券借贷_全市场排名
     */
    public BigDecimal getZqjdQscpm() {
        return zqjdQscpm;
    }

    /**
     * 设置债券借贷_全市场排名
     *
     * @param zqjdQscpm 债券借贷_全市场排名
     */
    public void setZqjdQscpm(BigDecimal zqjdQscpm) {
        this.zqjdQscpm = zqjdQscpm;
    }

    /**
     * 获取总体_总交易量
     *
     * @return ZT_ZJYL - 总体_总交易量
     */
    public BigDecimal getZtZjyl() {
        return ztZjyl;
    }

    /**
     * 设置总体_总交易量
     *
     * @param ztZjyl 总体_总交易量
     */
    public void setZtZjyl(BigDecimal ztZjyl) {
        this.ztZjyl = ztZjyl;
    }

    /**
     * 获取总体_城商行排名
     *
     * @return ZT_CSHPM - 总体_城商行排名
     */
    public BigDecimal getZtCshpm() {
        return ztCshpm;
    }

    /**
     * 设置总体_城商行排名
     *
     * @param ztCshpm 总体_城商行排名
     */
    public void setZtCshpm(BigDecimal ztCshpm) {
        this.ztCshpm = ztCshpm;
    }

    /**
     * 获取总体_全市场排名
     *
     * @return ZT_QSCPM - 总体_全市场排名
     */
    public BigDecimal getZtQscpm() {
        return ztQscpm;
    }

    /**
     * 设置总体_全市场排名
     *
     * @param ztQscpm 总体_全市场排名
     */
    public void setZtQscpm(BigDecimal ztQscpm) {
        this.ztQscpm = ztQscpm;
    }

    /**
     * 获取市场_城商行排名
     *
     * @return SC_CSHPM - 市场_城商行排名
     */
    public BigDecimal getScCshpm() {
        return scCshpm;
    }

    /**
     * 设置市场_城商行排名
     *
     * @param scCshpm 市场_城商行排名
     */
    public void setScCshpm(BigDecimal scCshpm) {
        this.scCshpm = scCshpm;
    }

    /**
     * 获取市场_全市场排名
     *
     * @return SC_QSCPM - 市场_全市场排名
     */
    public BigDecimal getScQscpm() {
        return scQscpm;
    }

    /**
     * 设置市场_全市场排名
     *
     * @param scQscpm 市场_全市场排名
     */
    public void setScQscpm(BigDecimal scQscpm) {
        this.scQscpm = scQscpm;
    }
}