package com.singlee.hrbnewport.model;

import java.math.BigDecimal;

/**
 *
 * 2021/12/7
 * @Auther:zk
 */    
    
/**
    * G14台账明细
    */
public class IfsReportG14Detail {
    /**
    * 客户名称
    */
    private String custName;

    /**
    * 余额/金额
    */
    private BigDecimal amt;

    /**
    * 减值准备
    */
    private BigDecimal decrease;

    /**
    * 考虑风险缓释作用的风险暴露--一般风险暴露
    */
    private BigDecimal genRiskSum;

    /**
    * 考虑风险缓释作用的风险暴露--特定风险暴露
    */
    private BigDecimal specRiskSum;

    /**
    * 考虑风险缓释作用的风险暴露--交易账簿风险暴露
    */
    private BigDecimal tradeRiskExp;

    /**
    * 考虑风险缓释作用的风险暴露--交易对手信用风险暴露
    */
    private BigDecimal custRiskSum;

    /**
    * 考虑风险缓释作用的风险暴露--潜在风险暴露
    */
    private BigDecimal qzRiskExp;

    /**
    * 考虑风险缓释作用的风险暴露--其他风险暴露
    */
    private BigDecimal qtRiskExp;

    /**
    * 考虑风险缓释作用的风险暴露--不可豁免风险暴露
    */
    private BigDecimal bkhmRiskExp;

    /**
    * 风险缓释作用
    */
    private BigDecimal revRiskEffect;

    /**
    * 考虑风险缓释作用的风险暴露合计
    */
    private BigDecimal includRevRisk;

    /**
    * 风险缓释转出的风险暴露（转入为负数）
    */
    private BigDecimal revRiskExp;

    /**
    * 不考虑风险缓释作用的风险暴露合计
    */
    private BigDecimal noneRevRisk;

    /**
    * 不考虑风险缓释作用的风险暴露-债券投资
    */
    private BigDecimal riskSec;

    /**
    * 不考虑风险缓释作用的风险暴露-资产管理产品
    */
    private BigDecimal riskZcgl;

    /**
    * 不考虑风险缓释作用的风险暴露-资产管理产品-信托产品
    */
    private BigDecimal riskZcglXt;

    /**
    * 不考虑风险缓释作用的风险暴露-资产管理产品-非保本理财
    */
    private BigDecimal riskZcglLc;

    /**
    * 不考虑风险缓释作用的风险暴露-资产管理产品-证券业资管产品
    */
    private BigDecimal riskZcglZq;

    /**
    * 不考虑风险缓释作用的风险暴露-资产证券化产品
    */
    private BigDecimal riskAbs;

    /**
    * 补充1(债券类型)
    */
    private String remarkSecType;

    /**
    * 补充2(所属集团)
    */
    private String remarkGroup;

    /**
    * 补充3(客户类型)
    */
    private String remarkCustType;

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public BigDecimal getAmt() {
        return amt;
    }

    public void setAmt(BigDecimal amt) {
        this.amt = amt;
    }

    public BigDecimal getDecrease() {
        return decrease;
    }

    public void setDecrease(BigDecimal decrease) {
        this.decrease = decrease;
    }

    public BigDecimal getGenRiskSum() {
        return genRiskSum;
    }

    public void setGenRiskSum(BigDecimal genRiskSum) {
        this.genRiskSum = genRiskSum;
    }

    public BigDecimal getSpecRiskSum() {
        return specRiskSum;
    }

    public void setSpecRiskSum(BigDecimal specRiskSum) {
        this.specRiskSum = specRiskSum;
    }

    public BigDecimal getTradeRiskExp() {
        return tradeRiskExp;
    }

    public void setTradeRiskExp(BigDecimal tradeRiskExp) {
        this.tradeRiskExp = tradeRiskExp;
    }

    public BigDecimal getCustRiskSum() {
        return custRiskSum;
    }

    public void setCustRiskSum(BigDecimal custRiskSum) {
        this.custRiskSum = custRiskSum;
    }

    public BigDecimal getQzRiskExp() {
        return qzRiskExp;
    }

    public void setQzRiskExp(BigDecimal qzRiskExp) {
        this.qzRiskExp = qzRiskExp;
    }

    public BigDecimal getQtRiskExp() {
        return qtRiskExp;
    }

    public void setQtRiskExp(BigDecimal qtRiskExp) {
        this.qtRiskExp = qtRiskExp;
    }

    public BigDecimal getBkhmRiskExp() {
        return bkhmRiskExp;
    }

    public void setBkhmRiskExp(BigDecimal bkhmRiskExp) {
        this.bkhmRiskExp = bkhmRiskExp;
    }

    public BigDecimal getRevRiskEffect() {
        return revRiskEffect;
    }

    public void setRevRiskEffect(BigDecimal revRiskEffect) {
        this.revRiskEffect = revRiskEffect;
    }

    public BigDecimal getIncludRevRisk() {
        return includRevRisk;
    }

    public void setIncludRevRisk(BigDecimal includRevRisk) {
        this.includRevRisk = includRevRisk;
    }

    public BigDecimal getRevRiskExp() {
        return revRiskExp;
    }

    public void setRevRiskExp(BigDecimal revRiskExp) {
        this.revRiskExp = revRiskExp;
    }

    public BigDecimal getNoneRevRisk() {
        return noneRevRisk;
    }

    public void setNoneRevRisk(BigDecimal noneRevRisk) {
        this.noneRevRisk = noneRevRisk;
    }

    public BigDecimal getRiskSec() {
        return riskSec;
    }

    public void setRiskSec(BigDecimal riskSec) {
        this.riskSec = riskSec;
    }

    public BigDecimal getRiskZcgl() {
        return riskZcgl;
    }

    public void setRiskZcgl(BigDecimal riskZcgl) {
        this.riskZcgl = riskZcgl;
    }

    public BigDecimal getRiskZcglXt() {
        return riskZcglXt;
    }

    public void setRiskZcglXt(BigDecimal riskZcglXt) {
        this.riskZcglXt = riskZcglXt;
    }

    public BigDecimal getRiskZcglLc() {
        return riskZcglLc;
    }

    public void setRiskZcglLc(BigDecimal riskZcglLc) {
        this.riskZcglLc = riskZcglLc;
    }

    public BigDecimal getRiskZcglZq() {
        return riskZcglZq;
    }

    public void setRiskZcglZq(BigDecimal riskZcglZq) {
        this.riskZcglZq = riskZcglZq;
    }

    public BigDecimal getRiskAbs() {
        return riskAbs;
    }

    public void setRiskAbs(BigDecimal riskAbs) {
        this.riskAbs = riskAbs;
    }

    public String getRemarkSecType() {
        return remarkSecType;
    }

    public void setRemarkSecType(String remarkSecType) {
        this.remarkSecType = remarkSecType;
    }

    public String getRemarkGroup() {
        return remarkGroup;
    }

    public void setRemarkGroup(String remarkGroup) {
        this.remarkGroup = remarkGroup;
    }

    public String getRemarkCustType() {
        return remarkCustType;
    }

    public void setRemarkCustType(String remarkCustType) {
        this.remarkCustType = remarkCustType;
    }
}