package com.singlee.hrbnewport.model;

import java.math.BigDecimal;

/**
 *
 * 2021/12/8
 * @Auther:zk
 */    
    
/**
    * 同业业务管理报告-基金投资（资产）投资业务明细表
    */
public class YjInnerbankFund {
    /**
    * 序号
    */
    private String seq;

    /**
    * 基金管理人
    */
    private String fundManager;

    /**
    * 基金简称
    */
    private String fundName;

    /**
    * 基金代码
    */
    private String fundCode;

    /**
    * 交易方向
    */
    private String dealDir;

    /**
    * 交易日期
    */
    private String dealDate;

    /**
    * 余额(万元)
    */
    private BigDecimal amt;

    public String getSeq() {
        return seq;
    }

    public void setSeq(String seq) {
        this.seq = seq;
    }

    public String getFundManager() {
        return fundManager;
    }

    public void setFundManager(String fundManager) {
        this.fundManager = fundManager;
    }

    public String getFundName() {
        return fundName;
    }

    public void setFundName(String fundName) {
        this.fundName = fundName;
    }

    public String getFundCode() {
        return fundCode;
    }

    public void setFundCode(String fundCode) {
        this.fundCode = fundCode;
    }

    public String getDealDir() {
        return dealDir;
    }

    public void setDealDir(String dealDir) {
        this.dealDir = dealDir;
    }

    public String getDealDate() {
        return dealDate;
    }

    public void setDealDate(String dealDate) {
        this.dealDate = dealDate;
    }

    public BigDecimal getAmt() {
        return amt;
    }

    public void setAmt(BigDecimal amt) {
        this.amt = amt;
    }
}