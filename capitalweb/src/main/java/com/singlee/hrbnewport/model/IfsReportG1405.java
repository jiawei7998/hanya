package com.singlee.hrbnewport.model;

import java.math.BigDecimal;

/**
 * @Author zhangkai
 * @Description 
 * @Date 
 */
/**
    * G1405大额风险暴露统计报表[G14_II不限制,G14_V同业单一客户100,G14_VI集团客户30]
    */
public class IfsReportG1405 {
    /**
    * 客户类型
    */
    private String custType;

    /**
    * 客户名称
    */
    private String custName;

    /**
    * 客户代码(组织机构代码)
    */
    private String custOrgcode;

    /**
    * 风险暴露综合-合计
    */
    private BigDecimal riskExpSum;

    /**
    * 风险暴露综合-其中:不可豁免风险暴漏(三家政策性银行是0)
    */
    private BigDecimal riskExp;

    /**
    * 占一级资本净额比例-合计
    */
    private BigDecimal netCapSum;

    /**
    * 占一级资本净额比例-其中不可豁免风险暴露
    */
    private BigDecimal netCap;

    /**
    * 一般风险暴露 (商业银行大额风险暴露管理办法)-合计
    */
    private BigDecimal genRiskSum;

    /**
    * 一般风险暴露 (商业银行大额风险暴露管理办法)-其中：拆放同业
    */
    private BigDecimal genRiskCafty;

    /**
    * 一般风险暴露 (商业银行大额风险暴露管理办法)-其中：同业拆借
    */
    private BigDecimal genRiskTycj;

    /**
    * 一般风险暴露 (商业银行大额风险暴露管理办法)-其中：同业拆借
    */
    private BigDecimal genRiskTyjk;

    /**
    * 一般风险暴露 (商业银行大额风险暴露管理办法)-其中：存放同业
    */
    private BigDecimal genRiskCufty;

    /**
    * 一般风险暴露 (商业银行大额风险暴露管理办法)-其中：买入返售(质押式)
    */
    private BigDecimal genRiskMrfszy;

    /**
    * 一般风险暴露 (商业银行大额风险暴露管理办法)-其中：同业债券（发行人主体类型是金融机构都算）
    */
    private BigDecimal genRiskJqzq;

    /**
    * 一般风险暴露 (商业银行大额风险暴露管理办法)-其中：政策性金融债
    */
    private BigDecimal genRiskZcxzq;

    /**
    * 一般风险暴露 (商业银行大额风险暴露管理办法)-其中：同业存单
    */
    private BigDecimal genRiskTycd;

    /**
    * 特定风险暴露-合计
    */
    private BigDecimal specRiskSum;

    /**
    * 特定风险暴露-资产管理产品
    */
    private BigDecimal specRiskZcgl;

    /**
    * 特定风险暴露-其中：信托产品
    */
    private BigDecimal specRiskXt;

    /**
    * 特定风险暴露-其中：非保本理财
    */
    private BigDecimal specRiskFblc;

    /**
    * 特定风险暴露-其中：证券业资管产品
    */
    private BigDecimal specRiskZqzg;

    /**
    * 特定风险暴露-其中：资产证券化产品
    */
    private BigDecimal specRiskZczqh;

    /**
    * 交易账簿风险暴露
    */
    private BigDecimal tradeRiskExp;

    /**
    * 交易对手信用风险暴露-合计
    */
    private BigDecimal custRiskSum;

    /**
    * 交易对手信用风险暴露-其中：场外衍生工具
    */
    private BigDecimal custRiskCwysp;

    /**
    * 交易对手信用风险暴露-其中：证券融资交易
    */
    private BigDecimal custRiskZqrz;

    /**
    * 潜在风险暴露
    */
    private BigDecimal qzRiskExp;

    /**
    * 其他风险暴露
    */
    private BigDecimal qtRiskExp;

    /**
    * 风险缓释转出的风险暴露（转入为负数）
    */
    private BigDecimal revRiskExp;

    /**
    * 不考虑风险缓释作用的风险暴露综合
    */
    private BigDecimal noneRevRisk;

    public String getCustType() {
        return custType;
    }

    public void setCustType(String custType) {
        this.custType = custType;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getCustOrgcode() {
        return custOrgcode;
    }

    public void setCustOrgcode(String custOrgcode) {
        this.custOrgcode = custOrgcode;
    }

    public BigDecimal getRiskExpSum() {
        return riskExpSum;
    }

    public void setRiskExpSum(BigDecimal riskExpSum) {
        this.riskExpSum = riskExpSum;
    }

    public BigDecimal getRiskExp() {
        return riskExp;
    }

    public void setRiskExp(BigDecimal riskExp) {
        this.riskExp = riskExp;
    }

    public BigDecimal getNetCapSum() {
        return netCapSum;
    }

    public void setNetCapSum(BigDecimal netCapSum) {
        this.netCapSum = netCapSum;
    }

    public BigDecimal getNetCap() {
        return netCap;
    }

    public void setNetCap(BigDecimal netCap) {
        this.netCap = netCap;
    }

    public BigDecimal getGenRiskSum() {
        return genRiskSum;
    }

    public void setGenRiskSum(BigDecimal genRiskSum) {
        this.genRiskSum = genRiskSum;
    }

    public BigDecimal getGenRiskCafty() {
        return genRiskCafty;
    }

    public void setGenRiskCafty(BigDecimal genRiskCafty) {
        this.genRiskCafty = genRiskCafty;
    }

    public BigDecimal getGenRiskTycj() {
        return genRiskTycj;
    }

    public void setGenRiskTycj(BigDecimal genRiskTycj) {
        this.genRiskTycj = genRiskTycj;
    }

    public BigDecimal getGenRiskTyjk() {
        return genRiskTyjk;
    }

    public void setGenRiskTyjk(BigDecimal genRiskTyjk) {
        this.genRiskTyjk = genRiskTyjk;
    }

    public BigDecimal getGenRiskCufty() {
        return genRiskCufty;
    }

    public void setGenRiskCufty(BigDecimal genRiskCufty) {
        this.genRiskCufty = genRiskCufty;
    }

    public BigDecimal getGenRiskMrfszy() {
        return genRiskMrfszy;
    }

    public void setGenRiskMrfszy(BigDecimal genRiskMrfszy) {
        this.genRiskMrfszy = genRiskMrfszy;
    }

    public BigDecimal getGenRiskJqzq() {
        return genRiskJqzq;
    }

    public void setGenRiskJqzq(BigDecimal genRiskJqzq) {
        this.genRiskJqzq = genRiskJqzq;
    }

    public BigDecimal getGenRiskZcxzq() {
        return genRiskZcxzq;
    }

    public void setGenRiskZcxzq(BigDecimal genRiskZcxzq) {
        this.genRiskZcxzq = genRiskZcxzq;
    }

    public BigDecimal getGenRiskTycd() {
        return genRiskTycd;
    }

    public void setGenRiskTycd(BigDecimal genRiskTycd) {
        this.genRiskTycd = genRiskTycd;
    }

    public BigDecimal getSpecRiskSum() {
        return specRiskSum;
    }

    public void setSpecRiskSum(BigDecimal specRiskSum) {
        this.specRiskSum = specRiskSum;
    }

    public BigDecimal getSpecRiskZcgl() {
        return specRiskZcgl;
    }

    public void setSpecRiskZcgl(BigDecimal specRiskZcgl) {
        this.specRiskZcgl = specRiskZcgl;
    }

    public BigDecimal getSpecRiskXt() {
        return specRiskXt;
    }

    public void setSpecRiskXt(BigDecimal specRiskXt) {
        this.specRiskXt = specRiskXt;
    }

    public BigDecimal getSpecRiskFblc() {
        return specRiskFblc;
    }

    public void setSpecRiskFblc(BigDecimal specRiskFblc) {
        this.specRiskFblc = specRiskFblc;
    }

    public BigDecimal getSpecRiskZqzg() {
        return specRiskZqzg;
    }

    public void setSpecRiskZqzg(BigDecimal specRiskZqzg) {
        this.specRiskZqzg = specRiskZqzg;
    }

    public BigDecimal getSpecRiskZczqh() {
        return specRiskZczqh;
    }

    public void setSpecRiskZczqh(BigDecimal specRiskZczqh) {
        this.specRiskZczqh = specRiskZczqh;
    }

    public BigDecimal getTradeRiskExp() {
        return tradeRiskExp;
    }

    public void setTradeRiskExp(BigDecimal tradeRiskExp) {
        this.tradeRiskExp = tradeRiskExp;
    }

    public BigDecimal getCustRiskSum() {
        return custRiskSum;
    }

    public void setCustRiskSum(BigDecimal custRiskSum) {
        this.custRiskSum = custRiskSum;
    }

    public BigDecimal getCustRiskCwysp() {
        return custRiskCwysp;
    }

    public void setCustRiskCwysp(BigDecimal custRiskCwysp) {
        this.custRiskCwysp = custRiskCwysp;
    }

    public BigDecimal getCustRiskZqrz() {
        return custRiskZqrz;
    }

    public void setCustRiskZqrz(BigDecimal custRiskZqrz) {
        this.custRiskZqrz = custRiskZqrz;
    }

    public BigDecimal getQzRiskExp() {
        return qzRiskExp;
    }

    public void setQzRiskExp(BigDecimal qzRiskExp) {
        this.qzRiskExp = qzRiskExp;
    }

    public BigDecimal getQtRiskExp() {
        return qtRiskExp;
    }

    public void setQtRiskExp(BigDecimal qtRiskExp) {
        this.qtRiskExp = qtRiskExp;
    }

    public BigDecimal getRevRiskExp() {
        return revRiskExp;
    }

    public void setRevRiskExp(BigDecimal revRiskExp) {
        this.revRiskExp = revRiskExp;
    }

    public BigDecimal getNoneRevRisk() {
        return noneRevRisk;
    }

    public void setNoneRevRisk(BigDecimal noneRevRisk) {
        this.noneRevRisk = noneRevRisk;
    }
}