package com.singlee.hrbnewport.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

/**
 * G3301银行账簿利率风险计量报表
 */
@Table(name = "IFS_REPORT_G3301")
public class IfsReportG3301 implements Serializable {
    /**
     * 项目
     */
    @Column(name = "PRODUCT")
    private String product;

    /**
     * 合计金额
     */
    @Column(name = "AMT")
    private BigDecimal amt;

    /**
     * 隔夜
     */
    @Column(name = "TENOR_1D")
    private BigDecimal tenor1d;

    /**
     * 隔夜-一个月（含）
     */
    @Column(name = "TENOR_1M")
    private BigDecimal tenor1m;

    /**
     * 1个月-3个月（含）
     */
    @Column(name = "TENOR_3M")
    private BigDecimal tenor3m;

    /**
     * 3个月-6个月（含）
     */
    @Column(name = "TENOR_6M")
    private BigDecimal tenor6m;

    /**
     * 6个月-9个月(含)
     */
    @Column(name = "TENOR_9M")
    private BigDecimal tenor9m;

    /**
     * 9个月-1年(含)
     */
    @Column(name = "TENOR_1Y")
    private BigDecimal tenor1y;

    /**
     * 1年-1.5年(含)
     */
    @Column(name = "TENOR_1_5Y")
    private BigDecimal tenor1d5y;

    /**
     * 1.5年-2年(含)
     */
    @Column(name = "TENOR_2Y")
    private BigDecimal tenor2y;

    /**
     * 2年-3年(含)
     */
    @Column(name = "TENOR_3Y")
    private BigDecimal tenor3y;

    /**
     * 3年-4年(含)
     */
    @Column(name = "TENOR_4Y")
    private BigDecimal tenor4y;

    /**
     * 4年-5年(含)
     */
    @Column(name = "TENOR_5Y")
    private BigDecimal tenor5y;

    /**
     * 5年-6年(含)
     */
    @Column(name = "TENOR_6Y")
    private BigDecimal tenor6y;

    /**
     * 6年-7年(含)
     */
    @Column(name = "TENOR_7Y")
    private BigDecimal tenor7y;

    /**
     * 7年-8年(含)
     */
    @Column(name = "TENOR_8Y")
    private BigDecimal tenor8y;

    /**
     * 8年-9年(含)
     */
    @Column(name = "TENOR_9Y")
    private BigDecimal tenor9y;

    /**
     * 9年-10年(含)
     */
    @Column(name = "TENOR_10Y")
    private BigDecimal tenor10y;

    /**
     * 10年-15年(含)
     */
    @Column(name = "TENOR_15Y")
    private BigDecimal tenor15y;

    /**
     * 15年-20年(含)
     */
    @Column(name = "TENOR_20Y")
    private BigDecimal tenor20y;

    /**
     * 20年以上
     */
    @Column(name = "TENOR_REMAING")
    private BigDecimal tenorRemaing;

    /**
     * 父级项目
     */
    @Column(name = "PARENT_PRODUCT")
    private String parentProduct;

    private static final long serialVersionUID = 1L;

    /**
     * 获取项目
     *
     * @return PRODUCT - 项目
     */
    public String getProduct() {
        return product;
    }

    /**
     * 设置项目
     *
     * @param product 项目
     */
    public void setProduct(String product) {
        this.product = product;
    }

    /**
     * 获取合计金额
     *
     * @return AMT - 合计金额
     */
    public BigDecimal getAmt() {
        return amt;
    }

    /**
     * 设置合计金额
     *
     * @param amt 合计金额
     */
    public void setAmt(BigDecimal amt) {
        this.amt = amt;
    }

    /**
     * 获取隔夜
     *
     * @return TENOR_1D - 隔夜
     */
    public BigDecimal getTenor1d() {
        return tenor1d;
    }

    /**
     * 设置隔夜
     *
     * @param tenor1d 隔夜
     */
    public void setTenor1d(BigDecimal tenor1d) {
        this.tenor1d = tenor1d;
    }

    /**
     * 获取隔夜-一个月（含）
     *
     * @return TENOR_1M - 隔夜-一个月（含）
     */
    public BigDecimal getTenor1m() {
        return tenor1m;
    }

    /**
     * 设置隔夜-一个月（含）
     *
     * @param tenor1m 隔夜-一个月（含）
     */
    public void setTenor1m(BigDecimal tenor1m) {
        this.tenor1m = tenor1m;
    }

    /**
     * 获取1个月-3个月（含）
     *
     * @return TENOR_3M - 1个月-3个月（含）
     */
    public BigDecimal getTenor3m() {
        return tenor3m;
    }

    /**
     * 设置1个月-3个月（含）
     *
     * @param tenor3m 1个月-3个月（含）
     */
    public void setTenor3m(BigDecimal tenor3m) {
        this.tenor3m = tenor3m;
    }

    /**
     * 获取3个月-6个月（含）
     *
     * @return TENOR_6M - 3个月-6个月（含）
     */
    public BigDecimal getTenor6m() {
        return tenor6m;
    }

    /**
     * 设置3个月-6个月（含）
     *
     * @param tenor6m 3个月-6个月（含）
     */
    public void setTenor6m(BigDecimal tenor6m) {
        this.tenor6m = tenor6m;
    }

    /**
     * 获取6个月-9个月(含)
     *
     * @return TENOR_9M - 6个月-9个月(含)
     */
    public BigDecimal getTenor9m() {
        return tenor9m;
    }

    /**
     * 设置6个月-9个月(含)
     *
     * @param tenor9m 6个月-9个月(含)
     */
    public void setTenor9m(BigDecimal tenor9m) {
        this.tenor9m = tenor9m;
    }

    /**
     * 获取9个月-1年(含)
     *
     * @return TENOR_1Y - 9个月-1年(含)
     */
    public BigDecimal getTenor1y() {
        return tenor1y;
    }

    /**
     * 设置9个月-1年(含)
     *
     * @param tenor1y 9个月-1年(含)
     */
    public void setTenor1y(BigDecimal tenor1y) {
        this.tenor1y = tenor1y;
    }

    /**
     * 获取1年-1.5年(含)
     *
     * @return TENOR_1_5Y - 1年-1.5年(含)
     */
    public BigDecimal getTenor1d5y() {
        return tenor1d5y;
    }

    /**
     * 设置1年-1.5年(含)
     *
     * @param tenor15y 1年-1.5年(含)
     */
    public void setTenor1d5y(BigDecimal tenor1d5y) {
        this.tenor1d5y = tenor1d5y;
    }

    /**
     * 获取1.5年-2年(含)
     *
     * @return TENOR_2Y - 1.5年-2年(含)
     */
    public BigDecimal getTenor2y() {
        return tenor2y;
    }

    /**
     * 设置1.5年-2年(含)
     *
     * @param tenor2y 1.5年-2年(含)
     */
    public void setTenor2y(BigDecimal tenor2y) {
        this.tenor2y = tenor2y;
    }

    /**
     * 获取2年-3年(含)
     *
     * @return TENOR_3Y - 2年-3年(含)
     */
    public BigDecimal getTenor3y() {
        return tenor3y;
    }

    /**
     * 设置2年-3年(含)
     *
     * @param tenor3y 2年-3年(含)
     */
    public void setTenor3y(BigDecimal tenor3y) {
        this.tenor3y = tenor3y;
    }

    /**
     * 获取3年-4年(含)
     *
     * @return TENOR_4Y - 3年-4年(含)
     */
    public BigDecimal getTenor4y() {
        return tenor4y;
    }

    /**
     * 设置3年-4年(含)
     *
     * @param tenor4y 3年-4年(含)
     */
    public void setTenor4y(BigDecimal tenor4y) {
        this.tenor4y = tenor4y;
    }

    /**
     * 获取4年-5年(含)
     *
     * @return TENOR_5Y - 4年-5年(含)
     */
    public BigDecimal getTenor5y() {
        return tenor5y;
    }

    /**
     * 设置4年-5年(含)
     *
     * @param tenor5y 4年-5年(含)
     */
    public void setTenor5y(BigDecimal tenor5y) {
        this.tenor5y = tenor5y;
    }

    /**
     * 获取5年-6年(含)
     *
     * @return TENOR_6Y - 5年-6年(含)
     */
    public BigDecimal getTenor6y() {
        return tenor6y;
    }

    /**
     * 设置5年-6年(含)
     *
     * @param tenor6y 5年-6年(含)
     */
    public void setTenor6y(BigDecimal tenor6y) {
        this.tenor6y = tenor6y;
    }

    /**
     * 获取6年-7年(含)
     *
     * @return TENOR_7Y - 6年-7年(含)
     */
    public BigDecimal getTenor7y() {
        return tenor7y;
    }

    /**
     * 设置6年-7年(含)
     *
     * @param tenor7y 6年-7年(含)
     */
    public void setTenor7y(BigDecimal tenor7y) {
        this.tenor7y = tenor7y;
    }

    /**
     * 获取7年-8年(含)
     *
     * @return TENOR_8Y - 7年-8年(含)
     */
    public BigDecimal getTenor8y() {
        return tenor8y;
    }

    /**
     * 设置7年-8年(含)
     *
     * @param tenor8y 7年-8年(含)
     */
    public void setTenor8y(BigDecimal tenor8y) {
        this.tenor8y = tenor8y;
    }

    /**
     * 获取8年-9年(含)
     *
     * @return TENOR_9Y - 8年-9年(含)
     */
    public BigDecimal getTenor9y() {
        return tenor9y;
    }

    /**
     * 设置8年-9年(含)
     *
     * @param tenor9y 8年-9年(含)
     */
    public void setTenor9y(BigDecimal tenor9y) {
        this.tenor9y = tenor9y;
    }

    /**
     * 获取9年-10年(含)
     *
     * @return TENOR_10Y - 9年-10年(含)
     */
    public BigDecimal getTenor10y() {
        return tenor10y;
    }

    /**
     * 设置9年-10年(含)
     *
     * @param tenor10y 9年-10年(含)
     */
    public void setTenor10y(BigDecimal tenor10y) {
        this.tenor10y = tenor10y;
    }

    /**
     * 获取10年-15年(含)
     *
     * @return TENOR_15Y - 10年-15年(含)
     */
    public BigDecimal getTenor15y() {
        return tenor15y;
    }

    /**
     * 设置10年-15年(含)
     *
     * @param tenor15y 10年-15年(含)
     */
    public void setTenor15y(BigDecimal tenor15y) {
        this.tenor15y = tenor15y;
    }

    /**
     * 获取15年-20年(含)
     *
     * @return TENOR_20Y - 15年-20年(含)
     */
    public BigDecimal getTenor20y() {
        return tenor20y;
    }

    /**
     * 设置15年-20年(含)
     *
     * @param tenor20y 15年-20年(含)
     */
    public void setTenor20y(BigDecimal tenor20y) {
        this.tenor20y = tenor20y;
    }

    /**
     * 获取20年以上
     *
     * @return TENOR_REMAING - 20年以上
     */
    public BigDecimal getTenorRemaing() {
        return tenorRemaing;
    }

    /**
     * 设置20年以上
     *
     * @param tenorRemaing 20年以上
     */
    public void setTenorRemaing(BigDecimal tenorRemaing) {
        this.tenorRemaing = tenorRemaing;
    }

    /**
     * 获取父级项目
     *
     * @return PARENT_PRODUCT - 父级项目
     */
    public String getParentProduct() {
        return parentProduct;
    }

    /**
     * 设置父级项目
     *
     * @param parentProduct 父级项目
     */
    public void setParentProduct(String parentProduct) {
        this.parentProduct = parentProduct;
    }
}