package com.singlee.hrbnewport.model;

import java.math.BigDecimal;

/**
    * 外汇自营交易限额执行情况表
    */
public class IfsReportForezyjyxe {
    /**
    * 货币对
    */
    private String currencypair;

    /**
    * 单笔<=1000万美元笔数
    */
    private BigDecimal amount1;

    /**
    * 1000万美元<单笔<=1500万美元笔数
    */
    private BigDecimal amount2;

    /**
    * 1500万美元<单笔<=2000万美元笔数
    */
    private BigDecimal amount3;

    /**
    * 单笔>2000万美元笔数
    */
    private BigDecimal amount4;

    public String getCurrencypair() {
        return currencypair;
    }

    public void setCurrencypair(String currencypair) {
        this.currencypair = currencypair == null ? null : currencypair.trim();
    }

    public BigDecimal getAmount1() {
        return amount1;
    }

    public void setAmount1(BigDecimal amount1) {
        this.amount1 = amount1;
    }

    public BigDecimal getAmount2() {
        return amount2;
    }

    public void setAmount2(BigDecimal amount2) {
        this.amount2 = amount2;
    }

    public BigDecimal getAmount3() {
        return amount3;
    }

    public void setAmount3(BigDecimal amount3) {
        this.amount3 = amount3;
    }

    public BigDecimal getAmount4() {
        return amount4;
    }

    public void setAmount4(BigDecimal amount4) {
        this.amount4 = amount4;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", currencypair=").append(currencypair);
        sb.append(", amount1=").append(amount1);
        sb.append(", amount2=").append(amount2);
        sb.append(", amount3=").append(amount3);
        sb.append(", amount4=").append(amount4);
        sb.append("]");
        return sb.toString();
    }
}