package com.singlee.hrbnewport.model;

/**
    * 合并持仓分析表-明细变化
    */
public class IfsReportHbccmxbh {
    /**
    * 账户类型
    */
    private String invtype;

    /**
    * 债券类型
    */
    private String sd;

    /**
    * 债券发行人
    */
    private String issuer;

    /**
    * 成本中心
    */
    private String cost;

    /**
    * 债券代码
    */
    private String secid;

    /**
    * 投资组合
    */
    private String port;

    /**
    * 久期
    */
    private String jiuqi;

    /**
    * PVBP值
    */
    private String pvbpx;

    /**
    * PV01值
    */
    private String pv01x;

    /**
    * VAR值
    */
    private String varx;

    /**
    * 债券发行总量(万)
    */
    private String fxzl;

    /**
    * 债券所属行业
    */
    private String sshy;

    /**
    * 最新主体评级
    */
    private String ztlevel;

    /**
    * 最新债项评级
    */
    private String zxlevel;

    /**
    * 发行主体评级
    */
    private String fxztlevel;

    /**
    * 发行债项评级
    */
    private String fxzxlevel;

    /**
    * 债券名称
    */
    private String sname;

    /**
    * 起息日
    */
    private String vdate;

    /**
    * 期限
    */
    private String tenor;

    /**
    * 到期日
    */
    private String mdate;

    /**
    * 债券面值
    */
    private String prinamt;

    /**
    * 票面利率
    */
    private String couprate;

    /**
    * 持仓类别
    */
    private String acctngtype;

    /**
    * 持仓成本(元)
    */
    private String cccb;

    /**
    * 应收利息(元)
    */
    private String yslx;

    /**
    * 账面价值(元)
    */
    private String zmjz;

    /**
    * 应计利息(元)
    */
    private String yjlx;

    /**
    * 计算期利息收入(元)
    */
    private String dnljlxsr;

    /**
    * 持有期利息收入(元)
    */
    private String syljlxsr;

    /**
    * 公允价值变动(元)
    */
    private String tdymtm;

    /**
    * 公允价值变动损益(元)
    */
    private String tjpmtm;

    /**
    * 债券买入成本
    */
    private String avgcost;

    /**
    * 债券投资成本
    */
    private String settavgcost;

    /**
    * 估值
    */
    private String gz;

    /**
    * 账面净价市值(元)
    */
    private String opicsjjsz;

    /**
    * 净价市值(元)
    */
    private String jjsz;

    /**
    * OPICS_溢折价(元）
    */
    private String yzj;

    /**
    * 溢折价(元）
    */
    private String windyzj;

    /**
    * 利息调整(元)
    */
    private String lxtz;

    /**
    * 到期收益率
    */
    private String syl;

    /**
    * 百元加权成本
    */
    private String jqcbper;

    /**
    * 百元净价成本
    */
    private String jjcbper;

    /**
    * 百元应收利息
    */
    private String yslxper;

    /**
    * 百元应计利息
    */
    private String yjlxper;

    /**
    * 摊余净价(元)
    */
    private String tyjj;

    /**
    * OPICS_净价浮赢(元)
    */
    private String jjfy;

    /**
    * 净价浮赢(元)
    */
    private String windjjfy;

    /**
    * 剩余年限
    */
    private String synx;

    /**
    * 付息剩余天数
    */
    private String fxsyts;

    /**
    * 最近利率重定价日
    */
    private String cdjr;

    /**
    * 年限
    */
    private String nx;

    /**
    * 发行日期
    */
    private String fxrq;

    /**
    * 付息类
    */
    private String fxl;

    /**
    * 付息频率
    */
    private String fxpl;

    /**
    * 浮动类型
    */
    private String fdlx;

    /**
    * 基准利率
    */
    private String jzll;

    /**
    * 浮动基点
    */
    private String fdjd;

    /**
    * 利差
    */
    private String lc;

    /**
    * 含权类
    */
    private String hqz;

    /**
    * 发行人
    */
    private String ifname;

    /**
    * 是否平台债
    */
    private String ptz;

    /**
    * 是否担保
    */
    private String db;

    /**
    * 担保方式
    */
    private String dbfs;

    /**
    * 担保人
    */
    private String dbname;

    /**
    * 买断抵押面额(元)
    */
    private String rbamount;

    /**
    * 质押面额(元)
    */
    private String rpamount;

    /**
    * 国库定期存款质押面额
    */
    private String rdamount;

    /**
    * 债券借贷借入面额
    */
    private String sbamount;

    /**
    * 债券借贷借出面额
    */
    private String slamount;

    /**
    * 常备借贷便利质押面额
    */
    private String cbamount;

    /**
    * 中期借贷便利质押面额
    */
    private String zqamount;

    /**
    * 压债业务质押面额
    */
    private String yzamount;

    /**
    * 是否逾期
    */
    private String me;

    /**
    * 是否逾期
    */
    private String yuqi;

    /**
    * 备注
    */
    private String bz;

    public String getInvtype() {
        return invtype;
    }

    public void setInvtype(String invtype) {
        this.invtype = invtype == null ? null : invtype.trim();
    }

    public String getSd() {
        return sd;
    }

    public void setSd(String sd) {
        this.sd = sd == null ? null : sd.trim();
    }

    public String getIssuer() {
        return issuer;
    }

    public void setIssuer(String issuer) {
        this.issuer = issuer == null ? null : issuer.trim();
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost == null ? null : cost.trim();
    }

    public String getSecid() {
        return secid;
    }

    public void setSecid(String secid) {
        this.secid = secid == null ? null : secid.trim();
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port == null ? null : port.trim();
    }

    public String getJiuqi() {
        return jiuqi;
    }

    public void setJiuqi(String jiuqi) {
        this.jiuqi = jiuqi == null ? null : jiuqi.trim();
    }

    public String getPvbpx() {
        return pvbpx;
    }

    public void setPvbpx(String pvbpx) {
        this.pvbpx = pvbpx == null ? null : pvbpx.trim();
    }

    public String getPv01x() {
        return pv01x;
    }

    public void setPv01x(String pv01x) {
        this.pv01x = pv01x == null ? null : pv01x.trim();
    }

    public String getVarx() {
        return varx;
    }

    public void setVarx(String varx) {
        this.varx = varx == null ? null : varx.trim();
    }

    public String getFxzl() {
        return fxzl;
    }

    public void setFxzl(String fxzl) {
        this.fxzl = fxzl == null ? null : fxzl.trim();
    }

    public String getSshy() {
        return sshy;
    }

    public void setSshy(String sshy) {
        this.sshy = sshy == null ? null : sshy.trim();
    }

    public String getZtlevel() {
        return ztlevel;
    }

    public void setZtlevel(String ztlevel) {
        this.ztlevel = ztlevel == null ? null : ztlevel.trim();
    }

    public String getZxlevel() {
        return zxlevel;
    }

    public void setZxlevel(String zxlevel) {
        this.zxlevel = zxlevel == null ? null : zxlevel.trim();
    }

    public String getFxztlevel() {
        return fxztlevel;
    }

    public void setFxztlevel(String fxztlevel) {
        this.fxztlevel = fxztlevel == null ? null : fxztlevel.trim();
    }

    public String getFxzxlevel() {
        return fxzxlevel;
    }

    public void setFxzxlevel(String fxzxlevel) {
        this.fxzxlevel = fxzxlevel == null ? null : fxzxlevel.trim();
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname == null ? null : sname.trim();
    }

    public String getVdate() {
        return vdate;
    }

    public void setVdate(String vdate) {
        this.vdate = vdate == null ? null : vdate.trim();
    }

    public String getTenor() {
        return tenor;
    }

    public void setTenor(String tenor) {
        this.tenor = tenor == null ? null : tenor.trim();
    }

    public String getMdate() {
        return mdate;
    }

    public void setMdate(String mdate) {
        this.mdate = mdate == null ? null : mdate.trim();
    }

    public String getPrinamt() {
        return prinamt;
    }

    public void setPrinamt(String prinamt) {
        this.prinamt = prinamt == null ? null : prinamt.trim();
    }

    public String getCouprate() {
        return couprate;
    }

    public void setCouprate(String couprate) {
        this.couprate = couprate == null ? null : couprate.trim();
    }

    public String getAcctngtype() {
        return acctngtype;
    }

    public void setAcctngtype(String acctngtype) {
        this.acctngtype = acctngtype == null ? null : acctngtype.trim();
    }

    public String getCccb() {
        return cccb;
    }

    public void setCccb(String cccb) {
        this.cccb = cccb == null ? null : cccb.trim();
    }

    public String getYslx() {
        return yslx;
    }

    public void setYslx(String yslx) {
        this.yslx = yslx == null ? null : yslx.trim();
    }

    public String getZmjz() {
        return zmjz;
    }

    public void setZmjz(String zmjz) {
        this.zmjz = zmjz == null ? null : zmjz.trim();
    }

    public String getYjlx() {
        return yjlx;
    }

    public void setYjlx(String yjlx) {
        this.yjlx = yjlx == null ? null : yjlx.trim();
    }

    public String getDnljlxsr() {
        return dnljlxsr;
    }

    public void setDnljlxsr(String dnljlxsr) {
        this.dnljlxsr = dnljlxsr == null ? null : dnljlxsr.trim();
    }

    public String getSyljlxsr() {
        return syljlxsr;
    }

    public void setSyljlxsr(String syljlxsr) {
        this.syljlxsr = syljlxsr == null ? null : syljlxsr.trim();
    }

    public String getTdymtm() {
        return tdymtm;
    }

    public void setTdymtm(String tdymtm) {
        this.tdymtm = tdymtm == null ? null : tdymtm.trim();
    }

    public String getTjpmtm() {
        return tjpmtm;
    }

    public void setTjpmtm(String tjpmtm) {
        this.tjpmtm = tjpmtm == null ? null : tjpmtm.trim();
    }

    public String getAvgcost() {
        return avgcost;
    }

    public void setAvgcost(String avgcost) {
        this.avgcost = avgcost == null ? null : avgcost.trim();
    }

    public String getSettavgcost() {
        return settavgcost;
    }

    public void setSettavgcost(String settavgcost) {
        this.settavgcost = settavgcost == null ? null : settavgcost.trim();
    }

    public String getGz() {
        return gz;
    }

    public void setGz(String gz) {
        this.gz = gz == null ? null : gz.trim();
    }

    public String getOpicsjjsz() {
        return opicsjjsz;
    }

    public void setOpicsjjsz(String opicsjjsz) {
        this.opicsjjsz = opicsjjsz == null ? null : opicsjjsz.trim();
    }

    public String getJjsz() {
        return jjsz;
    }

    public void setJjsz(String jjsz) {
        this.jjsz = jjsz == null ? null : jjsz.trim();
    }

    public String getYzj() {
        return yzj;
    }

    public void setYzj(String yzj) {
        this.yzj = yzj == null ? null : yzj.trim();
    }

    public String getWindyzj() {
        return windyzj;
    }

    public void setWindyzj(String windyzj) {
        this.windyzj = windyzj == null ? null : windyzj.trim();
    }

    public String getLxtz() {
        return lxtz;
    }

    public void setLxtz(String lxtz) {
        this.lxtz = lxtz == null ? null : lxtz.trim();
    }

    public String getSyl() {
        return syl;
    }

    public void setSyl(String syl) {
        this.syl = syl == null ? null : syl.trim();
    }

    public String getJqcbper() {
        return jqcbper;
    }

    public void setJqcbper(String jqcbper) {
        this.jqcbper = jqcbper == null ? null : jqcbper.trim();
    }

    public String getJjcbper() {
        return jjcbper;
    }

    public void setJjcbper(String jjcbper) {
        this.jjcbper = jjcbper == null ? null : jjcbper.trim();
    }

    public String getYslxper() {
        return yslxper;
    }

    public void setYslxper(String yslxper) {
        this.yslxper = yslxper == null ? null : yslxper.trim();
    }

    public String getYjlxper() {
        return yjlxper;
    }

    public void setYjlxper(String yjlxper) {
        this.yjlxper = yjlxper == null ? null : yjlxper.trim();
    }

    public String getTyjj() {
        return tyjj;
    }

    public void setTyjj(String tyjj) {
        this.tyjj = tyjj == null ? null : tyjj.trim();
    }

    public String getJjfy() {
        return jjfy;
    }

    public void setJjfy(String jjfy) {
        this.jjfy = jjfy == null ? null : jjfy.trim();
    }

    public String getWindjjfy() {
        return windjjfy;
    }

    public void setWindjjfy(String windjjfy) {
        this.windjjfy = windjjfy == null ? null : windjjfy.trim();
    }

    public String getSynx() {
        return synx;
    }

    public void setSynx(String synx) {
        this.synx = synx == null ? null : synx.trim();
    }

    public String getFxsyts() {
        return fxsyts;
    }

    public void setFxsyts(String fxsyts) {
        this.fxsyts = fxsyts == null ? null : fxsyts.trim();
    }

    public String getCdjr() {
        return cdjr;
    }

    public void setCdjr(String cdjr) {
        this.cdjr = cdjr == null ? null : cdjr.trim();
    }

    public String getNx() {
        return nx;
    }

    public void setNx(String nx) {
        this.nx = nx == null ? null : nx.trim();
    }

    public String getFxrq() {
        return fxrq;
    }

    public void setFxrq(String fxrq) {
        this.fxrq = fxrq == null ? null : fxrq.trim();
    }

    public String getFxl() {
        return fxl;
    }

    public void setFxl(String fxl) {
        this.fxl = fxl == null ? null : fxl.trim();
    }

    public String getFxpl() {
        return fxpl;
    }

    public void setFxpl(String fxpl) {
        this.fxpl = fxpl == null ? null : fxpl.trim();
    }

    public String getFdlx() {
        return fdlx;
    }

    public void setFdlx(String fdlx) {
        this.fdlx = fdlx == null ? null : fdlx.trim();
    }

    public String getJzll() {
        return jzll;
    }

    public void setJzll(String jzll) {
        this.jzll = jzll == null ? null : jzll.trim();
    }

    public String getFdjd() {
        return fdjd;
    }

    public void setFdjd(String fdjd) {
        this.fdjd = fdjd == null ? null : fdjd.trim();
    }

    public String getLc() {
        return lc;
    }

    public void setLc(String lc) {
        this.lc = lc == null ? null : lc.trim();
    }

    public String getHqz() {
        return hqz;
    }

    public void setHqz(String hqz) {
        this.hqz = hqz == null ? null : hqz.trim();
    }

    public String getIfname() {
        return ifname;
    }

    public void setIfname(String ifname) {
        this.ifname = ifname == null ? null : ifname.trim();
    }

    public String getPtz() {
        return ptz;
    }

    public void setPtz(String ptz) {
        this.ptz = ptz == null ? null : ptz.trim();
    }

    public String getDb() {
        return db;
    }

    public void setDb(String db) {
        this.db = db == null ? null : db.trim();
    }

    public String getDbfs() {
        return dbfs;
    }

    public void setDbfs(String dbfs) {
        this.dbfs = dbfs == null ? null : dbfs.trim();
    }

    public String getDbname() {
        return dbname;
    }

    public void setDbname(String dbname) {
        this.dbname = dbname == null ? null : dbname.trim();
    }

    public String getRbamount() {
        return rbamount;
    }

    public void setRbamount(String rbamount) {
        this.rbamount = rbamount == null ? null : rbamount.trim();
    }

    public String getRpamount() {
        return rpamount;
    }

    public void setRpamount(String rpamount) {
        this.rpamount = rpamount == null ? null : rpamount.trim();
    }

    public String getRdamount() {
        return rdamount;
    }

    public void setRdamount(String rdamount) {
        this.rdamount = rdamount == null ? null : rdamount.trim();
    }

    public String getSbamount() {
        return sbamount;
    }

    public void setSbamount(String sbamount) {
        this.sbamount = sbamount == null ? null : sbamount.trim();
    }

    public String getSlamount() {
        return slamount;
    }

    public void setSlamount(String slamount) {
        this.slamount = slamount == null ? null : slamount.trim();
    }

    public String getCbamount() {
        return cbamount;
    }

    public void setCbamount(String cbamount) {
        this.cbamount = cbamount == null ? null : cbamount.trim();
    }

    public String getZqamount() {
        return zqamount;
    }

    public void setZqamount(String zqamount) {
        this.zqamount = zqamount == null ? null : zqamount.trim();
    }

    public String getYzamount() {
        return yzamount;
    }

    public void setYzamount(String yzamount) {
        this.yzamount = yzamount == null ? null : yzamount.trim();
    }

    public String getMe() {
        return me;
    }

    public void setMe(String me) {
        this.me = me == null ? null : me.trim();
    }

    public String getYuqi() {
        return yuqi;
    }

    public void setYuqi(String yuqi) {
        this.yuqi = yuqi == null ? null : yuqi.trim();
    }

    public String getBz() {
        return bz;
    }

    public void setBz(String bz) {
        this.bz = bz == null ? null : bz.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", invtype=").append(invtype);
        sb.append(", sd=").append(sd);
        sb.append(", issuer=").append(issuer);
        sb.append(", cost=").append(cost);
        sb.append(", secid=").append(secid);
        sb.append(", port=").append(port);
        sb.append(", jiuqi=").append(jiuqi);
        sb.append(", pvbpx=").append(pvbpx);
        sb.append(", pv01x=").append(pv01x);
        sb.append(", varx=").append(varx);
        sb.append(", fxzl=").append(fxzl);
        sb.append(", sshy=").append(sshy);
        sb.append(", ztlevel=").append(ztlevel);
        sb.append(", zxlevel=").append(zxlevel);
        sb.append(", fxztlevel=").append(fxztlevel);
        sb.append(", fxzxlevel=").append(fxzxlevel);
        sb.append(", sname=").append(sname);
        sb.append(", vdate=").append(vdate);
        sb.append(", tenor=").append(tenor);
        sb.append(", mdate=").append(mdate);
        sb.append(", prinamt=").append(prinamt);
        sb.append(", couprate=").append(couprate);
        sb.append(", acctngtype=").append(acctngtype);
        sb.append(", cccb=").append(cccb);
        sb.append(", yslx=").append(yslx);
        sb.append(", zmjz=").append(zmjz);
        sb.append(", yjlx=").append(yjlx);
        sb.append(", dnljlxsr=").append(dnljlxsr);
        sb.append(", syljlxsr=").append(syljlxsr);
        sb.append(", tdymtm=").append(tdymtm);
        sb.append(", tjpmtm=").append(tjpmtm);
        sb.append(", avgcost=").append(avgcost);
        sb.append(", settavgcost=").append(settavgcost);
        sb.append(", gz=").append(gz);
        sb.append(", opicsjjsz=").append(opicsjjsz);
        sb.append(", jjsz=").append(jjsz);
        sb.append(", yzj=").append(yzj);
        sb.append(", windyzj=").append(windyzj);
        sb.append(", lxtz=").append(lxtz);
        sb.append(", syl=").append(syl);
        sb.append(", jqcbper=").append(jqcbper);
        sb.append(", jjcbper=").append(jjcbper);
        sb.append(", yslxper=").append(yslxper);
        sb.append(", yjlxper=").append(yjlxper);
        sb.append(", tyjj=").append(tyjj);
        sb.append(", jjfy=").append(jjfy);
        sb.append(", windjjfy=").append(windjjfy);
        sb.append(", synx=").append(synx);
        sb.append(", fxsyts=").append(fxsyts);
        sb.append(", cdjr=").append(cdjr);
        sb.append(", nx=").append(nx);
        sb.append(", fxrq=").append(fxrq);
        sb.append(", fxl=").append(fxl);
        sb.append(", fxpl=").append(fxpl);
        sb.append(", fdlx=").append(fdlx);
        sb.append(", jzll=").append(jzll);
        sb.append(", fdjd=").append(fdjd);
        sb.append(", lc=").append(lc);
        sb.append(", hqz=").append(hqz);
        sb.append(", ifname=").append(ifname);
        sb.append(", ptz=").append(ptz);
        sb.append(", db=").append(db);
        sb.append(", dbfs=").append(dbfs);
        sb.append(", dbname=").append(dbname);
        sb.append(", rbamount=").append(rbamount);
        sb.append(", rpamount=").append(rpamount);
        sb.append(", rdamount=").append(rdamount);
        sb.append(", sbamount=").append(sbamount);
        sb.append(", slamount=").append(slamount);
        sb.append(", cbamount=").append(cbamount);
        sb.append(", zqamount=").append(zqamount);
        sb.append(", yzamount=").append(yzamount);
        sb.append(", me=").append(me);
        sb.append(", yuqi=").append(yuqi);
        sb.append(", bz=").append(bz);
        sb.append("]");
        return sb.toString();
    }
}