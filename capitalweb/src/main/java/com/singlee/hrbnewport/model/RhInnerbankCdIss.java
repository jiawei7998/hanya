package com.singlee.hrbnewport.model;

import java.math.BigDecimal;

/**
 *
 * 2021/12/9
 * @Auther:zk
 */    
    
/**
    * 同业业务管理报告-同业存单发行投资业务明细表
    */
public class RhInnerbankCdIss {
    /**
    * 序号
    */
    private String seq;

    /**
    * 存单代码
    */
    private String secid;

    /**
    * 交易对手
    */
    private String custName;

    /**
    * 起息日
    */
    private String vdate;

    /**
    * 到期日
    */
    private String mdate;

    /**
    * 到期收益率
    */
    private BigDecimal field;

    /**
    * 交易金额
    */
    private BigDecimal dealAmt;

    public String getSeq() {
        return seq;
    }

    public void setSeq(String seq) {
        this.seq = seq;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getVdate() {
        return vdate;
    }

    public void setVdate(String vdate) {
        this.vdate = vdate;
    }

    public String getMdate() {
        return mdate;
    }

    public void setMdate(String mdate) {
        this.mdate = mdate;
    }

    public BigDecimal getField() {
        return field;
    }

    public void setField(BigDecimal field) {
        this.field = field;
    }

    public BigDecimal getDealAmt() {
        return dealAmt;
    }

    public void setDealAmt(BigDecimal dealAmt) {
        this.dealAmt = dealAmt;
    }

    public String getSecid() {
        return secid;
    }

    public void setSecid(String secid) {
        this.secid = secid;
    }
}