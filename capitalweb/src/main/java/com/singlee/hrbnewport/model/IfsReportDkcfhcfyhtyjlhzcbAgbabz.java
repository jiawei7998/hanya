package com.singlee.hrbnewport.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

/**
 * 贷款拆放含拆放银行同业及联行资产表 按国别按币种
 */
@Table(name = "IFS_REPORT_DKCFHCFYHTYJLHZCB_AGBABZ")
public class IfsReportDkcfhcfyhtyjlhzcbAgbabz implements Serializable {
    /**
     * 国别
     */
    @Column(name = "GB")
    private String gb;

    /**
     * 币种
     */
    @Column(name = "BIZ")
    private String biz;

    /**
     * 报告期
     */
    @Column(name = "BGQ")
    private String bgq;

    /**
     * 上月末本金余额
     */
    @Column(name = "SYMBJYE")
    private BigDecimal symbjye;

    /**
     * 上月末应收利息余额
     */
    @Column(name = "SYMYSLXYE")
    private BigDecimal symyslxye;

    /**
     * 本月末本金余额
     */
    @Column(name = "BYMBJYE")
    private BigDecimal bymbjye;

    /**
     * 本月末本金余额:其中剩余期限在一年及以下
     */
    @Column(name = "BYMBJYEQXYNYX")
    private BigDecimal bymbjyeqxynyx;

    /**
     * 本月末应收利息余额
     */
    @Column(name = "BYMYSLXYE")
    private BigDecimal bymyslxye;

    /**
     * 本月非交易变动
     */
    @Column(name = "BYFJYBD")
    private BigDecimal byfjybd;

    /**
     * 本月净发生额
     */
    @Column(name = "BYJFSE")
    private BigDecimal byjfse;

    /**
     * 本月利息收入
     */
    @Column(name = "BYLXSR")
    private BigDecimal bylxsr;

    /**
     * 备注
     */
    @Column(name = "BEIZ")
    private String beiz;

    private static final long serialVersionUID = 1L;

    /**
     * 获取国别
     *
     * @return GB - 国别
     */
    public String getGb() {
        return gb;
    }

    /**
     * 设置国别
     *
     * @param gb 国别
     */
    public void setGb(String gb) {
        this.gb = gb;
    }

    /**
     * 获取币种
     *
     * @return BIZ - 币种
     */
    public String getBiz() {
        return biz;
    }

    /**
     * 设置币种
     *
     * @param biz 币种
     */
    public void setBiz(String biz) {
        this.biz = biz;
    }

    /**
     * 获取报告期
     *
     * @return BGQ - 报告期
     */
    public String getBgq() {
        return bgq;
    }

    /**
     * 设置报告期
     *
     * @param bgq 报告期
     */
    public void setBgq(String bgq) {
        this.bgq = bgq;
    }

    /**
     * 获取上月末本金余额
     *
     * @return SYMBJYE - 上月末本金余额
     */
    public BigDecimal getSymbjye() {
        return symbjye;
    }

    /**
     * 设置上月末本金余额
     *
     * @param symbjye 上月末本金余额
     */
    public void setSymbjye(BigDecimal symbjye) {
        this.symbjye = symbjye;
    }

    /**
     * 获取上月末应收利息余额
     *
     * @return SYMYSLXYE - 上月末应收利息余额
     */
    public BigDecimal getSymyslxye() {
        return symyslxye;
    }

    /**
     * 设置上月末应收利息余额
     *
     * @param symyslxye 上月末应收利息余额
     */
    public void setSymyslxye(BigDecimal symyslxye) {
        this.symyslxye = symyslxye;
    }

    /**
     * 获取本月末本金余额
     *
     * @return BYMBJYE - 本月末本金余额
     */
    public BigDecimal getBymbjye() {
        return bymbjye;
    }

    /**
     * 设置本月末本金余额
     *
     * @param bymbjye 本月末本金余额
     */
    public void setBymbjye(BigDecimal bymbjye) {
        this.bymbjye = bymbjye;
    }

    /**
     * 获取本月末本金余额:其中剩余期限在一年及以下
     *
     * @return BYMBJYEQXYNYX - 本月末本金余额:其中剩余期限在一年及以下
     */
    public BigDecimal getBymbjyeqxynyx() {
        return bymbjyeqxynyx;
    }

    /**
     * 设置本月末本金余额:其中剩余期限在一年及以下
     *
     * @param bymbjyeqxynyx 本月末本金余额:其中剩余期限在一年及以下
     */
    public void setBymbjyeqxynyx(BigDecimal bymbjyeqxynyx) {
        this.bymbjyeqxynyx = bymbjyeqxynyx;
    }

    /**
     * 获取本月末应收利息余额
     *
     * @return BYMYSLXYE - 本月末应收利息余额
     */
    public BigDecimal getBymyslxye() {
        return bymyslxye;
    }

    /**
     * 设置本月末应收利息余额
     *
     * @param bymyslxye 本月末应收利息余额
     */
    public void setBymyslxye(BigDecimal bymyslxye) {
        this.bymyslxye = bymyslxye;
    }

    /**
     * 获取本月非交易变动
     *
     * @return BYFJYBD - 本月非交易变动
     */
    public BigDecimal getByfjybd() {
        return byfjybd;
    }

    /**
     * 设置本月非交易变动
     *
     * @param byfjybd 本月非交易变动
     */
    public void setByfjybd(BigDecimal byfjybd) {
        this.byfjybd = byfjybd;
    }

    /**
     * 获取本月净发生额
     *
     * @return BYJFSE - 本月净发生额
     */
    public BigDecimal getByjfse() {
        return byjfse;
    }

    /**
     * 设置本月净发生额
     *
     * @param byjfse 本月净发生额
     */
    public void setByjfse(BigDecimal byjfse) {
        this.byjfse = byjfse;
    }

    /**
     * 获取本月利息收入
     *
     * @return BYLXSR - 本月利息收入
     */
    public BigDecimal getBylxsr() {
        return bylxsr;
    }

    /**
     * 设置本月利息收入
     *
     * @param bylxsr 本月利息收入
     */
    public void setBylxsr(BigDecimal bylxsr) {
        this.bylxsr = bylxsr;
    }

    /**
     * 获取备注
     *
     * @return BEIZ - 备注
     */
    public String getBeiz() {
        return beiz;
    }

    /**
     * 设置备注
     *
     * @param beiz 备注
     */
    public void setBeiz(String beiz) {
        this.beiz = beiz;
    }
}