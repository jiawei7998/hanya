package com.singlee.hrbnewport.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

/**
 * 同业客户-基本信息表
 */
@Table(name = "IFS_REPORT_TYKHJBXX")
public class IfsReportTykhjbxx implements Serializable {
    /**
     * 客户名称
     */
    @Column(name = "KHMC")
    private String khmc;

    /**
     * 客户代码
     */
    @Column(name = "KHDM")
    private String khdm;

    /**
     * 客户类别
     */
    @Column(name = "KHLB")
    private String khlb;

    /**
     * 统一信用社会代码/组织机构代码证代码
     */
    @Column(name = "SHDM")
    private String shdm;

    /**
     * 国别及地区
     */
    @Column(name = "GBJDQ")
    private String gbjdq;

    /**
     * 内部评级
     */
    @Column(name = "NBPJ")
    private String nbpj;

    /**
     * 外部评级
     */
    @Column(name = "WBPJ")
    private String wbpj;

    /**
     * 拆放同业（元）
     */
    @Column(name = "CHAFTY")
    private BigDecimal chafty;

    /**
     * 存放同业（元）
     */
    @Column(name = "CUNFTY")
    private BigDecimal cunfty;

    /**
     * 债券回购（元）-买入返售
     */
    @Column(name = "ZQHGMRFS")
    private BigDecimal zqhgmrfs;

    /**
     * 股票质押贷款（元）
     */
    @Column(name = "GPZYDK")
    private BigDecimal gpzydk;

    /**
     * 买入返售资产（元）
     */
    @Column(name = "MRFSZC")
    private BigDecimal mrfszc;

    /**
     * 买断式转贴现（元）
     */
    @Column(name = "MDSZTX")
    private BigDecimal mdsztx;

    /**
     * 持有债券（元）
     */
    @Column(name = "CYZQ")
    private BigDecimal cyzq;

    /**
     * 股权投资（元）
     */
    @Column(name = "GQTZ")
    private BigDecimal gqtz;

    /**
     * 同业代付（元）
     */
    @Column(name = "TYDF")
    private BigDecimal tydf;

    /**
     * 卖出回购资产（元）
     */
    @Column(name = "MCHGZC")
    private BigDecimal mchgzc;

    /**
     * 其他表内业务-存单投资
     */
    @Column(name = "QTBNYWCDTZ")
    private BigDecimal qtbnywcdtz;

    private static final long serialVersionUID = 1L;

    /**
     * 获取客户名称
     *
     * @return KHMC - 客户名称
     */
    public String getKhmc() {
        return khmc;
    }

    /**
     * 设置客户名称
     *
     * @param khmc 客户名称
     */
    public void setKhmc(String khmc) {
        this.khmc = khmc;
    }

    /**
     * 获取客户代码
     *
     * @return KHDM - 客户代码
     */
    public String getKhdm() {
        return khdm;
    }

    /**
     * 设置客户代码
     *
     * @param khdm 客户代码
     */
    public void setKhdm(String khdm) {
        this.khdm = khdm;
    }

    /**
     * 获取客户类别
     *
     * @return KHLB - 客户类别
     */
    public String getKhlb() {
        return khlb;
    }

    /**
     * 设置客户类别
     *
     * @param khlb 客户类别
     */
    public void setKhlb(String khlb) {
        this.khlb = khlb;
    }

    /**
     * 获取统一信用社会代码/组织机构代码证代码
     *
     * @return SHDM - 统一信用社会代码/组织机构代码证代码
     */
    public String getShdm() {
        return shdm;
    }

    /**
     * 设置统一信用社会代码/组织机构代码证代码
     *
     * @param shdm 统一信用社会代码/组织机构代码证代码
     */
    public void setShdm(String shdm) {
        this.shdm = shdm;
    }

    /**
     * 获取国别及地区
     *
     * @return GBJDQ - 国别及地区
     */
    public String getGbjdq() {
        return gbjdq;
    }

    /**
     * 设置国别及地区
     *
     * @param gbjdq 国别及地区
     */
    public void setGbjdq(String gbjdq) {
        this.gbjdq = gbjdq;
    }

    /**
     * 获取内部评级
     *
     * @return NBPJ - 内部评级
     */
    public String getNbpj() {
        return nbpj;
    }

    /**
     * 设置内部评级
     *
     * @param nbpj 内部评级
     */
    public void setNbpj(String nbpj) {
        this.nbpj = nbpj;
    }

    /**
     * 获取外部评级
     *
     * @return WBPJ - 外部评级
     */
    public String getWbpj() {
        return wbpj;
    }

    /**
     * 设置外部评级
     *
     * @param wbpj 外部评级
     */
    public void setWbpj(String wbpj) {
        this.wbpj = wbpj;
    }

    /**
     * 获取拆放同业（元）
     *
     * @return CHAFTY - 拆放同业（元）
     */
    public BigDecimal getChafty() {
        return chafty;
    }

    /**
     * 设置拆放同业（元）
     *
     * @param chafty 拆放同业（元）
     */
    public void setChafty(BigDecimal chafty) {
        this.chafty = chafty;
    }

    /**
     * 获取存放同业（元）
     *
     * @return CUNFTY - 存放同业（元）
     */
    public BigDecimal getCunfty() {
        return cunfty;
    }

    /**
     * 设置存放同业（元）
     *
     * @param cunfty 存放同业（元）
     */
    public void setCunfty(BigDecimal cunfty) {
        this.cunfty = cunfty;
    }

    /**
     * 获取债券回购（元）-买入返售
     *
     * @return ZQHGMRFS - 债券回购（元）-买入返售
     */
    public BigDecimal getZqhgmrfs() {
        return zqhgmrfs;
    }

    /**
     * 设置债券回购（元）-买入返售
     *
     * @param zqhgmrfs 债券回购（元）-买入返售
     */
    public void setZqhgmrfs(BigDecimal zqhgmrfs) {
        this.zqhgmrfs = zqhgmrfs;
    }

    /**
     * 获取股票质押贷款（元）
     *
     * @return GPZYDK - 股票质押贷款（元）
     */
    public BigDecimal getGpzydk() {
        return gpzydk;
    }

    /**
     * 设置股票质押贷款（元）
     *
     * @param gpzydk 股票质押贷款（元）
     */
    public void setGpzydk(BigDecimal gpzydk) {
        this.gpzydk = gpzydk;
    }

    /**
     * 获取买入返售资产（元）
     *
     * @return MRFSZC - 买入返售资产（元）
     */
    public BigDecimal getMrfszc() {
        return mrfszc;
    }

    /**
     * 设置买入返售资产（元）
     *
     * @param mrfszc 买入返售资产（元）
     */
    public void setMrfszc(BigDecimal mrfszc) {
        this.mrfszc = mrfszc;
    }

    /**
     * 获取买断式转贴现（元）
     *
     * @return MDSZTX - 买断式转贴现（元）
     */
    public BigDecimal getMdsztx() {
        return mdsztx;
    }

    /**
     * 设置买断式转贴现（元）
     *
     * @param mdsztx 买断式转贴现（元）
     */
    public void setMdsztx(BigDecimal mdsztx) {
        this.mdsztx = mdsztx;
    }

    /**
     * 获取持有债券（元）
     *
     * @return CYZQ - 持有债券（元）
     */
    public BigDecimal getCyzq() {
        return cyzq;
    }

    /**
     * 设置持有债券（元）
     *
     * @param cyzq 持有债券（元）
     */
    public void setCyzq(BigDecimal cyzq) {
        this.cyzq = cyzq;
    }

    /**
     * 获取股权投资（元）
     *
     * @return GQTZ - 股权投资（元）
     */
    public BigDecimal getGqtz() {
        return gqtz;
    }

    /**
     * 设置股权投资（元）
     *
     * @param gqtz 股权投资（元）
     */
    public void setGqtz(BigDecimal gqtz) {
        this.gqtz = gqtz;
    }

    /**
     * 获取同业代付（元）
     *
     * @return TYDF - 同业代付（元）
     */
    public BigDecimal getTydf() {
        return tydf;
    }

    /**
     * 设置同业代付（元）
     *
     * @param tydf 同业代付（元）
     */
    public void setTydf(BigDecimal tydf) {
        this.tydf = tydf;
    }

    /**
     * 获取卖出回购资产（元）
     *
     * @return MCHGZC - 卖出回购资产（元）
     */
    public BigDecimal getMchgzc() {
        return mchgzc;
    }

    /**
     * 设置卖出回购资产（元）
     *
     * @param mchgzc 卖出回购资产（元）
     */
    public void setMchgzc(BigDecimal mchgzc) {
        this.mchgzc = mchgzc;
    }

    /**
     * 获取其他表内业务-存单投资
     *
     * @return QTBNYWCDTZ - 其他表内业务-存单投资
     */
    public BigDecimal getQtbnywcdtz() {
        return qtbnywcdtz;
    }

    /**
     * 设置其他表内业务-存单投资
     *
     * @param qtbnywcdtz 其他表内业务-存单投资
     */
    public void setQtbnywcdtz(BigDecimal qtbnywcdtz) {
        this.qtbnywcdtz = qtbnywcdtz;
    }
}