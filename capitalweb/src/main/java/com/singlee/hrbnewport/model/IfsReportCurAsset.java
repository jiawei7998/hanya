package com.singlee.hrbnewport.model;

import java.math.BigDecimal;

/**
 *
 * 2021/12/9
 * @Auther:zk
 */    
    
/**
    * 哈尔滨银行优质流动性资产台账
    */
public class IfsReportCurAsset {
    /**
    * 序号
    */
    private String seq;

    /**
    * 债券代码
    */
    private String secid;

    /**
    * 债券名称
    */
    private String descr;

    /**
    * 债券种类
    */
    private String secType;

    /**
    * LCR资产等级
    */
    private String assetType;

    /**
    * 面值(万元)
    */
    private BigDecimal prinamt;

    /**
    * 票面利率(%)
    */
    private BigDecimal rate;

    /**
    * 主题评级
    */
    private String lev;

    /**
    * 债项评级
    */
    private String crating;

    /**
    * 发行日
    */
    private String vdate;

    /**
    * 到期日
    */
    private String mdate;

    /**
    * 剩余期限(年)
    */
    private String exerviseyears;

    /**
    * 质押状态
    */
    private String pledgeState;

    /**
    * 未质押账面价值(万元)(可计入LCR金额)
    */
    private BigDecimal unpleAmt;

    /**
    * 已质押账面价值(万元)
    */
    private BigDecimal pleAmt;

    /**
    * 已质押剩余期限(年)
    */
    private String pleExeYears;

    /**
    * 质押到期日
    */
    private String pldMdate;

    /**
    * 人行借款 质押率(%)
    */
    private String pleRate;

    /**
    * 评估价值(万元)
    */
    private BigDecimal val;

    /**
    * 资产类型  国债/政金债/地方政府债/短期融资券/中期票据
    */
    private String secAssetType;

    public String getSeq() {
        return seq;
    }

    public void setSeq(String seq) {
        this.seq = seq;
    }

    public String getSecid() {
        return secid;
    }

    public void setSecid(String secid) {
        this.secid = secid;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public String getSecType() {
        return secType;
    }

    public void setSecType(String secType) {
        this.secType = secType;
    }

    public String getAssetType() {
        return assetType;
    }

    public void setAssetType(String assetType) {
        this.assetType = assetType;
    }

    public BigDecimal getPrinamt() {
        return prinamt;
    }

    public void setPrinamt(BigDecimal prinamt) {
        this.prinamt = prinamt;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public String getLev() {
        return lev;
    }

    public void setLev(String lev) {
        this.lev = lev;
    }

    public String getCrating() {
        return crating;
    }

    public void setCrating(String crating) {
        this.crating = crating;
    }

    public String getVdate() {
        return vdate;
    }

    public void setVdate(String vdate) {
        this.vdate = vdate;
    }

    public String getMdate() {
        return mdate;
    }

    public void setMdate(String mdate) {
        this.mdate = mdate;
    }

    public String getExerviseyears() {
        return exerviseyears;
    }

    public void setExerviseyears(String exerviseyears) {
        this.exerviseyears = exerviseyears;
    }

    public String getPledgeState() {
        return pledgeState;
    }

    public void setPledgeState(String pledgeState) {
        this.pledgeState = pledgeState;
    }

    public BigDecimal getUnpleAmt() {
        return unpleAmt;
    }

    public void setUnpleAmt(BigDecimal unpleAmt) {
        this.unpleAmt = unpleAmt;
    }

    public BigDecimal getPleAmt() {
        return pleAmt;
    }

    public void setPleAmt(BigDecimal pleAmt) {
        this.pleAmt = pleAmt;
    }

    public String getPleExeYears() {
        return pleExeYears;
    }

    public void setPleExeYears(String pleExeYears) {
        this.pleExeYears = pleExeYears;
    }

    public String getPldMdate() {
        return pldMdate;
    }

    public void setPldMdate(String pldMdate) {
        this.pldMdate = pldMdate;
    }

    public String getPleRate() {
        return pleRate;
    }

    public void setPleRate(String pleRate) {
        this.pleRate = pleRate;
    }

    public BigDecimal getVal() {
        return val;
    }

    public void setVal(BigDecimal val) {
        this.val = val;
    }

    public String getSecAssetType() {
        return secAssetType;
    }

    public void setSecAssetType(String secAssetType) {
        this.secAssetType = secAssetType;
    }
}