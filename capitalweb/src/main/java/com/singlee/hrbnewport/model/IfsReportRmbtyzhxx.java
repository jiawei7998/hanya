package com.singlee.hrbnewport.model;

import java.io.Serializable;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 20:25
 * @description：${description}
 * @modified By：
 * @version:   
 */
public class IfsReportRmbtyzhxx implements Serializable {
    /**
    * 银行机构代码
    */
    private String yhjgdm;

    /**
    * 账户类型
    */
    private String zhlx;

    /**
    * 操作日期
    */
    private String czrq;

    /**
    * 取现标识
    */
    private String qxbs;

    /**
    * 国民经济部门分类
    */
    private String gmjjbmfl;

    /**
    * 银行机构名称
    */
    private String yhjgmc;

    /**
    * 境外参加行SWIFT BIC
    */
    private String jwcjh;

    /**
    * 联系人名称
    */
    private String lxrmc;

    /**
    * 证明文件种类
    */
    private String zmwjzl;

    /**
    * 境外参加行邮编
    */
    private String jwcjhyb;

    /**
    * 金融许可证编号
    */
    private String jrxkzbh;

    /**
    * 银行SWIFT BIC
    */
    private String bic;

    /**
    * 联系人名称
    */
    private String lxrmc2;

    /**
    * 联系电话
    */
    private String phone;

    /**
    * 电子邮件地址
    */
    private String yx;

    private static final long serialVersionUID = 1L;

    public String getYhjgdm() {
        return yhjgdm;
    }

    public void setYhjgdm(String yhjgdm) {
        this.yhjgdm = yhjgdm;
    }

    public String getZhlx() {
        return zhlx;
    }

    public void setZhlx(String zhlx) {
        this.zhlx = zhlx;
    }

    public String getCzrq() {
        return czrq;
    }

    public void setCzrq(String czrq) {
        this.czrq = czrq;
    }

    public String getQxbs() {
        return qxbs;
    }

    public void setQxbs(String qxbs) {
        this.qxbs = qxbs;
    }

    public String getGmjjbmfl() {
        return gmjjbmfl;
    }

    public void setGmjjbmfl(String gmjjbmfl) {
        this.gmjjbmfl = gmjjbmfl;
    }

    public String getYhjgmc() {
        return yhjgmc;
    }

    public void setYhjgmc(String yhjgmc) {
        this.yhjgmc = yhjgmc;
    }

    public String getJwcjh() {
        return jwcjh;
    }

    public void setJwcjh(String jwcjh) {
        this.jwcjh = jwcjh;
    }

    public String getLxrmc() {
        return lxrmc;
    }

    public void setLxrmc(String lxrmc) {
        this.lxrmc = lxrmc;
    }

    public String getZmwjzl() {
        return zmwjzl;
    }

    public void setZmwjzl(String zmwjzl) {
        this.zmwjzl = zmwjzl;
    }

    public String getJwcjhyb() {
        return jwcjhyb;
    }

    public void setJwcjhyb(String jwcjhyb) {
        this.jwcjhyb = jwcjhyb;
    }

    public String getJrxkzbh() {
        return jrxkzbh;
    }

    public void setJrxkzbh(String jrxkzbh) {
        this.jrxkzbh = jrxkzbh;
    }

    public String getBic() {
        return bic;
    }

    public void setBic(String bic) {
        this.bic = bic;
    }

    public String getLxrmc2() {
        return lxrmc2;
    }

    public void setLxrmc2(String lxrmc2) {
        this.lxrmc2 = lxrmc2;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getYx() {
        return yx;
    }

    public void setYx(String yx) {
        this.yx = yx;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", yhjgdm=").append(yhjgdm);
        sb.append(", zhlx=").append(zhlx);
        sb.append(", czrq=").append(czrq);
        sb.append(", qxbs=").append(qxbs);
        sb.append(", gmjjbmfl=").append(gmjjbmfl);
        sb.append(", yhjgmc=").append(yhjgmc);
        sb.append(", jwcjh=").append(jwcjh);
        sb.append(", lxrmc=").append(lxrmc);
        sb.append(", zmwjzl=").append(zmwjzl);
        sb.append(", jwcjhyb=").append(jwcjhyb);
        sb.append(", jrxkzbh=").append(jrxkzbh);
        sb.append(", bic=").append(bic);
        sb.append(", lxrmc2=").append(lxrmc2);
        sb.append(", phone=").append(phone);
        sb.append(", yx=").append(yx);
        sb.append("]");
        return sb.toString();
    }
}