package com.singlee.hrbnewport.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author ：meigy
 * @date ：Created in 2021/11/9 13:19
 * @description：${description}
 * @modified By：
 * @version:
 */

/**
 * 表一 表内外投资业务总体情况表（穿透前）
 */
@Entity
@Table(name = "IFS_REPORT_CSHBNWTZ_ZTCTQ")
public class IfsReportCshbnwtzZtctq implements Serializable {
    /**
     * 资金来源
     */
    private String zjtype;

    /**
     * 投资类型
     */
    private String tztype;

    /**
     * 本季末数额
     */
    private BigDecimal bjmAmt;

    /**
     * 上季末数额
     */
    private BigDecimal sjmAmt;

    /**
     * 备注
     */
    private String bz;

    /**
     * 投资类型 父级
     */
    private String parentTztype;

    @Transient
    private String tztypeo;

    private static final long serialVersionUID = 1L;

    public String getZjtype() {
        return zjtype;
    }

    public void setZjtype(String zjtype) {
        this.zjtype = zjtype;
    }

    public String getTztype() {
        return tztype;
    }

    public void setTztype(String tztype) {
        this.tztype = tztype;
    }

    public BigDecimal getBjmAmt() {
        return bjmAmt;
    }

    public void setBjmAmt(BigDecimal bjmAmt) {
        this.bjmAmt = bjmAmt;
    }

    public BigDecimal getSjmAmt() {
        return sjmAmt;
    }

    public void setSjmAmt(BigDecimal sjmAmt) {
        this.sjmAmt = sjmAmt;
    }

    public String getBz() {
        return bz;
    }

    public void setBz(String bz) {
        this.bz = bz;
    }

    public String getParentTztype() {
        return parentTztype;
    }

    public void setParentTztype(String parentTztype) {
        this.parentTztype = parentTztype;
    }

    public String getTztypeo() {
        return tztypeo;
    }

    public void setTztypeo(String tztypeo) {
        this.tztypeo = tztypeo;
    }
}