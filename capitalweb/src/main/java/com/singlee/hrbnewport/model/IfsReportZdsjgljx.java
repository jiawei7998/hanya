package com.singlee.hrbnewport.model;

import java.io.Serializable;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 20:52
 * @description：${description}
 * @modified By：
 * @version:   
 */
public class IfsReportZdsjgljx implements Serializable {
    /**
    * 排序
    */
    private String ser;

    /**
    * 关联方名称
    */
    private String glfmc;

    /**
    * 客户代码
    */
    private String ghdm;

    /**
    * 关联方类型
    */
    private String glflx;

    /**
    * 持股
比例(%)
    */
    private String cgbl;

    /**
    * 净额
    */
    private String je;

    /**
    * 占资本净额
比例
    */
    private String zzbjebl;

    /**
    * 各项贷款
    */
    private String gxdk;

    /**
    * 债券投资
    */
    private String zqtz;

    /**
    * 特定目的载体投资
    */
    private String tdmdzt;

    /**
    * 其他表内授信
    */
    private String qtbnsx;

    /**
    * 不可撤销的承诺及或有负债
    */
    private String bkcdfz;

    /**
    * 本行非保本理财产品进行的授信
    */
    private String fbsx;

    /**
    * 其他表外授信
    */
    private String qitabwsx;

    /**
    * 净额
    */
    private String jz;

    /**
    * 保证金、银行存单、国债 
    */
    private String bzj;

    /**
    * 占资本净额
比例
    */
    private String bl;

    private static final long serialVersionUID = 1L;

    public String getSer() {
        return ser;
    }

    public void setSer(String ser) {
        this.ser = ser;
    }

    public String getGlfmc() {
        return glfmc;
    }

    public void setGlfmc(String glfmc) {
        this.glfmc = glfmc;
    }

    public String getGhdm() {
        return ghdm;
    }

    public void setGhdm(String ghdm) {
        this.ghdm = ghdm;
    }

    public String getGlflx() {
        return glflx;
    }

    public void setGlflx(String glflx) {
        this.glflx = glflx;
    }

    public String getCgbl() {
        return cgbl;
    }

    public void setCgbl(String cgbl) {
        this.cgbl = cgbl;
    }

    public String getJe() {
        return je;
    }

    public void setJe(String je) {
        this.je = je;
    }

    public String getZzbjebl() {
        return zzbjebl;
    }

    public void setZzbjebl(String zzbjebl) {
        this.zzbjebl = zzbjebl;
    }

    public String getGxdk() {
        return gxdk;
    }

    public void setGxdk(String gxdk) {
        this.gxdk = gxdk;
    }

    public String getZqtz() {
        return zqtz;
    }

    public void setZqtz(String zqtz) {
        this.zqtz = zqtz;
    }

    public String getTdmdzt() {
        return tdmdzt;
    }

    public void setTdmdzt(String tdmdzt) {
        this.tdmdzt = tdmdzt;
    }

    public String getQtbnsx() {
        return qtbnsx;
    }

    public void setQtbnsx(String qtbnsx) {
        this.qtbnsx = qtbnsx;
    }

    public String getBkcdfz() {
        return bkcdfz;
    }

    public void setBkcdfz(String bkcdfz) {
        this.bkcdfz = bkcdfz;
    }

    public String getFbsx() {
        return fbsx;
    }

    public void setFbsx(String fbsx) {
        this.fbsx = fbsx;
    }

    public String getQitabwsx() {
        return qitabwsx;
    }

    public void setQitabwsx(String qitabwsx) {
        this.qitabwsx = qitabwsx;
    }

    public String getJz() {
        return jz;
    }

    public void setJz(String jz) {
        this.jz = jz;
    }

    public String getBzj() {
        return bzj;
    }

    public void setBzj(String bzj) {
        this.bzj = bzj;
    }

    public String getBl() {
        return bl;
    }

    public void setBl(String bl) {
        this.bl = bl;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", ser=").append(ser);
        sb.append(", glfmc=").append(glfmc);
        sb.append(", ghdm=").append(ghdm);
        sb.append(", glflx=").append(glflx);
        sb.append(", cgbl=").append(cgbl);
        sb.append(", je=").append(je);
        sb.append(", zzbjebl=").append(zzbjebl);
        sb.append(", gxdk=").append(gxdk);
        sb.append(", zqtz=").append(zqtz);
        sb.append(", tdmdzt=").append(tdmdzt);
        sb.append(", qtbnsx=").append(qtbnsx);
        sb.append(", bkcdfz=").append(bkcdfz);
        sb.append(", fbsx=").append(fbsx);
        sb.append(", qitabwsx=").append(qitabwsx);
        sb.append(", jz=").append(jz);
        sb.append(", bzj=").append(bzj);
        sb.append(", bl=").append(bl);
        sb.append("]");
        return sb.toString();
    }
}