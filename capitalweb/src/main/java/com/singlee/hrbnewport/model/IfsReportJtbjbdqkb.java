package com.singlee.hrbnewport.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

/**
 * 计提本金变动情况报表
 */
@Table(name = "IFS_REPORT_JTBJBDQKB")
public class IfsReportJtbjbdqkb implements Serializable {
    /**
     * 项目
     */
    @Column(name = "PRODUCT")
    private String product;

    /**
     * 日期1
     */
    @Column(name = "DATE1")
    private BigDecimal date1;

    /**
     * 日期2
     */
    @Column(name = "DATE2")
    private BigDecimal date2;

    /**
     * 日期3
     */
    @Column(name = "DATE3")
    private BigDecimal date3;

    /**
     * 日期4
     */
    @Column(name = "DATE4")
    private BigDecimal date4;

    /**
     * 日期5
     */
    @Column(name = "DATE5")
    private BigDecimal date5;

    /**
     * 日期6
     */
    @Column(name = "DATE6")
    private BigDecimal date6;

    /**
     * 日期7
     */
    @Column(name = "DATE7")
    private BigDecimal date7;

    /**
     * 日期8
     */
    @Column(name = "DATE8")
    private BigDecimal date8;

    /**
     * 日期9
     */
    @Column(name = "DATE9")
    private BigDecimal date9;

    /**
     * 日期10
     */
    @Column(name = "DATE10")
    private BigDecimal date10;

    /**
     * 日期11
     */
    @Column(name = "DATE11")
    private BigDecimal date11;

    /**
     * 日期12
     */
    @Column(name = "DATE12")
    private BigDecimal date12;

    /**
     * 日期13
     */
    @Column(name = "DATE13")
    private BigDecimal date13;

    /**
     * 日期14
     */
    @Column(name = "DATE14")
    private BigDecimal date14;

    /**
     * 日期15
     */
    @Column(name = "DATE15")
    private BigDecimal date15;

    /**
     * 日期16
     */
    @Column(name = "DATE16")
    private BigDecimal date16;

    /**
     * 日期17
     */
    @Column(name = "DATE17")
    private BigDecimal date17;

    /**
     * 日期18
     */
    @Column(name = "DATE18")
    private BigDecimal date18;

    /**
     * 日期19
     */
    @Column(name = "DATE19")
    private BigDecimal date19;

    /**
     * 日期20
     */
    @Column(name = "DATE20")
    private BigDecimal date20;

    /**
     * 日期21
     */
    @Column(name = "DATE21")
    private BigDecimal date21;

    /**
     * 日期22
     */
    @Column(name = "DATE22")
    private BigDecimal date22;

    /**
     * 日期23
     */
    @Column(name = "DATE23")
    private BigDecimal date23;

    /**
     * 日期24
     */
    @Column(name = "DATE24")
    private BigDecimal date24;

    /**
     * 日期25
     */
    @Column(name = "DATE25")
    private BigDecimal date25;

    /**
     * 日期26
     */
    @Column(name = "DATE26")
    private BigDecimal date26;

    /**
     * 日期27
     */
    @Column(name = "DATE27")
    private BigDecimal date27;

    /**
     * 日期28
     */
    @Column(name = "DATE28")
    private BigDecimal date28;

    /**
     * 日期29
     */
    @Column(name = "DATE29")
    private BigDecimal date29;

    /**
     * 日期30
     */
    @Column(name = "DATE30")
    private BigDecimal date30;

    /**
     * 日期31
     */
    @Column(name = "DATE31")
    private BigDecimal date31;

    /**
     * 合计
     */
    @Column(name = "DATEHJ")
    private BigDecimal datehj;

    /**
     * 平均值
     */
    @Column(name = "DATEPJZ")
    private BigDecimal datepjz;

    private static final long serialVersionUID = 1L;

    /**
     * 获取项目
     *
     * @return PRODUCT - 项目
     */
    public String getProduct() {
        return product;
    }

    /**
     * 设置项目
     *
     * @param product 项目
     */
    public void setProduct(String product) {
        this.product = product;
    }

    /**
     * 获取日期1
     *
     * @return DATE1 - 日期1
     */
    public BigDecimal getDate1() {
        return date1;
    }

    /**
     * 设置日期1
     *
     * @param date1 日期1
     */
    public void setDate1(BigDecimal date1) {
        this.date1 = date1;
    }

    /**
     * 获取日期2
     *
     * @return DATE2 - 日期2
     */
    public BigDecimal getDate2() {
        return date2;
    }

    /**
     * 设置日期2
     *
     * @param date2 日期2
     */
    public void setDate2(BigDecimal date2) {
        this.date2 = date2;
    }

    /**
     * 获取日期3
     *
     * @return DATE3 - 日期3
     */
    public BigDecimal getDate3() {
        return date3;
    }

    /**
     * 设置日期3
     *
     * @param date3 日期3
     */
    public void setDate3(BigDecimal date3) {
        this.date3 = date3;
    }

    /**
     * 获取日期4
     *
     * @return DATE4 - 日期4
     */
    public BigDecimal getDate4() {
        return date4;
    }

    /**
     * 设置日期4
     *
     * @param date4 日期4
     */
    public void setDate4(BigDecimal date4) {
        this.date4 = date4;
    }

    /**
     * 获取日期5
     *
     * @return DATE5 - 日期5
     */
    public BigDecimal getDate5() {
        return date5;
    }

    /**
     * 设置日期5
     *
     * @param date5 日期5
     */
    public void setDate5(BigDecimal date5) {
        this.date5 = date5;
    }

    /**
     * 获取日期6
     *
     * @return DATE6 - 日期6
     */
    public BigDecimal getDate6() {
        return date6;
    }

    /**
     * 设置日期6
     *
     * @param date6 日期6
     */
    public void setDate6(BigDecimal date6) {
        this.date6 = date6;
    }

    /**
     * 获取日期7
     *
     * @return DATE7 - 日期7
     */
    public BigDecimal getDate7() {
        return date7;
    }

    /**
     * 设置日期7
     *
     * @param date7 日期7
     */
    public void setDate7(BigDecimal date7) {
        this.date7 = date7;
    }

    /**
     * 获取日期8
     *
     * @return DATE8 - 日期8
     */
    public BigDecimal getDate8() {
        return date8;
    }

    /**
     * 设置日期8
     *
     * @param date8 日期8
     */
    public void setDate8(BigDecimal date8) {
        this.date8 = date8;
    }

    /**
     * 获取日期9
     *
     * @return DATE9 - 日期9
     */
    public BigDecimal getDate9() {
        return date9;
    }

    /**
     * 设置日期9
     *
     * @param date9 日期9
     */
    public void setDate9(BigDecimal date9) {
        this.date9 = date9;
    }

    /**
     * 获取日期10
     *
     * @return DATE10 - 日期10
     */
    public BigDecimal getDate10() {
        return date10;
    }

    /**
     * 设置日期10
     *
     * @param date10 日期10
     */
    public void setDate10(BigDecimal date10) {
        this.date10 = date10;
    }

    /**
     * 获取日期11
     *
     * @return DATE11 - 日期11
     */
    public BigDecimal getDate11() {
        return date11;
    }

    /**
     * 设置日期11
     *
     * @param date11 日期11
     */
    public void setDate11(BigDecimal date11) {
        this.date11 = date11;
    }

    /**
     * 获取日期12
     *
     * @return DATE12 - 日期12
     */
    public BigDecimal getDate12() {
        return date12;
    }

    /**
     * 设置日期12
     *
     * @param date12 日期12
     */
    public void setDate12(BigDecimal date12) {
        this.date12 = date12;
    }

    /**
     * 获取日期13
     *
     * @return DATE13 - 日期13
     */
    public BigDecimal getDate13() {
        return date13;
    }

    /**
     * 设置日期13
     *
     * @param date13 日期13
     */
    public void setDate13(BigDecimal date13) {
        this.date13 = date13;
    }

    /**
     * 获取日期14
     *
     * @return DATE14 - 日期14
     */
    public BigDecimal getDate14() {
        return date14;
    }

    /**
     * 设置日期14
     *
     * @param date14 日期14
     */
    public void setDate14(BigDecimal date14) {
        this.date14 = date14;
    }

    /**
     * 获取日期15
     *
     * @return DATE15 - 日期15
     */
    public BigDecimal getDate15() {
        return date15;
    }

    /**
     * 设置日期15
     *
     * @param date15 日期15
     */
    public void setDate15(BigDecimal date15) {
        this.date15 = date15;
    }

    /**
     * 获取日期16
     *
     * @return DATE16 - 日期16
     */
    public BigDecimal getDate16() {
        return date16;
    }

    /**
     * 设置日期16
     *
     * @param date16 日期16
     */
    public void setDate16(BigDecimal date16) {
        this.date16 = date16;
    }

    /**
     * 获取日期17
     *
     * @return DATE17 - 日期17
     */
    public BigDecimal getDate17() {
        return date17;
    }

    /**
     * 设置日期17
     *
     * @param date17 日期17
     */
    public void setDate17(BigDecimal date17) {
        this.date17 = date17;
    }

    /**
     * 获取日期18
     *
     * @return DATE18 - 日期18
     */
    public BigDecimal getDate18() {
        return date18;
    }

    /**
     * 设置日期18
     *
     * @param date18 日期18
     */
    public void setDate18(BigDecimal date18) {
        this.date18 = date18;
    }

    /**
     * 获取日期19
     *
     * @return DATE19 - 日期19
     */
    public BigDecimal getDate19() {
        return date19;
    }

    /**
     * 设置日期19
     *
     * @param date19 日期19
     */
    public void setDate19(BigDecimal date19) {
        this.date19 = date19;
    }

    /**
     * 获取日期20
     *
     * @return DATE20 - 日期20
     */
    public BigDecimal getDate20() {
        return date20;
    }

    /**
     * 设置日期20
     *
     * @param date20 日期20
     */
    public void setDate20(BigDecimal date20) {
        this.date20 = date20;
    }

    /**
     * 获取日期21
     *
     * @return DATE21 - 日期21
     */
    public BigDecimal getDate21() {
        return date21;
    }

    /**
     * 设置日期21
     *
     * @param date21 日期21
     */
    public void setDate21(BigDecimal date21) {
        this.date21 = date21;
    }

    /**
     * 获取日期22
     *
     * @return DATE22 - 日期22
     */
    public BigDecimal getDate22() {
        return date22;
    }

    /**
     * 设置日期22
     *
     * @param date22 日期22
     */
    public void setDate22(BigDecimal date22) {
        this.date22 = date22;
    }

    /**
     * 获取日期23
     *
     * @return DATE23 - 日期23
     */
    public BigDecimal getDate23() {
        return date23;
    }

    /**
     * 设置日期23
     *
     * @param date23 日期23
     */
    public void setDate23(BigDecimal date23) {
        this.date23 = date23;
    }

    /**
     * 获取日期24
     *
     * @return DATE24 - 日期24
     */
    public BigDecimal getDate24() {
        return date24;
    }

    /**
     * 设置日期24
     *
     * @param date24 日期24
     */
    public void setDate24(BigDecimal date24) {
        this.date24 = date24;
    }

    /**
     * 获取日期25
     *
     * @return DATE25 - 日期25
     */
    public BigDecimal getDate25() {
        return date25;
    }

    /**
     * 设置日期25
     *
     * @param date25 日期25
     */
    public void setDate25(BigDecimal date25) {
        this.date25 = date25;
    }

    /**
     * 获取日期26
     *
     * @return DATE26 - 日期26
     */
    public BigDecimal getDate26() {
        return date26;
    }

    /**
     * 设置日期26
     *
     * @param date26 日期26
     */
    public void setDate26(BigDecimal date26) {
        this.date26 = date26;
    }

    /**
     * 获取日期27
     *
     * @return DATE27 - 日期27
     */
    public BigDecimal getDate27() {
        return date27;
    }

    /**
     * 设置日期27
     *
     * @param date27 日期27
     */
    public void setDate27(BigDecimal date27) {
        this.date27 = date27;
    }

    /**
     * 获取日期28
     *
     * @return DATE28 - 日期28
     */
    public BigDecimal getDate28() {
        return date28;
    }

    /**
     * 设置日期28
     *
     * @param date28 日期28
     */
    public void setDate28(BigDecimal date28) {
        this.date28 = date28;
    }

    /**
     * 获取日期29
     *
     * @return DATE29 - 日期29
     */
    public BigDecimal getDate29() {
        return date29;
    }

    /**
     * 设置日期29
     *
     * @param date29 日期29
     */
    public void setDate29(BigDecimal date29) {
        this.date29 = date29;
    }

    /**
     * 获取日期30
     *
     * @return DATE30 - 日期30
     */
    public BigDecimal getDate30() {
        return date30;
    }

    /**
     * 设置日期30
     *
     * @param date30 日期30
     */
    public void setDate30(BigDecimal date30) {
        this.date30 = date30;
    }

    /**
     * 获取日期31
     *
     * @return DATE31 - 日期31
     */
    public BigDecimal getDate31() {
        return date31;
    }

    /**
     * 设置日期31
     *
     * @param date31 日期31
     */
    public void setDate31(BigDecimal date31) {
        this.date31 = date31;
    }

    /**
     * 获取合计
     *
     * @return DATEHJ - 合计
     */
    public BigDecimal getDatehj() {
        return datehj;
    }

    /**
     * 设置合计
     *
     * @param datehj 合计
     */
    public void setDatehj(BigDecimal datehj) {
        this.datehj = datehj;
    }

    /**
     * 获取平均值
     *
     * @return DATEPJZ - 平均值
     */
    public BigDecimal getDatepjz() {
        return datepjz;
    }

    /**
     * 设置平均值
     *
     * @param datepjz 平均值
     */
    public void setDatepjz(BigDecimal datepjz) {
        this.datepjz = datepjz;
    }
}