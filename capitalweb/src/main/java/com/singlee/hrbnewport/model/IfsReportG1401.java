package com.singlee.hrbnewport.model;

import java.math.BigDecimal;

/**
 *
 * 2021/12/7
 * @Auther:zk
 */    
    
/**
    * G1401大额风险暴露总体情况
    */
public class IfsReportG1401 {
    /**
    * 项目
    */
    private String project;

    /**
    * 风险暴露总和/余额
    */
    private BigDecimal riskExpSum;

    /**
    * 风险暴露构成-一般风险暴露
    */
    private BigDecimal genRiskSum;

    /**
    * 风险暴露构成-特定风险暴露
    */
    private BigDecimal specRiskSum;

    /**
    * 风险暴露构成-交易账簿风险暴露
    */
    private BigDecimal tradeRiskExp;

    /**
    * 风险暴露构成-交易对手信用风险暴露
    */
    private BigDecimal custRiskSum;

    /**
    * 风险暴露构成-潜在风险暴露
    */
    private BigDecimal qzRiskExp;

    /**
    * 风险暴露构成-其他风险暴露
    */
    private BigDecimal qtRiskExp;

    /**
    * 排序字段
    */
    private BigDecimal sortFiled;

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public BigDecimal getRiskExpSum() {
        return riskExpSum;
    }

    public void setRiskExpSum(BigDecimal riskExpSum) {
        this.riskExpSum = riskExpSum;
    }

    public BigDecimal getGenRiskSum() {
        return genRiskSum;
    }

    public void setGenRiskSum(BigDecimal genRiskSum) {
        this.genRiskSum = genRiskSum;
    }

    public BigDecimal getSpecRiskSum() {
        return specRiskSum;
    }

    public void setSpecRiskSum(BigDecimal specRiskSum) {
        this.specRiskSum = specRiskSum;
    }

    public BigDecimal getTradeRiskExp() {
        return tradeRiskExp;
    }

    public void setTradeRiskExp(BigDecimal tradeRiskExp) {
        this.tradeRiskExp = tradeRiskExp;
    }

    public BigDecimal getCustRiskSum() {
        return custRiskSum;
    }

    public void setCustRiskSum(BigDecimal custRiskSum) {
        this.custRiskSum = custRiskSum;
    }

    public BigDecimal getQzRiskExp() {
        return qzRiskExp;
    }

    public void setQzRiskExp(BigDecimal qzRiskExp) {
        this.qzRiskExp = qzRiskExp;
    }

    public BigDecimal getQtRiskExp() {
        return qtRiskExp;
    }

    public void setQtRiskExp(BigDecimal qtRiskExp) {
        this.qtRiskExp = qtRiskExp;
    }

    public BigDecimal getSortFiled() {
        return sortFiled;
    }

    public void setSortFiled(BigDecimal sortFiled) {
        this.sortFiled = sortFiled;
    }
}