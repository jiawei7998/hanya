package com.singlee.hrbnewport.model;

import java.math.BigDecimal;

/**
 *
 * 2021/11/25
 * @Auther:zk
 */    
    
/**
    * G1402大额风险暴露统计报表[G14_II不限制,G14_V同业单一客户100,G14_VI集团客户30]
    */
public class IfsReportG1402 {
    /**
    * 客户类型
    */
    private String custType;

    /**
    * 客户名称
    */
    private String custName;

    /**
    * 客户代码(组织机构代码)
    */
    private String custOrgcode;

    /**
    * 风险暴露总和-合计
    */
    private BigDecimal riskExpSum;

    /**
    * 风险暴露总和-其中:不可豁免风险暴露(三家政策性银行是0)
    */
    private BigDecimal riskExp;

    /**
    * 占一级资本净额比例-合计
    */
    private BigDecimal netCapSum;

    /**
    * 占一级资本净额比例-其中不可豁免风险暴露
    */
    private BigDecimal netCap;

    /**
    * 一般风险暴露 (商业银行大额风险暴露管理办法)-合计
    */
    private BigDecimal genRiskSum;

    /**
    * 特定风险暴露-合计
    */
    private BigDecimal specRiskSum;

    /**
    * 交易账簿风险暴露
    */
    private BigDecimal tradeRiskExp;

    /**
    * 交易对手信用风险暴露-合计
    */
    private BigDecimal custRiskSum;

    /**
    * 潜在风险暴露
    */
    private BigDecimal qzRiskExp;

    /**
    * 其他风险暴露
    */
    private BigDecimal qtRiskExp;

    /**
    * 风险缓释转出的风险暴露（转入为负数）
    */
    private BigDecimal revRiskExp;

    /**
    * 不考虑风险缓释作用的风险暴露总和
    */
    private BigDecimal noneRevRisk;

    /**
    * 不考虑风险缓释作用的风险暴露-占一级资本净额比例);
    */
    private BigDecimal noneRevRiskPer;

    public String getCustType() {
        return custType;
    }

    public void setCustType(String custType) {
        this.custType = custType;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getCustOrgcode() {
        return custOrgcode;
    }

    public void setCustOrgcode(String custOrgcode) {
        this.custOrgcode = custOrgcode;
    }

    public BigDecimal getRiskExpSum() {
        return riskExpSum;
    }

    public void setRiskExpSum(BigDecimal riskExpSum) {
        this.riskExpSum = riskExpSum;
    }

    public BigDecimal getRiskExp() {
        return riskExp;
    }

    public void setRiskExp(BigDecimal riskExp) {
        this.riskExp = riskExp;
    }

    public BigDecimal getNetCapSum() {
        return netCapSum;
    }

    public void setNetCapSum(BigDecimal netCapSum) {
        this.netCapSum = netCapSum;
    }

    public BigDecimal getNetCap() {
        return netCap;
    }

    public void setNetCap(BigDecimal netCap) {
        this.netCap = netCap;
    }

    public BigDecimal getGenRiskSum() {
        return genRiskSum;
    }

    public void setGenRiskSum(BigDecimal genRiskSum) {
        this.genRiskSum = genRiskSum;
    }

    public BigDecimal getSpecRiskSum() {
        return specRiskSum;
    }

    public void setSpecRiskSum(BigDecimal specRiskSum) {
        this.specRiskSum = specRiskSum;
    }

    public BigDecimal getTradeRiskExp() {
        return tradeRiskExp;
    }

    public void setTradeRiskExp(BigDecimal tradeRiskExp) {
        this.tradeRiskExp = tradeRiskExp;
    }

    public BigDecimal getCustRiskSum() {
        return custRiskSum;
    }

    public void setCustRiskSum(BigDecimal custRiskSum) {
        this.custRiskSum = custRiskSum;
    }

    public BigDecimal getQzRiskExp() {
        return qzRiskExp;
    }

    public void setQzRiskExp(BigDecimal qzRiskExp) {
        this.qzRiskExp = qzRiskExp;
    }

    public BigDecimal getQtRiskExp() {
        return qtRiskExp;
    }

    public void setQtRiskExp(BigDecimal qtRiskExp) {
        this.qtRiskExp = qtRiskExp;
    }

    public BigDecimal getRevRiskExp() {
        return revRiskExp;
    }

    public void setRevRiskExp(BigDecimal revRiskExp) {
        this.revRiskExp = revRiskExp;
    }

    public BigDecimal getNoneRevRisk() {
        return noneRevRisk;
    }

    public void setNoneRevRisk(BigDecimal noneRevRisk) {
        this.noneRevRisk = noneRevRisk;
    }

    public BigDecimal getNoneRevRiskPer() {
        return noneRevRiskPer;
    }

    public void setNoneRevRiskPer(BigDecimal noneRevRiskPer) {
        this.noneRevRiskPer = noneRevRiskPer;
    }
}