package com.singlee.hrbnewport.model;

/**
    * 自营资金业务余额表
    */
public class TbZyzjbalance {
    /**
    * 采集日期
    */
    private String cjrq;

    /**
    * 存量日期
    */
    private String clrq;

    /**
    * 银行机构代码
    */
    private String yxjgdm;

    /**
    * 金融许可证号
    */
    private String jrxkzh;

    /**
    * 银行机构名称
    */
    private String yxjgmc;

    /**
    * 内部机构号
    */
    private String nbjgh;

    /**
    * 行内归属部门
    */
    private String hnssbm;

    /**
    * 账户类型
    */
    private String jyzhlx;

    /**
    * 交易编号
    */
    private String jybh;

    /**
    * 业务中类
    */
    private String ywzl;

    /**
    * 业务小类
    */
    private String ywxl;

    /**
    * 银行产品
    */
    private String yhcp;

    /**
    * 业务余额
    */
    private String ywye;

    /**
    * 币种
    */
    private String bz;

    /**
    * 基础资产编号
    */
    private String jczcbh;

    /**
    * 基础资产名称
    */
    private String jczcmc;

    /**
    * 基础资产评级
    */
    private String jczcpj;

    /**
    * 基础资产评级机构
    */
    private String jczcpjjg;

    /**
    * 基础资产客户编号
    */
    private String jczckhbh;

    /**
    * 基础资产客户名称
    */
    private String jczckhmc;

    /**
    * 基础资产客户国家
    */
    private String jczckhgj;

    /**
    * 基础资产客户评级
    */
    private String jczckhpj;

    /**
    * 基础客户评级机构
    */
    private String jckhpjjg;

    /**
    * 基础资产或客户行业
    */
    private String jczchkhhy;

    /**
    * 最终投向类型
    */
    private String zztxlx;

    /**
    * 最终投向行业
    */
    private String zztxhy;

    /**
    * 信用风险权重
    */
    private String xyfxqz;

    /**
    * 五级分类
    */
    private String wjfl;

    /**
    * 减值准备
    */
    private String jzzb;

    /**
    * 起息日
    */
    private String qxr;

    /**
    * 到期日
    */
    private String dqr;

    /**
    * 交易方向
    */
    private String jyfx;

    /**
    * 剩余面值
    */
    private String symz;

    /**
    * 账面余额
    */
    private String zmye;

    /**
    * 违约金额
    */
    private String wyje;

    /**
    * 年华利率
    */
    private String nhll;

    /**
    * 审批人
    */
    private String spr;

    /**
    * 交易员
    */
    private String jyy;

    public String getCjrq() {
        return cjrq;
    }

    public void setCjrq(String cjrq) {
        this.cjrq = cjrq == null ? null : cjrq.trim();
    }

    public String getClrq() {
        return clrq;
    }

    public void setClrq(String clrq) {
        this.clrq = clrq == null ? null : clrq.trim();
    }

    public String getYxjgdm() {
        return yxjgdm;
    }

    public void setYxjgdm(String yxjgdm) {
        this.yxjgdm = yxjgdm == null ? null : yxjgdm.trim();
    }

    public String getJrxkzh() {
        return jrxkzh;
    }

    public void setJrxkzh(String jrxkzh) {
        this.jrxkzh = jrxkzh == null ? null : jrxkzh.trim();
    }

    public String getYxjgmc() {
        return yxjgmc;
    }

    public void setYxjgmc(String yxjgmc) {
        this.yxjgmc = yxjgmc == null ? null : yxjgmc.trim();
    }

    public String getNbjgh() {
        return nbjgh;
    }

    public void setNbjgh(String nbjgh) {
        this.nbjgh = nbjgh == null ? null : nbjgh.trim();
    }

    public String getHnssbm() {
        return hnssbm;
    }

    public void setHnssbm(String hnssbm) {
        this.hnssbm = hnssbm == null ? null : hnssbm.trim();
    }

    public String getJyzhlx() {
        return jyzhlx;
    }

    public void setJyzhlx(String jyzhlx) {
        this.jyzhlx = jyzhlx == null ? null : jyzhlx.trim();
    }

    public String getJybh() {
        return jybh;
    }

    public void setJybh(String jybh) {
        this.jybh = jybh == null ? null : jybh.trim();
    }

    public String getYwzl() {
        return ywzl;
    }

    public void setYwzl(String ywzl) {
        this.ywzl = ywzl == null ? null : ywzl.trim();
    }

    public String getYwxl() {
        return ywxl;
    }

    public void setYwxl(String ywxl) {
        this.ywxl = ywxl == null ? null : ywxl.trim();
    }

    public String getYhcp() {
        return yhcp;
    }

    public void setYhcp(String yhcp) {
        this.yhcp = yhcp == null ? null : yhcp.trim();
    }

    public String getYwye() {
        return ywye;
    }

    public void setYwye(String ywye) {
        this.ywye = ywye == null ? null : ywye.trim();
    }

    public String getBz() {
        return bz;
    }

    public void setBz(String bz) {
        this.bz = bz == null ? null : bz.trim();
    }

    public String getJczcbh() {
        return jczcbh;
    }

    public void setJczcbh(String jczcbh) {
        this.jczcbh = jczcbh == null ? null : jczcbh.trim();
    }

    public String getJczcmc() {
        return jczcmc;
    }

    public void setJczcmc(String jczcmc) {
        this.jczcmc = jczcmc == null ? null : jczcmc.trim();
    }

    public String getJczcpj() {
        return jczcpj;
    }

    public void setJczcpj(String jczcpj) {
        this.jczcpj = jczcpj == null ? null : jczcpj.trim();
    }

    public String getJczcpjjg() {
        return jczcpjjg;
    }

    public void setJczcpjjg(String jczcpjjg) {
        this.jczcpjjg = jczcpjjg == null ? null : jczcpjjg.trim();
    }

    public String getJczckhbh() {
        return jczckhbh;
    }

    public void setJczckhbh(String jczckhbh) {
        this.jczckhbh = jczckhbh == null ? null : jczckhbh.trim();
    }

    public String getJczckhmc() {
        return jczckhmc;
    }

    public void setJczckhmc(String jczckhmc) {
        this.jczckhmc = jczckhmc == null ? null : jczckhmc.trim();
    }

    public String getJczckhgj() {
        return jczckhgj;
    }

    public void setJczckhgj(String jczckhgj) {
        this.jczckhgj = jczckhgj == null ? null : jczckhgj.trim();
    }

    public String getJczckhpj() {
        return jczckhpj;
    }

    public void setJczckhpj(String jczckhpj) {
        this.jczckhpj = jczckhpj == null ? null : jczckhpj.trim();
    }

    public String getJckhpjjg() {
        return jckhpjjg;
    }

    public void setJckhpjjg(String jckhpjjg) {
        this.jckhpjjg = jckhpjjg == null ? null : jckhpjjg.trim();
    }

    public String getJczchkhhy() {
        return jczchkhhy;
    }

    public void setJczchkhhy(String jczchkhhy) {
        this.jczchkhhy = jczchkhhy == null ? null : jczchkhhy.trim();
    }

    public String getZztxlx() {
        return zztxlx;
    }

    public void setZztxlx(String zztxlx) {
        this.zztxlx = zztxlx == null ? null : zztxlx.trim();
    }

    public String getZztxhy() {
        return zztxhy;
    }

    public void setZztxhy(String zztxhy) {
        this.zztxhy = zztxhy == null ? null : zztxhy.trim();
    }

    public String getXyfxqz() {
        return xyfxqz;
    }

    public void setXyfxqz(String xyfxqz) {
        this.xyfxqz = xyfxqz == null ? null : xyfxqz.trim();
    }

    public String getWjfl() {
        return wjfl;
    }

    public void setWjfl(String wjfl) {
        this.wjfl = wjfl == null ? null : wjfl.trim();
    }

    public String getJzzb() {
        return jzzb;
    }

    public void setJzzb(String jzzb) {
        this.jzzb = jzzb == null ? null : jzzb.trim();
    }

    public String getQxr() {
        return qxr;
    }

    public void setQxr(String qxr) {
        this.qxr = qxr == null ? null : qxr.trim();
    }

    public String getDqr() {
        return dqr;
    }

    public void setDqr(String dqr) {
        this.dqr = dqr == null ? null : dqr.trim();
    }

    public String getJyfx() {
        return jyfx;
    }

    public void setJyfx(String jyfx) {
        this.jyfx = jyfx == null ? null : jyfx.trim();
    }

    public String getSymz() {
        return symz;
    }

    public void setSymz(String symz) {
        this.symz = symz == null ? null : symz.trim();
    }

    public String getZmye() {
        return zmye;
    }

    public void setZmye(String zmye) {
        this.zmye = zmye == null ? null : zmye.trim();
    }

    public String getWyje() {
        return wyje;
    }

    public void setWyje(String wyje) {
        this.wyje = wyje == null ? null : wyje.trim();
    }

    public String getNhll() {
        return nhll;
    }

    public void setNhll(String nhll) {
        this.nhll = nhll == null ? null : nhll.trim();
    }

    public String getSpr() {
        return spr;
    }

    public void setSpr(String spr) {
        this.spr = spr == null ? null : spr.trim();
    }

    public String getJyy() {
        return jyy;
    }

    public void setJyy(String jyy) {
        this.jyy = jyy == null ? null : jyy.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", cjrq=").append(cjrq);
        sb.append(", clrq=").append(clrq);
        sb.append(", yxjgdm=").append(yxjgdm);
        sb.append(", jrxkzh=").append(jrxkzh);
        sb.append(", yxjgmc=").append(yxjgmc);
        sb.append(", nbjgh=").append(nbjgh);
        sb.append(", hnssbm=").append(hnssbm);
        sb.append(", jyzhlx=").append(jyzhlx);
        sb.append(", jybh=").append(jybh);
        sb.append(", ywzl=").append(ywzl);
        sb.append(", ywxl=").append(ywxl);
        sb.append(", yhcp=").append(yhcp);
        sb.append(", ywye=").append(ywye);
        sb.append(", bz=").append(bz);
        sb.append(", jczcbh=").append(jczcbh);
        sb.append(", jczcmc=").append(jczcmc);
        sb.append(", jczcpj=").append(jczcpj);
        sb.append(", jczcpjjg=").append(jczcpjjg);
        sb.append(", jczckhbh=").append(jczckhbh);
        sb.append(", jczckhmc=").append(jczckhmc);
        sb.append(", jczckhgj=").append(jczckhgj);
        sb.append(", jczckhpj=").append(jczckhpj);
        sb.append(", jckhpjjg=").append(jckhpjjg);
        sb.append(", jczchkhhy=").append(jczchkhhy);
        sb.append(", zztxlx=").append(zztxlx);
        sb.append(", zztxhy=").append(zztxhy);
        sb.append(", xyfxqz=").append(xyfxqz);
        sb.append(", wjfl=").append(wjfl);
        sb.append(", jzzb=").append(jzzb);
        sb.append(", qxr=").append(qxr);
        sb.append(", dqr=").append(dqr);
        sb.append(", jyfx=").append(jyfx);
        sb.append(", symz=").append(symz);
        sb.append(", zmye=").append(zmye);
        sb.append(", wyje=").append(wyje);
        sb.append(", nhll=").append(nhll);
        sb.append(", spr=").append(spr);
        sb.append(", jyy=").append(jyy);
        sb.append("]");
        return sb.toString();
    }
}