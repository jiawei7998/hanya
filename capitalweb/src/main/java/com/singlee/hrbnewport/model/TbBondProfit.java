package com.singlee.hrbnewport.model;

import java.math.BigDecimal;

/**
    * 利润测算表
    */
public class TbBondProfit {
    /**
    * 资产分类
    */
    private String assetsClassification;

    /**
    * 债券分类
    */
    private String bondType;

    /**
    * 债券代码
    */
    private String bondCode;

    /**
    * 债券名称
    */
    private String bondName;

    /**
    * 持仓面额
    */
    private BigDecimal faceamt;

    /**
    * 利息变动额
    */
    private BigDecimal rateChange;

    public String getAssetsClassification() {
        return assetsClassification;
    }

    public void setAssetsClassification(String assetsClassification) {
        this.assetsClassification = assetsClassification == null ? null : assetsClassification.trim();
    }

    public String getBondType() {
        return bondType;
    }

    public void setBondType(String bondType) {
        this.bondType = bondType == null ? null : bondType.trim();
    }

    public String getBondCode() {
        return bondCode;
    }

    public void setBondCode(String bondCode) {
        this.bondCode = bondCode == null ? null : bondCode.trim();
    }

    public String getBondName() {
        return bondName;
    }

    public void setBondName(String bondName) {
        this.bondName = bondName == null ? null : bondName.trim();
    }

    public BigDecimal getFaceamt() {
        return faceamt;
    }

    public void setFaceamt(BigDecimal faceamt) {
        this.faceamt = faceamt;
    }

    public BigDecimal getRateChange() {
        return rateChange;
    }

    public void setRateChange(BigDecimal rateChange) {
        this.rateChange = rateChange;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", assetsClassification=").append(assetsClassification);
        sb.append(", bondType=").append(bondType);
        sb.append(", bondCode=").append(bondCode);
        sb.append(", bondName=").append(bondName);
        sb.append(", faceamt=").append(faceamt);
        sb.append(", rateChange=").append(rateChange);
        sb.append("]");
        return sb.toString();
    }
}