package com.singlee.hrbnewport.model;

import java.math.BigDecimal;

/**
    * 交易账户债券业务已实现损益情况
    */
public class IfsReportBondtraded {
    /**
    * 业务类型
    */
    private String businessType;

    /**
    * 组合类型
    */
    private String combinationType;

    /**
    * 当日损益
    */
    private BigDecimal dayGainsLosses;

    /**
    * 周累计损益
    */
    private BigDecimal weekGainsLosses;

    /**
    * 当月累计损益
    */
    private BigDecimal monthGainsLosses;

    /**
    * 当季累计损益
    */
    private BigDecimal seasonGainsLosses;

    /**
    * 当年累计损益
    */
    private BigDecimal yearGainsLosses;

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType == null ? null : businessType.trim();
    }

    public String getCombinationType() {
        return combinationType;
    }

    public void setCombinationType(String combinationType) {
        this.combinationType = combinationType == null ? null : combinationType.trim();
    }

    public BigDecimal getDayGainsLosses() {
        return dayGainsLosses;
    }

    public void setDayGainsLosses(BigDecimal dayGainsLosses) {
        this.dayGainsLosses = dayGainsLosses;
    }

    public BigDecimal getWeekGainsLosses() {
        return weekGainsLosses;
    }

    public void setWeekGainsLosses(BigDecimal weekGainsLosses) {
        this.weekGainsLosses = weekGainsLosses;
    }

    public BigDecimal getMonthGainsLosses() {
        return monthGainsLosses;
    }

    public void setMonthGainsLosses(BigDecimal monthGainsLosses) {
        this.monthGainsLosses = monthGainsLosses;
    }

    public BigDecimal getSeasonGainsLosses() {
        return seasonGainsLosses;
    }

    public void setSeasonGainsLosses(BigDecimal seasonGainsLosses) {
        this.seasonGainsLosses = seasonGainsLosses;
    }

    public BigDecimal getYearGainsLosses() {
        return yearGainsLosses;
    }

    public void setYearGainsLosses(BigDecimal yearGainsLosses) {
        this.yearGainsLosses = yearGainsLosses;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", businessType=").append(businessType);
        sb.append(", combinationType=").append(combinationType);
        sb.append(", dayGainsLosses=").append(dayGainsLosses);
        sb.append(", weekGainsLosses=").append(weekGainsLosses);
        sb.append(", monthGainsLosses=").append(monthGainsLosses);
        sb.append(", seasonGainsLosses=").append(seasonGainsLosses);
        sb.append(", yearGainsLosses=").append(yearGainsLosses);
        sb.append("]");
        return sb.toString();
    }
}