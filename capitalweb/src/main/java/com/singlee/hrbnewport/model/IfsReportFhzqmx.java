package com.singlee.hrbnewport.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

/**
 * 分行债券明细表
 */
@Table(name = "IFS_REPORT_FHZQMX")
public class IfsReportFhzqmx implements Serializable {
    /**
     * 分管部门
     */
    @Column(name = "FGBM")
    private String fgbm;

    /**
     * 哪家分行
     */
    @Column(name = "NJFH")
    private String njfh;

    /**
     * 债券类型
     */
    @Column(name = "ZQLX")
    private String zqlx;

    /**
     * 债券名称
     */
    @Column(name = "ZQMC")
    private String zqmc;

    /**
     * 债券代码
     */
    @Column(name = "ZQDM")
    private String zqdm;

    /**
     * 起息日
     */
    @Column(name = "QXR")
    private String qxr;

    /**
     * 到期日
     */
    @Column(name = "DQR")
    private String dqr;

    /**
     * 债券面值
     */
    @Column(name = "ZQMZ")
    private BigDecimal zqmz;

    /**
     * 票面利率
     */
    @Column(name = "PMLL")
    private BigDecimal pmll;

    /**
     * 计算收益区间开始日
     */
    @Column(name = "JSSYQJKSR")
    private String jssyqjksr;

    /**
     * 计算收益区间到期日
     */
    @Column(name = "JSSYQJDQR")
    private String jssyqjdqr;

    /**
     * 计算天数
     */
    @Column(name = "JSTS")
    private BigDecimal jsts;

    /**
     * 收益
     */
    @Column(name = "SY")
    private BigDecimal sy;

    /**
     * 增值税
     */
    @Column(name = "ZZS")
    private BigDecimal zzs;

    /**
     * 下划分行收益
     */
    @Column(name = "XHFHSY")
    private BigDecimal xhfhsy;

    /**
     * 计算收益区间实际收到利息
     */
    @Column(name = "JSSYQJSJSDLX")
    private BigDecimal jssyqjsjsdlx;

    private static final long serialVersionUID = 1L;

    /**
     * 获取分管部门
     *
     * @return FGBM - 分管部门
     */
    public String getFgbm() {
        return fgbm;
    }

    /**
     * 设置分管部门
     *
     * @param fgbm 分管部门
     */
    public void setFgbm(String fgbm) {
        this.fgbm = fgbm;
    }

    /**
     * 获取哪家分行
     *
     * @return NJFH - 哪家分行
     */
    public String getNjfh() {
        return njfh;
    }

    /**
     * 设置哪家分行
     *
     * @param njfh 哪家分行
     */
    public void setNjfh(String njfh) {
        this.njfh = njfh;
    }

    /**
     * 获取债券类型
     *
     * @return ZQLX - 债券类型
     */
    public String getZqlx() {
        return zqlx;
    }

    /**
     * 设置债券类型
     *
     * @param zqlx 债券类型
     */
    public void setZqlx(String zqlx) {
        this.zqlx = zqlx;
    }

    /**
     * 获取债券名称
     *
     * @return ZQMC - 债券名称
     */
    public String getZqmc() {
        return zqmc;
    }

    /**
     * 设置债券名称
     *
     * @param zqmc 债券名称
     */
    public void setZqmc(String zqmc) {
        this.zqmc = zqmc;
    }

    /**
     * 获取债券代码
     *
     * @return ZQDM - 债券代码
     */
    public String getZqdm() {
        return zqdm;
    }

    /**
     * 设置债券代码
     *
     * @param zqdm 债券代码
     */
    public void setZqdm(String zqdm) {
        this.zqdm = zqdm;
    }

    /**
     * 获取起息日
     *
     * @return QXR - 起息日
     */
    public String getQxr() {
        return qxr;
    }

    /**
     * 设置起息日
     *
     * @param qxr 起息日
     */
    public void setQxr(String qxr) {
        this.qxr = qxr;
    }

    /**
     * 获取到期日
     *
     * @return DQR - 到期日
     */
    public String getDqr() {
        return dqr;
    }

    /**
     * 设置到期日
     *
     * @param dqr 到期日
     */
    public void setDqr(String dqr) {
        this.dqr = dqr;
    }

    /**
     * 获取债券面值
     *
     * @return ZQMZ - 债券面值
     */
    public BigDecimal getZqmz() {
        return zqmz;
    }

    /**
     * 设置债券面值
     *
     * @param zqmz 债券面值
     */
    public void setZqmz(BigDecimal zqmz) {
        this.zqmz = zqmz;
    }

    /**
     * 获取票面利率
     *
     * @return PMLL - 票面利率
     */
    public BigDecimal getPmll() {
        return pmll;
    }

    /**
     * 设置票面利率
     *
     * @param pmll 票面利率
     */
    public void setPmll(BigDecimal pmll) {
        this.pmll = pmll;
    }

    /**
     * 获取计算收益区间开始日
     *
     * @return JSSYQJKSR - 计算收益区间开始日
     */
    public String getJssyqjksr() {
        return jssyqjksr;
    }

    /**
     * 设置计算收益区间开始日
     *
     * @param jssyqjksr 计算收益区间开始日
     */
    public void setJssyqjksr(String jssyqjksr) {
        this.jssyqjksr = jssyqjksr;
    }

    /**
     * 获取计算收益区间到期日
     *
     * @return JSSYQJDQR - 计算收益区间到期日
     */
    public String getJssyqjdqr() {
        return jssyqjdqr;
    }

    /**
     * 设置计算收益区间到期日
     *
     * @param jssyqjdqr 计算收益区间到期日
     */
    public void setJssyqjdqr(String jssyqjdqr) {
        this.jssyqjdqr = jssyqjdqr;
    }

    /**
     * 获取计算天数
     *
     * @return JSTS - 计算天数
     */
    public BigDecimal getJsts() {
        return jsts;
    }

    /**
     * 设置计算天数
     *
     * @param jsts 计算天数
     */
    public void setJsts(BigDecimal jsts) {
        this.jsts = jsts;
    }

    /**
     * 获取收益
     *
     * @return SY - 收益
     */
    public BigDecimal getSy() {
        return sy;
    }

    /**
     * 设置收益
     *
     * @param sy 收益
     */
    public void setSy(BigDecimal sy) {
        this.sy = sy;
    }

    /**
     * 获取增值税
     *
     * @return ZZS - 增值税
     */
    public BigDecimal getZzs() {
        return zzs;
    }

    /**
     * 设置增值税
     *
     * @param zzs 增值税
     */
    public void setZzs(BigDecimal zzs) {
        this.zzs = zzs;
    }

    /**
     * 获取下划分行收益
     *
     * @return XHFHSY - 下划分行收益
     */
    public BigDecimal getXhfhsy() {
        return xhfhsy;
    }

    /**
     * 设置下划分行收益
     *
     * @param xhfhsy 下划分行收益
     */
    public void setXhfhsy(BigDecimal xhfhsy) {
        this.xhfhsy = xhfhsy;
    }

    /**
     * 获取计算收益区间实际收到利息
     *
     * @return JSSYQJSJSDLX - 计算收益区间实际收到利息
     */
    public BigDecimal getJssyqjsjsdlx() {
        return jssyqjsjsdlx;
    }

    /**
     * 设置计算收益区间实际收到利息
     *
     * @param jssyqjsjsdlx 计算收益区间实际收到利息
     */
    public void setJssyqjsjsdlx(BigDecimal jssyqjsjsdlx) {
        this.jssyqjsjsdlx = jssyqjsjsdlx;
    }
}