package com.singlee.hrbnewport.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

/**
    * 债券投资账户结构表(按会计账户）
    */
@Table(name = "IFS_REPORT_JRSCBYWGL_ZQTZZHAZH")
public class IfsReportJrscbywglZqtzzhazh implements Serializable {
    /**
     * 账户类型
     */
    @Column(name = "ZHTYPE")
    private String zhtype;

    /**
     * 余额
     */
    @Column(name = "YE")
    private BigDecimal ye;

    /**
     * 日均额
     */
    @Column(name = "RJE")
    private BigDecimal rje;

    /**
     * 待偿期（年）
     */
    @Column(name = "DCQ")
    private BigDecimal dcq;

    /**
     * 收益率
     */
    @Column(name = "SYL")
    private BigDecimal syl;

    /**
     * 含溢价收益率
     */
    @Column(name = "HYJSYL")
    private BigDecimal hyjsyl;

    /**
     * 浮盈（亏）
     */
    @Column(name = "FY")
    private BigDecimal fy;

    /**
     * 利息收入
     */
    @Column(name = "LXSR")
    private BigDecimal lxsr;

    /**
     * 处置收益
     */
    @Column(name = "CZSY")
    private BigDecimal czsy;

    /**
     * 中间业务收入
     */
    @Column(name = "ZJYWSR")
    private BigDecimal zjywsr;

    /**
     * 上月余额
     */
    @Column(name = "SYYE")
    private BigDecimal syye;

    /**
     * 较上月(+/-)
     */
    @Column(name = "JSY")
    private String jsy;

    /**
     * 较上月增幅
     */
    @Column(name = "JSYZF")
    private BigDecimal jsyzf;

    /**
     * 年初余额
     */
    @Column(name = "NCYE")
    private BigDecimal ncye;

    /**
     * 较年初(+/-)
     */
    @Column(name = "JNC")
    private String jnc;

    /**
     * 较年初增幅
     */
    @Column(name = "JNCZF")
    private BigDecimal jnczf;

    private static final long serialVersionUID = 1L;

    /**
     * 获取账户类型
     *
     * @return ZHTYPE - 账户类型
     */
    public String getZhtype() {
        return zhtype;
    }

    /**
     * 设置账户类型
     *
     * @param zhtype 账户类型
     */
    public void setZhtype(String zhtype) {
        this.zhtype = zhtype;
    }

    /**
     * 获取余额
     *
     * @return YE - 余额
     */
    public BigDecimal getYe() {
        return ye;
    }

    /**
     * 设置余额
     *
     * @param ye 余额
     */
    public void setYe(BigDecimal ye) {
        this.ye = ye;
    }

    /**
     * 获取日均额
     *
     * @return RJE - 日均额
     */
    public BigDecimal getRje() {
        return rje;
    }

    /**
     * 设置日均额
     *
     * @param rje 日均额
     */
    public void setRje(BigDecimal rje) {
        this.rje = rje;
    }

    /**
     * 获取待偿期（年）
     *
     * @return DCQ - 待偿期（年）
     */
    public BigDecimal getDcq() {
        return dcq;
    }

    /**
     * 设置待偿期（年）
     *
     * @param dcq 待偿期（年）
     */
    public void setDcq(BigDecimal dcq) {
        this.dcq = dcq;
    }

    /**
     * 获取收益率
     *
     * @return SYL - 收益率
     */
    public BigDecimal getSyl() {
        return syl;
    }

    /**
     * 设置收益率
     *
     * @param syl 收益率
     */
    public void setSyl(BigDecimal syl) {
        this.syl = syl;
    }

    /**
     * 获取含溢价收益率
     *
     * @return HYJSYL - 含溢价收益率
     */
    public BigDecimal getHyjsyl() {
        return hyjsyl;
    }

    /**
     * 设置含溢价收益率
     *
     * @param hyjsyl 含溢价收益率
     */
    public void setHyjsyl(BigDecimal hyjsyl) {
        this.hyjsyl = hyjsyl;
    }

    /**
     * 获取浮盈（亏）
     *
     * @return FY - 浮盈（亏）
     */
    public BigDecimal getFy() {
        return fy;
    }

    /**
     * 设置浮盈（亏）
     *
     * @param fy 浮盈（亏）
     */
    public void setFy(BigDecimal fy) {
        this.fy = fy;
    }

    /**
     * 获取利息收入
     *
     * @return LXSR - 利息收入
     */
    public BigDecimal getLxsr() {
        return lxsr;
    }

    /**
     * 设置利息收入
     *
     * @param lxsr 利息收入
     */
    public void setLxsr(BigDecimal lxsr) {
        this.lxsr = lxsr;
    }

    /**
     * 获取处置收益
     *
     * @return CZSY - 处置收益
     */
    public BigDecimal getCzsy() {
        return czsy;
    }

    /**
     * 设置处置收益
     *
     * @param czsy 处置收益
     */
    public void setCzsy(BigDecimal czsy) {
        this.czsy = czsy;
    }

    /**
     * 获取中间业务收入
     *
     * @return ZJYWSR - 中间业务收入
     */
    public BigDecimal getZjywsr() {
        return zjywsr;
    }

    /**
     * 设置中间业务收入
     *
     * @param zjywsr 中间业务收入
     */
    public void setZjywsr(BigDecimal zjywsr) {
        this.zjywsr = zjywsr;
    }

    /**
     * 获取上月余额
     *
     * @return SYYE - 上月余额
     */
    public BigDecimal getSyye() {
        return syye;
    }

    /**
     * 设置上月余额
     *
     * @param syye 上月余额
     */
    public void setSyye(BigDecimal syye) {
        this.syye = syye;
    }

    /**
     * 获取较上月(+/-)
     *
     * @return JSY - 较上月(+/-)
     */
    public String getJsy() {
        return jsy;
    }

    /**
     * 设置较上月(+/-)
     *
     * @param jsy 较上月(+/-)
     */
    public void setJsy(String jsy) {
        this.jsy = jsy;
    }

    /**
     * 获取较上月增幅
     *
     * @return JSYZF - 较上月增幅
     */
    public BigDecimal getJsyzf() {
        return jsyzf;
    }

    /**
     * 设置较上月增幅
     *
     * @param jsyzf 较上月增幅
     */
    public void setJsyzf(BigDecimal jsyzf) {
        this.jsyzf = jsyzf;
    }

    /**
     * 获取年初余额
     *
     * @return NCYE - 年初余额
     */
    public BigDecimal getNcye() {
        return ncye;
    }

    /**
     * 设置年初余额
     *
     * @param ncye 年初余额
     */
    public void setNcye(BigDecimal ncye) {
        this.ncye = ncye;
    }

    /**
     * 获取较年初(+/-)
     *
     * @return JNC - 较年初(+/-)
     */
    public String getJnc() {
        return jnc;
    }

    /**
     * 设置较年初(+/-)
     *
     * @param jnc 较年初(+/-)
     */
    public void setJnc(String jnc) {
        this.jnc = jnc;
    }

    /**
     * 获取较年初增幅
     *
     * @return JNCZF - 较年初增幅
     */
    public BigDecimal getJnczf() {
        return jnczf;
    }

    /**
     * 设置较年初增幅
     *
     * @param jnczf 较年初增幅
     */
    public void setJnczf(BigDecimal jnczf) {
        this.jnczf = jnczf;
    }
}