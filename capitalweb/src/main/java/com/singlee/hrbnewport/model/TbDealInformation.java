package com.singlee.hrbnewport.model;

import java.math.BigDecimal;

/**
    * EAST资金交易信息表
    */
public class TbDealInformation {
    /**
    * 银行机构代码
    */
    private String yhjgdm;

    /**
    * 金融许可证号
    */
    private String jrxkzh;

    /**
    * 内部机构号
    */
    private String nbjgh;

    /**
    * 明细科目编号
    */
    private String mxkmbh;

    /**
    * 银行机构名称
    */
    private String yxjgmc;

    /**
    * 明细科目名称
    */
    private String mxkmmc;

    /**
    * 交易编号
    */
    private String jybh;

    /**
    * 理财产品登记编码
    */
    private String lccpdjbm;

    /**
    * 资金交易类型
    */
    private String jylx;

    /**
    * 资金交易子类
    */
    private String jyzl;

    /**
    * 金融工具编号
    */
    private String jrgjbh;

    /**
    * 账户类型
    */
    private String jyzhlx;

    /**
    * 合同号
    */
    private String hth;

    /**
    * 合同金额
    */
    private BigDecimal htje;

    /**
    * 币种
    */
    private String bz;

    /**
    * 基础资产客户名称
    */
    private String jczckhmc;

    /**
    * 基础资产所属行业
    */
    private String jczcsshy;

    /**
    * 基础资产是否为本行客户
    */
    private String jczcsfwbhkh;

    /**
    * 基础资产增信方式
    */
    private String jczczxfs;

    /**
    * 基础资产增信人
    */
    private String jczczxr;

    /**
    * 交易柜员
    */
    private String jygy;

    /**
    * 审批人
    */
    private String spr;

    /**
    * 交易对手代码
    */
    private String jydsdm;

    /**
    * 交易对手名称
    */
    private String jydsmc;

    /**
    * 交易日期
    */
    private String jyrq;

    /**
    * 起始日期
    */
    private String qsrq;

    /**
    * 到期日期
    */
    private String dqrq;

    /**
    * 买卖标志
    */
    private String mmbz;

    /**
    * 即远期标志
    */
    private String jyqbz;

    /**
    * 买入币种
    */
    private String mrbz;

    /**
    * 买入金额
    */
    private BigDecimal mrje;

    /**
    * 卖出币种
    */
    private String mcbz;

    /**
    * 卖出金额
    */
    private BigDecimal mcje;

    /**
    * 成交价格
    */
    private String cjjg;

    /**
    * 交易状态
    */
    private String jyzt;

    /**
    * 复核日期
    */
    private String fhrq;

    /**
    * 取消日期
    */
    private String qxrq;

    /**
    * 实际交割日期
    */
    private String sjjgrq;

    /**
    * 清算标志
    */
    private String qsbz;

    /**
    * 借方账号
    */
    private String jfzh;

    /**
    * 贷方账号
    */
    private String dfzh;

    /**
    * 借方金额
    */
    private BigDecimal jfje;

    /**
    * 贷方金额
    */
    private BigDecimal dfje;

    /**
    * 借方币种
    */
    private String jfbz;

    /**
    * 贷方币种
    */
    private String dfbz;

    /**
    * 借方利率
    */
    private BigDecimal jfll;

    /**
    * 贷方利率
    */
    private BigDecimal dfll;

    /**
    * 保证金交易标志
    */
    private String bzjjybz;

    /**
    * 关联保证金账户
    */
    private String glbzjzh;

    /**
    * 关联业务编号
    */
    private String glywbh;

    /**
    * 外部关联系统名称
    */
    private String wbglxtmc;

    /**
    * 采集日期
    */
    private String cjrq;

    public String getYhjgdm() {
        return yhjgdm;
    }

    public void setYhjgdm(String yhjgdm) {
        this.yhjgdm = yhjgdm;
    }

    public String getJrxkzh() {
        return jrxkzh;
    }

    public void setJrxkzh(String jrxkzh) {
        this.jrxkzh = jrxkzh;
    }

    public String getNbjgh() {
        return nbjgh;
    }

    public void setNbjgh(String nbjgh) {
        this.nbjgh = nbjgh;
    }

    public String getMxkmbh() {
        return mxkmbh;
    }

    public void setMxkmbh(String mxkmbh) {
        this.mxkmbh = mxkmbh;
    }

    public String getYxjgmc() {
        return yxjgmc;
    }

    public void setYxjgmc(String yxjgmc) {
        this.yxjgmc = yxjgmc;
    }

    public String getMxkmmc() {
        return mxkmmc;
    }

    public void setMxkmmc(String mxkmmc) {
        this.mxkmmc = mxkmmc;
    }

    public String getJybh() {
        return jybh;
    }

    public void setJybh(String jybh) {
        this.jybh = jybh;
    }

    public String getLccpdjbm() {
        return lccpdjbm;
    }

    public void setLccpdjbm(String lccpdjbm) {
        this.lccpdjbm = lccpdjbm;
    }

    public String getJylx() {
        return jylx;
    }

    public void setJylx(String jylx) {
        this.jylx = jylx;
    }

    public String getJyzl() {
        return jyzl;
    }

    public void setJyzl(String jyzl) {
        this.jyzl = jyzl;
    }

    public String getJrgjbh() {
        return jrgjbh;
    }

    public void setJrgjbh(String jrgjbh) {
        this.jrgjbh = jrgjbh;
    }

    public String getJyzhlx() {
        return jyzhlx;
    }

    public void setJyzhlx(String jyzhlx) {
        this.jyzhlx = jyzhlx;
    }

    public String getHth() {
        return hth;
    }

    public void setHth(String hth) {
        this.hth = hth;
    }

    public BigDecimal getHtje() {
        return htje;
    }

    public void setHtje(BigDecimal htje) {
        this.htje = htje;
    }

    public String getBz() {
        return bz;
    }

    public void setBz(String bz) {
        this.bz = bz;
    }

    public String getJczckhmc() {
        return jczckhmc;
    }

    public void setJczckhmc(String jczckhmc) {
        this.jczckhmc = jczckhmc;
    }

    public String getJczcsshy() {
        return jczcsshy;
    }

    public void setJczcsshy(String jczcsshy) {
        this.jczcsshy = jczcsshy;
    }

    public String getJczcsfwbhkh() {
        return jczcsfwbhkh;
    }

    public void setJczcsfwbhkh(String jczcsfwbhkh) {
        this.jczcsfwbhkh = jczcsfwbhkh;
    }

    public String getJczczxfs() {
        return jczczxfs;
    }

    public void setJczczxfs(String jczczxfs) {
        this.jczczxfs = jczczxfs;
    }

    public String getJczczxr() {
        return jczczxr;
    }

    public void setJczczxr(String jczczxr) {
        this.jczczxr = jczczxr;
    }

    public String getJygy() {
        return jygy;
    }

    public void setJygy(String jygy) {
        this.jygy = jygy;
    }

    public String getSpr() {
        return spr;
    }

    public void setSpr(String spr) {
        this.spr = spr;
    }

    public String getJydsdm() {
        return jydsdm;
    }

    public void setJydsdm(String jydsdm) {
        this.jydsdm = jydsdm;
    }

    public String getJydsmc() {
        return jydsmc;
    }

    public void setJydsmc(String jydsmc) {
        this.jydsmc = jydsmc;
    }

    public String getJyrq() {
        return jyrq;
    }

    public void setJyrq(String jyrq) {
        this.jyrq = jyrq;
    }

    public String getQsrq() {
        return qsrq;
    }

    public void setQsrq(String qsrq) {
        this.qsrq = qsrq;
    }

    public String getDqrq() {
        return dqrq;
    }

    public void setDqrq(String dqrq) {
        this.dqrq = dqrq;
    }

    public String getMmbz() {
        return mmbz;
    }

    public void setMmbz(String mmbz) {
        this.mmbz = mmbz;
    }

    public String getJyqbz() {
        return jyqbz;
    }

    public void setJyqbz(String jyqbz) {
        this.jyqbz = jyqbz;
    }

    public String getMrbz() {
        return mrbz;
    }

    public void setMrbz(String mrbz) {
        this.mrbz = mrbz;
    }

    public BigDecimal getMrje() {
        return mrje;
    }

    public void setMrje(BigDecimal mrje) {
        this.mrje = mrje;
    }

    public String getMcbz() {
        return mcbz;
    }

    public void setMcbz(String mcbz) {
        this.mcbz = mcbz;
    }

    public BigDecimal getMcje() {
        return mcje;
    }

    public void setMcje(BigDecimal mcje) {
        this.mcje = mcje;
    }

    public String getCjjg() {
        return cjjg;
    }

    public void setCjjg(String cjjg) {
        this.cjjg = cjjg;
    }

    public String getJyzt() {
        return jyzt;
    }

    public void setJyzt(String jyzt) {
        this.jyzt = jyzt;
    }

    public String getFhrq() {
        return fhrq;
    }

    public void setFhrq(String fhrq) {
        this.fhrq = fhrq;
    }

    public String getQxrq() {
        return qxrq;
    }

    public void setQxrq(String qxrq) {
        this.qxrq = qxrq;
    }

    public String getSjjgrq() {
        return sjjgrq;
    }

    public void setSjjgrq(String sjjgrq) {
        this.sjjgrq = sjjgrq;
    }

    public String getQsbz() {
        return qsbz;
    }

    public void setQsbz(String qsbz) {
        this.qsbz = qsbz;
    }

    public String getJfzh() {
        return jfzh;
    }

    public void setJfzh(String jfzh) {
        this.jfzh = jfzh;
    }

    public String getDfzh() {
        return dfzh;
    }

    public void setDfzh(String dfzh) {
        this.dfzh = dfzh;
    }

    public BigDecimal getJfje() {
        return jfje;
    }

    public void setJfje(BigDecimal jfje) {
        this.jfje = jfje;
    }

    public BigDecimal getDfje() {
        return dfje;
    }

    public void setDfje(BigDecimal dfje) {
        this.dfje = dfje;
    }

    public String getJfbz() {
        return jfbz;
    }

    public void setJfbz(String jfbz) {
        this.jfbz = jfbz;
    }

    public String getDfbz() {
        return dfbz;
    }

    public void setDfbz(String dfbz) {
        this.dfbz = dfbz;
    }

    public BigDecimal getJfll() {
        return jfll;
    }

    public void setJfll(BigDecimal jfll) {
        this.jfll = jfll;
    }

    public BigDecimal getDfll() {
        return dfll;
    }

    public void setDfll(BigDecimal dfll) {
        this.dfll = dfll;
    }

    public String getBzjjybz() {
        return bzjjybz;
    }

    public void setBzjjybz(String bzjjybz) {
        this.bzjjybz = bzjjybz;
    }

    public String getGlbzjzh() {
        return glbzjzh;
    }

    public void setGlbzjzh(String glbzjzh) {
        this.glbzjzh = glbzjzh;
    }

    public String getGlywbh() {
        return glywbh;
    }

    public void setGlywbh(String glywbh) {
        this.glywbh = glywbh;
    }

    public String getWbglxtmc() {
        return wbglxtmc;
    }

    public void setWbglxtmc(String wbglxtmc) {
        this.wbglxtmc = wbglxtmc;
    }

    public String getCjrq() {
        return cjrq;
    }

    public void setCjrq(String cjrq) {
        this.cjrq = cjrq;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", yhjgdm=").append(yhjgdm);
        sb.append(", jrxkzh=").append(jrxkzh);
        sb.append(", nbjgh=").append(nbjgh);
        sb.append(", mxkmbh=").append(mxkmbh);
        sb.append(", yxjgmc=").append(yxjgmc);
        sb.append(", mxkmmc=").append(mxkmmc);
        sb.append(", jybh=").append(jybh);
        sb.append(", lccpdjbm=").append(lccpdjbm);
        sb.append(", jylx=").append(jylx);
        sb.append(", jyzl=").append(jyzl);
        sb.append(", jrgjbh=").append(jrgjbh);
        sb.append(", jyzhlx=").append(jyzhlx);
        sb.append(", hth=").append(hth);
        sb.append(", htje=").append(htje);
        sb.append(", bz=").append(bz);
        sb.append(", jczckhmc=").append(jczckhmc);
        sb.append(", jczcsshy=").append(jczcsshy);
        sb.append(", jczcsfwbhkh=").append(jczcsfwbhkh);
        sb.append(", jczczxfs=").append(jczczxfs);
        sb.append(", jczczxr=").append(jczczxr);
        sb.append(", jygy=").append(jygy);
        sb.append(", spr=").append(spr);
        sb.append(", jydsdm=").append(jydsdm);
        sb.append(", jydsmc=").append(jydsmc);
        sb.append(", jyrq=").append(jyrq);
        sb.append(", qsrq=").append(qsrq);
        sb.append(", dqrq=").append(dqrq);
        sb.append(", mmbz=").append(mmbz);
        sb.append(", jyqbz=").append(jyqbz);
        sb.append(", mrbz=").append(mrbz);
        sb.append(", mrje=").append(mrje);
        sb.append(", mcbz=").append(mcbz);
        sb.append(", mcje=").append(mcje);
        sb.append(", cjjg=").append(cjjg);
        sb.append(", jyzt=").append(jyzt);
        sb.append(", fhrq=").append(fhrq);
        sb.append(", qxrq=").append(qxrq);
        sb.append(", sjjgrq=").append(sjjgrq);
        sb.append(", qsbz=").append(qsbz);
        sb.append(", jfzh=").append(jfzh);
        sb.append(", dfzh=").append(dfzh);
        sb.append(", jfje=").append(jfje);
        sb.append(", dfje=").append(dfje);
        sb.append(", jfbz=").append(jfbz);
        sb.append(", dfbz=").append(dfbz);
        sb.append(", jfll=").append(jfll);
        sb.append(", dfll=").append(dfll);
        sb.append(", bzjjybz=").append(bzjjybz);
        sb.append(", glbzjzh=").append(glbzjzh);
        sb.append(", glywbh=").append(glywbh);
        sb.append(", wbglxtmc=").append(wbglxtmc);
        sb.append(", cjrq=").append(cjrq);
        sb.append("]");
        return sb.toString();
    }
}