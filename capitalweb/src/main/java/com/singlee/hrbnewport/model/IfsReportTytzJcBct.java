package com.singlee.hrbnewport.model;

import java.io.Serializable;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 16:20
 * @description：${description}
 * @modified By：
 * @version:   
 */
public class IfsReportTytzJcBct implements Serializable {
    /**
    * 序号
    */
    private String ser;

    /**
    * 分行名称（含总行）
    */
    private String fhnc;

    /**
    * 支行名称（含总行部门）
    */
    private String zhmc;

    /**
    * 归属部门
    */
    private String gsbm;

    /**
    * 交易对手总部全称
    */
    private String jydszbqc;

    /**
    * 交易对手落地机构
    */
    private String jydsldjg;

    /**
    * 交易对手地域 
    */
    private String jydsdy;

    /**
    * 活期/定期
    */
    private String qxlx;

    /**
    * 外部账号
    */
    private String wbzh;

    /**
    * 内部账号
    */
    private String nbzh;

    /**
    * 业务开始日期
    */
    private String ywkssj;

    /**
    * 业务结束日期
    */
    private String ywjssj;

    /**
    * 利率 （%）
    */
    private String rate;

    /**
    * 金额（元）
    */
    private String amt;

    /**
    * 余额（元）
    */
    private String ye;

    /**
    * 限额（元）
    */
    private String xe;

    /**
    * 限额备注
    */
    private String xebz;

    /**
    * 记账科目
    */
    private String jzkm;

    /**
    * 账户性质
    */
    private String zhxz;

    /**
    * 账户用途）
    */
    private String zhyt;

    /**
    * 账户状态
    */
    private String zhzt;

    /**
    * 是否开网银
    */
    private String sfkwy;

    /**
    * 联系人/电话
    */
    private String phone;

    /**
    * 备注
    */
    private String beizhu;

    /**
    * 交易对手（全称）
    */
    private String jyds;

    private static final long serialVersionUID = 1L;

    public String getSer() {
        return ser;
    }

    public void setSer(String ser) {
        this.ser = ser;
    }

    public String getFhnc() {
        return fhnc;
    }

    public void setFhnc(String fhnc) {
        this.fhnc = fhnc;
    }

    public String getZhmc() {
        return zhmc;
    }

    public void setZhmc(String zhmc) {
        this.zhmc = zhmc;
    }

    public String getGsbm() {
        return gsbm;
    }

    public void setGsbm(String gsbm) {
        this.gsbm = gsbm;
    }

    public String getJydszbqc() {
        return jydszbqc;
    }

    public void setJydszbqc(String jydszbqc) {
        this.jydszbqc = jydszbqc;
    }

    public String getJydsldjg() {
        return jydsldjg;
    }

    public void setJydsldjg(String jydsldjg) {
        this.jydsldjg = jydsldjg;
    }

    public String getJydsdy() {
        return jydsdy;
    }

    public void setJydsdy(String jydsdy) {
        this.jydsdy = jydsdy;
    }

    public String getQxlx() {
        return qxlx;
    }

    public void setQxlx(String qxlx) {
        this.qxlx = qxlx;
    }

    public String getWbzh() {
        return wbzh;
    }

    public void setWbzh(String wbzh) {
        this.wbzh = wbzh;
    }

    public String getNbzh() {
        return nbzh;
    }

    public void setNbzh(String nbzh) {
        this.nbzh = nbzh;
    }

    public String getYwkssj() {
        return ywkssj;
    }

    public void setYwkssj(String ywkssj) {
        this.ywkssj = ywkssj;
    }

    public String getYwjssj() {
        return ywjssj;
    }

    public void setYwjssj(String ywjssj) {
        this.ywjssj = ywjssj;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getAmt() {
        return amt;
    }

    public void setAmt(String amt) {
        this.amt = amt;
    }

    public String getYe() {
        return ye;
    }

    public void setYe(String ye) {
        this.ye = ye;
    }

    public String getXe() {
        return xe;
    }

    public void setXe(String xe) {
        this.xe = xe;
    }

    public String getXebz() {
        return xebz;
    }

    public void setXebz(String xebz) {
        this.xebz = xebz;
    }

    public String getJzkm() {
        return jzkm;
    }

    public void setJzkm(String jzkm) {
        this.jzkm = jzkm;
    }

    public String getZhxz() {
        return zhxz;
    }

    public void setZhxz(String zhxz) {
        this.zhxz = zhxz;
    }

    public String getZhyt() {
        return zhyt;
    }

    public void setZhyt(String zhyt) {
        this.zhyt = zhyt;
    }

    public String getZhzt() {
        return zhzt;
    }

    public void setZhzt(String zhzt) {
        this.zhzt = zhzt;
    }

    public String getSfkwy() {
        return sfkwy;
    }

    public void setSfkwy(String sfkwy) {
        this.sfkwy = sfkwy;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getBeizhu() {
        return beizhu;
    }

    public void setBeizhu(String beizhu) {
        this.beizhu = beizhu;
    }

    public String getJyds() {
        return jyds;
    }

    public void setJyds(String jyds) {
        this.jyds = jyds;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", ser=").append(ser);
        sb.append(", fhnc=").append(fhnc);
        sb.append(", zhmc=").append(zhmc);
        sb.append(", gsbm=").append(gsbm);
        sb.append(", jydszbqc=").append(jydszbqc);
        sb.append(", jydsldjg=").append(jydsldjg);
        sb.append(", jydsdy=").append(jydsdy);
        sb.append(", qxlx=").append(qxlx);
        sb.append(", wbzh=").append(wbzh);
        sb.append(", nbzh=").append(nbzh);
        sb.append(", ywkssj=").append(ywkssj);
        sb.append(", ywjssj=").append(ywjssj);
        sb.append(", rate=").append(rate);
        sb.append(", amt=").append(amt);
        sb.append(", ye=").append(ye);
        sb.append(", xe=").append(xe);
        sb.append(", xebz=").append(xebz);
        sb.append(", jzkm=").append(jzkm);
        sb.append(", zhxz=").append(zhxz);
        sb.append(", zhyt=").append(zhyt);
        sb.append(", zhzt=").append(zhzt);
        sb.append(", sfkwy=").append(sfkwy);
        sb.append(", phone=").append(phone);
        sb.append(", beizhu=").append(beizhu);
        sb.append(", jyds=").append(jyds);
        sb.append("]");
        return sb.toString();
    }
}