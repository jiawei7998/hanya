package com.singlee.hrbnewport.model;

/**
 *
 * 2021/12/6
 * @Auther:zk
 */    
    
/**
    * 业务明细-投资同业存单明细
    */
public class DealdetailCdTpos {
    /**
    * 债券代码
    */
    private String secid;

    /**
    * 债券简称
    */
    private String secName;

    /**
    * 起息日
    */
    private String vdate;

    /**
    * 期限
    */
    private String tenor;

    /**
    * 到期日
    */
    private String mdate;

    /**
    * 账面价值
    */
    private String secValue;

    /**
    * 预期收益率
    */
    private String expYield;

    public String getSecid() {
        return secid;
    }

    public void setSecid(String secid) {
        this.secid = secid;
    }

    public String getSecName() {
        return secName;
    }

    public void setSecName(String secName) {
        this.secName = secName;
    }

    public String getVdate() {
        return vdate;
    }

    public void setVdate(String vdate) {
        this.vdate = vdate;
    }

    public String getTenor() {
        return tenor;
    }

    public void setTenor(String tenor) {
        this.tenor = tenor;
    }

    public String getMdate() {
        return mdate;
    }

    public void setMdate(String mdate) {
        this.mdate = mdate;
    }

    public String getSecValue() {
        return secValue;
    }

    public void setSecValue(String secValue) {
        this.secValue = secValue;
    }

    public String getExpYield() {
        return expYield;
    }

    public void setExpYield(String expYield) {
        this.expYield = expYield;
    }
}