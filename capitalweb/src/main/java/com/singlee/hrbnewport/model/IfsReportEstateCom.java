package com.singlee.hrbnewport.model;

import java.math.BigDecimal;

/**
 * @Author zhangkai
 * @Description 
 * @Date 
 */
/**
    * S67房地产融资风险检测表
    */
public class IfsReportEstateCom {
    /**
    * 项目 
    */
    private String xm;

    /**
    * 本期 
    */
    private BigDecimal bq;

    /**
    * 上年同期 
    */
    private BigDecimal sntq;

    /**
    * 当月新发放金额 
    */
    private BigDecimal dyxffje;

    /**
    * 当月收回金额 
    */
    private BigDecimal dyhsje;

    /**
    * 正常贷款 
    */
    private BigDecimal zcdk;

    /**
    * 正常类 
    */
    private BigDecimal zcl;

    /**
    * 关注类 
    */
    private BigDecimal gzl;

    /**
    * 不良贷款 
    */
    private BigDecimal bldk;

    /**
    * 次级类 
    */
    private BigDecimal cjl;

    /**
    * 可疑类 
    */
    private BigDecimal kyl;

    /**
    * 损失类 
    */
    private BigDecimal ssl;

    /**
    * 展期 
    */
    private BigDecimal zq;

    /**
    * 逾期余额 
    */
    private BigDecimal yqye;

    /**
    * 逾期1-30天 
    */
    private BigDecimal yq1;

    /**
    * 逾期31-60天 
    */
    private BigDecimal yq31;

    /**
    * 逾期61-90天 
    */
    private BigDecimal yq61;

    /**
    * 逾期91-180天 
    */
    private BigDecimal yq91;

    /**
    * 逾期181-360天 
    */
    private BigDecimal yq181;

    /**
    * 逾期361天以上 
    */
    private BigDecimal yq361;

    public String getXm() {
        return xm;
    }

    public void setXm(String xm) {
        this.xm = xm;
    }

    public BigDecimal getBq() {
        return bq;
    }

    public void setBq(BigDecimal bq) {
        this.bq = bq;
    }

    public BigDecimal getSntq() {
        return sntq;
    }

    public void setSntq(BigDecimal sntq) {
        this.sntq = sntq;
    }

    public BigDecimal getDyxffje() {
        return dyxffje;
    }

    public void setDyxffje(BigDecimal dyxffje) {
        this.dyxffje = dyxffje;
    }

    public BigDecimal getDyhsje() {
        return dyhsje;
    }

    public void setDyhsje(BigDecimal dyhsje) {
        this.dyhsje = dyhsje;
    }

    public BigDecimal getZcdk() {
        return zcdk;
    }

    public void setZcdk(BigDecimal zcdk) {
        this.zcdk = zcdk;
    }

    public BigDecimal getZcl() {
        return zcl;
    }

    public void setZcl(BigDecimal zcl) {
        this.zcl = zcl;
    }

    public BigDecimal getGzl() {
        return gzl;
    }

    public void setGzl(BigDecimal gzl) {
        this.gzl = gzl;
    }

    public BigDecimal getBldk() {
        return bldk;
    }

    public void setBldk(BigDecimal bldk) {
        this.bldk = bldk;
    }

    public BigDecimal getCjl() {
        return cjl;
    }

    public void setCjl(BigDecimal cjl) {
        this.cjl = cjl;
    }

    public BigDecimal getKyl() {
        return kyl;
    }

    public void setKyl(BigDecimal kyl) {
        this.kyl = kyl;
    }

    public BigDecimal getSsl() {
        return ssl;
    }

    public void setSsl(BigDecimal ssl) {
        this.ssl = ssl;
    }

    public BigDecimal getZq() {
        return zq;
    }

    public void setZq(BigDecimal zq) {
        this.zq = zq;
    }

    public BigDecimal getYqye() {
        return yqye;
    }

    public void setYqye(BigDecimal yqye) {
        this.yqye = yqye;
    }

    public BigDecimal getYq1() {
        return yq1;
    }

    public void setYq1(BigDecimal yq1) {
        this.yq1 = yq1;
    }

    public BigDecimal getYq31() {
        return yq31;
    }

    public void setYq31(BigDecimal yq31) {
        this.yq31 = yq31;
    }

    public BigDecimal getYq61() {
        return yq61;
    }

    public void setYq61(BigDecimal yq61) {
        this.yq61 = yq61;
    }

    public BigDecimal getYq91() {
        return yq91;
    }

    public void setYq91(BigDecimal yq91) {
        this.yq91 = yq91;
    }

    public BigDecimal getYq181() {
        return yq181;
    }

    public void setYq181(BigDecimal yq181) {
        this.yq181 = yq181;
    }

    public BigDecimal getYq361() {
        return yq361;
    }

    public void setYq361(BigDecimal yq361) {
        this.yq361 = yq361;
    }
}