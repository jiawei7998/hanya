package com.singlee.hrbnewport.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

/**
 * 地方法人金融机构月度监测表（表二）
 */
@Table(name = "IFS_REPORT_DFFRJRJGYDJCBEE")
public class IfsReportDffrjrjgydjcbee implements Serializable {
    /**
     * 机构类别
     */
    @Column(name = "JGLB")
    private String jglb;

    /**
     * 机构名称
     */
    @Column(name = "JGMC")
    private String jgmc;

    /**
     * 同业情况-计财 （同业融入+同业存单）/总负债(%)
     */
    @Column(name = "TYQK_TRJTCCZFZ")
    private BigDecimal tyqkTrjtcczfz;

    /**
     * 同业情况-计财 最大单家同业融出比例(%)
     */
    @Column(name = "TYQK_ZDDJTCBL")
    private BigDecimal tyqkZddjtcbl;

    /**
     * 资产质量-授信、财会 逾期90天以上贷款占不良贷款比例(%)
     */
    @Column(name = "ZCZL_YQ90ZBLDKBL")
    private BigDecimal zczlYq90zbldkbl;

    /**
     * 资产质量-授信、财会 不良贷款率(%)
     */
    @Column(name = "ZCZL_BLDKL")
    private BigDecimal zczlBldkl;

    /**
     * 资产质量-授信、财会 贷款损失准备充足率(%)
     */
    @Column(name = "ZCZL_DKSSZBCZL")
    private BigDecimal zczlDksszbczl;

    /**
     * 资产质量-授信、财会 拨备覆盖率(%)
     */
    @Column(name = "ZCZL_BBFGL")
    private BigDecimal zczlBbfgl;

    /**
     * 资产质量-授信、财会 贷款拨备率(%)
     */
    @Column(name = "ZCZL_DKBBL")
    private BigDecimal zczlDkbbl;

    /**
     * 利润情况-财会 成本收入比(%)
     */
    @Column(name = "LRQK_CBSRB")
    private BigDecimal lrqkCbsrb;

    /**
     * 利润情况-财会 资本利润率(%)
     */
    @Column(name = "LRQK_ZBLRL")
    private BigDecimal lrqkZblrl;

    /**
     * 利润情况-财会 资产利润率(%)
     */
    @Column(name = "LRQK_ZCLRL")
    private BigDecimal lrqkZclrl;

    /**
     * 流动性-资负、财会 人民币超额备付金率(%)
     */
    @Column(name = "LDX_RMBCEBFJL")
    private BigDecimal ldxRmbcebfjl;

    /**
     * 流动性-资负、财会 存贷款比例(%)
     */
    @Column(name = "LDX_CDKBL")
    private BigDecimal ldxCdkbl;

    /**
     * 流动性-资负、财会 流动性比例(%)
     */
    @Column(name = "LDX_LDXBL")
    private BigDecimal ldxLdxbl;

    /**
     * 流动性-资负、财会 流动性覆盖率(%)
     */
    @Column(name = "LDX_LDXFGL")
    private BigDecimal ldxLdxfgl;

    /**
     * 流动性-资负、财会 净稳定资金比例(%)
     */
    @Column(name = "LDX_JWDZJBL")
    private BigDecimal ldxJwdzjbl;

    /**
     * 流动性-资负、财会 流动性缺口率(%)
     */
    @Column(name = "LDX_LDXQKL")
    private BigDecimal ldxLdxqkl;

    /**
     * 流动性-资负、财会 核心负债依存度(%)
     */
    @Column(name = "LDX_HXFZYCD")
    private BigDecimal ldxHxfzycd;

    /**
     * 流动性-资负、财会 流动性匹配率(%)
     */
    @Column(name = "LDX_LDXPPL")
    private BigDecimal ldxLdxppl;

    /**
     * 流动性-资负、财会 优质流动性资产充足率(%)
     */
    @Column(name = "LDX_YZLDXZCCZL")
    private BigDecimal ldxYzldxzcczl;

    /**
     * 集中度-授信、金融市场部 单一客户授信集中度(%)
     */
    @Column(name = "JZD_DYKHSXJZD")
    private BigDecimal jzdDykhsxjzd;

    /**
     * 集中度-授信、金融市场部 最大十家集团客户授信集中度(%)
     */
    @Column(name = "JZD_ZDSJJTKHSXJZD")
    private BigDecimal jzdZdsjjtkhsxjzd;

    /**
     * 集中度-授信、金融市场部 最大单家非同业单一客户风险暴露占一级资本净额的比例(%)
     */
    @Column(name = "JZD_ZDFTYFXBLZYJZBJEBL")
    private BigDecimal jzdZdftyfxblzyjzbjebl;

    /**
     * 集中度-授信、金融市场部 最大单家非同业单一客户贷款余额占资本净额的比例(%)
     */
    @Column(name = "JZD_ZDFTYDKYEZZBJEBL")
    private BigDecimal jzdZdftydkyezzbjebl;

    /**
     * 集中度-授信、金融市场部 最大单家同业单一客户风险暴露占一级资本净额的比例(%)
     */
    @Column(name = "JZD_ZDTYDYFXBLZYJZBJEBL")
    private BigDecimal jzdZdtydyfxblzyjzbjebl;

    /**
     * 集中度-授信、金融市场部 最大单家同业集团客户风险暴露占一级资本净额的比例(%)
     */
    @Column(name = "JZD_ZDTYJTFXBLZYJZBJEBL")
    private BigDecimal jzdZdtyjtfxblzyjzbjebl;

    /**
     * 资本充足情况-财会 资本充足率(%)
     */
    @Column(name = "ZBCZQK_ZBCZL")
    private BigDecimal zbczqkZbczl;

    /**
     * 资本充足情况-财会 一级资本充足率(%)
     */
    @Column(name = "ZBCZQK_YJZBCZL")
    private BigDecimal zbczqkYjzbczl;

    /**
     * 资本充足情况-财会 核心一级资本充足率(%)
     */
    @Column(name = "ZBCZQK_HXYJZBCZL")
    private BigDecimal zbczqkHxyjzbczl;

    /**
     * 资本充足情况-财会  杠杆率(%)
     */
    @Column(name = "ZBCZQK_GGL")
    private BigDecimal zbczqkGgl;

    private static final long serialVersionUID = 1L;

    /**
     * 获取机构类别
     *
     * @return JGLB - 机构类别
     */
    public String getJglb() {
        return jglb;
    }

    /**
     * 设置机构类别
     *
     * @param jglb 机构类别
     */
    public void setJglb(String jglb) {
        this.jglb = jglb;
    }

    /**
     * 获取机构名称
     *
     * @return JGMC - 机构名称
     */
    public String getJgmc() {
        return jgmc;
    }

    /**
     * 设置机构名称
     *
     * @param jgmc 机构名称
     */
    public void setJgmc(String jgmc) {
        this.jgmc = jgmc;
    }

    /**
     * 获取同业情况-计财 （同业融入+同业存单）/总负债(%)
     *
     * @return TYQK_TRJTCCZFZ - 同业情况-计财 （同业融入+同业存单）/总负债(%)
     */
    public BigDecimal getTyqkTrjtcczfz() {
        return tyqkTrjtcczfz;
    }

    /**
     * 设置同业情况-计财 （同业融入+同业存单）/总负债(%)
     *
     * @param tyqkTrjtcczfz 同业情况-计财 （同业融入+同业存单）/总负债(%)
     */
    public void setTyqkTrjtcczfz(BigDecimal tyqkTrjtcczfz) {
        this.tyqkTrjtcczfz = tyqkTrjtcczfz;
    }

    /**
     * 获取同业情况-计财 最大单家同业融出比例(%)
     *
     * @return TYQK_ZDDJTCBL - 同业情况-计财 最大单家同业融出比例(%)
     */
    public BigDecimal getTyqkZddjtcbl() {
        return tyqkZddjtcbl;
    }

    /**
     * 设置同业情况-计财 最大单家同业融出比例(%)
     *
     * @param tyqkZddjtcbl 同业情况-计财 最大单家同业融出比例(%)
     */
    public void setTyqkZddjtcbl(BigDecimal tyqkZddjtcbl) {
        this.tyqkZddjtcbl = tyqkZddjtcbl;
    }

    /**
     * 获取资产质量-授信、财会 逾期90天以上贷款占不良贷款比例(%)
     *
     * @return ZCZL_YQ90ZBLDKBL - 资产质量-授信、财会 逾期90天以上贷款占不良贷款比例(%)
     */
    public BigDecimal getZczlYq90zbldkbl() {
        return zczlYq90zbldkbl;
    }

    /**
     * 设置资产质量-授信、财会 逾期90天以上贷款占不良贷款比例(%)
     *
     * @param zczlYq90zbldkbl 资产质量-授信、财会 逾期90天以上贷款占不良贷款比例(%)
     */
    public void setZczlYq90zbldkbl(BigDecimal zczlYq90zbldkbl) {
        this.zczlYq90zbldkbl = zczlYq90zbldkbl;
    }

    /**
     * 获取资产质量-授信、财会 不良贷款率(%)
     *
     * @return ZCZL_BLDKL - 资产质量-授信、财会 不良贷款率(%)
     */
    public BigDecimal getZczlBldkl() {
        return zczlBldkl;
    }

    /**
     * 设置资产质量-授信、财会 不良贷款率(%)
     *
     * @param zczlBldkl 资产质量-授信、财会 不良贷款率(%)
     */
    public void setZczlBldkl(BigDecimal zczlBldkl) {
        this.zczlBldkl = zczlBldkl;
    }

    /**
     * 获取资产质量-授信、财会 贷款损失准备充足率(%)
     *
     * @return ZCZL_DKSSZBCZL - 资产质量-授信、财会 贷款损失准备充足率(%)
     */
    public BigDecimal getZczlDksszbczl() {
        return zczlDksszbczl;
    }

    /**
     * 设置资产质量-授信、财会 贷款损失准备充足率(%)
     *
     * @param zczlDksszbczl 资产质量-授信、财会 贷款损失准备充足率(%)
     */
    public void setZczlDksszbczl(BigDecimal zczlDksszbczl) {
        this.zczlDksszbczl = zczlDksszbczl;
    }

    /**
     * 获取资产质量-授信、财会 拨备覆盖率(%)
     *
     * @return ZCZL_BBFGL - 资产质量-授信、财会 拨备覆盖率(%)
     */
    public BigDecimal getZczlBbfgl() {
        return zczlBbfgl;
    }

    /**
     * 设置资产质量-授信、财会 拨备覆盖率(%)
     *
     * @param zczlBbfgl 资产质量-授信、财会 拨备覆盖率(%)
     */
    public void setZczlBbfgl(BigDecimal zczlBbfgl) {
        this.zczlBbfgl = zczlBbfgl;
    }

    /**
     * 获取资产质量-授信、财会 贷款拨备率(%)
     *
     * @return ZCZL_DKBBL - 资产质量-授信、财会 贷款拨备率(%)
     */
    public BigDecimal getZczlDkbbl() {
        return zczlDkbbl;
    }

    /**
     * 设置资产质量-授信、财会 贷款拨备率(%)
     *
     * @param zczlDkbbl 资产质量-授信、财会 贷款拨备率(%)
     */
    public void setZczlDkbbl(BigDecimal zczlDkbbl) {
        this.zczlDkbbl = zczlDkbbl;
    }

    /**
     * 获取利润情况-财会 成本收入比(%)
     *
     * @return LRQK_CBSRB - 利润情况-财会 成本收入比(%)
     */
    public BigDecimal getLrqkCbsrb() {
        return lrqkCbsrb;
    }

    /**
     * 设置利润情况-财会 成本收入比(%)
     *
     * @param lrqkCbsrb 利润情况-财会 成本收入比(%)
     */
    public void setLrqkCbsrb(BigDecimal lrqkCbsrb) {
        this.lrqkCbsrb = lrqkCbsrb;
    }

    /**
     * 获取利润情况-财会 资本利润率(%)
     *
     * @return LRQK_ZBLRL - 利润情况-财会 资本利润率(%)
     */
    public BigDecimal getLrqkZblrl() {
        return lrqkZblrl;
    }

    /**
     * 设置利润情况-财会 资本利润率(%)
     *
     * @param lrqkZblrl 利润情况-财会 资本利润率(%)
     */
    public void setLrqkZblrl(BigDecimal lrqkZblrl) {
        this.lrqkZblrl = lrqkZblrl;
    }

    /**
     * 获取利润情况-财会 资产利润率(%)
     *
     * @return LRQK_ZCLRL - 利润情况-财会 资产利润率(%)
     */
    public BigDecimal getLrqkZclrl() {
        return lrqkZclrl;
    }

    /**
     * 设置利润情况-财会 资产利润率(%)
     *
     * @param lrqkZclrl 利润情况-财会 资产利润率(%)
     */
    public void setLrqkZclrl(BigDecimal lrqkZclrl) {
        this.lrqkZclrl = lrqkZclrl;
    }

    /**
     * 获取流动性-资负、财会 人民币超额备付金率(%)
     *
     * @return LDX_RMBCEBFJL - 流动性-资负、财会 人民币超额备付金率(%)
     */
    public BigDecimal getLdxRmbcebfjl() {
        return ldxRmbcebfjl;
    }

    /**
     * 设置流动性-资负、财会 人民币超额备付金率(%)
     *
     * @param ldxRmbcebfjl 流动性-资负、财会 人民币超额备付金率(%)
     */
    public void setLdxRmbcebfjl(BigDecimal ldxRmbcebfjl) {
        this.ldxRmbcebfjl = ldxRmbcebfjl;
    }

    /**
     * 获取流动性-资负、财会 存贷款比例(%)
     *
     * @return LDX_CDKBL - 流动性-资负、财会 存贷款比例(%)
     */
    public BigDecimal getLdxCdkbl() {
        return ldxCdkbl;
    }

    /**
     * 设置流动性-资负、财会 存贷款比例(%)
     *
     * @param ldxCdkbl 流动性-资负、财会 存贷款比例(%)
     */
    public void setLdxCdkbl(BigDecimal ldxCdkbl) {
        this.ldxCdkbl = ldxCdkbl;
    }

    /**
     * 获取流动性-资负、财会 流动性比例(%)
     *
     * @return LDX_LDXBL - 流动性-资负、财会 流动性比例(%)
     */
    public BigDecimal getLdxLdxbl() {
        return ldxLdxbl;
    }

    /**
     * 设置流动性-资负、财会 流动性比例(%)
     *
     * @param ldxLdxbl 流动性-资负、财会 流动性比例(%)
     */
    public void setLdxLdxbl(BigDecimal ldxLdxbl) {
        this.ldxLdxbl = ldxLdxbl;
    }

    /**
     * 获取流动性-资负、财会 流动性覆盖率(%)
     *
     * @return LDX_LDXFGL - 流动性-资负、财会 流动性覆盖率(%)
     */
    public BigDecimal getLdxLdxfgl() {
        return ldxLdxfgl;
    }

    /**
     * 设置流动性-资负、财会 流动性覆盖率(%)
     *
     * @param ldxLdxfgl 流动性-资负、财会 流动性覆盖率(%)
     */
    public void setLdxLdxfgl(BigDecimal ldxLdxfgl) {
        this.ldxLdxfgl = ldxLdxfgl;
    }

    /**
     * 获取流动性-资负、财会 净稳定资金比例(%)
     *
     * @return LDX_JWDZJBL - 流动性-资负、财会 净稳定资金比例(%)
     */
    public BigDecimal getLdxJwdzjbl() {
        return ldxJwdzjbl;
    }

    /**
     * 设置流动性-资负、财会 净稳定资金比例(%)
     *
     * @param ldxJwdzjbl 流动性-资负、财会 净稳定资金比例(%)
     */
    public void setLdxJwdzjbl(BigDecimal ldxJwdzjbl) {
        this.ldxJwdzjbl = ldxJwdzjbl;
    }

    /**
     * 获取流动性-资负、财会 流动性缺口率(%)
     *
     * @return LDX_LDXQKL - 流动性-资负、财会 流动性缺口率(%)
     */
    public BigDecimal getLdxLdxqkl() {
        return ldxLdxqkl;
    }

    /**
     * 设置流动性-资负、财会 流动性缺口率(%)
     *
     * @param ldxLdxqkl 流动性-资负、财会 流动性缺口率(%)
     */
    public void setLdxLdxqkl(BigDecimal ldxLdxqkl) {
        this.ldxLdxqkl = ldxLdxqkl;
    }

    /**
     * 获取流动性-资负、财会 核心负债依存度(%)
     *
     * @return LDX_HXFZYCD - 流动性-资负、财会 核心负债依存度(%)
     */
    public BigDecimal getLdxHxfzycd() {
        return ldxHxfzycd;
    }

    /**
     * 设置流动性-资负、财会 核心负债依存度(%)
     *
     * @param ldxHxfzycd 流动性-资负、财会 核心负债依存度(%)
     */
    public void setLdxHxfzycd(BigDecimal ldxHxfzycd) {
        this.ldxHxfzycd = ldxHxfzycd;
    }

    /**
     * 获取流动性-资负、财会 流动性匹配率(%)
     *
     * @return LDX_LDXPPL - 流动性-资负、财会 流动性匹配率(%)
     */
    public BigDecimal getLdxLdxppl() {
        return ldxLdxppl;
    }

    /**
     * 设置流动性-资负、财会 流动性匹配率(%)
     *
     * @param ldxLdxppl 流动性-资负、财会 流动性匹配率(%)
     */
    public void setLdxLdxppl(BigDecimal ldxLdxppl) {
        this.ldxLdxppl = ldxLdxppl;
    }

    /**
     * 获取流动性-资负、财会 优质流动性资产充足率(%)
     *
     * @return LDX_YZLDXZCCZL - 流动性-资负、财会 优质流动性资产充足率(%)
     */
    public BigDecimal getLdxYzldxzcczl() {
        return ldxYzldxzcczl;
    }

    /**
     * 设置流动性-资负、财会 优质流动性资产充足率(%)
     *
     * @param ldxYzldxzcczl 流动性-资负、财会 优质流动性资产充足率(%)
     */
    public void setLdxYzldxzcczl(BigDecimal ldxYzldxzcczl) {
        this.ldxYzldxzcczl = ldxYzldxzcczl;
    }

    /**
     * 获取集中度-授信、金融市场部 单一客户授信集中度(%)
     *
     * @return JZD_DYKHSXJZD - 集中度-授信、金融市场部 单一客户授信集中度(%)
     */
    public BigDecimal getJzdDykhsxjzd() {
        return jzdDykhsxjzd;
    }

    /**
     * 设置集中度-授信、金融市场部 单一客户授信集中度(%)
     *
     * @param jzdDykhsxjzd 集中度-授信、金融市场部 单一客户授信集中度(%)
     */
    public void setJzdDykhsxjzd(BigDecimal jzdDykhsxjzd) {
        this.jzdDykhsxjzd = jzdDykhsxjzd;
    }

    /**
     * 获取集中度-授信、金融市场部 最大十家集团客户授信集中度(%)
     *
     * @return JZD_ZDSJJTKHSXJZD - 集中度-授信、金融市场部 最大十家集团客户授信集中度(%)
     */
    public BigDecimal getJzdZdsjjtkhsxjzd() {
        return jzdZdsjjtkhsxjzd;
    }

    /**
     * 设置集中度-授信、金融市场部 最大十家集团客户授信集中度(%)
     *
     * @param jzdZdsjjtkhsxjzd 集中度-授信、金融市场部 最大十家集团客户授信集中度(%)
     */
    public void setJzdZdsjjtkhsxjzd(BigDecimal jzdZdsjjtkhsxjzd) {
        this.jzdZdsjjtkhsxjzd = jzdZdsjjtkhsxjzd;
    }

    /**
     * 获取集中度-授信、金融市场部 最大单家非同业单一客户风险暴露占一级资本净额的比例(%)
     *
     * @return JZD_ZDFTYFXBLZYJZBJEBL - 集中度-授信、金融市场部 最大单家非同业单一客户风险暴露占一级资本净额的比例(%)
     */
    public BigDecimal getJzdZdftyfxblzyjzbjebl() {
        return jzdZdftyfxblzyjzbjebl;
    }

    /**
     * 设置集中度-授信、金融市场部 最大单家非同业单一客户风险暴露占一级资本净额的比例(%)
     *
     * @param jzdZdftyfxblzyjzbjebl 集中度-授信、金融市场部 最大单家非同业单一客户风险暴露占一级资本净额的比例(%)
     */
    public void setJzdZdftyfxblzyjzbjebl(BigDecimal jzdZdftyfxblzyjzbjebl) {
        this.jzdZdftyfxblzyjzbjebl = jzdZdftyfxblzyjzbjebl;
    }

    /**
     * 获取集中度-授信、金融市场部 最大单家非同业单一客户贷款余额占资本净额的比例(%)
     *
     * @return JZD_ZDFTYDKYEZZBJEBL - 集中度-授信、金融市场部 最大单家非同业单一客户贷款余额占资本净额的比例(%)
     */
    public BigDecimal getJzdZdftydkyezzbjebl() {
        return jzdZdftydkyezzbjebl;
    }

    /**
     * 设置集中度-授信、金融市场部 最大单家非同业单一客户贷款余额占资本净额的比例(%)
     *
     * @param jzdZdftydkyezzbjebl 集中度-授信、金融市场部 最大单家非同业单一客户贷款余额占资本净额的比例(%)
     */
    public void setJzdZdftydkyezzbjebl(BigDecimal jzdZdftydkyezzbjebl) {
        this.jzdZdftydkyezzbjebl = jzdZdftydkyezzbjebl;
    }

    /**
     * 获取集中度-授信、金融市场部 最大单家同业单一客户风险暴露占一级资本净额的比例(%)
     *
     * @return JZD_ZDTYDYFXBLZYJZBJEBL - 集中度-授信、金融市场部 最大单家同业单一客户风险暴露占一级资本净额的比例(%)
     */
    public BigDecimal getJzdZdtydyfxblzyjzbjebl() {
        return jzdZdtydyfxblzyjzbjebl;
    }

    /**
     * 设置集中度-授信、金融市场部 最大单家同业单一客户风险暴露占一级资本净额的比例(%)
     *
     * @param jzdZdtydyfxblzyjzbjebl 集中度-授信、金融市场部 最大单家同业单一客户风险暴露占一级资本净额的比例(%)
     */
    public void setJzdZdtydyfxblzyjzbjebl(BigDecimal jzdZdtydyfxblzyjzbjebl) {
        this.jzdZdtydyfxblzyjzbjebl = jzdZdtydyfxblzyjzbjebl;
    }

    /**
     * 获取集中度-授信、金融市场部 最大单家同业集团客户风险暴露占一级资本净额的比例(%)
     *
     * @return JZD_ZDTYJTFXBLZYJZBJEBL - 集中度-授信、金融市场部 最大单家同业集团客户风险暴露占一级资本净额的比例(%)
     */
    public BigDecimal getJzdZdtyjtfxblzyjzbjebl() {
        return jzdZdtyjtfxblzyjzbjebl;
    }

    /**
     * 设置集中度-授信、金融市场部 最大单家同业集团客户风险暴露占一级资本净额的比例(%)
     *
     * @param jzdZdtyjtfxblzyjzbjebl 集中度-授信、金融市场部 最大单家同业集团客户风险暴露占一级资本净额的比例(%)
     */
    public void setJzdZdtyjtfxblzyjzbjebl(BigDecimal jzdZdtyjtfxblzyjzbjebl) {
        this.jzdZdtyjtfxblzyjzbjebl = jzdZdtyjtfxblzyjzbjebl;
    }

    /**
     * 获取资本充足情况-财会 资本充足率(%)
     *
     * @return ZBCZQK_ZBCZL - 资本充足情况-财会 资本充足率(%)
     */
    public BigDecimal getZbczqkZbczl() {
        return zbczqkZbczl;
    }

    /**
     * 设置资本充足情况-财会 资本充足率(%)
     *
     * @param zbczqkZbczl 资本充足情况-财会 资本充足率(%)
     */
    public void setZbczqkZbczl(BigDecimal zbczqkZbczl) {
        this.zbczqkZbczl = zbczqkZbczl;
    }

    /**
     * 获取资本充足情况-财会 一级资本充足率(%)
     *
     * @return ZBCZQK_YJZBCZL - 资本充足情况-财会 一级资本充足率(%)
     */
    public BigDecimal getZbczqkYjzbczl() {
        return zbczqkYjzbczl;
    }

    /**
     * 设置资本充足情况-财会 一级资本充足率(%)
     *
     * @param zbczqkYjzbczl 资本充足情况-财会 一级资本充足率(%)
     */
    public void setZbczqkYjzbczl(BigDecimal zbczqkYjzbczl) {
        this.zbczqkYjzbczl = zbczqkYjzbczl;
    }

    /**
     * 获取资本充足情况-财会 核心一级资本充足率(%)
     *
     * @return ZBCZQK_HXYJZBCZL - 资本充足情况-财会 核心一级资本充足率(%)
     */
    public BigDecimal getZbczqkHxyjzbczl() {
        return zbczqkHxyjzbczl;
    }

    /**
     * 设置资本充足情况-财会 核心一级资本充足率(%)
     *
     * @param zbczqkHxyjzbczl 资本充足情况-财会 核心一级资本充足率(%)
     */
    public void setZbczqkHxyjzbczl(BigDecimal zbczqkHxyjzbczl) {
        this.zbczqkHxyjzbczl = zbczqkHxyjzbczl;
    }

    /**
     * 获取资本充足情况-财会  杠杆率(%)
     *
     * @return ZBCZQK_GGL - 资本充足情况-财会  杠杆率(%)
     */
    public BigDecimal getZbczqkGgl() {
        return zbczqkGgl;
    }

    /**
     * 设置资本充足情况-财会  杠杆率(%)
     *
     * @param zbczqkGgl 资本充足情况-财会  杠杆率(%)
     */
    public void setZbczqkGgl(BigDecimal zbczqkGgl) {
        this.zbczqkGgl = zbczqkGgl;
    }
}