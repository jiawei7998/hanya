package com.singlee.hrbnewport.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

/**
 * 债券借贷收入比较情况表
 */
@Table(name = "IFS_REPORT_JRSCBYWGL_ZQJDSRBJ")
public class IfsReportJrscbywglZqjdsrbj implements Serializable {
    /**
     * 月度
     */
    @Column(name = "YD")
    private String yd;

    /**
     * 融出_交易量
     */
    @Column(name = "RC_JYL")
    private BigDecimal rcJyl;

    /**
     * 融出_费用
     */
    @Column(name = "RC_FY")
    private BigDecimal rcFy;

    /**
     * 融入_到期月
     */
    @Column(name = "RR_JYL")
    private BigDecimal rrJyl;

    /**
     * 融入到期月
     */
    @Column(name = "RR_FY")
    private BigDecimal rrFy;

    /**
     * 净收入
     */
    @Column(name = "JSR")
    private BigDecimal jsr;

    private static final long serialVersionUID = 1L;

    /**
     * 获取月度
     *
     * @return YD - 月度
     */
    public String getYd() {
        return yd;
    }

    /**
     * 设置月度
     *
     * @param yd 月度
     */
    public void setYd(String yd) {
        this.yd = yd;
    }

    /**
     * 获取融出_交易量
     *
     * @return RC_JYL - 融出_交易量
     */
    public BigDecimal getRcJyl() {
        return rcJyl;
    }

    /**
     * 设置融出_交易量
     *
     * @param rcJyl 融出_交易量
     */
    public void setRcJyl(BigDecimal rcJyl) {
        this.rcJyl = rcJyl;
    }

    /**
     * 获取融出_费用
     *
     * @return RC_FY - 融出_费用
     */
    public BigDecimal getRcFy() {
        return rcFy;
    }

    /**
     * 设置融出_费用
     *
     * @param rcFy 融出_费用
     */
    public void setRcFy(BigDecimal rcFy) {
        this.rcFy = rcFy;
    }

    /**
     * 获取融入_到期月
     *
     * @return RR_JYL - 融入_到期月
     */
    public BigDecimal getRrJyl() {
        return rrJyl;
    }

    /**
     * 设置融入_到期月
     *
     * @param rrJyl 融入_到期月
     */
    public void setRrJyl(BigDecimal rrJyl) {
        this.rrJyl = rrJyl;
    }

    /**
     * 获取融入到期月
     *
     * @return RR_FY - 融入到期月
     */
    public BigDecimal getRrFy() {
        return rrFy;
    }

    /**
     * 设置融入到期月
     *
     * @param rrFy 融入到期月
     */
    public void setRrFy(BigDecimal rrFy) {
        this.rrFy = rrFy;
    }

    /**
     * 获取净收入
     *
     * @return JSR - 净收入
     */
    public BigDecimal getJsr() {
        return jsr;
    }

    /**
     * 设置净收入
     *
     * @param jsr 净收入
     */
    public void setJsr(BigDecimal jsr) {
        this.jsr = jsr;
    }
}