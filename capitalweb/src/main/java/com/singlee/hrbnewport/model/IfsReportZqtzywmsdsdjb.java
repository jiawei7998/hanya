package com.singlee.hrbnewport.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

/**
 * 债券投资业务免所得税登记表
 */
@Table(name = "IFS_REPORT_ZQTZYWMSDSDJB")
public class IfsReportZqtzywmsdsdjb implements Serializable {
    /**
     * 交易序号
     */
    @Column(name = "JJXH")
    private String jjxh;

    /**
     * 账户类型
     */
    @Column(name = "ZHLX")
    private String zhlx;

    /**
     * 债券类型
     */
    @Column(name = "ZQLX")
    private String zqlx;

    /**
     * 债券代码
     */
    @Column(name = "ZQDM")
    private String zqdm;

    /**
     * 债券名称
     */
    @Column(name = "ZQMC")
    private String zqmc;

    /**
     * 期限
     */
    @Column(name = "QX")
    private String qx;

    /**
     * 债券起息日
     */
    @Column(name = "ZQQXR")
    private String zqqxr;

    /**
     * 债券到期日
     */
    @Column(name = "ZQDQR")
    private String zqdqr;

    /**
     * 票面利率_固定利率
     */
    @Column(name = "PMLV_GDLV")
    private BigDecimal pmlvGdlv;

    /**
     * 票面利率_浮动利率
     */
    @Column(name = "PMLV_FDLV")
    private BigDecimal pmlvFdlv;

    /**
     * 付息频率
     */
    @Column(name = "FXPL")
    private String fxpl;

    /**
     * 付息日期
     */
    @Column(name = "FXRQ")
    private String fxrq;

    /**
     * 买入日期
     */
    @Column(name = "MRRQ")
    private String mrrq;

    /**
     * 买入票面金额
     */
    @Column(name = "MRPMJE")
    private BigDecimal mrpmje;

    /**
     * 买入全价
     */
    @Column(name = "MRQJ")
    private BigDecimal mrqj;

    /**
     * 买入净价
     */
    @Column(name = "MRJJ")
    private BigDecimal mrjj;

    /**
     * 买入应计利息
     */
    @Column(name = "MRYJLX")
    private BigDecimal mryjlx;

    /**
     * 卖出日期
     */
    @Column(name = "MCRQ")
    private String mcrq;

    /**
     * 卖出票面金额
     */
    @Column(name = "MCPMJE")
    private BigDecimal mcpmje;

    /**
     * 卖出全价
     */
    @Column(name = "MCQJ")
    private BigDecimal mcqj;

    /**
     * 卖出净价
     */
    @Column(name = "MCJJ")
    private BigDecimal mcjj;

    /**
     * 卖出应计利息
     */
    @Column(name = "MCYJLX")
    private BigDecimal mcyjlx;

    /**
     * 实际收息日期
     */
    @Column(name = "SJSXRQ")
    private String sjsxrq;

    /**
     * 实际收息金额
     */
    @Column(name = "SJSXJE")
    private BigDecimal sjsxje;

    /**
     * 当年实收利息
     */
    @Column(name = "DNSSLX")
    private BigDecimal dnsslx;

    /**
     * 免所得税国债利息收入
     */
    @Column(name = "MSDSGZLXSR")
    private BigDecimal msdsgzlxsr;

    /**
     * 实际免利息收入
     */
    @Column(name = "SJMLXSR")
    private BigDecimal sjmlxsr;

    private static final long serialVersionUID = 1L;

    /**
     * 获取交易序号
     *
     * @return JJXH - 交易序号
     */
    public String getJjxh() {
        return jjxh;
    }

    /**
     * 设置交易序号
     *
     * @param jjxh 交易序号
     */
    public void setJjxh(String jjxh) {
        this.jjxh = jjxh;
    }

    /**
     * 获取账户类型
     *
     * @return ZHLX - 账户类型
     */
    public String getZhlx() {
        return zhlx;
    }

    /**
     * 设置账户类型
     *
     * @param zhlx 账户类型
     */
    public void setZhlx(String zhlx) {
        this.zhlx = zhlx;
    }

    /**
     * 获取债券类型
     *
     * @return ZQLX - 债券类型
     */
    public String getZqlx() {
        return zqlx;
    }

    /**
     * 设置债券类型
     *
     * @param zqlx 债券类型
     */
    public void setZqlx(String zqlx) {
        this.zqlx = zqlx;
    }

    /**
     * 获取债券代码
     *
     * @return ZQDM - 债券代码
     */
    public String getZqdm() {
        return zqdm;
    }

    /**
     * 设置债券代码
     *
     * @param zqdm 债券代码
     */
    public void setZqdm(String zqdm) {
        this.zqdm = zqdm;
    }

    /**
     * 获取债券名称
     *
     * @return ZQMC - 债券名称
     */
    public String getZqmc() {
        return zqmc;
    }

    /**
     * 设置债券名称
     *
     * @param zqmc 债券名称
     */
    public void setZqmc(String zqmc) {
        this.zqmc = zqmc;
    }

    /**
     * 获取期限
     *
     * @return QX - 期限
     */
    public String getQx() {
        return qx;
    }

    /**
     * 设置期限
     *
     * @param qx 期限
     */
    public void setQx(String qx) {
        this.qx = qx;
    }

    /**
     * 获取债券起息日
     *
     * @return ZQQXR - 债券起息日
     */
    public String getZqqxr() {
        return zqqxr;
    }

    /**
     * 设置债券起息日
     *
     * @param zqqxr 债券起息日
     */
    public void setZqqxr(String zqqxr) {
        this.zqqxr = zqqxr;
    }

    /**
     * 获取债券到期日
     *
     * @return ZQDQR - 债券到期日
     */
    public String getZqdqr() {
        return zqdqr;
    }

    /**
     * 设置债券到期日
     *
     * @param zqdqr 债券到期日
     */
    public void setZqdqr(String zqdqr) {
        this.zqdqr = zqdqr;
    }

    /**
     * 获取票面利率_固定利率
     *
     * @return PMLV_GDLV - 票面利率_固定利率
     */
    public BigDecimal getPmlvGdlv() {
        return pmlvGdlv;
    }

    /**
     * 设置票面利率_固定利率
     *
     * @param pmlvGdlv 票面利率_固定利率
     */
    public void setPmlvGdlv(BigDecimal pmlvGdlv) {
        this.pmlvGdlv = pmlvGdlv;
    }

    /**
     * 获取票面利率_浮动利率
     *
     * @return PMLV_FDLV - 票面利率_浮动利率
     */
    public BigDecimal getPmlvFdlv() {
        return pmlvFdlv;
    }

    /**
     * 设置票面利率_浮动利率
     *
     * @param pmlvFdlv 票面利率_浮动利率
     */
    public void setPmlvFdlv(BigDecimal pmlvFdlv) {
        this.pmlvFdlv = pmlvFdlv;
    }

    /**
     * 获取付息频率
     *
     * @return FXPL - 付息频率
     */
    public String getFxpl() {
        return fxpl;
    }

    /**
     * 设置付息频率
     *
     * @param fxpl 付息频率
     */
    public void setFxpl(String fxpl) {
        this.fxpl = fxpl;
    }

    /**
     * 获取付息日期
     *
     * @return FXRQ - 付息日期
     */
    public String getFxrq() {
        return fxrq;
    }

    /**
     * 设置付息日期
     *
     * @param fxrq 付息日期
     */
    public void setFxrq(String fxrq) {
        this.fxrq = fxrq;
    }

    /**
     * 获取买入日期
     *
     * @return MRRQ - 买入日期
     */
    public String getMrrq() {
        return mrrq;
    }

    /**
     * 设置买入日期
     *
     * @param mrrq 买入日期
     */
    public void setMrrq(String mrrq) {
        this.mrrq = mrrq;
    }

    /**
     * 获取买入票面金额
     *
     * @return MRPMJE - 买入票面金额
     */
    public BigDecimal getMrpmje() {
        return mrpmje;
    }

    /**
     * 设置买入票面金额
     *
     * @param mrpmje 买入票面金额
     */
    public void setMrpmje(BigDecimal mrpmje) {
        this.mrpmje = mrpmje;
    }

    /**
     * 获取买入全价
     *
     * @return MRQJ - 买入全价
     */
    public BigDecimal getMrqj() {
        return mrqj;
    }

    /**
     * 设置买入全价
     *
     * @param mrqj 买入全价
     */
    public void setMrqj(BigDecimal mrqj) {
        this.mrqj = mrqj;
    }

    /**
     * 获取买入净价
     *
     * @return MRJJ - 买入净价
     */
    public BigDecimal getMrjj() {
        return mrjj;
    }

    /**
     * 设置买入净价
     *
     * @param mrjj 买入净价
     */
    public void setMrjj(BigDecimal mrjj) {
        this.mrjj = mrjj;
    }

    /**
     * 获取买入应计利息
     *
     * @return MRYJLX - 买入应计利息
     */
    public BigDecimal getMryjlx() {
        return mryjlx;
    }

    /**
     * 设置买入应计利息
     *
     * @param mryjlx 买入应计利息
     */
    public void setMryjlx(BigDecimal mryjlx) {
        this.mryjlx = mryjlx;
    }

    /**
     * 获取卖出日期
     *
     * @return MCRQ - 卖出日期
     */
    public String getMcrq() {
        return mcrq;
    }

    /**
     * 设置卖出日期
     *
     * @param mcrq 卖出日期
     */
    public void setMcrq(String mcrq) {
        this.mcrq = mcrq;
    }

    /**
     * 获取卖出票面金额
     *
     * @return MCPMJE - 卖出票面金额
     */
    public BigDecimal getMcpmje() {
        return mcpmje;
    }

    /**
     * 设置卖出票面金额
     *
     * @param mcpmje 卖出票面金额
     */
    public void setMcpmje(BigDecimal mcpmje) {
        this.mcpmje = mcpmje;
    }

    /**
     * 获取卖出全价
     *
     * @return MCQJ - 卖出全价
     */
    public BigDecimal getMcqj() {
        return mcqj;
    }

    /**
     * 设置卖出全价
     *
     * @param mcqj 卖出全价
     */
    public void setMcqj(BigDecimal mcqj) {
        this.mcqj = mcqj;
    }

    /**
     * 获取卖出净价
     *
     * @return MCJJ - 卖出净价
     */
    public BigDecimal getMcjj() {
        return mcjj;
    }

    /**
     * 设置卖出净价
     *
     * @param mcjj 卖出净价
     */
    public void setMcjj(BigDecimal mcjj) {
        this.mcjj = mcjj;
    }

    /**
     * 获取卖出应计利息
     *
     * @return MCYJLX - 卖出应计利息
     */
    public BigDecimal getMcyjlx() {
        return mcyjlx;
    }

    /**
     * 设置卖出应计利息
     *
     * @param mcyjlx 卖出应计利息
     */
    public void setMcyjlx(BigDecimal mcyjlx) {
        this.mcyjlx = mcyjlx;
    }

    /**
     * 获取实际收息日期
     *
     * @return SJSXRQ - 实际收息日期
     */
    public String getSjsxrq() {
        return sjsxrq;
    }

    /**
     * 设置实际收息日期
     *
     * @param sjsxrq 实际收息日期
     */
    public void setSjsxrq(String sjsxrq) {
        this.sjsxrq = sjsxrq;
    }

    /**
     * 获取实际收息金额
     *
     * @return SJSXJE - 实际收息金额
     */
    public BigDecimal getSjsxje() {
        return sjsxje;
    }

    /**
     * 设置实际收息金额
     *
     * @param sjsxje 实际收息金额
     */
    public void setSjsxje(BigDecimal sjsxje) {
        this.sjsxje = sjsxje;
    }

    /**
     * 获取当年实收利息
     *
     * @return DNSSLX - 当年实收利息
     */
    public BigDecimal getDnsslx() {
        return dnsslx;
    }

    /**
     * 设置当年实收利息
     *
     * @param dnsslx 当年实收利息
     */
    public void setDnsslx(BigDecimal dnsslx) {
        this.dnsslx = dnsslx;
    }

    /**
     * 获取免所得税国债利息收入
     *
     * @return MSDSGZLXSR - 免所得税国债利息收入
     */
    public BigDecimal getMsdsgzlxsr() {
        return msdsgzlxsr;
    }

    /**
     * 设置免所得税国债利息收入
     *
     * @param msdsgzlxsr 免所得税国债利息收入
     */
    public void setMsdsgzlxsr(BigDecimal msdsgzlxsr) {
        this.msdsgzlxsr = msdsgzlxsr;
    }

    /**
     * 获取实际免利息收入
     *
     * @return SJMLXSR - 实际免利息收入
     */
    public BigDecimal getSjmlxsr() {
        return sjmlxsr;
    }

    /**
     * 设置实际免利息收入
     *
     * @param sjmlxsr 实际免利息收入
     */
    public void setSjmlxsr(BigDecimal sjmlxsr) {
        this.sjmlxsr = sjmlxsr;
    }
}