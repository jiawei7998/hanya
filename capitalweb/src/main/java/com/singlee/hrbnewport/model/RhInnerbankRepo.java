package com.singlee.hrbnewport.model;

import java.math.BigDecimal;

/**
 *
 * 2021/12/9
 * @Auther:zk
 */    
    
/**
    * 同业业务管理报告-卖出回购债券业务明细表
    */
public class RhInnerbankRepo {
    /**
    * 序号
    */
    private String seq;

    /**
    * 交易方向
    */
    private String dealDir;

    /**
    * 交易对手
    */
    private String cust;

    /**
    * 首次结算日
    */
    private String vdate;

    /**
    * 到期结算日
    */
    private String mdate;

    /**
    * 实际占款天数
    */
    private String tenor;

    /**
    * 回购利率
    */
    private BigDecimal repoRate;

    /**
    * 交易金额
    */
    private BigDecimal amt;

    public String getSeq() {
        return seq;
    }

    public void setSeq(String seq) {
        this.seq = seq;
    }

    public String getDealDir() {
        return dealDir;
    }

    public void setDealDir(String dealDir) {
        this.dealDir = dealDir;
    }

    public String getCust() {
        return cust;
    }

    public void setCust(String cust) {
        this.cust = cust;
    }

    public String getVdate() {
        return vdate;
    }

    public void setVdate(String vdate) {
        this.vdate = vdate;
    }

    public String getMdate() {
        return mdate;
    }

    public void setMdate(String mdate) {
        this.mdate = mdate;
    }

    public String getTenor() {
        return tenor;
    }

    public void setTenor(String tenor) {
        this.tenor = tenor;
    }

    public BigDecimal getRepoRate() {
        return repoRate;
    }

    public void setRepoRate(BigDecimal repoRate) {
        this.repoRate = repoRate;
    }

    public BigDecimal getAmt() {
        return amt;
    }

    public void setAmt(BigDecimal amt) {
        this.amt = amt;
    }
}