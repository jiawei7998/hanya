package com.singlee.hrbnewport.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author ：meigy
 * @date ：Created in 2021/11/10 13:51
 * @description：${description}
 * @modified By：
 * @version:
 */

/**
 * 表内外投资业务明细表（穿透后）
 */
@Entity
@Table(name = "IFS_REPORT_CSHBNWTZ_MXCTH")
public class IfsReportCshbnwtzMxcth implements Serializable {
    /**
     * 表外理财资产_资金投向
     */
    private String bwZjtx;

    /**
     * 表外理财资产_本季末数额
     */
    private BigDecimal bwBjmAmt;

    /**
     * 表外理财资产_上季末数额
     */
    private BigDecimal bwSjmAmt;

    /**
     * 表外理财资产_备注
     */
    private String bwBz;

    /**
     * 表内理财资产_资金投向
     */
    private String bnZjtx;

    /**
     * 表内理财资产_本季末数额
     */
    private BigDecimal bnBjmAmt;

    /**
     * 表内理财资产_上季末数额
     */
    private BigDecimal bnSjmAmt;

    /**
     * 表内理财资产_备注
     */
    private String bnBz;

    private String parentZjtx;

    private static final long serialVersionUID = 1L;

    public String getBwZjtx() {
        return bwZjtx;
    }

    public void setBwZjtx(String bwZjtx) {
        this.bwZjtx = bwZjtx;
    }

    public BigDecimal getBwBjmAmt() {
        return bwBjmAmt;
    }

    public void setBwBjmAmt(BigDecimal bwBjmAmt) {
        this.bwBjmAmt = bwBjmAmt;
    }

    public BigDecimal getBwSjmAmt() {
        return bwSjmAmt;
    }

    public void setBwSjmAmt(BigDecimal bwSjmAmt) {
        this.bwSjmAmt = bwSjmAmt;
    }

    public String getBwBz() {
        return bwBz;
    }

    public void setBwBz(String bwBz) {
        this.bwBz = bwBz;
    }

    public String getBnZjtx() {
        return bnZjtx;
    }

    public void setBnZjtx(String bnZjtx) {
        this.bnZjtx = bnZjtx;
    }

    public BigDecimal getBnBjmAmt() {
        return bnBjmAmt;
    }

    public void setBnBjmAmt(BigDecimal bnBjmAmt) {
        this.bnBjmAmt = bnBjmAmt;
    }

    public BigDecimal getBnSjmAmt() {
        return bnSjmAmt;
    }

    public void setBnSjmAmt(BigDecimal bnSjmAmt) {
        this.bnSjmAmt = bnSjmAmt;
    }

    public String getBnBz() {
        return bnBz;
    }

    public void setBnBz(String bnBz) {
        this.bnBz = bnBz;
    }

    public String getParentZjtx() {
        return parentZjtx;
    }

    public void setParentZjtx(String parentZjtx) {
        this.parentZjtx = parentZjtx;
    }
}