package com.singlee.hrbnewport.model;

import java.math.BigDecimal;

public class TbRateSwap {
    private String dealno;

    private String accountType;

    private String dealType;

    private String dealDate;

    private String vdate;

    private String firstDate;

    private String mdate;

    private BigDecimal timeLimit;

    private BigDecimal yearLimit;

    private String direction;

    private BigDecimal notionalPrincipal;

    private String cno;

    private String cnoCreditGrade;

    private String interestDay;

    private String clearType;

    private BigDecimal fixedRate;

    private String fixedRatePayCycle;

    private String floatRateWay;

    private BigDecimal referRate;

    private String resetFrequency;

    private String floatRatePayCycle;

    private BigDecimal valueamt;

    public String getDealno() {
        return dealno;
    }

    public void setDealno(String dealno) {
        this.dealno = dealno;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getDealType() {
        return dealType;
    }

    public void setDealType(String dealType) {
        this.dealType = dealType;
    }

    public String getDealDate() {
        return dealDate;
    }

    public void setDealDate(String dealDate) {
        this.dealDate = dealDate;
    }

    public String getVdate() {
        return vdate;
    }

    public void setVdate(String vdate) {
        this.vdate = vdate;
    }

    public String getFirstDate() {
        return firstDate;
    }

    public void setFirstDate(String firstDate) {
        this.firstDate = firstDate;
    }

    public String getMdate() {
        return mdate;
    }

    public void setMdate(String mdate) {
        this.mdate = mdate;
    }

    public BigDecimal getTimeLimit() {
        return timeLimit;
    }

    public void setTimeLimit(BigDecimal timeLimit) {
        this.timeLimit = timeLimit;
    }

    public BigDecimal getYearLimit() {
        return yearLimit;
    }

    public void setYearLimit(BigDecimal yearLimit) {
        this.yearLimit = yearLimit;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public BigDecimal getNotionalPrincipal() {
        return notionalPrincipal;
    }

    public void setNotionalPrincipal(BigDecimal notionalPrincipal) {
        this.notionalPrincipal = notionalPrincipal;
    }

    public String getCno() {
        return cno;
    }

    public void setCno(String cno) {
        this.cno = cno;
    }

    public String getCnoCreditGrade() {
        return cnoCreditGrade;
    }

    public void setCnoCreditGrade(String cnoCreditGrade) {
        this.cnoCreditGrade = cnoCreditGrade;
    }

    public String getInterestDay() {
        return interestDay;
    }

    public void setInterestDay(String interestDay) {
        this.interestDay = interestDay;
    }

    public String getClearType() {
        return clearType;
    }

    public void setClearType(String clearType) {
        this.clearType = clearType;
    }

    public BigDecimal getFixedRate() {
        return fixedRate;
    }

    public void setFixedRate(BigDecimal fixedRate) {
        this.fixedRate = fixedRate;
    }

    public String getFixedRatePayCycle() {
        return fixedRatePayCycle;
    }

    public void setFixedRatePayCycle(String fixedRatePayCycle) {
        this.fixedRatePayCycle = fixedRatePayCycle;
    }

    public String getFloatRateWay() {
        return floatRateWay;
    }

    public void setFloatRateWay(String floatRateWay) {
        this.floatRateWay = floatRateWay;
    }

    public BigDecimal getReferRate() {
        return referRate;
    }

    public void setReferRate(BigDecimal referRate) {
        this.referRate = referRate;
    }

    public String getResetFrequency() {
        return resetFrequency;
    }

    public void setResetFrequency(String resetFrequency) {
        this.resetFrequency = resetFrequency;
    }

    public String getFloatRatePayCycle() {
        return floatRatePayCycle;
    }

    public void setFloatRatePayCycle(String floatRatePayCycle) {
        this.floatRatePayCycle = floatRatePayCycle;
    }

    public BigDecimal getValueamt() {
        return valueamt;
    }

    public void setValueamt(BigDecimal valueamt) {
        this.valueamt = valueamt;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", dealno=").append(dealno);
        sb.append(", accountType=").append(accountType);
        sb.append(", dealType=").append(dealType);
        sb.append(", dealDate=").append(dealDate);
        sb.append(", vdate=").append(vdate);
        sb.append(", firstDate=").append(firstDate);
        sb.append(", mdate=").append(mdate);
        sb.append(", timeLimit=").append(timeLimit);
        sb.append(", yearLimit=").append(yearLimit);
        sb.append(", direction=").append(direction);
        sb.append(", notionalPrincipal=").append(notionalPrincipal);
        sb.append(", cno=").append(cno);
        sb.append(", cnoCreditGrade=").append(cnoCreditGrade);
        sb.append(", interestDay=").append(interestDay);
        sb.append(", clearType=").append(clearType);
        sb.append(", fixedRate=").append(fixedRate);
        sb.append(", fixedRatePayCycle=").append(fixedRatePayCycle);
        sb.append(", floatRateWay=").append(floatRateWay);
        sb.append(", referRate=").append(referRate);
        sb.append(", resetFrequency=").append(resetFrequency);
        sb.append(", floatRatePayCycle=").append(floatRatePayCycle);
        sb.append(", valueamt=").append(valueamt);
        sb.append("]");
        return sb.toString();
    }
}