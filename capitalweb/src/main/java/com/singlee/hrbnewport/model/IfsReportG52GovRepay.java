package com.singlee.hrbnewport.model;

import java.math.BigDecimal;

/**
 *
 * 2021/12/8
 * @Auther:zk
 */    
    
/**
    * G52地方政府融资平台及支出责任债务持有情况统计表
    */
public class IfsReportG52GovRepay {
    /**
    * 序号 
    */
    private String seq;

    /**
    * 地区 
    */
    private String area;

    /**
    * 合计 
    */
    private BigDecimal sum;

    /**
    * 债务人层级-省级 
    */
    private String issLvProvince;

    /**
    * 债务人层级-地市级 
    */
    private String issLvCity;

    /**
    * 债务人层级-区县级 
    */
    private String issLvCountry;

    /**
    * 资金来源-贷款余额 
    */
    private BigDecimal sourceLoan;

    /**
    * 资金来源-投资债券类融资工具-小计 
    */
    private BigDecimal sourceSecSum;

    /**
    * 资金来源-投资债券类融资工具-按剩余期限-一年以内(含一年) 
    */
    private BigDecimal sourceSec1y;

    /**
    * 资金来源-投资债券类融资工具-按剩余期限-一年至五年(含五年) 
    */
    private BigDecimal sourceSec5y;

    /**
    * 资金来源-投资债券类融资工具-按剩余期限-五年至十年(含十年) 
    */
    private BigDecimal sourceSec10y;

    /**
    * 资金来源-投资债券类融资工具-按剩余期限-十年以上 
    */
    private BigDecimal sourceSec10yup;

    /**
    * 资金来源-表内其他信用资产余额 
    */
    private BigDecimal sourceBnQtxy;

    /**
    * 资金来源-表外信用余额 
    */
    private BigDecimal sourceBwXyye;

    /**
    * 资金来源-理财资金投资-小计 
    */
    private BigDecimal sourceLcSum;

    /**
    * 资金来源-理财资金投资-按最终投向-债券类 
    */
    private BigDecimal sourceLcSec;

    /**
    * 资金来源-理财资金投资-按最终投向-非标准化债权类资产 
    */
    private BigDecimal sourceLcUnstand;

    /**
    * 资金来源-理财资金投资-按最终投向-股权类 
    */
    private BigDecimal sourceLcRight;

    /**
    * 资金来源-理财资金投资-按最终投向-其他类 
    */
    private BigDecimal sourceLcOther;

    /**
    * 资金来源-信托 
    */
    private BigDecimal sourceLcXt;

    /**
    * 资金来源-融资租赁 
    */
    private BigDecimal sourceRzzl;

    /**
    * 贷款情况-资产质量-正常 
    */
    private BigDecimal loanLvNormal;

    /**
    * 贷款情况-资产质量-关注 
    */
    private BigDecimal loanLvAttention;

    /**
    * 贷款情况-资产质量-次级 
    */
    private BigDecimal loanLvSub;

    /**
    * 贷款情况-资产质量-可疑 
    */
    private BigDecimal loanLvKy;

    /**
    * 贷款情况-资产质量-损失 
    */
    private BigDecimal loanLvLoss;

    /**
    * 贷款情况-资产质量-贷款损失准备金余额 
    */
    private BigDecimal loanLvPreloss;

    /**
    * 贷款情况-剩余期限-一年以内(含一年) 
    */
    private BigDecimal loanTenor1y;

    /**
    * 贷款情况-剩余期限-一年至五年(含五年) 
    */
    private BigDecimal loanTenor5y;

    /**
    * 贷款情况-剩余期限-五年至十年(含十年) 
    */
    private BigDecimal loanTenor10y;

    /**
    * 贷款情况-剩余期限-十年以上 
    */
    private BigDecimal loanTenor10yup;

    /**
    * 贷款情况-资金投向-铁路 
    */
    private BigDecimal loanZjtxRailway;

    /**
    * 贷款情况-资金投向-公路 
    */
    private BigDecimal loanZjtxRoad;

    /**
    * 贷款情况-资金投向-其中:高速公路 
    */
    private BigDecimal loanZjtxHighspeed;

    /**
    * 贷款情况-资金投向-机场 
    */
    private BigDecimal loanZjtxAirport;

    /**
    * 贷款情况-资金投向-市政建设 
    */
    private BigDecimal loanZjtxSzjs;

    /**
    * 贷款情况-资金投向-土地储备 
    */
    private BigDecimal loanZjtxLand;

    /**
    * 贷款情况-资金投向-保障性住房 
    */
    private BigDecimal loanZjtxHouse;

    /**
    * 贷款情况-资金投向-其中:棚户区改造 
    */
    private BigDecimal loanZjtxPhq;

    /**
    * 贷款情况-资金投向-生态建设和环境保护 
    */
    private BigDecimal loanZjtxEnvir;

    /**
    * 贷款情况-资金投向-证券建设 
    */
    private BigDecimal loanZjtxZqjs;

    /**
    * 贷款情况-资金投向-教育 
    */
    private BigDecimal loanZjtxEdu;

    /**
    * 贷款情况-资金投向-科学 
    */
    private BigDecimal loanZjtxSince;

    /**
    * 贷款情况-资金投向-文化 
    */
    private BigDecimal loanZjtxCul;

    /**
    * 贷款情况-资金投向-医疗卫生 
    */
    private BigDecimal loanZjtxYlws;

    /**
    * 贷款情况-资金投向-社会保障 
    */
    private BigDecimal loanZjtxShbz;

    /**
    * 贷款情况-资金投向-粮油物资储备 
    */
    private BigDecimal loanZjtxFood;

    /**
    * 贷款情况-资金投向-农林水利建设 
    */
    private BigDecimal loanZjtxNlsl;

    /**
    * 贷款情况-资金投向-港口 
    */
    private BigDecimal loanZjtxPort;

    /**
    * 贷款情况-资金投向-其他公益性项目 
    */
    private BigDecimal loanZjtxGyx;

    /**
    * 贷款情况-资金投向-非公益性项目 
    */
    private BigDecimal loanZjtxFgyx;

    /**
    * 贷款情况-资金投向-非项目 
    */
    private BigDecimal loanZjtxFxm;

    /**
    * 数据类型 1-地方政府承诺偿还/2-地方政府提供担保/3-形成地方政府长期支出责任的融资
    */
    private String dataType;

    public String getSeq() {
        return seq;
    }

    public void setSeq(String seq) {
        this.seq = seq;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public BigDecimal getSum() {
        return sum;
    }

    public void setSum(BigDecimal sum) {
        this.sum = sum;
    }

    public String getIssLvProvince() {
        return issLvProvince;
    }

    public void setIssLvProvince(String issLvProvince) {
        this.issLvProvince = issLvProvince;
    }

    public String getIssLvCity() {
        return issLvCity;
    }

    public void setIssLvCity(String issLvCity) {
        this.issLvCity = issLvCity;
    }

    public String getIssLvCountry() {
        return issLvCountry;
    }

    public void setIssLvCountry(String issLvCountry) {
        this.issLvCountry = issLvCountry;
    }

    public BigDecimal getSourceLoan() {
        return sourceLoan;
    }

    public void setSourceLoan(BigDecimal sourceLoan) {
        this.sourceLoan = sourceLoan;
    }

    public BigDecimal getSourceSecSum() {
        return sourceSecSum;
    }

    public void setSourceSecSum(BigDecimal sourceSecSum) {
        this.sourceSecSum = sourceSecSum;
    }

    public BigDecimal getSourceSec1y() {
        return sourceSec1y;
    }

    public void setSourceSec1y(BigDecimal sourceSec1y) {
        this.sourceSec1y = sourceSec1y;
    }

    public BigDecimal getSourceSec5y() {
        return sourceSec5y;
    }

    public void setSourceSec5y(BigDecimal sourceSec5y) {
        this.sourceSec5y = sourceSec5y;
    }

    public BigDecimal getSourceSec10y() {
        return sourceSec10y;
    }

    public void setSourceSec10y(BigDecimal sourceSec10y) {
        this.sourceSec10y = sourceSec10y;
    }

    public BigDecimal getSourceSec10yup() {
        return sourceSec10yup;
    }

    public void setSourceSec10yup(BigDecimal sourceSec10yup) {
        this.sourceSec10yup = sourceSec10yup;
    }

    public BigDecimal getSourceBnQtxy() {
        return sourceBnQtxy;
    }

    public void setSourceBnQtxy(BigDecimal sourceBnQtxy) {
        this.sourceBnQtxy = sourceBnQtxy;
    }

    public BigDecimal getSourceBwXyye() {
        return sourceBwXyye;
    }

    public void setSourceBwXyye(BigDecimal sourceBwXyye) {
        this.sourceBwXyye = sourceBwXyye;
    }

    public BigDecimal getSourceLcSum() {
        return sourceLcSum;
    }

    public void setSourceLcSum(BigDecimal sourceLcSum) {
        this.sourceLcSum = sourceLcSum;
    }

    public BigDecimal getSourceLcSec() {
        return sourceLcSec;
    }

    public void setSourceLcSec(BigDecimal sourceLcSec) {
        this.sourceLcSec = sourceLcSec;
    }

    public BigDecimal getSourceLcUnstand() {
        return sourceLcUnstand;
    }

    public void setSourceLcUnstand(BigDecimal sourceLcUnstand) {
        this.sourceLcUnstand = sourceLcUnstand;
    }

    public BigDecimal getSourceLcRight() {
        return sourceLcRight;
    }

    public void setSourceLcRight(BigDecimal sourceLcRight) {
        this.sourceLcRight = sourceLcRight;
    }

    public BigDecimal getSourceLcOther() {
        return sourceLcOther;
    }

    public void setSourceLcOther(BigDecimal sourceLcOther) {
        this.sourceLcOther = sourceLcOther;
    }

    public BigDecimal getSourceLcXt() {
        return sourceLcXt;
    }

    public void setSourceLcXt(BigDecimal sourceLcXt) {
        this.sourceLcXt = sourceLcXt;
    }

    public BigDecimal getSourceRzzl() {
        return sourceRzzl;
    }

    public void setSourceRzzl(BigDecimal sourceRzzl) {
        this.sourceRzzl = sourceRzzl;
    }

    public BigDecimal getLoanLvNormal() {
        return loanLvNormal;
    }

    public void setLoanLvNormal(BigDecimal loanLvNormal) {
        this.loanLvNormal = loanLvNormal;
    }

    public BigDecimal getLoanLvAttention() {
        return loanLvAttention;
    }

    public void setLoanLvAttention(BigDecimal loanLvAttention) {
        this.loanLvAttention = loanLvAttention;
    }

    public BigDecimal getLoanLvSub() {
        return loanLvSub;
    }

    public void setLoanLvSub(BigDecimal loanLvSub) {
        this.loanLvSub = loanLvSub;
    }

    public BigDecimal getLoanLvKy() {
        return loanLvKy;
    }

    public void setLoanLvKy(BigDecimal loanLvKy) {
        this.loanLvKy = loanLvKy;
    }

    public BigDecimal getLoanLvLoss() {
        return loanLvLoss;
    }

    public void setLoanLvLoss(BigDecimal loanLvLoss) {
        this.loanLvLoss = loanLvLoss;
    }

    public BigDecimal getLoanLvPreloss() {
        return loanLvPreloss;
    }

    public void setLoanLvPreloss(BigDecimal loanLvPreloss) {
        this.loanLvPreloss = loanLvPreloss;
    }

    public BigDecimal getLoanTenor1y() {
        return loanTenor1y;
    }

    public void setLoanTenor1y(BigDecimal loanTenor1y) {
        this.loanTenor1y = loanTenor1y;
    }

    public BigDecimal getLoanTenor5y() {
        return loanTenor5y;
    }

    public void setLoanTenor5y(BigDecimal loanTenor5y) {
        this.loanTenor5y = loanTenor5y;
    }

    public BigDecimal getLoanTenor10y() {
        return loanTenor10y;
    }

    public void setLoanTenor10y(BigDecimal loanTenor10y) {
        this.loanTenor10y = loanTenor10y;
    }

    public BigDecimal getLoanTenor10yup() {
        return loanTenor10yup;
    }

    public void setLoanTenor10yup(BigDecimal loanTenor10yup) {
        this.loanTenor10yup = loanTenor10yup;
    }

    public BigDecimal getLoanZjtxRailway() {
        return loanZjtxRailway;
    }

    public void setLoanZjtxRailway(BigDecimal loanZjtxRailway) {
        this.loanZjtxRailway = loanZjtxRailway;
    }

    public BigDecimal getLoanZjtxRoad() {
        return loanZjtxRoad;
    }

    public void setLoanZjtxRoad(BigDecimal loanZjtxRoad) {
        this.loanZjtxRoad = loanZjtxRoad;
    }

    public BigDecimal getLoanZjtxHighspeed() {
        return loanZjtxHighspeed;
    }

    public void setLoanZjtxHighspeed(BigDecimal loanZjtxHighspeed) {
        this.loanZjtxHighspeed = loanZjtxHighspeed;
    }

    public BigDecimal getLoanZjtxAirport() {
        return loanZjtxAirport;
    }

    public void setLoanZjtxAirport(BigDecimal loanZjtxAirport) {
        this.loanZjtxAirport = loanZjtxAirport;
    }

    public BigDecimal getLoanZjtxSzjs() {
        return loanZjtxSzjs;
    }

    public void setLoanZjtxSzjs(BigDecimal loanZjtxSzjs) {
        this.loanZjtxSzjs = loanZjtxSzjs;
    }

    public BigDecimal getLoanZjtxLand() {
        return loanZjtxLand;
    }

    public void setLoanZjtxLand(BigDecimal loanZjtxLand) {
        this.loanZjtxLand = loanZjtxLand;
    }

    public BigDecimal getLoanZjtxHouse() {
        return loanZjtxHouse;
    }

    public void setLoanZjtxHouse(BigDecimal loanZjtxHouse) {
        this.loanZjtxHouse = loanZjtxHouse;
    }

    public BigDecimal getLoanZjtxPhq() {
        return loanZjtxPhq;
    }

    public void setLoanZjtxPhq(BigDecimal loanZjtxPhq) {
        this.loanZjtxPhq = loanZjtxPhq;
    }

    public BigDecimal getLoanZjtxEnvir() {
        return loanZjtxEnvir;
    }

    public void setLoanZjtxEnvir(BigDecimal loanZjtxEnvir) {
        this.loanZjtxEnvir = loanZjtxEnvir;
    }

    public BigDecimal getLoanZjtxZqjs() {
        return loanZjtxZqjs;
    }

    public void setLoanZjtxZqjs(BigDecimal loanZjtxZqjs) {
        this.loanZjtxZqjs = loanZjtxZqjs;
    }

    public BigDecimal getLoanZjtxEdu() {
        return loanZjtxEdu;
    }

    public void setLoanZjtxEdu(BigDecimal loanZjtxEdu) {
        this.loanZjtxEdu = loanZjtxEdu;
    }

    public BigDecimal getLoanZjtxSince() {
        return loanZjtxSince;
    }

    public void setLoanZjtxSince(BigDecimal loanZjtxSince) {
        this.loanZjtxSince = loanZjtxSince;
    }

    public BigDecimal getLoanZjtxCul() {
        return loanZjtxCul;
    }

    public void setLoanZjtxCul(BigDecimal loanZjtxCul) {
        this.loanZjtxCul = loanZjtxCul;
    }

    public BigDecimal getLoanZjtxYlws() {
        return loanZjtxYlws;
    }

    public void setLoanZjtxYlws(BigDecimal loanZjtxYlws) {
        this.loanZjtxYlws = loanZjtxYlws;
    }

    public BigDecimal getLoanZjtxShbz() {
        return loanZjtxShbz;
    }

    public void setLoanZjtxShbz(BigDecimal loanZjtxShbz) {
        this.loanZjtxShbz = loanZjtxShbz;
    }

    public BigDecimal getLoanZjtxFood() {
        return loanZjtxFood;
    }

    public void setLoanZjtxFood(BigDecimal loanZjtxFood) {
        this.loanZjtxFood = loanZjtxFood;
    }

    public BigDecimal getLoanZjtxNlsl() {
        return loanZjtxNlsl;
    }

    public void setLoanZjtxNlsl(BigDecimal loanZjtxNlsl) {
        this.loanZjtxNlsl = loanZjtxNlsl;
    }

    public BigDecimal getLoanZjtxPort() {
        return loanZjtxPort;
    }

    public void setLoanZjtxPort(BigDecimal loanZjtxPort) {
        this.loanZjtxPort = loanZjtxPort;
    }

    public BigDecimal getLoanZjtxGyx() {
        return loanZjtxGyx;
    }

    public void setLoanZjtxGyx(BigDecimal loanZjtxGyx) {
        this.loanZjtxGyx = loanZjtxGyx;
    }

    public BigDecimal getLoanZjtxFgyx() {
        return loanZjtxFgyx;
    }

    public void setLoanZjtxFgyx(BigDecimal loanZjtxFgyx) {
        this.loanZjtxFgyx = loanZjtxFgyx;
    }

    public BigDecimal getLoanZjtxFxm() {
        return loanZjtxFxm;
    }

    public void setLoanZjtxFxm(BigDecimal loanZjtxFxm) {
        this.loanZjtxFxm = loanZjtxFxm;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }
}