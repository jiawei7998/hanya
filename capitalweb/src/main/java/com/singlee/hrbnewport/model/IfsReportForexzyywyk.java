package com.singlee.hrbnewport.model;

import java.math.BigDecimal;

/**
    * 外汇自营业务浮盈亏表
    */
public class IfsReportForexzyywyk {
    /**
    * 业务层级
    */
    private String product;

    /**
    * 货币对/合约
    */
    private String currencypair;

    /**
    * 日损益（元）
    */
    private BigDecimal dayLossgain;

    /**
    * 周损益（元）
    */
    private BigDecimal weekLossgain;

    /**
    * 当月损益（元）
    */
    private BigDecimal monthLossgain;

    /**
    * 季度损益（元）
    */
    private BigDecimal quarterLossgain;

    /**
    * 年度损益（元）
    */
    private BigDecimal yearLossgain;

    /**
    * 父业务
    */
    private String parentProduct;

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product == null ? null : product.trim();
    }

    public String getCurrencypair() {
        return currencypair;
    }

    public void setCurrencypair(String currencypair) {
        this.currencypair = currencypair == null ? null : currencypair.trim();
    }

    public BigDecimal getDayLossgain() {
        return dayLossgain;
    }

    public void setDayLossgain(BigDecimal dayLossgain) {
        this.dayLossgain = dayLossgain;
    }

    public BigDecimal getWeekLossgain() {
        return weekLossgain;
    }

    public void setWeekLossgain(BigDecimal weekLossgain) {
        this.weekLossgain = weekLossgain;
    }

    public BigDecimal getMonthLossgain() {
        return monthLossgain;
    }

    public void setMonthLossgain(BigDecimal monthLossgain) {
        this.monthLossgain = monthLossgain;
    }

    public BigDecimal getQuarterLossgain() {
        return quarterLossgain;
    }

    public void setQuarterLossgain(BigDecimal quarterLossgain) {
        this.quarterLossgain = quarterLossgain;
    }

    public BigDecimal getYearLossgain() {
        return yearLossgain;
    }

    public void setYearLossgain(BigDecimal yearLossgain) {
        this.yearLossgain = yearLossgain;
    }

    public String getParentProduct() {
        return parentProduct;
    }

    public void setParentProduct(String parentProduct) {
        this.parentProduct = parentProduct == null ? null : parentProduct.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", product=").append(product);
        sb.append(", currencypair=").append(currencypair);
        sb.append(", dayLossgain=").append(dayLossgain);
        sb.append(", weekLossgain=").append(weekLossgain);
        sb.append(", monthLossgain=").append(monthLossgain);
        sb.append(", quarterLossgain=").append(quarterLossgain);
        sb.append(", yearLossgain=").append(yearLossgain);
        sb.append(", parentProduct=").append(parentProduct);
        sb.append("]");
        return sb.toString();
    }
}