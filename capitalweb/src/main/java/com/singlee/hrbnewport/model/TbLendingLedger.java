package com.singlee.hrbnewport.model;

/**
    * 拆借交易台账2020-11-01到2020-11-30
    */
public class TbLendingLedger {
    /**
    * 成交单号
    */
    private String dealno;

    /**
    * 成本中心
    */
    private String costdesc;

    /**
    * 对手方
    */
    private String cname;

    /**
    * 对手属性
    */
    private String acctdesc;

    /**
    * 账户类型
    */
    private String accttype;

    /**
    * 拆借(回购)方式
    */
    private String descr;

    /**
    * 首期交割日
    */
    private String vdate;

    /**
    * 期限
    */
    private String term;

    /**
    * 到期交割日
    */
    private String mdate;

    /**
    * 利率(%)
    */
    private String intrate;

    /**
    * 券面（拆借）总额
    */
    private String ccyamtabs;

    /**
    * 首期结算额
    */
    private String ccyamt;

    /**
    * 到期结算额
    */
    private String totpayamt;

    /**
    * 利息支额
    */
    private String totalint;

    /**
    * 资金占用
    */
    private String occupyamt;

    /**
    * 交易手续费
    */
    private String handamt;

    /**
    * 剩余期限
    */
    private String overterm;

    /**
    * 应收（付）利息额
    */
    private String rplint;

    /**
    * 剩余利息额
    */
    private String overlint;

    /**
    * 已实现损益
    */
    private String proloss;

    public String getDealno() {
        return dealno;
    }

    public void setDealno(String dealno) {
        this.dealno = dealno == null ? null : dealno.trim();
    }

    public String getCostdesc() {
        return costdesc;
    }

    public void setCostdesc(String costdesc) {
        this.costdesc = costdesc == null ? null : costdesc.trim();
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname == null ? null : cname.trim();
    }

    public String getAcctdesc() {
        return acctdesc;
    }

    public void setAcctdesc(String acctdesc) {
        this.acctdesc = acctdesc == null ? null : acctdesc.trim();
    }

    public String getAccttype() {
        return accttype;
    }

    public void setAccttype(String accttype) {
        this.accttype = accttype == null ? null : accttype.trim();
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr == null ? null : descr.trim();
    }

    public String getVdate() {
        return vdate;
    }

    public void setVdate(String vdate) {
        this.vdate = vdate == null ? null : vdate.trim();
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term == null ? null : term.trim();
    }

    public String getMdate() {
        return mdate;
    }

    public void setMdate(String mdate) {
        this.mdate = mdate == null ? null : mdate.trim();
    }

    public String getIntrate() {
        return intrate;
    }

    public void setIntrate(String intrate) {
        this.intrate = intrate == null ? null : intrate.trim();
    }

    public String getCcyamtabs() {
        return ccyamtabs;
    }

    public void setCcyamtabs(String ccyamtabs) {
        this.ccyamtabs = ccyamtabs == null ? null : ccyamtabs.trim();
    }

    public String getCcyamt() {
        return ccyamt;
    }

    public void setCcyamt(String ccyamt) {
        this.ccyamt = ccyamt == null ? null : ccyamt.trim();
    }

    public String getTotpayamt() {
        return totpayamt;
    }

    public void setTotpayamt(String totpayamt) {
        this.totpayamt = totpayamt == null ? null : totpayamt.trim();
    }

    public String getTotalint() {
        return totalint;
    }

    public void setTotalint(String totalint) {
        this.totalint = totalint == null ? null : totalint.trim();
    }

    public String getOccupyamt() {
        return occupyamt;
    }

    public void setOccupyamt(String occupyamt) {
        this.occupyamt = occupyamt == null ? null : occupyamt.trim();
    }

    public String getHandamt() {
        return handamt;
    }

    public void setHandamt(String handamt) {
        this.handamt = handamt == null ? null : handamt.trim();
    }

    public String getOverterm() {
        return overterm;
    }

    public void setOverterm(String overterm) {
        this.overterm = overterm == null ? null : overterm.trim();
    }

    public String getRplint() {
        return rplint;
    }

    public void setRplint(String rplint) {
        this.rplint = rplint == null ? null : rplint.trim();
    }

    public String getOverlint() {
        return overlint;
    }

    public void setOverlint(String overlint) {
        this.overlint = overlint == null ? null : overlint.trim();
    }

    public String getProloss() {
        return proloss;
    }

    public void setProloss(String proloss) {
        this.proloss = proloss == null ? null : proloss.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", dealno=").append(dealno);
        sb.append(", costdesc=").append(costdesc);
        sb.append(", cname=").append(cname);
        sb.append(", acctdesc=").append(acctdesc);
        sb.append(", accttype=").append(accttype);
        sb.append(", descr=").append(descr);
        sb.append(", vdate=").append(vdate);
        sb.append(", term=").append(term);
        sb.append(", mdate=").append(mdate);
        sb.append(", intrate=").append(intrate);
        sb.append(", ccyamtabs=").append(ccyamtabs);
        sb.append(", ccyamt=").append(ccyamt);
        sb.append(", totpayamt=").append(totpayamt);
        sb.append(", totalint=").append(totalint);
        sb.append(", occupyamt=").append(occupyamt);
        sb.append(", handamt=").append(handamt);
        sb.append(", overterm=").append(overterm);
        sb.append(", rplint=").append(rplint);
        sb.append(", overlint=").append(overlint);
        sb.append(", proloss=").append(proloss);
        sb.append("]");
        return sb.toString();
    }
}