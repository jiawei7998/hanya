package com.singlee.hrbnewport.model;

import java.math.BigDecimal;

/**
    * 持仓债券评级情况表（元）
    */
public class TbPositionRatingBonds {
    /**
    * 债券类型
    */
    private String bondType;

    /**
    * 账户类型
    */
    private String accountType;

    /**
    * 债券代码
    */
    private String bondCode;

    /**
    * 债券名称
    */
    private String bondName;

    /**
    * 票面利率（%）
    */
    private BigDecimal faceRate;

    /**
    * 起息日
    */
    private String vdate;

    /**
    * 期限
    */
    private String limitTime;

    /**
    * 到期日
    */
    private String mdate;

    /**
    * 待偿期（年）
    */
    private BigDecimal theirYears;

    /**
    * 债券面值
    */
    private BigDecimal bondPrice;

    /**
    * 最新买入日
    */
    private String latestBuyDate;

    /**
    * 主体评级
    */
    private String mainRating;

    /**
    * 主体评级准入限额
    */
    private String mainRatingLimit;

    /**
    * 债项评级
    */
    private String debtRating;

    /**
    * 债项评级准入限额
    */
    private String debtRatingLimit;

    /**
    * 是否超出准入标准
    */
    private String isThanCriterion;

    public String getBondType() {
        return bondType;
    }

    public void setBondType(String bondType) {
        this.bondType = bondType;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getBondCode() {
        return bondCode;
    }

    public void setBondCode(String bondCode) {
        this.bondCode = bondCode;
    }

    public String getBondName() {
        return bondName;
    }

    public void setBondName(String bondName) {
        this.bondName = bondName;
    }

    public BigDecimal getFaceRate() {
        return faceRate;
    }

    public void setFaceRate(BigDecimal faceRate) {
        this.faceRate = faceRate;
    }

    public String getVdate() {
        return vdate;
    }

    public void setVdate(String vdate) {
        this.vdate = vdate;
    }

    public String getLimitTime() {
        return limitTime;
    }

    public void setLimitTime(String limitTime) {
        this.limitTime = limitTime;
    }

    public String getMdate() {
        return mdate;
    }

    public void setMdate(String mdate) {
        this.mdate = mdate;
    }

    public BigDecimal getTheirYears() {
        return theirYears;
    }

    public void setTheirYears(BigDecimal theirYears) {
        this.theirYears = theirYears;
    }

    public BigDecimal getBondPrice() {
        return bondPrice;
    }

    public void setBondPrice(BigDecimal bondPrice) {
        this.bondPrice = bondPrice;
    }

    public String getLatestBuyDate() {
        return latestBuyDate;
    }

    public void setLatestBuyDate(String latestBuyDate) {
        this.latestBuyDate = latestBuyDate;
    }

    public String getMainRating() {
        return mainRating;
    }

    public void setMainRating(String mainRating) {
        this.mainRating = mainRating;
    }

    public String getMainRatingLimit() {
        return mainRatingLimit;
    }

    public void setMainRatingLimit(String mainRatingLimit) {
        this.mainRatingLimit = mainRatingLimit;
    }

    public String getDebtRating() {
        return debtRating;
    }

    public void setDebtRating(String debtRating) {
        this.debtRating = debtRating;
    }

    public String getDebtRatingLimit() {
        return debtRatingLimit;
    }

    public void setDebtRatingLimit(String debtRatingLimit) {
        this.debtRatingLimit = debtRatingLimit;
    }

    public String getIsThanCriterion() {
        return isThanCriterion;
    }

    public void setIsThanCriterion(String isThanCriterion) {
        this.isThanCriterion = isThanCriterion;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", bondType=").append(bondType);
        sb.append(", accountType=").append(accountType);
        sb.append(", bondCode=").append(bondCode);
        sb.append(", bondName=").append(bondName);
        sb.append(", faceRate=").append(faceRate);
        sb.append(", vdate=").append(vdate);
        sb.append(", limitTime=").append(limitTime);
        sb.append(", mdate=").append(mdate);
        sb.append(", theirYears=").append(theirYears);
        sb.append(", bondPrice=").append(bondPrice);
        sb.append(", latestBuyDate=").append(latestBuyDate);
        sb.append(", mainRating=").append(mainRating);
        sb.append(", mainRatingLimit=").append(mainRatingLimit);
        sb.append(", debtRating=").append(debtRating);
        sb.append(", debtRatingLimit=").append(debtRatingLimit);
        sb.append(", isThanCriterion=").append(isThanCriterion);
        sb.append("]");
        return sb.toString();
    }
}