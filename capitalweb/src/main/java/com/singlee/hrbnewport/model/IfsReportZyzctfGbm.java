package com.singlee.hrbnewport.model;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/8 20:17
 * @description：${description}
 * @modified By：
 * @version:   
 */
public class IfsReportZyzctfGbm implements Serializable {
    /**
    * 部门
    */
    private String product;

    /**
    * 合计
    */
    private BigDecimal amt;

    /**
    * 上日余额（本周三）
    */
    private BigDecimal srye;

    /**
    * 本周日余额（预计数）
    */
    private BigDecimal bzrye;

    /**
    * 下周投放额（下周一-下周日）
    */
    private BigDecimal xztfe;

    /**
    * 下周到期额（下周一-下周日）
    */
    private BigDecimal xzdqe;

    /**
    * 其中：预计到期不能收回资金额度
    */
    private BigDecimal bnhsje;

    /**
    * 下周末余额（下周日）
    */
    private BigDecimal xzmye;

    /**
    * 下周净增
    */
    private BigDecimal xzjz;

    /**
    * 上周日余额
    */
    private BigDecimal szrye;

    /**
    * 排序
    */
    private Short px;

    /**
    * 下周增加额
    */
    private BigDecimal xzzje;

    private static final long serialVersionUID = 1L;

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public BigDecimal getAmt() {
        return amt;
    }

    public void setAmt(BigDecimal amt) {
        this.amt = amt;
    }

    public BigDecimal getSrye() {
        return srye;
    }

    public void setSrye(BigDecimal srye) {
        this.srye = srye;
    }

    public BigDecimal getBzrye() {
        return bzrye;
    }

    public void setBzrye(BigDecimal bzrye) {
        this.bzrye = bzrye;
    }

    public BigDecimal getXztfe() {
        return xztfe;
    }

    public void setXztfe(BigDecimal xztfe) {
        this.xztfe = xztfe;
    }

    public BigDecimal getXzdqe() {
        return xzdqe;
    }

    public void setXzdqe(BigDecimal xzdqe) {
        this.xzdqe = xzdqe;
    }

    public BigDecimal getBnhsje() {
        return bnhsje;
    }

    public void setBnhsje(BigDecimal bnhsje) {
        this.bnhsje = bnhsje;
    }

    public BigDecimal getXzmye() {
        return xzmye;
    }

    public void setXzmye(BigDecimal xzmye) {
        this.xzmye = xzmye;
    }

    public BigDecimal getXzjz() {
        return xzjz;
    }

    public void setXzjz(BigDecimal xzjz) {
        this.xzjz = xzjz;
    }

    public BigDecimal getSzrye() {
        return szrye;
    }

    public void setSzrye(BigDecimal szrye) {
        this.szrye = szrye;
    }

    public Short getPx() {
        return px;
    }

    public void setPx(Short px) {
        this.px = px;
    }

    public BigDecimal getXzzje() {
        return xzzje;
    }

    public void setXzzje(BigDecimal xzzje) {
        this.xzzje = xzzje;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", product=").append(product);
        sb.append(", amt=").append(amt);
        sb.append(", srye=").append(srye);
        sb.append(", bzrye=").append(bzrye);
        sb.append(", xztfe=").append(xztfe);
        sb.append(", xzdqe=").append(xzdqe);
        sb.append(", bnhsje=").append(bnhsje);
        sb.append(", xzmye=").append(xzmye);
        sb.append(", xzjz=").append(xzjz);
        sb.append(", szrye=").append(szrye);
        sb.append(", px=").append(px);
        sb.append(", xzzje=").append(xzzje);
        sb.append("]");
        return sb.toString();
    }
}