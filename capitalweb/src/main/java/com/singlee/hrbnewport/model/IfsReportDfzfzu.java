package com.singlee.hrbnewport.model;

import java.io.Serializable;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 20:52
 * @description：${description}
 * @modified By：
 * @version:   
 */
public class IfsReportDfzfzu implements Serializable {
    /**
    * 序号
    */
    private String ser;

    /**
    * 地区
    */
    private String dq;

    /**
    * 余额
    */
    private String ye;

    /**
    * 一般债券
    */
    private String ybzq;

    /**
    * 专项债券
    */
    private String zxzq;

    /**
    * 公开发行债券
    */
    private String gkfxzq;

    /**
    * 定向承销债券
    */
    private String dxcxzq;

    /**
    * 较基准国债平均收益率上浮15%以内（含15%）
    */
    private String l15;

    /**
    * 较基准国债平均收益率上浮15%至30%（含30%）
    */
    private String b30;

    /**
    * 较基准国债平均收益率上浮30%以上
    */
    private String g30;

    /**
    * 一年以内（含一年）
    */
    private String y1;

    /**
    * 一年至五年（含五年）
    */
    private String y5;

    /**
    * 五年至十年（含十年）
    */
    private String y10;

    /**
    * 十年以上
    */
    private String gy10;

    private static final long serialVersionUID = 1L;

    public String getSer() {
        return ser;
    }

    public void setSer(String ser) {
        this.ser = ser;
    }

    public String getDq() {
        return dq;
    }

    public void setDq(String dq) {
        this.dq = dq;
    }

    public String getYe() {
        return ye;
    }

    public void setYe(String ye) {
        this.ye = ye;
    }

    public String getYbzq() {
        return ybzq;
    }

    public void setYbzq(String ybzq) {
        this.ybzq = ybzq;
    }

    public String getZxzq() {
        return zxzq;
    }

    public void setZxzq(String zxzq) {
        this.zxzq = zxzq;
    }

    public String getGkfxzq() {
        return gkfxzq;
    }

    public void setGkfxzq(String gkfxzq) {
        this.gkfxzq = gkfxzq;
    }

    public String getDxcxzq() {
        return dxcxzq;
    }

    public void setDxcxzq(String dxcxzq) {
        this.dxcxzq = dxcxzq;
    }

    public String getL15() {
        return l15;
    }

    public void setL15(String l15) {
        this.l15 = l15;
    }

    public String getB30() {
        return b30;
    }

    public void setB30(String b30) {
        this.b30 = b30;
    }

    public String getG30() {
        return g30;
    }

    public void setG30(String g30) {
        this.g30 = g30;
    }

    public String getY1() {
        return y1;
    }

    public void setY1(String y1) {
        this.y1 = y1;
    }

    public String getY5() {
        return y5;
    }

    public void setY5(String y5) {
        this.y5 = y5;
    }

    public String getY10() {
        return y10;
    }

    public void setY10(String y10) {
        this.y10 = y10;
    }

    public String getGy10() {
        return gy10;
    }

    public void setGy10(String gy10) {
        this.gy10 = gy10;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", ser=").append(ser);
        sb.append(", dq=").append(dq);
        sb.append(", ye=").append(ye);
        sb.append(", ybzq=").append(ybzq);
        sb.append(", zxzq=").append(zxzq);
        sb.append(", gkfxzq=").append(gkfxzq);
        sb.append(", dxcxzq=").append(dxcxzq);
        sb.append(", l15=").append(l15);
        sb.append(", b30=").append(b30);
        sb.append(", g30=").append(g30);
        sb.append(", y1=").append(y1);
        sb.append(", y5=").append(y5);
        sb.append(", y10=").append(y10);
        sb.append(", gy10=").append(gy10);
        sb.append("]");
        return sb.toString();
    }
}