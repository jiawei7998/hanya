package com.singlee.hrbnewport.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

/**
 * 存款含银行同业和联行存放负债 境外机构存款表
 */
@Table(name = "IFS_REPORT_CKHYHTYHLHCFFZ_JWJGCKB")
public class IfsReportCkhyhtyhlhcffzJwjgckb implements Serializable {
    /**
     * 数据自编码
     */
    @Column(name = "SJZBM")
    private String sjzbm;

    /**
     * 填报机构代码
     */
    @Column(name = "TBJGDM")
    private String tbjgdm;

    /**
     * 报告期
     */
    @Column(name = "BGQ")
    private String bgq;

    /**
     * 外债编号
     */
    @Column(name = "WZBH")
    private String wzbh;

    /**
     * 上月末应付利息余额
     */
    @Column(name = "SYMYFLXYE")
    private BigDecimal symyflxye;

    /**
     * 本月末本金余额其中剩余期限在一年及以下
     */
    @Column(name = "BYMBJYESYQXYNYX")
    private BigDecimal bymbjyesyqxynyx;

    /**
     * 本月末应付利息余额
     */
    @Column(name = "BYMYFLXYE")
    private BigDecimal bymyflxye;

    /**
     * 本月净发生额
     */
    @Column(name = "BYJFSE")
    private BigDecimal byjfse;

    /**
     * 本月利息支出
     */
    @Column(name = "BYLXZC")
    private BigDecimal bylxzc;

    /**
     * 备注
     */
    @Column(name = "BZ")
    private String bz;

    private static final long serialVersionUID = 1L;

    /**
     * 获取数据自编码
     *
     * @return SJZBM - 数据自编码
     */
    public String getSjzbm() {
        return sjzbm;
    }

    /**
     * 设置数据自编码
     *
     * @param sjzbm 数据自编码
     */
    public void setSjzbm(String sjzbm) {
        this.sjzbm = sjzbm;
    }

    /**
     * 获取填报机构代码
     *
     * @return TBJGDM - 填报机构代码
     */
    public String getTbjgdm() {
        return tbjgdm;
    }

    /**
     * 设置填报机构代码
     *
     * @param tbjgdm 填报机构代码
     */
    public void setTbjgdm(String tbjgdm) {
        this.tbjgdm = tbjgdm;
    }

    /**
     * 获取报告期
     *
     * @return BGQ - 报告期
     */
    public String getBgq() {
        return bgq;
    }

    /**
     * 设置报告期
     *
     * @param bgq 报告期
     */
    public void setBgq(String bgq) {
        this.bgq = bgq;
    }

    /**
     * 获取外债编号
     *
     * @return WZBH - 外债编号
     */
    public String getWzbh() {
        return wzbh;
    }

    /**
     * 设置外债编号
     *
     * @param wzbh 外债编号
     */
    public void setWzbh(String wzbh) {
        this.wzbh = wzbh;
    }

    /**
     * 获取上月末应付利息余额
     *
     * @return SYMYFLXYE - 上月末应付利息余额
     */
    public BigDecimal getSymyflxye() {
        return symyflxye;
    }

    /**
     * 设置上月末应付利息余额
     *
     * @param symyflxye 上月末应付利息余额
     */
    public void setSymyflxye(BigDecimal symyflxye) {
        this.symyflxye = symyflxye;
    }

    /**
     * 获取本月末本金余额其中剩余期限在一年及以下
     *
     * @return BYMBJYESYQXYNYX - 本月末本金余额其中剩余期限在一年及以下
     */
    public BigDecimal getBymbjyesyqxynyx() {
        return bymbjyesyqxynyx;
    }

    /**
     * 设置本月末本金余额其中剩余期限在一年及以下
     *
     * @param bymbjyesyqxynyx 本月末本金余额其中剩余期限在一年及以下
     */
    public void setBymbjyesyqxynyx(BigDecimal bymbjyesyqxynyx) {
        this.bymbjyesyqxynyx = bymbjyesyqxynyx;
    }

    /**
     * 获取本月末应付利息余额
     *
     * @return BYMYFLXYE - 本月末应付利息余额
     */
    public BigDecimal getBymyflxye() {
        return bymyflxye;
    }

    /**
     * 设置本月末应付利息余额
     *
     * @param bymyflxye 本月末应付利息余额
     */
    public void setBymyflxye(BigDecimal bymyflxye) {
        this.bymyflxye = bymyflxye;
    }

    /**
     * 获取本月净发生额
     *
     * @return BYJFSE - 本月净发生额
     */
    public BigDecimal getByjfse() {
        return byjfse;
    }

    /**
     * 设置本月净发生额
     *
     * @param byjfse 本月净发生额
     */
    public void setByjfse(BigDecimal byjfse) {
        this.byjfse = byjfse;
    }

    /**
     * 获取本月利息支出
     *
     * @return BYLXZC - 本月利息支出
     */
    public BigDecimal getBylxzc() {
        return bylxzc;
    }

    /**
     * 设置本月利息支出
     *
     * @param bylxzc 本月利息支出
     */
    public void setBylxzc(BigDecimal bylxzc) {
        this.bylxzc = bylxzc;
    }

    /**
     * 获取备注
     *
     * @return BZ - 备注
     */
    public String getBz() {
        return bz;
    }

    /**
     * 设置备注
     *
     * @param bz 备注
     */
    public void setBz(String bz) {
        this.bz = bz;
    }
}