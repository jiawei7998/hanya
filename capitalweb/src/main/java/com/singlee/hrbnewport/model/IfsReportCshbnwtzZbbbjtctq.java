package com.singlee.hrbnewport.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author ：meigy
 * @date ：Created in 2021/11/15 11:07
 * @description：${description}
 * @modified By：
 * @version:
 */

/**
 * 表内外投资业务资本拨备计提情况表（穿透前）
 */
@Entity
@Table(name = "IFS_REPORT_CSHBNWTZ_ZBBBJTCTQ")
public class IfsReportCshbnwtzZbbbjtctq implements Serializable {
    /**
     * 资金来源
     */
    private String zjtype;

    /**
     * 项目
     */
    private String product;

    /**
     * 余额_本季末数额
     */
    private BigDecimal yeBjmAmt;

    /**
     * 余额_上季末数额
     */
    private BigDecimal yeSjmAmt;

    /**
     * 账面不良资产_本季末数额
     */
    private BigDecimal zmblzcBjmAmt;

    /**
     * 账面不良资产_上季末数额
     */
    private BigDecimal zmblzcSjmAmt;

    /**
     * 实质不良资产_本季末数额
     */
    private BigDecimal szblzcBjmAmt;

    /**
     * 实质不良资产_上季末数额
     */
    private BigDecimal szblzcSjmAmt;

    /**
     * 备注
     */
    private String bz;

    private String parentProduct;

    private static final long serialVersionUID = 1L;

    public String getZjtype() {
        return zjtype;
    }

    public void setZjtype(String zjtype) {
        this.zjtype = zjtype;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public BigDecimal getYeBjmAmt() {
        return yeBjmAmt;
    }

    public void setYeBjmAmt(BigDecimal yeBjmAmt) {
        this.yeBjmAmt = yeBjmAmt;
    }

    public BigDecimal getYeSjmAmt() {
        return yeSjmAmt;
    }

    public void setYeSjmAmt(BigDecimal yeSjmAmt) {
        this.yeSjmAmt = yeSjmAmt;
    }

    public BigDecimal getZmblzcBjmAmt() {
        return zmblzcBjmAmt;
    }

    public void setZmblzcBjmAmt(BigDecimal zmblzcBjmAmt) {
        this.zmblzcBjmAmt = zmblzcBjmAmt;
    }

    public BigDecimal getZmblzcSjmAmt() {
        return zmblzcSjmAmt;
    }

    public void setZmblzcSjmAmt(BigDecimal zmblzcSjmAmt) {
        this.zmblzcSjmAmt = zmblzcSjmAmt;
    }

    public BigDecimal getSzblzcBjmAmt() {
        return szblzcBjmAmt;
    }

    public void setSzblzcBjmAmt(BigDecimal szblzcBjmAmt) {
        this.szblzcBjmAmt = szblzcBjmAmt;
    }

    public BigDecimal getSzblzcSjmAmt() {
        return szblzcSjmAmt;
    }

    public void setSzblzcSjmAmt(BigDecimal szblzcSjmAmt) {
        this.szblzcSjmAmt = szblzcSjmAmt;
    }

    public String getBz() {
        return bz;
    }

    public void setBz(String bz) {
        this.bz = bz;
    }

    public String getParentProduct() {
        return parentProduct;
    }

    public void setParentProduct(String parentProduct) {
        this.parentProduct = parentProduct;
    }
}