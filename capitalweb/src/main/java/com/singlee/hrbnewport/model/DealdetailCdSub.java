package com.singlee.hrbnewport.model;

/**
 *
 * 2021/12/6
 * @Auther:zk
 */    
    
/**
    * 业务明细-同业存单申购信息
    */
public class DealdetailCdSub {
    /**
    * 申购机构
    */
    private String cust;

    /**
    * 到期日
    */
    private String mdate;

    /**
    * 起息日
    */
    private String vdate;

    /**
    * 成交量（亿元）
    */
    private String amt;

    /**
    * 申购机构是否是货币基金
    */
    private String isfund;

    public String getCust() {
        return cust;
    }

    public void setCust(String cust) {
        this.cust = cust;
    }

    public String getMdate() {
        return mdate;
    }

    public void setMdate(String mdate) {
        this.mdate = mdate;
    }

    public String getVdate() {
        return vdate;
    }

    public void setVdate(String vdate) {
        this.vdate = vdate;
    }

    public String getAmt() {
        return amt;
    }

    public void setAmt(String amt) {
        this.amt = amt;
    }

    public String getIsfund() {
        return isfund;
    }

    public void setIsfund(String isfund) {
        this.isfund = isfund;
    }
}