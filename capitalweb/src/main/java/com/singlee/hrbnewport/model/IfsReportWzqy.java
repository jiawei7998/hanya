package com.singlee.hrbnewport.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

/**
 * 外债签约信息报送表
 */
@Table(name = "IFS_REPORT_WZQY")
public class IfsReportWzqy implements Serializable {
    /**
     * 外债人代码
     */
    @Column(name = "WZRDM")
    private String wzrdm;

    /**
     * 债务类型
     */
    @Column(name = "ZWLX")
    private String zwlx;

    /**
     * 二级债务类型
     */
    @Column(name = "EJZWLX")
    private String ejzwlx;

    /**
     * 货币类型
     */
    @Column(name = "HBLX")
    private String hblx;

    /**
     * 起息日
     */
    @Column(name = "QXR")
    private String qxr;

    /**
     * 债权人总部所在国家(地区)代码
     */
    @Column(name = "ZQRZBGJDM")
    private String zqrzbgjdm;

    /**
     * 债权人经营地所在国家(地区)代码
     */
    @Column(name = "ZQRJYDGJDM")
    private String zqrjydgjdm;

    /**
     * 债权人类型代码
     */
    @Column(name = "ZQRLXDM")
    private String zqrlxdm;

    /**
     * 债权人类型二级代码
     */
    @Column(name = "ZQRLXEJDM")
    private String zqrlxejdm;

    /**
     * 币种
     */
    @Column(name = "BIZ")
    private String biz;

    /**
     * SWIFTCODE
     */
    @Column(name = "SWIFTCODE")
    private String swiftcode;

    /**
     * 债权人代码
     */
    @Column(name = "ZQRDM")
    private String zqrdm;

    /**
     * 债权人中文名
     */
    @Column(name = "ZQRZWM")
    private String zqrzwm;

    /**
     * 债权人英文名
     */
    @Column(name = "ZQRYWM")
    private String zqrywm;

    /**
     * 是否不纳入跨境融资风险加权余额计算
     */
    @Column(name = "SFBNRFXJQYEJS")
    private String sfbnrfxjqyejs;

    /**
     * 是否浮动利率
     */
    @Column(name = "SFFDLL")
    private String sffdll;

    /**
     * 年化利率值
     */
    @Column(name = "NHLLZ")
    private BigDecimal nhllz;

    /**
     * 存款业务类型
     */
    @Column(name = "CKYWLX")
    private String ckywlx;

    /**
     * 对方与本方的关系
     */
    @Column(name = "DFYBFGX")
    private String dfybfgx;

    /**
     * 原始期限
     */
    @Column(name = "YSQX")
    private String ysqx;

    /**
     * 币种2
     */
    @Column(name = "BIZ2")
    private String biz2;

    /**
     * SWIFTCODE2
     */
    @Column(name = "SWIFTCODE2")
    private String swiftcode2;

    /**
     * 备注
     */
    @Column(name = "BEIZ")
    private String beiz;

    private static final long serialVersionUID = 1L;

    /**
     * 获取外债人代码
     *
     * @return WZRDM - 外债人代码
     */
    public String getWzrdm() {
        return wzrdm;
    }

    /**
     * 设置外债人代码
     *
     * @param wzrdm 外债人代码
     */
    public void setWzrdm(String wzrdm) {
        this.wzrdm = wzrdm;
    }

    /**
     * 获取债务类型
     *
     * @return ZWLX - 债务类型
     */
    public String getZwlx() {
        return zwlx;
    }

    /**
     * 设置债务类型
     *
     * @param zwlx 债务类型
     */
    public void setZwlx(String zwlx) {
        this.zwlx = zwlx;
    }

    /**
     * 获取二级债务类型
     *
     * @return EJZWLX - 二级债务类型
     */
    public String getEjzwlx() {
        return ejzwlx;
    }

    /**
     * 设置二级债务类型
     *
     * @param ejzwlx 二级债务类型
     */
    public void setEjzwlx(String ejzwlx) {
        this.ejzwlx = ejzwlx;
    }

    /**
     * 获取货币类型
     *
     * @return HBLX - 货币类型
     */
    public String getHblx() {
        return hblx;
    }

    /**
     * 设置货币类型
     *
     * @param hblx 货币类型
     */
    public void setHblx(String hblx) {
        this.hblx = hblx;
    }

    /**
     * 获取起息日
     *
     * @return QXR - 起息日
     */
    public String getQxr() {
        return qxr;
    }

    /**
     * 设置起息日
     *
     * @param qxr 起息日
     */
    public void setQxr(String qxr) {
        this.qxr = qxr;
    }

    /**
     * 获取债权人总部所在国家(地区)代码
     *
     * @return ZQRZBGJDM - 债权人总部所在国家(地区)代码
     */
    public String getZqrzbgjdm() {
        return zqrzbgjdm;
    }

    /**
     * 设置债权人总部所在国家(地区)代码
     *
     * @param zqrzbgjdm 债权人总部所在国家(地区)代码
     */
    public void setZqrzbgjdm(String zqrzbgjdm) {
        this.zqrzbgjdm = zqrzbgjdm;
    }

    /**
     * 获取债权人经营地所在国家(地区)代码
     *
     * @return ZQRJYDGJDM - 债权人经营地所在国家(地区)代码
     */
    public String getZqrjydgjdm() {
        return zqrjydgjdm;
    }

    /**
     * 设置债权人经营地所在国家(地区)代码
     *
     * @param zqrjydgjdm 债权人经营地所在国家(地区)代码
     */
    public void setZqrjydgjdm(String zqrjydgjdm) {
        this.zqrjydgjdm = zqrjydgjdm;
    }

    /**
     * 获取债权人类型代码
     *
     * @return ZQRLXDM - 债权人类型代码
     */
    public String getZqrlxdm() {
        return zqrlxdm;
    }

    /**
     * 设置债权人类型代码
     *
     * @param zqrlxdm 债权人类型代码
     */
    public void setZqrlxdm(String zqrlxdm) {
        this.zqrlxdm = zqrlxdm;
    }

    /**
     * 获取债权人类型二级代码
     *
     * @return ZQRLXEJDM - 债权人类型二级代码
     */
    public String getZqrlxejdm() {
        return zqrlxejdm;
    }

    /**
     * 设置债权人类型二级代码
     *
     * @param zqrlxejdm 债权人类型二级代码
     */
    public void setZqrlxejdm(String zqrlxejdm) {
        this.zqrlxejdm = zqrlxejdm;
    }

    /**
     * 获取币种
     *
     * @return BIZ - 币种
     */
    public String getBiz() {
        return biz;
    }

    /**
     * 设置币种
     *
     * @param biz 币种
     */
    public void setBiz(String biz) {
        this.biz = biz;
    }

    /**
     * 获取SWIFTCODE
     *
     * @return SWIFTCODE - SWIFTCODE
     */
    public String getSwiftcode() {
        return swiftcode;
    }

    /**
     * 设置SWIFTCODE
     *
     * @param swiftcode SWIFTCODE
     */
    public void setSwiftcode(String swiftcode) {
        this.swiftcode = swiftcode;
    }

    /**
     * 获取债权人代码
     *
     * @return ZQRDM - 债权人代码
     */
    public String getZqrdm() {
        return zqrdm;
    }

    /**
     * 设置债权人代码
     *
     * @param zqrdm 债权人代码
     */
    public void setZqrdm(String zqrdm) {
        this.zqrdm = zqrdm;
    }

    /**
     * 获取债权人中文名
     *
     * @return ZQRZWM - 债权人中文名
     */
    public String getZqrzwm() {
        return zqrzwm;
    }

    /**
     * 设置债权人中文名
     *
     * @param zqrzwm 债权人中文名
     */
    public void setZqrzwm(String zqrzwm) {
        this.zqrzwm = zqrzwm;
    }

    /**
     * 获取债权人英文名
     *
     * @return ZQRYWM - 债权人英文名
     */
    public String getZqrywm() {
        return zqrywm;
    }

    /**
     * 设置债权人英文名
     *
     * @param zqrywm 债权人英文名
     */
    public void setZqrywm(String zqrywm) {
        this.zqrywm = zqrywm;
    }

    /**
     * 获取是否不纳入跨境融资风险加权余额计算
     *
     * @return SFBNRFXJQYEJS - 是否不纳入跨境融资风险加权余额计算
     */
    public String getSfbnrfxjqyejs() {
        return sfbnrfxjqyejs;
    }

    /**
     * 设置是否不纳入跨境融资风险加权余额计算
     *
     * @param sfbnrfxjqyejs 是否不纳入跨境融资风险加权余额计算
     */
    public void setSfbnrfxjqyejs(String sfbnrfxjqyejs) {
        this.sfbnrfxjqyejs = sfbnrfxjqyejs;
    }

    /**
     * 获取是否浮动利率
     *
     * @return SFFDLL - 是否浮动利率
     */
    public String getSffdll() {
        return sffdll;
    }

    /**
     * 设置是否浮动利率
     *
     * @param sffdll 是否浮动利率
     */
    public void setSffdll(String sffdll) {
        this.sffdll = sffdll;
    }

    /**
     * 获取年化利率值
     *
     * @return NHLLZ - 年化利率值
     */
    public BigDecimal getNhllz() {
        return nhllz;
    }

    /**
     * 设置年化利率值
     *
     * @param nhllz 年化利率值
     */
    public void setNhllz(BigDecimal nhllz) {
        this.nhllz = nhllz;
    }

    /**
     * 获取存款业务类型
     *
     * @return CKYWLX - 存款业务类型
     */
    public String getCkywlx() {
        return ckywlx;
    }

    /**
     * 设置存款业务类型
     *
     * @param ckywlx 存款业务类型
     */
    public void setCkywlx(String ckywlx) {
        this.ckywlx = ckywlx;
    }

    /**
     * 获取对方与本方的关系
     *
     * @return DFYBFGX - 对方与本方的关系
     */
    public String getDfybfgx() {
        return dfybfgx;
    }

    /**
     * 设置对方与本方的关系
     *
     * @param dfybfgx 对方与本方的关系
     */
    public void setDfybfgx(String dfybfgx) {
        this.dfybfgx = dfybfgx;
    }

    /**
     * 获取原始期限
     *
     * @return YSQX - 原始期限
     */
    public String getYsqx() {
        return ysqx;
    }

    /**
     * 设置原始期限
     *
     * @param ysqx 原始期限
     */
    public void setYsqx(String ysqx) {
        this.ysqx = ysqx;
    }

    public String getBiz2() {
        return biz2;
    }

    public void setBiz2(String biz2) {
        this.biz2 = biz2;
    }

    public String getSwiftcode2() {
        return swiftcode2;
    }

    public void setSwiftcode2(String swiftcode2) {
        this.swiftcode2 = swiftcode2;
    }

    /**
     * 获取备注
     *
     * @return BEIZ - 备注
     */
    public String getBeiz() {
        return beiz;
    }

    /**
     * 设置备注
     *
     * @param beiz 备注
     */
    public void setBeiz(String beiz) {
        this.beiz = beiz;
    }
}