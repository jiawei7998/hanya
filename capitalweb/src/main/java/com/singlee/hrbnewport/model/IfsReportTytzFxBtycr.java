package com.singlee.hrbnewport.model;

import java.io.Serializable;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 16:33
 * @description：${description}
 * @modified By：
 * @version:   
 */
public class IfsReportTytzFxBtycr implements Serializable {
    /**
    * 序号
    */
    private String ser;

    /**
    * 分行名称（含总行）
    */
    private String fhnc;

    /**
    * 交易对手总部全称
    */
    private String jydszbqc;

    /**
    * 活期/定期
    */
    private String qxlx;

    /**
    * 业务开始日期
    */
    private String ywkssj;

    /**
    * 业务结束日期
    */
    private String ywjssj;

    /**
    * 利率 （%）
    */
    private String rate;

    /**
    * 金额（元）
    */
    private String amt;

    /**
    * 余额（元）
    */
    private String ye;

    /**
    * 记账科目
    */
    private String jzkm;

    /**
    * 备注
    */
    private String beizhu;

    private static final long serialVersionUID = 1L;

    public String getSer() {
        return ser;
    }

    public void setSer(String ser) {
        this.ser = ser;
    }

    public String getFhnc() {
        return fhnc;
    }

    public void setFhnc(String fhnc) {
        this.fhnc = fhnc;
    }

    public String getJydszbqc() {
        return jydszbqc;
    }

    public void setJydszbqc(String jydszbqc) {
        this.jydszbqc = jydszbqc;
    }

    public String getQxlx() {
        return qxlx;
    }

    public void setQxlx(String qxlx) {
        this.qxlx = qxlx;
    }

    public String getYwkssj() {
        return ywkssj;
    }

    public void setYwkssj(String ywkssj) {
        this.ywkssj = ywkssj;
    }

    public String getYwjssj() {
        return ywjssj;
    }

    public void setYwjssj(String ywjssj) {
        this.ywjssj = ywjssj;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getAmt() {
        return amt;
    }

    public void setAmt(String amt) {
        this.amt = amt;
    }

    public String getYe() {
        return ye;
    }

    public void setYe(String ye) {
        this.ye = ye;
    }

    public String getJzkm() {
        return jzkm;
    }

    public void setJzkm(String jzkm) {
        this.jzkm = jzkm;
    }

    public String getBeizhu() {
        return beizhu;
    }

    public void setBeizhu(String beizhu) {
        this.beizhu = beizhu;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", ser=").append(ser);
        sb.append(", fhnc=").append(fhnc);
        sb.append(", jydszbqc=").append(jydszbqc);
        sb.append(", qxlx=").append(qxlx);
        sb.append(", ywkssj=").append(ywkssj);
        sb.append(", ywjssj=").append(ywjssj);
        sb.append(", rate=").append(rate);
        sb.append(", amt=").append(amt);
        sb.append(", ye=").append(ye);
        sb.append(", jzkm=").append(jzkm);
        sb.append(", beizhu=").append(beizhu);
        sb.append("]");
        return sb.toString();
    }
}