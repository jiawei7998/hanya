package com.singlee.hrbnewport.model;

import java.math.BigDecimal;

/**
    * 债券投资结构报表
    */
public class TbBondInvestment {
    /**
    * 投资账户类型
    */
    private String accountType;

    /**
    * 业务类型
    */
    private String productType;

    /**
    * 本日面值(亿元)
    */
    private BigDecimal todayFaceamt;

    /**
    * 上日面值(亿元)
    */
    private BigDecimal ondayFaceamt;

    /**
    * 增减(+/-)(亿元)
    */
    private BigDecimal increaseDecrease;

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType == null ? null : accountType.trim();
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType == null ? null : productType.trim();
    }

    public BigDecimal getTodayFaceamt() {
        return todayFaceamt;
    }

    public void setTodayFaceamt(BigDecimal todayFaceamt) {
        this.todayFaceamt = todayFaceamt;
    }

    public BigDecimal getOndayFaceamt() {
        return ondayFaceamt;
    }

    public void setOndayFaceamt(BigDecimal ondayFaceamt) {
        this.ondayFaceamt = ondayFaceamt;
    }

    public BigDecimal getIncreaseDecrease() {
        return increaseDecrease;
    }

    public void setIncreaseDecrease(BigDecimal increaseDecrease) {
        this.increaseDecrease = increaseDecrease;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", accountType=").append(accountType);
        sb.append(", productType=").append(productType);
        sb.append(", todayFaceamt=").append(todayFaceamt);
        sb.append(", ondayFaceamt=").append(ondayFaceamt);
        sb.append(", increaseDecrease=").append(increaseDecrease);
        sb.append("]");
        return sb.toString();
    }
}