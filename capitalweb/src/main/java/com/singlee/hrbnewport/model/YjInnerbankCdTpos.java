package com.singlee.hrbnewport.model;

import java.math.BigDecimal;

/**
 *
 * 2021/12/8
 * @Auther:zk
 */    
    
/**
    * 同业业务管理报告-同业存单投资
    */
public class YjInnerbankCdTpos {
    /**
    * 序号
    */
    private String seq;

    /**
    * 存单代码
    */
    private String secid;

    /**
    * 存单名称
    */
    private String secName;

    /**
    * 起息日
    */
    private String vdate;

    /**
    * 期限
    */
    private String tenor;

    /**
    * 到期日
    */
    private String mdate;

    /**
    * 债券面值
    */
    private BigDecimal faceAmt;

    /**
    * 到期收益率
    */
    private BigDecimal field;

    public String getSeq() {
        return seq;
    }

    public void setSeq(String seq) {
        this.seq = seq;
    }

    public String getSecid() {
        return secid;
    }

    public void setSecid(String secid) {
        this.secid = secid;
    }

    public String getSecName() {
        return secName;
    }

    public void setSecName(String secName) {
        this.secName = secName;
    }

    public String getVdate() {
        return vdate;
    }

    public void setVdate(String vdate) {
        this.vdate = vdate;
    }

    public String getTenor() {
        return tenor;
    }

    public void setTenor(String tenor) {
        this.tenor = tenor;
    }

    public String getMdate() {
        return mdate;
    }

    public void setMdate(String mdate) {
        this.mdate = mdate;
    }

    public BigDecimal getFaceAmt() {
        return faceAmt;
    }

    public void setFaceAmt(BigDecimal faceAmt) {
        this.faceAmt = faceAmt;
    }

    public BigDecimal getField() {
        return field;
    }

    public void setField(BigDecimal field) {
        this.field = field;
    }
}