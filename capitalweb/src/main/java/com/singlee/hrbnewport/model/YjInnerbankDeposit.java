package com.singlee.hrbnewport.model;

import java.math.BigDecimal;

/**
 *
 * 2021/12/8
 * @Auther:zk
 */    
    
/**
    * 同业业务管理报告-买入返售债券业务明细表
    */
public class YjInnerbankDeposit {
    /**
    * 序号
    */
    private String seq;

    /**
    * 交易对手
    */
    private String cust;

    /**
    * 业务开始日期
    */
    private String vdate;

    /**
    * 业务结束日期
    */
    private String mdate;

    /**
    * 利率
    */
    private BigDecimal rate;

    /**
    * 金额
    */
    private BigDecimal amt;

    /**
    * 交易方向 CT-存放同业/TC-同业存放/CC-拆放同业/CR-同业拆入
    */
    private String dealDir;

    public String getSeq() {
        return seq;
    }

    public void setSeq(String seq) {
        this.seq = seq;
    }

    public String getCust() {
        return cust;
    }

    public void setCust(String cust) {
        this.cust = cust;
    }

    public String getVdate() {
        return vdate;
    }

    public void setVdate(String vdate) {
        this.vdate = vdate;
    }

    public String getMdate() {
        return mdate;
    }

    public void setMdate(String mdate) {
        this.mdate = mdate;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public BigDecimal getAmt() {
        return amt;
    }

    public void setAmt(BigDecimal amt) {
        this.amt = amt;
    }

    public String getDealDir() {
        return dealDir;
    }

    public void setDealDir(String dealDir) {
        this.dealDir = dealDir;
    }
}