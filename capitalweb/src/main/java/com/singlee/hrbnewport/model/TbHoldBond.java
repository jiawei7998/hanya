package com.singlee.hrbnewport.model;

import java.math.BigDecimal;

/**
    * 持有债券结构情况表
    */
public class TbHoldBond {
    /**
    * 风险账户类型
    */
    private String accountType;

    /**
    * 风险账户债券占比
    */
    private BigDecimal accountBondRatio;

    /**
    * 业务类型
    */
    private String businessType;

    /**
    * 业务类型债券占比
    */
    private BigDecimal businessBondRatio;

    /**
    * 债券类型
    */
    private String bondType;

    /**
    * 债券类型占比
    */
    private BigDecimal bondTypeRatio;

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public BigDecimal getAccountBondRatio() {
        return accountBondRatio;
    }

    public void setAccountBondRatio(BigDecimal accountBondRatio) {
        this.accountBondRatio = accountBondRatio;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public BigDecimal getBusinessBondRatio() {
        return businessBondRatio;
    }

    public void setBusinessBondRatio(BigDecimal businessBondRatio) {
        this.businessBondRatio = businessBondRatio;
    }

    public String getBondType() {
        return bondType;
    }

    public void setBondType(String bondType) {
        this.bondType = bondType;
    }

    public BigDecimal getBondTypeRatio() {
        return bondTypeRatio;
    }

    public void setBondTypeRatio(BigDecimal bondTypeRatio) {
        this.bondTypeRatio = bondTypeRatio;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", accountType=").append(accountType);
        sb.append(", accountBondRatio=").append(accountBondRatio);
        sb.append(", businessType=").append(businessType);
        sb.append(", businessBondRatio=").append(businessBondRatio);
        sb.append(", bondType=").append(bondType);
        sb.append(", bondTypeRatio=").append(bondTypeRatio);
        sb.append("]");
        return sb.toString();
    }
}