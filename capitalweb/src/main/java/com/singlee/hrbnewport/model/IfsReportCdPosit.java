package com.singlee.hrbnewport.model;

import java.math.BigDecimal;

/**
 *
 * 2021/12/9
 * @Auther:zk
 */    
    
/**
    * 同 业 存 单
    */
public class IfsReportCdPosit {
    /**
    * 序号
    */
    private String seq;

    /**
    * 存单代码
    */
    private String secid;

    /**
    * 分类
    */
    private String acctngtype;

    /**
    * 认购人
    */
    private String cust;

    /**
    * 是否是货币市场基金
    */
    private String isFund;

    /**
    * 存单面值
    */
    private BigDecimal faceAmt;

    /**
    * 应缴纳金额
    */
    private BigDecimal amt;

    /**
    * 利息调整
    */
    private BigDecimal interestAdj;

    /**
    * 利息调整余额
    */
    private BigDecimal unamortamt;

    /**
    * 起息日
    */
    private String vdate;

    /**
    * 到期日
    */
    private String mdate;

    /**
    * 发行天数
    */
    private String issDays;

    /**
    * 直线摊销法摊销
    */
    private BigDecimal amortizeStraight;

    /**
    * 已计提
    */
    private BigDecimal accrual;

    /**
    * 下月最后一天
    */
    private String nxMonthEnd;

    private String days;

    /**
    * 下个月计提
    */
    private BigDecimal nxAccrual;

    /**
    * 下个月第一天
    */
    private String nxMonthStart;

    /**
    * 下个月计提天数
    */
    private String nxMonthDays;

    /**
    * 报送月份
    */
    private String mon;

    public String getSeq() {
        return seq;
    }

    public void setSeq(String seq) {
        this.seq = seq;
    }

    public String getSecid() {
        return secid;
    }

    public void setSecid(String secid) {
        this.secid = secid;
    }

    public String getAcctngtype() {
        return acctngtype;
    }

    public void setAcctngtype(String acctngtype) {
        this.acctngtype = acctngtype;
    }

    public String getCust() {
        return cust;
    }

    public void setCust(String cust) {
        this.cust = cust;
    }

    public String getIsFund() {
        return isFund;
    }

    public void setIsFund(String isFund) {
        this.isFund = isFund;
    }

    public BigDecimal getFaceAmt() {
        return faceAmt;
    }

    public void setFaceAmt(BigDecimal faceAmt) {
        this.faceAmt = faceAmt;
    }

    public BigDecimal getAmt() {
        return amt;
    }

    public void setAmt(BigDecimal amt) {
        this.amt = amt;
    }

    public BigDecimal getInterestAdj() {
        return interestAdj;
    }

    public void setInterestAdj(BigDecimal interestAdj) {
        this.interestAdj = interestAdj;
    }

    public BigDecimal getUnamortamt() {
        return unamortamt;
    }

    public void setUnamortamt(BigDecimal unamortamt) {
        this.unamortamt = unamortamt;
    }

    public String getVdate() {
        return vdate;
    }

    public void setVdate(String vdate) {
        this.vdate = vdate;
    }

    public String getMdate() {
        return mdate;
    }

    public void setMdate(String mdate) {
        this.mdate = mdate;
    }

    public String getIssDays() {
        return issDays;
    }

    public void setIssDays(String issDays) {
        this.issDays = issDays;
    }

    public BigDecimal getAmortizeStraight() {
        return amortizeStraight;
    }

    public void setAmortizeStraight(BigDecimal amortizeStraight) {
        this.amortizeStraight = amortizeStraight;
    }

    public BigDecimal getAccrual() {
        return accrual;
    }

    public void setAccrual(BigDecimal accrual) {
        this.accrual = accrual;
    }

    public String getNxMonthEnd() {
        return nxMonthEnd;
    }

    public void setNxMonthEnd(String nxMonthEnd) {
        this.nxMonthEnd = nxMonthEnd;
    }

    public String getDays() {
        return days;
    }

    public void setDays(String days) {
        this.days = days;
    }

    public BigDecimal getNxAccrual() {
        return nxAccrual;
    }

    public void setNxAccrual(BigDecimal nxAccrual) {
        this.nxAccrual = nxAccrual;
    }

    public String getNxMonthStart() {
        return nxMonthStart;
    }

    public void setNxMonthStart(String nxMonthStart) {
        this.nxMonthStart = nxMonthStart;
    }

    public String getNxMonthDays() {
        return nxMonthDays;
    }

    public void setNxMonthDays(String nxMonthDays) {
        this.nxMonthDays = nxMonthDays;
    }

    public String getMon() {
        return mon;
    }

    public void setMon(String mon) {
        this.mon = mon;
    }
}