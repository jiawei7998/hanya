package com.singlee.hrbnewport.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

/**
 * @author ：meigy
 * @date ：Created in 2021/11/19 17:06
 * @description：${description}
 * @modified By：
 * @version:
 */

/**
 * 债券质押情况表
 */
@Table(name = "IFS_REPORT_JRSCBYWGL_ZQZY")
public class IfsReportJrscbywglZqzy implements Serializable {
    /**
     * 项目
     */
    @Column(name = "PRODUCT")
    private String product;

    /**
     * 押券业务余额
     */
    @Column(name = "YQYWYE")
    private BigDecimal yqywye;

    /**
     * 国债
     */
    @Column(name = "GZ")
    private BigDecimal gz;

    /**
     * 地方政府债
     */
    @Column(name = "DFZFZ")
    private BigDecimal dfzfz;

    /**
     * 政策性金融债
     */
    @Column(name = "ZCXJRZ")
    private BigDecimal zcxjrz;

    /**
     * 商业银行及非银行金融机构债
     */
    @Column(name = "SYXHJFYXJRJGZ")
    private BigDecimal syxhjfyxjrjgz;

    /**
     * 存单
     */
    @Column(name = "CD")
    private BigDecimal cd;

    /**
     * 信用债（中债）
     */
    @Column(name = "XYZZZ")
    private BigDecimal xyzzz;

    /**
     * 信用债（上清）
     */
    @Column(name = "XYZSQ")
    private BigDecimal xyzsq;

    /**
     * 铁道债
     */
    @Column(name = "TDZ")
    private BigDecimal tdz;

    /**
     * 合计
     */
    @Column(name = "HJ")
    private BigDecimal hj;

    @Transient
    private String producto;

    private static final long serialVersionUID = 1L;

    /**
     * 获取项目
     *
     * @return PRODUCT - 项目
     */
    public String getProduct() {
        return product;
    }

    /**
     * 设置项目
     *
     * @param product 项目
     */
    public void setProduct(String product) {
        this.product = product;
    }

    /**
     * 获取押券业务余额
     *
     * @return YQYWYE - 押券业务余额
     */
    public BigDecimal getYqywye() {
        return yqywye;
    }

    /**
     * 设置押券业务余额
     *
     * @param yqywye 押券业务余额
     */
    public void setYqywye(BigDecimal yqywye) {
        this.yqywye = yqywye;
    }

    /**
     * 获取国债
     *
     * @return GZ - 国债
     */
    public BigDecimal getGz() {
        return gz;
    }

    /**
     * 设置国债
     *
     * @param gz 国债
     */
    public void setGz(BigDecimal gz) {
        this.gz = gz;
    }

    /**
     * 获取地方政府债
     *
     * @return DFZFZ - 地方政府债
     */
    public BigDecimal getDfzfz() {
        return dfzfz;
    }

    /**
     * 设置地方政府债
     *
     * @param dfzfz 地方政府债
     */
    public void setDfzfz(BigDecimal dfzfz) {
        this.dfzfz = dfzfz;
    }

    /**
     * 获取政策性金融债
     *
     * @return ZCXJRZ - 政策性金融债
     */
    public BigDecimal getZcxjrz() {
        return zcxjrz;
    }

    /**
     * 设置政策性金融债
     *
     * @param zcxjrz 政策性金融债
     */
    public void setZcxjrz(BigDecimal zcxjrz) {
        this.zcxjrz = zcxjrz;
    }

    /**
     * 获取商业银行及非银行金融机构债
     *
     * @return SYXHJFYXJRJGZ - 商业银行及非银行金融机构债
     */
    public BigDecimal getSyxhjfyxjrjgz() {
        return syxhjfyxjrjgz;
    }

    /**
     * 设置商业银行及非银行金融机构债
     *
     * @param syxhjfyxjrjgz 商业银行及非银行金融机构债
     */
    public void setSyxhjfyxjrjgz(BigDecimal syxhjfyxjrjgz) {
        this.syxhjfyxjrjgz = syxhjfyxjrjgz;
    }

    /**
     * 获取存单
     *
     * @return CD - 存单
     */
    public BigDecimal getCd() {
        return cd;
    }

    /**
     * 设置存单
     *
     * @param cd 存单
     */
    public void setCd(BigDecimal cd) {
        this.cd = cd;
    }

    /**
     * 获取信用债（中债）
     *
     * @return XYZZZ - 信用债（中债）
     */
    public BigDecimal getXyzzz() {
        return xyzzz;
    }

    /**
     * 设置信用债（中债）
     *
     * @param xyzzz 信用债（中债）
     */
    public void setXyzzz(BigDecimal xyzzz) {
        this.xyzzz = xyzzz;
    }

    /**
     * 获取信用债（上清）
     *
     * @return XYZSQ - 信用债（上清）
     */
    public BigDecimal getXyzsq() {
        return xyzsq;
    }

    /**
     * 设置信用债（上清）
     *
     * @param xyzsq 信用债（上清）
     */
    public void setXyzsq(BigDecimal xyzsq) {
        this.xyzsq = xyzsq;
    }

    /**
     * 获取铁道债
     *
     * @return TDZ - 铁道债
     */
    public BigDecimal getTdz() {
        return tdz;
    }

    /**
     * 设置铁道债
     *
     * @param tdz 铁道债
     */
    public void setTdz(BigDecimal tdz) {
        this.tdz = tdz;
    }

    /**
     * 获取合计
     *
     * @return HJ - 合计
     */
    public BigDecimal getHj() {
        return hj;
    }

    /**
     * 设置合计
     *
     * @param hj 合计
     */
    public void setHj(BigDecimal hj) {
        this.hj = hj;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }
}