package com.singlee.hrbnewport.model;

import java.math.BigDecimal;

/**
 *
 * 2021/12/8
 * @Auther:zk
 */    
    
/**
    * 同业业务管理报告-买入返售债券业务明细表
    */
public class YjInnerbankRvrepo {
    /**
    * 序号
    */
    private String seq;

    /**
    * 交易方向
    */
    private String dealDir;

    /**
    * 首次结算日
    */
    private String vdate;

    /**
    * 实际占款天数
    */
    private String tenor;

    /**
    * 到期结算日
    */
    private String mdate;

    /**
    * 回购利率
    */
    private BigDecimal repoRate;

    /**
    * 回购天数
    */
    private String repoDay;

    /**
    * 交易金额
    */
    private BigDecimal amt;

    /**
    * 质押物明细
    */
    private String pledgeDetail;

    public String getSeq() {
        return seq;
    }

    public void setSeq(String seq) {
        this.seq = seq;
    }

    public String getDealDir() {
        return dealDir;
    }

    public void setDealDir(String dealDir) {
        this.dealDir = dealDir;
    }

    public String getVdate() {
        return vdate;
    }

    public void setVdate(String vdate) {
        this.vdate = vdate;
    }

    public String getTenor() {
        return tenor;
    }

    public void setTenor(String tenor) {
        this.tenor = tenor;
    }

    public String getMdate() {
        return mdate;
    }

    public void setMdate(String mdate) {
        this.mdate = mdate;
    }

    public BigDecimal getRepoRate() {
        return repoRate;
    }

    public void setRepoRate(BigDecimal repoRate) {
        this.repoRate = repoRate;
    }

    public String getRepoDay() {
        return repoDay;
    }

    public void setRepoDay(String repoDay) {
        this.repoDay = repoDay;
    }

    public BigDecimal getAmt() {
        return amt;
    }

    public void setAmt(BigDecimal amt) {
        this.amt = amt;
    }

    public String getPledgeDetail() {
        return pledgeDetail;
    }

    public void setPledgeDetail(String pledgeDetail) {
        this.pledgeDetail = pledgeDetail;
    }
}