package com.singlee.hrbnewport.model;

import java.util.Date;

/**
 * 导出日志表
 */
public class TaReportLog {
	/**
	 * 用户id
	 */
	private String userId;

	/**
	 * 用户名称
	 */
	private String userName;

	/**
	 * 导出文件名称
	 */
	private String exportName;

	/**
	 * 导出文件所属机构
	 */
	private String exportInst;

	/**
	 * 导出时间
	 */
	private Date exportTime;

	/**
	 * 导出状态
	 */
	private String exportStatus;

	/**
	 * 备注
	 */
	private String remark;


	/**
	 * 导出耗时
	 */
	private Long timeCost;

	/**
	 * ip地址
	 */
	private String ip;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getExportName() {
		return exportName;
	}

	public void setExportName(String exportName) {
		this.exportName = exportName;
	}

	public String getExportInst() {
		return exportInst;
	}

	public void setExportInst(String exportInst) {
		this.exportInst = exportInst;
	}

	public Date getExportTime() {
		return exportTime;
	}

	public void setExportTime(Date exportTime) {
		this.exportTime = exportTime;
	}

	public Long getTimeCost() {
		return timeCost;
	}

	public void setTimeCost(Long timeCost) {
		this.timeCost = timeCost;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getExportStatus() {
		return exportStatus;
	}

	public void setExportStatus(String exportStatus) {
		this.exportStatus = exportStatus;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public String toString() {
		return "TaReportLog{" +
				"userId='" + userId + '\'' +
				", userName='" + userName + '\'' +
				", exportName='" + exportName + '\'' +
				", exportInst='" + exportInst + '\'' +
				", exportTime=" + exportTime +
				", exportStatus=" + exportStatus +
				", timeCost=" + timeCost +
				", ip='" + ip + '\'' +
				'}';
	}
}