package com.singlee.hrbnewport.model;

import java.math.BigDecimal;

/**
 *
 * 2021/12/9
 * @Auther:zk
 */    
    
/**
    * 同业业务管理报告-债券投资业务明细表
    */
public class RhInnerbankSecTpos {
    /**
    * 序号
    */
    private String seq;

    /**
    * 债券代码
    */
    private String secid;

    /**
    * 债券名称
    */
    private String secName;

    /**
    * 起息日
    */
    private String vdate;

    /**
    * 期限
    */
    private String tenor;

    /**
    * 到期日
    */
    private String mdate;

    /**
    * 债券面值
    */
    private BigDecimal faceAmt;

    /**
    * 票面利率
    */
    private BigDecimal faceRate;

    public String getSeq() {
        return seq;
    }

    public void setSeq(String seq) {
        this.seq = seq;
    }

    public String getSecid() {
        return secid;
    }

    public void setSecid(String secid) {
        this.secid = secid;
    }

    public String getSecName() {
        return secName;
    }

    public void setSecName(String secName) {
        this.secName = secName;
    }

    public String getVdate() {
        return vdate;
    }

    public void setVdate(String vdate) {
        this.vdate = vdate;
    }

    public String getTenor() {
        return tenor;
    }

    public void setTenor(String tenor) {
        this.tenor = tenor;
    }

    public String getMdate() {
        return mdate;
    }

    public void setMdate(String mdate) {
        this.mdate = mdate;
    }

    public BigDecimal getFaceAmt() {
        return faceAmt;
    }

    public void setFaceAmt(BigDecimal faceAmt) {
        this.faceAmt = faceAmt;
    }

    public BigDecimal getFaceRate() {
        return faceRate;
    }

    public void setFaceRate(BigDecimal faceRate) {
        this.faceRate = faceRate;
    }
}