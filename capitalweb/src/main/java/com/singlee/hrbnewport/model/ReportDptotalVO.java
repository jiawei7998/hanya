package com.singlee.hrbnewport.model;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author Liang
 * @date 2021/11/6 17:35
 * =======================
 */
public class ReportDptotalVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String  pubMonth        ; //月份
    private BigDecimal  totalLoseAmount; //总负债
    private BigDecimal  sameLoseAmount; //同业负债
    private BigDecimal  sameDpsum		; //同业存单发行笔数
    private BigDecimal  sameDpmoney	; //同业存单发行金额
    private BigDecimal  sameDpbalance	; //同业存单余额

    public String getPubMonth() {
        return pubMonth;
    }

    public void setPubMonth(String pubMonth) {
        this.pubMonth = pubMonth;
    }

    public BigDecimal getTotalLoseAmount() {
        return totalLoseAmount;
    }

    public void setTotalLoseAmount(BigDecimal totalLoseAmount) {
        this.totalLoseAmount = totalLoseAmount;
    }

    public BigDecimal getSameLoseAmount() {
        return sameLoseAmount;
    }

    public void setSameLoseAmount(BigDecimal sameLoseAmount) {
        this.sameLoseAmount = sameLoseAmount;
    }

    public BigDecimal getSameDpsum() {
        return sameDpsum;
    }

    public void setSameDpsum(BigDecimal sameDpsum) {
        this.sameDpsum = sameDpsum;
    }

    public BigDecimal getSameDpmoney() {
        return sameDpmoney;
    }

    public void setSameDpmoney(BigDecimal sameDpmoney) {
        this.sameDpmoney = sameDpmoney;
    }

    public BigDecimal getSameDpbalance() {
        return sameDpbalance;
    }

    public void setSameDpbalance(BigDecimal sameDpbalance) {
        this.sameDpbalance = sameDpbalance;
    }
}
