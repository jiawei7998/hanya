package com.singlee.hrbnewport.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

/**
 * 城商行民营银行主要监管指标情况表
 */
@Table(name = "IFS_REPORT_CSHMYYHZYJGZBQKB")
public class IfsReportCshmyyhzyjgzbqkb implements Serializable {
    /**
     * 期数基础指标
     */
    @Column(name = "QSJCZS")
    private String qsjczs;

    /**
     * 基础指标
     */
    @Column(name = "JCZB")
    private String jczb;

    /**
     * 本期
     */
    @Column(name = "BQ")
    private BigDecimal bq;

    /**
     * 比上月增减_数额
     */
    @Column(name = "BSYZJ_SE")
    private BigDecimal bsyzjSe;

    /**
     * 比上月增减_幅度
     */
    @Column(name = "BSYZJ_FD")
    private BigDecimal bsyzjFd;

    /**
     * 比年初增减_数额
     */
    @Column(name = "BNCZJ_SE")
    private BigDecimal bnczjSe;

    /**
     * 比年初增减_幅度
     */
    @Column(name = "BNCZJ_FD")
    private BigDecimal bnczjFd;

    /**
     * 比去年同期增减_数额
     */
    @Column(name = "BQNTQZJ_SE")
    private BigDecimal bqntqzjSe;

    /**
     * 比去年同期增减_幅度
     */
    @Column(name = "BQNTQZJ_FD")
    private BigDecimal bqntqzjFd;

    /**
     * 牵头部门
     */
    @Column(name = "QTBM")
    private String qtbm;

    @Column(name = "PARENT_JCZB")
    private String parentJczb;

    private static final long serialVersionUID = 1L;

    /**
     * 获取期数基础指标
     *
     * @return QSJCZS - 期数基础指标
     */
    public String getQsjczs() {
        return qsjczs;
    }

    /**
     * 设置期数基础指标
     *
     * @param qsjczs 期数基础指标
     */
    public void setQsjczs(String qsjczs) {
        this.qsjczs = qsjczs;
    }

    /**
     * 获取基础指标
     *
     * @return JCZB - 基础指标
     */
    public String getJczb() {
        return jczb;
    }

    /**
     * 设置基础指标
     *
     * @param jczb 基础指标
     */
    public void setJczb(String jczb) {
        this.jczb = jczb;
    }

    /**
     * 获取本期
     *
     * @return BQ - 本期
     */
    public BigDecimal getBq() {
        return bq;
    }

    /**
     * 设置本期
     *
     * @param bq 本期
     */
    public void setBq(BigDecimal bq) {
        this.bq = bq;
    }

    /**
     * 获取比上月增减_数额
     *
     * @return BSYZJ_SE - 比上月增减_数额
     */
    public BigDecimal getBsyzjSe() {
        return bsyzjSe;
    }

    /**
     * 设置比上月增减_数额
     *
     * @param bsyzjSe 比上月增减_数额
     */
    public void setBsyzjSe(BigDecimal bsyzjSe) {
        this.bsyzjSe = bsyzjSe;
    }

    /**
     * 获取比上月增减_幅度
     *
     * @return BSYZJ_FD - 比上月增减_幅度
     */
    public BigDecimal getBsyzjFd() {
        return bsyzjFd;
    }

    /**
     * 设置比上月增减_幅度
     *
     * @param bsyzjFd 比上月增减_幅度
     */
    public void setBsyzjFd(BigDecimal bsyzjFd) {
        this.bsyzjFd = bsyzjFd;
    }

    /**
     * 获取比年初增减_数额
     *
     * @return BNCZJ_SE - 比年初增减_数额
     */
    public BigDecimal getBnczjSe() {
        return bnczjSe;
    }

    /**
     * 设置比年初增减_数额
     *
     * @param bnczjSe 比年初增减_数额
     */
    public void setBnczjSe(BigDecimal bnczjSe) {
        this.bnczjSe = bnczjSe;
    }

    /**
     * 获取比年初增减_幅度
     *
     * @return BNCZJ_FD - 比年初增减_幅度
     */
    public BigDecimal getBnczjFd() {
        return bnczjFd;
    }

    /**
     * 设置比年初增减_幅度
     *
     * @param bnczjFd 比年初增减_幅度
     */
    public void setBnczjFd(BigDecimal bnczjFd) {
        this.bnczjFd = bnczjFd;
    }

    /**
     * 获取比去年同期增减_数额
     *
     * @return BQNTQZJ_SE - 比去年同期增减_数额
     */
    public BigDecimal getBqntqzjSe() {
        return bqntqzjSe;
    }

    /**
     * 设置比去年同期增减_数额
     *
     * @param bqntqzjSe 比去年同期增减_数额
     */
    public void setBqntqzjSe(BigDecimal bqntqzjSe) {
        this.bqntqzjSe = bqntqzjSe;
    }

    /**
     * 获取比去年同期增减_幅度
     *
     * @return BQNTQZJ_FD - 比去年同期增减_幅度
     */
    public BigDecimal getBqntqzjFd() {
        return bqntqzjFd;
    }

    /**
     * 设置比去年同期增减_幅度
     *
     * @param bqntqzjFd 比去年同期增减_幅度
     */
    public void setBqntqzjFd(BigDecimal bqntqzjFd) {
        this.bqntqzjFd = bqntqzjFd;
    }

    /**
     * 获取牵头部门
     *
     * @return QTBM - 牵头部门
     */
    public String getQtbm() {
        return qtbm;
    }

    /**
     * 设置牵头部门
     *
     * @param qtbm 牵头部门
     */
    public void setQtbm(String qtbm) {
        this.qtbm = qtbm;
    }

    /**
     * @return PARENT_JCZB
     */
    public String getParentJczb() {
        return parentJczb;
    }

    /**
     * @param parentJczb
     */
    public void setParentJczb(String parentJczb) {
        this.parentJczb = parentJczb;
    }
}