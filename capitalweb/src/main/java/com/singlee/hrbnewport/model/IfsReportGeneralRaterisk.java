package com.singlee.hrbnewport.model;

/**
    * 市场风险标准法资本要求情况表（一般利率风险-到期日法）
    */
public class IfsReportGeneralRaterisk {
    /**
    * 序号
    */
    private String sort;

    /**
    * 时段
    */
    private String timeframe;

    /**
    * 年息票率大于等于3%
    */
    private String annualratel;

    /**
    * 年息票率小于3%
    */
    private String annualrates;

    /**
    * 债券多头(万元)
    */
    private String zqdt;

    /**
    * 债券空头(万元)
    */
    private String zqkt;

    /**
    * 利率多头(万元)
    */
    private String lldt;

    /**
    * 利率空头（万元）
    */
    private String llkt;

    /**
    * 总额多头(万元)
    */
    private String zedt;

    /**
    * 总额空头(万元)
    */
    private String zekt;

    /**
    * 风险权重
    */
    private String fxqz;

    /**
    * 风险多头（万元）
    */
    private String fxdt;

    /**
    * 风险空头（万元）
    */
    private String fxkt;

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort == null ? null : sort.trim();
    }

    public String getTimeframe() {
        return timeframe;
    }

    public void setTimeframe(String timeframe) {
        this.timeframe = timeframe == null ? null : timeframe.trim();
    }

    public String getAnnualratel() {
        return annualratel;
    }

    public void setAnnualratel(String annualratel) {
        this.annualratel = annualratel == null ? null : annualratel.trim();
    }

    public String getAnnualrates() {
        return annualrates;
    }

    public void setAnnualrates(String annualrates) {
        this.annualrates = annualrates == null ? null : annualrates.trim();
    }

    public String getZqdt() {
        return zqdt;
    }

    public void setZqdt(String zqdt) {
        this.zqdt = zqdt == null ? null : zqdt.trim();
    }

    public String getZqkt() {
        return zqkt;
    }

    public void setZqkt(String zqkt) {
        this.zqkt = zqkt == null ? null : zqkt.trim();
    }

    public String getLldt() {
        return lldt;
    }

    public void setLldt(String lldt) {
        this.lldt = lldt == null ? null : lldt.trim();
    }

    public String getLlkt() {
        return llkt;
    }

    public void setLlkt(String llkt) {
        this.llkt = llkt == null ? null : llkt.trim();
    }

    public String getZedt() {
        return zedt;
    }

    public void setZedt(String zedt) {
        this.zedt = zedt == null ? null : zedt.trim();
    }

    public String getZekt() {
        return zekt;
    }

    public void setZekt(String zekt) {
        this.zekt = zekt == null ? null : zekt.trim();
    }

    public String getFxqz() {
        return fxqz;
    }

    public void setFxqz(String fxqz) {
        this.fxqz = fxqz == null ? null : fxqz.trim();
    }

    public String getFxdt() {
        return fxdt;
    }

    public void setFxdt(String fxdt) {
        this.fxdt = fxdt == null ? null : fxdt.trim();
    }

    public String getFxkt() {
        return fxkt;
    }

    public void setFxkt(String fxkt) {
        this.fxkt = fxkt == null ? null : fxkt.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", sort=").append(sort);
        sb.append(", timeframe=").append(timeframe);
        sb.append(", annualratel=").append(annualratel);
        sb.append(", annualrates=").append(annualrates);
        sb.append(", zqdt=").append(zqdt);
        sb.append(", zqkt=").append(zqkt);
        sb.append(", lldt=").append(lldt);
        sb.append(", llkt=").append(llkt);
        sb.append(", zedt=").append(zedt);
        sb.append(", zekt=").append(zekt);
        sb.append(", fxqz=").append(fxqz);
        sb.append(", fxdt=").append(fxdt);
        sb.append(", fxkt=").append(fxkt);
        sb.append("]");
        return sb.toString();
    }
}