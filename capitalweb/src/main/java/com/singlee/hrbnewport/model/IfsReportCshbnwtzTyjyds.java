package com.singlee.hrbnewport.model;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author ：meigy
 * @date ：Created in 2021/11/16 15:00
 * @description：${description}
 * @modified By：
 * @version:
 */

/**
 * 同业交易对手情况表
 */
public class IfsReportCshbnwtzTyjyds implements Serializable {
    /**
     * 机构类型
     */
    private String jgtype;

    /**
     * 表内同业负债业务_本季末余额
     */
    private BigDecimal bntyfzBjmAmt;

    /**
     * 表内同业负债业务_上季末余额
     */
    private BigDecimal bntyfzSjmAmt;

    /**
     * 表内同业负债业务_年初至本季末累计资金成本率
     */
    private BigDecimal bntyfzCzbjmljAmt;

    /**
     * 表内同业负债业务_年初至上季末累计资金成本率
     */
    private BigDecimal bntyfzCzsjmljAmt;

    /**
     * 发行同业存单业务_本季末余额
     */
    private BigDecimal fxtycdBjmAmt;

    /**
     * 发行同业存单业务_上季末余额
     */
    private BigDecimal fxtycdSjmAmt;

    /**
     * 发行同业存单业务_年初至本季末累计资金成本率
     */
    private BigDecimal fxtycdCzbjmljAmt;

    /**
     * 发行同业存单业务_年初至上季末累计资金成本率
     */
    private BigDecimal fxtycdCzsjmljAmt;

    /**
     * 表内同业资产业务_本季末余额
     */
    private BigDecimal bntyzcBjmAmt;

    /**
     * 表内同业资产业务_上季末余额
     */
    private BigDecimal bntyzcSjmAmt;

    /**
     * 表内同业资产业务_年初至本季末累计资金成本率
     */
    private BigDecimal bntyzcCzbjmljAmt;

    /**
     * 表内同业资产业务_年初至上季末累计资金成本率
     */
    private BigDecimal bntyzcCzsjmljAmt;

    /**
     * 表内同业投资业务_本季末余额
     */
    private BigDecimal bntytzBjmAmt;

    /**
     * 表内同业投资业务_上季末余额
     */
    private BigDecimal bntytzSjmAmt;

    /**
     * 表内同业投资业务_年初至本季末累计资金成本率
     */
    private BigDecimal bntytzCzbjmljAmt;

    /**
     * 表内同业投资业务_年初至上季末累计资金成本率
     */
    private BigDecimal bntytzCzsjmljAmt;

    /**
     * 表外理财同业投资业务合计_本季末余额
     */
    private BigDecimal bwlctytzBjmAmt;

    /**
     * 表外理财同业投资业务合计_上季末余额
     */
    private BigDecimal bwlctytzSjmAmt;

    /**
     * 表外理财同业投资业务合计_年初至本季末累计资金成本率
     */
    private BigDecimal bwlctytzCzbjmljAmt;

    /**
     * 表外理财同业投资业务合计_年初至上季末累计资金成本率
     */
    private BigDecimal bwlctytzCzsjmljAmt;

    /**
     * 表内外同业投资业务合计_本季末余额
     */
    private BigDecimal bwwtytzBjmAmt;

    /**
     * 表内外同业投资业务合计_上季末余额
     */
    private BigDecimal bwwtytzSjmAmt;

    /**
     * 表内外同业投资业务合计_年初至本季末累计资金成本率
     */
    private BigDecimal bwwtytzCzbjmljAmt;

    /**
     * 表内外同业投资业务合计_年初至上季末累计资金成本率
     */
    private BigDecimal bwwtytzCzsjmljAmt;

    private String parentProduct;

    private static final long serialVersionUID = 1L;

    public String getJgtype() {
        return jgtype;
    }

    public void setJgtype(String jgtype) {
        this.jgtype = jgtype;
    }

    public BigDecimal getBntyfzBjmAmt() {
        return bntyfzBjmAmt;
    }

    public void setBntyfzBjmAmt(BigDecimal bntyfzBjmAmt) {
        this.bntyfzBjmAmt = bntyfzBjmAmt;
    }

    public BigDecimal getBntyfzSjmAmt() {
        return bntyfzSjmAmt;
    }

    public void setBntyfzSjmAmt(BigDecimal bntyfzSjmAmt) {
        this.bntyfzSjmAmt = bntyfzSjmAmt;
    }

    public BigDecimal getBntyfzCzbjmljAmt() {
        return bntyfzCzbjmljAmt;
    }

    public void setBntyfzCzbjmljAmt(BigDecimal bntyfzCzbjmljAmt) {
        this.bntyfzCzbjmljAmt = bntyfzCzbjmljAmt;
    }

    public BigDecimal getBntyfzCzsjmljAmt() {
        return bntyfzCzsjmljAmt;
    }

    public void setBntyfzCzsjmljAmt(BigDecimal bntyfzCzsjmljAmt) {
        this.bntyfzCzsjmljAmt = bntyfzCzsjmljAmt;
    }

    public BigDecimal getFxtycdBjmAmt() {
        return fxtycdBjmAmt;
    }

    public void setFxtycdBjmAmt(BigDecimal fxtycdBjmAmt) {
        this.fxtycdBjmAmt = fxtycdBjmAmt;
    }

    public BigDecimal getFxtycdSjmAmt() {
        return fxtycdSjmAmt;
    }

    public void setFxtycdSjmAmt(BigDecimal fxtycdSjmAmt) {
        this.fxtycdSjmAmt = fxtycdSjmAmt;
    }

    public BigDecimal getFxtycdCzbjmljAmt() {
        return fxtycdCzbjmljAmt;
    }

    public void setFxtycdCzbjmljAmt(BigDecimal fxtycdCzbjmljAmt) {
        this.fxtycdCzbjmljAmt = fxtycdCzbjmljAmt;
    }

    public BigDecimal getFxtycdCzsjmljAmt() {
        return fxtycdCzsjmljAmt;
    }

    public void setFxtycdCzsjmljAmt(BigDecimal fxtycdCzsjmljAmt) {
        this.fxtycdCzsjmljAmt = fxtycdCzsjmljAmt;
    }

    public BigDecimal getBntyzcBjmAmt() {
        return bntyzcBjmAmt;
    }

    public void setBntyzcBjmAmt(BigDecimal bntyzcBjmAmt) {
        this.bntyzcBjmAmt = bntyzcBjmAmt;
    }

    public BigDecimal getBntyzcSjmAmt() {
        return bntyzcSjmAmt;
    }

    public void setBntyzcSjmAmt(BigDecimal bntyzcSjmAmt) {
        this.bntyzcSjmAmt = bntyzcSjmAmt;
    }

    public BigDecimal getBntyzcCzbjmljAmt() {
        return bntyzcCzbjmljAmt;
    }

    public void setBntyzcCzbjmljAmt(BigDecimal bntyzcCzbjmljAmt) {
        this.bntyzcCzbjmljAmt = bntyzcCzbjmljAmt;
    }

    public BigDecimal getBntyzcCzsjmljAmt() {
        return bntyzcCzsjmljAmt;
    }

    public void setBntyzcCzsjmljAmt(BigDecimal bntyzcCzsjmljAmt) {
        this.bntyzcCzsjmljAmt = bntyzcCzsjmljAmt;
    }

    public BigDecimal getBntytzBjmAmt() {
        return bntytzBjmAmt;
    }

    public void setBntytzBjmAmt(BigDecimal bntytzBjmAmt) {
        this.bntytzBjmAmt = bntytzBjmAmt;
    }

    public BigDecimal getBntytzSjmAmt() {
        return bntytzSjmAmt;
    }

    public void setBntytzSjmAmt(BigDecimal bntytzSjmAmt) {
        this.bntytzSjmAmt = bntytzSjmAmt;
    }

    public BigDecimal getBntytzCzbjmljAmt() {
        return bntytzCzbjmljAmt;
    }

    public void setBntytzCzbjmljAmt(BigDecimal bntytzCzbjmljAmt) {
        this.bntytzCzbjmljAmt = bntytzCzbjmljAmt;
    }

    public BigDecimal getBntytzCzsjmljAmt() {
        return bntytzCzsjmljAmt;
    }

    public void setBntytzCzsjmljAmt(BigDecimal bntytzCzsjmljAmt) {
        this.bntytzCzsjmljAmt = bntytzCzsjmljAmt;
    }

    public BigDecimal getBwlctytzBjmAmt() {
        return bwlctytzBjmAmt;
    }

    public void setBwlctytzBjmAmt(BigDecimal bwlctytzBjmAmt) {
        this.bwlctytzBjmAmt = bwlctytzBjmAmt;
    }

    public BigDecimal getBwlctytzSjmAmt() {
        return bwlctytzSjmAmt;
    }

    public void setBwlctytzSjmAmt(BigDecimal bwlctytzSjmAmt) {
        this.bwlctytzSjmAmt = bwlctytzSjmAmt;
    }

    public BigDecimal getBwlctytzCzbjmljAmt() {
        return bwlctytzCzbjmljAmt;
    }

    public void setBwlctytzCzbjmljAmt(BigDecimal bwlctytzCzbjmljAmt) {
        this.bwlctytzCzbjmljAmt = bwlctytzCzbjmljAmt;
    }

    public BigDecimal getBwlctytzCzsjmljAmt() {
        return bwlctytzCzsjmljAmt;
    }

    public void setBwlctytzCzsjmljAmt(BigDecimal bwlctytzCzsjmljAmt) {
        this.bwlctytzCzsjmljAmt = bwlctytzCzsjmljAmt;
    }

    public BigDecimal getBwwtytzBjmAmt() {
        return bwwtytzBjmAmt;
    }

    public void setBwwtytzBjmAmt(BigDecimal bwwtytzBjmAmt) {
        this.bwwtytzBjmAmt = bwwtytzBjmAmt;
    }

    public BigDecimal getBwwtytzSjmAmt() {
        return bwwtytzSjmAmt;
    }

    public void setBwwtytzSjmAmt(BigDecimal bwwtytzSjmAmt) {
        this.bwwtytzSjmAmt = bwwtytzSjmAmt;
    }

    public BigDecimal getBwwtytzCzbjmljAmt() {
        return bwwtytzCzbjmljAmt;
    }

    public void setBwwtytzCzbjmljAmt(BigDecimal bwwtytzCzbjmljAmt) {
        this.bwwtytzCzbjmljAmt = bwwtytzCzbjmljAmt;
    }

    public BigDecimal getBwwtytzCzsjmljAmt() {
        return bwwtytzCzsjmljAmt;
    }

    public void setBwwtytzCzsjmljAmt(BigDecimal bwwtytzCzsjmljAmt) {
        this.bwwtytzCzsjmljAmt = bwwtytzCzsjmljAmt;
    }

    public String getParentProduct() {
        return parentProduct;
    }

    public void setParentProduct(String parentProduct) {
        this.parentProduct = parentProduct;
    }
}