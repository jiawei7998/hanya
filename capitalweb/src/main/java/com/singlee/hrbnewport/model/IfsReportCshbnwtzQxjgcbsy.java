package com.singlee.hrbnewport.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author     ：meigy
 * @date       ：Created in 2021/11/13 15:01
 * @description：${description}
 * @modified By：
 * @version:     
 */
/**
    * 表内外投资业务期限结构及成本收益表
    */
@Entity
@Table(name = "IFS_REPORT_CSHBNWTZ_QXJGCBSY")
public class IfsReportCshbnwtzQxjgcbsy implements Serializable {
    /**
    * 项目
    */
    private String product;

    /**
    * 资金来源_期限
    */
    private String zjlyQx;

    /**
    * 资金来源_余额_本季末数额
    */
    private BigDecimal zjlyYeBjmAmt;

    /**
    * 资金来源_余额_上季末数额
    */
    private BigDecimal zjlyYeSjmAmt;

    /**
    * 资金来源_平均年化资金成本_年初至本季末累计资金成本率
    */
    private BigDecimal zjlyNhcbCzbjmljAmt;

    /**
    * 资金来源_平均年化资金成本_年初至上季末累计资金成本率
    */
    private BigDecimal zjlyNhcbCzsjmljAmt;

    /**
    * 资金运用_到期期限
    */
    private String zjyyDqqx;

    /**
    * 资金运用_余额_本季末数额
    */
    private BigDecimal zjyyYeBjmAmt;

    /**
    * 资金运用_余额_上季末数额
    */
    private BigDecimal zjyyYeSjmAmt;

    /**
    * 资金运用_平均年化收益水平_年初至本季末累计年化收益率
    */
    private BigDecimal zjyyNhsyCzbjmljAmt;

    /**
    * 资金运用_平均年化收益水平_年初至上季末累计年化收益率
    */
    private BigDecimal zjyyNhsyCzsjmljAmt;

    /**
    * 资金运用_其中：非标债权_本季末数额
    */
    private BigDecimal zjyyFbzqBjmAmt;

    /**
    * 资金运用_其中：非标债权_上季末数额
    */
    private BigDecimal zjyyFbzqSjmAmt;

    /**
    * 资金运用_其中：非标债权平均年化收益水平_年初至本季末累计年化收益率
    */
    private BigDecimal zjyyFbzqnhCzbjmljAmt;

    /**
    * 资金运用_其中：非标债权平均年化收益水平_年初至上季末累计年化收益率
    */
    private BigDecimal zjyyFbzqnhCzsjmljAmt;

    private String parentZjlyQx;

    private static final long serialVersionUID = 1L;

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getZjlyQx() {
        return zjlyQx;
    }

    public void setZjlyQx(String zjlyQx) {
        this.zjlyQx = zjlyQx;
    }

    public BigDecimal getZjlyYeBjmAmt() {
        return zjlyYeBjmAmt;
    }

    public void setZjlyYeBjmAmt(BigDecimal zjlyYeBjmAmt) {
        this.zjlyYeBjmAmt = zjlyYeBjmAmt;
    }

    public BigDecimal getZjlyYeSjmAmt() {
        return zjlyYeSjmAmt;
    }

    public void setZjlyYeSjmAmt(BigDecimal zjlyYeSjmAmt) {
        this.zjlyYeSjmAmt = zjlyYeSjmAmt;
    }

    public BigDecimal getZjlyNhcbCzbjmljAmt() {
        return zjlyNhcbCzbjmljAmt;
    }

    public void setZjlyNhcbCzbjmljAmt(BigDecimal zjlyNhcbCzbjmljAmt) {
        this.zjlyNhcbCzbjmljAmt = zjlyNhcbCzbjmljAmt;
    }

    public BigDecimal getZjlyNhcbCzsjmljAmt() {
        return zjlyNhcbCzsjmljAmt;
    }

    public void setZjlyNhcbCzsjmljAmt(BigDecimal zjlyNhcbCzsjmljAmt) {
        this.zjlyNhcbCzsjmljAmt = zjlyNhcbCzsjmljAmt;
    }

    public String getZjyyDqqx() {
        return zjyyDqqx;
    }

    public void setZjyyDqqx(String zjyyDqqx) {
        this.zjyyDqqx = zjyyDqqx;
    }

    public BigDecimal getZjyyYeBjmAmt() {
        return zjyyYeBjmAmt;
    }

    public void setZjyyYeBjmAmt(BigDecimal zjyyYeBjmAmt) {
        this.zjyyYeBjmAmt = zjyyYeBjmAmt;
    }

    public BigDecimal getZjyyYeSjmAmt() {
        return zjyyYeSjmAmt;
    }

    public void setZjyyYeSjmAmt(BigDecimal zjyyYeSjmAmt) {
        this.zjyyYeSjmAmt = zjyyYeSjmAmt;
    }

    public BigDecimal getZjyyNhsyCzbjmljAmt() {
        return zjyyNhsyCzbjmljAmt;
    }

    public void setZjyyNhsyCzbjmljAmt(BigDecimal zjyyNhsyCzbjmljAmt) {
        this.zjyyNhsyCzbjmljAmt = zjyyNhsyCzbjmljAmt;
    }

    public BigDecimal getZjyyNhsyCzsjmljAmt() {
        return zjyyNhsyCzsjmljAmt;
    }

    public void setZjyyNhsyCzsjmljAmt(BigDecimal zjyyNhsyCzsjmljAmt) {
        this.zjyyNhsyCzsjmljAmt = zjyyNhsyCzsjmljAmt;
    }

    public BigDecimal getZjyyFbzqBjmAmt() {
        return zjyyFbzqBjmAmt;
    }

    public void setZjyyFbzqBjmAmt(BigDecimal zjyyFbzqBjmAmt) {
        this.zjyyFbzqBjmAmt = zjyyFbzqBjmAmt;
    }

    public BigDecimal getZjyyFbzqSjmAmt() {
        return zjyyFbzqSjmAmt;
    }

    public void setZjyyFbzqSjmAmt(BigDecimal zjyyFbzqSjmAmt) {
        this.zjyyFbzqSjmAmt = zjyyFbzqSjmAmt;
    }

    public BigDecimal getZjyyFbzqnhCzbjmljAmt() {
        return zjyyFbzqnhCzbjmljAmt;
    }

    public void setZjyyFbzqnhCzbjmljAmt(BigDecimal zjyyFbzqnhCzbjmljAmt) {
        this.zjyyFbzqnhCzbjmljAmt = zjyyFbzqnhCzbjmljAmt;
    }

    public BigDecimal getZjyyFbzqnhCzsjmljAmt() {
        return zjyyFbzqnhCzsjmljAmt;
    }

    public void setZjyyFbzqnhCzsjmljAmt(BigDecimal zjyyFbzqnhCzsjmljAmt) {
        this.zjyyFbzqnhCzsjmljAmt = zjyyFbzqnhCzsjmljAmt;
    }

    public String getParentZjlyQx() {
        return parentZjlyQx;
    }

    public void setParentZjlyQx(String parentZjlyQx) {
        this.parentZjlyQx = parentZjlyQx;
    }
}