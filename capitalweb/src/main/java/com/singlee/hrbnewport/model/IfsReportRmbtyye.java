package com.singlee.hrbnewport.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

/**
 * 人民币同业余额
 */
@Table(name = "IFS_REPORT_RMBTYYE")
public class IfsReportRmbtyye implements Serializable {
    /**
     * 明细操作类型
     */
    @Column(name = "MXCZLX")
    private String mxczlx;

    /**
     * 境外参加行SWIFT BIC
     */
    @Column(name = "JWCAHSWIFTBIC")
    private String jwcahswiftbic;

    /**
     * 人民币同业往来账号账户
     */
    @Column(name = "RMBTYWLZHZH")
    private String rmbtywlzhzh;

    /**
     * 账户日终余额
     */
    @Column(name = "ZHRZYE")
    private BigDecimal zhrzye;

    /**
     * 系统更新时间
     */
    @Column(name = "XTGXSJ")
    private String xtgxsj;

    private static final long serialVersionUID = 1L;

    /**
     * 获取明细操作类型
     *
     * @return MXCZLX - 明细操作类型
     */
    public String getMxczlx() {
        return mxczlx;
    }

    /**
     * 设置明细操作类型
     *
     * @param mxczlx 明细操作类型
     */
    public void setMxczlx(String mxczlx) {
        this.mxczlx = mxczlx;
    }

    /**
     * 获取境外参加行SWIFT BIC
     *
     * @return JWCAHSWIFTBIC - 境外参加行SWIFT BIC
     */
    public String getJwcahswiftbic() {
        return jwcahswiftbic;
    }

    /**
     * 设置境外参加行SWIFT BIC
     *
     * @param jwcahswiftbic 境外参加行SWIFT BIC
     */
    public void setJwcahswiftbic(String jwcahswiftbic) {
        this.jwcahswiftbic = jwcahswiftbic;
    }

    /**
     * 获取人民币同业往来账号账户
     *
     * @return RMBTYWLZHZH - 人民币同业往来账号账户
     */
    public String getRmbtywlzhzh() {
        return rmbtywlzhzh;
    }

    /**
     * 设置人民币同业往来账号账户
     *
     * @param rmbtywlzhzh 人民币同业往来账号账户
     */
    public void setRmbtywlzhzh(String rmbtywlzhzh) {
        this.rmbtywlzhzh = rmbtywlzhzh;
    }

    /**
     * 获取账户日终余额
     *
     * @return ZHRZYE - 账户日终余额
     */
    public BigDecimal getZhrzye() {
        return zhrzye;
    }

    /**
     * 设置账户日终余额
     *
     * @param zhrzye 账户日终余额
     */
    public void setZhrzye(BigDecimal zhrzye) {
        this.zhrzye = zhrzye;
    }

    /**
     * 获取系统更新时间
     *
     * @return XTGXSJ - 系统更新时间
     */
    public String getXtgxsj() {
        return xtgxsj;
    }

    /**
     * 设置系统更新时间
     *
     * @param xtgxsj 系统更新时间
     */
    public void setXtgxsj(String xtgxsj) {
        this.xtgxsj = xtgxsj;
    }
}