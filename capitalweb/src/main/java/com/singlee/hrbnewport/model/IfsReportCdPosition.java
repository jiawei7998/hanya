package com.singlee.hrbnewport.model;

import java.math.BigDecimal;

/**
 * 同业存单
 * 2021/12/8
 * @Auther:zk
 */    
    
/**
    * 同业存单
    */
public class IfsReportCdPosition {
    /**
    * 序号
    */
    private String seq;

    /**
    * 存单代码
    */
    private String secid;

    /**
    * 分类
    */
    private String acctngtype;

    /**
    * 认购人
    */
    private String cust;

    /**
    * 存单面值
    */
    private BigDecimal faceAmt;

    /**
    * 应缴纳金额
    */
    private BigDecimal amt;

    /**
    * 利息调整
    */
    private BigDecimal interestAdj;

    /**
    * 利息调整余额
    */
    private BigDecimal unamortamt;

    /**
    * 起息日
    */
    private String vdate;

    /**
    * 到期日
    */
    private String mdate;

    /**
    * 发行天数
    */
    private String issDays;

    /**
    * 直线摊销法摊销
    */
    private BigDecimal amortizeStraight;

    /**
    * 已摊销
    */
    private BigDecimal amortize;

    /**
    * 剩余天数
    */
    private String days;

    /**
    * 剩余天数区间
    */
    private String daysRange;

    /**
    * 报送月份
    */
    private String mon;

    public String getSeq() {
        return seq;
    }

    public void setSeq(String seq) {
        this.seq = seq;
    }

    public String getSecid() {
        return secid;
    }

    public void setSecid(String secid) {
        this.secid = secid;
    }

    public String getAcctngtype() {
        return acctngtype;
    }

    public void setAcctngtype(String acctngtype) {
        this.acctngtype = acctngtype;
    }

    public String getCust() {
        return cust;
    }

    public void setCust(String cust) {
        this.cust = cust;
    }

    public BigDecimal getFaceAmt() {
        return faceAmt;
    }

    public void setFaceAmt(BigDecimal faceAmt) {
        this.faceAmt = faceAmt;
    }

    public BigDecimal getAmt() {
        return amt;
    }

    public void setAmt(BigDecimal amt) {
        this.amt = amt;
    }

    public BigDecimal getInterestAdj() {
        return interestAdj;
    }

    public void setInterestAdj(BigDecimal interestAdj) {
        this.interestAdj = interestAdj;
    }

    public BigDecimal getUnamortamt() {
        return unamortamt;
    }

    public void setUnamortamt(BigDecimal unamortamt) {
        this.unamortamt = unamortamt;
    }

    public String getVdate() {
        return vdate;
    }

    public void setVdate(String vdate) {
        this.vdate = vdate;
    }

    public String getMdate() {
        return mdate;
    }

    public void setMdate(String mdate) {
        this.mdate = mdate;
    }

    public String getIssDays() {
        return issDays;
    }

    public void setIssDays(String issDays) {
        this.issDays = issDays;
    }

    public BigDecimal getAmortizeStraight() {
        return amortizeStraight;
    }

    public void setAmortizeStraight(BigDecimal amortizeStraight) {
        this.amortizeStraight = amortizeStraight;
    }

    public BigDecimal getAmortize() {
        return amortize;
    }

    public void setAmortize(BigDecimal amortize) {
        this.amortize = amortize;
    }

    public String getDays() {
        return days;
    }

    public void setDays(String days) {
        this.days = days;
    }

    public String getDaysRange() {
        return daysRange;
    }

    public void setDaysRange(String daysRange) {
        this.daysRange = daysRange;
    }

    public String getMon() {
        return mon;
    }

    public void setMon(String mon) {
        this.mon = mon;
    }
}