package com.singlee.hrbnewport.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

/**
 * G22流动性比例监测表
 */
@Table(name = "IFS_REPORT_G2200")
public class IfsReportG2200 implements Serializable {
    /**
     * 项目
     */
    @Column(name = "PRODUCT")
    private String product;

    /**
     * 人名币
     */
    @Column(name = "CNY_AMT")
    private BigDecimal cnyAmt;

    /**
     * 外币折人名币
     */
    @Column(name = "CNY_CONV_AMT")
    private BigDecimal cnyConvAmt;

    /**
     * 本外币合计
     */
    @Column(name = "TOTAL_AMT")
    private BigDecimal totalAmt;

    @Column(name = "PARENT_PRODUCT")
    private String parentProduct;

    private static final long serialVersionUID = 1L;

    /**
     * 获取项目
     *
     * @return PRODUCT - 项目
     */
    public String getProduct() {
        return product;
    }

    /**
     * 设置项目
     *
     * @param product 项目
     */
    public void setProduct(String product) {
        this.product = product;
    }

    /**
     * @return CNY_AMT
     */
    public BigDecimal getCnyAmt() {
        return cnyAmt;
    }

    /**
     * @param cnyAmt
     */
    public void setCnyAmt(BigDecimal cnyAmt) {
        this.cnyAmt = cnyAmt;
    }

    /**
     * @return CNY_CONV_AMT
     */
    public BigDecimal getCnyConvAmt() {
        return cnyConvAmt;
    }

    /**
     * @param cnyConvAmt
     */
    public void setCnyConvAmt(BigDecimal cnyConvAmt) {
        this.cnyConvAmt = cnyConvAmt;
    }

    /**
     * @return TOTAL_AMT
     */
    public BigDecimal getTotalAmt() {
        return totalAmt;
    }

    /**
     * @param totalAmt
     */
    public void setTotalAmt(BigDecimal totalAmt) {
        this.totalAmt = totalAmt;
    }

    /**
     * @return PARENT_PRODUCT
     */
    public String getParentProduct() {
        return parentProduct;
    }

    /**
     * @param parentProduct
     */
    public void setParentProduct(String parentProduct) {
        this.parentProduct = parentProduct;
    }
}