package com.singlee.hrbnewport.model;

import java.io.Serializable;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 20:50
 * @description：${description}
 * @modified By：
 * @version:   
 */
/**
    * 2101跨境收入基本信息（变更）
    */
public class IfsReportKjjbsrxx implements Serializable {
    /**
    * 申报号码
    */
    private String sbhm;

    /**
    * 操作类型
    */
    private String czlx;

    /**
    * 变更/撤销原因
    */
    private String bgyy;

    /**
    * 银行机构代码
    */
    private String yhjgdm;

    /**
    * 收款人名称
    */
    private String skrmc;

    /**
    * 收款人属性
    */
    private String skrsx;

    /**
    * 收款人机构代码或身份证件号码
    */
    private String skrjgdm;

    /**
    * 付款人名称
    */
    private String skrmc2;

    /**
    * 付款行行号
    */
    private String skhhh;

    /**
    * 总金额
    */
    private String zje;

    /**
    * 收款日期
    */
    private String skrq;

    /**
    * 结算方式
    */
    private String jsfs;

    /**
    * 收款币种
    */
    private String skbz;

    /**
    * 银行业务编号
    */
    private String yhbh;

    private static final long serialVersionUID = 1L;

    public String getSbhm() {
        return sbhm;
    }

    public void setSbhm(String sbhm) {
        this.sbhm = sbhm;
    }

    public String getCzlx() {
        return czlx;
    }

    public void setCzlx(String czlx) {
        this.czlx = czlx;
    }

    public String getBgyy() {
        return bgyy;
    }

    public void setBgyy(String bgyy) {
        this.bgyy = bgyy;
    }

    public String getYhjgdm() {
        return yhjgdm;
    }

    public void setYhjgdm(String yhjgdm) {
        this.yhjgdm = yhjgdm;
    }

    public String getSkrmc() {
        return skrmc;
    }

    public void setSkrmc(String skrmc) {
        this.skrmc = skrmc;
    }

    public String getSkrsx() {
        return skrsx;
    }

    public void setSkrsx(String skrsx) {
        this.skrsx = skrsx;
    }

    public String getSkrjgdm() {
        return skrjgdm;
    }

    public void setSkrjgdm(String skrjgdm) {
        this.skrjgdm = skrjgdm;
    }

    public String getSkrmc2() {
        return skrmc2;
    }

    public void setSkrmc2(String skrmc2) {
        this.skrmc2 = skrmc2;
    }

    public String getSkhhh() {
        return skhhh;
    }

    public void setSkhhh(String skhhh) {
        this.skhhh = skhhh;
    }

    public String getZje() {
        return zje;
    }

    public void setZje(String zje) {
        this.zje = zje;
    }

    public String getSkrq() {
        return skrq;
    }

    public void setSkrq(String skrq) {
        this.skrq = skrq;
    }

    public String getJsfs() {
        return jsfs;
    }

    public void setJsfs(String jsfs) {
        this.jsfs = jsfs;
    }

    public String getSkbz() {
        return skbz;
    }

    public void setSkbz(String skbz) {
        this.skbz = skbz;
    }

    public String getYhbh() {
        return yhbh;
    }

    public void setYhbh(String yhbh) {
        this.yhbh = yhbh;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", sbhm=").append(sbhm);
        sb.append(", czlx=").append(czlx);
        sb.append(", bgyy=").append(bgyy);
        sb.append(", yhjgdm=").append(yhjgdm);
        sb.append(", skrmc=").append(skrmc);
        sb.append(", skrsx=").append(skrsx);
        sb.append(", skrjgdm=").append(skrjgdm);
        sb.append(", skrmc2=").append(skrmc2);
        sb.append(", skhhh=").append(skhhh);
        sb.append(", zje=").append(zje);
        sb.append(", skrq=").append(skrq);
        sb.append(", jsfs=").append(jsfs);
        sb.append(", skbz=").append(skbz);
        sb.append(", yhbh=").append(yhbh);
        sb.append("]");
        return sb.toString();
    }
}