package com.singlee.hrbnewport.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

/**
 * 金融市场业务负债期限结构表
 */
@Table(name = "IFS_REPORT_JRSCBYWGL_YWFZQX")
public class IfsReportJrscbywglYwfzqx implements Serializable {
    /**
     * 到期月
     */
    @Column(name = "DQY")
    private String dqy;

    /**
     * 同业存放(定期)_到期额
     */
    @Column(name = "TYCF_DQE")
    private BigDecimal tycfDqe;

    /**
     * 同业存放(定期)_余额
     */
    @Column(name = "TYCF_YE")
    private BigDecimal tycfYe;

    /**
     * 同业拆入_到期额
     */
    @Column(name = "TYCR_DQE")
    private BigDecimal tycrDqe;

    /**
     * 同业拆入_余额
     */
    @Column(name = "TYCR_YE")
    private BigDecimal tycrYe;

    /**
     * 发行债券_到期额
     */
    @Column(name = "FXZQ_DQE")
    private BigDecimal fxzqDqe;

    /**
     * 发行债券_余额
     */
    @Column(name = "FXZQ_YE")
    private BigDecimal fxzqYe;

    /**
     * 发行存单_到期额
     */
    @Column(name = "FXCD_DQE")
    private BigDecimal fxcdDqe;

    /**
     * 发行存单_余额
     */
    @Column(name = "FXCD_YE")
    private BigDecimal fxcdYe;

    /**
     * MLF_到期额
     */
    @Column(name = "MLF_DQE")
    private BigDecimal mlfDqe;

    /**
     * MLF_余额
     */
    @Column(name = "MLF_YE")
    private BigDecimal mlfYe;

    /**
     * SLF_到期额
     */
    @Column(name = "SLF_DQE")
    private BigDecimal slfDqe;

    /**
     * SLF_余额
     */
    @Column(name = "SLF_YE")
    private BigDecimal slfYe;

    /**
     * 中央国库定期存款_到期额
     */
    @Column(name = "ZYGKDQCK_DQE")
    private BigDecimal zygkdqckDqe;

    /**
     * 中央国库定期存款_余额
     */
    @Column(name = "ZYGKDQCK_YE")
    private BigDecimal zygkdqckYe;

    /**
     * 再贷款_到期额
     */
    @Column(name = "ZDK_DQE")
    private BigDecimal zdkDqe;

    /**
     * 再贷款_余额
     */
    @Column(name = "ZDK_YE")
    private BigDecimal zdkYe;

    private static final long serialVersionUID = 1L;

    /**
     * 获取到期月
     *
     * @return DQY - 到期月
     */
    public String getDqy() {
        return dqy;
    }

    /**
     * 设置到期月
     *
     * @param dqy 到期月
     */
    public void setDqy(String dqy) {
        this.dqy = dqy;
    }

    /**
     * 获取同业存放(定期)_到期额
     *
     * @return TYCF_DQE - 同业存放(定期)_到期额
     */
    public BigDecimal getTycfDqe() {
        return tycfDqe;
    }

    /**
     * 设置同业存放(定期)_到期额
     *
     * @param tycfDqe 同业存放(定期)_到期额
     */
    public void setTycfDqe(BigDecimal tycfDqe) {
        this.tycfDqe = tycfDqe;
    }

    /**
     * 获取同业存放(定期)_余额
     *
     * @return TYCF_YE - 同业存放(定期)_余额
     */
    public BigDecimal getTycfYe() {
        return tycfYe;
    }

    /**
     * 设置同业存放(定期)_余额
     *
     * @param tycfYe 同业存放(定期)_余额
     */
    public void setTycfYe(BigDecimal tycfYe) {
        this.tycfYe = tycfYe;
    }

    /**
     * 获取同业拆入_到期额
     *
     * @return TYCR_DQE - 同业拆入_到期额
     */
    public BigDecimal getTycrDqe() {
        return tycrDqe;
    }

    /**
     * 设置同业拆入_到期额
     *
     * @param tycrDqe 同业拆入_到期额
     */
    public void setTycrDqe(BigDecimal tycrDqe) {
        this.tycrDqe = tycrDqe;
    }

    /**
     * 获取同业拆入_余额
     *
     * @return TYCR_YE - 同业拆入_余额
     */
    public BigDecimal getTycrYe() {
        return tycrYe;
    }

    /**
     * 设置同业拆入_余额
     *
     * @param tycrYe 同业拆入_余额
     */
    public void setTycrYe(BigDecimal tycrYe) {
        this.tycrYe = tycrYe;
    }

    /**
     * 获取发行债券_到期额
     *
     * @return FXZQ_DQE - 发行债券_到期额
     */
    public BigDecimal getFxzqDqe() {
        return fxzqDqe;
    }

    /**
     * 设置发行债券_到期额
     *
     * @param fxzqDqe 发行债券_到期额
     */
    public void setFxzqDqe(BigDecimal fxzqDqe) {
        this.fxzqDqe = fxzqDqe;
    }

    /**
     * 获取发行债券_余额
     *
     * @return FXZQ_YE - 发行债券_余额
     */
    public BigDecimal getFxzqYe() {
        return fxzqYe;
    }

    /**
     * 设置发行债券_余额
     *
     * @param fxzqYe 发行债券_余额
     */
    public void setFxzqYe(BigDecimal fxzqYe) {
        this.fxzqYe = fxzqYe;
    }

    /**
     * 获取发行存单_到期额
     *
     * @return FXCD_DQE - 发行存单_到期额
     */
    public BigDecimal getFxcdDqe() {
        return fxcdDqe;
    }

    /**
     * 设置发行存单_到期额
     *
     * @param fxcdDqe 发行存单_到期额
     */
    public void setFxcdDqe(BigDecimal fxcdDqe) {
        this.fxcdDqe = fxcdDqe;
    }

    /**
     * 获取发行存单_余额
     *
     * @return FXCD_YE - 发行存单_余额
     */
    public BigDecimal getFxcdYe() {
        return fxcdYe;
    }

    /**
     * 设置发行存单_余额
     *
     * @param fxcdYe 发行存单_余额
     */
    public void setFxcdYe(BigDecimal fxcdYe) {
        this.fxcdYe = fxcdYe;
    }

    /**
     * 获取MLF_到期额
     *
     * @return MLF_DQE - MLF_到期额
     */
    public BigDecimal getMlfDqe() {
        return mlfDqe;
    }

    /**
     * 设置MLF_到期额
     *
     * @param mlfDqe MLF_到期额
     */
    public void setMlfDqe(BigDecimal mlfDqe) {
        this.mlfDqe = mlfDqe;
    }

    /**
     * 获取MLF_余额
     *
     * @return MLF_YE - MLF_余额
     */
    public BigDecimal getMlfYe() {
        return mlfYe;
    }

    /**
     * 设置MLF_余额
     *
     * @param mlfYe MLF_余额
     */
    public void setMlfYe(BigDecimal mlfYe) {
        this.mlfYe = mlfYe;
    }

    /**
     * 获取SLF_到期额
     *
     * @return SLF_DQE - SLF_到期额
     */
    public BigDecimal getSlfDqe() {
        return slfDqe;
    }

    /**
     * 设置SLF_到期额
     *
     * @param slfDqe SLF_到期额
     */
    public void setSlfDqe(BigDecimal slfDqe) {
        this.slfDqe = slfDqe;
    }

    /**
     * 获取SLF_余额
     *
     * @return SLF_YE - SLF_余额
     */
    public BigDecimal getSlfYe() {
        return slfYe;
    }

    /**
     * 设置SLF_余额
     *
     * @param slfYe SLF_余额
     */
    public void setSlfYe(BigDecimal slfYe) {
        this.slfYe = slfYe;
    }

    /**
     * 获取中央国库定期存款_到期额
     *
     * @return ZYGKDQCK_DQE - 中央国库定期存款_到期额
     */
    public BigDecimal getZygkdqckDqe() {
        return zygkdqckDqe;
    }

    /**
     * 设置中央国库定期存款_到期额
     *
     * @param zygkdqckDqe 中央国库定期存款_到期额
     */
    public void setZygkdqckDqe(BigDecimal zygkdqckDqe) {
        this.zygkdqckDqe = zygkdqckDqe;
    }

    /**
     * 获取中央国库定期存款_余额
     *
     * @return ZYGKDQCK_YE - 中央国库定期存款_余额
     */
    public BigDecimal getZygkdqckYe() {
        return zygkdqckYe;
    }

    /**
     * 设置中央国库定期存款_余额
     *
     * @param zygkdqckYe 中央国库定期存款_余额
     */
    public void setZygkdqckYe(BigDecimal zygkdqckYe) {
        this.zygkdqckYe = zygkdqckYe;
    }

    /**
     * 获取再贷款_到期额
     *
     * @return ZDK_DQE - 再贷款_到期额
     */
    public BigDecimal getZdkDqe() {
        return zdkDqe;
    }

    /**
     * 设置再贷款_到期额
     *
     * @param zdkDqe 再贷款_到期额
     */
    public void setZdkDqe(BigDecimal zdkDqe) {
        this.zdkDqe = zdkDqe;
    }

    /**
     * 获取再贷款_余额
     *
     * @return ZDK_YE - 再贷款_余额
     */
    public BigDecimal getZdkYe() {
        return zdkYe;
    }

    /**
     * 设置再贷款_余额
     *
     * @param zdkYe 再贷款_余额
     */
    public void setZdkYe(BigDecimal zdkYe) {
        this.zdkYe = zdkYe;
    }
}