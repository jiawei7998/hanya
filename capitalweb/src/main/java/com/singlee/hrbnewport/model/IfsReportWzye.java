package com.singlee.hrbnewport.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

/**
 * 外债余额
 */
@Table(name = "IFS_REPORT_WZYE")
public class IfsReportWzye implements Serializable {

    @Column(name = "WZLX")
    private String wzlx;
    /**
     * 外债编号
     */
    @Column(name = "WZBH")
    private String wzbh;

    /**
     * 银行账号
     */
    @Column(name = "YHZH")
    private String yhzh;

    /**
     * 变动编号
     */
    @Column(name = "BDBM")
    private String bdbm;

    /**
     * 变动日期
     */
    @Column(name = "BDRQ")
    private String bdrq;

    /**
     * 外债余额
     */
    @Column(name = "WZYE")
    private BigDecimal wzye;

    /**
     * 备注
     */
    @Column(name = "BZ")
    private String bz;

    private static final long serialVersionUID = 1L;

    public String getWzlx() {
        return wzlx;
    }

    public void setWzlx(String wzlx) {
        this.wzlx = wzlx;
    }

    /**
     * 获取外债编号
     *
     * @return WZBH - 外债编号
     */
    public String getWzbh() {
        return wzbh;
    }

    /**
     * 设置外债编号
     *
     * @param wzbh 外债编号
     */
    public void setWzbh(String wzbh) {
        this.wzbh = wzbh;
    }

    /**
     * 获取银行账号
     *
     * @return YHZH - 银行账号
     */
    public String getYhzh() {
        return yhzh;
    }

    /**
     * 设置银行账号
     *
     * @param yhzh 银行账号
     */
    public void setYhzh(String yhzh) {
        this.yhzh = yhzh;
    }

    /**
     * 获取变动编号
     *
     * @return BDBM - 变动编号
     */
    public String getBdbm() {
        return bdbm;
    }

    /**
     * 设置变动编号
     *
     * @param bdbm 变动编号
     */
    public void setBdbm(String bdbm) {
        this.bdbm = bdbm;
    }

    /**
     * 获取变动日期
     *
     * @return BDRQ - 变动日期
     */
    public String getBdrq() {
        return bdrq;
    }

    /**
     * 设置变动日期
     *
     * @param bdrq 变动日期
     */
    public void setBdrq(String bdrq) {
        this.bdrq = bdrq;
    }

    /**
     * 获取外债余额
     *
     * @return WZYE - 外债余额
     */
    public BigDecimal getWzye() {
        return wzye;
    }

    /**
     * 设置外债余额
     *
     * @param wzye 外债余额
     */
    public void setWzye(BigDecimal wzye) {
        this.wzye = wzye;
    }

    /**
     * 获取备注
     *
     * @return BZ - 备注
     */
    public String getBz() {
        return bz;
    }

    /**
     * 设置备注
     *
     * @param bz 备注
     */
    public void setBz(String bz) {
        this.bz = bz;
    }
}