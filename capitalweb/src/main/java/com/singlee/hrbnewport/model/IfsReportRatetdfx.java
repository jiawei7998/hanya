package com.singlee.hrbnewport.model;

/**
    * 利率特定风险计算表
    */
public class IfsReportRatetdfx {
    /**
    * 类别
    */
    private String sort0;

    /**
     * 类别
     */
    private String sort;

    /**
    * 债券ID
    */
    private String bondid;

    /**
    * 债券描述
    */
    private String zqms;

    /**
    * 债券名称
    */
    private String zqmc;

    /**
    * 发行人
    */
    private String fxr;

    /**
    * 主体评级
    */
    private String ztpj;

    /**
    * 到期日或（最近一次付息日）
    */
    private String dqr;

    /**
    * 剩余期限（年）
    */
    private String syqx;

    /**
    * 风险权重（合格证券）
    */
    private String fxqzhgzq;

    /**
    * 风险权重（其他证券）
    */
    private String fxqzqtzq;

    /**
    * 风险权重（混合）
    */
    private String fxqzhh;

    /**
    * 债券市值（万元）
    */
    private String zqsz;

    /**
    * 资本占用（合格证券）（万元）
    */
    private String zbzyhgzq;

    /**
    * 资本占用（其他证券）（万元）
    */
    private String zbzyqtzq;

    /**
    * 资本占用（混合法）（万元）
    */
    private String zbzyhhf;

    public String getSort0() {
        return sort0;
    }

    public void setSort0(String sort0) {
        this.sort0 = sort0;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getBondid() {
        return bondid;
    }

    public void setBondid(String bondid) {
        this.bondid = bondid;
    }

    public String getZqms() {
        return zqms;
    }

    public void setZqms(String zqms) {
        this.zqms = zqms;
    }

    public String getZqmc() {
        return zqmc;
    }

    public void setZqmc(String zqmc) {
        this.zqmc = zqmc;
    }

    public String getFxr() {
        return fxr;
    }

    public void setFxr(String fxr) {
        this.fxr = fxr;
    }

    public String getZtpj() {
        return ztpj;
    }

    public void setZtpj(String ztpj) {
        this.ztpj = ztpj;
    }

    public String getDqr() {
        return dqr;
    }

    public void setDqr(String dqr) {
        this.dqr = dqr;
    }

    public String getSyqx() {
        return syqx;
    }

    public void setSyqx(String syqx) {
        this.syqx = syqx;
    }

    public String getFxqzhgzq() {
        return fxqzhgzq;
    }

    public void setFxqzhgzq(String fxqzhgzq) {
        this.fxqzhgzq = fxqzhgzq;
    }

    public String getFxqzqtzq() {
        return fxqzqtzq;
    }

    public void setFxqzqtzq(String fxqzqtzq) {
        this.fxqzqtzq = fxqzqtzq;
    }

    public String getFxqzhh() {
        return fxqzhh;
    }

    public void setFxqzhh(String fxqzhh) {
        this.fxqzhh = fxqzhh;
    }

    public String getZqsz() {
        return zqsz;
    }

    public void setZqsz(String zqsz) {
        this.zqsz = zqsz;
    }

    public String getZbzyhgzq() {
        return zbzyhgzq;
    }

    public void setZbzyhgzq(String zbzyhgzq) {
        this.zbzyhgzq = zbzyhgzq;
    }

    public String getZbzyqtzq() {
        return zbzyqtzq;
    }

    public void setZbzyqtzq(String zbzyqtzq) {
        this.zbzyqtzq = zbzyqtzq;
    }

    public String getZbzyhhf() {
        return zbzyhhf;
    }

    public void setZbzyhhf(String zbzyhhf) {
        this.zbzyhhf = zbzyhhf;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", sort=").append(sort);
        sb.append(", bondid=").append(bondid);
        sb.append(", zqms=").append(zqms);
        sb.append(", zqmc=").append(zqmc);
        sb.append(", fxr=").append(fxr);
        sb.append(", ztpj=").append(ztpj);
        sb.append(", dqr=").append(dqr);
        sb.append(", syqx=").append(syqx);
        sb.append(", fxqzhgzq=").append(fxqzhgzq);
        sb.append(", fxqzqtzq=").append(fxqzqtzq);
        sb.append(", fxqzhh=").append(fxqzhh);
        sb.append(", zqsz=").append(zqsz);
        sb.append(", zbzyhgzq=").append(zbzyhgzq);
        sb.append(", zbzyqtzq=").append(zbzyqtzq);
        sb.append(", zbzyhhf=").append(zbzyhhf);
        sb.append("]");
        return sb.toString();
    }
}