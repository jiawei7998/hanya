package com.singlee.hrbnewport.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

/**
 * 存款含银行同业和联行存放负债 汇总
 */
@Table(name = "IFS_REPORT_CKHYHTYHLHCFFZ_HZ")
public class IfsReportCkhyhtyhlhcffzHz implements Serializable {
    /**
     * 国别
     */
    @Column(name = "GB")
    private String gb;

    /**
     * 币种
     */
    @Column(name = "BZ")
    private String bz;

    /**
     * 报告期
     */
    @Column(name = "BGQ")
    private String bgq;

    /**
     * 上月末应付利息余额
     */
    @Column(name = "SYMYFLXYE")
    private BigDecimal symyflxye;

    /**
     * 本月末本金余额
     */
    @Column(name = "BYMBJYE")
    private BigDecimal bymbjye;

    /**
     * 本月末应付利息余额
     */
    @Column(name = "BYMYFLXYE")
    private BigDecimal bymyflxye;

    /**
     * 本月净发生额
     */
    @Column(name = "BYJFSE")
    private BigDecimal byjfse;

    /**
     * 本月利息支出
     */
    @Column(name = "BYLXZC")
    private BigDecimal bylxzc;

    private static final long serialVersionUID = 1L;

    /**
     * 获取国别
     *
     * @return GB - 国别
     */
    public String getGb() {
        return gb;
    }

    /**
     * 设置国别
     *
     * @param gb 国别
     */
    public void setGb(String gb) {
        this.gb = gb;
    }

    /**
     * 获取币种
     *
     * @return BZ - 币种
     */
    public String getBz() {
        return bz;
    }

    /**
     * 设置币种
     *
     * @param bz 币种
     */
    public void setBz(String bz) {
        this.bz = bz;
    }

    /**
     * 获取报告期
     *
     * @return BGQ - 报告期
     */
    public String getBgq() {
        return bgq;
    }

    /**
     * 设置报告期
     *
     * @param bgq 报告期
     */
    public void setBgq(String bgq) {
        this.bgq = bgq;
    }

    /**
     * 获取上月末应付利息余额
     *
     * @return SYMYFLXYE - 上月末应付利息余额
     */
    public BigDecimal getSymyflxye() {
        return symyflxye;
    }

    /**
     * 设置上月末应付利息余额
     *
     * @param symyflxye 上月末应付利息余额
     */
    public void setSymyflxye(BigDecimal symyflxye) {
        this.symyflxye = symyflxye;
    }

    /**
     * 获取本月末本金余额
     *
     * @return BYMBJYE - 本月末本金余额
     */
    public BigDecimal getBymbjye() {
        return bymbjye;
    }

    /**
     * 设置本月末本金余额
     *
     * @param bymbjye 本月末本金余额
     */
    public void setBymbjye(BigDecimal bymbjye) {
        this.bymbjye = bymbjye;
    }

    /**
     * 获取本月末应付利息余额
     *
     * @return BYMYFLXYE - 本月末应付利息余额
     */
    public BigDecimal getBymyflxye() {
        return bymyflxye;
    }

    /**
     * 设置本月末应付利息余额
     *
     * @param bymyflxye 本月末应付利息余额
     */
    public void setBymyflxye(BigDecimal bymyflxye) {
        this.bymyflxye = bymyflxye;
    }

    /**
     * 获取本月净发生额
     *
     * @return BYJFSE - 本月净发生额
     */
    public BigDecimal getByjfse() {
        return byjfse;
    }

    /**
     * 设置本月净发生额
     *
     * @param byjfse 本月净发生额
     */
    public void setByjfse(BigDecimal byjfse) {
        this.byjfse = byjfse;
    }

    /**
     * 获取本月利息支出
     *
     * @return BYLXZC - 本月利息支出
     */
    public BigDecimal getBylxzc() {
        return bylxzc;
    }

    /**
     * 设置本月利息支出
     *
     * @param bylxzc 本月利息支出
     */
    public void setBylxzc(BigDecimal bylxzc) {
        this.bylxzc = bylxzc;
    }
}