package com.singlee.hrbnewport.model;

/**
    * 合并持仓分析表-给郑诗宇
    */
public class IfsReportHbzsy {
    /**
    * 账户类型
    */
    private String invtype;

    /**
    * 债券类型
    */
    private String sd;

    /**
    * 债券发行人
    */
    private String issuer;

    /**
    * 债券代码
    */
    private String secid;

    /**
    * 债券名称
    */
    private String sname;

    /**
    * 起息日
    */
    private String vdate;

    /**
    * 期限
    */
        private String tenor;

    /**
    * 到期日
    */
    private String mdate;

    /**
    * 债券面值
    */
    private String prinamt;

    /**
    * 票面利率
    */
    private String couprate;

    public String getInvtype() {
        return invtype;
    }

    public void setInvtype(String invtype) {
        this.invtype = invtype == null ? null : invtype.trim();
    }

    public String getSd() {
        return sd;
    }

    public void setSd(String sd) {
        this.sd = sd == null ? null : sd.trim();
    }

    public String getIssuer() {
        return issuer;
    }

    public void setIssuer(String issuer) {
        this.issuer = issuer == null ? null : issuer.trim();
    }

    public String getSecid() {
        return secid;
    }

    public void setSecid(String secid) {
        this.secid = secid == null ? null : secid.trim();
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname == null ? null : sname.trim();
    }

    public String getVdate() {
        return vdate;
    }

    public void setVdate(String vdate) {
        this.vdate = vdate == null ? null : vdate.trim();
    }

    public String getTenor() {
        return tenor;
    }

    public void setTenor(String tenor) {
        this.tenor = tenor == null ? null : tenor.trim();
    }

    public String getMdate() {
        return mdate;
    }

    public void setMdate(String mdate) {
        this.mdate = mdate == null ? null : mdate.trim();
    }

    public String getPrinamt() {
        return prinamt;
    }

    public void setPrinamt(String prinamt) {
        this.prinamt = prinamt == null ? null : prinamt.trim();
    }

    public String getCouprate() {
        return couprate;
    }

    public void setCouprate(String couprate) {
        this.couprate = couprate == null ? null : couprate.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", invtype=").append(invtype);
        sb.append(", sd=").append(sd);
        sb.append(", issuer=").append(issuer);
        sb.append(", secid=").append(secid);
        sb.append(", sname=").append(sname);
        sb.append(", vdate=").append(vdate);
        sb.append(", tenor=").append(tenor);
        sb.append(", mdate=").append(mdate);
        sb.append(", prinamt=").append(prinamt);
        sb.append(", couprate=").append(couprate);
        sb.append("]");
        return sb.toString();
    }
}