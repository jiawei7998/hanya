package com.singlee.hrbnewport.model;

import java.math.BigDecimal;

/**
    * opics-新版客户风险需求（交易账户）
    */
public class TbCustomerRisks {
    /**
    * 序号
    */
    private String sort;

    /**
    * 客户名称
    */
    private String clientName;

    /**
    * 客户代码
    */
    private String clientCode;

    /**
    * 国家代码
    */
    private String countryCode;

    /**
    * 非现场监管统计机构编码
    */
    private String institutionCode;

    /**
    * 组织机构代码
    */
    private String organizationInstitutionCode;

    /**
    * 客户类别
    */
    private String clientType;

    /**
    * 内部评级
    */
    private String internalRating;

    /**
    * 外部评级
    */
    private String withoutRating;

    /**
    * 拆放同业
    */
    private BigDecimal loanTrade;

    /**
    * 存放同业
    */
    private BigDecimal interbankLoan;

    /**
    * 债券回购
    */
    private BigDecimal bondRepurchase;

    /**
    * 股票质押贷款
    */
    private BigDecimal stockPledgeLoan;

    /**
    * 买入返售资产
    */
    private BigDecimal buyingResold;

    /**
    * 买断式转贴现
    */
    private BigDecimal buyoutDiscount;

    /**
    * 持有债券
    */
    private BigDecimal hoidingBond;

    /**
    * 股权投资
    */
    private BigDecimal equityInvestment;

    /**
    * 同业代付
    */
    private BigDecimal tradePayment;

    /**
    * 其他表内业务
    */
    private BigDecimal otherServicesTable;

    /**
    * 卖出回购资产
    */
    private BigDecimal sellingRepurchasedAssets;

    /**
    * 不可撤销的承诺及或有负债
    */
    private BigDecimal commitmentDebt;

    /**
    * 其他表外业务
    */
    private BigDecimal otherServicesWithout;

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort == null ? null : sort.trim();
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName == null ? null : clientName.trim();
    }

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode == null ? null : clientCode.trim();
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode == null ? null : countryCode.trim();
    }

    public String getInstitutionCode() {
        return institutionCode;
    }

    public void setInstitutionCode(String institutionCode) {
        this.institutionCode = institutionCode == null ? null : institutionCode.trim();
    }

    public String getOrganizationInstitutionCode() {
        return organizationInstitutionCode;
    }

    public void setOrganizationInstitutionCode(String organizationInstitutionCode) {
        this.organizationInstitutionCode = organizationInstitutionCode == null ? null : organizationInstitutionCode.trim();
    }

    public String getClientType() {
        return clientType;
    }

    public void setClientType(String clientType) {
        this.clientType = clientType == null ? null : clientType.trim();
    }

    public String getInternalRating() {
        return internalRating;
    }

    public void setInternalRating(String internalRating) {
        this.internalRating = internalRating == null ? null : internalRating.trim();
    }

    public String getWithoutRating() {
        return withoutRating;
    }

    public void setWithoutRating(String withoutRating) {
        this.withoutRating = withoutRating == null ? null : withoutRating.trim();
    }

    public BigDecimal getLoanTrade() {
        return loanTrade;
    }

    public void setLoanTrade(BigDecimal loanTrade) {
        this.loanTrade = loanTrade;
    }

    public BigDecimal getInterbankLoan() {
        return interbankLoan;
    }

    public void setInterbankLoan(BigDecimal interbankLoan) {
        this.interbankLoan = interbankLoan;
    }

    public BigDecimal getBondRepurchase() {
        return bondRepurchase;
    }

    public void setBondRepurchase(BigDecimal bondRepurchase) {
        this.bondRepurchase = bondRepurchase;
    }

    public BigDecimal getStockPledgeLoan() {
        return stockPledgeLoan;
    }

    public void setStockPledgeLoan(BigDecimal stockPledgeLoan) {
        this.stockPledgeLoan = stockPledgeLoan;
    }

    public BigDecimal getBuyingResold() {
        return buyingResold;
    }

    public void setBuyingResold(BigDecimal buyingResold) {
        this.buyingResold = buyingResold;
    }

    public BigDecimal getBuyoutDiscount() {
        return buyoutDiscount;
    }

    public void setBuyoutDiscount(BigDecimal buyoutDiscount) {
        this.buyoutDiscount = buyoutDiscount;
    }

    public BigDecimal getHoidingBond() {
        return hoidingBond;
    }

    public void setHoidingBond(BigDecimal hoidingBond) {
        this.hoidingBond = hoidingBond;
    }

    public BigDecimal getEquityInvestment() {
        return equityInvestment;
    }

    public void setEquityInvestment(BigDecimal equityInvestment) {
        this.equityInvestment = equityInvestment;
    }

    public BigDecimal getTradePayment() {
        return tradePayment;
    }

    public void setTradePayment(BigDecimal tradePayment) {
        this.tradePayment = tradePayment;
    }

    public BigDecimal getOtherServicesTable() {
        return otherServicesTable;
    }

    public void setOtherServicesTable(BigDecimal otherServicesTable) {
        this.otherServicesTable = otherServicesTable;
    }

    public BigDecimal getSellingRepurchasedAssets() {
        return sellingRepurchasedAssets;
    }

    public void setSellingRepurchasedAssets(BigDecimal sellingRepurchasedAssets) {
        this.sellingRepurchasedAssets = sellingRepurchasedAssets;
    }

    public BigDecimal getCommitmentDebt() {
        return commitmentDebt;
    }

    public void setCommitmentDebt(BigDecimal commitmentDebt) {
        this.commitmentDebt = commitmentDebt;
    }

    public BigDecimal getOtherServicesWithout() {
        return otherServicesWithout;
    }

    public void setOtherServicesWithout(BigDecimal otherServicesWithout) {
        this.otherServicesWithout = otherServicesWithout;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", sort=").append(sort);
        sb.append(", clientName=").append(clientName);
        sb.append(", clientCode=").append(clientCode);
        sb.append(", countryCode=").append(countryCode);
        sb.append(", institutionCode=").append(institutionCode);
        sb.append(", organizationInstitutionCode=").append(organizationInstitutionCode);
        sb.append(", clientType=").append(clientType);
        sb.append(", internalRating=").append(internalRating);
        sb.append(", withoutRating=").append(withoutRating);
        sb.append(", loanTrade=").append(loanTrade);
        sb.append(", interbankLoan=").append(interbankLoan);
        sb.append(", bondRepurchase=").append(bondRepurchase);
        sb.append(", stockPledgeLoan=").append(stockPledgeLoan);
        sb.append(", buyingResold=").append(buyingResold);
        sb.append(", buyoutDiscount=").append(buyoutDiscount);
        sb.append(", hoidingBond=").append(hoidingBond);
        sb.append(", equityInvestment=").append(equityInvestment);
        sb.append(", tradePayment=").append(tradePayment);
        sb.append(", otherServicesTable=").append(otherServicesTable);
        sb.append(", sellingRepurchasedAssets=").append(sellingRepurchasedAssets);
        sb.append(", commitmentDebt=").append(commitmentDebt);
        sb.append(", otherServicesWithout=").append(otherServicesWithout);
        sb.append("]");
        return sb.toString();
    }
}