package com.singlee.hrbnewport.model;

import java.io.Serializable;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 16:22
 * @description：${description}
 * @modified By：
 * @version:   
 */
public class IfsReportTytzZfWct implements Serializable {
    /**
    * 序号
    */
    private String ser;

    /**
    * 分行名称（含总行）
    */
    private String fhnc;

    /**
    * 支行名称（含总行部门）
    */
    private String zhmc;

    /**
    * 归属部门
    */
    private String gsbm;

    /**
    * 交易对手总部全称
    */
    private String jydszbqc;

    /**
    * 交易对手落地机构
    */
    private String jydsldjg;

    /**
    * 交易对手地域
    */
    private String jydsdy;

    /**
    * 活期/定期
    */
    private String qxlx;
    /**
     * 外部账号
     */
    private String wbzh;

    /**
     * 内部账号
     */
    private String nbzh;

    /**
    * 业务开始日期
    */
    private String ywkssj;

    /**
    * 业务结束日期
    */
    private String ywjssj;

    /**
    * 利率 （%）
    */
    private String rate;

    /**
    * 币种
    */
    private String ccy;

    /**
    * 原币金额（货币基本单位） 
    */
    private String amt;

    /**
    * 汇率
    */
    private String hl;

    /**
    * 折人民币金额（元）
    */
    private String cnyamt;

    /**
    * 记账科目
    */
    private String jzkm;

    /**
    * 账户性质
    */
    private String zhxz;

    /**
    * 账户用途
    */
    private String zhyt;

    /**
    * 账户状态
    */
    private String zhzt;

    /**
    * 是否开网银
    */
    private String sfkwy;

    /**
    * 联系人/电话
    */
    private String phone;

    /**
    * 备注
    */
    private String beizhu;

    /**
    * 月末汇率
    */
    private String ymhl;

    /**
    * 原币余额
    */
    private String ybye;

    /**
    * 折人民币余额
    */
    private String rmbye;

    private static final long serialVersionUID = 1L;

    public String getSer() {
        return ser;
    }

    public void setSer(String ser) {
        this.ser = ser;
    }

    public String getFhnc() {
        return fhnc;
    }

    public void setFhnc(String fhnc) {
        this.fhnc = fhnc;
    }

    public String getZhmc() {
        return zhmc;
    }

    public void setZhmc(String zhmc) {
        this.zhmc = zhmc;
    }

    public String getGsbm() {
        return gsbm;
    }

    public void setGsbm(String gsbm) {
        this.gsbm = gsbm;
    }

    public String getJydszbqc() {
        return jydszbqc;
    }

    public void setJydszbqc(String jydszbqc) {
        this.jydszbqc = jydszbqc;
    }

    public String getJydsldjg() {
        return jydsldjg;
    }

    public void setJydsldjg(String jydsldjg) {
        this.jydsldjg = jydsldjg;
    }

    public String getJydsdy() {
        return jydsdy;
    }

    public void setJydsdy(String jydsdy) {
        this.jydsdy = jydsdy;
    }

    public String getQxlx() {
        return qxlx;
    }

    public void setQxlx(String qxlx) {
        this.qxlx = qxlx;
    }

    public String getWbzh() {
        return wbzh;
    }

    public void setWbzh(String wbzh) {
        this.wbzh = wbzh;
    }

    public String getNbzh() {
        return nbzh;
    }

    public void setNbzh(String nbzh) {
        this.nbzh = nbzh;
    }

    public String getYwkssj() {
        return ywkssj;
    }

    public void setYwkssj(String ywkssj) {
        this.ywkssj = ywkssj;
    }

    public String getYwjssj() {
        return ywjssj;
    }

    public void setYwjssj(String ywjssj) {
        this.ywjssj = ywjssj;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getCcy() {
        return ccy;
    }

    public void setCcy(String ccy) {
        this.ccy = ccy;
    }

    public String getAmt() {
        return amt;
    }

    public void setAmt(String amt) {
        this.amt = amt;
    }

    public String getHl() {
        return hl;
    }

    public void setHl(String hl) {
        this.hl = hl;
    }

    public String getCnyamt() {
        return cnyamt;
    }

    public void setCnyamt(String cnyamt) {
        this.cnyamt = cnyamt;
    }

    public String getJzkm() {
        return jzkm;
    }

    public void setJzkm(String jzkm) {
        this.jzkm = jzkm;
    }

    public String getZhxz() {
        return zhxz;
    }

    public void setZhxz(String zhxz) {
        this.zhxz = zhxz;
    }

    public String getZhyt() {
        return zhyt;
    }

    public void setZhyt(String zhyt) {
        this.zhyt = zhyt;
    }

    public String getZhzt() {
        return zhzt;
    }

    public void setZhzt(String zhzt) {
        this.zhzt = zhzt;
    }

    public String getSfkwy() {
        return sfkwy;
    }

    public void setSfkwy(String sfkwy) {
        this.sfkwy = sfkwy;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getBeizhu() {
        return beizhu;
    }

    public void setBeizhu(String beizhu) {
        this.beizhu = beizhu;
    }

    public String getYmhl() {
        return ymhl;
    }

    public void setYmhl(String ymhl) {
        this.ymhl = ymhl;
    }

    public String getYbye() {
        return ybye;
    }

    public void setYbye(String ybye) {
        this.ybye = ybye;
    }

    public String getRmbye() {
        return rmbye;
    }

    public void setRmbye(String rmbye) {
        this.rmbye = rmbye;
    }

    @Override
    public String toString() {
        return "IfsReportTytzZfWct{" +
                "ser='" + ser + '\'' +
                ", fhnc='" + fhnc + '\'' +
                ", zhmc='" + zhmc + '\'' +
                ", gsbm='" + gsbm + '\'' +
                ", jydszbqc='" + jydszbqc + '\'' +
                ", jydsldjg='" + jydsldjg + '\'' +
                ", jydsdy='" + jydsdy + '\'' +
                ", qxlx='" + qxlx + '\'' +
                ", wbzh='" + wbzh + '\'' +
                ", nbzh='" + nbzh + '\'' +
                ", ywkssj='" + ywkssj + '\'' +
                ", ywjssj='" + ywjssj + '\'' +
                ", rate='" + rate + '\'' +
                ", ccy='" + ccy + '\'' +
                ", amt='" + amt + '\'' +
                ", hl='" + hl + '\'' +
                ", cnyamt='" + cnyamt + '\'' +
                ", jzkm='" + jzkm + '\'' +
                ", zhxz='" + zhxz + '\'' +
                ", zhyt='" + zhyt + '\'' +
                ", zhzt='" + zhzt + '\'' +
                ", sfkwy='" + sfkwy + '\'' +
                ", phone='" + phone + '\'' +
                ", beizhu='" + beizhu + '\'' +
                ", ymhl='" + ymhl + '\'' +
                ", ybye='" + ybye + '\'' +
                ", rmbye='" + rmbye + '\'' +
                '}';
    }
}