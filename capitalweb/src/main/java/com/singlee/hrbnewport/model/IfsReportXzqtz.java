package com.singlee.hrbnewport.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author ：meigy
 * @date ：Created in 2021/11/6 14:29
 * @description：${description}
 * @modified By：
 * @version:
 */

/**
 * 债券投资待偿期限结构表
 */
@Entity
@Table(name = "IFS_REPORT_XZQTZ")
public class IfsReportXzqtz implements Serializable {
    /**
     * 券种
     */
    private String qz;

    /**
     * 风险权重
     */
    private String fxqz;

    /**
     * 交易账户_余额
     */
    private BigDecimal jyzhYe;

    /**
     * 交易账户_应收利息
     */
    private BigDecimal jyzhYslx;

    /**
     * 银行账户_余额
     */
    private BigDecimal yhzhYe;

    /**
     * 银行账户_应收利息
     */
    private BigDecimal yhzhYslx;

    @Transient
    private String product;
    @Transient
    private String qzo;
    @Transient
    private String qzt;

    private static final long serialVersionUID = 1L;

    public String getQz() {
        return qz;
    }

    public void setQz(String qz) {
        this.qz = qz;
    }

    public String getFxqz() {
        return fxqz;
    }

    public void setFxqz(String fxqz) {
        this.fxqz = fxqz;
    }

    public BigDecimal getJyzhYe() {
        return jyzhYe;
    }

    public void setJyzhYe(BigDecimal jyzhYe) {
        this.jyzhYe = jyzhYe;
    }

    public BigDecimal getJyzhYslx() {
        return jyzhYslx;
    }

    public void setJyzhYslx(BigDecimal jyzhYslx) {
        this.jyzhYslx = jyzhYslx;
    }

    public BigDecimal getYhzhYe() {
        return yhzhYe;
    }

    public void setYhzhYe(BigDecimal yhzhYe) {
        this.yhzhYe = yhzhYe;
    }

    public BigDecimal getYhzhYslx() {
        return yhzhYslx;
    }

    public void setYhzhYslx(BigDecimal yhzhYslx) {
        this.yhzhYslx = yhzhYslx;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getQzo() {
        return qzo;
    }

    public void setQzo(String qzo) {
        this.qzo = qzo;
    }

    public String getQzt() {
        return qzt;
    }

    public void setQzt(String qzt) {
        this.qzt = qzt;
    }
}