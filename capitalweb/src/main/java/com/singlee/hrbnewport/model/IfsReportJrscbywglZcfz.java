package com.singlee.hrbnewport.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

/**
 * @author ：meigy
 * @date ：Created in 2021/11/17 14:26
 * @description：${description}
 * @modified By：
 * @version:
 */

/**
 * 资产负债数据表
 */
@Table(name = "IFS_REPORT_JRSCBYWGL_ZCFZ")
public class IfsReportJrscbywglZcfz implements Serializable {
    /**
     * 资产
     */
    @Column(name = "ZC")
    private String zc;

    /**
     * 资产_余额
     */
    @Column(name = "ZC_YE")
    private BigDecimal zcYe;

    /**
     * 资产_收益率
     */
    @Column(name = "ZC_SYL")
    private BigDecimal zcSyl;

    /**
     * 资产_利息收入
     */
    @Column(name = "ZC_LXSR")
    private BigDecimal zcLxsr;

    /**
     * 资产_收益率（含溢价）%
     */
    @Column(name = "ZC_SYLHYJ")
    private BigDecimal zcSylhyj;

    /**
     * 资产_实际溢(折)价
     */
    @Column(name = "ZC_SJYJ")
    private BigDecimal zcSjyj;

    /**
     * 资产_收益率（含溢价、损益）%
     */
    @Column(name = "ZC_SYLHYJSY")
    private BigDecimal zcSylhyjsy;

    /**
     * 资产_公允价值变动损益
     */
    @Column(name = "ZC_GYJZBDSY")
    private BigDecimal zcGyjzbdsy;

    /**
     * 资产_上月余额
     */
    @Column(name = "ZC_SYYE")
    private BigDecimal zcSyye;

    /**
     * 资产_较上月(+/-)
     */
    @Column(name = "ZC_JSY")
    private String zcJsy;

    /**
     * 资产_较上月增幅
     */
    @Column(name = "ZC_JSYZF")
    private BigDecimal zcJsyzf;

    /**
     * 资产_年初余额
     */
    @Column(name = "ZC_NCYE")
    private BigDecimal zcNcye;

    /**
     * 资产_较年初(+/-)
     */
    @Column(name = "ZC_JNC")
    private String zcJnc;

    /**
     * 资产_较年初增幅
     */
    @Column(name = "ZC_JNCZF")
    private BigDecimal zcJnczf;

    /**
     * 资产_日均额
     */
    @Column(name = "ZC_RJE")
    private BigDecimal zcRje;

    /**
     * 负债
     */
    @Column(name = "FZ")
    private String fz;

    /**
     * 负债_余额
     */
    @Column(name = "FZ_YE")
    private BigDecimal fzYe;

    /**
     * 负债_付息率%
     */
    @Column(name = "FZ_FXL")
    private BigDecimal fzFxl;

    /**
     * 负债_利息支出
     */
    @Column(name = "FZ_LXZC")
    private BigDecimal fzLxzc;

    /**
     * 负债_上月余额
     */
    @Column(name = "FZ_SYYE")
    private BigDecimal fzSyye;

    /**
     * 负债_较上月(+/-)
     */
    @Column(name = "FZ_JSY")
    private String fzJsy;

    /**
     * 负债_较上月增幅
     */
    @Column(name = "FZ_JSYZF")
    private BigDecimal fzJsyzf;

    /**
     * 负债_年初余额
     */
    @Column(name = "FZ_NCYE")
    private BigDecimal fzNcye;

    /**
     * 负债_较年初(+/-)
     */
    @Column(name = "FZ_JNC")
    private String fzJnc;

    /**
     * 负债_较年初增幅
     */
    @Column(name = "FZ_JNCZF")
    private BigDecimal fzJnczf;

    /**
     * 负债_日均额
     */
    @Column(name = "FZ_RJE")
    private BigDecimal fzRje;

    @Transient
    private String zco;
    @Transient
    private String fzo;

    private static final long serialVersionUID = 1L;

    /**
     * 获取资产
     *
     * @return ZC - 资产
     */
    public String getZc() {
        return zc;
    }

    /**
     * 设置资产
     *
     * @param zc 资产
     */
    public void setZc(String zc) {
        this.zc = zc;
    }

    /**
     * 获取资产_余额
     *
     * @return ZC_YE - 资产_余额
     */
    public BigDecimal getZcYe() {
        return zcYe;
    }

    /**
     * 设置资产_余额
     *
     * @param zcYe 资产_余额
     */
    public void setZcYe(BigDecimal zcYe) {
        this.zcYe = zcYe;
    }

    /**
     * 获取资产_收益率
     *
     * @return ZC_SYL - 资产_收益率
     */
    public BigDecimal getZcSyl() {
        return zcSyl;
    }

    /**
     * 设置资产_收益率
     *
     * @param zcSyl 资产_收益率
     */
    public void setZcSyl(BigDecimal zcSyl) {
        this.zcSyl = zcSyl;
    }

    /**
     * 获取资产_利息收入
     *
     * @return ZC_LXSR - 资产_利息收入
     */
    public BigDecimal getZcLxsr() {
        return zcLxsr;
    }

    /**
     * 设置资产_利息收入
     *
     * @param zcLxsr 资产_利息收入
     */
    public void setZcLxsr(BigDecimal zcLxsr) {
        this.zcLxsr = zcLxsr;
    }

    /**
     * 获取资产_收益率（含溢价）%
     *
     * @return ZC_SYLHYJ - 资产_收益率（含溢价）%
     */
    public BigDecimal getZcSylhyj() {
        return zcSylhyj;
    }

    /**
     * 设置资产_收益率（含溢价）%
     *
     * @param zcSylhyj 资产_收益率（含溢价）%
     */
    public void setZcSylhyj(BigDecimal zcSylhyj) {
        this.zcSylhyj = zcSylhyj;
    }

    /**
     * 获取资产_实际溢(折)价
     *
     * @return ZC_SJYJ - 资产_实际溢(折)价
     */
    public BigDecimal getZcSjyj() {
        return zcSjyj;
    }

    /**
     * 设置资产_实际溢(折)价
     *
     * @param zcSjyj 资产_实际溢(折)价
     */
    public void setZcSjyj(BigDecimal zcSjyj) {
        this.zcSjyj = zcSjyj;
    }

    /**
     * 获取资产_收益率（含溢价、损益）%
     *
     * @return ZC_SYLHYJSY - 资产_收益率（含溢价、损益）%
     */
    public BigDecimal getZcSylhyjsy() {
        return zcSylhyjsy;
    }

    /**
     * 设置资产_收益率（含溢价、损益）%
     *
     * @param zcSylhyjsy 资产_收益率（含溢价、损益）%
     */
    public void setZcSylhyjsy(BigDecimal zcSylhyjsy) {
        this.zcSylhyjsy = zcSylhyjsy;
    }

    /**
     * 获取资产_公允价值变动损益
     *
     * @return ZC_GYJZBDSY - 资产_公允价值变动损益
     */
    public BigDecimal getZcGyjzbdsy() {
        return zcGyjzbdsy;
    }

    /**
     * 设置资产_公允价值变动损益
     *
     * @param zcGyjzbdsy 资产_公允价值变动损益
     */
    public void setZcGyjzbdsy(BigDecimal zcGyjzbdsy) {
        this.zcGyjzbdsy = zcGyjzbdsy;
    }

    /**
     * 获取资产_上月余额
     *
     * @return ZC_SYYE - 资产_上月余额
     */
    public BigDecimal getZcSyye() {
        return zcSyye;
    }

    /**
     * 设置资产_上月余额
     *
     * @param zcSyye 资产_上月余额
     */
    public void setZcSyye(BigDecimal zcSyye) {
        this.zcSyye = zcSyye;
    }

    /**
     * 获取资产_较上月(+/-)
     *
     * @return ZC_JSY - 资产_较上月(+/-)
     */
    public String getZcJsy() {
        return zcJsy;
    }

    /**
     * 设置资产_较上月(+/-)
     *
     * @param zcJsy 资产_较上月(+/-)
     */
    public void setZcJsy(String zcJsy) {
        this.zcJsy = zcJsy;
    }

    /**
     * 获取资产_较上月增幅
     *
     * @return ZC_JSYZF - 资产_较上月增幅
     */
    public BigDecimal getZcJsyzf() {
        return zcJsyzf;
    }

    /**
     * 设置资产_较上月增幅
     *
     * @param zcJsyzf 资产_较上月增幅
     */
    public void setZcJsyzf(BigDecimal zcJsyzf) {
        this.zcJsyzf = zcJsyzf;
    }

    /**
     * 获取资产_年初余额
     *
     * @return ZC_NCYE - 资产_年初余额
     */
    public BigDecimal getZcNcye() {
        return zcNcye;
    }

    /**
     * 设置资产_年初余额
     *
     * @param zcNcye 资产_年初余额
     */
    public void setZcNcye(BigDecimal zcNcye) {
        this.zcNcye = zcNcye;
    }

    /**
     * 获取资产_较年初(+/-)
     *
     * @return ZC_JNC - 资产_较年初(+/-)
     */
    public String getZcJnc() {
        return zcJnc;
    }

    /**
     * 设置资产_较年初(+/-)
     *
     * @param zcJnc 资产_较年初(+/-)
     */
    public void setZcJnc(String zcJnc) {
        this.zcJnc = zcJnc;
    }

    /**
     * 获取资产_较年初增幅
     *
     * @return ZC_JNCZF - 资产_较年初增幅
     */
    public BigDecimal getZcJnczf() {
        return zcJnczf;
    }

    /**
     * 设置资产_较年初增幅
     *
     * @param zcJnczf 资产_较年初增幅
     */
    public void setZcJnczf(BigDecimal zcJnczf) {
        this.zcJnczf = zcJnczf;
    }

    /**
     * 获取资产_日均额
     *
     * @return ZC_RJE - 资产_日均额
     */
    public BigDecimal getZcRje() {
        return zcRje;
    }

    /**
     * 设置资产_日均额
     *
     * @param zcRje 资产_日均额
     */
    public void setZcRje(BigDecimal zcRje) {
        this.zcRje = zcRje;
    }

    /**
     * 获取负债
     *
     * @return FZ - 负债
     */
    public String getFz() {
        return fz;
    }

    /**
     * 设置负债
     *
     * @param fz 负债
     */
    public void setFz(String fz) {
        this.fz = fz;
    }

    /**
     * 获取负债_余额
     *
     * @return FZ_YE - 负债_余额
     */
    public BigDecimal getFzYe() {
        return fzYe;
    }

    /**
     * 设置负债_余额
     *
     * @param fzYe 负债_余额
     */
    public void setFzYe(BigDecimal fzYe) {
        this.fzYe = fzYe;
    }

    /**
     * 获取负债_付息率%
     *
     * @return FZ_FXL - 负债_付息率%
     */
    public BigDecimal getFzFxl() {
        return fzFxl;
    }

    /**
     * 设置负债_付息率%
     *
     * @param fzFxl 负债_付息率%
     */
    public void setFzFxl(BigDecimal fzFxl) {
        this.fzFxl = fzFxl;
    }

    /**
     * 获取负债_利息支出
     *
     * @return FZ_LXZC - 负债_利息支出
     */
    public BigDecimal getFzLxzc() {
        return fzLxzc;
    }

    /**
     * 设置负债_利息支出
     *
     * @param fzLxzc 负债_利息支出
     */
    public void setFzLxzc(BigDecimal fzLxzc) {
        this.fzLxzc = fzLxzc;
    }

    /**
     * 获取负债_上月余额
     *
     * @return FZ_SYYE - 负债_上月余额
     */
    public BigDecimal getFzSyye() {
        return fzSyye;
    }

    /**
     * 设置负债_上月余额
     *
     * @param fzSyye 负债_上月余额
     */
    public void setFzSyye(BigDecimal fzSyye) {
        this.fzSyye = fzSyye;
    }

    /**
     * 获取负债_较上月(+/-)
     *
     * @return FZ_JSY - 负债_较上月(+/-)
     */
    public String getFzJsy() {
        return fzJsy;
    }

    /**
     * 设置负债_较上月(+/-)
     *
     * @param fzJsy 负债_较上月(+/-)
     */
    public void setFzJsy(String fzJsy) {
        this.fzJsy = fzJsy;
    }

    /**
     * 获取负债_较上月增幅
     *
     * @return FZ_JSYZF - 负债_较上月增幅
     */
    public BigDecimal getFzJsyzf() {
        return fzJsyzf;
    }

    /**
     * 设置负债_较上月增幅
     *
     * @param fzJsyzf 负债_较上月增幅
     */
    public void setFzJsyzf(BigDecimal fzJsyzf) {
        this.fzJsyzf = fzJsyzf;
    }

    /**
     * 获取负债_年初余额
     *
     * @return FZ_NCYE - 负债_年初余额
     */
    public BigDecimal getFzNcye() {
        return fzNcye;
    }

    /**
     * 设置负债_年初余额
     *
     * @param fzNcye 负债_年初余额
     */
    public void setFzNcye(BigDecimal fzNcye) {
        this.fzNcye = fzNcye;
    }

    /**
     * 获取负债_较年初(+/-)
     *
     * @return FZ_JNC - 负债_较年初(+/-)
     */
    public String getFzJnc() {
        return fzJnc;
    }

    /**
     * 设置负债_较年初(+/-)
     *
     * @param fzJnc 负债_较年初(+/-)
     */
    public void setFzJnc(String fzJnc) {
        this.fzJnc = fzJnc;
    }

    /**
     * 获取负债_较年初增幅
     *
     * @return FZ_JNCZF - 负债_较年初增幅
     */
    public BigDecimal getFzJnczf() {
        return fzJnczf;
    }

    /**
     * 设置负债_较年初增幅
     *
     * @param fzJnczf 负债_较年初增幅
     */
    public void setFzJnczf(BigDecimal fzJnczf) {
        this.fzJnczf = fzJnczf;
    }

    /**
     * 获取负债_日均额
     *
     * @return FZ_RJE - 负债_日均额
     */
    public BigDecimal getFzRje() {
        return fzRje;
    }

    /**
     * 设置负债_日均额
     *
     * @param fzRje 负债_日均额
     */
    public void setFzRje(BigDecimal fzRje) {
        this.fzRje = fzRje;
    }

    public String getZco() {
        return zco;
    }

    public void setZco(String zco) {
        this.zco = zco;
    }

    public String getFzo() {
        return fzo;
    }

    public void setFzo(String fzo) {
        this.fzo = fzo;
    }
}