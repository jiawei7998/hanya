package com.singlee.hrbnewport.model;

import java.math.BigDecimal;

/**
    * 重点业务对账表
    */
public class IfsReportImportantCheck {
    /**
    * 项目
    */
    private String product;

    /**
    * 总账数据
    */
    private BigDecimal totcalData;

    /**
    * 业务明细数据
    */
    private BigDecimal listData;

    /**
    * 差异
    */
    private BigDecimal differenceData;

    /**
    * 差异原因
    */
    private String differenceReason;

    /**
    * 牵头报送部门
    */
    private String leadDepart;

    /**
    * 配合部门
    */
    private String assortDepart;

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product == null ? null : product.trim();
    }

    public BigDecimal getTotcalData() {
        return totcalData;
    }

    public void setTotcalData(BigDecimal totcalData) {
        this.totcalData = totcalData;
    }

    public BigDecimal getListData() {
        return listData;
    }

    public void setListData(BigDecimal listData) {
        this.listData = listData;
    }

    public BigDecimal getDifferenceData() {
        return differenceData;
    }

    public void setDifferenceData(BigDecimal differenceData) {
        this.differenceData = differenceData;
    }

    public String getDifferenceReason() {
        return differenceReason;
    }

    public void setDifferenceReason(String differenceReason) {
        this.differenceReason = differenceReason == null ? null : differenceReason.trim();
    }

    public String getLeadDepart() {
        return leadDepart;
    }

    public void setLeadDepart(String leadDepart) {
        this.leadDepart = leadDepart == null ? null : leadDepart.trim();
    }

    public String getAssortDepart() {
        return assortDepart;
    }

    public void setAssortDepart(String assortDepart) {
        this.assortDepart = assortDepart == null ? null : assortDepart.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", product=").append(product);
        sb.append(", totcalData=").append(totcalData);
        sb.append(", listData=").append(listData);
        sb.append(", differenceData=").append(differenceData);
        sb.append(", differenceReason=").append(differenceReason);
        sb.append(", leadDepart=").append(leadDepart);
        sb.append(", assortDepart=").append(assortDepart);
        sb.append("]");
        return sb.toString();
    }
}