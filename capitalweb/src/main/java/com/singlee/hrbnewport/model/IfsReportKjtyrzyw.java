package com.singlee.hrbnewport.model;

import javax.persistence.Id;
import java.io.Serializable;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/21 10:28
 * @description：${description}
 * @modified By：
 * @version:
 */
/**
 * 2103跨境同业融资业务信息（变更）
 */
public class IfsReportKjtyrzyw implements Serializable {
    /**
     * 申报号码
     */
    private String sbhm;

    /**
     * 操作类型
     */
    private String czlx;

    /**
     * 变更/撤销原因
     */
    private String bgyy;

    /**
     * 报送机构代码
     */
    private String bsjgdm;

    /**
     * 是否自贸区业务
     */
    private String zmqyw;

    /**
     * 是否分账核算单元
     */
    private String fzhsdy;

    /**
     * 业务属性
     */
    private String ywsx;

    /**
     * 业务种类
     */
    private String ywzl;

    /**
     * 融出机构代码
     */
    private String rcjgdm;

    /**
     * 融出机构名称
     */
    private String rcjgmc;

    /**
     * 融出机构类型
     */
    private String rcjglx;

    /**
     * 融出机构所在国家（地区）代码
     */
    private String rcjgszgj;

    /**
     * 融入机构代码
     */
    private String rrjgdm;

    /**
     * 融入机构名称
     */
    private String rrjgmc;

    /**
     * 融入机构类型
     */
    private String rrjglx;

    /**
     * 融入机构所在国家（地区）代码
     */
    private String rrjgszhj;

    /**
     * 融资类型
     */
    private String rzlx;

    /**
     * 融资币种
     */
    private String rzbz;

    /**
     * 融资金额
     */
    private String rzje;

    /**
     * 是否联行及附属机构往来
     */
    private String sflx;

    /**
     * 交易日
     */
    private String jye;

    /**
     * 起息日
     */
    private String qxr;

    /**
     * 到期日
     */
    private String dqr;

    /**
     * 利率
     */
    private String ll;

    /**
     * 利率类型
     */
    private String lllx;

    /**
     * 利率期限单位
     */
    private String llqxdw;

    /**
     * 利率重新定价期限
     */
    private String llcxdjqx;

    /**
     * 浮动利率基准
     */
    private String fdlljz;

    /**
     * 浮动利率加点
     */
    private String fdlljd;

    /**
     * 融资（贷款）状态
     */
    private String rzzt;

    /**
     * 逾期未还款金额
     */
    private String yqwhk;

    /**
     * 银行业务编号
     */
    @Id
    private String yhywbh;

    /**
     * 交易附言
     */
    private String jyfy;

    /**
     * 账务日期
     */
    private String postdate;

    private static final long serialVersionUID = 1L;

    public String getSbhm() {
        return sbhm;
    }

    public void setSbhm(String sbhm) {
        this.sbhm = sbhm;
    }

    public String getCzlx() {
        return czlx;
    }

    public void setCzlx(String czlx) {
        this.czlx = czlx;
    }

    public String getBgyy() {
        return bgyy;
    }

    public void setBgyy(String bgyy) {
        this.bgyy = bgyy;
    }

    public String getBsjgdm() {
        return bsjgdm;
    }

    public void setBsjgdm(String bsjgdm) {
        this.bsjgdm = bsjgdm;
    }

    public String getZmqyw() {
        return zmqyw;
    }

    public void setZmqyw(String zmqyw) {
        this.zmqyw = zmqyw;
    }

    public String getFzhsdy() {
        return fzhsdy;
    }

    public void setFzhsdy(String fzhsdy) {
        this.fzhsdy = fzhsdy;
    }

    public String getYwsx() {
        return ywsx;
    }

    public void setYwsx(String ywsx) {
        this.ywsx = ywsx;
    }

    public String getYwzl() {
        return ywzl;
    }

    public void setYwzl(String ywzl) {
        this.ywzl = ywzl;
    }

    public String getRcjgdm() {
        return rcjgdm;
    }

    public void setRcjgdm(String rcjgdm) {
        this.rcjgdm = rcjgdm;
    }

    public String getRcjgmc() {
        return rcjgmc;
    }

    public void setRcjgmc(String rcjgmc) {
        this.rcjgmc = rcjgmc;
    }

    public String getRcjglx() {
        return rcjglx;
    }

    public void setRcjglx(String rcjglx) {
        this.rcjglx = rcjglx;
    }

    public String getRcjgszgj() {
        return rcjgszgj;
    }

    public void setRcjgszgj(String rcjgszgj) {
        this.rcjgszgj = rcjgszgj;
    }

    public String getRrjgdm() {
        return rrjgdm;
    }

    public void setRrjgdm(String rrjgdm) {
        this.rrjgdm = rrjgdm;
    }

    public String getRrjgmc() {
        return rrjgmc;
    }

    public void setRrjgmc(String rrjgmc) {
        this.rrjgmc = rrjgmc;
    }

    public String getRrjglx() {
        return rrjglx;
    }

    public void setRrjglx(String rrjglx) {
        this.rrjglx = rrjglx;
    }

    public String getRrjgszhj() {
        return rrjgszhj;
    }

    public void setRrjgszhj(String rrjgszhj) {
        this.rrjgszhj = rrjgszhj;
    }

    public String getRzlx() {
        return rzlx;
    }

    public void setRzlx(String rzlx) {
        this.rzlx = rzlx;
    }

    public String getRzbz() {
        return rzbz;
    }

    public void setRzbz(String rzbz) {
        this.rzbz = rzbz;
    }

    public String getRzje() {
        return rzje;
    }

    public void setRzje(String rzje) {
        this.rzje = rzje;
    }

    public String getSflx() {
        return sflx;
    }

    public void setSflx(String sflx) {
        this.sflx = sflx;
    }

    public String getJye() {
        return jye;
    }

    public void setJye(String jye) {
        this.jye = jye;
    }

    public String getQxr() {
        return qxr;
    }

    public void setQxr(String qxr) {
        this.qxr = qxr;
    }

    public String getDqr() {
        return dqr;
    }

    public void setDqr(String dqr) {
        this.dqr = dqr;
    }

    public String getLl() {
        return ll;
    }

    public void setLl(String ll) {
        this.ll = ll;
    }

    public String getLllx() {
        return lllx;
    }

    public void setLllx(String lllx) {
        this.lllx = lllx;
    }

    public String getLlqxdw() {
        return llqxdw;
    }

    public void setLlqxdw(String llqxdw) {
        this.llqxdw = llqxdw;
    }

    public String getLlcxdjqx() {
        return llcxdjqx;
    }

    public void setLlcxdjqx(String llcxdjqx) {
        this.llcxdjqx = llcxdjqx;
    }

    public String getFdlljz() {
        return fdlljz;
    }

    public void setFdlljz(String fdlljz) {
        this.fdlljz = fdlljz;
    }

    public String getFdlljd() {
        return fdlljd;
    }

    public void setFdlljd(String fdlljd) {
        this.fdlljd = fdlljd;
    }

    public String getRzzt() {
        return rzzt;
    }

    public void setRzzt(String rzzt) {
        this.rzzt = rzzt;
    }

    public String getYqwhk() {
        return yqwhk;
    }

    public void setYqwhk(String yqwhk) {
        this.yqwhk = yqwhk;
    }

    public String getYhywbh() {
        return yhywbh;
    }

    public void setYhywbh(String yhywbh) {
        this.yhywbh = yhywbh;
    }

    public String getJyfy() {
        return jyfy;
    }

    public void setJyfy(String jyfy) {
        this.jyfy = jyfy;
    }

    public String getPostdate() {
        return postdate;
    }

    public void setPostdate(String postdate) {
        this.postdate = postdate;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", sbhm=").append(sbhm);
        sb.append(", czlx=").append(czlx);
        sb.append(", bgyy=").append(bgyy);
        sb.append(", bsjgdm=").append(bsjgdm);
        sb.append(", zmqyw=").append(zmqyw);
        sb.append(", fzhsdy=").append(fzhsdy);
        sb.append(", ywsx=").append(ywsx);
        sb.append(", ywzl=").append(ywzl);
        sb.append(", rcjgdm=").append(rcjgdm);
        sb.append(", rcjgmc=").append(rcjgmc);
        sb.append(", rcjglx=").append(rcjglx);
        sb.append(", rcjgszgj=").append(rcjgszgj);
        sb.append(", rrjgdm=").append(rrjgdm);
        sb.append(", rrjgmc=").append(rrjgmc);
        sb.append(", rrjglx=").append(rrjglx);
        sb.append(", rrjgszhj=").append(rrjgszhj);
        sb.append(", rzlx=").append(rzlx);
        sb.append(", rzbz=").append(rzbz);
        sb.append(", rzje=").append(rzje);
        sb.append(", sflx=").append(sflx);
        sb.append(", jye=").append(jye);
        sb.append(", qxr=").append(qxr);
        sb.append(", dqr=").append(dqr);
        sb.append(", ll=").append(ll);
        sb.append(", lllx=").append(lllx);
        sb.append(", llqxdw=").append(llqxdw);
        sb.append(", llcxdjqx=").append(llcxdjqx);
        sb.append(", fdlljz=").append(fdlljz);
        sb.append(", fdlljd=").append(fdlljd);
        sb.append(", rzzt=").append(rzzt);
        sb.append(", yqwhk=").append(yqwhk);
        sb.append(", yhywbh=").append(yhywbh);
        sb.append(", jyfy=").append(jyfy);
        sb.append(", postdate=").append(postdate);
        sb.append("]");
        return sb.toString();
    }
}