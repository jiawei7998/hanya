package com.singlee.hrbnewport.model;

import java.math.BigDecimal;

/**
    * 交易账户债券投资结构情况
    */
public class TbTradingAccountBondinvest {
    /**
    * 债券类别
    */
    private String bondType;

    /**
    * 占比(%)
    */
    private BigDecimal bondStartProportion;

    /**
    * 占比(%)
    */
    private BigDecimal bondEndProportion;

    /**
    * 发行主题外部评级
    */
    private String fxztwbpj;

    /**
    * 债项评级
    */
    private String qxpj;

    /**
    * 余额面值
    */
    private BigDecimal lastFaceamt;

    /**
    * 占比(%)
    */
    private BigDecimal lastProportion;

    /**
    * 增/减/持
    */
    private BigDecimal zjc;

    /**
    * 余额面值
    */
    private BigDecimal endFaceamt;

    /**
    * 占比(%)
    */
    private BigDecimal endProportion;

    public String getBondType() {
        return bondType;
    }

    public void setBondType(String bondType) {
        this.bondType = bondType;
    }

    public BigDecimal getBondStartProportion() {
        return bondStartProportion;
    }

    public void setBondStartProportion(BigDecimal bondStartProportion) {
        this.bondStartProportion = bondStartProportion;
    }

    public BigDecimal getBondEndProportion() {
        return bondEndProportion;
    }

    public void setBondEndProportion(BigDecimal bondEndProportion) {
        this.bondEndProportion = bondEndProportion;
    }

    public String getFxztwbpj() {
        return fxztwbpj;
    }

    public void setFxztwbpj(String fxztwbpj) {
        this.fxztwbpj = fxztwbpj;
    }

    public String getQxpj() {
        return qxpj;
    }

    public void setQxpj(String qxpj) {
        this.qxpj = qxpj;
    }

    public BigDecimal getLastFaceamt() {
        return lastFaceamt;
    }

    public void setLastFaceamt(BigDecimal lastFaceamt) {
        this.lastFaceamt = lastFaceamt;
    }

    public BigDecimal getLastProportion() {
        return lastProportion;
    }

    public void setLastProportion(BigDecimal lastProportion) {
        this.lastProportion = lastProportion;
    }

    public BigDecimal getZjc() {
        return zjc;
    }

    public void setZjc(BigDecimal zjc) {
        this.zjc = zjc;
    }

    public BigDecimal getEndFaceamt() {
        return endFaceamt;
    }

    public void setEndFaceamt(BigDecimal endFaceamt) {
        this.endFaceamt = endFaceamt;
    }

    public BigDecimal getEndProportion() {
        return endProportion;
    }

    public void setEndProportion(BigDecimal endProportion) {
        this.endProportion = endProportion;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", bondType=").append(bondType);
        sb.append(", bondStartProportion=").append(bondStartProportion);
        sb.append(", bondEndProportion=").append(bondEndProportion);
        sb.append(", fxztwbpj=").append(fxztwbpj);
        sb.append(", qxpj=").append(qxpj);
        sb.append(", lastFaceamt=").append(lastFaceamt);
        sb.append(", lastProportion=").append(lastProportion);
        sb.append(", zjc=").append(zjc);
        sb.append(", endFaceamt=").append(endFaceamt);
        sb.append(", endProportion=").append(endProportion);
        sb.append("]");
        return sb.toString();
    }
}