package com.singlee.hrbnewport.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

/**
 * 债券投资到期结构表
 */
@Table(name = "IFS_REPORT_JRSCBYWGL_ZQTZDQ")
public class IfsReportJrscbywglZqtzdq implements Serializable {
    /**
     * 到期月
     */
    @Column(name = "DQY")
    private String dqy;

    /**
     * 投资到期额_债券
     */
    @Column(name = "TZDQE_ZQ")
    private BigDecimal tzdqeZq;

    /**
     * 投资到期额_存单
     */
    @Column(name = "TZDQE_CD")
    private BigDecimal tzdqeCd;

    /**
     * 投资余额_债券
     */
    @Column(name = "TZYE_ZQ")
    private BigDecimal tzyeZq;

    /**
     * 投资余额_存单
     */
    @Column(name = "TZYE_CD")
    private BigDecimal tzyeCd;

    /**
     * 逾期
     */
    @Column(name = "YQ")
    private BigDecimal yq;

    private static final long serialVersionUID = 1L;

    /**
     * 获取到期月
     *
     * @return DQY - 到期月
     */
    public String getDqy() {
        return dqy;
    }

    /**
     * 设置到期月
     *
     * @param dqy 到期月
     */
    public void setDqy(String dqy) {
        this.dqy = dqy;
    }

    /**
     * 获取投资到期额_债券
     *
     * @return TZDQE_ZQ - 投资到期额_债券
     */
    public BigDecimal getTzdqeZq() {
        return tzdqeZq;
    }

    /**
     * 设置投资到期额_债券
     *
     * @param tzdqeZq 投资到期额_债券
     */
    public void setTzdqeZq(BigDecimal tzdqeZq) {
        this.tzdqeZq = tzdqeZq;
    }

    /**
     * 获取投资到期额_存单
     *
     * @return TZDQE_CD - 投资到期额_存单
     */
    public BigDecimal getTzdqeCd() {
        return tzdqeCd;
    }

    /**
     * 设置投资到期额_存单
     *
     * @param tzdqeCd 投资到期额_存单
     */
    public void setTzdqeCd(BigDecimal tzdqeCd) {
        this.tzdqeCd = tzdqeCd;
    }

    /**
     * 获取投资余额_债券
     *
     * @return TZYE_ZQ - 投资余额_债券
     */
    public BigDecimal getTzyeZq() {
        return tzyeZq;
    }

    /**
     * 设置投资余额_债券
     *
     * @param tzyeZq 投资余额_债券
     */
    public void setTzyeZq(BigDecimal tzyeZq) {
        this.tzyeZq = tzyeZq;
    }

    /**
     * 获取投资余额_存单
     *
     * @return TZYE_CD - 投资余额_存单
     */
    public BigDecimal getTzyeCd() {
        return tzyeCd;
    }

    /**
     * 设置投资余额_存单
     *
     * @param tzyeCd 投资余额_存单
     */
    public void setTzyeCd(BigDecimal tzyeCd) {
        this.tzyeCd = tzyeCd;
    }

    /**
     * 获取逾期
     *
     * @return YQ - 逾期
     */
    public BigDecimal getYq() {
        return yq;
    }

    /**
     * 设置逾期
     *
     * @param yq 逾期
     */
    public void setYq(BigDecimal yq) {
        this.yq = yq;
    }
}