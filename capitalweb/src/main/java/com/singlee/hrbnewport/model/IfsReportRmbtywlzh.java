package com.singlee.hrbnewport.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 20:51
 * @description：${description}
 * @modified By：
 * @version:   
 */
@Embeddable
public class IfsReportRmbtywlzh implements Serializable {
    /**
    * 申报号码
    */
    private String sbhm;

    /**
    * 操作类型
    */
    private String czlx;

    /**
    * 变更/撤销原因
    */
    private String bgyy;

    /**
    * 银行机构代码
    */
    private String yhjgdm;

    /**
    * 主账户账号
    */
    @Id
    @Column(name="zhhzh")
    private String zhhzh;

    /**
    * 币种
    */
    @Id
    @Column(name="bz")
    private String bz;

    /**
    * 账户状态
    */
    private String zhzt;

    /**
    * 业务属性
    */
    private String ywsx;

    /**
    * 开户日期
    */
    @Id
    @Column(name="khrq")
    private String khrq;

    /**
    * 操作日期
    */
    private String czrq;

    /**
    * 备案日期
    */
    private String barq;

    /**
    * 明细操作类型
    */
    private String mxczlx;

    /**
    * 序号
    */
    private String xh;

    /**
    * 账户状态
    */
    private String zzhzt;

    /**
    * 子账户账号
    */
    private String zzhzh;

    /**
    * 子账户币种
    */
    private String zzhbz;

    /**
    * 子账户开户日期
    */
    private String zzhkhrq;

    /**
    * 子账户操作日期
    */
    private String zzhczrq;

    /**
    * 国民经济部门分类
    */
    private String gmjjbmfl;

    /**
    * 银行机构名称
    */
    private String yhjgmc;

    /**
    * 银行机构英文名称
    */
    private String yhjgyw;

    /**
    * 注册国家（地区）代码
    */
    private String zcgj;

    /**
    * 境外银行机构SWIFT BIC
    */
    private String yhjg;

    /**
    * 境外联系人名称
    */
    private String lxrmc;

    /**
    * 境外联系电话
    */
    private String lxdh;

    /**
    * 境外备注
    */
    private String jwbz;

    /**
    * 银行机构邮编
    */
    private String yhjgyb;

    /**
    * 境外联系地址
    */
    private String lxdz;

    /**
    * 银行SWIFT BIC
    */
    private String yhbic;

    /**
    * 境内联系人名称
    */
    private String jnlxrmc;

    /**
    * 境内联系电话
    */
    private String jnlxrdh;

    /**
    * 境内联系地址
    */
    private String jnlxrdz;

    /**
    * 境内邮编
    */
    private String yb;

    /**
    * 境内电子邮件地址
    */
    private String dzyjdz;
//    /**
//     * 账务日期
//     */
//    @Id
//    private String postdate;

    private static final long serialVersionUID = 1L;

    public String getSbhm() {
        return sbhm;
    }

    public void setSbhm(String sbhm) {
        this.sbhm = sbhm;
    }

    public String getCzlx() {
        return czlx;
    }

    public void setCzlx(String czlx) {
        this.czlx = czlx;
    }

    public String getBgyy() {
        return bgyy;
    }

    public void setBgyy(String bgyy) {
        this.bgyy = bgyy;
    }

    public String getYhjgdm() {
        return yhjgdm;
    }

    public void setYhjgdm(String yhjgdm) {
        this.yhjgdm = yhjgdm;
    }

    public String getZhhzh() {
        return zhhzh;
    }

    public void setZhhzh(String zhhzh) {
        this.zhhzh = zhhzh;
    }

    public String getBz() {
        return bz;
    }

    public void setBz(String bz) {
        this.bz = bz;
    }

    public String getZhzt() {
        return zhzt;
    }

    public void setZhzt(String zhzt) {
        this.zhzt = zhzt;
    }

    public String getYwsx() {
        return ywsx;
    }

    public void setYwsx(String ywsx) {
        this.ywsx = ywsx;
    }

    public String getKhrq() {
        return khrq;
    }

    public void setKhrq(String khrq) {
        this.khrq = khrq;
    }

    public String getCzrq() {
        return czrq;
    }

    public void setCzrq(String czrq) {
        this.czrq = czrq;
    }

    public String getBarq() {
        return barq;
    }

    public void setBarq(String barq) {
        this.barq = barq;
    }

    public String getMxczlx() {
        return mxczlx;
    }

    public void setMxczlx(String mxczlx) {
        this.mxczlx = mxczlx;
    }

    public String getXh() {
        return xh;
    }

    public void setXh(String xh) {
        this.xh = xh;
    }

    public String getZzhzt() {
        return zzhzt;
    }

    public void setZzhzt(String zzhzt) {
        this.zzhzt = zzhzt;
    }

    public String getZzhzh() {
        return zzhzh;
    }

    public void setZzhzh(String zzhzh) {
        this.zzhzh = zzhzh;
    }

    public String getZzhbz() {
        return zzhbz;
    }

    public void setZzhbz(String zzhbz) {
        this.zzhbz = zzhbz;
    }

    public String getZzhkhrq() {
        return zzhkhrq;
    }

    public void setZzhkhrq(String zzhkhrq) {
        this.zzhkhrq = zzhkhrq;
    }

    public String getZzhczrq() {
        return zzhczrq;
    }

    public void setZzhczrq(String zzhczrq) {
        this.zzhczrq = zzhczrq;
    }

    public String getGmjjbmfl() {
        return gmjjbmfl;
    }

    public void setGmjjbmfl(String gmjjbmfl) {
        this.gmjjbmfl = gmjjbmfl;
    }

    public String getYhjgmc() {
        return yhjgmc;
    }

    public void setYhjgmc(String yhjgmc) {
        this.yhjgmc = yhjgmc;
    }

    public String getYhjgyw() {
        return yhjgyw;
    }

    public void setYhjgyw(String yhjgyw) {
        this.yhjgyw = yhjgyw;
    }

    public String getZcgj() {
        return zcgj;
    }

    public void setZcgj(String zcgj) {
        this.zcgj = zcgj;
    }

    public String getYhjg() {
        return yhjg;
    }

    public void setYhjg(String yhjg) {
        this.yhjg = yhjg;
    }

    public String getLxrmc() {
        return lxrmc;
    }

    public void setLxrmc(String lxrmc) {
        this.lxrmc = lxrmc;
    }

    public String getLxdh() {
        return lxdh;
    }

    public void setLxdh(String lxdh) {
        this.lxdh = lxdh;
    }

    public String getJwbz() {
        return jwbz;
    }

    public void setJwbz(String jwbz) {
        this.jwbz = jwbz;
    }

    public String getYhjgyb() {
        return yhjgyb;
    }

    public void setYhjgyb(String yhjgyb) {
        this.yhjgyb = yhjgyb;
    }

    public String getLxdz() {
        return lxdz;
    }

    public void setLxdz(String lxdz) {
        this.lxdz = lxdz;
    }

    public String getYhbic() {
        return yhbic;
    }

    public void setYhbic(String yhbic) {
        this.yhbic = yhbic;
    }

    public String getJnlxrmc() {
        return jnlxrmc;
    }

    public void setJnlxrmc(String jnlxrmc) {
        this.jnlxrmc = jnlxrmc;
    }

    public String getJnlxrdh() {
        return jnlxrdh;
    }

    public void setJnlxrdh(String jnlxrdh) {
        this.jnlxrdh = jnlxrdh;
    }

    public String getJnlxrdz() {
        return jnlxrdz;
    }

    public void setJnlxrdz(String jnlxrdz) {
        this.jnlxrdz = jnlxrdz;
    }

    public String getYb() {
        return yb;
    }

    public void setYb(String yb) {
        this.yb = yb;
    }

    public String getDzyjdz() {
        return dzyjdz;
    }

    public void setDzyjdz(String dzyjdz) {
        this.dzyjdz = dzyjdz;
    }

//    public String getPostdate() {
//        return postdate;
//    }
//
//    public void setPostdate(String postdate) {
//        this.postdate = postdate;
//    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", sbhm=").append(sbhm);
        sb.append(", czlx=").append(czlx);
        sb.append(", bgyy=").append(bgyy);
        sb.append(", yhjgdm=").append(yhjgdm);
        sb.append(", zhhzh=").append(zhhzh);
        sb.append(", bz=").append(bz);
        sb.append(", zhzt=").append(zhzt);
        sb.append(", ywsx=").append(ywsx);
        sb.append(", khrq=").append(khrq);
        sb.append(", czrq=").append(czrq);
        sb.append(", barq=").append(barq);
        sb.append(", mxczlx=").append(mxczlx);
        sb.append(", xh=").append(xh);
        sb.append(", zzhzt=").append(zzhzt);
        sb.append(", zzhzh=").append(zzhzh);
        sb.append(", zzhbz=").append(zzhbz);
        sb.append(", zzhkhrq=").append(zzhkhrq);
        sb.append(", zzhczrq=").append(zzhczrq);
        sb.append(", gmjjbmfl=").append(gmjjbmfl);
        sb.append(", yhjgmc=").append(yhjgmc);
        sb.append(", yhjgyw=").append(yhjgyw);
        sb.append(", zcgj=").append(zcgj);
        sb.append(", yhjg=").append(yhjg);
        sb.append(", lxrmc=").append(lxrmc);
        sb.append(", lxdh=").append(lxdh);
        sb.append(", jwbz=").append(jwbz);
        sb.append(", yhjgyb=").append(yhjgyb);
        sb.append(", lxdz=").append(lxdz);
        sb.append(", yhbic=").append(yhbic);
        sb.append(", jnlxrmc=").append(jnlxrmc);
        sb.append(", jnlxrdh=").append(jnlxrdh);
        sb.append(", jnlxrdz=").append(jnlxrdz);
        sb.append(", yb=").append(yb);
        sb.append(", dzyjdz=").append(dzyjdz);
        sb.append("]");
        return sb.toString();
    }
}