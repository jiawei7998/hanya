package com.singlee.hrbnewport.model;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author Liang
 * @date 2021/11/22 19:09
 * =======================
 */
public class A1411ReportVO implements Serializable {

    private static final long serialVersionUID = 1L;
    private Integer  id; //
    private String   zbbh; //指标编号
    private String   zbmc; //指标名称
    private BigDecimal rmbye	; //人民币（余额）
    private BigDecimal   sqye	; //上期余额
    private String   parent_id	;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getZbbh() {
        return zbbh;
    }

    public void setZbbh(String zbbh) {
        this.zbbh = zbbh;
    }

    public String getZbmc() {
        return zbmc;
    }

    public void setZbmc(String zbmc) {
        this.zbmc = zbmc;
    }

    public BigDecimal getRmbye() {
        return rmbye;
    }

    public void setRmbye(BigDecimal rmbye) {
        this.rmbye = rmbye;
    }

    public BigDecimal getSqye() {
        return sqye;
    }

    public void setSqye(BigDecimal sqye) {
        this.sqye = sqye;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }
}
