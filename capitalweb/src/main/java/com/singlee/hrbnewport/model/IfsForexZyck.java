package com.singlee.hrbnewport.model;

import java.math.BigDecimal;

/**
    * 外汇自营敞口情况表
    */
public class IfsForexZyck {
    /**
    * 日期
    */
    private String zydate;

    /**
    * USD
    */
    private BigDecimal usd;

    /**
    * EUR
    */
    private BigDecimal eur;

    /**
    * GBP
    */
    private BigDecimal gbp;

    /**
    * RUB
    */
    private BigDecimal rub;

    /**
    * NZD
    */
    private BigDecimal nzd;

    /**
    * JPY
    */
    private BigDecimal jpy;

    /**
    * HKD
    */
    private BigDecimal hkd;

    /**
    * CAD
    */
    private BigDecimal cad;

    /**
    * AUD
    */
    private BigDecimal aud;

    public String getZydate() {
        return zydate;
    }

    public void setZydate(String zydate) {
        this.zydate = zydate == null ? null : zydate.trim();
    }

    public BigDecimal getUsd() {
        return usd;
    }

    public void setUsd(BigDecimal usd) {
        this.usd = usd;
    }

    public BigDecimal getEur() {
        return eur;
    }

    public void setEur(BigDecimal eur) {
        this.eur = eur;
    }

    public BigDecimal getGbp() {
        return gbp;
    }

    public void setGbp(BigDecimal gbp) {
        this.gbp = gbp;
    }

    public BigDecimal getRub() {
        return rub;
    }

    public void setRub(BigDecimal rub) {
        this.rub = rub;
    }

    public BigDecimal getNzd() {
        return nzd;
    }

    public void setNzd(BigDecimal nzd) {
        this.nzd = nzd;
    }

    public BigDecimal getJpy() {
        return jpy;
    }

    public void setJpy(BigDecimal jpy) {
        this.jpy = jpy;
    }

    public BigDecimal getHkd() {
        return hkd;
    }

    public void setHkd(BigDecimal hkd) {
        this.hkd = hkd;
    }

    public BigDecimal getCad() {
        return cad;
    }

    public void setCad(BigDecimal cad) {
        this.cad = cad;
    }

    public BigDecimal getAud() {
        return aud;
    }

    public void setAud(BigDecimal aud) {
        this.aud = aud;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", zydate=").append(zydate);
        sb.append(", usd=").append(usd);
        sb.append(", eur=").append(eur);
        sb.append(", gbp=").append(gbp);
        sb.append(", rub=").append(rub);
        sb.append(", nzd=").append(nzd);
        sb.append(", jpy=").append(jpy);
        sb.append(", hkd=").append(hkd);
        sb.append(", cad=").append(cad);
        sb.append(", aud=").append(aud);
        sb.append("]");
        return sb.toString();
    }
}