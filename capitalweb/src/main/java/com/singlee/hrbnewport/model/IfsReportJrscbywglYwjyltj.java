package com.singlee.hrbnewport.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

/**
 * 业务交易量统计表
 */
@Table(name = "IFS_REPORT_JRSCBYWGL_YWJYLTJ")
public class IfsReportJrscbywglYwjyltj implements Serializable {
    /**
     * 项目
     */
    @Column(name = "PRODUCT")
    private String product;

    /**
     * 交易量
     */
    @Column(name = "JYL")
    private BigDecimal jyl;

    /**
     * 笔(只)数
     */
    @Column(name = "BS")
    private BigDecimal bs;

    /**
     * 备注
     */
    @Column(name = "BZ")
    private String bz;

    @Column(name = "PARENT_PRODUCT")
    private String parentProduct;

    private static final long serialVersionUID = 1L;

    /**
     * 获取项目
     *
     * @return PRODUCT - 项目
     */
    public String getProduct() {
        return product;
    }

    /**
     * 设置项目
     *
     * @param product 项目
     */
    public void setProduct(String product) {
        this.product = product;
    }

    /**
     * 获取交易量
     *
     * @return JYL - 交易量
     */
    public BigDecimal getJyl() {
        return jyl;
    }

    /**
     * 设置交易量
     *
     * @param jyl 交易量
     */
    public void setJyl(BigDecimal jyl) {
        this.jyl = jyl;
    }

    /**
     * 获取笔(只)数
     *
     * @return BS - 笔(只)数
     */
    public BigDecimal getBs() {
        return bs;
    }

    /**
     * 设置笔(只)数
     *
     * @param bs 笔(只)数
     */
    public void setBs(BigDecimal bs) {
        this.bs = bs;
    }

    /**
     * 获取备注
     *
     * @return BZ - 备注
     */
    public String getBz() {
        return bz;
    }

    /**
     * 设置备注
     *
     * @param bz 备注
     */
    public void setBz(String bz) {
        this.bz = bz;
    }

    /**
     * @return PARENT_PRODUCT
     */
    public String getParentProduct() {
        return parentProduct;
    }

    /**
     * @param parentProduct
     */
    public void setParentProduct(String parentProduct) {
        this.parentProduct = parentProduct;
    }
}