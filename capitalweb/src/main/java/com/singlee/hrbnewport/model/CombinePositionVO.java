package com.singlee.hrbnewport.model;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author Liang
 * @date 2021/11/17 11:01
 * =======================
 */
public class CombinePositionVO implements Serializable {
    private static final long serialVersionUID = 1L;
    private String busType; //投资类型
    private String acctDesc; //债券类型
    private String  issuer; //债券发行人
    private String  secID	; //债券代码
    private String   descr	; //债券名称
    private String   port	; //债券名称
    private String   vDate	; //起息日
    private String   teno	; //期限（天）
    private String   mDate	; //到期日
    private BigDecimal   faceAmt	; //债券面值
    private BigDecimal   coupRate	; //票面利率
    private BigDecimal   settAmt	; //账面价值（元）

    public String getBusType() {
        return busType;
    }

    public void setBusType(String busType) {
        this.busType = busType;
    }

    public String getAcctDesc() {
        return acctDesc;
    }

    public void setAcctDesc(String acctDesc) {
        this.acctDesc = acctDesc;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getIssuer() {
        return issuer;
    }

    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }

    public String getSecID() {
        return secID;
    }

    public void setSecID(String secID) {
        this.secID = secID;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public String getvDate() {
        return vDate;
    }

    public void setvDate(String vDate) {
        this.vDate = vDate;
    }

    public String getTeno() {
        return teno;
    }

    public void setTeno(String teno) {
        this.teno = teno;
    }

    public String getmDate() {
        return mDate;
    }

    public void setmDate(String mDate) {
        this.mDate = mDate;
    }

    public BigDecimal getFaceAmt() {
        return faceAmt;
    }

    public void setFaceAmt(BigDecimal faceAmt) {
        this.faceAmt = faceAmt;
    }

    public BigDecimal getCoupRate() {
        return coupRate;
    }

    public void setCoupRate8(BigDecimal coupRate) {
        this.coupRate = coupRate;
    }

    public BigDecimal getSettAmt() {
        return settAmt;
    }

    public void setSettAmt(BigDecimal settAmt) {
        this.settAmt = settAmt;
    }
}
