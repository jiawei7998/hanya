package com.singlee.hrbnewport.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

/**
 * G31投资业务情况表 非底层资产投资情况
 */
@Table(name = "IFS_REPORT_G3102")
public class IfsReportG3102 implements Serializable {
    /**
     * 项目
     */
    @Column(name = "PRODUCT")
    private String product;

    /**
     * 期末余额
     */
    @Column(name = "QMYE")
    private BigDecimal qmye;

    /**
     * 投资收入（年初至报告期末数）
     */
    @Column(name = "TZSR")
    private BigDecimal tzsr;

    /**
     * 持有非底层资产产生的间接负债
     */
    @Column(name = "CYFDCZCJJFZ")
    private BigDecimal cyfdczcjjfz;

    /**
     * 最终投向类型_货币市场工具及货币市场公募基金
     */
    @Column(name = "ZZTXLX_HBSCGJJHBSCGMJJ")
    private BigDecimal zztxlxHbscgjjhbscgmjj;

    /**
     * 最终投向类型_债券及债券公募基金
     */
    @Column(name = "ZZTXLX_ZQJZQGMJJ")
    private BigDecimal zztxlxZqjzqgmjj;

    /**
     * 最终投向类型_存款
     */
    @Column(name = "ZZTXLX_CK")
    private BigDecimal zztxlxCk;

    /**
     * 最终投向类型_信贷类投资
     */
    @Column(name = "ZZTXLX_XDLTZ")
    private BigDecimal zztxlxXdltz;

    /**
     * 最终投向类型_权益类投资及股票公募基金
     */
    @Column(name = "ZZTXLX_QYLTZJGPGMJJ")
    private BigDecimal zztxlxQyltzjgpgmjj;

    /**
     * 最终投向类型_其他
     */
    @Column(name = "ZZTXLX_QT")
    private BigDecimal zztxlxQt;

    /**
     * 由于估值方法不同而产生的差异
     */
    @Column(name = "YYGZFFBTRCSCY")
    private BigDecimal yygzffbtrcscy;

    /**
     * （最终投向为信贷类、权益类或其他的）行业归属_农林牧渔业
     */
    @Column(name = "HYGS_NLMYY")
    private BigDecimal hygsNlmyy;

    /**
     * （最终投向为信贷类、权益类或其他的）行业归属_制造业
     */
    @Column(name = "HYGS_ZZY")
    private BigDecimal hygsZzy;

    /**
     * （最终投向为信贷类、权益类或其他的）行业归属_采矿业
     */
    @Column(name = "HYGS_CKY")
    private BigDecimal hygsCky;

    /**
     * （最终投向为信贷类、权益类或其他的）行业归属_建筑业
     */
    @Column(name = "HYGS_JZY")
    private BigDecimal hygsJzy;

    /**
     * （最终投向为信贷类、权益类或其他的）行业归属_基础设施建设
     */
    @Column(name = "HYGS_JCJSSS")
    private BigDecimal hygsJcjsss;

    /**
     * （最终投向为信贷类、权益类或其他的）行业归属_房地产业
     */
    @Column(name = "HYGS_FDCY")
    private BigDecimal hygsFdcy;

    /**
     * （最终投向为信贷类、权益类或其他的）行业归属_金融业
     */
    @Column(name = "HYGS_JRY")
    private BigDecimal hygsJry;

    /**
     * （最终投向为信贷类、权益类或其他的）行业归属_其他服务业
     */
    @Column(name = "HYGS_QTFWY")
    private BigDecimal hygsQtfwy;

    /**
     * （最终投向为信贷类、权益类或其他的）行业归属_其他（含资金池业务）
     */
    @Column(name = "HYGS_QTHZJCYW")
    private BigDecimal hygsQthzjcyw;

    @Column(name = "PARENT_PRODUCT")
    private String parentProduct;

    @Transient
    private String producto;

    private static final long serialVersionUID = 1L;

    /**
     * 获取项目
     *
     * @return PRODUCT - 项目
     */
    public String getProduct() {
        return product;
    }

    /**
     * 设置项目
     *
     * @param product 项目
     */
    public void setProduct(String product) {
        this.product = product;
    }

    /**
     * 获取期末余额
     *
     * @return QMYE - 期末余额
     */
    public BigDecimal getQmye() {
        return qmye;
    }

    /**
     * 设置期末余额
     *
     * @param qmye 期末余额
     */
    public void setQmye(BigDecimal qmye) {
        this.qmye = qmye;
    }

    /**
     * 获取投资收入（年初至报告期末数）
     *
     * @return TZSR - 投资收入（年初至报告期末数）
     */
    public BigDecimal getTzsr() {
        return tzsr;
    }

    /**
     * 设置投资收入（年初至报告期末数）
     *
     * @param tzsr 投资收入（年初至报告期末数）
     */
    public void setTzsr(BigDecimal tzsr) {
        this.tzsr = tzsr;
    }

    /**
     * 获取持有非底层资产产生的间接负债
     *
     * @return CYFDCZCJJFZ - 持有非底层资产产生的间接负债
     */
    public BigDecimal getCyfdczcjjfz() {
        return cyfdczcjjfz;
    }

    /**
     * 设置持有非底层资产产生的间接负债
     *
     * @param cyfdczcjjfz 持有非底层资产产生的间接负债
     */
    public void setCyfdczcjjfz(BigDecimal cyfdczcjjfz) {
        this.cyfdczcjjfz = cyfdczcjjfz;
    }

    /**
     * 获取最终投向类型_货币市场工具及货币市场公募基金
     *
     * @return ZZTXLX_HBSCGJJHBSCGMJJ - 最终投向类型_货币市场工具及货币市场公募基金
     */
    public BigDecimal getZztxlxHbscgjjhbscgmjj() {
        return zztxlxHbscgjjhbscgmjj;
    }

    /**
     * 设置最终投向类型_货币市场工具及货币市场公募基金
     *
     * @param zztxlxHbscgjjhbscgmjj 最终投向类型_货币市场工具及货币市场公募基金
     */
    public void setZztxlxHbscgjjhbscgmjj(BigDecimal zztxlxHbscgjjhbscgmjj) {
        this.zztxlxHbscgjjhbscgmjj = zztxlxHbscgjjhbscgmjj;
    }

    /**
     * 获取最终投向类型_债券及债券公募基金
     *
     * @return ZZTXLX_ZQJZQGMJJ - 最终投向类型_债券及债券公募基金
     */
    public BigDecimal getZztxlxZqjzqgmjj() {
        return zztxlxZqjzqgmjj;
    }

    /**
     * 设置最终投向类型_债券及债券公募基金
     *
     * @param zztxlxZqjzqgmjj 最终投向类型_债券及债券公募基金
     */
    public void setZztxlxZqjzqgmjj(BigDecimal zztxlxZqjzqgmjj) {
        this.zztxlxZqjzqgmjj = zztxlxZqjzqgmjj;
    }

    /**
     * 获取最终投向类型_存款
     *
     * @return ZZTXLX_CK - 最终投向类型_存款
     */
    public BigDecimal getZztxlxCk() {
        return zztxlxCk;
    }

    /**
     * 设置最终投向类型_存款
     *
     * @param zztxlxCk 最终投向类型_存款
     */
    public void setZztxlxCk(BigDecimal zztxlxCk) {
        this.zztxlxCk = zztxlxCk;
    }

    /**
     * 获取最终投向类型_信贷类投资
     *
     * @return ZZTXLX_XDLTZ - 最终投向类型_信贷类投资
     */
    public BigDecimal getZztxlxXdltz() {
        return zztxlxXdltz;
    }

    /**
     * 设置最终投向类型_信贷类投资
     *
     * @param zztxlxXdltz 最终投向类型_信贷类投资
     */
    public void setZztxlxXdltz(BigDecimal zztxlxXdltz) {
        this.zztxlxXdltz = zztxlxXdltz;
    }

    /**
     * 获取最终投向类型_权益类投资及股票公募基金
     *
     * @return ZZTXLX_QYLTZJGPGMJJ - 最终投向类型_权益类投资及股票公募基金
     */
    public BigDecimal getZztxlxQyltzjgpgmjj() {
        return zztxlxQyltzjgpgmjj;
    }

    /**
     * 设置最终投向类型_权益类投资及股票公募基金
     *
     * @param zztxlxQyltzjgpgmjj 最终投向类型_权益类投资及股票公募基金
     */
    public void setZztxlxQyltzjgpgmjj(BigDecimal zztxlxQyltzjgpgmjj) {
        this.zztxlxQyltzjgpgmjj = zztxlxQyltzjgpgmjj;
    }

    /**
     * 获取最终投向类型_其他
     *
     * @return ZZTXLX_QT - 最终投向类型_其他
     */
    public BigDecimal getZztxlxQt() {
        return zztxlxQt;
    }

    /**
     * 设置最终投向类型_其他
     *
     * @param zztxlxQt 最终投向类型_其他
     */
    public void setZztxlxQt(BigDecimal zztxlxQt) {
        this.zztxlxQt = zztxlxQt;
    }

    /**
     * 获取由于估值方法不同而产生的差异
     *
     * @return YYGZFFBTRCSCY - 由于估值方法不同而产生的差异
     */
    public BigDecimal getYygzffbtrcscy() {
        return yygzffbtrcscy;
    }

    /**
     * 设置由于估值方法不同而产生的差异
     *
     * @param yygzffbtrcscy 由于估值方法不同而产生的差异
     */
    public void setYygzffbtrcscy(BigDecimal yygzffbtrcscy) {
        this.yygzffbtrcscy = yygzffbtrcscy;
    }

    /**
     * 获取（最终投向为信贷类、权益类或其他的）行业归属_农林牧渔业
     *
     * @return HYGS_NLMYY - （最终投向为信贷类、权益类或其他的）行业归属_农林牧渔业
     */
    public BigDecimal getHygsNlmyy() {
        return hygsNlmyy;
    }

    /**
     * 设置（最终投向为信贷类、权益类或其他的）行业归属_农林牧渔业
     *
     * @param hygsNlmyy （最终投向为信贷类、权益类或其他的）行业归属_农林牧渔业
     */
    public void setHygsNlmyy(BigDecimal hygsNlmyy) {
        this.hygsNlmyy = hygsNlmyy;
    }

    /**
     * 获取（最终投向为信贷类、权益类或其他的）行业归属_制造业
     *
     * @return HYGS_ZZY - （最终投向为信贷类、权益类或其他的）行业归属_制造业
     */
    public BigDecimal getHygsZzy() {
        return hygsZzy;
    }

    /**
     * 设置（最终投向为信贷类、权益类或其他的）行业归属_制造业
     *
     * @param hygsZzy （最终投向为信贷类、权益类或其他的）行业归属_制造业
     */
    public void setHygsZzy(BigDecimal hygsZzy) {
        this.hygsZzy = hygsZzy;
    }

    /**
     * 获取（最终投向为信贷类、权益类或其他的）行业归属_采矿业
     *
     * @return HYGS_CKY - （最终投向为信贷类、权益类或其他的）行业归属_采矿业
     */
    public BigDecimal getHygsCky() {
        return hygsCky;
    }

    /**
     * 设置（最终投向为信贷类、权益类或其他的）行业归属_采矿业
     *
     * @param hygsCky （最终投向为信贷类、权益类或其他的）行业归属_采矿业
     */
    public void setHygsCky(BigDecimal hygsCky) {
        this.hygsCky = hygsCky;
    }

    /**
     * 获取（最终投向为信贷类、权益类或其他的）行业归属_建筑业
     *
     * @return HYGS_JZY - （最终投向为信贷类、权益类或其他的）行业归属_建筑业
     */
    public BigDecimal getHygsJzy() {
        return hygsJzy;
    }

    /**
     * 设置（最终投向为信贷类、权益类或其他的）行业归属_建筑业
     *
     * @param hygsJzy （最终投向为信贷类、权益类或其他的）行业归属_建筑业
     */
    public void setHygsJzy(BigDecimal hygsJzy) {
        this.hygsJzy = hygsJzy;
    }

    /**
     * 获取（最终投向为信贷类、权益类或其他的）行业归属_基础设施建设
     *
     * @return HYGS_JCJSSS - （最终投向为信贷类、权益类或其他的）行业归属_基础设施建设
     */
    public BigDecimal getHygsJcjsss() {
        return hygsJcjsss;
    }

    /**
     * 设置（最终投向为信贷类、权益类或其他的）行业归属_基础设施建设
     *
     * @param hygsJcjsss （最终投向为信贷类、权益类或其他的）行业归属_基础设施建设
     */
    public void setHygsJcjsss(BigDecimal hygsJcjsss) {
        this.hygsJcjsss = hygsJcjsss;
    }

    /**
     * 获取（最终投向为信贷类、权益类或其他的）行业归属_房地产业
     *
     * @return HYGS_FDCY - （最终投向为信贷类、权益类或其他的）行业归属_房地产业
     */
    public BigDecimal getHygsFdcy() {
        return hygsFdcy;
    }

    /**
     * 设置（最终投向为信贷类、权益类或其他的）行业归属_房地产业
     *
     * @param hygsFdcy （最终投向为信贷类、权益类或其他的）行业归属_房地产业
     */
    public void setHygsFdcy(BigDecimal hygsFdcy) {
        this.hygsFdcy = hygsFdcy;
    }

    /**
     * 获取（最终投向为信贷类、权益类或其他的）行业归属_金融业
     *
     * @return HYGS_JRY - （最终投向为信贷类、权益类或其他的）行业归属_金融业
     */
    public BigDecimal getHygsJry() {
        return hygsJry;
    }

    /**
     * 设置（最终投向为信贷类、权益类或其他的）行业归属_金融业
     *
     * @param hygsJry （最终投向为信贷类、权益类或其他的）行业归属_金融业
     */
    public void setHygsJry(BigDecimal hygsJry) {
        this.hygsJry = hygsJry;
    }

    /**
     * 获取（最终投向为信贷类、权益类或其他的）行业归属_其他服务业
     *
     * @return HYGS_QTFWY - （最终投向为信贷类、权益类或其他的）行业归属_其他服务业
     */
    public BigDecimal getHygsQtfwy() {
        return hygsQtfwy;
    }

    /**
     * 设置（最终投向为信贷类、权益类或其他的）行业归属_其他服务业
     *
     * @param hygsQtfwy （最终投向为信贷类、权益类或其他的）行业归属_其他服务业
     */
    public void setHygsQtfwy(BigDecimal hygsQtfwy) {
        this.hygsQtfwy = hygsQtfwy;
    }

    /**
     * 获取（最终投向为信贷类、权益类或其他的）行业归属_其他（含资金池业务）
     *
     * @return HYGS_QTHZJCYW - （最终投向为信贷类、权益类或其他的）行业归属_其他（含资金池业务）
     */
    public BigDecimal getHygsQthzjcyw() {
        return hygsQthzjcyw;
    }

    /**
     * 设置（最终投向为信贷类、权益类或其他的）行业归属_其他（含资金池业务）
     *
     * @param hygsQthzjcyw （最终投向为信贷类、权益类或其他的）行业归属_其他（含资金池业务）
     */
    public void setHygsQthzjcyw(BigDecimal hygsQthzjcyw) {
        this.hygsQthzjcyw = hygsQthzjcyw;
    }

    /**
     * @return PARENT_PRODUCT
     */
    public String getParentProduct() {
        return parentProduct;
    }

    /**
     * @param parentProduct
     */
    public void setParentProduct(String parentProduct) {
        this.parentProduct = parentProduct;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }
}