package com.singlee.hrbnewport.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

/**
 * 贷款拆放含拆放银行同业及联行资产表
 */
@Table(name = "IFS_REPORT_DKCFHCFYHTYJLHZCB")
public class IfsReportDkcfhcfyhtyjlhzcb implements Serializable {
    /**
     * 数据自编码
     */
    @Column(name = "SJZBM")
    private String sjzbm;

    /**
     * 填报机构代码
     */
    @Column(name = "TBJGDM")
    private String tbjgdm;

    /**
     * 报告期
     */
    @Column(name = "BGQ")
    private String bgq;

    /**
     * 是否委托贷款
     */
    @Column(name = "SFWTDK")
    private String sfwtdk;

    /**
     * 委托所属部门
     */
    @Column(name = "WTSSBM")
    private String wtssbm;

    /**
     * 对方国家/地区
     */
    @Column(name = "DFGJDQ")
    private String dfgjdq;

    /**
     * 对方部门
     */
    @Column(name = "DFBM")
    private String dfbm;

    /**
     * 对方与本机构/委托人关系
     */
    @Column(name = "DFYBFGX")
    private String dfybfgx;

    /**
     * 原始期限
     */
    @Column(name = "YSQX")
    private String ysqx;

    /**
     * 原始币种
     */
    @Column(name = "YSBZ")
    private String ysbz;

    /**
     * 上月末本金余额
     */
    @Column(name = "SYMBJYE")
    private BigDecimal symbjye;

    /**
     * 上月末应收利息余额
     */
    @Column(name = "SYMYSLXYE")
    private BigDecimal symyslxye;

    /**
     * 本月末本金余额
     */
    @Column(name = "BYMBJYE")
    private BigDecimal bymbjye;

    /**
     * 本月末本金余额:其中剩余期限在一年及以下
     */
    @Column(name = "BYMBJYEQXYNYX")
    private BigDecimal bymbjyeqxynyx;

    /**
     * 本月末应收利息余额
     */
    @Column(name = "BYMYSLXYE")
    private BigDecimal bymyslxye;

    /**
     * 本月非交易变动
     */
    @Column(name = "BYFJYBD")
    private BigDecimal byfjybd;

    /**
     * 本月净发生额
     */
    @Column(name = "BYJFSE")
    private BigDecimal byjfse;

    /**
     * 本月利息收入
     */
    @Column(name = "BYLXSR")
    private BigDecimal bylxsr;

    /**
     * 备注
     */
    @Column(name = "BZ")
    private String bz;

    private static final long serialVersionUID = 1L;

    /**
     * 获取数据自编码
     *
     * @return SJZBM - 数据自编码
     */
    public String getSjzbm() {
        return sjzbm;
    }

    /**
     * 设置数据自编码
     *
     * @param sjzbm 数据自编码
     */
    public void setSjzbm(String sjzbm) {
        this.sjzbm = sjzbm;
    }

    /**
     * 获取填报机构代码
     *
     * @return TBJGDM - 填报机构代码
     */
    public String getTbjgdm() {
        return tbjgdm;
    }

    /**
     * 设置填报机构代码
     *
     * @param tbjgdm 填报机构代码
     */
    public void setTbjgdm(String tbjgdm) {
        this.tbjgdm = tbjgdm;
    }

    /**
     * 获取报告期
     *
     * @return BGQ - 报告期
     */
    public String getBgq() {
        return bgq;
    }

    /**
     * 设置报告期
     *
     * @param bgq 报告期
     */
    public void setBgq(String bgq) {
        this.bgq = bgq;
    }

    /**
     * 获取是否委托贷款
     *
     * @return SFWTDK - 是否委托贷款
     */
    public String getSfwtdk() {
        return sfwtdk;
    }

    /**
     * 设置是否委托贷款
     *
     * @param sfwtdk 是否委托贷款
     */
    public void setSfwtdk(String sfwtdk) {
        this.sfwtdk = sfwtdk;
    }

    /**
     * 获取委托所属部门
     *
     * @return WTSSBM - 委托所属部门
     */
    public String getWtssbm() {
        return wtssbm;
    }

    /**
     * 设置委托所属部门
     *
     * @param wtssbm 委托所属部门
     */
    public void setWtssbm(String wtssbm) {
        this.wtssbm = wtssbm;
    }

    /**
     * 获取对方国家/地区
     *
     * @return DFGJDQ - 对方国家/地区
     */
    public String getDfgjdq() {
        return dfgjdq;
    }

    /**
     * 设置对方国家/地区
     *
     * @param dfgjdq 对方国家/地区
     */
    public void setDfgjdq(String dfgjdq) {
        this.dfgjdq = dfgjdq;
    }

    /**
     * 获取对方部门
     *
     * @return DFBM - 对方部门
     */
    public String getDfbm() {
        return dfbm;
    }

    /**
     * 设置对方部门
     *
     * @param dfbm 对方部门
     */
    public void setDfbm(String dfbm) {
        this.dfbm = dfbm;
    }

    /**
     * 获取对方与本机构/委托人关系
     *
     * @return DFYBFGX - 对方与本机构/委托人关系
     */
    public String getDfybfgx() {
        return dfybfgx;
    }

    /**
     * 设置对方与本机构/委托人关系
     *
     * @param dfybfgx 对方与本机构/委托人关系
     */
    public void setDfybfgx(String dfybfgx) {
        this.dfybfgx = dfybfgx;
    }

    /**
     * 获取原始期限
     *
     * @return YSQX - 原始期限
     */
    public String getYsqx() {
        return ysqx;
    }

    /**
     * 设置原始期限
     *
     * @param ysqx 原始期限
     */
    public void setYsqx(String ysqx) {
        this.ysqx = ysqx;
    }

    /**
     * 获取原始币种
     *
     * @return YSBZ - 原始币种
     */
    public String getYsbz() {
        return ysbz;
    }

    /**
     * 设置原始币种
     *
     * @param ysbz 原始币种
     */
    public void setYsbz(String ysbz) {
        this.ysbz = ysbz;
    }

    /**
     * 获取上月末本金余额
     *
     * @return SYMBJYE - 上月末本金余额
     */
    public BigDecimal getSymbjye() {
        return symbjye;
    }

    /**
     * 设置上月末本金余额
     *
     * @param symbjye 上月末本金余额
     */
    public void setSymbjye(BigDecimal symbjye) {
        this.symbjye = symbjye;
    }

    /**
     * 获取上月末应收利息余额
     *
     * @return SYMYSLXYE - 上月末应收利息余额
     */
    public BigDecimal getSymyslxye() {
        return symyslxye;
    }

    /**
     * 设置上月末应收利息余额
     *
     * @param symyslxye 上月末应收利息余额
     */
    public void setSymyslxye(BigDecimal symyslxye) {
        this.symyslxye = symyslxye;
    }

    /**
     * 获取本月末本金余额
     *
     * @return BYMBJYE - 本月末本金余额
     */
    public BigDecimal getBymbjye() {
        return bymbjye;
    }

    /**
     * 设置本月末本金余额
     *
     * @param bymbjye 本月末本金余额
     */
    public void setBymbjye(BigDecimal bymbjye) {
        this.bymbjye = bymbjye;
    }

    /**
     * 获取本月末本金余额:其中剩余期限在一年及以下
     *
     * @return BYMBJYEQXYNYX - 本月末本金余额:其中剩余期限在一年及以下
     */
    public BigDecimal getBymbjyeqxynyx() {
        return bymbjyeqxynyx;
    }

    /**
     * 设置本月末本金余额:其中剩余期限在一年及以下
     *
     * @param bymbjyeqxynyx 本月末本金余额:其中剩余期限在一年及以下
     */
    public void setBymbjyeqxynyx(BigDecimal bymbjyeqxynyx) {
        this.bymbjyeqxynyx = bymbjyeqxynyx;
    }

    /**
     * 获取本月末应收利息余额
     *
     * @return BYMYSLXYE - 本月末应收利息余额
     */
    public BigDecimal getBymyslxye() {
        return bymyslxye;
    }

    /**
     * 设置本月末应收利息余额
     *
     * @param bymyslxye 本月末应收利息余额
     */
    public void setBymyslxye(BigDecimal bymyslxye) {
        this.bymyslxye = bymyslxye;
    }

    /**
     * 获取本月非交易变动
     *
     * @return BYFJYBD - 本月非交易变动
     */
    public BigDecimal getByfjybd() {
        return byfjybd;
    }

    /**
     * 设置本月非交易变动
     *
     * @param byfjybd 本月非交易变动
     */
    public void setByfjybd(BigDecimal byfjybd) {
        this.byfjybd = byfjybd;
    }

    /**
     * 获取本月净发生额
     *
     * @return BYJFSE - 本月净发生额
     */
    public BigDecimal getByjfse() {
        return byjfse;
    }

    /**
     * 设置本月净发生额
     *
     * @param byjfse 本月净发生额
     */
    public void setByjfse(BigDecimal byjfse) {
        this.byjfse = byjfse;
    }

    /**
     * 获取本月利息收入
     *
     * @return BYLXSR - 本月利息收入
     */
    public BigDecimal getBylxsr() {
        return bylxsr;
    }

    /**
     * 设置本月利息收入
     *
     * @param bylxsr 本月利息收入
     */
    public void setBylxsr(BigDecimal bylxsr) {
        this.bylxsr = bylxsr;
    }

    /**
     * 获取备注
     *
     * @return BZ - 备注
     */
    public String getBz() {
        return bz;
    }

    /**
     * 设置备注
     *
     * @param bz 备注
     */
    public void setBz(String bz) {
        this.bz = bz;
    }
}