package com.singlee.hrbnewport.model;

import javax.persistence.Id;
import java.io.Serializable;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 20:51
 * @description：${description}
 * @modified By：
 * @version:   
 */
/**
    * 2102人民币购售业务信息
    */
public class IfsReportRmbgsyw implements Serializable {
    /**
    * 申报号码
    */
    private String sbhm;

    /**
    * 操作类型
    */
    private String czlx;

    /**
    * 变更/撤销原因
    */
    private String bgyy;

    /**
    * 申报日期
    */
    private String sbrq;

    /**
    * 境内机构代码
    */
    private String jnjgdm;

    /**
    * 境内机构名称
    */
    private String jnjgmc;

    /**
    * 境内机构类型
    */
    private String jnjglx;

    /**
    * 境外主体类型
    */
    private String jnztlx;

    /**
    * 境外主体代码
    */
    private String jwztdm;

    /**
    * 境外主体名称
    */
    private String jwztmc;

    /**
    * 人民币账
    */
    private String rmbzhzh;

    /**
    * 人民币跨境购售类型
    */
    private String rmbkjgslx;

    /**
    * 购售用途
    */
    private String gsyt;

    /**
    * 业务种类
    */
    private String ywlx;

    /**
    * 银行业务编号
    */
    @Id
    private String yhywbh;

    /**
    * 交易附言
    */
    private String jyfy;

    /**
    * 明细操作类型
    */
    private String jqmxczlx;

    /**
    * 序号
    */
    private String jqxh;

    /**
    * 买入币种
    */
    private String jqbz;

    /**
    * 买入金额
    */
    private String jqje;

    /**
    * 价格
    */
    private String jqjg;

    /**
    * 卖出币种
    */
    private String jqmcbz;

    /**
    * 卖出金额
    */
    private String jqmcje;

    /**
    * 交易日
    */
    private String jyr;

    /**
    * 结算日
    */
    private String jsr;

    /**
    * 明细操作类型
    */
    private String yqmxczlx;

    /**
    * 序号
    */
    private String yqxh;

    /**
    * 买入币种
    */
    private String yqmrbz;

    /**
    * 买入金额
    */
    private String yqmrje;

    /**
    * 价格
    */
    private String yqjg;

    /**
    * 卖出币种
    */
    private String yqmcbz;

    /**
    * 卖出金额
    */
    private String yqmcje;

    /**
    * 交易日
    */
    private String yqjyr;

    /**
    * 到期日
    */
    private String yqdqr;

    /**
    * 结算日
    */
    private String yqjsr;

    /**
    * 明细操作类型
    */
    private String dqczmx;

    /**
    * 序号
    */
    private String dqxh;

    /**
    * 交易日
    */
    private String dqjyr;

    /**
    * 远端买入(名义本金)币种
    */
    private String dqydmrbz;

    /**
    * 远端买入(名义本金)金额
    */
    private String dqydmrje;

    /**
    * 远端卖出(名义本金)币种
    */
    private String dqydmcbz;

    /**
    * 远端卖出(名义本金)金额
    */
    private String dqmcje;

    /**
    * 近端交割日
    */
    private String dqjdjgr;

    /**
    * 近端交易价格
    */
    private String dqjdjyjg;

    /**
    * 远端交割日
    */
    private String dqydjgr;

    /**
    * 远端交易价格
    */
    private String dqydjyjg;

    /**
    * 交易类型
    */
    private String jxlx;

    //账务日期
    private String postdate;

    private static final long serialVersionUID = 1L;

    public String getSbhm() {
        return sbhm;
    }

    public void setSbhm(String sbhm) {
        this.sbhm = sbhm;
    }

    public String getCzlx() {
        return czlx;
    }

    public void setCzlx(String czlx) {
        this.czlx = czlx;
    }

    public String getBgyy() {
        return bgyy;
    }

    public void setBgyy(String bgyy) {
        this.bgyy = bgyy;
    }

    public String getSbrq() {
        return sbrq;
    }

    public void setSbrq(String sbrq) {
        this.sbrq = sbrq;
    }

    public String getJnjgdm() {
        return jnjgdm;
    }

    public void setJnjgdm(String jnjgdm) {
        this.jnjgdm = jnjgdm;
    }

    public String getJnjgmc() {
        return jnjgmc;
    }

    public void setJnjgmc(String jnjgmc) {
        this.jnjgmc = jnjgmc;
    }

    public String getJnjglx() {
        return jnjglx;
    }

    public void setJnjglx(String jnjglx) {
        this.jnjglx = jnjglx;
    }

    public String getJnztlx() {
        return jnztlx;
    }

    public void setJnztlx(String jnztlx) {
        this.jnztlx = jnztlx;
    }

    public String getJwztdm() {
        return jwztdm;
    }

    public void setJwztdm(String jwztdm) {
        this.jwztdm = jwztdm;
    }

    public String getJwztmc() {
        return jwztmc;
    }

    public void setJwztmc(String jwztmc) {
        this.jwztmc = jwztmc;
    }

    public String getRmbzhzh() {
        return rmbzhzh;
    }

    public void setRmbzhzh(String rmbzhzh) {
        this.rmbzhzh = rmbzhzh;
    }

    public String getRmbkjgslx() {
        return rmbkjgslx;
    }

    public void setRmbkjgslx(String rmbkjgslx) {
        this.rmbkjgslx = rmbkjgslx;
    }

    public String getGsyt() {
        return gsyt;
    }

    public void setGsyt(String gsyt) {
        this.gsyt = gsyt;
    }

    public String getYwlx() {
        return ywlx;
    }

    public void setYwlx(String ywlx) {
        this.ywlx = ywlx;
    }

    public String getYhywbh() {
        return yhywbh;
    }

    public void setYhywbh(String yhywbh) {
        this.yhywbh = yhywbh;
    }

    public String getJyfy() {
        return jyfy;
    }

    public void setJyfy(String jyfy) {
        this.jyfy = jyfy;
    }

    public String getJqmxczlx() {
        return jqmxczlx;
    }

    public void setJqmxczlx(String jqmxczlx) {
        this.jqmxczlx = jqmxczlx;
    }

    public String getJqxh() {
        return jqxh;
    }

    public void setJqxh(String jqxh) {
        this.jqxh = jqxh;
    }

    public String getJqbz() {
        return jqbz;
    }

    public void setJqbz(String jqbz) {
        this.jqbz = jqbz;
    }

    public String getJqje() {
        return jqje;
    }

    public void setJqje(String jqje) {
        this.jqje = jqje;
    }

    public String getJqjg() {
        return jqjg;
    }

    public void setJqjg(String jqjg) {
        this.jqjg = jqjg;
    }

    public String getJqmcbz() {
        return jqmcbz;
    }

    public void setJqmcbz(String jqmcbz) {
        this.jqmcbz = jqmcbz;
    }

    public String getJqmcje() {
        return jqmcje;
    }

    public void setJqmcje(String jqmcje) {
        this.jqmcje = jqmcje;
    }

    public String getJyr() {
        return jyr;
    }

    public void setJyr(String jyr) {
        this.jyr = jyr;
    }

    public String getJsr() {
        return jsr;
    }

    public void setJsr(String jsr) {
        this.jsr = jsr;
    }

    public String getYqmxczlx() {
        return yqmxczlx;
    }

    public void setYqmxczlx(String yqmxczlx) {
        this.yqmxczlx = yqmxczlx;
    }

    public String getYqxh() {
        return yqxh;
    }

    public void setYqxh(String yqxh) {
        this.yqxh = yqxh;
    }

    public String getYqmrbz() {
        return yqmrbz;
    }

    public void setYqmrbz(String yqmrbz) {
        this.yqmrbz = yqmrbz;
    }

    public String getYqmrje() {
        return yqmrje;
    }

    public void setYqmrje(String yqmrje) {
        this.yqmrje = yqmrje;
    }

    public String getYqjg() {
        return yqjg;
    }

    public void setYqjg(String yqjg) {
        this.yqjg = yqjg;
    }

    public String getYqmcbz() {
        return yqmcbz;
    }

    public void setYqmcbz(String yqmcbz) {
        this.yqmcbz = yqmcbz;
    }

    public String getYqmcje() {
        return yqmcje;
    }

    public void setYqmcje(String yqmcje) {
        this.yqmcje = yqmcje;
    }

    public String getYqjyr() {
        return yqjyr;
    }

    public void setYqjyr(String yqjyr) {
        this.yqjyr = yqjyr;
    }

    public String getYqdqr() {
        return yqdqr;
    }

    public void setYqdqr(String yqdqr) {
        this.yqdqr = yqdqr;
    }

    public String getYqjsr() {
        return yqjsr;
    }

    public void setYqjsr(String yqjsr) {
        this.yqjsr = yqjsr;
    }

    public String getDqczmx() {
        return dqczmx;
    }

    public void setDqczmx(String dqczmx) {
        this.dqczmx = dqczmx;
    }

    public String getDqxh() {
        return dqxh;
    }

    public void setDqxh(String dqxh) {
        this.dqxh = dqxh;
    }

    public String getDqjyr() {
        return dqjyr;
    }

    public void setDqjyr(String dqjyr) {
        this.dqjyr = dqjyr;
    }

    public String getDqydmrbz() {
        return dqydmrbz;
    }

    public void setDqydmrbz(String dqydmrbz) {
        this.dqydmrbz = dqydmrbz;
    }

    public String getDqydmrje() {
        return dqydmrje;
    }

    public void setDqydmrje(String dqydmrje) {
        this.dqydmrje = dqydmrje;
    }

    public String getDqydmcbz() {
        return dqydmcbz;
    }

    public void setDqydmcbz(String dqydmcbz) {
        this.dqydmcbz = dqydmcbz;
    }

    public String getDqmcje() {
        return dqmcje;
    }

    public void setDqmcje(String dqmcje) {
        this.dqmcje = dqmcje;
    }

    public String getDqjdjgr() {
        return dqjdjgr;
    }

    public void setDqjdjgr(String dqjdjgr) {
        this.dqjdjgr = dqjdjgr;
    }

    public String getDqjdjyjg() {
        return dqjdjyjg;
    }

    public void setDqjdjyjg(String dqjdjyjg) {
        this.dqjdjyjg = dqjdjyjg;
    }

    public String getDqydjgr() {
        return dqydjgr;
    }

    public void setDqydjgr(String dqydjgr) {
        this.dqydjgr = dqydjgr;
    }

    public String getDqydjyjg() {
        return dqydjyjg;
    }

    public void setDqydjyjg(String dqydjyjg) {
        this.dqydjyjg = dqydjyjg;
    }

    public String getJxlx() {
        return jxlx;
    }

    public void setJxlx(String jxlx) {
        this.jxlx = jxlx;
    }

    public String getPostdate() {
        return postdate;
    }

    public void setPostdate(String postdate) {
        this.postdate = postdate;
    }

    @Override
    public String toString() {
        return "IfsReportRmbgsyw{" +
                "sbhm='" + sbhm + '\'' +
                ", czlx='" + czlx + '\'' +
                ", bgyy='" + bgyy + '\'' +
                ", sbrq='" + sbrq + '\'' +
                ", jnjgdm='" + jnjgdm + '\'' +
                ", jnjgmc='" + jnjgmc + '\'' +
                ", jnjglx='" + jnjglx + '\'' +
                ", jnztlx='" + jnztlx + '\'' +
                ", jwztdm='" + jwztdm + '\'' +
                ", jwztmc='" + jwztmc + '\'' +
                ", rmbzhzh='" + rmbzhzh + '\'' +
                ", rmbkjgslx='" + rmbkjgslx + '\'' +
                ", gsyt='" + gsyt + '\'' +
                ", ywlx='" + ywlx + '\'' +
                ", yhywbh='" + yhywbh + '\'' +
                ", jyfy='" + jyfy + '\'' +
                ", jqmxczlx='" + jqmxczlx + '\'' +
                ", jqxh='" + jqxh + '\'' +
                ", jqbz='" + jqbz + '\'' +
                ", jqje='" + jqje + '\'' +
                ", jqjg='" + jqjg + '\'' +
                ", jqmcbz='" + jqmcbz + '\'' +
                ", jqmcje='" + jqmcje + '\'' +
                ", jyr='" + jyr + '\'' +
                ", jsr='" + jsr + '\'' +
                ", yqmxczlx='" + yqmxczlx + '\'' +
                ", yqxh='" + yqxh + '\'' +
                ", yqmrbz='" + yqmrbz + '\'' +
                ", yqmrje='" + yqmrje + '\'' +
                ", yqjg='" + yqjg + '\'' +
                ", yqmcbz='" + yqmcbz + '\'' +
                ", yqmcje='" + yqmcje + '\'' +
                ", yqjyr='" + yqjyr + '\'' +
                ", yqdqr='" + yqdqr + '\'' +
                ", yqjsr='" + yqjsr + '\'' +
                ", dqczmx='" + dqczmx + '\'' +
                ", dqxh='" + dqxh + '\'' +
                ", dqjyr='" + dqjyr + '\'' +
                ", dqydmrbz='" + dqydmrbz + '\'' +
                ", dqydmrje='" + dqydmrje + '\'' +
                ", dqydmcbz='" + dqydmcbz + '\'' +
                ", dqmcje='" + dqmcje + '\'' +
                ", dqjdjgr='" + dqjdjgr + '\'' +
                ", dqjdjyjg='" + dqjdjyjg + '\'' +
                ", dqydjgr='" + dqydjgr + '\'' +
                ", dqydjyjg='" + dqydjyjg + '\'' +
                ", jxlx='" + jxlx + '\'' +
                ", postdate='" + postdate + '\'' +
                '}';
    }
}