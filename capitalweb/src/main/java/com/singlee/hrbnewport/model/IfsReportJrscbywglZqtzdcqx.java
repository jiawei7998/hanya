package com.singlee.hrbnewport.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

/**
 * 债券投资待偿期限结构表
 */
@Table(name = "IFS_REPORT_JRSCBYWGL_ZQTZDCQX")
public class IfsReportJrscbywglZqtzdcqx implements Serializable {
    /**
     * 债券类型
     */
    @Column(name = "ZQTYPE")
    private String zqtype;

    /**
     * ≤1年
     */
    @Column(name = "XYYNH")
    private BigDecimal xyynh;

    /**
     * 1至3年（含）
     */
    @Column(name = "YNZSNH")
    private BigDecimal ynzsnh;

    /**
     * 3-5年（含）
     */
    @Column(name = "SNZWNH")
    private BigDecimal snzwnh;

    /**
     * ≥5年
     */
    @Column(name = "DYWNH")
    private BigDecimal dywnh;

    /**
     * 合计
     */
    @Column(name = "HJ")
    private BigDecimal hj;

    private static final long serialVersionUID = 1L;

    /**
     * 获取债券类型
     *
     * @return ZQTYPE - 债券类型
     */
    public String getZqtype() {
        return zqtype;
    }

    /**
     * 设置债券类型
     *
     * @param zqtype 债券类型
     */
    public void setZqtype(String zqtype) {
        this.zqtype = zqtype;
    }

    /**
     * 获取≤1年
     *
     * @return XYYNH - ≤1年
     */
    public BigDecimal getXyynh() {
        return xyynh;
    }

    /**
     * 设置≤1年
     *
     * @param xyynh ≤1年
     */
    public void setXyynh(BigDecimal xyynh) {
        this.xyynh = xyynh;
    }

    /**
     * 获取1至3年（含）
     *
     * @return YNZSNH - 1至3年（含）
     */
    public BigDecimal getYnzsnh() {
        return ynzsnh;
    }

    /**
     * 设置1至3年（含）
     *
     * @param ynzsnh 1至3年（含）
     */
    public void setYnzsnh(BigDecimal ynzsnh) {
        this.ynzsnh = ynzsnh;
    }

    /**
     * 获取3-5年（含）
     *
     * @return SNZWNH - 3-5年（含）
     */
    public BigDecimal getSnzwnh() {
        return snzwnh;
    }

    /**
     * 设置3-5年（含）
     *
     * @param snzwnh 3-5年（含）
     */
    public void setSnzwnh(BigDecimal snzwnh) {
        this.snzwnh = snzwnh;
    }

    /**
     * 获取≥5年
     *
     * @return DYWNH - ≥5年
     */
    public BigDecimal getDywnh() {
        return dywnh;
    }

    /**
     * 设置≥5年
     *
     * @param dywnh ≥5年
     */
    public void setDywnh(BigDecimal dywnh) {
        this.dywnh = dywnh;
    }

    /**
     * 获取合计
     *
     * @return HJ - 合计
     */
    public BigDecimal getHj() {
        return hj;
    }

    /**
     * 设置合计
     *
     * @param hj 合计
     */
    public void setHj(BigDecimal hj) {
        this.hj = hj;
    }
}