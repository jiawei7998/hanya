package com.singlee.hrbnewport.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

/**
 * 债券投资收益结构表（按会计账户）
 */
@Table(name = "IFS_REPORT_JRSCBYWGL_ZQTZSYAZH")
public class IfsReportJrscbywglZqtzsyazh implements Serializable {
    /**
     * 账户类型
     */
    @Column(name = "ZHTYPE")
    private String zhtype;

    /**
     * 国债_余额
     */
    @Column(name = "GZ_YE")
    private BigDecimal gzYe;

    /**
     * 国债_久期
     */
    @Column(name = "GZ_JQ")
    private BigDecimal gzJq;

    /**
     * 国债_收益率
     */
    @Column(name = "GZ_SYL")
    private BigDecimal gzSyl;

    /**
     * 地方政府债_余额
     */
    @Column(name = "DFZFZ_YE")
    private BigDecimal dfzfzYe;

    /**
     * 地方政府债_久期
     */
    @Column(name = "DFZFZ_JQ")
    private BigDecimal dfzfzJq;

    /**
     * 地方政府债_收益率
     */
    @Column(name = "DFZFZ_SYL")
    private BigDecimal dfzfzSyl;

    /**
     * 政策性金融债_余额
     */
    @Column(name = "ZCXJRZ_YE")
    private BigDecimal zcxjrzYe;

    /**
     * 政策性金融债_久期
     */
    @Column(name = "ZCXJRZ_JQ")
    private BigDecimal zcxjrzJq;

    /**
     * 政策性金融债_收益率
     */
    @Column(name = "ZCXJRZ_SYL")
    private BigDecimal zcxjrzSyl;

    /**
     * 商业银行及非银行金融机构债_余额
     */
    @Column(name = "SYYXJFYHJRJGZ_YE")
    private BigDecimal syyxjfyhjrjgzYe;

    /**
     * 商业银行及非银行金融机构债_久期
     */
    @Column(name = "SYYXJFYHJRJGZ_JQ")
    private BigDecimal syyxjfyhjrjgzJq;

    /**
     * 商业银行及非银行金融机构债_收益率
     */
    @Column(name = "SYYXJFYHJRJGZ_SYL")
    private BigDecimal syyxjfyhjrjgzSyl;

    /**
     * 信用债_余额
     */
    @Column(name = "XYZ_YE")
    private BigDecimal xyzYe;

    /**
     * 信用债_久期
     */
    @Column(name = "XYZ_JQ")
    private BigDecimal xyzJq;

    /**
     * 信用债_收益率
     */
    @Column(name = "XYZ_SYL")
    private BigDecimal xyzSyl;

    /**
     * 同业存单_余额
     */
    @Column(name = "TYCD_YE")
    private BigDecimal tycdYe;

    /**
     * 同业存单_久期
     */
    @Column(name = "TYCD_JQ")
    private BigDecimal tycdJq;

    /**
     * 同业存单_收益率
     */
    @Column(name = "TYCD_SYL")
    private BigDecimal tycdSyl;

    private static final long serialVersionUID = 1L;

    /**
     * 获取账户类型
     *
     * @return ZHTYPE - 账户类型
     */
    public String getZhtype() {
        return zhtype;
    }

    /**
     * 设置账户类型
     *
     * @param zhtype 账户类型
     */
    public void setZhtype(String zhtype) {
        this.zhtype = zhtype;
    }

    /**
     * 获取国债_余额
     *
     * @return GZ_YE - 国债_余额
     */
    public BigDecimal getGzYe() {
        return gzYe;
    }

    /**
     * 设置国债_余额
     *
     * @param gzYe 国债_余额
     */
    public void setGzYe(BigDecimal gzYe) {
        this.gzYe = gzYe;
    }

    /**
     * 获取国债_久期
     *
     * @return GZ_JQ - 国债_久期
     */
    public BigDecimal getGzJq() {
        return gzJq;
    }

    /**
     * 设置国债_久期
     *
     * @param gzJq 国债_久期
     */
    public void setGzJq(BigDecimal gzJq) {
        this.gzJq = gzJq;
    }

    /**
     * 获取国债_收益率
     *
     * @return GZ_SYL - 国债_收益率
     */
    public BigDecimal getGzSyl() {
        return gzSyl;
    }

    /**
     * 设置国债_收益率
     *
     * @param gzSyl 国债_收益率
     */
    public void setGzSyl(BigDecimal gzSyl) {
        this.gzSyl = gzSyl;
    }

    /**
     * 获取地方政府债_余额
     *
     * @return DFZFZ_YE - 地方政府债_余额
     */
    public BigDecimal getDfzfzYe() {
        return dfzfzYe;
    }

    /**
     * 设置地方政府债_余额
     *
     * @param dfzfzYe 地方政府债_余额
     */
    public void setDfzfzYe(BigDecimal dfzfzYe) {
        this.dfzfzYe = dfzfzYe;
    }

    /**
     * 获取地方政府债_久期
     *
     * @return DFZFZ_JQ - 地方政府债_久期
     */
    public BigDecimal getDfzfzJq() {
        return dfzfzJq;
    }

    /**
     * 设置地方政府债_久期
     *
     * @param dfzfzJq 地方政府债_久期
     */
    public void setDfzfzJq(BigDecimal dfzfzJq) {
        this.dfzfzJq = dfzfzJq;
    }

    /**
     * 获取地方政府债_收益率
     *
     * @return DFZFZ_SYL - 地方政府债_收益率
     */
    public BigDecimal getDfzfzSyl() {
        return dfzfzSyl;
    }

    /**
     * 设置地方政府债_收益率
     *
     * @param dfzfzSyl 地方政府债_收益率
     */
    public void setDfzfzSyl(BigDecimal dfzfzSyl) {
        this.dfzfzSyl = dfzfzSyl;
    }

    /**
     * 获取政策性金融债_余额
     *
     * @return ZCXJRZ_YE - 政策性金融债_余额
     */
    public BigDecimal getZcxjrzYe() {
        return zcxjrzYe;
    }

    /**
     * 设置政策性金融债_余额
     *
     * @param zcxjrzYe 政策性金融债_余额
     */
    public void setZcxjrzYe(BigDecimal zcxjrzYe) {
        this.zcxjrzYe = zcxjrzYe;
    }

    /**
     * 获取政策性金融债_久期
     *
     * @return ZCXJRZ_JQ - 政策性金融债_久期
     */
    public BigDecimal getZcxjrzJq() {
        return zcxjrzJq;
    }

    /**
     * 设置政策性金融债_久期
     *
     * @param zcxjrzJq 政策性金融债_久期
     */
    public void setZcxjrzJq(BigDecimal zcxjrzJq) {
        this.zcxjrzJq = zcxjrzJq;
    }

    /**
     * 获取政策性金融债_收益率
     *
     * @return ZCXJRZ_SYL - 政策性金融债_收益率
     */
    public BigDecimal getZcxjrzSyl() {
        return zcxjrzSyl;
    }

    /**
     * 设置政策性金融债_收益率
     *
     * @param zcxjrzSyl 政策性金融债_收益率
     */
    public void setZcxjrzSyl(BigDecimal zcxjrzSyl) {
        this.zcxjrzSyl = zcxjrzSyl;
    }

    /**
     * 获取商业银行及非银行金融机构债_余额
     *
     * @return SYYXJFYHJRJGZ_YE - 商业银行及非银行金融机构债_余额
     */
    public BigDecimal getSyyxjfyhjrjgzYe() {
        return syyxjfyhjrjgzYe;
    }

    /**
     * 设置商业银行及非银行金融机构债_余额
     *
     * @param syyxjfyhjrjgzYe 商业银行及非银行金融机构债_余额
     */
    public void setSyyxjfyhjrjgzYe(BigDecimal syyxjfyhjrjgzYe) {
        this.syyxjfyhjrjgzYe = syyxjfyhjrjgzYe;
    }

    /**
     * 获取商业银行及非银行金融机构债_久期
     *
     * @return SYYXJFYHJRJGZ_JQ - 商业银行及非银行金融机构债_久期
     */
    public BigDecimal getSyyxjfyhjrjgzJq() {
        return syyxjfyhjrjgzJq;
    }

    /**
     * 设置商业银行及非银行金融机构债_久期
     *
     * @param syyxjfyhjrjgzJq 商业银行及非银行金融机构债_久期
     */
    public void setSyyxjfyhjrjgzJq(BigDecimal syyxjfyhjrjgzJq) {
        this.syyxjfyhjrjgzJq = syyxjfyhjrjgzJq;
    }

    /**
     * 获取商业银行及非银行金融机构债_收益率
     *
     * @return SYYXJFYHJRJGZ_SYL - 商业银行及非银行金融机构债_收益率
     */
    public BigDecimal getSyyxjfyhjrjgzSyl() {
        return syyxjfyhjrjgzSyl;
    }

    /**
     * 设置商业银行及非银行金融机构债_收益率
     *
     * @param syyxjfyhjrjgzSyl 商业银行及非银行金融机构债_收益率
     */
    public void setSyyxjfyhjrjgzSyl(BigDecimal syyxjfyhjrjgzSyl) {
        this.syyxjfyhjrjgzSyl = syyxjfyhjrjgzSyl;
    }

    /**
     * 获取信用债_余额
     *
     * @return XYZ_YE - 信用债_余额
     */
    public BigDecimal getXyzYe() {
        return xyzYe;
    }

    /**
     * 设置信用债_余额
     *
     * @param xyzYe 信用债_余额
     */
    public void setXyzYe(BigDecimal xyzYe) {
        this.xyzYe = xyzYe;
    }

    /**
     * 获取信用债_久期
     *
     * @return XYZ_JQ - 信用债_久期
     */
    public BigDecimal getXyzJq() {
        return xyzJq;
    }

    /**
     * 设置信用债_久期
     *
     * @param xyzJq 信用债_久期
     */
    public void setXyzJq(BigDecimal xyzJq) {
        this.xyzJq = xyzJq;
    }

    /**
     * 获取信用债_收益率
     *
     * @return XYZ_SYL - 信用债_收益率
     */
    public BigDecimal getXyzSyl() {
        return xyzSyl;
    }

    /**
     * 设置信用债_收益率
     *
     * @param xyzSyl 信用债_收益率
     */
    public void setXyzSyl(BigDecimal xyzSyl) {
        this.xyzSyl = xyzSyl;
    }

    /**
     * 获取同业存单_余额
     *
     * @return TYCD_YE - 同业存单_余额
     */
    public BigDecimal getTycdYe() {
        return tycdYe;
    }

    /**
     * 设置同业存单_余额
     *
     * @param tycdYe 同业存单_余额
     */
    public void setTycdYe(BigDecimal tycdYe) {
        this.tycdYe = tycdYe;
    }

    /**
     * 获取同业存单_久期
     *
     * @return TYCD_JQ - 同业存单_久期
     */
    public BigDecimal getTycdJq() {
        return tycdJq;
    }

    /**
     * 设置同业存单_久期
     *
     * @param tycdJq 同业存单_久期
     */
    public void setTycdJq(BigDecimal tycdJq) {
        this.tycdJq = tycdJq;
    }

    /**
     * 获取同业存单_收益率
     *
     * @return TYCD_SYL - 同业存单_收益率
     */
    public BigDecimal getTycdSyl() {
        return tycdSyl;
    }

    /**
     * 设置同业存单_收益率
     *
     * @param tycdSyl 同业存单_收益率
     */
    public void setTycdSyl(BigDecimal tycdSyl) {
        this.tycdSyl = tycdSyl;
    }
}