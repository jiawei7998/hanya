package com.singlee.hrbnewport.model;

/**
 * 业务明细_人行_公墓基金
 * 2021/12/3
 * @Auther:zk
 */    
    
/**
    * 业务明细-公募基金
    */
public class DealdetailPubFund {
    /**
    * 基金管理人
    */
    private String fundManager;

    /**
    * 基金简称
    */
    private String fundName;

    /**
    * 基金类型
    */
    private String fundType;

    /**
    * 基金代码
    */
    private String fundCode;

    /**
    * 交易方向
    */
    private String dealDir;

    /**
    * 余额（万元）
    */
    private String amt;

    /**
    * 申购日
    */
    private String purchDate;

    public String getFundManager() {
        return fundManager;
    }

    public void setFundManager(String fundManager) {
        this.fundManager = fundManager;
    }

    public String getFundName() {
        return fundName;
    }

    public void setFundName(String fundName) {
        this.fundName = fundName;
    }

    public String getFundType() {
        return fundType;
    }

    public void setFundType(String fundType) {
        this.fundType = fundType;
    }

    public String getFundCode() {
        return fundCode;
    }

    public void setFundCode(String fundCode) {
        this.fundCode = fundCode;
    }

    public String getDealDir() {
        return dealDir;
    }

    public void setDealDir(String dealDir) {
        this.dealDir = dealDir;
    }

    public String getAmt() {
        return amt;
    }

    public void setAmt(String amt) {
        this.amt = amt;
    }

    public String getPurchDate() {
        return purchDate;
    }

    public void setPurchDate(String purchDate) {
        this.purchDate = purchDate;
    }
}