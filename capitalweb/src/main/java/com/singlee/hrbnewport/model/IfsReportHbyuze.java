package com.singlee.hrbnewport.model;

import java.math.BigDecimal;

/**
    * 合并持仓分析表-给于泽
    */
public class IfsReportHbyuze {
    /**
    * 成本中心
    */
    private String cost;

    /**
    * 投资组合
    */
    private String port;

    /**
    * 久期
    */
    private String jiuqi;

    /**
    * PVBP值
    */
    private String pvbpx;

    /**
    * PV01值
    */
    private String pv01x;

    /**
    * VAR值
    */
    private String varx;

    /**
    * 债券发行总量(万)
    */
    private BigDecimal fxzl;

    /**
    * 债券所属行业
    */
    private String sshy;

    /**
    * 最新主体评级
    */
    private String ztlevel;

    /**
    * 最新债项评级
    */
    private String zxlevel;

    /**
    * 发行主体评级
    */
    private String fxztlevel;

    /**
    * 发行债项评级
    */
    private String fxzxlevel;

    /**
    * 持仓类别
    */
    private BigDecimal acctngtype;

    /**
    * 持仓成本(元)
    */
    private BigDecimal cccb;

    /**
    * 应收利息(元)
    */
    private BigDecimal yslx;

    /**
    * 账面价值(元)
    */
    private BigDecimal zmjz;

    /**
    * 应计利息(元)
    */
    private BigDecimal yjlx;

    /**
    * 计算期利息收入(元)
    */
    private BigDecimal dnljlxsr;

    /**
    * 持有期利息收入(元)
    */
    private BigDecimal syljlxsr;

    /**
    * 公允价值变动(元)
    */
    private BigDecimal tdymtm;

    /**
    * 公允价值变动损益(元)
    */
    private BigDecimal tjpmtm;

    /**
    * 债券买入成本
    */
    private BigDecimal avgcost;

    /**
    * 债券投资成本
    */
    private BigDecimal settavgcost;

    /**
    * 估值
    */
    private BigDecimal gz;

    /**
    * 账面净价市值(元)
    */
    private BigDecimal opicsjjsz;

    /**
    * 净价市值(元)
    */
    private BigDecimal jjsz;

    /**
    * OPICS_溢折价(元）
    */
    private BigDecimal yzj;

    /**
    * 溢折价(元）
    */
    private BigDecimal windyzj;

    /**
    * 利息调整(元)
    */
    private BigDecimal lxtz;

    /**
    * 到期收益率
    */
    private BigDecimal syl;

    /**
    * 百元加权成本
    */
    private BigDecimal jqcbper;

    /**
    * 百元净价成本
    */
    private BigDecimal jjcbper;

    /**
    * 百元应收利息
    */
    private BigDecimal yslxper;

    /**
    * 百元应计利息
    */
    private BigDecimal yjlxper;

    /**
    * 摊余净价(元)
    */
    private BigDecimal tyjj;

    /**
    * OPICS_净价浮赢(元)
    */
    private BigDecimal jjfy;

    /**
    * 净价浮赢(元)
    */
    private BigDecimal windjjfy;

    /**
    * 剩余年限
    */
    private String synx;

    /**
    * 付息剩余天数
    */
    private String fxsyts;

    /**
    * 最近利率重定价日
    */
    private String cdjr;

    /**
    * 年限
    */
    private String nx;

    /**
    * 发行日期
    */
    private String fxrq;

    /**
    * 付息类
    */
    private String fxl;

    /**
    * 付息频率
    */
    private BigDecimal fxpl;

    /**
    * 浮动类型
    */
    private String fdlx;

    /**
    * 基准利率
    */
    private BigDecimal jzll;

    /**
    * 浮动基点
    */
    private BigDecimal fdjd;

    /**
    * 利差
    */
    private BigDecimal lc;

    /**
    * 含权类
    */
    private String hqz;

    /**
    * 发行人
    */
    private String ifname;

    /**
    * 是否平台债
    */
    private String ptz;

    /**
    * 是否担保
    */
    private String db;

    /**
    * 担保方式
    */
    private String dbfs;

    /**
    * 担保人
    */
    private String dbname;

    /**
    * 买断抵押面额(元)
    */
    private BigDecimal rbamount;

    /**
    * 质押面额(元)
    */
    private BigDecimal rpamount;

    /**
    * 国库定期存款质押面额
    */
    private BigDecimal rdamount;

    /**
    * 可用面额(元)
    */
    private BigDecimal me;

    /**
    * 备注
    */
    private String bz;

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost == null ? null : cost.trim();
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port == null ? null : port.trim();
    }

    public String getJiuqi() {
        return jiuqi;
    }

    public void setJiuqi(String jiuqi) {
        this.jiuqi = jiuqi == null ? null : jiuqi.trim();
    }

    public String getPvbpx() {
        return pvbpx;
    }

    public void setPvbpx(String pvbpx) {
        this.pvbpx = pvbpx == null ? null : pvbpx.trim();
    }

    public String getPv01x() {
        return pv01x;
    }

    public void setPv01x(String pv01x) {
        this.pv01x = pv01x == null ? null : pv01x.trim();
    }

    public String getVarx() {
        return varx;
    }

    public void setVarx(String varx) {
        this.varx = varx == null ? null : varx.trim();
    }

    public BigDecimal getFxzl() {
        return fxzl;
    }

    public void setFxzl(BigDecimal fxzl) {
        this.fxzl = fxzl;
    }

    public String getSshy() {
        return sshy;
    }

    public void setSshy(String sshy) {
        this.sshy = sshy == null ? null : sshy.trim();
    }

    public String getZtlevel() {
        return ztlevel;
    }

    public void setZtlevel(String ztlevel) {
        this.ztlevel = ztlevel == null ? null : ztlevel.trim();
    }

    public String getZxlevel() {
        return zxlevel;
    }

    public void setZxlevel(String zxlevel) {
        this.zxlevel = zxlevel == null ? null : zxlevel.trim();
    }

    public String getFxztlevel() {
        return fxztlevel;
    }

    public void setFxztlevel(String fxztlevel) {
        this.fxztlevel = fxztlevel == null ? null : fxztlevel.trim();
    }

    public String getFxzxlevel() {
        return fxzxlevel;
    }

    public void setFxzxlevel(String fxzxlevel) {
        this.fxzxlevel = fxzxlevel == null ? null : fxzxlevel.trim();
    }

    public BigDecimal getAcctngtype() {
        return acctngtype;
    }

    public void setAcctngtype(BigDecimal acctngtype) {
        this.acctngtype = acctngtype;
    }

    public BigDecimal getCccb() {
        return cccb;
    }

    public void setCccb(BigDecimal cccb) {
        this.cccb = cccb;
    }

    public BigDecimal getYslx() {
        return yslx;
    }

    public void setYslx(BigDecimal yslx) {
        this.yslx = yslx;
    }

    public BigDecimal getZmjz() {
        return zmjz;
    }

    public void setZmjz(BigDecimal zmjz) {
        this.zmjz = zmjz;
    }

    public BigDecimal getYjlx() {
        return yjlx;
    }

    public void setYjlx(BigDecimal yjlx) {
        this.yjlx = yjlx;
    }

    public BigDecimal getDnljlxsr() {
        return dnljlxsr;
    }

    public void setDnljlxsr(BigDecimal dnljlxsr) {
        this.dnljlxsr = dnljlxsr;
    }

    public BigDecimal getSyljlxsr() {
        return syljlxsr;
    }

    public void setSyljlxsr(BigDecimal syljlxsr) {
        this.syljlxsr = syljlxsr;
    }

    public BigDecimal getTdymtm() {
        return tdymtm;
    }

    public void setTdymtm(BigDecimal tdymtm) {
        this.tdymtm = tdymtm;
    }

    public BigDecimal getTjpmtm() {
        return tjpmtm;
    }

    public void setTjpmtm(BigDecimal tjpmtm) {
        this.tjpmtm = tjpmtm;
    }

    public BigDecimal getAvgcost() {
        return avgcost;
    }

    public void setAvgcost(BigDecimal avgcost) {
        this.avgcost = avgcost;
    }

    public BigDecimal getSettavgcost() {
        return settavgcost;
    }

    public void setSettavgcost(BigDecimal settavgcost) {
        this.settavgcost = settavgcost;
    }

    public BigDecimal getGz() {
        return gz;
    }

    public void setGz(BigDecimal gz) {
        this.gz = gz;
    }

    public BigDecimal getOpicsjjsz() {
        return opicsjjsz;
    }

    public void setOpicsjjsz(BigDecimal opicsjjsz) {
        this.opicsjjsz = opicsjjsz;
    }

    public BigDecimal getJjsz() {
        return jjsz;
    }

    public void setJjsz(BigDecimal jjsz) {
        this.jjsz = jjsz;
    }

    public BigDecimal getYzj() {
        return yzj;
    }

    public void setYzj(BigDecimal yzj) {
        this.yzj = yzj;
    }

    public BigDecimal getWindyzj() {
        return windyzj;
    }

    public void setWindyzj(BigDecimal windyzj) {
        this.windyzj = windyzj;
    }

    public BigDecimal getLxtz() {
        return lxtz;
    }

    public void setLxtz(BigDecimal lxtz) {
        this.lxtz = lxtz;
    }

    public BigDecimal getSyl() {
        return syl;
    }

    public void setSyl(BigDecimal syl) {
        this.syl = syl;
    }

    public BigDecimal getJqcbper() {
        return jqcbper;
    }

    public void setJqcbper(BigDecimal jqcbper) {
        this.jqcbper = jqcbper;
    }

    public BigDecimal getJjcbper() {
        return jjcbper;
    }

    public void setJjcbper(BigDecimal jjcbper) {
        this.jjcbper = jjcbper;
    }

    public BigDecimal getYslxper() {
        return yslxper;
    }

    public void setYslxper(BigDecimal yslxper) {
        this.yslxper = yslxper;
    }

    public BigDecimal getYjlxper() {
        return yjlxper;
    }

    public void setYjlxper(BigDecimal yjlxper) {
        this.yjlxper = yjlxper;
    }

    public BigDecimal getTyjj() {
        return tyjj;
    }

    public void setTyjj(BigDecimal tyjj) {
        this.tyjj = tyjj;
    }

    public BigDecimal getJjfy() {
        return jjfy;
    }

    public void setJjfy(BigDecimal jjfy) {
        this.jjfy = jjfy;
    }

    public BigDecimal getWindjjfy() {
        return windjjfy;
    }

    public void setWindjjfy(BigDecimal windjjfy) {
        this.windjjfy = windjjfy;
    }

    public String getSynx() {
        return synx;
    }

    public void setSynx(String synx) {
        this.synx = synx == null ? null : synx.trim();
    }

    public String getFxsyts() {
        return fxsyts;
    }

    public void setFxsyts(String fxsyts) {
        this.fxsyts = fxsyts == null ? null : fxsyts.trim();
    }

    public String getCdjr() {
        return cdjr;
    }

    public void setCdjr(String cdjr) {
        this.cdjr = cdjr == null ? null : cdjr.trim();
    }

    public String getNx() {
        return nx;
    }

    public void setNx(String nx) {
        this.nx = nx == null ? null : nx.trim();
    }

    public String getFxrq() {
        return fxrq;
    }

    public void setFxrq(String fxrq) {
        this.fxrq = fxrq == null ? null : fxrq.trim();
    }

    public String getFxl() {
        return fxl;
    }

    public void setFxl(String fxl) {
        this.fxl = fxl == null ? null : fxl.trim();
    }

    public BigDecimal getFxpl() {
        return fxpl;
    }

    public void setFxpl(BigDecimal fxpl) {
        this.fxpl = fxpl;
    }

    public String getFdlx() {
        return fdlx;
    }

    public void setFdlx(String fdlx) {
        this.fdlx = fdlx == null ? null : fdlx.trim();
    }

    public BigDecimal getJzll() {
        return jzll;
    }

    public void setJzll(BigDecimal jzll) {
        this.jzll = jzll;
    }

    public BigDecimal getFdjd() {
        return fdjd;
    }

    public void setFdjd(BigDecimal fdjd) {
        this.fdjd = fdjd;
    }

    public BigDecimal getLc() {
        return lc;
    }

    public void setLc(BigDecimal lc) {
        this.lc = lc;
    }

    public String getHqz() {
        return hqz;
    }

    public void setHqz(String hqz) {
        this.hqz = hqz == null ? null : hqz.trim();
    }

    public String getIfname() {
        return ifname;
    }

    public void setIfname(String ifname) {
        this.ifname = ifname == null ? null : ifname.trim();
    }

    public String getPtz() {
        return ptz;
    }

    public void setPtz(String ptz) {
        this.ptz = ptz == null ? null : ptz.trim();
    }

    public String getDb() {
        return db;
    }

    public void setDb(String db) {
        this.db = db == null ? null : db.trim();
    }

    public String getDbfs() {
        return dbfs;
    }

    public void setDbfs(String dbfs) {
        this.dbfs = dbfs == null ? null : dbfs.trim();
    }

    public String getDbname() {
        return dbname;
    }

    public void setDbname(String dbname) {
        this.dbname = dbname == null ? null : dbname.trim();
    }

    public BigDecimal getRbamount() {
        return rbamount;
    }

    public void setRbamount(BigDecimal rbamount) {
        this.rbamount = rbamount;
    }

    public BigDecimal getRpamount() {
        return rpamount;
    }

    public void setRpamount(BigDecimal rpamount) {
        this.rpamount = rpamount;
    }

    public BigDecimal getRdamount() {
        return rdamount;
    }

    public void setRdamount(BigDecimal rdamount) {
        this.rdamount = rdamount;
    }

    public BigDecimal getMe() {
        return me;
    }

    public void setMe(BigDecimal me) {
        this.me = me;
    }

    public String getBz() {
        return bz;
    }

    public void setBz(String bz) {
        this.bz = bz == null ? null : bz.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", cost=").append(cost);
        sb.append(", port=").append(port);
        sb.append(", jiuqi=").append(jiuqi);
        sb.append(", pvbpx=").append(pvbpx);
        sb.append(", pv01x=").append(pv01x);
        sb.append(", varx=").append(varx);
        sb.append(", fxzl=").append(fxzl);
        sb.append(", sshy=").append(sshy);
        sb.append(", ztlevel=").append(ztlevel);
        sb.append(", zxlevel=").append(zxlevel);
        sb.append(", fxztlevel=").append(fxztlevel);
        sb.append(", fxzxlevel=").append(fxzxlevel);
        sb.append(", acctngtype=").append(acctngtype);
        sb.append(", cccb=").append(cccb);
        sb.append(", yslx=").append(yslx);
        sb.append(", zmjz=").append(zmjz);
        sb.append(", yjlx=").append(yjlx);
        sb.append(", dnljlxsr=").append(dnljlxsr);
        sb.append(", syljlxsr=").append(syljlxsr);
        sb.append(", tdymtm=").append(tdymtm);
        sb.append(", tjpmtm=").append(tjpmtm);
        sb.append(", avgcost=").append(avgcost);
        sb.append(", settavgcost=").append(settavgcost);
        sb.append(", gz=").append(gz);
        sb.append(", opicsjjsz=").append(opicsjjsz);
        sb.append(", jjsz=").append(jjsz);
        sb.append(", yzj=").append(yzj);
        sb.append(", windyzj=").append(windyzj);
        sb.append(", lxtz=").append(lxtz);
        sb.append(", syl=").append(syl);
        sb.append(", jqcbper=").append(jqcbper);
        sb.append(", jjcbper=").append(jjcbper);
        sb.append(", yslxper=").append(yslxper);
        sb.append(", yjlxper=").append(yjlxper);
        sb.append(", tyjj=").append(tyjj);
        sb.append(", jjfy=").append(jjfy);
        sb.append(", windjjfy=").append(windjjfy);
        sb.append(", synx=").append(synx);
        sb.append(", fxsyts=").append(fxsyts);
        sb.append(", cdjr=").append(cdjr);
        sb.append(", nx=").append(nx);
        sb.append(", fxrq=").append(fxrq);
        sb.append(", fxl=").append(fxl);
        sb.append(", fxpl=").append(fxpl);
        sb.append(", fdlx=").append(fdlx);
        sb.append(", jzll=").append(jzll);
        sb.append(", fdjd=").append(fdjd);
        sb.append(", lc=").append(lc);
        sb.append(", hqz=").append(hqz);
        sb.append(", ifname=").append(ifname);
        sb.append(", ptz=").append(ptz);
        sb.append(", db=").append(db);
        sb.append(", dbfs=").append(dbfs);
        sb.append(", dbname=").append(dbname);
        sb.append(", rbamount=").append(rbamount);
        sb.append(", rpamount=").append(rpamount);
        sb.append(", rdamount=").append(rdamount);
        sb.append(", me=").append(me);
        sb.append(", bz=").append(bz);
        sb.append("]");
        return sb.toString();
    }
}