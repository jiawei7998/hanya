package com.singlee.hrbnewport.model;

import java.math.BigDecimal;

/**
    * 外汇敞口情况表
    */
public class IfsReportForeck {
    /**
    * 业务类型
    */
    private String product;

    /**
    * 币种
    */
    private String bz;

    /**
    * 即期多头
    */
    private BigDecimal gqdt;

    /**
    * 即期空头
    */
    private BigDecimal gqkt;

    /**
    * 远期多头
    */
    private BigDecimal yqdt;

    /**
    * 远期空头
    */
    private BigDecimal yqkt;

    /**
    * 净敞口
    */
    private BigDecimal jck;

    /**
    * 总净敞口
    */
    private BigDecimal zjck;

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product == null ? null : product.trim();
    }

    public String getBz() {
        return bz;
    }

    public void setBz(String bz) {
        this.bz = bz == null ? null : bz.trim();
    }

    public BigDecimal getGqdt() {
        return gqdt;
    }

    public void setGqdt(BigDecimal gqdt) {
        this.gqdt = gqdt;
    }

    public BigDecimal getGqkt() {
        return gqkt;
    }

    public void setGqkt(BigDecimal gqkt) {
        this.gqkt = gqkt;
    }

    public BigDecimal getYqdt() {
        return yqdt;
    }

    public void setYqdt(BigDecimal yqdt) {
        this.yqdt = yqdt;
    }

    public BigDecimal getYqkt() {
        return yqkt;
    }

    public void setYqkt(BigDecimal yqkt) {
        this.yqkt = yqkt;
    }

    public BigDecimal getJck() {
        return jck;
    }

    public void setJck(BigDecimal jck) {
        this.jck = jck;
    }

    public BigDecimal getZjck() {
        return zjck;
    }

    public void setZjck(BigDecimal zjck) {
        this.zjck = zjck;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", product=").append(product);
        sb.append(", bz=").append(bz);
        sb.append(", gqdt=").append(gqdt);
        sb.append(", gqkt=").append(gqkt);
        sb.append(", yqdt=").append(yqdt);
        sb.append(", yqkt=").append(yqkt);
        sb.append(", jck=").append(jck);
        sb.append(", zjck=").append(zjck);
        sb.append("]");
        return sb.toString();
    }
}