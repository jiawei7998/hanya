package com.singlee.hrbnewport.model;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author Liang
 * @date 2021/11/11 14:18
 * =======================
 */
public class G01ReportVO implements Serializable {

    private static final long serialVersionUID = 1L;
    private String product; //产品
    private BigDecimal cnyAmt; //人民币
    private BigDecimal  cnyConvAmt; //外币折人民币
    private BigDecimal  totalAmt		; //外币合人民币
    private String   parentProduct	; //父产品

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public BigDecimal getCnyAmt() {
        return cnyAmt;
    }

    public void setCnyAmt(BigDecimal cnyAmt) {
        this.cnyAmt = cnyAmt;
    }

    public BigDecimal getCnyConvAmt() {
        return cnyConvAmt;
    }

    public void setCnyConvAmt(BigDecimal cnyConvAmt) {
        this.cnyConvAmt = cnyConvAmt;
    }

    public BigDecimal getTotalAmt() {
        return totalAmt;
    }

    public void setTotalAmt(BigDecimal totalAmt) {
        this.totalAmt = totalAmt;
    }

    public String getParentProduct() {
        return parentProduct;
    }

    public void setParentProduct(String parentProduct) {
        this.parentProduct = parentProduct;
    }
}
