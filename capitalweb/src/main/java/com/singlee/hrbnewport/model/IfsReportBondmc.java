package com.singlee.hrbnewport.model;

import java.math.BigDecimal;

/**
    * 债券卖出兑付明细表
    */
public class IfsReportBondmc {
    /**
    * 交易号
    */
    private String dealno;

    /**
    * 风险账户类型
    */
    private String fxzhlx;

    /**
    * 汇集账户类型
    */
    private String kjzhlx;

    /**
    * 业务类型
    */
    private String ywlx;

    /**
    * 债券类型
    */
    private String zqlx;

    /**
    * 债券号码
    */
    private String zqhm;

    /**
    * 债券名称
    */
    private String zqmc;

    /**
    * 票面利率
    */
    private BigDecimal pmrate;

    /**
    * 头寸处理日
    */
    private String tcclr;

    /**
    * 卖出（兑付）日
    */
    private String mcr;

    /**
    * 复核日
    */
    private String fhr;

    /**
    * 撤销日
    */
    private String cxr;

    /**
    * 卖出（兑付）价
    */
    private BigDecimal mcj;

    /**
    * 债券卖出（兑付）面值（万元）
    */
    private BigDecimal zqmcmz;

    /**
    * 债券净价成本价
    */
    private BigDecimal zqjjcbj;

    /**
    * 债券净价成本(万元)
    */
    private BigDecimal zqjjcb;

    /**
    * 债券全价成本价
    */
    private BigDecimal zqqjcbj;

    /**
    * 债券全价成本(万元)
    */
    private BigDecimal zqqjcb;

    /**
    * 债券净价卖出价格
    */
    private BigDecimal zqjjmcjg;

    /**
    * 债券净价卖出额(万元)
    */
    private BigDecimal zqjjmce;

    /**
    * 债券全价卖出价格
    */
    private BigDecimal zqqjmcjg;

    /**
    * 债券全价卖出额（万元）
    */
    private BigDecimal zqqjmce;

    /**
    * 价差收入（元）
    */
    private BigDecimal jcsr;

    /**
    * 利息收入（元）
    */
    private BigDecimal lxsr;

    public String getDealno() {
        return dealno;
    }

    public void setDealno(String dealno) {
        this.dealno = dealno == null ? null : dealno.trim();
    }

    public String getFxzhlx() {
        return fxzhlx;
    }

    public void setFxzhlx(String fxzhlx) {
        this.fxzhlx = fxzhlx == null ? null : fxzhlx.trim();
    }

    public String getKjzhlx() {
        return kjzhlx;
    }

    public void setKjzhlx(String kjzhlx) {
        this.kjzhlx = kjzhlx == null ? null : kjzhlx.trim();
    }

    public String getYwlx() {
        return ywlx;
    }

    public void setYwlx(String ywlx) {
        this.ywlx = ywlx == null ? null : ywlx.trim();
    }

    public String getZqlx() {
        return zqlx;
    }

    public void setZqlx(String zqlx) {
        this.zqlx = zqlx == null ? null : zqlx.trim();
    }

    public String getZqhm() {
        return zqhm;
    }

    public void setZqhm(String zqhm) {
        this.zqhm = zqhm == null ? null : zqhm.trim();
    }

    public String getZqmc() {
        return zqmc;
    }

    public void setZqmc(String zqmc) {
        this.zqmc = zqmc == null ? null : zqmc.trim();
    }

    public BigDecimal getPmrate() {
        return pmrate;
    }

    public void setPmrate(BigDecimal pmrate) {
        this.pmrate = pmrate;
    }

    public String getTcclr() {
        return tcclr;
    }

    public void setTcclr(String tcclr) {
        this.tcclr = tcclr == null ? null : tcclr.trim();
    }

    public String getMcr() {
        return mcr;
    }

    public void setMcr(String mcr) {
        this.mcr = mcr == null ? null : mcr.trim();
    }

    public String getFhr() {
        return fhr;
    }

    public void setFhr(String fhr) {
        this.fhr = fhr == null ? null : fhr.trim();
    }

    public String getCxr() {
        return cxr;
    }

    public void setCxr(String cxr) {
        this.cxr = cxr == null ? null : cxr.trim();
    }

    public BigDecimal getMcj() {
        return mcj;
    }

    public void setMcj(BigDecimal mcj) {
        this.mcj = mcj;
    }

    public BigDecimal getZqmcmz() {
        return zqmcmz;
    }

    public void setZqmcmz(BigDecimal zqmcmz) {
        this.zqmcmz = zqmcmz;
    }

    public BigDecimal getZqjjcbj() {
        return zqjjcbj;
    }

    public void setZqjjcbj(BigDecimal zqjjcbj) {
        this.zqjjcbj = zqjjcbj;
    }

    public BigDecimal getZqjjcb() {
        return zqjjcb;
    }

    public void setZqjjcb(BigDecimal zqjjcb) {
        this.zqjjcb = zqjjcb;
    }

    public BigDecimal getZqqjcbj() {
        return zqqjcbj;
    }

    public void setZqqjcbj(BigDecimal zqqjcbj) {
        this.zqqjcbj = zqqjcbj;
    }

    public BigDecimal getZqqjcb() {
        return zqqjcb;
    }

    public void setZqqjcb(BigDecimal zqqjcb) {
        this.zqqjcb = zqqjcb;
    }

    public BigDecimal getZqjjmcjg() {
        return zqjjmcjg;
    }

    public void setZqjjmcjg(BigDecimal zqjjmcjg) {
        this.zqjjmcjg = zqjjmcjg;
    }

    public BigDecimal getZqjjmce() {
        return zqjjmce;
    }

    public void setZqjjmce(BigDecimal zqjjmce) {
        this.zqjjmce = zqjjmce;
    }

    public BigDecimal getZqqjmcjg() {
        return zqqjmcjg;
    }

    public void setZqqjmcjg(BigDecimal zqqjmcjg) {
        this.zqqjmcjg = zqqjmcjg;
    }

    public BigDecimal getZqqjmce() {
        return zqqjmce;
    }

    public void setZqqjmce(BigDecimal zqqjmce) {
        this.zqqjmce = zqqjmce;
    }

    public BigDecimal getJcsr() {
        return jcsr;
    }

    public void setJcsr(BigDecimal jcsr) {
        this.jcsr = jcsr;
    }

    public BigDecimal getLxsr() {
        return lxsr;
    }

    public void setLxsr(BigDecimal lxsr) {
        this.lxsr = lxsr;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", dealno=").append(dealno);
        sb.append(", fxzhlx=").append(fxzhlx);
        sb.append(", kjzhlx=").append(kjzhlx);
        sb.append(", ywlx=").append(ywlx);
        sb.append(", zqlx=").append(zqlx);
        sb.append(", zqhm=").append(zqhm);
        sb.append(", zqmc=").append(zqmc);
        sb.append(", pmrate=").append(pmrate);
        sb.append(", tcclr=").append(tcclr);
        sb.append(", mcr=").append(mcr);
        sb.append(", fhr=").append(fhr);
        sb.append(", cxr=").append(cxr);
        sb.append(", mcj=").append(mcj);
        sb.append(", zqmcmz=").append(zqmcmz);
        sb.append(", zqjjcbj=").append(zqjjcbj);
        sb.append(", zqjjcb=").append(zqjjcb);
        sb.append(", zqqjcbj=").append(zqqjcbj);
        sb.append(", zqqjcb=").append(zqqjcb);
        sb.append(", zqjjmcjg=").append(zqjjmcjg);
        sb.append(", zqjjmce=").append(zqjjmce);
        sb.append(", zqqjmcjg=").append(zqqjmcjg);
        sb.append(", zqqjmce=").append(zqqjmce);
        sb.append(", jcsr=").append(jcsr);
        sb.append(", lxsr=").append(lxsr);
        sb.append("]");
        return sb.toString();
    }
}