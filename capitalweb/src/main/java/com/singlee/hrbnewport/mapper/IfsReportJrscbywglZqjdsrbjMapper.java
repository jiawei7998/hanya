package com.singlee.hrbnewport.mapper;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportJrscbywglZqjdsrbj;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

public interface IfsReportJrscbywglZqjdsrbjMapper extends Mapper<IfsReportJrscbywglZqjdsrbj> {

    SlOutBean ifsReportJrscbywglZqjdsrbjCreate(Map<String, String> map);
}