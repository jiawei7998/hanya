package com.singlee.hrbnewport.mapper;

import com.singlee.hrbnewport.model.IfsReportZdsjgljx;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 20:52
 * @description：${description}
 * @modified By：
 * @version:   
 */
@Mapper
public interface IfsReportZdsjgljxMapper {
    /**
     * insert record to table
     * @param record the record
     * @return insert count
     */
    int insert(IfsReportZdsjgljx record);

    /**
     * insert record to table selective
     * @param record the record
     * @return insert count
     */
    int insertSelective(IfsReportZdsjgljx record);

    List<IfsReportZdsjgljx> getDate(Map<String,Object> map);

    void call(Map<String,Object> map);
}