package com.singlee.hrbnewport.mapper;
import org.apache.ibatis.annotations.Param;
import java.util.List;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportG2200;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

public interface IfsReportG2200Mapper extends Mapper<IfsReportG2200> {

    SlOutBean ifsReportG2200Create(Map<String, String> map);

    List<IfsReportG2200> selectByProductExcel();

}