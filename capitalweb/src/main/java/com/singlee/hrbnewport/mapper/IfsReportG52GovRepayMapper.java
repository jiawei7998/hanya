package com.singlee.hrbnewport.mapper;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.model.IfsReportG1401;
import com.singlee.hrbnewport.model.IfsReportG52GovRepay;
import org.apache.ibatis.session.RowBounds;

import java.util.Map;

/**
 * G52地方政府融资平台及支出责任债务持有情况统计表
 * 2021/12/8
 * @Auther:zk
 */

public interface IfsReportG52GovRepayMapper {

    Page<IfsReportG52GovRepay> searchAllPage(Map<String,Object> param, RowBounds rowBounds);

    void createData(Map<String,Object> map);

}