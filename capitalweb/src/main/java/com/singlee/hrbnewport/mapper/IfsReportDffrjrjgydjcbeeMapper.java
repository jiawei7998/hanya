package com.singlee.hrbnewport.mapper;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportDffrjrjgydjcbee;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

public interface IfsReportDffrjrjgydjcbeeMapper extends Mapper<IfsReportDffrjrjgydjcbee> {

    SlOutBean ifsReportDffrjrjgydjcbeeCreate(Map<String, String> map);
}