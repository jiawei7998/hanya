package com.singlee.hrbnewport.mapper;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.TbZyzjinfo;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

public interface TbZyzjinfoMapper {
    int insert(TbZyzjinfo record);

    int insertSelective(TbZyzjinfo record);

    Page<TbZyzjinfo> getTbZYZJINFO(RowBounds rb);

    List<TbZyzjinfo> getTbZYZJINFO2();

    SlOutBean ZYZJINFOReportCreate(Map<String, String> map);
}