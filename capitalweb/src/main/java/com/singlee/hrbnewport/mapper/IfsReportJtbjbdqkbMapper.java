package com.singlee.hrbnewport.mapper;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportJtbjbdqkb;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

public interface IfsReportJtbjbdqkbMapper extends Mapper<IfsReportJtbjbdqkb> {

    SlOutBean ifsReportJtbjbdqkbCreate(Map<String, String> map);
}