package com.singlee.hrbnewport.mapper;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.A1411ReportVO;

import java.util.Map;

public interface A1411ReportMapper {

    public Page<A1411ReportVO> getA1411Report();

    SlOutBean A1411ReportCreate(Map<String, String> map);
}
