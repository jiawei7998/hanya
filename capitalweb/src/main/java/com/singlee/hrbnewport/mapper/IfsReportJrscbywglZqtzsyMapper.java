package com.singlee.hrbnewport.mapper;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportJrscbywglZqtzsy;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

public interface IfsReportJrscbywglZqtzsyMapper extends Mapper<IfsReportJrscbywglZqtzsy> {

    SlOutBean ifsReportJrscbywglZqtzsyCreate(Map<String, String> map);
}