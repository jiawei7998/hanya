package com.singlee.hrbnewport.mapper;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.model.IfsReportDepositInvestorReport;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/**
 *申购信息查询结果
 * 2021/12/6
 * @Auther:zk
 */    
    
public interface IfsReportDepositInvestorReportMapper {
    Page<IfsReportDepositInvestorReport> selectAllReport(Map<String,Object> map, RowBounds rowBounds);

    void creatReport(Map<String,Object> map);

    List<IfsReportDepositInvestorReport> selectResult();
}