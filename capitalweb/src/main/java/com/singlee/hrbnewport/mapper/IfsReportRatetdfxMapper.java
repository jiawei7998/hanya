package com.singlee.hrbnewport.mapper;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportRatetdfx;

import java.util.List;
import java.util.Map;

/**
 * 利率特定风险计算表
 */
public interface IfsReportRatetdfxMapper {


    List<IfsReportRatetdfx> getRateTDFX();

    SlOutBean rateTDFXCreate(Map<String, String> map);
}