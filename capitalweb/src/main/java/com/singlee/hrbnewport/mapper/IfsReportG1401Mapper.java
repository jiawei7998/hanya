package com.singlee.hrbnewport.mapper;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.model.IfsReportG1401;
import org.apache.ibatis.session.RowBounds;

import java.util.Map;

/**
 * G1401大额风险暴露总体情况
 * 2021/12/7
 * @Auther:zk
 */    
    
public interface IfsReportG1401Mapper {

    Page<IfsReportG1401> searchAllPage(Map<String,Object> param, RowBounds rowBounds);

    void createData(Map<String,Object> map);

}