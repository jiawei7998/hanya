package com.singlee.hrbnewport.mapper;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportCshbnwtzQxjgcbsy;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

/**
 * @author     ：meigy
 * @date       ：Created in 2021/11/13 15:01
 * @description：${description}
 * @modified By：
 * @version:     
 */
public interface IfsReportCshbnwtzQxjgcbsyMapper extends Mapper<IfsReportCshbnwtzQxjgcbsy> {

    SlOutBean ifsReportCshbnwtzQxjgcbsyCreate(Map<String, String> map);
}