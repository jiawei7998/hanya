package com.singlee.hrbnewport.mapper;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportGeneralRaterisk;

import java.util.Map;

/**
 * 市场风险标准法资本要求情况表（一般利率风险-到期日法）
 */
public interface IfsReportGeneralRateriskMapper {
    int insert(IfsReportGeneralRaterisk record);

    int insertSelective(IfsReportGeneralRaterisk record);

    Page<IfsReportGeneralRaterisk> getGeneralRateRisk();

    SlOutBean generalRateRiskCreate(Map<String, String> map);
}