package com.singlee.hrbnewport.mapper;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.TbHoldBond;

import java.util.List;
import java.util.Map;

/**
 * 持有债券结构情况表
 */
public interface TbHoldBondMapper {


    List<TbHoldBond> getHoldBond();

    SlOutBean holdBond2Create(Map<String, String> map);
}