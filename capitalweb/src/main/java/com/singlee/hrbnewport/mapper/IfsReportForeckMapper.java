package com.singlee.hrbnewport.mapper;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportForeck;

import java.util.List;
import java.util.Map;

/**
 * 外汇敞口情况表
 */
public interface IfsReportForeckMapper {


    List<IfsReportForeck> getForeck();

    SlOutBean ReportForeckCreate(Map<String, String> map);
}