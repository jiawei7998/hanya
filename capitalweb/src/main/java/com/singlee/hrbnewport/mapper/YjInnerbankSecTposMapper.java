package com.singlee.hrbnewport.mapper;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.model.YjInnerbankRvrepo;
import com.singlee.hrbnewport.model.YjInnerbankSecTpos;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/**
 *同业业务管理报告-债券投资
 * 2021/12/8
 * @Auther:zk
 */    
    
public interface YjInnerbankSecTposMapper {
    Page<YjInnerbankSecTpos> searchAllPage(Map<String,Object> param, RowBounds rowBounds);

    void createData(Map<String,Object> map);

    List<YjInnerbankSecTpos> searchAll(Map<String, Object> param);
}