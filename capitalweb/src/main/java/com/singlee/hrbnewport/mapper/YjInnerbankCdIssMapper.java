package com.singlee.hrbnewport.mapper;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.model.YjInnerbankCdIss;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/**
 *同业业务管理报告-同业存单发行
 * 2021/12/8
 * @Auther:zk
 */    
    
public interface YjInnerbankCdIssMapper {
    Page<YjInnerbankCdIss> searchAllPage(Map<String,Object> param, RowBounds rowBounds);

    void createData(Map<String,Object> map);

    List<YjInnerbankCdIss> searchAll(Map<String, Object> param);
}