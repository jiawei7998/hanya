package com.singlee.hrbnewport.mapper;

import java.util.List;
import java.util.Map;

public interface G21NewReportMapper {

	List<Map<String,Object>> getG21Data(Map<String,Object> map);

	Map<String,Object> generateG21Data(Map<String,Object> map);
}
