package com.singlee.hrbnewport.mapper;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportCshbnwtzJczc;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

/**
 * @author     ：meigy
 * @date       ：Created in 2021/11/11 13:32
 * @description：${description}
 * @modified By：
 * @version:     
 */
public interface IfsReportCshbnwtzJczcMapper extends Mapper<IfsReportCshbnwtzJczc> {

    SlOutBean ifsReportCshbnwtzJczcCreate(Map<String, String> map);
}