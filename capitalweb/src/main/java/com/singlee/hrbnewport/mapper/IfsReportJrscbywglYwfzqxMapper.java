package com.singlee.hrbnewport.mapper;
import org.apache.ibatis.annotations.Param;
import java.util.List;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportJrscbywglYwfzqx;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

public interface IfsReportJrscbywglYwfzqxMapper extends Mapper<IfsReportJrscbywglYwfzqx> {

    SlOutBean ifsReportJrscbywglYwfzqxCreate(Map<String, String> map);
}