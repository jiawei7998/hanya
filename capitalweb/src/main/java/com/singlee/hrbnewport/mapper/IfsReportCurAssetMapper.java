package com.singlee.hrbnewport.mapper;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.model.IfsReportCdPosit;
import com.singlee.hrbnewport.model.IfsReportCurAsset;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/**
 * 哈尔滨银行优质流动性资产台账
 * 2021/12/9
 * @Auther:zk
 */    
    
public interface IfsReportCurAssetMapper {
    Page<IfsReportCurAsset> searchAllPage(Map<String,Object> param, RowBounds rowBounds);

    void createData(Map<String,Object> map);

    List<IfsReportCurAsset> searchAll(Map<String, Object> map);
}