package com.singlee.hrbnewport.mapper;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportCshbnwtzMxcth;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

/**
 * @author     ：meigy
 * @date       ：Created in 2021/11/10 13:51
 * @description：${description}
 * @modified By：
 * @version:     
 */
public interface IfsReportCshbnwtzMxcthMapper extends Mapper<IfsReportCshbnwtzMxcth> {

    SlOutBean ifsReportCshbnwtzMxcthCreate(Map<String, String> map);
}