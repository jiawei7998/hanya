package com.singlee.hrbnewport.mapper;

import java.util.List;
import java.util.Map;

/**
 * G25_II-净稳定资金比例
 * @author copysun
 */
public interface IfsReportG2501Mapper {

    List<Map<String, Object>> getExportListData(Map<String, Object> map);

    void generateData(Map<String, Object> map);
}