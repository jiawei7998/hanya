package com.singlee.hrbnewport.mapper;

import java.util.List;
import java.util.Map;

public interface IfsReportBondVarietyStructureMapper {

    void generateData(Map<String,Object> map);

    List<Map<String,Object>> getData(Map<String,Object> map);
}