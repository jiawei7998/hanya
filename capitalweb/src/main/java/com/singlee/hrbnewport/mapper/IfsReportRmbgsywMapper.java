package com.singlee.hrbnewport.mapper;

import com.singlee.hrbnewport.model.IfsReportRmbgsyw;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 20:51
 * @description：${description}
 * @modified By：
 * @version:   
 */

public interface IfsReportRmbgsywMapper extends Mapper<IfsReportRmbgsyw> {
    /**
     * insert record to table
     * @param record the record
     * @return insert count
     */
    int insertB(IfsReportRmbgsyw record);

    /**
     * insert record to table selective
     * @param record the record
     * @return insert count
     */
    int insertSelective(IfsReportRmbgsyw record);

    List<IfsReportRmbgsyw> getDate(Map<String,Object> map);

    void call(Map<String,Object> map);

    List<Map<String,Object>> getBaseDate(Map<String,Object> map);

    String getSpotId();

    String getSwapId();

    String getFwdindId();

    Map<String,Object> getFxdh(Map<String,Object> map);
}