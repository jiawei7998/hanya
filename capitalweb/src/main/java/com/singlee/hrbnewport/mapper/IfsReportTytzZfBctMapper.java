package com.singlee.hrbnewport.mapper;

import com.singlee.hrbnewport.model.IfsReportTytzZfBct;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 16:22
 * @description：${description}
 * @modified By：
 * @version:   
 */
@Mapper
public interface IfsReportTytzZfBctMapper {
    /**
     * insert record to table
     * @param record the record
     * @return insert count
     */
    int insert(IfsReportTytzZfBct record);

    /**
     * insert record to table selective
     * @param record the record
     * @return insert count
     */
    int insertSelective(IfsReportTytzZfBct record);

    List<IfsReportTytzZfBct> getDate(Map<String,Object> map);

    void call(Map<String,Object> map);
}