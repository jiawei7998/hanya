package com.singlee.hrbnewport.mapper;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportRmbtyye;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

public interface IfsReportRmbtyyeMapper extends Mapper<IfsReportRmbtyye> {

    SlOutBean ifsReportRmbtyyeCreate(Map<String, String> map);
}