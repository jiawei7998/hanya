package com.singlee.hrbnewport.mapper;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportCkhyhtyhlhcffzHz;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

public interface IfsReportCkhyhtyhlhcffzHzMapper extends Mapper<IfsReportCkhyhtyhlhcffzHz> {

    SlOutBean ifsReportCkhyhtyhlhcffzHzCreate(Map<String, String> map);
}