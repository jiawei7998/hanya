package com.singlee.hrbnewport.mapper;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.TbRateSwap;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

public interface TbRateSwapMapper {

    Page<TbRateSwap> getRateSwap(RowBounds rb);

    List<TbRateSwap> getRateSwap();

    SlOutBean rateSwapCreate(Map<String, String> map);
}