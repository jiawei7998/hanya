package com.singlee.hrbnewport.mapper;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.model.YjInnerbankRvrepo;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/**
 *同业业务管理报告-买入返售债券业务明细表
 * 2021/12/8
 * @Auther:zk
 */    
    
public interface YjInnerbankRvrepoMapper {
    Page<YjInnerbankRvrepo> searchAllPage(Map<String,Object> param, RowBounds rowBounds);

    void createData(Map<String,Object> map);

    List<YjInnerbankRvrepo> searchAll(Map<String, Object> param);
}