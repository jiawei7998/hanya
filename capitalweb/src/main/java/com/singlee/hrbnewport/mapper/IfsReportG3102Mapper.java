package com.singlee.hrbnewport.mapper;
import org.apache.ibatis.annotations.Param;
import java.util.List;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportG3102;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

public interface IfsReportG3102Mapper extends Mapper<IfsReportG3102> {

    SlOutBean ifsReportG3102Create(Map<String, String> map);

    List<IfsReportG3102> selectByProductExcel();

}