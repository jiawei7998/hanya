package com.singlee.hrbnewport.mapper;

import com.singlee.hrbnewport.model.IfsReportTytzWtycc;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/3 15:37
 * @description：${description}
 * @modified By：
 * @version:   
 */
@Mapper
public interface IfsReportTytzWtyccMapper {
    /**
     * insert record to table
     * @param record the record
     * @return insert count
     */
    int insert(IfsReportTytzWtycc record);

    /**
     * insert record to table selective
     * @param record the record
     * @return insert count
     */
    int insertSelective(IfsReportTytzWtycc record);

    List<IfsReportTytzWtycc> getDate(Map<String,Object> map);

    void call(Map<String,Object> map);
}