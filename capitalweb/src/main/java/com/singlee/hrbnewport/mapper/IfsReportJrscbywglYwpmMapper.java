package com.singlee.hrbnewport.mapper;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportJrscbywglYwpm;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

public interface IfsReportJrscbywglYwpmMapper extends Mapper<IfsReportJrscbywglYwpm> {

    SlOutBean ifsReportJrscbywglYwpmCreate(Map<String, String> map);
}