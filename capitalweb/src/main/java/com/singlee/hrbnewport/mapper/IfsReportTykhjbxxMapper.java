package com.singlee.hrbnewport.mapper;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportTykhjbxx;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

public interface IfsReportTykhjbxxMapper extends Mapper<IfsReportTykhjbxx> {

    SlOutBean ifsReportTykhjbxxCreate(Map<String, String> map);
}