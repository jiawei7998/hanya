package com.singlee.hrbnewport.mapper;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportG3301;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

public interface IfsReportG3301Mapper extends Mapper<IfsReportG3301> {

    SlOutBean ifsReportG3301Create(Map<String, String> map);
}