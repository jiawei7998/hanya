package com.singlee.hrbnewport.mapper;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportDkcfhcfyhtyjlhzcbAgbabz;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

public interface IfsReportDkcfhcfyhtyjlhzcbAgbabzMapper extends Mapper<IfsReportDkcfhcfyhtyjlhzcbAgbabz> {

    SlOutBean ifsReportDkcfhcfyhtyjlhzcbAgbabzCreate(Map<String, String> map);
}