package com.singlee.hrbnewport.mapper;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.TbDealInformation;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

public interface TbDealInformationMapper {
    int insert(TbDealInformation record);

    int insertSelective(TbDealInformation record);

    Page<TbDealInformation> getTbDealInformation(RowBounds rb);

    List<TbDealInformation> getTbDealInformation();

    SlOutBean ZJJYXXReportCreate(Map<String, String> map);
}