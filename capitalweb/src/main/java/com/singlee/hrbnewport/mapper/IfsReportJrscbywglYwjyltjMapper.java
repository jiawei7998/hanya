package com.singlee.hrbnewport.mapper;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportJrscbywglYwjyltj;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

public interface IfsReportJrscbywglYwjyltjMapper extends Mapper<IfsReportJrscbywglYwjyltj> {

    SlOutBean ifsReportJrscbywglYwjyltjCreate(Map<String, String> map);
}