package com.singlee.hrbnewport.mapper;

import com.github.pagehelper.Page;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/**
 * @author copysun
 */
public interface PeopleBankExportMapper {

	/**
	 * 生成数据
	 * @param map
	 */
	void generateData(Map<String, Object> map);

	/**
	 * 对公客户信息表
	 *
	 * @param map
	 * @return
	 */
	Page<Map<String, Object>> getCustPage(Map<String, Object> map, RowBounds rb);
	List<Map<String, Object>> getCustPage(Map<String, Object> map);
	/**
	 * 同业存款基础信息表
	 *
	 * @param map
	 * @return
	 */
	Page<Map<String, Object>> getInterbankDepositsBasePage(Map<String, Object> map, RowBounds rb);
	List<Map<String, Object>> getInterbankDepositsBasePage(Map<String, Object> map);

	/**
	 * 同业存款余额信息表
	 *
	 * @param map
	 * @return
	 */
	Page<Map<String, Object>> getInterbankDepositsBalancePage(Map<String, Object> map, RowBounds rb);
	List<Map<String, Object>> getInterbankDepositsBalancePage(Map<String, Object> map);

	/**
	 * 同业存款发生额信息表
	 *
	 * @param map
	 * @return
	 */
	Page<Map<String, Object>> getInterbankDepositsAmountPage(Map<String, Object> map, RowBounds rb);
	List<Map<String, Object>> getInterbankDepositsAmountPage(Map<String, Object> map);
	/**
	 * 同业借贷基础信息表
	 *
	 * @param map
	 * @return
	 */
	Page<Map<String, Object>> getInterbankBorrowBasePage(Map<String, Object> map, RowBounds rb);
	List<Map<String, Object>> getInterbankBorrowBasePage(Map<String, Object> map);
	/**
	 * 同业借贷余额表
	 *
	 * @param map
	 * @return
	 */
	Page<Map<String, Object>> getInterbankBorrowBalancePage(Map<String, Object> map, RowBounds rb);
	List<Map<String, Object>> getInterbankBorrowBalancePage(Map<String, Object> map);
	/**
	 * 同业借贷发生额信息表
	 *
	 * @param map
	 * @return
	 */
	Page<Map<String, Object>> getInterbankBorrowAmountPage(Map<String, Object> map, RowBounds rb);
	List<Map<String, Object>> getInterbankBorrowAmountPage(Map<String, Object> map);
	/**
	 * 买入返售及卖出回购基础信息表
	 *
	 * @param map
	 * @return
	 */
	Page<Map<String, Object>> getRprhBasePage(Map<String, Object> map, RowBounds rb);
	List<Map<String, Object>> getRprhBasePage(Map<String, Object> map);
	/**
	 * 买入返售及卖出回购余额信息表
	 *
	 * @param map
	 * @return
	 */
	Page<Map<String, Object>> getRprhBalancePage(Map<String, Object> map, RowBounds rb);
	List<Map<String, Object>> getRprhBalancePage(Map<String, Object> map);
	/**
	 * 买入返售及卖出回购发生额信息表
	 *
	 * @param map
	 * @return
	 */
	Page<Map<String, Object>> getRprhAmountPage(Map<String, Object> map, RowBounds rb);
	List<Map<String, Object>> getRprhAmountPage(Map<String, Object> map);


}
