package com.singlee.hrbnewport.mapper;

import com.singlee.hrbnewport.model.IfsReportZyzctfGbm;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/8 20:17
 * @description：${description}
 * @modified By：
 * @version:   
 */
@Mapper
public interface IfsReportZyzctfGbmMapper {
    /**
     * insert record to table
     * @param record the record
     * @return insert count
     */
    int insert(IfsReportZyzctfGbm record);

    /**
     * insert record to table selective
     * @param record the record
     * @return insert count
     */
    int insertSelective(IfsReportZyzctfGbm record);

    List<Map<String,Object>> getDate(Map<String,Object> map);

    List<Map<String,Object>> getDate2(Map<String,Object> map);

    List<Map<String,Object>> getDate3(Map<String,Object> map);

    void call(Map<String,Object> map);
}