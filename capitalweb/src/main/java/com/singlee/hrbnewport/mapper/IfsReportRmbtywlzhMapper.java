package com.singlee.hrbnewport.mapper;

import com.singlee.hrbnewport.model.IfsReportRmbtywlzh;
import tk.mybatis.mapper.common.Mapper;


import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 20:51
 * @description：${description}
 * @modified By：
 * @version:   
 */

public interface IfsReportRmbtywlzhMapper extends Mapper<IfsReportRmbtywlzh> {
    /**
     * insert record to table
     * @param record the record
     * @return insert count
     */
    int insert(IfsReportRmbtywlzh record);

    /**
     * insert record to table selective
     * @param record the record
     * @return insert count
     */
    int insertSelective(IfsReportRmbtywlzh record);

    List<IfsReportRmbtywlzh> getDate(Map<String,Object> map);

    void call(Map<String,Object> map);
}