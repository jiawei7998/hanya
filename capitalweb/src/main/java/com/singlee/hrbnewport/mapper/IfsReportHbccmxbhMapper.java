package com.singlee.hrbnewport.mapper;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportHbccmxbh;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

public interface IfsReportHbccmxbhMapper {


    Page<IfsReportHbccmxbh> getIfsReportHbccmxbh(RowBounds rb);

    List<IfsReportHbccmxbh> getIfsReportHbccmxbh();

    SlOutBean ifsReportHbccmxbhCreate(Map<String, String> map);
}