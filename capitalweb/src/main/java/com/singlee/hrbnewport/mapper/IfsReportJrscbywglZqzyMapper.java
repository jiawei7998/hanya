package com.singlee.hrbnewport.mapper;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportJrscbywglZqzy;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

/**
 * @author ：meigy
 * @date ：Created in 2021/11/19 17:06
 * @description：${description}
 * @modified By：
 * @version:
 */
public interface IfsReportJrscbywglZqzyMapper extends Mapper<IfsReportJrscbywglZqzy> {

    SlOutBean ifsReportJrscbywglZqzyCreate(Map<String, String> map);
}