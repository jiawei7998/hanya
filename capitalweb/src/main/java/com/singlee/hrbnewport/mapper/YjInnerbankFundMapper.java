package com.singlee.hrbnewport.mapper;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.model.YjInnerbankFund;
import com.singlee.hrbnewport.model.YjInnerbankSecTpos;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/**
 *同业业务管理报告-基金投资（资产）投资业务明细表
 * 2021/12/8
 * @Auther:zk
 */    
    
public interface YjInnerbankFundMapper {
    Page<YjInnerbankFund> searchAllPage(Map<String,Object> param, RowBounds rowBounds);

    void createData(Map<String,Object> map);

    List<YjInnerbankFund> searchAll(Map<String, Object> param);
}