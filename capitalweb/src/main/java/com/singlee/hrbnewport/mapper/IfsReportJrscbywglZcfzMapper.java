package com.singlee.hrbnewport.mapper;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportJrscbywglZcfz;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

/**
 * @author ：meigy
 * @date ：Created in 2021/11/17 14:26
 * @description：${description}
 * @modified By：
 * @version:
 */
public interface IfsReportJrscbywglZcfzMapper extends Mapper<IfsReportJrscbywglZcfz> {

    SlOutBean ifsReportJrscbywglZcfzCreate(Map<String, String> map);
}