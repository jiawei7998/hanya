package com.singlee.hrbnewport.mapper;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.model.IfsReportCdPosit;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/**
 *
 * 2021/12/9
 * @Auther:zk
 */    
    
public interface IfsReportCdPositMapper {

    Page<IfsReportCdPosit> searchAllPage(Map<String,Object> param, RowBounds rowBounds);

    void createData(Map<String,Object> map);


    List<IfsReportCdPosit> searchAll(Map<String, Object> map);

}