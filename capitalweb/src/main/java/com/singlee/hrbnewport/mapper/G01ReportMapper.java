package com.singlee.hrbnewport.mapper;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.G01ReportVO;

import java.util.Map;

public interface G01ReportMapper {


	public Page<G01ReportVO> getG01Report();

	SlOutBean G01ReportCreate(Map<String, String> map);
	
	
}
