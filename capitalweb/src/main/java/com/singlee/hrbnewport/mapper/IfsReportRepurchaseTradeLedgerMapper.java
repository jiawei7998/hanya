package com.singlee.hrbnewport.mapper;

import com.github.pagehelper.Page;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

public interface IfsReportRepurchaseTradeLedgerMapper {

    void generateData(Map<String,Object> map);

    Page<Map<String,Object>> getData(Map<String,Object> map, RowBounds rowBounds);
    List<Map<String,Object>> getData(Map<String,Object> map);
}