package com.singlee.hrbnewport.mapper;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportBondmc;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/**
 * 债券卖出、兑付明细表
 */
public interface IfsReportBondmcMapper {


    Page<IfsReportBondmc> getBondMC(RowBounds rb);

    List<IfsReportBondmc> getBondMC();

    SlOutBean ReportBondMCCreate(Map<String, String> map);
}