package com.singlee.hrbnewport.mapper;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.TbCustomerRisks;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

public interface TbCustomerRisksMapper {

    Page<TbCustomerRisks> getCustomerRisks(RowBounds rb);

    List<TbCustomerRisks> getCustomerRisks();

    SlOutBean customerRisksCreate(Map<String, String> map);
}