package com.singlee.hrbnewport.mapper;

import com.singlee.hrbnewport.model.IfsReportEstateCom;

import java.util.List;
import java.util.Map;

/**
 * @Author zhangkai
 * @Description 
 * @Date 
 */
public interface IfsReportEstateComMapper {

    List<IfsReportEstateCom> selectAllReport();

    void creatReport(Map<String,Object> map);

    Map<String, Object> selectResult();

}