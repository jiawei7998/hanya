package com.singlee.hrbnewport.mapper;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportCshbnwtzZbbbjtctq;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

/**
 * @author ：meigy
 * @date ：Created in 2021/11/15 11:07
 * @description：${description}
 * @modified By：
 * @version:
 */
public interface IfsReportCshbnwtzZbbbjtctqMapper extends Mapper<IfsReportCshbnwtzZbbbjtctq> {

    SlOutBean ifsReportCshbnwtzZbbbjtctqCreate(Map<String, String> map);
}