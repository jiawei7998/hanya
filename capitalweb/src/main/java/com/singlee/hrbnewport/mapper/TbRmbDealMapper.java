package com.singlee.hrbnewport.mapper;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.TbRmbDeal;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/**
 * 本币交易情况表
 */
public interface TbRmbDealMapper {

    Page<TbRmbDeal> getRMBDeal(RowBounds rb);

    List<TbRmbDeal> getRMBDeal();

    SlOutBean RMBDealReportCreate(Map<String, String> map);
}