package com.singlee.hrbnewport.mapper;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportForezyjyxe;

import java.util.List;
import java.util.Map;

public interface IfsReportForezyjyxeMapper {

    List<IfsReportForezyjyxe> getForeZyjyxe();

    SlOutBean reportForeckCreate(Map<String, String> map);
}