package com.singlee.hrbnewport.mapper;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportJrscbywglZqtzdcqx;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

public interface IfsReportJrscbywglZqtzdcqxMapper extends Mapper<IfsReportJrscbywglZqtzdcqx> {

    SlOutBean ifsReportJrscbywglZqtzdcqxCreate(Map<String, String> map);
}