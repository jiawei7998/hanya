package com.singlee.hrbnewport.mapper;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.model.IfsReportG1402;
import com.singlee.hrbnewport.model.IfsReportG1405;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/**
 * G1402-大额风险暴露
 * 2021/11/25
 * @Auther:zk
 */    
    
public interface IfsReportG1402Mapper {
    Page<IfsReportG1402> searchAllPage(Map<String,Object> map, RowBounds rb);

    void createG1402Report(Map<String , Object> map);

    List<IfsReportG1402> searchAll(Map<String, Object> map);
}