package com.singlee.hrbnewport.mapper;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.TbConsolidatePosition;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

public interface TbConsolidatePositionMapper {
    Page<TbConsolidatePosition> getConsolidatePosition(Map<String, String> map ,RowBounds rb);

    List<TbConsolidatePosition> getConsolidatePosition(Map<String, String> map );
    //基金投资
    Page<TbConsolidatePosition> getConsolidatePosition1(Map<String, String> map ,RowBounds rb);

    List<TbConsolidatePosition> getConsolidatePosition1(Map<String, String> map);

    SlOutBean ConsolidatePositionCreate(Map<String, String > map);
}