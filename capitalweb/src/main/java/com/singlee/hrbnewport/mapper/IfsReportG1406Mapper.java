package com.singlee.hrbnewport.mapper;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.model.IfsReportG1406;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/**
 * G1406-大额风险暴露
 * by zk
 */
public interface IfsReportG1406Mapper {

    Page<IfsReportG1406> searchAllPage(Map<String,Object> map, RowBounds rb);

    void createG1406Report(Map<String , Object> map);

    List<IfsReportG1406> searchAll(Map<String, Object> map);
}