package com.singlee.hrbnewport.mapper;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.TbBondProfit;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/**
 * 利润测算表
 */
public interface TbBondProfitMapper {

    Page<TbBondProfit> getBondProfit(RowBounds rb);

    List<TbBondProfit> getBondProfit();

    SlOutBean bondProfitCreate(Map<String, String> map);
}