package com.singlee.hrbnewport.mapper;

import com.singlee.hrbnewport.model.IfsReportTytzBtycr;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author ：chenguo
 * @date ：Created in 2021/12/3 15:37
 * @description：${description}
 * @modified By：
 * @version:
 */
@Mapper
public interface IfsReportTytzBtycrMapper {
    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(IfsReportTytzBtycr record);

    /**
     * insert record to table selective
     *
     * @param record the record
     * @return insert count
     */
    int insertSelective(IfsReportTytzBtycr record);

    List<IfsReportTytzBtycr> getDate(Map<String,Object> map);

    void call(Map<String,Object> map);
}