package com.singlee.hrbnewport.mapper;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportXzqtz;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

/**
 * @author     ：meigy
 * @date       ：Created in 2021/11/6 14:29
 * @description：${description}
 * @modified By：
 * @version:     
 */
public interface IfsReportXzqtzMapper extends Mapper<IfsReportXzqtz> {

    SlOutBean ifsReportXzqtzCreate(Map<String, String> map);
}