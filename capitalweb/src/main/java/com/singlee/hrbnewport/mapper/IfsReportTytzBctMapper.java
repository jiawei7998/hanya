package com.singlee.hrbnewport.mapper;

import com.singlee.hrbnewport.model.IfsReportTytzBct;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author ：chenguo
 * @date ：Created in 2021/12/3 15:37
 * @description：${description}
 * @modified By：
 * @version:
 */
@Mapper
public interface IfsReportTytzBctMapper {
    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(IfsReportTytzBct record);

    /**
     * insert record to table selective
     *
     * @param record the record
     * @return insert count
     */
    int insertSelective(IfsReportTytzBct record);

    List<IfsReportTytzBct> getDateBct(Map<String,Object> map);

    void callBct(Map<String,Object> map);
}