package com.singlee.hrbnewport.mapper;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportG1102;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

public interface IfsReportG1102Mapper extends Mapper<IfsReportG1102> {

    SlOutBean ifsReportG1102Create(Map<String, String> map);
}