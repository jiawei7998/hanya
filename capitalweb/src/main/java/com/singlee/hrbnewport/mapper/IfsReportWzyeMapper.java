package com.singlee.hrbnewport.mapper;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportWzye;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

public interface IfsReportWzyeMapper extends Mapper<IfsReportWzye> {


    SlOutBean ifsReportWzyeCreate(Map<String, String> map);
}