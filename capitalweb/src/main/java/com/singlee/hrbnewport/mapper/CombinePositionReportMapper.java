package com.singlee.hrbnewport.mapper;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.model.CombinePositionVO;
import org.apache.ibatis.session.RowBounds;

import java.util.Map;

public interface CombinePositionReportMapper {

    public Page<CombinePositionVO> selectBond(Map<String,Object> map,RowBounds rb);

    /**
     * 同业存单投资
     * @param map
     * @param rb
     * @return
     */
    public Page<CombinePositionVO> selectCDBond(Map<String,Object> map,RowBounds rb);

    /**
     * 基金申购
     * @param map
     * @param rb
     * @return
     */
    public Page<CombinePositionVO> queryFundPay(Map<String,Object> map,RowBounds rb);

    /**
     * 基金赎回
     * @param map
     * @param rb
     * @return
     */
    public Page<CombinePositionVO> queryFundSell(Map<String,Object> map,RowBounds rb);
}
