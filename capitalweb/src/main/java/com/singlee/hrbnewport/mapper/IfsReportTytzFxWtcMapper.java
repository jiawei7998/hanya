package com.singlee.hrbnewport.mapper;

import com.singlee.hrbnewport.model.IfsReportTytzFxWtc;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 16:21
 * @description：${description}
 * @modified By：
 * @version:   
 */
@Mapper
public interface IfsReportTytzFxWtcMapper {
    /**
     * insert record to table
     * @param record the record
     * @return insert count
     */
    int insert(IfsReportTytzFxWtc record);

    /**
     * insert record to table selective
     * @param record the record
     * @return insert count
     */
    int insertSelective(IfsReportTytzFxWtc record);

    List<IfsReportTytzFxWtc> getDate(Map<String,Object> map);

    void call(Map<String,Object> map);
}