package com.singlee.hrbnewport.mapper;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportImportantCheck;

import java.util.Map;

public interface IfsReportImportantCheckMapper {
    int insert(IfsReportImportantCheck record);

    int insertSelective(IfsReportImportantCheck record);

    Page<IfsReportImportantCheck> getImportantCheck();

    SlOutBean importantCheckReportCreate(Map<String, String> map);
}