package com.singlee.hrbnewport.mapper;

import com.github.pagehelper.Page;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/**
 *同业业务管理报告-买入返售债券业务明细表
 * 2021/12/9
 * @Auther:zk
 */    
    
public interface RhInnerbankRvrepoMapper {
    Page<Map<String,Object>> searchAllPage(Map<String,Object> param, RowBounds rowBounds);

    void createData(Map<String,Object> param);

    List<Map<String, Object>> searchAll(Map<String, Object> map);
}