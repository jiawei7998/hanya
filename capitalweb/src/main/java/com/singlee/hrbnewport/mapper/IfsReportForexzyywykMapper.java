package com.singlee.hrbnewport.mapper;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportForexzyywyk;

import java.util.Map;

/**
 * 外汇自营业务浮盈亏表
 */
public interface IfsReportForexzyywykMapper {
    int insert(IfsReportForexzyywyk record);

    int insertSelective(IfsReportForexzyywyk record);

    Page<IfsReportForexzyywyk> getForexzyywyk();

    SlOutBean ReportForexzyywykCreate(Map<String, String> map);
}