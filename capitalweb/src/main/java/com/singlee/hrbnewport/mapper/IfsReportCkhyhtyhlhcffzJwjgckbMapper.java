package com.singlee.hrbnewport.mapper;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportCkhyhtyhlhcffzJwjgckb;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

public interface IfsReportCkhyhtyhlhcffzJwjgckbMapper extends Mapper<IfsReportCkhyhtyhlhcffzJwjgckb> {

    SlOutBean ifsReportCkhyhtyhlhcffzJwjgckbCreate(Map<String, String> map);
}