package com.singlee.hrbnewport.mapper;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.TbZyzjbalance;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

public interface TbZyzjbalanceMapper {
    int insert(TbZyzjbalance record);

    int insertSelective(TbZyzjbalance record);

    Page<TbZyzjbalance> getTbZYZJBALANCE(RowBounds rb);


    List<TbZyzjbalance> getTbZYZJBALANCE2();

    SlOutBean ZYZJBALANCEReportCreate(Map<String, String> map);
}