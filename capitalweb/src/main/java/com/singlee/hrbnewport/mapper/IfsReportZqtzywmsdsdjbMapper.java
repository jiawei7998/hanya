package com.singlee.hrbnewport.mapper;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportZqtzywmsdsdjb;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

public interface IfsReportZqtzywmsdsdjbMapper extends Mapper<IfsReportZqtzywmsdsdjb> {

    SlOutBean ifsReportZqtzywmsdsdjbCreate(Map<String, String> map);
}