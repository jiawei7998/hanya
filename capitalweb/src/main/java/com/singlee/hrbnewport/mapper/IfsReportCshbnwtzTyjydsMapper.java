package com.singlee.hrbnewport.mapper;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportCshbnwtzTyjyds;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

/**
 * @author ：meigy
 * @date ：Created in 2021/11/16 15:00
 * @description：${description}
 * @modified By：
 * @version:
 */
public interface IfsReportCshbnwtzTyjydsMapper extends Mapper<IfsReportCshbnwtzTyjyds> {

    SlOutBean ifsReportCshbnwtzTyjydsCreate(Map<String, String> map);
}