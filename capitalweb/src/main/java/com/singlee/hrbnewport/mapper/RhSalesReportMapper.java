package com.singlee.hrbnewport.mapper;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import org.apache.ibatis.session.RowBounds;

import java.util.Map;

public interface RhSalesReportMapper {
    //人行金数-调用存储过程生成数据
    SlOutBean RHJSReportCreate(Map<String, Object> map);
    //人行金数-存量同业借贷
    public Page<Map<String,Object>> sql1(RowBounds rb);

    public Page<Map<String,Object>> sql1();

    //人行金数-同业借贷发生额
    public Page<Map<String,Object>> sql2(RowBounds rb);

    public Page<Map<String,Object>> sql2();

    //人行金数-存量同业存款
    public Page<Map<String,Object>> sql3(RowBounds rb );

    public Page<Map<String,Object>> sql3();

    //人行金数-同业存款发生额
    public Page<Map<String,Object>> sql4(RowBounds rb );

    public Page<Map<String,Object>> sql4();

    //人行金数-同业客户基本信息
    public Page<Map<String,Object>> sql5(RowBounds rb );
    public Page<Map<String,Object>> sql5( );

    //人行金数-存量债券信息
    public Page<Map<String,Object>> sql6(RowBounds rb );
    public Page<Map<String,Object>> sql6();

    //人行金数-债券投资发生额信息
    public Page<Map<String,Object>> sql7(RowBounds rb );
    public Page<Map<String,Object>> sql7();

    //人行金数-存量债券发行信息
    public Page<Map<String,Object>> sql8(RowBounds rb );
    public Page<Map<String,Object>> sql8( );

    //人行金数-债券发行发生额信息
    public Page<Map<String,Object>> sql9(RowBounds rb );
    public Page<Map<String,Object>> sql9();

    //人行金数-存量特定目的载体投资信息
    public Page<Map<String,Object>> sql10(RowBounds rb );
    public Page<Map<String,Object>> sql10();

    //人行金数-特定目的载体投资发生额信息
    public Page<Map<String,Object>> sql11(RowBounds rb );
    public Page<Map<String,Object>> sql11();

}
