package com.singlee.hrbnewport.mapper;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportCshmyyhzyjgzbqkb;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

public interface IfsReportCshmyyhzyjgzbqkbMapper extends Mapper<IfsReportCshmyyhzyjgzbqkb> {

    SlOutBean ifsReportCshmyyhzyjgzbqkbCreate(Map<String, String> map);
}