package com.singlee.hrbnewport.mapper;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;

import java.util.Map;

public interface ReportDptotalMapper {

    public Page<Map<String ,Object>> getReportDptotal();


    SlOutBean DptotalCreate(Map<String, Object> map);
}
