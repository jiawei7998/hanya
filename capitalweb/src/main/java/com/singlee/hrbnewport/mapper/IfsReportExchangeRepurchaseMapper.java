package com.singlee.hrbnewport.mapper;

import com.github.pagehelper.Page;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/**
 * @author copysun
 */
public interface IfsReportExchangeRepurchaseMapper {
    Page<Map<String, Object>> getExportPageData(Map<String, Object> map, RowBounds rowBounds);
    List<Map<String, Object>> getExportPageData(Map<String, Object> map);
    void generateData(Map<String, Object> map);
}