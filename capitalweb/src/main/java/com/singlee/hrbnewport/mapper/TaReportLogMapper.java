package com.singlee.hrbnewport.mapper;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.model.TaReportLog;
import org.apache.ibatis.session.RowBounds;

import java.util.Map;

public interface TaReportLogMapper {
	int insert(TaReportLog record);

	int insertSelective(TaReportLog record);

	Page<Map<String, Object>> getPageList(Map<String, Object> map, RowBounds rowBounds);
}