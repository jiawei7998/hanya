package com.singlee.hrbnewport.mapper;

import com.singlee.hrbnewport.model.IfsReportTytzJcWct;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 16:38
 * @description：${description}
 * @modified By：
 * @version:   
 */
@Mapper
public interface IfsReportTytzJcWctMapper {
    /**
     * insert record to table
     * @param record the record
     * @return insert count
     */
    int insert(IfsReportTytzJcWct record);

    /**
     * insert record to table selective
     * @param record the record
     * @return insert count
     */
    int insertSelective(IfsReportTytzJcWct record);

    List<IfsReportTytzJcWct> getDate(Map<String,Object> map);

    void call(Map<String,Object> map);
}