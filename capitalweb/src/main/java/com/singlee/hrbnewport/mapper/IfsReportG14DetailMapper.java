package com.singlee.hrbnewport.mapper;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.model.IfsReportG14Detail;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/**
 *
 * 2021/12/7
 * @Auther:zk
 */    
    
public interface IfsReportG14DetailMapper {

    Page<IfsReportG14Detail> searchAllPage(Map<String, Object> param, RowBounds rowBounds);

    void createData(Map<String,Object> map);

    List<IfsReportG14Detail> searchAll(Map<String, Object> param);
}