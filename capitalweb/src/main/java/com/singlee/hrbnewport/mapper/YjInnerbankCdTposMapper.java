package com.singlee.hrbnewport.mapper;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.model.YjInnerbankCdTpos;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/**
 * 同业业务管理报告-同业存单投资
 * 2021/12/8
 * @Auther:zk
 */    
    
public interface YjInnerbankCdTposMapper {
    Page<YjInnerbankCdTpos> searchAllPage(Map<String,Object> param, RowBounds rowBounds);

    void createData(Map<String,Object> map);

    List<YjInnerbankCdTpos> searchAll(Map<String, Object> param);
}