package com.singlee.hrbnewport.mapper;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.model.IfsReportCdPosition;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/**
 *同业存单
 * 2021/12/8
 * @Auther:zk
 */    
    
public interface IfsReportCdPositionMapper {

    Page<IfsReportCdPosition> searchAllPage(Map<String,Object> param, RowBounds rowBounds);

    void createData(Map<String,Object> map);

    List<IfsReportCdPosition> searchAll(Map<String, Object> map);
}