package com.singlee.hrbnewport.mapper;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportCkhyhtyhlhcffzFhsc;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

public interface IfsReportCkhyhtyhlhcffzFhscMapper extends Mapper<IfsReportCkhyhtyhlhcffzFhsc> {

    SlOutBean ifsReportCkhyhtyhlhcffzFhscCreate(Map<String, String> map);
}