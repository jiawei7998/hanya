package com.singlee.hrbnewport.mapper;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportCshbnwtzZtctq;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

/**
 * @author     ：meigy
 * @date       ：Created in 2021/11/9 13:19
 * @description：${description}
 * @modified By：
 * @version:     
 */
public interface IfsReportCshbnwtzZtctqMapper extends Mapper<IfsReportCshbnwtzZtctq> {

    SlOutBean ifsReportCshbnwtzZtctqCreate(Map<String, String> map);
}