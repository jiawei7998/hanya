package com.singlee.hrbnewport.mapper;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportG2400;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

public interface IfsReportG2400Mapper extends Mapper<IfsReportG2400> {

    SlOutBean ifsReportG2400Create(Map<String, String> map);
}