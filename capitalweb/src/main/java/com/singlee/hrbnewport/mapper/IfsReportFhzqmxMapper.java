package com.singlee.hrbnewport.mapper;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportFhzqmx;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

public interface IfsReportFhzqmxMapper extends Mapper<IfsReportFhzqmx> {

    SlOutBean ifsReportFhzqmxCreate(Map<String, String> map);
}