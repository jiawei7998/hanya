package com.singlee.hrbnewport.mapper;

import java.util.List;
import java.util.Map;

public interface NewBondInvestReportMapper {
	
	public List<Map<String,Object>>   getSql1(Map<String,Object> map);
	
	public List<Map<String,Object>>   getSql2(Map<String,Object> map);
	
	public List<Map<String,Object>>   getSql3(Map<String,Object> map);
	
	public List<Map<String,Object>>   getSql4(Map<String,Object> map);
	
	public List<Map<String,Object>>   getSql5(Map<String,Object> map);
	
	public List<Map<String,Object>>   getSql6(Map<String,Object> map);

	public List<Map<String,Object>>   getSql7(Map<String,Object> map);
	
	public List<Map<String,Object>>   getSql8(Map<String,Object> map);
	
	public List<Map<String,Object>>   getSql9(Map<String,Object> map);
	
	public List<Map<String,Object>>   getSql10(Map<String,Object> map);
	
	public List<Map<String,Object>>   getSql11(Map<String,Object> map);
	
	public List<Map<String,Object>>   getSql12(Map<String,Object> map);

}
