package com.singlee.hrbnewport.mapper;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportJrscbywglZqtzsyazh;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

public interface IfsReportJrscbywglZqtzsyazhMapper extends Mapper<IfsReportJrscbywglZqtzsyazh> {

    SlOutBean ifsReportJrscbywglZqtzsyazhCreate(Map<String, String> map);
}