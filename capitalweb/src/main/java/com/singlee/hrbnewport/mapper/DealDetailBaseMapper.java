package com.singlee.hrbnewport.mapper;

import com.github.pagehelper.Page;
import org.apache.ibatis.session.RowBounds;
import org.apache.poi.ss.formula.functions.T;

import java.util.List;
import java.util.Map;

/**
 * 2021/12/7
 *
 * @Auther:zk
 */
public interface DealDetailBaseMapper {

    Page<T> searAllPage(Map<String,Object> param, RowBounds rowBounds);

    void createData(Map<String,Object> param);

    List<T> searAll(Map<String, Object> map);

}
