package com.singlee.hrbnewport.mapper;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.TbBondInvestment;

import java.util.List;
import java.util.Map;

/**
 * 债券投资结构报表
 */
public interface TbBondInvestmentMapper {


    List<TbBondInvestment> getBondInvestment();

    SlOutBean bondInvestmentCreate(Map<String, String> map);
}