package com.singlee.hrbnewport.mapper;

import java.util.List;
import java.util.Map;

public interface FundTposReportMapper {


	/**
	 * 货币基金日报
	 *
	 * @param map
	 * @return
	 */
	List<Map<String, Object>> getExportData(Map<String, Object> map);



	void generateData(Map<String, Object> map);
}