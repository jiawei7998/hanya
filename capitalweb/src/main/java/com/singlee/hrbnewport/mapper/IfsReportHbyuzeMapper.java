package com.singlee.hrbnewport.mapper;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportHbyuze;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

public interface IfsReportHbyuzeMapper {
    int insert(IfsReportHbyuze record);

    int insertSelective(IfsReportHbyuze record);

    Page<IfsReportHbyuze> getIfsReportHbyuze(RowBounds rb);

    List<IfsReportHbyuze> getIfsReportHbyuze();

    SlOutBean ifsReportHbyuzeCreate(Map<String, String> map);
}