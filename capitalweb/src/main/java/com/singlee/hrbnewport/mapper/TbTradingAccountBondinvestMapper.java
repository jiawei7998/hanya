package com.singlee.hrbnewport.mapper;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.TbTradingAccountBondinvest;

import java.util.Map;

/**
 * 交易账户债券投资结构情况
 */
public interface TbTradingAccountBondinvestMapper {


    Page<TbTradingAccountBondinvest> getAccountBondInvest();

    SlOutBean accountBondInvestCreate(Map<String, String> map);
}