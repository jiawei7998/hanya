package com.singlee.hrbnewport.mapper;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.TbLendingLedger;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

public interface TbLendingLedgerMapper {


    Page<TbLendingLedger> getLending(RowBounds rb);
    
    List<TbLendingLedger> getLending();

    SlOutBean lendingReportCreate(Map<String, String> map);
}