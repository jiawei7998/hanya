package com.singlee.hrbnewport.mapper;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportJrscbywglZqtzdq;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

public interface IfsReportJrscbywglZqtzdqMapper extends Mapper<IfsReportJrscbywglZqtzdq> {

    SlOutBean ifsReportJrscbywglZqtzdqCreate(Map<String, String> map);
}