package com.singlee.hrbnewport.mapper;

import java.util.List;
import java.util.Map;

public interface PositionReportMapper {

	List<Map<String,Object>> getExportData(Map<String,Object> map);

	void generateExportData(Map<String,Object> map);
	
	
}
