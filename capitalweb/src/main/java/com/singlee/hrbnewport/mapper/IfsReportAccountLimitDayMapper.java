package com.singlee.hrbnewport.mapper;

import java.util.List;
import java.util.Map;

public interface IfsReportAccountLimitDayMapper {
    List<Map<String,Object>> getData(Map<String,Object> map);

    void generateData(Map<String,Object> map);
}