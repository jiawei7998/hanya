package com.singlee.hrbnewport.mapper;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsForexZyck;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/**
 * 外汇自营敞口情况表
 */
public interface IfsForexZyckMapper {

    Page<IfsForexZyck> getForexZyck(RowBounds rb);

    List<IfsForexZyck> getForexZyck();

    SlOutBean ReportForexZyckCreate(Map<String, String> map);
}