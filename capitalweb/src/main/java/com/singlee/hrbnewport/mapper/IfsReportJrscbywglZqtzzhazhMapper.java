package com.singlee.hrbnewport.mapper;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportJrscbywglZqtzzhazh;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

public interface IfsReportJrscbywglZqtzzhazhMapper extends Mapper<IfsReportJrscbywglZqtzzhazh> {

    SlOutBean ifsReportJrscbywglZqtzzhazhCreate(Map<String, String> map);
}