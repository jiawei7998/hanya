package com.singlee.hrbnewport.mapper;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.model.YjInnerbankCdTpos;
import com.singlee.hrbnewport.model.YjInnerbankDeposit;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/**
 *同业业务管理报告-人名币拆借/存款
 * 2021/12/8
 * @Auther:zk
 */    
    
public interface YjInnerbankDepositMapper {
    Page<YjInnerbankDeposit> searchAllPage(Map<String,Object> param, RowBounds rowBounds);

    void createData(Map<String,Object> map);

    List<YjInnerbankDeposit> searchAll(Map<String, Object> param);
}