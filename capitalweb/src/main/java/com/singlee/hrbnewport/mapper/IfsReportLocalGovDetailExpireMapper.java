package com.singlee.hrbnewport.mapper;

import com.github.pagehelper.Page;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

public interface IfsReportLocalGovDetailExpireMapper {
    Page<Map<String,Object>> getData(Map<String,Object> map, RowBounds rowBounds);
    void generateData(Map<String,Object> map);
    List<Map<String,Object>> getData(Map<String,Object> map);
}