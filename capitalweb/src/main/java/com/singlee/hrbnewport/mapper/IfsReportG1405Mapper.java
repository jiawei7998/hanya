package com.singlee.hrbnewport.mapper;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.model.IfsReportG1405;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/**
 * G1405-大额风险暴露
 * @Author zhangkai
 * @Description 
 * @Date 
 */
public interface IfsReportG1405Mapper {

    Page<IfsReportG1405> searchAllPage(Map<String,Object> map, RowBounds rb);

    void createG1405Report(Map<String , Object> map);

    List<IfsReportG1405> searchAll(Map<String, Object> map);
}