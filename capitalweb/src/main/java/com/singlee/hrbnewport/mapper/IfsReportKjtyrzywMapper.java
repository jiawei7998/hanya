package com.singlee.hrbnewport.mapper;

import com.singlee.hrbnewport.model.IfsReportKjtyrzyw;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 20:50
 * @description：${description}
 * @modified By：
 * @version:   
 */
public interface IfsReportKjtyrzywMapper extends Mapper<IfsReportKjtyrzyw> {
    /**
     * insert record to table
     * @param record the record
     * @return insert count
     */
    int insert(IfsReportKjtyrzyw record);

    /**
     * insert record to table selective
     * @param record the record
     * @return insert count
     */
    int insertSelective(IfsReportKjtyrzyw record);

    List<IfsReportKjtyrzyw> getDate(Map<String,Object> map);

    void call(Map<String,Object> map);
}