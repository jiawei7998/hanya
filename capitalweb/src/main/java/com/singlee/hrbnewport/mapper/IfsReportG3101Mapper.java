package com.singlee.hrbnewport.mapper;
import org.apache.ibatis.annotations.Param;
import java.util.List;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportG3101;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

public interface IfsReportG3101Mapper extends Mapper<IfsReportG3101> {

    SlOutBean ifsReportG3101Create(Map<String, String> map);

    List<IfsReportG3101> selectByProductExcel();

}