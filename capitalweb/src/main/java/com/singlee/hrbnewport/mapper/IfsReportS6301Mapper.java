package com.singlee.hrbnewport.mapper;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportS6301;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

public interface IfsReportS6301Mapper extends Mapper<IfsReportS6301> {

    SlOutBean ifsReportS6301Create(Map<String, String> map);
}