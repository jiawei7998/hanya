package com.singlee.hrbnewport.mapper;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.hrbnewport.model.IfsReportJrscbywglZqtzzhafx;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

public interface IfsReportJrscbywglZqtzzhafxMapper extends Mapper<IfsReportJrscbywglZqtzzhafx> {

    SlOutBean ifsReportJrscbywglZqtzzhafxCreate(Map<String, String> map);
}