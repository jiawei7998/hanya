package com.singlee.hrbnewport.mapper;

import com.github.pagehelper.Page;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/**
 *同业业务管理报告-同业存单发行投资业务明细表
 * 2021/12/9
 * @Auther:zk
 */    
    
public interface RhInnerbankCdIssMapper {

    Page<Map<String,Object>> searchAllPage(Map<String,Object> param, RowBounds rowBounds);

    List<Map<String,Object>> searchAll(Map<String,Object> param);

    void createData(Map<String,Object> param);

}