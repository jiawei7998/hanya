package com.singlee.hrbnewport.mapper;

import com.singlee.hrbnewport.model.IfsReportTytzJcWtycr;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author ：chenguo
 * @date ：Created in 2021/12/6 16:37
 * @description：${description}
 * @modified By：
 * @version:
 */
@Mapper
public interface IfsReportTytzJcWtycrMapper {
    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(IfsReportTytzJcWtycr record);

    /**
     * insert record to table selective
     *
     * @param record the record
     * @return insert count
     */
    int insertSelective(IfsReportTytzJcWtycr record);

    List<IfsReportTytzJcWtycr> getDate(Map<String,Object> map);

    void call(Map<String,Object> map);
}