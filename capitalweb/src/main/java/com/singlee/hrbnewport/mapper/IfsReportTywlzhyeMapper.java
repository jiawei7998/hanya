package com.singlee.hrbnewport.mapper;

import com.singlee.hrbnewport.model.IfsReportTywlzhye;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/6 20:51
 * @description：${description}
 * @modified By：
 * @version:   
 */

public interface IfsReportTywlzhyeMapper  extends Mapper<IfsReportTywlzhye> {
    /**
     * insert record to table
     * @param record the record
     * @return insert count
     */
    int insert(IfsReportTywlzhye record);

    /**
     * insert record to table selective
     * @param record the record
     * @return insert count
     */
    int insertSelective(IfsReportTywlzhye record);

    List<IfsReportTywlzhye> getDate(Map<String,Object> map);

    void call(Map<String,Object> map);
}