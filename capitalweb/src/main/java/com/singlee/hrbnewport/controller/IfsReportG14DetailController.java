package com.singlee.hrbnewport.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.hrbnewport.model.IfsReportG14Detail;
import com.singlee.hrbnewport.service.IfsReportG14DetailSrevice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * 2021/12/7
 *
 * @Auther:zk
 */
@Controller
@RequestMapping("/IfsReportG14DetailController")
public class IfsReportG14DetailController {

    @Autowired
    IfsReportG14DetailSrevice ifsReportG14DetailSrevice;

    @ResponseBody
    @RequestMapping("/searchAllPage")
    public RetMsg<PageInfo<IfsReportG14Detail>> searchAllPage(@RequestBody Map<String,Object> map){
        Page<IfsReportG14Detail> ifsReportG14Details = ifsReportG14DetailSrevice.searchAllPage(map);
        return RetMsgHelper.ok(ifsReportG14Details);
    }

}
