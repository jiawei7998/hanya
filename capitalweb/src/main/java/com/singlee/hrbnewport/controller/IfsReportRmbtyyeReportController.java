package com.singlee.hrbnewport.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.SlErrors;
import com.singlee.hrbnewport.model.IfsReportRmbtyye;
import com.singlee.hrbnewport.service.IfsReportRmbtyyeReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;


/**
 * 人民币同业余额
 */
@Controller
@RequestMapping(value = "/IfsReportRmbtyyeReportController")
public class IfsReportRmbtyyeReportController extends CommonController {

    @Autowired
    private IfsReportRmbtyyeReportService ifsReportRmbtyyeReportService;

    @ResponseBody
    @RequestMapping(value = "/searchIfsReportRmbtyyeReportPage")
    public RetMsg<PageInfo<IfsReportRmbtyye>> searchIfsReportRmbtyyeReportPage(@RequestBody Map<String, String> map) {
        Page<IfsReportRmbtyye> page = ifsReportRmbtyyeReportService.searchIfsReportRmbtyyePage(map);
        return RetMsgHelper.ok(page);
    }

    @ResponseBody
    @RequestMapping(value = "/createIfsReportRmbtyye")
    public RetMsg<SlOutBean> createIfsReportRmbtyye(@RequestBody Map<String, String> params) {
        SlOutBean msg = new SlOutBean();
        try {
            msg = ifsReportRmbtyyeReportService.ifsReportRmbtyyeCreate(params);
        } catch (RemoteConnectFailureException e) {
            msg.setRetCode(SlErrors.FAILED_RMI_OPICS.getErrCode());
            msg.setRetMsg(SlErrors.FAILED_RMI_OPICS.getErrMsg() + ":" + e.getMessage() + "\n");
            msg.setRetStatus(RetStatusEnum.F);
        } catch (Exception e) {
            msg.setRetCode(SlErrors.FAILED.getErrCode());
            msg.setRetMsg(SlErrors.FAILED.getErrMsg() + ":" + e.getMessage() + "\n");
            msg.setRetStatus(RetStatusEnum.F);
        }
        return RetMsgHelper.ok(msg);
    }

}
