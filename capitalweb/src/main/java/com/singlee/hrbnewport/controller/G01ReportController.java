package com.singlee.hrbnewport.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.hrbnewport.model.G01ReportVO;
import com.singlee.hrbnewport.service.G01ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * @author Liang
 * @date 2021/11/11 15:01
 * =======================
 */
@Controller
@RequestMapping(value = "/G01ReportController")
public class G01ReportController extends CommonController {
    @Autowired
    private G01ReportService g01ReportService;

    @ResponseBody
    @RequestMapping(value = "/searchg01ReportPage")
    public RetMsg<PageInfo<G01ReportVO>> searchg01ReportPage(@RequestBody Map<String, String> map) {
        String datadate = ParameterUtil.getString(map,"queryDate","");
        if(StringUtil.isNullOrEmpty(datadate)) {
            JY.raise("缺少参数......");
        }
        map.put("DATADATE", datadate);
        Page<G01ReportVO> reportDptotalPage = g01ReportService.getG01Report(map);
        return RetMsgHelper.ok(reportDptotalPage);
    }
}
