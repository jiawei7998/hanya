package com.singlee.hrbnewport.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.hrbnewport.model.IfsReportG1405;
import com.singlee.hrbnewport.service.IfsReportG1405Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.Map;

/**
 * G1405-大额风险暴露
 * by zk
 */
@Controller
@RequestMapping("/IfsReportG1405Controller")
public class IfsReportG1405Controller {

    @Autowired
    IfsReportG1405Service ifsReportG1405Service;

    /**
     * 生成报表数据
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping("/creatG1405")
    public RetMsg<Serializable> creatG1405(@RequestBody Map<String,Object> param){
        ifsReportG1405Service.creatG1405(param);
        return RetMsgHelper.ok();
    }

    /**
     * 查询亦生成的数据
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping("searchIfsReportG1405")
    public RetMsg<PageInfo<IfsReportG1405>> searchIfsReportG1405(@RequestBody Map<String,Object> param){
        Page<IfsReportG1405> ifsReportG1405s = ifsReportG1405Service.searchIfsReportG1405(param);
        return RetMsgHelper.ok(ifsReportG1405s);
    }

}
