package com.singlee.hrbnewport.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbnewport.model.*;
import com.singlee.hrbnewport.service.*;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author ：chenguo
 * @date ：Created in 2021/12/3 18:58
 * @description：哈尔滨银行本外币同业台账-金融市场部-给资负
 * @modified By：
 * @version:
 */
@Controller
@RequestMapping("/TheForeignCurrencyFinancialZFController")
public class TheForeignCurrencyFinancialZFController {


    @Resource
    IfsReportTytzZfBctService ifsReportTytzZfBctService;
    @Resource
    IfsReportTytzZfBtcService ifsReportTytzZfBtcService;
    @Resource
    IfsReportTytzZfBtyccService ifsReportTytzZfBtyccService;
    @Resource
    IfsReportTytzZfBttycrService ifsReportTytzZfBttycrService;
    @Resource
    IfsReportTytzZfWctService ifsReportTytzZfWctService;
    @Resource
    IfsReportTytzZfWtcService ifsReportTytzZfWtcService;

    //外币-存放同业
    @Resource
    IfsReportTytzZfBcftyService ifsReportTytzZfBcftyService;

    @Resource
    IfsReportTytzZfWtycrService ifsReportTytzZfWtycrService;




        /*
        * 本币存放同业
        * */
    @ResponseBody
    @RequestMapping("/call2")
    public RetMsg<PageInfo<IfsReportTytzZfBct>> call2(@RequestBody Map<String ,Object> map){
        map.put("intodates", ParameterUtil.getString(map, "queryDate", DateUtil.getCurrentDateAsString()));
        List<IfsReportTytzZfBct> list= ifsReportTytzZfBctService.getDate(map);
        int pageNum = ParameterUtil.getInt(map,"pageNumber",1);
        int pageSize =ParameterUtil.getInt(map,"pageSize",30);
        Page<IfsReportTytzZfBct> page=HrbReportUtils.producePage(list,pageNum,pageSize);
        return RetMsgHelper.ok(page);
    }
    /*
     * 本币同业存放
     * */
    @ResponseBody
    @RequestMapping("/call1")
    public RetMsg<PageInfo<IfsReportTytzZfBtc>> call1(@RequestBody Map<String ,Object> map){
        map.put("intodates", ParameterUtil.getString(map, "queryDate", DateUtil.getCurrentDateAsString()));
        List<IfsReportTytzZfBtc> list= ifsReportTytzZfBtcService.getDate(map);
        int pageNum = ParameterUtil.getInt(map,"pageNumber",1);
        int pageSize =ParameterUtil.getInt(map,"pageSize",30);
        Page<IfsReportTytzZfBtc> page=HrbReportUtils.producePage(list,pageNum,pageSize);
        return RetMsgHelper.ok(page);
    }



    /*
     * 本币同业拆出
     * */
    @ResponseBody
    @RequestMapping("/call4")
    public RetMsg<PageInfo<IfsReportTytzZfBtycc>> call4(@RequestBody Map<String ,Object> map){
        map.put("intodates", ParameterUtil.getString(map, "queryDate", DateUtil.getCurrentDateAsString()));
        List<IfsReportTytzZfBtycc> list= ifsReportTytzZfBtyccService.getDate(map);
        int pageNum = ParameterUtil.getInt(map,"pageNumber",1);
        int pageSize =ParameterUtil.getInt(map,"pageSize",30);
        Page<IfsReportTytzZfBtycc> page=HrbReportUtils.producePage(list,pageNum,pageSize);
        return RetMsgHelper.ok(page);
    }


    /*
     * 本币同业拆入
     * */
    @ResponseBody
    @RequestMapping("/call3")
    public RetMsg<PageInfo<IfsReportTytzZfBttycr>> call3(@RequestBody Map<String ,Object> map){
        map.put("intodates", ParameterUtil.getString(map, "queryDate", DateUtil.getCurrentDateAsString()));
        List<IfsReportTytzZfBttycr> list= ifsReportTytzZfBttycrService.getDate(map);
        int pageNum = ParameterUtil.getInt(map,"pageNumber",1);
        int pageSize =ParameterUtil.getInt(map,"pageSize",30);
        Page<IfsReportTytzZfBttycr> page=HrbReportUtils.producePage(list,pageNum,pageSize);
        return RetMsgHelper.ok(page);
    }


    /*
     * 外币存放同业
     * */
    @ResponseBody
    @RequestMapping("/call6")
    public RetMsg<PageInfo<IfsReportTytzZfWct>> call6(@RequestBody Map<String ,Object> map){
        map.put("intodates", ParameterUtil.getString(map, "queryDate", DateUtil.getCurrentDateAsString()));
        List<IfsReportTytzZfWct> list= ifsReportTytzZfWctService.getDate(map);
        int pageNum = ParameterUtil.getInt(map,"pageNumber",1);
        int pageSize =ParameterUtil.getInt(map,"pageSize",30);
        Page<IfsReportTytzZfWct> page=HrbReportUtils.producePage(list,pageNum,pageSize);
        return RetMsgHelper.ok(page);
    }


    /*
     * 外币同业存放
     * */
    @ResponseBody
    @RequestMapping("/call5")
    public RetMsg<PageInfo<IfsReportTytzZfWtc>> call5(@RequestBody Map<String ,Object> map){
        map.put("intodates", ParameterUtil.getString(map, "queryDate", DateUtil.getCurrentDateAsString()));
        List<IfsReportTytzZfWtc> list= ifsReportTytzZfWtcService.getDate(map);
        int pageNum = ParameterUtil.getInt(map,"pageNumber",1);
        int pageSize =ParameterUtil.getInt(map,"pageSize",30);
        Page<IfsReportTytzZfWtc> page=HrbReportUtils.producePage(list,pageNum,pageSize);
        return RetMsgHelper.ok(page);
    }


    /*
     * 外币同业拆出
     * */
    @ResponseBody
    @RequestMapping("/call8")
    public RetMsg<PageInfo<IfsReportTytzZfBcfty>> call8(@RequestBody Map<String ,Object> map){
        map.put("intodates", ParameterUtil.getString(map, "queryDate", DateUtil.getCurrentDateAsString()));
        List<IfsReportTytzZfBcfty> list= ifsReportTytzZfBcftyService.getDate(map);
        int pageNum = ParameterUtil.getInt(map,"pageNumber",1);
        int pageSize =ParameterUtil.getInt(map,"pageSize",30);
        Page<IfsReportTytzZfBcfty> page=HrbReportUtils.producePage(list,pageNum,pageSize);
        return RetMsgHelper.ok(page);
    }

    /*
     * 外币同业拆入
     * */
    @ResponseBody
    @RequestMapping("/call7")
    public RetMsg<PageInfo<IfsReportTytzZfWtycr>> call7(@RequestBody Map<String ,Object> map){
        map.put("intodates", ParameterUtil.getString(map, "queryDate", DateUtil.getCurrentDateAsString()));
        List<IfsReportTytzZfWtycr> list= ifsReportTytzZfWtycrService.getDate(map);
        int pageNum = ParameterUtil.getInt(map,"pageNumber",1);
        int pageSize =ParameterUtil.getInt(map,"pageSize",30);
        Page<IfsReportTytzZfWtycr> page=HrbReportUtils.producePage(list,pageNum,pageSize);
        return RetMsgHelper.ok(page);
    }



}
