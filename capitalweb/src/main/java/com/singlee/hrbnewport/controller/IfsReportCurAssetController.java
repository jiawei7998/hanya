package com.singlee.hrbnewport.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.hrbnewport.model.IfsReportCurAsset;
import com.singlee.hrbnewport.service.IfsReportCurAssetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * 哈尔滨银行优质流动性资产台账
 * 2021/12/9
 * @Auther:zk
 */
@Controller
@RequestMapping("/IfsReportCurAssetController")
public class IfsReportCurAssetController {

    @Autowired
    IfsReportCurAssetService ifsReportCurAssetService;

    @ResponseBody
    @RequestMapping("/searchAllPage")
    public RetMsg<PageInfo<IfsReportCurAsset>> searchAllPage(@RequestBody Map<String,Object> param){
        Page<IfsReportCurAsset> ifsReportCurAssets = ifsReportCurAssetService.searchAllPage(param);
        return RetMsgHelper.ok(ifsReportCurAssets);
    }
}
