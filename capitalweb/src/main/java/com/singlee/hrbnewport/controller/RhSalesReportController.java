package com.singlee.hrbnewport.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.hrbnewport.service.RhSalesReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * 人行金数报表
 * @author Liang
 * @date 2021/11/9 14:40
 * =======================
 */
@Controller
@RequestMapping(value = "/RhSalesReportController")
public class RhSalesReportController extends CommonController {

    @Autowired
    private RhSalesReportService rhSalesReportService;

    @ResponseBody
    @RequestMapping(value = "/searchRhSalesReportPage")
    public RetMsg<PageInfo<Map<String, Object>>> searchRhSalesReportPage(@RequestBody Map<String, String> map) {

        Page<Map<String, Object>> page = rhSalesReportService.getrhSalesReportMap(map);
        return RetMsgHelper.ok(page);
    }
}
