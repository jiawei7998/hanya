package com.singlee.hrbnewport.controller;


import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.hrbnewport.service.CommonExportService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

/**
 * @author copysun
 */
@Controller
@RequestMapping("/ExportDataController")
public class ExportDataController {

	@ResponseBody
	@RequestMapping("/getExportData")
	public RetMsg<Map<String,Object>> getExportData(@RequestBody Map<String ,Object> map){
		map.put("queryDate",map.get("queryDate").toString());
		CommonExportService commonExportService= SpringContextHolder.getBean(map.get("implClass").toString());
		Map<String,Object> res=commonExportService.getExportData(map);
		return RetMsgHelper.ok(res);
	}

	@ResponseBody
	@RequestMapping("/getExportListData")
	public RetMsg<List<Map<String,Object>>> getExportListData(@RequestBody Map<String ,Object> map){
		map.put("queryDate",map.get("queryDate").toString());
		CommonExportService commonExportService= SpringContextHolder.getBean(map.get("implClass").toString());
		List<Map<String,Object>> resList=commonExportService.getExportListData(map);
		return RetMsgHelper.ok(resList);
	}

	@ResponseBody
	@RequestMapping("/getExportPageData")
	public RetMsg<PageInfo<Map<String,Object>>> getExportPageData(@RequestBody Map<String ,Object> map){
		CommonExportService commonExportService= SpringContextHolder.getBean(map.get("implClass").toString());
		Page<Map<String, Object>> resPageData = commonExportService.getExportPageData(map);
		return RetMsgHelper.ok(resPageData);
	}

}
