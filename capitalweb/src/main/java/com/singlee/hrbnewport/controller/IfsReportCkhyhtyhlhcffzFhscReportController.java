package com.singlee.hrbnewport.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.SlErrors;
import com.singlee.hrbnewport.model.IfsReportCkhyhtyhlhcffzFhsc;
import com.singlee.hrbnewport.service.IfsReportCkhyhtyhlhcffzFhscReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;


/**
 * 存款含银行同业和联行存放负债 分行上传表
 */
@Controller
@RequestMapping(value = "/IfsReportCkhyhtyhlhcffzFhscReportController")
public class IfsReportCkhyhtyhlhcffzFhscReportController extends CommonController {

    @Autowired
    private IfsReportCkhyhtyhlhcffzFhscReportService ifsReportCkhyhtyhlhcffzFhscReportService;

    @ResponseBody
    @RequestMapping(value = "/searchIfsReportCkhyhtyhlhcffzFhscReportPage")
    public RetMsg<PageInfo<IfsReportCkhyhtyhlhcffzFhsc>> searchIfsReportCkhyhtyhlhcffzFhscReportPage(@RequestBody Map<String, String> map) {
        Page<IfsReportCkhyhtyhlhcffzFhsc> page = ifsReportCkhyhtyhlhcffzFhscReportService.searchIfsReportCkhyhtyhlhcffzFhscPage(map);
        return RetMsgHelper.ok(page);
    }

    @ResponseBody
    @RequestMapping(value = "/createIfsReportCkhyhtyhlhcffzFhsc")
    public RetMsg<SlOutBean> createIfsReportCkhyhtyhlhcffzFhsc(@RequestBody Map<String, String> params) {
        SlOutBean msg = new SlOutBean();
        try {
            msg = ifsReportCkhyhtyhlhcffzFhscReportService.ifsReportCkhyhtyhlhcffzFhscCreate(params);
        } catch (RemoteConnectFailureException e) {
            msg.setRetCode(SlErrors.FAILED_RMI_OPICS.getErrCode());
            msg.setRetMsg(SlErrors.FAILED_RMI_OPICS.getErrMsg() + ":" + e.getMessage() + "\n");
            msg.setRetStatus(RetStatusEnum.F);
        } catch (Exception e) {
            msg.setRetCode(SlErrors.FAILED.getErrCode());
            msg.setRetMsg(SlErrors.FAILED.getErrMsg() + ":" + e.getMessage() + "\n");
            msg.setRetStatus(RetStatusEnum.F);
        }
        return RetMsgHelper.ok(msg);
    }

}
