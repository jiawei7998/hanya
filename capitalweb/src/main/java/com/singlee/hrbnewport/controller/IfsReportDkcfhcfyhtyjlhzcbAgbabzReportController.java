package com.singlee.hrbnewport.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.SlErrors;
import com.singlee.hrbnewport.model.IfsReportDkcfhcfyhtyjlhzcbAgbabz;
import com.singlee.hrbnewport.service.IfsReportDkcfhcfyhtyjlhzcbAgbabzReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;


/**
 * 贷款拆放含拆放银行同业及联行资产表 按国别按币种
 */
@Controller
@RequestMapping(value = "/IfsReportDkcfhcfyhtyjlhzcbAgbabzReportController")
public class IfsReportDkcfhcfyhtyjlhzcbAgbabzReportController extends CommonController {

    @Autowired
    private IfsReportDkcfhcfyhtyjlhzcbAgbabzReportService ifsReportDkcfhcfyhtyjlhzcbAgbabzReportService;

    @ResponseBody
    @RequestMapping(value = "/searchIfsReportDkcfhcfyhtyjlhzcbAgbabzReportPage")
    public RetMsg<PageInfo<IfsReportDkcfhcfyhtyjlhzcbAgbabz>> searchIfsReportDkcfhcfyhtyjlhzcbAgbabzReportPage(@RequestBody Map<String, String> map) {
        Page<IfsReportDkcfhcfyhtyjlhzcbAgbabz> page = ifsReportDkcfhcfyhtyjlhzcbAgbabzReportService.searchIfsReportDkcfhcfyhtyjlhzcbAgbabzPage(map);
        return RetMsgHelper.ok(page);
    }

    @ResponseBody
    @RequestMapping(value = "/createIfsReportDkcfhcfyhtyjlhzcbAgbabz")
    public RetMsg<SlOutBean> createIfsReportDkcfhcfyhtyjlhzcbAgbabz(@RequestBody Map<String, String> params) {
        SlOutBean msg = new SlOutBean();
        try {
            msg = ifsReportDkcfhcfyhtyjlhzcbAgbabzReportService.ifsReportDkcfhcfyhtyjlhzcbAgbabzCreate(params);
        } catch (RemoteConnectFailureException e) {
            msg.setRetCode(SlErrors.FAILED_RMI_OPICS.getErrCode());
            msg.setRetMsg(SlErrors.FAILED_RMI_OPICS.getErrMsg() + ":" + e.getMessage() + "\n");
            msg.setRetStatus(RetStatusEnum.F);
        } catch (Exception e) {
            msg.setRetCode(SlErrors.FAILED.getErrCode());
            msg.setRetMsg(SlErrors.FAILED.getErrMsg() + ":" + e.getMessage() + "\n");
            msg.setRetStatus(RetStatusEnum.F);
        }
        return RetMsgHelper.ok(msg);
    }

}
