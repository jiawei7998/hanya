package com.singlee.hrbnewport.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbnewport.model.*;
import com.singlee.hrbnewport.service.*;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author ：chenguo
 * @date ：Created in 2021/12/3 18:58
 * @description：哈尔滨银行本外币同业台账-金融市场部
 * @modified By：
 * @version:
 */
@Controller
@RequestMapping("/TheForeignCurrencyFinancialController")
public class TheForeignCurrencyFinancialController {


    @Resource
    IfsReportTytzBctService ifsReportTytzBctService;
    @Resource
    IfsReportTytzBtcService ifsReportTytzBtcService;
    @Resource
    IfsReportTytzBtyccService ifsReportTytzBtyccService;
    @Resource
    IfsReportTytzBtycrService ifsReportTytzBtycrService;
    @Resource
    IfsReportTytzWctService ifsReportTytzWctService;
    @Resource
    IfsReportTytzWtcService ifsReportTytzWtcService;
    @Resource
    IfsReportTytzWtyccService ifsReportTytzWtyccService;
    @Resource
    IfsReportTytzWtycrService ifsReportTytzWtycrService;




        /*
        * 本币存放同业
        * */
    @ResponseBody
    @RequestMapping("/call2")
    public RetMsg<PageInfo<IfsReportTytzBct>> call2(@RequestBody Map<String ,Object> map){
        map.put("intodates", ParameterUtil.getString(map, "queryDate", DateUtil.getCurrentDateAsString()));
        List<IfsReportTytzBct> list= ifsReportTytzBctService.getDate(map);
        int pageNum = ParameterUtil.getInt(map,"pageNumber",1);
        int pageSize =ParameterUtil.getInt(map,"pageSize",30);
        Page<IfsReportTytzBct> page=HrbReportUtils.producePage(list,pageNum,pageSize);
        return RetMsgHelper.ok(page);
    }
    /*
     * 本币同业存放
     * */
    @ResponseBody
    @RequestMapping("/call1")
    public RetMsg<PageInfo<IfsReportTytzBtc>> call1(@RequestBody Map<String ,Object> map){
        map.put("intodates", ParameterUtil.getString(map, "queryDate", DateUtil.getCurrentDateAsString()));
        List<IfsReportTytzBtc> list= ifsReportTytzBtcService.getDate(map);
        int pageNum = ParameterUtil.getInt(map,"pageNumber",1);
        int pageSize =ParameterUtil.getInt(map,"pageSize",30);
        Page<IfsReportTytzBtc> page=HrbReportUtils.producePage(list,pageNum,pageSize);
        return RetMsgHelper.ok(page);
    }



    /*
     * 本币同业拆出
     * */
    @ResponseBody
    @RequestMapping("/call4")
    public RetMsg<PageInfo<IfsReportTytzBtycc>> call4(@RequestBody Map<String ,Object> map){
        map.put("intodates", ParameterUtil.getString(map, "queryDate", DateUtil.getCurrentDateAsString()));
        List<IfsReportTytzBtycc> list= ifsReportTytzBtyccService.getDate(map);
        int pageNum = ParameterUtil.getInt(map,"pageNumber",1);
        int pageSize =ParameterUtil.getInt(map,"pageSize",30);
        Page<IfsReportTytzBtycc> page=HrbReportUtils.producePage(list,pageNum,pageSize);
        return RetMsgHelper.ok(page);
    }


    /*
     * 本币同业拆入
     * */
    @ResponseBody
    @RequestMapping("/call3")
    public RetMsg<PageInfo<IfsReportTytzBtycr>> call3(@RequestBody Map<String ,Object> map){
        map.put("intodates", ParameterUtil.getString(map, "queryDate", DateUtil.getCurrentDateAsString()));
        List<IfsReportTytzBtycr> list= ifsReportTytzBtycrService.getDate(map);
        int pageNum = ParameterUtil.getInt(map,"pageNumber",1);
        int pageSize =ParameterUtil.getInt(map,"pageSize",30);
        Page<IfsReportTytzBtycr> page=HrbReportUtils.producePage(list,pageNum,pageSize);
        return RetMsgHelper.ok(page);
    }


    /*
     * 外币存放同业
     * */
    @ResponseBody
    @RequestMapping("/call6")
    public RetMsg<PageInfo<IfsReportTytzWct>> call6(@RequestBody Map<String ,Object> map){
        map.put("intodates", ParameterUtil.getString(map, "queryDate", DateUtil.getCurrentDateAsString()));
        List<IfsReportTytzWct> list= ifsReportTytzWctService.getDate(map);
        int pageNum = ParameterUtil.getInt(map,"pageNumber",1);
        int pageSize =ParameterUtil.getInt(map,"pageSize",30);
        Page<IfsReportTytzWct> page=HrbReportUtils.producePage(list,pageNum,pageSize);
        return RetMsgHelper.ok(page);
    }


    /*
     * 外币同业存放
     * */
    @ResponseBody
    @RequestMapping("/call5")
    public RetMsg<PageInfo<IfsReportTytzWtc>> call5(@RequestBody Map<String ,Object> map){
        map.put("intodates", ParameterUtil.getString(map, "queryDate", DateUtil.getCurrentDateAsString()));
        List<IfsReportTytzWtc> list= ifsReportTytzWtcService.getDate(map);
        int pageNum = ParameterUtil.getInt(map,"pageNumber",1);
        int pageSize =ParameterUtil.getInt(map,"pageSize",30);
        Page<IfsReportTytzWtc> page=HrbReportUtils.producePage(list,pageNum,pageSize);
        return RetMsgHelper.ok(page);
    }


    /*
     * 外币同业拆出
     * */
    @ResponseBody
    @RequestMapping("/call8")
    public RetMsg<PageInfo<IfsReportTytzWtycc>> call8(@RequestBody Map<String ,Object> map){
        map.put("intodates", ParameterUtil.getString(map, "queryDate", DateUtil.getCurrentDateAsString()));
        List<IfsReportTytzWtycc> list= ifsReportTytzWtyccService.getDate(map);
        int pageNum = ParameterUtil.getInt(map,"pageNumber",1);
        int pageSize =ParameterUtil.getInt(map,"pageSize",30);
        Page<IfsReportTytzWtycc> page=HrbReportUtils.producePage(list,pageNum,pageSize);
        return RetMsgHelper.ok(page);
    }

    /*
     * 外币同业拆入
     * */
    @ResponseBody
    @RequestMapping("/call7")
    public RetMsg<PageInfo<IfsReportTytzWtycr>> call7(@RequestBody Map<String ,Object> map){
        map.put("intodates", ParameterUtil.getString(map, "queryDate", DateUtil.getCurrentDateAsString()));
        List<IfsReportTytzWtycr> list= ifsReportTytzWtycrService.getDate(map);
        int pageNum = ParameterUtil.getInt(map,"pageNumber",1);
        int pageSize =ParameterUtil.getInt(map,"pageSize",30);
        Page<IfsReportTytzWtycr> page=HrbReportUtils.producePage(list,pageNum,pageSize);
        return RetMsgHelper.ok(page);
    }



}
