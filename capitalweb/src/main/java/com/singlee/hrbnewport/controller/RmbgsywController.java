package com.singlee.hrbnewport.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbnewport.model.IfsReportRmbgsyw;
import com.singlee.hrbnewport.service.IfsReportRmbgsywService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author ：chenguo
 * @date ：Created in 2021/12/3 18:58
 * @description：2102人民币购售业务信息
 * @modified By：
 * @version:
 */
@Controller
@RequestMapping("/RmbgsywController")
public class RmbgsywController {


    @Resource
    IfsReportRmbgsywService ifsReportRmbgsywService;

    
    @ResponseBody
    @RequestMapping("/callRmbgsyw")
    public RetMsg<PageInfo<IfsReportRmbgsyw>> callRmbgsyw(@RequestBody Map<String ,Object> map){
        map.put("intodates", ParameterUtil.getString(map, "queryDate", DateUtil.getCurrentDateAsString()));
        map.put("OPICS", HrbReportUtils.OPICS);
        map.put("HRBCB", HrbReportUtils.HRBCB);
//        List<IfsReportRmbgsyw> list= ifsReportRmbgsywService.getDate(map);
        List<IfsReportRmbgsyw> list= ifsReportRmbgsywService.getBaseDate(map);
        int pageNum = ParameterUtil.getInt(map,"pageNumber",1);
        int pageSize =ParameterUtil.getInt(map,"pageSize",30);
        if (list==null){
            Page<IfsReportRmbgsyw> page=new Page<>();
            return RetMsgHelper.ok(page);
        }
        Page<IfsReportRmbgsyw> page=HrbReportUtils.producePage(list,pageNum,pageSize);
        return RetMsgHelper.ok(page);
    }


    @ResponseBody
    @RequestMapping("/upDateRmbgsyw")
    public RetMsg<Serializable> upDateRmbgsyw(@RequestBody List<IfsReportRmbgsyw> list){
        if (list.size()>0){
            for (IfsReportRmbgsyw ifsReportRmbgsyw:list){
                ifsReportRmbgsywService.upDate(ifsReportRmbgsyw);
            }
        }

    return RetMsgHelper.ok();
    }

}
