package com.singlee.hrbnewport.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.hrbnewport.model.*;
import com.singlee.hrbnewport.service.*;
import com.singlee.hrbnewport.service.impl.TbConsolidatePositionServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

/**
 *
 * @author Liang
 * @date 2021/11/17 13:29
 * =======================
 */
@Controller
@RequestMapping(value = "CombinePositionReportController")
public class CombinePositionReportController extends CommonController {

    @Autowired
    private TbConsolidatePositionServiceImpl tbConsolidatePositionService;

    @Autowired
    private A1411ReportService a1411ReportService;

    @Autowired
    private TbDealInformationService tbDealInformationService;

    @Autowired
    private TbZyzjbalanceService tbZyzjbalanceService;

    @Autowired
    private TbZyzjinfoService tbZyzjinfoService;

    @Autowired
    private TbLendingLedgerService tbLendingLedgerService;

    @Autowired
    private IfsReportHbyuzeService ifsReportHbyuzeService;

    @Autowired
    private IfsReportHbzsyService ifsReportHbzsyService;

    @Autowired
    private IfsReportImportantCheckService ifsReportImportantCheckService;

    @Autowired
    private IfsForexZyckService ifsForexZyckService;

    @Autowired
    private IfsReportForexzyywykService ifsReportForexzyywykService;

    @Autowired
    private IfsReportForeckService ifsReportForeckService;

    @Autowired
    private IfsReportForezyjyxeService ifsReportForezyjyxeService;

    @Autowired
    private IfsReportBondmcService ifsReportBondmcService;

    @Autowired
    private IfsReportRatetdfxService ifsReportRatetdfxService;

    @Autowired
    private IfsReportGeneralRateriskService ifsReportGeneralRateriskService;

    @Autowired
    private IfsReportBondtradedService ifsReportBondtradedService;

    @Autowired
    private TbBondInvestmentService tbBondInvestmentService;

    @Autowired
    private TbTradingAccountBondinvestService tbTradingAccountBondinvestService;

    @Autowired
    private TbRmbDealService tbRmbDealService;

    @Autowired
    private TbHoldBondService tbHoldBondService;

    @Autowired
    private TbRateSwapService tbRateSwapService;

    @Autowired
    private TbBondProfitService tbBondProfitService;

    @Autowired
    private TbCustomerRisksService tbCustomerRisksService;

    @Autowired
    private TbPositionRatingBondsService tbPositionRatingBondsService;

    @Autowired
    private IfsReportHbccmxbhService ifsReportHbccmxbhService;

    /**
     * 合并持仓明细变化
     * @param map
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/searchCombinePositionPage")
    public RetMsg<PageInfo<TbConsolidatePosition>> searchCombinePositionPage(@RequestBody Map<String, String> map){
        Page<TbConsolidatePosition> page=tbConsolidatePositionService.selectReport(map);
        return RetMsgHelper.ok(page);
    }

    /**
     * 新表A1411表
     * @param map
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/searchA1411PositionPage")
    public RetMsg<PageInfo<A1411ReportVO>> searchA1411PositionPage(@RequestBody Map<String, String> map){
        String datadate = ParameterUtil.getString(map,"queryDate","");
        if("".equals(datadate)) {
            JY.raise("缺少参数......");
        }
        map.put("DATADATE", datadate);
        Page<A1411ReportVO> page=a1411ReportService.selectA1411Report(map);
        return RetMsgHelper.ok(page);
    }

    /**
     * East资金交易信息表
     * @param map
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/searchEastPositionPage1")
    public RetMsg<PageInfo<TbDealInformation>> searchEastPositionPage1(@RequestBody Map<String, String> map){
        String datadate = ParameterUtil.getString(map,"queryDate","");
        if("".equals(datadate)) {
            JY.raise("缺少参数......");
        }
        map.put("DATADATE", datadate);
        Page<TbDealInformation> page=tbDealInformationService.selectReport(map);
        return RetMsgHelper.ok(page);
    }

    /**
     * East自营资金交易信息表
     * @param map
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/searchEastPositionPage2")
    public RetMsg<PageInfo<TbZyzjinfo>> searchEastPositionPage2(@RequestBody Map<String, String> map){
        String datadate = ParameterUtil.getString(map,"queryDate","");
        if("".equals(datadate)) {
            JY.raise("缺少参数......");
        }
        map.put("DATADATE", datadate);
        Page<TbZyzjinfo> page=tbZyzjinfoService.selectReport(map);
        return RetMsgHelper.ok(page);
    }

    /**
     * East自营资金业务余额表
     * @param map
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/searchEastPositionPage3")
    public RetMsg<PageInfo<TbZyzjbalance>> searchEastPositionPage3(@RequestBody Map<String, String> map){
        String datadate = ParameterUtil.getString(map,"queryDate","");
        if("".equals(datadate)) {
            JY.raise("缺少参数......");
        }
        map.put("DATADATE", datadate);
        Page<TbZyzjbalance> page=tbZyzjbalanceService.selectReport(map);
        return RetMsgHelper.ok(page);
    }

    /**
     * 拆借交易台账
     * @param map
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/searchTbLending")
    public RetMsg<PageInfo<TbLendingLedger>> searchTbLending(@RequestBody Map<String, String> map){
        map.put("sdate", ParameterUtil.getString(map,"startDate",""));
        map.put("edate", ParameterUtil.getString(map,"endDate",""));
        map.put("dtype", ParameterUtil.getString(map,"type",""));
        map.put("dccy", ParameterUtil.getString(map,"ccy",""));
        map.put("dbr", ParameterUtil.getString(map,"br",""));
        Page<TbLendingLedger> page=tbLendingLedgerService.selectReport(map);
        return RetMsgHelper.ok(page);
    }

    /**
     *合并持仓明细分析报表【含逾期】-给于泽
     * @param map
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/searchIfsReportHbyuze")
    public RetMsg<PageInfo<IfsReportHbccmxbh>> searchIfsReportHbyuze(@RequestBody Map<String, String> map){
        String datadate = ParameterUtil.getString(map,"queryDate","");
        if("".equals(datadate)) {
            JY.raise("缺少参数......");
        }
        map.put("DATADATE", datadate);
        Page<IfsReportHbccmxbh> page=ifsReportHbccmxbhService.selectReport(map);
        return RetMsgHelper.ok(page);
    }

    /**
     *合并持仓明细分析报表-给郑诗宇
     * @param map
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/searchIfsReportHbzsy")
    public RetMsg<PageInfo<IfsReportHbccmxbh>> searchIfsReportHbzsy(@RequestBody Map<String, String> map){
        String datadate = ParameterUtil.getString(map,"queryDate","");
        if("".equals(datadate)) {
            JY.raise("缺少参数......");
        }
        map.put("DATADATE", datadate);
        Page<IfsReportHbccmxbh> page=ifsReportHbccmxbhService.selectReport(map);
        return RetMsgHelper.ok(page);
    }

    /**
     *重点业务对账表
     * @param map
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/searchIfsReportImportantCheck")
    public RetMsg<PageInfo<IfsReportImportantCheck>> searchIfsReportImportantCheck(@RequestBody Map<String, String> map){
        String datadate = ParameterUtil.getString(map,"queryDate","");
        if("".equals(datadate)) {
            JY.raise("缺少参数......");
        }
        map.put("DATADATE", datadate);
        Page<IfsReportImportantCheck> page=ifsReportImportantCheckService.selectReport(map);
        return RetMsgHelper.ok(page);
    }

    /**
     *外汇自营敞口情况表
     * @param map
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/searchIfsForexZyck")
    public RetMsg<PageInfo<IfsForexZyck>> searchIfsForexZyck(@RequestBody Map<String, String> map){
        String sdate = ParameterUtil.getString(map,"sdate","");
        String edate = ParameterUtil.getString(map,"edate","");
        String ccytype = ParameterUtil.getString(map,"ccytype","");
        String unit = ParameterUtil.getString(map,"unit","");
        if("".equals(sdate)||"".equals(edate)) {
            JY.raise("缺少参数......");
        }
        map.put("sdate", sdate);
        map.put("edate", edate);
        map.put("ccytype", ccytype);
        map.put("unit", unit);
        Page<IfsForexZyck> page=ifsForexZyckService.selectReport(map);
        return RetMsgHelper.ok(page);
    }

    /**
     *外汇自营业务浮盈亏表
     * @param map
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/searchIfsReportForexzyywyk")
    public RetMsg<PageInfo<IfsReportForexzyywyk>> searchIfsReportForexzyywyk(@RequestBody Map<String, String> map){
        String datadate = ParameterUtil.getString(map,"queryDate","");
        if("".equals(datadate)) {
            JY.raise("缺少参数......");
        }
        map.put("DATADATE", datadate);
        Page<IfsReportForexzyywyk> page=ifsReportForexzyywykService.selectReport(map);
        return RetMsgHelper.ok(page);
    }

    /**
     *外汇敞口情况表
     * @param map
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/searchIfsReportForeck")
    public RetMsg<List<IfsReportForeck>> searchIfsReportForeck(@RequestBody Map<String, String> map){
        String datadate = ParameterUtil.getString(map,"queryDate","");
        String unit = ParameterUtil.getString(map,"unit","");
        String ccytype = ParameterUtil.getString(map,"ccytype","");
        if("".equals(datadate)||"".equals(unit)||"".equals(ccytype)) {
            JY.raise("缺少参数......");
        }
        map.put("DATADATE", datadate);
        map.put("UNIT", unit);
        map.put("CCYTYPE", ccytype);
        List<IfsReportForeck> page=ifsReportForeckService.selectReport(map);
        return RetMsgHelper.ok(page);
    }

    /**
     *外汇自营交易限额执行情况表
     * @param map
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/searchIfsReportForezyjyxe")
    public RetMsg<List<IfsReportForezyjyxe>> searchIfsReportForezyjyxe(@RequestBody Map<String, String> map){
        String sdate = ParameterUtil.getString(map,"sdate","");
        String edate = ParameterUtil.getString(map,"edate","");
        String wd1 = ParameterUtil.getString(map,"wd1","1000");
        String wd2 = ParameterUtil.getString(map,"wd2","1500");
        String wd3 = ParameterUtil.getString(map,"wd3","2000");
        if("".equals(sdate)||"".equals(edate)) {
            JY.raise("缺少参数......");
        }
        map.put("sdate", sdate);
        map.put("edate", edate);
        map.put("wd1", wd1);
        map.put("wd2", wd2);
        map.put("wd3", wd3);
        List<IfsReportForezyjyxe> page=ifsReportForezyjyxeService.selectReport(map);
        return RetMsgHelper.ok(page);
    }

    /**
     *债券卖出、兑付明细表
     * @param map
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/searchIfsReportBondmc")
    public RetMsg<PageInfo<IfsReportBondmc>> searchIfsReportBondmc(@RequestBody Map<String, String> map){
        String sdate = ParameterUtil.getString(map,"sdate","");
        String edate = ParameterUtil.getString(map,"edate","");
        if("".equals(edate)||"".equals(sdate)) {
            JY.raise("缺少参数......");
        }
        map.put("edate", edate);
        map.put("sdate", sdate);
        Page<IfsReportBondmc> page=ifsReportBondmcService.selectReport(map);
        return RetMsgHelper.ok(page);
    }

    /**
     *利率特定风险计算表
     * @param map
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/searchIfsReportRatetdfx")
    public RetMsg<List<IfsReportRatetdfx>> searchIfsReportRatetdfx(@RequestBody Map<String, String> map){
        String datadate = ParameterUtil.getString(map,"queryDate","");
        if("".equals(datadate)) {
            JY.raise("缺少参数......");
        }
        map.put("DATADATE", datadate);
        List<IfsReportRatetdfx> list=ifsReportRatetdfxService.selectReport(map);
        return RetMsgHelper.ok(list);
    }

    /**
     * 市场风险标准法资本要求情况表（一般利率风险-到期日法）
     * @param map
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/searchIfsReportGeneralRaterisk")
    public RetMsg<PageInfo<IfsReportGeneralRaterisk>> searchIfsReportGeneralRaterisk(@RequestBody Map<String, String> map){
        String datadate = ParameterUtil.getString(map,"queryDate","");
        if("".equals(datadate)) {
            JY.raise("缺少参数......");
        }
        map.put("DATADATE", datadate);
        Page<IfsReportGeneralRaterisk> page=ifsReportGeneralRateriskService.selectReport(map);
        return RetMsgHelper.ok(page);
    }

    /**
     * 交易账户债券业务已实现损益情况
     * @param map
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/searchIfsReportBondtraded")
    public RetMsg<List<IfsReportBondtraded>> searchIfsReportBondtraded(@RequestBody Map<String, String> map){
        String datadate = ParameterUtil.getString(map,"queryDate","");
        String br = ParameterUtil.getString(map,"br","");
        String trad = ParameterUtil.getString(map,"trad","");
        if("".equals(datadate)) {
            JY.raise("缺少参数......");
        }
        map.put("datadate", datadate);
        map.put("br", br);
        map.put("trad", trad);
        List<IfsReportBondtraded> page=ifsReportBondtradedService.selectReport(map);
        return RetMsgHelper.ok(page);
    }

    /**
     * 债券投资结构报表
     * @param map
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/searchTbBondInvestment")
    public RetMsg<List<TbBondInvestment>> searchTbBondInvestment(@RequestBody Map<String, String> map){
        String datadate = ParameterUtil.getString(map,"queryDate","");
        String trad = ParameterUtil.getString(map,"trad","");
        if("".equals(datadate)) {
            JY.raise("缺少参数......");
        }
        map.put("datadate", datadate);
        map.put("trad", trad);
        List<TbBondInvestment> page=tbBondInvestmentService.selectReport(map);
        return RetMsgHelper.ok(page);
    }

    /**
     * 交易账户债券投资结构情况
     * @param map
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/searchTbTradingAccountBondinvest")
    public RetMsg<PageInfo<TbTradingAccountBondinvest>> searchTbTradingAccountBondinvest(@RequestBody Map<String, String> map){
        String sdate = ParameterUtil.getString(map,"sdate","");
        String edate = ParameterUtil.getString(map,"edate","");
        String br = ParameterUtil.getString(map,"br","");
        if("".equals(edate)||"".equals(sdate)) {
            JY.raise("缺少参数......");
        }
        map.put("sdate", sdate);
        map.put("edate", edate);
        map.put("br", br);
        Page<TbTradingAccountBondinvest> page=tbTradingAccountBondinvestService.selectReport(map);
        return RetMsgHelper.ok(page);
    }

    /**
     * 本币交易情况表
     * @param map
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/searchTbRmbDeal")
    public RetMsg<PageInfo<TbRmbDeal>> searchTbRmbDeal(@RequestBody Map<String, String> map){
        String sdate = ParameterUtil.getString(map,"sdate","");
        String edate = ParameterUtil.getString(map,"edate","");
        String trad = ParameterUtil.getString(map,"trad","");
        if("".equals(sdate)||"".equals(edate)) {
            JY.raise("缺少参数......");
        }
        map.put("sdate", sdate);
        map.put("edate", edate);
        map.put("trad", trad);
        Page<TbRmbDeal> page=tbRmbDealService.selectReport(map);
        return RetMsgHelper.ok(page);
    }

    /**
     * 持有债券结构情况表
     * @param map
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/searchTbHoldBond")
    public RetMsg<List<TbHoldBond>> searchTbHoldBond(@RequestBody Map<String, String> map){
        String datadate = ParameterUtil.getString(map,"queryDate","");
        String trad = ParameterUtil.getString(map,"trad","3");
        if("".equals(datadate)) {
            JY.raise("缺少参数......");
        }
        map.put("datadate", datadate);
        map.put("trad", trad);
        List<TbHoldBond> page=tbHoldBondService.selectReport(map);
        return RetMsgHelper.ok(page);
    }

    /**
     * 利率互换业务明细表
     * @param map
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/searchTbRateSwap")
    public RetMsg<PageInfo<TbRateSwap>> searchTbRateSwap(@RequestBody Map<String, String> map){
        String datadate = ParameterUtil.getString(map,"queryDate","");
        if("".equals(datadate)) {
            JY.raise("缺少参数......");
        }
        map.put("DATADATE", datadate);
        Page<TbRateSwap> page=tbRateSwapService.selectReport(map);
        return RetMsgHelper.ok(page);
    }

    /**
     * 利润测算表
     * @param map
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/searchTbBondProfit")
    public RetMsg<PageInfo<TbBondProfit>> searchTbBondProfit(@RequestBody Map<String, String> map){
        String datadate = ParameterUtil.getString(map,"queryDate","");
        String profittype = ParameterUtil.getString(map,"profittype","");
        if("".equals(datadate)) {
            JY.raise("缺少参数......");
        }
        map.put("datadate", datadate);
        map.put("profittype", profittype);
        Page<TbBondProfit> page=tbBondProfitService.selectReport(map);
        return RetMsgHelper.ok(page);
    }

    /**
     * opics-新版客户风险需求（交易账户）
     * @param map
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/searchTbCustomerRisks")
    public RetMsg<PageInfo<TbCustomerRisks>> searchTbCustomerRisks(@RequestBody Map<String, String> map){
        String datadate = ParameterUtil.getString(map,"queryDate","");
        if("".equals(datadate)) {
            JY.raise("缺少参数......");
        }
        map.put("DATADATE", datadate);
        Page<TbCustomerRisks> page=tbCustomerRisksService.selectReport(map);
        return RetMsgHelper.ok(page);
    }

    /**
     * 持仓债券评级情况表
     * @param map
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/searchtbPositionRatingBonds")
    public RetMsg<PageInfo<TbPositionRatingBonds>> searchtbPositionRatingBonds(@RequestBody Map<String, String> map){
        String datadate = ParameterUtil.getString(map,"queryDate","");
        if("".equals(datadate)) {
            JY.raise("缺少参数......");
        }
        map.put("DATADATE", datadate);
        Page<TbPositionRatingBonds> page=tbPositionRatingBondsService.selectReport(map);
        return RetMsgHelper.ok(page);
    }

}
