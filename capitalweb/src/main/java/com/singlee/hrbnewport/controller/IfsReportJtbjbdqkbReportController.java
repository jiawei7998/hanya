package com.singlee.hrbnewport.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.SlErrors;
import com.singlee.hrbnewport.model.IfsReportJtbjbdqkb;
import com.singlee.hrbnewport.service.IfsReportJtbjbdqkbReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;


/**
 * 计提本金变动情况报表
 */
@Controller
@RequestMapping(value = "/IfsReportJtbjbdqkbReportController")
public class IfsReportJtbjbdqkbReportController extends CommonController {

    @Autowired
    private IfsReportJtbjbdqkbReportService ifsReportJtbjbdqkbReportService;

    @ResponseBody
    @RequestMapping(value = "/searchIfsReportJtbjbdqkbReportPage")
    public RetMsg<PageInfo<IfsReportJtbjbdqkb>> searchIfsReportJtbjbdqkbReportPage(@RequestBody Map<String, String> map) {
        Page<IfsReportJtbjbdqkb> page = ifsReportJtbjbdqkbReportService.searchIfsReportJtbjbdqkbPage(map);
        return RetMsgHelper.ok(page);
    }

    @ResponseBody
    @RequestMapping(value = "/createIfsReportJtbjbdqkb")
    public RetMsg<SlOutBean> createIfsReportJtbjbdqkb(@RequestBody Map<String, String> params) {
        SlOutBean msg = new SlOutBean();
        try {
            msg = ifsReportJtbjbdqkbReportService.ifsReportJtbjbdqkbCreate(params);
        } catch (RemoteConnectFailureException e) {
            msg.setRetCode(SlErrors.FAILED_RMI_OPICS.getErrCode());
            msg.setRetMsg(SlErrors.FAILED_RMI_OPICS.getErrMsg() + ":" + e.getMessage() + "\n");
            msg.setRetStatus(RetStatusEnum.F);
        } catch (Exception e) {
            msg.setRetCode(SlErrors.FAILED.getErrCode());
            msg.setRetMsg(SlErrors.FAILED.getErrMsg() + ":" + e.getMessage() + "\n");
            msg.setRetStatus(RetStatusEnum.F);
        }
        return RetMsgHelper.ok(msg);
    }

}
