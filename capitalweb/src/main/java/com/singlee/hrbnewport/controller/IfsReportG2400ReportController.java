package com.singlee.hrbnewport.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.SlErrors;
import com.singlee.hrbnewport.model.IfsReportG2400;
import com.singlee.hrbnewport.service.IfsReportG2400ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;


/**
 * G24最大百家金融机构同业融入情况表
 */
@Controller
@RequestMapping(value = "/IfsReportG2400ReportController")
public class IfsReportG2400ReportController extends CommonController {

    @Autowired
    private IfsReportG2400ReportService ifsReportG2400ReportService;

    @ResponseBody
    @RequestMapping(value = "/searchIfsReportG2400ReportPage")
    public RetMsg<PageInfo<IfsReportG2400>> searchIfsReportG2400ReportPage(@RequestBody Map<String, String> map) {
        Page<IfsReportG2400> page = ifsReportG2400ReportService.searchIfsReportG2400Page(map);
        return RetMsgHelper.ok(page);
    }

    @ResponseBody
    @RequestMapping(value = "/createIfsReportG2400")
    public RetMsg<SlOutBean> createIfsReportG2400(@RequestBody Map<String, String> params) {
        SlOutBean msg = new SlOutBean();
        try {
            msg = ifsReportG2400ReportService.ifsReportG2400Create(params);
        } catch (RemoteConnectFailureException e) {
            msg.setRetCode(SlErrors.FAILED_RMI_OPICS.getErrCode());
            msg.setRetMsg(SlErrors.FAILED_RMI_OPICS.getErrMsg() + ":" + e.getMessage() + "\n");
            msg.setRetStatus(RetStatusEnum.F);
        } catch (Exception e) {
            msg.setRetCode(SlErrors.FAILED.getErrCode());
            msg.setRetMsg(SlErrors.FAILED.getErrMsg() + ":" + e.getMessage() + "\n");
            msg.setRetStatus(RetStatusEnum.F);
        }
        return RetMsgHelper.ok(msg);
    }

}
