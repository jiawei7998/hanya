package com.singlee.hrbnewport.controller;


import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.hrbnewport.service.FundTposReportServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * 基金日报
 * 
 *
 */
@Controller
@RequestMapping("/FundTposReportController")
public class FundTposReportController extends CommonController {
    
    @Autowired
    FundTposReportServer fundTposReport;
    
    @ResponseBody
    @RequestMapping("/search")
    public RetMsg<Map<String,Object>> search(@RequestBody Map<String ,Object> map){
        Map<String, Object> exportData = fundTposReport.getExportData(map);
        return  RetMsgHelper.ok(exportData);
    }
    
    
    
}
