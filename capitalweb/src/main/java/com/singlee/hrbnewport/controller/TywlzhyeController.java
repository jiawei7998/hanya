package com.singlee.hrbnewport.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbnewport.model.IfsReportTywlzhye;
import com.singlee.hrbnewport.service.IfsReportTywlzhyeService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author ：chenguo
 * @date ：Created in 2021/12/3 18:58
 * @description：2105同业往来账户余额信息
 * @modified By：
 * @version:
 */
@Controller
@RequestMapping("/TywlzhyeController")
public class TywlzhyeController {


    @Resource
    IfsReportTywlzhyeService ifsReportTywlzhyeService;

    
    @ResponseBody
    @RequestMapping("/callTywlzhye")
    public RetMsg<PageInfo<IfsReportTywlzhye>> callTywlzhye(@RequestBody Map<String ,Object> map){
        map.put("intodates", ParameterUtil.getString(map, "queryDate", DateUtil.getCurrentDateAsString()));
        List<IfsReportTywlzhye> list= ifsReportTywlzhyeService.getDate(map);
        int pageNum = ParameterUtil.getInt(map,"pageNumber",1);
        int pageSize =ParameterUtil.getInt(map,"pageSize",30);
        Page<IfsReportTywlzhye> page=HrbReportUtils.producePage(list,pageNum,pageSize);
        return RetMsgHelper.ok(page);
    }

    @ResponseBody
    @RequestMapping("/upDateTywlzhye")
    public RetMsg<Serializable> upDateTywlzhye(@RequestBody List<IfsReportTywlzhye> list){
        if (list.size()>0){
            for (IfsReportTywlzhye ifsReportTywlzhye:list){
                ifsReportTywlzhyeService.upDate(ifsReportTywlzhye);
            }
        }

        return RetMsgHelper.ok();
    }


}
