package com.singlee.hrbnewport.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbnewport.model.IfsReportRmbtywlzh;
import com.singlee.hrbnewport.service.IfsReportRmbtywlzhService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author ：chenguo
 * @date ：Created in 2021/12/3 18:58
 * @description： 2104人民币同业往来账户信息
 * @modified By：
 * @version:
 */
@Controller
@RequestMapping("/RmbtywlzhController")
public class RmbtywlzhController {


    @Resource
    IfsReportRmbtywlzhService ifsReportRmbtywlzhService;

    
    @ResponseBody
    @RequestMapping("/callRmbtywlzh")
    public RetMsg<PageInfo<IfsReportRmbtywlzh>> callRmbtywlzh(@RequestBody Map<String ,Object> map){
        map.put("intodates", ParameterUtil.getString(map, "queryDate", DateUtil.getCurrentDateAsString()));
        List<IfsReportRmbtywlzh> list= ifsReportRmbtywlzhService.getDate(map);
        int pageNum = ParameterUtil.getInt(map,"pageNumber",1);
        int pageSize =ParameterUtil.getInt(map,"pageSize",30);
        Page<IfsReportRmbtywlzh> page=HrbReportUtils.producePage(list,pageNum,pageSize);
        return RetMsgHelper.ok(page);
    }


    @ResponseBody
    @RequestMapping("/upDateRmbtywlzh")
    public RetMsg<Serializable>upDateRmbtywlzh(@RequestBody List<IfsReportRmbtywlzh> list){
        if (list.size()>0){
            for (IfsReportRmbtywlzh ifsReportRmbtywlzh:list){
                ifsReportRmbtywlzhService.upDate(ifsReportRmbtywlzh);
            }
        }
        return RetMsgHelper.ok();
    }


}
