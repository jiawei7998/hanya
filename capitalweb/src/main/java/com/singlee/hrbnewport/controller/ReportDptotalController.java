package com.singlee.hrbnewport.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.hrbnewport.service.impl.ReportDptotalServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * 同业存单发行统计表
 * @author Liang
 * @date 2021/11/8 9:09
 * =======================
 */
@Controller
@RequestMapping(value = "/ReportDptotalController")
public class ReportDptotalController extends CommonController {

    @Autowired
    private ReportDptotalServiceImpl reportDptotalService;

    @ResponseBody
    @RequestMapping(value = "/searchRepoDptotalPage")
    public RetMsg<PageInfo<Map<String,Object>>> searchRepoDptotalPage(@RequestBody Map<String, Object> map) {
        String datadate = ParameterUtil.getString(map,"queryDate","");
        if("".equals(datadate) || "".equals(datadate)) {
            JY.raise("缺少参数......");
        }
        map.put("DATADATE", datadate);
        Page<Map<String,Object>> reportDptotalPage = reportDptotalService.getExportPageData(map);
        return RetMsgHelper.ok(reportDptotalPage);
    }
}
