package com.singlee.hrbnewport.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.SlErrors;
import com.singlee.hrbnewport.model.IfsReportCshbnwtzMxcth;
import com.singlee.hrbnewport.service.IfsReportCshbnwtzMxcthReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;


/**
 * [1104]-附表1：城商行表内外投资业务和同业交易对手情况表（2021年版本）---更新-给计财张碧雪、资负杨晓欣(1)
 * <p>
 * 表二 表内外投资业务明细表（穿透后）
 */
@Controller
@RequestMapping(value = "/IfsReportCshbnwtzMxcthReportController")
public class IfsReportCshbnwtzMxcthReportController extends CommonController {

    @Autowired
    private IfsReportCshbnwtzMxcthReportService ifsReportCshbnwtzMxcthReportService;

    @ResponseBody
    @RequestMapping(value = "/searchIfsReportCshbnwtzMxcthReportPage")
    public RetMsg<PageInfo<IfsReportCshbnwtzMxcth>> searchIfsReportCshbnwtzMxcthReportPage(@RequestBody Map<String, String> map) {
        Page<IfsReportCshbnwtzMxcth> page = ifsReportCshbnwtzMxcthReportService.searchIfsReportCshbnwtzMxcthPage(map);
        return RetMsgHelper.ok(page);
    }

    @ResponseBody
    @RequestMapping(value = "/createIfsReportCshbnwtzMxcth")
    public RetMsg<SlOutBean> createIfsReportCshbnwtzMxcth(@RequestBody Map<String, String> params) {
        SlOutBean msg = new SlOutBean();
        try {
            msg = ifsReportCshbnwtzMxcthReportService.ifsReportCshbnwtzMxcthCreate(params);
        } catch (RemoteConnectFailureException e) {
            msg.setRetCode(SlErrors.FAILED_RMI_OPICS.getErrCode());
            msg.setRetMsg(SlErrors.FAILED_RMI_OPICS.getErrMsg() + ":" + e.getMessage() + "\n");
            msg.setRetStatus(RetStatusEnum.F);
        } catch (Exception e) {
            msg.setRetCode(SlErrors.FAILED.getErrCode());
            msg.setRetMsg(SlErrors.FAILED.getErrMsg() + ":" + e.getMessage() + "\n");
            msg.setRetStatus(RetStatusEnum.F);
        }
        return RetMsgHelper.ok(msg);
    }

}
