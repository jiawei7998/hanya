package com.singlee.hrbnewport.controller;

import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.hrbnewport.service.PositionReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * g21报表到处
 * @author szl
 *
 */
@Controller
@RequestMapping("/HrbPositionExportController")
public class HrbPositonExportDataController extends CommonController {
    
    @Autowired
    PositionReportService positionReportService;
    

    @ResponseBody
    @RequestMapping("/getExportData")
    public RetMsg<List<Map<String,Object>>> getG21Data(@RequestBody Map<String ,Object> map){
        map.put("queryDate",map.get("queryDate").toString());
        List<Map<String, Object>> exportData = new ArrayList<>();
        return RetMsgHelper.ok(exportData);
    }

    
}
