package com.singlee.hrbnewport.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.hrbnewport.model.IfsReportG1401;
import com.singlee.hrbnewport.model.IfsReportG52GovRepay;
import com.singlee.hrbnewport.service.IfsReportG52GovRepayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 *  G52地方政府融资平台及支出责任债务持有情况统计表
 * 2021/12/8
 * @Auther:zk
 */
@Controller
@RequestMapping("/IfsReportG52GovRepayController")
public class IfsReportG52GovRepayController {

    @Autowired
    IfsReportG52GovRepayService ifsReportG52GovRepayService;

    @ResponseBody
    @RequestMapping("/searchAllPage")
    public RetMsg<PageInfo<IfsReportG52GovRepay>> searchAllPage(@RequestBody Map<String,Object> param){
        Page<IfsReportG52GovRepay> ifsReportG52GovRepays = ifsReportG52GovRepayService.searchAllPage(param);
        return RetMsgHelper.ok(ifsReportG52GovRepays);
    }

}
