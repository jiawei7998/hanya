package com.singlee.hrbnewport.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbnewport.model.*;
import com.singlee.hrbnewport.service.*;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author ：chenguo
 * @date ：Created in 2021/12/3 18:58
 * @description：哈尔滨银行本外币同业台账-金融市场部-给计财
 * @modified By：
 * @version:
 */
@Controller
@RequestMapping("/TheForeignCurrencyFinancialJCController")
public class TheForeignCurrencyFinancialJCController {


    @Resource
    IfsReportTytzJcBctService ifsReportTytzJcBctService;
    @Resource
    IfsReportTytzJcBtcService ifsReportTytzJcBtcService;
    @Resource
    IfsReportTytzJcBtyccService ifsReportTytzJcBtyccService;
    @Resource
    IfsReportTytzJcBttycrService ifsReportTytzJcBttycrService;
    @Resource
    IfsReportTytzJcWctService ifsReportTytzJcWctService;
    @Resource
    IfsReportTytzJcWtcService ifsReportTytzJcWtcService;
    @Resource
    IfsReportTytzJcWcftyService ifsReportTytzJcWcftyService;
    @Resource
    IfsReportTytzJcWtycrService ifsReportTytzJcWtycrService;




        /*
        * 本币存放同业
        * */
    @ResponseBody
    @RequestMapping("/call2")
    public RetMsg<PageInfo<IfsReportTytzJcBct>> call2(@RequestBody Map<String ,Object> map){
        map.put("intodates", ParameterUtil.getString(map, "queryDate", DateUtil.getCurrentDateAsString()));
        List<IfsReportTytzJcBct> list= ifsReportTytzJcBctService.getDate(map);
        int pageNum = ParameterUtil.getInt(map,"pageNumber",1);
        int pageSize =ParameterUtil.getInt(map,"pageSize",30);
        Page<IfsReportTytzJcBct> page=HrbReportUtils.producePage(list,pageNum,pageSize);
        return RetMsgHelper.ok(page);
    }
    /*
     * 本币同业存放
     * */
    @ResponseBody
    @RequestMapping("/call1")
    public RetMsg<PageInfo<IfsReportTytzJcBtc>> call1(@RequestBody Map<String ,Object> map){
        map.put("intodates", ParameterUtil.getString(map, "queryDate", DateUtil.getCurrentDateAsString()));
        List<IfsReportTytzJcBtc> list= ifsReportTytzJcBtcService.getDate(map);
        int pageNum = ParameterUtil.getInt(map,"pageNumber",1);
        int pageSize =ParameterUtil.getInt(map,"pageSize",30);
        Page<IfsReportTytzJcBtc> page=HrbReportUtils.producePage(list,pageNum,pageSize);
        return RetMsgHelper.ok(page);
    }



    /*
     * 本币同业拆出
     * */
    @ResponseBody
    @RequestMapping("/call4")
    public RetMsg<PageInfo<IfsReportTytzJcBtycc>> call4(@RequestBody Map<String ,Object> map){
        map.put("intodates", ParameterUtil.getString(map, "queryDate", DateUtil.getCurrentDateAsString()));
        List<IfsReportTytzJcBtycc> list= ifsReportTytzJcBtyccService.getDate(map);
        int pageNum = ParameterUtil.getInt(map,"pageNumber",1);
        int pageSize =ParameterUtil.getInt(map,"pageSize",30);
        Page<IfsReportTytzJcBtycc> page=HrbReportUtils.producePage(list,pageNum,pageSize);
        return RetMsgHelper.ok(page);
    }


    /*
     * 本币同业拆入
     * */
    @ResponseBody
    @RequestMapping("/call3")
    public RetMsg<PageInfo<IfsReportTytzJcBttycr>> call3(@RequestBody Map<String ,Object> map){
        map.put("intodates", ParameterUtil.getString(map, "queryDate", DateUtil.getCurrentDateAsString()));
        List<IfsReportTytzJcBttycr> list= ifsReportTytzJcBttycrService.getDate(map);
        int pageNum = ParameterUtil.getInt(map,"pageNumber",1);
        int pageSize =ParameterUtil.getInt(map,"pageSize",30);
        Page<IfsReportTytzJcBttycr> page=HrbReportUtils.producePage(list,pageNum,pageSize);
        return RetMsgHelper.ok(page);
    }


    /*
     * 外币存放同业
     * */
    @ResponseBody
    @RequestMapping("/call6")
    public RetMsg<PageInfo<IfsReportTytzJcWct>> call6(@RequestBody Map<String ,Object> map){
        map.put("intodates", ParameterUtil.getString(map, "queryDate", DateUtil.getCurrentDateAsString()));
        List<IfsReportTytzJcWct> list= ifsReportTytzJcWctService.getDate(map);
        int pageNum = ParameterUtil.getInt(map,"pageNumber",1);
        int pageSize =ParameterUtil.getInt(map,"pageSize",30);
        Page<IfsReportTytzJcWct> page=HrbReportUtils.producePage(list,pageNum,pageSize);
        return RetMsgHelper.ok(page);
    }


    /*
     * 外币同业存放
     * */
    @ResponseBody
    @RequestMapping("/call5")
    public RetMsg<PageInfo<IfsReportTytzJcWtc>> call5(@RequestBody Map<String ,Object> map){
        map.put("intodates", ParameterUtil.getString(map, "queryDate", DateUtil.getCurrentDateAsString()));
        List<IfsReportTytzJcWtc> list= ifsReportTytzJcWtcService.getDate(map);
        int pageNum = ParameterUtil.getInt(map,"pageNumber",1);
        int pageSize =ParameterUtil.getInt(map,"pageSize",30);
        Page<IfsReportTytzJcWtc> page=HrbReportUtils.producePage(list,pageNum,pageSize);
        return RetMsgHelper.ok(page);
    }


    /*
     * 外币同业拆出
     * */
    @ResponseBody
    @RequestMapping("/call8")
    public RetMsg<PageInfo<IfsReportTytzJcWcfty>> call8(@RequestBody Map<String ,Object> map){
        map.put("intodates", ParameterUtil.getString(map, "queryDate", DateUtil.getCurrentDateAsString()));
        List<IfsReportTytzJcWcfty> list= ifsReportTytzJcWcftyService.getDate(map);
        int pageNum = ParameterUtil.getInt(map,"pageNumber",1);
        int pageSize =ParameterUtil.getInt(map,"pageSize",30);
        Page<IfsReportTytzJcWcfty> page=HrbReportUtils.producePage(list,pageNum,pageSize);
        return RetMsgHelper.ok(page);
    }

    /*
     * 外币同业拆入
     * */
    @ResponseBody
    @RequestMapping("/call7")
    public RetMsg<PageInfo<IfsReportTytzJcWtycr>> call7(@RequestBody Map<String ,Object> map){
        map.put("intodates", ParameterUtil.getString(map, "queryDate", DateUtil.getCurrentDateAsString()));
        List<IfsReportTytzJcWtycr> list= ifsReportTytzJcWtycrService.getDate(map);
        int pageNum = ParameterUtil.getInt(map,"pageNumber",1);
        int pageSize =ParameterUtil.getInt(map,"pageSize",30);
        Page<IfsReportTytzJcWtycr> page=HrbReportUtils.producePage(list,pageNum,pageSize);
        return RetMsgHelper.ok(page);
    }



}
