package com.singlee.hrbnewport.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.SlErrors;
import com.singlee.hrbnewport.model.IfsReportCshmyyhzyjgzbqkb;
import com.singlee.hrbnewport.service.IfsReportCshmyyhzyjgzbqkbReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;


/**
 * 城商行民营银行主要监管指标情况表
 */
@Controller
@RequestMapping(value = "/IfsReportCshmyyhzyjgzbqkbReportController")
public class IfsReportCshmyyhzyjgzbqkbReportController extends CommonController {

    @Autowired
    private IfsReportCshmyyhzyjgzbqkbReportService ifsReportCshmyyhzyjgzbqkbReportService;

    @ResponseBody
    @RequestMapping(value = "/searchIfsReportCshmyyhzyjgzbqkbReportPage")
    public RetMsg<PageInfo<IfsReportCshmyyhzyjgzbqkb>> searchIfsReportCshmyyhzyjgzbqkbReportPage(@RequestBody Map<String, String> map) {
        Page<IfsReportCshmyyhzyjgzbqkb> page = ifsReportCshmyyhzyjgzbqkbReportService.searchIfsReportCshmyyhzyjgzbqkbPage(map);
        return RetMsgHelper.ok(page);
    }

    @ResponseBody
    @RequestMapping(value = "/createIfsReportCshmyyhzyjgzbqkb")
    public RetMsg<SlOutBean> createIfsReportCshmyyhzyjgzbqkb(@RequestBody Map<String, String> params) {
        SlOutBean msg = new SlOutBean();
        try {
            msg = ifsReportCshmyyhzyjgzbqkbReportService.ifsReportCshmyyhzyjgzbqkbCreate(params);
        } catch (RemoteConnectFailureException e) {
            msg.setRetCode(SlErrors.FAILED_RMI_OPICS.getErrCode());
            msg.setRetMsg(SlErrors.FAILED_RMI_OPICS.getErrMsg() + ":" + e.getMessage() + "\n");
            msg.setRetStatus(RetStatusEnum.F);
        } catch (Exception e) {
            msg.setRetCode(SlErrors.FAILED.getErrCode());
            msg.setRetMsg(SlErrors.FAILED.getErrMsg() + ":" + e.getMessage() + "\n");
            msg.setRetStatus(RetStatusEnum.F);
        }
        return RetMsgHelper.ok(msg);
    }

}
