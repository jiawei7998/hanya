package com.singlee.hrbnewport.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.hrbnewport.model.*;
import com.singlee.hrbnewport.service.YjInnerbankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * 【106号文-银监局】同业业务管理报告
 * 2021/12/8
 * @Auther:zk
 */
@Controller
@RequestMapping("/YjInnerbankController")
public class YjInnerbankController {

    @Autowired
    YjInnerbankService yjInnerbankService;

    @ResponseBody
    @RequestMapping("/searchRepoAllPage")
    public RetMsg<PageInfo<YjInnerbankRepo>> searchRepoAllPage(@RequestBody  Map<String, Object> param){
        Page<YjInnerbankRepo> yjInnerbankRepos = yjInnerbankService.searchRepoAllPage(param);
        return RetMsgHelper.ok(yjInnerbankRepos);
    }

    @ResponseBody
    @RequestMapping("/searchRvrepoAllPage")
    public RetMsg<PageInfo<YjInnerbankRvrepo>> searchRvrepoAllPage(@RequestBody Map<String, Object> param){
        Page<YjInnerbankRvrepo> yjInnerbankRvrepos = yjInnerbankService.searchRvrepoAllPage(param);
        return RetMsgHelper.ok(yjInnerbankRvrepos);
    }

    @ResponseBody
    @RequestMapping("/searchSecTposAllPage")
    public RetMsg<PageInfo<YjInnerbankSecTpos>> searchSecTposAllPage(@RequestBody Map<String, Object> param){
        Page<YjInnerbankSecTpos> yjInnerbankSecTpos = yjInnerbankService.searchSecTposAllPage(param);
        return RetMsgHelper.ok(yjInnerbankSecTpos);
    }

    @ResponseBody
    @RequestMapping("/searchFundAllPage")
    public RetMsg<PageInfo<YjInnerbankFund>> searchFundAllPage(@RequestBody Map<String, Object> param){
        Page<YjInnerbankFund> yjInnerbankFunds = yjInnerbankService.searchFundAllPage(param);
        return RetMsgHelper.ok(yjInnerbankFunds);
    }

    @ResponseBody
    @RequestMapping("/searchCdTposAllPage")
    public RetMsg<PageInfo<YjInnerbankCdTpos>> searchCdTposAllPage(@RequestBody Map<String, Object> param){
        Page<YjInnerbankCdTpos> yjInnerbankCdTpos = yjInnerbankService.searchCdTposAllPage(param);
        return RetMsgHelper.ok(yjInnerbankCdTpos);
    }

    @ResponseBody
    @RequestMapping("/searchDepositAllPage")
    public RetMsg<PageInfo<YjInnerbankDeposit>> searchDepositAllPage(@RequestBody Map<String, Object> param){
        Page<YjInnerbankDeposit> yjInnerbankDeposits = yjInnerbankService.searchDepositAllPage(param);
        return RetMsgHelper.ok(yjInnerbankDeposits);
    }

    @ResponseBody
    @RequestMapping("/searchCdIssAllPage")
    public RetMsg<PageInfo<YjInnerbankCdIss>> searchCdIssAllPage(@RequestBody Map<String, Object> param){
        Page<YjInnerbankCdIss> yjInnerbankCdIsses = yjInnerbankService.searchCdIssAllPage(param);
        return RetMsgHelper.ok(yjInnerbankCdIsses);
    }

}
