package com.singlee.hrbnewport.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.hrbnewport.model.IfsReportCdPosition;
import com.singlee.hrbnewport.service.IfsReportCdPositionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * 同业存单
 * 2021/12/8
 * @Auther:zk
 */
@Controller
@RequestMapping("/IfsReportCdPositionController")
public class IfsReportCdPositionController {

    @Autowired
    IfsReportCdPositionService ifsReportCdPositionService;

    @ResponseBody
    @RequestMapping("/searchAllPage")
    public RetMsg<PageInfo<IfsReportCdPosition>> searchAllPage(@RequestBody Map<String,Object> param){
        Page<IfsReportCdPosition> ifsReportCdPositions = ifsReportCdPositionService.searchAllPage(param);
        return RetMsgHelper.ok(ifsReportCdPositions);
    }

}
