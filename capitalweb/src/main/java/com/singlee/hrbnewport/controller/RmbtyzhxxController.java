package com.singlee.hrbnewport.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbnewport.model.*;
import com.singlee.hrbnewport.service.*;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author ：chenguo
 * @date ：Created in 2021/12/3 18:58
 * @description：人民币同业账户信息
 * @modified By：
 * @version:
 */
@Controller
@RequestMapping("/RmbtyzhxxController")
public class RmbtyzhxxController {


    @Resource
    IfsReportRmbtyzhxxService ifsReportRmbtyzhxxService;


    @ResponseBody
    @RequestMapping("/callRmbtyzhxx")
    public RetMsg<PageInfo<IfsReportRmbtyzhxx>> callRmbtyzhxx(@RequestBody Map<String ,Object> map){
        map.put("intodates", ParameterUtil.getString(map, "queryDate", DateUtil.getCurrentDateAsString()));
        List<IfsReportRmbtyzhxx> list= ifsReportRmbtyzhxxService.getDate(map);
        int pageNum = ParameterUtil.getInt(map,"pageNumber",1);
        int pageSize =ParameterUtil.getInt(map,"pageSize",30);
        Page<IfsReportRmbtyzhxx> page=HrbReportUtils.producePage(list,pageNum,pageSize);
        return RetMsgHelper.ok(page);
    }



}
