package com.singlee.hrbnewport.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.hrbnewport.model.IfsReportG1406;
import com.singlee.hrbnewport.service.IfsReportG1406Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.Map;

/**
 * G1406-大额风险暴露
 * by zk
 */
@Controller
@RequestMapping("/IfsReportG1406Controller")
public class IfsReportG1406Controller {

    @Autowired
    IfsReportG1406Service ifsReportG1406Service;

    /**
     * 生成数据
     * @param map
     * @return
     */
    @ResponseBody
    @RequestMapping("/creatG1406")
    public RetMsg<Serializable> creatG1406(@RequestBody Map<String , Object> map){
        ifsReportG1406Service.creatG1406(map);
        return RetMsgHelper.ok();
    }

    /**
     * 查询已生成的数据
     * @param map
     * @return
     */
    @ResponseBody
    @RequestMapping("/searchIfsReportG1406")
    public RetMsg<PageInfo<IfsReportG1406>> searchIfsReportG1406(@RequestBody Map<String,Object> map){
        Page<IfsReportG1406> ifsReportG1406s = ifsReportG1406Service.searchIfsReportG1406(map);
        return RetMsgHelper.ok(ifsReportG1406s);
    }

}
