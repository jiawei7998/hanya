package com.singlee.hrbnewport.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.hrbnewport.service.TaReportLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * @author copysun
 */
@Controller
@RequestMapping("/TaReportLogController")
public class TaReportLogController {

	@Autowired
	private TaReportLogService taReportLogService;


	@ResponseBody
	@RequestMapping("/getPageData")
	public RetMsg<PageInfo<Map<String, Object>>> getExportPageData(@RequestBody Map<String, Object> map) {
		Page<Map<String, Object>> pageList = taReportLogService.getPageList(map);
		return RetMsgHelper.ok(pageList);
	}
}
