package com.singlee.hrbnewport.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.SlErrors;
import com.singlee.hrbnewport.model.IfsReportCshbnwtzZbbbjtctq;
import com.singlee.hrbnewport.service.IfsReportCshbnwtzZbbbjtctqReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;


/**
 * [1104]-附表1：城商行表内外投资业务和同业交易对手情况表（2021年版本）---更新-给计财张碧雪、资负杨晓欣(1)
 * <p>
 * 表六 表内外投资业务资本拨备计提情况表（穿透前）
 */
@Controller
@RequestMapping(value = "/IfsReportCshbnwtzZbbbjtctqReportController")
public class IfsReportCshbnwtzZbbbjtctqReportController extends CommonController {

    @Autowired
    private IfsReportCshbnwtzZbbbjtctqReportService ifsReportCshbnwtzZbbbjtctqReportService;

    @ResponseBody
    @RequestMapping(value = "/searchIfsReportCshbnwtzZbbbjtctqReportPage")
    public RetMsg<PageInfo<IfsReportCshbnwtzZbbbjtctq>> searchIfsReportCshbnwtzZbbbjtctqReportPage(@RequestBody Map<String, String> map) {
        Page<IfsReportCshbnwtzZbbbjtctq> page = ifsReportCshbnwtzZbbbjtctqReportService.searchIfsReportCshbnwtzZbbbjtctqPage(map);
        return RetMsgHelper.ok(page);
    }

    @ResponseBody
    @RequestMapping(value = "/createIfsReportCshbnwtzZbbbjtctq")
    public RetMsg<SlOutBean> createIfsReportCshbnwtzZbbbjtctq(@RequestBody Map<String, String> params) {
        SlOutBean msg = new SlOutBean();
        try {
            msg = ifsReportCshbnwtzZbbbjtctqReportService.ifsReportCshbnwtzZbbbjtctqCreate(params);
        } catch (RemoteConnectFailureException e) {
            msg.setRetCode(SlErrors.FAILED_RMI_OPICS.getErrCode());
            msg.setRetMsg(SlErrors.FAILED_RMI_OPICS.getErrMsg() + ":" + e.getMessage() + "\n");
            msg.setRetStatus(RetStatusEnum.F);
        } catch (Exception e) {
            msg.setRetCode(SlErrors.FAILED.getErrCode());
            msg.setRetMsg(SlErrors.FAILED.getErrMsg() + ":" + e.getMessage() + "\n");
            msg.setRetStatus(RetStatusEnum.F);
        }
        return RetMsgHelper.ok(msg);
    }

}
