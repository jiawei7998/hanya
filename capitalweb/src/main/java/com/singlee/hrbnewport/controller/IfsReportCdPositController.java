package com.singlee.hrbnewport.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.hrbnewport.model.IfsReportCdPosit;
import com.singlee.hrbnewport.service.IfsReportCdPositService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * 同 业 存 单
 * 2021/12/9
 * @Auther:zk
 */
@Controller
@RequestMapping("/IfsReportCdPositController")
public class IfsReportCdPositController {

    @Autowired
    IfsReportCdPositService ifsReportCdPositService;

    @ResponseBody
    @RequestMapping("/searchAllPage")
    public RetMsg<PageInfo<IfsReportCdPosit>> searchAllPage(@RequestBody Map<String,Object> param){
        Page<IfsReportCdPosit> ifsReportCdPositions = ifsReportCdPositService.searchAllPage(param);
        return RetMsgHelper.ok(ifsReportCdPositions);
    }

}
