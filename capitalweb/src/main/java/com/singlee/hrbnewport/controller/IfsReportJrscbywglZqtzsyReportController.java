package com.singlee.hrbnewport.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.SlErrors;
import com.singlee.hrbnewport.model.IfsReportJrscbywglZqtzsy;
import com.singlee.hrbnewport.service.IfsReportJrscbywglZqtzsyReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;


/**
 * [1104]-金融市场部业务管理报表202105会计
 * <p>
 * 债券投资收益结构表
 */
@Controller
@RequestMapping(value = "/IfsReportJrscbywglZqtzsyReportController")
public class IfsReportJrscbywglZqtzsyReportController extends CommonController {

    @Autowired
    private IfsReportJrscbywglZqtzsyReportService ifsReportJrscbywglZqtzsyReportService;

    @ResponseBody
    @RequestMapping(value = "/searchIfsReportJrscbywglZqtzsyReportPage")
    public RetMsg<PageInfo<IfsReportJrscbywglZqtzsy>> searchIfsReportJrscbywglZqtzsyReportPage(@RequestBody Map<String, String> map) {
        Page<IfsReportJrscbywglZqtzsy> page = ifsReportJrscbywglZqtzsyReportService.searchIfsReportJrscbywglZqtzsyPage(map);
        return RetMsgHelper.ok(page);
    }

    @ResponseBody
    @RequestMapping(value = "/createIfsReportJrscbywglZqtzsy")
    public RetMsg<SlOutBean> createIfsReportJrscbywglZqtzsy(@RequestBody Map<String, String> params) {
        SlOutBean msg = new SlOutBean();
        try {
            msg = ifsReportJrscbywglZqtzsyReportService.ifsReportJrscbywglZqtzsyCreate(params);
        } catch (RemoteConnectFailureException e) {
            msg.setRetCode(SlErrors.FAILED_RMI_OPICS.getErrCode());
            msg.setRetMsg(SlErrors.FAILED_RMI_OPICS.getErrMsg() + ":" + e.getMessage() + "\n");
            msg.setRetStatus(RetStatusEnum.F);
        } catch (Exception e) {
            msg.setRetCode(SlErrors.FAILED.getErrCode());
            msg.setRetMsg(SlErrors.FAILED.getErrMsg() + ":" + e.getMessage() + "\n");
            msg.setRetStatus(RetStatusEnum.F);
        }
        return RetMsgHelper.ok(msg);
    }

}
