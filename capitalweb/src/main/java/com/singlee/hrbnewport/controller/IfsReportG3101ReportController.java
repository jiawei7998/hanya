package com.singlee.hrbnewport.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.SlErrors;
import com.singlee.hrbnewport.model.IfsReportG3101;
import com.singlee.hrbnewport.service.IfsReportG3101ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;


/**
 * G31投资业务情况表 底层资产投资情况
 */
@Controller
@RequestMapping(value = "/IfsReportG3101ReportController")
public class IfsReportG3101ReportController extends CommonController {

    @Autowired
    private IfsReportG3101ReportService ifsReportG3101ReportService;

    @ResponseBody
    @RequestMapping(value = "/searchIfsReportG3101ReportPage")
    public RetMsg<PageInfo<IfsReportG3101>> searchIfsReportG3101ReportPage(@RequestBody Map<String, String> map) {
        Page<IfsReportG3101> page = ifsReportG3101ReportService.searchIfsReportG3101Page(map);
        return RetMsgHelper.ok(page);
    }

    @ResponseBody
    @RequestMapping(value = "/createIfsReportG3101")
    public RetMsg<SlOutBean> createIfsReportG3101(@RequestBody Map<String, String> params) {
        SlOutBean msg = new SlOutBean();
        try {
            msg = ifsReportG3101ReportService.ifsReportG3101Create(params);
        } catch (RemoteConnectFailureException e) {
            msg.setRetCode(SlErrors.FAILED_RMI_OPICS.getErrCode());
            msg.setRetMsg(SlErrors.FAILED_RMI_OPICS.getErrMsg() + ":" + e.getMessage() + "\n");
            msg.setRetStatus(RetStatusEnum.F);
        } catch (Exception e) {
            msg.setRetCode(SlErrors.FAILED.getErrCode());
            msg.setRetMsg(SlErrors.FAILED.getErrMsg() + ":" + e.getMessage() + "\n");
            msg.setRetStatus(RetStatusEnum.F);
        }
        return RetMsgHelper.ok(msg);
    }

}
