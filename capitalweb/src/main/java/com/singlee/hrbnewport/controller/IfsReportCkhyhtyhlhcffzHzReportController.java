package com.singlee.hrbnewport.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.SlErrors;
import com.singlee.hrbnewport.model.IfsReportCkhyhtyhlhcffzHz;
import com.singlee.hrbnewport.service.IfsReportCkhyhtyhlhcffzHzReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;


/**
 * 存款含银行同业和联行存放负债 汇总
 */
@Controller
@RequestMapping(value = "/IfsReportCkhyhtyhlhcffzHzReportController")
public class IfsReportCkhyhtyhlhcffzHzReportController extends CommonController {

    @Autowired
    private IfsReportCkhyhtyhlhcffzHzReportService ifsReportCkhyhtyhlhcffzHzReportService;

    @ResponseBody
    @RequestMapping(value = "/searchIfsReportCkhyhtyhlhcffzHzReportPage")
    public RetMsg<PageInfo<IfsReportCkhyhtyhlhcffzHz>> searchIfsReportCkhyhtyhlhcffzHzReportPage(@RequestBody Map<String, String> map) {
        Page<IfsReportCkhyhtyhlhcffzHz> page = ifsReportCkhyhtyhlhcffzHzReportService.searchIfsReportCkhyhtyhlhcffzHzPage(map);
        return RetMsgHelper.ok(page);
    }

    @ResponseBody
    @RequestMapping(value = "/createIfsReportCkhyhtyhlhcffzHz")
    public RetMsg<SlOutBean> createIfsReportCkhyhtyhlhcffzHz(@RequestBody Map<String, String> params) {
        SlOutBean msg = new SlOutBean();
        try {
            msg = ifsReportCkhyhtyhlhcffzHzReportService.ifsReportCkhyhtyhlhcffzHzCreate(params);
        } catch (RemoteConnectFailureException e) {
            msg.setRetCode(SlErrors.FAILED_RMI_OPICS.getErrCode());
            msg.setRetMsg(SlErrors.FAILED_RMI_OPICS.getErrMsg() + ":" + e.getMessage() + "\n");
            msg.setRetStatus(RetStatusEnum.F);
        } catch (Exception e) {
            msg.setRetCode(SlErrors.FAILED.getErrCode());
            msg.setRetMsg(SlErrors.FAILED.getErrMsg() + ":" + e.getMessage() + "\n");
            msg.setRetStatus(RetStatusEnum.F);
        }
        return RetMsgHelper.ok(msg);
    }

}
