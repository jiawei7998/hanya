package com.singlee.hrbnewport.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.hrbnewport.model.IfsReportDepositInvestorReport;
import com.singlee.hrbnewport.service.IfsReportDepositInvestorReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.Map;

/**
 *申购信息查询结果
 * 2021/12/6
 * @Auther:zk
 */
@Controller
@RequestMapping("/IfsReportDepositInvestorReportController")
public class IfsReportDepositInvestorReportController {
    @Autowired
    IfsReportDepositInvestorReportService ifsReportDepositInvestorReportService;

    /**
     * 生成报表数据
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping("/creatDepositInvestor")
    public RetMsg<Serializable> creatDepositInvestor(@RequestBody Map<String,Object> param){
        ifsReportDepositInvestorReportService.createReport(param);
        return RetMsgHelper.ok();
    }

    /**
     * 查询亦生成的数据
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping("/searchDepositInvestor")
    public RetMsg<PageInfo<IfsReportDepositInvestorReport>> searchDepositInvestor(@RequestBody Map<String,Object> param){
        Page<IfsReportDepositInvestorReport> reportPage = ifsReportDepositInvestorReportService.selectAllReport(param);
        return RetMsgHelper.ok(reportPage);
    }

}
