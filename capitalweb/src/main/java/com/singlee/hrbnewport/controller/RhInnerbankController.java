package com.singlee.hrbnewport.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.hrbnewport.model.YjInnerbankRepo;
import com.singlee.hrbnewport.service.RhInnerbankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * 人行-同业业务管理报告
 * 2021/12/9
 * @Auther:zk
 */
@Controller
@RequestMapping("/RhInnerbankController")
public class RhInnerbankController {

    @Autowired
    RhInnerbankService rhInnerbankService;

    @ResponseBody
    @RequestMapping("/searchAllPage")
    public RetMsg<PageInfo<Map<String, Object>>> searchAllPage(@RequestBody Map<String, Object> param){
        Page<Map<String, Object>> maps = rhInnerbankService.searchAllPage(param);
        return RetMsgHelper.ok(maps);
    }

}
