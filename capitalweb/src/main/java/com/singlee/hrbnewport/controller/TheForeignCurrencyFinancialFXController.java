package com.singlee.hrbnewport.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbnewport.model.*;
import com.singlee.hrbnewport.service.*;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author ：chenguo
 * @date ：Created in 2021/12/3 18:58
 * @description：哈尔滨银行本外币同业台账-金融市场部-给风险
 * @modified By：
 * @version:
 */
@Controller
@RequestMapping("/TheForeignCurrencyFinancialFXController")
public class TheForeignCurrencyFinancialFXController {


    @Resource
    IfsReportTytzFxBctService ifsReportTytzFxBctService;
    @Resource
    IfsReportTytzFxBtcService ifsReportTytzFxBtcService;
    @Resource
    IfsReportTytzFxBtyccService ifsReportTytzFxBtyccService;
    @Resource
    IfsReportTytzFxBtycrService ifsReportTytzFxBtycrService;
    @Resource
    IfsReportTytzFxWctService ifsReportTytzFxWctService;
    @Resource
    IfsReportTytzFxWtcService ifsReportTytzFxWtcService;
    @Resource
    IfsReportTytzFxWtyccService ifsReportTytzFxWtyccService;
    @Resource
    IfsReportTytzFxWtycrService ifsReportTytzFxWtycrService;




        /*
        * 本币存放同业
        * */
    @ResponseBody
    @RequestMapping("/call2")
    public RetMsg<PageInfo<IfsReportTytzFxBct>> call2(@RequestBody Map<String ,Object> map){
        map.put("intodates", ParameterUtil.getString(map, "queryDate", DateUtil.getCurrentDateAsString()));
        List<IfsReportTytzFxBct> list= ifsReportTytzFxBctService.getDate(map);
        int pageNum = ParameterUtil.getInt(map,"pageNumber",1);
        int pageSize =ParameterUtil.getInt(map,"pageSize",30);
        Page<IfsReportTytzFxBct> page=HrbReportUtils.producePage(list,pageNum,pageSize);
        return RetMsgHelper.ok(page);
    }
    /*
     * 本币同业存放
     * */
    @ResponseBody
    @RequestMapping("/call1")
    public RetMsg<PageInfo<IfsReportTytzFxBtc>> call1(@RequestBody Map<String ,Object> map){
        map.put("intodates", ParameterUtil.getString(map, "queryDate", DateUtil.getCurrentDateAsString()));
        List<IfsReportTytzFxBtc> list= ifsReportTytzFxBtcService.getDate(map);
        int pageNum = ParameterUtil.getInt(map,"pageNumber",1);
        int pageSize =ParameterUtil.getInt(map,"pageSize",30);
        Page<IfsReportTytzFxBtc> page=HrbReportUtils.producePage(list,pageNum,pageSize);
        return RetMsgHelper.ok(page);
    }



    /*
     * 本币同业拆出
     * */
    @ResponseBody
    @RequestMapping("/call4")
    public RetMsg<PageInfo<IfsReportTytzFxBtycc>> call4(@RequestBody Map<String ,Object> map){
        map.put("intodates", ParameterUtil.getString(map, "queryDate", DateUtil.getCurrentDateAsString()));
        List<IfsReportTytzFxBtycc> list= ifsReportTytzFxBtyccService.getDate(map);
        int pageNum = ParameterUtil.getInt(map,"pageNumber",1);
        int pageSize =ParameterUtil.getInt(map,"pageSize",30);
        Page<IfsReportTytzFxBtycc> page=HrbReportUtils.producePage(list,pageNum,pageSize);
        return RetMsgHelper.ok(page);
    }


    /*
     * 本币同业拆入
     * */
    @ResponseBody
    @RequestMapping("/call3")
    public RetMsg<PageInfo<IfsReportTytzFxBtycr>> call3(@RequestBody Map<String ,Object> map){
        map.put("intodates", ParameterUtil.getString(map, "queryDate", DateUtil.getCurrentDateAsString()));
        List<IfsReportTytzFxBtycr> list= ifsReportTytzFxBtycrService.getDate(map);
        int pageNum = ParameterUtil.getInt(map,"pageNumber",1);
        int pageSize =ParameterUtil.getInt(map,"pageSize",30);
        Page<IfsReportTytzFxBtycr> page=HrbReportUtils.producePage(list,pageNum,pageSize);
        return RetMsgHelper.ok(page);
    }


    /*
     * 外币存放同业
     * */
    @ResponseBody
    @RequestMapping("/call6")
    public RetMsg<PageInfo<IfsReportTytzFxWct>> call6(@RequestBody Map<String ,Object> map){
        map.put("intodates", ParameterUtil.getString(map, "queryDate", DateUtil.getCurrentDateAsString()));
        List<IfsReportTytzFxWct> list= ifsReportTytzFxWctService.getDate(map);
        int pageNum = ParameterUtil.getInt(map,"pageNumber",1);
        int pageSize =ParameterUtil.getInt(map,"pageSize",30);
        Page<IfsReportTytzFxWct> page=HrbReportUtils.producePage(list,pageNum,pageSize);
        return RetMsgHelper.ok(page);
    }


    /*
     * 外币同业存放
     * */
    @ResponseBody
    @RequestMapping("/call5")
    public RetMsg<PageInfo<IfsReportTytzFxWtc>> call5(@RequestBody Map<String ,Object> map){
        map.put("intodates", ParameterUtil.getString(map, "queryDate", DateUtil.getCurrentDateAsString()));
        List<IfsReportTytzFxWtc> list= ifsReportTytzFxWtcService.getDate(map);
        int pageNum = ParameterUtil.getInt(map,"pageNumber",1);
        int pageSize =ParameterUtil.getInt(map,"pageSize",30);
        Page<IfsReportTytzFxWtc> page=HrbReportUtils.producePage(list,pageNum,pageSize);
        return RetMsgHelper.ok(page);
    }


    /*
     * 外币同业拆出
     * */
    @ResponseBody
    @RequestMapping("/call8")
    public RetMsg<PageInfo<IfsReportTytzFxWtycc>> call8(@RequestBody Map<String ,Object> map){
        map.put("intodates", ParameterUtil.getString(map, "queryDate", DateUtil.getCurrentDateAsString()));
        List<IfsReportTytzFxWtycc> list= ifsReportTytzFxWtyccService.getDate(map);
        int pageNum = ParameterUtil.getInt(map,"pageNumber",1);
        int pageSize =ParameterUtil.getInt(map,"pageSize",30);
        Page<IfsReportTytzFxWtycc> page=HrbReportUtils.producePage(list,pageNum,pageSize);
        return RetMsgHelper.ok(page);
    }

    /*
     * 外币同业拆入
     * */
    @ResponseBody
    @RequestMapping("/call7")
    public RetMsg<PageInfo<IfsReportTytzFxWtycr>> call7(@RequestBody Map<String ,Object> map){
        map.put("intodates", ParameterUtil.getString(map, "queryDate", DateUtil.getCurrentDateAsString()));
        List<IfsReportTytzFxWtycr> list= ifsReportTytzFxWtycrService.getDate(map);
        int pageNum = ParameterUtil.getInt(map,"pageNumber",1);
        int pageSize =ParameterUtil.getInt(map,"pageSize",30);
        Page<IfsReportTytzFxWtycr> page=HrbReportUtils.producePage(list,pageNum,pageSize);
        return RetMsgHelper.ok(page);
    }



}
