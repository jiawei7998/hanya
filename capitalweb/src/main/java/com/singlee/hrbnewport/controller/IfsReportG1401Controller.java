package com.singlee.hrbnewport.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.hrbnewport.model.IfsReportG1401;
import com.singlee.hrbnewport.service.IfsReportG1401Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 *G1401大额风险暴露总体情况
 * 2021/11/25
 * @Auther:zk
 */
@Controller
@RequestMapping("/IfsReportG1401Controller")
public class IfsReportG1401Controller {

    @Autowired
    IfsReportG1401Service ifsReportG1401Service;

    @ResponseBody
    @RequestMapping("/searchAllPage")
    public RetMsg<PageInfo<IfsReportG1401>> searchAllPage(@RequestBody Map<String,Object> param){
        Page<IfsReportG1401> ifsReportG1401s = ifsReportG1401Service.searchAllPage(param);
        return RetMsgHelper.ok(ifsReportG1401s);
    }

}
