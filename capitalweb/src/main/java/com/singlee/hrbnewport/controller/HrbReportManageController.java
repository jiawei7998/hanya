package com.singlee.hrbnewport.controller;

import com.singlee.capital.common.session.SessionStaticUtil;
import com.singlee.capital.common.session.SlSession;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.interfacex.qdb.util.PathUtils;
import com.singlee.capital.jxls.JxlsBuilder;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.hrbnewport.model.TaReportLog;
import com.singlee.hrbnewport.service.CommonExportService;
import com.singlee.hrbnewport.service.TaReportLogService;
import com.singlee.hrbreport.model.TaHrbReport;
import com.singlee.hrbreport.service.ReportService;
import com.singlee.hrbreport.service.TaHrbReportService;
import com.singlee.hrbreport.util.HrbReportUtils;
import com.singlee.hrbreport.util.HrbReports;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * 自定义报表控制层
 *
 * @author
 */
@Controller
@RequestMapping(value = "/HrbReportManageController")
public class HrbReportManageController extends CommonController {
	public final static Log logger = LogFactory.getLog(HrbReportManageController.class);
	@Autowired
	TaHrbReportService taHrbReportService;

	@Autowired
	TaReportLogService taReportLogService;

	/**
	 * EXCEL报表导出
	 */
	@ResponseBody
	@RequestMapping(value = "/exportExcelReport")
	public void exportExcelReport(HttpServletRequest request, HttpServletResponse response) {
		TaReportLog taReportLog = null;
		TaHrbReport report=null;
		long startTime = System.currentTimeMillis();
		try {
			//获取参数
			Map<String, Object> param = requestParamToMap(request);
			param.put("OPICS", HrbReportUtils.OPICS);
			param.put("HRBCB", HrbReportUtils.HRBCB);
			logger.info("request转map：" + param);
			//获取日期要素
			String reportDate = ParameterUtil.getString(param, "queryDate", DateUtil.getCurrentCompactDateTimeAsString());
			/***
			 * excelName需要在前台传，格式为xxx.xls：
			 */
			String id = String.valueOf(param.get("id"));
			//获取具体文件信息
			report = taHrbReportService.searchTaHrbReport(param);
			taReportLog = this.setTaReportLog(report);
			//判断是否为空
			JY.require(report != null, "缺少报表名称或报表名称未配置！");
			//获取要导出的数据
			Map<String, Object> data = getDownloadData(param, report);
			logger.info("要导出的数据为：" + data);

			//设置模板名称
			String fileNamePath = "";
			String sheetType = ParameterUtil.getString(param, "sheetType", "");
			if (StringUtils.isNotEmpty(sheetType)) {
				fileNamePath = PathUtils.getWebRootPath() + report.getExeclName().replace(".xls", "_" + sheetType + ".xls");
			} else {
				fileNamePath = PathUtils.getWebRootPath() + report.getExeclName();

			}
			//设置导出文件名称
			String fileName = "";
			String sheetTypeValue = ParameterUtil.getString(param, "sheetTypeValue", "");
			if (StringUtils.isNotEmpty(sheetTypeValue)) {
				fileName = URLEncoder.encode(sheetTypeValue + reportDate + ".xls", "UTF-8");
			} else {
				fileName = URLEncoder.encode(report.getReportNmae() + reportDate + ".xls", "UTF-8");

			}

			//设置头信息
			response.addHeader("Content-Disposition", "attachment;filename=" + fileName);
			response.setHeader("Access-Control-Allow-Origin", "*");
			response.setHeader("Cache-Control", "no-cache");
			response.setContentType("application/x-msdownload");
			ServletOutputStream outputStream = response.getOutputStream();

			//初始化jxls对象
			JxlsBuilder jxlsBuilder = JxlsBuilder
					.getBuilder(fileNamePath)
//                    .getBuilder("D:\\恒生利融\\HrbBank\\trunk\\capitalweb\\target\\fund-capital\\WEB-INF/classes/template/hrbreport/jgbs/G21-流动性期限缺口统计表.xls")
					.putAll(data)
					.out(outputStream)
					.build();
			System.out.println("导出成功");

		} catch (Exception e) {
			taReportLog.setExportStatus("0");
			taReportLog.setRemark(e.getMessage());
			logger.error("EXCEL报表导出异常，异常信息：", e);
		} finally {
			//记录日志
			taReportLog.setTimeCost(System.currentTimeMillis() - startTime);
			taReportLogService.insert(taReportLog);
		}
	}

	/**
	 * 设置报表日志参数
	 *
	 * @return
	 */
	public TaReportLog setTaReportLog(TaHrbReport taHrbReport) {
		TaReportLog taReportLog = new TaReportLog();
		SlSession session = SessionStaticUtil.getSlSessionThreadLocal();
		taReportLog.setUserId(SlSessionHelper.getUserId(session));
		taReportLog.setUserName("");
		taReportLog.setExportName(taHrbReport.getReportNmae());
		taReportLog.setExportInst(taHrbReport.getReportInst());
		taReportLog.setExportTime(new Date());
		taReportLog.setExportStatus("1");
		taReportLog.setIp(SlSessionHelper.getIp(session));
		return taReportLog;
	}

	/**
	 * 交易页面导出
	 */
	@ResponseBody
	@RequestMapping(value = "/exportExcel")
	public void exportExcel(HttpServletRequest request, HttpServletResponse response) {
		//获取参数
		Map<String, Object> param = requestParamToMap(request);
		param.put("OPICS", HrbReportUtils.OPICS);
		param.put("HRBCB", HrbReportUtils.HRBCB);
		logger.info("request转map：" + param);
		//获取日期要素
		String reportDate = ParameterUtil.getString(param, "queryDate", DateUtil.getCurrentCompactDateTimeAsString());
		/***
		 * excelName需要在前台传，格式为xxx.xls：
		 */
		String excelName = String.valueOf(param.get("excelName"));
		//获取具体文件信息
		HrbReports report = HrbReports.getReportsByFileName(excelName);
		//判断是否为空
		JY.require(report != null, "缺少报表名称或报表名称未配置！");
		//获取要导出的数据
		Map<String, Object> data = null;
		try {
			data = getDownloadData(param);
			logger.info("要导出的数据为：" + data);
			//设置头信息
			String fileName = URLEncoder.encode(report.getDesc() + reportDate + ".xls", "UTF-8");
			response.addHeader("Content-Disposition", "attachment;filename=" + fileName);
			response.setHeader("Access-Control-Allow-Origin", "*");
			response.setHeader("Cache-Control", "no-cache");
			response.setContentType("application/x-msdownload");
			ServletOutputStream outputStream = response.getOutputStream();
			//初始化jxls对象
			JxlsBuilder jxlsBuilder = JxlsBuilder
					.getBuilder(HrbReports.getReportModel(report))
					.putAll(data)
					.out(outputStream)
					.build();
			System.out.println("导出成功");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}


	/***
	 * 根据beanNo获取要导出的数据
	 * @param map
	 * @return
	 * @throws ClassNotFoundException
	 */
	private Map<String, Object> getDownloadData(Map<String, Object> map, TaHrbReport taHrbReport) throws ClassNotFoundException {
		Map<String, Object> result = new HashMap<String, Object>();

		CommonExportService commonExportService = SpringContextHolder.getBean(taHrbReport.getReportServer());
		JY.require(commonExportService != null, "请检查报表传入参数");
		result = commonExportService.getExportData(map);
		return result;
	}

	private Map<String, Object> getDownloadData(Map<String, Object> map) throws ClassNotFoundException {
		Map<String, Object> result = new HashMap<String, Object>();
		//获取具体文件信息
		TaHrbReport taHrbReport = taHrbReportService.searchTaHrbReport(map);
		ReportService reportService = SpringContextHolder.getBean(taHrbReport.getReportServer());
		JY.require(reportService != null, "请检查报表传入参数");
		result = reportService.getReportExcel(map);
		return result;
	}

	/**
	 * 读取request中的参数,转换为map对象
	 *
	 * @param request
	 * @return
	 */
	@SuppressWarnings({"unchecked", "rawtypes"})
	private HashMap requestParamToMap(HttpServletRequest request) {
		HashMap parameters = new HashMap();
		Enumeration<String> Enumeration = request.getParameterNames();
		while (Enumeration.hasMoreElements()) {
			String paramName = Enumeration.nextElement();
			String paramValue = request.getParameter(paramName);
			// 形成键值对应的map
			if (!StringUtils.isEmpty(paramValue)) {
				parameters.put(paramName, paramValue);
			}
		}
		return parameters;
	}
}
