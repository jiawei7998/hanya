package com.singlee.hrbnewport.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.hrbnewport.model.IfsReportG1402;
import com.singlee.hrbnewport.model.IfsReportG1405;
import com.singlee.hrbnewport.service.IfsReportG1402Service;
import com.singlee.hrbnewport.service.IfsReportG1405Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.Map;

/**
 * G1402-大额风险暴露
 * 2021/11/25
 * @Auther:zk
 */
@Controller
@RequestMapping("/IfsReportG1402Controller")
public class IfsReportG1402Controller {

    @Autowired
    IfsReportG1402Service ifsReportG1402Service;

    /**
     * 生成报表数据
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping("/creatG1402")
    public RetMsg<Serializable> creatG1402(@RequestBody Map<String,Object> param){
        ifsReportG1402Service.creatG1402(param);
        return RetMsgHelper.ok();
    }

    /**
     * 查询亦生成的数据
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping("/searchIfsReportG1402")
    public RetMsg<PageInfo<IfsReportG1402>> searchIfsReportG1402(@RequestBody Map<String,Object> param){
        Page<IfsReportG1402> ifsReportG1402s = ifsReportG1402Service.searchIfsReportG1402(param);
        return RetMsgHelper.ok(ifsReportG1402s);
    }
}
