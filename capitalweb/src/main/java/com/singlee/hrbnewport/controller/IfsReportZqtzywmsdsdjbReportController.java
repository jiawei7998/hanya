package com.singlee.hrbnewport.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.SlErrors;
import com.singlee.hrbnewport.model.IfsReportZqtzywmsdsdjb;
import com.singlee.hrbnewport.service.IfsReportZqtzywmsdsdjbReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;


/**
 * 债券投资业务免所得税登记表
 */
@Controller
@RequestMapping(value = "/IfsReportZqtzywmsdsdjbReportController")
public class IfsReportZqtzywmsdsdjbReportController extends CommonController {

    @Autowired
    private IfsReportZqtzywmsdsdjbReportService ifsReportZqtzywmsdsdjbReportService;

    @ResponseBody
    @RequestMapping(value = "/searchIfsReportZqtzywmsdsdjbReportPage")
    public RetMsg<PageInfo<IfsReportZqtzywmsdsdjb>> searchIfsReportZqtzywmsdsdjbReportPage(@RequestBody Map<String, String> map) {
        Page<IfsReportZqtzywmsdsdjb> page = ifsReportZqtzywmsdsdjbReportService.searchIfsReportZqtzywmsdsdjbPage(map);
        return RetMsgHelper.ok(page);
    }

    @ResponseBody
    @RequestMapping(value = "/createIfsReportZqtzywmsdsdjb")
    public RetMsg<SlOutBean> createIfsReportZqtzywmsdsdjb(@RequestBody Map<String, String> params) {
        SlOutBean msg = new SlOutBean();
        try {
            msg = ifsReportZqtzywmsdsdjbReportService.ifsReportZqtzywmsdsdjbCreate(params);
        } catch (RemoteConnectFailureException e) {
            msg.setRetCode(SlErrors.FAILED_RMI_OPICS.getErrCode());
            msg.setRetMsg(SlErrors.FAILED_RMI_OPICS.getErrMsg() + ":" + e.getMessage() + "\n");
            msg.setRetStatus(RetStatusEnum.F);
        } catch (Exception e) {
            msg.setRetCode(SlErrors.FAILED.getErrCode());
            msg.setRetMsg(SlErrors.FAILED.getErrMsg() + ":" + e.getMessage() + "\n");
            msg.setRetStatus(RetStatusEnum.F);
        }
        return RetMsgHelper.ok(msg);
    }

}
