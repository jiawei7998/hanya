package com.singlee.hrbnewport.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.hrbreport.model.MutualFundsVO;
import com.singlee.hrbreport.model.RepoDealDetailVO;
import com.singlee.hrbreport.model.SecDetailVO;
import com.singlee.hrbreport.model.SecPurchaseVO;
import com.singlee.hrbnewport.service.DealDetailService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * 业务明细_人行
 * @author zk
 *
 */
@Controller
@RequestMapping("/DealDetailController")
public class DealDetailController extends CommonController {
    
    @Autowired
    DealDetailService dealDetail;

    @ResponseBody
    @RequestMapping("/searchDetailPage")
    public RetMsg<PageInfo<T>> searchDetailPage(@RequestBody Map<String,Object> param){
        Page<T> ts = dealDetail.searchDetailPage(param);
        return RetMsgHelper.ok(ts);
    }
    
}
