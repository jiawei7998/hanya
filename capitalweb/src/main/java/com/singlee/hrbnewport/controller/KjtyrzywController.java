package com.singlee.hrbnewport.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbnewport.model.IfsReportKjtyrzyw;
import com.singlee.hrbnewport.service.IfsReportKjtyrzywService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author ：chenguo
 * @date ：Created in 2021/12/3 18:58
 * @description：2103跨境同业融资业务信息
 * @modified By：
 * @version:
 */
@Controller
@RequestMapping("/KjtyrzywController")
public class KjtyrzywController {


    @Resource
    IfsReportKjtyrzywService ifsReportKjtyrzywService;

    
    @ResponseBody
    @RequestMapping("/callKjtyrzyw")
    public RetMsg<PageInfo<IfsReportKjtyrzyw>> callKjtyrzyw(@RequestBody Map<String ,Object> map){
        map.put("intodates", ParameterUtil.getString(map, "queryDate", DateUtil.getCurrentDateAsString()));
        List<IfsReportKjtyrzyw> list= ifsReportKjtyrzywService.getDate(map);
        int pageNum = ParameterUtil.getInt(map,"pageNumber",1);
        int pageSize =ParameterUtil.getInt(map,"pageSize",30);
        Page<IfsReportKjtyrzyw> page=HrbReportUtils.producePage(list,pageNum,pageSize);
        return RetMsgHelper.ok(page);
    }



    @ResponseBody
    @RequestMapping("/upDateKjtyrzyw")
    public RetMsg<Serializable> upDateKjtyrzyw(@RequestBody List<IfsReportKjtyrzyw >list){

        if (list.size()>0){
            for (IfsReportKjtyrzyw ifsReportKjtyrzyw:list){
                ifsReportKjtyrzywService.upDate(ifsReportKjtyrzyw);
            }
        }
        return RetMsgHelper.ok();
    }
}
