package com.singlee.rules.test.opics;

import com.singlee.rules.api.Condition;
import com.singlee.rules.api.Facts;

public class AcupCondition implements Condition {

	static AcupCondition acupCondition() {
		return new AcupCondition();
	}

	@Override
	public boolean evaluate(Facts facts) {
		return facts.iterator().hasNext();
	}
}
