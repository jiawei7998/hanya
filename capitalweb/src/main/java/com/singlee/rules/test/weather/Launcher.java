package com.singlee.rules.test.weather;

import com.singlee.rules.api.Facts;
import com.singlee.rules.api.Rules;
import com.singlee.rules.api.RulesEngine;
import com.singlee.rules.core.DefaultRulesEngine;

public class Launcher {

    public static void main(String[] args) {
        // define facts
        Facts facts = new Facts();
        facts.put("rain", true);

        // define rules
        WeatherRule weatherRule = new WeatherRule();
        Rules rules = new Rules();
        rules.register(weatherRule);

        // fire rules on known facts
        RulesEngine rulesEngine = new DefaultRulesEngine();
        rulesEngine.fire(rules, facts);
    }

}