package com.singlee.rules.test.fizzbuzz;

import com.singlee.rules.api.Facts;
import com.singlee.rules.api.Rules;
import com.singlee.rules.api.RulesEngine;
import com.singlee.rules.core.DefaultRulesEngine;
import com.singlee.rules.core.RulesEngineParameters;

public class FizzBuzzWithEasyRules {
    public static void main(String[] args) {
        // create rules engine
        RulesEngineParameters parameters = new RulesEngineParameters()
//        			.skipOnFirstFailedRule(true)
//        			.skipOnFirstNonTriggeredRule(true)
//        			.skipOnFirstAppliedRule(true);
        
        ;// 如果第一个规则满足条件，后面的规则将不再执行
        						
        RulesEngine fizzBuzzEngine = new DefaultRulesEngine(parameters);

        // create rules
        Rules rules = new Rules();
        //rules.register(new FizzRule());
        //rules.register(new BuzzRule());
        rules.register(new FizzBuzzRule(new BuzzRule(),new FizzRule()));
        //rules.register(new NonFizzBuzzRule());

        // fire rules
        Facts facts = new Facts();
        for (int i = 1; i <= 10; i++) {
            facts.put("number", i);
            fizzBuzzEngine.fire(rules, facts);
            System.out.println();
        }
    }
}
