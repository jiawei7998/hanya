package com.singlee.rules.test.opics;

import com.singlee.rules.api.Facts;
import com.singlee.rules.api.Rule;
import com.singlee.rules.api.Rules;
import com.singlee.rules.api.RulesEngine;
import com.singlee.rules.core.InferenceRulesEngine;
import com.singlee.rules.core.RuleBuilder;
import com.singlee.rules.core.RulesEngineParameters;

import static com.singlee.rules.test.opics.AcupAction.acupAction;
import static com.singlee.rules.test.opics.AcupCondition.acupCondition;

public class AcupRules {
	public static void main(String[] args) {

		// define facts
		Facts facts = new Facts();
		Acup acup1 = new Acup();
		acup1.setBr("01");
		acup1.setTableId("SL_CCYCODE");
		acup1.setTableValue("CNY");
		acup1.setTableText("01");
		Acup acup2 = new Acup();
		acup2.setBr("01");
		acup2.setTableId("SL_CCYCODE");
		acup2.setTableValue("GBP");
		acup2.setTableText("12");
		facts.put("acup1", acup1);// 规则集合
		facts.put("acup2", acup2);// 规则集合

		// define rules
		Rule acupRule = new RuleBuilder().name("opics规则匹配集合").when(acupCondition()).then(acupAction()).build();
		Rules rules = new Rules();
		rules.register(acupRule);
		// 规则引擎参数
		RulesEngineParameters parameters = new RulesEngineParameters();
		// 采用推理规则引擎
		RulesEngine acupEngine = new InferenceRulesEngine(parameters);
		// 执行规则
		acupEngine.fire(rules, facts);
		
	}
}
