package com.singlee.rules.test.opics;

import java.util.Iterator;
import java.util.Map;

import com.singlee.rules.api.Action;
import com.singlee.rules.api.Facts;

public class AcupAction implements Action {

	static AcupAction acupAction() {
		return new AcupAction();
	}

	@Override
	public void execute(Facts facts) throws Exception {
		Iterator<Map.Entry<String, Object>> it = facts.iterator();
		Map.Entry<String, Object> entry = it.next();
		System.out.println("key= " + entry.getKey() + " and value= " + entry.getValue());
		// ............TODO
		// 执行完毕后移除改条件，否则引起无限循环
		facts.remove(entry.getKey());
	}

}
