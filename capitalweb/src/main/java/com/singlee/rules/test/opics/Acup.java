package com.singlee.rules.test.opics;

/**
 * sl_gent表达式
 * 
 * @author shenzl
 * 
 */
public class Acup {
	// 渠道
	String br;
	// 模块
	String tableId;
	// 表达式
	String tableValue;
	//
	String tableText;
	// 匹配结果
	String text;

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getTableId() {
		return tableId;
	}

	public void setTableId(String tableId) {
		this.tableId = tableId;
	}

	public String getTableValue() {
		return tableValue;
	}

	public void setTableValue(String tableValue) {
		this.tableValue = tableValue;
	}

	public String getTableText() {
		return tableText;
	}

	public void setTableText(String tableText) {
		this.tableText = tableText;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return "Acup [br=" + br + ", tableId=" + tableId + ", tableValue=" + tableValue + ", tableText=" + tableText + ", text=" + text + "]";
	}

}
