package com.singlee.rules.test.helloworld;

import com.singlee.rules.api.Facts;
import com.singlee.rules.api.Rules;
import com.singlee.rules.api.RulesEngine;
import com.singlee.rules.core.DefaultRulesEngine;

public class Launcher {

    public static void main(String[] args) {

        // create facts
        Facts facts = new Facts();

        // create rules
        Rules rules = new Rules();
        rules.register(new HelloWorldRule());

        // create a rules engine and fire rules on known facts
        RulesEngine rulesEngine = new DefaultRulesEngine();
        rulesEngine.fire(rules, facts);

    }
}
