package com.singlee.rules.test.fizzbuzz;

import com.singlee.rules.annotation.*;

@Rule
public class FizzRule {

    @Condition
    public boolean isFizz(@Fact("number") Integer number) {//规则执行的条件true-执行 ，false不执行
    	return false;
    }

    @Action
    public void printFizz() {//执行具体内容
        System.out.print("fizz");
    }

    @Priority
    public int getPriority() {//规则的顺序
        return 1;
    }
}
