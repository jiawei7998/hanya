package com.singlee.rules.test.helloworld;

import com.singlee.rules.annotation.Action;
import com.singlee.rules.annotation.Condition;
import com.singlee.rules.annotation.Rule;

@Rule(name = "Hello World rule", description = "Always say hello world")
public class HelloWorldRule {

    @Condition
    public boolean when() {
        return true;
    }

    @Action
    public void then() throws Exception {
        System.out.println("hello world");
    }

}
