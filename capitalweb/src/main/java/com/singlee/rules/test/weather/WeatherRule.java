package com.singlee.rules.test.weather;

import com.singlee.rules.annotation.Action;
import com.singlee.rules.annotation.Condition;
import com.singlee.rules.annotation.Fact;
import com.singlee.rules.annotation.Rule;

@Rule(name = "weather rule", description = "if it rains then take an umbrella" )
public class WeatherRule {

    @Condition
    public boolean itRains(@Fact("rain") boolean rain) {
        return rain;
    }
    
    @Action
    public void takeAnUmbrella() {
        System.out.println("It rains, take an umbrella!");
    }
}