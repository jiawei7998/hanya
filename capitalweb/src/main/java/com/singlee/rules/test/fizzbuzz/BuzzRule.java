package com.singlee.rules.test.fizzbuzz;

import com.singlee.rules.annotation.*;

@Rule
public class BuzzRule {

    @Condition
    public boolean isBuzz(@Fact("number") Integer number) {//规则执行的条件true-执行 ，false不执行
        return number % 7 == 0;
    }

    @Action
    public void printBuzz() {//执行具体内容
        System.out.print("buzz");
    }

    @Priority
    public int getPriority() {//规则的顺序
        return 2;
    }
}
