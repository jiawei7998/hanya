package com.singlee.rules.test.web;

import javax.servlet.http.HttpServletRequest;
import com.singlee.rules.annotation.Action;
import com.singlee.rules.annotation.Condition;
import com.singlee.rules.annotation.Fact;
import com.singlee.rules.annotation.Rule;

@Rule
public class SuspiciousRequestRule {

    static final String SUSPICIOUS = "suspicious";

    @Condition
    public boolean isSuspicious(@Fact("request") HttpServletRequest request) {
        // criteria of suspicious could be based on ip, user-agent, etc.
        // here for simplicity, it is based on the presence of a request parameter 'suspicious'
        return request.getParameter(SUSPICIOUS) != null;
    }
    
    @Action
    public void setSuspicious(@Fact("request") HttpServletRequest request) {
        request.setAttribute(SUSPICIOUS, true);
    }
}