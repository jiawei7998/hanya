package com.singlee.rules.annotation;

import java.lang.annotation.*;

@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Rule {

    /**
     * The rule name which must be unique within an rules registry.
     * @return The rule name
     */
    String name() default com.singlee.rules.api.Rule.DEFAULT_NAME;

    /**
     * The rule description.
     * @return The rule description
     */
    String description() default  com.singlee.rules.api.Rule.DEFAULT_DESCRIPTION;

}