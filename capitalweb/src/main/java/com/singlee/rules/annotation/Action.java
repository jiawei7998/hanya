package com.singlee.rules.annotation;

import java.lang.annotation.*;
import javax.persistence.Inheritance;

@Inheritance
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Action {
	int order() default 0;
}
