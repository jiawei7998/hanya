package com.singlee.rules.core;

class NoSuchFactException extends RuntimeException {

    private String missingFact;

    public NoSuchFactException(String message, String missingFact) {
        super(message);
        this.missingFact = missingFact;
    }

    public String getMissingFact() {
        return missingFact;
    }
}
