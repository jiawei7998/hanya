package com.singlee.rules.core;

import com.singlee.rules.api.Facts;
import com.singlee.rules.api.Rule;
import com.singlee.rules.api.Rules;
import com.singlee.rules.api.RulesEngineListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

class DefaultRulesEngineListener implements RulesEngineListener {

	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultRuleListener.class);

	private RulesEngineParameters parameters;

	DefaultRulesEngineListener(RulesEngineParameters parameters) {
		this.parameters = parameters;
	}

	@Override
	public void beforeEvaluate(Rules rules, Facts facts) {
		if (!rules.isEmpty()) {
			logEngineParameters();
			log(rules);
			log(facts);
			LOGGER.info("Rules evaluation started");
		} else {
			LOGGER.warn("No rules registered! Nothing to apply");
		}
	}

	@Override
	public void afterExecute(Rules rules, Facts facts) {

	}

	private void logEngineParameters() {
		LOGGER.info(parameters.toString());
	}

	private void log(Rules rules) {
		LOGGER.info("Registered rules:");
		for (Rule rule : rules) {
			LOGGER.info("Rule { name = '{}', description = '{}', priority = '{}'}", rule.getName(), rule.getDescription() + "==>" + rule.getPriority());
		}
	}

	private void log(Facts facts) {
		LOGGER.info("Known facts:");
		for (Map.Entry<String, Object> fact : facts) {
			LOGGER.info("Fact { {} : {} }", fact.getKey(), String.valueOf(fact.getValue()));
		}
	}
}
