package com.singlee.rules.api;

import java.util.*;

import com.singlee.rules.util.Objects;

import static java.lang.String.format;

public class Facts implements Iterable<Map.Entry<String, Object>> {

	private Map<String, Object> facts = new HashMap<String, Object>();
	 /**
     * Put a fact in the working memory.
     * This will replace any fact having the same name.
     *
     * @param name fact name
     * @param fact object to put in the working memory
     * @return the previous value associated with <tt>name</tt>, or
     *         <tt>null</tt> if there was no mapping for <tt>name</tt>.
     *         (A <tt>null</tt> return can also indicate that the map
     *         previously associated <tt>null</tt> with <tt>name</tt>.)
     */
    public Object put(String name, Object fact) {
    	Objects.requireNonNull(name);
        return facts.put(name, fact);
    }

    /**
     * Remove fact.
     *
     * @param name of fact to remove
     * @return the previous value associated with <tt>name</tt>, or
     *         <tt>null</tt> if there was no mapping for <tt>name</tt>.
     *         (A <tt>null</tt> return can also indicate that the map
     *         previously associated <tt>null</tt> with <tt>name</tt>.)
     */
    public Object remove(String name) {
    	Objects.requireNonNull(name);
        return facts.remove(name);
    }

    /**
     * Get a fact by name.
     *
     * @param name of the fact
     * @param <T> type of the fact
     * @return the fact having the given name, or null if there is no fact with the given name
     */
    @SuppressWarnings("unchecked")
    public <T> T get(String name) {
    	Objects.requireNonNull(name);
        return (T) facts.get(name);
    }

    /**
     * Return facts as a map.
     *
     * @return the current facts as a {@link HashMap}
     */
    public Map<String, Object> asMap() {
        return facts;
    }

    @Override
    public Iterator<Map.Entry<String, Object>> iterator() {
        return facts.entrySet().iterator();
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder("[");
        List<Map.Entry<String, Object>> entries = new ArrayList<Map.Entry<String, Object>>(facts.entrySet());
        for (int i = 0; i < entries.size(); i++) {
            Map.Entry<String, Object> entry = entries.get(i);
            stringBuilder.append(format(" { %s : %s } ", entry.getKey(), String.valueOf(entry.getValue())));
            if (i < entries.size() - 1) {
                stringBuilder.append(",");
            }
        }
        stringBuilder.append("]");
        return  stringBuilder.toString();
    }
}
