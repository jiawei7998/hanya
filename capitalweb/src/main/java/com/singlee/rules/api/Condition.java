package com.singlee.rules.api;
/**
 * This interface represents a rule's condition.
 *
 * @author Mahmoud Ben Hassine (mahmoud.benhassine@icloud.com)
 */
public interface Condition {

    /**
     * Evaluate the condition according to the known facts.
     *
     * @param facts known when evaluating the rule.
     *
     * @return true if the rule should be triggered, false otherwise
     */
    boolean evaluate(Facts facts);

    /**
     * A NoOp {@link Condition} that always returns false.
     */
    Condition FALSE = new Condition() {
        @Override
        public boolean evaluate(Facts facts) {
            return false;
        }
    };

    /**
     * A NoOp {@link Condition} that always returns true.
     */
    Condition TRUE = new Condition() {
        @Override
        public boolean evaluate(Facts facts) {
            return true;
        }
    };
}