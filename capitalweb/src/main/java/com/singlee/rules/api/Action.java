package com.singlee.rules.api;

/**
 * This interface represents a rule's action.
 *
 * @author Mahmoud Ben Hassine (mahmoud.benhassine@icloud.com)
 */
public interface Action {

    /**
     * Execute the action when the rule evaluates to true.
     *
     * @param facts known at the time of execution of the action
     * @throws Exception when unable to execute the action
     */
    void execute(Facts facts) throws Exception;
}