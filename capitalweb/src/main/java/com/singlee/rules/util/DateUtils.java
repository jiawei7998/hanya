package com.singlee.rules.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {

	private static final String format = "yyyy-MM-dd";
	private static final ThreadLocal<SimpleDateFormat> threadLocal = new
            ThreadLocal<SimpleDateFormat>();
	/**
	 * @param startDateS:小日期
	 * @param endDateS：大日期
	 * @param type  1:相差天数,2:相差月份，3.相差年份
	 * @return 计算两个日期字符串相差
	 */
	public static int sumDays(String startDateS,String endDateS,String type){
		int betweendays=0;

		SimpleDateFormat df = null;
		df = threadLocal.get();
		if (df == null){
			df = new SimpleDateFormat(format);
		}

		try {

			Date startDate=df.parse(startDateS);
			Date endDate=df.parse(endDateS);
			if("1".equals(type)){
				//计算相差的天数
				betweendays = (int) ((endDate.getTime() - startDate.getTime())/ (1000 * 3600 * 24));
			}else if("2".equals(type)){
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return betweendays;
	}
}
