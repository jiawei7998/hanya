package com.singlee.cap.base.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * 维度值
 * @author lizm
 *
 */
@Entity
@Table(name = "TBK_PARAMS_MAP")
public class TbkParamsMap implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8002094501030389384L;
	/**
	 * 参数key
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT SEQ_PARAMS_MAP.NEXTVAL FROM DUAL")
	private String pKey;
	/**
	 * 维度id
	 */
	private String paramId;
	/**
	 * 维度名称
	 */
	private String paramName;
	/**
	 * 参数值
	 */
	private String pValue;
	/**
	 * 参数值注释
	 */
	private String pValueTxt;
	/**
	 * 权重
	 */
	@Transient
	private Integer paramQz;
	
	private String lastUpdate;

	public Integer getParamQz() {
		return paramQz;
	}

	public void setParamQz(Integer paramQz) {
		this.paramQz = paramQz;
	}

	public String getpKey() {
		return pKey;
	}

	public void setpKey(String pKey) {
		this.pKey = pKey;
	}

	public String getParamId() {
		return paramId;
	}

	public void setParamId(String paramId) {
		this.paramId = paramId;
	}

	public String getParamName() {
		return paramName;
	}

	public void setParamName(String paramName) {
		this.paramName = paramName;
	}

	public String getpValue() {
		return pValue;
	}

	public void setpValue(String pValue) {
		this.pValue = pValue;
	}

	public String getpValueTxt() {
		return pValueTxt;
	}

	public void setpValueTxt(String pValueTxt) {
		this.pValueTxt = pValueTxt;
	}

	public String getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(String lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

}
