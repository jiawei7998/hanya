package com.singlee.cap.base.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
/**
 * 产品-事件-维度    场景表
 * @author louhuanqing
 *
 */
@Entity
@Table(name="TBK_ENTRY_SCENE_DETAIL")
public class TbkEntrySceneDetail implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private String dId;
	//场景ID
	private String sceneId;
	//产品事件关联ID
	private String dbId;
	//tbk_params 维度KEY
	private String pId;
	//tbk_params_map 维度VALUE
	private String pValue;
	@Transient
	private String paramsString;//维度KEY-VALUE串
	
	public String getParamsString() {
		return paramsString;
	}
	public void setParamsString(String paramsString) {
		this.paramsString = paramsString;
	}
	public String getdId() {
		return dId;
	}
	public void setdId(String dId) {
		this.dId = dId;
	}
	public String getSceneId() {
		return sceneId;
	}
	public void setSceneId(String sceneId) {
		this.sceneId = sceneId;
	}
	public String getDbId() {
		return dbId;
	}
	public void setDbId(String dbId) {
		this.dbId = dbId;
	}
	public String getpId() {
		return pId;
	}
	public void setpId(String pId) {
		this.pId = pId;
	}
	public String getpValue() {
		return pValue;
	}
	public void setpValue(String pValue) {
		this.pValue = pValue;
	}
	@Override
	public String toString() {
		return "TbkEntrySceneDetail [sceneId=" + sceneId + ", dbId=" + dbId + ", pId=" + pId + ", pValue=" + pValue
				+ "]";
	}
	

}
