package com.singlee.cap.base.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.cap.base.model.TbkSubjectDef;

/*
 * 科目表Mapper层  201805
 */
public interface TbkSubjectDefMapper extends Mapper<TbkSubjectDef> {
	/*
	 * 遍历科目表
	 */
	public Page<TbkSubjectDef> getTbkSubjectDefList(Map<String,Object> map);
	
	public Page<TbkSubjectDef> getTbkSubjectDefList(Map<String,Object> map, RowBounds rb);

	public void deleteTbkSubjectDefByTaskId(String taskId);
	
	/*
	 * 遍历科目表
	 */
	public List<TbkSubjectDef> queryTbkSubjectDefList(Map<String,Object> map);

	/*
	 * 查询条件科目及上级科目
	 */
	public List<TbkSubjectDef> getTbkSubjectAndParentList(Map<String,Object> map);
	
	/**
	 * 根据科目号 精确查询
	 * @param map
	 * @return
	 */
	public Page<TbkSubjectDef> getTbkSubjectDefSame(Map<String,Object> map);
	
	/**
	 * 根据科目号 精确查询
	 * @param map
	 * @return
	 */
	public List<TbkSubjectDef> getTbkSubjectDefDistinctFirst(Map<String,Object> map);

	public void updateBySubjCode(TbkSubjectDef ttbksubjectdef);

	public TbkSubjectDef selectBySubjCodeAndsubjName(TbkSubjectDef ttbksubjectdef);

	public Page<TbkSubjectDef> searchPageSubjectBySubjectName(
			Map<String, Object> allMap, RowBounds rowBounds);

	public void deleteBysubjCode(String string);
}
