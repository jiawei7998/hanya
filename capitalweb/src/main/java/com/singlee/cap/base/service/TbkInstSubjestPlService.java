package com.singlee.cap.base.service;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.cap.base.model.TbkInstSubjestPl;

public interface TbkInstSubjestPlService  {

	/**
	 * 分野查询产品事件
	 * @param map
	 * @param rb
	 * @return
	 */
	public Page<TbkInstSubjestPl> selectTbkInstSubjestPlPage(Map<String, Object> map,RowBounds rb);
	/**
	 * 删除
	 * @param map
	 */
	public void deleteInstSubjestPlById(Map<String, Object> map);
	/**
	 * 添加
	 * @param tbkInstSubjestPl
	 */
	public boolean addInstSubjectPl(TbkInstSubjestPl tbkInstSubjestPl) throws Exception ;
	/**
	 * 修改
	 * @param tbkInstSubjestPl
	 */
	public boolean updateTbkInstSubjestPlById(TbkInstSubjestPl tbkInstSubjestPl);
} 
