package com.singlee.cap.base.mapper;

import java.util.List;
import java.util.Map;

import com.singlee.cap.base.model.TbkEntrySceneDetail;

import tk.mybatis.mapper.common.Mapper;
/**
 * 产品/工具事件  涉及到的维度配置 201805
 * @author louhuanqing
 *
 */
public interface TbkEntrySceneDetailMapper extends Mapper<TbkEntrySceneDetail>{
	public String getEntrySceneDetailId();
	
	public void deleteAllTbkEntrySceneDetail();
	/**
	 * 根据产品及事件获得场景
	 * @param acupMap
	 * @return
	 */
	public List<TbkEntrySceneDetail> gTbkEntrySceneDetailsByPrdNoEventId(Map<String, Object> acupMap);
}
