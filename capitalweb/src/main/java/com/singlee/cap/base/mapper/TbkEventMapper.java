package com.singlee.cap.base.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.cap.base.model.TbkEvent;
import com.singlee.capital.common.spring.mybatis.MyMapper;

/**
 * 会计事件  201805
 * @author 
 *
 */
public interface TbkEventMapper extends MyMapper<TbkEvent>{ 
	
	public Page<TbkEvent> getTbkEventPage(Map<String,Object> map, RowBounds rb);
	
	public List<TbkEvent> getTbkEventPage(Map<String,Object> map);
	
	/**
	 * 检查会计事件代码表中是否有当前的会计事件代码（eventId）
	 * 
	 * @param acctCode
	 * @return
	 */
	public Integer checkTbkEventByEventId(String eventId);
	
	/**
	 * 跟新排序
	 * 
	 * @param params
	 */
	public void update (Map<String, Object> params);
	
	public int selectTotal();
	
	public List<TbkEvent> lockTbkEvent();
	/**
	 * 获得产品项下已分配的事件
	 * @param params
	 * @return
	 */
	public List<TbkEvent> getTbkProductEventByPrdNoYes(Map<String, Object> params);
	
	/**
	 * 获得产品项下未分配的事件
	 * @param params
	 * @return
	 */
	public List<TbkEvent> getTbkProductEventByPrdNoNo(Map<String, Object> params);
}
