package com.singlee.cap.base.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table(name = "TBK_PRODUCT_EVENT")
public class TbkProductEvent implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT SEQ_TBK_PRODUCT_EVENT.NEXTVAL FROM DUAL")
	private String dbId;   //自增ID
	
	private String prdNo;	//产品ID
	private String eventId;  //自增事件ID
	
	@Transient
	private String pValueTxt;
	
	@Transient
	private String pValue;
	
	@Transient
	private String eventName;
	
	
	
	
	public String getpValue() {
		return pValue;
	}
	public void setpValue(String pValue) {
		this.pValue = pValue;
	}
	public String getpValueTxt() {
		return pValueTxt;
	}
	public void setpValueTxt(String pValueTxt) {
		this.pValueTxt = pValueTxt;
	}
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public String getDbId() {
		return dbId;
	}
	public void setDbId(String dbId) {
		this.dbId = dbId;
	}
	public String getPrdNo() {
		return prdNo;
	}
	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}
	public String getEventId() {
		return eventId;
	}
	public void setEventId(String eventId) {
		this.eventId = eventId;
	}
	
	
	
}
