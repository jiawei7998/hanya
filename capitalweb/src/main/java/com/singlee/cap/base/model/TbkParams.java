package com.singlee.cap.base.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 维度
 * @author lizm
 *
 */
@Entity
@Table(name = "TBK_PARAMS")
public class TbkParams implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2789080546476207507L;
	/**
	 * 启用
	 */
	public static String activeYes = "1";
	/**
	 * 维度id
	 */
	@Id
	private String paramId;
	/**
	 * 维度名称
	 */
	private String paramName;
	/**
	 * 维度权重
	 */
	private Integer paramQz;
	/**
	 * 维度分组
	 */
	private String paramSource;
	/**
	 * 最后更新时间
	 */
	private String lastUpdate;
	
	/**
	 * 是否启用
	 */
	private String active;

	public String getParamId() {
		return paramId;
	}

	public void setParamId(String paramId) {
		this.paramId = paramId;
	}

	public String getParamName() {
		return paramName;
	}

	public void setParamName(String paramName) {
		this.paramName = paramName;
	}

	public Integer getParamQz() {
		return paramQz;
	}

	public void setParamQz(Integer paramQz) {
		this.paramQz = paramQz;
	}

	public String getParamSource() {
		return paramSource;
	}

	public void setParamSource(String paramSource) {
		this.paramSource = paramSource;
	}
	public String getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(String lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

}
