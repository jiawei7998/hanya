package com.singlee.cap.base.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.cap.base.mapper.TbkEntrySettingMapper;
import com.singlee.cap.base.mapper.TbkEventMapper;
import com.singlee.cap.base.mapper.TbkProductEventMapper;
import com.singlee.cap.base.model.TbkEntrySetting;
import com.singlee.cap.base.model.TbkEvent;
import com.singlee.cap.base.model.TbkProductEvent;
import com.singlee.cap.base.service.TbkEventService;
import com.singlee.capital.common.i18n.MsgUtils;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.dict.DictConstants;

/*
 * 会计事件表serviceIMpl
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TbkEventServiceImpl implements TbkEventService {
	@Autowired
	private TbkEventMapper tbkEventMapper;
	@Autowired
	private TbkEntrySettingMapper tbkEntrySettingMapper;
	
	@Autowired
	private  TbkProductEventMapper tbkProductEventMapper;
	@Override
	public Page<TbkEvent> getTbkEventPage(Map<String, Object> map, RowBounds rb) {
		return tbkEventMapper.getTbkEventPage(map, rb);
	}
	
	@Override
	public List<TbkEvent> getTbkEventList(Map<String, Object> map) {
		return tbkEventMapper.getTbkEventPage(map);
	}

	@Override
	public void createTbkEvent(TbkEvent tbkEvent) {
		Map<String, Object> params = new HashMap<String, Object>();
		Integer order = 0;
		params.put("eventId", tbkEvent.getEventId());
		List<TbkEvent> tbkEventList = tbkEventMapper.getTbkEventPage(params,
				ParameterUtil.getRowBounds(params));
		// 查询总条数
		int total = tbkEventMapper.selectTotal();
		if (tbkEventList != null && tbkEventList.size() != 0) {
			JY.raiseRException(MsgUtils.getMessage("会计分录事件代码已经存在"));
		} else {
			order = total + 1;
		}
		tbkEvent.setSortStr(order);
		tbkEventMapper.insert(tbkEvent);
	}

	@Override
	public void updateTbkEvent(TbkEvent tbkEvent) {
		if (tbkEventMapper.checkTbkEventByEventId(tbkEvent.getEventId()) == 0) {
			JY.raiseRException(MsgUtils.getMessage("会计分录事件代码不存在"));
		} else {
			tbkEventMapper.updateByPrimaryKey(tbkEvent);
		}
	}

	@Override
	public void deleteTbkEvent(String eventId) {
		List<TbkEntrySetting> attachList = tbkEntrySettingMapper.selectEventId(eventId);
		if (attachList != null && attachList.size() > 0) {
			JY.raise("不允许删除有分录配置引用的事件。");
		}
		tbkEventMapper.deleteByPrimaryKey(eventId);
	}

	@Override
	public Integer checkTbkEventByEventId(String eventId) {
		return tbkEventMapper.checkTbkEventByEventId(eventId);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void paixuUp(Map<String, Object> map, RowBounds rb) {
		// 上一行的信息
		Map<String, Object> map1 = (Map<String, Object>) map.get("up");
		String eventIdup = (String) map1.get("eventId"); // 上一行的编号
		int sortStrup = (Integer) map1.get("sortStr"); // 上一行的排序

		// 当前行的信息
		List<Object> list = (List<Object>) map.get("rows");
		Map<String, Object> map3 = (Map<String, Object>) list.get(0);
		TbkEvent tbkEvent = new TbkEvent();
		BeanUtil.populate(tbkEvent, map3);

		// 跟新当前行排序
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("eventId", tbkEvent.getEventId());
		params.put("sortStr", sortStrup);
		tbkEventMapper.update(params);
		// 跟新上一行排序(上移)
		HashMap<String, Object> params1 = new HashMap<String, Object>();
		params1.put("eventId", eventIdup);
		params1.put("sortStr", tbkEvent.getSortStr());
		tbkEventMapper.update(params1);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void paixuDown(Map<String, Object> map, RowBounds rb) {
		// 下一行的信息
		Map<String, Object> map1 = (Map<String, Object>) map.get("down");
		String eventIddown = (String) map1.get("eventId");
		int sortStrdown = (Integer) map1.get("sortStr");

		// 当前行的信息
		List<Object> list = (List<Object>) map.get("rows");
		Map<String, Object> map3 = (Map<String, Object>) list.get(0);
		TbkEvent tbkEvent = new TbkEvent();
		BeanUtil.populate(tbkEvent, map3);

		// 跟新当前行排序
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("eventId", tbkEvent.getEventId());
		params.put("sortStr", sortStrdown);
		tbkEventMapper.update(params);
		// 跟新下一行(下移)
		HashMap<String, Object> params1 = new HashMap<String, Object>();
		params1.put("eventId", eventIddown);
		params1.put("sortStr", tbkEvent.getSortStr());
		tbkEventMapper.update(params1);
	}

	@Override
	public List<TbkEvent> getProductEvents(Map<String, Object> map) {
		// TODO Auto-generated method stub
		String type = ParameterUtil.getString(map, "type", "");
		List<TbkEvent> events  =new ArrayList<TbkEvent>();
		if(type.equals(DictConstants.YesNo.YES)) {
			events = this.tbkEventMapper.getTbkProductEventByPrdNoYes(map);
		}
		else {
			events = this.tbkEventMapper.getTbkProductEventByPrdNoNo(map);
		}
		return events;
	}

}