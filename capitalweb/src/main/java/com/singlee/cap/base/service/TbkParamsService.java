package com.singlee.cap.base.service;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.github.pagehelper.Page;
import com.singlee.cap.base.model.TbkParams;
import com.singlee.cap.base.model.TbkParamsMap;

public interface TbkParamsService {
	/**
	 * 获取维度信息
	 * @param params
	 * @return
	 */
	public Page<TbkParams> searchParamsPage(Map<String, Object> params);

	/**
	 * 添加维度信息
	 * @param params
	 */
	public void createParams(TbkParams params);

	/**
	 * 修改维度信息
	 * @param params
	 */
	public void updateParams(TbkParams params);

	/**
	 * 删除维度信息
	 * @param paramId
	 */
	public void deleteParams(String paramId);
	
	/**
	 * @author huqin
	 * @param params
	 * @return
	 */
	public Page<TbkParams> getParamName (Map<String, Object> params);
	
	/**
	 * 查询出active = "1" 的所有记录
	 */
	List<TbkParams> getParamsActiveList(@Param("active") String active);
	/**
	 * 改变该维度信息的启用/停用状态
	 * @param paramId
	 */
	public void changeParamsStatus(String paramId);
	
	
}
