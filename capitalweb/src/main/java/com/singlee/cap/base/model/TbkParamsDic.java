package com.singlee.cap.base.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 维度字典
 * @author lizm
 *
 */
@Entity
@Table(name = "TBK_PARAMS_DIC")
public class TbkParamsDic implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 952262748667461475L;
	/**
	 * 维度id
	 */
	@Id
	private String paramId;
	/**
	 * 维度名称
	 */
	private String paramName;
	/**
	 * 维度来源
	 */
	private String paramSource;
	/**
	 * 维度说明
	 */
	private String remark;
	/**
	 * 更新时间
	 */
	private String lastUpdate;

	public String getParamId() {
		return paramId;
	}

	public void setParamId(String paramId) {
		this.paramId = paramId;
	}

	public String getParamName() {
		return paramName;
	}

	public void setParamName(String paramName) {
		this.paramName = paramName;
	}

	public String getParamSource() {
		return paramSource;
	}

	public void setParamSource(String paramSource) {
		this.paramSource = paramSource;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(String lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

}
