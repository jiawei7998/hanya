
package com.singlee.cap.base.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.cap.base.mapper.TbkEntrySceneInfoMapper;
import com.singlee.cap.base.model.TbkEntrySceneInfo;
import com.singlee.cap.base.service.TbkEntrySceneInfoService;
import com.singlee.capital.common.util.ParameterUtil;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TbkEntrySceneInfoServiceImpl implements TbkEntrySceneInfoService {
	@Autowired
	private TbkEntrySceneInfoMapper tbkEntrySceneInfoMapper;

	@Override
	public TbkEntrySceneInfo save(TbkEntrySceneInfo ttEntrySceneInfo) {
		tbkEntrySceneInfoMapper.insert(ttEntrySceneInfo);
		return ttEntrySceneInfo;
	}

	@Override
	public TbkEntrySceneInfo update(TbkEntrySceneInfo ttEntrySceneInfo) {
		tbkEntrySceneInfoMapper.updateByPrimaryKey(ttEntrySceneInfo);
		return ttEntrySceneInfo;
	}

	@Override
	public void delTbkEntrySceneInfo() {
		tbkEntrySceneInfoMapper.delTbkEntrySceneInfo();
	}

	@Override
	public List<TbkEntrySceneInfo> getMatchedEntryScene(Map<String, Object> map) {
		return tbkEntrySceneInfoMapper.getMatchedEntryScene(map);
	}

	@Override
	public String getTbkEntrySceneInfoById(String id) {
		return tbkEntrySceneInfoMapper.getTbkEntrySceneInfoById(id);
	}

	@Override
	public Page<TbkEntrySceneInfo> getTbkEntrySceneInfoPage(
			Map<String, Object> map) {
		return tbkEntrySceneInfoMapper.getTbkEntrySceneInfoList(map,ParameterUtil.getRowBounds(map));
	}

}
