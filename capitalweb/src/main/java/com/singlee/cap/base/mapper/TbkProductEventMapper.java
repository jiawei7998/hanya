package com.singlee.cap.base.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.cap.base.model.TbkProductEvent;

import tk.mybatis.mapper.common.Mapper;
/**
 * 产品/工具 对应的事件
 * @author 
 *201805
 */
public interface TbkProductEventMapper extends Mapper<TbkProductEvent> {
	
		
	/**
	 * 分野查询产品事件
	 * @param map
	 * @param rb
	 * @return
	 */
	public Page<TbkProductEvent> showAllTbkProductEvent(Map<String, Object> map,RowBounds rb);
	/**
	 * 查询产品事件
	 * @param map
	 * @return
	 */
	public List<TbkProductEvent> showAllTbkProductEvent(Map<String, Object> map);

	/**
	 * 获得产品旗下唯一的事件标识
	 * @param map
	 * @return
	 */
	public TbkProductEvent getTbkProductEventById(Map<String, Object> map);
	public void deleteTbkProductEventByProNo(Map<String, Object> map);
	/**
	 * 通过产品prdNo和eventId锁定
	 * @param map
	 * @return
	 */
	public TbkProductEvent lockTbkProductEventById(Map<String, Object> map);
}
