package com.singlee.cap.base.service;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.cap.base.model.TbkEntrySceneInfo;

/*
 *分录场景信息service层
 */
public interface TbkEntrySceneInfoService {
	/*
	 * 添加分录场景信息
	 */
	public TbkEntrySceneInfo save(TbkEntrySceneInfo tbkEntrySceneInfo);

	/*
	 * 修改分录场景信息
	 */
	public TbkEntrySceneInfo update(TbkEntrySceneInfo tbkEntrySceneInfo);

	/*
	 * 删除分录场景信息
	 */
	public void delTbkEntrySceneInfo();

	/*
	 * 查询出匹配的TbkEntrySceneInfo
	 */
	public List<TbkEntrySceneInfo> getMatchedEntryScene(Map<String, Object> map);

	public String getTbkEntrySceneInfoById(String id);
	
	public Page<TbkEntrySceneInfo> getTbkEntrySceneInfoPage(Map<String, Object> map);
}
