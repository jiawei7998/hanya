package com.singlee.cap.base.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.cap.base.mapper.TbkScenceSubjectMapper;
import com.singlee.cap.base.model.TbkScenceSubject;
import com.singlee.cap.base.service.TbkScenceSubjectService;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.ParentChildUtil;


@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TbkScenceSubjectServiceImpl implements TbkScenceSubjectService {
	@Autowired
	private TbkScenceSubjectMapper tbkScenceSubjectMapper;
	
	/**
	 * 查询所有
	 */
	@Override
	public Page<TbkScenceSubject> showAllTbkScenceSubject(
			Map<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		return tbkScenceSubjectMapper.showAllTbkScenceSubject(map, rowBounds);
	}
	
	/**
	 * 新增
	 */
	@Override
	public void addTbkScenceSubject(Map<String, Object> map) {
			TbkScenceSubject tss = new TbkScenceSubject();
			ParentChildUtil.HashMapToClass(map, tss);
			tbkScenceSubjectMapper.insert(tss);
	}
	
	/**
	 * 删除
	 */
	@Override
	public void deleteTbkScenceSubject(Map<String, Object> map) {
			tbkScenceSubjectMapper.deleteByPrimaryKey(map.get("ssId"));
	}
	
	/**
	 * 查询相同
	 */
	@Override
	public List<TbkScenceSubject> showSameTbkScenceSubject(
			Map<String, Object> map) {
		TbkScenceSubject tss = new TbkScenceSubject();
		ParentChildUtil.HashMapToClass(map, tss);
		List<TbkScenceSubject> list = tbkScenceSubjectMapper.select(tss);
		return list;
	}

}
