package com.singlee.cap.base.service;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.cap.base.model.TbkParamsMap;
import com.singlee.cap.base.model.TbkParamsMapModule;

public interface TbkParamsMapService {
	/**
	 * 获取维度值信息
	 * @param params
	 * @return
	 */
	public Page<TbkParamsMap> searchParamsMapPage(Map<String, Object> params);

	/**
	 * 通过pkey查询该维度值信息是否存在
	 * @param pKey
	 * @return
	 */
	public Integer checkParamsMapByPkey(String pKey);
	/**
	 * 通过id查询该维度值信息是否存在
	 * @param paramId
	 * @return
	 */
	public Integer checkParamsMapByParamId(String paramId);

	/**
	 * 添加维度值信息
	 * @param paramsMap
	 */
	public void createParamsMap(TbkParamsMap paramsMap);

	/**
	 * 修改维度值信息
	 * @param paramsMap
	 */
	public void updateParamsMap(TbkParamsMap paramsMap);

	/**
	 * 删除维度值信息
	 * @param pKey
	 */
	public void deleteParamsMap(String pKey);
	
	/**
	 * @author huqin
	 * @date 2017-2-23
	 */
	public List<TbkParamsMap> getPvalueTxtList (Map<String, Object> params);
 
	/**
	 * 获取一条维度值信息
	 * @param map
	 * @return
	 */
	public TbkParamsMap getParamsMap(TbkParamsMap map);

	/**
	 * @author shiting
	 * @date 2017-2-23
	 */
	public List<TbkParamsMap> searchBySourceList (Map<String, Object> params);

	public List<TbkParamsMapModule> searchParamsMapPages(
			Map<String, Object> params);
	

	}
