package com.singlee.cap.base.mapper;

import com.singlee.cap.base.model.TbkProductParams;

import java.util.List;
import java.util.Map;

import tk.mybatis.mapper.common.Mapper;
/**
 * 产品对应约束会计的维度参数 201805
 * @author louhuanqing
 *
 */
public interface TbkProductParamsMapper extends Mapper<TbkProductParams>{
	/**
	 * 删除所以的维度配置
	 */
	public void deleteAllTbkProductParams();
	/**
	 * 自动迁移老会计引擎的产品维度参数
	 */
	public void insertAllTbkProductParams();
	
	/**
	 * 通过产品id获得产品所配置的会计维度
	 * @param paramsMap
	 * @return
	 */
	public List<TbkProductParams> geTbkProductParamsByPrdNo(Map<String, Object> paramsMap);
	
}
