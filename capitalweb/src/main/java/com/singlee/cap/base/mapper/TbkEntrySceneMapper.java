package com.singlee.cap.base.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.cap.base.model.TbkEntrySceneInfo;

/*
 * 分录场景信息
 */
public interface TbkEntrySceneMapper extends Mapper<TbkEntrySceneInfo> {
	/*
	 * 分录场景信息
	 */
	public Page<TbkEntrySceneInfo> getTbkEntrySceneList(Map<String, Object> map, RowBounds rb);

	/**
	 * 取TBK_ENTRY_SCENE_INFO第一条数据 分录场景信息表
	 */
	public TbkEntrySceneInfo getTbkEntrySettingLimitFirst(Map<String, Object> map);

	/**
	 * 删除所有
	 */
	public void delTbkEntryScene();

	/**
	 * 查询出匹配的TbkEntryScene
	 */
	List<TbkEntrySceneInfo> getMatchedEntryScene(@Param("map") Map<String, Object> map);

	public String getTbkEntrySceneById(String id);
	
	public void updateSceneForContain(Map<String, Object> map);


}
