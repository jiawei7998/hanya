package com.singlee.cap.base.mapper;

import java.util.List;

import com.singlee.cap.base.vo.TradingAccountVo;

import tk.mybatis.mapper.common.Mapper;

public interface TradingAccountMapper extends Mapper<TradingAccountVo> {
			
		/**
		 * 查询所有
		 * @return
		 */
		public List<TradingAccountVo> selectAllTradingAccount();
}
