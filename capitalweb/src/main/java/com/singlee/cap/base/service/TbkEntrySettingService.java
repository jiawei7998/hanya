package com.singlee.cap.base.service;

import java.util.List;
import java.util.Map;

import jxl.Workbook;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.cap.base.model.TbkEntrySetting;


public interface TbkEntrySettingService {
	public Page<TbkEntrySetting> getBkEntrySettingList(Map<String,Object> map, RowBounds rb);
	public Page<TbkEntrySetting> getBkEntrySettingList(Map<String,Object> map);
	public TbkEntrySetting save(TbkEntrySetting tbkEntrySetting);
	public TbkEntrySetting update(TbkEntrySetting tbkEntrySetting);
	public void delTbkEntrySetting(String string);
	List<TbkEntrySetting> getEntrySettingList(String sceneId, String eventId);
	List<TbkEntrySetting> getEntrySettingList(TbkEntrySetting tbkEntrySetting);
	/**
	 * 保存维度信息
	 * 
	 * @param TbkEntrySetting 
	 * @author huqin
	 * @date 2017-4-19
	 */
	public void saveEntrySetting(Map<String,Object> map);
	/**
	 * 删除
	 * @author huqin
	 * @date 2017-4-20
	 */
	public void deleteEntrySetting(String[] eventids);
	/**
	 * 导入excel
	 * @param wb
	 * @param rebuild
	 * @return
	 */
	public String uploadDefExcel(Workbook wb, boolean rebuild);
	/**
	 * 查询分录套表
	 * 
	 * @author huqin
	 * @date 2017-4-20
	 */
	public TbkEntrySetting selectEntrySetting(Map<String, Object> params);
	
	public Page<TbkEntrySetting> getScentEntrySettingList(Map<String,Object> map, RowBounds rb);
	
	 
}
