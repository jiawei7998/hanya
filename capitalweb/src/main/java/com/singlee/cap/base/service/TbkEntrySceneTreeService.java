package com.singlee.cap.base.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.cap.base.model.TbkEntrySceneTree;

/*
 *会计分录场景树结构表service层
 */
public interface TbkEntrySceneTreeService {
	/*
	 * 获取会计分录场景树结构表数据
	 */
	public Page<TbkEntrySceneTree> getTbkEntrySceneTreeList(Map<String, Object> map);

	/*
	 * 添加会计分录场景树结构表
	 */
	public TbkEntrySceneTree save(TbkEntrySceneTree tbkEntrySceneTree);

	/*
	 * 修改会计分录场景树结构表
	 */
	public TbkEntrySceneTree update(TbkEntrySceneTree tbkEntrySceneTree);

	/*
	 * 删除会计分录场景树结构表
	 */
	public void delTbkEntrySceneTree();

	public void updateTbkEntrySceneTreeNO();

	public void updateTbkEntrySceneTree();
}
