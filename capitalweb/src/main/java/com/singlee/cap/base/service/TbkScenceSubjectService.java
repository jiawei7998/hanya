package com.singlee.cap.base.service;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.cap.base.model.TbkScenceSubject;

public interface TbkScenceSubjectService {
		
	
	
	/**
	 * 分野查询产品事件
	 * @param map
	 * @param rb
	 * @return
	 */
	public Page<TbkScenceSubject> showAllTbkScenceSubject(Map<String, Object> map);
	
	/**
	 * 新增
	 * @param map
	 */
	public void addTbkScenceSubject(Map<String, Object> map);
	
	/**
	 * 删除 
	 * @param map
	 */
	public void deleteTbkScenceSubject(Map<String, Object> map);
	
	/**
	 * 查询相同
	 * @param map
	 * @return
	 */
	public List<TbkScenceSubject> showSameTbkScenceSubject(Map<String, Object> map);
	
	
}
