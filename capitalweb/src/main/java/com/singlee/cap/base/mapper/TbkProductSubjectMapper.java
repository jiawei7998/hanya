package com.singlee.cap.base.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.cap.base.model.TbkProductSubject;

import tk.mybatis.mapper.common.Mapper;
/**
 * 产品科目操作类  201805
 * @author louhuanqing
 *
 */
public interface TbkProductSubjectMapper extends Mapper<TbkProductSubject>{
	/**
	 * 获取产品科目列表
	 * @param params
	 * @return
	 */
	public Page<TbkProductSubject> searchTbkProductSubjectsPage(Map<String, Object> params,RowBounds rb);
	/**
	 * 删除产品与科目关系
	 */
	public void deleteAllTbkProductSubject();
	/**
	 * 根据产品迁移老会计引擎产品所辖科目
	 * @param params
	 */
	public void insertTbkProductSubjectByPrdNo(Map<String, Object> params);
	public void insertTbkProductSubjects(TbkProductSubject tbkProductSubject);
}
