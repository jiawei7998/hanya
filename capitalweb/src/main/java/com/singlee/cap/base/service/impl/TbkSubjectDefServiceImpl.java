
package com.singlee.cap.base.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.cap.base.mapper.TbkSubjectDefMapper;
import com.singlee.cap.base.model.TbkEntrySetting;
import com.singlee.cap.base.model.TbkSubjectDef;
import com.singlee.cap.base.service.TbkEntrySettingService;
import com.singlee.cap.base.service.TbkSubjectDefService;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.TreeUtil;

/*
 * 科目表serviceIMple
 */
@Service
/*
 * 事物
 */
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TbkSubjectDefServiceImpl implements TbkSubjectDefService {
	/**
	 * 不能用缓存，不然在查询相应套账下的科目信息时不会弹出提示“没有查询到该账套相关的科目信息”
	 */
	// private static final String cacheName =
	// "com.singlee.cap.base.model.subjectdefs";
	/*
	 * 科目Mapper映射
	 */
	@Autowired
	private TbkSubjectDefMapper ttBkSubjectDefMapper;
	@Autowired
	private TbkEntrySettingService tbkEntrySettingService;

	/*
	 * 科目列表
	 */
	// @Cacheable(value = cacheName, key = "'whole'")
	@Override
	public List<TbkSubjectDef> getTbkSubjectDefList(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<TbkSubjectDef> page = ttBkSubjectDefMapper.getTbkSubjectDefList(map, rb);
		//return TreeUtil.mergeChildrenList(page.getResult(), "-1");
		return page;
	}
	/**
	 * 获取科目树形结构201805
	 */
	@Override
	public List<TbkSubjectDef> getTbkSubjectDefListForTree(Map<String, Object> map){
		map.put("pageSize", 99999);
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<TbkSubjectDef> page = ttBkSubjectDefMapper.getTbkSubjectDefList(map, rb);
		return TreeUtil.mergeChildrenList(page.getResult(), "-1");
	}
	/*
	 * 添加科目
	 */
	@Override
	public TbkSubjectDef creatTbkSubjectDef(TbkSubjectDef ttbksubjectdef) {
		ttBkSubjectDefMapper.insert(ttbksubjectdef);
		return null;
	}

	/*
	 * 修改科目
	 */
	@Override
	public void updateTbkSubjectDef(TbkSubjectDef ttbksubjectdef) {
		TbkSubjectDef ttbksubjectdefs = new TbkSubjectDef();
		ttbksubjectdefs = ttBkSubjectDefMapper.selectBySubjCodeAndsubjName(ttbksubjectdef);
		if (ttbksubjectdefs != null ){
		String subjCode = ttbksubjectdefs.getSubjCode();
		String subjName = ttbksubjectdefs.getSubjName();
		TbkEntrySetting entry = new TbkEntrySetting();
		entry.setSubjCode(subjCode);
		List<TbkEntrySetting> entrylist = tbkEntrySettingService.getEntrySettingList(entry);
		if (entrylist.size() > 0) {
			for (TbkEntrySetting tbkEntrySetting : entrylist) {
				tbkEntrySetting.setSubjCode(subjCode);
				tbkEntrySetting.setSubjName(subjName);
				tbkEntrySettingService.update(tbkEntrySetting);
			}
		}
		}
		ttBkSubjectDefMapper.updateBySubjCode(ttbksubjectdef);
	}

	/*
	 * 删除科目
	 */
	@Override
	public void delTbkSubjectDef(String[] dbIds) {
		for (int i = 0; i < dbIds.length; i++) {
			ttBkSubjectDefMapper.deleteByPrimaryKey(dbIds[i]);
		}

	}

	@Override
	public Page<TbkSubjectDef> getTbkSubjectDefPageList(Map<String, Object> map) {
		return ttBkSubjectDefMapper.getTbkSubjectDefList(map,ParameterUtil.getRowBounds(map));
	}
	
	@Override
	public List<TbkSubjectDef> selectAll() {
		return ttBkSubjectDefMapper.selectAll();
	}

	@Override
	public List<TbkSubjectDef> getTbkSubjectAndParentList(
			Map<String, Object> map) {
		return ttBkSubjectDefMapper.getTbkSubjectAndParentList(map);
	}

	@Override
	public Page<TbkSubjectDef> getTbkSubjectDefSameService(
			Map<String, Object> map) {
		return ttBkSubjectDefMapper.getTbkSubjectDefSame(map);
	}
	@Override
	public Page<TbkSubjectDef> searchPageSubjectBySubjectName(
			Map<String, Object> allMap) {
		return ttBkSubjectDefMapper.searchPageSubjectBySubjectName(allMap,ParameterUtil.getRowBounds(allMap));
	}
	@Override
	public void delTbkSubjectDefs(String[] dbIds) {
		for (int i = 0; i < dbIds.length; i++) {
			ttBkSubjectDefMapper.deleteBysubjCode(dbIds[i]);
		
	}
	}
}
