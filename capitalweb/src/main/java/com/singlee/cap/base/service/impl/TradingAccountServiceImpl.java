package com.singlee.cap.base.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.cap.base.mapper.TradingAccountMapper;
import com.singlee.cap.base.service.TradingAccountService;
import com.singlee.cap.base.vo.TradingAccountVo;

@Service
/*
 * 事物
 */
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TradingAccountServiceImpl implements TradingAccountService {
	@Autowired
	private TradingAccountMapper tradingAccountMapper;
	
	@Override
	public List<TradingAccountVo> selectAllTradingAccountService() {
		List<TradingAccountVo> list = tradingAccountMapper.selectAllTradingAccount();
		return list;
	}

}
