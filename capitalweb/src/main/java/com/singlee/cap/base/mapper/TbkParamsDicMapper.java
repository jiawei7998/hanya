package com.singlee.cap.base.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.cap.base.model.TbkParamsDic;

public interface TbkParamsDicMapper extends Mapper<TbkParamsDic> {
	/**
	 * 查询字典信息
	 * @param params
	 * @param rb
	 * @return
	 */
	public Page<TbkParamsDic> searchParamsDicPage(Map<String, Object> params, RowBounds rb);
}
