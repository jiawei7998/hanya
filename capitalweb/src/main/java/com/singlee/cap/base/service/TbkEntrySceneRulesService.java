package com.singlee.cap.base.service;

import java.util.Map;
import org.apache.ibatis.session.RowBounds;
import com.github.pagehelper.Page;
import com.singlee.cap.base.model.TbkEntrySceneRules;

/*
 *场景核算规则信息service层
 */
public interface TbkEntrySceneRulesService{
 
	/**
	 * 查询分录列表
	 * 含分页
	 * @param map
	 * @param rb
	 * @return
	 */
	public Page<TbkEntrySceneRules> getTbkEntrySceneRulesList(Map<String,Object> map, RowBounds rb);
	
	/**
	 * 新增
	 * 
	 * @param tbkEntrySetting - 核算规则对象
	 * @author shiting
	 * @date 2017-5-20
	 */
	public void insertTbkEntrySceneRules(Map<String, Object> map);

	/**
	 * 修改
	 * 
	 * @param tbkEntrySetting - 核算规则对象
	 * @author shiting
	 * @date 2017-5-20
	 */
	public void updateTbkEntrySceneRules(Map<String, Object> map);
	

	/**
	 * 删除
	 * 
	 * @param tbkEntrySetting - 核算规则对象
	 * @author shiting
	 * @date 2017-5-20
	 */
	public void deleteTbkEntrySceneRules(String taskId);
	
	/**
	 * 获取一个场景的核算规则
	 * @param params
	 * @return
	 */
	public TbkEntrySceneRules selectTbkEntrySceneRules(Map<String, Object> params);

}
