package com.singlee.cap.base.vo;

import com.singlee.cap.base.model.TbkEntrySetting;

public class TbkEntrySettingVo extends TbkEntrySetting{
	
	private static final long serialVersionUID = -5823925604434284966L;

	private String eventName;
	
	private String amountName;
	@Override
	public String getEventName() {
		return eventName;
	}
	@Override
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	@Override
	public String getAmountName() {
		return amountName;
	}
	@Override
	public void setAmountName(String amountName) {
		this.amountName = amountName;
	}
	
}
