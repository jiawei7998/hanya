package com.singlee.cap.base.service.impl;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.cap.base.mapper.TbkInstSubjestPlMapper;
import com.singlee.cap.base.model.TbkInstSubjestPl;
import com.singlee.cap.base.service.TbkInstSubjestPlService;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.util.JY;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TbkInstSubjestPlServiceImpl implements TbkInstSubjestPlService{

	@Autowired
	private TbkInstSubjestPlMapper tbkInstSubjestPlMapper;
	
	@Override
	public Page<TbkInstSubjestPl> selectTbkInstSubjestPlPage(
			Map<String, Object> map, RowBounds rb) {
		return tbkInstSubjestPlMapper.selectTbkInstSubjestPlPage(map, rb);
	}

	/**
	 * 
	 * 删除
	 */
	@Override
	public void deleteInstSubjestPlById(Map<String, Object> map) {
    tbkInstSubjestPlMapper.deleteByPrimaryKey(map.get("ispId"));
	}

	/**
	 * 添加
	 * 
	 */
	@Override
	public boolean addInstSubjectPl(TbkInstSubjestPl tbkInstSubjestPl) throws Exception {
		int total = tbkInstSubjestPlMapper.selectTotal(tbkInstSubjestPl);
		if(total > 0){
			//throw new RException("该");
			return false;
		}else{
		    tbkInstSubjestPlMapper.insert(tbkInstSubjestPl);
		    return true;
		}
	}

	@Override
	public boolean updateTbkInstSubjestPlById(TbkInstSubjestPl tbkInstSubjestPl) {
		int total = tbkInstSubjestPlMapper.selectTotal(tbkInstSubjestPl);
		if(total > 0){
			//throw new RException("该");
			return false;
		}else{
       tbkInstSubjestPlMapper.updateByPrimaryKey(tbkInstSubjestPl); 
            return true;
		}
	}
	
}


