package com.singlee.cap.base.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/*
 * 事件表
 */
@Entity
@Table(name = "TBK_EVENT")
public class TbkEvent implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3176215053657018436L;
	@Id
	/**
	 * 事件代码
	 */
	private String eventId;
	/**
	 * 事件名称
	 */
	private String eventName;
	/**
	 * 事件描述
	 */
	private String eventDesc;
	/**
	 * 排序
	 */
	private int sortStr;
	
	private String eventType;

	public String getEventId() {
		return eventId;
	}

	public void setEventId(String eventId) {
		this.eventId = eventId;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getEventDesc() {
		return eventDesc;
	}

	public void setEventDesc(String eventDesc) {
		this.eventDesc = eventDesc;
	}

	public int getSortStr() {
		return sortStr;
	}

	public void setSortStr(int sortStr) {
		this.sortStr = sortStr;
	}

	public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	
}
