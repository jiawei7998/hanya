package com.singlee.cap.base.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/*
 * 会计分录配置表
 */
@Entity
@Table(name = "TBK_ENTRY_SETTING_CONFIG")
public class TbkEntrySettingConfig implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5725557165397050983L;
	/*
	 *配置ID 
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY,generator="SELECT SEQ_TBK_CONFIG_DB.nextval FROM DUAL")
	private String dbId;
	private String configId;
	/*
	 *类型
	 */
	private String entryType;
	/*
	 * 类型值 
	 */
	private String entryTypeValue;
	/*
	 * 类型值 
	 */
	private String entryTypeName;
	/*
	 *类型
	 */
	private String entryType1;
	/*
	 * 类型值 
	 */
	private String entryType1Value;
	/*
	 * 类型值 
	 */
	private String entryType1Name;
	/*
	 *类型
	 */
	private String entryType2;
	/*
	 * 类型值 
	 */
	private String entryType2Value;
	/*
	 * 类型值 
	 */
	private String entryType2Name;
	/*
	 *科目类型 1-成本 2-预付款递延利息收入 3-付款项利息收入 4-应计利息
	 */
	private String entrySubjType;
	/*
	 *科目代码 
	 */
	private String subjCode;
	/*
	 *科目名称
	 */
	private String subjName;
	/*
	 *内部户序号
	 */
	private String innerAcctSn;
	/*
	 *最后更新时间
	 */
	private String updateTime;
	public String getEntryType() {
		return entryType;
	}
	public void setEntryType(String entryType) {
		this.entryType = entryType;
	}
	public String getEntryTypeValue() {
		return entryTypeValue;
	}
	public void setEntryTypeValue(String entryTypeValue) {
		this.entryTypeValue = entryTypeValue;
	}
	public String getEntrySubjType() {
		return entrySubjType;
	}
	public void setEntrySubjType(String entrySubjType) {
		this.entrySubjType = entrySubjType;
	}
	public String getSubjCode() {
		return subjCode;
	}
	public void setSubjCode(String subjCode) {
		this.subjCode = subjCode;
	}
	public String getSubjName() {
		return subjName;
	}
	public void setSubjName(String subjName) {
		this.subjName = subjName;
	}
	public String getEntryTypeName() {
		return entryTypeName;
	}
	public void setEntryTypeName(String entryTypeName) {
		this.entryTypeName = entryTypeName;
	}
	public String getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	public String getInnerAcctSn() {
		return innerAcctSn;
	}
	public void setInnerAcctSn(String innerAcctSn) {
		this.innerAcctSn = innerAcctSn;
	}
	public String getEntryType1() {
		return entryType1;
	}
	public void setEntryType1(String entryType1) {
		this.entryType1 = entryType1;
	}
	public String getEntryType1Value() {
		return entryType1Value;
	}
	public void setEntryType1Value(String entryType1Value) {
		this.entryType1Value = entryType1Value;
	}
	public String getEntryType1Name() {
		return entryType1Name;
	}
	public void setEntryType1Name(String entryType1Name) {
		this.entryType1Name = entryType1Name;
	}
	public String getEntryType2() {
		return entryType2;
	}
	public void setEntryType2(String entryType2) {
		this.entryType2 = entryType2;
	}
	public String getEntryType2Value() {
		return entryType2Value;
	}
	public void setEntryType2Value(String entryType2Value) {
		this.entryType2Value = entryType2Value;
	}
	public String getEntryType2Name() {
		return entryType2Name;
	}
	public void setEntryType2Name(String entryType2Name) {
		this.entryType2Name = entryType2Name;
	}
	public String getDbId() {
		return dbId;
	}
	public void setDbId(String dbId) {
		this.dbId = dbId;
	}
	public String getConfigId() {
		return configId;
	}
	public void setConfigId(String configId) {
		this.configId = configId;
	}

}
