package com.singlee.cap.base.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.cap.base.model.TbkAmount;
/**
 * 会计金额对应的操作类 201805
 * @author
 *
 */
public interface TbkAmountMapper extends Mapper<TbkAmount> {
	public Page<TbkAmount> getTbkAmountList(Map<String, Object> map, RowBounds rb);

	/**
	 * 检查金额指针中是否有当前指针代码
	 * 
	 * @param amountId
	 * @return
	 */
	public Integer checkTbkAmountById(String amountId);

	public Integer queryName(String amountName);
}
