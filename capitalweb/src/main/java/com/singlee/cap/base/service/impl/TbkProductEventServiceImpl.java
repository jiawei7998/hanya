package com.singlee.cap.base.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.cap.base.mapper.TbkProductEventMapper;
import com.singlee.cap.base.model.TbkProductEvent;
import com.singlee.cap.base.service.TbkProductEventService;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.ParentChildUtil;
/*
 * 事物
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TbkProductEventServiceImpl implements TbkProductEventService {
	@Autowired
	private TbkProductEventMapper tbkProductEventMapper;
	
	
	@Override
	public void addTbkProductEventService(Map<String, Object> map) {
		TbkProductEvent tpe = new TbkProductEvent();
		ParentChildUtil.HashMapToClass(map, tpe);
		tbkProductEventMapper.insert(tpe);
	}


	@Override
	public Page<TbkProductEvent> showAllTbkProductEventService(
			Map<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<TbkProductEvent> result = tbkProductEventMapper.showAllTbkProductEvent(map, rowBounds);
		return result;
	}


	@Override
	public void deleteTbkProductEventById(Map<String, Object> map) {
		tbkProductEventMapper.deleteByPrimaryKey(map.get("dbId"));
	}


	@Override
	public List<TbkProductEvent> getProductEventList(Map<String, Object> map) {
		return tbkProductEventMapper.showAllTbkProductEvent(map);
	}


	@Override
	public TbkProductEvent getTbkProductEventById(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return tbkProductEventMapper.getTbkProductEventById(map);
	}


	@Override
	public void deleteTbkProductEventByProNo(Map<String, Object> map) {
		tbkProductEventMapper.deleteTbkProductEventByProNo(map);
		
	}



}
