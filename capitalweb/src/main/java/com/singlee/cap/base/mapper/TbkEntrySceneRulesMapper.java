package com.singlee.cap.base.mapper;

import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.common.Mapper;
import com.github.pagehelper.Page;
import com.singlee.cap.base.model.TbkEntrySceneRules;

public interface TbkEntrySceneRulesMapper extends Mapper<TbkEntrySceneRules>{
 
	/**
	 * 查询分录列表
	 * 含分页
	 * @param map
	 * @param rb
	 * @return
	 */
	public Page<TbkEntrySceneRules> getTbkEntrySceneRulesList(Map<String,Object> map, RowBounds rb);
	
	/**
	 * 新增
	 * 
	 * @param tbkEntrySetting - 核算规则对象
	 * @author shiting
	 * @date 2017-5-20
	 */
	public void insertTbkEntrySceneRules(TbkEntrySceneRules tbkEntrySceneRules);

	/**
	 * 修改
	 * 
	 * @param tbkEntrySetting - 核算规则对象
	 * @author shiting
	 * @date 2017-5-20
	 */
	public void updateTbkEntrySceneRules(TbkEntrySceneRules tbkEntrySceneRules);
	

	/**
	 * 删除
	 * 
	 * @param tbkEntrySetting - 核算规则对象
	 * @author shiting
	 * @date 2017-5-20
	 */
	public void deleteTbkEntrySceneRules(String taskId);
	
	/**
	 * 根据场景删除核算规则
	 * 
	 * @param tbkEntrySetting - 核算规则对象
	 * @author shiting
	 * @date 2017-5-20
	 */
	public void deleteSceneRulesBySceneId(@Param("sceneId")String sceneId);

}
