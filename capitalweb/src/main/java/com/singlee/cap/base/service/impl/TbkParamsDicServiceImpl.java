package com.singlee.cap.base.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.cap.base.mapper.TbkParamsDicMapper;
import com.singlee.cap.base.mapper.TbkParamsMapMapper;
import com.singlee.cap.base.mapper.TbkParamsMapper;
import com.singlee.cap.base.model.TbkParams;
import com.singlee.cap.base.model.TbkParamsDic;
import com.singlee.cap.base.model.TbkParamsMap;
import com.singlee.cap.base.service.TbkParamsDicService;
import com.singlee.capital.common.i18n.MsgUtils;
import com.singlee.capital.common.log.AutoLogMethod;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;


@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TbkParamsDicServiceImpl implements TbkParamsDicService {
	@Autowired
	private TbkParamsDicMapper paramsDicMapper;
	@Autowired
	private TbkParamsMapper paramsMapper;
	@Autowired
	private TbkParamsMapMapper paramsMapMapper;
	
	
	@Override
	public Page<TbkParamsDic> searchParamsDicPage(Map<String, Object> params) {
		return paramsDicMapper.searchParamsDicPage(params, ParameterUtil.getRowBounds(params));
	}

	@Override
	@AutoLogMethod(value="新增")
	public void createParamsDic(TbkParamsDic paramsDic) {
		if(paramsDicMapper.selectByPrimaryKey(paramsDic)==null){
			paramsDic.setLastUpdate(DateUtil.getCurrentDateTimeAsString());
			paramsDicMapper.insert(paramsDic);
		}else{
			JY.raiseRException(MsgUtils.getMessage("该维度id已经存在"));
		}
		
	}

	@Override
	@AutoLogMethod(value="修改")
	public void updateParamsDic(TbkParamsDic paramsDic) {
		if(paramsDicMapper.selectByPrimaryKey(paramsDic)==null){
			JY.raiseRException(MsgUtils.getMessage("该维度id不存在"));
		}else{
			/***********
			 * 修改维度信息中的相关信息
			 */
			TbkParams params=new TbkParams();
			params.setParamId(paramsDic.getParamId());
			params.setParamName(paramsDic.getParamName());
			params.setParamSource(paramsDic.getParamSource());
			paramsMapper.updateByPrimaryKeySelective(params);
			/**********
			 * 修改维度值中的相关信息
			 */
			TbkParamsMap paramsMap=new TbkParamsMap();
			paramsMap.setParamId(paramsDic.getParamId());
			paramsMap.setParamName(paramsDic.getParamName());
			paramsMapMapper.updateParamsMapParamNameByParamId(paramsMap);
			/**
			 * 修改维度字典中的相关信息
			 */
			paramsDic.setLastUpdate(DateUtil.getCurrentDateTimeAsString());
			paramsDicMapper.updateByPrimaryKey(paramsDic);
		}
	}

	@Override
	@AutoLogMethod(value="删除")
	public void deleteParamsDic(String paramId) {
		int paramsHadCount=paramsMapper.checkParamsByParamId(paramId);
		if(paramsHadCount!=0){
			JY.raiseRException(MsgUtils.getMessage("该维度字典已在维度信息中配置，请先删除维度信息中的相关信息"));
		}else{
			if(paramsDicMapper.selectByPrimaryKey(paramId)==null){
				JY.raiseRException(MsgUtils.getMessage("该维度字典信息已被删除，请刷新页面"));
			}
			TbkParamsDic paramsDic=new TbkParamsDic();
			paramsDic.setParamId(paramId);
			paramsDicMapper.deleteByPrimaryKey(paramsDic);
		}
	}
}
