package com.singlee.cap.base.vo;

import java.io.Serializable;

public class TradingAccountVo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private String dealNo;
	private String subjCode;   //科目号
	//private String subjName;
	private double settlAmt;    //头寸
	private double debitValue;   //借
	private double creditValue;  //贷
	private double subjBalance;  //科目余额
	private double difference;   //差额
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public String getSubjCode() {
		return subjCode;
	}
	public void setSubjCode(String subjCode) {
		this.subjCode = subjCode;
	}

	
	public double getSettlAmt() {
		return settlAmt;
	}
	public void setSettlAmt(double settlAmt) {
		this.settlAmt = settlAmt;
	}
	public double getDebitValue() {
		return debitValue;
	}
	public void setDebitValue(double debitValue) {
		this.debitValue = debitValue;
	}
	public double getCreditValue() {
		return creditValue;
	}
	public void setCreditValue(double creditValue) {
		this.creditValue = creditValue;
	}
	public double getSubjBalance() {
		return subjBalance;
	}
	public void setSubjBalance(double subjBalance) {
		this.subjBalance = subjBalance;
	}
	public double getDifference() {
		return difference;
	}
	public void setDifference(double difference) {
		this.difference = difference;
	}
	
	
}
