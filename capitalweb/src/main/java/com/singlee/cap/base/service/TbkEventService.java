package com.singlee.cap.base.service;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.cap.base.model.TbkEvent;

public interface TbkEventService {
	public Page<TbkEvent> getTbkEventPage(Map<String,Object> map, RowBounds rb);
	
	/**
	 * 检查会计事件代码:判断是否有当前的会计事件代码（aKey）
	 * 
	 * @param acctCode
	 * @return
	 */
	public Integer checkTbkEventByEventId(String eventId);

	/**
	 * 添加会计事件代码
	 * 
	 * @param acctCode
	 */
	public void createTbkEvent(TbkEvent tbkEvent);

	/**
	 * 修改会计事件代码
	 * 
	 * @param acctCode
	 */
	public void updateTbkEvent(TbkEvent tbkEvent);

	/**
	 * 删除会计事件代码，删除条件为aKey
	 * 
	 * @param seq
	 */
	public void deleteTbkEvent(String eventId);
	
	public void paixuUp(Map<String,Object> map, RowBounds rb);
	public void paixuDown(Map<String,Object> map, RowBounds rb);

	List<TbkEvent> getTbkEventList(Map<String, Object> map);

	/**
	 * 获得产品项下的事件
	 * @param map
	 * @return
	 */
	List<TbkEvent> getProductEvents(Map<String, Object> map);


}
