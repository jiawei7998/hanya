
package com.singlee.cap.base.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.cap.base.mapper.TbkAmountMapper;
import com.singlee.cap.base.model.TbkAmount;
import com.singlee.cap.base.service.TbkAmountService;
import com.singlee.capital.common.i18n.MsgUtils;
import com.singlee.capital.common.util.JY;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TbkAmountServiceImpl implements TbkAmountService {
	@Autowired
	private TbkAmountMapper tbkAmountMapper;

	@Override
	public Page<TbkAmount> getTbkAmountList(Map<String, Object> map, RowBounds rb) {
		return tbkAmountMapper.getTbkAmountList(map, rb);//
	}

	@Override
	public TbkAmount getAmountById(String amountId) {
		return tbkAmountMapper.selectByPrimaryKey(amountId);//
	}
	@Override
	public List<TbkAmount> getAllTbkAmount() {
		return tbkAmountMapper.selectAll();//
	}
	@Override
	public void createAmount(TbkAmount tbkAmount) {
		if (tbkAmountMapper.checkTbkAmountById(tbkAmount.getAmountId()) == 0) {
			tbkAmountMapper.insert(tbkAmount);
		} else {
			JY.raiseRException(MsgUtils.getMessage("金额代码已经存在"));
		}

	}

	@Override
	public void updateAmount(TbkAmount tbkAmount) {
		if (tbkAmountMapper.checkTbkAmountById(tbkAmount.getAmountId()) == 0) {
			JY.raiseRException(MsgUtils.getMessage("金额代码不存在"));
		} else {
			tbkAmountMapper.updateByPrimaryKey(tbkAmount);
		}
	}

	@Override
	public void deleteAmount(String amountId) {
		tbkAmountMapper.deleteByPrimaryKey(amountId);
	}

		
	@Override
	public void queryName(String amountName) {
		if(tbkAmountMapper.queryName(amountName) != 0){
			JY.raiseRException(MsgUtils.getMessage("金额名称已存在"));
		
	}

}

}