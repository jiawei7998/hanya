package com.singlee.cap.base.mapper;

import java.util.List;
import java.util.Map;

import com.singlee.cap.base.model.TbkEntrySceneAcup;

import tk.mybatis.mapper.common.Mapper;
/**
 * 产品/工具事件  涉及到的维度配置场景分录 201805
 * @author louhuanqing
 *
 */
public interface TbkEntrySceneAcupMapper  extends Mapper<TbkEntrySceneAcup>{
	
	public void deleteAllTbkEntrySceneAcup();
	/**
	 * 通过维度场景进行遍历会计分录配置
	 * @param scenesMap  List("list")
	 * @return
	 */
	public List<TbkEntrySceneAcup> getTbkEntrySceneAcups(Map<String, Object> scenesMap);

}
