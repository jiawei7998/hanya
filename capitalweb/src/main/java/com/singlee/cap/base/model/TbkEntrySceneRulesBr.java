package com.singlee.cap.base.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

/*
 * 场景核算规则表
 */
@Entity
@Table(name = "TBK_ENTRY_SCENE_RULES")
public class TbkEntrySceneRulesBr implements Serializable {
	private static final long serialVersionUID = -6413559444440533876L;
	/*
	 * 账套ID
	 */
	private String taskId;
	/*
	 * 科目性质
	 */
	private String subjNature;
	/*
	 * 科目代码
	 */
	private String subjCode;
	/*
	 * 科目名称
	 */
	private String subjName;
	
	public String getSubjNature() {
		return subjNature;
	}
	public void setSubjNature(String subjNature) {
		this.subjNature = subjNature;
	}
	public String getSubjCode() {
		return subjCode;
	}
	public void setSubjCode(String subjCode) {
		this.subjCode = subjCode;
	}
	public String getSubjName() {
		return subjName;
	}
	public void setSubjName(String subjName) {
		this.subjName = subjName;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
}
