package com.singlee.cap.base.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.cap.base.mapper.TbkProductSubjectMapper;
import com.singlee.cap.base.model.TbkProductSubject;
import com.singlee.cap.base.service.TbkProductSubjectService;
import com.singlee.capital.common.i18n.MsgUtils;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TbkProductSubjectServiceImpl implements TbkProductSubjectService {
	@Autowired
	private TbkProductSubjectMapper tbkProductSubjectMapper;
	@Override
	public Page<TbkProductSubject> searchProductSubjectsPage(Map<String, Object> params) {
		// TODO Auto-generated method stub
		return tbkProductSubjectMapper.searchTbkProductSubjectsPage(params,ParameterUtil.getRowBounds(params));
	}
	@Override
	public void deleteProductSubjectPage(String ssId) {
		// TODO Auto-generated method stu
		tbkProductSubjectMapper.deleteByPrimaryKey(ssId);
	}
	@Override
	public void addProductSubjectPage(TbkProductSubject tbkProductSubject) {
		// TODO Auto-generated method stub
		if(tbkProductSubjectMapper.selectByPrimaryKey(tbkProductSubject.getSsId()) == null){
			tbkProductSubjectMapper.insertTbkProductSubjects(tbkProductSubject);
		} else {
			JY.raiseRException(MsgUtils.getMessage("科目代码已经存在"));		}
	}
	@Override
	public void updateProductSubjectPage(TbkProductSubject tbkProductSubject) {
		if(tbkProductSubjectMapper.selectByPrimaryKey(tbkProductSubject.getSsId()) != null){
			tbkProductSubjectMapper.updateByPrimaryKey(tbkProductSubject);
		} else {
			JY.raiseRException(MsgUtils.getMessage("科目代码不存在"));		}
	}

}
