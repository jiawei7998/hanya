package com.singlee.cap.base.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/*
 * 科目表
 */
@Entity
@Table(name = "TBK_ENTRY_SCENE_INFO")
public class TbkEntrySceneInfo implements Serializable {
	private static final long serialVersionUID = -6413559444440533876L;
	/*
	 * 账套ID
	 */
	private String taskId;
	/*
	 * 场景id
	 */
	@Id
	private String sceneId;
	
	private String streeNodeId;
	/*
	 * 场景描述
	 */
	private String sceneDescr;
	/*
	 * 是否包含分录
	 */
	private String containEntry;
	/*
	 * 记账机构
	 */
	private String bookInsId;
	/*
	 * 币种
	 */
	private String ccy1;
	/*
	 * 交易对手大类
	 */
	private String partyClass;
	/*
	 * 交易对手小类
	 */
	private String partySubclass;
	/*
	 * 会记四分类
	 */
	private String invType;
	/*
	 * 资产类型
	 */
	private String aType;
	/*
	 * 产品类型
	 */
	private String pClass;
	/*
	 * 标的物aType
	 */
	private String subjectMatterAType;
	/*
	 * 标的物m_type
	 */
	private String subjectMatterPClass;
	/*
	 * 
	 */
	private String trdType;
	/*
	 * 交易 atype
	 */
	private String trdAType;
	/*
	 * 收息方式
	 * */
	private String intType; 
	/*
	 * 付息频率
	 * */ 
	private String intFre; 
	/*
	 * 盈亏
	 * */ 
	private String lossGainFlag;

	private String paramI0;
	
	private String paramI1;	

	private String paramI2;

	private String paramI3;

	private String paramI4;

	private String paramI5;

	private String paramI6;

	private String paramI7;

	private String paramI8;

	private String paramI9;
	
	public String getTrdType() {
		return trdType;
	}

	public void setTrdType(String trdType) {
		this.trdType = trdType;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getSceneId() {
		return sceneId;
	}

	public void setSceneId(String sceneId) {
		this.sceneId = sceneId;
	}

	public String getStreeNodeId() {
		return streeNodeId;
	}

	public void setStreeNodeId(String streeNodeId) {
		this.streeNodeId = streeNodeId;
	}

	public String getContainEntry() {
		return containEntry;
	}

	public void setContainEntry(String containEntry) {
		this.containEntry = containEntry;
	}

	public String getBookInsId() {
		return bookInsId;
	}

	public void setBookInsId(String bookInsId) {
		this.bookInsId = bookInsId;
	}

	public String getCcy1() {
		return ccy1;
	}

	public void setCcy1(String ccy1) {
		this.ccy1 = ccy1;
	}

	public void setaType(String aType) {
		this.aType = aType;
	}

	public void setpClass(String pClass) {
		this.pClass = pClass;
	}

	public String getPartyClass() {
		return partyClass;
	}

	public void setPartyClass(String partyClass) {
		this.partyClass = partyClass;
	}

	public String getPartySubclass() {
		return partySubclass;
	}

	public void setPartySubclass(String partySubclass) {
		this.partySubclass = partySubclass;
	}

	public String getInvType() {
		return invType;
	}

	public void setInvType(String invType) {
		this.invType = invType;
	}

	public String getaType() {
		return aType;
	}

	public void setAType(String aType) {
		this.aType = aType;
	}

	public String getpClass() {
		return pClass;
	}

	public void setPClass(String pClass) {
		this.pClass = pClass;
	}

	public String getSubjectMatterAType() {
		return subjectMatterAType;
	}

	public void setSubjectMatterAType(String subjectMatterAType) {
		this.subjectMatterAType = subjectMatterAType;
	}

	public String getSubjectMatterPClass() {
		return subjectMatterPClass;
	}

	public void setSubjectMatterPClass(String subjectMatterPClass) {
		this.subjectMatterPClass = subjectMatterPClass;
	}

	public String getSceneDescr() {
		return sceneDescr;
	}

	public void setSceneDescr(String sceneDescr) {
		this.sceneDescr = sceneDescr;
	}

	public String getIntType() {
		return intType;
	}

	public void setIntType(String intType) {
		this.intType = intType;
	}

	public String getIntFre() {
		return intFre;
	}

	public void setIntFre(String intFre) {
		this.intFre = intFre;
	}

	public String getLossGainFlag() {
		return lossGainFlag;
	}

	public void setLossGainFlag(String lossGainFlag) {
		this.lossGainFlag = lossGainFlag;
	}

	public String getParamI0() {
		return paramI0;
	}

	public void setParamI0(String paramI0) {
		this.paramI0 = paramI0;
	}

	public String getParamI1() {
		return paramI1;
	}

	public void setParamI1(String paramI1) {
		this.paramI1 = paramI1;
	}

	public String getParamI2() {
		return paramI2;
	}

	public void setParamI2(String paramI2) {
		this.paramI2 = paramI2;
	}

	public String getParamI3() {
		return paramI3;
	}

	public void setParamI3(String paramI3) {
		this.paramI3 = paramI3;
	}

	public String getParamI4() {
		return paramI4;
	}

	public void setParamI4(String paramI4) {
		this.paramI4 = paramI4;
	}

	public String getParamI5() {
		return paramI5;
	}

	public void setParamI5(String paramI5) {
		this.paramI5 = paramI5;
	}

	public String getParamI6() {
		return paramI6;
	}

	public void setParamI6(String paramI6) {
		this.paramI6 = paramI6;
	}

	public String getParamI7() {
		return paramI7;
	}

	public void setParamI7(String paramI7) {
		this.paramI7 = paramI7;
	}

	public String getParamI8() {
		return paramI8;
	}

	public void setParamI8(String paramI8) {
		this.paramI8 = paramI8;
	}

	public String getParamI9() {
		return paramI9;
	}

	public void setParamI9(String paramI9) {
		this.paramI9 = paramI9;
	}

	/**
	 * @return the trdAType
	 */
	public String getTrdAType() {
		return trdAType;
	}

	/**
	 * @param trdAType
	 *            the trdAType to set
	 */
	public void setTrdAType(String trdAType) {
		this.trdAType = trdAType;
	}

}
