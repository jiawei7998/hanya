package com.singlee.cap.base.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "TBK_SCENCE_SUBJECT")
public class TbkScenceSubject implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT SEQ_TBK_SCENCE_SUBJECT.NEXTVAL FROM DUAL")
	private String ssId;   //自增ID
	
	private String prdNo;       //产品编号
	private String subjCode;    //科目编号
	private String subjName;    //科目名称
	
	private String param1;
	private String paramVal1;
	private String paramVal1cn;
	private String param2;
	private String paramVal2;
	private String paramVal2cn;
	private String param3;
	private String paramVal3;
	private String paramVal3cn;
	
	private String paramVal1dv;
	private String paramVal2dv;
	private String paramVal3dv;
	
	
	
	@Transient
	private String fldType;
	
	@Transient
	private String pValueTxt;   
	
	@Transient
	private String pValue;

	
	
	
	
	

	public String getParamVal1dv() {
		return paramVal1dv;
	}

	public void setParamVal1dv(String paramVal1dv) {
		this.paramVal1dv = paramVal1dv;
	}

	public String getParamVal2dv() {
		return paramVal2dv;
	}

	public void setParamVal2dv(String paramVal2dv) {
		this.paramVal2dv = paramVal2dv;
	}

	public String getParamVal3dv() {
		return paramVal3dv;
	}

	public void setParamVal3dv(String paramVal3dv) {
		this.paramVal3dv = paramVal3dv;
	}

	public String getParamVal1cn() {
		return paramVal1cn;
	}

	public void setParamVal1cn(String paramVal1cn) {
		this.paramVal1cn = paramVal1cn;
	}

	public String getParamVal2cn() {
		return paramVal2cn;
	}

	public void setParamVal2cn(String paramVal2cn) {
		this.paramVal2cn = paramVal2cn;
	}

	public String getParamVal3cn() {
		return paramVal3cn;
	}

	public void setParamVal3cn(String paramVal3cn) {
		this.paramVal3cn = paramVal3cn;
	}

	public String getParam1() {
		return param1;
	}

	public void setParam1(String param1) {
		this.param1 = param1;
	}

	public String getParamVal1() {
		return paramVal1;
	}

	public void setParamVal1(String paramVal1) {
		this.paramVal1 = paramVal1;
	}

	public String getParam2() {
		return param2;
	}

	public void setParam2(String param2) {
		this.param2 = param2;
	}

	public String getParamVal2() {
		return paramVal2;
	}

	public void setParamVal2(String paramVal2) {
		this.paramVal2 = paramVal2;
	}

	public String getParam3() {
		return param3;
	}

	public void setParam3(String param3) {
		this.param3 = param3;
	}

	public String getParamVal3() {
		return paramVal3;
	}

	public void setParamVal3(String paramVal3) {
		this.paramVal3 = paramVal3;
	}

	public String getFldType() {
		return fldType;
	}

	public void setFldType(String fldType) {
		this.fldType = fldType;
	}

	public String getSsId() {
		return ssId;
	}

	public void setSsId(String ssId) {
		this.ssId = ssId;
	}

	public String getPrdNo() {
		return prdNo;
	}

	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}

	public String getSubjCode() {
		return subjCode;
	}

	public void setSubjCode(String subjCode) {
		this.subjCode = subjCode;
	}

	public String getSubjName() {
		return subjName;
	}

	public void setSubjName(String subjName) {
		this.subjName = subjName;
	}

	public String getpValueTxt() {
		return pValueTxt;
	}

	public void setpValueTxt(String pValueTxt) {
		this.pValueTxt = pValueTxt;
	}

	public String getpValue() {
		return pValue;
	}

	public void setpValue(String pValue) {
		this.pValue = pValue;
	}
	
	

}
