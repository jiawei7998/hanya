package com.singlee.cap.base.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/*
 * 场景核算规则表
 */
@Entity
@Table(name = "TBK_ENTRY_SCENE_RULES")
public class TbkEntrySceneRules implements Serializable {
	private static final long serialVersionUID = -6413559444440533876L;
	/*
	 * 账套ID
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY,generator="SELECT SEQ_TBK_ENTRY_SETTING.nextval FROM DUAL")
	private String taskId;
	/*
	 * 场景id
	 */
	private String sceneId;
	/*
	 * 场景描述
	 */
	private String sceneDescr;
	/*
	 * 科目性质
	 */
	private String subjNature;
	/*
	 * 科目代码
	 */
	private String subjCode;
	/*
	 * 科目名称
	 */
	private String subjName;
	/*
	 * 最后更新时间
	 */
	private String lstDate;
	
	@Transient
	private List<TbkEntrySceneRulesBr> rules_list; //用于存放多条信息
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public String getSceneId() {
		return sceneId;
	}
	public void setSceneId(String sceneId) {
		this.sceneId = sceneId;
	}
	public String getSceneDescr() {
		return sceneDescr;
	}
	public void setSceneDescr(String sceneDescr) {
		this.sceneDescr = sceneDescr;
	}
	public String getSubjNature() {
		return subjNature;
	}
	public void setSubjNature(String subjNature) {
		this.subjNature = subjNature;
	}
	public String getSubjCode() {
		return subjCode;
	}
	public void setSubjCode(String subjCode) {
		this.subjCode = subjCode;
	}
	public String getSubjName() {
		return subjName;
	}
	public void setSubjName(String subjName) {
		this.subjName = subjName;
	}
	public String getLstDate() {
		return lstDate;
	}
	public void setLstDate(String lstDate) {
		this.lstDate = lstDate;
	}
	public List<TbkEntrySceneRulesBr> getRules_list() {
		return rules_list;
	}
	public void setRules_list(List<TbkEntrySceneRulesBr> rulesList) {
		rules_list = rulesList;
	}
}
