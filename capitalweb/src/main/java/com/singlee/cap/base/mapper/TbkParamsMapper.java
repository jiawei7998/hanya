package com.singlee.cap.base.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.cap.base.model.TbkParams;
import com.singlee.cap.base.model.TbkParamsMapModule;
/**
 * 会计参数维度 201805
 * @author 
 *
 */
public interface TbkParamsMapper extends Mapper<TbkParams>{
	/**
	 * 获取维度值信息
	 * @param params
	 * @param rb
	 * @return
	 */
	public Page<TbkParams> searchParamsPage(Map<String, Object> params, RowBounds rb);

	/**
	 * 检查该维度值是否存在
	 * @param paramId
	 * @return
	 */
	public Integer checkParamsByParamId(String paramId);
	
	/**
	 * @author huqin
	 * @date 2017-2-22
	 */
	public Page<TbkParams> getParamName(Map<String, Object> params);
	
	public TbkParams getParamsByParamId(String paramId);

	/**
	 * 查询出active = "1" 的所有记录
	 */
	List<TbkParams> getParamsActiveList(@Param("active") String active);
	/**
	 * 改变维度是否启用状态	 
	 * * @param params
	 */
	public void changeParamsStatus(TbkParams params);

	List<TbkParams> getParamsList();
}
