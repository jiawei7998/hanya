package com.singlee.cap.base.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.cap.base.model.TbkScenceSubject;

public interface TbkScenceSubjectMapper extends Mapper<TbkScenceSubject> {
	
		
	/**
	 * 分野查询产品事件
	 * @param map
	 * @param rb
	 * @return
	 */
	public Page<TbkScenceSubject> showAllTbkScenceSubject(Map<String, Object> map,RowBounds rb);
	


}
