package com.singlee.cap.base.model;

import java.io.Serializable;

import javax.persistence.Table;

import javax.persistence.Entity;
/**
 * 场景对应的会计分录配置
 * @author louhuanqing
 *
 */
@Entity
@Table(name="TBK_ENTRY_SCENE_ACUP")
public class TbkEntrySceneAcup implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//场景ID
	private String sceneId;
	//借贷方向
	private String debitCreditFlag;
	//科目号
	private String subjCode;
	//科目名称
	private String subjName;
	//金额表达式
	private String expression;
	//排序规则
	private int sort;
	public String getSceneId() {
		return sceneId;
	}
	public void setSceneId(String sceneId) {
		this.sceneId = sceneId;
	}
	public String getDebitCreditFlag() {
		return debitCreditFlag;
	}
	public void setDebitCreditFlag(String debitCreditFlag) {
		this.debitCreditFlag = debitCreditFlag;
	}
	public String getSubjCode() {
		return subjCode;
	}
	public void setSubjCode(String subjCode) {
		this.subjCode = subjCode;
	}
	public String getSubjName() {
		return subjName;
	}
	public void setSubjName(String subjName) {
		this.subjName = subjName;
	}
	public String getExpression() {
		return expression;
	}
	public void setExpression(String expression) {
		this.expression = expression;
	}
	public int getSort() {
		return sort;
	}
	public void setSort(int sort) {
		this.sort = sort;
	}
	@Override
	public String toString() {
		return "TbkEntrySceneAcup [sceneId=" + sceneId + ", debitCreditFlag=" + debitCreditFlag + ", subjCode="
				+ subjCode + ", subjName=" + subjName + ", expression=" + expression + ", sort=" + sort + "]";
	}
	

}
