package com.singlee.cap.base.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.cap.base.model.TbkProductSubject;

public interface TbkProductSubjectService {
	/**
	 * 获取产品项下的科目列表
	 * @param params
	 * @return
	 */
	public Page<TbkProductSubject> searchProductSubjectsPage(Map<String, Object> params);

	public void deleteProductSubjectPage(String ssId);

	public void addProductSubjectPage(TbkProductSubject tbkProductSubject);

	public void updateProductSubjectPage(TbkProductSubject tbkProductSubject);
	
}
