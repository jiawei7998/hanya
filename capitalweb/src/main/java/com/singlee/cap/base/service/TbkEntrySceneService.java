package com.singlee.cap.base.service;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.cap.base.model.TbkEntrySceneInfo;

/*
 *分录场景信息service层
 */
public interface TbkEntrySceneService {
	/*
	 * 添加场景信息
	 */
	public void save(Map<String, Object> map);

	/*
	 * 修改分录场景信息
	 */
	public TbkEntrySceneInfo update(TbkEntrySceneInfo TbkEntryScene);

	/*
	 * 删除分录场景信息
	 */
	public void delTbkEntryScene(Map<String, Object> map);

	/*
	 * 查询出匹配的TbkEntryScene
	 */
	public List<TbkEntrySceneInfo> getMatchedEntryScene(Map<String, Object> map);

	public String getTbkEntrySceneById(String id);
	
	public Page<TbkEntrySceneInfo> getTbkEntryScenePage(Map<String, Object> map);
}
