package com.singlee.cap.base.model;

import java.io.Serializable;

import javax.persistence.Id;
import javax.persistence.Transient;
/**
 * 产品/工具  维度参数关联表
 * @author louhuanqing
 *
 */
public class TbkProductParams implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private String prdNo;//系统产品/工具
	@Id
	private String paramId;//维度参数代码
	@Transient
	private String paramName;//维度参数名称
	@Transient
	private String paramSource;//维度参数实体
	@Transient
	private String paramQz;//维度参数排序
	public String getPrdNo() {
		return prdNo;
	}
	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}
	public String getParamId() {
		return paramId;
	}
	public void setParamId(String paramId) {
		this.paramId = paramId;
	}
	public String getParamName() {
		return paramName;
	}
	public void setParamName(String paramName) {
		this.paramName = paramName;
	}
	public String getParamSource() {
		return paramSource;
	}
	public void setParamSource(String paramSource) {
		this.paramSource = paramSource;
	}
	public String getParamQz() {
		return paramQz;
	}
	public void setParamQz(String paramQz) {
		this.paramQz = paramQz;
	}
	@Override
	public String toString() {
		return "TbkProductParams [prdNo=" + prdNo + ", paramId=" + paramId + ", paramName=" + paramName
				+ ", paramSource=" + paramSource + ", paramQz=" + paramQz + "]";
	}
	
	

}
