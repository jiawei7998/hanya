package com.singlee.cap.base.service;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.cap.base.model.TbkEntrySettingConfig;


public interface TbkEntrySettingConfigService {
	
	public Page<TbkEntrySettingConfig> getEntrySettingConfigPage(Map<String,Object> map, RowBounds rb);
	
	public List<TbkEntrySettingConfig> getEntrySettingConfigList(Map<String,Object> map);
	/**
	 * 保存
	 * 
	 * @param TbkEntrySetting 
	 */
	public void saveEntrySettingConfig(Map<String,Object> map);
	/**
	 * 删除
	 */
	public void deleteEntrySettingConfig(Map<String,Object> map);
	
	public void updateEntrySettingConfig(Map<String, Object> map);
	/**
	 * 查询科目列表及动态科目信息
	 */
	public Page<TbkEntrySettingConfig> searchSubjAndConfig(Map<String,Object> map, RowBounds rb);
}
