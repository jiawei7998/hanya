package com.singlee.cap.base.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.cap.base.model.TbkParamsMap;
/**
 * 会计参数维度 对应的取值 201805
 * @author
 *
 */
public interface TbkParamsMapMapper extends Mapper<TbkParamsMap> {
	/**
	 * 获取维度值信息
	 * @param params
	 * @param rb
	 * @return
	 */
	public Page<TbkParamsMap> searchParamsMapPage(Map<String, Object> params, RowBounds rb);

	/**
	 * 通过pkey查询是否存在相应的维度值信息
	 * @param pKey
	 * @return
	 */
	public Integer checkParamsMapByPkey(String pKey);
	/**
	 * 通过维度id查询是否存在相应的维度信息
	 * @param paramId
	 * @return
	 */
	public Integer checkParamsMapByParamId(String paramId);
	
	
	/**
	 * @author huqin
	 * @param map
	 * @return
	 */
	public List<TbkParamsMap> getPvalueTxtList(Map<String, Object> map);

	/**
	 * 获取维度值信息
	 * @param map
	 * @return
	 */
	public List<TbkParamsMap> getParamsMap(TbkParamsMap map);

	/**
	 * @author huqin
	 * @param map
	 * @return
	 */
	public TbkParamsMap getParamsMapKey(TbkParamsMap map);
	
	/**
	 * 添加维度值信息
	 * @param map
	 */
	public void insertParamsMap(TbkParamsMap map);
	
	/**
	 * 修改维度值信息
	 * @param map
	 */
	public void updateParamsMap(TbkParamsMap map);
	
	/**
	 * 通过维度id修改维度名称
	 * @param paramsMap
	 */
	public void updateParamsMapParamNameByParamId(TbkParamsMap paramsMap);

	/**
	 * @author shiting
	 * @date 2017-2-23
	 */
	public List<TbkParamsMap> searchBySourceList (Map<String, Object> params);
	
	public List<TbkParamsMap> searchParamsMapByParamId(String paramId);
}
