package com.singlee.cap.base.service;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.cap.base.model.TbkAmount;

public interface TbkAmountService {
	public Page<TbkAmount> getTbkAmountList(Map<String, Object> map, RowBounds rb);

	public List<TbkAmount> getAllTbkAmount();
	
	public TbkAmount getAmountById(String amountId);

	/**
	 * 添加金额代码
	 * 
	 * @param tbkAmount
	 */
	public void createAmount(TbkAmount tbkAmount);

	/**
	 * 修改金额指针代码
	 * 
	 * @param tbkAmount
	 */
	public void updateAmount(TbkAmount tbkAmount);

	/**
	 * 删除金额代码
	 * 
	 * @param amountId
	 */
	public void deleteAmount(String amountId);

	public void queryName(String string);


}
