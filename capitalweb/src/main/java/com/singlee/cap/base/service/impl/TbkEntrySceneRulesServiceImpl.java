package com.singlee.cap.base.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.cap.base.mapper.TbkEntrySceneRulesMapper;
import com.singlee.cap.base.mapper.TbkEntrySettingMapper;
import com.singlee.cap.base.model.TbkEntrySceneRules;
import com.singlee.cap.base.model.TbkEntrySceneRulesBr;
import com.singlee.cap.base.service.TbkEntrySceneRulesService;
import com.singlee.capital.common.spring.json.FastJsonUtil;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.ParameterUtil;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TbkEntrySceneRulesServiceImpl implements TbkEntrySceneRulesService {

	@Autowired
	private TbkEntrySceneRulesMapper tbkEntrySceneRulesMapper;
	
	private TbkEntrySettingMapper tbkEntrySettingMapper;
	
	@Override
	public void deleteTbkEntrySceneRules(String taskId) {
		tbkEntrySceneRulesMapper.deleteByPrimaryKey(taskId);
	}

	@Override
	public Page<TbkEntrySceneRules> getTbkEntrySceneRulesList(
			Map<String, Object> map, RowBounds rb) {
		return tbkEntrySceneRulesMapper.getTbkEntrySceneRulesList(map, rb);
	}

	@Override
	public void insertTbkEntrySceneRules(Map<String, Object> map) {
		TbkEntrySceneRules rules = new TbkEntrySceneRules();
		BeanUtil.populate(rules, map);
		String constact_listJson = ParameterUtil.getString(map, "rules_list", "");
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("sceneId", rules.getSceneId());
		List<TbkEntrySceneRules> rlist = getTbkEntrySceneRulesList(params, ParameterUtil.getRowBounds(params)).getResult();
		List<TbkEntrySceneRulesBr> conList = FastJsonUtil.parseArrays(constact_listJson, TbkEntrySceneRulesBr.class);
		if (conList.size() > 0) {
			for (TbkEntrySceneRulesBr tbkEntrySceneRulesBr : conList) {
				rules.setSubjNature(tbkEntrySceneRulesBr.getSubjNature()); // 科目性质
				rules.setSubjCode(tbkEntrySceneRulesBr.getSubjCode());// 科目代码
				rules.setSubjName(tbkEntrySceneRulesBr.getSubjName());// 科目名称
				//如何存在，进行修改
				if(tbkEntrySceneRulesBr.getTaskId() != null && !"".equals(tbkEntrySceneRulesBr.getTaskId())){
					for (int i = 0; i < rlist.size(); i++) {
						if(rlist.get(i).getTaskId().equals(tbkEntrySceneRulesBr.getTaskId())){
							tbkEntrySceneRulesMapper.updateByPrimaryKey(rules);
							if(!rlist.get(i).getSubjCode().equals(tbkEntrySceneRulesBr.getSubjCode())){
								tbkEntrySettingMapper.updateEntrySettingByCode(tbkEntrySceneRulesBr.getSubjCode(),rlist.get(i).getSubjCode());
							}
							rlist.remove(i);
						}
					}
				}else{
					tbkEntrySceneRulesMapper.insert(rules);
				}
			}
		}
		//根据场景查询的原数据还有未剔除，说明已经被删除了
		if(rlist.size()>0){
			for (TbkEntrySceneRules esr : rlist) {
				tbkEntrySceneRulesMapper.deleteByPrimaryKey(esr);
			}
		}
	}

	@Override
	public void updateTbkEntrySceneRules(Map<String, Object> map) {

	}

	@Override
	public TbkEntrySceneRules selectTbkEntrySceneRules(Map<String, Object> params) {
		TbkEntrySceneRules rules = new TbkEntrySceneRules();
		List<TbkEntrySceneRules> list = getTbkEntrySceneRulesList(params, ParameterUtil.getRowBounds(params)).getResult();
		if (list.size() > 0) {
			rules.setSceneId(list.get(0).getSceneId());
			rules.setSceneDescr(list.get(0).getSceneDescr());
			List<TbkEntrySceneRulesBr> volist = new ArrayList<TbkEntrySceneRulesBr>();
			TbkEntrySceneRulesBr vo = null;
			for (TbkEntrySceneRules setacctDef : list) {
				HashMap<String, Object> map = BeanUtil.beanToHashMap(setacctDef);
				vo = new TbkEntrySceneRulesBr();
				BeanUtil.populate(vo, map);
				volist.add(vo);
			}
			rules.setRules_list(volist);
		}
		return rules;
	}

}
