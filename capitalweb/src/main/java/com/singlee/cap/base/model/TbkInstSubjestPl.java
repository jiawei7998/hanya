package com.singlee.cap.base.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "TBK_INST_SUBJECT_PL")
public class TbkInstSubjestPl implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY,generator="SELECT SEQ_TBK_INST_SUBJECT_PL.nextval FROM DUAL")
	private String ispId;
	/**
	 * 机构ID
	 */
	private String instId;
	/**
	 * 科目ID
	 */
	private String subjCode;
	 /**
	  * PL账号
	  */
	 private String plCode;
	 /**
	  * 币种
	  * @return
	  */
	 private String ccy;
	 
	 private String mediumType;
	 
	 private String prdNo;
	 
	 private String subjName;
	 
	 
	public String getIspId() {
		return ispId;
	}
	public void setIspId(String ispId) {
		this.ispId = ispId;
	}
	public String getInstId() {
		return instId;
	}
	public void setInstId(String instId) {
		this.instId = instId;
	}
	public String getSubjCode() {
		return subjCode;
	}
	public void setSubjCode(String subjCode) {
		this.subjCode = subjCode;
	}	
	public String getSubjName() {
		return subjName;
	}
	public void setSubjName(String subjName) {
		this.subjName = subjName;
	}
	public String getPlCode() {
		return plCode;
	}
	public void setPlCode(String plCode) {
		this.plCode = plCode;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public String getMediumType() {
		return mediumType;
	}
	public void setMediumType(String mediumType) {
		this.mediumType = mediumType;
	}
	public String getPrdNo() {
		return prdNo;
	}
	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}
    
}
