package com.singlee.cap.base.service;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.cap.base.model.TbkEntrySceneConfig;

public interface TbkEntrySceneConfigService {
	/**
	 * 查询分录场景配置，无分页
	 * 
	 * @param params
	 * @return
	 */
	public Page<TbkEntrySceneConfig> searchEntrySceneConfig(Map<String, Object> params);

	/**
	 * 查询分录场景配置，有分页
	 * 
	 * @param params
	 * @param rb
	 * @return
	 */
	public Page<TbkEntrySceneConfig> searchEntrySceneConfigPage(Map<String, Object> params, RowBounds rb);

	/**
	 * 配置分录场景信息
	 * 
	 * @param tbkEntrySceneConfig
	 */
	public void configEntryScene(TbkEntrySceneConfig tbkEntrySceneConfig);

	/**
	 * 添加一条场景分录配置信息
	 * 
	 * @param tbkEntrySceneConfig
	 */
	public void createEntrySceneConfig(TbkEntrySceneConfig tbkEntrySceneConfig);

	/**
	 * 根据场景删除场景分录配置信息 传入条件为删除条件
	 * 
	 * @param sceneId
	 */
	public void deleteEntrySceneConfigBySceneId(String sceneId);

	/**
	 * 配置分录场景信息
	 * 
	 * @param tbkEntrySceneConfig
	 */
	public void configSceneEntry(Map<String, Object> map);
	
	/**
	 * 根据场景ID 和  分录ID 删除 配置
	 * 
	 * @param sceneId
	 */
	public void deleteEntrySceneConfigByParam(TbkEntrySceneConfig tbkEntrySceneConfig);
}
