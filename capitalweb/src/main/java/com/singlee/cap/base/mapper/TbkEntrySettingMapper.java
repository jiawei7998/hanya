package com.singlee.cap.base.mapper;

import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.common.Mapper;
import com.github.pagehelper.Page;
import com.singlee.cap.base.model.TbkEntrySetting;

public interface TbkEntrySettingMapper extends Mapper<TbkEntrySetting>{
 
	/**
	 * 查询分录列表
	 * 含分页
	 * @param map
	 * @param rb
	 * @return
	 */
	public Page<TbkEntrySetting> getBkEntrySettingList(Map<String,Object> map, RowBounds rb);
	/**
	 * 查询分录列表
	 * 不含分页
	 * @param map
	 * @return
	 */
	public Page<TbkEntrySetting> getBkEntrySettingList(Map<String,Object> map);
	/**
	 *  
	 * @param SceneId 场景ID
	 * @param event 会计事件
	 * @return
	 */
	List<TbkEntrySetting> getSettingList(@Param("sceneId")String sceneId,@Param("eventId")String eventId);
	
	void delTbkEntrySetting(String string);
	/**
     * 查询会计分录
     * @author huqin
	 * @date 2017-4-19
     */
	public Page<TbkEntrySetting> getTbkEntrySettingList(Map<String,Object> map, RowBounds rb);
	
	public String querySettingGroupId();

	
	/**
	 * 新增
	 * 
	 * @param tbkEntrySetting - 会计分录套表对象
	 * @author huqin
	 * @date 2017-4-20
	 */
	public void insertEntrySetting(TbkEntrySetting tbkEntrySetting);
	public void deleteBySettingGroupId(String settingGroupId);
	
	/**
	 * 查询（删除会计事件时候   需要先查询是否有分录配置等引用，有则不允许删除）
	 * 
	 * @param tbkEntrySetting - 会计分录套表对象
	 * @author huqin
	 * @date 2017-4-24
	 */
	List<TbkEntrySetting> selectEventId(String eventId);
	/**
	 * 根据科目进行修改
	 * @param params
	 */
	public void updateEntrySettingByCode(@Param("ncode")String ncode,@Param("ocode")String ocode);
	

	/**
	 * 查询场景对应的分录列表
	 * 含分页
	 * @param map
	 * @param rb
	 * @return
	 */
	public Page<TbkEntrySetting> getScentEntrySettingList(Map<String,Object> map, RowBounds rb);
	

}
