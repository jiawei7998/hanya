package com.singlee.cap.base.model;

import java.io.Serializable;

import javax.persistence.Id;
/**
 * 定义产品对应的科目
 * 来约束产品可能的科目列表 201805
 * @author louhuanqing
 *
 */
public class TbkProductSubject implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	private String ssId;//唯一标识
	
	private String prdNo;//产品代码
	
	private String subjCode;//科目代码
	
	private String subjName;//科目名称

	public String getSsId() {
		return ssId;
	}

	public void setSsId(String ssId) {
		this.ssId = ssId;
	}

	public String getPrdNo() {
		return prdNo;
	}

	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}

	public String getSubjCode() {
		return subjCode;
	}

	public void setSubjCode(String subjCode) {
		this.subjCode = subjCode;
	}

	public String getSubjName() {
		return subjName;
	}

	public void setSubjName(String subjName) {
		this.subjName = subjName;
	}

	@Override
	public String toString() {
		return "TbkProductSubject [ssId=" + ssId + ", prdNo=" + prdNo + ", subjCode=" + subjCode + ", subjName="
				+ subjName + "]";
	}
	
	

}
