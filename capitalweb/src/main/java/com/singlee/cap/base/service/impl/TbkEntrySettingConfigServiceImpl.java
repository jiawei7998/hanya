package com.singlee.cap.base.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.singlee.cap.base.mapper.TbkEntrySettingConfigMapper;
import com.singlee.cap.base.model.TbkEntrySettingConfig;
import com.singlee.cap.base.service.TbkEntrySettingConfigService;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.ParameterUtil;

@Service
public class TbkEntrySettingConfigServiceImpl implements
		TbkEntrySettingConfigService {
	@Autowired
	private TbkEntrySettingConfigMapper tbkEntrySettingConfigMapper;
	
	@Override
	public void deleteEntrySettingConfig(Map<String, Object> map) {
		tbkEntrySettingConfigMapper.deleteByPrimaryKey(map);
	}

	@Override
	public Page<TbkEntrySettingConfig> getEntrySettingConfigPage(
			Map<String, Object> map, RowBounds rb) {
		return tbkEntrySettingConfigMapper.getTbkEntrySettingConfigList(map, ParameterUtil.getRowBounds(map));
	}

	@Override
	public List<TbkEntrySettingConfig> getEntrySettingConfigList(
			Map<String, Object> map) {
		return tbkEntrySettingConfigMapper.getTbkEntrySettingConfigList(map);
	}

	@Override
	public void saveEntrySettingConfig(Map<String, Object> map) {
		List<TbkEntrySettingConfig> list = getEntrySettingConfigList(map);
		if(list.size() > 0){
			throw new RException("该笔分录动态科目已配置！"); 
		}else{
			TbkEntrySettingConfig config = new TbkEntrySettingConfig();
			BeanUtil.populate(config, map);
			StringBuffer configId = new StringBuffer();
			configId.append("&"+config.getEntryType());
			if(!"".equals(config.getEntryType1())){
				configId.append("+"+config.getEntryType1());
			}
			if(!"".equals(config.getEntryType2())){
				configId.append("+"+config.getEntryType2());
			}
			configId.append("&"+config.getEntrySubjType());
			config.setConfigId(configId.toString());
			tbkEntrySettingConfigMapper.insert(config);
		}
	}

	@Override
	public void updateEntrySettingConfig(Map<String, Object> map) {
		TbkEntrySettingConfig config = new TbkEntrySettingConfig();
		BeanUtil.populate(config, map);
		StringBuffer configId = new StringBuffer();
		configId.append("&"+config.getEntryType());
		if(!"".equals(config.getEntryType1())){
			configId.append("+"+config.getEntryType1());
		}
		if(!"".equals(config.getEntryType2())){
			configId.append("+"+config.getEntryType2());
		}
		configId.append("&"+config.getEntrySubjType());
		config.setConfigId(configId.toString());
		tbkEntrySettingConfigMapper.updateByPrimaryKey(config);
	}

	@Override
	public Page<TbkEntrySettingConfig> searchSubjAndConfig(
			Map<String, Object> map, RowBounds rb) {
		return tbkEntrySettingConfigMapper.searchSubjAndConfig(map, rb);
	}

}
