package com.singlee.cap.base.service.impl;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.cap.base.mapper.TbkEntrySceneConfigMapper;
import com.singlee.cap.base.mapper.TbkEntrySceneMapper;
import com.singlee.cap.base.mapper.TbkEntrySceneRulesMapper;
import com.singlee.cap.base.model.TbkEntrySceneConfig;
import com.singlee.cap.base.model.TbkEntrySceneRules;
import com.singlee.cap.base.service.TbkEntrySceneConfigService;
import com.singlee.capital.common.spring.mybatis.BatchDao;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.dict.DictConstants; 

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TbkEntrySceneConfigServiceImpl implements TbkEntrySceneConfigService {
	@Autowired
	private TbkEntrySceneConfigMapper tbkEntrySceneConfigMapper;
	@Autowired
	private TbkEntrySceneMapper tbkEntrySceneMapper;
	@Autowired
	private TbkEntrySceneRulesMapper tbkEntrySceneRulesMapper;

	@Autowired
	private BatchDao batchDao;

	@Override
	public Page<TbkEntrySceneConfig> searchEntrySceneConfig(Map<String, Object> params) {
		return tbkEntrySceneConfigMapper.searchEntrySceneConfig(params);
	}

	@Override
	public Page<TbkEntrySceneConfig> searchEntrySceneConfigPage(Map<String, Object> params, RowBounds rb) {
		return tbkEntrySceneConfigMapper.searchEntrySceneConfig(params, rb);
	}

	@Override
	public void createEntrySceneConfig(TbkEntrySceneConfig tbkEntrySceneConfig) {
		tbkEntrySceneConfigMapper.createEntrySceneConfig(tbkEntrySceneConfig);

	}

	@Override
	public void deleteEntrySceneConfigBySceneId(String sceneId) {
		tbkEntrySceneConfigMapper.deleteEntrySceneConfigBySceneId(sceneId);

	}

	@Override
	public void configEntryScene(TbkEntrySceneConfig tbkEntrySceneConfig) {
		tbkEntrySceneConfigMapper.deleteEntrySceneConfigBySceneId(tbkEntrySceneConfig.getSceneId());
		String settingGroupIds = tbkEntrySceneConfig.getSettingGroupId();
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("sceneId", tbkEntrySceneConfig.getSceneId());
		if (StringUtils.isNotEmpty(settingGroupIds)) {
			map.put("containEntry", DictConstants.YesNo.YES);
			tbkEntrySceneMapper.updateSceneForContain(map);
			String[] settingGroupId = settingGroupIds.split(",");
			List<TbkEntrySceneConfig> list = new LinkedList<TbkEntrySceneConfig>();
			for (int i = 0; i < settingGroupId.length; i++) {
				TbkEntrySceneConfig entrySceneConfig = new TbkEntrySceneConfig();
				entrySceneConfig.setSceneId(tbkEntrySceneConfig.getSceneId());
				entrySceneConfig.setSettingGroupId(settingGroupId[i]);
				entrySceneConfig.setLastUpdateDate(DateUtil.getCurrentDateTimeAsString());
				list.add(entrySceneConfig);
			}
			batchDao.batch("com.singlee.cap.base.mapper.TbkEntrySceneConfigMapper.createEntrySceneConfig", list);
		} else {
			map.put("containEntry", DictConstants.YesNo.NO);
			tbkEntrySceneMapper.updateSceneForContain(map);
		}
	}

	@Override
	public void configSceneEntry(Map<String, Object> map) {
		String sceneId = ParameterUtil.getString(map, "sceneId", "");
		tbkEntrySceneConfigMapper.deleteEntrySceneConfigBySceneId(sceneId);
		String settingGroupIds = ParameterUtil.getString(map, "settingGroupId", "");
		String subjCodes = ParameterUtil.getString(map, "subjCode", "");
		if (StringUtils.isNotEmpty(settingGroupIds)) {
			String[] subjCode = subjCodes.split(",");
			String[] rulesCode = null;
			String[] settingGroupId = settingGroupIds.split(",");
			List<TbkEntrySceneRules> rulesList = tbkEntrySceneRulesMapper.getTbkEntrySceneRulesList(map, ParameterUtil.getRowBounds(map));
			if(rulesList.size() > 0){
				rulesCode = new String[rulesList.size()];
				for (int i = 0; i < rulesList.size(); i++) {
					rulesCode[i] = rulesList.get(i).getSubjCode();
				}
				if(!filter(subjCode, rulesCode)){
					JY.raise("选择的凭证信息不符合场景核算规则！");
				}
			}
			map.put("containEntry", DictConstants.YesNo.YES);
			tbkEntrySceneMapper.updateSceneForContain(map);
			List<TbkEntrySceneConfig> list = new LinkedList<TbkEntrySceneConfig>();
			for (int i = 0; i < settingGroupId.length; i++) {
				TbkEntrySceneConfig entrySceneConfig = new TbkEntrySceneConfig();
				entrySceneConfig.setSceneId(sceneId);
				entrySceneConfig.setSettingGroupId(settingGroupId[i]);
				entrySceneConfig.setLastUpdateDate(DateUtil.getCurrentDateTimeAsString());
				list.add(entrySceneConfig);
			}
			batchDao.batch("com.singlee.cap.base.mapper.TbkEntrySceneConfigMapper.createEntrySceneConfig", list);
		} else {
			map.put("containEntry", DictConstants.YesNo.NO);
			tbkEntrySceneMapper.updateSceneForContain(map);
		}
	}
	
	public boolean filter(String[] keys,String[] strings){
		boolean flag = true;
		for (String key : keys) {
			if(!Arrays.asList(strings).contains(key)){
				flag = false;
				break;
			}
		}
		return flag;
	}

	@Override
	public void deleteEntrySceneConfigByParam(
			TbkEntrySceneConfig tbkEntrySceneConfig) {
		tbkEntrySceneConfig.setLastUpdateDate("");
		tbkEntrySceneConfigMapper.delete(tbkEntrySceneConfig);
	}

}
