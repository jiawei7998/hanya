
package com.singlee.cap.base.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.cap.base.mapper.TbkEntrySceneConfigMapper;
import com.singlee.cap.base.mapper.TbkEntrySceneMapper;
import com.singlee.cap.base.mapper.TbkEntrySceneRulesMapper;
import com.singlee.cap.base.model.TbkEntrySceneInfo;
import com.singlee.cap.base.model.TbkParams;
import com.singlee.cap.base.service.TbkEntrySceneService;
import com.singlee.cap.base.service.TbkParamsService;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.dict.DictConstants;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TbkEntrySceneServiceImpl implements TbkEntrySceneService {
	@Autowired
	private TbkEntrySceneMapper TbkEntrySceneMapper;
	@Autowired
	private TbkParamsService TbkParamsService;
	@Autowired
	private TbkEntrySceneConfigMapper tbkEntrySceneConfigMapper;
	@Autowired
	private TbkEntrySceneRulesMapper tbkEntrySceneRulesMapper;

	@Override
	public void save(Map<String, Object> map) {
		TbkEntrySceneInfo scene = new TbkEntrySceneInfo();
		StringBuffer sceneDescr = new StringBuffer();
		List<TbkParams> paramsList = TbkParamsService.getParamsActiveList("1");
		for (TbkParams tbkParams : paramsList) {
			if(map.get(tbkParams.getParamSource()) != null && !"".equals(map.get(tbkParams.getParamSource()).toString())){
				map.put(tbkParams.getParamSource(), ","+map.get(tbkParams.getParamSource())+",");
			}
			String key = tbkParams.getParamSource()+"_text";
			if(map.get(key) != null && !"".equals(map.get(key))){
				sceneDescr.append("["+map.get(key).toString().substring(1, map.get(key).toString().length()-1)+"]");
			}
		}
		if(sceneDescr == null || "".equals(sceneDescr)) {
			JY.require(sceneDescr == null || "".equals(sceneDescr), "请至少选择一个维度信息！");}
		BeanUtil.populate(scene, map);
		scene.setSceneDescr(sceneDescr.toString());
		scene.setContainEntry(DictConstants.YesNo.NO);
		//修改
		if(scene.getSceneId() != null && !"".equals(scene.getSceneId())){
			TbkEntrySceneMapper.updateByPrimaryKey(scene);
		}else{
			String sceneId = DateUtil.getCurrentCompactDateTimeAsString();
			scene.setSceneId(sceneId);
			scene.setStreeNodeId(sceneId);
			//新增
			TbkEntrySceneMapper.insert(scene);
		}
	}

	@Override
	public TbkEntrySceneInfo update(TbkEntrySceneInfo ttEntrySceneInfo) {
		TbkEntrySceneMapper.updateByPrimaryKey(ttEntrySceneInfo);
		return ttEntrySceneInfo;
	}

	@Override
	public void delTbkEntryScene(Map<String, Object> map) {
		TbkEntrySceneInfo scene = new TbkEntrySceneInfo();
		BeanUtil.populate(scene, map);
		TbkEntrySceneMapper.deleteByPrimaryKey(scene);
		tbkEntrySceneConfigMapper.deleteEntrySceneConfigBySceneId(scene.getSceneId());
		tbkEntrySceneRulesMapper.deleteSceneRulesBySceneId(scene.getSceneId());
	}

	@Override
	public List<TbkEntrySceneInfo> getMatchedEntryScene(Map<String, Object> map) {
		return TbkEntrySceneMapper.getMatchedEntryScene(map);
	}

	@Override
	public String getTbkEntrySceneById(String id) {
		return TbkEntrySceneMapper.getTbkEntrySceneById(id);
	}

	@Override
	public Page<TbkEntrySceneInfo> getTbkEntryScenePage(
			Map<String, Object> map) {
		return TbkEntrySceneMapper.getTbkEntrySceneList(map,ParameterUtil.getRowBounds(map));
	}

}
