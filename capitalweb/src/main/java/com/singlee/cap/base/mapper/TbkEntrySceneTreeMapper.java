package com.singlee.cap.base.mapper;

import java.util.Map;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.cap.base.model.TbkEntrySceneTree;

/*
 * 会计分录场景树结构表Mapper层
 */
public interface TbkEntrySceneTreeMapper extends Mapper<TbkEntrySceneTree> {
	/*
	 * 获取会计分录场景树结构表数据
	 */
	public Page<TbkEntrySceneTree> getTbkEntrySceneTreeList(Map<String, Object> map);

	/**
	 * 删除会计分录场景树结构表所有数据
	 */
	public void delTbkEntrySceneTree();

	/**
	 * 修改状态
	 */
	public void updateTbkEntrySceneTreeNO();

	public void updateTbkEntrySceneTree();

	/**
	 * 根据nodeid修改是否包含分录
	 * 
	 * @param map
	 *            其中nodeId为查询条件，containEntry为修改字段
	 */
	public void updateTbkEntrySceneTreeByNodeIdForContainEntry(Map<String, Object> map);

}
