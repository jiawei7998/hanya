package com.singlee.cap.base.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 分录套表
 * 
 * @author huqin
 *
 */
@Entity
@Table(name = "TBK_ENTRY_SETTING")
public class TbkEntrySettingBr implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3623210457962456413L;
	/*
	 *借贷标识 
	 */
	private String debitCreditFlag;
	/*
	 *科目代码 
	 */
	private String subjCode;
	/*
	 *科目名称
	 */
	private String subjName;
	/*
	 *金额表达式
	 */
	private String expression;
	public String getDebitCreditFlag() {
		return debitCreditFlag;
	}
	public void setDebitCreditFlag(String debitCreditFlag) {
		this.debitCreditFlag = debitCreditFlag;
	}
	public String getSubjCode() {
		return subjCode;
	}
	public void setSubjCode(String subjCode) {
		this.subjCode = subjCode;
	}
	public String getSubjName() {
		return subjName;
	}
	public void setSubjName(String subjName) {
		this.subjName = subjName;
	}
	public String getExpression() {
		return expression;
	}
	public void setExpression(String expression) {
		this.expression = expression;
	}
}
	