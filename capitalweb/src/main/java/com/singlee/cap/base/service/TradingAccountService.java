package com.singlee.cap.base.service;

import java.util.List;

import com.singlee.cap.base.vo.TradingAccountVo;

public interface TradingAccountService {
	/**
	 * 查询所有
	 * @return
	 */
	public List<TradingAccountVo> selectAllTradingAccountService();
}
