package com.singlee.cap.base.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "TBK_ENTRY_SCENE_CONFIG")
public class TbkEntrySceneConfig implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6067872563051816943L;
	/**
	 * 场景节点ID
	 */
	private String sceneId;
	/**
	 * 分录配置组ID
	 */
	private String settingGroupId;
	/**
	 * 最后更新时间
	 */
	private String lastUpdateDate;

	public String getSceneId() {
		return sceneId;
	}

	public void setSceneId(String sceneId) {
		this.sceneId = sceneId;
	}

	public String getSettingGroupId() {
		return settingGroupId;
	}

	public void setSettingGroupId(String settingGroupId) {
		this.settingGroupId = settingGroupId;
	}

	public String getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(String lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

}
