package com.singlee.cap.base.mapper;

import java.util.List;
import java.util.Map;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.common.Mapper;
import com.github.pagehelper.Page;
import com.singlee.cap.base.model.TbkEntrySettingConfig;

public interface TbkEntrySettingConfigMapper extends Mapper<TbkEntrySettingConfig>{
 
	/**
	 * 查询
	 * 含分页
	 * @param map
	 * @param rb
	 * @return
	 */
	public Page<TbkEntrySettingConfig> getTbkEntrySettingConfigList(Map<String,Object> map, RowBounds rb);
	/**
	 * 查询
	 * 不含分页
	 * @param map
	 * @return
	 */
	public List<TbkEntrySettingConfig> getTbkEntrySettingConfigList(Map<String,Object> map);
	
	/**
	 * 查询科目列表及动态科目信息
	 */
	public Page<TbkEntrySettingConfig> searchSubjAndConfig(Map<String,Object> map, RowBounds rb);
}
