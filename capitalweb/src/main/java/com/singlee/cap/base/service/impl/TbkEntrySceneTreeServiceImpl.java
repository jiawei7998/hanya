
package com.singlee.cap.base.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.cap.base.mapper.TbkEntrySceneTreeMapper;
import com.singlee.cap.base.model.TbkEntrySceneTree;
import com.singlee.cap.base.service.TbkEntrySceneTreeService;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TbkEntrySceneTreeServiceImpl implements TbkEntrySceneTreeService {
	@Autowired
	private TbkEntrySceneTreeMapper tbkEntrySceneTreeMapper;

	@Override
	public Page<TbkEntrySceneTree> getTbkEntrySceneTreeList(Map<String, Object> map) {
		return tbkEntrySceneTreeMapper.getTbkEntrySceneTreeList(map);
	}

	@Override
	public TbkEntrySceneTree save(TbkEntrySceneTree ttEntrySceneTree) {
		tbkEntrySceneTreeMapper.insert(ttEntrySceneTree);
		return ttEntrySceneTree;
	}

	@Override
	public TbkEntrySceneTree update(TbkEntrySceneTree ttEntrySceneTree) {
		tbkEntrySceneTreeMapper.updateByPrimaryKey(ttEntrySceneTree);
		return ttEntrySceneTree;
	}

	@Override
	public void delTbkEntrySceneTree() {
		tbkEntrySceneTreeMapper.delTbkEntrySceneTree();
	}

	@Override
	public void updateTbkEntrySceneTreeNO() {
		tbkEntrySceneTreeMapper.updateTbkEntrySceneTreeNO();

	}

	@Override
	public void updateTbkEntrySceneTree() {
		tbkEntrySceneTreeMapper.updateTbkEntrySceneTree();
	}
}
