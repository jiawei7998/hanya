package com.singlee.cap.base.vo;

import java.io.Serializable;

public class TbkParamMap implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String pId;
	private String pValue;
	public String getpId() {
		return pId;
	}
	public void setpId(String pId) {
		this.pId = pId;
	}
	public String getpValue() {
		return pValue;
	}
	public void setpValue(String pValue) {
		this.pValue = pValue;
	}
	
}
