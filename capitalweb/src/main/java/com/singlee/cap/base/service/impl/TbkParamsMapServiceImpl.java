package com.singlee.cap.base.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.cap.base.mapper.TbkParamsMapMapper;
import com.singlee.cap.base.mapper.TbkParamsMapper;
import com.singlee.cap.base.model.TbkParams;
import com.singlee.cap.base.model.TbkParamsMap;
import com.singlee.cap.base.model.TbkParamsMapModule;
import com.singlee.cap.base.service.TbkParamsMapService;
import com.singlee.capital.common.log.AutoLogMethod;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;


@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TbkParamsMapServiceImpl implements TbkParamsMapService {
	@Autowired
	private TbkParamsMapMapper paramsMapMapper;
	
	@Autowired
	private TbkParamsMapper paramsMapper;
	@Override
	public Page<TbkParamsMap> searchParamsMapPage(Map<String, Object> params) {
		return paramsMapMapper.searchParamsMapPage(params, ParameterUtil.getRowBounds(params));
	}

	@Override
	@AutoLogMethod(value="新增")
	public void createParamsMap(TbkParamsMap paramsMap) {
		paramsMapMapper.insertParamsMap(paramsMap);
	}

	@Override
	@AutoLogMethod(value="修改")
	public void updateParamsMap(TbkParamsMap paramsMap) {
		TbkParamsMap paramsMapOld=paramsMapMapper.selectByPrimaryKey(paramsMap);
		if(paramsMapOld==null){
			JY.raiseRException("该维度值信息不存在");
		}else{
			paramsMapMapper.updateParamsMap(paramsMap);
		}
	}

	@Override
	@AutoLogMethod(value="删除")
	public void deleteParamsMap(String pKey) {
		if (paramsMapMapper.checkParamsMapByPkey(pKey) == 0) {
			JY.raiseRException("该维度值已被删除，请刷新页面");
		} else {
			paramsMapMapper.deleteByPrimaryKey(pKey);
		}
	}

	@Override
	public List<TbkParamsMap> getPvalueTxtList(Map<String, Object> params) {
		List<TbkParamsMap> oriParamList = paramsMapMapper.getPvalueTxtList(params);
		return oriParamList;
	}

	 
	@Override
	public Integer checkParamsMapByPkey(String pKey) {
		return paramsMapMapper.checkParamsMapByPkey(pKey);
	}

	@Override
	public Integer checkParamsMapByParamId(String paramId) {
		return paramsMapMapper.checkParamsMapByParamId(paramId);
	}
	
	@Override
	public TbkParamsMap getParamsMap(TbkParamsMap map) {
		return paramsMapMapper.getParamsMap(map).get(0);
	}

	@Override
	public List<TbkParamsMap> searchBySourceList(Map<String, Object> params) {
		return paramsMapMapper.searchBySourceList(params);
	}

	@Override
	public List<TbkParamsMapModule> searchParamsMapPages(
			Map<String, Object> params) {
		List<TbkParamsMapModule> arryList = new ArrayList<TbkParamsMapModule>();
		List<TbkParams> TbkParamsList = paramsMapper.getParamsList();
		if(TbkParamsList != null){
			for (int i = 0; i < TbkParamsList.size(); i++) {
				TbkParamsMapModule tbkParamsMapModule = new TbkParamsMapModule();
				String paramId = TbkParamsList.get(i).getParamId();
				String paramName = TbkParamsList.get(i).getParamName();
				tbkParamsMapModule.setParamId(paramId);
				tbkParamsMapModule.setParamName(paramName);
				List<TbkParamsMap> tbkParamsMapList = paramsMapMapper.searchParamsMapByParamId(paramId);
				if(tbkParamsMapList != null){
					tbkParamsMapModule.setAttributes(tbkParamsMapList);
				}
				arryList.add(tbkParamsMapModule);
			}
		}
		return arryList;
	}

}