package com.singlee.cap.base.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.cap.base.model.TbkEntrySceneConfig;

/**
 * 场景分录mapper
 * 
 * 未继承mapper<TbkEntrySceneConfig>,因该张表中没有主键   201805
 * 
 * @author lizm
 *
 */

public interface TbkEntrySceneConfigMapper extends Mapper<TbkEntrySceneConfig>{
	/**
	 * 无分页查询场景分录配置
	 * 
	 * @param params
	 * @return
	 */
	public Page<TbkEntrySceneConfig> searchEntrySceneConfig(Map<String, Object> params);

	/**
	 * 分页查询场景分录配置
	 * 
	 * @param params
	 * @param rb
	 * @return
	 */
	public Page<TbkEntrySceneConfig> searchEntrySceneConfig(Map<String, Object> params, RowBounds rb);

	/**
	 * 添加场景分录配置
	 * 
	 * @param tbkEntrySceneConfig
	 */
	public void createEntrySceneConfig(TbkEntrySceneConfig tbkEntrySceneConfig);

	/**
	 * 根据场景节点id删除场景分录配置信息
	 * 
	 * @param sceneId
	 */
	public void deleteEntrySceneConfigBySceneId(String sceneId);
}
