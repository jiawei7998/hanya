package com.singlee.cap.base.model;

import java.util.List;

import com.singlee.capital.system.model.TaFunction;

public class TbkParamsMapModule {
	private String paramId;
	private String paramName;
	private List<TbkParamsMap> attributes;
	public String getParamId() {
		return paramId;
	}
	public void setParamId(String paramId) {
		this.paramId = paramId;
	}
	public String getParamName() {
		return paramName;
	}
	public void setParamName(String paramName) {
		this.paramName = paramName;
	}
	public List<TbkParamsMap> getAttributes() {
		return attributes;
	}
	public void setAttributes(List<TbkParamsMap> attributes) {
		this.attributes = attributes;
	}
	
}
