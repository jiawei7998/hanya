package com.singlee.cap.base.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.cap.base.mapper.TbkParamsMapper;
import com.singlee.cap.base.model.TbkParams;
import com.singlee.cap.base.model.TbkParamsMap;
import com.singlee.cap.base.service.TbkParamsMapService;
import com.singlee.cap.base.service.TbkParamsService;
import com.singlee.capital.common.i18n.MsgUtils;
import com.singlee.capital.common.log.AutoLogMethod;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.dict.DictConstants;


@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TbkParamsServiceImpl implements TbkParamsService {
	@Autowired
	private TbkParamsMapper paramsMapper;
	@Autowired
	private TbkParamsMapService paramsMapService;

	@Override
	@AutoLogMethod(value="添加")
	public void createParams(TbkParams params) {
		if (paramsMapper.checkParamsByParamId(params.getParamId()) == 0) {
			params.setLastUpdate(DateUtil.getCurrentDateTimeAsString());
			paramsMapper.insert(params);
		} else {
			JY.raiseRException(MsgUtils.getMessage("该维度ID已经存在"));
		}
	}

	@Override
	@AutoLogMethod(value="修改")
	public void updateParams(TbkParams params) {
		TbkParams paramsOld=paramsMapper.getParamsByParamId(params.getParamId());
		if(paramsOld==null){
			JY.raiseRException(MsgUtils.getMessage("该维度ID不存在"));
		}else{
			params.setLastUpdate(DateUtil.getCurrentDateTimeAsString());
			paramsMapper.updateByPrimaryKey(params);
		}
	}

	@Override
	@AutoLogMethod(value="删除")
	public void deleteParams(String paramId) {
		if (paramsMapper.checkParamsByParamId(paramId) == 0) {
			JY.raiseRException(MsgUtils.getMessage("该维度已被删除，请刷新页面"));
		}else{
			int paramsMapHadNum= paramsMapService.checkParamsMapByParamId(paramId);
			if(paramsMapHadNum == 0){
				paramsMapper.deleteByPrimaryKey(paramId);
			}else{
				JY.raiseRException(MsgUtils.getMessage("该维度已在维度值信息中存在！请先删除相应的维度值配置"));
			}
		}
	}
	
	@Override
	public Page<TbkParams> getParamName(Map<String, Object> params) {
		return paramsMapper.getParamName(params);
	}

	@Override
	public Page<TbkParams> searchParamsPage(Map<String, Object> params) {
		return paramsMapper.searchParamsPage(params,ParameterUtil.getRowBounds(params));
	}

	@Override
	public List<TbkParams> getParamsActiveList(String active) {
		return paramsMapper.getParamsActiveList(active);
	}

	@Override
	public void changeParamsStatus(String paramId) {
		TbkParams  params = paramsMapper.getParamsByParamId(paramId);
		if(DictConstants.YesNo.YES.equals(params.getActive())){
			params.setActive(DictConstants.YesNo.NO);
		}else{
			params.setActive(DictConstants.YesNo.YES);
		}
		paramsMapper.changeParamsStatus(params);
	}


}
