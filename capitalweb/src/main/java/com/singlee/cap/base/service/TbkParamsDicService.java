package com.singlee.cap.base.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.cap.base.model.TbkParamsDic;

public interface TbkParamsDicService {
	/**
	 * 维度字典信息
	 * @param params
	 * @return
	 */
	public Page<TbkParamsDic> searchParamsDicPage(Map<String, Object> params);

	/**
	 * 添加维度字典信息
	 * @param paramsDic
	 */
	public void createParamsDic(TbkParamsDic paramsDic);

	/**
	 * 修改维度字典信息
	 * @param paramsDic
	 */
	public void updateParamsDic(TbkParamsDic paramsDic);

	/**
	 * 删除维度字典信息
	 * @param paramId
	 */
	public void deleteParamsDic(String paramId);
}
