package com.singlee.cap.base.service;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.cap.base.model.TbkSubjectDef;
/*
 *科目表service层
 */
public interface TbkSubjectDefService {
	/*
	 * 遍历科目
	 */
	public List<TbkSubjectDef> getTbkSubjectDefList(Map<String,Object> map);
	/*
	 * 添加科目
	 */
	public TbkSubjectDef creatTbkSubjectDef(TbkSubjectDef ttbksubjectdef);
	
	/*
	 * 修改科目
	 */
	public void updateTbkSubjectDef(TbkSubjectDef ttbksubjectdef);
	/*
	 * 删除科目
	 */
	public void delTbkSubjectDef(String[] dbIds);
	/*
	 * 遍历科目
	 */
	public Page<TbkSubjectDef> getTbkSubjectDefPageList(Map<String, Object> map);
	
	List<TbkSubjectDef> selectAll();
	
	/*
	 * 查询条件科目及上级科目
	 */
	public List<TbkSubjectDef> getTbkSubjectAndParentList(Map<String,Object> map);
	
	/**
	 * 根据科目号 精确查询
	 * @param map
	 * @return
	 */
	public Page<TbkSubjectDef> getTbkSubjectDefSameService(Map<String,Object> map);
	
	/**
	 * 树形展示的科目定义 201805
	 * @param map
	 * @return
	 */
	public List<TbkSubjectDef> getTbkSubjectDefListForTree(Map<String, Object> map);
	public Page<TbkSubjectDef> searchPageSubjectBySubjectName(
			Map<String, Object> allMap);
	public void delTbkSubjectDefs(String[] dbIds);
}
