package com.singlee.cap.accounting.model;

import java.io.Serializable;

/**
 * 产品指令包
 * @author lgp
 * create time:2017-5-6 下午5:36:05
 */
public abstract class ProductInstruction  implements Serializable{
	
	private static final long serialVersionUID = -3803665069872183940L;
	/** 接口调用流水号 */
	private String intSeqNo;
	/**操作类型  新增、作废【字典项，待分配】*/
	private String opType;
	/** 产品代码 */
	private String prodId;
	/** 子产品代码 */
	private String subProdId;
	/** 子产品期数/期次 */
	private double subProdSeq;
	/** 产品收益率 */
	private double yield;
	/** 产品净值 */
	private double netValue;
	/** 应付收益 */
	private double accruedIncome;
	
	
	/** 报价日 */
	private String quoteDate;
	public String getIntSeqNo() {
		return intSeqNo;
	}
	public void setIntSeqNo(String intSeqNo) {
		this.intSeqNo = intSeqNo;
	}
	public String getOpType() {
		return opType;
	}
	public void setOpType(String opType) {
		this.opType = opType;
	}
	public String getProdId() {
		return prodId;
	}
	public void setProdId(String prodId) {
		this.prodId = prodId;
	}
	public String getSubProdId() {
		return subProdId;
	}
	public void setSubProdId(String subProdId) {
		this.subProdId = subProdId;
	}
	public double getSubProdSeq() {
		return subProdSeq;
	}
	public void setSubProdSeq(double subProdSeq) {
		this.subProdSeq = subProdSeq;
	}
	public double getYield() {
		return yield;
	}
	public void setYield(double yield) {
		this.yield = yield;
	}
	public double getNetValue() {
		return netValue;
	}
	public void setNetValue(double netValue) {
		this.netValue = netValue;
	}
	public double getAccruedIncome() {
		return accruedIncome;
	}
	public void setAccruedIncome(double accruedIncome) {
		this.accruedIncome = accruedIncome;
	}
	public String getQuoteDate() {
		return quoteDate;
	}
	public void setQuoteDate(String quoteDate) {
		this.quoteDate = quoteDate;
	}
}
