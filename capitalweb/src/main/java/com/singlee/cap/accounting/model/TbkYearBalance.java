package com.singlee.cap.accounting.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/*
 * 年结配置表
 */
@Entity
@Table(name = "TBK_YEAR_BALANCE")
public class TbkYearBalance implements Serializable {
	private static final long serialVersionUID = -2840141545039907322L;

    /**  分录id          */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT SEQ_TBK_YEAR_BAL.NEXTVAL FROM DUAL")
	private String dbId;
	
	private String yearSer;

	/**
	 * 机构
	 * */
	private String sponInst;
	/**
	 * 需结转科目
	 * */
	private String fromSubjCode;
	/**
	 * 需结转科目名称
	 * */
	private String fromSubjName;
	/**
	 * 需结转科目币种
	 * */
	private String fromCcy;
	/**
	 * 结转后科目
	 * */
	private String endSubjCode;		
	/**
	 * 结转后科目名称
	 * */
	private String endSubjName;	
	/**
	 * 结转后科目币种
	 * */
	private String endCcy;	
	/**
	 * 需结转科目方向
	 * */
	private String fromDebitCredit;			
	/**
	 * 结转后科目方向
	 * */
	private String endDebitCredit;			
	/**
	 * 更新时间
	 * */
	private String lasDate;
	/**
	 * 是否红字
	 */
	private String redFlag;
	
	@Transient
	private String instName;
	
	@Transient
	private double amount;

	@Transient
	private String beaNo;
	


	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getFromSubjCode() {
		return fromSubjCode;
	}
	public void setFromSubjCode(String fromSubjCode) {
		this.fromSubjCode = fromSubjCode;
	}
	public String getEndSubjCode() {
		return endSubjCode;
	}
	public void setEndSubjCode(String endSubjCode) {
		this.endSubjCode = endSubjCode;
	}
	public String getFromDebitCredit() {
		return fromDebitCredit;
	}
	public void setFromDebitCredit(String fromDebitCredit) {
		this.fromDebitCredit = fromDebitCredit;
	}
	public String getEndDebitCredit() {
		return endDebitCredit;
	}
	public void setEndDebitCredit(String endDebitCredit) {
		this.endDebitCredit = endDebitCredit;
	}
	public String getLasDate() {
		return lasDate;
	}
	public void setLasDate(String lasDate) {
		this.lasDate = lasDate;
	}
	public String getFromSubjName() {
		return fromSubjName;
	}
	public void setFromSubjName(String fromSubjName) {
		this.fromSubjName = fromSubjName;
	}
	public String getEndSubjName() {
		return endSubjName;
	}
	public void setEndSubjName(String endSubjName) {
		this.endSubjName = endSubjName;
	}
	public String getSponInst() {
		return sponInst;
	}
	public void setSponInst(String sponInst) {
		this.sponInst = sponInst;
	}
	public String getFromCcy() {
		return fromCcy;
	}
	public void setFromCcy(String fromCcy) {
		this.fromCcy = fromCcy;
	}
	public String getEndCcy() {
		return endCcy;
	}
	public void setEndCcy(String endCcy) {
		this.endCcy = endCcy;
	}
	public String getYearSer() {
		return yearSer;
	}
	public void setYearSer(String yearSer) {
		this.yearSer = yearSer;
	}
	
	public String getRedFlag() {
		return redFlag;
	}
	public void setRedFlag(String redFlag) {
		this.redFlag = redFlag;
	}
	public String getDbId() {
		return dbId;
	}
	public void setDbId(String dbId) {
		this.dbId = dbId;
	}
	public String getInstName() {
		return instName;
	}
	public void setInstName(String instName) {
		this.instName = instName;
	}
	public String getBeaNo() {
		return beaNo;
	}
	public void setBeaNo(String beaNo) {
		this.beaNo = beaNo;
	}	

}
