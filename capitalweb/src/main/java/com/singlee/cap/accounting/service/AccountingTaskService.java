package com.singlee.cap.accounting.service;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.cap.accounting.model.TacTask;

public interface AccountingTaskService {
	
	 /**
	  * 查询帐套分页
	 * @param map
	 * @return
	 */
	public Page<TacTask> selectTacTask(Map<String,Object> map);
	 /**
	  * 添加帐套信息
	 * @param ttaccountingtask
	 */
	public void add( Map<String, Object> map);
	 /**
	  * 修改帐套信息
	 * @param task
	 */
	public void updateTask(Map<String, Object> map);
	 /**
	  * 删除帐套信息
	 * @param tta
	 */
	public void deleteTacTask(String [] tta);
	 /**
	 * 获取一个Task
	 * @param taskId
	 * @param return
	 */
	 public TacTask getTask(String taskId);
	/**
	 * 修改当前日期
	 * @param taskId
	 * @param date
	 */
	public void updateCurrentDate(String taskId, String date);
	
	/**
	 * 获取一个Task
	 * @param taskId
	 * @param return
	 */
	public List<TacTask> selectTaskList(Map<String, Object> map);
	/**
	  * 查询所有账套
	 * @param map
	 * @return
	 */
	List<TacTask> selectAllTask();
	
	/**
	 * 获取一个Task
	 * @param taskId
	 * @param return
	 */
	public List<TacTask> selectEntryTaskList(Map<String, Object> map);
	
}
