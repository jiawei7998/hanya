package com.singlee.cap.accounting.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.singlee.cap.accounting.mapper.TbkYearBalanceMapper;
import com.singlee.cap.accounting.model.TbkYearBalance;
import com.singlee.cap.accounting.service.TbkYearBalanceService;
import com.singlee.cap.bookkeeping.service.TbkEntryService;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.spring.mybatis.BatchDao;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;

@Service("tbkYearBalanceService")
public class TbkYearBalanceServiceImpl implements TbkYearBalanceService {

	@Autowired
	private TbkYearBalanceMapper tbkYearBalanceMapper;
	@Autowired
	BatchDao batchDao;
	@Autowired
	private TbkEntryService bookkeepingEntryService;
	
	@Override
	public void add(Map<String, Object> map) {
		TbkYearBalance yearBal = new TbkYearBalance();
		yearBal.setSponInst(ParameterUtil.getString(map, "sponInst", ""));
		yearBal.setFromCcy(ParameterUtil.getString(map, "fromCcy", ""));
		yearBal.setFromDebitCredit(ParameterUtil.getString(map, "fromDebitCredit", ""));
        yearBal.setRedFlag(ParameterUtil.getString(map, "redFlag", ""));
		//		BeanUtil.populate(yearBal, map);
		String fromStr = ParameterUtil.getString(map, "fromStr", "");
		String endParam = ParameterUtil.getString(map, "constact_list", "");
		Map<String, Object> endMap = JSONObject.parseObject(endParam);
		BeanUtil.populate(yearBal, endMap);
		String yearSer = bookkeepingEntryService.getNewFlowNo();;
		yearBal.setYearSer(yearSer);
		List<TbkYearBalance> balList = new ArrayList<TbkYearBalance>();
		if(fromStr != null && !"".equals(fromStr)){
			TbkYearBalance bal = null;
			String[] from = fromStr.split(",");
			for (String string : from) {
				bal = new TbkYearBalance(); 
				BeanUtil.copyNotEmptyProperties(bal, yearBal);
				bal.setFromSubjCode(string.split("-")[0]);
				bal.setFromSubjName(string.split("-")[1]);
				bal.setLasDate(DateUtil.getCurrentDateTimeAsString());
				List<TbkYearBalance> list = selectFromYearBalanceList(BeanUtil.beanToMap(bal));
				if(list.size() > 0){
					throw new RException("机构:"+bal.getSponInst()+"下的科目:"+string+"已进行了结转，请不要重复配置！");
				}else{
					balList.add(bal);
				}
			}
		}
		batchDao.batch("com.singlee.cap.accounting.mapper.TbkYearBalanceMapper.insert", balList);
	}
	
	@Override
	public void deleteTacTask(String[] tta) {
		for(int i=0;i<tta.length;i++){
			tbkYearBalanceMapper.deleteByPrimaryKey(tta[i]);
		}
	}


	@Override
	public Page<TbkYearBalance> selectTbkYearBalancePage(
			Map<String, Object> map) {
		return tbkYearBalanceMapper.selectTbkYearBalanceList(map, ParameterUtil.getRowBounds(map));
	}


	@Override
	public List<TbkYearBalance> selectTbkYearBalanceList(Map<String, Object> map) {
		return tbkYearBalanceMapper.selectTbkYearBalanceList(map);
	}

	@Override
	public List<TbkYearBalance> selectFromYearBalanceList(Map<String, Object> map) {
		return tbkYearBalanceMapper.selectFromYearBalanceList(map);
	}

	@Override
	public List<TbkYearBalance> selectEndTbkYearBalanceService(
			Map<String, Object> map) {
		List<TbkYearBalance> list = tbkYearBalanceMapper.selectEndTbkYearBalance(map);
		return list;
	}
	
	
}
