package com.singlee.cap.accounting.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.singlee.cap.accounting.mapper.TacTaskMapper;
import com.singlee.cap.accounting.model.TacTask;
import com.singlee.cap.accounting.service.AccountingTaskService;
import com.singlee.cap.bookkeeping.mapper.TbkEntryMapper;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.i18n.MsgUtils;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;

@Service("accountingTaskService")
public class AccountingTaskServiceImpl implements AccountingTaskService {

	@Autowired
	private TacTaskMapper accountingTaskMapper;
	@Autowired
	private TbkEntryMapper bookkeepingEntryMapper;
	
	/**
	 * 修改当前日期
	 * @param taskId
	 * @param currentDate
	 */
	@Override
	public void updateCurrentDate(String taskId, String currentDate) {
		accountingTaskMapper.updateCurrentDate(taskId, currentDate);
	}


	@Override
	public Page<TacTask> selectTacTask(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		return accountingTaskMapper.selectTaccountingtask(map, rb);
		
	}

	@Override
	public void add( Map<String, Object> map) {
		TacTask ttaccountingtask = new TacTask();
		BeanUtil.populate(ttaccountingtask, map);
		ttaccountingtask.setCreateDate(DateUtil.getCurrentDateAsString());
		ttaccountingtask.setCreateTime(DateUtil.getCurrentTimeAsString());
		ttaccountingtask.setUpdateTime(DateUtil.getCurrentDateTimeAsString());
		ttaccountingtask.setInstId(","+ParameterUtil.getString(map, "instId", "")+",");
		ttaccountingtask.setBkMain(","+ParameterUtil.getString(map, "bkMain", "")+",");
		ttaccountingtask.setBkMainNm(ParameterUtil.getString(map, "bkMain_text", ""));
		ttaccountingtask.setInstIdNm(ParameterUtil.getString(map, "instId_text", ""));
		accountingTaskMapper.insert(ttaccountingtask);
		
	}


	@Override
	public void deleteTacTask(String[] tta) {
		for(int i=0;i<tta.length;i++){
			if(bookkeepingEntryMapper.checkBookkeepingEntryByTaskId(tta[i])==0){
				//检查记账凭证是否存在，如果存在将不允许删除，如果不存在将执行以下代码
				/**
				 * 删除账套管理中的该信息
				 */
				accountingTaskMapper.deleteByPrimaryKey(tta[i]);
				//以下逻辑存在争议暂时注释掉，部分未写
				/**
				 * 删除科目中的相关信息，包含分录配置、场景配置
				 */
				//ttBkSubjectDefMapper.deleteTbkSubjectDefByTaskId(tta[i]);
				/**
				 * 删除分录配置中的相关信息
				 */
				
				/**
				 * 删除场景配置中的相关信息
				 */
				
			}else{
				JY.raiseRException(MsgUtils.getMessage("该账套信息在记账凭证中已经存在，不允许删除！"));
			}
			
		}
	}
	
	/**
	 * 获取一个Task
	 * @param taskId
	 * @param return
	 */
	@Override
	public TacTask getTask(String taskId) {
		TacTask task = accountingTaskMapper.selectByPrimaryKey(taskId);
		if(task == null){
			throw new RException("核算任务配置错误,ID="+taskId);
		}
		return task;
	}


	@Override
	public void updateTask(Map<String, Object> map) {
		TacTask task = new TacTask();
		BeanUtil.populate(task, map);
		task.setUpdateTime(DateUtil.getCurrentDateTimeAsString());
		String instId = ParameterUtil.getString(map, "instId", "");
		if(!",".equals(instId.substring(0,1))){
			instId =","+instId;
		}
		if(!",".equals(instId.substring(instId.length()-1,instId.length()))){
			instId =instId+",";
		}
		String bkMain = ParameterUtil.getString(map, "bkMain", "");
		if(!",".equals(bkMain.substring(0,1))){
			bkMain =","+bkMain;
		}
		if(!",".equals(bkMain.substring(bkMain.length()-1,bkMain.length()))){
			bkMain =bkMain+",";
		}
		task.setInstId(instId);
		task.setBkMain(bkMain);
		task.setBkMainNm(ParameterUtil.getString(map, "bkMain_text", ""));
		task.setInstIdNm(ParameterUtil.getString(map, "instId_text", ""));
		accountingTaskMapper.updateByPrimaryKey(task);
		
	}


	@Override
	public List<TacTask> selectTaskList(Map<String, Object> map) {
		return accountingTaskMapper.selectTaskList(map);
	}


	@Override
	public List<TacTask> selectAllTask() {
		return accountingTaskMapper.selectAll();
	}


	@Override
	public List<TacTask> selectEntryTaskList(Map<String, Object> map) {
		return accountingTaskMapper.selectEntryTaskList(map);
	}
	
}
