package com.singlee.cap.accounting.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import tk.mybatis.mapper.annotation.NameStyle;
import tk.mybatis.mapper.code.Style;

@Entity
@Table(name = "TT_ACCOUNTING_INSTR_S")
@NameStyle(Style.uppercase)
public class SecuInstruction extends AbstractInstruction implements Serializable, Cloneable{

	private static final long serialVersionUID = 1L;
	/** 券指令id  */
	private String secuInstructionId;
	/** 券业务类型  */
	private String secuBizType;
	/** 平仓指定交易号  */
	private String closeTradeId;
	/** 面额  */
	private double amount;
	/** 理论净价金额  */
	private double theoryCp;
	/** 实际净价金额  */
	private double realCp;
	/** 理论利息  */
	private double theoryAI;
	/** 实际利息  */
	private double realAI;
	/** 已收利息  */
	private double receivedAI;
	/** 费用  */
	private double fee;
	/** 销售费 */
	private double sellFee;
	/** 托管费 */
	private double hostFee;
	/** 管理费 */
	private double managerFee;
	/** 其他费 */
	private double otherFee;
	/** 保证金  */
	@Transient
	private double margin;
	
	/** 交易币种2金额，次交易币种金额  */
	@Transient
	private double amount2;
	/** 近端交易币种1金额，  近端主交易币种金额  */
	@Transient
	private String nearAmount;
	/** 近端交易币种2金额，近端次交易币种金额  */
	@Transient
	private String nearAmount2;

	/**  icode */
	@Transient
	private String hICode;

	/**  atype */
	@Transient
	private String hAType;

	/**  mtype  */
	@Transient
	private String hMType;

	public String getSecuInstructionId() {
		return secuInstructionId;
	}
	public void setSecuInstructionId(String secuInstructionId) {
		this.secuInstructionId = secuInstructionId;
	}
	public String getSecuBizType() {
		return secuBizType;
	}
	public void setSecuBizType(String secuBizType) {
		this.secuBizType = secuBizType;
	}
	public String getCloseTradeId() {
		return closeTradeId;
	}
	public void setCloseTradeId(String closeTradeId) {
		this.closeTradeId = closeTradeId;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public double getTheoryCp() {
		return theoryCp;
	}
	public void setTheoryCp(double theoryCp) {
		this.theoryCp = theoryCp;
	}
	public double getRealCp() {
		return realCp;
	}
	public void setRealCp(double realCp) {
		this.realCp = realCp;
	}
	public double getTheoryAI() {
		return theoryAI;
	}
	public void setTheoryAI(double theoryAI) {
		this.theoryAI = theoryAI;
	}
	public double getRealAI() {
		return realAI;
	}
	public void setRealAI(double realAI) {
		this.realAI = realAI;
	}
	public double getReceivedAI() {
		return receivedAI;
	}
	public void setReceivedAI(double receivedAI) {
		this.receivedAI = receivedAI;
	}
	public double getFee() {
		return fee;
	}
	public void setFee(double fee) {
		this.fee = fee;
	}
	public double getMargin() {
		return margin;
	}
	public void setMargin(double margin) {
		this.margin = margin;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public double getAmount2() {
		return amount2;
	}
	public void setAmount2(double amount2) {
		this.amount2 = amount2;
	}
	public String getNearAmount() {
		return nearAmount;
	}
	public void setNearAmount(String nearAmount) {
		this.nearAmount = nearAmount;
	}
	public String getNearAmount2() {
		return nearAmount2;
	}
	public void setNearAmount2(String nearAmount2) {
		this.nearAmount2 = nearAmount2;
	}
	
	public String gethICode() {
		return hICode;
	}
	public void sethICode(String hICode) {
		this.hICode = hICode;
	}
	public String gethAType() {
		return hAType;
	}
	public void sethAType(String hAType) {
		this.hAType = hAType;
	}
	public String gethMType() {
		return hMType;
	}
	public void sethMType(String hMType) {
		this.hMType = hMType;
	}
	public double getSellFee() {
		return sellFee;
	}
	public void setSellFee(double sellFee) {
		this.sellFee = sellFee;
	}
	public double getHostFee() {
		return hostFee;
	}
	public void setHostFee(double hostFee) {
		this.hostFee = hostFee;
	}
	public double getManagerFee() {
		return managerFee;
	}
	public void setManagerFee(double managerFee) {
		this.managerFee = managerFee;
	}
	public double getOtherFee() {
		return otherFee;
	}
	public void setOtherFee(double otherFee) {
		this.otherFee = otherFee;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SecuInstruction [secuInstructionId=");
		builder.append(secuInstructionId);
		builder.append(", secuBizType=");
		builder.append(secuBizType);
		builder.append(", closeTradeId=");
		builder.append(closeTradeId);
		builder.append(", amount=");
		builder.append(amount);
		builder.append(", theoryCp=");
		builder.append(theoryCp);
		builder.append(", realCp=");
		builder.append(realCp);
		builder.append(", theoryAI=");
		builder.append(theoryAI);
		builder.append(", realAI=");
		builder.append(realAI);
		builder.append(", receivedAI=");
		builder.append(receivedAI);
		builder.append(", fee=");
		builder.append(fee);
		builder.append(", margin=");
		builder.append(margin);
		builder.append(", amount2=");
		builder.append(amount2);
		builder.append(", nearAmount=");
		builder.append(nearAmount);
		builder.append(", nearAmount2=");
		builder.append(nearAmount2);
		builder.append(", hICode=");
		builder.append(hICode);
		builder.append(", hAType=");
		builder.append(hAType);
		builder.append(", hMType=");
		builder.append(hMType);
		builder.append(", toString()=");
		builder.append(super.toString());
		builder.append("]");
		return builder.toString();
	}
}
