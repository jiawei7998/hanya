package com.singlee.cap.accounting.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import tk.mybatis.mapper.annotation.NameStyle;
import tk.mybatis.mapper.code.Style;

@Entity
@Table(name = "TT_ACCOUNTING_INSTR_P")
@NameStyle(Style.uppercase)
public class PrimaryInstruction  implements Serializable, Cloneable{

	private static final long serialVersionUID = 1L;

	/** 主指令id  */
	@Id
	private String primaryInstructionId;

	/** 主指令类型  */
	private String instructionType;

	/** 业务类型  */
	private String bizType;

	/** 组合交易类型  */
	private String tradeGroupType;

	/** 组合交易id  */
	private String tradeGroupId;

	/** 外部交易编号  */
	private String tradeId;

	/** 结算日期  */
	private String setDate;

	/** 计算日期  */
	private String calcDate;

	/** 状态  */
	private String state;

	/**  icode */
	@Transient
	private String hICode;

	/**  atype */
	@Transient
	private String hAType;

	/**  mtype  */
	@Transient
	private String hMType;

	/** 标签，附加对象 */
	@Transient
	private Object tag;
	
	/** 币种对 */
	@Transient
	private String ccyPair;
	
	/** 交易币种1，主交易币种  */
	private String ccy1;
	
	/** 交易币种2，次交易币种  */
	@Transient
	private String ccy2;
	
	/** 近端交易币种1，  近端主交易币种  */
	@Transient
	private String nearCcy1;
	
	/** 近端交易币种2，近端次交易币种  */
	@Transient
	private String nearCcy2;

	public String getPrimaryInstructionId() {
		return primaryInstructionId;
	}

	public void setPrimaryInstructionId(String primaryInstructionId) {
		this.primaryInstructionId = primaryInstructionId;
	}

	public String getInstructionType() {
		return instructionType;
	}

	public void setInstructionType(String instructionType) {
		this.instructionType = instructionType;
	}

	public String getBizType() {
		return bizType;
	}

	public void setBizType(String bizType) {
		this.bizType = bizType;
	}

	public String getTradeGroupType() {
		return tradeGroupType;
	}

	public void setTradeGroupType(String tradeGroupType) {
		this.tradeGroupType = tradeGroupType;
	}

	public String getTradeGroupId() {
		return tradeGroupId;
	}

	public void setTradeGroupId(String tradeGroupId) {
		this.tradeGroupId = tradeGroupId;
	}

	public String getTradeId() {
		return tradeId;
	}

	public void setTradeId(String tradeId) {
		this.tradeId = tradeId;
	}

	public String getSetDate() {
		return setDate;
	}

	public void setSetDate(String setDate) {
		this.setDate = setDate;
	}

	public String getCalcDate() {
		return calcDate;
	}

	public void setCalcDate(String calcDate) {
		this.calcDate = calcDate;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String gethICode() {
		return hICode;
	}

	public void sethICode(String hICode) {
		this.hICode = hICode;
	}

	public String gethAType() {
		return hAType;
	}

	public void sethAType(String hAType) {
		this.hAType = hAType;
	}

	public String gethMType() {
		return hMType;
	}

	public void sethMType(String hMType) {
		this.hMType = hMType;
	}

	public Object getTag() {
		return tag;
	}

	public void setTag(Object tag) {
		this.tag = tag;
	}

	public String getCcyPair() {
		return ccyPair;
	}

	public void setCcyPair(String ccyPair) {
		this.ccyPair = ccyPair;
	}

	public String getCcy1() {
		return ccy1;
	}

	public void setCcy1(String ccy1) {
		this.ccy1 = ccy1;
	}

	public String getCcy2() {
		return ccy2;
	}

	public void setCcy2(String ccy2) {
		this.ccy2 = ccy2;
	}

	public String getNearCcy1() {
		return nearCcy1;
	}

	public void setNearCcy1(String nearCcy1) {
		this.nearCcy1 = nearCcy1;
	}

	public String getNearCcy2() {
		return nearCcy2;
	}

	public void setNearCcy2(String nearCcy2) {
		this.nearCcy2 = nearCcy2;
	}

	@Override
	public PrimaryInstruction clone() {
		try {
			return (PrimaryInstruction)super.clone();
		} catch (CloneNotSupportedException e) {
			return null;
		}
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PrimaryInstruction [primaryInstructionId=");
		builder.append(primaryInstructionId);
		builder.append(", instructionType=");
		builder.append(instructionType);
		builder.append(", bizType=");
		builder.append(bizType);
		builder.append(", tradeGroupType=");
		builder.append(tradeGroupType);
		builder.append(", tradeGroupId=");
		builder.append(tradeGroupId);
		builder.append(", tradeId=");
		builder.append(tradeId);
		builder.append(", setDate=");
		builder.append(setDate);
		builder.append(", calcDate=");
		builder.append(calcDate);
		builder.append(", state=");
		builder.append(state);
		builder.append(", hICode=");
		builder.append(hICode);
		builder.append(", hAType=");
		builder.append(hAType);
		builder.append(", hMType=");
		builder.append(hMType);
		builder.append(", tag=");
		builder.append(tag);
		builder.append(", ccyPair=");
		builder.append(ccyPair);
		builder.append(", ccy1=");
		builder.append(ccy1);
		builder.append(", ccy2=");
		builder.append(ccy2);
		builder.append(", nearCcy1=");
		builder.append(nearCcy1);
		builder.append(", nearCcy2=");
		builder.append(nearCcy2);
		builder.append("]");
		return builder.toString();
	}
	
	
	
	
	
}
