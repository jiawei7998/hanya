package com.singlee.cap.accounting.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InstructionPackage implements Serializable{

	private static final long serialVersionUID = 1L;

	/** 主指令 */
	private PrimaryInstruction primaryInstruction;
	
	/** 券指令 */
	private List<SecuInstruction> secuInstructions;
	
	/** 资金指令 */
	private List<CashInstruction> cashInstructions;
	
	private Map<String, Object> infoMap;
	
	private Map<String, Object> amtMap;

	public InstructionPackage(){
		this.secuInstructions = new ArrayList<SecuInstruction>(); 
		this.cashInstructions = new ArrayList<CashInstruction>();
		this.infoMap = new HashMap<String, Object>();
		this.amtMap = new HashMap<String, Object>();
	}
	
	public PrimaryInstruction getPrimaryInstruction() {
		return primaryInstruction;
	}

	public void setPrimaryInstruction(PrimaryInstruction primaryInstruction) {
		this.primaryInstruction = primaryInstruction;
	}

	public List<SecuInstruction> getSecuInstructions() {
		return secuInstructions;
	}

	public void setSecuInstructions(List<SecuInstruction> secuInstructions) {
		this.secuInstructions = secuInstructions;
	}

	public List<CashInstruction> getCashInstructions() {
		return cashInstructions;
	}

	public void setCashInstructions(List<CashInstruction> cashInstructions) {
		this.cashInstructions = cashInstructions;
	}

	public Map<String, Object> getInfoMap() {
		return infoMap;
	}

	public void setInfoMap(Map<String, Object> infoMap) {
		this.infoMap = infoMap;
	}

	public Map<String, Object> getAmtMap() {
		return amtMap;
	}

	public void setAmtMap(Map<String, Object> amtMap) {
		this.amtMap = amtMap;
	}

	@Override
	public String toString() {
		return "InstructionPackage [primaryInstruction=" + primaryInstruction + ", secuInstructions=" + secuInstructions + ", cashInstructions=" + cashInstructions + "]";
	}
	
	
}
