package com.singlee.cap.accounting.model;

import java.io.Serializable;

import javax.persistence.Id;
import javax.persistence.Transient;


public abstract class AbstractInstruction extends ProductInstruction implements Serializable, Cloneable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/** 主指令id  */
	@Id
	private String primaryInstructionId;
	/** 内部账户id  */
	private String accIdIn;
	/** 外部账户id */
	private String accIdOut;
	/** 对手方账户id */
	private String partyAccId;
	/** 资产池ID */
	private String productPoolId;
	/** 资产池内产品ID */
	private String productId;
	/** icode 资产编号  */
	private String iCode;
	/** atype  */
	private String aType;
	/** mtype  */
	private String mType;
	/** 结算日期  */
	private String setDate;
	/** 计算日期  */
	private String calcDate;
	/** 状态  */
	private String state;
	/** 投资类型 T R A H O  */
	private String invType;
	/** 标签，附加对象 */
	@Transient
	private Object tag;
	public String getPrimaryInstructionId() {
		return primaryInstructionId;
	}
	public void setPrimaryInstructionId(String primaryInstructionId) {
		this.primaryInstructionId = primaryInstructionId;
	}
	public String getAccIdIn() {
		return accIdIn;
	}
	public void setAccIdIn(String accIdIn) {
		this.accIdIn = accIdIn;
	}
	public String getAccIdOut() {
		return accIdOut;
	}
	public void setAccIdOut(String accIdOut) {
		this.accIdOut = accIdOut;
	}
	public String getPartyAccId() {
		return partyAccId;
	}
	public void setPartyAccId(String partyAccId) {
		this.partyAccId = partyAccId;
	}
	public String getSetDate() {
		return setDate;
	}
	public void setSetDate(String setDate) {
		this.setDate = setDate;
	}
	public String getCalcDate() {
		return calcDate;
	}
	public void setCalcDate(String calcDate) {
		this.calcDate = calcDate;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getiCode() {
		return iCode;
	}
	public void setiCode(String iCode) {
		this.iCode = iCode;
	}
	public String getaType() {
		return aType;
	}
	public void setaType(String aType) {
		this.aType = aType;
	}
	public String getmType() {
		return mType;
	}
	public void setmType(String mType) {
		this.mType = mType;
	}
	public Object getTag() {
		return tag;
	}
	public void setTag(Object tag) {
		this.tag = tag;
	}
	
	@Override
	public AbstractInstruction clone() {
		try {
			return (AbstractInstruction)super.clone();
		} catch (CloneNotSupportedException e) {
			return null;
		}
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AbstractInstruction [primaryInstructionId=");
		builder.append(primaryInstructionId);
		builder.append(", accIdIn=");
		builder.append(accIdIn);
		builder.append(", accIdOut=");
		builder.append(accIdOut);
		builder.append(", partyAccId=");
		builder.append(partyAccId);
		builder.append(", productPoolId=");
		builder.append(productPoolId);
		builder.append(", productId=");
		builder.append(productId);
		builder.append(", iCode=");
		builder.append(iCode);
		builder.append(", aType=");
		builder.append(aType);
		builder.append(", mType=");
		builder.append(mType);
		builder.append(", setDate=");
		builder.append(setDate);
		builder.append(", calcDate=");
		builder.append(calcDate);
		builder.append(", state=");
		builder.append(state);
		builder.append(", invType=");
		builder.append(invType);
		builder.append(", tag=");
		builder.append(tag);
		builder.append("]");
		return builder.toString();
	}
	public String getProductPoolId() {
		return productPoolId;
	}
	public void setProductPoolId(String assetPoolId) {
		this.productPoolId = assetPoolId;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getInvType() {
		return invType;
	}
	public void setInvType(String invType) {
		this.invType = invType;
	}

}
