package com.singlee.cap.accounting.service;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.cap.accounting.model.TbkYearBalance;

public interface TbkYearBalanceService {
	
	 /**
	  * 查询结转信息分页
	 * @param map
	 * @return
	 */
	public Page<TbkYearBalance> selectTbkYearBalancePage(Map<String,Object> map);
	
	/**
	 * 获取一个Task
	 * @param taskId
	 * @param return
	 */
	public List<TbkYearBalance> selectTbkYearBalanceList(Map<String, Object> map);
	
	 /**
	  * 添加结转信息信息
	 * @param ttaccountingtask
	 */
	public void add(Map<String, Object> map);

	 /**
	  * 删除结转信息信息
	 * @param tta
	 */
	public void deleteTacTask(String [] tta);
	/**根据条件查询科目是否进行结转
	 * @param map
	 * @return
	 */
	public List<TbkYearBalance> selectFromYearBalanceList(Map<String, Object> map);
	
	 
	 /**
	  * 年终结算报表生成 
	  * @param map
	  * @return
	  */
	 List<TbkYearBalance> selectEndTbkYearBalanceService(Map<String,Object> map);
	
}
