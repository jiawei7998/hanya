package com.singlee.cap.accounting.mapper;

import org.apache.ibatis.annotations.Param;

public interface ICurrentHistory {
	
	  /** 清除表内容，如果有chg_date，则按照日期清除 */
	  public void clearTable(@Param("history_or_current")String historyOrCurrent, @Param("task_id")String taskId, @Param("date")String date);

	  /** 把历史表内容移动到当前表 */
	  public void copyToCurrent(@Param("task_id")String taskId, @Param("date")String date);
	  
	  /** 把当前表内容移动到历史表 */
	  public void copyToHistory(@Param("task_id")String taskId);
	  
	  /** 把taskID的数据拷贝一份为targetTaskId */
	  public void copyData(@Param("history_or_current")String historyOrCurrent, @Param("task_id")String taskID, @Param("target_task_id")String targetTaskId, @Param("date")String date);
	  
	  /** 设置当前表的日期  */
	  public void setCurrentDate(@Param("task_id")String taskId, @Param("date")String date);
}
