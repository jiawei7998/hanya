package com.singlee.cap.controller;

import java.io.Serializable;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.cap.bookkeeping.model.QueryBalanceModel;
import com.singlee.cap.bookkeeping.model.TbkDealBalance;
import com.singlee.cap.bookkeeping.service.BookkeepingService;
import com.singlee.cap.bookkeeping.service.ManualAdjustmentService;
import com.singlee.cap.bookkeeping.service.TBkDealBalanceService;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;

@Controller
@RequestMapping(value = "/BookkeepingController")
public class BookkeepingController extends CommonController {

	@Autowired
	private BookkeepingService bookkeepingService;

	@Autowired
	private ManualAdjustmentService manualAdjustmentService;
	
	@Autowired
	private TBkDealBalanceService tBkDealBalanceService;
	/**
	 * 根据信息生成会计分录明细
	 * 
	 * @param tbkAmount
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/createEntry")
	public RetMsg<Serializable> createEntry(@RequestBody Map<String, Object> params){
		bookkeepingService.createEntry(params);
		return RetMsgHelper.ok();
	}
	/**
	 * 根据信息查询需要调账交易的科目余额
	 * 
	 * @param tbkAmount
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/queryDealBal")
	public RetMsg<PageInfo<TbkDealBalance>> queryDealBal(@RequestBody Map<String, Object> params){
		Page<TbkDealBalance> page = manualAdjustmentService.queryEntrySum(params);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 基金科目余额查询
	 * @param tbkAmount
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/queryFundBal")
	public RetMsg<PageInfo<QueryBalanceModel>> queryFundBal(@RequestBody Map<String, Object> params){
		Page<QueryBalanceModel> page = tBkDealBalanceService.queryFundBal(params);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 产品科目余额查询
	 * @param tbkAmount
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/queryProductBal")
	public RetMsg<PageInfo<QueryBalanceModel>> queryProductBal(@RequestBody Map<String, Object> params){
		Page<QueryBalanceModel> page = tBkDealBalanceService.queryProductBal(params);
		return RetMsgHelper.ok(page);
	}
} 
