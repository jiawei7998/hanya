package com.singlee.cap.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import jxl.Workbook;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import com.github.pagehelper.Page;
import com.singlee.cap.base.model.TbkAmount;
import com.singlee.cap.base.model.TbkEntrySetting;
import com.singlee.cap.base.model.TbkEvent;
import com.singlee.cap.base.model.TbkEntrySceneInfo;
import com.singlee.cap.base.model.TbkEntrySceneTree;
import com.singlee.cap.base.service.TbkAmountService;
import com.singlee.cap.base.service.TbkEntrySettingService;
import com.singlee.cap.base.service.TbkEventService;
import com.singlee.cap.base.service.TbkEntrySceneInfoService;
import com.singlee.cap.base.service.TbkEntrySceneTreeService;
import com.singlee.capital.base.util.FileManage;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.UploadedFile;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.spring.json.FastJsonUtil;
import com.singlee.capital.common.util.HttpParameterParse;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.TreeUtil;
import com.singlee.capital.system.controller.CommonController;

/*
 * 维度
 */
@Controller
@RequestMapping(value = "/TbkEntrySceneTreeController")
public class TbkEntrySceneTreeController extends CommonController {
	@Autowired
	private TbkEntrySceneTreeService treeService;
	@Autowired
	private TbkEntrySceneInfoService tbkEntrySceneInfoService;
	@Autowired
	private TbkEntrySettingService tbkEntrySettingService;
	@Autowired
	private TbkEventService tbkEventService;
	@Autowired
	private TbkAmountService tbkAmountService;

	/**
	 * 查询会计分录返回列表Y
	 * 
	 * @author huqin
	 * @date 2017-4-19
	 */
	@ResponseBody
	@RequestMapping(value = "/searchBkEntrySettingVoPage")
	public RetMsg<PageInfo<TbkEntrySetting>> searchBkEntrySettingVoPage(@RequestBody Map<String, Object> params) {
		Page<TbkEntrySetting> page = tbkEntrySettingService.getBkEntrySettingList(params,
				ParameterUtil.getRowBounds(params));
		return RetMsgHelper.ok(page);
	}

	/**
	 * 查询会计分录返回列表 查询全部，没有分页
	 * 
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchBkEntrySettingVoPage2")
	public RetMsg<PageInfo<TbkEntrySetting>> searchBkEntrySettingVoPage2(@RequestBody Map<String, Object> params) {
		Page<TbkEntrySetting> page = tbkEntrySettingService.getBkEntrySettingList(params);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 根据场景查询会计分录，没有分页
	 * 
	 * @param params
	 * @return
	 * @author shiting
	 */
	@ResponseBody
	@RequestMapping(value = "/searchEntrySettingByScene")
	public RetMsg<List<TbkEntrySetting>> searchEntrySettingByScene(@RequestBody Map<String, Object> params) {
		List<TbkEntrySetting> page = tbkEntrySettingService.getEntrySettingList(
				ParameterUtil.getString(params, "sceneId", ""), ParameterUtil.getString(params, "eventId", ""));
		return RetMsgHelper.ok(page);
	}

	/**
	 * 添加维度信息
	 * 
	 * @author huqin
	 * @date 2017-4-19
	 */
	@ResponseBody
	@RequestMapping(value = "/saveEntrySetting")
	public RetMsg<Serializable> saveEntrySetting(@RequestBody Map<String, Object> map) {
		tbkEntrySettingService.saveEntrySetting(map);
		return RetMsgHelper.ok();
	}

	/**
	 * 删除信息
	 * 
	 * @author huqin
	 * @date 2017-4-20
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteEntrySetting")
	public RetMsg<Serializable> deleteEntrySetting(@RequestBody String[] settingGroupId) {
		tbkEntrySettingService.deleteEntrySetting(settingGroupId);
		return RetMsgHelper.ok();
	}

	/**
	 * 获得一套分录的具体信息 map
	 * 
	 * @param acctId
	 *            分录编号
	 * @throws IOException
	 */
	@RequestMapping(value = "/selectEntrySettingVo")
	@ResponseBody
	public TbkEntrySetting selectEntrySettingVo(@RequestBody Map<String, Object> map) {
		return tbkEntrySettingService.selectEntrySetting(map);
	}

	/**
	 * 上传文件
	 * 
	 * @author huqin
	 * @date 2017-4-20
	 */
	@ResponseBody
	@RequestMapping(value = "/uploadDefExcel", method = RequestMethod.POST, produces = "text/plain;charset=UTF-8")
	public String uploadDefExcel(MultipartHttpServletRequest request, HttpServletResponse response) {
		StringBuffer sb = new StringBuffer();
		try {
			// 获取excel的工作簿
			Workbook wb = parseWorkBook(request);
			boolean rebuild = HttpParameterParse.getBooleanParameter(request, "isRebuildTable", true);
			// 重建或TRUNCATE表T_BOOKKEEPING_ENTRY_SETTING_CONFIG，插入数据
			sb.append(tbkEntrySettingService.uploadDefExcel(wb, rebuild));

		} catch (Exception e) {
			JY.error(e);
			sb.append(e.getMessage());
		}
		return StringUtils.isEmpty(sb.toString()) ? "配置成功" : sb.toString();
	}

	/**
	 * 解析xls
	 */
	public Workbook parseWorkBook(MultipartHttpServletRequest request) throws Exception {
		// 1.文件获取
		List<UploadedFile> uploadedFileList = FileManage.requestExtractor(request);
		// 2.交验并处理成excel
		List<Workbook> excelList = new LinkedList<Workbook>();
		for (UploadedFile uploadedFile : uploadedFileList) {
			JY.ensure("xls".equals(uploadedFile.getType()), "上传文件类型错误,仅允许上传xls格式文件.");
			Workbook book = Workbook.getWorkbook(new ByteArrayInputStream(uploadedFile.getBytes()));
			excelList.add(book);
		}
		JY.require(excelList.size() == 1, "只能处理一份excel");
		return excelList.get(0);

	}

	/*
	 * 查询分录配置tree
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageTbkEntrySceneTree")
	public RetMsg<List<TbkEntrySceneTree>> searchPageTbkEntrySceneTree(@RequestBody Map<String, Object> allMap)
			throws RException {
		Page<TbkEntrySceneTree> page = treeService.getTbkEntrySceneTreeList(allMap);
		return RetMsgHelper.ok(TreeUtil.mergeChildrenList(page.getResult(), "0"));
	}

	// 添加树，并关联添加分录维度
	@ResponseBody
	@RequestMapping(value = "/saveTbkEntrySceneTree")
	public RetMsg<Serializable> saveTbkEntrySceneTree(@RequestBody Map<String, Object> allMap) {
		String strTreeList = FastJsonUtil.toJSONString(allMap.get("treeList"));
		String strEntrySceneList = FastJsonUtil.toJSONString(allMap.get("entrySceneList"));
		List<TbkEntrySceneTree> treeList = FastJsonUtil.parseArrays(strTreeList, TbkEntrySceneTree.class);
		List<TbkEntrySceneInfo> entrySceneList = FastJsonUtil.parseArrays(strEntrySceneList, TbkEntrySceneInfo.class);
		// 刪除表裡的所有數據，重新插入
		if (treeList == null || entrySceneList == null) {
			return null;
		}
		treeService.delTbkEntrySceneTree();
		for (TbkEntrySceneTree tree : treeList) {
			treeService.save(tree);
		}
		tbkEntrySceneInfoService.delTbkEntrySceneInfo();
		for (TbkEntrySceneInfo ttEntrySceneInfo : entrySceneList) {
			tbkEntrySceneInfoService.save(ttEntrySceneInfo);
		}
		return RetMsgHelper.ok();
	}

	// 查询分录配置
	@ResponseBody
	@RequestMapping(value = "/searchPageBkEntrySetting")
	public RetMsg<PageInfo<TbkEntrySetting>> searchPageBkEntrySetting(@RequestBody Map<String, Object> allMap)
			throws RException {
		// 得到节点ID
		String streeNodeId = (String) allMap.get("streeNodeId");
		RowBounds rowBounds = ParameterUtil.getRowBounds(allMap);
		if (!"".equals(streeNodeId)) {
			String sceneId = tbkEntrySceneInfoService.getTbkEntrySceneInfoById(streeNodeId.toString());
			allMap.remove("streeNodeId");
			allMap.put("sceneId", sceneId);
		}

		if (!"".equals(streeNodeId)) {
			allMap.put("sceneId", streeNodeId);
		} else {
			allMap.put("sceneId", "sceneId");
		}

		Page<TbkEntrySetting> page = tbkEntrySettingService.getBkEntrySettingList(allMap, rowBounds);
		return RetMsgHelper.ok(page);
	}

	// 添加分录配置
	@ResponseBody
	@RequestMapping(value = "/saveTbkEntrySetting")
	public RetMsg<Serializable> saveTbkEntrySetting(@RequestBody Map<String, Object> allMap) {
		String strSettingList = FastJsonUtil.toJSONString(allMap.get("settingList"));
		List<TbkEntrySetting> settingList = FastJsonUtil.parseArrays(strSettingList, TbkEntrySetting.class);
		String streeNodeId = (String) allMap.get("streeNodeId");
		tbkEntrySettingService.delTbkEntrySetting(streeNodeId);
		for (TbkEntrySetting tbkEntrySetting : settingList) {
			tbkEntrySettingService.save(tbkEntrySetting);
		}
		treeService.updateTbkEntrySceneTreeNO();
		treeService.updateTbkEntrySceneTree();
		return RetMsgHelper.ok();
	}

	// 查询会计事件
	@ResponseBody
	@RequestMapping(value = "/searchPageTtAccountingEvent")
	public RetMsg<PageInfo<TbkEvent>> searchPageTtAccountingEvent(@RequestBody Map<String, Object> allMap)
			throws RException {
		RowBounds rowBounds = ParameterUtil.getRowBounds(allMap);
		Page<TbkEvent> page = tbkEventService.getTbkEventPage(allMap, rowBounds);
		return RetMsgHelper.ok(page);
	}

	// 查询会计事件
	@ResponseBody
	@RequestMapping(value = "/searchListTtAccountingEvent")
	public RetMsg<List<TbkEvent>> searchListTtAccountingEvent(@RequestBody Map<String, Object> allMap)
			throws RException {
		List<TbkEvent> list = tbkEventService.getTbkEventList(allMap);
		return RetMsgHelper.ok(list);
	}
	
	// 查询金额代码
	@ResponseBody
	@RequestMapping(value = "/searchPageTbkAmount")
	public RetMsg<PageInfo<TbkAmount>> searchPageTbkAmount(@RequestBody Map<String, Object> allMap) throws RException {
		RowBounds rowBounds = ParameterUtil.getRowBounds(allMap);
		Page<TbkAmount> page = tbkAmountService.getTbkAmountList(allMap, rowBounds);
		return RetMsgHelper.ok(page);
	}
	

	/**
	 * 查询场景对应的会计分录返回列表Y
	 * 
	 * @author 
	 */
	@ResponseBody
	@RequestMapping(value = "/searchScentEntrySettingList")
	public RetMsg<PageInfo<TbkEntrySetting>> searchScentEntrySettingList(@RequestBody Map<String, Object> params) {
		Page<TbkEntrySetting> page = tbkEntrySettingService.getScentEntrySettingList(params,
				ParameterUtil.getRowBounds(params));
		return RetMsgHelper.ok(page);
	}
}
