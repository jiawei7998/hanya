package com.singlee.cap.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.cap.base.model.TbkEvent;
import com.singlee.cap.base.service.TbkEventService;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.controller.CommonController;

@Controller
@RequestMapping(value = "/TbkEventController")
public class TbkEventController extends CommonController {
	@Autowired
	private TbkEventService tbkEventService;

	/**
     * 查询会计事件返回列表
     * @author huqin
	 * @date 2017-4-18
     */
	@ResponseBody
	@RequestMapping(value = "/searchTbkEventPage")
	public RetMsg<PageInfo<TbkEvent>> searchTbkEventPage(@RequestBody Map<String, Object> params) {
		Page<TbkEvent> page = tbkEventService.getTbkEventPage(params, ParameterUtil.getRowBounds(params));
		return RetMsgHelper.ok(page);
	}
	/**
	 * 添加会计事件代码
	 * @author huqin
	 * @date 2017-4-18
	 */
	@ResponseBody
	@RequestMapping(value = "/createTbkEvent")
	public RetMsg<Serializable> createTbkEvent(@RequestBody TbkEvent tbkEvent) {
		tbkEventService.createTbkEvent(tbkEvent);
		return RetMsgHelper.ok();
	}
	/**
	 * 修改会计事件代码 
	 * 
	 * @param acctCode
	 */
	@ResponseBody
	@RequestMapping(value = "/updateTbkEvent")
	public RetMsg<Serializable> updateTbkEvent(@RequestBody TbkEvent tbkEvent) {
		tbkEventService.updateTbkEvent(tbkEvent);
		return RetMsgHelper.ok();
	}

	/**
	 * 删除会计事件代码，删除条件为eventId
	 * 
	 * @param seq
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteTbkEvent")
	public RetMsg<Serializable> deleteTbkEvent(@RequestBody HashMap<String,Object> map) {
		tbkEventService.deleteTbkEvent(ParameterUtil.getString(map, "eventId", ""));
		return RetMsgHelper.ok();
	}
	/**
	 * 排序（上移）
	 * 
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/paixuUp")
	public RetMsg<Serializable> paixuUp(@RequestBody HashMap<String,Object> map)  throws IOException {
		tbkEventService.paixuUp(map, ParameterUtil.getRowBounds(map));
		return RetMsgHelper.ok();
	}
	/**
	 * 排序（下移）
	 * 
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/paixuDown")
	public RetMsg<Serializable> paixuDown(@RequestBody HashMap<String,Object> map)  throws IOException {
		tbkEventService.paixuDown(map, ParameterUtil.getRowBounds(map));
		return RetMsgHelper.ok();
	}
	
	
	/**
     * 查询所有tbk_event
     * @author 汪泽峰
	 * @date 2017-8-1
     */
	@ResponseBody
	@RequestMapping(value = "/getAllTbkEvent")
	public RetMsg<PageInfo<TbkEvent>> getAllTbkEvent(@RequestBody Map<String, Object> map) {
		map.put("pageSize", "99999");
		map.put("pageNumber", "1");
		Page<TbkEvent> page = tbkEventService.getTbkEventPage(map, ParameterUtil.getRowBounds(map));
		return RetMsgHelper.ok(page);
	}
	
	@ResponseBody
	@RequestMapping(value = "/getProductEvents")
	public RetMsg<List<TbkEvent>> getProductEvents(@RequestBody Map<String, Object> map) {
		return RetMsgHelper.ok(tbkEventService.getProductEvents(map));
	}
	

}
