package com.singlee.cap.controller;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.singlee.capital.common.pojo.page.PageInfo;
import com.github.pagehelper.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.singlee.cap.base.model.TbkSubjectDef;
import com.singlee.cap.base.service.TbkSubjectDefService;
import com.singlee.cap.bookkeeping.service.TbkManualEntryService;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.system.controller.CommonController;

/*
 * 科目表controller层
 */
@Controller
@RequestMapping(value = "/TbkSubjectDefController")
public class TbkSubjectDefController extends CommonController {
	/*
	 * 科目表service层
	 */
	@Autowired
	private TbkSubjectDefService tbkSubjectDefService;
	@Autowired
	private TbkManualEntryService tbkYearEntryService;

	/*
	 * 查询科目树信息
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageTbkSubjectDef")
	public RetMsg<List<TbkSubjectDef>> searchPageTbkSubjectDef(@RequestBody Map<String, Object> allMap)
			throws RException {
		List<TbkSubjectDef> list = tbkSubjectDefService.getTbkSubjectDefList(allMap);
		return RetMsgHelper.ok(list);
	}
	
	
	@ResponseBody
	@RequestMapping(value = "/searchPageTbkSubjectDefForTree")
	public RetMsg<List<TbkSubjectDef>> searchPageTbkSubjectDefForTree(@RequestBody Map<String, Object> allMap)
			throws RException {
		List<TbkSubjectDef> list = tbkSubjectDefService.getTbkSubjectDefListForTree(allMap);
		return RetMsgHelper.ok(list);
	}
	
	/*
	 * 查询科目信息
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageSubject")
	public RetMsg<PageInfo<TbkSubjectDef>> searchPageSubject(@RequestBody Map<String, Object> allMap)
			throws RException {
		Page<TbkSubjectDef> page = tbkSubjectDefService.getTbkSubjectDefPageList(allMap);
		return RetMsgHelper.ok(page);
	}


	/*
	 * 修改科目信息
	 */
	@ResponseBody
	@RequestMapping(value = "/updateTbkSubjectDef")
	public RetMsg<Serializable> updateTbkSubjectDef(@RequestBody TbkSubjectDef ttbksubjectdef) throws RException {
		tbkSubjectDefService.updateTbkSubjectDef(ttbksubjectdef);
		return RetMsgHelper.ok();
	}

	/*
	 * 添加科目信息
	 */
	@ResponseBody
	@RequestMapping(value = "/saveTbkSubjectDef")
	public RetMsg<Serializable> saveTbkSubjectDef(@RequestBody TbkSubjectDef ttbksubjectdef) throws RException {
		tbkSubjectDefService.creatTbkSubjectDef(ttbksubjectdef);
		return RetMsgHelper.ok();
	}

	/*
	 * 删除科目信息
	 */
	@ResponseBody
	@RequestMapping(value = "/delTbkSubjectDef")
	public RetMsg<Serializable> delTbkSubjectDef(@RequestBody String[] dbIds) throws RException {
		tbkSubjectDefService.delTbkSubjectDef(dbIds);
		return RetMsgHelper.ok();
	}

	/*
	 * 删除科目信息
	 */
	@ResponseBody
	@RequestMapping(value = "/delTbkSubjectDefs")
	public RetMsg<Serializable> delTbkSubjectDefs(@RequestBody String[] dbIds) throws RException {
		tbkSubjectDefService.delTbkSubjectDefs(dbIds);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 生成年结分录
	 * @param map
	 * @return
	 */ 
	@ResponseBody
	@RequestMapping(value = "/createTbkYearEntry")
	public RetMsg<Serializable> createTbkYearEntry() {
		tbkYearEntryService.createYearEntry2();
		return RetMsgHelper.ok();
	} 
	@ResponseBody
	@RequestMapping(value = "/searchPageSubjectBySubjectName")
	public RetMsg<PageInfo<TbkSubjectDef>> searchPageSubjectBySubjectName(@RequestBody Map<String, Object> allMap)
			throws RException {
		Page<TbkSubjectDef> page = tbkSubjectDefService.searchPageSubjectBySubjectName(allMap);
		return RetMsgHelper.ok(page);
	}
}
