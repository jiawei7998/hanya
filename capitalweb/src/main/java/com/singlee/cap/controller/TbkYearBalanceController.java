package com.singlee.cap.controller;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.cap.accounting.model.TbkYearBalance;
import com.singlee.cap.accounting.service.TbkYearBalanceService;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;

/**
 * 年结配置控制器层
 * 
 *
 */
@Controller
@RequestMapping("/TbkYearBalanceController")
public class TbkYearBalanceController extends CommonController{
    @Autowired
	private TbkYearBalanceService tbkYearBalanceService;
    
    /**
     * 获取年结配置列表
     * @return
     */
	@ResponseBody
	@RequestMapping(value = "/selectPageTbkYearBalance")
	public RetMsg<PageInfo<TbkYearBalance>> selectPageTbkYearBalance(@RequestBody Map<String, Object> map) {
		Page<TbkYearBalance> page=tbkYearBalanceService.selectTbkYearBalancePage(map);
		return RetMsgHelper.ok(page);
	}

    /**
     * 获取年结配置列表
     * @return
     */
	@ResponseBody
	@RequestMapping(value = "/selectTbkYearBalancekList")
	public RetMsg<List<TbkYearBalance>> selectTbkYearBalancekList(@RequestBody Map<String, Object> map) {
		List<TbkYearBalance> list=tbkYearBalanceService.selectTbkYearBalanceList(map);
		return RetMsgHelper.ok(list);
	}
	
	 /**
     * 添加年结配置列表
     * @return
     */
	@ResponseBody
	@RequestMapping(value = "/addTbkYearBalance")
	public RetMsg<Serializable> addTbkYearBalance(@RequestBody Map<String, Object> map) throws IOException {
		tbkYearBalanceService.add(map);
		return RetMsgHelper.ok();
	}
	
	 /**
     * 删除年结配置信息
     * @return
     */
	@RequestMapping(value = "/delTbkYearBalance")
	@ResponseBody
	public RetMsg<Serializable> delTbkYearBalance(@RequestBody String [] tta) throws IOException {
		tbkYearBalanceService.deleteTacTask(tta);
		return RetMsgHelper.ok();
	}
}
