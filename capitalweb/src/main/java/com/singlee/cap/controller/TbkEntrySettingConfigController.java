package com.singlee.cap.controller;

import java.io.Serializable;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.github.pagehelper.StringUtil;
import com.singlee.cap.base.model.TbkEntrySettingConfig;
import com.singlee.cap.base.service.TbkEntrySettingConfigService;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.controller.CommonController;
/*
 * 分录controller层
 */
@Controller
@RequestMapping(value = "/TbkEntrySettingConfigController")
public class TbkEntrySettingConfigController extends CommonController{
	/*
	 * 分录表service层
	 */
	@Autowired
	private TbkEntrySettingConfigService tbkEntrySettingConfigService;
	/*
	 * 查询分录参数配置信息
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageConfig")
	public RetMsg<PageInfo<TbkEntrySettingConfig>> searchPageConfig(@RequestBody Map<String,Object> allMap) throws RException {
		Page<TbkEntrySettingConfig> page = tbkEntrySettingConfigService.getEntrySettingConfigPage(allMap, ParameterUtil.getRowBounds(allMap));
		return RetMsgHelper.ok(page);
	}

	/**
	 * 添加分录参数配置代码
	 * 
	 * @param tbkAmount
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/createEntrySettingConfig")
	public RetMsg<Serializable> createEntrySettingConfig(@RequestBody Map<String,Object> map) {
		map.put("entryTypeValue", ","+map.get("entryTypeValue")+",");
		
		if(StringUtil.isNotEmpty(map.get("entryType1Value").toString())){
			map.put("entryType1Value", ","+map.get("entryType1Value").toString()+",");
		}
		if(StringUtil.isNotEmpty(map.get("entryType2Value").toString())){
			map.put("entryType2Value", ","+map.get("entryType2Value").toString()+",");
		}
		tbkEntrySettingConfigService.saveEntrySettingConfig(map);
		return RetMsgHelper.ok();
	}

	/**
	 * 修改分录参数配置代码
	 * 
	 * @param tbkAmount
	 */
	@ResponseBody
	@RequestMapping(value = "/updateEntrySettingConfig")
	public RetMsg<Serializable> updateEntrySettingConfig(@RequestBody Map<String,Object> map) {
		map.put("entryTypeValue", ","+map.get("entryTypeValue")+",");
		
		if(StringUtil.isNotEmpty(map.get("entryType1Value").toString())){
			map.put("entryTypeValue", ","+map.get("entryType1Value").toString()+",");
		}
		if(StringUtil.isNotEmpty(map.get("entryType2Value").toString())){
			map.put("entryTypeValue", ","+map.get("entryType2Value").toString()+",");
		}
		tbkEntrySettingConfigService.updateEntrySettingConfig(map);
		return RetMsgHelper.ok();
	}

	/**
	 * 删除分录参数配置
	 * 
	 * @param amountId
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteEntrySettingConfig")
	public RetMsg<Serializable> deleteEntrySettingConfig(@RequestBody Map<String,Object> map) {
		tbkEntrySettingConfigService.deleteEntrySettingConfig(map);
		return RetMsgHelper.ok();
	}

	/**
	 * 查询科目列表及动态科目信息
	 */
	@ResponseBody
	@RequestMapping(value = "/searchSubjAndConfig")
	public RetMsg<PageInfo<TbkEntrySettingConfig>> searchSubjAndConfig(@RequestBody Map<String,Object> allMap) throws RException {
		Page<TbkEntrySettingConfig> page = tbkEntrySettingConfigService.searchSubjAndConfig(allMap, ParameterUtil.getRowBounds(allMap));
		return RetMsgHelper.ok(page);
	}

}
