package com.singlee.cap.controller;


import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import java.io.Serializable;
import java.util.HashMap;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.github.pagehelper.Page;
import com.singlee.cap.base.model.TbkParamsMap;
import com.singlee.cap.base.model.TbkParamsMapModule;
import com.singlee.cap.base.service.TbkParamsMapService;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.controller.CommonController;

@Controller
@RequestMapping(value = "/ParamsMapController")
public class TbkParamsMapController extends CommonController {
	@Autowired
	private TbkParamsMapService paramsMapService;
	
	
	/**
	 * @author huqin
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPvalueTxtList")
	public RetMsg<List<TbkParamsMap>> searchPvalueTxtList(@RequestBody Map<String,Object> params){
		//String paramId = ParameterUtil.getString(params, "paramId", null);
		List<TbkParamsMap> list = paramsMapService.getPvalueTxtList(params);
		return RetMsgHelper.ok(list);
	}
  
	/**
	 * 获取维度值信息
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchParamsMapPage")
	public RetMsg<PageInfo<TbkParamsMap>> searchParamsMapPage(@RequestBody Map<String, Object> params) {
		Page<TbkParamsMap> page = paramsMapService.searchParamsMapPage(params);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 添加维度值信息
	 * @param paramsMap
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/createParamsMap")
	public RetMsg<Serializable> createParamsMap(@RequestBody TbkParamsMap paramsMap) {
		paramsMapService.createParamsMap(paramsMap);
		return RetMsgHelper.ok();
	}

	/**
	 * 修改维度值信息
	 * @param paramsMap
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/updateParamsMap")
	public RetMsg<Serializable> updateParamsMap(@RequestBody TbkParamsMap paramsMap) {
		paramsMapService.updateParamsMap(paramsMap);
		return RetMsgHelper.ok();
	}

	/**
	 * 删除维度值信息
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteParamsMap")
	public RetMsg<Serializable> deleteParamsMap(@RequestBody HashMap<String,Object> map) {
		paramsMapService.deleteParamsMap(ParameterUtil.getString(map, "pKey", ""));
		return RetMsgHelper.ok();
	}

	/**
	 * @author shiting
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchBySourceList")
	public RetMsg<List<TbkParamsMap>> searchBySourceList(@RequestBody Map<String,Object> params){
		//String paramId = ParameterUtil.getString(params, "paramId", null);
		List<TbkParamsMap> list = paramsMapService.searchBySourceList(params);
		return RetMsgHelper.ok(list);
	}
	
	/**
	 * @author汪泽峰 
	 * 查找Paramid为prdNo的所有产品
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchByParamId")
	public RetMsg<List<TbkParamsMap>> searchByParamId(@RequestBody Map<String,Object> map){
		map.put("pageSize", "99999");
		map.put("pageNumber", "1");
		map.put("paramId", "prdNo");
		Page<TbkParamsMap> page = paramsMapService.searchParamsMapPage(map);
		List<TbkParamsMap> list = page;
		return RetMsgHelper.ok(list);
	}
	
	/**
	 * 获取维度值信息
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchParamsMapPages")
	public RetMsg<List<TbkParamsMapModule>> searchParamsMapPages(@RequestBody Map<String, Object> params) {
		List<TbkParamsMapModule> list = paramsMapService.searchParamsMapPages(params);
		return RetMsgHelper.ok(list);
	}
	
}
