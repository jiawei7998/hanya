package com.singlee.cap.controller;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.github.pagehelper.Page;
import com.singlee.cap.base.model.TbkParamsDic;
import com.singlee.cap.base.service.TbkParamsDicService;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.controller.CommonController;

@Controller
@RequestMapping(value = "/ParamsDicController")
public class TbkParamsDicController extends CommonController {
	@Autowired
	private TbkParamsDicService paramsDicService;
	/**
	 * 查询维度字典
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchParamsDicPage")
	public RetMsg<PageInfo<TbkParamsDic>> searchParamsPage(@RequestBody Map<String, Object> params) {
		Page<TbkParamsDic> page = paramsDicService.searchParamsDicPage(params);
		return RetMsgHelper.ok(page);
	}
	/**
	 * 添加维度字典
	 * @param paramsDic
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/createParamsDic")
	public RetMsg<Serializable> createParamsDic(@RequestBody TbkParamsDic paramsDic) {
		paramsDicService.createParamsDic(paramsDic);
		return RetMsgHelper.ok();
	}
	/**
	 * 修改维度字典
	 * @param paramsDic
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/updateParamsDic")
	public RetMsg<Serializable> updateParamsDic(@RequestBody TbkParamsDic paramsDic) {
		paramsDicService.updateParamsDic(paramsDic);
		return RetMsgHelper.ok();
	}
	/**
	 * 删除维度字典
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteParamsDic")
	public RetMsg<Serializable> deleteParamsDic(@RequestBody HashMap<String,Object> map) {
		paramsDicService.deleteParamsDic(ParameterUtil.getString(map, "paramId", ""));
		return RetMsgHelper.ok();
	}
}
