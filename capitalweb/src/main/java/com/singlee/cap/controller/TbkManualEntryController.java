package com.singlee.cap.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.cap.bookkeeping.model.TbkManualEntry;
import com.singlee.cap.bookkeeping.service.TbkManualEntryService;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.spring.json.FastJsonUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;

@Controller
@RequestMapping(value = "/TbkManualEntryController")
public class TbkManualEntryController extends CommonController{
	@Autowired
	private TbkManualEntryService tbkYearEntryService;
	/**
	 * 条件查询列表(我发起的)
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-8-10
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageTbkYearEntryByMyself")
	public RetMsg<PageInfo<TbkManualEntry>> searchPageTbkYearEntryByMyself(@RequestBody Map<String,Object> params){
		if(!"year".equals(ParameterUtil.getString(params, "eventType", ""))){
			params.put("userId", SlSessionHelper.getUserId());
		}
		params.put("isActive", DictConstants.YesNo.YES);
//		String approveStatusArr =  DictConstants.ApproveStatus.New;
//		params.put("approveStatus", approveStatusArr.split(","));
		Page<TbkManualEntry> page = tbkYearEntryService.getManualEntryListMine(params,ParameterUtil.getRowBounds(params));
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 条件查询列表(未完成)
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-8-10
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageTbkYearEntryUnfinished")
	public RetMsg<PageInfo<TbkManualEntry>> searchPageTbkYearEntryUnfinished(@RequestBody Map<String,Object> params){
		Page<TbkManualEntry> page  = new Page<TbkManualEntry>();
		try {
			params.put("isActive", DictConstants.YesNo.YES);
			String approveStatusArr =  DictConstants.ApproveStatus.WaitApprove+","+DictConstants.ApproveStatus.Approving;
			params.put("approveStatus", approveStatusArr.split(","));
			params.put("userId", SlSessionHelper.getUserId());
			params.put("insId", SlSessionHelper.getInstitutionId());  
			page = tbkYearEntryService.getManualEntryList(params,ParameterUtil.getRowBounds(params));
		} catch (Exception e) {
			//e.printStackTrace();
			throw new RException(e);
		}
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 手工调账 --待审批列表 查询
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageTbkYearEntryUnfinishedHand")
	public RetMsg<PageInfo<TbkManualEntry>> searchPageTbkYearEntryUnfinishedHand(@RequestBody Map<String,Object> params){
		Page<TbkManualEntry> page  = new Page<TbkManualEntry>();
		try {
			params.put("isActive", DictConstants.YesNo.YES);
			String approveStatusArr =  DictConstants.ApproveStatus.WaitApprove+","+DictConstants.ApproveStatus.Approving;
			params.put("approveStatus", approveStatusArr.split(","));
			params.put("userId", SlSessionHelper.getUserId());
			params.put("insId", SlSessionHelper.getInstitutionId());  
			page = tbkYearEntryService.getManualEntryListHand(params,ParameterUtil.getRowBounds(params));
		} catch (Exception e) {
			//e.printStackTrace();
			throw new RException(e);
		}
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 条件查询列表(已审批)
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-8-10
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageTbkYearEntryFinished")
	public RetMsg<PageInfo<TbkManualEntry>> searchPageTbkYearEntryFinished(@RequestBody Map<String,Object> params){
		params.put("isActive", DictConstants.YesNo.YES);
		String approveStatus = ParameterUtil.getString(params, "approveStatus", "");
		if(StringUtil.isNullOrEmpty(approveStatus)){
			approveStatus = DictConstants.ApproveStatus.Approving + "," + DictConstants.ApproveStatus.ApprovedPass;
			approveStatus += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			params.put("approveStatus", approveStatus.split(","));
		}
		params.put("userId", SlSessionHelper.getUserId());
		Page<TbkManualEntry> page = tbkYearEntryService.getManualEntryFinishList(params,ParameterUtil.getRowBounds(params));
		return RetMsgHelper.ok(page);
	}

	/**
	 * 条件查询列表(我发起的)
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-8-10
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageTbkYearEntry")
	public RetMsg<PageInfo<TbkManualEntry>> searchPageTbkYearEntry(@RequestBody Map<String,Object> params){
		Page<TbkManualEntry> page = tbkYearEntryService.getEntryList(params, ParameterUtil.getRowBounds(params));
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 修改分录实际金额
	 * @param map
	 * @return
	 */ 
	@ResponseBody
	@RequestMapping(value = "/updateTbkYearEntry")
	public RetMsg<Serializable> updateTbkYearEntry(@RequestBody TbkManualEntry map) {
		tbkYearEntryService.update(map);
		return RetMsgHelper.ok();
	} 
	/*
	 * 保存手工调账分录
	 */
	@ResponseBody
	@RequestMapping(value = "/addEntryList")
	public  RetMsg<Serializable> addEntryList(@RequestBody Map<String,Object> params) {
		String entryJson = params.get("constact_list").toString();
		List<TbkManualEntry> entryList = FastJsonUtil.parseArrays(entryJson, TbkManualEntry.class);
		tbkYearEntryService.saveManualEntry(params,entryList);
		return RetMsgHelper.ok();
	}
	

	/**
	 * 获得一套分录的具体信息 map
	 * 
	 * @param acctId
	 *            分录编号
	 * @throws IOException
	 */
	@RequestMapping(value = "/selectManualEntryVo")
	@ResponseBody
	public TbkManualEntry selectManualEntryVo(@RequestBody Map<String, Object> map) {
		return tbkYearEntryService.selectManualEntryVo(map);
	}
    
	/*
	 * 年终结算确认
	 */
	@ResponseBody
	@RequestMapping(value = "/comfirYearendEntry")
	public  RetMsg<Serializable> comfirYearendEntry(@RequestBody Map<String,Object> params) {
		tbkYearEntryService.comfirYearendEntry(params);
		return RetMsgHelper.ok();
	}
	
	
	/*
	 * 年终发送
	 */
	@ResponseBody
	@RequestMapping(value = "/sendYearendInterfaceToT24")
	public  RetMsg<Serializable> sendYearendInterfaceToT24(@RequestBody Map<String,Object> params) {
		String flowId = "";
		flowId = tbkYearEntryService.selectYearendEntryFlowId(params);
		if(flowId != null && !"".equals(flowId)){
			String[] tta = flowId.split(",");
			for (String string : tta) {
				tbkYearEntryService.sendYearendInterfaceToT24(string);
			}
		}else{
			JY.error("没有需要发送核心的年结信息");
			throw new RException("没有需要发送核心的年结信息");
		}
		return RetMsgHelper.ok();
	}
	/*
	 * 手工调账删除
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteManualEntry")
 public	RetMsg<Serializable> deleteManualEntry(@RequestBody Map<String,Object> map){
		tbkYearEntryService.deleteManualEntry(map);
		return RetMsgHelper.ok();
	}
}
