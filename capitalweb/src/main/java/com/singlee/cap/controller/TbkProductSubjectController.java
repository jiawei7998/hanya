package com.singlee.cap.controller;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.cap.base.model.TbkProductSubject;
import com.singlee.cap.base.service.TbkProductSubjectService;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.controller.CommonController;
@Controller
@RequestMapping(value="/ProductSubjectController")
public class TbkProductSubjectController extends CommonController {

	@Autowired
	private TbkProductSubjectService tbkProductSubjectService;
	
	@ResponseBody
	@RequestMapping(value = "/searchProductSubjectPage")
	public RetMsg<PageInfo<TbkProductSubject>> searchProductSubjectPage(@RequestBody Map<String,Object> allMap) throws RException {
		Page<TbkProductSubject> page = tbkProductSubjectService.searchProductSubjectsPage(allMap);
		return RetMsgHelper.ok(page);
	}
	
	@ResponseBody
	@RequestMapping(value = "/deleteProductSubjectPage")
	public RetMsg<Serializable> deleteProductSubjectPage (@RequestBody HashMap<String, String> map){
		tbkProductSubjectService.deleteProductSubjectPage(ParameterUtil.getString(map, "ssId", ""));
		return RetMsgHelper.ok();
	}
	
	@ResponseBody
	@RequestMapping(value = "/addProductSubjectPage")
	public RetMsg<Serializable> addProductSubjectPage (@RequestBody TbkProductSubject tbkProductSubject ){
		tbkProductSubjectService.addProductSubjectPage(tbkProductSubject);
		return RetMsgHelper.ok();
	}
	
	@ResponseBody
	@RequestMapping(value = "/updateProductSubjectPage")
	public RetMsg<Serializable> updateProductSubjectPage (@RequestBody TbkProductSubject tbkProductSubject ){
		tbkProductSubjectService.updateProductSubjectPage(tbkProductSubject);
		return RetMsgHelper.ok();
	}

}
