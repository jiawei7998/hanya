package com.singlee.cap.controller;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.github.pagehelper.Page;
import com.singlee.cap.bookkeeping.model.TbkEntry;
import com.singlee.cap.bookkeeping.service.TbkEntryService;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.controller.CommonController;

/*
 * 分录controller层
 */
@Controller
@RequestMapping(value = "/TbkEntryController")
public class TbkEntryController extends CommonController{
	/*
	 * 分录表service层
	 */
	@Autowired
	private TbkEntryService bookkeepingEntryService;
	/*
	 * 查询分录信息
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageEntries")
	public RetMsg<PageInfo<List<TbkEntry>>> searchPageEntries(@RequestBody Map<String,Object> allMap) throws RException {
		Page<List<TbkEntry>> page = bookkeepingEntryService.getEntryList(allMap, ParameterUtil.getRowBounds(allMap));
		return RetMsgHelper.ok(page);
	}


}
