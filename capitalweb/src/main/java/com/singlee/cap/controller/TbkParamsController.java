package com.singlee.cap.controller;


import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import java.io.Serializable;
import java.util.HashMap;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.github.pagehelper.Page;
import com.singlee.cap.base.model.TbkParams;
import com.singlee.cap.base.model.TbkParamsMap;
import com.singlee.cap.base.service.TbkParamsService;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.controller.CommonController;
 

@Controller
@RequestMapping(value = "/ParamsController")
public class TbkParamsController extends CommonController {

	
	@Autowired
	private TbkParamsService paramsService;
	
	/**
	 * @author huqin
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchParamName")
	public RetMsg<PageInfo<TbkParams>> searchParamName(@RequestBody Map<String,Object> params){
		Page<TbkParams> page = paramsService.getParamName(params);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 获取维度信息
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchParamsPage")
	public RetMsg<PageInfo<TbkParams>> searchParamsPage(@RequestBody Map<String, Object> params) {
		Page<TbkParams> page = paramsService.searchParamsPage(params);
		return RetMsgHelper.ok(page);
	}

	
	/**
	 * 添加维度信息
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/createParams")
	public RetMsg<Serializable> createParams(@RequestBody TbkParams params) {
		paramsService.createParams(params);
		return RetMsgHelper.ok();
	}

	/**
	 * 修改维度信息
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/updateParams")
	public RetMsg<Serializable> updateParams(@RequestBody TbkParams params) {
		paramsService.updateParams(params);
		return RetMsgHelper.ok();
	}

	/**
	 * 删除维度信息
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteParams")
	public RetMsg<Serializable> deleteParams(@RequestBody HashMap<String,Object> map) {
		paramsService.deleteParams(ParameterUtil.getString(map, "paramId", ""));
		return RetMsgHelper.ok();
	}
	
	@ResponseBody
	@RequestMapping(value = "/changeParamsStatus")
	public RetMsg<Serializable> changeParamsStatus(@RequestBody HashMap<String,Object> map) {
		paramsService.changeParamsStatus(ParameterUtil.getString(map, "paramId", ""));
		return RetMsgHelper.ok();
	}

	@ResponseBody
	@RequestMapping(value = "/getParamsActiveList")
	public RetMsg<List<TbkParams>> getParamsActiveList() {
		List<TbkParams> paramsList = paramsService.getParamsActiveList("1");
		return RetMsgHelper.ok(paramsList);
	}
}
