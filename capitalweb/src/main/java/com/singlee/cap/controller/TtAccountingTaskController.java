package com.singlee.cap.controller;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.cap.accounting.model.TacTask;
import com.singlee.cap.accounting.service.AccountingTaskService;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;

/**
 * 账套控制器层
 * 
 *
 */
@Controller
@RequestMapping("/TtAccountingTaskController")
public class TtAccountingTaskController extends CommonController{
    @Autowired
	private AccountingTaskService ttaccountingtaskService;
    
    /**
     * 获取账套列表
     * @return
     */
	@ResponseBody
	@RequestMapping(value = "/selectPageAccountingTask")
	public RetMsg<PageInfo<TacTask>> selectPageBAccountingTask(@RequestBody Map<String, Object> map) {
		Page<TacTask> page=ttaccountingtaskService.selectTacTask(map);
		return RetMsgHelper.ok(page);
	}

    /**
     * 获取账套列表
     * @return
     */
	@ResponseBody
	@RequestMapping(value = "/selectAccountingTaskList")
	public RetMsg<List<TacTask>> selectAccountingTaskList(@RequestBody Map<String, Object> map) {
		List<TacTask> list=ttaccountingtaskService.selectTaskList(map);
		return RetMsgHelper.ok(list);
	}
	
	 /**
     * 添加账套列表
     * @return
     */
	@ResponseBody
	@RequestMapping(value = "/addTacTask")
	public RetMsg<Serializable> addTacTask(@RequestBody Map<String, Object> map) throws IOException {
		ttaccountingtaskService.add(map);
		return RetMsgHelper.ok();	
	}
	
	 /**
     * 修改账套列表
     * @return
     */
	@RequestMapping(value = "/editTacTask")
	@ResponseBody
	public RetMsg<Serializable> editTacTask(@RequestBody Map<String, Object> map) throws IOException {
		ttaccountingtaskService.updateTask(map);
		return RetMsgHelper.ok();		
	}
	 /**
     * 删除账套信息
     * @return
     */
	@RequestMapping(value = "/delTacTask")
	@ResponseBody
	public RetMsg<Serializable> delTacTask(@RequestBody String [] tta) throws IOException {
		ttaccountingtaskService.deleteTacTask(tta);
		return RetMsgHelper.ok();
	}
}
