package com.singlee.cap.controller;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.github.pagehelper.StringUtil;
import com.singlee.cap.base.model.TbkProductEvent;
import com.singlee.cap.base.model.TbkScenceSubject;
import com.singlee.cap.base.service.TbkProductEventService;
import com.singlee.cap.base.service.TbkScenceSubjectService;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.spring.json.FastJsonUtil;
import com.singlee.capital.dict.model.TaDictVo;
import com.singlee.capital.dict.service.DictionaryGetService;
import com.singlee.capital.system.controller.CommonController;

@Controller
@RequestMapping(value = "/TbkProductEventController")
public class TbkProductEventController extends CommonController {
	@Autowired
	private TbkProductEventService tbkProductEventService;
	@Autowired
	private TbkScenceSubjectService tbkScenceSubjectService;
	@Autowired
	private DictionaryGetService dictionaryGetService;

	
	/**
	 * 新增产品事件
	 * @author 汪泽峰
	 * @date 2017-8-1
	 */
	@ResponseBody
	@RequestMapping(value = "/addTbkProductEvent")
	public RetMsg<Serializable> addTbkProductEvent(@RequestBody Map<String,Object> map) {
		String [] eventid = map.get("eventId").toString().split(",");
		tbkProductEventService.deleteTbkProductEventByProNo(map);
		for(int i=0;i<eventid.length;i++){
			map.put("eventId", eventid[i]);
			tbkProductEventService.addTbkProductEventService(map);  //循环插入 
		}
		return RetMsgHelper.ok();
		
	}
	
	/**
	 * 查询所有产品事件
	 * @author 汪泽峰
	 * @date 2017-8-1
	 */
	@ResponseBody
	@RequestMapping(value = "/showAllTbkProductEventPaged")
	public RetMsg<PageInfo<TbkProductEvent>> showAllTbkProductEventPaged(@RequestBody Map<String,Object> map) {
		Page<TbkProductEvent> page = tbkProductEventService.showAllTbkProductEventService(map);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 删除 产品事件
	 * @author 汪泽峰
	 * @date 2017-8-1
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteTbkProductEvent")
	public RetMsg<Serializable> deleteTbkProductEvent(@RequestBody Map<String,Object> map) {
		tbkProductEventService.deleteTbkProductEventById(map);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 查询所有产品科目
	 * @author 汪泽峰
	 * @date 2017-8-1
	 */
	@ResponseBody
	@RequestMapping(value = "/showAllTbkScenceSubjectPaged")
	public RetMsg<PageInfo<TbkScenceSubject>> showAllTbkScenceSubjectPaged(@RequestBody Map<String,Object> map) {
		//tbkEntryInterfaceService.GtpTbkEntryOutputDataWrite(map);
		Page<TbkScenceSubject> page = tbkScenceSubjectService.showAllTbkScenceSubject(map);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 新增
	 * @author 汪泽峰
	 * @date 2017-8-1
	 */
	@ResponseBody
	@RequestMapping(value = "/addTbkScenceSubject")
	public RetMsg<Serializable> addTbkScenceSubject(@RequestBody Map<String,Object> map) {
		String entryJson = map.get("constact_list").toString();
		List<TbkScenceSubject> entryList = FastJsonUtil.parseArrays(entryJson, TbkScenceSubject.class);
		
		for (TbkScenceSubject tbkScenceSubject : entryList) {
			Map<String, Object> map1 = new HashMap<String, Object>();
			for(Map.Entry<String, Object> e :map.entrySet()){
				map1.put(e.getKey(), e.getValue());
			}
			map1.remove("constact_list");
			if(StringUtil.isNotEmpty(map1.get("paramVal1cn").toString())){
				//根据字典名字  和 key 值  获取 字典中文 值 
				TaDictVo tdv = dictionaryGetService.getTaDictByCodeAndKey(map1.get("defKey1").toString(),map1.get("paramVal1cn").toString());
				map1.put("paramVal1",tdv.getDict_value() );
				map1.put("paramVal1dv", map1.get("paramVal1cn"));  
			}
			if(StringUtil.isNotEmpty(map1.get("paramVal2cn").toString())){
				//根据字典名字  和 key 值  获取 字典中文 值 
				TaDictVo tdv = dictionaryGetService.getTaDictByCodeAndKey(map1.get("defKey2").toString(),map1.get("paramVal2cn").toString());
				map1.put("paramVal2", tdv.getDict_value());
				map1.put("paramVal2dv", map1.get("paramVal2cn"));  
			}
			if(StringUtil.isNotEmpty(map1.get("paramVal3cn").toString())){
				//根据字典名字  和 key 值  获取 字典中文 值 
				TaDictVo tdv = dictionaryGetService.getTaDictByCodeAndKey(map1.get("defKey3").toString(),map1.get("paramVal3cn").toString());
				map1.put("paramVal3", tdv.getDict_value());
				map1.put("paramVal3dv", map1.get("paramVal3cn"));  
			}
			map1.put("paramVal1cn", map1.get("val1"));
			map1.put("paramVal2cn", map1.get("val2"));
			map1.put("paramVal3cn", map1.get("val3"));
			map1.put("subjCode", tbkScenceSubject.getSubjCode());
			map1.put("subjName", tbkScenceSubject.getSubjName());
			tbkScenceSubjectService.addTbkScenceSubject(map1);
			map1.clear();
		}
		return RetMsgHelper.ok();
		
	}
	
	/**
	 * 删除 
	 * @author 汪泽峰
	 * @date 2017-8-1
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteTbkScenceSubject")
	public RetMsg<Serializable> deleteTbkScenceSubject(@RequestBody Map<String,Object> map) {
		tbkScenceSubjectService.deleteTbkScenceSubject(map);
		return RetMsgHelper.ok();
	}
	

}
