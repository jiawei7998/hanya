package com.singlee.cap.controller;

import java.io.Serializable;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.cap.bookkeeping.model.TbkManualEntry;
import com.singlee.cap.bookkeeping.service.TbkManualEntryService;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;

@Controller
@RequestMapping(value = "/YearBalController")
public class YearBalController extends CommonController{
	
	@Autowired
	private TbkManualEntryService tbkYearEntryService;
	
	/**
	 * 条件查询列表(我发起的)
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-8-10
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageTbkYearEntryByMyself")
	public RetMsg<PageInfo<TbkManualEntry>> searchPageTbkYearEntryByMyself(@RequestBody Map<String,Object> params){
		params.put("userId", SlSessionHelper.getUserId());
		String approveStatusArr =  DictConstants.ApproveStatus.New;
		params.put("approveStatus", approveStatusArr.split(","));
		Page<TbkManualEntry> page = tbkYearEntryService.getEntryList(params,ParameterUtil.getRowBounds(params));
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 条件查询列表(未完成)
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-8-10
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageTbkYearEntryUnfinished")
	public RetMsg<PageInfo<TbkManualEntry>> searchPageTbkYearEntryUnfinished(@RequestBody Map<String,Object> params){
		Page<TbkManualEntry> page  = new Page<TbkManualEntry>();
		try {
			params.put("isActive", DictConstants.YesNo.YES);
			String approveStatusArr =  DictConstants.ApproveStatus.WaitApprove+","+DictConstants.ApproveStatus.Approving;
			params.put("approveStatus", approveStatusArr.split(","));
			params.put("userId", SlSessionHelper.getUserId());
			params.put("insId", SlSessionHelper.getInstitutionId());  
			page = tbkYearEntryService.getEntryList(params,ParameterUtil.getRowBounds(params));
		} catch (Exception e) {
			//e.printStackTrace();
			throw new RException(e);
		}
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 条件查询列表(已审批)
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-8-10
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageTbkYearEntryFinished")
	public RetMsg<PageInfo<TbkManualEntry>> searchPageTbkYearEntryFinished(@RequestBody Map<String,Object> params){
		params.put("isActive", DictConstants.YesNo.YES);
		String approveStatus = ParameterUtil.getString(params, "approveStatus", "");
		if(StringUtil.isNullOrEmpty(approveStatus)){
			approveStatus = DictConstants.ApproveStatus.Approving + "," + DictConstants.ApproveStatus.ApprovedPass;
			approveStatus += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			params.put("approveStatus", approveStatus.split(","));
		}
		params.put("userId", SlSessionHelper.getUserId());
		Page<TbkManualEntry> page = tbkYearEntryService.getEntryList(params,ParameterUtil.getRowBounds(params));
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 修改分录实际金额
	 * @param map
	 * @return
	 */ 
	@ResponseBody
	@RequestMapping(value = "/updateTbkYearEntry")
	public RetMsg<Serializable> updateTbkYearEntry(@RequestBody TbkManualEntry map) {
		tbkYearEntryService.update(map);
		return RetMsgHelper.ok();
	} 

	
}
