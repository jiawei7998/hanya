package com.singlee.cap.controller;

import java.util.HashMap;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.cap.bookkeeping.mapper.BalanceOfAccountFor407Mapper;
import com.singlee.cap.bookkeeping.mapper.TbkEntryMapper;
import com.singlee.cap.bookkeeping.mapper.TbkSubjectBalanceSumMapper;
import com.singlee.cap.bookkeeping.model.TbkEntry;
import com.singlee.cap.bookkeeping.model.TbkSubjectBalanceSum;
import com.singlee.cap.bookkeeping.model.TiInterfaceTdpacstmtinq;
import com.singlee.cap.bookkeeping.service.TBkSubjectBalanceSumService;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.controller.CommonController;
/*
 * 科目表controller层
 */
@Controller
@RequestMapping(value = "/TbkSubjectBalanceSumController")
public class TbkSubjectBalanceSumController extends CommonController{
	/*
	 * 科目表service层
	 */
	@Autowired
	private TBkSubjectBalanceSumService tBkSubjectBalanceSumService;
	@Autowired
	private TbkEntryMapper tbkEntryMapper;
	@Autowired
	private BalanceOfAccountFor407Mapper balanceOfAccountFor407Mapper;
	/*
	 * 查询科目信息
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageTbkSubjectBalanceSum")
	public RetMsg<PageInfo<TbkSubjectBalanceSum>> searchPageTbkSubjectBalanceSum(@RequestBody Map<String,Object> allMap) throws RException {
		RowBounds rowBounds = ParameterUtil.getRowBounds(allMap);
		Page<TbkSubjectBalanceSum> page = tBkSubjectBalanceSumService.selectByMap(allMap, rowBounds);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 407对账
	 */
	@ResponseBody
	@RequestMapping(value = "/balanceOfAccountFor407")
	public RetMsg<PageInfo<TbkSubjectBalanceSum>> balanceOfAccountFor407(@RequestBody HashMap<String,Object> allMap) throws RException {
		RowBounds rowBounds = ParameterUtil.getRowBounds(allMap);
		Page<TbkSubjectBalanceSum> page = tBkSubjectBalanceSumService.balanceOfAccountFor407(allMap, rowBounds);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 407对账详情  同业
	 */
	@ResponseBody
	@RequestMapping(value = "/searchBalanceOfAccountFor407TY")
	public RetMsg<PageInfo<TbkEntry>> searchBalanceOfAccountFor407TY(@RequestBody Map<String,Object> allMap) throws RException {
		RowBounds rowBounds = ParameterUtil.getRowBounds(allMap);
		Page<TbkEntry> page = null;
		String ccy = ParameterUtil.getString(allMap, "ccy", "");
//		if(ccy.equals("CNY")){
//			page = tbkEntryMapper.selectByBkkpgOrgIdAndPostDateAndCcy(allMap, rowBounds);//人名币
//		}
//		if(ccy.equals("USD")){
			page = tbkEntryMapper.selectByBkkpgOrgIdAndPostDateAndUsd(allMap, rowBounds);//美元
//		}	
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 407对账详情  核心
	 */
	@ResponseBody
	@RequestMapping(value = "/searchBalanceOfAccountFor407HX")
	public RetMsg<PageInfo<TiInterfaceTdpacstmtinq>> searchBalanceOfAccountFor407HX(@RequestBody Map<String,Object> allMap) throws RException {
		RowBounds rowBounds = ParameterUtil.getRowBounds(allMap);
		Page<TiInterfaceTdpacstmtinq> page = null;
		String ccy = ParameterUtil.getString(allMap, "ccy", "");
//		if(ccy.equals("CNY")){
//			page = balanceOfAccountFor407Mapper.selectByBookingDateAndInstIdAndCcy(allMap, rowBounds);//人名币
//		}
//		if(ccy.equals("USD")){
			page = balanceOfAccountFor407Mapper.selectByBookingDateAndInstIdAndUsd(allMap, rowBounds);//美元
//		}	
		
		return RetMsgHelper.ok(page);
	}
}
