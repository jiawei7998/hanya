package com.singlee.cap.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.cap.base.model.TbkEntrySceneRules;
import com.singlee.cap.base.service.TbkEntrySceneRulesService;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.controller.CommonController;

/*
 * 维度
 */
@Controller
@RequestMapping(value = "/TbkEntrySceneRulesController")
public class TbkEntrySceneRulesController extends CommonController {

	@Autowired
	private TbkEntrySceneRulesService tbkEntrySceneRulesService;
	/**
	 * @author shiting
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchTbkEntrySceneRules")
	public RetMsg<PageInfo<TbkEntrySceneRules>> searchTbkEntrySceneRules(@RequestBody Map<String,Object> map){
		Page<TbkEntrySceneRules> page = tbkEntrySceneRulesService.getTbkEntrySceneRulesList(map, ParameterUtil.getRowBounds(map));
		return RetMsgHelper.ok(page);
	}

	/**
	 * 添加维度信息
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/createTbkEntrySceneRules")
	public RetMsg<Serializable> createTbkEntrySceneRules(@RequestBody Map<String,Object> map) {
		tbkEntrySceneRulesService.insertTbkEntrySceneRules(map);
		return RetMsgHelper.ok();
	}

	/**
	 * 修改维度信息
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/updateTbkEntrySceneRules")
	public RetMsg<Serializable> updateTbkEntrySceneRules(@RequestBody Map<String, Object> map) {
		tbkEntrySceneRulesService.updateTbkEntrySceneRules(map);
		return RetMsgHelper.ok();
	}

	/**
	 * 删除维度信息
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteTbkEntrySceneRules")
	public RetMsg<Serializable> deleteTbkEntrySceneRules(@RequestBody HashMap<String,Object> map) {
		tbkEntrySceneRulesService.deleteTbkEntrySceneRules(ParameterUtil.getString(map, "taskId", ""));
		return RetMsgHelper.ok();
	}

	/**
	 * 获得一套分录的具体信息 map
	 * 
	 * @param acctId
	 *            分录编号
	 * @throws IOException
	 */
	@RequestMapping(value = "/selectTbkEntrySceneRules")
	@ResponseBody
	public TbkEntrySceneRules selectTbkEntrySceneRules(@RequestBody Map<String, Object> map) {
		return tbkEntrySceneRulesService.selectTbkEntrySceneRules(map);
	}

	
}
