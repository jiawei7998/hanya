package com.singlee.cap.controller;

import java.io.Serializable;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.github.pagehelper.Page;
import com.singlee.cap.base.model.TbkEntrySceneInfo;
import com.singlee.cap.base.service.TbkEntrySceneService;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;

/*
 * 维度
 */
@Controller
@RequestMapping(value = "/TbkEntrySceneController")
public class TbkEntrySceneController extends CommonController {
	@Autowired
	private TbkEntrySceneService tbkEntrySceneService;

	/*
	 * 查询场景信息
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageTbkEntrySceneInfo")
	public RetMsg<PageInfo<TbkEntrySceneInfo>> searchPageTbkEntrySceneInfo(@RequestBody Map<String, Object> allMap)
			throws RException {
		Page<TbkEntrySceneInfo> page = tbkEntrySceneService.getTbkEntryScenePage(allMap);
		return RetMsgHelper.ok(page);
	}
	

	/**
	 * 添加场景信息
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/createEntryScene")
	public RetMsg<Serializable> createEntryScene(@RequestBody Map<String, Object> allMap) {
		tbkEntrySceneService.save(allMap);
		return RetMsgHelper.ok();
	}
	/**
	 * 修改场景信息
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/editEntryScene")
	public RetMsg<Serializable> editEntryScene(@RequestBody Map<String, Object> allMap) {
		tbkEntrySceneService.save(allMap);
		return RetMsgHelper.ok();
	}

	/**
	 * 删除场景信息
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteEntryScene")
	public RetMsg<Serializable> deleteEntryScene(@RequestBody Map<String, Object> allMap) {
		tbkEntrySceneService.delTbkEntryScene(allMap);
		return RetMsgHelper.ok();
	}
}
