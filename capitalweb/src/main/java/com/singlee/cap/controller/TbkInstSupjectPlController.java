package com.singlee.cap.controller;

import java.io.Serializable;
import java.util.Map;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;

import com.singlee.cap.base.model.TbkInstSubjestPl;
import com.singlee.cap.base.service.TbkInstSubjestPlService;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;

@Controller
@RequestMapping(value = "/TbkInstSupjectPlController")
public class TbkInstSupjectPlController {
	
	@Autowired
    private TbkInstSubjestPlService tbkInstSubjestPlService;
	/**
	 * 
	 * 查询
	 * 
	 */
	@ResponseBody
	@RequestMapping(value = "/selectInstSubjestPlServicePage")
	public RetMsg<PageInfo<TbkInstSubjestPl>> selectInstSubjestPlServicePage(@RequestBody Map<String, Object> params) {
		Page<TbkInstSubjestPl> page = tbkInstSubjestPlService.selectTbkInstSubjestPlPage(params, ParameterUtil.getRowBounds(params));
		return RetMsgHelper.ok(page);
	}
	/**
	 * 删除
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteInstSubjestPl")
	public RetMsg<Serializable> deleteInstSubjestPl(@RequestBody Map<String,Object> map) {
		tbkInstSubjestPlService.deleteInstSubjestPlById(map);
		return RetMsgHelper.ok();
	}
	/**
	 * 添加
	 * @author 
	 * @date 
	 */
	//Serializable
	@ResponseBody
	@RequestMapping(value = "/createTbkInstSubjestPl")
	public RetMsg<Boolean> createTbkInstSubjestPl(@RequestBody  TbkInstSubjestPl tbkInstSubjestPl) throws Exception  {
		boolean boo = tbkInstSubjestPlService.addInstSubjectPl(tbkInstSubjestPl);
		return RetMsgHelper.ok(boo);
	}
	/**
	 * 修改
	 * @author 
	 * @date 
	 */
	@ResponseBody
	@RequestMapping(value = "/updateTbkInstSubjestPl")
	public RetMsg<Boolean> updateTbkInstSubjestPl(@RequestBody TbkInstSubjestPl tbkInstSubjestPl) {
	boolean boo=tbkInstSubjestPlService.updateTbkInstSubjestPlById(tbkInstSubjestPl);
      return RetMsgHelper.ok(boo);	
	}
}
