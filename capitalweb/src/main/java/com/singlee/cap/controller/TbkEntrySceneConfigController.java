package com.singlee.cap.controller;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.github.pagehelper.StringUtil;
import com.singlee.cap.base.model.TbkEntrySceneConfig;
import com.singlee.cap.base.service.TbkEntrySceneConfigService;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.ParentChildUtil;
import com.singlee.capital.system.controller.CommonController;

@Controller
@RequestMapping(value = "/TbkEntrySceneConfigController")
public class TbkEntrySceneConfigController extends CommonController {
	@Autowired
	private TbkEntrySceneConfigService tbkEntrySceneConfigService;

	/**
	 * 查询分录场景配置，有分页
	 * 
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchEntrySceneConfigPage")
	public RetMsg<PageInfo<TbkEntrySceneConfig>> searchEntrySceneConfigPage(@RequestBody Map<String, Object> params) {
		Page<TbkEntrySceneConfig> page = tbkEntrySceneConfigService.searchEntrySceneConfigPage(params,
				ParameterUtil.getRowBounds(params));
		return RetMsgHelper.ok(page);
	}

	/**
	 * 查询分录场景配置，无分页
	 * 
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchEntrySceneConfig")
	public RetMsg<PageInfo<TbkEntrySceneConfig>> searchEntrySceneConfig(@RequestBody Map<String, Object> params) {
		Page<TbkEntrySceneConfig> page = tbkEntrySceneConfigService.searchEntrySceneConfig(params);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 配置场景分录信息
	 * 
	 * @param tbkEntrySceneConfig
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/configEntryScene")
	public RetMsg<Serializable> configEntryScene(@RequestBody TbkEntrySceneConfig tbkEntrySceneConfig) {
		tbkEntrySceneConfigService.configEntryScene(tbkEntrySceneConfig);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 配置场景分录信息  直接插入 重复则忽略
	 * 
	 * @param tbkEntrySceneConfig
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/configEntryScene2")
	public RetMsg<Serializable> configEntryScene2(@RequestBody TbkEntrySceneConfig tbkEntrySceneConfig) {
		if(StringUtil.isNotEmpty(tbkEntrySceneConfig.getSettingGroupId())){
			String SettingGroupId = tbkEntrySceneConfig.getSettingGroupId();
			String[] groupid = SettingGroupId.split(",");
			for(int i = 0 ; i < groupid.length ; i++){
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("sceneId", tbkEntrySceneConfig.getSceneId());
				map.put("settingGroupId", groupid[i]);
				map.put("lastUpdateDate", DateUtil.getCurrentDateTimeAsString());
				Page<TbkEntrySceneConfig> page = tbkEntrySceneConfigService.searchEntrySceneConfig(map);
				if(page.size()>0){
					continue;
				}
				else{
					TbkEntrySceneConfig tes = new TbkEntrySceneConfig();
					ParentChildUtil.HashMapToClass(map, tes);
					tbkEntrySceneConfigService.createEntrySceneConfig(tes);
				}
				
			}
			
			
		}
		
		return RetMsgHelper.ok();
	}
	
	
	/**
	 * 删除场景分录信息
	 * 
	 * @param tbkEntrySceneConfig
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteEntrySceneByParam")
	public RetMsg<Serializable> deleteEntrySceneByParam(@RequestBody TbkEntrySceneConfig tbkEntrySceneConfig) {
		tbkEntrySceneConfigService.deleteEntrySceneConfigByParam(tbkEntrySceneConfig);
		return RetMsgHelper.ok();
	}
	
	
	
	/**
	 * 新场景分录配置
	 * 
	 * @param tbkEntrySceneConfig
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/configSceneEntry")
	public RetMsg<Serializable> configSceneEntry(@RequestBody Map<String, Object> map) {
		tbkEntrySceneConfigService.configSceneEntry(map);
		return RetMsgHelper.ok();
	}
}
