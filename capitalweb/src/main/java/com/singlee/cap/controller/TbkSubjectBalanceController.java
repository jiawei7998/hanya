package com.singlee.cap.controller;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.cap.bookkeeping.model.TbkSubjectBalance;
import com.singlee.cap.bookkeeping.service.TBkSubjectBalanceService;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.controller.CommonController;
/*
 * 科目表controller层
 */
@Controller
@RequestMapping(value = "/TbkSubjectBalanceController")
public class TbkSubjectBalanceController extends CommonController{
	/*
	 * 科目表service层
	 */
	@Autowired
	private TBkSubjectBalanceService tbkSubjectBalanceService;
	/*
	 * 查询科目信息
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageTbkSubjectBalance")
	public RetMsg<PageInfo<TbkSubjectBalance>> searchPageTbkSubjectBalance(@RequestBody Map<String,Object> allMap) throws RException {
		RowBounds rowBounds = ParameterUtil.getRowBounds(allMap);
		Page<TbkSubjectBalance> page = tbkSubjectBalanceService.selectByMap(allMap, rowBounds);
		return RetMsgHelper.ok(page);
	}

}
