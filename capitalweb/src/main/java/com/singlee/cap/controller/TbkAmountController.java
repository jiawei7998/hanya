package com.singlee.cap.controller;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.cap.base.model.TbkAmount;
import com.singlee.cap.base.service.TbkAmountService;
import com.singlee.cap.bookkeeping.service.BookkeepingService;
import com.singlee.cap.bookkeeping.service.Ifrs9BookkeepingService;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.controller.CommonController;

@Controller
@RequestMapping(value = "/TbkAmountController")
public class TbkAmountController extends CommonController {
	@Autowired
	private TbkAmountService tbkAmountService;
	@Autowired
	private Ifrs9BookkeepingService ifrs9BookkeepingService;
	/**
	 * 查询金额指针返回列表
	 * 
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchAmountPointerPage")
	public RetMsg<PageInfo<TbkAmount>> searchAmountPointerPage(@RequestBody Map<String, Object> params) {
		Page<TbkAmount> page = tbkAmountService.getTbkAmountList(params, ParameterUtil.getRowBounds(params));
		return RetMsgHelper.ok(page);
	}
	
	@ResponseBody
	@RequestMapping(value = "/queryByName")
	public RetMsg<Serializable> queryName(@RequestBody HashMap<String, Object> map) {
		tbkAmountService.queryName(ParameterUtil.getString(map, "amountName", ""));
		return RetMsgHelper.ok();
	}
	
	/**
	 * 添加金额指针代码
	 * 
	 * @param tbkAmount
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/createAmount")
	public RetMsg<Serializable> createAmount(@RequestBody TbkAmount tbkAmount) {
		tbkAmountService.createAmount(tbkAmount);
		return RetMsgHelper.ok();
	}

	/**
	 * 修改金额指针代码
	 * 
	 * @param tbkAmount
	 */
	@ResponseBody
	@RequestMapping(value = "/updateAmount")
	public RetMsg<Serializable> updateAmount(@RequestBody TbkAmount tbkAmount) {
		tbkAmountService.updateAmount(tbkAmount);
		return RetMsgHelper.ok();
	}

	/**
	 * 删除金额代码
	 * 
	 * @param amountId
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteAmount")
	public RetMsg<Serializable> deleteAmount(@RequestBody HashMap<String, Object> map) {
		tbkAmountService.deleteAmount(ParameterUtil.getString(map, "amountId", ""));
		return RetMsgHelper.ok();
	}
	
	@Autowired
	BookkeepingService bookkeepingService;
	@ResponseBody
	@RequestMapping(value = "/testTransfer")
	public RetMsg<Serializable> testTransfer(@RequestBody HashMap<String, Object> map) {
		ifrs9BookkeepingService.tranferIas9ToIfrs9AutoService();
		return RetMsgHelper.ok();
	}

}
