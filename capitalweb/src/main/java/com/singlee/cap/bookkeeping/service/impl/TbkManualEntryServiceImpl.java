package com.singlee.cap.bookkeeping.service.impl;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.cap.accounting.mapper.TbkYearBalanceMapper;
import com.singlee.cap.accounting.model.TacTask;
import com.singlee.cap.accounting.model.TbkYearBalance;
import com.singlee.cap.accounting.service.AccountingTaskService;
import com.singlee.cap.base.mapper.TbkEventMapper;
import com.singlee.cap.base.mapper.TbkInstSubjestPlMapper;
import com.singlee.cap.base.model.TbkEvent;
import com.singlee.cap.base.model.TbkInstSubjestPl;
import com.singlee.cap.base.model.TbkSubjectDef;
import com.singlee.cap.base.service.TbkSubjectDefService;
import com.singlee.cap.bookkeeping.mapper.TbkApproveMapper;
import com.singlee.cap.bookkeeping.mapper.TbkManualEntryMapper;
import com.singlee.cap.bookkeeping.model.TbkApprove;
import com.singlee.cap.bookkeeping.model.TbkEntry;
import com.singlee.cap.bookkeeping.model.TbkManualEntry;
import com.singlee.cap.bookkeeping.service.BookkeepingService;
import com.singlee.cap.bookkeeping.service.TbkEntryService;
import com.singlee.cap.bookkeeping.service.TbkManualEntryService;
import com.singlee.capital.acc.service.SlSessionHelperExt;
import com.singlee.capital.base.cal.LogManager;
import com.singlee.capital.base.cal.tools.PlaningTools;
import com.singlee.capital.base.mapper.Acctno407Mapper;
import com.singlee.capital.base.model.Acctno407;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.log.AutoLogMethod;
import com.singlee.capital.common.spring.mybatis.BatchDao;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.dict.service.DictionaryGetService;
import com.singlee.capital.fund.mapper.ProductApproveFundMapper;
import com.singlee.capital.fund.mapper.TdDurationFundCashdivMapper;
import com.singlee.capital.fund.mapper.TdDurationFundRedeemMapper;
import com.singlee.capital.fund.mapper.TdDurationFundReinvestMapper;
import com.singlee.capital.fund.mapper.TdDurationFundTransMapper;
import com.singlee.capital.fund.mapper.TdProductFundTposMapper;
import com.singlee.capital.fund.model.ProductApproveFund;
import com.singlee.capital.fund.model.TdDurationFundCashdiv;
import com.singlee.capital.fund.model.TdDurationFundRedeem;
import com.singlee.capital.fund.model.TdDurationFundReinvest;
import com.singlee.capital.fund.model.TdProductFundTpos;
import com.singlee.capital.interfacex.mapper.Ti2ndPaymentMapper;
import com.singlee.capital.interfacex.model.CommonRqHdr;
import com.singlee.capital.interfacex.model.CreditBeaRec;
import com.singlee.capital.interfacex.model.DebitBeaRec;
import com.singlee.capital.interfacex.model.TExReconAllInqRec;
import com.singlee.capital.interfacex.model.TExReconAllInqRq;
import com.singlee.capital.interfacex.model.TExReconAllInqRs;
import com.singlee.capital.interfacex.model.TExReconT24lz;
import com.singlee.capital.interfacex.model.TStBeaRepayAaaRq;
import com.singlee.capital.interfacex.model.TStBeaRepayAaaRs;
import com.singlee.capital.interfacex.service.SocketClientService;
import com.singlee.capital.interfacex.socketservice.InterfaceCode;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.mapper.TtInstitutionMapper;
import com.singlee.capital.system.model.TtInstitution;
import com.singlee.capital.system.service.InstitutionService;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.capital.tpos.mapper.TrdTposMapper;
import com.singlee.capital.tpos.model.TdTrdTpos;
import com.singlee.capital.tpos.service.TrdTposService;
import com.singlee.capital.trade.mapper.TdProductApproveMainMapper;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.slbpm.externalInterface.SlbpmCallBackInteface;
import com.singlee.slbpm.externalInterface.TaskEventInterface;

/**
 * 记账分录服务实现
 * @author shiting
 *
 */
@Service("tbkManualEntryService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TbkManualEntryServiceImpl implements TbkManualEntryService,SlbpmCallBackInteface,TaskEventInterface {

	@Autowired
	private TbkManualEntryMapper tbkManualEntryMapper;
	@Autowired
	private TbkEntryService tbkEntryService;
	@Autowired
	private TbkEventMapper tbkEventMapper;
	@Autowired
	private BatchDao batchDao;
	/** 业务日 */
	@Autowired
	private DayendDateService dayendDateService;
	@Autowired
	private AccountingTaskService accountingTaskService;
	@Autowired
	private TbkSubjectDefService tbkSubjectDefService;
	@Autowired
	private TrdTposService trdTposService;
	@Autowired
	private TrdTposMapper trdTposMapper;
	@Autowired
	private TbkApproveMapper tbkApproveMapper;
	@Autowired
	private DictionaryGetService dictionaryGetService;
	@Autowired
	InstitutionService institutionService;
	@Autowired
	BookkeepingService bookkeepingService;
	@Autowired
	TdProductApproveMainMapper tdProductApproveMainMapper;
	@Autowired
	ProductApproveFundMapper productApproveFundMapper;
	@Autowired
	TdDurationFundRedeemMapper tdDurationFundRedeemMapper;
	@Autowired
	TdDurationFundCashdivMapper tdDurationFundCashdivMapper;
	@Autowired
	TdDurationFundTransMapper tdDurationFundTransMapper;
	@Autowired
	TdDurationFundReinvestMapper tdDurationFundReinvestMapper;
	@Autowired 
	TdProductFundTposMapper tdProductFundTposMapper;
	@Autowired
	Acctno407Mapper acctno407Mapper;
	@Autowired
	TbkInstSubjestPlMapper tbkInstSubjestPlMapper;
	@Autowired
	TtInstitutionMapper ttInstitutionMapper;

	@Autowired
	private Ti2ndPaymentMapper ti2ndPaymentMapper;
	@Autowired
	private SocketClientService socketClientService;
	@Autowired
	TbkYearBalanceMapper tbkYearBalanceMapper;
	
	/**  获取新的流水号   */
	@Override
	public String getNewFlowNo() {
		return tbkManualEntryMapper.getNewFlowNo();
	}

	@Override 
	public void deleteManualEntry(Map<String, Object> params){
           tbkManualEntryMapper.deleteByFlowId(params);
           TbkApprove tbkApprove=new TbkApprove();
           tbkApprove.setDealNo(params.get("flowId").toString());
           tbkApproveMapper.delete(tbkApprove);
	}
	
	@Override
	public Page<TbkManualEntry> getEntryList(Map<String, Object> map,RowBounds rb) {
		if("year".equals(ParameterUtil.getString(map, "eventType", ""))){
			String postDate = dayendDateService.getDayendDate();
			map.put("postDate", postDate);
		}
		return tbkManualEntryMapper.selectByMap(map, rb);
	}
	
	@Override
	public void save(TbkManualEntry entry) {
		tbkManualEntryMapper.insert(entry);
	}

	@Override
	public List<TbkManualEntry> getDealEntryList(Map<String, Object> map) {
		return tbkManualEntryMapper.selectByMap(map);
	}

	@Override
	public void createYearEntry2(){
		String yearLastDay=DateUtil.format(DateUtil.getCurrYearLast());//每年最後一天
		String postDate = dayendDateService.getDayendDate();//賬務日期
		if(yearLastDay.replaceAll("-", "").equals(postDate.replaceAll("-", ""))){
			List<TbkManualEntry> entryList = tbkManualEntryMapper.selectYearEntry2tt(null);
			if(entryList != null && entryList.size() > 0){
				String eventId = "";
				TbkEvent event = new TbkEvent();
				event.setEventType("year");
				List<TbkEvent> eventList = tbkEventMapper.getTbkEventPage(BeanUtil.beanToMap(event));//会计事件
				if(eventList.size() > 0){
					eventId = eventList.get(0).getEventId();
				}else{
					throw new RException("未找到对应的会计事件代码"); 
				}
				List<TacTask>taskList = accountingTaskService.selectEntryTaskList(null);
				TacTask task = taskList.get(0);
				String flowId = tbkManualEntryMapper.getNewFlowNo();
				String bkkpgOrgId = "0";
				String ccy = "0";
				int i = 1;
				List<TbkManualEntry> list = new ArrayList<TbkManualEntry>();
				Map<String,Object> acctno = new HashMap<String, Object>();
				Map<String,Object> map = null;
				TbkYearBalance yearbal = null;
				TbkManualEntry entry = null;
	
				String spDealNo = "YEAREND"+postDate.replace("-", "");
				TbkApprove approve = new TbkApprove();
				approve.setDealNo(spDealNo);
				approve.setDealType(DictConstants.DealType.Verify);//审批单类型 1-审批 2-核实
				approve.setSponsor(SlSessionHelper.getUserId());//审批发起人
				approve.setSponInst(SlSessionHelper.getInstitutionId());//审批发起机构
				approve.setaDate(postDate);//审批开始日期
				approve.setVersion(DictConstants.YesNo.YES);//版本
				approve.setApproveStatus(DictConstants.ApproveStatus.New);//审批状态
				approve.setLstDate(DateUtil.getCurrentDateTimeAsString());
				tbkApproveMapper.insert(approve);
				
				for (TbkManualEntry tbkManualEntry : entryList) {
					if(tbkManualEntry.getInnerAcctSn() != null && !"".equals(tbkManualEntry.getInnerAcctSn())){
						map = new HashMap<String, Object>();;
						map.put("fromSubjCode", tbkManualEntry.getSubjCode());
						map.put("fromCcy", tbkManualEntry.getCcy());
						yearbal = tbkYearBalanceMapper.selectFromYearBalance(map);
						if(yearbal != null){
							entry = new TbkManualEntry();
							if("407004".equals(yearbal.getEndSubjCode())){
								acctno.put("subjCode", "407-004");
								acctno.put("ccy", tbkManualEntry.getCcy());
								acctno.put("instId", tbkManualEntry.getBkkpgOrgId());
								List<Acctno407> acctlist = acctno407Mapper.selectAcctno407(acctno);
								if(acctlist.size() > 0){
									entry.setInnerAcctSn(acctlist.get(0).getAcctNo());
									entry.setMediumType(acctlist.get(0).getMediumType());
								}else{
									throw new RException("科目："+yearbal.getEndSubjCode()+"未找到对应的账号");
								}
							}else{
								acctno.put("subjectCode", yearbal.getEndSubjCode());
								acctno.put("ccy", tbkManualEntry.getCcy());
								acctno.put("instId", tbkManualEntry.getBkkpgOrgId());
								List<TbkInstSubjestPl> acctList = tbkInstSubjestPlMapper.selectTbkInstSubjestPlPage(acctno);
								if(acctList.size() > 0){
									entry.setInnerAcctSn(acctList.get(0).getPlCode());
									entry.setMediumType(acctList.get(0).getMediumType());
								}else{
									throw new RException("科目："+yearbal.getEndSubjCode()+"未找到对应的账号");
								}
							}
		
							entry.setTaskId(task.getTaskId());
							entry.setState(DictConstants.ApproveStatus.New);
							entry.setEventId(eventId);
							entry.setSubjCode(yearbal.getEndSubjCode());
							entry.setSubjName(yearbal.getEndSubjName());
							entry.setPrdNo(tbkManualEntry.getPrdNo());
							entry.setCcy(tbkManualEntry.getCcy());
							entry.setDebitCreditFlag(yearbal.getEndDebitCredit());
							entry.setPostDate(postDate);
							entry.setSpDealNo(spDealNo);
							entry.setBkkpgOrgId(tbkManualEntry.getBkkpgOrgId());
							entry.setRedFlag(tbkManualEntry.getRedFlag());
							if(yearbal.getRedFlag().equals(DictConstants.YesNo.YES)){
								entry.setRedFlag(DictConstants.redFlag.RED);
								entry.setValue(-tbkManualEntry.getAmount());
								entry.setAmount(-tbkManualEntry.getAmount());
							}else{
								entry.setValue(tbkManualEntry.getAmount());
							}
							entry.setUpdateTime(DateUtil.getCurrentDateTimeAsString());
							entry.setSendFlag(DictConstants.EntrySendFlag.UnSend);
		
							entry.setFlowSeq(String.valueOf(i));
							tbkManualEntry.setFlowSeq(String.valueOf(i+1));
							
							if("0".equals(bkkpgOrgId) && "0".equals(ccy)){
								entry.setFlowSeq(String.valueOf(i));
								tbkManualEntry.setFlowSeq(String.valueOf(i+1));
								i = i+2;
							}else{
								if(bkkpgOrgId.equals(tbkManualEntry.getBkkpgOrgId())&&ccy.equals(tbkManualEntry.getCcy())){
									entry.setFlowSeq(String.valueOf(i));
									tbkManualEntry.setFlowSeq(String.valueOf(i+1));
									i = i+2;
								}else{
									i = 1;
									flowId = tbkManualEntryMapper.getNewFlowNo();
									entry.setFlowSeq(String.valueOf(i));
									tbkManualEntry.setFlowSeq(String.valueOf(i+1));
									i = i+2;
								}
							}
		
							entry.setFlowId("YD"+postDate.replace("-", "")+flowId);
							list.add(entry);
		
							tbkManualEntry.setFlowId("YD"+postDate.replace("-", "")+flowId);
							tbkManualEntry.setTaskId(task.getTaskId());
							tbkManualEntry.setState(DictConstants.ApproveStatus.New);
							tbkManualEntry.setEventId(eventId);
							tbkManualEntry.setPostDate(postDate);
							tbkManualEntry.setSpDealNo(spDealNo);
							tbkManualEntry.setPrdNo(tbkManualEntry.getPrdNo());
							tbkManualEntry.setBkkpgOrgId(tbkManualEntry.getBkkpgOrgId());
							tbkManualEntry.setRedFlag(tbkManualEntry.getRedFlag());
							if(yearbal.getRedFlag().equals(DictConstants.YesNo.YES)){
								tbkManualEntry.setRedFlag(DictConstants.redFlag.RED);
								tbkManualEntry.setValue(-tbkManualEntry.getAmount());
								tbkManualEntry.setAmount(-tbkManualEntry.getAmount());
							}else{
								tbkManualEntry.setValue(tbkManualEntry.getAmount());
							}
							tbkManualEntry.setUpdateTime(DateUtil.getCurrentDateTimeAsString());
							tbkManualEntry.setSendFlag(DictConstants.EntrySendFlag.UnSend);
							list.add(tbkManualEntry);
	
							bkkpgOrgId = tbkManualEntry.getBkkpgOrgId();
							ccy = tbkManualEntry.getCcy();
						}else{
							throw new RException("科目："+tbkManualEntry.getSubjCode()+"币种："+tbkManualEntry.getCcy()+"未找到需要结转的信息！");
						}
					}else{
						throw new RException("科目："+tbkManualEntry.getSubjCode()+"未找到对应的账号！");
					}
				}
				BookkeepingUtil.CheckManualEntryList(list);
				batchDao.batch("com.singlee.cap.bookkeeping.mapper.TbkManualEntryMapper.insert", list);
			}
		}
	}
	
	@Override
	public void createYearEntry(){
		List<TbkManualEntry> entryList = tbkManualEntryMapper.selectYearEntry();
		if(entryList != null && entryList.size() > 0){
			String eventId = "";
			TbkEvent event = new TbkEvent();
			event.setEventType("year");
			List<TbkEvent> eventList = tbkEventMapper.getTbkEventPage(BeanUtil.beanToMap(event));//会计事件
			if(eventList.size() > 0){
				eventId = eventList.get(0).getEventId();
			}else{
				throw new RException("未找到对应的会计事件代码"); 
			}
			List<TacTask>taskList = accountingTaskService.selectEntryTaskList(null);
			TacTask task = taskList.get(0);
			String bkkpgOrgSource = dictionaryGetService.getTaDictByCodeAndKey("bkkpgOrgSource", "01").getDict_value();
			String bkkpgOrgId = null;	
			String postDate = dayendDateService.getDayendDate();
			String flowId = "";
			int i = 1;
			List<TbkManualEntry> list = new ArrayList<TbkManualEntry>();
			Map<String,Object> acctno = new HashMap<String, Object>();
			for (TbkManualEntry tbkManualEntry : entryList) {
				if("1".equals(bkkpgOrgSource)){//机构
					TtInstitution inst = institutionService.getInstitutionById(tbkManualEntry.getSponInst());
					if(inst != null){
						bkkpgOrgId = inst.getbInstId();
					}
				}else{
					bkkpgOrgId = task.getBkkpgOrgId();
				}
				
				if(!flowId.equals(tbkManualEntry.getFlowId())){
					i = 1;
					flowId = tbkManualEntry.getFlowId();
				}else{
					i++;
				}
				acctno = new HashMap<String, Object>();
				if("407004".equals(tbkManualEntry.getSubjCode())){
					acctno.put("subjCode", "407-004");
					acctno.put("ccy", tbkManualEntry.getCcy());
					acctno.put("instId", tbkManualEntry.getBkkpgOrgId());
					List<Acctno407> acctlist = acctno407Mapper.selectAcctno407(acctno);
					if(acctlist.size() > 0){
						tbkManualEntry.setInnerAcctSn(acctlist.get(0).getAcctNo());
						tbkManualEntry.setMediumType(acctlist.get(0).getMediumType());
					}else{
						throw new RException("科目："+tbkManualEntry.getSubjCode()+"未找到对应的账号");
					}
				}else{
					acctno.put("subjectCode", tbkManualEntry.getSubjCode());
					acctno.put("ccy", tbkManualEntry.getCcy());
					acctno.put("instId", tbkManualEntry.getBkkpgOrgId());
					List<TbkInstSubjestPl> acctList = tbkInstSubjestPlMapper.selectTbkInstSubjestPlPage(acctno);
					if(acctList.size() > 0){
						tbkManualEntry.setInnerAcctSn(acctList.get(0).getPlCode());
						tbkManualEntry.setMediumType(acctList.get(0).getMediumType());
					}else{
						throw new RException("科目："+tbkManualEntry.getSubjCode()+"未找到对应的账号");
					}
				}
				tbkManualEntry.setFlowId("YD"+postDate.replace("-", "")+flowId);
				tbkManualEntry.setTaskId(task.getTaskId());
				tbkManualEntry.setState(DictConstants.ApproveStatus.New);
				tbkManualEntry.setFlowSeq(String.valueOf(i));
				tbkManualEntry.setEventId(eventId);
				tbkManualEntry.setPostDate(postDate);
				tbkManualEntry.setBkkpgOrgId(bkkpgOrgId);
				tbkManualEntry.setRedFlag(tbkManualEntry.getRedFlag());
				if(tbkManualEntry.getRedFlag().equals(DictConstants.YesNo.YES)){
					tbkManualEntry.setValue(-tbkManualEntry.getAmount());
					tbkManualEntry.setAmount(-tbkManualEntry.getAmount());
				}else{
					tbkManualEntry.setValue(tbkManualEntry.getAmount());
				}
				tbkManualEntry.setUpdateTime(DateUtil.getCurrentDateTimeAsString());
				tbkManualEntry.setSendFlag(DictConstants.EntrySendFlag.UnSend);
				list.add(tbkManualEntry);
			}
			BookkeepingUtil.CheckManualEntryList(list);
			batchDao.batch("com.singlee.cap.bookkeeping.mapper.TbkManualEntryMapper.insert", list);
		}
	}

	@Override
	public Object getBizObj(String flowType, String flowId, String serialNo) {
		return null;
	}

	/**
	 * 流程状态变更(用于审批流程函数回调)
	 * @param flow_id		流程id
	 * @param serial_no		序号
	 * @param status		最新审批状态
	 */
	@Override
	public void statusChange(String flowType, String yearEnd, String serialNo,
			String status, String flowCompleteType) {
		
		HashMap<String, Object> mapRow = new HashMap<String, Object>();
		mapRow.put("dealNo", serialNo);
		mapRow.put("is_locked", true);
		TbkApprove approve = tbkApproveMapper.getTbkApproveById(mapRow);
		// 保存原来的审批状态
//		String oldOrderStatus = approve.getApproveStatus();
		// 更新审批状态
		approve.setApproveStatus(status);
		
		HashMap<String, Object> tradeMap = new HashMap<String, Object>();
		tradeMap.put("spDealNo", serialNo);
		tradeMap.put("is_locked", true);
		List<TbkManualEntry> ManualentryList = null;
		List<TbkEntry> entryList = null;
		if(DictConstants.FlowType.YearEndFlow.equals(flowType)){
			tradeMap.put("eventType", "year");
		}else if(DictConstants.FlowType.ManualAdjFlow.equals(flowType)){
			tradeMap.put("eventType", "manual");
		}
		TbkEntry entry = null;
		ManualentryList = tbkManualEntryMapper.selectByMap(tradeMap);
		if(DictConstants.ApproveStatus.Approving.equals(status)){
			approve.setSponsor(SlSessionHelper.getUserId());
			if(DictConstants.FlowType.YearEndFlow.equals(flowType)){
				BookkeepingUtil.CheckManualEntryList(ManualentryList);
			}
		}
		if(DictConstants.ApproveStatus.ApprovedPass.equals(status)){
			entryList = new ArrayList<TbkEntry>();
			if(DictConstants.FlowType.ManualAdjFlow.equals(flowType)){
				/**
				 * 审批通过，把审批的分录生成到分录信息表中
				 */
				for (TbkManualEntry tbkYearEntry : ManualentryList) {
					entry = new TbkEntry();
					BeanUtil.copyNotEmptyProperties(entry, tbkYearEntry);
					entry.setState(DictConstants.EntryState.NORMAL);
					entry.setSendFlag(DictConstants.EntrySendFlag.UnSend);
					
					entryList.add(entry);
						
					//更新头寸
					String subjCode = entry.getSubjCode();
					String dealNo = tbkYearEntry.getDealNo();
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("subjCode", subjCode);
					List<TbkSubjectDef> list = tbkSubjectDefService.getTbkSubjectDefList(map);
					
					Map<String, Object> map1 = new HashMap<String, Object>();
					map1.put("dealNo", dealNo);
					TdTrdTpos ttts = trdTposService.getTdTrdTposByKey(map1);
					if(ttts != null){
						/**科目性质不为空*/
						if(list.get(0).getSubjNature() != null && !"".equals(list.get(0).getSubjNature())){
							if("1".equals(list.get(0).getSubjNature())){
								if(tbkYearEntry.getDebitCreditFlag().equals(DictConstants.DebitCreditFlag.Credit)){
									ttts.setSettlAmt(PlaningTools.sub(ttts.getSettlAmt(), entry.getValue()));
								}else if(tbkYearEntry.getDebitCreditFlag().equals(DictConstants.DebitCreditFlag.Debit)){
									ttts.setSettlAmt(PlaningTools.add(entry.getValue(), ttts.getSettlAmt()));
								}
								trdTposMapper.updateByPrimaryKey(ttts);
							}
							/**是否逾期   否*/
							if(tbkYearEntry.getOutDate().equals(DictConstants.YesNo.NO)){  
								if("5".equals(list.get(0).getSubjNature())|| "2".equals(list.get(0).getSubjNature())){
									if(tbkYearEntry.getDebitCreditFlag().equals(DictConstants.DebitCreditFlag.Credit)){
										ttts.setAccruedTint(PlaningTools.sub(ttts.getAccruedTint(),entry.getValue()));
									}else if(tbkYearEntry.getDebitCreditFlag().equals(DictConstants.DebitCreditFlag.Debit)){
										ttts.setAccruedTint(PlaningTools.add(entry.getValue(), ttts.getAccruedTint()));
									}
									trdTposMapper.updateByPrimaryKey(ttts);
								}else if("3".equals(list.get(0).getSubjNature())){
									if(tbkYearEntry.getDebitCreditFlag().equals(DictConstants.DebitCreditFlag.Credit)){
										ttts.setActualTint(PlaningTools.sub(ttts.getActualTint(),entry.getValue() ));
									}else if(tbkYearEntry.getDebitCreditFlag().equals(DictConstants.DebitCreditFlag.Debit)){
										ttts.setActualTint(PlaningTools.add(entry.getValue(), ttts.getActualTint()));
									}
									trdTposMapper.updateByPrimaryKey(ttts);
								}
							}
							/**是否逾期   是*/
							if(tbkYearEntry.getOutDate().equals(DictConstants.YesNo.YES)){   
								if("5".equals(list.get(0).getSubjNature())|| "2".equals(list.get(0).getSubjNature())){
									if(tbkYearEntry.getDebitCreditFlag().equals(DictConstants.DebitCreditFlag.Credit)){
										ttts.setOverdueAccruedTint(PlaningTools.sub(ttts.getOverdueAccruedTint(),entry.getValue() ));
									}else if(tbkYearEntry.getDebitCreditFlag().equals(DictConstants.DebitCreditFlag.Debit)){
										ttts.setOverdueAccruedTint(PlaningTools.add(entry.getValue(), ttts.getOverdueAccruedTint()));
									}
									trdTposMapper.updateByPrimaryKey(ttts);
								}else if("3".equals(list.get(0).getSubjNature())){
									if(tbkYearEntry.getDebitCreditFlag().equals(DictConstants.DebitCreditFlag.Credit)){
										ttts.setOverdueActualTint(PlaningTools.sub(ttts.getActualTint(),entry.getValue() ));
									}else if(tbkYearEntry.getDebitCreditFlag().equals(DictConstants.DebitCreditFlag.Debit)){
										ttts.setOverdueActualTint(PlaningTools.add(entry.getValue(), ttts.getActualTint()));
									}
									trdTposMapper.updateByPrimaryKey(ttts);
								}
							}
						}
					}else{
						String cno = null;
						HashMap<String, Object> map2 = new HashMap<String, Object>();
						map2.put("dealNo", entry.getDealNo());
						ProductApproveFund fund = productApproveFundMapper.getProductApproveFund(map2);
						if(fund != null){
							cno = fund.getcNo();
						}
						//基金分红
						TdDurationFundCashdiv tdDurationFundCashdiv = new TdDurationFundCashdiv();
						tdDurationFundCashdiv.setDealNo(ParameterUtil.getString(map2, "dealNo", ""));
						tdDurationFundCashdiv = tdDurationFundCashdivMapper.getTdDurationFundCashdivByDealNo(tdDurationFundCashdiv);
						if(tdDurationFundCashdiv != null){
							cno = tdDurationFundCashdiv.getcNo();
						}
						//基金红利再投
						TdDurationFundReinvest tdDurationFundReinvest = new TdDurationFundReinvest();
						tdDurationFundReinvest.setDealNo(ParameterUtil.getString(map2, "dealNo", ""));
						tdDurationFundReinvest = tdDurationFundReinvestMapper.getTdDurationFundReinvestByDealNo(tdDurationFundReinvest);
						if(tdDurationFundReinvest != null){
							cno = tdDurationFundReinvest.getcNo();
						}
						
						//基金赎回
						TdDurationFundRedeem redeem = tdDurationFundRedeemMapper.getTdDurationFundRedeemByDealNo(map2);
						if(redeem != null){
							cno = redeem.getcNo();
						}
						if(cno != null){
							HashMap<String, Object> fundMap = new HashMap<String, Object>();
							fundMap.put("fundCode", entry.getFundCode());
							fundMap.put("ccy", entry.getCcy());
							fundMap.put("prdNo", entry.getPrdNo());
							fundMap.put("cNo", cno);
							TdProductFundTpos fundTpos = tdProductFundTposMapper.getFundTpos(fundMap);
							if(fundTpos != null){
								/**科目性质不为空*/
								if(list.get(0).getSubjNature() != null && !"".equals(list.get(0).getSubjNature())){
									if("1".equals(list.get(0).getSubjNature())){
										if(tbkYearEntry.getDebitCreditFlag().equals(DictConstants.DebitCreditFlag.Credit)){
											fundTpos.setUtAmt(new BigDecimal(PlaningTools.sub(fundTpos.getUtAmt().doubleValue(), entry.getValue())));
										}else if(tbkYearEntry.getDebitCreditFlag().equals(DictConstants.DebitCreditFlag.Debit)){
											fundTpos.setUtAmt(new BigDecimal(PlaningTools.add(entry.getValue(), fundTpos.getUtAmt().doubleValue())));
										}
									}else if("2".equals(list.get(0).getSubjNature())){
										
									}
									tdProductFundTposMapper.updateTdProductFundTpos(fundTpos);
								}
							}
						}
					}
				}
				tbkEntryService.save(entryList);
				bookkeepingService.updateBookkeepingBalance(entryList);
			}else if(DictConstants.FlowType.YearEndFlow.equals(flowType)){
				TbkManualEntry tbkYearEntry = null;
				for (int i = 0; i < ManualentryList.size(); i++) {
					tbkYearEntry = ManualentryList.get(i);
					if(tbkYearEntry.getValue() != 0){
						entry = new TbkEntry();
						BeanUtil.copyNotEmptyProperties(entry, tbkYearEntry);
						entry.setState(DictConstants.EntryState.NORMAL);
						entry.setSendFlag(DictConstants.EntrySendFlag.UnSend);
						if(tbkYearEntry.getRedFlag().equals(DictConstants.YesNo.YES)){
							entry.setRedFlag(DictConstants.redFlag.RED);
						}
						entryList.add(entry);
					}
				}
				tbkEntryService.save(entryList);
				bookkeepingService.updateBookkeepingBalance(entryList);
//				sendYearendInterfaceToT24(tradeMap);
			}
		}
//		tbkApproveMapper.updateByPrimaryKey(approve);
		tbkApproveMapper.updateTbkApproveStatus2(approve);
//		HashMap<String, Object> entryMap = new HashMap<String, Object>();
//		entryMap.put("flowId", serialNo);
//		tradeMap.put("state", status);
//		tbkManualEntryMapper.updateEntryByFlowid(entryMap);
	}

	@Override
	public void update(TbkManualEntry entry) {
		tbkManualEntryMapper.updateEntryAmt(BeanUtil.beanToMap(entry));
	}
	
	@Override
	public void comfirYearendEntry(Map<String, Object> params){
//		HashMap<String, Object> tradeMap = new HashMap<String, Object>();
//		List<TbkManualEntry> ManualentryList = null;
//		List<TbkEntry> entryList = null;
//		String flowId = "";
//		if(params != null){
//			flowId = ParameterUtil.getString(params, "flowId", "");
//		}
//		if(flowId != null && !flowId.equals("")){
//			tradeMap.put("flowId", flowId);
//		}else{
//			flowId = tbkManualEntryMapper.selectYearendEntryFlowId();
//		}
//		tradeMap.put("eventType", "year");
//		TbkEntry entry = null;
//		ManualentryList = tbkManualEntryMapper.selectByMap(tradeMap);
//		TbkManualEntry tbkYearEntry = null;
//		if(ManualentryList.size() > 0){
//			entryList = new ArrayList<TbkEntry>();
//			for (int i = 0; i < ManualentryList.size(); i++) {
//				tbkYearEntry = ManualentryList.get(i);
//				entry = new TbkEntry();
//				BeanUtil.copyNotEmptyProperties(entry, tbkYearEntry);
//				entry.setState(DictConstants.EntryState.NORMAL);
//				entry.setSendFlag(DictConstants.EntrySendFlag.UnSend);
//				if(tbkYearEntry.getRedFlag().equals(DictConstants.YesNo.YES)){
//					entry.setRedFlag(DictConstants.redFlag.RED);
//				}
//				entryList.add(entry);
//			}
//			tbkEntryService.save(entryList);
//			bookkeepingService.updateBookkeepingBalance(entryList);
//		}
	}

	/**
	 * 保存手工记账分录
	 * @param date		日期
	 * @param taskId	任务ID
	 * @param bookkeepingOrgId  记账机构
	 * @param entryList		分录列表
	 * @return
	 */
	@Override
	@AutoLogMethod("保存手工记账分录")
	public void saveManualEntry(Map<String,Object> params, @Param("entryList")List<TbkManualEntry> entryList){
		String flowNo = "";
		String flowId = params.get("flowId").toString();
		params.put("eventType", "manual");
		if(flowId!=null && (!("".equals(flowId)))){
			flowNo = params.get("flowId").toString();
			TbkApprove approve = new TbkApprove();
			approve.setDealNo(flowNo);
			tbkApproveMapper.deleteByPrimaryKey(approve);
			Map<String, Object> maps = new HashMap<String, Object>();
			maps.put("flowId", flowNo);
			tbkManualEntryMapper.deleteByFlowId(maps);              //明天测试一下 
		}else{
			flowNo = getNewFlowNo();
		}
		
		String eventId = "";
		TbkEvent event = new TbkEvent();
		event.setEventType(params.get("eventType").toString());
		List<TbkEvent> eventList = tbkEventMapper.getTbkEventPage(BeanUtil.beanToMap(event));//会计事件
		if(eventList.size() > 0){
			eventId = eventList.get(0).getEventId();
		}else{
			throw new RException("未找到对应的会计事件代码"); 
		}
		String postDate = dayendDateService.getDayendDate();
		
		TbkApprove approve = new TbkApprove();
		approve.setDealNo(flowNo);
		approve.setDealType(DictConstants.DealType.Verify);//审批单类型 1-审批 2-核实
		approve.setSponsor(SlSessionHelper.getUserId());//审批发起人
		approve.setSponInst(SlSessionHelper.getInstitutionId());//审批发起机构
		approve.setaDate(postDate);//审批开始日期
		approve.setVersion(DictConstants.YesNo.YES);//版本
		approve.setApproveStatus(DictConstants.ApproveStatus.New);//审批状态
		approve.setLstDate(DateUtil.getCurrentDateTimeAsString());
		tbkApproveMapper.insert(approve);
		
		String prdNo = null;
		String fundCode = null;
		String sponInst = null;
		TdProductApproveMain main = tdProductApproveMainMapper.getProductApproveMain(params);
		if(main != null){
			prdNo = main.getPrdNo().toString();
			sponInst = main.getSponInst();
		}else{
			//基金申购
			ProductApproveFund fund = productApproveFundMapper.getProductApproveFund(params);
			if(fund != null){
				prdNo = fund.getPrdNo().toString();
				fundCode = fund.getFundCode();
				sponInst = fund.getSponInst();
			}
			//基金分红
			TdDurationFundCashdiv tdDurationFundCashdiv = new TdDurationFundCashdiv();
			tdDurationFundCashdiv.setDealNo(ParameterUtil.getString(params, "dealNo", ""));
			tdDurationFundCashdiv = tdDurationFundCashdivMapper.getTdDurationFundCashdivByDealNo(tdDurationFundCashdiv);
			if(tdDurationFundCashdiv != null){
				prdNo = tdDurationFundCashdiv.getPrdNo().toString();
				fundCode = tdDurationFundCashdiv.getFundCode();
				sponInst = tdDurationFundCashdiv.getSponInst();
			}
			//基金红利再投
			TdDurationFundReinvest tdDurationFundReinvest = new TdDurationFundReinvest();
			tdDurationFundReinvest.setDealNo(ParameterUtil.getString(params, "dealNo", ""));
			tdDurationFundReinvest = tdDurationFundReinvestMapper.getTdDurationFundReinvestByDealNo(tdDurationFundReinvest);
			if(tdDurationFundReinvest != null){
				prdNo = tdDurationFundReinvest.getPrdNo().toString();
				fundCode = tdDurationFundReinvest.getFundCode();
				sponInst = tdDurationFundReinvest.getSponInst();
			}
			
			//基金赎回
			TdDurationFundRedeem redeem = tdDurationFundRedeemMapper.getTdDurationFundRedeemByDealNo(params);
			if(redeem != null){
				prdNo = redeem.getPrdNo().toString();
				fundCode = redeem.getFundCode();
				sponInst = redeem.getSponInst();
			}
		}
	    String sponInstId=	SlSessionHelper.getInstitutionId();
		if(prdNo != null && !"".equals(prdNo)){
			if(!"admin".equals(SlSessionHelperExt.getUserId())){
			    if(!sponInst.equals(sponInstId)){
			    	throw new RException("流水号所属机构与当前登录机构不相同");
			    }
			}
		    if(!sponInst.equals(params.get("sponInst").toString())){
		    	throw new RException("所填机构与流水号所属机构不相同");
		    }
			List<TacTask>taskList = accountingTaskService.selectEntryTaskList(null);
			TacTask task = taskList.get(0);
			
			String bkkpgOrgSource = dictionaryGetService.getTaDictByCodeAndKey("bkkpgOrgSource", "01").getDict_value();
			String bkkpgOrgId = null;
			if("1".equals(bkkpgOrgSource)){//机构
				TtInstitution inst = institutionService.getInstitutionById(ParameterUtil.getString(params,"sponInst",""));
				if(inst != null){
					bkkpgOrgId = inst.getbInstId();
				}
			}else{
				bkkpgOrgId = task.getBkkpgOrgId();
			}
			
			//将已有的手工记账分录修改掉
			for(int i=0;i<entryList.size();i++){
				TbkManualEntry entry = entryList.get(i);
				entry.setFlowId(flowNo);
				entry.setFlowSeq(String.valueOf(i+1));
				entry.setTaskId(task.getTaskId());
				entry.setEventId(eventId);
				entry.setPrdNo(prdNo);
				entry.setFundCode(fundCode);
				entry.setSpDealNo(flowNo);
				entry.setCcy(ParameterUtil.getString(params,"ccy",""));
				entry.setBkkpgOrgId(bkkpgOrgId);
				entry.setPostDate(postDate);
				entry.setState(DictConstants.ApproveStatus.New);
				entry.setUpdateTime(DateUtil.getCurrentDateTimeAsString());
				entry.setOutDate(params.get("outDate").toString());
				entry.setDealNo(params.get("dealNo").toString());
				entry.setSponInst(params.get("sponInst").toString());
			}
			BookkeepingUtil.CheckManualEntryList(entryList);
			batchDao.batch("com.singlee.cap.bookkeeping.mapper.TbkManualEntryMapper.insert", entryList);
		}else{
			throw new RException("通过流水号："+params.get("dealNo").toString()+"未找到对应的交易信息");
		}
	}

	@Override
	public Page<TbkManualEntry> getManualEntryFinishList(
			Map<String, Object> params, RowBounds rb) {
		return tbkManualEntryMapper.getManualEntryFinishList(params, rb);
	}

	@Override
	public Page<TbkManualEntry> getManualEntryList(Map<String, Object> params,
			RowBounds rb) {
		return tbkManualEntryMapper.getManualEntryList(params, rb);
	}
	
	@Override
	public Page<TbkManualEntry> getManualEntryListHand(
			Map<String, Object> params, RowBounds rb) {
		
		HashMap<String, Object> map=new HashMap<String, Object>();
		map.put("instId", SlSessionHelper.getInstitutionId());
		List<TtInstitution> list = ttInstitutionMapper.searchChildrenInst(map);
		List<String> instIds = new ArrayList<String>();
		for(TtInstitution itlist : list){
			instIds.add(itlist.getInstId());
		}
		params.put("instIds", instIds);

		return tbkManualEntryMapper.getManualEntryList(params, rb);
	}

	@Override
	public Page<TbkManualEntry> getManualEntryListMine(
			Map<String, Object> params, RowBounds rb) {
		
		return tbkManualEntryMapper.getManualEntryListMine(params, rb);
	}

	@Override
	public Map<String, Object> fireEvent(String flowId, String serialNo,
			String taskId, String taskDefKey) throws Exception {
		return null;
	}
	

	@Override
	public TbkManualEntry selectManualEntryVo(Map<String, Object> params) {
		TbkManualEntry acct = new TbkManualEntry();
		List<TbkManualEntry> list = getManualEntryListMine(params, ParameterUtil.getRowBounds(params)).getResult();
		if (list.size() > 0) {
			acct = list.get(0);
			List<TbkManualEntry> volist = new ArrayList<TbkManualEntry>();
			TbkManualEntry vo = null;
			for (TbkManualEntry setacctDef : list) {
				HashMap<String, Object> map = BeanUtil.beanToHashMap(setacctDef);
				vo = new TbkManualEntry();
				BeanUtil.populate(vo, map);
				volist.add(vo);
			}
			acct.setConstact_list(volist);
		}
		return acct;
	}
	@Override
	public String selectYearendEntryFlowId(Map<String, Object> params){
		String flowId = "";
		flowId = tbkManualEntryMapper.selectYearendEntryFlowId(params);
		return flowId;
	}
	
	@Override
	public void sendYearendInterfaceToT24(String flowId) {
		List<TbkManualEntry> yearList = null;
		Map<String,Object> mapT24lz = null;
		TbkManualEntry entry = null;
		try{
			mapT24lz = new HashMap<String, Object>();
			mapT24lz.put("feeBaseLogid", flowId);
			TExReconT24lz texReconT24lz	= ti2ndPaymentMapper.searchTExReconT24lz(mapT24lz);
			boolean flag = true;//判断是否发送成功
			if(texReconT24lz == null){
				flag = false;//未发送
			}
			if(texReconT24lz != null && !"000000".equals(texReconT24lz.getRETCODE())){
				TExReconAllInqRq texReconAllInqRq = new TExReconAllInqRq();
				CommonRqHdr hdr = new CommonRqHdr();
				hdr.setRqUID(UUID.randomUUID().toString());
				hdr.setChannelId(InterfaceCode.TI_CHANNELNO);
				hdr.setTranDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
				hdr.setTranTime(new SimpleDateFormat("HHmmss").format(new Date()));
				texReconAllInqRq.setCommonRqHdr(hdr);
				texReconAllInqRq.setChannelId(InterfaceCode.TI_CHANNELNO);
				texReconAllInqRq.setFBID(InterfaceCode.TI_FBID);
				texReconAllInqRq.setTxnType(InterfaceCode.TI_IFBM0002);
				texReconAllInqRq.setMode("I");
				texReconAllInqRq.setTradeDt(new SimpleDateFormat("yyyyMMdd").format(new Date()));
				texReconAllInqRq.setTransId(texReconT24lz.getSrquid());
				TExReconAllInqRs  texReconAllInqRs = socketClientService.t24TexReconAllInqRequest(texReconAllInqRq);
				if(texReconAllInqRs.getCommonRsHdr().getStatusCode().equals(InterfaceCode.TI_SUCCESS)|| 
						"10510206".equals(texReconAllInqRs.getCommonRsHdr().getStatusCode())){
					List<TExReconAllInqRec> texReconAllInqRecList = texReconAllInqRs.getTExReconAllInqRec();
					//判断是否已经发送
					if(null != texReconAllInqRecList && texReconAllInqRecList.size()>0){
						for (int i = 0; i < texReconAllInqRecList.size(); i++) {
							//检查发送状态是否已经成功，如果不成功，则重新进行发送
							TExReconAllInqRec texReconAllInqRec = texReconAllInqRecList.get(i);
							if(texReconAllInqRec.getTxnStatus() == null || !"0000".equals(texReconAllInqRec.getTxnStatus())){
								flag = false;//发送成功，查询对账未处理成功，需重新发送
							}
							if("0000".equals(texReconAllInqRec.getTxnStatus())){
								entry = new TbkManualEntry();
								entry.setFlowId(texReconAllInqRec.getFeeBaseLogId());
								entry.setSendFlag(DictConstants.EntrySendFlag.SendSuccess);
								entry.setBeaNo(texReconAllInqRec.getTransRef());
								entry.setRetCode(texReconAllInqRec.getTxnStatus());
								tbkManualEntryMapper.updateEntrySendFlag(BeanUtil.beanToMap(entry));
							}
						}
					}
				}else{
					LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("T24对账接口异常，请稍后重试！");
				}
			}
		
			//未发送，组装多借多贷报文进行发送
			if(!flag){
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("flowId", flowId);
				yearList = tbkManualEntryMapper.selectByMap(map);
				if(yearList.size() > 0){
					TStBeaRepayAaaRq request = new TStBeaRepayAaaRq();
					request.setCurrencyDr(yearList.get(0).getCcy());
					request.setCurrencyCr(yearList.get(0).getCcy());
					List<CreditBeaRec> crList = new ArrayList<CreditBeaRec>();
					List<DebitBeaRec> drList = new ArrayList<DebitBeaRec>();
					CreditBeaRec recc1;
					DebitBeaRec rec1;
					for (int i = 0; i < yearList.size(); i++) {
						entry = yearList.get(i);
						if(entry.getDebitCreditFlag().equals(DictConstants.DebitCreditFlag.Debit)){
							rec1 = new DebitBeaRec();
							rec1.setDebitMedType(entry.getMediumType());
							BigDecimal bd = new BigDecimal(entry.getAmount()+"").abs();
							rec1.setAmtDr(String.valueOf(bd));
							rec1.setAcctNoDr(entry.getInnerAcctSn());
							drList.add(rec1);
						}else if(entry.getDebitCreditFlag().equals(DictConstants.DebitCreditFlag.Credit)){
							recc1 = new CreditBeaRec();
							recc1.setCreditMedType(entry.getMediumType());
							BigDecimal bd = new BigDecimal(entry.getAmount()+"").abs();
							recc1.setAmtCr(String.valueOf(bd));
							recc1.setAcctNoCr(entry.getInnerAcctSn());
							crList.add(recc1);
						}
					}
					request.setFbNo(InterfaceCode.TI_FBID);
					request.setCreditBeaRec(crList);
					request.setDebitBeaRec(drList);
					request.setTxnType(InterfaceCode.TI_IFBM0002);
					request.setFeeBaseLogId(flowId);
					CommonRqHdr hdr2 = new CommonRqHdr();
					hdr2.setRqUID(UUID.randomUUID().toString());
					hdr2.setChannelId(InterfaceCode.TI_CHANNELNO);
					hdr2.setTranDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
					hdr2.setTranTime(new SimpleDateFormat("HHmmss").format(new Date()));
					hdr2.setCompanyCode(yearList.get(0).getBkkpgOrgId().trim());
					request.setCommonRqHdr(hdr2);
					TStBeaRepayAaaRs response = new TStBeaRepayAaaRs();
					try {
						response = socketClientService.t24TStBeaRepayAaaRequest(request);
						if("000000".equals(response.getCommonRsHdr().getStatusCode()) || "105114".equals(response.getCommonRsHdr().getStatusCode())){
							entry = new TbkManualEntry();
							entry.setFlowId(flowId);
							entry.setSendFlag(DictConstants.EntrySendFlag.SendSuccess);
							entry.setRetCode(response.getCommonRsHdr().getStatusCode());
							entry.setRetMsg(response.getCommonRsHdr().getServerStatusCode());
							entry.setBeaNo(response.getSPRsUID());
							tbkManualEntryMapper.updateEntrySendFlag(BeanUtil.beanToMap(entry));
							LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("年终决算调用多借多贷接口处理成功");
						}else {
							entry = new TbkManualEntry();
							entry.setFlowId(flowId);
							entry.setSendFlag(DictConstants.EntrySendFlag.SendError);
							entry.setRetCode(response.getCommonRsHdr().getStatusCode());
							entry.setRetMsg(response.getCommonRsHdr().getServerStatusCode());
							entry.setBeaNo("");
							tbkManualEntryMapper.updateEntrySendFlag(BeanUtil.beanToMap(entry));
							LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("1、年终决算调用多借多贷接口处理失败,错误信息："+response.getCommonRsHdr().getServerStatusCode());
						}
					} catch (Exception e) {
						entry = new TbkManualEntry();
						entry.setFlowId(flowId);
						entry.setSendFlag(DictConstants.EntrySendFlag.SendError);
						entry.setRetCode(response.getCommonRsHdr().getStatusCode());
						entry.setRetMsg(response.getCommonRsHdr().getServerStatusCode());
						entry.setBeaNo("");
						tbkManualEntryMapper.updateEntrySendFlag(BeanUtil.beanToMap(entry));
						LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("2、年终决算调用多借多贷接口处理失败,错误信息："+response.getCommonRsHdr().getServerStatusCode());
					}
				}
			}
		}catch (Exception e) {
			entry = new TbkManualEntry();
			entry.setFlowId(flowId);
			entry.setSendFlag(DictConstants.EntrySendFlag.SendError);
			tbkManualEntryMapper.updateEntrySendFlag(BeanUtil.beanToMap(entry));
			LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("3、年终决算调用多借多贷接口处理失败,错误信息："+e.getMessage());
		}
	}

    @Override
    public void LimitOccupy(String serial_no, String prd_no) throws Exception {
        // TODO Auto-generated method stub
        
    }

}
