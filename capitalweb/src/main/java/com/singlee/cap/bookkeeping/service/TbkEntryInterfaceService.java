package com.singlee.cap.bookkeeping.service;

import java.util.Map;

import com.singlee.capital.common.pojo.RetMsg;

public interface TbkEntryInterfaceService {
		
	public RetMsg<Object> GtpTbkEntryOutputDataWrite(Map<String, Object> map) ;
}
