package com.singlee.cap.bookkeeping.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.cap.bookkeeping.mapper.TbkSubjectBalanceMapper;
import com.singlee.cap.bookkeeping.model.TbkEntry;
import com.singlee.cap.bookkeeping.model.TbkSubjectBalance;
import com.singlee.cap.bookkeeping.service.TBkSubjectBalanceService;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.DateUtil;

/**
 * 记账余额的服务类
 * @author Libo
 *
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TBkSubjectBalanceServiceImpl implements TBkSubjectBalanceService {

	@Autowired
	private TbkSubjectBalanceMapper bookkeepingBalanceMapper;
//	@Autowired
//	private AccountingTaskService accountingTaskService;
//	
//	@Override
//	public void setCurrentDate(String taskId, String date) {
//		int days = DateUtil.daysBetween(accountingTaskService.getTask(taskId).getTaskCurrentDate(), date);
//		CurrentHistoryUtil.setCurrentDate(bookkeepingBalanceMapper, days, taskId, date, false);
//		//修改期初数据
//		bookkeepingBalanceMapper.updateBeginValue(new HashMap<String,Object>());
//	}
	
	/**
	 * 根据分录查找相应的余额，如查询不到，则创建新的并返回
	 * @param taskId
	 * @param date
	 * @param entry
	 */
	@Override
	public TbkSubjectBalance findOrCreateByEntry(TbkEntry entry){
		TbkSubjectBalance balance = new TbkSubjectBalance();
		balance.setTaskId(entry.getTaskId());
		balance.setBeginDate(entry.getPostDate());
		balance.setSubjCode(entry.getSubjCode());
		balance.setAcctNo(entry.getCoreAcctCode());
		balance.setInnerSn(entry.getInnerAcctSn());
		balance.setOrgNo(entry.getBkkpgOrgId());
		balance.setCcy(entry.getCcy());
		balance.setBkkpgOrgId(entry.getBkkpgOrgId());
		List<TbkSubjectBalance> balanceList = bookkeepingBalanceMapper.selectByMap(BeanUtil.beanToHashMap(balance),new RowBounds());
		if(balanceList != null && balanceList.size()>0){
			return balanceList.get(0);
		}
		
		balance.setEndDate(entry.getPostDate());
		balance.setUpdateTime(DateUtil.getCurrentDateTimeAsString());
		balance.setSubjName(entry.getSubjName());
		bookkeepingBalanceMapper.insert(balance);
		
		return balance;
		
	}
	
	/**
	 * 更新余额，只更新四个数字余额（借、贷、支、付）
	 * @param balance
	 */
	@Override
	public void updateBalance(TbkSubjectBalance balance){
		bookkeepingBalanceMapper.updateByPrimaryKey(balance);
	}

	@Override
	public Page<TbkSubjectBalance> selectByMap(Map<String, Object> map,
			RowBounds rb) {
		return bookkeepingBalanceMapper.selectByMap(map, rb);
	}
	
	@Override
	public List<TbkSubjectBalance> selectByMap(Map<String, Object> map) {
		return bookkeepingBalanceMapper.selectByMap(map);
	}

	@Override
	public void insert(TbkSubjectBalance balance) {
		bookkeepingBalanceMapper.insert(balance);
	}
}
