package com.singlee.cap.bookkeeping.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.cap.accounting.model.InstructionPackage;
import com.singlee.cap.accounting.model.TacTask;
import com.singlee.cap.accounting.service.AccountingTaskService;
import com.singlee.cap.base.mapper.TbkEntrySceneAcupMapper;
import com.singlee.cap.base.mapper.TbkEntrySceneConfigMapper;
import com.singlee.cap.base.mapper.TbkEntrySceneDetailMapper;
import com.singlee.cap.base.mapper.TbkEventMapper;
import com.singlee.cap.base.mapper.TbkProductEventMapper;
import com.singlee.cap.base.mapper.TbkProductParamsMapper;
import com.singlee.cap.base.mapper.TbkProductSubjectMapper;
import com.singlee.cap.base.model.TbkAmount;
import com.singlee.cap.base.model.TbkEntrySceneAcup;
import com.singlee.cap.base.model.TbkEntrySceneConfig;
import com.singlee.cap.base.model.TbkEntrySceneDetail;
import com.singlee.cap.base.model.TbkEntrySceneInfo;
import com.singlee.cap.base.model.TbkEntrySetting;
import com.singlee.cap.base.model.TbkEvent;
import com.singlee.cap.base.model.TbkProductParams;
import com.singlee.cap.base.service.TbkAmountService;
import com.singlee.cap.base.service.TbkEntrySceneService;
import com.singlee.cap.base.service.TbkEntrySettingService;
import com.singlee.cap.base.service.TbkEventService;
import com.singlee.cap.base.service.TbkProductEventService;
import com.singlee.cap.base.service.impl.TbkEntrySettingServiceImpl;
import com.singlee.cap.bookkeeping.model.TbkEntry;
import com.singlee.cap.bookkeeping.service.Ifrs9BookkeepingService;
import com.singlee.cap.bookkeeping.service.PretreatmentService;
import com.singlee.cap.bookkeeping.service.TbkEntryService;
import com.singlee.capital.base.model.TcProduct;
import com.singlee.capital.base.service.ProductService;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.MathUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.dict.service.DictionaryGetService;
import com.singlee.capital.eu.util.CollectionUtils;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.model.TtInstitution;
import com.singlee.capital.system.service.InstitutionService;
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class Ifrs9BookkeepingServiceImpl implements Ifrs9BookkeepingService {
	@Autowired
	private TbkProductParamsMapper tbkProductParamsMapper;
	@Autowired
	private TbkEntrySceneDetailMapper tbkEntrySceneDetailMapper;
	@Autowired
	private TbkEntrySceneAcupMapper  tbkEntrySceneAcupMapper;
	@Autowired
	private TbkEventMapper tbkEventMapper;
	@Autowired
	private TbkProductEventMapper tbkProductEventMapper;
	@Autowired
	private ProductService productService;
	@Autowired
	private PretreatmentService pretreatmentService;
	@Autowired
	private TbkEventService tbkEventService;
	@Autowired
	private AccountingTaskService accountingTaskService;
	@Autowired
	private DayendDateService dayendDateService;
	@Autowired
	private DictionaryGetService dictionaryGetService;
	@Autowired
	InstitutionService institutionService;
	@Autowired
	private TbkEntryService bookkeepingEntryService;
	@Autowired
	private TbkAmountService tbkAmountService;
	@Autowired
	private TbkEntrySceneService tbkEntrySceneService;
	@Autowired
	private TbkEntrySettingService entrySettingServiceImpl;
	@Autowired
	private TbkEntrySceneConfigMapper tbkEntrySceneConfigMapper;
	@Autowired
	private TbkProductSubjectMapper tbkProductSubjectMapper;
	@Autowired
	private TbkProductEventService tbkProductEventService;
	@Override
	public synchronized List<TbkEntrySceneAcup> getIfrs9SceneByConfigure(Map<String, Object> paramsMap) {
		
		// TODO Auto-generated method stub
		getIfrs9ParamsString(paramsMap);
		/**
		 * 校验事件是否为空
		 */
		JY.require(!StringUtil.checkEmptyNull(ParameterUtil.getString(paramsMap, "eventId", null)), "事件参数：eventId代码不能为空！");
		List<TbkEntrySceneDetail> details = tbkEntrySceneDetailMapper.gTbkEntrySceneDetailsByPrdNoEventId(paramsMap);
		JY.require(!(details.size()>0), ParameterUtil.getString(paramsMap, "pIdString", null)+"."+ParameterUtil.getString(paramsMap, "pValueString", null)+"未配置场景！");
		for(TbkEntrySceneDetail detail:details) {
			//打印获得的会计场景序列ID
			System.out.println("SceneId:"+detail.getSceneId()+".paramsKeyValue:"+detail.getParamsString());
		}
		paramsMap.put("list", details);//设置查询条件
		return tbkEntrySceneAcupMapper.getTbkEntrySceneAcups(paramsMap);//获得结果集，会计分录
	}

	@Override
	public synchronized Map<String, Object> getIfrs9ParamsString(Map<String, Object> beanPropertysMap) {
		// TODO Auto-generated method stub
		JY.require(!StringUtil.checkEmptyNull(ParameterUtil.getString(beanPropertysMap, "prdNo", null)), "带入参数：prdNo产品代码不能为空！");
		/**s
		 * elect p.*,m.param_name,param_qz,param_source from tbk_product_params p left join tbk_params m on p.param_id=m.param_source
		 * where prd_no='288' and active='1'
		 */
		List<TbkProductParams> tbkProductParamsList =  tbkProductParamsMapper.geTbkProductParamsByPrdNo(beanPropertysMap);
		//检查会计维度是否含值
		for(TbkProductParams tbkProductParams:tbkProductParamsList) {
			JY.require(!StringUtil.checkEmptyNull(ParameterUtil.getString(beanPropertysMap, tbkProductParams.getParamSource(), null)), "会计维度参数："+tbkProductParams.getParamName()+"."+tbkProductParams.getParamId()+"不能为空！");
		}
		/**
		 * 按照权重排序，形成特定的组合
		 */
		CollectionUtils.sort(tbkProductParamsList, false, "paramQz");
		String[] pIdString = new String[tbkProductParamsList.size()];
		String[] pValueString = new String[tbkProductParamsList.size()];
		for(int i = 0 ; i < tbkProductParamsList.size() ; i++) {
			pIdString[i] = tbkProductParamsList.get(i).getParamSource();
			pValueString[i] = ParameterUtil.getString(beanPropertysMap, tbkProductParamsList.get(i).getParamSource(), null);
		}
		beanPropertysMap.put("pIdString", StringUtils.join(pIdString, ","));
		beanPropertysMap.put("pValueString", StringUtils.join(pValueString, ","));
		return beanPropertysMap;
	}

	@Override
	public String generateEntry4Instruction(InstructionPackage instructionPackage) {
		// TODO Auto-generated method stub
		/**
		 * instructionPackage InfoMap 交易Map
		 * 查看 instructionPackage 是否存在prdNo 和  eventId  如果存在则锁定行，避免出现更改tbk_event表和多并发造成的数据混乱
		 */
		JY.require( !(null == instructionPackage), "主体instructionPackage会计包不能为空");
		JY.require(!StringUtil.checkEmptyNull(ParameterUtil.getString(instructionPackage.getInfoMap(), "prdNo", null)), "带入参数：prdNo产品代码不能为空！");
		Map<String, Object> paramsMap = new HashMap<String, Object>();//中间调用参数集合
		paramsMap.put("prdNo", ParameterUtil.getString(instructionPackage.getInfoMap(), "prdNo", null));
		String eventId = ParameterUtil.getString(instructionPackage.getInfoMap(), "eventId", "");
		if(StringUtil.checkEmptyNull(eventId)) {//判断事件驱动，如果为空，则选择使用老会计引擎
			//表级锁
			tbkEventMapper.lockTbkEvent();
		}else
		{
			//行级锁
			paramsMap.put("prdNo", ParameterUtil.getString(instructionPackage.getInfoMap(), "eventId", null));
			tbkProductEventMapper.lockTbkProductEventById(paramsMap);
		}
		if(!productService.checkIsAcctAcup(instructionPackage.getInfoMap()))//检查当前产品，当前币种 是否需要出会计分录；
		{
			return "000000";//无需出会计分录
		}
		// 预处理
		pretreatmentService.dealPretreatment(instructionPackage);
		String eventType = ParameterUtil.getString(instructionPackage.getInfoMap(), "eventType", "");//获得金额类别的会计类型
		JY.require(!StringUtil.checkEmptyNull(eventType) && !StringUtil.checkEmptyNull(eventId), "带入参数：eventId 会计事件为空的前提下，eventType金额会计类型不能为空！");
		TbkEvent event = new TbkEvent();
		event.setEventType(eventType);
		List<TbkEvent> eventList = tbkEventService.getTbkEventList(BeanUtil.beanToMap(event));//会计事件
		JY.require(!(eventList.size()>0) && !StringUtil.checkEmptyNull(eventId),  "带入参数：eventId 会计事件为空的前提下，金额事件 eventList 不能为空！");
		eventId = !StringUtil.checkEmptyNull(eventId)?eventId:eventList.get(0).getEventType();
		instructionPackage.getInfoMap().put("eventId", eventId);//将会计事件纳入Map
		
		
		//根据条件获取帐套信息
		TacTask task = new TacTask();
		task.setBkMain(","+ParameterUtil.getString(instructionPackage.getInfoMap(),"prdNo","")+",");
		task.setInstId(","+ParameterUtil.getString(instructionPackage.getInfoMap(),"sponInst","")+",");
		List<TacTask> taskList = accountingTaskService.selectEntryTaskList(BeanUtil.beanToMap(task));
		if(taskList != null && taskList.size() > 0){
			task = taskList.get(0);
		}else{
			taskList = accountingTaskService.selectEntryTaskList(null);
			task = taskList.get(0);
		}
		instructionPackage.getInfoMap().put("taskId", task.getTaskId());
		
		//定义会计分录列表
		List<TbkEntry> entryList = new ArrayList<TbkEntry>();
		String postDate = dayendDateService.getDayendDate();//账务日期
		String bkkpgOrgSource = dictionaryGetService.getTaDictByCodeAndKey("bkkpgOrgSource", "01").getDict_value();
		String bkkpgOrgId = null;
		if("1".equals(bkkpgOrgSource)){//记账机构
			TtInstitution inst = institutionService.getInstitutionById(ParameterUtil.getString(instructionPackage.getInfoMap(),"sponInst",""));
			if(inst != null){
				bkkpgOrgId = inst.getbInstId();
			}
		}else{
			bkkpgOrgId = task.getBkkpgOrgId();
		}

		String flowNo = bookkeepingEntryService.getNewFlowNo();
		//查询交易所对应的所有维度信息 项下的分录配置
		List<TbkEntrySceneAcup> tbkEntrySceneAcups = getIfrs9SceneByConfigure(instructionPackage.getInfoMap());
		JY.require(!(tbkEntrySceneAcups.size()>0), "没有配置当前场景下的会计分录！");
		for(TbkEntrySceneAcup ttBkEntrySetting : tbkEntrySceneAcups) {
			TbkEntry entry = new TbkEntry();
			//自动映射同名属性 （简化操作）
			try {
				org.springframework.beans.BeanUtils.copyProperties(ttBkEntrySetting, entry);
				BeanUtils.populate(entry, instructionPackage.getInfoMap());
			} catch (Exception e1) {
				JY.raise("BeanUtils.populate", e1);;
				throw new RException(e1);
			}
			if("99".equals(ParameterUtil.getString(instructionPackage.getInfoMap(),"prdNo",""))){//费用产品设定
				entry.setPrdNo(ParameterUtil.getString(instructionPackage.getInfoMap(),"prdNo_1",""));
			}
			entry.setEventId(eventId);
			entry.setTaskId(task.getTaskId());
			entry.setFlowId(flowNo);
			entry.setPostDate(postDate);
			entry.setFlowSeq(String.valueOf(ttBkEntrySetting.getSort()));
			entry.setBkkpgOrgId(bkkpgOrgId);
			entry.setExpression(ttBkEntrySetting.getExpression());
			entry.setState(DictConstants.EntryState.NORMAL);
			entry.setSendFlag(DictConstants.EntrySendFlag.UnSend);
			entry.setUpdateTime(DateUtil.getCurrentDateTimeAsString());
			//暂时去掉了科目账号公式（后续可以引入表达式）
			entry.setCoreAcctCode(entry.getSubjCode());
			Map<String, Object> calMap = new HashMap<String, Object>();
			calMap.put("amtMap", instructionPackage.getAmtMap());
			double value = 0;
			try {
				TbkAmount tbkAmount = tbkAmountService.getAmountById(ttBkEntrySetting.getExpression());
					JY.require(tbkAmount.getAmountFormula() != null, "交易编号为："+ParameterUtil.getString(instructionPackage.getInfoMap(),"dealNo","")+"未配置金额公式代码");
				value = calcBkIndex(calMap, tbkAmount.getAmountFormula()); //计算
			} catch (Exception e) {
				JY.error("",e);
			}
			if(DictConstants.DebitCreditConfigFlag.Debit.equals(ttBkEntrySetting.getDebitCreditFlag())){
				//借		取绝对值放借方
				entry.setDebitCreditFlag(DictConstants.DebitCreditFlag.Debit);
			}else if(DictConstants.DebitCreditConfigFlag.PositiveDebit.equals(ttBkEntrySetting.getDebitCreditFlag())){
				//借(+) PD		正数放借方，负数不出分录
				if(value > 0){
					entry.setDebitCreditFlag(DictConstants.DebitCreditFlag.Debit);
				}
			}else if(DictConstants.DebitCreditConfigFlag.MinusDebit.equals(ttBkEntrySetting.getDebitCreditFlag())){
				//借(-) MD		负数放借方，正数不出分录
				if(value < 0){
					entry.setDebitCreditFlag(DictConstants.DebitCreditFlag.Debit);
				}
			}else if(DictConstants.DebitCreditConfigFlag.Credit.equals(ttBkEntrySetting.getDebitCreditFlag())){
				//贷		取绝对值放贷方
				entry.setDebitCreditFlag(DictConstants.DebitCreditFlag.Credit);
			}else if(DictConstants.DebitCreditConfigFlag.PositiveCredit.equals(ttBkEntrySetting.getDebitCreditFlag())){
				//贷(+) PC	正数放贷方，负数不出分录
				if(value > 0){
					entry.setDebitCreditFlag(DictConstants.DebitCreditFlag.Credit);
				}
			}else if(DictConstants.DebitCreditConfigFlag.MinusCredit.equals(ttBkEntrySetting.getDebitCreditFlag())){
				//贷(-) MC	负数放贷方，正数不出分录
				if(value < 0){
					entry.setDebitCreditFlag(DictConstants.DebitCreditFlag.Credit);
				}
			}else if(DictConstants.DebitCreditConfigFlag.PositiveDebitMinusCredit.equals(ttBkEntrySetting.getDebitCreditFlag())){
				//借(+)/贷(-) PDMC	正数放借方，负数放贷方
				if(value > 0){
					entry.setDebitCreditFlag(DictConstants.DebitCreditFlag.Debit);
					entry.setFlowSeq(String.valueOf(ttBkEntrySetting.getSort()-1));
				}else{
					entry.setDebitCreditFlag(DictConstants.DebitCreditFlag.Credit);
					entry.setFlowSeq(String.valueOf(ttBkEntrySetting.getSort()+1));
				}
			}else if(DictConstants.DebitCreditConfigFlag.PositiveCreditMinusDebit.equals(ttBkEntrySetting.getDebitCreditFlag())){
				//贷(+)/借(-) PCMD	正数放贷方，负数放借方
				if(value > 0){
					entry.setDebitCreditFlag(DictConstants.DebitCreditFlag.Credit);
					entry.setFlowSeq(String.valueOf(ttBkEntrySetting.getSort()+1));
				}else{
					entry.setDebitCreditFlag(DictConstants.DebitCreditFlag.Debit);
					entry.setFlowSeq(String.valueOf(ttBkEntrySetting.getSort()-1));
				}
			}

			entry.setValue(MathUtil.round(Math.abs(value), 2));
			//分录中判断得到的借贷方向与分录配置中强制借贷方向不相同，需修改借贷方向，设置为红字  （需要关联科目）
			entryList.add(entry);
		}
		return null;
	}
	
	/**
	 * 脚本引擎加载，计算表达式
	 */
	private static ScriptEngineManager manager = new ScriptEngineManager(); 
	private static ScriptEngine engine;
	
	/**
	 * 计算记账指标
	 * @param map
	 * @param expression
	 * @return
	 */
	public synchronized static double calcBkIndex(Map<String, Object> map, String expression){
		if(engine == null) {
			engine =  manager.getEngineByName("js");
		}
		for(String key:map.keySet()){
			engine.put(key, map.get(key));
		}
		try {
			Object val = engine.eval(expression);
			if(val != null){
				return Double.parseDouble(val.toString());
			}
		} catch (Exception e) {
			//e.printStackTrace();
			throw new RException(e);
		}
		return 0;
	}

	
	
	@Override
	public boolean tranferIas9ToIfrs9AutoService() {
		// TODO Auto-generated method stub
		//取出所以IAS9的会计场景表数据TBK_ENTRY_SCENE_INFO
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("pageSize", 9999999);
		Page<TbkEntrySceneInfo> entryPage= tbkEntrySceneService.getTbkEntryScenePage(map);
		List<TbkEntrySceneInfo> entrySceneInfoList = entryPage.getResult();
		/**
		 * delete from tbk_entry_scene_acup;
		 * delete from tbk_entry_scene_detail;
		 */
		tbkEntrySceneAcupMapper.deleteAllTbkEntrySceneAcup();
		tbkEntrySceneDetailMapper.deleteAllTbkEntrySceneDetail();
		for(TbkEntrySceneInfo entrySceneInfo : entrySceneInfoList) {
			/**
			 *  paramI2--pClass-intType-intFre-invType-paramI0--
			 */
			//产品列表
			String pClass_1 = entrySceneInfo.getpClass();List<String> pClassResult = new ArrayList<String>();
			if(null != pClass_1) {
				String[] pClassArray = pClass_1.split(",");
				for(String pClass:pClassArray) {
					if(!StringUtil.checkEmptyNull(pClass)) {
						pClassResult.add("@pClass@"+pClass+"_pClass_");
					}
				}
			}
			//金融资产四分类
			List<String> invTypeResult = new ArrayList<String>();String invType_1 = entrySceneInfo.getInvType();
			if(null != invType_1) {
				String[] invTypeArray = invType_1.split(",");
				for(String invType:invTypeArray) {
					if(!StringUtil.checkEmptyNull(invType)) {
						invTypeResult.add("@invType@"+invType+"_invType_");
					}
				}
			}
			//正常/逾期 paramI0
			List<String> paramI0Result = new ArrayList<String>();String paramI0_1 = entrySceneInfo.getParamI0();
			if(null != paramI0_1) {
				String[] paramI0Array = paramI0_1.split(",");
				for(String paramI0:paramI0Array) {
					if(!StringUtil.checkEmptyNull(paramI0)) {
						paramI0Result.add("@paramI0@"+paramI0+"_paramI0_");
					}
				}
			}

			//付息频率 intFre
			String intFre_1 = entrySceneInfo.getIntFre();List<String> intFreResult = new ArrayList<String>();
			if(null != intFre_1) {
				String[] intFreArray = intFre_1.split(",");
				for(String intFre:intFreArray) {
					if(!StringUtil.checkEmptyNull(intFre)) {
						intFreResult.add("@intFre@"+intFre+"_intFre_");
					}
				}
			}

			//收息方式 intType
			String intType_1 = entrySceneInfo.getIntType();List<String> intTypeResult = new ArrayList<String>();
			if(null != intType_1) {
				String[] intTypeArray = intType_1.split(",");
				
				for(String intType:intTypeArray) {
					if(!StringUtil.checkEmptyNull(intType)) {
						intTypeResult.add("@intType@"+intType+"_intType_");
					}
				}
			}

			//利率调整 paramI2
			List<String> paramI2Result = new ArrayList<String>();String paramI2_1 = entrySceneInfo.getParamI2();
			if(null != paramI2_1) {
				String[] paramI2Array = paramI2_1.split(",");
				for(String paramI2:paramI2Array) {
					if(!StringUtil.checkEmptyNull(paramI2)) {
						paramI2Result.add("@paramI2@"+paramI2+"_paramI2_");
					}
				}
			}
			String[][] result = {pClassResult.toArray(new String[pClassResult.size()]),invTypeResult.toArray(new String[invTypeResult.size()]),
					paramI0Result.toArray(new String[paramI0Result.size()]),intFreResult.toArray(new String[intFreResult.size()]),
					intTypeResult.toArray(new String[intTypeResult.size()]),paramI2Result.toArray(new String[paramI2Result.size()])};
			List<String[]> result2 = new ArrayList<String[]>();
					
			for(int i=0;i<result.length;i++){			
				 if(result[i].length!=0) {
					 result2.add(result[i]);
					}
				}	
			String[][] result3 = new String[result2.size()][];
			for(int i = 0;i<result2.size();i++)
			{
				result3[i]=result2.get(i);
			}
			List<String>strs=getList(result3);		
			//会计分录
			List<TbkEntrySetting> ttBkEntrySettingList = entrySettingServiceImpl.getEntrySettingList(entrySceneInfo.getSceneId(),null);
			for(TbkEntrySetting entrySetting : ttBkEntrySettingList) {
				TbkEntrySceneAcup tbkEntrySceneAcup = new TbkEntrySceneAcup();
				BeanUtil.copyNotEmptyProperties(tbkEntrySceneAcup, entrySetting);
				tbkEntrySceneAcup.setSceneId(entrySceneInfo.getSceneId()+"_"+entrySetting.getSettingGroupId());
				this.tbkEntrySceneAcupMapper.insert(tbkEntrySceneAcup);
				
			}
			//设置维度场景
			Map<String,Object> params = new HashMap<String, Object>();
			params.put("sceneId", entrySceneInfo.getSceneId());
			List<TbkEntrySceneConfig> tbkEntrySceneConfigs = tbkEntrySceneConfigMapper.searchEntrySceneConfig(params);
			for(TbkEntrySceneConfig entrySceneConfig:tbkEntrySceneConfigs) {
				TbkEntrySceneDetail entrySceneDetail = new TbkEntrySceneDetail();
				entrySceneDetail.setSceneId(entrySceneInfo.getSceneId()+"_"+entrySceneConfig.getSettingGroupId());//这里的groupId作为唯一的场景
				for(String temp:strs){		
					String dId = tbkEntrySceneDetailMapper.getEntrySceneDetailId();
					entrySceneDetail.setdId(dId);
					//开始循环存储 key value
					if(!StringUtil.checkEmptyNull(temp.substring(temp.indexOf("@pClass@")+8, temp.indexOf("_pClass_")))) {
						entrySceneDetail.setpId("pClass");
						entrySceneDetail.setpValue(temp.substring(temp.indexOf("@pClass@")+8, temp.indexOf("_pClass_")));
						Map<String, Object> paramsMap = new HashMap<String, Object>();
						paramsMap.put("prdNo", entrySceneDetail.getpValue());
						paramsMap.put("settingGroupId", entrySceneConfig.getSettingGroupId());
						System.out.println(entrySceneConfig.getSettingGroupId());
						List<TbkEntrySetting> entrySettings = entrySettingServiceImpl.getBkEntrySettingList(paramsMap);
						if(entrySettings.size()<=0) {continue;}//说明以前多配置了 场景对应的分录组别
							else
						{paramsMap.put("eventId", entrySettingServiceImpl.getBkEntrySettingList(paramsMap).get(0).getEventId());}
						if(tbkProductEventService.getTbkProductEventById(paramsMap)!=null) {
							entrySceneDetail.setDbId(tbkProductEventService.getTbkProductEventById(paramsMap).getDbId());}
						else {//产生产品和事件的维护关系
							tbkProductEventService.addTbkProductEventService(paramsMap);
							entrySceneDetail.setDbId(tbkProductEventService.getTbkProductEventById(paramsMap).getDbId());
						}
						tbkEntrySceneDetailMapper.insert(entrySceneDetail);
					}
					if(temp.contains("_paramI2_") && !StringUtil.checkEmptyNull(temp.substring(temp.indexOf("@paramI2@")+9, temp.indexOf("_paramI2_")))) {
						entrySceneDetail.setpId("paramI2");
						entrySceneDetail.setpValue(temp.substring(temp.indexOf("@paramI2@")+9, temp.indexOf("_paramI2_")));
						tbkEntrySceneDetailMapper.insert(entrySceneDetail);
					}
					if(temp.contains("_intType_") && !StringUtil.checkEmptyNull(temp.substring(temp.indexOf("@intType@")+9, temp.indexOf("_intType_")))) {
						entrySceneDetail.setpId("intType");
						entrySceneDetail.setpValue(temp.substring(temp.indexOf("@intType@")+9, temp.indexOf("_intType_")));
						tbkEntrySceneDetailMapper.insert(entrySceneDetail);
					}
					if(temp.contains("_intFre_") && !StringUtil.checkEmptyNull(temp.substring(temp.indexOf("@intFre@")+8, temp.indexOf("_intFre_")))) {
						entrySceneDetail.setpId("intFre");
						entrySceneDetail.setpValue(temp.substring(temp.indexOf("@intFre@")+8, temp.indexOf("_intFre_")));
						tbkEntrySceneDetailMapper.insert(entrySceneDetail);
					}
					if(temp.contains("_invType_") && !StringUtil.checkEmptyNull(temp.substring(temp.indexOf("@invType@")+9, temp.indexOf("_invType_")))) {
						entrySceneDetail.setpId("invType");
						entrySceneDetail.setpValue(temp.substring(temp.indexOf("@invType@")+9, temp.indexOf("_invType_")));
						tbkEntrySceneDetailMapper.insert(entrySceneDetail);
					}
					if(temp.contains("_paramI0_") && !StringUtil.checkEmptyNull(temp.substring(temp.indexOf("@paramI0@")+9, temp.indexOf("_paramI0_")))) {
						entrySceneDetail.setpId("paramI0");
						entrySceneDetail.setpValue(temp.substring(temp.indexOf("@paramI0@")+9, temp.indexOf("_paramI0_")));
						tbkEntrySceneDetailMapper.insert(entrySceneDetail);
					}
				}
			}
		}
		/**
		 * 执行完需要手动在PLSQL执行
		 * delete from tbk_product_params;
		 * insert into tbk_product_params
			select distinct e.prd_no,d.p_id from TBK_ENTRY_SCENE_DETAIL d left join  tbk_product_event e on e.db_id = d.db_id order by e.prd_no;
		   将数据刷入产品的参数列表（此表决定产品影响会计的维度
		 */
		tbkProductParamsMapper.deleteAllTbkProductParams();
		tbkProductParamsMapper.insertAllTbkProductParams();
		/**
		 * 将已配置场景的产品所属科目归类
		 * delete from tbk_product_subject;
		 * insert into tbk_product_subject
			select SEQ_ED_CUST_ID.nextval,'801',subj_code,subj_name  from  (
			select distinct subj_code,subj_name from tbk_entry_scene_acup where scene_id in (
			select distinct scene_id from tbk_entry_scene_detail where db_id in (
			select db_id from tbk_product_event T where exists (select e.event_id from tbk_product_event e where e.prd_no='801' and t.event_id=e.event_id) )));
		 */
		tbkProductSubjectMapper.deleteAllTbkProductSubject();
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("pageSize",99999);
		params.put("isAcup", 1);
		Page<TcProduct> page = productService.getProductPage(params);
		for(TcProduct product : page) {
			params.put("prdNo", product.getPrdNo());
			tbkProductSubjectMapper.insertTbkProductSubjectByPrdNo(params);
		}
		return false;
	}
	public static List<String> getList(String[][]a){//列举多维数据的组合值
		Random r=new Random();		
		List<String> strs=new ArrayList<String>();		
		int n=1;		
		//计算共有多少种情况 总数为所有数组长度的积		
		for(int i=0;i<a.length;i++){			
			n*=a[i].length==0?1:a[i].length;		
		}		
		for(int i=0;i<n;i++){			
			String c="";			
			for(int j=0;j<a.length;j++){
		//每一个数组里取一个随机元素				
				int index=r.nextInt(a[j].length);//随机数的范围是这个数组的长度				
				c+=a[j][index];			
			}			
		//判断集合中是否含有这个元素,有则总数+1,无则添加到这个集合中			
			if(strs.contains(c)){				
				n++;			
			}else{				
				strs.add(c);			
			}		
		}		
		Collections.sort(strs);//最后排序一下		
		return strs;	
		}
}
