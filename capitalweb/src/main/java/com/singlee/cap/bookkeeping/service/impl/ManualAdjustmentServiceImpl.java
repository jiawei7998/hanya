package com.singlee.cap.bookkeeping.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.cap.bookkeeping.model.TbkDealBalance;
import com.singlee.cap.bookkeeping.service.ManualAdjustmentService;
import com.singlee.cap.bookkeeping.service.TBkDealBalanceService;
import com.singlee.capital.common.util.ParameterUtil;

/**
 * 记账余额的服务类
 * @author shiting
 *
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class ManualAdjustmentServiceImpl implements ManualAdjustmentService {
	@Autowired
	private TBkDealBalanceService tBkDealBalanceService;
	@Override
	public void manualAdjustment(String dealNo) {
		
	}

	@Override
	public Page<TbkDealBalance> queryEntrySum(Map<String, Object> map) {
		return tBkDealBalanceService.selectByMap(map, ParameterUtil.getRowBounds(map));
	}
}
