package com.singlee.cap.bookkeeping.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * 交易科目余额汇总表
 * @author shiting
 *
 */
@Entity
@Table(name = "TBK_DEAL_BALANCE")
public class TbkDealBalance implements Serializable {
	private static final long serialVersionUID = -2840141545039907322L;
	/**　　帐套ID **/
	private String taskId;
	/**　　交易流水号**/
	private String dealNo;
	/**　　记账机构**/
	private String bkkpgOrgId;
	/**　　币种**/
	private String ccy;
	/**　　科目代码**/
	private String subjCode;
	/**　　科目名称**/
	private String subjName;
	/**　　借方余额**/
	private double debitValue;
	/**　　贷方余额**/
	private double creditValue;
	/**　　收方余额**/
	private double receiveValue;
	/**　　付方余额**/
	private double payValue;
	/**　　最后修改时间**/
	private String lstDate;
	
	private String expression;

	@Transient
	private String amountName;
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public String getBkkpgOrgId() {
		return bkkpgOrgId;
	}
	public void setBkkpgOrgId(String bkkpgOrgId) {
		this.bkkpgOrgId = bkkpgOrgId;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public String getSubjCode() {
		return subjCode;
	}
	public void setSubjCode(String subjCode) {
		this.subjCode = subjCode;
	}
	public String getSubjName() {
		return subjName;
	}
	public void setSubjName(String subjName) {
		this.subjName = subjName;
	}
	public double getDebitValue() {
		return debitValue;
	}
	public void setDebitValue(double debitValue) {
		this.debitValue = debitValue;
	}
	public double getCreditValue() {
		return creditValue;
	}
	public void setCreditValue(double creditValue) {
		this.creditValue = creditValue;
	}
	public double getReceiveValue() {
		return receiveValue;
	}
	public void setReceiveValue(double receiveValue) {
		this.receiveValue = receiveValue;
	}
	public double getPayValue() {
		return payValue;
	}
	public void setPayValue(double payValue) {
		this.payValue = payValue;
	}
	public String getLstDate() {
		return lstDate;
	}
	public void setLstDate(String lstDate) {
		this.lstDate = lstDate;
	}
	public String getExpression() {
		return expression;
	}
	public void setExpression(String expression) {
		this.expression = expression;
	}
	public String getAmountName() {
		return amountName;
	}
	public void setAmountName(String amountName) {
		this.amountName = amountName;
	}

}
