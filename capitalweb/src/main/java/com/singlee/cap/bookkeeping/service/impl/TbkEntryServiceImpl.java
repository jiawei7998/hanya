package com.singlee.cap.bookkeeping.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.cap.bookkeeping.mapper.TbkEntryMapper;
import com.singlee.cap.bookkeeping.model.TbkEntry;
import com.singlee.cap.bookkeeping.service.TbkEntryService;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.mapper.TtInstitutionMapper;
import com.singlee.capital.system.model.TtInstitution;
import com.singlee.capital.system.service.InstitutionService;
import com.singlee.capital.system.session.impl.SlSessionHelper;

/**
 * 记账分录服务实现
 * @author Libo
 *
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TbkEntryServiceImpl implements TbkEntryService {

	@Autowired
	private TbkEntryMapper bookkeepingEntryMapper;
	@Autowired
	private TtInstitutionMapper institutionMapper;
	@Autowired
	private InstitutionService institutionService;
	
	/**  获取新的流水号   */
	@Override
	public String getNewFlowNo() {
		return bookkeepingEntryMapper.getNewFlowNo();
	}

	@Override
	public Page<List<TbkEntry>> getEntryList(Map<String, Object> map,RowBounds rb) {
		if("".equals(ParameterUtil.getString(map, "eventId", "")) &&
		   !"9001".equals(ParameterUtil.getString(map, "eventId", ""))){
			HashMap<String, Object> params=new HashMap<String, Object>();
			params.put("instId", SlSessionHelper.getInstitutionId());
			TtInstitution ttinstitu =new TtInstitution();
			ttinstitu.setInstId(SlSessionHelper.getInstitutionId());
			// 根据机构主键获取机构信息
			ttinstitu = institutionMapper.selectByPrimaryKey(ttinstitu);
			
			// 根据登录人机构ID查询他的子机构
			List <TtInstitution> tlist = institutionService.searchChildrenInst(params);
			List<String> ttlist=new ArrayList<String>();
			for (TtInstitution ttInstitution : tlist) {
				ttlist.add(ttInstitution.getInstId());   //登录人子机构ID
			}
			map.put("tttlist", ttlist);
		}
		if("8012".equals(ParameterUtil.getString(map, "prdNo", ""))){//会计分录查询-理财债基
			map.put("prdNo", "801");
			map.put("fundType", 2);
		}else if("801".equals(ParameterUtil.getString(map, "prdNo", ""))){
			map.put("prdNo", "801");
			map.put("fundType", 1);
		}
		return bookkeepingEntryMapper.selectByMap(map, rb);
	}
	
	/**
	 * 会计分录excel导出
	 */
	@Override
	public Page<TbkEntry> getEntryList2(Map<String, Object> map,RowBounds rb) {
		if("".equals(ParameterUtil.getString(map, "eventId", "")) &&
		   !"9001".equals(ParameterUtil.getString(map, "eventId", ""))){
			HashMap<String, Object> params=new HashMap<String, Object>();
			params.put("instId", SlSessionHelper.getInstitutionId());
			TtInstitution ttinstitu =new TtInstitution();
			ttinstitu.setInstId(SlSessionHelper.getInstitutionId());
			// 根据机构主键获取机构信息
			ttinstitu = institutionMapper.selectByPrimaryKey(ttinstitu);
			
			// 根据登录人机构ID查询他的子机构
			List <TtInstitution> tlist = institutionService.searchChildrenInst(params);
			List<String> ttlist=new ArrayList<String>();
			for (TtInstitution ttInstitution : tlist) {
				ttlist.add(ttInstitution.getInstId());   //登录人子机构ID
			}
			map.put("tttlist", ttlist);
		}
		if("8012".equals(ParameterUtil.getString(map, "prdNo", ""))){//会计分录查询-理财债基
			map.put("prdNo", "801");
			map.put("fundType", 2);
		}else if("801".equals(ParameterUtil.getString(map, "prdNo", ""))){
			map.put("prdNo", "801");
			map.put("fundType", 1);
		}
		return bookkeepingEntryMapper.selectByMap2(map, ParameterUtil.getRowBounds(map));
	}
	
	@Override
	public void save(TbkEntry entry) {
		bookkeepingEntryMapper.insert(entry);
	}
	
	@Override
	public void save(List<TbkEntry> entryListAll) {
		if(entryListAll == null){
			return;
		}
		for(TbkEntry entry : entryListAll){
			save(entry);
		}
	}

	@Override
	public List<TbkEntry> getDealEntryList(Map<String, Object> map) {
		return bookkeepingEntryMapper.selectByMap(map);
	}

	@Override
	public List<TbkEntry> selectEntryFlowId(Map<String, Object> map) {
		return bookkeepingEntryMapper.selectEntryFlowId(map);
	}

	@Override
	public void updateEntryState(TbkEntry entry) {
		bookkeepingEntryMapper.updateEntryState(entry);
	}

	@Override
	public List<TbkEntry> selectByTimeService(Map<String, Object> map) {
		return bookkeepingEntryMapper.selectByTime(map);
	}
}
