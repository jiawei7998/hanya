package com.singlee.cap.bookkeeping.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.cap.bookkeeping.mapper.TbkApproveMapper;
import com.singlee.cap.bookkeeping.model.TbkApprove;
import com.singlee.cap.bookkeeping.service.TbkApproveService;
import com.singlee.capital.cashflow.mapper.TdAmtRangeMapper;
import com.singlee.capital.common.spring.json.FastJsonUtil;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.fund.service.TdDurationFundCashdivService;
import com.singlee.capital.fund.service.TdDurationFundRedeemService;
import com.singlee.capital.fund.service.TdFundAcupService;
import com.singlee.capital.fund.service.TdProductFundTposService;
import com.singlee.capital.ordersorting.model.OrderSortingDetail;
import com.singlee.capital.ordersorting.service.OrderSortingDetailService;
import com.singlee.capital.ordersorting.service.OrderSortingService;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.slbpm.externalInterface.SlbpmCallBackInteface;
import com.singlee.slbpm.externalInterface.TaskEventInterface;

@Transactional(value="transactionManager",rollbackFor=Exception.class)
@Service("tbkApproveService")
public class TbkApproveImpl implements TbkApproveService,SlbpmCallBackInteface,TaskEventInterface{

	@Resource
	TbkApproveMapper tbkApproveMapper;
	@Resource
	OrderSortingDetailService orderSortingDetailService;
	@Resource
	TdProductFundTposService tdProductFundTposService;
	
//	交易还本执行表
	@Resource
	TdAmtRangeMapper amtRangeMapper;
	@Resource
	TdDurationFundRedeemService tdDurationFundRedeemService;
	@Resource
	TdFundAcupService tdFundAcupService;
	@Resource
	TdDurationFundCashdivService tdDurationFundCashdivService;
	@Resource
	OrderSortingService orderSortingService;
	@Override
	public List<TbkApprove> getTbkApprove(
			Map<String, String> params) {
		return null;
	}

	@Override
	public Page<TbkApprove> searchTbkApprove(
			Map<String, Object> params, RowBounds rb) {
		return tbkApproveMapper.searchTbkApprove(params, rb);
	}

	@Override
	public int deleteTbkApprove(TbkApprove params) {
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("dealNo",params.getDealNo());
		//先更新动账表status为 0
		List<OrderSortingDetail> list=orderSortingDetailService.getOrderSortingDetail(map);
		if(list.size()>0){
			String tnoStr="";
			for (OrderSortingDetail orderSortingDetail : list) {
				if(orderSortingDetail.getRefType()==1){ // 0 同业系统实收    1来账列表实收
					tnoStr+=orderSortingDetail.getRefNo()+",";
				}
			}
			if(tnoStr.length()>0){
				map.put("tNo", tnoStr);
				map.put("status", '0');
				orderSortingService.updateTiTsaAccountStatusByTno(map); //更新动账表ti_tsaAccount
			}
		}
		//先删除款项明细表
		orderSortingDetailService.deleteOrderSortingDetailByDeal(map);
		return tbkApproveMapper.delete(params);
	}

	@Override
	public int insertTbkApprove(Map<String,Object> params) {
		TbkApprove TbkApprove = new TbkApprove();
		BeanUtil.populate(TbkApprove, params);
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		TbkApprove.setLstDate(df.format(new Date()));
		//查询是否已经存在，如果存在：先删除，再添加
		TbkApprove orderSorting = tbkApproveMapper.getTbkApproveById(params);
		if(null != orderSorting){
			tbkApproveMapper.deleteByPrimaryKey(TbkApprove);
			//先更新动账表status为 0
			List<OrderSortingDetail> list=orderSortingDetailService.getOrderSortingDetail(params);
			if(list.size()>0){
				String tnoStr="";
				for (OrderSortingDetail orderSortingDetail : list) {
					if(orderSortingDetail.getRefType()==1){
						tnoStr+=orderSortingDetail.getRefNo()+",";
					}
				}
				if(tnoStr.length()>0){
					params.put("tNo", tnoStr);
					params.put("status", '0');
					orderSortingService.updateTiTsaAccountStatusByTno(params); //更新动账表
				}
			}
			orderSortingDetailService.deleteOrderSortingDetailByDeal(params);
		}
		int flag = tbkApproveMapper.insert(TbkApprove);
		if(flag >0){
			flag = orderSortingDetailService.insertOrderSortingDetail(params);
			String jsonStr = params.get("jsonStr") != null ? params.get("jsonStr").toString():"";
			if(!"".equals(jsonStr)){
				List<OrderSortingDetail> list = FastJsonUtil.parseArrays(jsonStr, OrderSortingDetail.class);
				String tnoStr="";
				for (OrderSortingDetail orderSortingDetail : list) {
					if(orderSortingDetail.getRefType()==1){
						tnoStr+=orderSortingDetail.getRefNo()+",";
					}
				}
				if(tnoStr.length()>0){
					params.put("tNo", tnoStr);
					params.put("status", '2');
					orderSortingService.updateTiTsaAccountStatusByTno(params); //更新动账表
				}
			}
		}
		return flag;
	}

	@Override
	public int updateTbkApprove(TbkApprove params) {
		return tbkApproveMapper.updateByPrimaryKey(params);
	}

	@Override
	public Object getBizObj(String flowType, String flowId, String serialNo) {
		return null;
	}

	@Override
	public void statusChange(String flowType, String flowId, String serialNo,
			String status, String flowCompleteType) {
		
		HashMap<String, Object> tradeMap = new HashMap<String, Object>();
		tradeMap.put("flowId", serialNo);
		tradeMap.put("is_locked", true);
		if(DictConstants.FlowType.YearEndFlow.equals(flowType)){
			tradeMap.put("eventType", "year");
		}else if(DictConstants.FlowType.ManualAdjFlow.equals(flowType)){
			tradeMap.put("eventType", "manual");
		}
//		entryList = tbkApproveMapper.selectByMap(tradeMap);
//		if(DictConstants.ApproveStatus.ApprovedPass.equals(status)){
//			/**
//			 * 审批通过，把审批的分录生成到分录信息表中
//			 */
//			for (TbkManualEntry tbkYearEntry : entryList) {
//				TbkEntry entry = new TbkEntry();
//				BeanUtil.copyNotEmptyProperties(entry, tbkYearEntry);
//				entry.setState(DictConstants.EntryState.NORMAL);
//				entry.setSendFlag(DictConstants.EntrySendFlag.UnSend);
//				tbkEntryMapper.insert(entry);
//				
//				if(DictConstants.FlowType.ManualAdjFlow.equals(flowType)){
//						
//					//更新头寸
//					String subjCode = entry.getSubjCode();
//					String dealNo = tbkYearEntry.getDealNo();
//					Map<String, Object> map = new HashMap<String, Object>();
//					map.put("subjCode", subjCode);
//					List<TbkSubjectDef> list = tbkSubjectDefService.getTbkSubjectDefList(map);
//					
//					Map<String, Object> map1 = new HashMap<String, Object>();
//					map1.put("dealNo", dealNo);
//					TdTrdTpos ttts = trdTposService.getTdTrdTposByKey(map1);
//					/**科目性质不为空*/
//					if(list.get(0).getSubjNature()!=""){   
//						if(list.get(0).getSubjNature().equals("1")){
//							if(tbkYearEntry.getDebitCreditFlag().equals("C")){
//								ttts.setSettlAmt(PlaningTools.sub(entry.getValue(), ttts.getSettlAmt()));
//							}else if(tbkYearEntry.getDebitCreditFlag().equals("D")){
//								ttts.setSettlAmt(PlaningTools.add(entry.getValue(), ttts.getSettlAmt()));
//							}
//							trdTposMapper.updateByPrimaryKey(ttts);
//							continue;   //跳出此次循环
//						}
//						/**是否逾期   否*/
//						if(tbkYearEntry.getOutDate().equals("0")){  
//							if(list.get(0).getSubjNature().equals("5")||list.get(0).getSubjNature().equals("2")){
//								if(tbkYearEntry.getDebitCreditFlag().equals("C")){
//									ttts.setSettlAmt(PlaningTools.sub(entry.getValue(), ttts.getAccruedTint()));
//								}else if(tbkYearEntry.getDebitCreditFlag().equals("D")){
//									ttts.setSettlAmt(PlaningTools.add(entry.getValue(), ttts.getAccruedTint()));
//								}
//								trdTposMapper.updateByPrimaryKey(ttts);
//								continue;   //跳出此次循环
//							}else if(list.get(0).getSubjNature().equals("3")){
//								if(tbkYearEntry.getDebitCreditFlag().equals("C")){
//									ttts.setSettlAmt(PlaningTools.sub(entry.getValue(), ttts.getActualTint()));
//								}else if(tbkYearEntry.getDebitCreditFlag().equals("D")){
//									ttts.setSettlAmt(PlaningTools.add(entry.getValue(), ttts.getActualTint()));
//								}
//								trdTposMapper.updateByPrimaryKey(ttts);
//								continue;   //跳出此次循环
//							}
//						}
//						/**是否逾期   是*/
//						if(tbkYearEntry.getOutDate().equals("1")){   
//							if(list.get(0).getSubjNature().equals("5")||list.get(0).getSubjNature().equals("2")){
//								if(tbkYearEntry.getDebitCreditFlag().equals("C")){
//									ttts.setSettlAmt(PlaningTools.sub(entry.getValue(), ttts.getOverdueAccruedTint()));
//								}else if(tbkYearEntry.getDebitCreditFlag().equals("D")){
//									ttts.setSettlAmt(PlaningTools.add(entry.getValue(), ttts.getOverdueAccruedTint()));
//								}
//								trdTposMapper.updateByPrimaryKey(ttts);
//								continue;   //跳出此次循环
//							}else if(list.get(0).getSubjNature().equals("3")){
//								if(tbkYearEntry.getDebitCreditFlag().equals("C")){
//									ttts.setSettlAmt(PlaningTools.sub(entry.getValue(), ttts.getActualTint()));
//								}else if(tbkYearEntry.getDebitCreditFlag().equals("D")){
//									ttts.setSettlAmt(PlaningTools.add(entry.getValue(), ttts.getActualTint()));
//								}
//								trdTposMapper.updateByPrimaryKey(ttts);
//								continue;   //跳出此次循环
//							}
//						}
//					}
//					
//				}
//				
//			}
//		}
//		HashMap<String, Object> entryMap = new HashMap<String, Object>();
//		entryMap.put("flowId", serialNo);
//		tradeMap.put("state", status);
//		tbkManualEntryMapper.updateEntryByFlowid(entryMap);
	}

	@Override
	public TbkApprove getTbkApproveById(
			Map<String, Object> params) {
		return tbkApproveMapper.getTbkApproveById(params);
	}

	@Override
	public Map<String, Object> fireEvent(String flow_id, String serial_no,
			String task_id, String task_def_key) {
		return null;
	}

	@Override
	public Page<TbkApprove> searchTbkApproveMySelf(
			Map<String, Object> params, RowBounds rb) {
		return tbkApproveMapper.searchTbkApproveMySelf(params, rb);
	}

	@Override
	public Page<TbkApprove> searchTbkApproveFinished(
			Map<String, Object> params, RowBounds rb) {
		return tbkApproveMapper.searchTbkApproveFinished(params, rb);
	}

    @Override
    public void LimitOccupy(String serial_no, String prd_no) throws Exception {
        // TODO Auto-generated method stub
        
    }
}
