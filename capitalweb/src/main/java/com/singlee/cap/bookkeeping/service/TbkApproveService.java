package com.singlee.cap.bookkeeping.service;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.cap.bookkeeping.model.TbkApprove;

public interface TbkApproveService {
	
	public Page<TbkApprove> searchTbkApprove(Map<String,Object> params, RowBounds rb);
	
	public Page<TbkApprove> searchTbkApproveMySelf(Map<String,Object> params, RowBounds rb);
	public Page<TbkApprove> searchTbkApproveFinished(Map<String,Object> params, RowBounds rb);
	public List<TbkApprove> getTbkApprove(Map<String, String> params);
	
	public  int insertTbkApprove(Map<String,Object> params);
	
	public  int deleteTbkApprove(TbkApprove params);
	
	public  int updateTbkApprove(TbkApprove params);
	
	public TbkApprove getTbkApproveById(Map<String, Object> params);
}
