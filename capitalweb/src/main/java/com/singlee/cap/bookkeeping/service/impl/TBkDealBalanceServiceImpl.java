package com.singlee.cap.bookkeeping.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.cap.bookkeeping.mapper.QueryBalanceMapper;
import com.singlee.cap.bookkeeping.mapper.TbkDealBalanceMapper;
import com.singlee.cap.bookkeeping.model.QueryBalanceModel;
import com.singlee.cap.bookkeeping.model.TbkDealBalance;
import com.singlee.cap.bookkeeping.service.TBkDealBalanceService;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.ParameterUtil;

/**
 * 记账余额的服务类
 * @author Libo
 *
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TBkDealBalanceServiceImpl implements TBkDealBalanceService {

	@Autowired
	private TbkDealBalanceMapper tbkDealBalanceMapper;
	
	@Autowired
	private QueryBalanceMapper queryBalanceMapper;
	
	/**
	 * 更新余额，只更新四个数字余额（借、贷、支、付）
	 * @param balance
	 */
	@Override
	public void updateBalance(TbkDealBalance balance){
		tbkDealBalanceMapper.updateBalance(BeanUtil.beanToMap(balance));
	}

	@Override
	public Page<TbkDealBalance> selectByMap(Map<String, Object> map,
			RowBounds rb) {
		return tbkDealBalanceMapper.selectByMap(map, rb);
	}
	
	@Override
	public List<TbkDealBalance> selectByMap(Map<String, Object> map) {
		return tbkDealBalanceMapper.selectByMap(map);
	}

	@Override
	public void insert(TbkDealBalance balance) {
		tbkDealBalanceMapper.insert(balance);
	}

	@Override
	public Page<QueryBalanceModel> queryFundBal(Map<String, Object> map) {
		return queryBalanceMapper.queryFundBal(map,ParameterUtil.getRowBounds(map));
	}

	@Override
	public Page<QueryBalanceModel> queryProductBal(Map<String, Object> map) {
		return queryBalanceMapper.queryProductBal(map, ParameterUtil.getRowBounds(map));
	}

}
