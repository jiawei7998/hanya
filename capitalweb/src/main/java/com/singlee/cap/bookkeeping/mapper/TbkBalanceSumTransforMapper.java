package com.singlee.cap.bookkeeping.mapper;

import java.util.List;
import java.util.Map;

import tk.mybatis.mapper.common.Mapper;

import com.singlee.cap.bookkeeping.model.TbkBalanceSumTransfor;

public interface TbkBalanceSumTransforMapper extends Mapper<TbkBalanceSumTransfor> {
	 /**
	   * 根据条件查询专用表
	   * @param map
	   * @return
	   */
	  public List<TbkBalanceSumTransfor> selectTbkBalanceSumTransforList(Map<String,Object> map);
	  
	  /**
	   * 修改 
	   * @param map
	   */
	  public void updateBalanceSumTransfor(Map<String, Object> map);
}
