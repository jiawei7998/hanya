package com.singlee.cap.bookkeeping.service.impl;

import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.cap.bookkeeping.mapper.BalanceOfAccountFor407Mapper;
import com.singlee.cap.bookkeeping.mapper.TbkSubjectBalanceSumMapper;
import com.singlee.cap.bookkeeping.model.Acctno407WorkingbalH;
import com.singlee.cap.bookkeeping.model.TbkEntry;
import com.singlee.cap.bookkeeping.model.TbkSubjectBalanceSum;
import com.singlee.cap.bookkeeping.model.TiInterfaceTdpacstmtinq;
import com.singlee.cap.bookkeeping.service.TBkSubjectBalanceSumService;
import com.singlee.capital.calendar.util.CalendarUtil;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.interfacex.model.BOSFXII;
import com.singlee.capital.interfacex.model.CommonRqHdr;
import com.singlee.capital.interfacex.model.TDpAcStmtInfoRec;
import com.singlee.capital.interfacex.model.TDpAcStmtInqRq;
import com.singlee.capital.interfacex.model.TDpAcStmtInqRs;
import com.singlee.capital.interfacex.model.TDpAcctTypeInqRq;
import com.singlee.capital.interfacex.model.TDpAcctTypeInqRs;
import com.singlee.capital.interfacex.service.SocketClientService;
import com.singlee.capital.interfacex.socketservice.InterfaceCode;
import com.singlee.capital.system.mapper.AcctNoMapper;
import com.singlee.capital.system.model.AcctNo;
import com.singlee.capital.system.model.TtInstitution;
import com.singlee.capital.system.service.InstitutionService;

/**
 * 记账余额的服务类
 * @author shiting
 *
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TBkSubjectBalanceServiceSumImpl implements TBkSubjectBalanceSumService {

	@Autowired
	private TbkSubjectBalanceSumMapper tbkBalanceSumMapper;
	@Autowired
	DayendDateService dateService;
	@Autowired
	SocketClientService socketClientService;
	@Autowired
	private AcctNoMapper acctNoMapper;
	@Autowired
	private BalanceOfAccountFor407Mapper balanceOfAccountFor407Mapper;
	@Autowired
	InstitutionService institutionService;
	@Resource
	private DayendDateService dayendDateService;
	
	/**
	 * 根据分录查找相应的余额，如查询不到，则创建新的并返回
	 * @param taskId
	 * @param date
	 * @param entry
	 */
	@Override
	public TbkSubjectBalanceSum findOrCreateByEntry(TbkEntry entry){
		TbkSubjectBalanceSum balance = new TbkSubjectBalanceSum();
		balance.setTaskId(entry.getTaskId());
		balance.setSubjCode(entry.getSubjCode());
		balance.setCcy(entry.getCcy());
		balance.setBkkpgOrgId(entry.getBkkpgOrgId());
		List<TbkSubjectBalanceSum> balanceList = tbkBalanceSumMapper.selectByMap(BeanUtil.beanToHashMap(balance),new RowBounds());
		if(balanceList != null && balanceList.size()>0){
			return balanceList.get(0);
		}
		
		balance.setUpdateTime(DateUtil.getCurrentDateTimeAsString());
		balance.setSubjName(entry.getSubjName());
		tbkBalanceSumMapper.insert(balance);
		
		return balance;
		
	}
	
	/**
	 * 更新余额，只更新四个数字余额（借、贷、支、付）
	 * @param balance
	 */
	@Override
	public void updateBalance(Map<String, Object> map){
		tbkBalanceSumMapper.updateBalance(map);
	}

	@Override
	public Page<TbkSubjectBalanceSum> selectByMap(Map<String, Object> map,
			RowBounds rb) {
		return tbkBalanceSumMapper.selectByMap(map, rb);
	}
	
	@Override
	public List<TbkSubjectBalanceSum> selectByMap(Map<String, Object> map) {
		return tbkBalanceSumMapper.selectSumList(map);
	}

	@Override
	public void insert(TbkSubjectBalanceSum balance) {
		tbkBalanceSumMapper.insert(balance);
	}


	@Override
	public List<TbkSubjectBalanceSum> getAllDiffListService(
			Map<String, Object> map) {
		return tbkBalanceSumMapper.getAllDiffList(map);
	}


	@Override
	public void updateBeforeBalance() {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("postDate", dateService.getDayendDate());
		tbkBalanceSumMapper.copySubjectBalSum(map);
		tbkBalanceSumMapper.updateBeginValue();
	}

	/**
	 * 407对账
	 */
	@Override
	public Page<TbkSubjectBalanceSum> balanceOfAccountFor407(
			HashMap<String, Object> map, RowBounds rb) {
		List<String> ttList = new ArrayList<String>();
		String postDate = ParameterUtil.getString(map, "postDate", "");
		if("".equals(ParameterUtil.getString(map, "instId", ""))){
			map.put("instId", ParameterUtil.getString(map, "noInstId", ""));			 
			 List <TtInstitution> lists = institutionService.searchChildrenInst(map);
		      for (TtInstitution tt : lists) {
		                ttList.add(tt.getInstId());
		      }
			map.put("instId", ttList);
		}else{
			ttList.add(ParameterUtil.getString(map, "instId", ""));
			map.put("instId",ttList );
		}		
		
		Page<TbkSubjectBalanceSum> page2 = tbkBalanceSumMapper.balanceOfAcc407(map, rb);	//从407查询出来的总的数目（407总表）
		Page<TbkSubjectBalanceSum> page = new Page<TbkSubjectBalanceSum>();					//该page用来保存从科目余额表/科目余额历史表查出来的对应日期的407科目下的数据
		if(postDate.toString().equals(dayendDateService.getDayendDate())||postDate.toString()==dayendDateService.getDayendDate()){
//			page = tbkBalanceSumMapper.balanceOfAccountFor407(map, rb);//查询实时表
			page = tbkBalanceSumMapper.balanceOfAccountFor407(map);
		}else{
//			page = tbkBalanceSumMapper.balanceOfAccountFor407AndHistory(map, rb);//查询历史表	
			page = tbkBalanceSumMapper.balanceOfAccountFor407AndHistory(map);
		}
		HashMap<String, Object> map1=new HashMap<String, Object>();
		TDpAcStmtInqRq request = null;
		TDpAcctTypeInqRq request2 = null;
		/**
		 * 根据其中的网点和币种去acctno407表中获取账号和账号类型，
		 * 发送交易明细查询接口
		 */
		for(TbkSubjectBalanceSum p:page){
			map1.put("instId", p.getBkkpgOrgId());
			map1.put("ccy", p.getCcy());
			
			List<AcctNo> ls = acctNoMapper.searchAcctNo407List(map1);
//			HashMap<String, Object> map2 = new HashMap<String, Object>();
//			map2.put("instId", p.getBkkpgOrgId());
//			map2.put("ccy", p.getCcy());
//			map2.put("acctNo", ls.get(0).getAcctNo());
//			map2.put("postDate", postDate);
			p.setUpdateTime(postDate);			
			p.setReceiveChg(0);//该字段放核心贷方金额，在这先设置0
			p.setReceiveValue(p.getDebitChg()-p.getReceiveChg());//该字段放同业借方与核心贷方的金额差异
			p.setPayChg(0);//该字段放核心借方金额，在这先设置0
			p.setPayValue(p.getCreditChg()-p.getPayChg());//该字段放同业贷方与核心借方的金额差异
			p.setCreditValue(0);//该字段放核心407余额，在这先设置0
			p.setbDebitValue(p.getDebitValue()-p.getCreditValue());//该字段放同业余额方与核心余额的金额差异
			
//			//添加一个账户介质类型查询接口,用于查询当天该407账号下的时点余额
//			if(postDate.toString().equals(dayendDateService.getDayendDate())||postDate.toString()==dayendDateService.getDayendDate()){
//				request2 = new TDpAcctTypeInqRq();
//				request2.setMedAcctNo(ls.get(0).getAcctNo());
//				try {
//					TDpAcctTypeInqRs  response2 = socketClientService.t24TdpAcctTypeInqRequest(request2);
//					if(response2.getCommonRsHdr().getStatusCode().equals("000000")){
//						if(response2.getWorkingBal()==null){
//							p.setCreditValue(0);
//						}else{
//							p.setCreditValue(Double.valueOf(response2.getWorkingBal()));//该字段放核心407余额,用于下面计算同业余额方与核心余额的金额差异
//							p.setbDebitValue(p.getDebitValue()-p.getCreditValue());//该字段放同业余额方与核心余额的金额差异
//						}
//					}					
//				} catch (Exception e1) {
//					e1.printStackTrace();
//				}
//			}else{//该路径查询的是历史的407账号下的时点余额，查询407时点余额的表	ACCTNO407_WORKINGBAL_H
//				Acctno407WorkingbalH acc = tbkBalanceSumMapper.getAcctno407WorkingbalH(map2);
//				if(acc!=null){
//					p.setCreditValue(Double.valueOf(acc.getAmt()));//该字段放核心407余额,用于下面计算同业余额方与核心余额的金额差异
//					p.setbDebitValue(p.getDebitValue()-p.getCreditValue());//该字段放同业余额方与核心余额的金额差异
//				}				
//			}
			

			request=new TDpAcStmtInqRq();
			request.setMediumType(ls.get(0).getMediumType());
			request.setAcctNo(ls.get(0).getAcctNo());
			request.setCurrency(ls.get(0).getCcy());
			request.setBeginDt(postDate.toString().replace("-", "").replace("-", ""));
			request.setEndDt(postDate.toString().replace("-", "").replace("-", ""));			
			request.setMode("I");
			CommonRqHdr hdr = new CommonRqHdr();
			hdr.setChannelId(InterfaceCode.TI_CHANNELNO);
			hdr.setRqUID(UUID.randomUUID().toString());
			hdr.setTranDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
			hdr.setTranTime(new SimpleDateFormat("HHmmss").format(new Date()));
			request.setCommonRqHdr(hdr);
			try {
				TDpAcStmtInqRs response= socketClientService.t24TdpAcStmtInqRequest(request);
				if("000000".equals(response.getCommonRsHdr().getStatusCode())){
//				Map<String, Object> mapx = new HashMap<String, Object>();
				List<TDpAcStmtInfoRec> list=response.getTDpAcStmtInfoRec();

				p.setReceiveChg(Double.valueOf(list.get(0).getTotCrAmt()));//该字段放核心贷方金额，在这先设置0	//TotCrAmt	贷方总金额
				p.setReceiveValue(p.getDebitChg()-p.getReceiveChg());//该字段放同业借方与核心贷方的金额差异
				p.setPayChg(Double.valueOf(list.get(0).getTotDrAmt()));//该字段放核心借方金额，在这先设置0		//TotDrAmt  借方总金额
				p.setPayValue(p.getCreditChg()-p.getPayChg());//该字段放同业贷方与核心借方的金额差异
//				p.setCreditValue(0);//该字段放核心407余额，在这先设置0
//				p.setbDebitValue(p.getDebitValue()-p.getCreditValue());//该字段放同业余额方与核心余额的金额差异

				/**
				 * 把TDpAcStmtInfoRec存入数据库TI_INTERFACE_TDPACSTMTINQ表
				 */
//				Map<String, Object> mapt = new HashMap<String, Object>();
				for (TDpAcStmtInfoRec tDpAcStmtInfoRec : list) {
						TiInterfaceTdpacstmtinq tt =new TiInterfaceTdpacstmtinq();							
						tt.setInstId(p.getBkkpgOrgId());
						tt.setAcctNo(ls.get(0).getAcctNo());
						tt.setMediumType(ls.get(0).getMediumType());
						tt.setTxnRef(tDpAcStmtInfoRec.getTxnRef());
						tt.setBookingDate(new SimpleDateFormat("yyyy-MM-dd").format(new SimpleDateFormat("yyyyMMdd").parse(tDpAcStmtInfoRec.getBookingDate())));
						tt.setTxnDate(tDpAcStmtInfoRec.getTxnDate());
						tt.setTxnDrcr("DEBIT".equals(tDpAcStmtInfoRec.getTxnDrcr())?"D":"C");
						tt.setTxnAmt(tDpAcStmtInfoRec.getTxnAmt());
						tt.setDescs(tDpAcStmtInfoRec.getDesc());
						tt.setOppbrName(tDpAcStmtInfoRec.getOppbrName());
						tt.setCompName(tDpAcStmtInfoRec.getCompName());
						tt.setTxnFbDesc(tDpAcStmtInfoRec.getTxnFbDesc());
						tt.setTransCode(tDpAcStmtInfoRec.getTransCode());
						tt.setTxnShowDate(tDpAcStmtInfoRec.getTxnShowDate());
						tt.setFbid(tDpAcStmtInfoRec.getFbid());
						tt.setOperUser(tDpAcStmtInfoRec.getOperUser());
						tt.setMediumnFlg(tDpAcStmtInfoRec.getMediumNflg());
						tt.setCcy(p.getCcy());
						tt.setOppAc(tDpAcStmtInfoRec.getOppAc());
						tt.setOppacName(tDpAcStmtInfoRec.getOppacName());
						tt.setTxnOppno(tDpAcStmtInfoRec.getTxnOppno());
						tt.setStmt(tDpAcStmtInfoRec.getStmt());
						if(balanceOfAccountFor407Mapper.selectByTxnRefCount(tDpAcStmtInfoRec.getStmt())<=0){
							balanceOfAccountFor407Mapper.insert(tt);
						}						
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}		
		for(TbkSubjectBalanceSum p2:page2){
			for(TbkSubjectBalanceSum p1:page){
				if(p1.getBkkpgOrgId()==p2.getBkkpgOrgId()||p1.getBkkpgOrgId().endsWith(p2.getBkkpgOrgId())){
					p2.setbCreditValue(p1.getbCreditValue());
					p2.setbDebitValue(p1.getbDebitValue());
					p2.setBkkpgOrgId(p1.getBkkpgOrgId());
					p2.setbPayValue(p1.getbPayValue());
					p2.setbReceiveValue(p1.getbReceiveValue());
					p2.setCcy(p1.getCcy());
					p2.setCreditChg(p1.getCreditChg());
					p2.setCreditValue(p1.getCreditValue());
					p2.setDebitChg(p1.getDebitChg());
					p2.setDebitValue(p1.getDebitValue());
					p2.setInstName(p1.getInstName());
					p2.setPayChg(p1.getPayChg());
					p2.setPayValue(p1.getPayValue());
					p2.setReceiveChg(p1.getReceiveChg());
					p2.setReceiveValue(p1.getReceiveValue());
					p2.setSubjCode(p1.getSubjCode());
					p2.setSubjName(p1.getSubjName());
					p2.setTaskId(p1.getTaskId());
					p2.setTaskName(p1.getTaskName());
				}				
			}
			p2.setUpdateTime(postDate);	
			
			map1.put("instId", p2.getBkkpgOrgId());
			map1.put("ccy", p2.getCcy());
			
			List<AcctNo> list = acctNoMapper.searchAcctNo407List(map1);
			HashMap<String, Object> map2 = new HashMap<String, Object>();
			map2.put("instId", p2.getBkkpgOrgId());
			map2.put("ccy", p2.getCcy());
			map2.put("acctNo", list.get(0).getAcctNo());
			map2.put("postDate", postDate);
			//添加一个账户介质类型查询接口,用于查询当天该407账号下的时点余额
			if(postDate.toString().equals(dayendDateService.getDayendDate())||postDate.toString()==dayendDateService.getDayendDate()){
				request2 = new TDpAcctTypeInqRq();
				request2.setMedAcctNo(list.get(0).getAcctNo());
				try {
					TDpAcctTypeInqRs  response2 = socketClientService.t24TdpAcctTypeInqRequest(request2);
					if("000000".equals(response2.getCommonRsHdr().getStatusCode())){
						if(response2.getWorkingBal()==null){
							p2.setCreditValue(0);
						}else{
							p2.setCreditValue(Double.valueOf(response2.getWorkingBal()));//该字段放核心407余额,用于下面计算同业余额方与核心余额的金额差异
							p2.setbDebitValue(p2.getDebitValue()-p2.getCreditValue());//该字段放同业余额方与核心余额的金额差异
						}
					}					
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}else{//该路径查询的是历史的407账号下的时点余额，查询407时点余额的表	ACCTNO407_WORKINGBAL_H
				Acctno407WorkingbalH acc = tbkBalanceSumMapper.getAcctno407WorkingbalH(map2);
				if(acc!=null){
					p2.setCreditValue(Double.valueOf(acc.getAmt()));//该字段放核心407余额,用于下面计算同业余额方与核心余额的金额差异
					p2.setbDebitValue(p2.getDebitValue()-p2.getCreditValue());//该字段放同业余额方与核心余额的金额差异
				}				
			}
		}
		return page2;
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T converyToJavaBean(String xml, Class<T> c) {  
        T t = null;  
        try {  
        	int pos=xml.indexOf("<?xml version");
        	xml=xml.substring(pos);
        	xml=xml.replace(" xmlns=\"http://www.bankofshanghai.com/BOSFX/2010/08\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.bankofshanghai.com/BOSFX/2010/08 BOSFX2.0.xsd\"","");

        	JAXBContext context = JAXBContext.newInstance(c);  
            Unmarshaller unmarshaller = context.createUnmarshaller();  
            t = (T) unmarshaller.unmarshal(new StringReader(xml));  
        } catch (Exception e) {  
        	//e.printStackTrace();
			throw new RException(e);
        }  
  
        return t;  
    }
	
}
