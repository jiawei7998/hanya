package com.singlee.cap.bookkeeping.service;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.cap.bookkeeping.model.TbkEntry;

/**
 * 记账分录服务接口
 * @author Libo
 *
 */
public interface TbkEntryService {
	
	/**  获取新的流水号   */
	public String getNewFlowNo();

	/**
	 * 查询分录（分页）
	 * @author huqin
	 */
	public Page<List<TbkEntry>> getEntryList(Map<String,Object> map,RowBounds rb);
	
	/**
	 * 会计分录excel导出
	 */
	public Page<TbkEntry> getEntryList2(Map<String,Object> map,RowBounds rb);
	
	/**  保存分录   */
	public void save(TbkEntry entry);
	
	/**  保存分录   */
	public void save(List<TbkEntry> entryList);
	
	/**
	 * 查询分录
	 * @author huqin
	 */
	public List<TbkEntry> getDealEntryList(Map<String,Object> map);

  	/**
  	 * 查询需要送帐的套号
  	 * @param map
  	 * @return
  	 */
  	public List<TbkEntry> selectEntryFlowId(Map<String, Object> map);
  	
  	/**
  	 * 更新总账送帐状态
  	 * @param entry
  	 */
  	public void updateEntryState(TbkEntry entry);
  	
  	
  	/**
  	 * 根据时间范围查询 
  	 * 汪泽峰
  	 * @param map
  	 * @return
  	 */
  	public List<TbkEntry>  selectByTimeService(Map<String,Object> map);
  	
}
