package com.singlee.cap.bookkeeping.service;

import com.singlee.cap.accounting.model.InstructionPackage;
import com.singlee.cap.bookkeeping.model.TbkEntry;

import java.util.List;
import java.util.Map;

/**
 * 正常交易出账接口
 * @author shiting
 *
 */
public interface BookkeepingService {

	void createEntry(Map<String, Object> params);
	
	String generateEntry4Instruction(InstructionPackage instructionPackage);
	
	void checkInfoSceneEntry(InstructionPackage instructionPackage);
	
	void updateTbkSubjectBalanceSum(TbkEntry entry);
	
	void updateBookkeepingBalance(List<TbkEntry> entryList);
	/**
	 * 20180410 冲销接口
	 * @param map
	 * @return
	 */
	List<TbkEntry> reverseDealAndEntry(Map<String, Object> map);
}
