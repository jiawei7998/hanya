package com.singlee.cap.bookkeeping.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.cap.bookkeeping.model.TbkT24YearendInterface;
import com.singlee.capital.common.spring.mybatis.MyMapper;

public interface TbkT24YearendInterfaceMapper extends MyMapper<TbkT24YearendInterface>{

	 /**
	  * 查询分页
	 * @param map
	 * @param rb
	 * @return
	 */
	public Page<TbkT24YearendInterface>  selectByMap(Map<String,Object> map,RowBounds rb);
	
	/**
	 * 更新状态
	 * @param map
	 */
	public void updateInterfaceStatus(Map<String,Object> map);
}
