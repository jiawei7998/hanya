package com.singlee.cap.bookkeeping.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.cap.bookkeeping.model.TbkApprove;

import tk.mybatis.mapper.common.Mapper;


public interface TbkApproveMapper extends Mapper<TbkApprove>{

	public Page<TbkApprove> searchTbkApprove(Map<String, Object> params, RowBounds rb);
	
	public Page<TbkApprove> searchTbkApproveMySelf(Map<String, Object> params, RowBounds rb);
	
	public int updateTbkApproveStatus(Map<String, Object> params);
	
	public TbkApprove getTbkApproveById(Map<String, Object> params);
	
	public Page<TbkApprove> searchTbkApproveFinished(Map<String, Object> params, RowBounds rb);

	public int updateTbkApproveStatus2(TbkApprove tbkApprove);
}
