package com.singlee.cap.bookkeeping.util;

public class StringChgUtils {

	//把一个字符串中的小写转换为大写,大写字母前加_
	//如 taskId --> TASK_ID
	public static String exChange(String str){  
	    StringBuffer sb = new StringBuffer();  
	    if(str!=null){  
	        for(int i=0;i<str.length();i++){  
	            char c = str.charAt(i);  
	            if(Character.isUpperCase(c)){  
	                sb.append("_").append(c);  
	            }else if(Character.isLowerCase(c)){  
	                sb.append(Character.toUpperCase(c));   
	            }else{
	            	sb.append(c);
	            }
	        }  
	    }  
	      
	    return sb.toString();  
	} 

}
