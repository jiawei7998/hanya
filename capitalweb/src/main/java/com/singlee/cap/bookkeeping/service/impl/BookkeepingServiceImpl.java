package com.singlee.cap.bookkeeping.service.impl;

import com.singlee.cap.accounting.model.InstructionPackage;
import com.singlee.cap.accounting.model.TacTask;
import com.singlee.cap.accounting.service.AccountingTaskService;
import com.singlee.cap.base.mapper.TbkEventMapper;
import com.singlee.cap.base.model.*;
import com.singlee.cap.base.service.*;
import com.singlee.cap.bookkeeping.model.TbkDealBalance;
import com.singlee.cap.bookkeeping.model.TbkEntry;
import com.singlee.cap.bookkeeping.model.TbkSubjectBalance;
import com.singlee.cap.bookkeeping.model.TbkSubjectBalanceSum;
import com.singlee.cap.bookkeeping.service.*;
import com.singlee.cap.bookkeeping.util.StringChgUtils;
import com.singlee.capital.base.cal.LogManager;
import com.singlee.capital.base.service.ProductService;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.util.*;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.dict.service.DictionaryGetService;
import com.singlee.capital.eu.util.CollectionUtils;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.model.TtInstitution;
import com.singlee.capital.system.service.InstitutionService;
import com.singlee.capital.system.service.UserParamService;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class BookkeepingServiceImpl implements BookkeepingService{

	@Autowired
	private TBkSubjectBalanceService bkSubjectBalanceService;
	@Autowired
	private TbkEntryService bookkeepingEntryService;
	@Autowired
	private TbkParamsService tbkParamsService;
	@Autowired
	private TbkEntrySceneService tbkEntrySceneService;
	@Autowired
	private TbkEntrySettingService entrySettingServiceImpl;
	@Autowired
	private TbkAmountService tbkAmountService;
	@Autowired
	private TBkDealBalanceService tBkDealBalanceService;
	@Autowired
	private PretreatmentService pretreatmentService;
	@Autowired
	private TbkEventService tbkEventService;
	@Autowired
	private TBkSubjectBalanceSumService tBkSubjectBalanceSumService;
	@Autowired
	private TbkEntrySettingConfigService tbkEntrySettingConfigService;
	@Autowired
	private AccountingTaskService accountingTaskService;
	@Autowired
	private DayendDateService dayendDateService;
	@Autowired
	private TbkSubjectDefService tbkSubjectDefService;
	@Autowired
	private TbkProductEventService tbkProductEventService;
	@Autowired
	private DictionaryGetService dictionaryGetService;
	@Autowired
	InstitutionService institutionService;
	@Autowired
	private ProductService productService;
	@Autowired
	private TbkEventMapper tbkEventMapper;
	
	@Override
	public String generateEntry4Instruction(InstructionPackage instructionPackage){
		//表级锁
		tbkEventMapper.lockTbkEvent();
		
		//  基本校验
		if(instructionPackage == null){
			throw new RException("记账参数信息不能为空");
		}
		if(!productService.checkIsAcctAcup(instructionPackage.getInfoMap()))
		{
			return "000000";
		}
		
		// 预处理
		pretreatmentService.dealPretreatment(instructionPackage);
		
		LogManager.getLogger(LogManager.MODEL_BOOKKEEPING).info("核算接口业务信息map为："+instructionPackage.getInfoMap().toString());
		LogManager.getLogger(LogManager.MODEL_BOOKKEEPING).info("核算接口金额map为："+instructionPackage.getAmtMap().toString());
		//  生成记账分录
		String flowId ="";
		
		String eventType = ParameterUtil.getString(instructionPackage.getInfoMap(), "eventType", "");
		LogManager.getLogger(LogManager.MODEL_BOOKKEEPING).info("会计事件类型为："+eventType);
		if("".equals(eventType)){
			throw new RException("交易编号为："+ParameterUtil.getString(instructionPackage.getInfoMap(),"dealNo","")+"的交易，未找到对应的会计事件代码"); 
		}
		String eventId = "";
		
		TbkEvent event = new TbkEvent();
		event.setEventType(eventType);
		List<TbkEvent> eventList = tbkEventService.getTbkEventList(BeanUtil.beanToMap(event));//会计事件
		if(eventList.size() > 0){
			eventId = eventList.get(0).getEventType();
			instructionPackage.getInfoMap().put("eventId", eventId);
			TbkProductEvent prdEvent = new TbkProductEvent();
			prdEvent.setEventId(eventId);
			prdEvent.setPrdNo(ParameterUtil.getString(instructionPackage.getInfoMap(),"prdNo",""));
			List<TbkProductEvent> list = tbkProductEventService.getProductEventList(BeanUtil.beanToMap(prdEvent));
			if(list != null && list.size() > 0){
				List<TbkEntry> entryList = generateEntryByView(instructionPackage,eventList.get(0));
				CollectionUtils.sort(entryList, false, "flowSeq");
				if(entryList.size()> 0) {
					// 生成交易科目余额
					// 生成科目明细余额
					// 生成科目汇总余额
					updateBookkeepingBalance(entryList);
					bookkeepingEntryService.save(entryList);
					flowId = entryList.get(0).getFlowId();
				}
			}
		}else{
			throw new RException("交易编号为："+ParameterUtil.getString(instructionPackage.getInfoMap(),"dealNo","")+"的交易，未配置对应的事件"); 
		}
		return flowId;
	}
	
	public List<TbkEntry> generateEntryByView(InstructionPackage instructionPackage,TbkEvent event){
		//根据条件获取帐套信息
		TacTask task = new TacTask();
		task.setBkMain(","+ParameterUtil.getString(instructionPackage.getInfoMap(),"prdNo","")+",");
		task.setInstId(","+ParameterUtil.getString(instructionPackage.getInfoMap(),"sponInst","")+",");
		List<TacTask> taskList = accountingTaskService.selectEntryTaskList(BeanUtil.beanToMap(task));
		if(taskList != null && taskList.size() > 0){
			task = taskList.get(0);
		}else{
			taskList = accountingTaskService.selectEntryTaskList(null);
			task = taskList.get(0);
		}
		instructionPackage.getInfoMap().put("taskId", task.getTaskId());
		
		//查询交易所对应的所有维度信息
		String orders = null;
		List<TbkParams> paramsActiveList = tbkParamsService.getParamsActiveList(TbkParams.activeYes); //维度
		Map<String, Object> map = new HashMap<String, Object>();   //param map of TT_ENTRY_SCENE_INFO
		List<String> orderList = new ArrayList<String>();
		for(TbkParams params:paramsActiveList){
			map.put(StringChgUtils.exChange(params.getParamSource()), ParameterUtil.getString(instructionPackage.getInfoMap(),params.getParamId(),""));
			orderList.add(StringChgUtils.exChange(params.getParamSource())); //key集合
		}
		if(orderList != null && orderList.size() > 0) {
			orders = "order by " + StringUtils.join(orderList," asc , ") + " asc";
			}
		map.put("orders", orders);
		// 根据维度信息查询对应场景
		List<TbkEntrySceneInfo> ttEntrySceneInfoList = tbkEntrySceneService.getMatchedEntryScene(map); //场景
		if(ttEntrySceneInfoList == null || ttEntrySceneInfoList.size() == 0){
//			JY.require(ttEntrySceneInfoList == null || ttEntrySceneInfoList.size() == 0, "未找到对应的场景信息");
			throw new RException("交易编号为："+ParameterUtil.getString(instructionPackage.getInfoMap(),"dealNo","")+"的交易，未找到对应的场景信息");
		}
		List<TbkEntry> entryList = new ArrayList<TbkEntry>();
		String postDate = dayendDateService.getDayendDate();
		String eventId = ParameterUtil.getString(instructionPackage.getInfoMap(),"eventId","");
		
		String bkkpgOrgSource = dictionaryGetService.getTaDictByCodeAndKey("bkkpgOrgSource", "01").getDict_value();
		String bkkpgOrgId = null;
		if("1".equals(bkkpgOrgSource)){//机构
			TtInstitution inst = institutionService.getInstitutionById(ParameterUtil.getString(instructionPackage.getInfoMap(),"sponInst",""));
			if(inst != null){
				bkkpgOrgId = inst.getbInstId();
			}
		}else{
			bkkpgOrgId = task.getBkkpgOrgId();
		}

		String flowNo = bookkeepingEntryService.getNewFlowNo();
//		String settingGroupId = "";
		//会计分录
		List<TbkEntrySetting> ttBkEntrySettingList = entrySettingServiceImpl.getEntrySettingList(ttEntrySceneInfoList.get(0).getSceneId(),eventId);
		if(ttBkEntrySettingList.size() > 0){
			for(TbkEntrySetting ttBkEntrySetting: ttBkEntrySettingList) {
				
//				if (!settingGroupId.equals(ttBkEntrySetting.getSettingGroupId())) {
//					settingGroupId = ttBkEntrySetting.getSettingGroupId();
//					flowNo = bookkeepingEntryService.getNewFlowNo();
//				}
				TbkEntry entry = new TbkEntry();
				//自动映射同名属性
				try {
					org.springframework.beans.BeanUtils.copyProperties(ttBkEntrySetting, entry);
					BeanUtils.populate(entry, instructionPackage.getInfoMap());
				} catch (IllegalAccessException e1) {
					throw new RException(e1);
					//e1.printStackTrace();
				} catch (InvocationTargetException e1) {
					throw new RException(e1);
					//e1.printStackTrace();
				}
				if("99".equals(ParameterUtil.getString(instructionPackage.getInfoMap(),"prdNo",""))){
					entry.setPrdNo(ParameterUtil.getString(instructionPackage.getInfoMap(),"prdNo_1",""));
				}
				entry.setEventId(ttBkEntrySetting.getEventId());
				entry.setTaskId(task.getTaskId());
				entry.setFlowId(flowNo);
				entry.setPostDate(postDate);
				entry.setFlowSeq(String.valueOf(ttBkEntrySetting.getSort()));
				entry.setBkkpgOrgId(bkkpgOrgId);
				entry.setExpression(ttBkEntrySetting.getExpression());
				entry.setState(DictConstants.EntryState.NORMAL);
				entry.setSendFlag(DictConstants.EntrySendFlag.UnSend);
				entry.setUpdateTime(DateUtil.getCurrentDateTimeAsString());

				if(ttBkEntrySetting.getSubjCode().indexOf("&") == 0){// 科目是公式需解析    根据某几个要素确定动态科目
					String[] strs = ttBkEntrySetting.getSubjCode().split("&");
					String configType = "";
					TbkEntrySettingConfig configMap = new TbkEntrySettingConfig();
					for (int i = 0; i < strs.length; i++) {
						if(i == 1){
							configType = strs[i];
						}else if(i == 2){
							configMap.setEntrySubjType(strs[i]);
						}
					}
					if(configType.indexOf("+") > 0){
						String[] type = configType.split("\\+");
						for (int i = 0; i < type.length; i++) {
							if(i == 0){
								configMap.setEntryType(type[i]);
							}
							if(i == 1){
								configMap.setEntryType1(type[i]);
							}
							if(i == 2){
								configMap.setEntryType2(type[i]);
							}
						}
					}else{
						configMap.setEntryType(configType);
					}
					if(!"".equals(ParameterUtil.getString(instructionPackage.getInfoMap(), configMap.getEntryType(), ""))){
						configMap.setEntryTypeValue(ParameterUtil.getString(instructionPackage.getInfoMap(), configMap.getEntryType(), ""));
						configMap.setEntryType1Value(ParameterUtil.getString(instructionPackage.getInfoMap(), configMap.getEntryType1(), ""));
						configMap.setEntryType2Value(ParameterUtil.getString(instructionPackage.getInfoMap(), configMap.getEntryType2(), ""));
						List<TbkEntrySettingConfig> configList = tbkEntrySettingConfigService.getEntrySettingConfigList(BeanUtil.beanToMap(configMap));
						if(configList.size() > 0){
							entry.setSubjCode(configList.get(0).getSubjCode());
							entry.setSubjName(configList.get(0).getSubjName());
							entry.setInnerAcctSn(configList.get(0).getInnerAcctSn());
						}else{
							throw new RException("交易编号为："+ParameterUtil.getString(instructionPackage.getInfoMap(),"dealNo","")+"未找到对应的分录科目配置"); 
						}
					}else{
						throw new RException("交易编号为："+ParameterUtil.getString(instructionPackage.getInfoMap(),"dealNo","")+"未找到对应的分录科目配置"); 
					}
				}else if(ttBkEntrySetting.getSubjCode().indexOf("@") == 0){
					String subj = "";
					if(ttBkEntrySetting.getSubjCode().indexOf("@") == 0){
						subj = ttBkEntrySetting.getSubjCode().substring(1,ttBkEntrySetting.getSubjCode().length());
						String subjCode = ParameterUtil.getString(instructionPackage.getInfoMap(), subj, "");
						entry.setSubjCode(subjCode);
					}else{
						throw new RException("交易编号为："+ParameterUtil.getString(instructionPackage.getInfoMap(),"dealNo","")+"的交易,未找到参数科目为："+subj); 
					}
					if(ttBkEntrySetting.getInnerAcctSn()!= null && !"".equals(ttBkEntrySetting.getInnerAcctSn())){
						if(ttBkEntrySetting.getInnerAcctSn().indexOf("@") == 0){
							entry.setInnerAcctSn(ParameterUtil.getString(instructionPackage.getInfoMap(), ttBkEntrySetting.getInnerAcctSn().substring(1,ttBkEntrySetting.getInnerAcctSn().length()), ""));
						}
						if(ttBkEntrySetting.getSubjName().indexOf("@") == 0){
							String subjName = ttBkEntrySetting.getSubjName().split("\\@")[1];
							entry.setSubjName(ParameterUtil.getString(instructionPackage.getInfoMap(), subjName, ttBkEntrySetting.getSubjName().split("\\@")[2]));
						}
					}
				}else{
					entry.setInnerAcctSn(ttBkEntrySetting.getInnerAcctSn());
					entry.setCoreAcctCode(entry.getSubjCode());
				}
				Map<String, Object> calMap = new HashMap<String, Object>();
				calMap.put("amtMap", instructionPackage.getAmtMap());
				double value = 0;
				try {
					TbkAmount tbkAmount = tbkAmountService.getAmountById(ttBkEntrySetting.getExpression());
						JY.require(tbkAmount.getAmountFormula() != null, "交易编号为："+ParameterUtil.getString(instructionPackage.getInfoMap(),"dealNo","")+"未配置金额公式代码");
					value = calcBkIndex(calMap, tbkAmount.getAmountFormula()); //计算
				} catch (Exception e) {
					JY.error("",e);
				}
				if(DictConstants.DebitCreditConfigFlag.Debit.equals(ttBkEntrySetting.getDebitCreditFlag())){
					//借		取绝对值放借方
					entry.setDebitCreditFlag(DictConstants.DebitCreditFlag.Debit);
				}else if(DictConstants.DebitCreditConfigFlag.PositiveDebit.equals(ttBkEntrySetting.getDebitCreditFlag())){
					//借(+) PD		正数放借方，负数不出分录
					if(value > 0){
						entry.setDebitCreditFlag(DictConstants.DebitCreditFlag.Debit);
					}
				}else if(DictConstants.DebitCreditConfigFlag.MinusDebit.equals(ttBkEntrySetting.getDebitCreditFlag())){
					//借(-) MD		负数放借方，正数不出分录
					if(value < 0){
						entry.setDebitCreditFlag(DictConstants.DebitCreditFlag.Debit);
					}
				}else if(DictConstants.DebitCreditConfigFlag.Credit.equals(ttBkEntrySetting.getDebitCreditFlag())){
					//贷		取绝对值放贷方
					entry.setDebitCreditFlag(DictConstants.DebitCreditFlag.Credit);
				}else if(DictConstants.DebitCreditConfigFlag.PositiveCredit.equals(ttBkEntrySetting.getDebitCreditFlag())){
					//贷(+) PC	正数放贷方，负数不出分录
					if(value > 0){
						entry.setDebitCreditFlag(DictConstants.DebitCreditFlag.Credit);
					}
				}else if(DictConstants.DebitCreditConfigFlag.MinusCredit.equals(ttBkEntrySetting.getDebitCreditFlag())){
					//贷(-) MC	负数放贷方，正数不出分录
					if(value < 0){
						entry.setDebitCreditFlag(DictConstants.DebitCreditFlag.Credit);
					}
				}else if(DictConstants.DebitCreditConfigFlag.PositiveDebitMinusCredit.equals(ttBkEntrySetting.getDebitCreditFlag())){
					//借(+)/贷(-) PDMC	正数放借方，负数放贷方
					if(value > 0){
						entry.setDebitCreditFlag(DictConstants.DebitCreditFlag.Debit);
						entry.setFlowSeq(String.valueOf(ttBkEntrySetting.getSort()-1));
					}else{
						entry.setDebitCreditFlag(DictConstants.DebitCreditFlag.Credit);
						entry.setFlowSeq(String.valueOf(ttBkEntrySetting.getSort()+1));
					}
				}else if(DictConstants.DebitCreditConfigFlag.PositiveCreditMinusDebit.equals(ttBkEntrySetting.getDebitCreditFlag())){
					//贷(+)/借(-) PCMD	正数放贷方，负数放借方
					if(value > 0){
						entry.setDebitCreditFlag(DictConstants.DebitCreditFlag.Credit);
						entry.setFlowSeq(String.valueOf(ttBkEntrySetting.getSort()+1));
					}else{
						entry.setDebitCreditFlag(DictConstants.DebitCreditFlag.Debit);
						entry.setFlowSeq(String.valueOf(ttBkEntrySetting.getSort()-1));
					}
				}

				entry.setValue(MathUtil.round(Math.abs(value), 2));
				
				//分录中判断得到的借贷方向与分录配置中强制借贷方向不相同，需修改借贷方向，设置为红字
				if(ttBkEntrySetting.getEnforceDc() != null && !"".equals(ttBkEntrySetting.getEnforceDc())){
					if(!ttBkEntrySetting.getEnforceDc().equals(entry.getDebitCreditFlag())){//需强制借贷方向
						entry.setDebitCreditFlag(ttBkEntrySetting.getEnforceDc());
						entry.setRedFlag(DictConstants.redFlag.RED);
						entry.setValue(MathUtil.round(-Math.abs(value), 2));
					}
				}
				
				entryList.add(entry);
			}
			//检查借贷是否平衡
			BookkeepingUtil.adjustAndCheckEntryList(entryList);
		}else{
//				JY.require(ttBkEntrySettingList.size() <= 0, "未找到对应的分录配置");
			throw new RException("交易编号为："+ParameterUtil.getString(instructionPackage.getInfoMap(),"dealNo","")+"的交易，在场景:"+ttEntrySceneInfoList.get(0).getSceneId()+":"
					+ttEntrySceneInfoList.get(0).getSceneDescr()+"中未找到事件为："+event.getEventName()+"对应的分录配置"); 
		}
		return entryList;
	}
	
	/**
	 * 更新交易的科目余额
	 * @param entry
	 */
	public void updateDealEntryBal(TbkEntry entry){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("dealNo", entry.getDealNo());
		map.put("subjCode", entry.getSubjCode());
		List<TbkDealBalance> list = tBkDealBalanceService.selectByMap(map);
		TbkDealBalance balance = null;
		if(list.size() > 0){
			balance = list.get(0);
			if(DictConstants.DebitCreditFlag.Debit.equals(entry.getDebitCreditFlag())){
				balance.setDebitValue(balance.getDebitValue() + entry.getValue());
			}else if(DictConstants.DebitCreditFlag.Credit.equals(entry.getDebitCreditFlag())){
				balance.setCreditValue(balance.getCreditValue() + entry.getValue());
			}else if(DictConstants.DebitCreditFlag.Pay.equals(entry.getDebitCreditFlag())){
				balance.setPayValue(balance.getPayValue() + entry.getValue());
			}else if(DictConstants.DebitCreditFlag.Receive.equals(entry.getDebitCreditFlag())){
				balance.setReceiveValue(balance.getReceiveValue() + entry.getValue());
			}
			tBkDealBalanceService.updateBalance(balance);
		}else{
			balance = new TbkDealBalance();
			balance.setTaskId(entry.getTaskId());
			balance.setDealNo(entry.getDealNo());
			balance.setBkkpgOrgId(entry.getBkkpgOrgId());
			balance.setCcy(entry.getCcy());
			balance.setSubjCode(entry.getSubjCode());
			balance.setSubjName(entry.getSubjName());
//				balance.setExpression(entry.getExpression());
			if(DictConstants.DebitCreditFlag.Debit.equals(entry.getDebitCreditFlag())){
				balance.setDebitValue(entry.getValue());
			}else if(DictConstants.DebitCreditFlag.Credit.equals(entry.getDebitCreditFlag())){
				balance.setCreditValue(entry.getValue());
			}else if(DictConstants.DebitCreditFlag.Pay.equals(entry.getDebitCreditFlag())){
				balance.setPayValue(entry.getValue());
			}else if(DictConstants.DebitCreditFlag.Receive.equals(entry.getDebitCreditFlag())){
				balance.setReceiveValue(entry.getValue());
			}
			
			balance.setLstDate(DateUtil.getCurrentDateTimeAsString());
			tBkDealBalanceService.insert(balance);
		}
	}
	
	/**
	 * 根据分录更新余额
	 * @param entryList
	 */
	@Override
	public void updateBookkeepingBalance(List<TbkEntry> entryList){
		for(TbkEntry entry : entryList){
			if(entry.getDealNo() != null && !"".equals(entry.getDealNo())){
				updateDealEntryBal(entry);
			}
			updateBookkeepingBalance(entry);
			updateTbkSubjectBalanceSum(entry);
		}
	}

	/**
	 * 通过分录更新科目余额
	 * @param entry
	 */
	private void updateBookkeepingBalance(TbkEntry entry){
		
		TbkSubjectBalance balance = bkSubjectBalanceService.findOrCreateByEntry(entry);
		if(DictConstants.DebitCreditFlag.Debit.equals(entry.getDebitCreditFlag())){
			balance.setDebitChg(balance.getDebitChg() + entry.getValue());
			balance.setDebitValue(balance.getDebitValue() + entry.getValue());
		}else if(DictConstants.DebitCreditFlag.Credit.equals(entry.getDebitCreditFlag())){
			balance.setCreditChg(balance.getCreditChg() + entry.getValue());
			balance.setCreditValue(balance.getCreditValue() + entry.getValue());
		}else if(DictConstants.DebitCreditFlag.Pay.equals(entry.getDebitCreditFlag())){
			balance.setPayChg(balance.getPayChg() + entry.getValue());
			balance.setPayValue(balance.getPayValue() + entry.getValue());
		}else if(DictConstants.DebitCreditFlag.Receive.equals(entry.getDebitCreditFlag())){
			balance.setReceiveChg(balance.getReceiveChg() + entry.getValue());
			balance.setReceiveValue(balance.getReceiveValue() + entry.getValue());
		}else{
			throw new RException("分录借贷方向错误：" + entry);
		}
		
		if(DictConstants.DebitCreditFlag.Debit.equals(balance.getBalanceDirect())){
			balance.setDebitValue(balance.getDebitValue() - balance.getCreditValue());
			balance.setCreditValue(0);
		}else if(DictConstants.DebitCreditFlag.Credit.equals(balance.getBalanceDirect())){
			balance.setCreditValue(balance.getCreditValue() - balance.getDebitValue());
			balance.setDebitValue(0);
		}else if(DictConstants.DebitCreditFlag.Receive.equals(balance.getBalanceDirect())){
			balance.setReceiveValue(balance.getReceiveValue() - balance.getPayValue());
			balance.setPayValue(0);
		}else if(DictConstants.DebitCreditFlag.Pay.equals(balance.getBalanceDirect())){
			balance.setPayValue(balance.getPayValue() - balance.getReceiveValue());
			balance.setReceiveValue(0);
		}else{
			balance.setDebitValue(balance.getDebitValue() - balance.getCreditValue());
			balance.setCreditValue(0);
			balance.setPayValue(balance.getPayValue() - balance.getReceiveValue());
			balance.setReceiveValue(0);
			
			if(balance.getDebitValue() < 0){
				balance.setCreditValue(-balance.getDebitValue());
				balance.setDebitValue(0);
			}
			if(balance.getPayValue() < 0 ){
				balance.setReceiveValue(-balance.getPayValue());
				balance.setPayValue(0);
			}
		}
		
		if (StringUtil.isEmpty(balance.getDbId())) {
			bkSubjectBalanceService.insert(balance);
		} else {
			bkSubjectBalanceService.updateBalance(balance);
		}
	}
	/**
	 * 更新科目余额汇总
	 * @param entry
	 */
	@Override
	public void updateTbkSubjectBalanceSum(TbkEntry entry){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("taskId", entry.getTaskId());
		map.put("subjCode", entry.getSubjCode());
		List<TbkSubjectDef> defList = tbkSubjectDefService.getTbkSubjectAndParentList(map);
		for (TbkSubjectDef tbkSubjectDef : defList) {
			Map<String, Object> balMap = new HashMap<String, Object>();
			balMap.put("taskId", entry.getTaskId());
			balMap.put("bkkpgOrgId", entry.getBkkpgOrgId());
			balMap.put("ccy", entry.getCcy());
			balMap.put("subjCode", tbkSubjectDef.getSubjCode());
			List<TbkSubjectBalanceSum> list = tBkSubjectBalanceSumService.selectByMap(balMap);
			TbkSubjectBalanceSum balance = null;
			if(list.size() > 0){
				balance = list.get(0);
				if(tbkSubjectDef.getDebitCredit() != null && !"".equals(tbkSubjectDef.getDebitCredit())){
					if(tbkSubjectDef.getDebitCredit().equals(DictConstants.debitCredit.Debit)){
						if(DictConstants.DebitCreditFlag.Debit.equals(entry.getDebitCreditFlag())){
							balance.setDebitValue(balance.getDebitValue() + entry.getValue());
							balance.setDebitChg(balance.getDebitChg() + entry.getValue());
						}else if(DictConstants.DebitCreditFlag.Credit.equals(entry.getDebitCreditFlag())){
							balance.setDebitValue(balance.getDebitValue() - entry.getValue());
							balance.setCreditChg(balance.getCreditChg() + entry.getValue());
						}
					}else if(tbkSubjectDef.getDebitCredit().equals(DictConstants.debitCredit.Credit)){
						if(DictConstants.DebitCreditFlag.Debit.equals(entry.getDebitCreditFlag())){
							balance.setCreditValue(balance.getCreditValue() - entry.getValue());
							balance.setDebitChg(balance.getDebitChg() + entry.getValue());
						}else if(DictConstants.DebitCreditFlag.Credit.equals(entry.getDebitCreditFlag())){
							balance.setCreditValue(balance.getCreditValue() + entry.getValue());
							balance.setCreditChg(balance.getCreditChg() + entry.getValue());
						}
					}else{
						if(DictConstants.DebitCreditFlag.Debit.equals(entry.getDebitCreditFlag())){
							balance.setDebitValue(balance.getDebitValue() + entry.getValue());
							balance.setDebitChg(balance.getDebitChg() + entry.getValue());
						}else if(DictConstants.DebitCreditFlag.Credit.equals(entry.getDebitCreditFlag())){
							balance.setDebitValue(balance.getDebitValue() - entry.getValue());
							balance.setCreditChg(balance.getCreditChg() + entry.getValue());
						}
					}
				}else{
					if(DictConstants.DebitCreditFlag.Debit.equals(entry.getDebitCreditFlag())){
						balance.setDebitValue(balance.getDebitValue() + entry.getValue());
						balance.setDebitChg(balance.getDebitChg() + entry.getValue());
					}else if(DictConstants.DebitCreditFlag.Credit.equals(entry.getDebitCreditFlag())){
						balance.setDebitValue(balance.getDebitValue() - entry.getValue());
						balance.setCreditChg(balance.getCreditChg() + entry.getValue());
					}
				}
					
				if(tbkSubjectDef.getPayRecive() != null && !"".equals(tbkSubjectDef.getPayRecive())){
					if(tbkSubjectDef.getPayRecive().equals(DictConstants.payRecive.P)){
						if(DictConstants.DebitCreditFlag.Pay.equals(entry.getDebitCreditFlag())){
							balance.setPayChg(balance.getPayChg()+entry.getValue());
							balance.setPayValue(balance.getPayValue() + entry.getValue());
						}else if(DictConstants.DebitCreditFlag.Receive.equals(entry.getDebitCreditFlag())){
							balance.setPayChg(balance.getPayChg() - entry.getValue());
							balance.setReceiveChg(balance.getReceiveChg() + entry.getValue());
						}
					}else if(tbkSubjectDef.getPayRecive().equals(DictConstants.payRecive.P)){
						if(DictConstants.DebitCreditFlag.Pay.equals(entry.getDebitCreditFlag())){
							balance.setReceiveValue(balance.getReceiveValue() - entry.getValue());
							balance.setPayValue(balance.getPayValue() + entry.getValue());
						}else if(DictConstants.DebitCreditFlag.Receive.equals(entry.getDebitCreditFlag())){
							balance.setReceiveValue(balance.getReceiveValue() + entry.getValue());
							balance.setReceiveChg(balance.getReceiveChg() + entry.getValue());
						}
					}else{
						if(DictConstants.DebitCreditFlag.Pay.equals(entry.getDebitCreditFlag())){
							balance.setPayChg(balance.getPayChg()+entry.getValue());
							balance.setPayValue(balance.getPayValue() + entry.getValue());
						}else if(DictConstants.DebitCreditFlag.Receive.equals(entry.getDebitCreditFlag())){
							balance.setPayChg(balance.getPayChg() - entry.getValue());
							balance.setReceiveChg(balance.getReceiveChg() + entry.getValue());
						}
					}
				}else{
					if(DictConstants.DebitCreditFlag.Pay.equals(entry.getDebitCreditFlag())){
						balance.setPayChg(balance.getPayChg()+entry.getValue());
						balance.setPayValue(balance.getPayValue() + entry.getValue());
					}else if(DictConstants.DebitCreditFlag.Receive.equals(entry.getDebitCreditFlag())){
						balance.setPayChg(balance.getPayChg() - entry.getValue());
						balance.setReceiveChg(balance.getReceiveChg() + entry.getValue());
					}
				}
				balance.setUpdateTime(DateUtil.getCurrentDateTimeAsString());
				tBkSubjectBalanceSumService.updateBalance(BeanUtil.beanToMap(balance));
			}else{
				balance = new TbkSubjectBalanceSum();
				balance.setTaskId(entry.getTaskId());
				balance.setCcy(entry.getCcy());
				balance.setBkkpgOrgId(entry.getBkkpgOrgId());
				balance.setSubjCode(tbkSubjectDef.getSubjCode());
				balance.setSubjName(tbkSubjectDef.getSubjName());
				if(tbkSubjectDef.getDebitCredit() != null && !"".equals(tbkSubjectDef.getDebitCredit())){
					if(tbkSubjectDef.getDebitCredit().equals(DictConstants.debitCredit.Debit)){
						if(DictConstants.DebitCreditFlag.Debit.equals(entry.getDebitCreditFlag())){
							balance.setDebitValue(balance.getDebitValue() + entry.getValue());
							balance.setDebitChg(balance.getDebitChg() + entry.getValue());
						}else if(DictConstants.DebitCreditFlag.Credit.equals(entry.getDebitCreditFlag())){
							balance.setDebitValue(balance.getDebitValue() - entry.getValue());
							balance.setCreditChg(balance.getCreditChg() + entry.getValue());
						}
					}else if(tbkSubjectDef.getDebitCredit().equals(DictConstants.debitCredit.Credit)){
						if(DictConstants.DebitCreditFlag.Debit.equals(entry.getDebitCreditFlag())){
							balance.setCreditValue(balance.getCreditValue() - entry.getValue());
							balance.setDebitChg(balance.getDebitChg() + entry.getValue());
						}else if(DictConstants.DebitCreditFlag.Credit.equals(entry.getDebitCreditFlag())){
							balance.setCreditValue(balance.getCreditValue() + entry.getValue());
							balance.setCreditChg(balance.getCreditChg() + entry.getValue());
						}
					}else{
						if(DictConstants.DebitCreditFlag.Debit.equals(entry.getDebitCreditFlag())){
							balance.setDebitValue(balance.getDebitValue() + entry.getValue());
							balance.setDebitChg(balance.getDebitChg() + entry.getValue());
						}else if(DictConstants.DebitCreditFlag.Credit.equals(entry.getDebitCreditFlag())){
							balance.setDebitValue(balance.getDebitValue() - entry.getValue());
							balance.setCreditChg(balance.getCreditChg() + entry.getValue());
						}
					}
				}else{
					if(DictConstants.DebitCreditFlag.Debit.equals(entry.getDebitCreditFlag())){
						balance.setDebitValue(balance.getDebitValue() + entry.getValue());
						balance.setDebitChg(balance.getDebitChg() + entry.getValue());
					}else if(DictConstants.DebitCreditFlag.Credit.equals(entry.getDebitCreditFlag())){
						balance.setDebitValue(balance.getDebitValue() - entry.getValue());
						balance.setCreditChg(balance.getCreditChg() + entry.getValue());
					}
				}
					
				if(tbkSubjectDef.getPayRecive() != null && !"".equals(tbkSubjectDef.getPayRecive())){
					if(tbkSubjectDef.getPayRecive().equals(DictConstants.payRecive.P)){
						if(DictConstants.DebitCreditFlag.Pay.equals(entry.getDebitCreditFlag())){
							balance.setPayChg(balance.getPayChg()+entry.getValue());
							balance.setPayValue(balance.getPayValue() + entry.getValue());
						}else if(DictConstants.DebitCreditFlag.Receive.equals(entry.getDebitCreditFlag())){
							balance.setPayChg(balance.getPayChg() - entry.getValue());
							balance.setReceiveChg(balance.getReceiveChg() + entry.getValue());
						}
					}else if(tbkSubjectDef.getPayRecive().equals(DictConstants.payRecive.P)){
						if(DictConstants.DebitCreditFlag.Pay.equals(entry.getDebitCreditFlag())){
							balance.setReceiveValue(balance.getReceiveValue() - entry.getValue());
							balance.setPayValue(balance.getPayValue() + entry.getValue());
						}else if(DictConstants.DebitCreditFlag.Receive.equals(entry.getDebitCreditFlag())){
							balance.setReceiveValue(balance.getReceiveValue() + entry.getValue());
							balance.setReceiveChg(balance.getReceiveChg() + entry.getValue());
						}
					}else{
						if(DictConstants.DebitCreditFlag.Pay.equals(entry.getDebitCreditFlag())){
							balance.setPayChg(balance.getPayChg()+entry.getValue());
							balance.setPayValue(balance.getPayValue() + entry.getValue());
						}else if(DictConstants.DebitCreditFlag.Receive.equals(entry.getDebitCreditFlag())){
							balance.setPayChg(balance.getPayChg() - entry.getValue());
							balance.setReceiveChg(balance.getReceiveChg() + entry.getValue());
						}
					}
				}else{
					if(DictConstants.DebitCreditFlag.Pay.equals(entry.getDebitCreditFlag())){
						balance.setPayChg(balance.getPayChg()+entry.getValue());
						balance.setPayValue(balance.getPayValue() + entry.getValue());
					}else if(DictConstants.DebitCreditFlag.Receive.equals(entry.getDebitCreditFlag())){
						balance.setPayChg(balance.getPayChg() - entry.getValue());
						balance.setReceiveChg(balance.getReceiveChg() + entry.getValue());
					}
				}
				balance.setUpdateTime(DateUtil.getCurrentDateTimeAsString());
				tBkSubjectBalanceSumService.insert(balance);
			}
		}
	}
	
	
	@Override
	public void createEntry(@RequestBody Map<String, Object> params) {
		InstructionPackage inst = new InstructionPackage();
		inst.setInfoMap(params);
		this.generateEntryByView(inst,null);
	}

	private static ScriptEngineManager manager = new ScriptEngineManager(); 
	private static ScriptEngine engine;
	/**
	 * 计算记账指标
	 * @param map
	 * @param expression
	 * @return
	 */
	public static double calcBkIndex(Map<String, Object> map, String expression){
		if(engine == null) {
			engine =  manager.getEngineByName("js");
		}
		for(String key:map.keySet()){
			engine.put(key, map.get(key));
		}
		try {
			Object val = engine.eval(expression);
			if(val != null){
				return Double.parseDouble(val.toString());
			}
		} catch (Exception e) {
			//e.printStackTrace();
			throw new RException(e);
		}
		return 0;
	}

	@Override
	public void checkInfoSceneEntry(InstructionPackage instructionPackage) {
	//  基本校验
		if(instructionPackage == null){
			throw new RException("记账参数信息不能为空");
		}
		// 预处理
		pretreatmentService.dealPretreatment(instructionPackage);
		
		String eventType = ParameterUtil.getString(instructionPackage.getInfoMap(), "eventType", "");
		if("".equals(eventType)){
			throw new RException("交易编号为："+ParameterUtil.getString(instructionPackage.getInfoMap(),"dealNo","")+"的交易，未找到对应的会计事件代码"); 
		}
		String eventId = "";
		
		TbkEvent event = new TbkEvent();
		event.setEventType(eventType);
		List<TbkEvent> eventList = tbkEventService.getTbkEventList(BeanUtil.beanToMap(event));//会计事件
		if(eventList.size() > 0){
			eventId = eventList.get(0).getEventType();
			instructionPackage.getInfoMap().put("eventId", eventId);
			TbkProductEvent prdEvent = new TbkProductEvent();
			prdEvent.setEventId(eventId);
			prdEvent.setPrdNo(ParameterUtil.getString(instructionPackage.getInfoMap(),"prdNo",""));
			List<TbkProductEvent> list = tbkProductEventService.getProductEventList(BeanUtil.beanToMap(prdEvent));
			if(list != null && list.size() > 0){
				//查询交易所对应的所有维度信息
				String orders = null;
				List<TbkParams> paramsActiveList = tbkParamsService.getParamsActiveList(TbkParams.activeYes); //维度
				Map<String, Object> map = new HashMap<String, Object>();   //param map of TT_ENTRY_SCENE_INFO
				List<String> orderList = new ArrayList<String>();
				for(TbkParams params:paramsActiveList){
					map.put(StringChgUtils.exChange(params.getParamSource()), ParameterUtil.getString(instructionPackage.getInfoMap(),params.getParamId(),""));
					orderList.add(StringChgUtils.exChange(params.getParamSource())); //key集合
				}
				if(orderList != null && orderList.size() > 0) {
					orders = "order by " + StringUtils.join(orderList," asc , ") + " asc";}
				map.put("orders", orders);
				// 根据维度信息查询对应场景
				List<TbkEntrySceneInfo> ttEntrySceneInfoList = tbkEntrySceneService.getMatchedEntryScene(map); //场景
				if(ttEntrySceneInfoList == null || ttEntrySceneInfoList.size() == 0){
//					JY.require(ttEntrySceneInfoList == null || ttEntrySceneInfoList.size() == 0, "未找到对应的场景信息");
					throw new RException("该笔交易未找到对应的场景，请检查会计四分类、收息方式等交易要素！");
				}
				//会计分录
				List<TbkEntrySetting> ttBkEntrySettingList = entrySettingServiceImpl.getEntrySettingList(ttEntrySceneInfoList.get(0).getSceneId(),eventId);
				if(ttBkEntrySettingList.size() > 0){
					for(TbkEntrySetting ttBkEntrySetting: ttBkEntrySettingList) {
						if(ttBkEntrySetting.getSubjCode().indexOf("&") == 0){// 科目是公式需解析    根据某几个要素确定动态科目
							String[] strs = ttBkEntrySetting.getSubjCode().split("&");
							String configType = "";
							TbkEntrySettingConfig configMap = new TbkEntrySettingConfig();
							for (int i = 0; i < strs.length; i++) {
								if(i == 1){
									configType = strs[i];
								}else if(i == 2){
									configMap.setEntrySubjType(strs[i]);
								}
							}
							if(configType.indexOf("+") > 0){
								String[] type = configType.split("\\+");
								for (int i = 0; i < type.length; i++) {
									if(i == 0){
										configMap.setEntryType(type[i]);
									}
									if(i == 1){
										configMap.setEntryType1(type[i]);
									}
									if(i == 2){
										configMap.setEntryType2(type[i]);
									}
								}
							}else{
								configMap.setEntryType(configType);
							}
							if(!"".equals(ParameterUtil.getString(instructionPackage.getInfoMap(), configMap.getEntryType(), ""))){
								configMap.setEntryTypeValue(ParameterUtil.getString(instructionPackage.getInfoMap(), configMap.getEntryType(), ""));
								configMap.setEntryType1Value(ParameterUtil.getString(instructionPackage.getInfoMap(), configMap.getEntryType1(), ""));
								configMap.setEntryType2Value(ParameterUtil.getString(instructionPackage.getInfoMap(), configMap.getEntryType2(), ""));
								List<TbkEntrySettingConfig> configList = tbkEntrySettingConfigService.getEntrySettingConfigList(BeanUtil.beanToMap(configMap));
								if(configList.size() <= 0){
									throw new RException("该笔交易未找到对应的分录科目配置，请检查教的的客户类型或者再保理类型等交易要素！"); 
								}
							}else{
								throw new RException("该笔交易未找到对应的分录科目配置，请检查教的的客户类型或者再保理类型等交易要素！"); 
							}
						}
					}
				}else{
					throw new RException("该笔交易在场景:"+ttEntrySceneInfoList.get(0).getSceneId()+":"
							+ttEntrySceneInfoList.get(0).getSceneDescr()+"中未找到事件为："+eventList.get(0).getEventName()+"对应的分录配置"); 
				}
			}
		}else{
			throw new RException("该笔交易未找到对应的会计事件代码！"); 
		}
	}
	/**
	 * 20180410  交易冲销接口
	 */
	@Autowired
	private UserParamService userParamService;
	@Override
	public List<TbkEntry> reverseDealAndEntry(Map<String, Object> map){
		String redFlag = userParamService.getSysParamByName("redFlag","R");//默认红字
		String flowId = "";
		String oldFlowId = "";
		List<TbkEntry> list = null;
		List<TbkEntry> entryList = bookkeepingEntryService.getDealEntryList(map);
		if(entryList.size() > 0){
			list = new ArrayList<TbkEntry>();
			for (TbkEntry tbkEntry : entryList) {
				tbkEntry.setEntryId("");
				if(!oldFlowId.equals(tbkEntry.getFlowId())){
					oldFlowId = tbkEntry.getFlowId();
					flowId = bookkeepingEntryService.getNewFlowNo();
				}
				tbkEntry.setFlowId(flowId);
				if(redFlag.equals(DictConstants.redFlag.RED)){
					tbkEntry.setValue(-1*tbkEntry.getValue());
				}else{
					if(tbkEntry.getDebitCreditFlag().equals(DictConstants.DebitCreditFlag.Credit)){
						tbkEntry.setDebitCreditFlag(DictConstants.DebitCreditFlag.Debit);
					}else if(tbkEntry.getDebitCreditFlag().equals(DictConstants.DebitCreditFlag.Debit)){
						tbkEntry.setDebitCreditFlag(DictConstants.DebitCreditFlag.Credit);
					}else if(tbkEntry.getDebitCreditFlag().equals(DictConstants.DebitCreditFlag.Pay)){
						tbkEntry.setDebitCreditFlag(DictConstants.DebitCreditFlag.Receive);
					}else if(tbkEntry.getDebitCreditFlag().equals(DictConstants.DebitCreditFlag.Receive)){
						tbkEntry.setDebitCreditFlag(DictConstants.DebitCreditFlag.Pay);
					}
				}
				tbkEntry.setEventId("7001");
				tbkEntry.setSendFlag(DictConstants.EntrySendFlag.UnSend);
				tbkEntry.setUpdateTime(DateUtil.getCurrentCompactDateTimeAsString());
				tbkEntry.setPostDate(dayendDateService.getDayendDate());
				list.add(tbkEntry);
			}
			if(list.size()>0){
				updateBookkeepingBalance(list);
				bookkeepingEntryService.save(list);
			}
		}
		return list;
	}

	
}
