package com.singlee.cap.bookkeeping.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 407账号时点余额历史表
 * @author Administrator
 *
 */
@Entity
@Table(name="ACCTNO407_WORKINGBAL_H")
public class Acctno407WorkingbalH implements Serializable {
	  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String   post_date   ;//账务日期
    private String   subj_code   ;//科目号
    private String   acct_no     ;//帐号
    private String   ccy         ;//币种
    private String   medium_type ;//帐号类型
    private String   inst_id     ;//机构号
    private double   amt         ;//时点余额
	public String getPost_date() {
		return post_date;
	}
	public void setPost_date(String postDate) {
		post_date = postDate;
	}
	public String getSubj_code() {
		return subj_code;
	}
	public void setSubj_code(String subjCode) {
		subj_code = subjCode;
	}
	public String getAcct_no() {
		return acct_no;
	}
	public void setAcct_no(String acctNo) {
		acct_no = acctNo;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public String getMedium_type() {
		return medium_type;
	}
	public void setMedium_type(String mediumType) {
		medium_type = mediumType;
	}
	public String getInst_id() {
		return inst_id;
	}
	public void setInst_id(String instId) {
		inst_id = instId;
	}
	public double getAmt() {
		return amt;
	}
	public void setAmt(double amt) {
		this.amt = amt;
	}
    
    

}
