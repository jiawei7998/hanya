package com.singlee.cap.bookkeeping.service;

import java.util.List;
import java.util.Map;

import com.singlee.cap.accounting.model.InstructionPackage;
import com.singlee.cap.base.model.TbkEntrySceneAcup;

/**
 * IFRS 9 会计核算匹配引擎
 * @author louhuanqing
 *
 */


public interface Ifrs9BookkeepingService {

	/**
	 *  select s.d_id,s.scene_id,s.db_id,wm_concat(s.p_id||':'||s.p_value|| '  ' ) as paramsString from tbk_entry_scene_detail s 
		left join tbk_product_event e on e.db_id=s.DB_ID  where s.d_id in (
		select res.d_id from (
		select d_id,wm_concat(p_id) as p_id,wm_concat(p_value) as p_value  from (
		(select d.d_id,d.p_id,d.p_value,d.db_id,d.scene_id from tbk_entry_scene_detail d left join tbk_params p on p.param_source=d.p_id order by d.d_id,p.param_qz)
		)
		group by d_id
		) res where res.p_id='intFre,paramI0,invType,pClass,intType' and res.p_value='12,2,1,288,2'
		) and e.event_id='1206' group by s.d_id,s.scene_id,s.db_id
		tbk_params  会计维度排序规则（权重）param_qz
		tbk_entry_scene_detail  会计维度KEY-VALUE 扩展模式
		wm_concat 构建维度排序列表 和 对应维度列表的值列表
		
		paramsMap:参数说明
		pIdString:  'intFre,paramI0,invType,pClass,intType'
		pValueString: '12,2,1,288,2'
		eventId: '1206'
		通过给出的维度串，和维度值串  + 触发事件   得到会计维度场景配置
		
		通过select t.* from tbk_entry_scene_acup t
		where t.scene_id
		in
	 	<foreach collection="list" index="index" item="item" open="(" separator="," close=")">
           #{item.scene_id}
        </foreach>
        获得所需要分录配置
	 */
	
	public List<TbkEntrySceneAcup> getIfrs9SceneByConfigure(Map<String, Object> paramsMap);
	/**
	 * 通过带入产品的代码获得约束会计引擎的维度串和维度值
	 * 返回维度串取值 Map<String, Object>
	 * pIdString:  'intFre,paramI0,invType,pClass,intType'
		pValueString: '12,2,1,288,2'
	 * Map<String, Object> beanPropertysMap  主体javabean
	 * 必须包含 prdNo
	 * @return
	 */
	public Map<String, Object> getIfrs9ParamsString(Map<String, Object> beanPropertysMap);
	
	/**
	 * 会计分录统一路口
	 * @param instructionPackage
	 * @return
	 */
	public String generateEntry4Instruction(InstructionPackage instructionPackage);
	/**
	 * 自动化处理转换IAS9  --> IFRS9
	 * @return
	 */
	public boolean tranferIas9ToIfrs9AutoService() ;
}
