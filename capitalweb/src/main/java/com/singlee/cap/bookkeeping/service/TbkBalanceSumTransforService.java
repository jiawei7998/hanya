package com.singlee.cap.bookkeeping.service;

import java.util.List;
import java.util.Map;

import com.singlee.cap.bookkeeping.model.TbkBalanceSumTransfor;

public interface TbkBalanceSumTransforService {
	/**
	   * 根据条件查询专用表
	   * @param map
	   * @return
	   */
	public List<TbkBalanceSumTransfor> selectTbkBalanceSumTransforListService(Map<String,Object> map);

	  /**
	   * 修改 
	   * @param map
	   */
	  public void updateBalanceSumTransforService(Map<String, Object> map);
}
