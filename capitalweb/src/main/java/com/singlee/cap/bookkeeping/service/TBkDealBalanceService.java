package com.singlee.cap.bookkeeping.service;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.cap.bookkeeping.model.QueryBalanceModel;
import com.singlee.cap.bookkeeping.model.TbkDealBalance;


/**
 * 交易科目余额的服务接口
 * @author shiting
 *
 */
public interface TBkDealBalanceService {
	
	void insert(TbkDealBalance balance);
	/**
	 * 更新余额，只更新四个数字余额（借、贷、支、付）
	 * @param balance
	 */
	public void updateBalance(TbkDealBalance balance);
	
	public Page<TbkDealBalance> selectByMap(Map<String,Object> map,RowBounds rb);

	public List<TbkDealBalance> selectByMap(Map<String, Object> map);
	
	/**
	 * 基金科目余额查询
	 * @param map
	 * @return
	 */
	public Page<QueryBalanceModel> queryFundBal(Map<String,Object> map);
	
	/**
	 * 产品科目余额查询
	 * @param map
	 * @return
	 */
	public Page<QueryBalanceModel> queryProductBal(Map<String,Object> map);
}
