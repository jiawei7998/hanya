package com.singlee.cap.bookkeeping.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.cap.bookkeeping.model.TbkEntry;
import com.singlee.cap.bookkeeping.model.TbkSubjectBalanceSum;


/**
 * 记账余额的服务接口
 * @author shiting
 *
 */
public interface TBkSubjectBalanceSumService {
	
	/**
	 * 根据分录查找相应的余额，如查询不到，则创建新的并返回
	 * @param entry
	 */
	public TbkSubjectBalanceSum findOrCreateByEntry(TbkEntry entry);
	
	void insert(TbkSubjectBalanceSum balance);
	/**
	 * 更新余额，只更新四个数字余额（借、贷、支、付）
	 * @param balance
	 */
	public void updateBalance(Map<String, Object> map);
	
	public Page<TbkSubjectBalanceSum> selectByMap(Map<String,Object> map,RowBounds rb);

	List<TbkSubjectBalanceSum> selectByMap(Map<String, Object> map);

	/**
	 * 更新上日科目余额
	 * @param balance
	 */
	public void updateBeforeBalance();
	public List<TbkSubjectBalanceSum> getAllDiffListService(Map<String,Object> map);

	/**
	 * 407对账
	 */
	public Page<TbkSubjectBalanceSum> balanceOfAccountFor407(HashMap<String,Object> map,RowBounds rb);

}
