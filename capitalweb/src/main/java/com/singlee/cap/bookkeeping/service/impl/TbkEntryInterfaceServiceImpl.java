package com.singlee.cap.bookkeeping.service.impl;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.cap.base.model.TbkScenceSubject;
import com.singlee.cap.base.service.TbkScenceSubjectService;
import com.singlee.cap.bookkeeping.model.TbkEntry;
import com.singlee.cap.bookkeeping.service.TbkEntryInterfaceService;
import com.singlee.cap.bookkeeping.service.TbkEntryService;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.interfacex.socketservice.FileReadWriteService;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TbkEntryInterfaceServiceImpl implements TbkEntryInterfaceService {
	
	@Autowired
	private TbkEntryService tbkEntryService ;
	@Autowired
	private TbkScenceSubjectService tbkScenceSubjectService;
	
	
	@Override
	
	public RetMsg<Object> GtpTbkEntryOutputDataWrite(Map<String, Object> map) {
		//获取已配置的产品科目
		map.put("pageSize", 9999); map.put("pageNumber", 1);
		List<TbkScenceSubject> list1 = tbkScenceSubjectService.showAllTbkScenceSubject(map);
		
		Map<String, Object> map2 = new HashMap<String, Object>();
//		map2.put("postDate1", "2017-08-30");
//		map2.put("postDate2", "2017-09-05");
		if(map.get("time1")!=null && map.get("time2")!= null){
			map2.put("postDate1", map.get("time1"));
			map2.put("postDate2", map.get("time2"));
		}
		//获取所有的tbkEntry
		List<TbkEntry> listt = tbkEntryService.selectByTimeService(map2);
		List<TbkEntry> list = new ArrayList<TbkEntry>();
		//过滤出已配置产品科目的tbkEntry
		for (TbkEntry tbkEntry : listt) {
			for (TbkScenceSubject tss : list1) {
				if(tbkEntry.getPrdNo().equals(tss.getPrdNo())&&tbkEntry.getSubjCode().equals(tss.getSubjCode()))
				{
					list.add(tbkEntry);
				}
			}
		}
		
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd_HHmmss");
		//,
		String split    = ",";
		String filepath = "c:/";
		String filebak  = "c:/my/";
		//IFBM_OUTPUT_DATA_
		//String indexof  = dictionaryGetService.getTaDictByCodeAndKey("ifbmCrmsGtpQuota", "04").getDict_value();
		String encoding  = "UTF-8";
		String filename = "TX_OUTPUT_DATA_"+format.format(new Date())+".csv";
		StringBuffer buffer = null;
		StringBuffer allbuffer = new StringBuffer();
		
		if(list.size()>0){
			StringBuffer buffer1 = new StringBuffer();
			buffer1.append(StringUtils.trimToEmpty("TX_ID")).append(split);
			buffer1.append(StringUtils.trimToEmpty("DATA_DATE")).append(split);
			buffer1.append(StringUtils.trimToEmpty("BRNO")).append(split);
			buffer1.append(StringUtils.trimToEmpty("SEQ_NO")).append(split);
			buffer1.append(StringUtils.trimToEmpty("TX_DATE")).append(split);
			buffer1.append(StringUtils.trimToEmpty("VALUE_DATE")).append(split);
			buffer1.append(StringUtils.trimToEmpty("SYS_CODE")).append(split);
			buffer1.append(StringUtils.trimToEmpty("TX_CODE")).append(split);
			buffer1.append(StringUtils.trimToEmpty("ENTRY_NO")).append(split);
			buffer1.append(StringUtils.trimToEmpty("ACT_NO")).append(split);
			buffer1.append(StringUtils.trimToEmpty("DC_FLAG")).append(split);
			buffer1.append(StringUtils.trimToEmpty("CUST_BRNO")).append(split);
			buffer1.append(StringUtils.trimToEmpty("CUST_NO")).append(split);
			buffer1.append(StringUtils.trimToEmpty("CUST_ACT_NO")).append(split);
			buffer1.append(StringUtils.trimToEmpty("PROD_TYPE")).append(split);
			buffer1.append(StringUtils.trimToEmpty("PROD_CODE")).append(split);
			buffer1.append(StringUtils.trimToEmpty("CCY")).append(split);
			buffer1.append(StringUtils.trimToEmpty("TAX_INCLUSIVE_AMT")).append(split);
			buffer1.append(StringUtils.trimToEmpty("INCOME_AMT")).append(split);
			buffer1.append(StringUtils.trimToEmpty("TAX_AMT")).append(split);
			buffer1.append(StringUtils.trimToEmpty("TAX_RATE")).append(split);
			buffer1.append(StringUtils.trimToEmpty("FX_RATE")).append(split);
			buffer1.append(StringUtils.trimToEmpty("CNY_TAX_INCLUSIVE_AMT")).append(split);
			buffer1.append(StringUtils.trimToEmpty("CNY_AMT")).append(split);
			buffer1.append(StringUtils.trimToEmpty("CNY_TAX_AMT")).append(split);
			buffer1.append(StringUtils.trimToEmpty("INC_TAX")).append(split);
			buffer1.append(StringUtils.trimToEmpty("TAX_CCY")).append(split);
			buffer1.append(StringUtils.trimToEmpty("NUM")).append(split);
			buffer1.append(StringUtils.trimToEmpty("PRICE_AMT")).append(split);
			buffer1.append(StringUtils.trimToEmpty("PAY_FLAG")).append(split);
			buffer1.append(StringUtils.trimToEmpty("REMARKS")).append(split);
			buffer1.append(StringUtils.trimToEmpty("REVERSE_FLAG")).append(split);
			buffer1.append(StringUtils.trimToEmpty("ORI_TX_ID")).append(split);
			buffer1.append(StringUtils.trimToEmpty("ORI_SEQ_NO")).append(split);
			buffer1.append(StringUtils.trimToEmpty("ORI_TX_DATE")).append(split);
			buffer1.append(StringUtils.trimToEmpty("REF_NO")).append(split);
			buffer1.append(StringUtils.trimToEmpty("OP_USER")).append(split);
			buffer1.append(StringUtils.trimToEmpty("OP_SSN")).append(split);
			buffer1.append(StringUtils.trimToEmpty("DEPT_NO")).append(split);
			buffer1.append(StringUtils.trimToEmpty("EXT1")).append(split);
			buffer1.append(StringUtils.trimToEmpty("EXT2")).append(split);
			buffer1.append(StringUtils.trimToEmpty("EXT3")).append(split);
			buffer1.append(StringUtils.trimToEmpty("EXT4")).append(split);
			buffer1.append(StringUtils.trimToEmpty("EXT5")).append(split);
			buffer1.append(StringUtils.trimToEmpty("")).append("\r\n");
			allbuffer.append(buffer1);
			
		for(TbkEntry gvat : list){

			buffer = new StringBuffer();
			buffer.append(StringUtils.trimToEmpty(DateUtil.getCurrentCompactDateTimeAsString()+gvat.getFlowId())).append(split);
			buffer.append(StringUtils.trimToEmpty(DateUtil.getCurrentDateAsString().replace("-", ""))).append(split); 
			buffer.append(StringUtils.trimToEmpty(gvat.getSponInst()==null?"":gvat.getSponInst())).append(split); 
			buffer.append(StringUtils.trimToEmpty(gvat.getDealNo()==null?"":gvat.getDealNo())).append(split); 
			buffer.append(StringUtils.trimToEmpty("")).append(split);
			buffer.append(StringUtils.trimToEmpty(gvat.getPostDate()==null?"":gvat.getPostDate())).append(split); 
			buffer.append(StringUtils.trimToEmpty("IFBM")).append(split); 
			buffer.append(StringUtils.trimToEmpty("")).append(split);
			buffer.append(StringUtils.trimToEmpty(gvat.getSubjCode()==null?"":gvat.getSubjCode())).append(split); 
			buffer.append(StringUtils.trimToEmpty("")).append(split);
			buffer.append(StringUtils.trimToEmpty(gvat.getDebitCreditFlag()=="D"?"1":"2")).append(split); 
			buffer.append(StringUtils.trimToEmpty("")).append(split);
			buffer.append(StringUtils.trimToEmpty("")).append(split); //客户编号
			buffer.append(StringUtils.trimToEmpty("")).append(split); //客户帐号
			buffer.append(StringUtils.trimToEmpty("")).append(split);//产品类型
			buffer.append(StringUtils.trimToEmpty(gvat.getPrdNo()==null?"":gvat.getPrdNo())).append(split);//产品代码
			buffer.append(StringUtils.trimToEmpty(gvat.getCcy()==null?"":gvat.getCcy())).append(split); 
			buffer.append(StringUtils.trimToEmpty(gvat.getValue()+""==null?"":gvat.getValue()+"")).append(split); 
			buffer.append(StringUtils.trimToEmpty(gvat.getValue()+""==null?"":gvat.getValue()+"")).append(split); 
			buffer.append(StringUtils.trimToEmpty("")).append(split); 
			buffer.append(StringUtils.trimToEmpty("")).append(split); 
			buffer.append(StringUtils.trimToEmpty("1")).append(split); 
			buffer.append(StringUtils.trimToEmpty(gvat.getValue()+""==null?"":gvat.getValue()+"")).append(split); 
			buffer.append(StringUtils.trimToEmpty(gvat.getValue()+""==null?"":gvat.getValue()+"")).append(split);
			buffer.append(StringUtils.trimToEmpty("")).append(split); 
			buffer.append(StringUtils.trimToEmpty("")).append(split);
			buffer.append(StringUtils.trimToEmpty("")).append(split);
			buffer.append(StringUtils.trimToEmpty("")).append(split);
			buffer.append(StringUtils.trimToEmpty("")).append(split);
			buffer.append(StringUtils.trimToEmpty("")).append(split);
			buffer.append(StringUtils.trimToEmpty("")).append(split);
			buffer.append(StringUtils.trimToEmpty("否")).append(split); 
			buffer.append(StringUtils.trimToEmpty("")).append(split); 
			buffer.append(StringUtils.trimToEmpty("")).append(split); 
			buffer.append(StringUtils.trimToEmpty(gvat.getUpdateTime()==null?"":gvat.getUpdateTime())).append(split); 
			buffer.append(StringUtils.trimToEmpty("")).append(split);
			buffer.append(StringUtils.trimToEmpty("")).append(split);
			buffer.append(StringUtils.trimToEmpty(gvat.getOperDealno()==null?"":gvat.getOperDealno())).append(split);
			buffer.append(StringUtils.trimToEmpty("")).append(split);
			buffer.append(StringUtils.trimToEmpty("")).append(split);
			buffer.append(StringUtils.trimToEmpty("")).append(split);
			buffer.append(StringUtils.trimToEmpty("")).append(split); 
			buffer.append(StringUtils.trimToEmpty("")).append(split);
			buffer.append(StringUtils.trimToEmpty("")).append(split); 
			buffer.append(StringUtils.trimToEmpty("")).append("\r\n");
			allbuffer.append(buffer);
		}
		
		}
		
		Map<String, Object> map1 = FileReadWriteService.writeFile(allbuffer.toString(), filename, filebak, filepath,encoding);
		StringBuffer buffers = new StringBuffer();
		
		File f = new File("c:/"+filename+"/");
		
		buffers.append(filename+","+f.length()+","+(list.size()+1));
		String filenames = "TX_OUTPUT_DATA_"+format.format(new Date())+".flg";
		FileReadWriteService.writeFile(buffers.toString(), filenames, filebak, filepath,encoding);
		RetMsg<Object> retMsg = new RetMsg<Object>(map1.get("retcode").toString(), map1.get("retmsg").toString(), "", null);
		return retMsg;
	}
	

	}
	


