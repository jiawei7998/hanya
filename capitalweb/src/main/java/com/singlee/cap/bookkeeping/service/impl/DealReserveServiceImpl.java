package com.singlee.cap.bookkeeping.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.cap.bookkeeping.model.TbkEntry;
import com.singlee.cap.bookkeeping.service.TbkEntryService;
import com.singlee.cap.bookkeeping.service.DealReserveService;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.dict.DictConstants;
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class DealReserveServiceImpl implements DealReserveService {

	@Autowired
	private TbkEntryService bookkeepingEntryService;
	
	@Override
	public void ReserveDealEntry(Map<String, Object> map) {
		//红字蓝字冲正00-蓝字,01-红字
		String redFlag = "01";
		String flowId = "";
		String oldFlowId = null;
		List<TbkEntry> list = new ArrayList<TbkEntry>();
		List<TbkEntry> entryList = bookkeepingEntryService.getDealEntryList(map);
		for (TbkEntry tbkEntry : entryList) {
			tbkEntry.setEntryId("");
			if(!oldFlowId.equals(tbkEntry.getFlowId())){
				oldFlowId = tbkEntry.getFlowId();
				flowId = bookkeepingEntryService.getNewFlowNo();
			}
			tbkEntry.setFlowId(flowId);
			if(tbkEntry.getRedFlag().equals(redFlag)){//红字  方向不变，金额反向
				tbkEntry.setValue(-1*tbkEntry.getValue());
			}else{//蓝字  方向反向，金额不变
				
				// 未完善，要考虑科目只能单边的情况 如：科目为借方，蓝字冲正的时候为贷方是不允许的
				// shiting 2017-5-25
				if(tbkEntry.getDebitCreditFlag().equals(DictConstants.DebitCreditFlag.Credit)){
					tbkEntry.setDebitCreditFlag(DictConstants.DebitCreditFlag.Debit);
				}else if(tbkEntry.getDebitCreditFlag().equals(DictConstants.DebitCreditFlag.Debit)){
					tbkEntry.setDebitCreditFlag(DictConstants.DebitCreditFlag.Credit);
				}else if(tbkEntry.getDebitCreditFlag().equals(DictConstants.DebitCreditFlag.Receive)){
					tbkEntry.setDebitCreditFlag(DictConstants.DebitCreditFlag.Pay);
				}else if(tbkEntry.getDebitCreditFlag().equals(DictConstants.DebitCreditFlag.Pay)){
					tbkEntry.setDebitCreditFlag(DictConstants.DebitCreditFlag.Receive);
				}
			}
			tbkEntry.setPostDate(ParameterUtil.getString(map, "bizDate", DateUtil.getCurrentDateAsString()));
			list.add(tbkEntry);
		}
		bookkeepingEntryService.save(entryList);
	}

}
