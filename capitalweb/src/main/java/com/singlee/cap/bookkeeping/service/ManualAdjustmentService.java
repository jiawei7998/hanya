package com.singlee.cap.bookkeeping.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.cap.bookkeeping.model.TbkDealBalance;

/**
 * 手工调账服务接口
 * @author Administrator
 *
 */
public interface ManualAdjustmentService {
	/**
	 * 查询需要调账交易的账务汇总信息
	 * @param dealNo
	 */
	public Page<TbkDealBalance> queryEntrySum(Map<String, Object> map);
	
	/**
	 * 手工调账
	 * @param dealNo
	 */
	public void manualAdjustment(String dealNo);
	
}
