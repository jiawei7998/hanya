package com.singlee.cap.bookkeeping.model;

import java.io.Serializable;

import javax.persistence.Entity;

/**
 * 科目余额查询实体类
 * @author zhangyc
 *
 */
@Entity
public class QueryBalanceModel implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5628335551665975598L;
	/**　　交易流水号**/
	private String dealNo;
	/**　　记账机构**/
	private String bkkpgOrgId;
	/**　　币种**/
	private String ccy;
	/**　　科目代码**/
	private String subjCode;
	/**　　科目名称**/
	private String subjName;
	/**　　借方余额**/
	private double debitValue;
	/**　　贷方余额**/
	private double creditValue;
	/**　　收方余额**/
	private double receiveValue;
	/**　　付方余额**/
	private double payValue;
	/**　　最后修改时间**/
	private String lstDate;
	
	private String postDate    ;    
	private String fundCode    ;    
	private String fundName    ;    
	private String prdNo       ;    
	private String prdName     ;    
	private String instId      ;    
	private String instName    ;    

	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public String getBkkpgOrgId() {
		return bkkpgOrgId;
	}
	public void setBkkpgOrgId(String bkkpgOrgId) {
		this.bkkpgOrgId = bkkpgOrgId;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public String getSubjCode() {
		return subjCode;
	}
	public void setSubjCode(String subjCode) {
		this.subjCode = subjCode;
	}
	public String getSubjName() {
		return subjName;
	}
	public void setSubjName(String subjName) {
		this.subjName = subjName;
	}
	public double getDebitValue() {
		return debitValue;
	}
	public void setDebitValue(double debitValue) {
		this.debitValue = debitValue;
	}
	public double getCreditValue() {
		return creditValue;
	}
	public void setCreditValue(double creditValue) {
		this.creditValue = creditValue;
	}
	public double getReceiveValue() {
		return receiveValue;
	}
	public void setReceiveValue(double receiveValue) {
		this.receiveValue = receiveValue;
	}
	public double getPayValue() {
		return payValue;
	}
	public void setPayValue(double payValue) {
		this.payValue = payValue;
	}
	public String getLstDate() {
		return lstDate;
	}
	public void setLstDate(String lstDate) {
		this.lstDate = lstDate;
	}
	public String getPostDate() {
		return postDate;
	}
	public void setPostDate(String postDate) {
		this.postDate = postDate;
	}
	public String getFundCode() {
		return fundCode;
	}
	public void setFundCode(String fundCode) {
		this.fundCode = fundCode;
	}
	public String getFundName() {
		return fundName;
	}
	public void setFundName(String fundName) {
		this.fundName = fundName;
	}
	public String getPrdNo() {
		return prdNo;
	}
	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}
	public String getPrdName() {
		return prdName;
	}
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	public String getInstId() {
		return instId;
	}
	public void setInstId(String instId) {
		this.instId = instId;
	}
	public String getInstName() {
		return instName;
	}
	public void setInstName(String instName) {
		this.instName = instName;
	}
	
	
}
