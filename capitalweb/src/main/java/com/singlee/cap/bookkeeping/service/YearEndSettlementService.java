package com.singlee.cap.bookkeeping.service;

/**
 * 年终结算服务接口
 * @author shiting
 *
 */
public interface YearEndSettlementService {

	public void YearEndSettlement();
}
