package com.singlee.cap.bookkeeping.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.cap.bookkeeping.model.TbkSubjectBalanceSum;
import com.singlee.cap.bookkeeping.model.TiInterfaceTdpacstmtinq;
import com.singlee.capital.interfacex.model.TtT24FloatRate;

public interface BalanceOfAccountFor407Mapper extends Mapper<TiInterfaceTdpacstmtinq> {
	
	  
	  void insertTiInterfaceTdpacstmtinq(TiInterfaceTdpacstmtinq tiInterfaceTdpacstmtinq);
	  public Page<TiInterfaceTdpacstmtinq> selectByBookingDateAndInstIdAndCcy(Map<String,Object> map,RowBounds rb);
	  public Page<TiInterfaceTdpacstmtinq> selectByBookingDateAndInstIdAndUsd(Map<String,Object> map,RowBounds rb);
	  public int selectByTxnRefCount(String txnRef);
}
