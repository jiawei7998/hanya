package com.singlee.cap.bookkeeping.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.cap.bookkeeping.mapper.TbkBalanceSumTransforMapper;
import com.singlee.cap.bookkeeping.model.TbkBalanceSumTransfor;
import com.singlee.cap.bookkeeping.service.TbkBalanceSumTransforService;

/**
 * 记账余额的服务类
 * @author shiting
 *
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TBkBalanceSumTransforServiceImpl implements TbkBalanceSumTransforService {
	@Autowired
	private TbkBalanceSumTransforMapper tbkBalanceSumTransforMapper;
	
	@Override
	public List<TbkBalanceSumTransfor> selectTbkBalanceSumTransforListService(
			Map<String, Object> map) {
		List<TbkBalanceSumTransfor> list = tbkBalanceSumTransforMapper.selectTbkBalanceSumTransforList(map);
		return list;
	}

	@Override
	public void updateBalanceSumTransforService(Map<String, Object> map) {
		tbkBalanceSumTransforMapper.updateBalanceSumTransfor(map);
	}

	
}
