package com.singlee.cap.bookkeeping.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;
/**
 * 交易明细接口表
 * @author Administrator
 *
 */
@Entity
@Table(name="TI_INTERFACE_TDPACSTMTINQ")
public class TiInterfaceTdpacstmtinq implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String   instId		 ;//机构
	private String   acctNo      ;//帐号
	private String   mediumType  ;//帐号类型
	private String   txnRef      ;//交易流水号
	private String   bookingDate ;//记账日期
	private String   txnDate     ;//交易日期
	private String   txnDrcr     ;//借贷标识
	private String   txnAmt      ;//交易金额
	private String   descs       ;//交易摘要
	private String   oppbrName   ;//对方行名
	private String   compName    ;//交易网点名称
	private String   txnFbDesc   ;//中间业务类型描述
	private String   transCode   ;
	private String   txnShowDate ;//交易显示日期
	private String   fbid        ;//FBID
	private String   operUser    ;//操作员
	private String   mediumnFlg  ;//一户多介质标志
	private String   ccy  		 ;//币种
	
	private String   oppAc   	 ;//对方账号
	private String   oppacName	 ;//对方户名
	private String   txnOppno	 ;//对方行号
	private String   stmt		 ;//核心分录号
	
	
	
	
	
	public String getStmt() {
		return stmt;
	}
	public void setStmt(String stmt) {
		this.stmt = stmt;
	}
	public String getOppAc() {
		return oppAc;
	}
	public void setOppAc(String oppAc) {
		this.oppAc = oppAc;
	}
	public String getOppacName() {
		return oppacName;
	}
	public void setOppacName(String oppacName) {
		this.oppacName = oppacName;
	}
	public String getTxnOppno() {
		return txnOppno;
	}
	public void setTxnOppno(String txnOppno) {
		this.txnOppno = txnOppno;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public String getInstId() {
		return instId;
	}
	public void setInstId(String instId) {
		this.instId = instId;
	}
	public String getAcctNo() {
		return acctNo;
	}
	public void setAcctNo(String acctNo) {
		this.acctNo = acctNo;
	}
	public String getMediumType() {
		return mediumType;
	}
	public void setMediumType(String mediumType) {
		this.mediumType = mediumType;
	}
	public String getTxnRef() {
		return txnRef;
	}
	public void setTxnRef(String txnRef) {
		this.txnRef = txnRef;
	}
	public String getBookingDate() {
		return bookingDate;
	}
	public void setBookingDate(String bookingDate) {
		this.bookingDate = bookingDate;
	}
	public String getTxnDate() {
		return txnDate;
	}
	public void setTxnDate(String txnDate) {
		this.txnDate = txnDate;
	}
	public String getTxnDrcr() {
		return txnDrcr;
	}
	public void setTxnDrcr(String txnDrcr) {
		this.txnDrcr = txnDrcr;
	}
	public String getTxnAmt() {
		return txnAmt;
	}
	public void setTxnAmt(String txnAmt) {
		this.txnAmt = txnAmt;
	}
	public String getDescs() {
		return descs;
	}
	public void setDescs(String descs) {
		this.descs = descs;
	}
	public String getOppbrName() {
		return oppbrName;
	}
	public void setOppbrName(String oppbrName) {
		this.oppbrName = oppbrName;
	}
	public String getCompName() {
		return compName;
	}
	public void setCompName(String compName) {
		this.compName = compName;
	}
	public String getTxnFbDesc() {
		return txnFbDesc;
	}
	public void setTxnFbDesc(String txnFbDesc) {
		this.txnFbDesc = txnFbDesc;
	}
	public String getTransCode() {
		return transCode;
	}
	public void setTransCode(String transCode) {
		this.transCode = transCode;
	}
	public String getTxnShowDate() {
		return txnShowDate;
	}
	public void setTxnShowDate(String txnShowDate) {
		this.txnShowDate = txnShowDate;
	}
	public String getFbid() {
		return fbid;
	}
	public void setFbid(String fbid) {
		this.fbid = fbid;
	}
	public String getOperUser() {
		return operUser;
	}
	public void setOperUser(String operUser) {
		this.operUser = operUser;
	}
	public String getMediumnFlg() {
		return mediumnFlg;
	}
	public void setMediumnFlg(String mediumnFlg) {
		this.mediumnFlg = mediumnFlg;
	}
	
	

}
