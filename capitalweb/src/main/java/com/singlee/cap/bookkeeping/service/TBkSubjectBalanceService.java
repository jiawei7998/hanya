package com.singlee.cap.bookkeeping.service;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.cap.bookkeeping.model.TbkEntry;
import com.singlee.cap.bookkeeping.model.TbkSubjectBalance;


/**
 * 记账余额的服务接口
 * @author Libo
 *
 */
public interface TBkSubjectBalanceService {

//	/**
//	 * 设置记账变动当前日期
//	 * @param taskId
//	 * @param date
//	 */
//	public void setCurrentDate(String taskId, String date);
	
	/**
	 * 根据分录查找相应的余额，如查询不到，则创建新的并返回
	 * @param entry
	 */
	public TbkSubjectBalance findOrCreateByEntry(TbkEntry entry);
	
	void insert(TbkSubjectBalance balance);
	/**
	 * 更新余额，只更新四个数字余额（借、贷、支、付）
	 * @param balance
	 */
	public void updateBalance(TbkSubjectBalance balance);
	
	public Page<TbkSubjectBalance> selectByMap(Map<String,Object> map,RowBounds rb);

	List<TbkSubjectBalance> selectByMap(Map<String, Object> map);
}
