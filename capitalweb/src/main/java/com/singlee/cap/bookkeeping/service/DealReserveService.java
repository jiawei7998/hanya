package com.singlee.cap.bookkeeping.service;

import java.util.Map;

/**
 * 交易冲销出冲销的账务
 * @author Administrator
 *
 */
public interface DealReserveService {

	public void ReserveDealEntry(Map<String, Object> map);
}
