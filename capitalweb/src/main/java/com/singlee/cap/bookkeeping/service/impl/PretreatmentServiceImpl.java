package com.singlee.cap.bookkeeping.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.cap.accounting.model.InstructionPackage;
import com.singlee.cap.bookkeeping.service.PretreatmentService;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.duration.service.DurationConstants;

/**
 * 预处理服务类
 * @author shiting
 *
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class PretreatmentServiceImpl implements PretreatmentService {

	@Override
	public void dealPretreatment(InstructionPackage instructionPackage) {
		//正常-1/逾期-2/逾期90天-3/逾期大于90天-4
		String overdueDays = "";
		int Days = ParameterUtil.getInt(instructionPackage.getInfoMap(),"overdueDays",0);
		if(Days == 0){
			overdueDays = "1";
		}else if(Days == 90){
			overdueDays = "3";
		}else if(Days > 90){
			overdueDays = "4";
		}else{
			overdueDays = "2";
		}
		instructionPackage.getInfoMap().put("overdueDays", overdueDays);
		
		//判断是否为中收
		Set<String> keys = instructionPackage.getAmtMap().keySet();
		
		List<String> keysList = new ArrayList<String>();
		for (String string : keys){
			keysList.add(string);
		}
		
		for (String string : keysList) {
			if(string.toLowerCase().indexOf("fee") != -1){//
				instructionPackage.getInfoMap().put("prdNo_1", instructionPackage.getInfoMap().get("prdNo"));
				instructionPackage.getInfoMap().put("prdNo", "99");
				if(!string.equals(DurationConstants.AccType.FEE_AMT_INTADJ)){ //
					instructionPackage.getAmtMap().put("fee_Amt", instructionPackage.getAmtMap().get(string));
				}
			}
		}

		// 确定会计事件
		if("".equals(ParameterUtil.getString(instructionPackage.getInfoMap(), "eventType", ""))){
			String event = "";
			for (String string : keysList) {
				if(!event.equals(string.split("_")[0])){
					event = string.split("_")[0];
				}
			}
			instructionPackage.getInfoMap().put("eventType", event);
		}
		
		
		//补记-1/销记-2
		String PARAM_I1 = "";
		Double am_IntAdj = ParameterUtil.getDouble(instructionPackage.getAmtMap(),"am_IntAdj",0);
		if(am_IntAdj.compareTo(new Double(0)) > 0){//应收息调整-提前到期大于0
			PARAM_I1 = "1";
		}else if(am_IntAdj.compareTo(new Double(0)) < 0){//应收息调整-提前到期小于0
			PARAM_I1 = "2";
		}
		instructionPackage.getInfoMap().put("PARAM_I1", PARAM_I1);

		//利率调大-1/利率调小-2
		String PARAM_I2 = "";
		Double adj_ByRateChage_Interest = ParameterUtil.getDouble(instructionPackage.getAmtMap(),"adj_ByRateChage_Interest",0);
		if(adj_ByRateChage_Interest.compareTo(new Double(0)) > 0){//利率变更引起的调整大于0
			PARAM_I2 = "1";
		}else if(adj_ByRateChage_Interest.compareTo(new Double(0)) < 0){//利率变更引起的调整小于0
			PARAM_I2 = "2";
		}
		instructionPackage.getInfoMap().put("adjFlag", PARAM_I2);
		
				
		
	}
	
	public static void main(String[] args) {
		String str = "fee_finadvfeerate";
		System.out.println(str.toLowerCase().indexOf("fee") != -1);
	}
}
