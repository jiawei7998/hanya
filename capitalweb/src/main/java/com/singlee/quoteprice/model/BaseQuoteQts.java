package com.singlee.quoteprice.model;

import java.io.Serializable;

public class BaseQuoteQts implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id;
	private String sName;
	private String qstId;
	private String status;
	private String inputUser;
	private String inputDate;
	private String updateUser;
	private String updateDate;
	private String note;
	private String sourcePhoto;
	private String heartIp;
	private String heartport;
	private String agjbbp;
	private String agjsbp;
	private String webpage;
	
	

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getsName() {
		return sName;
	}
	public void setsName(String sName) {
		this.sName = sName;
	}
	public String getQstId() {
		return qstId;
	}
	public void setQstId(String qstId) {
		this.qstId = qstId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getInputUser() {
		return inputUser;
	}
	public void setInputUser(String inputUser) {
		this.inputUser = inputUser;
	}
	public String getInputDate() {
		return inputDate;
	}
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	public String getUpdateUser() {
		return updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
	public String getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getSourcePhoto() {
		return sourcePhoto;
	}
	public void setSourcePhoto(String sourcePhoto) {
		this.sourcePhoto = sourcePhoto;
	}
	public String getHeartIp() {
		return heartIp;
	}
	public void setHeartIp(String heartIp) {
		this.heartIp = heartIp;
	}
	public String getHeartport() {
		return heartport;
	}
	public void setHeartport(String heartport) {
		this.heartport = heartport;
	}
	public String getAgjbbp() {
		return agjbbp;
	}
	public void setAgjbbp(String agjbbp) {
		this.agjbbp = agjbbp;
	}
	public String getAgjsbp() {
		return agjsbp;
	}
	public void setAgjsbp(String agjsbp) {
		this.agjsbp = agjsbp;
	}
	public String getWebpage() {
		return webpage;
	}
	public void setWebpage(String webpage) {
		this.webpage = webpage;
	}

	
}
