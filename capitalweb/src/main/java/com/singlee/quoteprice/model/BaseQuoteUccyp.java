package com.singlee.quoteprice.model;

import java.io.Serializable;

public class BaseQuoteUccyp implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String uid1;
	private String ccyp;
	private String systemid;
	private String x;
	private String y;
	private String note;
	private String corder;
	private String cid;
	public String getUid1() {
		return uid1;
	}
	public void setUid1(String uid1) {
		this.uid1 = uid1;
	}
	public String getCcyp() {
		return ccyp;
	}
	public void setCcyp(String ccyp) {
        this.ccyp = ccyp == null ? null : ccyp.trim();
	}
	public String getSystemid() {
		return systemid;
	}
	public void setSystemid(String systemid) {
		this.systemid = systemid;
	}
	public String getX() {
		return x;
	}
	public void setX(String x) {
		this.x = x;
	}
	public String getY() {
		return y;
	}
	public void setY(String y) {
		this.y = y;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getCorder() {
		return corder;
	}
	public void setCorder(String corder) {
		this.corder = corder;
	}
	public String getCid() {
		return cid;
	}
	public void setCid(String cid) {
		this.cid = cid;
	}
	
	
}
