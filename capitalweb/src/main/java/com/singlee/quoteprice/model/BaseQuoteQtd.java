package com.singlee.quoteprice.model;

import java.io.Serializable;

public class BaseQuoteQtd implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String did;
	private String dName;
	private String status;
	private String note;
	private String qtStatus;
	private String ip;
	private String port;
	private String inputUser;
	private String inputDate;
	private String updateUser;
	private String updateDate;
	private String assunit;
	public String getDid() {
		return did;
	}
	public void setDid(String did) {
		this.did = did;
	}
	public String getdName() {
		return dName;
	}
	public void setdName(String dName) {
		this.dName = dName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getQtStatus() {
		return qtStatus;
	}
	public void setQtStatus(String qtStatus) {
		this.qtStatus = qtStatus;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public String getInputUser() {
		return inputUser;
	}
	public void setInputUser(String inputUser) {
		this.inputUser = inputUser;
	}
	public String getInputDate() {
		return inputDate;
	}
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	public String getUpdateUser() {
		return updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
	public String getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
	public String getAssunit() {
		return assunit;
	}
	public void setAssunit(String assunit) {
		this.assunit = assunit;
	}

	
}
