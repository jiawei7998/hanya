package com.singlee.quoteprice.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class BaseQuoteCcypair implements Serializable {
    private String ccyp;

    private String pricenum;

    private String cbuybp;

    private String csalebp;

    private String maxbp;

    private String qtstatus;

    private String qttype;

    private String qtsid;

    private String pop;

    private String inputuser;

    private String inputdate;

    private String updateuser;

    private String updatedate;

    private String note;

    private String quoteunits;

    private String rbuybp;

    private String rsalebp;

    private String cbuybpweb;

    private String csalebpweb;

    private String rbuybpweb;

    private String rsalebpweb;

    private String cbuywfb;

    private String csalewfb;

    private String rbuywfb;

    private String rsalewfb;

    private String cbuywfbweb;

    private String csalewfbweb;

    private String rbuywfbweb;

    private String rsalewfbweb;

    private String cbuybpdate;

    private String csalebpdate;

    private String rbuybpdate;

    private String rsalebpdate;

    private String cbuybpwebdate;

    private String csalebpwebdate;

    private String rbuybpwebdate;

    private String rsalebpwebdate;

    private String cbuywfbdate;

    private String csalewfbdate;

    private String rbuywfbdate;

    private String rsalewfbdate;

    private String cbuywfbwebdate;

    private String csalewfbwebdate;

    private String rbuywfbwebdate;

    private String rsalewfbwebdate;

    private String maxwfb;

    private String wgjrxzjc;

    private String wgjcxzjc;

    private String isused;

    private BigDecimal cnyoprice;

    private BigDecimal cnyiprice;

    private String obp;

    private String ibp;

    private String fixupdateuser;

    private String fixupdatedate;

    private String floatupdateuser;

    private String floatupdatedate;

    private String owfb;

    private String iwfb;

    private String branchpop;

    private String syspop;

    private String syspopdate;

    private String webpop;

    private String webpopdate;

    private static final long serialVersionUID = 1L;

    public String getCcyp() {
        return ccyp;
    }

    public void setCcyp(String ccyp) {
        this.ccyp = ccyp == null ? null : ccyp.trim();
    }

    public String getPricenum() {
        return pricenum;
    }

    public void setPricenum(String pricenum) {
        this.pricenum = pricenum;
    }

    public String getCbuybp() {
        return cbuybp;
    }

    public void setCbuybp(String cbuybp) {
        this.cbuybp = cbuybp;
    }

    public String getCsalebp() {
        return csalebp;
    }

    public void setCsalebp(String csalebp) {
        this.csalebp = csalebp;
    }

    public String getMaxbp() {
        return maxbp;
    }

    public void setMaxbp(String maxbp) {
        this.maxbp = maxbp;
    }

    public String getQtstatus() {
        return qtstatus;
    }

    public void setQtstatus(String qtstatus) {
        this.qtstatus = qtstatus;
    }

    public String getQttype() {
        return qttype;
    }

    public void setQttype(String qttype) {
        this.qttype = qttype;
    }

    public String getQtsid() {
        return qtsid;
    }

    public void setQtsid(String qtsid) {
        this.qtsid = qtsid;
    }

    public String getPop() {
        return pop;
    }

    public void setPop(String pop) {
        this.pop = pop;
    }

    public String getInputuser() {
        return inputuser;
    }

    public void setInputuser(String inputuser) {
        this.inputuser = inputuser == null ? null : inputuser.trim();
    }

    public String getInputdate() {
        return inputdate;
    }

    public void setInputdate(String inputdate) {
        this.inputdate = inputdate == null ? null : inputdate.trim();
    }

    public String getUpdateuser() {
        return updateuser;
    }

    public void setUpdateuser(String updateuser) {
        this.updateuser = updateuser == null ? null : updateuser.trim();
    }

    public String getUpdatedate() {
        return updatedate;
    }

    public void setUpdatedate(String updatedate) {
        this.updatedate = updatedate == null ? null : updatedate.trim();
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note == null ? null : note.trim();
    }

    public String getQuoteunits() {
        return quoteunits;
    }

    public void setQuoteunits(String quoteunits) {
        this.quoteunits = quoteunits;
    }

    public String getRbuybp() {
        return rbuybp;
    }

    public void setRbuybp(String rbuybp) {
        this.rbuybp = rbuybp;
    }

    public String getRsalebp() {
        return rsalebp;
    }

    public void setRsalebp(String rsalebp) {
        this.rsalebp = rsalebp;
    }

    public String getCbuybpweb() {
        return cbuybpweb;
    }

    public void setCbuybpweb(String cbuybpweb) {
        this.cbuybpweb = cbuybpweb;
    }

    public String getCsalebpweb() {
        return csalebpweb;
    }

    public void setCsalebpweb(String csalebpweb) {
        this.csalebpweb = csalebpweb;
    }

    public String getRbuybpweb() {
        return rbuybpweb;
    }

    public void setRbuybpweb(String rbuybpweb) {
        this.rbuybpweb = rbuybpweb;
    }

    public String getRsalebpweb() {
        return rsalebpweb;
    }

    public void setRsalebpweb(String rsalebpweb) {
        this.rsalebpweb = rsalebpweb;
    }

    public String getCbuywfb() {
        return cbuywfb;
    }

    public void setCbuywfb(String cbuywfb) {
        this.cbuywfb = cbuywfb;
    }

    public String getCsalewfb() {
        return csalewfb;
    }

    public void setCsalewfb(String csalewfb) {
        this.csalewfb = csalewfb;
    }

    public String getRbuywfb() {
        return rbuywfb;
    }

    public void setRbuywfb(String rbuywfb) {
        this.rbuywfb = rbuywfb;
    }

    public String getRsalewfb() {
        return rsalewfb;
    }

    public void setRsalewfb(String rsalewfb) {
        this.rsalewfb = rsalewfb;
    }

    public String getCbuywfbweb() {
        return cbuywfbweb;
    }

    public void setCbuywfbweb(String cbuywfbweb) {
        this.cbuywfbweb = cbuywfbweb;
    }

    public String getCsalewfbweb() {
        return csalewfbweb;
    }

    public void setCsalewfbweb(String csalewfbweb) {
        this.csalewfbweb = csalewfbweb;
    }

    public String getRbuywfbweb() {
        return rbuywfbweb;
    }

    public void setRbuywfbweb(String rbuywfbweb) {
        this.rbuywfbweb = rbuywfbweb;
    }

    public String getRsalewfbweb() {
        return rsalewfbweb;
    }

    public void setRsalewfbweb(String rsalewfbweb) {
        this.rsalewfbweb = rsalewfbweb;
    }

    public String getCbuybpdate() {
        return cbuybpdate;
    }

    public void setCbuybpdate(String cbuybpdate) {
        this.cbuybpdate = cbuybpdate;
    }

    public String getCsalebpdate() {
        return csalebpdate;
    }

    public void setCsalebpdate(String csalebpdate) {
        this.csalebpdate = csalebpdate;
    }

    public String getRbuybpdate() {
        return rbuybpdate;
    }

    public void setRbuybpdate(String rbuybpdate) {
        this.rbuybpdate = rbuybpdate;
    }

    public String getRsalebpdate() {
        return rsalebpdate;
    }

    public void setRsalebpdate(String rsalebpdate) {
        this.rsalebpdate = rsalebpdate;
    }

    public String getCbuybpwebdate() {
        return cbuybpwebdate;
    }

    public void setCbuybpwebdate(String cbuybpwebdate) {
        this.cbuybpwebdate = cbuybpwebdate;
    }

    public String getCsalebpwebdate() {
        return csalebpwebdate;
    }

    public void setCsalebpwebdate(String csalebpwebdate) {
        this.csalebpwebdate = csalebpwebdate;
    }

    public String getRbuybpwebdate() {
        return rbuybpwebdate;
    }

    public void setRbuybpwebdate(String rbuybpwebdate) {
        this.rbuybpwebdate = rbuybpwebdate;
    }

    public String getRsalebpwebdate() {
        return rsalebpwebdate;
    }

    public void setRsalebpwebdate(String rsalebpwebdate) {
        this.rsalebpwebdate = rsalebpwebdate;
    }

    public String getCbuywfbdate() {
        return cbuywfbdate;
    }

    public void setCbuywfbdate(String cbuywfbdate) {
        this.cbuywfbdate = cbuywfbdate;
    }

    public String getCsalewfbdate() {
        return csalewfbdate;
    }

    public void setCsalewfbdate(String csalewfbdate) {
        this.csalewfbdate = csalewfbdate;
    }

    public String getRbuywfbdate() {
        return rbuywfbdate;
    }

    public void setRbuywfbdate(String rbuywfbdate) {
        this.rbuywfbdate = rbuywfbdate;
    }

    public String getRsalewfbdate() {
        return rsalewfbdate;
    }

    public void setRsalewfbdate(String rsalewfbdate) {
        this.rsalewfbdate = rsalewfbdate;
    }

    public String getCbuywfbwebdate() {
        return cbuywfbwebdate;
    }

    public void setCbuywfbwebdate(String cbuywfbwebdate) {
        this.cbuywfbwebdate = cbuywfbwebdate;
    }

    public String getCsalewfbwebdate() {
        return csalewfbwebdate;
    }

    public void setCsalewfbwebdate(String csalewfbwebdate) {
        this.csalewfbwebdate = csalewfbwebdate;
    }

    public String getRbuywfbwebdate() {
        return rbuywfbwebdate;
    }

    public void setRbuywfbwebdate(String rbuywfbwebdate) {
        this.rbuywfbwebdate = rbuywfbwebdate;
    }

    public String getRsalewfbwebdate() {
        return rsalewfbwebdate;
    }

    public void setRsalewfbwebdate(String rsalewfbwebdate) {
        this.rsalewfbwebdate = rsalewfbwebdate;
    }

    public String getMaxwfb() {
        return maxwfb;
    }

    public void setMaxwfb(String maxwfb) {
        this.maxwfb = maxwfb;
    }

    public String getWgjrxzjc() {
        return wgjrxzjc;
    }

    public void setWgjrxzjc(String wgjrxzjc) {
        this.wgjrxzjc = wgjrxzjc;
    }

    public String getWgjcxzjc() {
        return wgjcxzjc;
    }

    public void setWgjcxzjc(String wgjcxzjc) {
        this.wgjcxzjc = wgjcxzjc;
    }

    public String getIsused() {
        return isused;
    }

    public void setIsused(String isused) {
        this.isused = isused;
    }

    public BigDecimal getCnyoprice() {
        return cnyoprice;
    }

    public void setCnyoprice(BigDecimal cnyoprice) {
        this.cnyoprice = cnyoprice;
    }

    public BigDecimal getCnyiprice() {
        return cnyiprice;
    }

    public void setCnyiprice(BigDecimal cnyiprice) {
        this.cnyiprice = cnyiprice;
    }

    public String getObp() {
        return obp;
    }

    public void setObp(String obp) {
        this.obp = obp;
    }

    public String getIbp() {
        return ibp;
    }

    public void setIbp(String ibp) {
        this.ibp = ibp;
    }

    public String getFixupdateuser() {
        return fixupdateuser;
    }

    public void setFixupdateuser(String fixupdateuser) {
        this.fixupdateuser = fixupdateuser == null ? null : fixupdateuser.trim();
    }

    public String getFixupdatedate() {
        return fixupdatedate;
    }

    public void setFixupdatedate(String fixupdatedate) {
        this.fixupdatedate = fixupdatedate == null ? null : fixupdatedate.trim();
    }

    public String getFloatupdateuser() {
        return floatupdateuser;
    }

    public void setFloatupdateuser(String floatupdateuser) {
        this.floatupdateuser = floatupdateuser == null ? null : floatupdateuser.trim();
    }

    public String getFloatupdatedate() {
        return floatupdatedate;
    }

    public void setFloatupdatedate(String floatupdatedate) {
        this.floatupdatedate = floatupdatedate == null ? null : floatupdatedate.trim();
    }

    public String getOwfb() {
        return owfb;
    }

    public void setOwfb(String owfb) {
        this.owfb = owfb;
    }

    public String getIwfb() {
        return iwfb;
    }

    public void setIwfb(String iwfb) {
        this.iwfb = iwfb;
    }

    public String getBranchpop() {
        return branchpop;
    }

    public void setBranchpop(String branchpop) {
        this.branchpop = branchpop;
    }

    public String getSyspop() {
        return syspop;
    }

    public void setSyspop(String syspop) {
        this.syspop = syspop;
    }

    public String getSyspopdate() {
        return syspopdate;
    }

    public void setSyspopdate(String syspopdate) {
        this.syspopdate = syspopdate;
    }

    public String getWebpop() {
        return webpop;
    }

    public void setWebpop(String webpop) {
        this.webpop = webpop;
    }

    public String getWebpopdate() {
        return webpopdate;
    }

    public void setWebpopdate(String webpopdate) {
        this.webpopdate = webpopdate;
    }
}