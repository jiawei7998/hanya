package com.singlee.quoteprice.model;

import java.io.Serializable;

public class BaseQuoteBaseInfo implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String rmbpType;
	private String startTime;
	private String endTime;
	private String mpType;
	private String msgCount;
//	private String ;
	public String getRmbpType() {
		return rmbpType;
	}
	public void setRmbpType(String rmbpType) {
		this.rmbpType = rmbpType;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getMpType() {
		return mpType;
	}
	public void setMpType(String mpType) {
		this.mpType = mpType;
	}
	public String getMsgCount() {
		return msgCount;
	}
	public void setMsgCount(String msgCount) {
		this.msgCount = msgCount;
	}
	
	
}
