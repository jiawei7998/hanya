package com.singlee.quoteprice.model;

public class WebSourceBean {
	
	private String ccyPair;
	private String bidPrc;
	private String askPrc;
	private String midprice;
	private String time;
	public String getCcyPair() {
		return ccyPair;
	}
	public void setCcyPair(String ccyPair) {
		this.ccyPair = ccyPair;
	}
	public String getBidPrc() {
		return bidPrc;
	}
	public void setBidPrc(String bidPrc) {
		this.bidPrc = bidPrc;
	}
	public String getAskPrc() {
		return askPrc;
	}
	public void setAskPrc(String askPrc) {
		this.askPrc = askPrc;
	}
	public String getMidprice() {
		return midprice;
	}
	public void setMidprice(String midprice) {
		this.midprice = midprice;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	
	
}
