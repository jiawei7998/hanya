/**
 * Project Name:capitalweb
 * File Name:SendBean.java
 * Package Name:com.singlee.quoteprice.DwsUtil
 * Date:2018-6-4下午07:30:55
 * Copyright (c) 2018, chenzhou1025@126.com All Rights Reserved.
 *
*/

package com.singlee.quoteprice.model;
/**
 * ClassName:SendBean <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason:	 TODO ADD REASON. <br/>
 * Date:     2018-6-4 下午07:30:55 <br/>
 * @author   huang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class SendBean {
	
	private String ccyp;
	//货币对
	private String webcbuy;
	//钞买
	private String webcsale;
	//钞卖
	private String webrbuy;
	//汇买
	private String webrsale;
	//汇卖
	private String webcenter;
	//中间价
	private String webtime;
	//时间
	public String getCcyp() {
		return ccyp;
	}
	public void setCcyp(String ccyp) {
		this.ccyp = ccyp;
	}
	public String getWebcbuy() {
		return webcbuy;
	}
	public void setWebcbuy(String webcbuy) {
		this.webcbuy = webcbuy;
	}
	public String getWebcsale() {
		return webcsale;
	}
	public void setWebcsale(String webcsale) {
		this.webcsale = webcsale;
	}
	public String getWebrbuy() {
		return webrbuy;
	}
	public void setWebrbuy(String webrbuy) {
		this.webrbuy = webrbuy;
	}
	public String getWebrsale() {
		return webrsale;
	}
	public void setWebrsale(String webrsale) {
		this.webrsale = webrsale;
	}
	public String getWebcenter() {
		return webcenter;
	}
	public void setWebcenter(String webcenter) {
		this.webcenter = webcenter;
	}
	public String getWebtime() {
		return webtime;
	}
	public void setWebtime(String webtime) {
		this.webtime = webtime;
	}
	
	
}

