package com.singlee.quoteprice.model;


import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class MidPrice implements Serializable {

	private Map<String, String> data;
	private List<WebSourceBean> records;
	private Map<String, String> head;

	public Map<String, String> getData() {
		return data;
	}

	public void setData(Map<String, String> data) {
		this.data = data;
	}


	public List<WebSourceBean> getRecords() {
		return records;
	}

	public void setRecords(List<WebSourceBean> records) {
		this.records = records;
	}

	public Map<String, String> getHead() {
		return head;
	}

	public void setHead(Map<String, String> head) {
		this.head = head;
	}

}
