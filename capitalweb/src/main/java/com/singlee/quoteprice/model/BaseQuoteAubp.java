package com.singlee.quoteprice.model;

import java.io.Serializable;

public class BaseQuoteAubp implements Serializable {
    private String auser;

    private String atime;

    private String ccyp;

    private String cbuybp;

    private String csalebp;

    private String shuser;

    private String shtime;

    private String shresult;

    private String shnote;

    private String id;

    private String rbuybp;

    private String rsalebp;

    private String webpage;

    private String cbuywfb;

    private String csalewfb;

    private String rbuywfb;

    private String rsalewfb;

    private String pop;

    private static final long serialVersionUID = 1L;

	public String getAuser() {
		return auser;
	}

	public void setAuser(String auser) {
		this.auser = auser;
	}

	public String getAtime() {
		return atime;
	}

	public void setAtime(String atime) {
		this.atime = atime;
	}

	public String getCcyp() {
		return ccyp;
	}

	public void setCcyp(String ccyp) {
		this.ccyp = ccyp;
	}

	public String getCbuybp() {
		return cbuybp;
	}

	public void setCbuybp(String cbuybp) {
		this.cbuybp = cbuybp;
	}

	public String getCsalebp() {
		return csalebp;
	}

	public void setCsalebp(String csalebp) {
		this.csalebp = csalebp;
	}

	public String getShuser() {
		return shuser;
	}

	public void setShuser(String shuser) {
		this.shuser = shuser;
	}

	public String getShtime() {
		return shtime;
	}

	public void setShtime(String shtime) {
		this.shtime = shtime;
	}

	public String getShresult() {
		return shresult;
	}

	public void setShresult(String shresult) {
		this.shresult = shresult;
	}

	public String getShnote() {
		return shnote;
	}

	public void setShnote(String shnote) {
		this.shnote = shnote;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getRbuybp() {
		return rbuybp;
	}

	public void setRbuybp(String rbuybp) {
		this.rbuybp = rbuybp;
	}

	public String getRsalebp() {
		return rsalebp;
	}

	public void setRsalebp(String rsalebp) {
		this.rsalebp = rsalebp;
	}

	public String getWebpage() {
		return webpage;
	}

	public void setWebpage(String webpage) {
		this.webpage = webpage;
	}

	public String getCbuywfb() {
		return cbuywfb;
	}

	public void setCbuywfb(String cbuywfb) {
		this.cbuywfb = cbuywfb;
	}

	public String getCsalewfb() {
		return csalewfb;
	}

	public void setCsalewfb(String csalewfb) {
		this.csalewfb = csalewfb;
	}

	public String getRbuywfb() {
		return rbuywfb;
	}

	public void setRbuywfb(String rbuywfb) {
		this.rbuywfb = rbuywfb;
	}

	public String getRsalewfb() {
		return rsalewfb;
	}

	public void setRsalewfb(String rsalewfb) {
		this.rsalewfb = rsalewfb;
	}

	public String getPop() {
		return pop;
	}

	public void setPop(String pop) {
		this.pop = pop;
	}

    
}