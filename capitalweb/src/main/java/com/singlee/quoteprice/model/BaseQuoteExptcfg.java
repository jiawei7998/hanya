package com.singlee.quoteprice.model;

import java.io.Serializable;

public class BaseQuoteExptcfg implements Serializable {
    private String exptcode;

    private String dealtype;

    private String updateuser;

    private String updatedate;

    private String note;

    private static final long serialVersionUID = 1L;

    public String getExptcode() {
        return exptcode;
    }

    public void setExptcode(String exptcode) {
        this.exptcode = exptcode == null ? null : exptcode.trim();
    }


    public String getDealtype() {
		return dealtype;
	}

	public void setDealtype(String dealtype) {
		this.dealtype = dealtype;
	}

	public String getUpdateuser() {
        return updateuser;
    }

    public void setUpdateuser(String updateuser) {
        this.updateuser = updateuser == null ? null : updateuser.trim();
    }

    public String getUpdatedate() {
        return updatedate;
    }

    public void setUpdatedate(String updatedate) {
        this.updatedate = updatedate == null ? null : updatedate.trim();
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note == null ? null : note.trim();
    }
}