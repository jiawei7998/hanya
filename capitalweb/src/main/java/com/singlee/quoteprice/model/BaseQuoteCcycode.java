package com.singlee.quoteprice.model;

import java.io.Serializable;

public class BaseQuoteCcycode implements Serializable {
    private String ccy;

    private String ccyname;

    private String status;

    private String inputuser;

    private String inputdate;

    private String updateuser;

    private String updatedate;

    private String note;

    private String usdmprice;

    private String usdyprice;

    private String stip;

    private String stop;

    private String peoplepriceunit;

    private String peopleprice;

    private String peoplepricedate;

    private Short isuse;

    private static final long serialVersionUID = 1L;

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getCcyname() {
		return ccyname;
	}

	public void setCcyname(String ccyname) {
		this.ccyname = ccyname;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getInputuser() {
		return inputuser;
	}

	public void setInputuser(String inputuser) {
		this.inputuser = inputuser;
	}

	public String getInputdate() {
		return inputdate;
	}

	public void setInputdate(String inputdate) {
		this.inputdate = inputdate;
	}

	public String getUpdateuser() {
		return updateuser;
	}

	public void setUpdateuser(String updateuser) {
		this.updateuser = updateuser;
	}

	public String getUpdatedate() {
		return updatedate;
	}

	public void setUpdatedate(String updatedate) {
		this.updatedate = updatedate;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getUsdmprice() {
		return usdmprice;
	}

	public void setUsdmprice(String usdmprice) {
		this.usdmprice = usdmprice;
	}

	public String getUsdyprice() {
		return usdyprice;
	}

	public void setUsdyprice(String usdyprice) {
		this.usdyprice = usdyprice;
	}

	public String getStip() {
		return stip;
	}

	public void setStip(String stip) {
		this.stip = stip;
	}

	public String getStop() {
		return stop;
	}

	public void setStop(String stop) {
		this.stop = stop;
	}

	public String getPeoplepriceunit() {
		return peoplepriceunit;
	}

	public void setPeoplepriceunit(String peoplepriceunit) {
		this.peoplepriceunit = peoplepriceunit;
	}

	public String getPeopleprice() {
		return peopleprice;
	}

	public void setPeopleprice(String peopleprice) {
		this.peopleprice = peopleprice;
	}

	public String getPeoplepricedate() {
		return peoplepricedate;
	}

	public void setPeoplepricedate(String peoplepricedate) {
		this.peoplepricedate = peoplepricedate;
	}

	public Short getIsuse() {
		return isuse;
	}

	public void setIsuse(Short isuse) {
		this.isuse = isuse;
	}


}