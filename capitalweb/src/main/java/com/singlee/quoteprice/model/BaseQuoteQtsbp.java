package com.singlee.quoteprice.model;

import java.io.Serializable;

public class BaseQuoteQtsbp implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id;
	private String sid;
	private String sname;
	private String ccyp;
	private String pop;
	private String cbuybp;
	private String csalebp;
	private String rbuybp;
	private String rsalebp;
	private String cbuybpw;
	private String csalebpw;
	private String rbuybpw;
	private String rsalebpw;
	private String qtsunit;
	private String note;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSid() {
		return sid;
	}
	public void setSid(String sid) {
		this.sid = sid;
	}
	public String getSname() {
		return sname;
	}
	public void setSname(String sname) {
		this.sname = sname;
	}
	public String getCcyp() {
		return ccyp;
	}
	public void setCcyp(String ccyp) {
		this.ccyp = ccyp;
	}
	public String getPop() {
		return pop;
	}
	public void setPop(String pop) {
		this.pop = pop;
	}
	public String getCbuybp() {
		return cbuybp;
	}
	public void setCbuybp(String cbuybp) {
		this.cbuybp = cbuybp;
	}
	public String getCsalebp() {
		return csalebp;
	}
	public void setCsalebp(String csalebp) {
		this.csalebp = csalebp;
	}
	public String getRbuybp() {
		return rbuybp;
	}
	public void setRbuybp(String rbuybp) {
		this.rbuybp = rbuybp;
	}
	public String getRsalebp() {
		return rsalebp;
	}
	public void setRsalebp(String rsalebp) {
		this.rsalebp = rsalebp;
	}
	public String getCbuybpw() {
		return cbuybpw;
	}
	public void setCbuybpw(String cbuybpw) {
		this.cbuybpw = cbuybpw;
	}
	public String getCsalebpw() {
		return csalebpw;
	}
	public void setCsalebpw(String csalebpw) {
		this.csalebpw = csalebpw;
	}
	public String getRbuybpw() {
		return rbuybpw;
	}
	public void setRbuybpw(String rbuybpw) {
		this.rbuybpw = rbuybpw;
	}
	public String getRsalebpw() {
		return rsalebpw;
	}
	public void setRsalebpw(String rsalebpw) {
		this.rsalebpw = rsalebpw;
	}
	public String getQtsunit() {
		return qtsunit;
	}
	public void setQtsunit(String qtsunit) {
		this.qtsunit = qtsunit;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}

	
}
