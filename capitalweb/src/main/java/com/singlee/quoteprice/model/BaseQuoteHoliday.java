package com.singlee.quoteprice.model;

import java.io.Serializable;

public class BaseQuoteHoliday implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3988771626283807143L;
	private String endDate;
	private String startDate;
	private String name;
	private String updateDate;
	private String updateUser;
	
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
	public String getUpdateUser() {
		return updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	
}
