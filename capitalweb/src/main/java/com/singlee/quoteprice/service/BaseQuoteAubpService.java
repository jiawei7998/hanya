package com.singlee.quoteprice.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.quoteprice.model.BaseQuoteAubp;

public interface BaseQuoteAubpService {

	void saveAubp(BaseQuoteAubp baseQuoteAubp);

	Page<BaseQuoteAubp> searchAubp(Map<String, Object> map);
	//更新审批结果
	void updateAubp(Map<String, Object> map);

}
