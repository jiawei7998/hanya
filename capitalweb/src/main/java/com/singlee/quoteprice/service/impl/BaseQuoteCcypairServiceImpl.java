package com.singlee.quoteprice.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.quoteprice.mapper.BaseQuoteCcypairMapper;
import com.singlee.quoteprice.model.BaseQuoteCcypair;
import com.singlee.quoteprice.model.BaseQuoteQtd;
import com.singlee.quoteprice.service.BaseQuoteCcypairService;
@Service
public class BaseQuoteCcypairServiceImpl implements BaseQuoteCcypairService{
	@Resource
	BaseQuoteCcypairMapper baseQuoteCcypairMapper;

	@Override
	public List<BaseQuoteCcypair> searchCcypair() {
		List<BaseQuoteCcypair> list = baseQuoteCcypairMapper.searchCcypair();
		return list;
	}

	@Override
	public void saveCcypair(Map<String, Object> map) {
		baseQuoteCcypairMapper.saveCcypair(map);
	}

	@Override
	public Page<BaseQuoteCcypair> searchCcypairpage(Map<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<BaseQuoteCcypair> result = baseQuoteCcypairMapper.searchCcypairpage(map, rowBounds);
		return result;	
		}

	@Override
	public void deleteCcypair(Map<String, Object> map) {
		baseQuoteCcypairMapper.deleteCcypair(map);
	}

	@Override
	public void updateCcypairIsused(Map<String, Object> map) {
		baseQuoteCcypairMapper.updateCcypairIsused(map);
	}

	@Override
	public void updateCcypair(Map<String, Object> map) {
		baseQuoteCcypairMapper.updateCcypair(map);
	}
	//货币对审批完成更新
	@Override
	public void updateCcypairAubp(BaseQuoteCcypair baseQuoteCcypair) {

		baseQuoteCcypairMapper.updateCcypairAubp(baseQuoteCcypair);
	}
	//取出货币对
	@Override
	public List<BaseQuoteCcypair> selectCcypairmap() {

		List<BaseQuoteCcypair> map=baseQuoteCcypairMapper.selectCcypairmap();
		return map;
	}
}
