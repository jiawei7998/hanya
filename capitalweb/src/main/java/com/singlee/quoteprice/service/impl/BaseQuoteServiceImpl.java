package com.singlee.quoteprice.service.impl;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.ifs.model.IfsCfetsfxCswap;
import com.singlee.quoteprice.mapper.BaseQuoteBaseInfoMapper;
import com.singlee.quoteprice.mapper.BaseQuoteMapper;
import com.singlee.quoteprice.model.BaseQuoteBaseInfo;
import com.singlee.quoteprice.model.BaseQuoteHoliday;
import com.singlee.quoteprice.service.BaseQuoteService;

@Service
public class BaseQuoteServiceImpl implements BaseQuoteService{
	@Resource
	BaseQuoteMapper baseQuoteMapper;
	@Resource
	BaseQuoteBaseInfoMapper baseQuoteBaseInfoMapper;
	@Override
	public void saveHoliday(Map<String, Object> map) {
		baseQuoteMapper.saveHoliday(map);
	}
	@Override
	public Page<BaseQuoteHoliday> searchHoliday(Map<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<BaseQuoteHoliday> result = baseQuoteMapper.searchHoliday(map, rowBounds);
		return result;
		}
	@Override
	public void deleteHoliday(Map<String, Object> map) {
		baseQuoteMapper.deleteHoliday(map);
	}
	@Override
	public void updateHoliday(Map<String, Object> map) {
		baseQuoteMapper.updateHoliday(map);
	}
	@Override
	public BaseQuoteHoliday checkHoliday(Map<String, Object> map) {
		BaseQuoteHoliday result=baseQuoteMapper.checkHoliday(map);
		return result;
	}
	
	
	
	
	@Override
	public BaseQuoteBaseInfo sercherBaseInfo(Map<String, Object> map) {
		BaseQuoteBaseInfo resule = baseQuoteBaseInfoMapper.sercherBaseInfo(map);
		return resule;
	}
	@Override
	public void updateBaseInfo(Map<String, Object> map) {
		baseQuoteBaseInfoMapper.updateBaseInfo(map);
		
	}

	
}
