package com.singlee.quoteprice.service.impl;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.quoteprice.mapper.BaseQuoteExptMapper;
import com.singlee.quoteprice.model.BaseQuoteExpt;
import com.singlee.quoteprice.model.BaseQuoteQtsbp;
import com.singlee.quoteprice.service.BaseQuoteExptService;

@Service
public class BaseQuoteExptServiceImpl implements BaseQuoteExptService{
	
	@Resource
	BaseQuoteExptMapper baseQuoteExptMapper;
	
	@Override
	public void updateExpt(Map<String, Object> map) {
		baseQuoteExptMapper.updateExpt(map);
		
	}

	@Override
	public Page<BaseQuoteExpt> searchExpt(Map<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<BaseQuoteExpt> result = baseQuoteExptMapper.searchExpt(map, rowBounds);
		return result;
	}

}
