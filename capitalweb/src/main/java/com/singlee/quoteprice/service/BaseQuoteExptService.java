package com.singlee.quoteprice.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.quoteprice.model.BaseQuoteExpt;

public interface BaseQuoteExptService {

	void updateExpt(Map<String, Object> map);

	Page<BaseQuoteExpt> searchExpt(Map<String, Object> map);
	
}
