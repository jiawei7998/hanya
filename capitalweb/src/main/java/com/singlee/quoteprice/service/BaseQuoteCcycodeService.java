package com.singlee.quoteprice.service;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.quoteprice.model.BaseQuoteCcycode;

public interface BaseQuoteCcycodeService {

	void saveCcycode(Map<String, Object> map);

	Page<BaseQuoteCcycode> searchCcycode(Map<String, Object> map);

	void deleteCcycode(Map<String, Object> map);

	void updateCcycode(Map<String, Object> map);
	//获取所有基本货币
	List<BaseQuoteCcycode> saveCcyMap();

}
