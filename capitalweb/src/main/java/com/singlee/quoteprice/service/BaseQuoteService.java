package com.singlee.quoteprice.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.quoteprice.model.BaseQuoteBaseInfo;
import com.singlee.quoteprice.model.BaseQuoteHoliday;

public interface BaseQuoteService {

	public void saveHoliday(Map<String, Object> map);

	public Page<BaseQuoteHoliday> searchHoliday(Map<String, Object> map);

	public void deleteHoliday(Map<String, Object> map);

	public void updateHoliday(Map<String, Object> map);
	
	public BaseQuoteHoliday checkHoliday(Map<String, Object> map);

	
	
	public BaseQuoteBaseInfo sercherBaseInfo(Map<String, Object> map);

	public void updateBaseInfo(Map<String, Object> map);


}
