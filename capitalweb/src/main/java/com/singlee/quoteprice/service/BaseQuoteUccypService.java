package com.singlee.quoteprice.service;

import java.util.List;
import java.util.Map;

import com.singlee.quoteprice.model.BaseQuoteUccyp;

public interface BaseQuoteUccypService {

	List<BaseQuoteUccyp> searchUseUccyp(Map<String, Object> map);

	List<BaseQuoteUccyp> searchUnUseUccyp(Map<String, Object> map);

	void saveAllUccyp(Map<String, Object> map);

	void deleteAllUccyp(String uid1);

}
