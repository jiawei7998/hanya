package com.singlee.quoteprice.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.quoteprice.model.BaseQuoteQtsbp;

public interface BaseQuoteQtsbpService {

	void saveQtsbp(Map<String, Object> map);

	Page<BaseQuoteQtsbp> searchQtsbp(Map<String, Object> map);

	void deleteQtsbp(Map<String, Object> map);

	void updateQtsbp(Map<String, Object> map);

}
