package com.singlee.quoteprice.service.impl;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.quoteprice.mapper.BaseQuoteQtsMapper;
import com.singlee.quoteprice.model.BaseQuoteHoliday;
import com.singlee.quoteprice.model.BaseQuoteQts;
import com.singlee.quoteprice.service.BaseQuoteQtsService;
@Service
public class BaseQuoteQtsServiceImpl implements BaseQuoteQtsService{
	
		@Autowired
		BaseQuoteQtsMapper baseQuoteQtsMapper;

		@Override
		public void saveQts(Map<String, Object> map) {
			baseQuoteQtsMapper.saveQts(map);
			
		}

		@Override
		public Page<BaseQuoteQts> searchQts(Map<String, Object> map) {
			RowBounds rowBounds = ParameterUtil.getRowBounds(map);
			Page<BaseQuoteQts> result = baseQuoteQtsMapper.searchQts(map, rowBounds);
			return result;
		}

		@Override
		public void deleteQts(Map<String, Object> map) {
			
			baseQuoteQtsMapper.deleteQts(map);
		}

		@Override
		public void updateQts(Map<String, Object> map) {
			baseQuoteQtsMapper.updateQts(map);
			
		}

		
}
