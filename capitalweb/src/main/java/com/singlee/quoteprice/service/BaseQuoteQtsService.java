package com.singlee.quoteprice.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.quoteprice.model.BaseQuoteQts;

public interface BaseQuoteQtsService {

	void saveQts(Map<String, Object> map);

	Page<BaseQuoteQts> searchQts(Map<String, Object> map);

	void deleteQts(Map<String, Object> map);

	void updateQts(Map<String, Object> map);

}
