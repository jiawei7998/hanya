package com.singlee.quoteprice.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.quoteprice.model.BaseQuoteQtd;

public interface BaseQuoteQtdService {

	public void saveQtd(Map<String, Object> map);

	public Page<BaseQuoteQtd> searchQtd(Map<String, Object> map);

	public void deleteQtd(Map<String, Object> map);

	public void updateQtd(Map<String, Object> map);

}
