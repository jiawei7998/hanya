package com.singlee.quoteprice.service.impl;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.quoteprice.mapper.BaseQuoteQtdMapper;
import com.singlee.quoteprice.model.BaseQuoteHoliday;
import com.singlee.quoteprice.model.BaseQuoteQtd;
import com.singlee.quoteprice.service.BaseQuoteQtdService;

@Service
public class BaseQuoteQtdServiceImpl implements BaseQuoteQtdService {
	@Autowired
	BaseQuoteQtdMapper baseQuoteQtdMapper;
	
	@Override
	public void saveQtd(Map<String, Object> map) {
		baseQuoteQtdMapper.saveQtd(map);
	}

	@Override
	public Page<BaseQuoteQtd> searchQtd(Map<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<BaseQuoteQtd> result = baseQuoteQtdMapper.searchQtd(map, rowBounds);
		return result;
	}

	@Override
	public void deleteQtd(Map<String, Object> map) {
		baseQuoteQtdMapper.deleteQtd(map);
		
	}

	@Override
	public void updateQtd(Map<String, Object> map) {
		baseQuoteQtdMapper.updateQtd(map);
		
	}

}
