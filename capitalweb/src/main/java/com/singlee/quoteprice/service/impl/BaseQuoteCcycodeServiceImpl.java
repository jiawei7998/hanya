package com.singlee.quoteprice.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.quoteprice.mapper.BaseQuoteCcycodeMapper;
import com.singlee.quoteprice.model.BaseQuoteCcycode;
import com.singlee.quoteprice.model.BaseQuoteQtd;
import com.singlee.quoteprice.service.BaseQuoteCcycodeService;
@Service
public class BaseQuoteCcycodeServiceImpl implements BaseQuoteCcycodeService{
	@Resource
	BaseQuoteCcycodeMapper baseQuoteCcycodeMapper;
	@Override
	public void saveCcycode(Map<String, Object> map) {
		baseQuoteCcycodeMapper.saveCcycode(map);
	}

	@Override
	public Page<BaseQuoteCcycode> searchCcycode(Map<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<BaseQuoteCcycode> result = baseQuoteCcycodeMapper.searchCcycode(map, rowBounds);
		return result;
	}

	@Override
	public void deleteCcycode(Map<String, Object> map) {
		baseQuoteCcycodeMapper.deleteCcycode(map);
	}

	@Override
	public void updateCcycode(Map<String, Object> map) {
		baseQuoteCcycodeMapper.updateCcycode(map);
	}

	@Override
	public List<BaseQuoteCcycode> saveCcyMap() {
		List<BaseQuoteCcycode> result = baseQuoteCcycodeMapper.saveCcyMap();
		return result;
	}

}
