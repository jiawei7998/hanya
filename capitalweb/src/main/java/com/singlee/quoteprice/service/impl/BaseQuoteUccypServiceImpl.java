package com.singlee.quoteprice.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.singlee.quoteprice.mapper.BaseQuoteUccypMapper;
import com.singlee.quoteprice.model.BaseQuoteUccyp;
import com.singlee.quoteprice.service.BaseQuoteUccypService;
@Service
public class BaseQuoteUccypServiceImpl implements BaseQuoteUccypService{
	
	@Resource
	BaseQuoteUccypMapper baseQuoteUccypMapper;

	@Override
	public List<BaseQuoteUccyp> searchUseUccyp(Map<String, Object> map) {
		List<BaseQuoteUccyp> result = baseQuoteUccypMapper.searchUseUccyp(map);
		return result;
	}

	@Override
	public List<BaseQuoteUccyp> searchUnUseUccyp(Map<String, Object> map) {
		List<BaseQuoteUccyp> result = baseQuoteUccypMapper.searchUnUseUccyp(map);
		return result;
	}

	@Override
	public void saveAllUccyp(Map<String, Object> map) {
		baseQuoteUccypMapper.saveAllUccyp(map);
		
	}

	@Override
	public void deleteAllUccyp(String uid1) {
		baseQuoteUccypMapper.deleteAllUccyp(uid1);
		
	}
}
