package com.singlee.quoteprice.service.impl;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.quoteprice.mapper.BaseQuoteAubpMapper;
import com.singlee.quoteprice.model.BaseQuoteAubp;
import com.singlee.quoteprice.service.BaseQuoteAubpService;
@Service
public class BaseQuoteAubpServiceImpl implements BaseQuoteAubpService{

	@Resource
	BaseQuoteAubpMapper baseQuoteAubpMapper;

	@Override
	public void saveAubp(BaseQuoteAubp baseQuoteAubp) {
		baseQuoteAubpMapper.saveAubp(baseQuoteAubp);
		
	}

	@Override
	public Page<BaseQuoteAubp> searchAubp(Map<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<BaseQuoteAubp> result = baseQuoteAubpMapper.searchAubp(map, rowBounds);
		return result;	
	}

	@Override
	public void updateAubp(Map<String, Object> map) {

		baseQuoteAubpMapper.updateAubp(map);
	}
	
}
