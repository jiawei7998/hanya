package com.singlee.quoteprice.controller;

import java.io.Serializable;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.quoteprice.model.BaseQuoteQts;
import com.singlee.quoteprice.service.BaseQuoteQtsService;

@Controller
@RequestMapping(value = "/baseQuoteQtsController")
public class BaseQuoteQtsController {
	@Autowired
	 BaseQuoteQtsService baseQuoteQtsService;
	
	@ResponseBody
	@RequestMapping(value = "/saveQts")
	public RetMsg<Serializable> saveQts(@RequestBody Map<String, Object> map){
		baseQuoteQtsService.saveQts(map);
		return RetMsgHelper.ok();
	}
	
	@ResponseBody
	@RequestMapping(value = "/searchQts")
	public RetMsg<PageInfo<BaseQuoteQts>> searchQts(@RequestBody Map<String, Object> map) {
		Page<BaseQuoteQts> page = baseQuoteQtsService.searchQts(map);
		return RetMsgHelper.ok(page);
	}
	
	@ResponseBody
	@RequestMapping(value = "/deleteQts")
	public RetMsg<Serializable> deleteQts(@RequestBody Map<String, Object> map){
		baseQuoteQtsService.deleteQts(map);
		return RetMsgHelper.ok();
	}
	
	@ResponseBody
	@RequestMapping(value = "/updateQts")
	public RetMsg<Serializable> updateQts(@RequestBody Map<String, Object> map){
		baseQuoteQtsService.updateQts(map);
		return RetMsgHelper.ok();
	}
}
