package com.singlee.quoteprice.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.quoteprice.model.BaseQuoteCcycode;
import com.singlee.quoteprice.service.BaseQuoteCcycodeService;

@Controller
@RequestMapping(value="/baseQuoteCcycodeController")
public class BaseQuoteCcycodeController {
	
	@Autowired
	BaseQuoteCcycodeService baseQuoteCcycodeService;
	@ResponseBody
	@RequestMapping(value = "/saveCcycode")
	public RetMsg<Serializable> saveCcycode(@RequestBody Map<String, Object> map){
		baseQuoteCcycodeService.saveCcycode(map);
		return RetMsgHelper.ok();
	}
	
	@ResponseBody
	@RequestMapping(value = "/searchCcycode")
	public RetMsg<PageInfo<BaseQuoteCcycode>> searchCcycode(@RequestBody Map<String, Object> map) {
		Page<BaseQuoteCcycode> page = baseQuoteCcycodeService.searchCcycode(map);
		return RetMsgHelper.ok(page);
	}
	
	@ResponseBody
	@RequestMapping(value = "/deleteCcycode")
	public RetMsg<Serializable> deleteCcycode(@RequestBody Map<String, Object> map){
		baseQuoteCcycodeService.deleteCcycode(map);
		return RetMsgHelper.ok();
	}
	
	@ResponseBody
	@RequestMapping(value = "/updateCcycode")
	public RetMsg<Serializable> updateCcycode(@RequestBody Map<String, Object> map){
		baseQuoteCcycodeService.updateCcycode(map);
		return RetMsgHelper.ok();

}
	@ResponseBody
	@RequestMapping(value = "/saveCcyMap")
	public String saveCcyMap(@RequestBody Map<String, Object> map){
		List<BaseQuoteCcycode> baseQuoteCcycode = baseQuoteCcycodeService.saveCcyMap();
		ArrayList<String> arr = new ArrayList<String>();
		for(int i=0;i<baseQuoteCcycode.size();i++){
			String str = baseQuoteCcycode.get(i).getCcy();
			arr.add(str);
		}
		String str1 = JSONObject.toJSONString(arr);
		return str1;
	}
}