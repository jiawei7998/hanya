package com.singlee.quoteprice.controller;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import jxl.Sheet;
import jxl.Workbook;


import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.github.pagehelper.Page;
import com.singlee.capital.base.util.FileManage;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.UploadedFile;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.bean.SlQuotedPrice;
import com.singlee.financial.bean.TRtrate;
import com.singlee.financial.opics.IBaseServer;
import com.singlee.financial.opics.IQuotedPriceServer;
import com.singlee.financial.opics.IRmDataServer;
import com.singlee.ifs.mapper.IfsForeignCcyMapper;
import com.singlee.quoteprice.model.BaseQuoteBaseInfo;
import com.singlee.quoteprice.model.BaseQuoteHoliday;
import com.singlee.quoteprice.service.BaseQuoteService;

@Controller
@RequestMapping(value = "/baseQuoteController")
public class BaseQuoteController {
	@Autowired
	 BaseQuoteService baseQuoteService;
	@Autowired
	IQuotedPriceServer quotedPriceServer;
	@Autowired
	IRmDataServer rmDataServer;
	@Autowired
	IBaseServer baseServer;// 查询opics系统时间
	@Autowired
	IfsForeignCcyMapper ifsForeignCcyMapper;
	
	
	
	private static Logger logger = LoggerFactory.getLogger(BaseQuoteController.class);
	@ResponseBody
	@RequestMapping(value = "/saveHoliday")
	public RetMsg<Serializable> saveHoliday(@RequestBody Map<String, Object> map){
		baseQuoteService.saveHoliday(map);
		return RetMsgHelper.ok();
	}
	
	@ResponseBody
	@RequestMapping(value = "/searchQuotePrice")
	public RetMsg<com.singlee.financial.page.PageInfo<SlQuotedPrice>> searchQuotePrice(@RequestBody Map<String, Object> map) {
		// 查询全部
		com.singlee.financial.page.PageInfo<SlQuotedPrice> page = null;
		try {
			page = quotedPriceServer.getQuotedPriceList(map);
		} catch (RemoteConnectFailureException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return RetMsgHelper.ok(page);
	}
	@ResponseBody
	@RequestMapping(value = "/searchHoliday")
	public RetMsg<PageInfo<BaseQuoteHoliday>> searchHoliday(@RequestBody Map<String, Object> map) {
		Page<BaseQuoteHoliday> page = baseQuoteService.searchHoliday(map);
		return RetMsgHelper.ok(page);
	}
	
	@ResponseBody
	@RequestMapping(value = "/deleteHoliday")
	public RetMsg<Serializable> deleteHoliday(@RequestBody Map<String, Object> map){
		baseQuoteService.deleteHoliday(map);
		return RetMsgHelper.ok();
	}
	
	@ResponseBody
	@RequestMapping(value = "/updateHoliday")
	public RetMsg<Serializable> updateHoliday(@RequestBody Map<String, Object> map){
		baseQuoteService.updateHoliday(map);
		return RetMsgHelper.ok();
	}
	@ResponseBody
	@RequestMapping(value = "/checkHoliday")
	public RetMsg<BaseQuoteHoliday> checkHoliday(@RequestBody Map<String, Object> map) {
		BaseQuoteHoliday result = baseQuoteService.checkHoliday(map);
		return RetMsgHelper.ok(result);
	}
	@ResponseBody
	@RequestMapping(value = "/quotePriceEvent")
	public RetMsg<SlOutBean> quotePriceEvent(@RequestBody Map<String, Object> map) {
		SlOutBean result = quotedPriceServer.quotedPriceEvent();
		return RetMsgHelper.ok(result);
	}
	@ResponseBody
	@RequestMapping(value = "/sercherBaseInfo")
	public RetMsg<BaseQuoteBaseInfo> sercherBaseInfo(@RequestBody Map<String, Object> map) {
		BaseQuoteBaseInfo result = baseQuoteService.sercherBaseInfo(map);
		return RetMsgHelper.ok(result);
	}
	@ResponseBody
	@RequestMapping(value = "/updateBaseInfo")
	public RetMsg<Serializable> updateBaseInfo(@RequestBody Map<String, Object> map){
		baseQuoteService.updateBaseInfo(map);
		return RetMsgHelper.ok();
	}
	//	上传报价信息
/*	@ResponseBody
	@RequestMapping(value = "/uploadFile", method = RequestMethod.POST, produces = "text/html;charset=UTF-8")
	public RetMsg<Serializable> uploadFile(MultipartHttpServletRequest request) {
		OutputStream out = null;
		InputStream in = null;
		String mssage="操作成功";
		try {
			logger.info("报价信息上传...");
			MultipartFile mf = request.getFile("Fdata");
			if (null == mf) {
				mssage= "上传文件为空";
			}
			in = mf.getInputStream();
			String sysDate=DateUtil.getCurrentDateAsString("yyyyMMdd");
			String savePath = SystemProperties.uploadPrice_url+sysDate;
			File folder = new File(savePath);
			if (!folder.exists()) {
				folder.mkdirs();
			}
			//原文件名
			String filename = mf.getOriginalFilename();
			//后缀
			String prefix=filename.substring(filename.lastIndexOf(".")+1);
			if(!(prefix.equals("xls")||prefix.equals("xlsx"))){
				mssage= "文件不是xls或xlsx格式";
				return RetMsgHelper.simple(mssage);
			}
			//因为是统一文件名，所以同一天同一个文件重复上传会被覆盖
			logger.info("上传文件路径:" + savePath + filename);
			out = new FileOutputStream(new File(savePath, filename));
			int length = 0;
			byte[] buf = new byte[1024];
			while ((length = in.read(buf)) != -1) {
				// in.read(buf) 每次读到的数据存放在 buf 数组中
				// 在 buf 数组中 取出数据 写到 （输出流）磁盘上
				out.write(buf, 0, length); 
			}
		} catch (IOException e) {
			logger.info("更新异常：" + e.getMessage());
			mssage="更新异常";
		} finally {
			try {
				if (null != in) {
					in.close();
				}
			} catch (IOException e) {
			}
			try {
				if (null != out) {
					out.close();
				}
			} catch (IOException e) {
			}
		}
		return RetMsgHelper.ok(mssage);
	}*/
	
	/**
	 * Excel上传市场数据
	 */
	@ResponseBody
	@RequestMapping(value = "/importRmDataByExcel", method = RequestMethod.POST, produces = "text/plain;charset=UTF-8")
	public String importRmDataByExcel(MultipartHttpServletRequest request, HttpServletResponse response) {
		// 1.文件获取
		List<UploadedFile> uploadedFileList = null;
		String mssage = "操作成功";
		Workbook wb = null;
		double rate=0;
		double rate2=0;
		try {
			uploadedFileList = FileManage.requestExtractor(request);
			// 2.交验并处理成excel
			List<Workbook> excelList = new LinkedList<Workbook>();
			for (UploadedFile uploadedFile : uploadedFileList) {
				if (!("xls".equals(uploadedFile.getType())||"xlsx".equals(uploadedFile.getType()))) {
					mssage = "上传文件类型错误,仅允许上传xls或xlsx格式文件";
					return mssage;
				}
				Workbook book = Workbook.getWorkbook(new ByteArrayInputStream(uploadedFile.getBytes()));
				excelList.add(book);
			}
			if (excelList.size() != 1) {
				mssage = "只能处理一份excel";
				return mssage;
			}
			wb = excelList.get(0);
			Sheet[] sheets = wb.getSheets();// 获得卡片sheet数量
			Date date = baseServer.getOpicsSysDate("01");
			String postDate=DateUtil.format(date);
			for (Sheet sheet : sheets) {
				if("RMDATA".equals(sheet.getName())){
					int rowCount = sheet.getRows();// 行数
					for (int i = 1; i < rowCount; i++) { // rows=1开始 第二行开始读取，第一行为列名
						String aContent=StringUtils.trimToEmpty(sheet.getCell(0, i).getContents());//A列
						String bContent=StringUtils.trimToEmpty(sheet.getCell(1, i).getContents());//B列
						String cContent=StringUtils.trimToEmpty(sheet.getCell(2, i).getContents());//C列
						String dContent=StringUtils.trimToEmpty(sheet.getCell(3, i).getContents());//D列
						try {
							rate=Double.valueOf(bContent);
						} catch (Exception e) {
							rate=0;
						}
						try {
							rate2=Double.valueOf(dContent);
						} catch (Exception e) {
							rate2=0;
						}
						
						TRtrate tRtrate=new TRtrate();
						tRtrate.setFormula("RMDATA|"+aContent);
						tRtrate.setRate_8(BigDecimal.valueOf(rate));
						tRtrate.setInputDate(postDate);
						tRtrate.setErrorcode("0");
						SlOutBean outBean=quotedPriceServer.impRmdata(tRtrate);
						TRtrate tRtrate2=new TRtrate();
						tRtrate2.setFormula("RMDATA|"+cContent);
						tRtrate2.setRate_8(BigDecimal.valueOf(rate2));
						tRtrate2.setInputDate(postDate);
						tRtrate2.setErrorcode("0");
						SlOutBean outBean2=quotedPriceServer.impRmdata(tRtrate2);
						if(!("0000".equals(outBean.getRetCode())&& "0000".equals(outBean2.getRetCode()))){
							mssage="市场数据插入T_Rtrate表异常";
							return mssage;
						}
					}
				}
				
				
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("市场数据导入异常", e);
			throw new RException(e);
		} finally {
			if (null != wb) {
                wb.close();
            }
		}
		return mssage;
	}
	
	

}
