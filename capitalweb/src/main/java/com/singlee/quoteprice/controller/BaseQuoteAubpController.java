package com.singlee.quoteprice.controller;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.quoteprice.model.BaseQuoteAubp;
import com.singlee.quoteprice.model.BaseQuoteCcycode;
import com.singlee.quoteprice.model.BaseQuoteCcypair;
import com.singlee.quoteprice.service.BaseQuoteAubpService;
import com.singlee.quoteprice.service.BaseQuoteCcypairService;

@Controller
@RequestMapping(value="/baseQuoteAubpController")
public class BaseQuoteAubpController {
	
	@Autowired
	BaseQuoteAubpService baseQuoteAubpService;
	@Autowired
	BaseQuoteCcypairService baseQuoteCcypairService;
	
	@ResponseBody
	@RequestMapping(value="/saveAubp")
	public RetMsg<Serializable> saveAubp(@RequestBody Map<String, Object> map){
		BaseQuoteAubp baseQuoteAubp = new BaseQuoteAubp();
		String str = map.get("type").toString();
		baseQuoteAubp.setAuser(String.valueOf(map.get("updateuser")==null ? null : map.get("updateuser")));
		baseQuoteAubp.setAtime(String.valueOf(map.get("updatedate")==null ? null : map.get("updatedate")));
		baseQuoteAubp.setCcyp(String.valueOf(map.get("ccyp")==null ? null : map.get("ccyp")));
		baseQuoteAubp.setPop(String.valueOf(map.get("pop")==null ? null : map.get("pop")));
		baseQuoteAubp.setWebpage(String.valueOf(map.get("type")==null ? null : map.get("type")));
		if("1".equals(str)){
			baseQuoteAubp.setCbuybp(String.valueOf(map.get("cbuybp")==null ? null : map.get("cbuybp")));//钞买点差
			baseQuoteAubp.setCsalebp(String.valueOf(map.get("csalebp")==null ? null : map.get("csalebp")));
			baseQuoteAubp.setRbuybp(String.valueOf(map.get("rbuybp")==null ? null : map.get("rbuybp")));//汇买点差
			baseQuoteAubp.setRsalebp(String.valueOf(map.get("rsalebp")==null ? null : map.get("rsalebp")));
			baseQuoteAubp.setCbuywfb(String.valueOf(map.get("cbuywfb")==null ? null : map.get("cbuywfb")));//钞买万分比
			baseQuoteAubp.setCsalewfb(String.valueOf(map.get("csalewfb")==null ? null : map.get("csalewfb")));
			baseQuoteAubp.setRbuywfb(String.valueOf(map.get("rbuywfb")==null ? null : map.get("rbuywfb")));//汇买万分比
			baseQuoteAubp.setRsalewfb(String.valueOf(map.get("rsalewfb")==null ? null : map.get("rsalewfb")));
		}else if("2".equals(str)){
			baseQuoteAubp.setCbuybp(String.valueOf(map.get("cbuybpdate")==null ? null : map.get("cbuybpdate")));//钞买点差
			baseQuoteAubp.setCsalebp(String.valueOf(map.get("csalebpdate")==null ? null : map.get("csalebpdate")));
			baseQuoteAubp.setRbuybp(String.valueOf(map.get("rbuybpdate")==null ? null : map.get("rbuybpdate")));//汇买点差
			baseQuoteAubp.setRsalebp(String.valueOf(map.get("rsalebpdate")==null ? null : map.get("rsalebpdate")));
			baseQuoteAubp.setCbuywfb(String.valueOf(map.get("cbuywfbdate")==null ? null : map.get("cbuywfbdate")));//钞买万分比
			baseQuoteAubp.setCsalewfb(String.valueOf(map.get("csalewfbdate")==null ? null : map.get("csalewfbdate")));
			baseQuoteAubp.setRbuywfb(String.valueOf(map.get("rbuywfbdate")==null ? null : map.get("rbuywfbdate")));//汇买万分比
			baseQuoteAubp.setRsalewfb(String.valueOf(map.get("rsalewfbdate")==null ? null : map.get("rsalewfbdate")));			
		}else if("3".equals(str)){
			baseQuoteAubp.setCbuybp(String.valueOf(map.get("cbuybpweb")==null ? null : map.get("cbuybpweb")));//钞买点差
			baseQuoteAubp.setCsalebp(String.valueOf(map.get("csalebpweb")==null ? null : map.get("csalebpweb")));
			baseQuoteAubp.setRbuybp(String.valueOf(map.get("rbuybpweb")==null ? null : map.get("rbuybpweb")));//汇买点差
			baseQuoteAubp.setRsalebp(String.valueOf(map.get("rsalebpweb")==null ? null : map.get("rsalebpweb")));
			baseQuoteAubp.setCbuywfb(String.valueOf(map.get("cbuywfbweb")==null ? null : map.get("cbuywfbweb")));//钞买万分比
			baseQuoteAubp.setCsalewfb(String.valueOf(map.get("csalewfbweb")==null ? null : map.get("csalewfbweb")));
			baseQuoteAubp.setRbuywfb(String.valueOf(map.get("rbuywfbweb")==null ? null : map.get("rbuywfbweb")));//汇买万分比
			baseQuoteAubp.setRsalewfb(String.valueOf(map.get("rsalewfbweb")==null ? null : map.get("rsalewfbweb")));			
		}else if("4".equals(str)){
			baseQuoteAubp.setCbuybp(String.valueOf(map.get("cbuybpwebdate")==null ? null : map.get("cbuybpwebdate")));//钞买点差
			baseQuoteAubp.setCsalebp(String.valueOf(map.get("csalebpwebdate")==null ? null : map.get("csalebpwebdate")));
			baseQuoteAubp.setRbuybp(String.valueOf(map.get("rbuybpwebdate")==null ? null : map.get("rbuybpwebdate")));//汇买点差
			baseQuoteAubp.setRsalebp(String.valueOf(map.get("rsalebpwebdate")==null ? null : map.get("rsalebpwebdate")));
			baseQuoteAubp.setCbuywfb(String.valueOf(map.get("cbuywfbwebdate")==null ? null : map.get("cbuywfbwebdate")));//钞买万分比
			baseQuoteAubp.setCsalewfb(String.valueOf(map.get("csalewfbwebdate")==null ? null : map.get("csalewfbwebdate")));
			baseQuoteAubp.setRbuywfb(String.valueOf(map.get("rbuywfbwebdate")==null ? null : map.get("rbuywfbwebdate")));//汇买万分比
			baseQuoteAubp.setRsalewfb(String.valueOf(map.get("rsalewfbwebdate")==null ? null : map.get("rsalewfbwebdate")));			
		}

		baseQuoteAubpService.saveAubp(baseQuoteAubp);
		return RetMsgHelper.ok();
	}
	
	@ResponseBody
	@RequestMapping(value="/searchAubp")
	public RetMsg<PageInfo<BaseQuoteAubp>> searchAubp(@RequestBody Map<String, Object> map){
		Page<BaseQuoteAubp> page= baseQuoteAubpService.searchAubp(map);
		return RetMsgHelper.ok(page);
	}
	//货币对审批结果更新
	@ResponseBody
	@RequestMapping(value="/updateAubp")
	public RetMsg<Serializable> updateAubp(@RequestBody Map<String, Object> map){
		 baseQuoteAubpService.updateAubp(map);
		 BaseQuoteCcypair baseQuoteCcypair = new BaseQuoteCcypair();
		 String str = map.get("webpage").toString();
		 if("1".equals(map.get("shresult").toString())){
			 		baseQuoteCcypair.setCcyp(String.valueOf(map.get("ccyp")==null ? null : map.get("ccyp")));
			 if("1".equals(str)){
				 	baseQuoteCcypair.setCbuybp(String.valueOf(map.get("cbuybp")==null ? null : map.get("cbuybp")));//钞买点差
				 	baseQuoteCcypair.setCsalebp(String.valueOf(map.get("csalebp")==null ? null : map.get("csalebp")));
				 	baseQuoteCcypair.setRbuybp(String.valueOf(map.get("rbuybp")==null ? null : map.get("rbuybp")));//汇买点差
					baseQuoteCcypair.setRsalebp(String.valueOf(map.get("rsalebp")==null ? null : map.get("rsalebp")));
					baseQuoteCcypair.setCbuywfb(String.valueOf(map.get("cbuywfb")==null ? null : map.get("cbuywfb")));//钞买万分比
					baseQuoteCcypair.setCsalewfb(String.valueOf(map.get("csalewfb")==null ? null : map.get("csalewfb")));
					baseQuoteCcypair.setRbuywfb(String.valueOf(map.get("rbuywfb")==null ? null : map.get("rbuywfb")));//汇买万分比
					baseQuoteCcypair.setRsalewfb(String.valueOf(map.get("rsalewfb")==null ? null : map.get("rsalewfb")));
				}else if("2".equals(str)){
					baseQuoteCcypair.setCbuybpdate(String.valueOf(map.get("cbuybp")==null ? null : map.get("cbuybp")));//钞买点差
					baseQuoteCcypair.setCsalebpdate(String.valueOf(map.get("csalebp")==null ? null : map.get("csalebp")));
					baseQuoteCcypair.setRbuybpdate(String.valueOf(map.get("rbuybp")==null ? null : map.get("rbuybp")));//汇买点差
					baseQuoteCcypair.setRsalebpdate(String.valueOf(map.get("rsalebp")==null ? null : map.get("rsalebp")));
					baseQuoteCcypair.setCbuywfbdate(String.valueOf(map.get("cbuywfb")==null ? null : map.get("cbuywfb")));//钞买万分比
					baseQuoteCcypair.setCsalewfbdate(String.valueOf(map.get("csalewfb")==null ? null : map.get("csalewfb")));
					baseQuoteCcypair.setRbuywfbdate(String.valueOf(map.get("rbuywfb")==null ? null : map.get("rbuywfb")));//汇买万分比
					baseQuoteCcypair.setRsalewfbdate(String.valueOf(map.get("rsalewfb")==null ? null : map.get("rsalewfb")));			
				}else if("3".equals(str)){
					baseQuoteCcypair.setCbuybpweb(String.valueOf(map.get("cbuybp")==null ? null : map.get("cbuybp")));//钞买点差
					baseQuoteCcypair.setCsalebpweb(String.valueOf(map.get("csalebp")==null ? null : map.get("csalebp")));
					baseQuoteCcypair.setRbuybpweb(String.valueOf(map.get("rbuybp")==null ? null : map.get("rbuybp")));//汇买点差
					baseQuoteCcypair.setRsalebpweb(String.valueOf(map.get("rsalebp")==null ? null : map.get("rsalebp")));
					baseQuoteCcypair.setCbuywfbweb(String.valueOf(map.get("cbuywfb")==null ? null : map.get("cbuywfb")));//钞买万分比
					baseQuoteCcypair.setCsalewfbweb(String.valueOf(map.get("csalewfb")==null ? null : map.get("csalewfb")));
					baseQuoteCcypair.setRbuywfbweb(String.valueOf(map.get("rbuywfb")==null ? null : map.get("rbuywfb")));//汇买万分比
					baseQuoteCcypair.setRsalewfbweb(String.valueOf(map.get("rsalewfb")==null ? null : map.get("rsalewfb")));			
				}else if("4".equals(str)){
					baseQuoteCcypair.setCbuybpwebdate(String.valueOf(map.get("cbuybp")==null ? null : map.get("cbuybp")));//钞买点差
					baseQuoteCcypair.setCsalebpwebdate(String.valueOf(map.get("csalebp")==null ? null : map.get("csalebp")));
					baseQuoteCcypair.setRbuybpwebdate(String.valueOf(map.get("rbuybp")==null ? null : map.get("rbuybp")));//汇买点差
					baseQuoteCcypair.setRsalebpwebdate(String.valueOf(map.get("rsalebp")==null ? null : map.get("rsalebp")));
					baseQuoteCcypair.setCbuywfbwebdate(String.valueOf(map.get("cbuywfb")==null ? null : map.get("cbuywfb")));//钞买万分比
					baseQuoteCcypair.setCsalewfbwebdate(String.valueOf(map.get("csalewfb")==null ? null : map.get("csalewfb")));
					baseQuoteCcypair.setRbuywfbwebdate(String.valueOf(map.get("rbuywfb")==null ? null : map.get("rbuywfb")));//汇买万分比
					baseQuoteCcypair.setRsalewfbwebdate(String.valueOf(map.get("rsalewfb")==null ? null : map.get("rsalewfb")));			
				}
			 
			 		baseQuoteCcypairService.updateCcypairAubp(baseQuoteCcypair);
		 }
		return RetMsgHelper.ok();
	}
}
