package com.singlee.quoteprice.controller;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.quoteprice.model.BaseQuoteCcypair;
import com.singlee.quoteprice.model.BaseQuoteUccyp;
import com.singlee.quoteprice.service.BaseQuoteCcypairService;
import com.singlee.quoteprice.service.BaseQuoteUccypService;

@Controller
@RequestMapping(value = "/baseQuoteUccypController")
public class BaseQuoteUccypController {
	
	@Autowired
	BaseQuoteUccypService baseQuoteUccypService;
	@Autowired
	BaseQuoteCcypairService baseQuoteCcypairService;
	
	@ResponseBody
	@RequestMapping(value = "/searchAllUccyp")
	public RetMsg<Pair<Object,Object>> searchAllUccyp(@RequestBody Map<String, Object> map){
		List<BaseQuoteUccyp> list = baseQuoteUccypService.searchUseUccyp(map);
		List<BaseQuoteCcypair> list3 = baseQuoteCcypairService.searchCcypair();
		for(BaseQuoteUccyp temp: list){
			for(int i=0;i<list3.size();i++){
				BaseQuoteCcypair baseQuoteCcypair = list3.get(i);
				
				if(temp.getCcyp().equals(baseQuoteCcypair.getCcyp())){
					list3.remove(i);
				}
			}
		}
		List<BaseQuoteUccyp> list1 = baseQuoteUccypService.searchUnUseUccyp(map);
		return RetMsgHelper.ok(list3, list1);
	}
	
//	@ResponseBody
//	@RequestMapping(value = "/deleteAllUccyp")
//	public RetMsg<Serializable> deleteAllUccyp(@RequestBody Map<String, Object> map){
//		baseQuoteUccypService.deleteAllUccyp(map);
//		return RetMsgHelper.ok();
//	}
	
	@ResponseBody
	@RequestMapping(value = "/saveAllUccyp")
	public RetMsg<Serializable> saveAllUccyp(@RequestBody Map<String, Object> map){
		String[] str = map.get("ccyp").toString().split(",");
		String userId=map.get("userid").toString();
		String branchId = map.get("branchid").toString();
		baseQuoteUccypService.deleteAllUccyp(userId);
		for(int i=0;i<str.length;i++){
			System.out.println(str.length);
			String ss = str[i];
			Map<String, Object> map1 = new HashMap<String, Object>();
			map1.put("ccyp", ss);
			map1.put("userId",userId);
			map1.put("branchId", branchId);
			baseQuoteUccypService.saveAllUccyp(map1);
		}
		return RetMsgHelper.ok();
	}
 }
