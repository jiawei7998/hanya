package com.singlee.quoteprice.controller;

import java.io.Serializable;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.quoteprice.mapper.BaseQuoteExptcfgMapper;
import com.singlee.quoteprice.model.BaseQuoteExpt;
import com.singlee.quoteprice.model.BaseQuoteExptcfg;
import com.singlee.quoteprice.model.BaseQuoteHoliday;
import com.singlee.quoteprice.service.BaseQuoteExptService;
import com.singlee.quoteprice.service.BaseQuoteService;

@Controller
@RequestMapping(value="/baseQuoteExptController")
public class BaseQuoteExptController {
	@Resource
	BaseQuoteExptcfgMapper baseQuoteExptcfgMapper;
	@Autowired
	BaseQuoteExptService baseQuoteExptService;

	@ResponseBody
	@RequestMapping(value = "/updateExpt")
	public RetMsg<Serializable> updateExpt(@RequestBody Map<String, Object> map){
		baseQuoteExptService.updateExpt(map);
		String str = map.get("dealtype").toString();
		String[] map1 = str.split(",");
//		BaseQuoteExptcfg baseQuoteExptcfg = null ;
		if(map1.length>1){
//			maps.put("dealtype", "2");
//			baseQuoteExptcfg.setDealtype("2");
			map.remove("dealtype");
			map.put("dealtype", "2");
			baseQuoteExptcfgMapper.updateExptcfg(map);
		}else{
			baseQuoteExptcfgMapper.updateExptcfg(map);
//			baseQuoteExptcfg.setDealtype(str);
//			maps.put("dealtype", str);
		}
//		baseQuoteExptcfg.setExptcode(map.get("exptcode").toString());
//		maps.put("exptcode", map.get("exptcode").toString());
//		baseQuoteExptcfgMapper.updateExptcfg(baseQuoteExptcfg);
//		System.out.println(map.get("exptcode").toString());
		
		return RetMsgHelper.ok();
	}
	
	@ResponseBody
	@RequestMapping(value = "/searchExpt")
	public RetMsg<PageInfo<BaseQuoteExpt>> searchExpt(@RequestBody Map<String, Object> map) {
		Page<BaseQuoteExpt> page = baseQuoteExptService.searchExpt(map);
		return RetMsgHelper.ok(page);
	}
}
