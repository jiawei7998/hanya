package com.singlee.quoteprice.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.quoteprice.model.BaseQuoteCcypair;
import com.singlee.quoteprice.service.BaseQuoteCcypairService;

@Controller
@RequestMapping(value = "/baseQuoteCcypairController")
public class BaseQuoteCcypairController {
	@Autowired
	BaseQuoteCcypairService baseQuoteCcypairService;
	
	@ResponseBody
	@RequestMapping(value = "/saveCcypair")
	public RetMsg<Serializable> saveCcypair(@RequestBody Map<String, Object> map){
		baseQuoteCcypairService.saveCcypair(map);
		return RetMsgHelper.ok();
	}
	
	@ResponseBody
	@RequestMapping(value = "/searchCcypairpage")
	public RetMsg<PageInfo<BaseQuoteCcypair>> searchCcypair(@RequestBody Map<String, Object> map) {
		Page<BaseQuoteCcypair> page = baseQuoteCcypairService.searchCcypairpage(map);
		return RetMsgHelper.ok(page);
	}
	
	@ResponseBody
	@RequestMapping(value = "/deleteCcypair")
	public RetMsg<Serializable> deleteCcypair(@RequestBody Map<String, Object> map){
		baseQuoteCcypairService.deleteCcypair(map);
		return RetMsgHelper.ok();
	}
	
	//交易货币对的删除
	@ResponseBody
	@RequestMapping(value = "/updateCcypairIsused")
	public RetMsg<Serializable> updateCcypairIsused(@RequestBody Map<String, Object> map){
		baseQuoteCcypairService.updateCcypairIsused(map);
		return RetMsgHelper.ok();
	}
	//交易货币对的增加
	@ResponseBody
	@RequestMapping(value = "/updateCcypair")
	public RetMsg<Serializable> updateCcypair(@RequestBody Map<String, Object> map){
		baseQuoteCcypairService.updateCcypair(map);
		return RetMsgHelper.ok();
	}
	//货币对审批完成更新数据库
//	@ResponseBody
//	@RequestMapping(value = "/updateCcypair")
//	public RetMsg<Serializable> updateCcypairAubp(@RequestBody Map<String, Object> map){
//		baseQuoteCcypairService.updateCcypairAubp(map);
//		return RetMsgHelper.ok();
//	}
	
	@ResponseBody
	@RequestMapping(value = "/selectCcypairmap")
	public String selectCcypairmap(@RequestBody Map<String, Object> map){
		List<BaseQuoteCcypair> map1 =baseQuoteCcypairService.selectCcypairmap();
		ArrayList<String> arr = new ArrayList<String>();
		for(int i=0;i<map1.size();i++){
			String str = map1.get(i).getCcyp();
			arr.add(str);
		}
		//map1.put("str", str1);
		String str1 = JSONObject.toJSONString(arr);
		return str1;
	}
}
