package com.singlee.quoteprice.controller;

import java.io.Serializable;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.quoteprice.model.BaseQuoteQtsbp;
import com.singlee.quoteprice.service.BaseQuoteQtsbpService;

@Controller
@RequestMapping(value= "/baseQuoteQtsbpController")
public class BaseQuoteQtsbpController {
	
	@Autowired
	BaseQuoteQtsbpService baseQuoteQtsbpService;
	
	@ResponseBody
	@RequestMapping(value = "/saveQtsbp")
	public RetMsg<Serializable> saveQtsbp(@RequestBody Map<String, Object> map){
		baseQuoteQtsbpService.saveQtsbp(map);
		return RetMsgHelper.ok();
	}
	
	@ResponseBody
	@RequestMapping(value = "/searchQtsbp")
	public RetMsg<PageInfo<BaseQuoteQtsbp>> searchQtsbp(@RequestBody Map<String, Object> map) {
		Page<BaseQuoteQtsbp> page = baseQuoteQtsbpService.searchQtsbp(map);
		return RetMsgHelper.ok(page);
	}
	
	@ResponseBody
	@RequestMapping(value = "/deleteQtsbp")
	public RetMsg<Serializable> deleteQtsbp(@RequestBody Map<String, Object> map){
		baseQuoteQtsbpService.deleteQtsbp(map);
		return RetMsgHelper.ok();
	}
	
	@ResponseBody
	@RequestMapping(value = "/updateQtsbp")
	public RetMsg<Serializable> updateQtsbp(@RequestBody Map<String, Object> map){
		baseQuoteQtsbpService.updateQtsbp(map);
		return RetMsgHelper.ok();
	}
}
