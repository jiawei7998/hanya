package com.singlee.quoteprice.controller;

import java.io.Serializable;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.quoteprice.model.BaseQuoteQtd;
import com.singlee.quoteprice.service.BaseQuoteQtdService;
import com.singlee.quoteprice.service.BaseQuoteService;

@Controller
@RequestMapping(value = "/baseQuoteQtdController")
public class BaseQuoteQtdController {
	@Autowired
	 BaseQuoteQtdService baseQuoteQtdService;
	//BaseQuoteQtd
	@ResponseBody
	@RequestMapping(value = "/saveQtd")
	public RetMsg<Serializable> saveQtd(@RequestBody Map<String, Object> map){
		baseQuoteQtdService.saveQtd(map);
		return RetMsgHelper.ok();
	}
	
	@ResponseBody
	@RequestMapping(value = "/searchQtd")
	public RetMsg<PageInfo<BaseQuoteQtd>> searchQtd(@RequestBody Map<String, Object> map) {
		Page<BaseQuoteQtd> page = baseQuoteQtdService.searchQtd(map);
		return RetMsgHelper.ok(page);
	}
	
	@ResponseBody
	@RequestMapping(value = "/deleteQtd")
	public RetMsg<Serializable> deleteQtd(@RequestBody Map<String, Object> map){
		baseQuoteQtdService.deleteQtd(map);
		return RetMsgHelper.ok();
	}
	
	@ResponseBody
	@RequestMapping(value = "/updateQtd")
	public RetMsg<Serializable> updateQtd(@RequestBody Map<String, Object> map){
		baseQuoteQtdService.updateQtd(map);
		return RetMsgHelper.ok();
	}
}
