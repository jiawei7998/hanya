package com.singlee.quoteprice.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.quoteprice.model.BaseQuoteHoliday;

public interface BaseQuoteMapper {

	public void saveHoliday(Map<String, Object> map);

	public Page<BaseQuoteHoliday> searchHoliday(Map<String, Object> map, RowBounds rowBounds);

	public void deleteHoliday(Map<String, Object> map);

	public void updateHoliday(Map<String, Object> map);

	public BaseQuoteHoliday checkHoliday(Map<String, Object> map);

}
