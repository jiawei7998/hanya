package com.singlee.quoteprice.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.quoteprice.model.BaseQuoteCcypair;

public interface BaseQuoteCcypairMapper {

	List<BaseQuoteCcypair> searchCcypair();

	void saveCcypair(Map<String, Object> map);

	Page<BaseQuoteCcypair> searchCcypairpage(Map<String, Object> map,
			RowBounds rowBounds);

	void deleteCcypair(Map<String, Object> map);

	void updateCcypairIsused(Map<String, Object> map);

	void updateCcypair(Map<String, Object> map);

	void updateCcypairAubp(BaseQuoteCcypair baseQuoteCcypair);

	List<BaseQuoteCcypair> selectCcypairmap();
}