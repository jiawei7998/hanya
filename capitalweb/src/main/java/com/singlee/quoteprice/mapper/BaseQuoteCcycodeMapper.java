package com.singlee.quoteprice.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.quoteprice.model.BaseQuoteCcycode;

public interface BaseQuoteCcycodeMapper {

	void saveCcycode(Map<String, Object> map);

	Page<BaseQuoteCcycode> searchCcycode(Map<String, Object> map,
			RowBounds rowBounds);

	void deleteCcycode(Map<String, Object> map);

	void updateCcycode(Map<String, Object> map);

	List<BaseQuoteCcycode> saveCcyMap();
    
	
}