package com.singlee.quoteprice.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.quoteprice.model.BaseQuoteExpt;

public interface BaseQuoteExptMapper {

	void updateExpt(Map<String, Object> map);

	Page<BaseQuoteExpt> searchExpt(Map<String, Object> map, RowBounds rowBounds);

}
