package com.singlee.quoteprice.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.singlee.quoteprice.model.BaseQuoteUccyp;

public interface BaseQuoteUccypMapper {

	List<BaseQuoteUccyp> searchUseUccyp(Map<String, Object> map);

	List<BaseQuoteUccyp> searchUnUseUccyp(Map<String, Object> map);

	void saveAllUccyp(Map<String, Object> map);

	void deleteAllUccyp(@Param("uid1") String uid1);

}
