package com.singlee.quoteprice.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.quoteprice.model.BaseQuoteQtd;

public interface BaseQuoteQtdMapper {

	void saveQtd(Map<String, Object> map);

	Page<BaseQuoteQtd> searchQtd(Map<String, Object> map, RowBounds rowBounds);

	void deleteQtd(Map<String, Object> map);

	void updateQtd(Map<String, Object> map);

	

}
