package com.singlee.quoteprice.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.quoteprice.model.BaseQuoteQtsbp;

public interface BaseQuoteQtsbpMapper {

	void saveQtsbp(Map<String, Object> map);

	Page<BaseQuoteQtsbp> searchQtsbp(Map<String, Object> map,
			RowBounds rowBounds);

	void deleteQtsbp(Map<String, Object> map);

	void updateQtsbp(Map<String, Object> map);

}
