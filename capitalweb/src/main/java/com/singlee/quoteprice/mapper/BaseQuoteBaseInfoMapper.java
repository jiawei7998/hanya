package com.singlee.quoteprice.mapper;

import java.util.Map;

import com.singlee.quoteprice.model.BaseQuoteBaseInfo;

public interface BaseQuoteBaseInfoMapper {

	BaseQuoteBaseInfo sercherBaseInfo(Map<String, Object> map);

	void updateBaseInfo(Map<String, Object> map);

}
