package com.singlee.quoteprice.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.quoteprice.model.BaseQuoteQts;

public interface BaseQuoteQtsMapper {

	void saveQts(Map<String, Object> map);

	Page<BaseQuoteQts> searchQts(Map<String, Object> map, RowBounds rowBounds);

	void deleteQts(Map<String, Object> map);

	void updateQts(Map<String, Object> map);

}
