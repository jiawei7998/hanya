package com.singlee.quoteprice.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.quoteprice.model.BaseQuoteAubp;

public interface BaseQuoteAubpMapper {

	void saveAubp(BaseQuoteAubp baseQuoteAubp);

	Page<BaseQuoteAubp> searchAubp(Map<String, Object> map, RowBounds rowBounds);

	void updateAubp(Map<String, Object> map);

}