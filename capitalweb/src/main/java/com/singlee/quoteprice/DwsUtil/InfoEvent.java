package com.singlee.quoteprice.DwsUtil;

import org.springframework.context.ApplicationEvent;




public class InfoEvent extends ApplicationEvent {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public InfoEvent(Object source) {
        super(source);
    }

}
