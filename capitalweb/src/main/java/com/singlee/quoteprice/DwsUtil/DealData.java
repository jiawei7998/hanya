package com.singlee.quoteprice.DwsUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.GetMethod;

import net.sf.json.JSONObject;



import com.singlee.quoteprice.model.MidPrice;
import com.singlee.quoteprice.model.WebSourceBean;
@SuppressWarnings("unchecked")
public class DealData {
	  private HashMap<String, String> map = new HashMap();
	  private static int conTimeOut;
	  private static int getTimeOut;
	public  List getWebData() throws IOException{
	
		List ablist = new ArrayList();
		String url = "http://www.chinamoney.com.cn/r/cms/www/chinamoney/data/fx/rfx-sp-quot.json?";
		InputStream is = new URL(url).openStream();
		try {
	
			BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
			System.out.println(rd.read());
			StringBuilder sb = new StringBuilder();
			int cp;
			while ((cp = rd.read()) != -1) {
				sb.append((char) cp);
			}
			String jsonStr = sb.toString();
			String str = "{"+jsonStr + "}";
			System.out.println(jsonStr);
			//JSONObject jsonBean = new JSONObject(str);
			JSONObject jsonBean = JSONObject.fromObject(str);
			//Map classMap = new HashMap();
			Map classMap = new HashMap();
			List<WebSourceBean> list = new ArrayList<WebSourceBean>();
//			System.out.println(jsonBean.get("records"));
//			String[] classMap =  jsonBean.get("records").toString().split(",");
//			System.out.println(classMap[0]);
//			String dt = (String) ((JSONObject)jsonBean.get("data")).get("showDateEN");
			classMap.put("records", WebSourceBean.class);
			MidPrice midPrice = (MidPrice) JSONObject.toBean(jsonBean, MidPrice.class, classMap);
			String dt = midPrice.getData().get("showDateEN");
			System.out.println(dt);
			for (WebSourceBean records : midPrice.getRecords()) {
				if (records.getCcyPair().startsWith("USD")) {
					ablist.add("1401|" + dt + "|" + records.getCcyPair() + "|" + records.getBidPrc() + "|" + records.getAskPrc() +  "|" + records.getBidPrc() + "|" + records.getAskPrc() + "|"
							+ records.getMidprice() + "|" + dt);
				} else if (records.getCcyPair().startsWith("HKD")) {
					ablist.add("1301|" + dt + "|" + records.getCcyPair() + "|" + records.getBidPrc() + "|" + records.getAskPrc() +  "|" + records.getBidPrc() + "|" + records.getAskPrc() + "|"
							+ records.getMidprice() + "|" + dt);
				} else if (records.getCcyPair().startsWith("100JPY")) {
					ablist.add("1901|" + dt + "|" + records.getCcyPair().substring(3, records.getCcyPair().length()) + "|" + records.getBidPrc() + "|" + records.getAskPrc() + "|" + records.getBidPrc() + "|" + records.getAskPrc() + "|"
							+ records.getMidprice() + "|" + dt);
				} else if (records.getCcyPair().startsWith("EUR")) {
					ablist.add("2201|" + dt + "|" + records.getCcyPair() + "|" + records.getBidPrc() + "|" + records.getAskPrc() + "|" + records.getBidPrc() + "|" + records.getAskPrc() + "|"
							+ records.getMidprice() + "|" + dt);
					
				} else if (records.getCcyPair().startsWith("GBP")) {
					ablist.add("1201|" + dt + "|" + records.getCcyPair() + "|" + records.getBidPrc() + "|" + records.getAskPrc() + "|" + records.getBidPrc() + "|" + records.getAskPrc() + "|"
							+ records.getMidprice() + "|" + dt);
				}
			}
		} finally {
			is.close();
		}
		return ablist;
	}
	
	  public List dealData2()
	    throws Exception
	  {
	    String s = "";
	    List boclist = new ArrayList();
	    try {
	      s = getWebText("http://www.boc.cn/sourcedb/whpj/");
	      if ((s == null) || ("".equals(s)) || (s.split("发布时间").length < 2) || 
	        (s.split("发布时间")[1] == null) || 
	        ("".equals(s.split("发布时间")[1])) || 
	        (s.split("发布时间")[1].split("往日外汇牌价搜索")[0] == null) || 
	        (s.split("发布时间")[1].split("往日外汇牌价搜索")[0].trim().split("\n").length == 0))
	      {
	        if ((s == null) || ("".equals(s))) {
                System.out.println("已丢弃：抓取数据为空：" + new Date().toString() + "\r\n");
            } else {
	          System.out.println("已丢弃：抓取数据有误：" + new Date().toString() + "|" + s + "\r\n");
	        }
	        return boclist;
	      }
	      s = s.split("发布时间")[1].split("往日外汇牌价搜索")[0];
	      String[] ss = s.trim().split("\n");
	      SimpleDateFormat format = new SimpleDateFormat(
	        "yyyy-MM-dd HH:mm:ss:SSS");
	      SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
	      ss = removeArrayItem(ss);
	      for (int i = 0; i < ss.length; i++) {
	        StringBuffer str = new StringBuffer();
	        if ("美元".equals(ss[i].trim())) {
	          if (!compareValue(i + 7, ss.length)) {
	            System.out.println("已丢弃：抓取数据过期(下表越界)：" + ss[i].trim() + new Date().toString() + "|本次时间" + ss[(i + 7)].trim() + "\r\n");
	            return new ArrayList();
	          }
	          str.append("1401|" + format.format(new Date()) + "|" + 
	            "USD/CNY" + "|" + 
	            Double.parseDouble(ss[(i + 2)].trim()) + "|" + 
	            Double.parseDouble(ss[(i + 3)].trim()) + "|" + 
	            Double.parseDouble(ss[(i + 1)].trim()) + "|" + 
	            Double.parseDouble(ss[(i + 3)].trim()) + "|" + 
	            Double.parseDouble(ss[(i + 5)].trim()) + "|" + 
	            ss[(i + 6)].trim() + " " + ss[(i + 7)].trim() + 
	            ":000");
	          boclist.add(str);
	          if (format2.parse(ss[(i + 6)].trim()).before(format2.parse(format2.format(new Date())))) {
	            System.out.println("已丢弃：抓取数据过期：" + ss[i].trim() + new Date().toString() + "|本次时间" + ss[(i + 7)].trim() + "|" + str.toString() + "\r\n");
	            return new ArrayList();
	          }
	          if (!this.map.containsKey(382 + ss[i].trim())) {
	            this.map.put(382 + ss[i].trim(), ss[(i + 6)].trim() + " " + ss[(i + 7)].trim() + ":000");
	          }
	          else if (format.parse(ss[(i + 6)].trim() + " " + ss[(i + 7)].trim() + ":000").before(format.parse((String)this.map.get(382 + ss[i].trim())))) {
	            System.out.println("已丢弃：" + ss[i].trim() + new Date().toString() + "|上次时间" + (String)this.map.get(new StringBuilder(String.valueOf(382)).append(ss[i].trim()).toString()) + "|本次时间" + ss[(i + 6)].trim() + " " + ss[(i + 7)].trim() + ":000" + "|" + str.toString() + "\r\n");
	            return new ArrayList();
	          }

	          this.map.put(382 + ss[i].trim(), ss[(i + 6)].trim() + " " + ss[(i + 7)].trim() + ":000");
	        }

	        if ("港币".equals(ss[i].trim())) {
	          if (!compareValue(i + 7, ss.length)) {
	            System.out.println("已丢弃：抓取数据过期(下表越界)：" + ss[i].trim() + new Date().toString() + "|本次时间" + ss[(i + 7)].trim() + "\r\n");
	            return new ArrayList();
	          }
	          str.append("1301|" + format.format(new Date()) + "|" + 
	            "HKD/CNY" + "|" + 
	            Double.parseDouble(ss[(i + 2)].trim()) + "|" + 
	            Double.parseDouble(ss[(i + 3)].trim()) + "|" + 
	            Double.parseDouble(ss[(i + 1)].trim()) + "|" + 
	            Double.parseDouble(ss[(i + 3)].trim()) + "|" + 
	            Double.parseDouble(ss[(i + 5)].trim()) + "|" + 
	            ss[(i + 6)].trim() + " " + ss[(i + 7)].trim() + 
	            ":000");
	          boclist.add(str);
	          if (format2.parse(ss[(i + 6)].trim()).before(format2.parse(format2.format(new Date())))) {
	            System.out.println("已丢弃：抓取数据过期：" + ss[i].trim() + new Date().toString() + "|本次时间" + ss[(i + 7)].trim() + "|" + str.toString() + "\r\n");
	            return new ArrayList();
	          }
	          if (!this.map.containsKey(382 + ss[i].trim())) {
	            this.map.put(382 + ss[i].trim(), ss[(i + 6)].trim() + " " + ss[(i + 7)].trim() + ":000");
	          }
	          else if (format.parse(ss[(i + 6)].trim() + " " + ss[(i + 7)].trim() + ":000").before(format.parse((String)this.map.get(382 + ss[i].trim())))) {
	            System.out.println("已丢弃：" + ss[i].trim() + new Date().toString() + "|上次时间" + (String)this.map.get(new StringBuilder(String.valueOf(382)).append(ss[i].trim()).toString()) + "|本次时间" + ss[(i + 6)].trim() + " " + ss[(i + 7)].trim() + ":000" + "|" + str.toString() + "\r\n");
	            return new ArrayList();
	          }

	          this.map.put(382 + ss[i].trim(), ss[(i + 6)].trim() + " " + ss[(i + 7)].trim() + ":000");
	        }

	        if ("日元".equals(ss[i].trim())) {
	          if (!compareValue(i + 7, ss.length)) {
	            System.out.println("已丢弃：抓取数据过期(下表越界)：" + ss[i].trim() + new Date().toString() + "|本次时间" + ss[(i + 7)].trim() + "\r\n");
	            return new ArrayList();
	          }
	          str.append("1901|" + format.format(new Date()) + "|" + 
	            "JPY/CNY" + "|" + 
	            Double.parseDouble(ss[(i + 2)].trim()) + "|" + 
	            Double.parseDouble(ss[(i + 3)].trim()) + "|" + 
	            Double.parseDouble(ss[(i + 1)].trim()) + "|" + 
	            Double.parseDouble(ss[(i + 3)].trim()) + "|" + 
	            Double.parseDouble(ss[(i + 5)].trim()) + "|" + 
	            ss[(i + 6)].trim() + " " + ss[(i + 7)].trim() + 
	            ":000");
	          boclist.add(str);
	          if (format2.parse(ss[(i + 6)].trim()).before(format2.parse(format2.format(new Date())))) {
	            System.out.println("已丢弃：抓取数据过期：" + ss[i].trim() + new Date().toString() + "|本次时间" + ss[(i + 7)].trim() + "|" + str.toString() + "\r\n");
	            return new ArrayList();
	          }
	          if (!this.map.containsKey(382 + ss[i].trim())) {
	            this.map.put(382 + ss[i].trim(), ss[(i + 6)].trim() + " " + ss[(i + 7)].trim() + ":000");
	          }
	          else if (format.parse(ss[(i + 6)].trim() + " " + ss[(i + 7)].trim() + ":000").before(format.parse((String)this.map.get(382 + ss[i].trim())))) {
	            System.out.println("已丢弃：" + ss[i].trim() + new Date().toString() + "|上次时间" + (String)this.map.get(new StringBuilder(String.valueOf(382)).append(ss[i].trim()).toString()) + "|本次时间" + ss[(i + 6)].trim() + " " + ss[(i + 7)].trim() + ":000" + "|" + str.toString() + "\r\n");
	            return new ArrayList();
	          }

	          this.map.put(382 + ss[i].trim(), ss[(i + 6)].trim() + " " + ss[(i + 7)].trim() + ":000");
	        }
	        if ("欧元".equals(ss[i].trim())) {
	          if (!compareValue(i + 7, ss.length)) {
	            System.out.println("已丢弃：抓取数据过期(下表越界)：" + ss[i].trim() + new Date().toString() + "|本次时间" + ss[(i + 7)].trim() + "\r\n");
	            return new ArrayList();
	          }
	          str.append("2201|" + format.format(new Date()) + "|" + 
	            "EUR/CNY" + "|" + 
	            Double.parseDouble(ss[(i + 2)].trim()) + "|" + 
	            Double.parseDouble(ss[(i + 3)].trim()) + "|" + 
	            Double.parseDouble(ss[(i + 1)].trim()) + "|" + 
	            Double.parseDouble(ss[(i + 3)].trim()) + "|" + 
	            Double.parseDouble(ss[(i + 5)].trim()) + "|" + 
	            ss[(i + 6)].trim() + " " + ss[(i + 7)].trim() + 
	            ":000");
	          boclist.add(str);
	          if (format2.parse(ss[(i + 6)].trim()).before(format2.parse(format2.format(new Date())))) {
	            System.out.println("已丢弃：抓取数据过期：" + ss[i].trim() + new Date().toString() + "|本次时间" + ss[(i + 7)].trim() + "|" + str.toString() + "\r\n");
	            return new ArrayList();
	          }
	          if (!this.map.containsKey(382 + ss[i].trim())) {
	            this.map.put(382 + ss[i].trim(), ss[(i + 6)].trim() + " " + ss[(i + 7)].trim() + ":000");
	          }
	          else if (format.parse(ss[(i + 6)].trim() + " " + ss[(i + 7)].trim() + ":000").before(format.parse((String)this.map.get(382 + ss[i].trim())))) {
	            System.out.println("已丢弃：" + ss[i].trim() + new Date().toString() + "|上次时间" + (String)this.map.get(new StringBuilder(String.valueOf(382)).append(ss[i].trim()).toString()) + "|本次时间" + ss[(i + 6)].trim() + " " + ss[(i + 7)].trim() + ":000" + "|" + str.toString() + "\r\n");
	            return new ArrayList();
	          }

	          this.map.put(382 + ss[i].trim(), ss[(i + 6)].trim() + " " + ss[(i + 7)].trim() + ":000");
	        }
	        if ("英镑".equals(ss[i].trim())) {
	          if (!compareValue(i + 7, ss.length)) {
	            System.out.println("已丢弃：抓取数据过期(下表越界)：" + ss[i].trim() + new Date().toString() + "|本次时间" + ss[(i + 7)].trim() + "\r\n");
	            return new ArrayList();
	          }
	          str.append("1201|" + format.format(new Date()) + "|" + 
	            "GBP/CNY" + "|" + 
	            Double.parseDouble(ss[(i + 2)].trim()) + "|" + 
	            Double.parseDouble(ss[(i + 3)].trim()) + "|" + 
	            Double.parseDouble(ss[(i + 1)].trim()) + "|" + 
	            Double.parseDouble(ss[(i + 3)].trim()) + "|" + 
	            Double.parseDouble(ss[(i + 5)].trim()) + "|" + 
	            ss[(i + 6)].trim() + " " + ss[(i + 7)].trim() + 
	            ":000");
	          boclist.add(str);
	          if (format2.parse(ss[(i + 6)].trim()).before(format2.parse(format2.format(new Date())))) {
	            System.out.println("已丢弃：抓取数据过期：" + ss[i].trim() + new Date().toString() + "|本次时间" + ss[(i + 7)].trim() + "|" + str.toString() + "\r\n");
	            return new ArrayList();
	          }
	          if (!this.map.containsKey(382 + ss[i].trim())) {
	            this.map.put(382 + ss[i].trim(), ss[(i + 6)].trim() + " " + ss[(i + 7)].trim() + ":000");
	          }
	          else if (format.parse(ss[(i + 6)].trim() + " " + ss[(i + 7)].trim() + ":000").before(format.parse((String)this.map.get(382 + ss[i].trim())))) {
	            System.out.println("已丢弃：" + ss[i].trim() + new Date().toString() + "|上次时间" + (String)this.map.get(new StringBuilder(String.valueOf(382)).append(ss[i].trim()).toString()) + "|本次时间" + ss[(i + 6)].trim() + " " + ss[(i + 7)].trim() + ":000" + "|" + str.toString() + "\r\n");
	            return new ArrayList();
	          }

	          this.map.put(382 + ss[i].trim(), ss[(i + 6)].trim() + " " + ss[(i + 7)].trim() + ":000");
	        }
	      }
	      return boclist;
	    } catch (Exception e) {
	    }
		return boclist;
	  }
	  
	  
	  public static String getWebText(String url)
	  {
	    String content = "";
	    GetMethod getMethod = null;
	    try {
	      HttpClient httpClient = new HttpClient();

	      httpClient.getHttpConnectionManager().getParams()
	        .setConnectionTimeout(conTimeOut);

	      getMethod = new GetMethod(url);

	      getMethod.getParams().setParameter("http.socket.timeout", 
	        Integer.valueOf(getTimeOut));

	      getMethod.getParams().setParameter("http.method.retry-handler", 
	        new DefaultHttpMethodRetryHandler());
	      httpClient.getParams().setParameter(
	        "http.protocol.content-charset", "utf-8");

	      int statusCode = httpClient.executeMethod(getMethod);

	      if (statusCode != 200) {
	        System.err.println("Method failed: " + 
	          getMethod.getStatusLine());
	      }
	      content = CatchWebUtil.Html2Text(getMethod
	        .getResponseBodyAsString().replaceAll("&nbsp;", ""));
	    }
	    catch (HttpException e) {
	      e.printStackTrace();
	    }
	    catch (Exception e) {
	      e.printStackTrace();
	    }
	    finally {
	      getMethod.releaseConnection();
	    }
	    return content;
	  }
	  public static String[] removeArrayItem(String[] arrs)
	  {
	    try
	    {
	      int len = arrs.length;
	      List list = new ArrayList();
	      for (int i = 0; i < len; i++) {
	        if ((arrs[i] != null) && (!"\n".equals(arrs[i].trim())) && (!"".equals(arrs[i].trim())) && (!"\r".equals(arrs[i].trim()))) {
	          list.add(arrs[i]);
	        }
	      }
	      int length = list.size();

	      for (int i = 0; i < length; i++) {
	        String str = (String)list.get(i);

	        if ((str.matches("[\\u4E00-\\u9FA5]+")) && (!str.startsWith("英镑")) && (!str.startsWith("港币")) && (!str.startsWith("美元")) && (!str.startsWith("日元")) && (!str.startsWith("欧元"))) {
	          list.remove(i--);

	          int k = 1;
	          for (int j = i + 1; j < length - 1; j++) {
	            String del = (String)list.get(j);
	            list.remove(j--);
	            k++;
	            if ((del.contains(":")) || (del.endsWith("页"))) {
	              break;
	            }
	          }
	          length -= k;
	        }

	      }

	      arrs = (String[])list.toArray(new String[list.size()]);
	    } catch (Exception e) {
	      e.printStackTrace();
	    }
	    return arrs;
	  }
	  public static boolean compareValue(int num1, int num2)
	  {
	    if (num1 <= num2)
	    {
	      return true;
	    }
	    return false;
	  }
	  
	  public static void main(String[] args) throws Exception {
		  DealData data2 = new DealData();
		 List list= data2.dealData2();
		 for(int i=0;i<list.size();i++){
			 System.out.println(list.get(i));
		 }
	}
}
