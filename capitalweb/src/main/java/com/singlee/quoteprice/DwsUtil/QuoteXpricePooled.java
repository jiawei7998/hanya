package com.singlee.quoteprice.DwsUtil;


import com.alibaba.fastjson.JSONObject;
import com.singlee.quoteprice.model.BaseQuoteCcypair;
import com.singlee.quoteprice.model.PerformInfo;
import com.singlee.quoteprice.model.SendBean;
import com.singlee.quoteprice.service.BaseQuoteCcypairService;

import org.apache.commons.lang.ArrayUtils;
import org.directwebremoting.ScriptSession;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class QuoteXpricePooled implements Runnable, ApplicationContextAware {
    private ApplicationContext ctx;
    private Map<String, Object> result= new HashMap<String, Object>();//返回到监听页面的结果
    private List<SendBean> param = new ArrayList<SendBean>();//请求数据参数
    private List<BaseQuoteCcypair> list1 = new ArrayList<BaseQuoteCcypair>();
    private PerformInfo info=new PerformInfo();
    @Autowired
    BaseQuoteCcypairService baseQuoteCcypairService;

    @Override
    public void run() {
        while (true) {
            try {
                Collection<ScriptSession> scriptSessions= DWRScriptSessionListener.getScriptSessions();
                Object[] keys=DWRScriptSessionListener.scriptSessionMap.keySet().toArray();
                for (ScriptSession scriptSession : scriptSessions) {
                    if(scriptSession.isInvalidated()){
                        System.out.println(new Date().toString()+" scriptSession.isInvalidated "+scriptSession.getId());
                        for (Object key : keys) {
                            if(DWRScriptSessionListener.scriptSessionMap.get(key)==scriptSession){
                                System.out.println(new Date().toString()+" scriptSessionMap remove  [key="+key+" , value= "+scriptSession.getId()+" ]");
                                DWRScriptSessionListener.scriptSessionMap.remove(key);
                                System.gc();
                            }
                        }
                    }
                }
                if(DWRScriptSessionListener.scriptSessionMap.size()>0){
                    getXpriceQuotedate();
                }
                Thread.sleep(2000);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    
    public void getXpriceQuotedate() throws IOException{
    			
    			
    			result.clear();
    			Date date = new Date();
    			java.text.DateFormat format = new java.text.SimpleDateFormat("HH:mm:ss");
    			param.clear();
    			DealData data = new DealData();
    			List list = data.getWebData();
    			List<BaseQuoteCcypair> list1=baseQuoteCcypairService.selectCcypairmap();
    			for(int i=0;i<list1.size();i++){
    				for(int n=0;n<list.size();n++){
    					if(list1.get(i).equals(list.get(n).toString().split("\\|")[n])){
    						
    					}else if(n==list.size()-1){
    						list.remove(n);
    						n--;
    						System.out.println(list.size());
    					}
    					// ArrayUtils.contains(list,list1.get(i));
    				}
    			}
    			for(int i=0;i<list.size();i++){
    				SendBean sendBean = new SendBean();
    				String[] str = list.get(i).toString().split("\\|");
    				sendBean.setCcyp(str[2]);
        			sendBean.setWebcbuy(str[3]);
        			sendBean.setWebcenter("---".equals(str[7])?"0.0":str[7]);
        			sendBean.setWebcsale(str[4]);
        			sendBean.setWebrbuy(str[5]);
        			sendBean.setWebrsale(str[6]);
        			param.add(sendBean);
        			
    			}
         	 result.put("websouce", param);
             info.setId(1);
             info.setMsg(JSONObject.toJSONString(result));
             info.setTime(new Date());
             
             ctx.publishEvent(new InfoEvent(info));//传递EVENT到监听页面
             System.out.println(new Date().toString()+"====publishEvent====");

    }
    public String getCcyp(){
//    	this.list1=baseQuoteCcypairService.selectCcypairmap();
//    	List<String> arr =new ArrayList();
//    	for(int i = 0;i<list1.size();i++){
//    		arr.add(list1.get(i).getCcyp());
//    	}
//    	String str = JSONObject.toJSONString(arr);
    	return null;
    }
    public ApplicationContext getCtx() {
        return ctx;
    }
    public void setCtx(ApplicationContext ctx) {
        this.ctx = ctx;
    }
   
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.ctx=applicationContext;
    }

    
}
