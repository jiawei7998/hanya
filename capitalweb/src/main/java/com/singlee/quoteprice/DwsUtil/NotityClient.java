package com.singlee.quoteprice.DwsUtil;



import org.directwebremoting.Browser;
import org.directwebremoting.ScriptBuffer;
import org.directwebremoting.ScriptSession;
import org.directwebremoting.ScriptSessionFilter;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.web.context.ServletContextAware;

import com.singlee.quoteprice.model.PerformInfo;

import java.util.Collection;
import java.util.Date;

import javax.servlet.ServletContext;

@SuppressWarnings("rawtypes")
public class NotityClient implements ApplicationListener,ServletContextAware,ApplicationContextAware{
   
    @SuppressWarnings("unused")
    private ServletContext servletContext=null;
    @SuppressWarnings("unused")
    private ApplicationContext applicationContext=null;
    
    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        if(event instanceof InfoEvent){
           final PerformInfo info=(PerformInfo)event.getSource();
           this.send(info.getReceiver(), info.getMsg());
        }
    }

    private void send(final String receiverid,final String msg){
    	System.out.println(receiverid);
    	System.out.println("11111111111111111111111111111111111111111");
        Browser.withAllSessionsFiltered(new ScriptSessionFilter() {
            @Override
            public boolean match(ScriptSession session) {
//               if(session.getAttribute("userInfo")==null){
//                   return false;
//               }else{
                   return true;
//               }
            }
        }, new Runnable() {
            @Override
            public void run() {
                //Collection<ScriptSession> collection=Browser.getTargetSessions();
                Collection<ScriptSession> collection=DWRScriptSessionListener.getScriptSessions();
                for (ScriptSession scriptSession : collection) {
                    scriptSession.addScript(initFunctionCall("putInfo", msg));
                }
            }
        });
    }
       
    private ScriptBuffer initFunctionCall(String funcName,Object object){
        ScriptBuffer script=new ScriptBuffer();
        script.appendScript(funcName).appendScript("(");
        script.appendData(object);
        script.appendScript(")");
        System.out.println(new Date().toString()+"ScriptBuffer  putInfo ");
        return script;
    }
    
    @Override
    public void setServletContext(ServletContext servletContext) {
        this.servletContext=servletContext;
    }
    public void script() {
    	Collection<ScriptSession> collection=DWRScriptSessionListener.getScriptSessions();
        for (ScriptSession scriptSession : collection) {
            System.out.println(scriptSession);
        }
    }
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext=applicationContext;
    }
    
}
