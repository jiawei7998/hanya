package com.singlee.quoteprice.DwsUtil;


import org.directwebremoting.ScriptSession;
import org.directwebremoting.WebContextFactory;
import org.directwebremoting.event.ScriptSessionEvent;
import org.directwebremoting.event.ScriptSessionListener;

import com.singlee.capital.common.session.SessionService;
import com.singlee.capital.common.session.SlSession;
import com.singlee.capital.system.model.TaUserInfo;
import com.singlee.capital.system.session.impl.SlSessionHelper;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

public class DWRScriptSessionListener implements ScriptSessionListener {
    
    /* 维护一个Map key为session的id，value为ScriptSession对象  */
    public static final Map<String, ScriptSession> scriptSessionMap=new HashMap<String, ScriptSession>();
   
    /**
     * ScriptSession创建事件
     */
    @Override
    public void sessionCreated(ScriptSessionEvent event) {
        HttpSession session=WebContextFactory.get().getSession();
        session.setMaxInactiveInterval(-1);
        ScriptSession scriptSession=event.getSession();
        
//        System.out.println((SlSession)session.getAttribute(SessionService.SessionKey));
//        TaUserInfo infoResult=(TaUserInfo)session.getAttribute("userId");
        scriptSession.setAttribute("userInfo", session.getId());
        scriptSessionMap.put(session.getId(), scriptSession);
        System.out.println("scriptSessionMap put:[ session= "+session.getId()+"  ][ scriptSession="+scriptSession.getId()+"]"
                + " setAttribute userinfo= ["+session.getId()+"]");
    }
    /**
     * ScriptSession销毁
     */
    @Override
    public void sessionDestroyed(ScriptSessionEvent event) {
        if(WebContextFactory.get()!=null){
            HttpSession session=WebContextFactory.get().getSession();
            ScriptSession scriptSession=scriptSessionMap.remove(session.getId());
            System.out.println(new Date().toString()+"scriptSessionMap remove  [key="+session.getId()+" , value= "+" ]");
        }
        System.out.println(new Date().toString()+"ScriptSession Destroyed "+event.getSession().getId());
    }
    
    public static Collection<ScriptSession> getScriptSessions(){
        return scriptSessionMap.values();
    }
}
