<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="./global.jsp"%>
<head>
<title>IFBM@<%=__bizDate %></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
 <link href="menu/res/menu/menu.css?64987981656" rel="stylesheet" type="text/css" />
 <script src="menu/res/menu/menu.js" type="text/javascript"></script>
 <link href="menu/res/tabs.css" rel="stylesheet" type="text/css" />
 <link href="menu/res/frame.css" rel="stylesheet" type="text/css" />
 <link href="menu/res/index.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">

function parseQueryString(url){
	var obj={},
	queryStr=url.substr(url.lastIndexOf('?')+1,url.length)
	arr=queryStr.split('&');
	for(var i=0,len=arr.length;i<len;i++){
	var tmp=arr[i].split('=');
		obj[tmp[0]]=tmp[1];
	}
	return obj;
}
 
</script>
<style type="text/css">
.logo_shb {
  display: inline-block;
  width: 204px;
  height: 54px;
  margin: 10px 10px 0 0;
  background-image:url('./style/images_shb/logo.png');
  background-repeat:no-repeat;
}
</style>
</head>
<body id="body">

<div class="navbar">
    <div class="navbar-header">
        <div class="navbar-brand"> <img alt="" draggable="false" src="<%=basePath%>/miniScript/logo/<%=__brcc.getBranchLogo()%>.png" style="height:30px"></div>
        <div class="header-tabs">
        		<div class="tabs-item" style="width:340px;">
        			<div style="margin-top:15px;color: #333333;font-size: 19px;font-family: arial;"><%=__brcc.getSystemName()%><sup><font color="#CD2626">V2.0</font></sup></div>
        		</div>
        </div>
    </div>
    <div class="nav navbar-nav top-menu">
        <div id="mainMenu"></div>
    </div>
    <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><i class="fa fa-user"></i></a></li>
        <li><a href="#"><i class="fa fa-comments"></i></a></li>
        <li><a href="#"><i class="fa fa-tasks"></i></a></li>
        <li class="dropdown">
            <a class="dropdown-toggle userinfo">
                <img class="user-img" src="menu/res/images/user.jpg" />个人资料<i class="fa fa-angle-down"></i>
            </a>
            <ul class="dropdown-menu pull-right">
                <li id="pwdUpdate"><a href="#"><i class="fa fa-user "></i>密码设置</a></li>
                <li class="divider"></li>
                <li>
                    <a href="#"> <i class="fa fa-question-circle-o"></i>帮助文档</a>
                </li>
                <!-- <li><a href="#"><i class="fa fa-arrows-alt"></i>全屏</a></li>
                <li><a href="#"><i class="fa fa-lock "></i>锁住系统</a></li> -->
                <li id="exit"><a href="#"><i class="fa fa-key "></i>注销</a></li>
            </ul>
        </li>
    </ul>
</div>

<div class="container">
    <div id="mainTabs" class="mini-tabs" activeIndex="0" style="height:100%;" plain="false"
            buttons="#tabsButtons" arrowPosition="side">
        <div name="index" iconCls="fa-android" title="系统首页" url="Fund/portal.jsp" uid="mainPage">
            MiniUI
        </div>
    </div>
    <div id="tabsButtons">
        <a class="tabsBtn" onclick="activeTab2()"><i class="fa fa-home"></i></a>
        <a class="tabsBtn" onclick="refleshTab2()"><i class="fa fa-refresh"></i></a>
    </div>
</div>
<%-- 
	<div class="header">
        <div class="logo" style="background:#fff;">
            <i class="fa" style="padding-left:10px;background-color:white"></i>
            <img alt="" draggable="false" src="<%=basePath%>/miniScript/logo/<%=__brcc.getBranchLogo()%>.png" style="height:30px">
        </div>
        <i class="menu-hide-iocn"></i>
        <div class="header-tabs">
        		<div class="tabs-item" style="width:280px;">
        			<span class="item-text2">同业业务管理系统V2.0</span>
        		</div>
        </div>
        <div class="header-nav">

            <ul class="nav-bar">
                <li><div class="nav-item">我的工作台</div></li>
                <li class="dropdown">
                    <div class="nav-item  dropdown-toggle">
                        <img src="./Fund/Professional_menu/photo/user2-160x160.jpg" />
                        个人资料
                        <i class="fa fa-angle-down"></i>
                    </div>
                    <ul class="dropdown-menu">
                        <li>
                            <i class="fa fa-user"></i>退出登录
                        </li>
                        <li><i class="fa fa-eye "></i>切换账号</li>
                    </ul>
                </li>

            </ul>
        </div>

    </div>
    <div class="sidebar">
       <div id="left-menu" style="border-right:1px solid #cbcbcb;"></div>
    </div>
     <div class="main" style="border: solid 1px #cbcbcb;" id="main_tabs">
        <div class="mini-fit">
            <div id="mainTabs" class="mini-tabs" activeIndex="0" style="width:100%;height:100%;"
                    plain="true" contextMenu="#tabsMenu" >
            		<div name="first" id="first" title="我的工作台" url="Fund/portal.jsp"></div>
            </div>
            </div>
        </div>
    </div>
    <div class="footer"><span>CopyRight © 2018-2027 杭州新利科技有限公司 版权所有</span><div></div></div>
    <ul id="tabsMenu" class="mini-contextmenu" onbeforeopen="onBeforeOpen">        
        <li onclick="closeTab">关闭标签页</li>                
	    <li onclick="closeAllBut">关闭其他标签页</li>
	    <li onclick="closeAll">关闭所有标签页</li>        
        <li onclick="closeAllButFirst">关闭其他[首页除外]</li>   
    </ul>
    
 --%>
	
</body>
<script>
mini.parse();
top["win"] = window;
var tabs=mini.get("mainTabs");

function activeTab2(){
	var tab = tabs.getTab("index");
	tabs.activeTab(tab);
}

function refleshTab2(){
	/* var tab1=tabs.getActiveTab();
	var tab2=mini.clone(tab1); */
} 

$(document).ready(function () {
    
    //menu
    var menu = new Menu("#mainMenu", {
        itemclick: function (item) {
            if (!item.children) {
                activeTab(item);
            }
        }
    });

     CommonUtil.ajax({
        url:"/deskMenuController/getDeskMenus",
        data:{'branchId':branchId,'userId':userId,'instId':instId,'roleId':roleId},
		callback:function(data){
			if(data.obj){
				 menu.loadData(data.obj);
			}
		}
	}); 
    //dropdown
    $(".dropdown-toggle").click(function (event) {
        $(this).parent().addClass("open");
        return false;
    });

    $(document).click(function (event) {
        $(".dropdown").removeClass("open");
    });
});

//tab监听事件
tabs.on("tabdestroy",function(e){
  if(e.tab.parentId && tabs.getTabIFrameEl(e.tab.parentId) && typeof(tabs.getTabIFrameEl(e.tab.parentId).contentWindow.search)=="function"){
      try {
          tabs.getTabIFrameEl(e.tab.parentId).contentWindow.search(10,0);
      } catch (error) {
          
      }
      
  }else if(e.tab.params && e.tab.params.pWinId && e.tab.params.closeFun){
      try {
          eval('top["'+e.tab.params.pWinId+'"].'+e.tab.params.closeFun+'()');
      } catch (error) {
          
      }

  }
});

function onValueChanged(e) {
  var item = e.selected;
  if (item) {
      var tab ={
			id:'quickSearch',
			name:'quickSearch',
			title:item.moduleName,
			url: CommonUtil.baseWebPath() + item.moduleUrl,
			showCloseButton:true,
			parentId:top['win'].tabs.getActiveTab().name 
		};
		var param ={};
		//打开Tabs
      CommonUtil.openNewMenuTab(tab,param);
  }
}

function onBeforeLoad(e){
e.contentType="application/json;charset=UTF-8";
e.data.userId =userId;
e.data.instId =instId;
e.data.roleId =roleId;
e.data.branchId = branchId;
e.data =$.toJSON(e.data);
}
function activeTab(item) {
  var tabs = mini.get("mainTabs");
  var tab = tabs.getTab(item.uid);
  var node = $(this)[0];
  if (!tab) {
	  tab = {};
      tab.name = item.uid;
      tab.title = item.text;
      tab.showCloseButton = true;
      tab.url = item.url;
      tab.deskId =item.uid;
      tab.parentId = node.parentId;
      tab.iconCls = item.icon;
      tabs.addTab(tab);
      
    /*   tab = { name: item.uid, title: item.text, url: item.url, iconCls: item.icon, showCloseButton: true };
      tab = tabs.addTab(tab); */
  }
  tabs.activeTab(tab);
}
   


/***************************************************
 *                                                *
 *               以下为功能性切换                   *
 * ===============================================*
 *                                                *
 *                                                *
 *************************************************/
 /* $(document.body).on("click", ".menu-title", function () {
        var node = $(this)[0];
        node.text = node.innerText;
        node.url = $(node).attr("url");
        node.id = $(node).attr("uid");
        node.name = $(node).attr("name");
        node.branchId = branchId;
        node.userId =userId;
        node.instId =instId;
        node.roleId =roleId;
        var tabs = mini.get("mainTabs");
        showTab(node);

    });*/

    function showTab(node) {
        var id = "tab$" + node.id
        var tab = tabs.getTab(id);
        if (!tab) {
            tab = {};
            tab.name = id;
            tab.title = node.text;
            tab.showCloseButton = true;
            tab.url = node.url;
            tab.deskId =node.name;
            tab.parentId = node.parentId;
            tabs.addTab(tab);
        }
        tabs.activeTab(tab);
    }
    
    function openNewTab(tab,params) {
        if (CommonUtil.isNull(tabs.getTab(tab.name))){
            tab.params = params;
            tabs.addTab(tab);
		}
		tabs.activeTab(tabs.getTab(tab.name));
    }
    
    /**
     * 关闭Tab 并刷新父窗体.需要设置parentId 
     **/
    function closeMenuTab(){
        var activeTab = top["win"].tabs.getActiveTab();
        if(activeTab){
            tabs.removeTab(activeTab.name);
        }
               
	}

    var currentTab = null;

    function onBeforeOpen(e) {
        currentTab = tabs.getTabByEvent(e.htmlEvent);
        if (!currentTab) {
            e.cancel = true;
        }
    }
    ///////////////////////////
    function closeTab() {
        tabs.removeTab(currentTab);
    }
    function closeAllBut() {
        tabs.removeAll(currentTab);
    }
    function closeAll() {
        tabs.removeAll();
    }
    function closeAllButFirst() {
        var but = [currentTab];
        but.push(tabs.getTab("first"));
        tabs.removeAll(but);
    }
    
    /**
     * 系统安全退出
     * */
    $("#exit").click(function () {
        mini.confirm('您确认要退出系统吗？','系统提示',function(r){   
            if (r=='ok'){   
                CommonUtil.ajax({
                    url:"/UserController/logout",
                    callback : function(data) {
                        //window.location.href=data.desc;
                        window.location.href="./login.jsp";
                        //window.location.href="http://10.240.41.10:31201/iamgate/index.jsp";//测试
                        //window.location.href="http://iam.bosc/iamgate/index.jsp";
                    }
                });
            }   
        });

    });
    /**
     * 密码修改
     * */
     $("#pwdUpdate").click(function () {
        mini.open({
            title:'密码设置',
            showModal: true,
            allowResize: true,
            height:300,width:500,
            url:'./PasswordChange.jsp',
           ondestroy:function(action){
             if(action == "ok"){
                 //退出登录
                CommonUtil.ajax({
                    url:"/UserController/logout",
                    callback : function(data) {
                        window.location.href="./login.jsp";
                    }
                });
             }
           }
        });
    });
</script>
</script>
</html>