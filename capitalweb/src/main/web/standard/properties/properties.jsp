<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ include file="../global.jsp"%>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
<title></title>
</head>
<body style="width:100%;height:100%;background:white">
<div  id="field_form"  class="mini-fit area" style="background:white" >
	<div id="datagrid" class="mini-panel" title="配置文件修改" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
		<div class="leftarea">
			<input id="swift.ftp.ip" name="swift.ftp.ip" class="mini-textbox" labelField="true" requiredErrorText="该输入项为必输项" required="true"  label="swiftFTP IP 地址："  style="width:100%;"  labelStyle="text-align:left;width:130px;"  />
			<input id="swift.ftp.port" name="swift.ftp.port" class="mini-textbox" labelField="true" requiredErrorText="该输入项为必输项" required="true"  label="swiftFTP 端口："  labelStyle="text-align:left;width:130px;" style="width:100%;" vtype="maxLength:40"/>
			<input id="swift.ftp.userName" name="swift.ftp.userName" class="mini-textbox" labelField="true" requiredErrorText="该输入项为必输项" required="true"  label="swiftFTP 用户名："  labelStyle="text-align:left;width:130px;" style="width:100%;"  onvalidation="onEnglishAndNumberValidation" vtype="maxLength:18"/>
			<input id="swift.ftp.password" name="swift.ftp.password" class="mini-textbox" labelField="true" requiredErrorText="该输入项为必输项" required="true"  label="swiftFTP 密码："  labelStyle="text-align:left;width:130px;" style="width:100%;"  vtype="maxLength:40" />
			<input id="swift.ftp.localpath" name="swift.ftp.localpath" class="mini-textbox" labelField="true" requiredErrorText="该输入项为必输项" required="true"  label="swiftFTP 本地路径："  labelStyle="text-align:left;width:130px;" style="width:100%;"   onvalidation="onEnglishAndNumberValidation" vtype="maxLength:18"/>
			<input id="swift.ftp.bakuppath" name="swift.ftp.bakuppath" class="mini-textbox" labelField="true" requiredErrorText="该输入项为必输项" required="true"  label="swiftFTP 备份地址："  labelStyle="text-align:left;width:130px;" style="width:100%;"   onvalidation="onEnglishAndNumberValidation" vtype="maxLength:23"/>
			<input id="swift.ftp.remotepath" name="swift.ftp.remotepath" class="mini-textbox" labelField="true" requiredErrorText="该输入项为必输项" required="true"  label="swiftFTP 目标地址："  labelStyle="text-align:left;width:130px;" style="width:100%;"  onvalidation="onEnglishAndNumberValidation" vtype="maxLength:18"/>
			<input id="swift.ftp.imtf9" name="swift.ftp.imtf9" class="mini-textbox" labelField="true" requiredErrorText="该输入项为必输项" required="true"  label="swiftFTP 文件名称："  labelStyle="text-align:left;width:130px;" style="width:100%;"  onvalidation="onEnglishAndNumberValidation" vtype="maxLength:18"/>
			<input id="swift.ftp.ftptype" name="swift.ftp.ftptype" class="mini-textbox" labelField="true" requiredErrorText="该输入项为必输项" required="true"  label="swiftFTP 传输方式："  labelStyle="text-align:left;width:130px;" style="width:100%;"  vtype="maxLength:40" />
		</div>
		<div class="rightarea">
			<input id="esb.bap.pip" name="esb.app.ip" class="mini-textbox" labelField="true" requiredErrorText="该输入项为必输项" required="true"  label="ESB 应用服务器地址："  style="width:100%;"  labelStyle="text-align:left;width:130px;"  "/>
			<input id="esb.app.port" name="esb.app.port" class="mini-textbox" labelField="true" requiredErrorText="该输入项为必输项" required="true"  label="ESB 应用服务器端口："  labelStyle="text-align:left;width:130px;" style="width:100%;" vtype="maxLength:40" />
			<input id="esb.ftp.ip" name="esb.ftp.ip" class="mini-textbox" labelField="true" requiredErrorText="该输入项为必输项" required="true"  label="ESB 文件服务器地址："  labelStyle="text-align:left;width:130px;" style="width:100%;" vtype="maxLength:40"/>
			<input id="esb.ftp.port" name="esb.ftp.port" class="mini-textbox" labelField="true"  label="ESB 文件服务器端口：" requiredErrorText="该输入项为必输项" required="true"  labelStyle="text-align:left;width:130px;" style="width:100%;" onvalidation="onEnglishAndNumberValidation" vtype="maxLength:20" />
			<input id="esb.ftp.userName" name="esb.ftp.userName" class="mini-textbox" labelField="true"  label="ESB文件服务器用户名：" requiredErrorText="该输入项为必输项" required="true"  labelStyle="text-align:left;width:130px;" style="width:100%;" onvalidation="onEnglishAndNumberValidation" vtype="maxLength:20" />
			<input id="esb.ftp.password" name="esb.ftp.password" class="mini-textbox" labelField="true" requiredErrorText="该输入项为必输项" required="true"  label="ESB文件服务器密码："  style="width:100%;"  labelStyle="text-align:left;width:130px;" data="CommonUtil.serverData.dictionary.Currency" />
			<input id="esb.ftp.remotepath" name="esb.ftp.remotepath" class="mini-textbox" labelField="true" requiredErrorText="该输入项为必输项" required="true"  label="ESB文件服务器路径："  labelStyle="text-align:left;width:130px;" style="width:100%;" vtype="maxLength:40" />
			<input id="esb.ftp.create.localpath" name="esb.ftp.create.localpath" class="mini-textbox" labelField="true" requiredErrorText="该输入项为必输项" required="true"  label="本地文件生成路径："  labelStyle="text-align:left;width:130px;" style="width:100%;" vtype="maxLength:40"/>
			<input id="esb.ftp.receive.localpath" name="esb.ftp.receive.localpath" class="mini-textbox" labelField="true"  label="本地存放他系统下行文件路径 ：" requiredErrorText="该输入项为必输项" required="true"  labelStyle="text-align:left;width:130px;" style="width:100%;" onvalidation="onEnglishAndNumberValidation" vtype="maxLength:20" />
			<input id="esb.ftp.acup" name="esb.ftp.acup" class="mini-textbox" labelField="true"  label="总账文件名 路径：" requiredErrorText="该输入项为必输项" required="true"  labelStyle="text-align:left;width:130px;" style="width:100%;" onvalidation="onEnglishAndNumberValidation" vtype="maxLength:20" />
			<input id="esb.ftp.nupd" name="esb.ftp.nupd" class="mini-textbox" labelField="true" requiredErrorText="该输入项为必输项" required="true"  label="往来帐文件名 路径："  style="width:100%;"  labelStyle="text-align:left;width:130px;" data="CommonUtil.serverData.dictionary.Currency" />
		</div>	
	</div>
		<a id="search_btn" class="mini-button" style="display: none"  onclick="getPropertiesList()">查询</a>
	  	<a id="search_btn" class="mini-button" style="display: none"  onclick="save()">提交</a>
</div>
	<script type="text/javascript">
	mini.parse();
	//获取当前tab
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	function save(){
		var form = new mini.Form("field_form");
		if (form.isValid() == false) {
			mini.alert("信息填写有误，请重新填写","系统提示");
			return;
		} 
		
		var data = form.getData(false,false);	//获取表单多个控件的数据
		
		var param = [];
		$.each(data,function(i,v){
			var json = {"key":i,"value":v,"commont":""};
			param.push(json);
		});
		
		mini.confirm("确认以当前数据为准吗？","确认",function (action) {
			if (action != "ok") {
				return;
			}
			CommonUtil.ajax({
			    url: "/IpropertiesController/editProperties",
			    data:param,
			    callback:function (data) {
			    	mini.alert(data,'提示信息',function(){
						if(data=='修改成功'){
							top["win"].closeMenuTab();
						}
					});
			    }
			});
		});
	}
		
	function getPropertiesList(){
		CommonUtil.ajax( {
			url:"/IpropertiesController/getPropertiesList",
			data:params,
			callback : function(data) {
				var form = new mini.Form("field_form");
				var result = {};
				$.each(data,function(i,v){
					result[v.key] = v.value;
				});
				form.setData(result);
			}
		});
	}
	
	function close(){
		top["win"].closeMenuTab();
	}

	$(document).ready(function() {
		getPropertiesList();
	});  
	
	</script>
</body>
</html>