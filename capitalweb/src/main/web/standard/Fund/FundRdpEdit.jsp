<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
	<style>
		.header {
	    height: 30px;
	    /* background: #95B8E7; */
	    color: #404040;
	    background: linear-gradient(to bottom,#EFF5FF 0,#E0ECFF 100%);
	    background-repeat: repeat-x;
	    line-height:30px; /*设置line-height与父级元素的height相等*/
	    text-align: center; /*设置文本水平居中*/
	    font-size:18px;
		}
	</style>
</head>
<body style="width:100%;height:100%;background:white">
<div id="layout1" class="mini-layout" style="width:100%;height:100%;">
	
	 <div class="header" region="north" height="30" showSplit="false" showHeader="false">
        货币基金（赎回）
    </div>
    <div title="center" region="center" bodyStyle="overflow:hidden;">
	<div class="mini-fit" id="field_form"> 
	
	<div  class="mini-panel" title="赎回信息*" style="width:100%;"  allowResize="true" collapseOnTitleClick="true">
		<div class="leftarea" style="margin-top:5px;">
			<input id="fundCode" name="fundCode" class="mini-buttonedit" labelField="true"  label="基金代码："  onbuttonclick="selectFundInfo" maxLength="30" style="width:95%;"  labelStyle="text-align:left;width:150px;margin-left:5px;" required="true" />
			<input id="ccy" name="ccy" class="mini-combobox" labelField="true"  label="基金币种：" data="CommonUtil.serverData.dictionary.Currency" style="width:95%;"  labelStyle="text-align:left;width:150px;margin-left:5px;" required="true"  enabled="false"/>
			<input id="vdate" name="vdate"  allowInput="false" class="mini-datepicker" labelField="true" label="赎回日期："  labelStyle="text-align:left;width:150px;margin-left:5px;" style="width:95%;" required="true" />
		</div>
		<div class="rightarea" style="margin-top:5px;">
			<input id="fundName" name="fundName" class="mini-textbox" labelField="true"  label="货币基金名称：" maxLength="255" labelStyle="text-align:left;width:150px;margin-left:5px;" style="width:95%;"   required="true"  enabled="false"/>
			<input id="canShareAmt" name="canShareAmt" class="mini-spinner" labelField="true"  label="可赎回份额（份）："  changeOnMousewheel='false' maxValue="999999999999999" format="n2" labelField="true"  labelStyle="text-align:left;width:150px;margin-left:5px;"  style="width:95%;" enabled="false"//>
			<input id="shareAmt" name="shareAmt" class="mini-spinner" labelField="true"  label="赎回份额（份）："  changeOnMousewheel='false' maxValue="999999999999999" format="n2" labelField="true"  labelStyle="text-align:left;width:150px;margin-left:5px;"  style="width:95%;" required="true" />
			
		</div>
		<div class="leftarea" style="margin-top:5px;">
			<input id="cno" name="cno" class="mini-buttonedit" labelField="true"  label="基金管理人编号：" onbuttonclick="onButtonEdit" maxLength="30" style="width:95%;"  labelStyle="text-align:left;width:150px;margin-left:5px;" required="true"  enabled="false"/>
		</div>
	  	<div class="rightarea" style="margin-top:5px;">
	  	    <input id="cname" name="cname" class="mini-textbox" labelField="true"  label="基金管理人名称：" maxLength="255" labelStyle="text-align:left;width:150px;margin-left:5px;" style="width:95%;"   required="true"  enabled="false"/>
		</div>
		<div class="leftarea" style="margin-top:5px;">
			<input id="invType" name="invType" data="CommonUtil.serverData.dictionary.AccThreeSubject" class="mini-combobox"  labelField="true"  label="会计类型："  labelStyle="text-align:left;width:150px;margin-left:5px;" style="width:95%;"  enabled="false" required="true" />
		</div>
		<div class="rightarea" style="margin-top:5px;">
			<input id="amtOld" name="amtOld" class="mini-hidden" />
		</div>
		
	</div>
	<div  class="mini-panel" title="审批信息*" style="width:100%;"  allowResize="true" collapseOnTitleClick="true">
		<div class="leftarea" style="margin-top:5px;">
			<input id="dealNo" name="dealNo" class="mini-textbox" labelField="true" style="width:95%;" label="审批单编号：" enabled="false" labelStyle="text-align:left;width:150px;margin-left:5px;"  vtype="maxLength:32"  enabled="false" emptyText="由系统默认自动生成"/>
			<input id="adate" name="adate" class="mini-datepicker"  allowInput="false" labelField="true" label="审批日期："  labelStyle="text-align:left;width:150px;margin-left:5px;"  style="width:95%;"  enabled="false"/>
		</div>
		<div class="rightarea" style="margin-top:5px;">
			<input id="sponsor" name="sponsor" class="mini-textbox" labelField="true"  label="审批发起人：" labelStyle="text-align:left;width:150px;margin-left:5px;" style="width:95%;"  enabled="false" />
			<input id="sponinst" name="sponinst" class="mini-combobox" labelField="true"  label="审批发起机构：" labelStyle="text-align:left;width:150px;margin-left:5px;" style="width:95%;"  enabled="false" />
		</div>
	</div>
 	 <%@ include file="../Common/Flow/MiniApproveOpCommon.jsp"%>
	</div>
</div>

	
	<div title="south" region="south" showSplit="false" showHeader="false" height="38px;"  style="line-height:38px;text-align: center;">
       <div id="functionIds">
			<a class="mini-button" style="display: none"  style="width:120px;" id="save_btn"  onclick="save">保存交易</a>
			<a class="mini-button" style="display: none"  style="width:120px;" id="close_btn"  onclick="close">关闭界面</a>
		</div>
    </div>
</div>
	<script type="text/javascript">
	mini.parse();
	//获取当前tab
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var row=params.selectedData;
	var action =params.action;
	var form =new mini.Form("#field_form");
	var grid=mini.get("datagrid");
	var prdNo = '801';//货币基金
	var dealType = "1";//事前审批
	var tradeData={};
	if(row!=null){
		tradeData.selectData=row;
		tradeData.operType=action;
		tradeData.serial_no=row.dealNo;
		tradeData.task_id=row.taskId;
	}
	//保存审批单
	function save(){
		//表单验证！！！
		form.validate();
		if (form.isValid() == false) {
			return;
		}

		var data=form.getData(true);
		data['fundCode'] = mini.get("fundCode").getValue();
		data['ccy'] = mini.get("ccy").getValue();
		data['prdNo']=prdNo;
		data['dealType']=dealType;
		data['trdtype']=ApproveTrdType.FundTrdType.Rdp;//交易操作类型
		var params=mini.encode(data);
		CommonUtil.ajax({
			url:"/ApproveFundManageController/saveApprove",
			data:params,
			callback:function(data){
				mini.alert("货基赎回-保存成功！请确认信息后再提交审批！",'提示信息',function(){
					top["win"].closeMenuTab();
					})
			}
		});
	}
	
	function selectFundInfo(){
		mini.open({
            url: CommonUtil.baseWebPath() + "./Fund/FtTposMini.jsp",
            title: "基金持仓信息",
            width: 800,
			height: 500,
			ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                    		mini.get("fundCode").setValue(data.fundCode);
    						mini.get("fundCode").setText(data.fundCode);
    						mini.get("shareAmt").setValue(CommonUtil.accSubtr(data.utQty,data.newQty));
    						mini.get("canShareAmt").setValue(CommonUtil.accSubtr(data.utQty,data.newQty));
    						mini.get("fundName").setValue(data.fundName);
    						mini.get("ccy").setValue(data.ccy);
    						mini.get("cno").setValue(data.managComp);
    						mini.get("cno").setText(data.managComp);
    						mini.get("cname").setValue(data.managCompNm);
    						mini.get("invType").setValue(data.invtype);
    						form.validate();
                    }
                }
            }
        });
	}
	$(document).ready(function(){
		if($.inArray(action,["approve","detail"])>-1){
			mini.get("save_btn").setVisible(false);
			form.setEnabled(false);
			var form11=new mini.Form("#approve_operate_form");
			form11.setEnabled(true);
		}else if(action=="add"){//增加时，设置审批发起人 审批发起机构
			mini.get("sponsor").setValue("<%=__sessionUser.getUserId()%>");
			mini.get("sponinst").setValue("<%=__sessionUser.getInstId() %>");
			mini.get("adate").setValue("<%=__bizDate%>");
			mini.get("vdate").setValue("<%=__bizDate%>");
		}
		if($.inArray(action,["edit","approve","detail"])>-1){
			form.setData(row);
			mini.get("fundCode").setValue(row.fundCode);
			mini.get("fundCode").setText(row.fundCode);
			mini.get("fundCode").setEnabled(false);
			mini.get("cno").setValue(row.cno);
			mini.get("cno").setText(row.cno);
			mini.get("cname").setText(row.cname);
		}
	});
	function close(){
		top["win"].closeMenuTab();
	}
	</script>
	<script type="text/javascript" src="<%=basePath%>/standard/Common/Flow/MiniApproveOpCommon.js"></script>	
</body>
</html>