<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="../global.jsp"%>
<html>
<head>
<title>基金基础信息</title>   
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>
<style>

<style>
	table th{
	text-align:left;
	padding-left:10px;
	width:120px;
    border-right: 0;
    }
    .header {
    height: 30px;
    /* background: #95B8E7; */
    color: #404040;
    background: linear-gradient(to bottom,#EFF5FF 0,#E0ECFF 100%);
    background-repeat: repeat-x;
    line-height:30px; /*设置line-height与父级元素的height相等*/
    text-align: center; /*设置文本水平居中*/
    font-size:18px;
	}
	</style>
</head>
<body style="width:100%;height:100%;background:white">
<div id="layout1" class="mini-layout" style="width:100%;height:100%;">
	
	 <div class="header" region="north" height="32" showSplit="false" showHeader="false">
        货币基金基础产品信息
    </div>
    <div title="center" region="center" bodyStyle="overflow:hidden;">
     <div class="mini-fit" id="field_form">   
	 <div class="mini-panel" title="基金概况" style="width:100%">
		<table  class="form-table" width="100%" >
			<tr>
				<th style="width:15%;">基金代码：</th>
				<td style="width:35%;">
					<input id="fundCode" name="fundCode" class="mini-textbox" style="width:95%"  required="true" />
				</td>
				<th style="width:15%;">基金简称：</th>
				<td style="width:35%;">
					<input id="fundName" name="fundName"  class="mini-textbox"  style="width:95%"  required="true" />
				</td>
			</tr>
			<tr>
				<th>基金全称：</th>
				<td>
					<input id="fundFullName" name="fundFullName" style="width:95%" class="mini-textbox"  required="true" />
				</td>
				<th>	基金类型：</th>
				<td>
					<input id="fundType" name="fundType" style="width:95%"  class="mini-combobox"  data="CommonUtil.serverData.dictionary.FundType" required="true" />
				</td>
			</tr>
			<tr>
				<th>基金管理人编码：</th>
				<td>
					<input id="managComp" name="managComp" style="width:95%"  class="mini-textbox" required="true" />
				</td>
				<th>基金管理人名称：</th>
				<td>
					<input id="managCompNm" name="managCompNm" style="width:95%"  class="mini-textbox"  required="true" />
				</td>
			</tr>
			<tr>
				<th>基金币种：</th>
				<td>
					<input id="ccy" name="ccy"  style="width:95%" data="CommonUtil.serverData.dictionary.Currency" class="mini-combobox"  required="true" />
				</td>
				<th>起息规则：</th>
				<td>
					<input id="vType" name="vType"  style="width:95%" data="CommonUtil.serverData.dictionary.ValueType" class="mini-combobox"  required="true" />
				</td>
			</tr>
			<tr>
				<th>基金经理：</th>
				<td>
					<input id="managerMen" name="managerMen" style="width:95%" class="mini-textbox"  required="true" />
				</td>
				<th>基金经理联系方式：</th>
				<td>
					<input id="managerMenPhone" name="managerMenPhone" style="width:95%" class="mini-textbox"   required="true" />
				</td>
			</tr>
			<tr>
				<th>份额结转方式：</th>
				<td>
					<input id="coverMode" name="coverMode" data="CommonUtil.serverData.dictionary.CoverMode" class="mini-combobox"style="width:95%"  required="true" />
				</td>
				<th>份额结转日期：</th>
				<td>
					<input id="coverDay" name="coverDay"  class="mini-spinner"  style="width:95%"  required="true" />
				</td>
			</tr>
			</table>
	</div>	
	<div class="mini-panel" title="基金管理人清算信息" style="width:100%">
		<table  class="form-table" width="100%" >
			<tr>
				<th style="width:15%;">汇款账号：</th>
				<td style="width:35%;">
					<input id="partyAcccode" name="partyAcccode" class="mini-textbox"  style="width:95%;"   required="true"  />
				</td>
				<th style="width:15%;">账号名称：</th>
				<td style="width:35%;">
					<input id="partyAccname" name="partyAccname"  class="mini-textbox"  style="width:95%" required="true"  />
				</td>
			</tr>
			<tr>
				<th>开户行行号：</th>
				<td>
					<input id="partyBankcode" name="partyBankcode" class="mini-textbox"  style="width:95%;"   required="true"  />
				</td>
				<th>开户行名称：</th>
				<td>
					<input id="partyBankname" name="partyBankname"  class="mini-textbox"  style="width:95%" required="true" />
				</td>
			</tr>
		</table>
	</div>
	<div class="mini-panel" title="基金其他信息" style="width:100%">
			<table  class="form-table" width="100%" >
				<tr>
					<th style="width:15%;">投资目标分类：</th>
					<td style="width:35%;">
						<input id="invType" name="invType" style="width:95%"  class="mini-textbox" />
					</td>
					<th style="width:15%;">成立日期</th>
					<td style="width:35%;">
						<input id="estDate" name="estDate"  allowInput="false" style="width:95%"  class="mini-textbox" />
					</td>
				</tr>
				
				<tr>
					<th>	基金托管人：</th>
					<td>
						<input id="custodian" name="custodian" style="width:95%" class="mini-textbox" />
					</td>
				</tr>
				<tr>
					<th>	信息披露人：</th>
					<td>
						<input id="infoDiscloser" name="infoDiscloser"  style="width:95%" class="mini-textbox" />
					</td>
					<th>发行方式：</th>
					<td>
						<input id="issueType" name="issueType" style="width:95%" class="mini-textbox" />
					</td>
				</tr>
				
				<tr>
					<th>	发行协调人：</th>
					<td>
						<input id="distCoordinator" name="distCoordinator" style="width:95%"  class="mini-textbox" />
					</td>
					<th>基金总份额（份：</th>
					<td>
						<input id="totalQty" name="totalQty" style="width:95%"  class="mini-spinner" maxValue="99999999999" format="n2"/>
					</td>
				</tr>
				<tr>
					<th>	流通份额(份)：</th>
					<td>
						 <input id="trueQty" name="trueQty"   style="width:95%"  class="mini-spinner" changeOnMousewheel='false' maxValue="99999999999" format="n2"  /> 
					</td>
					<th>存续期限描述：</th>
					<td>
						<input id="term" name="term"  style="width:95%"class="mini-textbox" />
					</td>
				</tr>
				
				<tr>
					<th>管理费率%：</th>
					<td>
						<input id="managRate" name="managRate" style="width:95%" class="mini-spinner" changeOnMousewheel="false" maxValue="9999" format="n6"/>
					</td>
					<th>托管费率%：</th>
					<td>
						<input id="trustRate" name="trustRate" style="width:95%" class="mini-spinner" changeOnMousewheel="false" maxValue="9999" format="n6"/>
					</td>
				</tr>
				<tr>
					<th>申购费率%：</th>
					<td>
						<input id="applyRate" name="applyRate" style="width:95%" class="mini-spinner" changeOnMousewheel="false" maxValue="9999" format="n6"/>
					</td>
					<th>赎回费率%：</th>
					<td>
						<input id="redeemRate" name="redeemRate" style="width:95%" class="mini-spinner" changeOnMousewheel="false" maxValue="9999" format="n6" />
					</td>
				</tr>
				
				<tr>
					<th>备注信息：</th>
					<td colspan="3">
						<input id="remark" name="remark" style="width:98%;height:80px;" class="mini-textarea" />
					</td>
				</tr>
			</table>
		</div>
	</div>
</div>
	<div title="south" region="south" showSplit="false" showHeader="false" height="39px;"  style="line-height:38px;text-align: center;">
       <div id="functionIds">
			<a class="mini-button" style="display: none"  style="width:120px;" id="save_btn"  onclick="save">保存交易</a>
			<a class="mini-button" style="display: none"  style="width:120px;" id="close_btn"  onclick="close">关闭界面</a>
		</div>
    </div>
</div>
<script type="text/javascript">
	mini.parse();
	top['fundBaseDetails'] = window;

	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var row=params.selectedData;
	var action =params.action;
	var form =new mini.Form("#field_form");

	function save(){
		//表单验证！！！
		form.validate();
		if (form.isValid() == false) {
			return;
		}

		var data=form.getData(true);
		data['productType']="FtInfo";
		var params=mini.encode(data);
		CommonUtil.ajax({
			url:"/FundInfoController/saveInfo",
			data:params,
			callback:function(data){
				mini.alert("保存成功",'提示信息',function(){
					top["win"].closeMenuTab();
					})
			}
		});
	}
	$(document).ready(function(){
		if($.inArray(action,["approve","detail"])>-1){
			mini.get("save_btn").setVisible(false);
			form.setEnabled(false);
		}
		if($.inArray(action,["edit","approve","detail"])>-1){
			form.setData(row);
		}
	})
</script>
</body>
</html>