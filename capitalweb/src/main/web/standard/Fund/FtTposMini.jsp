<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
  <head>
	<script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
    <title></title>
  </head>
  
<body style="width:100%;height:100%;background:white">
	<div class="mini-panel" title="基金持仓列表" style="width:100%">
		<div style="width:100%;padding-top:10px;">
			<div id="search_form" class="mini-form" width="80%">
				<input id="dealNo" name="dealNo" class="mini-textbox" vtype="maxLength:50" labelField="true"  label="交易单号：" labelStyle="text-align:right;"  emptyText="请输入交易单号"/>
				<!-- <input id="approveStatus" name="approveStatus" class="mini-combobox" data = "CommonUtil.serverData.dictionary.ApproveStatus" labelField="true"  label="审批状态：" labelStyle="text-align:right;" emptyText="请选择审批单状态"> -->
				<span style="float:right;margin-right: 150px">
					<a id="search_btn" class="mini-button" style="display: none"  onclick="query()">查询</a>
					<a id="clear_btn" class="mini-button" style="display: none"  >清空</a>
				</span>
			</div>
		</div>
	</div>

	
	<div id="tradeManage" class="mini-fit">      
		<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="dealNo" 
		allowAlternating="true" allowResize="true" onrowdblclick="onrowdblclick" border="true" 
		multiSelect="false" sortMode="client">
			<div property="columns">
				<div type="indexcolumn" headerAlign="center" width="50">序号</div>
				<div field="fundCode" width="80" headerAlign="center" align="left" allowSort="true">基金代码</div>
				<div field="fundName" width="200" headerAlign="center" align="left" allowSort="true">基金名称</div>
				<div field="utQty" numberFormat="#,0.00" align="right" width="150" headerAlign="center" allowSort="true">持仓份额（数量）</div>
			</div>
		</div>  
	</div>
	<script type="text/javascript">
	
	mini.parse();
		var form=new mini.Form("#search_form");
		var grid=mini.get("datagrid");
		top["ftTposMini"] = window;
		var rows='';
		function query(){
		search(grid.pageSize,0);
	}
	/* 查询 */
	function search(pageSize,pageIndex){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统也提示");
			return;
		}

		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		data['prdNo']='801';
		url = "/FundTposController/getFtTposList";

		var params = mini.encode(data);
		CommonUtil.ajax({
			url:url,
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});
	function onrowdblclick(){
		CloseWindow("ok");
	}
	function GetData() {
        var row = grid.getSelected();
        return row;
	}
	function CloseWindow(action) {
        if (window.CloseOwnerWindow)
            return window.CloseOwnerWindow(action);
        else
            window.close();
	}
    //关闭窗口
    function onCancel() {
        CloseWindow("cancel");
    }
	$(document).ready(function(){
		search(10,0);
	})
	</script>
</body>
</html>