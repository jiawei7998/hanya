<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
	<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
    <title></title>
  </head>
  
<body style="width:100%;height:100%;background:white">
	<div id="fundManage" class="mini-fit">      
		<div id="datagrid" class="mini-datagrid" style="width:100%;height:100%;" idField="dealNo" 
		allowAlternating="true" allowResize="true" onrowdblclick="onRowDblClick" border="true" onrowclick="onRowClick"
		multiSelect="false" sortMode="client">
			<div property="columns">
				<div type="indexcolumn" headerAlign="center" width="50">序号</div>
				<div field="prdNo" width="80" headerAlign="center" align="center" allowSort="true" renderer="CommonUtil.dictRenderer" data-options="{dict:'FundType'}">产品名称</div>
				<div field="cname" width="180" headerAlign="center" align="center" allowSort="true" >客户名称</div>
				<div field="fundCode" width="80" headerAlign="center" align="center" allowSort="true">基金代码</div>
				<div field="fundName" width="120" headerAlign="center" align="center" allowSort="true">基金名称</div>
				<div field="invType" width="80" headerAlign="center" align="center" allowSort="true" renderer="CommonUtil.dictRenderer" data-options="{dict:'FAccThreeSubject'}">会计类型</div>
				<div field="tableId" width="80" headerAlign="center" align="center" allowSort="true" renderer="CommonUtil.dictRenderer" data-options="{dict:'TableId'}">单据类型</div>
				<div field="amt" numberFormat="#,0.00" align="right" width="150" headerAlign="center" allowSort="true">金额(元)</div>
				<div field="markId" width="260" headerAlign="center" align="center" allowSort="true">识别要素</div>
				<div field="dealType" width="70" renderer="CommonUtil.dictRenderer" data-options="{dict:'DealType'}" align="center" headerAlign="center">审批类型</div>
				<div field="approveStatus" width="70" renderer="CommonUtil.dictRenderer" data-options="{dict:'ApproveStatus'}" align="center" headerAlign="center">审批状态</div>
				<div field="adate" width="80" headerAlign="center" allowSort="true">审批日期</div>
				<div field="userName" width="80" headerAlign="center" allowSort="true">审批发起人</div>   
				<div field="instName" width="180" headerAlign="center" allowSort="true">审批发起机构</div>
				<div field="dealNo" width="120" headerAlign="center" align="center" allowSort="true">交易编号</div> 
			</div>
		</div>  
	</div>
	<script type="text/javascript">
		mini.parse();
		top["approveListManager"] = window;
		var grid = mini.get("datagrid");

		/* 按钮 查询事件 */
		function query(){
			search(grid.pageSize,0);
		}
		/* 查询 */
		function search(pageSize,pageIndex){
			var data={};
			data['pageNumber']=pageIndex+1;
			data['pageSize']=pageSize;
			data['branchId']=branchId;
			url = "/FundController/getFtMonitorPage";
			var params = mini.encode(data);
			CommonUtil.ajax({
				url:url,
				data:params,
				callback : function(data) {
					grid.setTotalCount(data.obj.total);
					grid.setPageIndex(pageIndex);
			        grid.setPageSize(pageSize);
					grid.setData(data.obj.rows);
				}
			});
		}
		grid.on("beforeload", function (e) {
			e.cancel = true;
			var pageIndex = e.data.pageIndex; 
			var pageSize = e.data.pageSize;
			search(pageSize,pageIndex);
		});
		$(document).ready(function(){
			search(10,0);
		})
	</script>
</body>
</html>