<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
    <title></title>
  </head>
  
<body style="width:100%;height:100%;background:white">
	<div class="mini-panel" title="基金市场数据信息" style="width:100%;">
	<div style="width:100%;padding-top:10px;">
		<div id="search_form" class="mini-form" width="80%">
			<input id="fundCode" name="fundCode" class="mini-textbox"  labelField="true"  label="基金代码：" labelStyle="text-align:right;width:80px;"  emptyText="请输入精确代码"/>
			<input id="fundName" name="fundName" class="mini-textbox"  labelField="true"  label="基金名称：" labelStyle="text-align:right;width:80px;"  emptyText="请输入模糊名称"/>
			<input id="fundType" name="fundType" class="mini-combobox"  labelField="true" label="基金类型：" emptyText="请选择基金类型" data="CommonUtil.serverData.dictionary.FundType" allowInput="false"  labelStyle="text-align:right;width:80px;" />
			<input id="postdate" name="postdate" class="mini-datepicker" labelField="true" label="扣款日期：" allowInput="false"  labelStyle="text-align:right;width:80px;" />
			<span style="float:right;margin-right: 80px">
				<a id="search_btn" class="mini-button" style="display: none"  >查询</a>
				<a id="add_btn" class="mini-button" style="display: none"  >设置</a>
				<a id="import_btn" class="mini-button" style="display: none"  >Excel导入</a>
			</span>
		</div>
	</div>
	</div>
	<div id="fundManage" class="mini-fit" >      
		<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="dealNo"  title="基金市场数据列表" showHeader="true"
		allowAlternating="true" allowResize="true" onrowdblclick="onRowDblClick" border="true" onrowclick="onRowClick"
		multiSelect="false" sortMode="client">
			<div property="columns">
				<div type="indexcolumn" headerAlign="center" width="50">序号</div>
				<div field="fundPrice" numberFormat="#,0.000000" align="right" width="80" headerAlign="center" allowSort="true">万份收益/单位净值</div>  
				<div field="postdate" width="80" headerAlign="center" align="center" allowSort="true">账务日期</div>  
				<div field="fundCode" width="80" headerAlign="center" align="left" allowSort="true">基金代码</div>  
				<div field="fundName" width="250" headerAlign="center" align="left" allowSort="true">基金简称</div>
				<div field="fundType" width="120" headerAlign="center" allowSort="true" renderer="CommonUtil.dictRenderer" data-options="{dict:'FundType'}">基金类型</div>   
				<div field="ccy" width="80" headerAlign="center" align="center" allowSort="true" renderer="CommonUtil.dictRenderer" data-options="{dict:'Currency'}">基金币种</div> 
				<div field="managCompNm" width="320" headerAlign="center" align="left" allowSort="true">基金管理人名称</div> 
				
			</div>
		</div>  
	</div>
	<script type="text/javascript">
		mini.parse();
		mini.get("postdate").setValue("<%=__bizDate%>");
		var currTab = top["win"].tabs.getActiveTab();
		var params = currTab.params;
		var prdNo="9998";
		var prdName='基金市场数据信息';
		top["fundBaseManager"] = window;
		var grid=mini.get("datagrid");
		var form=new mini.Form("#search_form");
		function functionMain(prdNo,url,prdName,title,details){
			var url = CommonUtil.baseWebPath() + url;
			var tab = {
				id : prdNo + "_"+details,
				name : prdNo + "_"+details,
				iconCls : "",
				title : prdName + title,
				url : url,
				showCloseButton:true
			};
			var paramData = {'operType':'A','closeFun':'query','pWinId':'fundBaseManager',selectedData:grid.getSelected(),action:details};
			
			CommonUtil.openNewMenuTab(tab,paramData);
		}
		$("#add_btn").on("click",function(){
			var messageid = mini.loading("系统正在处理...", "请稍后");
			try {
				functionMain('001',"/Fund/mkt/FundMktDetail.jsp","基金市场数据","设置","edit");
			} catch (error) {
				
			}
			mini.hideMessageBox(messageid);
		});
	/* 按钮 查询事件 */
	function query(){
		search(grid.pageSize,0);
	}
	/* 查询 */
	function search(pageSize,pageIndex){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统也提示");
			return;
		}
		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		url = "/FundController/searchFundMktList";

		var params = mini.encode(data);
		CommonUtil.ajax({
			url:url,
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});
	$(document).ready(function(){
		search(10,0);
	})
	$("#search_btn").on("click",function(){
		 query();
	});
	</script>
</body>
</html>