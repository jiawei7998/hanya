<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="../../global.jsp"%>
<html>
<head>
<title>基金市场数据信息</title>   
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>
<style>

<style>
	table th{
	text-align:left;
	padding-left:10px;
	width:120px;
    border-right: 0;
    }
    .header {
    height: 30px;
    /* background: #95B8E7; */
    color: #404040;
    background: linear-gradient(to bottom,#EFF5FF 0,#E0ECFF 100%);
    background-repeat: repeat-x;
    line-height:30px; /*设置line-height与父级元素的height相等*/
    text-align: center; /*设置文本水平居中*/
    font-size:18px;
	}
	</style>
</head>
<body style="width:100%;height:100%;background:white">
<div id="layout1" class="mini-layout" style="width:100%;height:100%;">
	
	 <div class="header" region="north" height="32" showSplit="false" showHeader="false">
        基金市场数据设置
    </div>
    <div title="center" region="center" bodyStyle="overflow:hidden;">
     <div class="mini-fit" id="field_form">   
	 <div class="mini-panel" title="基金概况" style="width:100%" id="fundForm">
		<table  class="form-table" width="100%" >
			<tr>
				<th style="width:15%;">基金代码：</th>
				<td style="width:35%;">
					<input id="fundCode" name="fundCode"  allowInput="false" class="mini-buttonedit" style="width:95%"  required="true"   onbuttonclick="selectFundInfo"/>
				</td>
				<th style="width:15%;">基金简称：</th>
				<td style="width:35%;">
					<input id="fundName" name="fundName"  class="mini-textbox"  style="width:95%"  enabled="false"/>
				</td>
			</tr>
			<tr>
				<th>基金全称：</th>
				<td>
					<input id="fundFullName" name="fundFullName" style="width:95%" class="mini-textbox"  enabled="false"/>
				</td>
				<th>	基金类型：</th>
				<td>
					<input id="fundType" name="fundType" style="width:95%"  class="mini-combobox"  data="CommonUtil.serverData.dictionary.FundType"  enabled="false"/>
				</td>
			</tr>
			<tr>
				<th>基金管理人编码：</th>
				<td>
					<input id="managComp" name="managComp" style="width:95%"  class="mini-textbox" enabled="false"/>
				</td>
				<th>基金管理人名称：</th>
				<td>
					<input id="managCompNm" name="managCompNm" style="width:95%"  class="mini-textbox"  enabled="false"/>
				</td>
			</tr>
			<tr>
				<th>基金币种：</th>
				<td>
					<input id="ccy" name="ccy"  style="width:95%" data="CommonUtil.serverData.dictionary.Currency" class="mini-combobox"  enabled="false"/>
				</td>
				<th>起息规则：</th>
				<td>
					<input id="vType" name="vType"  style="width:95%" data="CommonUtil.serverData.dictionary.ValueType" class="mini-combobox"  enabled="false"/>
				</td>
			</tr>
			<tr>
				<th>基金经理：</th>
				<td>
					<input id="managerMen" name="managerMen" style="width:95%" class="mini-textbox"  enabled="false"/>
				</td>
				<th>基金经理联系方式：</th>
				<td>
					<input id="managerMenPhone" name="managerMenPhone" style="width:95%" class="mini-textbox"   enabled="false"/>
				</td>
			</tr>
			<tr>
				<th>份额结转方式：</th>
				<td>
					<input id="coverMode" name="coverMode" data="CommonUtil.serverData.dictionary.CoverMode" class="mini-combobox"style="width:95%"  enabled="false"/>
				</td>
				<th>份额结转日期：</th>
				<td>
					<input id="coverDay" name="coverDay"  class="mini-spinner"  style="width:95%"  enabled="false"/>
				</td>
			</tr>
			</table>
	</div>	
	
	<div class="mini-panel" title="市场数据设置*" style="width:100%">
		<table  class="form-table" width="100%" >
			  <tr>
				<th style="width:15%;">账务日期：</th>
				<td style="width:35%;">
					<input id="postdate" name="postdate" class="mini-datepicker" style="width:95%"  require="true" allowInput="false" onvaluechanged="onDrawDate"/>
				</td>
				<th style="width:15%;" id="priceType">万份收益：</th>
				<td style="width:35%;">
					<input id="fundPrice" name="fundPrice"  class="mini-spinner" required="true"  onValuechanged="interestAmount" changeOnMousewheel='false' maxValue="999" format="n6" style="width:95%"   />
				</td>
			</tr>
		</table>
	  </div>
	</div>
</div>
	<div title="south" region="south" showSplit="false" showHeader="false" height="39px;"  style="line-height:38px;text-align: center;">
       <div id="functionIds">
			<a class="mini-button" style="display: none"  style="width:120px;margin-top:8px;" id="save_btn"  onclick="save">保存设置</a>
		</div>
    </div>
</div>
<script type="text/javascript">
	mini.parse();
	top['fundBaseDetails'] = window;
	mini.get("postdate").setValue("<%=__bizDate%>");
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var row=params.selectedData;
	var action =params.action;
	var form =new mini.Form("#field_form");

	function save(){
		//表单验证！！！
		form.validate();
		if (form.isValid() == false) {
			return;
		}

		var data=form.getData(true);
		var params=mini.encode(data);
		CommonUtil.ajax({
			url:"/FundController/saveFtPriceDaily",
			data:params,
			callback:function(data){
				mini.alert("保存成功",'提示信息',function(){
					top["win"].closeMenuTab();
					})
			}
		});
	}
	$(document).ready(function(){
		
		if($.inArray(action,["edit"])>-1){
			form.setData(row);
			if(null != row){
			CommonUtil.ajax({
				url:"/FundInfoController/seachInfoByCode",
				data:{'fundCode':row.fundCode},
				callback:function(data){
					if(data.obj){
						var fundForm  = new mini.Form("#fundForm");
						fundForm.setData(data.obj);
						mini.get("fundCode").setValue(data.obj.fundCode);
						mini.get("fundCode").setText(data.obj.fundCode);
						if(data.obj.fundType=='801'){
							$("#priceType").html("万份收益");
						}else{
							$("#priceType").html("单位净值");
						}
					}
				}
			});
			var fundForm  = new mini.Form("#fundForm");
			fundForm.setEnabled(false);
			}else{
				mini.get("postdate").setValue("<%=__bizDate%>");
			}
		}
	})
	function selectFundInfo(){
		mini.open({
            url: CommonUtil.baseWebPath() + "./Fund/mkt/FundBaseMini.jsp",
            title: "债币基金信息",
            width: 924,
			height: 500,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                    		var fundForm  = new mini.Form("#fundForm");
                    		fundForm.setData(data);
                    		mini.get("fundCode").setValue(data.fundCode);
    						mini.get("fundCode").setText(data.fundCode);
    						mini.get("postdate").setValue("<%=__bizDate%>");
    						if(data.fundType=='801'){
    							$("#priceType").html("万份收益");
    						}else{
    							$("#priceType").html("单位净值");
    						}
    						fundForm.validate();
    						CommonUtil.ajax({
    							url:"/FundController/getFtPriceDailyByKey",
    							data:{'fundCode':data.fundCode,"postdate":'<%=__bizDate%>'},
    							callback:function(data){
    								if(data.obj){
    									mini.get("fundPrice").setValue(data.obj.fundPrice);
    								}
    							}
    						});
                    }
                }
            }
        });
	}
	
	function onDrawDate(e) {
	   var date = e.value;
	   if("" != mini.get("fundCode").getValue()){
	   CommonUtil.ajax({
			url:"/FundController/getFtPriceDailyByKey",
			data:{'fundCode':mini.get("fundCode").getValue(),"postdate":mini.formatDate (date, "yyyy-MM-dd" )},
			callback:function(data){
				if(data.obj){
					mini.get("fundPrice").setValue(data.obj.fundPrice);
				}else{
					mini.get("fundPrice").setValue(0.00);
				}
			}
		});
	   }
	 }
</script>
</body>
</html>