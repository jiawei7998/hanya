<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
	<style>
	table th{
	text-align:left;
	padding-left:10px;
	width:120px;
    border-right: 0;
    }
    .header {
    height: 30px;
    /* background: #95B8E7; */
    color: #404040;
    background: linear-gradient(to bottom,#EFF5FF 0,#E0ECFF 100%);
    background-repeat: repeat-x;
    line-height:30px; /*设置line-height与父级元素的height相等*/
    text-align: center; /*设置文本水平居中*/
    font-size:18px;
	}
	</style>
</head>
<body style="width:100%;height:100%;background:white">
<div id="layout1" class="mini-layout" style="width:100%;height:100%;">
	
	 <div class="header" region="north" height="30" showSplit="false" showHeader="false">
        债券基金（申购）
    </div>
    <div title="center" region="center" bodyStyle="overflow:hidden;">
		<div class="mini-fit" style="width:100%;height:100%;">
		   <div class="mini-splitter" style="width:100%;height:100%;border:0px;" allowResize="false">
		   <div size="70%">
		   <div class="mini-fit" id="field_form">   
			<div  class="mini-panel" title="客户信息*" style="width:100%;"  allowResize="true" collapseOnTitleClick="true">
				<div class="leftarea" style="margin-top:5px;">
					<input id="cno" name="cno" class="mini-buttonedit" labelField="true"  label="基金管理人编号：" enabled="false" onbuttonclick="onButtonEdit" maxLength="30" style="width:95%;"  labelStyle="text-align:left;margin-left:5px;"  required="true" />
					<input id="cname" name="cname" class="mini-textbox" labelField="true"  label="基金管理人名称："  enabled="false" maxLength="255" labelStyle="text-align:left;margin-left:5px;" style="width:95%;"   required="true"  />
				</div>
				<div class="rightarea" style="margin-top:5px;">
					<input id="contact" name="contact" class="mini-textbox" labelField="true"  label="基金经理：" enabled="false" maxLength="20" labelStyle="text-align:left;margin-left:5px;" style="width:95%;"   />
					<input id="contactPhone" name="contactPhone" class="mini-textbox" labelField="true"  label="联系电话：" enabled="false" labelStyle="text-align:left;margin-left:5px;" style="width:95%;"/>
				</div>
				
			</div>
			
		  	<div  class="mini-panel" title="申购信息*" style="width:100%;"  allowResize="true" collapseOnTitleClick="true">
		  	 <div class="leftarea" style="margin-top:5px;">
					<!-- <input id="fundCode" name="fundCode" class="mini-buttonedit" labelField="true"  label="基金代码："  onbuttonclick="selectFundInfo" maxLength="30" style="width:95%;"  labelStyle="text-align:left;margin-left:5px;" />
					<input id="ccy" name="ccy" class="mini-combobox" labelField="true"  label="基金币种：" data="CommonUtil.serverData.dictionary.Currency" style="width:95%;"  labelStyle="text-align:left;margin-left:5px;" />
					 --><!-- <input id="shareAmt" name="shareAmt" class="mini-spinner" labelField="true"  label="申购份额："   required="true"  onValuechanged="interestAmount" changeOnMousewheel='false' maxValue="999999999999999" format="n2" labelField="true"  labelStyle="text-align:left"  style="width:95%;" /> -->
					<input id="amt" name="amt" class="mini-spinner" labelField="true"  label="申购金额（元）：" labelStyle="text-align:left;margin-left:5px;"  required="true"  onValuechanged="interestAmount" changeOnMousewheel='false' maxValue="999999999999999" format="n2" labelField="true"  labelStyle="text-align:left" style="width:95%;"  required="true"  />
					<input id="tdate" name="tdate" class="mini-datepicker" allowInput="false" labelField="true" label="划款日期："  labelStyle="text-align:left;margin-left:5px;" style="width:95%;"  required="true" />
			</div>
			<div class="rightarea" style="margin-top:5px;">
					<!-- <input id="price" name="price" class="mini-spinner" labelField="true"  label="参考单位净值（元）："  labelStyle="text-align:left;margin-left:5px;"   required="true"  changeOnMousewheel='false' maxValue="99" format="n6" labelField="true"  labelStyle="text-align:left"  style="width:95%;" /> -->
					<input id="vdate" name="vdate" class="mini-datepicker" allowInput="false" labelField="true" label="起息日期："  labelStyle="text-align:left;margin-left:5px;" style="width:95%;"  required="true" />
			</div>
			<div class="leftarea" style="margin-top:5px;">
				<input id="selfAcccode" name="selfAcccode" class="mini-textbox" labelField="true"  label="本方账号："  enabled="false" maxLength="50" labelStyle="text-align:left;margin-left:5px;" style="width:95%;"    required="true" />
				<input id="selfAccname" name="selfAccname" class="mini-textbox" labelField="true"  label="本方账号名称："  enabled="false" maxLength="30" labelStyle="text-align:left;margin-left:5px;" style="width:95%;"   required="true"  />
				<input id="selfBankcode" name="selfBankcode" class="mini-textbox" labelField="true"  label="本方开户行行号："  enabled="false" maxLength="50" labelStyle="text-align:left;margin-left:5px;" style="width:95%;"    required="true" />
				<input id="selfBankname" name="selfBankname" class="mini-textbox" labelField="true"  label="本方开户行名称："  enabled="false" maxLength="30" labelStyle="text-align:left;margin-left:5px;" style="width:95%;"    required="true" />
			</div>
			<div class="rightarea" style="margin-top:5px;">
				 <input id="partyAcccode" name="partyAcccode" class="mini-textbox" labelField="true"  enabled="false" label="对手方账号：" maxLength="50" labelStyle="text-align:left;margin-left:5px;" style="width:95%;"   required="true"  />
				<input id="partyAccname" name="partyAccname" class="mini-textbox" labelField="true"  enabled="false" label="对手方账号名称：" maxLength="30" labelStyle="text-align:left;margin-left:5px;" style="width:95%;"   required="true"  />
				<input id="partyBankcode" name="partyBankcode" class="mini-textbox" labelField="true" enabled="false" label="对手方开户行行号：" maxLength="50" labelStyle="text-align:left;margin-left:5px;" style="width:95%;"    required="true" />
				<input id="partyBankname" name="partyBankname" class="mini-textbox" labelField="true"  enabled="false" label="对手方开户行名称：" maxLength="30" labelStyle="text-align:left;margin-left:5px;" style="width:95%;"   required="true"  />
			</div>
		   </div>
		  <div  class="mini-panel" title="管理要素*" style="width:100%;"  allowResize="true" collapseOnTitleClick="true">
				<div class="leftarea" style="margin-top:5px;">
					<input id="invType" name="invType" data="CommonUtil.serverData.dictionary.AccThreeSubject" class="mini-combobox"  labelField="true"  label="会计类型："  labelStyle="text-align:left;margin-left:5px;" style="width:95%;"  required="true" />
					<input id="platformInv" name="platformInv" data="CommonUtil.serverData.dictionary.indusEarmark" class="mini-combobox"  labelField="true"  label="行业投向：" labelStyle="text-align:left;margin-left:5px;" style="width:95%;"   />
				</div>
				<div class="rightarea" style="margin-top:5px;">
					<input id="eventuallyInv" name="eventuallyInv" data="CommonUtil.serverData.dictionary.investFinal" class="mini-combobox"  labelField="true"  label="最终投向：" maxLength="20" labelStyle="text-align:left;margin-left:5px;" style="width:95%;"   />
				</div>
				<div class="centerarea" style="margin-top:5px;">
					<input id="remark" name="remark" style="width:98%;height:120px;" labelField="true"  labelStyle="text-align:left;margin-left:5px;"  label="备注信息：" class="mini-textarea" />
				</div>
			</div>
			<div  class="mini-panel" title="审批信息*" style="width:100%;"  allowResize="true" collapseOnTitleClick="true">
				<div class="leftarea" style="margin-top:5px;">
				    <input id="dealNo" name="dealNo" class="mini-textbox" labelField="true" style="width:95%;" label="审批单编号：" enabled="false" abelStyle="text-align:left;margin-left:5px;"  vtype="maxLength:32" emptyText="由系统默认自动生成"/>
					<input id="adate" name="adate" class="mini-datepicker"  allowInput="false" labelField="true" label="审批日期："  labelStyle="text-align:left;margin-left:5px;"  enabled="false" style="width:95%;"   required="true" />
				</div>
				<div class="rightarea" style="margin-top:5px;">
					<input id="sponsor" name="sponsor" class="mini-textbox" labelField="true"  label="审批发起人：" labelStyle="text-align:left;margin-left:5px;" style="width:95%;"   required="true"  enabled="false" />
					<input id="sponinst" name="sponinst" class="mini-combobox" labelField="true"  label="审批发起机构：" labelStyle="text-align:left;margin-left:5px;" style="width:95%;"   required="true"  enabled="false"/>
					
				</div>
			</div>
			<%@ include file="../../Common/Flow/MiniApproveOpCommon.jsp"%>
			</div>
			</div>
		   <div size="30%">
		   <div id="panel1" class="mini-panel" title="请选择需要申购的基金*" style="width:100%;height:99%;"  allowResize="true" collapseOnTitleClick="true">
		   	<table  id="fundForm" class="form-table" width="100%" >
				<tr>
					<th>基金代码：</th>
					<td>
						<input id="fundCode" name="fundCode" class="mini-buttonedit" style="width:95%"  required="true"   onbuttonclick="selectFundInfo"/>
					</td>
				</tr>
				<tr>
					<th>基金简称：</th>
					<td>
						<input id="fundName" name="fundName"  class="mini-textbox"  style="width:95%"  required="true"  enabled="false" />
					</td>
				</tr>
				<tr>
					<th>基金全称：</th>
					<td>
						<input id="fundFullName" name="fundFullName" style="width:95%" class="mini-textbox" enabled="false" />
					</td>
				</tr>
				<tr>
					<th>	基金类型：</th>
					<td>
						<input id="fundType" name="fundType" style="width:95%"  class="mini-textbox" enabled="false" />
					</td>
				</tr>
				
				<tr>
				<th>基金币种：</th>
				<td>
					<input id="ccy" name="ccy"  style="width:95%" data="CommonUtil.serverData.dictionary.Currency" class="mini-combobox"  enabled="false"/>
				</td>
				</tr>
				<tr>
				<th>起息规则：</th>
				<td>
					<input id="vType" name="vType"  style="width:95%" data="CommonUtil.serverData.dictionary.ValueType" class="mini-combobox"  enabled="false"/>
				</td>
				</tr>
				<tr>
					<th>基金管理人编码：</th>
					<td>
						<input id="managComp" name="managComp" style="width:95%"  class="mini-textbox" enabled="false"/>
					</td>
				</tr>
				<tr>
					<th>基金管理人名称：</th>
					<td>
						<input id="managCompNm" name="managCompNm" style="width:95%"  class="mini-textbox"  enabled="false"/>
					</td>
				</tr>
				<tr>
					<th>基金经理：</th>
					<td>
						<input id="managerMen" name="managerMen" style="width:95%" class="mini-textbox"  enabled="false"/>
					</td>
				</tr>
				<tr>
					<th>基金经理联系方式：</th>
					<td>
						<input id="managerMenPhone" name="managerMenPhone" style="width:95%" class="mini-textbox"  enabled="false"/>
					</td>
				</tr>
				<tr>
					<th>份额结转方式：</th>
					<td>
						<input id="coverMode" name="coverMode" data="CommonUtil.serverData.dictionary.CoverMode" class="mini-combobox"style="width:95%" enabled="false"/>
					</td>
				</tr>
				<tr>
					<th>份额结转日期：</th>
					<td>
						<input id="coverDay" name="coverDay"  class="mini-spinner"  style="width:95%"  enabled="false"/>
					</td>
				</tr>
				<tr>
					<th>备注信息：</th>
					<td colspan="3">
						<input id="remark" name="remark" style="width:95%;height:180px;" class="mini-textarea" enabled="false" />
					</td>
				</tr>
			</table>
			</div>
		   </div>
		   
		   </div>
		   </div>
	</div>

	
	<div title="south" region="south" showSplit="false" showHeader="false" height="38px;"  style="line-height:38px;text-align: center;">
       <div id="functionIds">
			<a class="mini-button" style="display: none"  style="width:120px;" id="save_btn"  onclick="save">保存交易</a>
			<a class="mini-button" style="display: none"  style="width:120px;" id="close_btn"  onclick="close">关闭界面</a>
		</div>
    </div>
</div>
	<script type="text/javascript">
	mini.parse();
	//获取当前tab
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var row=params.selectedData;
	var action =params.action;
	var form =new mini.Form("#field_form");
	var prdNo = '802';//债币基金
	var dealType = "1";//事前审批
	var tradeData={};
	if(row!=null){
		tradeData.selectData=row;
		tradeData.operType=action;
		tradeData.serial_no=row.dealNo;
		tradeData.task_id=row.taskId;
	}
	//保存审批单
	function save(){
		//表单验证！！！
		form.validate();
		var fundForm  = new mini.Form("#fundForm");
		fundForm.validate();
		if (form.isValid() == false) {
			return;
		}

		var data=form.getData(true);
		data['fundCode'] = mini.get("fundCode").getValue();
		data['ccy'] = mini.get("ccy").getValue();
		data['prdNo']=prdNo;
		data['dealType']=dealType;
		data['trdtype']=ApproveTrdType.FundTrdType.Afp;//交易操作类型
		var params=mini.encode(data);
		CommonUtil.ajax({
			url:"/ApproveFundManageController/saveApprove",
			data:params,
			callback:function(data){
				mini.alert("债基申购-保存成功！请确认信息后再提交审批！",'提示信息',function(){
					top["win"].closeMenuTab();
					})
			}
		});
	}

	function selectFundInfo(){
		mini.open({
            url: CommonUtil.baseWebPath() + "./Fund/bond/FundBaseMini.jsp",
            title: "债币基金信息",
            width: 924,
			height: 500,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                    		var fundForm  = new mini.Form("#fundForm");
                    		fundForm.setData(data)
                    		mini.get("fundCode").setValue(data.fundCode);
    						mini.get("fundCode").setText(data.fundCode);
    						mini.get("fundCode").setValue(data.fundCode);
    						mini.get("fundCode").setText(data.fundCode);
    						mini.get("cno").setValue(data.managComp);
    						mini.get("cno").setText(data.managComp);
    						mini.get("cname").setValue(data.managCompNm);
    						mini.get("contact").setValue(data.managerMen);
    						mini.get("contactPhone").setValue(data.managerMenPhone);
    						mini.get("partyAcccode").setValue(data.partyAcccode);
    						mini.get("partyAccname").setValue(data.partyAccname);
    						mini.get("partyBankcode").setValue(data.partyBankcode);
    						mini.get("partyBankname").setValue(data.partyBankname);
    						fundForm.validate();
    							var paramsData={};
 							paramsData['instId']="<%=__sessionUser.getInstId() %>";
 							paramsData['branchId']=branchId;
 							paramsData['ccy']=data.ccy;
 	    						CommonUtil.ajax({
 	    							url:"/InstitutionSettlsController/settlsList",
 	    							data:paramsData,
 	    							callback:function(data){
 	    								if(data.obj){
 	    									mini.get("selfAcccode").setValue(data.obj.rows[0].instAcctNo);
 		    		    						mini.get("selfAccname").setValue(data.obj.rows[0].instAcctNm);
 		    		    						mini.get("selfBankcode").setValue(data.obj.rows[0].instAcctPbocNo);
 		    		    						mini.get("selfBankname").setValue(data.obj.rows[0].instAcctBknm);
 	    								}
 	    							}
 	    						});
                    }
                }
            }
        });
	}
	$(document).ready(function(){
		if($.inArray(action,["approve","detail"])>-1){
			mini.get("save_btn").setVisible(false);
			form.setEnabled(false);
			var form11=new mini.Form("#approve_operate_form");
			form11.setEnabled(true);
		}else if(action=="add"){//增加时，设置审批发起人 审批发起机构
			mini.get("sponsor").setValue("<%=__sessionUser.getUserId()%>");
			mini.get("sponinst").setValue("<%=__sessionUser.getInstId() %>");
			mini.get("adate").setValue("<%=__bizDate%>");
		}
		if($.inArray(action,["edit","approve","detail"])>-1){
			form.setData(row);
			mini.get("cno").setText(row.cno);
			CommonUtil.ajax({
				url:"/FundInfoController/seachInfoByCode",
				data:{'fundCode':row.fundCode},
				callback:function(data){
					if(data.obj){
						var fundForm  = new mini.Form("#fundForm");
						fundForm.setData(data.obj);
						mini.get("fundCode").setValue(data.obj.fundCode);
						mini.get("fundCode").setText(data.obj.fundCode);
					}
				}
			});
		}
	})
	function close(){
		top["win"].closeMenuTab();
	}
	</script>
	<script type="text/javascript" src="../../Common/Flow/MiniApproveOpCommon.js"></script>			
</body>
</html>