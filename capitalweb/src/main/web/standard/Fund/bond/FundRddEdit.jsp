<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
	<style>
	table th{
	text-align:left;
	padding-left:10px;
	width:120px;
    border-right: 0;
    }
    .header {
    height: 30px;
    /* background: #95B8E7; */
    color: #404040;
    background: linear-gradient(to bottom,#EFF5FF 0,#E0ECFF 100%);
    background-repeat: repeat-x;
    line-height:30px; /*设置line-height与父级元素的height相等*/
    text-align: center; /*设置文本水平居中*/
    font-size:18px;
	}
	</style>
</head>
<body style="width:100%;height:100%;background:white">
<div id="layout1" class="mini-layout" style="width:100%;height:100%;">
	
	 <div class="header" region="north" height="32" showSplit="false" showHeader="false">
        债券基金（分红）
    </div>
    <div title="center" region="center" bodyStyle="overflow:hidden;">
		<div class="mini-fit" style="width:100%;height:100%;">
		   <div class="mini-splitter" style="width:100%;height:100%;border:0px;" allowResize="false">
		   <div size="70%">
		   <div class="mini-fit" id="field_form">   
				<div class="leftarea" style="margin-top:5px;">
					<input id="cno" name="cno" class="mini-buttonedit" labelField="true"  label="基金管理人编号：" enabled="false" onbuttonclick="onButtonEdit" maxLength="30" style="width:95%;"  labelStyle="width:120px;text-align:left;margin-left:5px;"  required="true" />
					<input id="cname" name="cname" class="mini-textbox" labelField="true"  label="基金管理人名称："  enabled="false" maxLength="255" labelStyle="width:120px;text-align:left;margin-left:5px;" style="width:95%;"   required="true"  />
				</div>
				<div class="rightarea" style="margin-top:5px;">
					<input id="contact" name="contact" class="mini-textbox" labelField="true"  label="基金经理：" enabled="false" maxLength="20" labelStyle="width:120px;text-align:left;margin-left:5px;" style="width:95%;"   />
					<input id="contactPhone" name="contactPhone" class="mini-textbox" labelField="true"  label="联系电话：" enabled="false" labelStyle="width:120px;text-align:left;margin-left:5px;" style="width:95%;"/>
				</div>
				<div class="leftarea" style="margin-top:5px;">
					<input id="invType" name="invType" data="CommonUtil.serverData.dictionary.AccThreeSubject" class="mini-combobox"  labelField="true"  label="会计类型："  labelStyle="text-align:left;width:120px;margin-left:5px;" style="width:95%;"  required="true" />
				</div>
		  	<div  class="mini-panel" title="分红信息*" style="width:100%;"  allowResize="true" collapseOnTitleClick="true">
		  	<div class="centerarea" style="margin-top:5px;">
		<span style="float:left;">
		
		</span>
		
		<div class="mini-toolbar" style="padding:0px;border-bottom:0;" id="toolbar">
	        <table style="width:100%;">
	            <tr>
	                <td style="width:100%;">                
	                    <a id="add_btn" class="mini-button" style="display: none"  >新增</a>
						<a id="delete_btn" class="mini-button" style="display: none"  >删除</a>
	                </td>
	            </tr>
	        </table>
	    </div>
	</div>
		<div class="centerarea" style="height:220px;">
			<div id="tradeManage" class="mini-fit" >      
				<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="dealNo" 
				allowAlternating="true" allowResize="true"  border="true" onrowDblclick="onrowDblclick" showHeader="true" title="分红明细*" showPager="false"
				multiSelect="false" sortMode="client">
					<div property="columns">
						<div type="indexcolumn" headerAlign="center" width="50">序号</div>
						<div field="" align="center" headeralign="center" width="390">
			                申购持仓信息
			                <div property="columns" align="center">
			                        <div field="dealNo" width="90" headerAlign="center" align="center" allowSort="true">（申购）交易单号</div>
			                        <div field="shareAmt" numberFormat="#,0.00" align="right" width="110" headerAlign="center" allowSort="true">申购原始份额（数量）</div>
			                        <div field="reShareAmt" numberFormat="#,0.00" align="right" width="110" headerAlign="center" allowSort="true">当前持仓份额（数量）</div>
			                        <div field="ftpprice" numberFormat="#,0.00000" align="right" width="80" headerAlign="center" allowSort="true">FTP资金价格</div>
			                    </div>
			            </div>
						<div field="" align="center" headeralign="center" width="450">
			                分红信息
			                <div property="columns" align="center">
			                		<!-- <div field="cPrice" numberFormat="#,0.000000" align="right" width="150" headerAlign="center" allowSort="true">分红后单位净值</div> -->
		                        <div field="amt" numberFormat="#,0.00" align="right" width="150" headerAlign="center" allowSort="true">本次分红金额（元）</div>
		                    </div>
			            </div>  
					</div>
				</div>  
			</div>
		</div>
		  	 <div class="leftarea" style="margin-top:5px;">
					<input id="amt" name="amt" class="mini-spinner" labelField="true"  label="分红金额（元）："  enabled="false"  labelStyle="width:120px;text-align:left;margin-left:5px;"  required="true"  onValuechanged="interestAmount" changeOnMousewheel='false' maxValue="999999999999999" format="n2" labelField="true"  labelStyle="text-align:left" style="width:95%;"  required="true"  />
					<input id="price" name="price" class="mini-spinner" labelField="true"  label="分红后单位净值："  labelStyle="width:120px;text-align:left;margin-left:5px;"  required="true"  onValuechanged="interestAmount" changeOnMousewheel='false' maxValue="999" format="n6" labelField="true"  labelStyle="text-align:left" style="width:95%;"  required="true"  />
			</div>
			<div class="rightarea" style="margin-top:5px;">
				<input id="vdate" name="vdate" class="mini-datepicker" allowInput="false" labelField="true" label="分红宣告日期："  labelStyle="width:120px;text-align:left;margin-left:5px;" style="width:95%;"  required="true" />
				<input id="tdate" name="tdate" class="mini-datepicker" allowInput="false" labelField="true" label="分红确认日期："  labelStyle="width:120px;text-align:left;margin-left:5px;" style="width:95%;"  required="true" />
		   	</div>
		   </div>
			<div  class="mini-panel" title="审批信息*" style="width:100%;"  allowResize="true" collapseOnTitleClick="true">
				<div class="leftarea" style="margin-top:5px;">
				    <input id="dealNo" name="dealNo" class="mini-textbox" labelField="true" style="width:95%;" label="审批单编号：" enabled="false" labelStyle="width:120px;text-align:left;margin-left:5px;"  emptyText="由系统默认自动生成"/>
					<input id="adate" name="adate" class="mini-datepicker" labelField="true" label="审批日期："  labelStyle="width:120px;text-align:left;margin-left:5px;"  enabled="false" style="width:95%;"   required="true" />
				</div>
				<div class="rightarea" style="margin-top:5px;">
					<input id="sponsor" name="sponsor" class="mini-textbox" labelField="true"  label="审批发起人：" labelStyle="width:120px;text-align:left;margin-left:5px;" style="width:95%;"   required="true"  enabled="false" />
					<input id="sponinst" name="sponinst" class="mini-combobox" labelField="true"  label="审批发起机构：" labelStyle="width:120px;text-align:left;margin-left:5px;" style="width:95%;"   required="true"  enabled="false"/>
					
				</div>
			</div>
			<%@ include file="../../Common/Flow/MiniApproveOpCommon.jsp"%>
			</div>
			</div>
		   <div size="30%">
		   <div id="panel1" class="mini-panel" title="请选择需要分红的基金*" style="width:100%;height:99%;"  allowResize="true" collapseOnTitleClick="true">
		   	<table  id="fundForm" class="form-table" width="100%" >
				<tr>
					<th>基金代码：</th>
					<td>
						<input id="fundCode" name="fundCode" class="mini-buttonedit" style="width:95%"  required="true"   onbuttonclick="selectFundInfo"/>
					</td>
				</tr>
				<tr>
					<th>基金简称：</th>
					<td>
						<input id="fundName" name="fundName"  class="mini-textbox"  style="width:95%"  required="true"  enabled="false" />
					</td>
				</tr>
				<tr>
					<th>基金全称：</th>
					<td>
						<input id="fundFullName" name="fundFullName" style="width:95%" class="mini-textbox" enabled="false" />
					</td>
				</tr>
				<tr>
					<th>	基金类型：</th>
					<td>
						<input id="fundType" name="fundType" style="width:95%"  class="mini-textbox" enabled="false" />
					</td>
				</tr>
				
				<tr>
				<th>基金币种：</th>
				<td>
					<input id="ccy" name="ccy"  style="width:95%" data="CommonUtil.serverData.dictionary.Currency" class="mini-combobox"  enabled="false"/>
				</td>
				</tr>
				<tr>
				<th>起息规则：</th>
				<td>
					<input id="vType" name="vType"  style="width:95%" data="CommonUtil.serverData.dictionary.ValueType" class="mini-combobox"  enabled="false"/>
				</td>
				</tr>
				<tr>
					<th>基金管理人编码：</th>
					<td>
						<input id="managComp" name="managComp" style="width:95%"  class="mini-textbox" enabled="false"/>
					</td>
				</tr>
				<tr>
					<th>基金管理人名称：</th>
					<td>
						<input id="managCompNm" name="managCompNm" style="width:95%"  class="mini-textbox"  enabled="false"/>
					</td>
				</tr>
				<tr>
					<th>基金经理：</th>
					<td>
						<input id="managerMen" name="managerMen" style="width:95%" class="mini-textbox"  enabled="false"/>
					</td>
				</tr>
				<tr>
					<th>基金经理联系方式：</th>
					<td>
						<input id="managerMenPhone" name="managerMenPhone" style="width:95%" class="mini-textbox"  enabled="false"/>
					</td>
				</tr>
				<tr>
					<th>份额结转方式：</th>
					<td>
						<input id="coverMode" name="coverMode" data="CommonUtil.serverData.dictionary.CoverMode" class="mini-combobox"style="width:95%" enabled="false"/>
					</td>
				</tr>
				<tr>
					<th>份额结转日期：</th>
					<td>
						<input id="coverDay" name="coverDay"  class="mini-spinner"  style="width:95%"  enabled="false"/>
					</td>
				</tr>
				<tr>
					<th>备注信息：</th>
					<td colspan="3">
						<input id="remark" name="remark" style="width:95%;height:180px;" class="mini-textarea" enabled="false" />
					</td>
				</tr>
			</table>
			</div>
		   </div>
		   
		   </div>
		   </div>
	</div>

	
	<div title="south" region="south" showSplit="false" showHeader="false" height="39px;"  style="line-height:38px;text-align: center;">
       <div id="functionIds">
			<a class="mini-button" style="display: none"  style="width:120px;" id="save_btn"  onclick="save">保存交易</a>
			<a class="mini-button" style="display: none"  style="width:120px;" id="close_btn"  onclick="close">关闭界面</a>
		</div>
    </div>
</div>
	<script type="text/javascript">
	mini.parse();
	//获取当前tab
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var row=params.selectedData;
	var action =params.action;
	var form =new mini.Form("#field_form");
	var prdNo = '802';//债币基金
	var dealType = "1";//事前审批
	var grid=mini.get("datagrid");
	var tradeData={};
	if(row!=null){
		tradeData.selectData=row;
		tradeData.operType=action;
		tradeData.serial_no=row.dealNo;
		tradeData.task_id=row.taskId;
	}
	//保存审批单
	function save(){
		//表单验证！！！
		form.validate();
		var fundForm  = new mini.Form("#fundForm");
		fundForm.validate();
		if (form.isValid() == false) {
			return;
		}

		var data=form.getData(true);
		var reddDetailsList = 
		data['fundCode'] = mini.get("fundCode").getValue();
		data['ccy'] = mini.get("ccy").getValue();
		data['prdNo']=prdNo;
		data['dealType']=dealType;
		data['trdtype']=ApproveTrdType.FundTrdType.Rdd;//交易操作类型
		var reddDetailsList=grid.getData();
		data['reddDetailsList']=reddDetailsList;
		var params=mini.encode(data);
		CommonUtil.ajax({
			url:"/ApproveFundManageController/saveApprove",
			data:params,
			callback:function(data){
				mini.alert("债基分红-保存成功！请确认信息后再提交审批！",'提示信息',function(){
					top["win"].closeMenuTab();
					})
			}
		});
	}

	function selectFundInfo(){
		mini.open({
            url: CommonUtil.baseWebPath() + "./Fund/bond/FundBaseMini.jsp",
            title: "债币基金信息",
            width: 924,
			height: 500,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                    		var fundForm  = new mini.Form("#fundForm");
                    		fundForm.setData(data)
                    		mini.get("fundCode").setValue(data.fundCode);
    						mini.get("fundCode").setText(data.fundCode);
    						mini.get("fundCode").setValue(data.fundCode);
    						mini.get("fundCode").setText(data.fundCode);
    						mini.get("cno").setValue(data.managComp);
    						mini.get("cno").setText(data.managComp);
    						mini.get("cname").setValue(data.managCompNm);
    						mini.get("contact").setValue(data.managerMen);
    						mini.get("contactPhone").setValue(data.managerMenPhone);
    						//mini.get("invType").setValue(data.invtype);
    						fundForm.validate();
                    }
                }
            }
        });
	}
	function initGrid(){
		var url="/FundController/searchReddDetails";
		var data={};
		data['tradeNo']=mini.get("dealNo").getValue();
		var params=mini.encode(data);
		CommonUtil.ajax({
			url:url,
			data:params,
			callback : function(data) {
				grid.setData(data.obj);
			}
		});
	}
	$(document).ready(function(){
		if($.inArray(action,["approve","detail"])>-1){
			mini.get("save_btn").setVisible(false);
			form.setEnabled(false);
			var form11=new mini.Form("#approve_operate_form");
			form11.setEnabled(true);
			mini.get("toolbar").setVisible(false);
		}else if(action=="add"){//增加时，设置审批发起人 审批发起机构
			mini.get("sponsor").setValue("<%=__sessionUser.getUserId()%>");
			mini.get("sponinst").setValue("<%=__sessionUser.getInstId() %>");
			mini.get("vdate").setValue("<%=__bizDate%>");
			mini.get("adate").setValue("<%=__bizDate%>");
			mini.get("tdate").setValue("<%=__bizDate%>");
			
		}
		if($.inArray(action,["edit","approve","detail"])>-1||null != row){
			form.setData(row);
			initGrid();
			mini.get("cno").setText(row.cno);
			CommonUtil.ajax({
				url:"/FundInfoController/seachInfoByCode",
				data:{'fundCode':row.fundCode},
				callback:function(data){
					if(data.obj){
						var fundForm  = new mini.Form("#fundForm");
						fundForm.setData(data.obj);
						mini.get("fundCode").setValue(data.obj.fundCode);
						mini.get("fundCode").setText(data.obj.fundCode);
					}
				}
			});
		}
	})
	
	
	
	$("#add_btn").on("click",function(){
		selectReddInfo();
	});
	$("#delete_btn").on("click",function(){
		mini.confirm("您确认要删除选中记录?","系统警告",function(value){   
			if (value=='ok'){   
			var data = grid.getSelected();
			grid.removeRow ( data, true );
			mini.get("amt").setValue(mini.get("amt").getValue()-data.amt);
			}
		}
		);
	});
	function selectReddInfo(){
		if(mini.get("fundCode").getValue()==null||mini.get("fundCode").getValue()==''){
			mini.alert("请选择基金代码！",'提示信息');
			return;
		}
		if(mini.get("invType").getValue()==null||mini.get("invType").getValue()==''){
			mini.alert("请选择会计类型！",'提示信息');
			return;
		}
		mini.open({
            url: CommonUtil.baseWebPath() + "./Fund/bond/FundRddMini.jsp",
            title: "选择需要分红的明细",
            width: 924,
			height: 500,
			onload: function () {
				var iframe = this.getIFrameEl();
				var data=form.getData(true);
				iframe.contentWindow.SetData(data);
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    data['dealNo'] = data.tradeNo;
                    if (data) {
             			//判定是不是针对同一个审批分红流水进行操作的；如果是，那么合二为一
             			var rows = grid.getData();
             			var flag = true;
             			for(var i=0,l=rows.length;i<l;i++){
             			   var row = rows[i]
             			   if(row.dealNo==data.dealNo){
             				  row.amt = row.amt+data.amt;
            				  	  grid.updateRow(row,row);
            				  	  mini.get("amt").setValue(mini.get("amt").getValue()+data.amt);
             				  flag = false;
             			   }
             			}
             			if(flag){
	                         grid.addRow(data, 0);
	                         grid.deselectAll();
	                         grid.select(data);
	                         mini.get("amt").setValue(mini.get("amt").getValue()+data.amt);
             			}
             		
                    }
                }
            }
        });
	}
	function close(){
		top["win"].closeMenuTab();
	}
	</script>
	<script type="text/javascript" src="../../Common/Flow/MiniApproveOpCommon.js"></script>			
</body>
</html>