<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
    <title></title>
  </head>
  
<body style="width:100%;height:100%;background:white">
	<div class="mini-panel" title="债券基金申购列表（已打款）" style="width:100%">
	<div style="width:100%;padding-top:10px;">
		<div id="search_form" class="mini-form" width="80%">
			<input id="dealNo" name="dealNo" class="mini-textbox" vtype="maxLength:50" labelField="true"  label="交易单号：" labelStyle="text-align:right;"  emptyText="请输入交易单号"/>
			<!-- <input id="approveStatus" name="approveStatus" class="mini-combobox" data = "CommonUtil.serverData.dictionary.ApproveStatus" labelField="true"  label="审批状态：" labelStyle="text-align:right;" emptyText="请选择审批单状态"> -->
			<span style="float:right;margin-right: 150px">
				<a id="search_btn" class="mini-button" style="display: none"  onclick="query()">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"  >清空</a>
			</span>
		</div>
	</div>
	</div>
	<div id="tradeManage" class="mini-fit">
		<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="dealNo" 
		allowAlternating="true" allowResize="true" onrowdblclick="onRowDblClick" border="true" 
		multiSelect="false" sortMode="client">
			<div property="columns">
				<div type="indexcolumn" headerAlign="center" width="50">序号</div>
				<div field="fundName" width="280" headerAlign="center" align="center" allowSort="true">基金名称</div>
				<div field="amt" numberFormat="#,0.00" align="right" width="150" headerAlign="center" allowSort="true">打款本金（元）</div>
				<div field="cname" width="280" headerAlign="center" allowSort="true">客户名称</div>  
				<div field="fundCode" width="80" headerAlign="center" align="center" allowSort="true">基金代码</div>  
				<div field="ccy" width="80" headerAlign="center" align="center" allowSort="true">基金币种</div>
			</div>
		</div>  
	</div>
	<script type="text/javascript">
		mini.parse();
		var form=new mini.Form("#search_form");
		var grid=mini.get("datagrid");
		top["fundAfpMini"] = window;
		var rows='';
		var fundCode='';
		/* 按钮 查询事件 */
	function query(){
		search(grid.pageSize,0);
	}

	/* 查询 */
	function search(pageSize,pageIndex){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统也提示");
			return;
		}

		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		data['prdNo'] = '802';
		data['trdtype']=ApproveTrdType.FundTrdType.Afp;//交易操作类型
		url = "/ApproveFundManageController/searchFundDealsPage";
		data['approveType']=1;
		data['approveStatus']='6';
		data['isConfirm']="0";
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:url,
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});
	//选中一行将数据展示
	function onRowDblClick(){
		CloseWindow("ok");
	}
	function GetData() {
        var row = grid.getSelected();
        return row;
	}
	function SetData(data){
		rows=data.gridData;
		fundCode=data.fundCode;
	}
	function CloseWindow(action) {
        if (window.CloseOwnerWindow)
            return window.CloseOwnerWindow(action);
        else
            window.close();
	}
    //关闭窗口
    function onCancel() {
        CloseWindow("cancel");
    }
	$(document).ready(function(){
		search(10,0);
	})
	</script>
</body>
</html>