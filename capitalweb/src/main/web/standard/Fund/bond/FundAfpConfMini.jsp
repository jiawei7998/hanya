<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
    	<style>
	table th{
	text-align:left;
	padding-left:10px;
	width:120px;
    border-right: 0;
    }
    .header {
    height: 30px;
    /* background: #95B8E7; */
    color: #404040;
    background: linear-gradient(to bottom,#EFF5FF 0,#E0ECFF 100%);
    background-repeat: repeat-x;
    line-height:30px; /*设置line-height与父级元素的height相等*/
    text-align: center; /*设置文本水平居中*/
    font-size:18px;
	}
	</style>
</head>
<body style="width:100%;height:100%;background:white">
<div id="layout1" class="mini-layout" style="width:100%;height:100%;">
	
	 <div class="header" region="north" height="30" showSplit="false" showHeader="false">
        基金持仓明细
    </div>
    <div title="center" region="center" bodyStyle="overflow:hidden;">
	<div style="width:100%;padding-top:10px;">
		<div id="search_form" class="mini-form" width="80%">
		<input id="tradeNo" name="tradeNo" class="mini-textbox" vtype="maxLength:50" labelField="true"  label="交易单号：" labelStyle="text-align:right;"  emptyText="请输入交易单号"/>
		<input id="cno" name="cno" class="mini-textbox" labelField="true"  label="客户号：" labelStyle="text-align:right;" emptyText="请输入客户号"/>
			<span style="float:right;margin-right: 110px">
				<a id="search_btn" class="mini-button" style="display: none"  onclick="query">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"  >清空</a>
			</span>
		</div>
	</div>
	<div id="tradeManage" class="mini-fit">      
		<div id="datagrid" class="mini-datagrid" style="width:100%;height:100%;" idField="dealNo" 
		allowAlternating="true" allowResize="true"  border="true"  onRowclick="gridSelect"
		multiSelect="false" sortMode="client"><!-- onrowdblclick="onrowdblclick" -->
			<div property="columns">
				<div type="indexcolumn" headerAlign="center" width="50">序号</div>
				<div field="cname" width="200" headerAlign="center" allowSort="true">客户名称</div>
				<div field="fundCode" width="80" headerAlign="center" align="center" allowSort="true">基金代码</div>
				<div field="fundName" width="200" headerAlign="center" align="center" allowSort="true">基金名称</div>    
				<div field="ccy" width="80" headerAlign="center" align="center" allowSort="true">基金币种</div>   
				<div field="shareAmt" numberFormat="#,0.00" align="right" width="150" headerAlign="center" allowSort="true">总份额（数量）</div>
				<div field="reShareAmt" numberFormat="#,0.00" align="right" width="150" headerAlign="center" allowSort="true">可赎回份额（数量）</div>
				<div field="sponinst" width="120" headerAlign="center" allowSort="true">所属机构</div>
			</div>
		</div>  
	</div>
	</div>
	<div title="south" region="south" showSplit="false" showHeader="false" height="108px;"  style="line-height:108px;text-align: center;">
       <div id="data_form" class="mini-form" width="100%">
			<div class="leftarea" style="margin-top:5px;">
				<input id="reShareAmt" name="reShareAmt" class="mini-spinner" labelField="true"  label="可赎回份额（份）：" enabled="false"  required="true"   changeOnMousewheel='false' maxValue="999999999999999" format="n2" labelField="true"  labelStyle="text-align:left;margin-left:5px;" style="width:95%;"/>
				<input id="qty" name="qty" class="mini-spinner" labelField="true"  label="赎回份额（份）："  onvalueChanged="amtChanged" required="true"   changeOnMousewheel='false' maxValue="999999999999999" format="n2" labelField="true"  labelStyle="text-align:left;margin-left:5px;" style="width:95%;" />
			</div>
			<div class="leftarea" style="margin-top:5px;">
				<input id="amt" name="amt" class="mini-spinner" labelField="true"  label="赎回金额（元）："   required="true"   changeOnMousewheel='false' maxValue="999999999999999" format="n2" labelField="true"  labelStyle="text-align:left;margin-left:5px;" style="width:95%;"/>
				<input id="intamt" name="intamt" class="mini-spinner" labelField="true"  label="赎回红利（元）："  required="true"   changeOnMousewheel='false' maxValue="999999999999999" format="n2" labelField="true"  labelStyle="text-align:left;margin-left:5px;" style="width:95%;"/>
			</div>		
			<div class="centerarea" style="margin-top:5px;">
				<a class="mini-button" style="display: none"  style="width:120px;" id="save_btn">确定</a>
			</div>
		</div>
    </div>
</div>
<script type="text/javascript">
	mini.parse();
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var prdNo='802';
	var prdName='债券基金';
	var form=new mini.Form("#search_form");
	var grid=mini.get("datagrid");
	top["fundRdpConfEdit"] = window;
	var getParams={};	
	$("#save_btn").on("click",function(){
		var value = mini.get("qty").getValue();
		var reAmt=mini.get("reShareAmt").getValue();//剩余可确认的金额
		if(value>reAmt){
			mini.alert("超出可赎回份额，请重新输入","提示");
			return ;
		}
		onrowdblclick();
	});
	/* 按钮 查询事件 */
	function query(){
		search(grid.pageSize,0);
	}
	/* 查询 */
	function search(pageSize,pageIndex){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统也提示");
			return;
		}

		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		data['prdNo']=prdNo;
		data['trdtype']=ApproveTrdType.FundTrdType.Afp;//交易操作类型
		data['dealType']='2';//确认的申购交易
		url = "/ApproveFundManageController/searchFundDealsPage";
		data['approveType']=4;
		data['fundCode']=getParams.fundCode;
		data['cno']=getParams.cno;
		data['sponinst']=getParams.sponinst;
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:url,
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});
	function onrowdblclick(){
		CloseWindow("ok");
	}
	function GetData() {
        var row = grid.getSelected();
        row['amt'] = mini.get("amt").getValue();
        row['qty'] = mini.get("qty").getValue();
        row['intamt'] = mini.get("intamt").getValue();
        return row;
	}
	function SetData(data){
		getParams=mini.encode(data);
		search(10,0);
	}
	function CloseWindow(action) {
        if (window.CloseOwnerWindow)
            return window.CloseOwnerWindow(action);
        else
            window.close();
	}
	function gridSelect(){
        var data = grid.getSelected();
        mini.get("reShareAmt").setValue(data.reShareAmt);
        mini.get("qty").setValue(data.reShareAmt);
        mini.get("amt").setValue(data.reShareAmt);
    }
    //关闭窗口
    function onCancel() {
        CloseWindow("cancel");
    }
    function amtChanged(e){
		var value=e.value;//本次确认金额
		var reAmt=mini.get("reShareAmt").getValue();//剩余可确认的金额
		if(value>reAmt){
			mini.alert("超出可赎回份额，请重新输入","提示");
			return ;
		}
	    mini.get("amt").setValue(value);//份额输入后自动填写一样的值 债券基金
	}
	</script>
</body>
</html>