<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
	<script src="../js/echarts.min.js"></script>
<style>
	table th{
	text-align:left;
	padding-left:10px;
	width:120px;
    border-right: 0;
    }
    .header {
    height: 30px;
    /* background: #95B8E7; */
    color: #404040;
    background: linear-gradient(to bottom,#EFF5FF 0,#E0ECFF 100%);
    background-repeat: repeat-x;
    line-height:30px; /*设置line-height与父级元素的height相等*/
    text-align: center; /*设置文本水平居中*/
    font-size:18px;
	}
	</style>
</head>
<body style="width:100%;height:100%;background:white">
<div id="layout1" class="mini-layout" style="width:100%;height:100%;">
	
	 <div class="header" region="north" height="32" showSplit="false" showHeader="false">
        债券基金持仓信息
    </div>
    <div region="east" bodyStyle="overflow:hidden;" width="700px;" showSplit="false" showHeader="false">
		<div id="chartContainer" style="height:99%;width:99%;"></div>
	</div>
	<div title="center" region="center" bodyStyle="overflow:hidden;">
	<div style="width:100%;padding-top:10px;">
		<div id="search_form" class="mini-form" width="100%">
			<input id="fundCode" name="fundCode" class="mini-textbox" labelField="true"  label="基金代码：" labelStyle="text-align:right;width:80px;"  emptyText="请输入精确代码"/>
			<input id="fundName" name="fundName" class="mini-textbox" vtype="maxLength:50" labelField="true"  label="基金名称：" labelStyle="text-align:right;width:80px;"  emptyText="请输入模糊名称"/>
			<span style="float:right;margin-right: 0px">
				<a id="search_btn" class="mini-button" style="display: none"  onclick="query">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"  >清空</a>
			</span>
		</div>
	</div>

	<div id="fundManage" class="mini-fit" >      
		<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="dealNo"  title="基金信息列表" showHeader="true"
		allowAlternating="true" allowResize="true" onrowdblclick="onRowDblClick" border="true" onrowclick="onRowClick"
		multiSelect="false" sortMode="client">
			<div property="columns">
				<div type="indexcolumn" headerAlign="center" width="50">序号</div>
				<div field="fundCode" width="70" headerAlign="center" align="left" allowSort="true">基金代码</div>
				<div field="fundName" width="150" headerAlign="center" align="left" allowSort="true">基金名称</div>
				<div field="ccy" width="60" headerAlign="center" align="center" allowSort="true" renderer="CommonUtil.dictRenderer" data-options="{dict:'Currency'}">基金币种</div> 
				<div field="utQty" numberFormat="#,0.00" align="right" width="120" headerAlign="center" allowSort="true">持仓份额（数量）</div>
				<!-- <div field="utAmt" numberFormat="#,0.00" align="right" width="120" headerAlign="center" allowSort="true">持仓本金（元）</div> -->
				<div field="yield_8" numberFormat="#,0.00000" align="right" width="120" headerAlign="center" allowSort="true">单位净值（%）</div>
				<div field="postdate" align="right" width="80" headerAlign="center" allowSort="true">账务日期</div>
			</div>
		</div>  
	</div>
	</div>
</div>
	<script type="text/javascript">
		mini.parse();
		var currTab = top["win"].tabs.getActiveTab();
		var params = currTab.params;
		var prdNo="9999";
		var prdName='基金信息';
		top["fundHdManager"] = window;
		var grid=mini.get("datagrid");
		var form=new mini.Form("#search_form");
		var chart = echarts.init(document.getElementById('chartContainer'));
		var grid2 = mini.get("datagrid");
	function query(){
		search(grid2.pageSize,0);
	}
		/* 查询 */
	function search(pageSize,pageIndex){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统也提示");
			return;
		}

		var data2=form.getData(true);
		data2['pageNumber']=pageIndex+1;
		data2['pageSize']=pageSize;
		data2['branchId']=branchId;
		data2['prdNo']='802';
		url = "/FundTposController/getFtTposList";

		var params2 = mini.encode(data2);
		CommonUtil.ajax({
			url:url,
			data:params2,
			callback : function(data) {
				grid2.setTotalCount(data.obj.total);
				grid2.setPageIndex(pageIndex);
		        grid2.setPageSize(pageSize);
				grid2.setData(data.obj.rows);
			}
		});
		
		
		CommonUtil.ajax({
			url:"/FundTposController/getFtTposForPair",
			data:params2,
			callback : function(data) {
				option = {
			            title: {
			                text: '基金存量持仓柱状图'
			            },
			            tooltip: {
			                trigger: 'axis',
			                axisPointer: {
			                    type: 'shadow'
			                }
			            },
			            legend: {
			                data: ['基金份额']
			            },
			            grid: {
			                left: '1%',
			                right: '1%',
			                bottom: '1%',
			                containLabel: true
			            },
			            xAxis: {
			                type: 'value',
			                boundaryGap: [0, 0.5]
			            },
			            yAxis: {
			                type: 'category',
			                data: data.obj.left
			            },
			            series: [
			                {
			                    name: '基金份额（万份）',
			                    type: 'bar',
			                    data: data.obj.right,
			                  //顶部数字展示pzr
			                    itemStyle: {
			                        normal: {
			                            //柱形图圆角，初始化效果
			                            label: {
			                                show: true,//是否展示
			                                textStyle: {
			                                    fontWeight:'bolder',
			                                    fontSize : '12',
			                                    fontFamily : '微软雅黑',
			                                }
			                            }
			                        }
			                    }
			                }
			            ],
			            barWidth: 26,
	                    itemStyle:{
	                        normal:{
	                            color:'#CD7054'
	                        }
	                    },
	                    label:{ 
		                   normal:{ 
		                       show: true, 
		                       position: 'insideTop'} 
		                       }
			        };
				 chart.setOption(option);
			}
		});
	}
	
	grid2.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});
	$(document).ready(function(){
		search(20,0);
	})
	
	
	
	
    
	</script>
</body>
</html>