<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
	<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
    <title></title>
  </head>
  
<body style="width:100%;height:100%;background:white">
	<div class="mini-panel" title="债券基金信息" style="width:100%;">
	<div style="width:100%;padding-top:10px;">
		<div id="search_form" class="mini-form" width="80%">
			<input id="fundCode" name="fundCode" class="mini-buttonedit" onbuttonclick="onInsQuery" labelField="true"  label="基金代码：" labelStyle="text-align:right;width:80px;" allowInput="false" emptyText="请输入精确代码"/>
			<input id="fundName" name="fundName" class="mini-textbox" vtype="maxLength:50" labelField="true"  label="基金名称：" labelStyle="text-align:right;width:80px;"  emptyText="请输入模糊名称"/>
			<span style="float:right;margin-right: 150px">
				<a id="search_btn" class="mini-button" style="display: none"  >查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"  >清空</a>
				<a id="add_btn" class="mini-button" style="display: none"  >新增</a>
				<a id="edit_btn" class="mini-button" style="display: none"  >修改</a>
				<a id="delete_btn" class="mini-button" style="display: none"  >删除</a>
			</span>
		</div>
	</div>
	</div>
	<div id="fundManage" class="mini-fit" >      
		<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="dealNo"  title="基金信息列表" showHeader="true"
		allowAlternating="true" allowResize="true" onrowdblclick="onRowDblClick" border="true" onrowclick="onRowClick"
		multiSelect="false" sortMode="client">
			<div property="columns">
				<div type="indexcolumn" headerAlign="center" width="50">序号</div>
				<div field="fundCode" width="80" headerAlign="center" align="center" allowSort="true">基金代码</div>  
				<div field="fundName" width="150" headerAlign="center" align="center" allowSort="true">基金简称</div>
				<div field="ccy" width="80" headerAlign="center" align="center" allowSort="true" renderer="CommonUtil.dictRenderer" data-options="{dict:'Currency'}">基金币种</div> 
				<div field="fundType" width="120" headerAlign="center" allowSort="true" renderer="CommonUtil.dictRenderer" data-options="{dict:'FundType'}">基金类型</div>   
				<div field="managCompNm" width="220" headerAlign="center" allowSort="true">基金管理人名称</div> 
				<div field=vType width="120" headerAlign="center" allowSort="true" renderer="CommonUtil.dictRenderer" data-options="{dict:'ValueType'}">起息规则</div> 
				<div field=managerMen width="120" headerAlign="center" allowSort="true">基金经理</div> 
				<div field="coverMode" width="120" headerAlign="center" align="center" allowSort="true" renderer="CommonUtil.dictRenderer" data-options="{dict:'CoverMode'}">份额结转方式</div>
				<div field="coverDay" width="120" headerAlign="center" align="center" allowSort="true">份额结转日期</div>
			</div>
		</div>  
	</div>
	<script type="text/javascript">
		mini.parse();
		var currTab = top["win"].tabs.getActiveTab();
		var params = currTab.params;
		var prdNo="9999";
		var prdName='基金信息';
		top["fundBaseManager"] = window;
		var grid=mini.get("datagrid");
		var form=new mini.Form("#search_form");
		function functionMain(prdNo,url,prdName,title,details){
			var url = CommonUtil.baseWebPath() + url;
			var tab = {
				id : prdNo + "_"+details,
				name : prdNo + "_"+details,
				iconCls : "",
				title : prdName + title,
				url : url,
				showCloseButton:true
			};
			var paramData = {'operType':'A','closeFun':'query','pWinId':'fundBaseManager',selectedData:grid.getSelected(),action:details};
			
			CommonUtil.openNewMenuTab(tab,paramData);
		}
		$("#add_btn").on("click",function(){
			var messageid = mini.loading("系统正在处理...", "请稍后");
			try {
				functionMain('001',"/Fund/FundBaseDetail.jsp","债券基金","新增","add");
			} catch (error) {
				
			}
			mini.hideMessageBox(messageid);
		});
		//修改
		$("#edit_btn").on("click",function(){
			var messageid = mini.loading("系统正在处理...", "请稍后");
			try {
				functionMain('001',"/Fund/bond/FundBaseDetail.jsp","债券基金","修改","edit");
			} catch (error) {
				
			}
			mini.hideMessageBox(messageid);
		})
		//删除
		$("#delete_btn").on("click",function(){
			var rows=grid.getSelecteds();
			if(rows.length==0){
				mini.alert("请选中一行","提示");
				return;
			}
			mini.confirm("您确认要删除选中记录?","系统警告",function(value){   
				if (value=='ok'){   
					var data=rows[0];
					data['productType']="FtInfo";
					params=mini.encode(data);
					CommonUtil.ajax( {
						url:"/FundController/deleteApprove",
						data:params,
						callback : function(data) {
							mini.alert("删除成功.","系统提示");
							search(10,0);
						}
					});
				}
			});
		})

		/* 按钮 查询事件 */
	function query(){
		search(grid.pageSize,0);
	}
	/* 查询 */
	function search(pageSize,pageIndex){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统也提示");
			return;
		}

		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		data['fundType']='802';
		url = "/FundInfoController/seachInfoPage";

		var params = mini.encode(data);
		CommonUtil.ajax({
			url:url,
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});
	$(document).ready(function(){
		search(10,0);
	})
	</script>
</body>
</html>