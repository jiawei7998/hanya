<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
  <head>
	<script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
	<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
    <title></title>
  </head>
  
<body style="width:100%;height:100%;background:white">
	<div class="mini-panel" title="货币基金赎回" style="width:100%">
		<div style="width:100%;padding-top:10px;">
			<div id="search_form" class="mini-form" width="80%">
				<input id="sponInst" name="sponInst" class="mini-buttonedit" onbuttonclick="onInsQuery" labelField="true"  label="所属机构：" labelStyle="text-align:right;" allowInput="false" emptyText="请选择机构"/>
				<input id="dealNo" name="dealNo" class="mini-textbox" vtype="maxLength:50" labelField="true"  label="交易单号：" labelStyle="text-align:right;"  emptyText="请输入交易单号"/>
				<input id="cNo" name="cNo" class="mini-textbox" labelField="true"  label="客户号：" labelStyle="text-align:right;" emptyText="请输入客户号"/>
				<input id="approveStatus" name="approveStatus" class="mini-combobox" data = "CommonUtil.serverData.dictionary.ApproveStatus" labelField="true"  label="审批状态：" labelStyle="text-align:right;" emptyText="请选择审批单状态">
				<span style="float:right;margin-right: 150px">
					<a id="search_btn" class="mini-button" style="display: none"  onclick="query">查询</a>
					<a id="clear_btn" class="mini-button" style="display: none"  >清空</a>
				</span>
			</div>
		</div>
	

	<table width="100%">
		<tr>
			<td width="60%" style="padding:10px 0 5px 5px">
					<a id="add_btn" class="mini-button" style="display: none"  >新增</a>
					<a id="edit_btn" class="mini-button" style="display: none"  >修改</a>
					<a id="delete_btn" class="mini-button" style="display: none"  >删除</a>
					<a id="approve_commit_btn" class="mini-button" style="display: none"   >审批</a>
					<a id="approve_mine_commit_btn" class="mini-button" style="display: none"  >提交预审</a>
					<a id="approve_log" class="mini-button" style="display: none"   >审批日志</a>
					<a id="print_bk_btn" class="mini-button" style="display: none"    onclick="print">打印</a>
			</td>

			<td width="40%">
				<div id = "approveType" name = "approveType" class="mini-checkboxlist" style="float:right;width:100%; " labelField="true" 
					label="审批列表选择：" labelStyle="text-align:right;" 
					value="approve" textField="text" valueField="id" multiSelect="false"
					data="[{id:'approve',text:'待审批列表'},{id:'finished',text:'已审批列表'},{id:'mine',text:'我发起的'}]" 
					onvaluechanged="checkBoxValuechanged">
				</div>
			</td>
		</tr>
	</table>
	</div>
	<div id="tradeManage" class="mini-fit">      
		<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="dealNo" 
		allowAlternating="true" allowResize="true" onrowdblclick="onRowDblClick" border="true" 
		multiSelect="false" sortMode="client">
			<div property="columns">
				<div type="indexcolumn" headerAlign="center" width="50">序号</div>
				<div field="approveStatus" width="70" renderer="CommonUtil.dictRenderer" data-options="{dict:'ApproveStatus'}" align="center" headerAlign="center">审批状态</div>
				<div field="cname" width="280" headerAlign="center" allowSort="true">客户名称</div> 
				<div field="fundCode" width="80" headerAlign="center" align="center" allowSort="true">基金代码</div>  
				<div field="fundName" width="280" headerAlign="center" allowSort="true">基金名称</div> 
				<div field="shareAmt" numberFormat="#,0.00" align="right" width="150" headerAlign="center" allowSort="true">赎回份额（数量）</div>
				<div field="vdate" width="80" headerAlign="center" align="center" allowSort="true">赎回日期</div>
				<div field="adate" width="100" headerAlign="center" align="center" allowSort="true">审批日期</div>   
				<div field="ccy" width="80" headerAlign="center" align="center" allowSort="true">基金币种</div> 
				<div field="sponsor" width="100" headerAlign="center" allowSort="true">审批发起人</div>   
				<div field="sponinst" width="120" headerAlign="center" allowSort="true">审批发起机构</div>
				<div field="dealNo" width="180" headerAlign="center" align="center" allowSort="true">交易单号</div>   
			</div>
		</div>  
	</div>
	<script type="text/javascript">
		mini.parse();
		top["fundRdpManager"] = window;
		var currTab = top["win"].tabs.getActiveTab();
		var params = currTab.params;
		var prdNo='801';
		var prdName="货币基金";
		var form=new mini.Form("#search_form");
		var grid=mini.get("datagrid");
		function functionMain(prdNo,url,prdName,title,details){
			var url = CommonUtil.baseWebPath() + url;
			var tab = {
				id : prdNo + "_"+details,
				name : prdNo + "_"+details,
				iconCls : "",
				title : prdName + title,
				url : url,
				showCloseButton:true
			};
			var paramData = {'operType':'A','closeFun':'query','pWinId':'fundRdpManager',selectedData:grid.getSelected(),action:details};
			
			CommonUtil.openNewMenuTab(tab,paramData);
		}
		$("#add_btn").on("click",function(){
			var messageid = mini.loading("系统正在处理...", "请稍后");
			try {
				functionMain('801',"/Fund/FundRdpEdit.jsp","货币基金赎回","新增","add");
			} catch (error) {
				
			}
			mini.hideMessageBox(messageid);
		});
		//修改
		$("#edit_btn").on("click",function(){
			var messageid = mini.loading("系统正在处理...", "请稍后");
			try {
				functionMain('801',"/Fund/FundRdpEdit.jsp","货币基金赎回","修改","edit");
			} catch (error) {
				
			}
			mini.hideMessageBox(messageid);
		})
		//删除
		$("#delete_btn").on("click",function(){
			var rows=grid.getSelecteds();
			if(rows.length==0){
				mini.alert("请选中一行","提示");
				return;
			}
			mini.confirm("您确认要删除选中记录?","系统警告",function(value){   
				if (value=='ok'){   
					var data=rows[0];
					data['productType']="FtRdp";
					data['trdtype']=ApproveTrdType.FundTrdType.Rdp;//交易操作类型
					params=mini.encode(data);
					CommonUtil.ajax( {
						url:"/ApproveFundManageController/deleteApprove",
						data:params,
						callback : function(data) {
							mini.alert("删除成功.","系统提示");
							search(10,0);
						}
					});
				}
			});
		})

		/* 按钮 查询事件 */
	function query(){
		search(grid.pageSize,0);
	}

	/* 查询 */
	function search(pageSize,pageIndex){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统也提示");
			return;
		}

		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		data['prdNo'] = prdNo;
		data['trdtype']=ApproveTrdType.FundTrdType.Rdp;//交易操作类型
		data['dealType']='1';
		url = "/ApproveFundManageController/searchFundDealsPage";

		var approveType = mini.get("approveType").getValue();
		if(approveType == "mine"){//我发起的
			data['approveType']=1;
		}else if(approveType == "approve"){//待审批
			data['approveType']=2;
		}else{//已审批
			data['approveType']=3;
		}
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:url,
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});
	function checkBoxValuechanged(e){
		search(10,0);
		var approveType=this.getValue();
		if(approveType == "approve"){//待审批
			initButton();
		}else if(approveType == "finished"){//已审批
			mini.get("add_btn").setVisible(false);
			mini.get("edit_btn").setVisible(false);
			mini.get("delete_btn").setVisible(false);
			mini.get("approve_commit_btn").setVisible(false);
			mini.get("approve_mine_commit_btn").setVisible(false);
			mini.get("approve_log").setVisible(true);//审批日志
			mini.get("approve_log").setEnabled(true);//审批日志高亮
			mini.get("print_bk_btn").setVisible(true);//打印
			mini.get("print_bk_btn").setEnabled(true);//打印高亮
		}else{//我发起的
			mini.get("add_btn").setVisible(true);
			mini.get("add_btn").setEnabled(true);
			mini.get("edit_btn").setVisible(true);
			mini.get("edit_btn").setEnabled(true);
			mini.get("delete_btn").setVisible(true);
			mini.get("delete_btn").setEnabled(true);
			mini.get("approve_commit_btn").setVisible(false);
			mini.get("approve_mine_commit_btn").setVisible(true);//提交审批显示
			mini.get("approve_mine_commit_btn").setEnabled(true);//提交审批高亮
			mini.get("approve_log").setVisible(true);//审批日志
			mini.get("approve_log").setEnabled(true);//审批日志高亮
			mini.get("print_bk_btn").setVisible(true);//打印
			mini.get("print_bk_btn").setEnabled(true);//打印高亮
		}
	}
	function initButton(){
		mini.get("add_btn").setVisible(false);
		mini.get("edit_btn").setVisible(false);
		mini.get("delete_btn").setVisible(false);
		mini.get("approve_commit_btn").setVisible(true);//审批
		mini.get("approve_commit_btn").setEnabled(true);//审批高亮
		mini.get("approve_mine_commit_btn").setVisible(false);//提交审批
		mini.get("approve_log").setVisible(true);//审批日志
		mini.get("approve_log").setEnabled(true);//审批日志高亮
		mini.get("print_bk_btn").setVisible(true);//打印
		mini.get("print_bk_btn").setEnabled(true);//打印高亮
	}

	grid.on("select",function(e){
		var row=e.record;
		mini.get("approve_mine_commit_btn").setEnabled(false);
		mini.get("approve_commit_btn").setEnabled(false);
		mini.get("edit_btn").setEnabled(false);
		mini.get("delete_btn").setEnabled(false);
		if(row.approveStatus == "3"){//新建
			mini.get("approve_mine_commit_btn").setEnabled(true);
			mini.get("approve_commit_btn").setEnabled(false);
			mini.get("edit_btn").setEnabled(true);
			mini.get("delete_btn").setEnabled(true);
			mini.get("approve_log").setEnabled(false);
		}
		if( row.approveStatus == "6" || row.approveStatus == "5"){//审批通过：6   审批中:5
			mini.get("approve_mine_commit_btn").setEnabled(false);
			mini.get("approve_commit_btn").setEnabled(true);
			mini.get("edit_btn").setEnabled(false);
			mini.get("delete_btn").setEnabled(false);
		}
		if(row.approveStatus != null && row.approveStatus != "3") {
			mini.get("approve_log").setEnabled(true);
		}
	});
	/*****************************审批相关操作*******************************/
	//审批日志查看
	function appLog(selections){
		var flow_type = Approve.FlowType.VerifyApproveFlow;
		if(selections.length <= 0){
			mini.alert("请选择要操作的数据","系统提示");
			return;
		}
		if(selections.length > 1){
			mini.alert("系统不支持多笔操作","系统提示");
			return;
		}
		if(selections[0].tradeSource == "3"){
			mini.alert("初始化导入的业务没有审批日志","系统提示");
			return false;
		}
		Approve.approveLog(flow_type,selections[0].dealNo);
	};
	//提交正式审批、待审批
	function verify(selections){
		if(selections.length == 0){
			mini.alert("请选中一条记录！","消息提示");
			return false;
		}else if(selections.length > 1){
			mini.alert("暂不支持多笔提交","系统提示");
			return false;
		}
		if(selections[0]["approveStatus"] != "3" ){
			try {
				functionMain('801',"/Fund/FundRdpEdit.jsp","货币基金赎回","审批","approve");
			} catch (error) {
				
			}
		}else{
			//flow_type,serial_no,order_status,callBackFun,product_type,closedFun  rdpApproveFLow=11 赎回审批
			Approve.approveCommit(Approve.FlowType.rdpApproveFLow,selections[0]["dealNo"],Approve.OrderStatus.New,"approveFundManageService",prdNo,function(){
				search(grid.pageSize,grid.pageIndex);
			});
		}
	};
	//审批
	$("#approve_commit_btn").on("click",function(){
		mini.get("approve_commit_btn").setEnabled(false);
		var messageid = mini.loading("系统正在处理...", "请稍后");
		try {
			verify(grid.getSelecteds());
		} catch (error) {
			
		}
		mini.hideMessageBox(messageid);	
		mini.get("approve_commit_btn").setEnabled(true);
	});
	//提交审批
	$("#approve_mine_commit_btn").on("click",function(){
		mini.get("approve_mine_commit_btn").setEnabled(false);
		var messageid = mini.loading("系统正在处理...", "请稍后");
		try {
			verify(grid.getSelecteds());
		} catch (error) {
			
		}
		mini.hideMessageBox(messageid);	
		mini.get("approve_mine_commit_btn").setEnabled(true);
	});
	//审批日志
	$("#approve_log").on("click",function(){
		appLog(grid.getSelecteds());
	});
	$(document).ready(function(){
		search(10,0);
		initButton();
	})
	</script>
</body>
</html>