<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<style>
		.header {
	    height: 30px;
	    /* background: #95B8E7; */
	    color: #404040;
	    background: linear-gradient(to bottom,#EFF5FF 0,#E0ECFF 100%);
	    background-repeat: repeat-x;
	    line-height:30px; /*设置line-height与父级元素的height相等*/
	    text-align: center; /*设置文本水平居中*/
	    font-size:18px;
		}
	</style>
</head>
<body style="width:100%;height:100%;background:white">
<div id="layout1" class="mini-layout" style="width:100%;height:100%;">
	
	 <div class="header" region="north" height="30" showSplit="false" showHeader="false">
        货币基金（赎回确认）
    </div>
    <div title="center" region="center" bodyStyle="overflow:hidden;" >
	<div class="mini-fit" id="field_form">  
   <div  class="mini-panel" title="赎回确认信息*" style="width:100%;"  allowResize="true" collapseOnTitleClick="true">
  	 <div class="leftarea" style="margin-top:5px;">
			<input id="fundCode" name="fundCode" class="mini-buttonedit" labelField="true"  label="基金代码："  onbuttonclick="selectFundInfo" maxLength="30" style="width:95%;"  labelStyle="text-align:left;width:150px;margin-left:5px;" />
			<input id="vdate" name="vdate" class="mini-datepicker"  allowInput="false" labelField="true" label="赎回日期："  labelStyle="text-align:left;width:150px;margin-left:5px;" style="width:95%;"/>
			
	</div>
	<div class="rightarea" style="margin-top:5px;">
			<input id="fundName" name="fundName" class="mini-textbox" labelField="true"  label="基金名称："  onbuttonclick="selectFundInfo" maxLength="30" style="width:95%;"  labelStyle="text-align:left;width:150px;margin-left:5px;" />
			<input id="tdate" name="tdate" class="mini-datepicker"  allowInput="false" labelField="true" label="回款日期："  labelStyle="text-align:left;width:150px;margin-left:5px;" style="width:95%;"/>
	</div>
	<div class="leftarea" style="margin-top:5px;">
		<input id="cno" name="cno" class="mini-buttonedit" labelField="true"  label="交易对手：" onbuttonclick="onButtonEdit" maxLength="30" style="width:95%;"  labelStyle="text-align:left;width:150px;margin-left:5px;" required="true"  enabled="false"/>
	    <input id="ccy" name="ccy" class="mini-combobox" labelField="true"  label="基金币种：" data="CommonUtil.serverData.dictionary.Currency" style="width:95%;"  labelStyle="text-align:left;width:150px;margin-left:5px;" />
	</div>
  	<div class="rightarea" style="margin-top:5px;">
  	    <input id="cname" name="cname" class="mini-textbox" labelField="true"  label="交易对手名称：" maxLength="255" labelStyle="text-align:left;width:150px;margin-left:5px;" style="width:95%;"   required="true"  enabled="false"/>
		<input id="invType" name="invType" data="CommonUtil.serverData.dictionary.AccThreeSubject" class="mini-combobox"  labelField="true"  label="会计类型："  labelStyle="text-align:left;width:150px;margin-left:5px;" style="width:95%;"  enabled="false" required="true" />
  	</div>
	<div class="centerarea" style="margin-top:5px;">
		<span style="float:left;">
		
		</span>
		
		<div class="mini-toolbar" style="padding:0px;border-bottom:0;" id="toolbar">
	        <table style="width:100%;">
	            <tr>
	                <td style="width:100%;">                
	                    <a id="add_btn" class="mini-button" style="display: none"  >新增</a>
						<a id="delete_btn" class="mini-button" style="display: none"  >删除</a>
	                </td>
	            </tr>
	        </table>
	    </div>
	</div>
	<div class="centerarea" style="height:220px;">
		<div id="tradeManage" class="mini-fit" >      
			<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="dealNo" 
			allowAlternating="true" allowResize="true"  border="true" onrowDblclick="onrowDblclick" showHeader="true" title="赎回明细列表" showPager="false"
			multiSelect="false" sortMode="client">
				<div property="columns">
					<div type="indexcolumn" headerAlign="center" width="50">序号</div>
					<div field="" align="center" headeralign="center" width="390">
		                申购信息
		                <div property="columns" align="center">
		                        <div field="dealNo" width="90" headerAlign="center" align="center" allowSort="true">（申购）交易单号</div>
		                        <div field="shareAmt" numberFormat="#,0.00" align="right" width="110" headerAlign="center" allowSort="true">申购份额（数量）</div>
		                        <div field="reShareAmt" numberFormat="#,0.00" align="right" width="110" headerAlign="center" allowSort="true">尚未赎回份额（数量）</div>
		                        <div field="ftpprice" numberFormat="#,0.00000" align="right" width="80" headerAlign="center" allowSort="true">FTP成本价格</div>
		                    </div>
		            </div>
					<div field="" align="center" headeralign="center" width="450">
		                赎回信息
		                <div property="columns" align="center">
		                		<div field="qty" numberFormat="#,0.00" align="right" width="150" headerAlign="center" allowSort="true">本次赎回份额（数量）</div>
	                        <div field="amt" numberFormat="#,0.00" align="right" width="150" headerAlign="center" allowSort="true">本次赎回本金（元）</div>
							<div field="intamt" numberFormat="#,0.00" align="right" width="150" headerAlign="center" allowSort="true">本次赎回红利（元）</div>
	                    </div>
		            </div>  
				</div>
			</div>  
		</div>
	</div>
	<div class="leftarea" style="margin-top:5px;">
		<input id="shareAmt" name="shareAmt" class="mini-spinner" labelField="true"  label="累计确认赎回份额（份）：" enabled="false"  required="true"   changeOnMousewheel='false' maxValue="999999999999999" format="n2" labelField="true"  labelStyle="text-align:left;width:150px;margin-left:5px;"  style="width:95%;" />
		<input id="amt" name="amt" class="mini-spinner" labelField="true"  label="累计赎回金额（元）：" required="true"  enabled="false" changeOnMousewheel='false' maxValue="999999999999999" format="n2" labelField="true"  labelStyle="text-align:left;width:150px;margin-left:5px;" style="width:95%;" />
		<input id="intamt" name="intamt" class="mini-spinner" labelField="true"  label="累计赎回红利（元）：" required="true"  enabled="false" changeOnMousewheel='false' maxValue="999999999999999" format="n2" labelField="true"  labelStyle="text-align:left;width:150px;margin-left:5px;" style="width:95%;" />
	</div>
	<div class="rightarea" style="margin-top:5px;">
		<input id="reShareAmt" name="reShareAmt" class="mini-spinner" labelField="true"  label="审批可赎回份额（份）：" enabled="false"  required="true"   changeOnMousewheel='false' maxValue="999999999999999" format="n2" labelField="true"  labelStyle="text-align:left;width:150px;margin-left:5px;"  style="width:95%;" />
		<input id="isConfirm" name="isConfirm" class="mini-combobox" labelField="true"  label="确认审批份额分配结束：" data="CommonUtil.serverData.dictionary.YesNo" maxLength="20" labelStyle="text-align:left;width:150px;margin-left:5px;" style="width:95%;"   />
		<div style="width:95%;color:red"><label>是：不能再对所选赎回审批单进行后续确认操作（1对多）  否：后续还可以针对所选审批单进行可用赎回份额确认！</label></div>
	</div>
  </div>
  
    <div  class="mini-panel" title="审批信息*" style="width:100%;"  allowResize="true" collapseOnTitleClick="true">
		<div class="leftarea" style="margin-top:5px;">
			<input id="tradeNo" name="tradeNo" class="mini-textbox" labelField="true" style="width:95%;" label="审批编号：" labelStyle="text-align:left;width:150px;margin-left:5px;"  vtype="maxLength:32" required="true"   emptyText="由系统默认自动生成"/>
		</div>	
		<div class="rightarea" style="margin-top:5px;">
			<input id="dealNo" name="dealNo" class="mini-textbox" labelField="true" style="width:95%;" label="交易编号：" labelStyle="text-align:left;width:150px;margin-left:5px;"  vtype="maxLength:32" required="true" />
		</div>
		<div class="leftarea" style="margin-top:5px;">
			<input id="sponsor" name="sponsor" class="mini-textbox" labelField="true"  label="审批发起人：" labelStyle="text-align:left;width:150px;margin-left:5px;" style="width:95%;"   />
			<input id="adate" name="adate" class="mini-datepicker"  allowInput="false" labelField="true" label="审批日期："  labelStyle="text-align:left;width:150px;margin-left:5px;"  style="width:95%;"  />
		</div>
		<div class="rightarea" style="margin-top:5px;">
			<input id="sponinst" name="sponinst" class="mini-combobox" labelField="true"  label="审批发起机构：" labelStyle="text-align:left;width:150px;margin-left:5px;" style="width:95%;"   />
			
		</div>
	</div>
	<%@ include file="../Common/Flow/MiniApproveOpCommon.jsp"%>
	</div>
</div>
	<div title="south" region="south" showSplit="false" showHeader="false" height="38px;"  style="line-height:38px;text-align: center;">
       <div id="functionIds">
			<a class="mini-button" style="display: none"  style="width:120px;" id="save_btn"  onclick="save">保存交易</a>
			<a class="mini-button" style="display: none"  style="width:120px;" id="close_btn"  onclick="close">关闭界面</a>
		</div>
    </div>
	
</div>
	<script type="text/javascript">
	mini.parse();
	//获取当前tab
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var row=params.selectedData;
	var action =params.action;
	var form =new mini.Form("#field_form");
	var grid=mini.get("datagrid");
	var prdNo = '801';//货币基金
	var dealType = "2";//确认审批
	var tradeData={};
	if(row!=null){
		tradeData.selectData=row;
		tradeData.operType=action;
		tradeData.serial_no=row.dealNo;
		tradeData.task_id=row.taskId;
	}

	grid.on("drawcell",function(e){
		var record = e.record,
        column = e.column,
        field = e.field,
        value = e.value;
		if(field=="qty"){
			e.value=0;
		}
	})
	//保存审批单
	function save(){
		//表单验证！！！
		form.validate();
		if (form.isValid() == false) {
			return;
		}
		
		var data=form.getData(true);
		var rdpDetailsConfList=grid.getData();
		data['productType']="FtRdpConf";
		data['rdpDetailsConfList']=rdpDetailsConfList;
		data['ccy'] = mini.get("ccy").getValue();
		data['prdNo']=prdNo;
		data['dealType']=dealType;
		data['trdtype']=ApproveTrdType.FundTrdType.Rdp;//交易操作类型
		var rdpDetailsConfList=grid.getData();
		data['rdpDetailsConfList']=rdpDetailsConfList;
		var params=mini.encode(data);
		CommonUtil.ajax({
			url:"/ApproveFundManageController/saveApprove",
			data:params,
			callback:function(data){
				mini.alert("货基赎回确认-保存成功！请确认信息后再提交审批！",'提示信息',function(){
					top["win"].closeMenuTab();
					})
			}
		});
		
	}

	function init(){
		form.setData(row);
		form.setEnabled(false);
		mini.get("fundName").setValue(row.fundName);
		mini.get("fundCode").setValue(row.fundCode);
		mini.get("fundCode").setText(row.fundCode);
		mini.get("cno").setValue(row.cno);
		mini.get("cno").setText(row.cno);
		mini.get("cname").setValue(row.cname);
		mini.get("isConfirm").setEnabled(true);
		mini.get("tdate").setEnabled(true);
		mini.get("vdate").setEnabled(true);
		mini.get("invType").setValue(row.invType)
		mini.get("shareAmt").setValue(row.shareAmt)
		mini.get("reShareAmt").setValue(row.reShareAmt);
	}
	function initGrid(){
		var url="/FundController/searchRdpConfDetails";
		var data={};
		data['tradeNo']=mini.get("tradeNo").getValue();
		var params=mini.encode(data);
		CommonUtil.ajax({
			url:url,
			data:params,
			callback : function(data) {
				grid.setData(data.obj);
			}
		});
	}
	$("#add_btn").on("click",function(){
		selectAfpInfo();
	});
	$("#delete_btn").on("click",function(){
		mini.confirm("您确认要删除选中记录?","系统警告",function(value){   
			if (value=='ok'){   
			var data = grid.getSelected();
			grid.removeRow ( data, true );
			mini.get("amt").setValue(mini.get("amt").getValue()-data.amt);
            mini.get("shareAmt").setValue(mini.get("shareAmt").getValue()-data.qty);
            mini.get("intamt").setValue(mini.get("intamt").getValue()-data.intamt);
			}
		}
		);
	});
	function selectAfpInfo(){
		mini.open({
            url: CommonUtil.baseWebPath() + "./Fund/FundAfpConfMini.jsp",
            title: "选择需要赎回的明细",
            width: 924,
			height: 500,
			onload: function () {
				var iframe = this.getIFrameEl();
				var data=form.getData(true);
				iframe.contentWindow.SetData(data);
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    data['dealNo'] = data.tradeNo;
                    if (data) {
                         var value=mini.get("shareAmt").getValue()+data.qty;
                 		var reAmt=mini.get("reShareAmt").getValue();//剩余可确认的金额
                 		if(value>reAmt){
                 			mini.alert("超出可赎回审批余额，请重新输入","提示");
                 			return ;
                 		}else{
                 			//判定是不是针对同一个审批申购流水进行操作的；如果是，那么合二为一
                 			var rows = grid.getData();
                 			var flag = true;
                 			for(var i=0,l=rows.length;i<l;i++){
                 			   var row = rows[i]
                 			   if(row.dealNo==data.dealNo){
                 				  row.qty = row.qty+data.qty;
                 				  row.amt = row.amt+data.amt;
                 				  row.intamt = row.intamt+data.intamt;
                 				  if(row.qty>data.reShareAmt){
                 					 mini.alert("超出本条赎回持仓的可用份额","提示");
                          			 return ;
                 				  }else{
                 				  	grid.updateRow(row,row);
                 				  	mini.get("amt").setValue(mini.get("amt").getValue()+data.amt);
                                     mini.get("shareAmt").setValue(mini.get("shareAmt").getValue()+data.qty);
                                     mini.get("intamt").setValue(mini.get("intamt").getValue()+data.intamt);
                 				  }
                 				  flag = false;
                 			   }
                 			}
                 			if(flag){
		                         grid.addRow(data, 0);
		                         grid.deselectAll();
		                         grid.select(data);
		                         mini.get("amt").setValue(mini.get("amt").getValue()+data.amt);
                                 mini.get("shareAmt").setValue(mini.get("shareAmt").getValue()+data.qty);
                                 mini.get("intamt").setValue(mini.get("intamt").getValue()+data.intamt);
                 			}
                 		}
                    }
                }
            }
        });
	}
	
	function onrowDblclick(e){/* 
		mini.open({
			url : CommonUtil.baseWebPath() +"/Fund/FundRdpDetailsMini.jsp",
			title : "赎回确认",
			width : 600,
			height : 300,
			onload: function () {
				var iframe = this.getIFrameEl();
				var data = e.record;
				iframe.contentWindow.SetData(data);
            },
			ondestroy: function(action) {	
				if (action == "ok"){
					var iframe = this.getIFrameEl();
					var data = iframe.contentWindow.GetData();
					// data = mini.clone(data); //必须
					var row=data;
					grid.updateRow(e.record,row);
				}
			}
		}); */
	}
	$(document).ready(function(){
		init();
		initGrid();
		mini.get("sponsor").setValue("<%=__sessionUser.getUserId()%>");
		mini.get("sponinst").setValue("<%=__sessionUser.getInstId() %>");
		mini.get("adate").setValue("<%=__bizDate%>");
		mini.get("vdate").setValue("<%=__bizDate%>");
		mini.get("tdate").setValue("<%=__bizDate%>");
		if($.inArray(action,["approve","detail"])>-1){
			mini.get("save_btn").setVisible(false);
			form.setEnabled(false);
			var form11=new mini.Form("#approve_operate_form");
			form11.setEnabled(true);
			mini.get("toolbar").setVisible(false);
		}else if($.inArray(action,["add"])>-1){
			mini.get("shareAmt").setValue(0.00);
			mini.get("reShareAmt").setValue(row.reShareAmt);
		}
	})
	function close(){
		top["win"].closeMenuTab();
	}
	</script>
	<script type="text/javascript" src="<%=basePath%>/standard/Common/Flow/MiniApproveOpCommon.js"></script>
</body>
</html>