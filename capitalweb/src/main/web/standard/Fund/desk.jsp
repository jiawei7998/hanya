<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css">
    body{
        margin:0;padding:0;border:0;width:100%;height:100%;overflow:hidden;
    }    

    .header {
    height: 30px;
    /* background: #95B8E7; */
    color: #404040;
    /* background: linear-gradient(to bottom,#EFF5FF 0,#E0ECFF 100%); */
    background-repeat: repeat-x;
	}
	.Reports
    {
        background:url(../menu/picasa.png) no-repeat;width:32px;height:32px;
    }
    </style>    
</head>
<body >   

<div id="layout1" class="mini-layout" style="width:100%;height:100%;">
    <div class="header" region="north" height="30" showSplit="false" showHeader="false">
        <h3 style="margin:0;width:200px;padding:5px 5px 25px 15px;box-sizing: border-box;height: 30px;cursor:default;">我的工作台（常用）</h3>
    </div>
    <div title="south" region="south" showSplit="false" showHeader="false" height="30" >
        <div style="line-height:28px;text-align:center;cursor:default">Copyright © 江苏银行股份有限公司 </div>
    </div>
    <div showHeader="false" region="west" width="150" maxWidth="200" minWidth="100" >
        <!--OutlookMenu-->
        <div id="leftTree" class="mini-outlookmenu" onitemselect="onItemSelect"
            idField="id" parentField="pid" textField="text" borderStyle="border:0" 
        >
        </div>

    </div>
    <div title="center" region="center" bodyStyle="overflow:hidden;">
        <iframe id="mainframe" frameborder="0" name="main" style="width:100%;height:100%;" border="0"></iframe>
    </div>
</div>
    
    <script type="text/javascript">
        mini.parse();
        var iframe = document.getElementById("mainframe");
        function onItemSelect(e) {
            var item = e.item;
            iframe.src = item.url;
        }
        var data = [
	        { id: "myDeskBook", text: "我的工作台", iconCls: "sl-deskCust-black" },
	        { id: "myApproveDeskBook", pid: "myDeskBook", iconCls:"icon-node",text: "我的待审批",  url: "./portal/ApproveListManager.jsp" },
	        { id: "myNeedToDoDeskBook", pid: "myDeskBook", iconCls:"icon-node",text: "我的待处理",  url: "" },
	        { id: "myMessgeDeskBook", pid: "myDeskBook", iconCls:"icon-node",text: "我的信息",  url: "" },

	        { id: "right", text: "常用菜单",iconCls:"icon-find" },
	        { id: "addRight", pid: "right", text: "基金持仓", iconCls: "Reports", url: "", iconPosition: "top" },
	        { id: "editRight", pid: "right", text: "基金信息", iconCls: "Reports", url: "./FundBaseManager.jsp", iconPosition: "top" }
        ];
        var tree = mini.get("leftTree");
        tree.loadList(data, "id", "pid");
    </script>

</body>
</html>