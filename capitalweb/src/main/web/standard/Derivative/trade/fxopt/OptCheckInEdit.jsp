<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../../global.jsp"%>
<html>
  <head>
  <title>期权-登记维护</title>
  <script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
  </head>
  
  <body style="width:100%;height:100%;background:white">
		<div class="mini-splitter" style="width:100%;height:100%;">
			<div size="90%" showCollapseButton="true">
				<div class="mini-fit"  style="background:white" id="fwdinfo">
						
							<table  width="100%" class="mini-table">
								<tr>
                                    <td><input class="mini-textbox"  id="coptNo"  name="coptNo" style="width:400px" labelField="true" label="期权业务编号"  labelStyle="width:135px" emptyText="系统自动生成" enabled="false"  /></td>
                                    <td><input class="mini-buttonedit"  onbuttonclick="onInsQuery" id="brchNo" name="brchNo"  style="width:400px" labelField="true" label="机构号"  labelStyle="width:135px" emptyText=""   /></td>
								</tr>
								<tr>
									<td><input class="mini-buttonedit" onbuttonclick="onCustQuery"  id="custNo"  name="custNo" style="width:400px"  labelField="true" label="客户代码"  required="true"  labelStyle="width:135px" emptyText="" /></td>
                                    <td><input class="mini-textbox" id="custNm"  name="custNm" labelField="true" label="客户名称"  labelStyle="width:135px" style="width:400px"  emptyText="同步反显" enabled="false"></td>
                                </tr>
								<tr>
									<td><input class="mini-textbox" id="custAd"  name="custAd" labelField="true" label="客户地址" style="width:400px" emptyText="同步反显"  labelStyle="width:135px" enabled="false"></td>
                                    <td><input class="mini-datepicker" id="dlDate"  name="dlDate" format="yyyy-MM-dd" style="width:400px" labelField="true" label="交易日期"  required="true"  labelStyle="width:135px" emptyText="" showOkButton="true" showClearButton="false"/></td>
                                </tr>
                                <tr>
                                    <td><input class="mini-combobox" id="jsyqTp"  name="jsyqTp" labelField="true" label="交易类型"  required="true"  labelStyle="width:135px" style="width:400px" emptyText="" data = "CommonUtil.serverData.dictionary.UPDOWN" ></td>
                                    <td><input class="mini-buttonedit" onbuttonclick="onInquiryPriceQuery" id="fdqvNo"  name="fdqvNo" style="width:400px" labelField="true" label="询价业务编号"  required="true"  labelStyle="width:135px"  emptyText=""/></td>
                                </tr>
                                <tr>
									<td><input class="mini-combobox" id="upDown"  name="upDown" style="width:400px" labelField="true" label="看涨/看跌"  required="true"  labelStyle="width:135px" emptyText="" data = "CommonUtil.serverData.dictionary.UPDOWN"/></td>
									<td><input class="mini-combobox" id="yucyNo"  name="yucyNo" style="width:400px" labelField="true" label="看涨币种"   required="true"  labelStyle="width:135px" emptyText="" data = "CommonUtil.serverData.dictionary.Currency" /></td>
                                </tr>
                                <tr>
									<td><input class="mini-combobox" id="mdcyNo"  name="mdcyNo" style="width:400px" labelField="true" label="看跌币种"   required="true"  labelStyle="width:135px" emptyText="" data = "CommonUtil.serverData.dictionary.Currency"/></td>
									<td><input class="mini-buttonedit" onbuttonclick="onRmbAccoQuery" id="mdacNo"  name="mdacNo" style="width:400px" labelField="true" label="人民币账号"   required="true"  labelStyle="width:135px" emptyText="" /></td>
                                </tr>
                                <tr>
                                    <td><input class="mini-textbox" id="trAmts"  name="trAmts" style="width:400px" labelField="true" label="交易金额(外币)"  onvalidation="onMoneyValidation" required="true"  labelStyle="width:135px" emptyText="" /></td>
                                    <td><input class="mini-textbox" id="contRt"  name="contRt" style="width:400px" labelField="true" label="执行价格" onvalidation="onPriceValidation" required="true"  labelStyle="width:135px"  emptyText="" /></td>
                                </tr>
                                <tr>
                                    <td><input class="mini-textbox" id="jqRate"  name="jqRate" style="width:400px" labelField="true" label="即期汇率"  onvalidation="onPriceValidation" required="true"  labelStyle="width:135px" emptyText="" /></td>
                                    <td><input class="mini-textbox" id="custFr"  name="custFr" style="width:400px" labelField="true" label="对客期权费率" onvalidation="onPriceValidation" required="true"   labelStyle="width:135px"  emptyText="" /></td>
                                </tr>
                                <tr>
                                    <td><input class="mini-textbox" id="custPr"  name="custPr" style="width:400px" labelField="true" label="对客期权费(人民币)"   onvalidation="onPriceValidation" required="true"  labelStyle="width:135px" emptyText="" /></td>
                                    <td><input class="mini-textbox" id="bankFr"  name="bankFr" style="width:400px" labelField="true" label="总行平盘期权费率"  onvalidation="onPriceValidation" required="true"   labelStyle="width:135px" emptyText="" /></td>
                                </tr>
                                <tr>
                                    <td><input class="mini-textbox" id="bankPr"  name="bankPr" style="width:400px" labelField="true" label="总行平盘期权费(人民币)"  onvalidation="onPriceValidation" required="true"  labelStyle="width:135px" emptyText="" /></td>
                                    <td><input class="mini-datepicker" id="exerDt"  name="exerDt" format="yyyy-MM-dd" style="width:400px" labelField="true" label="行权日期"   required="true"  labelStyle="width:135px" emptyText="" showOkButton="true" showClearButton="false"/></td>
                                </tr>
                                <tr>
                                    <td><input class="mini-textarea" id="tranMo"  name="tranMo" style="width:400px" labelField="true" label="备注"  vtype="maxLength:128" labelStyle="width:135px" emptyText="" /></td>  
                                </tr>
	
							</table>
						
				</div>
			</div>
			<div id="functionIds" showCollapseButton="true">
				<div style="margin-bottom:10px; margin-top:50px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_valid"   onclick="valid()">校验要素</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_save"          onclick="save">保存交易</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_verify"    enabled="false" onclick="verify">提交复核</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_refuse"        enabled="false" onclick="refuse">拒绝交易</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_checkTrade"        enabled="false"   onclick="checkTrade">复核交易</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_close"              onclick="close">关闭界面</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_tradeFee"     enabled="false" onclick="tradeFee">交易费用</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_tradeProof"      enabled="false" onclick="tradeProof">交易凭证</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_agencyFeeBtn"     enabled="false"    onclick="">会计传票</a></div>
			</div>      
		</div>


   
	
<script type="text/javascript">
     
	mini.parse();

	var url = window.location.search;
    var action = CommonUtil.getParam(url,"action");
	var dataTp = CommonUtil.getParam(url,"dataTp");

	$(document).ready(function(){
        init();
    });

	function init(){
		if(action == "new"){
			
		}else if(action == "edit" || action == "detail"){
			var form1 = new mini.Form("fwdinfo");
			var data=top["optCheckInList"].getData();
			form1.setData(data);
			//buttonedit控件需要单独设置值
			//1.机构号
			mini.get("brchNo").setValue(data.brchNo);
			mini.get("brchNo").setText(data.brchNo);
			//2.客户代码
			mini.get("custNo").setValue(data.custNo);
			mini.get("custNo").setText(data.custNo);
			//3.询价业务编号
			mini.get("fdqvNo").setValue(data.fdqvNo);
			mini.get("fdqvNo").setText(data.fdqvNo);
			//4.人民币账号
			mini.get("mdacNo").setValue(data.mdacNo);
			mini.get("mdacNo").setText(data.mdacNo);
			
			if(action == "detail"){
				form1.setEnabled(false);
				if(dataTp == "verify"){//待复核
					mini.get("btn_valid").setEnabled(false);//检验要素
					mini.get("btn_save").setEnabled(false);//保存交易
					mini.get("btn_verify").setEnabled(false);//提交复核
					mini.get("btn_refuse").setEnabled(true);//拒绝交易
					mini.get("btn_checkTrade").setEnabled(true);//复核交易
					mini.get("btn_close").setEnabled(false);//关闭界面
					mini.get("btn_tradeFee").setEnabled(false);//交易费用
					mini.get("btn_tradeProof").setEnabled(false);//交易凭证
					mini.get("btn_agencyFeeBtn").setEnabled(false);//会计传票
				}else if(dataTp == "effi"){//生效
					mini.get("btn_valid").setEnabled(false);//检验要素
					mini.get("btn_save").setEnabled(false);//保存交易
					mini.get("btn_verify").setEnabled(false);//提交复核
					mini.get("btn_refuse").setEnabled(false);//拒绝交易
					mini.get("btn_checkTrade").setEnabled(false);//复核交易
					mini.get("btn_close").setEnabled(false);//关闭界面
					mini.get("btn_tradeFee").setEnabled(true);//交易费用
					mini.get("btn_tradeProof").setEnabled(true);//交易凭证
					mini.get("btn_agencyFeeBtn").setEnabled(true);//会计传票
				}
			}else if(action == "edit"){
				if(dataTp == "modify"){//待修改
					mini.get("btn_valid").setEnabled(false);//检验要素
					mini.get("btn_save").setEnabled(false);//保存交易
					mini.get("btn_verify").setEnabled(true);//提交复核
					mini.get("btn_refuse").setEnabled(false);//拒绝交易
					mini.get("btn_checkTrade").setEnabled(false);//复核交易
					mini.get("btn_close").setEnabled(false);//关闭界面
					mini.get("btn_tradeFee").setEnabled(false);//交易费用
					mini.get("btn_tradeProof").setEnabled(false);//交易凭证
					mini.get("btn_agencyFeeBtn").setEnabled(false);//会计传票
				}
			}

		}
	}

	

	//机构的查询
	function onInsQuery(e) {
		var btnEdit = this;
		mini.open({
			url : "/fund-capital/standard/InterBank/mini_system/MiniInstitutionSelectManages.jsp",
			title : "机构选择",
			width : 700,
			height : 600,
			ondestroy : function(action) {	
				if (action == "ok") 
				{
					var iframe = this.getIFrameEl();
					var data = iframe.contentWindow.GetData();
					data = mini.clone(data); //必须
					if (data) {
						btnEdit.setValue(data.instId);
						btnEdit.setText(data.instId);
						btnEdit.focus();
					}

					
				}

			}
		});
	}

	//客户的查询
	function onCustQuery(e) {
		var btnEdit = this;
		mini.open({
			url : CommonUtil.baseWebPath() +"/base/MiniCPManageYQ.jsp",
			title : "客户选择",
			width : 700,
			height : 600,
			ondestroy : function(action) {	
				if (action == "ok") 
				{
					var iframe = this.getIFrameEl();
					var data = iframe.contentWindow.GetData();
					data = mini.clone(data); //必须
					if (data) {
						btnEdit.setValue(data.party_id);
						btnEdit.setText(data.party_id);
						
						mini.get("custNm").setValue(data.party_name);
						//有联系人
						if(data.constact_list.length>0){
							for(var i=0;i<data.constact_list.length;i++){
								if(data.constact_list[i].address){//有联系人，还有地址
									mini.get("custAd").setValue(data.constact_list[i].address);
									return;
								}else{//有联系人，没有地址
									mini.get("custAd").setValue("");
								}
							}
							
						}else{//没有联系人
							mini.get("custAd").setValue("");
						}

						
						
						btnEdit.focus();
						//btnEdit.doValueChanged(alert("1"));
						//mini.get("custNo").doValueChanged(custNoChange());
					}

					
				}

			}
		});
	}

	//客户代码值改变时发生
	var btnEdit1 = mini.get("custNo");
    btnEdit1.on("valuechanged", function () {
       //客户代码值改变，原账号清空
		mini.get("yuacNo").setValue("");
		mini.get("yuacNo").setText("");
		//客户代码值改变，目的账号清空
		mini.get("mdacNo").setValue("");
		mini.get("mdacNo").setText(""); 
    })

	/* function custNoChange(){
		////debugger
		//客户代码值改变，原账号清空
		mini.get("yuacNo").setValue("");
		mini.get("yuacNo").setText("");
		//客户代码值改变，目的账号清空
		mini.get("mdacNo").setValue("");
		mini.get("mdacNo").setText("");
	} */


	//原币种值改变时发生
	function yucyNoChange(){
		//原币种值改变，原账号清空
		mini.get("yuacNo").setValue("");
		mini.get("yuacNo").setText("");
		
	}

	//目的币种值改变时发生
	function mdcyNoChange(){
		//目的币种值改变，目的账号清空
		mini.get("mdacNo").setValue("");
		mini.get("mdacNo").setText("");
	} 

	//账号的查询
	function onAccoQuery(e) {
		
		var custNo = mini.get("custNo").getValue();
		if(!custNo){
			mini.alert("请选择客户代码!");
			return;
		}
		//原账号
		if(e.sender.id == "yuacNo"){
			//获取原币种
			var yucyNo = mini.get("yucyNo").getValue();
			if(!yucyNo){
				mini.alert("请选择原币种!");
				return;
			}
		}else if(e.sender.id == "mdacNo"){//目的账号
			//获取目的币种
			var mdcyNo = mini.get("mdcyNo").getValue();
			if(!mdcyNo){
				mini.alert("请选择目的币种!");
				return;
			}
		}
		
		
		var btnEdit = this;
		mini.open({
			url : CommonUtil.baseWebPath() +"/base/MiniAccountManage.jsp",
			title : "账号选择",
			width : 700,
			height : 600,
			
			onload: function () {//弹出页面加载完成
				var iframe = this.getIFrameEl(); 
				var data = {};
				//原账号
				if(e.sender.id == "yuacNo"){
					var custNo = mini.get("custNo").getValue();//客户代码
					var yucyNo = mini.get("yucyNo").getValue();//原币种
					data = {"custNo":custNo,"yucyNo":yucyNo,"type":"orig"};	
				}else if(e.sender.id == "mdacNo"){//目的账号
					var custNo = mini.get("custNo").getValue();//客户代码
					var mdcyNo = mini.get("mdcyNo").getValue();//目的币种
					data = {"custNo":custNo,"mdcyNo":mdcyNo,"type":"dest"};	
				}
				       
				//调用弹出页面方法进行初始化
				iframe.contentWindow.SetData(data); 
								
			},
			ondestroy : function(action) {	
				if (action == "ok") 
				{
					var iframe = this.getIFrameEl();
					var data = iframe.contentWindow.GetData();
					data = mini.clone(data); //必须
					if (data) {
						btnEdit.setValue(data.accoNo);
						btnEdit.setText(data.accoNo);
						btnEdit.focus();
					}

					
				}

			}
		});
	}
	//询价业务编号的查询=>需要先选择 客户代码、币种（外币）、交割总金额（外币）、结汇/售汇
	function onInquiryPriceQuery(e){
		//客户代码
		var custNo = mini.get("custNo").getValue();
		//交割总金额（外币）
		var wBamts = mini.get("wBamts").getValue();
		//结售汇类型
		var jsyqTp = mini.get("jsyqTp").getValue();
		//原币种
		var yucyNo = mini.get("yucyNo").getValue();
		//目的币种
		var mdcyNo = mini.get("mdcyNo").getValue();

		if((custNo==null||custNo=="")  || (wBamts==null||wBamts=="")  || (jsyqTp==null||jsyqTp=="") || (yucyNo==null||yucyNo=="") || (mdcyNo==null||mdcyNo=="") ){
			mini.alert("客户代码、交割总金额、结售汇类型、原币种、目的币种都必须正确填入(选择)!");
			return;
		}

		var btnEdit = this;
		mini.open({
			url : CommonUtil.baseWebPath() +"/inquiry/fxfwd/FwdInquiryListMini.jsp",
			title : "询价业务编号选择",
			width : 700,
			height : 600,
			
			onload: function () {//弹出页面加载完成
				var iframe = this.getIFrameEl(); 
				var data = {};
				data = {"custNo":custNo,"yucyNo":yucyNo,"mdcyNo":mdcyNo,"jsyqTp":jsyqTp,"wBamts":wBamts};	
				//调用弹出页面方法进行初始化
				iframe.contentWindow.SetData(data); 
			},
			ondestroy : function(action) {	
				if (action == "ok") 
				{
					var iframe = this.getIFrameEl();
					var data = iframe.contentWindow.GetData();
					data = mini.clone(data); //必须
					if (data) {
						btnEdit.setValue(data.fDQVNO);
						btnEdit.setText(data.fDQVNO);
						mini.get("myRate").setValue(data.mYRATE);
						mini.get("exRate").setValue(data.eXRATE);
						btnEdit.focus();
					}

					
				}

			}
		});
		
		
	}

    //交易金额（外币）的校验
	function onMoneyValidation(e) {
		if (e.isValid) {
			if (isMoney(e.value) == false) {
				e.errorText = "金额整数位最多17位，小数位最多2位!";
				e.isValid = false;
			}
		}
    }
    /* 金额number(19,2)的验证 */
	function isMoney(v) {
		var re = new RegExp("^[0-9]{1,17}([.]{1}[0-9]{1,2}){0,1}$");
		if (re.test(v)){
			return true;
		}else{
			return false;
		} 
		
	}

    //执行价格、即期汇率、对客期权费率的校验
	function onPriceValidation(e) {
		if (e.isValid) {
			if (isPrice(e.value) == false) {
				e.errorText = "整数位最多12位，小数位最多7位!";
				e.isValid = false;
			}
		}
    }
    /* number(19,7)的验证 */
	function isPrice(v) {
		var re = new RegExp("^[0-9]{1,12}([.]{1}[0-9]{1,7}){0,1}$");
		if (re.test(v)){
			return true;
		}else{
			return false;
		} 
		
	}


//*****************************************事件*************************************************************

	//检验要素
	function valid(){
		var form = new mini.Form("fwdinfo");
		form.validate();
	}

    //保存
    function save(){
     	var form = new mini.Form("#fwdinfo");
		//验证表单
		form.validate();
		if(form.isValid() == false){
			mini.alert("表单填写错误,请确认!","提示信息")
			return;
		}
    		   
		var data = form.getData(true); //获取表单多个控件的数据
		if(data.jsyqTp == "1"){
            data.jsyqTp = "结汇";
        }else if(data.jsyqTp == "2"){
            data.jsyqTp = "售汇";
        }
		data.userSt='S'
		data.statCd='A'
		data.newStatCd='A'
		var param = mini.encode(data); //序列化成JSON
		
		CommonUtil.ajax({
					url:'/OptionController/save',
					data:param ,
					callback:function(data){
						mini.get("coptNo").setValue(data.desc);
            			mini.alert("保存成功!");
            			mini.get("btn_verify").setEnabled(true);
						
					}
		});
    		
	}

	//提交
	function verify(){
       	var form = new mini.Form("#fwdinfo");
		//验证表单
		form.validate();
		if(form.isValid() == false){
			mini.alert("表单填写错误,请确认!","提示信息")
			return;
		}
			
		var data = form.getData(true); //获取表单多个控件的数据
		if(data.jsyqTp == "1"){
            data.jsyqTp = "结汇";
        }else if(data.jsyqTp == "2"){
            data.jsyqTp = "售汇";
        }
		data.userSt='Y'
		data.wbamBl=''
		data.newStatCd='A'
		var param = mini.encode(data); //序列化成JSON
		
		CommonUtil.ajax({
			url:'/OptionController/commit',
			data:param,
			callback:function(data){
				mini.alert("提交成功!","系统提示",function(value){
                    //关闭并刷新
					setTimeout(function(){ top["win"].closeMenuTab() },100);
                });
				
			}
		});
			
	}

	//复核通过
    function checkTrade(){
       var form = new mini.Form("#fwdinfo");
		//验证表单
		form.validate();
		if(form.isValid() == false){
		mini.alert("表单填写错误,请确认!","提示信息")
		return;
		}
			
		var data = form.getData(true); //获取表单多个控件的数据
		if(data.jsyqTp == "1"){
            data.jsyqTp = "结汇";
        }else if(data.jsyqTp == "2"){
            data.jsyqTp = "售汇";
        }
		data.userSt='Y'
		data.chckSt='Y'
        data.statCd='A'
        data.wbamBl=''
        data.newStatCd='A'
		var param = mini.encode(data); //序列化成JSON
		
		CommonUtil.ajax({
			url:'/OptionController/checkTradeTr',
			data:param ,
			callback:function(data){
				mini.alert("复核通过!","系统提示",function(value){
                    //关闭并刷新
					setTimeout(function(){ top["win"].closeMenuTab() },100);
                });
				
			}
		});
		
	}

	//拒绝
	function refuse(){
       var form = new mini.Form("#fwdinfo");
		//验证表单
		form.validate();
		if(form.isValid() == false){
		mini.alert("表单填写错误,请确认!","提示信息")
		return;
		}
			
		var data = form.getData(true); //获取表单多个控件的数据
		if(data.jsyqTp == "1"){
            data.jsyqTp = "结汇";
        }else if(data.jsyqTp == "2"){
            data.jsyqTp = "售汇";
        }
		data.userSt='Y'
		data.chckSt='N'
		data.wbamBl=''
		data.newStatCd='A'
		var param = mini.encode(data); //序列化成JSON
		
		CommonUtil.ajax({
			url:'/OptionController/refuse',
			data:param ,
			callback:function(data){
				mini.alert("复核拒绝成功!","系统提示",function(value){
                    //关闭并刷新
					setTimeout(function(){ top["win"].closeMenuTab() },100);
                });
				
			}
		});
			
	}

	//择期交割日-开始日期
	function minDateChange(){
		////debugger
		var form = new mini.Form("#fwdinfo");
		var data = form.getData(true); //获取表单多个控件的数据
		/* if(data.jgDatt == null || data.jgDatt == ""){//至
			return false;
		}
 */
		if(CommonUtil.dateDiff(data.jgDatf,data.jgDatt) == 0){
			mini.alert("此日期不能等于结束择期交割日!","提示");
			mini.get("jgDatf").setValue("");
			return false;
		}

		if(CommonUtil.dateCompare(data.jgDatf,data.jgDatt)){
			mini.alert("此日期不能大于结束择期交割日!","提示");
			mini.get("jgDatf").setValue("");
			return false;
		}
		
	}


	//择期交割日-结束日期
	function maxDateChange(){
		////debugger
		var form = new mini.Form("#fwdinfo");
		var data = form.getData(true); //获取表单多个控件的数据
		if(data.jgDatt == null || data.jgDatt == ""){//至
			return false;
		}

		if(CommonUtil.dateDiff(data.jgDatf,data.jgDatt) == 0){
			mini.alert("此日期不能等于开始择期交割日!","提示");
			mini.get("jgDatt").setValue("");
			return false;
		}

		if(CommonUtil.dateCompare(data.jgDatf,data.jgDatt)){
			mini.alert("此日期不能小于开始择期交割日!","提示");
			mini.get("jgDatt").setValue("");
			return false;
		}
		
	}
	//交割方式改变时发生
	function jgTypeChange(){
		var jgType = mini.get("jgType").getValue();
		if(jgType == "1"){//固定交割
			mini.get("jgDate").setEnabled(true);//固定交割日
			mini.get("jgDatf").setEnabled(false);//择期交割日
			mini.get("jgDatt").setEnabled(false);//至
			mini.get("jgDatf").setValue("");
			mini.get("jgDatt").setValue("");
		}else if(jgType == "2"){//择期交割
			mini.get("jgDate").setEnabled(false);//固定交割日
			mini.get("jgDatf").setEnabled(true);//择期交割日
			mini.get("jgDatt").setEnabled(true);//至
			mini.get("jgDate").setValue("");
		}
	}


	
</script>
</body>
</html>
