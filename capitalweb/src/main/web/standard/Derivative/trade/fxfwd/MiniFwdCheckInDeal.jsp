<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../../global.jsp"%>
<html>
  <head>
  <title>远期外汇买卖-登记维护</title>
  <script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
  </head>
  
  <body style="width:100%;height:100%;background:white">
		<div class="mini-splitter" style="width:100%;height:100%;">
			<div size="90%" showCollapseButton="true">
				<div class="mini-fit"  style="background:white" id="fwdinfo">
						<fieldset class="mini-fieldset title">
							<legend>远期外汇买卖-登记</legend>
							<table  width="100%" class="mini-table">
								<tr>
									<td><input class="mini-buttonedit"  onbuttonclick="onInsQuery" id="brchNo" name="brchNo"  style="width:400px" labelField="true" label="机构号" emptyText=""  required="true"  /></td>
									<td><input class="mini-textbox"  id ="brchNm" name="brchNm"   style="width:400px"labelField="true" label="机构名称"  emptyText="同步反显" enabled="false"/></td>
								</tr>
								<tr>
									<td><input class="mini-textbox"  id="jsyqNo"  name="jsyqNo" style="width:400px" labelField="true" label="业务编号" emptyText="系统自动生成" enabled="false"  /></td>
									<td><input class="mini-buttonedit" onbuttonclick="onCustQuery"  id="custNo"  name="custNo" style="width:400px"  labelField="true" label="客户代码" emptyText="" /></td>
								</tr>
								<tr>
									<td><input class="mini-textbox" id="custNm"  name="custNm" labelField="true" label="客户名称" style="width:400px"  emptyText="同步反显" enabled="false"></td>
									<td><input class="mini-textbox" id="custAd"  name="custAd" labelField="true" label="客户地址" style="width:400px" emptyText="同步反显" enabled="false"></td>
								</tr>
								<tr>
									<td><input class="mini-combobox" id="jsyqTp"  name="jsyqTp" style="width:400px" labelField="true" label="结汇/售汇"  emptyText="" data = "CommonUtil.serverData.dictionary.JHSH"/></td>
									<td><input class="mini-combobox" id="yucyNo"  name="yucyNo" style="width:400px" labelField="true" label="原币种"  emptyText="" data = "CommonUtil.serverData.dictionary.Currency" onvaluechanged="yucyNoChange"/></td>
								</tr>

								<tr>
									
									<td><input class="mini-buttonedit" onbuttonclick="onAccoQuery" id="yuacNo"  name="yuacNo" style="width:400px" labelField="true" label="原账号"  emptyText=""/></td>
									<td><input class="mini-combobox" id="mdcyNo"  name="mdcyNo" style="width:400px" labelField="true" label="目的币种"  emptyText="" data = "CommonUtil.serverData.dictionary.Currency" onvaluechanged="mdcyNoChange"/></td>
								</tr>
								<tr>
									
									<td><input class="mini-buttonedit" onbuttonclick="onAccoQuery" id="mdacNo"  name="mdacNo" style="width:400px" labelField="true" label="目的账号"  emptyText=""/></td>
									<td><input class="mini-textbox" id="wBamts"  name="wBamts" style="width:400px" labelField="true" label="交割总金额(外币)"  emptyText="" vtype="float;maxLength:19"/></td>
								</tr>
								
								<tr>
									<td><input class="mini-buttonedit" onbuttonclick="onInquiryPriceQuery" id="fdQvno"  name="fdQvno" style="width:400px" labelField="true" label="询价业务编号"  emptyText=""/></td>
									<td><input class="mini-textbox" id="myRate"  name="myRate" style="width:400px" labelField="true" label="我行报客户汇率"  emptyText="同步回显" enabled="false"/></td>
								</tr>
								<tr>
									
									<td ><input class="mini-textbox" id="exRate"  name="exRate" style="width:400px" labelField="true" label="总行平盘汇率"  emptyText="同步回显" enabled="false"/></td>
									<td ><input class="mini-combobox" id="jgType"  name="jgType"  style="width:400px" labelField="true" label="交割方式"  onvaluechanged="jgTypeChange" emptyText=""  data = "CommonUtil.serverData.dictionary.DeliveryType"/></td>
								</tr>
								<tr>
									
									<td><input class="mini-datepicker" id="jgDate"  name="jgDate" format="yyyy-MM-dd" style="width:400px" labelField="true" label="固定交割日"  enabled="false" emptyText="" format="yyyy-MM-dd" showOkButton="true" showClearButton="false"/></td>
									<td><input class="mini-datepicker"  id="jgDatf"  name="jgDatf" style="width:400px" labelField="true" label="择期交割日" onvaluechanged="minDateChange" required="true"  enabled="false" emptyText="" format="yyyy-MM-dd" showOkButton="true" showClearButton="false"/></td>
								</tr>
								<tr>
									<td>
										<input class="mini-datepicker"  id="jgDatt"  name="jgDatt" style="width:400px" labelField="true" label="至："  onvaluechanged="maxDateChange" required="true"   enabled="false" emptyText="" format="yyyy-MM-dd" showOkButton="true" showClearButton="false"/>
									</td>	
								</tr>
							</table>
						</fieldset>
						<fieldset class="mini-fieldset title" style="margin-top:20px;">
							<legend>额度-保证金信息</legend>
							<table  width="100%" class="mini-table">
								<tr>
									<td><input class="mini-combobox"  id="qtCyno"  name="qtCyno" style="width:400px" labelField="true" label="占用额度币种"  emptyText="" data = "CommonUtil.serverData.dictionary.Currency"/></td>
									<td><input class="mini-textbox" id="qTamts"  name="qTamts" style="width:400px" labelField="true" label="金额"  emptyText="" vtype="float;maxLength:19"/></td>
								</tr>
								
								<tr>
									<td><input class="mini-buttonedit" id="mgAcno"  name="mgAcno" style="width:400px" labelField="true" label="冻结编号一"  emptyText="" /></td>
									<td><input class="mini-textbox" id="suAcno"  name="suAcno" style="width:400px" labelField="true" label="保证金账号"  emptyText="同步回显" enabled="false"/></td>
								</tr>
								
								<tr>
									<td><input class="mini-combobox" id="mgAccy"  name="mgAccy" style="width:400px" labelField="true" label="币种"  emptyText="" data = "CommonUtil.serverData.dictionary.Currency"/></td>
									<td><input class="mini-textbox"  id="mgAcam"  name="mgAcam"  style="width:400px"labelField="true" label="金额"  emptyText="" vtype="float;maxLength:19"/></td>
								</tr>
								
								<tr>
									<td><input class="mini-buttonedit"  id="mgAcn2"  name="mgAcn2" style="width:400px" labelField="true" label="冻结编号二"  emptyText="" /></td>
									<td><input class="mini-textbox"  id="suAcn2"  name="suAcn2" style="width:400px" labelField="true" label="保证金账号"  emptyText="同步回显" enabled="false"/></td>
								</tr>
								
								<tr>
									<td><input class="mini-combobox" id="mgAcc2"  name="mgAcc2"  style="width:400px" labelField="true" label="币种"  emptyText="" data = "CommonUtil.serverData.dictionary.Currency"/></td>
									<td><input class="mini-textbox"  id="mgAca2"  name="mgAca2"  style="width:400px" labelField="true" label="金额"  emptyText=""  vtype="float;maxLength:19"/></td>
								</tr>
								
								<tr>
									<td><input class="mini-buttonedit"  id="mgAcn3"  name="mgAcn3" style="width:400px" labelField="true" label="冻结编号三"  emptyText=""/></td>
									<td><input class="mini-textbox" id="suAcn3"  name="suAcn3" style="width:400px" labelField="true" label="保证金账号"  emptyText="同步回显" enabled="false"/></td>
								</tr>
								
								<tr>
									<td><input class="mini-combobox" id="mgAcc3"  name="mgAcc3"  style="width:400px" labelField="true" label="币种"  emptyText="" data = "CommonUtil.serverData.dictionary.Currency"/></td>
									<td><input class="mini-textbox"  id="mgAca3"  name="mgAca3" style="width:400px" labelField="true" label="金额"  emptyText=""  vtype="float;maxLength:19"/></td>
								</tr>
								
							</table>
						</fieldset>
				</div>
			</div>
			<div id="functionIds" showCollapseButton="true">
				<div style="margin-bottom:10px; margin-top:50px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_valid"   onclick="valid()">校验要素</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_save"          onclick="save">保存交易</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_verify"    enabled="false" onclick="verify">提交复核</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_refuse"        enabled="false" onclick="refuse">拒绝交易</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_checkTrade"        enabled="false"   onclick="checkTrade">复核交易</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_close"              onclick="close">关闭界面</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_tradeFee"     enabled="false" onclick="tradeFee">交易费用</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_tradeProof"      enabled="false" onclick="tradeProof">交易凭证</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_agencyFeeBtn"     enabled="false"    onclick="">会计传票</a></div>
			</div>      
		</div>


   
	
<script type="text/javascript">
     
	mini.parse();

	var url = window.location.search;
    var action = CommonUtil.getParam(url,"action");
	var dataTp = CommonUtil.getParam(url,"dataTp");

	$(document).ready(function(){
		//mini.get("custNo").doValueChanged();
        init();
    });

	function init(){
		if(action == "new"){
			//$("#labell").html("<a href='javascript:CommonUtil.activeTab();'>科目管理</a> >> 新增");
			
		}else if(action == "edit" || action == "detail"){
			var form1 = new mini.Form("fwdinfo");
			var data=top["fwdMainConfirmList"].getData();
			form1.setData(data);
			//buttonedit控件需要单独设置值
			//1.机构号
			mini.get("brchNo").setValue(data.brchNo);
			mini.get("brchNo").setText(data.brchNo);
			//2.客户代码
			mini.get("custNo").setValue(data.custNo);
			mini.get("custNo").setText(data.custNo);
			//3.原账号
			mini.get("yuacNo").setValue(data.yuacNo);
			mini.get("yuacNo").setText(data.yuacNo);
			//4.目的账号
			mini.get("mdacNo").setValue(data.mdacNo);
			mini.get("mdacNo").setText(data.mdacNo);
			//5.询价业务编号
			mini.get("fdQvno").setValue(data.fdQvno);
			mini.get("fdQvno").setText(data.fdQvno);
			//6.冻结编号一
			mini.get("mgAcno").setValue(data.mgAcno);
			mini.get("mgAcno").setText(data.mgAcno);
			//7.冻结编号二
			mini.get("mgAcn2").setValue(data.mgAcn2);
			mini.get("mgAcn2").setText(data.mgAcn2);
			//8.冻结编号三
			mini.get("mgAcn3").setValue(data.mgAcn3);
			mini.get("mgAcn3").setText(data.mgAcn3);

			if(action == "detail"){
				form1.setEnabled(false);
				if(dataTp == "verify"){//待复核
					mini.get("btn_valid").setEnabled(false);//检验要素
					mini.get("btn_save").setEnabled(false);//保存交易
					mini.get("btn_verify").setEnabled(false);//提交复核
					mini.get("btn_refuse").setEnabled(true);//拒绝交易
					mini.get("btn_checkTrade").setEnabled(true);//复核交易
					mini.get("btn_close").setEnabled(false);//关闭界面
					mini.get("btn_tradeFee").setEnabled(false);//交易费用
					mini.get("btn_tradeProof").setEnabled(false);//交易凭证
					mini.get("btn_agencyFeeBtn").setEnabled(false);//会计传票
				}else if(dataTp == "effi"){//生效
					mini.get("btn_valid").setEnabled(false);//检验要素
					mini.get("btn_save").setEnabled(false);//保存交易
					mini.get("btn_verify").setEnabled(false);//提交复核
					mini.get("btn_refuse").setEnabled(false);//拒绝交易
					mini.get("btn_checkTrade").setEnabled(false);//复核交易
					mini.get("btn_close").setEnabled(false);//关闭界面
					mini.get("btn_tradeFee").setEnabled(true);//交易费用
					mini.get("btn_tradeProof").setEnabled(true);//交易凭证
					mini.get("btn_agencyFeeBtn").setEnabled(true);//会计传票
				}
			}else if(action == "edit"){
				if(dataTp == "modify"){//待修改
					mini.get("btn_valid").setEnabled(false);//检验要素
					mini.get("btn_save").setEnabled(false);//保存交易
					mini.get("btn_verify").setEnabled(true);//提交复核
					mini.get("btn_refuse").setEnabled(false);//拒绝交易
					mini.get("btn_checkTrade").setEnabled(false);//复核交易
					mini.get("btn_close").setEnabled(false);//关闭界面
					mini.get("btn_tradeFee").setEnabled(false);//交易费用
					mini.get("btn_tradeProof").setEnabled(false);//交易凭证
					mini.get("btn_agencyFeeBtn").setEnabled(false);//会计传票
				}
			}

		}
	}

	//保存
    function save(){
     	var form = new mini.Form("#fwdinfo");
		//验证表单
		form.validate();
		if(form.isValid() == false){
			mini.alert("表单填写错误,请确认!","提示信息")
			return;
		}
    		   
		var data = form.getData(true); //获取表单多个控件的数据
		if(data.jsyqTp == "1"){
            data.jsyqTp = "结汇";
        }else if(data.jsyqTp == "2"){
            data.jsyqTp = "售汇";
        }
		data.userSt='S'
		data.statCd='A'
		data.newStatCd='A'
		var param = mini.encode(data); //序列化成JSON
		
		CommonUtil.ajax({
					url:'/ForwardController/save',
					data:param ,
					callback:function(data){
						mini.get("jsyqNo").setValue(data.desc);
            			mini.alert("保存成功!");
            			mini.get("btn_verify").setEnabled(true);
						
					}
		});
    		
	}

	//机构的查询
	function onInsQuery(e) {
		var btnEdit = this;
		mini.open({
			url : "/fund-capital/standard/InterBank/mini_system/MiniInstitutionSelectManages.jsp",
			title : "机构选择",
			width : 900,
			height : 600,
			ondestroy : function(action) {	
				if (action == "ok") 
				{
					var iframe = this.getIFrameEl();
					var data = iframe.contentWindow.GetData();
					data = mini.clone(data); //必须
					if (data) {
						btnEdit.setValue(data.instId);
						btnEdit.setText(data.instId);
						mini.get("brchNm").setValue(data.instName);
						btnEdit.focus();
					}

					
				}

			}
		});
	}

	//客户的查询
	function onCustQuery(e) {
		var btnEdit = this;
		mini.open({
			url : CommonUtil.baseWebPath() +"/base/MiniCPManageYQ.jsp",
			title : "客户选择",
			width : 900,
			height : 600,
			ondestroy : function(action) {	
				if (action == "ok") 
				{
					var iframe = this.getIFrameEl();
					var data = iframe.contentWindow.GetData();
					data = mini.clone(data); //必须
					if (data) {
						btnEdit.setValue(data.party_id);
						btnEdit.setText(data.party_id);
						
						mini.get("custNm").setValue(data.party_name);
						//有联系人
						if(data.constact_list.length>0){
							for(var i=0;i<data.constact_list.length;i++){
								if(data.constact_list[i].address){//有联系人，还有地址
									mini.get("custAd").setValue(data.constact_list[i].address);
									return;
								}else{//有联系人，没有地址
									mini.get("custAd").setValue("");
								}
							}
							
						}else{//没有联系人
							mini.get("custAd").setValue("");
						}

						//mini.get("custNo").doValueChanged();
						
						
						btnEdit.focus();
						//btnEdit.doValueChanged(alert("1"));
						//mini.get("custNo").doValueChanged(custNoChange());
					}

					
				}

			}
		});
	}

	//客户代码值改变时发生
	var btnEdit1 = mini.get("custNo");
    btnEdit1.on("valuechanged", function () {
		//debugger
       //客户代码值改变，原账号清空
		mini.get("yuacNo").setValue("");
		mini.get("yuacNo").setText("");
		//客户代码值改变，目的账号清空
		mini.get("mdacNo").setValue("");
		mini.get("mdacNo").setText(""); 
    })

	/* function custNoChange(){
		////debugger
		//客户代码值改变，原账号清空
		mini.get("yuacNo").setValue("");
		mini.get("yuacNo").setText("");
		//客户代码值改变，目的账号清空
		mini.get("mdacNo").setValue("");
		mini.get("mdacNo").setText("");
	} */


	//原币种值改变时发生
	function yucyNoChange(){
		//原币种值改变，原账号清空
		mini.get("yuacNo").setValue("");
		mini.get("yuacNo").setText("");
		
	}

	//目的币种值改变时发生
	function mdcyNoChange(){
		//目的币种值改变，目的账号清空
		mini.get("mdacNo").setValue("");
		mini.get("mdacNo").setText("");
	} 

	//账号的查询
	function onAccoQuery(e) {
		
		var custNo = mini.get("custNo").getValue();
		if(!custNo){
			mini.alert("请选择客户代码!");
			return;
		}
		//原账号
		if(e.sender.id == "yuacNo"){
			//获取原币种
			var yucyNo = mini.get("yucyNo").getValue();
			if(!yucyNo){
				mini.alert("请选择原币种!");
				return;
			}
		}else if(e.sender.id == "mdacNo"){//目的账号
			//获取目的币种
			var mdcyNo = mini.get("mdcyNo").getValue();
			if(!mdcyNo){
				mini.alert("请选择目的币种!");
				return;
			}
		}
		
		
		var btnEdit = this;
		mini.open({
			url : CommonUtil.baseWebPath() +"/base/MiniAccountManage.jsp",
			title : "账号选择",
			width : 900,
			height : 600,
			
			onload: function () {//弹出页面加载完成
				var iframe = this.getIFrameEl(); 
				var data = {};
				//原账号
				if(e.sender.id == "yuacNo"){
					var custNo = mini.get("custNo").getValue();//客户代码
					var yucyNo = mini.get("yucyNo").getValue();//原币种
					data = {"custNo":custNo,"yucyNo":yucyNo,"type":"orig"};	
				}else if(e.sender.id == "mdacNo"){//目的账号
					var custNo = mini.get("custNo").getValue();//客户代码
					var mdcyNo = mini.get("mdcyNo").getValue();//目的币种
					data = {"custNo":custNo,"mdcyNo":mdcyNo,"type":"dest"};	
				}
				       
				//调用弹出页面方法进行初始化
				iframe.contentWindow.SetData(data); 
								
			},
			ondestroy : function(action) {	
				if (action == "ok") 
				{
					var iframe = this.getIFrameEl();
					var data = iframe.contentWindow.GetData();
					data = mini.clone(data); //必须
					if (data) {
						btnEdit.setValue(data.accoNo);
						btnEdit.setText(data.accoNo);
						btnEdit.focus();
					}

					
				}

			}
		});
	}
	//询价业务编号的查询=>需要先选择 客户代码、币种（外币）、交割总金额（外币）、结汇/售汇
	function onInquiryPriceQuery(e){
		//客户代码
		var custNo = mini.get("custNo").getValue();
		//交割总金额（外币）
		var wBamts = mini.get("wBamts").getValue();
		//结售汇类型
		var jsyqTp = mini.get("jsyqTp").getValue();
		//原币种
		var yucyNo = mini.get("yucyNo").getValue();
		//目的币种
		var mdcyNo = mini.get("mdcyNo").getValue();

		if((custNo==null||custNo=="")  || (wBamts==null||wBamts=="")  || (jsyqTp==null||jsyqTp=="") || (yucyNo==null||yucyNo=="") || (mdcyNo==null||mdcyNo=="") ){
			mini.alert("客户代码、交割总金额、结售汇类型、原币种、目的币种都必须正确填入(选择)!");
			return;
		}

		var btnEdit = this;
		mini.open({
			url : CommonUtil.baseWebPath() +"/inquiry/fxfwd/FwdInquiryListMini.jsp",
			title : "远期-询价业务编号选择",
			width : 900,
			height : 600,
			
			onload: function () {//弹出页面加载完成
				var iframe = this.getIFrameEl(); 
				var data = {};
				data = {"custNo":custNo,"yucyNo":yucyNo,"mdcyNo":mdcyNo,"jsyqTp":jsyqTp,"wBamts":wBamts};	
				//调用弹出页面方法进行初始化
				iframe.contentWindow.SetData(data); 
			},
			ondestroy : function(action) {	
				if (action == "ok") 
				{
					var iframe = this.getIFrameEl();
					var data = iframe.contentWindow.GetData();
					data = mini.clone(data); //必须
					if (data) {
						btnEdit.setValue(data.fDQVNO);
						btnEdit.setText(data.fDQVNO);
						mini.get("myRate").setValue(data.mYRATE);
						mini.get("exRate").setValue(data.eXRATE);
						btnEdit.focus();
					}

					
				}

			}
		});
		
		
	}

	//检验要素
	function valid(){
		var form = new mini.Form("fwdinfo");
		form.validate();
	}

	//提交
	function verify(){
       	var form = new mini.Form("#fwdinfo");
		//验证表单
		form.validate();
		if(form.isValid() == false){
			mini.alert("表单填写错误,请确认!","提示信息")
			return;
		}
			
		var data = form.getData(true); //获取表单多个控件的数据
		if(data.jsyqTp == "1"){
            data.jsyqTp = "结汇";
        }else if(data.jsyqTp == "2"){
            data.jsyqTp = "售汇";
        }
		data.userSt='Y'
		data.wbamBl=''
		data.newStatCd='A'
		var param = mini.encode(data); //序列化成JSON
		
		CommonUtil.ajax({
			url:'/ForwardController/commit',
			data:param,
			callback:function(data){
				mini.alert("提交成功!","系统提示",function(value){
                    //关闭并刷新
					setTimeout(function(){ top["win"].closeMenuTab() },100);
                });
				/* mini.alert("提交成功!");
				mini.get("btn_verify").setEnabled(false);
				mini.get("btn_valid").setEnabled(false);
				mini.get("btn_save").setEnabled(false); */
			}
		});
			
	}

	//复核通过
    function checkTrade(){
       var form = new mini.Form("#fwdinfo");
		//验证表单
		form.validate();
		if(form.isValid() == false){
		mini.alert("表单填写错误,请确认!","提示信息")
		return;
		}
			
		var data = form.getData(true); //获取表单多个控件的数据
		if(data.jsyqTp == "1"){
            data.jsyqTp = "结汇";
        }else if(data.jsyqTp == "2"){
            data.jsyqTp = "售汇";
        }
		data.userSt='Y'
		data.chckSt='Y'
        data.statCd='A'
        data.wbamBl=''
        data.newStatCd='A'
		var param = mini.encode(data); //序列化成JSON
		
		CommonUtil.ajax({
			url:'/ForwardController/checkTradeTr',
			data:param ,
			callback:function(data){
				mini.alert("复核通过!","系统提示",function(value){
                    //关闭并刷新
					setTimeout(function(){ top["win"].closeMenuTab() },100);
                });
				/* mini.alert("复核通过!");
				mini.get("btn_verify").setEnabled(false);
				mini.get("btn_valid").setEnabled(false);
				mini.get("btn_save").setEnabled(false);
				mini.get("btn_checkTrade").setEnabled(false);
				mini.get("btn_refuse").setEnabled(false); */
			}
		});
		
	}

	//拒绝
	function refuse(){
       var form = new mini.Form("#fwdinfo");
		//验证表单
		form.validate();
		if(form.isValid() == false){
		mini.alert("表单填写错误,请确认!","提示信息")
		return;
		}
			
		var data = form.getData(true); //获取表单多个控件的数据
		if(data.jsyqTp == "1"){
            data.jsyqTp = "结汇";
        }else if(data.jsyqTp == "2"){
            data.jsyqTp = "售汇";
        }
		data.userSt='Y'
		data.chckSt='N'
		data.wbamBl=''
		data.newStatCd='A'
		var param = mini.encode(data); //序列化成JSON
		
		CommonUtil.ajax({
			url:'/ForwardController/refuse',
			data:param ,
			callback:function(data){
				mini.alert("复核拒绝成功!","系统提示",function(value){
                    //关闭并刷新
					setTimeout(function(){ top["win"].closeMenuTab() },100);
                });
				/* mini.alert("复核拒绝成功!");
				mini.get("btn_verify").setEnabled(false);
				mini.get("btn_valid").setEnabled(false);
				mini.get("btn_save").setEnabled(false);
				mini.get("btn_checkTrade").setEnabled(false);
				mini.get("btn_refuse").setEnabled(false); */
			}
		});
			
	}

	//择期交割日-开始日期
	function minDateChange(){
		////debugger
		var form = new mini.Form("#fwdinfo");
		var data = form.getData(true); //获取表单多个控件的数据
		/* if(data.jgDatt == null || data.jgDatt == ""){//至
			return false;
		}
 */
		if(CommonUtil.dateDiff(data.jgDatf,data.jgDatt) == 0){
			mini.alert("此日期不能等于结束择期交割日!","提示");
			mini.get("jgDatf").setValue("");
			return false;
		}

		if(CommonUtil.dateCompare(data.jgDatf,data.jgDatt)){
			mini.alert("此日期不能大于结束择期交割日!","提示");
			mini.get("jgDatf").setValue("");
			return false;
		}
		
	}


	//择期交割日-结束日期
	function maxDateChange(){
		////debugger
		var form = new mini.Form("#fwdinfo");
		var data = form.getData(true); //获取表单多个控件的数据
		if(data.jgDatt == null || data.jgDatt == ""){//至
			return false;
		}

		if(CommonUtil.dateDiff(data.jgDatf,data.jgDatt) == 0){
			mini.alert("此日期不能等于开始择期交割日!","提示");
			mini.get("jgDatt").setValue("");
			return false;
		}

		if(CommonUtil.dateCompare(data.jgDatf,data.jgDatt)){
			mini.alert("此日期不能小于开始择期交割日!","提示");
			mini.get("jgDatt").setValue("");
			return false;
		}
		
	}
	//交割方式改变时发生
	function jgTypeChange(){
		var jgType = mini.get("jgType").getValue();
		if(jgType == "1"){//固定交割
			mini.get("jgDate").setEnabled(true);//固定交割日
			mini.get("jgDatf").setEnabled(false);//择期交割日
			mini.get("jgDatt").setEnabled(false);//至
			mini.get("jgDatf").setValue("");
			mini.get("jgDatt").setValue("");
		}else if(jgType == "2"){//择期交割
			mini.get("jgDate").setEnabled(false);//固定交割日
			mini.get("jgDatf").setEnabled(true);//择期交割日
			mini.get("jgDatt").setEnabled(true);//至
			mini.get("jgDate").setValue("");
		}
	}


	
</script>
</body>
</html>
