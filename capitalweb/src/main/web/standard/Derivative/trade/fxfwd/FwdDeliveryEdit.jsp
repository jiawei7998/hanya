<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../../global.jsp"%>
<html>
  <head>
  <title>远期交割维护</title>
  <script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
  </head>
  
  <body style="width:100%;height:100%;background:white">
		<div class="mini-splitter" style="width:100%;height:100%;">
			<div size="90%" showCollapseButton="true">
				<div class="mini-fit"  style="background:white" id="inquiry_form">
							<table  width="100%" class="mini-table">
								<tr>
                                    <td><input class="mini-textbox"  id ="jsyqNo" name="jsyqNo"   style="width:400px"labelField="true" label="业务编号"  emptyText="系统自动生成" labelStyle="width:130px" enabled="false"/></td>
									<td><input class="mini-buttonedit"  onbuttonclick="onInsQuery" id="brchNo" name="brchNo"  style="width:400px" labelField="true" label="机构号" labelStyle="width:130px" emptyText=""  required="true"  vtype="maxLength:32"/></td>
								</tr>
								<tr>
                                    <td><input class="mini-buttonedit" onbuttonclick="onCustQuery"  id="custNo"  name="custNo" style="width:400px"  labelStyle="width:130px" labelField="true" label="客户代码" emptyText="" required="true" /></td>
                                    <td><input class="mini-textbox"  id="custNm"  name="custNm" style="width:400px" labelField="true" label="客户名称" labelStyle="width:130px" emptyText="同步反显" enabled="false"  /></td>
								</tr>
								<tr>
									<td><input class="mini-combobox" id="bkFlag"  name="bkFlag" labelField="true" label="客户性质" style="width:400px"  labelStyle="width:130px" emptyText="" data = "CommonUtil.serverData.dictionary.JHSH"></td> 
									<td><input class="mini-combobox" id="jsyqTp"  name="jsyqTp" labelField="true" label="结汇/售汇" style="width:400px" labelStyle="width:130px" emptyText="" data = "CommonUtil.serverData.dictionary.JHSH"></td>
								</tr>
								<!-- <tr>
									<td><input class="mini-textbox" id="qaBank"  name="qaBank" style="width:400px" labelField="true" label="资格银行代码"  labelStyle="width:130px" emptyText="" required="true"  /></td>
									<td><input class="mini-textbox" id="qabkNm"  name="qabkNm" style="width:400px" labelField="true" label="资格银行名称"  labelStyle="width:130px" emptyText="" required="true"   /></td>
								</tr> -->

								<tr>
									<td><input class="mini-combobox" id="yucyNo"  name="yucyNo" style="width:400px" labelField="true" label="原币种"  labelStyle="width:130px" emptyText="" required="true"  data = "CommonUtil.serverData.dictionary.Currency" /></td>
                                    <td><input class="mini-buttonedit" onbuttonclick="onAccoQuery" id="yuacNo"  name="yuacNo" style="width:400px" labelField="true" label="原账号"  labelStyle="width:130px" emptyText="" required="true" /></td>
                                </tr>
                                <tr>
									<td><input class="mini-combobox" id="mdcyNo"  name="mdcyNo" style="width:400px" labelField="true" label="目的币种"  labelStyle="width:130px" emptyText="" required="true"  data = "CommonUtil.serverData.dictionary.Currency" /></td>
                                    <td><input class="mini-buttonedit" onbuttonclick="onAccoQuery" id="mdacNo"  name="mdacNo" style="width:400px" labelField="true" label="目的账号" labelStyle="width:130px"  emptyText="" required="true" /></td>
                                </tr>
								<tr>
									<td><input class="mini-combobox"  id="jsxjTp"  name="jsxjTp" style="width:400px" labelField="true" label="相关业务类型"  labelStyle="width:130px" emptyText="" required="true"  data = "CommonUtil.serverData.dictionary.Term"/></td>
									<td><input class="mini-textbox" id="wBamts"  name="wBamts" style="width:400px" labelField="true" label="交割总金额（外币）" labelStyle="width:130px" emptyText="" vtype="float;maxLength:19"/></td>
                                </tr>
                                <tr>
									<td><input class="mini-textbox" id="wbamDo"  name="wbamDo" style="width:400px" labelField="true" label="本次交割金额（外币）" labelStyle="width:130px" emptyText="" onvaluechanged="onwbamDoChanged" vtype="float;maxLength:19"/></td>
									<td><input class="mini-textbox"  id="wbamBl"  name="wbamBl" style="width:400px" labelField="true" label="未交割金额（外币）" labelStyle="width:130px" emptyText="" required="true"  vtype="float;maxLength:19"/></td>
                                </tr>
                                <tr>
									<td><input class="mini-textbox" id="exRate"  name="exRate" style="width:400px" labelField="true" label="总行平盘汇率" labelStyle="width:130px" emptyText="" vtype="float;maxLength:19"/></td>
									<td><input class="mini-textbox"  id="myRate"  name="myRate" style="width:400px" labelField="true" label="我行报客户汇率" labelStyle="width:130px" emptyText="" vtype="float;maxLength:19"/></td>
								</tr>

                                <tr>
									<td><input class="mini-textbox"  id="oexRat"  name="oexRat" style="width:400px" labelField="true" label="原总行平盘汇率" labelStyle="width:130px" emptyText="" vtype="float;maxLength:19"/></td>
									<td><input class="mini-textbox" id="omyRat"  name="omyRat" style="width:400px" labelField="true" label="原我行报客户汇率" labelStyle="width:130px" emptyText="" vtype="float;maxLength:19"/></td>
                                </tr>
                               
                                <tr>
									<td><input class="mini-textbox"  id="whrbAm"  name="whrbAm" style="width:400px" labelField="true" label="我行金额（人民币）" labelStyle="width:130px" emptyText="" vtype="float;maxLength:19"/></td>
									<td><input class="mini-textbox"  id="curbAm"  name="curbAm" style="width:400px" labelField="true" label="客户金额（人民币）" labelStyle="width:130px" emptyText="" vtype="float;maxLength:19"/></td>
									
                                </tr>
                                <tr>
									
									<td><input class="mini-textbox" id="gnlsAm"  name="gnlsAm" style="width:400px" labelField="true" label="盈亏金额（人民币）" labelStyle="width:130px" emptyText="" vtype="float;maxLength:19"/></td>
									<td><input class="mini-textbox" id="reRate"  name="reRate" style="width:400px" labelField="true" label="掉期远端汇率" labelStyle="width:130px" emptyText="" vtype="float;maxLength:19"/></td>
								</tr>
                                
                                <tr>
                                    <td><input class="mini-combobox" id="jgType"  name="jgType" style="width:400px" labelField="true" label="交割方式" labelStyle="width:130px" emptyText="" data = "CommonUtil.serverData.dictionary.DeliveryType"/></td>
									<td><input class="mini-datepicker"  id="jgDate"  name="jgDate" style="width:400px" labelField="true" label="固定交割日" labelStyle="width:130px" emptyText="" showOkButton="true" showClearButton="false"/></td>
									
                                </tr>
                                <tr>
                                        <td><input class="mini-datepicker" id="jgDatf"  name="jgDatf" style="width:400px" labelField="true" label="择期交割日" labelStyle="width:130px" emptyText="" showOkButton="true" showClearButton="false"/></td>
                                        <td><input class="mini-datepicker" id="jgDatt"  name="jgDatt" style="width:400px" labelField="true" label="至" labelStyle="width:130px" emptyText="" showOkButton="true" showClearButton="false"/></td>
                                </tr>
                                <tr>
									<td><input class="mini-combobox"  id="naType"  name="naType" style="width:400px" labelField="true" label="结售汇统计类型" labelStyle="width:130px" emptyText="" data = "CommonUtil.serverData.dictionary.JHSH"/></td>
									<td><input class="mini-combobox" id="naKnd2"  name="naKnd2" style="width:400px" labelField="true" label="结售汇统计报表类型" labelStyle="width:130px" emptyText="" data = "CommonUtil.serverData.dictionary.JHSH"/></td>
                                </tr>
                                <tr>
									<td><input class="mini-textbox"  id="oqabTp"  name="oqabTp" style="width:400px" labelField="true" label="交易编码" labelStyle="width:130px" emptyText="" vtype="maxLength:8"/></td>
									<td><input class="mini-textbox" id="naType"  name="naType" style="width:400px" labelField="true" label="外汇局批件号" labelStyle="width:130px" emptyText="" vtype="maxLength:20"/></td>
                                </tr>
                                <tr>
									<td><input class="mini-textbox"  id="fuType"  name="fuType" style="width:400px" labelField="true" label="结汇用途" labelStyle="width:130px" emptyText="" vtype="maxLength:64"/></td>
									<td><input class="mini-textarea"  id="remark"  name="remark" style="width:400px" labelField="true" label="结汇详细用途" labelStyle="width:130px" emptyText="" vtype="maxLength:128"/></td>
                                </tr>
                               
								
								
							</table>
						
				</div>
			</div>
			<div id="functionIds" showCollapseButton="true">
				<div style="margin-bottom:10px; margin-top:5px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_valid"   onclick="valid()">校验要素</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_save"          onclick="save">保存交易</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_verify"    enabled="false" onclick="verify">提交复核</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_refuse"        enabled="false" onclick="refuse">拒绝交易</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_checkTrade"        enabled="false"   onclick="checkTrade">复核交易</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_close"              onclick="close">关闭界面</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_tradeFee"     enabled="false" onclick="tradeFee">交易费用</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_tradeProof"      enabled="false" onclick="tradeProof">交易凭证</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_agencyFeeBtn"     enabled="false"    onclick="">会计传票</a></div>
			</div>      
		</div>


</body>

<script type="text/javascript">
    mini.parse();

    var url = window.location.search;
    var action = CommonUtil.getParam(url,"action");
    var dataTp = CommonUtil.getParam(url,"dataTp");

    $(document).ready(function(){
        init();
    });

    function init(){
		if(action == "new"){
			
		}else if(action == "edit" || action == "detail"){
            var form1 = new mini.Form("inquiry_form");
			var data=top["fwdDeliveryList"].getData();
			form1.setData(data);
			//buttonedit控件需要单独设置值
			//1.机构号
			mini.get("brchNo").setValue(data.brchNo);
			mini.get("brchNo").setText(data.brchNo);
			//2.客户代码
			mini.get("custNo").setValue(data.custNo);
			mini.get("custNo").setText(data.custNo);
			if(action == "detail"){
				form1.setEnabled(false);
				
				if(dataTp == "verify"){//待复核
					
					mini.get("btn_valid").setEnabled(false);//检验要素
					mini.get("btn_save").setEnabled(false);//保存交易
					mini.get("btn_verify").setEnabled(false);//提交复核
					mini.get("btn_refuse").setEnabled(true);//拒绝交易
					mini.get("btn_checkTrade").setEnabled(true);//复核交易
					mini.get("btn_close").setEnabled(false);//关闭界面
					mini.get("btn_tradeFee").setEnabled(false);//交易费用
					mini.get("btn_tradeProof").setEnabled(false);//交易凭证
					mini.get("btn_agencyFeeBtn").setEnabled(false);//会计传票
				}else if(dataTp == "effi"){//生效
					mini.get("btn_valid").setEnabled(false);//检验要素
					mini.get("btn_save").setEnabled(false);//保存交易
					mini.get("btn_verify").setEnabled(false);//提交复核
					mini.get("btn_refuse").setEnabled(false);//拒绝交易
					mini.get("btn_checkTrade").setEnabled(false);//复核交易
					mini.get("btn_close").setEnabled(false);//关闭界面
					mini.get("btn_tradeFee").setEnabled(true);//交易费用
					mini.get("btn_tradeProof").setEnabled(true);//交易凭证
					mini.get("btn_agencyFeeBtn").setEnabled(true);//会计传票
				}else if(dataTp == "handle"){//经办
					mini.get("wbamDo").setEnabled(true);//本次交割金额可以修改

					mini.get("btn_valid").setEnabled(false);//检验要素
					mini.get("btn_save").setEnabled(true);//保存交易
					mini.get("btn_verify").setEnabled(false);//提交复核
					mini.get("btn_refuse").setEnabled(false);//拒绝交易
					mini.get("btn_checkTrade").setEnabled(false);//复核交易
					mini.get("btn_close").setEnabled(false);//关闭界面
					mini.get("btn_tradeFee").setEnabled(false);//交易费用
					mini.get("btn_tradeProof").setEnabled(false);//交易凭证
					mini.get("btn_agencyFeeBtn").setEnabled(false);//会计传票
				}
			}else if(action == "edit"){
				form1.setEnabled(false);
				mini.get("wbamDo").setEnabled(false);//本次交割金额可以修改

				if(dataTp == "modify"){//待修改
					mini.get("btn_valid").setEnabled(false);//检验要素
					mini.get("btn_save").setEnabled(false);//保存交易
					mini.get("btn_verify").setEnabled(true);//提交复核
					mini.get("btn_refuse").setEnabled(false);//拒绝交易
					mini.get("btn_checkTrade").setEnabled(false);//复核交易
					mini.get("btn_close").setEnabled(false);//关闭界面
					mini.get("btn_tradeFee").setEnabled(false);//交易费用
					mini.get("btn_tradeProof").setEnabled(false);//交易凭证
					mini.get("btn_agencyFeeBtn").setEnabled(false);//会计传票
				}
			}

        }
    }

    //机构的查询
	function onInsQuery(e) {
		var btnEdit = this;
		mini.open({
			url : CommonUtil.baseWebPath() +"InterBank/mini_system/MiniInstitutionSelectManages.jsp",
			title : "机构选择",
			width : 900,
			height : 600,
			ondestroy : function(action) {	
				if (action == "ok") 
				{
					var iframe = this.getIFrameEl();
					var data = iframe.contentWindow.GetData();
					data = mini.clone(data); //必须
					if (data) {
						btnEdit.setValue(data.instId);
						btnEdit.setText(data.instId);
						btnEdit.focus();
					}

					
				}

			}
		});
	}

     //客户的查询
	function onCustQuery(e) {
		var btnEdit = this;
		mini.open({
			url : CommonUtil.baseWebPath() +"/base/MiniCPManageYQ.jsp",
			title : "客户选择",
			width : 900,
			height : 600,
			ondestroy : function(action) {	
				if (action == "ok") 
				{
					var iframe = this.getIFrameEl();
					var data = iframe.contentWindow.GetData();
					data = mini.clone(data); //必须
					if (data) {
						btnEdit.setValue(data.party_id);
						btnEdit.setText(data.party_id);
						mini.get("custNm").setValue(data.party_name);
						btnEdit.focus();
						
					}

					
				}

			}
		});
	}

     //账号的查询
	function onAccoQuery(e) {
		
		var custNo = mini.get("custNo").getValue();
		if(!custNo){
			mini.alert("请选择客户代码!");
			return;
		}
		//原账号
		if(e.sender.id == "yuacNo"){
			//获取原币种
			var yucyNo = mini.get("yucyNo").getValue();
			if(!yucyNo){
				mini.alert("请选择原币种!");
				return;
			}
		}else if(e.sender.id == "mdacNo"){//目的账号
			//获取目的币种
			var mdcyNo = mini.get("mdcyNo").getValue();
			if(!mdcyNo){
				mini.alert("请选择目的币种!");
				return;
			}
		}
		var btnEdit = this;
		mini.open({
			url : CommonUtil.baseWebPath() +"/base/MiniAccountManage.jsp",
			title : "账号选择",
			width : 900,
			height : 600,
			
			onload: function () {//弹出页面加载完成
				var iframe = this.getIFrameEl(); 
				var data = {};
				//原账号
				if(e.sender.id == "yuacNo"){
					var custNo = mini.get("custNo").getValue();//客户代码
					var yucyNo = mini.get("yucyNo").getValue();//原币种
					data = {"custNo":custNo,"yucyNo":yucyNo,"type":"orig"};	
				}else if(e.sender.id == "mdacNo"){//目的账号
					var custNo = mini.get("custNo").getValue();//客户代码
					var mdcyNo = mini.get("mdcyNo").getValue();//目的币种
					data = {"custNo":custNo,"mdcyNo":mdcyNo,"type":"dest"};	
				}
				       
				//调用弹出页面方法进行初始化
				iframe.contentWindow.SetData(data); 
								
			},
			ondestroy : function(action) {	
				if (action == "ok") 
				{
					var iframe = this.getIFrameEl();
					var data = iframe.contentWindow.GetData();
					data = mini.clone(data); //必须
					if (data) {
						btnEdit.setValue(data.accoNo);
						btnEdit.setText(data.accoNo);
						btnEdit.focus();
					}

					
				}

			}
		});
	}
	//本次交割金额  改变时发生
	function onwbamDoChanged(){
		//获取交割总金额
		var totalMoney = mini.get("wBamts").getValue();
		//获取本次交割金额
		var thisMoney = mini.get("wbamDo").getValue();
		//未交割金额
		var notMoney = CommonUtil.accSubtr(totalMoney,thisMoney);
		mini.get("wbamBl").setValue(notMoney.toString());

		//总行平盘汇率
		var totalRate = mini.get("exRate").getValue();
		totalRate = CommonUtil.accDiv(totalRate,100);
		//我行金额
		var myMoney =  CommonUtil.accMul(thisMoney,totalRate);
		mini.get("whrbAm").setValue(myMoney.toString());

		//我行报客户汇率
		var custRate = mini.get("myRate").getValue();
		custRate = CommonUtil.accDiv(custRate,100);
		//客户金额
		var custMoney =  CommonUtil.accMul(thisMoney,custRate);
		mini.get("curbAm").setValue(custMoney.toString());

		//盈亏金额
		var profitLossMoney = CommonUtil.accSubtr(myMoney,custMoney); 
		mini.get("gnlsAm").setValue(profitLossMoney.toString());



	}

//***********************************事件*****************************************************************
    
    //检验要素
	function valid(){
		var form = new mini.Form("inquiry_form");
		form.validate();
	}

    //保存
    function save(){
     	var form = new mini.Form("#inquiry_form");
		//验证表单
		form.validate();
		if(form.isValid() == false){
			mini.alert("表单填写错误,请确认!","提示信息")
			return;
		}
    		   
		var data = form.getData(true); //获取表单多个控件的数据
        if(data.jsyqTp == "1"){
            data.jsyqTp = "结汇";
        }else if(data.jsyqTp == "2"){
            data.jsyqTp = "售汇";
        }
        data.userSt='S'
        data.statCd='C'
        data.newStatCd='C'
		var param = mini.encode(data); //序列化成JSON
		
		CommonUtil.ajax({
					url:'//ForwardController/save',
					data:param ,
					callback:function(data){
                        //配置业务编号
						mini.get("jsyqNo").setValue(data.desc);
            			mini.alert("保存成功!");
                        //提交按钮显示
            			mini.get("btn_verify").setEnabled(true);
						
					}
		});
    		
	}

    //提交
	function verify(){
       	var form = new mini.Form("#inquiry_form");
		//验证表单
		form.validate();
		if(form.isValid() == false){
			mini.alert("表单填写错误,请确认!","提示信息")
			return;
		}
			
		var data = form.getData(true); //获取表单多个控件的数据
		if(data.jsyqTp == "1"){
            data.jsyqTp = "结汇";
        }else if(data.jsyqTp == "2"){
            data.jsyqTp = "售汇";
        }
		data.userSt='Y'
		data.newStatCd='C'
		var param = mini.encode(data); //序列化成JSON
		
		CommonUtil.ajax({
			url:'/ForwardController/commit',
			data:param,
			callback:function(data){
				mini.alert("提交成功!","系统提示",function(value){
                    //关闭并刷新
					setTimeout(function(){ top["win"].closeMenuTab() },100);
                });
				
			}
		});
			
	}

    //复核通过
    function checkTrade(){
       var form = new mini.Form("#inquiry_form");
		//验证表单
		form.validate();
		if(form.isValid() == false){
		mini.alert("表单填写错误,请确认!","提示信息")
		return;
		}
			
		var data = form.getData(true); //获取表单多个控件的数据
		if(data.jsyqTp == "1"){
            data.jsyqTp = "结汇";
        }else if(data.jsyqTp == "2"){
            data.jsyqTp = "售汇";
        }
		data.userSt='Y'
		data.chckSt='Y'
		data.statCd='C'
		data.newStatCd='C'
		var param = mini.encode(data); //序列化成JSON
		
		CommonUtil.ajax({
			url:'/ForwardController/checkTradeTr',
			data:param ,
			callback:function(data){
				mini.alert("复核通过!","系统提示",function(value){
					//关闭并刷新
					setTimeout(function(){ top["win"].closeMenuTab() },100);
				});
				
			}
		});
		
	}

    //拒绝
	function refuse(){
       var form = new mini.Form("#inquiry_form");
		//验证表单
		form.validate();
		if(form.isValid() == false){
		mini.alert("表单填写错误,请确认!","提示信息")
		return;
		}
			
		var data = form.getData(true); //获取表单多个控件的数据
		if(data.jsyqTp == "1"){
            data.jsyqTp = "结汇";
        }else if(data.jsyqTp == "2"){
            data.jsyqTp = "售汇";
        }
		data.userSt='Y'
		data.chckSt='N'
		data.newStatCd='C'
		var param = mini.encode(data); //序列化成JSON
		
		CommonUtil.ajax({
			url:'/ForwardController/refuse',
			data:param ,
			callback:function(data){
				mini.alert("复核拒绝成功!","系统提示",function(value){
					//关闭并刷新
					setTimeout(function(){ top["win"].closeMenuTab() },100);
				});
				
			}
		});
			
	}



</script>


</html>