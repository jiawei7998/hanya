<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../../global.jsp"%> 
<html>
<head>
  <title>远期外汇买卖-交割列表</title>
  <script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
</head>
<body style="width:100%;height:100%;background:white">
	
	<fieldset>
		<legend><label>查询条件</label></legend>
			<div id="search_form"  >
				<input id="custNo" name="custNo" class="mini-textbox"  labelField="true"  label="客户代码：" style="width:310px;" emptyText="请输入客户代码" labelStyle="text-align:right;"/>
				<input id="custNm" name="custNm" class="mini-textbox"  labelField="true"  label="客户名称：" style="width:310px;" emptyText="请输入客户名称" labelStyle="text-align:right;"/>
				<span style="float:right;margin-right: 90px">
					<a id="search_btn" class="mini-button" style="display: none"   onclick="search1">查询</a>
					<a id="clear_btn" class="mini-button" style="display: none"    onClick="clear">清空</a>
				</span>
			</div>
		   
	</fieldset>
	<span style="margin:2px;display: block;">
		<!-- <a id="add_btn" class="mini-button" style="display: none"   onclick="add">新增</a> -->
		<a id="edit_btn" class="mini-button" style="display: none"   onclick="edit">修改</a>
		<a id="del_btn" class="mini-button" style="display: none"   onclick="del">删除</a>
		<a id="detail_btn" class="mini-button" style="display: none"    onclick="detail">查看详情</a>
		
		<div id = "approveType" name = "approveType" labelField="true" label=""  class="mini-checkboxlist" 
			value="approve" textField="text" valueField="id" multiSelect="false" style="float:right; margin-right: 50px;" 
			labelStyle="text-align:right;" 
			data="[{id:'handle',text:'经办'},{id:'temp_storage',text:'暂存'},{id:'wait_modify',text:'待修改'},{id:'wait_verify',text:'待复核'},{id:'efficient',text:'生效'}]" onvaluechanged="checkBoxValuechanged">
		</div>
	</span>
	
	<div class="mini-fit" id="fitId" style="margin-top: 2px;">
		<div id="fwdDelivery_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" 
		idField="fldNo"  pageSize="10" multiSelect="true" onRowdblclick="custDetail" sortMode="client"
		allowAlternating="true">
			<div property="columns" >
				<div type="indexcolumn" headerAlign="center"  align="center" width="40px">序号</div>
				<div type="checkcolumn"></div>
				<div field="jsyqNo" headerAlign="center"  align="center" width="160px" allowSort="true" dataType="int">业务编号</div>    
				<div field="brchNo" headerAlign="center" width="150px">机构号</div>                            
                <div field="custNo"  headerAlign="center" width="100px">客户代码</div>
                <div field="custNm"   headerAlign="center"  align="center" width="150px">客户名称</div>
               
				<!-- <div field="qaBank"  headerAlign="center"  align="center" width="180px" allowSort="true" dataType="int">资格银行代码</div>
                <div field="qabkNm"  headerAlign="center"  align="center" width="180px" >资格银行名称</div> -->
                <div field="jsyqTp"    headerAlign="center"  width="100px" >结汇/售汇</div> 
				<div field="jsxjTp"  headerAlign="center"  align="center" width="120px" allowSort="true" dataType="int">相关业务类型</div>
                <div field="yucyNo"    headerAlign="center"  width="100px" renderer="CommonUtil.dictRenderer" data-options="{dict:'Currency'}">原币种</div> 
                <div field="yuacNo"  headerAlign="center"  align="center" width="160px" >原账号</div>                                           
				<div field="mdcyNo"  headerAlign="center"  align="center" width="100px" allowSort="true" dataType="int" renderer="CommonUtil.dictRenderer" data-options="{dict:'Currency'}">目的币种</div>
                <div field="mdacNo"  headerAlign="center"  align="center" width="160px" >目的账号</div> 
                <div field="wBamts"  headerAlign="center"  align="center" width="160px" allowSort="true" dataType="int">交割总金额（外币）</div> 
				<div field="wbamBl"  headerAlign="center"  align="center"  width="160px" allowSort="true" dataType="int">未交割金额（外币）</div>
                <div field="wbamDo"    headerAlign="center"   width="160px" align="center" >本次交割金额（外币）</div>
                
                <!-- <div field="qaRate"    headerAlign="center"   width="160px" align="center" >资格行汇率</div> -->
                <div field="omyRat"    headerAlign="center"   width="160px" align="center" >原我行报客户汇率</div>
                <div field="oexRat"    headerAlign="center"   width="160px" align="center" >原总行平盘汇率</div>
                <div field="exRate"    headerAlign="center"   width="160px" align="center" >总行平盘汇率</div>
                <div field="myRate"    headerAlign="center"   width="160px" align="center" >我行报客户汇率</div>
                <div field="reRate"    headerAlign="center"   width="160px" align="center" >掉期远端汇率</div>
                <div field="curbAm"    headerAlign="center"   width="160px" align="center" >客户金额（人民币）</div>
                <div field="gnlsAm"    headerAlign="center"   width="160px" align="center" >盈亏金额（人民币）</div>
                <div field="whrbAm"    headerAlign="center"   width="160px" align="center" >我行金额（人民币）</div>
                <div field="jgType"    headerAlign="center"   width="160px" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'DeliveryType'}">交割方式</div>
                <div field="jgDate"    headerAlign="center"   width="160px" align="center" >固定交割日</div>
                <div field="jgDatf"    headerAlign="center"   width="160px" align="center" >择期交割日</div>
                <div field="jgDatt"    headerAlign="center"   width="160px" align="center" >至</div>

                <div field="naType"    headerAlign="center"   width="160px" align="center" >结售汇统计类型</div>
                <div field="naKnd2"    headerAlign="center"   width="160px" align="center" >结售汇统计报表类型</div>
                <div field="oqabTp"    headerAlign="center"   width="160px" align="center" >交易编码</div>
                <div field="naType"    headerAlign="center"   width="160px" align="center" >外汇局批件号</div>
                <div field="fuType"    headerAlign="center"   width="160px" align="center" >结汇用途</div>
               <!--  <div field="sbaoNo"    headerAlign="center"   width="160px" align="center" >申报号</div> -->
                <div field="remark"    headerAlign="center"   width="160px" align="center" >结汇详细用途</div>
                
				                                                                                                
			</div>
		</div>
     </div>
     <script type="text/javascript">
        mini.parse();

		$(document).ready(function(){//控制按钮显示
			$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
				//初始化为经办列表页面
				mini.get("approveType").setValue("handle");
				//经办列表页面,修改、删除按钮要隐藏
				/* mini.get("add_btn").setVisible(false); */
				mini.get("edit_btn").setVisible(false);
				mini.get("del_btn").setVisible(false);


				var grid = mini.get("fwdDelivery_grid");
				grid.on("beforeload", function (e) {
					e.cancel = true;
					var pageIndex = e.data.pageIndex;
					var pageSize = e.data.pageSize;
					search(pageSize, pageIndex);
				});
				search(grid.pageSize, 0);
			});
		});

        top["fwdDeliveryList"]=window;
        function getData(){
            var grid = mini.get("fwdDelivery_grid");
            var record =grid.getSelected(); 
            return record;
        }

		function search1(){
            var grid =mini.get("fwdDelivery_grid");
            search(grid.pageSize,0); 
        }

        function search(pageSize,pageIndex){
            var form =new mini.Form("search_form");
            form.validate();
            if (form.isValid() == false) return;//表单验证
            var data =form.getData();//获取表单数据
            data['pageNumber']=pageIndex+1;
			data['pageSize']=pageSize;
			data['sysDate']=sysDate;

			var url="";
			var approveType = mini.get("approveType").getValue();
			if(approveType == "handle"){//经办
				data.newStatCd='C';
				data.statCd='A';
				data.otherStatCd1='T';
				data.otherStatCd2='Z';
				data.otherStatCd3='W';
			
				url = "/ForwardController/searchPageOperate";

			}else if(approveType == "temp_storage"){//暂存
				data.statCd='C';
				url = "/ForwardController/searchPageTempStorage";

			}else if(approveType == "wait_modify"){//待修改
				data.statCd='C';
				url = "/ForwardController/searchPageWaitModify";

			}else if(approveType == "wait_verify"){//待复核
				data.statCd='C';
				url = "/ForwardController/searchPageWaitVerify";

			}else{//生效efficient
				data.statCd='C';
				url = "/ForwardController/searchPageEfficient";
			}

            var param = mini.encode(data); //序列化成JSON
               CommonUtil.ajax({
            	url:url,
                data:param,
                callback:function(data){
                        
                    var grid =mini.get("fwdDelivery_grid");
                    //设置分页
                    grid.setTotalCount(data.obj.total);
                    grid.setPageIndex(pageIndex);
                    grid.setPageSize(pageSize);
                    //设置数据
					grid.setData(data.obj.rows);

                }
            });
		}

        //新增
		function add(){
			var url = CommonUtil.baseWebPath() + "/trade/fxfwd/FwdDeliveryEdit.jsp?action=new";
			var tab = { id: "fwdDeliveryListAdd", name: "fwdDeliveryListAdd", text: "远期外汇买卖-交割新增", url: url,showCloseButton:true,parentId:top['win'].tabs.getActiveTab().name };
			top['win'].showTab(tab);
		}

		//修改
        function edit(){
			var type = mini.get("approveType").getValue();
			var url = "";
			if(type == "temp_storage"){//暂存
				url = CommonUtil.baseWebPath() + "/trade/fxfwd/FwdDeliveryEdit.jsp?action=edit&dataTp=temp";	
			}else if(type == "wait_modify"){//待修改
				url = CommonUtil.baseWebPath() + "/trade/fxfwd/FwdDeliveryEdit.jsp?action=edit&dataTp=modify";	
			}
            var grid = mini.get("fwdDelivery_grid");
            var  record =grid.getSelected();
            if(record){
                var tab = { id: "fwdDeliveryListEdit", name: "fwdDeliveryListEdit", text: "远期外汇买卖-交割修改", url: url,showCloseButton:true,parentId:top['win'].tabs.getActiveTab().name };
                top['win'].showTab(tab);
            }else{
                mini.alert("请选中一条记录!","系统提示"); 
            }
        }

        //查看详情
        function detail(){
			var type = mini.get("approveType").getValue();
			var url = "";
			if(type == "handle"){//经办
				url = CommonUtil.baseWebPath() + "/trade/fxfwd/FwdDeliveryEdit.jsp?action=detail&dataTp=handle";	
			}else if(type == "wait_verify"){//待复核
				url = CommonUtil.baseWebPath() + "/trade/fxfwd/FwdDeliveryEdit.jsp?action=detail&dataTp=verify";	
			}else if(type == "efficient"){//生效
				url = CommonUtil.baseWebPath() + "/trade/fxfwd/FwdDeliveryEdit.jsp?action=detail&dataTp=effi";	
			}

            var grid = mini.get("fwdDelivery_grid");
            var  record =grid.getSelected();
            if(record){
                
                var tab = { id: "fwdDeliveryListDetail", name: "fwdDeliveryListDetail", text: "远期外汇买卖-交割详情", url: url,showCloseButton:true,parentId:top['win'].tabs.getActiveTab().name };
                top['win'].showTab(tab);
            }else{
                mini.alert("请选中一条记录!","系统提示"); 
            }
        } 

        //清空
		function clear(){
			var form =new mini.Form("search_form");
			form.clear();
			search1();
			
		}

        //删除
		function del(){
			var grid = mini.get("fwdDelivery_grid");
            //获取选中行
            var rows =grid.getSelecteds();
            
            if (rows.length>0) {
                //把业务编号放在数组里
				var jsyqNos = new Array();
				for(var i=0;i<rows.length;i++){
					jsyqNos.push(rows[i].jsyqNo);
				}
                
                mini.confirm("您确认要删除?","系统警告",function(value){   
                    if (value == "ok"){   
                        CommonUtil.ajax({
                            url : '',
                            data : jsyqNos,
                            callback : function(data){
                                if("error.common.0000" == data.code){
                                    mini.alert("删除成功！","系统提示");
                                    var grid =mini.get("fwdDelivery_grid");
                                    search(grid.pageSize,0);
                                }
                            }
                        });
                    
                    }   
                });       
                    
            }else{
                mini.alert("请选择一条记录!","系统提示");
            }
		}

        function checkBoxValuechanged(){
			var type = mini.get("approveType").getValue();
			if(type == "handle"){//经办
				mini.get("del_btn").setVisible(false);
				/* mini.get("add_btn").setVisible(false); */
				mini.get("edit_btn").setVisible(false);
				mini.get("detail_btn").setVisible(true);
			}else if(type == "temp_storage"){//暂存
				/* mini.get("add_btn").setVisible(true); */
				mini.get("edit_btn").setVisible(true);
				mini.get("detail_btn").setVisible(false);
				mini.get("del_btn").setVisible(true);
			}else if(type == "wait_modify"){//待修改
				/* mini.get("add_btn").setVisible(false); */
				mini.get("edit_btn").setVisible(true);
				mini.get("detail_btn").setVisible(false);
				mini.get("del_btn").setVisible(false);
			}else if(type == "wait_verify"){//待复核
				/* mini.get("add_btn").setVisible(false); */
				mini.get("edit_btn").setVisible(false);
				mini.get("detail_btn").setVisible(true);
				mini.get("del_btn").setVisible(false);
			}else{//生效
				/* mini.get("add_btn").setVisible(false); */
				mini.get("edit_btn").setVisible(false);
				mini.get("detail_btn").setVisible(true);
				mini.get("del_btn").setVisible(false);
			}
			
			search1();
		}




     </script>
</body>
</html>