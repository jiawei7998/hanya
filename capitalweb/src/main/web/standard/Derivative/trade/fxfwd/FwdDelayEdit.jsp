<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../../global.jsp"%>
<html>
  <head>
  <title>远期-展期-维护</title>
  <script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
  </head>
  
  <body style="width:100%;height:100%;background:white">
		<div class="mini-splitter" style="width:100%;height:100%;">
			<div size="90%" showCollapseButton="true">
				<div class="mini-fit"  style="background:white" id="inquiry_form">
							<table  width="100%" class="mini-table">
								<tr>
                                    <td><input class="mini-textbox"  id ="jsyqNo" name="jsyqNo"   style="width:400px"labelField="true" label="业务编号"  emptyText="系统自动生成" labelStyle="width:130px" enabled="false"/></td>
									<td><input class="mini-buttonedit"   id="brchNo" name="brchNo"  style="width:400px" labelField="true" label="机构号" labelStyle="width:130px" emptyText=""  required="true"  /></td>
								</tr>
								<tr>
                                    <td><input class="mini-buttonedit"   id="custNo"  name="custNo" style="width:400px"  labelStyle="width:130px" labelField="true" label="客户代码" emptyText="" required="true" /></td>
                                    <td><input class="mini-textbox"  id="custNm"  name="custNm" style="width:400px" labelField="true" label="客户名称" labelStyle="width:130px" emptyText="同步反显" enabled="false"  /></td>
								</tr>
								

								<tr>
									<td><input class="mini-combobox" id="yucyNo"  name="yucyNo" style="width:400px" labelField="true" label="原币种"  labelStyle="width:130px" emptyText="" required="true"  data = "CommonUtil.serverData.dictionary.Currency" /></td>
                                    <td><input class="mini-buttonedit"  id="yuacNo"  name="yuacNo" style="width:400px" labelField="true" label="原账号"  labelStyle="width:130px" emptyText="" required="true" /></td>
                                </tr>
                                <tr>
									<td><input class="mini-combobox" id="mdcyNo"  name="mdcyNo" style="width:400px" labelField="true" label="目的币种"  labelStyle="width:130px" emptyText="" required="true"  data = "CommonUtil.serverData.dictionary.Currency" /></td>
                                    <td><input class="mini-buttonedit"  id="mdacNo"  name="mdacNo" style="width:400px" labelField="true" label="目的账号" labelStyle="width:130px"  emptyText="" required="true" /></td>
                                </tr>
								<tr>
                                    <td><input class="mini-textbox" id="wBamts"  name="wBamts" style="width:400px" labelField="true" label="交割总金额（外币）" labelStyle="width:130px" emptyText=""/></td>
                                    <td><input class="mini-textbox"  id="wbamBl"  name="wbamBl" style="width:400px" labelField="true" label="未交割金额（外币）" labelStyle="width:130px" emptyText="" required="true"  /></td>
                                </tr>

                                <tr>
									<td><input class="mini-combobox" id="jsyqTp"  name="jsyqTp" labelField="true" label="结汇/售汇" style="width:400px" labelStyle="width:130px" emptyText="" data = "CommonUtil.serverData.dictionary.JHSH"></td>
                                    <td><input class="mini-buttonedit" onbuttonclick="onInquiryPriceQuery" id="fdQvno"  name="fdQvno" style="width:400px" labelStyle="width:130px" labelField="true" label="询价业务编号" required="true"  emptyText=""/></td>
                                    
                                </tr>
                                <tr>
									<td><input class="mini-textbox"  id="myRate"  name="myRate" style="width:400px" labelField="true" label="新我行报客户汇率" labelStyle="width:130px" emptyText="同步反显" enabled="false" emptyText="" /></td>
                                    <td><input class="mini-textbox" id="exRate"  name="exRate" style="width:400px" labelField="true" label="新总行平盘汇率" labelStyle="width:130px" emptyText="同步反显" enabled="false" emptyText=""/></td>
                                    
								</tr>
								<tr>
									<td><input class="mini-textbox" id="omyRat"  name="omyRat" style="width:400px" labelField="true" label="原我行报客户汇率" labelStyle="width:130px"  enabled="false" emptyText=""/></td>
									<td><input class="mini-textbox"  id="oexRat"  name="oexRat" style="width:400px" labelField="true" label="原总行平盘汇率" labelStyle="width:130px"  enabled="false" emptyText="" /></td>
								</tr>
                                <tr>
                                    <td><input class="mini-textbox" id="neRate"  name="neRate" style="width:400px" labelField="true" label="掉期近端汇率"  onvaluechanged="onNeRateChanged" onvalidation="onNearRateValidation" required="true"  labelStyle="width:130px" emptyText=""/></td>
                                    <td><input class="mini-textbox" id="gnlsAm"  name="gnlsAm" style="width:400px" labelField="true" label="盈亏金额（人民币）" labelStyle="width:130px" emptyText=""/></td>
                                </tr>

                                <tr>
                                    <td><input class="mini-combobox" id="ojgTyp"  name="ojgTyp" style="width:400px" labelField="true" label="原交割方式" labelStyle="width:130px" emptyText="" data = "CommonUtil.serverData.dictionary.DeliveryType"/></td>
									<td><input class="mini-datepicker"  id="ojgDat"  name="ojgDat" style="width:400px" labelField="true" label="原固定交割日" labelStyle="width:130px" emptyText="" showOkButton="true" showClearButton="false"/></td>
									
                                </tr>
                                <tr>
                                    <td><input class="mini-datepicker" id="ojgDaf"  name="ojgDaf" style="width:400px" labelField="true" label="原择期交割日" labelStyle="width:130px" emptyText="" showOkButton="true" showClearButton="false"/></td>
                                    <td><input class="mini-datepicker" id="ojgDtt"  name="ojgDtt" style="width:400px" labelField="true" label="至" labelStyle="width:130px" emptyText="" showOkButton="true" showClearButton="false"/></td>
                                </tr>

                                <tr>
                                    <td><input class="mini-combobox" id="jgType"  name="jgType" style="width:400px" labelField="true" label="新交割方式" labelStyle="width:130px" onvaluechanged="jgTypeChange" required="true"  emptyText="" data = "CommonUtil.serverData.dictionary.DeliveryType"/></td>
									<td><input class="mini-datepicker"  id="jgDate"  name="jgDate" style="width:400px" labelField="true" label="新固定交割日" labelStyle="width:130px" required="true"  emptyText="" showOkButton="true" showClearButton="false"/></td>
									
                                </tr>
                                <tr>
                                    <td><input class="mini-datepicker" id="jgDatf"  name="jgDatf" style="width:400px" labelField="true" label="新择期交割日" onvaluechanged="minDateChange" required="true"  labelStyle="width:130px" emptyText="" showOkButton="true" showClearButton="false"/></td>
                                    <td><input class="mini-datepicker" id="jgDatt"  name="jgDatt" style="width:400px" labelField="true" label="至"  onvaluechanged="maxDateChange" required="true"  labelStyle="width:130px" emptyText="" showOkButton="true" showClearButton="false"/></td>
                                </tr>
                                <tr>
									
									<td><input class="mini-textarea"  id="remark"  name="remark" style="width:400px" labelField="true" label="备注" labelStyle="width:130px" emptyText="" /></td>
                                </tr>
                               
								
								
							</table>
						
				</div>
			</div>
			<div id="functionIds" showCollapseButton="true">
				<div style="margin-bottom:10px; margin-top:5px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_valid"   onclick="valid()">校验要素</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_save"          onclick="save">保存交易</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_verify"    enabled="false" onclick="verify">提交复核</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_refuse"        enabled="false" onclick="refuse">拒绝交易</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_checkTrade"        enabled="false"   onclick="checkTrade">复核交易</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_close"              onclick="close">关闭界面</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_tradeFee"     enabled="false" onclick="tradeFee">交易费用</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_tradeProof"      enabled="false" onclick="tradeProof">交易凭证</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_agencyFeeBtn"     enabled="false"    onclick="">会计传票</a></div>
			</div>      
		</div>


</body>

<script type="text/javascript">
    mini.parse();

    var url = window.location.search;
    var action = CommonUtil.getParam(url,"action");
    var dataTp = CommonUtil.getParam(url,"dataTp");

    $(document).ready(function(){
        init();
    });

    function init(){
		if(action == "new"){
			
		}else if(action == "edit" || action == "detail"){
            var form1 = new mini.Form("inquiry_form");
			var data=top["fwdDelayList"].getData();
			form1.setData(data);
			//未交割金额初始值与交割总金额相同
			var totalMoney = mini.get("wBamts").getValue();//获取交割总金额
			mini.get("wbamBl").setValue(totalMoney);//初始化未交割金额

			//buttonedit控件需要单独设置值
			//1.机构号
			mini.get("brchNo").setValue(data.brchNo);
			mini.get("brchNo").setText(data.brchNo);
			//2.客户代码
			mini.get("custNo").setValue(data.custNo);
			mini.get("custNo").setText(data.custNo);
			//3.原账号
			mini.get("yuacNo").setValue(data.yuacNo);
			mini.get("yuacNo").setText(data.yuacNo);
			//4.目的账号
			mini.get("mdacNo").setValue(data.mdacNo);
			mini.get("mdacNo").setText(data.mdacNo);
			if(action == "detail"){
				form1.setEnabled(false);
				if(dataTp == "verify"){//待复核
					mini.get("btn_valid").setEnabled(false);//检验要素
					mini.get("btn_save").setEnabled(false);//保存交易
					mini.get("btn_verify").setEnabled(false);//提交复核
					mini.get("btn_refuse").setEnabled(true);//拒绝交易
					mini.get("btn_checkTrade").setEnabled(true);//复核交易
					mini.get("btn_close").setEnabled(false);//关闭界面
					mini.get("btn_tradeFee").setEnabled(false);//交易费用
					mini.get("btn_tradeProof").setEnabled(false);//交易凭证
					mini.get("btn_agencyFeeBtn").setEnabled(false);//会计传票
				}else if(dataTp == "effi"){//生效
					mini.get("btn_valid").setEnabled(false);//检验要素
					mini.get("btn_save").setEnabled(false);//保存交易
					mini.get("btn_verify").setEnabled(false);//提交复核
					mini.get("btn_refuse").setEnabled(false);//拒绝交易
					mini.get("btn_checkTrade").setEnabled(false);//复核交易
					mini.get("btn_close").setEnabled(false);//关闭界面
					mini.get("btn_tradeFee").setEnabled(true);//交易费用
					mini.get("btn_tradeProof").setEnabled(true);//交易凭证
					mini.get("btn_agencyFeeBtn").setEnabled(true);//会计传票
				}else if(dataTp == "handle"){//经办
					mini.get("fdQvno").setEnabled(true);//询价业务编号可以修改
					mini.get("jgType").setEnabled(true);//新交割方式可以修改
					mini.get("neRate").setEnabled(true);//掉期近端汇率可以修改

					mini.get("btn_valid").setEnabled(false);//检验要素
					mini.get("btn_save").setEnabled(true);//保存交易
					mini.get("btn_verify").setEnabled(false);//提交复核
					mini.get("btn_refuse").setEnabled(false);//拒绝交易
					mini.get("btn_checkTrade").setEnabled(false);//复核交易
					mini.get("btn_close").setEnabled(false);//关闭界面
					mini.get("btn_tradeFee").setEnabled(false);//交易费用
					mini.get("btn_tradeProof").setEnabled(false);//交易凭证
					mini.get("btn_agencyFeeBtn").setEnabled(false);//会计传票
				}
			}else if(action == "edit"){
				form1.setEnabled(false);
				mini.get("fdQvno").setEnabled(true);//询价业务编号可以修改
				mini.get("jgType").setEnabled(true);//新交割方式可以修改
				mini.get("neRate").setEnabled(true);//掉期近端汇率可以修改
				if(dataTp == "modify"){//待修改
					mini.get("btn_valid").setEnabled(false);//检验要素
					mini.get("btn_save").setEnabled(false);//保存交易
					mini.get("btn_verify").setEnabled(true);//提交复核
					mini.get("btn_refuse").setEnabled(false);//拒绝交易
					mini.get("btn_checkTrade").setEnabled(false);//复核交易
					mini.get("btn_close").setEnabled(false);//关闭界面
					mini.get("btn_tradeFee").setEnabled(false);//交易费用
					mini.get("btn_tradeProof").setEnabled(false);//交易凭证
					mini.get("btn_agencyFeeBtn").setEnabled(false);//会计传票
				}
			}

        }
    }

    //询价业务编号的查询=>需要先选择 客户代码、币种（外币）、交割总金额（外币）、结汇/售汇
	function onInquiryPriceQuery(e){
		//客户代码
		var custNo = mini.get("custNo").getValue();
		//交割总金额（外币）
		var wBamts = mini.get("wBamts").getValue();
		//结售汇类型
		var jsyqTp = mini.get("jsyqTp").getValue();
		//原币种
		var yucyNo = mini.get("yucyNo").getValue();
		//目的币种
		var mdcyNo = mini.get("mdcyNo").getValue();

		if((custNo==null||custNo=="")  || (wBamts==null||wBamts=="")  || (jsyqTp==null||jsyqTp=="") || (yucyNo==null||yucyNo=="") || (mdcyNo==null||mdcyNo=="") ){
			mini.alert("客户代码、交割总金额、结售汇类型、原币种、目的币种都必须正确填入(选择)!");
			return;
		}

		var btnEdit = this;
		mini.open({
			url : CommonUtil.baseWebPath() +"/inquiry/fxfwd/FwdInquiryListMini.jsp",
			title : "询价业务编号选择",
			width : 900,
			height : 600,
			
			onload: function () {//弹出页面加载完成
				var iframe = this.getIFrameEl(); 
				var data = {};
				data = {"custNo":custNo,"yucyNo":yucyNo,"mdcyNo":mdcyNo,"jsyqTp":jsyqTp,"wBamts":wBamts};	
				//调用弹出页面方法进行初始化
				iframe.contentWindow.SetData(data); 
			},
			ondestroy : function(action) {	
				if (action == "ok") 
				{
					var iframe = this.getIFrameEl();
					var data = iframe.contentWindow.GetData();
					data = mini.clone(data); //必须
					if (data) {
						btnEdit.setValue(data.fDQVNO);
						btnEdit.setText(data.fDQVNO);
						mini.get("myRate").setValue(data.mYRATE);
						mini.get("exRate").setValue(data.eXRATE);
						btnEdit.focus();
					}

					
				}

			}
		});
		
		
	}


    

   
	//新交割方式改变时发生
	function jgTypeChange(){
		var jgType = mini.get("jgType").getValue();
		if(jgType == "1"){//固定交割
			mini.get("jgDate").setEnabled(true);//固定交割日
			mini.get("jgDatf").setEnabled(false);//择期交割日
			mini.get("jgDatt").setEnabled(false);//至
			mini.get("jgDatf").setValue("");
			mini.get("jgDatt").setValue("");
		}else if(jgType == "2"){//择期交割
			mini.get("jgDate").setEnabled(false);//固定交割日
			mini.get("jgDatf").setEnabled(true);//择期交割日
			mini.get("jgDatt").setEnabled(true);//至
			mini.get("jgDate").setValue("");
		}
	}

	//新择期交割日-开始日期
	function minDateChange(){
		////debugger
		var form = new mini.Form("#inquiry_form");
		var data = form.getData(true); //获取表单多个控件的数据
		
		if(CommonUtil.dateDiff(data.jgDatf,data.jgDatt) == 0){
			mini.alert("此日期不能等于结束择期交割日!","提示");
			mini.get("jgDatf").setValue("");
			return false;
		}

		if(CommonUtil.dateCompare(data.jgDatf,data.jgDatt)){
			mini.alert("此日期不能大于结束择期交割日!","提示");
			mini.get("jgDatf").setValue("");
			return false;
		}
		
	}
	//择期交割日-结束日期
	function maxDateChange(){
		////debugger
		var form = new mini.Form("#inquiry_form");
		var data = form.getData(true); //获取表单多个控件的数据
		if(data.jgDatt == null || data.jgDatt == ""){//至
			return false;
		}

		if(CommonUtil.dateDiff(data.jgDatf,data.jgDatt) == 0){
			mini.alert("此日期不能等于开始择期交割日!","提示");
			mini.get("jgDatt").setValue("");
			return false;
		}

		if(CommonUtil.dateCompare(data.jgDatf,data.jgDatt)){
			mini.alert("此日期不能小于开始择期交割日!","提示");
			mini.get("jgDatt").setValue("");
			return false;
		}
		
	}
	//掉期近端汇率  改变时发生
	function onNeRateChanged(){
		////获取交割总金额
		var totalMoney = mini.get("wBamts").getValue();
		//获取 原总行平盘汇率
		var oexRat = mini.get("oexRat").getValue();
		//获取 掉期近端汇率
		var neRate = mini.get("neRate").getValue();
		// 原总行平盘汇率-掉期近端汇率
		var subRate = CommonUtil.accSubtr(oexRat,neRate); 
		subRate = CommonUtil.accDiv(subRate,100);
		//计算出盈亏金额
		var subM =  CommonUtil.accMul(totalMoney,subRate);
		mini.get("gnlsAm").setValue(subM.toString());

	}
	//掉期近端汇率 的验证
	function onNearRateValidation(e){
		if (e.isValid) {
			if (isRate(e.value) == false) {
				e.errorText = "汇率整数位最多17位，小数位最多2位!";
				e.isValid = false;
			}
		}
	}

	function isRate(v) {
		var re = new RegExp("^[0-9]{1,17}([.]{1}[0-9]{1,2}){0,1}$");
		if (re.test(v)){
			return true;
		}else{
			return false;
		} 
		
	}

//***********************************事件*****************************************************************
    
    //检验要素
	function valid(){
		var form = new mini.Form("inquiry_form");
		form.validate();
	}


	 //保存
    function save(){
    	 
		
    	var form = new mini.Form("#inquiry_form");
		//验证表单
		form.validate();
		
		if(form.isValid() == false){
			mini.alert("表单填写错误,请确认!","提示信息")
			return;
		} 
		
		var data = form.getData(true); //获取表单多个控件的数据
        if(data.jsyqTp == "1"){
            data.jsyqTp = "结汇";
        }else if(data.jsyqTp == "2"){
            data.jsyqTp = "售汇";
        }
        data.userSt='S'
    	data.statCd='Z'
    	data.newStatCd='Z'
		var param = mini.encode(data); //序列化成JSON
		
		CommonUtil.ajax({
					url:'/ForwardController/save',
					data:param ,
					callback:function(data){
                        //配置业务编号
						mini.get("jsyqNo").setValue(data.desc);
            			mini.alert("保存成功!");
                        //提交按钮显示
            			mini.get("btn_verify").setEnabled(true);
						
					}
		});
    		
	}
    //保存
/*     function save(){
     	var form = new mini.Form("#inquiry_form");
		//验证表单
		form.validate();
		if(form.isValid() == false){
			mini.alert("表单填写错误,请确认!","提示信息")
			return;
		}
    		   
		var data = form.getData(true); //获取表单多个控件的数据
        if(data.jsyqTp == "1"){
            data.jsyqTp = "结汇";
        }else if(data.jsyqTp == "2"){
            data.jsyqTp = "售汇";
        }
		data.userSt='S'
		data.statCd='Z'
		data.newStatCd='Z'
		var param = mini.encode(data); //序列化成JSON
		
		CommonUtil.ajax({
					url:'/ForwardController/save',
					data:param ,
					callback:function(data){
                        //配置业务编号
						mini.get("jsyqNo").setValue(data.desc);
            			mini.alert("保存成功!");
                        //提交按钮显示
            			mini.get("btn_verify").setEnabled(true);
						
					}
		});
    		
	} */

    //提交
	function verify(){
       	var form = new mini.Form("#inquiry_form");
		//验证表单
		form.validate();
		if(form.isValid() == false){
			mini.alert("表单填写错误,请确认!","提示信息")
			return;
		}
			
		var data = form.getData(true); //获取表单多个控件的数据
		if(data.jsyqTp == "1"){
            data.jsyqTp = "结汇";
        }else if(data.jsyqTp == "2"){
            data.jsyqTp = "售汇";
        }
		data.userSt='Y'
		data.newStatCd='Z'
		var param = mini.encode(data); //序列化成JSON
		
		CommonUtil.ajax({
			url:'/ForwardController/commit',
			data:param,
			callback:function(data){
				mini.alert("提交成功!","系统提示",function(value){
                    //关闭并刷新
					setTimeout(function(){ top["win"].closeMenuTab() },100);
                });
				
			}
		});
			
	}

    //复核通过
    function checkTrade(){
       var form = new mini.Form("#inquiry_form");
		//验证表单
		form.validate();
		if(form.isValid() == false){
		mini.alert("表单填写错误,请确认!","提示信息")
		return;
		}
			
		var data = form.getData(true); //获取表单多个控件的数据
		if(data.jsyqTp == "1"){
            data.jsyqTp = "结汇";
        }else if(data.jsyqTp == "2"){
            data.jsyqTp = "售汇";
        }
		data.userSt='Y'
		data.chckSt='Y'
		data.statCd='Z'
		data.newStatCd='Z'
		
		var param = mini.encode(data); //序列化成JSON
		
		CommonUtil.ajax({
			url:'/ForwardController/checkTradeTr',
			data:param ,
			callback:function(data){
				mini.alert("复核通过!","系统提示",function(value){
					//关闭并刷新
					setTimeout(function(){ top["win"].closeMenuTab() },100);
				});
				
			}
		});
		
	}

    //拒绝
	function refuse(){
       var form = new mini.Form("#inquiry_form");
		//验证表单
		form.validate();
		if(form.isValid() == false){
		mini.alert("表单填写错误,请确认!","提示信息")
		return;
		}
			
		var data = form.getData(true); //获取表单多个控件的数据
		if(data.jsyqTp == "1"){
            data.jsyqTp = "结汇";
        }else if(data.jsyqTp == "2"){
            data.jsyqTp = "售汇";
        }
		data.userSt='Y'
		data.chckSt='N'
		data.newStatCd='Z'
		var param = mini.encode(data); //序列化成JSON
		
		CommonUtil.ajax({
			url:'/ForwardController/refuse',
			data:param ,
			callback:function(data){
				mini.alert("复核拒绝成功!","系统提示",function(value){
					//关闭并刷新
					setTimeout(function(){ top["win"].closeMenuTab() },100);
				});
				
			}
		});
			
	}



</script>


</html>