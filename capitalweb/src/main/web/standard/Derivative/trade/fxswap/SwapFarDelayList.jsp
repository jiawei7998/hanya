<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../../global.jsp"%> 
<html>
<head>
  <title>外汇掉期-远端展期列表</title>
  <script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
</head>
<body style="width:100%;height:100%;background:white">
	
	<fieldset>
		<legend><label>查询条件</label></legend>
			<div id="search_form"  >
				<input id="custNo" name="custNo" class="mini-textbox"  labelField="true"  label="客户代码：" style="width:310px;" emptyText="请输入客户代码" labelStyle="text-align:right;"/>
				<input id="custNm" name="custNm" class="mini-textbox"  labelField="true"  label="客户名称：" style="width:310px;" emptyText="请输入客户名称" labelStyle="text-align:right;"/>
				<span style="float:right;margin-right: 90px">
					<a id="search_btn" class="mini-button" style="display: none"   onclick="search1">查询</a>
					<a id="clear_btn" class="mini-button" style="display: none"    onClick="clear">清空</a>
				</span>
			</div>
		   
	</fieldset>
	<span style="margin:2px;display: block;">
		<a id="add_btn" class="mini-button" style="display: none"   onclick="add">新增</a>
		<a id="edit_btn" class="mini-button" style="display: none"   onclick="edit">修改</a>
		<a id="del_btn" class="mini-button" style="display: none"   onclick="del">删除</a>
		<a id="detail_btn" class="mini-button" style="display: none"    onclick="detail">查看详情</a>
		
		<div id = "approveType" name = "approveType" labelField="true" label=""  class="mini-checkboxlist" 
			value="approve" textField="text" valueField="id" multiSelect="false" style="float:right; margin-right: 50px;" 
			labelStyle="text-align:right;" 
			data="[{id:'handle',text:'经办'},{id:'temp_storage',text:'暂存'},{id:'wait_modify',text:'待修改'},{id:'wait_verify',text:'待复核'},{id:'efficient',text:'生效'}]" onvaluechanged="checkBoxValuechanged">
		</div>
	</span>
	
	<div class="mini-fit" id="fitId" style="margin-top: 2px;">
		<div id="swapFarDelay_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" 
		idField="fldNo"  pageSize="10" multiSelect="true"  sortMode="client"
		allowAlternating="true">
			<div property="columns" >
				<div type="indexcolumn" headerAlign="center"  align="center" width="40px">序号</div>
				<div type="checkcolumn"></div>
				<div field="fdseNo" headerAlign="center"  align="center" width="160px" >掉期业务编号</div>    
				<div field="brchNo" headerAlign="center" width="150px">机构号</div>                            
				<div field="custNo"  headerAlign="center" width="100px">客户代码</div>
                <div field="custNm"   headerAlign="center"  align="center" width="150px">客户名称</div>
                <!-- <div field="custAd"   headerAlign="center"  align="center" width="150px">客户地址</div> -->
                <div field="dlDate"   headerAlign="center"  align="center" width="150px">交易日期</div>
                <div field="fdqvNo"   headerAlign="center"  align="center" width="150px">询价业务编号</div> 
                <div field="spotSt"   headerAlign="center"  align="center" width="150px" renderer="CommonUtil.dictRenderer" data-options="{dict:'JHSH'}">近端状态（外币）</div>
                <div field="forwSt"   headerAlign="center"  align="center" width="150px" renderer="CommonUtil.dictRenderer" data-options="{dict:'JHSH'}">远端状态（外币）</div>
                <div field="ybcyNo"   headerAlign="center"  align="center" width="150px">近端买入币种</div>
                <div field="bycyNo"   headerAlign="center"  align="center" width="150px">远端买入币种</div>
                <div field="yscyNo"   headerAlign="center"  align="center" width="150px">近端卖出币种</div>
                <div field="slcyNo"   headerAlign="center"  align="center" width="150px">远端卖出币种</div>
                <div field="trAmts"   headerAlign="center"  align="center" width="150px">交易金额（外币）</div>

                <div field="oSpotf"   headerAlign="center"  align="center" width="150px">原近端对客汇率</div>
                <div field="oforWf"   headerAlign="center"  align="center" width="150px">原远端对客汇率</div>
                <div field="oTram1"   headerAlign="center"  align="center" width="150px">原近端人民币金额</div>
                <div field="oTram2"   headerAlign="center"  align="center" width="150px">原远端人民币金额</div>

                <div field="spotFr"   headerAlign="center"  align="center" width="150px">近端对客汇率</div>
                <div field="forwFr"   headerAlign="center"  align="center" width="150px">远端对客汇率</div>
                <div field="trAmt1"   headerAlign="center"  align="center" width="150px">近端人民币金额</div>
                <div field="trAmt2"   headerAlign="center"  align="center" width="150px">远端人民币金额</div>
                <div field="slacNo"   headerAlign="center"  align="center" width="150px">近端卖出/远端买入账号</div>
                <div field="byacNo"   headerAlign="center"  align="center" width="150px">近端买入/远端卖出账号</div>
				
				<div field="spotDt"   headerAlign="center"  align="center" width="150px">近端交割日</div>
				<div field="oForwD"   headerAlign="center"  align="center" width="150px">原远端交割日</div>
                <div field="forwDt"   headerAlign="center"  align="center" width="150px">新远端交割日</div>
                
                <div field="tranMo"    headerAlign="center"   width="160px" align="center" >备注</div>



				                                                                                                
			</div>
		</div>
	 </div> 
	<script type="text/javascript">
		mini.parse();

		$(document).ready(function(){
			//控制按钮显示
			$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
				//初始化为经办列表页面
				mini.get("approveType").setValue("handle");
				//经办列表页面,新增修改删除按钮要隐藏
				mini.get("add_btn").setVisible(false);
				mini.get("edit_btn").setVisible(false);
				mini.get("del_btn").setVisible(false);

				var grid = mini.get("swapFarDelay_grid");
				grid.on("beforeload", function (e) {
					e.cancel = true;
					var pageIndex = e.data.pageIndex;
					var pageSize = e.data.pageSize;
					search(pageSize, pageIndex);
				});
				search(grid.pageSize, 0);
			});
		});

		top["swapFarDelayList"]=window;
        function getData(){
            var grid = mini.get("swapFarDelay_grid");
            var record =grid.getSelected(); 
            return record;
        }

		function search1(){
            var grid =mini.get("swapFarDelay_grid");
            search(grid.pageSize,0); 
        }

		function search(pageSize,pageIndex){
            var form =new mini.Form("search_form");
            form.validate();
            if (form.isValid() == false) return;//表单验证
            var data =form.getData();//获取表单数据
            data['pageNumber']=pageIndex+1;
			data['pageSize']=pageSize;

			var url="";
			var approveType = mini.get("approveType").getValue();
			
            if(approveType == "handle"){//经办
				data.newStatCd='Z'
				data.statCd='A';
				data.otherStatCd1='T';
				data.otherStatCd2='C';
				data.otherStatCd3='W';
				data.otherStatCd4='Y';
				url = "/SwapController/searchPageOperate";

			}else if(approveType == "temp_storage"){//暂存
				data.statCd='Z'
				url = "/SwapController/searchPageTempStorage";

			}else if(approveType == "wait_modify"){//待修改
				data.statCd='Z'
				url = "/SwapController/searchPageWaitModify";

			}else if(approveType == "wait_verify"){//待复核
				data.statCd='Z'
				url = "/SwapController/searchPageWaitVerify";

			}else{//生效efficient
				data.statCd='Z'
				url = "/SwapController/searchPageEfficient";
			}

            var param = mini.encode(data); //序列化成JSON
               CommonUtil.ajax({
            	url:url,
                data:param,
                callback:function(data){
                        
                    var grid =mini.get("swapFarDelay_grid");
                    //设置分页
                    grid.setTotalCount(data.obj.total);
                    grid.setPageIndex(pageIndex);
                    grid.setPageSize(pageSize);
                    //设置数据
					grid.setData(data.obj.rows);
					var rows = grid.getData();
					for(var i=0,l=rows.length;i<l;i++){
						var row = rows[i];
						grid.updateRow(row,{spotFr:"",forwFr:"",trAmt1:"",trAmt2:"",forwDt:"",
											oSpotf:data.obj.rows[i].spotFr,oforWf:data.obj.rows[i].forwFr,
											oTram1:data.obj.rows[i].trAmt1,oTram2:data.obj.rows[i].trAmt2,
											oForwD:data.obj.rows[i].forwDt});
						
					}
					
                }
            });
		} 

		
		//新增
		function add(){
			var url = CommonUtil.baseWebPath() + "/trade/fxswap/SwapFarDelayEdit.jsp?action=new";
			var tab = { id: "swapFarDelayListAdd", name: "swapFarDelayListAdd", text: "外汇掉期-远端展期新增", url: url,showCloseButton:true,parentId:top['win'].tabs.getActiveTab().name };
			top['win'].showTab(tab);
		}

		//修改
        function edit(){
			var type = mini.get("approveType").getValue();
			var url = "";
			if(type == "temp_storage"){//暂存
				url = CommonUtil.baseWebPath() + "/trade/fxswap/SwapFarDelayEdit.jsp?action=edit&dataTp=temp";	
			}else if(type == "wait_modify"){//待修改
				url = CommonUtil.baseWebPath() + "/trade/fxswap/SwapFarDelayEdit.jsp?action=edit&dataTp=modify";	
			}
            var grid = mini.get("swapFarDelay_grid");
            var  record =grid.getSelected();
            if(record){
                var tab = { id: "swapFarDelayListEdit", name: "swapFarDelayListEdit", text: "外汇掉期-远端展期修改", url: url,showCloseButton:true,parentId:top['win'].tabs.getActiveTab().name };
                top['win'].showTab(tab);
            }else{
                mini.alert("请选中一条记录!","系统提示"); 
            }
        }

		//查看详情
        function detail(){
			var type = mini.get("approveType").getValue();
			var url = "";
			if(type == "handle"){//经办
				url = CommonUtil.baseWebPath() + "/trade/fxswap/SwapFarDelayEdit.jsp?action=detail&dataTp=handle";	
			}else if(type == "wait_verify"){//待复核
				url = CommonUtil.baseWebPath() + "/trade/fxswap/SwapFarDelayEdit.jsp?action=detail&dataTp=verify";	
			}else if(type == "efficient"){//生效
				url = CommonUtil.baseWebPath() + "/trade/fxswap/SwapFarDelayEdit.jsp?action=detail&dataTp=effi";	
			}

            var grid = mini.get("swapFarDelay_grid");
            var  record =grid.getSelected();
            if(record){
                
                var tab = { id: "swapFarDelayListDetail", name: "swapFarDelayListDetail", text: "外汇掉期-远端展期详情", url: url,showCloseButton:true,parentId:top['win'].tabs.getActiveTab().name };
                top['win'].showTab(tab);
            }else{
                mini.alert("请选中一条记录!","系统提示"); 
            }
        }

		//清空
		function clear(){
			var form =new mini.Form("search_form");
			form.clear();
			search1();
			
		}


		//删除
		function del(){
			var grid = mini.get("swapFarDelay_grid");
            //获取选中行
            var rows =grid.getSelecteds();
            
            if (rows.length>0) {
                //把业务编号放在数组里
				var jsyqNos = new Array();
				for(var i=0;i<rows.length;i++){
					jsyqNos.push(rows[i].jsyqNo);
				}
                
                mini.confirm("您确认要删除?","系统警告",function(value){   
                    if (value == "ok"){   
                        CommonUtil.ajax({
                            url : '/SwapController/delete',
                            data : jsyqNos,
                            callback : function(data){
                                if("error.common.0000" == data.code){
                                    mini.alert("删除成功！","系统提示");
                                    var grid =mini.get("swapFarDelay_grid");
                                    search(grid.pageSize,0);
                                }
                            }
                        });
                    
                    }   
                });       
                    
            }else{
                mini.alert("请选择一条记录!","系统提示");
            }
		}

		function checkBoxValuechanged(){
			var type = mini.get("approveType").getValue();
			if(type == "handle"){//经办
				mini.get("add_btn").setVisible(false);
				mini.get("edit_btn").setVisible(false);
				mini.get("detail_btn").setVisible(true);
				mini.get("del_btn").setVisible(false);
			}else if(type == "temp_storage"){//暂存
				mini.get("add_btn").setVisible(true);
				mini.get("edit_btn").setVisible(true);
				mini.get("detail_btn").setVisible(false);
				mini.get("del_btn").setVisible(true);
			}else if(type == "wait_modify"){//待修改
				mini.get("add_btn").setVisible(false);
				mini.get("edit_btn").setVisible(true);
				mini.get("detail_btn").setVisible(false);
				mini.get("del_btn").setVisible(false);
			}else if(type == "wait_verify"){//待复核
				mini.get("add_btn").setVisible(false);
				mini.get("edit_btn").setVisible(false);
				mini.get("detail_btn").setVisible(true);
				mini.get("del_btn").setVisible(false);
			}else{//生效
				mini.get("add_btn").setVisible(false);
				mini.get("edit_btn").setVisible(false);
				mini.get("detail_btn").setVisible(true);
				mini.get("del_btn").setVisible(false);
			}
			
			search1();
		}

    	
       
       
    </script>
</body>
</html>
