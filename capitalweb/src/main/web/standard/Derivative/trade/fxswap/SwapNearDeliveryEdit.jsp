<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../../global.jsp"%>
<html>
  <head>
  <title>外汇掉期-近端交割维护</title>
  <script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
  </head>
  
  <body style="width:100%;height:100%;background:white">
		<div class="mini-splitter" style="width:100%;height:100%;">
			<div size="90%" showCollapseButton="true">
				<div class="mini-fit"  style="background:white" id="fwdinfo">
						<fieldset class="mini-fieldset title">
							<legend>外汇掉期-近端交割</legend>
							<table  width="100%" class="mini-table">
								<tr>
									<td><input class="mini-buttonedit"  onbuttonclick="onInsQuery" id="brchNo" name="brchNo"  style="width:400px" labelField="true" label="机构号" labelStyle="width:130px;" emptyText=""  /></td>
									<td><input class="mini-buttonedit" onbuttonclick="onCustQuery"  id="custNo"  name="custNo" style="width:400px"  labelField="true" label="客户代码" required="true"   labelStyle="width:130px;" emptyText="" /></td>
								</tr>
								
								<tr>
									<td><input class="mini-textbox" id="custNm"  name="custNm" labelField="true" label="客户名称" style="width:400px"  emptyText="同步反显" labelStyle="width:130px;" enabled="false"></td>
									<td><input class="mini-textbox" id="custAd"  name="custAd" labelField="true" label="客户地址" style="width:400px" emptyText="同步反显" labelStyle="width:130px;" enabled="false"></td>
                                </tr>
                                <tr>
                                    <td><input class="mini-textbox" id="fdseNo"  name="fdseNo" style="width:400px" labelField="true" labelStyle="width:130px;" label="掉期业务编号"  emptyText=""/></td>
                                    <td><input class="mini-buttonedit" onbuttonclick="onInquiryPriceQuery" id="fdqvNo"  name="fdqvNo" style="width:400px" labelField="true" labelStyle="width:130px;" label="询价业务编号"  required="true"   emptyText=""/></td>
                                </tr>
								<tr>
                                    <td><input class="mini-combobox" id="spotSt"  name="spotSt" style="width:400px" labelField="true" label="近端状态（外币）"  labelStyle="width:130px;" required="true"   emptyText="" data = "CommonUtil.serverData.dictionary.JHSH"/></td>
                                    <td><input class="mini-combobox" id="forwSt"  name="forwSt" style="width:400px" labelField="true" label="远端状态（外币）"  labelStyle="width:130px;" required="true"   emptyText="" data = "CommonUtil.serverData.dictionary.JHSH"/></td>
									
                                </tr>
                                <tr>
                                    <td><input class="mini-combobox" id="bycyNo"  name="bycyNo" style="width:400px" labelField="true" label="近端买入币种"  labelStyle="width:130px;" required="true"  emptyText="" data = "CommonUtil.serverData.dictionary.Currency" /></td>
                                    <td><input class="mini-combobox" id="ybcyNo"  name="ybcyNo" style="width:400px" labelField="true" label="远端买入币种"  labelStyle="width:130px;" required="true"  emptyText="" data = "CommonUtil.serverData.dictionary.Currency" /></td>
                                </tr>
                                <tr>
                                    <td><input class="mini-combobox" id="slcyNo"  name="slcyNo" style="width:400px" labelField="true" label="近端卖出币种"  labelStyle="width:130px;" required="true"  emptyText="" data = "CommonUtil.serverData.dictionary.Currency" /></td>
                                    <td><input class="mini-combobox" id="yscyNo"  name="yscyNo" style="width:400px" labelField="true" label="远端卖出币种"  labelStyle="width:130px;" required="true"  emptyText="" data = "CommonUtil.serverData.dictionary.Currency" /></td>
                                </tr>
                                <tr>
									<td><input class="mini-datepicker" id="dlDate"  name="dlDate" format="yyyy-MM-dd" style="width:400px" labelField="true" label="交易日期"  labelStyle="width:130px;"  required="true"  emptyText="" format="yyyy-MM-dd" showOkButton="true" showClearButton="false"/></td>
                                    <td><input class="mini-textbox" id="trAmts"  name="trAmts" style="width:400px" labelField="true" label="交易金额（外币）"  labelStyle="width:130px;" required="true"  emptyText=""/></td>
                                </tr>
                                <tr>
                                    <td><input class="mini-textbox" id="spotFr"  name="spotFr" style="width:400px" labelField="true" label="近端对客汇率"  labelStyle="width:130px;" required="true"   emptyText="" enabled="false"/></td>
                                    <td><input class="mini-textbox" id="forwFr"  name="forwFr" style="width:400px" labelField="true" label="远端对客汇率"  labelStyle="width:130px;" required="true"   emptyText="" enabled="false"/></td>
								</tr>
								<tr>
                                    <td><input class="mini-textbox" id="trAmt1"  name="trAmt1" style="width:400px" labelField="true" label="近端人民币金额" labelStyle="width:130px;" required="true"  emptyText="" /></td>
                                    <td><input class="mini-textbox" id="trAmt2"  name="trAmt2" style="width:400px" labelField="true" label="远端人民币金额" labelStyle="width:130px;" required="true"  emptyText="" /></td>
								</tr>
								<tr>
                                    <td><input class="mini-textbox" id="slacNo"  name="slacNo" style="width:400px" labelField="true" label="近端卖出/远端买入账号" labelStyle="width:130px;" required="true"  emptyText="" /></td>
                                    <td><input class="mini-textbox" id="byacNo"  name="byacNo" style="width:400px" labelField="true" label="近端买入/远端卖出账号" labelStyle="width:130px;" required="true"  emptyText="" /></td>
                                </tr>
								<tr>
									<td><input class="mini-datepicker" id="spotDt"  name="spotDt" style="width:400px" labelField="true" label="近端交割日"  labelStyle="width:130px;" required="true"   emptyText=""  showOkButton="true" showClearButton="false"/></td>
									<td><input class="mini-datepicker"  id="forwDt"  name="forwDt" style="width:400px" labelField="true" label="远端交割日"  labelStyle="width:130px;" required="true"   emptyText=""  showOkButton="true" showClearButton="false"/></td>
								</tr>
								
							</table>
						</fieldset>
						<fieldset class="mini-fieldset title" style="margin-top:20px;">
							<legend>额度-保证金信息</legend>
							<table  width="100%" class="mini-table">
								<tr>
									<td><input class="mini-combobox"  id="qtCyno"  name="qtCyno" style="width:400px" labelField="true" label="占用额度币种"  labelStyle="width:130px;" emptyText="" data = "CommonUtil.serverData.dictionary.Currency"/></td>
									<td><input class="mini-textbox" id="qTamts"  name="qTamts" style="width:400px" labelField="true" label="金额"  labelStyle="width:130px;" emptyText="" /></td>
								</tr>
								
								<tr>
									<td><input class="mini-buttonedit" id="mgAcno"  name="mgAcno" style="width:400px" labelField="true" label="冻结编号一"  labelStyle="width:130px;" emptyText="" /></td>
									<td><input class="mini-textbox" id="suAcno"  name="suAcno" style="width:400px" labelField="true" label="保证金账号"  labelStyle="width:130px;" emptyText="同步回显" enabled="false"/></td>
								</tr>
								
								<tr>
									<td><input class="mini-combobox" id="mgAccy"  name="mgAccy" style="width:400px" labelField="true" label="币种"  labelStyle="width:130px;" emptyText="" data = "CommonUtil.serverData.dictionary.Currency"/></td>
									<td><input class="mini-textbox"  id="mgAcam"  name="mgAcam"  style="width:400px"labelField="true" label="金额"  labelStyle="width:130px;" emptyText="" /></td>
								</tr>
								<tr>
									<td><input class="mini-textarea"  id="tranMo"  name="tranMo"  style="width:400px"labelField="true" label="备注"  labelStyle="width:130px;" emptyText="" /></td>
								</tr>
							</table>
						</fieldset>
				</div>
			</div>
			<div id="functionIds" showCollapseButton="true">
				<div style="margin-bottom:10px; margin-top:50px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_valid"   onclick="valid()">校验要素</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_save"          onclick="save">保存交易</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_verify"    enabled="false" onclick="verify">提交复核</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_refuse"        enabled="false" onclick="refuse">拒绝交易</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_checkTrade"        enabled="false"   onclick="checkTrade">复核交易</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_close"              onclick="close">关闭界面</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_tradeFee"     enabled="false" onclick="tradeFee">交易费用</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_tradeProof"      enabled="false" onclick="tradeProof">交易凭证</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_agencyFeeBtn"     enabled="false"    onclick="">会计传票</a></div>
			</div>      
		</div>


   
	
<script type="text/javascript">
     
	mini.parse();

	var url = window.location.search;
    var action = CommonUtil.getParam(url,"action");
	var dataTp = CommonUtil.getParam(url,"dataTp");

	$(document).ready(function(){
        init();
    });

	function init(){
		if(action == "new"){
			
		}else if(action == "edit" || action == "detail"){
			var form1 = new mini.Form("fwdinfo");
			var data=top["swapNearDeliveryList"].getData();
			form1.setData(data);
			//buttonedit控件需要单独设置值
			//1.机构号
			mini.get("brchNo").setValue(data.brchNo);
			mini.get("brchNo").setText(data.brchNo);
			//2.客户代码
			mini.get("custNo").setValue(data.custNo);
			mini.get("custNo").setText(data.custNo);
			
			//3.询价业务编号
			mini.get("fdqvNo").setValue(data.fdqvNo);
			mini.get("fdqvNo").setText(data.fdqvNo);
			//4.冻结编号一
			mini.get("mgAcno").setValue(data.mgAcno);
			mini.get("mgAcno").setText(data.mgAcno);
			

			if(action == "detail"){
				form1.setEnabled(false);
				if(dataTp == "verify"){//待复核
					mini.get("btn_valid").setEnabled(false);//检验要素
					mini.get("btn_save").setEnabled(false);//保存交易
					mini.get("btn_verify").setEnabled(false);//提交复核
					mini.get("btn_refuse").setEnabled(true);//拒绝交易
					mini.get("btn_checkTrade").setEnabled(true);//复核交易
					mini.get("btn_close").setEnabled(false);//关闭界面
					mini.get("btn_tradeFee").setEnabled(false);//交易费用
					mini.get("btn_tradeProof").setEnabled(false);//交易凭证
					mini.get("btn_agencyFeeBtn").setEnabled(false);//会计传票
				}else if(dataTp == "effi"){//生效
					mini.get("btn_valid").setEnabled(false);//检验要素
					mini.get("btn_save").setEnabled(false);//保存交易
					mini.get("btn_verify").setEnabled(false);//提交复核
					mini.get("btn_refuse").setEnabled(false);//拒绝交易
					mini.get("btn_checkTrade").setEnabled(false);//复核交易
					mini.get("btn_close").setEnabled(false);//关闭界面
					mini.get("btn_tradeFee").setEnabled(true);//交易费用
					mini.get("btn_tradeProof").setEnabled(true);//交易凭证
					mini.get("btn_agencyFeeBtn").setEnabled(true);//会计传票
				}else if(dataTp == "handle"){//经办
					//mini.get("wbamDo").setEnabled(false);//本次交割金额可以修改

					mini.get("btn_valid").setEnabled(false);//检验要素
					mini.get("btn_save").setEnabled(true);//保存交易
					mini.get("btn_verify").setEnabled(false);//提交复核
					mini.get("btn_refuse").setEnabled(false);//拒绝交易
					mini.get("btn_checkTrade").setEnabled(false);//复核交易
					mini.get("btn_close").setEnabled(false);//关闭界面
					mini.get("btn_tradeFee").setEnabled(false);//交易费用
					mini.get("btn_tradeProof").setEnabled(false);//交易凭证
					mini.get("btn_agencyFeeBtn").setEnabled(false);//会计传票
				}
			}else if(action == "edit"){
                form1.setEnabled(false);
				if(dataTp == "modify"){//待修改
					mini.get("btn_valid").setEnabled(false);//检验要素
					mini.get("btn_save").setEnabled(false);//保存交易
					mini.get("btn_verify").setEnabled(true);//提交复核
					mini.get("btn_refuse").setEnabled(false);//拒绝交易
					mini.get("btn_checkTrade").setEnabled(false);//复核交易
					mini.get("btn_close").setEnabled(false);//关闭界面
					mini.get("btn_tradeFee").setEnabled(false);//交易费用
					mini.get("btn_tradeProof").setEnabled(false);//交易凭证
					mini.get("btn_agencyFeeBtn").setEnabled(false);//会计传票
				}
			}

		}
	}

	//保存
    function save(){
     	var form = new mini.Form("#fwdinfo");
		//验证表单
		form.validate();
		if(form.isValid() == false){
			mini.alert("表单填写错误,请确认!","提示信息")
			return;
		}
    		   
		var data = form.getData(true); //获取表单多个控件的数据
        data.userSt='S'
        data.statCd='C'
        data.newStatCd='C'
		var param = mini.encode(data); //序列化成JSON
		
		CommonUtil.ajax({
					url:'/SwapController/save',
					data:param ,
					callback:function(data){
						mini.get("fdseNo").setValue(data.desc);
            			mini.alert("保存成功!");
            			mini.get("btn_verify").setEnabled(true);
						
					}
		});
    		
	}

	//机构的查询
	function onInsQuery(e) {
		var btnEdit = this;
		mini.open({
			url : CommonUtil.baseWebPath() +"InterBank/mini_system/MiniInstitutionSelectManages.jsp",
			title : "机构选择",
			width : 700,
			height : 600,
			ondestroy : function(action) {	
				if (action == "ok") 
				{
					var iframe = this.getIFrameEl();
					var data = iframe.contentWindow.GetData();
					data = mini.clone(data); //必须
					if (data) {
						btnEdit.setValue(data.instId);
						btnEdit.setText(data.instId);
						btnEdit.focus();
					}

					
				}

			}
		});
	}

	//客户的查询
	function onCustQuery(e) {
		var btnEdit = this;
		mini.open({
			url : CommonUtil.baseWebPath() +"/base/MiniCPManageYQ.jsp",
			title : "客户选择",
			width : 700,
			height : 600,
			ondestroy : function(action) {	
				if (action == "ok") 
				{
					var iframe = this.getIFrameEl();
					var data = iframe.contentWindow.GetData();
					data = mini.clone(data); //必须
					if (data) {
						btnEdit.setValue(data.party_id);
						btnEdit.setText(data.party_id);
						
						mini.get("custNm").setValue(data.party_name);
						//有联系人
						if(data.constact_list.length>0){
							for(var i=0;i<data.constact_list.length;i++){
								if(data.constact_list[i].address){//有联系人，还有地址
									mini.get("custAd").setValue(data.constact_list[i].address);
									return;
								}else{//有联系人，没有地址
									mini.get("custAd").setValue("");
								}
							}
							
						}else{//没有联系人
							mini.get("custAd").setValue("");
						}

						btnEdit.focus();
					}

					
				}

			}
		});
	}

	

	

	

	
	//询价业务编号的查询=>需要先选择 客户代码、币种（外币）、交割总金额（外币）、结汇/售汇
	function onInquiryPriceQuery(e){
		//客户代码
		var custNo = mini.get("custNo").getValue();
		//交割总金额（外币）
		var wBamts = mini.get("wBamts").getValue();
		//结售汇类型
		var jsyqTp = mini.get("jsyqTp").getValue();
		//原币种
		var yucyNo = mini.get("yucyNo").getValue();
		//目的币种
		var mdcyNo = mini.get("mdcyNo").getValue();

		if((custNo==null||custNo=="")  || (wBamts==null||wBamts=="")  || (jsyqTp==null||jsyqTp=="") || (yucyNo==null||yucyNo=="") || (mdcyNo==null||mdcyNo=="") ){
			mini.alert("客户代码、交割总金额、结售汇类型、原币种、目的币种都必须正确填入(选择)!");
			return;
		}

		var btnEdit = this;
		mini.open({
			url : CommonUtil.baseWebPath() +"/inquiry/fxfwd/FwdInquiryListMini.jsp",
			title : "询价业务编号选择",
			width : 700,
			height : 600,
			
			onload: function () {//弹出页面加载完成
				var iframe = this.getIFrameEl(); 
				var data = {};
				data = {"custNo":custNo,"yucyNo":yucyNo,"mdcyNo":mdcyNo,"jsyqTp":jsyqTp,"wBamts":wBamts};	
				//调用弹出页面方法进行初始化
				iframe.contentWindow.SetData(data); 
			},
			ondestroy : function(action) {	
				if (action == "ok") 
				{
					var iframe = this.getIFrameEl();
					var data = iframe.contentWindow.GetData();
					data = mini.clone(data); //必须
					if (data) {
						btnEdit.setValue(data.fdqvNo);
						btnEdit.setText(data.fdqvNo);
						mini.get("myRate").setValue(data.mYRATE);
						mini.get("exRate").setValue(data.eXRATE);
						btnEdit.focus();
					}

					
				}

			}
		});
		
		
	}

	//检验要素
	function valid(){
		var form = new mini.Form("fwdinfo");
		form.validate();
	}

	//提交
	function verify(){
       	var form = new mini.Form("#fwdinfo");
		//验证表单
		form.validate();
		if(form.isValid() == false){
			mini.alert("表单填写错误,请确认!","提示信息")
			return;
		}
			
		var data = form.getData(true); //获取表单多个控件的数据
		data.userSt='Y'
		data.newStatCd='C'
		var param = mini.encode(data); //序列化成JSON
		
		CommonUtil.ajax({
			url:'/SwapController/commit',
			data:param,
			callback:function(data){
				mini.alert("提交成功!","系统提示",function(value){
                    //关闭并刷新
					setTimeout(function(){ top["win"].closeMenuTab() },100);
                });
				
			}
		});
			
	}

	//复核通过
    function checkTrade(){
       var form = new mini.Form("#fwdinfo");
		//验证表单
		form.validate();
		if(form.isValid() == false){
		mini.alert("表单填写错误,请确认!","提示信息")
		return;
		}
			
		var data = form.getData(true); //获取表单多个控件的数据
		data.userSt='Y'
		data.chckSt='Y'
		data.statCd='C'
		data.newStatCd='C'
		var param = mini.encode(data); //序列化成JSON
		
		CommonUtil.ajax({
			url:'/SwapController/checkTradeTr',
			data:param ,
			callback:function(data){
				mini.alert("复核通过!","系统提示",function(value){
                    //关闭并刷新
					setTimeout(function(){ top["win"].closeMenuTab() },100);
                });
				
			}
		});
		
	}

	//拒绝
	function refuse(){
       var form = new mini.Form("#fwdinfo");
		//验证表单
		form.validate();
		if(form.isValid() == false){
		mini.alert("表单填写错误,请确认!","提示信息")
		return;
		}
			
		var data = form.getData(true); //获取表单多个控件的数据
		data.userSt='Y'
		data.chckSt='N'
		data.newStatCd='C'
		var param = mini.encode(data); //序列化成JSON
		
		CommonUtil.ajax({
			url:'/SwapController/refuse',
			data:param ,
			callback:function(data){
				mini.alert("复核拒绝成功!","系统提示",function(value){
                    //关闭并刷新
					setTimeout(function(){ top["win"].closeMenuTab() },100);
                });
				
			}
		});
			
	}

	


	
	


	
</script>
</body>
</html>
