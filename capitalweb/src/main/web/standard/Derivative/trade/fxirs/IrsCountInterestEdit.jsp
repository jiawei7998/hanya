<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../../global.jsp"%>
<html>
  <head>
  <title>利率互换-计息维护</title>
  <script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
  </head>
  
  <body style="width:100%;height:100%;background:white">
		<div class="mini-splitter" style="width:100%;height:100%;">
			<div size="90%" showCollapseButton="true">
				<div class="mini-fit"  style="background:white" id="fwdinfo">
                    <fieldset>
                        <legend>业务录入</legend>
                        <table  width="100%" class="mini-table">
                            <tr>
                                <td><input class="mini-textbox"  id="ltisNo"  name="ltisNo" style="width:400px" labelField="true" label="业务编号"  labelStyle="width:135px" emptyText="系统自动生成" enabled="false"  /></td>
                                <td><input class="mini-buttonedit"  onbuttonclick="onInsQuery" id="brchNo" name="brchNo"  style="width:400px" labelField="true" label="机构号"  labelStyle="width:135px" emptyText=""   /></td>
                            </tr>
                            <tr>
                                <td><input class="mini-buttonedit" onbuttonclick="onCustQuery"  id="custNo"  name="custNo" style="width:400px"  labelField="true" label="客户代码"  required="true"  labelStyle="width:135px" emptyText="" /></td>
                                <td><input class="mini-textbox" id="custNm"  name="custNm" labelField="true" label="客户名称"  labelStyle="width:135px" style="width:400px"  emptyText="同步反显" enabled="false"></td>
                            </tr>
                            <tr>
                                <td><input class="mini-textbox" id="custAd"  name="custAd" labelField="true" label="客户地址" style="width:400px" emptyText="同步反显"  labelStyle="width:135px" enabled="false"></td>
                                <td><input class="mini-textbox" id="contNo"  name="contNo" style="width:400px" labelField="true" label="交易序号"  vtype="maxLength:30" labelStyle="width:135px"  emptyText=""/></td>
                            </tr>
                            <tr>
                                <td><input class="mini-textbox" id="prName"  name="prName" labelField="true" label="产品名称" vtype="maxLength:128" style="width:400px" emptyText=""  labelStyle="width:135px" ></td>
                                <td><input class="mini-combobox" id="ltisTp"  name="ltisTp" labelField="true" label="利率互换类型"  required="true"  labelStyle="width:135px" style="width:400px" emptyText="" data = "CommonUtil.serverData.dictionary.JHSH" ></td>
                            </tr>
                            <tr>
                                <td><input class="mini-textarea" id="remark"  name="remark" style="width:400px" labelField="true" label="备注" vtype="maxLength:64"  labelStyle="width:135px" emptyText="" /></td>  
                                <td><input class="mini-datepicker" id="apDate"  name="apDate" format="yyyy-MM-dd" style="width:400px" labelField="true" label="交易日期"  required="true"  labelStyle="width:135px" emptyText="" showOkButton="true" showClearButton="false"/></td>
                            </tr>
                            <tr>
                                <td><input class="mini-datepicker" id="stDate"  name="stDate" format="yyyy-MM-dd" style="width:400px" labelField="true" label="利率互换开始日期"  required="true"  labelStyle="width:135px" emptyText="" showOkButton="true" showClearButton="false"/></td>
                                <td><input class="mini-datepicker" id="mtDate"  name="mtDate" format="yyyy-MM-dd" style="width:400px" labelField="true" label="利率互换到期日期"  required="true"  labelStyle="width:135px" emptyText="" showOkButton="true" showClearButton="false"/></td>
                            </tr>
                            <tr>
                                <td><input class="mini-combobox" id="cucyNo"  name="cucyNo" labelField="true" label="币种"  required="true"  labelStyle="width:135px" style="width:400px" emptyText="" data = "CommonUtil.serverData.dictionary.Currency" ></td>
                                <td><input class="mini-buttonedit" onbuttonclick="onCustNoQuery" id="slacNo"  name="slacNo" style="width:400px" labelField="true" label="客户账号"   required="true"  labelStyle="width:135px" emptyText="" /></td>
                            </tr>
                            <tr>
                                <td><input class="mini-textbox" id="trAmts"  name="trAmts" style="width:400px" labelField="true" label="名义本金"  onvalidation="onMoneyValidation" required="true"  labelStyle="width:135px" emptyText="" /></td>
                                <td><input class="mini-textbox" id="trAmts1"  name="trAmts1" style="width:400px" labelField="true" label="前端费" onvalidation="onMoneyValidation" required="true"   labelStyle="width:135px"  emptyText="" /></td>
                            </tr>

                        </table>
                    </fieldset>
                    <fieldset>
                        <legend>付</legend>
                        <table width="100%" class="mini-table">
                            <tr>
                                <td><input class="mini-combobox" id="formu1"  name="formu1" style="width:400px" labelField="true" label="计息方式"  required="true"  labelStyle="width:135px" emptyText="" data = "CommonUtil.serverData.dictionary.JHSH"/></td>
                                <td><input class="mini-combobox" id="fType1"  name="fType1" style="width:400px" labelField="true" label="固定/浮动利息"   required="true"  labelStyle="width:135px" emptyText="" data = "CommonUtil.serverData.dictionary.Currency" /></td>
                            </tr>
                            <tr>
                                <td><input class="mini-buttonedit" onbuttonclick="onGetRate" id="iRate1"  name="iRate1" style="width:400px" labelField="true" label="利率(%)"   required="true"  labelStyle="width:135px" emptyText="" /></td>
                                <td><input class="mini-textbox" id="ipips1"  name="ipips1" style="width:400px" labelField="true" label="利率点差"   onvalidation="onPriceValidation"labelStyle="width:135px" emptyText="" /></td>
                            </tr>
                            <tr>
                                <td><input class="mini-combobox" id="icurve1"  name="icurve1" style="width:400px" labelField="true" label="浮动利率基准曲线"   labelStyle="width:135px" emptyText="" data = "CommonUtil.serverData.dictionary.JHSH"/></td>
                                <td><input class="mini-combobox" id="band1"  name="band1" style="width:400px" labelField="true" label="浮动利率期限"   labelStyle="width:135px" emptyText="" data = "CommonUtil.serverData.dictionary.Term" /></td> 
                            </tr>
                            <tr>
                                <td><input class="mini-textbox" id="offset1"  name="offset1" style="width:400px" labelField="true" label="天数偏移"  onvalidation="onPriceValidation"  labelStyle="width:135px" emptyText="" /></td> 
                                <td><input class="mini-combobox" id="daycv1"  name="daycv1" style="width:400px" labelField="true" label="计息天数"   labelStyle="width:135px" emptyText="" data = "CommonUtil.serverData.dictionary.Term" /></td> 
                            </tr>
                            <tr>
                                <td><input class="mini-combobox" id="paFreq"  name="paFreq" style="width:400px" labelField="true" label="付息频率"   required="true"  labelStyle="width:135px" emptyText="" data = "CommonUtil.serverData.dictionary.JHSH"/></td>
                                <td><input class="mini-combobox" id="rtFre1"  name="rtFre1" style="width:400px" labelField="true" label="利率重置频率"   labelStyle="width:135px" emptyText="" data = "CommonUtil.serverData.dictionary.Term" /></td>   
                            </tr>
                            <tr>
                                <td><input class="mini-datepicker" id="isTrd1"  name="isTrd1" format="yyyy-MM-dd" style="width:400px" labelField="true" label="本次付息日期"  required="true"  labelStyle="width:135px" emptyText="" showOkButton="true" showClearButton="false"/></td>
                                <td><input class="mini-datepicker" id="isRpdt"  name="isRpdt" format="yyyy-MM-dd" style="width:400px" labelField="true" label="本次利率重置日期"   labelStyle="width:135px" emptyText="" showOkButton="true" showClearButton="false"/></td> 
                            </tr>
                            <tr>
                                <td><input class="mini-textbox" id="nPiamt"  name="nPiamt" style="width:400px" labelField="true" label="付息金额"  onvalidation="onPriceValidation"  labelStyle="width:135px" emptyText="" /></td> 
                                <td><input class="mini-textbox" id="iRate3"  name="iRate3" style="width:400px" labelField="true" label="重置后利率"   labelStyle="width:135px" emptyText=""  /></td> 
                            </tr>
                        </table>
                    </fieldset>
                    <fieldset>
                        <legend>收</legend>
                        <table width="100%" class="mini-table">
                            <tr>
                                <td><input class="mini-combobox" id="formu2"  name="formu2" style="width:400px" labelField="true" label="计息方式"  required="true"  labelStyle="width:135px" emptyText="" data = "CommonUtil.serverData.dictionary.JHSH"/></td>
                                <td><input class="mini-combobox" id="fType2"  name="fType2" style="width:400px" labelField="true" label="固定/浮动利息"   required="true"  labelStyle="width:135px" emptyText="" data = "CommonUtil.serverData.dictionary.Currency" /></td>
                            </tr>
                            <tr>
                                <td><input class="mini-buttonedit" onbuttonclick="onGetRate" id="iRate2"  name="iRate2" style="width:400px" labelField="true" label="利率(%)"   required="true"  labelStyle="width:135px" emptyText="" /></td>
                                <td><input class="mini-textbox" id="ipips2"  name="ipips2" style="width:400px" labelField="true" label="利率点差"  onvalidation="onPriceValidation" labelStyle="width:135px" emptyText="" /></td>
                            </tr>
                            <tr>
                                <td><input class="mini-combobox" id="icurve2"  name="icurve2" style="width:400px" labelField="true" label="浮动利率基准曲线"   labelStyle="width:135px" emptyText="" data = "CommonUtil.serverData.dictionary.JHSH"/></td>
                                <td><input class="mini-combobox" id="band2"  name="band2" style="width:400px" labelField="true" label="浮动利率期限"   labelStyle="width:135px" emptyText="" data = "CommonUtil.serverData.dictionary.Term" /></td> 
                            </tr>
                            <tr>
                                <td><input class="mini-textbox" id="offset2"  name="offset2" style="width:400px" labelField="true" label="天数偏移"  onvalidation="onPriceValidation" labelStyle="width:135px" emptyText="" /></td> 
                                <td><input class="mini-combobox" id="daycv2"  name="daycv2" style="width:400px" labelField="true" label="计息天数"   labelStyle="width:135px" emptyText="" data = "CommonUtil.serverData.dictionary.Term" /></td> 
                            </tr>
                            <tr>
                                <td><input class="mini-combobox" id="reFreq"  name="reFreq" style="width:400px" labelField="true" label="收息频率"   required="true"  labelStyle="width:135px" emptyText="" data = "CommonUtil.serverData.dictionary.JHSH"/></td>
                                <td><input class="mini-combobox" id="rtFre2"  name="rtFre2" style="width:400px" labelField="true" label="利率重置频率"   labelStyle="width:135px" emptyText="" data = "CommonUtil.serverData.dictionary.Term" /></td>   
                            </tr>
                            <tr>
                                <td><input class="mini-datepicker" id="isTrd2"  name="isTrd2" format="yyyy-MM-dd" style="width:400px" labelField="true" label="本次收息日期"  required="true"  labelStyle="width:135px" emptyText="" showOkButton="true" showClearButton="false"/></td>
                                <td><input class="mini-datepicker" id="isCpdt"  name="isCpdt" format="yyyy-MM-dd" style="width:400px" labelField="true" label="本次利率重置日期"   labelStyle="width:135px" emptyText="" showOkButton="true" showClearButton="false"/></td> 
                            </tr>
                            <tr>
                                <td><input class="mini-textbox" id="nRiamt"  name="nRiamt" style="width:400px" labelField="true" label="收息金额"  onvalidation="onPriceValidation"  labelStyle="width:135px" emptyText="" /></td> 
                                <td><input class="mini-textbox" id="iRate4"  name="iRate4" style="width:400px" labelField="true" label="重置后利率"   labelStyle="width:135px" emptyText=""  /></td> 
                            </tr>
                        </table>
                    </fieldset>
                			
				</div>
			</div>
			<div id="functionIds" showCollapseButton="true">
				<div style="margin-bottom:10px; margin-top:50px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_valid"   onclick="valid()">校验要素</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_save"          onclick="save">保存交易</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_verify"    enabled="false" onclick="verify">提交复核</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_refuse"        enabled="false" onclick="refuse">拒绝交易</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_checkTrade"        enabled="false"   onclick="checkTrade">复核交易</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_close"              onclick="close">关闭界面</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_tradeFee"     enabled="false" onclick="tradeFee">交易费用</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_tradeProof"      enabled="false" onclick="tradeProof">交易凭证</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_agencyFeeBtn"     enabled="false"    onclick="">会计传票</a></div>
			</div>      
		</div>


   
	
<script type="text/javascript">
     
	mini.parse();

	var url = window.location.search;
    var action = CommonUtil.getParam(url,"action");
	var dataTp = CommonUtil.getParam(url,"dataTp");

	$(document).ready(function(){
        init();
    });

	function init(){
		if(action == "new"){
			
		}else if(action == "edit" || action == "detail"){
			var form1 = new mini.Form("fwdinfo");
			var data=top["irsCountInterestList"].getData();
			form1.setData(data);
			//buttonedit控件需要单独设置值
			//1.机构号
			mini.get("brchNo").setValue(data.brchNo);
			mini.get("brchNo").setText(data.brchNo);
			//2.客户代码
			mini.get("custNo").setValue(data.custNo);
			mini.get("custNo").setText(data.custNo);
			
			
			
			if(action == "detail"){
				form1.setEnabled(false);
				if(dataTp == "verify"){//待复核
					mini.get("btn_valid").setEnabled(false);//检验要素
					mini.get("btn_save").setEnabled(false);//保存交易
					mini.get("btn_verify").setEnabled(false);//提交复核
					mini.get("btn_refuse").setEnabled(true);//拒绝交易
					mini.get("btn_checkTrade").setEnabled(true);//复核交易
					mini.get("btn_close").setEnabled(false);//关闭界面
					mini.get("btn_tradeFee").setEnabled(false);//交易费用
					mini.get("btn_tradeProof").setEnabled(false);//交易凭证
					mini.get("btn_agencyFeeBtn").setEnabled(false);//会计传票
				}else if(dataTp == "effi"){//生效
					mini.get("btn_valid").setEnabled(false);//检验要素
					mini.get("btn_save").setEnabled(false);//保存交易
					mini.get("btn_verify").setEnabled(false);//提交复核
					mini.get("btn_refuse").setEnabled(false);//拒绝交易
					mini.get("btn_checkTrade").setEnabled(false);//复核交易
					mini.get("btn_close").setEnabled(false);//关闭界面
					mini.get("btn_tradeFee").setEnabled(true);//交易费用
					mini.get("btn_tradeProof").setEnabled(true);//交易凭证
					mini.get("btn_agencyFeeBtn").setEnabled(true);//会计传票
				}else if(dataTp == "handle"){//经办
					

					mini.get("btn_valid").setEnabled(false);//检验要素
					mini.get("btn_save").setEnabled(true);//保存交易
					mini.get("btn_verify").setEnabled(false);//提交复核
					mini.get("btn_refuse").setEnabled(false);//拒绝交易
					mini.get("btn_checkTrade").setEnabled(false);//复核交易
					mini.get("btn_close").setEnabled(false);//关闭界面
					mini.get("btn_tradeFee").setEnabled(false);//交易费用
					mini.get("btn_tradeProof").setEnabled(false);//交易凭证
					mini.get("btn_agencyFeeBtn").setEnabled(false);//会计传票
				}
			}else if(action == "edit"){
                form1.setEnabled(false);
				if(dataTp == "modify"){//待修改
					mini.get("btn_valid").setEnabled(false);//检验要素
					mini.get("btn_save").setEnabled(false);//保存交易
					mini.get("btn_verify").setEnabled(true);//提交复核
					mini.get("btn_refuse").setEnabled(false);//拒绝交易
					mini.get("btn_checkTrade").setEnabled(false);//复核交易
					mini.get("btn_close").setEnabled(false);//关闭界面
					mini.get("btn_tradeFee").setEnabled(false);//交易费用
					mini.get("btn_tradeProof").setEnabled(false);//交易凭证
					mini.get("btn_agencyFeeBtn").setEnabled(false);//会计传票
				}
			}

		}
	}

	

	//机构的查询
	function onInsQuery(e) {
		var btnEdit = this;
		mini.open({
			url : CommonUtil.baseWebPath() +"InterBank/mini_system/MiniInstitutionSelectManages.jsp",
			title : "机构选择",
			width : 700,
			height : 600,
			ondestroy : function(action) {	
				if (action == "ok") 
				{
					var iframe = this.getIFrameEl();
					var data = iframe.contentWindow.GetData();
					data = mini.clone(data); //必须
					if (data) {
						btnEdit.setValue(data.instId);
						btnEdit.setText(data.instId);
						btnEdit.focus();
					}

					
				}

			}
		});
	}

	//客户的查询
	function onCustQuery(e) {
		var btnEdit = this;
		mini.open({
			url : CommonUtil.baseWebPath() +"/base/MiniCPManageYQ.jsp",
			title : "客户选择",
			width : 700,
			height : 600,
			ondestroy : function(action) {	
				if (action == "ok") 
				{
					var iframe = this.getIFrameEl();
					var data = iframe.contentWindow.GetData();
					data = mini.clone(data); //必须
					if (data) {
						btnEdit.setValue(data.party_id);
						btnEdit.setText(data.party_id);
						
						mini.get("custNm").setValue(data.party_name);
						//有联系人
						if(data.constact_list.length>0){
							for(var i=0;i<data.constact_list.length;i++){
								if(data.constact_list[i].address){//有联系人，还有地址
									mini.get("custAd").setValue(data.constact_list[i].address);
									return;
								}else{//有联系人，没有地址
									mini.get("custAd").setValue("");
								}
							}
							
						}else{//没有联系人
							mini.get("custAd").setValue("");
						}

						
						
						btnEdit.focus();
						//btnEdit.doValueChanged(alert("1"));
						//mini.get("custNo").doValueChanged(custNoChange());
					}

					
				}

			}
		});
	}

	//客户代码值改变时发生
	var btnEdit1 = mini.get("custNo");
    btnEdit1.on("valuechanged", function () {
       //客户代码值改变，原账号清空
		mini.get("yuacNo").setValue("");
		mini.get("yuacNo").setText("");
		//客户代码值改变，目的账号清空
		mini.get("mdacNo").setValue("");
		mini.get("mdacNo").setText(""); 
    })

	/* function custNoChange(){
		////debugger
		//客户代码值改变，原账号清空
		mini.get("yuacNo").setValue("");
		mini.get("yuacNo").setText("");
		//客户代码值改变，目的账号清空
		mini.get("mdacNo").setValue("");
		mini.get("mdacNo").setText("");
	} */


	

	
	

    //交易金额（外币）的校验
	function onMoneyValidation(e) {
		if (e.isValid) {
			if (isMoney(e.value) == false) {
				e.errorText = "金额整数位最多17位，小数位最多2位!";
				e.isValid = false;
			}
		}
    }
    /* 金额number(19,2)的验证 */
	function isMoney(v) {
		var re = new RegExp("^[0-9]{1,17}([.]{1}[0-9]{1,2}){0,1}$");
		if (re.test(v)){
			return true;
		}else{
			return false;
		} 
		
	}

    //执行价格、即期汇率、对客期权费率的校验
	function onPriceValidation(e) {
		if (e.isValid) {
			if (isPrice(e.value) == false) {
				e.errorText = "整数位最多12位，小数位最多7位!";
				e.isValid = false;
			}
		}
    }
    /* number(19,7)的验证 */
	function isPrice(v) {
		var re = new RegExp("^[0-9]{1,12}([.]{1}[0-9]{1,7}){0,1}$");
		if (re.test(v)){
			return true;
		}else{
			return false;
		} 
		
	}


//*****************************************事件*************************************************************

	//检验要素
	function valid(){
		var form = new mini.Form("fwdinfo");
		form.validate();
	}

    //保存
    function save(){
     	var form = new mini.Form("#fwdinfo");
		//验证表单
		form.validate();
		if(form.isValid() == false){
			mini.alert("表单填写错误,请确认!","提示信息")
			return;
		}
    		   
		var data = form.getData(true); //获取表单多个控件的数据
		
		data.userSt='S'
		data.statCd='A'
		data.newStatCd='A'
		var param = mini.encode(data); //序列化成JSON
		
		CommonUtil.ajax({
					url:'/ForwardController/save',
					data:param ,
					callback:function(data){
						//mini.get("coptNo").setValue(data.desc);
            			mini.alert("保存成功!");
            			mini.get("btn_verify").setEnabled(true);
						
					}
		});
    		
	}

	//提交
	function verify(){
       	var form = new mini.Form("#fwdinfo");
		//验证表单
		form.validate();
		if(form.isValid() == false){
			mini.alert("表单填写错误,请确认!","提示信息")
			return;
		}
			
		var data = form.getData(true); //获取表单多个控件的数据
		
		data.userSt='Y'
		data.wbamBl=''
		data.newStatCd='A'
		var param = mini.encode(data); //序列化成JSON
		
		CommonUtil.ajax({
			url:'/ForwardController/commit',
			data:param,
			callback:function(data){
				mini.alert("提交成功!","系统提示",function(value){
                    //关闭并刷新
					setTimeout(function(){ top["win"].closeMenuTab() },100);
                });
				
			}
		});
			
	}

	//复核通过
    function checkTrade(){
       var form = new mini.Form("#fwdinfo");
		//验证表单
		form.validate();
		if(form.isValid() == false){
		mini.alert("表单填写错误,请确认!","提示信息")
		return;
		}
			
		var data = form.getData(true); //获取表单多个控件的数据
		
		data.userSt='Y'
		data.chckSt='Y'
        data.statCd='A'
        data.wbamBl=''
        data.newStatCd='A'
		var param = mini.encode(data); //序列化成JSON
		
		CommonUtil.ajax({
			url:'/ForwardController/checkTradeTr',
			data:param ,
			callback:function(data){
				mini.alert("复核通过!","系统提示",function(value){
                    //关闭并刷新
					setTimeout(function(){ top["win"].closeMenuTab() },100);
                });
				
			}
		});
		
	}

	//拒绝
	function refuse(){
       var form = new mini.Form("#fwdinfo");
		//验证表单
		form.validate();
		if(form.isValid() == false){
		mini.alert("表单填写错误,请确认!","提示信息")
		return;
		}
			
		var data = form.getData(true); //获取表单多个控件的数据
		
		data.userSt='Y'
		data.chckSt='N'
		data.wbamBl=''
		data.newStatCd='A'
		var param = mini.encode(data); //序列化成JSON
		
		CommonUtil.ajax({
			url:'/ForwardController/refuse',
			data:param ,
			callback:function(data){
				mini.alert("复核拒绝成功!","系统提示",function(value){
                    //关闭并刷新
					setTimeout(function(){ top["win"].closeMenuTab() },100);
                });
				
			}
		});
			
	}

	//择期交割日-开始日期
	function minDateChange(){
		////debugger
		var form = new mini.Form("#fwdinfo");
		var data = form.getData(true); //获取表单多个控件的数据
		/* if(data.jgDatt == null || data.jgDatt == ""){//至
			return false;
		}
 */
		if(CommonUtil.dateDiff(data.jgDatf,data.jgDatt) == 0){
			mini.alert("此日期不能等于结束择期交割日!","提示");
			mini.get("jgDatf").setValue("");
			return false;
		}

		if(CommonUtil.dateCompare(data.jgDatf,data.jgDatt)){
			mini.alert("此日期不能大于结束择期交割日!","提示");
			mini.get("jgDatf").setValue("");
			return false;
		}
		
	}


	//择期交割日-结束日期
	function maxDateChange(){
		////debugger
		var form = new mini.Form("#fwdinfo");
		var data = form.getData(true); //获取表单多个控件的数据
		if(data.jgDatt == null || data.jgDatt == ""){//至
			return false;
		}

		if(CommonUtil.dateDiff(data.jgDatf,data.jgDatt) == 0){
			mini.alert("此日期不能等于开始择期交割日!","提示");
			mini.get("jgDatt").setValue("");
			return false;
		}

		if(CommonUtil.dateCompare(data.jgDatf,data.jgDatt)){
			mini.alert("此日期不能小于开始择期交割日!","提示");
			mini.get("jgDatt").setValue("");
			return false;
		}
		
	}
	


	
</script>
</body>
</html>
