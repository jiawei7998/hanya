<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../../global.jsp"%> 
<html>
<head>
  <title>利率互换-交割列表</title>
  <script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
</head>
<body style="width:100%;height:100%;background:white">
	
	<fieldset>
		<legend><label>查询条件</label></legend>
			<div id="search_form"  >
				<input id="custNo" name="custNo" class="mini-textbox"  labelField="true"  label="客户代码：" style="width:310px;" emptyText="请输入客户代码" labelStyle="text-align:right;"/>
				<input id="custNm" name="custNm" class="mini-textbox"  labelField="true"  label="客户名称：" style="width:310px;" emptyText="请输入客户名称" labelStyle="text-align:right;"/>
				<span style="float:right;margin-right: 90px">
					<a id="search_btn" class="mini-button" style="display: none"   onclick="search1">查询</a>
					<a id="clear_btn" class="mini-button" style="display: none"    onClick="clear">清空</a>
				</span>
			</div>
		   
	</fieldset>
	<span style="margin:2px;display: block;">
		<!-- <a id="add_btn" class="mini-button" style="display: none"   onclick="add">新增</a> -->
		<a id="edit_btn" class="mini-button" style="display: none"   onclick="edit">修改</a>
		<a id="del_btn" class="mini-button" style="display: none"   onclick="del">删除</a>
		<a id="detail_btn" class="mini-button" style="display: none"    onclick="detail">查看详情</a>
		
		<div id = "approveType" name = "approveType" labelField="true" label=""  class="mini-checkboxlist" 
			value="approve" textField="text" valueField="id" multiSelect="false" style="float:right; margin-right: 50px;" 
			labelStyle="text-align:right;" 
			data="[{id:'handle',text:'经办'},{id:'temp_storage',text:'暂存'},{id:'wait_modify',text:'待修改'},{id:'wait_verify',text:'待复核'},{id:'efficient',text:'生效'}]" onvaluechanged="checkBoxValuechanged">
		</div>
	</span>
	
	<div class="mini-fit" id="fitId" style="margin-top: 2px;">
		<div id="irsDelivery_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" 
		idField="fldNo"  pageSize="10" multiSelect="true"  sortMode="client"
		allowAlternating="true">
			<div property="columns" >
				<div type="indexcolumn" headerAlign="center"  align="center" width="40px">序号</div>
				<div type="checkcolumn"></div>
				<div field="ltisNo" headerAlign="center"  align="center" width="160px" >业务编号</div>  
                <div field="brchNo" headerAlign="center" width="150px">机构号</div>
                <!-- <div field="contNo" headerAlign="center" width="150px">交易序号</div> -->
                
				<div field="custNo"  headerAlign="center" width="100px">客户代码</div>
                <div field="custNm"   headerAlign="center"  width="150px">客户名称</div>
                <div field="custAd"   headerAlign="center"   width="150px">客户地址</div>
               <!--  <div field="fdqvNo"   headerAlign="center"  align="center" width="150px">询价业务编号</div> -->
                <div field="prName"   headerAlign="center"   width="150px">产品名称</div>
                <div field="ltisTp"   headerAlign="center"  align="center" width="150px">利率互换类型</div>
                <div field="remark"   headerAlign="center"  width="150px">备注</div>
                <div field="apDate"   headerAlign="center"  align="center" width="150px">交易日期</div>
                <div field="stDate"   headerAlign="center"  align="center" width="150px">利率互换开始日期</div>
                <div field="mtDate"   headerAlign="center"  align="center" width="150px">利率互换到期日期</div>
                <div field="cucyNo"   headerAlign="center"   width="150px">币种</div>
                <div field="slacNo"   headerAlign="center"  align="center" width="150px">客户账号</div>
                <div field="trAmts"   headerAlign="center"  align="center" width="150px">名义本金</div>
                <div field="trAmts1"   headerAlign="center"  align="center" width="150px">前端费</div>
                <div field="trAmts2"   headerAlign="center"  align="center" width="150px">净收额</div>

                <div field="formu1"   headerAlign="center"  align="center" width="150px" visible="false">交割方式</div>
                <div field="fType1"   headerAlign="center"  align="center" width="150px" visible="false">固定/浮动利息</div>
                <div field="iRate1"   headerAlign="center"  align="center" width="150px" visible="false">利率(%)</div>
                <div field="ipips1"   headerAlign="center"  align="center" width="150px" visible="false">利率点差</div>
                <div field="icurve1"   headerAlign="center"  align="center" width="150px" visible="false">浮动利率基准曲线</div>
                <div field="band1"   headerAlign="center"  align="center" width="150px" visible="false">浮动利率期限</div>
                <div field="offset1"   headerAlign="center"  align="center" width="150px" visible="false">天数偏移</div>
                <div field="daycv1"   headerAlign="center"  align="center" width="150px" visible="false">交割天数</div>
                <div field="paFreq"   headerAlign="center"  align="center" width="150px" visible="false">付息频率</div>
                <div field="rtFre1"   headerAlign="center"  align="center" width="150px" visible="false">利率重置频率</div>
                <div field="isTrd1"   headerAlign="center"  align="center" width="150px" visible="false">本次付息日期</div>
                <div field="isRpdt"   headerAlign="center"  align="center" width="150px" visible="false">本次利率重置日期</div>
                <div field="nPiamt"   headerAlign="center"  align="center" width="150px" visible="false">付息金额</div>
               <!--  <div field="iRate3"   headerAlign="center"  align="center" width="150px" visible="false">重置后利率</div> -->



                <div field="formu2"   headerAlign="center"  align="center" width="150px" visible="false">交割方式</div>
                <div field="fType2"   headerAlign="center"  align="center" width="150px" visible="false">固定/浮动利息</div>
                <div field="iRate2"   headerAlign="center"  align="center" width="150px" visible="false">利率(%)</div>
                <div field="ipips2"   headerAlign="center"  align="center" width="150px" visible="false">利率点差</div>
                <div field="icurve2"   headerAlign="center"  align="center" width="150px" visible="false">浮动利率基准曲线</div>
                <div field="band2"   headerAlign="center"  align="center" width="150px" visible="false">浮动利率期限</div>
                <div field="offset2"   headerAlign="center"  align="center" width="150px" visible="false">天数偏移</div>
                <div field="daycv2"   headerAlign="center"  align="center" width="150px" visible="false">交割天数</div>
                <div field="reFreq"   headerAlign="center"  align="center" width="150px" visible="false">收息频率</div>
                <div field="rtFre2"   headerAlign="center"  align="center" width="150px" visible="false">利率重置频率</div>
                <div field="isTrd2"   headerAlign="center"  align="center" width="150px" visible="false">本次收息日期</div>
                <div field="isCpdt"   headerAlign="center"  align="center" width="150px" visible="false">本次利率重置日期</div>
                <div field="nRiamt"   headerAlign="center"  align="center" width="150px" visible="false">收息金额</div>
                <!-- <div field="iRate4"   headerAlign="center"  align="center" width="150px" visible="false">重置后利率</div> -->



				                                                                                      
               
			</div>
		</div>
     </div>
     <script type="text/javascript">
        mini.parse();

		$(document).ready(function(){
            //控制按钮显示
            $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
                //初始化为经办列表页面
                mini.get("approveType").setValue("handle");
                //经办列表页面,修改、删除按钮要隐藏
                /* mini.get("add_btn").setVisible(false); */
                mini.get("edit_btn").setVisible(false);
                mini.get("del_btn").setVisible(false);

                var grid = mini.get("irsDelivery_grid");
                grid.on("beforeload", function (e) {
                    e.cancel = true;
                    var pageIndex = e.data.pageIndex;
                    var pageSize = e.data.pageSize;
                    search(pageSize, pageIndex);
                });
                search(grid.pageSize, 0);
            });
		});

        top["irsDeliveryList"]=window;
        function getData(){
            var grid = mini.get("irsDelivery_grid");
            var record =grid.getSelected(); 
            return record;
        }

		function search1(){
            var grid =mini.get("irsDelivery_grid");
            search(grid.pageSize,0); 
        }

        function search(pageSize,pageIndex){
            var form =new mini.Form("search_form");
            form.validate();
            if (form.isValid() == false) return;//表单验证
            var data =form.getData();//获取表单数据
            data['pageNumber']=pageIndex+1;
			data['pageSize']=pageSize;

			var url="";
			var approveType = mini.get("approveType").getValue();
			if(approveType == "handle"){//经办
				data.newStatCd='Z'
				data.statCd='A';
				data.otherStatCd1='T';
				data.otherStatCd2='C';
				data.otherStatCd3='W';
				url = "/ForwardController/searchPageOperate";

			}else if(approveType == "temp_storage"){//暂存
				data.statCd='Z';
				url = "/ForwardController/searchPageTempStorage";

			}else if(approveType == "wait_modify"){//待修改
				data.statCd='Z';
				url = "/ForwardController/searchPageWaitModify";

			}else if(approveType == "wait_verify"){//待复核
				data.statCd='Z';
				url = "/ForwardController/searchPageWaitVerify";

			}else{//生效efficient
				data.statCd='Z';
				url = "/ForwardController/searchPageEfficient";
			}

            var param = mini.encode(data); //序列化成JSON
               CommonUtil.ajax({
					url:url,
					data:param,
					callback:function(data){
							
						var grid =mini.get("irsDelivery_grid");
						//设置分页
						grid.setTotalCount(data.obj.total);
						grid.setPageIndex(pageIndex);
						grid.setPageSize(pageSize);
						//设置数据
						grid.setData(data.obj.rows);
						//
						var rows = grid.getData();
						for(var i=0,l=rows.length;i<l;i++){
							var row = rows[i];
							grid.updateRow(row,{exRate:"",myRate:"",omyRat:data.obj.rows[i].myRate,oexRat:data.obj.rows[i].exRate});
							grid.updateRow(row,{ojgTyp:data.obj.rows[i].jgType,ojgDat:data.obj.rows[i].jgDate,ojgDaf:data.obj.rows[i].jgDatf,ojgDtt:data.obj.rows[i].jgDatt});
							grid.updateRow(row,{jgType:"",jgDate:"",jgDatf:"",jgDatt:""});
						}
							
					}
	
                });
		}

        //新增
		function add(){
			var url = CommonUtil.baseWebPath() + "/trade/fxirs/IrsDeliveryEdit.jsp?action=new";
			var tab = { id: "irsDeliveryListAdd", name: "irsDeliveryListAdd", text: "利率互换-交割新增", url: url,showCloseButton:true,parentId:top['win'].tabs.getActiveTab().name };
			top['win'].showTab(tab);
		}

		//修改
        function edit(){
			var type = mini.get("approveType").getValue();
			var url = "";
			if(type == "temp_storage"){//暂存
				url = CommonUtil.baseWebPath() + "/trade/fxirs/IrsDeliveryEdit.jsp?action=edit&dataTp=temp";	
			}else if(type == "wait_modify"){//待修改
				url = CommonUtil.baseWebPath() + "/trade/fxirs/IrsDeliveryEdit.jsp?action=edit&dataTp=modify";	
			}
            var grid = mini.get("irsDelivery_grid");
            var  record =grid.getSelected();
            if(record){
                var tab = { id: "irsDeliveryListEdit", name: "irsDeliveryListEdit", text: "利率互换-交割修改", url: url,showCloseButton:true,parentId:top['win'].tabs.getActiveTab().name };
                top['win'].showTab(tab);
            }else{
                mini.alert("请选中一条记录!","系统提示"); 
            }
        }

        //查看详情
        function detail(){
			var type = mini.get("approveType").getValue();
			var url = "";
			if(type == "handle"){//经办
				url = CommonUtil.baseWebPath() + "/trade/fxirs/IrsDeliveryEdit.jsp?action=detail&dataTp=handle";	
			}else if(type == "wait_verify"){//待复核
				url = CommonUtil.baseWebPath() + "/trade/fxirs/IrsDeliveryEdit.jsp?action=detail&dataTp=verify";	
			}else if(type == "efficient"){//生效
				url = CommonUtil.baseWebPath() + "/trade/fxirs/IrsDeliveryEdit.jsp?action=detail&dataTp=effi";	
			}

            var grid = mini.get("irsDelivery_grid");
            var  record =grid.getSelected();
            if(record){
                
                var tab = { id: "irsDeliveryListDetail", name: "irsDeliveryListDetail", text: "利率互换-交割详情", url: url,showCloseButton:true,parentId:top['win'].tabs.getActiveTab().name };
                top['win'].showTab(tab);
            }else{
                mini.alert("请选中一条记录!","系统提示"); 
            }
        } 

        //清空
		function clear(){
			var form =new mini.Form("search_form");
			form.clear();
			search1();
			
		}

        //删除
		function del(){
			var grid = mini.get("irsDelivery_grid");
            //获取选中行
            var rows =grid.getSelecteds();
            
            if (rows.length>0) {
                //把业务编号放在数组里
				var ltisNos = new Array();
				for(var i=0;i<rows.length;i++){
					ltisNos.push(rows[i].ltisNo);
				}
                
                mini.confirm("您确认要删除?","系统警告",function(value){   
                    if (value == "ok"){   
                        CommonUtil.ajax({
                            url : '',
                            data : ltisNos,
                            callback : function(data){
                                if("error.common.0000" == data.code){
                                    mini.alert("删除成功！","系统提示");
                                    var grid =mini.get("irsDelivery_grid");
                                    search(grid.pageSize,0);
                                }
                            }
                        });
                    
                    }   
                });       
                    
            }else{
                mini.alert("请选择一条记录!","系统提示");
            }
		}

        function checkBoxValuechanged(){
			var type = mini.get("approveType").getValue();
			if(type == "handle"){//经办
				/* mini.get("add_btn").setVisible(false); */
				mini.get("edit_btn").setVisible(false);
				mini.get("detail_btn").setVisible(true);
				mini.get("del_btn").setVisible(false);
			}else if(type == "temp_storage"){//暂存
				/* mini.get("add_btn").setVisible(true); */
				mini.get("edit_btn").setVisible(true);
				mini.get("detail_btn").setVisible(false);
				mini.get("del_btn").setVisible(true);
			}else if(type == "wait_modify"){//待修改
				/* mini.get("add_btn").setVisible(false); */
				mini.get("edit_btn").setVisible(true);
				mini.get("detail_btn").setVisible(false);
				mini.get("del_btn").setVisible(false);
			}else if(type == "wait_verify"){//待复核
				/* mini.get("add_btn").setVisible(false); */
				mini.get("edit_btn").setVisible(false);
				mini.get("detail_btn").setVisible(true);
				mini.get("del_btn").setVisible(false);
			}else{//生效
				/* mini.get("add_btn").setVisible(false); */
				mini.get("edit_btn").setVisible(false);
				mini.get("detail_btn").setVisible(true);
				mini.get("del_btn").setVisible(false);
			}
			
			search1();
		}




     </script>
</body>
</html>