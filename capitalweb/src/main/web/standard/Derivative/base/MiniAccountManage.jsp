<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../../global.jsp" %>
<html>
<head>
    <title>账号管理</title>
    <script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset>
    <legend><label>远期外汇买卖-登记列表</label></legend>
    <div id="search_form">
        <input id="custNo" name="custNo" class="mini-textbox" labelField="true" label="客户代码：" style="width:310px;"
               labelStyle="text-align:right;" enabled="false"/>
        <input id="yucyNo" name="yucyNo" class="mini-textbox" labelField="true" label="原币种：" style="width:310px;"
               labelStyle="text-align:right;" enabled="false"/>
        <input id="mdcyNo" name="mdcyNo" class="mini-textbox" labelField="true" label="目的币种：" style="width:310px;"
               labelStyle="text-align:right;" enabled="false"/>
        <!-- <input id="accotp" name="accotp" class="mini-combobox"  labelField="true"  label="账号类型：" style="width:310px;" emptyText="请选择账号类型" labelStyle="text-align:right;"data = "CommonUtil.serverData.dictionary.AccountType"/> -->

        <!-- <span style="float:right;margin-right: 90px">
            <a id="search_btn" class="mini-button" style="display: none"   onclick="search1">查询</a>
            <a id="clear_btn" class="mini-button" style="display: none"    onClick="clear">清空</a>
        </span> -->
    </div>
</fieldset>
<div class="mini-fit" style="margin-top: 2px;">
    <div id="account_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
         idField="fldNo" pageSize="10" multiSelect="true" onRowdblclick="onRowdblclickAccount" sortMode="client"
         allowAlternating="true">
        <div property="columns">
            <div type="indexcolumn" headerAlign="center" align="center" width="40px">序号</div>
            <div type="checkcolumn"></div>
            <div field="custNo" headerAlign="center" align="center" width="80px" allowSort="true" dataType="int">客户代码
            </div>
            <div field="accoTp" headerAlign="center" width="150px">账号类型</div>
            <div field="accoNo" headerAlign="center" width="150px">账号</div>
        </div>
    </div>
</div>

</body>
<script type="text/javascript">
    mini.parse();

    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn){
        });
    });

    //接收远期登记维护页面的值
    //客户代码、原币种/目的币种
    function SetData(data) {

        mini.get("custNo").setValue(data.custNo);
        if (data.type == "orig") {
            mini.get("yucyNo").setValue(data.yucyNo);
            mini.get("mdcyNo").setVisible(false);
        } else if (data.type == "dest") {
            mini.get("mdcyNo").setValue(data.mdcyNo);
            mini.get("yucyNo").setVisible(false);
        }

        var grid = mini.get("account_grid");
        grid.on("beforeload", function (e) {
            e.cancel = true;
            var pageIndex = e.data.pageIndex;
            var pageSize = e.data.pageSize;
            search(pageSize, pageIndex);
        });
        search(grid.pageSize, 0);
    }


    function search1() {
        var grid = mini.get("account_grid");
        search(grid.pageSize, 0);
    }

    function search(pageSize, pageIndex) {
        var form = new mini.Form("search_form");
        form.validate();
        if (form.isValid() == false) return;//表单验证
        var data = form.getData();//获取表单数据
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;

        var param = mini.encode(data); //序列化成JSON
        CommonUtil.ajax({
            url: "/ForwardController/searchCustomerAccount",
            data: param,
            callback: function (data) {

                var grid = mini.get("account_grid");
                //设置分页
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                //设置数据
                grid.setData(data.obj.rows);
            }
        });
    }

    //清空
    function clear() {
        var form = new mini.Form("search_form");
        form.clear();
        search1();
    }

    function GetData() {
        var grid = mini.get("account_grid");
        var row = grid.getSelected();
        return row;
    }

    /* function onRowDblClick(e) {
        onOk();
    } */

    function CloseWindow(action) {
        if (window.CloseOwnerWindow)
            return window.CloseOwnerWindow(action);
        else
            window.close();
    }

    function onOk() {
        CloseWindow("ok");
    }

    function onCancel() {
        CloseWindow("cancel");
    }

    //双击行选择
    function onRowdblclickAccount() {
        onOk();
    }


</script>
</html>
