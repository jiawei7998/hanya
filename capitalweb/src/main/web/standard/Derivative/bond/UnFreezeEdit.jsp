<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
  <title>保证金-解冻-维护</title>
  <script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
  </head>
  
  <body style="width:100%;height:100%;background:white">
		<div class="mini-splitter" style="width:100%;height:100%;">
			<div size="90%" showCollapseButton="true">
				<div class="mini-fit"  style="background:white" id="inquiry_form">
                    <table  width="100%" class="mini-table">
                        <tr>
                            <td><input class="mini-textbox"  id ="mgrgno" name="mgrgno"   style="width:400px"labelField="true" label="保证金冻结编号" labelStyle="width:130px" emptyText="系统自动生成" enabled="false"/></td>
                            <td><input class="mini-buttonedit"  onbuttonclick="onInsQuery" id="brchno" name="brchno"  style="width:400px" labelField="true" label="机构号" labelStyle="width:130px"emptyText=""  /></td>
                        </tr>
                        <tr>
                            <td><input class="mini-buttonedit" onbuttonclick="onCustQuery"  id="custno"  name="custno" style="width:400px"  labelField="true" label="客户代码" labelStyle="width:130px"emptyText="" required="true" /></td>
                            <td><input class="mini-textbox"  id="cuname"  name="cuname" style="width:400px" labelField="true" label="客户名称" labelStyle="width:130px" emptyText="同步反显" enabled="false"  /></td>
                        </tr>
                        <tr>
                            <td><input class="mini-textbox" id="crcono"  name="crcono" labelField="true" label="合同号" labelStyle="width:130px" style="width:400px"   vtype="maxLength:30" onvalidation="onEnglishAndNumberValidation" emptyText="" ></td>
                            <td><input class="mini-combobox" id="lcyqcy"  name="lcyqcy" labelField="true" label="掉期/远期结售汇币种" labelStyle="width:130px" style="width:400px"   required="true"  data = "CommonUtil.serverData.dictionary.Currency" emptyText="" ></td>
                        </tr>
                        <tr>
                            <td><input class="mini-textbox" id="lcyqam"  name="lcyqam" style="width:400px" labelField="true" label="掉期/远期结售汇金额"  labelStyle="width:130px" emptyText="" required="true"  /></td>
                            <td><input class="mini-textbox" id="mgrate"  name="mgrate" style="width:400px" labelField="true" label="保证金比例%"  labelStyle="width:130px" emptyText="" required="true"    /></td>
                        </tr>

                        <tr>
                            <td><input class="mini-buttonedit" onbuttonclick="onBondNoQuery" id="mgacno"  name="mgacno" style="width:400px" labelField="true" label="保证金帐号" labelStyle="width:130px" emptyText="" required="true"  /></td>
                            <td><input class="mini-textbox" id="suacno"  name="suacno" style="width:400px" labelField="true" label="子户号" labelStyle="width:130px" onvalidation="onMoneyValidation" emptyText="" /></td>
                        </tr>
                        <tr>
                            <td><input class="mini-combobox"  id="mgaccy"  name="mgaccy" style="width:400px" labelField="true" label="币种"  labelStyle="width:130px" emptyText="" data = "CommonUtil.serverData.dictionary.Currency"/></td>
                            <td><input class="mini-textbox" id="mgacam"  name="mgacam" style="width:400px" labelField="true" label="金额"  labelStyle="width:130px" vtype="maxLength:70" emptyText="" required="true" /></td>
                            
                        </tr>
                        <tr>
                            <td><input class="mini-textbox" id="mgblam"  name="mgblam" style="width:400px" labelField="true" label="余额" labelStyle="width:130px" vtype="maxLength:70" emptyText="" required="true" /></td>
                            <td><input class="mini-textbox" id="rtamts"  name="rtamts" style="width:400px" labelField="true" label="解冻金额" labelStyle="width:130px" vtype="maxLength:70" emptyText="" required="true" /></td>
                        </tr>
                        
                        
                    </table>		
						
				</div>
			</div>
			<div id="functionIds" showCollapseButton="true">
				<div style="margin-bottom:10px; margin-top:5px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_valid"   onclick="valid()">校验要素</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_save"          onclick="save">保存交易</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_verify"    enabled="false" onclick="verify">提交复核</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_refuse"        enabled="false" onclick="refuse">拒绝交易</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_checkTrade"        enabled="false"   onclick="checkTrade">复核交易</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_close"              onclick="close">关闭界面</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_tradeFee"     enabled="false" onclick="tradeFee">交易费用</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_tradeProof"      enabled="false" onclick="tradeProof">交易凭证</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_agencyFeeBtn"     enabled="false"    onclick="">会计传票</a></div>
			</div>      
		</div>


</body>

<script type="text/javascript">
    mini.parse();

    var url = window.location.search;
    var action = CommonUtil.getParam(url,"action");
    var dataTp = CommonUtil.getParam(url,"dataTp");

    $(document).ready(function(){
		
        init();
    });

    function init(){
		if(action == "new"){
			
			
		}else if(action == "edit" || action == "detail"){
            var form1 = new mini.Form("inquiry_form");
			var data=top["unfreezeList"].getData();
			form1.setData(data);
			//buttonedit控件需要单独设置值
			//1.机构号
			mini.get("brchno").setValue(data.brchno);
			mini.get("brchno").setText(data.brchno);
			//2.客户代码
			mini.get("custno").setValue(data.custno);
			mini.get("custno").setText(data.custno);
            //3.保证金帐号
			mini.get("mgacno").setValue(data.mgacno);
			mini.get("mgacno").setText(data.mgacno);
			if(action == "detail"){
				form1.setEnabled(false);
				if(dataTp == "verify"){//待复核
					mini.get("btn_valid").setEnabled(false);//检验要素
					mini.get("btn_save").setEnabled(false);//保存交易
					mini.get("btn_verify").setEnabled(false);//提交复核
					mini.get("btn_refuse").setEnabled(true);//拒绝交易
					mini.get("btn_checkTrade").setEnabled(true);//复核交易
					mini.get("btn_close").setEnabled(false);//关闭界面
					mini.get("btn_tradeFee").setEnabled(false);//交易费用
					mini.get("btn_tradeProof").setEnabled(false);//交易凭证
					mini.get("btn_agencyFeeBtn").setEnabled(false);//会计传票
				}else if(dataTp == "effi"){//生效
					mini.get("btn_valid").setEnabled(false);//检验要素
					mini.get("btn_save").setEnabled(false);//保存交易
					mini.get("btn_verify").setEnabled(false);//提交复核
					mini.get("btn_refuse").setEnabled(false);//拒绝交易
					mini.get("btn_checkTrade").setEnabled(false);//复核交易
					mini.get("btn_close").setEnabled(false);//关闭界面
					mini.get("btn_tradeFee").setEnabled(true);//交易费用
					mini.get("btn_tradeProof").setEnabled(true);//交易凭证
					mini.get("btn_agencyFeeBtn").setEnabled(true);//会计传票
				}else if(dataTp == "handle"){//经办
					
					mini.get("btn_valid").setEnabled(false);//检验要素
					mini.get("btn_save").setEnabled(true);//保存交易
					mini.get("btn_verify").setEnabled(false);//提交复核
					mini.get("btn_refuse").setEnabled(false);//拒绝交易
					mini.get("btn_checkTrade").setEnabled(false);//复核交易
					mini.get("btn_close").setEnabled(false);//关闭界面
					mini.get("btn_tradeFee").setEnabled(false);//交易费用
					mini.get("btn_tradeProof").setEnabled(false);//交易凭证
					mini.get("btn_agencyFeeBtn").setEnabled(false);//会计传票
				}
			}else if(action == "edit"){
				form1.setEnabled(false);
				if(dataTp == "temp"){//暂存
					

				}else if(dataTp == "modify"){//待修改
					

					mini.get("btn_valid").setEnabled(false);//检验要素
					mini.get("btn_save").setEnabled(false);//保存交易
					mini.get("btn_verify").setEnabled(true);//提交复核
					mini.get("btn_refuse").setEnabled(false);//拒绝交易
					mini.get("btn_checkTrade").setEnabled(false);//复核交易
					mini.get("btn_close").setEnabled(false);//关闭界面
					mini.get("btn_tradeFee").setEnabled(false);//交易费用
					mini.get("btn_tradeProof").setEnabled(false);//交易凭证
					mini.get("btn_agencyFeeBtn").setEnabled(false);//会计传票
				}
			}

        }
    }

    //机构的查询
	function onInsQuery(e) {
		var btnEdit = this;
		mini.open({
			url :"/fund-capital/standard/InterBank/mini_system/MiniInstitutionSelectManages.jsp",
			title : "机构选择",
			width : 900,
			height : 600,
			ondestroy : function(action) {	
				if (action == "ok") 
				{
					var iframe = this.getIFrameEl();
					var data = iframe.contentWindow.GetData();
					data = mini.clone(data); //必须
					if (data) {
						btnEdit.setValue(data.instId);
						btnEdit.setText(data.instId);
						mini.get("brchno").setValue(data.instName);
						btnEdit.focus();
					}

					
				}

			}
		});
	}

    //客户的查询
	function onCustQuery(e) {
		var btnEdit = this;
		mini.open({
			url : CommonUtil.baseWebPath() +"/base/MiniCPManageYQ.jsp",
			title : "客户选择",
			width : 900,
			height : 600,
			ondestroy : function(action) {	
				if (action == "ok") 
				{
					var iframe = this.getIFrameEl();
					var data = iframe.contentWindow.GetData();
					data = mini.clone(data); //必须
					if (data) {
						btnEdit.setValue(data.party_id);
						btnEdit.setText(data.party_id);
						mini.get("cuname").setValue(data.party_name);
						btnEdit.focus();
						
					}

					
				}

			}
		});
	}

	//汇率的校验
	function onRateValidation(e) {
		if (e.isValid) {
			if (isRate(e.value) == false) {
				e.errorText = "汇率整数位最多17位，小数位最多2位!";
				e.isValid = false;
			}
		}
    }
	/* 汇率number(19,2)的验证 */
	function isRate(v) {
		
		var re = new RegExp("^[0-9]{1,17}([.]{1}[0-9]{1,17}){0,1}$");
		if (re.test(v)){
			return true;
		}else{
			return false;
		} 
		
	}

///////////////////////////事件///////////////////////////////////////////////////////////////////////////////
    //检验要素
	function valid(){
		var form = new mini.Form("inquiry_form");
		form.validate();
	}

    //保存
    function save(){
        ////debugger
     	var form = new mini.Form("#inquiry_form");
		//验证表单
		form.validate();
		if(form.isValid() == false){
			mini.alert("表单填写错误,请确认!","提示信息")
			return;
		}
    		   
		var data = form.getData(true); //获取表单多个控件的数据
		var param = mini.encode(data); //序列化成JSON
		
		CommonUtil.ajax({
					url:'/InquiryController/save',
					data:param ,
					callback:function(data){
						//mini.get("fDQVNO").setValue(data.desc);
            			mini.alert("保存成功!");
            			mini.get("btn_verify").setEnabled(true);
						
					}
		});
    		
	}

	//提交
	function verify(){
       	var form = new mini.Form("#inquiry_form");
		//验证表单
		form.validate();
		if(form.isValid() == false){
			mini.alert("表单填写错误,请确认!","提示信息")
			return;
		}
			
		var data = form.getData(); //获取表单多个控件的数据
		var param = mini.encode(data); //序列化成JSON
		
		CommonUtil.ajax({
			url:'/InquiryController/commit',
			data:param,
			callback:function(data){
				mini.alert("提交成功!","系统提示",function(value){
					//关闭并刷新
					setTimeout(function(){ top["win"].closeMenuTab() },100);
				});
				
			}
		});
			
	}

	//复核通过
    function checkTrade(){
       var form = new mini.Form("#inquiry_form");
		//验证表单
		form.validate();
		if(form.isValid() == false){
		mini.alert("表单填写错误,请确认!","提示信息")
		return;
		}
			
		var data = form.getData(); //获取表单多个控件的数据
		var param = mini.encode(data); //序列化成JSON
		
		CommonUtil.ajax({
			url:'/InquiryController/checkTradeTr',
			data:param ,
			callback:function(data){
				mini.alert("复核通过!","系统提示",function(value){
					//关闭并刷新
					setTimeout(function(){ top["win"].closeMenuTab() },100);
				});
				
			}
		});
		
	}

	//拒绝
	function refuse(){
       var form = new mini.Form("#inquiry_form");
		//验证表单
		form.validate();
		if(form.isValid() == false){
		mini.alert("表单填写错误,请确认!","提示信息")
		return;
		}
			
		var data = form.getData(); //获取表单多个控件的数据
		
		var param = mini.encode(data); //序列化成JSON
		
		CommonUtil.ajax({
			url:'/InquiryController/refuse',
			data:param ,
			callback:function(data){
				mini.alert("复核拒绝成功!","系统提示",function(value){
					//关闭并刷新
					setTimeout(function(){ top["win"].closeMenuTab() },100);
				});
				
			}
		});
			
	}
</script>
</html>