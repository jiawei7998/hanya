<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%> 
<html>
<head>
  <title>保证金-查询-列表</title>
  <script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
</head>
<body style="width:100%;height:100%;background:white">
	
	<fieldset>
		<legend><label>查询条件</label></legend>
			<div id="search_form"  >
                <input id="mgrgno" name="mgrgno" class="mini-textbox"  labelField="true"  label="保证金冻结编号：" style="width:310px;" emptyText="请输入保证金冻结编号" labelStyle="text-align:right;"/>
                <input id="custno" name="custno" class="mini-textbox"  labelField="true"  label="客户代码：" style="width:310px;" emptyText="请输入客户代码" labelStyle="text-align:right;"/>
				<input id="cuname" name="cuname" class="mini-textbox"  labelField="true"  label="客户名称：" style="width:310px;" emptyText="请输入客户名称" labelStyle="text-align:right;"/>
				<span style="float:right;margin-right: 90px">
					<a id="search_btn" class="mini-button" style="display: none"   onclick="search1">查询</a>
					<a id="clear_btn" class="mini-button" style="display: none"    onClick="clear">清空</a>
				</span>
			</div>
		   
	</fieldset>
	<span style="margin:2px;display: block;">
		<!-- <a id="add_btn" class="mini-button" style="display: none"   onclick="add">新增</a>
		<a id="edit_btn" class="mini-button" style="display: none"   onclick="edit">修改</a>
		<a id="del_btn" class="mini-button" style="display: none"   onclick="del">删除</a> -->
		<a id="detail_btn" class="mini-button" style="display: none"    onclick="detail">查看详情</a>
		<div id = "approveType" name = "approveType" labelField="true" label=""  class="mini-checkboxlist" 
			value="handle" textField="text" valueField="id" multiSelect="false" style="float:right; margin-right: 50px;" 
			labelStyle="text-align:right;" data="[{id:'handle',text:'经办'}]" >
		</div>
    </span>
    <div class="mini-fit" id="fitId" style="margin-top: 2px;">
		<div id="inq_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" 
		idField="fldNo"  pageSize="10" multiSelect="true" onRowdblclick="custDetail" sortMode="client"
		allowAlternating="true">
			<div property="columns" >
				<div type="indexcolumn" headerAlign="center"  align="center" width="40px">序号</div>
                <div type="checkcolumn"></div>
                
				<div field="mgrgno" headerAlign="center"  align="center" width="160px" allowSort="true" dataType="int">保证金冻结编号</div>    
				<div field="brchno" headerAlign="center" width="150px">机构号</div>                            
				<div field="custno"  headerAlign="center"   width="100px" align="center" >客户代码</div>
				<div field="cuname"    headerAlign="center"  width="180px" >客户名称</div>                                
				<div field="crcono"  headerAlign="center" width="150px">合同号</div>
				<div field="lcyqcy"   headerAlign="center"  align="center" width="200px">掉期/远期结售汇币种</div>
				<div field="lcyqam"  headerAlign="center"  align="center" width="200px" >掉期/远期结售汇金额</div>
				<div field="mgrate"  headerAlign="center"  align="center" width="120px" >保证金比例%</div>
				<div field="mgacno"  headerAlign="center"  align="center" width="120px" allowSort="true" dataType="int" >保证金帐号</div>
				<div field="suacno"  headerAlign="center"  align="center" width="120px" allowSort="true" dataType="int" >子户号</div>
				<div field="mgaccy"  headerAlign="center"  align="center" width="100px" allowSort="true" dataType="int">币种</div> 
                <div field="mgacam"   headerAlign="center"  width="160px"  align="center">金额</div>
                <div field="mgblam"   headerAlign="center"  width="160px"  align="center">余额</div>
			</div>
		</div>
	</div> 
	<script type="text/javascript">
		mini.parse();

		

		$(document).ready(function(){
			//控制按钮显示
			$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
				var grid = mini.get("inq_grid");
				grid.on("beforeload", function (e) {
					e.cancel = true;
					var pageIndex = e.data.pageIndex;
					var pageSize = e.data.pageSize;
					search(pageSize, pageIndex);
				});
				search(grid.pageSize, 0);
			});
		});

		top["inquiryList"]=window;
        function getData(){
            var grid = mini.get("inq_grid");
            var record =grid.getSelected(); 
            return record;
        }

		function search1(){
            var grid =mini.get("inq_grid");
            search(grid.pageSize,0); 
        }

		function search(pageSize,pageIndex){
            var form =new mini.Form("search_form");
            form.validate();
            if (form.isValid() == false) return;//表单验证
            var data =form.getData();//获取表单数据
            data['pageNumber']=pageIndex+1;
			data['pageSize']=pageSize;
			

			var url="";
			var approveType = mini.get("approveType").getValue();
			if(approveType == "handle"){//经办
				data.statCd='A'
				url = "/InquiryController/searchPageTempStorage";

			}
           /*  else if(approveType == "wait_modify"){//待修改
				data.statCd='A'
				url = "/InquiryController/searchPageWaitModify";

			}else if(approveType == "wait_verify"){//待复核
				data.statCd='A'
				url = "/InquiryController/searchPageWaitVerify";

			}else{//生效efficient
				data.statCd='A'
				url = "/InquiryController/searchPageEfficient";
			} */

            var param = mini.encode(data); //序列化成JSON
               CommonUtil.ajax({
            	url:url,
                data:param,
                callback:function(data){
                        
                    var grid =mini.get("inq_grid");
                    //设置分页
                    grid.setTotalCount(data.obj.total);
                    grid.setPageIndex(pageIndex);
                    grid.setPageSize(pageSize);
                    //设置数据
					grid.setData(data.obj.rows);
					
                }
            });
		} 

		
	

		//清空
		function clear(){
			var form =new mini.Form("search_form");
			form.clear();
			search1();
			
		}

		/* function checkBoxValuechanged(){
			var type = mini.get("approveType").getValue();
			if(type == "temp_storage"){//暂存
				mini.get("add_btn").setVisible(true);
				mini.get("edit_btn").setVisible(true);
				mini.get("detail_btn").setVisible(false);
				mini.get("del_btn").setVisible(true);
			}else if(type == "wait_modify"){//待修改
				mini.get("add_btn").setVisible(false);
				mini.get("edit_btn").setVisible(true);
				mini.get("detail_btn").setVisible(false);
				mini.get("del_btn").setVisible(false);
			}else if(type == "wait_verify"){//待复核
				mini.get("add_btn").setVisible(false);
				mini.get("edit_btn").setVisible(false);
				mini.get("detail_btn").setVisible(true);
				mini.get("del_btn").setVisible(false);
			}else{//生效
				mini.get("add_btn").setVisible(false);
				mini.get("edit_btn").setVisible(false);
				mini.get("detail_btn").setVisible(true);
				mini.get("del_btn").setVisible(false);
			}
			
			search1();
		} */

		//新增
		/* function add(){
			var url = CommonUtil.baseWebPath() + "/bond/FreezeEdit.jsp?action=new";
			var tab = { id: "freezeListAdd", name: "freezeListAdd", text: "保证金冻结-新增", url: url,showCloseButton:true,parentId:top['win'].tabs.getActiveTab().name };
			top['win'].showTab(tab);
		} */

		//修改
       /*  function edit(){
			var type = mini.get("approveType").getValue();
			var url = "";
			if(type == "temp_storage"){//暂存
				url = CommonUtil.baseWebPath() + "/bond/FreezeEdit.jsp?action=edit&dataTp=temp";	
			}else if(type == "wait_modify"){//待修改
				url = CommonUtil.baseWebPath() + "/bond/FreezeEdit.jsp?action=edit&dataTp=modify";	
			}
            var grid = mini.get("inq_grid");
            var  record =grid.getSelected();
            if(record){
                var tab = { id: "freezeListEdit", name: "freezeListEdit", text: "保证金冻结-修改", url: url,showCloseButton:true,parentId:top['win'].tabs.getActiveTab().name };
                top['win'].showTab(tab);
            }else{
                mini.alert("请选中一条记录!","系统提示"); 
            }
        } */

		//查看详情
        function detail(){
			var type = mini.get("approveType").getValue();
			var url = "";
			url = CommonUtil.baseWebPath() + "/bond/InquiryEdit.jsp?action=detail";	
            var grid = mini.get("inq_grid");
            var  record =grid.getSelected();
            if(record){
                var tab = { id: "inquiryListDetail", name: "inquiryListDetail", text: "保证金经办-详情", url: url,showCloseButton:true,parentId:top['win'].tabs.getActiveTab().name };
                top['win'].showTab(tab);
            }else{
                mini.alert("请选中一条记录!","系统提示"); 
            }
        }

		//删除
		/* function del(){
			var grid = mini.get("inq_grid");
            //获取选中行
            var rows =grid.getSelecteds();
            
            if (rows.length>0) {
                //把询价业务编号放在数组里
				var mgrgnos = new Array();
				for(var i=0;i<rows.length;i++){
					mgrgnos.push(rows[i].mgrgno);
				}
                
                mini.confirm("您确认要删除?","系统警告",function(value){   
                    if (value == "ok"){   
                        CommonUtil.ajax({
                            url : '/InquiryController/delete',
                            data : mgrgnos,
                            callback : function(data){
                                if("error.common.0000" == data.code){
                                    mini.alert("删除成功！","系统提示");
                                    var grid =mini.get("inq_grid");
                                    search(grid.pageSize,0);
                                }
                            }
                        });
                    
                    }   
                });       
                    
            }else{
                mini.alert("请选择一条记录!","系统提示");
            }
		} */

    	
       
       
    </script>




</body>
</html>
	