<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../../global.jsp"%> 
<html>
<head>
  <title>期权-询价授权-列表</title>
  <script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
</head>
<body style="width:100%;height:100%;background:white">
	
	<fieldset>
		<legend><label>查询条件</label></legend>
			<div id="search_form"  >
				<input id="CUSTNO" name="CUSTNO" class="mini-textbox"  labelField="true"  label="客户代码：" style="width:310px;" emptyText="请输入客户代码" labelStyle="text-align:right;"/>
				<!-- <input id="custNm" name="custNm" class="mini-textbox"  labelField="true"  label="客户名称：" style="width:310px;" emptyText="请输入客户名称" labelStyle="text-align:right;"/> -->
				<span style="float:right;margin-right: 90px">
					<a id="search_btn" class="mini-button" style="display: none"   onclick="search1">查询</a>
					<a id="clear_btn" class="mini-button" style="display: none"    onClick="clear">清空</a>
				</span>
			</div>
		   
	</fieldset>
	<span style="margin:2px;display: block;">
		<a id="add_btn" class="mini-button" style="display: none"   onclick="add">新增</a>
		<a id="edit_btn" class="mini-button" style="display: none"   onclick="edit">修改</a>
		<a id="detail_btn" class="mini-button" style="display: none"    onclick="detail">查看详情</a>
		<div id = "approveType" name = "approveType" labelField="true" label=""  class="mini-checkboxlist" 
			value="approve" textField="text" valueField="id" multiSelect="false" style="float:right; margin-right: 50px;" 
			labelStyle="text-align:right;" 
			data="[{id:'handle',text:'经办'},{id:'temp_storage',text:'暂存'},{id:'wait_modify',text:'待修改'},{id:'wait_verify',text:'待复核'},{id:'efficient',text:'生效'}]" onvaluechanged="checkBoxValuechanged">
		</div>
    </span>
    <div class="mini-fit" id="fitId" style="margin-top: 2px;">
		<div id="optInqAuth_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" 
		idField="fldNo"  pageSize="10" multiSelect="true" onRowdblclick="custDetail" sortMode="client"
		allowAlternating="true">
			<div property="columns" >
				<div type="indexcolumn" headerAlign="center"  align="center" width="40px">序号</div>
				<div type="checkcolumn"></div>
				<div field="fDQVNO" headerAlign="center"  align="center" width="160px" allowSort="true" dataType="int">询价业务编号</div>    
                <div field="bRCHNO" headerAlign="center" width="150px">机构号</div>
                <div field="fDQVTP" headerAlign="center" width="150px"  renderer="CommonUtil.dictRenderer" data-options="{dict:'inqDirec'}">询价方向</div>  
                                           
				<div field="cUSTNO"  headerAlign="center"   width="100px" align="center" >客户代码</div>
				<div field="cUNAME"    headerAlign="center"  width="180px" >客户名称</div>                                
				<div field="cRCONO"  headerAlign="center" width="150px">合同号</div>
                <div field="jSYQNO"   headerAlign="center"  align="center" width="150px">相关业务编号</div>
                
				<div field="jSYQTP"  headerAlign="center"  align="center" width="100px"  >交易类型</div>
                <div field="jSXJTP"  headerAlign="center"  align="center" width="120px"  renderer="CommonUtil.dictRenderer" data-options="{dict:'InquiryType'}">询价业务类型</div>
                <div field="uPDOWN"  headerAlign="center"  align="center" width="100px"  renderer="CommonUtil.dictRenderer" data-options="{dict:'upDownType'}">看涨/看跌</div>

				<div field="cUCYNO"  headerAlign="center"  align="center" width="120px"  renderer="CommonUtil.dictRenderer" data-options="{dict:'Currency'}">合同币种（外币）</div>
                <div field="tRAMTS"  headerAlign="center"  align="center" width="160px" allowSort="true" dataType="int" >合同金额（外币）</div>
                <div field="cONTRT"   headerAlign="center"  width="160px"  align="center">执行价格</div>

                <div field="tIMETP"  headerAlign="center"  align="center" width="100px" allowSort="true" dataType="int">期权期限</div>
                <div field="jQRATE"   headerAlign="center"  width="160px"  align="center">即期汇率</div>

				<div field="cUSTFR"  headerAlign="center"  align="center"  width="150px" >对客期权费率</div>
                <div field="cUSTPR"  headerAlign="center"   width="150px" align="center" >对客期权费(人民币)</div>
                <div field="bANKFR"  headerAlign="center"  align="center"  width="150px" >总行平盘期权费率</div>
                <div field="bANKPR"  headerAlign="center"   width="150px" align="center" >总行平盘期权费(人民币)</div>
                
				<div field="rEMARK"   headerAlign="center"  width="160px"  align="center">备注</div>
			</div>
		</div>
	</div> 
	<script type="text/javascript">
		mini.parse();

		$(document).ready(function(){
			//控制按钮显示
			$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
				//初始化为经办列表页面
				mini.get("approveType").setValue("handle");
				//经办列表页面,新增修改按钮要隐藏
				mini.get("add_btn").setVisible(false);
				mini.get("edit_btn").setVisible(false);

				var grid = mini.get("optInqAuth_grid");
				grid.on("beforeload", function (e) {
					e.cancel = true;
					var pageIndex = e.data.pageIndex;
					var pageSize = e.data.pageSize;
					search(pageSize, pageIndex);
				});
				search(grid.pageSize, 0);
			});
		});

		top["optInquiryAuthList"]=window;
        function getData(){
            var grid = mini.get("optInqAuth_grid");
            var record =grid.getSelected(); 
            return record;
        }

		function search1(){
            var grid =mini.get("optInqAuth_grid");
            search(grid.pageSize,0); 
        }

		function search(pageSize,pageIndex){
            var form =new mini.Form("search_form");
            form.validate();
            if (form.isValid() == false) return;//表单验证
            var data =form.getData();//获取表单数据
            data['pageNumber']=pageIndex+1;
			data['pageSize']=pageSize;
			//询价类型：远期FDQV、掉期SPQV、期权OPQV、利率互换IRQV
			data.inqType='OPQV';

			var url="";
			var approveType = mini.get("approveType").getValue();
			if(approveType == "handle"){//经办
				data.newStatCd='C'
				data.statCd='A';
				url = "/InquiryController/searchPageOperate";

			}else if(approveType == "temp_storage"){//暂存
				data.statCd='C';
				url = "/InquiryController/searchPageTempStorage";

			}else if(approveType == "wait_modify"){//待修改
				data.statCd='C';
				url = "/InquiryController/searchPageWaitModify";

			}else if(approveType == "wait_verify"){//待复核
				data.statCd='C';
				url = "/InquiryController/searchPageWaitVerify";

			}else{//生效efficient
				data.statCd='C';
				url = "/InquiryController/searchPageEfficient";
			}

            var param = mini.encode(data); //序列化成JSON
               CommonUtil.ajax({
            	url:url,
                data:param,
                callback:function(data){
                        
                    var grid =mini.get("optInqAuth_grid");
                    //设置分页
                    grid.setTotalCount(data.obj.total);
                    grid.setPageIndex(pageIndex);
                    grid.setPageSize(pageSize);
                    //设置数据
					grid.setData(data.obj.rows);
					
                }
            });
		} 

		//清空
		function clear(){
			var form =new mini.Form("search_form");
			form.clear();
			search1();
			
		}

		function checkBoxValuechanged(){
			var type = mini.get("approveType").getValue();
			if(type == "handle"){//经办
				mini.get("add_btn").setVisible(false);
				mini.get("edit_btn").setVisible(false);
				mini.get("detail_btn").setVisible(true);
			}else if(type == "temp_storage"){//暂存
				mini.get("add_btn").setVisible(false);
				mini.get("edit_btn").setVisible(true);
				mini.get("detail_btn").setVisible(false);
			}else if(type == "wait_modify"){//待修改
				mini.get("add_btn").setVisible(false);
				mini.get("edit_btn").setVisible(true);
				mini.get("detail_btn").setVisible(false);
			}else if(type == "wait_verify"){//待复核
				mini.get("add_btn").setVisible(false);
				mini.get("edit_btn").setVisible(false);
				mini.get("detail_btn").setVisible(true);
			}else{//生效
				mini.get("add_btn").setVisible(false);
				mini.get("edit_btn").setVisible(false);
				mini.get("detail_btn").setVisible(true);
			}
			
			search1();
		}

		//新增
		function add(){
			var url = CommonUtil.baseWebPath() + "/inquiry/fxopt/OptInquiryAuthEdit.jsp?action=new";
			var tab = { id: "optInquiryAuthListAdd", name: "optInquiryAuthListAdd", text: "期权-询价授权新增", url: url,showCloseButton:true,parentId:top['win'].tabs.getActiveTab().name };
			top['win'].showTab(tab);
		}

		//修改
        function edit(){
			var type = mini.get("approveType").getValue();
			var url = "";
			if(type == "temp_storage"){//暂存
				url = CommonUtil.baseWebPath() + "/inquiry/fxopt/OptInquiryAuthEdit.jsp?action=edit&dataTp=temp";	
			}else if(type == "wait_modify"){//待修改
				url = CommonUtil.baseWebPath() + "/inquiry/fxopt/OptInquiryAuthEdit.jsp?action=edit&dataTp=modify";	
			}
            var grid = mini.get("optInqAuth_grid");
            var  record =grid.getSelected();
            if(record){
                var tab = { id: "optInquiryAuthListEdit", name: "optInquiryAuthListEdit", text: "期权-询价授权修改", url: url,showCloseButton:true,parentId:top['win'].tabs.getActiveTab().name };
                top['win'].showTab(tab);
            }else{
                mini.alert("请选中一条记录!","系统提示"); 
            }
        }

		//查看详情
        function detail(){
			var type = mini.get("approveType").getValue();
			var url = "";
			if(type == "handle"){//经办
				url = CommonUtil.baseWebPath() + "/inquiry/fxopt/OptInquiryAuthEdit.jsp?action=detail&dataTp=handle";	
			}else if(type == "wait_verify"){//待复核
				url = CommonUtil.baseWebPath() + "/inquiry/fxopt/OptInquiryAuthEdit.jsp?action=detail&dataTp=verify";	
			}else if(type == "efficient"){//生效
				url = CommonUtil.baseWebPath() + "/inquiry/fxopt/OptInquiryAuthEdit.jsp?action=detail&dataTp=effi";	
			}

            var grid = mini.get("optInqAuth_grid");
            var  record =grid.getSelected();
            if(record){
                
                var tab = { id: "optInquiryAuthListDetail", name: "optInquiryAuthListDetail", text: "期权-询价授权详情", url: url,showCloseButton:true,parentId:top['win'].tabs.getActiveTab().name };
                top['win'].showTab(tab);
            }else{
                mini.alert("请选中一条记录!","系统提示"); 
            }
        }


		
	
	
	
	</script>




</body>
</html>
	