<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../../global.jsp"%>
<html>
  <head>
  <title>掉期-询价-维护</title>
  <script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
  </head>
  
  <body style="width:100%;height:100%;background:white">
		<div class="mini-splitter" style="width:100%;height:100%;">
			<div size="90%" showCollapseButton="true">
				<div class="mini-fit"  style="background:white" id="inquiry_form">
						<!-- <fieldset class="mini-fieldset title">
							<legend>询价</legend> -->
							<table  width="100%" class="mini-table">
								<tr>
                                    <td><input class="mini-textbox"  id ="fDQVNO" name="fDQVNO"   style="width:400px"labelField="true" label="询价业务编号"  emptyText="系统自动生成" enabled="false"/></td>
									<td><input class="mini-buttonedit"  onbuttonclick="onInsQuery" id="bRCHNO" name="bRCHNO"  style="width:400px" labelField="true" label="机构号" emptyText=""   /></td>
								</tr>
								<tr>
                                    <td><input class="mini-buttonedit" onbuttonclick="onCustQuery"  id="cUSTNO"  name="cUSTNO" style="width:400px"  labelField="true" label="客户代码" emptyText="请选择客户代码" required="true" /></td>
                                    <td><input class="mini-textbox"  id="cUNAME"  name="cUNAME" style="width:400px" labelField="true" label="客户名称" emptyText="同步反显" enabled="false"  /></td>
								</tr>
								<tr>
									<td><input class="mini-textbox" id="cRCONO"  name="cRCONO" labelField="true" label="合同号" style="width:400px"   vtype="maxLength:30" onvalidation="onEnglishAndNumberValidation" required="false" emptyText="" ></td>
									<td><input class="mini-textbox" id="jSYQNO"  name="jSYQNO" labelField="true" label="相关业务编号" style="width:400px"   vtype="maxLength:20" onvalidation="onEnglishAndNumberValidation" required="false" emptyText="" ></td>
								</tr>
								<tr>
									<td><input class="mini-combobox" id="jSYQTP"  name="jSYQTP" style="width:400px" labelField="true" label="结汇/售汇"  emptyText="" required="true"  data = "CommonUtil.serverData.dictionary.JHSH"/></td>
									<td><input class="mini-combobox" id="jSXJTP"  name="jSXJTP" style="width:400px" labelField="true" label="询价业务类型"  emptyText="" required="true"   data = "CommonUtil.serverData.dictionary.InquiryType" /></td>
								</tr>

								<tr>
									<td><input class="mini-combobox" id="cUCYNO"  name="cUCYNO" style="width:400px" labelField="true" label="合同币种（外币）"  emptyText="" required="true"  data = "CommonUtil.serverData.dictionary.Currency" /></td>
                                    <td><input class="mini-textbox" id="tRAMTS"  name="tRAMTS" style="width:400px" labelField="true" label="合同金额（外币）" onvalidation="onMoneyValidation" emptyText="" required="true" /></td>
								</tr>
								<tr>
									<td><input class="mini-textbox" id="sPOTFR"  name="sPOTFR" labelField="true" label="近端对客汇率" style="width:400px"    onvalidation="onRateValidation" required="true"  emptyText="" ></td>
									<td><input class="mini-textbox" id="fORWFR"  name="fORWFR" labelField="true" label="远端对客汇率" style="width:400px"    onvalidation="onRateValidation" required="true"  emptyText="" ></td>
								</tr>
								<tr>
									
									<td><input class="mini-combobox"  id="tIMETP"  name="tIMETP" style="width:400px" labelField="true" label="远期汇率期限"  emptyText="" required="true"  data = "CommonUtil.serverData.dictionary.Term"/></td>
									<td><input class="mini-textbox" id="rEMARK"  name="rEMARK" style="width:400px" labelField="true" label="备注"  vtype="maxLength:70" emptyText=""/></td>
								</tr>
								
								
							</table>
						<!-- </fieldset> -->
						
				</div>
			</div>
			<div id="functionIds" showCollapseButton="true">
				<div style="margin-bottom:10px; margin-top:5px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_valid"   onclick="valid()">校验要素</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_save"          onclick="save">保存交易</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_verify"    enabled="false" onclick="verify">提交复核</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_refuse"        enabled="false" onclick="refuse">拒绝交易</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_checkTrade"        enabled="false"   onclick="checkTrade">复核交易</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_close"              onclick="close">关闭界面</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_tradeFee"     enabled="false" onclick="tradeFee">交易费用</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_tradeProof"      enabled="false" onclick="tradeProof">交易凭证</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_agencyFeeBtn"     enabled="false"    onclick="">会计传票</a></div>
			</div>      
		</div>


</body>

<script type="text/javascript">
    mini.parse();

    var url = window.location.search;
    var action = CommonUtil.getParam(url,"action");
    var dataTp = CommonUtil.getParam(url,"dataTp");

    $(document).ready(function(){
		//mini.get("custNo").doValueChanged();
        init();
    });

    function init(){
		if(action == "new"){
			//$("#labell").html("<a href='javascript:CommonUtil.activeTab();'>科目管理</a> >> 新增");
			
		}else if(action == "edit" || action == "detail"){
            var form1 = new mini.Form("inquiry_form");
			var data=top["swapInquiryList"].getData();
			form1.setData(data);
			//buttonedit控件需要单独设置值
			//1.机构号
			mini.get("bRCHNO").setValue(data.bRCHNO);
			mini.get("bRCHNO").setText(data.bRCHNO);
			//2.客户代码
			mini.get("cUSTNO").setValue(data.cUSTNO);
			mini.get("cUSTNO").setText(data.cUSTNO);
			if(action == "detail"){
				form1.setEnabled(false);
				if(dataTp == "verify"){//待复核
					mini.get("btn_valid").setEnabled(false);//检验要素
					mini.get("btn_save").setEnabled(false);//保存交易
					mini.get("btn_verify").setEnabled(false);//提交复核
					mini.get("btn_refuse").setEnabled(true);//拒绝交易
					mini.get("btn_checkTrade").setEnabled(true);//复核交易
					mini.get("btn_close").setEnabled(false);//关闭界面
					mini.get("btn_tradeFee").setEnabled(false);//交易费用
					mini.get("btn_tradeProof").setEnabled(false);//交易凭证
					mini.get("btn_agencyFeeBtn").setEnabled(false);//会计传票
				}else if(dataTp == "effi"){//生效
					mini.get("btn_valid").setEnabled(false);//检验要素
					mini.get("btn_save").setEnabled(false);//保存交易
					mini.get("btn_verify").setEnabled(false);//提交复核
					mini.get("btn_refuse").setEnabled(false);//拒绝交易
					mini.get("btn_checkTrade").setEnabled(false);//复核交易
					mini.get("btn_close").setEnabled(false);//关闭界面
					mini.get("btn_tradeFee").setEnabled(true);//交易费用
					mini.get("btn_tradeProof").setEnabled(true);//交易凭证
					mini.get("btn_agencyFeeBtn").setEnabled(true);//会计传票
				}
			}else if(action == "edit"){
				if(dataTp == "modify"){//待修改
					mini.get("btn_valid").setEnabled(false);//检验要素
					mini.get("btn_save").setEnabled(false);//保存交易
					mini.get("btn_verify").setEnabled(true);//提交复核
					mini.get("btn_refuse").setEnabled(false);//拒绝交易
					mini.get("btn_checkTrade").setEnabled(false);//复核交易
					mini.get("btn_close").setEnabled(false);//关闭界面
					mini.get("btn_tradeFee").setEnabled(false);//交易费用
					mini.get("btn_tradeProof").setEnabled(false);//交易凭证
					mini.get("btn_agencyFeeBtn").setEnabled(false);//会计传票
				}
			}

        }
    }

    //机构的查询
	function onInsQuery(e) {
		var btnEdit = this;
		mini.open({
			url : "/fund-capital/standard/InterBank/mini_system/MiniInstitutionSelectManages.jsp",
			title : "机构选择",
			width : 900,
			height : 600,
			ondestroy : function(action) {	
				if (action == "ok") 
				{
					var iframe = this.getIFrameEl();
					var data = iframe.contentWindow.GetData();
					data = mini.clone(data); //必须
					if (data) {
						btnEdit.setValue(data.instId);
						btnEdit.setText(data.instId);
						//mini.get("bRCHNO").setValue(data.instName);
						btnEdit.focus();
					}

					
				}

			}
		});
	}

    //客户的查询
	function onCustQuery(e) {
		var btnEdit = this;
		mini.open({
			url : CommonUtil.baseWebPath() +"/base/MiniCPManageYQ.jsp",
			title : "客户选择",
			width : 900,
			height : 600,
			ondestroy : function(action) {	
				if (action == "ok") 
				{
					var iframe = this.getIFrameEl();
					var data = iframe.contentWindow.GetData();
					data = mini.clone(data); //必须
					if (data) {
						btnEdit.setValue(data.party_id);
						btnEdit.setText(data.party_id);
						mini.get("cUNAME").setValue(data.party_name);
						btnEdit.focus();
						
					}

					
				}

			}
		});
	}
	//合同金额（外币）的校验
	function onMoneyValidation(e) {
		////debugger
		if (e.isValid) {
			if (isMoney(e.value) == false) {
				e.errorText = "金额整数位最多17位，小数位最多2位!";
				e.isValid = false;
			}
		}
    }
	//合同号、相关业务编号的校验
	function onEnglishAndNumberValidation(e) {
		if(e.value == null || e.value == ""){//可以为空，为空不需要校验
			return;
		}
		if (e.isValid) {
			if (isEnglishAndNumber(e.value) == false) {
				e.errorText = "必须输入英文+数字!";
				e.isValid = false;
			}
		}
    }
	 /* 是否英文+数字 */
	 function isEnglishAndNumber(v) {
            
		var re = new RegExp("^[0-9a-zA-Z\_]+$");
		if (re.test(v)) return true;
		return false;
	}


	/* 金额number(19,2)的验证 */
	function isMoney(v) {
		
		//var re = new RegExp("^([1-9][0-9]{0,16})|(([0]\.\d{1,2})|([1-9][0-9]{0,16}\.\d{1,2}))$");
		var re = new RegExp("^[0-9]{1,17}([.]{1}[0-9]{1,2}){0,1}$");
		//var re = new RegExp("^\d{1,17}(\.\d{1,2})?$");
		if (re.test(v)){
			return true;
		}else{
			return false;
		} 
		
	}

	//近端对客汇率、远端对客汇率的校验
	function onRateValidation(e) {
		if (e.isValid) {
			if (isRate(e.value) == false) {
				e.errorText = "整数位最多12位，小数位最多7位!";
				e.isValid = false;
			}
		}
    }
    /* number(19,7)的验证 */
	function isRate(v) {
		var re = new RegExp("^[0-9]{1,12}([.]{1}[0-9]{1,7}){0,1}$");
		if (re.test(v)){
			return true;
		}else{
			return false;
		} 
		
	}


///////////////////////////事件///////////////////////////////////////////////////////////////////////////////
    //检验要素
	function valid(){
		var form = new mini.Form("inquiry_form");
		form.validate();
	}

    //保存
    function save(){
        ////debugger
     	var form = new mini.Form("#inquiry_form");
		//验证表单
		form.validate();
		if(form.isValid() == false){
			mini.alert("表单填写错误,请确认!","提示信息")
			return;
		}
    		   
		var data = form.getData(true); //获取表单多个控件的数据
        if(data.jSYQTP == "1"){
            data.jSYQTP = "结汇";
        }else if(data.jSYQTP == "2"){
            data.jSYQTP = "售汇";
        }
		data.userSt='S'
		data.STATCD='A'
		//询价类型：远期FDQV、掉期SPQV、期权OPQV、利率互换IRQV
		data.inqType='SPQV';
		var param = mini.encode(data); //序列化成JSON
		
		CommonUtil.ajax({
					url:'/InquiryController/save',
					data:param ,
					callback:function(data){
						mini.get("fDQVNO").setValue(data.desc);
            			mini.alert("保存成功!");
            			mini.get("btn_verify").setEnabled(true);
						
					}
		});
    		
	}

	//提交
	function verify(){
       	var form = new mini.Form("#inquiry_form");
		//验证表单
		form.validate();
		if(form.isValid() == false){
			mini.alert("表单填写错误,请确认!","提示信息")
			return;
		}
			
		var data = form.getData(); //获取表单多个控件的数据
		if(data.jSYQTP == "1"){
            data.jSYQTP = "结汇";
        }else if(data.jSYQTP == "2"){
            data.jSYQTP = "售汇";
        }
		data.userSt='Y'
		var param = mini.encode(data); //序列化成JSON
		
		CommonUtil.ajax({
			url:'/InquiryController/commit',
			data:param,
			callback:function(data){
				mini.alert("提交成功!","系统提示",function(value){
                    //关闭并刷新
					setTimeout(function(){ top["win"].closeMenuTab() },100);
                });
				
			}
		});
			
	}

	//复核通过
    function checkTrade(){
       var form = new mini.Form("#inquiry_form");
		//验证表单
		form.validate();
		if(form.isValid() == false){
		mini.alert("表单填写错误,请确认!","提示信息")
		return;
		}
			
		var data = form.getData(); //获取表单多个控件的数据
		if(data.jSYQTP == "1"){
            data.jSYQTP = "结汇";
        }else if(data.jSYQTP == "2"){
            data.jSYQTP = "售汇";
        }
		data.userSt='Y'
		data.chckSt='Y'
		data.STATCD='A'
		var param = mini.encode(data); //序列化成JSON
		
		CommonUtil.ajax({
			url:'/InquiryController/checkTradeTr',
			data:param ,
			callback:function(data){
				mini.alert("复核通过!","系统提示",function(value){
					//关闭并刷新
					setTimeout(function(){ top["win"].closeMenuTab() },100);
				});
				
			}
		});
		
	}

	//拒绝
	function refuse(){
       var form = new mini.Form("#inquiry_form");
		//验证表单
		form.validate();
		if(form.isValid() == false){
		mini.alert("表单填写错误,请确认!","提示信息")
		return;
		}
			
		var data = form.getData(); //获取表单多个控件的数据
		if(data.jSYQTP == "1"){
            data.jSYQTP = "结汇";
        }else if(data.jSYQTP == "2"){
            data.jSYQTP = "售汇";
        }
		data.userSt='Y'
		data.chckSt='N'
		var param = mini.encode(data); //序列化成JSON
		
		CommonUtil.ajax({
			url:'/InquiryController/refuse',
			data:param ,
			callback:function(data){
				mini.alert("复核拒绝成功!","系统提示",function(value){
					//关闭并刷新
					setTimeout(function(){ top["win"].closeMenuTab() },100);
				});
				
			}
		});
			
	}
</script>
</html>