<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../../global.jsp"%>
<html>
  <head>
  <title>利率互换-询价-维护</title>
  <script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
  </head>
  
  <body style="width:100%;height:100%;background:white">
		<div class="mini-splitter" style="width:100%;height:100%;">
			<div size="90%" showCollapseButton="true">
				<div class="mini-fit"  style="background:white" id="inquiry_form">
					<fieldset>
                        <legend>业务录入</legend>
                        <table  width="100%" class="mini-table">
                            <tr>
                                <td><input class="mini-textbox"  id="fDQVNO"  name="fDQVNO" style="width:400px" labelField="true" label="询价业务编号"  labelStyle="width:135px" emptyText="系统自动生成" enabled="false"  /></td>
                                <td><input class="mini-buttonedit"  onbuttonclick="onInsQuery" id="bRCHNO" name="bRCHNO"  style="width:400px" labelField="true" label="机构号"  labelStyle="width:135px" emptyText=""   /></td>
                            </tr>
                            <tr>
                                <td><input class="mini-buttonedit" onbuttonclick="onCustQuery"  id="cUSTNO"  name="cUSTNO" style="width:400px"  labelField="true" label="客户代码"  required="true"  labelStyle="width:135px" emptyText="" /></td>
                                <td><input class="mini-textbox" id="cUNAME"  name="cUNAME" labelField="true" label="客户名称"  labelStyle="width:135px" style="width:400px"  emptyText="同步反显" enabled="false"></td>
                            </tr>
                            <tr>
                                
                                <td><input class="mini-textbox" id="pRNAME"  name="pRNAME" labelField="true" label="产品名称" required="true"  vtype="maxLength:50" style="width:400px" emptyText=""  labelStyle="width:135px" ></td>
                                <td><input class="mini-combobox" id="lTISTP"  name="lTISTP" labelField="true" label="利率互换类型"  required="true"  labelStyle="width:135px" style="width:400px" emptyText="" data = "CommonUtil.serverData.dictionary.IRS_type" ></td>
                            </tr>
                            <tr>
                                
                                <td><input class="mini-textarea" id="rEMARK"  name="rEMARK" style="width:400px" labelField="true" label="备注" vtype="maxLength:70"  labelStyle="width:135px" emptyText="" /></td>  
                                <td><input class="mini-combobox" id="cUCYNO"  name="cUCYNO" labelField="true" label="币种"  required="true"  labelStyle="width:135px" style="width:400px" emptyText="" data = "CommonUtil.serverData.dictionary.Currency" ></td>
                            </tr>
                            <tr>
                                
                                <td><input class="mini-textbox" id="tRAMTS"  name="tRAMTS" style="width:400px" labelField="true" label="利率互换本金"  onvalidation="onMoneyValidation" required="true"  labelStyle="width:135px" emptyText="" /></td>
                                <td><input class="mini-textbox" id="tRAMTS1"  name="tRAMTS1" style="width:400px" labelField="true" label="前端费" onvalidation="onRateValidation" required="true"   labelStyle="width:135px"  emptyText="" /></td>
                            </tr>
                           

                        </table>
                    </fieldset>
                    <fieldset>
                        <legend>付</legend>
                        <table width="100%" class="mini-table">
                            <tr>
                                <td><input class="mini-combobox" id="fORMU1"  name="fORMU1" style="width:400px" labelField="true" label="计息方式"  required="true"  labelStyle="width:135px" emptyText="" data = "CommonUtil.serverData.dictionary.calIrsMethod"/></td>
                                <td><input class="mini-combobox" id="fTYPE1"  name="fTYPE1" style="width:400px" labelField="true" label="固定/浮动利息"   required="true"  labelStyle="width:135px" emptyText="" data = "CommonUtil.serverData.dictionary.fixFloat" /></td>
                            </tr>
                            <tr>
                                <td><input class="mini-textbox"  id="iRATE1"  name="iRATE1" style="width:400px" labelField="true" label="利率(%)"   onvalidation="onRateValidation" required="true"  labelStyle="width:135px" emptyText="" /></td>
                                <td><input class="mini-textbox" id="iPIPS1"  name="iPIPS1" style="width:400px" labelField="true" label="利率点差"   onvalidation="onRateValidation"labelStyle="width:135px" emptyText="" /></td>
                            </tr>
                            <tr>
                                <td><input class="mini-combobox" id="iCURVE1"  name="iCURVE1" style="width:400px" labelField="true" label="浮动利率基准曲线"   labelStyle="width:135px" emptyText="" data = "CommonUtil.serverData.dictionary.liborShibor"/></td>
                                <td><input class="mini-combobox" id="bAND1"  name="bAND1" style="width:400px" labelField="true" label="浮动利率期限"   labelStyle="width:135px" emptyText="" data = "CommonUtil.serverData.dictionary.Term" /></td> 
                            </tr>
                            <tr>
                                <td><input class="mini-textbox" id="oFFSET1"  name="oFFSET1" style="width:400px" labelField="true" label="天数偏移"  onvalidation="onRateValidation"  labelStyle="width:135px" emptyText="" /></td> 
                                <td><input class="mini-combobox" id="dAYCV1"  name="dAYCV1" style="width:400px" labelField="true" label="计息天数"   labelStyle="width:135px" emptyText="" data = "CommonUtil.serverData.dictionary.calIrsDays" /></td> 
                            </tr>
                            <tr>
                                <td><input class="mini-combobox" id="pAFREQ"  name="pAFREQ" style="width:400px" labelField="true" label="付息频率"   required="true"  labelStyle="width:135px" emptyText="" data = "CommonUtil.serverData.dictionary.payIrsFreq"/></td>
                                <td><input class="mini-combobox" id="rTFRE1"  name="rTFRE1" style="width:400px" labelField="true" label="利率重置频率"   labelStyle="width:135px" emptyText="" data = "CommonUtil.serverData.dictionary.payIrsFreq" /></td>   
                            </tr>
                            
                        </table>
                    </fieldset>
                    <fieldset>
                        <legend>收</legend>
                        <table width="100%" class="mini-table">
                            <tr>
                                <td><input class="mini-combobox" id="fORMU2"  name="fORMU2" style="width:400px" labelField="true" label="计息方式"  required="true"  labelStyle="width:135px" emptyText="" data = "CommonUtil.serverData.dictionary.calIrsMethod"/></td>
                                <td><input class="mini-combobox" id="fTYPE2"  name="fTYPE2" style="width:400px" labelField="true" label="固定/浮动利息"   required="true"  labelStyle="width:135px" emptyText="" data = "CommonUtil.serverData.dictionary.fixFloat" /></td>
                            </tr>
                            <tr>
                                <td><input class="mini-textbox"  id="iRATE2"  name="iRATE2" style="width:400px" labelField="true" label="利率(%)"  onvalidation="onRateValidation" required="true"  labelStyle="width:135px" emptyText="" /></td>
                                <td><input class="mini-textbox" id="iPIPS2"  name="iPIPS2" style="width:400px" labelField="true" label="利率点差"  onvalidation="onRateValidation" labelStyle="width:135px" emptyText="" /></td>
                            </tr>
                            <tr>
                                <td><input class="mini-combobox" id="iCURVE2"  name="iCURVE2" style="width:400px" labelField="true" label="浮动利率基准曲线"   labelStyle="width:135px" emptyText="" data = "CommonUtil.serverData.dictionary.liborShibor"/></td>
                                <td><input class="mini-combobox" id="bAND2"  name="bAND2" style="width:400px" labelField="true" label="浮动利率期限"   labelStyle="width:135px" emptyText="" data = "CommonUtil.serverData.dictionary.Term" /></td> 
                            </tr>
                            <tr>
                                <td><input class="mini-textbox" id="oFFSET2"  name="oFFSET2" style="width:400px" labelField="true" label="天数偏移"  onvalidation="onRateValidation" labelStyle="width:135px" emptyText="" /></td> 
                                <td><input class="mini-combobox" id="dAYCV2"  name="dAYCV2" style="width:400px" labelField="true" label="计息天数"   labelStyle="width:135px" emptyText="" data = "CommonUtil.serverData.dictionary.calIrsDays" /></td> 
                            </tr>
                            <tr>
                                <td><input class="mini-combobox" id="rEFREQ"  name="rEFREQ" style="width:400px" labelField="true" label="收息频率"   required="true"  labelStyle="width:135px" emptyText="" data = "CommonUtil.serverData.dictionary.payIrsFreq"/></td>
                                <td><input class="mini-combobox" id="rTFRE2"  name="rTFRE2" style="width:400px" labelField="true" label="利率重置频率"   labelStyle="width:135px" emptyText="" data = "CommonUtil.serverData.dictionary.payIrsFreq" /></td>   
                            </tr>
                           
                        </table>
                    </fieldset>	
                                    
						
				</div>
			</div>
			<div id="functionIds" showCollapseButton="true">
				<div style="margin-bottom:10px; margin-top:5px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_valid"   onclick="valid()">校验要素</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_save"          onclick="save">保存交易</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_verify"    enabled="false" onclick="verify">提交复核</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_refuse"        enabled="false" onclick="refuse">拒绝交易</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_checkTrade"        enabled="false"   onclick="checkTrade">复核交易</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_close"              onclick="close">关闭界面</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_tradeFee"     enabled="false" onclick="tradeFee">交易费用</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_tradeProof"      enabled="false" onclick="tradeProof">交易凭证</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_agencyFeeBtn"     enabled="false"    onclick="">会计传票</a></div>
			</div>      
		</div>


</body>

<script type="text/javascript">
    mini.parse();

    var url = window.location.search;
    var action = CommonUtil.getParam(url,"action");
    var dataTp = CommonUtil.getParam(url,"dataTp");

    $(document).ready(function(){
        init();
    });

    function init(){
		if(action == "new"){
			
		}else if(action == "edit" || action == "detail"){
            var form1 = new mini.Form("inquiry_form");
			var data=top["irsInquiryList"].getData();
			form1.setData(data);
			//buttonedit控件需要单独设置值
			//1.机构号
			mini.get("bRCHNO").setValue(data.bRCHNO);
			mini.get("bRCHNO").setText(data.bRCHNO);
			//2.客户代码
			mini.get("cUSTNO").setValue(data.cUSTNO);
			mini.get("cUSTNO").setText(data.cUSTNO);
			if(action == "detail"){
				form1.setEnabled(false);
				if(dataTp == "verify"){//待复核
					mini.get("btn_valid").setEnabled(false);//检验要素
					mini.get("btn_save").setEnabled(false);//保存交易
					mini.get("btn_verify").setEnabled(false);//提交复核
					mini.get("btn_refuse").setEnabled(true);//拒绝交易
					mini.get("btn_checkTrade").setEnabled(true);//复核交易
					mini.get("btn_close").setEnabled(false);//关闭界面
					mini.get("btn_tradeFee").setEnabled(false);//交易费用
					mini.get("btn_tradeProof").setEnabled(false);//交易凭证
					mini.get("btn_agencyFeeBtn").setEnabled(false);//会计传票
				}else if(dataTp == "effi"){//生效
					mini.get("btn_valid").setEnabled(false);//检验要素
					mini.get("btn_save").setEnabled(false);//保存交易
					mini.get("btn_verify").setEnabled(false);//提交复核
					mini.get("btn_refuse").setEnabled(false);//拒绝交易
					mini.get("btn_checkTrade").setEnabled(false);//复核交易
					mini.get("btn_close").setEnabled(false);//关闭界面
					mini.get("btn_tradeFee").setEnabled(true);//交易费用
					mini.get("btn_tradeProof").setEnabled(true);//交易凭证
					mini.get("btn_agencyFeeBtn").setEnabled(true);//会计传票
				}
			}else if(action == "edit"){
				if(dataTp == "modify"){//待修改
					mini.get("btn_valid").setEnabled(false);//检验要素
					mini.get("btn_save").setEnabled(false);//保存交易
					mini.get("btn_verify").setEnabled(true);//提交复核
					mini.get("btn_refuse").setEnabled(false);//拒绝交易
					mini.get("btn_checkTrade").setEnabled(false);//复核交易
					mini.get("btn_close").setEnabled(false);//关闭界面
					mini.get("btn_tradeFee").setEnabled(false);//交易费用
					mini.get("btn_tradeProof").setEnabled(false);//交易凭证
					mini.get("btn_agencyFeeBtn").setEnabled(false);//会计传票
				}
			}

        }
    }

    //机构的查询
	function onInsQuery(e) {
		var btnEdit = this;
		mini.open({
			url :  "/fund-capital/standard/InterBank/mini_system/MiniInstitutionSelectManages.jsp",
			title : "机构选择",
			width : 900,
			height : 600,
			ondestroy : function(action) {	
				if (action == "ok") 
				{
					var iframe = this.getIFrameEl();
					var data = iframe.contentWindow.GetData();
					data = mini.clone(data); //必须
					if (data) {
						btnEdit.setValue(data.instId);
						btnEdit.setText(data.instId);
						//mini.get("bRCHNO").setValue(data.instName);
						btnEdit.focus();
					}

					
				}

			}
		});
	}

    //客户的查询
	function onCustQuery(e) {
		var btnEdit = this;
		mini.open({
			url : CommonUtil.baseWebPath() +"/base/MiniCPManageYQ.jsp",
			title : "客户选择",
			width : 900,
			height : 600,
			ondestroy : function(action) {	
				if (action == "ok") 
				{
					var iframe = this.getIFrameEl();
					var data = iframe.contentWindow.GetData();
					data = mini.clone(data); //必须
					if (data) {
						btnEdit.setValue(data.party_id);
						btnEdit.setText(data.party_id);
						mini.get("cUNAME").setValue(data.party_name);
						btnEdit.focus();
						
					}

					
				}

			}
		});
	}
	//利率互换本金 的校验
	function onMoneyValidation(e) {
		if (e.isValid) {
			if (isMoney(e.value) == false) {
				e.errorText = "金额整数位最多17位，小数位最多2位!";
				e.isValid = false;
			}
		}
    }
    /* 金额number(19,2)的验证 */
	function isMoney(v) {
		var re = new RegExp("^[0-9]{1,17}([.]{1}[0-9]{1,2}){0,1}$");
		if (re.test(v)){
			return true;
		}else{
			return false;
		} 
		
	}


	


	

	//前端费、利率点差、天数偏移、利率的校验
	function onRateValidation(e) {
        if(e.value == null || e.value == ""){//可以为空，为空不需要校验
			return;
		}
		if (e.isValid) {
			if (isRate(e.value) == false) {
				e.errorText = "整数位最多12位，小数位最多7位!";
				e.isValid = false;
			}
		}
    }
    /* number(19,7)的验证 */
	function isRate(v) {
		var re = new RegExp("^[0-9]{1,12}([.]{1}[0-9]{1,7}){0,1}$");
		if (re.test(v)){
			return true;
		}else{
			return false;
		} 
		
	}


///////////////////////////事件///////////////////////////////////////////////////////////////////////////////
    //检验要素
	function valid(){
		var form = new mini.Form("inquiry_form");
		form.validate();
	}

    //保存
    function save(){
        ////debugger
     	var form = new mini.Form("#inquiry_form");
		//验证表单
		form.validate();
		if(form.isValid() == false){
			mini.alert("表单填写错误,请确认!","提示信息")
			return;
		}
    		   
		var data = form.getData(true); //获取表单多个控件的数据
        
		data.userSt='S'
		data.STATCD='A'
		//询价类型：远期FDQV、掉期SPQV、期权OPQV、利率互换IRQV
		data.inqType='IRQV';
		var param = mini.encode(data); //序列化成JSON
		
		CommonUtil.ajax({
					url:'/InquiryController/save',
					data:param ,
					callback:function(data){
						mini.get("fDQVNO").setValue(data.desc);
            			mini.alert("保存成功!");
            			mini.get("btn_verify").setEnabled(true);
						
					}
		});
    		
	}

	//提交
	function verify(){
       	var form = new mini.Form("#inquiry_form");
		//验证表单
		form.validate();
		if(form.isValid() == false){
			mini.alert("表单填写错误,请确认!","提示信息")
			return;
		}
			
		var data = form.getData(); //获取表单多个控件的数据
		
		data.userSt='Y'
		var param = mini.encode(data); //序列化成JSON
		
		CommonUtil.ajax({
			url:'/InquiryController/commit',
			data:param,
			callback:function(data){
				mini.alert("提交成功!","系统提示",function(value){
                    //关闭并刷新
					setTimeout(function(){ top["win"].closeMenuTab() },100);
                });
				
			}
		});
			
	}

	//复核通过
    function checkTrade(){
       var form = new mini.Form("#inquiry_form");
		//验证表单
		form.validate();
		if(form.isValid() == false){
		mini.alert("表单填写错误,请确认!","提示信息")
		return;
		}
			
		var data = form.getData(); //获取表单多个控件的数据
		
		data.userSt='Y'
		data.chckSt='Y'
		data.STATCD='A'
		var param = mini.encode(data); //序列化成JSON
		
		CommonUtil.ajax({
			url:'/InquiryController/checkTradeTr',
			data:param ,
			callback:function(data){
				mini.alert("复核通过!","系统提示",function(value){
					//关闭并刷新
					setTimeout(function(){ top["win"].closeMenuTab() },100);
				});
				
			}
		});
		
	}

	//拒绝
	function refuse(){
       var form = new mini.Form("#inquiry_form");
		//验证表单
		form.validate();
		if(form.isValid() == false){
		mini.alert("表单填写错误,请确认!","提示信息")
		return;
		}
			
		var data = form.getData(); //获取表单多个控件的数据
		
		data.userSt='Y'
		data.chckSt='N'
		var param = mini.encode(data); //序列化成JSON
		
		CommonUtil.ajax({
			url:'/InquiryController/refuse',
			data:param ,
			callback:function(data){
				mini.alert("复核拒绝成功!","系统提示",function(value){
					//关闭并刷新
					setTimeout(function(){ top["win"].closeMenuTab() },100);
				});
				
			}
		});
			
	}
</script>
</html>