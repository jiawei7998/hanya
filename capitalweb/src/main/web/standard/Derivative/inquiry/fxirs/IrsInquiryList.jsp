<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../../global.jsp"%> 
<html>
<head>
  <title>利率互换-询价-列表</title>
  <script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
</head>
<body style="width:100%;height:100%;background:white">
	
	<fieldset>
		<legend><label>查询条件</label></legend>
			<div id="search_form"  >
				<input id="CUSTNO" name="CUSTNO" class="mini-textbox"  labelField="true"  label="客户代码：" style="width:310px;" emptyText="请输入客户代码" labelStyle="text-align:right;"/>
				<!-- <input id="custNm" name="custNm" class="mini-textbox"  labelField="true"  label="客户名称：" style="width:310px;" emptyText="请输入客户名称" labelStyle="text-align:right;"/> -->
				<span style="float:right;margin-right: 90px">
					<a id="search_btn" class="mini-button" style="display: none"   onclick="search1">查询</a>
					<a id="clear_btn" class="mini-button" style="display: none"    onClick="clear">清空</a>
				</span>
			</div>
		   
	</fieldset>
	<span style="margin:2px;display: block;">
		<a id="add_btn" class="mini-button" style="display: none"   onclick="add">新增</a>
		<a id="edit_btn" class="mini-button" style="display: none"   onclick="edit">修改</a>
		<a id="del_btn" class="mini-button" style="display: none"   onclick="del">删除</a>
		<a id="detail_btn" class="mini-button" style="display: none"    onclick="detail">查看详情</a>
		<div id = "approveType" name = "approveType" labelField="true" label=""  class="mini-checkboxlist" 
			value="approve" textField="text" valueField="id" multiSelect="false" style="float:right; margin-right: 50px;" 
			labelStyle="text-align:right;" 
			data="[{id:'temp_storage',text:'暂存'},{id:'wait_modify',text:'待修改'},{id:'wait_verify',text:'待复核'},{id:'efficient',text:'生效'}]" onvaluechanged="checkBoxValuechanged">
		</div>
    </span>
    <div class="mini-fit" id="fitId" style="margin-top: 2px;">
		<div id="irsInq_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" 
		idField="fldNo"  pageSize="10" multiSelect="true" onRowdblclick="custDetail" sortMode="client"
		allowAlternating="true">
			<div property="columns" >
				<div type="indexcolumn" headerAlign="center"  align="center" width="40px">序号</div>
				<div type="checkcolumn"></div>
				<div field="bRCHNO" headerAlign="center" width="150px">机构号</div>                            
				<div field="cUSTNO"  headerAlign="center" width="100px">客户代码</div>
                <div field="cUNAME"   headerAlign="center"   width="150px">客户名称</div>
                
                <div field="fDQVNO"   headerAlign="center"  align="center" width="150px">询价业务编号</div>
                <div field="pRNAME"   headerAlign="center"   width="150px">产品名称</div>
                <div field="lTISTP"   headerAlign="center"  align="center" width="150px"  renderer="CommonUtil.dictRenderer" data-options="{dict:'IRS_type'}">利率互换类型</div>
                <div field="rEMARK"   headerAlign="center"  width="150px">备注</div>
                <div field="cUCYNO"   headerAlign="center"  align="center" width="150px">币种</div>
                <div field="tRAMTS"   headerAlign="center"  align="center" width="150px">利率互换本金</div>
                <div field="tRAMTS1"   headerAlign="center"  align="center" width="150px">前端费</div>


                <div field="fORMU1"   headerAlign="center"  align="center" width="150px" visible="false">计息方式</div>
                <div field="fTYPE1"   headerAlign="center"  align="center" width="150px" visible="false">固定/浮动利息</div>
                <div field="iRATE1"   headerAlign="center"  align="center" width="150px" visible="false">利率(%)</div>
                <div field="iPIPS1"   headerAlign="center"  align="center" width="150px" visible="false">利率点差</div>
                <div field="iCURVE1"   headerAlign="center"  align="center" width="150px" visible="false">浮动利率基准曲线</div>
                <div field="bAND1"   headerAlign="center"  align="center" width="150px" visible="false">浮动利率期限</div>
                <div field="oFFSET1"   headerAlign="center"  align="center" width="150px" visible="false">天数偏移</div>
                <div field="dAYCV1"   headerAlign="center"  align="center" width="150px" visible="false">计息天数</div>
                <div field="pAFREQ"   headerAlign="center"  align="center" width="150px" visible="false">付息频率</div>
                <div field="rTFRE1"   headerAlign="center"  align="center" width="150px" visible="false">利率重置频率</div>
               

                <div field="fORMU2"   headerAlign="center"  align="center" width="150px" visible="false">计息方式</div>
                <div field="fTYPE2"   headerAlign="center"  align="center" width="150px" visible="false">固定/浮动利息</div>
                <div field="iRATE2"   headerAlign="center"  align="center" width="150px" visible="false">利率(%)</div>
                <div field="iPIPS2"   headerAlign="center"  align="center" width="150px" visible="false">利率点差</div>
                <div field="iCURVE2"   headerAlign="center"  align="center" width="150px" visible="false">浮动利率基准曲线</div>
                <div field="bAND2"   headerAlign="center"  align="center" width="150px" visible="false">浮动利率期限</div>
                <div field="oFFSET2"   headerAlign="center"  align="center" width="150px" visible="false">天数偏移</div>
                <div field="dAYCV2"   headerAlign="center"  align="center" width="150px" visible="false">计息天数</div>
                <div field="rEFREQ"   headerAlign="center"  align="center" width="150px" visible="false">收息频率</div>
                <div field="rTFRE2"   headerAlign="center"  align="center" width="150px" visible="false">利率重置频率</div>
                


			</div>
		</div>
	</div> 
	<script type="text/javascript">
		mini.parse();

		$(document).ready(function(){
            //控制按钮显示
            $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
                //初始化为暂存列表页面
                mini.get("approveType").setValue("temp_storage");
                //暂存列表页面,查看详情要隐藏
                mini.get("detail_btn").setVisible(false);
                //mini.get("edit_btn").setVisible(false);

                var grid = mini.get("irsInq_grid");
                grid.on("beforeload", function (e) {
                    e.cancel = true;
                    var pageIndex = e.data.pageIndex;
                    var pageSize = e.data.pageSize;
                    search(pageSize, pageIndex);
                });
                search(grid.pageSize, 0);
            });
		});

		top["irsInquiryList"]=window;
        function getData(){
            var grid = mini.get("irsInq_grid");
            var record =grid.getSelected(); 
            return record;
        }

		function search1(){
            var grid =mini.get("irsInq_grid");
            search(grid.pageSize,0); 
        }

		function search(pageSize,pageIndex){
            var form =new mini.Form("search_form");
            form.validate();
            if (form.isValid() == false) return;//表单验证
            var data =form.getData();//获取表单数据
            data['pageNumber']=pageIndex+1;
			data['pageSize']=pageSize;
			//询价类型：远期FDQV、掉期SPQV、期权OPQV、利率互换IRQV
			data.inqType='IRQV';

			var url="";
			var approveType = mini.get("approveType").getValue();
			if(approveType == "temp_storage"){//暂存 //statCd表示登记/授权状态 :A表示登记状态,C表示授权
				data.statCd='A'
				url = "/InquiryController/searchPageTempStorage";

			}else if(approveType == "wait_modify"){//待修改
				data.statCd='A'
				url = "/InquiryController/searchPageWaitModify";

			}else if(approveType == "wait_verify"){//待复核
				data.statCd='A'
				url = "/InquiryController/searchPageWaitVerify";

			}else{//生效efficient
				data.statCd='A'
				url = "/InquiryController/searchPageEfficient";
			}

            var param = mini.encode(data); //序列化成JSON
               CommonUtil.ajax({
            	url:url,
                data:param,
                callback:function(data){
                        
                    var grid =mini.get("irsInq_grid");
                    //设置分页
                    grid.setTotalCount(data.obj.total);
                    grid.setPageIndex(pageIndex);
                    grid.setPageSize(pageSize);
                    //设置数据
					grid.setData(data.obj.rows);
					
                }
            });
		} 

		
	

		//清空
		function clear(){
			var form =new mini.Form("search_form");
			form.clear();
			search1();
			
		}

		function checkBoxValuechanged(){
			var type = mini.get("approveType").getValue();
			if(type == "temp_storage"){//暂存
				mini.get("add_btn").setVisible(true);
				mini.get("edit_btn").setVisible(true);
				mini.get("detail_btn").setVisible(false);
				mini.get("del_btn").setVisible(true);
			}else if(type == "wait_modify"){//待修改
				mini.get("add_btn").setVisible(false);
				mini.get("edit_btn").setVisible(true);
				mini.get("detail_btn").setVisible(false);
				mini.get("del_btn").setVisible(false);
			}else if(type == "wait_verify"){//待复核
				mini.get("add_btn").setVisible(false);
				mini.get("edit_btn").setVisible(false);
				mini.get("detail_btn").setVisible(true);
				mini.get("del_btn").setVisible(false);
			}else{//生效
				mini.get("add_btn").setVisible(false);
				mini.get("edit_btn").setVisible(false);
				mini.get("detail_btn").setVisible(true);
				mini.get("del_btn").setVisible(false);
			}
			
			search1();
		}

		//新增
		function add(){
			var url = CommonUtil.baseWebPath() + "/inquiry/fxirs/IrsInquiryEdit.jsp?action=new";
			var tab = { id: "irsInquiryListAdd", name: "irsInquiryListAdd", text: "利率互换-询价新增", url: url,showCloseButton:true,parentId:top['win'].tabs.getActiveTab().name };
			top['win'].showTab(tab);
		}

		//修改
        function edit(){
			var type = mini.get("approveType").getValue();
			var url = "";
			if(type == "temp_storage"){//暂存
				url = CommonUtil.baseWebPath() + "/inquiry/fxirs/IrsInquiryEdit.jsp?action=edit&dataTp=temp";	
			}else if(type == "wait_modify"){//待修改
				url = CommonUtil.baseWebPath() + "/inquiry/fxirs/IrsInquiryEdit.jsp?action=edit&dataTp=modify";	
			}
            var grid = mini.get("irsInq_grid");
            var  record =grid.getSelected();
            if(record){
                var tab = { id: "irsInquiryListEdit", name: "irsInquiryListEdit", text: "利率互换-询价修改", url: url,showCloseButton:true,parentId:top['win'].tabs.getActiveTab().name };
                top['win'].showTab(tab);
            }else{
                mini.alert("请选中一条记录!","系统提示"); 
            }
        }

		//查看详情
        function detail(){
			var type = mini.get("approveType").getValue();
			var url = "";
			if(type == "wait_verify"){//待复核
				url = CommonUtil.baseWebPath() + "/inquiry/fxirs/IrsInquiryEdit.jsp?action=detail&dataTp=verify";	
			}else if(type == "efficient"){//生效
				url = CommonUtil.baseWebPath() + "/inquiry/fxirs/IrsInquiryEdit.jsp?action=detail&dataTp=effi";	
			}

            var grid = mini.get("irsInq_grid");
            var  record =grid.getSelected();
            if(record){
                
                var tab = { id: "irsInquiryListDetail", name: "irsInquiryListDetail", text: "利率互换-询价详情", url: url,showCloseButton:true,parentId:top['win'].tabs.getActiveTab().name };
                top['win'].showTab(tab);
            }else{
                mini.alert("请选中一条记录!","系统提示"); 
            }
        }

		//删除
		function del(){
			var grid = mini.get("irsInq_grid");
            //获取选中行
            var rows =grid.getSelecteds();
            
            if (rows.length>0) {
                //把询价业务编号放在数组里
				var fDQVNOs = new Array();
				for(var i=0;i<rows.length;i++){
					fDQVNOs.push(rows[i].fDQVNO);
				}
                
                mini.confirm("您确认要删除?","系统警告",function(value){   
                    if (value == "ok"){   
                        CommonUtil.ajax({
                            url : '/InquiryController/delete',
                            data : fDQVNOs,
                            callback : function(data){
                                if("error.common.0000" == data.code){
                                    mini.alert("删除成功！","系统提示");
                                    var grid =mini.get("irsInq_grid");
                                    search(grid.pageSize,0);
                                }
                            }
                        });
                    
                    }   
                });       
                    
            }else{
                mini.alert("请选择一条记录!","系统提示");
            }
		}

    	
       
       
    </script>




</body>
</html>
	