<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../../global.jsp"%> 
<html>
<head>
  <title>远期询价列表(小)-用于打开远期登记维护界面的询价业务编号</title>
  <script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
</head>
<body style="width:100%;height:100%;background:white">
	
	<fieldset>
		<legend><label>查询条件</label></legend>
			<div id="search_form"  >
				<input id="cUSTNO" name="cUSTNO" class="mini-textbox"  labelField="true"  label="客户代码：" style="width:310px;" enabled="false" emptyText="" labelStyle="text-align:right;"/>
                <input id="jSYQTP" name="jSYQTP" class="mini-textbox"  labelField="true"  label="结售汇类型：" style="width:310px;" enabled="false" emptyText="" labelStyle="text-align:right;"/> 
                <input id="wBamts" name="wBamts" class="mini-textbox"  labelField="true"  label="交割总金额：" style="width:310px;" enabled="false" emptyText="" labelStyle="text-align:right;"/>
                <input id="yucyNo" name="yucyNo" class="mini-textbox"  labelField="true"  label="原币种：" style="width:310px;" enabled="false" emptyText="" labelStyle="text-align:right;"/> 
                <input id="mdcyNo" name="mdcyNo" class="mini-textbox"  labelField="true"  label="目的币种：" style="width:310px;" enabled="false" emptyText="" labelStyle="text-align:right;"/>  
				
			</div>
		   
	</fieldset>
	
    <div class="mini-fit" id="fitId" style="margin-top: 2px;">
		<div id="inquiryPrice_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" 
		idField="fldNo"  pageSize="10" multiSelect="true" onRowdblclick="onRowdblclickPriceNo" sortMode="client"
		allowAlternating="true">
			<div property="columns" >
				<div type="indexcolumn" headerAlign="center"  align="center" width="40px">序号</div>
				<div type="checkcolumn"></div>
				<div field="fDQVNO" headerAlign="center"  align="center" width="160px" allowSort="true" dataType="int">询价业务编号</div>    
				<div field="mYRATE" headerAlign="center" width="150px">我行报客户汇率</div>                            
				<div field="eXRATE"  headerAlign="center"   width="150px" align="center" >总行平盘汇率</div>
				
			</div>
		</div>
    </div> 
</body>

<script type="text/javascript">
    mini.parse();

    function SetData(data) {
        //客户代码
        mini.get("cUSTNO").setValue(data.custNo);
        //原币种
        mini.get("yucyNo").setValue(data.yucyNo);
        //目的币种
        mini.get("mdcyNo").setValue(data.mdcyNo);
        //结售汇
        if(data.jsyqTp == "1"){
            mini.get("jSYQTP").setValue("结汇");
        }else{
            mini.get("jSYQTP").setValue("售汇");
        }
        
        //交割总金额
        mini.get("wBamts").setValue(data.wBamts);
        

        var grid =mini.get("inquiryPrice_grid");
        grid.on("beforeload", function (e) {
            e.cancel = true;
            var pageIndex = e.data.pageIndex; 
            var pageSize = e.data.pageSize;
            search(pageSize,pageIndex);
        });
        search(grid.pageSize,0);
    }

    function GetData() {
        var grid = mini.get("inquiryPrice_grid");
        var row = grid.getSelected();
        return row;
    }

    function search(pageSize,pageIndex){
        var form =new mini.Form("search_form");
        form.validate();
        if (form.isValid() == false) return;//表单验证
        var data =form.getData();//获取表单数据
        data['pageNumber']=pageIndex+1;
        data['pageSize']=pageSize;
        var param = mini.encode(data); //序列化成JSON
        CommonUtil.ajax({
            url:"/ForwardController/searchfdqvno",
            data:param,
            callback:function(data){
                    
                var grid =mini.get("inquiryPrice_grid");
                //设置分页
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                //设置数据
                grid.setData(data.obj.rows);
                
            }
        });
    }

    //双击行选择
    function onRowdblclickPriceNo(){
        onOk();
    }

    function CloseWindow(action) {
        if (window.CloseOwnerWindow)
            return window.CloseOwnerWindow(action);
        else
            window.close();
    }

    function onOk() {
        CloseWindow("ok");
    }

    //关闭窗口
    function onCancel() {
        CloseWindow("cancel");
    }
    



</script>
</html>