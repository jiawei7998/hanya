<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
	<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
    <title></title>
  </head>
<body style="width:100%;height:99%;background:white">
    <div style="padding-left:10px;">
          <label >科目名称（模糊）：</label>
          <input id="key" class="mini-textbox" style="width:150px;" />
          <!-- onenter="onKeyEnter" -->
          <input id="branchId" class="mini-hidden" name="branchId" value="<%=__sessionUser.getBranchId()%>" />
          <a class="mini-button" style="display: none"  style="width:60px;" onclick="querys()">查询</a>  <span>例如输入“本金”后点击按钮</span>
          
         	<a id="add_btn" class="mini-button" style="display: none"   onclick="add">新增</a>
			<a id="edit_btn" class="mini-button" style="display: none"  onclick="update">修改</a>
			<a id="delete_btn" class="mini-button" style="display: none"  onclick="del">删除</a>
    </div>
<div id="tradeManage" class="mini-fit">
<div id="treegrid1" class="mini-treegrid" style="width:100%;height:99%;"     
    url="" showTreeIcon="true" 
    treeColumn="subjName" idField="dbId" parentField="" resultAsTree="false" expandOnLoad="true"
>
    <div property="columns">
        <div type="indexcolumn" headerAlign="center"  width="50">序号</div>
        <div name="subjName" field="subjName" headerAlign="center"  width="280">科目名称</div>
        <div field="subjCode" headerAlign="center"  width="120">科目号</div>
        <div field="subjType" headerAlign="center"  width="120" renderer="CommonUtil.dictRenderer" data-options="{dict:'subjType'}">科目类型</div>
        <div field="subjNature" headerAlign="center"  width="120">科目性质</div>
        <div field="debitCredit" headerAlign="center"  width="120">余额方向</div>
        <div field="payRecive" headerAlign="center"  width="120">余额收付方向</div>
    </div>
 </div>
</div>

</body>
</html>
    <script type="text/javascript">
        mini.parse();
        top['SubjectManage'] = window;
        var tree = mini.get("treegrid1");
        function search(){
        CommonUtil.ajax({
            url: "/TbkSubjectDefController/searchPageTbkSubjectDefForTree",
            data: {},
            callback: function (data) {
            	tree.setData(data.obj);
            }
        });
    }
        // function search() {
        //     var key = mini.get("key").getValue();
        //     if (key == "") {
                
        //     } else {
        //         key = key.toLowerCase();

        //         //查找到节点
        //         var nodes = tree.findNodes(function (node) {
        //             var text = node.subjName ? node.subjName.toLowerCase() : "";
        //             if (text.indexOf(key) != -1) {
        //                 return true;
        //             }
        //         });

        //         //展开所有找到的节点
        //         for (var i = 0, l = nodes.length; i < l; i++) {
        //             var node = nodes[i];
        //             tree.expandPath(node);
        //         }
                
        //         //第一个节点选中并滚动到视图
        //         var firstNode = nodes[0];
        //         if (firstNode) {
        //             tree.selectNode(firstNode);
        //             tree.scrollIntoView(firstNode);
        //         }
        //     }
        // }
        
       
        function query() {
            var subjName1 = mini.get("#key").getValue();
            var data = {};
            data['subjName'] = subjName1;
            var param = mini.encode(data);
                var subjName = mini.encode(subjName1);  
                    CommonUtil.ajax({
                    url: "/TbkSubjectDefController/searchPageSubjectBySubjectName",
                    data:param,
                    callback: function (data) {
                        var grid = mini.get("treegrid1");
                        //grid.setTotalCount(data.obj.total);
                        grid.setData(data.obj.rows);
                    }
                });


                }
                function querys() {
                        var subjName1 = mini.get("#key").getValue();
                        if (subjName1 == "") {
                            getTree()
                        } else {
                            query()
                        }

                    }
        
        
        
        function add() {
				var url = CommonUtil.baseWebPath() + "/../cap/base/SubjectManageEdit.jsp?action=add";
				var tab = { id: "MiniInstitutionAdd", name: "MiniInstitutionAdd", title: "科目代码新增", url: url, showCloseButton: true, parentId: top["win"].tabs.getActiveTab().name };
				top['win'].openNewTab(tab);
			}
            function update() {
                    var row = tree.getSelected();
                    if (row) {
                        var url = CommonUtil.baseWebPath() + "/../cap/base/SubjectManageEdit.jsp?action=edit";
                        var tab = { id: "MiniAccOutCashEdit", name: "MiniAccOutCashEdit", title: "科目代码修改", url: url, showCloseButton: true, parentId: top["win"].tabs.getActiveTab().name };
                        top["win"].openNewTab(tab, row);

                    } else {
                        mini.alert("请选中一条记录！", "消息提示");
                    }
                }
        
                function del() {
				//debugger
                var row = tree.getSelected();
                var subjCode = row.subjCode;
                if (row) {
					mini.confirm("您确认要删除选中记录?","系统警告",function(value){
						if(value=="ok"){
						CommonUtil.ajax({
							url: "/TbkSubjectDefController/delTbkSubjectDefs",
							data: [subjCode],
							callback: function (data) {
								if (data.code == 'error.common.0000') {
									mini.alert("删除成功")
									getTree();
                                    //search(grid.pageSize,grid.pageIndex);
								} else {
									mini.alert("删除失败");
								}
							}
						});
						}
					});
				}
				else {
					mini.alert("请选中一条记录！", "消息提示");
				}

			}  
        
        function onKeyEnter(e) {
            search();
        }
        
        function getData(action) {
					var row = null;
					if (action != "add") {
						row = tree.getSelected();
					}
					return row;
				}
    
                $(document).ready(function () {
                    search();
                    });
    </script>