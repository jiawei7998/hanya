﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
	<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
    <title></title>
	<link href="../css/zzsc.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../js/jquery-1.7.2.min.js"></script>
	<script type="text/javascript" src="../js/jquery.json.min.js"></script>
	<link href="../css/base.css" rel="stylesheet" />
	<link href="../css/workbench.css" rel="stylesheet" />
</head>
<body>
<div class="w1200">
<div id="bd" style="padding-right: 0px;">
			        
   <div class="center-part" style="padding-right: 0px;">
       <div class="center-items todo" style="padding-right: 0px;">
           <ul class="work-items clearfix">
               <li style="width:380px;">
                   <div class="work-inner" id="alf">
                       <div class="work-item">
                           <i class="iconfont" style="color:#404040;">&#xe674;</i>
                           <span class="num">金额代码&nbsp;<span class="unit">定义</span></span>
                           <label>定义会计分录金额对应的取值</label>
                       </div>
                   </div>
               </li>
               <li style="width:380px;">
                   <div class="work-inner" id="rdp">
                       <div class="work-item">
                            <i class="iconfont" style="color:#404040;">&#xe6dc;</i>
                           <span class="num">科目代码&nbsp;<span class="unit">定义</span></span>
                           <label>定义系统所使用的会计科目范围</label>
                       </div>
                   </div>
               </li>
               <li style="width:380px;">
                   <div class="work-inner">
                       <div class="work-item" id="rein">
                            <i class="iconfont" style="color:#404040;">&#xe67e;</i>
                           <span class="num">事件代码&nbsp;<span class="unit">定义</span></span>
                           <label>定义交易或则系统操作触发的动作表述</label>
                       </div>
                   </div>
               </li>
               <li style="width:380px;">
                   <div class="work-inner" id="fundBaseDetail">
                       <div class="work-item">
                            <i class="iconfont" style="color:#404040;">&#xe69e;</i>
                           <span  class="num">会计维度&nbsp;<span class="unit">定义</span></span>
                           <label>定义决定会计核算形态的参数</label>
                       </div>
                   </div>
               </li>
                <li style="width:380px;">
                   <div class="work-inner"  id="abc" >
                       <div class="work-item">
                            <i class="iconfont" style="color:#404040;">&#xe63d;</i>
                           <span  class="num">维度取值&nbsp;<span class="unit">定义</span></span>
                           <label>定义决定会计核算形态的参数的枚举值</label>
                       </div>
                   </div>
               </li>
               <li style="width:380px;">
                   <div class="work-inner" id="efg">
                       <div class="work-item" >
                            <i class="iconfont" style="color:#404040;">&#xe6fd;</i>
                           <span  class="num">产品事件&nbsp;<span class="unit">定义</span></span>
                           <label>定义产品/工具 有哪些触发事件</label>
                       </div>
                   </div>
               </li>
               <li style="width:380px;">
                   <div class="work-inner" id="jkl">
                       <div class="work-item">
                            <i class="iconfont" style="color:#404040;">&#xe6c1;</i>
                           <span class="num">产品科目&nbsp;<span class="unit">定义</span></span>
                           <label>规定产品涉猎的科目（本金、利息、损益等）</label>
                       </div>
                   </div>
               </li>
           </ul>
       </div>
   </div>
</div>
</div>
</body>
<script>
		//获取当前tab
		var currTab = top["win"].tabs.getActiveTab();
		var params = currTab.params;
		var dealType = "2";
		var prdNo = '801';
		var prdName = '货币基金';
		var prdProp = '1';
		var prdRootType = '1';
		
		mini.parse();
		function functionMain(url,prdName,title){
			var url = CommonUtil.baseWebPath() + url;
			var tab = {
				id : prdNo + "_" + dealType + "_add",
				name : prdNo + "_" + dealType + "_add",
				iconCls : "",
				title : prdName + title,
				url : url,
				showCloseButton:true
			};
			var paramData = {'dealType':dealType,'prdNo':prdNo,'prdName':prdName,
				'prdProp':prdProp,'prdRootType':prdRootType,'operType':'A','closeFun':'query','pWinId':'tradeManager_' + prdNo};
			
			CommonUtil.openNewMenuTab(tab,paramData);
		}
		 $("#alf").on("click",function(){
			var messageid = mini.loading("系统正在处理...", "请稍后");
			try {
				functionMain("/cap/base/AmountPointerManager.jsp","金额代码","定义");
			} catch (error) {
				
			}
			mini.hideMessageBox(messageid);
		});
		 $("#rdp").on("click",function(){
			var messageid = mini.loading("系统正在处理...", "请稍后");
			try {
				functionMain("/cap/base/SubjectManage.jsp","科目代码","定义");
			} catch (error) {
				
			}
			mini.hideMessageBox(messageid);
		});
		$("#rein").on("click",function(){
			var messageid = mini.loading("系统正在处理...", "请稍后");
			try {
				functionMain("/cap/base/AccCodeManager.jsp","事件代码","定义");
			} catch (error) {
				
			}
			mini.hideMessageBox(messageid);
		});
		 $("#fundBaseDetail").on("click",function(){
			var messageid = mini.loading("系统正在处理...", "请稍后");
			try {
				functionMain("/cap/base/Params.jsp","维度代码","定义");
			} catch (error) {
				
			}
			mini.hideMessageBox(messageid);
		});
		 $("#abc").on("click",function(){
			var messageid = mini.loading("系统正在处理...", "请稍后");
			try {
				functionMain("/cap/base/ParamsMap.jsp","维度取值","定义");
			} catch (error) {
				
			}
			mini.hideMessageBox(messageid);
		});
		 $("#efg").on("click",function(){
				var messageid = mini.loading("系统正在处理...", "请稍后");
				try {
					functionMain("/cap/base/TbkProductEventMain.jsp","产品事件","定义");
				} catch (error) {
					
				}
				mini.hideMessageBox(messageid);
			});
		 $("#jkl").on("click",function(){
				var messageid = mini.loading("系统正在处理...", "请稍后");
				try {
					functionMain("/cap/base/TbkScenceSubjectMain.jsp","产品科目","定义");
				} catch (error) {
					
				}
				mini.hideMessageBox(messageid);
			});
	</script>
</html>  
