<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
	<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
    <title></title>
        <style>

table th{
	border-top:2px solid #428bca;
	text-align:left;
	padding-left:10px;
    border: solid 1px #d9d9d9;
    border-right: 0;
    background-color: #f3f3f3;
    }
</style>
</head>
<body style="width:100%;height:99%;background:white">
<div class="mini-splitter" style="width:100%;height:100%;">

    <div size="380" showCollapseButton="true" >
    <div id="panel1" class="mini-panel" title="产品/工具列表" style="width:100%;height:99%;"  allowResize="true" collapseOnTitleClick="true">
        <div class="mini-fit" >
            <ul id="productTree" class="mini-tree" style="width:100%;"
                showTreeIcon="true" textField="prdName" idField="prdNo" parentField="" resultAsTree="false" >        
            </ul>
        </div>
     </div>
    </div>
    <div showCollapseButton="true">
    		<div id="panel1" class="mini-panel" title="产品所属科目列表" style="width:100%;height:99%;"  allowResize="true" collapseOnTitleClick="true">
         <div id="productSubjectGrid" class="mini-datagrid"  style="width:99.9%;height:450px;" 
		      url="" idField="id" 
		      pageSize="10" 
		      allowCellEdit="true" allowCellSelect="true" multiSelect="true" 
		      editNextOnEnterKey="true"  editNextRowCell="true" onRowclick="amonutPointer">
		      <div property="columns" width="500px;">
		          <div type="indexcolumn" width="50">序号</div>
		          <div name="subjCode"  field="subjCode" headerAlign="center" allowSort="true" width="80" >科目代码</div>
		          <div field="subjName" name="subjName" width="500" headerAlign="center">科目名称</div>
		      </div>
		  </div>
		  <div id="panel1" class="mini-panel" title="科目详情" style="width:100%;padding:0px;"  allowResize="true" collapseOnTitleClick="true">
		          <table id="productSubjectForm"  class="form-table" width="100%">
		          	<tr>
					<th>科目代码：</th>
					<td>
						<input name="subjCode" id="subjCode" class="mini-textbox" style="width:100%" required="true"  vtype="float,30"/>
                        <input id="branchId" class="mini-hidden" name="branchId" value="<%=__sessionUser.getBranchId()%>" />
                    </td>
					<th>科目名称：</th>
					<td>
						<input name="subjName" id="subjName" class="mini-textbox"  style="width:100%" required="true"  vtype="maxLength:125"/>
                    </td>
					</tr>
		              <tr>
		                  <td style="text-align:right;padding-top:5px;padding-right:20px;" colspan="4">
		                  	   <a id="Add_Button"    class="mini-button" style="display: none"  onclick="add">增加</a>
					           <a id="Update_Button" class="mini-button" style="display: none"  onclick="update">修改</a>
					           <a id="Cancel_Button" class="mini-button" style="display: none"  onclick="del">删除</a>
					           <a id="Clear_Button" class="mini-button" style="display: none"  onclick="clear">清空</a>
		                  </td>                
		              </tr>
	          	</table>
		           
		</div>
    		</div>  
    </div>      
</div>
    
    <script type="text/javascript">
    mini.parse();
    var branchId = mini.get("branchId").getValue();
    var form = new mini.Form("#productSubjectForm");
    var tree = mini.get("productTree");
    CommonUtil.ajax({
        url: "/ProductController/searchPageProduct",
        data: {pageSize:9999,branchId:branchId},
        callback: function (data) {
        	tree.setData(data.obj.rows);
        }
    });
    tree.on('nodeclick',function(e){
        var node = e.node;
        var isLeaf = e.isLeaf;
        	if(isLeaf){
        		search(10,0,node.prdNo);
        	}
    });
    var grid = mini.get("productSubjectGrid");
  //查询
    function search(pageSize,pageIndex,prdNo){
        var data = {};
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        data['prdNo'] = prdNo;
        var param = mini.encode(data);
        CommonUtil.ajax({
            url: "/ProductSubjectController/searchProductSubjectPage",
            data: param,
            callback: function (data) {
            	grid.setTotalCount(data.obj.total);
            	grid.setPageIndex(pageIndex);
            	grid.setPageSize(pageSize);
            	grid.setData(data.obj.rows);
            }
        });
    }
    
    function clear(){
			var form = new mini.Form("#productSubjectForm");
			form.clear();
            mini.get("subjCode").setEnabled(true);
        }
        
        function query(){
			search(grid.pageSize,0,tree.getSelected().prdNo);
		}  	   
        
        function add() {
			//debugger
            var row = tree.getSelected();
            var prdNo = row.prdNo;
            var form = new mini.Form("#productSubjectForm");
		    form.validate();
		if (form.isValid() == false) {
			mini.alert("信息填写有误，请重新填写!", "消息提示")
			return;
		}
		//提交数据																					
		var data = form.getData(true); //获取表单多个控件的数据  
        data['prdNo'] = prdNo;
        var param = mini.encode(data); //序列化成JSON
		CommonUtil.ajax({
                url: "/ProductSubjectController/addProductSubjectPage",
                data: param,
                callback: function (data_1) {
                    if (data_1.code == 'error.common.0000') {
                        mini.alert("保存成功");
                        clear();
                        query();
                    } else {
                        mini.alert("保存失败");
                    }
                }
            });
	}
        
        
        function del() {
				//debugger
                var row = grid.getSelected();
				var ssId = row.ssId;
                if (row) {
					mini.confirm("您确认要删除选中记录?","系统警告",function(value){
						if(value=="ok"){
						CommonUtil.ajax({
							url: "/ProductSubjectController/deleteProductSubjectPage",
							data: {ssId: ssId},
							callback: function (data) {
								if (data.code == 'error.common.0000') {
									mini.alert("删除成功")
                                    clear();
                                    query();
								} else {
									mini.alert("删除失败");
								}
							}
						});
						}
					});
				}
				else {
					mini.alert("请选中一条记录！", "消息提示");
				}

			}   
    
            function update() {
                    var form = new mini.Form("#productSubjectForm");
                    var row = grid.getSelected();
                    var ssId = row.ssId;
                    var prdNo = row.prdNo;
                    form.validate();
                    if (form.isValid() == false) {
                        mini.alert("信息填写有误，请重新填写!", "消息提示")
                        return;
                    }
                    //提交数据																					
                    var data = form.getData(true); //获取表单多个控件的数据  
                    data['ssId'] = ssId;
                    data['prdNo'] = prdNo;
                    var param = mini.encode(data); //序列化成JSON
                    CommonUtil.ajax({
                        url: "/ProductSubjectController/updateProductSubjectPage",
                        data: param,
                        callback: function (data_1) {
                            if (data_1.code == 'error.common.0000') {
                                mini.alert(data.desc);
                                clear();
                                query();
                            } else {
                                mini.alert("修改失败");
                            }
                        }
                    });
                }
                
                function amonutPointer(){
            var datas = grid.getSelected();
            form.setData(datas);
            mini.get("subjCode").setEnabled(false);
        }
    //绑定表单
    // var db = new mini.DataBinding();
    // db.bindForm("productSubjectForm", grid);
    </script>
</body>
</html>