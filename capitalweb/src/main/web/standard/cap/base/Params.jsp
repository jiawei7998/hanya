<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
	<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
    <title></title>
    <style>

table th{
	border-top:2px solid #428bca;
	text-align:left;
	padding-left:10px;
    border: solid 1px #d9d9d9;
    border-right: 0;
    background-color: #f3f3f3;
    }
</style>
  </head>
<body style="width:100%;height:99%;background:white">
<div style="width:100%;height:42px;">
	<div id="search_form">
     <div class="mini-toolbar" style="border-bottom:0;padding:0px;">
         <table style="width:100%;">
             <tr >
                 <td style="white-space:nowrap;">
                     <input id="paramName" name="paramName" class="mini-textbox" emptyText="请输入会计维度名称-模糊" style="width:250px;" onenter="onKeyEnter"/>   
                     <a class="mini-button" style="display: none"  onclick="query()">查询</a>
                 </td>
             </tr>
         </table>           
     </div>
  </div>
 </div>

  <div id="paramsGrid" class="mini-datagrid"  style="width:99.9%;height:450px;" 
      url="" idField="id" 
      pageSize="10" 
      allowCellEdit="true" allowCellSelect="true" multiSelect="true" 
      editNextOnEnterKey="true"  editNextRowCell="true" onRowclick="amonutPointer">
      
      <div property="columns">
          <div type="indexcolumn" width="30"></div>
          <div name="paramId"  field="paramId" headerAlign="center" allowSort="true" width="80" >维度编号
          </div>
          <div field="paramName" name="paramName" width="200" headerAlign="center">维度名称</div>
          <div field="paramQz" name="paramQz" width="80" headerAlign="center">维度权重</div>
          <div field="paramSource" name="paramSource" width="80" headerAlign="center">维度字段</div>
          <div type="checkboxcolumn" field="active" trueValue="1" falseValue="0" width="60" headerAlign="center">是否启用</div>    
      </div>
  </div>
<div id="panel1" class="mini-panel" title="事件代码定义详情" style="width:100%;padding:0px;"  allowResize="true" collapseOnTitleClick="true">
          <table id="paramsForm"  class="form-table" width="100%">
          	<tr>
			<th>维度编号：</th>
			<td>
				<input name="paramId" id="paramId"  class="mini-textbox" style="width:100%" required="true"   onvalidation="onEnglishAndNumberValidations"/>
			</td>
			<th>维度名称：</th>
			<td>
				<input name="paramName" id="paramName" class="mini-textbox"  style="width:100%" required="true"  vtype="maxLength:20"/>
			</td>
		</tr>
		<tr>
			<th>维度权重：</th>
			<td>
				<input name="paramQz" id="paramQz" class="mini-textbox" style="width:100%" required="true"  vtype="float"/>
			</td>
			<th>维度字段：</th>
			<td>
				<input name="paramSource" id="paramSource" class="mini-textbox"  style="width:100%"/>
			</td>
		</tr>
	       <tr>
	               <th>是否启用：</th>
			<td colspan="3"><input name="active"  class="mini-combobox"  data = "CommonUtil.serverData.dictionary.YesNo"/></td>
	        </tr>
              <tr>
                  <td style="text-align:right;padding-top:5px;padding-right:20px;" colspan="4">
                  	   <a id="Add_Button"    class="mini-button" style="display: none"  onclick="add">增加</a>
			           <a id="Update_Button" class="mini-button" style="display: none"  onclick="update">修改</a>
			           <a id="Cancel_Button" class="mini-button" style="display: none"  onclick="del">删除</a>
			           <a id="Clear_Button" class="mini-button" style="display: none"  onclick="clear">清空</a>
                  </td>                
              </tr>
          </table>
           
</div>
    <script type="text/javascript">
     
    mini.parse();
    var form = new mini.Form("#paramsForm");
    var grid = mini.get("paramsGrid");
    $(document).ready(function(){
    		grid.on("beforeload",function(e){
            e.cancel = true;
            var pageIndex = e.data.pageIndex; 
            var pageSize = e.data.pageSize;
            search(pageSize,pageIndex);
        });
        search(10,0);
    })
  	//查询
    function search(pageSize,pageIndex){
        var form = new mini.Form("#search_form");
        form.validate();
        if (form.isValid() == false) return;//表单验证
        var data = form.getData();//获取表单数据
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        var param = mini.encode(data);
        CommonUtil.ajax({
            url: "/ParamsController/searchParamsPage",
            data: param,
            callback: function (data) {
            	grid.setTotalCount(data.obj.total);
            	grid.setPageIndex(pageIndex);
            	grid.setPageSize(pageSize);
            	grid.setData(data.obj.rows);
            }
        });
    }
    
    function add() {
        var form = new mini.Form("#paramsForm");
		form.validate();
		if (form.isValid() == false) {
			mini.alert("信息填写有误，请重新填写!", "消息提示")
			return;
		}
		//提交数据																					
		var data = form.getData(true); //获取表单多个控件的数据  
		var param = mini.encode(data); //序列化成JSON
		CommonUtil.ajax({
                url: "/ParamsController/createParams",
                data: param,
                callback: function (data_1) {
                    if (data_1.code == 'error.common.0000') {
                        mini.alert("保存成功");
                        clear();
                        query();
                    } else {
                        mini.alert("保存失败");
                    }
                }
            });
	}
        
    function update() {
        var form = new mini.Form("paramsForm");
		form.validate();
		if (form.isValid() == false) {
			mini.alert("信息填写有误，请重新填写!", "消息提示")
			return;
		}
		//提交数据																					
		var data = form.getData(true); //获取表单多个控件的数据  
		var param = mini.encode(data); //序列化成JSON
		CommonUtil.ajax({
                url: "/ParamsController/updateParams",
                data: param,
                callback: function (data_1) {
                    if (data_1.code == 'error.common.0000') {
                        mini.alert(data.desc);
                        clear();
                        query();
                    } else {
                        mini.alert("修改失败");
                    }
                }
            });
	}

      function del() {
				var row = grid.getSelected();
				if (row) {
					mini.confirm("您确认要删除选中记录?","系统警告",function(value){
						if(value=="ok"){
						CommonUtil.ajax({
							url: "/ParamsController/deleteParams",
							data: {paramId: row.paramId},
							callback: function (data) {
								if (data.code == 'error.common.0000') {
									mini.alert("删除成功")
                                    clear();
                                    query();
								} else {
									mini.alert("删除失败");
								}
							}
						});
						}
					});
				}
				else {
					mini.alert("请选中一条记录！", "消息提示");
				}

			}   
    //绑定表单
    // var db = new mini.DataBinding();
    // db.bindForm("paramsForm", grid);

      function clear(){
			var form = new mini.Form("#paramsForm");
			form.clear();
            mini.get("paramId").setEnabled(true);
        }
    
        function query(){
			search(grid.pageSize,0);
		}  
    
        function amonutPointer(){
            var datas = grid.getSelected();
            form.setData(datas);
            mini.get("paramId").setEnabled(false);
        }
    
        function onEnglishAndNumberValidations(e) {
		if(e.value == "" || e.value == null){//值为空，就不做校验
			return;
		}
		if (e.isValid) {
			if (isEnglishAndNumbers(e.value) == false) {
				e.errorText = "必须输入英文+数字+下划线";
				e.isValid = false;
			}
		}
	}
	/* 是否英文+数字 */
	function isEnglishAndNumbers(v) {
		var re = new RegExp("^[0-9a-zA-Z_\_\-]+$");
		if (re.test(v)) return true;
		return false;
	}	
    
    </script>
</body>
</html>