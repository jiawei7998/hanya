<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
	<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
    <title></title>
        <style>

table th{
	border-top:2px solid #428bca;
	text-align:left;
	padding-left:10px;
    border: solid 1px #d9d9d9;
    border-right: 0;
    background-color: #f3f3f3;
    }
</style>
</head>
<body style="width:100%;height:99%;background:white">
<div class="mini-splitter" style="width:100%;height:100%;">

    <div size="440" showCollapseButton="true" >
    <div id="panel1" class="mini-panel" title="产品/工具列表" style="width:100%;height:99%;"  allowResize="true" collapseOnTitleClick="true">
        <div class="mini-fit" >
            <ul id="tree1" class="mini-tree" url="" style="width:100%;"
                showTreeIcon="true" textField="prdName" idField="prdNo" parentField="" resultAsTree="false" >        
            </ul>
        </div>
     </div>
    </div>
    <div showCollapseButton="false">
    		<div class="mini-splitter" style="width:100%;height:100%;">
    			<div size="340" showCollapseButton="true" >
			    <div id="panel2" class="mini-panel" title="产品/事件列表" style="width:100%;height:99%;"  allowResize="true" collapseOnTitleClick="true">
			        <div class="mini-fit" >
			            <ul id="tree2" class="mini-tree" url="" style="width:100%;"
			                showTreeIcon="true" textField="eventName" idField="eventId" parentField="" resultAsTree="false" >        
			            </ul>
			        </div>
			     </div>
			    </div>
			    <div showCollapseButton="false">
			    		<div id="panel1" class="mini-panel" title="该产品-事件项下的已配置的维度场景列表" style="width:100%;height:430px;"  allowResize="true" collapseOnTitleClick="true">
			    		<a id="Qry_Button"    class="mini-button" style="display: none"  >刷新</a>
			    		<a id="Del_Button"    class="mini-button" style="display: none"  >删除</a>
			         <div id="datagrid1" class="mini-datagrid"  style="width:99.9%;height:350px;" 
					      url="" idField="id" 
					      pageSize="10" 
					      allowCellEdit="true" allowCellSelect="true" multiSelect="true" 
					      editNextOnEnterKey="true"  editNextRowCell="true"   allowResize="true">
					      <div property="columns" >
					          <div type="indexcolumn" width="30"></div>
					          <div name="paramsSumId"  field="paramsSumId" headerAlign="center" allowSort="true" width="80" >维度组合代码</div>
					          <div field="paramsSumName" name="paramsSumName" width="300" headerAlign="center">维度组合名称</div>
					      </div>
					  </div>
					</div>
				<div id="panel1" class="mini-panel" title="该产品-事件项下勾选维度以增加场景" style="width:100%;height:700px;"  collapseOnTitleClick="true">
						<a id="Add_Button"    class="mini-button" style="display: none"  onclick="save()" >增加</a>
					<!-- <div id="treegrid1" class="mini-treegrid" style="width:100%;height:auto;"     
						    url=""
						    textField="paramName" idField="paramId" parentField="pid" resultAsTree="false"  
						    allowResize="true" expandOnLoad="true" showTreeIcon="true"
						    allowSelect="false" allowCellSelect="false" enableHotTrack="false"
						    ondrawcell="ondrawcell" 
						>
						    <div property="columns">
						        <div type="indexcolumn"></div>
						        <div name="name" field="name" width="280" >维度名称</div>
						        <div field="functions" width="100%">维度取值</div>
						    </div>
						</div> -->
                        <div id="treegrid1" class="mini-datagrid borderAll" style="width: 100%; height: 100%;" allowAlternating="true" allowResize="true"
                        border="true" sortMode="client" ondrawcell="ondrawcell">
                            <div property="columns">
                                <div type="indexcolumn"></div>
						        <div name="paramName" field="paramName" width="280" >维度名称</div>
						        <div field="functions"  width="100%">维度取值</div>
                            </div>
                        </div>
					</div>
		 		</div>
    		</div>
    	</div>      
</div>
    
    <script type="text/javascript">
        mini.parse();

        var tree1 = mini.get("tree1");
        var tree2 = mini.get("tree2");
        var grid = mini.get("datagrid1");
        var data = [{},{},{},{},{}];
        grid.setData(data);
        var tree = mini.get("treegrid1");
        tree1.on('nodeclick',function(e){
	        var node = e.node;
	        var isLeaf = e.isLeaf;
	        	if(isLeaf){
	        		searchProductEvent(9999,0,node.prdNo);
	        	}
        });

       function searchProductEvent(pageSize,pageIndex,prdNo){
           var data = {};
           data['pageNumber'] = pageIndex + 1;
           data['pageSize'] = pageSize;
           data['prdNo'] = prdNo;
           data['type'] = 1;
           var param = mini.encode(data);
           CommonUtil.ajax({
               url: "/TbkEventController/getProductEvents",
               data: param,
               callback: function (data) {
            	   tree2.setData(data.obj);
               }
           });
       }
        
        function getproduct(){
            var branchId = "<%=__sessionUser.getBranchId()%>";
            CommonUtil.ajax({
            url: "/ProductController/searchPageProduct",
            data: {pageSize:9999,branchId:branchId},
            callback: function (data) {
            	tree1.setData(data.obj.rows);
            }
        });
        }
        
        // function getParamsMap() {
        //         //debugger
        //         var data = {};
        //         data.paramId = tree.data[0].paramId;
        //         var param = mini.encode(data);
        //         CommonUtil.ajax({
        //             url: "/ParamsMapController/searchPvalueTxtList",
        //             data: param,
        //             callback: function (data) {
        //                 //tree.setData(data.obj);
        //             }
        //         });
        //     }
        
        function getparam(){
            CommonUtil.ajax({
            url: "/ParamsMapController/searchParamsMapPages",
            data: {pageSize:9999},
            callback: function (data) {
            	tree.setData(data.obj);
            }
        });
        }
   
        $(document).ready(function() {
			getproduct();
            getparam();
            //getParamsMap();
        });
   
        function ondrawcell(e) {
                var tree = e.sender,
                    record = e.record,
                    column = e.column,
                    field = e.field,
                    id = record[tree.getIdField()],
                    funs = record.attributes;
                function createCheckboxs(funs) {
                    if (!funs) return "";
                    var html = "";

                    for (var i = 0, l = funs.length; i < l; i++) {
                        var fn = funs[i];
                        var clickFn = 'checkFunc(\'' + id + '\',\'' + fn.pValue + '\', this.checked)';
                        var checked = fn.checked ? 'checked' : '';
                        html += '<label class="function-item"><input onclick="' + clickFn + '" ' + checked + ' type="checkbox" name="'
                            + fn.pValue + '" id=' + fn.pValue + ' hideFocus/>' + fn.pValueTxt + '</label>';
                    }
                    return html;
                }
                if (field == 'functions') {
                e.cellHtml = createCheckboxs(funs);
            }
            }
            
            function checkFunc(id, pValue, checked) {
                var record = tree.getRecord(id);
                if (!record) return;
                var funs = record.attributes;
                if (!funs) return;
                function getAction(pValue) {
                    for (var i = 0, l = funs.length; i < l; i++) {
                        var o = funs[i];
                        if (o.pValue == pValue) return o;
                    }
                }
                var obj = getAction(pValue);
                if (!obj) return;
                obj.checked = checked;
    }
           
        function save() {
            //获取菜单选中的内容
            var paramObj = {};
            var functionList = new Array();//功能点保存对象
            var prdNo = mini.get('tree1').getSelectedNode ().prdNo;
            var eventId = mini.get('tree2').getSelectedNode ().eventId;
            //获取选中的功能点
            var treeAllNote = tree.getData();
            for (var i = 0; i < treeAllNote.length; i++) {
                var tbkParamsMap = {};
                  tbkParamsMap.prdNo = prdNo;
                tbkParamsMap.eventId = eventId;
                //tbkParamsMap.pValue =treeAllNote[i].attributes[i].pValue;
                //tbkParamsMap. pValueTxt=treeAllNote[i].attributes[i].pValueTxt;
                functionList.push(tbkParamsMap);
            }
            paramObj.functionSelected = functionList;
            if (paramObj) {
                if (paramObj.functionSelected != null && paramObj.functionSelected.length > 0) {
                    //发送ajax
                    mini.confirm("您确定保存吗？", "温馨提示", function (action) {
                        if (action == 'ok') {
                            CommonUtil.ajax({
                                url: "",
                                data: mini.encode(paramObj),
                                callback: function (data) {
                                    if (data.code == 'error.common.0000') {
                                        mini.alert("保存成功","系统提示",function(){
                                            onCancel();
                                        });
                                    } else {
                                        mini.alert("保存失败，请重试~","系统提示");
                                    }
                                }
                            });
                        }
                    })
                 } else  {
                     mini.alert("请先选择产品","系统提示");
                 }
             } else {
                 mini.alert("请先选择产品事件","系统提示");
             }
        }
            
        
        function onCancel(e) {
                window.CloseOwnerWindow();
            }
    </script>
</body>
</html>