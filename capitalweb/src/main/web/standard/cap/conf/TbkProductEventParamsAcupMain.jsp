<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
	<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
    <title></title>
        <style>

table th{
	border-top:2px solid #428bca;
	text-align:left;
	padding-left:10px;
    border: solid 1px #d9d9d9;
    border-right: 0;
    background-color: #f3f3f3;
    }
</style>
</head>
<body style="width:100%;height:99%;background:white">
<div class="mini-splitter" style="width:100%;height:100%;">

    <div size="240" showCollapseButton="true" >
    <div id="panel1" class="mini-panel" title="产品/工具列表" style="width:100%;height:99%;"  allowResize="true" collapseOnTitleClick="true">
        <div class="mini-fit" >
            <ul id="tree1" class="mini-tree" url="../data/productTree.txt" style="width:100%;"
                showTreeIcon="true" textField="name" idField="id" parentField="pid" resultAsTree="false" >        
            </ul>
        </div>
     </div>
    </div>
    <div showCollapseButton="false">
    		<div class="mini-splitter" style="width:100%;height:100%;">
    			<div size="240" showCollapseButton="true" >
			    <div id="panel2" class="mini-panel" title="产品/事件列表" style="width:100%;height:99%;"  allowResize="true" collapseOnTitleClick="true">
			        <div class="mini-fit" >
			            <ul id="tree2" class="mini-tree" url="../data/eventTree.txt" style="width:100%;"
			                showTreeIcon="true" textField="name" idField="id" parentField="pid" resultAsTree="false" >        
			            </ul>
			        </div>
			     </div>
			    </div>
			    <div showCollapseButton="false">
			    		<div id="panel1" class="mini-panel" title="该产品-事件项下的已配置的维度场景列表" style="width:100%;height:480px;"  allowResize="true" collapseOnTitleClick="true">
			         <div id="datagrid1" class="mini-datagrid"  style="width:99.9%;height:380px;" 
					      url="" idField="id" 
					      pageSize="10" 
					      allowCellEdit="true" allowCellSelect="true" multiSelect="true" 
					      editNextOnEnterKey="true"  editNextRowCell="true" >
					      <div property="columns" >
					          <div type="indexcolumn" width="30"></div>
					          <div name="paramsSumId"  field="paramsSumId" headerAlign="center" allowSort="true" width="80" >维度组合代码</div>
					          <div field="paramsSumName" name="paramsSumName" width="300" headerAlign="center">维度组合名称</div>
					      </div>
					  </div>
					</div>
				<div id="panel2" class="mini-panel" title="该产品-事件项下勾选维度以增加场景" style="width:100%;height:700px;"  collapseOnTitleClick="true">
						<a id="Add_Button"    class="mini-button" style="display: none"  >保存</a>
						<div id="datagrid2" class="mini-datagrid"  style="width:99.9%;height:auto;" 
					      url="" idField="id" 
					      pageSize="10" 
					      allowCellEdit="true" allowCellSelect="true" multiSelect="true" 
					      editNextOnEnterKey="true"  editNextRowCell="true" >
					      <div property="columns" >
					      	  <div name="drcrInd"  field="drcrInd" headerAlign="center" allowSort="true" width="80" >借贷关系</div>
					          <div name="subjCode"  field="subjCode" headerAlign="center" allowSort="true" width="80" >科目代码</div>
					          <div field="subjName" name="subjName" width="300" headerAlign="center">科目名称</div>
					           <div field="amountName" name="amountName" width="200" headerAlign="center">金额名称</div>
					          <div field="amountId" name="amountId" width="200" headerAlign="center">金额代码</div>
					      </div>
					  </div>
					</div>
		 		</div>
    		</div>
    	</div>      
</div>
    
    <script type="text/javascript">
    		mini.parse();
        var tree1 = mini.get("tree1");
        var tree2 = mini.get("tree2");
        var grid = mini.get("datagrid1");
        var data1 = [{},{},{},{},{}];
        grid.setData(data1);
        var tree = mini.get("datagrid2");
        var data2 = [{},{},{},{},{}];
        tree.setData(data2);
        
    </script>
</body>
</html>