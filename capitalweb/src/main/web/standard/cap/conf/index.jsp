﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
	<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
    <title></title>
	<link href="../css/zzsc.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../js/jquery-1.7.2.min.js"></script>
	<script type="text/javascript" src="../js/jquery.json.min.js"></script>
	<link href="../css/base.css" rel="stylesheet" />
	<link href="../css/workbench.css" rel="stylesheet" />
</head>
<body>
<div class="w1200">
<div id="bd" style="padding-right: 0px;">
			        
   <div class="center-part" style="padding-right: 0px;">
       <div class="center-items todo" style="padding-right: 0px;">
           <ul class="work-items clearfix">
               <li style="width:380px;">
                   <div class="work-inner" id="alf">
                       <div class="work-item">
                           <i class="iconfont" style="color:#404040;">&#xe6f5;</i>
                           <span class="num">会计场景&nbsp;<span class="unit">配置</span></span>
                           <label>配置产品项下不同会计事件所含的维度取值组合</label>
                       </div>
                   </div>
               </li>
               <li style="width:380px;">
                   <div class="work-inner" id="rdp">
                       <div class="work-item">
                            <i class="iconfont" style="color:#404040;">&#xe65b;</i>
                           <span class="num">场景分录&nbsp;<span class="unit">配置</span></span>
                           <label>配置不同场景下的会计分录。</label>
                       </div>
                   </div>
               </li>
               
           </ul>
       </div>
   </div>
</div>
</div>
</body>
<script>
		//获取当前tab
		var currTab = top["win"].tabs.getActiveTab();
		var params = currTab.params;
		var dealType = "2";
		var prdNo = '801';
		var prdName = '货币基金';
		var prdProp = '1';
		var prdRootType = '1';
		
		mini.parse();
		function functionMain(url,prdName,title){
			var url = CommonUtil.baseWebPath() + url;
			var tab = {
				id : prdNo + "_" + dealType + "_add",
				name : prdNo + "_" + dealType + "_add",
				iconCls : "",
				title : prdName + title,
				url : url,
				showCloseButton:true
			};
			var paramData = {'dealType':dealType,'prdNo':prdNo,'prdName':prdName,
				'prdProp':prdProp,'prdRootType':prdRootType,'operType':'A','closeFun':'query','pWinId':'tradeManager_' + prdNo};
			
			CommonUtil.openNewMenuTab(tab,paramData);
		}
		 $("#alf").on("click",function(){
			var messageid = mini.loading("系统正在处理...", "请稍后");
			try {
				functionMain("/cap/conf/TbkProductEventParamsMain.jsp","会计场景","配置");
			} catch (error) {
				
			}
			mini.hideMessageBox(messageid);
		});
		 $("#rdp").on("click",function(){
			var messageid = mini.loading("系统正在处理...", "请稍后");
			try {
				functionMain("/cap/conf/TbkProductEventParamsAcupMain.jsp","场景分录","配置");
			} catch (error) {
				
			}
			mini.hideMessageBox(messageid);
		});
		
	</script>
</html>  
