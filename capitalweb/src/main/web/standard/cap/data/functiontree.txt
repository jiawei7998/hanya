﻿[
	{ id: 'sys', name: '维度参数' },
	{ id: 'dept', name: '会计四分类', pid: 'sys',
		functions: [
			{ action: 'add', name: '可供出售', checked: true},
			{ action: 'remove', name: '持有到期', checked: true},
			{ action: 'edit', name: '交易类', checked: false},
			{ action: 'search', name: '应收款项', checked: false}
		]
	 },
	{ id: 'user', name: '境内境外' , pid: 'sys',
		functions: [
			{ action: 'add', name: '境内', checked: false},
			{ action: 'remove', name: '境外', checked: false}
		]
	},
	{ id: 'menu', name: '利息调整' , pid: 'sys',
		functions: [
			{ action: 'add', name: '调大', checked: true},
			{ action: 'remove', name: '调小', checked: true}
		]
	},
	{ id: 'role', name: '是否保本' , pid: 'sys',
		functions: [
			{ action: 'add', name: '保本', checked: true},
			{ action: 'remove', name: '非保本', checked: true}
		]
	},
	{ id: 'backup', name: '基金属性' , pid: 'sys',
		functions: [
			{ action: 'submit', name: '公募', checked: true},
			{ action: 'reset', name: '私募', checked: false}
		]
	},
	{ id: 'args', name: '是否有保证金' , pid: 'sys',
		functions: [
			{ action: 'add', name: '是', checked: true},
			{ action: 'remove', name: '否', checked: true}
		]
	},
	{ id: 'fun', name: '收息类型' , pid: 'sys',
		functions: [
			{ action: 'add', name: '按季', checked: true},
			{ action: 'remove', name: '按年', checked: true},
			{ action: 'edit', name: '按月', checked: false},
			{ action: 'search', name: '一次性', checked: false}
		]
	},
	{ id: 'reduction', name: '收息方式' , pid: 'sys',
		functions: [
			{ action: 'submit', name: '前收息', checked: true},
			{ action: 'reset', name: '后收息', checked: false}
		]
	}
]