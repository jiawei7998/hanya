--------------------------------------------------------
--  文件已创建 - 星期三-六月-06-2018   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table TBK_PRODUCT_EVENT
--------------------------------------------------------

  CREATE TABLE "TBK_PRODUCT_EVENT" 
   (	"DB_ID" VARCHAR2(20 BYTE), 
	"PRD_NO" VARCHAR2(32 BYTE), 
	"EVENT_ID" VARCHAR2(32 BYTE)
   ) 
 

   COMMENT ON COLUMN "TBK_PRODUCT_EVENT"."DB_ID" IS '自增ID'
 
   COMMENT ON COLUMN "TBK_PRODUCT_EVENT"."PRD_NO" IS '产品Id'
 
   COMMENT ON COLUMN "TBK_PRODUCT_EVENT"."EVENT_ID" IS '会计事件id'
 
   COMMENT ON TABLE "TBK_PRODUCT_EVENT"  IS '产品事件配置'
--------------------------------------------------------
--  DDL for Table TBK_AMOUNT
--------------------------------------------------------

  CREATE TABLE "TBK_AMOUNT" 
   (	"AMOUNT_ID" VARCHAR2(50 BYTE), 
	"AMOUNT_NAME" VARCHAR2(50 BYTE), 
	"AMOUNT_FORMULA" VARCHAR2(200 BYTE), 
	"AMOUNT_DESC" VARCHAR2(200 BYTE)
   ) 
 

   COMMENT ON COLUMN "TBK_AMOUNT"."AMOUNT_ID" IS '金额代码'
 
   COMMENT ON COLUMN "TBK_AMOUNT"."AMOUNT_NAME" IS '金额名称'
 
   COMMENT ON COLUMN "TBK_AMOUNT"."AMOUNT_FORMULA" IS '金额公式'
 
   COMMENT ON COLUMN "TBK_AMOUNT"."AMOUNT_DESC" IS '金额描述'
 
   COMMENT ON TABLE "TBK_AMOUNT"  IS '金额代码表'
--------------------------------------------------------
--  DDL for Table TBK_EVENT
--------------------------------------------------------

  CREATE TABLE "TBK_EVENT" 
   (	"EVENT_ID" VARCHAR2(50 BYTE), 
	"EVENT_NAME" VARCHAR2(50 BYTE), 
	"EVENT_DESC" VARCHAR2(200 BYTE), 
	"SORT_STR" NUMBER(5,0), 
	"EVENT_TYPE" VARCHAR2(50 BYTE)
   ) 
 

   COMMENT ON COLUMN "TBK_EVENT"."EVENT_ID" IS '事件代码
'
 
   COMMENT ON COLUMN "TBK_EVENT"."EVENT_NAME" IS '事件名称
'
 
   COMMENT ON COLUMN "TBK_EVENT"."EVENT_DESC" IS '描述'
 
   COMMENT ON COLUMN "TBK_EVENT"."SORT_STR" IS '排序'
 
   COMMENT ON COLUMN "TBK_EVENT"."EVENT_TYPE" IS '事件标识'
 
   COMMENT ON TABLE "TBK_EVENT"  IS '会计事件表'
--------------------------------------------------------
--  DDL for Table TBK_PARAMS
--------------------------------------------------------

  CREATE TABLE "TBK_PARAMS" 
   (	"PARAM_ID" VARCHAR2(100 BYTE), 
	"PARAM_NAME" VARCHAR2(40 BYTE), 
	"PARAM_QZ" NUMBER(*,0), 
	"PARAM_SOURCE" VARCHAR2(50 BYTE), 
	"LAST_UPDATE" VARCHAR2(20 BYTE), 
	"ACTIVE" VARCHAR2(2 BYTE)
   ) 
 

   COMMENT ON COLUMN "TBK_PARAMS"."PARAM_ID" IS '参数ID'
 
   COMMENT ON COLUMN "TBK_PARAMS"."PARAM_NAME" IS '参数注释'
 
   COMMENT ON COLUMN "TBK_PARAMS"."PARAM_QZ" IS '参数权重'
 
   COMMENT ON COLUMN "TBK_PARAMS"."PARAM_SOURCE" IS '参数分组'
 
   COMMENT ON COLUMN "TBK_PARAMS"."LAST_UPDATE" IS '更新时间'
 
   COMMENT ON COLUMN "TBK_PARAMS"."ACTIVE" IS '是否启用'
 
   COMMENT ON TABLE "TBK_PARAMS"  IS '参数权重表'
--------------------------------------------------------
--  DDL for Table TBK_PARAMS_MAP
--------------------------------------------------------

  CREATE TABLE "TBK_PARAMS_MAP" 
   (	"P_KEY" VARCHAR2(6 BYTE), 
	"PARAM_ID" VARCHAR2(100 BYTE), 
	"PARAM_NAME" VARCHAR2(40 BYTE), 
	"P_VALUE" VARCHAR2(40 BYTE), 
	"P_VALUE_TXT" VARCHAR2(40 BYTE), 
	"LAST_UPDATE" VARCHAR2(20 BYTE)
   ) 
 

   COMMENT ON COLUMN "TBK_PARAMS_MAP"."P_KEY" IS '参数key（自动添加，SEQ_PARAMS_MAP）'
 
   COMMENT ON COLUMN "TBK_PARAMS_MAP"."PARAM_ID" IS '参数ID，与参数表中的参数id对应'
 
   COMMENT ON COLUMN "TBK_PARAMS_MAP"."PARAM_NAME" IS '参数注释'
 
   COMMENT ON COLUMN "TBK_PARAMS_MAP"."P_VALUE" IS '参数值'
 
   COMMENT ON COLUMN "TBK_PARAMS_MAP"."P_VALUE_TXT" IS '参数值注释'
 
   COMMENT ON COLUMN "TBK_PARAMS_MAP"."LAST_UPDATE" IS '最后更新时间'
 
   COMMENT ON TABLE "TBK_PARAMS_MAP"  IS '参数纬度值表'
--------------------------------------------------------
--  DDL for Table TBK_SUBJECT_DEF
--------------------------------------------------------

  CREATE TABLE "TBK_SUBJECT_DEF" 
   (	"TASK_ID" VARCHAR2(20 BYTE), 
	"SUBJ_CODE" VARCHAR2(20 BYTE), 
	"SUBJ_NAME" VARCHAR2(100 BYTE), 
	"PARENT_SUBJ_CODE" VARCHAR2(20 BYTE), 
	"SUBJ_TYPE" VARCHAR2(10 BYTE), 
	"DEBIT_CREDIT" VARCHAR2(20 BYTE), 
	"PAY_RECIVE" VARCHAR2(20 BYTE), 
	"SUBJ_NATURE" VARCHAR2(10 BYTE), 
	"DIRECTION" VARCHAR2(10 BYTE), 
	"SUBJ_LEVEL" VARCHAR2(20 BYTE), 
	"SOURCE" VARCHAR2(1 BYTE)
   ) 
 

   COMMENT ON COLUMN "TBK_SUBJECT_DEF"."TASK_ID" IS '所属账套ID'
 
   COMMENT ON COLUMN "TBK_SUBJECT_DEF"."SUBJ_CODE" IS '科目号'
 
   COMMENT ON COLUMN "TBK_SUBJECT_DEF"."SUBJ_NAME" IS '科目名称'
 
   COMMENT ON COLUMN "TBK_SUBJECT_DEF"."PARENT_SUBJ_CODE" IS '上级科目号'
 
   COMMENT ON COLUMN "TBK_SUBJECT_DEF"."SUBJ_TYPE" IS '科目类型[01-资产类/02-负债类/03-所有者权益类/04-资产负债共通类/05-损益类/06-表外科目/07-备忘科目/99-其他科目]'
 
   COMMENT ON COLUMN "TBK_SUBJECT_DEF"."DEBIT_CREDIT" IS '余额借贷方向'
 
   COMMENT ON COLUMN "TBK_SUBJECT_DEF"."PAY_RECIVE" IS '余额收付方向'
 
   COMMENT ON COLUMN "TBK_SUBJECT_DEF"."SUBJ_NATURE" IS '科目性质1-成本 2-预付款递延利息收入 3-付款项利息收入 4-应收利息 5-应计利息'
 
   COMMENT ON COLUMN "TBK_SUBJECT_DEF"."DIRECTION" IS '收支方向[收入/支出]'
 
   COMMENT ON COLUMN "TBK_SUBJECT_DEF"."SUBJ_LEVEL" IS '科目级别'
 
   COMMENT ON COLUMN "TBK_SUBJECT_DEF"."SOURCE" IS '数据来源1.本地 2.接口 '
 
   COMMENT ON TABLE "TBK_SUBJECT_DEF"  IS '科目信息表'
REM INSERTING into TBK_PRODUCT_EVENT
SET DEFINE OFF;
Insert into TBK_PRODUCT_EVENT (DB_ID,PRD_NO,EVENT_ID) values ('410','801','1040');
Insert into TBK_PRODUCT_EVENT (DB_ID,PRD_NO,EVENT_ID) values ('411','801','2010');
Insert into TBK_PRODUCT_EVENT (DB_ID,PRD_NO,EVENT_ID) values ('412','802','1020');
Insert into TBK_PRODUCT_EVENT (DB_ID,PRD_NO,EVENT_ID) values ('413','802','1040');
Insert into TBK_PRODUCT_EVENT (DB_ID,PRD_NO,EVENT_ID) values ('408','801','1020');
Insert into TBK_PRODUCT_EVENT (DB_ID,PRD_NO,EVENT_ID) values ('409','801','1030');
Insert into TBK_PRODUCT_EVENT (DB_ID,PRD_NO,EVENT_ID) values ('414','802','1050');
Insert into TBK_PRODUCT_EVENT (DB_ID,PRD_NO,EVENT_ID) values ('415','802','2020');
REM INSERTING into TBK_AMOUNT
SET DEFINE OFF;
REM INSERTING into TBK_EVENT
SET DEFINE OFF;
Insert into TBK_EVENT (EVENT_ID,EVENT_NAME,EVENT_DESC,SORT_STR,EVENT_TYPE) values ('2010','每日计提','货币基金（万份收益）-货币基金按照上一工作日每万份收益乘以持有份额，按日计提应收红利',5,null);
Insert into TBK_EVENT (EVENT_ID,EVENT_NAME,EVENT_DESC,SORT_STR,EVENT_TYPE) values ('2020','每日估值','债券基金每日根据净值进行差额估值',6,null);
Insert into TBK_EVENT (EVENT_ID,EVENT_NAME,EVENT_DESC,SORT_STR,EVENT_TYPE) values ('1020','申购确认','货币/债券基金申购完成后触发事件（经营部门根据协议内容约定，在公募基金认购或申购期间内，向营运部出具资金划拨单，由营运部将相应款项划入指定投资账户）',0,null);
Insert into TBK_EVENT (EVENT_ID,EVENT_NAME,EVENT_DESC,SORT_STR,EVENT_TYPE) values ('1030','红利再投','货币基金完成红利再投审批后触发事件（在收益结转日，公募基金收益会结转成相应份额，经营部门收到基金公司的收益结转确认书，交由营运部进行收益结转确认）',0,null);
Insert into TBK_EVENT (EVENT_ID,EVENT_NAME,EVENT_DESC,SORT_STR,EVENT_TYPE) values ('1040','赎回确认','货币/债券基金赎回确认完成后触发事件（在投资期限届满或根据实际需求，经营部门对投资公募基金进行全部或部分赎回，并通过重要业务联系单将相关指令告知总行营运部，由营运部进行确认）',0,null);
Insert into TBK_EVENT (EVENT_ID,EVENT_NAME,EVENT_DESC,SORT_STR,EVENT_TYPE) values ('1050','红利分红','债券基金分红完成后触发事件',5,null);
REM INSERTING into TBK_PARAMS
SET DEFINE OFF;
Insert into TBK_PARAMS (PARAM_ID,PARAM_NAME,PARAM_QZ,PARAM_SOURCE,LAST_UPDATE,ACTIVE) values ('invType','会计类型（可售、交易、持有到期）',1,'invType','2018-06-05 21:25:08','1');
Insert into TBK_PARAMS (PARAM_ID,PARAM_NAME,PARAM_QZ,PARAM_SOURCE,LAST_UPDATE,ACTIVE) values ('ccy','货币代码（人民币、美元……）',2,'ccy','2018-06-05 21:25:48','1');
REM INSERTING into TBK_PARAMS_MAP
SET DEFINE OFF;
Insert into TBK_PARAMS_MAP (P_KEY,PARAM_ID,PARAM_NAME,P_VALUE,P_VALUE_TXT,LAST_UPDATE) values ('332','invType','会计类型（可售、交易、持有到期）','3','可供出售（会计四分类）',null);
Insert into TBK_PARAMS_MAP (P_KEY,PARAM_ID,PARAM_NAME,P_VALUE,P_VALUE_TXT,LAST_UPDATE) values ('333','invType','会计类型（可售、交易、持有到期）','2','交易类（会计四分类）',null);
Insert into TBK_PARAMS_MAP (P_KEY,PARAM_ID,PARAM_NAME,P_VALUE,P_VALUE_TXT,LAST_UPDATE) values ('335','ccy','货币代码（人民币、美元……）','CNY','人民币',null);
Insert into TBK_PARAMS_MAP (P_KEY,PARAM_ID,PARAM_NAME,P_VALUE,P_VALUE_TXT,LAST_UPDATE) values ('336','ccy','货币代码（人民币、美元……）','USD','美元',null);
Insert into TBK_PARAMS_MAP (P_KEY,PARAM_ID,PARAM_NAME,P_VALUE,P_VALUE_TXT,LAST_UPDATE) values ('337','ccy','货币代码（人民币、美元……）','HKD','港币',null);
REM INSERTING into TBK_SUBJECT_DEF
SET DEFINE OFF;
Insert into TBK_SUBJECT_DEF (TASK_ID,SUBJ_CODE,SUBJ_NAME,PARENT_SUBJ_CODE,SUBJ_TYPE,DEBIT_CREDIT,PAY_RECIVE,SUBJ_NATURE,DIRECTION,SUBJ_LEVEL,SOURCE) values (null,'110','交易性金融资产',null,'01',null,null,null,null,null,null);
Insert into TBK_SUBJECT_DEF (TASK_ID,SUBJ_CODE,SUBJ_NAME,PARENT_SUBJ_CODE,SUBJ_TYPE,DEBIT_CREDIT,PAY_RECIVE,SUBJ_NATURE,DIRECTION,SUBJ_LEVEL,SOURCE) values (null,'11001','交易性金融资产成本','110','01',null,null,null,null,null,null);
Insert into TBK_SUBJECT_DEF (TASK_ID,SUBJ_CODE,SUBJ_NAME,PARENT_SUBJ_CODE,SUBJ_TYPE,DEBIT_CREDIT,PAY_RECIVE,SUBJ_NATURE,DIRECTION,SUBJ_LEVEL,SOURCE) values (null,'11002','交易性金融资产公允价值变动','110','01',null,null,null,null,null,null);
Insert into TBK_SUBJECT_DEF (TASK_ID,SUBJ_CODE,SUBJ_NAME,PARENT_SUBJ_CODE,SUBJ_TYPE,DEBIT_CREDIT,PAY_RECIVE,SUBJ_NATURE,DIRECTION,SUBJ_LEVEL,SOURCE) values (null,'11602','其他应收利息','116','01',null,null,null,null,null,null);
Insert into TBK_SUBJECT_DEF (TASK_ID,SUBJ_CODE,SUBJ_NAME,PARENT_SUBJ_CODE,SUBJ_TYPE,DEBIT_CREDIT,PAY_RECIVE,SUBJ_NATURE,DIRECTION,SUBJ_LEVEL,SOURCE) values (null,'14101','可供出售金融资产成本','141','01',null,null,null,null,null,null);
Insert into TBK_SUBJECT_DEF (TASK_ID,SUBJ_CODE,SUBJ_NAME,PARENT_SUBJ_CODE,SUBJ_TYPE,DEBIT_CREDIT,PAY_RECIVE,SUBJ_NATURE,DIRECTION,SUBJ_LEVEL,SOURCE) values (null,'50209','其他资本公积','502','05',null,null,null,null,null,null);
Insert into TBK_SUBJECT_DEF (TASK_ID,SUBJ_CODE,SUBJ_NAME,PARENT_SUBJ_CODE,SUBJ_TYPE,DEBIT_CREDIT,PAY_RECIVE,SUBJ_NATURE,DIRECTION,SUBJ_LEVEL,SOURCE) values (null,'5020952510000020','投资债券型基金公允价值变动','50209','05',null,null,null,null,null,null);
Insert into TBK_SUBJECT_DEF (TASK_ID,SUBJ_CODE,SUBJ_NAME,PARENT_SUBJ_CODE,SUBJ_TYPE,DEBIT_CREDIT,PAY_RECIVE,SUBJ_NATURE,DIRECTION,SUBJ_LEVEL,SOURCE) values (null,'6050175810000001','交易性-债券型基金投资收益','60501','03',null,null,null,null,null,null);
Insert into TBK_SUBJECT_DEF (TASK_ID,SUBJ_CODE,SUBJ_NAME,PARENT_SUBJ_CODE,SUBJ_TYPE,DEBIT_CREDIT,PAY_RECIVE,SUBJ_NATURE,DIRECTION,SUBJ_LEVEL,SOURCE) values (null,'6050175820000001','交易性-货币基金投资收益','60501','03',null,null,null,null,null,null);
Insert into TBK_SUBJECT_DEF (TASK_ID,SUBJ_CODE,SUBJ_NAME,PARENT_SUBJ_CODE,SUBJ_TYPE,DEBIT_CREDIT,PAY_RECIVE,SUBJ_NATURE,DIRECTION,SUBJ_LEVEL,SOURCE) values (null,'6050175840000001','可供出售-货币基金投资收益','60501','03',null,null,null,null,null,null);
Insert into TBK_SUBJECT_DEF (TASK_ID,SUBJ_CODE,SUBJ_NAME,PARENT_SUBJ_CODE,SUBJ_TYPE,DEBIT_CREDIT,PAY_RECIVE,SUBJ_NATURE,DIRECTION,SUBJ_LEVEL,SOURCE) values (null,'613','公允价值变动损益',null,'05',null,null,null,null,null,null);
Insert into TBK_SUBJECT_DEF (TASK_ID,SUBJ_CODE,SUBJ_NAME,PARENT_SUBJ_CODE,SUBJ_TYPE,DEBIT_CREDIT,PAY_RECIVE,SUBJ_NATURE,DIRECTION,SUBJ_LEVEL,SOURCE) values (null,'1100150140000001','交易性-投资货币基金成本','11001','01',null,null,null,null,null,null);
Insert into TBK_SUBJECT_DEF (TASK_ID,SUBJ_CODE,SUBJ_NAME,PARENT_SUBJ_CODE,SUBJ_TYPE,DEBIT_CREDIT,PAY_RECIVE,SUBJ_NATURE,DIRECTION,SUBJ_LEVEL,SOURCE) values (null,'1100150140000002','交易性-投资债券型基金成本','11001','01',null,null,null,null,null,null);
Insert into TBK_SUBJECT_DEF (TASK_ID,SUBJ_CODE,SUBJ_NAME,PARENT_SUBJ_CODE,SUBJ_TYPE,DEBIT_CREDIT,PAY_RECIVE,SUBJ_NATURE,DIRECTION,SUBJ_LEVEL,SOURCE) values (null,'1160275780000001','交易性-货币基金应收红利','11602','01',null,null,null,null,null,null);
Insert into TBK_SUBJECT_DEF (TASK_ID,SUBJ_CODE,SUBJ_NAME,PARENT_SUBJ_CODE,SUBJ_TYPE,DEBIT_CREDIT,PAY_RECIVE,SUBJ_NATURE,DIRECTION,SUBJ_LEVEL,SOURCE) values (null,'1160275780000004','可供出售-债券型基金应收红利','11602','01',null,null,null,null,null,null);
Insert into TBK_SUBJECT_DEF (TASK_ID,SUBJ_CODE,SUBJ_NAME,PARENT_SUBJ_CODE,SUBJ_TYPE,DEBIT_CREDIT,PAY_RECIVE,SUBJ_NATURE,DIRECTION,SUBJ_LEVEL,SOURCE) values (null,'141','可供出售金融资产',null,'01',null,null,null,null,null,null);
Insert into TBK_SUBJECT_DEF (TASK_ID,SUBJ_CODE,SUBJ_NAME,PARENT_SUBJ_CODE,SUBJ_TYPE,DEBIT_CREDIT,PAY_RECIVE,SUBJ_NATURE,DIRECTION,SUBJ_LEVEL,SOURCE) values (null,'1410475800000001','投资债券型基金公允价值变动','14104','01',null,null,null,null,null,null);
Insert into TBK_SUBJECT_DEF (TASK_ID,SUBJ_CODE,SUBJ_NAME,PARENT_SUBJ_CODE,SUBJ_TYPE,DEBIT_CREDIT,PAY_RECIVE,SUBJ_NATURE,DIRECTION,SUBJ_LEVEL,SOURCE) values (null,'61301','交易性金融资产公允价值变动损益','613','05',null,null,null,null,null,null);
Insert into TBK_SUBJECT_DEF (TASK_ID,SUBJ_CODE,SUBJ_NAME,PARENT_SUBJ_CODE,SUBJ_TYPE,DEBIT_CREDIT,PAY_RECIVE,SUBJ_NATURE,DIRECTION,SUBJ_LEVEL,SOURCE) values (null,'116','应收账款',null,'01',null,null,null,null,null,null);
Insert into TBK_SUBJECT_DEF (TASK_ID,SUBJ_CODE,SUBJ_NAME,PARENT_SUBJ_CODE,SUBJ_TYPE,DEBIT_CREDIT,PAY_RECIVE,SUBJ_NATURE,DIRECTION,SUBJ_LEVEL,SOURCE) values (null,'10201','存放中央银行准备金存款','102','01',null,null,null,null,null,null);
Insert into TBK_SUBJECT_DEF (TASK_ID,SUBJ_CODE,SUBJ_NAME,PARENT_SUBJ_CODE,SUBJ_TYPE,DEBIT_CREDIT,PAY_RECIVE,SUBJ_NATURE,DIRECTION,SUBJ_LEVEL,SOURCE) values (null,'1100275770000001','投资债券型基金公允价值变动','11002','01',null,null,null,null,null,null);
Insert into TBK_SUBJECT_DEF (TASK_ID,SUBJ_CODE,SUBJ_NAME,PARENT_SUBJ_CODE,SUBJ_TYPE,DEBIT_CREDIT,PAY_RECIVE,SUBJ_NATURE,DIRECTION,SUBJ_LEVEL,SOURCE) values (null,'502','资本公积',null,'05',null,null,null,null,null,null);
Insert into TBK_SUBJECT_DEF (TASK_ID,SUBJ_CODE,SUBJ_NAME,PARENT_SUBJ_CODE,SUBJ_TYPE,DEBIT_CREDIT,PAY_RECIVE,SUBJ_NATURE,DIRECTION,SUBJ_LEVEL,SOURCE) values (null,'102','存放中央银行款项',null,'01',null,null,null,null,null,null);
Insert into TBK_SUBJECT_DEF (TASK_ID,SUBJ_CODE,SUBJ_NAME,PARENT_SUBJ_CODE,SUBJ_TYPE,DEBIT_CREDIT,PAY_RECIVE,SUBJ_NATURE,DIRECTION,SUBJ_LEVEL,SOURCE) values (null,'1160275780000003','交易性-债券型基金应收红利','11602','01',null,null,null,null,null,null);
Insert into TBK_SUBJECT_DEF (TASK_ID,SUBJ_CODE,SUBJ_NAME,PARENT_SUBJ_CODE,SUBJ_TYPE,DEBIT_CREDIT,PAY_RECIVE,SUBJ_NATURE,DIRECTION,SUBJ_LEVEL,SOURCE) values (null,'1020100160000001','存放中央银行超额存款准备金','10201','01',null,null,null,null,null,null);
Insert into TBK_SUBJECT_DEF (TASK_ID,SUBJ_CODE,SUBJ_NAME,PARENT_SUBJ_CODE,SUBJ_TYPE,DEBIT_CREDIT,PAY_RECIVE,SUBJ_NATURE,DIRECTION,SUBJ_LEVEL,SOURCE) values (null,'1160275780000002','可供出售-货币基金应收红利','11602','01',null,null,null,null,null,null);
Insert into TBK_SUBJECT_DEF (TASK_ID,SUBJ_CODE,SUBJ_NAME,PARENT_SUBJ_CODE,SUBJ_TYPE,DEBIT_CREDIT,PAY_RECIVE,SUBJ_NATURE,DIRECTION,SUBJ_LEVEL,SOURCE) values (null,'14104','可供出售金融资产公允价值变动','141','01',null,null,null,null,null,null);
Insert into TBK_SUBJECT_DEF (TASK_ID,SUBJ_CODE,SUBJ_NAME,PARENT_SUBJ_CODE,SUBJ_TYPE,DEBIT_CREDIT,PAY_RECIVE,SUBJ_NATURE,DIRECTION,SUBJ_LEVEL,SOURCE) values (null,'1410175790000001','投资货币基金成本','14101','01',null,null,null,null,null,null);
Insert into TBK_SUBJECT_DEF (TASK_ID,SUBJ_CODE,SUBJ_NAME,PARENT_SUBJ_CODE,SUBJ_TYPE,DEBIT_CREDIT,PAY_RECIVE,SUBJ_NATURE,DIRECTION,SUBJ_LEVEL,SOURCE) values (null,'6050175830000001','可供出售-债券型基金投资收益','60501','03',null,null,null,null,null,null);
Insert into TBK_SUBJECT_DEF (TASK_ID,SUBJ_CODE,SUBJ_NAME,PARENT_SUBJ_CODE,SUBJ_TYPE,DEBIT_CREDIT,PAY_RECIVE,SUBJ_NATURE,DIRECTION,SUBJ_LEVEL,SOURCE) values (null,'1410175790000002','投资债券型基金成本','14101','01',null,null,null,null,null,null);
Insert into TBK_SUBJECT_DEF (TASK_ID,SUBJ_CODE,SUBJ_NAME,PARENT_SUBJ_CODE,SUBJ_TYPE,DEBIT_CREDIT,PAY_RECIVE,SUBJ_NATURE,DIRECTION,SUBJ_LEVEL,SOURCE) values (null,'605','投资收益',null,'03',null,null,null,null,null,null);
Insert into TBK_SUBJECT_DEF (TASK_ID,SUBJ_CODE,SUBJ_NAME,PARENT_SUBJ_CODE,SUBJ_TYPE,DEBIT_CREDIT,PAY_RECIVE,SUBJ_NATURE,DIRECTION,SUBJ_LEVEL,SOURCE) values (null,'60501','可供交易金融资产投资收益','605','03',null,null,null,null,null,null);
Insert into TBK_SUBJECT_DEF (TASK_ID,SUBJ_CODE,SUBJ_NAME,PARENT_SUBJ_CODE,SUBJ_TYPE,DEBIT_CREDIT,PAY_RECIVE,SUBJ_NATURE,DIRECTION,SUBJ_LEVEL,SOURCE) values (null,'6130175850000001','投资债券型基金公允价值变动损益','61301','05',null,null,null,null,null,null);
--------------------------------------------------------
--  DDL for Index PK_TBK_PRODUCT_EVENT
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_TBK_PRODUCT_EVENT" ON "TBK_PRODUCT_EVENT" ("DB_ID")
--------------------------------------------------------
--  DDL for Index SYS_C0014425
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0014425" ON "TBK_AMOUNT" ("AMOUNT_ID")
--------------------------------------------------------
--  DDL for Index PK_TBK_EVENT
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_TBK_EVENT" ON "TBK_EVENT" ("EVENT_ID")
--------------------------------------------------------
--  DDL for Index PK_TBK_PARAMS
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_TBK_PARAMS" ON "TBK_PARAMS" ("PARAM_ID")
--------------------------------------------------------
--  DDL for Index PK_TBK_PARAMS_MAP
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_TBK_PARAMS_MAP" ON "TBK_PARAMS_MAP" ("PARAM_ID", "P_VALUE")
--------------------------------------------------------
--  Constraints for Table TBK_PRODUCT_EVENT
--------------------------------------------------------

  ALTER TABLE "TBK_PRODUCT_EVENT" ADD CONSTRAINT "PK_TBK_PRODUCT_EVENT" PRIMARY KEY ("DB_ID") ENABLE
 
  ALTER TABLE "TBK_PRODUCT_EVENT" MODIFY ("DB_ID" NOT NULL ENABLE)
--------------------------------------------------------
--  Constraints for Table TBK_AMOUNT
--------------------------------------------------------

  ALTER TABLE "TBK_AMOUNT" MODIFY ("AMOUNT_ID" NOT NULL ENABLE)
 
  ALTER TABLE "TBK_AMOUNT" MODIFY ("AMOUNT_NAME" NOT NULL ENABLE)
 
  ALTER TABLE "TBK_AMOUNT" MODIFY ("AMOUNT_FORMULA" NOT NULL ENABLE)
 
  ALTER TABLE "TBK_AMOUNT" ADD PRIMARY KEY ("AMOUNT_ID") ENABLE
--------------------------------------------------------
--  Constraints for Table TBK_EVENT
--------------------------------------------------------

  ALTER TABLE "TBK_EVENT" ADD CONSTRAINT "PK_TBK_EVENT" PRIMARY KEY ("EVENT_ID") ENABLE
 
  ALTER TABLE "TBK_EVENT" MODIFY ("EVENT_ID" NOT NULL ENABLE)
 
  ALTER TABLE "TBK_EVENT" MODIFY ("EVENT_NAME" NOT NULL ENABLE)
 
  ALTER TABLE "TBK_EVENT" MODIFY ("SORT_STR" NOT NULL ENABLE)
--------------------------------------------------------
--  Constraints for Table TBK_PARAMS
--------------------------------------------------------

  ALTER TABLE "TBK_PARAMS" ADD CONSTRAINT "PK_TBK_PARAMS" PRIMARY KEY ("PARAM_ID") ENABLE
 
  ALTER TABLE "TBK_PARAMS" MODIFY ("PARAM_ID" NOT NULL ENABLE)
--------------------------------------------------------
--  Constraints for Table TBK_PARAMS_MAP
--------------------------------------------------------

  ALTER TABLE "TBK_PARAMS_MAP" ADD CONSTRAINT "PK_TBK_PARAMS_MAP" PRIMARY KEY ("PARAM_ID", "P_VALUE") ENABLE
 
  ALTER TABLE "TBK_PARAMS_MAP" MODIFY ("P_KEY" NOT NULL ENABLE)
 
  ALTER TABLE "TBK_PARAMS_MAP" MODIFY ("PARAM_ID" NOT NULL ENABLE)
 
  ALTER TABLE "TBK_PARAMS_MAP" MODIFY ("P_VALUE" NOT NULL ENABLE)
--------------------------------------------------------
--  Constraints for Table TBK_SUBJECT_DEF
--------------------------------------------------------

  ALTER TABLE "TBK_SUBJECT_DEF" MODIFY ("SUBJ_CODE" NOT NULL ENABLE)
