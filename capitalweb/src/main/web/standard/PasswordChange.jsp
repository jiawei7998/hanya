<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="./global.jsp"%>
<script src="<%=basePath%>/miniScript/md5.js" type="text/javascript" ></script>
<script src="<%=basePath%>/miniScript/sha512.js" type="text/javascript" ></script>  

<style>
table th{
text-align:left;
padding-left:10px;
width:120px;
   border-right: 0;
   }
   .header {
   height: 30px;
   /* background: #95B8E7; */
   color: #404040;
   background: linear-gradient(to bottom,#EFF5FF 0,#E0ECFF 100%);
   background-repeat: repeat-x;
   line-height:30px; /*设置line-height与父级元素的height相等*/
   text-align: center; /*设置文本水平居中*/
   font-size:18px;
}
</style>
<div id="layout1" class="mini-layout" style="width:100%;height:100%;">
	 <div class="header" region="north" height="32" showSplit="false" showHeader="false">
        密码设置
    </div>
    <div title="center" region="center" bodyStyle="overflow:hidden;">
		<table id="pwd_form" class="form_table">
			<tr>
				<th lass="red-star">原密码：</th>
				<td><input id="oldPwd" name="oldPwd" class="mini-password" vtype="rangeLength:1,20"  required="true"  /></td>
				<td><td id="oldPwd_error" class="errorText"></td></td>
			</tr>
			<tr>
				<th class="red-star">新密码</th>
				<td><input id="newPwd" name="newPwd" class="mini-password " vtype="rangeLength:1,20"   required="true"  /></td>
				<td><td id="newPwd_error" class="errorText"></td></td>
			</tr>
			<tr>
				<th class="red-star">密码确认</th>
				<td><input id="newPwdConfirm" name="newPwdConfirm" class="mini-password" vtype="rangeLength:1,20"  required="true" /></td>
				<td><td id="newPwdConfirm_error" class="errorText"></td></td>
			</tr>
		</table>	
	</div>
	<div title="south" region="south" showSplit="false" showHeader="false" height="39px;"  style="line-height:39px;text-align: center;">
        <div id="functionIds">
       		 <a id="add_btn"  class="mini-button"  style="width:120px;">保存修改</a>
		</div>
    </div>
</div>


<script type="text/javascript">
	mini.parse();

	$('#add_btn').on('click',function(){

		if(mini.get('newPwd').getValue() != mini.get('newPwdConfirm').getValue()){
			mini.alert('两次密码输入不一致','提示信息');
			return;
		}
		var form = new mini.Form("#pwd_form");
			form.validate();
			if (form.isValid() == false) return;
				var param = form.getData(); //获取表单多个控件的数据

			var mediumRegex = new RegExp("^(?=.{6,})(((?=.*[A-Z])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[0-9]))|((?=.*[a-z])(?=.*[0-9]))).*$", "g");
		    var enoughRegex = new RegExp("(?=.{6,}).*", "g");
		    var strongRegex=new RegExp("^(?=.*[a-zA-Z])^(?=.*?[0-9])^(?=.*?[.,/?!@#$%^&*-_+=]+$).{6,}");
		    // 密码必须同时包含字母、数字及特殊符号！且不能少于8位
		    var string = new RegExp("^(?=.*[a-zA-Z])");
		    var number = new RegExp("^(?=.*?[0-9])");
			//var str = new RegExp("(?=.*?[.~!@#$%\^\+\*&\\\/?\|:\.{}()';=\"])");

		    if(param.newPwd.length <8){
		   	 	mini.alert("密码长度不能少于8位","提示");	
		    	return false;
		    }
			if(string.test(param.newPwd) && number.test(param.newPwd)){
				param.newPwdConfirm = "";
// 				param.oldPwd = hex_md5(param.oldPwd).toUpperCase();
// 				param.newPwd = hex_md5(param.newPwd).toUpperCase();
				param.oldPwd = hex_sha512(param.oldPwd).toUpperCase();
				param.newPwd = hex_sha512(param.newPwd).toUpperCase();
				
				CommonUtil.ajax({
					url:'/UserController/changeUserPassword/',
					data:mini.encode(param),
					relbtn:"save_btn",
					callback:function(data){
						mini.alert("修改成功.","提示",function(){
							//关闭自己
							window.CloseOwnerWindow("ok");
							
						});
						
					}
				});
			
			}else if(string.test(param.newPwd) || number.test(param.newPwd)){
				mini.alert("密码必须同时包含字母、数字及特殊符号！","提示");
 			}else {
				mini.alert("密码不符合规范！","提示");	
			}
            
		});
		

</script>
