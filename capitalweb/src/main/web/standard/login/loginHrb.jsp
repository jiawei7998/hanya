<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="./../global.jsp"%>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>韩亚银行资金管理系统</title>
    <link rel="stylesheet" type="text/css" href="<%=basePath%>/miniScript/css/login/zui.css" media="all">
    <link rel="stylesheet" type="text/css" href="<%=basePath%>/miniScript/css/login/login.css" media="all">
    <link rel="stylesheet" type="text/css" href="<%=basePath%>/miniScript/css/login/animate.min.css" >
    <link rel="stylesheet" type="text/css" href="<%=basePath%>/miniScript/css/login/font-awesome.min.css">
    <script src="<%=basePath%>/miniScript/sha512.js" type="text/javascript" ></script>
    <script src="<%=basePath%>/miniScript/jscookie.min.js" type="text/javascript" ></script>
</head>
<body id="mainBox" style="box-sizing:border-box;overflow: hidden;">
<div class="main-box">
    <div class="main-content" id="main-content">
        <div class="login-body  ">
            <div class="login-logo"></div>
            <div class="login-text">
                <h3> 资金业务管理系统</h3>
                <h5 style="padding-bottom: 10px;" > Capital Trade Management System</h5>
            </div>
            <div class="login-main pr">
                <form action="javascript:;" method="post" class="login-form">
<%--                    <h3> 用户登录 </h3>--%>
<%--                    <h5 style="padding-bottom: 10px"></h5>--%>
                    <!-- 账号登陆 -->
                    <div id="MobileBox" class="item-box">
                        <div class="input-group user-name"><span class="input-group-addon"><i
                                class="icon-user"></i></span>
                            <input type="text" name="userId" id="userId" class="form-control" placeholder="用户名">
                            <input type="hidden" id="branchId" name="branchId"/>
                        </div>
                        <div class="input-group password"><span class="input-group-addon"><i
                                class="icon-lock"></i></span>
                            <input type="password" name="passwd" id="passwd" class="form-control" placeholder="密码">
                        </div>
                        <div class="login_btn_panel">
                            <button id="btn" class=" btn btn-primary btn-block btn-lg" data-ajax="post"
                                    data-callback="success" type="submit" onclick="open_index()">登录
                            </button>
                            <div class="check-tips"></div>
                        </div>
                </form>
            </div>
        </div>
    </div>
</div>
<style>
    .copyrights{text-indent:-9999px;height:0;line-height:0;font-size:0;overflow:hidden;}
</style>
<script type="text/javascript">
    $(document).ready(function(){
        autoHeight();
        $('#userId').keydown(function (e){
            if(e.keyCode ==13){
                $('#passwd').focus();
            }
        });
        $('#passwd').keydown(function (e){
            if(e.keyCode ==13){
                $('#btn').focus();
            }
        });
        $('#userId').change(function(){
            $('.check-tips').html("");
            $('#passwd').val("");
        });

        $('#passwd').change(function(){
            $('.check-tips').html("");
        });
        //获取当前银行信息
        CommonUtil.ajax({
            url:"/brccController/getBrccs",
            callback:function(data){
                $('#branchId').val(data.obj[0].branchId);
<%--                 $(".login-logo").css('background','url("<%=basePath%>/miniScript/css/images/'+data.obj[0].branchLogo+'.png") no-repeat right'); --%>
//                 $(".login-logo").css('background-size','contain');
                $('#userId').focus();
            }
        });
    });

    function open_index() {
        let param = {};
        let userId = $('#userId').val();
        let passwd = $('#passwd').val();
        if (userId =="" ){
            $('.check-tips').html("请输入用户!");
            return;
        }
        if (passwd == ""){
            $('.check-tips').html("请输入密码!");
            return;
        }
        // if (typeof(Cookies.get('userId')) != "undefined" &amp;&amp; Cookies.get('userId') != data.userId){ //不存在
        //     $('.check-tips').html("已登录其它用户,不允许多个用户登录!");
        //     return;
        // }
        //data.passwd = hex_md5(data.passwd);
//         param.passwd = hex_sha512(passwd);
        param.userId = userId;
        param.branchId =$('#branchId').val();
        param.isActive = '1';
        CommonUtil.ajax({
            url:"/UserController/checkLogin",
            data:param,
            callback : function(ret) {
                if(ret.desc!="error.system.0000"){
                    mini.confirm('用户已在其他地址登录，您确认要继续登录系统吗？','系统提示',function(r){
                        if (r=='ok'){
                            CommonUtil.ajax({
                                url:"/UserController/loginSimple",
                                data:param,
                                callback:function(data){
                                    //写入本地cookie
                                    Cookies.set('userId', data.detail, {expires: 0.01});
                                    window.location.href =data.desc;
                                }
                            });
                        }
                    });
                }else{
                    CommonUtil.ajax({
                        url:"/UserController/loginSimple",
                        data:param,
                        callback:function(data){
                            window.location.href =data.desc;
                        }
                    });
                }
            }
        });
    }

    //高度自适应屏幕高度
    function autoHeight(){
        if (window.innerHeight){
            nowHeight = window.innerHeight;
        }else{
            nowHeight = document.documentElement.clientHeight;
        }
        document.getElementById('main-content').style.height = nowHeight + 'px';
    }
    autoHeight();
    window.onresize = autoHeight;
</script>
</body>
</html>
