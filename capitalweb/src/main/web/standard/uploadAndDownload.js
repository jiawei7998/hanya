//上传
   function upload(){
	   var ticketId=mini.get("ticketId").getValue();
	   if(!ticketId||ticketId==""){
		   mini.alert("请先保存交易信息", "消息");
	       return;
	   }
       var fdata = mini.get("Fdata"), value =fdata.getValue(),file = $("input[name='Fdata']")[0];
       if(!value||value==""){
       	mini.alert("请选择上传文件", "消息");
       	return;
       }
       
       $.ajaxFileUpload({
           url: CommonUtil.pPath + '/sl/IfsForeignController/uploadFile',         //用于文件上传的服务器端请求地址
           fileElementId:file,      
           //文件上传域的ID
           data:{ticketId:ticketId},
           dataType: 'text',                                     //返回值类型 一般设置为json
           success: function (data, status)                     //服务器成功响应处理函数
           {
           	var json = JSON.parse(data);
				mini.alert(json.desc, "消息", function() {
					fdata.setText("");
					window.location.reload();
				});
			},
			error : function(data, status, e) //服务器响应失败处理函数
			{
				mini.alert("上传失败：" + e, "消息",function(){
					fdata.setText("");
					window.location.reload();
				});
			}
		});
   }
 	
 	//下载附件
   function download(){
	    var ticketId=mini.get("ticketId").getValue();
	   	if(ticketId){
	   		window.open(CommonUtil.pPath+'/sl/IfsForeignController/downloadFile/'+ticketId);
	   	}	
   }