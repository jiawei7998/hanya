<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<title>外币拆借</title>
<script type="text/javascript" >
	/**prdCode用于页面投资组合的选择   该全局变量必须为prdCode*/
	var prdCode="GOL";
	</script>
</head>
<body style="width:100%;height:100%;background:white">
	<div class="mini-splitter" style="width:100%;height:100%;">
			<div size="90%" showCollapseButton="false">
<h1 style="text-align:center"><strong>黄金拆借交易</strong></h1>
	<div  id="field_form"  class="mini-fit area" style="background:white" >
	<input id="dealTransType" name="dealTransType" class="mini-hidden"/>
	<input id="sponsor" name="sponsor" class="mini-hidden" />
	<input id="sponInst" name="sponInst" class="mini-hidden" />
	<input id="ticketId" name="ticketId" class="mini-hidden" />
	<input id="aDate" name="aDate" class="mini-hidden"/>
	<div class="mini-panel" title="成交单编号" style="width:100%;"  allowResize="true" collapseOnTitleClick="true">
			<div class="leftarea">
				<input style="width:100%;" id="contractId" name="contractId" class="mini-textbox mini-mustFill" labelField="true" requiredErrorText="该输入项为必输项" required="true"  label="成交单编号：" labelStyle="text-align:left;width:130px;" onvalidation="onEnglishAndNumberValidations" vtype="maxLength:20"/>
			</div>
		</div>
	<div class="mini-panel" title="成交双方信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
		<div class="leftarea">
			<input id="instId" name="instId" class="mini-textbox" labelField="true"  label="本方机构："  style="width:100%;" maxLength="25" labelStyle="text-align:left;width:130px;" />
			<input id="dealer" name="dealer" class="mini-textbox" labelField="true"  label="本方交易员：" maxLength="20" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
			<input id="forDate" name="forDate" class="mini-datepicker" labelField="true"  label="交易日期："  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
		</div>
		<div class="rightarea">
			<input id="counterpartyInstId" name="counterpartyInstId" class="mini-buttonedit mini-mustFill" labelField="true" allowInput="false" label="对方机构：" onbuttonclick="onButtonEdit" maxLength="25" style="width:100%;"  labelStyle="text-align:left;width:130px;" required="true"  />
			<input id="counterpartyDealer" name="counterpartyDealer" class="mini-textbox" labelField="true"  label="对方交易员：" maxLength="20" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
			<input id="forTime" name="forTime" class="mini-timespinner" labelField="true" format="H:mm:ss" label="交易时间："  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
		</div>
	</div>
	<%@ include file="../../Common/opicsLess.jsp"%>
	<%@ include file="../../Common/RiskCenter.jsp"%>
	<div class="mini-panel" title="交易主体信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
		<div class="leftarea">
			<input style="width:100%;" id="currency" name="currency" class="mini-combobox" labelField="true"  data="CommonUtil.serverData.dictionary.metalccy" label="货币："   labelStyle="text-align:left;width:130px;" />
			<input style="width:100%;" id="basis" name="basis" class="mini-combobox" labelField="true"  label="计息基准：" onValuechanged="interestAmount"  data="CommonUtil.serverData.dictionary.calIrsDays" labelStyle="text-align:left;width:130px;" />
			<input style="width:100%;" id="tenor" name="tenor" class="mini-combobox" labelField="true"  label="期限：" data="CommonUtil.serverData.dictionary.Term" labelStyle="text-align:left;width:130px;"    />
			<input style="width:100%;" id="maturityDate" name="maturityDate" class="mini-datepicker mini-mustFill" labelField="true" onValuechanged="compareDate" label="到期日：" ondrawdate="onDrawDateEnd" required="true"  labelStyle="text-align:left;width:130px;"    />
			<input style="width:100%;" id="amount" name="amount" class="mini-spinner mini-mustFill" labelField="true"  changeOnMousewheel='false'  format="n4" label="拆借金额：" onValuechanged="interestAmount" required="true"  maxValue="999999999999999" labelStyle="text-align:left;width:130px;"    />
			<input style="width:100%;" id="repaymentAmount" name="repaymentAmount" class="mini-spinner mini-mustFill" labelField="true" changeOnMousewheel='false'  format="n4" label="到期还款金额：" required="true"  maxValue="999999999999999" labelStyle="text-align:left;width:130px;"    />
		</div>
		<div class="rightarea">
			<input style="width:100%;" id="rate" name="rate" class="mini-spinner mini-mustFill" labelField="true"  label="拆借利率：" onValuechanged="interestAmount"  changeOnMousewheel='false' required="true"  format="n8" labelStyle="text-align:left;width:130px;"    />
			<input style="width:100%;" id="direction" name="direction" class="mini-combobox" labelField="true"  label="交易方向：" data="CommonUtil.serverData.dictionary.trading" labelStyle="text-align:left;width:130px;" onvaluechanged="dirVerify" />
			<input style="width:100%;" id="valueDate" name="valueDate" class="mini-datepicker mini-mustFill" labelField="true" onValuechanged="compareDate" label="起息日：" ondrawdate="onDrawDateStart" required="true"   labelStyle="text-align:left;width:130px;" />
			<input style="width:100%;" id="occupancyDays" name="occupancyDays" class="mini-spinner" labelField="true" minErrorText="0" label="实际占款天数：" onValuechanged="interestAmount" changeOnMousewheel='false' maxValue="10000" labelStyle="text-align:left;width:130px;"    />
			<input style="width:100%;" id="interest" name="interest" class="mini-spinner mini-mustFill" labelField="true"  label="应计利息：" maxValue="999999999999999" changeOnMousewheel='false'  format="n4" required="true"  labelStyle="text-align:left;width:130px;" />
			<input id="rateCode" name="rateCode" class="mini-buttonedit mini-mustFill" onbuttonclick="onRateEdit" labelField="true" label="利率代码：" style="width: 100%;" labelStyle="text-align:left;width:130px;" allowInput="false" requiredErrorText="该输入项为必输项" required="true" />
		</div>
	</div>
	<div class="mini-panel" title="清算信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
		<div class="leftarea">
			<input id="myAccount" name="myAccount" class="mini-textbox" labelField="true"  label="本方清算账户"  style="width:100%;" maxLength="50" labelStyle="text-align:left;width:130px;" />
		</div>
		<div class="rightarea">
			<input id="otherAccount" name="otherAccount" class="mini-textbox" labelField="true"  label="对手方清算账户" maxLength="50" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
		</div>
	</div>
	
		<%@ include file="../../Common/Flow/MiniApproveOpCommon.jsp"%>
	</div>
</div>
<div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
		<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="save_btn"  onclick="save">保存交易</a></div>
		<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="close_btn"  onclick="close">关闭界面</a></div>
	</div>
	</div>
		<script type="text/javascript">
			mini.parse();
			//获取当前tab
			var currTab = top["win"].tabs.getActiveTab();
			var params = currTab.params;
			var row=params.selectData;
			var url = window.location.search;
			var action = CommonUtil.getParam(url, "action");
			var ticketId = CommonUtil.getParam(url, "ticketid");
			mini.get("forDate").setValue("<%=__bizDate %>");
			mini.get("instId").setEnabled(false);
			mini.get("dealer").setEnabled(false);
			
			var timestamp = Date.parse(new Date());
			sys(timestamp);
			function sys(stamp){
				var time = new Date(stamp);
				var result = "";
				result += CommonUtil.singleNumberFormatter(time.getHours()) + ":"; 
				result += CommonUtil.singleNumberFormatter(time.getMinutes()) + ":";
				result += CommonUtil.singleNumberFormatter(time.getSeconds());
				mini.get("forTime").setValue(result);
			}

			
			var tradeData={};
			
			tradeData.selectData=row;
			tradeData.operType=action;
			tradeData.serial_no=row.ticketId;
			tradeData.task_id=row.taskId;
			
			function inme() {
				if (action == "detail" || action == "edit"||action=="approve") {
					var from = new mini.Form("field_form");
					CommonUtil.ajax({
						url : '/IfsCurrencyController/searchCcyLending',
						data : {
							ticketId : ticketId
						},
						callback : function(text) {
							//投资组合
							mini.get("port").setValue(text.obj.port);
							mini.get("port").setText(text.obj.port);
							mini.get("counterpartyInstId").setValue(text.obj.counterpartyInstId);
							queryTextName(text.obj.counterpartyInstId);
							/* mini.get("countecounterpartyInstIdInstIdText(text.obj.counterpartyInstId); */
							from.setData(text.obj);
							mini.get("instId").setValue("<%=__sessionInstitution.getInstName()%>");
							//清算路径
							mini.get("ccysmeans").setValue(text.obj.ccysmeans);
							mini.get("ccysmeans").setText(text.obj.ccysmeans);
							mini.get("ctrsmeans").setValue(text.obj.ctrsmeans);
							mini.get("ctrsmeans").setText(text.obj.ctrsmeans);
							mini.get("rateCode").setValue(text.obj.rateCode);
							mini.get("rateCode").setText(text.obj.rateCode);
							
							//风险中台
							mini.get("applyNo").setValue(text.obj.applyNo);
							mini.get("applyNo").setText(text.obj.applyNo);
							queryCustNo(text.obj.custNo);
						}
					}); 
					if (action == "detail"||action=="approve") {
						mini.get("save_btn").hide();
						from.setEnabled(false);
						var form1=new mini.Form("approve_operate_form");
						form1.setEnabled(true);
					} 
					mini.get("contractId").setEnabled(false);
				}else if(action=="add"){
					mini.get("dealTransType").setValue("M");
					mini.get("instId").setValue("<%=__sessionInstitution.getInstName()%>");
					mini.get("dealer").setValue("<%=__sessionUser.getUserId() %>");
					mini.get("sponsor").setValue("<%=__sessionUser.getUserId()%>");
					mini.get("sponInst").setValue("<%=__sessionUser.getInstId() %>");
				}
			}
			
			function save() {
				var form = new mini.Form("field_form");
				form.validate();
				if (form.isValid() == false) {
					return;
				}
				var form = new mini.Form("field_form");
				var data = form.getData(true); //获取表单多个控件的数据
				data['instId']="<%=__sessionInstitution.getInstId()%>";
				var json = mini.encode(data); //序列化成JSON
				if (action == "add") {
					CommonUtil.ajax({
						url : "/IfsCurrencyController/addCcyLending",
						data : json,
						callback : function(data) {
							
							mini.alert(data,'提示信息',function(){
								if(data=='保存成功'){
								top["win"].closeMenuTab();
								}
							});
							
						}
					});
				} else if (action = "edit") {
					CommonUtil.ajax({
						url : "/IfsCurrencyController/editCcyLending",
						data : json,
						callback : function(data) {
							if (data.code == 'error.common.0000') {
								mini.alert(data.desc, '提示信息', function() {
									top["win"].closeMenuTab();
								});
							} else {
								mini.alert("保存失败");
							}
						}
					})
				}
			}
			function onButtonEdit(e) {
	            var btnEdit = this;
	            mini.open({
	                url: CommonUtil.baseWebPath() + "../../Common/CustMini.jsp",
	                title: "选择对手方列表",
	                width: 900,
	                height: 600,
	                ondestroy: function (action) {
	                    if (action == "ok") {
	                        var iframe = this.getIFrameEl();
	                        var data = iframe.contentWindow.GetData();
	                        data = mini.clone(data);    //必须
	                        if (data) {
	                            btnEdit.setValue(data.cno);
	                            btnEdit.setText(data.cname);
	                            btnEdit.focus();
	                        }
	                    }

	                }
	            });

	        }
			//根据交易对手编号查询全称
			function queryTextName(cno){
				CommonUtil.ajax({
				    url: "/IfsOpicsCustController/searchIfsOpicsCust",
				    data : {'cno' : cno},
				    callback:function (data) {
				    	if (data && data.obj) {
				    		mini.get("counterpartyInstId").setText(data.obj.cfn);
						} else {
							mini.get("counterpartyInstId").setText();
				    }
				    }
				});
			}
			//利率代码的选择
			function onRateEdit(){
				var btnEdit = this;
		        mini.open({
		            url: CommonUtil.baseWebPath() + "/opics/rateMini.jsp",
		            title: "选择利率代码",
		            width: 900,
		            height: 600,
		            ondestroy: function (action) {
		                if (action == "ok") {
		                    var iframe = this.getIFrameEl();
		                    var data = iframe.contentWindow.GetData();
		                    data = mini.clone(data);    //必须
		                    if (data) {
		                        btnEdit.setValue(data.ratecode);
		                        btnEdit.setText(data.ratecode);
		                        btnEdit.focus();
		                    }
		                }

		            }
		        });
			}
	//文、数字、下划线 的验证
	function onEnglishAndNumberValidations(e) {
		if(e.value == "" || e.value == null){//值为空，就不做校验
			return;
		}
		if (e.isValid) {
			if (isEnglishAndNumbers(e.value) == false) {
				e.errorText = "必须输入英文小写+数字";
				e.isValid = false;
			}
		}
	}
	/* 是否英文+数字 */
	function isEnglishAndNumbers(v) {
		var re = new RegExp("^[0-9a-z\_\-]+$");
		if (re.test(v)) return true;
		return false;
	}
	
			
			//英文、数字、下划线 的验证
			function onEnglishAndNumberValidation(e) {
				if(e.value == "" || e.value == null){//值为空，就不做校验
					return;
				}
				if (e.isValid) {
					if (isEnglishAndNumber(e.value) == false) {
						e.errorText = "必须输入英文+数字";
						e.isValid = false;
					}
				}
			}
			/* 是否英文+数字 */
			function isEnglishAndNumber(v) {
				var re = new RegExp("^[0-9a-zA-Z\_\-]+$");
				if (re.test(v)) return true;
				return false;
			}
			function close() {
				top["win"].closeMenuTab();
			}
			$(document).ready(function() {
				inme();
			});
			//日期判断
            function compareDate() {
                var sta = mini.get("valueDate");
                var end = mini.get("maturityDate");
                //mini.alert(sta.getValue());
               // mini.alert(end.getText());
               /*  if (sta.getValue() > end.getValue() && end.getValue("") && sta.getValue() !=null) {
                    mini.alert("起息日不能大于到期日", "系统提示");
                    return end.setValue(sta.getValue());
                } */
              if(sta.getValue()!=null && sta.getValue("")&& end.getValue("") && end.getValue()!=null){
                    sDate1 = Date.parse(sta.getValue());
                    sDate2 = Date.parse(end.getValue());
                    dateSpan = sDate2 - sDate1;
                    dateSpan = Math.abs(dateSpan);
                    iDays = Math.floor(dateSpan / (24 * 3600 * 1000));
                	mini.get("occupancyDays").setValue(iDays);
                }  
            }
        	//到期日 
        	function onDrawDateEnd(e) {
        		var date = e.date;
        		var d = mini.get("valueDate").getValue();
        		if(d==null || d == ""){
        			return;
        		}

        		if (date.getTime() < d.getTime()) {
        			e.allowSelect = false;
        		}
        	}
            //起息日 
        	function onDrawDateStart(e){
        		var date = e.date;
        		var d = mini.get("maturityDate").getValue();
        		if(d==null || d == ""){
        			return;
        		}

        		if (date.getTime() > d.getTime()) {
        			e.allowSelect = false;
        		}
        	}
			//利率计算
			function interestAmount(){
				var a = mini.get("rate").getValue();//利率
				var b = parseInt(mini.get("basis").getValue());//利率基础
				var c = mini.get("occupancyDays").getValue();//占款天数
				var d = mini.get("amount").getValue();//拆借金额
				if(a!="" && a!=null && b!="" && b!=null && c!="" && c!=null && d!="" && d!=null){
					if(b!=1){
						var e = 365;
					}else{
						var e = 360;
					}
					var interest=Number(a)*Number(c)*Number(d)/(parseInt(e)*100);
					mini.get("interest").setValue(interest);
					var allamount = interest + Number(d);
					mini.get("repaymentAmount").setValue(allamount);
				}
			}
			
			 /**   清算路径1的选择   */
			function settleMeans1(){
				 var cpInstId = mini.get("counterpartyInstId").getValue();
				if(cpInstId == null || cpInstId == ""){
					mini.alert("请先选择对方机构!");
					return;
				} 
				
				var ccy = mini.get("currency").getText();
				if(ccy == null || ccy == ""){
					mini.alert("请先选择货币!");
					return;
				}
				var url;
				var data;
				if(ccy!="CNY"){//外币
					url="./Ifs/opics/nostMini.jsp";
		        	data = { ccy:ccy ,cust:cpInstId};
		        }else if(ccy=="CNY"){//人民币
		        	data = { ccy:ccy ,cust:cpInstId};
		        	url="./Ifs/opics/setaMini.jsp";
		        }
				
				var btnEdit = this;
		       mini.open({
		           url: url,
		           title: "选择清算路径",
		           width: 900,
		           height: 600,
		           onload: function () {
		               var iframe = this.getIFrameEl();
		               iframe.contentWindow.SetData(data);
		           },
		           ondestroy: function (action) {
		               if (action == "ok") {
		                   var iframe = this.getIFrameEl();
		                   var data1 = iframe.contentWindow.GetData();
		                   data1 = mini.clone(data1);    //必须
		                   if (data1) {
		                	   if(ccy!="CNY"){//外币
		                    		mini.get("ccysmeans").setValue($.trim(data1.smeans));
		                    		mini.get("ccysmeans").setText($.trim(data1.smeans));
		                            mini.get("ccysacct").setValue($.trim(data1.nos));
		                        }else if(ccy=="CNY"){//人民币
		                        	mini.get("ccysmeans").setValue($.trim(data1.smeans));
			                    	 mini.get("ccysmeans").setText($.trim(data1.smeans));
			                         mini.get("ccysacct").setValue($.trim(data1.sacct)); 
		                        }
		                       btnEdit.focus();
		                   }
		               }
		           }
		       });
			}
			
			/**   清算路径2的选择   */
			function settleMeans2(){
				 var cpInstId = mini.get("counterpartyInstId").getValue();
				if(cpInstId == null || cpInstId == ""){
					mini.alert("请先选择对方机构!");
					return;
				} 
				
				var ccy = mini.get("currency").getText();
				if(ccy == null || ccy == ""){
					mini.alert("请先选择货币!");
					return;
				}
				
				var url;
				var data;
				if(ccy!="CNY"){//外币
					url="./Ifs/opics/nostMini.jsp";
		        	data = { ccy:ccy ,cust:cpInstId};
		        }else if(ccy=="CNY"){//人民币
		        	data = { ccy:ccy ,cust:cpInstId};
		        	url="./Ifs/opics/setaMini.jsp";
		        }
				
				var btnEdit = this;
		       mini.open({
		           url: url,
		           title: "选择清算路径",
		           width: 900,
		           height: 600,
		           onload: function () {
		               var iframe = this.getIFrameEl();
		               iframe.contentWindow.SetData(data);
		           },
		           ondestroy: function (action) {
		               if (action == "ok") {
		                   var iframe = this.getIFrameEl();
		                   var data1 = iframe.contentWindow.GetData();
		                   data1 = mini.clone(data1);    //必须
		                   if (data1) {
		                	   if(ccy!="CNY"){//外币
		                    		mini.get("ctrsmeans").setValue($.trim(data1.smeans));
		                    		mini.get("ctrsmeans").setText($.trim(data1.smeans));
		                            mini.get("ctrsacct").setValue($.trim(data1.nos));
		                        }else if(ccy=="CNY"){//人民币
		                        	mini.get("ctrsmeans").setValue($.trim(data1.smeans));
			                    	 mini.get("ctrsmeans").setText($.trim(data1.smeans));
			                         mini.get("ctrsacct").setValue($.trim(data1.sacct)); 
		                        }
		                       btnEdit.focus();
		                   }
		               }

		           }
		       });
			}
			function dirVerify(e){
				var direction = mini.get("direction").getValue();
				if(direction == 'P'){//拆入
					mini.get("applyNo").setEnabled(false);//信用风险审查单号：
					mini.get("applyProd").setEnabled(false);//产品名称
					mini.get("applyNo").setValue("");//信用风险审查单号：
					mini.get("applyNo").setText("");//信用风险审查单号：
					mini.get("applyProd").setValue("");//产品名称
				}else if(direction == 'S'){//拆出
					mini.get("applyNo").setEnabled(true);//信用风险审查单号：
					mini.get("applyProd").setEnabled(true);//产品名称
				}
			}
</script>
			<script type="text/javascript" src="../../Common/Flow/MiniApproveOpCommon.js"></script>			
</body>
</html>