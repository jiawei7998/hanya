<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
  <script type="text/javascript" src="./rmbVerify.js"></script>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>质押式回购维护</title>
  </head>

<body style="width:100%;height:100%;background:white">
<div class="mini-splitter" style="width:100%;height:100%;">
	<div size="90%" showCollapseButton="false">
		<h1 style="text-align:center"><strong>质押式回购</strong></h1>
		<div  id="field_form" class="mini-fit area"  style="background:white" >
		<input id="sponsor" name="sponsor" class="mini-hidden" />
		<input id="sponInst" name="sponInst" class="mini-hidden" />
		<input id="aDate" name="aDate" class="mini-hidden"/>
		<input id="postDate" name="postDate" class="mini-datepicker"  labelField="true"  width="30%" style="font-size:18px;float:left;margin-left: 100px;"  label="报送日期："  required="true"  labelStyle="text-align:left;" />
		<input id="ticketId" name="ticketId" class="mini-textbox"  labelField="true" width="30%" labelField="true" style="font-size:18px; margin-left: 25%;" enabled="false" label="审批单号：" labelStyle="text-align:left;width:120px" emptyText="由系统自动生成"/> 
		<div class="centerarea">
			<fieldset>
				<legend>交易明细</legend>
				<input id="direction" name="direction" class="mini-combobox"  labelField="true" label="交易方向：" labelStyle="text-align:left;width:120px;" data = "CommonUtil.serverData.dictionary.trading" />
				<input id="counterpartyInstId"  name="counterpartyInstId" label="交易对手 ：" onbuttonclick="onButtonEdit" class="mini-buttonedit" labelField="true" labelStyle="text-align:left;width:120px;"/>
				<input id="bondCode" name="bondCode" class="mini-textbox" labelField="true"  label="债券代码："  required="true"     vtype="maxLength:20" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])"  labelStyle="text-align:left;width:120px;" />
				<input id="bondName" name="bondName" class="mini-textbox" labelField="true"  label="债券名称："  required="true"     labelStyle="text-align:left;width:120px;"   />
				<input id="totalFaceValue" name="totalFaceValue" class="mini-spinner"   changeOnMousewheel='false'  required="true"  labelField="true"  label="券面总额(万元)：" maxValue="99999999999999999.9999" format="n4"  labelStyle="text-align:left;width:120px;"   />
				<input id="underlyingQty" name="underlyingQty" class="mini-spinner"   changeOnMousewheel='false' labelField="true"  required="true"   label="券面总额合计(万元)："  maxValue="99999999999999999.9999" format="n4"  labelStyle="text-align:left;width:120px;"   />
				<input id="underlyingStipType" name="underlyingStipType" class="mini-spinner"   changeOnMousewheel='false' required="true"   labelField="true"  label="折算比例(%)：" minValue="0" onvaluechanged="linkageCalculatAmount" maxValue="99999999999999999999.99999999" format="n8" changeOnMousewheel="false" labelStyle="text-align:left;width:120px;"   />
				<input id="tradeAmount" name="tradeAmount" class="mini-spinner"   changeOnMousewheel='false' labelField="true"  required="true"   label="交易金额(元)：" maxValue="99999999999999999.9999" format="n4"   labelStyle="text-align:left;width:120px;"   />
				<input id="tradingProduct" name="tradingProduct" class="mini-textbox" labelField="true"  label="交易品种："  required="true"  vtype="maxLength:10"  labelStyle="text-align:left;width:120px;"   />
				<input id="repoRate" name="repoRate" class="mini-spinner"   changeOnMousewheel='false' labelField="true"  required="true"   label="回购利率(%)：" minValue="0" maxValue="99999999999999999999.99999999" format="n8" onvaluechanged="linkageCalculatRate" changeOnMousewheel="false" required="true"  labelStyle="text-align:left;width:120px;" />
				<input id="firstSettlementMethod" name="firstSettlementMethod" class="mini-textbox" labelField="true"  required="true"   label="首次结算方式：" vtype="maxLength:50"   labelStyle="text-align:left;width:120px;"   />
				<input id="secondSettlementMethod" name="secondSettlementMethod" class="mini-textbox" labelField="true"  required="true"   label="到期结算方式：" vtype="maxLength:50"  labelStyle="text-align:left;width:120px;"   />
				<input id="firstSettlementDate" name="firstSettlementDate" class="mini-datepicker" labelField="true"  required="true"   label="首次结算日：" onvaluechanged="linkageCalculatDay" ondrawdate="onDrawDateFirst" labelStyle="text-align:left;width:120px;"   />
				<input id="secondSettlementDate" name="secondSettlementDate" class="mini-datepicker" labelField="true"  required="true"   onvaluechanged="linkageCalculatDay" label="到期结算日：" ondrawdate="onDrawDateSecond" labelStyle="text-align:left;width:120px;"   />
				<input id="tenor" name="tenor" class="mini-spinner"   changeOnMousewheel='false' labelField="true"  required="true"   label="回购期限(天)：" maxValue="99999"  labelStyle="text-align:left;width:120px;"   />
				<input id="occupancyDays" name="occupancyDays" class="mini-spinner"   changeOnMousewheel='false' labelField="true"  required="true"   label="实际占款天数(天)：" maxValue="99999"  onvaluechanged="linkageCalculat" labelStyle="text-align:left;width:120px;"   />
				<input id="accuredInterest" name="accuredInterest" class="mini-spinner"   changeOnMousewheel='false' labelField="true"  required="true"   label="应计利息(元)：" maxValue="99999999999999999.9999" format="n4"   labelStyle="text-align:left;width:120px;"   />
				<input id="secondSettlementAmount" name="secondSettlementAmount" class="mini-spinner"   changeOnMousewheel='false' required="true"   labelField="true" maxValue="99999999999999999.9999" format="n4"   label="到期结算金额(元)："  labelStyle="text-align:left;width:120px;"   />
			</fieldset>
		</div>
		<%@ include file="../../Common/opicsApprove.jsp"%>
		<%@ include file="../../Common/Flow/MiniApproveOpCommon.jsp"%>
		</div>
	</div>
	<div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
		<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="save_btn"   onclick="save">保存交易</a></div>
		<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="close_btn"   onclick="close">关闭界面</a></div>
	</div>
</div>	
<script type="text/javascript">
mini.parse();
	//获取当前tab
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var row=params.selectData;
	var url=window.location.search;
	var action=CommonUtil.getParam(url,"action");
	var form=new mini.Form("#field_form");

	var tradeData={};
	
	tradeData.selectData=row;
	tradeData.operType=action;
	tradeData.serial_no=row.ticketId;
	tradeData.task_id=row.taskId;
	//加载机构树
	function nodeclick(e) {
		var oldvalue=e.sender.value;
		var param = mini.encode({"branchId":branchId}); //序列化成JSON
		var oldData=e.sender.data;
		if(oldData==null||oldData==""){
			CommonUtil.ajax({
				url: "/InstitutionController/searchInstitutionByBranchId",
				data: param,
				callback: function (data) {
					var obj=e.sender;
					obj.setData(data.obj);
					obj.setValue(oldvalue);
				}
			});
		}
	}

	//保存
	function save(){
		//表单验证！！！
		form.validate();
		if (form.isValid() == false) {
			return;
		}
		var data=form.getData(true);
		if(toDecimal(data['tradeAmount'])!=toDecimal(data['underlyingStipType']*data['underlyingQty']*10000/100)||data['tenor']!=data['occupancyDays']||toDecimal(data['accuredInterest'])!=toDecimal(data['tradeAmount']*data['repoRate']*data['occupancyDays']/100/365)
				||toDecimal(data['secondSettlementAmount'])!=parseFloat(toDecimal((data['tradeAmount'])))+parseFloat(toDecimal((data['accuredInterest'])))!="0.00"){
			mini.confirm("确认以当前数据为准吗？","确认",function (action) {
				if (action != "ok") {
					return;
				}
				data['sponInst']="<%=__sessionInstitution.getInstId()%>";
				data['dealTransType']="0";
				var params=mini.encode(data);
				CommonUtil.ajax({
					url:"/IfsApprovermbCrController/saveCr",
					data:params,
					callback:function(data){
						mini.alert("保存成功",'提示信息',function(){
							top["win"].closeMenuTab();
						});
					}
				});
			});
		}else{
			data['sponInst']="<%=__sessionInstitution.getInstId()%>";
			data['dealTransType']="0";
			var params=mini.encode(data);
			CommonUtil.ajax({
				url:"/IfsApprovermbCrController/saveCr",
				data:params,
				callback:function(data){
					mini.alert("保存成功",'提示信息',function(){
						top["win"].closeMenuTab();
					});
				}
			});
		}
	}
	
	//文、数字、下划线 的验证
	function onEnglishAndNumberValidations(e) {
		if(e.value == "" || e.value == null){//值为空，就不做校验
			return;
		}
		if (e.isValid) {
			if (isEnglishAndNumbers(e.value) == false) {
				e.errorText = "必须输入英文小写+数字";
				e.isValid = false;
			}
		}
	}
	/* 是否英文+数字 */
	function isEnglishAndNumbers(v) {
		var re = new RegExp("^[0-9a-z\_\-]+$");
		if (re.test(v)) return true;
		return false;
	}	
	
	//英文、数字、下划线 的验证
	function onEnglishAndNumberValidation(e) {
		if(e.value == "" || e.value == null){//值为空，就不做校验
			return;
		}
		if (e.isValid) {
			if (isEnglishAndNumber(e.value) == false) {
				e.errorText = "必须输入英文+数字";
				e.isValid = false;
			}
		}
	}
	/* 是否英文+数字 */
	function isEnglishAndNumber(v) {
		var re = new RegExp("^[0-9a-zA-Z\_\-]+$");
		if (re.test(v)) return true;
		return false;
	}
	function close(){
		top["win"].closeMenuTab();
	}
	$(document).ready(function(){
		if($.inArray(action,["approve","detail"])>-1){
			mini.get("save_btn").setVisible(false);
			form.setEnabled(false);
			var approForm=new mini.Form("#approve_operate_form");
			approForm.setEnabled(true);
			mini.get("postDate").setEnabled(false);
		}
		if($.inArray(action,["edit","approve","detail"])>-1){//修改、审批、详情
			form.setData(row);
			mini.get("postDate").setValue(row.postDate);
			mini.get("ticketId").setValue(row.ticketId);
			//投资组合
			mini.get("port").setValue(row.port);
			mini.get("port").setText(row.port);
			//对方机构counterpartyInstId
			mini.get("counterpartyInstId").setValue(row.cno);
			mini.get("counterpartyInstId").setText(row.counterpartyInstId);
		}else{//增加 
			mini.get("sponsor").setValue("<%=__sessionUser.getUserId()%>");
			mini.get("sponInst").setValue("<%=__sessionUser.getInstId() %>");
			mini.get("postDate").setValue(new Date());
		}
	});
	function onButtonEdit(e) {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Common/CustMini.jsp",
            title: "选择对手方列表",
            width: 900,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.cno);
                        btnEdit.setText(data.cfn);
                        btnEdit.focus();
                    }
                }

            }
        });
    }
	function onDrawDateFirst(e) {
        var firstDate = e.date;
        var secondDate= mini.get("secondSettlementDate").getValue();
        if(CommonUtil.isNull(secondDate)){
        	return;
        }
        if (secondDate.getTime() < firstDate.getTime()) {
            e.allowSelect = false;
        }
    }
	function onDrawDateSecond(e) {
        var secondDate = e.date;
        var firstDate = mini.get("firstSettlementDate").getValue();
        if(CommonUtil.isNull(firstDate)){
        	return;
        }
        if (secondDate.getTime() < firstDate.getTime()) {
            e.allowSelect = false;
        }
    }
	function linkageCalculatDay(e){
		var firstDate = mini.get("firstSettlementDate").getValue();
		var secondDate= mini.get("secondSettlementDate").getValue();
		if(CommonUtil.isNull(firstDate)||CommonUtil.isNull(secondDate)){
			return;
		}
		var subDate= Math.abs(parseInt((secondDate.getTime() - firstDate.getTime())/1000/3600/24));
		mini.get("tenor").setValue(subDate);
		mini.get("occupancyDays").setValue(subDate);
		var occupancyDays=mini.get("occupancyDays").getValue();
		var rate=mini.get("repoRate").getValue();
		var tradeAmount=mini.get("tradeAmount").getValue();
		mini.get("accuredInterest").setValue(tradeAmount*rate*occupancyDays/100/365);
		mini.get("secondSettlementAmount").setValue(tradeAmount+tradeAmount*rate*occupancyDays/100/365);
	}
    function linkageCalculatAmount(e){
		var underlyingStip=e.value;
		var qty=mini.get("underlyingQty").getValue();
		mini.get("tradeAmount").setValue(underlyingStip*qty*10000/100);
	}
   function linkageCalculat(e){
		var occupancyDays=e.value;
		var rate=mini.get("repoRate").getValue();
		var tradeAmount=mini.get("tradeAmount").getValue();
		mini.get("accuredInterest").setValue(tradeAmount*rate*occupancyDays/100/365);
		mini.get("secondSettlementAmount").setValue(tradeAmount+tradeAmount*rate*occupancyDays/100/365);
	} 
   function linkageCalculatRate(e){
	    var rate=e.value;
		var occupancyDays=mini.get("occupancyDays").getValue();
		var tradeAmount=mini.get("tradeAmount").getValue();
		mini.get("accuredInterest").setValue(tradeAmount*rate*occupancyDays/100/365);
		mini.get("secondSettlementAmount").setValue(tradeAmount+tradeAmount*rate*occupancyDays/100/365);
   }
</script>
<script type="text/javascript" src="../../Common/Flow/MiniApproveOpCommon.js"></script>			
</body>
</html>
