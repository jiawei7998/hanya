<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>现券买卖  维护</title>
  </head>

<body style="width:100%;height:100%;background:white">
<div class="mini-splitter" style="width:100%;height:100%;">
		<div size="90%" showCollapseButton="false">
		<h1 style="text-align:center"><strong>现券买卖</strong></h1>
		<div id="field_form" class="mini-fit area"  style="background:white">
		<input id="sponsor" name="sponsor" class="mini-hidden" />
		<input id="sponInst" name="sponInst" class="mini-hidden" />
		<input id="aDate" name="aDate" class="mini-hidden"/>
		<input id="postDate" name="postDate" class="mini-datepicker"  labelField="true"  width="30%" style="font-size:18px;float:left;margin-left: 100px;"  label="报送日期："  required="true"  labelStyle="text-align:left;" />
		<input id="ticketId" name="ticketId" class="mini-textbox"  labelField="true" width="30%" labelField="true" style="font-size:18px; margin-left: 25%;" enabled="false" label="审批单号：" labelStyle="text-align:left;width:120px" emptyText="由系统自动生成"/> 
			<div class="centerarea">
				<fieldset>
					<legend>审批交易明细</legend>
					<input id="direction" name="direction" class="mini-combobox"  labelField="true" label="交易方向：" labelStyle="text-align:left;width:120px;" data = "CommonUtil.serverData.dictionary.trading" />
					<input id="counterpartyInstId"  name="counterpartyInstId" label="交易对手 ：" onbuttonclick="onButtonEdit" class="mini-buttonedit" labelField="true" labelStyle="text-align:left;width:120px;"/>
					<input id="bondCode" name="bondCode" class="mini-textbox" labelField="true"  label="债券代码：" required="true"  onvalidation="onEnglishAndNumberValidation" vtype="maxLength:20"   labelStyle="text-align:left;width:120px;" />
					<input id="bondName" name="bondName" class="mini-textbox" labelField="true"  label="债券名称：" required="true"   labelStyle="text-align:left;width:120px;"   />
					<input id="cleanPrice" name="cleanPrice" class="mini-spinner" labelField="true"  label="净价(元)：" required="true"  onvaluechanged="onAccuredInterestChanged" minValue="0" maxValue="99999999999999999.9999" format="n4" changeOnMousewheel="false"  labelStyle="text-align:left;width:120px;"   />
					<input id="totalFaceValue" name="totalFaceValue" class="mini-spinner" labelField="true"  label="券面总额(万元)："  required="true"  onvaluechanged="onAccuredInterestChanged"   minValue="0" maxValue="99999999999999999.9999" format="n4"  changeOnMousewheel="false" labelStyle="text-align:left;width:120px;"   />
					<input id="yield" name="yield" class="mini-spinner" labelField="true"  label="到期收益率(%)：" required="true"   minValue="0" maxValue="99999999999999999999.99999999" format="n8" changeOnMousewheel="false"  labelStyle="text-align:left;width:120px;"   />
					<input id="tradeAmount" name="tradeAmount" class="mini-spinner" labelField="true"   label="交易金额(元)："   required="true"  onvaluechanged="calSettleAmount"  minValue="0" maxValue="99999999999999999.9999" format="n4" changeOnMousewheel="false" labelStyle="text-align:left;width:120px;"   />
					<input id="accuredInterest" name="accuredInterest" class="mini-spinner" labelField="true"  label="应计利息(元)：" required="true"  onvaluechanged="onAccuredInterestChanged" minValue="0" maxValue="99999999999999999.9999" format="n4" changeOnMousewheel="false" labelStyle="text-align:left;width:120px;"   />
					<input id="totalAccuredInterest" name="totalAccuredInterest" class="mini-spinner" labelField="true"  label="应计利息总额(元)："  required="true"  onvaluechanged="calSettleAmount" minValue="0" maxValue="99999999999999999.9999" format="n4" changeOnMousewheel="false" labelStyle="text-align:left;width:120px;"   />
					<input id="dirtyPrice" name="dirtyPrice" class="mini-spinner" labelField="true"  label="全价(元)：" required="true"  minValue="0" maxValue="99999999999999999.9999" format="n4" changeOnMousewheel="false" labelStyle="text-align:left;width:120px;"   />
					<input id="settlementAmount" name="settlementAmount" class="mini-spinner" labelField="true"  label="结算金额(元)：" required="true"   minValue="0" maxValue="99999999999999999.9999" format="n4" changeOnMousewheel="false" labelStyle="text-align:left;width:120px;"   />
					<input id="settlementMethod" name="settlementMethod" class="mini-combobox" labelField="true"  label="结算方式："  required="true"  data = "CommonUtil.serverData.dictionary.SettlementMethod"  labelStyle="text-align:left;width:120px;"   />
					<input id="settlementDate" name="settlementDate" class="mini-datepicker" labelField="true"  label="结算日：" required="true"   labelStyle="text-align:left;width:120px;"   />
				</fieldset>
			</div>
			<%@ include file="../../Common/opicsApprove.jsp"%>
			<%@ include file="../../Common/Flow/MiniApproveOpCommon.jsp"%>
			</div>
			
		</div>
		<div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
			<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="save_btn"   onclick="save">保存交易</a></div>
			<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="close_btn"   onclick="close">关闭界面</a></div>
		</div>
	</div>		
<script type="text/javascript">
	mini.parse();
	//获取当前tab
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var row=params.selectData;
	var url=window.location.search;
	var action=CommonUtil.getParam(url,"action");
	var form=new mini.Form("#field_form");

	var tradeData={};
	
	tradeData.selectData=row;
	tradeData.operType=action;
	tradeData.serial_no=row.ticketId;
	tradeData.task_id=row.taskId;
	//保存
	function save(){

		//表单验证！！！
		form.validate();
		if (form.isValid() == false) {
			return;
		}
		var data=form.getData(true);
		//对数据做最后的确认
		if(data['dirtyPrice'] != data['cleanPrice'] + data['accuredInterest'] || 
		   data['settlementAmount'] != data['tradeAmount'] + data['totalAccuredInterest'] || 
		   data['tradeAmount'] != data['totalFaceValue'] * data['cleanPrice'] * 100 ||
		   data['totalAccuredInterest'] != data['accuredInterest'] * data['totalFaceValue'] * 100){
				mini.confirm("确认以手输数据为准吗？","确认",function (action) {
					if (action != "ok") {
						return;
					}
					data['sponInst']="<%=__sessionInstitution.getInstId()%>";
					data['dealTransType']="0";
					var params=mini.encode(data);
					CommonUtil.ajax({
						url:"/IfsApprovermbCbtController/saveCbt",
						data:params,
						callback:function(data){
							mini.alert("保存成功",'提示信息',function(){
								top["win"].closeMenuTab();
						})
						}
					});
					
					
				});
		   }else{
			   data['sponInst']="<%=__sessionInstitution.getInstId()%>";
			   data['dealTransType']="0";
				var params=mini.encode(data);
				CommonUtil.ajax({
					url:"/IfsApprovermbCbtController/saveCbt",
					data:params,
					callback:function(data){
						mini.alert("保存成功",'提示信息',function(){
							top["win"].closeMenuTab();
					})
					}
				});

		   }
		
	}
	function close(){
		top["win"].closeMenuTab();
	}
	$(document).ready(function(){
		if($.inArray(action,["approve","detail"])>-1){
			mini.get("save_btn").setVisible(false);
			form.setEnabled(false);
			var form11=new mini.Form("#approve_operate_form");
			form11.setEnabled(true);
			mini.get("postDate").setEnabled(false);
		}
		if($.inArray(action,["edit","approve","detail"])>-1){
			form.setData(row);
			mini.get("postDate").setValue(row.postDate);
			mini.get("ticketId").setValue(row.ticketId);
			//投资组合
			mini.get("port").setValue(row.port);
			mini.get("port").setText(row.port);
			//对方机构counterpartyInstId
			mini.get("counterpartyInstId").setValue(row.cno);
			mini.get("counterpartyInstId").setText(row.counterpartyInstId);

		}else{
			mini.get("sponsor").setValue("<%=__sessionUser.getUserId()%>");
			mini.get("sponInst").setValue("<%=__sessionUser.getInstId() %>");
			mini.get("postDate").setValue(new Date());
		}
	});
	//加载机构树
	function nodeclick(e) {
		
		var oldvalue=e.sender.value;
		var param = mini.encode({"branchId":branchId}); //序列化成JSON
		CommonUtil.ajax({
		  url: "/InstitutionController/searchInstitutionByBranchId",
		  data: param,
		  callback: function (data) {
			var obj=e.sender;
			obj.setData(data.obj);
			obj.setValue(oldvalue);
			
		  }
		});
	}
	
	//英文、数字、下划线 的验证
	function onEnglishAndNumberValidation(e) {
		if(e.value == "" || e.value == null){//值为空，就不做校验
			return;
		}
		if (e.isValid) {
			if (isEnglishAndNumber(e.value) == false) {
				e.errorText = "必须输入英文+数字";
				e.isValid = false;
			}
		}
	}
	/* 是否英文+数字 */
	function isEnglishAndNumber(v) {
		var re = new RegExp("^[0-9a-zA-Z\_\-]+$");
		if (re.test(v)) return true;
		return false;
	}

	function onButtonEdit(e) {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Common/CustMini.jsp",
            title: "选择对手方列表",
            width: 900,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.cno);
                        btnEdit.setText(data.cfn);
                        btnEdit.focus();
                    }
                }

            }
        });

	}
	//应计利息、净价  改变时发生  ->  全价=净价+应计利息
	function onAccuredInterestChanged(e){
		/*               全价=净价+应计利息              */
		var accuredInterest = mini.get("accuredInterest").getValue();//应计利息
		var cleanPrice = mini.get("cleanPrice").getValue();//净价
		var sum = CommonUtil.accAdd(accuredInterest,cleanPrice);
		mini.get("dirtyPrice").setValue(sum);//全价

		/*             应计利息总额=(应计利息x券面总额【元】)/100                  */
		var totalFaceValue = mini.get("totalFaceValue").getValue();//券面总额
		var mul = CommonUtil.accMul(accuredInterest,totalFaceValue);//乘积
		var result = CommonUtil.accMul(mul,100);
		mini.get("totalAccuredInterest").setValue(result);//应计利息总额

		/*             交易金额=(券面总额【元】x净价)/100                  */
		var mul = CommonUtil.accMul(totalFaceValue,cleanPrice);//乘积
		var result = CommonUtil.accMul(mul,100);
		mini.get("tradeAmount").setValue(result);//交易金额

		
	}
	//券面总额  改变时发生  ->  交易金额=(券面总额x净价)/100
	/* function onTotalFaceValueChanged(){
		var totalFaceValue = mini.get("totalFaceValue").getValue();//券面总额
		var cleanPrice = mini.get("cleanPrice").getValue();//净价
		var mul = CommonUtil.accMul(totalFaceValue,cleanPrice);//乘积
		var result = CommonUtil.accDiv(mul,100);
		mini.get("tradeAmount").setValue(result);//交易金额
	} */

	//交易金额、应计利息总额  改变时发生  ->  结算金额=交易金额+应计利息总额 
	function calSettleAmount(){
		
		var tradeAmount = mini.get("tradeAmount").getValue();//交易金额
		var totalIst = mini.get("totalAccuredInterest").getValue();//应计利息总额
		var sum = CommonUtil.accAdd(tradeAmount,totalIst);
		mini.get("settlementAmount").setValue(sum);//结算金额
	}
</script>
<script type="text/javascript" src="../../Common/Flow/MiniApproveOpCommon.js"></script>			
</body>
</html>
