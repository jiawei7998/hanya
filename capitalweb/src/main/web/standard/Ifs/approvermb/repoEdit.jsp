<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>买断式回购</title>
  </head>
<body style="width:100%;height:100%;background:white">
<div class="mini-splitter" style="width:100%;height:100%;">
		<div size="90%" showCollapseButton="false">
			<h1 style="text-align:center"><strong>买断式回购</strong></h1>
			<div id="field_form" class="mini-fit area"  style="background:white">
			<input id="sponsor" name="sponsor" class="mini-hidden" />
			<input id="sponInst" name="sponInst" class="mini-hidden" />
			<input id="aDate" name="aDate" class="mini-hidden"/>
			<input id="postDate" name="postDate" class="mini-datepicker"  labelField="true"  width="30%" style="font-size:18px;float:left;margin-left: 100px;"  label="报送日期："  required="true"  labelStyle="text-align:left;" />
			<input id="ticketId" name="ticketId" class="mini-textbox"  labelField="true" width="30%" labelField="true" style="font-size:18px; margin-left: 25%;" enabled="false" label="审批单号：" labelStyle="text-align:left;width:120px" emptyText="由系统自动生成"/> 		
			<div class="centerarea">
				<fieldset>
					<legend>交易明细</legend>
					<input id="direction" name="direction" class="mini-combobox"  labelField="true" label="交易方向：" labelStyle="text-align:left;width:120px;" data = "CommonUtil.serverData.dictionary.trading" />
					<input id="counterpartyInstId"  name="counterpartyInstId" label="交易对手 ：" onbuttonclick="onButtonEdit" class="mini-buttonedit" labelField="true" labelStyle="text-align:left;width:120px;"/>
					<input id="bondCode" name="bondCode" class="mini-textbox" labelField="true"  label="债券代码：" required="true"   onvalidation="onEnglishAndNumberValidation" vtype="maxLength:10"   labelStyle="text-align:left;width:120px;" />
					<input id="bondName" name="bondName" class="mini-textbox" labelField="true"  label="债券名称：" required="true"   labelStyle="text-align:left;width:120px;"   />
					<input id="tradingProduct" name="tradingProduct" class="mini-textbox" labelField="true"  label="交易品种：" required="true"  onvalidation="onEnglishAndNumberValidation" vtype="maxLength:10"   labelStyle="text-align:left;width:120px;"   />
					<input id="firstSettlementDate" name="firstSettlementDate" class="mini-datepicker" labelField="true"   label="首次结算日："  required="true"  ondrawdate="onDrawDateStart" onvaluechanged="onStartDateChanged" labelStyle="text-align:left;width:120px;"   />
					<input id="secondSettlementDate" name="secondSettlementDate" class="mini-datepicker"   labelField="true"  label="到期结算日：" required="true"   ondrawdate="onDrawDateEnd" onvaluechanged="onEndDateChanged" labelStyle="text-align:left;width:120px;"   />
					<input id="occupancyDays" name="occupancyDays" class="mini-spinner" labelField="true"   label="实际占款天数(天)：" required="true"  minValue="0" maxValue="99999"  changeOnMousewheel="false"  labelStyle="text-align:left;width:120px;"   />
					<input id="tenor" name="tenor" class="mini-spinner" labelField="true"   label="回购期限(天)：" required="true"  minValue="0" maxValue="9999999999"  changeOnMousewheel="false"  labelStyle="text-align:left;width:120px;"   />
					<input id="repoRate" name="repoRate" class="mini-spinner" labelField="true"  label="回购利率(%)：" required="true"  minValue="0" maxValue="99999999999999999999.99999999" format="n8" changeOnMousewheel="false"    labelStyle="text-align:left;width:120px;" />
					<input id="totalFaceValue" name="totalFaceValue" class="mini-spinner" labelField="true"   label="券面总额(万元)：" required="true"  onvaluechanged="onFirstPriceChanged"  minValue="0" maxValue="99999999999999999.9999" format="n4" changeOnMousewheel="false" labelStyle="text-align:left;width:120px;"   />
					<input id="firstCleanPrice" name="firstCleanPrice" class="mini-spinner" labelField="true"  label="首次净价(元)：" required="true"  onvaluechanged="onFirstInterestChanged" minValue="0" maxValue="99999999999999999.9999" format="n4" changeOnMousewheel="false"   labelStyle="text-align:left;width:120px;"   />
					<input id="firstYield" name="firstYield" class="mini-spinner" labelField="true"   label="首次收益率(%)：" required="true"   onvaluechanged="onFirstYieldChanged" minValue="0" maxValue="99999999999999999999.99999999" format="n8" changeOnMousewheel="false" labelStyle="text-align:left;width:120px;"   />
					<input id="firstAccuredInterest" name="firstAccuredInterest" class="mini-spinner" labelField="true"   label="首次应计利息(元)：" required="true"  onvaluechanged="onFirstInterestChanged"  minValue="0" maxValue="99999999999999999.9999" format="n4" changeOnMousewheel="false"  labelStyle="text-align:left;width:120px;"   />
					<input id="secondCleanPrice" name="secondCleanPrice" class="mini-spinner" labelField="true"   label="到期净价(元)：" required="true"  onvaluechanged="onSecondInterestChanged"  minValue="0" maxValue="99999999999999999.9999" format="n4" changeOnMousewheel="false" labelStyle="text-align:left;width:120px;"   />
					<input id="secondYield" name="secondYield" class="mini-spinner" labelField="true"  label="到期收益率(%)：" required="true"  minValue="0" maxValue="99999999999999999999.99999999" format="n8" changeOnMousewheel="false"    labelStyle="text-align:left;width:120px;"   />
					<input id="secondAccuredInterest" name="secondAccuredInterest" class="mini-spinner"  labelField="true"  label="到期应计利息(元)：" required="true"  onvaluechanged="onSecondInterestChanged" minValue="0" maxValue="99999999999999999.9999" format="n4" changeOnMousewheel="false" labelStyle="text-align:left;width:120px;"   />
					<input id="firstDirtyPrice" name="firstDirtyPrice" class="mini-spinner" labelField="true"  label="首次全价(元)：" required="true"   onvaluechanged="onFirstPriceChanged" minValue="0" maxValue="99999999999999999.9999" format="n4" changeOnMousewheel="false"    labelStyle="text-align:left;width:120px;"   />
					<input id="firstSettlementAmount" name="firstSettlementAmount" class="mini-spinner"   labelField="true"  label="首次结算金额(元)："  required="true"  minValue="0" maxValue="99999999999999999.9999" format="n4" changeOnMousewheel="false" labelStyle="text-align:left;width:120px;"   />
					<input id="firstSettlementMethod" name="firstSettlementMethod" class="mini-combobox" labelField="true"   label="首次结算方式：" required="true"  data = "CommonUtil.serverData.dictionary.SettlementMethod" labelStyle="text-align:left;width:120px;"   />
					<input id="secondDirtyPrice" name="secondDirtyPrice" class="mini-spinner" labelField="true"   label="到期全价(元)：" required="true"  onvaluechanged="onFirstPriceChanged" minValue="0" maxValue="99999999999999999.9999" format="n4" changeOnMousewheel="false"  labelStyle="text-align:left;width:120px;"   />
					<input id="secondSettlementAmount" name="secondSettlementAmount" class="mini-spinner" labelField="true"   label="到期结算金额(元)：" required="true"  minValue="0" maxValue="99999999999999999.9999" format="n4" changeOnMousewheel="false"  labelStyle="text-align:left;width:120px;"   />
					<input id="secondSettlementMethod" name="secondSettlementMethod" class="mini-combobox"  labelField="true"  label="到期结算方式："  required="true"  data = "CommonUtil.serverData.dictionary.SettlementMethod" labelStyle="text-align:left;width:120px;"   />
				</fieldset>
			</div>
			<%@ include file="../../Common/opicsApprove.jsp"%>						
			<%@ include file="../../Common/Flow/MiniApproveOpCommon.jsp"%>
			</div>
			
		</div>
		<div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
			<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="save_btn"   onclick="save">保存交易</a></div>
			<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="close_btn"   onclick="close">关闭界面</a></div>
		</div>
</div>
<script type="text/javascript">
	mini.parse();
	//获取当前tab
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var row=params.selectData;
	var url=window.location.search;
	var action=CommonUtil.getParam(url,"action");
	var form=new mini.Form("#field_form");

	var tradeData={};
	
	tradeData.selectData=row;
	tradeData.operType=action;
	tradeData.serial_no=row.ticketId;
	tradeData.task_id=row.taskId;
	//保存
	function save(){
		//表单验证！！！
		form.validate();
		if (form.isValid() == false) {
			return;
		}

		var data=form.getData(true);
		//对数据做最后的确认
		if(data['firstDirtyPrice'] != data['firstAccuredInterest'] + data['firstCleanPrice'] || 
		   data['secondDirtyPrice'] != data['secondAccuredInterest'] + data['secondCleanPrice'] || 
		   data['firstSettlementAmount'] != data['firstDirtyPrice'] * data['totalFaceValue'] * 100 ||
		   data['secondSettlementAmount'] != data['secondDirtyPrice'] * data['totalFaceValue'] * 100){
			mini.confirm("确认以手输数据为准吗？","确认",function (action) {
				if (action != "ok") {
					return;
				}
				data['sponInst']="<%=__sessionInstitution.getInstId()%>";
				data['dealTransType']="0";
				var params=mini.encode(data);
				CommonUtil.ajax({
					url:"/IfsApprovermbOrController/saveOr",
					data:params,
					callback:function(data){
						mini.alert("保存成功",'提示信息',function(){
							top["win"].closeMenuTab();
						});
					}
				});
				
			});
		}else{
			data['sponInst']="<%=__sessionInstitution.getInstId()%>";
			data['dealTransType']="0";
			var params=mini.encode(data);
			CommonUtil.ajax({
				url:"/IfsApprovermbOrController/saveOr",
				data:params,
				callback:function(data){
					mini.alert("保存成功",'提示信息',function(){
						top["win"].closeMenuTab();
					});
				}
			});

		}
 		
	}
	function close(){
		top["win"].closeMenuTab();
	}

	$(document).ready(function(){
		if($.inArray(action,["approve","detail"])>-1){
			mini.get("save_btn").setVisible(false);
			form.setEnabled(false);
			var form11=new mini.Form("#approve_operate_form");
			form11.setEnabled(true);
			mini.get("postDate").setEnabled(false);
		}

		if($.inArray(action,["edit","approve","detail"])>-1){//修改、审批、详情
			form.setData(row);
			mini.get("postDate").setValue(row.postDate);
			mini.get("ticketId").setValue(row.ticketId);
			//投资组合
			mini.get("port").setValue(row.port);
			mini.get("port").setText(row.port);
			//对方机构counterpartyInstId
			mini.get("counterpartyInstId").setValue(row.cno);
			mini.get("counterpartyInstId").setText(row.counterpartyInstId);
		}else{//增加 
			mini.get("sponsor").setValue("<%=__sessionUser.getUserId()%>");
			mini.get("sponInst").setValue("<%=__sessionUser.getInstId() %>");
			mini.get("postDate").setValue(new Date());
		}
	});

	
	//加载机构树
	function nodeclick(e) {
		
		var oldvalue=e.sender.value;
		var param = mini.encode({"branchId":branchId}); //序列化成JSON
		var oldData=e.sender.data;
		if(oldData == null || oldData == ""){
			CommonUtil.ajax({
				url: "/InstitutionController/searchInstitutionByBranchId",
				data: param,
				callback: function (data) {
					var obj=e.sender;
					obj.setData(data.obj);
					obj.setValue(oldvalue);
					
				}
			});
		}
		
	}
	
	//文、数字、下划线 的验证
	function onEnglishAndNumberValidations(e) {
		if(e.value == "" || e.value == null){//值为空，就不做校验
			return;
		}
		if (e.isValid) {
			if (isEnglishAndNumbers(e.value) == false) {
				e.errorText = "必须输入英文小写+数字";
				e.isValid = false;
			}
		}
	}
	/* 是否英文+数字 */
	function isEnglishAndNumbers(v) {
		var re = new RegExp("^[0-9a-z\_\-]+$");
		if (re.test(v)) return true;
		return false;
	}	
	
	
	//英文、数字、下划线 的验证
	function onEnglishAndNumberValidation(e) {
		if(e.value == "" || e.value == null){//值为空，就不做校验
			return;
		}
		if (e.isValid) {
			if (isEnglishAndNumber(e.value) == false) {
				e.errorText = "必须输入英文+数字";
				e.isValid = false;
			}
		}
	}
	/* 是否英文+数字 */
	function isEnglishAndNumber(v) {
		var re = new RegExp("^[0-9a-zA-Z\_\-]+$");
		if (re.test(v)) return true;
		return false;
	}

	function onButtonEdit(e) {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Common/CustMini.jsp",
            title: "选择对手方列表",
            width: 900,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.cno);
                        btnEdit.setText(data.cfn);
                        btnEdit.focus();
                    }
                }

            }
        });

	}
	
	//到期结算日 
	function onDrawDateEnd(e) {
		var date = e.date;
		var d = mini.get("firstSettlementDate").getValue();
		if(d==null || d == ""){
			return;
		}

		if (date.getTime() < d.getTime()) {
			e.allowSelect = false;
		}
	}
    //首次结算日 
	function onDrawDateStart(e){
		var date = e.date;
		var d = mini.get("secondSettlementDate").getValue();
		if(d==null || d == ""){
			return;
		}

		if (date.getTime() > d.getTime()) {
			e.allowSelect = false;
		}
	}
	//首次结算日 值改变时发生
	function onStartDateChanged(e){
		var startDate = mini.get("firstSettlementDate").getFormValue();
		var endDate = mini.get("secondSettlementDate").getFormValue();
		if(endDate == null || endDate == ""){
			return;
		}
		var diffDays = CommonUtil.dateDiff(startDate,endDate);
		mini.get("occupancyDays").setValue(diffDays);
		mini.get("tenor").setValue(diffDays);
	}
	//到期结算日 值改变时发生
	function onEndDateChanged(e){
		var endDate = mini.get("secondSettlementDate").getFormValue();
		var startDate = mini.get("firstSettlementDate").getFormValue();
		if(startDate == null || startDate == ""){
			return;
		}
		var diffDays = CommonUtil.dateDiff(startDate,endDate);
		mini.get("occupancyDays").setValue(diffDays);
		mini.get("tenor").setValue(diffDays);
	}

	//首次收益率 值改变时发生->首次应计利息=首次收益率x期限/365？
	function onFirstYieldChanged(){

	}


	//首次应计利息/首次净价  值改变时发生->首次全价=首次净价+首次应计利息
	function onFirstInterestChanged(){
		var firstIst = mini.get("firstAccuredInterest").getValue();//首次应计利息
		var firstPrice = mini.get("firstCleanPrice").getValue();//首次净价
		var sum = CommonUtil.accAdd(firstPrice,firstIst);//首次净价+首次应计利息
		mini.get("firstDirtyPrice").setValue(sum);//设置首次全价
	}

	

	//到期应计利息/到期净价 值改变时发生->到期全价=到期净价+到期应计利息
	function onSecondInterestChanged(){
		var secondIst = mini.get("secondAccuredInterest").getValue();//到期应计利息
		var secondPrice = mini.get("secondCleanPrice").getValue();//到期净价
		var sum = CommonUtil.accAdd(secondPrice,secondIst);//到期净价+到期应计利息
		mini.get("secondDirtyPrice").setValue(sum);//设置到期全价
	}

	//首次全价/券面总额 值改变时发生->首次结算金额=(首次全价[元]x券面总额)/100
	function onFirstPriceChanged(){
		var firstTotal = mini.get("firstDirtyPrice").getValue();//首次全价
		var secondTotal = mini.get("secondDirtyPrice").getValue();//到期全价
		var faceTotal = mini.get("totalFaceValue").getValue();//券面总额
		var mul = CommonUtil.accMul(firstTotal,faceTotal);//首次全价x券面总额
		var mul2 = CommonUtil.accMul(secondTotal,faceTotal);//到期全价x券面总额
		var result = CommonUtil.accMul(mul,100);
		var result2 = CommonUtil.accMul(mul2,100);
		mini.get("firstSettlementAmount").setValue(result);//设置首次结算金额
		mini.get("secondSettlementAmount").setValue(result2);//设置到期结算金额
	}

	
</script>
<script type="text/javascript" src="../../Common/Flow/MiniApproveOpCommon.js"></script>			
</body>
</html>
