<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>利率互换  维护</title>
  </head>


<body style="width:100%;height:100%;background:white">
	<div class="mini-splitter" style="width:100%;height:100%;">
		<div size="90%" showCollapseButton="false">
			<h1 style="text-align:center"><strong>利率互换</strong></h1>
			<div id="field_form" class="mini-fit area"  style="background:white" >
			<input id="sponsor" name="sponsor" class="mini-hidden" />
			<input id="sponInst" name="sponInst" class="mini-hidden" />
			<input id="aDate" name="aDate" class="mini-hidden"/>
			<input id="postDate" name="postDate" class="mini-datepicker"  labelField="true"  width="30%" style="font-size:18px;float:left;margin-left: 100px;"  label="报送日期："  required="true"  labelStyle="text-align:left;" />
			<input id="ticketId" name="ticketId" class="mini-textbox"  labelField="true" width="30%" labelField="true" style="font-size:18px; margin-left: 25%;" enabled="false" label="审批单号：" labelStyle="text-align:left;width:120px" emptyText="由系统自动生成"/> 
			<div class="centerarea">
			  <fieldset>
				  <legend>交易明细</legend>
				<input id="direction" name="direction" class="mini-combobox"  labelField="true" label="交易方向：" labelStyle="text-align:left;" data = "CommonUtil.serverData.dictionary.trading" />
				<input id="counterpartyInstId"  name="counterpartyInstId" label="交易对手 ：" onbuttonclick="onButtonEdit" class="mini-buttonedit" labelField="true" labelStyle="text-align:left;"/>
				<input id="productName" name="productName" class="mini-textbox" labelField="true"  label="产品名称："  required="true"     labelStyle="text-align:left;" />
				<input id="lastQty" name="lastQty" class="mini-spinner"minValue="0" maxValue="99999999999999999.9999" changeOnMousewheel="false" labelField="true"  label="名义本金："  required="true"  format="n4" labelStyle="text-align:left;"   />
				<input id="startDate" name="startDate" class="mini-datepicker" labelField="true"  label="起息日：" required="true"  ondrawdate="onDrawDateStart" onvaluechanged="onStartDateChanged"  labelStyle="text-align:left;" />
				<input id="endDate" name="endDate" class="mini-datepicker" labelField="true"  label="到期日："  required="true"  ondrawdate="onDrawDateEnd"  labelStyle="text-align:left;"   />
				<input id="calculateAgency" name="calculateAgency" class="mini-textbox" labelField="true"  label="计算机构：" required="true"    labelStyle="text-align:left;"  showTreeLines="true" showTreeIcon="true" textField="instName" parentField="parentId" />
				<input id="tenor" name="tenor" class="mini-spinner" labelField="true"  label="期限："  required="true"   labelStyle="text-align:left;"   changeOnMousewheel='false' maxValue="9999999999"  minValue="0"/>
				<input id="interestRateAdjustment" name="interestRateAdjustment" class="mini-combobox" labelField="true"  label="计息天数调整：" required="true"   labelStyle="text-align:left;"  data = "CommonUtil.serverData.dictionary.calIrsDays" />
				<input id="firstPeriodStartDate" name="firstPeriodStartDate" class="mini-datepicker" labelField="true"  label="首期起息日："  required="true"  labelStyle="text-align:left;"   />
				<input id="couponPaymentDateReset" name="couponPaymentDateReset" class="mini-datepicker" labelField="true"  label="支付日调整："  required="true"  ondrawdate="onDrawDatePay" onvaluechanged="onPayDateChanged" labelStyle="text-align:left;"   />
				<input id="tradeMethod" name="tradeMethod" class="mini-combobox" labelField="true"  label="清算方式："  required="true"  labelStyle="text-align:left;"   data = "CommonUtil.serverData.dictionary.Term"/>
			  </fieldset>
			</div>
			<%@ include file="../../Common/opicsApprove.jsp"%>
			<%@ include file="../../Common/Flow/MiniApproveOpCommon.jsp"%>
			</div>
			
		</div>
		<div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
			<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="save_btn"   onclick="save">保存交易</a></div>
			<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="close_btn"   onclick="close">关闭界面</a></div>
		</div>
	</div>	
<script type="text/javascript">
	mini.parse();
	//获取当前tab
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var row=params.selectData;
	var url=window.location.search;
	var action=CommonUtil.getParam(url,"action");
	var ticketId = CommonUtil.getParam(url, "ticketId");
	var form=new mini.Form("#field_form");

	var tradeData={};
	
	tradeData.selectData=row;
	tradeData.operType=action;
	tradeData.serial_no=row.ticketId;
	tradeData.task_id=row.taskId;
	//保存
	function save(){
		//表单验证！！！
		form.validate();
		if (form.isValid() == false) {
			return;
		}

		var data=form.getData(true);
		data['sponInst']="<%=__sessionInstitution.getInstId()%>";
		data['dealTransType']="0";
		var params=mini.encode(data);
		CommonUtil.ajax({
			url:"/IfsApprovermbIrsController/saveIrs",
			data:params,
			callback:function(data){
				mini.alert("保存成功",'提示信息',function(){
					top["win"].closeMenuTab();
				});
			}
		});
	}
	function close(){
		top["win"].closeMenuTab();
	}
	$(document).ready(function(){
		if($.inArray(action,["approve","detail"])>-1){
			mini.get("save_btn").setVisible(false);
			form.setEnabled(false);
			mini.get("postDate").setEnabled(false);
			var form11=new mini.Form("#approve_operate_form");
			form11.setEnabled(true);
		}else if(action=="add"){//增加时，设置审批发起人 审批发起机构
			mini.get("sponsor").setValue("<%=__sessionUser.getUserId()%>");
			mini.get("sponInst").setValue("<%=__sessionUser.getInstId() %>");
		}
		if($.inArray(action,["edit","approve","detail"])>-1){
			form.setData(row);
			mini.get("postDate").setValue(row.postDate);
			mini.get("ticketId").setValue(row.ticketId);
			//.投资组合
			mini.get("port").setValue(row.port);
			mini.get("port").setText(row.port);
			//对方机构counterpartyInstId
			mini.get("counterpartyInstId").setValue(row.cno);
			mini.get("counterpartyInstId").setText(row.counterpartyInstId);
			
		}else{
			mini.get("postDate").setValue(new Date());
		}
	});

	//加载机构树
	function nodeclick(e) {
		
		var oldvalue=e.sender.value;
		var param = mini.encode({"branchId":branchId}); //序列化成JSON
		CommonUtil.ajax({
		  url: "/InstitutionController/searchInstitutionByBranchId",
		  data: param,
		  callback: function (data) {
			var obj=e.sender;
			obj.setData(data.obj);
			obj.setValue(oldvalue);
			
		  }
		});
	}

	

	//文、数字、下划线 的验证
	function onEnglishAndNumberValidations(e) {
		if(e.value == "" || e.value == null){//值为空，就不做校验
			return;
		}
		if (e.isValid) {
			if (isEnglishAndNumbers(e.value) == false) {
				e.errorText = "必须输入英文小写+数字";
				e.isValid = false;
			}
		}
	}
	/* 是否英文+数字 */
	function isEnglishAndNumbers(v) {
		var re = new RegExp("^[0-9a-z\_\-]+$");
		if (re.test(v)) return true;
		return false;
	}	
	
	//英文、数字、下划线 的验证
	function onEnglishAndNumberValidation(e) {
		if(e.value == "" || e.value == null){//值为空，就不做校验
			return;
		}
		if (e.isValid) {
			if (isEnglishAndNumber(e.value) == false) {
				e.errorText = "必须输入英文+数字";
				e.isValid = false;
			}
		}
	}
	/* 是否英文+数字 */
	function isEnglishAndNumber(v) {
		var re = new RegExp("^[0-9a-zA-Z\_\-]+$");
		if (re.test(v)) return true;
		return false;
	}

	function onButtonEdit(e) {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Common/CustMini.jsp",
            title: "选择对手方列表",
            width: 900,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.cno);
                        btnEdit.setText(data.cfn);
                        btnEdit.focus();
                    }
                }

            }
        });

	}
	//起息日 值改变时发生 -> 首期起息日 默认为 起息日时间
	function onStartDateChanged(e){
		mini.get("firstPeriodStartDate").setValue(e.value);
	}

	//到期日 
	function onDrawDateEnd(e) {
		var date = e.date;
		var d = mini.get("startDate").getValue();
		if(d==null || d == ""){
			return;
		}

		if (date.getTime() < d.getTime()) {
			e.allowSelect = false;
		}
	}
    //起息日 
	function onDrawDateStart(e){
		var date = e.date;
		var d = mini.get("endDate").getValue();
		if(d==null || d == ""){
			return;
		}

		if (date.getTime() > d.getTime()) {
			e.allowSelect = false;
		}
	}

	//支付日 > 到期日
	function onDrawDatePay(e){
		var date = e.date;
		var d = mini.get("endDate").getValue();
		if(d==null || d == ""){
			return;
		}

		if (date.getTime() < d.getTime()) {
			e.allowSelect = false;
		}
	}
	//支付日 改变时发生
	function onPayDateChanged(e){
		var d = mini.get("endDate").getValue();
		if(d==null || d == ""){
			mini.alert("请先选择到期日！");
			mini.get("couponPaymentDateReset").setValue("");
			return;
		}
	}

	
	
</script>
<script type="text/javascript" src="../../Common/Flow/MiniApproveOpCommon.js"></script>			
</body>
</html>
