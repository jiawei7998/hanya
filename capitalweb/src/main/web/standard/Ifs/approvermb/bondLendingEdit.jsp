<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
    <script type="text/javascript" src="./rmbVerify.js"></script>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
    <title>债券借贷 维护</title>
    <script type="text/javascript" >
	/**prdCode用于页面投资组合的选择   该全局变量必须为prdCode*/
	var prdCode="SECLEN";
	</script>
  </head>

<body style="width:100%;height:100%;background:white">
<div class="mini-splitter" style="width:100%;height:100%;">
	<div size="90%" showCollapseButton="false">
		<h1 style="text-align:center"><strong>债券借贷</strong></h1>
		<div id="field_form" class="mini-fit area"  style="background:white">
			<input id="sponsor" name="sponsor" class="mini-hidden" />
			<input id="sponInst" name="sponInst" class="mini-hidden" />
			<input id="aDate" name="aDate" class="mini-hidden"/>
			<input id="ticketId" name="ticketId" class="mini-hidden" />
			
			<div class="mini-panel" title="成交单编号" style="width:100%;"  allowResize="true" collapseOnTitleClick="false">
				<div class="leftarea">
					<input style="width:100%;" id="contractId" name="contractId" class="mini-textbox mini-mustFill" labelField="true" requiredErrorText="该输入项为必输项" label="成交单编号：" labelStyle="text-align:left;width:145px;" required="true"  vtype="maxLength:35"/>
				</div>
			</div>
			
			<div class="mini-panel" title="交易基本信息" style="width:100%;"  allowResize="true" collapseOnTitleClick="false">
				<div class="Toparea">
					<input style="width:100%;" id="lendInst" name="lendInst" class="mini-buttonedit mini-mustFill" onbuttonclick="onButtonEdit" labelField="true" requiredErrorText="该输入项为必输项" label="交易对手：" labelStyle="text-align:left;width:145px;" required="true"  vtype="maxLength:35"/>
				</div>
			
				<div class="leftarea">
					<input id="underlyingSecurityId" name="underlyingSecurityId" class="mini-buttonedit mini-mustFill" style="width:100%" required="true"  labelField="true"  label="标的债券代码：" maxLength="20" onbuttonclick="onSecurityQuery"  allowInput="false" labelStyle="text-align:left;width:145px;" />
					<input id="price" name="price" class="mini-spinner mini-mustFill input-text-strong" style="width:100%"   changeOnMousewheel='false' required="true"   labelField="true" label="借贷费率(%)：" minValue="0" maxValue="9999999999999999.99999999" format="n8" changeOnMousewheel="false"  labelStyle="text-align:left;width:145px;" required="true"  />
					<input id="firstSettlementDate" name="firstSettlementDate" class="mini-datepicker mini-mustFill"  required="true"  style="width:100%" labelField="true"  label="首次结算日："  onvaluechanged="linkageCalculatDay" ondrawdate="onDrawDateFirst" labelStyle="text-align:left;width:145px;"   />
					<input id="basis" name="basis" style="width:100%;"  class="mini-spinner mini-mustFill input-text-strong" labelField="true" requiredErrorText="该输入项为必输项" required="true"  label="质押比例%："  labelStyle="text-align:left;width:145px;" />
					<input id="creditamt" name="creditamt" class="mini-spinner mini-mustFill input-text-strong" style="width:100%" enabled="false"  required="true"   changeOnMousewheel='false' labelField="true"  label="同业授信额度(万元)：" maxValue="9999999999999999.9999" format="n4"  labelStyle="text-align:left;width:145px;" required="true"   onvalidation="zeroValidation"/>
					<input id="oppoDir" name="oppoDir" class="mini-combobox mini-mustFill"   labelField="true" required="true"  label="借贷方向："  style="width:100%;"  labelStyle="text-align:left;width:145px;" data = "CommonUtil.serverData.dictionary.oppoDir" />
				</div>
				<div class="rightarea">
					<input id="underlyingQty" name="underlyingQty" class="mini-spinner mini-mustFill input-text-strong" style="width:100%"   required="true"   changeOnMousewheel='false' labelField="true"  label="标的债券券面总额(万元)：" maxValue="9999999999999999.9999" format="n4"  labelStyle="text-align:left;width:145px;" required="true"   onvalidation="zeroValidation"/>
					<input id="tenor" name="tenor" class="mini-spinner mini-mustFill" style="width:100%"  required="true"    changeOnMousewheel='false' labelField="true"  label="借贷期限(天)：" maxValue="99999" labelStyle="text-align:left;width:145px;"   />
					<input id="secondSettlementDate" name="secondSettlementDate" class="mini-datepicker mini-mustFill" required="true"   style="width:100%" labelField="true"  label="到期结算日：" onvaluechanged="linkageCalculatDay" ondrawdate="onDrawDateSecond" labelStyle="text-align:left;width:145px;"   />
					<input id="marginTotalAmt" name="marginTotalAmt" class="mini-spinner mini-mustFill input-text-strong" style="width:100%" enabled="false"  required="true"   changeOnMousewheel='false' labelField="true"  label="质押债券市值总额(万元)：" maxValue="9999999999999999.9999" format="n4"  labelStyle="text-align:left;width:145px;" required="true"   onvalidation="zeroValidation"/>
					<input id="creditProdamt" name="creditProdamt" class="mini-spinner mini-mustFill input-text-strong" style="width:100%" enabled="false"  required="true"   changeOnMousewheel='false' labelField="true"  label="同业用信额度(万元)：" maxValue="9999999999999999.9999" format="n4"  labelStyle="text-align:left;width:145px;" required="true"   onvalidation="zeroValidation"/>
					<input id="ispayint" name="ispayint" class="mini-combobox mini-mustFill"  labelField="true"   label="借贷期间是否付息："  style="width:100%;"  labelStyle="text-align:left;width:145px;" data = "CommonUtil.serverData.dictionary.IsOrnot" required="true"  />
				</div>
				
				<div class="underarea">
					<input id="iscanum" name="iscanum" class="mini-combobox mini-mustFill"  labelField="true"   label="是否跨市场托管："  style="width:100%;"  labelStyle="text-align:left;width:145px;" data = "CommonUtil.serverData.dictionary.IsOrnot" required="true"  />
				</div>
			</div>
			
			<div class="mini-panel" title="质押券信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
				<div id="search_detail_form">
					<div class="centerarea">
						<input id="marginSecuritiesId" name="marginSecuritiesId" class="mini-buttonedit" labelField="true" allowInput="false" label="质押债券代码：" onbuttonclick="onBondQuery"  maxLength="20" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" style="width:33%" labelStyle="text-align:left;width:170px;"   />
						<input id="marginSymbol" name="marginSymbol" class="mini-textbox" labelField="true"   enabled="false"  label="质押债券名称："  labelStyle="text-align:left;width:170px;"  style="width:33%" />
						<input id="marginAmt" name="marginAmt" class="mini-spinner input-text-strong"    changeOnMousewheel='false' labelField="true"  label="质押债券券面总额(万元)：" maxValue="999999999999" format="n4" style="width:33%" labelStyle="text-align:left;width:170px;"   />
						<input id="marginReplacement" name="marginReplacement" class="mini-spinner input-text-strong"    changeOnMousewheel='false' labelField="true"  label="质押券发行规模(亿元)：" maxValue="999999999999" format="n8" style="width:33%" labelStyle="text-align:left;width:170px;"   />
					</div>
				</div>
				
				<div class="centerarea" style="margin-top:5px;">
					<span style="float:left;"></span>
				
					<div class="mini-toolbar" style="padding:0px;border-bottom:0;" id="toolbar"> 
						<table style="width:100%;">
							<tr>
								<td style="width:100%;">                
									<a  id="add_btn" class="mini-button" style="display: none"   onclick="add()">添加</a>
									<a  id="edit_btn" class="mini-button" style="display: none"   onclick="edit()">修改</a>
									<a  id="delete_btn" class="mini-button" style="display: none"   onclick="del()">删除</a>
									<a  id="saves_btn" class="mini-button" style="display: none"    onclick="saves()" enabled="false" >保存</a>
								</td>
							</tr>
						</table>
					</div>
				</div>
			
				<div class="centerarea" style="height:150px;">
					<div class="mini-fit" >
						<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id"  allowAlternating="true"
							fitColumns="false" allowResize="true" sortMode="client" allowAlternating="true" showpager="false">
							<div property="columns">
								<div type="indexcolumn" width="50px" headerAlign="center">序号</div>
								<div field="marginSecuritiesId" width="200" align="center" headerAlign="center"  >质押债券代码</div>
								<div field="marginSymbol" width="230px" align="center"  headerAlign="center" >质押债券名称</div> 
								<div field="marginAmt" width="200" align="right" headerAlign="center"  allowSort="true" numberFormat="n4">质押债券券面总额(万元)</div>
								<div field="marginReplacement" width="200" align="right" headerAlign="center"  allowSort="true" numberFormat="n4">质押券发行规模(亿元)</div>
								<div field="marginLevel" width="200" align="right" headerAlign="center"  allowSort="true" numberFormat="n4">质押债券主体评级</div>
							</div>
						</div>
					</div> 
				</div>
			</div>
	
			<%@ include file="../../Common/RiskCenter.jsp"%>
			<%@ include file="../../Common/Flow/MiniApproveOpCommon.jsp"%>
		</div>
	</div>
	
	<div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
		<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="save_btn"   onclick="save">保存交易</a></div>
		<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="close_btn"   onclick="close">关闭界面</a></div>
	</div>
</div>

<script type="text/javascript">
	mini.parse();

	//获取当前tab
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var row=params.selectData;
	var url=window.location.search;
	var action=CommonUtil.getParam(url,"action");
	var prdNo = CommonUtil.getParam(url, "prdNo");
	var grid=mini.get("datagrid");
	var form=new mini.Form("#field_form");
	
	var tradeData={};
	tradeData.selectData=row;
	tradeData.operType=action;
	tradeData.serial_no=row.ticketId;
	tradeData.task_id=row.taskId;
	
	//加载机构树
	function nodeclick(e) {
		var oldvalue=e.sender.value;
		var param = mini.encode({"branchId":branchId}); //序列化成JSON
		var oldData=e.sender.data;
		if(oldData==null||oldData==""){
			CommonUtil.ajax({
				url: "/InstitutionController/searchInstitutionByBranchId",
				data: param,
				callback: function (data) {
					var obj=e.sender;
					obj.setData(data.obj);
					obj.setValue(oldvalue);
				}
			});
		}
	}
	function onBondQuery(e) {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Ifs/bond/bondAddMini.jsp",
            title: "选择债券列表",
            width: 700,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.bndcd);
                        btnEdit.setText(data.bndcd);
                        mini.get("marginSymbol").setValue(data.bndnm_cn);
                        mini.get("marginSymbol").setText(data.bndnm_cn);
                        btnEdit.focus();
                    }
                }
            }
        });
	}
	
	function onSecurityQuery(e) {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Ifs/bond/bondAddMini.jsp",
            title: "选择债券列表",
            width: 700,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.bndcd);
                        btnEdit.setText(data.bndcd);
                        mini.get("underlyingSymbol").setValue(data.bndnm_cn);
                        mini.get("underlyingSymbol").setText(data.bndnm_cn);
						creditsubvalue(data.bndcd);
                        btnEdit.focus();
                    }
                }
            }
        });
	}

	//添加债券信息
    function add(){
        var detailForm = new mini.Form("#search_detail_form");
		var detailData=detailForm.getData(true);
		if(CommonUtil.isNull(detailData.marginSecuritiesId)||CommonUtil.isNull(detailData.marginAmt)){
			mini.alert("请将债券信息填写完整","系统提示");
			return;
		}
        addRow(detailData);
    }
    function addRow(detailData){
        var row = {
        		marginSecuritiesId:detailData.marginSecuritiesId,
        		marginSymbol:detailData.marginSymbol,
        		marginAmt:detailData.marginAmt,
        		marginReplacement:detailData.marginReplacement
        };
        grid.addRow(row);
        var detailForm = new mini.Form("#search_detail_form");
        detailForm.clear();
        //重新计算券面总额合计(万元)
        calcAmtSum(grid.getData());     
    }
    /**计算 券面总额合计*/
	function calcAmtSum (rows){
		var marginAllAmt=0;
		for(var i=0 ; i <rows.length ; i++){
			marginAllAmt=marginAllAmt + parseFloat(rows[i].marginAmt);
		}
        mini.get("marginTotalAmt").setValue(marginAllAmt);	
	}; 
	
	//删除
   function del(){
       var row = grid.getSelected();
       if(!row){
           mini.alert("请选中要移除的一行","提示");
           return;
       }
        grid.removeRow(row);
        //重新计算授信额度总和
        calcAmtSum(grid.getData());
   }

	   //修改
	   function edit(){
	        var row = grid.getSelected();
	        if(!row){
	            mini.alert("请选中一行进行修改!","提示");
	            return;
	        }
	        var detailForm = new mini.Form("#search_detail_form");
	        detailForm.setData(row);
	        mini.get("marginSecuritiesId").setValue(row.marginSecuritiesId);
	        mini.get("marginSecuritiesId").setText(row.marginSecuritiesId);

	        mini.get("marginSymbol").setText(row.marginSymbol);
	        mini.get("marginAmt").setText(row.marginAmt);
	        mini.get("marginReplacement").setText(row.marginReplacement);
	        
	        mini.get("add_btn").setEnabled(false);
	        mini.get("edit_btn").setEnabled(false);
	        mini.get("delete_btn").setEnabled(false);
	        mini.get("saves_btn").setEnabled(true);
	   }

	   //修改保存
	   function saves(){
	        del();
	        add();
	        mini.get("add_btn").setEnabled(true);
	        mini.get("edit_btn").setEnabled(true);
	        mini.get("delete_btn").setEnabled(true);
	        mini.get("saves_btn").setEnabled(false);
	   }
	   //初始化债券信息
	   function initGrid(){
			var url="/IfsApprovermbSlController/searchBondSlDetails";
			var data={};
			data['ticketId']=mini.get("ticketId").getValue();
			var params=mini.encode(data);
			CommonUtil.ajax({
				url:url,
				data:params,
				callback : function(data) {
					grid.setData(data.obj.rows);
				}
			});
		}
	   
	//保存
	function save(){
		
		//表单验证！！！
		form.validate();
		if (form.isValid() == false) {
			return;
		}

		var data=form.getData(true);
		var rows = grid.getData();
		if(true){
			mini.confirm("确认以当前数据为准吗？","确认",function (action) {
				if (action != "ok") {
					return;
				}
				data['borrowInst']="<%=__sessionInstitution.getInstId()%>";
				data['borrowTrader']="<%=__sessionUser.getUserId() %>";
				data['sponInst']="<%=__sessionInstitution.getInstId()%>";
 				data['dealTransType']="1";
				data['bondDetailsList']=rows;
				var params=mini.encode(data);
				CommonUtil.ajax({
					url:"/IfsApprovermbSlController/saveSl",
					data:params,
					callback:function(data){
						mini.alert(data.desc,'提示信息',function(){
							if(data.code =='error.common.0000'){
								top["win"].closeMenuTab();//关闭并刷新当前界面
							}
						});
					}
				});
			});
		}else{
			data['borrowInst']="<%=__sessionInstitution.getInstId()%>";
			data['borrowTrader']="<%=__sessionUser.getUserId() %>";
			data['sponInst']="<%=__sessionInstitution.getInstId()%>";
 			data['dealTransType']="1";
			data['bondDetailsList']=rows;
			var params=mini.encode(data);
			CommonUtil.ajax({
				url:"/IfsApprovermbSlController/saveSl",
				data:params,
				callback:function(data){
					mini.alert(data,'提示信息',function(){
						if(data.code =='error.common.0000'){
							top["win"].closeMenuTab();//关闭并刷新当前界面
						}
					});
				}
			});
		}
	} 
	
	function close(){
		top["win"].closeMenuTab();
	}
	
	//文、数字、下划线 的验证
	function onEnglishAndNumberValidations(e) {
		if(e.value == "" || e.value == null){//值为空，就不做校验
			return;
		}
		if (e.isValid) {
			if (isEnglishAndNumbers(e.value) == false) {
				e.errorText = "必须输入英文小写+数字";
				e.isValid = false;
			}
		}
	}
	/* 是否英文+数字 */
	function isEnglishAndNumbers(v) {
		var re = new RegExp("^[0-9a-z\_\-]+$");
		if (re.test(v)) return true;
		return false;
	}	
	
	
	//英文、数字、下划线 的验证
	function onEnglishAndNumberValidation(e) {
		if(e.value == "" || e.value == null){//值为空，就不做校验
			return;
		}
		if (e.isValid) {
			if (isEnglishAndNumber(e.value) == false) {
				e.errorText = "必须输入英文+数字";
				e.isValid = false;
			}
		}
	}
	/* 是否英文+数字 */
	function isEnglishAndNumber(v) {
		var re = new RegExp("^[0-9a-zA-Z\_\-]+$");
		if (re.test(v)) return true;
		return false;
	}
	$(document).ready(function(){
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
			if ($.inArray(action, ["approve", "detail"]) > -1) {
				mini.get("save_btn").setVisible(false);
				form.setEnabled(false);
				var approForm = new mini.Form("#approve_operate_form");
				approForm.setEnabled(true);
				mini.get("toolbar").setVisible(false);
				mini.get("marginSecuritiesId").setVisible(false);
				mini.get("marginSymbol").setVisible(false);
				mini.get("marginAmt").setVisible(false);
				mini.get("marginReplacement").setVisible(false);
			}
			if ($.inArray(action, ["add", "edit"]) > -1) {
				mini.get("marginSecuritiesId").setVisible(true);
				mini.get("marginSymbol").setVisible(true);
				mini.get("marginAmt").setVisible(true);
			}
			if ($.inArray(action, ["edit", "approve", "detail"]) > -1) {
				form.setData(row);
				initGrid();
				mini.get("ticketId").setValue(row.ticketId);

				//标的券代码
				mini.get("underlyingSecurityId").setValue(row.underlyingSecurityId);
				mini.get("underlyingSecurityId").setText(row.underlyingSecurityId);

				//对方机构lendInst
				mini.get("lendInst").setValue(row.cno);
// 			mini.get("lendInst").setText(row.lendInst); 
				queryTextName(row.cno);

				queryCustNo(row.custNo);
				//dirVerify();
				creditsubvalue(row.underlyingSecurityId)

			} else {
				mini.get("sponsor").setValue("<%=__sessionUser.getUserId()%>");
				mini.get("sponInst").setValue("<%=__sessionUser.getInstId() %>");
			}
//		mini.get("dealTransType").setEnabled(false);
		});
	});

	//获取债券发行人
	function initBond(bndcd){
		CommonUtil.ajax({
			url:"/IfsOpicsBondController/searchOneById",
			data:{"bndcd":bndcd},
			callback : function(data) {
				if(data!=null){
					return data.issuer;
				}else{
					return null;
				}
			}
		});
	}

	function onButtonEdit(e) {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Common/CustMini.jsp",
            title: "选择对手方列表",
            width: 900,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.cno);
                        btnEdit.setText(data.cliname);
                        queryCustNo(data.creditsub);//占用授信主体
                        btnEdit.focus();
                    }
                }

            }
        });
    }
  //根据交易对手编号查询全称
	function queryTextName(cno){
		CommonUtil.ajax({
		    url: "/IfsOpicsCustController/searchIfsOpicsCust",
		    data : {'cno' : cno},
		    callback:function (data) {
		    	if (data && data.obj) {
		    		mini.get("lendInst").setText(data.obj.cliname);
				} else {
					mini.get("lendInst").setText(cno);
		    }
		    }
		});
	}
	function onDrawDateFirst(e) {
        var firstDate = e.date;
        var secondDate= mini.get("secondSettlementDate").getValue();
        if(CommonUtil.isNull(secondDate)){
        	return;
        }
        if (secondDate.getTime() < firstDate.getTime()) {
            e.allowSelect = false;
        }
    }
	function onDrawDateSecond(e) {
        var secondDate = e.date;
        var firstDate = mini.get("firstSettlementDate").getValue();
        if(CommonUtil.isNull(firstDate)){
        	return;
        }
        if (secondDate.getTime() < firstDate.getTime()) {
            e.allowSelect = false;
        }
    }
	function linkageCalculatDay(e){
		var firstDate = mini.get("firstSettlementDate").getValue();
		var secondDate= mini.get("secondSettlementDate").getValue();
		if(CommonUtil.isNull(firstDate)||CommonUtil.isNull(secondDate)){
			return;
		}
		var subDate= Math.abs(parseInt((secondDate.getTime() - firstDate.getTime())/1000/3600/24));
		mini.get("tenor").setValue(subDate);
		
		var rate=mini.get("price").getValue();
		var tradeAmount=mini.get("underlyingQty").getValue();
		//mini.get("miscFeeType").setValue(tradeAmount*10000*rate*subDate/100/365);
		
	}
	
	
	function dirVerify(e){
		var myDir = mini.get("myDir").getValue();
		if(myDir == 'P'){//借入
			mini.get("oppoDir").setValue("S");
			mini.get("applyProd").setVisible(false);
			mini.get("custNo").setVisible(false);
			mini.get("weight").setVisible(false);
        	<%-- mini.get("custNo").setValue("<%=__sessionUser.getInstId() %>");//占用授信主体
			mini.get("custNo").setText("<%=__sessionInstitution.getInstFullname()%>");//占用授信主体 --%>
		}else if(myDir == 'S'){//借出
			mini.get("oppoDir").setValue("P");
			mini.get("applyProd").setVisible(false);
			mini.get("custNo").setVisible(true);
			mini.get("weight").setVisible(true);
			/* mini.get("custNo").setValue(mini.get("sellInst").getValue());//产品名称
			mini.get("custNo").setValue(mini.get("sellInst").getText());//产品名称 */
		}
	};
	
	//授信主体
	function creditsubvalue(bondCode) {
		CommonUtil.ajax({
			url:"/HrbCreditLimitOccupyController/checklimit",
			data:{currency:"CNY",custName:null,prodType:"445",bondCode:bondCode,creditType:"1"},
			callback:function(data){
				if(data.obj.length == 0) {
					mini.get("creditamt").setValue(null);
					mini.get("creditProdamt").setValue(null);
				}else {
					mini.get("creditamt").setValue(data.obj[0].loanAmt);
					mini.get("creditProdamt").setValue(data.obj[0].proAvlAmt);
				}
			}
		});
	}

</script>
<script type="text/javascript" src="../../Common/Flow/MiniApproveOpCommon.js"></script>			
</body>
</html>
