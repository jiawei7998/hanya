<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<script type="text/javascript" >
	/**prdCode用于页面投资组合的选择   该全局变量必须为prdCode*/
	var prdCode="SECLEN";
</script>
<title>债券借贷</title>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
	<legend>债券借贷成交单查询</legend>	
	<div>
		<div id="search_form" style="width:100%" cols="6">
<%--			<input id="startDate" name="startDate" class="mini-datepicker" labelField="true"  label="成交日期："--%>
<%--				   ondrawdate="onDrawDateStart" format="yyyy-MM-dd" value="<%=__bizDate%>"  labelStyle="text-align:right;" emptyText="开始日期" />--%>
<%--			<span>~</span>--%>
<%--			<input id="endDate" name="endDate" class="mini-datepicker"--%>
<%--				   ondrawdate="onDrawDateEnd" emptyText="结束日期" format="yyyy-MM-dd" value="<%=__bizDate%>"/>--%>
			<input id="contractId" name="contractId" class="mini-textbox" labelField="true"  label="成交单编号：" width="280px" labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请输入成交编号" />
			<input id="ticketId" name="ticketId" class="mini-textbox" labelField="true"  label="审批单号：" width="280px" labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请输入审批单号" />
			<input id="approveStatus" name="approveStatus" class="mini-combobox" data="CommonUtil.serverData.dictionary.ApproveSta" width="280px" emptyText="请选择审批状态" labelField="true"  label="审批状态：" labelStyle="text-align:right;"/>
			<span style="float: right; margin-right: 150px"> 
				<a id="search_btn" class="mini-button" style="display: none"   onclick="searchs()">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
			</span>
		</div>
	</div>
</fieldset> 
<%@ include file="../batchReback.jsp"%>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>   
<div class="mini-fit" >
	<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:80%;" idField="id"  allowAlternating="true"
		onrowdblclick="onRowDblClick" allowResize="true" sortMode="client" allowAlternating="true" multiSelect="true">
		<div property="columns">
		<div type="checkcolumn"></div>
			<div type="indexcolumn" width="50px" headerAlign="center">序号</div>
			<div field="prdNo" width="100" allowSort="false" headerAlign="center" align="center" visible="false"></div>
			<div field="taskName" width="180" allowSort="false" headerAlign="center" align="">审批状态</div>
			<div field="contractId" width="180px" align="center"  headerAlign="center" >成交单号</div>
			<div field="ticketId" width="150" allowSort="false" headerAlign="center" align="center">审批单号</div>
			<div field="lendInst" width="150px" align="center"  headerAlign="center" >融出方机构</div>
			<div field="oppoDir" width="100px" align="center" headerAlign="center"  renderer="CommonUtil.dictRenderer" data-options="{dict:'oppoDir'}">本方方向</div>

			<div field="secname" width="180px" align="left"  headerAlign="center" >标的债券简称</div>
			<div field="underlyingQty" width="180px" align="right"  headerAlign="center" allowSort="true" numberFormat='n4'>标的债券券面总额（万元）</div>
			<div field="price" width="100px" align="right"  headerAlign="center" allowSort="true" numberFormat='n8'>借贷费率</div>
			<div field="tenor" width="80px" align="right"  headerAlign="center" allowSort="true" >借贷期限(天)</div>
			<div field="basis" width="80px" align="right"  headerAlign="center" allowSort="true" >质押比例%</div>
			<div field="marginTotalAmt" width="180px" align="right"  headerAlign="center" allowSort="true" numberFormat='n4'>质押债券市值总额(万元)</div>
			<div field="firstSettlementDate" width="120px" align="center"  headerAlign="center" allowSort="true">首次结算日</div>
			<div field="secondSettlementDate" width="120px" align="center"  headerAlign="center" allowSort="true">到期结算日</div>
			<div field="sponsor" width="120" allowSort="false" headerAlign="center" align="center">审批发起人</div>
			<div field="sponInst" width="120" allowSort="false" headerAlign="center" align="center">审批发起机构</div>
			<div field="aDate" width="180" allowSort="false" headerAlign="center" align="center">审批发起时间</div>
		</div>
	</div>
	<fieldset style="width: 100%;border:solid 1px #aaa;margin-top:8px;position:relative;">
		<legend>债券借贷详情</legend>
			<div id="MiniSettleForeigDetail" style="padding:5px;" class="mini-fit area" tyle="width: 100%; height: 50%;" allowAlternating="true" allowResize="true" border="true" sortMode="client">
				<input class="mini-hidden" name="id"/>
					
				<div class="mini-panel" title="交易基本信息" style="width:100%;"  allowResize="true" collapseOnTitleClick="false">
					<div class="Toparea">
						<input style="width:100%;" id="lendInst" name="lendInst" class="mini-textbox mini-mustFill" labelField="true" requiredErrorText="该输入项为必输项" label="交易对手：" labelStyle="text-align:left;width:145px;" required="true"  vtype="maxLength:35"/>
					</div>
				
					<div class="leftarea">
						<input id="underlyingSecurityId" name="underlyingSecurityId" class="mini-textbox mini-mustFill" style="width:100%" required="true"  labelField="true"  label="标的债券代码：" maxLength="20" onbuttonclick="onSecurityQuery"  allowInput="false" labelStyle="text-align:left;width:145px;" />
						<input id="price" name="price" class="mini-spinner mini-mustFill" style="width:100%"   changeOnMousewheel='false' required="true"   labelField="true" label="借贷费率(%)：" minValue="0" maxValue="9999999999999999.99999999" format="n8" changeOnMousewheel="false"  labelStyle="text-align:left;width:145px;" required="true"  />
						<input id="firstSettlementDate" name="firstSettlementDate" class="mini-datepicker mini-mustFill"  required="true"  style="width:100%" labelField="true"  label="首次结算日："  onvaluechanged="linkageCalculatDay" ondrawdate="onDrawDateFirst" labelStyle="text-align:left;width:145px;"   />
						<input id="basis" name="basis" style="width:100%;"  class="mini-spinner mini-mustFill" labelField="true" requiredErrorText="该输入项为必输项" required="true"  label="质押比例%："  labelStyle="text-align:left;width:145px;" />
						<input id="int1" name="int1" class="mini-spinner mini-mustFill" style="width:100%" enabled="false"  required="true"   changeOnMousewheel='false' labelField="true"  label="同业授信额度(万元)：" maxValue="9999999999999999.9999" format="n4"  labelStyle="text-align:left;width:145px;" required="true"   onvalidation="zeroValidation"/>
						<input id="oppoDir" name="oppoDir" class="mini-combobox mini-mustFill"   labelField="true" required="true"  label="借贷方向："  style="width:100%;"  labelStyle="text-align:left;width:145px;" data = "CommonUtil.serverData.dictionary.oppoDir" />
					</div>
					<div class="rightarea">
						<input id="underlyingQty" name="underlyingQty" class="mini-spinner mini-mustFill" style="width:100%"   required="true"   changeOnMousewheel='false' labelField="true"  label="标的债券券面总额(万元)：" maxValue="9999999999999999.9999" format="n4"  labelStyle="text-align:left;width:145px;" required="true"   onvalidation="zeroValidation"/>
						<input id="tenor" name="tenor" class="mini-spinner mini-mustFill" style="width:100%"  required="true"    changeOnMousewheel='false' labelField="true"  label="借贷期限(天)：" maxValue="99999" labelStyle="text-align:left;width:145px;"   />
						<input id="secondSettlementDate" name="secondSettlementDate" class="mini-datepicker mini-mustFill" required="true"   style="width:100%" labelField="true"  label="到期结算日：" onvaluechanged="linkageCalculatDay" ondrawdate="onDrawDateSecond" labelStyle="text-align:left;width:145px;"   />
						<input id="marginTotalAmt" name="marginTotalAmt" class="mini-spinner mini-mustFill" style="width:100%" enabled="false"  required="true"   changeOnMousewheel='false' labelField="true"  label="质押债券市值总额(万元)：" maxValue="9999999999999999.9999" format="n4"  labelStyle="text-align:left;width:145px;" required="true"   onvalidation="zeroValidation"/>
						<input id="int2" name="int2" class="mini-spinner mini-mustFill" style="width:100%" enabled="false"  required="true"   changeOnMousewheel='false' labelField="true"  label="同业用信额度(万元)：" maxValue="9999999999999999.9999" format="n4"  labelStyle="text-align:left;width:145px;" required="true"   onvalidation="zeroValidation"/>
						<input id="ispayint" name="ispayint" class="mini-combobox mini-mustFill"  labelField="true"   label="借贷期间是否付息："  style="width:100%;"  labelStyle="text-align:left;width:145px;" data = "CommonUtil.serverData.dictionary.IsOrnot" required="true"  />
					</div>
					
					<div class="underarea">
						<input id="iscanum" name="iscanum" class="mini-combobox mini-mustFill"  labelField="true"   label="是否跨市场托管："  style="width:100%;"  labelStyle="text-align:left;width:145px;" data = "CommonUtil.serverData.dictionary.IsOrnot" required="true"  />
					</div>
				</div>
				
				<div class="mini-panel" title="交易明细信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
					<div class="centerarea" style="height:150px;">
						<div class="mini-fit" >
							<div id="datagrid1" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id"  allowAlternating="true"
								fitColumns="false" allowResize="true" sortMode="client" allowAlternating="true" showpager="false">
								<div property="columns">
									<div type="indexcolumn" width="50px" headerAlign="center">序号</div>
						            <div field="marginSecuritiesId" width="200" align="center" headerAlign="center"  >质押债券代码</div>
						            <div field="marginSymbol" width="230px" align="center"  headerAlign="center" >质押债券名称</div> 
						            <div field="marginAmt" width="200" align="right" headerAlign="center"  allowSort="true" numberFormat="n4">质押债券券面总额(万元)</div>
									<div field="invtype" width="150" align="right" headerAlign="center"  allowSort="true"  renderer="CommonUtil.dictRenderer" data-options="{dict:'invtype'}">投资类型</div>
									<div field="costb" width="150" align="right" headerAlign="center"  allowSort="true">成本中心</div>
									<div field="prodb" width="150" align="right" headerAlign="center"  allowSort="true">产品</div>
									<div field="prodtypeb" width="150" align="right" headerAlign="center"  allowSort="true">产品类型</div>
								</div>
							</div>
						</div> 
					</div>
				</div>
			</div>
	</fieldset>
</div>   

<script>
	mini.parse();

	var url = window.location.search;
	var prdNo = CommonUtil.getParam(url, "prdNo");
	var prdName = CommonUtil.getParam(url, "prdName");
	var form = new mini.Form("#search_form");
	var grid=mini.get("datagrid");
	var grid1=mini.get("datagrid1");
	var row="";
	/**************************************点击下面显示详情开始******************************/
	var from = new mini.Form("MiniSettleForeigDetail");
	from.setEnabled(false);
	var grid = mini.get("datagrid");
	//grid.load();
	 //绑定表单
    var db = new mini.DataBinding();
    db.bindForm("MiniSettleForeigDetail", grid);
	$(document).ready(function () {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn){
		});
	});
    /**************************************点击下面显示详情结束******************************/
	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});
	//成交日期
	function onDrawDateStart(e) {
		var startDate = e.date;
		var endDate= mini.get("endDate").getValue();
		if(CommonUtil.isNull(endDate)){
			return;
		}
		if (endDate.getTime() < startDate.getTime()) {
			e.allowSelect = false;
		}
	}
	function onDrawDateEnd(e) {
		var endDate = e.date;
		var startDate = mini.get("startDate").getValue();
		if(CommonUtil.isNull(startDate)){
			return;
		}
		if (endDate.getTime() < startDate.getTime()) {
			e.allowSelect = false;
		}
	}
	//初始化债券信息
	function initGrid(ticketId){
			var url="/IfsApprovermbSlController/searchBondSlDetails";
			var data={};
			data['ticketId']=ticketId;
			var params=mini.encode(data);
			CommonUtil.ajax({
				url:url,
				data:params,
				callback : function(data) {
					grid1.setData(data.obj.rows);
				}
			});
		}
	//查看详情
	function onRowDblClick(e) {
		var url = CommonUtil.baseWebPath() +"/approvermb/bondLendingEdit.jsp?action=detail&prdNo="+prdNo+"&prdName="+prdName;
		var tab = {id: "SlDetail",name:"SlDetail",url:url,title:"债券借贷成交单详情",
					parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
		var paramData = {selectData:grid.getSelected()};
		CommonUtil.openNewMenuTab(tab,paramData);
	}
	//清空
	function clear(){
		var MiniSettleForeigDetail=new mini.Form("MiniSettleForeigDetail");
		MiniSettleForeigDetail.clear();
		var form=new mini.Form("search_form");
		form.clear();
		searchs();
	}
	//添加
	function add(){
		var url = CommonUtil.baseWebPath() +"/approvermb/bondLendingEdit.jsp?action=add&prdNo="+prdNo+"&prdName="+prdName;
		var tab = {id: "SlAdd",name:"SlAdd",url:url,title:"债券借贷成交单补录",
					parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
		var paramData = {selectData:""};
		CommonUtil.openNewMenuTab(tab,paramData);
	}
	//修改
	function edit(){
		var row=grid.getSelected();
		if(row){
		var url = CommonUtil.baseWebPath() +"/approvermb/bondLendingEdit.jsp?action=edit&prdNo="+prdNo+"&prdName="+prdName;
		var tab = {id: "SlEdit",name:"SlEdit",url:url,title:"债券借贷成交单修改",
					parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
		var paramData = {selectData:row};
		CommonUtil.openNewMenuTab(tab,paramData);
	}else{
		mini.alert("请选择一条记录","提示");
	}
	}
	//删除
	function del(){
		var rows=grid.getSelecteds();
		if(rows.length==0){
			mini.alert("请选中一行","提示");
			return;
		}
		if(rows.length>1){
			mini.alert("系统不支持多条数据进行删除","消息提示");
			return;
		}
		mini.confirm("您确认要删除选中记录?","系统警告",function(value){   
			if (value=='ok'){   
				var data=rows[0];
				params=mini.encode(data);
				CommonUtil.ajax( {
					url:"/IfsApprovermbSlController/deleteCfetsrmbSl",
					data:params,
					callback : function(data) {
						mini.alert("删除成功.","系统提示");
						search(10,0);
					}
				});
			}
		});
	}
	
	
	grid.on("select",function(e){
		var row=e.record;
		mini.get("approve_mine_commit_btn").setEnabled(false);
		mini.get("approve_commit_btn").setEnabled(false);
		mini.get("edit_btn").setEnabled(false);
		mini.get("delete_btn").setEnabled(false);
		mini.get("approve_log").setEnabled(true);
		mini.get("recall_btn").setEnabled(true);
		mini.get("batch_commit_btn").setEnabled(true);
		if(row.approveStatus == "3"){//新建
			mini.get("edit_btn").setEnabled(true);
			mini.get("delete_btn").setEnabled(true);
			mini.get("approve_mine_commit_btn").setEnabled(true);
			mini.get("approve_commit_btn").setEnabled(false);
			mini.get("reback_btn").setEnabled(false);
			mini.get("recall_btn").setEnabled(false);
		}
		if( row.approveStatus == "6" || row.approveStatus == "7" || row.approveStatus == "8" || row.approveStatus == "17"){//审批通过：6    审批拒绝:5
			mini.get("edit_btn").setEnabled(false);
			mini.get("delete_btn").setEnabled(false);
			mini.get("approve_mine_commit_btn").setEnabled(false);
			mini.get("approve_commit_btn").setEnabled(true);
			mini.get("batch_approve_btn").setEnabled(false);
			mini.get("batch_commit_btn").setEnabled(false);
			mini.get("opics_check_btn").setEnabled(false);
			mini.get("reback_btn").setEnabled(false);
			mini.get("recall_btn").setEnabled(false);
		}
		if(row.approveStatus == "5"){//审批中:5
			mini.get("edit_btn").setEnabled(false);
			mini.get("delete_btn").setEnabled(false);
			mini.get("approve_mine_commit_btn").setEnabled(false);
			mini.get("approve_commit_btn").setEnabled(true);
			mini.get("batch_commit_btn").setEnabled(false);
			mini.get("reback_btn").setEnabled(true);
			mini.get("recall_btn").setEnabled(true);
		}
		
	});

	//查询 按钮
	function searchs(){
		search(grid.pageSize,0);
	}

	function search(pageSize,pageIndex){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统也提示");
			return;
		}

		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		var url=null;

		var approveType = mini.get("approveType").getValue();
		if(approveType == "mine"){
			url = "/IfsApprovermbSlController/searchRmbSlSwapMine";
		}else if(approveType == "approve"){
			url = "/IfsApprovermbSlController/searchPageRmbSlUnfinished";
		}else{
			url = "/IfsApprovermbSlController/searchPageRmbSlFinished";
		}
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:url,
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	/**************************审批相关****************************/
	//审批日志查看
	function appLog(selections){
		var flow_type = Approve.FlowType.VerifyApproveFlow;
		if(selections.length <= 0){
			mini.alert("请选择要操作的数据","系统提示");
			return;
		}
		if(selections.length > 1){
			mini.alert("系统不支持多笔操作","系统提示");
			return;
		}
		if(selections[0].tradeSource == "3"){
			mini.alert("初始化导入的业务没有审批日志","系统提示");
			return false;
		}
		Approve.approveLog(flow_type,selections[0].ticketId);
	};
	//提交正式审批、待审批
	function verify(selections){
		if(selections.length == 0){
			mini.alert("请选中一条记录！","消息提示");
			return false;
		}else if(selections.length > 1){
			mini.alert("该功能不支持多笔提交，如需多笔提交请点击[批量提交]按钮","系统提示");
			return false;
		}
		if(selections[0]["approveStatus"] != "3" ){
			var url = CommonUtil.baseWebPath() +"/approvermb/bondLendingEdit.jsp?action=approve&ticketId="+row.ticketId;
			var tab = {"id": "bondLendingApprove",name:"bondLendingApprove",url:url,title:"债券借贷审批",
						parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
			var paramData = {selectData:selections[0]};
			CommonUtil.openNewMenuTab(tab,paramData);
			// top["win"].showTab(tab);
		}else{
			Approve.approveCommit(Approve.FlowType.preApprovse,selections[0]["ticketId"],Approve.OrderStatus.New,"IfsApproveRmbSlService",prdNo,function(){
				search(grid.pageSize,grid.pageIndex);
			});
		}
	};
	//审批
	function approve(){
		mini.get("approve_commit_btn").setEnabled(false);
		var messageid = mini.loading("系统正在处理...", "请稍后");
		try {
			verify(grid.getSelecteds());
		} catch (error) {
			
		}
		mini.hideMessageBox(messageid);	
		mini.get("approve_commit_btn").setEnabled(true);
	}
	//提交审批
	function commit(){
		mini.get("approve_mine_commit_btn").setEnabled(false);
		var messageid = mini.loading("系统正在处理...", "请稍后");
		try {
			verify(grid.getSelecteds());
		} catch (error) {
			
		}
		mini.hideMessageBox(messageid);	
		mini.get("approve_mine_commit_btn").setEnabled(true);
	}
	//审批日志
	function searchlog(){
		appLog(grid.getSelecteds());
	}
	//打印
    function print(){
		var selections = grid.getSelecteds();
		if(selections == null || selections.length == 0){
			mini.alert('请选择一条要打印的数据！','系统提示');
			return false;
		}else if(selections.length>1){
			mini.alert('暂不支持多条数据同时打印，请选择一条要打印的数据！','系统提示');
			return false;
		}
		var canPrint = selections[0].ticketId;
		
		if(!CommonUtil.isNull(canPrint)){
			var actionStr = CommonUtil.pPath + "/sl/IfsPrintUtilController/exportload/print/"+PrintNo.bondLendingApprove+"/" + canPrint;
			$('<form target="_blank" action=' + actionStr + ' method="post"></form>').appendTo('body').submit().remove();
		};
	}

	//导出报表
	function exportExcel(){
		exportExcelManin("jyzqjdsq.xls","jyzqjdsq");
	}
	
</script>
</body>
</html>
