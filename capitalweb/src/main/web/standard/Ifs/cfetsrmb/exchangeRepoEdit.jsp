<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../../global.jsp" %>
<html>
<head>
    <script type="text/javascript" src="./rmbVerify.js"></script>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/standard/Common/RiskCenter.js"></script>
    <title>交易所回购维护</title>
    <script type="text/javascript">
        /**prdCode用于页面投资组合的选择   该全局变量必须为prdCode*/
        var prdCode = "REPO";
    </script>
</head>

<body style="width:100%;height:100%;background:white">
<div class="mini-splitter" style="width:100%;height:100%;">
    <div size="90%" showCollapseButton="false">
        <h1 style="text-align:center"><strong>交易所回购</strong></h1>
        <div id="field_form" class="mini-fit area" style="background:white">
            <input id="sponsor" name="sponsor" class="mini-hidden"/>
            <input id="sponInst" name="sponInst" class="mini-hidden"/>
            <input id="aDate" name="aDate" class="mini-hidden"/>
            <input id="ticketId" name="ticketId" class="mini-hidden"/>

            <div class="mini-panel" title="成交单编号" style="width:100%;" allowResize="true" collapseOnTitleClick="true">
                <div class="leftarea">
                    <input style="width:100%;" id="contractId" name="contractId" class="mini-textbox mini-mustFill"
                           labelField="true" required="true" requiredErrorText="该输入项为必输项" label="成交单编号："
                           labelStyle="text-align:left;width:130px;" vtype="maxLength:20"
                           onvalidation="onEnglishAndNumberValidation"/>
                </div>
                <div class="rightarea">
                    <input style="width:100%;" id="cfetsno" name="cfetsno" class="mini-textbox" label="CFETS机构码："
                           labelField="true" visible="false" enabled="false" labelStyle="text-align:left;width:130px;"/>
                </div>
            </div>
            <div class="mini-panel" title="成交双方信息" style="width:100%" allowResize="true" collapseOnTitleClick="true">
                <div class="leftarea">
                    <fieldset>
                        <legend>本方信息</legend>
                        <input id="myDir" name="myDir" class="mini-combobox mini-mustFill" labelField="true"
                               onvaluechanged="dirVerify" label="本方方向：" style="width:100%;" required="true"
                               labelStyle="text-align:left;width:120px;"
                               data="CommonUtil.serverData.dictionary.repoing"/>
                        <input id="positiveInst" name="positiveInst" class="mini-textbox mini-mustFill"
                               labelField="true" label="机构：" style="width:100%;" enabled="false" required="true"
                               labelStyle="text-align:left;width:120px;"/>
                        <input id="positiveTrader" name="positiveTrader" class="mini-textbox mini-mustFill"
                               labelField="true" label="交易员：" required="true" vtype="maxLength:5" enabled="false"
                               labelStyle="text-align:left;width:120px;" style="width:100%;"/>
                        <input id="quotaOccupyType" name="quotaOccupyType" class="mini-combobox mini-mustFill"
                               labelField="true" label="额度占用类型：" required="true" style="width:100%;"
                               labelStyle="text-align:left;width:120px;"
                               data="CommonUtil.serverData.dictionary.QuotaOccupyType" onvaluechanged="quotaJudge"
                               value="1"/>
                        <!-- <input id="positiveTel" name="positiveTel" class="mini-textbox" labelField="true"  label="电话：" onvalidation="CommonUtil.onValidation(e,'phone',[null])"  labelStyle="text-align:left;width:120px;" style="width:100%;"   />
                        <input id="positiveFax" name="positiveFax" class="mini-textbox" labelField="true"  label="传真："  onvalidation="CommonUtil.onValidation(e,'phone',[null])"  labelStyle="text-align:left;width:120px;" style="width:100%;"   />
                        <input id="positiveCorp" name="positiveCorp" class="mini-textbox" labelField="true"  label="法人代表："  vtype="maxLength:10" labelStyle="text-align:left;width:120px;" style="width:100%;"   />
                        <input id="positiveAddr" name="positiveAddr" class="mini-textbox" labelField="true"  label="地址："  vtype="maxLength:50"  labelStyle="text-align:left;width:120px;" style="width:100%;"   />
                     -->
                    </fieldset>
                </div>
                <div class="rightarea">
                    <fieldset>
                        <legend>对手方信息</legend>
                        <input id="oppoDir" name="oppoDir" class="mini-combobox mini-mustFill" enabled="false"
                               labelField="true" label="对方方向：" required="true" style="width:100%;"
                               labelStyle="text-align:left;width:120px;"
                               data="CommonUtil.serverData.dictionary.repoing"/>
                        <input id="reverseInst" name="reverseInst" class="mini-buttonedit mini-mustFill"
                               onbuttonclick="onButtonEdit" labelField="true" allowInput="false" required="true"
                               label="机构：" vtype="maxLength:50" style="width:100%;"
                               labelStyle="text-align:left;width:120px;" enabled="false"/>
                        <input id="reverseTrader" name="reverseTrader" class="mini-textbox"
                               labelField="true" label="交易员：" vtype="maxLength:20"
                               labelStyle="text-align:left;width:120px;" style="width:100%;margin-bottom: 2px"/>
                        <!-- <input id="reverseTel" name="reverseTel" class="mini-textbox" labelField="true"  label="电话："  onvalidation="CommonUtil.onValidation(e,'phone',[null])"  labelStyle="text-align:left;width:120px;" style="width:100%;"   />
                        <input id="reverseFax" name="reverseFax" class="mini-textbox" labelField="true"  label="传真："  onvalidation="CommonUtil.onValidation(e,'phone',[null])"  labelStyle="text-align:left;width:120px;" style="width:100%;"   />
                        <input id="reverseCorp" name="reverseCorp" class="mini-textbox" labelField="true"  label="法人代表："  vtype="maxLength:10" labelStyle="text-align:left;width:120px;" style="width:100%;"   />
                        <input id="reverseAddr" name="reverseAddr" class="mini-textbox" labelField="true"  label="地址："  vtype="maxLength:50"  labelStyle="text-align:left;width:120px;" style="width:100%;"   />
                     -->
                    </fieldset>
                </div>
            </div>
            <div class="mini-panel" title="交易主体信息" style="width:100%" allowResize="true" collapseOnTitleClick="true">
                <div id="search_detail_form">
                    <div class="leftarea">
                        <input style="width:100%" id="bondCode" name="bondCode" class="mini-buttonedit"
                               labelField="true" label="债券代码：" allowInput="false" onbuttonclick="onBondQuery"
                               vtype="maxLength:20" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])"
                               labelStyle="text-align:left;width:130px;"/>
                        <input style="width:100%" id="totalFaceValue" name="totalFaceValue"
                               class="mini-spinner mini-mustFill input-text-strong" changeOnMousewheel='false'
                               required="true" labelField="true" label="券面总额(万元)：" maxValue="9999999999.9999"
                               format="n4" required="true" labelStyle="text-align:left;width:130px;"/>
                        <input style="width:100%;" id="prodb" name="prodb" onbuttonclick="onBondPrdEdit"
                               allowInput="false" class="mini-buttonedit mini-mustFill" labelField="true" label="产品代码："
                               maxLength="6" labelStyle="text-align:left;width:130px;">
                        <input id="portb" name="portb" class="mini-buttonedit mini-mustFill"
                               onbuttonclick="onPortCostQuery" style="width:100%;" labelField="true" label="投资组合："
                               allowInput="false" vtype="maxLength:4" labelStyle="text-align:left;width:130px;">
                        <input id="invtype" name="invtype" class="mini-combobox mini-mustFill" labelField="true"
                               label="投资类型：" style="width:100%;" labelStyle="text-align:left;width:130px;"
                               data="CommonUtil.serverData.dictionary.invtype"/>
                    </div>
                    <div class="rightarea">
                        <input style="width:100%" id="bondName" name="bondName" class="mini-textbox" labelField="true"
                               label="债券名称：" enabled="false" vtype="maxLength:25"
                               labelStyle="text-align:left;width:130px;"/>
                        <input style="width:100%" id="underlyingStipType" name="underlyingStipType"
                               class="mini-spinner mini-mustFill input-text-strong" changeOnMousewheel='false'
                               required="true" labelField="true" label="折算比例(%)：" minValue="0" maxValue="100"
                               format="n6" changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"/>
                        <input style="width:100%;" id="prodtypeb" name="prodtypeb" onbuttonclick="onBondTypeEdit"
                               allowInput="false" class="mini-buttonedit mini-mustFill" labelField="true" label="产品类型："
                               vtype="maxLength:2" labelStyle="text-align:left;width:130px;">
                        <input style="width:100%;" id="costb" name="costb" onbuttonclick="onBondCostEdit"
                               allowInput="false" class="mini-buttonedit mini-mustFill" labelField="true" label="成本中心："
                               vtype="maxLength:15" labelStyle="text-align:left;width:130px;">
                        <input id="note" name="note" class="mini-textbox" labelField="true" label="交易事由："
                               labelStyle="text-align:left;width:130px;" style="width:100%;" vtype="maxLength:16"/>
                    </div>
                </div>
                <div class="centerarea" style="margin-top:5px;">
        <span style="float:left;">
        </span>

                    <div class="mini-toolbar" style="padding:0px;border-bottom:0;" id="toolbar">
                        <table style="width:100%;">
                            <tr>
                                <td style="width:100%;">
                                    <a id="add_btn" class="mini-button" style="display: none" onclick="add()">添加</a>
                                    <a id="edit_btn" class="mini-button" style="display: none" onclick="edit()">修改</a>
                                    <a id="delete_btn" class="mini-button" style="display: none" onclick="del()">删除</a>
                                    <a id="saves_btn" class="mini-button" style="display: none" onclick="saves()"
                                       enabled="false">保存</a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="centerarea" style="height:150px;">
                    <div class="mini-fit">
                        <div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id"
                             allowAlternating="true"
                             fitColumns="false" allowResize="true" sortMode="client" allowAlternating="true"
                             showpager="false">
                            <div property="columns">
                                <div type="indexcolumn" width="50px" headerAlign="center">序号</div>
                                <div field="bondCode" width="150" align="center" headerAlign="center">债券代码</div>
                                <div field="bondName" width="230px" align="center" headerAlign="center">债券名称</div>
                                <div field="totalFaceValue" width="200" align="right" headerAlign="center"
                                     allowSort="true" numberFormat="n4">券面总额(万元)
                                </div>
                                <div field="underlyingStipType" width="120" align="right" headerAlign="center"
                                     allowSort="true" numberFormat="n2">折算比例(%)
                                </div>
                                <div field="prodb" width="100" align="center" headerAlign="center" allowSort="true">
                                    产品代码
                                </div>
                                <div field="prodtypeb" width="80" align="center" headerAlign="center" allowSort="true">
                                    产品类型
                                </div>
                                <div field="portb" width="100" align="center" headerAlign="center" allowSort="true">
                                    投资组合
                                </div>
                                <div field="costb" width="100" align="center" headerAlign="center" allowSort="true">
                                    成本中心
                                </div>
                                <div field="invtype" width="100" align="center" headerAlign="center" allowSort="true"
                                     renderer="CommonUtil.dictRenderer" data-options="{dict:'invtype'}">投资类型
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <%@ include file="../../Common/custLimit.jsp" %>
                <div class="leftarea">
                    <input style="width:100%;" id="forDate" name="forDate" class="mini-datepicker mini-mustFill"
                           labelField="true" required="true" label="成交日期：" labelStyle="text-align:left;width:130px;"/>
                    <input style="width:100%" id="underlyingQty" name="underlyingQty"
                           class="mini-spinner mini-mustFill input-text-strong" changeOnMousewheel='false'
                           labelField="true" required="true" label="券面总额合计(万元)：" maxValue="9999999999.9999" format="n4"
                           required="true" labelStyle="text-align:left;width:130px;" onvalidation="zeroValidation"/>
                    <input style="width:100%" id="tradingProduct" name="tradingProduct" class="mini-combobox"
                           allowInput="false" labelField="true" label="交易品种：" labelStyle="text-align:left;width:130px;"
                           data="CommonUtil.serverData.dictionary.dealVariety" onvaluechanged="associaDataCalculation"/>
                    <input style="width:100%" id="firstSettlementMethod" name="firstSettlementMethod"
                           class="mini-combobox mini-mustFill" labelField="true" required="true" label="首次结算方式："
                           vtype="maxLength:50" data="CommonUtil.serverData.dictionary.SettlementMethod"
                           labelStyle="text-align:left;width:130px;"/>
                    <input style="width:100%" id="firstSettlementDate" name="firstSettlementDate"
                           class="mini-datepicker mini-mustFill" labelField="true" required="true" label="首次结算日："
                           onvaluechanged="associaDataCalculation" ondrawdate="onDrawDateFirst"
                           labelStyle="text-align:left;width:130px;"/>
                    <input style="width:100%" id="tenor" name="tenor" class="mini-spinner mini-mustFill"
                           changeOnMousewheel='false' labelField="true" required="true" label="回购期限(天)："
                           maxValue="99999" labelStyle="text-align:left;width:130px;"/>
                    <input style="width:100%" id="accuredInterest" name="accuredInterest"
                           class="mini-spinner mini-mustFill input-text-strong" changeOnMousewheel='false'
                           labelField="true" required="true" label="应计利息(元)：" maxValue="99999999999999.9999" format="n4"
                           labelStyle="text-align:left;width:130px;"/>
                    <input id="basis" name="basis" class="mini-combobox  mini-mustFill" required="true"
                           labelField="true" label="计息基准：" style="width:100%;"
                           data="CommonUtil.serverData.dictionary.Basis" vtype="maxLength:15"
                           onvaluechanged="linkIntCalculat" labelStyle="text-align:left;width:130px;">
                    <input id="dealTransType" name="dealTransType" class="mini-combobox" labelField="true"
                           style="width:100%;" label="数据来源：" data="CommonUtil.serverData.dictionary.dealTransType1"
                           vtype="maxLength:10" labelStyle="text-align:left;width:130px;" value="1">
                    <input id="fiveLevelClass" name="fiveLevelClass" class="mini-combobox" labelField="true"
                           requiredErrorText="该输入项为必输项" label="五级分类：" labelStyle="text-align:left;width:130px;"
                           style="width:100%;" data="CommonUtil.serverData.dictionary.FiveLevelClass" value="0"/>

                </div>
                <div class="rightarea">
                    <input id="nettingStatus" name="nettingStatus" class="mini-combobox mini-mustFill" required="true"
                           labelField="true" label="净额清算状态：" labelStyle="text-align:left;width:130px;"
                           style="width:100%;" data="CommonUtil.serverData.dictionary.YesNo" showNullItem="true"/>
                    <input style="width:100%" id="tradeAmount" name="tradeAmount"
                           class="mini-spinner mini-mustFill input-text-strong" changeOnMousewheel='false'
                           labelField="true" required="true" label="交易金额(元)：" maxValue="99999999999999.9999" format="n4"
                           onvaluechanged="associaDataCalculation" labelStyle="text-align:left;width:130px;"
                           onvalidation="zeroValidation"/>
                    <input style="width:100%" id="repoRate" name="repoRate"
                           class="mini-spinner mini-mustFill input-text-strong" changeOnMousewheel='false'
                           labelField="true" required="true" label="回购利率(%)：" onvalidation="zeroValidation" minValue="0"
                           maxValue="99999.999999" format="n6" onvaluechanged="associaDataCalculation"
                           changeOnMousewheel="false" required="true" labelStyle="text-align:left;width:130px;"/>
                    <input style="width:100%" id="secondSettlementMethod" name="secondSettlementMethod"
                           class="mini-combobox mini-mustFill" labelField="true" required="true" label="到期结算方式："
                           vtype="maxLength:50" data="CommonUtil.serverData.dictionary.SettlementMethod"
                           labelStyle="text-align:left;width:130px;"/>
                    <input style="width:100%" id="secondSettlementDate" name="secondSettlementDate"
                           class="mini-datepicker mini-mustFill" labelField="true" required="true"
                           onvaluechanged="associaDataCalculation" label="到期结算日：" ondrawdate="onDrawDateSecond"
                           labelStyle="text-align:left;width:130px;"/>
                    <input style="width:100%" id="occupancyDays" name="occupancyDays" class="mini-spinner mini-mustFill"
                           changeOnMousewheel='false' labelField="true" required="true" label="实际占款天数(天)："
                           maxValue="99999" onvaluechanged="linkageCalculat" labelStyle="text-align:left;width:130px;"/>
                    <input style="width:100%" id="secondSettlementAmount" name="secondSettlementAmount"
                           class="mini-spinner mini-mustFill input-text-strong" changeOnMousewheel='false'
                           required="true" labelField="true" maxValue="99999999999999.9999" format="n4"
                           label="到期结算金额(元)：" labelStyle="text-align:left;width:130px;"/>
                    <input id="tradingModel" name="tradingModel" class="mini-combobox  mini-mustFill" required="true"
                           labelField="true" label="交易模式：" labelStyle="text-align:left;width:130px;" style="width:100%;"
                           data="CommonUtil.serverData.dictionary.TradeModel"/>
                    <input id="attributionDept" name="attributionDept" valueField="id" showFolderCheckBox="true"
                           labelField="true" label="归属机构：" required="true" showCheckBox="true" style="width:100%"
                           labelStyle="text-align:left;width:130px"
                           showTreeLines="true" showTreeIcon="true" textField="instName" parentField="parentId"
                           class="mini-treeselect mini-mustFill" expandOnLoad="true" resultAsTree="false"
                           valueFromSelect="true" emptyText="请选择机构"/>
                    <input style="width:100%" id="handleAmt" name="handleAmt"
                           class="mini-spinner mini-mustFill input-text-strong" changeOnMousewheel='false'
                           required="true" labelField="true" maxValue="99999999999999.9999" format="n4"
                           label="手续费(元)：" labelStyle="text-align:left;width:130px;"/>
                </div>
            </div>
            <%@ include file="../../Common/opicsRepo.jsp" %>
            <%@ include file="../../Common/RiskCenter.jsp" %>
            <div class="mini-panel" title="清算信息" style="width:100%" allowResize="true" collapseOnTitleClick="true">
                <div class="leftarea">
                    <fieldset>
                        <legend>本方账户</legend>
                        <input id="positiveAccname" name="positiveAccname" class="mini-textbox" labelField="true"
                               label="资金账户户名：" vtype="maxLength:1333" style="width:100%;"
                               labelStyle="text-align:left;width:120px;"/>
                        <input id="positiveOpbank" name="positiveOpbank" class="mini-textbox" labelField="true"
                               label="资金开户行：" vtype="maxLength:1333" labelStyle="text-align:left;width:120px;"
                               style="width:100%;"/>
                        <input id="positiveAccnum" name="positiveAccnum" class="mini-textbox" labelField="true"
                               label="资金账号：" vtype="maxLength:400"
                               onvalidation="CommonUtil.onValidation(e,'alphanum',[null])"
                               labelStyle="text-align:left;width:120px;" style="width:100%;"/>
                        <input id="positivePsnum" name="positivePsnum" class="mini-textbox" labelField="true"
                               label="支付系统行号：" vtype="maxLength:400"
                               onvalidation="CommonUtil.onValidation(e,'alphanum',[null])"
                               labelStyle="text-align:left;width:120px;" style="width:100%;"/>
                        <input id="positiveCaname" name="positiveCaname" class="mini-textbox" labelField="true"
                               label="托管账户户名：" vtype="maxLength:1333" labelStyle="text-align:left;width:120px;"
                               style="width:100%;"/>
                        <input id="positiveCustname" name="positiveCustname" class="mini-buttonedit mini-mustFill"
                               onbuttonclick="onSaccQuery" required="true" labelField="true" allowInput="false"
                               label="托管机构：" vtype="maxLength:1333" labelStyle="text-align:left;width:120px;"
                               style="width:100%;"/>
                        <input id="positiveCanum" name="positiveCanum" class="mini-textbox" labelField="true"
                               label="托管帐号：" vtype="maxLength:400"
                               onvalidation="CommonUtil.onValidation(e,'alphanum',[null])"
                               labelStyle="text-align:left;width:120px;" style="width:100%;"/>
                    </fieldset>
                </div>
                <div class="rightarea">
                    <fieldset>
                        <legend>对手方账户</legend>
                        <input id="reverseAccname" name="reverseAccname" class="mini-textbox" labelField="true"
                               label="资金账户户名：" vtype="maxLength:1333" style="width:100%;"
                               labelStyle="text-align:left;width:120px;"/>
                        <input id="reverseOpbank" name="reverseOpbank" class="mini-textbox" labelField="true"
                               label="资金开户行：" vtype="maxLength:1333" labelStyle="text-align:left;width:120px;"
                               style="width:100%;"/>
                        <input id="reverseAccnum" name="reverseAccnum" class="mini-textbox" labelField="true"
                               label="资金账号：" vtype="maxLength:400"
                               onvalidation="CommonUtil.onValidation(e,'alphanum',[null])"
                               labelStyle="text-align:left;width:120px;" style="width:100%;"/>
                        <input id="reversePsnum" name="reversePsnum" class="mini-textbox" labelField="true"
                               label="支付系统行号：" vtype="maxLength:400"
                               onvalidation="CommonUtil.onValidation(e,'alphanum',[null])"
                               labelStyle="text-align:left;width:120px;" style="width:100%;"/>
                        <input id="reverseCaname" name="reverseCaname" class="mini-textbox" labelField="true"
                               label="托管账户户名：" vtype="maxLength:1333" labelStyle="text-align:left;width:120px;"
                               style="width:100%;"/>
                        <input id="reverseCustname" name="reverseCustname" class="mini-buttonedit mini-mustFill"
                               onbuttonclick="onSaccQuery" required="true" labelField="true" allowInput="false"
                               label="托管机构：" vtype="maxLength:1333" labelStyle="text-align:left;width:120px;"
                               style="width:100%;"/>
                        <input id="reverseCanum" name="reverseCanum" class="mini-textbox" labelField="true"
                               label="托管帐号：" vtype="maxLength:400"
                               onvalidation="CommonUtil.onValidation(e,'alphanum',[null])"
                               labelStyle="text-align:left;width:120px;" style="width:100%;"/>
                    </fieldset>
                </div>
            </div>
            <%@ include file="../../Common/Flow/MiniApproveOpCommon.jsp" %>
            <%@ include file="../../Common/print/approveFlowLog.jsp" %>
        </div>
    </div>
    <div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
        <div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"
                                                                style="width:120px" id="save_btn"
                                                                onclick="save">保存交易</a></div>
        <div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"
                                                                style="width:120px" id="close_btn"
                                                                onclick="close">关闭界面</a></div>
    </div>
</div>
<script type="text/javascript">
    mini.parse();
    //获取当前tab

    var url = window.location.search;
    var action = CommonUtil.getParam(url, "action");
    var ticketId = CommonUtil.getParam(url, "ticketid");
    mini.get("priceDeviation").setVisible(true);//偏离度
    mini.get("applyNo").setVisible(false);//信用风险审查单号
    mini.get("custNo").setVisible(false);//占用授信主体
    mini.get("applyProd").setVisible(false);//产品名称
    mini.get("weight").setVisible(false);//权重
    var panel13 = mini.get("panel13");

    var prdNo = CommonUtil.getParam(url, "prdNo");
    var fPrdCode = CommonUtil.getParam(url, "fPrdCode");
    var form = new mini.Form("#field_form");
    var grid = mini.get("datagrid");

    var row;
    var detailData;
    var tradeData = {};
    if (action != "print") {
        var currTab = top["win"].tabs.getActiveTab();
        var params = currTab.params;
        row = params.selectData;
        detailData = row;
        tradeData.selectData = row;
        tradeData.operType = action;
        tradeData.serial_no = row.ticketId;
        tradeData.task_id = row.taskId;
    }

    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize, pageIndex);
    });

    function quotaJudge() {
        var quotaType = mini.get("quotaOccupyType").getValue();
        if ("0" == quotaType) {
            //不占额度，隐藏授信页面
            mini.get("panel1").setVisible(false);
            mini.get("panel12").setVisible(false);
        } else if ("1" == quotaType) {
            //占用发行人额度
            mini.get("panel1").setVisible(true);
            mini.get("panel12").setVisible(true);
            var bondCode = mini.get("bondCode").getValue();
            creditsubvalue(null, "446", bondCode);
        } else {
            //占用交易对手
            mini.get("panel1").setVisible(true);
            mini.get("panel12").setVisible(true);
            var cno = mini.get("reverseInst").getValue();
            creditsubvalue(cno, "446", null);
        }
    }

    //加载机构树
    function nodeclick(e) {
        var oldvalue = e.sender.value;
        var param = mini.encode({"branchId": branchId}); //序列化成JSON
        var oldData = e.sender.data;
        if (oldData == null || oldData == "") {
            CommonUtil.ajax({
                url: "/InstitutionController/searchInstitutionByBranchId",
                data: param,
                callback: function (data) {
                    var obj = e.sender;
                    obj.setData(data.obj);
                    obj.setValue(oldvalue);
                }
            });
        }
    }

    function onBondQuery(e) {
        var btnEdit = this;
        /*  var startdate = mini.get("firstSettlementDate").getValue();
         var enddate =mini.get("secondSettlementDate").getValue(); */
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Ifs/bond/bondAddMini.jsp",
            title: "选择债券列表",
            width: 700,
            height: 600,
            /* onload: function () {
                var iframe = this.getIFrameEl();
                var data ={startdate:startdate,enddate:enddate};
                iframe.contentWindow.SetData(data);
            }, */
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.bndcd);
                        btnEdit.setText(data.bndcd);
                        mini.get("bondName").setValue(data.bndnm_cn);
                        mini.get("bondName").setText(data.bndnm_cn);

                        //逆回购 出钱设置授信主体
                        var myDir = mini.get("myDir").getValue();
                        if (myDir == 'P') {
                            if ("1" == mini.get("quotaOccupyType").value) {
                                //额度占用类型为发行人时。填充授信信息
                                creditsubvalue(null, "446", data.bndcd);
                            }
                        }
                        btnEdit.focus();
                    }
                }

            }
        });
    }

    function onCreditTypeChange() {
        var bondCode = mini.get("bondCode").getValue();
        creditsubvalue(null, "468", bondCode)
    }

    function onPortCostQuery() {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "/opics/portMiniLess.jsp",
            title: "选择投资组合",
            width: 500,
            height: 500,
            onload: function () {
                var iframe = this.getIFrameEl();
                iframe.contentWindow.SetData({});
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.portfolio);
                        btnEdit.setText(data.portfolio);
                        btnEdit.focus();
                    }
                }

            }
        });
    }

    //添加债券信息
    function add() {
        var myDir = mini.get("myDir").getValue();
        if (myDir == "") {
            mini.alert("请先选择方向", "系统提示");
            return;
        }

        var detailForm = new mini.Form("#search_detail_form");
        var detailData = detailForm.getData(true);
        if (CommonUtil.isNull(detailData.bondCode) || CommonUtil.isNull(detailData.portb)
            || CommonUtil.isNull(detailData.costb) || CommonUtil.isNull(detailData.invtype)
            || CommonUtil.isNull(detailData.prodb) || CommonUtil.isNull(detailData.prodtypeb)
            || toDecimal(detailData.totalFaceValue) <= 0 || toDecimal(detailData.underlyingStipType) <= 0
        ) {
            mini.alert("请将债券信息填写完整", "系统提示");
            return;
        }

        if (myDir == 'P') {
            var creditForm = new mini.Form("#search_credit_form");
            var creditData = creditForm.getData(true);
            creditData['bondCode'] = detailData.bondCode;
            if("0"!=mini.get("quotaOccupyType").getValue()){
                if (CommonUtil.isNull(creditData.custNo) || CommonUtil.isNull(creditData.creditSubnm)
                    || CommonUtil.isNull(creditData.weight) || CommonUtil.isNull(creditData.custType)
                    || CommonUtil.isNull(creditData.creditBamt) || CommonUtil.isNull(creditData.creditProdamt)
                    || CommonUtil.isNull(creditData.bondCode)
                ) {
                    mini.alert("请将授信额度信息填写完善", "系统提示");
                    return;
                }
            }
            addCredit(creditData);
        }

        addRow(detailData);

        mini.get("add_btn").setEnabled(true);
        mini.get("edit_btn").setEnabled(true);
        mini.get("delete_btn").setEnabled(true);
        mini.get("saves_btn").setEnabled(false);
    }

    function addRow(detailData) {
        var row = {
            bondCode: detailData.bondCode,
            bondName: detailData.bondName,
            totalFaceValue: detailData.totalFaceValue,
            portb: detailData.portb,
            costb: detailData.costb,
            invtype: detailData.invtype,
            prodb: detailData.prodb,
            prodtypeb: detailData.prodtypeb,
            underlyingStipType: detailData.underlyingStipType
        };
        grid.addRow(row);
        var detailForm = new mini.Form("#search_detail_form");
        detailForm.clear();
        //重新计算券面总额合计(万元)
        calcAmtSum(grid.getData());
    }

    /**计算 券面总额合计*/
    function calcAmtSum(rows) {
        var underlyingQty = 0;
        var tradeAmount = 0;
        for (var i = 0; i < rows.length; i++) {
            underlyingQty = underlyingQty + parseFloat(rows[i].totalFaceValue);
            var qty = parseFloat(rows[i].underlyingStipType);
            tradeAmount = tradeAmount + parseFloat(rows[i].totalFaceValue) * qty * 100;
        }
        mini.get("underlyingQty").setValue(underlyingQty);
        mini.get("tradeAmount").setValue(tradeAmount);
    }

    //删除
    function del() {
        var row = grid.getSelected();
        if (!row) {
            mini.alert("请选中要移除的一行", "提示");
            return;
        }
        grid.removeRow(row);
        //重新计算授信额度总和
        calcAmtSum(grid.getData());

        var myDir = mini.get("myDir").getValue();
        if (myDir == 'P') {
            delCredit(row.bondCode);
        }

        mini.get("add_btn").setEnabled(true);
        mini.get("edit_btn").setEnabled(true);
        mini.get("delete_btn").setEnabled(true);
        mini.get("saves_btn").setEnabled(false);
    }

    //修改
    function edit() {
        var row = grid.getSelected();
        if (!row) {
            mini.alert("请选中一行进行修改!", "提示");
            return;
        }
        var detailForm = new mini.Form("#search_detail_form");
        detailForm.setData(row);
        //债券代码
        mini.get("bondCode").setValue(row.bondCode);
        mini.get("bondCode").setText(row.bondCode);
        mini.get("prodb").setValue(row.prodb);
        mini.get("prodb").setText(row.prodb);
        mini.get("prodtypeb").setValue(row.prodtypeb);
        mini.get("prodtypeb").setText(row.prodtypeb);
        mini.get("costb").setValue(row.costb);
        mini.get("costb").setText(row.costb);
        mini.get("portb").setValue(row.portb);
        mini.get("portb").setText(row.portb);
        mini.get("add_btn").setEnabled(false);
        mini.get("edit_btn").setEnabled(false);
        mini.get("delete_btn").setEnabled(false);
        mini.get("saves_btn").setEnabled(true);

        editCredit(row.bondCode);
    }

    //修改保存
    function saves() {
        del();
        add();
    }

    //初始化债券信息
    function initGrid() {
        var url = "/IfsCfetsrmbCrController/searchBondCrDetails";
        var data = {};
        data['ticketId'] = mini.get("ticketId").getValue();
        data['prodNo'] = "468";
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: url,
            data: params,
            callback: function (data) {
                var rows = data.obj.rows;
                grid.setData(rows);

                var myDir = mini.get("myDir").getValue();
                if (myDir == 'P') {
                    fenkai(rows);
                }
            }
        });
    }

    //保存
    function save() {
        //表单验证！！！
        form.validate();
        if (form.isValid() == false) {
            mini.alert("信息填写有误，请重新填写", "系统提示");
            return;
        }
        var data = form.getData(true);
        var rows = grid.getData();
        data.custNo = mini.get("creditCno").getValue();
        if (rows.length == 0) {
            mini.alert("债券信息不能为空", "系统提示");
            return;
        }

        if (data.myDir == 'P') {
            var crets = mini.get("panel13").getData();
            if (crets.length == 0) {
                mini.alert("额度信息不能为空", "系统提示");
                return;
            }
            if (rows.length != crets.length) {
                mini.alert("额度信息和债券信息数据不对应", "系统提示");
                return;
            }
            hebing(rows, crets);
        }

        if (toDecimal(data['tradeAmount']) != toDecimal(data['underlyingStipType'] * data['underlyingQty'] * 10000 / 100) || data['tenor'] != data['occupancyDays'] || toDecimal(data['accuredInterest']) != toDecimal(data['tradeAmount'] * data['repoRate'] * data['occupancyDays'] / 100 / 365)
            || toDecimal(data['secondSettlementAmount']) != parseFloat(toDecimal((data['tradeAmount']))) + parseFloat(toDecimal((data['accuredInterest']))) != "0.00") {
            mini.confirm("确认以当前数据为准吗？", "确认", function (action) {
                if (action != "ok") {
                    return;
                }
                data['positiveInst'] = "<%=__sessionInstitution.getInstId()%>";
                data['positiveTrader'] = "<%=__sessionUser.getUserId() %>";
                data['sponInst'] = "<%=__sessionInstitution.getInstId()%>";
                data['sponsor'] = "<%=__sessionUser.getUserId() %>";
//              data['dealTransType']="1";
                data['fPrdCode']=fPrdCode;
                data['bondDetailsList'] = rows;
                var params = mini.encode(data);
                CommonUtil.ajax({
                    url: "/IfsCfetsrmbCrController/saveCr",
                    data: params,
                    callback: function (data) {
                        if (data.desc.indexOf("短头寸") > -1) {
                            //短头寸仍然可以提交
                            mini.confirm(data + ",是否保存", "确认", function (action) {
                                if (action != "ok") {
                                    return;
                                }
                                saveCrTrue(params);
                            });
                        } else {
                            mini.alert(data.desc, '提示信息', function () {
                                if (data.code == 'error.common.0000') {
                                    top["win"].closeMenuTab();//关闭并刷新当前界面
                                }
                            });
                        }
                    }
                });
            });
        } else {
            data['positiveInst'] = "<%=__sessionInstitution.getInstId()%>";
            data['positiveTrader'] = "<%=__sessionUser.getUserId() %>";
            data['sponInst'] = "<%=__sessionInstitution.getInstId()%>";
            data['sponsor'] = "<%=__sessionUser.getUserId() %>";
//          data['dealTransType']="1";
            data['fPrdCode']=fPrdCode;
            data['bondDetailsList'] = rows;
            var params = mini.encode(data);
            CommonUtil.ajax({
                url: "/IfsCfetsrmbCrController/saveCr",
                data: params,
                callback: function (data) {
                    if (data.desc.indexOf("短头寸") > -1) {
                        //短头寸仍然可以提交
                        mini.confirm(data + ",是否保存", "确认", function (action) {
                            if (action != "ok") {
                                return;
                            }
                            saveCrTrue(params);
                        });
                    } else {
                        mini.alert(data.desc, '提示信息', function () {
                            if (data.code == 'error.common.0000') {
                                top["win"].closeMenuTab();//关闭并刷新当前界面
                            }
                        });
                    }
                }
            });
        }
    }

    //不校验短头寸保存
    function saveCrTrue(params) {
        CommonUtil.ajax({
            url: "/IfsCfetsrmbCrController/saveCrTrue",
            data: params,
            callback: function (data) {
                mini.alert(data.desc, '提示信息', function () {
                    if (data.code == 'error.common.0000') {
                        top["win"].closeMenuTab();//关闭并刷新当前界面
                    }
                });
            }
        });
    }

    //文、数字、下划线 的验证
    function onEnglishAndNumberValidations(e) {
        if (e.value == "" || e.value == null) {//值为空，就不做校验
            return;
        }
        if (e.isValid) {
            if (isEnglishAndNumbers(e.value) == false) {
                e.errorText = "必须输入英文小写+数字";
                e.isValid = false;
            }
        }
    }

    /* 是否英文+数字 */
    function isEnglishAndNumbers(v) {
        var re = new RegExp("^[0-9a-z\_\-]+$");
        if (re.test(v)) return true;
        return false;
    }

    //英文、数字、下划线 的验证
    function onEnglishAndNumberValidation(e) {
        if (e.value == "" || e.value == null) {//值为空，就不做校验
            return;
        }
        if (e.isValid) {
            if (isEnglishAndNumber(e.value) == false) {
                e.errorText = "必须输入英文+数字";
                e.isValid = false;
            }
        }
    }

    /* 是否英文+数字 */
    function isEnglishAndNumber(v) {
        var re = new RegExp("^[0-9a-zA-Z\_\-]+$");
        if (re.test(v)) return true;
        return false;
    }

    function close() {
        top["win"].closeMenuTab();
    }

    function loadInstitutionTree() {
        CommonUtil.ajax({
            data: {"branchId": branchId},
            url: "/InstitutionController/searchInstitutions",
            callback: function (data) {
                mini.get("attributionDept").setData(data.obj);
                if (detailData != null && detailData.attributionDept != null) {
                    mini.get("attributionDept").setValue(detailData.attributionDept);
                } else {
                    mini.get("attributionDept").setValue("<%=__sessionInstitution.getInstId()%>");
                }
            }
        });
    }

    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
            debugger;
            if ($.inArray(action, ["approve", "detail", "print"]) > -1) {
                mini.get("save_btn").setVisible(false);
                form.setEnabled(false);
                var approForm = new mini.Form("#approve_operate_form");
                approForm.setEnabled(true);
                mini.get("forDate").setEnabled(false);
                mini.get("toolbar").setVisible(false);
                mini.get("bondCode").setVisible(false);
                mini.get("bondName").setVisible(false);
                mini.get("totalFaceValue").setVisible(false);
                mini.get("underlyingStipType").setVisible(false);

                mini.get("prodb").setVisible(false);
                mini.get("portb").setVisible(false);
                mini.get("invtype").setVisible(false);
                mini.get("prodtypeb").setVisible(false);
                mini.get("costb").setVisible(false);
                mini.get("note").setVisible(false);
            }
            if ($.inArray(action, ["add", "edit"]) > -1) {
                mini.get("toolbar").setVisible(true);
                mini.get("bondCode").setVisible(true);
                mini.get("bondName").setVisible(true);
                mini.get("totalFaceValue").setVisible(true);
                mini.get("underlyingStipType").setVisible(true);

                mini.get("creditCno").setRequired(false);
                mini.get("creditWeight").setRequired(false);
                mini.get("creditType").setRequired(false);
            }
            if ($.inArray(action, ["edit", "approve", "detail"]) > -1) {//修改、审批、详情
                form.setData(row);
                initGrid();
                mini.get("positiveInst").setValue(row.positiveInst);
                mini.get("forDate").setValue(row.forDate);
                mini.get("ticketId").setValue(row.ticketId);
                //投资组合
                //mini.get("port").setValue(row.port);
                //mini.get("port").setText(row.port);
                //成本中心
                mini.get("cost").setValue(row.cost);
                mini.get("cost").setText(row.cost);
                //产品代码
                mini.get("product").setValue(row.product);
                mini.get("product").setText(row.product);
                //产品类型
                mini.get("prodType").setValue(row.prodType);
                mini.get("prodType").setText(row.prodType);
                //对方机构
                mini.get("reverseInst").setValue(row.cno);
//          mini.get("reverseInst").setText(row.reverseInst);
                queryTextName(row.cno);
                mini.get("positiveInst").setValue(row.positiveInst);
                mini.get("positiveTrader").setValue(row.myUserName);
                mini.get("contractId").setEnabled(false);

                //清算路径
                mini.get("ccysmeans").setValue(row.ccysmeans);
                mini.get("ccysmeans").setText(row.ccysmeans);
                mini.get("ctrsmeans").setValue(row.ctrsmeans);
                mini.get("ctrsmeans").setText(row.ctrsmeans);

                mini.get("positiveCustname").setValue(row.positiveCustname);
                mini.get("positiveCustname").setText(row.positiveCustname);
                mini.get("reverseCustname").setValue(row.reverseCustname);
                mini.get("reverseCustname").setText(row.reverseCustname);

                mini.get("applyNo").setValue(row.applyNo);
                mini.get("applyNo").setText(row.applyNo);
                queryCustNo(row.custNo);
                mini.get("panel1").setVisible(false);
                dirVerify();

                if (action == "edit" && row.weight == null) {
                    searchProductWeightNew();
                }
                if (action == "edit") {//偏离度
                    if (CommonUtil.isNull(row.priceDeviation)) {
                        mini.get("tradingProduct").setValue(row.tradingProduct);
                        mini.get("tradingProduct").setText(row.tradingProduct);
                        mini.get("repoRate").setText(row.repoRate);
                        mini.get("repoRate").setValue(row.repoRate);
                        searchPriceDeviation();
                    }
                }
            } else {//增加

                //设置对手方机构
                mini.get("reverseInst").setValue("888882");
                mini.get("reverseInst").setText("交易所默认客户");

                mini.get("sponsor").setValue("<%=__sessionUser.getUserId()%>");
                mini.get("sponInst").setValue("<%=__sessionUser.getInstId() %>");
                mini.get("positiveInst").setValue("<%=__sessionInstitution.getInstFullname()%>");
                mini.get("positiveTrader").setValue("<%=__sessionUser.getUserName() %>");
                mini.get("forDate").setValue("<%=__bizDate %>");
                mini.get("basis").select(1);

                mini.get("invtype").setText("T");
                mini.get("invtype").setValue("T");

                //券投资组合
                mini.get("portb").setText("ZQJY");
                mini.get("portb").setValue("ZQJY");

                mini.get("costb").setValue("8400000000");
                mini.get("costb").setText("8400000000");

                //产品代码
                mini.get("product").setValue("REPO");
                mini.get("product").setText("REPO");

                mini.get("myDir").setValue("P");
                dirVerify();

                mini.get("creditCno").setRequired(false);
                mini.get("creditWeight").setRequired(false);
                mini.get("creditType").setRequired(false);
            }
            if ($.inArray(action, ["print"]) > -1) {//打印
                var dealTransType = mini.get("dealTransType").getValue();
                mini.get("ticketId").setValue(ticketId);
                CommonUtil.ajax({
                    url: '/IfsCfetsrmbCrController/searchCfetsrmbCrById',
                    data: {"dealTransType": dealTransType, "ticketId": ticketId},
                    callback: function (data) {
                        form.setData(data.obj);
                        mini.get("reverseInst").setValue(data.obj.reverseInst);
                        mini.get("reverseInst").setText(data.obj.reverseInst);
                        mini.get("product").setValue(data.obj.product);
                        mini.get("product").setText(data.obj.product);
                        mini.get("prodType").setValue(data.obj.prodType);
                        mini.get("prodType").setText(data.obj.prodType);
                        mini.get("cost").setValue(data.obj.cost);
                        mini.get("cost").setText(data.obj.cost);
                        mini.get("positiveCustname").setValue(data.obj.positiveCustname);
                        mini.get("positiveCustname").setText(data.obj.positiveCustname);
                        mini.get("reverseCustname").setValue(data.obj.reverseCustname);
                        mini.get("reverseCustname").setText(data.obj.reverseCustname);
                    }
                });
                $("#fileMsg").hide();
                //债券信息
                initGrid();
                //审批日志
                log_grid.show();
                CommonUtil.ajax({
                    url: "/IfsFlowController/getOneFlowDefineBaseInfo",
                    data: {serial_no: ticketId},
                    callerror: function (data) {
                        ApproveFlowLog.loadLogInfo();
                    },
                    callback: function (data) {
                        var innerInterval;
                        innerInitFunction = function () {
                            clearInterval(innerInterval);
                            ApproveFlowLog.loadLogInfo();
                        },
                            innerInterval = setInterval("innerInitFunction()", 100);
                    }
                });
            }
            mini.get("dealTransType").setEnabled(false);
            loadInstitutionTree();
        });
    });

    function onButtonEdit(e) {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Common/CustMini.jsp",
            title: "选择对手方列表",
            width: 900,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.cno);
                        btnEdit.setText(data.cliname);
                        btnEdit.focus();
                        //判断额度占用类型是否为交易对手
                        if ("2" == mini.get("quotaOccupyType").value) {
                            // queryCustNo(data.creditsub);
                            creditsubvalue(data.cno, "468", null)
                        }
                    }
                }

            }
        });
    }

    //根据交易对手编号查询全称
    function queryTextName(cno) {
        CommonUtil.ajax({
            url: "/IfsOpicsCustController/searchIfsOpicsCust",
            data: {'cno': cno},
            callback: function (data) {
                if (data && data.obj) {
                    mini.get("reverseInst").setText(data.obj.cliname);
                } else {
                    mini.get("reverseInst").setText(cno);
                }
            }
        });
    }

    function onDrawDateFirst(e) {
        var firstDate = e.date;
        var secondDate = mini.get("secondSettlementDate").getValue();
        if (CommonUtil.isNull(secondDate)) {
            return;
        }
        if (secondDate.getTime() < firstDate.getTime()) {
            e.allowSelect = false;
        }
    }

    function onDrawDateSecond(e) {
        var secondDate = e.date;
        var firstDate = mini.get("firstSettlementDate").getValue();
        if (CommonUtil.isNull(firstDate)) {
            return;
        }
        if (secondDate.getTime() < firstDate.getTime()) {
            e.allowSelect = false;
        }
    }

    function associaDataCalculation(e) {
        var firstDate = mini.get("firstSettlementDate").getValue();
        var secondDate = mini.get("secondSettlementDate").getValue();
        if (CommonUtil.isNull(firstDate) || CommonUtil.isNull(secondDate)) {
            return;
        }
        var subDate = Math.abs(parseInt((secondDate.getTime() - firstDate.getTime()) / 1000 / 3600 / 24));
        mini.get("tenor").setValue(subDate);
        mini.get("occupancyDays").setValue(subDate);
        var occupancyDays = mini.get("occupancyDays").getValue();
        var rate = mini.get("repoRate").getValue();
        var tradeAmount = mini.get("tradeAmount").getValue();
        var basis = mini.get("basis").getValue();
        var calDays = basisTonumber(basis);
        mini.get("accuredInterest").setValue(tradeAmount * rate * occupancyDays / 100 / calDays);
        mini.get("secondSettlementAmount").setValue(tradeAmount + tradeAmount * rate * occupancyDays / 100 / calDays);

        searchPriceDeviation();
        searchProductWeightNew();
    }

    //折算比例改变
    function linkageCalculatAmount(e) {
        var underlyingStip = e.value;
        var qty = mini.get("underlyingQty").getValue();
        mini.get("tradeAmount").setValue(underlyingStip * qty * 10000 / 100);
    }

    //实际占款天数
    function linkageCalculat(e) {
        var occupancyDays = e.value;
        var rate = mini.get("repoRate").getValue();
        var tradeAmount = mini.get("tradeAmount").getValue();
        var basis = mini.get("basis").getValue();
        var calDays = basisTonumber(basis);
        mini.get("accuredInterest").setValue(tradeAmount * rate * occupancyDays / 100 / calDays);
        mini.get("secondSettlementAmount").setValue(tradeAmount + tradeAmount * rate * occupancyDays / 100 / calDays);
    }

    /* function linkageCalculatRate(e){
         var rate=e.value;
         var occupancyDays=mini.get("occupancyDays").getValue();
         var tradeAmount=mini.get("tradeAmount").getValue();
         mini.get("accuredInterest").setValue(tradeAmount*rate*occupancyDays/100/365);
         mini.get("secondSettlementAmount").setValue(tradeAmount+tradeAmount*rate*occupancyDays/100/365);
    } */


    //计息基准点击事件
    function linkIntCalculat(e) {
        var basis = e.value;
        var occupancyDays = mini.get("occupancyDays").getValue();
        var rate = mini.get("repoRate").getValue();
        var tradeAmount = mini.get("tradeAmount").getValue();
        var calDays = basisTonumber(basis);
        mini.get("accuredInterest").setValue(tradeAmount * rate * occupancyDays / 100 / calDays);
        mini.get("secondSettlementAmount").setValue(tradeAmount + tradeAmount * rate * occupancyDays / 100 / calDays);
    }

    /**   清算路径1的选择   */
    function settleMeans1() {
        var cpInstId = mini.get("reverseInst").getValue();
        if (cpInstId == null || cpInstId == "") {
            mini.alert("请先选择对方机构!");
            return;
        }
        var ccy = "CNY";
        var url = "./Ifs/opics/setaMini.jsp";
        var data = {ccy: ccy, cust: cpInstId};

        var btnEdit = this;
        mini.open({
            url: url,
            title: "选择清算路径",
            width: 700,
            height: 600,
            onload: function () {
                var iframe = this.getIFrameEl();
                iframe.contentWindow.SetData(data);
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data1 = iframe.contentWindow.GetData();
                    data1 = mini.clone(data1);    //必须
                    if (data1) {
                        mini.get("ccysmeans").setValue($.trim(data1.smeans));
                        mini.get("ccysmeans").setText($.trim(data1.smeans));
                        mini.get("ccysacct").setValue($.trim(data1.sacct));
                        btnEdit.focus();
                    }
                }
            }
        });
    }

    /**   清算路径2的选择   */
    function settleMeans2() {
        var cpInstId = mini.get("reverseInst").getValue();
        if (cpInstId == null || cpInstId == "") {
            mini.alert("请先选择对方机构!");
            return;
        }
        var ccy = "CNY";
        var url = "./Ifs/opics/setaMini.jsp";
        var data = {ccy: ccy, cust: cpInstId};

        var btnEdit = this;
        mini.open({
            url: url,
            title: "选择清算路径",
            width: 700,
            height: 600,
            onload: function () {
                var iframe = this.getIFrameEl();
                iframe.contentWindow.SetData(data);
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data1 = iframe.contentWindow.GetData();
                    data1 = mini.clone(data1);    //必须
                    if (data1) {
                        mini.get("ctrsmeans").setValue($.trim(data1.smeans));
                        mini.get("ctrsmeans").setText($.trim(data1.smeans));
                        mini.get("ctrsacct").setValue($.trim(data1.sacct));
                        btnEdit.focus();
                    }
                }

            }
        });
    }

    //托管机构选择
    function onSaccQuery(e) {
        var btnEdit = this;
        mini.open({
            url: "./Ifs/opics/saccMini.jsp",
            title: "选择托管机构列表",
            width: 700,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.accountno);
                        btnEdit.setText(data.accountno);
                        btnEdit.focus();
                    }
                }

            }
        });
    }

    /* 券的产品代码的选择 */
    function onBondPrdEdit() {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "/opics/prodMiniLess.jsp",
            title: "选择产品代码",
            width: 700,
            height: 500,
            onload: function () {
                var iframe = this.getIFrameEl();
                iframe.contentWindow.SetData({});
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.pcode);
                        btnEdit.setText(data.pcode);
                        onBondPrdChanged();
                        btnEdit.focus();
                    }
                }

            }
        });
    }

    function onBondPrdChanged() {
        if (mini.get("prodtypeb").getValue() != "") {
            mini.get("prodtypeb").setValue("");
            mini.get("prodtypeb").setText("");
            mini.alert("产品代码已改变，请重新选择产品类型!");
        }
    }

    /* 券产品类型的选择 */
    function onBondTypeEdit() {
        var prd = mini.get("prodb").getValue();
        if (prd == null || prd == "") {
            mini.alert("请先选择产品代码!");
            return;
        }
        var data = {prd: prd};
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "/opics/typeMiniLess.jsp",
            title: "选择产品类型",
            width: 700,
            height: 500,
            onload: function () {
                var iframe = this.getIFrameEl();
                iframe.contentWindow.SetData(data);
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.type);
                        btnEdit.setText(data.type);
                        btnEdit.focus();
                    }
                }

            }
        });
    }

    /* 成功中心的选择 */
    function onBondCostEdit() {
        var btnEdit = this;
        var data = {prdName: "SECUR"};
        mini.open({
            url: CommonUtil.baseWebPath() + "/opics/costMiniLess.jsp",
            title: "选择成本中心",
            width: 700,
            height: 500,
            onload: function () {
                var iframe = this.getIFrameEl();
                iframe.contentWindow.SetData(data);
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.costcent);
                        btnEdit.setText(data.costcent);
                        btnEdit.focus();
                    }
                }

            }
        });
    }

    function dirVerify(e) {
        var myDir = mini.get("myDir").getValue();
        if (myDir == 'S') {//逆回购
            mini.get("panel1").setVisible(false);
            mini.get("panel12").setVisible(false);
            mini.get("quotaOccupyType").setVisible(false);
            mini.get("oppoDir").setValue("P");
            mini.get("applyNo").setVisible(false);
            mini.get("applyProd").setVisible(false);
            mini.get("custNo").setVisible(false);
            mini.get("weight").setVisible(false);
            mini.get("reverseTrader").setStyle("margin-bottom: 2px");
            <%-- mini.get("custNo").setValue("<%=__sessionUser.getInstId() %>");//占用授信主体
            mini.get("custNo").setText("<%=__sessionInstitution.getInstFullname()%>");//占用授信主体 --%>
        } else if (myDir == 'P') {//正回购
            if ("0" != mini.get("quotaOccupyType").getValue()) {
                mini.get("panel1").setVisible(true);
                mini.get("panel12").setVisible(true);
            }
            mini.get("quotaOccupyType").setVisible(true);
            mini.get("oppoDir").setValue("S");
            mini.get("applyNo").setVisible(false);
            mini.get("applyProd").setVisible(false);
            mini.get("custNo").setVisible(true);
            mini.get("weight").setVisible(true);
            mini.get("reverseTrader").setStyle("margin-bottom: 36px");
            /* mini.get("custNo").setValue(mini.get("sellInst").getValue());//产品名称
            mini.get("custNo").setValue(mini.get("sellInst").getText());//产品名称 */

            var bondCode = mini.get("bondCode").getValue();
            creditsubvalue(null, "468", bondCode);
        }
    }

    /* 成功中心的选择 */
    //    function onCostEdit(){
    //        var btnEdit = this;
    //        mini.open({
    //            url: CommonUtil.baseWebPath() + "/opics/costMiniLess.jsp",
    //            title: "选择成本中心",
    //            width: 700,
    //            height: 500,
    //            ondestroy: function (action) {
    //                if (action == "ok") {
    //                    var iframe = this.getIFrameEl();
    //                    var data = iframe.contentWindow.GetData();
    //                    data = mini.clone(data);    //必须
    //                    if (data) {
    //                        btnEdit.setValue(data.costcent);
    //                        btnEdit.setText(data.costcent);
    //                        //searchMarketData();
    //                        btnEdit.focus();
    //                    }
    //                }
    //            }
    //        });
    //    }

    function searchMarketData() {
        var costCent = mini.get("cost").getValue();//成本中心
        var code = mini.get("invtype").getValue();//交易品种
        if (CommonUtil.isNull(code) || CommonUtil.isNull(costCent)) {
            return;
        }
        queryMarketData(code, costCent);
    }

    //获取DV01，久期限，止损值查询
    function queryMarketData(code, costCent) {
        CommonUtil.ajax({
            url: "/IfsOpicsCustController/queryMarketData",
            data: {'code': code, 'costCent': costCent},
            callback: function (data) {
                if (data && data.obj) {
                    mini.get("longLimit").setValue(data.obj.duration);//久期限
                    mini.get("lossLimit").setValue(data.obj.convexity);//止损
                    mini.get("DV01Risk").setValue(data.obj.delta);//DV01
                } else {
                    mini.get("longLimit").setValue();//久期限
                    mini.get("lossLimit").setValue();//止损
                    mini.get("DV01Risk").setValue();//DV01
                }
            }
        });
    }

    //获取权重
    function searchProductWeightNew() {
        var form = new mini.Form("field_form");
        var data = form.getData(true);
        var mDate = data.secondSettlementDate;
        var vDate = data.forDate;//交易日期
        if (CommonUtil.isNull(mDate) || CommonUtil.isNull(vDate)) {
            return;
        }
        searchProductWeight(20, 0, prdNo, mDate);
    }

    function searchPriceDeviation() {//偏离度
        var tradingProduct = mini.get("tradingProduct").getValue();//交易品种
        var repoRate = mini.get("repoRate").getValue();//回购利率
        if (CommonUtil.isNull(tradingProduct) || CommonUtil.isNull(repoRate)) {
            return;
        }
        CommonUtil.ajax({
            url: "/IfsLimitController/queryRhisIntrate",
            data: {'prdNo': prdNo, 'tradingProduct': tradingProduct, 'repoRate': repoRate},
            callback: function (data) {
                if (data != null && data != "") {
                    mini.get("priceDeviation").setValue(data);
                } else {
                    mini.get("priceDeviation").setValue();
                }
            }
        });
    }

    var ApproveFlowLog = {};
    var log_grid = mini.get("log_grid");//隐藏日志信息列表，打印时显示
    log_grid.hide();
    /**
     *   获取审批日志信息
     */
    ApproveFlowLog.loadLogInfo = function () {
        CommonUtil.ajax({
            url: "/IfsFlowController/approveLog",
            data: {'serial_no': ticketId},
            callback: function (data) {
                if (data != null) {
                    log_grid.setData(data.obj);
                    if (typeof (FlowDesigner) != "undefined") {
                        FlowDesigner.addApporveLog(data.obj);
                    }
                }
            }
        });
    };

</script>
<script type="text/javascript" src="../../Common/Flow/MiniApproveOpCommon.js"></script>
</body>
</html>
