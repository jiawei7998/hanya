<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
<title>存单发行</title>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
	<legend>存单发行成交单查询</legend>	
	<div>
		<div id="search_form" style="width:100%" cols="6">
			<input id="startDate" name="startDate" class="mini-datepicker" labelField="true"  label="成交日期："
				   ondrawdate="onDrawDateStart" format="yyyy-MM-dd" value="<%=__bizDate%>"  labelStyle="text-align:right;" emptyText="开始日期" />
			<span>~</span>
			<input id="endDate" name="endDate" class="mini-datepicker"
				   ondrawdate="onDrawDateEnd" emptyText="结束日期" format="yyyy-MM-dd" value="<%=__bizDate%>"/>
			<input id="contractId" name="contractId" class="mini-textbox" labelField="true"  label="成交单编号：" width="280px" labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请输入成交单编号" />
			<input id="ticketId" name="ticketId" class="mini-textbox" labelField="true" label="审批单号：
" labelStyle="text-align:right;" emptyText="请填写审批单号" width="280px" />
			<input id="dealNo" name="dealNo" class="mini-textbox" labelField="true" visible="false"
				   label="opics交易号：" emptyText="请填写opics交易号" labelStyle="text-align:right;" width="280px"/>
			<input id="approveStatus" name="approveStatus" class="mini-combobox" data="CommonUtil.serverData.dictionary.ApproveSta" width="280px" emptyText="请选择审批状态" labelField="true"  label="审批状态：" labelStyle="text-align:right;"/>
			<span style="float: right; margin-right: 150px"> 
				<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
			</span>
		</div>
	</div>
</fieldset> 
<%@ include file="../batchReback.jsp"%>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script> 
<div class="mini-fit" >
	<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:70%;" idField="id"  allowAlternating="true" allowResize="true" sortMode="client" allowAlternating="true" multiSelect="true" onrowdblclick="onRowDblClick">
		<div property="columns">
		<div type="checkcolumn"></div>
			<div type="indexcolumn" width="50px" headerAlign="center">序号</div>
			<div field="prdNo" width="100px" allowSort="false" headerAlign="center" align="center" visible="false"></div>
			<div field="taskName" width="120px" allowSort="false" headerAlign="center" align="center">审批状态</div>
			<div field="contractId" width="160px" align="center"  headerAlign="center" >成交单编号</div>
			<div field="ticketId" width="160px" align="center"  headerAlign="center" >审批单编号</div> 
			<div field="dealNo" width="160px" align="center"  headerAlign="center" visible="false">opics交易号</div>
			<div field="statcode" width="160px" align="center"  headerAlign="center" visible="false">opics处理状态</div>
			<div field="forDate" width="100px" allowSort="false" headerAlign="center" align="center">成交日期</div>
			<div field="depositCode" width="100px" align="center"  headerAlign="center" >存单代码</div>
			<div field="depositName" width="200px" align="center"  headerAlign="center" >存单简称</div>
			<div field="depositTerm" width="100px" align="center"  headerAlign="center" >存单期限</div>
			<div field="interestType" width="100px" align="center"  headerAlign="center" >息票类型</div>
			<div field="offerAmount" width="140px" align="right"  headerAlign="center" numberFormat="n4">认购量总额</div>
			<div field="shouldPayMoney" width="140px" align="right"  headerAlign="center" numberFormat="n4">应缴金额总额</div>
			<div field="publishDate" width="100px" align="center"  headerAlign="center" allowSort="true">发行日</div>
			<div field="valueDate" width="100px" align="center"  headerAlign="center" allowSort="true">起息日</div>
			<div field="duedate" width="100px" align="center"  headerAlign="center" allowSort="true">到期日</div>
			<div field="publishPrice" width="100px" align="center"  headerAlign="center" allowSort="true" numberFormat="n4">发行价格</div>
			<div field="benchmarkcurvename" width="100px" align="center"  headerAlign="center" allowSort="true" numberFormat="n6">参考收益率(%)</div>
			<div field="basis" width="100px" align="center"  headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'Basis'}" allowSort="true">计息基准</div>
			<div field="sponsor" width="100" allowSort="false" headerAlign="center" align="center">审批发起人</div>
			<div field="sponInst" width="120" allowSort="false" headerAlign="center" align="center">审批发起机构</div>
			<div field="aDate" width="160" allowSort="false" headerAlign="center" align="center">审批发起时间</div>	
		</div>
	</div>
	
	<fieldset style="width: 100%;border:solid 1px #aaa;margin-top:8px;position:relative;">
		<legend>存单发行详情</legend>
			<div id="MiniSettleForeigDetail" style="padding:5px;" class="mini-fit area" tyle="width: 100%; height: 50%;" allowAlternating="true" allowResize="true" border="true" sortMode="client">
			<input class="mini-hidden" name="id"/>
			<div class="mini-panel" title="成交单编号" style="width:100%;"  allowResize="true" collapseOnTitleClick="true">
				<div class="leftarea">
					<input style="width:100%;" id="forDate" name="forDate" class="mini-datepicker mini-mustFill" labelField="true" required="true"  label="成交日期：" labelStyle="text-align:left;width:130px;" />
				</div>
				<div class="rightarea">
					<input style="width:100%;" id="contractId" name="contractId" class="mini-textbox mini-mustFill" labelField="true" requiredErrorText="该输入项为必输项" required="true"  label="成交单编号：" labelStyle="text-align:left;width:130px;" onvalidation="onEnglishAndNumberValidations" vtype="maxLength:20"/>
				</div>
				
			</div>
				<%@ include file="../../Common/opicsDetailDepositPublish.jsp"%>
				<div class="mini-panel" title="交易主体信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
					<input style="width:100%" id="depositAllName" name="depositAllName" class="mini-textbox" labelField="true"  label="存单全称：" required="true"  onvalidation="onEnglishAndNumberValidation" vtype="maxLength:20"   labelStyle="text-align:left;width:130px;" />
				<div class="leftarea">
					<input style="width:100%" id="depositName" name="depositName" class="mini-textbox" labelField="true"  label="存单简称：" required="true"  onvalidation="onEnglishAndNumberValidation" vtype="maxLength:20"   labelStyle="text-align:left;width:130px;" />
					<input style="width:100%" id="depositTerm" name="depositTerm" class="mini-textbox" labelField="true"  label="存单期限：" required="true"  vtype="maxLength:10"   labelStyle="text-align:left;width:130px;"   />
					<input id="interestType" name="interestType" class="mini-combobox mini-mustFill"  labelField="true"   label="息票类型："  style="width:100%;"  labelStyle="text-align:left;width:130px;" data = "CommonUtil.serverData.dictionary.invtype" />
					<input style="width:100%" id="planAmount" name="planAmount" class="mini-spinner" labelField="true"  label="计划发行量(亿元)：" required="true"  onvaluechanged="onAccuredInterestChanged" minValue="0" maxValue="9999999999999999.9999" format="n4" changeOnMousewheel="false"  labelStyle="text-align:left;width:130px;"   />
					<input style="width:100%" id="publishPrice" name="publishPrice" class="mini-spinner" labelField="true"  label="发行价格(元)：" required="true"   minValue="0" maxValue="9999999999999999.99999999" format="n8" changeOnMousewheel="false"  labelStyle="text-align:left;width:130px;"   />
					<input style="width:100%" id="valueDate" name="valueDate" class="mini-spinner" labelField="true"  label="起息日(缴款日期)：" required="true"  minValue="0" maxValue="9999999999999999.9999" format="n4" changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"   />
					<input style="width:100%" id="currency" name="currency" class="mini-spinner" labelField="true"  label="币种：" required="true"  onvaluechanged="onAccuredInterestChanged" minValue="0" maxValue="9999999999999999.9999" format="n4" changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"   />
				</div>
				<div class="rightarea">
					<input style="width:100%;" id="depositCode" name="depositCode" class="mini-textbox mini-mustFill" labelField="true"  label="存单代码：" required="true"  vtype="maxLength:10"   labelStyle="text-align:left;width:130px;"  enabled="false" />
					<input style="width:100%" id="termType" name="termType" class="mini-textbox" labelField="true"  label="期限类型：" required="true"  vtype="maxLength:10"   labelStyle="text-align:left;width:130px;"   />
					<input style="width:100%" id="publishDate" name="publishDate" class="mini-spinner" labelField="true"  label="发行日期："  required="true"  onvaluechanged="onAccuredInterestChanged"   minValue="0" maxValue="9999999999999999.9999"  changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"   />
					<input style="width:100%" id="actualAmount" name="actualAmount" class="mini-spinner" labelField="true"   label="实际发行量(亿元)："   required="true"  onvaluechanged="calSettleAmount"  minValue="0" maxValue="9999999999999999.9999" format="n4" changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"   />
					<input style="width:100%" id="benchmarkCurveName" name="benchmarkCurveName" class="mini-spinner" labelField="true"  label="参考收益率(%)："  required="true"  onvaluechanged="calSettleAmount" minValue="0" maxValue="9999999999999999.9999" format="n4" changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"   />
					<input style="width:100%" id="dueDate" name="dueDate" class="mini-combobox" labelField="true"  label="到期日："  required="true"  data = "CommonUtil.serverData.dictionary.SettlementMethod"  labelStyle="text-align:left;width:130px;"   />
				</div>
			</div>
			<div class="centerarea" style="height:150px;">
			<div class="mini-fit" >
				<div id="datagrid1" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id"  allowAlternating="true"
					fitColumns="false" allowResize="true" sortMode="client" allowAlternating="true" showpager="false">
					<div property="columns">
						<div type="indexcolumn" width="50px" headerAlign="center">序号</div>
						<div field="dealNo" width="100px" align="center"  headerAlign="center" >opics编号</div>
						<div field="lendInst" width="100px" align="right" headerAlign="center">机构</div>
						<div field="lendInstName" width="200px" align="left" headerAlign="center">机构中文名</div>
			            <div field="lendTrader" width="100px" align="center" headerAlign="center"  >认购人</div>
						<div field="lendTel" width="100px" align="right" headerAlign="center">电话</div>
						<div field="lendFax" width="100px" align="right" headerAlign="center">传真</div>
						<div field="lendCorp" width="100px" align="right" headerAlign="center">法人代表</div>
						<div field="lendAddr" width="100px" align="right" headerAlign="center">地址</div>
						<div field="offerAmount" width="100px" align="right" headerAlign="center">认购量(亿元)</div>
						<div field="shouldPayMoney" width="100px" align="right" headerAlign="center">应缴纳金额(元)</div>
						<div field="lendOpbank" width="100px" align="right" headerAlign="center">资金开户行</div>
						<div field="lendAccnum" width="100px" align="right" headerAlign="center">资金账号</div>
						<div field="lendPsnum" width="100px" align="right" headerAlign="center">支付系统行号</div>
						<div field="lendCaname" width="100px" align="right" headerAlign="center">托管账户户名</div>
						<div field="lendCustname" width="100px" align="right" headerAlign="center">托管机构</div>
						<div field="lendCanum" width="100px" align="right" headerAlign="center">托管帐号</div>
					</div>
				</div>
			</div> 
			</div>
			<div class="mini-panel" title="清算信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
				<fieldset>
					<legend>发行方账户</legend>
					<div class="leftarea">
						<input id="sellOpbank" name="sellOpbank" class="mini-textbox" labelField="true"  label="资金开户行："  vtype="maxLength:1333"  labelStyle="text-align:left;width:120px;" style="width:100%;"   />
						<input id="sellPsnum" name="sellPsnum" class="mini-textbox" labelField="true"  label="支付系统行号："   onvalidation="onEnglishAndNumberValidation" vtype="maxLength:400" labelStyle="text-align:left;width:120px;" style="width:100%;"   />
						<input id="sellCustname" name="sellCustname" class="mini-textbox" labelField="true"  label="托管机构：" vtype="maxLength:1333"   labelStyle="text-align:left;width:120px;" style="width:100%;"   />
					</div>
					<div class="rightarea">
						<input id="sellAccnum" name="sellAccnum" class="mini-textbox" labelField="true"  label="资金账号："  onvalidation="onEnglishAndNumberValidation" vtype="maxLength:400" labelStyle="text-align:left;width:120px;" style="width:100%;"   />
						<input id="sellCaname" name="sellCaname" class="mini-textbox" labelField="true"  label="托管账户户名：" vtype="maxLength:1333"   labelStyle="text-align:left;width:120px;" style="width:100%;"   />
						<input id="sellCanum" name="sellCanum" class="mini-textbox" labelField="true"  label="托管帐号："   onvalidation="onEnglishAndNumberValidation" vtype="maxLength:400" labelStyle="text-align:left;width:120px;" style="width:100%;"   />
					</div>
				</fieldset>
			
			
			</div>
		</div>
	</fieldset>
</div>
<script>
	mini.parse();

	var url = window.location.search;
	var prdNo = CommonUtil.getParam(url, "prdNo");
	var fPrdCode = CommonUtil.getParam(url, "fPrdCode");
	var prdName = CommonUtil.getParam(url, "prdName");
	var form = new mini.Form("#search_form");
	var grid=mini.get("datagrid");
	var grid1=mini.get("datagrid1");
	var row="";
	
	/**************************************点击下面显示详情开始******************************/
	var from = new mini.Form("MiniSettleForeigDetail");
	from.setEnabled(false);
	var grid = mini.get("datagrid");
	//grid.load();
	 //绑定表单
    var db = new mini.DataBinding();
    db.bindForm("MiniSettleForeigDetail", grid);
	$(document).ready(function () {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn){
			initButton();
			search(10,0);
		});
	});
    /**************************************点击下面显示详情结束******************************/
	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});
	//成交日期
	function onDrawDateStart(e) {
		var startDate = e.date;
		var endDate= mini.get("endDate").getValue();
		if(CommonUtil.isNull(endDate)){
			return;
		}
		if (endDate.getTime() < startDate.getTime()) {
			e.allowSelect = false;
		}
	}
	function onDrawDateEnd(e) {
		var endDate = e.date;
		var startDate = mini.get("startDate").getValue();
		if(CommonUtil.isNull(startDate)){
			return;
		}
		if (endDate.getTime() < startDate.getTime()) {
			e.allowSelect = false;
		}
	}
	//查看详情
	function onRowDblClick(e) {
		var url = CommonUtil.baseWebPath() +"/cfetsrmb/cdPublishEdit.jsp?action=detail&prdNo="+prdNo+"&fPrdCode="+fPrdCode;
		var tab = {id: "cdDetail",name:"cdDetail",url:url,title:"存单发行成交单详情",
					parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
		var paramData = {selectData:grid.getSelected()};
		CommonUtil.openNewMenuTab(tab,paramData);
	}
	
	//清空
	function clear(){
		var MiniSettleForeigDetail=new mini.Form("MiniSettleForeigDetail");
		MiniSettleForeigDetail.clear();
		var form=new mini.Form("search_form");
		form.clear();
		query();
	}
	
	function getData(action) {
		var row = null;
		if (action != "add") {
			row = grid.getSelected();
		}
		return row;
	}
	
	//添加
	function add(){
		var url = CommonUtil.baseWebPath() +"/cfetsrmb/cdPublishEdit.jsp?action=add&prdNo="+prdNo+"&fPrdCode="+fPrdCode;
		var tab = {id: "cdAdd",name:"cdAdd",url:url,title:"存单发行成交单补录",
					parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
		var paramData = {selectData:""};
		CommonUtil.openNewMenuTab(tab,paramData);
	}
	
	//修改
	function edit(){
		var row=grid.getSelected();
		if(row){
		var url = CommonUtil.baseWebPath() +"/cfetsrmb/cdPublishEdit.jsp?action=edit&prdNo="+prdNo+"&fPrdCode="+fPrdCode;
		var tab = {id: "cdEdit",name:"cdEdit",url:url,title:"存单发行成交单修改",
					parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
		var paramData = {selectData:row};
		CommonUtil.openNewMenuTab(tab,paramData);
		}else{
			mini.alert("请选择一条记录","提示");
		}
	}
	
	//删除
	function del(){
		var rows=grid.getSelecteds();
		if(rows.length==0){
			mini.alert("请选中一行","提示");
			return;
		}
		if(rows.length>1){
			mini.alert("系统不支持多条数据进行删除","消息提示");
			return;
		}
		mini.confirm("您确认要删除选中记录?","系统警告",function(value){   
			if (value=='ok'){   
				var data=rows[0];
				params=mini.encode(data);
				CommonUtil.ajax( {
					url:"/IfsCfetsrmbDpController/deleteCfetsrmbDp",
					data:params,
					callback : function(data) {
						mini.alert("删除成功.","系统提示");
						query();
					}
				});
			}
		});
	}
	
	//初始化债券认购人信息
   function initGrid(ticketId){
		var url="/IfsCfetsrmbDpController/searchOfferDpDetails";
		var data={};
		data['ticketId']=ticketId;
		var params=mini.encode(data);
		CommonUtil.ajax({
			url:url,
			data:params,
			callback : function(data) {
				grid1.setData(data.obj.rows);
			}
		});
	}
	
	grid.on("select",function(e){
		var row=e.record;
		initGrid(row.ticketId);
		mini.get("approve_mine_commit_btn").setEnabled(false);
		mini.get("approve_commit_btn").setEnabled(false);
		mini.get("edit_btn").setEnabled(false);
		mini.get("delete_btn").setEnabled(false);
		mini.get("approve_log").setEnabled(true);
		mini.get("recall_btn").setEnabled(true);
		mini.get("batch_commit_btn").setEnabled(true);
		if(row.approveStatus == "3"){//新建
			mini.get("edit_btn").setEnabled(true);
			mini.get("delete_btn").setEnabled(true);
			mini.get("approve_mine_commit_btn").setEnabled(true);
			mini.get("approve_commit_btn").setEnabled(false);
			mini.get("reback_btn").setEnabled(false);
			mini.get("recall_btn").setEnabled(false);
		}
		if( row.approveStatus == "6" || row.approveStatus == "7"){//审批通过：6    审批拒绝:5
			mini.get("edit_btn").setEnabled(false);
			mini.get("delete_btn").setEnabled(false);
			mini.get("approve_mine_commit_btn").setEnabled(false);
			mini.get("approve_commit_btn").setEnabled(true);
			mini.get("batch_approve_btn").setEnabled(false);
			mini.get("batch_commit_btn").setEnabled(false);
			mini.get("opics_check_btn").setEnabled(false);
			mini.get("reback_btn").setEnabled(false);
			mini.get("recall_btn").setEnabled(false);
		}
		if(row.approveStatus == "5"){//审批中:5
			mini.get("edit_btn").setEnabled(false);
			mini.get("delete_btn").setEnabled(false);
			mini.get("approve_mine_commit_btn").setEnabled(false);
			mini.get("approve_commit_btn").setEnabled(true);
			mini.get("batch_commit_btn").setEnabled(false);
			mini.get("reback_btn").setEnabled(true);
			mini.get("recall_btn").setEnabled(true);
		}
		
	});

	/* 查询 按钮事件 */
	function query(){
		search(grid.pageSize,0);
	}
	
	/* 查询 */
	function search(pageSize,pageIndex){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统也提示");
			return;
		}

		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		data['instrumentType']="CD";
		data['fPrdCode']=fPrdCode;
		var url=null;

		var approveType = mini.get("approveType").getValue();
		if(approveType == "mine"){
			url = "/IfsCfetsrmbDpController/searchRmbDpSwapMine";
		}else if(approveType == "approve"){
			url = "/IfsCfetsrmbDpController/searchPageRmbDpUnfinished";
		}else{
			url = "/IfsCfetsrmbDpController/searchPageRmbDpFinished";
		}
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:url,
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	
	/**************************审批相关****************************/
	//审批日志查看
	function appLog(selections){
		var flow_type = Approve.FlowType.VerifyApproveFlow;
		if(selections.length <= 0){
			mini.alert("请选择要操作的数据","系统提示");
			return;
		}
		if(selections.length > 1){
			mini.alert("系统不支持多笔操作","系统提示");
			return;
		}
		if(selections[0].tradeSource == "3"){
			mini.alert("初始化导入的业务没有审批日志","系统提示");
			return false;
		}
		Approve.approveLog(flow_type,selections[0].ticketId);
	};
	
	//提交正式审批、待审批
	function verify(selections){
		if(selections.length == 0){
			mini.alert("请选中一条记录！","消息提示");
			return false;
		}else if(selections.length > 1){
			mini.alert("该功能不支持多笔提交，如需多笔提交请点击[批量提交]按钮","系统提示");
			return false;
		}
		if(selections[0]["approveStatus"] != "3" ){
			var url = CommonUtil.baseWebPath() +"/cfetsrmb/cdPublishEdit.jsp?action=approve&ticketId="+row.ticketId+"&fPrdCode="+fPrdCode;
			var tab = {"id": "cdPublishApprove",name:"cdPublishApprove",url:url,title:"存单发行审批",
						parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
			var paramData = {selectData:selections[0]};
			CommonUtil.openNewMenuTab(tab,paramData);
			// top["win"].showTab(tab);
		}else{
			Approve.approveCommit(Approve.FlowType.VerifyApproveFlow,selections[0]["ticketId"],Approve.OrderStatus.New,"IfsRmbDpService",prdNo,function(){
				search(grid.pageSize,grid.pageIndex);
			});
		}
	};
	
	//审批
	function approve(){
		mini.get("approve_commit_btn").setEnabled(false);
		var messageid = mini.loading("系统正在处理...", "请稍后");
		try {
			verify(grid.getSelecteds());
		} catch (error) {
			
		}
		mini.hideMessageBox(messageid);	
		mini.get("approve_commit_btn").setEnabled(true);
	}
	
	//提交审批
	function commit(){
		mini.get("approve_mine_commit_btn").setEnabled(false);
		var messageid = mini.loading("系统正在处理...", "请稍后");
		try {
			verify(grid.getSelecteds());
		} catch (error) {
			
		}
		mini.hideMessageBox(messageid);	
		mini.get("approve_mine_commit_btn").setEnabled(true);
	}
	
	//审批日志
	function searchlog(){
		appLog(grid.getSelecteds());
	}

	//打印
	function print(){
		var selections = grid.getSelecteds();
		if(selections == null || selections.length == 0){
			mini.alert('请选择一条要打印的数据！','系统提示');
			return false;
		}else if(selections.length>1){
			mini.alert('暂不支持多条数据同时打印，请选择一条要打印的数据！','系统提示');
			return false;
		}
		var canPrint = selections[0].ticketId;

		if(!CommonUtil.isNull(canPrint)){
			var actionStr = CommonUtil.pPath + "/sl/IfsPrintUtilController/exportload/print/"+PrintNo.cdPublish+"/" + canPrint;
			$('<form target="_blank" action=' + actionStr + ' method="post"></form>').appendTo('body').submit().remove();
		};
	}

	//导出报表
	function exportExcel(){
		exportExcelManin("jycdfx.xls","jycdfx");
	}

</script>
</body>
</html>