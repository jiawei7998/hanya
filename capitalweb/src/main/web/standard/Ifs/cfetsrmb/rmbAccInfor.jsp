<%@ page language="java" pageEncoding="UTF-8"%>
<div class="mini-panel" title="清算账户信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">	
	<div class="leftarea">
			<fieldset>
				<legend>本方账户</legend>
				<input id="ownAccname" name="ownAccname" class="mini-textbox" labelField="true"  label="资金账户户名："  vtype="maxLength:50" style="width:100%;"  labelStyle="text-align:left;" />
				<input id="ownOpbank" name="ownOpbank" class="mini-textbox" labelField="true"  label="资金开户行："  vtype="maxLength:50" labelStyle="text-align:left;" style="width:100%;"   />
				<input id="ownAccnum" name="ownAccnum" class="mini-textbox" labelField="true"  label="资金账号："  vtype="maxLength:30" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" labelStyle="text-align:left;" style="width:100%;"   />
				<input id="ownPsnum" name="ownPsnum" class="mini-textbox" labelField="true"  label="支付系统行号："  vtype="maxLength:30" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" labelStyle="text-align:left;" style="width:100%;"   />
				<input id="ownCaname" name="ownCaname" class="mini-textbox" labelField="true"  label="托管账户户名："  vtype="maxLength:50" labelStyle="text-align:left;" style="width:100%;"   />
				<input id="ownCustname" name="ownCustname" class="mini-textbox" labelField="true"  label="托管机构："  vtype="maxLength:50" labelStyle="text-align:left;" style="width:100%;"   />
				<input id="ownCanum" name="ownCanum" class="mini-textbox" labelField="true"  label="托管帐号："  vtype="maxLength:30" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" labelStyle="text-align:left;" style="width:100%;"   />
			</fieldset>
		</div>
		<div class="rightarea">
			<fieldset>
				<legend>对方账户</legend>
				<input id="otherAccname" name="otherAccname" class="mini-textbox" labelField="true"  label="资金账户户名："  vtype="maxLength:50" style="width:100%;"  labelStyle="text-align:left;" />
				<input id="otherOpbank" name="otherOpbank" class="mini-textbox" labelField="true"  label="资金开户行："  vtype="maxLength:50" labelStyle="text-align:left;" style="width:100%;"   />
				<input id="otherAccnum" name="otherAccnum" class="mini-textbox" labelField="true"  label="资金账号："  vtype="maxLength:30" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" labelStyle="text-align:left;" style="width:100%;"   />
				<input id="otherPsnum" name="otherPsnum" class="mini-textbox" labelField="true"  label="支付系统行号："  vtype="maxLength:30" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" labelStyle="text-align:left;" style="width:100%;"   />
				<input id="otherCaname" name="otherCaname" class="mini-textbox" labelField="true"  label="托管账户户名："  vtype="maxLength:50" labelStyle="text-align:left;" style="width:100%;"   />
				<input id="otherCustname" name="otherCustname" class="mini-textbox" labelField="true"  label="托管机构："  vtype="maxLength:50" labelStyle="text-align:left;" style="width:100%;"   />
				<input id="otherCanum" name="otherCanum" class="mini-textbox" labelField="true"  label="托管帐号：" vtype="maxLength:30" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" labelStyle="text-align:left;" style="width:100%;"   />
			</fieldset>
		</div>

</div>

