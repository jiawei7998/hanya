<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../../global.jsp" %>
<html>
<head>
    <script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
    <title>同业借款</title>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
    <legend>同业借款成交单查询</legend>
    <div>
        <div id="search_form" style="width:100%" cols="6">
            <input id="startDate" name="startDate" class="mini-datepicker" labelField="true" label="成交日期："
                   ondrawdate="onDrawDateStart" format="yyyy-MM-dd" value="<%=__bizDate%>"
                   labelStyle="text-align:right;" emptyText="开始日期"/>
            <span>~</span>
            <input id="endDate" name="endDate" class="mini-datepicker"
                   ondrawdate="onDrawDateEnd" emptyText="结束日期" format="yyyy-MM-dd" value="<%=__bizDate%>"/>
            <input id="contractId" name="contractId" class="mini-textbox" labelField="true" label="成交单编号：" width="280px"
                   labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请输入成交编号"/>
            <input id="ticketId" name="ticketId" class="mini-textbox" labelField="true" label="审批单号：
" labelStyle="text-align:right;" emptyText="请填写审批单号" width="280px"/>
            <input id="dealNo" name="dealNo" class="mini-textbox" labelField="true"
                   label="opics交易号：" emptyText="请填写opics交易号" labelStyle="text-align:right;" width="280px"/>
            <input id="approveStatus" name="approveStatus" class="mini-combobox"
                   data="CommonUtil.serverData.dictionary.ApproveSta" width="280px" emptyText="请选择审批状态"
                   labelField="true" label="审批状态：" labelStyle="text-align:right;"/>
            <span style="float: right; margin-right: 150px"> 
                <a id="search_btn" class="mini-button" style="display: none" onclick="searchs()">查询</a>
                <a id="clear_btn" class="mini-button" style="display: none" onclick="clear()">清空</a>
            </span>
        </div>
    </div>
</fieldset>
<%@ include file="../batchReback.jsp" %>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<div class="mini-fit" style="margin-top: 2px;">
    <div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:60%;" idField="id"
         allowAlternating="true" allowResize="true" sortMode="client" allowAlternating="true" multiSelect="true"
         onrowdblclick="onRowDblClick">
        <div property="columns">
            <div type="checkcolumn"></div>
            <div type="indexcolumn" width="50px" headerAlign="center">序号</div>
            <div field="prdNo" width="100px" allowSort="false" headerAlign="center" align="center"
                 visible="false"></div>
            <div field="taskName" width="120px" allowSort="false" headerAlign="center" align="center">审批状态</div>
            <div field="contractId" width="160px" align="center" headerAlign="center">成交单编号</div>
            <div field="ticketId" width="160px" align="center" headerAlign="center">审批单编号</div>
            <div field="dealNo" width="100px" align="center" headerAlign="center">opics交易号</div>
            <div field="statcode" width="160px" align="center" headerAlign="center">opics处理状态</div>
            <div field="removeInst" width="200px" align="center" headerAlign="center">对手方机构名称</div>
            <div field="myDir" width="100" align="center" headerAlign="center" renderer="CommonUtil.dictRenderer"
                 data-options="{dict:'lending'}">本方方向
            </div>
            <div field="amt" width="140px" align="right" headerAlign="center" numberFormat="n4">拆借金额（万元）</div>
            <div field="rate" width="140px" align="right" headerAlign="center" numberFormat="n8">拆借利率</div>
            <div field="firstSettlementDate" width="100px" align="center" headerAlign="center" allowSort="true">起息日
            </div>
            <div field="secondSettlementDate" width="100px" align="center" headerAlign="center" allowSort="true">到期结算日
            </div>
            <div field="tenor" width="100px" align="center" headerAlign="center" allowSort="true">期限</div>
            <div field="occupancyDays" width="100px" align="center" headerAlign="center" allowSort="true">实际占款天数</div>
            <div field="basis" width="100px" align="center" headerAlign="center" renderer="CommonUtil.dictRenderer"
                 data-options="{dict:'Basis'}" allowSort="true">计息基准
            </div>
            <div field="intcalRule" width="100px" align="center" headerAlign="center" renderer="CommonUtil.dictRenderer"
                 data-options="{dict:'scheduleType'}" allowSort="true">付息类型
            </div>
            <div field="intcalWay" width="100px" align="center" headerAlign="center" renderer="CommonUtil.dictRenderer"
                 data-options="{dict:'intPayCycle'}" allowSort="true">付息周期
            </div>
            <div field="settlementAmount" width="140px" align="right" headerAlign="center" allowSort="true"
                 numberFormat="n4">到期结算金额（元）
            </div>
            <div field="sponsor" width="120px" allowSort="true" headerAlign="center" align="center">审批发起人</div>
            <div field="sponInst" width="120px" allowSort="true" align="center">审批发起机构</div>
            <div field="aDate" width="160px" allowSort="true" align="center">审批发起时间</div>
        </div>
    </div>
    <fieldset style="width: 100%;border:solid 1px #aaa;margin-top:8px;position:relative;">
        <legend>同业借款详情</legend>
        <div id="MiniSettleForeigDetail" style="padding:5px;" class="mini-fit area" tyle="width: 100%; height: 50%;"
             allowAlternating="true" allowResize="true" border="true" sortMode="client">
            <input class="mini-hidden" name="id"/>
            <div class="mini-panel" title="成交双方信息" style="width:100%" allowResize="true" collapseOnTitleClick="true">
                <div class="leftarea">
                    <fieldset>
                        <legend>本方信息</legend>
                        <input id="myDir" name="myDir" class="mini-combobox" labelField="true"
                               onvaluechanged="dirVerify" label="本方方向：" required="true" style="width:100%;"
                               labelStyle="text-align:left;width:120px;"
                               data="CommonUtil.serverData.dictionary.lending"/>
                        <input id="borrowInst" name="borrowInst" class="mini-textbox" labelField="true" label="机构："
                               style="width:100%;" required="true" enabled="false"
                               labelStyle="text-align:left;width:120px;"/>
                        <input id="borrowTrader" name="borrowTrader" class="mini-textbox" labelField="true" label="交易员："
                               vtype="maxLength:5" required="true" enabled="false"
                               labelStyle="text-align:left;width:120px;" style="width:100%;"/>
                    </fieldset>
                </div>
                <div class="rightarea">
                    <fieldset>
                        <legend>对手方信息</legend>
                        <input id="oppoDir" name="oppoDir" class="mini-combobox" enabled="false" labelField="true"
                               label="对方方向：" required="true" style="width:100%;"
                               labelStyle="text-align:left;width:120px;"
                               data="CommonUtil.serverData.dictionary.lending"/>
                        <input id="removeInst" name="removeInst" class="mini-textbox" labelField="true" required="true"
                               label="机构：" style="width:100%;" labelStyle="text-align:left;width:120px;"/>
                        <input id="removeTrader" name="removeTrader" class="mini-textbox" labelField="true" label="交易员："
                               required="true" vtype="maxLength:5" labelStyle="text-align:left;width:120px;"
                               style="width:100%;"/>
                    </fieldset>
                </div>
            </div>
            <div class="mini-panel" title="交易主体信息" style="width:100%" allowResize="true" collapseOnTitleClick="true">
                <div class="leftarea">
                    <input style="width:100%;" id="forDate" name="forDate" class="mini-datepicker mini-mustFill"
                           labelField="true" required="true" label="成交日期：" labelStyle="text-align:left;width:130px;"/>
                    <input style="width:100%" id="amt" name="amt" class="mini-spinner mini-mustFill input-text-strong"
                           labelField="true" label="拆借金额(万元)：" required="true" onvaluechanged="associaDataCalculation"
                           required="true" minValue="0" maxValue="9999999999.9999" format="n4"
                           changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"
                           onvalidation="zeroValidation"/>
                    <input style="width:100%" id="firstSettlementDate" name="firstSettlementDate"
                           class="mini-datepicker mini-mustFill" labelField="true" required="true" label="首次结算日："
                           onvaluechanged="associaDataCalculation" ondrawdate="onDrawDateFirst"
                           labelStyle="text-align:left;width:130px;"/>
                    <input style="width:100%" id="tenor" name="tenor" class="mini-spinner mini-mustFill"
                           labelField="true" label="拆借期限(天)：" minValue="0" required="true" maxValue="99999"
                           changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"/>
                    <input style="width:100%" id="accuredInterest" name="accuredInterest"
                           class="mini-spinner mini-mustFill input-text-strong" labelField="true" required="true"
                           label="应计利息(元)：" minValue="0" maxValue="99999999999999.9999" format="n4"
                           changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"/>
                    <input id="scheduleType" name="scheduleType" class="mini-combobox  mini-mustFill" required="true"
                           labelField="true" label="付息类型：" labelStyle="text-align:left;width:130px;" style="width:100%;"
                           data="CommonUtil.serverData.dictionary.scheduleType" value="D" onvaluechanged="selectCycle"/>
                    <%--                            <input style="width:100%" id="firstIPayDate" name="firstIPayDate" class="mini-datepicker mini-mustFill" labelField="true"  required="true"   label="首次付息日："  ondrawdate="onDrawDateFirst" labelStyle="text-align:left;width:130px;" onvaluechanged="getDayNumber" />--%>
                    <input style="width:100%" id="intPayDay" name="intPayDay" class="mini-spinner input-text-strong"
                           labelField="true" label="利息付款日："
                           labelStyle="text-align:left;width:130px;" onvaluechanged="getDayNumber" value="1"/>
                    <input id="basis" name="basis" class="mini-combobox mini-mustFill" required="true" labelField="true"
                           label="计息基准：" style="width:100%;" data="CommonUtil.serverData.dictionary.Basis"
                           onvaluechanged="linkIntCalculat" vtype="maxLength:15"
                           labelStyle="text-align:left;width:130px;">
                    <input style="width:100%" id="tradingProduct" name="tradingProduct"
                           class="mini-combobox mini-mustFill" required="true" allowInput="false" labelField="true"
                           label="交易品种：" labelStyle="text-align:left;width:130px;"
                           data="CommonUtil.serverData.dictionary.dealVarietyCredit"
                           onvaluechanged="associaDataCalculation"/>
                    <input id="tradingModel" name="tradingModel" class="mini-combobox  mini-mustFill" required="true"
                           labelField="true" label="交易模式：" labelStyle="text-align:left;width:130px;" style="width:100%;"
                           data="CommonUtil.serverData.dictionary.TradeModel"/>
                    <input id="dealTransType" name="dealTransType" class="mini-combobox" labelField="true"
                           style="width:100%;" label="数据来源：" data="CommonUtil.serverData.dictionary.dealTransType1"
                           vtype="maxLength:10" labelStyle="text-align:left;width:130px;" value="1"/>
                </div>
                <div class="rightarea">
                    <input style="width:100%" id="rate" name="rate" class="mini-spinner mini-mustFill input-text-strong"
                           labelField="true" label="拆借利率(%)：" required="true" minValue="0" maxValue="99999.999999"
                           format="n6" onvaluechanged="associaDataCalculation" changeOnMousewheel="false"
                           labelStyle="text-align:left;width:130px;" onvalidation="zeroValidation"/>
                    <%--                        <input style="width:100%" id="spread8" name="spread8" class="mini-spinner  input-text-strong" labelField="true"  label="利率差(%)："   minValue="0" maxValue="99999.999999" format="n6" onvaluechanged="associaDataCalculation" changeOnMousewheel="false" labelStyle="text-align:left;width:130px;" onvalidation="zeroValidation"  />--%>
                    <input style="width:100%;" id="spread8" name="spread8" class="mini-spinner input-text-strong"
                           labelField="true" label="利率差(%)：" changeOnMousewheel='false' format="n6"
                           labelStyle="text-align:left;width:130px" minValue="-100" maxValue="1000000" value="0"/>
                    <input style="width:100%" id="secondSettlementDate" name="secondSettlementDate"
                           class="mini-datepicker mini-mustFill" labelField="true" required="true" label="到期结算日："
                           onvaluechanged="associaDataCalculation" ondrawdate="onDrawDateSecond"
                           labelStyle="text-align:left;width:130px;"/>
                    <input style="width:100%" id="occupancyDays" name="occupancyDays" class="mini-spinner mini-mustFill"
                           labelField="true" onvaluechanged="linkageCalculat" required="true" enabled="false"
                           label="实际占款天数(天)：" minValue="0" maxValue="99999" changeOnMousewheel="false"
                           labelStyle="text-align:left;width:130px;"/>
                    <input style="width:100%" id="settlementAmount" name="settlementAmount"
                           class="mini-spinner mini-mustFill input-text-strong" labelField="true" required="true"
                           label="到期还款金额(元)：" onvalidation="zeroValidation" minValue="0" maxValue="99999999999999.9999"
                           format="n4" changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"/>
                    <input id="intPayCycle" name="intPayCycle" class="mini-combobox " labelField="true" label="付息周期："
                           labelStyle="text-align:left;width:130px;" style="width:100%;"
                           data="CommonUtil.serverData.dictionary.intPayCycle" showNullItem="true"/>
                    <input id="intDateRule" name="intDateRule" class="mini-combobox " labelField="true"
                           required="true" label="利息日期规则：" labelStyle="text-align:left;width:130px;" style="width:100%;"
                           data="CommonUtil.serverData.dictionary.intPayRule" showNullItem="true" />
                    <%--                            <input style="width:100%" id="lastIntPayDate" name="lastIntPayDate" class="mini-datepicker mini-mustFill" labelField="true" required="true"    label="最后一次付息日：" onvaluechanged="associaDataCalculation"  ondrawdate="onDrawDateSecond" labelStyle="text-align:left;width:130px;"   />--%>
                    <input id="rateCode" name="rateCode" class="mini-buttonedit mini-mustFill"
                           onbuttonclick="onRateEdit" labelField="true" label="利率代码：" style="width: 100%;"
                           labelStyle="text-align:left;width:130px;" allowInput="false" requiredErrorText="该输入项为必输项"
                           required="true"/>
                    <input id="nettingStatus" name="nettingStatus" class="mini-combobox  mini-mustFill" required="true"
                           labelField="true" label="净额清算状态：" labelStyle="text-align:left;width:130px;"
                           style="width:100%;" data="CommonUtil.serverData.dictionary.YesNo" showNullItem="true"/>
                    <input id="note" name="note" class="mini-textbox" labelField="true" label="交易事由："
                           labelStyle="text-align:left;width:130px;" style="width:100%;" vtype="maxLength:16"/>
                </div>
            </div>
            <%@ include file="../../Common/opicsLessDetail.jsp" %>
            <div class="mini-panel" title="清算信息" style="width:100%" allowResize="true" collapseOnTitleClick="true">
                <div class="leftarea">
                    <fieldset>
                        <legend>本方账户</legend>
                        <input id="borrowAccname" name="borrowAccname" class="mini-textbox" labelField="true"
                               label="资金账户户名：" vtype="maxLength:1333" style="width:100%;"
                               labelStyle="text-align:left;width:120px;"/>
                        <input id="borrowOpenBank" name="borrowOpenBank" class="mini-textbox" labelField="true"
                               label="资金开户行：" vtype="maxLength:1333" labelStyle="text-align:left;width:120px;"
                               style="width:100%;"/>
                        <input id="borrowAccnum" name="borrowAccnum" class="mini-textbox" labelField="true"
                               label="资金账号：" onvalidation="onEnglishAndNumberValidation" vtype="maxLength:400"
                               labelStyle="text-align:left;width:120px;" style="width:100%;"/>
                        <input id="borrowPsnum" name="borrowPsnum" class="mini-textbox" labelField="true"
                               label="支付系统行号：" onvalidation="onEnglishAndNumberValidation" vtype="maxLength:400"
                               labelStyle="text-align:left;width:120px;" style="width:100%;"/>
                    </fieldset>
                </div>
                <div class="rightarea">
                    <fieldset>
                        <legend>对手方账户</legend>
                        <input id="removeAccname" name="removeAccname" class="mini-textbox" labelField="true"
                               label="资金账户户名：" vtype="maxLength:1333" style="width:100%;"
                               labelStyle="text-align:left;width:120px;"/>
                        <input id="removeOpenBank" name="removeOpenBank" class="mini-textbox" labelField="true"
                               label="资金开户行：" vtype="maxLength:1333" labelStyle="text-align:left;width:120px;"
                               style="width:100%;"/>
                        <input id="removeAccnum" name="removeAccnum" class="mini-textbox" labelField="true"
                               label="资金账号：" onvalidation="onEnglishAndNumberValidation" vtype="maxLength:400"
                               labelStyle="text-align:left;width:120px;" style="width:100%;"/>
                        <input id="removePsnum" name="removePsnum" class="mini-textbox" labelField="true"
                               label="支付系统行号：" onvalidation="onEnglishAndNumberValidation" vtype="maxLength:400"
                               labelStyle="text-align:left;width:120px;" style="width:100%;"/>
                    </fieldset>
                </div>
            </div>
        </div>
    </fieldset>
</div>

<script>
    mini.parse();

    var url = window.location.search;
    var prdNo = CommonUtil.getParam(url, "prdNo");
    var fPrdCode = CommonUtil.getParam(url, "fPrdCode");
    var prdName = CommonUtil.getParam(url, "prdName");
    var dealType = CommonUtil.getParam(url, "dealType");
    var form = new mini.Form("#search_form");
    var grid = mini.get("datagrid");
    var row = "";

    function getData(action) {
        var row = null;
        if (action != "add") {
            row = grid.getSelected();
        }
        return row;
    }

    /**************************************点击下面显示详情开始******************************/
    var from = new mini.Form("MiniSettleForeigDetail");
    from.setEnabled(false);
    var grid = mini.get("datagrid");
    //grid.load();
    //绑定表单
    var db = new mini.DataBinding();
    db.bindForm("MiniSettleForeigDetail", grid);
    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn){
            initButton();
            search(10,0);
        });
    });
    /**************************************点击下面显示详情结束******************************/
    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize, pageIndex);
    });

    //成交日期
    function onDrawDateStart(e) {
        var startDate = e.date;
        var endDate = mini.get("endDate").getValue();
        if (CommonUtil.isNull(endDate)) {
            return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }

    function onDrawDateEnd(e) {
        var endDate = e.date;
        var startDate = mini.get("startDate").getValue();
        if (CommonUtil.isNull(startDate)) {
            return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }

    //查看详情
    function onRowDblClick(e) {
        var url = CommonUtil.baseWebPath() + "/cfetsrmb/creditTYLoanEdit.jsp?action=detail&prdNo=" + prdNo + "&prdName=" + prdName+"&fPrdCode="+fPrdCode;
        var tab = {
            id: "IboTYDetail", name: "IboTYDetail", url: url, title: "同业借款成交单详情",
            parentId: top["win"].tabs.getActiveTab().name, showCloseButton: true
        };
        var paramData = {selectData: grid.getSelected()};
        CommonUtil.openNewMenuTab(tab, paramData);
    }

    //清空
    function clear() {
        var MiniSettleForeigDetail = new mini.Form("MiniSettleForeigDetail");
        MiniSettleForeigDetail.clear();
        var form = new mini.Form("search_form");
        form.clear();
        searchs();
    }

    //添加
    function add() {
        var url = CommonUtil.baseWebPath() + "/cfetsrmb/creditTYLoanEdit.jsp?action=add?t=" + new Date() + "&prdNo=" + prdNo + "&prdName=" + prdName + "&dealType=" + dealType+"&fPrdCode="+fPrdCode;
        var tab = {
            id: "creditTYLoanAdd", name: "creditTYLoanAdd", url: url, title: "同业借款成交单补录",
            parentId: top["win"].tabs.getActiveTab().name, showCloseButton: true
        };
        var paramData = {selectData: ""};
        CommonUtil.openNewMenuTab(tab, paramData);
    }

    //修改
    function edit() {
        var row = grid.getSelected();
        if (row) {
            var url = CommonUtil.baseWebPath() + "/cfetsrmb/creditTYLoanEdit.jsp?action=edit&prdNo=" + prdNo + "&prdName=" + prdName + "&dealType=" + dealType+"&fPrdCode="+fPrdCode;
            var tab = {
                id: "IboTYEdit", name: "IboTYEdit", url: url, title: "同业借款成交单修改",
                parentId: top["win"].tabs.getActiveTab().name, showCloseButton: true
            };
            var paramData = {selectData: row};
            CommonUtil.openNewMenuTab(tab, paramData);
        } else {
            mini.alert("请选择一条记录", "提示");
        }
    }

    //删除
    function del() {
        var rows = grid.getSelecteds();
        if (rows.length == 0) {
            mini.alert("请选中一行", "提示");
            return;
        }
        if (rows.length > 1) {
            mini.alert("系统不支持多条数据进行删除", "消息提示");
            return;
        }
        mini.confirm("您确认要删除选中记录?", "系统警告", function (value) {
            if (value == 'ok') {
                var data = rows[0];
                params = mini.encode(data);
                CommonUtil.ajax({
                    url: "/IfsCfetsrmbIboController/deleteCfetsrmbIbo",
                    data: params,
                    callback: function (data) {
                        mini.alert("删除成功.", "系统提示");
                        searchs();
                    }
                });
            }
        });
    }


    grid.on("select", function (e) {
        var row = e.record;
        mini.get("approve_mine_commit_btn").setEnabled(false);
        mini.get("approve_commit_btn").setEnabled(false);
        mini.get("edit_btn").setEnabled(false);
        mini.get("delete_btn").setEnabled(false);
        mini.get("approve_log").setEnabled(true);
        mini.get("recall_btn").setEnabled(true);
        mini.get("batch_commit_btn").setEnabled(true);
        if (row.approveStatus == "3") {//新建
            mini.get("edit_btn").setEnabled(true);
            mini.get("delete_btn").setEnabled(true);
            mini.get("approve_mine_commit_btn").setEnabled(true);
            mini.get("approve_commit_btn").setEnabled(false);
            mini.get("reback_btn").setEnabled(false);
            mini.get("recall_btn").setEnabled(false);
        }
        if (row.approveStatus == "6" || row.approveStatus == "7" || row.approveStatus == "8" || row.approveStatus == "17") {//审批通过：6    审批拒绝:5
            mini.get("edit_btn").setEnabled(false);
            mini.get("delete_btn").setEnabled(false);
            mini.get("approve_mine_commit_btn").setEnabled(false);
            mini.get("approve_commit_btn").setEnabled(true);
            mini.get("batch_approve_btn").setEnabled(false);
            mini.get("batch_commit_btn").setEnabled(false);
            mini.get("opics_check_btn").setEnabled(false);
            mini.get("reback_btn").setEnabled(false);
            mini.get("recall_btn").setEnabled(false);
        }
        if (row.approveStatus == "5") {//审批中:5
            mini.get("edit_btn").setEnabled(false);
            mini.get("delete_btn").setEnabled(false);
            mini.get("approve_mine_commit_btn").setEnabled(false);
            mini.get("approve_commit_btn").setEnabled(true);
            mini.get("batch_commit_btn").setEnabled(false);
            mini.get("reback_btn").setEnabled(true);
            mini.get("recall_btn").setEnabled(true);
        }

    });

    //查询按钮
    function searchs() {
        search(grid.pageSize, 0);
    }

    function search(pageSize, pageIndex) {
        form.validate();
        if (form.isValid() == false) {
            mini.alert("信息填写有误，请重新填写", "系统也提示");
            return;
        }

        var data = form.getData(true);
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        data['branchId'] = branchId;
        data['prdNo'] = prdNo;
        data['fPrdCode']=fPrdCode;
        var url = null;

        var approveType = mini.get("approveType").getValue();
        if (approveType == "mine") {
            url = "/IfsCfetsrmbIboController/searchRmbIboSwapMine";
        } else if (approveType == "approve") {
            url = "/IfsCfetsrmbIboController/searchPageRmbIboUnfinished";
        } else {
            url = "/IfsCfetsrmbIboController/searchPageRmbIboFinished";
        }
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: url,
            data: params,
            callback: function (data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }

    /**************************审批相关****************************/
    //审批日志查看
    function appLog(selections) {
        var flow_type = Approve.FlowType.VerifyApproveFlow;
        if (selections.length <= 0) {
            mini.alert("请选择要操作的数据", "系统提示");
            return;
        }
        if (selections.length > 1) {
            mini.alert("系统不支持多笔操作", "系统提示");
            return;
        }
        if (selections[0].tradeSource == "3") {
            mini.alert("初始化导入的业务没有审批日志", "系统提示");
            return false;
        }
        Approve.approveLog(flow_type, selections[0].ticketId);
    };

    //提交正式审批、待审批
    function verify(selections) {
        if (selections.length == 0) {
            mini.alert("请选中一条记录！", "消息提示");
            return false;
        } else if (selections.length > 1) {
            mini.alert("该功能不支持多笔提交，如需多笔提交请点击[批量提交]按钮", "系统提示");
            return false;
        }
        if (CommonUtil.isNull(selections[0]["dealSource"]) || CommonUtil.isNull(selections[0]["port"]) || CommonUtil.isNull(selections[0]["cost"]) ||
            CommonUtil.isNull(selections[0]["product"]) || CommonUtil.isNull(selections[0]["prodType"])) {
            mini.alert("请先进行opics要素检查更新!", "提示");
            return;
        }
        if (selections[0]["approveStatus"] != "3") {
            var url = CommonUtil.baseWebPath() + "/cfetsrmb/creditTYLoanEdit.jsp?action=approve&ticketId=" + row.ticketId+"&fPrdCode="+fPrdCode;
            var tab = {
                "id": "creditLoanApprove", name: "creditLoanApprove", url: url, title: "同业借款审批",
                parentId: top["win"].tabs.getActiveTab().name, showCloseButton: true
            };
            var paramData = {selectData: selections[0]};
            CommonUtil.openNewMenuTab(tab, paramData);
            // top["win"].showTab(tab);
        } else {
            Approve.approveCommit(Approve.FlowType.VerifyApproveFlow, selections[0]["ticketId"], Approve.OrderStatus.New, "IfsRmbIboService", prdNo, function () {
                search(grid.pageSize, grid.pageIndex);
            });
        }
    };

    //审批
    function approve() {
        mini.get("approve_commit_btn").setEnabled(false);
        var messageid = mini.loading("系统正在处理...", "请稍后");
        try {
            verify(grid.getSelecteds());
        } catch (error) {

        }
        mini.hideMessageBox(messageid);
        mini.get("approve_commit_btn").setEnabled(true);
    }

    //提交审批
    function commit() {
        mini.get("approve_mine_commit_btn").setEnabled(false);
        var messageid = mini.loading("系统正在处理...", "请稍后");
        try {
            verify(grid.getSelecteds());
        } catch (error) {

        }
        mini.hideMessageBox(messageid);
        mini.get("approve_mine_commit_btn").setEnabled(true);
    }

    //审批日志
    function searchlog() {
        appLog(grid.getSelecteds());
    }

    //打印
    function print() {
        var selections = grid.getSelecteds();
        if (selections == null || selections.length == 0) {
            mini.alert('请选择一条要打印的数据！', '系统提示');
            return false;
        } else if (selections.length > 1) {
            mini.alert('暂不支持多条数据同时打印，请选择一条要打印的数据！', '系统提示');
            return false;
        }
        var canPrint = selections[0].ticketId;

        if (!CommonUtil.isNull(canPrint)) {
            var actionStr = CommonUtil.pPath + "/sl/IfsPrintUtilController/exportload/print/" + PrintNo.creditTYLoan + "/" + canPrint;
            $('<form target="_blank" action=' + actionStr + ' method="post"></form>').appendTo('body').submit().remove();
        }
        ;
    }

    //导出报表
    function exportExcel() {
        exportExcelManin("jytyjk.xls", "jytyjk");
    }

</script>
</body>
</html>