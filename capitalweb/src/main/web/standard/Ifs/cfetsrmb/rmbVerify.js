  
//本方方向 值改变时调用此方法(本方方向改变，则对方方向改变)  
function  dirVerify(e){
    if(e.value == "P"){//  买入/拆入/正回购
        mini.get("oppoDir").setValue("S");
    }else if(e.value == "S"){//  卖出/拆出/逆回购
        mini.get("oppoDir").setValue("P");
    }

}
//制保留2位小数，如：2，会在2后面补上00.即2.00 
function toDecimal(x) { 
	    var f = parseFloat(x); 
	    if (isNaN(f)) { 
	    	return false; 
	    } 
	    var f = Math.round(x*100)/100; 
	    var s = f.toString(); 
	    var rs = s.indexOf('.'); 
	    if (rs < 0) { 
	    	rs = s.length; 
	    	s += '.'; 
	    } 
	    while (s.length <= rs + 2) { 
	    	s += '0'; 
	    } 
	    return s; 
 }

//日期改变时发生
//主要用于检验是否选择了节假日，若选择了节假日，则提示，并清空选择的值
/*function onDateChanged(param){
	var date = mini.get(param).getValue();
	var data={};
	data['holidate']=date;
	var params=mini.encode(data);
	CommonUtil.ajax({
		url:"/IfsOpicsHldyController/getCalendarById",
		data:params,
		callback:function(data){
			if(data.code == 'error.common.0000'){
				if(data.obj){
					mini.alert("您选择的日期为节假日日期,请重新选择!");
					mini.get(param).setValue("");
				}
			}
			
		}
	});
	
	
}*/


/* 金额非0校验 */
function zeroValidation(e){
	if(e.value!=0){
		return true;
	}else{
		e.errorText = "金额不能为0";
        e.isValid = false;
	}
}

/*计息基础转换 BY XQQ*/
function basisTonumber(basis){
	switch (basis) {
		case 'A360':
			return 360;
			break;
		case 'A365':
			return 365;
			break;
		case 'ACTUAL':
			return 366;
			break;
		default:
			return 360;
	}
}
/*付息频率转换 BY XQQ*/
function intpaycircleTonumber(intpaycircle){
	switch (intpaycircle) {
		case 'S':
			return 90;
			break;
		case 'A':
			return 360;
			break;
		case '91':
			return 91;
			break;
		case 'Q':
			return 180;
			break;	
		case 'M':
			return 30;
			break;	
		case '28':
			return 28;
			break;
		case '84':
			return 84;
			break;
		case '182':
			return 182;
			break;
		default:
			return 10000;
			break;
	}
}

function EnglistAndNumberValidation(e) {
	var reg = new RegExp("^[0-9a-zA-Z@.]+$");
	if (reg.test(e.value)) {
		return true;
	}else{
		e.errorText = "只能输入英文和字母和@.符号";
		e.isValid = false;
	} 
}


















