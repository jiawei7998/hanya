<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../../global.jsp" %>
<html>
<head>
    <script type="text/javascript" src="./rmbVerify.js"></script>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>债券借贷 维护</title>
    <script type="text/javascript">
        /**prdCode用于页面投资组合的选择   该全局变量必须为prdCode*/
        var prdCode = "SECLEN";
    </script>
</head>

<body style="width:100%;height:100%;background:white">
<div class="mini-splitter" style="width:100%;height:100%;">
    <div size="90%" showCollapseButton="false">
        <h1 style="text-align:center"><strong>债券借贷</strong></h1>
        <div id="field_form" class="mini-fit area" style="background:white">
            <input id="sponsor" name="sponsor" class="mini-hidden"/>
            <input id="sponInst" name="sponInst" class="mini-hidden"/>
            <input id="aDate" name="aDate" class="mini-hidden"/>
            <input id="ticketId" name="ticketId" class="mini-hidden"/>

            <div class="mini-panel" title="成交单编号" style="width:100%;" allowResize="true" collapseOnTitleClick="true">
                <div class="leftarea">
                    <input style="width:100%;" id="contractId" name="contractId" class="mini-textbox mini-mustFill"
                           required="true" labelField="true" requiredErrorText="该输入项为必输项" label="成交单编号："
                           labelStyle="text-align:left;width:130px;" vtype="maxLength:20"/>
                </div>
                <div class="rightarea">
                    <input style="width:100%;" id="cfetsno" name="cfetsno" class="mini-textbox" label="CFETS机构码："
                           labelField="true" visible="false" enabled="false" labelStyle="text-align:left;width:130px;"/>
                </div>
            </div>
            <div class="mini-panel" title="成交双方信息" style="width:100%" allowResize="true" collapseOnTitleClick="true">
                <div class="leftarea">
                    <fieldset>
                        <legend>本方信息</legend>
                        <input id="myDir" name="myDir" class="mini-combobox mini-mustFill" labelField="true"
                               required="true" label="本方方向：" style="width:100%;"
                               labelStyle="text-align:left;width:120px;"
                               data="CommonUtil.serverData.dictionary.SlTrading" onvaluechanged="dirVerify"/>
                        <!--onvaluechanged="dirVerify" -->
                        <input id="borrowInst" name="borrowInst" class="mini-textbox mini-mustFill" labelField="true"
                               label="机构：" required="true" style="width:100%;" enabled="false"
                               labelStyle="text-align:left;width:120px;"/>
                        <input id="borrowTrader" name="borrowTrader" class="mini-textbox mini-mustFill"
                               labelField="true" label="交易员：" required="true" vtype="maxLength:5" enabled="false"
                               labelStyle="text-align:left;width:120px;" style="width:100%;"/>
                        <input id="quotaOccupyType" name="quotaOccupyType" class="mini-combobox mini-mustFill"
                               labelField="true" label="额度占用类型：" required="true" style="width:100%;"
                               labelStyle="text-align:left;width:120px;"
                               data="CommonUtil.serverData.dictionary.QuotaOccupyType" onvaluechanged="quotaJudge"
                               value="1"/>
                        <!-- <input id="borrowTel" name="borrowTel" class="mini-textbox" labelField="true"  label="电话："   onvalidation="CommonUtil.onValidation(e,'phone',[null])"  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
                        <input id="borrowFax" name="borrowFax" class="mini-textbox" labelField="true"  label="传真："  onvalidation="CommonUtil.onValidation(e,'phone',[null])" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
                        <input id="borrowCorp" name="borrowCorp" class="mini-textbox" labelField="true"  label="法人代表："  vtype="maxLength:10"  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
                        <input id="borrowAddr" name="borrowAddr" class="mini-textbox" labelField="true"  label="地址："  vtype="maxLength:50" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
                     -->
                    </fieldset>
                </div>
                <div class="rightarea">
                    <fieldset>
                        <legend>对手方信息</legend>
                        <input id="oppoDir" name="oppoDir" class="mini-combobox mini-mustFill" enabled="false"
                               labelField="true" required="true" label="对方方向：" style="width:100%;"
                               labelStyle="text-align:left;width:120px;"
                               data="CommonUtil.serverData.dictionary.SlTrading"/>
                        <input id="lendInst" name="lendInst" class="mini-buttonedit mini-mustFill"
                               onbuttonclick="onButtonEdit" required="true" labelField="true" allowInput="false"
                               label="机构：" vtype="maxLength:50" style="width:100%;"
                               labelStyle="text-align:left;width:120px;"/>
                        <input id="lendTrader" name="lendTrader" class="mini-textbox mini-mustFill" labelField="true"
                               label="交易员：" vtype="maxLength:30" labelStyle="text-align:left;width:120px;"
                               style="width:100%;margin-bottom: 2px" required="true"/>
                        <!-- <input id="lendTel" name="lendTel" class="mini-textbox" labelField="true"  label="电话：" onvalidation="CommonUtil.onValidation(e,'phone',[null])"  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
                        <input id="lendFax" name="lendFax" class="mini-textbox" labelField="true"  label="传真："  onvalidation="CommonUtil.onValidation(e,'phone',[null])"  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
                        <input id="lendCorp" name="lendCorp" class="mini-textbox" labelField="true"  label="法人代表："  vtype="maxLength:10"  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
                        <input id="lendAddr" name="lendAddr" class="mini-textbox" labelField="true"  label="地址："  vtype="maxLength:50" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
                     -->
                    </fieldset>
                </div>
            </div>

            <div class="mini-panel" title="交易主体信息" style="width:100%" allowResize="true" collapseOnTitleClick="true">
                <div class="leftarea">
                    <input style="width:100%;" id="forDate" name="forDate" class="mini-datepicker mini-mustFill"
                           labelField="true" required="true" label="成交日期：" labelStyle="text-align:left;width:130px;"/>
                    <input id="underlyingSecurityId" name="underlyingSecurityId" class="mini-buttonedit mini-mustFill"
                           style="width:100%" required="true" labelField="true" label="标的债券代码：" maxLength="20"
                           onbuttonclick="onSecurityQuery" allowInput="false"
                           labelStyle="text-align:left;width:130px;"/>
                    <!-- 				<input  style="width:100%;" id="bondProduct" name="bondProduct" onbuttonclick="onBondPrdEdit" allowInput="false" class="mini-buttonedit mini-mustFill" labelField="true"   label="券的产品代码：" required="true"  maxLength="6"  labelStyle="text-align:left;width:130px;" > -->
                    <input id="bondPort" name="bondPort" class="mini-buttonedit mini-mustFill"
                           onbuttonclick="onBondPortEdit" style="width:100%;" labelField="true" label="券的投资组合："
                           required="true" allowInput="false" vtype="maxLength:4"
                           labelStyle="text-align:left;width:130px;">
                    <input id="underlyingQty" name="underlyingQty" class="mini-spinner mini-mustFill input-text-strong"
                           style="width:100%" required="true" changeOnMousewheel='false' labelField="true"
                           label="标的债券券面总额(万元)：" maxValue="9999999999.9999" format="n4"
                           labelStyle="text-align:left;width:130px;" required="true" onvaluechanged="linkageCalculatFee"
                           onvalidation="zeroValidation"/>
                    <input id="investType" name="investType" class="mini-combobox mini-mustFill" labelField="true"
                           label="投资类型：" style="width:100%;" labelStyle="text-align:left;width:130px;"
                           data="CommonUtil.serverData.dictionary.invtype" required="true"/>
                    <input id="firstSettlementMethod" name="firstSettlementMethod" class="mini-combobox mini-mustFill"
                           required="true" style="width:100%" labelField="true" label="首次结算方式：" vtype="maxLength:50"
                           labelStyle="text-align:left;width:130px;"
                           data="CommonUtil.serverData.dictionary.SettlementMethod"/>
                    <input id="firstSettlementDate" name="firstSettlementDate" class="mini-datepicker mini-mustFill"
                           required="true" style="width:100%" labelField="true" label="首次结算日："
                           onvaluechanged="linkageCalculatDay" ondrawdate="onDrawDateFirst"
                           labelStyle="text-align:left;width:130px;"/>
                    <input id="occupancyDays" name="occupancyDays" class="mini-spinner mini-mustFill" style="width:100%"
                           required="true" changeOnMousewheel='false' labelField="true" label="实际占券天数(天)："
                           maxValue="99999" onvaluechanged="linkageCalculat" labelStyle="text-align:left;width:130px;"
                           required="true"/>
                    <input id="miscFeeType" name="miscFeeType" class="mini-spinner mini-mustFill input-text-strong"
                           style="width:100%" required="true" changeOnMousewheel='false' labelField="true"
                           label="借贷费用(元)：" maxValue="99999999999999.9999" format="n4"
                           labelStyle="text-align:left;width:130px;" onvalidation="zeroValidation"/>
                    <input id="tradingModel" name="tradingModel" class="mini-combobox mini-mustFill" required="true"
                           labelField="true" label="交易模式：" labelStyle="text-align:left;width:130px;" style="width:100%;"
                           data="CommonUtil.serverData.dictionary.TradeModel"/>
                    <input id="dealTransType" name="dealTransType" class="mini-combobox" labelField="true"
                           style="width:100%;" label="数据来源：" data="CommonUtil.serverData.dictionary.dealTransType1"
                           vtype="maxLength:10" labelStyle="text-align:left;width:130px;" value="1">
                    <input id="settleMeans" name="settleMeans" class="mini-combobox mini-mustFill" labelField="true"
                           label="费用结算方式：" labelStyle="text-align:left;width:130px;" style="width:100%;" required="true"
                           data="CommonUtil.serverData.dictionary.settleMeans"/>
                    <input id="attributionDept" name="attributionDept" valueField="id" showFolderCheckBox="true"
                           labelField="true" label="归属机构：" required="true" showCheckBox="true" style="width:100%"
                           labelStyle="text-align:left;width:130px"
                           showTreeLines="true" showTreeIcon="true" textField="instName" parentField="parentId"
                           class="mini-treeselect mini-mustFill" expandOnLoad="true" resultAsTree="false"
                           valueFromSelect="true" emptyText="请选择机构"/>
                    <input id="remoteService" name="remoteService" class="mini-combobox" labelField="true"
                           style="width:100%;" label="是否异地业务：" data="CommonUtil.serverData.dictionary.remoteService"
                           vtype="maxLength:10" labelStyle="text-align:left;width:130px;" value="1">
                </div>
                <div class="rightarea">
                    <input id="nettingStatus" name="nettingStatus" class="mini-combobox mini-mustFill" required="true"
                           labelField="true" label="净额清算状态：" labelStyle="text-align:left;width:130px;"
                           style="width:100%;" data="CommonUtil.serverData.dictionary.YesNo" showNullItem="true"/>
                    <input id="underlyingSymbol" name="underlyingSymbol" class="mini-textbox mini-mustFill"
                           style="width:100%" required="true" labelField="true" label="标的债券名称："
                           labelStyle="text-align:left;width:130px;" enabled="false"/>
                    <!-- 				<input style="width:100%;" id="bondProdType" name="bondProdType" onbuttonclick="onBondTypeEdit" allowInput="false" class="mini-buttonedit mini-mustFill" labelField="true"  label="券的产品类型：" required="true"  vtype="maxLength:2" labelStyle="text-align:left;width:130px;" > -->
                    <input style="width:100%;" id="bondCost" name="bondCost" onbuttonclick="onBondCostEdit"
                           allowInput="false" class="mini-buttonedit mini-mustFill" labelField="true" label="券的成本中心："
                           required="true" vtype="maxLength:15" labelStyle="text-align:left;width:130px;">
                    <input id="price" name="price" class="mini-spinner mini-mustFill input-text-strong"
                           style="width:100%" changeOnMousewheel='false' required="true" labelField="true"
                           label="借贷费率(%)：" minValue="0" maxValue="99999.999999" format="n6" changeOnMousewheel="false"
                           labelStyle="text-align:left;width:130px;" required="true" onvaluechanged="linkageCalculatFee"
                           onvalidation="zeroValidation"/>
                    <input id="tradingProduct" name="tradingProduct" class="mini-combobox mini-mustFill" required="true"
                           style="width:100%" labelField="true" label="交易品种：" vtype="maxLength:10"
                           labelStyle="text-align:left;width:130px;"
                           data="CommonUtil.serverData.dictionary.dealVarietySl"/>
                    <input id="secondSettlementMethod" name="secondSettlementMethod" class="mini-combobox mini-mustFill"
                           required="true" style="width:100%" labelField="true" label="到期结算方式：" vtype="maxLength:50"
                           labelStyle="text-align:left;width:130px;"
                           data="CommonUtil.serverData.dictionary.SettlementMethod"/>
                    <input id="secondSettlementDate" name="secondSettlementDate" class="mini-datepicker mini-mustFill"
                           required="true" style="width:100%" labelField="true" label="到期结算日："
                           onvaluechanged="linkageCalculatDay" ondrawdate="onDrawDateSecond"
                           labelStyle="text-align:left;width:130px;"/>
                    <input id="tenor" name="tenor" class="mini-spinner mini-mustFill" style="width:100%" required="true"
                           changeOnMousewheel='false' labelField="true" label="借贷期限(天)：" maxValue="99999"
                           labelStyle="text-align:left;width:130px;"/>
                    <input id="basis" name="basis" class="mini-combobox mini-mustFill" required="true" labelField="true"
                           label="计息基准：" onvaluechanged="linkageCalculatFee" style="width:100%;" value="A365"
                           data="CommonUtil.serverData.dictionary.Basis" vtype="maxLength:15"
                           labelStyle="text-align:left;width:130px;">
                    <input id="solution" name="solution" class="mini-combobox mini-mustFill" labelField="true"
                           style="width:100%" required="true" label="争议解决方式：" labelStyle="text-align:left;width:130px;"
                           data="CommonUtil.serverData.dictionary.solution"/>
                    <input id="note" name="note" class="mini-textbox" labelField="true" label="交易事由："
                           labelStyle="text-align:left;width:130px;" style="width:100%;" vtype="maxLength:16"/>
                    <input id="settleAcct" name="settleAcct" class="mini-combobox mini-mustFill" labelField="true"
                           label="费用结算账号：" labelStyle="text-align:left;width:130px;" style="width:100%;" required="true"
                           data="CommonUtil.serverData.dictionary.settleAcct" value="DVP"/>
                    <input id="fiveLevelClass" name="fiveLevelClass" class="mini-combobox" labelField="true"
                           requiredErrorText="该输入项为必输项" label="五级分类：" labelStyle="text-align:left;width:130px;"
                           style="width:100%;" data="CommonUtil.serverData.dictionary.FiveLevelClass" value="0"/>
                    <input id="approvalFormName" name="approvalFormName" class="mini-textbox" labelField="true"
                           label="审批单表头："
                           labelStyle="text-align:left;width:130px;" style="width:100%;"/>
                </div>
            </div>
            <div class="mini-panel" title="标的债券付息信息" style="width:100%" allowResize="true" collapseOnTitleClick="true">
                <%--			<div id="search_detail_form">--%>
                <div class="centerarea">
                    <%--					<input id="marginSecuritiesId" name="marginSecuritiesId" class="mini-buttonedit" labelField="true" allowInput="false" label="质押债券代码：" onbuttonclick="onBondQuery"  maxLength="20" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" style="width:33%" labelStyle="text-align:left;width:170px;"   />--%>
                    <%--					<input id="marginSymbol" name="marginSymbol" class="mini-textbox" labelField="true"   enabled="false"  label="质押债券名称："   labelStyle="text-align:left;width:170px;"  style="width:33%" />--%>
                    <%--					<input id="marginAmt" name="marginAmt" class="mini-spinner input-text-strong"    changeOnMousewheel='false' labelField="true"  label="质押债券券面总额(万元)：" maxValue="9999999999.9999" format="n4" style="width:33%" labelStyle="text-align:left;width:170px;"   />--%>
                    <input id="interestPaymentDate" name="interestPaymentDate" class="mini-datepicker "
                           style="width:33%" labelField="true" label="付息日：" labelStyle="text-align:left;width:170px;"/>
                    <input id="interestPaymentAmounts" name="interestPaymentAmounts"
                           class="mini-spinner input-text-strong" changeOnMousewheel='false' labelField="true"
                           label="应计利息总额(万元)：" maxValue="9999999999.9999" format="n4" style="width:33%"
                           labelStyle="text-align:left;width:170px;"/>
                    <input id="couponRate" name="couponRate" class="mini-spinner input-text-strong" style="width:33%"
                           changeOnMousewheel='false' labelField="true" label="票面利率(%)：" minValue="0"
                           maxValue="99999.999999" format="n6" changeOnMousewheel="false"
                           labelStyle="text-align:left;width:170px;" onvalidation="zeroValidation"/>
                </div>

            </div>
            <div class="mini-panel" title="质押品明细信息" style="width:100%" allowResize="true" collapseOnTitleClick="true">
                <div id="search_detail_form">
                    <div class="centerarea">
                        <input id="marginSecuritiesId" name="marginSecuritiesId" class="mini-buttonedit"
                               labelField="true" allowInput="false" label="质押债券代码：" onbuttonclick="onBondQuery"
                               maxLength="20" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])"
                               style="width:33%" labelStyle="text-align:left;width:170px;"/>
                        <input id="marginSymbol" name="marginSymbol" class="mini-textbox" labelField="true"
                               enabled="false" label="质押债券名称：" labelStyle="text-align:left;width:170px;"
                               style="width:33%"/>
                        <input id="marginAmt" name="marginAmt" class="mini-spinner input-text-strong"
                               changeOnMousewheel='false' labelField="true" label="质押债券券面总额(万元)："
                               maxValue="9999999999.9999" format="n4" style="width:33%"
                               labelStyle="text-align:left;width:170px;" onvaluechanged="calculateRatio"/>
                        <input id="invtype" name="invtype" class="mini-combobox" labelField="true" label="投资类型："
                               labelStyle="text-align:left;width:170px;" data="CommonUtil.serverData.dictionary.invtype"
                               style="width:33%"/>
                        <input id="prodb" name="prodb" class="mini-buttonedit" labelField="true"
                               onbuttonclick="onPrdEdit1" allowInput="false" label="产品：" vtype="maxLength:6"
                               labelStyle="text-align:left;width:170px;" style="width:33%"/>
                        <input id="portb" name="portb" class="mini-buttonedit" labelField="true"
                               onbuttonclick="onBondPortEdit1" allowInput="false" label="投资组合：" vtype="maxLength:6"
                               labelStyle="text-align:left;width:170px;" style="width:33%"/>
                        <!--<input id="prodtypeb" name="prodtypeb" class="mini-buttonedit" onbuttonclick="onTypeEdit1" labelField="true" allowInput="false" label="产品类型：" vtype="maxLength:2" labelStyle="text-align:left;width:170px;" style="width:33%" >-->
                        <input id="costb" name="costb" class="mini-buttonedit" onbuttonclick="onCostEdit1"
                               labelField="true" allowInput="false" label="成本中心：" vtype="maxLength:15"
                               labelStyle="text-align:left;width:170px;" style="width:33%"/>
                        <input id="marginRatio" name="marginRatio" class="mini-spinner input-text-strong"
                               style="width:33%" changeOnMousewheel='false' labelField="true" label="质押比例(%)："
                               minValue="0" maxValue="99999.99" format="n6" changeOnMousewheel="false"
                               labelStyle="text-align:left;width:170px;"/>
                    </div>
                </div>
                <div class="centerarea" style="margin-top:5px;">
		<span style="float:left;">
		</span>
                    <div class="mini-toolbar" style="padding:0px;border-bottom:0;" id="toolbar">
                        <table style="width:100%;">
                            <tr>
                                <td style="width:100%;">
                                    <a id="add_btn" class="mini-button" style="display: none" onclick="add()">添加</a>
                                    <a id="edit_btn" class="mini-button" style="display: none" onclick="edit()">修改</a>
                                    <a id="delete_btn" class="mini-button" style="display: none" onclick="del()">删除</a>
                                    <a id="saves_btn" class="mini-button" style="display: none" onclick="saves()"
                                       enabled="false">保存</a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="centerarea" style="height:150px;">
                    <div class="mini-fit">
                        <div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:80%;" idField="id"
                             allowAlternating="true"
                             fitColumns="false" allowResize="true" sortMode="client" allowAlternating="true"
                             showpager="false">
                            <div property="columns">
                                <div type="indexcolumn" width="50px" headerAlign="center">序号</div>
                                <div field="marginSecuritiesId" width="200" align="center" headerAlign="center">质押债券代码
                                </div>
                                <div field="marginSymbol" width="230px" align="center" headerAlign="center">质押债券名称</div>
                                <div field="marginAmt" width="200" align="right" headerAlign="center" allowSort="true"
                                     numberFormat="n4">质押债券券面总额(万元)
                                </div>
                                <div field="invtype" width="150" align="right" headerAlign="center" allowSort="true"
                                     renderer="CommonUtil.dictRenderer" data-options="{dict:'invtype'}">投资类型
                                </div>
                                <div field="prodb" width="150" align="right" headerAlign="center" allowSort="true">产品
                                </div>
                                <div field="portb" width="150" align="right" headerAlign="center" allowSort="true">
                                    投资组合
                                </div>
                                <div field="costb" width="150" align="right" headerAlign="center" allowSort="true">
                                    成本中心
                                </div>
                                <div field="marginRatio" width="150" align="right" headerAlign="center"
                                     allowSort="true">质押比例(%)
                                </div>
                                <!--<div field="prodtypeb" width="150" align="right" headerAlign="center"  allowSort="true">产品类型</div> -->
                            </div>
                        </div>
                        <div class="leftarea">
                            <input id="marginReplacement" name="marginReplacement" class="mini-combobox mini-mustFill"
                                   required="true" labelField="true" label="质押债券置换安排：" vtype="maxLength:10"
                                   style="width:100%;" labelStyle="text-align:left;width:170px;" value="是"
                                   data="CommonUtil.serverData.dictionary.isPlan"/>
                        </div>
                        <div class="rightarea">
                            <input id="marginTotalAmt" name="marginTotalAmt"
                                   class="mini-spinner mini-mustFill input-text-strong" required="true"
                                   changeOnMousewheel='false' labelField="true" label="质押债券券面总额合计(万元)："
                                   maxValue="9999999999.9999" format="n4" style="width:100%;"
                                   labelStyle="text-align:left;width:170px;" onvalidation="zeroValidation"/>
                        </div>
                    </div>
                </div>
                <%@ include file="../../Common/custLimit.jsp" %>
            </div>
            <%@ include file="../../Common/opicsLess.jsp" %>
            <div class="mini-panel" title="清算信息" style="width:100%" allowResize="true" collapseOnTitleClick="true">
                <div class="leftarea">
                    <fieldset>
                        <legend>本方账户</legend>
                        <input id="borrowAccname" name="borrowAccname" class="mini-textbox" labelField="true"
                               label="资金账户户名：" vtype="maxLength:1333" style="width:100%;"
                               labelStyle="text-align:left;width:120px;"/>
                        <input id="borrowOpenBank" name="borrowOpenBank" class="mini-textbox" labelField="true"
                               label="资金开户行：" vtype="maxLength:1333" labelStyle="text-align:left;width:120px;"
                               style="width:100%;"/>
                        <input id="borrowAccnum" name="borrowAccnum" class="mini-textbox" labelField="true"
                               label="资金账号：" maxLength="400" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])"
                               labelStyle="text-align:left;width:120px;" style="width:100%;"/>
                        <input id="borrowPsnum" name="borrowPsnum" class="mini-textbox" labelField="true"
                               label="支付系统行号：" maxLength="400"
                               onvalidation="CommonUtil.onValidation(e,'alphanum',[null])"
                               labelStyle="text-align:left;width:120px;" style="width:100%;"/>
                        <input id="borrowCaname" name="borrowCaname" class="mini-textbox" labelField="true"
                               label="托管账户户名：" vtype="maxLength:1333" labelStyle="text-align:left;width:120px;"
                               style="width:100%;"/>
                        <input id="borrowCustname" name="borrowCustname" class="mini-buttonedit mini-mustFill"
                               onbuttonclick="onSaccQuery" labelField="true" label="托管机构：" required="true"
                               labelStyle="text-align:left;width:120px;" style="width:100%;" allowInput="false"/>
                        <input id="borrowCanum" name="borrowCanum" class="mini-textbox" labelField="true" label="托管帐号："
                               maxLength="400" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])"
                               labelStyle="text-align:left;width:120px;" style="width:100%;"/>
                    </fieldset>
                </div>
                <div class="leftarea">
                    <fieldset>
                        <legend>对手方账户</legend>
                        <input id="lendAccname" name="lendAccname" class="mini-textbox" labelField="true"
                               label="资金账户户名：" vtype="maxLength:1333" style="width:100%;"
                               labelStyle="text-align:left;width:120px;"/>
                        <input id="lendOpenBank" name="lendOpenBank" class="mini-textbox" labelField="true"
                               label="资金开户行：" vtype="maxLength:1333" labelStyle="text-align:left;width:120px;"
                               style="width:100%;"/>
                        <input id="lendAccnum" name="lendAccnum" class="mini-textbox" labelField="true" label="资金账号："
                               maxLength="400" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])"
                               labelStyle="text-align:left;width:120px;" style="width:100%;"/>
                        <input id="lendPsnum" name="lendPsnum" class="mini-textbox" labelField="true" label="支付系统行号："
                               maxLength="400" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])"
                               labelStyle="text-align:left;width:120px;" style="width:100%;"/>
                        <input id="lendCaname" name="lendCaname" class="mini-textbox" labelField="true" label="托管账户户名："
                               vtype="maxLength:1333" labelStyle="text-align:left;width:120px;" style="width:100%;"/>
                        <input id="lendCustname" name="lendCustname" class="mini-buttonedit mini-mustFill"
                               onbuttonclick="onSaccQuery" labelField="true" label="托管机构：" required="true"
                               labelStyle="text-align:left;width:120px;" style="width:100%;" allowInput="false"/>
                        <input id="lendCanum" name="lendCanum" class="mini-textbox" labelField="true" label="托管帐号："
                               maxLength="400" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])"
                               labelStyle="text-align:left;width:120px;" style="width:100%;"/>
                    </fieldset>
                </div>
            </div>
            <%@ include file="../../Common/Flow/MiniApproveOpCommon.jsp" %>
        </div>
    </div>
    <div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
        <div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"
                                                                style="width:120px;" id="save_btn"
                                                                onclick="save">保存交易</a></div>
        <div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"
                                                                style="width:120px;" id="close_btn"
                                                                onclick="close">关闭界面</a></div>
    </div>
</div>
<script type="text/javascript">
    mini.parse();

    //获取当前tab
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var row = params.selectData;
    var detailData = row;
    var url = window.location.search;
    var action = CommonUtil.getParam(url, "action");

    var prdNo = CommonUtil.getParam(url, "prdNo");
    var fPrdCode = CommonUtil.getParam(url, "fPrdCode");
    var form = new mini.Form("#field_form");
    var grid = mini.get("datagrid");


    // grid.on("beforeload", function (e) {
    // 	e.cancel = true;
    // 	var pageIndex = e.data.pageIndex;
    // 	var pageSize = e.data.pageSize;
    // 	search(pageSize,pageIndex);
    // });
    var tradeData = {};

    tradeData.selectData = row;
    tradeData.operType = action;
    tradeData.serial_no = row.ticketId;
    tradeData.task_id = row.taskId;

    //加载机构树
    function nodeclick(e) {
        var oldvalue = e.sender.value;
        var param = mini.encode({"branchId": branchId}); //序列化成JSON
        var oldData = e.sender.data;
        if (oldData == null || oldData == "") {
            CommonUtil.ajax({
                url: "/InstitutionController/searchInstitutionByBranchId",
                data: param,
                callback: function (data) {
                    var obj = e.sender;
                    obj.setData(data.obj);
                    obj.setValue(oldvalue);
                }
            });
        }
    }

    //计算质押比例：标的债券市值总额/质押债券市值总额
    function calculateRatio() {
        debugger;
        var underlyingQty = mini.get("underlyingQty").getValue();
        var marginAmt = mini.get("marginAmt").getValue();

        var biaodi= mini.get("underlyingSecurityId").getValue();
        var zhiya=mini.get("marginSecuritiesId").getValue();

        if(underlyingQty==0||marginAmt==0||biaodi==''||zhiya==''){
            return 0;
        }
        //获取质押比例
        var url = "/IfsCfetsrmbSlController/getMarginRatio";
        var data = {underlyingQty:underlyingQty,marginAmt:marginAmt,biaodi:biaodi,zhiya:zhiya};
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: url,
            data: params,
            callback: function (data) {
                mini.get("marginRatio").setValue(data.obj);
            }
        });
    }

    function quotaJudge() {
        var quotaType = mini.get("quotaOccupyType").getValue();
        if ("0" == quotaType) {
            //不占额度，隐藏授信页面
            mini.get("panel1").setVisible(false);
            // mini.get("panel12").setVisible(false);
        } else if ("1" == quotaType) {
            //占用发行人额度
            mini.get("panel1").setVisible(true);
            // mini.get("panel12").setVisible(true);
            var bondCode = mini.get("underlyingSecurityId").getValue();
            creditsubvalue(null, "445", bondCode);
        } else {
            //占用交易对手
            mini.get("panel1").setVisible(true);
            // mini.get("panel12").setVisible(true);
            var cno = mini.get("lendInst").getValue();
            creditsubvalue(cno, "445", null);
        }
    }

    //质押券
    function onBondQuery(e) {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Ifs/bond/bondAddMini.jsp",
            title: "选择债券列表",
            width: 900,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.bndcd);
                        btnEdit.setText(data.bndcd);
                        mini.get("marginSymbol").setValue(data.bndnm_cn);
                        mini.get("marginSymbol").setText(data.bndnm_cn);
                        btnEdit.focus();
                    }
                }
            }
        });
    }

    //标的券
    function onSecurityQuery(e) {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Ifs/bond/bondAddMini.jsp",
            title: "选择债券列表",
            width: 900,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.bndcd);
                        btnEdit.setText(data.bndcd);
                        mini.get("underlyingSymbol").setValue(data.bndnm_cn);
                        mini.get("underlyingSymbol").setText(data.bndnm_cn);

                        //买入出钱设置授信主体
                        var myDir = mini.get("myDir").getValue();
                        if (myDir == 'S') {
                            if ("1" == mini.get("quotaOccupyType").value) {
                                //额度占用类型为发行人时。填充授信信息
                                creditsubvalue(null, "445", data.bndcd);
                            }
                        }
                        btnEdit.focus();
                    }
                }
            }
        });
    }

    function onCreditTypeChange() {
        var underlyingSecurityId = mini.get("underlyingSecurityId").getValue();
        creditsubvalue(null, "445", underlyingSecurityId);
    }

    function onPortCostQuery() {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "/../Common/OpicsTradeParamMini.jsp",
            title: "投资组合选择",
            width: 900,
            height: 500,
            onload: function () {//弹出页面加载完成
                var iframe = this.getIFrameEl();
                var data1 = {prdCode: prdCode};
                //调用弹出页面方法进行初始化
                iframe.contentWindow.SetData(data1);
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data); //必须
                    if (data) {
                        btnEdit.setValue(data.port);
                        btnEdit.setText(data.port);
                        mini.get("costb").setValue(data.cost);//成本中心
                        mini.get("prodb").setValue(data.product);//产品
                        // mini.get("prodtypeb").setValue(data.prodType);//产品类型
                        btnEdit.focus();
                    }

                }

            }
        });
    }

    //添加债券信息
    function add() {
        var detailForm = new mini.Form("#search_detail_form");
        var detailData = detailForm.getData(true);
        //||CommonUtil.isNull(detailData.prodb)||CommonUtil.isNull(detailData.costb)
        //||CommonUtil.isNull(detailData.invtype)||CommonUtil.isNull(detailData.prodtypeb)
        if (CommonUtil.isNull(detailData.marginSecuritiesId) || CommonUtil.isNull(detailData.marginAmt)) {
            mini.alert("请将债券信息填写完整", "系统提示");
            return;
        }
        addRow(detailData);
    }

    function addRow(detailData) {
        var row = {
            marginSecuritiesId: detailData.marginSecuritiesId,
            marginSymbol: detailData.marginSymbol,
            portb: detailData.portb,
            costb: detailData.costb,
            invtype: detailData.invtype,
            marginAmt: detailData.marginAmt,
            prodb: detailData.prodb,
            portb: detailData.portb,
            marginRatio: detailData.marginRatio
            // prodtypeb:detailData.prodtypeb
        };
        grid.addRow(row);
        var detailForm = new mini.Form("#search_detail_form");
        detailForm.clear();
        //重新计算券面总额合计(万元)
        calcAmtSum(grid.getData());
    }

    /**计算 券面总额合计*/
    function calcAmtSum(rows) {
        var marginTotalAmt = 0;
        for (var i = 0; i < rows.length; i++) {
            marginTotalAmt = marginTotalAmt + parseFloat(rows[i].marginAmt);
        }
        mini.get("marginTotalAmt").setValue(marginTotalAmt);
    }

    //删除
    function del() {
        var row = grid.getSelected();
        if (!row) {
            mini.alert("请选中要移除的一行", "提示");
            return;
        }
        grid.removeRow(row);
        //重新计算授信额度总和
        calcAmtSum(grid.getData());
    }

    //修改
    function edit() {
        var row = grid.getSelected();
        if (!row) {
            mini.alert("请选中一行进行修改!", "提示");
            return;
        }
        var detailForm = new mini.Form("#search_detail_form");
        detailForm.setData(row);
        mini.get("marginSecuritiesId").setValue(row.marginSecuritiesId);
        mini.get("marginSecuritiesId").setText(row.marginSecuritiesId);
        //投资组合
        mini.get("portb").setValue(row.portb);
        mini.get("portb").setText(row.portb);
        //成本中心
        mini.get("costb").setValue(row.costb);
        mini.get("costb").setText(row.costb);
        //产品代码
        mini.get("prodb").setValue(row.prodb);
        mini.get("prodb").setText(row.prodb);
        //产品类型
        // mini.get("prodtypeb").setValue(row.prodtypeb);
        // mini.get("prodtypeb").setText(row.prodtypeb);

        mini.get("add_btn").setEnabled(false);
        mini.get("edit_btn").setEnabled(false);
        mini.get("saves_btn").setEnabled(true);
    }

    //修改保存
    function saves() {
        del();
        add();
        mini.get("add_btn").setEnabled(true);
        mini.get("edit_btn").setEnabled(true);
        mini.get("saves_btn").setEnabled(false);
    }

    //初始化债券信息
    function initGrid() {
        var url = "/IfsCfetsrmbSlController/searchBondSlDetails";
        var data = {};
        data['ticketId'] = mini.get("ticketId").getValue();
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: url,
            data: params,
            callback: function (data) {
                grid.setData(data.obj.rows);
            }
        });
    }

    //保存
    function save() {
        //表单验证！！！
        form.validate();
        if (form.isValid() == false) {
            mini.alert("信息填写有误，请重新填写", "系统提示");
            return;
        }
        //校验opcis
        if (!checkOpics()) {
            return;
        }
        var data = form.getData(true);
        var rows = grid.getData();
        data.custNo = mini.get("creditCno").getValue();
        if (toDecimal(data['miscFeeType']) != toDecimal(data['underlyingQty'] * 10000 * data['price'] * data['occupancyDays'] / 100 / 365) || data['tenor'] != data['occupancyDays']) {
            mini.confirm("确认以当前数据为准吗？", "确认", function (action) {
                if (action != "ok") {
                    return;
                }
                data['borrowInst'] = "<%=__sessionInstitution.getInstId()%>";
                data['borrowTrader'] = "<%=__sessionUser.getUserId() %>";
                data['sponInst'] = "<%=__sessionInstitution.getInstId()%>";
                data['sponsor'] = "<%=__sessionUser.getUserId() %>";
// 				data['dealTransType']="M";
                data['bondDetailsList'] = rows;
                data['fPrdCode'] = fPrdCode;
                var params = mini.encode(data);
                CommonUtil.ajax({
                    url: "/IfsCfetsrmbSlController/saveSl",
                    data: params,
                    callback: function (data) {
                        mini.alert(data.desc, '提示信息', function () {
                            if (data.code == 'error.common.0000') {
                                top["win"].closeMenuTab();//关闭并刷新当前界面
                            }
                        });
                    }
                });
            });
        } else {
            data['borrowInst'] = "<%=__sessionInstitution.getInstId()%>";
            data['borrowTrader'] = "<%=__sessionUser.getUserId() %>";
            data['sponInst'] = "<%=__sessionInstitution.getInstId()%>";
            data['sponsor'] = "<%=__sessionUser.getUserId() %>";
// 			data['dealTransType']="M";
            data['bondDetailsList'] = rows;
            data['fPrdCode'] = fPrdCode;
            var params = mini.encode(data);
            CommonUtil.ajax({
                url: "/IfsCfetsrmbSlController/saveSl",
                data: params,
                callback: function (data) {
                    mini.alert(data.desc, '提示信息', function () {
                        if (data.code == 'error.common.0000') {
                            top["win"].closeMenuTab();//关闭并刷新当前界面
                        }
                    });
                }
            });
        }
    }

    function close() {
        top["win"].closeMenuTab();
    }

    //文、数字、下划线 的验证
    function onEnglishAndNumberValidations(e) {
        if (e.value == "" || e.value == null) {//值为空，就不做校验
            return;
        }
        if (e.isValid) {
            if (isEnglishAndNumbers(e.value) == false) {
                e.errorText = "必须输入英文小写+数字";
                e.isValid = false;
            }
        }
    }

    /* 是否英文+数字 */
    function isEnglishAndNumbers(v) {
        var re = new RegExp("^[0-9a-z\_\-]+$");
        if (re.test(v)) return true;
        return false;
    }


    //英文、数字、下划线 的验证
    function onEnglishAndNumberValidation(e) {
        if (e.value == "" || e.value == null) {//值为空，就不做校验
            return;
        }
        if (e.isValid) {
            if (isEnglishAndNumber(e.value) == false) {
                e.errorText = "必须输入英文+数字";
                e.isValid = false;
            }
        }
    }

    /* 是否英文+数字 */
    function isEnglishAndNumber(v) {
        var re = new RegExp("^[0-9a-zA-Z\_\-]+$");
        if (re.test(v)) return true;
        return false;
    }

    function loadInstitutionTree() {
        CommonUtil.ajax({
            data: {"branchId": branchId},
            url: "/InstitutionController/searchInstitutions",
            callback: function (data) {
                mini.get("attributionDept").setData(data.obj);
                if (detailData != null && detailData.attributionDept != null) {
                    mini.get("attributionDept").setValue(detailData.attributionDept);
                } else {
                    mini.get("attributionDept").setValue("<%=__sessionInstitution.getInstId()%>");
                }
            }
        });
    }

    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function (visibleBtn) {
            if ($.inArray(action, ["approve", "detail"]) > -1) {
                mini.get("save_btn").setVisible(false);
                form.setEnabled(false);
                var approForm = new mini.Form("#approve_operate_form");
                approForm.setEnabled(true);
                mini.get("forDate").setEnabled(false);
                mini.get("toolbar").setVisible(false);
                mini.get("marginSecuritiesId").setVisible(false);
                mini.get("marginSymbol").setVisible(false);
                mini.get("marginAmt").setVisible(false);
            }
            if ($.inArray(action, ["add", "edit"]) > -1) {
                mini.get("toolbar").setVisible(true);
                mini.get("marginSecuritiesId").setVisible(true);
                mini.get("marginSymbol").setVisible(true);
                mini.get("marginAmt").setVisible(true);
                mini.get("panel1").setVisible(false);
                // mini.get("panel12").setVisible(false);
                mini.get("quotaOccupyType").setVisible(false);
            }
            if ($.inArray(action, ["edit", "approve", "detail"]) > -1) {
                form.setData(row);
                mini.get("marginRatio").setValue(0);
                mini.get("marginSymbol").setValue('');
                mini.get("marginAmt").setValue(0);
                initGrid();
                mini.get("forDate").setValue(row.forDate);
                mini.get("ticketId").setValue(row.ticketId);
                //投资组合
                mini.get("port").setValue(row.port);
                mini.get("port").setText(row.port);
                //成本中心
                mini.get("cost").setValue(row.cost);
                mini.get("cost").setText(row.cost);
                //产品代码
                mini.get("product").setValue(row.product);
                mini.get("product").setText(row.product);
                //产品类型
                mini.get("prodType").setValue(row.prodType);
                mini.get("prodType").setText(row.prodType);
                //标的券代码
                mini.get("underlyingSecurityId").setValue(row.underlyingSecurityId);
                mini.get("underlyingSecurityId").setText(row.underlyingSecurityId);
                //标的券投资组合
                mini.get("bondPort").setValue(row.bondPort);
                mini.get("bondPort").setText(row.bondPort);
                //标的券成本中心
                mini.get("bondCost").setValue(row.bondCost);
                mini.get("bondCost").setText(row.bondCost);
                //对方机构lendInst
                mini.get("lendInst").setValue(row.cno);
// 			mini.get("lendInst").setText(row.lendInst); 
                queryTextName(row.cno);

                if (row.cfetsno == null) {
                    mini.get("cfetsno").setValue(row.cno);
                }

                mini.get("borrowInst").setValue(row.borrowInst);
                mini.get("borrowTrader").setValue(row.myUserName);
                mini.get("contractId").setEnabled(false);

                //清算路径
                mini.get("ccysmeans").setValue(row.ccysmeans);
                mini.get("ccysmeans").setText(row.ccysmeans);
                mini.get("ctrsmeans").setValue(row.ctrsmeans);
                mini.get("ctrsmeans").setText(row.ctrsmeans);

                mini.get("borrowCustname").setValue(row.borrowCustname);
                mini.get("borrowCustname").setText(row.borrowCustname);
                mini.get("lendCustname").setValue(row.lendCustname);
                mini.get("lendCustname").setText(row.lendCustname);
//			
//			mini.get("applyNo").setValue(row.applyNo);
//			mini.get("applyNo").setText(row.applyNo);
//			queryCustNo(row.custNo);
                mini.get("panel1").setVisible(false);
                dirVerify();

                if (action == "edit" && row.weight == null) {
//				searchProductWeightNew();
                }

                //借出出钱设置授信主体
                // if(row.myDir == 'S') {creditsubvalue(row.custNo,"445",null);}

            } else {
                mini.get("sponsor").setValue("<%=__sessionUser.getUserId()%>");
                mini.get("sponInst").setValue("<%=__sessionUser.getInstId() %>");
                mini.get("borrowInst").setValue("<%=__sessionInstitution.getInstFullname()%>");
                mini.get("borrowTrader").setValue("<%=__sessionUser.getUserName() %>");
                mini.get("forDate").setValue("<%=__bizDate %>");
                //产品代码
                mini.get("product").setValue("SECLEN");
                mini.get("product").setText("SECLEN");
            }
            mini.get("dealTransType").setEnabled(false);
            loadInstitutionTree();
        });
    });

    function onButtonEdit(e) {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Common/CustMini.jsp",
            title: "选择对手方列表",
            width: 900,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.cno);
                        btnEdit.setText(data.cliname);
                        btnEdit.focus();
                        //判断额度占用类型是否为交易对手
                        if ("2" == mini.get("quotaOccupyType").value) {
                            creditsubvalue(data.cno, "445", null);
                        }
                    }
                }

            }
        });
    }

    //根据交易对手编号查询全称
    function queryTextName(cno) {
        CommonUtil.ajax({
            url: "/IfsOpicsCustController/searchIfsOpicsCust",
            data: {'cno': cno},
            callback: function (data) {
                if (data && data.obj) {
                    mini.get("lendInst").setText(data.obj.cliname);
                } else {
                    mini.get("lendInst").setText(cno);
                }
            }
        });
    }

    //获取债券发行人
    function initBond(bndcd) {
        CommonUtil.ajax({
            url: "/IfsOpicsBondController/searchOneById",
            data: {"bndcd": bndcd},
            callback: function (data) {
                if (data != null) {
                    return data.issuer;
                } else {
                    return null;
                }
            }
        });
    }

    function onDrawDateFirst(e) {
        var firstDate = e.date;
        var secondDate = mini.get("secondSettlementDate").getValue();
        if (CommonUtil.isNull(secondDate)) {
            return;
        }
        if (secondDate.getTime() < firstDate.getTime()) {
            e.allowSelect = false;
        }
    }

    function onDrawDateSecond(e) {
        var secondDate = e.date;
        var firstDate = mini.get("firstSettlementDate").getValue();
        if (CommonUtil.isNull(firstDate)) {
            return;
        }
        if (secondDate.getTime() < firstDate.getTime()) {
            e.allowSelect = false;
        }
    }

    function linkageCalculatFee() {
        var basis = mini.get("basis").getValue();
        var occupancyDays = mini.get("occupancyDays").getValue();
        var rate = mini.get("price").getValue();
        var tradeAmount = mini.get("underlyingQty").getValue();
        if (basis != "" && occupancyDays != "" && rate != "" && tradeAmount != "") {
            if (basis == "A360") {
                mini.get("miscFeeType").setText(tradeAmount * 10000 * rate * occupancyDays / 100 / 360);
                mini.get("miscFeeType").setValue(tradeAmount * 10000 * rate * occupancyDays / 100 / 360);
            } else if (basis == "A365") {
                mini.get("miscFeeType").setText(tradeAmount * 10000 * rate * occupancyDays / 100 / 365);
                mini.get("miscFeeType").setValue(tradeAmount * 10000 * rate * occupancyDays / 100 / 365);
            }
        }

        var underlyingQty = mini.get("underlyingQty").getValue();
        var marginAmt = mini.get("marginAmt").getValue();
        if (marginAmt == 0) {
            mini.get("marginRatio").setValue(0);
        } else {
            mini.get("marginRatio").setValue(underlyingQty / marginAmt);
        }

    }

    function linkageCalculatDay(e) {
        var firstDate = mini.get("firstSettlementDate").getValue();
        var secondDate = mini.get("secondSettlementDate").getValue();
        if (CommonUtil.isNull(firstDate) || CommonUtil.isNull(secondDate)) {
            return;
        }
        var subDate = Math.abs(parseInt((secondDate.getTime() - firstDate.getTime()) / 1000 / 3600 / 24));
        mini.get("tenor").setValue(subDate);
        mini.get("occupancyDays").setValue(subDate);
        var occupancyDays = mini.get("occupancyDays").getValue();
        var rate = mini.get("price").getValue();
        var tradeAmount = mini.get("underlyingQty").getValue();
        linkageCalculatFee();
//		searchProductWeightNew();
    }

    function linkageCalculat(e) {
        var occupancyDays = e.value;
        var rate = mini.get("price").getValue();
        var tradeAmount = mini.get("underlyingQty").getValue();
        var basis = mini.get("basis").getValue();
        if (basis != "" && occupancyDays != "" && rate != "" && tradeAmount != "") {
            if (basis == "A360") {
                mini.get("miscFeeType").setText(tradeAmount * 10000 * rate * occupancyDays / 100 / 360);
                mini.get("miscFeeType").setValue(tradeAmount * 10000 * rate * occupancyDays / 100 / 360);
            } else if (basis == "A365") {
                mini.get("miscFeeType").setText(tradeAmount * 10000 * rate * occupancyDays / 100 / 365);
                mini.get("miscFeeType").setValue(tradeAmount * 10000 * rate * occupancyDays / 100 / 365);
            }
        }
    }


    /**   清算路径1的选择   */
    function settleMeans1() {
        var cpInstId = mini.get("lendInst").getValue();
        if (cpInstId == null || cpInstId == "") {
            mini.alert("请先选择对方机构!");
            return;
        }
        var ccy = "CNY";
        var url = "./Ifs/opics/setaMini.jsp";
        var data = {ccy: ccy, cust: cpInstId};

        var btnEdit = this;
        mini.open({
            url: url,
            title: "选择清算路径",
            width: 900,
            height: 600,
            onload: function () {
                var iframe = this.getIFrameEl();
                iframe.contentWindow.SetData(data);
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data1 = iframe.contentWindow.GetData();
                    data1 = mini.clone(data1);    //必须
                    if (data1) {
                        mini.get("ccysmeans").setValue($.trim(data1.smeans));
                        mini.get("ccysmeans").setText($.trim(data1.smeans));
                        mini.get("ccysacct").setValue($.trim(data1.sacct));
                        btnEdit.focus();
                    }
                }
            }
        });
    }

    /**   清算路径2的选择   */
    function settleMeans2() {
        var cpInstId = mini.get("lendInst").getValue();
        if (cpInstId == null || cpInstId == "") {
            mini.alert("请先选择对方机构!");
            return;
        }
        var ccy = "CNY";
        var url = "./Ifs/opics/setaMini.jsp";
        var data = {ccy: ccy, cust: cpInstId};

        var btnEdit = this;
        mini.open({
            url: url,
            title: "选择清算路径",
            width: 900,
            height: 600,
            onload: function () {
                var iframe = this.getIFrameEl();
                iframe.contentWindow.SetData(data);
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data1 = iframe.contentWindow.GetData();
                    data1 = mini.clone(data1);    //必须
                    if (data1) {
                        mini.get("ctrsmeans").setValue($.trim(data1.smeans));
                        mini.get("ctrsmeans").setText($.trim(data1.smeans));
                        mini.get("ctrsacct").setValue($.trim(data1.sacct));
                        btnEdit.focus();
                    }
                }

            }
        });
    }

    //托管机构选择
    function onSaccQuery(e) {
        var btnEdit = this;
        mini.open({
            url: "./Ifs/opics/saccMini.jsp",
            title: "选择托管机构列表",
            width: 800,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.accountno);
                        btnEdit.setText(data.accountno);
                        btnEdit.focus();
                    }
                }

            }
        });
    }

    /* 产品代码的选择 */
    function onPrdEdit1() {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "/opics/prodMiniLess.jsp",
            title: "选择产品代码",
            width: 900,
            height: 600,
            onload: function () {
                var iframe = this.getIFrameEl();
                iframe.contentWindow.SetData({});
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.pcode);
                        btnEdit.setText(data.pcode);
                        onPrdChanged();
                        btnEdit.focus();
                    }
                }

            }
        });

    }

    function onPrdChanged() {
        if (mini.get("prodtypeb").getValue() != "") {
            mini.get("prodtypeb").setValue("");
            mini.get("prodtypeb").setText("");
            mini.alert("产品代码已改变，请重新选择产品类型!");
        }
    }

    /* 产品类型的选择 */
    function onTypeEdit1() {
        var prd = mini.get("prodb").getValue();
        if (prd == null || prd == "") {
            mini.alert("请先选择产品代码!");
            return;
        }
        var data = {prd: prd};
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "/opics/typeMiniLess.jsp",
            title: "选择产品类型",
            width: 900,
            height: 600,
            onload: function () {
                var iframe = this.getIFrameEl();
                iframe.contentWindow.SetData(data);
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.type);
                        btnEdit.setText(data.type);
                        btnEdit.focus();
                    }
                }

            }
        });
    }

    /* 质押投资组合的选择 */
    function onBondPortEdit1() {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "/opics/portMiniLess.jsp",
            title: "选择投资组合",
            width: 900,
            height: 500,
            onload: function () {
                var iframe = this.getIFrameEl();
                iframe.contentWindow.SetData({});
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.portfolio);
                        btnEdit.setText(data.portfolio);
                        btnEdit.focus();
                    }
                }

            }
        });

    }

    /* 成功中心的选择 */
    function onCostEdit1() {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "/opics/costMiniLess.jsp",
            title: "选择成本中心",
            width: 900,
            height: 600,
            onload: function () {
                var iframe = this.getIFrameEl();
                iframe.contentWindow.SetData({});
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.costcent);
                        btnEdit.setText(data.costcent);
                        btnEdit.focus();
                    }
                }

            }
        });

    }

    /* 成功中心的选择 */
    function onBondCostEdit() {
        var btnEdit = this;
        var data = {prdName: "SECUR"};
        mini.open({
            url: CommonUtil.baseWebPath() + "/opics/costMiniLess.jsp",
            title: "选择成本中心",
            width: 900,
            height: 500,
            onload: function () {
                var iframe = this.getIFrameEl();
                iframe.contentWindow.SetData(data);
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.costcent);
                        btnEdit.setText(data.costcent);
                        btnEdit.focus();
                    }
                }

            }
        });
    }


    /* 券投资组合的选择 */
    function onBondPortEdit() {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "/opics/portMiniLess.jsp",
            title: "选择投资组合",
            width: 900,
            height: 500,
            onload: function () {
                var iframe = this.getIFrameEl();
                iframe.contentWindow.SetData({});
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.portfolio);
                        btnEdit.setText(data.portfolio);
                        btnEdit.focus();
                    }
                }

            }
        });

    }

    //opics处理校验
    function checkOpics() {
        //不允许做倒起息
        var forDate = mini.formatDate(mini.get("forDate").getValue(), 'yyyy-MM-dd');
        var firstSettlementDate = mini.formatDate(mini.get("firstSettlementDate").getValue(), 'yyyy-MM-dd');
        var opicsFlag = CommonUtil.dateCompare(firstSettlementDate, forDate);
        if (!opicsFlag) {
            mini.alert("首次结算日要大于等于成交日期!");
        }
        return opicsFlag;
    }

    function dirVerify(e) {
        var myDir = mini.get("myDir").getValue();
        if (myDir == 'P') {//借入
            mini.get("panel1").setVisible(false);
            // mini.get("panel12").setVisible(false);
            mini.get("quotaOccupyType").setVisible(false);
            mini.get("oppoDir").setValue("S");
            mini.get("lendTrader").setStyle("margin-bottom: 2px")
            // mini.get("applyNo").setVisible(false);
            // mini.get("applyProd").setVisible(false);
            // mini.get("custNo").setVisible(false);
            // mini.get("weight").setVisible(false);
            /* mini.get("custNo").setValue("




            <%=__sessionUser.getInstId() %>");//占用授信主体
			mini.get("custNo").setText("




            <%=__sessionInstitution.getInstFullname()%>");//占用授信主体 */
        } else if (myDir == 'S') {//借出
            if ("0" != mini.get("quotaOccupyType").getValue()) {
                mini.get("panel1").setVisible(true);
                if ("1" == mini.get("quotaOccupyType").getValue()) {
                    var underlyingSecurityId = mini.get("underlyingSecurityId").getValue();
                    creditsubvalue(null, "445", underlyingSecurityId);
                } else {
                    var cno = mini.get("lendInst").getValue();
                    creditsubvalue(cno, "445", null);
                }

            }
            // mini.get("panel12").setVisible(true);
            mini.get("quotaOccupyType").setVisible(true);
            mini.get("oppoDir").setValue("P");
            mini.get("lendTrader").setStyle("margin-bottom: 36px")
            // mini.get("applyNo").setVisible(false);
            // mini.get("applyProd").setVisible(false);
            // mini.get("custNo").setVisible(true);
            // mini.get("weight").setVisible(true);
            /* mini.get("custNo").setValue(mini.get("sellInst").getValue());//产品名称
            mini.get("custNo").setValue(mini.get("sellInst").getText());//产品名称 */


        }
    }

    /* 成功中心的选择 */
    //    function onCostEdit(){
    //    	var btnEdit = this;
    //        mini.open({
    //            url: CommonUtil.baseWebPath() + "/opics/costMiniLess.jsp",
    //            title: "选择成本中心",
    //            width: 900,
    //            height: 500,
    //            ondestroy: function (action) {
    //                if (action == "ok") {
    //                    var iframe = this.getIFrameEl();
    //                    var data = iframe.contentWindow.GetData();
    //                    data = mini.clone(data);    //必须
    //                    if (data) {
    //                        btnEdit.setValue(data.costcent);
    //                        btnEdit.setText(data.costcent);
    //                        //searchMarketData();
    //                        btnEdit.focus();
    //                    }
    //                }
    //            }
    //        });
    //    }

    function searchMarketData() {
        var costCent = mini.get("cost").getValue();//成本中心
        var code = mini.get("investType").getValue();//交易品种
        if (CommonUtil.isNull(code) || CommonUtil.isNull(costCent)) {
            return;
        }
        queryMarketData(code, costCent);
    }

    //获取DV01，久期限，止损值查询
    function queryMarketData(code, costCent) {
        CommonUtil.ajax({
            url: "/IfsOpicsCustController/queryMarketData",
            data: {'code': code, 'costCent': costCent},
            callback: function (data) {
                if (data && data.obj) {
                    mini.get("longLimit").setValue(data.obj.duration);//久期限
                    mini.get("lossLimit").setValue(data.obj.convexity);//止损
                    mini.get("DV01Risk").setValue(data.obj.delta);//DV01
                } else {
                    mini.get("longLimit").setValue();//久期限
                    mini.get("lossLimit").setValue();//止损
                    mini.get("DV01Risk").setValue();//DV01
                }
            }
        });
    }

    //获取权重
    function searchProductWeightNew() {
        var form = new mini.Form("field_form");
        var data = form.getData(true);
        var mDate = data.secondSettlementDate;
        var vDate = data.forDate;//交易日期
        if (CommonUtil.isNull(mDate) || CommonUtil.isNull(vDate)) {

        }
//    	searchProductWeight(20,0,prdNo,mDate);
    }

</script>
<script type="text/javascript" src="../../Common/Flow/MiniApproveOpCommon.js"></script>
</body>
</html>
