<%@ page language="java" pageEncoding="UTF-8"%>
<div class="mini-panel" title="成交双方信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
		<div class="leftarea">
			<fieldset>
				<legend>本方信息</legend>
				<input id="myDir" name="myDir" class="mini-combobox"  labelField="true"  onvaluechanged="dirVerify" label="本方方向："  style="width:100%;"  required="true"   labelStyle="text-align:left;" data = "CommonUtil.serverData.dictionary.trading" />
				<input id="ownInst"  name="ownInst" class="mini-textbox"  labelField="true"  label="机构："  style="width:100%;" enabled="false" required="true"   labelStyle="text-align:left;"/>
				<input id="ownTrader" name="ownTrader" class="mini-textbox" labelField="true"  label="交易员："  required="true"   vtype="maxLength:5" enabled="false" labelStyle="text-align:left;" style="width:100%;"   />
				<input id="ownTel" name="ownTel" class="mini-textbox" labelField="true"  label="电话：" onvalidation="CommonUtil.onValidation(e,'phone',[null])"  labelStyle="text-align:left;" style="width:100%;"   />
				<input id="ownFax" name="ownFax" class="mini-textbox" labelField="true"  label="传真："  onvalidation="CommonUtil.onValidation(e,'phone',[null])"  labelStyle="text-align:left;" style="width:100%;"   />
				<input id="ownCorp" name="ownCorp" class="mini-textbox" labelField="true"  label="法人代表："  vtype="maxLength:10" labelStyle="text-align:left;" style="width:100%;"   />
				<input id="ownAddr" name="ownAddr" class="mini-textbox" labelField="true"  label="地址："  vtype="maxLength:50"  labelStyle="text-align:left;" style="width:100%;"   />
			</fieldset>
		</div>
		<div class="rightarea">
			<fieldset>
				<legend>对方信息</legend>
				<input id="oppoDir" name="oppoDir" class="mini-combobox"  enabled="false"  labelField="true" label="本方方向："  required="true"   style="width:100%;"  labelStyle="text-align:left;" data = "CommonUtil.serverData.dictionary.trading" />
				<input id="otherInst" name="otherInst" class="mini-buttonedit" onbuttonclick="onButtonEdit" labelField="true"  required="true"   label="机构：" vtype="maxLength:50"  style="width:100%;"  labelStyle="text-align:left;" />
				<input id="otherTrader" name="otherTrader" class="mini-textbox" labelField="true"  label="交易员："  required="true"  vtype="maxLength:10" labelStyle="text-align:left;" style="width:100%;"   />
				<input id="otherTel" name="otherTel" class="mini-textbox" labelField="true"  label="电话："  onvalidation="CommonUtil.onValidation(e,'phone',[null])"  labelStyle="text-align:left;" style="width:100%;"   />
				<input id="otherFax" name="otherFax" class="mini-textbox" labelField="true"  label="传真："  onvalidation="CommonUtil.onValidation(e,'phone',[null])"  labelStyle="text-align:left;" style="width:100%;"   />
				<input id="otherCorp" name="otherCorp" class="mini-textbox" labelField="true"  label="法人代表："  vtype="maxLength:10" labelStyle="text-align:left;" style="width:100%;"   />
				<input id="otherAddr" name="otherAddr" class="mini-textbox" labelField="true"  label="地址："  vtype="maxLength:50"  labelStyle="text-align:left;" style="width:100%;"   />
			</fieldset>
		</div>
</div>


