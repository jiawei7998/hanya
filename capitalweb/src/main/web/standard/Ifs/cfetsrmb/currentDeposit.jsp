<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
    <title>存放同业(活期)</title>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
</head>
<body style="width:100%;height:100%;background:white">
    <fieldset class="mini-fieldset title">
        <legend>基金基本信息查询</legend>
        <div>
            <div id="search_form" style="width:100%" cols="6">
                <input id="startDate" name="startDate" class="mini-datepicker" labelField="true"  label="业务开始日期："
                       ondrawdate="onDrawDateStart" format="yyyy-MM-dd" value="<%=__bizDate%>"  labelStyle="text-align:right;" emptyText="开始日期" />
                <span>~</span>
                <input id="endDate" name="endDate" class="mini-datepicker"
                       ondrawdate="onDrawDateEnd" emptyText="结束日期" format="yyyy-MM-dd" value="<%=__bizDate%>"/>
                <span style="float: right; margin-right: 150px">
                    <a id="search_btn" class="mini-button" style="display: none"   onclick="search()">查询</a>
                    <a id="clear_btn" class="mini-button" style="display: none"  onclick="clear()">清空</a>
                </span>
            </div>
        </div>
        <span style="margin:2px;display: block;">
            <a  id="export_btn" class="mini-button"    style="display: none" onclick="ExcelExport()">下载模板</a>
            <a  id="upload_btn" class="mini-button"   style="display: none" onclick="ExcelImport()">上传交易信息</a>
            <span style="color: red"> 请先下载模板</span>
        </span>
    </fieldset>

    <div class="mini-fit" style="width:100%;height:100%;">
        <div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
             sortMode="client" allowAlternating="true"  idField="fundCode"  allowResize="true" >
            <div property="columns">
                <div type="indexcolumn" width="50px" headerAlign="center">序号</div>
                <div field="branch" width="100px" allowSort="false" headerAlign="center" align="center">支行名称</div>
                <div field="custName" width="100px" allowSort="false" headerAlign="center" align="center">交易对手总部全称</div>
                <div field="sfn" width="100px" allowSort="false" headerAlign="center" align="center">交易对手（全称）</div>
                <div field="custArea" width="100px" allowSort="false" headerAlign="center" align="center">交易对手地域</div>
                <div field="curFix" width="100px" allowSort="false" headerAlign="center" align="center">活期/定期</div>
                <div field="vdate" width="100px" allowSort="false" headerAlign="center" align="center">业务开始日期</div>
                <div field="mdate" width="100px" allowSort="false" headerAlign="center" align="center">业务结束日期</div>
                <div field="rate" width="100px" allowSort="false" headerAlign="center" align="center">利率</div>
                <div field="amt" width="100px" allowSort="false" headerAlign="center" align="center" numberFormat="n4">余额</div>
                <div field="subject" width="100px" allowSort="false" headerAlign="center" align="center">记帐科目</div>
                <div field="custType" width="100px" allowSort="false" headerAlign="center" align="center">交易对手类型</div>
                <div field="dealType" width="100px" allowSort="false" headerAlign="center" align="center">交易类型</div>
            </div>
        </div>
    </div>
</body>
<script>
    /****************************初始化**********************************************/
    mini.parse();

    var form = new mini.Form("#search_form");
    var grid = mini.get("datagrid");
    var url=window.location.search;
    var action=CommonUtil.getParam(url,"action");


    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        searchs(pageSize,pageIndex);
    });
    $(document).ready(function() {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
            search();
        });
    });

    /****************************按钮**********************************************/
    //查询按钮
    function search(){
        searchs(10,0);
    }

    function searchs(pageSize, pageIndex) {
        form.validate();
        if (form.isValid() == false) {
            mini.alert("表单填写错误,请确认!", "提示信息");
            return;
        }
        var data = form.getData(true);
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        data['branchId']=branchId;
        var params = mini.encode(data);
        CommonUtil.ajax({
            url : "/CurrentDepositController/getCurrentDepositPage",
            data : params,
            callback : function(data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }

    //清空
    function clear(){
        var form=new mini.Form("search_form");
        form.clear();
        search(10,0);
    }

    //基金信息文件导入
    function ExcelImport(){
        mini.open({
            showModal: true,
            allowResize: true,
            url: CommonUtil.baseWebPath() +"/../Ifs/cfetsrmb/currentDepositImport.jsp",
            width: 420,
            height: 300,
            title: "Excel导入",
            ondestroy: function (action) {
                search(10,0);//重新加载页面
            }
        });
    }

    //导出模板
    function ExcelExport() {
        mini.confirm("您确认要导出Excel吗?","系统提示",
            function (action) {
                if (action == "ok"){
                    var form = new mini.Form("#search_form");
                    var data = form.getData(true);
                    var fields = null;
                    for(var id in data){
                        fields += '<input type="hidden" id="' + id + '" name="'+ id +'" value="' + data[id] + '">';
                    }
                    var urls = CommonUtil.pPath + "/sl/CurrentDepositController/exportcurrentDepositoutTemplate  ";
                    $('<form action="'+ urls +'" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();
                }
            }
        );
    }

    /****************************其他**********************************************/
    //成交日期
    function onDrawDateStart(e) {
        var startDate = e.date;
        var endDate= mini.get("endDate").getValue();
        if(CommonUtil.isNull(endDate)){
            return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }
    function onDrawDateEnd(e) {
        var endDate = e.date;
        var startDate = mini.get("startDate").getValue();
        if(CommonUtil.isNull(startDate)){
            return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }

</script>
</html>