<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../../global.jsp" %>
<html>
<head>
    <script type="text/javascript" src="./rmbVerify.js"></script>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/standard/Common/RiskCenter.js"></script>
    <title>同业存款 维护</title>
    <script type="text/javascript">
    </script>
</head>

<body style="width:100%;height:100%;background:white">
<div class="mini-splitter" style="width:100%;height:100%;">
    <div size="90%" showCollapseButton="false">
        <h1 style="text-align:center"><strong>同业存款</strong></h1>
        <div id="field_form" class="mini-fit area" style="background:white">
            <input id="aDate" name="aDate" class="mini-hidden"/>
            <div class="mini-panel" title="审批单编号" style="width:100%;" allowResize="true" collapseOnTitleClick="true">
                <div class="leftarea">
                    <input style="width:100%;" id="ticketId" enabled="false" name="ticketId" class="mini-textbox"
                           labelField="true" label="审批单编号：" labelStyle="text-align:left;width:130px;"
                           vtype="maxLength:20" onvalidation="onEnglishAndNumberValidation"/>
                </div>
            </div>
            <div class="mini-panel" title="成交双方信息" style="width:100%" allowResize="true" collapseOnTitleClick="true">
                <div class="leftarea">
                    <fieldset>
                        <legend>本方信息</legend>
                        <input id="sponInst" name="sponInst" class="mini-textbox mini-mustFill" labelField="true"
                               label="发起机构：" style="width:100%;" required="true" enabled="false"
                               labelStyle="text-align:left;width:120px;"/>
                        <input id="sponsor" name="sponsor" class="mini-textbox mini-mustFill" labelField="true"
                               label="发起人：" vtype="maxLength:20" required="true" enabled="false"
                               labelStyle="text-align:left;width:120px;" style="width:100%;"/>
                    </fieldset>
                </div>
                <div class="rightarea">
                    <fieldset>
                        <legend>对手方信息</legend>
                        <input id="counterpartyInstId" name="counterpartyInstId" class="mini-buttonedit mini-mustFill"
                               onbuttonclick="onButtonEdit" labelField="true" allowInput="false" label="客户号："
                               style="width:100%;" labelStyle="text-align:left;width:120px;" vtype="maxLength:16"
                               requiredErrorText="该输入项为必输项" required="true"/>
                        <input id="cname" name="cname" class="mini-textbox mini-mustFill" labelField="true"
                               label="客户名称：" vtype="maxLength:20" labelStyle="text-align:left;width:120px;"
                               style="width:100%;" required="true"/>
                    </fieldset>
                </div>
            </div>
            <div class="mini-panel" title="交易主体信息" style="width:100%" allowResize="true" collapseOnTitleClick="true">
                <div class="leftarea">
                    <input id="postDate" name="postDate" class="mini-datepicker" labelField="true" label="交易日期："
                           labelStyle="text-align:left;width:130px;" style="width:100%;"/>
                    <input style="width:100%;" id="amt" name="amt" class="mini-spinner mini-mustFill input-text-strong"
                           labelField="true" changeOnMousewheel='false' format="n4" label="存款金额(元)："
                           onValuechanged="interestAmount" required="true" maxValue="99999999999.9999"
                           labelStyle="text-align:left;width:130px;" onvalidation="zeroValidation"/>
                    <input style="width:100%;" id="firstSettlementDate" name="firstSettlementDate"
                           class="mini-datepicker mini-mustFill" labelField="true" onValuechanged="interestAmount"
                           label="起息日：" ondrawdate="onDrawDateStart" required="true"
                           labelStyle="text-align:left;width:130px;"/>
                    <input style="width:100%" id="tenor" name="tenor" class="mini-spinner mini-mustFill"
                           labelField="true" label="存款期限(天)：" minValue="0" required="true" maxValue="99999"
                           changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"/>
                    <input style="width:100%;" id="settlementAmount" name="settlementAmount"
                           class="mini-spinner mini-mustFill input-text-strong" labelField="true"
                           changeOnMousewheel='false' format="n4" label="到期还款金额：" required="true"
                           maxValue="99999999999999.9999" labelStyle="text-align:left;width:130px;"
                           onvalidation="zeroValidation"/>
                    <input style="width:100%;" id="basis" name="basis" class="mini-combobox mini-mustFill"
                           labelField="true" label="计息基准：" onValuechanged="interestAmount"
                           data="CommonUtil.serverData.dictionary.Basis" labelStyle="text-align:left;width:130px;"
                           required="true"/>
                    <input style="width:100%;" id="intFeq" name="intFeq" class="mini-combobox mini-mustFill"
                           labelField="true" data="CommonUtil.serverData.dictionary.CouponFrequently" label="付息频率："
                           labelStyle="text-align:left;width:130px;" required="true" allowInput="false" value="0"/>

                    <input id="note" name="note" class="mini-textbox" labelField="true" label="交易事由："
                           labelStyle="text-align:left;width:130px;" style="width:100%;" vtype="maxLength:16"/>
                    <input id="fiveLevelClass" name="fiveLevelClass" class="mini-combobox" labelField="true"
                           requiredErrorText="该输入项为必输项" label="五级分类：" labelStyle="text-align:left;width:130px;"
                           style="width:100%;" data="CommonUtil.serverData.dictionary.FiveLevelClass" value="0"/>

                </div>
                <div class="rightarea">
                    <input style="width:100%;" id="currency" name="currency" class="mini-combobox mini-mustFill"
                           labelField="true" data="CommonUtil.serverData.dictionary.Currency" label="货币："
                           labelStyle="text-align:left;width:130px;" required="true" allowInput="false" value="CNY"/>
                    <input style="width:100%;" id="rate" name="rate"
                           class="mini-spinner mini-mustFill input-text-strong" labelField="true" label="存款利率(%)："
                           onValuechanged="interestAmount" changeOnMousewheel='false' required="true"
                           maxValue="99999.999999" format="n6" labelStyle="text-align:left;width:130px;"
                           onvalidation="zeroValidation"/>
                    <input style="width:100%;" id="secondSettlementDate" name="secondSettlementDate"
                           class="mini-datepicker mini-mustFill" labelField="true" onValuechanged="interestAmount"
                           label="到期日：" ondrawdate="onDrawDateEnd" required="true"
                           labelStyle="text-align:left;width:130px;"/>
                    <input style="width:100%;" id="occupancyDays" name="occupancyDays" class="mini-spinner"
                           labelField="true" minErrorText="0" label="实际占款天数：" onValuechanged="interestAmount"
                           changeOnMousewheel='false' maxValue="10000" labelStyle="text-align:left;width:130px;"/>
                    <input style="width:100%;" id="accuredInterest" name="accuredInterest"
                           class="mini-spinner mini-mustFill input-text-strong" labelField="true" label="应计利息："
                           maxValue="99999999999999.9999" changeOnMousewheel='false' format="n4" required="true"
                           labelStyle="text-align:left;width:130px;"/>
                    <input id="dealTransType" name="dealTransType" class="mini-combobox" labelField="true"
                           style="width:100%;" label="数据来源：" data="CommonUtil.serverData.dictionary.dealTransType1"
                           vtype="maxLength:10" labelStyle="text-align:left;width:130px;" value="1">
                    <input style="width:100%;" id="amtFeq" name="amtFeq" class="mini-combobox mini-mustFill"
                           labelField="true" data="CommonUtil.serverData.dictionary.CouponFrequently" label="还本频率："
                           labelStyle="text-align:left;width:130px;" required="true" allowInput="false" value="0"/>
                    <input id="attributionDept" name="attributionDept" valueField="id" showFolderCheckBox="true"
                           labelField="true" label="归属机构：" required="true" showCheckBox="true" style="width:100%"
                           labelStyle="text-align:left;width:130px"
                           showTreeLines="true" showTreeIcon="true" textField="instName" parentField="parentId"
                           class="mini-treeselect mini-mustFill" expandOnLoad="true" resultAsTree="false"
                           valueFromSelect="true" emptyText="请选择机构"/>
                </div>
            </div>
            <%@ include file="../../Common/custLimit.jsp" %>

            <div class="mini-panel" title="清算信息" style="width:100%" allowResize="true" collapseOnTitleClick="true">
                <div class="leftarea">
                    <fieldset>
                        <legend>本方账户</legend>
                        <input id="borrowBnknm" name="borrowBnknm" class="mini-textbox" labelField="true"
                               label="本方开户行名：" vtype="maxLength:1333" style="width:100%;"
                               labelStyle="text-align:left;width:120px;"/>
                        <input id="borrowBnkno" name="borrowBnkno" class="mini-textbox" labelField="true"
                               label="本方开户行号：" vtype="maxLength:1333" labelStyle="text-align:left;width:120px;"
                               style="width:100%;"/>
                        <input id="borrowAccnm" name="borrowAccnm" class="mini-textbox" labelField="true" label="本方户名："
                               onvalidation="onEnglishAndNumberValidation" vtype="maxLength:400"
                               labelStyle="text-align:left;width:120px;" style="width:100%;"/>
                        <input id="borrowAccno" name="borrowAccno" class="mini-textbox" labelField="true" label="本方账号："
                               onvalidation="onEnglishAndNumberValidation" vtype="maxLength:400"
                               labelStyle="text-align:left;width:120px;" style="width:100%;"/>

                    </fieldset>
                </div>
                <div class="rightarea">
                    <fieldset>
                        <legend>对手方账户</legend>
                        <input id="removeBnknm" name="removeBnknm" class="mini-textbox" labelField="true"
                               label="对手方开户行名：" vtype="maxLength:1333" style="width:100%;"
                               labelStyle="text-align:left;width:120px;"/>
                        <input id="removeBnkno" name="removeBnkno" class="mini-textbox" labelField="true"
                               label="对手方开户行号：" vtype="maxLength:1333" labelStyle="text-align:left;width:120px;"
                               style="width:100%;"/>
                        <input id="removeAccnm" name="removeAccnm" class="mini-textbox" labelField="true" label="对手方户名："
                               onvalidation="onEnglishAndNumberValidation" vtype="maxLength:400"
                               labelStyle="text-align:left;width:120px;" style="width:100%;"/>
                        <input id="removeAccno" name="removeAccno" class="mini-textbox" labelField="true" label="对手方账号："
                               onvalidation="onEnglishAndNumberValidation" vtype="maxLength:400"
                               labelStyle="text-align:left;width:120px;" style="width:100%;"/>
                    </fieldset>
                </div>
            </div>
            <%@ include file="../../Common/Flow/MiniApproveOpCommon.jsp" %>
        </div>
    </div>
    <div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
        <div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"
                                                                style="width:120px;" id="save_btn"
                                                                onclick="save">保存交易</a></div>
        <div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"
                                                                style="width:120px;" id="close_btn"
                                                                onclick="close">关闭界面</a></div>
    </div>
</div>
<script type="text/javascript">
    mini.parse();

    //获取当前tab
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var row = params.selectData;
    var detailData = row;
    var url = window.location.search;
    var action = CommonUtil.getParam(url, "action");
    var form = new mini.Form("#field_form");


    var prdNo = CommonUtil.getParam(url, "prdNo");
    var prdName = CommonUtil.getParam(url, "prdName");
    var dealType = CommonUtil.getParam(url, "dealType");
    var tradeData = {};

    tradeData.selectData = row;
    tradeData.operType = action;

    tradeData.serial_no = row.ticketId;
    tradeData.task_id = row.taskId;

    //保存
    function save() {
        //表单验证！！！
        form.validate();
        if (form.isValid() == false) {
            mini.alert("信息填写有误，请重新填写", "系统提示");
            return;
        }
        var data = form.getData(true);
        if (toDecimal(data['accuredInterest']) != toDecimal(data['amt'] * 10000 * data['rate'] * data['occupancyDays'] / 100) || data['tenor'] != data['occupancyDays']
            || toDecimal(data['settlementAmount']) != parseFloat(toDecimal(data['amt'] * 10000)) + parseFloat(toDecimal(data['accuredInterest'])) != "0.00") {
            mini.confirm("确认以当前数据为准吗？", "确认", function (action) {
                if (action != "ok") {
                    return;
                }
                data['sponInst'] = "<%=__sessionInstitution.getInstId()%>";
                data['sponsor'] = "<%=__sessionUser.getUserId() %>";
                data['product'] = prdNo;
                var params = mini.encode(data);
                CommonUtil.ajax({
                    url: "/IfsApprovermbDepositController/saveDeposit",
                    data: params,
                    callback: function (data) {
                        mini.alert(data.desc, '提示信息', function () {
                            if (data.code == 'error.common.0000') {
                                top["win"].closeMenuTab();//关闭并刷新当前界面
                            }
                        });
                    }
                });
            });
        } else {
            data['sponInst'] = "<%=__sessionInstitution.getInstId()%>";
            data['sponsor'] = "<%=__sessionUser.getUserId() %>";
            data['product'] = prdNo;
            var params = mini.encode(data);
            CommonUtil.ajax({
                url: "/IfsApprovermbDepositController/saveDeposit",
                data: params,
                callback: function (data) {
                    mini.alert(data.desc, '提示信息', function () {
                        if (data.code == 'error.common.0000') {
                            top["win"].closeMenuTab();//关闭并刷新当前界面
                        }
                    });
                }
            });
        }
    }

    function close() {
        top["win"].closeMenuTab();
    }

    /**
     *   获取机构
     */
    function loadInstitutionTree() {
        CommonUtil.ajax({
            data: {"branchId": branchId},
            url: "/InstitutionController/searchInstitutions",
            callback: function (data) {
                mini.get("attributionDept").setData(data.obj);
                if (detailData != null && detailData.attributionDept != null) {
                    mini.get("attributionDept").setValue(detailData.attributionDept);
                } else {
                    mini.get("attributionDept").setValue("<%=__sessionInstitution.getInstId()%>");
                }
            }
        });
    }

    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
            if ($.inArray(action, ["approve", "detail"]) > -1) {
                mini.get("save_btn").setVisible(false);
                form.setEnabled(false);
                var approForm = new mini.Form("#approve_operate_form");
                approForm.setEnabled(true);
                mini.get("postDate").setEnabled(false);
            }
            if ($.inArray(action, ["edit", "approve", "detail"]) > -1) {
                form.setData(row);
                mini.get("postDate").setValue(row.postDate);
                mini.get("ticketId").setValue(row.ticketId);
                mini.get("counterpartyInstId").setText(row.counterpartyInstId);
                row['cno'] = row.counterpartyInstId;

                creditsubvalue(row.custNo, "800", null);

            } else {
                mini.get("sponsor").setValue("<%=__sessionUser.getUserId()%>");
                mini.get("sponInst").setValue("<%=__sessionInstitution.getInstFullname() %>");
                mini.get("postDate").setValue("<%=__bizDate %>");
                mini.get("aDate").setValue("<%=__bizDate %>");

                mini.get("basis").select(0);
            }
            loadInstitutionTree();
        });
    });

    //文、数字、下划线 的验证
    function onEnglishAndNumberValidations(e) {
        if (e.value == "" || e.value == null) {//值为空，就不做校验
            return;
        }
        if (e.isValid) {
            if (isEnglishAndNumbers(e.value) == false) {
                e.errorText = "必须输入英文小写+数字";
                e.isValid = false;
            }
        }
    }

    /* 是否英文+数字 */
    function isEnglishAndNumbers(v) {
        var re = new RegExp("^[0-9a-z\_\-]+$");
        if (re.test(v)) return true;
        return false;
    }

    //英文、数字、下划线 的验证
    function onEnglishAndNumberValidation(e) {
        if (e.value == "" || e.value == null) {//值为空，就不做校验
            return;
        }
        if (e.isValid) {
            if (isEnglishAndNumber(e.value) == false) {
                e.errorText = "必须输入英文+数字";
                e.isValid = false;
            }
        }
    }

    /* 是否英文+数字 */
    function isEnglishAndNumber(v) {
        var re = new RegExp("^[0-9a-zA-Z\_\-]+$");
        if (re.test(v)) return true;
        return false;
    }

    function onButtonEdit(e) {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Common/CustMini.jsp",
            title: "选择对手方列表",
            width: 900,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.cno);
                        btnEdit.setText(data.cno);
                        creditsubvalue(data.cno, "800", null);
                        btnEdit.focus();
                        mini.get("cname").setValue(data.cliname);
                    }
                }

            }
        });
    }

    function onCreditTypeChange() {
        var cno = mini.get("counterpartyInstId").getValue();
        creditsubvalue(cno, "800", null);
    }

    //根据交易对手编号查询全称
    function queryTextName(cno) {
        CommonUtil.ajax({
            url: "/IfsOpicsCustController/searchIfsOpicsCust",
            data: {'cno': cno},
            callback: function (data) {
                if (data && data.obj) {
                    mini.get("removeInst").setText(data.obj.cliname);
                } else {
                    mini.get("removeInst").setText(cno);
                }
            }
        });
    }

    function onDrawDateStart(e) {
        var firstDate = e.date;
        var secondDate = mini.get("secondSettlementDate").getValue();
        if (CommonUtil.isNull(secondDate)) {
            return;
        }
        if (secondDate.getTime() < firstDate.getTime()) {
            e.allowSelect = false;
        }
    }

    function onDrawDateEnd(e) {
        var secondDate = e.date;
        var firstDate = mini.get("firstSettlementDate").getValue();
        if (CommonUtil.isNull(firstDate)) {
            return;
        }
        if (secondDate.getTime() < firstDate.getTime()) {
            e.allowSelect = false;
        }
    }

    function interestAmount(e) {
        var firstDate = mini.get("firstSettlementDate").getValue();
        var secondDate = mini.get("secondSettlementDate").getValue();
        if (CommonUtil.isNull(firstDate) || CommonUtil.isNull(secondDate)) {
            return;
        }
        var subDate = Math.abs(parseInt((secondDate.getTime() - firstDate.getTime()) / 1000 / 3600 / 24));
        mini.get("tenor").setValue(subDate);
        mini.get("occupancyDays").setValue(subDate);
        var occupancyDays = mini.get("occupancyDays").getValue();
        var amt = mini.get("amt").getValue();
        var rate = mini.get("rate").getValue();
        var basis = mini.get("basis").getValue();
        var calDays = basisTonumber(basis);
        mini.get("accuredInterest").setValue(amt * rate * occupancyDays / 100 / calDays);
        mini.get("settlementAmount").setValue(amt + amt * rate * occupancyDays / 100 / calDays);

    }

    //实际占款天数点击事件
    function linkageCalculat(e) {
        var occupancyDays = e.value;
        var amt = mini.get("amt").getValue();
        var rate = mini.get("rate").getValue();
        var basis = mini.get("basis").getValue();
        var calDays = basisTonumber(basis);
        mini.get("accuredInterest").setValue(amt * rate * occupancyDays / 100 / calDays);
        mini.get("settlementAmount").setValue(amt + amt * rate * occupancyDays / 100 / calDays);
    }

    //计息基准点击事件
    function linkIntCalculat(e) {
        var basis = e.value;
        var occupancyDays = mini.get("occupancyDays").getValue();
        var amt = mini.get("amt").getValue();
        var rate = mini.get("rate").getValue();
        var calDays = basisTonumber(basis);
        mini.get("accuredInterest").setValue(amt * rate * occupancyDays / 100 / calDays);
        mini.get("settlementAmount").setValue(amt + amt * rate * occupancyDays / 100 / calDays);
    }

</script>
<script type="text/javascript" src="../../Common/Flow/MiniApproveOpCommon.js"></script>
</body>
</html>
