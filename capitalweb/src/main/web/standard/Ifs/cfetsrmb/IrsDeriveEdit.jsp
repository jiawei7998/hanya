<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
	<script type="text/javascript" src="<%=basePath%>/standard/Common/RiskCenter.js"></script>
	<script type="text/javascript" src="./rmbVerify.js"></script>
    <title>结构衍生 维护</title>
    <script type="text/javascript" >
	/**prdCode用于页面投资组合的选择   该全局变量必须为prdCode*/
	var prdCode="SWAP";
	</script>
  </head>


<body style="width:100%;height:100%;background:white">
	<div class="mini-splitter" style="width:100%;height:100%;">
		<div size="90%" showCollapseButton="false">
			<h1 style="text-align:center"><strong>结构衍生</strong></h1>
			<div id="field_form" class="mini-fit area"  style="background:white" >
			<input id="sponsor" name="sponsor" class="mini-hidden" />
			<input id="sponInst" name="sponInst" class="mini-hidden" />
			<input id="aDate" name="aDate" class="mini-hidden"/>
			<input id="ticketId" name="ticketId" class="mini-hidden" />
			<div class="mini-panel" title="成交单编号" style="width:100%;"  allowResize="true" collapseOnTitleClick="true">
			<div class="leftarea">
				<input style="width:100%;" id="contractId" name="contractId" class="mini-textbox mini-mustFill" labelField="true" required="true"  requiredErrorText="该输入项为必输项" label="成交单编号：" labelStyle="text-align:left;width:130px;"  vtype="maxLength:35" onvalidation="onEnglishAndNumberValidation"/>
			</div>
			
		</div>
			<div class="mini-panel" title="成交双方信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
				<div class="leftarea">
					<fieldset>
						<legend>本方信息</legend>
						<input id="myDir" name="myDir" class="mini-combobox mini-mustFill"  labelField="true"  label="本方方向：" required="true"  onvaluechanged="dirVerify"  style="width:100%;"  labelStyle="text-align:left;width:130px;" data = "CommonUtil.serverData.dictionary.irsing" onvalidation="onFixFloatDirValidation"/>
						<input id="fixedInst" name="fixedInst" class="mini-textbox mini-mustFill"  labelField="true"  label="机构："  required="true"  style="width:100%;" enabled="false" labelStyle="text-align:left;width:130px;"/>
						<input id="fixedTrader" name="fixedTrader" class="mini-textbox mini-mustFill" labelField="true"  label="交易员：" required="true"   vtype="maxLength:5" enabled="false" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						<!-- <input id="fixedTel" name="fixedTel" class="mini-textbox" labelField="true"  label="电话："  onvalidation="onEnglishAndNumberValidation" vtype="maxLength:20" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						<input id="fixedFax" name="fixedFax" class="mini-textbox" labelField="true"  label="传真："  onvalidation="onEnglishAndNumberValidation" vtype="maxLength:20" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						<input id="fixedCorp" name="fixedCorp" class="mini-textbox" labelField="true"  label="法人代表："   vtype="maxLength:5" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						<input id="fixedAddr" name="fixedAddr" class="mini-textbox" labelField="true"  label="地址："  vtype="maxLength:50"  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						 -->
					</fieldset>
				</div>
				<div class="rightarea">
					<fieldset>
						<legend>对手方信息</legend>
						<input id="oppoDir" name="oppoDir" class="mini-combobox mini-mustFill"  labelField="true"  label="对方方向："  required="true"  enabled="false" style="width:100%;"  labelStyle="text-align:left;width:130px;" data = "CommonUtil.serverData.dictionary.irsing" />
						<input id="floatInst" name="floatInst" class="mini-buttonedit mini-mustFill" onbuttonclick="onButtonEdit" labelField="true"  label="机构：" allowInput="false" required="true"  style="width:100%;"  labelStyle="text-align:left;width:130px;" />
						<input id="floatTrader" name="floatTrader" class="mini-textbox mini-mustFill" labelField="true"  label="交易员："   vtype="maxLength:10"  labelStyle="text-align:left;width:130px;" style="width:100%;" required="true"   />
						<!-- <input id="floatTel" name="floatTel" class="mini-textbox" labelField="true"  label="电话：" onvalidation="onEnglishAndNumberValidation" vtype="maxLength:20" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						<input id="floatFax" name="floatFax" class="mini-textbox" labelField="true"  label="传真："  onvalidation="onEnglishAndNumberValidation" vtype="maxLength:20" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						<input id="floatCorp" name="floatCorp" class="mini-textbox" labelField="true"  label="法人代表："   vtype="maxLength:5" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						<input id="floatAddr" name="floatAddr" class="mini-textbox" labelField="true"  label="地址："  vtype="maxLength:50" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						 -->
					</fieldset>
				</div>
			</div>
			<div class="mini-panel" title="交易主体信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
				<div class="leftarea" >
					<input style="width:100%;" id="forDate" name="forDate" class="mini-datepicker  mini-mustFill" labelField="true" required="true"  label="成交日期：" labelStyle="text-align:left;width:130px;" />
					<input style="width:100%" id="productName" name="productName" class="mini-buttonedit mini-mustFill" onbuttonclick="onPrdNameEdit" labelField="true"  label="产品名称："  required="true"  allowInput="false" labelStyle="text-align:left;width:130px;" />
					<input style="width:100%" id="startDate" name="startDate" class="mini-datepicker mini-mustFill" labelField="true"  label="起息日：" required="true"  ondrawdate="onDrawDateStart" onvaluechanged="onStartDateChanged"  labelStyle="text-align:left;width:130px;" />
					<input style="width:100%" id="calculateAgency" name="calculateAgency" class="mini-combobox mini-mustFill" labelField="true"  label="计算机构：" required="true"    labelStyle="text-align:left;width:130px;" data = "CommonUtil.serverData.dictionary.calInst" />
					<input style="width:100%" id="interestRateAdjustment" name="interestRateAdjustment" class="mini-combobox mini-mustFill" labelField="true"  label="计息天数调整：" required="true"   labelStyle="text-align:left;width:130px;"  data = "CommonUtil.serverData.dictionary.daysChange" />
					<input style="width:100%" id="couponPaymentDateReset" name="couponPaymentDateReset" class="mini-combobox mini-mustFill" labelField="true"  label="支付日调整："  required="true"   labelStyle="text-align:left;width:130px;"  data = "CommonUtil.serverData.dictionary.payChange" />
					<input style="width:100%" id="accountingMethod" name="accountingMethod" class="mini-combobox mini-mustFill" labelField="true" label="交易类型："  labelStyle="text-align:left;width:130px;"  vtype="maxLength:20" data="CommonUtil.serverData.dictionary.acctMethod" requiredErrorText="该输入项为必输项" required="true" />
					<input id="nettingStatus" name="nettingStatus" class="mini-combobox mini-mustFill" required="true"  labelField="true"   label="净额清算状态：" labelStyle="text-align:left;width:130px;" style="width:100%;" data="CommonUtil.serverData.dictionary.YesNo" showNullItem="true"/>
					<input id="dealTransType" name="dealTransType" class="mini-combobox" labelField="true"  style="width:100%;"  label="数据来源：" data="CommonUtil.serverData.dictionary.dealTransType1" vtype="maxLength:10"  labelStyle="text-align:left;width:130px;"  value="1">
				</div>
				<div class="rightarea" >
					<input style="width:100%" id="ccy" name="ccy" class="mini-combobox mini-mustFill" labelField="true" label="交易币种：" labelStyle="text-align:left;width:130px;"  vtype="maxLength:20" data="CommonUtil.serverData.dictionary.Currency" requiredErrorText="该输入项为必输项" required="true" />
					<input style="width:100%" id="lastQty" name="lastQty" class="mini-spinner mini-mustFill" minValue="0" maxValue="9999999999999999.9999" format="n4" changeOnMousewheel="false" labelField="true"  label="名义本金(万元)："  required="true"   labelStyle="text-align:left;width:130px;" onvalidation="zeroValidation"  />
					<input style="width:100%" id="endDate" name="endDate" class="mini-datepicker mini-mustFill" labelField="true" label="到期日：" required="true"  ondrawdate="onDrawDateEnd"  labelStyle="text-align:left;width:130px;"  onvaluechanged = "searchProductWeightNew()" />
					<input style="width:100%" id="tenor" name="tenor" class="mini-combobox mini-mustFill" labelField="true" label="期限：" required="true"  labelStyle="text-align:left;width:130px;" data = "CommonUtil.serverData.dictionary.tenor"/>
					<input style="width:100%" id="firstPeriodStartDate" name="firstPeriodStartDate" class="mini-datepicker mini-mustFill" labelField="true" label="首期起息日："  required="true"  labelStyle="text-align:left;width:130px;"   />
					<input style="width:100%" id="tradeMethod" name="tradeMethod" class="mini-combobox mini-mustFill" labelField="true" label="清算方式："  required="true"  labelStyle="text-align:left;width:130px;"   data = "CommonUtil.serverData.dictionary.CleanMethoed"/>
					<input style="width:100%" id="fixedFloatDir" name="fixedFloatDir" class="mini-combobox mini-mustFill" labelField="true" label="固定/浮动方向：" labelStyle="text-align:left;width:130px;"  required="true"  data="CommonUtil.serverData.dictionary.fixFltDir" enabled="false" />
					<input id="tradingModel" name="tradingModel" class="mini-combobox mini-mustFill" required="true"  labelField="true" label="交易模式：" labelStyle="text-align:left;width:130px;" style="width:100%;"  data="CommonUtil.serverData.dictionary.TradeModel"/>
					<input id="note" name="note" class="mini-textbox" labelField="true" label="备注：" labelStyle="text-align:left;width:130px;" style="width:100%;" vtype="maxLength:16"/>
				</div>
			</div>
			<div class="mini-panel" title="交易明细信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
				<div class="leftarea" >
					<fieldset >
						<legend>利率明细</legend>
						<input id="fixedFloatL" name="fixedFloatL" class="mini-combobox mini-mustFill" labelField="true"  label="固定/浮动：" data = "CommonUtil.serverData.dictionary.fixfloat"  labelStyle="text-align:left;width:130px;" style="width:100%;"  required="true"  onvalidation="onFixFloatDirValidation"/>
						<input id="legPrice" name="legPrice" class="mini-spinner" minValue="0" maxValue="9999999999999999.99999999" format="n8" changeOnMousewheel="false" labelField="true"  label="利率(%)："  style="width:100%;"  labelStyle="text-align:left;width:130px;" />
<!-- 						<input id="benchmarkCurveNameL" name="benchmarkCurveNameL" class="mini-buttonedit" labelField="true"  label="参考利率：" onbuttonclick="onRateEdit" allowInput="false" vtype="maxLength:20" style="width:100%;"  labelStyle="text-align:left;width:130px;" /> -->
						<input id="fixedLinkTarget" name="fixedLinkTarget" class="mini-textbox" labelField="true"  label="挂钩标的：" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						<input id="benchmarkSpreadL" name="benchmarkSpreadL" class="mini-spinner" minValue="0" maxValue="999.9999" format="n4" changeOnMousewheel="false"  labelField="true"  label="利差(bps)："  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						<input id="fixedPaymentDate" name="fixedPaymentDate" class="mini-datepicker mini-mustFill" labelField="true"  label="首期定期支付日："  labelStyle="text-align:left;width:130px;" style="width:100%;" required="true"   />
						<input id="fixedPaymentFrequency" name="fixedPaymentFrequency" class="mini-combobox mini-mustFill" required="true"  labelField="true"  label="支付周期：" data = "CommonUtil.serverData.dictionary.Intpaycircle"  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						<input id="legInterestAccrualDateL" name="legInterestAccrualDateL" class="mini-datepicker mini-mustFill" labelField="true"  label="首次利率确定日："  required="true"  style="width:100%;"  labelStyle="text-align:left;width:130px;" />
						<input id="interestResetFrequencyL" name="interestResetFrequencyL" class="mini-combobox mini-mustFill" labelField="true"  label="重置频率：" required="true"   data = "CommonUtil.serverData.dictionary.Intpaycircle" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						<input id="interestMethodL" name="interestMethodL" class="mini-combobox mini-mustFill" labelField="true"  label="计息方式：" required="true"   data = "CommonUtil.serverData.dictionary.calIrsMethod"labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						<input id="fixedLegDayCount" name="fixedLegDayCount" class="mini-combobox mini-mustFill" labelField="true"  label="计息基准：" data = "CommonUtil.serverData.dictionary.Basis"  labelStyle="text-align:left;width:130px;" style="width:100%;" required="true"   />
						<input id="leftDiscountCurve" name="leftDiscountCurve" class="mini-buttonedit mini-mustFill" onbuttonclick="onYchdEdit" labelField="true" label="利率曲线：" style="width: 100%;" labelStyle="text-align:left;width:130px;"  requiredErrorText="该输入项为必输项" required="true" />
						<input id="leftRateCode" name="leftRateCode" class="mini-buttonedit mini-mustFill" onbuttonclick="onRateEdit" labelField="true" label="利率代码：" style="width: 100%;" labelStyle="text-align:left;width:130px;"  requiredErrorText="该输入项为必输项" required="true" />
					</fieldset>
				</div>
				<div class="rightarea">
					<fieldset>
						<legend>利率明细</legend>
						<input id="fixedFloatR" name="fixedFloatR" class="mini-combobox mini-mustFill" labelField="true"  label="固定/浮动：" data = "CommonUtil.serverData.dictionary.fixfloat"  labelStyle="text-align:left;width:130px;" style="width:100%;"  required="true"  onvalidation="onFixFloatDirValidation" />
						<input id="rightRate" name="rightRate" class="mini-spinner" minValue="0" maxValue="9999999999999999.99999999" format="n8" changeOnMousewheel="false" labelField="true"  label="利率(%)："  style="width:100%;"  labelStyle="text-align:left;width:130px;" />
<!-- 						<input id="benchmarkCurveName" name="benchmarkCurveName" class="mini-buttonedit" labelField="true"  label="参考利率：" onbuttonclick="onRateEdit" allowInput="false" vtype="maxLength:20" style="width:100%;"  labelStyle="text-align:left;width:130px;" /> -->
						<input id="floatLinkTarget" name="floatLinkTarget" class="mini-textbox" labelField="true"  label="挂钩标的：" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						<input id="benchmarkSpread" name="benchmarkSpread" class="mini-spinner" minValue="0" maxValue="999.9999" format="n4" changeOnMousewheel="false"  labelField="true"  label="利差(bps)："  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						<input id="floatPaymentDate" name="floatPaymentDate" class="mini-datepicker mini-mustFill" labelField="true"  label="首期定期支付日："  labelStyle="text-align:left;width:130px;" style="width:100%;" required="true"   />
						<input id="floatPaymentFrequency" name="floatPaymentFrequency" class="mini-combobox mini-mustFill" required="true"  labelField="true"  label="支付周期："  data = "CommonUtil.serverData.dictionary.Intpaycircle" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						<input id="legInterestAccrualDate" name="legInterestAccrualDate" class="mini-datepicker mini-mustFill" labelField="true"  label="首次利率确定日："  required="true"  style="width:100%;"  labelStyle="text-align:left;width:130px;" />
						<input id="interestResetFrequency" name="interestResetFrequency" class="mini-combobox mini-mustFill" labelField="true"  label="重置频率：" required="true"   data = "CommonUtil.serverData.dictionary.Intpaycircle" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						<input id="interestMethod" name="interestMethod" class="mini-combobox mini-mustFill" labelField="true"  label="计息方式：" required="true"   data = "CommonUtil.serverData.dictionary.calIrsMethod"labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						<input id="floatLegDayCount" name="floatLegDayCount" class="mini-combobox mini-mustFill" labelField="true"  label="计息基准："  required="true"  data = "CommonUtil.serverData.dictionary.Basis" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						<input id="rightDiscountCurve" name="rightDiscountCurve" class="mini-buttonedit mini-mustFill" onbuttonclick="onYchdEdit" labelField="true" label="利率曲线：" style="width: 100%;" labelStyle="text-align:left;width:130px;" allowInput="false" requiredErrorText="该输入项为必输项" required="true" />
						<input id="rightRateCode" name="rightRateCode" class="mini-buttonedit mini-mustFill" onbuttonclick="onRateEdit" labelField="true" label="利率代码：" style="width: 100%;" labelStyle="text-align:left;width:130px;" allowInput="false" requiredErrorText="该输入项为必输项" required="true" />
					</fieldset>
				</div>
			</div>
			<div class="mini-panel" title="费用模块" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
				<div class="leftarea" >
					<input id="feeFloat" name="feeFloat" class="mini-combobox mini-mustFill" labelField="true"  label="本方方向：" data = "CommonUtil.serverData.dictionary.irsing"  labelStyle="text-align:left;width:130px;" style="width:100%;"  required="true"  />
					<input id="feePaymentDate" name="feePaymentDate" class="mini-datepicker mini-mustFill" labelField="true"  label="收付日期："  labelStyle="text-align:left;width:130px;" style="width:100%;" required="true"   />
					<!-- <input id="feeInterestAccrualDate" name="feeInterestAccrualDate" class="mini-datepicker mini-mustFill" labelField="true"  label="首次利率确定日："  required="true"  style="width:100%;"  labelStyle="text-align:left;width:130px;" /> -->
<!-- 					<input id="feeDiscountCurve" name="feeDiscountCurve" class="mini-buttonedit mini-mustFill" onbuttonclick="onYchdEdit" labelField="true" label="利率曲线：" style="width: 100%;" labelStyle="text-align:left;width:130px;"  requiredErrorText="该输入项为必输项" required="true" /> -->
				</div>
				<div class="rightarea">
					<input id="feePrice" name="feePrice" class="mini-spinner" minValue="0" maxValue="99999999.9999" format="n4" changeOnMousewheel="false" labelField="true"  label="金额："  style="width:100%;"  labelStyle="text-align:left;width:130px;" />
					<!-- <input id="feePaymentFrequency" name="feePaymentFrequency" class="mini-combobox mini-mustFill" required="true"  labelField="true"  label="支付周期：" data = "CommonUtil.serverData.dictionary.Intpaycircle"  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
					<input id="feeLegDayCount" name="feeLegDayCount" class="mini-combobox mini-mustFill" labelField="true"  label="计息基准：" data = "CommonUtil.serverData.dictionary.Basis"  labelStyle="text-align:left;width:130px;" style="width:100%;" required="true"   /> -->
<!-- 					<input id="feeRateCode" name="feeRateCode" class="mini-buttonedit mini-mustFill" onbuttonclick="onRateEdit" labelField="true" label="利率代码：" style="width: 100%;" labelStyle="text-align:left;width:130px;"  requiredErrorText="该输入项为必输项" required="true" /> -->
				</div>
			</div>
			<%@ include file="../../Common/opicsLess.jsp" %>
			<%@ include file="../../Common/RiskCenter.jsp"%>
			<div class="mini-panel" title="清算信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
				<div class="leftarea">
					<fieldset>
						<legend>本方账户</legend>
						<input id="fixedAccname" name="fixedAccname" class="mini-textbox" labelField="true"  label="资金账户户名：" vtype="maxLength:1333" style="width:100%;"  labelStyle="text-align:left;width:130px;" />
						<input id="fixedOpenBank" name="fixedOpenBank" class="mini-textbox" labelField="true"  label="资金开户行：" vtype="maxLength:1333" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						<input id="fixedAccnum" name="fixedAccnum" class="mini-textbox" labelField="true"  label="资金账号：" onvalidation="onEnglishAndNumberValidation" vtype="maxLength:400"  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						<input id="fixedPsnum" name="fixedPsnum" class="mini-textbox" labelField="true"  label="支付系统行号：" onvalidation="onEnglishAndNumberValidation" vtype="maxLength:400"  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
					</fieldset>
				</div>
				<div class="rightarea">
					<fieldset>
						<legend>对手方账户</legend>
						<input id="floatAccname" name="floatAccname" class="mini-textbox" labelField="true"  label="资金账户户名：" vtype="maxLength:1333" style="width:100%;"  labelStyle="text-align:left;width:130px;" />
						<input id="floatOpenBank" name="floatOpenBank" class="mini-textbox" labelField="true"  label="资金开户行："  vtype="maxLength:1333" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						<input id="floatAccnum" name="floatAccnum" class="mini-textbox" labelField="true"  label="资金账号："   onvalidation="onEnglishAndNumberValidation" vtype="maxLength:400"  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						<input id="floatPsnum" name="floatPsnum" class="mini-textbox" labelField="true"  label="支付系统行号："  onvalidation="onEnglishAndNumberValidation" vtype="maxLength:400" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
					</fieldset>
				</div>
			</div>
			<%@ include file="../../Common/Flow/MiniApproveOpCommon.jsp"%>
			</div>
			
		</div>
		<div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
			<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="save_btn"   onclick="save">保存交易</a></div>
			<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="close_btn"   onclick="close">关闭界面</a></div>
		</div>
	</div>	
<script type="text/javascript">
	mini.parse();
	//获取当前tab
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var row=params.selectData;
	var url=window.location.search;
	var action=CommonUtil.getParam(url,"action");
	var ticketId = CommonUtil.getParam(url, "ticketId");
	var form=new mini.Form("#field_form");

	var prdNo = CommonUtil.getParam(url, "prdNo");
	
	var tradeData={};
	
	tradeData.selectData=row;
	tradeData.operType=action;
	tradeData.serial_no=row.ticketId;
	tradeData.task_id=row.taskId;
	//保存
	function save(){
		//表单验证！！！
		form.validate();
		if (form.isValid() == false) {
			return;
		}

		var data=form.getData(true);
// 		data['dealTransType']="M";
		data['fixedInst']="<%=__sessionInstitution.getInstId()%>";
		data['fixedTrader']="<%=__sessionUser.getUserId() %>";
		data['sponInst']="<%=__sessionInstitution.getInstId()%>";
		data['prdFlag']="1";//代表结构衍生
		var params=mini.encode(data);
		CommonUtil.ajax({
			url:"/IfsCfetsrmbIrsController/saveIrs",
			data:params,
			callback:function(data){
				mini.alert(data,'提示信息',function(){
					if(data=="保存成功"||data=="修改成功"){
						top["win"].closeMenuTab();
					}
				});
			}
		});
	}
	function close(){
		top["win"].closeMenuTab();
	}
	$(document).ready(function(){
		if($.inArray(action,["approve","detail"])>-1){
			mini.get("save_btn").setVisible(false);
			form.setEnabled(false);
			mini.get("forDate").setEnabled(false);
			var form11=new mini.Form("#approve_operate_form");
			form11.setEnabled(true);
		}else if(action=="add"){//增加时，设置审批发起人 审批发起机构
			mini.get("sponsor").setValue("<%=__sessionUser.getUserId()%>");
			mini.get("sponInst").setValue("<%=__sessionUser.getInstId() %>");
		}
		if($.inArray(action,["edit","approve","detail"])>-1){
			mini.get("applyNo").setVisible(false);//信用风险审查单号：
			mini.get("applyProd").setVisible(false);//产品名称
			form.setData(row);
			mini.get("forDate").setValue(row.forDate);
			mini.get("ticketId").setValue(row.ticketId);
			mini.get("fixedInst").setValue("<%=__sessionInstitution.getInstFullname()%>");
			mini.get("fixedTrader").setValue(row.myUserName);
			//.投资组合
			mini.get("port").setValue(row.port);
			mini.get("port").setText(row.port);
			//成本中心
			mini.get("cost").setValue(row.cost);
			mini.get("cost").setText(row.cost);
			//产品代码
			mini.get("product").setValue(row.product);
			mini.get("product").setText(row.product);
			//产品类型
			mini.get("prodType").setValue(row.prodType);
			mini.get("prodType").setText(row.prodType);
			//对方机构floatInst
			mini.get("floatInst").setValue(row.cno);
			mini.get("floatInst").setText(row.floatInst);
			queryCustNo(row.custNo);
			queryTextName(row.cno);
			mini.get("contractId").setEnabled(false);
			//清算路径
			mini.get("ccysmeans").setValue(row.ccysmeans);
			mini.get("ccysmeans").setText(row.ccysmeans);
			mini.get("ctrsmeans").setValue(row.ctrsmeans);
			mini.get("ctrsmeans").setText(row.ctrsmeans);
			//产品名称
			mini.get("productName").setValue(row.productName);
			mini.get("productName").setText(row.productName);
			//参考利率
// 			mini.get("benchmarkCurveNameL").setValue(row.benchmarkCurveNameL);
// 			mini.get("benchmarkCurveNameL").setText(row.benchmarkCurveNameL);
// 			mini.get("benchmarkCurveName").setValue(row.benchmarkCurveName);
// 			mini.get("benchmarkCurveName").setText(row.benchmarkCurveName);
			//利率代码、利率曲线
			mini.get("leftDiscountCurve").setValue(row.leftDiscountCurve);
			mini.get("leftDiscountCurve").setText(row.leftDiscountCurve);
			mini.get("rightDiscountCurve").setValue(row.rightDiscountCurve);
			mini.get("rightDiscountCurve").setText(row.rightDiscountCurve);
			
			mini.get("leftRateCode").setValue(row.leftRateCode);
			mini.get("leftRateCode").setText(row.leftRateCode);
			mini.get("rightRateCode").setValue(row.rightRateCode);
			mini.get("rightRateCode").setText(row.rightRateCode);
			//风险中台
			mini.get("applyNo").setValue(row.applyNo);
			mini.get("applyNo").setText(row.applyNo);
			
			//mini.get("weight").setValue(text.obj.weight);
			if(action == "edit" && row.weight ==null ){
				searchProductWeightNew();
			}
			
		}else{
			mini.get("fixedInst").setValue("<%=__sessionInstitution.getInstFullname()%>");
			mini.get("fixedTrader").setValue("<%=__sessionUser.getUserName() %>");
			mini.get("forDate").setValue("<%=__bizDate %>");
			mini.get("applyNo").setVisible(false);//信用风险审查单号：
			mini.get("applyProd").setVisible(false);//产品名称
			//searchProductWeightNew();
		}
		mini.get("dealTransType").setEnabled(false);
	});

	//加载机构树
	function nodeclick(e) {
		
		var oldvalue=e.sender.value;
		var param = mini.encode({"branchId":branchId}); //序列化成JSON
		CommonUtil.ajax({
		  url: "/InstitutionController/searchInstitutionByBranchId",
		  data: param,
		  callback: function (data) {
			var obj=e.sender;
			obj.setData(data.obj);
			obj.setValue(oldvalue);
			
		  }
		});
	}

	

	//文、数字、下划线 的验证
	function onEnglishAndNumberValidations(e) {
		if(e.value == "" || e.value == null){//值为空，就不做校验
			return;
		}
		if (e.isValid) {
			if (isEnglishAndNumbers(e.value) == false) {
				e.errorText = "必须输入英文小写+数字";
				e.isValid = false;
			}
		}
	}
	/* 是否英文+数字 */
	function isEnglishAndNumbers(v) {
		var re = new RegExp("^[0-9a-z\_\-]+$");
		if (re.test(v)) return true;
		return false;
	}	
	
	//英文、数字、下划线 的验证
	function onEnglishAndNumberValidation(e) {
		if(e.value == "" || e.value == null){//值为空，就不做校验
			return;
		}
		if (e.isValid) {
			if (isEnglishAndNumber(e.value) == false) {
				e.errorText = "必须输入英文+数字";
				e.isValid = false;
			}
		}
	}
	/* 是否英文+数字 */
	function isEnglishAndNumber(v) {
		var re = new RegExp("^[0-9a-zA-Z\_\-]+$");
		if (re.test(v)) return true;
		return false;
	}

	function onButtonEdit(e) {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Common/CustMini.jsp",
            title: "选择对手方列表",
            width: 900,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.cno);
                        btnEdit.setText(data.cliname);
                        queryCustNo(data.creditsub);
                        btnEdit.focus();
                    }
                }

            }
        });

	}
	//根据交易对手编号查询全称
	function queryTextName(cno){
		CommonUtil.ajax({
		    url: "/IfsOpicsCustController/searchIfsOpicsCust",
		    data : {'cno' : cno},
		    callback:function (data) {
		    	if (data && data.obj) {
		    		mini.get("floatInst").setText(data.obj.cliname);
				} else {
					mini.get("floatInst").setText();
		    }
		    }
		});
	}
	//起息日 值改变时发生 -> 首期起息日 默认为 起息日时间
	function onStartDateChanged(e){
// 		mini.get("firstPeriodStartDate").setValue(e.value);
	}

	//到期日 
	function onDrawDateEnd(e) {
		var date = e.date;
		var d = mini.get("startDate").getValue();
		if(d==null || d == ""){
			return;
		}

		if (date.getTime() < d.getTime()) {
			e.allowSelect = false;
		}
	}
    //起息日 
	function onDrawDateStart(e){
		var date = e.date;
		var d = mini.get("endDate").getValue();
		if(d==null || d == ""){
			return;
		}

		if (date.getTime() > d.getTime()) {
			e.allowSelect = false;
		}
	}

	//支付日 > 到期日
	function onDrawDatePay(e){
		var date = e.date;
		var d = mini.get("endDate").getValue();
		if(d==null || d == ""){
			return;
		}

		if (date.getTime() < d.getTime()) {
			e.allowSelect = false;
		}
	}
	//支付日 改变时发生
	function onPayDateChanged(e){
		var d = mini.get("endDate").getValue();
		if(d==null || d == ""){
			mini.alert("请先选择到期日！");
			mini.get("couponPaymentDateReset").setValue("");
			return;
		}
	}
	
	/**   付款路径的选择   */
	function settleMeans1(){
		 var cpInstId = mini.get("floatInst").getValue();
		if(cpInstId == null || cpInstId == ""){
			mini.alert("请先选择对方机构!");
			return;
		} 
		
		
		var currency = mini.get("ccy").getValue();
		if(currency == null || currency == ""){
			mini.alert("请先选择交易币种!");
			return;
		}
		
		var url;
		var data;
		
		if(currency!="CNY"){//外币
			url="./Ifs/opics/nostMini.jsp";
        	data = { ccy: currency ,cust:cpInstId};
        }else if(currency=="CNY"){//人民币   查seta表
        	data = { ccy: currency ,cust:cpInstId};
        	url="./Ifs/opics/setaMini.jsp";
        }
		
		var btnEdit = this;
        mini.open({
            url: url,
            title: "选择清算路径",
            width: 700,
            height: 600,
            onload: function () {
                var iframe = this.getIFrameEl();
                iframe.contentWindow.SetData(data);
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data1 = iframe.contentWindow.GetData();
                    data1 = mini.clone(data1);    //必须
                    if (data1) {
                    	if(currency!="CNY"){//外币
                    		mini.get("ccysmeans").setValue($.trim(data1.smeans));
                    		mini.get("ccysmeans").setText($.trim(data1.smeans));
                            mini.get("ccysacct").setValue($.trim(data1.nos));
                        }else if(currency=="CNY"){//人民币
                        	mini.get("ccysmeans").setValue($.trim(data1.smeans));
	                    	 mini.get("ccysmeans").setText($.trim(data1.smeans));
	                         mini.get("ccysacct").setValue($.trim(data1.sacct)); 
                        }
                        
                        btnEdit.focus();
                    }
                }

            }
        });
		
		
	}
	
	/**   收款路径的选择   */
	function settleMeans2(){
		var cpInstId = mini.get("floatInst").getValue();
		if(cpInstId == null || cpInstId == ""){
			mini.alert("请先选择对方机构!");
			return;
		} 
		
		
		var currency = mini.get("ccy").getValue();
		if(currency == null || currency == ""){
			mini.alert("请先选择交易币种!");
			return;
		}
		
		var url;
		var data;
		
		if(currency!="CNY"){//外币
			url="./Ifs/opics/nostMini.jsp";
        	data = { ccy: currency ,cust:cpInstId};
        }else if(currency=="CNY"){//人民币   查seta表
        	data = { ccy: currency ,cust:cpInstId};
        	url="./Ifs/opics/setaMini.jsp";
        }
		
		
		
		var btnEdit = this;
        mini.open({
            url: url,
            title: "选择清算路径",
            width: 700,
            height: 600,
            onload: function () {
                var iframe = this.getIFrameEl();
                iframe.contentWindow.SetData(data);
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data1 = iframe.contentWindow.GetData();
                    data1 = mini.clone(data1);    //必须
                    if (data1) {
                    	if(currency!="CNY"){//外币
                    		mini.get("ctrsmeans").setValue($.trim(data1.smeans));
                    		mini.get("ctrsmeans").setText($.trim(data1.smeans));
                            mini.get("ctrsacct").setValue($.trim(data1.nos));
                        }else if(currency == "CNY"){//人民币
                        	mini.get("ctrsmeans").setValue($.trim(data1.smeans));
	                    	 mini.get("ctrsmeans").setText($.trim(data1.smeans));
	                         mini.get("ctrsacct").setValue($.trim(data1.sacct)); 
                        }
                        
                        btnEdit.focus();
                    }
                }

            }
        });
		
		
	}
	//利率代码的选择
	function onRateEdit(){
		var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "/opics/rateMini.jsp",
            title: "选择利率代码",
            width: 700,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.ratecode);
                        btnEdit.setText(data.ratecode);
                        btnEdit.focus();
                    }
                }

            }
        });
	}
	
	//利率曲线的选择
	function onYchdEdit(){
		var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "/opics/ychdMini.jsp",
            title: "选择利率曲线",
            width: 700,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.yieldcurve);
                        btnEdit.setText(data.yieldcurve);
                        btnEdit.focus();
                    }
                }

            }
        });
	}
	
	//根据买卖方向和固定浮动来自动生成固定/浮动方向
	function onFixFloatDirValidation(){
		var dir=mini.get("myDir").getValue();
		var myFixedFloat=mini.get("fixedFloatL").getValue();
		var itFixedFloat=mini.get("fixedFloatR").getValue();
		if(dir == "P"){//买入
			if(myFixedFloat == "Fixed" && itFixedFloat == "Float"){//买fixed卖float
				mini.get("fixedFloatDir").setValue("R");
			}else if(myFixedFloat == "Float" && itFixedFloat == "Fixed"){//买float卖fixed
				mini.get("fixedFloatDir").setValue("P");
			}
		}else if(dir == "S"){//卖出
			if(myFixedFloat == "Fixed" && itFixedFloat == "Float"){//卖fixed买float
				mini.get("fixedFloatDir").setValue("P");
			}else if(myFixedFloat == "Float" && itFixedFloat == "Fixed"){//卖float买fixed
				mini.get("fixedFloatDir").setValue("R");
			}
		}
		if(myFixedFloat == "Fixed" && itFixedFloat == "Fixed"){//都是fixed
			mini.get("fixedFloatDir").setValue("X");
		}
		if(myFixedFloat == "Float" && itFixedFloat == "Float"){//都是float
			mini.get("fixedFloatDir").setValue("L");
		}
	}
	
	/* 产品名称的选择 */
    function onPrdNameEdit(){
    	var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "/opics/prodMini.jsp",
            title: "选择产品名称",
            width: 700,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.pdesc);
                        btnEdit.setText(data.pdesc);
                        btnEdit.focus();
                    }
                }
            }
        });
    }
	
    function searchProductWeightNew(){
    	var form = new mini.Form("field_form");
    	var data = form.getData(true);
    	var mDate = data.endDate;
    	var vDate = data.forDate;//交易日期
    	if(CommonUtil.isNull(mDate)||CommonUtil.isNull(vDate)){
			return;
		}
    	searchProductWeight(20,0,prdNo,mDate);
    }
	
	function dirVerify(e){
		var myDir = mini.get("myDir").getValue();
		if(myDir == 'P'){//收取
			mini.get("oppoDir").setValue("S");
			mini.get("applyNo").setVisible(false);
			mini.get("applyProd").setVisible(false);
			mini.get("custNo").setVisible(false);
			mini.get("weight").setVisible(false);
        	<%-- mini.get("custNo").setValue("<%=__sessionUser.getInstId() %>");//占用授信主体
			mini.get("custNo").setText("<%=__sessionInstitution.getInstFullname()%>");//占用授信主体 --%>
		}else if(myDir == 'S'){//支付
			mini.get("oppoDir").setValue("P");
			mini.get("applyNo").setVisible(false);
			mini.get("applyProd").setVisible(false);
			mini.get("custNo").setVisible(true);
			mini.get("weight").setVisible(true);
			/* mini.get("custNo").setValue(mini.get("sellInst").getValue());//产品名称
			mini.get("custNo").setValue(mini.get("sellInst").getText());//产品名称 */
		}
	};	
    
</script>
<script type="text/javascript" src="../../Common/Flow/MiniApproveOpCommon.js"></script>			
</body>
</html>
