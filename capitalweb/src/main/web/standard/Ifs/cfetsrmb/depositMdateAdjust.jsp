<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>同业存放偿还状态</title>
    <script type="text/javascript" >
        /**prdCode用于页面投资组合的选择   该全局变量必须为prdCode*/
        var prdCode="MM";
    </script>
</head>
<body style="width: 100%; height: 100%; background: white">
<fieldset class="mini-fieldset">
    <legend>查询</legend>
    <div id="search_form" style="width: 100%;">
        <nobr>
            <input id="startDate" name="startDate" class="mini-datepicker" labelField="true" label="到期日期：" value="<%=__bizDate%>"
                   ondrawdate="onDrawDateStart" labelStyle="text-align:right;" emptyText="起始日期" format="yyyy-MM-dd"/>
            <span>~</span>
            <input id="endDate" name="endDate" class="mini-datepicker" value="<%=__bizDate%>"
                   ondrawdate="onDrawDateEnd" emptyText="结束日期" format="yyyy-MM-dd"/>
        </nobr>
        <input id="repayStatus" name="repayStatus" class="mini-combobox" data="CommonUtil.serverData.dictionary.RepayStatus" width="280px" emptyText="请选择偿还状态" labelField="true"  label="审批状态：" labelStyle="text-align:right;"/>
        <span style="float: right; margin-right: 100px">
            <a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
            <a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
        </span>
    </div>
</fieldset>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<span style="margin: 2px; display: block;">
	<a  id="edit_btn" class="mini-button" style="display: none"   onclick="edit(1)">提前还款</a>
    <a  id="save_btn" class="mini-button" style="display: none"   onclick="edit(2)">延期</a>
    <a  id="confirm_btn" class="mini-button" style="display: none"   onclick="edit(3)">已结清</a>
</span>
<div id="MiniSettleForeignSpot" class="mini-fit" style="margin-top: 2px;">
    <div id="datagrid" class="mini-datagrid borderAll" style="width: 100%; height: 100%;" allowAlternating="true"  multiSelect="true"
         allowResize="true" border="true" sortMode="client" >
        <div property="columns">
<%--            <div type="checkcolumn"></div>--%>
            <div type="indexcolumn" headerAlign="center" width="40">序号</div>
<%--            <div field="taskName" width="120px" allowSort="false" headerAlign="center" align="center">审批状态</div>--%>
            <%--            <div field="dealTransType" width="100px" allowSort="false" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'dealTransType'}">交易状态</div>--%>
            <div field="ticketId" width="100px" allowSort="false" headerAlign="center" align="center">交易单号</div>
            <div field="contractId" width="100px" allowSort="false" headerAlign="center" align="center">成交单号</div>
<%--            <div field="sponsor" width="100px" allowSort="false" headerAlign="center" align="center">审批发起人</div>--%>
<%--            <div field="sponInst" width="100px" allowSort="false" headerAlign="center" align="center">审批发起机构</div>--%>
<%--            <div field="aDate" width="100px" allowSort="false" headerAlign="center" align="center">审批开始时间</div>--%>
            <div field="custId" width="100px" allowSort="false" headerAlign="center" align="center">客户号</div>
            <div field="custName" width="100px" allowSort="false" headerAlign="center" align="center">客户名称</div>
            <div field="ctype" width="100px" allowSort="false" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'CType'}">客户类型</div>
            <div field="clitype" width="100px" allowSort="false" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'clitype'}">金融机构类型</div>
            <div field="repayStatus" width="100px" allowSort="false" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'RepayStatus'}">偿还状态</div>
            <div field="forDate" width="100px" allowSort="false" headerAlign="center" align="center">成交日期</div>
            <div field="firstSettlementDate" width="100px" allowSort="false" headerAlign="center" align="center">协议开始日期</div>
            <div field="secondSettlementDate" width="100px" allowSort="false" headerAlign="center" align="center">协议到期日期</div>
            <div field="currency" width="100px" allowSort="false" headerAlign="center" align="center">币种</div>
            <div field="amt" width="100px" allowSort="false" headerAlign="center" align="right"  numberFormat="#,0.0000">本金(元)</div>
            <div field="rateType" width="100px" allowSort="false" headerAlign="center" align="center">利率类型</div>
            <div field="basisRateCode" width="100px" allowSort="false" headerAlign="center" align="center">基准利率代码</div>
            <div field="benchmarkSpread" width="100px" allowSort="false" headerAlign="center"  align="right"  numberFormat="#,0.0000">利率点差(千分之一‰)</div>
            <div field="rate" width="100px" allowSort="false" headerAlign="center"  align="right"  numberFormat="#,0.0000">利率(%)</div>
            <div field="basis" width="100px" allowSort="false" headerAlign="center" align="center">计息基础</div>
            <div field="tenor" width="100px" allowSort="false" headerAlign="center" align="center">存期</div>
            <div field="punishRate" width="100px" allowSort="false" headerAlign="center"  align="right"  numberFormat="#,0.0000">提前支取利率(%)</div>
            <div field="paymentFrequency" width="100px" allowSort="false" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'intPayCycle'}">付息频率</div>
            <div field="trad" width="100px" allowSort="false" headerAlign="center" align="center">客户经理</div>
            <div field="bussInstId" width="100px" allowSort="false" headerAlign="center" align="center">业务归属部门</div>
            <div field="note" width="100px" allowSort="false" headerAlign="center" align="center">备注</div>
<%--            <div field="selfAcccode" width="100px" allowSort="false" headerAlign="center" align="center">付款账号</div>--%>
<%--            <div field="selfAccname" width="100px" allowSort="false" headerAlign="center" align="center">付款账户名称</div>--%>
<%--            <div field="selfBankcode" width="100px" allowSort="false" headerAlign="center" align="center">付款开户行大额行号</div>--%>
<%--            <div field="selfBankname" width="100px" allowSort="false" headerAlign="center" align="center">付款开户行名称</div>--%>
<%--            <div field="partyAcccode" width="100px" allowSort="false" headerAlign="center" align="center">收款账号</div>--%>
<%--            <div field="partyAccname" width="100px" allowSort="false" headerAlign="center" align="center">收款账户名称</div>--%>
<%--            <div field="partyBankcode" width="100px" allowSort="false" headerAlign="center" align="center">收款开户行大额行号</div>--%>
<%--            <div field="partyBankname" width="100px" allowSort="false" headerAlign="center" align="center">收款开户行名称</div>--%>
        </div>
    </div>
</div>
<script>
    /**************************************初始化*********************************************************/
    mini.parse();

    var url = window.location.search;
    var prdNo = CommonUtil.getParam(url, "prdNo");
    var prdName = CommonUtil.getParam(url, "prdName");
    var dealType = CommonUtil.getParam(url, "dealType");
    var form = new mini.Form("#search_form");
    var grid = mini.get("datagrid");

    grid.on("beforeload", function(e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize, pageIndex);
    });
    //控制按钮显示
    $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
        $(document).ready(function () {
            search(10, 0);
        });
    });


    grid.on("select",function(e){
        console.log(e)
        var status=e.record.repayStatus;
        var btn1=mini.get("edit_btn");
        var btn2=mini.get("save_btn");
        var btn3=mini.get("confirm_btn");
        if("1"===status){
            //提前还款
            btn1.setEnabled(false);
            btn2.setEnabled(false);
            btn3.setEnabled(false);
        }else if("2"===status){
            //延期
            btn1.setEnabled(false);
            btn2.setEnabled(false);
            btn3.setEnabled(true);
        }else if("3"===status){
            //已结清
            btn1.setEnabled(false);
            btn2.setEnabled(false);
            btn3.setEnabled(false);
        }else {
            //正常存续
            btn1.setEnabled(true);
            btn2.setEnabled(true);
            btn3.setEnabled(true);
        }



    })
    /**************************************按钮方法*********************************************************/

    function search(pageSize,pageIndex){
        form.validate();
        if(form.isValid()==false){
            mini.alert("信息填写有误，请重新填写","系统提示");
            return;
        }
        var data=form.getData(true);
        data['pageNumber']=pageIndex+1;
        data['pageSize']=pageSize;
        data['branchId']=branchId;
        var url=null;
        url = "/IfsRmbDepositInController/selectAdjustDepositPage";

        var params = mini.encode(data);
        CommonUtil.ajax({
            url:url,
            data:params,
            callback : function(data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }

    function query() {
        search(grid.pageSize, 0);
    }
``
    function clear(){
        // MiniSettleForeigDetail.clear();
        form.clear();
        search(10,0);
    }

    //提前还款/延期 跳转修改页面
    function edit(status){
        var row = grid.getSelected();
        // var selections = grid.getSelecteds();
        // if(selections.length>1){
        //     mini.alert("系统不支持多条数据进行编辑","消息提示");
        //     return;
        // }
        if(row){
            console.log(row)
            row.repayStatus=status;
            //修改状态
            var params=row;
            CommonUtil.ajax({
                url:"/IfsRmbDepositInController/editRmbDepositIn",
                data:params,
                callback : function(data) {
                    mini.alert("修改成功!");
                    return;
                }
            });


            // var url = CommonUtil.baseWebPath() + "/cfetsrmb/rmbDepositInEdit.jsp?action=adjust&ticketid="+row.ticketId+"&prdNo="+prdNo+"&prdName="+prdName+"&dealType="+dealType;
            // var tab = { id: "MiniRmbDepositInRepay", name: "MiniRmbDepositInRepay", title: "同业存放偿还状态", url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
            // var paramData = {selectData:row};
            // CommonUtil.openNewMenuTab(tab,paramData);
        } else {
            mini.alert("请选中一条记录！","消息提示");
        }
    }

    /**************************其他方法****************************/

    //交易日期
    function onDrawDateStart(e) {
        var startDate = e.date;
        var endDate= mini.get("endDate").getValue();
        if(CommonUtil.isNull(endDate)){
            return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }

    function onDrawDateEnd(e) {
        var endDate = e.date;
        var startDate = mini.get("startDate").getValue();
        if(CommonUtil.isNull(startDate)){
            return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }
</script>
</body>
</html>