<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>同业存放</title>
    <script type="text/javascript" >
        /**prdCode用于页面投资组合的选择   该全局变量必须为prdCode*/
        var prdCode="MM";
    </script>
</head>
<body style="width:100%;height:100%;background:white">
<div class="mini-splitter" style="width:100%;height:100%;" id="splitter">
    <div size="90%" showCollapseButton="false">
        <h1 style="text-align:center"><strong></strong></h1>
        <div  id="field_form"  class="mini-fit area" style="background:white" >
            <input id="dealer" name="dealer" class="mini-hidden" />
            <input id="dealTransType" name="dealTransType" class="mini-hidden" />
            <%--            <input id="ticketId" name="ticketId" class="mini-hidden" />--%>
            <%--            <input id="aDate" name="aDate" class="mini-hidden"/>--%>
            <div class="mini-panel" title="成交单编号" style="width:100%;"  allowResize="true" collapseOnTitleClick="false">
                <div class="leftarea">
                    <input style="width:98%;" id="contractId" name="contractId" class="mini-textbox mini-mustFill" labelField="true" requiredErrorText="该输入项为必输项" label="成交单编号：" required="true"  labelStyle="text-align:left;width:130px;"  vtype="maxLength:20" onvalidation="onEnglishAndNumberValidation"/>
                </div>
                <div class="rightarea">
                    <input  style="width:100%;" id="cfetscn" name="cfetscn" class="mini-textbox" label="CFETS机构码：" labelField="true" visible="false" enabled="false"  labelStyle="text-align:left;width:130px;" visible="flase"/>
                </div>
            </div>
            <div class="mini-panel" title="基础信息" style="width:100%"  allowResize="true" collapseOnTitleClick="false">
                <div class="leftarea">
                    <fieldset>
                        <input id="ticketId" name="ticketId" class="mini-textbox"  labelField="true"  label="合同号："  style="width:100%;" emptyText="系统自动生成" enabled="false" labelStyle="text-align:left;width:120px;"/>
                        <input id="sponsor" name="sponsor" class="mini-textbox" labelField="true"  label="审批发起人："  vtype="maxLength:20"  enabled="false" labelStyle="text-align:left;width:120px;" style="width:100%;"   />
                    </fieldset>
                </div>
                <div class="rightarea">
                    <fieldset>
                        <input id="sponInst" name="sponInst" class="mini-textbox" labelField="true" label="审批发起机构：" allowInput="false" style="width:100%;"  labelStyle="text-align:left;width:120px;"  enabled="false"/>
                        <input id="aDate" name="aDate" class="mini-datepicker" labelField="true"  label="审批开始日期：" vtype="maxLength:20" labelStyle="text-align:left;width:120px;" style="width:100%;" enabled="false"/>
                    </fieldset>
                </div>
            </div>
            <div class="mini-panel" title="交易对手信息" style="width:100%"  allowResize="true" collapseOnTitleClick="false">
                <div class="leftarea">
                    <fieldset>
                        <input id="custId" name="custId" class="mini-buttonedit mini-mustFill" onbuttonclick="onButtonEdit" labelField="true" required="true" label="客户号：" allowInput="false" style="width:100%;" labelStyle="text-align:left;width:120px;" />
                        <input id="ctype" name="ctype" class="mini-combobox mini-mustFill" labelField="true"  label="客户类型："  style="width:100%;" required="true" data="CommonUtil.serverData.dictionary.CType" enabled="false" labelStyle="text-align:left;width:120px;"/>
                    </fieldset>
                </div>
                <div class="rightarea">
                    <fieldset>
                        <input id="custName" name="custName" class="mini-textbox mini-mustFill"  labelField="true"  label="客户名称："  style="width:100%;" required="true"   enabled="false" labelStyle="text-align:left;width:120px;"/>
                        <input id="clitype" name="clitype" class="mini-combobox mini-mustFill"  labelField="true"  label="金融机构类型："  style="width:100%;" required="true" data="CommonUtil.serverData.dictionary.clitype"  enabled="false" labelStyle="text-align:left;width:120px;"/>
                    </fieldset>
                </div>
            </div>
            <div class="mini-panel" title="交易主体信息" style="width:100%"  allowResize="true" collapseOnTitleClick="false">
                <div class="leftarea">
                    <input style="width:100%;" id="rateType" name="rateType" class="mini-combobox mini-mustFill" labelField="true"  data="CommonUtil.serverData.dictionary.fixfloat" label="利率类型："   labelStyle="text-align:left;width:130px;" required="true"  allowInput="false"/>
                    <input style="width:100%" id="benchmarkSpread" name="benchmarkSpread" class="mini-spinner mini-mustFill input-text-strong" labelField="true"  label="利率点差(千分之一)：" minValue="-1000000" required="true" format="n4" maxValue="99999.9999"  changeOnMousewheel="false"   labelStyle="text-align:left;width:130px;" onValuechanged="interestAmount" />
                    <input style="width:100%;" id="firstSettlementDate" name="firstSettlementDate" class="mini-datepicker mini-mustFill" labelField="true" onValuechanged="compareDate" label="协议开始日期：" ondrawdate="onDrawDateStart" required="true"   labelStyle="text-align:left;width:130px;" />
                    <input style="width:100%" id="tenor" name="tenor" class="mini-textbox mini-mustFill" labelField="true"  label="存期(天)：" required="true"   labelStyle="text-align:left;width:130px;" />
                    <input style="width:100%;" id="amt" name="amt" class="mini-spinner mini-mustFill input-text-strong" labelField="true"  changeOnMousewheel='false'  format="n4" label="本金(元)：" required="true"  maxValue="99999999999999.9999" labelStyle="text-align:left;width:130px;" onValuechanged="interestAmount"/>
                    <input style="width:100%" id="settlementAmount" name="settlementAmount"  class="mini-spinner mini-mustFill input-text-strong" labelField="true" label="到期金额(元)："  minValue="0" required="true" format="n4" maxValue="99999999999999.9999"  changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"/>
                    <input id="scheduleType" name="scheduleType" class="mini-combobox  mini-mustFill" required="true" labelField="true" label="付息类型：" labelStyle="text-align:left;width:130px;" style="width:100%;"
                           data="CommonUtil.serverData.dictionary.scheduleType" value="D" onvaluechanged="selectCycle"/>
                    <input style="width:100%;" id="firstSettlDay" name="firstSettlDay" class="mini-datepicker" labelField="true"  label="首次结息日："  labelStyle="text-align:left;width:130px;"/>
                    <input style="width:100%;" id="settlDayAgreement" name="settlDayAgreement" class="mini-combobox" labelField="true" data="CommonUtil.serverData.dictionary.SettAgreement" label="结息日约定：" labelStyle="text-align:left;width:130px;" allowInput="false"/>
                    <input style="width:100%;" id="advAgreement" name="advAgreement" class="mini-combobox mini-mustFill" labelField="true" data="CommonUtil.serverData.dictionary.AdvAgreement" label="提前支取约定：" labelStyle="text-align:left;width:130px;" required="true" allowInput="false" value="1" onvaluechanged="selectAgreement"/>
                    <input id="payIncomWay" name="payIncomWay" class="mini-textbox" labelField="true"  label="支取方式：" vtype="maxLength:20" labelStyle="text-align:left;width:130px;" style="width:100%;" />
                    <input id="forDate" name="forDate" class="mini-datepicker" labelField="true"  label="交易日期："  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
                    <input style="width:100%;" id="depositBalance" name="depositBalance" class="mini-spinner mini-mustFill input-text-strong" labelField="true"  changeOnMousewheel='false'  format="n4" label="同业存款余额(元)：" required="true"  maxValue="99999999999999.9999" labelStyle="text-align:left;width:130px;"/>
                </div>
                <div class="rightarea">
                    <input style="width: 100%;" id="basisRateCode" name="basisRateCode" class="mini-buttonedit mini-mustFill" onbuttonclick="onRateEdit" labelField="true" label="基准利率代码："  labelStyle="text-align:left;width:130px;" allowInput="false" requiredErrorText="该输入项为必输项"  required="true" />
                    <input style="width:100%" id="rate" name="rate" class="mini-spinner mini-mustFill input-text-strong" labelField="true"  label="利率(%)：" minValue="0" required="true"  format="n4" maxValue="99999.9999"  changeOnMousewheel="false"   labelStyle="text-align:left;width:130px;" />
                    <input style="width:100%;" id="secondSettlementDate" name="secondSettlementDate" class="mini-datepicker mini-mustFill" labelField="true" onValuechanged="compareDate" label="协议到期日期：" ondrawdate="onDrawDateEnd" required="true"  labelStyle="text-align:left;width:130px;"    />
                    <input style="width:100%" id="occupancyDays" name="occupancyDays" class="mini-textbox mini-mustFill" labelField="true" label="实际占款期限(天)：" required="true" labelStyle="text-align:left;width:130px;"/>
                    <input style="width:100%;" id="basis" name="basis" class="mini-combobox mini-mustFill" labelField="true"  label="计息基准："   data="CommonUtil.serverData.dictionary.Basis" labelStyle="text-align:left;width:130px;" required="true" onValuechanged="interestAmount" />
                    <input style="width:100%;" id="accuredInterest" name="accuredInterest" class="mini-spinner mini-mustFill input-text-strong" labelField="true" changeOnMousewheel='false' format="n4" label="到期利息(元)：" required="true" maxValue="99999999999999.9999" labelStyle="text-align:left;width:130px;" onValuechanged="interestAmount"/>
                    <input style="width:100%" id="paymentFrequency" name="paymentFrequency" class="mini-combobox mini-mustFill" labelField="true"  label="付息频率：" required="true"   labelStyle="text-align:left;width:130px;" data="CommonUtil.serverData.dictionary.intPayCycle"/>
                    <input style="width:100%;" id="firstPayDay" name="firstPayDay" class="mini-datepicker" labelField="true" label="首次付息日：" labelStyle="text-align:left;width:130px;"/>
                    <input style="width:100%;" id="payDayAgreement" name="payDayAgreement" class="mini-combobox" labelField="true" data="CommonUtil.serverData.dictionary.SettAgreement" label="付息日约定："  labelStyle="text-align:left;width:130px;" allowInput="false"/>
                    <input style="width:100%" id="advRate" name="advRate" class="mini-spinner input-text-strong" labelField="true" label="提前支取预期利率（%）："  minValue="-100" format="n4" maxValue="100" changeOnMousewheel="false"  labelStyle="text-align:left;width:130px;"/>
                    <input style="width:100%;" id="repayStatus" name="repayStatus" class="mini-combobox" labelField="true"  label="偿还状态：" vtype="maxLength:20" labelStyle="text-align:left;width:130px;" data="CommonUtil.serverData.dictionary.RepayStatus" visible="false" value="0"/>
                    <input style="width:100%;" id="autoTransInd" name="autoTransInd" class="mini-textbox" labelField="true"  label="自动转存标志：" vtype="maxLength:20" labelStyle="text-align:left;width:130px;"/>
                    <input style="width:100%" id="punishRate" name="punishRate" class="mini-spinner mini-mustFill input-text-strong" labelField="true"  label="提前支取利率(%)：" minValue="0" required="true" format="n4" maxValue="99999.9999" changeOnMousewheel="false"   labelStyle="text-align:left;width:130px;" visible="false" />
                    <input style="width:100%;" id="currency" name="currency" class="mini-combobox mini-mustFill" labelField="true"  data="CommonUtil.serverData.dictionary.Currency" label="币种：" value="CNY"  enabled="false" labelStyle="text-align:left;width:130px;" required="true"  allowInput="false"/>
                </div>
            </div>
            <div class="mini-panel" title="报表要素" style="width:100%;" allowResize="true" collapseOnTitleClick="false">
                <div class="leftarea">
                    <input id="attributionDept" name="attributionDept" valueField="id" showFolderCheckBox="true" labelField="true" label="归属机构：" showCheckBox="true" style="width:100%"
                           labelStyle="text-align:left;width:130px" showTreeLines="true" showTreeIcon="true" textField="instName" parentField="parentId"  class="mini-treeselect" expandOnLoad="true" resultAsTree="false"  valueFromSelect="true" emptyText="请选择机构"/>
                    <input id="accountNature" name="accountNature" class="mini-combobox" labelField="true" label="账户性质：" labelStyle="text-align:left;width:130px;" style="width:100%;" data="CommonUtil.serverData.dictionary.accountNature" value="1"/>
                    <input id="pricingStandardType" name="pricingStandardType" class="mini-combobox" labelField="true" label="定价基准类型：" labelStyle="text-align:left;width:130px;" style="width:100%;" data="CommonUtil.serverData.dictionary.pricingStandardType" enabled="false"/>
                    <input id="depositAccount" name="depositAccount" class="mini-textbox" labelField="true" label="存款账号：" labelStyle="text-align:left;width:130px;" style="width:100%;" />
                    <input style="width:100%;" id="note" name="note" class="mini-textbox" labelField="true"  label="交易事由：" vtype="maxLength:20" labelStyle="text-align:left;width:130px;" />
                </div>
                <div class="rightarea">
                    <input id="fiveLevelClass" name="fiveLevelClass" class="mini-combobox" labelField="true" requiredErrorText="该输入项为必输项" label="五级分类：" labelStyle="text-align:left;width:130px;"  style="width:100%;" data="CommonUtil.serverData.dictionary.FiveLevelClass" value="0"/>
                    <input style="width:100%;" id="accountUse" name="accountUse" class="mini-textbox" labelField="true" label="账户用途：" labelStyle="text-align:left;width:130px;"/>
                    <input id="rateFloatFrequency" name="rateFloatFrequency" class="mini-combobox" labelField="true" label="利率浮动频率：" labelStyle="text-align:left;width:130px;" style="width:100%;" data="CommonUtil.serverData.dictionary.rateFloatFrequency"/>
                    <input style="width:100%;" id="trad" name="trad" class="mini-textbox" labelField="true"  label="客户经理：" vtype="maxLength:20" labelStyle="text-align:left;width:130px;"  />
                </div>
            </div>
            <div class="mini-panel" title="清算信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
                <div class="leftarea">
                    <fieldset>
                        <legend>本方账户</legend>
                        <input	style="width:100%;" id="selfAcccode" name="selfAcccode" class="mini-textbox" labelField="true"  label="本方帐号：" 	labelStyle="text-align:left;width:120px;"	onvalidation="CommonUtil.onValidation(e,'alphanum',[null])"  vtype="maxLength:1333" value = "9001000113901401"/>
                        <input	style="width:100%;" id="selfAccname" name="selfAccname" class="mini-textbox" labelField="true"  label="本方帐号名称："  labelStyle="text-align:left;width:120px;"	vtype="maxLength:1333" value = "投资类业务"/>
                        <input	style="width:100%;" id="selfBankcode" name="selfBankcode" class="mini-textbox" labelField="true"  label="本方开户行行号："  labelStyle="text-align:left;width:120px;"	onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" vtype="maxLength:400" value = "9001000113901401"/>
                        <input	style="width:100%;" id="selfBankname" name="selfBankname" class="mini-textbox" labelField="true"  label="本方开户行名称："  labelStyle="text-align:left;width:120px;"	vtype="maxLength:400" value = "投资类业务" />
                    </fieldset>
                </div>
                <div class="rightarea">
                    <fieldset>
                        <legend>对手方账户</legend>
                        <input	style="width:100%;" id="partyAcccode" name="partyAcccode" class="mini-textbox" labelField="true"  label="对方帐号："  labelStyle="text-align:left;width:120px;"	onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" vtype="maxLength:1333" />
                        <input	style="width:100%;" id="partyAccname" name="partyAccname" class="mini-textbox" labelField="true"  label="对方帐号名称："  labelStyle="text-align:left;width:120px;"	vtype="maxLength:1333" />
                        <input	style="width:100%;" id="partyBankcode" name="partyBankcode" class="mini-textbox" labelField="true"  label="对方开户行行号：" 	labelStyle="text-align:left;width:120px;"	onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" vtype="maxLength:400" />
                        <input	style="width:100%;" id="partyBankname" name="partyBankname" class="mini-textbox" labelField="true"  label="对方开户行名称："  labelStyle="text-align:left;width:120px;"	vtype="maxLength:400" />
                    </fieldset>
                </div>
            </div>

            <%@ include file="../../Common/Flow/MiniApproveOpCommon.jsp"%>
            <%@ include file="../../Common/print/approveFlowLog.jsp"%>
        </div>
    </div>
    <div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
        <div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="save_btn"  onclick="save">保存交易</a></div>
        <div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="close_btn"  onclick="close">关闭界面</a></div>
    </div>
</div>
<script type="text/javascript">
    /**************************************初始化*********************************************************/
    mini.parse();
    var detailData;

    var url = window.location.search;
    var action = CommonUtil.getParam(url, "action");
    var ticketId = CommonUtil.getParam(url, "ticketid");
    var prdNo = CommonUtil.getParam(url, "prdNo");
    var fPrdCode = CommonUtil.getParam(url, "fPrdCode");
    var prdName = CommonUtil.getParam(url, "prdName");
    var dealType = CommonUtil.getParam(url, "dealType");

    var params ={};
    var tradeData={};
    if(action!="print"){
        //获取当前tab
        var currTab = top["win"].tabs.getActiveTab();
        var params = currTab.params;
        var row=params.selectData;
        tradeData.selectData=row;
        tradeData.operType=action;
        tradeData.serial_no=row.ticketId;
        tradeData.task_id=row.taskId;
    }
    var ApproveFlowLog = {};
    var log_grid = mini.get("log_grid");//隐藏日志信息列表，打印时显示
    log_grid.hide();
    /**
     *   获取审批日志信息
     */
    ApproveFlowLog.loadLogInfo = function(){
        CommonUtil.ajax({
            url:"/IfsFlowController/approveLog",
            data:{'serial_no':ticketId},
            callback : function(data) {
                if(data != null){
                    log_grid.setData(data.obj);
                    if(typeof(FlowDesigner) != "undefined")
                    {
                        FlowDesigner.addApporveLog(data.obj);
                    }
                }
            }
        });
    };
    mini.get("forDate").setValue("<%=__bizDate %>");
    // mini.get("instId").setEnabled(false);
    var timestamp = Date.parse(new Date());
    sys(timestamp);
    function sys(stamp){
        var time = new Date(stamp);
        var result = "";
        result += CommonUtil.singleNumberFormatter(time.getHours()) + ":";
        result += CommonUtil.singleNumberFormatter(time.getMinutes()) + ":";
        result += CommonUtil.singleNumberFormatter(time.getSeconds());
    }

    $(document).ready(function() {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
            inme();
            loadInstitutionTree();
            selectCycle();
            selectAgreement();
        });
    });
    /**
     *   获取机构
     */
    function loadInstitutionTree(){
        CommonUtil.ajax({
            data:{"branchId":branchId},
            url:"/InstitutionController/searchInstitutions",
            callback:function(data){
                mini.get("attributionDept").setData(data.obj);
                if(detailData!=null&&detailData.attributionDept!=null){
                    mini.get("attributionDept").setValue(detailData.attributionDept);
                }else {
                    mini.get("attributionDept").setValue("<%=__sessionInstitution.getInstId()%>");
                }
            }
        });
    }
    function inme() {
        if (action == "detail" || action == "edit"||action=="approve"||action=="print"||action=="adjust") {
            var from = new mini.Form("field_form");
            $("#fileMsg").hide();
            CommonUtil.ajax({
                url : '/IfsRmbDepositInController/searchByTicketId',
                data : {
                    ticketId : ticketId
                },
                callback : function(text) {
                    detailData=text.obj;
                    from.setData(text.obj);
                    mini.get("custId").setValue(text.obj.custId);
                    mini.get("custId").setText(text.obj.custId);
                    mini.get("basisRateCode").setValue(text.obj.basisRateCode);
                    mini.get("basisRateCode").setText(text.obj.basisRateCode);

                    if(action == "edit"){
                        if (text.obj.scheduleType == "D") {
                            mini.get("paymentFrequency").setEnabled(false);
                            // mini.get("intpayday").setEnabled(false);
                            // mini.get("intdaterule").setEnabled(false);
                            mini.get("firstSettlDay").setEnabled(false);
                            mini.get("firstPayDay").setEnabled(false);
                            mini.get("settlDayAgreement").setEnabled(false);
                            mini.get("payDayAgreement").setEnabled(false);
                        } else if (text.obj.scheduleType == "IR") {
                            mini.get("paymentFrequency").setEnabled(true);
                            // mini.get("intpayday").setEnabled(true);
                            // mini.get("intdaterule").setEnabled(true);
                            mini.get("firstSettlDay").setEnabled(true);
                            mini.get("firstPayDay").setEnabled(true);
                            mini.get("settlDayAgreement").setEnabled(true);
                            mini.get("payDayAgreement").setEnabled(true);
                        }
                        if (text.obj.advAgreement == "1") {
                            mini.get("advRate").setEnabled(false);
                        } else {
                            mini.get("advRate").setEnabled(true);
                        }
                    }

                }
            });
            if (action == "detail"||action=="approve") {
                mini.get("save_btn").hide();
                from.setEnabled(false);
                var form1=new mini.Form("approve_operate_form");
                form1.setEnabled(true);
                // mini.get("upload_btn").setEnabled(false);
            }
            if(action=="print"){
                from.setEnabled(false);
                $("#fileMsg").hide();
                mini.get("splitter").hidePane(2);

                log_grid.show();
                CommonUtil.ajax({
                    url:"/IfsFlowController/getOneFlowDefineBaseInfo",
                    data:{serial_no:ticketId},
                    callerror: function(data){
                        ApproveFlowLog.loadLogInfo();
                    },
                    callback : function(data) {
                        var innerInterval;
                        innerInitFunction = function(){
                            clearInterval(innerInterval);
                            ApproveFlowLog.loadLogInfo();
                        },
                            innerInterval = setInterval("innerInitFunction()",100);
                    }
                });
            }
            if(action=="adjust"){ //偿还状态调整
                mini.get("save_btn").show();
                mini.get("close_btn").show();
                from.setEnabled(false);
                mini.get("repayStatus").setVisible(true);
                mini.get("repayStatus").setEnabled(true);
            }

            mini.get("contractId").setEnabled(false);
        }else if(action=="add"){
            mini.get("dealTransType").setValue("1");
            <%--mini.get("instId").setValue("<%=__sessionInstitution.getInstFullname()%>");--%>
            mini.get("dealer").setValue("<%=__sessionUser.getUserName() %>");
            <%--mini.get("sponsor").setValue("<%=__sessionUser.getUserId()%>");--%>
            mini.get("sponsor").setValue("<%=__sessionUser.getUserName()%>");
            <%--mini.get("sponInst").setValue("<%=__sessionUser.getInstId() %>");--%>
            mini.get("sponInst").setValue("<%=__sessionInstitution.getInstFullname() %>");
            $("#fileMsg").hide();
        }
        mini.get("dealTransType").setEnabled(false);
    }

    /**************************************按钮方法*********************************************************/
    function save() {
        var form = new mini.Form("field_form");
        form.validate();
        if (form.isValid() == false) {
            mini.alert("信息填写有误，请重新填写","系统提示");
            return;
        }
        var form = new mini.Form("field_form");
        var data = form.getData(true); //获取表单多个控件的数据
        data['sponInst']="<%=__sessionInstitution.getInstId()%>";
        data['sponsor']="<%=__sessionUser.getUserId() %>";
        data['fPrdCode']=fPrdCode;
        //data['dealTransType']="1";//修改后的状态永远为1
        var json = mini.encode(data); //序列化成JSON
        mini.confirm("确认以当前数据为准吗？","确认",function (actions) {
            if (actions != "ok") {
                return;
            }
            CommonUtil.ajax({
                url :action == "add" ? "/IfsRmbDepositInController/addRmbDepositIn" : "/IfsRmbDepositInController/editRmbDepositIn",
                data:json,
                callback:function(data){
                    mini.alert(data.desc,'提示信息',function(){//提示弹框并执行回调
                        if(data.code =='error.common.0000'){
                            top["win"].closeMenuTab();//关闭并刷新当前界面
                        }
                    });
                }
            });
        });
    }

    function close() {
        top["win"].closeMenuTab();
    }

    /**************************************时间触发*********************************************************/

    function onButtonEdit(e) {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Common/CustMini.jsp",
            title: "选择对手方列表",
            width: 900,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.cno);
                        btnEdit.setText(data.cno);
                        mini.get("custName").setValue(data.cliname);//客户名称
                        mini.get("ctype").setValue(data.ctype);//客户类型
                        mini.get("clitype").setValue(data.clitype);//金融机构类型

                        btnEdit.focus();
                    }
                }
            }
        });
    }

    function  onCreditTypeChange(){
        var cno = mini.get("counterpartyInstId").getValue();
        creditsubvalue(cno,"438",null);
    }

    //根据交易对手编号查询全称
    function queryTextName(cno){
        CommonUtil.ajax({
            url: "/IfsOpicsCustController/searchIfsOpicsCust",
            data : {'cno' : cno},
            callback:function (data) {
                if (data && data.obj) {
                    mini.get("counterpartyInstId").setText(data.obj.cliname);
                } else {
                    mini.get("counterpartyInstId").setText();
                }
            }
        });
    }
    //控制付息频率Enabled
    function selectCycle() {
        var param = mini.get("scheduleType").getValue();
        if (param == "D") {
            mini.get("paymentFrequency").setEnabled(false);
            mini.get("paymentFrequency").setValue("");

            mini.get("firstSettlDay").setEnabled(false);
            mini.get("firstPayDay").setEnabled(false);
            mini.get("settlDayAgreement").setEnabled(false);
            mini.get("payDayAgreement").setEnabled(false);
        } else if (param == "IR") {
            mini.get("paymentFrequency").setEnabled(true);

            mini.get("firstSettlDay").setEnabled(true);
            mini.get("firstPayDay").setEnabled(true);
            mini.get("settlDayAgreement").setEnabled(true);
            mini.get("payDayAgreement").setEnabled(true);
        }
    }

    //提前支取情况
    function selectAgreement() {
        var param = mini.get("advAgreement").getValue();
        if (param == "1") {
            mini.get("advRate").setEnabled(false);
        } else {
            mini.get("advRate").setEnabled(true);
        }
    }

    //利率代码的选择
    function onRateEdit(){
        var btnEdit = this;
        var ccy =mini.get("currency").getValue();
        mini.open({
            url: CommonUtil.baseWebPath() + "/opics/rateMini.jsp?ccy="+ccy,
            title: "选择利率代码",
            width: 900,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        mini.get("pricingStandardType").setValue(data.ratetype);
                        mini.get("rateFloatFrequency").setText("");
                        if("Fixed"==data.ratetype){
                            mini.get("rateFloatFrequency").setEnabled(false);
                        }else{
                            mini.get("rateFloatFrequency").setEnabled(true);
                        }
                        btnEdit.setValue(data.ratecode);
                        btnEdit.setText(data.ratecode);
                        interestAmount();
                        btnEdit.focus();
                    }
                }
            }
        });
    }

    //英文、数字、下划线 的验证
    function onEnglishAndNumberValidation(e) {
        if(e.value == "" || e.value == null){//值为空，就不做校验
            return;
        }
        if (e.isValid) {
            if (isEnglishAndNumber(e.value) == false) {
                e.errorText = "必须输入英文+数字";
                e.isValid = false;
            }
        }
    }
    /* 是否英文+数字 */
    function isEnglishAndNumber(v) {
        var re = new RegExp("^[0-9a-zA-Z\_\-]+$");
        if (re.test(v)) return true;
        return false;
    }

    //日期判断
    function compareDate() {
        var sta = mini.get("firstSettlementDate");
        var end = mini.get("secondSettlementDate");
        if(sta.getValue()!=null && sta.getValue("")&& end.getValue("") && end.getValue()!=null){
            sDate1 = Date.parse(sta.getValue());
            sDate2 = Date.parse(end.getValue());
            dateSpan = sDate2 - sDate1;
            dateSpan = Math.abs(dateSpan);
            iDays = Math.floor(dateSpan / (24 * 3600 * 1000));
            mini.get("occupancyDays").setValue(iDays);
            mini.get("tenor").setValue(iDays);
            interestAmount();
        }
    }
    //到期日
    function onDrawDateEnd(e) {
        var date = e.date;
        var d = mini.get("firstSettlementDate").getValue();
        if(d==null || d == ""){
            return;
        }

        if (date.getTime() < d.getTime()) {
            e.allowSelect = false;
        }
    }
    //起息日
    function onDrawDateStart(e){
        var date = e.date;
        var d = mini.get("secondSettlementDate").getValue();
        if(d==null || d == ""){
            return;
        }

        if (date.getTime() > d.getTime()) {
            e.allowSelect = false;
        }
    }
    //利率计算
    function interestAmount(){
        var a = mini.get("rate").getValue();//利率
        var b = mini.get("basis").getValue();//利率基础
        var c = mini.get("occupancyDays").getValue();//占款天数
        var d = mini.get("amt").getValue();//拆借金额
        var ccy=mini.get("currency").getValue();//币种
        var spread = mini.get("benchmarkSpread").getValue();//利率点差(‰)
        if(ccy=="JPY"){//交易币种是日元时无角分
            d=Math.ceil(d);
            mini.get("amount").setValue(d);
        }
        if(a!="" && a!=null && b!="" && b!=null && c!="" && c!=null && d!="" && d!=null){
            if(b!='A360'){
                var e = 365;
            }else{
                var e = 360;
            }
            var interest=(Number(a) + Number(spread) / 100)*Number(c)*Number(d)/(parseInt(e)*100);
            if(ccy=="JPY"){  // 交易币种是日元时无角分
                interest=Math.ceil(interest);
            }
            mini.get("accuredInterest").setValue(interest);
            var allamount = interest + Number(d);
            mini.get("settlementAmount").setValue(allamount);
        }
    }


    //根据英文节点类型返回相应中文
    ApproveFlowLog.ActivityType = function(type){
        var result = null;
        switch(type){
            case "startEvent":
                result = "开始节点";
                break;
            case "exclusiveGateway":
                result = "决策节点";
                break;
            case "userTask":
                result = "人工节点";
                break;
            case "endEvent":
                result = "结束节点";
                break;
            default:
                result = type;
        }
        return result;
    };

    function stampToTimeRenderer(e) {
        if(e.value){
            return CommonUtil.stampToTime(e.value);
        }else{
            return "";
        }
    }

    function timerFormatRenderer(e) {
        if(e.value){
            return CommonUtil.secondFormatter(e.value/1000);
        }else{
            return "";
        }
    }

    function activityTypeRenderer(e){
        if(e.value){
            return e.value;
        }else{
            return ApproveFlowLog.ActivityType(e.value);
        }
    }
</script>
<script type="text/javascript" src="../../Common/Flow/MiniApproveOpCommon.js"></script>
</body>
</html>