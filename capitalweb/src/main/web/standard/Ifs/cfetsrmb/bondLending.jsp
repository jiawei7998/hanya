<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
<script type="text/javascript" >
	/**prdCode用于页面投资组合的选择   该全局变量必须为prdCode*/
	var prdCode="SECLEN";
</script>
<title>债券借贷</title>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
	<legend>债券借贷成交单查询</legend>	
	<div>
		<div id="search_form" style="width:100%" cols="6">
			<input id="startDate" name="startDate" class="mini-datepicker" labelField="true"  label="成交日期："
				   ondrawdate="onDrawDateStart" format="yyyy-MM-dd" value="<%=__bizDate%>"  labelStyle="text-align:right;" emptyText="开始日期" />
			<span>~</span>
			<input id="endDate" name="endDate" class="mini-datepicker"
				   ondrawdate="onDrawDateEnd" emptyText="结束日期" format="yyyy-MM-dd" value="<%=__bizDate%>"/>
			<input id="contractId" name="contractId" class="mini-textbox" labelField="true"  label="成交单编号：" width="280px" labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请输入成交编号" />
			<input id="ticketId" name="ticketId" class="mini-textbox" labelField="true" label="审批单号：
" labelStyle="text-align:right;" emptyText="请填写审批单号" width="280px" />
			<input id="dealNo" name="dealNo" class="mini-textbox" labelField="true"
				   label="opics交易号：" emptyText="请填写opics交易号" labelStyle="text-align:right;" width="280px"/>
			<input id="approveStatus" name="approveStatus" class="mini-combobox" data="CommonUtil.serverData.dictionary.ApproveSta" width="280px" emptyText="请选择审批状态" labelField="true"  label="审批状态：" labelStyle="text-align:right;"/>
			<span style="float: right; margin-right: 150px"> 
				<a id="search_btn" class="mini-button" style="display: none"   onclick="searchs()">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
			</span>
		</div>
	</div>
</fieldset> 
<%@ include file="../batchReback.jsp"%>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>   
<div class="mini-fit" >
	<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:80%;" idField="id"  allowAlternating="true"
		onrowdblclick="onRowDblClick" allowResize="true" sortMode="client" allowAlternating="true" multiSelect="true">
		<div property="columns">
		<div type="checkcolumn"></div>
			<div type="indexcolumn" width="50px" headerAlign="center">序号</div>
			<div field="prdNo" width="100px" allowSort="false" headerAlign="center" align="center" visible="false"></div>
			<div field="taskName" width="120px" allowSort="false" headerAlign="center" align="center">审批状态</div>
			<div field="contractId" width="160px" align="center"  headerAlign="center" >成交单编号</div>
			<div field="ticketId" width="160px" align="center"  headerAlign="center" >审批单编号</div> 
			<div field="dealNo" width="100px" align="center"  headerAlign="center" >opics交易号</div> 
			<div field="statcode" width="160px" align="center"  headerAlign="center">opics处理状态</div>
			<div field="lendInst" width="150px" align="center"  headerAlign="center" >对手方机构</div>
			<div field="myDir" width="100px" align="center" headerAlign="center"  renderer="CommonUtil.dictRenderer" data-options="{dict:'SlTrading'}">本方方向</div>
			<div field="underlyingSymbol" width="200px" align="center"  headerAlign="center" >标的债券简称</div>
			<div field="underlyingQty" width="140px" align="right"  headerAlign="center" allowSort="true" numberFormat='n4'>标的债券券面总额（万元）</div>
			<div field="tradingProduct" width="100px" align="center"  headerAlign="center" allowSort="true">交易品种</div>
			<div field="firstSettlementDate" width="100px" align="center"  headerAlign="center" allowSort="true">首次结算日</div>
			<div field="firstSettlementMethod" width="100px" align="center"  headerAlign="center" allowSort="true">首次结算方式</div>
			<div field="secondSettlementDate" width="100px" align="center"  headerAlign="center" allowSort="true">到期结算日</div>
			<div field="secondSettlementMethod" width="100px" align="center"  headerAlign="center" allowSort="true">到期结算方式</div>
			<div field="forDate" width="100px" align="center"  headerAlign="center" allowSort="true">成交日期</div>
			<div field="sponsor" width="100px" allowSort="false" headerAlign="center" align="center">审批发起人</div>
			<div field="sponInst" width="120px" allowSort="false" headerAlign="center" align="center">审批发起机构</div>
			<div field="aDate" width="160px" allowSort="false" headerAlign="center" align="center">审批发起时间</div>
		</div>
	</div>
	<fieldset style="width: 100%;border:solid 1px #aaa;margin-top:8px;position:relative;">
		<legend>债券借贷详情</legend>
			<div id="MiniSettleForeigDetail" style="padding:5px;" class="mini-fit area" tyle="width: 100%; height: 50%;" allowAlternating="true" allowResize="true" border="true" sortMode="client">
			<input class="mini-hidden" name="id"/>
				<div class="mini-panel" title="成交双方信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
					<div class="leftarea">
						<fieldset>
							<legend>本方信息</legend>
							<input id="myDir" name="myDir" class="mini-combobox"  labelField="true"  onvaluechanged="dirVerify" required="true"  label="本方方向："  style="width:100%;"  labelStyle="text-align:left;width:120px;" data = "CommonUtil.serverData.dictionary.trading" />
							<input id="borrowInst" name="borrowInst" class="mini-textbox"  labelField="true"  label="机构：" required="true"  style="width:100%;" enabled="false" labelStyle="text-align:left;width:120px;"/>
							<input id="borrowTrader" name="borrowTrader" class="mini-textbox" labelField="true"  label="交易员：" required="true"  vtype="maxLength:5" enabled="false" labelStyle="text-align:left;width:120px;" style="width:100%;"   />
						</fieldset>
					</div>			
					<div class="rightarea">
						<fieldset>
							<legend>对手方信息</legend>
							<input id="oppoDir" name="oppoDir" class="mini-combobox"  enabled="false" labelField="true" required="true"  label="对方方向："  style="width:100%;"  labelStyle="text-align:left;width:120px;" data = "CommonUtil.serverData.dictionary.trading" />
							<input id="lendInst" name="lendInst" class="mini-textbox" required="true"  labelField="true"  label="机构：" vtype="maxLength:50" style="width:100%;"  labelStyle="text-align:left;width:120px;" />
							<input id="lendTrader" name="lendTrader" class="mini-textbox" labelField="true"  label="交易员：" required="true"  vtype="maxLength:10"  labelStyle="text-align:left;width:120px;" style="width:100%;"   />
						</fieldset>
					</div>
				</div>
			<div class="mini-panel" title="交易主体信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
						<div class="leftarea">
							<input style="width:100%;" id="forDate" name="forDate" class="mini-datepicker mini-mustFill" labelField="true" required="true"  label="成交日期：" labelStyle="text-align:left;width:130px;" />
							<input id="underlyingSecurityId" name="underlyingSecurityId" class="mini-textbox mini-mustFill" style="width:100%" required="true"  labelField="true"  label="标的债券代码：" maxLength="20" onbuttonclick="onSecurityQuery"  allowInput="false" labelStyle="text-align:left;width:130px;" />
							<input id="bondPort" name="bondPort" class="mini-textbox mini-mustFill" onbuttonclick="onBondPortEdit"  style="width:100%;"  labelField="true"  label="券的投资组合：" required="true"  allowInput="false" vtype="maxLength:4" labelStyle="text-align:left;width:130px;" >
							<input id="underlyingQty" name="underlyingQty" class="mini-spinner mini-mustFill" style="width:100%"   required="true"   changeOnMousewheel='false' labelField="true"  label="标的债券券面总额(万元)：" maxValue="9999999999999999.9999" format="n4"  labelStyle="text-align:left;width:130px;" required="true"   onvalidation="zeroValidation"/>
							<input id="investType" name="investType" class="mini-combobox mini-mustFill"  labelField="true"   label="投资类型："  style="width:100%;"  labelStyle="text-align:left;width:130px;" data = "CommonUtil.serverData.dictionary.invtype" required="true" />
							<input id="firstSettlementMethod" name="firstSettlementMethod" class="mini-combobox mini-mustFill"  required="true"  style="width:100%" labelField="true"  label="首次结算方式：" vtype="maxLength:50"  labelStyle="text-align:left;width:130px;" data = "CommonUtil.serverData.dictionary.SettlementMethodSl"/>
							<input id="firstSettlementDate" name="firstSettlementDate" class="mini-datepicker mini-mustFill"  required="true"  style="width:100%" labelField="true"  label="首次结算日："  onvaluechanged="linkageCalculatDay" ondrawdate="onDrawDateFirst" labelStyle="text-align:left;width:130px;"   />
							<input id="occupancyDays" name="occupancyDays" class="mini-spinner mini-mustFill" style="width:100%"   required="true"   changeOnMousewheel='false' labelField="true"  label="实际占券天数(天)：" maxValue="99999" onvaluechanged="linkageCalculat"  labelStyle="text-align:left;width:130px;" required="true"   />
							<input id="miscFeeType" name="miscFeeType" class="mini-spinner mini-mustFill" style="width:100%"  required="true"    changeOnMousewheel='false' labelField="true"  label="借贷费用(元)：" maxValue="9999999999999999.9999" format="n4"   labelStyle="text-align:left;width:130px;" onvalidation="zeroValidation"  />
							<input id="tradingModel" name="tradingModel" class="mini-combobox mini-mustFill" required="true"  labelField="true"   label="交易模式："  labelStyle="text-align:left;width:130px;" style="width:100%;"  data="CommonUtil.serverData.dictionary.TradeModel"/>
						</div>
						<div class="rightarea">
							<input id="nettingStatus" name="nettingStatus" class="mini-combobox mini-mustFill" required="true"  labelField="true"   label="净额清算状态：" labelStyle="text-align:left;width:130px;" style="width:100%;" data="CommonUtil.serverData.dictionary.YesNo" showNullItem="true"/>
							<input id="underlyingSymbol" name="underlyingSymbol" class="mini-textbox mini-mustFill" style="width:100%" required="true"   labelField="true"  label="标的债券名称："    labelStyle="text-align:left;width:130px;" enabled="false"   />
							<input style="width:100%;" id="bondCost" name="bondCost" onbuttonclick="onBondCostEdit" allowInput="false" class="mini-textbox mini-mustFill" labelField="true"  label="券的成本中心："   required="true"  vtype="maxLength:15" labelStyle="text-align:left;width:130px;"  >
							<input id="price" name="price" class="mini-spinner mini-mustFill" style="width:100%"   changeOnMousewheel='false' required="true"   labelField="true" label="借贷费率(%)：" minValue="0" maxValue="9999999999999999.99999999" format="n8" changeOnMousewheel="false"  labelStyle="text-align:left;width:130px;" required="true"  />
							<input id="tradingProduct" name="tradingProduct" class="mini-combobox" style="width:100%" labelField="true"  label="交易品种：" vtype="maxLength:10"  labelStyle="text-align:left;width:130px;" data = "CommonUtil.serverData.dictionary.dealVarietySl" />
							<input id="secondSettlementMethod" name="secondSettlementMethod" class="mini-combobox mini-mustFill" required="true"   style="width:100%" labelField="true"  label="到期结算方式：" vtype="maxLength:50"  labelStyle="text-align:left;width:130px;" data = "CommonUtil.serverData.dictionary.SettlementMethodSl"/>
							<input id="secondSettlementDate" name="secondSettlementDate" class="mini-datepicker mini-mustFill" required="true"   style="width:100%" labelField="true"  label="到期结算日：" onvaluechanged="linkageCalculatDay" ondrawdate="onDrawDateSecond" labelStyle="text-align:left;width:130px;"   />
							<input id="tenor" name="tenor" class="mini-spinner mini-mustFill" style="width:100%"  required="true"    changeOnMousewheel='false' labelField="true"  label="借贷期限(天)：" maxValue="99999" labelStyle="text-align:left;width:130px;"   />
							<input id="basis" name="basis" class="mini-combobox mini-mustFill" required="true"  labelField="true"  label="计息基准：" style="width:100%;" data = "CommonUtil.serverData.dictionary.Basis" vtype="maxLength:15" labelStyle="text-align:left;width:130px;" >
							<input id="solution" name="solution" class="mini-combobox mini-mustFill" labelField="true" style="width:100%" required="true"    label="争议解决方式：" labelStyle="text-align:left;width:130px;" data = "CommonUtil.serverData.dictionary.solution" />
						</div>	
					</div>
			<div class="mini-panel" title="质押品明细信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
				<div class="centerarea" style="height:150px;">
					<div class="mini-fit" >
						<div id="datagrid1" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id"  allowAlternating="true"
							fitColumns="false" allowResize="true" sortMode="client" allowAlternating="true" showpager="false">
							<div property="columns">
								<div type="indexcolumn" width="50px" headerAlign="center">序号</div>
					            <div field="marginSecuritiesId" width="200" align="center" headerAlign="center"  >质押债券代码</div>
					            <div field="marginSymbol" width="230px" align="center"  headerAlign="center" >质押债券名称</div> 
					            <div field="marginAmt" width="200" align="right" headerAlign="center"  allowSort="true" numberFormat="n4">质押债券券面总额(万元)</div>
								<div field="invtype" width="150" align="right" headerAlign="center"  allowSort="true"  renderer="CommonUtil.dictRenderer" data-options="{dict:'invtype'}">投资类型</div>
								<div field="costb" width="150" align="right" headerAlign="center"  allowSort="true">成本中心</div>
								<div field="prodb" width="150" align="right" headerAlign="center"  allowSort="true">产品</div>
								<div field="portb" width="150" align="right" headerAlign="center"  allowSort="true">投资组合</div>
								<div field="marginRatio" width="150" align="right" headerAlign="center"  allowSort="true">质押比例(%)</div>
							</div>
						</div>
					</div> 
				</div>
				<div class="leftarea">
					<input id="marginReplacement" name="marginReplacement" class="mini-textbox mini-mustFill" required="true"   labelField="true"  label="质押债券置换安排：" vtype="maxLength:10" style="width:100%;"  labelStyle="text-align:left;width:170px;"   />
				</div>
				<div class="rightarea">
					<input id="marginTotalAmt" name="marginTotalAmt" class="mini-spinner mini-mustFill"  required="true"    changeOnMousewheel='false' labelField="true"  label="质押债券券面总额合计(万元)：" maxValue="999999999999" format="n4"  style="width:100%;"  labelStyle="text-align:left;width:170px;" onvalidation="zeroValidation"  />
				</div>
		</div>
<%--		<div class="mini-panel" title="质押品明细" style="width:100%;"  allowResize="true" collapseOnTitleClick="true">--%>
<%--			<div class="leftarea">--%>
<%--				<input id="marginTotalAmt" name="marginTotalAmt" class="mini-spinner mini-mustFill"  required="true"    changeOnMousewheel='false' labelField="true"  label="质押债券券面总额合计(万元)：" maxValue="999999999999" format="n4"  style="width:100%;"  labelStyle="text-align:left;width:170px;" onvalidation="zeroValidation"  />--%>
<%--			</div>--%>
<%--			<div class="rightarea">--%>
<%--				<input id="marginReplacement" name="marginReplacement" class="mini-textbox mini-mustFill" required="true"   labelField="true"  label="质押债券置换安排：" vtype="maxLength:10" style="width:100%;"  labelStyle="text-align:left;width:170px;"   />--%>
<%--			</div>--%>
<%--		</div>--%>
		<%@ include file="../../Common/opicsLessDetail.jsp"%> 
		<div class="mini-panel" title="清算信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
			<div class="leftarea">
			<fieldset >
				<legend>本方账户</legend>
				<input id="borrowAccname" name="borrowAccname" class="mini-textbox" labelField="true"  label="资金账户户名："  vtype="maxLength:1333"  style="width:100%;"  labelStyle="text-align:left;width:120px;" />
				<input id="borrowOpenBank" name="borrowOpenBank" class="mini-textbox" labelField="true"  label="资金开户行："  vtype="maxLength:1333" labelStyle="text-align:left;width:120px;" style="width:100%;"   />
				<input id="borrowAccnum" name="borrowAccnum" class="mini-textbox" labelField="true"  label="资金账号："  maxLength="400" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])"  labelStyle="text-align:left;width:120px;" style="width:100%;"   />
				<input id="borrowPsnum" name="borrowPsnum" class="mini-textbox" labelField="true"  label="支付系统行号："  maxLength="400" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" labelStyle="text-align:left;width:120px;" style="width:100%;"   />
				<input id="borrowCaname" name="borrowCaname" class="mini-textbox" labelField="true"  label="托管账户户名："  vtype="maxLength:1333"  labelStyle="text-align:left;width:120px;" style="width:100%;"   />
				<input id="borrowCustname" name="borrowCustname" class="mini-textbox mini-mustFill" onbuttonclick="onSaccQuery"  labelField="true"  label="托管机构：" required="true"  labelStyle="text-align:left;width:120px;" style="width:100%;"  allowInput="false"  />
				<input id="borrowCanum" name="borrowCanum" class="mini-textbox" labelField="true"  label="托管帐号："  maxLength="400" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" labelStyle="text-align:left;width:120px;" style="width:100%;"   />
			</fieldset>
			</div>
			<div class="leftarea">
			<fieldset>
				<legend>对手方账户</legend>
				<input id="lendAccname" name="lendAccname" class="mini-textbox" labelField="true"  label="资金账户户名："  vtype="maxLength:1333"  style="width:100%;"  labelStyle="text-align:left;width:120px;" />
				<input id="lendOpenBank" name="lendOpenBank" class="mini-textbox" labelField="true"  label="资金开户行："  vtype="maxLength:1333" labelStyle="text-align:left;width:120px;" style="width:100%;"   />
				<input id="lendAccnum" name="lendAccnum" class="mini-textbox" labelField="true"  label="资金账号："  maxLength="400" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" labelStyle="text-align:left;width:120px;" style="width:100%;"   />
				<input id="lendPsnum" name="lendPsnum" class="mini-textbox" labelField="true"  label="支付系统行号：" maxLength="400" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" labelStyle="text-align:left;width:120px;" style="width:100%;"   />
				<input id="lendCaname" name="lendCaname" class="mini-textbox" labelField="true"  label="托管账户户名：" vtype="maxLength:1333" labelStyle="text-align:left;width:120px;" style="width:100%;"   />
				<input id="lendCustname" name="lendCustname" class="mini-textbox mini-mustFill" onbuttonclick="onSaccQuery" labelField="true"  label="托管机构：" required="true"  labelStyle="text-align:left;width:120px;" style="width:100%;"  allowInput="false" />
				<input id="lendCanum" name="lendCanum" class="mini-textbox" labelField="true"  label="托管帐号：" maxLength="400" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" labelStyle="text-align:left;width:120px;" style="width:100%;"   />
			</fieldset>
			</div>	
		</div>
		
	</div>
	</fieldset>
</div>   

<script>
	mini.parse();

	var url = window.location.search;
	var prdNo = CommonUtil.getParam(url, "prdNo");
	var fPrdCode = CommonUtil.getParam(url, "fPrdCode");
    var prdName = CommonUtil.getParam(url, "prdName");
    var dealType = CommonUtil.getParam(url, "dealType");
	var form = new mini.Form("#search_form");
	var grid=mini.get("datagrid");
	var grid1=mini.get("datagrid1");
	var row="";
	/**************************************点击下面显示详情开始******************************/
	var from = new mini.Form("MiniSettleForeigDetail");
	from.setEnabled(false);
	var grid = mini.get("datagrid");
	//grid.load();
	 //绑定表单
    var db = new mini.DataBinding();
    db.bindForm("MiniSettleForeigDetail", grid);
	$(document).ready(function () {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn){
			initButton();
			search(10,0);
		});
	});
    /**************************************点击下面显示详情结束******************************/
	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});
	grid.on("select",function (e) {
		var row=e.record;
		initGrid(row.ticketId);
	});

	//成交日期
	function onDrawDateStart(e) {
		var startDate = e.date;
		var endDate= mini.get("endDate").getValue();
		if(CommonUtil.isNull(endDate)){
			return;
		}
		if (endDate.getTime() < startDate.getTime()) {
			e.allowSelect = false;
		}
	}
	function onDrawDateEnd(e) {
		var endDate = e.date;
		var startDate = mini.get("startDate").getValue();
		if(CommonUtil.isNull(startDate)){
			return;
		}
		if (endDate.getTime() < startDate.getTime()) {
			e.allowSelect = false;
		}
	}
	//初始化债券信息
	   function initGrid(ticketId){
			var url="/IfsCfetsrmbSlController/searchBondSlDetails";
			var data={};
			data['ticketId']=ticketId;
			var params=mini.encode(data);
			CommonUtil.ajax({
				url:url,
				data:params,
				callback : function(data) {
					grid1.setData(data.obj.rows);
				}
			});
		}
	//查看详情
	function onRowDblClick(e) {
		var url = CommonUtil.baseWebPath() +"/cfetsrmb/bondLendingEdit.jsp?action=detail&prdNo="+prdNo+"&prdName="+prdName+"&dealType="+dealType+"&fPrdCode="+fPrdCode;
		var tab = {id: "SlDetail",name:"SlDetail",url:url,title:"债券借贷成交单详情",
					parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
		var paramData = {selectData:grid.getSelected()};
		CommonUtil.openNewMenuTab(tab,paramData);
	}
	//清空
	function clear(){
		var MiniSettleForeigDetail=new mini.Form("MiniSettleForeigDetail");
		MiniSettleForeigDetail.clear();
		var form=new mini.Form("search_form");
		form.clear();
		searchs();
	}
	//添加
	function add(){
		var url = CommonUtil.baseWebPath() +"/cfetsrmb/bondLendingEdit.jsp?action=add&prdNo="+prdNo+"&prdName="+prdName+"&dealType="+dealType+"&fPrdCode="+fPrdCode;
		var tab = {id: "SlAdd",name:"SlAdd",url:url,title:"债券借贷成交单补录",
					parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
		var paramData = {selectData:""};
		CommonUtil.openNewMenuTab(tab,paramData);
	}
	
	//添加事前审批
	function addsl(){
		var url = CommonUtil.baseWebPath() +"/cfetsrmb/bondLendingApp.jsp?action=add&prdNo="+prdNo+"&prdName="+prdName;
		var tab = {id: "SlAddsl",name:"SlAddsl",url:url,title:"债券借贷事前单详情",
					parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
		var paramData = {selectData:""};
		CommonUtil.openNewMenuTab(tab,paramData);
	}
	
	
	//修改
	function edit(){
		var row=grid.getSelected();
		if(row){
		var url = CommonUtil.baseWebPath() +"/cfetsrmb/bondLendingEdit.jsp?action=edit&prdNo="+prdNo+"&prdName="+prdName+"&dealType="+dealType+"&fPrdCode="+fPrdCode;
		var tab = {id: "SlEdit",name:"SlEdit",url:url,title:"债券借贷成交单修改",
					parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
		var paramData = {selectData:row};
		CommonUtil.openNewMenuTab(tab,paramData);
	}else{
		mini.alert("请选择一条记录","提示");
	}
	}
	//删除
	function del(){
		var rows=grid.getSelecteds();
		if(rows.length==0){
			mini.alert("请选中一行","提示");
			return;
		}
		if(rows.length>1){
			mini.alert("系统不支持多条数据进行删除","消息提示");
			return;
		}
		mini.confirm("您确认要删除选中记录?","系统警告",function(value){   
			if (value=='ok'){   
				var data=rows[0];
				params=mini.encode(data);
				CommonUtil.ajax( {
					url:"/IfsCfetsrmbSlController/deleteCfetsrmbSl",
					data:params,
					callback : function(data) {
						mini.alert("删除成功.","系统提示");
						search(10,0);
					}
				});
			}
		});
	}
	
	
	grid.on("select",function(e){
		var row=e.record;
		mini.get("approve_mine_commit_btn").setEnabled(false);
		mini.get("approve_commit_btn").setEnabled(false);
		mini.get("edit_btn").setEnabled(false);
		mini.get("delete_btn").setEnabled(false);
		mini.get("approve_log").setEnabled(true);
		mini.get("recall_btn").setEnabled(true);
		mini.get("batch_commit_btn").setEnabled(true);
		if(row.approveStatus == "3"){//新建
			mini.get("edit_btn").setEnabled(true);
			mini.get("delete_btn").setEnabled(true);
			mini.get("approve_mine_commit_btn").setEnabled(true);
			mini.get("approve_commit_btn").setEnabled(false);
			mini.get("reback_btn").setEnabled(false);
			mini.get("recall_btn").setEnabled(false);
		}
		if( row.approveStatus == "6" || row.approveStatus == "7" || row.approveStatus == "8" || row.approveStatus == "17"){//审批通过：6    审批拒绝:5
			mini.get("edit_btn").setEnabled(false);
			mini.get("delete_btn").setEnabled(false);
			mini.get("approve_mine_commit_btn").setEnabled(false);
			mini.get("approve_commit_btn").setEnabled(true);
			mini.get("batch_approve_btn").setEnabled(false);
			mini.get("batch_commit_btn").setEnabled(false);
			mini.get("opics_check_btn").setEnabled(false);
			mini.get("reback_btn").setEnabled(false);
			mini.get("recall_btn").setEnabled(false);
		}
		if(row.approveStatus == "5"){//审批中:5
			mini.get("edit_btn").setEnabled(false);
			mini.get("delete_btn").setEnabled(false);
			mini.get("approve_mine_commit_btn").setEnabled(false);
			mini.get("approve_commit_btn").setEnabled(true);
			mini.get("batch_commit_btn").setEnabled(false);
			mini.get("reback_btn").setEnabled(true);
			mini.get("recall_btn").setEnabled(true);
		}
		
	});

	//查询 按钮
	function searchs(){
		search(grid.pageSize,0);
	}

	function search(pageSize,pageIndex){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统也提示");
			return;
		}

		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		data['fPrdCode']=fPrdCode;
		var url=null;

		var approveType = mini.get("approveType").getValue();
		if(approveType == "mine"){
			url = "/IfsCfetsrmbSlController/searchRmbSlSwapMine";
		}else if(approveType == "approve"){
			url = "/IfsCfetsrmbSlController/searchPageRmbSlUnfinished";
		}else{
			url = "/IfsCfetsrmbSlController/searchPageRmbSlFinished";
		}
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:url,
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	/**************************审批相关****************************/
	//审批日志查看
	function appLog(selections){
		var flow_type = Approve.FlowType.VerifyApproveFlow;
		if(selections.length <= 0){
			mini.alert("请选择要操作的数据","系统提示");
			return;
		}
		if(selections.length > 1){
			mini.alert("系统不支持多笔操作","系统提示");
			return;
		}
		if(selections[0].tradeSource == "3"){
			mini.alert("初始化导入的业务没有审批日志","系统提示");
			return false;
		}
		Approve.approveLog(flow_type,selections[0].ticketId);
	};
	//提交正式审批、待审批
	function verify(selections){
		if(selections.length == 0){
			mini.alert("请选中一条记录！","消息提示");
			return false;
		}else if(selections.length > 1){
			mini.alert("该功能不支持多笔提交，如需多笔提交请点击[批量提交]按钮","系统提示");
			return false;
		}
		if(CommonUtil.isNull(selections[0]["dealSource"])||CommonUtil.isNull(selections[0]["port"])||CommonUtil.isNull(selections[0]["cost"])||
                CommonUtil.isNull(selections[0]["product"])||CommonUtil.isNull(selections[0]["prodType"])){
            mini.alert("请先进行opics要素检查更新!","提示");
            return;
        }
		if(selections[0]["approveStatus"] != "3" ){
			var url = CommonUtil.baseWebPath() +"/cfetsrmb/bondLendingEdit.jsp?action=approve&ticketId="+row.ticketId+"&fPrdCode="+fPrdCode;
			var tab = {"id": "bondLendingApprove",name:"bondLendingApprove",url:url,title:"债券借贷审批",
						parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
			var paramData = {selectData:selections[0]};
			CommonUtil.openNewMenuTab(tab,paramData);
			// top["win"].showTab(tab);
		}else{
			Approve.approveCommit(Approve.FlowType.VerifyApproveFlow,selections[0]["ticketId"],Approve.OrderStatus.New,"IfsRmbSlService",prdNo,function(){
				search(grid.pageSize,grid.pageIndex);
			});
		}
	};
	//审批
	function approve(){
		mini.get("approve_commit_btn").setEnabled(false);
		var messageid = mini.loading("系统正在处理...", "请稍后");
		try {
			verify(grid.getSelecteds());
		} catch (error) {
			
		}
		mini.hideMessageBox(messageid);	
		mini.get("approve_commit_btn").setEnabled(true);
	}
	//提交审批
	function commit(){
		mini.get("approve_mine_commit_btn").setEnabled(false);
		var messageid = mini.loading("系统正在处理...", "请稍后");
		try {
			verify(grid.getSelecteds());
		} catch (error) {
			
		}
		mini.hideMessageBox(messageid);	
		mini.get("approve_mine_commit_btn").setEnabled(true);
	}
	//审批日志
	function searchlog(){
		appLog(grid.getSelecteds());
	}

	//打印
	function print(){
		var selections = grid.getSelecteds();
		if(selections == null || selections.length == 0){
			mini.alert('请选择一条要打印的数据！','系统提示');
			return false;
		}else if(selections.length>1){
			mini.alert('暂不支持多条数据同时打印，请选择一条要打印的数据！','系统提示');
			return false;
		}
		var canPrint = selections[0].ticketId;

		if(!CommonUtil.isNull(canPrint)){
			var actionStr = CommonUtil.pPath + "/sl/IfsPrintUtilController/exportload/print/"+PrintNo.bondLending+"/" + canPrint;
			$('<form target="_blank" action=' + actionStr + ' method="post"></form>').appendTo('body').submit().remove();
		};
	}

	//导出报表
	function exportExcel(){
		exportExcelManin("jyzqjd.xls","jyzqjd");
	}
	
</script>
</body>
</html>
