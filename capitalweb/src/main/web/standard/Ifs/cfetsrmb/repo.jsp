<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
<title>买断式回购</title>
<script type="text/javascript" >
	/**prdCode用于页面投资组合的选择   该全局变量必须为prdCode*/
	var prdCode="SECLEN-PO";
</script>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
	<legend>买断式回购成交单查询</legend>	
	<div>
		<div id="search_form" style="width:100%" cols="6">
			<input id="startDate" name="startDate" class="mini-datepicker" labelField="true"  label="成交日期："
				   ondrawdate="onDrawDateStart" format="yyyy-MM-dd" value="<%=__bizDate%>"  labelStyle="text-align:right;" emptyText="开始日期" />
			<span>~</span>
			<input id="endDate" name="endDate" class="mini-datepicker"
				   ondrawdate="onDrawDateEnd" emptyText="结束日期" format="yyyy-MM-dd" value="<%=__bizDate%>"/>
			<input id="contractId" name="contractId" class="mini-textbox" labelField="true"  label="成交单编号：" width="280px" labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请输入成交单编号" />
			<input id="ticketId" name="ticketId" class="mini-textbox" labelField="true" label="审批单号：
" labelStyle="text-align:right;" emptyText="请填写审批单号" width="280px" />
			<input id="dealNo" name="dealNo" class="mini-textbox" labelField="true"
				   label="opics交易号：" emptyText="请填写opics交易号" labelStyle="text-align:right;" width="280px"/>
			<input id="approveStatus" name="approveStatus" class="mini-combobox" data="CommonUtil.serverData.dictionary.ApproveSta" width="280px" emptyText="请选择审批状态" labelField="true"  label="审批状态：" labelStyle="text-align:right;"/>
			<span style="float: right; margin-right: 150px"> 
				<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
			</span>
		</div>
	</div>
</fieldset> 
<%@ include file="../batchReback.jsp"%>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script> 
<div class="mini-fit" >
	<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:70%;" idField="id"  allowAlternating="true" allowResize="true" onrowdblclick="onRowDblClick" sortMode="client" allowAlternating="true" multiSelect="true">
		<div property="columns">
			<div type="checkcolumn"></div> 
			<div type="indexcolumn" width="50px" headerAlign="center">序号</div>
			<div field="prdNo" width="100px" allowSort="false" headerAlign="center" align="center" visible="false"></div>
			<div field="taskName" width="120px" allowSort="false" headerAlign="center" align="center">审批状态</div>
			<div field="contractId" width="160px" align="center"  headerAlign="center" >成交单编号</div>
			<div field="ticketId" width="160px" align="center"  headerAlign="center" >审批单编号</div> 
			<div field="dealNo" width="100px" align="center"  headerAlign="center" >opics交易号</div>  
			<div field="statcode" width="160px" align="center"  headerAlign="center">opics处理状态</div>
			<div field="reverseInst" width="200px" align="center"  headerAlign="center" >对手方机构名称</div> 
			<div field="myDir" width="100px" align="center" headerAlign="center"  renderer="CommonUtil.dictRenderer" data-options="{dict:'repoing'}">本方方向</div>
			<div field="totalFaceValue" width="140px" align="right"  headerAlign="center" numberFormat="n4">交易金额（万元）</div>
			<div field="repoRate" width="140px" align="right"  headerAlign="center" numberFormat="n8">回购利率(%)</div>
			<div field="forDate" width="100px" align="center"  headerAlign="center" allowSort="true">起息日</div>
			<div field="secondSettlementDate" width="100px" align="center"  headerAlign="center" allowSort="true">到期结算日</div>
			<div field="tenor" width="100px" align="center"  headerAlign="center" allowSort="true">期限</div>
			<div field="occupancyDays" width="100px" align="center"  headerAlign="center" allowSort="true">实际占款天数</div>
			<div field="basis" width="100px" align="center"  headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'Basis'}" allowSort="true">计息基准</div>
			<div field="secondSettlementAmount" width="140px" align="right"  headerAlign="center" allowSort="true" numberFormat="n4">到期结算金额（元）</div>
			<div field="sponsor" width="120px" allowSort="true" headerAlign="center"  align="center" >审批发起人</div>
			<div field="sponInst" width="120px" allowSort="true" align="center"  >审批发起机构</div>
			<div field="aDate" width="160px" allowSort="true" align="center">审批发起时间</div>
		</div>
	</div>
	
	<fieldset style="width: 100%;border:solid 1px #aaa;margin-top:8px;position:relative;">
		<legend>买断式回购详情</legend>
			<div id="MiniSettleForeigDetail" style="padding:5px;" class="mini-fit area" tyle="width: 100%; height: 50%;" allowAlternating="true" allowResize="true" border="true" sortMode="client">
			<input class="mini-hidden" name="id"/>
				<div class="mini-panel" title="成交双方信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
					<div class="leftarea">
						<fieldset>
							<legend>本方信息</legend>
							<input id="myDir" name="myDir" class="mini-combobox"  labelField="true"  label="本方方向：" required="true"   onvaluechanged="dirVerify" style="width:100%;"  labelStyle="text-align:left;width:120px;" data = "CommonUtil.serverData.dictionary.repoing" />
							<input id="positiveInst" name="positiveInst" class="mini-textbox"  labelField="true"  label="机构：" required="true"   style="width:100%;" enabled="false" labelStyle="text-align:left;width:120px;"/>
							<input id="positiveTrader" name="positiveTrader" class="mini-textbox" labelField="true"  label="交易员："  required="true"  vtype="maxLength:5" enabled="false" labelStyle="text-align:left;width:120px;" style="width:100%;"   />
						</fieldset>
					</div>			
					<div class="leftarea">
						<fieldset>
							<legend>对手方信息</legend>
							<input id="oppoDir" name="oppoDir" class="mini-combobox"  labelField="true"  label="对方方向："  required="true"  enabled="false" style="width:100%;"  labelStyle="text-align:left;width:120px;" data = "CommonUtil.serverData.dictionary.repoing" />
							<input id="reverseInst" name="reverseInst"  class="mini-textbox"  labelField="true"  label="机构：" required="true"  style="width:100%;"  labelStyle="text-align:left;width:120px;" />
							<input id="reverseTrader" name="reverseTrader" class="mini-textbox" labelField="true"  label="交易员："  vtype="maxLength:5" labelStyle="text-align:left;width:120px;" style="width:100%;"   />
						</fieldset>
					</div>	
				</div>
			<div class="mini-panel" title="交易主体信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
				<div class="leftarea">
					<input style="width:100%;" id="forDate" name="forDate" class="mini-datepicker mini-mustFill" labelField="true" required="true"  label="成交日期：" labelStyle="text-align:left;width:130px;" />
					<input style="width:100%" id="bondCode" name="bondCode" class="mini-textbox mini-mustFill" onbuttonclick="onBondQuery" labelField="true"  label="债券代码：" required="true"     labelStyle="text-align:left;width:130px;" />
					<input  style="width:100%;" id="bondProduct" name="bondProduct" onbuttonclick="onBondPrdEdit" allowInput="false" class="mini-textbox mini-mustFill" labelField="true"   label="券的产品代码：" required="true"  maxLength="6"  labelStyle="text-align:left;width:130px;" >
					<input id="bondPort" name="bondPort" class="mini-textbox mini-mustFill" onbuttonclick="onBondPortEdit"  style="width:100%;"  labelField="true"  label="券的投资组合：" required="true"  allowInput="false" vtype="maxLength:4" labelStyle="text-align:left;width:130px;" >
					<input id="invtype" name="invtype" class="mini-combobox mini-mustFill"  labelField="true"   label="券的投资类型："  style="width:100%;"  labelStyle="text-align:left;width:130px;" data = "CommonUtil.serverData.dictionary.invtype" />
					<input style="width:100%" id="totalFaceValue" name="totalFaceValue" class="mini-spinner mini-mustFill" labelField="true"   label="券面总额(万元)：" required="true"  onvaluechanged="onFirstPriceChanged"  minValue="0" maxValue="9999999999999999.9999" format="n4" changeOnMousewheel="false" labelStyle="text-align:left;width:130px;" onvalidation="zeroValidation"  />
					<input style="width:100%" id="firstSettlementDate" name="firstSettlementDate" class="mini-datepicker mini-mustFill" labelField="true"   label="首次结算日："  required="true"  ondrawdate="onDrawDateStart" onvaluechanged="onStartDateChanged" labelStyle="text-align:left;width:130px;"   />
					<input style="width:100%" id="tenor" name="tenor" class="mini-spinner mini-mustFill" labelField="true"   label="回购期限(天)：" required="true"  minValue="0" maxValue="9999999999"  changeOnMousewheel="false"  labelStyle="text-align:left;width:130px;"   />
					<input style="width:100%" id="firstYield" name="firstYield" class="mini-spinner mini-mustFill" labelField="true"   label="首次收益率(%)：" required="true"   onvaluechanged="onFirstYieldChanged" minValue="0" maxValue="9999999999999999.99999999" format="n8" changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"   />
					<input style="width:100%" id="firstCleanPrice" name="firstCleanPrice" class="mini-spinner mini-mustFill" labelField="true"  label="首次净价(元)：" required="true"  onvaluechanged="onFirstPriceChanged" minValue="0" maxValue="9999999999999999.9999" format="n4" changeOnMousewheel="false"   labelStyle="text-align:left;width:130px;"   />
					<input style="width:100%" id="firstAccuredInterest" name="firstAccuredInterest" class="mini-spinner mini-mustFill" labelField="true"   label="首次应计利息(元)：" required="true"  onvaluechanged="onFirstPriceChanged"  minValue="0" maxValue="9999999999999999.9999" format="n5" changeOnMousewheel="false"  labelStyle="text-align:left;width:130px;"   />
					<input style="width:100%" id="firstDirtyPrice" name="firstDirtyPrice" class="mini-spinner mini-mustFill" labelField="true"  label="首次全价(元)：" required="true"   onvaluechanged="onFirstPriceChanged" minValue="0" maxValue="9999999999999999.9999" format="n4" changeOnMousewheel="false"    labelStyle="text-align:left;width:130px;"   />
					<input style="width:100%" id="firstSettlementMethod" name="firstSettlementMethod" class="mini-combobox mini-mustFill" labelField="true"   label="首次结算方式：" required="true"  data = "CommonUtil.serverData.dictionary.SettlementMethod" labelStyle="text-align:left;width:130px;"   />
					<input style="width:100%" id="firstSettlementAmount" name="firstSettlementAmount" class="mini-spinner mini-mustFill"   labelField="true"  label="首次结算金额(元)："  required="true"  minValue="0" maxValue="9999999999999999.9999" format="n4" changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"  onvalidation="zeroValidation" onvaluechanged="sumSecondSettlementAmount"/>
					<input id="basis" name="basis" class="mini-combobox mini-mustFill" required="true"  labelField="true"  label="计息基准：" style="width:100%;" data = "CommonUtil.serverData.dictionary.Basis" vtype="maxLength:15" labelStyle="text-align:left;width:130px;"  onvaluechanged="onFirstPriceChanged">
					<input id="note" name="note" class="mini-textbox" labelField="true" label="交易事由：" labelStyle="text-align:left;width:130px;" style="width:100%;" vtype="maxLength:16"/>
				</div>
				<div class="rightarea">
					<input id="nettingStatus" name="nettingStatus" class="mini-combobox mini-mustFill" required="true"  labelField="true"   label="净额清算状态：" labelStyle="text-align:left;width:130px;" style="width:100%;" data="CommonUtil.serverData.dictionary.YesNo" showNullItem="true"/>
					<input style="width:100%" id="bondName" name="bondName" class="mini-textbox mini-mustFill" labelField="true"  label="债券名称：" required="true"    labelStyle="text-align:left;width:130px;"   enabled="false"/>
					<input style="width:100%;" id="bondProdType" name="bondProdType" onbuttonclick="onBondTypeEdit" allowInput="false" class="mini-textbox mini-mustFill" labelField="true"  label="券的产品类型：" required="true"  vtype="maxLength:2" labelStyle="text-align:left;width:130px;" >
					<input style="width:100%;" id="bondCost" name="bondCost" onbuttonclick="onBondCostEdit" allowInput="false" class="mini-textbox mini-mustFill" labelField="true"  label="券的成本中心："   required="true"  vtype="maxLength:15" labelStyle="text-align:left;width:130px;"  >
					<input style="width:100%" id="tradingProduct" name="tradingProduct" class="mini-combobox" allowInput="false" labelField="true"  label="交易品种：" labelStyle="text-align:left;width:130px;" data = "CommonUtil.serverData.dictionary.dealVarietyBuy"/>
					<input style="width:100%" id="repoRate" name="repoRate" class="mini-spinner mini-mustFill" labelField="true"  label="回购利率(%)：" required="true"  minValue="0" maxValue="9999999999999999.99999999" format="n8" changeOnMousewheel="false"    labelStyle="text-align:left;width:130px;" onvaluechanged="onFirstPriceChanged" />
					<input style="width:100%;display:none" id="startDate" name="startDate" class="mini-textbox mini-mustFill" labelField="true"  label="起息日：" required="true"  vtype="maxLength:10"  labelStyle="text-align:left;width:130px;"   enabled="false"/>
					<input style="width:100%;display:none" id="matdt" name="matdt" class="mini-textbox mini-mustFill" labelField="true"  label="到期日：" required="true"  vtype="maxLength:10"  labelStyle="text-align:left;width:130px;"   enabled="false"/>
					<input style="width:100%" id="secondSettlementDate" name="secondSettlementDate" class="mini-datepicker mini-mustFill"   labelField="true"  label="到期结算日：" required="true"   ondrawdate="onDrawDateEnd" onvaluechanged="onEndDateChanged" labelStyle="text-align:left;width:130px;"   />
					<input style="width:100%" id="occupancyDays" name="occupancyDays" class="mini-spinner mini-mustFill" labelField="true"   label="实际占款天数(天)：" required="true"  minValue="0" maxValue="99999"  changeOnMousewheel="false"  labelStyle="text-align:left;width:130px;"   onvaluechanged="onFirstPriceChanged"/>
					<input style="width:100%" id="secondYield" name="secondYield" class="mini-spinner mini-mustFill" labelField="true"  label="到期收益率(%)：" required="true"  minValue="0" maxValue="9999999999999999.99999999" format="n8" changeOnMousewheel="false"    labelStyle="text-align:left;width:130px;"   />
					<input style="width:100%" id="secondCleanPrice" name="secondCleanPrice" class="mini-spinner mini-mustFill" labelField="true"   label="到期净价(元)：" required="true"  onvaluechanged="onSecondInterestChanged"  minValue="0" maxValue="9999999999999999.9999" format="n4" changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"   />
					<input style="width:100%" id="secondAccuredInterest" name="secondAccuredInterest" class="mini-spinner mini-mustFill"  labelField="true"  label="到期应计利息(元)：" required="true"  onvaluechanged="onSecondInterestChanged" minValue="0" maxValue="9999999999999999.9999" format="n5" changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"   />
					<input style="width:100%" id="secondDirtyPrice" name="secondDirtyPrice" class="mini-spinner mini-mustFill" labelField="true"   label="到期全价(元)：" required="true"  onvaluechanged="onFirstPriceChanged" minValue="0" maxValue="9999999999999999.9999" format="n4" changeOnMousewheel="false"  labelStyle="text-align:left;width:130px;"   />
					<input style="width:100%" id="secondSettlementMethod" name="secondSettlementMethod" class="mini-combobox mini-mustFill"  labelField="true"  label="到期结算方式："  required="true"  data = "CommonUtil.serverData.dictionary.SettlementMethod" labelStyle="text-align:left;width:130px;"   />
					<input style="width:100%" id="secondSettlementAmount" name="secondSettlementAmount" class="mini-spinner mini-mustFill" labelField="true"   label="到期结算金额(元)：" required="true"  minValue="0" maxValue="9999999999999999.9999" format="n4" changeOnMousewheel="false"  labelStyle="text-align:left;width:130px;"  onvalidation="zeroValidation" />
					<input id="tradingModel" name="tradingModel" class="mini-combobox mini-mustFill" required="true"  labelField="true"   label="交易模式："  labelStyle="text-align:left;width:130px;" style="width:100%;"  data="CommonUtil.serverData.dictionary.TradeModel"/>
				</div>
			</div>
			<%@ include file="../../Common/opicsRepoDetail.jsp"%>
			<%@ include file="../../Common/RiskCenterDetail.jsp"%>	
			<div class="mini-panel" title="清算信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
				<div class="leftarea">
					<fieldset>
						<legend>本方账户</legend>
						<input id="positiveAccname" name="positiveAccname" class="mini-textbox" labelField="true"  label="资金账户户名：" vtype="maxLength:1333"  style="width:100%;"  labelStyle="text-align:left;width:120px;" />
						<input id="positiveOpenBank" name="positiveOpenBank" class="mini-textbox" labelField="true"  label="资金开户行：" vtype="maxLength:1333"  labelStyle="text-align:left;width:120px;" style="width:100%;"   />
						<input id="positiveAccnum" name="positiveAccnum" class="mini-textbox" labelField="true"  label="资金账号：" onvalidation="onEnglishAndNumberValidation" vtype="maxLength:400" labelStyle="text-align:left;width:120px;" style="width:100%;"   />
						<input id="positivePsnum" name="positivePsnum" class="mini-textbox" labelField="true"  label="支付系统行号："  onvalidation="onEnglishAndNumberValidation" vtype="maxLength:400" labelStyle="text-align:left;width:120px;" style="width:100%;"   />
						<input id="positiveCaname" name="positiveCaname" class="mini-textbox" labelField="true"  label="托管账户户名：" vtype="maxLength:1333"  labelStyle="text-align:left;width:120px;" style="width:100%;"   />
						<input id="positiveCustname" name="positiveCustname" class="mini-textbox mini-mustFill" onbuttonclick="onSaccQuery" required="true"  labelField="true" allowInput="false"  label="托管机构：" vtype="maxLength:1333"  labelStyle="text-align:left;width:120px;" style="width:100%;"/>
						<input id="positiveCanum" name="positiveCanum" class="mini-textbox" labelField="true"  label="托管帐号："  onvalidation="onEnglishAndNumberValidation" vtype="maxLength:400"labelStyle="text-align:left;width:120px;" style="width:100%;"   />
					</fieldset>
				</div>
				<div class="rightarea">
					<fieldset>
						<legend>对手方账户</legend>
						<input id="reverseAccname" name="reverseAccname" class="mini-textbox" labelField="true"  label="资金账户户名：" vtype="maxLength:1333"  style="width:100%;"  labelStyle="text-align:left;width:120px;" />
						<input id="reverseOpenBank" name="reverseOpenBank" class="mini-textbox" labelField="true"  label="资金开户行：" vtype="maxLength:1333"  labelStyle="text-align:left;width:120px;" style="width:100%;"   />
						<input id="reverseAccnum" name="reverseAccnum" class="mini-textbox" labelField="true"  label="资金账号：" onvalidation="onEnglishAndNumberValidation" vtype="maxLength:400" labelStyle="text-align:left;width:120px;" style="width:100%;"   />
						<input id="reversePsnum" name="reversePsnum" class="mini-textbox" labelField="true"  label="支付系统行号："  onvalidation="onEnglishAndNumberValidation" vtype="maxLength:400" labelStyle="text-align:left;width:120px;" style="width:100%;"   />
						<input id="reverseCaname" name="reverseCaname" class="mini-textbox" labelField="true"  label="托管账户户名："  vtype="maxLength:1333"  labelStyle="text-align:left;width:120px;" style="width:100%;"   />
						<input id="reverseCustname" name="reverseCustname" class="mini-textbox mini-mustFill" onbuttonclick="onSaccQuery" required="true"  labelField="true" allowInput="false"  label="托管机构："  vtype="maxLength:1333" labelStyle="text-align:left;width:120px;" style="width:100%;"/>
						<input id="reverseCanum" name="reverseCanum" class="mini-textbox" labelField="true"  label="托管帐号："  onvalidation="onEnglishAndNumberValidation" vtype="maxLength:400" labelStyle="text-align:left;width:120px;" style="width:100%;"   />
					</fieldset>
				</div>	
			</div>		
			</div>
	</fieldset>
</div>   

<script>
	mini.parse();

	var url = window.location.search;
	var prdNo = CommonUtil.getParam(url, "prdNo");
	var fPrdCode = CommonUtil.getParam(url, "fPrdCode");
    var prdName = CommonUtil.getParam(url, "prdName");
    var dealType = CommonUtil.getParam(url, "dealType");
	var form = new mini.Form("#search_form");
	var grid=mini.get("datagrid");
	var row="";
	/**************************************点击下面显示详情开始******************************/
	var from = new mini.Form("MiniSettleForeigDetail");
	from.setEnabled(false);
	var grid = mini.get("datagrid");
	//grid.load();
	 //绑定表单
    var db = new mini.DataBinding();
    db.bindForm("MiniSettleForeigDetail", grid);
	$(document).ready(function () {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn){
			initButton();
			search(10,0);
		});
	});
    /**************************************点击下面显示详情结束******************************/
	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});
	/* function searchs(pageSize,pageIndex){
		form.validate();
		if (form.isValid()==false) return;
		//提交的数据
		var data=form.getData(true);
		
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		var params=mini.encode(data);
		//发送ajax请求
		CommonUtil.ajax({
			url:'/IfsCfetsrmbOrController/searchCfetsrmbOr',
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
				grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	} */
	//成交日期
	function onDrawDateStart(e) {
		var startDate = e.date;
		var endDate= mini.get("endDate").getValue();
		if(CommonUtil.isNull(endDate)){
			return;
		}
		if (endDate.getTime() < startDate.getTime()) {
			e.allowSelect = false;
		}
	}
	function onDrawDateEnd(e) {
		var endDate = e.date;
		var startDate = mini.get("startDate").getValue();
		if(CommonUtil.isNull(startDate)){
			return;
		}
		if (endDate.getTime() < startDate.getTime()) {
			e.allowSelect = false;
		}
	}
	//查看详情
	function onRowDblClick(e) {
		var url = CommonUtil.baseWebPath() +"/cfetsrmb/repoEdit.jsp?action=detail&prdNo="+prdNo+"&prdName="+prdName+"&dealType="+dealType;
		var tab = {id: "OrDetail",name:"OrDetail",url:url,title:"买断式回购成交单详情",
					parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
		var paramData = {selectData:grid.getSelected()};
		CommonUtil.openNewMenuTab(tab,paramData);
	}
	//清空
	function clear(){
		var MiniSettleForeigDetail=new mini.Form("MiniSettleForeigDetail");
		MiniSettleForeigDetail.clear();
		form.clear();
		query();
	}
	//添加
	function add(){
		var url = CommonUtil.baseWebPath() +"/cfetsrmb/repoEdit.jsp?action=add?t="+new Date()+"&prdNo="+prdNo+"&prdName="+prdName+"&dealType="+dealType+"&fPrdCode="+fPrdCode;
		var tab = {id: "OrAdd",name:"OrAdd",url:url,title:"买断式回购成交单补录",
					parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
		var paramData = {selectData:""};
		CommonUtil.openNewMenuTab(tab,paramData);
	}
	//修改
	function edit(){
		var row=grid.getSelected();
		if(row){
		var url = CommonUtil.baseWebPath() +"/cfetsrmb/repoEdit.jsp?action=edit&prdNo="+prdNo+"&prdName="+prdName+"&dealType="+dealType+"&fPrdCode="+fPrdCode;
		var tab = {id: "OrEdit",name:"OrEdit",url:url,title:"买断式回购成交单修改",
					parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
		var paramData = {selectData:row};
		CommonUtil.openNewMenuTab(tab,paramData);
		}else{
			mini.alert("请选择一条记录","提示");
		}
	}
	
	function onRowDblClick(e) {
		var grid = mini.get("datagrid");
		var row = grid.getSelected();
		if(row){
				var url = CommonUtil.baseWebPath() + "/cfetsrmb/repoEdit.jsp?action=detail&ticketid="+row.ticketId+"&prdNo="+prdNo+"&fPrdCode="+fPrdCode;
				var tab = { id: "MiniRmbfwdDetail", name: "MiniRmbfwdDetail", title: "买断式回购详情", url: url ,showCloseButton:true};
				var paramData = {selectData:row};
				CommonUtil.openNewMenuTab(tab,paramData);

		} else {
			mini.alert("请选中一条记录！","消息提示");
		}
	}
	
	//删除
	function del(){
		var rows=grid.getSelecteds();
		if(rows.length==0){
			mini.alert("请选中一行","提示");
			return;
		}
		if(rows.length>1){
			mini.alert("系统不支持多条数据进行删除","消息提示");
			return;
		}
		mini.confirm("您确认要删除选中记录?","系统警告",function(value){   
			if (value=='ok'){   
				var data=rows[0];
				params=mini.encode(data);
				CommonUtil.ajax( {
					url:"/IfsCfetsrmbOrController/deleteCfetsrmbOr",
					data:params,
					callback : function(data) {
						mini.alert("删除成功.","系统提示");
						search(10,0);
					}
				});
			}
		});
	}
	
	
	grid.on("select",function(e){
		var row=e.record;
		mini.get("approve_mine_commit_btn").setEnabled(false);
		mini.get("approve_commit_btn").setEnabled(false);
		mini.get("edit_btn").setEnabled(false);
		mini.get("delete_btn").setEnabled(false);
		mini.get("approve_log").setEnabled(true);
		mini.get("recall_btn").setEnabled(true);
		mini.get("batch_commit_btn").setEnabled(true);
		if(row.approveStatus == "3"){//新建
			mini.get("edit_btn").setEnabled(true);
			mini.get("delete_btn").setEnabled(true);
			mini.get("approve_mine_commit_btn").setEnabled(true);
			mini.get("approve_commit_btn").setEnabled(false);
			mini.get("reback_btn").setEnabled(false);
			mini.get("recall_btn").setEnabled(false);
		}
		if( row.approveStatus == "6" || row.approveStatus == "7" || row.approveStatus == "8" || row.approveStatus == "17"){//审批通过：6    审批拒绝:5
			mini.get("edit_btn").setEnabled(false);
			mini.get("delete_btn").setEnabled(false);
			mini.get("approve_mine_commit_btn").setEnabled(false);
			mini.get("approve_commit_btn").setEnabled(true);
			mini.get("batch_approve_btn").setEnabled(false);
			mini.get("batch_commit_btn").setEnabled(false);
			mini.get("opics_check_btn").setEnabled(false);
			mini.get("reback_btn").setEnabled(false);
			mini.get("recall_btn").setEnabled(false);
		}
		if(row.approveStatus == "5"){//审批中:5
			mini.get("edit_btn").setEnabled(false);
			mini.get("delete_btn").setEnabled(false);
			mini.get("approve_mine_commit_btn").setEnabled(false);
			mini.get("approve_commit_btn").setEnabled(true);
			mini.get("batch_commit_btn").setEnabled(false);
			mini.get("reback_btn").setEnabled(true);
			mini.get("recall_btn").setEnabled(true);
		}
		
	});
	/* 查询 按钮事件 */
	function query(){
		search(grid.pageSize,0);
	}
	/* 查询 */
	function search(pageSize,pageIndex){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统也提示");
			return;
		}

		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		data['fPrdCode']=fPrdCode;
		var url=null;

		var approveType = mini.get("approveType").getValue();
		if(approveType == "mine"){//我发起的
			url = "/IfsCfetsrmbOrController/searchRmbOrSwapMine";
		}else if(approveType == "approve"){//待审批
			url = "/IfsCfetsrmbOrController/searchPageRmbOrUnfinished";
		}else{//已审批
			url = "/IfsCfetsrmbOrController/searchPageRmbOrFinished";
		}
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:url,
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	/**************************审批相关****************************/
	//审批日志查看
	function appLog(selections){
		var flow_type = Approve.FlowType.VerifyApproveFlow;
		if(selections.length <= 0){
			mini.alert("请选择要操作的数据","系统提示");
			return;
		}
		if(selections.length > 1){
			mini.alert("系统不支持多笔操作","系统提示");
			return;
		}
		if(selections[0].tradeSource == "3"){
			mini.alert("初始化导入的业务没有审批日志","系统提示");
			return false;
		}
		Approve.approveLog(flow_type,selections[0].ticketId);
	};
	//提交正式审批、待审批
	function verify(selections){
		if(selections.length == 0){
			mini.alert("请选中一条记录！","消息提示");
			return false;
		}else if(selections.length > 1){
			mini.alert("该功能不支持多笔提交，如需多笔提交请点击[批量提交]按钮","系统提示");
			return false;
		}
        if(CommonUtil.isNull(selections[0]["dealSource"])||CommonUtil.isNull(selections[0]["cost"])||
                CommonUtil.isNull(selections[0]["product"])||CommonUtil.isNull(selections[0]["prodType"])){
            mini.alert("请先进行opics要素检查更新!","提示");
            return;
        }
		if(selections[0]["approveStatus"] != "3" ){
			var url = CommonUtil.baseWebPath() +"/cfetsrmb/repoEdit.jsp?action=approve&ticketId="+row.ticketId+"&fPrdCode="+fPrdCode;
			var tab = {"id": "repoApprove",name:"repoApprove",url:url,title:"买断式回购",
						parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
			var paramData = {selectData:selections[0]};
			CommonUtil.openNewMenuTab(tab,paramData);
			// top["win"].showTab(tab);
		}else{
			Approve.approveCommit(Approve.FlowType.VerifyApproveFlow,selections[0]["ticketId"],Approve.OrderStatus.New,"IfsRmbOrService",prdNo,function(){
				search(grid.pageSize,grid.pageIndex);
			});
		}
	};
	//审批
	function approve(){
		mini.get("approve_commit_btn").setEnabled(false);
		var messageid = mini.loading("系统正在处理...", "请稍后");
		try {
			verify(grid.getSelecteds());
		} catch (error) {
			
		}
		mini.hideMessageBox(messageid);	
		mini.get("approve_commit_btn").setEnabled(true);
	}
	//提交审批
	function commit(){
		mini.get("approve_mine_commit_btn").setEnabled(false);
		var messageid = mini.loading("系统正在处理...", "请稍后");
		try {
			verify(grid.getSelecteds());
		} catch (error) {
			
		}
		mini.hideMessageBox(messageid);	
		mini.get("approve_mine_commit_btn").setEnabled(true);
	}
	//审批日志
	function searchlog(){
		appLog(grid.getSelecteds());
	}
	//打印
//    function print(){
//		var selections = grid.getSelecteds();
//		if(selections == null || selections.length == 0){
//			mini.alert('请选择一条要打印的数据！','系统提示');
//			return false;
//		}else if(selections.length>1){
//			mini.alert('暂不支持多条数据同时打印，请选择一条要打印的数据！','系统提示');
//			return false;
//		}
//		var canPrint = selections[0].ticketId;
//		
//		if(!CommonUtil.isNull(canPrint)){
//			var actionStr = CommonUtil.pPath + "/sl/IfsPrintUtilController/exportload/print/" +PrintNo.repo+"/"+ canPrint;
//			$('<form target="_blank" action=' + actionStr + ' method="post"></form>').appendTo('body').submit().remove();
//		};
//	}

	//打印
	function print(){
		var selections = grid.getSelecteds();
		if(selections == null || selections.length == 0){
			mini.alert('请选择一条要打印的数据！','系统提示');
			return false;
		}else if(selections.length>1){
			mini.alert('暂不支持多条数据同时打印，请选择一条要打印的数据！','系统提示');
			return false;
		}
		var canPrint = selections[0].ticketId;

		if(!CommonUtil.isNull(canPrint)){
			var actionStr = CommonUtil.pPath + "/sl/IfsPrintUtilController/exportload/print/"+PrintNo.repo+"/" + canPrint;
			$('<form target="_blank" action=' + actionStr + ' method="post"></form>').appendTo('body').submit().remove();
		};
	}

	//导出报表
	function exportExcel(){
		exportExcelManin("jymdshg.xls","jymdshg");
	}
	
</script>
</body>
</html>
