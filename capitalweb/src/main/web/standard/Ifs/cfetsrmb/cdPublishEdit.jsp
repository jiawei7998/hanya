<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../../global.jsp" %>
<html>
<head>
    <script type="text/javascript" src="./rmbVerify.js"></script>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>存单发行维护</title>
    <script type="text/javascript">
        /**prdCode用于页面投资组合的选择   该全局变量必须为prdCode*/
        var prdCode = "ISCD";
    </script>
</head>

<body style="width:100%;height:100%;background:white">
<div class="mini-splitter" style="width:100%;height:100%;">
    <div size="90%" showCollapseButton="false">
        <h1 style="text-align:center"><strong>存单发行</strong></h1>
        <div id="field_form" class="mini-fit area" style="background:white">
            <input id="sponsor" name="sponsor" class="mini-hidden"/>
            <input id="sponInst" name="sponInst" class="mini-hidden"/>
            <input id="aDate" name="aDate" class="mini-hidden"/>
            <input id="ticketId" name="ticketId" class="mini-hidden"/>

            <div class="mini-panel" title="成交单编号" style="width:100%;" allowResize="true" collapseOnTitleClick="true">
                <div class="leftarea">
                    <input style="width:100%;" id="contractId" name="contractId" class="mini-textbox mini-mustFill"
                           required="true" labelField="true" requiredErrorText="该输入项为必输项" label="成交单编号："
                           labelStyle="text-align:left;width:130px;" vtype="maxLength:20"
                           onvalidation="onEnglishAndNumberValidation"/>
                </div>
            </div>
            <div class="mini-panel" title="融入方信息" style="width:100%" allowResize="true" collapseOnTitleClick="true">
                <input id="borrowInst" name="borrowInst" class="mini-textbox mini-mustFill" labelField="true"
                       label="机构：" required="true" style="width:49.5%;" enabled="false"
                       labelStyle="text-align:left;width:130px;"/>
                <input id="borrowTrader" name="borrowTrader" class="mini-textbox mini-mustFill" labelField="true"
                       label="交易员：" required="true" vtype="maxLength:5" enabled="false"
                       labelStyle="text-align:left;width:135px;" style="width:49.5%;"/>
                <!-- <input id="borrowTel" name="borrowTel" class="mini-textbox" labelField="true"  label="电话："   onvalidation="CommonUtil.onValidation(e,'phone',[null])"  labelStyle="text-align:left;width:130px;" style="width:49.5%;"   />
                <input id="borrowFax" name="borrowFax" class="mini-textbox" labelField="true"  label="传真："  onvalidation="CommonUtil.onValidation(e,'phone',[null])" labelStyle="text-align:left;width:130px;" style="width:49.5%;"   />
                <input id="borrowCorp" name="borrowCorp" class="mini-textbox" labelField="true"  label="法人代表："  vtype="maxLength:10"  labelStyle="text-align:left;width:130px;" style="width:49.5%;"   />
                <input id="borrowAddr" name="borrowAddr" class="mini-textbox" labelField="true"  label="地址："  vtype="maxLength:50" labelStyle="text-align:left;width:130px;" style="width:49.5%;"   />
             -->
            </div>

            <div class="mini-panel" title="交易主体信息" style="width:100%" allowResize="true" collapseOnTitleClick="true">
                <div><span id="depositMsg" style="color:red;font-size:16px;"></span></div>
                <div class="leftarea">
                    <input style="width:100%;" id="forDate" name="forDate" class="mini-datepicker mini-mustFill"
                           labelField="true" required="true" label="成交日期：" labelStyle="text-align:left;width:130px;"/>
                    <input style="width:100%;" id="depositCode" name="depositCode" class="mini-buttonedit mini-mustFill"
                           labelField="true" onbuttonclick="onBondQuery" label="存单代码：" required="true"
                           vtype="maxLength:30" allowInput="false" labelStyle="text-align:left;width:130px;"/>
                    <input style="width:100%" id="depositName" name="depositName" class="mini-textbox" labelField="true"
                           label="存单简称：" required="true" vtype="maxLength:40"
                           labelStyle="text-align:left;width:130px;"/>
                    <!-- 					<input style="width:100%" id="planAmount" name="planAmount" class="mini-spinner" labelField="true"  label="计划发行量(元)：" required="true"   minValue="0" maxValue="99999999999999.9999" format="n4" changeOnMousewheel="false"  labelStyle="text-align:left;width:130px;"  onvalidation="zeroValidation" /> -->
                    <input style="width:100%" id="actualAmount" name="actualAmount"
                           class="mini-spinner input-text-strong" labelField="true" label="实际发行量(元)：" enabled="false"
                           required="true" minValue="0" maxValue="99999999999999.9999" format="n4"
                           changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"/>
                    <input style="width:100%" id="publishPrice" name="publishPrice"
                           class="mini-spinner input-text-strong" labelField="true" label="发行价格(元)：" required="true"
                           minValue="0" maxValue="9999999.9999" format="n4" changeOnMousewheel="false"
                           labelStyle="text-align:left;width:130px;" onvalidation="zeroValidation"/>
                    <input style="width:100%" id="valueDate" name="valueDate" class="mini-datepicker mini-mustFill"
                           labelField="true" label="起息日(缴款日期)：" required="true"
                           labelStyle="text-align:left;width:130px;"/>
                    <input style="width:100%" id="currency" name="currency" class="mini-combobox" labelField="true"
                           label="币种：" data="CommonUtil.serverData.dictionary.Currency"
                           labelStyle="text-align:left;width:130px;" value="CNY"/>
                    <input id="dealTransType" name="dealTransType" class="mini-combobox" labelField="true"
                           style="width:100%;" label="数据来源：" data="CommonUtil.serverData.dictionary.dealTransType1"
                           vtype="maxLength:10" labelStyle="text-align:left;width:130px;" value="1">
                    <input id="note" name="note" class="mini-textbox" labelField="true" label="交易事由："
                           labelStyle="text-align:left;width:130px;" style="width:100%;" vtype="maxLength:16"/>
                </div>
                <div class="rightarea">
                    <input id="nettingStatus" name="nettingStatus" class="mini-combobox mini-mustFill" required="true"
                           labelField="true" label="净额清算状态：" labelStyle="text-align:left;width:130px;"
                           style="width:100%;" data="CommonUtil.serverData.dictionary.YesNo" showNullItem="true"/>
                    <input style="width:100%" id="depositAllName" name="depositAllName" class="mini-textbox"
                           labelField="true" label="存单全称：" required="true" vtype="maxLength:200"
                           labelStyle="text-align:left;width:130px;"/>
                    <input id="interestType" name="interestType" class="mini-combobox mini-mustFill" required="true"
                           labelField="true" label="投资类型：" style="width:100%;" labelStyle="text-align:left;width:130px;"
                           data="CommonUtil.serverData.dictionary.invtype"/>
                    <input style="width:100%" id="benchmarkcurvename" name="benchmarkcurvename"
                           class="mini-spinner input-text-strong" labelField="true" label="参考收益率(%)：" required="true"
                           minValue="0" maxValue="9999.99999999" format="n8" changeOnMousewheel="false"
                           labelStyle="text-align:left;width:130px;" onValuechanged="getPriceDeviation"/>
                    <input style="width:100%" id="publishDate" name="publishDate" class="mini-datepicker mini-mustFill"
                           labelField="true" label="发行日期：" required="true" changeOnMousewheel="false"
                           labelStyle="text-align:left;width:130px;"/>
                    <input style="width:100%" id="duedate" name="duedate" class="mini-datepicker mini-mustFill"
                           labelField="true" label="到期日：" required="true" labelStyle="text-align:left;width:130px;"/>
                    <input style="width:100%" id="depositTerm" name="depositTerm" class="mini-spinner" labelField="true"
                           label="存单期限：" required="true" vtype="maxLength:999" labelStyle="text-align:left;width:130px;"
                           changeOnMousewheel="false"/>
                    <input id="tradingModel" name="tradingModel" class="mini-combobox mini-mustFill" required="true"
                           labelField="true" label="交易模式：" labelStyle="text-align:left;width:130px;" style="width:100%;"
                           data="CommonUtil.serverData.dictionary.TradeModel"/>
                </div>
            </div>
            <div class="mini-panel" title="报表要素" style="width:100%;" allowResize="true" collapseOnTitleClick="false">
                <div class="leftarea">
                    <input id="attributionDept" name="attributionDept" valueField="id" showFolderCheckBox="true"
                           labelField="true" label="归属机构：" showCheckBox="true" style="width:100%"
                           labelStyle="text-align:left;width:130px"
                           showTreeLines="true" showTreeIcon="true" textField="instName" parentField="parentId"
                           class="mini-treeselect" expandOnLoad="true" resultAsTree="false"
                           valueFromSelect="true" emptyText="请选择机构"/>
                    <input id="remoteService" name="remoteService" class="mini-combobox" labelField="true"
                           style="width:100%;" label="是否异地业务：" data="CommonUtil.serverData.dictionary.remoteService"
                           vtype="maxLength:10" labelStyle="text-align:left;width:130px;" value="1">
                </div>
                <div class="rightarea">
                    <input id="fiveLevelClass" name="fiveLevelClass" class="mini-combobox" labelField="true"
                           requiredErrorText="该输入项为必输项" label="五级分类：" labelStyle="text-align:left;width:130px;"
                           style="width:100%;" data="CommonUtil.serverData.dictionary.FiveLevelClass" value="0"/>
                    <input id="depositAccount" name="depositAccount" class="mini-textbox" labelField="true" label="存款账号："
                           labelStyle="text-align:left;width:130px;" style="width:100%;" />
<%--                    <input id="depositAgreementCode" name="depositAgreementCode" class="mini-textbox" labelField="true"--%>
<%--                           label="存款协议代码："maxlength="50" labelStyle="text-align:left;width:130px;"style="width:100%;"/>--%>
                </div>
            </div>
            <div class="mini-panel" title="认购人明细信息" style="width:100%" allowResize="true" collapseOnTitleClick="true">
                <div id="search_detail_form">
                    <div class="mini-panel" title="认购人信息" style="width:100%" allowResize="true"
                         collapseOnTitleClick="true">
                        <input id="lendInst" name="lendInst" class="mini-buttonedit mini-mustFill"
                               onbuttonclick="onButtonEdit" labelField="true" label="机构：" vtype="maxLength:50"
                               labelStyle="text-align:left;width:130px;" style="width:40%;"/>
                        <input id="lendTrader" name="lendTrader" class="mini-textbox"   labelField="true"
                               label="交易员："  labelStyle="text-align:left;width:130px;"
                               style="width:40%;"/>
                        <input id="manageType" name="manageType" class="mini-combobox mini-mustFill"
                               data="CommonUtil.serverData.dictionary.ManageType" labelField="true" label="管理人类型："
                               vtype="maxLength:30" labelStyle="text-align:left;width:130px;" style="width:40%;"/>
                        <input id="isCuryFund" name="isCuryFund" class="mini-combobox"
                               data="CommonUtil.serverData.dictionary.YesNo" labelField="true" label="是否货币市场基金："
                               vtype="maxLength:30" labelStyle="text-align:left;width:130px;" style="width:40%;"/>
                    </div>
                    <div class="mini-panel" title="认购人交易主体" style="width:100%" allowResize="true"
                         collapseOnTitleClick="true">
                        <input id="offerAmount" name="offerAmount" onvaluechanged="setShouldPayMoney"
                               class="mini-spinner mini-mustFill input-text-strong" changeOnMousewheel='false'
                               labelField="true" label="认购量(元)：" maxValue="99999999999999.9999" format="n4"
                               style="width:40%" labelStyle="text-align:left;width:130px;"/>
                        <input id="shouldPayMoney" name="shouldPayMoney"
                               class="mini-spinner mini-mustFill input-text-strong" labelField="true" label="应缴纳金额(元)："
                               maxValue="99999999999999.9999" changeOnMousewheel="false" format="n4"
                               labelStyle="text-align:left;width:130px;" style="width:40%"/>
                    </div>
                    <%@ include file="../../Common/opicsLess.jsp" %>
                    <%@ include file="../../Common/RiskCenter.jsp" %>
                </div>
                <div class="centerarea" style="margin-top:5px;">
		<span style="float:left;">
		
		</span>

                    <div class="mini-toolbar" style="padding:0px;border-bottom:0;" id="toolbar">
                        <table style="width:100%;">
                            <tr>
                                <td style="width:100%;">
                                    <a id="add_btn" class="mini-button" style="display: none" onclick="add()">添加</a>
                                    <a id="edit_btn" class="mini-button" style="display: none" onclick="edit()">修改</a>
                                    <a id="delete_btn" class="mini-button" style="display: none" onclick="del()">删除</a>
                                    <a id="saves_btn" class="mini-button" style="display: none" onclick="saves()"
                                       enabled="false">保存</a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="centerarea" style="height:150px;">
                    <div class="mini-fit">
                        <div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id"
                             allowAlternating="true"
                             fitColumns="false" allowResize="true" sortMode="client" allowAlternating="true"
                             showpager="false">
                            <div property="columns">
                                <div type="indexcolumn" width="50px" headerAlign="center">序号</div>
                                <div field="lendInst" width="100px" align="center" headerAlign="center">机构</div>
                                <div field="lendInstName" width="160px" align="center" headerAlign="center">机构名称</div>
                                <div field="manageType" width="100px" align="center" headerAlign="center"
                                     renderer="CommonUtil.dictRenderer" data-options="{dict:'ManageType'}">管理人类型
                                </div>
                                <div field="isCuryFund" width="120px" align="center" headerAlign="center"
                                     renderer="CommonUtil.dictRenderer" data-options="{dict:'YesNo'}">是否货币市场基金
                                </div>
                                <div field="offerAmount" width="100px" align="right" headerAlign="center"
                                     numberFormat="n4">认购量(元)
                                </div>
                                <div field="shouldPayMoney" width="100px" align="right" headerAlign="center"
                                     numberFormat="n4">应缴纳金额(元)
                                </div>
                                <div field="dealSource" width="120px" align="center" headerAlign="center"
                                     renderer="CommonUtil.dictRenderer" data-options="{dict:'TradeSource'}">交易来源
                                </div>
                                <div field="port" width="80px" align="center" headerAlign="center">投资组合</div>
                                <div field="cost" width="100px" align="center" headerAlign="center">成本中心</div>
                                <div field="product" width="80px" align="center" headerAlign="center">产品代码</div>
                                <div field="prodType" width="80px" align="center" headerAlign="center">产品类型</div>
                                <div field="dealNo" width="100px" align="center" headerAlign="center">opics交易号</div>
                                <div field="statcode" width="100px" align="center" headerAlign="center">opics处理状态</div>
                                <div field="ccysmeans" width="80px" align="center" headerAlign="center">付款方式</div>
                                <div field="ccysacct" width="80px" align="center" headerAlign="center">付款账户</div>
                                <div field="ctrsmeans" width="80px" align="center" headerAlign="center">收款方式</div>
                                <div field="ctrsacct" width="80px" align="center" headerAlign="center">收款账户</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mini-panel" title="融入方清算信息" style="width:100%" allowResize="true" collapseOnTitleClick="true">
                <input id="borrowOpBank" name="borrowOpBank" class="mini-textbox" labelField="true" label="资金开户行："
                       vtype="maxLength:1333" labelStyle="text-align:left;width:130px;" style="width:49%;"/>
                <input id="borrowAccnum" name="borrowAccnum" class="mini-textbox" labelField="true" label="资金账号："
                       onvalidation="onEnglishAndNumberValidation" vtype="maxLength:400"
                       labelStyle="text-align:left;width:130px;" style="width:49%;"/>
                <input id="borrowPsnum" name="borrowPsnum" class="mini-textbox" labelField="true" label="支付系统行号："
                       onvalidation="onEnglishAndNumberValidation" vtype="maxLength:400"
                       labelStyle="text-align:left;width:130px;" style="width:49%;"/>
                <input id="borrowCaname" name="borrowCaname" class="mini-textbox" labelField="true" label="托管账户户名："
                       vtype="maxLength:1333" labelStyle="text-align:left;width:130px;" style="width:49%;"/>
                <input id="borrowCustname" name="borrowCustname" class="mini-buttonedit mini-mustFill"
                       onbuttonclick="onSaccQuery" required="true" labelField="true" allowInput="false" label="托管机构："
                       vtype="maxLength:1333" labelStyle="text-align:left;width:130px;" style="width:49%;"/>
                <input id="borrowCanum" name="borrowCanum" class="mini-textbox" labelField="true" label="托管帐号："
                       onvalidation="onEnglishAndNumberValidation" vtype="maxLength:400"
                       labelStyle="text-align:left;width:130px;" style="width:49%;"/>
            </div>
            <%@ include file="../../Common/Flow/MiniApproveOpCommon.jsp" %>
        </div>
    </div>
    <div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
        <div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"
                                                                style="width:130px;" id="save_btn"
                                                                onclick="save">保存交易</a></div>
        <div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"
                                                                style="width:130px;" id="close_btn"
                                                                onclick="close">关闭界面</a></div>
    </div>
</div>
<script type="text/javascript">
    mini.parse();

    //获取当前tab
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var row = params.selectData;
    var detailData = row;
    var url = window.location.search;
    var action = CommonUtil.getParam(url, "action");
    mini.get("priceDeviation").setVisible(true);//偏离度
    mini.get("applyNo").setVisible(false);//信用风险审查单号
    mini.get("custNo").setVisible(false);//占用授信主体
    mini.get("applyProd").setVisible(false);//产品名称
    mini.get("weight").setVisible(false);//权重

    var prdNo = CommonUtil.getParam(url, "prdNo");
    var fPrdCode = CommonUtil.getParam(url, "fPrdCode");
    var form = new mini.Form("#field_form");
    var grid = mini.get("datagrid");

    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize, pageIndex);
    });
    var tradeData = {};

    tradeData.selectData = row;
    tradeData.operType = action;
    tradeData.serial_no = row.ticketId;
    tradeData.task_id = row.taskId;

    //加载机构树
    function nodeclick(e) {
        var oldvalue = e.sender.value;
        var param = mini.encode({"branchId": branchId}); //序列化成JSON
        var oldData = e.sender.data;
        if (oldData == null || oldData == "") {
            CommonUtil.ajax({
                url: "/InstitutionController/searchInstitutionByBranchId",
                data: param,
                callback: function (data) {
                    var obj = e.sender;
                    obj.setData(data.obj);
                    obj.setValue(oldvalue);
                }
            });
        }
    }

    function onBondQuery(e) {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Ifs/bond/bondAddMini.jsp?prodtype=CI",
            title: "选择债券列表",
            width: 700,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        //债券特殊判断(债券代码)
// 	                     if(data.bndnm_cn=="23143124"){
// 							$('#depositMsg').html("dsaf");
// 	                    	$('#depositMsg').html("");
// 	                     }
                        //加载其他信息
                        setCdOtherInfo(data);
                        btnEdit.setValue(data.bndcd);
                        btnEdit.setText(data.bndcd);
                        getPriceDeviation();
                        btnEdit.focus();
                    }
                }
            }
        });
    }

    function setCdOtherInfo(data) {
        mini.get("depositAllName").setValue(data.bndnm_cn);
        mini.get("depositName").setValue(data.bndnm_en);
        mini.get("publishPrice").setValue(data.issprice);
        //mini.get("valueDate").setValue(data.issudt);
        mini.get("valueDate").setValue("<%=__bizDate %>");
        mini.get("currency").setValue(data.ccy);
        mini.get("publishDate").setValue(data.pubdt);
        mini.get("duedate").setValue(data.matdt);
        mini.get("depositTerm").setValue(data.bondperiod);
        mini.get("benchmarkcurvename").setValue(data.couponrate);
    }

    function onSecurityQuery(e) {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Ifs/bond/bondAddMini.jsp",
            title: "选择债券列表",
            width: 700,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.bndcd);
                        btnEdit.setText(data.bndcd);
                        mini.get("underlyingSymbol").setValue(data.bndnm_cn);
                        mini.get("underlyingSymbol").setText(data.bndnm_cn);
                        btnEdit.focus();
                    }
                }
            }
        });
    }

    function onPortCostQuery() {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "/../Common/OpicsTradeParamMini.jsp",
            title: "投资组合选择",
            width: 900,
            height: 500,

            onload: function () {//弹出页面加载完成
                var iframe = this.getIFrameEl();
                var data1 = {prdCode: prdCode};
                //调用弹出页面方法进行初始化
                iframe.contentWindow.SetData(data1);
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data); //必须
                    if (data) {
                        btnEdit.setValue(data.port);
                        btnEdit.setText(data.port);
                        mini.get("costb").setValue(data.cost);//成本中心
                        mini.get("prodb").setValue(data.product);//产品
                        mini.get("prodtypeb").setValue(data.prodType);//产品类型
                        btnEdit.focus();
                    }

                }

            }
        });
    }


    //添加债券认购人信息
    function add() {
        var detailForm = new mini.Form("#search_detail_form");
        var detailData = detailForm.getData(true);
        if (CommonUtil.isNull(detailData.lendInst)
            || CommonUtil.isNull(detailData.shouldPayMoney) || CommonUtil.isNull(detailData.dealSource) || CommonUtil.isNull(detailData.port)
            || CommonUtil.isNull(detailData.product) || CommonUtil.isNull(detailData.cost) || CommonUtil.isNull(detailData.prodType)
        ) {
            mini.alert("请将债券信息填写完整", "系统提示");
            return;
        }
        //校验金额不能为空
        if (detailData.offerAmount == 0) {
            mini.alert("认购量不能为0");
            return;
        }
        if (detailData.shouldPayMoney == 0) {
            mini.alert("应交金额不能为0");
            return;
        }
        addRow(detailData);

    }

    function addRow(detailData) {
        var row = {
            lendInst: detailData.lendInst,
            lendInstName: mini.get("lendInst").getText(),
            lendTrader: detailData.lendTrader,
            manageType: detailData.manageType,
            isCuryFund: detailData.isCuryFund,
            offerAmount: detailData.offerAmount,
            shouldPayMoney: detailData.shouldPayMoney,
            lendCcysmeans: detailData.lendCcysmeans,
            lendCcysacct: detailData.lendCcysacct,
            lendCtrsmeans: detailData.lendCtrsmeans,
            lendCtrsacct: detailData.lendCtrsacct,
            dealSource: detailData.dealSource,
            port: detailData.port,
            cost: detailData.cost,
            product: detailData.product,
            prodType: detailData.prodType,
            ccysmeans: detailData.ccysmeans,
            ccysacct: detailData.ccysacct,
            ctrsmeans: detailData.ctrsmeans,
            ctrsacct: detailData.ctrsacct
        };
        grid.addRow(row);
        var detailForm = new mini.Form("#search_detail_form");
        detailForm.clear();
        //重新计算券面总额合计(万元)
        calcAmtSum(grid.getData());
    }

    /**计算 券面总额合计*/
    function calcAmtSum(rows) {
        var marginTotalAmt = 0;
        for (var i = 0; i < rows.length; i++) {
            marginTotalAmt = marginTotalAmt + parseFloat(rows[i].offerAmount);
        }
        mini.get("actualAmount").setValue(marginTotalAmt);
    };

    //删除
    function del() {
        var row = grid.getSelected();
        if (!row) {
            mini.alert("请选中要移除的一行", "提示");
            return;
        }
        grid.removeRow(row);
        //重新计算授信额度总和
        calcAmtSum(grid.getData());
    }

    //修改
    function edit() {
        var row = grid.getSelected();
        if (!row) {
            mini.alert("请选中一行进行修改!", "提示");
            return;
        }
        var detailForm = new mini.Form("#search_detail_form");
        detailForm.setData(row);
        //投资组合
        mini.get("port").setValue(row.port);
        mini.get("port").setText(row.port);
        //成本中心
        mini.get("cost").setValue(row.cost);
        mini.get("cost").setText(row.cost);
        //产品代码
        mini.get("product").setValue(row.product);
        mini.get("product").setText(row.product);
        //产品类型
        mini.get("prodType").setValue(row.prodType);
        mini.get("prodType").setText(row.prodType);
        //添加机构和托管机构
        mini.get("lendInst").setValue(row.lendInst);
// 	        mini.get("lendInst").setText(row.lendInst);
        queryTextName(row.lendInst);

        mini.get("add_btn").setEnabled(false);
        mini.get("edit_btn").setEnabled(false);
        mini.get("saves_btn").setEnabled(true);
    }

    //修改保存
    function saves() {
        del();
        add();
        mini.get("add_btn").setEnabled(true);
        mini.get("edit_btn").setEnabled(true);
        mini.get("saves_btn").setEnabled(false);
    }

    //初始化债券信息
    function initGrid() {
        var url = "/IfsCfetsrmbDpController/searchOfferDpDetails";
        var data = {};
        data['ticketId'] = mini.get("ticketId").getValue();
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: url,
            data: params,
            callback: function (data) {
                grid.setData(data.obj.rows);
            }
        });
    }

    //保存
    function save() {
        //表单验证！！！
        //设置交易来源和投资组合不必填
        mini.get("dealSource").setRequired(false);
        mini.get("product").setRequired(false);
        mini.get("cost").setRequired(false);
        mini.get("port").setRequired(false);
        mini.get("prodType").setRequired(false);
        form.validate();
        if (form.isValid() == false) {
            mini.alert("信息填写有误，请重新填写", "系统提示");
            return;
        }
        if (!checkOpics()) {
            return;
        }
        if (!checkDate()) {
            return;
        }
        var data = form.getData(true);
        var rows = grid.getData();
        if (rows.length == 0) {
            mini.alert("认购人信息不能为空", "系统提示");
            return;
        }
        if (toDecimal(data['miscFeeType']) != toDecimal(data['underlyingQty'] * 10000 * data['price'] * data['occupancyDays'] / 100 / 365) || data['tenor'] != data['occupancyDays']) {
            mini.confirm("确认以当前数据为准吗？", "确认", function (action) {
                if (action != "ok") {
                    return;
                }
                data['borrowInst'] = "<%=__sessionInstitution.getInstId()%>";
                data['borrowTrader'] = "<%=__sessionUser.getUserId() %>";
                data['sponInst'] = "<%=__sessionInstitution.getInstId()%>";
                data['sponsor'] = "<%=__sessionUser.getUserId() %>";
// 				data['dealTransType']="M";
                data['IfsCfetsrmbDpOffer'] = rows;
                data['instrumentType'] = "CD";
                data['fPrdCode']=fPrdCode;
                var params = mini.encode(data);
                CommonUtil.ajax({
                    url: "/IfsCfetsrmbDpController/saveDp",
                    data: params,
                    callback: function (data) {
                        mini.alert(data.desc, '提示信息', function () {
                            if (data.code == 'error.common.0000') {
                                top["win"].closeMenuTab();//关闭并刷新当前界面
                            }
                        });
                    }
                });
            });
        } else {
            data['borrowInst'] = "<%=__sessionInstitution.getInstId()%>";
            data['borrowTrader'] = "<%=__sessionUser.getUserId() %>";
            data['sponInst'] = "<%=__sessionInstitution.getInstId()%>";
            data['sponsor'] = "<%=__sessionUser.getUserId() %>";
// 			data['dealTransType']="M";
            data['IfsCfetsrmbDpOffer'] = rows;
            data['instrumentType'] = "CD";
            data['fPrdCode']=fPrdCode;
            var params = mini.encode(data);
            CommonUtil.ajax({
                url: "/IfsCfetsrmbDpController/saveDp",
                data: params,
                callback: function (data) {
                    mini.alert(data.desc, '提示信息', function () {
                        if (data.code == 'error.common.0000') {
                            top["win"].closeMenuTab();//关闭并刷新当前界面
                        }
                    });
                }
            });
        }
    }

    //opics处理校验
    function checkOpics() {
        //起息日要大于成交日期
        var forDate = mini.formatDate(mini.get("forDate").getValue(), 'yyyy-MM-dd');
        var valueDate = mini.formatDate(mini.get("valueDate").getValue(), 'yyyy-MM-dd');
        var opicsFlag = CommonUtil.dateCompare(valueDate, forDate);
        if (!opicsFlag) {
            mini.alert("起息日要大于等于成交日期!");
        }
        return opicsFlag;
    }

    function checkDate() {
        //起息日大于发行日期
        var publishDate = mini.formatDate(mini.get("publishDate").getValue(), 'yyyy-MM-dd');
        var valueDate = mini.formatDate(mini.get("valueDate").getValue(), 'yyyy-MM-dd');
        var opicsFlag = CommonUtil.dateCompare(valueDate, publishDate);
        if (!opicsFlag) {
            mini.alert("起息日要大于等于发行日期!");
        }
        return opicsFlag;
    }

    function close() {
        top["win"].closeMenuTab();
    }

    //文、数字、下划线 的验证
    function onEnglishAndNumberValidations(e) {
        if (e.value == "" || e.value == null) {//值为空，就不做校验
            return;
        }
        if (e.isValid) {
            if (isEnglishAndNumbers(e.value) == false) {
                e.errorText = "必须输入英文小写+数字";
                e.isValid = false;
            }
        }
    }

    /* 是否英文+数字 */
    function isEnglishAndNumbers(v) {
        var re = new RegExp("^[0-9a-z\_\-]+$");
        if (re.test(v)) return true;
        return false;
    }


    //英文、数字、下划线 的验证
    function onEnglishAndNumberValidation(e) {
        if (e.value == "" || e.value == null) {//值为空，就不做校验
            return;
        }
        if (e.isValid) {
            if (isEnglishAndNumber(e.value) == false) {
                e.errorText = "必须输入英文+数字";
                e.isValid = false;
            }
        }
    }

    /* 是否英文+数字 */
    function isEnglishAndNumber(v) {
        var re = new RegExp("^[0-9a-zA-Z\_\-]+$");
        if (re.test(v)) return true;
        return false;
    }

    function loadInstitutionTree() {
        CommonUtil.ajax({
            data: {"branchId": branchId},
            url: "/InstitutionController/searchInstitutions",
            callback: function (data) {
                mini.get("attributionDept").setData(data.obj);
                if (detailData != null && detailData.attributionDept != null) {
                    mini.get("attributionDept").setValue(detailData.attributionDept);
                } else {
                    mini.get("attributionDept").setValue("<%=__sessionInstitution.getInstId()%>");
                }
            }
        });
    }

    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
            if ($.inArray(action, ["approve", "detail"]) > -1) {
                mini.get("save_btn").setVisible(false);
                form.setEnabled(false);
                var approForm = new mini.Form("#approve_operate_form");
                approForm.setEnabled(true);
                mini.get("forDate").setEnabled(false);
                mini.get("toolbar").setVisible(false);
            }
            if ($.inArray(action, ["add", "edit"]) > -1) {
                mini.get("toolbar").setVisible(true);
            }
            if ($.inArray(action, ["edit", "approve", "detail"]) > -1) {
                form.setData(row);
                mini.get("depositCode").setValue(row.depositCode);
                mini.get("depositCode").setText(row.depositCode);
                initGrid();
                mini.get("forDate").setValue(row.forDate);
                mini.get("ticketId").setValue(row.ticketId);
                //对方机构lendInst
                mini.get("borrowInst").setValue(row.borrowInst);
                mini.get("borrowTrader").setValue(row.myUserName);
                mini.get("contractId").setEnabled(false);

                mini.get("borrowCustname").setValue(row.borrowCustname);
                mini.get("borrowCustname").setText(row.borrowCustname);

//			if(action == "edit" && row.priceDeviation ==null ){//偏离度
//				if(CommonUtil.isNull(row.priceDeviation)){
//					mini.get("price").setValue(row.price);
//					mini.get("price").setText(row.price);
//					mini.get("currencyPair").setText(row.currencyPair);
//					mini.get("currencyPair").setValue(row.currencyPair);
//					getPriceDeviation();
//				}
//			}

            } else {
                mini.get("sponsor").setValue("<%=__sessionUser.getUserId()%>");
                mini.get("sponInst").setValue("<%=__sessionUser.getInstId() %>");
                mini.get("borrowInst").setValue("<%=__sessionInstitution.getInstFullname()%>");
                mini.get("borrowTrader").setValue("<%=__sessionUser.getUserName() %>");
                mini.get("forDate").setValue("<%=__bizDate %>");
                //产品代码
                mini.get("product").setValue("SECUR");
                mini.get("product").setText("SECUR");
            }
            mini.get("dealTransType").setEnabled(false);
            loadInstitutionTree();
        });
    });

    function onButtonEdit(e) {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Common/CustMini.jsp",
            title: "选择对手方列表",
            width: 900,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.cno);
                        btnEdit.setText(data.cliname);
                        if (data.cliname.indexOf("基金") > -1) {
                            mini.get("manageType").setValue("2");
                            mini.get("isCuryFund").setEnabled(true);
                        } else {
                            mini.get("manageType").setValue("1");
                            mini.get("isCuryFund").setEnabled(false);
                        }
                        btnEdit.focus();
                    }
                }

            }
        });
    }

    //根据交易对手编号查询全称
    function queryTextName(cno) {
        CommonUtil.ajax({
            url: "/IfsOpicsCustController/searchIfsOpicsCust",
            data: {'cno': cno},
            callback: function (data) {
                if (data && data.obj) {
                    mini.get("lendInst").setText(data.obj.cliname);
                } else {
                    mini.get("lendInst").setText();
                }
            }
        });
    }

    function onDrawDateFirst(e) {
        var firstDate = e.date;
        var secondDate = mini.get("secondSettlementDate").getValue();
        if (CommonUtil.isNull(secondDate)) {
            return;
        }
        if (secondDate.getTime() < firstDate.getTime()) {
            e.allowSelect = false;
        }
    }

    function onDrawDateSecond(e) {
        var secondDate = e.date;
        var firstDate = mini.get("firstSettlementDate").getValue();
        if (CommonUtil.isNull(firstDate)) {
            return;
        }
        if (secondDate.getTime() < firstDate.getTime()) {
            e.allowSelect = false;
        }
    }

    function linkageCalculatDay(e) {
        var firstDate = mini.get("firstSettlementDate").getValue();
        var secondDate = mini.get("secondSettlementDate").getValue();
        if (CommonUtil.isNull(firstDate) || CommonUtil.isNull(secondDate)) {
            return;
        }
        var subDate = Math.abs(parseInt((secondDate.getTime() - firstDate.getTime()) / 1000 / 3600 / 24));
        mini.get("tenor").setValue(subDate);
        mini.get("occupancyDays").setValue(subDate);
        var occupancyDays = mini.get("occupancyDays").getValue();
        var rate = mini.get("price").getValue();
        var tradeAmount = mini.get("underlyingQty").getValue();
        mini.get("miscFeeType").setValue(tradeAmount * 10000 * rate * occupancyDays / 100 / 365);
    }

    function linkageCalculat(e) {
        var occupancyDays = e.value;
        var rate = mini.get("price").getValue();
        var tradeAmount = mini.get("underlyingQty").getValue();
        mini.get("miscFeeType").setValue(tradeAmount * 10000 * rate * occupancyDays / 100 / 365);
    }


    /**   清算路径1的选择   */
    function settleMeans1(type) {
        var cpInstId = mini.get("borrowInst").getValue();
        if (type == '2') {
            cpInstId = mini.get("lendInst").getValue();
        }
        if (cpInstId == null || cpInstId == "") {
            mini.alert("请先选择机构!");
            return;
        }
        var ccy = "CNY";
        var url = "./Ifs/opics/setaMini.jsp";
        var data = {ccy: ccy, cust: cpInstId};

        var btnEdit = this;
        mini.open({
            url: url,
            title: "选择清算路径",
            width: 700,
            height: 600,
            onload: function () {
                var iframe = this.getIFrameEl();
                iframe.contentWindow.SetData(data);
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data1 = iframe.contentWindow.GetData();
                    data1 = mini.clone(data1);    //必须
                    if (data1) {
                        if (type == '2') {
                            mini.get("lendCcysmeans").setValue($.trim(data1.smeans));
                            mini.get("lendCcysmeans").setText($.trim(data1.smeans));
                            mini.get("lendCcysacct").setValue($.trim(data1.sacct));
                        } else {
                            mini.get("ccysmeans").setValue($.trim(data1.smeans));
                            mini.get("ccysmeans").setText($.trim(data1.smeans));
                            mini.get("ccysacct").setValue($.trim(data1.sacct));
                        }
                        btnEdit.focus();
                    }
                }
            }
        });
    }

    /**   清算路径2的选择   */
    function settleMeans2(type) {
        var cpInstId = mini.get("borrowInst").getValue();
        if (type == '2') {
            cpInstId = mini.get("lendInst").getValue();
        }
        if (cpInstId == null || cpInstId == "") {
            mini.alert("请先选择机构!");
            return;
        }
        var ccy = "CNY";
        var url = "./Ifs/opics/setaMini.jsp";
        var data = {ccy: ccy, cust: cpInstId};

        var btnEdit = this;
        mini.open({
            url: url,
            title: "选择清算路径",
            width: 700,
            height: 600,
            onload: function () {
                var iframe = this.getIFrameEl();
                iframe.contentWindow.SetData(data);
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data1 = iframe.contentWindow.GetData();
                    data1 = mini.clone(data1);    //必须
                    if (data1) {
                        if (type == '2') {
                            mini.get("lendCtrsmeans").setValue($.trim(data1.smeans));
                            mini.get("lendCtrsmeans").setText($.trim(data1.smeans));
                            mini.get("lendCtrsacct").setValue($.trim(data1.sacct));
                        } else {
                            mini.get("ctrsmeans").setValue($.trim(data1.smeans));
                            mini.get("ctrsmeans").setText($.trim(data1.smeans));
                            mini.get("ctrsacct").setValue($.trim(data1.sacct));
                        }
                        btnEdit.focus();
                    }
                }

            }
        });
    }

    //托管机构选择
    function onSaccQuery(e) {
        var btnEdit = this;
        mini.open({
            url: "./Ifs/opics/saccMini.jsp",
            title: "选择托管机构列表",
            width: 700,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.accountno);
                        btnEdit.setText(data.accountno);
                        btnEdit.focus();
                    }
                }

            }
        });
    }

    function setShouldPayMoney(e) {
        var publishPrice = mini.get("publishPrice").getValue();
        var ShouldPayMoney = CommonUtil.accMul(e.value, publishPrice);
        ShouldPayMoney = CommonUtil.accDiv(ShouldPayMoney, 100);
        mini.get("shouldPayMoney").setValue(ShouldPayMoney);
    }

    function getPriceDeviation() {
        var depositCode = mini.get("depositCode").getValue();//存单代码
        var benchmarkcurvename = mini.get("benchmarkcurvename").getValue();//参考收益率
        if (CommonUtil.isNull(benchmarkcurvename) || CommonUtil.isNull(depositCode)) {
            return;
        }
        CommonUtil.ajax({
            url: "/IfsLimitController/queryRhisIntrate",
            data: {'prdNo': prdNo, 'benchmarkcurvename': benchmarkcurvename, 'depositCode': depositCode},
            callback: function (data) {
                if (data != null && data != "") {
                    mini.get("priceDeviation").setValue(data);
                } else {
                    mini.get("priceDeviation").setValue();
                }
            }
        });
    }

    /* 成功中心的选择 */
    // function onCostEdit(){
    // 	var btnEdit = this;
    //     mini.open({
    //         url: CommonUtil.baseWebPath() + "/opics/costMiniLess.jsp",
    //         title: "选择成本中心",
    //         width: 700,
    //         height: 500,
    //         onload: function () {
    //             var iframe = this.getIFrameEl();
    //             iframe.contentWindow.SetData({});
    //         },
    //         ondestroy: function (action) {
    //             if (action == "ok") {
    //                 var iframe = this.getIFrameEl();
    //                 var data = iframe.contentWindow.GetData();
    //                 data = mini.clone(data);    //必须
    //                 if (data) {
    //                     btnEdit.setValue(data.costcent);
    //                     btnEdit.setText(data.costcent);
    //                     //searchMarketData();
    //                     btnEdit.focus();
    //                 }
    //             }
    //         }
    //     });
    // }

    function searchMarketData() {
        var costCent = mini.get("cost").getValue();//成本中心
        var code = mini.get("interestType").getValue();//交易品种
        if (CommonUtil.isNull(code) || CommonUtil.isNull(costCent)) {
            return;
        }
        queryMarketData(code, costCent);
    }

    //获取DV01，久期限，止损值查询
    function queryMarketData(code, costCent) {
        CommonUtil.ajax({
            url: "/IfsOpicsCustController/queryMarketData",
            data: {'code': code, 'costCent': costCent},
            callback: function (data) {
                if (data && data.obj) {
                    mini.get("longLimit").setValue(data.obj.duration);//久期限
                    mini.get("lossLimit").setValue(data.obj.convexity);//止损
                    mini.get("DV01Risk").setValue(data.obj.delta);//DV01
                } else {
                    mini.get("longLimit").setValue();//久期限
                    mini.get("lossLimit").setValue();//止损
                    mini.get("DV01Risk").setValue();//DV01
                }
            }
        });
    }
</script>
<script type="text/javascript" src="../../Common/Flow/MiniApproveOpCommon.js"></script>
</body>
</html>
