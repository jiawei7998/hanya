<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../../global.jsp" %>
<html>
<head>
    <script type="text/javascript" src="./rmbVerify.js"></script>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/standard/Common/RiskCenter.js"></script>
    <title>同业拆放 维护</title>
    <script type="text/javascript">
        /**prdCode用于页面投资组合的选择   该全局变量必须为prdCode*/
        var prdCode = "MM";
    </script>
</head>

<body style="width:100%;height:100%;background:white">
<div class="mini-splitter" style="width:100%;height:100%;">
    <div size="90%" showCollapseButton="false">
        <h1 style="text-align:center"><strong>同业拆放</strong></h1>
        <div id="field_form" class="mini-fit area" style="background:white">
            <input id="sponsor" name="sponsor" class="mini-hidden"/>
            <input id="sponInst" name="sponInst" class="mini-hidden"/>
            <input id="aDate" name="aDate" class="mini-hidden"/>
            <input id="ticketId" name="ticketId" class="mini-hidden"/>
            <div class="mini-panel" title="成交单编号" style="width:100%;" allowResize="true" collapseOnTitleClick="true">
                <div class="leftarea">
                    <input style="width:100%;" id="contractId" name="contractId" class="mini-textbox mini-mustFill"
                           required="true" labelField="true" requiredErrorText="该输入项为必输项" label="成交单编号："
                           labelStyle="text-align:left;width:130px;" vtype="maxLength:20"
                           onvalidation="onEnglishAndNumberValidation"/>
                </div>
                <div class="rightarea">
                    <input style="width:100%;" id="cfetsno" name="cfetsno" class="mini-textbox" label="CFETS机构码："
                           labelField="true" visible="false" enabled="false" labelStyle="text-align:left;width:130px;"/>
                </div>
            </div>
            <div class="mini-panel" title="成交双方信息" style="width:100%" allowResize="true" collapseOnTitleClick="true">
                <div class="leftarea">
                    <fieldset>
                        <legend>本方信息</legend>
                        <input id="myDir" name="myDir" class="mini-combobox mini-mustFill" labelField="true"
                               onvaluechanged="dirVerify" label="本方方向：" required="true" style="width:100%;"
                               labelStyle="text-align:left;width:120px;"
                               data="CommonUtil.serverData.dictionary.lending"/>
                        <input id="borrowInst" name="borrowInst" class="mini-textbox mini-mustFill" labelField="true"
                               label="机构：" style="width:100%;" required="true" enabled="false"
                               labelStyle="text-align:left;width:120px;"/>
                        <input id="borrowTrader" name="borrowTrader" class="mini-textbox mini-mustFill"
                               labelField="true" label="交易员：" vtype="maxLength:20" required="true" enabled="false"
                               labelStyle="text-align:left;width:120px;" style="width:100%;"/>
                        <!-- <input id="borrowTel" name="borrowTel" class="mini-textbox" labelField="true"  label="电话："  onvalidation="CommonUtil.onValidation(e,'phone',[null])" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
                        <input id="borrowFax" name="borrowFax" class="mini-textbox" labelField="true"  label="传真："   onvalidation="CommonUtil.onValidation(e,'phone',[null])" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
                        <input id="borrowCorp" name="borrowCorp" class="mini-textbox" labelField="true"  label="法人代表：" vtype="maxLength:5" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
                        <input id="borrowAddr" name="borrowAddr" class="mini-textbox" labelField="true"  label="地址："   vtype="maxLength:50" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
                     -->
                    </fieldset>
                </div>
                <div class="rightarea">
                    <fieldset>
                        <legend>对手方信息</legend>
                        <input id="oppoDir" name="oppoDir" class="mini-combobox mini-mustFill" enabled="false"
                               labelField="true" label="对方方向：" required="true" style="width:100%;"
                               labelStyle="text-align:left;width:120px;"
                               data="CommonUtil.serverData.dictionary.lending"/>
                        <input id="removeInst" name="removeInst" class="mini-buttonedit mini-mustFill"
                               onbuttonclick="onButtonEdit" labelField="true" required="true" label="机构："
                               allowInput="false" style="width:100%;" labelStyle="text-align:left;width:120px;"/>
                        <input id="removeTrader" name="removeTrader" class="mini-textbox mini-mustFill"
                               labelField="true" label="交易员：" vtype="maxLength:20"
                               labelStyle="text-align:left;width:120px;" style="width:100%;" required="true"/>
                        <!-- <input id="removeTel" name="removeTel" class="mini-textbox" labelField="true"  label="电话："  onvalidation="CommonUtil.onValidation(e,'phone',[null])" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
                        <input id="removeFax" name="removeFax" class="mini-textbox" labelField="true"  label="传真："  onvalidation="CommonUtil.onValidation(e,'phone',[null])" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
                        <input id="removeCorp" name="removeCorp" class="mini-textbox" labelField="true"  label="法人代表：" vtype="maxLength:5" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
                        <input id="removeAddr" name="removeAddr" class="mini-textbox" labelField="true"  label="地址：" vtype="maxLength:50" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
                     -->
                    </fieldset>
                </div>
            </div>
            <div class="mini-panel" title="交易主体信息" style="width:100%" allowResize="true" collapseOnTitleClick="true">
                <div class="leftarea">
                    <input style="width:100%;" id="forDate" name="forDate" class="mini-datepicker mini-mustFill"
                           labelField="true" required="true" label="成交日期：" labelStyle="text-align:left;width:130px;"/>
                    <input style="width:100%;" id="ccy" name="ccy" class="mini-combobox mini-mustFill"
                           labelField="true" data="CommonUtil.serverData.dictionary.Currency" label="币种："
                           labelStyle="text-align:left;width:130px;" required="true" allowInput="false"/>
                    <input style="width:100%" id="amt" name="amt" class="mini-spinner mini-mustFill input-text-strong"
                           labelField="true" label="拆借金额(万元)：" required="true" onvaluechanged="associaDataCalculation"
                           required="true" minValue="0" maxValue="9999999999.9999" format="n4"
                           changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"
                           onvalidation="zeroValidation"/>
                    <input style="width:100%" id="firstSettlementDate" name="firstSettlementDate"
                           class="mini-datepicker mini-mustFill" labelField="true" required="true" label="首次结算日："
                           onvaluechanged="associaDataCalculation" ondrawdate="onDrawDateFirst"
                           labelStyle="text-align:left;width:130px;"/>
                    <input style="width:100%" id="tenor" name="tenor" class="mini-spinner mini-mustFill"
                           labelField="true" label="拆借期限(天)：" minValue="0" required="true" maxValue="99999"
                           changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"/>
                    <input style="width:100%" id="accuredInterest" name="accuredInterest"
                           class="mini-spinner mini-mustFill input-text-strong" labelField="true" required="true"
                           label="应计利息(元)：" minValue="0" maxValue="99999999999999.9999" format="n4"
                           changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"/>

                    <input id="basis" name="basis" class="mini-combobox mini-mustFill" required="true" labelField="true"
                           label="计息基准：" style="width:100%;" data="CommonUtil.serverData.dictionary.Basis"
                           onvaluechanged="linkIntCalculat" vtype="maxLength:15"
                           labelStyle="text-align:left;width:130px;">
                    <input style="width:100%" id="tradingProduct" name="tradingProduct"
                           class="mini-combobox mini-mustFill" required="true" allowInput="false" labelField="true"
                           label="交易品种：" labelStyle="text-align:left;width:130px;"
                           data="CommonUtil.serverData.dictionary.dealVarietyCredit"
                           onvaluechanged="associaDataCalculation"/>
                    <input id="dealTransType" name="dealTransType" class="mini-combobox" labelField="true"
                           style="width:100%;" label="数据来源：" data="CommonUtil.serverData.dictionary.dealTransType1"
                           vtype="maxLength:10" labelStyle="text-align:left;width:130px;" value="1"/>
                </div>
                <div class="rightarea">
                    <input style="width:100%" id="rate" name="rate" class="mini-spinner mini-mustFill input-text-strong"
                           labelField="true" label="拆借利率(%)：" required="true" minValue="0" maxValue="99999.999999"
                           format="n6" onvaluechanged="associaDataCalculation" changeOnMousewheel="false"
                           labelStyle="text-align:left;width:130px;" onvalidation="zeroValidation"/>
                    <%--                        <input style="width:100%" id="spread8" name="spread8" class="mini-spinner  input-text-strong" labelField="true"  label="利率差(%)："   minValue="0" maxValue="99999.999999" format="n6" onvaluechanged="associaDataCalculation" changeOnMousewheel="false" labelStyle="text-align:left;width:130px;" onvalidation="zeroValidation"  />--%>
                    <input style="width:100%;" id="spread8" name="spread8" class="mini-spinner input-text-strong"
                           labelField="true" label="利率差(%)：" changeOnMousewheel='false' format="n6"  onvaluechanged="associaDataCalculation"
                           labelStyle="text-align:left;width:130px" minValue="-100" maxValue="1000000" value="0"/>
                    <input style="width:100%" id="secondSettlementDate" name="secondSettlementDate"
                           class="mini-datepicker mini-mustFill" labelField="true" required="true" label="到期结算日："
                           onvaluechanged="associaDataCalculation" ondrawdate="onDrawDateSecond"
                           labelStyle="text-align:left;width:130px;"/>
                    <input style="width:100%" id="occupancyDays" name="occupancyDays" class="mini-spinner mini-mustFill"
                           labelField="true" onvaluechanged="linkageCalculat" required="true" enabled="false"
                           label="实际占款天数(天)：" minValue="0" maxValue="99999" changeOnMousewheel="false"
                           labelStyle="text-align:left;width:130px;"/>
                    <input style="width:100%" id="settlementAmount" name="settlementAmount"
                           class="mini-spinner mini-mustFill input-text-strong" labelField="true" required="true"
                           label="到期还款金额(元)：" onvalidation="zeroValidation" minValue="0" maxValue="99999999999999.9999"
                           format="n4" changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"/>

                    <%--                            <input style="width:100%" id="lastIntPayDate" name="lastIntPayDate" class="mini-datepicker mini-mustFill" labelField="true" required="true"    label="最后一次付息日：" onvaluechanged="associaDataCalculation"  ondrawdate="onDrawDateSecond" labelStyle="text-align:left;width:130px;"   />--%>
                    <input id="rateCode" name="rateCode" class="mini-buttonedit mini-mustFill"
                           onbuttonclick="onRateEdit" labelField="true" label="利率代码：" style="width: 100%;"
                           labelStyle="text-align:left;width:130px;" allowInput="false" requiredErrorText="该输入项为必输项"
                           required="true"/>
                    <input id="nettingStatus" name="nettingStatus" class="mini-combobox  mini-mustFill" required="true"
                           labelField="true" label="净额清算状态：" labelStyle="text-align:left;width:130px;"
                           style="width:100%;" data="CommonUtil.serverData.dictionary.YesNo" showNullItem="true"/>

                    <input id="tradingModel" name="tradingModel" class="mini-combobox  mini-mustFill" required="true"
                           labelField="true" label="交易模式：" labelStyle="text-align:left;width:130px;" style="width:100%;"
                           data="CommonUtil.serverData.dictionary.TradeModel"/>
                    <input id="note" name="note" class="mini-textbox" labelField="true" label="交易事由："
                           labelStyle="text-align:left;width:130px;" style="width:100%;" vtype="maxLength:16"/>
                </div>
            </div>
            <div class="mini-panel" title="报表要素" style="width:100%;" allowResize="true" collapseOnTitleClick="false">
                <div class="leftarea">
                    <input id="attributionDept" name="attributionDept" valueField="id" showFolderCheckBox="true"
                           labelField="true" label="归属机构：" showCheckBox="true" style="width:100%"
                           labelStyle="text-align:left;width:130px"
                           showTreeLines="true" showTreeIcon="true" textField="instName" parentField="parentId"
                           class="mini-treeselect" expandOnLoad="true" resultAsTree="false"
                           valueFromSelect="true" emptyText="请选择机构"/>
                    <input id="accountNature" name="accountNature" class="mini-combobox" labelField="true"
                           label="账户性质：" labelStyle="text-align:left;width:130px;"
                           style="width:100%;" data="CommonUtil.serverData.dictionary.accountNature" value="1"/>
                    <input id="pricingStandardType" name="pricingStandardType" class="mini-textbox" labelField="true"
                           textField="text" valueField="id"
                           label="定价基准类型：" labelStyle="text-align:left;width:130px;"
                           style="width:100%;" enabled="false"/>
                    <input id="scheduleType" name="scheduleType" class="mini-combobox  mini-mustFill" required="true"
                           labelField="true" label="付息类型：" labelStyle="text-align:left;width:130px;" style="width:100%;"
                           data="CommonUtil.serverData.dictionary.scheduleType" value="D" onvaluechanged="selectCycle"/>
                    <%--                            <input style="width:100%" id="firstIPayDate" name="firstIPayDate" class="mini-datepicker mini-mustFill" labelField="true"  required="true"   label="首次付息日："  ondrawdate="onDrawDateFirst" labelStyle="text-align:left;width:130px;" onvaluechanged="getDayNumber" />--%>
                    <input style="width:100%" id="intPayDay" name="intPayDay" class="mini-spinner input-text-strong"
                           labelField="true" label="利息付款日："
                           labelStyle="text-align:left;width:130px;" onvaluechanged="getDayNumber" value="1"/>
                </div>
                <div class="rightarea">
                    <input id="fiveLevelClass" name="fiveLevelClass" class="mini-combobox" labelField="true"
                           requiredErrorText="该输入项为必输项" label="五级分类：" labelStyle="text-align:left;width:130px;"
                           style="width:100%;" data="CommonUtil.serverData.dictionary.FiveLevelClass" value="0"/>
                    <input style="width:100%;" id="accountUse" name="accountUse" class="mini-textbox" labelField="true"
                           label="账户用途：" labelStyle="text-align:left;width:130px;"/>
                    <input id="rateFloatFrequency" name="rateFloatFrequency" class="mini-combobox" labelField="true"
                           label="利率浮动频率：" labelStyle="text-align:left;width:130px;"
                           style="width:100%;" data="CommonUtil.serverData.dictionary.rateFloatFrequency"/>
                    <input id="intPayCycle" name="intPayCycle" class="mini-combobox " labelField="true" label="付息周期："
                           labelStyle="text-align:left;width:130px;" style="width:100%;"
                           data="CommonUtil.serverData.dictionary.intPayCycle" showNullItem="true"/>
                    <input id="intDateRule" name="intDateRule" class="mini-combobox" labelField="true"
                           required="true" label="利息日期规则：" labelStyle="text-align:left;width:130px;" style="width:100%;"
                           data="CommonUtil.serverData.dictionary.intPayRule" showNullItem="true"/>
                </div>
            </div>
            <%@ include file="../../Common/opicsLess.jsp" %>
            <%@ include file="../../Common/RiskCenter.jsp" %>
            <%@ include file="../../Common/custLimit.jsp" %>
            <div class="mini-panel" title="清算信息" style="width:100%" allowResize="true" collapseOnTitleClick="true">
                <div class="leftarea">
                    <fieldset>
                        <legend>本方账户</legend>
                        <input id="borrowAccname" name="borrowAccname" class="mini-textbox" labelField="true"
                               label="资金账户户名：" vtype="maxLength:1333" style="width:100%;"
                               labelStyle="text-align:left;width:120px;"/>
                        <input id="borrowOpenBank" name="borrowOpenBank" class="mini-textbox" labelField="true"
                               label="资金开户行：" vtype="maxLength:1333" labelStyle="text-align:left;width:120px;"
                               style="width:100%;"/>
                        <input id="borrowAccnum" name="borrowAccnum" class="mini-textbox" labelField="true"
                               label="资金账号：" onvalidation="onEnglishAndNumberValidation" vtype="maxLength:400"
                               labelStyle="text-align:left;width:120px;" style="width:100%;"/>
                        <input id="borrowPsnum" name="borrowPsnum" class="mini-textbox" labelField="true"
                               label="支付系统行号：" onvalidation="onEnglishAndNumberValidation" vtype="maxLength:400"
                               labelStyle="text-align:left;width:120px;" style="width:100%;"/>
                    </fieldset>
                </div>
                <div class="rightarea">
                    <fieldset>
                        <legend>对手方账户</legend>
                        <input id="removeAccname" name="removeAccname" class="mini-textbox" labelField="true"
                               label="资金账户户名：" vtype="maxLength:1333" style="width:100%;"
                               labelStyle="text-align:left;width:120px;"/>
                        <input id="removeOpenBank" name="removeOpenBank" class="mini-textbox" labelField="true"
                               label="资金开户行：" vtype="maxLength:1333" labelStyle="text-align:left;width:120px;"
                               style="width:100%;"/>
                        <input id="removeAccnum" name="removeAccnum" class="mini-textbox" labelField="true"
                               label="资金账号：" onvalidation="onEnglishAndNumberValidation" vtype="maxLength:400"
                               labelStyle="text-align:left;width:120px;" style="width:100%;"/>
                        <input id="removePsnum" name="removePsnum" class="mini-textbox" labelField="true"
                               label="支付系统行号：" onvalidation="onEnglishAndNumberValidation" vtype="maxLength:400"
                               labelStyle="text-align:left;width:120px;" style="width:100%;"/>
                    </fieldset>
                </div>
            </div>
            <%@ include file="../../Common/Flow/MiniApproveOpCommon.jsp" %>
        </div>
    </div>
    <div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
        <div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"
                                                                style="width:120px;" id="save_btn"
                                                                onclick="save">保存交易</a></div>
        <div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"
                                                                style="width:120px;" id="close_btn"
                                                                onclick="close">关闭界面</a></div>
    </div>
</div>
<script type="text/javascript">
    mini.parse();

    //获取当前tab
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var row = params.selectData;
    var detailData = row;
    var url = window.location.search;
    var action = CommonUtil.getParam(url, "action");
    var form = new mini.Form("#field_form");
    mini.get("applyNo").setVisible(false);//信用风险审查单号：
    mini.get("applyProd").setVisible(false);//产品名称
    mini.get("priceDeviation").setVisible(true);//偏离度
    var ticketId = CommonUtil.getParam(url, "ticketid");
    var prdNo = CommonUtil.getParam(url, "prdNo");
    var fPrdCode = CommonUtil.getParam(url, "fPrdCode");
    var prdName = CommonUtil.getParam(url, "prdName");
    var dealType = CommonUtil.getParam(url, "dealType");
    var tradeData = {};

    tradeData.selectData = row;
    tradeData.operType = action;
    tradeData.serial_no = row.ticketId;
    tradeData.task_id = row.taskId;

    function onDateRenderer(e) {
        var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value) == null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g, '/'));
        if (value) return mini.formatDate(value, 'yyyy-MM-dd');
    }

    //保存
    function save() {
        //表单验证！！！
        form.validate();
        if (form.isValid() == false) {
            mini.alert("信息填写有误，请重新填写", "系统提示");
            return;
        }
        var data = form.getData(true);
        console.log(data)
        if (toDecimal(data['accuredInterest']) != toDecimal(data['amt'] * 10000 * (data['rate'] + data['spread8']) * data['occupancyDays'] / 100) || data['tenor'] != data['occupancyDays']
            || toDecimal(data['settlementAmount']) != parseFloat(toDecimal(data['amt'] * 10000)) + parseFloat(toDecimal(data['accuredInterest'])) != "0.00") {
            mini.confirm("确认以当前数据为准吗？", "确认", function (action) {
                if (action != "ok") {
                    return;
                }
                data['borrowInst'] = "<%=__sessionInstitution.getInstId()%>";
                data['sponInst'] = "<%=__sessionInstitution.getInstId()%>";
                data['sponsor'] = "<%=__sessionUser.getUserId() %>";
//              data['dealTransType']="M";
                data['borrowTrader'] = "<%=__sessionUser.getUserId() %>";
                data['fPrdCode'] = fPrdCode;
                var params = mini.encode(data);
                CommonUtil.ajax({
                    url: "/IfsCfetsrmbIboController/saveIbo",
                    data: params,
                    callback: function (data) {
                        mini.alert(data.desc, '提示信息', function () {
                            if (data.code == 'error.common.0000') {
                                top["win"].closeMenuTab();//关闭并刷新当前界面
                            }
                        });
                    }
                });
            });
        } else {
            data['borrowInst'] = "<%=__sessionInstitution.getInstId()%>";
            data['sponInst'] = "<%=__sessionInstitution.getInstId()%>";
            data['sponsor'] = "<%=__sessionUser.getUserId() %>";
//          data['dealTransType']="M";
            data['borrowTrader'] = "<%=__sessionUser.getUserId() %>";
            data['fPrdCode'] = fPrdCode;
            var params = mini.encode(data);
            CommonUtil.ajax({
                url: "/IfsCfetsrmbIboController/saveIbo",
                data: params,
                callback: function (data) {
                    mini.alert(data.desc, '提示信息', function () {
                        if (data.code == 'error.common.0000') {
                            top["win"].closeMenuTab();//关闭并刷新当前界面
                        }
                    });
                }
            });
        }
    }

    function close() {
        top["win"].closeMenuTab();
    }

    /**
     *   获取机构
     */
    function loadInstitutionTree() {
        CommonUtil.ajax({
            data: {"branchId": branchId},
            url: "/InstitutionController/searchInstitutions",
            callback: function (data) {
                mini.get("attributionDept").setData(data.obj);
                if (detailData != null && detailData.attributionDept != null) {
                    mini.get("attributionDept").setValue(detailData.attributionDept);
                } else {
                    mini.get("attributionDept").setValue("<%=__sessionInstitution.getInstId()%>");
                }
            }
        });
    }

    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function (visibleBtn) {
            if ($.inArray(action, ["approve", "detail"]) > -1) {
                mini.get("save_btn").setVisible(false);
                form.setEnabled(false);
                var approForm = new mini.Form("#approve_operate_form");
                approForm.setEnabled(true);
                mini.get("forDate").setEnabled(false);
            }
            if ($.inArray(action, ["edit", "approve", "detail"]) > -1) {
                form.setData(row);
                mini.get("forDate").setValue(row.forDate);
                mini.get("ticketId").setValue(row.ticketId);
                //投资组合
                mini.get("port").setValue(row.port);
                mini.get("port").setText(row.port);
                //成本中心
                mini.get("cost").setValue(row.cost);
                mini.get("cost").setText(row.cost);
                //产品代码
                mini.get("product").setValue(row.product);
                mini.get("product").setText(row.product);
                //产品类型
                mini.get("prodType").setValue(row.prodType);
                mini.get("prodType").setText(row.prodType);
                //对方机构removeInst
                mini.get("removeInst").setValue(row.cno);
                queryCustNo(row.custNo);
//          mini.get("removeInst").setText(row.removeInst);
                queryTextName(row.cno);

                mini.get("borrowInst").setValue("<%=__sessionInstitution.getInstFullname()%>");
                mini.get("borrowTrader").setValue(row.myUserName);
                mini.get("contractId").setEnabled(false);

                //清算路径
                mini.get("ccysmeans").setValue(row.ccysmeans);
                mini.get("ccysmeans").setText(row.ccysmeans);
                mini.get("ctrsmeans").setValue(row.ctrsmeans);
                mini.get("ctrsmeans").setText(row.ctrsmeans);

                mini.get("rateCode").setValue(row.rateCode);
                mini.get("rateCode").setText(row.rateCode);

                //风险中台
                mini.get("applyNo").setValue(row.applyNo);
                mini.get("applyNo").setText(row.applyNo);

                //付息周期
                selectCycle();
                //searchProductWeightNew();//显示权重
                if (action == "edit" && row.weight == null) {
                    searchProductWeightNew();
                }
                if (action == "edit") {//偏离度
                    if (CommonUtil.isNull(row.priceDeviation)) {
                        mini.get("tradingProduct").setValue(row.tradingProduct);
                        mini.get("tradingProduct").setText(row.tradingProduct);
                        mini.get("rate").setText(row.rate);
                        mini.get("rate").setValue(row.rate);
                        searchPriceDeviation();
                    }
                }
                dirVerify();

                //买入出钱设置授信主体
                if (row.myDir == 'S') {
                    creditsubvalue(row.custNo, "544", null);
                }

            } else {
                mini.get("sponsor").setValue("<%=__sessionUser.getUserId()%>");
                mini.get("sponInst").setValue("<%=__sessionUser.getInstId() %>");
                mini.get("borrowInst").setValue("<%=__sessionInstitution.getInstFullname()%>");
                mini.get("borrowTrader").setValue("<%=__sessionUser.getUserName() %>");
                mini.get("forDate").setValue("<%=__bizDate %>");
                mini.get("basis").select(0);
                mini.get("product").setValue("MM");
                mini.get("product").setText("MM");
                //付息周期
                selectCycle();

                getDayNumber();
            }
            loadInstitutionTree();
        });
    });

    //加载机构树
    function nodeclick(e) {

        var oldvalue = e.sender.value;
        var param = mini.encode({"branchId": branchId}); //序列化成JSON
        CommonUtil.ajax({
            url: "/InstitutionController/searchInstitutionByBranchId",
            data: param,
            callback: function (data) {
                var obj = e.sender;
                obj.setData(data.obj);
                obj.setValue(oldvalue);

            }
        });
    }

    /**加载付息周期**/
    function selectCycle() {
        var param = mini.get("scheduleType").getValue();
        if (param == "D") {
            mini.get("intPayCycle").setEnabled(false)
            mini.get("intPayDay").setEnabled(false)
            mini.get("intDateRule").setEnabled(false)
            mini.get("intPayCycle").setValue("")
            mini.get("intPayDay").setValue("")
        } else if (param == "IR") {
            mini.get("intPayCycle").setEnabled(true)
            mini.get("intPayDay").setEnabled(true)
            mini.get("intDateRule").setEnabled(true)
        }
    }

    /**获取时间的day**/
    function getDayNumber() {
        if (mini.get("scheduleType").getValue() == "IR") {
            var dayNumber = mini.get("intPayDay").getValue();
            if (dayNumber < 1 || dayNumber > 31) {
                mini.alert("请输入日期为1号到31号之间的值");
                mini.get("intPayDay").setValue(1)

            }
        }
    }

    /**获取时间的day**/
    // function getDayNumber(){
    //     var firstDate = mini.get("firstIPayDate").getValue();
    //     var test=mini.formatDate(firstDate, 'dd');
    //     mini.get("intPayDay").setValue(test)
    // }

    //格式化日期格式
    function ondayRenderer(e) {
        if (e.value != "合计") {
            var value = new Date(e.value);
            if (value) return mini.formatDate(value, 'dd');
        } else {
            return e.value;
        }
    }

    //利率代码的选择
    function onRateEdit() {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "/opics/rateMini.jsp",
            title: "选择利率代码",
            width: 700,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        debugger;
                        mini.get("pricingStandardType").setValue(data.ratetype);
                        mini.get("rateFloatFrequency").setText("");
                        if ("FIXED" == data.ratetype) {
                            mini.get("rateFloatFrequency").setEnabled(false);
                        } else {
                            mini.get("rateFloatFrequency").setEnabled(true);
                        }
                        btnEdit.setValue(data.ratecode);
                        btnEdit.setText(data.ratecode);
                        btnEdit.focus();
                    }
                }

            }
        });
    }

    //文、数字、下划线 的验证
    function onEnglishAndNumberValidations(e) {
        if (e.value == "" || e.value == null) {//值为空，就不做校验
            return;
        }
        if (e.isValid) {
            if (isEnglishAndNumbers(e.value) == false) {
                e.errorText = "必须输入英文小写+数字";
                e.isValid = false;
            }
        }
    }

    /* 是否英文+数字 */
    function isEnglishAndNumbers(v) {
        var re = new RegExp("^[0-9a-z\_\-]+$");
        if (re.test(v)) return true;
        return false;
    }

    //英文、数字、下划线 的验证
    function onEnglishAndNumberValidation(e) {
        if (e.value == "" || e.value == null) {//值为空，就不做校验
            return;
        }
        if (e.isValid) {
            if (isEnglishAndNumber(e.value) == false) {
                e.errorText = "必须输入英文+数字";
                e.isValid = false;
            }
        }
    }

    /* 是否英文+数字 */
    function isEnglishAndNumber(v) {
        var re = new RegExp("^[0-9a-zA-Z\_\-]+$");
        if (re.test(v)) return true;
        return false;
    }

    function onButtonEdit(e) {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Common/CustMini.jsp",
            title: "选择对手方列表",
            width: 900,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.cno);
                        btnEdit.setText(data.cliname);
                        queryCustNo(data.creditsub);

                        //拆出出钱设置授信主体
                        var myDir = mini.get("myDir").getValue();
                        if (myDir == 'S') {
                            creditsubvalue(data.cno, "544", null);
                        }

                        btnEdit.focus();
                    }
                }

            }
        });
    }

    function onCreditTypeChange() {
        var cno = mini.get("removeInst").getValue();
        creditsubvalue(cno, "544", null);
    }

    //根据交易对手编号查询全称
    function queryTextName(cno) {
        CommonUtil.ajax({
            url: "/IfsOpicsCustController/searchIfsOpicsCust",
            data: {'cno': cno},
            callback: function (data) {
                if (data && data.obj) {
                    mini.get("removeInst").setText(data.obj.cliname);
                } else {
                    mini.get("removeInst").setText(cno);
                }
            }
        });
    }

    function onDrawDateFirst(e) {
        var firstDate = e.date;
        var secondDate = mini.get("secondSettlementDate").getValue();
        if (CommonUtil.isNull(secondDate)) {
            return;
        }
        if (secondDate.getTime() < firstDate.getTime()) {
            e.allowSelect = false;
        }
    }

    function onDrawDateSecond(e) {
        var secondDate = e.date;
        var firstDate = mini.get("firstSettlementDate").getValue();
        if (CommonUtil.isNull(firstDate)) {
            return;
        }
        if (secondDate.getTime() < firstDate.getTime()) {
            e.allowSelect = false;
        }
    }

    function associaDataCalculation(e) {
        var firstDate = mini.get("firstSettlementDate").getValue();
        var secondDate = mini.get("secondSettlementDate").getValue();
        if (CommonUtil.isNull(firstDate) || CommonUtil.isNull(secondDate)) {
            return;
        }
        var subDate = Math.abs(parseInt((secondDate.getTime() - firstDate.getTime()) / 1000 / 3600 / 24));
        mini.get("tenor").setValue(subDate);
        mini.get("occupancyDays").setValue(subDate);
        var occupancyDays = mini.get("occupancyDays").getValue();
        var amt = mini.get("amt").getValue();
        var rate = mini.get("rate").getValue();
        var spread8 = mini.get("spread8").getValue();
        var basis = mini.get("basis").getValue();
        var calDays = basisTonumber(basis);
        mini.get("accuredInterest").setValue(amt * 10000 * (rate + spread8) * occupancyDays / 100 / calDays);
        mini.get("settlementAmount").setValue(amt * 10000 + amt * 10000 * (rate + spread8) * occupancyDays / 100 / calDays);
        searchProductWeightNew();

        searchPriceDeviation();
    }

    //实际占款天数点击事件
    function linkageCalculat(e) {
        var occupancyDays = e.value;
        var amt = mini.get("amt").getValue();
        var rate = mini.get("rate").getValue();
        var spread8 = mini.get("spread8").getValue();
        var basis = mini.get("basis").getValue();
        var calDays = basisTonumber(basis);
        mini.get("accuredInterest").setValue(amt * 10000 * (rate + spread8) * occupancyDays / 100 / calDays);
        mini.get("settlementAmount").setValue(amt * 10000 + amt * 10000 * (rate + spread8) * occupancyDays / 100 / calDays);
    }

    //计息基准点击事件
    function linkIntCalculat(e) {
        var basis = e.value;
        var occupancyDays = mini.get("occupancyDays").getValue();
        var amt = mini.get("amt").getValue();
        var rate = mini.get("rate").getValue();
        var spread8 = mini.get("spread8").getValue();
        var calDays = basisTonumber(basis);
        mini.get("accuredInterest").setValue(amt * 10000 * (rate + spread8) * occupancyDays / 100 / calDays);
        mini.get("settlementAmount").setValue(amt * 10000 + amt * 10000 * (rate + spread8) * occupancyDays / 100 / calDays);
    }


    /**   清算路径1的选择   */
    function settleMeans1() {
        var cpInstId = mini.get("removeInst").getValue();
        if (cpInstId == null || cpInstId == "") {
            mini.alert("请先选择对方机构!");
            return;
        }
        var ccy = "CNY";
        var url = "./Ifs/opics/setaMini.jsp";
        var data = {ccy: ccy, cust: cpInstId};

        var btnEdit = this;
        mini.open({
            url: url,
            title: "选择清算路径",
            width: 700,
            height: 600,
            onload: function () {
                var iframe = this.getIFrameEl();
                iframe.contentWindow.SetData(data);
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data1 = iframe.contentWindow.GetData();
                    data1 = mini.clone(data1);    //必须
                    if (data1) {
                        mini.get("ccysmeans").setValue($.trim(data1.smeans));
                        mini.get("ccysmeans").setText($.trim(data1.smeans));
                        mini.get("ccysacct").setValue($.trim(data1.sacct));
                        btnEdit.focus();
                    }
                }
            }
        });
    }

    /**   清算路径2的选择   */
    function settleMeans2() {
        var cpInstId = mini.get("removeInst").getValue();
        if (cpInstId == null || cpInstId == "") {
            mini.alert("请先选择对方机构!");
            return;
        }
        var ccy = "CNY";
        var url = "./Ifs/opics/setaMini.jsp";
        var data = {ccy: ccy, cust: cpInstId};

        var btnEdit = this;
        mini.open({
            url: url,
            title: "选择清算路径",
            width: 700,
            height: 600,
            onload: function () {
                var iframe = this.getIFrameEl();
                iframe.contentWindow.SetData(data);
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data1 = iframe.contentWindow.GetData();
                    data1 = mini.clone(data1);    //必须
                    if (data1) {
                        mini.get("ctrsmeans").setValue($.trim(data1.smeans));
                        mini.get("ctrsmeans").setText($.trim(data1.smeans));
                        mini.get("ctrsacct").setValue($.trim(data1.sacct));
                        btnEdit.focus();
                    }
                }

            }
        });
    }

    function dirVerify(e) {
        var myDir = mini.get("myDir").getValue();
        if (myDir == 'S') {//拆出
            mini.get("panel1").setVisible(true);
            mini.get("oppoDir").setValue("P");
            mini.get("custNo").setVisible(true);//占用授信主体
            mini.get("weight").setVisible(true);//权重

            var cno = mini.get("removeInst").getValue();
            creditsubvalue(cno, "544", null);
        } else if (myDir == 'P') {//拆入
            mini.get("panel1").setVisible(false);
            mini.get("oppoDir").setValue("S");
            mini.get("custNo").setVisible(false);//占用授信主体
            mini.get("weight").setVisible(false);//权重
            //mini.get("weight").setValue("0");//权重
            //mini.get("weight").setText("0");//权重
        }
    }

    function searchProductWeightNew() {
        var form = new mini.Form("field_form");
        var data = form.getData(true);
        var mDate = data.secondSettlementDate;
        var vDate = data.forDate;//交易日期
        if (CommonUtil.isNull(mDate) || CommonUtil.isNull(vDate)) {
            return;
        }
        searchProductWeight(20, 0, prdNo, mDate);
    }

    function searchPriceDeviation() {
        var tradingProduct = mini.get("tradingProduct").getValue();//交易品种
        var rate = mini.get("rate").getValue();
        if (CommonUtil.isNull(tradingProduct) || CommonUtil.isNull(rate)) {
            return;
        }
        CommonUtil.ajax({
            url: "/IfsLimitController/queryRhisIntrate",
            data: {'prdNo': prdNo, 'rate': rate, 'tradingProduct': tradingProduct},
            callback: function (data) {
                if (data != null && data != "") {
                    mini.get("priceDeviation").setValue(data);
                } else {
                    mini.get("priceDeviation").setValue();
                }
            }
        });
    }

</script>
<script type="text/javascript" src="../../Common/Flow/MiniApproveOpCommon.js"></script>
</body>
</html>
