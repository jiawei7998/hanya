<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
<script type="text/javascript" >
	/**prdCode用于页面投资组合的选择   该全局变量必须为prdCode*/
	var prdCode="SWAP";
</script>
<title>结构衍生</title>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
	<legend>结构衍生成交单查询</legend>	
	<div>
		<div id="search_form" style="width:100%" cols="6">
			<input id="contractId" name="contractId" class="mini-textbox" labelField="true"  label="成交单编号："  width="280px"  labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请输入成交单编号" />
			<input id="forDate" name="forDate" class="mini-datepicker" labelField="true"  label="成交日期："  value="<%=__bizDate%>"  width="280px"  labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请输入成交日期" />
			<input id="approveStatus" name="approveStatus" class="mini-combobox" data="CommonUtil.serverData.dictionary.ApproveSta" width="280px" emptyText="请选择审批状态" labelField="true"  label="审批状态：" labelStyle="text-align:right;"/>
			<span style="float: right; margin-right: 150px"> 
				<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
			</span>
		</div>
	</div>
</fieldset> 
	<%@ include file="../batchReback.jsp"%>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script> 
<div class="mini-fit" >
	<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:70%;" idField="id"  allowAlternating="true"
		onrowdblclick="onRowDblClick" allowResize="true" sortMode="client" allowAlternating="true" multiSelect="true">
		<div property="columns">
			<div type="checkcolumn"></div>
			<div type="indexcolumn" width="50px" headerAlign="center">序号</div>
			<div field="prdNo" width="100" allowSort="false" headerAlign="center" align="center" visible="false"></div>
			<div field="ticketId" width="200" allowSort="false" headerAlign="center" align="center">审批单号</div>
			<div field="contractId" width="180px" align="center"  headerAlign="center" >成交单号</div>
			<div field="dealNo" width="100px" align="center"  headerAlign="center" >opics单号</div>
			<div field="myDir" width="100px" align="center" headerAlign="center"  renderer="CommonUtil.dictRenderer" data-options="{dict:'irsing'}">本方方向</div>
			<div field="floatInst" width="150px" align="center"  headerAlign="center" >对手方机构名称</div>
			<div field="lastQty" width="150px" align="right"  headerAlign="center" allowSort="true" numberFormat="n4">名义本金(万元)</div>    
			<div field="rateCode" width="100px" align="center"  headerAlign="center" >利率代码</div>
			<div field="startDate" width="90px" align="center"  headerAlign="center" allowSort="true">起息日</div>
			<div field="endDate" width="90px" align="center"  headerAlign="center" allowSort="true">到期日</div>
			
			<div field="productName" width="100px" align="left"  headerAlign="center" >产品名称</div>
			<div field="fixedInst" width="180px" align="center"  headerAlign="center" >固定利率支付方机构</div>                            
			<div field="calculateAgency" width="110px"   headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'calInst'}">计算机构</div>
			<div field="tradeMethod" width="100px" align="center"  headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'CleanMethoed'}">清算方式</div>
			<div field="forDate" width="90px" align="center"  headerAlign="center" allowSort="true">成交日期</div>
			<div field="approveStatus" width="90" align="center" headerAlign="center"  renderer="CommonUtil.dictRenderer" data-options="{dict:'ApproveStatus'}">审批状态</div>
			<div field="dealTransType" width="90" align="center" headerAlign="center"  renderer="CommonUtil.dictRenderer" data-options="{dict:'dealTransType'}">交易状态</div>
			<div field="sponsor" width="100" allowSort="false" headerAlign="center" align="center">审批发起人</div>
			<div field="sponInst" width="120" allowSort="false" headerAlign="center" align="center">审批发起机构</div>
			<div field="aDate" width="130" allowSort="false" headerAlign="center" align="center">审批发起时间</div>
			
		</div>
	</div>
	
	<fieldset style="width: 100%;border:solid 1px #aaa;margin-top:8px;position:relative;">
		<legend>结构衍生详情</legend>
			<div id="MiniSettleForeigDetail" style="padding:5px;" class="mini-fit area" tyle="width: 100%; height: 50%;" allowAlternating="true" allowResize="true" border="true" sortMode="client">
			<input class="mini-hidden" name="id"/>
			<div class="mini-panel" title="成交双方信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
					<div class="leftarea">
					<fieldset>
					<legend>本方信息</legend>
						<input id="myDir" name="myDir" class="mini-combobox"  labelField="true"  label="本方方向：" required="true"  onvaluechanged="dirVerify"  style="width:100%;"  labelStyle="text-align:left;width:130px;" data = "CommonUtil.serverData.dictionary.irsing" />
						<input id="fixedInst" name="fixedInst" class="mini-textbox"  labelField="true"  label="机构："  required="true"  style="width:100%;" enabled="false" labelStyle="text-align:left;width:130px;"/>
						<input id="fixedTrader" name="fixedTrader" class="mini-textbox" labelField="true"  label="交易员：" required="true"   vtype="maxLength:5" enabled="false" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
					</fieldset>
					</div>			
					<div class="leftarea">
					<fieldset>
					<legend>对手方信息</legend>
						<input id="oppoDir" name="oppoDir" class="mini-combobox"  labelField="true"  label="对方方向："  required="true"  enabled="false" style="width:100%;"  labelStyle="text-align:left;width:130px;" data = "CommonUtil.serverData.dictionary.irsing" />
						<input id="floatInst" name="floatInst" class="mini-textbox" labelField="true"  label="机构："  required="true"  style="width:100%;"  labelStyle="text-align:left;width:130px;" />
						<input id="floatTrader" name="floatTrader" class="mini-textbox" labelField="true"  label="交易员："  required="true"  vtype="maxLength:5"  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
					</fieldset>
				</div>	
			</div>
			<div class="mini-panel" title="交易主体信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
				<div class="leftarea" >
					<input style="width:100%;" id="forDate" name="forDate" class="mini-datepicker  mini-mustFill" labelField="true" required="true"  label="成交日期：" labelStyle="text-align:left;width:130px;" />
					<input style="width:100%" id="productName" name="productName" class="mini-textbox mini-mustFill" onbuttonclick="onPrdNameEdit" labelField="true"  label="产品名称："  required="true"  allowInput="false" labelStyle="text-align:left;width:130px;" />
					<input style="width:100%" id="startDate" name="startDate" class="mini-datepicker mini-mustFill" labelField="true"  label="起息日：" required="true"  ondrawdate="onDrawDateStart" onvaluechanged="onStartDateChanged"  labelStyle="text-align:left;width:130px;" />
					<input style="width:100%" id="calculateAgency" name="calculateAgency" class="mini-combobox mini-mustFill" labelField="true"  label="计算机构：" required="true"    labelStyle="text-align:left;width:130px;" data = "CommonUtil.serverData.dictionary.calInst" />
					<input style="width:100%" id="interestRateAdjustment" name="interestRateAdjustment" class="mini-combobox mini-mustFill" labelField="true"  label="计息天数调整：" required="true"   labelStyle="text-align:left;width:130px;"  data = "CommonUtil.serverData.dictionary.daysChange" />
					<input style="width:100%" id="couponPaymentDateReset" name="couponPaymentDateReset" class="mini-combobox mini-mustFill" labelField="true"  label="支付日调整："  required="true"   labelStyle="text-align:left;width:130px;"  data = "CommonUtil.serverData.dictionary.payChange" />
					<input style="width:100%" id="accountingMethod" name="accountingMethod" class="mini-combobox mini-mustFill" labelField="true" label="交易类型："  labelStyle="text-align:left;width:130px;"  vtype="maxLength:20" data="CommonUtil.serverData.dictionary.acctMethod" requiredErrorText="该输入项为必输项" required="true" />
					<input id="nettingStatus" name="nettingStatus" class="mini-combobox mini-mustFill" required="true"  labelField="true"   label="净额清算状态：" labelStyle="text-align:left;width:130px;" style="width:100%;" data="CommonUtil.serverData.dictionary.YesNo" showNullItem="true"/>
				</div>
				<div class="leftarea" >
					<input style="width:100%" id="ccy" name="ccy" class="mini-combobox mini-mustFill" labelField="true" label="交易币种："  labelStyle="text-align:left;width:130px;"  vtype="maxLength:20" data="CommonUtil.serverData.dictionary.Currency" requiredErrorText="该输入项为必输项" required="true" />
					<input style="width:100%" id="lastQty" name="lastQty" class="mini-spinner mini-mustFill" minValue="0" maxValue="9999999999999999.9999" format="n4" changeOnMousewheel="false" labelField="true"  label="名义本金(万元)："  required="true"   labelStyle="text-align:left;width:130px;" onvalidation="zeroValidation"  />
					<input style="width:100%" id="endDate" name="endDate" class="mini-datepicker mini-mustFill" labelField="true"  label="到期日："  required="true"  ondrawdate="onDrawDateEnd"  labelStyle="text-align:left;width:130px;"  onvaluechanged = "searchProductWeightNew()" />
					<input style="width:100%" id="tenor" name="tenor" class="mini-combobox mini-mustFill" labelField="true"  label="期限："  required="true"   labelStyle="text-align:left;width:130px;" data = "CommonUtil.serverData.dictionary.tenor"/>
					<input style="width:100%" id="firstPeriodStartDate" name="firstPeriodStartDate" class="mini-datepicker mini-mustFill" labelField="true"  label="首期起息日："  required="true"  labelStyle="text-align:left;width:130px;"   />
					<input style="width:100%" id="tradeMethod" name="tradeMethod" class="mini-combobox mini-mustFill" labelField="true"  label="清算方式："  required="true"  labelStyle="text-align:left;width:130px;"   data = "CommonUtil.serverData.dictionary.CleanMethoed"/>
					<input style="width:100%" id="fixedFloatDir" name="fixedFloatDir" class="mini-combobox mini-mustFill" labelField="true" label="固定/浮动方向："  labelStyle="text-align:left;width:130px;"  required="true"  data="CommonUtil.serverData.dictionary.fixFltDir" enabled="false" />
					<input id="tradingModel" name="tradingModel" class="mini-combobox mini-mustFill" required="true"  labelField="true"   label="交易模式："  labelStyle="text-align:left;width:130px;" style="width:100%;"  data="CommonUtil.serverData.dictionary.TradeModel"/>
				</div>
			</div>
			<div class="mini-panel" title="交易明细信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
				<div class="leftarea" >
					<fieldset >
						<legend>利率明细</legend>
						<input id="fixedFloatL" name="fixedFloatL" class="mini-combobox mini-mustFill" labelField="true"  label="固定/浮动：" data = "CommonUtil.serverData.dictionary.fixfloat"  labelStyle="text-align:left;width:130px;" style="width:100%;"  required="true"  onvalidation="onFixFloatDirValidation"/>
						<input id="legPrice" name="legPrice" class="mini-spinner" minValue="0" maxValue="9999999999999999.99999999" format="n8" changeOnMousewheel="false" labelField="true"  label="利率(%)："  style="width:100%;"  labelStyle="text-align:left;width:130px;" />
						<input id="benchmarkSpreadL" name="benchmarkSpreadL" class="mini-spinner" minValue="0" maxValue="999.9999" format="n4" changeOnMousewheel="false"  labelField="true"  label="利差(bps)："  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						<input id="fixedPaymentDate" name="fixedPaymentDate" class="mini-datepicker mini-mustFill" labelField="true"  label="首期定期支付日："  labelStyle="text-align:left;width:130px;" style="width:100%;" required="true"   />
						<input id="fixedPaymentFrequency" name="fixedPaymentFrequency" class="mini-combobox mini-mustFill" required="true"  labelField="true"  label="支付周期：" data = "CommonUtil.serverData.dictionary.Intpaycircle"  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						<input id="legInterestAccrualDateL" name="legInterestAccrualDateL" class="mini-datepicker mini-mustFill" labelField="true"  label="首次利率确定日："  required="true"  style="width:100%;"  labelStyle="text-align:left;width:130px;" />
						<input id="interestResetFrequencyL" name="interestResetFrequencyL" class="mini-combobox mini-mustFill" labelField="true"  label="重置频率：" required="true"   data = "CommonUtil.serverData.dictionary.Intpaycircle" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						<input id="interestMethodL" name="interestMethodL" class="mini-combobox mini-mustFill" labelField="true"  label="计息方式：" required="true"   data = "CommonUtil.serverData.dictionary.calIrsMethod"labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						<input id="fixedLegDayCount" name="fixedLegDayCount" class="mini-combobox mini-mustFill" labelField="true"  label="计息基准：" data = "CommonUtil.serverData.dictionary.Basis"  labelStyle="text-align:left;width:130px;" style="width:100%;" required="true"   />
						<input id="leftDiscountCurve" name="leftDiscountCurve" class="mini-textbox mini-mustFill" onbuttonclick="onYchdEdit" labelField="true" label="利率曲线：" style="width: 100%;" labelStyle="text-align:left;width:130px;"  requiredErrorText="该输入项为必输项" required="true" />
						<input id="leftRateCode" name="leftRateCode" class="mini-textbox mini-mustFill" onbuttonclick="onRateEdit" labelField="true" label="利率代码：" style="width: 100%;" labelStyle="text-align:left;width:130px;"  requiredErrorText="该输入项为必输项" required="true" />
					</fieldset>
				</div>
				<div class="rightarea">
					<fieldset>
						<legend>利率明细</legend>
						<input id="fixedFloatR" name="fixedFloatR" class="mini-combobox mini-mustFill" labelField="true"  label="固定/浮动：" data = "CommonUtil.serverData.dictionary.fixfloat"  labelStyle="text-align:left;width:130px;" style="width:100%;"  required="true"  onvalidation="onFixFloatDirValidation" />
						<input id="rightRate" name="rightRate" class="mini-spinner" minValue="0" maxValue="9999999999999999.99999999" format="n8" changeOnMousewheel="false" labelField="true"  label="利率(%)："  style="width:100%;"  labelStyle="text-align:left;width:130px;" />
						<input id="benchmarkSpread" name="benchmarkSpread" class="mini-spinner" minValue="0" maxValue="999.9999" format="n4" changeOnMousewheel="false"  labelField="true"  label="利差(bps)："  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						<input id="floatPaymentDate" name="floatPaymentDate" class="mini-datepicker mini-mustFill" labelField="true"  label="首期定期支付日："  labelStyle="text-align:left;width:130px;" style="width:100%;" required="true"   />
						<input id="floatPaymentFrequency" name="floatPaymentFrequency" class="mini-combobox mini-mustFill" required="true"  labelField="true"  label="支付周期："  data = "CommonUtil.serverData.dictionary.Intpaycircle" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						<input id="legInterestAccrualDate" name="legInterestAccrualDate" class="mini-datepicker mini-mustFill" labelField="true"  label="首次利率确定日："  required="true"  style="width:100%;"  labelStyle="text-align:left;width:130px;" />
						<input id="interestResetFrequency" name="interestResetFrequency" class="mini-combobox mini-mustFill" labelField="true"  label="重置频率：" required="true"   data = "CommonUtil.serverData.dictionary.Intpaycircle" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						<input id="interestMethod" name="interestMethod" class="mini-combobox mini-mustFill" labelField="true"  label="计息方式：" required="true"   data = "CommonUtil.serverData.dictionary.calIrsMethod"labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						<input id="floatLegDayCount" name="floatLegDayCount" class="mini-combobox mini-mustFill" labelField="true"  label="计息基准："  required="true"  data = "CommonUtil.serverData.dictionary.Basis" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						<input id="rightDiscountCurve" name="rightDiscountCurve" class="mini-textbox mini-mustFill" onbuttonclick="onYchdEdit" labelField="true" label="利率曲线：" style="width: 100%;" labelStyle="text-align:left;width:130px;" allowInput="false" requiredErrorText="该输入项为必输项" required="true" />
						<input id="rightRateCode" name="rightRateCode" class="mini-textbox mini-mustFill" onbuttonclick="onRateEdit" labelField="true" label="利率代码：" style="width: 100%;" labelStyle="text-align:left;width:130px;" allowInput="false" requiredErrorText="该输入项为必输项" required="true" />
					</fieldset>
				</div>
			</div>
			<%@ include file="../../Common/opicsLessDetail.jsp"%>
			<%@ include file="../../Common/RiskCenterDetail.jsp"%>
			<div class="mini-panel" title="清算信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
				<div class="leftarea">
					<fieldset>
						<legend>本方账户</legend>
						<input id="fixedAccname" name="fixedAccname" class="mini-textbox" labelField="true"  label="资金账户户名：" vtype="maxLength:1333" style="width:100%;"  labelStyle="text-align:left;width:130px;" />
						<input id="fixedOpenBank" name="fixedOpenBank" class="mini-textbox" labelField="true"  label="资金开户行：" vtype="maxLength:1333" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						<input id="fixedAccnum" name="fixedAccnum" class="mini-textbox" labelField="true"  label="资金账号：" onvalidation="onEnglishAndNumberValidation" vtype="maxLength:400"  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						<input id="fixedPsnum" name="fixedPsnum" class="mini-textbox" labelField="true"  label="支付系统行号：" onvalidation="onEnglishAndNumberValidation" vtype="maxLength:400"  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
					</fieldset>
				</div>
				<div class="rightarea">
					<fieldset>
						<legend>对手方账户</legend>
						<input id="floatAccname" name="floatAccname" class="mini-textbox" labelField="true"  label="资金账户户名：" vtype="maxLength:1333" style="width:100%;"  labelStyle="text-align:left;width:130px;" />
						<input id="floatOpenBank" name="floatOpenBank" class="mini-textbox" labelField="true"  label="资金开户行："  vtype="maxLength:1333" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						<input id="floatAccnum" name="floatAccnum" class="mini-textbox" labelField="true"  label="资金账号："   onvalidation="onEnglishAndNumberValidation" vtype="maxLength:400"  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						<input id="floatPsnum" name="floatPsnum" class="mini-textbox" labelField="true"  label="支付系统行号："  onvalidation="onEnglishAndNumberValidation" vtype="maxLength:400" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
					</fieldset>
				</div>
			</div>
	</div>
	</fieldset>
</div>   

<script>
	mini.parse();

	var url = window.location.search;
	var prdNo = CommonUtil.getParam(url, "prdNo");
	var prdName = CommonUtil.getParam(url, "prdName");
	var form = new mini.Form("#search_form");
	var grid=mini.get("datagrid");
	var row="";
	/**************************************点击下面显示详情开始******************************/
	var from = new mini.Form("MiniSettleForeigDetail");
	from.setEnabled(false);
	var grid = mini.get("datagrid");
	//grid.load();
	 //绑定表单
    var db = new mini.DataBinding();
    db.bindForm("MiniSettleForeigDetail", grid);
	$(document).ready(function () {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn){
		});
	});
    /**************************************点击下面显示详情结束******************************/
	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});
	/* function searchs(pageSize,pageIndex){
		form.validate();
		if (form.isValid()==false) return;
		//提交的数据
		var data=form.getData(true);
		
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		var params=mini.encode(data);
		//发送ajax请求
		CommonUtil.ajax({
			url:'/IfsCfetsrmbIrsController/searchCfetsrmbIrs',
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
				grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	} */
	
	//查看详情
	function onRowDblClick(e) {
		var url = CommonUtil.baseWebPath() +"/cfetsrmb/IrsDeriveEdit.jsp?action=detail"+"&prdNo="+prdNo;
		var tab = {id: "IrsDetail",name:"IrsDetail",url:url,title:"结构衍生成交单详情",
					parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
		var paramData = {selectData:grid.getSelected()};
		CommonUtil.openNewMenuTab(tab,paramData);
	}
	
	//清空
	function clear(){
		var form=new mini.Form("search_form");
		form.clear();
	}
	
	function getData(action) {
		var row = null;
		if (action != "add") {
			row = grid.getSelected();
		}
		return row;
	}
	
	//添加
	function add(){
		var url = CommonUtil.baseWebPath() +"/cfetsrmb/IrsDeriveEdit.jsp?action=add&prdNo="+prdNo;
		var tab = {id: "IrsAdd",name:"IrsAdd",url:url,title:"结构衍生成交单补录",
					parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
		var paramData = {selectData:""};
		CommonUtil.openNewMenuTab(tab,paramData);
	}
	//修改
	function edit(){
		var row=grid.getSelected();
		if(row){
		var url = CommonUtil.baseWebPath() +"/cfetsrmb/IrsDeriveEdit.jsp?action=edit"+"&prdNo="+prdNo;
		var tab = {id: "IrsDeriveEdit",name:"IrsDeriveEdit",url:url,title:"结构衍生成交单修改",
					parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
		var paramData = {selectData:row};
		CommonUtil.openNewMenuTab(tab,paramData);
	}else{
		mini.alert("请选择一条记录","提示")
	}
	}
	//删除
	function del(){
		var rows=grid.getSelecteds();
		if(rows.length==0){
			mini.alert("请选中一行","提示");
			return;
		}
		mini.confirm("您确认要删除选中记录?","系统警告",function(value){   
			if (value=='ok'){   
				var data=rows[0];
				params=mini.encode(data);
				CommonUtil.ajax( {
					url:"/IfsCfetsrmbIrsController/deleteCfetsrmbIrs",
					data:params,
					callback : function(data) {
						mini.alert("删除成功.","系统提示");
						search(10,0);
					}
				});
			}
		});
	}
	
	grid.on("select",function(e){
		var row=e.record;
		mini.get("approve_mine_commit_btn").setEnabled(false);
		mini.get("approve_commit_btn").setEnabled(false);
		mini.get("edit_btn").setEnabled(false);
		mini.get("delete_btn").setEnabled(false);
		mini.get("approve_log").setEnabled(true);
		mini.get("recall_btn").setEnabled(true);
		mini.get("batch_commit_btn").setEnabled(true);
		if(row.approveStatus == "3"){//新建
			mini.get("edit_btn").setEnabled(true);
			mini.get("delete_btn").setEnabled(true);
			mini.get("approve_mine_commit_btn").setEnabled(true);
			mini.get("approve_commit_btn").setEnabled(false);
			mini.get("reback_btn").setEnabled(true);
			mini.get("recall_btn").setEnabled(false);
		}
		if( row.approveStatus == "6" || row.approveStatus == "7"){//审批通过：6    审批拒绝:5
			mini.get("edit_btn").setEnabled(false);
			mini.get("delete_btn").setEnabled(false);
			mini.get("approve_mine_commit_btn").setEnabled(false);
			mini.get("approve_commit_btn").setEnabled(true);
			mini.get("batch_approve_btn").setEnabled(false);
			mini.get("batch_commit_btn").setEnabled(false);
			mini.get("opics_check_btn").setEnabled(false);
			mini.get("reback_btn").setEnabled(false);
			mini.get("recall_btn").setEnabled(false);
		}
		if(row.approveStatus == "5"){//审批中:5
			mini.get("edit_btn").setEnabled(false);
			mini.get("delete_btn").setEnabled(false);
			mini.get("approve_mine_commit_btn").setEnabled(false);
			mini.get("approve_commit_btn").setEnabled(true);
			mini.get("batch_commit_btn").setEnabled(false);
			mini.get("reback_btn").setEnabled(false);
			mini.get("recall_btn").setEnabled(true);
		}
		
	});

	/* 按钮 查询事件 */
	function query(){
		search(grid.pageSize,0);
	}

	/* 查询 */
	function search(pageSize,pageIndex){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统提示");
			return;
		}
		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		data['prdFlag']='1';//结构性衍生为1
		var url=null;
		var approveType = mini.get("approveType").getValue();
		if(approveType == "mine"){//我发起的
			url = "/IfsCfetsrmbIrsController/searchRmbIrsSwapMine";
		}else if(approveType == "approve"){//待审批
			url = "/IfsCfetsrmbIrsController/searchPageRmbIrsUnfinished";
		}else{//已审批
			url = "/IfsCfetsrmbIrsController/searchPageRmbIrsFinished";
		}
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:url,
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	
	/**************************审批相关****************************/
	//审批日志查看
	function appLog(selections){
		var flow_type = Approve.FlowType.VerifyApproveFlow;
		if(selections.length <= 0){
			mini.alert("请选择要操作的数据","系统提示");
			return;
		}
		if(selections.length > 1){
			mini.alert("系统不支持多笔操作","系统提示");
			return;
		}
		if(selections[0].tradeSource == "3"){
			mini.alert("初始化导入的业务没有审批日志","系统提示");
			return false;
		}
		Approve.approveLog(flow_type,selections[0].ticketId);
	};
	
	//提交正式审批、待审批
	function verify(selections){
		if(selections.length == 0){
			mini.alert("请选中一条记录！","消息提示");
			return false;
		}else if(selections.length > 1){
			mini.alert("该功能不支持多笔提交，如需多笔提交请点击[批量提交]按钮","系统提示");
			return false;
		}
		if(selections[0]["approveStatus"] != "3" ){
			var url = CommonUtil.baseWebPath() +"/cfetsrmb/IrsDeriveEdit.jsp?action=approve&ticketId="+row.ticketId;
			var tab = {"id": "IRSApprove",name:"IRSApprove",url:url,title:"结构衍生审批",
						parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
			var paramData = {selectData:selections[0]};
			CommonUtil.openNewMenuTab(tab,paramData);
			// top["win"].showTab(tab);
		}else{
			Approve.approveCommit(Approve.FlowType.VerifyApproveFlow,selections[0]["ticketId"],Approve.OrderStatus.New,"IfsRmbIrsService",prdNo,function(){
				search(grid.pageSize,grid.pageIndex);
			});
		}
	};
	
	//审批
	function approve(){
		mini.get("approve_commit_btn").setEnabled(false);
		var messageid = mini.loading("系统正在处理...", "请稍后");
		try {
			verify(grid.getSelecteds());
		} catch (error) {
			
		}
		mini.hideMessageBox(messageid);	
		mini.get("approve_commit_btn").setEnabled(true);
	}
	
	//提交审批
	function commit(){
		mini.get("approve_mine_commit_btn").setEnabled(false);
		var messageid = mini.loading("系统正在处理...", "请稍后");
		try {
			verify(grid.getSelecteds());
		} catch (error) {
			
		}
		mini.hideMessageBox(messageid);	
		mini.get("approve_mine_commit_btn").setEnabled(true);
	}
	
	//审批日志
	function searchlog(){
		appLog(grid.getSelecteds());
	}
	
	//打印
    function print(){
		var selections = grid.getSelecteds();
		if(selections == null || selections.length == 0){
			mini.alert('请选择一条要打印的数据！','系统提示');
			return false;
		}else if(selections.length>1){
			mini.alert('暂不支持多条数据同时打印，请选择一条要打印的数据！','系统提示');
			return false;
		}
		var canPrint = selections[0].ticketId;
		
		if(!CommonUtil.isNull(canPrint)){
			var actionStr = CommonUtil.pPath + "/sl/IfsPrintUtilController/exportload/print/"+PrintNo.irsDerive+"/" + canPrint;
			$('<form target="_blank" action=' + actionStr + ' method="post"></form>').appendTo('body').submit().remove();
		};
	}
</script>
</body>
</html>