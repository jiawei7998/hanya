<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../../global.jsp" %>
<html>
<head>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/standard/Common/RiskCenter.js"></script>
    <script type="text/javascript" src="./rmbVerify.js"></script>
    <title>买断式回购</title>
    <script type="text/javascript">
        /**prdCode用于页面投资组合的选择   该全局变量必须为prdCode*/
        var prdCode = "REPO";
    </script>
</head>

<body style="width:100%;height:100%;background:white">
<div class="mini-splitter" style="width:100%;height:100%;">
    <div size="90%" showCollapseButton="false">
        <h1 style="text-align:center"><strong>买断式回购</strong></h1>
        <div id="field_form" class="mini-fit area" style="background:white">
            <input id="sponsor" name="sponsor" class="mini-hidden"/>
            <input id="sponInst" name="sponInst" class="mini-hidden"/>
            <input id="aDate" name="aDate" class="mini-hidden"/>
            <input id="ticketId" name="ticketId" class="mini-hidden"/>

            <div class="mini-panel" title="成交单编号" style="width:100%;" allowResize="true" collapseOnTitleClick="true">
                <div class="leftarea">
                    <input style="width:100%;" id="contractId" name="contractId" class="mini-textbox mini-mustFill"
                           labelField="true" required="true" requiredErrorText="该输入项为必输项" label="成交单编号："
                           labelStyle="text-align:left;width:130px;" vtype="maxLength:20"
                           onvalidation="onEnglishAndNumberValidation"/>
                </div>
                <div class="rightarea">
                    <input style="width:100%;" id="cfetsno" name="cfetsno" class="mini-textbox" label="CFETS机构码："
                           labelField="true" visible="false" enabled="false" labelStyle="text-align:left;width:130px;"/>
                </div>
            </div>
            <div class="mini-panel" title="成交双方信息" style="width:100%" allowResize="true" collapseOnTitleClick="true">
                <div class="leftarea">
                    <fieldset>
                        <legend>本方信息</legend>
                        <input id="myDir" name="myDir" class="mini-combobox mini-mustFill" labelField="true"
                               label="本方方向：" required="true" onvaluechanged="dirVerify" style="width:100%;"
                               labelStyle="text-align:left;width:120px;"
                               data="CommonUtil.serverData.dictionary.repoing"/>
                        <input id="positiveInst" name="positiveInst" class="mini-textbox mini-mustFill"
                               labelField="true" label="机构：" required="true" style="width:100%;" enabled="false"
                               labelStyle="text-align:left;width:120px;"/>
                        <input id="positiveTrader" name="positiveTrader" class="mini-textbox mini-mustFill"
                               labelField="true" label="交易员：" required="true" vtype="maxLength:5" enabled="false"
                               labelStyle="text-align:left;width:120px;" style="width:100%;"/>
                        <input id="quotaOccupyType" name="quotaOccupyType" class="mini-combobox mini-mustFill"
                               labelField="true" label="额度占用类型：" required="true" style="width:100%;"
                               labelStyle="text-align:left;width:120px;"
                               data="CommonUtil.serverData.dictionary.QuotaOccupyType" onvaluechanged="quotaJudge"
                               value="1"/>
                        <!-- <input id="positiveTel" name="positiveTel" class="mini-textbox" labelField="true"  label="电话："  onvalidation="onEnglishAndNumberValidation" vtype="maxLength:20" labelStyle="text-align:left;width:120px;" style="width:100%;"   />
                        <input id="positiveFax" name="positiveFax" class="mini-textbox" labelField="true"  label="传真："  onvalidation="onEnglishAndNumberValidation" vtype="maxLength:20" labelStyle="text-align:left;width:120px;" style="width:100%;"   />
                        <input id="positiveCorp" name="positiveCorp" class="mini-textbox" labelField="true"  label="法人代表："  vtype="maxLength:5" labelStyle="text-align:left;width:120px;" style="width:100%;"   />
                        <input id="positiveAddr" name="positiveAddr" class="mini-textbox" labelField="true"  label="地址："  vtype="maxLength:50" labelStyle="text-align:left;width:120px;" style="width:100%;"   />
                     -->
                    </fieldset>
                </div>
                <div class="leftarea">
                    <fieldset>
                        <legend>对手方信息</legend>
                        <input id="oppoDir" name="oppoDir" class="mini-combobox mini-mustFill" labelField="true"
                               label="对方方向：" required="true" enabled="false" style="width:100%;"
                               labelStyle="text-align:left;width:120px;"
                               data="CommonUtil.serverData.dictionary.repoing"/>
                        <input id="reverseInst" name="reverseInst" class="mini-buttonedit mini-mustFill"
                               onbuttonclick="onButtonEdit" labelField="true" allowInput="false" label="机构："
                               required="true" style="width:100%;" labelStyle="text-align:left;width:120px;"/>
                        <input id="reverseTrader" name="reverseTrader" class="mini-textbox mini-mustFill"
                               labelField="true" label="交易员：" vtype="maxLength:20"
                               labelStyle="text-align:left;width:120px;" style="width:100%;margin-bottom: 2px"
                               required="true"/>
                        <!-- <input id="reverseTel" name="reverseTel" class="mini-textbox" labelField="true"  label="电话："  onvalidation="onEnglishAndNumberValidation" vtype="maxLength:20" labelStyle="text-align:left;width:120px;" style="width:100%;"   />
                        <input id="reverseFax" name="reverseFax" class="mini-textbox" labelField="true"  label="传真："  onvalidation="onEnglishAndNumberValidation" vtype="maxLength:20" labelStyle="text-align:left;width:120px;" style="width:100%;"   />
                        <input id="reverseCorp" name="reverseCorp" class="mini-textbox" labelField="true"  label="法人代表：" vtype="maxLength:5" labelStyle="text-align:left;width:120px;" style="width:100%;"   />
                        <input id="reverseAddr" name="reverseAddr" class="mini-textbox" labelField="true"  label="地址：" vtype="maxLength:50" labelStyle="text-align:left;width:120px;" style="width:100%;"   />
                     -->
                    </fieldset>
                </div>
            </div>

            <div class="mini-panel" title="交易主体信息" style="width:100%" allowResize="true" collapseOnTitleClick="true">
                <div class="leftarea">
                    <input style="width:100%;" id="forDate" name="forDate" class="mini-datepicker mini-mustFill"
                           labelField="true" required="true" label="成交日期：" labelStyle="text-align:left;width:130px;"/>
                    <input style="width:100%" id="bondCode" name="bondCode" class="mini-buttonedit mini-mustFill"
                           onbuttonclick="onBondQuery" labelField="true" label="债券代码：" required="true"
                           labelStyle="text-align:left;width:130px;"/>
                    <input style="width:100%;" id="bondProduct" name="bondProduct" onbuttonclick="onBondPrdEdit"
                           allowInput="false" class="mini-buttonedit mini-mustFill" labelField="true" label="券的产品代码："
                           required="true" maxLength="6" labelStyle="text-align:left;width:130px;">
                    <input id="bondPort" name="bondPort" class="mini-buttonedit mini-mustFill"
                           onbuttonclick="onBondPortEdit" style="width:100%;" labelField="true" label="券的投资组合："
                           required="true" allowInput="false" vtype="maxLength:4"
                           labelStyle="text-align:left;width:130px;">
                    <input id="invtype" name="invtype" class="mini-combobox mini-mustFill" labelField="true"
                           label="券的投资类型：" style="width:100%;" labelStyle="text-align:left;width:130px;"
                           data="CommonUtil.serverData.dictionary.invtype"/>
                    <input style="width:100%" id="totalFaceValue" name="totalFaceValue"
                           class="mini-spinner mini-mustFill input-text-strong" labelField="true" label="券面总额(万元)："
                           required="true" minValue="0" maxValue="9999999999.9999" format="n4"
                           changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"
                           onvalidation="zeroValidation" onvaluechanged="onFirstPriceChanged"/>
                    <!-- onvaluechanged="onFirstPriceChanged"  -->
                    <input style="width:100%" id="firstSettlementDate" name="firstSettlementDate"
                           class="mini-datepicker mini-mustFill" labelField="true" label="首次结算日：" required="true"
                           ondrawdate="onDrawDateStart" onvaluechanged="onStartDateChanged"
                           labelStyle="text-align:left;width:130px;"/>
                    <input style="width:100%" id="tenor" name="tenor" class="mini-spinner mini-mustFill"
                           labelField="true" label="回购期限(天)：" required="true" minValue="0" maxValue="99999"
                           changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"/>
                    <input style="width:100%" id="firstYield" name="firstYield"
                           class="mini-spinner mini-mustFill input-text-strong" labelField="true" label="首次收益率(%)："
                           required="true" onvaluechanged="onFirstYieldChanged" minValue="0" maxValue="999.999999"
                           format="n6" changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"/>
                    <input style="width:100%" id="firstCleanPrice" name="firstCleanPrice"
                           class="mini-spinner mini-mustFill input-text-strong" labelField="true" label="首次净价(元)："
                           required="true" onvalidation="zeroValidation" minValue="0" maxValue="99999999999999.9999"
                           format="n4" changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"
                           onvaluechanged="onFirstPriceChanged"/><!-- onvaluechanged="onFirstPriceChanged" -->
                    <input style="width:100%" id="firstAccuredInterest" name="firstAccuredInterest"
                           class="mini-spinner mini-mustFill input-text-strong" labelField="true" label="首次应计利息(元)："
                           required="true" minValue="0" maxValue="99999999999999.9999" format="n4"
                           changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"
                           onvaluechanged="onFirstPriceChanged"/><!-- onvaluechanged="onFirstPriceChanged" -->
                    <input style="width:100%" id="firstDirtyPrice" name="firstDirtyPrice"
                           class="mini-spinner mini-mustFill input-text-strong" labelField="true" label="首次全价(元)："
                           enabled="false" required="true" onvalidation="zeroValidation" minValue="0"
                           maxValue="99999999999999.9999" format="n4" changeOnMousewheel="false"
                           labelStyle="text-align:left;width:130px;" onvaluechanged="firstDirtyPriceChanged"/>
                    <!-- onvaluechanged="firstDirtyPriceChanged" -->
                    <input style="width:100%" id="firstSettlementMethod" name="firstSettlementMethod"
                           class="mini-combobox mini-mustFill" labelField="true" label="首次结算方式：" required="true"
                           data="CommonUtil.serverData.dictionary.SettlementMethod"
                           labelStyle="text-align:left;width:130px;"/>
                    <input style="width:100%" id="firstSettlementAmount" name="firstSettlementAmount"
                           class="mini-spinner mini-mustFill input-text-strong" labelField="true" label="首次结算金额(元)："
                           required="true" minValue="0" maxValue="99999999999999.9999" format="n4"
                           changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"
                           onvalidation="zeroValidation" onvaluechanged="sumSecondSettlementAmountNew"/>
                    <input id="basis" name="basis" class="mini-combobox mini-mustFill" required="true" labelField="true"
                           label="计息基准：" style="width:100%;" data="CommonUtil.serverData.dictionary.Basis"
                           vtype="maxLength:15" labelStyle="text-align:left;width:130px;">
                    <!-- onvaluechanged="onFirstPriceChanged" -->
                    <input id="dealTransType" name="dealTransType" class="mini-combobox" labelField="true"
                           style="width:100%;" label="数据来源：" data="CommonUtil.serverData.dictionary.dealTransType1"
                           vtype="maxLength:10" labelStyle="text-align:left;width:130px;" value="1">
                </div>
                <div class="rightarea">
                    <input id="nettingStatus" name="nettingStatus" class="mini-combobox mini-mustFill" required="true"
                           labelField="true" label="净额清算状态：" labelStyle="text-align:left;width:130px;"
                           style="width:100%;" data="CommonUtil.serverData.dictionary.YesNo" showNullItem="true"/>
                    <input style="width:100%" id="bondName" name="bondName" class="mini-textbox mini-mustFill"
                           labelField="true" label="债券名称：" required="true" maxLength="33"
                           labelStyle="text-align:left;width:130px;" enabled="false"/>
                    <input style="width:100%;" id="bondProdType" name="bondProdType" onbuttonclick="onBondTypeEdit"
                           allowInput="false" class="mini-buttonedit mini-mustFill" labelField="true" label="券的产品类型："
                           required="true" vtype="maxLength:2" labelStyle="text-align:left;width:130px;">
                    <input style="width:100%;" id="bondCost" name="bondCost" onbuttonclick="onBondCostEdit"
                           allowInput="false" class="mini-buttonedit mini-mustFill" labelField="true" label="券的成本中心："
                           required="true" vtype="maxLength:15" labelStyle="text-align:left;width:130px;">
                    <input style="width:100%" id="tradingProduct" name="tradingProduct" class="mini-combobox"
                           allowInput="false" labelField="true" label="交易品种：" labelStyle="text-align:left;width:130px;"
                           data="CommonUtil.serverData.dictionary.dealVarietyBuy"
                           onvaluechanged="sumSecondSettlementAmountNew"/>
                    <input style="width:100%" id="repoRate" name="repoRate"
                           class="mini-spinner mini-mustFill input-text-strong" labelField="true" label="回购利率(%)："
                           required="true" onvalidation="zeroValidation" minValue="0" maxValue="99999.999999"
                           format="n6" changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"
                           onvaluechanged="sumSecondSettlementAmountNew"/>
                    <input style="width:100%;display:none" id="startDate" name="startDate"
                           class="mini-textbox mini-mustFill" labelField="true" label="起息日：" required="true"
                           vtype="maxLength:10" labelStyle="text-align:left;width:130px;" enabled="false"/>
                    <input style="width:100%;display:none" id="matdt" name="matdt" class="mini-textbox mini-mustFill"
                           labelField="true" label="到期日：" required="true" vtype="maxLength:10"
                           labelStyle="text-align:left;width:130px;" enabled="false"/>
                    <input style="width:100%" id="secondSettlementDate" name="secondSettlementDate"
                           class="mini-datepicker mini-mustFill" labelField="true" label="到期结算日：" required="true"
                           ondrawdate="onDrawDateEnd" onvaluechanged="onEndDateChanged"
                           labelStyle="text-align:left;width:130px;"/>
                    <input style="width:100%" id="occupancyDays" name="occupancyDays" class="mini-spinner mini-mustFill"
                           labelField="true" label="实际占款天数(天)：" required="true" minValue="0" maxValue="99999"
                           changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"
                           onvaluechanged="sumSecondSettlementAmountNew"/>
                    <input style="width:100%" id="secondYield" name="secondYield"
                           class="mini-spinner mini-mustFill input-text-strong" labelField="true" label="到期收益率(%)："
                           required="true" minValue="0" maxValue="999.999999" format="n6" changeOnMousewheel="false"
                           labelStyle="text-align:left;width:130px;"/>
                    <input style="width:100%" id="secondCleanPrice" name="secondCleanPrice"
                           class="mini-spinner mini-mustFill input-text-strong" labelField="true" label="到期净价(元)："
                           required="true" onvalidation="zeroValidation" minValue="0" maxValue="99999999999999.9999"
                           format="n4" changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"
                           onvaluechanged="onSecondInterestChanged"/><!-- onvaluechanged="onSecondInterestChanged" -->
                    <input style="width:100%" id="secondAccuredInterest" name="secondAccuredInterest"
                           class="mini-spinner mini-mustFill input-text-strong" labelField="true" label="到期应计利息(元)："
                           required="true" minValue="0" maxValue="99999999999999.9999" format="n4"
                           changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"
                           onvaluechanged="onSecondInterestChanged"/><!-- onvaluechanged="onSecondInterestChanged" -->
                    <input style="width:100%" id="secondDirtyPrice" name="secondDirtyPrice"
                           class="mini-spinner mini-mustFill input-text-strong" labelField="true" label="到期全价(元)："
                           enabled="false" required="true" onvalidation="zeroValidation" minValue="0"
                           maxValue="99999999999999.9999" format="n4" changeOnMousewheel="false"
                           labelStyle="text-align:left;width:130px;" onvaluechanged="onFirstPriceChanged"/>
                    <!-- onvaluechanged="onFirstPriceChanged" -->
                    <input style="width:100%" id="secondSettlementMethod" name="secondSettlementMethod"
                           class="mini-combobox mini-mustFill" labelField="true" label="到期结算方式：" required="true"
                           data="CommonUtil.serverData.dictionary.SettlementMethod"
                           labelStyle="text-align:left;width:130px;"/>
                    <input style="width:100%" id="secondSettlementAmount" name="secondSettlementAmount"
                           class="mini-spinner mini-mustFill input-text-strong" labelField="true" label="到期结算金额(元)："
                           required="true" minValue="0" maxValue="99999999999999.9999" format="n4"
                           changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"
                           onvalidation="zeroValidation"/>
                    <input id="tradingModel" name="tradingModel" class="mini-combobox mini-mustFill" required="true"
                           labelField="true" label="交易模式：" labelStyle="text-align:left;width:130px;" style="width:100%;"
                           data="CommonUtil.serverData.dictionary.TradeModel"/>
                    <input id="note" name="note" class="mini-textbox" labelField="true" label="交易事由："
                           labelStyle="text-align:left;width:130px;" style="width:100%;" vtype="maxLength:16"/>

                </div>
                <%@ include file="../../Common/custLimit.jsp" %>
            </div>
            <div class="mini-panel" title="报表要素" style="width:100%;" allowResize="true" collapseOnTitleClick="true">
                <div class="leftarea">
                    <input id="fiveLevelClass" name="fiveLevelClass" class="mini-combobox" labelField="true"
                           requiredErrorText="该输入项为必输项" label="五级分类：" labelStyle="text-align:left;width:130px;"
                           style="width:100%;" data="CommonUtil.serverData.dictionary.FiveLevelClass" value="0"/>
                    <input id="pricingStandardType" name="pricingStandardType" class="mini-combobox" labelField="true"
                           label="借贷定价基准类型：" labelStyle="text-align:left;width:130px;"
                           style="width:100%;" data="CommonUtil.serverData.dictionary.pricingStandardType"/>
                </div>
                <div class="rightarea">
                    <input id="attributionDept" name="attributionDept" valueField="id" showFolderCheckBox="true"
                           labelField="true" label="归属机构：" showCheckBox="true" style="width:100%"
                           labelStyle="text-align:left;width:130px"
                           showTreeLines="true" showTreeIcon="true" textField="instName" parentField="parentId"
                           class="mini-treeselect" expandOnLoad="true" resultAsTree="false"
                           valueFromSelect="true" emptyText="请选择机构"/>
                </div>
            </div>
            <%@ include file="../../Common/opicsRepo.jsp" %>
            <%@ include file="../../Common/RiskCenter.jsp" %>
            <div class="mini-panel" title="清算信息" style="width:100%" allowResize="true" collapseOnTitleClick="true">
                <div class="leftarea">
                    <fieldset>
                        <legend>本方账户</legend>
                        <input id="positiveAccname" name="positiveAccname" class="mini-textbox" labelField="true"
                               label="资金账户户名：" vtype="maxLength:1333" style="width:100%;"
                               labelStyle="text-align:left;width:120px;"/>
                        <input id="positiveOpenBank" name="positiveOpenBank" class="mini-textbox" labelField="true"
                               label="资金开户行：" vtype="maxLength:1333" labelStyle="text-align:left;width:120px;"
                               style="width:100%;"/>
                        <input id="positiveAccnum" name="positiveAccnum" class="mini-textbox" labelField="true"
                               label="资金账号：" onvalidation="onEnglishAndNumberValidation" vtype="maxLength:400"
                               labelStyle="text-align:left;width:120px;" style="width:100%;"/>
                        <input id="positivePsnum" name="positivePsnum" class="mini-textbox" labelField="true"
                               label="支付系统行号：" onvalidation="onEnglishAndNumberValidation" vtype="maxLength:400"
                               labelStyle="text-align:left;width:120px;" style="width:100%;"/>
                        <input id="positiveCaname" name="positiveCaname" class="mini-textbox" labelField="true"
                               label="托管账户户名：" vtype="maxLength:1333" labelStyle="text-align:left;width:120px;"
                               style="width:100%;"/>
                        <input id="positiveCustname" name="positiveCustname" class="mini-buttonedit mini-mustFill"
                               onbuttonclick="onSaccQuery" required="true" labelField="true" allowInput="false"
                               label="托管机构：" vtype="maxLength:1333" labelStyle="text-align:left;width:120px;"
                               style="width:100%;"/>
                        <input id="positiveCanum" name="positiveCanum" class="mini-textbox" labelField="true"
                               label="托管帐号：" onvalidation="onEnglishAndNumberValidation" vtype="maxLength:400"
                               labelStyle="text-align:left;width:120px;" style="width:100%;"/>
                    </fieldset>
                </div>
                <div class="rightarea">
                    <fieldset>
                        <legend>对手方账户</legend>
                        <input id="reverseAccname" name="reverseAccname" class="mini-textbox" labelField="true"
                               label="资金账户户名：" vtype="maxLength:1333" style="width:100%;"
                               labelStyle="text-align:left;width:120px;"/>
                        <input id="reverseOpenBank" name="reverseOpenBank" class="mini-textbox" labelField="true"
                               label="资金开户行：" vtype="maxLength:1333" labelStyle="text-align:left;width:120px;"
                               style="width:100%;"/>
                        <input id="reverseAccnum" name="reverseAccnum" class="mini-textbox" labelField="true"
                               label="资金账号：" onvalidation="onEnglishAndNumberValidation" vtype="maxLength:400"
                               labelStyle="text-align:left;width:120px;" style="width:100%;"/>
                        <input id="reversePsnum" name="reversePsnum" class="mini-textbox" labelField="true"
                               label="支付系统行号：" onvalidation="onEnglishAndNumberValidation" vtype="maxLength:400"
                               labelStyle="text-align:left;width:120px;" style="width:100%;"/>
                        <input id="reverseCaname" name="reverseCaname" class="mini-textbox" labelField="true"
                               label="托管账户户名：" vtype="maxLength:1333" labelStyle="text-align:left;width:120px;"
                               style="width:100%;"/>
                        <input id="reverseCustname" name="reverseCustname" class="mini-buttonedit mini-mustFill"
                               onbuttonclick="onSaccQuery" required="true" labelField="true" allowInput="false"
                               label="托管机构：" vtype="maxLength:1333" labelStyle="text-align:left;width:120px;"
                               style="width:100%;"/>
                        <input id="reverseCanum" name="reverseCanum" class="mini-textbox" labelField="true"
                               label="托管帐号：" onvalidation="onEnglishAndNumberValidation" vtype="maxLength:400"
                               labelStyle="text-align:left;width:120px;" style="width:100%;"/>
                    </fieldset>
                </div>
            </div>
            <%@ include file="../../Common/Flow/MiniApproveOpCommon.jsp" %>
        </div>

    </div>
    <div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
        <div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"
                                                                style="width:120px;" id="save_btn"
                                                                onclick="save">保存交易</a></div>
        <div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"
                                                                style="width:120px;" id="close_btn"
                                                                onclick="close">关闭界面</a></div>
    </div>
</div>
<script type="text/javascript">
    mini.parse();

    //获取当前tab
    var url = window.location.search;
    var action = CommonUtil.getParam(url, "action");
    var ticketId = CommonUtil.getParam(url, "ticketid");
    var form = new mini.Form("#field_form");
    mini.get("priceDeviation").setVisible(true);//偏离度

    var prdNo = CommonUtil.getParam(url, "prdNo");
    var fPrdCode = CommonUtil.getParam(url, "fPrdCode");
    var tradeData = {};


    var row;
    var tradeData = {};
    if (action != "print") {
        var currTab = top["win"].tabs.getActiveTab();
        var params = currTab.params;
        row = params.selectData;
        var detailData = row;
        tradeData.selectData = row;
        tradeData.operType = action;
        tradeData.serial_no = row.ticketId;
        tradeData.task_id = row.taskId;
    }

    function quotaJudge() {
        var quotaType = mini.get("quotaOccupyType").getValue();
        if ("0" == quotaType) {
            //不占额度，隐藏授信页面
            mini.get("panel1").setVisible(false);
            // mini.get("panel12").setVisible(false);
        } else if ("1" == quotaType) {
            //占用发行人额度
            mini.get("panel1").setVisible(true);
            // mini.get("panel12").setVisible(true);
            var bondCode = mini.get("bondCode").getValue();
            creditsubvalue(null, "442", bondCode);
        } else {
            //占用交易对手
            mini.get("panel1").setVisible(true);
            // mini.get("panel12").setVisible(true);
            var cno = mini.get("reverseInst").getValue();
            creditsubvalue(cno, "442", null);
        }
    }

    //保存
    function save() {
        //表单验证！！！
        form.validate();
        if (form.isValid() == false) {
            mini.alert("信息填写有误，请重新填写", "系统提示");
            return;
        }

        var data = form.getData(true);
        data.custNo = mini.get("creditCno").getValue();
        //对数据做最后的确认
        if (data['firstDirtyPrice'] != data['firstAccuredInterest'] + data['firstCleanPrice'] ||
            data['secondDirtyPrice'] != data['secondAccuredInterest'] + data['secondCleanPrice'] ||
            data['firstSettlementAmount'] != data['firstDirtyPrice'] * data['totalFaceValue'] * 100 ||
            data['secondSettlementAmount'] != data['secondDirtyPrice'] * data['totalFaceValue'] * 100) {
            mini.confirm("确认以手输数据为准吗？", "确认", function (action) {
                if (action != "ok") {
                    return;
                }
                data['positiveInst'] = "<%=__sessionInstitution.getInstId()%>";
                data['positiveTrader'] = "<%=__sessionUser.getUserId() %>";
// 				data['dealTransType']="M";
                data['sponInst'] = "<%=__sessionInstitution.getInstId()%>";
                data['sponsor'] = "<%=__sessionUser.getUserId() %>";

                data['fPrdCode']=fPrdCode;
                var params = mini.encode(data);
                CommonUtil.ajax({
                    url: "/IfsCfetsrmbOrController/saveOr",
                    data: params,
                    callback: function (data) {
                        if (data.desc.indexOf("短头寸") > -1) {
                            //短头寸仍然可以提交
                            mini.confirm(data + ",是否保存", "确认", function (action) {
                                if (action != "ok") {
                                    return;
                                }
                                saveOrTrue(params);
                            });
                        } else {
                            mini.alert(data.desc, '提示信息', function () {
                                if (data.code == 'error.common.0000') {
                                    top["win"].closeMenuTab();//关闭并刷新当前界面
                                }
                            });
                        }
                    }
                });

            });
        } else {
            data['positiveInst'] = "<%=__sessionInstitution.getInstId()%>";
            data['positiveTrader'] = "<%=__sessionUser.getUserId() %>";
// 			data['dealTransType']="M";
            data['sponInst'] = "<%=__sessionInstitution.getInstId()%>";
            data['sponsor'] = "<%=__sessionUser.getUserId() %>";

            data['fPrdCode']=fPrdCode;
            var params = mini.encode(data);
            CommonUtil.ajax({
                url: "/IfsCfetsrmbOrController/saveOr",
                data: params,
                callback: function (data) {
                    if (data.desc.indexOf("短头寸") > -1) {
                        //短头寸仍然可以提交
                        mini.confirm(data + ",是否保存", "确认", function (action) {
                            if (action != "ok") {
                                return;
                            }
                            saveOrTrue(params);
                        });
                    } else {
                        mini.alert(data.desc, '提示信息', function () {
                            if (data.code == 'error.common.0000') {
                                top["win"].closeMenuTab();//关闭并刷新当前界面
                            }
                        });
                    }
                }
            });

        }

    }

    //不校验短头寸保存
    function saveOrTrue(params) {
        CommonUtil.ajax({
            url: "/IfsCfetsrmbOrController/saveOrTrue",
            data: params,
            callback: function (data) {
                mini.alert(data.desc, '提示信息', function () {
                    if (data.code == 'error.common.0000') {
                        top["win"].closeMenuTab();//关闭并刷新当前界面
                    }
                });
            }
        });
    }

    function close() {
        top["win"].closeMenuTab();
    }

    function loadInstitutionTree() {
        CommonUtil.ajax({
            data: {"branchId": branchId},
            url: "/InstitutionController/searchInstitutions",
            callback: function (data) {
                mini.get("attributionDept").setData(data.obj);
                if (detailData != null && detailData.attributionDept != null) {
                    mini.get("attributionDept").setValue(detailData.attributionDept);
                } else {
                    mini.get("attributionDept").setValue("<%=__sessionInstitution.getInstId()%>");
                }
            }
        });
    }

    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
            if ($.inArray(action, ["approve", "detail", "print"]) > -1) {
                mini.get("save_btn").setVisible(false);
                form.setEnabled(false);
                var form11 = new mini.Form("#approve_operate_form");
                form11.setEnabled(true);
                mini.get("forDate").setEnabled(false);
            }

            if ($.inArray(action, ["edit", "approve", "detail"]) > -1) {//修改、审批、详情
                form.setData(row);
                mini.get("forDate").setValue(row.forDate);
                mini.get("ticketId").setValue(row.ticketId);
                //投资组合
                //mini.get("port").setValue(row.port);
                //mini.get("port").setText(row.port);
                //成本中心
                mini.get("cost").setValue(row.cost);
                mini.get("cost").setText(row.cost);
                //产品代码
                mini.get("product").setValue(row.product);
                mini.get("product").setText(row.product);
                //产品类型
                mini.get("prodType").setValue(row.prodType);
                mini.get("prodType").setText(row.prodType);
                //对方机构reverseInst
                mini.get("reverseInst").setValue(row.cno);
// 			mini.get("reverseInst").setText(row.reverseInst);
                queryTextName(row.cno);
                //债券代码
                mini.get("bondCode").setText(row.bondCode);
                mini.get("contractId").setEnabled(false);
                //清算路径
                mini.get("ccysmeans").setValue(row.ccysmeans);
                mini.get("ccysmeans").setText(row.ccysmeans);
                mini.get("ctrsmeans").setValue(row.ctrsmeans);
                mini.get("ctrsmeans").setText(row.ctrsmeans);

                mini.get("positiveCustname").setValue(row.positiveCustname);
                mini.get("positiveCustname").setText(row.positiveCustname);
                mini.get("reverseCustname").setValue(row.reverseCustname);
                mini.get("reverseCustname").setText(row.reverseCustname);

                //风险中台
                mini.get("applyNo").setValue(row.applyNo);
                mini.get("applyNo").setText(row.applyNo);
                queryCustNo(row.custNo);
                mini.get("panel1").setVisible(false);
                dirVerify();

                //逆回购 出钱设置授信主体
                // if(row.myDir == 'P') {creditsubvalue(row.custNo,"442",null);}

                //券的投资组合
                mini.get("bondPort").setValue(row.bondPort);
                mini.get("bondPort").setText(row.bondPort);
                //券的成本中心
                mini.get("bondCost").setValue(row.bondCost);
                mini.get("bondCost").setText(row.bondCost);
                //券的产品代码
                mini.get("bondProduct").setValue(row.bondProduct);
                mini.get("bondProduct").setText(row.bondProduct);
                //券的产品类型
                mini.get("bondProdType").setValue(row.bondProdType);
                mini.get("bondProdType").setText(row.bondProdType);

                //searchProductWeightNew();
                //mini.get("weight").setValue(row.weight);
                if (action == "edit" && row.weight == null) {
                    searchProductWeightNew();
                }

                if (action == "edit") {//偏离度
                    if (CommonUtil.isNull(row.priceDeviation)) {
                        mini.get("repoRate").setValue(row.repoRate);
                        mini.get("repoRate").setText(row.repoRate);
                        mini.get("tradingProduct").setText(row.tradingProduct);
                        mini.get("tradingProduct").setValue(row.tradingProduct);
                        searchPriceDeviation();
                    }
                }
                mini.get("positiveInst").setValue(row.positiveInst);
                mini.get("positiveTrader").setValue(row.myUserName);
            } else {//增加
                mini.get("sponsor").setValue("<%=__sessionUser.getUserId()%>");
                mini.get("sponInst").setValue("<%=__sessionUser.getInstId() %>");
                mini.get("positiveInst").setValue("<%=__sessionInstitution.getInstFullname()%>");
                mini.get("positiveTrader").setValue("<%=__sessionUser.getUserName() %>");
                mini.get("forDate").setValue("<%=__bizDate %>");
                //mini.get("custNo").setVisible(false);//占用授信主体
                //mini.get("weight").setVisible(false);//权重

                mini.get("basis").select(1);
                mini.get("firstSettlementMethod").select(0);
                mini.get("secondSettlementMethod").select(0);
                //产品代码
                mini.get("product").setValue("REPO");
                mini.get("product").setText("REPO");

                //券投资类型
                mini.get("invtype").setText("T");
                mini.get("invtype").setValue("T");
                //券投资组合
                mini.get("bondPort").setText("ZQJY");
                mini.get("bondPort").setValue("ZQJY");

                mini.get("bondCost").setText("8400000000");
                mini.get("bondCost").setValue("8400000000");

                mini.get("panel1").setVisible(false);
                // mini.get("panel12").setVisible(false);
                mini.get("quotaOccupyType").setVisible(false);

            }
            if ($.inArray(action, ["print"]) > -1) {
                var dealTransType = mini.get("dealTransType").getValue();
                mini.get("ticketId").setValue(ticketId);
                CommonUtil.ajax({
                    url: '/IfsCfetsrmbOrController/searchCfetsrmbOrById',
                    data: {"dealTransType": dealTransType, "ticketId": ticketId},
                    callback: function (data) {
                        form.setData(data.obj);
                        mini.get("reverseInst").setValue(data.obj.reverseInst);
                        mini.get("reverseInst").setText(data.obj.reverseInst);
                        mini.get("product").setValue(data.obj.product);
                        mini.get("product").setText(data.obj.product);

                        mini.get("prodType").setValue(data.obj.prodType);
                        mini.get("prodType").setText(data.obj.prodType);
                        mini.get("cost").setValue(data.obj.cost);
                        mini.get("cost").setText(data.obj.cost);
                        mini.get("bondCode").setText(data.obj.bondCode);
                        mini.get("bondProduct").setText(data.obj.bondProduct);
                        mini.get("bondProdType").setText(data.obj.bondProdType);
                        mini.get("bondCost").setText(data.obj.bondCost);
                        mini.get("bondPort").setText(data.obj.bondPort);
//                    mini.get("positiveCustname").setValue(data.obj.positiveCustname);
//                    mini.get("positiveCustname").setText(data.obj.positiveCustname);
//                    mini.get("reverseCustname").setValue(data.obj.reverseCustname);
//                    mini.get("reverseCustname").setText(data.obj.reverseCustname);
                    }
                });
                $("#fileMsg").hide();


                //债券信息
//            initGrid();
                //审批日志
//            log_grid.show();
//            CommonUtil.ajax({
//                url:"/IfsFlowController/getOneFlowDefineBaseInfo",
//                data:{serial_no:ticketId},
//                callerror: function(data){
//                    ApproveFlowLog.loadLogInfo();
//                },
//                callback : function(data) {
//                    var innerInterval;
//                    innerInitFunction = function(){
//                        clearInterval(innerInterval);
//                        ApproveFlowLog.loadLogInfo();
//                    },
//                    innerInterval = setInterval("innerInitFunction()",100);
//                }
//            });
            }
            mini.get("dealTransType").setEnabled(false);
            loadInstitutionTree();
        });
    });

    //获取债券发行人
    function initBond(bndcd) {
        CommonUtil.ajax({
            url: "/IfsOpicsBondController/searchOneById",
            data: {"bndcd": bndcd},
            callback: function (data) {
                if (data != null) {
                    return data.issuer;
                } else {
                    return null;
                }
            }
        });
    }


    //加载机构树
    function nodeclick(e) {

        var oldvalue = e.sender.value;
        var param = mini.encode({"branchId": branchId}); //序列化成JSON
        var oldData = e.sender.data;
        if (oldData == null || oldData == "") {
            CommonUtil.ajax({
                url: "/InstitutionController/searchInstitutionByBranchId",
                data: param,
                callback: function (data) {
                    var obj = e.sender;
                    obj.setData(data.obj);
                    obj.setValue(oldvalue);

                }
            });
        }

    }

    //文、数字、下划线 的验证
    function onEnglishAndNumberValidations(e) {
        if (e.value == "" || e.value == null) {//值为空，就不做校验
            return;
        }
        if (e.isValid) {
            if (isEnglishAndNumbers(e.value) == false) {
                e.errorText = "必须输入英文小写+数字";
                e.isValid = false;
            }
        }
    }

    /* 是否英文+数字 */
    function isEnglishAndNumbers(v) {
        var re = new RegExp("^[0-9a-z\_\-]+$");
        if (re.test(v)) return true;
        return false;
    }


    //英文、数字、下划线 的验证
    function onEnglishAndNumberValidation(e) {
        if (e.value == "" || e.value == null) {//值为空，就不做校验
            return;
        }
        if (e.isValid) {
            if (isEnglishAndNumber(e.value) == false) {
                e.errorText = "必须输入英文+数字";
                e.isValid = false;
            }
        }
    }

    /* 是否英文+数字 */
    function isEnglishAndNumber(v) {
        var re = new RegExp("^[0-9a-zA-Z\_\-]+$");
        if (re.test(v)) return true;
        return false;
    }

    function onButtonEdit(e) {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Common/CustMini.jsp",
            title: "选择对手方列表",
            width: 900,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.cno);
                        btnEdit.setText(data.cliname);
                        btnEdit.focus();
                        //判断额度占用类型是否为交易对手
                        if ("2" == mini.get("quotaOccupyType").value) {
                            creditsubvalue(data.cno, "442", null);
                        }
                    }
                }

            }
        });

    }

    //根据交易对手编号查询全称
    function queryTextName(cno) {
        CommonUtil.ajax({
            url: "/IfsOpicsCustController/searchIfsOpicsCust",
            data: {'cno': cno},
            callback: function (data) {
                if (data && data.obj) {
                    mini.get("reverseInst").setText(data.obj.cliname);
                } else {
                    mini.get("reverseInst").setText(cno);
                }
            }
        });
    }

    function onBondQuery(e) {
        var btnEdit = this;
        /* var startdate = mini.get("firstSettlementDate").getValue();
        var enddate =mini.get("secondSettlementDate").getValue(); */
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Ifs/bond/bondAddMini.jsp",
            title: "选择债券列表",
            width: 900,
            height: 600,
            /* onload: function () {
				var iframe = this.getIFrameEl();
				var data ={startdate:startdate,enddate:enddate};
				iframe.contentWindow.SetData(data);
		    }, */
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.bndcd);
                        btnEdit.setText(data.bndcd);
                        mini.get("bondName").setValue(data.bndnm_en);
                        mini.get("bondName").setText(data.bndnm_en);
                        mini.get("startDate").setValue(data.issudt);
                        mini.get("matdt").setValue(data.matdt);
                        mini.get("firstSettlementDate").setValue(null);
                        mini.get("secondSettlementDate").setValue(null);
                        //添加产品,产品类型,计息基础
                        mini.get("bondProduct").setValue(data.product);
                        mini.get("bondProduct").setText(data.product);
                        mini.get("bondProdType").setValue(data.prodtype);
                        mini.get("bondProdType").setText(data.prodtype);
//                         mini.get("basis").setValue(data.basis);

                        //逆回购 出钱设置授信主体
                        var myDir = mini.get("myDir").getValue();
                        if (myDir == 'P') {
                            if ("1" == mini.get("quotaOccupyType").value) {
                                //额度占用类型为发行人时。填充授信信息
                                creditsubvalue(null, "442", data.bndcd);
                            }
                        }

                        //queryCustNo(data.issuer); //债券发行方机构
                        btnEdit.focus();
                    }
                }

            }
        });
    }

    function onCreditTypeChange() {
        var bondCode = mini.get("bondCode").getValue();
        creditsubvalue(null, "442", bondCode);
    }

    //到期结算日
    function onDrawDateEnd(e) {
        var date = e.date;
        var d = mini.get("firstSettlementDate").getValue();
        var startdate = new Date(mini.get("startDate").getValue());
        var matdt = new Date(mini.get("matdt").getValue());
        if (startdate != null && startdate != "") {
            if (date.getTime() < (startdate.getTime() - 28800000)) {
                e.allowSelect = false;
            }
        }
        if (d != null && d != "") {
            if (date.getTime() < d.getTime()) {
                e.allowSelect = false;
            }
        }
        if (matdt != null && matdt != "") {
            if ((matdt.getTime() - 28800000 * 3) < date.getTime()) {
                e.allowSelect = false;
            }
        }

    }

    //首次结算日
    function onDrawDateStart(e) {
        var date = e.date;
        var d = mini.get("secondSettlementDate").getValue();
        var startdate = new Date(mini.get("startDate").getValue());
        var fordate = mini.get("forDate").getValue();
        if (startdate != null && startdate != "") {
            if (date.getTime() < (startdate.getTime() - 28800000)) {
                e.allowSelect = false;
            }
        }
        if (d != null && d != "") {
            if (date.getTime() > d.getTime()) {
                e.allowSelect = false;
            }
        }
        if (fordate != null && fordate != "") {
            if (fordate.getTime() > date.getTime()) {
                e.allowSelect = false;
            }
        }
    }

    //首次结算日 值改变时发生
    function onStartDateChanged(e) {
        var startDate = mini.get("firstSettlementDate").getFormValue();
        var endDate = mini.get("secondSettlementDate").getFormValue();
        if (endDate == null || endDate == "") {
            return;
        }
        var diffDays = CommonUtil.dateDiff(startDate, endDate);
        mini.get("occupancyDays").setValue(diffDays);
        mini.get("tenor").setValue(diffDays);
    }

    //到期结算日 值改变时发生
    function onEndDateChanged(e) {
        var endDate = mini.get("secondSettlementDate").getFormValue();
        var startDate = mini.get("firstSettlementDate").getFormValue();
        if (startDate == null || startDate == "") {
            return;
        }
        var diffDays = CommonUtil.dateDiff(startDate, endDate);
        mini.get("occupancyDays").setValue(diffDays);
        mini.get("tenor").setValue(diffDays);
        searchProductWeightNew();
    }

    //首次收益率 值改变时发生->首次应计利息=首次收益率x期限/365？
    function onFirstYieldChanged() {

    }


    //首次应计利息/首次净价  值改变时发生->首次全价=首次净价+首次应计利息
    function onFirstInterestChanged() {
        var firstIst = mini.get("firstAccuredInterest").getValue();//首次应计利息
        var firstPrice = mini.get("firstCleanPrice").getValue();//首次净价
        var sum = CommonUtil.accAdd(firstPrice, firstIst);//首次净价+首次应计利息
        mini.get("firstDirtyPrice").setValue(sum);//设置首次全价
    }


    //到期应计利息/到期净价 值改变时发生->到期全价=到期净价+到期应计利息
    function onSecondInterestChanged() {
        var secondIst = mini.get("secondAccuredInterest").getValue();//到期应计利息
        var secondPrice = mini.get("secondCleanPrice").getValue();//到期净价
        var sum = CommonUtil.accAdd(secondPrice, secondIst);//到期净价+到期应计利息
        mini.get("secondDirtyPrice").setValue(sum);//设置到期全价
    }

    function firstDirtyPriceChanged() {
        var firstAccuredInterest = mini.get("firstDirtyPrice").getValue() - mini.get("firstCleanPrice").getValue();//首次应计利息 = 首次净价-首次全价
        mini.get("firstAccuredInterest").setValue(firstAccuredInterest);

    }

    //首次全价/券面总额 值改变时发生->首次结算金额=(首次全价[元]x券面总额)/100
    function onFirstPriceChanged() {
        var firstIst = mini.get("firstAccuredInterest").getValue();//首次应计利息
        var firstPrice = mini.get("firstCleanPrice").getValue();//首次净价
        var firstTotal = CommonUtil.accAdd(firstPrice, firstIst);//首次净价+首次应计利息
        mini.get("firstDirtyPrice").setValue(firstTotal);//设置首次全价
        //var firstTotal = mini.get("firstDirtyPrice").getValue();//首次全价
        //var secondTotal = mini.get("secondDirtyPrice").getValue();//到期全价
        var faceTotal = mini.get("totalFaceValue").getValue();//券面总额
        var mul = CommonUtil.accMul(firstTotal, faceTotal);//首次全价x券面总额
        //var mul2 = CommonUtil.accMul(secondTotal,faceTotal);//到期全价x券面总额
        var result = CommonUtil.accMul(mul, 100);
        //var result2 = CommonUtil.accMul(mul2,100);
        mini.get("firstSettlementAmount").setValue(result);//设置首次结算金额
        //mini.get("secondSettlementAmount").setValue(result2);//设置到期结算金额repoRate
        var repoRate = mini.get("repoRate").getValue();//回购利率
        repoRate = CommonUtil.accDiv(repoRate, 100);
        var occupancyDays = mini.get("occupancyDays").getValue();//实际占款天数
        var basis = mini.get("basis").getValue();//计息基准
        basis = basisTonumber(basis);

        var ins = CommonUtil.accDiv(CommonUtil.accMul(CommonUtil.accMul(result, repoRate), occupancyDays), basis);//回购利息
        var secondSettlementAmount = CommonUtil.accAdd(result, ins);//到期结算金额
        mini.get("secondSettlementAmount").setValue(secondSettlementAmount);//设置到期结算金额repoRate

        var tradingProduct = mini.get("tradingProduct").getValue();//交易品种
        var repoRate = mini.get("repoRate").getValue();//回购利率
        if (CommonUtil.isNull(tradingProduct) || CommonUtil.isNull(repoRate)) {
            return;
        }
        CommonUtil.ajax({
            url: "/IfsLimitController/queryRhisIntrate",
            data: {'prdNo': prdNo, 'tradingProduct': tradingProduct, 'repoRate': repoRate},
            callback: function (data) {
                if (data != null && data != "") {
                    mini.get("priceDeviation").setValue(data);
                } else {
                    mini.get("priceDeviation").setValue();
                }
            }
        });

    }

    //根据首次结算金额计算到期结算金额
    function sumSecondSettlementAmount() {
        var firstSettlementAmount = mini.get("firstSettlementAmount").getValue();//首次结算金额
        var repoRate = mini.get("repoRate").getValue();//回购利率
        var occupancyDays = mini.get("occupancyDays").getValue();//实际占款天数
        var basis = mini.get("basis").getValue();//计息基准
        basis = basisTonumber(basis);
        var secondSettlementAmount = CommonUtil.accMul(firstSettlementAmount, repoRate);
        secondSettlementAmount = CommonUtil.accDiv(secondSettlementAmount, 100);
        secondSettlementAmount = CommonUtil.accMul(secondSettlementAmount, occupancyDays);
        secondSettlementAmount = CommonUtil.accDiv(secondSettlementAmount, basis);
        secondSettlementAmount = CommonUtil.accAdd(secondSettlementAmount, firstSettlementAmount);
        mini.get("secondSettlementAmount").setValue(secondSettlementAmount);
    }

    //到期结算金额计算新，到期结算金额=首次结算金额+首次结算金额*回购利率*实际占款天数/365
    function sumSecondSettlementAmountNew() {
        var firstSettlementAmount = mini.get("firstSettlementAmount").getValue();//首次结算金额
        var repoRate = mini.get("repoRate").getValue();//回购利率
        var occupancyDays = mini.get("occupancyDays").getValue();//实际占款天数

        if (CommonUtil.isNull(firstSettlementAmount) || CommonUtil.isNull(repoRate) || CommonUtil.isNull(occupancyDays)) {
            return;
        }
        var secondSettlementAmount = CommonUtil.accMul(firstSettlementAmount, repoRate);
        secondSettlementAmount = CommonUtil.accDiv(secondSettlementAmount, 100);
        secondSettlementAmount = CommonUtil.accDiv(secondSettlementAmount, 365);
        secondSettlementAmount = CommonUtil.accMul(secondSettlementAmount, occupancyDays);
        secondSettlementAmount = CommonUtil.accAdd(secondSettlementAmount, firstSettlementAmount);
        mini.get("secondSettlementAmount").setValue(secondSettlementAmount);

        searchPriceDeviation();

    }


    /**   清算路径1的选择   */
    function settleMeans1() {
        var cpInstId = mini.get("reverseInst").getValue();
        if (cpInstId == null || cpInstId == "") {
            mini.alert("请先选择对方机构!");
            return;
        }
        var ccy = "CNY";
        var url = "./Ifs/opics/setaMini.jsp";
        var data = {ccy: ccy, cust: cpInstId};

        var btnEdit = this;
        mini.open({
            url: url,
            title: "选择清算路径",
            width: 900,
            height: 600,
            onload: function () {
                var iframe = this.getIFrameEl();
                iframe.contentWindow.SetData(data);
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data1 = iframe.contentWindow.GetData();
                    data1 = mini.clone(data1);    //必须
                    if (data1) {
                        mini.get("ccysmeans").setValue($.trim(data1.smeans));
                        mini.get("ccysmeans").setText($.trim(data1.smeans));
                        mini.get("ccysacct").setValue($.trim(data1.sacct));
                        btnEdit.focus();
                    }
                }
            }
        });
    }

    /**   清算路径2的选择   */
    function settleMeans2() {
        var cpInstId = mini.get("reverseInst").getValue();
        if (cpInstId == null || cpInstId == "") {
            mini.alert("请先选择对方机构!");
            return;
        }
        var ccy = "CNY";
        var url = "./Ifs/opics/setaMini.jsp";
        var data = {ccy: ccy, cust: cpInstId};

        var btnEdit = this;
        mini.open({
            url: url,
            title: "选择清算路径",
            width: 900,
            height: 600,
            onload: function () {
                var iframe = this.getIFrameEl();
                iframe.contentWindow.SetData(data);
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data1 = iframe.contentWindow.GetData();
                    data1 = mini.clone(data1);    //必须
                    if (data1) {
                        mini.get("ctrsmeans").setValue($.trim(data1.smeans));
                        mini.get("ctrsmeans").setText($.trim(data1.smeans));
                        mini.get("ctrsacct").setValue($.trim(data1.sacct));
                        btnEdit.focus();
                    }
                }

            }
        });
    }

    //托管机构选择
    function onSaccQuery(e) {
        var btnEdit = this;
        mini.open({
            url: "./Ifs/opics/saccMini.jsp",
            title: "选择托管机构列表",
            width: 900,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.accountno);
                        btnEdit.setText(data.accountno);
                        btnEdit.focus();
                    }
                }

            }
        });
    }

    //逆回购需要授信，正回购不需要
    function dirVerify() {
        var myDir = mini.get("myDir").getValue();
        if (myDir == 'S') {//正回购
            mini.get("panel1").setVisible(false);
            mini.get("quotaOccupyType").setVisible(false);
            mini.get("oppoDir").setValue("P");
            mini.get("applyNo").setVisible(false);//信用风险审查单号：
            mini.get("applyProd").setVisible(false);//产品名称
            mini.get("custNo").setVisible(false);
            mini.get("weight").setVisible(false);
            /* mini.get("weight").setValue("0");//权重
            mini.get("weight").setText("0");//权重 */
            mini.get("applyNo").setVisible(false);//信用风险审查单号：
            mini.get("applyProd").setVisible(false);//产品名称
            mini.get("reverseTrader").setStyle("margin-bottom: 2px");
            mini.get("prodType").setText("RB");
            mini.get("prodType").setValue("RB");
        } else if (myDir == 'P') {//逆回购
            if ("0" != mini.get("quotaOccupyType").getValue()) {
                mini.get("panel1").setVisible(true);
            }
            mini.get("quotaOccupyType").setVisible(true);
            mini.get("oppoDir").setValue("S");
            mini.get("applyNo").setVisible(true);//信用风险审查单号：
            mini.get("applyProd").setVisible(true);//产品名称
            mini.get("custNo").setVisible(true);//占用授信主体
            mini.get("weight").setVisible(true);//权重
            mini.get("reverseTrader").setStyle("margin-bottom: 36px");
            //searchProductWeightNew();
            mini.get("prodType").setText("VB");
            mini.get("prodType").setValue("VB");
            var bondCode = mini.get("bondCode").getValue();
            creditsubvalue(null, "442", bondCode);
        }
    }

    /* 成功中心的选择 */
    function onBondCostEdit() {
        var btnEdit = this;
        var data = {prdName: "SECUR"};
        mini.open({
            url: CommonUtil.baseWebPath() + "/opics/costMiniLess.jsp",
            title: "选择成本中心",
            width: 900,
            height: 500,
            onload: function () {
                var iframe = this.getIFrameEl();
                iframe.contentWindow.SetData(data);
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.costcent);
                        btnEdit.setText(data.costcent);
                        btnEdit.focus();
                    }
                }

            }
        });
    }

    /* 券的产品代码的选择 */
    function onBondPrdEdit() {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "/opics/prodMiniLess.jsp",
            title: "选择产品代码",
            width: 900,
            height: 500,
            onload: function () {
                var iframe = this.getIFrameEl();
                iframe.contentWindow.SetData({});
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.pcode);
                        btnEdit.setText(data.pcode);
                        onBondPrdChanged();
                        btnEdit.focus();
                    }
                }

            }
        });
    }

    function onBondPrdChanged() {
        if (mini.get("bondProdType").getValue() != "") {
            mini.get("bondProdType").setValue("");
            mini.get("bondProdType").setText("");
            mini.alert("产品代码已改变，请重新选择产品类型!");
        }
    }

    /* 券产品类型的选择 */
    function onBondTypeEdit() {
        var prd = mini.get("bondProduct").getValue();
        if (prd == null || prd == "") {
            mini.alert("请先选择产品代码!");
            return;
        }
        var data = {prd: prd};
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "/opics/typeMiniLess.jsp",
            title: "选择产品类型",
            width: 900,
            height: 500,
            onload: function () {
                var iframe = this.getIFrameEl();
                iframe.contentWindow.SetData(data);
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.type);
                        btnEdit.setText(data.type);
                        btnEdit.focus();
                    }
                }

            }
        });
    }

    /* 券投资组合的选择 */
    function onBondPortEdit() {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "/opics/portMiniLess.jsp",
            title: "选择投资组合",
            width: 900,
            height: 500,
            onload: function () {
                var iframe = this.getIFrameEl();
                iframe.contentWindow.SetData({});
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.portfolio);
                        btnEdit.setText(data.portfolio);
                        btnEdit.focus();
                    }
                }

            }
        });

    }

    function searchProductWeightNew() {
        var form = new mini.Form("field_form");
        var data = form.getData(true);
        var mDate = data.secondSettlementDate;
        var vDate = data.forDate;//交易日期
        if (CommonUtil.isNull(mDate) || CommonUtil.isNull(vDate)) {
            return;
        }
        searchProductWeight(20, 0, prdNo, mDate);
    }

    /* 成功中心的选择 */
    //    function onCostEdit(){
    //    	var btnEdit = this;
    //        mini.open({
    //            url: CommonUtil.baseWebPath() + "/opics/costMiniLess.jsp",
    //            title: "选择成本中心",
    //            width: 900,
    //            height: 500,
    //            ondestroy: function (action) {
    //                if (action == "ok") {
    //                    var iframe = this.getIFrameEl();
    //                    var data = iframe.contentWindow.GetData();
    //                    data = mini.clone(data);    //必须
    //                    if (data) {
    //                        btnEdit.setValue(data.costcent);
    //                        btnEdit.setText(data.costcent);
    //                        //searchMarketData();
    //                        btnEdit.focus();
    //                    }
    //                }
    //            }
    //        });
    //    }

    function searchMarketData() {
        var costCent = mini.get("cost").getValue();//成本中心
        var code = mini.get("invtype").getValue();//投资类型
        if (CommonUtil.isNull(code) || CommonUtil.isNull(costCent)) {
            return;
        }
        queryMarketData(code, costCent);
    }

    //获取DV01，久期限，止损值查询
    function queryMarketData(code, costCent) {
        CommonUtil.ajax({
            url: "/IfsOpicsCustController/queryMarketData",
            data: {'code': code, 'costCent': costCent},
            callback: function (data) {
                if (data && data.obj) {
                    mini.get("longLimit").setValue(data.obj.duration);//久期限
                    mini.get("lossLimit").setValue(data.obj.convexity);//止损
                    mini.get("DV01Risk").setValue(data.obj.delta);//DV01
                } else {
                    mini.get("longLimit").setValue();//久期限
                    mini.get("lossLimit").setValue();//止损
                    mini.get("DV01Risk").setValue();//DV01
                }
            }
        });
    }

    function searchPriceDeviation() {
        var repoRate = mini.get("repoRate").getValue();//回购利率
        var tradingProduct = mini.get("tradingProduct").getValue();//交易品种
        if (CommonUtil.isNull(tradingProduct) || CommonUtil.isNull(repoRate)) {
            return;
        }
        CommonUtil.ajax({
            url: "/IfsLimitController/queryRhisIntrate",
            data: {'prdNo': prdNo, 'tradingProduct': tradingProduct, 'repoRate': repoRate},
            callback: function (data) {
                if (data != null && data != "") {
                    mini.get("priceDeviation").setValue(data);
                } else {
                    mini.get("priceDeviation").setValue();
                }
            }
        });
    }

</script>
<script type="text/javascript" src="../../Common/Flow/MiniApproveOpCommon.js"></script>
</body>
</html>
