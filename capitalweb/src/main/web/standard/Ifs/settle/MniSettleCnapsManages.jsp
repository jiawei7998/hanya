<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="../../global.jsp"%>
<html>

<head>
<title>大额支付往账交易经办</title>
<script src="<%=basePath %>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>

<body style="width: 100%; height: 100%; background: white">
	<fieldset id="fd1" class="mini-fieldset">
		<legend>
			<label>查询条件</label>
		</legend>
		<div id="search_form" style="width: 100%;">
				<input id="br" name="br" class=" mini-hidden mini-combobox mini-mustFill" width="320px" labelField="true" label="部门：" vtype="maxLength:5" required="true"
	              emptyText="请填写部门" labelStyle="text-align:right;"  value='<%=__sessionUser.getOpicsBr()%>' data="CommonUtil.serverData.dictionary.opicsBr"/>
			    <input id="dealNo_s" name="dealNo_s" class="mini-textbox" width="320px" labelField="true" label="opics编号："
					labelStyle="text-align:right;"  emptyText="请输入opics编号"/>
				<!-- <input id="dealNo_s" name="dealNo_s" class="mini-textbox" width="320px" labelField="true" label="流水号："
					labelStyle="text-align:right;" emptyText="" /> -->
				<input id="cname" name="cname" class="mini-textbox" width="320px" labelField="true" label="交易对手名称："
					labelStyle="text-align:right;" emptyText="请输入交易对手名称"/>
				<input id="product_s" name="product_s" class="mini-combobox" width="320px" labelField="true" label="产品类型："
					textField="typeValue" valueField="typeId" labelStyle="text-align:right;" emptyText="请选择产品类型"/>
				<!-- <input id="hvpType" name="hvpType" class="mini-combobox" width="320px" labelField="true" label="汇款方式：" value="A200"
					labelStyle="text-align:right;" data="[{text:'行间资金划拨',id:'A200'},{text:'普通汇兑',id:'A100'}]" /> -->
				<nobr>
				<input id="startDate" name="startDate" class="mini-datepicker" width="320px" labelField="true" label="结算日期："
							ondrawdate="onDrawDateStart" labelStyle="text-align:right;" emptyText="起始日期" format="yyyy-MM-dd" value="<%=__bizDate%>"/>
				<span>~</span>
				<input id="endDate" name="endDate" class="mini-datepicker" width="320px"
							ondrawdate="onDrawDateEnd" emptyText="结束日期" format="yyyy-MM-dd" value="<%=__bizDate%>"/>
				</nobr>
<%--				<input id="vdate" name="vdate" class="mini-datepicker" width="320px" labelField="true" label="结算日期："--%>
<%--					labelStyle="text-align:right;" emptyText="请选择结算日期" value="<%=__bizDate%>"/>--%>
				<span style="float: right; margin-right: 50px">
					<a id="search_btn" class="mini-button" style="display: none"  onclick="query()">查询</a>
					<a id="clear_btn" class="mini-button" style="display: none"  onClick="clear">清空</a>
				</span>
		</div>
	</fieldset>
	<fieldset id="fd2" class="mini-fieldset">
		<legend>
			<label>大额支付往账交易经办</label>
		</legend>
		<div id="find2" class="fieldset-body">
			<table id="field_form" class="form-table" width="100%">
				<tr>
					<!-- <td>收款人编号：</td>
					<td><input id="cno" name="cno" readonly="true"
						class="mini-textbox" enabled="false" style="width: 90%" /></td> -->
					<td>opics编号：</td>
					<td><input id="dealno" name="dealno" readonly="true" class="mini-textbox" enabled="false"
						style="width: 90%" /></td>
					<td>生成流水号：</td>
					<td><input id="fedealno" name="accocertno" class="mini-textbox" required="true"  enabled="false"
						style="width: 90%" allowInput="true"/></td>
				</tr>
				<!-- <tr>
					<td>证件发行国家：</td>
					<td><input id="cerIssuerCountry" name="cerIssuerCountry"
						style="width: 90%" value="IC-资金系统组织机构代码" readonly="true"
						class="mini-textbox" enabled="false" /></td>
					<td>证件类型：</td>
					<td><input id="cerType" name="cerType" style="width: 90%"
						value="CN-中华人民共和国" readonly="true" class="mini-textbox"
						enabled="false" /></td>
				</tr> -->
				<tr>
					<td>付款人账号：<i class='red-star'></i></td>
					<td><input id="payUserid" name="payUserid" style="width: 90%" class="mini-textbox" allowInput="true"
						maxLength="32" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" required="true"  /></td>
					<td>收款人账号：<i class='red-star'></i></td>
					<td><input id="recUserid" name="recUserid" style="width:90%" class="mini-textbox"
						allowInput="true" maxLength="32"
						onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" required="true"  /></td>
				</tr>
				<tr>
					<td>付款人名称：<i class='red-star'></i></td>
					<td><input id="payUsername" name="payUsername" style="width: 90%" class="mini-textbox"
						required="true"  vtype="maxLength:50" /></td>
					<td>收款人名称：<i class='red-star'></i></td>
					<td><input id="recUsername" name="recUsername" style="width: 90%" class="mini-textbox"
						required="true"  vtype="maxLength:50" /></td>
				</tr>
				<tr>
					<td>付款行行号：<i class='red-star'></i></td>
					<td><input id="payBankid" name="payBankid" style="width: 90%" required="true"
						class="mini-textbox" maxLength="14" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" />
					<td>收款行行号：<i class='red-star'></i></td>
					<td><input id="recBankid" name="recBankid" style="width:90%" required="true"
						class="mini-textbox" maxLength="14" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" />
				</tr>
				<tr>
					<td>付款行名称：<i class='red-star'></i></td>
					<td><input id="payBankname" name="payBankname" style="width: 90%" class="mini-textbox"
						required="true"  vtype="maxLength:50" /></td>
					</td>
					<td>收款行名称：<i class='red-star'></i></td>
					<td><input id="recBankname" name="recBankname" style="width: 90%" class="mini-textbox"
						required="true"  vtype="maxLength:50" /></td>
				</tr>
				<!-- <tr>
					<td>收款人开户行：</td>
					<td><input id="recBankId_1" name="recBankId_1" style="width: 90%" readonly="true"
						class="mini-textbox" enabled="false" /></td>
					<td>开户行名称：</td>
					<td><input id="recBankName_1" name="recBankName_1"
						style="width: 90%" readonly="true" class="mini-textbox" enabled="false" /></td>
				</tr> -->
				<tr>
					<td>业务类型：</td>
					<td><input id="hvpType" name="hvpType" style="width: 90%" class="mini-combobox"
						data="[{text:'A200-行间划拨',id:'A200'},{text:'A100-普通汇兑',id:'A100'}]" />
					</td>
					<td>币种代码：</td>
					<td><input id="ccy" name="ccy" readonly="true"
						style="width: 90%" value="RMB=人民币" class="mini-textbox" enabled="false" /></td>
				</tr>
				<tr>
					<td>金额：</td>
					<td><input id="amount" name="amount" style="width: 90%"
						 class="mini-textbox" enabled="false"/></td>
					<td>手续费：</td>
					<td><input id="handleAmt" name="handleAmt" readonly="true" style="width: 90%" format="n2"value="0.00"
						class="mini-spinner" changeOnMousewheel="false" enabled="false" /></td>
				</tr>
				<tr>
					<td>支付优先级：</td>
					<td><input id="level" name="level" style="width: 90%" value="HIGH-紧急" readonly="true"
						class="mini-textbox" enabled="false" /></td>
					<td>附言：</td>
					<td><input id="remarkFy" name="remarkFy" style="width: 90%" class="mini-textbox" vtype="maxLength:50" /></td>
				</tr>
			</table>
		</div>
		<span class="btn_div"
			style="float: left; margin: 15px; color: #0065d2;"> *
			双击列表将清算信息详情展示后再进行交易经办处理。 </span>
		<div class="btn_div" style="float: right; margin: 15px">
			<a id="send_btn" class="mini-button" style="display: none"  onClick="btn_send">交易经办</a>
		</div>
	</fieldset>
	<div id="SettleCnapsManages" class="mini-datagrid borderAll" style="width: 100%; height: 380px;" allowResize="true"
		pageSize="10" sortMode="client" allowAlternating="true" frozenStartColumn="0" frozenEndColumn="1">
		<div property="columns">
			<div type="indexcolumn" headerAlign="center" align="center" width="50px">序号</div>
			<div field="amount" width="120" numberFormat="#,0.00" align="right" headerAlign="center" allowSort="true" renderer='fmoney'>结算金额</div>
			<!-- <div field="cno" width="120" align="left" headerAlign="center" allowSort="false">客户号</div> -->
			<div field="cname" width="250" align="center" headerAlign="center" allowSort="false">交易对手名称</div>
			<div field="payrecind" width="80" align="center" headerAlign="center" allowSort="false" renderer="payrecnd">方向</div>
			<div field="ccy" width="80" align="center" headerAlign="center" allowSort="true">币种</div>
			<div field="vdate" width="120" align="center" headerAlign="center" allowSort="false" renderer="onDateRenderer">结算日期</div>
			<div field="sn" width="150" align="center" headerAlign="center" allowSort="false">生成流水号</div>
			<div field="fedealno" width="150" align="center" headerAlign="center" allowSort="false">审批单号</div>
			<div field="dealno" width="100" align="center" headerAlign="center" allowSort="false">opics编号</div>
			<div field="voidflag" width="100" align="center" headerAlign="center" allowSort="false" renderer="CommonUtil.dictRenderer" data-options="{dict:'voidflag'}">opics状态</div>
			<div field="product" width="80" align="center" headerAlign="center" allowSort="false">产品</div>
			<div field="prodtype" width="80" align="center" headerAlign="center" allowSort="false">产品类型</div>
			<div field="payUserid" width="200" align="center" headerAlign="center" allowSort="false">付款行行号</div>
			<div field="payUsername" width="200" align="center" headerAlign="center" allowSort="false">付款行名称</div>
			<div field="payBankid" width="200" align="center" headerAlign="center" allowSort="false">付款人账号</div>
			<div field="payBankname" width="200" align="center" headerAlign="center" allowSort="false">付款人名称</div>
			<div field="recBankid" width="200" align="center" headerAlign="center" allowSort="false">收款行行号</div>
			<div field="recBankname" width="200" align="center" headerAlign="center" allowSort="false">收款行名称</div>
			<div field="recUserid" width="200" align="center" headerAlign="center" allowSort="false">收款人账号</div>
			<div field="recUsername" width="200" align="center" headerAlign="center" allowSort="false">收款人名称</div>
		</div>
	</div>
	<script type="text/javascript">
		mini.parse();

		var grid = mini.get("SettleCnapsManages");
		var form1 = new mini.Form("field_form");
		
		grid.on("beforeload", function(e) {
			e.cancel = true;
			var pageIndex = e.data.pageIndex;
			var pageSize = e.data.pageSize;
			search(pageSize, pageIndex);
		});
		
		function onDateRenderer(e) {
			var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)==null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g,'/'));
			if (value) return mini.formatDate(value, 'yyyy-MM-dd');
		}
		
		function query() {
			search(grid.pageSize, 0);
		}

		/* function btn_send() {
			form1.validate();
			if (form1.validate() == false) {
				mini.alert("表单验证错误,请重新填写!", "消息提示")
				return;
			} */
			/* var data = form.getData(true);
			param['dealno'] = row.dealno;
			var param = mini.encode(data);
			CommonUtil.ajax({
				url : "/TrdSettleController/submitSettle",
				data : param,
				callback : function(data) {
					if (data.code == 'error.common.0000') {
						mini.alert("保存成功", '提示信息');
					} else {
						mini.alert("保存失败");
					}
				}
			});
		} */
		//金额格式化
		function fmoney(e) {
			var s=typeof(e.value)=="undefined"?e:e.value;
			s = parseFloat((s + "").replace(/[^\d\.-]/g, "")).toFixed(2) + "";
			var l = s.split(".")[0].split("").reverse(), r = s.split(".")[1];
			t = "";
			for (var i = 0; i < l.length; i++) {
			t += l[i] + ((i + 1) % 3 == 0 && (i + 1) != l.length ? "," : "");
			}
			return t.split("").reverse().join("") + "." + r;
			}
		function search(pageSize, pageIndex) {
			var form = new mini.Form("search_form");
			form.validate(); //表单验证
			if (form.isValid() == false) {
				mini.alert("表单填写错误,请确认!", "提示信息");
				return;
			}
			var data = form.getData(true); //获取表单数据
			data['pageNumber'] = pageIndex + 1;
			data['pageSize'] = pageSize;
			var param = mini.encode(data); //序列化json格式
			CommonUtil.ajax({
				url : "/TrdSettleController/searchSubmitSettleInstList",
				data : param,
				callback : function(data) {
					var grid = mini.get("SettleCnapsManages");
					//设置分页
					grid.setTotalCount(data.obj.total);
					grid.setPageIndex(pageIndex);
					grid.setPageSize(pageSize);
					grid.setData(data.obj.rows);
				}
			});
		}

		var payRecive = [ {
			id : 'P',
			text : '付'
		}, {
			id : 'R',
			text : '收'
		} ];

		function payrecnd(e) {
			for ( var i = 0, l = payRecive.length; i < 2; i++) {
				var g = payRecive[i];
				if (g.id == e.value)
					return g.text;
			}
			return " ";
		}
		
		// 交易经办
		function btn_send() {
			var row = grid.getSelected();
			if (row) {
				if(row.voidflag!='0'){
					mini.alert("该笔大额支付交易已作废或撤销！", "系统提示");
					return;
				}
				
				var form1 = new mini.Form("field_form");
				form1.validate();
				if (form1.isValid() == false) {
					mini.alert("表单填写有误，请检查！", "系统提示");
					return;
				}
				var data = form1.getData();
				data['fedealno'] = row.fedealno;
				var param = mini.encode(data);
				mini.confirm("确认提交该笔大额支付交易吗", "操作提示", function(value) {
					if (value == "ok") {
						CommonUtil.ajax({
							url : "/TrdSettleController/submitSettle",
							data : param,
							callback : function(data) {
								if (data.code = "error.common.0000") {
									mini.alert(data.desc, "系统提示", function() {
										form1.clear();
										mini.get("ccy").setValue("RMB=人民币");
										mini.get("level").setValue("HIGH-紧急");
										//mini.get("cerIssuerCountry").setValue("IC-资金系统组织机构代码");
										//mini.get("cerType").setValue("CN-中华人民共和国");
									});
									search(grid.pageSize, 0);
								} else {
									mini.alert(data.desc, "系统提示");
									search(grid.pageSize, 0);
								}
							}
						});
					}
				});
			} else {
				mini.alert("请先选择一条记录操作", "提示");
			}
		}

		function clear() {
			var form = new mini.Form("#search_form");
			form.clear();
			var find2 = new mini.Form("#find2");
			find2.clear();
			mini.get("br").setValue(opicsBr);//自贸区加br
			search(10, 0);
		}

		grid.on("select", function(e) {
			var row = e.record;
			//var cno = mini.get("cno");
			//cno.setValue(row.cno);
			//cno.setText(row.cno);
			
			//资金编号
			var dealno = mini.get("dealno");
			dealno.setValue(row.dealno);
			dealno.setText(row.dealno);
			//审批单编号
 			var fedealno=mini.get("fedealno");
 			fedealno.setValue(row.fedealno);
 			fedealno.setText(row.fedealno);
			//付款人账号
			var payUserid = mini.get("payUserid");
			payUserid.setValue(row.payUserid);
			payUserid.setText(row.payUserid);
			//付款人名称
			var payUsername = mini.get("payUsername");
			payUsername.setValue(row.payUsername);
			payUsername.setText(row.payUsername);
			
			//兑换组号
			//var userdealno = mini.get("userdealno");
			//userdealno.setValue(row.userdealno);
			//userdealno.setText(row.userdealno);
			//付款行行号
			var payBankid = mini.get("payBankid");
			payBankid.setValue(row.payBankid);
			payBankid.setText(row.payBankid);
			//付款行名称
			var payBankname = mini.get("payBankname");
			payBankname.setValue(row.payBankname);
			payBankname.setText(row.payBankname);
			//收款人账号
			var recUserid = mini.get("recUserid");
			recUserid.setValue(row.recUserid);
			recUserid.setText(row.recUserid);
			//收款人名称
			var recUsername = mini.get("recUsername");
			recUsername.setValue(row.recUsername);
			recUsername.setText(row.recUsername);
			//收款行行号
			var recBankid = mini.get("recBankid");
			recBankid.setValue(row.recBankid);
			recBankid.setText(row.recBankid);
			//收款行名称
			var recBankname = mini.get("recBankname");
			recBankname.setValue(row.recBankname);
			recBankname.setText(row.recBankname);

			//业务类型
			var hvpType = mini.get("hvpType");
			hvpType.setValue("A100");
			//hvpType.setValue(row.hvpType);
			//hvpType.setText(row.hvpType);
			//金额
			var amount=mini.get("amount");
	     	amount.setValue(row.amount);
		    amount.setText(fmoney(row.amount));

			// var handleAmt=mini.get("handleAmt");
			// handleAmt.setValue(row.handleAmt);
			// handleAmt.setText(fmoney(row.handleAmt));

			var remarkFy = mini.get("remarkFy");
			remarkFy.setValue(row.remarkFy);
			remarkFy.setText(row.remarkFy);
		});

		function onButtonEdit(e) {
			var btnEdit = this;
			mini.open({
				url : CommonUtil.baseWebPath() + "../../Common/CustMini.jsp",
				title : "选择对手方列表",
				width : 900,
				height : 600,
				ondestroy : function(action) {
					if (action == "ok") {
						var iframe = this.getIFrameEl();
						var data = iframe.contentWindow.GetData();
						data = mini.clone(data); //必须
						if (data) {
							btnEdit.setValue(data.cname);
							btnEdit.setText(data.cname);
							btnEdit.focus();
						}
					}
				}
			});
		}

		$(document).ready(function() {
			//控制按钮显示
			$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
				queryProdtype();
				search(10, 0);
			});
		});
		
		//查询产品类型
		function queryProdtype(){
			CommonUtil.ajax({
				url:"/IfsOpicsTypeController/searchProdtype",
				data:{},
				callback:function(data){
					var length = data.length;
	             	var jsonData = new Array();
	 			   	for(var i=0;i<length;i++){
	 			   		if(data[i].type != null){
	 			   			var tyId = data[i].type;
		 			   		jsonData.add({'typeId':tyId,'typeValue':tyId});
	 			   		}
	 			   	};
	 			   	mini.get("product_s").setData(jsonData);
	             }
	   		});
	    }
		
		//交易日期
		function onDrawDateStart(e) {
	        var startDate = e.date;
	        var endDate= mini.get("endDate").getValue();
	        if(CommonUtil.isNull(endDate)){
	        	return;
	        }
	        if (endDate.getTime() < startDate.getTime()) {
	            e.allowSelect = false;
	        }
	    }
		
		function onDrawDateEnd(e) {
	        var endDate = e.date;
	        var startDate = mini.get("startDate").getValue();
	        if(CommonUtil.isNull(startDate)){
	        	return;
	        }
	        if (endDate.getTime() < startDate.getTime()) {
	            e.allowSelect = false;
	        }
	    }
	</script>
</body>
</html>