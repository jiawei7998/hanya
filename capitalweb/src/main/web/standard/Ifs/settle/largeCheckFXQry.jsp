<%--
  Created by IntelliJ IDEA.
  User: hspcadmin
  Date: 2021/10/18
  Time: 21:52
  To change this template use File | Settings | File Templates.
--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../../global.jsp" %>
<html>
<head>
    <title>国际部支付凭证</title>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/hideBtn.js"></script>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset title">
    <legend>国际部支付凭证</legend>
    <div id="search_form" style="width: 100%;">
        <input id="dealno" name="dealno" class="mini-textbox" width="320px" labelField="true" label="流水号："
               labelStyle="text-align:right;" emptyText="流水号"/>
        <input id="product" name="product" class="mini-textbox" width="320px" emptyText="产品代码" labelField="true"
               label="产品代码：" labelStyle="text-align:right;"/>
        <input id="fedealno" name="fedealno" class="mini-textbox" width="320px" emptyText="审批单号" labelField="true"
               label="审批单号：" labelStyle="text-align:right;"/>
        <input id='vdate' name='vdate' class='mini-datepicker' width="320px" style='width:320px' labelField='true'
               label='付款日期：' labelStyle='text-align:right'/>
        <span style="float:right;margin-right: 150px">
            <a class="mini-button" style="display: none" id="search_btn" onclick="search()">查询</a>
            <a class="mini-button" style="display: none" id="clear_btn" onclick="clear()">清空</a>
            <a class="mini-button" id="print_btn" onclick="printView()">清算套打</a>
        </span>
    </div>
</fieldset>

<div class="mini-fit" style="width:100%;height:100%;">
    <div id="datagrid" class="mini-datagrid borderAll" style="height:100%;" idField="setno" allowResize="true"
         multiSelect="true" allowCellEdit="true" border="true">
        <div property="columns">
            <div type="indexcolumn" headerAlign="center" align="center" width="40px">序列</div>
            <div field="fedealno" headerAlign="center" align="left" width="120px">审批单号</div>
            <div field="dealno" headerAlign="center" align="left" width="120px">流水号</div>
            <div field="settflag" headerAlign="center" align="center" width="120px" renderer="CommonUtil.dictRenderer"
                 data-options="{dict:'settflag'}">处理状态
            </div>
            <div field="product" headerAlign="center" align="center" width="120px">产品代码</div>
            <div field="prodtype" headerAlign="center" align="center" width="120px">产品类型</div>
            <div field="cname" headerAlign="center" align="center" width="160px">交易对手</div>
            <div field="vdate" width="100" align="center" headerAlign="center" renderer="onDateRenderer">付款日期</div>
            <%--            <div field="payrecind" headerAlign="center"  align="center" width="120px" renderer="CommonUtil.dictRenderer" data-options="{dict:'payDirection'}" >收付方向 </div>--%>
            <%--            <div field="ccy" headerAlign="center" width="120px" renderer="CommonUtil.dictRenderer" data-options="{dict:'Currency'}" >币种</div>--%>
            <div field="amount" width="120" numberFormat="#,0.00" headerAlign="center" align="right" allowSort="true">
                金额
            </div>
            <div field="cdate" width="100" align="center" headerAlign="center" renderer="onDateRenderer">创建日期</div>
            <div field="setmeans" headerAlign="center" align="center" width="120px">清算方式</div>
            <div field="setacct" headerAlign="center" align="left" width="120px">清算账户</div>
            <div field="payBankid" headerAlign="center" align="left" width="200px">付款行行号</div>
            <div field="payBankname" headerAlign="center" align="left" width="200px">付款行行名</div>
            <div field="payUserid" headerAlign="center" align="left" width="200px">付款账号</div>
            <div field="payUsername" headerAlign="center" align="left" width="200px">付款账号名称</div>
            <div field="recBankid" headerAlign="center" align="left" width="200px">收款行行号</div>
            <div field="recBankname" headerAlign="center" align="left" width="200px">收款行行名</div>
            <div field="recUserid" headerAlign="center" align="left" width="200px">收款账号</div>
            <div field="recUsername" headerAlign="center" align="left" width="200px">收款账号名称</div>
            <div field="hurryLevel" headerAlign="center" align="center" width="120px">加急等级</div>
            <div field="ioper" headerAlign="center" align="center" width="120px">经办人员</div>
            <div field="voper" headerAlign="center" align="center" width="120px">复核人员</div>
            <div field="remarkFy" headerAlign="center" align="center" width="120px">附言</div>
            <div field="dealflag" headerAlign="center" align="center" width="120px" renderer="CommonUtil.dictRenderer"
                 data-options="{dict:'dealflag'}">经办状态
            </div>
            <div field="voidflag" headerAlign="center" align="center" width="120px" renderer="CommonUtil.dictRenderer"
                 data-options="{dict:'voidflag'}">撤销标志
            </div>
        </div>
    </div>
</div>
</body>
<script type="text/javascript" src="<%=basePath%>/miniScript/lodop/LodopFuncs.js"></script>
<script type="text/javascript">
    mini.parse();
    var LODOP; //声明为全局变量
    var selectRowData;

    var grid = mini.get("datagrid");
    var form = new mini.Form("#search_form");

    function onDateRenderer(e) {
        var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value) == null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g, '/'));
        if (value) return mini.formatDate(value, 'yyyy-MM-dd');
    }

    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        searchs(pageSize, pageIndex);
    });


    function data_string(str) {
        var d = eval('new ' + str.substr(1, str.length - 2));
        var ar_date = [d.getFullYear(), d.getMonth() + 1, d.getDate()];
        for (var i = 0; i < ar_date.length; i++) ar_date[i] = dFormat(ar_date[i]);
        return ar_date.join('-');

        function dFormat(i) {
            return i < 10 ? "0" + i.toString() : i;
        }
    }

    //查询按钮
    function search() {
        searchs(grid.pageSize, 0);
    }

    //查询数据
    function searchs(pageSize, pageIndex) {
        var form = new mini.Form("search_form");
        var data = form.getData(true);
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        data['sysDate'] = sysDate;
        url = "/TrdSettleController/queryFXTrdSettle";
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: url,
            data: params,
            callback: function (data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }

    //清空
    function clear() {
        var form = new mini.Form("search_form");
        form.clear();
        search();
    }

    // 初始化
    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
            search(10, 0);
        });
    });

    function printView() {
        var getRowData = grid.getSelecteds();
        selectRowData = getRowData[0];
        if (selectRowData == null || selectRowData.length == 0) {
            mini.alert("请选择一条打印数据！");
            return;
        }
        var dataList;
        url = "/FlowController/getAssignee";
        CommonUtil.ajax({
            url: url,
            data: selectRowData,
            async: false,
            callback: function (data) {
                dataList = data.obj;
            }
        });
        var print = {
            "paybankname": selectRowData.payUsername,
            "recvaccname": selectRowData.recUsername,
            "paybankno": selectRowData.payUserid,
            "recvaccno": selectRowData.recUserid,
            "recvbankname": selectRowData.recBankname,
            "mbfememo": selectRowData.remarkFy,
            "dealno": selectRowData.dealno,
            "coreflow": selectRowData.fedealno,
            "ioper": dataList.length > 0 && dataList[0] != null ? dataList[0] : "",
            "voper": dataList.length > 1 && dataList[1] != null ? dataList[1] : "",
            "payoper": dataList.length > 2 && dataList[2] != null ? dataList[2] : "",
            "payauthoper": dataList.length > 3 && dataList[3] != null ? dataList[3] : "",
            "authorization": dataList.length > 4 && dataList[4] != null ? dataList[4] : "",
            "recvbankno": selectRowData.setacct
        }
        var amount = selectRowData.amount;
        var amounts = 0;
        var amountint = 0;
        var amountdecimal = 0;
        if (amount != 0) {
            amounts = Math.abs(amount);
            if (amounts.toString().indexOf(".") > 0) {
                var amounttemp = amounts.toString().split(".");
                amountint = amounttemp[0];
                amountdecimal = amounttemp[1];
            } else {
                amountint = amounts;
            }
        }
        var temp = CommonUtil.chineseNumber(amount);

        CreateFullDvp(print, amountint, amountdecimal, temp);
        LODOP.PREVIEW();
        // LODOP.PRINT_DESIGN();
    };

    /**
     *
     **/
    function CreateFullDvp(print, amountint, amountdecimal, temp) {
        //开始打印处理
        LODOP = getLodop();
        //Top,Left,Width,Height
        LODOP.PRINT_INITA(-2, -48, 830, 700, "套打EMS的模板");
        LODOP.SET_LICENSES("哈尔滨银行", "92EC9F078EE17DE9F89608BCA683CAEA", "", "");
        LODOP.ADD_PRINT_SETUP_BKIMG("<IMG SRC='<%=basePath%>/miniScript/images/print_fx.jpg'/>");
        LODOP.SET_SHOW_MODE("BKIMG_LEFT", 10);
        LODOP.SET_SHOW_MODE("BKIMG_TOP", -4);
        LODOP.SET_SHOW_MODE("BKIMG_IN_PREVIEW", true);
        //委托人-全称
        LODOP.ADD_PRINT_TEXT(372, 228, 203, 24, print.paybankname);
        //收款人-全称
        LODOP.ADD_PRINT_TEXT(372, 545, 225, 24, print.recvaccname);
        //委托人-帐户或地址
        LODOP.ADD_PRINT_TEXT(404, 227, 158, 24, print.paybankno);
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 10);
        //收款人-帐户或地址
        LODOP.ADD_PRINT_TEXT(400, 545, 225, 24, print.recvaccno);
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 10);
        //委托人-开户行名称
        LODOP.ADD_PRINT_TEXT(430, 240, 149, 24, "哈尔滨银行结算中心");
        //委托人-开户行名称
        LODOP.ADD_PRINT_TEXT(430, 544, 225, 24, print.recvbankname);
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 10);
        //用途
        LODOP.ADD_PRINT_TEXT(514, 145, 287, 28, print.mbfememo);
        //大额支付系统行号
        LODOP.ADD_PRINT_TEXT(515, 567, 147, 25, print.recvbankno);
        //交易号
        LODOP.ADD_PRINT_TEXT(316, 120, 153, 24, "交易号: " + print.dealno);
        //交易流水
        LODOP.ADD_PRINT_TEXT(316, 500, 175, 24, "交易流水号: " + print.coreflow);
        //初审
        LODOP.ADD_PRINT_TEXT(585, 103, 105, 38, print.ioper);
        LODOP.SET_PRINT_STYLEA(0, "Alignment", 2);
        //复审
        LODOP.ADD_PRINT_TEXT(586, 228, 114, 38, print.voper);
        LODOP.SET_PRINT_STYLEA(0, "Alignment", 2);
        //副总经理(或助力)审
        LODOP.ADD_PRINT_TEXT(588, 353, 102, 38, print.payoper);
        LODOP.SET_PRINT_STYLEA(0, "Alignment", 2);
        //总经理审批
        LODOP.ADD_PRINT_TEXT(585, 485, 98, 38, print.payauthoper);
        LODOP.SET_PRINT_STYLEA(0, "Alignment", 2);
        //主管行长审批
        LODOP.ADD_PRINT_TEXT(587, 612, 101, 38, print.authorization);
        LODOP.SET_PRINT_STYLEA(0, "Alignment", 2);
        //金额
        LODOP.ADD_PRINT_TEXT(481, 688, 14, 28, AmountAdjustment(amountint, 1));
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
        LODOP.ADD_PRINT_TEXT(481, 673, 14, 28, AmountAdjustment(amountint, 2));
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
        LODOP.ADD_PRINT_TEXT(481, 656, 14, 28, AmountAdjustment(amountint, 3));
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
        LODOP.ADD_PRINT_TEXT(481, 642, 14, 28, AmountAdjustment(amountint, 4));
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
        LODOP.ADD_PRINT_TEXT(481, 628, 14, 28, AmountAdjustment(amountint, 5));
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
        LODOP.ADD_PRINT_TEXT(481, 614, 14, 28, AmountAdjustment(amountint, 6));
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
        LODOP.ADD_PRINT_TEXT(481, 599, 14, 28, AmountAdjustment(amountint, 7));
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
        LODOP.ADD_PRINT_TEXT(481, 584, 14, 28, AmountAdjustment(amountint, 8));
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
        LODOP.ADD_PRINT_TEXT(481, 571, 14, 28, AmountAdjustment(amountint, 9));
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
        LODOP.ADD_PRINT_TEXT(481, 555, 14, 28, AmountAdjustment(amountint, 10));
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
        LODOP.ADD_PRINT_TEXT(481, 703, 14, 28, amountdecimal.toString().charAt(0) / 1);
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
        LODOP.ADD_PRINT_TEXT(481, 718, 14, 28, amountdecimal.toString().charAt(1) / 1);
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
        LODOP.ADD_PRINT_TEXT(472, 246, 271, 36, temp);
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 10);

    }

    /**
     * 金额拆分
     * @param amount
     * @param number
     * @returns {number|string}
     * @constructor
     */
    function AmountAdjustment(amount, number) {
        var value;
        var amounts = amount.toString().charAt(amount.toString().length - number) / 1;
        var amounttop = amount.toString().substring(0, amount.toString().length - number + 1) / 1;
        if (amounttop == 0) {
            if (amounts != 0) {
                value = amounts;
            } else {
                value = "";
            }
        } else {
            if (amounts != 0) {
                value = amounts;
            } else {
                value = 0;
            }
        }
        return value;
    }
</script>
</html>
