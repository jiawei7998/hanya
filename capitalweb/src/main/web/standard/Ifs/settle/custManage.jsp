
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title></title>
  </head>
  
<body style="width:100%;height:100%;background:white">
	<fieldset class="mini-fieldset"> 
	<legend>查询</legend>
	<div id="search_form" style="width:100%;">
		<input id="cno" name="cno" class="mini-textbox" labelField="true" label="客户编号：" labelStyle="text-align:right;" emptyText="请输入客户编号"/>
		<input id="cmneEn" name="cmneEn" class="mini-textbox" labelField="true" label="客户英文简称：" labelStyle="text-align:right;" emptyText="请输入客户英文名称"/>
		<input id="cmneCn" name="cmneCn" class="mini-textbox" labelField="true" label="客户中文名称：" labelStyle="text-align:right;" emptyText="请输入客户中文名称"/>
		<!-- <input id="verindStatus" name="verindStatus" class="mini-combobox" label="状态" labelField="true" value="A100" labelStyle="text-align:right;" data="[{text:'经办',id:'A200'},{text:'已生效',id:'A100'}]" onvaluechanged="onDeptChanged"/> -->
		<span style="float:right;margin-right: 160px">
			<a id="search_btn" class="mini-button" style="display: none"   onclick="query">查询</a>
			<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear">清空</a>
		</span>
	</div>
	</fieldset>
	<span style="margin:2px;display: block;">
		<a  id="add_btn" class="mini-button" style="display: none"   onclick="add()">新增</a>
		<a id="edit_btn" class="mini-button" style="display: none"   onclick="update">修改</a>
		<a id="delete_btn" class="mini-button" style="display: none"   onclick="del">删除</a>
		<a id="handle_btn" class="mini-button" style="display: none"   onclick="onRowDblClick">经办</a>
	</span>
	<div id="AccOutCashManage" class="mini-fit" style="margin-top: 2px;">      
		<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" allowAlternating="true" allowResize="true" onrowdblclick="onRowDblClick" border="true" sortMode="client">
			<div property="columns">
				<div type="indexcolumn" headerAlign="center" width="40">序号</div>
				<div field="cno" width="80" allowSort="true" headerAlign="center" align="center">客户编号</div>
				<div field="product" width="150" allowSort="true" headerAlign="center" align="center">产品代码</div>
				<div field="dealFlag" width="150" allowSort="false" headerAlign="center" align="center" renderer="onDealFlagRenderer">处理状态</div>
				<div field="cmneEn" width="150" allowSort="false" headerAlign="center" align="center">客户英文简称</div>
				<div field="cmneCn" width="220" allowSort="false" headerAlign="center" align="">客户中文名称</div>
				<div field="pbnkno" width="200" allowSort="false" headerAlign="center" align="center">付款行行号</div>
				<div field="pbnknm" width="150" allowSort="false" headerAlign="center" align="">付款行行名</div>
				<div field="pacctno" width="180" allowSort="false" headerAlign="center" align="center">付款人账号</div>
				<div field="pacctnm" width="150" allowSort="false" headerAlign="center" align="">付款人名称</div>
				<div field="rbnkno" width="200" allowSort="false" headerAlign="center" align="center">收款行行号</div>
				<div field="rbnknm" width="150" allowSort="false" headerAlign="center" align="">收款行行名</div>
				<div field="racctno" width="180" allowSort="false" headerAlign="center" align="center">收款人账号</div>
				<div field="racctnm" width="150" allowSort="false" headerAlign="center" align="">收款人户名</div>
				<div field="manager" width="150" allowSort="false" headerAlign="center" align="center">经办用户</div>
				<div field="manageDate" width="150" allowSort="false" headerAlign="center" align="center" renderer="onDateRenderer">经办时间</div>
				<div field="checker" width="150" allowSort="false" headerAlign="center" align="center">复核用户</div>
				<div field="checkeDate" width="150" allowSort="false" headerAlign="center" align="center" renderer="onDateRenderer">复核时间</div>
				
			</div>
		</div>  
	</div>

	<script>
		mini.parse();
		var flashFlag="1";//是否从0开始  ,0是，1不是

		top['custManage'] = window;
		var grid = mini.get("datagrid");
		grid.on("beforeload", function (e) {
			e.cancel = true;
			flashFlag="0";
			var pageIndex = e.data.pageIndex; 
			var pageSize = e.data.pageSize;
			search(pageSize,pageIndex);
		});
		function onDateRenderer(e) {
			if(e.value!=null){
				var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)==null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g,'/'));
		        if (value) return mini.formatDate(value, 'yyyy-MM-dd');
			}
	    }
		
		var dealflags = [{ id: '0', text: '已生效' }, { id: '1', text: '已经办待复核'},{ id: '2', text: '待经办'}];
		function onDealFlagRenderer(e) {
            for (var i = 0, l = dealflags.length; i < l; i++) {
                var g = dealflags[i];
                if (g.id == e.value) return g.text;
            }
            return "";
        }
		
		grid.on("select",function(e){
			var row=e.record;
			if(row.dealFlag == "1"||row.dealFlag == "2"){
				mini.get("delete_btn").setEnabled(true);
			}else{
				mini.get("delete_btn").setEnabled(false);
			}
			if(row.dealFlag == "2"){
				mini.get("handle_btn").setEnabled(true);
			}else{
				mini.get("handle_btn").setEnabled(false);
			}
		}); 

		function search(pageSize,pageIndex){
			var form = new mini.Form("#search_form");
			if(flashFlag=="0"){
				//是否从0开始  ,0是，1不是
				flashFlag="1";
			}else{
				pageIndex=grid.pageIndex;
			}
			form.validate();
			if(form.isValid() == false){
				mini.alert("表单填写错误,请确认!","提示信息");
				return;
			}
			var data = form.getData();
			data['pageNumber'] = pageIndex+1;
			data['pageSize'] = pageSize;
			//自贸区改造加br
			data['br']=opicsBr;
			var params = mini.encode(data);
			CommonUtil.ajax({
				url:'/CustSettleController/searchCustSettleInfo',
				data:params,
				callback : function(data) {
					var grid = mini.get("datagrid");
					grid.setTotalCount(data.obj.total);
					grid.setPageIndex(pageIndex);
					grid.setPageSize(pageSize);
					grid.setData(data.obj.rows);
				}
			});
		}


		function update()
		{
			var row = grid.getSelected();
			if(row){
				var url = CommonUtil.baseWebPath() + "/settle/custManageEdit.jsp?action=edit";
				var tab = { id: "MiniCustManageEdit", name: "MiniCustManageEdit", title: "客户清算信息", url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
				var paramData = {selectData:row};
				CommonUtil.openNewMenuTab(tab,paramData);
			} else {
				mini.alert("请选中一条记录！","消息提示");
			}
		}

		function add(){
			flashFlag="0";
			var url = CommonUtil.baseWebPath() + "/settle/custManageEdit.jsp?action=add";
			var tab = { id: "MiniCustManageAdd", name: "MiniCustManageAdd", title: "客户清算信息", url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
			var paramData = {selectData:""};
			CommonUtil.openNewMenuTab(tab,paramData);
		}
		
		
		function onRowDblClick() {
			//var grid = mini.get("datagrid");
			var row = grid.getSelected();
			if(row){
					var url = CommonUtil.baseWebPath() + "/settle/custManageEdit.jsp?action=detail";
					var tab = { id: row.cno+row.seq, name: row.cno+row.seq, title: "客户清算信息", url: url ,showCloseButton:true,parentId:top["win"].tabs.getActiveTab().name};
					var paramData = {selectData:row};
					CommonUtil.openNewMenuTab(tab,paramData);
			} else {
				mini.alert("请选中一条记录！","消息提示");
			}
		}
		
		function query(){
			flashFlag="0";
			search(grid.pageSize,0);
		}
		
		function clear(){
			flashFlag="0";
			var form = new mini.Form("#search_form");
			form.clear();
			query();
		}
		
		
		//删除
		function del() {
				var grid = mini.get("datagrid");
				var row = grid.getSelected();
				if (row) {
					mini.confirm("您确认要删除选中记录?","系统警告",function(value){
						if(value=="ok"){
						CommonUtil.ajax({
							url: "/CustSettleController/deleteCustSettle",
							data: {cno: row.cno,seq: row.seq,product:row.product},
							callback: function (data) {
								if (data.code == 'error.common.0000') {
									mini.alert("删除成功");
									grid.reload();
									//search(grid.pageSize,grid.pageIndex);
								} else {
									mini.alert("删除失败");
								}
							}
						});
						}
					});
				}
				else {
					mini.alert("请选中一条记录！", "消息提示");
				}

			}

				
		
		$(document).ready(function(){
			//控制按钮显示
			$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
				//initButton();
				//onDeptChanged();
				search(10, 0);
			});
		});
		
	</script>
</body>
</html>
