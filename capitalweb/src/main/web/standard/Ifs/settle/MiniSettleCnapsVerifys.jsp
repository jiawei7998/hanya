<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="../../global.jsp"%>
<html>
<head>
    <title>大额支付往账交易签发</title>
    <script src="<%=basePath %>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
</head>

<body style="width:100%;height:100%;background:white">
<fieldset id="fd" class="mini-fieldset">
	<legend>
		<label>查询条件</label>
	</legend>
	<div id="search_form" style="width:100%;">
	        <input id="br" name="br" class="mini-hidden mini-combobox mini-mustFill" width="320px"  labelField="true" label="部门：" vtype="maxLength:5" required="true"
	          emptyText="请填写部门" labelStyle="text-align:right;" value='<%=__sessionUser.getOpicsBr()%>' data="CommonUtil.serverData.dictionary.opicsBr"/>
			<input id="dealo_s" name="dealo_s" class="mini-textbox"  width="320px" labelField="true" label="opics编号："
				labelStyle="text-align:right;"  emptyText="请输入opics编号"/>
			<input id="cname" name="cname" class="mini-textbox"  width="320px" labelField="true" label="客户名称："
				labelStyle="text-align:right;" emptyText="请输入客户名称"/>
			<input id="product_s" name="product_s" class="mini-combobox"  width="320px" labelField="true" label="产品类型："
				   textField="typeValue" valueField="typeId" labelStyle="text-align:right;" emptyText="请选择产品类型"/>
			<input id="dealstatus" name="dealstatus" class="mini-combobox"  width="320px" labelField="true" label="清算状态："
				data="CommonUtil.serverData.dictionary.handleflag"  labelStyle="text-align:right;"
				emptyText="请选择清算状态" />
		   <nobr>
			<input id="startDate" name="startDate" class="mini-datepicker"  width="320px" labelField="true" label="资金结算日："
						ondrawdate="onDrawDateStart" labelStyle="text-align:right;" emptyText="起始日期"  format="yyyy-MM-dd"/>
			<span>~</span>
			<input id="endDate" name="endDate" class="mini-datepicker"  width="200px"
						ondrawdate="onDrawDateEnd" emptyText="结束日期" value="<%=__bizDate%>" format="yyyy-MM-dd" />
			</nobr>
	        <span style="float: right; margin-right: 50px">
				<a id="search_btn" class="mini-button" style="display: none"    onclick="query">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"    onClick="clear">清空</a>
			</span>
	</div>
</fieldset>

<fieldset id="fd2" class="mini-fieldset">
	<legend>
		<label>大额支付往账交易签发</label>
	</legend>
	<div id="find2" class="fieldset-body">
		<table id="field_form" class="form-table" width="100%">
			<tr>
				<!-- <td>收款人编号：</td>
				<td>
					<input id="cno" name="cno" readonly="true" class="mini-textbox" enabled="false" style="width:90%" />
				</td> -->
				<td>opics编号：</td>
				<td>
					<input id="dealno" name="dealno" readonly="true" class="mini-textbox" enabled="false" style="width:90%" />
				</td>
				<td>生成流水号：</td>
				<td>
					<input id="fedealno" name="fedealno" class="mini-textbox" required="true"  enabled="false"
						style="width: 90%" allowInput="true"/>
				</td>
			</tr>
			<!-- <tr>
                <td>证件发行国家：</td>
                <td>
                		<input id="cerIssuerCountry" name="cerIssuerCountry" style="width:90%" value="IC-资金系统组织机构代码"
                    		readonly="true" class="mini-textbox" enabled="false" />
                </td>
                <td>证件类型：</td>
                <td>
                    <input id="cerType" name="cerType" style="width:90%" value="CN-中华人民共和国" readonly="true"
                    		class="mini-textbox" enabled="false"/>
                </td>
			</tr> -->
			<tr>
				<td>付款人账号：</td>
				<td>
					<input id="payUserid" name="payUserid" style="width:90%" class="mini-textbox" enabled="false" />
				</td>
				<td>收款人账号：</td>
				<td><input id="recUserid" name="recUserid" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])"
						onbuttonclick="recUserEdit" allowInput="true" maxLength="32" style="width:90%" class="mini-textbox"
						required="true"  enabled="false"/>
				</td>
			</tr>
			<tr>
				<td>付款人名称</td>
				<td>
				<input id="payUsername" name="payUsername" style="width:90%" readonly="true" class="mini-textbox"
					enabled="false"/>
				</td>
				<td>收款人名称：</td>
				<td>
					<input id="recUsername" name="recUsername" style="width:90%" class="mini-textbox" required="true"
						vtype="maxLength:50" enabled="false"/>
				</td>
			</tr>
			<tr>
				<td>付款行行号：</td>
				<td>
				<input id="payBankid" name="payBankid" style="width: 90%" readonly="true" class="mini-textbox"
					enabled="false" />
				</td>
				<td>收款行行号：</td>
				<td>
					<input id="recBankid" name="recBankid" style="width:90%" required="true"  class="mini-textbox"
						maxLength="14" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" enabled="false"/>
				</td>
			</tr>
			<tr>
				<td>付款行名称</td>
				<td>
				<input id="payBankname" name="payBankname" style="width: 90%" readonly="true" class="mini-textbox"
					enabled="false" />
				</td>
				<td>收款行名称：</td>
				<td>
					<input id="recBankname" name="recBankname" style="width:90%" class="mini-textbox" required="true"
                       	vtype="maxLength:50" enabled="false"/>
				</td>
			</tr>
               <!-- <tr>
                   <td>收款人开户行：</td>
                   <td>
                       <input id="recBankId_1" name="recBankId_1" style="width:90%" readonly="true" class="mini-textbox"
                       enabled="false" />
                   </td>
                   <td>开户行名称：</td>
                   <td>
                       <input id="recBankName_1" name="recBankName_1" style="width:90%" readonly="true" class="mini-textbox"
                       enabled="false" />
                   </td>
               </tr> -->
			<tr>
				<td>业务类型：</td>
				<td>
					<input id="hvpType" name="hvpType" style="width:90%" class="mini-combobox" enabled="false"
						data="[{text:'A200-行间划拨',id:'A200'},{text:'A100-普通汇兑',id:'A100'}]"/>
				</td>
				<td>币种代码：</td>
				<td>
					<input id="ccy" name="ccy" readonly="true" style="width:90%" value="RMB=人民币" class="mini-textbox"
						enabled="false" />
				</td>
			</tr>
			<tr>
				<td>金额：</td>
				<td>
					<input id="amount" name="amount" style="width:90%"  class="mini-textbox" required="true"  enabled="false"/>
				</td>
				<td>手续费：</td>
				<td>
					<input id="handleAmt" name="handleAmt" readonly="true" style="width:90%" format="n2"value="0.00"
						class="mini-spinner" changeOnMousewheel="false" enabled="false" />
				</td>
			</tr>
			<tr>
				<td>支付优先级：</td>
				<td>
					<input id="level" name="level" style="width:90%" value="HIGH-紧急" readonly="true" class="mini-textbox"
						enabled="false" />
				</td>
				<td>附言：</td>
				<td>
					<input id="remarkFy" name="remarkFy" style="width:90%" class="mini-textbox" vtype="maxLength:50"
						enabled="false"/>
				</td>
			</tr>
		</table>
	</div>
	<span class="btn_div little-tip" style="float: left;margin: 15px;">
		* 双击列表将清算信息详情展示后再进行交易签发处理。
	</span>
	<div class="btn_div" style="float: right;margin: 15px">
	   <!--  <a id="btn_account" class="mini-button" style="display: none"  onClick="btn_account">账务处理</a> -->
		<a id="btn_send" class="mini-button" style="display: none"    onClick="btn_send">确认签发</a>
		<a id="btn_back" class="mini-button" style="display: none"    onClick="btn_back">退回经办</a>
		<a id="btn_query" class="mini-button" style="display: none"    onClick="btn_query">更新处理状态</a>
		<a id="btn_print" class="mini-button" style="display: none"    onClick="print">打印</a>
	</div>
</fieldset>
   
<div id="approveType" name="approveType"  labelStyle="border: none;background-color: #fff;"
	class="mini-checkboxlist" repeatItems="0" repeatLayout="flow" value="0" textField="text" valueField="id"
	multiSelect="false" data="[{id:'0',text:'待处理列表'},{id:'1',text:'已处理列表'}]" onvaluechanged="checkBoxValuechanged">
</div>
   
<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:400px;" allowResize="true" border="true"
   		sortMode="client" frozenStartColumn="0" frozenEndColumn="1" allowAlternating="true" pageSize="10">
	<div property="columns">
		<div type="indexcolumn" headerAlign="center" align="center" width="50px">序号</div>
		<div field="fedealno" width="120" align="center" headerAlign="center" allowSort="true">生成流水号</div>
		<div field="note" width="220" align="center" headerAlign="center" allowSort="false">二代支付返回信息</div>
		<div field="tellseqno" width="120" align="center" headerAlign="center" allowSort="false">核心流水号</div>
		<div field="cname" width="250" headerAlign="center" allowSort="false" align="center">客户名称</div>
		<div field="product" width="80" align="center" headerAlign="center" allowSort="true">产品编码</div>
		<div field="prodtype" width="80" align="center" headerAlign="center" allowSort="true">产品类型</div>
		<div field="settflag" width="80" align="center" headerAlign="center" allowSort="true" renderer="CommonUtil.dictRenderer"
			data-options="{dict:'rbstatus'}">处理状态</div>
		<div field="dealstatus" width="80" align="center" headerAlign="center" allowSort="true"
			renderer="CommonUtil.dictRenderer" data-options="{dict:'handleflag'}">清算状态</div>
		<div field="amount" width="100" format="#,0.00" align="right" headerAlign="center" allowSort="false"  renderer='fmoney'>结算金额</div>
		<div field="dealno" width="100" align="center" headerAlign="center" allowSort="false">opics编号</div>
		<div field="voidflag" width="80" align="center" headerAlign="center" allowSort="false" renderer="CommonUtil.dictRenderer" data-options="{dict:'voidflag'}">opics状态</div>
		<!-- <div field="cno" width="120" headerAlign="center" align="center" allowSort="false">客户号</div> -->
		<div field="vdate" width="120" align="center" headerAlign="center" allowSort="false" renderer="onDateRenderer">资金结算日期</div>
<%--		<div field="signDate" width="120" align="center" headerAlign="center" allowSort="false">资金结算日</div>--%>
		<div field="payrecind" width="60" align="center" headerAlign="center" allowSort="false" renderer="payrecnd">方向</div>
		<div field="ioper" width="120" align="center" headerAlign="center" allowSort="false">经办人</div>
		<div field="voper" width="120" align="center" headerAlign="center" allowSort="false">复核人</div>
		<div field="ccy" width="80" align="center" headerAlign="center" allowSort="false">币种</div>
		<div field="payBankid" width="200" align="center" headerAlign="center" allowSort="false">付款行行号</div>
		<div field="payBankname" width="200" align="center" headerAlign="center" allowSort="false">付款行名称</div>
		<div field="payUserid" width="200" align="center" headerAlign="center" allowSort="false">付款人账号</div>
		<div field="payUsername" width="200" align="center" headerAlign="center" allowSort="false">付款人名称</div>
		<div field="recBankid" width="200" align="center" headerAlign="center" allowSort="false">收款行行号</div>
		<div field="recBankname" width="200" align="center" headerAlign="center" allowSort="false">收款行名称</div>
		<div field="recUserid" width="200" align="center" headerAlign="center" allowSort="false">收款人账号</div>
		<div field="recUsername" width="200" align="center" headerAlign="center" allowSort="false">收款人名称</div>
		<div field="interfaceNo" width="200" align="center" visible="false" headerAlign="center" allowSort="false">原交易流水号</div>
	</div>
</div>
    
<script type="text/javascript">   
	mini.parse();

	var grid = mini.get("datagrid");
	var form=new mini.Form("#search_form");
	var form2=new mini.Form("#field_form");

	//处理列表选择
	function checkBoxValuechanged(){
		boxChange();
		search(grid.pageSize,0);
	}
	
	function onDateRenderer(e) {
		var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)==null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g,'/'));
		if (value) return mini.formatDate(value, 'yyyy-MM-dd');
	}
	
	//列表刷新
	grid.on("beforeload", function(e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex;
		var pageSize = e.data.pageSize;
		search(pageSize, pageIndex);
	});
        
	//待处理或者已处理列表
	function boxChange(){
		var approveType = mini.get("approveType").getValue();
		if(approveType == "0"){ // 待处理
			//mini.get("recUserid").setEnabled(true);
			//mini.get("recBankid").setEnabled(true);
			//mini.get("btn_account").setVisible(true);
			mini.get("btn_back").setVisible(true);
			mini.get("btn_send").setVisible(true);
			mini.get("btn_query").setVisible(false);
			mini.get("btn_print").setVisible(false);
		}else{ // 已处理
			//mini.get("recUserid").setEnabled(false);
			//mini.get("recBankid").setEnabled(false);
			//mini.get("amount").setEnabled(false);
			mini.get("btn_back").setVisible(false);
			mini.get("btn_send").setVisible(false);
			mini.get("btn_query").setVisible(true);
			mini.get("btn_print").setVisible(true);
			//mini.get("btn_account").setVisible(false);
		}
	}
	//金额格式化
	function fmoney(e) {
		var s=typeof(e.value)=="undefined"?e:e.value;
		s = parseFloat((s + "").replace(/[^\d\.-]/g, "")).toFixed(2) + "";
		var l = s.split(".")[0].split("").reverse(), r = s.split(".")[1];
		t = "";
		for (var i = 0; i < l.length; i++) {
		t += l[i] + ((i + 1) % 3 == 0 && (i + 1) != l.length ? "," : "");
		}
		return t.split("").reverse().join("") + "." + r;
		}
	// 选中某一条，显示对应数据
	grid.on("select",function(e){
		var row=e.record;
		//var cno=mini.get("cno");
		//cno.setValue(row.cno);
		//cno.setText(row.cno); 
		//资金编号
		var dealno=mini.get("dealno");
		dealno.setValue(row.dealno);
		dealno.setText(row.dealno);
		//审批单编号
		var fedealno=mini.get("fedealno");
		fedealno.setValue(row.fedealno);
		fedealno.setText(row.fedealno);
		//付款人账号
		var payUserid = mini.get("payUserid");
		payUserid.setValue(row.payUserid);
		payUserid.setText(row.payUserid);
		//付款人名称
		var payUsername = mini.get("payUsername");
		payUsername.setValue(row.payUsername);
		payUsername.setText(row.payUsername);
		//付款行行号
		var payBankid = mini.get("payBankid");
		payBankid.setValue(row.payBankid);
		payBankid.setText(row.payBankid);
		//付款行名称
		var payBankname = mini.get("payBankname");
		payBankname.setValue(row.payBankname);
		payBankname.setText(row.payBankname);
		//兑换组号
		//var userdealno=mini.get("userdealno");
		//userdealno.setValue(row.userdealno);
		//userdealno.setText(row.userdealno);
		//收款人账号
		//mini.get("recUserid").setValue("");
		var recUserid=mini.get("recUserid");
		recUserid.setValue(row.recUserid);
		recUserid.setText(row.recUserid);
		//收款人名称
		var recUsername=mini.get("recUsername");
		recUsername.setValue(row.recUsername);
		recUsername.setText(row.recUsername); 
		//收款行行号
		//mini.get("recBankid").setValue("");
		var recBankid=mini.get("recBankid");
		recBankid.setValue(row.recBankid);
		recBankid.setText(row.recBankid);
		//收款行名称
		var recBankname=mini.get("recBankname");
		recBankname.setValue(row.recBankname);
		recBankname.setText(row.recBankname);
		//业务类型
		var hvpType=mini.get("hvpType");
		hvpType.setValue(row.hvpType);
		//hvpType.setText(row.hvpType);
		//金额
		var amount=mini.get("amount");
		amount.setValue(row.amount);
		amount.setText(fmoney(row.amount));

		//手续费
		// var handleAmt=mini.get("handleAmt");
		// handleAmt.setValue(row.handleAmt);
		// handleAmt.setText(fmoney(row.handleAmt));

		//附言
		var remarkFy=mini.get("remarkFy");
		remarkFy.setValue(row.remarkFy);
		remarkFy.setText(row.remarkFy);
	});
            
	// function settleInfo(){
	//     $("#male1").text("清算系统号：" + "313452060150");
	//     $("#male2").text("清算行名：" + "青岛银行");
	//     $("#male3").text("清算账号：" + "40700030039");
	//     $("#male4").text("清算户名：" + "青岛银行");
	// }
            
	function clear(){
		var form = new mini.Form("#search_form");
		form.clear();
		var find2 = new mini.Form("#find2");
		find2.clear();
		mini.get("br").setValue(opicsBr);//自贸区加br
		search(grid.pageSize,0);
	}
            
        //加载所有产品类型
        /* function institution() {
	        var param = mini.encode({});//  序列化json
	        CommonUtil.ajax({
	        	url: "/TdTrdSettleController/loadCustomProduct",
	            data: param,
	            callback: function (data) {
	            	var grid = mini.get("product_s");
	               	if(!data.obj == null ){
	                	grid.setData(data.obj);
	                }
	            }
	        });
        } */
        
	function query(){
		search(grid.pageSize,0);
	}

	//查询产品类型
	function queryProdtype(){
		CommonUtil.ajax({
			url:"/IfsOpicsTypeController/searchProdtype",
			data:{},
			callback:function(data){
				var length = data.length;
				var jsonData = new Array();
				for(var i=0;i<length;i++){
					if(data[i].type != null){
						var tyId = data[i].type;
						jsonData.add({'typeId':tyId,'typeValue':tyId});
					}
				};

				mini.get("product_s").setData(jsonData);
			}
		});
	}
        
	//查询返回列表信息
	function search(pageSize,pageIndex){
		var form=new mini.Form("#search_form");
	    form.validate();
		if(form.isValid() == false){
			mini.alert("表单填写错误,请确认!","提示信息");
			return;
		}
   		var data = form.getData(true);
   		data['pageNumber'] = pageIndex+1;
   		data['pageSize'] = pageSize;
   		var searchType = mini.get("approveType").getValue();
   		data['searchType'] = searchType;
   		data['approveType'] = searchType;
   		url="/TrdSettleController/searchVerifySettleInstList";
   		var params = mini.encode(data);
   		CommonUtil.ajax({
			url:url,
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
	            grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	
	//方向展示
	function payrecnd(e) {
		var payRecive = [{id: 'P',text: '付'}, {id: 'R',text: '收'}];
		for (var i = 0; i < payRecive.length; i++) {
			var g = payRecive[i];
	        if (g.id == e.value)
				return g.text;
	    }
	    return " ";
	}
	        
	//确认签发
	function btn_send(){
		var grid = mini.get("datagrid");
        var row = grid.getSelected();
        if(row){
        	if(row.voidflag!='0'){
				mini.alert("该笔大额支付交易已作废或撤销！", "系统提示");
				return;
			}
        	if(row.ioper == userId){
        	        mini.alert("经办用户和复核用户不能是同一人",'提示信息');
        	       return;
        	      }
	       	if(row.payBankid==null){
				mini.alert("付款行行号不能为空");
	       		return;
	       	}
	       	if(row.payBankname==null){
	       		mini.alert("付款行名称不能为空");
	       		return;
	       	}
	       	if(row.payUserid==null){
	       		mini.alert("付款人账号不能为空");
	       		return;
	       	}
	       	if(row.payUsername==null){
	       		mini.alert("付款人名称不能为空");
	       		return;
	       	}
	       	if(row.recBankname==null){
	       		mini.alert("收款行名称不能为空");
	       		return;
	       	}
	       	if(row.recUsername==null){
	       		mini.alert("收款人名称不能为空");
	       		return;
	       	}
	      // 	if(row.accflag!="1"){
	      // 		mini.alert("请先进行账务处理");
	      //		return;
	      // 	}
	       	
	       	var oriacct = row.recUserid;
       		var acct = mini.get("recUserid").getValue();
       		if(oriacct != acct){
       			mini.alert("收款人账号不一致");
				return;
			}
       		var oribankid = row.recBankid;
       		var bankid = mini.get("recBankid").getValue();
       		if(oribankid != bankid){
       			mini.alert("收款行行号不一致");
				return;
			}
       		var oriamt = row.amount;
       		var amt = mini.get("amount").getValue();
       		if(oriamt != amt){
       			mini.alert("金额不一致");
				return;
			}
       		
	       	var params = mini.encode(row);
	       	CommonUtil.ajax({ // 检查当前交易是否已经发送
				url: "/TrdSettleController/checkSettle",
                data: params,
                callback: function (data) {
                	if(data == "F"){
                	mini.alert("原业务流水不能为空");
                    return;
                }
				if(data != ""){
					mini.confirm("该笔交易已发送，确认重发？", "系统警告", function (value) {
						if (value == "ok") {
							CommonUtil.ajax({
			                	url: "/TrdSettleController/sendSettle",
			                    data: { dealno: row.dealno,cdate:row.cdate,fedealno: row.fedealno,
			                    	product:row.product,prodType:row.prodtype,payrecind:row.payrecind},
			                    complete: function (data) {
			                    	var text = data.responseText;
			                    	var desc = JSON.parse(text);
			                    	var content = desc["code"];
			                    	if (content == "000000") {
	                                	form2.clear(); //清空表格里的信息
	                                	mini.get("ccy").setValue("CNY=人民币");
										mini.get("level").setValue("HIGH-紧急");
	                                	//mini.get("cerIssuerCountry").setValue("IC-资金系统组织机构代码");
	 									//mini.get("cerType").setValue("CN-中华人民共和国");
	 									search(10,0);
			                        } else {
			                        	search(10,0);
			                        }
			                    }
							});
						}
					});
				} else {
					mini.confirm("确认签发该笔大额支付交易吗？", "系统警告", function (value) {
						if (value == "ok") {
							CommonUtil.ajax({
								url: "/TrdSettleController/sendSettle",
				                data: { dealno: row.dealno,cdate:row.cdate,fedealno: row.fedealno,
									product:row.product,prodType:row.prodtype,payrecind:row.payrecind},
								complete: function (data) {
				               		var text = data.responseText;
				                	var desc = JSON.parse(text);
				                    var content = desc["code"];
				                    if (content == "000000") {
				                    	form2.clear(); //清空表格里的信息
				                    	mini.get("ccy").setValue("CNY=人民币");
										mini.get("level").setValue("HIGH-紧急");
				                        //mini.get("cerIssuerCountry").setValue("IC-资金系统组织机构代码");
				 						//mini.get("cerType").setValue("CN-中华人民共和国");
				 						search(10,0);
				                    } else {
				                    	search(10,0);
				                    }
				                }
			                });
			       		}
					});
				}
			}
		});} else {
			mini.alert("请先选择一条记录", "系统提示");
			return;
		}
	}
	//账务处理
	function btn_account(){
		var grid = mini.get("datagrid");
        var row = grid.getSelected();
        if(row){
        	if(row.voidflag!='0'){
				mini.alert("该笔大额支付交易已作废或撤销！", "系统提示");
				return;
			}
        	if(row.ioper == userId){
        	        mini.alert("经办用户和复核用户不能是同一人",'提示信息');
        	       return;
        	      }
	       	if(row.payBankid==null){
				mini.alert("付款行行号不能为空");
	       		return;
	       	}
	       	if(row.payBankname==null){
	       		mini.alert("付款行名称不能为空");
	       		return;
	       	}
	       	if(row.payUserid==null){
	       		mini.alert("付款人账号不能为空");
	       		return;
	       	}
	       	if(row.payUsername==null){
	       		mini.alert("付款人名称不能为空");
	       		return;
	       	}
	       	if(row.recBankname==null){
	       		mini.alert("收款行名称不能为空");
	       		return;
	       	}
	       	if(row.recUsername==null){
	       		mini.alert("收款人名称不能为空");
	       		return;
	       	}
	       	
	       	var oriacct = row.recUserid;
       		var acct = mini.get("recUserid").getValue();
       		if(oriacct != acct){
       			mini.alert("收款人账号不一致");
				return;
			}
       		var oribankid = row.recBankid;
       		var bankid = mini.get("recBankid").getValue();
       		if(oribankid != bankid){
       			mini.alert("收款行行号不一致");
				return;
			}
       		var oriamt = row.amount;
       		var amt = mini.get("amount").getValue();
       		if(oriamt != amt){
       			mini.alert("金额不一致");
				return;
			}
       		if(row.accflag=="1"){
       			mini.alert("账务已记账");
				return;
			}
	       	var params = mini.encode(row);
	       	CommonUtil.ajax({ // 检查当前交易是否已经发送
				url: "/TrdSettleController/sendAccount",
                data: params,
                callback: function (data) {
                	mini.alert(data.desc, "系统提示");
                	search(10,0);
              }
           });		
	}else{
		mini.alert("请先选择一条记录", "系统提示");
		} 
	}
      
	
        
	//退回经办
	function btn_back() {
		var rows = grid.getSelected();
		var form2=new mini.Form("#field_form");
		if (rows) {
		   if(rows.ioper == userId){
    	        mini.alert("经办用户和复核用户不能是同一人",'提示信息');
    	       return;
    	      }
			mini.confirm("确认退回该笔大额支付交易吗？", "系统警告", function (value) {
				if (value == "ok") {
					CommonUtil.ajax({
						url: "/TrdSettleController/backSettle",
						data: { dealno: rows.dealno,fedealno:rows.fedealno },
						callback: function (data) {
	                        if (data.code == 'error.common.0000') {
	                            mini.alert(data.desc, "系统提示");
	                            form2.clear();
	                            mini.get("ccy").setValue("CNY=人民币");
								mini.get("level").setValue("HIGH-紧急");
	                            //mini.get("cerIssuerCountry").setValue("IC-资金系统组织机构代码");
					        	//mini.get("cerType").setValue("CN-中华人民共和国");
	                               search(10, 0);
	                           } else {
	                               form2.clear();
	                               mini.get("ccy").setValue("CNY=人民币");
									mini.get("level").setValue("HIGH-紧急");
	                               //mini.get("cerIssuerCountry").setValue("IC-资金系统组织机构代码");
						        //mini.get("cerType").setValue("CN-中华人民共和国");
	                               search(10, 0);
								mini.alert(data.desc, "系统提示");
							}
						}
					});
				}
			});
		} else {
			mini.alert("请先选择一条记录", "系统提示");
			return;
		}
	}
	//交易日期
	function onDrawDateStart(e) {
        var startDate = e.date;
        var endDate= mini.get("endDate").getValue();
        if(CommonUtil.isNull(endDate)){
        	return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }
	
	function onDrawDateEnd(e) {
        var endDate = e.date;
        var startDate = mini.get("startDate").getValue();
        if(CommonUtil.isNull(startDate)){
        	return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }
	// 初始化
	$(document).ready(function () {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
			queryProdtype();
			//institution();
			//settleInfo();
			checkBoxValuechanged();
		});
	});
          
	/* function onButtonEdit(e) {
		var btnEdit = this;
		mini.open({
			url: CommonUtil.baseWebPath() + "../../Common/CustMini.jsp",
			title: "选择对手方列表",
			width: 700,
			height: 600,
			ondestroy: function (action) {
				if (action == "ok") {
					var iframe = this.getIFrameEl();
					var data = iframe.contentWindow.GetData();
					data = mini.clone(data);    //必须
					if (data) {
						btnEdit.setValue(data.cname);
						btnEdit.setText(data.cname);
						btnEdit.focus();
					}
				}
			}
		});
	} */
        
	// 查询清算状态
	function btn_query() {
	   		// CommonUtil.ajax({
	   		// 	url:"/TrdSettleController/querySettle",
	   		// 	//使用的controller:IfsTrdSettleControllers
	   		// 	// data:null,
			// 	data:{},
	   		// 	complete : function(data) {
	   		// 		var text = data.responseText;
	        //     	var desc = JSON.parse(text);
	        //     	mini.alert(desc.desc);
	   		//
			// 	}
			// });
		search(10,0);
	}
	
	//打印
	function print(){
		var row = grid.getSelected();
		if(row){
			mini.confirm("确认打印该笔大额支付交易吗？", "系统警告", function (value) {
				if (value == "ok") {
					var fedealno=row.fedealno;
					var actionStr = CommonUtil.pPath + "/sl/IfsPrintUtilController/exportload/print/"+
									PrintNo.settleCnapsVerifys+"/"+ fedealno;
					$('<form target="_blank" action=' + actionStr + ' method="post"></form>').appendTo('body').submit().remove();
				}
			});
		} else {
			mini.alert('请选择一条要打印的数据！','系统提示');
			return;
		}
	}
</script>
</body>
</html>