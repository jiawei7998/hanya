
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
    <title></title>
  </head>
  
<body style="width:100%;height:100%;background:white">
	<fieldset class="mini-fieldset">
	<legend>查询</legend>
	<div id="search_form" style="width:100%;">
		<input id="certNo" name="certNo" class="mini-textbox" labelField="true" label="业务号：" labelStyle="text-align:right;" emptyText="请输入业务号"/>
		<input id="balanceDate" name="balanceDate" class="mini-datepicker" labelField="true"  label="清算日期：" labelStyle="text-align:right;" emptyText="请选择清算日期"/>
		<input id="certState" name="certState" class="mini-combobox" labelField="true" label="业务状态：" labelStyle="text-align:right;"data="[{text:'待发送二代',id:'0'},{text:'已发送二代',id:'1'},{text:'锁定',id:'2'},{text:'退回待处理',id:'3'}]" emptyText="请选择业务状态"/>
		<span style="float:right;margin-right: 160px">
			<a id="search_btn" class="mini-button" style="display: none"   onclick="query">查询</a>
			<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear">清空</a>
		</span>
	</div>
	</fieldset>
	<span style="margin:2px;display: block;">
		<a id="edit_btn" class="mini-button" style="display: none"   onclick="update">清算列表</a>
	</span>
	<div id="AccOutCashManage" class="mini-fit" style="margin-top: 2px;">      
		<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" allowAlternating="true" allowResize="true" onrowdblclick="onRowDblClick" border="true" sortMode="client">
			<div property="columns">
				<div type="indexcolumn" headerAlign="center" width="40">序号</div>
				<div field="certNo" width="100" allowSort="true" headerAlign="center" align="center">业务号</div>
				<div field="balanceDate" width="150" allowSort="true" headerAlign="center" align="center">清算日期</div>
				<div field="amount" width="150" allowSort="false" headerAlign="center" align="center" numberFormat="n4">结算金额</div>
				<div field="certState" width="150" allowSort="false" headerAlign="center" align="">业务状态</div>
				<div field="recNo" width="200" allowSort="false" headerAlign="center" align="center">收款人账号</div>
				<div field="recName" width="200" allowSort="false" headerAlign="center" align="">收款人名称</div>
				<div field="payNo" width="200" allowSort="false" headerAlign="center" align="center">汇入行行号</div>
				<div field="payName" width="200" allowSort="false" headerAlign="center" align="">汇入行名称</div>
				<div field="remark" width="200" allowSort="false" headerAlign="center" align="center">汇款附言</div>
			</div>
		</div>  
	</div>

	<script>
		mini.parse();
		top['AccOutCashManage'] = window;
		var grid = mini.get("datagrid");
		grid.on("beforeload", function (e) {
			e.cancel = true;
			var pageIndex = e.data.pageIndex; 
			var pageSize = e.data.pageSize;
			search(pageSize,pageIndex);
		});

		function search(pageSize,pageIndex)
		{
			
			var form = new mini.Form("#search_form");
			form.validate();
			if(form.isValid() == false){
				mini.alert("表单填写错误,请确认!","提示信息")
				return;
			}

			var data = form.getData();
			data['pageNumber'] = pageIndex+1;
			data['pageSize'] = pageSize;
			data['certState'] = mini.get("certState").getValue();
			var params = mini.encode(data,'yyyy-MM-dd');
			CommonUtil.ajax({
				url:'/CnapsController/searchCnapsList',
				data:params,
				callback : function(data) {
					grid.setTotalCount(data.obj.total);
					grid.setPageIndex(pageIndex);
					grid.setPageSize(pageSize);
					grid.setData(data.obj.rows);
				}
			});
		}

		function checkBoxValuechanged(){
			search(grid.pageSize,0);
		}
			
		function query(){
			search(grid.pageSize,0);
		}
		
		function clear(){
			var form = new mini.Form("#search_form");
			form.clear();
		}

		$(document).ready(function()
		{
			search(10,0);
		});
		
	</script>
</body>
</html>
