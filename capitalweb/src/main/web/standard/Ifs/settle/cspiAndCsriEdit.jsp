<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>CSPI/CSRI 维护</title>
    <style type="text/css">
    	.gray{background-color: #ddd;vertical-align:top;width:35px;}
    	
    </style>
  </head>

<body style="width:100%;height:100%;background:white">
    <div class="mini-splitter" style="width:100%;height:100%;">
    	<div size="90%" showCollapseButton="false">
    	<h1 style="text-align:center" id="labell"><strong>CSPI/CSRI</strong></h1>
    	<div id="field_form" class="mini-fit area"  style="background:white">
    	
        
        
        <div id="field" class="fieldset-body">
            <table id="field_form"  style="text-align:left;margin:auto;width:100%;" class="mini-sltable">
                <tr>
                    <td style="width:50%">
                    	<input id="br" name="br" class="mini-combobox mini-mustFill"  label="部门" labelField="true" labelStyle="text-align:left;width:170px" style="width:100%" data = "CommonUtil.serverData.dictionary.opicsBr" value='<%=__sessionUser.getOpicsBr()%>' required="true" />
                    	<input id="server" name="server" class="mini-hidden" />
                    	<input id="fedealno" name="fedealno" class="mini-hidden" />
                    	<input id="seq" name="seq" class="mini-hidden" />
                    	<input id="inoutind" name="inoutind" class="mini-hidden" />
                    	<input id="slflag" name="slflag" class="mini-hidden" />
                    	<input id="status" name="status" class="mini-hidden" />
                    
                    </td>
                    <td style="width:50%">
                        <input id="product" name="product" class="mini-buttonedit mini-mustFill"  label="产品代码" labelField="true"  labelStyle="text-align:left;width:170px" vtype="maxLength:6" style="width:100%" onbuttonclick="onPrdEdit"  required="true"  allowInput="false"/>
                    </td>
                   
                </tr>
                <tr>
                     <td style="width:50%">
                        <input id="prodtype" name="prodtype" class="mini-buttonedit mini-mustFill"  label="产品类型" required="true"  labelField="true" labelStyle="text-align:left;width:170px" vtype="maxLength:2" style="width:100%" onbuttonclick="onTypeEdit" required="true"  allowInput="false"/>
                    </td>
                    <td style="width:50%">
                        <input id="descrcn" name="descrcn" class="mini-textbox" enabled="false"  label="业务中文名称" required="true"  labelField="true" labelStyle="text-align:left;width:170px"  style="width:100%" />
                    </td>
                </tr>
                <tr>
                	<td style="width:50%">
                        <input id="ccy" name="ccy" class="mini-combobox mini-mustFill" label="币种"  labelField="true"   style="width:100%" labelStyle="text-align:left;width:170px" required="true"  data = "CommonUtil.serverData.dictionary.Currency" required="true" />
                    </td>
                	<td style="width:50%">
                        <input id="cno" name="cno" class="mini-buttonedit mini-mustFill"  label="交易对手号"  labelField="true" required="true"  style="width:100%" labelStyle="text-align:left;width:170px" vtype="maxLength:10"  onbuttonclick="onButtonEdit" required="true"  allowInput="false"/>
                    </td>
                </tr>
                <tr>
                    <td style="width:50%">
                        <input id="cliname" name="cliname" class="mini-textbox" enabled="false"  label="客户中文名称" required="true"  labelField="true" labelStyle="text-align:left;width:170px"  style="width:100%" />
                    </td>
                    <td style="width:50%">
                        <input id="settmeans" name="settmeans" class="mini-buttonedit mini-mustFill" label="清算方式" labelField="true" required="true"    vtype="maxLength:10" style="width:100%"labelStyle="text-align:left;width:170px" onvalidation="onEnglishAndNumberValidation" required="true"  onbuttonclick="onSettleMeans" allowInput="false"/>
                    </td>
                </tr>
                <tr>
                    <td style="width:50%">
                        <input id="settaccount" name="settaccount" class="mini-buttonedit mini-mustFill" label="清算账户"  labelField="true"   style="width:100%" labelStyle="text-align:left;width:170px" required="true"  onbuttonclick="onSettaccount" allowInput="false"/>
                    </td>
                </tr>
                
                <tr>
                	<td style="width:50%">
                		<input id="payrecind" name="payrecind" class="mini-radiobuttonlist" data="[{id: 'R', text: '收款(CSRI)'}, {id: 'P', text: '付款(CSPI)'}]" value="P" style="margin-left: 170px;"/>
                	</td>
                </tr>
                
                <tr id="t1">
                    <td style="width:50%">
                        <input id="bic_BE" name="bic_BE" class="mini-buttonedit mini-mustFill" label="Beneficlary:"  labelField="true"   style="width:85%" labelStyle="text-align:left;width:170px" onValueChanged="onBicChanged" onbuttonclick="onBicEdit" allowInput="false" showClose="true" oncloseclick="onCloseClick" />
                   		<a  id="BE" class="mini-button gray" name="Beneficlary:"  onclick="onButtonClick">BE</a>
                   		<div id="ck_BE" name="ck_BE" class="mini-checkbox" readOnly="true" text="" ></div>
                   		<input id="p1_BE" name="p1_BE" class="mini-hidden" />
                   		<input id="p2_BE" name="p2_BE" class="mini-hidden" />
                   		<input id="p3_BE" name="p3_BE" class="mini-hidden" />
                   		<input id="p4_BE" name="p4_BE" class="mini-hidden" />
                    </td>
                    <td style="width:50%">
                        <input id="acctno_BE" name="acctno_BE" class="mini-textbox" label="BE ac:"  labelField="true"   style="width:100%" labelStyle="text-align:left;width:170px" onvalidation="onAcctnoValidation"/>
                    </td>
                </tr>
                 <tr id="t2">
                    <td style="width:50%">
                        <input id="bic_AW" name="bic_AW" class="mini-buttonedit mini-mustFill" label="Account with:"  labelField="true"   style="width:85%" labelStyle="text-align:left;width:170px" onValueChanged="onBicChanged" onbuttonclick="onBicEdit" allowInput="false" showClose="true" oncloseclick="onCloseClick" />
                    	<a  id="AW" class="mini-button gray" name="Account with:"  onclick="onButtonClick">AW</a>
                    	<div id="ck_AW" name="ck_AW" class="mini-checkbox" readOnly="true" text="" ></div>
                    	<input id="p1_AW" name="p1_AW" class="mini-hidden" />
                   		<input id="p2_AW" name="p2_AW" class="mini-hidden" />
                   		<input id="p3_AW" name="p3_AW" class="mini-hidden" />
                   		<input id="p4_AW" name="p4_AW" class="mini-hidden" />
                    </td>
                    <td style="width:50%">
                        <input id="acctno_AW" name="acctno_AW" class="mini-textbox" label="AW ac:"  labelField="true"   style="width:100%" labelStyle="text-align:left;width:170px" onvalidation="onAcctnoValidation"/>
                    </td>
                </tr>
                 <tr id="t3">
                    <td style="width:50%">
                        <input id="bic_IN" name="bic_IN" class="mini-buttonedit mini-mustFill" label="Intermediary id:"  labelField="true"   style="width:85%" labelStyle="text-align:left;width:170px" onValueChanged="onBicChanged" onbuttonclick="onBicEdit" allowInput="false" showClose="true" oncloseclick="onCloseClick" />
                    	<a  id="IN" class="mini-button gray" name="Intermediary id:"  onclick="onButtonClick">IN</a>
                    	<div id="ck_IN" name="ck_IN" class="mini-checkbox" readOnly="true" text="" ></div>
                    	<input id="p1_IN" name="p1_IN" class="mini-hidden" />
                   		<input id="p2_IN" name="p2_IN" class="mini-hidden" />
                   		<input id="p3_IN" name="p3_IN" class="mini-hidden" />
                   		<input id="p4_IN" name="p4_IN" class="mini-hidden" />
                    </td>
                    <td style="width:50%">
                        <input id="acctno_IN" name="acctno_IN" class="mini-textbox" label="IN ac:"  labelField="true"   style="width:100%" labelStyle="text-align:left;width:170px" onvalidation="onAcctnoValidation"/>
                    </td>
                </tr>
                <tr id="t4">
                    <td style="width:50%">
                        <input id="bic_SC" name="bic_SC" class="mini-buttonedit mini-mustFill" label="Sender's corr:"  labelField="true"   style="width:85%" labelStyle="text-align:left;width:170px" onValueChanged="onBicChanged" onbuttonclick="onBicEdit" allowInput="false" showClose="true" oncloseclick="onCloseClick" />
                   		<a  id="SC" class="mini-button gray" name="Sender's corr:"   onclick="onButtonClick">SC</a>
                   		<div id="ck_SC" name="ck_SC" class="mini-checkbox" readOnly="true" text="" ></div>
                   		<input id="p1_SC" name="p1_SC" class="mini-hidden" />
                   		<input id="p2_SC" name="p2_SC" class="mini-hidden" />
                   		<input id="p3_SC" name="p3_SC" class="mini-hidden" />
                   		<input id="p4_SC" name="p4_SC" class="mini-hidden" />
                    </td>
                    <td style="width:50%">
                        <input id="acctno_SC" name="acctno_SC" class="mini-textbox" label="SC ac:"  labelField="true"   style="width:100%" labelStyle="text-align:left;width:170px" onvalidation="onAcctnoValidation"/>
                    </td>
                </tr>
                <tr id="t5">
                    <td style="width:50%">
                        <input id="bic_RC" name="bic_RC" class="mini-buttonedit mini-mustFill" label="Receiver's corr:"  labelField="true"   style="width:85%" labelStyle="text-align:left;width:170px" onValueChanged="onBicChanged" onbuttonclick="onBicEdit" allowInput="false" showClose="true" oncloseclick="onCloseClick" />
                    	<a  id="RC" class="mini-button gray" name="Receiver's corr:"  onclick="onButtonClick">RC</a>
                    	<div id="ck_RC" name="ck_RC" class="mini-checkbox" readOnly="true" text="" ></div>
                    	<input id="p1_RC" name="p1_RC" class="mini-hidden" />
                   		<input id="p2_RC" name="p2_RC" class="mini-hidden" />
                   		<input id="p3_RC" name="p3_RC" class="mini-hidden" />
                   		<input id="p4_RC" name="p4_RC" class="mini-hidden" />
                    </td>
                    <td style="width:50%">
                        <input id="acctno_RC" name="acctno_RC" class="mini-textbox" label="RC ac:"  labelField="true"   style="width:100%" labelStyle="text-align:left;width:170px" onvalidation="onAcctnoValidation"/>
                    </td>
                </tr>
                <tr id="t6">
                    <td style="width:50%">
                        <input id="bic_OC" name="bic_OC" class="mini-buttonedit mini-mustFill" label="B/O customer:"  labelField="true"   style="width:85%" labelStyle="text-align:left;width:170px" onValueChanged="onBicChanged" onbuttonclick="onBicEdit" allowInput="false" showClose="true" oncloseclick="onCloseClick" />
                   		<a  id="OC" class="mini-button gray" name="B/O customer:"  onclick="onButtonClick">OC</a>
                   		<div id="ck_OC" name="ck_OC" class="mini-checkbox" readOnly="true" text="" ></div>
                   		<input id="p1_OC" name="p1_OC" class="mini-hidden" />
                   		<input id="p2_OC" name="p2_OC" class="mini-hidden" />
                   		<input id="p3_OC" name="p3_OC" class="mini-hidden" />
                   		<input id="p4_OC" name="p4_OC" class="mini-hidden" />
                    </td>
                    <td style="width:50%">
                        <input id="acctno_OC" name="acctno_OC" class="mini-textbox" label="OC ac:"  labelField="true"   style="width:100%" labelStyle="text-align:left;width:170px" onvalidation="onAcctnoValidation"/>
                    </td>
                </tr>
                <tr id="t7">
                    <td style="width:50%">
                        <input id="bic_OB" name="bic_OB" class="mini-buttonedit mini-mustFill" label="B/O bank:"  labelField="true"   style="width:85%" labelStyle="text-align:left;width:170px" onValueChanged="onBicChanged" onbuttonclick="onBicEdit" allowInput="false" showClose="true" oncloseclick="onCloseClick" />
                    	<a  id="OB" class="mini-button gray" name="B/O bank:"  onclick="onButtonClick">OB</a>
                    	<div id="ck_OB" name="ck_OB" class="mini-checkbox" readOnly="true" text="" ></div>
                    	<input id="p1_OB" name="p1_OB" class="mini-hidden" />
                   		<input id="p2_OB" name="p2_OB" class="mini-hidden" />
                   		<input id="p3_OB" name="p3_OB" class="mini-hidden" />
                   		<input id="p4_OB" name="p4_OB" class="mini-hidden" />
                    </td>
                    <td style="width:50%">
                        <input id="acctno_OB" name="acctno_OB" class="mini-textbox" label="OB ac:"  labelField="true"   style="width:100%" labelStyle="text-align:left;width:170px" onvalidation="onAcctnoValidation"/>
                    </td>
                </tr>
                <tr id="t8">
                    <td style="width:50%">
                    <span>Details of payment:</span>
                    </td>
                    <td style="width:50%">
                    <span>Sender to receiver information:</span>
                    </td>
                </tr>
                <tr id="t9">
                    <td style="width:50%">
                    	<input id="p1" name="p1" class="mini-textbox"  style="width:90%" />
                    </td>
                    <td style="width:50%">
                    	<input id="r1" name="r1" class="mini-textbox"  style="width:100%" />
                    </td>
                </tr>
                <tr id="t10">
                    <td style="width:50%">
                    	<input id="p2" name="p2" class="mini-textbox"  style="width:90%" />
                    </td>
                    <td style="width:50%">
                    	<input id="r2" name="r2" class="mini-textbox"  style="width:100%" />
                    </td>
                </tr>
                <tr id="t11">
                    <td style="width:50%">
                    	<input id="p3" name="p3" class="mini-textbox"  style="width:90%" />
                    </td>
                    <td style="width:50%">
                    	<input id="r3" name="r3" class="mini-textbox"  style="width:100%" />
                    </td>
                </tr>
                <tr id="t12">
                    <td style="width:50%">
                    	<input id="p4" name="p4" class="mini-textbox"  style="width:90%" />
                    </td>
                    <td style="width:50%">
                    	<input id="r4" name="r4" class="mini-textbox"  style="width:100%" />
                    </td>
                </tr>
                <tr id="t13">
                    <td style="width:50%">
                    	<!-- <div id="ck" name="ck" class="mini-checkbox" text="Default instructions" ></div> -->
                    	<input id="det" name="det" class="mini-textbox"  style="width:90%;" labelField="true" label="Detail charges:"/>
                    	
                    </td>
                    <td style="width:50%">
                    	<input id="r5" name="r5" class="mini-textbox"  style="width:100%" />
                    </td>
                </tr>
                <tr id="t14">
                    <td style="width:50%">
                    </td>
                    <td style="width:50%">
                    	<input id="r6" name="r6" class="mini-textbox"  style="width:100%" />
                    </td>
                </tr>
                
                
                <tr id="t15">
                    <td style="width:50%">
                    	<input id="bic_R" name="bic_R" class="mini-buttonedit mini-mustFill" label="BIC:"  labelField="true"   style="width:100%" labelStyle="text-align:left;width:170px" onbuttonclick="onBicEdit" allowInput="false" showClose="true" oncloseclick="onCloseClick" />
                    </td>
                </tr>
                <tr id="t16">
                	<td style="width:50%">
                		<span>Customer receipt lines 1-4:</span>
                    	<input id="p1_R" name="p1_R" class="mini-textbox"  style="width:100%" />
                    </td>
                </tr>
                <tr id="t17">
                	<td style="width:50%">
                    	<input id="p2_R" name="p2_R" class="mini-textbox"  style="width:100%" />
                    </td>
                </tr>
                <tr id="t18">
                	<td style="width:50%">
                    	<input id="p3_R" name="p3_R" class="mini-textbox"  style="width:100%" />
                    </td>
                </tr>
                <tr id="t19">
                	<td style="width:50%">
                    	<input id="p4_R" name="p4_R" class="mini-textbox"  style="width:100%" />
                    </td>
                </tr>
                
            </table>
        </div>   
        </div>
        </div>
        
        <div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
        	<div style="margin-bottom:10px; text-align: center;">
				<a class="mini-button" style="display: none"  style="width:120px" id="close_btn" onclick="close">关闭界面</a>
			</div>
        	<div style="margin-bottom:10px; text-align: center;">
				<a  id="save_btn" class="mini-button" style="display: none"  style="width:120px"  onclick="save">保&nbsp;&nbsp;&nbsp;&nbsp;存</a>
			</div>
			<div style="margin-bottom:10px; text-align: center;">
				<a class="mini-button" style="display: none"  style="width:120px" id="add_btn" onclick="add">经&nbsp;&nbsp;&nbsp;&nbsp;办</a>
			</div>
			<div style="margin-bottom:10px; text-align: center;">
				<a class="mini-button" style="display: none"  style="width:120px" id="back_btn" onclick="back">退回经办</a>
			</div>
			<div style="margin-bottom:10px; text-align: center;">
				<a class="mini-button" style="display: none"  style="width:120px" id="check_btn" onclick="check">复&nbsp;&nbsp;&nbsp;&nbsp;核</a>
			</div>
	    </div>
        
        
        
    </div>
    <script type="text/javascript">
        mini.parse();
        //获取当前tab
        var currTab = top["win"].tabs.getActiveTab();
        var params = currTab.params;
        var row=params.selectData;
        var url=window.location.search;
        var action=CommonUtil.getParam(url,"action");
        
        
        var form=new mini.Form("#field_form");

        $(document).ready(function(){
            initData();
            initRadioButton();
        });
        
      //单选按钮
        var payrecind = mini.get("payrecind");
        payrecind.on("valuechanged", function (e) {
            if(this.getValue()=="R"){
            	for ( var int = 1; int < 15; int++) {
            		$("#t"+int).hide();
            	}
            	for ( var int = 15; int < 20; int++) {
            		$("#t"+int).show();
            	}
    			
        	}else{
        		for ( var int = 1; int < 15; int++) {
            		$("#t"+int).show();
            	}
        		for ( var int = 15; int < 20; int++) {
            		$("#t"+int).hide();
            	}
        	}
        });
        //单选按钮初始化
        function initRadioButton(){
        	if(row.payrecind=="R"){
        		for ( var int = 1; int < 15; int++) {
            		$("#t"+int).hide();
            	}
            	for ( var int = 15; int < 20; int++) {
            		$("#t"+int).show();
            	}
        	}else{
        		for ( var int = 1; int < 15; int++) {
            		$("#t"+int).show();
            	}
        		for ( var int = 15; int < 20; int++) {
            		$("#t"+int).hide();
            	}
        	}
        }
        
    	
        

        //初始化数据
        function initData(){
            if($.inArray(action,["edit","detail"])>-1){ //修改、详情
                form.setData(row);
                mini.get("product").setValue(row.product);
				mini.get("product").setText(row.product);
				mini.get("prodtype").setValue(row.prodtype);
				mini.get("prodtype").setText(row.prodtype);
				mini.get("cno").setValue(row.cno);
				mini.get("cno").setText(row.cno);
				mini.get("settmeans").setValue(row.settmeans);
				mini.get("settmeans").setText(row.settmeans);
				mini.get("settaccount").setValue(row.settaccount);
				mini.get("settaccount").setText(row.settaccount);
				
				mini.get("br").setEnabled(false);
				mini.get("product").setEnabled(false);
				mini.get("prodtype").setEnabled(false);
				mini.get("cno").setEnabled(false);
				mini.get("ccy").setEnabled(false);
				
				if(action == "edit"){
					initDetail();
					mini.get("add_btn").hide();
					mini.get("back_btn").hide();
	                mini.get("check_btn").hide();
	                mini.get("payrecind").setEnabled(false);
				}
                if(action == "detail"){
                	initDetail2();
                    mini.get("save_btn").hide();
                    form.setEnabled(false);
                    var detailType=CommonUtil.getParam(url,"detailType");
                    if(detailType=="1"||row.slflag!="1"){
                    	mini.get("back_btn").hide();
                        mini.get("check_btn").hide();
                    }
                    if(row.slflag=="1"){
                    	mini.get("add_btn").hide();
                    }
                    if(row.slflag=="2"){
                    	mini.get("add_btn").hide();
                    }
                }
            }else{//增加 
                mini.get("add_btn").hide();
                mini.get("back_btn").hide();
                mini.get("check_btn").hide();
            }
        }
        
      //初始化清算路径明细-修改
 	   function initDetail(){
 			var url="/IfsOpicsSettleManageController/searchCspiCsriDetails";
 			var data={};
 			data['fedealno']=mini.get("fedealno").getValue();
 			var payrecind=mini.get("payrecind").getValue();
 			var params=mini.encode(data);
 			CommonUtil.ajax({
 				url:url,
 				data:params,
 				callback : function(data) {
 					if(payrecind=="P"){
 						var ids=['BE','AW','IN','SC','RC','OC','OB'];
 	 					for ( var int = 0; int < ids.length; int++) {
 	 						var id = ids[int];
 	 						for ( var int2 = 0; int2 < data.obj.length; int2++) {
 								if(id==data.obj[int2].pcc){
 									mini.get("bic_"+id).setValue(data.obj[int2].bic);
 									mini.get("bic_"+id).setText(data.obj[int2].bic);
 						        	mini.get("acctno_"+id).setValue(data.obj[int2].acctno);
 						        	mini.get("p1_"+id).setValue(data.obj[int2].p1);
 						            mini.get("p2_"+id).setValue(data.obj[int2].p2);
 						        	mini.get("p3_"+id).setValue(data.obj[int2].p3);
 						        	mini.get("p4_"+id).setValue(data.obj[int2].p4);
 						        	
 						            if(!(CommonUtil.isNull(data.obj[int2].p1)&&CommonUtil.isNull(data.obj[int2].p2)
 				                       &&CommonUtil.isNull(data.obj[int2].p3)&&CommonUtil.isNull(data.obj[int2].p4))){
 						        		mini.get("ck_"+id).setChecked(true);
 		                                mini.get("bic_"+id).setEnabled(false);
 		                                mini.get(id).setEnabled(true);
 						        	}else{
 						        		mini.get("ck_"+id).setChecked(false);
 		                                mini.get("bic_"+id).setEnabled(true);
 		                                if(CommonUtil.isNull(data.obj[int2].bic)){
 		                                	mini.get(id).setEnabled(true);
 		                                }else{
 		                                	mini.get(id).setEnabled(false);
 		                                }
 		                              
 						        	}
 								}
 							}
 	 						
 	 					}
 	 					
 					}else{
 						mini.get("bic_R").setValue(data.obj[0].bic);
 						mini.get("bic_R").setText(data.obj[0].bic);
			        	mini.get("p1_R").setValue(data.obj[0].p1);
			            mini.get("p2_R").setValue(data.obj[0].p2);
			        	mini.get("p3_R").setValue(data.obj[0].p3);
			        	mini.get("p4_R").setValue(data.obj[0].p4);
 					}
 					
 					
 					
 				}
 			});
 		}
        
 	//初始化清算路径明细2-详情
 	   function initDetail2(){
 			var url="/IfsOpicsSettleManageController/searchCspiCsriDetails";
 			var data={};
 			data['fedealno']=mini.get("fedealno").getValue();
 			var payrecind=mini.get("payrecind").getValue();
 			var params=mini.encode(data);
 			CommonUtil.ajax({
 				url:url,
 				data:params,
 				callback : function(data) {
 					if(payrecind=="P"){
 						var ids=['BE','AW','IN','SC','RC','OC','OB'];
 	 					for ( var int = 0; int < ids.length; int++) {
 	 						var id = ids[int];
 	 						mini.get(id).setEnabled(false);
 	 						for ( var int2 = 0; int2 < data.obj.length; int2++) {
 								if(id==data.obj[int2].pcc){
 									mini.get("bic_"+id).setValue(data.obj[int2].bic);
 									mini.get("bic_"+id).setText(data.obj[int2].bic);
 						        	mini.get("acctno_"+id).setValue(data.obj[int2].acctno);
 						        	mini.get("p1_"+id).setValue(data.obj[int2].p1);
 						            mini.get("p2_"+id).setValue(data.obj[int2].p2);
 						        	mini.get("p3_"+id).setValue(data.obj[int2].p3);
 						        	mini.get("p4_"+id).setValue(data.obj[int2].p4);
 						            if(!(CommonUtil.isNull(data.obj[int2].p1)&&CommonUtil.isNull(data.obj[int2].p2)
 				                        	   &&CommonUtil.isNull(data.obj[int2].p3)&&CommonUtil.isNull(data.obj[int2].p4))){
 						        		mini.get("ck_"+id).setChecked(true);
 						        		mini.get(id).setEnabled(true);
 						        	}else{
 						        		mini.get("ck_"+id).setChecked(false);
 						        	}
 								}
 							}
 	 						
 	 					}
 					}else{
 						mini.get("bic_R").setValue(data.obj[0].bic);
 						mini.get("bic_R").setText(data.obj[0].bic);
			        	mini.get("p1_R").setValue(data.obj[0].p1);
			            mini.get("p2_R").setValue(data.obj[0].p2);
			        	mini.get("p3_R").setValue(data.obj[0].p3);
			        	mini.get("p4_R").setValue(data.obj[0].p4);
 					}
 					
 				}
 			});
 		}
        
        function getPList(){
        	var cspiCsriDetailList=[];
        	var ids=['BE','AW','IN','SC','RC','OC','OB'];
        	for ( var int = 0; int < ids.length; int++) {
				var id = ids[int];
				var bic=mini.get("bic_"+id).getValue();
	        	var acctno=mini.get("acctno_"+id).getValue();
	        	var p1=mini.get("p1_"+id).getValue();
	        	var p2=mini.get("p2_"+id).getValue();
	        	var p3=mini.get("p3_"+id).getValue();
	        	var p4=mini.get("p4_"+id).getValue();
	        	var check=mini.get("ck_"+id).getValue();
	        	var obj={};
	        	obj["bic"]=bic;
	        	obj["acctno"]=acctno;
	        	obj["pcc"]=id;
	        	obj["p1"]=p1;
	        	obj["p2"]=p2;
	        	obj["p3"]=p3;
	        	obj["p4"]=p4;
	        	if(!(CommonUtil.isNull(bic)&&CommonUtil.isNull(acctno)&&check=="false")){
	        		cspiCsriDetailList.push(obj);
	        	}
			}
        	return cspiCsriDetailList;
        }
        
        function getRList(){
        	var cspiCsriDetailList=[];
        	var obj={};
        	var bic=mini.get("bic_R").getValue();
        	var p1=mini.get("p1_R").getValue();
        	var p2=mini.get("p2_R").getValue();
        	var p3=mini.get("p3_R").getValue();
        	var p4=mini.get("p4_R").getValue();
        	obj["bic"]=bic;
        	obj["p1"]=p1;
        	obj["p2"]=p2;
        	obj["p3"]=p3;
        	obj["p4"]=p4;
        	cspiCsriDetailList.push(obj);
        	return cspiCsriDetailList;
        }
        
        //保存
         function save(){
            //表单验证！！！
            form.validate();
            if (form.isValid() == false) {
                return;
            }
            var data=form.getData(true);
            if(data['payrecind']=="P"&&data['settmeans']=="NOS"&&getPList().length==0){
            	mini.alert("swift编码不能全部为空！");
            	return;
            }else{
            	var bic_IN=mini.get("bic_IN").getValue();
            	var ck_IN=mini.get("ck_IN").getValue();
            	var bic_AW=mini.get("bic_AW").getValue();
            	var ck_AW=mini.get("ck_AW").getValue();
            	if((bic_IN!=null&&bic_IN!="")||ck_IN=="true"){
            		if((bic_AW==null||bic_AW=="")&&ck_AW=="false"){
            			mini.alert("Intermediary id不为空时，Account with不能为空！");
                    	return;
            		}
            	}
            }
            
            var saveUrl=null;
            if(action == "add"){//保存即经办
                saveUrl = "/IfsOpicsSettleManageController/saveCspiAndCsriAdd";
                data['slioper']=userId;
                if(data['payrecind']=="P"){
                	data['cspiCsriDetailList']=getPList();
                }else{
                	data['cspiCsriDetailList']=getRList();
                }
                var params=mini.encode(data);
                CommonUtil.ajax({
                    url:saveUrl,
                    data:params,
                    callback:function(data){
                    	if (data.code == 'error.common.0000') {
    						mini.alert("保存成功",'提示信息',function(){
    							top["win"].closeMenuTab();
    						});
    					} else {
    						mini.alert("保存失败");
    			    	}
                    }
    		    });
            }else if(action == "edit"){
                saveUrl = "/IfsOpicsSettleManageController/saveCspiAndCsriEdit";
                data['slioper']=userId;
                if(data['payrecind']=="P"){
                	data['cspiCsriDetailList']=getPList();
                }else{
                	data['cspiCsriDetailList']=getRList();
                }
                var params=mini.encode(data);
                CommonUtil.ajax({
                    url:saveUrl,
                    data:params,
                    callback:function(data){
                    	if (data.code == 'error.common.0000') {
    						mini.alert(data.desc,'提示信息',function(){
    							top["win"].closeMenuTab();
    						});
    					} else {
    						mini.alert("修改失败");
    			    	}
                    }
    		    });
            }

           
        }

        function close(){
            top["win"].closeMenuTab();
        }

        //英文、数字、下划线 的验证
        function onEnglishAndNumberValidation(e) {
            if(e.value == "" || e.value == null){//值为空，就不做校验
                return;
            }
            if (e.isValid) {
                if (isEnglishAndNumber(e.value) == false) {
                    e.errorText = "必须输入英文或数字";
                    e.isValid = false;
                }
            }
        }

        /* 是否英文、数字、下划线 */
        function isEnglishAndNumber(v) {
            var re = new RegExp("^[0-9a-zA-Z\_\-]+$");
            if (re.test(v)) return true;
            return false;
        }
        
        function onButtonClick(e) {
        	var flag=CommonUtil.getParam(url,"action");
            var id=this.id;
            var name=this.name;
            var action="";
            var check=mini.get("ck_"+id).getValue();
            var p1="";
            var p2="";
            var p3="";
            var p4="";
            if(flag=="detail"){
            	action="detail";
            	p1=mini.get("p1_"+id).getValue();
             	p2=mini.get("p2_"+id).getValue();
             	p3=mini.get("p3_"+id).getValue();
             	p4=mini.get("p4_"+id).getValue();
            }else{
            	 if(check=="false"){
                 	action="new";
                 }else{
                 	action="edit";
                 	p1=mini.get("p1_"+id).getValue();
                 	p2=mini.get("p2_"+id).getValue();
                 	p3=mini.get("p3_"+id).getValue();
                 	p4=mini.get("p4_"+id).getValue();
                 }
            }
            mini.open({
                url: "./Ifs/settle/cspiMini.jsp",
                title: "CSPI-"+id,
                width: 600,
                height:300,
                onload: function () {
                    var iframe = this.getIFrameEl();
                    var data = {name: name,action: action,p1:p1,p2:p2,p3:p3,p4:p4};
                    iframe.contentWindow.SetData(data);
                },
                ondestroy: function (action) {
                    if (action == "ok") {
                        var iframe = this.getIFrameEl();
                        var data = iframe.contentWindow.GetData();
                        data = mini.clone(data);    //必须
                        if (data) {
                        	if(!(CommonUtil.isNull(data.p1)&&CommonUtil.isNull(data.p2)
                        	   &&CommonUtil.isNull(data.p3)&&CommonUtil.isNull(data.p4))){
                                mini.get("ck_"+id).setChecked(true);
                                mini.get("bic_"+id).setEnabled(false);
                                
                        	}else{
                        		mini.get("ck_"+id).setChecked(false);
                                mini.get("bic_"+id).setEnabled(true);
                        	}
                        	mini.get("p1_"+id).setValue(data.p1);
                            mini.get("p2_"+id).setValue(data.p2);
                            mini.get("p3_"+id).setValue(data.p3);
                            mini.get("p4_"+id).setValue(data.p4);
                        }
                    }

                }
            });
        }
        
        function onBicChanged(e){
        	var id=this.id;
        	var buttonId=id.substring(id.length-2);
        	if(CommonUtil.isNull(e.value)){
        		mini.get(buttonId).setEnabled(true);
        	}else{
        		mini.get(buttonId).setEnabled(false);
        	}
        }
        
        
        /* 产品代码的选择 */
        function onPrdEdit(){
        	var btnEdit = this;
            mini.open({
                url: CommonUtil.baseWebPath() + "/opics/prodMini.jsp",
                title: "选择产品代码",
                width: 700,
                height: 600,
                ondestroy: function (action) {
                    if (action == "ok") {
                        var iframe = this.getIFrameEl();
                        var data = iframe.contentWindow.GetData();
                        data = mini.clone(data);    //必须
                        if (data) {
                            btnEdit.setValue(data.pcode);
                            btnEdit.setText(data.pcode);
                            onPrdChanged();
                            btnEdit.focus();
                        }
                    }

                }
            });
        }
        function onPrdChanged(){
        	if(mini.get("prodtype").getValue()!=""){
        		mini.get("prodtype").setValue("");
            	mini.get("prodtype").setText("");
            	mini.alert("产品代码已改变，请重新选择产品类型!");
        	}
        }
        
        /* 产品类型的选择 */
        function onTypeEdit(){
        	var prd = mini.get("product").getValue();
        	if(prd == null || prd == ""){
        		mini.alert("请先选择产品代码!");
        		return;
        	}
        	var data={prd:prd};
        	var btnEdit = this;
            mini.open({
                url: CommonUtil.baseWebPath() + "/opics/typeMini.jsp",
                title: "选择产品类型",
                width: 700,
                height: 600,
                onload: function () {
                    var iframe = this.getIFrameEl();
                    iframe.contentWindow.SetData(data);
                },
                ondestroy: function (action) {
                    if (action == "ok") {
                        var iframe = this.getIFrameEl();
                        var data = iframe.contentWindow.GetData();
                        data = mini.clone(data);    //必须
                        if (data) {
                            btnEdit.setValue(data.type);
                            btnEdit.setText(data.type);
                            mini.get("descrcn").setValue(data.descrcn);
                            
                            btnEdit.focus();
                        }
                    }

                }
            });
        }
        /* 交易对手的选择 */
        function onButtonEdit(e) {
            var btnEdit = this;
            mini.open({
                url: CommonUtil.baseWebPath() + "../../Common/CustMini.jsp",
                title: "选择交易对手列表",
                width: 900,
                height: 600,
                ondestroy: function (action) {
                    if (action == "ok") {
                        var iframe = this.getIFrameEl();
                        var data = iframe.contentWindow.GetData();
                        data = mini.clone(data);    //必须
                        if (data) {
                            btnEdit.setValue(data.cno);
                            btnEdit.setText(data.cno);
                            mini.get("cliname").setValue(data.cliname);
                            
                            btnEdit.focus();
                        }
                    }

                }
            });

        }
        
        /**清算路径的选择*/
		function onSettleMeans(){
		   var btnEdit = this;
	       mini.open({
	           url: "./Ifs/opics/setmMini.jsp",
	           title: "选择清算路径",
	           width: 700,
	           height: 600,
	           ondestroy: function (action) {
	               if (action == "ok") {
	                   var iframe = this.getIFrameEl();
	                   var data1 = iframe.contentWindow.GetData();
	                   data1 = mini.clone(data1);    //必须
	                   if (data1) {
	                	   mini.get("settmeans").setValue($.trim(data1.settlmeans));
	                       mini.get("settmeans").setText($.trim(data1.settlmeans));
	                       onSettleMeansChanged();
	                       btnEdit.focus();
	                   }
	               }
	           }
	       });
		}
		
		function onSettleMeansChanged(){
        	if(mini.get("settaccount").getValue()!=""){
        		mini.get("settaccount").setValue("");
            	mini.get("settaccount").setText("");
            	mini.alert("清算路径已改变，请重新选择清算账户!");
        	}
        }
		
		/* 清算账户的选择 */
        function onSettaccount(){
        	var settmeans = mini.get("settmeans").getValue();
        	var ccy = mini.get("ccy").getValue();
        	var cno = mini.get("cno").getValue();
        	if(ccy == null || ccy == ""){
        		mini.alert("请先选择币种!");
        		return;
        	}
        	/* if(cno == null || cno == ""){
        		mini.alert("请先选择交易对手!");
        		return;
        	} */
        	if(settmeans == null || settmeans == ""){
        		mini.alert("请先选择清算路径!");
        		return;
        	}
        	
        	var url;
			var data;
			if(settmeans=="NOS"){
				url="./Ifs/opics/nostMini.jsp";
	        }else{
	        	url="./Ifs/opics/setaMini.jsp";
	        }
        	data={smeans:settmeans,ccy:ccy,cust:cno};
        	var btnEdit = this;
            mini.open({
                url: url,
                title: "选择清算账户",
                width: 700,
                height: 600,
                onload: function () {
                    var iframe = this.getIFrameEl();
                    iframe.contentWindow.SetData(data);
                },
                ondestroy: function (action) {
                    if (action == "ok") {
                        var iframe = this.getIFrameEl();
                        var data = iframe.contentWindow.GetData();
                        data = mini.clone(data);    //必须
                        if (data) {
                        	if(settmeans=="NOS"){
	                    		mini.get("settaccount").setValue($.trim(data.nos));
	                    		mini.get("settaccount").setText($.trim(data.nos));
	                        }else{
	                        	mini.get("settaccount").setValue($.trim(data.sacct));
		                    	mini.get("settaccount").setText($.trim(data.sacct));
	                        }
                            btnEdit.focus();
                        }
                    }

                }
            });
        }
		
      //BIC
    	function onBicEdit(e) {
            var btnEdit = this;
            var id=this.id;
            var buttonId=id.substring(id.length-2);
            mini.open({
                url: "./Ifs/opics/bicoMini.jsp",
                title: "SWIFT码列表",
                width: 700,
                height: 600,
                ondestroy: function (action) {
                    if (action == "ok") {
                        var iframe = this.getIFrameEl();
                        var data = iframe.contentWindow.GetData();
                        data = mini.clone(data);    //必须
                        if (data) {
                            btnEdit.setValue(data.bic);
                            btnEdit.setText(data.bic);
                            mini.get(buttonId).setEnabled(false);
                            btnEdit.focus();
                        }
                    }

                }
            });
        }
		//经办
        function add(){
        	mini.confirm("您确定经办?","系统警告",function(value){
				if(value=="ok"){
					CommonUtil.ajax({
		        		url: "/IfsOpicsSettleManageController/updateSlflag",
		        		data: {fedealno: row.fedealno,userid:userId,slflag:"1"},
		        		callback: function (data) {
		        			if (data.code == "error.common.0000") {
		        				mini.alert("经办成功",'提示信息',function(){
		        					top["win"].closeMenuTab();
		        				});
		        			} else {
		        				mini.alert("经办失败");
		        			}
		        		}
		        	});
				}
			});
        }
		
      //退回经办
    	function back(){
    		mini.confirm("您确定退回经办?","系统警告",function(value){
				if(value=="ok"){
					CommonUtil.ajax({
						url: "/IfsOpicsSettleManageController/updateSlflag",
						data: {fedealno: row.fedealno,userid:userId,slflag:"0"},
						callback : function(data) {
							if (data.code == "error.common.0000") {
		        				mini.alert("退回经办成功",'提示信息',function(){
		        					top["win"].closeMenuTab();
		        				});
		        			} else {
		        				mini.alert("退回经办失败");
		        			}
						}
					});
				}
			});
    	}
    	//复核
    	function check(){
    		if(userId==row.slioper){
    			mini.alert("复核人员不能与经办人员相同,请确认!","提示信息");
        		return;
        	}
    		mini.confirm("您确定复核?","系统警告",function(value){
				if(value=="ok"){
					CommonUtil.ajax({
						//url: "/IfsOpicsSettleManageController/updateSlflag",
						url: "/IfsOpicsSettleManageController/syncCspiCsri",
						data: {fedealno: row.fedealno,userid:userId,slflag:"2"},
						callback : function(data) {
							if (data.code == "error.common.0000") {
		        				mini.alert("复核成功",'提示信息',function(){
		        					top["win"].closeMenuTab();
		        				});
		        			} else {
		        				mini.alert("复核失败");
		        			}
						}
					});
				}
			});
    	}
    	
    	
    	//以'/'开头 的验证
        function onAcctnoValidation(e) {
            if(e.value == "" || e.value == null){//值为空，就不做校验
                return;
            }
            if (e.isValid) {
                if (isSlash(e.value) == false) {
                    e.errorText = "必须以斜杠/开头";
                    e.isValid = false;
                }
            }
        }
    	
        /* 是否以'/'开头 */
        function isSlash(v) {
            var re = new RegExp("^/");
            if (re.test(v)) return true;
            return false;
        }
        
        //关闭按钮
        function onCloseClick(e) {
        	var btnEdit = this;
        	var id=this.id;
        	btnEdit.setValue(null);
            btnEdit.setText(null);
        	if(id!="bic_R"){
        		var buttonId=id.substring(id.length-2);
        		mini.get(buttonId).setEnabled(true);
        	}
        }

    </script>
</body>
</html>