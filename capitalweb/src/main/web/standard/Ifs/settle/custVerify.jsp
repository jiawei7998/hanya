
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title></title>
  </head>
  
<body style="width:100%;height:100%;background:white">
	<fieldset class="mini-fieldset">
	<legend>查询</legend>
	<div id="search_form" style="width:100%;">
		<input id="cno" name="cno" class="mini-textbox" labelField="true" label="客户编号：" labelStyle="text-align:right;" emptyText="请输入客户编号"/>
		<input id="cmneEn" name="cmneEn" class="mini-textbox" labelField="true" label="客户英文简称：" labelStyle="text-align:right;" emptyText="请输入客户英文名称"/>
		<input id="cmneCn" name="cmneCn" class="mini-textbox" labelField="true" label="客户中文名称：" labelStyle="text-align:right;" emptyText="请输入客户中文名称"/>
		<!-- <input id="verindStatus" name="verindStatus" class="mini-combobox" label="状态" labelField="true" value="A200" labelStyle="text-align:right;" data="[{text:'经办',id:'A200'},{text:'已生效',id:'A100'}]" onvaluechanged="onDeptChanged"/> -->
		<span style="float:right;margin-right: 160px">
			<a id="search_btn" class="mini-button" style="display: none"   onclick="query">查询</a>
			<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear">清空</a>
		</span>
	</div>
	</fieldset>
	<span style="margin:2px;display: block;">
		<a id="edit_btn" class="mini-button" style="display: none"   onclick="onRowDblClick">复核</a>
	</span>
	<div id="AccOutCashManage" class="mini-fit" style="margin-top: 2px;">      
		<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" allowAlternating="true" allowResize="true" onrowdblclick="onRowDblClick" border="true" sortMode="client">
			<div property="columns">
				<div type="indexcolumn" headerAlign="center" width="40">序号</div>
				<!-- <div type="checkcolumn" headerAlign="center"></div> -->
				<div field="cno" width="80" allowSort="true" headerAlign="center" align="center">客户编号</div>
				<div field="product" width="150" allowSort="true" headerAlign="center" align="center">产品代码</div>
				<div field="dealFlag" width="150" allowSort="false" headerAlign="center" align="center" renderer="onDealFlagRenderer">处理状态</div>
				<div field="cmneEn" width="150" allowSort="false" headerAlign="center" align="center">客户英文简称</div>
				<div field="cmneCn" width="220" allowSort="false" headerAlign="center" align="">客户中文名称</div>
				<div field="pbnkno" width="200" allowSort="false" headerAlign="center" align="center">付款行行号</div>
				<div field="pbnknm" width="150" allowSort="false" headerAlign="center" align="">付款行行名</div>
				<div field="pacctno" width="180" allowSort="false" headerAlign="center" align="center">付款人账号</div>
				<div field="pacctnm" width="150" allowSort="false" headerAlign="center" align="">付款人名称</div>
				<div field="rbnkno" width="200" allowSort="false" headerAlign="center" align="center">收款行行号</div>
				<div field="rbnknm" width="150" allowSort="false" headerAlign="center" align="">收款行行名</div>
				<div field="racctno" width="180" allowSort="false" headerAlign="center" align="center">收款人账号</div>
				<div field="racctnm" width="150" allowSort="false" headerAlign="center" align="">收款人户名</div>
				<div field="manager" width="150" allowSort="false" headerAlign="center" align="center">经办用户</div>
				<div field="manageDate" width="150" allowSort="false" headerAlign="center" align="center" renderer="onDateRenderer">经办时间</div>
			</div>
		</div>   
	</div>

	<script>
		mini.parse();

		top['AccOutCashManage'] = window;
		var grid = mini.get("datagrid");
		grid.on("beforeload", function (e) {
			e.cancel = true;
			var pageIndex = e.data.pageIndex; 
			var pageSize = e.data.pageSize;
			search(pageSize,pageIndex);
		});
		function onDateRenderer(e) {
			if(e.value!=null){
				var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)==null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g,'/'));
		        if (value) return mini.formatDate(value, 'yyyy-MM-dd');
			}
	    }
		var dealflags = [{ id: '0', text: '已生效' }, { id: '1', text: '已经办待复核'},{ id: '2', text: '待经办'}];
		function onDealFlagRenderer(e) {
            for (var i = 0, l = dealflags.length; i < l; i++) {
                var g = dealflags[i];
                if (g.id == e.value) return g.text;
            }
            return "";
        }
		function search(pageSize,pageIndex)
		{
			
			var form = new mini.Form("#search_form");
			form.validate();
			if(form.isValid() == false){
				mini.alert("表单填写错误,请确认!","提示信息")
				return;
			}

			var data = form.getData();
			data['pageNumber'] = pageIndex+1;
			data['pageSize'] = pageSize;
			data['dealFlag'] = "1";
			var params = mini.encode(data);
			CommonUtil.ajax({
				url:'/CustSettleController/searchCustSettleInfo',
				data:params,
				callback : function(data) {
					var grid = mini.get("datagrid");
					grid.setTotalCount(data.obj.total);
					grid.setPageIndex(pageIndex);
					grid.setPageSize(pageSize);
					grid.setData(data.obj.rows);
				}
			});
		}

		function checkBoxValuechanged(){
			search(grid.pageSize,0);
		}
			
		//双击详情页

		function update() {
			var grid = mini.get("datagrid");
			var row = grid.getSelected();
			if(row){
					var url = CommonUtil.baseWebPath() + "/settle/custVerifyEdit.jsp?action=update";
					var tab = { id: "MiniAccOutCashDetail", name: "MiniAccOutCashDetail", title: "客户清算信息", url: url ,showCloseButton:true,parentId:top["win"].tabs.getActiveTab().name};
					var paramData = {selectData:row};
					CommonUtil.openNewMenuTab(tab,paramData);
			} else {
				mini.alert("请选中一条记录！","消息提示");
			}
		}
		
		function onRowDblClick() {
			var grid = mini.get("datagrid");
			var row = grid.getSelected();
			if(row){
					var url = CommonUtil.baseWebPath() + "/settle/custVerifyEdit.jsp?action=detail";
					var tab = { id: "MiniAccOutCashDetail", name: "MiniAccOutCashDetail", title: "客户清算信息", url: url ,showCloseButton:true,parentId:top["win"].tabs.getActiveTab().name};
					var paramData = {selectData:row};
					CommonUtil.openNewMenuTab(tab,paramData);
			} else {
				mini.alert("请选中一条记录！","消息提示");
			}
		}
		
		function query(){
			search(grid.pageSize,0);
		}
		
		function clear(){
			var form = new mini.Form("#search_form");
			form.clear();
			query();
			/* mini.get("verindStatus").setValue("经办"); */
		}
		
		//删除
			function getData(action) {
					var row = null;
					if (action != "add") {
						row = grid.getSelected();
					}
					return row;
				}

		$(document).ready(function()
		{
			//控制按钮显示
			$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
				search(10, 0);
			});
		});
		function onDeptChanged(){
			var status = mini.get("verindStatus").getValue();
			if(status == 'A200'){
				search(10,0);
				initButton();
			} else {
				search(10,0);
				mini.get("edit_btn").setVisible(true);
			}
			}
	</script>
</body>
</html>
