<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
	<html>

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<%@ include file="../../global.jsp"%>
			<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
			<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
			<title></title>
	</head>

	<body style="width:100%;height:100%;background:white">
		<div class="mini-splitter" style="width:100%;height:100%;">
			<div size="90%" showCollapseButton="false">
				<div class="mini-fit" style="background:white" id="fieldform">
					<div style="width:100%;float:left;text-align:center;">
						<span style="text-align: center;font-size: 18px;">客户清算信息</span>
					</div>
					
					<div id="field_form" style="text-align:left;margin:auto;width:100%" class="mini-sltable">
					<fieldset class="mini-fieldset"> 
	                    <legend>客户主体信息</legend>
						<div class="leftarea" >
		                    	<input id="seq" name="seq" class="mini-hidden" />
								<input id="dealFlag" name="dealFlag" class="mini-hidden" />
								<input id="cno" name="cno" class="mini-buttonedit mini-mustFill" onbuttonclick="onButtonEdit" labelField="true" label="OPICS客户编号：" labelStyle="text-align:left;width:170px"
								 style="width:100%;"  requiredErrorText="该输入项为必输项"  allowInput="false" required="true" />
		                    	<input id="cmneCn" name="cmneCn" class="mini-textbox" labelField="true" label="中文名称：" labelStyle="text-align:left;width:170px"
								 style="width:100%;" requiredErrorText="该输入项为必输项" vtype="maxLength:40"/>
						</div>
						<div class="rightarea">
								<input id="cmneEn" name="cmneEn" class="mini-textbox" labelField="true" label="英文简称：" style="width:100%;" labelStyle="text-align:left;width:170px" requiredErrorText="该输入项为必输项" 
								vtype="maxLength:10"  onvalidation="onEnglishValidation"/>
								<input id="cfetsCno" name="cfetsCno" class="mini-textbox" labelField="true" label="交易中心客户编号：" style="width:100%;" labelStyle="text-align:left;width:170px;"
								  onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" vtype="maxLength:18"/>
						</div>
						</fieldset>
						<fieldset class="mini-fieldset"> 
	                    <legend>客户收/付款清算信息</legend>
							<div class="leftarea" >
							<input id="product" name="product" class="mini-buttonedit mini-mustFill" onbuttonclick="onPrdEdit" labelField="true" label="产品代码：" style="width:100%;" labelStyle="text-align:left;width:170px"
								required="true"  allowInput="false"/>
							<input id="pbnknm" name="pbnknm" class="mini-textbox" labelField="true" label="付款行行名：" labelStyle="text-align:left;width:170px"
								 style="width:100%;" requiredErrorText="该输入项为必输项"    vtype="maxLength:40"/>
							<input id="pbnkno" name="pbnkno" class="mini-textbox" labelField="true" label="付款行行号：" style="width:100%;" labelStyle="text-align:left;width:170px"
								 requiredErrorText="该输入项为必输项" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" vtype="maxLength:12"/>
							 <input id="pacctno" name="pacctno" class="mini-textbox" labelField="true" label="付款人账号：" style="width:100%;" labelStyle="text-align:left;width:170px"
								requiredErrorText="该输入项为必输项" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" vtype="maxLength:30"/>
							<input id="pacctnm" name="pacctnm" class="mini-textbox" labelField="true" label="付款人户名：" labelStyle="text-align:left;width:170px"
								 style="width:100%;" requiredErrorText="该输入项为必输项"  vtype="maxLength:40"/>
							</div>
							<div class="rightarea">
							<input id="isdefault" name="isdefault" class="mini-combobox" labelField="true" label="是否为默认账号：" labelStyle="text-align:left;width:170px"
								 style="width:100%;" data="[{text:'默认',id:'1'},{text:'不默认',id:'2'}]" required="true" />
						    <input id="rbnknm" name="rbnknm" class="mini-textbox" labelField="true" label="收款行行名：" labelStyle="text-align:left;width:170px"
								 style="width:100%;" requiredErrorText="该输入项为必输项" vtype="maxLength:40"/>
								
							<input id="rbnkno" name="rbnkno" class="mini-textbox" labelField="true" label="收款行行号：" labelStyle="text-align:left;width:170px"
								 style="width:100%;" requiredErrorText="该输入项为必输项"  onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" vtype="maxLength:12"/>
						    
							<input id="racctno" name="racctno" class="mini-textbox" labelField="true" label="收款人账号：" labelStyle="text-align:left;width:170px"
								 style="width:100%;"  requiredErrorText="该输入项为必输项" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" vtype="maxLength:30"/>
							
							<input id="racctnm" name="racctnm" class="mini-textbox" labelField="true" label="收款人户名：" labelStyle="text-align:left;width:170px"
								 style="width:100%;"  requiredErrorText="该输入项为必输项" vtype="maxLength:40"/>
							</div>
							<input id="note" name="note" class="mini-textarea" labelField="true" label="备注：" labelStyle="text-align:left;width:170px"
								 style="width:100%;" vtype="maxLength:6"/>
						
						<!-- <tr>
							<td style="width:50%">
								<input id="pacctbankid" name="pacctbankid" class="mini-textbox" labelField="true" label="付款人开户行行号：" labelStyle="text-align:left;width:170px"
								 style="width:100%;"  requiredErrorText="该输入项为必输项" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" vtype="maxLength:12"/>
							</td>
							<td style="width:50%">
								<input id="racctbankid" name="racctbankid" class="mini-textbox" labelField="true" label="收款人开户行行号：" labelStyle="text-align:left;width:170px"
								 style="width:100%;"  requiredErrorText="该输入项为必输项" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" vtype="maxLength:12"/>
							</td>
						</tr> --> 
								
						<!-- <tr>
							<td style="width:50%">
								<input id="inbankFlag" name="inbankFlag" class="mini-combobox" labelField="true" label="是否为行内账号：" labelStyle="text-align:left;width:170px"
								 style="width:100%;" data="[{text:'行内',id:'4697'},{text:'行外',id:'H100'}]"/>
							</td>
							<td style="width:50%">
								<input id="cityFlag" name="cityFlag" class="mini-combobox" labelField="true" label="城市标志：" labelStyle="text-align:left;width:170px"
								 style="width:100%;" data="[{text:'同城',id:'9857'},{text:'异地',id:'C100'}]"/>
							</td>
						</tr> -->
						<!-- <tr>
							<td style="width:50%">
								 <input id="manager" name="manager" class="mini-textbox" labelField="true" label="经办用户：" labelStyle="text-align:left;width:170px"
								 style="width:100%;" Enabled="false"/>
							</td>
							<td style="width:50%">
								<input id="manageDate" name="manageDate" class="mini-textbox" labelField="true" label="经办日期：" labelStyle="text-align:left;width:170px"
								 style="width:100%;" Enabled="false"/>
							</td>
						</tr> -->
							<!-- <td style="width:50%">
								<input id="inputdate" name="inputdate" class="mini-textbox" labelField="true" label="录入时间：" labelStyle="text-align:left;width:170px"
								 style="width:100%;" Enabled="false"/>
							</td> -->
								
                  </fieldset>
					</div>
				</div>
			</div>
			<div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
				<div style="margin-bottom:10px; text-align: center;">
					<a class="mini-button" style="display: none"  style="width:120px" id="save_btn"   onclick="save">复核</a>
				</div>
				<div style="margin-bottom:10px; text-align: center;">
				<a class="mini-button" style="display: none"  style="width:120px" id="back_btn" onclick="back">退回经办</a>
				</div>
				<div style="margin-bottom:10px; text-align: center;">
					<a class="mini-button" style="display: none"  style="width:120px" id="close_btn"   onclick="close">关闭界面</a>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			mini.parse();
			var currTab = top["win"].tabs.getActiveTab();
			var params = currTab.params;
			var row=params.selectData;
			var url = window.location.search;
			var action = CommonUtil.getParam(url, "action");
			var form = new mini.Form("#field_form");
			
			function save() {
				if(userId==row.manager){
	    			mini.alert("复核人员不能与经办人员相同,请确认!","提示信息");
	        		return;
	        	}
				mini.confirm("您确定复核?","系统警告",function(value){
					if(value=="ok"){
						var data = form.getData(true);
						data['checker']=userId;
						var params = mini.encode(data);
						CommonUtil.ajax({
							url: "/CustSettleController/confirmCustSettle",
							data: params,
							callback: function (data) {
								if (data.code == "error.common.0000") {
			        				mini.alert("复核成功",'提示信息',function(){
			        					top["win"].closeMenuTab();
			        				});
			        			} else {
			        				mini.alert("复核失败");
			        			}
							}
						});
					}
				});
			}
			 //退回经办
	    	 function back(){
	    		mini.confirm("您确定退回经办?","系统警告",function(value){
					if(value=="ok"){
						var data = form.getData(true);
						data['checker']=userId;
						var params = mini.encode(data);
						CommonUtil.ajax({
							url: "/CustSettleController/backCustSettle",
							data: params,
							callback : function(data) {
								if (data.code == "error.common.0000") {
			        				mini.alert("退回经办成功",'提示信息',function(){
			        					top["win"].closeMenuTab();
			        				});
			        			} else {
			        				mini.alert("退回经办失败");
			        			}
							}
						});
					}
				});
	    	} 
			
			
			
			
			//初始化数据
	        function initData() {
	          if ($.inArray(action, ["edit", "detail"]) > -1) { 
	        	form.setData(row);
	        	mini.get("cno").setValue(row.cno);
				mini.get("cno").setText(row.cno);
		        mini.get("product").setValue(row.product);
				mini.get("product").setText(row.product);
				mini.get("cno").setEnabled(false);
				mini.get("product").setEnabled(false);
				
	            if (action == "detail") {
                    form.setEnabled(false);
	            }
	           
				
	          } else { //增加 
	        	  
	          }
	        }
			
			
			
			function close() {
				top["win"].closeMenuTab();
			}
			$(document).ready(function () {
				initData();
			});

		</script>
	</body>

	</html>