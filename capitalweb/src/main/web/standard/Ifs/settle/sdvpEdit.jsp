<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>SDVP 维护</title>
    <style type="text/css">
    	.gray{background-color: #ddd;vertical-align:top;width:35px;}
    	.gray1{background-color: #ddd;vertical-align:top;width:200px;}
    </style>
  </head>

<body style="width:100%;height:100%;background:white">
    <div class="mini-splitter" style="width:100%;height:100%;">
    	<div size="90%" showCollapseButton="false">
    	<h1 style="text-align:center" id="labell"><strong>SDVP</strong></h1>
    	<div id="field_form" class="mini-fit area"  style="background:white">
        <div id="field" class="fieldset-body">
            <table id="field_form"  style="text-align:left;margin:auto;width:100%;" class="mini-sltable">
                <tr>
                    <td style="width:50%">
                    	<input id="br" name="br" class="mini-combobox mini-mustFill"  label="部门" labelField="true" labelStyle="text-align:left;width:170px" style="width:100%" data = "CommonUtil.serverData.dictionary.opicsBr" value='<%=__sessionUser.getOpicsBr()%>' required="true" />
                    	<input id="server" name="server" class="mini-hidden" />
                    	<input id="fedealno" name="fedealno" class="mini-hidden" />
                    	<input id="seq" name="seq" class="mini-hidden" />
                    	<input id="inoutind" name="inoutind" class="mini-hidden" />
                    	<input id="slflag" name="slflag" class="mini-hidden" />
                    	<input id="status" name="status" class="mini-hidden" />
                    </td>
                    <td style="width:50%">
                        <input id="product" name="product" class="mini-buttonedit mini-mustFill"  label="产品代码" labelField="true"  labelStyle="text-align:left;width:170px" vtype="maxLength:6" style="width:100%" onbuttonclick="onPrdEdit" required="true"  allowInput="false"/>
                    </td>
                   
                </tr>
                <tr>
                     <td style="width:50%">
                        <input id="prodtype" name="prodtype" class="mini-buttonedit mini-mustFill"  label="产品类型" required="true"  labelField="true" labelStyle="text-align:left;width:170px" vtype="maxLength:2" style="width:100%" onbuttonclick="onTypeEdit" required="true"  allowInput="false"/>
                    </td>
                     <td style="width:50%">
                        <input id="descrcn" name="descrcn" class="mini-textbox" enabled="false"  label="业务中文名称" required="true"  labelField="true" labelStyle="text-align:left;width:170px"  style="width:100%" />
                    </td>
                    
                    
                </tr>
                <tr>
                	<td style="width:50%">
                        <input id="ccy" name="ccy" class="mini-combobox mini-mustFill" label="币种"  labelField="true"   style="width:100%" labelStyle="text-align:left;width:170px" required="true"  data = "CommonUtil.serverData.dictionary.Currency" required="true" />
                    </td>
                    <td style="width:50%">
                        <input id="cno" name="cno" class="mini-buttonedit mini-mustFill"  label="交易对手号"  labelField="true" required="true"  style="width:100%" labelStyle="text-align:left;width:170px" vtype="maxLength:10"  onbuttonclick="onButtonEdit" required="true"  allowInput="false"/>
                    </td>
                </tr>
                <tr>
                    
                    <td style="width:50%">
                        <input id="cliname" name="cliname" class="mini-textbox" enabled="false"  label="客户中文名称" required="true"  labelField="true" labelStyle="text-align:left;width:170px"  style="width:100%" />
                    </td>
                    <td style="width:50%">
                        <input id="safekeepacct" name="safekeepacct" class="mini-buttonedit mini-mustFill" label="托管机构"  labelField="true"   style="width:100%" labelStyle="text-align:left;width:170px" onbuttonclick="onSafekeepacctEdit" required="true"  allowInput="false"/>
                    </td>
                </tr>
                <tr>
                    <td style="width:50%">
                        <input id="settmeans" name="settmeans" class="mini-buttonedit mini-mustFill" label="清算方式" labelField="true" required="true"    vtype="maxLength:10" style="width:100%"labelStyle="text-align:left;width:170px" onvalidation="onEnglishAndNumberValidation" required="true"  onbuttonclick="onSettleMeans" allowInput="false" />
                    </td>
                    <td style="width:50%">
                        <input id="settacct" name="settacct" class="mini-buttonedit mini-mustFill" label="清算账户"  labelField="true"   style="width:100%" labelStyle="text-align:left;width:170px" required="true"  onbuttonclick="onSettaccount" allowInput="false"/>
                    </td>
                </tr>
                
                <tr>
                	<td style="width:50%">
                		<input id="delrecind" name="delrecind" class="mini-radiobuttonlist" data="[{id: 'D', text: '付券(D)'}, {id: 'R', text: '收券(R)'}]" value="D" style="margin-left: 170px;"/>
                	</td>
                </tr>
                
                <tr id="t1">
                    <td style="width:50%">
                        <input id="bic_BS" name="bic_BS" class="mini-buttonedit mini-mustFill" label="Ben. of secur.:"  labelField="true"   style="width:85%" labelStyle="text-align:left;width:170px" onValueChanged="onBicChanged" onbuttonclick="onBicEdit" allowInput="false" showClose="true" oncloseclick="onCloseClick"/>
                   		<a  id="BS" class="mini-button gray" name="Ben. of secur.:"  onclick="onButtonClick">BS</a>
                   		<div id="ck_BS" name="ck_BS" class="mini-checkbox" readOnly="true" text="" ></div>
                   		<input id="c1_BS" name="c1_BS" class="mini-hidden" />
                   		<input id="c2_BS" name="c2_BS" class="mini-hidden" />
                   		<input id="c3_BS" name="c3_BS" class="mini-hidden" />
                   		<input id="c4_BS" name="c4_BS" class="mini-hidden" />
                    </td>
                    <td style="width:50%">
                        <input id="acctno_BS" name="acctno_BS" class="mini-textbox" label="BS ac:"  labelField="true"   style="width:100%" labelStyle="text-align:left;width:170px" onvalidation="onAcctnoValidation"/>
                    </td>
                </tr>
                 <tr id="t2">
                    <td style="width:50%">
                        <input id="bic_AW" name="bic_AW" class="mini-buttonedit mini-mustFill" label="Account with:"  labelField="true"   style="width:85%" labelStyle="text-align:left;width:170px" onValueChanged="onBicChanged" onbuttonclick="onBicEdit" allowInput="false" showClose="true" oncloseclick="onCloseClick"/>
                    	<a  id="AW" class="mini-button gray" name="Account with:"  onclick="onButtonClick">AW</a>
                    	<div id="ck_AW" name="ck_AW" class="mini-checkbox" readOnly="true" text="" ></div>
                    	<input id="c1_AW" name="c1_AW" class="mini-hidden" />
                   		<input id="c2_AW" name="c2_AW" class="mini-hidden" />
                   		<input id="c3_AW" name="c3_AW" class="mini-hidden" />
                   		<input id="c4_AW" name="c4_AW" class="mini-hidden" />
                    </td>
                    <td style="width:50%">
                        <input id="acctno_AW" name="acctno_AW" class="mini-textbox" label="AW ac:"  labelField="true"   style="width:100%" labelStyle="text-align:left;width:170px" onvalidation="onAcctnoValidation"/>
                    </td>
                </tr>
                 <tr id="t3">
                    <td style="width:50%">
                        <input id="bic_IN" name="bic_IN" class="mini-buttonedit mini-mustFill" label="Instructing:"  labelField="true"   style="width:85%" labelStyle="text-align:left;width:170px" onValueChanged="onBicChanged" onbuttonclick="onBicEdit" allowInput="false" showClose="true" oncloseclick="onCloseClick"/>
                    	<a  id="IN" class="mini-button gray" name="Instructing:"  onclick="onButtonClick">IN</a>
                    	<div id="ck_IN" name="ck_IN" class="mini-checkbox" readOnly="true" text="" ></div>
                    	<input id="c1_IN" name="c1_IN" class="mini-hidden" />
                   		<input id="c2_IN" name="c2_IN" class="mini-hidden" />
                   		<input id="c3_IN" name="c3_IN" class="mini-hidden" />
                   		<input id="c4_IN" name="c4_IN" class="mini-hidden" />
                    </td>
                    <td style="width:50%">
                        <input id="acctno_IN" name="acctno_IN" class="mini-textbox" label="IN ac:"  labelField="true"   style="width:100%" labelStyle="text-align:left;width:170px" onvalidation="onAcctnoValidation"/>
                    </td>
                </tr>
                <tr id="t4">
                    <td style="width:50%">
                        <input id="bic_BM" name="bic_BM" class="mini-buttonedit mini-mustFill" label="Ben. of money:"  labelField="true"   style="width:85%" labelStyle="text-align:left;width:170px" onValueChanged="onBicChanged" onbuttonclick="onBicEdit" allowInput="false" showClose="true" oncloseclick="onCloseClick"/>
                   		<a  id="BM" class="mini-button gray" name="Ben. of money:"   onclick="onButtonClick">BM</a>
                   		<div id="ck_BM" name="ck_BM" class="mini-checkbox" readOnly="true" text="" ></div>
                   		<input id="c1_BM" name="c1_BM" class="mini-hidden" />
                   		<input id="c2_BM" name="c2_BM" class="mini-hidden" />
                   		<input id="c3_BM" name="c3_BM" class="mini-hidden" />
                   		<input id="c4_BM" name="c4_BM" class="mini-hidden" />
                    </td>
                    <td style="width:50%">
                        <input id="acctno_BM" name="acctno_BM" class="mini-textbox" label="BM ac:"  labelField="true"   style="width:100%" labelStyle="text-align:left;width:170px" onvalidation="onAcctnoValidation"/>
                    </td>
                </tr>
                <tr id="t5">
                    <td style="width:50%">
                        <input id="bic_RE" name="bic_RE" class="mini-buttonedit mini-mustFill" label="Receiver:"  labelField="true"   style="width:85%" labelStyle="text-align:left;width:170px" onValueChanged="onBicChanged" onbuttonclick="onBicEdit" allowInput="false" showClose="true" oncloseclick="onCloseClick"/>
                    	<a  id="RE" class="mini-button gray" name="Receiver:"  onclick="onButtonClick">RE</a>
                    	<div id="ck_RE" name="ck_RE" class="mini-checkbox" readOnly="true" text="" ></div>
                    	<input id="c1_RE" name="c1_RE" class="mini-hidden" />
                   		<input id="c2_RE" name="c2_RE" class="mini-hidden" />
                   		<input id="c3_RE" name="c3_RE" class="mini-hidden" />
                   		<input id="c4_RE" name="c4_RE" class="mini-hidden" />
                    </td>
                    <td style="width:50%">
                        <input id="acctno_RE" name="acctno_RE" class="mini-textbox" label="RE ac:"  labelField="true"   style="width:100%" labelStyle="text-align:left;width:170px" onvalidation="onAcctnoValidation"/>
                    </td>
                </tr>
                <tr id="t6">
                    <td style="width:50%">
                        <input id="bic_DE" name="bic_DE" class="mini-buttonedit mini-mustFill" label="Deliverer:"  labelField="true"   style="width:85%" labelStyle="text-align:left;width:170px" onValueChanged="onBicChanged" onbuttonclick="onBicEdit" allowInput="false" showClose="true" oncloseclick="onCloseClick"/>
                   		<a  id="DE" class="mini-button gray" name="Deliverer:"  onclick="onButtonClick">DE</a>
                   		<div id="ck_DE" name="ck_DE" class="mini-checkbox" readOnly="true" text="" ></div>
                   		<input id="c1_DE" name="c1_DE" class="mini-hidden" />
                   		<input id="c2_DE" name="c2_DE" class="mini-hidden" />
                   		<input id="c3_DE" name="c3_DE" class="mini-hidden" />
                   		<input id="c4_DE" name="c4_DE" class="mini-hidden" />
                    </td>
                    <td style="width:50%">
                        <input id="acctno_DE" name="acctno_DE" class="mini-textbox" label="DE ac:"  labelField="true"   style="width:100%" labelStyle="text-align:left;width:170px" onvalidation="onAcctnoValidation"/>
                    </td>
                </tr>
                <tr id="t7">
                    <td style="width:50%">
                        <input id="bic_DI" name="bic_DI" class="mini-buttonedit mini-mustFill" label="Deliv. Instr.:"  labelField="true"   style="width:85%" labelStyle="text-align:left;width:170px" onValueChanged="onBicChanged" onbuttonclick="onBicEdit" allowInput="false" showClose="true" oncloseclick="onCloseClick"/>
                    	<a  id="DI" class="mini-button gray" name="Deliv. Instr.:"  onclick="onButtonClick">DI</a>
                    	<div id="ck_DI" name="ck_DI" class="mini-checkbox" readOnly="true" text="" ></div>
                    	<input id="c1_DI" name="c1_DI" class="mini-hidden" />
                   		<input id="c2_DI" name="c2_DI" class="mini-hidden" />
                   		<input id="c3_DI" name="c3_DI" class="mini-hidden" />
                   		<input id="c4_DI" name="c4_DI" class="mini-hidden" />
                    </td>
                    <td style="width:50%">
                        <input id="acctno_DI" name="acctno_DI" class="mini-textbox" label="DI ac:"  labelField="true"   style="width:100%" labelStyle="text-align:left;width:170px" onvalidation="onAcctnoValidation"/>
                    </td>
                </tr>
                <tr id="t8">
                    <td style="width:50%">
                    	<a  id="str" class="mini-button gray1" name="Sender to receiver"  onclick="onSClick" style="float: right;margin-right: 5%;">Sender to receiver</a>
                    	<input id="c1" name="c1" class="mini-hidden" />
                    	<input id="c2" name="c2" class="mini-hidden" />
                    	<input id="c3" name="c3" class="mini-hidden" />
                    	<input id="c4" name="c4" class="mini-hidden" />
                    	<input id="c5" name="c5" class="mini-hidden" />
                    	<input id="c6" name="c6" class="mini-hidden" />
                    </td>
                    <td style="width:50%">
                    	<a  id="afc" class="mini-button gray1" name="Account for charges"  onclick="onAClick">Account for charges</a>
                    	<input id="ac1" name="ac1" class="mini-hidden" />
                    	<input id="ac2" name="ac2" class="mini-hidden" />
                    	<input id="ac3" name="ac3" class="mini-hidden" />
                    	<input id="ac4" name="ac4" class="mini-hidden" />
                    	<input id="ac5" name="ac5" class="mini-hidden" />
                    	<input id="ac6" name="ac6" class="mini-hidden" />
                    </td>
                </tr>
                
                
            </table>
        </div>   
        </div>
        </div>
        
        <div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
        	<div style="margin-bottom:10px; text-align: center;">
				<a class="mini-button" style="display: none"  style="width:120px" id="close_btn" onclick="close">关闭界面</a>
			</div>
        	<div style="margin-bottom:10px; text-align: center;">
				<a  id="save_btn" class="mini-button" style="display: none"  style="width:120px"  onclick="save">保&nbsp;&nbsp;&nbsp;&nbsp;存</a>
			</div>
			
			<div style="margin-bottom:10px; text-align: center;">
				<a class="mini-button" style="display: none"  style="width:120px" id="add_btn" onclick="add">经&nbsp;&nbsp;&nbsp;&nbsp;办</a>
			</div>
			<div style="margin-bottom:10px; text-align: center;">
				<a class="mini-button" style="display: none"  style="width:120px" id="back_btn" onclick="back">退回经办</a>
			</div>
			<div style="margin-bottom:10px; text-align: center;">
				<a class="mini-button" style="display: none"  style="width:120px" id="check_btn" onclick="check">复&nbsp;&nbsp;&nbsp;&nbsp;核</a>
			</div>
	    </div>
        
        
        
    </div>
    <script type="text/javascript">
        mini.parse();
        //获取当前tab
        var currTab = top["win"].tabs.getActiveTab();
        var params = currTab.params;
        var row=params.selectData;
        var url=window.location.search;
        var action=CommonUtil.getParam(url,"action");
        var form=new mini.Form("#field_form");

        $(document).ready(function(){
            initData();
            
        });
        
      //单选按钮
        var delrecind = mini.get("delrecind");
        delrecind.on("valuechanged", function (e) {
            if(this.getValue()=="R"){
            	mini.get("RE").setEnabled(false);
        		mini.get("bic_RE").setEnabled(false);
            	mini.get("ck_RE").setEnabled(false);
            	mini.get("acctno_RE").setEnabled(false);
            	mini.get("DE").setEnabled(true);
            	mini.get("bic_DE").setEnabled(true);
            	mini.get("ck_DE").setEnabled(true);
            	mini.get("acctno_DE").setEnabled(true);
            	mini.get("DI").setEnabled(true);
            	mini.get("bic_DI").setEnabled(true);
            	mini.get("ck_DI").setEnabled(true);
            	mini.get("acctno_DI").setEnabled(true);
        	}else{
        		mini.get("RE").setEnabled(true);
        		mini.get("bic_RE").setEnabled(true);
            	mini.get("ck_RE").setEnabled(true);
            	mini.get("acctno_RE").setEnabled(true);
            	mini.get("DE").setEnabled(false);
        		mini.get("bic_DE").setEnabled(false);
            	mini.get("ck_DE").setEnabled(false);
            	mini.get("acctno_DE").setEnabled(false);
            	mini.get("DI").setEnabled(false);
            	mini.get("bic_DI").setEnabled(false);
            	mini.get("ck_DI").setEnabled(false);
            	mini.get("acctno_DI").setEnabled(false);
        	}
        });
        //单选按钮初始化
        function initRadioButton(){
        	if(row.delrecind=="R"){
        		mini.get("RE").setEnabled(false);
        		mini.get("bic_RE").setEnabled(false);
            	mini.get("ck_RE").setEnabled(false);
            	mini.get("acctno_RE").setEnabled(false);
            	mini.get("DE").setEnabled(true);
            	mini.get("bic_DE").setEnabled(true);
            	mini.get("ck_DE").setEnabled(true);
            	mini.get("acctno_DE").setEnabled(true);
            	mini.get("DI").setEnabled(true);
            	mini.get("bic_DI").setEnabled(true);
            	mini.get("ck_DI").setEnabled(true);
            	mini.get("acctno_DI").setEnabled(true);
        	}else{
        		mini.get("RE").setEnabled(true);
        		mini.get("bic_RE").setEnabled(true);
            	mini.get("ck_RE").setEnabled(true);
            	mini.get("acctno_RE").setEnabled(true);
            	mini.get("DE").setEnabled(false);
        		mini.get("bic_DE").setEnabled(false);
            	mini.get("ck_DE").setEnabled(false);
            	mini.get("acctno_DE").setEnabled(false);
            	mini.get("DI").setEnabled(false);
            	mini.get("bic_DI").setEnabled(false);
            	mini.get("ck_DI").setEnabled(false);
            	mini.get("acctno_DI").setEnabled(false);
        	}
        }

        //初始化数据
        function initData(){
            if($.inArray(action,["edit","detail"])>-1){ //修改、详情
                form.setData(row);
                mini.get("product").setValue(row.product);
				mini.get("product").setText(row.product);
				mini.get("prodtype").setValue(row.prodtype);
				mini.get("prodtype").setText(row.prodtype);
				mini.get("cno").setValue(row.cno);
				mini.get("cno").setText(row.cno);
				mini.get("settmeans").setValue(row.settmeans);
				mini.get("settmeans").setText(row.settmeans);
				mini.get("settacct").setValue(row.settacct);
				mini.get("settacct").setText(row.settacct);
				mini.get("safekeepacct").setValue(row.safekeepacct);
				mini.get("safekeepacct").setText(row.safekeepacct);
				mini.get("br").setEnabled(false);
				mini.get("product").setEnabled(false);
				mini.get("prodtype").setEnabled(false);
				mini.get("cno").setEnabled(false);
				mini.get("ccy").setEnabled(false);
				mini.get("safekeepacct").setEnabled(false);
				
				if(action == "edit"){
					initDetail();
					mini.get("add_btn").hide();
					mini.get("back_btn").hide();
	                mini.get("check_btn").hide();
	                mini.get("delrecind").setEnabled(false);
	                initRadioButton();
				}
                if(action == "detail"){
                	initDetail2();
                    mini.get("save_btn").hide();
                    form.setEnabled(false);
                    var detailType=CommonUtil.getParam(url,"detailType");
                    if(detailType=="1"||row.slflag!="1"){
                    	mini.get("back_btn").hide();
                        mini.get("check_btn").hide();
                    }
                   if(row.slflag=="1"){
                    	mini.get("add_btn").hide();
                    }
                    if(row.slflag=="2"){
                    	mini.get("add_btn").hide();
                    }
                }
            }else{//增加 
                mini.get("add_btn").hide();
                mini.get("back_btn").hide();
                mini.get("check_btn").hide();
               /*  mini.get("product").setValue("SECUR");
				mini.get("product").setText("SECUR");
				mini.get("prodtype").setValue("SD");
				mini.get("prodtype").setText("SD"); */
				initRadioButton();
            }
        }
        
      //初始化清算路径明细-修改
 	   function initDetail(){
 			var url="/IfsOpicsSettleManageController/searchSdvpDetails";
 			var data={};
 			data['fedealno']=mini.get("fedealno").getValue();
 			var params=mini.encode(data);
 			CommonUtil.ajax({
 				url:url,
 				data:params,
 				callback : function(data) {
 						var ids=['BS','AW','IN','BM','RE','DE','DI'];
 	 					for ( var int = 0; int < ids.length; int++) {
 	 						var id = ids[int];
 	 						for ( var int2 = 0; int2 < data.obj.length; int2++) {
 								if(id==data.obj[int2].dcc){
 									mini.get("bic_"+id).setValue(data.obj[int2].bic);
 									mini.get("bic_"+id).setText(data.obj[int2].bic);
 						        	mini.get("acctno_"+id).setValue(data.obj[int2].acctno);
 						        	mini.get("c1_"+id).setValue(data.obj[int2].c1);
 						            mini.get("c2_"+id).setValue(data.obj[int2].c2);
 						        	mini.get("c3_"+id).setValue(data.obj[int2].c3);
 						        	mini.get("c4_"+id).setValue(data.obj[int2].c4);
 						        	
 						            if(!(CommonUtil.isNull(data.obj[int2].c1)&&CommonUtil.isNull(data.obj[int2].c2)
 				                       &&CommonUtil.isNull(data.obj[int2].c3)&&CommonUtil.isNull(data.obj[int2].c4))){
 						        		mini.get("ck_"+id).setChecked(true);
 		                                mini.get("bic_"+id).setEnabled(false);
 		                                mini.get(id).setEnabled(true);
 						        	}else{
 						        		mini.get("ck_"+id).setChecked(false);
 		                                mini.get("bic_"+id).setEnabled(true);
 		                                if(CommonUtil.isNull(data.obj[int2].bic)){
 		                                	mini.get(id).setEnabled(true);
 		                                }else{
 		                                	mini.get(id).setEnabled(false);
 		                                }
 		                              
 						        	}
 								}
 							}
 	 					}
 	 					
 					
 					
 					
 				}
 			});
 		}
        
 	//初始化清算路径明细2-详情
 	   function initDetail2(){
 			var url="/IfsOpicsSettleManageController/searchSdvpDetails";
 			var data={};
 			data['fedealno']=mini.get("fedealno").getValue();
 			var params=mini.encode(data);
 			CommonUtil.ajax({
 				url:url,
 				data:params,
 				callback : function(data) {
 						var ids=['BS','AW','IN','BM','RE','DE','DI'];
 	 					for ( var int = 0; int < ids.length; int++) {
 	 						var id = ids[int];
 	 						mini.get(id).setEnabled(false);
 	 						for ( var int2 = 0; int2 < data.obj.length; int2++) {
 								if(id==data.obj[int2].dcc){
 									mini.get("bic_"+id).setValue(data.obj[int2].bic);
 									mini.get("bic_"+id).setText(data.obj[int2].bic);
 						        	mini.get("acctno_"+id).setValue(data.obj[int2].acctno);
 						        	mini.get("c1_"+id).setValue(data.obj[int2].c1);
 						            mini.get("c2_"+id).setValue(data.obj[int2].c2);
 						        	mini.get("c3_"+id).setValue(data.obj[int2].c3);
 						        	mini.get("c4_"+id).setValue(data.obj[int2].c4);
 						            if(!(CommonUtil.isNull(data.obj[int2].c1)&&CommonUtil.isNull(data.obj[int2].c2)
 				                        	   &&CommonUtil.isNull(data.obj[int2].c3)&&CommonUtil.isNull(data.obj[int2].c4))){
 						        		mini.get("ck_"+id).setChecked(true);
 						        		mini.get(id).setEnabled(true);
 						        	}else{
 						        		mini.get("ck_"+id).setChecked(false);
 						        	}
 								}
 							}
 	 						
 	 					}
 					
 				}
 			});
 		}
        
        function getList(){
        	var sdvpDetailList=[];
        	var ids=['BS','AW','IN','BM','RE','DE','DI'];
        	for ( var int = 0; int < ids.length; int++) {
				var id = ids[int];
				var bic=mini.get("bic_"+id).getValue();
	        	var acctno=mini.get("acctno_"+id).getValue();
	        	var c1=mini.get("c1_"+id).getValue();
	        	var c2=mini.get("c2_"+id).getValue();
	        	var c3=mini.get("c3_"+id).getValue();
	        	var c4=mini.get("c4_"+id).getValue();
	        	var check=mini.get("ck_"+id).getValue();
	        	var obj={};
	        	obj["bic"]=bic;
	        	obj["acctno"]=acctno;
	        	obj["dcc"]=id;
	        	obj["c1"]=c1;
	        	obj["c2"]=c2;
	        	obj["c3"]=c3;
	        	obj["c4"]=c4;
	        	if(!(CommonUtil.isNull(bic)&&CommonUtil.isNull(acctno)&&check=="false")){
	        		sdvpDetailList.push(obj);
	        	}
			}
        	return sdvpDetailList;
        }
        
        
        //保存
         function save(){
            //表单验证！！！
            form.validate();
            if (form.isValid() == false) {
                return;
            }
            var saveUrl=null;
            if(action == "add"){
                saveUrl = "/IfsOpicsSettleManageController/saveSdvpAdd";
                var data=form.getData(true);
                data['slioper']=userId;
                data['sdvpDetailList']=getList();
                var params=mini.encode(data);
                CommonUtil.ajax({
                    url:saveUrl,
                    data:params,
                    callback:function(data){
                    	if (data.code == 'error.common.0000') {
    						mini.alert("保存成功",'提示信息',function(){
    							top["win"].closeMenuTab();
    						});
    					} else {
    						mini.alert("保存失败");
    			    	}
                    }
    		    });
            }else if(action == "edit"){
                saveUrl = "/IfsOpicsSettleManageController/saveSdvpEdit";
                var data=form.getData(true);
                data['slioper']=userId;
                data['sdvpDetailList']=getList();
                var params=mini.encode(data);
                CommonUtil.ajax({
                    url:saveUrl,
                    data:params,
                    callback:function(data){
                    	if (data.code == 'error.common.0000') {
    						mini.alert(data.desc,'提示信息',function(){
    							top["win"].closeMenuTab();
    						});
    					} else {
    						mini.alert("修改失败");
    			    	}
                    }
    		    });
            }

           
        }

        function close(){
            top["win"].closeMenuTab();
        }

        //英文、数字、下划线 的验证
        function onEnglishAndNumberValidation(e) {
            if(e.value == "" || e.value == null){//值为空，就不做校验
                return;
            }
            if (e.isValid) {
                if (isEnglishAndNumber(e.value) == false) {
                    e.errorText = "必须输入英文或数字";
                    e.isValid = false;
                }
            }
        }

        /* 是否英文、数字、下划线 */
        function isEnglishAndNumber(v) {
            var re = new RegExp("^[0-9a-zA-Z\_\-]+$");
            if (re.test(v)) return true;
            return false;
        }
        
        function onButtonClick(e) {
        	var flag=CommonUtil.getParam(url,"action");
            var id=this.id;
            var name=this.name;
            var action="";
            var check=mini.get("ck_"+id).getValue();
            var c1="";
            var c2="";
            var c3="";
            var c4="";
            if(flag=="detail"){
            	action="detail";
            	c1=mini.get("c1_"+id).getValue();
             	c2=mini.get("c2_"+id).getValue();
             	c3=mini.get("c3_"+id).getValue();
             	c4=mini.get("c4_"+id).getValue();
            }else{
            	 if(check=="false"){
                 	action="new";
                 }else{
                 	action="edit";
                 	c1=mini.get("c1_"+id).getValue();
                 	c2=mini.get("c2_"+id).getValue();
                 	c3=mini.get("c3_"+id).getValue();
                 	c4=mini.get("c4_"+id).getValue();
                 }
            }
            mini.open({
                url: "./Ifs/settle/sdvpMini.jsp",
                title: "SDVP-"+id,
                width: 600,
                height:300,
                onload: function () {
                    var iframe = this.getIFrameEl();
                    var data = {name: name,action: action,c1:c1,c2:c2,c3:c3,c4:c4};
                    iframe.contentWindow.SetData(data);
                },
                ondestroy: function (action) {
                    if (action == "ok") {
                        var iframe = this.getIFrameEl();
                        var data = iframe.contentWindow.GetData();
                        data = mini.clone(data);    //必须
                        if (data) {
                        	if(!(CommonUtil.isNull(data.c1)&&CommonUtil.isNull(data.c2)
                        	   &&CommonUtil.isNull(data.c3)&&CommonUtil.isNull(data.c4))){
                                mini.get("ck_"+id).setChecked(true);
                                mini.get("bic_"+id).setEnabled(false);
                               
                        	}else{
                        		mini.get("ck_"+id).setChecked(false);
                                mini.get("bic_"+id).setEnabled(true);
                        	}
                        	 mini.get("c1_"+id).setValue(data.c1);
                             mini.get("c2_"+id).setValue(data.c2);
                             mini.get("c3_"+id).setValue(data.c3);
                             mini.get("c4_"+id).setValue(data.c4);
                        }
                    }

                }
            });
        }
        
        
        function onSClick(e) {
        	var flag=CommonUtil.getParam(url,"action");
            var action=flag;
            var c1=mini.get("c1").getValue();
            var c2=mini.get("c2").getValue();
            var c3=mini.get("c3").getValue();
            var c4=mini.get("c4").getValue();
            var c5=mini.get("c5").getValue();
            var c6=mini.get("c6").getValue();
            mini.open({
                url: "./Ifs/settle/sdvpSenderMini.jsp",
                title: "SDVP",
                width: 600,
                height:400,
                onload: function () {
                    var iframe = this.getIFrameEl();
                    var data = {action: action,c1:c1,c2:c2,c3:c3,c4:c4,c5:c5,c6:c6};
                    iframe.contentWindow.SetData(data);
                },
                ondestroy: function (action) {
                    if (action == "ok") {
                        var iframe = this.getIFrameEl();
                        var data = iframe.contentWindow.GetData();
                        data = mini.clone(data);    //必须
                        if (data) {
                        	mini.get("c1").setValue(data.c1);
                            mini.get("c2").setValue(data.c2);
                            mini.get("c3").setValue(data.c3);
                            mini.get("c4").setValue(data.c4);
                            mini.get("c5").setValue(data.c5);
                            mini.get("c6").setValue(data.c6);
                        }
                    }

                }
            });
        }
        function onAClick(e) {
        	var flag=CommonUtil.getParam(url,"action");
            var action=flag;
            var ac1=mini.get("ac1").getValue();
            var ac2=mini.get("ac2").getValue();
            var ac3=mini.get("ac3").getValue();
            var ac4=mini.get("ac4").getValue();
            var ac5=mini.get("ac5").getValue();
            var ac6=mini.get("ac6").getValue();
            mini.open({
                url: "./Ifs/settle/sdvpAccountMini.jsp",
                title: "SDVP",
                width: 600,
                height:400,
                onload: function () {
                    var iframe = this.getIFrameEl();
                    var data = {action: action,ac1:ac1,ac2:ac2,ac3:ac3,ac4:ac4,ac5:ac5,ac6:ac6};
                    iframe.contentWindow.SetData(data);
                },
                ondestroy: function (action) {
                    if (action == "ok") {
                        var iframe = this.getIFrameEl();
                        var data = iframe.contentWindow.GetData();
                        data = mini.clone(data);    //必须
                        if (data) {
                        	mini.get("ac1").setValue(data.ac1);
                            mini.get("ac2").setValue(data.ac2);
                            mini.get("ac3").setValue(data.ac3);
                            mini.get("ac4").setValue(data.ac4);
                            mini.get("ac5").setValue(data.ac5);
                            mini.get("ac6").setValue(data.ac6);
                        }
                    }

                }
            });
        }
        
        function onBicChanged(e){
        	var id=this.id;
        	var buttonId=id.substring(id.length-2);
        	if(CommonUtil.isNull(e.value)){
        		mini.get(buttonId).setEnabled(true);
        	}else{
        		mini.get(buttonId).setEnabled(false);
        	}
        }
        
        
        /* 产品代码的选择 */
        function onPrdEdit(){
        	var btnEdit = this;
            mini.open({
                url: CommonUtil.baseWebPath() + "/opics/prodMini.jsp",
                title: "选择产品代码",
                width: 700,
                height: 600,
                ondestroy: function (action) {
                    if (action == "ok") {
                        var iframe = this.getIFrameEl();
                        var data = iframe.contentWindow.GetData();
                        data = mini.clone(data);    //必须
                        if (data) {
                            btnEdit.setValue(data.pcode);
                            btnEdit.setText(data.pcode);
                            btnEdit.focus();
                        }
                    }

                }
            });
        }
        function onPrdChanged(){
        	if(mini.get("prodtype").getValue()!=""){
        		mini.get("prodtype").setValue("");
            	mini.get("prodtype").setText("");
            	mini.alert("产品代码已改变，请重新选择产品类型!");
        	}
        }
        
        /* 产品类型的选择 */
        function onTypeEdit(){
        	var prd = mini.get("product").getValue();
        	if(prd == null || prd == ""){
        		mini.alert("请先选择产品代码!");
        		return;
        	}
        	var data={prd:prd};
        	var btnEdit = this;
            mini.open({
                url: CommonUtil.baseWebPath() + "/opics/typeMini.jsp",
                title: "选择产品类型",
                width: 700,
                height: 600,
                onload: function () {
                    var iframe = this.getIFrameEl();
                    iframe.contentWindow.SetData(data);
                },
                ondestroy: function (action) {
                    if (action == "ok") {
                        var iframe = this.getIFrameEl();
                        var data = iframe.contentWindow.GetData();
                        data = mini.clone(data);    //必须
                        if (data) {
                            btnEdit.setValue(data.type);
                            btnEdit.setText(data.type);
                            mini.get("descrcn").setValue(data.descrcn);
                            btnEdit.focus();
                        }
                    }

                }
            });
        }
        /* 交易对手的选择 */
        function onButtonEdit(e) {
            var btnEdit = this;
            mini.open({
                url: CommonUtil.baseWebPath() + "../../Common/CustMini.jsp",
                title: "选择交易对手列表",
                width: 900,
                height: 600,
                ondestroy: function (action) {
                    if (action == "ok") {
                        var iframe = this.getIFrameEl();
                        var data = iframe.contentWindow.GetData();
                        data = mini.clone(data);    //必须
                        if (data) {
                            btnEdit.setValue(data.cno);
                            btnEdit.setText(data.cno);
                            mini.get("cliname").setValue(data.cliname);
                            btnEdit.focus();
                        }
                    }

                }
            });

        }
        
        /**清算路径的选择*/
		function onSettleMeans(){
		   var btnEdit = this;
	       mini.open({
	           url: "./Ifs/opics/setmMini.jsp",
	           title: "选择清算路径",
	           width: 700,
	           height: 600,
	           ondestroy: function (action) {
	               if (action == "ok") {
	                   var iframe = this.getIFrameEl();
	                   var data1 = iframe.contentWindow.GetData();
	                   data1 = mini.clone(data1);    //必须
	                   if (data1) {
	                	   mini.get("settmeans").setValue($.trim(data1.settlmeans));
	                       mini.get("settmeans").setText($.trim(data1.settlmeans));
	                       onSettleMeansChanged();
	                       btnEdit.focus();
	                   }
	               }
	           }
	       });
		}
		
		function onSettleMeansChanged(){
        	if(mini.get("settaccount").getValue()!=""){
        		mini.get("settaccount").setValue("");
            	mini.get("settaccount").setText("");
            	mini.alert("清算路径已改变，请重新选择清算账户!");
        	}
        }
		/* 清算账户的选择 */
        function onSettaccount(){
        	var settmeans = mini.get("settmeans").getValue();
        	var ccy = mini.get("ccy").getValue();
        	var cno = mini.get("cno").getValue();
        	if(ccy == null || ccy == ""){
        		mini.alert("请先选择币种!");
        		return;
        	}
        	/* if(cno == null || cno == ""){
        		mini.alert("请先选择交易对手!");
        		return;
        	} */
        	if(settmeans == null || settmeans == ""){
        		mini.alert("请先选择清算路径!");
        		return;
        	}
        	var url;
			var data;
			if(settmeans=="NOS"){
				url="./Ifs/opics/nostMini.jsp";
	        }else{
	        	url="./Ifs/opics/setaMini.jsp";
	        }
			data={smeans:settmeans,ccy:ccy,cust:cno};
        	var btnEdit = this;
            mini.open({
                url: url,
                title: "选择清算账户",
                width: 700,
                height: 600,
                onload: function () {
                    var iframe = this.getIFrameEl();
                    iframe.contentWindow.SetData(data);
                },
                ondestroy: function (action) {
                    if (action == "ok") {
                        var iframe = this.getIFrameEl();
                        var data = iframe.contentWindow.GetData();
                        data = mini.clone(data);    //必须
                        if (data) {
                        	if(settmeans=="NOS"){
	                    		mini.get("settacct").setValue($.trim(data.nos));
	                    		mini.get("settacct").setText($.trim(data.nos));
	                        }else{
	                        	mini.get("settacct").setValue($.trim(data.sacct));
		                    	mini.get("settacct").setText($.trim(data.sacct));
	                        }
                            btnEdit.focus();
                        }
                    }

                }
            });
        }
		
        
      //托管机构选择
    	function onSafekeepacctEdit(e) {
            var btnEdit = this;
            mini.open({
                url: "./Ifs/opics/saccMini.jsp",
                title: "选择托管机构列表",
                width: 700,
                height: 600,
                ondestroy: function (action) {
                    if (action == "ok") {
                        var iframe = this.getIFrameEl();
                        var data = iframe.contentWindow.GetData();
                        data = mini.clone(data);    //必须
                        if (data) {
                            btnEdit.setValue(data.accountno);
                            btnEdit.setText(data.accountno);
                            btnEdit.focus();
                        }
                    }

                }
            });
        }
		
      
    	//BIC
    	function onBicEdit(e) {
            var btnEdit = this;
            var id=this.id;
            var buttonId=id.substring(id.length-2);
            mini.open({
                url: "./Ifs/opics/bicoMini.jsp",
                title: "SWIFT码列表",
                width: 700,
                height: 600,
                ondestroy: function (action) {
                    if (action == "ok") {
                        var iframe = this.getIFrameEl();
                        var data = iframe.contentWindow.GetData();
                        data = mini.clone(data);    //必须
                        if (data) {
                            btnEdit.setValue(data.bic);
                            btnEdit.setText(data.bic);
                            mini.get(buttonId).setEnabled(false);
                            btnEdit.focus();
                        }
                    }

                }
            });
        }
		
		
		//经办
        function add(){
        	mini.confirm("您确定经办?","系统警告",function(value){
				if(value=="ok"){
					CommonUtil.ajax({
		        		url: "/IfsOpicsSettleManageController/updateSdvpSlflag",
		        		data: {fedealno: row.fedealno,userid:userId,slflag:"1"},
		        		callback: function (data) {
		        			if (data.code == "error.common.0000") {
		        				mini.alert("经办成功",'提示信息',function(){
		        					top["win"].closeMenuTab();
		        				});
		        			} else {
		        				mini.alert("经办失败");
		        			}
		        		}
		        	});
				}
			});
        }
		
      //退回经办
    	 function back(){
    		mini.confirm("您确定退回经办?","系统警告",function(value){
				if(value=="ok"){
					CommonUtil.ajax({
						url: "/IfsOpicsSettleManageController/updateSdvpSlflag",
						data: {fedealno: row.fedealno,userid:userId,slflag:"0"},
						callback : function(data) {
							if (data.code == "error.common.0000") {
		        				mini.alert("退回经办成功",'提示信息',function(){
		        					top["win"].closeMenuTab();
		        				});
		        			} else {
		        				mini.alert("退回经办失败");
		        			}
						}
					});
				}
			});
    	} 
    	//复核
    	function check(){
    		if(userId==row.slioper){
    			mini.alert("复核人员不能与经办人员相同,请确认!","提示信息");
        		return;
        	}
    		mini.confirm("您确定复核?","系统警告",function(value){
				if(value=="ok"){
					CommonUtil.ajax({
						url: "/IfsOpicsSettleManageController/syncSdvp",
						data: {fedealno: row.fedealno,userid:userId,slflag:"2"},
						callback : function(data) {
							if (data.code == "error.common.0000") {
		        				mini.alert("复核成功",'提示信息',function(){
		        					top["win"].closeMenuTab();
		        				});
		        			} else {
		        				mini.alert("复核失败");
		        			}
						}
					});
				}
			});
    	}
    	//以'/'开头 的验证
        function onAcctnoValidation(e) {
            if(e.value == "" || e.value == null){//值为空，就不做校验
                return;
            }
            if (e.isValid) {
                if (isSlash(e.value) == false) {
                    e.errorText = "必须以斜杠/开头";
                    e.isValid = false;
                }
            }
        }
    	
        /* 是否以'/'开头 */
        function isSlash(v) {
            var re = new RegExp("^/");
            if (re.test(v)) return true;
            return false;
        }
        
        //关闭按钮
        function onCloseClick(e) {
        	var btnEdit = this;
        	var id=this.id;
        	btnEdit.setValue(null);
            btnEdit.setText(null);
        	var buttonId=id.substring(id.length-2);
        	mini.get(buttonId).setEnabled(true);
        }
    </script>
</body>
</html>