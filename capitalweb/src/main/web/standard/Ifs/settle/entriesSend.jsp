<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>
<body style="width:100%;height:100%;background:white">
<div class="mini-panel" title="基金往来账管理" style="width:100%;">
	<div style="width:100%;padding-top:10px;padding-bottom:10px">
		<div id="search_form" >
			<input id="fundCode" name="fundCode" class="mini-textbox" width="320px" labelField="true" label="基金代码：" labelStyle="text-align:right;" emptyText="请输入基金代码"/>
			<input id="subjCode" name="subjCode" class="mini-textbox" width="320px"  labelField="true" label="科目号：" labelStyle="text-align:right;" emptyText="请输入科目号"/>
			<input id="prdNo" name="prdNo" class="mini-combobox" width="320px"  labelField="true" label="产品类型：" labelStyle="text-align:right;"   data="CommonUtil.serverData.dictionary.FundType" emptyText="请选择产品类型"/>
			<input id="eventId" name="eventId" class="mini-combobox"  width="320px"   labelField="true"  label="会计事件：" labelStyle="text-align:right;"  emptyText="请选择会计事件"/>
			<input id="entry_date_begin" name="entry_date_begin" class="mini-datepicker"  width="320px"  labelField="true" label="开始日期：" valueType="string" labelStyle="text-align:right;" allowInput = "false" emptyText="选择账务开始日期" value="<%=__bizDate%>"/>
			<input id="entry_date_end" name="entry_date_end" class="mini-datepicker"  width="320px"  allowInput = "false" valueType="string" labelField="true" label="结束日期：" onValuechanged="compareDate" labelStyle="text-align:right;" emptyText="选择账务结束日期" value="<%=__bizDate%>"/>
			<input id="sendFlag" name="sendFlag" class="mini-combobox"  width="320px"  labelField="true" label="发送状态：" labelStyle="text-align:right;"   data="CommonUtil.serverData.dictionary.EntrySendFlag"/>
			<span style="float:right;">
				<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
				<a id="send_btn" class="mini-button" style="display: none"   onclick="resend()">发送</a>
				<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
<%--			<a id="exp_btn" class="mini-button" style="display: none"   onclick="exportMessage()">导出</a>--%>
			</span>
		</div>
	</div>
</div>

<div id="EntriesQuerys" class="mini-fit" style="margin-top: 5px;">
	<div id="form_grid" class="mini-datagrid borderAll" allowAlternating="true" style="width:100%;height:97.5%;"
		 sortMode="client" fitColumns="false" frozenStartColumn="0" multiSelect="true"  frozenEndColumn="1" onshowrowdetail="onShowRowDetail">
		<div property="columns">
			<div type="checkcolumn"></div>
			<div field="sendFlag" width="150" allowSort="false" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'EntrySendFlag'}">发送状态</div>
			<div field="flowId" width="200" allowSort="true" headerAlign="center" align="center">账务流水号</div>
			<div type="expandcolumn" width="80">操作</div>
			<div field="postDate" width="150" allowSort="true" headerAlign="center" align="center">账务日期</div>
			<div field="dealNo" width="200" allowSort="true" headerAlign="center" align="center">交易编号</div>
			<div field="prdName" width="150" allowSort="false" headerAlign="center" align="center" >产品类型</div>
			<div field="sponInstName" width="150" allowSort="false" headerAlign="center" align="center">记账机构</div>
			<div field="fundCode" width="100" allowSort="false" headerAlign="center" align="center">基金代码</div>
			<div field="eventName" width="100" allowSort="false" headerAlign="center" align="center">会计事件</div>
			<div field="retCode" width="100" allowSort="true" headerAlign="center" align="center">核心返回码</div>
			<div field="retMsg" width="300" allowSort="true" headerAlign="center" align="center">核心返回信息</div>
			</div>
	</div>
</div>

<div id="detailGrid_Form" style="display:none;">
	<div id="detail_grid" class="mini-datagrid" style="width:100%;height:250px;" allowResize="true" sortMode="client" allowAlternating="true">
		<div property="columns">
			<div field="entryId" width="180" allowSort="true" headerAlign="center" align="center">分录ID</div>
			<div field="flowSeq" width="80" allowSort="true" headerAlign="center" align="center">流水序号</div>
			<div field="subjCode" width="80" allowSort="true" headerAlign="center" align="center">科目号</div>
			<div field="subjName" width="450" allowSort="false" headerAlign="center" align="left">科目名称</div>
			<div field="debitCreditFlag" width="80" allowSort="false" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'DebitCreditFlag'}" >借贷标识</div>
			<div field="value" width="120" numberFormat="#,0.00" headerAlign="center" align="right" allowSort="true">金额</div>
			<div field="ccy" width="80" allowSort="false" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'Currency'}">币种</div>
			<div field="updateTime" width="150" allowSort="false" headerAlign="center" align="center">生成时间</div>
		</div>
	</div>
</div>


<script type="text/javascript">
	mini.parse();
	var grid = mini.get("form_grid");
	var detail_grid = mini.get("detail_grid");
	var detailGrid_Form = document.getElementById("detailGrid_Form");

	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex;
		var pageSize = e.data.pageSize;
		search(pageSize, pageIndex);
	});

	function search(pageSize, pageIndex) {
		var form = new mini.Form("search_form");
		if (form.isValid == false)
			return;
		//提交数据
		var data = form.getData();//获取表单多个控件的数据
		data['pageNumber'] = pageIndex + 1;
		data['pageSize'] = pageSize;
		var param = mini.encode(data); //序列化成JSON
		CommonUtil.ajax({
			url: '/TbEntryController/searchPageTbEntryGroupByFlowId',
			data: param,
			callback: function (data) {
				var grid = mini.get("form_grid");
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
				grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}

	function onShowRowDetail(e) {
		var grid = e.sender;
		var row = e.record;
		var td = grid.getRowDetailCellEl(row);
		td.appendChild(detailGrid_Form);
		detailGrid_Form.style.display = "block";
		searchDetail(10,0,row.flowId);
	}

	function searchDetail(pageSize,pageIndex,flowId){
		var data = {};
		data["flowId"]=flowId;
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		var param = mini.encode(data);
		CommonUtil.ajax( {
			url:"/TbEntryController/searchPageTbEntryByFlowId",
			data:param,
			callback : function(data) {
				//设置分页
				detail_grid.setTotalCount(data.obj.total);
				detail_grid.setPageIndex(pageIndex);
				detail_grid.setPageSize(pageSize);
				//设置数据
				detail_grid.setData(data.obj.rows);
			}
		});
	}

	detail_grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex;
		var pageSize = e.data.pageSize;
		var row = credit_query_grid.getSelected();
		searchDetail(pageSize,pageIndex,row.flowId);
	});

	//重新发送失败账务
	function resend() {
		var grid = mini.get("form_grid");
		//获取选中行
		var rows = grid.getSelecteds();
		if (rows.length > 0) {
			if (rows.length > 1) {
				mini.alert("不支持多条发送", "系统提示");
				return;
			}
			var row = rows[0];
			if(row.sendFlag == '3' || row.sendFlag == '4'){
				mini.alert("该账务已发送成功！无法再次发送", "系统提示");
				return;
			}
			var data={'flowId':row.flowId}
			var param = mini.encode(data); //序列化成JSON*/
			CommonUtil.ajax({
				url: '/TbEntryController/sendCore',
				data: param,
				callback: function (data) {
					query();
				}
			});
		} else {
			mini.alert("请至少选择一条数据!", "系统提示");
		}
	}

	function clear() {
		var form = new mini.Form("#search_form");
		form.clear();
		query();

	}

	function init(){
		var form = new mini.Form("search_form");
		//提交数据
		var data = form.getData();//获取表单多个控件的数据
		data['sendFlag'] = 'send';
		var param = mini.encode(data); //序列化成JSON
		CommonUtil.ajax({
			url: '/TbEntryController/searchEvent',
			data: param,
			callback: function (data) {
				mini.get("eventId").load(data.obj);
			}
		});
	}

	function compareDate() {
		var sta = mini.get("entry_date_begin");
		var end = mini.get("entry_date_end");
		if (sta.getValue() > end.getValue() && end.getValue("")) {
			mini.alert("开始时间不能大于结束时间", "系统提示");
			return end.setValue(sta.getValue());
		}
	}

	//导出
	function exportMessage(){
		var content = grid.getData();
		if(content.length == 0){
			mini.alert("请先查询数据");
			return;
		}
		mini.confirm("您确认要导出Excel吗?","系统提示",
				function (action) {
					if (action == "ok"){
						var form = new mini.Form("#search_form");
						var data = form.getData(true);

						var fields = null;
						var fields  = "<input type='hidden' id='dealNo' name='dealNo' value='"+data.dealNo+"'>";
						fields += "<input type='hidden' id='prdNo' name='prdNo' value='"+data.prdNo+"'>";
						fields += "<input type='hidden' id='subjCode' name='subjCode' value='"+data.subjCode+"'>";
						fields += "<input type='hidden' id='eventId' name='eventId' value='"+data.eventId+"'>";
						fields += "<input type='hidden' id='entry_date_begin' name='entry_date_begin' value='"+data.entry_date_begin+"'>";
						fields += "<input type='hidden' id='entry_date_end' name='entry_date_end' value='"+data.entry_date_end+"'>";
						fields += "<input type='hidden' id='sendFlag' name='sendFlag' value='"+data.sendFlag+"'>";
						fields += "<input type='hidden' id='beanNo' name='beanNo' value='194'>";
						fields += "<input type='hidden' id='excelName' name='excelName' value='EntriesQuery.xls'>";
						fields += "<input type='hidden' id='branchId' name='branchId' value='"+branchId+"'>";


						var urls = CommonUtil.pPath + "/sl/TbEntryController/exportExcel";
						$('<form action="'+ urls +'" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();
					}
				}
		);
	}

	//查询的方法
	function query() {
		var grid = mini.get("form_grid");
		search(grid.pageSize, 0);
	}

	$(document).ready(function () {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
			search(10, 0);
			init(); //初始化事件下拉
			loadProduct();
		});
	})

	// 获取所有产品信息
	function loadProduct(){
		var branchId = "<%=__sessionUser.getBranchId()%>";
		CommonUtil.ajax({
			url: "/ProductController/searchProduct",
			data: {branchId:branchId, prdType:"7000",isAcup:'1'},
			callback: function (data) {
				var producType = new Array();
				for(var i=0;i<data;i++){

				}
			}
		});
	}
</script>
</body>
</html>