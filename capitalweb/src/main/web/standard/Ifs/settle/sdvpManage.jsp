
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title></title>
  </head>
  
<body style="width:100%;height:100%;background:white">
	<fieldset class="mini-fieldset">
	<legend>查询</legend>
	<div id="search_form" style="width:100%;">
		<!-- <input id="br" name="br" class="mini-textbox" labelField="true" label="部门：" labelStyle="text-align:right;" emptyText="请输入部门" renderer="CommonUtil.dictRenderer" data-options="{dict:'Br'}"/> -->
		<input id="product" name="product" class="mini-combobox" labelField="true" label="产品代码：" labelStyle="text-align:right;" allowInput="true"
			textField="typeValue" valueField="typeId" emptyText="请选择产品代码"/>
		<input id="prodtype" name="prodtype" class="mini-combobox" labelField="true" label="产品类型：" labelStyle="text-align:right;" allowInput="true"
			textField="typeValue" valueField="typeId" emptyText="请选择产品类型"/>
		<input id="ccy" name="ccy" class="mini-combobox" labelField="true" label="币种：" labelStyle="text-align:right;"
			emptyText="请选择币种" data="CommonUtil.serverData.dictionary.Currency"/>
		<input id="cliname" name="cliname" class="mini-textbox" labelField="true" label="客户中文名称：" labelStyle="text-align:right;" emptyText="请输入客户中文名称"/>
		<input id="settmeans" name="settmeans" class="mini-combobox" labelField="true" label="清算方式：";
			textField="typeValue" valueField="typeId" labelStyle="text-align:right;" emptyText="请选择清算方式"/>
		<!-- <input id="settacct" name="settacct" class="mini-textbox" labelField="true" label="清算账户：" labelStyle="text-align:right;" emptyText="请输入清算账户"/> -->
		<input id="delrecind" name="delrecind" class="mini-combobox" labelField="true" labelStyle="text-align:right;" data="[{text:'付款',id:'D'},{text:'收款',id:'R'}]" label="收付标识：" labelStyle="text-align:right;" emptyText="请选择收付标识"/>
		<input id="slioper" name="slioper" class="mini-textbox" labelField="true" label="经办人：" labelStyle="text-align:right;" emptyText="请输入经办人"/>
		<input id="veroper" name="veroper" class="mini-textbox" labelField="true" label="复核人：" labelStyle="text-align:right;" emptyText="请输入复核人"/>
		<nobr>
			<input id="startSDate" name="startSDate" class="mini-datepicker" labelField="true" label="经办日期："
						ondrawdate="onDrawDateStartS" labelStyle="text-align:right;" emptyText="起始日期" format="yyyy-MM-dd"/>
			<span>~</span>
			<input id="endSDate" name="endSDate" class="mini-datepicker" 
						ondrawdate="onDrawDateEndS" emptyText="结束日期" format="yyyy-MM-dd"/>
		</nobr>
		<!-- <nobr>
			<input id="startVDate" name="startVDate" class="mini-datepicker" labelField="true" label="复核日期："
						ondrawdate="onDrawDateStart" labelStyle="text-align:right;" emptyText="起始日期" format="yyyy-MM-dd"/>
			<span>~</span>
			<input id="endVDate" name="endVDate" class="mini-datepicker" 
						ondrawdate="onDrawDateEnd" emptyText="结束日期" format="yyyy-MM-dd"/>
		</nobr> -->
		
		<span style="float:right;margin-right: 160px">
			<a id="search_btn" class="mini-button" style="display: none"   onclick="query">查询</a>
			<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear">清空</a>
		</span>
	</div>
	</fieldset>
	
	<span style="margin:2px;display: block;">
		<a  id="add_btn" class="mini-button" style="display: none"   onclick="add()">新增</a>
		<a  id="edit_btn" class="mini-button" style="display: none"   onclick="edit">修改</a>
		<!-- <a id="edit_btn" class="mini-button" style="display: none"   onclick="update">更新opics</a> -->
		<a id="handle_btn" class="mini-button" style="display: none"   onclick="onRowDblClick">经办</a>
		<a id="sync_btn" class="mini-button" style="display: none"   onclick="syncStatus">状态更新</a>
		<a id="delete_btn" class="mini-button" style="display: none"   onclick="del">删除</a>
		
	</span>
	<div class="mini-splitter" vertical="true" handlerSize="2px" style="width:100%;height:100%;" allowResize="false">
	    <div size="50%">
	    	<div id="SdvpManage" class="mini-fit" style="margin-top: 2px;">
	    		<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" allowResize="true" onselectionchanged="onSelectionChangedSearchDetail" multiSelect="true" 
                    idField="id" pageSize="20" multiSelect="false" sortMode="client" allowAlternating="true" onrowdblclick="onRowDblClick">
					<div property="columns">
						<div type="checkcolumn"></div>
						<div type="indexcolumn" headerAlign="center" width="40">序号</div>
						<div field="fedealno" width="150" allowSort="true" headerAlign="center" align="center">流水号</div>
						<div field="br" width="80" allowSort="true" headerAlign="center" align="center">部门</div>
						<div field="product" width="80" allowSort="true" headerAlign="center" align="center">产品代码</div>
						<div field="prodtype" width="60" allowSort="false" headerAlign="center" align="center">产品类型</div>
						<div field="descrcn" width="100" allowSort="false" headerAlign="center" align="center">业务中文名称</div>
						<div field="ccy" width="60" allowSort="false" headerAlign="center" align="center">币种</div>
						<div field="cno" width="100" allowSort="false" headerAlign="center" align="center">交易对手号</div>
						<div field="cliname" width="100" allowSort="false" headerAlign="center" align="center">客户中文名称</div>
						<div field="safekeepacct" width="100" allowSort="false" headerAlign="center" align="center">托管机构</div>
						<div field="settmeans" width="100" allowSort="false" headerAlign="center" align="center">清算方式</div>
						<div field="settacct" width="120" allowSort="false" headerAlign="center" align="center">清算账户</div>
						<div field="delrecind" width="60" allowSort="false" headerAlign="center" align="center">收付标识</div>
						<div field="slflag" width="150" allowSort="false" headerAlign="center" align="center" renderer="onSlflagRenderer">流程状态</div>
						<div field="status" name="status" width="80" allowSort="true" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'DealStatus'}">同步状态</div>
						<div field="slioper" width="150" allowSort="false" headerAlign="center" align="">经办人</div>
						<div field="slidate" width="150" allowSort="false" headerAlign="center" align="" renderer="onDateRenderer">经办时间</div>
						<div field="veroper" width="180" allowSort="false" headerAlign="center" align="center">复核人</div>
						<div field="verdate" width="180" allowSort="false" headerAlign="center" align="center" renderer="onDateRenderer">复核时间</div>
					</div>
				</div>  
			</div>
			</div>
			
			<div>
				<div id="SdvpManage" class="mini-fit" style="margin-top: 2px;">
				<div id="detailgrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" allowResize="true" 
                    idField="id" pageSize="10" multiSelect="false" sortMode="client" allowAlternating="true">
					<div property="columns">
						<div type="indexcolumn" headerAlign="center" width="40">序号</div>
						<div field="dcc" width="60" allowSort="false" headerAlign="center" align="">DCC</div>
						<div field="acctno" width="100" allowSort="false" headerAlign="center" align="center">账号</div>
						<div field="bic" width="100" allowSort="false" headerAlign="center" align="center">SWIFT编码</div>
						<div field="c1" width="100" allowSort="false" headerAlign="center" align="center">C1</div>
						<div field="c2" width="100" allowSort="false" headerAlign="center" align="center">C2</div>
						<div field="c3" width="100" allowSort="false" headerAlign="center" align="center">C3</div>
						<div field="c4" width="100" allowSort="false" headerAlign="center" align="center">C4</div>
					</div>
				</div>  
			</div>
			
			</div>
	    	
	    	
	    	
	    </div>
    </div>
	
	
	
	
	
	
	

	<script>
		mini.parse();
		var flashFlag="1";//是否从0开始  ,0是，1不是

		function onDateTimeRenderer(e) {
			if(e.value!=null){
		    	var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)==null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g,'/'));
		        if (value) return mini.formatDate(value, 'yyyy-MM-dd');
			}
	    }
		function onDateRenderer(e) {
			if(e.value!=null){
				var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)==null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g,'/'));
		        if (value) return mini.formatDate(value, 'yyyy-MM-dd');
			}
	    }
		var Slflags = [{ id: '0', text: '待经办' }, { id: '1', text: '已经办待复核'},{ id: '2', text: '已复核'}];
		function onSlflagRenderer(e) {
            for (var i = 0, l = Slflags.length; i < l; i++) {
                var g = Slflags[i];
                if (g.id == e.value) return g.text;
            }
            return "";
        }
		var grid = mini.get("datagrid");
		grid.on("beforeload", function (e) {
			e.cancel = true;
			flashFlag="0";
			var pageIndex = e.data.pageIndex; 
			var pageSize = e.data.pageSize;
			search(pageSize,pageIndex);
		});
		grid.on("select",function(e){
			var row=e.record;
			if(row.slflag == "0"){
				mini.get("handle_btn").setEnabled(true);
			}else{
				mini.get("handle_btn").setEnabled(false);
			}
			
			/* if(row.slflag == "0"||row.slflag == "1"){
				mini.get("delete_btn").setEnabled(true);
			}else{
				mini.get("delete_btn").setEnabled(false);
			} */
			
			if(row.status == "1"){//已导入opics但未处理的
				mini.get("edit_btn").setEnabled(false);
			}else{
				mini.get("edit_btn").setEnabled(true);
			}
		}); 
		var detailgrid =mini.get("detailgrid");
		detailgrid.on("beforeload",function(e){
	         e.cancel = true;
	         var pageIndex = e.data.pageIndex; 
	         var pageSize = e.data.pageSize;
	         var row = grid.getSelected();
	         searchDetail(pageSize,pageIndex,row.fedealno);
	     });
		//选中一条=》出现明细
	    function onSelectionChangedSearchDetail(e){
	        var datagrid = e.sender;
	        var record = datagrid.getSelected();
	        if(!record){
			     return false;
		    }
	        row=record;
	        searchDetail(10,0,row.fedealno);
	    }

		function search(pageSize,pageIndex){
			var form = new mini.Form("#search_form");
			form.validate();
			if(form.isValid() == false){
				mini.alert("表单填写错误,请确认!","提示信息");
				return;
			}
			if(flashFlag=="0"){
				//是否从0开始  ,0是，1不是
				flashFlag="1";
			}else{
				pageIndex=grid.pageIndex;
			}
			var data = form.getData(true);
			data['pageNumber'] = pageIndex+1;
			data['pageSize'] = pageSize;
			//自贸区改造加br
			data['br']=opicsBr;
			var params = mini.encode(data);
			CommonUtil.ajax({
				url:'/IfsOpicsSettleManageController/searchPageSdvp',
				data:params,
				callback : function(data) {
					var grid = mini.get("datagrid");
					grid.setTotalCount(data.obj.total);
					grid.setPageIndex(pageIndex);
					grid.setPageSize(pageSize);
					grid.setData(data.obj.rows);
					mini.get("detailgrid").clearRows();
				}
			});
		}

		/* function update(){
			var row = grid.getSelected();
			if(row){
				var url = CommonUtil.baseWebPath() + "/settle/sdvpEdit.jsp?action=edit";
				var tab = { id: "MiniSdvpEdit", name: "MiniSdvpEdit", title: "SDVP", url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
				var paramData = {selectData:row};
				CommonUtil.openNewMenuTab(tab,paramData);
			} else {
				mini.alert("请选中一条记录！","消息提示");
			}
		} */
		
		function edit(){
			var row = grid.getSelected();
			if(row){
				var url = CommonUtil.baseWebPath() + "/settle/sdvpEdit.jsp?action=edit";
				var tab = { id: "MiniSdvpEdit", name: "MiniSdvpEdit", title: "SDVP", url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
				var paramData = {selectData:row};
				CommonUtil.openNewMenuTab(tab,paramData);
			} else {
				mini.alert("请选中一条记录！","消息提示");
			}
		}

		function add(){
			flashFlag="0";
			var url = CommonUtil.baseWebPath() + "/settle/sdvpEdit.jsp?action=add";
			var tab = { id: "MiniSdvpAdd", name: "MiniSdvpAdd", title: "SDVP", url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
			var paramData = {selectData:""};
			CommonUtil.openNewMenuTab(tab,paramData);
		}
		
		
		function onRowDblClick() {
			var row = grid.getSelected();
			if(row){
				var url = CommonUtil.baseWebPath() + "/settle/sdvpEdit.jsp?action=detail&detailType=1";
				var tab = { id: row.fedealno, name: row.fedealno, title: "SDVP", url: url ,showCloseButton:true,parentId:top["win"].tabs.getActiveTab().name};
				var paramData = {selectData:row};
				CommonUtil.openNewMenuTab(tab,paramData);
			} else {
				mini.alert("请选中一条记录！","消息提示");
			}
		}
		
		function query(){
			flashFlag="0";
			search(grid.pageSize,0);
		}
		
		function clear(){
			//mini.get("br").setValue("");
			mini.get("product").setValue("");
			mini.get("prodtype").setValue("");
			mini.get("ccy").setValue("");
			mini.get("cliname").setValue("");
			mini.get("settmeans").setValue("");
			//mini.get("settacct").setValue("");
			mini.get("slioper").setValue("");
			mini.get("veroper").setValue("");
			mini.get("startSDate").setValue(null);
			mini.get("endSDate").setValue(null);
			//mini.get("startVDate").setValue(null);
			//mini.get("endVDate").setValue(null);
			mini.get("delrecind").setValue(null);
			query();
		}
		function initButton(){
			mini.get("add_btn").setVisible(true);
			mini.get("edit_btn").setVisible(true);
		}
		//删除
		function del() {
			var grid = mini.get("datagrid");
			var row = grid.getSelected();
			if (row) {
				mini.confirm("您确认要删除选中记录?","系统警告",function(value){
				if(value=="ok"){
					CommonUtil.ajax({
					url: "/IfsOpicsSettleManageController/deleteSdvp",
					data: {fedealno: row.fedealno},
					callback: function (data) {
						if (data.code == 'error.common.0000') {
								mini.alert("删除成功");
								grid.reload();
						} else {
								mini.alert("删除失败");
						}
					}
						});
						}
					});
				}
				else {
					mini.alert("请选中一条记录！", "消息提示");
				}
			}
		
		// 初始化
		$(document).ready(function() {
			//控制按钮显示
			$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
				queryProduct();
				queryProdtype();
				querySettmeans();
				initButton();
				search(10, 0);
			});
		});
		
		
		
		
		function searchDetail(pageSize,pageIndex,fedealno){
	        if(flashFlag=="0"){
				//是否从0开始  ,0是，1不是
				flashFlag="1";
			}else{
				pageIndex=grid.pageIndex;
			}
			var data = {};
	        data["fedealno"]=fedealno;
	        data['pageNumber']=pageIndex+1;
	        data['pageSize']=pageSize;
	        var param = mini.encode(data);
	        CommonUtil.ajax( {
	        	url:"/IfsOpicsSettleManageController/searchPageSdvpDetails",
	            data:param,
	            callback : function(data) {
	                var grid =mini.get("detailgrid");
	                //设置分页
	                grid.setTotalCount(data.obj.total);
	                grid.setPageIndex(pageIndex);
	                grid.setPageSize(pageSize);
	                //设置数据
	                grid.setData(data.obj.rows);
	                
	            }
	        });
	    }
		
		//同步
	    function sync() {
	        var row = grid.getSelected();
	        if (row) {
	            mini.confirm("您确认要同步选中记录?","系统警告",function(value){
	                if(value=="ok"){
	                    CommonUtil.ajax({
	                        url: "/IfsOpicsSettleManageController/syncSdvp",
	                        data: {fedealno: row.fedealno},
	                        callback: function (data) {
	                        	mini.alert(data.obj.retMsg,data.obj.retCode);
	                        	grid.reload();
	                        }
	                    });
	                }
	            });
	        }else {
	            mini.alert("请选中一条记录！", "消息提示");
	        }
	    }
		
	  //经办日期
		function onDrawDateStartS(e) {
	        var startSDate = e.date;
	        var endSDate= mini.get("endSDate").getValue();
	        if(CommonUtil.isNull(endSDate)){
	        	return;
	        }
	        if (endSDate.getTime() < startSDate.getTime()) {
	            e.allowSelect = false;
	        }
	    }
		function onDrawDateEndS(e) {
	        var endSDate = e.date;
	        var startSDate = mini.get("startSDate").getValue();
	        if(CommonUtil.isNull(startSDate)){
	        	return;
	        }
	        if (endSDate.getTime() < startSDate.getTime()) {
	            e.allowSelect = false;
	        }
	    }
		
		//复核日期
	    function onDrawDateStart(e) {
	        var startVDate = e.date;
	        var endVDate= mini.get("endVDate").getValue();
	        if(CommonUtil.isNull(endVDate)){
	        	return;
	        }
	        if (endVDate.getTime() < startVDate.getTime()) {
	            e.allowSelect = false;
	        }
	    }
		function onDrawDateEnd(e) {
	        var endVDate = e.date;
	        var startVDate = mini.get("startVDate").getValue();
	        if(CommonUtil.isNull(startVDate)){
	        	return;
	        }
	        if (endVDate.getTime() < startVDate.getTime()) {
	            e.allowSelect = false;
	        }
	    }
		
		// 点击按钮，修改同步状态为‘同步成功’
		function syncStatus(){
			var rows = grid.getSelecteds();
			if(rows.length>0){//选中行校准
				 var str="";
			      if(rows.length==1){
			      	str=rows[0].fedealno;	
			      }else{
			      	for(var i=0;i<rows.length;i++){
			          	if(i == 0){
			          		str=rows[i].fedealno+",";	
			          	}else{
			          		if(i==rows.length-1){
			          			str=str+rows[i].fedealno;
			          		}else{
			          			str=str+rows[i].fedealno+",";
			          		}
			          	}
			          }
			      }
			      CommonUtil.ajax({
			    	  url: "/IfsOpicsSettleManageController/checkSvdpStatus",
		            	 data: {fedealno:str,type:1},
		                callback: function (data) {
		                	if(data.code == 'error.common.0000'){
		                		mini.alert("状态已手动更新，请稍等");
		                		search(10,0);
		                	}
		                }
		            });
			}else{
				CommonUtil.ajax({
					url: "/IfsOpicsSettleManageController/checkSvdpStatus",
	            	data: {type:2},
	                callback: function (data) {
	                	if(data.code == 'error.common.0000'){
	                		mini.alert("状态已手动更新，请稍等");
	                		search(10,0);
	                	}
	                }
	            });
			}
		}
		
		//查询产品代码
		function queryProduct(){
			CommonUtil.ajax({
				url:"/IfsOpicsProdController/searchProdcode",
				data:{},
				callback:function(data){
					var length = data.length;
	             	var jsonData = new Array();
	 			   	for(var i=0;i<length;i++){
	 			   		if(data[i].pdesc != null){
	 			   			var tyId = data[i].pcode;
		 			   		jsonData.add({'typeId':tyId,'typeValue':tyId});
	 			   		}
	 			   	};
	 			   	mini.get("product").setData(jsonData);
	             }
	   		});
	    }
		
		//查询产品类型
		function queryProdtype(){
			CommonUtil.ajax({
				url:"/IfsOpicsTypeController/searchProdtype",
				data:{},
				callback:function(data){
					var length = data.length;
	             	var jsonData = new Array();
	 			   	for(var i=0;i<length;i++){
	 			   		if(data[i].type != null){
	 			   			var tyId = data[i].type;
		 			   		jsonData.add({'typeId':tyId,'typeValue':tyId});
	 			   		}
	 			   	};
	 			   	mini.get("prodtype").setData(jsonData);
	             }
	   		});
	    }
		
		//查询清算方式
		function querySettmeans(){
			CommonUtil.ajax({
				url:"/IfsOpicsSetmController/searchSettmeans",
				data:{},
				callback:function(data){
					var length = data.length;
	             	var jsonData = new Array();
	 			   	for(var i=0;i<length;i++){
	 			   		if(data[i].settlmeans != null){
	 			   			var tyId = data[i].settlmeans;
		 			   		jsonData.add({'typeId':tyId,'typeValue':tyId});
	 			   		}
	 			   	};
	 			   	mini.get("settmeans").setData(jsonData);
	             }
	   		});
	    }
	</script>
</body>
</html>