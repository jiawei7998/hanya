<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
    <head>
        <title>Esb报文信息</title>
        <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    </head>
<body style="width:100%;height:100%;background:white">
   <fieldset class="mini-fieldset title">
    <legend>Esb报文信息查询</legend>
    <div id="search_form" style="width: 100%;">
        <input id="service" name="service" class="mini-textbox" width="320px" labelField="true" label="服务名：" labelStyle="text-align:right;" emptyText="请输入服务名" />
        <input id="dealno" name="dealno" class="mini-textbox" width="320px" labelField="true" label="交易单号：" labelStyle="text-align:right;" emptyText="请输入交易单号" />
        <input id="custno" name="custno" class="mini-textbox" width="320px" labelField="true" label="交易对手：" labelStyle="text-align:right;" emptyText="请输入交易对手" />
        
        <span style="float:right;margin-right: 150px">
            <a class="mini-button" style="display: none"  id="search_btn"  onclick="search()">查询</a>
            <a class="mini-button" style="display: none"  id="clear_btn"  onclick="clear()">清空</a>
        </span>
    </div>
    </fieldset>
    
    <div class="mini-fit" style="width:100%;height:100%;">
        <div id="data_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" 
            sortMode="client" allowAlternating="true"   idField="userId"  
            border="true" allowResize="true" >
            <div property="columns">
               
                <div field="service" headerAlign="center" align="center" width="150px">服务名</div>
                <div field="dealno" headerAlign="center" align="center" width="200px">交易单号</div>
                <div field="custno" headerAlign="center" align="center" width="120px">交易对手</div>
                <div field="product" headerAlign="center" align="center" width="80px">产品</div>
                <div field="producttype" headerAlign="center" align="center" width="80px">产品类型</div>
                <div field="amount" headerAlign="center" align="center" width="80px" >交易金额</div>
                <div field="ccy" headerAlign="center" align="center" width="80px">交易币种</div>
                <div field="setmeans" headerAlign="center" align="center" width="80px">报文类型</div>
                <div field="senddate" headerAlign="center" align="center" width="80px" renderer="onDateRenderer">发送时间</div>
                <div field="recdate" headerAlign="center" align="center" width="80px" renderer="onDateRenderer">接收时间</div>
                <div field="sendmsg" headerAlign="center" align="center" autoEscape="true"  width="200px">发送报文</div>
                <div field="recmsg" headerAlign="center" align="center" autoEscape="true" width="200px" >接收报文</div>
                <div field="rescode" headerAlign="center" align="center" width="200px">返回状态码</div>
                <div field="resmsg" headerAlign="center" align="center" width="300px">返回信息</div>
                <div field="sefile" headerAlign="center" align="center" width="250px">发送文件路径</div>
                <div field="refile" headerAlign="center" align="center" width="250px">接收文件路径</div>
              
                
            </div>
        </div>
    </div>
</body>
<script>
    mini.parse();

    var grid = mini.get("data_grid");
    grid.on("beforeload", function(e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        searchs(pageSize, pageIndex);
    });

    $(document).ready(function() {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
            search();
        });
    });

    function onDateRenderer(e) {
        var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)==null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g,'/'));
        if (value) return mini.formatDate(value, 'yyyy-MM-dd');
    }
    
    
    function setData(data){
        var form = new mini.Form("#search_form");
        form.setData(data);
    }
    
    function searchs(pageSize, pageIndex) {
        var form = new mini.Form("#search_form");
        form.validate();
        if (form.isValid() == false) {
            mini.alert("表单填写错误,请确认!", "提示信息");
            return;
        }

        var data = form.getData();
        
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        data['branchId']=branchId;
        var params = mini.encode(data);
        CommonUtil.ajax({
            url : "/IfsEsbMessageController/searchEsbList",
            data : params,
            callback : function(data) {
                var grid = mini.get("data_grid");
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }
  //查询按钮
    function search(){
        searchs(grid.pageSize,0);
    }
    //清空
    function clear(){
        var form=new mini.Form("search_form");
        form.clear();
        search();
    }

    function GetData() {
        var grid = mini.get("data_grid");
        var row = grid.getSelected();
        return row;
    }
    

    function CloseWindow(action) {
        if (window.CloseOwnerWindow)
            return window.CloseOwnerWindow(action);
        else
            window.close();
    }

    function onOk() {
        CloseWindow("ok");
    }

    //关闭窗口
    function onCancel() {
        CloseWindow("cancel");
    }
    
</script>
</html>