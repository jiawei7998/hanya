<%--
  Created by IntelliJ IDEA.
  User: hspcadmin
  Date: 2021/10/18
  Time: 21:52
  To change this template use File | Settings | File Templates.
--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../../global.jsp" %>
<html>
<head>
    <title>大额清算查询</title>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/hideBtn.js"></script>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset title">
    <legend>大额清算查询</legend>
    <div id="search_form" style="width: 100%;">
        <input id="dealno" name="dealno" class="mini-textbox" width="320px" labelField="true" label="流水号："
               labelStyle="text-align:right;" emptyText="流水号"/>
        <input id="product" name="product" class="mini-textbox" width="320px" emptyText="产品代码" labelField="true"
               label="产品代码：" labelStyle="text-align:right;"/>
        <input id="fedealno" name="fedealno" class="mini-textbox" width="320px" emptyText="审批单号" labelField="true"
               label="审批单号：" labelStyle="text-align:right;"/>
        <%--        <input id="status" name="status" class="mini-combobox" width="320px" data="CommonUtil.serverData.dictionary.DealStatus"--%>
        <%--               emptyText="请选择同步状态" labelField="true"  label="同步状态：" labelStyle="text-align:right;"/>--%>
        <input id='vdate' name='vdate' class='mini-datepicker' width="320px" style='width:320px' labelField='true'
               label='付款日期：' labelStyle='text-align:right'/>
        <span style="float:right;margin-right: 150px">
            <a class="mini-button" style="display: none" id="search_btn" onclick="search()">查询</a>
            <a class="mini-button" style="display: none" id="clear_btn" onclick="clear()">清空</a>
        </span>
    </div>
</fieldset>

<div class="mini-fit" style="width:100%;height:100%;">
    <div id="datagrid" class="mini-datagrid borderAll" style="height:100%;" idField="setno" allowResize="true"
         multiSelect="true" allowCellEdit="true" border="true">
        <div property="columns">
            <div type="indexcolumn" headerAlign="center" align="center" width="40px">序列</div>
            <div field="fedealno" headerAlign="center" align="left" width="120px">审批单号</div>
            <div field="dealno" headerAlign="center" align="left" width="120px">流水号</div>
            <div field="settflag" headerAlign="center" align="center" width="120px" renderer="CommonUtil.dictRenderer"
                 data-options="{dict:'settflag'}">处理状态
            </div>
            <div field="product" headerAlign="center" align="center" width="120px">产品代码</div>
            <div field="prodtype" headerAlign="center" align="center" width="120px">产品类型</div>
            <div field="cname" headerAlign="center" align="center" width="160px">交易对手</div>
            <div field="vdate" width="100" align="center" headerAlign="center" renderer="onDateRenderer">付款日期</div>
            <div field="payrecind" headerAlign="center" align="center" width="120px" renderer="CommonUtil.dictRenderer"
                 data-options="{dict:'payDirection'}">收付方向
            </div>
            <div field="ccy" headerAlign="center" width="120px" renderer="CommonUtil.dictRenderer"
                 data-options="{dict:'Currency'}">币种
            </div>
            <div field="amount" width="120" numberFormat="#,0.00" headerAlign="center" align="right" allowSort="true">
                金额
            </div>
            <div field="cdate" width="100" align="center" headerAlign="center" renderer="onDateRenderer">创建日期</div>
            <div field="setmeans" headerAlign="center" align="center" width="120px">清算方式</div>
            <div field="setacct" headerAlign="center" align="left" width="120px">清算账户</div>
            <div field="payBankid" headerAlign="center" align="left" width="200px">付款行行号</div>
            <div field="payBankname" headerAlign="center" align="left" width="200px">付款行行名</div>
            <div field="payUserid" headerAlign="center" align="left" width="200px">付款账号</div>
            <div field="payUsername" headerAlign="center" align="left" width="200px">付款账号名称</div>
            <div field="recBankid" headerAlign="center" align="left" width="200px">收款行行号</div>
            <div field="recBankname" headerAlign="center" align="left" width="200px">收款行行名</div>
            <div field="recUserid" headerAlign="center" align="left" width="200px">收款账号</div>
            <div field="recUsername" headerAlign="center" align="left" width="200px">收款账号名称</div>
            <div field="hurryLevel" headerAlign="center" align="center" width="120px">加急等级</div>
            <div field="ioper" headerAlign="center" align="center" width="120px">经办人员</div>
            <div field="voper" headerAlign="center" align="center" width="120px">复核人员</div>
            <div field="remarkFy" headerAlign="center" align="center" width="120px">附言</div>
            <div field="dealflag" headerAlign="center" align="center" width="120px" renderer="CommonUtil.dictRenderer"
                 data-options="{dict:'dealflag'}">经办状态
            </div>
            <div field="voidflag" headerAlign="center" align="center" width="120px" renderer="CommonUtil.dictRenderer"
                 data-options="{dict:'voidflag'}">撤销标志
            </div>
        </div>
    </div>
</div>
</body>
<script type="text/javascript">
    mini.parse();

    var grid = mini.get("datagrid");
    var form = new mini.Form("#search_form");

    function onDateRenderer(e) {
        var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value) == null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g, '/'));
        if (value) return mini.formatDate(value, 'yyyy-MM-dd');
    }

    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        searchs(pageSize, pageIndex);
    });


    function data_string(str) {
        var d = eval('new ' + str.substr(1, str.length - 2));
        var ar_date = [d.getFullYear(), d.getMonth() + 1, d.getDate()];
        for (var i = 0; i < ar_date.length; i++) ar_date[i] = dFormat(ar_date[i]);
        return ar_date.join('-');

        function dFormat(i) {
            return i < 10 ? "0" + i.toString() : i;
        }
    }

    //查询按钮
    function search() {
        searchs(grid.pageSize, 0);
    }

    //查询数据
    function searchs(pageSize, pageIndex) {
        var form = new mini.Form("search_form");
        var data = form.getData(true);
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        data['sysDate'] = sysDate;
        url = "/TrdSettleController/queryAllTrdSettle";
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: url,
            data: params,
            callback: function (data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }

    //清空
    function clear() {
        var form = new mini.Form("search_form");
        form.clear();
        search();
    }

    // 初始化
    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
            search(10, 0);
        });
    });

</script>
</html>
