<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title></title>
  </head>

<body style="width:100%;height:100%;background:white">
    <div>
        <div id="field" class="fieldset-body">
            <table id="field_form"  style="text-align:left;margin:auto;width:90%;" class="mini-sltable">
                <tr>
                    <td colspan="2" style="text-align:center;">
                    	<span id="labell" style="font-size: medium;">Beneficlary:</span>
                    </td>
                </tr>
                <tr>
                    <td>
                    	<input id="p1" name="p1" class="mini-textbox"  style="width:100%" />
                    </td>
                </tr>
                <tr>
                    <td>
                    	<input id="p2" name="p2" class="mini-textbox"  style="width:100%" />
                    </td>
                </tr>
                <tr>
                    <td>
                    	<input id="p3" name="p3" class="mini-textbox"  style="width:100%" />
                    </td>
                </tr>
                <tr>
                    <td>
                    	<input id="p4" name="p4" class="mini-textbox"  style="width:100%" />
                    </td>
                </tr>
                
                <tr>
                    <td colspan="2" style="text-align:center;">
                        <a  id="save_btn" class="mini-button" style="display: none"    onclick="onOk">保存</a>
                        <a id="cancel_btn" class="mini-button" style="display: none"    onclick="onCancel">取消</a>
                    </td>
                </tr>
            </table>
            
        </div>
    </div>
    <script type="text/javascript">
        mini.parse();
        var form = new mini.Form("field_form");
        function SetData(data) {
        	$("#labell").html(data.name);
            if (data.action == "edit"||data.action == "detail") {
                //跨页面传递的数据对象，克隆后才可以安全使用
                data = mini.clone(data);
                mini.get("p1").setValue(data.p1);
                mini.get("p2").setValue(data.p2);
                mini.get("p3").setValue(data.p3);
                mini.get("p4").setValue(data.p4);
                if(data.action == "detail"){
                	mini.get("save_btn").hide();
                	form.setEnabled(false);
                }
            }
        }
        
        function GetData() {
            var o = form.getData();
            return o;
        }
        
        function CloseWindow(action) {
            if (window.CloseOwnerWindow)
                return window.CloseOwnerWindow(action);
            else
                window.close();
        }
        function onOk(e) {
        	CloseWindow("ok");
        }
        function onCancel(e) {
            CloseWindow("cancel");
        }



    </script>
</body>
</html>