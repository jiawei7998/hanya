<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title></title>
  </head>

<body style="width:100%;height:100%;background:white">
    <div>
        <div id="field" class="fieldset-body">
            <table id="field_form"  style="text-align:left;margin:auto;width:90%;" class="mini-sltable">
                <tr>
                    <td colspan="2" style="text-align:center;">
                    	<span id="labell" style="font-size: medium;">Sender to receiver</span>
                    </td>
                </tr>
                <tr>
                    <td>
                    	<input id="c1" name="c1" class="mini-textbox"  style="width:100%" />
                    </td>
                </tr>
                <tr>
                    <td>
                    	<input id="c2" name="c2" class="mini-textbox"  style="width:100%" />
                    </td>
                </tr>
                <tr>
                    <td>
                    	<input id="c3" name="c3" class="mini-textbox"  style="width:100%" />
                    </td>
                </tr>
                <tr>
                    <td>
                    	<input id="c4" name="c4" class="mini-textbox"  style="width:100%" />
                    </td>
                </tr>
                <tr>
                    <td>
                    	<input id="c5" name="c5" class="mini-textbox"  style="width:100%" />
                    </td>
                </tr>
                <tr>
                    <td>
                    	<input id="c6" name="c6" class="mini-textbox"  style="width:100%" />
                    </td>
                </tr>
                
                <tr>
                    <td colspan="2" style="text-align:center;">
                        <a  id="save_btn" class="mini-button" style="display: none"    onclick="onOk">保存</a>
                        <a id="cancel_btn" class="mini-button" style="display: none"    onclick="onCancel">取消</a>
                    </td>
                </tr>
            </table>
            
        </div>
    </div>
    <script type="text/javascript">
        mini.parse();
        var form = new mini.Form("field_form");
        function SetData(data) {
        	//跨页面传递的数据对象，克隆后才可以安全使用
            data = mini.clone(data);
        	mini.get("c1").setValue(data.c1);
            mini.get("c2").setValue(data.c2);
            mini.get("c3").setValue(data.c3);
            mini.get("c4").setValue(data.c4);
            mini.get("c5").setValue(data.c5);
            mini.get("c6").setValue(data.c6);
            if(data.action == "detail"){
            	mini.get("save_btn").hide();
            	form.setEnabled(false);
            }
        }
        
        function GetData() {
            var o = form.getData();
            return o;
        }
        
        function CloseWindow(action) {
            if (window.CloseOwnerWindow)
                return window.CloseOwnerWindow(action);
            else
                window.close();
        }
        function onOk(e) {
        	CloseWindow("ok");
        }
        function onCancel(e) {
            CloseWindow("cancel");
        }



    </script>
</body>
</html>