<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title></title>
  </head>

<body style="width:100%;height:100%;background:white">
    <div>
        <div id="field" class="fieldset-body">
            <table id="field_form"  style="text-align:left;margin:auto;width:90%;" class="mini-sltable">
                <tr>
                    <td colspan="2" style="text-align:center;">
                    	<span id="labell" style="font-size: medium;">Account for charges</span>
                    </td>
                </tr>
                <tr>
                    <td>
                    	<input id="ac1" name="ac1" class="mini-textbox"  style="width:100%" />
                    </td>
                </tr>
                <tr>
                    <td>
                    	<input id="ac2" name="ac2" class="mini-textbox"  style="width:100%" />
                    </td>
                </tr>
                <tr>
                    <td>
                    	<input id="ac3" name="ac3" class="mini-textbox"  style="width:100%" />
                    </td>
                </tr>
                <tr>
                    <td>
                    	<input id="ac4" name="ac4" class="mini-textbox"  style="width:100%" />
                    </td>
                </tr>
                <tr>
                    <td>
                    	<input id="ac5" name="ac5" class="mini-textbox"  style="width:100%" />
                    </td>
                </tr>
                <tr>
                    <td>
                    	<input id="ac6" name="ac6" class="mini-textbox"  style="width:100%" />
                    </td>
                </tr>
                
                <tr>
                    <td colspan="2" style="text-align:center;">
                        <a  id="save_btn" class="mini-button" style="display: none"    onclick="onOk">保存</a>
                        <a id="cancel_btn" class="mini-button" style="display: none"    onclick="onCancel">取消</a>
                    </td>
                </tr>
            </table>
            
        </div>
    </div>
    <script type="text/javascript">
        mini.parse();
        var form = new mini.Form("field_form");
        function SetData(data) {
        	//跨页面传递的数据对象，克隆后才可以安全使用
            data = mini.clone(data);
            mini.get("ac1").setValue(data.ac1);
            mini.get("ac2").setValue(data.ac2);
            mini.get("ac3").setValue(data.ac3);
            mini.get("ac4").setValue(data.ac4);
            mini.get("ac5").setValue(data.ac5);
            mini.get("ac6").setValue(data.ac6);
            if(data.action == "detail"){
            	mini.get("save_btn").hide();
            	form.setEnabled(false);
            }
        }
        
        function GetData() {
            var o = form.getData();
            return o;
        }
        
        function CloseWindow(action) {
            if (window.CloseOwnerWindow)
                return window.CloseOwnerWindow(action);
            else
                window.close();
        }
        function onOk(e) {
        	CloseWindow("ok");
        }
        function onCancel(e) {
            CloseWindow("cancel");
        }



    </script>
</body>
</html>