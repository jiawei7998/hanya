<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title></title>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>

<body style="width: 100%; height: 100%; background: white">
<fieldset class="mini-fieldset">
	<legend>查询条件</legend>
	<div id="search_form" style="width: 100%;">
		<input id="sysDate" name="sysDate" class="mini-datepicker" labelField="true" label="成交日期："
        	labelStyle="text-align:right;" emptyText="请选择日期" />
       	<input id="payDate" name="payDate" class="mini-datepicker mini-mustFill" required="true"  labelField="true" label="交割日期："
       	labelStyle="text-align:right;" emptyText="请选择日期" value="<%=__bizDate%>"/>
        <input id="businessType" name="businessType" class="mini-combobox" labelField="true" width="280px" 
        	label="业务类型：" labelStyle="text-align:right;"  labelStyle="width:120px" emptyText="请选择业务类型"
        	data="CommonUtil.serverData.dictionary.businessType1"/> 
        <input id="dealno" name="dealno" class="mini-textbox" labelField="true" width="280px" 
        	label="OPICS编号：" labelStyle="text-align:right;"  labelStyle="width:120px" emptyText="请输入OPICS编号" /> 
			
       	<span style="float: right; margin-right: 50px"> 
			<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
			<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
			<a id="export" class="mini-button" style="display: none"   onclick="exportExcel">导出报表</a>
		</span>
	</div>
</fieldset>
<div style="width: 100%;">
<!-- 		USD,EUR,JPY,GBP,HKD,CNY -->
<fieldset class="mini-fieldset">
	<legend>币种金额合计</legend>
	<div id="search_form1" style="width: 100%;">
		<b>&nbsp;&nbsp;收款金额&nbsp;&nbsp;</b>
		<input id="UsdSumAmt" name="buySumAmt" class="mini-textbox" labelField="true" label="美元：" width="180px"
        	labelStyle="text-align:right; width:40px" value = "0"/>
        <input id="EurSumAmt" name="sellSumAmt" class="mini-textbox" labelField="true" width="180px"
        	label="欧元：" labelStyle="text-align:right; width:40px" value = "0"/> 
       	<input id="JpySumAmt" name="sellSumAmt" class="mini-textbox" labelField="true" width="200px"
       	label="日元：" labelStyle="text-align:right; width:40px" value = "0"/> 
       	<input id="GbpSumAmt" name="sellSumAmt" class="mini-textbox" labelField="true" width="180px"
       	label="英镑：" labelStyle="text-align:right; width:40px" value = "0"/> 
       	<input id="HkdSumAmt" name="sellSumAmt" class="mini-textbox" labelField="true" width="180px"
       	label="港元：" labelStyle="text-align:right; width:40px" value = "0"/> 
       	<input id="CnySumAmt" name="sellSumAmt" class="mini-textbox" labelField="true" width="200px"
       	label="人民币：" labelStyle="text-align:right; width:50px" value = "0"/> 
	</div>
	<div id="search_form2" style="width: 100%;">
		<b>&nbsp;&nbsp;付款金额&nbsp;&nbsp;</b>
		<input id="UsdSumAmtP" name="buySumAmtP" class="mini-textbox" labelField="true" label="美元：" width="180px"
        	labelStyle="text-align:right; width:40px" value = "0"/>
        <input id="EurSumAmtP" name="sellSumAmtP" class="mini-textbox" labelField="true" width="180px"
        	label="欧元：" labelStyle="text-align:right; width:40px" value = "0"/> 
       	<input id="JpySumAmtP" name="sellSumAmtP" class="mini-textbox" labelField="true" width="200px"
       	label="日元：" labelStyle="text-align:right; width:40px" value = "0"/> 
       	<input id="GbpSumAmtP" name="sellSumAmtP" class="mini-textbox" labelField="true" width="180px"
       	label="英镑：" labelStyle="text-align:right; width:40px" value = "0"/> 
       	<input id="HkdSumAmtP" name="sellSumAmtP" class="mini-textbox" labelField="true" width="180px"
       	label="港元：" labelStyle="text-align:right; width:40px" value = "0"/> 
       	<input id="CnySumAmtP" name="sellSumAmtP" class="mini-textbox" labelField="true" width="200px"
       	label="人民币：" labelStyle="text-align:right; width:50px" value = "0"/> 
	</div>
	<div id="search_form2" style="width: 100%;">
		<b>收付款差值</b>
		<input id="UsdSumAmtD" name="buySumAmtD" class="mini-textbox" labelField="true" label="美元：" width="180px"
        	labelStyle="text-align:right; width:40px" />
        <input id="EurSumAmtD" name="sellSumAmtD" class="mini-textbox" labelField="true" width="180px"
        	label="欧元：" labelStyle="text-align:right; width:40px" /> 
       	<input id="JpySumAmtD" name="sellSumAmtD" class="mini-textbox" labelField="true" width="200px"
       	label="日元：" labelStyle="text-align:right; width:40px" /> 
       	<input id="GbpSumAmtD" name="sellSumAmtD" class="mini-textbox" labelField="true" width="180px"
       	label="英镑：" labelStyle="text-align:right; width:40px" /> 
       	<input id="HkdSumAmtD" name="sellSumAmtD" class="mini-textbox" labelField="true" width="180px"
       	label="港元：" labelStyle="text-align:right; width:40px" /> 
       	<input id="CnySumAmtD" name="sellSumAmtD" class="mini-textbox" labelField="true" width="200px"
       	label="人民币：" labelStyle="text-align:right; width:50px" /> 
	</div>
</fieldset>
		
	</div>
	
<div id="MiniSettleForeignSpot" class="mini-fit" style="margin-top: 2px;">
	<div id="datagrid" class="mini-datagrid borderAll" style="width: 100%; height: 100%;" allowAlternating="true"
		allowResize="true" border="true" sortMode="client" multiSelect="true">
		<div property="columns">
			<div type="indexcolumn" headerAlign="center" width="40">序号</div>
			<div field="dealno" width="100" allowSort="false" headerAlign="center" align="center">OPICS编号</div>
			<div field="contractId" width="100" allowSort="false" headerAlign="center" align="center">成交单号</div>
			<div field="businessType" width="100" allowSort="false" headerAlign="center" align="center">业务类型</div>
			<div field="counterpartyDealer" width="100" align="center" headerAlign="center" >交易对手</div>
			<div field="dueBankAccount1" width="160" allowSort="true" headerAlign="center" align="center">收款账号</div>
			<div field="buyCurreny" width="80" allowSort="false" headerAlign="center" align="center">收款币种</div>
			<div field="buyAmt" width="80" allowSort="false" headerAlign="center" align="center">收款金额</div>
			<div field="dueBankAccount2" width="160" allowSort="false" headerAlign="center" align="center">付款账号</div>
			<div field="sellCurreny" width="80" allowSort="false" headerAlign="center" align="center">付款币种</div>
			<div field="sellAmt" width="80" allowSort="true" headerAlign="center" align="center" >付款金额</div>
			<div field="nettingStatus" width="60" allowSort="false" headerAlign="center" align="center">清算方式</div>
		</div>
	</div>
</div>

<script>
	mini.parse();
	var url = window.location.search;
	var grid = mini.get("datagrid");
	var userId='<%=__sessionUser.getUserId()%>';
	
	grid.on("beforeload", function(e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex;
		var pageSize = e.data.pageSize;
		search(pageSize, pageIndex);
	});
	
	// 查询
	function search(pageSize,pageIndex){
		var form = new mini.Form("#search_form");
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统也提示");
			return;
		}
		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:"/IfsNettingController/queryNettingList",
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
		//计算合计
		CommonUtil.ajax({
			url:"/IfsNettingController/queryNettingSum",
			data:params,
			callback : function(data) {
				//收款金额
				mini.get("UsdSumAmt").setValue("0");
				mini.get("EurSumAmt").setValue("0");
				mini.get("JpySumAmt").setValue("0");
				mini.get("GbpSumAmt").setValue("0");
				mini.get("HkdSumAmt").setValue("0");
				mini.get("CnySumAmt").setValue("0");
				if(data!=null){
					for(var i=0;i<data.length;i++){
						//收款金额
						if(data[i].buyCurreny=="USD"){
							mini.get("UsdSumAmt").setValue(data[i].sumAmt);
						}else if(data[i].buyCurreny=="EUR"){
							mini.get("EurSumAmt").setValue(data[i].sumAmt);
						}else if(data[i].buyCurreny=="JPY"){
							mini.get("JpySumAmt").setValue(data[i].sumAmt);
						}else if(data[i].buyCurreny=="GBP"){
							mini.get("GbpSumAmt").setValue(data[i].sumAmt);
						}else if(data[i].buyCurreny=="HKD"){
							mini.get("HkdSumAmt").setValue(data[i].sumAmt);
						}else if(data[i].buyCurreny=="CNY"){
							mini.get("CnySumAmt").setValue(data[i].sumAmt);
						}
					}
					mini.get("UsdSumAmtD").setValue(Number(mini.get("UsdSumAmt").getValue())-Number(mini.get("UsdSumAmtP").getValue()));
					mini.get("EurSumAmtD").setValue(Number(mini.get("EurSumAmt").getValue())-Number(mini.get("EurSumAmtP").getValue()));
					mini.get("JpySumAmtD").setValue(Number(mini.get("JpySumAmt").getValue())-Number(mini.get("JpySumAmtP").getValue()));
					mini.get("GbpSumAmtD").setValue(Number(mini.get("GbpSumAmt").getValue())-Number(mini.get("GbpSumAmtP").getValue()));
					mini.get("HkdSumAmtD").setValue(Number(mini.get("HkdSumAmt").getValue())-Number(mini.get("HkdSumAmtP").getValue()));
					mini.get("CnySumAmtD").setValue(Number(mini.get("CnySumAmt").getValue())-Number(mini.get("CnySumAmtP").getValue()));
				}
			}
		});
		
		//付款金额计算合计
		CommonUtil.ajax({
			url:"/IfsNettingController/queryNettingSumPay",
			data:params,
			callback : function(data) {
				//付款金额
				mini.get("UsdSumAmtP").setValue("0");
				mini.get("EurSumAmtP").setValue("0");
				mini.get("JpySumAmtP").setValue("0");
				mini.get("GbpSumAmtP").setValue("0");
				mini.get("HkdSumAmtP").setValue("0");
				mini.get("CnySumAmtP").setValue("0");
				if(data!=null){
					for(var i=0;i<data.length;i++){
						//付款金额
						if(data[i].sellCurreny=="USD"){
							mini.get("UsdSumAmtP").setValue(data[i].sumAmtP);
						}else if(data[i].sellCurreny=="EUR"){
							mini.get("EurSumAmtP").setValue(data[i].sumAmtP);
						}else if(data[i].sellCurreny=="JPY"){
							mini.get("JpySumAmtP").setValue(data[i].sumAmtP);
						}else if(data[i].sellCurreny=="GBP"){
							mini.get("GbpSumAmtP").setValue(data[i].sumAmtP);
						}else if(data[i].sellCurreny=="HKD"){
							mini.get("HkdSumAmtP").setValue(data[i].sumAmtP);
						}else if(data[i].sellCurreny=="CNY"){
							mini.get("CnySumAmtP").setValue(data[i].sumAmtP);
						}
					}
					mini.get("UsdSumAmtD").setValue(Number(mini.get("UsdSumAmt").getValue())-Number(mini.get("UsdSumAmtP").getValue()));
					mini.get("EurSumAmtD").setValue(Number(mini.get("EurSumAmt").getValue())-Number(mini.get("EurSumAmtP").getValue()));
					mini.get("JpySumAmtD").setValue(Number(mini.get("JpySumAmt").getValue())-Number(mini.get("JpySumAmtP").getValue()));
					mini.get("GbpSumAmtD").setValue(Number(mini.get("GbpSumAmt").getValue())-Number(mini.get("GbpSumAmtP").getValue()));
					mini.get("HkdSumAmtD").setValue(Number(mini.get("HkdSumAmt").getValue())-Number(mini.get("HkdSumAmtP").getValue()));
					mini.get("CnySumAmtD").setValue(Number(mini.get("CnySumAmt").getValue())-Number(mini.get("CnySumAmtP").getValue()));
				}
			}
		});
	}
		
	function query() {
    	search(grid.pageSize, 0);
    }
	
	function clear(){
        var form=new mini.Form("search_form");
        form.clear();
        search(10,0);
	}
	
	$(document).ready(function() {
		search(10, 0);
	});
	
	// 导出报表
   	function exportExcel(){
   		var content = grid.getData();
   		if(content.length == 0){
   			mini.alert("请先查询数据");
   			return;
   		}
   		mini.confirm("您确认要导出Excel吗?","系统提示", 
   			function (action) {
   				if (action == "ok"){
   					var form = new mini.Form("#search_form");
   					var data=form.getData(true);
   					data['UsdSumAmt']=mini.get("UsdSumAmt").getValue();
   					data['EurSumAmt']=mini.get("EurSumAmt").getValue();
   					data['JpySumAmt']=mini.get("JpySumAmt").getValue();
   					data['GbpSumAmt']=mini.get("GbpSumAmt").getValue();
   					data['HkdSumAmt']=mini.get("HkdSumAmt").getValue();
   					data['CnySumAmt']=mini.get("CnySumAmt").getValue();
   					
   					data['UsdSumAmtP']=mini.get("UsdSumAmtP").getValue();
   					data['EurSumAmtP']=mini.get("EurSumAmtP").getValue();
   					data['JpySumAmtP']=mini.get("JpySumAmtP").getValue();
   					data['GbpSumAmtP']=mini.get("GbpSumAmtP").getValue();
   					data['HkdSumAmtP']=mini.get("HkdSumAmtP").getValue();
   					data['CnySumAmtP']=mini.get("CnySumAmtP").getValue();
   					
   					data['UsdSumAmtD']=mini.get("UsdSumAmtD").getValue();
   					data['EurSumAmtD']=mini.get("EurSumAmtD").getValue();
   					data['JpySumAmtD']=mini.get("JpySumAmtD").getValue();
   					data['GbpSumAmtD']=mini.get("GbpSumAmtD").getValue();
   					data['HkdSumAmtD']=mini.get("HkdSumAmtD").getValue();
   					data['CnySumAmtD']=mini.get("CnySumAmtD").getValue();
   					
   					var fields = null;
   					for(var id in data){
   						fields += '<input type="hidden" id="' + id + '" name="'+ id +'" value="' + data[id] + '">';
   					}
   					var urls = CommonUtil.pPath + "/sl/IfsNettingController/exportExcel";                                                                                                                         
   					$('<form action="'+ urls +'" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();   
   				}
   			}
   		);
   	}
</script>
</body>
</html>