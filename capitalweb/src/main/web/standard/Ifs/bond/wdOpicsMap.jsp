<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
    <head>
        <title>债券信息映射</title>
        <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
        <script type="text/javascript" src="<%=basePath%>/miniScript/hideBtn.js"></script>
    </head>
<body style="width:100%;height:100%;background:white">
    <fieldset class="mini-fieldset title">
    <legend>债券信息映射查询</legend>
    <div id="search_form" style="width: 100%;">
    	<input id="mapname" name="mapname" class="mini-combobox"  data="CommonUtil.serverData.dictionary.SecMapType" width="280px" emptyText="请选择映射类型" labelField="true"  label="映射类型：" labelStyle="text-align:right;"/>
        <span style="float:right;margin-right: 150px">
            <a class="mini-button" style="display: none"  id="search_btn"  onclick="search()">查询</a>
            <a class="mini-button" style="display: none"  id="clear_btn"  onclick="clear()">清空</a>
        </span>
    </div>
    </fieldset>
    <span style="margin:2px;display: block;">
        <a class="mini-button" style="display: none"  id="add_btn"  onClick="add();">新增</a>
        <a class="mini-button" style="display: none"  id="edit_btn"  onClick="modify();">修改</a>
        <a class="mini-button" style="display: none"  id="delete_btn"  onClick="del();">删除</a>
    </span>
    <div class="mini-fit" style="width:100%;height:100%;">
        <div id="tradeParam_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" 
            sortMode="client" allowAlternating="true"   idField="userId" onrowdblclick="onRowDblClick" 
            border="true" allowResize="true" >
            <div property="columns">
                <div type="indexcolumn" headerAlign="center" align="center" width="40px">序列</div>
                <div field="mapid" headerAlign="center"  align="center" width="120px" >映射ID</div>
                <div field="mapname" headerAlign="center"  width="120px" renderer="CommonUtil.dictRenderer" data-options="{dict:'SecMapType'}">映射类型</div>
                <div field="wdid" headerAlign="center" width="120px">参数值</div>
                <div field="wdname" headerAlign="center" width="120px" >参数名称</div>
                <div field="opicsid" headerAlign="center" width="120px" >对应opics参数值</div>
                <div field="opicsname" headerAlign="center"  width="120px">对应opics参数名称</div>
                <div field="remark1" headerAlign="center"  width="160px">备注1</div>
                <div field="remark2" headerAlign="center" width="160px">备注2</div>
            </div>
        </div>
    </div>
</body>
<script>
    mini.parse();

    var grid = mini.get("tradeParam_grid");
    grid.on("beforeload", function(e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        searchs(pageSize, pageIndex);
    });

    $(document).ready(function() {
		search();
	});

    //查询按钮
    function search(){
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
            searchs(grid.pageSize, 0);
        });
    }

    function searchs(pageSize, pageIndex) {
        var form = new mini.Form("#search_form");
        form.validate();
        if (form.isValid() == false) {
            mini.alert("表单填写错误,请确认!", "提示信息");
            return;
        }

        var data = form.getData();
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        data['branchId']=branchId;

        var params = mini.encode(data);

        CommonUtil.ajax({
            url : "/IfsWdSecController/searchPageWdOpicsSecmap",
            data : params,
            callback : function(data) {
                var grid = mini.get("tradeParam_grid");
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }

    //清空
    function clear(){
        var form=new mini.Form("search_form");
        form.clear();
        search();
    }

    //新增
    function add(){
        var url = CommonUtil.baseWebPath() + "/bond/wdOpicsMapEdit.jsp?action=add";
        var tab = { id: "wdOpicsMapAdd", name: "tradeParamAdd", title: "债券信息映射新增", 
        url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
        var paramData = {selectData:""};
        CommonUtil.openNewMenuTab(tab,paramData);
    }
    
    //修改
    function modify(){
        var row = grid.getSelected();
        if(row){
            var url = CommonUtil.baseWebPath() + "/bond/wdOpicsMapEdit.jsp?action=edit";
            var tab = { id: "wdOpicsMapEdit", name: "tradeParamEdit", title: "债券信息映射修改", 
            url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
            var paramData = {selectData:row};
            CommonUtil.openNewMenuTab(tab,paramData);

        } else {
            mini.alert("请选中一条记录！","消息提示");
        }
    }
    
    //删除
    function del() {
        var row = grid.getSelected();
        if (row) {
            mini.confirm("您确认要删除选中记录?","系统警告",function(value){
                if(value=="ok"){
                    CommonUtil.ajax({
                        url: "/IfsWdSecController/deleteWdOpicsMap",
                        data: {mapid: row.mapid},
                        callback: function (data) {
                            if (data.code == 'error.common.0000') {
                                mini.alert("删除成功!");
                                grid.reload();
                            } else {
                                mini.alert("删除失败!");
                            }
                        }
                    });
                }
            });
        }else {
            mini.alert("请选中一条记录！", "消息提示");
        }

    }


    //双击详情
    function onRowDblClick(e) {
        var row = grid.getSelected();
        if(row){
                var url = CommonUtil.baseWebPath() + "/bond/wdOpicsMapEdit.jsp?action=detail";
                var tab = { id: "wdOpicsMapDetail", name: "wdOpicsMapDetail", title: "债券信息映射详情", url: url ,showCloseButton:true};
                var paramData = {selectData:row};
                CommonUtil.openNewMenuTab(tab,paramData);
        } else {
            mini.alert("请选中一条记录！","消息提示");
        }
    }
</script>
</html>