<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
    <head>
        <title>opics交易要素管理</title>
        <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
        <script type="text/javascript" src="<%=basePath%>/miniScript/hideBtn.js"></script>
    </head>
<body style="width:100%;height:100%;background:white">
    <fieldset class="mini-fieldset title">
    <legend>交易要素信息查询</legend>
    <div id="search_form" style="width: 100%;">
        <input id="element" name="element" class="mini-textbox" labelField="true" label="要素组合：" labelStyle="text-align:right;" emptyText="请输入要素组合" />
        <input id="product" name="product" class="mini-textbox" labelField="true" label="产品代码：" labelStyle="text-align:right;" emptyText="请输入产品代码" />
        <span style="float:right;margin-right: 150px">
            <a class="mini-button" style="display: none"  id="search_btn"  onclick="search()">查询</a>
            <a class="mini-button" style="display: none"  id="clear_btn"  onclick="clear()">清空</a>
        </span>
    </div>
    </fieldset>
    <span style="margin:2px;display: block;">
        <a class="mini-button" style="display: none"  id="add_btn"  onClick="add();">新增</a>
        <a class="mini-button" style="display: none"  id="edit_btn"  onClick="modify();">修改</a>
        <a class="mini-button" style="display: none"  id="delete_btn"  onClick="del();">删除</a>
    </span>
    <div class="mini-fit" style="width:100%;height:100%;">
        <div id="tradeParam_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" 
            sortMode="client" allowAlternating="true"   idField="userId" onrowdblclick="onRowDblClick" 
            border="true" allowResize="true" >
            <div property="columns">
                <div type="indexcolumn" headerAlign="center" align="center" width="40px">序列</div>
                <div field="groupId" headerAlign="center"  align="center" width="180px" >分组ID</div>
                <div field="element" headerAlign="center"  width="120px">要素组合</div>
                <div field="product" headerAlign="center" width="120px">产品代码</div>
                <div field="prodType" headerAlign="center" width="120px" >产品类型</div>
                <div field="intcalcrule" headerAlign="center" width="150px" renderer="CommonUtil.dictRenderer" data-options="{dict:'IntcalRule'}">计息规则</div>
                <div field="trader" headerAlign="center"  width="120px">交易员</div>
                <div field="note" headerAlign="center"  width="200px">备注</div>
                <div field="inputTime" headerAlign="center" align="center" width="160px">录入时间</div>
                <div field="lastTime" headerAlign="center"  align="center" width="160px">最后修改时间</div>
            </div>
        </div>
    </div>
</body>
<script>
    mini.parse();

    var grid = mini.get("tradeParam_grid");
    grid.on("beforeload", function(e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        searchs(pageSize, pageIndex);
    });

    $(document).ready(function() {
		search();
	});

    //查询按钮
    function search(){
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
            searchs(grid.pageSize, 0);
        });
    }

    function searchs(pageSize, pageIndex) {
        var form = new mini.Form("#search_form");
        form.validate();
        if (form.isValid() == false) {
            mini.alert("表单填写错误,请确认!", "提示信息");
            return;
        }

        var data = form.getData();
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        data['branchId']=branchId;

        var params = mini.encode(data);

        CommonUtil.ajax({
            url : "/IfsTradeElementParamController/searchPageTradeParam",
            data : params,
            callback : function(data) {
                var grid = mini.get("tradeParam_grid");
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }

    //清空
    function clear(){
        var form=new mini.Form("search_form");
        form.clear();
        search();
    }

    //新增
    function add(){
        var url = CommonUtil.baseWebPath() + "/bond/tradeElementParamEdit.jsp?action=add";
        var tab = { id: "tradeParamAdd", name: "tradeParamAdd", title: "交易要素新增", 
        url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
        var paramData = {selectData:""};
        CommonUtil.openNewMenuTab(tab,paramData);
    }
    
    //修改
    function modify(){
        var row = grid.getSelected();
        if(row){
            var url = CommonUtil.baseWebPath() + "/bond/tradeElementParamEdit.jsp?action=edit";
            var tab = { id: "tradeParamEdit", name: "tradeParamEdit", title: "交易要素修改", 
            url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
            var paramData = {selectData:row};
            CommonUtil.openNewMenuTab(tab,paramData);

        } else {
            mini.alert("请选中一条记录！","消息提示");
        }
    }
    
    //删除
    function del() {
        var row = grid.getSelected();
        if (row) {
            mini.confirm("您确认要删除选中记录?","系统警告",function(value){
                if(value=="ok"){
                    CommonUtil.ajax({
                        url: "/IfsTradeElementParamController/deleteTradeParam",
                        data: {groupId: row.groupId},
                        callback: function (data) {
                            if (data.code == 'error.common.0000') {
                                mini.alert("删除成功!");
                                grid.reload();
                            } else {
                                mini.alert("删除失败!");
                            }
                        }
                    });
                }
            });
        }else {
            mini.alert("请选中一条记录！", "消息提示");
        }

    }


    //双击详情
    function onRowDblClick(e) {
        var row = grid.getSelected();
        if(row){
                var url = CommonUtil.baseWebPath() + "/bond/tradeElementParamEdit.jsp?action=detail";
                var tab = { id: "tradeParamDetail", name: "tradeParamDetail", title: "交易要素详情", url: url ,showCloseButton:true};
                var paramData = {selectData:row};
                CommonUtil.openNewMenuTab(tab,paramData);
        } else {
            mini.alert("请选中一条记录！","消息提示");
        }
    }
</script>
</html>