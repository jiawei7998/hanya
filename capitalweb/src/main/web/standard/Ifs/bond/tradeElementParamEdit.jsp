<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>交易要素  维护</title>
  </head>

<body style="width:100%;height:100%;background:white">
    <div>
        <span id="labell"><a href="javascript:CommonUtil.activeTab();">交易要素</a> >> 修改</span>
        <div id="field" class="fieldset-body">
            <table id="field_form"  style="text-align:left;margin:auto;width:100%;" class="mini-sltable">
                <tr>
                    <td style="width:50%">
                        <input id="element" name="element" class="mini-textbox" emptyText="请输入要素组合名称"
                        labelField="true" required="true"  label="交易要素组合" style="width:100%" labelStyle="text-align:left;width:170px"
                        vtype="maxLength:4"/>
                    </td>
                    <td style="width:50%">
                        <input id="product" name="product" class="mini-textbox"  
                        labelField="true"  label="产品代码" emptyText="请输入产品代码"
                        required="true"  vtype="maxLength:10" style="width:100%"
                        labelStyle="text-align:left;width:170px"onvalidation="onEnglishAndNumberValidation"/>
                    </td>
                </tr>
                <tr>
                    <td style="width:50%">
                        <input id="prodType" name="prodType" class="mini-textbox" emptyText="请输入产品类型"
                        labelField="true" required="true"  label="产品类型" style="width:100%" labelStyle="text-align:left;width:170px"
                        vtype="maxLength:2" onvalidation="onEnglishAndNumberValidation"/>
                    </td>
                    <td style="width:50%">
                        <input id="trader" name="trader" class="mini-textbox"  
                        labelField="true"  label="交易员" emptyText="请输入交易员"
                        required="true"  vtype="maxLength:20" style="width:100%"
                        labelStyle="text-align:left;width:170px"onvalidation="onEnglishAndNumberValidation"/>
                    </td>
                </tr>
                <tr>
               		 <td style="width:50%">
                    	<input id="intcalcrule" name="intcalcrule" class="mini-combobox" required="true"  style="width:100%" labelField="true" label="计息规则："  labelStyle="text-align:left;width:170px;"   data = "CommonUtil.serverData.dictionary.IntcalRule"  />
                    	<input id="groupId" name="groupId" class="mini-hidden"/>
                    </td>
                    <td style="width:50%">
                        <input id="note" name="note" class="mini-textarea" emptyText="请输入备注"
                        labelField="true"  label="备注" style="width:100%" labelStyle="text-align:left;width:170px"
                        vtype="maxLength:50"/>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align:center;">
                        <a  id="save_btn" class="mini-button" style="display: none"    onclick="save">保存</a>
                        <a id="cancel_btn" class="mini-button" style="display: none"    onclick="cancel">取消</a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <script type="text/javascript">
        mini.parse();
        //获取当前tab
        var currTab = top["win"].tabs.getActiveTab();
        var params = currTab.params;
        var row=params.selectData;
        var url=window.location.search;
        var action=CommonUtil.getParam(url,"action");
        var form=new mini.Form("#field_form");

        $(document).ready(function(){
            initData();
        }) 

        //初始化数据
        function initData(){
            if($.inArray(action,["edit","detail"])>-1){//修改、详情
                form.setData(row);
                if(action == "detail"){
                    $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>交易要素</a> >> 详情");
                }
            }else{//增加 
                $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>交易要素</a> >> 新增");
            }
        }
        //保存
        function save(){
            //表单验证！！！
            form.validate();
            if (form.isValid() == false) {
                return;
            }

            var data=form.getData();
            var params=mini.encode(data);
            CommonUtil.ajax({
                url:"/IfsTradeElementParamController/saveTradeParam",
                data:params,
                callback:function(data){
                    mini.alert("保存成功",'提示信息',function(){
                        top["win"].closeMenuTab();
                    })
                }
		    });
        }

        function cancel(){
            top["win"].closeMenuTab();
        }

        //英文、数字、下划线 的验证
        function onEnglishAndNumberValidation(e) {
            if(e.value == "" || e.value == null){//值为空，就不做校验
                return;
            }
            if (e.isValid) {
                if (isEnglishAndNumber(e.value) == false) {
                    e.errorText = "必须输入英文或数字";
                    e.isValid = false;
                }
            }
        }

        /* 是否英文、数字、下划线 */
        function isEnglishAndNumber(v) {
            var re = new RegExp("^[0-9a-zA-Z\_\-]+$");
            if (re.test(v)) return true;
            return false;
        }

    </script>
</body>
</html>