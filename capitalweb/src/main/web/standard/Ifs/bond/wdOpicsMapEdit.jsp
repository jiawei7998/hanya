<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>债券信息映射  维护</title>
  </head>

<body style="width:100%;height:100%;background:white">
    <div>
        <span id="labell"><a href="javascript:CommonUtil.activeTab();">债券信息映射</a> >> 修改</span>
        <div id="field" class="fieldset-body">
            <table id="field_form"  style="text-align:left;margin:auto;width:100%;" class="mini-sltable">
                <tr>
                    <td style="width:50%">
                        <input id="mapid" name="mapid" class="mini-textbox" emptyText="请输入映射ID"
                        labelField="true" required="true"  label="映射ID" style="width:100%" labelStyle="text-align:left;width:170px"
                        vtype="maxLength:20" emptyText="由系统自动生成" enabled="false"/>
                    </td>
                    <td style="width:50%">
                        <input id="mapname" name="mapname" class="mini-combobox"  
                        labelField="true"  label="映射类型" emptyText="请选择映射类型"
                        required="true"  vtype="maxLength:30" style="width:100%"
                        labelStyle="text-align:left;width:170px"  data = "CommonUtil.serverData.dictionary.SecMapType" onValueChanged="onMapChange"/>
                    </td>
                </tr>
                <tr>
                    <td style="width:50%">
                        <input id="wdid" name="wdid"  class="mini-buttonedit" emptyText="请输入参数值"
                        labelField="true" required="true"  label="参数值" style="width:100%" labelStyle="text-align:left;width:170px"
                        vtype="maxLength:20" onvalidation="onEnglishAndNumberValidation" onbuttonclick="onIssuerEdit"/>
                    </td>
                    <td style="width:50%">
                        <input id="wdname" name="wdname" class="mini-textbox"  
                        labelField="true"  label="参数名称" emptyText="请输入参数名称"
                        required="true"  vtype="maxLength:50" style="width:100%"
                        labelStyle="text-align:left;width:170px" />
                    </td>
                </tr>
                <tr>
                    <td style="width:50%">
                        <input id="opicsid" name="opicsid" class="mini-buttonedit" emptyText="请输入对应opics参数值"
                        labelField="true" required="true"  label="对应opics参数值" style="width:100%" labelStyle="text-align:left;width:170px"
                        vtype="maxLength:20" onvalidation="onEnglishAndNumberValidation" onbuttonclick="onButtonEdit"/>
                    </td>
                    <td style="width:50%">
                        <input id="opicsname" name="opicsname" class="mini-textbox"  
                        labelField="true"  label="对应opics参数名称" emptyText="请输入对应opics参数名称"
                        required="true"  vtype="maxLength:50" style="width:100%"
                        labelStyle="text-align:left;width:170px"/>
                    </td>
                </tr>
                <tr>
                    <td style="width:50%">
                        <input id="remark1" name="remark1" class="mini-textbox" emptyText="请输入备注1"
                        labelField="true"  label="备注1" style="width:100%" labelStyle="text-align:left;width:170px"
                        vtype="maxLength:50"/>
                    </td>
                     <td style="width:50%">
                        <input id="remark2" name="remark2" class="mini-textbox" emptyText="请输入备注2"
                        labelField="true"  label="备注1" style="width:100%" labelStyle="text-align:left;width:170px"
                        vtype="maxLength:50"/>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align:center;">
                        <a  id="save_btn" class="mini-button" style="display: none"    onclick="save">保存</a>
                        <a id="cancel_btn" class="mini-button" style="display: none"    onclick="cancel">取消</a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <script type="text/javascript">
        mini.parse();
        //获取当前tab
        var currTab = top["win"].tabs.getActiveTab();
        var params = currTab.params;
        var row=params.selectData;
        var url=window.location.search;
        var action=CommonUtil.getParam(url,"action");
        var form=new mini.Form("#field_form");
        mini.get("wdid").setShowButton(false);
		mini.get("opicsid").setShowButton(false);

        $(document).ready(function(){
            initData();
        }); 

        //初始化数据
        function initData(){
            if($.inArray(action,["edit","detail"])>-1){//修改、详情
                form.setData(row);
                if(action == "detail"){
                    $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>债券信息映射</a> >> 详情");
                    mini.get("save_btn").setVisible(false);
    				form.setEnabled(false);	
                }
                mini.get("mapid").setEnabled(false);
                var mapname=mini.get("mapname").getValue();
                if(mapname=="ISSUER-CUST"){
                	mini.get("wdid").setShowButton(true);
            		mini.get("opicsid").setShowButton(true);
            		mini.get("wdid").setValue(row.wdid);
    				mini.get("wdid").setText(row.wdid);
    				mini.get("opicsid").setValue(row.opicsid);
    				mini.get("opicsid").setText(row.opicsid);
            		
                }else{
                	mini.get("wdid").setShowButton(false);
                	mini.get("wdid").setValue(row.wdid);
    				mini.get("wdid").setText(row.wdid);
    				mini.get("opicsid").setValue(row.opicsid);
    				mini.get("opicsid").setText(row.opicsid);
                }
                
                
                
            }else{//增加 
                $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>债券信息映射</a> >> 新增");
            }
        }
        //保存
        function save(){
            //表单验证！！！
            form.validate();
            if (form.isValid() == false) {
                return;
            }

            var data=form.getData();
            var params=mini.encode(data);
            CommonUtil.ajax({
                url:"/IfsWdSecController/saveWdOpicsMap",
                data:params,
                callback:function(data){
                    mini.alert("保存成功",'提示信息',function(){
                        top["win"].closeMenuTab();
                    });
                }
		    });
        }

        function cancel(){
            top["win"].closeMenuTab();
        }

        //英文、数字、下划线 的验证
        function onEnglishAndNumberValidation(e) {
            if(e.value == "" || e.value == null){//值为空，就不做校验
                return;
            }
            if (e.isValid) {
                if (isEnglishAndNumber(e.value) == false) {
                    e.errorText = "必须输入英文或数字";
                    e.isValid = false;
                }
            }
        }

        /* 是否英文、数字、下划线 */
        function isEnglishAndNumber(v) {
            var re = new RegExp("^[0-9a-zA-Z\_\-]+$");
            if (re.test(v)) return true;
            return false;
        }
        
        function onMapChange(e){
    		var mapname=e.value;
    		var defalt="ISSUER-CUST";
    		if(mapname==defalt){
    			mini.get("wdid").setShowButton(true);
    			mini.get("opicsid").setShowButton(true);
    		}else{
    			mini.get("wdid").setShowButton(false);
    			mini.get("opicsid").setShowButton(false);
    		};
    	}
        
        
        

        function onButtonEdit(e) {
            var btnEdit = this;
            mini.open({
                url: CommonUtil.baseWebPath() + "../../Common/CustMini.jsp",
                title: "选择交易对手列表",
                width: 900,
                height: 600,
                ondestroy: function (action) {
                    if (action == "ok") {
                        var iframe = this.getIFrameEl();
                        var data = iframe.contentWindow.GetData();
                        data = mini.clone(data);    //必须
                        if (data) {
                            btnEdit.setValue(data.cno);
                            btnEdit.setText(data.cno);
                            mini.get("opicsname").setValue(data.cfn);
                            btnEdit.focus();
                        }
                    }
                }
            });
        }
        
        function onIssuerEdit(e) {
            var btnEdit = this;
            mini.open({
                url: CommonUtil.baseWebPath() + "./bond/seccustMini.jsp",
                title: "选择发行人列表",
                width: 900,
                height: 600,
                ondestroy: function (action) {
                    if (action == "ok") {
                        var iframe = this.getIFrameEl();
                        var data = iframe.contentWindow.GetData();
                        data = mini.clone(data);    //必须
                        if (data) {
                            btnEdit.setValue(data.custid);
                            btnEdit.setText(data.custid);
                            mini.get("wdname").setValue(data.custnm);
                            btnEdit.focus();
                        }
                    }
                }
            });
        }
        
        
        
    </script>
</body>
</html>