<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>
  
<body style="width:100%;height:100%;background:white">
	<div class="mini-fit">
		<fieldset style="width:100%;height:90%;">
			<legend>复核日志详情</legend>
			<div id="log_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" 
				allowAlternating="true" allowResize="true" border="true" multiSelect="false" showPager="false">
				<div property="columns">
					<div type="indexcolumn" headerAlign="center" width="50">序号</div>
					<!-- <div field="dealno" width="150" headerAlign="center" allowSort="false">交易单编号</div>  -->  
					<div field="bndcd" width="150" align="center" headerAlign="center" allowSort="false">债券代码</div>
					<div field="status" width="100" align="center" headerAlign="center" allowSort="false" renderer="CommonUtil.dictRenderer" data-options="{dict:'dealBondStatus'}">交易状态</div>
					<div field="operatInst" width="145" align="center" headerAlign="center" allowSort="false" >操作机构</div> 
					<div field="operatUser" width="145" align="center" headerAlign="center" allowSort="false" >操作人员</div>    
					<div field="inputTime" width="100" align="center" headerAlign="center" allowSort="false" >更新时间</div>
				</div>
			</div>
		</fieldset>
	</div>
</body>
<script type="text/javascript">
	mini.parse();
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var bndcd = params.selectData[0].bndcd;
	var grid = mini.get("log_grid");
	/**
	 *   获取复核日志信息 
	*/
	function loadLogInfo(){
		CommonUtil.ajax({
			url:"/IfsBondCheckLogController/searchPageCheckLog",
			data:{'bndcd':bndcd},
			callback : function(data) {
				if(data != null){
					grid.setTotalCount(data.obj.total);
					grid.setData(data.obj.rows);
				}
			}
		});
	}
	$(document).ready(function(){
		var innerInterval=null;
		innerInitFunction = function(){
			clearInterval(innerInterval);
			loadLogInfo();
		},
		innerInterval = setInterval("innerInitFunction()",100);
	});
</script>
</html>