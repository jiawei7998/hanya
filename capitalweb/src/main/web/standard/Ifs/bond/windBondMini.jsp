<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
    <head>
        <title>万得债券信息管理</title>
        <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    </head>
<body style="width:100%;height:100%;background:white">
   <fieldset class="mini-fieldset title">
    <legend>万得债券信息查询</legend>
    <div id="search_form" style="width: 100%;">
        <input id="bondCode" name="bondCode" class="mini-textbox" labelField="true" label="万得债券代码：" labelStyle="text-align:right;" emptyText="请输入债券代码" />
        <input id="interestTypeCode" name="interestTypeCode" class="mini-combobox" width="320px" data="CommonUtil.serverData.dictionary.IntcalRule"  emptyText="请选择付息方式代码" labelField="true"  label="付息方式代码：" labelStyle="text-align:right;"/>
        <span style="float:right;margin-right: 150px">
            <a class="mini-button" style="display: none"  id="search_btn"  onclick="search()">查询</a>
            <a class="mini-button" style="display: none"  id="clear_btn"  onclick="clear()">清空</a>
        </span>
    </div>
    </fieldset>
    
    <div class="mini-fit" style="width:100%;height:100%;">
        <div id="data_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" 
            sortMode="client" allowAlternating="true"   idField="userId" onrowdblclick="onRowDblClick" 
            border="true" allowResize="true" >
            <div property="columns">
                <div type="indexcolumn" headerAlign="center" align="center" width="40px">序列</div>
                <div field="bondCode" headerAlign="center"  align="center" width="100px" >万得债券代码</div>
                <div field="bondShortName" headerAlign="center"  width="140px"> 万得债券简称</div>
                <div field="bondLongName" headerAlign="center"  width="220px"> 万得债券全称</div>
                <div field="circuplace" headerAlign="center"  width="80px"> 流通场所</div>
                <div field="circuplaceCode" headerAlign="center"  width="80px"> 流通场所代码</div>
                <div field="ccy" headerAlign="center"  width="80px"> 币种</div>
                <div field="custodian" headerAlign="center"  width="80px"> 托管场所</div>
                <div field="custodianCode" headerAlign="center"  width="80px"> 托管场所代码</div>
                <div field="issuerCode" headerAlign="center" width="100px" >发行人ID</div>
                <div field="issuer" headerAlign="center" width="180px" >发行人</div>
                <div field="issuePrice" headerAlign="center"  width="40px">发行价格(元)</div>
                <div field="issueDate" headerAlign="center"  width="120px"> 发行日期</div>
                <div field="vdate" headerAlign="center"  width="120px"> 起息日</div>
                <div field="mdate" headerAlign="center"  width="120px"> 到期日</div>
                <div field="interestTypeCode" headerAlign="center"  width="80px"> 付息方式</div>
                <div field="term" headerAlign="center"  width="80px"> 债券期限</div>
                <div field="rate" headerAlign="center"  width="80px"> 票面利率</div>
                <div field="freq" headerAlign="center"  width="80px"> 付息周期</div>
                <div field="spread" headerAlign="center"  width="80px"> 基本利差</div>
                <div field="variety" headerAlign="center"  width="120px"> 证券分类</div>
                <div field="zxInst" headerAlign="center"  width="120px"> 最新债项评级机构</div>
                <div field="zxZxLevel" headerAlign="center"  width="120px"> 最新债项评级</div>
                <div field="fxZxDate" headerAlign="center"  width="120px"> 债券评级日期</div>
                <div field="zxInst" headerAlign="center"  width="120px"> 最新主体评级机构</div>
                <div field="zxZxLevel" headerAlign="center"  width="120px"> 最新主体评级</div>
                <div field="fxZxDate" headerAlign="center"  width="120px"> 主体评级日期</div>
            </div>
        </div>
    </div>
</body>
<script>
    mini.parse();

    var grid = mini.get("data_grid");
    grid.on("beforeload", function(e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        searchs(pageSize, pageIndex);
    });

    $(document).ready(function() {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
            search();
        });
	});

	function setData(data){
    	var form = new mini.Form("#search_form");
    	form.setData(data);
    }
    
    function searchs(pageSize, pageIndex) {
        var form = new mini.Form("#search_form");
        form.validate();
        if (form.isValid() == false) {
            mini.alert("表单填写错误,请确认!", "提示信息");
            return;
        }

        var data = form.getData();
        
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        data['branchId']=branchId;
        var params = mini.encode(data);
        CommonUtil.ajax({
            url : "/IfsWdSecController/searchPageWdBond",
            data : params,
            callback : function(data) {
                var grid = mini.get("data_grid");
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }
  //查询按钮
    function search(){
        searchs(grid.pageSize,0);
    }
    //清空
    function clear(){
        var form=new mini.Form("search_form");
        form.clear();
        search();
    }

    function GetData() {
        var grid = mini.get("data_grid");
        var row = grid.getSelected();
        return row;
    }
    
    //双击行选择
    function onRowDblClick(){
    	var row = grid.getSelected();
    	var cust=row.cust;
    	var intpaycicle=row.intpaycicle;
    	var couponprod=row.couponprod;
    	var bondproperties=row.bondproperties;
    	
//    	if(cust==null||cust==""){
//    		mini.alert("请先映射发行人对应opics交易对手信息!", "提示信息");
//    		return;
//    	}
//    	if(couponprod=="附息"){
//    		if(intpaycicle==null||intpaycicle==""){
//        		mini.alert("请先映射付息周期对应opics信息!", "提示信息");
//        		return;
//        	}
//    	}
//    	if(bondproperties==null||bondproperties==""){
//    		mini.alert("请先映射证券分类对应opics信息!", "提示信息");
//    		return;
//    	}
    	
    	onOk();
        
    }

    function CloseWindow(action) {
        if (window.CloseOwnerWindow)
            return window.CloseOwnerWindow(action);
        else
            window.close();
    }

    function onOk() {
        CloseWindow("ok");
    }

    //关闭窗口
    function onCancel() {
        CloseWindow("cancel");
    }
    
</script>
</html>