<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>债券信息 维护</title>
  </head>

<body style="width:100%;height:100%;background:white">
<div class="mini-splitter" style="width:100%;height:100%;">
	<div size="90%" showCollapseButton="false">
	<h1 style="text-align:center"><strong>债券信息</strong></h1>
	<div id="field_form" class="mini-fit area"  style="background:white">
	<input id="userid" name="userid" class="mini-hidden" />
	<input id="checkUserId" name="checkUserId" class="mini-hidden" />
	<input id="inputdate" name="inputdate" class="mini-hidden" />
	<input id="dealno" name="dealno" class="mini-textbox" width="30%" labelField="true" style="font-size:18px; margin-left: 55%;" enabled="false" label="交易单号：" labelStyle="text-align:left;width:120px" emptyText="由系统自动生成"/> 
		<div class="centerarea">
			<fieldset>
				<legend>债券信息</legend>
				<input id="bndcd" name="bndcd" class="mini-textbox" labelField="true" width="33%" label="债券代码：" maxLength="20"  labelStyle="text-align:left;width:120px;" required="true"   onvalidation="onEnglishAndNumberValidation"/>
				<input id="descr" name="descr" class="mini-textbox" labelField="true"  label="描述：" width="33%"  labelStyle="text-align:left;width:120px;"  required="true"  onvalidation="onChineseValidation"/>
				<input id="bndnm_en" name="bndnm_en" class="mini-textbox" labelField="true" width="33%"  label="债券英文名：" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" vtype="maxLength:25"  labelStyle="text-align:left;width:120px;"/>
				<input id="bndnm_cn" name="bndnm_cn" class="mini-textbox" labelField="true" width="33%"  label="债券名称：" labelStyle="text-align:left;width:120px;" required="true" />
				<input id="br" name="br" class="mini-combobox" labelField="true" width="33%"  label="部门：" vtype="maxLength:10"  labelStyle="text-align:left;width:120px;" data = "CommonUtil.serverData.dictionary.Br" required="true"  />
				<input id="element" name="element" class="mini-buttonedit"  width="33%" onbuttonclick="onElementQuery"  labelField="true"  label="交易要素组合：" allowInput="false" vtype="maxLength:4" labelStyle="text-align:left;width:120px;" required="true"  >
				<input id="product" name="product" class="mini-textbox"  labelField="true" width="33%" label="产品：" enabled="false" labelStyle="text-align:left;width:120px;" required="true"  />
				<input id="prodtype" name="prodtype" class="mini-textbox" labelField="true" width="33%"  label="产品类型：" enabled="false" vtype="maxLength:25"  labelStyle="text-align:left;width:120px;" required="true"   />
				<input id="intcalcrule" name="intcalcrule" class="mini-combobox" labelField="true" width="33%"  label="计息规则："  enabled="false" labelStyle="text-align:left;width:120px;"   data = "CommonUtil.serverData.dictionary.IntcalRule"  />
				<input id="acctngtype" name="acctngtype" class="mini-buttonedit" onbuttonclick="onActyQuery" labelField="true"  label="会计类型：" width="33%"   labelStyle="text-align:left;width:120px;"   />
				<input id="ccy" name="ccy" class="mini-combobox" labelField="true" width="33%"  label="债券币种：" vtype="maxLength:50"  labelStyle="text-align:left;width:120px;"   data = "CommonUtil.serverData.dictionary.Currency" required="true"  />
				<input id="secunit" name="secunit" class="mini-combobox" labelField="true" width="33%"  label="债券单元：" vtype="maxLength:50"  labelStyle="text-align:left;width:120px;"  data = "CommonUtil.serverData.dictionary.Secunit"  required="true"  />
				<input id="denom" name="denom" class="mini-combobox" labelField="true" width="33%"  label="一手的量：" vtype="maxLength:50"  labelStyle="text-align:left;width:120px;"  data = "CommonUtil.serverData.dictionary.Denom" required="true"   />
				<input id="issuer" name="issuer" class="mini-buttonedit" width="33%" labelField="true" onbuttonclick="onButtonEdit" label="发行机构：" vtype="maxLength:50"  labelStyle="text-align:left;width:120px" />
				<input id="pubdt" name="pubdt" class="mini-datepicker" width="33%" labelField="true"  label="发行日期：" labelStyle="text-align:left;width:120px;" required="true"   />
				<input id="bondproperties" name="bondproperties" class="mini-combobox" labelField="true" width="33%"  label="债券性质：" vtype="maxLength:50"  labelStyle="text-align:left;width:120px;" required="true"  data = "CommonUtil.serverData.dictionary.BondPro"   />
				<input id="settccy" name="settccy" class="mini-combobox" labelField="true" width="33%"  label="清算币种：" vtype="maxLength:50"  labelStyle="text-align:left;width:120px;" required="true"   data = "CommonUtil.serverData.dictionary.Currency"  />
				<input id="issudt" name="issudt" class="mini-datepicker" labelField="true" width="33%"  label="起息日："  labelStyle="text-align:left;width:120px;" required="true"   />
				<input id="matdt" name="matdt" class="mini-datepicker" labelField="true" width="33%"  label="到期日：" labelStyle="text-align:left;width:120px;"  required="true"  />
				<input id="couponrate" name="couponrate" class="mini-spinner"   changeOnMousewheel='false' labelField="true" width="33%"  label="票面利率(%)："  maxValue="999999999999" format="n2" labelStyle="text-align:left;width:120px;" required="true"   />
				<input id="paramt" name="paramt" class="mini-spinner"   changeOnMousewheel='false' labelField="true" width="33%"  label="发行价："  maxValue="999999999999" format="n2"  labelStyle="text-align:left;width:120px;" required="true"   />
				
				<input id="intpaycircle" name="intpaycircle" class="mini-combobox" labelField="true" width="33%"  label="付息周期：" maxLength="20"  data = "CommonUtil.serverData.dictionary.Intpaycircle"    labelStyle="text-align:left;width:120px;" />
				<input id="intpaymethod" name="intpaymethod" class="mini-combobox" labelField="true" width="33%"  label="付息方式：" vtype="maxLength:25"  labelStyle="text-align:left;width:120px;"  data = "CommonUtil.serverData.dictionary.IntendRule"  required="true" />
				<input id="intenddaterule" name="intenddaterule" class="mini-combobox"  labelField="true" width="33%"  label="到期付息规则："  labelStyle="text-align:left;width:120px;" required="true"   data = "CommonUtil.serverData.dictionary.IntendRule" required="true"  />
				<input id="bondratingclass" name="bondratingclass" class="mini-combobox" labelField="true" width="33%"  label="评级类别：" vtype="maxLength:10"  labelStyle="text-align:left;width:120px;"  data = "CommonUtil.serverData.dictionary.RatingClass" required="true"   />
				<input id="bondratingagency" name="bondratingagency" class="mini-combobox" labelField="true" width="33%" label="评级机构：" labelStyle="text-align:left;width:120px;" data = "CommonUtil.serverData.dictionary.RatingAgency" required="true"  />
				<input id="bondrating" name="bondrating" class="mini-combobox" labelField="true"  label="评级：" width="33%" vtype="maxLength:25"  labelStyle="text-align:left;width:120px;"  data = "CommonUtil.serverData.dictionary.Rating"  required="true"  />
				<input id="accountno" name="accountno" class="mini-combobox" labelField="true"  label="托管账户：" width="33%" vtype="maxLength:50"  labelStyle="text-align:left;width:120px;"   data = "CommonUtil.serverData.dictionary.AccountNo" required="true"  />
				<input id="basis" name="basis" class="mini-combobox" labelField="true"  label="计息基础：" width="33%" vtype="maxLength:50"  labelStyle="text-align:left;width:120px;"   data = "CommonUtil.serverData.dictionary.Basis" />
				<input id="paragraphfirstplan" name="paragraphfirstplan" class="mini-datepicker" labelField="true" width="33%"  label="首次划款日："  labelStyle="text-align:left;width:120px;"   />
				<input id="bondperiod" name="bondperiod" class="mini-textbox" labelField="true"  label="债券期限：" width="33%" labelStyle="text-align:left;width:120px;"   />
				<input id="basicspread" name="basicspread" class="mini-textbox"  labelField="true"  label="基本利差：" width="33%"  labelStyle="text-align:left;width:120px;"   />
				<input id="issuevol" name="issuevol" class="mini-textbox"  labelField="true"  label="实际发行量：" width="33%"   labelStyle="text-align:left;width:120px;"   />
				<input id="note" name="note" class="mini-textbox" labelField="true"  label="备注信息：" width="33%"   labelStyle="text-align:left;width:120px;"   />
				
			</fieldset>
		</div>	
		</div>
	</div>
	<div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
		<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="save_btn"   onclick="check">复核</a></div>
		<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="close_btn"   onclick="close">关闭界面</a></div>
	</div>      
</div>	
<script type="text/javascript">
mini.parse();
	var grid = mini.get("issuername");
	//获取当前tab
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var row=params.selectData;
	var url=window.location.search;
	var action=CommonUtil.getParam(url,"action");
	var form=new mini.Form("#field_form");

	//复核更新
	function check(){
		//表单验证！！！
		form.validate();
		if (form.isValid() == false) {
			return;
		}
		var userId ='<%=__sessionUser.getUserId()%>';
		var data=form.getData(true);
			data['status']="B";
			data['checkUserId']=userId;
			data['issuername']=mini.get("issuer").getText();
			var params=mini.encode(data);
			CommonUtil.ajax({
				url:"/IfsOpicsBondController/check",
				data:params,
				callback:function(data){
					mini.alert("复核成功",'提示信息',function(){
						top["win"].closeMenuTab();
				});
				}
			});
	} 
	function close(){
		top["win"].closeMenuTab();
	}
	//加载机构树
	function nodeclick(e) {
		var oldvalue=e.sender.value;
		var param = mini.encode({"branchId":branchId}); //序列化成JSON
		var oldData=e.sender.data;
		if(oldData==null||oldData==""){
			CommonUtil.ajax({
				url: "/InstitutionController/searchInstitutionByBranchId",
				data: param,
				callback: function (data) {
					var obj=e.sender;
					obj.setData(data.obj);
					obj.setValue(oldvalue);
				}
			});
		}
	}
	$(document).ready(function(){
		if($.inArray(action,["check","detail"])>-1){
			form.setEnabled(false);
			form.setData(row);
			//债券交易要素组合
			mini.get("element").setValue(row.element);
			mini.get("element").setText(row.element);
			mini.get("issuer").setText(row.issuername);
			mini.get("issuer").setValue(row.issuer);
			if($.inArray(action,["detail"])>-1){
				mini.get("save_btn").setVisible(false);
			}
		}
	});
	//加载机构树
	function nodeclick(e) {
		var oldvalue=e.sender.value;
		var param = mini.encode({"branchId":branchId}); //序列化成JSON
		var oldData=e.sender.data;
		if(oldData==null||oldData==""){
			CommonUtil.ajax({
				url: "/InstitutionController/searchInstitutionByBranchId",
				data: param,
				callback: function (data) {
					var obj=e.sender;
					obj.setData(data.obj);
					obj.setValue(oldvalue);
				}
			});
		}
	}
	
	function onButtonEdit(e) {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Common/CustMini.jsp",
            title: "选择对手方列表",
            width: 900,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.cno);
                        btnEdit.setText(data.cfn);
                        btnEdit.focus();
                    }
                }

            }
        });

    }
	function onActyQuery(e){
		var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Ifs/opics/actyMini.jsp",
            title: "选择会计类型",
            width: 900,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.acctngtype);
                        btnEdit.setText(data.acctngtype);
                        btnEdit.focus();
                    }
                }

            }
        });
	}
</script>	
</body>
</html>
