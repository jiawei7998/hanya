<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../../global.jsp" %>
<html>
<head>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script src="<%=basePath%>/miniScript/pinyin.js" type="text/javascript"></script>
    <title>普通债券信息 维护</title>
</head>

<body style="width:100%;height:100%;background:white">
    <div class="mini-splitter" style="width:100%;height:100%;">
        <div size="90%" showCollapseButton="false">
            <h1 style="text-align:center"><strong>债券基本信息</strong></h1>
            <div id="field_form" class="mini-fit area"  style="background:white">
                <input id="userid" name="userid" class="mini-hidden"/>
                <input id="inputdate" name="inputdate" class="mini-hidden"/>
                <input id="status" name="status" class="mini-hidden"/>
                <input id="dealno" name="dealno" class="mini-textbox" width="30%" labelField="true"  style="font-size:18px; margin-left: 55%;" enabled="false" label="交易单号：" labelStyle="text-align:left;width:120px" emptyText="由系统自动生成"/>
                <div class="mini-panel" title="债券基本信息" style="width:100%;"  allowResize="true" collapseOnTitleClick="true">
                    <div class="leftarea">
                        <input id="bndcd" name="bndcd" class="mini-buttonedit mini-mustFill" onbuttonclick="onSecEdit" labelField="true" width="100%" label="债券代码："  labelStyle="text-align:left;width:120px;" required="true"  onValueChanged="onBondChange"/>
                        <input id="bndnm_cn" name="bndnm_cn" class="mini-textbox mini-mustFill" required="true"  labelField="true" width="100%"  label="债券全称：" labelStyle="text-align:left;width:120px;"/>
                        <input id="instId" name="instId" class="mini-buttonedit mini-mustFill" onbuttonclick="onInstId" labelField="true" width="100%" label="所属机构："  labelStyle="text-align:left;width:120px;" />

                    </div>
                    <div class="leftarea">
                        <input id="bndnm_en" name="bndnm_en" class="mini-textbox mini-mustFill" required="true"  labelField="true" width="100%"  label="债券简称："  vtype="maxLength:25"  labelStyle="text-align:left;width:120px;"/>
                        <input id="descr" name="descr" class="mini-textbox mini-mustFill" labelField="true"  label="债券描述：" width="100%"  labelStyle="text-align:left;width:120px;"  required="true"  onvalidation="onChineseValidation"/>
                        <input id="instName" name="instName" class="mini-hidden" labelField="true"   width="100%"  labelStyle="text-align:left;width:120px;"  />

                    </div>
                </div>
                <div class="mini-panel" title="OPICS要素" style="width:100%;"  allowResize="true" collapseOnTitleClick="true">
                    <div class="leftarea">
                        <input id="product" name="product" class="mini-buttonedit mini-mustFill" onbuttonclick="onPrdEdit"  labelField="true" width="100%" label="产品代码：" labelStyle="text-align:left;width:120px;" required="true"  onValueChanged="onPrdChanged"/>
                        <input id="intcalcrule" name="intcalcrule" class="mini-combobox mini-mustFill" labelField="true" width="100%"  label="计算规则：" labelStyle="text-align:left;width:120px;"   data = "CommonUtil.serverData.dictionary.IntcalRule"  required="true" />
                        <input id="ccy" name="ccy" class="mini-combobox mini-mustFill" labelField="true" width="100%"  label="货币：" vtype="maxLength:50"  labelStyle="text-align:left;width:120px;"   data = "CommonUtil.serverData.dictionary.Currency" required="true"  />
                        <input id="issuer" name="issuer" class="mini-buttonedit mini-mustFill" width="100%" labelField="true"  onbuttonclick="onButtonEdit" label="发行人：" vtype="maxLength:50" labelStyle="text-align:left;width:120px" required="true" />
                        <input id="issudt" name="issudt" class="mini-datepicker mini-mustFill" labelField="true" width="100%" label="起息日：" labelStyle="text-align:left;width:120px;" required="true"   onvaluechanged="onBondPeriod"/>
                        <input id="paragraphfirstplan" name="paragraphfirstplan" class="mini-datepicker mini-mustFill" required="true"  labelField="true" width="100%" label="第一个息票日期："  labelStyle="text-align:left;width:120px;"/>
                        <input id="bondperiod" name="bondperiod" class="mini-textbox " labelField="true" label="债券期限(天)：" width="100%" labelStyle="text-align:left;width:120px;"/>
                        <input id="issprice" name="issprice" class="mini-spinner mini-mustFill" required="true"  changeOnMousewheel='false' labelField="true" width="100%" label="面值："  maxValue="99999999.9999" format="n4" labelStyle="text-align:left;width:120px;"  required="true" />
                        <input id="basis" name="basis" class="mini-combobox mini-mustFill" labelField="true" label="计息方式：" width="100%" vtype="maxLength:50" labelStyle="text-align:left;width:120px;"  data="CommonUtil.serverData.dictionary.Basis" required="true" />
                        <input id="accountno" name="accountno" class="mini-buttonedit mini-mustFill" onbuttonclick="onSaccQuery" labelField="true" label="托管机构：" width="100%" labelStyle="text-align:left;width:120px;" required="true" />
                        <input id="intpaymethod" name="intpaymethod" class="mini-combobox mini-mustFill" labelField="true"  width="100%" label="利息支付规则：" vtype="maxLength:25" labelStyle="text-align:left;width:120px;" data="CommonUtil.serverData.dictionary.IntendRule" value="S" required="true" />
                        <input id="bondproperties" name="bondproperties" class="mini-combobox mini-mustFill" labelField="true" width="100%" label="标准行业代码：" vtype="maxLength:50" labelStyle="text-align:left;width:120px;" required="true"  data="CommonUtil.serverData.dictionary.BondPro"/>
                        <input id="settccy" name="settccy" class="mini-combobox mini-mustFill" labelField="true" width="100%"  label="结算币种：" vtype="maxLength:50" labelStyle="text-align:left;width:120px;" required="true"  data="CommonUtil.serverData.dictionary.Currency" value="CNY"/>
                        <input id="couponrate" name="couponrate" class="mini-spinner" changeOnMousewheel='false' labelField="true" width="100%" label="票面利率(%)："  maxValue="999999999999"  format="n2" labelStyle="text-align:left;width:120px;"  allowNull="true" value="null" required="true" />
                    </div>
                    <div class="leftarea">
                        <input id="prodtype" name="prodtype" class="mini-buttonedit mini-mustFill" onbuttonclick="onTypeEdit" labelField="true" width="100%"  label="产品类型：" vtype="maxLength:25"  labelStyle="text-align:left;width:120px;" required="true"   />
                        <input id="acctngtype" name="acctngtype" class="mini-buttonedit mini-mustFill" onbuttonclick="onActyQuery"  labelField="true"  label="会计类型：" width="100%"   labelStyle="text-align:left;width:120px;"  required="true"  />
                        <input id="secunit" name="secunit" class="mini-combobox mini-mustFill" labelField="true" width="100%"  label="股权类型：" vtype="maxLength:50"  labelStyle="text-align:left;width:120px;"  data = "CommonUtil.serverData.dictionary.Secunit" value="FMT" readonly="true" required="true"  />
                        <input id="pubdt" name="pubdt" class="mini-datepicker mini-mustFill" width="100%" labelField="true"  label="发行日期：" labelStyle="text-align:left;width:120px;" required="true" />
                        <input id="matdt" name="matdt" class="mini-datepicker mini-mustFill" labelField="true" width="100%" label="到期日：" labelStyle="text-align:left;width:120px;" required="true"  onvaluechanged="onBondPeriod"/>
                        <input id="paragraphlastplan" name="paragraphlastplan" class="mini-datepicker mini-mustFill" required="true"  labelField="true" width="100%" label="最后息票日期：" labelStyle="text-align:left;width:120px;"/>
                        <input id="intpaydatum" name="intpaydatum" class="mini-combobox mini-mustFill" labelField="true"  width="100%" label="付息基准：" vtype="maxLength:25" labelStyle="text-align:left;width:120px;" data="CommonUtil.serverData.dictionary.IntendRule" required="true" />
                        <input id="repayamt" name="repayamt" class="mini-spinner mini-mustFill" required="true"   changeOnMousewheel='false' labelField="true" width="100%" label="偿还金额：" maxValue="99999999.9999" format="n4" labelStyle="text-align:left;width:120px;"  required="true" />
                        <input id="floatratemark" name="floatratemark" class="mini-buttonedit" onbuttonclick="onRateEdit" showNullItem="true" labelField="true" label="利率代码：" width="100%" vtype="maxLength:50" labelStyle="text-align:left;width:120px;"/>
                        <input id="intpaycircle" name="intpaycircle" class="mini-combobox" showNullItem="true"  labelField="true" width="100%" label="支付频率：" maxLength="20"  data="CommonUtil.serverData.dictionary.frequency" labelStyle="text-align:left;width:120px;"/>
                        <input id="intenddaterule" name="intenddaterule" class="mini-combobox mini-mustFill" required="true"  labelField="true" width="100%" label="利息结束日规则：" labelStyle="text-align:left;width:120px;" data="CommonUtil.serverData.dictionary.IntendRule" value="D"/>
                        <input id="denom" name="denom" class="mini-combobox mini-mustFill" labelField="true" width="100%" label="单价：" vtype="maxLength:50" labelStyle="text-align:left;width:120px;" data="CommonUtil.serverData.dictionary.Denom" value="10000" readonly="true" required="true" />
                        <input id="settdays" name="settdays" class="mini-spinner" vtype="int" labelField="true" width="100%" label="结算天数：" labelStyle="text-align:left;width:120px;" value="0" required="true" />

                    </div>
                </div>
                <div class="mini-panel" title="扩展信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
                    <div class="leftarea">
                        <input id="ZT_INST" name="ztInst" class="mini-combobox " labelField="true" width="100%" label="主体评级机构：" labelStyle="text-align:left;width:120px;" data="CommonUtil.serverData.dictionary.outCompany"/>
                        <input id="FX_ZT_DATE" name="fxZtDate" class="mini-datepicker"  labelField="true" width="100%" label="主体评级时间：" labelStyle="text-align:left;width:120px;"/>
                        <input id="ZX_ZX_LEVEL" name="zxZxLevel" class="mini-combobox"   labelField="true" label="债项评级：" width="100%" vtype="maxLength:25" labelStyle="text-align:left;width:120px;" data="CommonUtil.serverData.dictionary.Rating"/>
                        <input id="isWeight" name="isWeight" class="mini-combobox" showNullItem="true" labelField="true" label="含权属性：" width="100%" vtype="maxLength:50" labelStyle="text-align:left;width:120px;"  data="CommonUtil.serverData.dictionary.isWeight"/>
                        <input id="slPubMethod" name="slPubMethod" class="mini-combobox "  labelField="true" label="发行方式：" width="100%" vtype="maxLength:50"  labelStyle="text-align:left;width:120px;"  data="CommonUtil.serverData.dictionary.slPubMethod"/>

                    </div>
                    <div class="leftarea">
                        <input id="ZX_ZT_LEVEL" name="zxZtLevel" class="mini-combobox"   labelField="true" label="主体信用评级：" width="100%" vtype="maxLength:25" labelStyle="text-align:left;width:120px;" data="CommonUtil.serverData.dictionary.Rating"/>
                        <input id="ZX_INST" name="zxInst" class="mini-combobox " labelField="true" width="100%" label="债项评级机构：" labelStyle="text-align:left;width:120px;" data="CommonUtil.serverData.dictionary.outCompany"/>
                        <input id="FX_ZX_DATE" name="fxZxDate" class="mini-datepicker"  labelField="true" width="100%" label="债项评级时间：" labelStyle="text-align:left;width:120px;"/>
                        <input id="rateType" name="rateType" class="mini-combobox "  labelField="true" label="利率类型：" width="100%" vtype="maxLength:50" labelStyle="text-align:left;width:120px;" data="CommonUtil.serverData.dictionary.ratetype"/>
                    </div>
                </div>
                <div class="mini-panel" title="报表要素" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
                    <div class="leftarea">
                        <input id="bongRightDebtDate" name="bongRightDebtDate" class="mini-datepicker"  labelField="true" width="100%" label="债权债务登记日：" vtype="maxLength:10" labelStyle="text-align:left;width:120px;" format="yyyy-MM-dd"/>
                        <input id="finalInvestmentType" name="finalInvestmentType" class="mini-textbox "    labelField="true" label="最终投向类型：" width="100%" vtype="maxLength:25" labelStyle="text-align:left;width:120px;"/>
                        <input id="issueEnterpriseScale" name="issueEnterpriseScale" class="mini-textbox" showNullItem="true" labelField="true" label="发行人企业规模" width="100%" vtype="maxLength:50" labelStyle="text-align:left;width:120px;"/>
                        <input id="financingType" name="financingType" class="mini-combobox" showNullItem="true" labelField="true" label="融资类型" width="100%" labelStyle="text-align:left;width:120px;"data="CommonUtil.serverData.dictionary.financingType"/>

                    </div>
                    <div class="leftarea">
                        <input id="reissueFrequency" name="reissueFrequency" class="mini-textbox"    labelField="true" width="100%" label="续发次数：" labelStyle="text-align:left;width:120px;" />
                        <input id="finalInvestmentIndustry" name="finalInvestmentIndustry" class="mini-textbox "   labelField="true" label="最终投向行业：" width="100%" vtype="maxLength:50" labelStyle="text-align:left;width:120px;" />
                        <input id="investmentFundsUse" name="investmentFundsUse" class="mini-textbox"    labelField="true" label="投资金用途：" width="100%" vtype="maxLength:50"  labelStyle="text-align:left;width:120px;"  />
                        <input id="debtorLevel" name="debtorLevel" class="mini-combobox" showNullItem="true" labelField="true" label="债务人层级" width="100%" labelStyle="text-align:left;width:120px;"data="CommonUtil.serverData.dictionary.debtorLevel"/>

                    </div>
                </div>
            </div>
        </div>
        <div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
            <div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="save_btn"   onclick="save">保存交易</a></div>
            <div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="close_btn"   onclick="close">关闭界面</a></div>
        </div>
    </div>

<%--<div class="mini-splitter" style="width:100%;height:100%;">--%>
<%--    <div size="90%" showCollapseButton="false">--%>
<%--        <h1 style="text-align:center"><strong>债券信息</strong></h1>--%>
<%--        <div id="field_form" class="mini-fit area" style="background:white">--%>
<%--            <div class="centerarea">--%>
<%--                <fieldset>--%>
<%--                    <legend>普通债券信息</legend>--%>
<%--                    <input id="br" name="br" class="mini-combobox mini-mustFill" labelField="true" width="100%"--%>
<%--                           label="部门：" vtype="maxLength:10" labelStyle="text-align:left;width:120px;"--%>
<%--                           data="CommonUtil.serverData.dictionary.Br" required="true" />--%>
<%--                    <div id="circuplace" name="circuplace" class="mini-hidden"/>--%>
<%--                    <input id="marketDate" name="marketDate" class="mini-datepicker mini-mustFill" labelField="true"--%>
<%--                           width="100%" label="上市日：" labelStyle="text-align:left;width:120px;" required="true" />--%>
<%--                    <input id="basicspread" name="basicspread" class="mini-textbox" labelField="true" label="基本利差："--%>
<%--                           width="100%" labelStyle="text-align:left;width:120px;"/>--%>
<%--                    <input id="issuevol" name="issuevol" class="mini-textbox" labelField="true" label="实际发行数量(亿元)："--%>
<%--                           width="100%" labelStyle="text-align:left;width:120px;"/>--%>
<%--                    <input id="note" name="note" class="mini-textbox" labelField="true" label="备注信息：" width="100%"--%>
<%--                           labelStyle="text-align:left;width:120px;"/>--%>
<%--                    <input id="settdays" name="settdays" class="mini-spinner" vtype="int" labelField="true" width="100%"--%>
<%--                           label="结算天数：" labelStyle="text-align:left;width:120px;" required="true" />--%>
<%--                </fieldset>--%>
<%--            </div>--%>
<%--        </div>--%>
<%--    </div>--%>
<%--</div>--%>


<script type="text/javascript">
    mini.parse();

    //获取当前tab
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var row = params.selectData;
    var url = window.location.search;
    var action = CommonUtil.getParam(url, "action");
    var form = new mini.Form("#field_form");

    var tradeData = {};

    tradeData.selectData = row;
    tradeData.operType = action;
    tradeData.serial_no = row.execId;
    tradeData.task_id = row.taskId;

    //保存
    function save() {
        //表单验证！！！
        form.validate();
        if (form.isValid() == false) {
            return;
        }
        if (checkOpics()) {
            return;
        }
        var userId = '<%=__sessionUser.getUserId()%>';
        var data = form.getData(true);
        if (data.status != "C") {
            data['status'] = "A";
        }
        data['userid'] = userId;
        data['issuername'] = mini.get("issuer").getText();
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: "/IfsOpicsBondController/save",
            data: params,
            callback: function (data) {
                mini.alert(data, '提示信息', function () {
                    if (data == '保存成功' || data == '信息修改成功') {
                        top["win"].closeMenuTab();
                    }
                });
            }
        });
    }

    function close() {
        top["win"].closeMenuTab();
    }

    //英文、数字、下划线 的验证
    function onEnglishAndNumberValidation(e) {
        if (e.value == "" || e.value == null) {//值为空，就不做校验
            return;
        }
        if (e.isValid) {
            if (isEnglishAndNumber(e.value) == false) {
                e.errorText = "必须输入英文+数字";
                e.isValid = false;
            }
        }
    }

    /* 是否英文+数字 */
    function isEnglishAndNumber(v) {
        var re = new RegExp("^[0-9a-zA-Z\_\-]+$");
        if (re.test(v)) return true;
        return false;
    }

    /* 是否汉字 */
    function isChinese(v) {
        var re = new RegExp("^[\u4e00-\u9fa5]+$");
        if (re.test(v)) return true;
        return false;
    }

    function onChineseValidation(e) {
        if (e.isValid) {
            if (isChinese(e.value) == true) {
                e.errorText = "不能输入中文";
                e.isValid = false;
            }
        }
    }

    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
            if ($.inArray(action, ["edit", "detail"]) > -1) {
                // onbondrating(row.bondratingagency);
                debugger;
                form.setData(row);
                if (row.intpaycircle == null || row.intpaycircle == "") {
                    mini.get("paragraphfirstplan").setRequired(false);
                }
                //债券交易要素组合
                mini.get("issuer").setValue(row.issuer);
                mini.get("issuer").setText(row.issuername);
                mini.get("bndcd").setEnabled(false);
                mini.get("acctngtype").setValue(row.acctngtype);
                mini.get("acctngtype").setText(row.acctngtype);
                mini.get("accountno").setValue(row.accountno);
                mini.get("accountno").setText(row.accountno);
                mini.get("product").setValue(row.product);
                mini.get("product").setText(row.product);
                mini.get("prodtype").setValue(row.prodtype);
                mini.get("prodtype").setText(row.prodtype);
                mini.get("bndcd").setValue(row.bndcd);
                mini.get("bndcd").setText(row.bndcd);
                mini.get("floatratemark").setValue(row.floatratemark);
                mini.get("floatratemark").setText(row.floatratemark);
                mini.get("instId").setText(row.instName);

                if (row.status != "A") {
                    //mini.get("br").setEnabled(false);
                    mini.get("product").setEnabled(false);
                    mini.get("prodtype").setEnabled(false);
                    mini.get("intcalcrule").setEnabled(false);
                    mini.get("acctngtype").setEnabled(false);
                    mini.get("ccy").setEnabled(false);
                    mini.get("secunit").setEnabled(false);
                    mini.get("denom").setEnabled(false);
                    mini.get("issuer").setEnabled(false);
                    mini.get("pubdt").setEnabled(false);
                    mini.get("bondproperties").setEnabled(false);
                    mini.get("issudt").setEnabled(false);
                    mini.get("matdt").setEnabled(false);
                    mini.get("couponrate").setEnabled(false);
// 				mini.get("issprice").setEnabled(false);
                    mini.get("intpaycircle").setEnabled(false);
                    mini.get("intpaymethod").setEnabled(false);
                    mini.get("intenddaterule").setEnabled(false);
                    // mini.get("accountno").setEnabled(false);
                    mini.get("basis").setEnabled(false);
                    mini.get("floatratemark").setEnabled(false);
                    //mini.get("basicspread").setEnabled(false);
                    // mini.get("settdays").setEnabled(false);
                }

                if ($.inArray(action, ["detail"]) > -1) {
                    mini.get("save_btn").setVisible(false);
                    form.setEnabled(false);
                }
            }
        });
    });

    function onElementQuery() {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "/bond/tradeElementParamMini.jsp",
            title: "交易要素组合选择",
            width: 900,
            height: 600,

            onload: function () {//弹出页面加载完成
                var iframe = this.getIFrameEl();
                var data = {"element": "SECUR"};
                iframe.contentWindow.setData(data);
                iframe.contentWindow.search();
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data); //必须
                    if (data) {
                        btnEdit.setValue(data.element);
                        btnEdit.setText(data.element);
                        mini.get("intcalcrule").setValue(data.intcalcrule);//计息规则
                        mini.get("prodtype").setValue(data.prodType);//产品类型
                        mini.get("product").setValue(data.product);//产品
                        btnEdit.focus();
                    }
                }
            }
        });
    }

    //加载机构树
    function nodeclick(e) {
        var oldvalue = e.sender.value;
        var param = mini.encode({"branchId": branchId}); //序列化成JSON
        var oldData = e.sender.data;
        if (oldData == null || oldData == "") {
            CommonUtil.ajax({
                url: "/InstitutionController/searchInstitutionByBranchId",
                data: param,
                callback: function (data) {
                    var obj = e.sender;
                    obj.setData(data.obj);
                    obj.setValue(oldvalue);
                }
            });
        }
    }

    function onButtonEdit(e) {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../credit/limit/LimitCustMini.jsp",
            title: "选择对手方列表",
            width: 900,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.cno);
                        btnEdit.setText(data.cliname);
                        btnEdit.focus();
                    }
                }

            }
        });

    }

    function onActyQuery(e) {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Ifs/opics/actyMini.jsp",
            title: "选择会计类型",
            width: 900,
            height: 600,
            onload: function () {
                var iframe = this.getIFrameEl();
                var data = {type1: "2", type2: "2"};//把会计归属类型传到小页面，"2"表示归属债券类，"3"表示归属客户及债券类
                iframe.contentWindow.SetData(data);
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.acctngtype);
                        btnEdit.setText(data.acctngtype);
                        btnEdit.focus();
                    }
                }

            }
        });
    }

    //托管机构选择
    function onSaccQuery(e) {
        var btnEdit = this;
        mini.open({
            url: "./Ifs/opics/saccMini.jsp",
            title: "选择托管机构列表",
            width: 900,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.accountno);
                        btnEdit.setText(data.accountno);
                        btnEdit.focus();
                    }
                }
            }
        });
    }

    function onBondChange(e) {
        var bndcd = e.value;
        //将输入值小写字母全部转成大写字母
        if (bndcd != null && bndcd != "") {
            bndcd = bndcd.toUpperCase();
            mini.get("bndcd").setValue(bndcd);
        }
        var param = mini.encode({"bndcd": bndcd}); //序列化成JSON
        CommonUtil.ajax({
            url: "/IfsOpicsBondController/isExist",
            data: param,
            callback: function (data) {
                if (data.obj.bndcd != null) {
                    mini.get("save_btn").disable();
                    mini.alert("债券信息在opics中已存在，请在查询同步后在修改界面操作!");

                }else {
                    mini.get("save_btn").enable();
                }
            }
        });
    }

    /* 产品代码的选择 */
    function onPrdEdit() {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "/opics/prodMiniLess.jsp",
            title: "选择产品代码",
            width: 900,
            height: 600,

            onload: function () {//弹出页面加载完成
                var iframe = this.getIFrameEl();
                var data = {"pcode": "SECUR"};
                iframe.contentWindow.SetData(data);
            },

            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.pcode);
                        btnEdit.setText(data.pcode);
                        btnEdit.focus();
                    }
                }
            }
        });
    }

    function onPrdChanged() {
        mini.get("prodType").setValue("");
        mini.alert("产品代码已改变，请重新选择产品类型!");
    }

    /* 产品类型的选择 */
    function onTypeEdit() {
        var prd = mini.get("product").getValue();
        if (prd == null || prd == "") {
            mini.alert("请先选择产品代码!");
            return;
        }
        var data = {prd: prd};
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "/opics/typeMiniLess.jsp",
            title: "选择产品类型",
            width: 900,
            height: 600,
            onload: function () {
                var iframe = this.getIFrameEl();
                iframe.contentWindow.SetData(data);
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.type);
                        btnEdit.setText(data.type);
                        btnEdit.focus();
                    }
                }

            }
        });
    }

    //债券代码选择
    function onSecEdit(e) {
        var btnEdit = this;
        mini.open({
            url: "./Ifs/bond/windBondMini.jsp",
            title: "选择债券列表",
            width: 900,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.circuplaceCode=='IB'? data.bondCode: data.bondCode + "." + data.circuplaceCode);
                        btnEdit.setText(data.circuplaceCode=='IB'? data.bondCode: data.bondCode + "." + data.circuplaceCode);
                        mini.get("bndcd").doValueChanged();//调用事件触发
                        mini.get("product").setValue("SECUR");
                        mini.get("product").setText("SECUR");
                        mini.get("bndnm_en").setValue(data.bondShortName);//债券简称
                        mini.get("bndnm_cn").setValue(data.bondLongName);//债券全称
                        mini.get("descr").setValue(pinyin(data.bondLongName, {style: pinyin.STYLE_FIRST_LETTER}).join(''));//债券描述
                        mini.get("intcalcrule").setValue(data.interestTypeCode);//计息规则
                        mini.get("ccy").setValue(data.ccy);//债券币种
                        queryIssuerMapping('CUST', data.issuerCode) //发行机构
                        mini.get("pubdt").setValue(data.issueDate);// 发行日期
                        //debugger
                        //queryBondpropertiesMapping('BONDPRO',data.varietyCode)//债券性质
                        mini.get("settccy").setValue(data.ccy);//清算币种
                        mini.get("issudt").setValue(data.vdate);//起息日
                        mini.get("matdt").setValue(data.mdate);//到期日
                        mini.get("couponrate").setValue(data.rate);//票面利率
                        mini.get("issprice").setValue(data.issuePrice);//发行价
                        mini.get("intpaycircle").setValue(fomatFreq(data.freq));//付息周期
                        //转化评级机构 类型 评级
                        // mini.get("bondratingclass").setValue(data.zxZxLevel);//评级类别
                        querySecLevel(data.bondCode);
                        mini.get("accountno").setValue(data.custodianCode);//托管机构
                        mini.get("accountno").setText(data.custodianCode);//托管机构
                        mini.get("basis").setValue(data.basisType);//计息基础
                        onBondPeriod();
                        mini.get("rateType").setValue(fomatRateType(data.rateType));//计息基础
                        //续发次数
                        mini.get("reissueFrequency").setValue(data.sfcs);
                        // mini.get("basicspread").setValue(data.spread);//基本利差
                        mini.get("bndnm_en").setEnabled(false);//债券简称
                        mini.get("bndnm_cn").setEnabled(false);//债券全称
                        //mini.get("intcalcrule").setEnabled(false);//计息规则
                        mini.get("ccy").setEnabled(false);//债券币种
                        mini.get("pubdt").setEnabled(false);// 发行日期
                        //mini.get("bondproperties").setEnabled(false);//债券性质
                        mini.get("settccy").setEnabled(false);//清算币种
                        mini.get("issudt").setEnabled(false);//起息日
                        mini.get("matdt").setEnabled(false);//到期日
                        // mini.get("couponrate").setEnabled(false);//票面利率
                        mini.get("issprice").setEnabled(false);//发行价
                        mini.get("intpaycircle").setEnabled(false);//付息周期
                        mini.get("bondratingclass").setEnabled(false);//评级类别
                        // mini.get("accountno").setEnabled(false);//托管机构
                        //mini.get("basis").setEnabled(false);//计息基础
                        mini.get("bondperiod").setEnabled(false);//期限
                        // mini.get("basicspread").setEnabled(false);//基本利差
                        btnEdit.focus();
                    }
                }
            }
        });
    }

    function onInstId(e){
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "/../Common/MiniInstitutionBrach.jsp",
            title: "机构选择",
            width: 700,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data); //必须
                    if (data) {
                        btnEdit.setValue(data.dict_key);
                        btnEdit.setText(data.dict_value);
                        mini.get("instName").setValue(data.dict_value);
                        btnEdit.focus();
                    }
                }
            }
        });
    }

    //根据评级机构value查询key
    function queryBondratingagency(code, value) {
        CommonUtil.ajax({
            url: "/IfsOpicsBondController/queryKey",
            data: {code: code, value: value},
            callback: function (data) {
                if (data) {
                    mini.get("bondratingagency").setValue(data);
                } else {
                    mini.get("bondratingagency").setValue();
                }
            }
        });
    }

    function queryBondrating(code, value) {
        CommonUtil.ajax({
            url: "/IfsOpicsBondController/queryKey",
            data: {code: code, value: value},
            callback: function (data) {
                if (data) {
                    mini.get("bondrating").setValue(data);
                } else {
                    mini.get("bondrating").setValue();
                }
            }
        });
    }

    //发行人映射
    function queryIssuerMapping(mapid, wdid) {
        CommonUtil.ajax({
            url: "/IfsOpicsBondController/queryKeyMapping",
            data: {mapid: mapid, wdid: wdid},
            callback: function (data) {
                if (data) {
                    mini.get("issuer").setValue(data.opicsid);
                    mini.get("issuer").setText(data.opicsname);
                } else {
                    mini.get("issuer").setValue();
                    mini.get("issuer").setText();
                }
            }
        });
    }

    //债券分类
    function queryBondpropertiesMapping(mapid, wdid) {
        CommonUtil.ajax({
            url: "/IfsOpicsBondController/queryKeyMapping",
            data: {mapid: mapid, wdid: wdid},
            callback: function (data) {
                if (data) {
                    mini.get("bondproperties").setValue(data.opicsid);
                    mini.get("bondproperties").setText(data.opicsname);
                } else {
                    mini.get("bondproperties").setValue();
                    mini.get("bondproperties").setText();
                }
            }
        });
    }

    //债券评级
    function querySecLevel(secid) {
        CommonUtil.ajax({
            url: "/IfsOpicsBondController/queryKeyLevel",
            data: {secid: secid},
            callback: function (data) {
                if (data) {
                    //主体评级信息
                    mini.get("ZT_INST").setValue(data.ztInst);
                    mini.get("ZX_ZT_LEVEL").setValue(data.zxZtLevel);
                    mini.get("FX_ZT_DATE").setValue(data.fxZtDate);

                    //债项评级信息
                    mini.get("ZX_INST").setValue(data.zxInst);
                    mini.get("ZX_ZX_LEVEL").setValue(data.zxZxLevel);
                    mini.get("FX_ZX_DATE").setValue(data.fxZxDate);

                } else {
                    // mini.get("bondratingagency").setValue();
                    // mini.get("bondratingagency").setText();
                    // mini.get("bondrating").setValue();
                    // mini.get("bondrating").setText();
                }
            }
        });
    }

    function onBondPeriod() {
        var issudt = fomatDate(mini.get("issudt").getValue());
        var matdt = fomatDate(mini.get("matdt").getValue());
        if (pubdt != "" && matdt != "") {
            var bondPeriod = CommonUtil.dateDiff(issudt, matdt);
            mini.get("bondperiod").setValue(bondPeriod);
        }
    }

    function fomatDate(dateStr) {
        if (dateStr == null || dateStr == '') {
            return "";
        }
        var date = new Date(dateStr);
        var y = date.getFullYear();
        var m = date.getMonth() + 1;
        m = m < 10 ? ('0' + m) : m;
        var d = date.getDate();
        d = d < 10 ? ('0' + d) : d;
        return y + '-' + m + '-' + d;
    }

    function fomatFreq(frea) {
        var intpaycircle = null;
        if (frea == 1) {
            intpaycircle = 'M';
        } else if (frea == 3) {
            intpaycircle = 'Q';
        } else if (frea == 6) {
            intpaycircle = 'S';
        } else if (frea == 12) {
            intpaycircle = 'A';
        }
        return intpaycircle;
    }

    function fomatRateType(rateName) {
        var rateType = null;
        if (rateName == "固定利率") {
            rateType = 'FIXED';
        } else if (rateName == "浮动利率") {
            rateType = 'FLOAT';
        } else if (rateName == "累进利率") {
            rateType = 'OPTSE';
        }
        return rateType;
    }

    //不同评级机构对应不同的评级类型
    function onbondrating(bragency) {
        if (bragency == 0) {
            bragency = mini.get("bondratingagency").getValue();//评级机构
            mini.get("bondrating").setValue();//评级设为空
        }
        if (bragency == '2') {
            //穆迪
            mini.get("bondrating").setData("CommonUtil.serverData.dictionary.RatingMd");
//     	}else if(bragency=='1'){
//     		//标普
//     		mini.get("bondrating").setData("CommonUtil.serverData.dictionary.RatingBp");
        } else {
            //惠普
            mini.get("bondrating").setData("CommonUtil.serverData.dictionary.Rating");
        }
    }

    //利率代码的选择
    function onRateEdit() {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "/opics/rateMini.jsp",
            title: "选择利率代码",
            width: 900,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.ratecode);
                        btnEdit.setText(data.ratecode);
                        btnEdit.focus();
                    }
                }
            }
        });
    }

    //opics规则校验
    function checkOpics() {
        //校验固定和浮息
        var flag = false;
        var msg = "";
        var intcalcrule = mini.get("intcalcrule").getValue();
        if (intcalcrule != 'DIS') {
            var couponrate = mini.get("couponrate").getValue();//票面利率
            var floatratemark = mini.get("floatratemark").getValue();//浮息基准
            if (couponrate == 0 && floatratemark == '') {
                msg = "固定和浮息必须选择一个";
                flag = true;
            } else if (couponrate != 0 && floatratemark != '') {
                msg = "固定和浮息只能选择一个";
                flag = true;
            }
            // if (floatratemark != '') {
            //     var basicspread = mini.get("basicspread").getValue();
            //     if (basicspread == null || basicspread == '') {
            //         msg = "浮息债基本利差不能为空";
            //         flag = true;
            //     }
            // }
        }
        if (intcalcrule == 'DIS') {
            //贴现债计息基础不能选actual
            var basis = mini.get("basis").getValue();
            if (basis == 'ACTUAL') {
                msg = "贴现债计息基础不能为ACTUAL";
                flag = true;
            }
        }
        if (msg != "") {
            mini.alert(msg);
        }
        return flag;
    }

</script>
<script src="<%=basePath%>/miniScript/miniMustFill.js"></script>
</body>
</html>