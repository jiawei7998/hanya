<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>发行同业存单信息 维护</title>
  </head>

<body style="width:100%;height:100%;background:white">
<div class="mini-splitter" style="width:100%;height:100%;">
	<div size="90%" showCollapseButton="false">
	<h1 style="text-align:center"><strong>债券信息</strong></h1>
	<div id="field_form" class="mini-fit area"  style="background:white">
	<input id="userid" name="userid" class="mini-hidden" />
	<input id="inputdate" name="inputdate" class="mini-hidden" />
	<input id="status" name="status" class="mini-hidden" />
	<input id="dealno" name="dealno" class="mini-textbox" width="30%" labelField="true" style="font-size:18px; margin-left: 55%;" enabled="false" label="交易单号：" labelStyle="text-align:left;width:120px" emptyText="由系统自动生成"/> 
		<div class="centerarea">
			<fieldset>
				<legend>发行同业存单信息</legend>
				<input id="bndcd" name="bndcd" class="mini-textbox" labelField="true" width="33%" label="债券代码：" maxLength="20"  labelStyle="text-align:left;width:120px;" required="true"   onvalidation="onEnglishAndNumberValidation" onvaluechanged="onBondChange"/>
				<input id="descr" name="descr" class="mini-textbox" labelField="true"  label="描述：" width="33%"  labelStyle="text-align:left;width:120px;"  required="true"  onvalidation="onChineseValidation"/>
				<input id="bndnm_en" name="bndnm_en" class="mini-textbox" labelField="true" width="33%"  label="债券英文名：" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" vtype="maxLength:25"  labelStyle="text-align:left;width:120px;"/>
				<input id="bndnm_cn" name="bndnm_cn" class="mini-textbox" labelField="true" width="33%"  label="债券名称：" labelStyle="text-align:left;width:120px;" required="true" />
				<input id="br" name="br" class="mini-combobox" labelField="true" width="33%"  label="部门：" vtype="maxLength:10"  labelStyle="text-align:left;width:120px;" data = "CommonUtil.serverData.dictionary.Br" required="true"  />
				<!-- <input id="element" name="element" class="mini-buttonedit"  width="33%" onbuttonclick="onElementQuery"  labelField="true"  label="交易要素组合：" allowInput="false" vtype="maxLength:4" labelStyle="text-align:left;width:120px;" required="true"  > -->
				<input id="product" name="product" class="mini-buttonedit mini-mustFill" onbuttonclick="onPrdEdit"  labelField="true" width="33%" label="产品：" labelStyle="text-align:left;width:120px;" required="true"  onValueChanged="onPrdChanged"/>
				<input id="prodtype" name="prodtype" class="mini-buttonedit mini-mustFill" onbuttonclick="onTypeEdit" labelField="true" width="33%"  label="产品类型：" vtype="maxLength:25"  labelStyle="text-align:left;width:120px;" required="true"   />
				<input id="intcalcrule" name="intcalcrule" class="mini-combobox" labelField="true" width="33%"  label="计息规则：" labelStyle="text-align:left;width:120px;"   data = "CommonUtil.serverData.dictionary.IntcalRule"  />
				<input id="acctngtype" name="acctngtype" class="mini-buttonedit mini-mustFill" onbuttonclick="onActyQuery"  labelField="true"  label="会计类型：" width="33%"   labelStyle="text-align:left;width:120px;"   />
				<input id="ccy" name="ccy" class="mini-combobox" labelField="true" width="33%"  label="债券币种：" vtype="maxLength:50"  labelStyle="text-align:left;width:120px;"   data = "CommonUtil.serverData.dictionary.ABSCurrency" required="true"  />
				<input id="secunit" name="secunit" class="mini-combobox" labelField="true" width="33%"  label="债券单元：" vtype="maxLength:50"  labelStyle="text-align:left;width:120px;"  data = "CommonUtil.serverData.dictionary.Secunit"  required="true"  />
				<input id="denom" name="denom" class="mini-combobox" labelField="true" width="33%"  label="一手的量：" vtype="maxLength:50"  labelStyle="text-align:left;width:120px;"  data = "CommonUtil.serverData.dictionary.Denom" required="true"   />
				<input id="issuer" name="issuer" class="mini-buttonedit" width="33%" labelField="true" onbuttonclick="onButtonEdit" label="发行机构：" vtype="maxLength:50"  labelStyle="text-align:left;width:120px" />
				<input id="pubdt" name="pubdt" class="mini-datepicker" width="33%" labelField="true"  label="发行日期：" labelStyle="text-align:left;width:120px;" required="true"   />
				<input id="bondproperties" name="bondproperties" class="mini-combobox" labelField="true" width="33%"  label="债券性质：" vtype="maxLength:50"  labelStyle="text-align:left;width:120px;" required="true"  data = "CommonUtil.serverData.dictionary.BondPro"   />
				<input id="settccy" name="settccy" class="mini-combobox" labelField="true" width="33%"  label="清算币种：" vtype="maxLength:50"  labelStyle="text-align:left;width:120px;" required="true"   data = "CommonUtil.serverData.dictionary.ABSCurrency"  />
				<input id="issudt" name="issudt" class="mini-datepicker" labelField="true" width="33%"  label="起息日："  labelStyle="text-align:left;width:120px;" required="true"   />
				<input id="matdt" name="matdt" class="mini-datepicker" labelField="true" width="33%"  label="到期日：" labelStyle="text-align:left;width:120px;"  required="true"  />
				<input id="couponrate" name="couponrate" class="mini-spinner"   changeOnMousewheel='false' labelField="true" width="33%"  label="票面利率(%)："  maxValue="999999999999" format="n2" labelStyle="text-align:left;width:120px;" required="true"   />
				<input id="paramt" name="paramt" class="mini-spinner"   changeOnMousewheel='false' labelField="true" width="33%"  label="发行价："  maxValue="999999999999" format="n2"  labelStyle="text-align:left;width:120px;" required="true"   />
				
				<input id="intpaycircle" name="intpaycircle" class="mini-combobox" labelField="true" width="33%"  label="付息周期：" maxLength="20"  data = "CommonUtil.serverData.dictionary.Intpaycircle"    labelStyle="text-align:left;width:120px;" />
				<input id="intpaymethod" name="intpaymethod" class="mini-combobox" labelField="true" width="33%"  label="付息方式：" vtype="maxLength:25"  labelStyle="text-align:left;width:120px;"  data = "CommonUtil.serverData.dictionary.IntendRule"  required="true" />
				<input id="intenddaterule" name="intenddaterule" class="mini-combobox"  labelField="true" width="33%"  label="到期付息规则："  labelStyle="text-align:left;width:120px;" required="true"   data = "CommonUtil.serverData.dictionary.IntendRule" required="true"  />
				<input id="bondratingclass" name="bondratingclass" class="mini-combobox" labelField="true" width="33%"  label="评级类别：" vtype="maxLength:10"  labelStyle="text-align:left;width:120px;"  data = "CommonUtil.serverData.dictionary.RatingClass" required="true"   />
				<input id="bondratingagency" name="bondratingagency" class="mini-combobox" labelField="true" width="33%" label="评级机构：" labelStyle="text-align:left;width:120px;" data = "CommonUtil.serverData.dictionary.RatingAgency" required="true"  />
				<input id="bondrating" name="bondrating" class="mini-combobox" labelField="true"  label="评级：" width="33%" vtype="maxLength:25"  labelStyle="text-align:left;width:120px;"  data = "CommonUtil.serverData.dictionary.Rating"  required="true"  />
				<input id="accountno" name="accountno" class="mini-buttonedit mini-mustFill" onbuttonclick="onSaccQuery" labelField="true"  label="托管账户：" width="33%"  labelStyle="text-align:left;width:120px;" required="true"  />
				<input id="basis" name="basis" class="mini-combobox" labelField="true"  label="计息基础：" width="33%" vtype="maxLength:50"  labelStyle="text-align:left;width:120px;"   data = "CommonUtil.serverData.dictionary.Basis" required="true" />
				<input id="paragraphfirstplan" name="paragraphfirstplan" class="mini-datepicker" labelField="true" width="33%"  label="首次划款日："  labelStyle="text-align:left;width:120px;"   />
				<input id="bondperiod" name="bondperiod" class="mini-textbox" labelField="true"  label="债券期限：" width="33%" labelStyle="text-align:left;width:120px;"   />
				<input id="basicspread" name="basicspread" class="mini-textbox"  labelField="true"  label="基本利差：" width="33%"  labelStyle="text-align:left;width:120px;"   />
				<input id="issuevol" name="issuevol" class="mini-textbox"  labelField="true"  label="实际发行量：" width="33%"   labelStyle="text-align:left;width:120px;"   />
				<input id="note" name="note" class="mini-textbox" labelField="true"  label="备注信息：" width="33%"   labelStyle="text-align:left;width:120px;"   />
				<input id="settdays" name="settdays" class="mini-spinner"  vtype="int" labelField="true" width="33%"  label="结算天数："  labelStyle="text-align:left;width:120px;" required="true"   />
		</div>	
		</div>
	</div>
	<div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
		<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="save_btn"   onclick="save">保存信息</a></div>
		<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="close_btn"   onclick="close">关闭界面</a></div>
	</div>      
</div>	
<script type="text/javascript">
mini.parse();
	//获取当前tab
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var row=params.selectData;
	var url=window.location.search;
	var action=CommonUtil.getParam(url,"action");
	var form=new mini.Form("#field_form");

	var tradeData={};
	
	tradeData.selectData=row;
	tradeData.operType=action;
	tradeData.serial_no=row.execId;
	tradeData.task_id=row.taskId;
	//保存
	function save(){
		//表单验证！！！
		form.validate();
		if (form.isValid() == false) {
			return;
		}
		var userId ='<%=__sessionUser.getUserId()%>';
		var data=form.getData(true);
			if(data.status!="C"){
				data['status']="A";
			}
			data['userid']=userId;
			data['issuername']=mini.get("issuer").getText();
			var params=mini.encode(data);
			CommonUtil.ajax({
				url:"/IfsOpicsBondController/save",
				data:params,
				callback:function(data){
					mini.alert(data,'提示信息',function(){
						top["win"].closeMenuTab();
					});
				}
			});
	} 
	function close(){
		top["win"].closeMenuTab();
	}
	//英文、数字、下划线 的验证
	function onEnglishAndNumberValidation(e) {
		if(e.value == "" || e.value == null){//值为空，就不做校验
			return;
		}
		if (e.isValid) {
			if (isEnglishAndNumber(e.value) == false) {
				e.errorText = "必须输入英文+数字";
				e.isValid = false;
			}
		}
	}
	/* 是否英文+数字 */
	function isEnglishAndNumber(v) {
		var re = new RegExp("^[0-9a-zA-Z\_\-]+$");
		if (re.test(v)) return true;
		return false;
	}
	 /* 是否汉字 */
    function isChinese(v) {
        var re = new RegExp("^[\u4e00-\u9fa5]+$");
        if (re.test(v)) return true;
        return false;
    }
    function onChineseValidation(e) {
        if (e.isValid) {
            if (isChinese(e.value) == true) {
                e.errorText = "不能输入中文";
                e.isValid = false;
            }
        }
    }

	
	$(document).ready(function(){
		if($.inArray(action,["edit","detail"])>-1){
			form.setData(row);
			//债券交易要素组合
			mini.get("issuer").setValue(row.issuer);
			mini.get("issuer").setText(row.issuername);
			mini.get("bndcd").setEnabled(false);
			mini.get("acctngtype").setValue(row.acctngtype);
			mini.get("acctngtype").setText(row.acctngtype);
			mini.get("accountno").setValue(row.accountno);
			mini.get("accountno").setText(row.accountno);
			mini.get("product").setValue(row.product);
			mini.get("product").setText(row.product);
			mini.get("prodtype").setValue(row.prodtype);
			mini.get("prodtype").setText(row.prodtype);
			
			
			if(row.status!="A"){
				mini.get("bndcd").setEnabled(false);
				mini.get("descr").setEnabled(false);
				mini.get("acctngtype").setEnabled(false);
				mini.get("ccy").setEnabled(false);
				mini.get("basis").setEnabled(false);
				mini.get("secunit").setEnabled(false);
				mini.get("denom").setEnabled(false);
				mini.get("pubdt").setEnabled(false);
				mini.get("issuer").setEnabled(false);
				mini.get("settccy").setEnabled(false);
				mini.get("couponrate").setEnabled(false);
				mini.get("intpaymethod").setEnabled(false);
				mini.get("accountno").setEnabled(false);
				mini.get("intenddaterule").setEnabled(false);
				mini.get("issudt").setEnabled(false);
				mini.get("matdt").setEnabled(false);
				mini.get("intpaycircle").setEnabled(false);
				mini.get("product").setEnabled(false);
				mini.get("prodtype").setEnabled(false);
				mini.get("settdays").setEnabled(false);
				mini.get("intcalcrule").setEnabled(false);
				mini.get("br").setEnabled(false);
			}
			
			if($.inArray(action,["detail"])>-1){
				mini.get("save_btn").setVisible(false);
				form.setEnabled(false);	
			}
		}
	});
	function  onElementQuery(){
	    var btnEdit = this;
	    mini.open({
	        url : CommonUtil.baseWebPath() +"/bond/tradeElementParamMini.jsp",
	        title : "交易要素组合选择",
	        width : 900,
	        height : 600,
	        
	        onload: function () {//弹出页面加载完成
	            var iframe = this.getIFrameEl(); 
	            var data = {"element":"SECUR"};
	            iframe.contentWindow.setData(data); 
	            iframe.contentWindow.search();
	        },
	        ondestroy : function(action) {	
	            if (action == "ok") 
	            {
	                var iframe = this.getIFrameEl();
	                var data = iframe.contentWindow.GetData();
	                data = mini.clone(data); //必须
	                if (data) {
	                    btnEdit.setValue(data.element);
	                    btnEdit.setText(data.element);
	                    mini.get("intcalcrule").setValue(data.intcalcrule);//计息规则
	                    mini.get("prodtype").setValue(data.prodType);//产品类型
	                    mini.get("product").setValue(data.product);//产品
	                    btnEdit.focus();
	                }
	            }
	        }
	    });
	}
	//加载机构树
	function nodeclick(e) {
		var oldvalue=e.sender.value;
		var param = mini.encode({"branchId":branchId}); //序列化成JSON
		var oldData=e.sender.data;
		if(oldData==null||oldData==""){
			CommonUtil.ajax({
				url: "/InstitutionController/searchInstitutionByBranchId",
				data: param,
				callback: function (data) {
					var obj=e.sender;
					obj.setData(data.obj);
					obj.setValue(oldvalue);
				}
			});
		}
	}
	
	function onButtonEdit(e) {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Common/CustMini.jsp",
            title: "选择对手方列表",
            width: 900,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.cno);
                        btnEdit.setText(data.cname);
                        btnEdit.focus();
                    }
                }

            }
        });

    }
	function onActyQuery(e){
		var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Ifs/opics/actyMini.jsp",
            title: "选择会计类型",
            width: 900,
            height: 600,
            onload: function () {
                var iframe = this.getIFrameEl();
                var data = {type1:"5",type2:"9"};//把会计归属类型传到小页面，"2"表示归属债券类，"3"表示归属客户及债券类
                //console.log(data);
                iframe.contentWindow.SetData(data);
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.acctngtype);
                        btnEdit.setText(data.acctngtype);
                        btnEdit.focus();
                    }
                }

            }
        });
	}
	//托管机构选择
	function onSaccQuery(e) {
        var btnEdit = this;
        mini.open({
            url: "./Ifs/opics/saccMini.jsp",
            title: "选择托管机构列表",
            width: 900,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.accountno);
                        btnEdit.setText(data.accountno);
                        btnEdit.focus();
                    }
                }

            }
        });
    }
	
	
	function onBondChange(e){
		var bndcd=e.value;
		var param = mini.encode({"bndcd":bndcd}); //序列化成JSON
		CommonUtil.ajax({
			url:"/IfsOpicsBondController/isExist",
			data:param,
			callback:function(data){
				if(data.obj.bndcd!=null){
					mini.alert("债券信息在opics中已存在");
					mini.get("descr").setValue(data.obj.descr);
					mini.get("product").setValue(data.obj.product);
					mini.get("prodtype").setValue(data.obj.prodtype);
					mini.get("intcalcrule").setValue(data.obj.intcalcrule);
					mini.get("acctngtype").setValue(data.obj.acctngtype);
					mini.get("ccy").setValue(data.obj.ccy);
					mini.get("basis").setValue(data.obj.basis);
					mini.get("secunit").setValue(data.obj.secunit);
					mini.get("denom").setValue(data.obj.denom);
					mini.get("pubdt").setValue(data.obj.pubdt);
					mini.get("issuer").setValue(data.obj.issuer);
					mini.get("settccy").setValue(data.obj.settccy);
					mini.get("couponrate").setValue(data.obj.couprate);
					mini.get("intpaymethod").setValue(data.obj.intpaymethod);
					mini.get("accountno").setValue(data.obj.accountno);
					mini.get("intenddaterule").setValue(data.obj.intenddaterule);
					mini.get("issudt").setValue(data.obj.issudt);
					mini.get("matdt").setValue(data.obj.matdt);
					mini.get("intpaycircle").setValue(data.obj.intpaycircle);
					mini.get("status").setValue("C");
					mini.get("br").setValue("1");
					
					mini.get("bndcd").setEnabled(false);
					mini.get("descr").setEnabled(false);
					mini.get("acctngtype").setEnabled(false);
					mini.get("ccy").setEnabled(false);
					mini.get("basis").setEnabled(false);
					mini.get("secunit").setEnabled(false);
					mini.get("denom").setEnabled(false);
					mini.get("pubdt").setEnabled(false);
					mini.get("issuer").setEnabled(false);
					mini.get("settccy").setEnabled(false);
					mini.get("couponrate").setEnabled(false);
					mini.get("intpaymethod").setEnabled(false);
					mini.get("accountno").setEnabled(false);
					mini.get("intenddaterule").setEnabled(false);
					mini.get("issudt").setEnabled(false);
					mini.get("matdt").setEnabled(false);
					mini.get("intpaycircle").setEnabled(false);
					mini.get("product").setEnabled(false);
					mini.get("prodtype").setEnabled(false);
					mini.get("intcalcrule").setEnabled(false);
					mini.get("br").setEnabled(false);
					mini.get("settdays").setEnabled(false);
				}
			}
		});
	}
	
	
	/* 产品代码的选择 */
    function onPrdEdit(){
    	var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "/opics/prodMini.jsp",
            title: "选择产品代码",
            width: 900,
            height: 600,
            
            onload: function () {//弹出页面加载完成
	            var iframe = this.getIFrameEl(); 
	            var data = {"pcode":"SECUR"};
	            iframe.contentWindow.SetData(data); 
	        },
            
            
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.pcode);
                        btnEdit.setText(data.pcode);
                        btnEdit.focus();
                    }
                }

            }
        });
    }
	
    function onPrdChanged(){
    	mini.get("prodType").setValue("");
    	mini.alert("产品代码已改变，请重新选择产品类型!");
    }
    
    /* 产品类型的选择 */
    function onTypeEdit(){
    	var prd = mini.get("product").getValue();
    	if(prd == null || prd == ""){
    		mini.alert("请先选择产品代码!");
    		return;
    	}
    	var data={prd:prd};
    	var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "/opics/typeMini.jsp",
            title: "选择产品类型",
            width: 900,
            height: 600,
            onload: function () {
                var iframe = this.getIFrameEl();
                iframe.contentWindow.SetData(data);
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.type);
                        btnEdit.setText(data.type);
                        btnEdit.focus();
                    }
                }

            }
        });
    }
</script>	
</body>
</html>
