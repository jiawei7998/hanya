<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/hideBtn.js"></script>
<title>债券借贷</title>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
	<legend>债券信息查询</legend>	
	<div>
		<div id="search_form" style="width:80%" cols="6">
			<input id="dealno" name="dealno" class="mini-textbox" labelField="true"  label="交易单号：" width="280px" labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请输入交易单号" />
			<input id="bndcd" name="bndcd" class="mini-textbox" labelField="true"  label="债券代码：" width="280px" labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请输入债券代码" />
			<span style="float: right; margin-right: 150px"> 
				<a id="search_btn" class="mini-button" style="display: none"   onclick="search()">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
			</span>
		</div>
	</div>
</fieldset> 
	<a  id="checked_bk_btn" class="mini-button" style="display: none"    onclick="check()">复核</a>
	</span>	  
<div class="mini-fit" >
	<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id"  allowAlternating="true"
		onrowdblclick="onRowDblClick" allowResize="true" sortMode="client" allowAlternating="true">
		<div property="columns">
			<div type="indexcolumn" width="50px" headerAlign="center">序号</div>
			<div field="dealno" width="180px" align="center"  headerAlign="center" >交易单号</div>    
			<div field="status" width="120" align="center" headerAlign="center"  renderer="CommonUtil.dictRenderer" data-options="{dict:'dealBondStatus'}">交易状态</div>
			<div field="bndcd" width="150px" align="center"  headerAlign="center" >债券代码</div> 
			<div field="descr" width="150" allowSort="false" headerAlign="center" align="center">描述</div>                           
			<div field="bndnm_en" width="150px" align="center"  headerAlign="center" >债券英文名</div>                            
			<div field="bndnm_cn" width="180px" align="center"  headerAlign="center" >债券名称</div>
			<div field="ccy" width="120px" align="center"  headerAlign="center" allowSort="true" renderer="CommonUtil.dictRenderer" data-options="{dict:'Currency'}">债券币种</div>                                
			<div field="br" width="120px" align="center"  headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'Br'}">部门</div>    
			<div field="issudt" width="120px" align="center"  headerAlign="center" allowSort="true">起息日</div>
			<div field="matdt" width="120px" align="center"  headerAlign="center" allowSort="true">到息日</div>
			<div field="settccy" width="150px" align="center"  headerAlign="center" allowSort="true" renderer="CommonUtil.dictRenderer" data-options="{dict:'Currency'}">清算币种</div>
			<div field="issuername" width="150px" align="center"  headerAlign="center" allowSort="true">发行机构</div>
			<div field="pubdt" width="110px" align="center"  headerAlign="center" allowSort="true">发行日</div>
			<div field="denom" width="110px" align="center"  headerAlign="center" allowSort="true" renderer="CommonUtil.dictRenderer" data-options="{dict:'Denom'}">一手的量</div>
			<div field="basis" width="120" allowSort="false" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'Basis'}">计息基础</div>
			<div field="intenddaterule" width="120" allowSort="false" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'IntendRule'}">到期付息规则</div>
			<div field="intpaycircle" width="180" allowSort="false" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'Intpaycircle'}">付息周期</div>
			<div field="couponrate" width="120px" align="center"  headerAlign="center" allowSort="true" numberFormat="#,0.00">票面利率(%)</div>
			<div field="paramt" width="120px" align="center"  headerAlign="center" allowSort="true" numberFormat="#,0.00">发行价</div>
			<div field="intcalcrule" width="150px" align="center"  headerAlign="center" allowSort="true" renderer="CommonUtil.dictRenderer" data-options="{dict:'IntcalRule'}">计息规则</div>
			<div field="product" width="150px" align="center"  headerAlign="center" allowSort="true">产品</div>
			<div field="prodtype" width="110px" align="center"  headerAlign="center" allowSort="true">产品类型</div>
			<div field="bondratingclass" width="110px" align="center"  headerAlign="center" allowSort="true" renderer="CommonUtil.dictRenderer" data-options="{dict:'RatingClass'}">评级类别</div>
			<div field="bondratingagency" width="120" allowSort="false" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'RatingAgency'}">评级机构</div>
			<div field="bondrating" width="120" allowSort="false" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'Rating'}">评级</div>
			<div field="bondproperties" width="150" allowSort="false" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'BondPro'}">债券性质</div>
			<div field="secunit" width="120px" align="center"  headerAlign="center" allowSort="true" renderer="CommonUtil.dictRenderer" data-options="{dict:'Secunit'}">债券单元</div>
			<div field="accountno" width="120px" align="center"  headerAlign="center" allowSort="true" renderer="CommonUtil.dictRenderer" data-options="{dict:'AccountNo'}">托管账户</div>
			<div field="intpaymethod" width="150px" align="center"  headerAlign="center" allowSort="true" renderer="CommonUtil.dictRenderer" data-options="{dict:'IntendRule'}">付息方式</div>
			<div field="paragraphfirstplan" width="150px" align="center"  headerAlign="center" allowSort="true">首次划款日</div>
			<div field="floatratemark" width="110px" align="center"  headerAlign="center" allowSort="true">浮动利率基准</div>
			<div field="issuevol" width="110px" align="center"  headerAlign="center" allowSort="true">实际发行量</div>
			<div field="bondperiod" width="120" allowSort="false" headerAlign="center" align="center">债券期限</div>
			<div field="basicspread" width="120" allowSort="false" headerAlign="center" align="center">基本利差</div>
			<div field="inputdate" width="180" allowSort="true" headerAlign="center" align="center">更新时间</div>
		</div>
	</div>
</div>   

<script>
	mini.parse();
	var url = window.location.search;
	var prdNo = CommonUtil.getParam(url, "prdNo");
	var prdName = CommonUtil.getParam(url, "prdName");
	var prodtype = CommonUtil.getParam(url, "prodtype");
	var form = new mini.Form("#search_form");
	var grid=mini.get("datagrid");
	var row="";

	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		searchs(pageSize,pageIndex);
	});

	//查看详情
	function onRowDblClick(e) {
		var url = CommonUtil.baseWebPath() +"/bond/bondCheckEdit.jsp?action=detail";
		var tab = {id: "BCDetail",name:"BCDetail",url:url,title:"债券信息详情",
					parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
		var paramData = {selectData:grid.getSelected()};
		CommonUtil.openNewMenuTab(tab,paramData);
	}
	//清空
	function clear(){
		var form=new mini.Form("search_form");
		form.clear();
		search();
	}
	//债券信息复核
	function check(){
		var row=grid.getSelected();
		if(row){
			var url = CommonUtil.baseWebPath() +"/bond/bondCheckEdit.jsp?action=check";
			var tab = {id: "BCCheck",name:"BCCheck",url:url,title:"债券信息复核",
						parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
			var paramData = {selectData:grid.getSelected()};
			CommonUtil.openNewMenuTab(tab,paramData);
		}else{
			mini.alert("请选择一条记录","提示");
		}
	}
	//查询 按钮
	function search(){
		searchs(grid.pageSize,0);
	}

	function searchs(pageSize,pageIndex){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统也提示");
			return;
		}
		var userId ='<%=__sessionUser.getUserId()%>';
		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		data['status']="A";
		data['userid']=userId;
		data['prodtype']=prodtype;
		var url="/IfsOpicsBondController/searchPageCheckTrd";
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:url,
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	$(document).ready(function() {
		 searchs(10, 0);
	});
</script>
</body>
</html>
