<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<title>现金流</title>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
	<legend>现金流查询</legend>	
	<div>
		<div id="search_form" style="width:80%" cols="6">
			<!-- <input id="dealno" name="dealno" class="mini-textbox" labelField="true"  label="交易单号：" width="280px" labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请输入交易单号" /> -->
			<input id="bndcd" name="bndcd" class="mini-textbox" labelField="true"  label="债券代码：" width="280px" labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请输入债券代码" />
		<!-- 	<input id="status" name="status" class="mini-combobox" data="CommonUtil.serverData.dictionary.opicsBondStatus" width="280px" emptyText="请选择交易状态" labelField="true"  label="交易状态：" labelStyle="text-align:right;"/> -->
			<span style="float: right; margin-right: 150px"> 
				<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
			</span>
		</div>
	</div>
</fieldset> 
	<span style="margin: 2px; display: block;"> 
	<!-- <a  id="edit_btn" class="mini-button" style="display: none"   onclick="edit()">修改</a> -->
	</span>	  
<div class="mini-fit" >
	<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id"  allowAlternating="true"
		onrowdblclick="onRowDblClick" allowResize="true" sortMode="client" allowAlternating="true">
		<div property="columns">
			<div type="indexcolumn" width="50px" headerAlign="center">序号</div>
			<div field="secId" width="150px" align="center"  headerAlign="center" >债券代码</div> 
			<div field="seq" width="180px" allowSort="false" headerAlign="center" align="center">序列号</div>   
			<div field="ipayDate" width="180px" align="center"  headerAlign="center" >利息支付日期</div>    
			<div field="callPrice_8" width="120" align="center" headerAlign="center" >看涨价格</div>
			<div field="cashFlow_8" width="150px" align="center"  headerAlign="center" >现金流数量</div>                            
			<div field="intRate_8" width="180px" align="center"  headerAlign="center" >利率</div>
			<div field="exdiVdate" width="120px" align="center"  headerAlign="center" allowSort="true" >除息日</div>                                
			<div field="intPayamt_8" width="120px" align="center"  headerAlign="center">利息金额</div>    
			<div field="intEnddte" width="120px" align="center"  headerAlign="center" allowSort="true">息期结束日期</div>
			<div field="intStrtdte" width="120px" align="center"  headerAlign="center" allowSort="true">息期开始日期</div>
			<div field="caprate_8" width="150px" align="center"  headerAlign="center" allowSort="true" >利率上限</div>
			<div field="rateCode" width="150px" align="center"  headerAlign="center" allowSort="true">利率代码</div>
			<div field="rateFixDate" width="110px" align="center"  headerAlign="center" allowSort="true">利率固定日期</div>
			<div field="lstmntDate" width="110px" align="center"  headerAlign="center" allowSort="true" >最后维护日期</div>
			<div field="prinamt_8" width="120" allowSort="false" headerAlign="center" align="center">本金</div>
			<div field="prinPayamt_8" width="120" allowSort="false" headerAlign="center" align="center" >主要付款金额</div>
			<div field="putPrice_8" width="180" allowSort="false" headerAlign="center" align="center" >看跌价格</div>
			<div field="basis" width="120px" align="center"  headerAlign="center">基准代码</div>
			<div field="notPrinamt_8" width="120px" align="center"  headerAlign="center"numberFormat="#,0.00">名义本金额</div>
			<div field="notPrinPayamt_8" width="150px" align="center"  headerAlign="center" allowSort="true" >名义本金付款金额</div>
			<div field="notIntPayamt_8" width="150px" align="center"  headerAlign="center" allowSort="true">名义利息付款金额</div>
			<div field="updateCounter" width="110px" align="center"  headerAlign="center" allowSort="true">更新次数</div>
		</div>
	</div>
</div>   

<script>
	mini.parse();
	var url = window.location.search;
	var prdNo = CommonUtil.getParam(url, "prdNo");
	var prdName = CommonUtil.getParam(url, "prdName");
	var form = new mini.Form("#search_form");
	var grid=mini.get("datagrid");
	var row="";

	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});
	grid.on("select",function(e){
		var row=e.record;
		mini.get("delete_btn").setEnabled(true);
		mini.get("add_btn").setEnabled(true);
		mini.get("import_bk_btn").setEnabled(true);
		mini.get("sync_btn").setEnabled(true);
		if( row.status == "B" || row.status == "C" || row.status == "D"){//已发送：B   opics处理成功:C  opics处理失败:D
		  mini.get("delete_btn").setEnabled(false);
		  mini.get("sync_btn").setEnabled(false);
		}
	});
	
	//清空
	function clear(){
		var form=new mini.Form("search_form");
		form.clear();
		search();
	}
	
	//查询 按钮
	function query() {
            search(grid.pageSize, 0);
        }
	
	function search(pageSize,pageIndex){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统也提示");
			return;
		}
		var userId ='<%=__sessionUser.getUserId()%>';
		var data=form.getData(true);
		//console.log(data);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		data['userid']=userId;
		var url="/CashFlowStatementController/searchSceId";
		var params = mini.encode(data);
		//console.log(params);
		CommonUtil.ajax({
			url:url,
			data:params,
			callback : function(data) {
				//console.log(data);
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	 $(document).ready(function() {
		 search(10, 0); 
	}); 
</script>
</body>
</html>
