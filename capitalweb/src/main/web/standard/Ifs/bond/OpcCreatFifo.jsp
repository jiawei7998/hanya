<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
    <head>
        <title>债券交易信息</title>
        <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    </head>
<body style="width:100%;height:100%;background:white">
   <fieldset class="mini-fieldset title">
    <legend>债券交易信息查询</legend>
    <div id="search_form" style="width: 100%;">
        <input id="pdealno" name="pdealno" class="mini-textbox" labelField="true" label="买入单号：" labelStyle="text-align:right;" emptyText="请输入买入单号" />
        <input id="sdealno" name="sdealno" class="mini-textbox" labelField="true" label="卖出单号：" labelStyle="text-align:right;" emptyText="请输入卖出单号" />
        
        <span style="float:right;margin-right: 150px">
            <a class="mini-button" style="display: none"  id="search"  onclick="search()">查询</a>
            <a class="mini-button" style="display: none"  id="clear_btn"  onclick="clear()">清空</a>
        </span>
    </div>
    </fieldset>
    
    <div class="mini-fit" style="width:100%;height:100%;">
        <div id="data_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" 
            sortMode="client" allowAlternating="true"   idField="userId" onrowdblclick="onRowDblClick" 
            border="true" allowResize="true" >
            <div property="columns">
                <div type="indexcolumn" headerAlign="center" align="center" width="40px">序列</div>
                <div field="br" headerAlign="center"  align="center" width="100px" >机构号</div>
                <div field="pdealno" headerAlign="center"  width="140px">买入交易号</div>
                <div field="cost" headerAlign="center"  width="220px"> 成本中心</div>
                <div field="ccy" headerAlign="center" width="120px">币种</div> -->
                <div field="invtype" headerAlign="center" width="100px" >发行人会计类型</div>
                <div field="port" headerAlign="center" width="180px" >投资类型</div>
                <div field="secid" headerAlign="center"  width="40px">债券</div>
                <div field="purchasedate" headerAlign="center"  width="120px">买入时间</div>
                <div field="pamount" headerAlign="center"  width="120px">买入金额</div>
                <div field="sdealno" headerAlign="center"  width="120px">卖出单号</div>
                <div field="saledate" headerAlign="center"  width="80px">卖出金额</div>
                <div field="samount" headerAlign="center"  width="80px">卖出时间</div>
               
            </div>
        </div>
    </div>
</body>
<script>
    mini.parse();
    var grid = mini.get("data_grid");
    grid.on("beforeload", function(e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        searchs(pageSize, pageIndex);
    });

    $(document).ready(function() {
    	search();
	});

	function setData(data){
    	var form = new mini.Form("#search_form");
    	form.setData(data);
    }
    
    function searchs(pageSize, pageIndex) {
        var form = new mini.Form("#search_form");
        form.validate();
        if (form.isValid() == false) {
            mini.alert("表单填写错误,请确认!", "提示信息");
            return;
        }

        var data = form.getData();
        
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        data['branchId']=branchId;
        var params = mini.encode(data);
        CommonUtil.ajax({
            url : "/IfsFifoController/searchSecm",
            data : params,
            callback : function(data) {
                var grid = mini.get("data_grid");
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }
  //查询按钮
    function search(){
        searchs(grid.pageSize,0);
    }
    //清空
    function clear(){
        var form=new mini.Form("search_form");
        form.clear();
        search();
    }

    function GetData() {
        var grid = mini.get("data_grid");
        var row = grid.getSelected();
        return row;
    }
    
    //双击行选择
    function onRowDblClick(){
    	var row = grid.getSelected();
    	var cust=row.cust;
    	var intpaycicle=row.intpaycicle;
    	var couponprod=row.couponprod;
    	var bondproperties=row.bondproperties;
    	
//    	if(cust==null||cust==""){
//    		mini.alert("请先映射发行人对应opics交易对手信息!", "提示信息");
//    		return;
//    	}
//    	if(couponprod=="附息"){
//    		if(intpaycicle==null||intpaycicle==""){
//        		mini.alert("请先映射付息周期对应opics信息!", "提示信息");
//        		return;
//        	}
//    	}
//    	if(bondproperties==null||bondproperties==""){
//    		mini.alert("请先映射证券分类对应opics信息!", "提示信息");
//    		return;
//    	}
    	
    	onOk();
        
    }

    function CloseWindow(action) {
        if (window.CloseOwnerWindow)
            return window.CloseOwnerWindow(action);
        else
            window.close();
    }

    function onOk() {
        CloseWindow("ok");
    }

    //关闭窗口
    function onCancel() {
        CloseWindow("cancel");
    }
    
</script>
</html>