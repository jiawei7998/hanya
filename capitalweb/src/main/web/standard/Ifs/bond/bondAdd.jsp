<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/hideBtn.js"></script>
<title>债券信息查询</title>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
	<legend>债券信息查询</legend>	
	<div>
		<div id="search_form" style="width:80%" cols="6">
			<input id="dealno" name="dealno" class="mini-textbox" width="320px" labelField="true"  label="交易单号：" labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请输入交易单号" />
			<input id="bndcd" name="bndcd" class="mini-textbox" width="320px" labelField="true"  label="债券代码："  labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请输入债券代码" />
			<input id="startDate" name="startDate" class="mini-datepicker" labelField="true" label="发行日期："
				   ondrawdate="onDrawDateStart" labelStyle="text-align:right;" emptyText="起始日期" format="yyyy-MM-dd"/>
			<span>~</span>
			<input id="endDate" name="endDate" class="mini-datepicker"
				   ondrawdate="onDrawDateEnd" emptyText="结束日期" format="yyyy-MM-dd"/>
			</nobr>
			<input id="issuer" name="issuer" class="mini-buttonedit mini-mustFill" labelField="true"
				   onbuttonclick="onButtonEdit" label="发行人："
				   labelStyle="text-align:right;" width="320px" />
			<input id="intcalcrule" name="intcalcrule" class="mini-combobox" width="320px" data="CommonUtil.serverData.dictionary.IntcalRule"  emptyText="请选择计算规则" labelField="true"  label="计算规则：" labelStyle="text-align:right;"/>
			<input id="status" name="status" class="mini-combobox" width="320px" data="CommonUtil.serverData.dictionary.opicsBondStatus"  emptyText="请选择交易状态" labelField="true"  label="交易状态：" labelStyle="text-align:right;"/>
			<span style="float: right; margin-right: 100px"> 
				<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
			</span>
		</div>
	</div>
	<span style="margin: 2px; display: block;">
	<a  id="add_btn" class="mini-button" style="display: none"   onclick="add()">添加</a>
	<a  id="edit_btn" class="mini-button" style="display: none"   onclick="edit()">修改</a>
	<a  id="delete_btn" class="mini-button" style="display: none"   onclick="del()">删除</a>
	<a  id="import_bk_btn" class="mini-button" style="display: none"    onclick="bondImport()">导入</a>
		<!-- <a  id="check_bk_btn" class="mini-button" style="display: none"    onclick="checkLog()">复核日志</a> -->
	<a  id="sync" class="mini-button" style="display: none"   onclick="sync()">同步至OPICS</a>
	<a class="mini-button" style="display: none"  id="calib"  onClick="calibration();">批量校准信息</a>
		<span style="color: red">静态参数以Opics为准，请在opics中添加完成后到前置进行数据批量较准操作</span>
	</span>
</fieldset> 

<div class="mini-fit" >
	<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id"  allowAlternating="true"
		onrowdblclick="onRowDblClick" allowResize="true" sortMode="client" allowAlternating="true">
		<div property="columns">
			<div type="indexcolumn" width="50px" headerAlign="center">序号</div>
			<div field="bndcd" width="150px" align="center"  headerAlign="center" >债券代码</div>
			<div field="dealno" width="180px" align="center"  headerAlign="center" >交易单号</div>    
			<div field="status" width="120" align="center" headerAlign="center"  renderer="CommonUtil.dictRenderer" data-options="{dict:'dealBondStatus'}">交易状态</div>
			<div field="descr" width="300px" allowSort="false" headerAlign="center" align="center">描述</div>
			<div field="bndnm_en" width="200px" align="center"  headerAlign="center" >债券简称</div>
			<div field="bndnm_cn" width="350px" align="center"  headerAlign="center" >债券全称</div>
			<div field="ccy" width="120px" align="center"  headerAlign="center" allowSort="true" renderer="CommonUtil.dictRenderer" data-options="{dict:'Currency'}">债券币种</div>
			<div field="settccy" width="150px" align="center"  headerAlign="center" allowSort="true" renderer="CommonUtil.dictRenderer" data-options="{dict:'Currency'}">结算币种</div>
			<div field="settdays" width="100px" align="center"  headerAlign="center" allowSort="true">结算天数</div>
<%--			<div field="br" width="120px" align="center"  headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'Br'}">部门</div>    --%>
			<div field="issudt" width="120px" align="center"  headerAlign="center" allowSort="true">起息日</div>
			<div field="matdt" width="120px" align="center"  headerAlign="center" allowSort="true">到息日</div>
			<div field="issuername" width="300px" align="center"  headerAlign="center" allowSort="true">发行人</div>
			<div field="pubdt" width="110px" align="center"  headerAlign="center" allowSort="true">发行日期</div>
			<div field="issprice" width="120px" align="right"  headerAlign="center" allowSort="true" numberFormat="#,0.00">面值</div>
<%--			<div field="issuevol" width="110px" align="center"  headerAlign="center" allowSort="true">实际发行数量(亿元)</div>--%>
			<div field="denom" width="110px" align="right"  headerAlign="center" allowSort="true" renderer="CommonUtil.dictRenderer" data-options="{dict:'Denom'}">单价</div>
			<div field="basis" width="120" allowSort="false" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'Basis'}">计息方式</div>
			<div field="intenddaterule" width="120" allowSort="false" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'IntendRule'}">利息结束日规则</div>
			<div field="intpaycircle" width="180" allowSort="false" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'Intpaycircle'}">支付频率</div>
			<div field="couponrate" width="120px" align="center"  headerAlign="center" allowSort="true" numberFormat="#,0.00">票面利率(%)</div>
			<div field="intcalcrule" width="150px" align="center"  headerAlign="center" allowSort="true" renderer="CommonUtil.dictRenderer" data-options="{dict:'IntcalRule'}">计算规则</div>
			<div field="product" width="200px" align="center"  headerAlign="center" allowSort="true">产品代码</div>
			<div field="prodtype" width="110px" align="center"  headerAlign="center" allowSort="true">产品类型</div>
			<div field="bondratingclass" width="110px" align="center"  headerAlign="center" allowSort="true" renderer="CommonUtil.dictRenderer" data-options="{dict:'RatingClass'}">评级类别</div>
			<div field="bondratingagency" width="120" allowSort="false" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'RatingAgency'}">评级机构</div>
			<div field="bondrating" width="120" allowSort="false" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'Rating'}">评级结果</div>
			<div field="bondproperties" width="250" allowSort="false" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'BondPro'}">标准行业代码</div>
			<div field="secunit" width="120px" align="center"  headerAlign="center" allowSort="true" renderer="CommonUtil.dictRenderer" data-options="{dict:'Secunit'}">股权类型</div>
			<div field="accountno" width="120px" align="center"  headerAlign="center" allowSort="true">托管机构</div>
			<div field="intpaymethod" width="150px" align="center"  headerAlign="center" allowSort="true" renderer="CommonUtil.dictRenderer" data-options="{dict:'IntendRule'}">利息支付规则</div>
			<div field="paragraphfirstplan" width="150px" align="center"  headerAlign="center" allowSort="true">第一个息票日期</div>
			<div field="floatratemark" width="110px" align="center"  headerAlign="center" allowSort="true">利率代码</div>
			<div field="bondperiod" width="120" allowSort="false" headerAlign="center" align="center">债券期限（天）</div>
			<div field="bongRightDebtDate" width="120" allowSort="false" headerAlign="center" align="center">债权债务登记日</div>
			<div field="reissueFrequency" width="120" allowSort="false" headerAlign="center" align="center">续发次数</div>
			<div field="finalInvestmentType" width="120" allowSort="false" headerAlign="center" align="center">最终投向类型</div>
			<div field="finalInvestmentIndustry" width="120" allowSort="false" headerAlign="center" align="center">最终投向行业</div>
			<div field="issueEnterpriseScale" width="120" allowSort="false" headerAlign="center" align="center">发行人企业规模</div>
			<div field="investmentFundsUse" width="120" allowSort="false" headerAlign="center" align="center">投资金用途</div>

		<%--			<div field="basicspread" width="120" allowSort="false" headerAlign="center" align="center">基本利差</div>--%>
<%--			<div field="inputdate" width="180" allowSort="true" headerAlign="center" align="center">更新时间</div>--%>
		</div>
	</div>
</div>   

<script>
	mini.parse();

	var url = window.location.search;
	var prdNo = CommonUtil.getParam(url, "prdNo");
	var prdName = CommonUtil.getParam(url, "prdName");
	var form = new mini.Form("#search_form");
	var grid=mini.get("datagrid");
	var row="";

	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});
	grid.on("select",function(e){
		var row=e.record;
		/* mini.get("edit_btn").setEnabled(true); */
		mini.get("delete_btn").setEnabled(true);
		mini.get("add_btn").setEnabled(true);
		mini.get("import_bk_btn").setEnabled(true);
		/* mini.get("check_bk_btn").setEnabled(true); */
		mini.get("sync").setEnabled(true);
		if( row.status == "B" || row.status == "C" || row.status == "D"){//已发送：B   opics处理成功:C  opics处理失败:D
		  /* mini.get("edit_btn").setEnabled(false); */
		  mini.get("delete_btn").setEnabled(false);
		  mini.get("sync").setEnabled(false);
		}
		/* if( row.status == "A"){
			mini.get("check_bk_btn").setEnabled(false);
		} */
		});
	//交易日期
	function onDrawDateStart(e) {
		var startDate = e.date;
		var endDate= mini.get("endDate").getValue();
		if(CommonUtil.isNull(endDate)){
			return;
		}
		if (endDate.getTime() < startDate.getTime()) {
			e.allowSelect = false;
		}
	}
	function onDrawDateEnd(e) {
		var endDate = e.date;
		var startDate = mini.get("startDate").getValue();
		if(CommonUtil.isNull(startDate)){
			return;
		}
		if (endDate.getTime() < startDate.getTime()) {
			e.allowSelect = false;
		}
	}
	function onButtonEdit(e) {
		var btnEdit = this;
		mini.open({
			url: CommonUtil.baseWebPath() + "../../credit/limit/LimitCustMini.jsp",
			title: "选择对手方列表",
			width: 900,
			height: 600,
			ondestroy: function (action) {
				if (action == "ok") {
					var iframe = this.getIFrameEl();
					var data = iframe.contentWindow.GetData();
					data = mini.clone(data);    //必须
					if (data) {
						btnEdit.setValue(data.cno);
						btnEdit.setText(data.cliname);
						btnEdit.focus();
					}
				}

			}
		});

	}
	//债券信息导入
	function bondImport(){
		mini.open({
			showModal: true,
		    allowResize: true,
			url: CommonUtil.baseWebPath() +"/bond/MiniBondImport.jsp",
			width: 420,
			height: 300,
			title: "文件上传",
			ondestroy: function (action) {
				search(10,0);//重新加载页面
			}
		});
	}
	//查看详情
	function onRowDblClick(e) {
		var url = CommonUtil.baseWebPath() +"/bond/bondAddEdit.jsp?action=detail";
		var tab = {id: "BADetail",name:"BADetail",url:url,title:"债券信息详情",
					parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
		var paramData = {selectData:grid.getSelected()};
		CommonUtil.openNewMenuTab(tab,paramData);
	}
	//清空
	function clear(){
		var form=new mini.Form("search_form");
		form.clear();
		search(grid.pageSize, 0);
	}
	//添加
	function add(){
		var url = CommonUtil.baseWebPath() +"/bond/bondAddEdit.jsp?action=add";
		var tab = {id: "BAAdd",name:"BAAdd",url:url,title:"债券信息添加",
					parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
		var paramData = {selectData:""};
		CommonUtil.openNewMenuTab(tab,paramData);
	}
	//修改
	function edit(){
		var row=grid.getSelected();
		if(row){
		var url = CommonUtil.baseWebPath() +"/bond/bondAddEdit.jsp?action=edit";
		var tab = {id: "BAEdit",name:"BAEdit",url:url,title:"债券信息修改",
					parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
		var paramData = {selectData:row};
		CommonUtil.openNewMenuTab(tab,paramData);
	}else{
		mini.alert("请选择一条记录","提示");
	}
	}
	//删除
	function del(){
		   var row = grid.getSelected();
	        if (row) {
	            mini.confirm("您确认要删除选中记录?","系统警告",function(value){
	                if(value=="ok"){
	                    CommonUtil.ajax({
	                        url: "/IfsOpicsBondController/delete",
	                        data: {bndcd: row.bndcd},
	                        callback: function (data) {
	                            if (data.code == 'error.common.0000') {
	                                mini.alert("删除成功!");
	                                grid.reload();
	                            } else {
	                                mini.alert("删除失败!");
	                            }
	                        }
	                    });
	                }
	            });
	        }else {
	            mini.alert("请选中一条记录！", "消息提示");
	        }
	}
	//查询 按钮
	function query() {
            search(grid.pageSize, 0);
        }
	//复核日志
	function checkLog(){
		var row=grid.getSelecteds();
		if(row.length==1){
		var url = CommonUtil.baseWebPath() +"/bond/bondCheckLog.jsp?action=edit";
		var tab = {id: "BCLog",name:"BCLog",url:url,title:"复核日志",
					parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
		var paramData = {selectData:row};
		CommonUtil.openNewMenuTab(tab,paramData);
		}else{
			mini.alert("请选择一条记录","提示");
		}
	}
	function search(pageSize,pageIndex){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统也提示");
			return;
		}
		var userId ='<%=__sessionUser.getUserId()%>';
		var data=form.getData(true);
		//console.log(data);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		data['userid']=userId;
// 		data['prodtype']="SD";
		var url="/IfsOpicsBondController/searchPageBondTrd";
		var params = mini.encode(data);
		//console.log(params);
		CommonUtil.ajax({
			url:url,
			data:params,
			callback : function(data) {
				//console.log(data);
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	//债券信息同步
	function sync(){
		var rows=grid.getSelecteds();
		if(rows.length==0){
			mini.alert("请选中一行","提示");
			return;
		}
		mini.confirm("您确认要同步该债券信息?","系统警告",function(value){   
			if (value=='ok'){   
				var data=rows[0];
				params=mini.encode(data);
				CommonUtil.ajax( {
					url:"/IfsOpicsBondController/syncOpicsBond",
					data:params,
					callback : function(data) {
						mini.alert(data.code,data.desc);
						grid.reload();
					}
				});
			}
		});
	}
	//批量校准信息
    function calibration() {
		var bndcd1 = mini.get("bndcd").getValue();
		if(bndcd1!=null && bndcd1!=""){
			mini.confirm("您确认要校准输入的债券?","系统警告",function(value){
    			if(value=="ok"){
    				CommonUtil.ajax({
    		             url: "/IfsOpicsBondController/batchCalibration",
    		             data: {bndcd:bndcd1,type:2},
    		             callback: function (data) {
    		            	 mini.alert(data.obj.retMsg,data.obj.retCode);
    		            	 grid.reload();
    		             }
    		         });
    			}
    		});
		}else{
			var rows = grid.getSelecteds();
	    	if(rows.length > 0){ //选中行校准
	    		var str = "";
	    		if(rows.length == 1){
	    			str = rows[0].bndcd;
	    		} else {
	    			for(var i=0;i<rows.length;i++){
	    				if(i==0){
	    					str = rows[i].bndcd + ",";
	    				} else {
	    					if(i==rows.length-1){
	    						str = str + rows[i].bndcd;
	    					} else {
	    						str = str + rows[i].bndcd + ",";
	    					}
	    				}
	    			}
	    		}
	    		mini.confirm("您确认要校准选中记录?","系统警告",function(value){
	    			if(value=="ok"){
	    				CommonUtil.ajax({
	    		             url: "/IfsOpicsBondController/batchCalibration",
	    		             data: {bndcd:str,type:1},
	    		             callback: function (data) {
	    		            	 mini.alert(data.obj.retMsg,data.obj.retCode);
	    		            	 grid.reload();
	    		             }
	    		         });
	    			}
	    		});
	    	} else { //未选中就全部校准
	    		mini.confirm("您确认要校准全部记录?","系统警告",function(value){
	    			if(value=="ok"){
	    				CommonUtil.ajax({
	    		             url: "/IfsOpicsBondController/batchCalibration",
	    		             data: {type:3},
	    		             callback: function (data) {
	    		            	 mini.alert(data.obj.retMsg,data.obj.retCode);
	    		            	 grid.reload();
	    		             }
	    		         });
	    			}
	    		});
	    	}
		}
    }
	
	
	$(document).ready(function() {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
			search(10, 0);
		});
	});
</script>
</body>
</html>
