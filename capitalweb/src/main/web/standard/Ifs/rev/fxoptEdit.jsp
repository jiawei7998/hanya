<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
  <%@ include file="../../global.jsp"%>
    <html>

    <head>
      <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
      
    <title>存续期管理台 期权类</title>
    </head>

    <body style="width:100%;height:100%;background:white">
      <div class="mini-splitter" style="width:100%;height:100%;">
        <div size="90%" showCollapseButton="false">
            <div id="field_form" class="mini-panel" title="期权类冲销信息" style="width:100%" allowResize="true" collapseOnTitleClick="true">
              <div class="leftarea">
                  <input id="sponsor" name="sponsor" class="mini-hidden" />
                  <input id="sponInst" name="sponInst" class="mini-hidden" />
                  <input id="ticketId" name="ticketId" enabled="false" class="mini-textbox" label="审批单流水号：" labelField="true" emptyText="系统自动生成" required="true" vtype="maxLength:35" style="width:100%" labelStyle="text-align:left;width:170px" />
                  <input id="fedealno" name="fedealno" class="mini-textbox" enabled="false" label="交易前端流水号：" emptyText="请输入交易前端流水号" labelField="true" style="width:100%" labelStyle="text-align:left;width:170px" />
                  <input id="adate" name="adate" class="mini-datepicker" label="冲销日期：" required="true"  emptyText="请输入冲销日期" labelField="true"style="width:100%" labelStyle="text-align:left;width:170px" vtype="maxLength:35" />
                  <input id="revreason" name="revreason" class="mini-textarea" label="冲销原因：" emptyText="请输入冲销原因" labelField="true"style="width:100%" labelStyle="text-align:left;width:170px" />
            </div>
              <div class="rightarea">
                  <input style="width:100%;" id="dealType" name="dealType" enabled="false" class="mini-combobox" labelField="true"  label="业务类型："  data="CommonUtil.serverData.dictionary.DEALTYPE" labelStyle="text-align:left;width:170px" />
                  <input id="dealNo" name="dealNo" enabled="false" class="mini-textbox" label="OPICS交易号：" emptyText="请输入OPICS交易号" labelField="true"style="width:100%" labelStyle="text-align:left;width:170px" />
                  <input id="br" name="br" class="mini-combobox" label="部门："  data="[{id:'01',text:'01'}]" emptyText="请选择部门" labelField="true" style="width:100%" labelStyle="text-align:left;width:170px" vtype="maxLength:50" required="true" />
                </div>
            <%@ include file="../../Common/Flow/MiniApproveOpCommon.jsp"%>
            </div>
          <div  id="field_form1"  class="mini-fit area" style="background:white" >
					<div class="mini-panel" title="人民币期权详情" style="width:100%;"  allowResize="true" collapseOnTitleClick="true">
					<div class="leftarea">
						<input style="width:100%;" id="contractId" name="contractId" class="mini-textbox mini-mustFill" labelField="true"required="true"  label="成交单编号：" labelStyle="text-align:left;width:130px;" onvalidation="onEnglishAndNumberValidations" vtype="maxLength:20"/>
					</div>
					<div class="rightarea">
						<input style="width:100%;" id="delta" name="delta" class="mini-textbox mini-mustFill" labelField="true"  required="true"  label="Delta交换交易号：" labelStyle="text-align:left;width:130px;" onvalidation="onEnglishAndNumberValidations" vtype="maxLength:20"/>
					</div>
				</div>
			<div class="mini-panel" title="成交双方信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
				<div class="leftarea" >
					<input id="instId" name="instId" class="mini-textbox" labelField="true"  label="本方机构：" maxLength="25"	 style="width:100%;"  labelStyle="text-align:left;width:130px;" />
					<input id="dealer" name="dealer" class="mini-textbox" labelField="true"  label="本方交易员：" maxLength="20"	 labelStyle="text-align:left;width:130px;" style="width:100%;"   />
					<input id="forDate" name="forDate" class="mini-datepicker" labelField="true"  label="交易日期："  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
				</div>
				<div class="rightarea">
					<input id="counterpartyInstId" name="counterpartyInstId" class="mini-textbox" labelField="true"  label="对方机构：" onbuttonclick="onButtonEdit" maxLength="25"	 style="width:100%;"  labelStyle="text-align:left;width:130px;" />
					<input id="counterpartyDealer" name="counterpartyDealer" class="mini-textbox" labelField="true"  label="对方交易员：" maxLength="20"	 labelStyle="text-align:left;width:130px;" style="width:100%;"   />
					<input id="forTime" name="forTime" class="mini-timespinner" labelField="true"  label="交易时间："  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
				</div>
			</div>		
			<%@ include file="../../Common/opicsDetail.jsp"%>
			<div class="mini-panel" title="交易主体信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
				<div class="leftarea">	
					<input style="width:100%" id="currencyPair" name="currencyPair" class="mini-combobox" labelField="true"  label="货币对："   data="CommonUtil.serverData.dictionary.CurrencyPair" labelStyle="text-align:left;width:130px;" />
					<input style="width:100%" id="tradingType" name="tradingType" class="mini-textbox" labelField="true"  label="交易类型：" maxLength="20"  labelStyle="text-align:left;width:130px;"   />
					<input style="width:100%" id="buyNotional" name="buyNotional" class="mini-combobox" labelField="true"  label="名义本金方向：" data="CommonUtil.serverData.dictionary.trading"  labelStyle="text-align:left;width:130px;"   />
					<input style="width:100%" id="sellNotional" name="sellNotional" class="mini-combobox" labelField="true"  label="名义本金方向：" data="CommonUtil.serverData.dictionary.trading"   labelStyle="text-align:left;width:130px;"   />
					<input style="width:100%" id="tenor" name="tenor" class="mini-combobox" labelField="true"  label="期限："  data="CommonUtil.serverData.dictionary.Term"  labelStyle="text-align:left;width:130px;" />
					<input style="width:100%" id="premiumDate" name="premiumDate" class="mini-datepicker" labelField="true"  label="期权费交付日："  ondrawdate="compareDate1"  labelStyle="text-align:left;width:130px;"   />
					<input style="width:100%" id="premiumRate" name="premiumRate" class="mini-spinner" labelField="true"  label="期权费率：" changeOnMousewheel='false' maxValue="1" format="n4"  labelStyle="text-align:left;width:130px;"   />
					<input style="width:100%" id="deliveryDate" name="deliveryDate" class="mini-datepicker" labelField="true"  label="交割日：" ondrawdate="compareDate3" labelStyle="text-align:left;width:130px;"   />
					<input style="width:100%" id="cutOffTime" name="cutOffTime" class="mini-timespinner" labelField="true"  label="行权截止时间："   labelStyle="text-align:left;width:130px;" />
					<input style="width:100%" id="optionStrategy" name="optionStrategy" class="mini-textbox" labelField="true"  label="期权类型：" maxLength="20"  labelStyle="text-align:left;width:130px;"   />
					<input style="width:100%" id="strategyId" name="strategyId" class="mini-textbox" labelField="true"  label="期权组合号：" maxLength="20"  labelStyle="text-align:left;width:130px;"   />
				    <input style="width:100%" id="plmethod" name="plmethod" class="mini-combobox" labelField="true" label="损益计算方法："  labelStyle="text-align:left;width:130px;"  vtype="maxLength:15" data="CommonUtil.serverData.dictionary.acctMethod" requiredErrorText="该输入项为必输项" required="true" />
				</div>
				<div class="rightarea">
					<input style="width:100%" id="direction" name="direction" class="mini-combobox" labelField="true"  label="交易方向：" data="CommonUtil.serverData.dictionary.trading"  labelStyle="text-align:left;width:130px;" />
					<input style="width:100%" id="strikePrice" name="strikePrice" class="mini-spinner" changeOnMousewheel='false'  format="n4" labelField="true"  label="执行价：" maxValue="999999999999999"  labelStyle="text-align:left;width:130px;"   />
					<input style="width:100%" id="buyNotionalAmount" name="buyNotionalAmount" class="mini-spinner" labelField="true"  label="名义本金金额：" changeOnMousewheel='false' maxValue="999999999999999" format="n4"  labelStyle="text-align:left;width:130px;"   />
					<input style="width:100%" id="sellNotionalAmount" name="sellNotionalAmount" class="mini-spinner" labelField="true"  label="名义本金金额：" changeOnMousewheel='false' maxValue="999999999999999" format="n4"  labelStyle="text-align:left;width:130px;"   />
					<input style="width:100%" id="spotReference" name="spotReference" class="mini-textbox" labelField="true"  label="即期参考汇率：" maxLength="20"  labelStyle="text-align:left;width:130px;" />
					<input style="width:100%" id="expiryDate" name="expiryDate" class="mini-datepicker" labelField="true"  label="行权日："  ondrawdate="compareDate2" labelStyle="text-align:left;width:130px;"   />
					<input style="width:100%" id="premiumType" name="premiumType" class="mini-textbox" labelField="true"  label="期权类型：" maxLength="20"  labelStyle="text-align:left;width:130px;"   /> 
					<input style="width:100%" id="premiumAmount" name="premiumAmount" class="mini-spinner" labelField="true"  label="期权费金额：" changeOnMousewheel='false' maxValue="999999999999999" format="n4"  labelStyle="text-align:left;width:130px;" />
					<input style="width:100%" id="deliveryType" name="deliveryType" class="mini-combobox" labelField="true"  label="交割方式：" maxLength="20" data="CommonUtil.serverData.dictionary.DeliveryType"  labelStyle="text-align:left;width:130px;"   />
					<input style="width:100%" id="exerciseType" name="exerciseType" class="mini-textbox" labelField="true"  label="执行方式：" maxLength="20"  labelStyle="text-align:left;width:130px;"   />
				</div>			
			</div>
			<%@ include file="../../Ifs/cfetsfx/accountInfor.jsp"%>  
		</div>
        </div>
        <div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
          <div style="margin-bottom:10px; text-align: center;">
              <a class="mini-button" style="display: none"  style="width:120px;" id="save_btn"  onclick="save">保存</a>
          </div>
          <div style="margin-bottom:10px; text-align: center;">
              <a class="mini-button" style="display: none"  style="width:120px;" id="cancel_btn"  onclick="cancel">取消</a>
          </div>
          
        </div>
        
    </div>
      <script type="text/javascript">
        mini.parse();

        //获取当前tab
        var currTab = top["win"].tabs.getActiveTab();
        var params = currTab.params;
        var row=params.selectedData;
        var url = window.location.search;
        var action = CommonUtil.getParam(url, "action");
        var form = new mini.Form("#field_form");
        var form1 = new mini.Form("#field_form1");
		
        var tradeData={};
    	tradeData.selectData=row;
    	tradeData.operType=action;
    	tradeData.serial_no=row.ticketId;
    	tradeData.task_id=row.taskId;
    	tradeData.dealno=row.dealno;
       
        $(document).ready(function () {
			//控制按钮显示
			$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
				initData();
			});
        });
        
       

      //初始化数据
        function initData() {
          if ($.inArray(action, ["edit", "detail","approve"]) > -1) { 
        	  form.setData(row);
        	  form1.setEnabled(false);
        	  search();
        	  if($.inArray(action,["approve","detail"])>-1){
	      			mini.get("save_btn").setVisible(false);
	      			form.setEnabled(false);
	      			var form11=new mini.Form("#approve_operate_form");
	      			form11.setEnabled(true);
      			}
          } else { //增加 
             form.setData(row);
             form1.setEnabled(false);
             mini.get("fedealno").setValue(row.ticketId);
             mini.get("ticketId").setValue("");
             mini.get("sponsor").setValue("<%=__sessionUser.getUserId()%>");
 			 mini.get("sponInst").setValue("<%=__sessionUser.getInstId() %>");
 			 mini.get("dealNo").setValue(row.dealNo);
             search();
          }
        }
       function search(){
    	   var ticketId =  mini.get("fedealno").getValue();
           var data = {};
           data['ticketId']=ticketId;
    	   var params = mini.encode(data);
    	   CommonUtil.ajax({
               url : "/IfsForeignController/searchOption",
               data : params,
               callback : function(data) {
                   form1.setData(data.obj);
               }
           });
       } 
        
        //保存
        function save() {
          //表单验证！！！
          form.validate();
          if (form.isValid() == false) {
            return;
          }
          var saveUrl = null;
            saveUrl = "/IfsRevIotdController/saveOptParam";
            var data = form.getData(true);
            var params = mini.encode(data);
            CommonUtil.ajax({
              url: saveUrl,
              data: params,
              callback: function (data) {
                if (data.code == 'error.common.0000') {
                  mini.alert("保存成功", '提示信息', function () {
                    top["fxoptManage"].query();
                    top["win"].closeMenuTab();
                  });
                } else {
                  mini.alert("保存失败");
                }
              }
            });
          }

        function cancel() {
          top["fxoptManage"].query();
          top["win"].closeMenuTab();
        }
      </script>
      <script type="text/javascript" src="../../Common/Flow/MiniApproveOpCommon.js"></script>
    </body>

    </html>