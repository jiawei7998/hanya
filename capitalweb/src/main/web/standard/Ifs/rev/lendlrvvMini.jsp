﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
    <title></title>
  </head>
  
<body style="width:100%;height:100%;background:white">
	<div class="mini-panel" title="拆借业务单据列表（已申请通过）" style="width:100%">
	<div style="width:100%;padding-top:10px;">
		<div id="search_form" class="mini-form" width="80%">
			<input style="width:35%;" id="dealType" name="dealType" align="center" class="mini-combobox" labelField="true"  label="业务类型：" onValuechanged="interestAmount"  data="CommonUtil.serverData.dictionary.BusinessType" labelStyle="text-align:right;" />
			<input style="width:35%;" id="ticketId" name="ticketId" class="mini-textbox" align="center" vtype="maxLength:50" labelField="true"  label="交易前端流水号：" labelStyle="text-align:right;"  emptyText="请输入交易前端流水号"/>
			<input style="width:35%;" id="contractId" name="contractId" class="mini-textbox" align="center" vtype="maxLength:50" labelField="true"  label="成交单编号：" labelStyle="text-align:right;"  emptyText="请输入成交单编号"/>
			<input style="width:35%;" id="dealNo" name="dealNo" class="mini-textbox" align="center" vtype="maxLength:50" labelField="true"  label="OPICS交易号：" labelStyle="text-align:right;"  emptyText="请输入OPICS交易号"/>
			<!-- <input id="approveStatus" name="approveStatus"  class="mini-combobox" data = "CommonUtil.serverData.dictionary.ApproveStatus" labelField="true"  label="审批状态：" labelStyle="text-align:right;" emptyText="请选择审批单状态"> -->
			<span style="float:right;margin-right: 150px">
				<a id="search_btn" class="mini-button" style="display: none"  onclick="query()">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"  onclick="clear()" >清空</a>
			</span>
		</div>
	</div>
	</div>
	<div id="tradeManage" class="mini-fit">
		<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="dealNo" 
		allowAlternating="true" allowResize="true" onrowdblclick="onRowDblClick" border="true" 
		multiSelect="false" sortMode="client">
			<div property="columns">
				<div type="indexcolumn" headerAlign="center" width="50">序号</div>
				<div field="ticketId" width="180" headerAlign="center" align="center" allowSort="true">交易前端流水号</div>   
<!-- 				<div field="rate" width="200" headerAlign="center" align="center" numberFormat="n8" allowSort="true">拆借利率</div> -->
				<div field="contractId" width="180" headerAlign="center" align="center" allowSort="true">成交单编号</div>
<!-- 				<div field="prodType" width="200" headerAlign="center" align="center" allowSort="true">产品类型</div>   -->
<!-- 				<div field="groupMod" width="200" headerAlign="center" align="center" allowSort="true">模块</div>   -->
<!-- 				<div field="dealSource" width="80" headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'TradeSource'}" align="center" allowSort="true">交易来源</div>   -->
<!-- 				<div field="trader" width="80" headerAlign="center" align="center" allowSort="true">交易员</div> -->
				<div field="dealNo" width="80" headerAlign="center" align="center" allowSort="true" style="display:none">OPICS交易号</div>
				<div field="forDate" width="180" headerAlign="center" align="center" allowSort="true">成交日期</div>
			</div>
		</div>  
	</div>
	<script type="text/javascript">
		mini.parse();

		var form=new mini.Form("#search_form");
		var grid=mini.get("datagrid");
		top["lendlrvvMini"] = window;
		var rows='';
		var fundCode='';
		mini.get("dealType").select(0);
		
		function interestAmount(){
			query();
		}
		/* 按钮 查询事件 */
	function query(){
		search(grid.pageSize,0);
	}

	/* 查询 */
	function search(pageSize,pageIndex){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统也提示");
			return;
		}
		var data=form.getData(true);
		var dealType = mini.get("dealType").getValue();
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		url = "/IfsCfetsrmbIboController/searchMiniPage";
		data['approveStatus']='6';
		data['prdNo'] = "438";
		
		if("creditLoan" == dealType) {
		    //信用拆借
		    dealType = "IFS_CFETSRMB_IBO";
		    data['prdNo'] = "444";
		}else if("tyLoan" == dealType) {
		    //同业借款
		    dealType = "IFS_CFETSRMB_IBO";
		    data['prdNo'] = "544";
		}else if("IFS_CFETSRMB_IBO" == dealType) {
			//同业拆放（对俄）
			dealType = "IFS_CFETSRMB_IBO";
			data['prdNo'] = "435";
		}else if("IFS_FX_DEPOSITOUT" == dealType) {
			//存放同业（对俄）
			dealType = "IFS_FX_DEPOSITOUT";
			data['prdNo'] = "437";
		}else if("IFS_RMB_DEPOSITOUT" == dealType) {
			//存放同业
			dealType = "IFS_RMB_DEPOSITOUT";
			data['prdNo'] = "791";
		}

		if("tyck" == dealType) {
		    //同业存款
		    dealType = "IFS_APPROVERMB_DEPOSIT";
		    url = "/IfsCfetsrmbIboController/searchDEMiniPage";
		}else {
		    data['statcode']='-4';
		}
		data['tableName'] = dealType;
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:url,
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	
	//清空
	function clear() {
        mini.get("ticketId").setValue("");
        mini.get("contractId").setValue("");
        mini.get("dealNo").setValue("");
        query();
      }
	
	
	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});
	
	//选中一行将数据展示
	function onRowDblClick(){
		CloseWindow("ok");
	}
	function GetData() {
        var row = grid.getSelected();
        var data = mini.get("dealType").getValue();
        row['dealType']=data;
        return row;
	}
	function SetData(data){
		rows=data.gridData;
		fundCode=data.fundCode;
	}
	function CloseWindow(action) {
        if (window.CloseOwnerWindow)
            return window.CloseOwnerWindow(action);
        else
            window.close();
	}
    //关闭窗口
    function onCancel() {
        CloseWindow("cancel");
    }
	$(document).ready(function(){
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
			search(10, 0);
		});
	})
	</script>
</body>
</html>