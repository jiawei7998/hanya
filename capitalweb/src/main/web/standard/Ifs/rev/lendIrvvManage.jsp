<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<title>拆借类</title>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>

<body style="width: 100%; height: 100%; background: white">
	<fieldset class="mini-fieldset">
		<legend>拆借类信息查询</legend>
		<div id="search_form" style="width: 100%;">
			<input id="ticketId" name="ticketId" class="mini-textbox" width="320px" labelField="true" label="审批单流水号：" labelStyle="text-align:right;width:110px" 	emptyText="请输入审批单流水号" />
			<!-- <input id="status" name="status" class="mini-combobox" data="CommonUtil.serverData.dictionary.DealStatus" width="280px" emptyText="请选择处理状态" labelField="true"  label="处理状态：" labelStyle="text-align:right;"/> -->
			<input id="dealno" name="dealno" class="mini-textbox" width="320px" labelField="true" label="OPICS交易号：" labelStyle="text-align:right;width:110px;" emptyText="请输入OPICS交易号"  />
			<span style="float: right; margin-right: 150px">
				<input id="dealType" name="dealType" class="mini-combobox" width="320px"   data="CommonUtil.serverData.dictionary.BusinessType" emptyText="请选择业务类型" labelField="true"  label="业务类型：" labelStyle="text-align:right;"/>
				<a class="mini-button" style="display: none"  id="search_btn" onclick="search()">查询</a>
				<a class="mini-button" style="display: none"  id="clear_btn" onclick="clear()">清空</a>
			</span>
		</div>
	</fieldset>

	<%@include file="./revBase.jsp" %>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>

	<div class="mini-fit" style="width: 100%; height: 100%;">
		<div id="irvv_grid" class="mini-datagrid borderAll" style="width: 100%; height: 100%;" sortMode="client" allowAlternating="true" idField="userId" 	onrowdblclick="onRowDblClick" border="true" allowResize="true">
			<div property="columns">
				<div type="indexcolumn" headerAlign="center" align="center"	width="40px">序列</div>
				<div field="ticketId" headerAlign="center" align="center" width="180px">审批单流水号</div>
				<div field="br" headerAlign="center" align="center" width="180px">部门</div>
				<div field="fedealno" headerAlign="center" align="center" width="180px">交易前端流水号</div>
				<div field="approveStatus" headerAlign="center" align="center"	width="180px" renderer="CommonUtil.dictRenderer" data-options="{dict:'ApproveStatus'}">审批状态</div>
				<div field="dealno" headerAlign="center" align="center" width="180px">OPICS交易号</div>
				<div field="revreason" headerAlign="center" align="center" width="180px">冲销原因</div>
				<div field="dealType" headerAlign="center" align="center" width="180px" renderer="CommonUtil.dictRenderer"	data-options="{dict:'BusinessType'}">业务类型</div>
				<div field="adate" headerAlign="center" align="center" width="180px">冲销日期</div>
				<div field="sponsor" headerAlign="center" align="center" width="180px">审批发起人</div>
				<div field="instName" headerAlign="center" align="center" width="180px">审批发起机构</div>
				<div field="sponDate" headerAlign="center" align="center" width="180px">审批发起时间</div>
				<div field="taskId" visible="false">流程id</div>
			</div>
		</div>
	</div>
</body>
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
<script>

	mini.parse();
	var url = window.location.search;
	var prdNo = CommonUtil.getParam(url, "prdNo");
	var prdName = CommonUtil.getParam(url, "prdName");
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	top["lendIrvvManage"] = window;
	
	var grid = mini.get("irvv_grid");
	grid.on("beforeload", function(e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex;
		var pageSize = e.data.pageSize;
		searchs(pageSize, pageIndex);
	});

	//行点击事件
    grid.on("select",function(e){
		var row=e.record;
		mini.get("approve_mine_commit_btn").setEnabled(false);
		mini.get("approve_commit_btn").setEnabled(false);
		mini.get("edit_btn").setEnabled(false);
		mini.get("delete_btn").setEnabled(false);
		if(row.approveStatus == "3"){//新建
			mini.get("approve_mine_commit_btn").setEnabled(true);
			mini.get("approve_commit_btn").setEnabled(false);
			mini.get("edit_btn").setEnabled(true);
			mini.get("delete_btn").setEnabled(true);
			mini.get("approve_log").setEnabled(false);
		}
		if( row.approveStatus == "6" || row.approveStatus == "5"){//审批通过：6   审批中:5
			mini.get("approve_mine_commit_btn").setEnabled(false);
			mini.get("approve_commit_btn").setEnabled(true);
			mini.get("edit_btn").setEnabled(false);
			mini.get("delete_btn").setEnabled(false);
		}
		if(row.approveStatus != null && row.approveStatus != "3") {
			mini.get("approve_log").setEnabled(true);
		}
	});
	
	$(document).ready(function() {
		initButton();
		search();
	});

	//查询按钮
	function search() {
		searchs(grid.pageSize, 0);
	}

	function searchs(pageSize, pageIndex) {
		var form = new mini.Form("#search_form");
		form.validate();
		if (form.isValid() == false) {
			mini.alert("表单填写错误,请确认!", "提示信息");
			return;
		}

		var data = form.getData();
		data['pageNumber'] = pageIndex + 1;
		data['pageSize'] = pageSize;
		data['branchId'] = branchId;

		var params = mini.encode(data);
		
		var url=null;
		var approveType=mini.get("approveType").getValue();
		if(approveType == "mine"){
			url="/IfsRevIrvvController/searchIrvvMine";
		}else if(approveType == "approve"){
			url="/IfsRevIrvvController/searchIrvvUnfinished";
		}else{
			url="/IfsRevIrvvController/searchIrvvFinished";
		}

		CommonUtil.ajax({
			/*url : "/IfsOpicsProdController/searchPageProd",*/
			//url : "/IfsRevIrvvController/searchPageIrvv",
			url: url,
			data : params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
				grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}

	//清空
	function clear() {
		var form = new mini.Form("search_form");
		form.clear();
		search();
	}

	function add() {
		var messageid = mini.loading("系统正在处理...", "请稍后");
		try {
			mini.open({
				url : CommonUtil.baseWebPath() + "/rev/lendlrvvMini.jsp",
				title : "拆借业务单据选择",
				width : 1000,
				height : 600,
				ondestroy : function(action) {
					if (action == "ok") {
						var iframe = this.getIFrameEl();
						var data = iframe.contentWindow.GetData();
						data = mini.clone(data); //必须lendlrvvManageEdit.jsp
						var row = data;
						var url = CommonUtil.baseWebPath()
								+ "/rev/lendIrvvManageEdit.jsp";
						var tab = {
							id : "_" + "add",
							name : "_" + "add",
							iconCls : "",
							title : "拆借类新增",
							url : url,
							showCloseButton : true
						};
						var paramData = {
							'operType' : 'A',
							'closeFun' : 'query',
							'pWinId' : 'lendlrvvManageEdit',
							selectedData : row,
							action : "add"
						};
						CommonUtil.openNewMenuTab(tab, paramData);
					}
				}
			});
		} catch (error) {

		}
		mini.hideMessageBox(messageid);
	};

	function query(){
		search();
	}
	
	//修改
	function edit() {
		var row = grid.getSelected();
		if (row) {
			var url = CommonUtil.baseWebPath()
					+ "/rev/lendIrvvManageEdit.jsp?action=edit";
			var tab = {
				id : "lendlrvvManageEdit",
				name : "lendlrvvManageEdit",
				title : "拆借类修改",
				url : url,
				showCloseButton : true,
				parentId : top["win"].tabs.getActiveTab().name
			};
			var paramData = {
				selectedData : row
			};
			CommonUtil.openNewMenuTab(tab, paramData);

		} else {
			mini.alert("请选中一条记录！", "消息提示");
		}
	}

	//删除
	function del() {
		var row = grid.getSelected();
		if (row) {
			mini.confirm("您确认要删除选中记录?", "系统警告", function(value) {
				if (value == "ok") {
					CommonUtil.ajax({
						url : "/IfsRevIrvvController/deleteIrvv",
						data : {
							ticketId : row.ticketId
						},
						callback : function(data) {
							if (data.code == 'error.common.0000') {
								mini.alert("删除成功!");
								grid.reload();
							} else {
								mini.alert("删除失败!");
							}
						}
					});
				}
			});
		} else {
			mini.alert("请选中一条记录！", "消息提示");
		}

	}
	
	//双击详情
	function onRowDblClick(e) {
	     var row = grid.getSelected();
	     if(row){
	             var url = CommonUtil.baseWebPath() + "/rev/lendIrvvManageEdit.jsp?action=detail";
	             var tab = { id: "lendDetail", name: row.ticketId, title: "拆借类详情", url: url ,showCloseButton:true};
	             var paramData = {selectedData:row};
	             CommonUtil.openNewMenuTab(tab,paramData);
	     } else {
	         mini.alert("请选中一条记录！","消息提示");
	     }
	 } 
	 
	 /**********************审批**************************/
	//提交审批
	function commit(){
		mini.get("approve_mine_commit_btn").setEnabled(false);
		var messageid = mini.loading("系统正在处理...", "请稍后");
		try {
			verify(grid.getSelecteds());
		} catch (error) {
			
		}
		mini.hideMessageBox(messageid);	
		mini.get("approve_mine_commit_btn").setEnabled(true);
	} 
 
	//提交正式审批、待审批
	function verify(selections){
		if(selections.length == 0){
			mini.alert("请选中一条记录！","消息提示");
			return false;
		}else if(selections.length > 1){
			mini.alert("该功能不支持多笔提交，如需多笔提交请点击[批量提交]按钮","系统提示");
			return false;
		}
		if(selections[0]["approveStatus"] != "3" ){
			var url = CommonUtil.baseWebPath() +"/rev/lendIrvvManageEdit.jsp?action=approve&ticketId="+selections[0].ticketId;
			var tab = {"id": "LendApprove",name:"LendApprove",url:url,title:"拆借审批",
						parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
			var paramData = {selectedData:selections[0]};
			CommonUtil.openNewMenuTab(tab,paramData);
		}else{
			var dealType = selections[0]["dealType"];
			prdNo = dealType == "creditLoan" ? "444" : dealType == "tyLoan" ? "544" : dealType == "tyck"? "800" : "438";
			Approve.approveCommit(Approve.FlowType.RevApproveFlow,selections[0]["ticketId"],Approve.OrderStatus.New,"IrvvRev",prdNo,function(){
				search(grid.pageSize,grid.pageIndex);
			});
		}
	};
	
	//审批日志
	function searchlog(){
		appLog(grid.getSelecteds());
	}
	
	//审批日志查看
	function appLog(selections){
		var flow_type = Approve.FlowType.VerifyApproveFlow;
		if(selections.length <= 0){
			mini.alert("请选择要操作的数据","系统提示");
			return;
		}
		if(selections.length > 1){
			mini.alert("系统不支持多笔操作","系统提示");
			return;
		}
		if(selections[0].tradeSource == "3"){
			mini.alert("初始化导入的业务没有审批日志","系统提示");
			return false;
		}
		
		Approve.approveLog(flow_type,selections[0].ticketId);
	};
	
	//审批
	function approve(){
		mini.get("approve_commit_btn").setEnabled(false);
		var messageid = mini.loading("系统正在处理...", "请稍后");
		try {
			verify(grid.getSelecteds());
		} catch (error) {
			
		}
		mini.hideMessageBox(messageid);	
		mini.get("approve_commit_btn").setEnabled(true);
	}
</script>
</html>