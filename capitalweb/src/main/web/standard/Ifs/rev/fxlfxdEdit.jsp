<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
  <%@ include file="../../global.jsp"%>
    <html>

    <head>
      <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>存续期管理台 外汇类</title>
    </head>

    <body style="width:100%;height:100%;background:white">
     
    <div class="mini-splitter" style="width:100%;height:100%;">
        <div size="90%" showCollapseButton="false">
        <div id="field" class="fieldset-body">
            <div id="field_form" class="mini-panel" title="外汇类冲销信息" style="width:100%" allowResize="true" collapseOnTitleClick="true">
               <div class="leftarea">
                  <input id="sponsor" name="sponsor" class="mini-hidden" />
                  <input id="sponInst" name="sponInst" class="mini-hidden" />
                  <input id="ticketId" name="ticketId" enabled="false" class="mini-textbox" label="审批单流水号：" labelField="true" emptyText="系统自动生成"  vtype="maxLength:35" style="width:100%" labelStyle="text-align:left;width:170px" />
                  <input id="fedealno" name="fedealno" class="mini-textbox" enabled="false" label="交易前端流水号：" emptyText="请输入交易前端流水号" labelField="true" style="width:100%" labelStyle="text-align:left;width:170px" />
                  <input id="adate" name="adate" class="mini-datepicker mini-mustFill" label="冲销日期：" required="true"  emptyText="请输入冲销日期" labelField="true"style="width:100%" onvaluechanged = "adateCheck" labelStyle="text-align:left;width:170px" vtype="maxLength:35" />
                  <input id="revreason" name="revreason" class="mini-textarea" label="冲销原因：" emptyText="请输入冲销原因" labelField="true"style="width:100%" labelStyle="text-align:left;width:170px" vtype="maxLength:255" />
            </div>
              <div class="rightarea">
                  <input style="width:100%;" id="dealType" name="dealType" enabled="false" class="mini-combobox" labelField="true"  label="业务类型："  data="CommonUtil.serverData.dictionary.dealType" labelStyle="text-align:left;width:170px" />
                  <input id="dealNo" name="dealNo" enabled="false" class="mini-textbox" label="OPICS交易号：" emptyText="请输入OPICS交易号" labelField="true"style="width:100%" labelStyle="text-align:left;width:170px" />
                  <input id="br" name="br" class="mini-combobox  mini-mustFill" label="部门："  data="[{id:'01',text:'01'}]" emptyText="请选择部门" labelField="true" style="width:100%" labelStyle="text-align:left;width:170px" vtype="maxLength:50" required="true" />
                </div>
            <%@ include file="../../Common/Flow/MiniApproveOpCommon.jsp"%>
            </div>
		<div  id="field_form1"  class="mini-fit" style="background:white" >
			<div class="mini-panel" title="外汇掉期详情" style="width:100%;"  allowResize="true" collapseOnTitleClick="true">
			<div class="leftarea">
				<input style="width:100%;" id="contractId" name="contractId" class="mini-textbox mini-mustFill" labelField="true" requiredErrorText="该输入项为必输项" required="true"  label="成交单编号：" labelStyle="text-align:left;width:130px;" onvalidation="onEnglishAndNumberValidation" vtype="maxLength:20"/>
			</div>
			</div>
			<div class="mini-panel" title="成交双方信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
				<div class="leftarea">
						<input id="instId1" name="instId" class="mini-textbox mini-mustFill" labelField="true"  label="本方机构：" style="width:100%;"  labelStyle="text-align:left;width:130px"requiredErrorText="该输入项为必输项" required="true"  vtype="maxLength:16"/>
						<input id="dealer1" name="dealer" class="mini-textbox mini-mustFill" labelField="true"  label="本方交易员："  labelStyle="text-align:left;width:130px"style="width:100%;"   requiredErrorText="该输入项为必输项" required="true"  vtype="maxLength:20" onvalidation="onEnglishAndNumberValidation" />
					</div>
					<div class="rightarea">
						<input id="counterpartyInstId1" name="counterpartyInstId" class="mini-textbox mini-mustFill" requiredErrorText="该输入项为必输项" required="true"  onbuttonclick="onButtonEdit" labelField="true" allowInput="false" label="对方机构："  style="width:100%;"  labelStyle="text-align:left;width:130px" vtype="maxLength:16"/>
						<input id="counterpartyDealer" name="counterpartyDealer" class="mini-textbox mini-mustFill" labelField="true"  label="对方交易员：" labelStyle="text-align:left;width:130px"style="width:100%;"   vtype="maxLength:30" required="true" />
					</div>
			</div>			
			<div class="mini-panel" title="交易主体信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
					<div class="leftarea">
						<input id="forDate" name="forDate" class="mini-datepicker mini-mustFill" labelField="true" requiredErrorText="该输入项为必输项" required="true"  label="交易日期："  labelStyle="text-align:left;width:130px"style="width:100%;"   />
						<input style="width:100%;"id="currencyPair" name="currencyPair" class="mini-combobox mini-mustFill" labelField="true"  label="货币对："   requiredErrorText="该输入项为必输项" required="true"  labelStyle="text-align:left;width:130px" data="CommonUtil.serverData.dictionary.CurrencyPair" onvaluechanged="onCurrencyPairChange"/>
					</div>
					<div class="rightarea">
						<input id="forTime" name="forTime" class="mini-timespinner mini-mustFill" labelField="true"  label="交易时间："  labelStyle="text-align:left;width:130px"requiredErrorText="该输入项为必输项" required="true"  style="width:100%;"   />
						<input style="width:100%;"id="direction" name="direction" class="mini-combobox mini-mustFill" labelField="true"  label="交易方向：" requiredErrorText="该输入项为必输项" required="true"  labelStyle="text-align:left;width:130px"    data="CommonUtil.serverData.dictionary.Swaptrading" onvaluechanged="onCurrencyPairChange"/>
					</div>
					<div class="centerarea">
						<input style="width:50%;" id="price"  name="price"  class="mini-spinner mini-mustFill" labelField="true"  label="SPOT汇率："  format="n8" maxValue="999999999999" changeOnMousewheel="false"   labelStyle="text-align:left;width:130px;"  onvaluechanged="getReversePrice" requiredErrorText="该输入项为必输项" required="true" />
					</div>
					<div class="leftarea">
						<input style="width:100%;"id="nearValuedate" name="nearValuedate" class="mini-datepicker mini-mustFill" labelField="true"  label="近端起息日：" requiredErrorText="该输入项为必输项" required="true"  ondrawdate="onDrawDateNear" onvaluechanged="associaDateCalculation" labelStyle="text-align:left;width:130px"    />
						<input style="width:100%;"id="nearPrice" name="nearPrice" class="mini-spinner mini-mustFill" labelField="true"  label="近端交易金额1：" requiredErrorText="该输入项为必输项" required="true"  maxValue="999999999999" changeOnMousewheel='false' format="n4"  labelStyle="text-align:left;width:130px"  onvaluechanged="getReversePrice" onvalidation="zeroValidation"/>
						<input style="width:100%;"id="nearSpread" name="nearSpread" class="mini-spinner mini-mustFill" labelField="true"  label="近端点：" changeOnMousewheel='false' format="n8" requiredErrorText="该输入项为必输项" required="true"  labelStyle="text-align:left;width:130px" onvaluechanged="getReversePrice" minValue="-1000000" maxValue="1000000" value="0"/>
						<input style="width:100%;"id="nearRate" name="nearRate" class="mini-spinner mini-mustFill" labelField="true"  label="近端成交汇率："  requiredErrorText="该输入项为必输项" required="true"  format="n8" changeOnMousewheel="false" maxValue="999999999999" labelStyle="text-align:left;width:130px" enabled="false"/>
						<input style="width:100%;"id="nearReversePrice" name="nearReversePrice" class="mini-spinner mini-mustFill" labelField="true"  label="近端交易金额2：" requiredErrorText="该输入项为必输项" required="true"  maxValue="999999999999" changeOnMousewheel='false' format="n4"  labelStyle="text-align:left;width:130px"  onvalidation="zeroValidation"  />
						<input style="width:100%;"id="tenor" name="tenor" class="mini-spinner" labelField="true"  label="期限：" requiredErrorText="该输入项为必输项"  enabled="false" maxValue="99999" changeOnMousewheel='false' labelStyle="text-align:left;width:130px"    />
						<!-- <input id="nettingStatus" name="nettingStatus" class="mini-combobox mini-mustFill" required="true"  labelField="true"   label="净额清算状态：" labelStyle="text-align:left;width:130px;" style="width:100%;"  data="CommonUtil.serverData.dictionary.YesNo" showNullItem="true"/> -->
						<input id="tradingType" name="tradingType" class="mini-combobox mini-mustFill" labelField="true"  label="交易方式：" requiredErrorText="该输入项为必输项" required="true"  value="RFQ" labelStyle="text-align:left;width:130px"style="width:100%;" data="CommonUtil.serverData.dictionary.TradeType"/>
						<input id="note" name="note" class="mini-textbox" labelField="true" label="备注：" labelStyle="text-align:left;width:130px;" style="width:100%;" vtype="maxLength:16"/>
						<!-- <input id="delaydElivind" name="delaydElivind" class="mini-combobox" labelField="true"   label="延期交易：" labelStyle="text-align:left;width:130px;" style="width:100%;" data="CommonUtil.serverData.dictionary.delaydElivind" value='0'/> -->
					</div>
					<div class="rightarea">
						<input style="width:100%;"id="fwdValuedate" name="fwdValuedate" class="mini-datepicker mini-mustFill" labelField="true"  label="远端起息日："  labelStyle="text-align:left;width:130px" requiredErrorText="该输入项为必输项" ondrawdate="onDrawDateFar" onvaluechanged="associaDateCalculation" required="true"    />
						<input style="width:100%;"id="fwdPrice" name="fwdPrice" class="mini-spinner mini-mustFill" labelField="true"  label="远端交易金额1：" requiredErrorText="该输入项为必输项" required="true"  maxValue="999999999999" changeOnMousewheel='false'  format="n4" labelStyle="text-align:left;width:130px"  onvaluechanged="getFwdReversePrice" onvalidation="zeroValidation" />
						<input style="width:100%;"id="spread" name="spread" class="mini-spinner mini-mustFill" labelField="true"  label="远端点：" changeOnMousewheel='false' format="n8" requiredErrorText="该输入项为必输项" required="true"  labelStyle="text-align:left;width:130px" onvaluechanged="getReversePrice" minValue="-1000000" maxValue="1000000" value="0"/>
						<input style="width:100%;"id="fwdRate" name="fwdRate" class="mini-spinner mini-mustFill" labelField="true"  label="远端成交汇率："  format="n8" changeOnMousewheel="false" requiredErrorText="该输入项为必输项" required="true"  maxValue="999999999999" labelStyle="text-align:left;width:130px"   data="CommonUtil.serverData.dictionary.trading"  enabled="false"/>
						<input style="width:100%;"id="fwdReversePrice" name="fwdReversePrice" class="mini-spinner mini-mustFill" labelField="true"  label="远端交易金额2：" maxValue="999999999999" requiredErrorText="该输入项为必输项" required="true"  changeOnMousewheel='false'  format="n4" labelStyle="text-align:left;width:130px"  onvalidation="zeroValidation"  />
						<input id="tradingModel" name="tradingModel" class="mini-combobox mini-mustFill" labelField="true" requiredErrorText="该输入项为必输项" required="true"  label="交易模式：" labelStyle="text-align:left;width:130px"style="width:100%;" data="CommonUtil.serverData.dictionary.TradeModel" />
						<input id="dealTransType" name="dealTransType" class="mini-combobox" labelField="true"  style="width:100%;"  label="数据来源：" data="CommonUtil.serverData.dictionary.dealTransType1" vtype="maxLength:10"  labelStyle="text-align:left;width:130px;"  value="1">
					</div>
			</div>  	
			<%@ include file="../../Common/opicsLessDetail.jsp"%>
			<%@ include file="../../Ifs/cfetsfx/accountInforSwap.jsp"%>
		</div>
		<div  id="field_form2"  class="mini-fit" style="background:white" >
		<div class="mini-panel" title="外汇远期详情" style="width:100%;"  allowResize="true" collapseOnTitleClick="true">
			<div class="leftarea">
				<input style="width:100%;" id="contractId" name="contractId" class="mini-textbox mini-mustFill" labelField="true" requiredErrorText="该输入项为必输项" required="true"  label="成交单编号：" labelStyle="text-align:left;width:130px;" onvalidation="onEnglishAndNumberValidations" vtype="maxLength:20"/>
			</div>
		</div>
		<div class="mini-panel" title="成交双方信息" style="width:100%;"  allowResize="true" collapseOnTitleClick="true">
			<div class="leftarea">
				<input id="instId"  name="instId" class="mini-textbox mini-mustFill" labelField="true"  label="本方机构："  style="width:100%;"  labelStyle="text-align:left;width:130px;"  requiredErrorText="该输入项为必输项" required="true"  vtype="maxLength:16"/>
				<input id="dealer"  name="dealer" class="mini-textbox mini-mustFill" labelField="true"  label="本方交易员："  labelStyle="text-align:left;width:130px;"  style="width:100%;"   requiredErrorText="该输入项为必输项" required="true"  vtype="maxLength:6"/>
			</div>
			<div class="rightarea">
				<input id="counterpartyInstId"  name="counterpartyInstId" class="mini-textbox mini-mustFill" requiredErrorText="该输入项为必输项" required="true"  onbuttonclick="onButtonEdit" labelField="true" allowInput="false" label="对方机构："  style="width:100%;"  labelStyle="text-align:left;width:130px;"   vtype="maxLength:16"/>
				<input id="counterpartyDealer"  name="counterpartyDealer" class="mini-textbox mini-mustFill" labelField="true" label="对方交易员："  labelStyle="text-align:left;width:130px;"  style="width:100%;"    vtype="maxLength:10" required="true"  />
			</div>
		</div>
		
	<div class="mini-panel" title="交易主体信息" style="width:100%;"  allowResize="true" collapseOnTitleClick="true">
		<div class="leftarea">
			<input id="forDate"  name="forDate" requiredErrorText="该输入项为必输项" required="true"  class="mini-datepicker mini-mustFill" labelField="true"   label="交易日期："  labelStyle="text-align:left;width:130px;"  style="width:100%;" onvaluechanged = "fwdPeriodDataCalculation"/>
			<input style="width:100%;" id="currencyPair"  name="currencyPair" class="mini-combobox mini-mustFill" labelField="true" requiredErrorText="该输入项为必输项" required="true"  label="货币对："    labelStyle="text-align:left;width:130px;"  data="CommonUtil.serverData.dictionary.CurrencyPair"  onvaluechanged="onCurrencyPairChange"/>
			<input style="width:100%;" id="valueDate"  name="valueDate" class="mini-datepicker mini-mustFill" labelField="true"   label="起息日期："  labelStyle="text-align:left;width:130px;"   ondrawdate="onDrawDate" requiredErrorText="该输入项为必输项" required="true"  onvaluechanged = "fwdPeriodDataCalculation"/>
			<input style="width:100%;" id="buyAmount"  name="buyAmount" class="mini-spinner mini-mustFill" labelField="true" maxValue="999999999999" format="n4" label="金额1："  labelStyle="text-align:left;width:130px;"    onvaluechanged="getSellAmount" requiredErrorText="该输入项为必输项" required="true"  onvalidation="zeroValidation"/>
			<input style="width:100%;" id="sellAmount"  name="sellAmount" class="mini-spinner mini-mustFill" labelField="true"  maxValue="999999999999" format="n4" label="金额2："  labelStyle="text-align:left;width:130px;"  requiredErrorText="该输入项为必输项" required="true"  onvalidation="zeroValidation"  />
			<input id="fwdPeriod" name="fwdPeriod" class="mini-spinner" labelField="true"  label="期限(天)：" width="100%" labelStyle="text-align:left;width:130px;" maxValue="9999" changeOnMousewheel="false"  />
			<!-- <input id="nettingStatus" name="nettingStatus" class="mini-combobox mini-mustFill" required="true"  labelField="true"   label="净额清算状态：" labelStyle="text-align:left;width:130px;" style="width:100%;" data="CommonUtil.serverData.dictionary.YesNo" showNullItem="true" /> -->
			<input id="tradingType"  name="tradingType" class="mini-combobox mini-mustFill" labelField="true" requiredErrorText="该输入项为必输项" required="true"   value="RFQ" label="交易方式："  labelStyle="text-align:left;width:130px;"  style="width:100%;" data="CommonUtil.serverData.dictionary.TradeType"  />
			<input id="note" name="note" class="mini-textbox" labelField="true" label="备注：" labelStyle="text-align:left;width:130px;" style="width:100%;" vtype="maxLength:16"/>
		</div>
		<div class="rightarea">	
			<input id="forTime"  name="forTime" class="mini-timespinner mini-mustFill" labelField="true" requiredErrorText="该输入项为必输项" required="true"  label="交易时间："  labelStyle="text-align:left;width:130px;"  style="width:100%;"   />
			<input style="width:100%;" id="buyDirection"  name="buyDirection" class="mini-combobox mini-mustFill" labelField="true" requiredErrorText="该输入项为必输项" required="true"  label="交易方向："  labelStyle="text-align:left;width:130px;"   data="CommonUtil.serverData.dictionary.trading"   onvaluechanged="onCurrencyPairChange"/>
			<input style="width:100%;" id="price"  name="price"  class="mini-spinner mini-mustFill" labelField="true"  label="SPOT汇率："  format="n8" maxValue="999999999999" changeOnMousewheel="false"   labelStyle="text-align:left;width:130px;"  onvaluechanged="getSellAmount" requiredErrorText="该输入项为必输项" required="true" />
			<input style="width:100%;"id="spread" name="spread" class="mini-spinner" labelField="true"  label="升贴水点：" changeOnMousewheel='false' format="n8"  labelStyle="text-align:left;width:130px" onvaluechanged="getSellAmount" minValue="-1000000" maxValue="1000000" value="0"/>
			<input style="width:100%;" id="exchangeRate"  name="exchangeRate"  class="mini-spinner" labelField="true"  label="成交汇率："  format="n8" maxValue="999999999999" changeOnMousewheel="false"   labelStyle="text-align:left;width:130px;"  enabled="false" />
			<input id="tradingModel"  name="tradingModel" requiredErrorText="该输入项为必输项" required="true"  class="mini-combobox mini-mustFill" labelField="true" label="交易模式："  labelStyle="text-align:left;width:130px;"  style="width:100%;"   data="CommonUtil.serverData.dictionary.TradeModel" />
			<input id="dealTransType" name="dealTransType" class="mini-combobox" labelField="true"  style="width:100%;"  label="数据来源：" data="CommonUtil.serverData.dictionary.dealTransType1" vtype="maxLength:10"  labelStyle="text-align:left;width:130px;"  value="1">
			<!-- <input id="delaydElivind" name="delaydElivind" class="mini-combobox" labelField="true"   label="延期交易：" labelStyle="text-align:left;width:130px;" style="width:100%;" data="CommonUtil.serverData.dictionary.delaydElivind" value='0'/> -->
		</div>
	</div>
	<%@ include file="../../Common/opicsLessDetail.jsp"%>
	<%@ include file="../../Ifs/cfetsfx/accountInfor.jsp"%>
</div>
		<div  id="field_form3"  class="mini-fit" style="background:white" >
			<div class="mini-panel" title="外汇即期详情" style="width:100%;"  allowResize="true" collapseOnTitleClick="true">
					<div class="leftarea">
						<input style="width:100%;" id="contractId" name="contractId" class="mini-textbox mini-mustFill" labelField="true" requiredErrorText="该输入项为必输项" required="true"  label="成交单编号：" labelStyle="text-align:left;width:130px;" onvalidation="onEnglishAndNumberValidations" vtype="maxLength:20"/>
					</div>
				</div>
			<div class="mini-panel" title="成交双方信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
				<div class="leftarea" >
							<input id="instId" name="instId" class="mini-textbox mini-mustFill" labelField="true"  label="本方机构："  style="width:100%;"  labelStyle="text-align:left;width:130px;" requiredErrorText="该输入项为必输项" required="true"  vtype="maxLength:16"/>
							<input id="dealer" name="dealer" class="mini-textbox mini-mustFill" labelField="true" requiredErrorText="该输入项为必输项" required="true"   label="本方交易员："  labelStyle="text-align:left;width:130px;" style="width:100%;"   requiredErrorText="该输入项为必输项" required="true"  vtype="maxLength:20" />
					</div>
					<div class="rightarea">
							<input id="counterpartyInstId" name="counterpartyInstId" class="mini-textbox mini-mustFill" onbuttonclick="onButtonEdit" labelField="true" allowInput="false" label="对方机构："  style="width:100%;"  labelStyle="text-align:left;width:130px;" vtype="maxLength:16" requiredErrorText="该输入项为必输项" required="true" />
							<input id="counterpartyDealer" name="counterpartyDealer" class="mini-textbox mini-mustFill" labelField="true" label="对方交易员："  labelStyle="text-align:left;width:130px;" style="width:100%;"   vtype="maxLength:6" required="true"  />
					</div>
			</div>
				
			<div class="mini-panel" title="交易主体信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
				<div class="leftarea">	
					  	<input id="forDate" name="forDate" class="mini-datepicker mini-mustFill" labelField="true" label="交易日期："  labelStyle="text-align:left;width:130px;" style="width:100%;"  requiredErrorText="该输入项为必输项" required="true"  />
						<input style="width:100%;"  id="currencyPair" name="currencyPair" class="mini-combobox mini-mustFill" labelField="true"  label="货币对："  requiredErrorText="该输入项为必输项" required="true"   labelStyle="text-align:left;width:130px;" data="CommonUtil.serverData.dictionary.CurrencyPair" onvaluechanged="onCurrencyPairChange"/>
						<input style="width:100%;"  id="valueDate" name="valueDate" class="mini-datepicker mini-mustFill" labelField="true" label="起息日："  labelStyle="text-align:left;width:130px;"  ondrawdate="onDrawDate"  requiredErrorText="该输入项为必输项" required="true"  onvaluechanged = "searchProductWeightNew()"/>
						<input style="width:100%;"  id="buyAmount" name="buyAmount" class="mini-spinner mini-mustFill" labelField="true"  label="金额1：" maxValue="999999999999" changeOnMousewheel='false' format="n4" labelStyle="text-align:left;width:130px;"  requiredErrorText="该输入项为必输项" required="true"  onvaluechanged="setSellAmount"/>
						<input style="width:100%;"  id="sellAmount" name="sellAmount" class="mini-spinner mini-mustFill" labelField="true"  label="金额2："  maxValue="999999999999" changeOnMousewheel='false' format="n4" labelStyle="text-align:left;width:130px;"  requiredErrorText="该输入项为必输项" required="true"   />
						<input id="delaydElivind" name="delaydElivind" class="mini-combobox" labelField="true"   label="延期交易：" labelStyle="text-align:left;width:130px;" style="width:100%;" data="CommonUtil.serverData.dictionary.delaydElivind" value='2'/>
						<input id="tradingType" name="tradingType" class="mini-combobox mini-mustFill" labelField="true" requiredErrorText="该输入项为必输项" required="true"  label="交易方式："  value="RFQ" labelStyle="text-align:left;width:130px;" style="width:100%;" data="CommonUtil.serverData.dictionary.TradeType"/>
						<input id="note" name="note" class="mini-textbox" labelField="true" label="备注：" labelStyle="text-align:left;width:130px;" style="width:100%;" vtype="maxLength:16"/>
					</div>
					<div class="rightarea">
						<input id="forTime" name="forTime" class="mini-timespinner mini-mustFill" labelField="true" requiredErrorText="该输入项为必输项" required="true"   label="交易时间："  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						<input style="width:100%;"  id="buyDirection" name="buyDirection" class="mini-combobox mini-mustFill" labelField="true" requiredErrorText="该输入项为必输项" required="true"  label="交易方向："  labelStyle="text-align:left;width:130px;"  data="CommonUtil.serverData.dictionary.trading"  onvaluechanged="onCurrencyPairChange"/>
						<input style="width:100%;"  id="price" name="price" class="mini-spinner mini-mustFill" labelField="true"  label="SPOT汇率："   format="n8" maxValue="999999999999" changeOnMousewheel="false" labelStyle="text-align:left;width:130px;" onvaluechanged="setSellAmount" requiredErrorText="该输入项为必输项" required="true" />
						<input style="width:100%;"id="spread" name="spread" class="mini-spinner" labelField="true"  label="升贴水点："  changeOnMousewheel='false' format="n8"  labelStyle="text-align:left;width:130px" onvaluechanged="setSellAmount" minValue="-1000000" maxValue="1000000" value="0"/>
						<input style="width:100%;" id="exchangeRate"  name="exchangeRate"  class="mini-spinner" labelField="true"  label="成交汇率："  format="n8" maxValue="999999999999" changeOnMousewheel="false"   labelStyle="text-align:left;width:130px;"  enabled="false" />
						<input id="tradingModel" name="tradingModel" class="mini-combobox mini-mustFill" required="true"  labelField="true"   label="交易模式：" labelStyle="text-align:left;width:130px;" style="width:100%;"  data="CommonUtil.serverData.dictionary.TradeModel"/>
						<input id="dealTransType" name="dealTransType" class="mini-combobox" labelField="true"  style="width:100%;"  label="数据来源：" data="CommonUtil.serverData.dictionary.dealTransType1" vtype="maxLength:10"  labelStyle="text-align:left;width:130px;"  value="1">
					</div>
			</div>
			<%@ include file="../../Common/opicsLessDetail.jsp"%>
			<%@ include file="../../Ifs/cfetsfx/accountInfor.jsp"%>  
		</div>
		
		<div  id="field_form4"  class="mini-fit" style="background:white" >
			<div class="mini-panel" title="黄金即期详情" style="width:100%;"  allowResize="true" collapseOnTitleClick="true">
					<div class="leftarea">
						<input style="width:100%;" id="contractId" name="contractId" class="mini-textbox mini-mustFill" labelField="true" requiredErrorText="该输入项为必输项" required="true"  label="成交单编号：" labelStyle="text-align:left;width:130px;" onvalidation="onEnglishAndNumberValidations" vtype="maxLength:20"/>
					</div>
				</div>
			<div class="mini-panel" title="成交双方信息" style="width:100%;"  allowResize="true" collapseOnTitleClick="true">
				<div class="leftarea">
					<input id="instId"  name="instId" label="本方机构：" class="mini-textbox" labelField="true" vtype="maxLength:50" style="width:100%;" labelStyle="text-align:left;width:130px;"enabled="false"/>
					<input id="dealer"  name="myUserName" label="本方交易员：" class="mini-textbox" labelField="true" vtype="maxLength:10" style="width:100%;" labelStyle="text-align:left;width:130px;"enabled="false"/>
				</div>
				<div class="rightarea">
					<input id="counterpartyInstId"  name="counterpartyInstId" label="对方机构：" class="mini-textbox mini-mustFill" onbuttonclick="onButtonEdit" allowInput="false" labelField="true" style="width:100%;" labelStyle="text-align:left;width:130px;"  required="true" />
					<input id="counterpartyDealer"  name="counterpartyDealer" label="对方交易员：" class="mini-textbox" labelField="true" vtype="maxLength:6" style="width:100%;"labelStyle="text-align:left;width:130px;"/>
				</div>
			</div>
			<div class="mini-panel" title="交易主体信息" style="width:100%;"  allowResize="true" collapseOnTitleClick="true">
					<div class="leftarea">
						<input id="forDate"  name="forDate" label="成交日期：" class="mini-datepicker" labelField="true" style="width:100%;"labelStyle="text-align:left;width:130px;"/>
						<input style="width:100%;" id="currencyPair"  name="currencyPair" label="货币对：" class="mini-combobox mini-mustFill" required="true"  data="CommonUtil.serverData.dictionary.CurrencyPair" onvaluechanged="onCurrencyPairChange"  labelField="true" labelStyle="text-align:left;width:130px;"/>
						<input style="width:100%;" id="quantity"  name="quantity" label="数量："   onvaluechanged="quantityMoney"  class="mini-spinner mini-mustFill"  required="true"  changeOnMousewheel='false'  maxValue="9999999999999999.9999" format="n4"  labelField="true" labelStyle="text-align:left;width:130px;"/>
						<input style="width:100%;" id="amount1"  name="amount1" label="金额："  class="mini-spinner mini-mustFill" required="true"   changeOnMousewheel='false'  maxValue="9999999999999999.9999" format="n4"  labelField="true" labelStyle="text-align:left;width:130px;"/>
						<input style="width:100%;" id="direction2"  name="direction2" label="对方方向：" enabled="false" vtype="maxLength:6" class="mini-hidden" labelField="true" labelStyle="text-align:left;width:130px;"data = "CommonUtil.serverData.dictionary.trading" />
						<input id="sellCurreny" name="sellCurreny" class="mini-combobox mini-mustFill" required="true"  labelField="true" requiredErrorText="该输入项为必输项" required="true"  label="卖出货币："  style="width:100%;"  labelStyle="text-align:left;width:130px;" data="CommonUtil.serverData.dictionary.Currency" />
						<input id="tradingModel" name="tradingModel" class="mini-combobox mini-mustFill" required="true"  labelField="true"   label="交易模式：" labelStyle="text-align:left;width:130px;" style="width:100%;"  data="CommonUtil.serverData.dictionary.TradeModel"/>
						<input style="width:100%;" id="settleMode"  name="settleMode" label="清算方式：" vtype="maxLength:13" class="mini-textbox"  labelField="true" labelStyle="text-align:left;width:130px;"/>
					</div>
					<div class="rightarea">
						<input id="forTime"  name="forTime" label="交易时间：" class="mini-timespinner" labelField="true" style="width:100%;"labelStyle="text-align:left;width:130px;"/>
						<input style="width:100%;" id="direction1"  name="direction1" label="交易方向："  onvaluechanged="onCurrencyPairChange" vtype="maxLength:6" class="mini-combobox mini-mustFill" required="true"   labelField="true" labelStyle="text-align:left;width:130px;"data = "CommonUtil.serverData.dictionary.trading" />
						<input style="width:100%;" id="prix"  name="prix" label="价格："   onvaluechanged="quantityMoney"  class="mini-spinner mini-mustFill" required="true"   changeOnMousewheel='false'  maxValue="9999999999999999.9999" format="n4"  labelField="true" labelStyle="text-align:left;width:130px;"/>
						<input style="width:100%;" id="valueDate1"  name="valueDate1" label="起息日：" class="mini-datepicker mini-mustFill" labelField="true" required="true"   labelStyle="text-align:left;width:130px;"/>
						<input id="buyCurreny" name="buyCurreny" class="mini-combobox mini-mustFill" required="true"  labelField="true" requiredErrorText="该输入项为必输项" required="true"  label="买入货币："  style="width:100%;"  labelStyle="text-align:left;width:130px;" data="CommonUtil.serverData.dictionary.Currency" />
						<input id="tradingType" name="tradingType" class="mini-combobox mini-mustFill" labelField="true" requiredErrorText="该输入项为必输项" required="true"  label="交易方式："  value="RFQ" labelStyle="text-align:left;width:130px;" style="width:100%;" data="CommonUtil.serverData.dictionary.TradeType"/>
					</div>
			</div>
			<%@ include file="../../Common/opicsLessDetail.jsp"%>
		</div>
		<div  id="field_form5"  class="mini-fit" style="background:white" >
			<div class="mini-panel" title="黄金远期详情" style="width:100%;"  allowResize="true" collapseOnTitleClick="true">
					<div class="leftarea">
						<input style="width:100%;" id="contractId" name="contractId" class="mini-textbox mini-mustFill" labelField="true" requiredErrorText="该输入项为必输项" required="true"  label="成交单编号：" labelStyle="text-align:left;width:130px;" onvalidation="onEnglishAndNumberValidations" vtype="maxLength:20"/>
					</div>
				</div>
			<div class="mini-panel" title="成交双方信息" style="width:100%;"  allowResize="true" collapseOnTitleClick="true">
				<div class="leftarea">
					<input id="instId"  name="instId" label="本方机构：" class="mini-textbox" labelField="true" vtype="maxLength:50" style="width:100%;" labelStyle="text-align:left;width:130px;"enabled="false"/>
					<input id="dealer"  name="myUserName" label="本方交易员：" class="mini-textbox" labelField="true" vtype="maxLength:10" style="width:100%;" labelStyle="text-align:left;width:130px;"enabled="false"/>
				</div>
				<div class="rightarea">
					<input id="counterpartyInstId"  name="counterpartyInstId" label="对方机构：" class="mini-textbox mini-mustFill" onbuttonclick="onButtonEdit" allowInput="false" labelField="true" style="width:100%;" labelStyle="text-align:left;width:130px;"  required="true" />
					<input id="counterpartyDealer"  name="counterpartyDealer" label="对方交易员：" class="mini-textbox" labelField="true" vtype="maxLength:6" style="width:100%;"labelStyle="text-align:left;width:130px;"/>
				</div>
			</div>
			<div class="mini-panel" title="交易主体信息" style="width:100%;"  allowResize="true" collapseOnTitleClick="true">
					<div class="leftarea">
						<input id="forDate"  name="forDate" label="成交日期：" class="mini-datepicker" labelField="true" style="width:100%;"labelStyle="text-align:left;width:130px;"/>
						<input style="width:100%;" id="currencyPair"  name="currencyPair" label="货币对：" class="mini-combobox mini-mustFill" required="true"  data="CommonUtil.serverData.dictionary.CurrencyPair" onvaluechanged="onCurrencyPairChange"  labelField="true" labelStyle="text-align:left;width:130px;"/>
						<input style="width:100%;" id="quantity"  name="quantity" label="数量："   onvaluechanged="quantityMoney"  class="mini-spinner mini-mustFill"  required="true"  changeOnMousewheel='false'  maxValue="9999999999999999.9999" format="n4"  labelField="true" labelStyle="text-align:left;width:130px;"/>
						<input style="width:100%;" id="amount1"  name="amount1" label="金额："  class="mini-spinner mini-mustFill" required="true"   changeOnMousewheel='false'  maxValue="9999999999999999.9999" format="n4"  labelField="true" labelStyle="text-align:left;width:130px;"/>
						<input style="width:100%;" id="direction2"  name="direction2" label="对方方向：" enabled="false" vtype="maxLength:6" class="mini-hidden" labelField="true" labelStyle="text-align:left;width:130px;"data = "CommonUtil.serverData.dictionary.trading" />
						<input id="sellCurreny" name="sellCurreny" class="mini-combobox mini-mustFill" required="true"  labelField="true" requiredErrorText="该输入项为必输项" required="true"  label="卖出货币："  style="width:100%;"  labelStyle="text-align:left;width:130px;" data="CommonUtil.serverData.dictionary.Currency" />
						<input id="tradingModel" name="tradingModel" class="mini-combobox mini-mustFill" required="true"  labelField="true"   label="交易模式：" labelStyle="text-align:left;width:130px;" style="width:100%;"  data="CommonUtil.serverData.dictionary.TradeModel"/>
						<input style="width:100%;" id="settleMode"  name="settleMode" label="清算方式：" vtype="maxLength:13" class="mini-textbox"  labelField="true" labelStyle="text-align:left;width:130px;"/>
					</div>
					<div class="rightarea">
						<input id="forTime"  name="forTime" label="交易时间：" class="mini-timespinner" labelField="true" style="width:100%;"labelStyle="text-align:left;width:130px;"/>
						<input style="width:100%;" id="direction1"  name="direction1" label="交易方向："  onvaluechanged="onCurrencyPairChange" vtype="maxLength:6" class="mini-combobox mini-mustFill" required="true"   labelField="true" labelStyle="text-align:left;width:130px;"data = "CommonUtil.serverData.dictionary.trading" />
						<input style="width:100%;" id="prix"  name="prix" label="价格："   onvaluechanged="quantityMoney"  class="mini-spinner mini-mustFill" required="true"   changeOnMousewheel='false'  maxValue="9999999999999999.9999" format="n4"  labelField="true" labelStyle="text-align:left;width:130px;"/>
						<input style="width:100%;" id="valueDate1"  name="valueDate1" label="起息日：" class="mini-datepicker mini-mustFill" labelField="true" required="true"   labelStyle="text-align:left;width:130px;"/>
						<input id="buyCurreny" name="buyCurreny" class="mini-combobox mini-mustFill" required="true"  labelField="true" requiredErrorText="该输入项为必输项" required="true"  label="买入货币："  style="width:100%;"  labelStyle="text-align:left;width:130px;" data="CommonUtil.serverData.dictionary.Currency" />
						<input id="tradingType" name="tradingType" class="mini-combobox mini-mustFill" labelField="true" requiredErrorText="该输入项为必输项" required="true"  label="交易方式："  value="RFQ" labelStyle="text-align:left;width:130px;" style="width:100%;" data="CommonUtil.serverData.dictionary.TradeType"/>
					</div>
			</div>
			<%@ include file="../../Common/opicsLessDetail.jsp"%>
		</div>
		<div  id="field_form6"  class="mini-fit" style="background:white" >
			<div class="mini-panel" title="黄金掉期详情" style="width:100%;"  allowResize="true" collapseOnTitleClick="true">
					<div class="leftarea">
						<input style="width:100%;" id="contractId" name="contractId" class="mini-textbox mini-mustFill" labelField="true" requiredErrorText="该输入项为必输项" required="true"  label="成交单编号：" labelStyle="text-align:left;width:130px;" onvalidation="onEnglishAndNumberValidations" vtype="maxLength:20"/>
					</div>
				</div>
			<div class="mini-panel" title="成交双方信息" style="width:100%;"  allowResize="true" collapseOnTitleClick="true">
				<div class="leftarea">
					<input id="instId"  name="instId" label="本方机构：" class="mini-textbox" labelField="true" vtype="maxLength:50" style="width:100%;" labelStyle="text-align:left;width:130px;"enabled="false"/>
					<input id="dealer"  name="myUserName" label="本方交易员：" class="mini-textbox" labelField="true" vtype="maxLength:10" style="width:100%;" labelStyle="text-align:left;width:130px;"enabled="false"/>
				</div>
				<div class="rightarea">
					<input id="counterpartyInstId"  name="counterpartyInstId" label="对方机构：" class="mini-textbox mini-mustFill" onbuttonclick="onButtonEdit" allowInput="false" labelField="true" style="width:100%;" labelStyle="text-align:left;width:130px;"  required="true" />
					<input id="counterpartyDealer"  name="counterpartyDealer" label="对方交易员：" class="mini-textbox" labelField="true" vtype="maxLength:6" style="width:100%;"labelStyle="text-align:left;width:130px;"/>
				</div>
			</div>
			<div class="mini-panel" title="交易主体信息" style="width:100%;"  allowResize="true" collapseOnTitleClick="true">	
				<div class="leftarea">
					<input id="forDate"  name="forDate" label="成交日期：" class="mini-datepicker" labelField="true" style="width:100%;"labelStyle="text-align:left;width:130px;"/>
					<input style="width:100%;" id="currencyPair"  name="currencyPair" label="货币对：" class="mini-combobox mini-mustFill" onvaluechanged="onCurrencyPairChange" data="CommonUtil.serverData.dictionary.CurrencyPair" labelField="true" required="true"  labelStyle="text-align:left;width:130px;"/>
					<input style="width:100%;" id="quantity"  name="quantity" label="数量：" required="true"   onvaluechanged="quantityMoney"  class="mini-spinner mini-mustFill"   changeOnMousewheel='false'  maxValue="9999999999999999.9999" format="n4"  labelField="true" labelStyle="text-align:left;width:130px;"/>
					<input style="width:100%;" id="prix"  name="prix" label="近端价格：" required="true"   onvaluechanged="quantityMoney"  class="mini-spinner mini-mustFill"   changeOnMousewheel='false'  maxValue="9999999999999999.9999" format="n6"  labelField="true" labelStyle="text-align:left;width:130px;"/>
					<input style="width:100%;" id="amount1"  name="amount1" label="近端金额(即期)："  class="mini-spinner mini-mustFill" onvaluechanged="calOppoMoney"  changeOnMousewheel='false'  maxValue="9999999999999999.9999" format="n2"  labelField="true" required="true"  labelStyle="text-align:left;width:130px;"/>
	 				<input style="width:100%;" id="valueDate1"  name="valueDate1" label="起息日(即期)：" required="true"  class="mini-datepicker mini-mustFill" labelField="true"  labelStyle="text-align:left;width:130px;" ondrawdate="onDrawDateNear"/>
					<input id="sellCurreny" name="sellCurreny" class="mini-combobox mini-mustFill" labelField="true" requiredErrorText="该输入项为必输项" required="true"  label="卖出货币："  style="width:100%;"  labelStyle="text-align:left;width:130px;" data="CommonUtil.serverData.dictionary.metalccy" />
					<input id="tradingModel" name="tradingModel" class="mini-combobox mini-mustFill" required="true"  labelField="true"   label="交易模式：" labelStyle="text-align:left;width:130px;" style="width:100%;"  data="CommonUtil.serverData.dictionary.TradeModel"/>
					<input style="width:100%;" id="settleMode"  name="settleMode" label="清算方式：" class="mini-textbox" vtype="maxLength:13"  labelField="true" labelStyle="text-align:left;width:130px;"/>
				</div>			
				<div class="rightarea">
					<input id="forTime"  name="forTime" label="交易时间：" class="mini-timespinner" labelField="true" style="width:100%;"labelStyle="text-align:left;width:130px;"/>
					<input style="width:100%;" id="direction1"  name="direction1" label="交易方向：" onvaluechanged="onCurrencyPairChange" data = "CommonUtil.serverData.dictionary.trading" vtype="maxLength:6" class="mini-combobox mini-mustFill"  labelField="true" required="true"  labelStyle="text-align:left;width:130px;"/>
					<input style="width:100%;" id="direction2"  name="direction2" label="本方方向(远端)：" enabled="false" data = "CommonUtil.serverData.dictionary.trading" vtype="maxLength:6" class="mini-hidden"  labelField="true" labelStyle="text-align:left;width:130px;"/>
					<input style="width:100%;" id="direction4"  name="direction4" label="对方方向(远端)：" enabled="false" data = "CommonUtil.serverData.dictionary.trading" vtype="maxLength:6" class="mini-hidden"  labelField="true" labelStyle="text-align:left;width:130px;"/>
					<input style="width:100%;" id="spread"  name="spread" label="远端点差：" class="mini-spinner mini-mustFill"   changeOnMousewheel='false' maxValue="9999999999999999.99999999" format="n6" minValue="-1111111" labelField="true" required="true"  labelStyle="text-align:left;width:130px;"/>
					<input style="width:100%;" id="tenor2"  name="tenor2" label="远端价格：" class="mini-spinner mini-mustFill"  format="n6"  changeOnMousewheel='false' maxValue="9999999999999999.99999999"  labelField="true" required="true"  onvaluechanged="getFwdReversePrice" labelStyle="text-align:left;width:130px;"/>
	 				<input style="width:100%;" id="amount2"  name="amount2" label="远端金额(远期)：" class="mini-spinner mini-mustFill"   changeOnMousewheel='false'  maxValue="9999999999999999.9999" format="n2"  labelField="true" required="true"  onvaluechanged="getFwdReversePrice" labelStyle="text-align:left;width:130px;"/>
					<input style="width:100%;" id="valueDate2"  name="valueDate2" label="起息日(远期)：" required="true"  class="mini-datepicker" labelField="true"  labelStyle="text-align:left;width:130px;" ondrawdate="onDrawDateFar"/>
					<input id="buyCurreny" name="buyCurreny" class="mini-combobox mini-mustFill" labelField="true" requiredErrorText="该输入项为必输项" required="true"  label="买入货币："  style="width:100%;"  labelStyle="text-align:left;width:130px;" data="CommonUtil.serverData.dictionary.metalccy" />
					<input id="tradingType" name="tradingType" class="mini-combobox mini-mustFill" labelField="true" requiredErrorText="该输入项为必输项" required="true"  label="交易方式："  value="RFQ" labelStyle="text-align:left;width:130px;" style="width:100%;" data="CommonUtil.serverData.dictionary.TradeType"/>
				</div>
			</div>
			<%@ include file="../../Common/opicsLessDetail.jsp"%>
		</div>
	</div>
	</div>
    <div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
          <div style="margin-bottom:10px; text-align: center;">
              <a class="mini-button" style="display: none"  style="width:120px;" id="save_btn"  onclick="save">保存</a>
          </div>
          <div style="margin-bottom:10px; text-align: center;">
              <a class="mini-button" style="display: none"  style="width:120px;" id="cancel_btn"  onclick="cancel">取消</a>
          </div>
          
        </div>    
        
    </div>
      <script type="text/javascript">
        mini.parse();

        //获取当前tab
        var currTab = top["win"].tabs.getActiveTab();
        var params = currTab.params;
        var row=params.selectedData;
        var url = window.location.search;
        var action = CommonUtil.getParam(url, "action");
        var form = new mini.Form("#field_form");
        var form1 = new mini.Form("#field_form1");
        var form2 = new mini.Form("#field_form2");
        var form3 = new mini.Form("#field_form3");
        var form4 = new mini.Form("#field_form4");
        var form5 = new mini.Form("#field_form5");
        var form6 = new mini.Form("#field_form6");
        
        var tradeData={};
    	
    	tradeData.selectData=row;
    	tradeData.operType=action;
    	tradeData.serial_no=row.ticketId;
    	tradeData.task_id=row.taskId;

        $(document).ready(function () {
			//控制按钮显示
			$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
				initData();
			});
        });
        
       

        //初始化数据
        function initData() {
        	
          if ($.inArray(action, ["edit", "detail","approve"]) > -1) { 
        	  form.setData(row);
        	  if($.inArray(action,["approve","detail"])>-1){
	      			mini.get("save_btn").setVisible(false);
	      			form.setEnabled(false);
	      			var form11=new mini.Form("#approve_operate_form");
	      			form11.setEnabled(true);
      			}
        	  if(mini.get("dealType").getValue() == "IFS_CFETSFX_SWAP"){//外汇掉期
   				$('#field_form2').hide();
   				$('#field_form3').hide();
   				$('#field_form4').hide();
   				$('#field_form5').hide();
   				$('#field_form6').hide();
   				form1.setEnabled(false);
   				search1();
   			 }else if(mini.get("dealType").getValue() == "IFS_CFETSFX_FWD"){//外汇远期
   				$('#field_form1').hide();
   				$('#field_form3').hide();
   				$('#field_form4').hide();
   				$('#field_form5').hide();
   				$('#field_form6').hide();
   				form2.setEnabled(false);
   				search2();
   			 }else if(mini.get("dealType").getValue() == "IFS_CFETSFX_SPT"){//外汇即期
   				$('#field_form1').hide();
   				$('#field_form2').hide();
   				$('#field_form4').hide();
   				$('#field_form5').hide();
   				$('#field_form6').hide();
   				form3.setEnabled(false);
   				search3();  
   			 }else if(mini.get("dealType").getValue() == "spt"){//黄金即期 
    				$('#field_form1').hide();
       				$('#field_form2').hide();
       				$('#field_form3').hide();
       				$('#field_form5').hide();
       				$('#field_form6').hide();
       				form4.setEnabled(false);
       				search4(); 
       		 }else if(mini.get("dealType").getValue() == "fwd"){//黄金远期 
 				$('#field_form1').hide();
   				$('#field_form2').hide();
   				$('#field_form3').hide();
   				$('#field_form4').hide();
   				$('#field_form6').hide();
   				form5.setEnabled(false);
   				search5(); 
   		 	 }else{//黄金掉期 
   		 		$('#field_form1').hide();
   				$('#field_form2').hide();
   				$('#field_form3').hide();
   				$('#field_form4').hide();
   				$('#field_form5').hide();
   				form6.setEnabled(false);
   				search6(); 
   		 	 }
        	  
        	  
        	  
          } else { //增加 

             form.setData(row);
             mini.get("fedealno").setValue(row.ticketId);
             mini.get("ticketId").setValue("");
             mini.get("sponsor").setValue("<%=__sessionUser.getUserId()%>");
 			 mini.get("sponInst").setValue("<%=__sessionUser.getInstId() %>");
 			if(mini.get("dealType").getValue() == "IFS_CFETSFX_SWAP"){//外汇掉期
 				$('#field_form2').hide();
 				$('#field_form3').hide();
 				$('#field_form4').hide();
   				$('#field_form5').hide();
   				$('#field_form6').hide();
 				form1.setEnabled(false);
 				search1();
 			 }else if(mini.get("dealType").getValue() == "IFS_CFETSFX_FWD"){//外汇远期
 				$('#field_form1').hide();
 				$('#field_form3').hide();
 				$('#field_form4').hide();
   				$('#field_form5').hide();
   				$('#field_form6').hide();
 				form2.setEnabled(false);
 				search2();
 			 }else if(mini.get("dealType").getValue() == "IFS_CFETSFX_SPT"){//外汇即期
 				$('#field_form1').hide();
 				$('#field_form2').hide();
 				$('#field_form4').hide();
   				$('#field_form5').hide();
   				$('#field_form6').hide();
 				form3.setEnabled(false);
 				search3(); 
 			 }else if(mini.get("dealType").getValue() == "spt"){//黄金即期 
 				$('#field_form1').hide();
   				$('#field_form2').hide();
   				$('#field_form3').hide();
   				$('#field_form5').hide();
   				$('#field_form6').hide();
   				form4.setEnabled(false);
   				search4(); 
   		 	}else if(mini.get("dealType").getValue() == "fwd"){//黄金远期 
				$('#field_form1').hide();
				$('#field_form2').hide();
				$('#field_form3').hide();
				$('#field_form4').hide();
				$('#field_form6').hide();
				form5.setEnabled(false);
				search5(); 
		 	 }else{//黄金掉期 
		 		$('#field_form1').hide();
				$('#field_form2').hide();
				$('#field_form3').hide();
				$('#field_form4').hide();
				$('#field_form5').hide();
				form6.setEnabled(false);
				search6(); 
		 	 }
 			
 			
          }
        }
      //外汇掉期
        function search1(){
     	    var ticketId =  mini.get("fedealno").getValue();
            var data = {};
            data['ticketId']=ticketId;
     	   var params = mini.encode(data);
     	   CommonUtil.ajax({
                url : "/IfsForeignController/searchrmswap",
                data : params,
                callback : function(data) {
                	if(data.obj){
                   		form1.setData(data.obj);
                   		mini.getByName("counterpartyInstId",form1).setValue(data.obj.counterpartyInstId);
                   		queryTextName(data.obj.counterpartyInstId,form1);
						mini.getByName("instId",form1).setValue(data.obj.instfullname);
						mini.getByName("dealer",form1).setValue(data.obj.myUserName);
						swapChangeAcct(data.obj.buyCurreny,data.obj.sellCurreny,data.obj.buyCurreny1,data.obj.sellCurreny1,form1,"#field_form1 ");
                	}
                }
            });
        }
      //外汇远期
        function search2(){
     	    var ticketId =  mini.get("fedealno").getValue();
            var data = {};
            data['ticketId']=ticketId;
     	   var params = mini.encode(data);
     	   CommonUtil.ajax({
                url : "/IfsForeignController/searchCredit",
                data : params,
                callback : function(data) {
                	if(data.obj){
                   		form2.setData(data.obj);
                   		mini.getByName("counterpartyInstId",form2).setValue(data.obj.counterpartyInstId);
                   		queryTextName(data.obj.counterpartyInstId,form2);
						mini.getByName("instId",form2).setValue(data.obj.instfullname);
						mini.getByName("dealer",form2).setValue(data.obj.myUserName);
                   		sptFwdChangeAcct(data.obj.buyCurreny,data.obj.sellCurreny,form2,"#field_form2 ");
                	}
                }
            });
        }
      //外汇即期
        function search3(){
     	    var ticketId =  mini.get("fedealno").getValue();
            var data = {};
            data['ticketId']=ticketId;
     	   var params = mini.encode(data);
     	   CommonUtil.ajax({
                url : "/IfsForeignController/searchSpot",
                data : params,
                callback : function(data) {
                	if(data.obj){
                   		form3.setData(data.obj);
                   		mini.getByName("counterpartyInstId",form3).setValue(data.obj.counterpartyInstId);
                   		queryTextName(data.obj.counterpartyInstId,form3);
						mini.getByName("instId",form3).setValue(data.obj.instfullname);
						mini.getByName("dealer",form3).setValue(data.obj.myUserName);
                   		sptFwdChangeAcct(data.obj.buyCurreny,data.obj.sellCurreny,form3,"#field_form3 ");
                	}
                }
            });
        }
      
        //黄金即期
        function search4(){
     	    var ticketId =  mini.get("fedealno").getValue();
            var data = {};
            data['ticketId']=ticketId;
            data['type']="spt";
     	   var params = mini.encode(data);
     	   CommonUtil.ajax({
                url : "/IfsCfetsmetalGoldController/searchGold",
                data : params,
                callback : function(data) {
                	if(data.obj){
                   		form4.setData(data.obj);
                   		mini.getByName("counterpartyInstId",form4).setValue(data.obj.counterpartyInstId);
                   		queryTextName(data.obj.counterpartyInstId,form4);
						mini.getByName("instId",form4).setValue(data.obj.instfullname);
						mini.getByName("dealer",form4).setValue(data.obj.myUserName);
                	}
                }
            });
        }
      //黄金远期
        function search5(){
     	    var ticketId =  mini.get("fedealno").getValue();
            var data = {};
            data['ticketId']=ticketId;
            data['type']="fwd";
     	   var params = mini.encode(data);
     	   CommonUtil.ajax({
                url : "/IfsCfetsmetalGoldController/searchGold",
                data : params,
                callback : function(data) {
                	if(data.obj){
                   		form5.setData(data.obj);
                   		mini.getByName("counterpartyInstId",form5).setValue(data.obj.counterpartyInstId);
                   		queryTextName(data.obj.counterpartyInstId,form5);
						mini.getByName("instId",form5).setValue(data.obj.instfullname);
						mini.getByName("dealer",form5).setValue(data.obj.myUserName);
                	}
                }
            });
        }
      //黄金掉期
        function search6(){
     	    var ticketId =  mini.get("fedealno").getValue();
            var data = {};
            data['ticketId']=ticketId;
            data['type']="swap";
     	   var params = mini.encode(data);
     	   CommonUtil.ajax({
                url : "/IfsCfetsmetalGoldController/searchGold",
                data : params,
                callback : function(data) {
                	if(data.obj){
                   		form6.setData(data.obj);
                   		mini.getByName("counterpartyInstId",form6).setValue(data.obj.counterpartyInstId);
                   		queryTextName(data.obj.counterpartyInstId,form6);
						mini.getByName("instId",form6).setValue(data.obj.instfullname);
						mini.getByName("dealer",form6).setValue(data.obj.myUserName);
                	}
                }
            });
        }
      
        
        //保存
        function save() {
          //表单验证！！！
          form.validate();
          if (form.isValid() == false) {
            return;
          }
          var saveUrl = null;
            saveUrl = "/IfsRevlfxdController/saveForexParam";
            var data = form.getData(true);
            var params = mini.encode(data);
            CommonUtil.ajax({
              url: saveUrl,
              data: params,
              callback: function (data) {
                if (data.code == 'error.common.0000') {
	                  mini.alert("保存成功", '提示信息', function () {
	                    top["fxlfxdManage"].query();
	                    top["win"].closeMenuTab();
	                  });
                } else {
                  mini.alert("保存失败");
                }
              }
            });
          }

        function cancel() {
	          top["fxlfxdManage"].query();
	          top["win"].closeMenuTab();
        }

        function swapChangeAcct(buyCurreny,sellCurreny,buyCurreny1,sellCurreny1,form,formId){
			   $(formId+"#bankBic1").show();
			   $(formId+"#intermediaryBankName1").show();
			   $(formId+"#intermediaryBankBicCode1").show();
			   $(formId+"#bankBic2").show();
			   $(formId+"#intermediaryBankName2").show();
			   $(formId+"#intermediaryBankBicCode2").show();
			   $(formId+"#bankAccount1").show();
			   $(formId+"#bankAccount2").show();
			   $(formId+"#intermediaryBankAcctNo1").show();
			   $(formId+"#intermediaryBankAcctNo2").show();
			   if(buyCurreny=="CNY"){
				   mini.getByName("bankName1",form).setLabel("资金账户户名：");
				   mini.getByName("dueBank1",form).setLabel("资金开户行：");
				   mini.getByName("dueBankName1",form).setLabel("支付系统行号：");
				   mini.getByName("dueBankAccount1",form).setLabel("资金账号：");
				   $(formId+"#bankBic1").hide();
				   $(formId+"#intermediaryBankName1").hide();
				   $(formId+"#intermediaryBankBicCode1").hide();
				   $(formId+"#bankAccount1").hide();
				   $(formId+"#intermediaryBankAcctNo1").hide();
				   
			   }else if(sellCurreny=="CNY"){
				   mini.getByName("bankName2",form).setLabel("资金账户户名：");
				   mini.getByName("dueBank2",form).setLabel("资金开户行：");
				   mini.getByName("dueBankName2",form).setLabel("支付系统行号：");
				   mini.getByName("dueBankAccount2",form).setLabel("资金账号：");
				   $(formId+"#bankBic2").hide();
				   $(formId+"#intermediaryBankName2").hide();
				   $(formId+"#intermediaryBankBicCode2").hide();
				   $(formId+"#bankAccount2").hide();
				   $(formId+"#intermediaryBankAcctNo2").hide();
			   }
			   
			   $(formId+"#bankBic3").show();
			   $(formId+"#intermediaryBankName3").show();
			   $(formId+"#intermediaryBankBicCode3").show();
			   $(formId+"#bankBic4").show();
			   $(formId+"#intermediaryBankName4").show();
			   $(formId+"#intermediaryBankBicCode4").show();
			   $(formId+"#bankAccount3").show();
			   $(formId+"#bankAccount4").show();
			   $(formId+"#intermediaryBankAcctNo3").show();
			   $(formId+"#intermediaryBankAcctNo4").show();
			   if(buyCurreny1=="CNY"){
				   mini.getByName("bankName3",form).setLabel("资金账户户名：");
				   mini.getByName("dueBank3",form).setLabel("资金开户行：");
				   mini.getByName("dueBankName3",form).setLabel("支付系统行号：");
				   mini.getByName("dueBankAccount3",form).setLabel("资金账号：");
				   $(formId+"#bankBic3").hide();
				   $(formId+"#intermediaryBankName3").hide();
				   $(formId+"#intermediaryBankBicCode3").hide();
				   $(formId+"#bankAccount3").hide();
				   $(formId+"#intermediaryBankAcctNo3").hide();
				   
			   }else if(sellCurreny1=="CNY"){
				   mini.getByName("bankName4",form).setLabel("资金账户户名：");
				   mini.getByName("dueBank4",form).setLabel("资金开户行：");
				   mini.getByName("dueBankName4",form).setLabel("支付系统行号：");
				   mini.getByName("dueBankAccount4",form).setLabel("资金账号：");
				   $(formId+"#bankBic4").hide();
				   $(formId+"#intermediaryBankName4").hide();
				   $(formId+"#intermediaryBankBicCode4").hide();
				   $(formId+"#bankAccount4").hide();
				   $(formId+"#intermediaryBankAcctNo4").hide();
			   }
		   }
      //根据交易对手编号查询全称
		function queryTextName(cno,form){
			CommonUtil.ajax({
			    url: "/IfsOpicsCustController/searchIfsOpicsCust",
			    data : {'cno' : cno},
			    callback:function (data) {
			    	if (data && data.obj) {
			    		mini.getByName("counterpartyInstId",form).setText(data.obj.cliname);
					} else {
						mini.getByName("counterpartyInstId",form).setText();
			    }
			    }
			});
		}
		function sptFwdChangeAcct(buyCurreny,sellCurreny,form,formId){
		 	   $(formId+"#bankBic1").show();
		 	   $(formId+"#intermediaryBankName1").show();
		 	   $(formId+"#intermediaryBankBicCode1").show();
		 	   $(formId+"#bankBic2").show();
		 	   $(formId+"#intermediaryBankName2").show();
		 	   $(formId+"#intermediaryBankBicCode2").show();
		 	   $(formId+"#bankAccount1").show();
		 	   $(formId+"#bankAccount2").show();
		 	   $(formId+"#intermediaryBankAcctNo1").show();
		 	   $(formId+"#intermediaryBankAcctNo2").show();
		 	   
		 	   if(buyCurreny=="CNY"){
		 		  mini.getByName("bankName1",form).setLabel("资金账户户名：");
		 		  mini.getByName("dueBank1",form).setLabel("资金开户行：");
		 		  mini.getByName("dueBankName1",form).setLabel("支付系统行号：");
		 		  mini.getByName("capitalAccount1",form).setLabel("资金账号：");
		 		   $(formId+"#bankBic1").hide();
		 		   $(formId+"#intermediaryBankName1").hide();
		 		   $(formId+"#intermediaryBankBicCode1").hide();
		 		   $(formId+"#bankAccount1").hide();
		 		   $(formId+"#intermediaryBankAcctNo1").hide();
		 		   
		 	   }else if(sellCurreny=="CNY"){
		 		   mini.getByName("bankName2",form).setLabel("资金账户户名：");
		 		   mini.getByName("dueBank2",form).setLabel("资金开户行：");
		 		   mini.getByName("dueBankName2",form).setLabel("支付系统行号：");
		 		   mini.getByName("capitalAccount2",form).setLabel("资金账号：");
		 		   $(formId+"#bankBic2").hide();
		 		   $(formId+"#intermediaryBankName2").hide();
		 		   $(formId+"#intermediaryBankBicCode2").hide();
		 		   $(formId+"#bankAccount2").hide();
		 		   $(formId+"#intermediaryBankAcctNo2").hide();
		 	   }
		    }

		    function adateCheck(e){
        		if(mini.parseDate(row.forDate) > e.value){
        			mini.alert("冲销日期不得早于成交日期","提示");
					mini.get("adate").setValue('');
				}
			}
        
      </script>
      <script type="text/javascript" src="../../Common/Flow/MiniApproveOpCommon.js"></script>
    </body>

    </html>