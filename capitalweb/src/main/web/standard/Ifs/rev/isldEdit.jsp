<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../../global.jsp" %>
<html>
<head>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title></title>
</head>

<body style="width:100%;height:100%;background:white">
<div class="mini-splitter" style="width:100%;height:100%;">
    <div size="90%" showCollapseButton="false">
        <div id="field_form" class="mini-panel" title="债券类冲销信息" style="width:100%" allowResize="true"
             collapseOnTitleClick="true">
            <div class="leftarea">
                <input id="sponsor" name="sponsor" class="mini-hidden"/>
                <input id="sponInst" name="sponInst" class="mini-hidden"/>
                <input id="ticketId" name="ticketId" enabled="false" class="mini-textbox" label="审批单流水号："
                       labelField="true" emptyText="系统自动生成"  vtype="maxLength:35" style="width:100%"
                       labelStyle="text-align:left;width:170px"/>
                <input id="fedealno" name="fedealno" class="mini-textbox" enabled="false" label="交易前端流水号："
                       emptyText="请输入交易前端流水号" labelField="true" style="width:100%"
                       labelStyle="text-align:left;width:170px"/>
                <input id="adate" name="adate" class="mini-datepicker mini-mustFill" label="冲销日期：" required="true"
                       emptyText="请输入冲销日期" labelField="true" style="width:100%" labelStyle="text-align:left;width:170px"
                       vtype="maxLength:35" onvaluechanged="adateCheck"/>
                <input id="revreason" name="revreason" class="mini-textarea" label="冲销原因：" emptyText="请输入冲销原因"
                       labelField="true" style="width:100%" labelStyle="text-align:left;width:170px"/>
            </div>
            <div class="rightarea">
                <input style="width:100%;" id="dealType" name="dealType" enabled="false" class="mini-combobox"
                       labelField="true" label="业务类型：" data="CommonUtil.serverData.dictionary.ISLDYYPE"
                       labelStyle="text-align:left;width:170px"/>
                <input id="dealNo" name="dealNo" enabled="false" class="mini-textbox" label="OPICS交易号："
                       emptyText="请输入OPICS交易号" labelField="true" style="width:100%"
                       labelStyle="text-align:left;width:170px"/>
                <input id="br" name="br" class="mini-combobox mini-mustFill" label="部门：" data="[{id:'01',text:'01'}]"
                       emptyText="请选择部门" labelField="true" style="width:100%" labelStyle="text-align:left;width:170px"
                       vtype="maxLength:50" required="true" />
            </div>
            <%@ include file="../../Common/Flow/MiniApproveOpCommon.jsp" %>
        </div>


        <div id="field_form1" class="mini-fit area" style="background:white">
            <div class="mini-panel" title="现券买卖详情" style="width:100%;" allowResize="true" collapseOnTitleClick="true">
                <div class="leftarea">
                    <input style="width:100%;" id="contractId" name="contractId" class="mini-textbox mini-mustFill"
                           labelField="true" requiredErrorText="该输入项为必输项" required="true"  label="成交单编号："
                           labelStyle="text-align:left;width:130px;" onvalidation="onEnglishAndNumberValidations"
                           vtype="maxLength:20"/>
                </div>
            </div>
            <div class="mini-panel" title="成交双方信息" style="width:100%" allowResize="true" collapseOnTitleClick="true">
                <div class="leftarea">
                    <fieldset>
                        <legend>本方信息</legend>
                        <input id="myDir" name="myDir" class="mini-combobox" labelField="true" label="本方方向："
                               required="true"  onvaluechanged="dirVerify" style="width:100%;"
                               labelStyle="text-align:left;width:130px;"
                               data="CommonUtil.serverData.dictionary.trading"/>
                        <input id="buyInst" name="buyInst" class="mini-textbox" labelField="true" label="机构："
                               required="true"  style="width:100%;" enabled="false"
                               labelStyle="text-align:left;width:130px;"/>
                        <input id="buyTrader" name="buyTrader" class="mini-textbox" labelField="true" label="交易员："
                               required="true"  vtype="maxLength:5" enabled="false"
                               labelStyle="text-align:left;width:130px;" style="width:100%;"/>
                    </fieldset>
                </div>
                <div class="leftarea">
                    <fieldset>
                        <legend>对手方信息</legend>
                        <input id="oppoDir" name="oppoDir" class="mini-combobox" labelField="true" label="对方方向："
                               required="true"  enabled="false" style="width:100%;"
                               labelStyle="text-align:left;width:130px;"
                               data="CommonUtil.serverData.dictionary.trading"/>
                        <input id="sellInst" name="sellInst" class="mini-textbox" labelField="true" label="机构："
                               required="true"  style="width:100%;" labelStyle="text-align:left;width:130px;"/>
                        <input id="sellTrader" name="sellTrader" class="mini-textbox" labelField="true" label="交易员："
                               required="true"  vtype="maxLength:5" labelStyle="text-align:left;width:130px;"
                               style="width:100%;"/>
                    </fieldset>
                </div>
            </div>
            <div class="mini-panel" title="交易主体信息" style="width:100%" allowResize="true" collapseOnTitleClick="true">
                <div class="leftarea">
                    <input style="width:100%;" id="forDate" name="forDate" class="mini-datepicker mini-mustFill"
                           labelField="true" required="true"  label="成交日期：" labelStyle="text-align:left;width:120px;"/>
                    <input style="width:100%" id="bondCode" name="bondCode" class="mini-textbox mini-mustFill"
                           onbuttonclick="onBondQuery" labelField="true" label="债券代码：" required="true"
                           allowInput="false" labelStyle="text-align:left;width:120px;"/>
                    <input style="width:100%;display:none" id="startDate" name="startDate"
                           class="mini-textbox mini-mustFill" labelField="true" label="起息日：" required="true"
                           vtype="maxLength:10" labelStyle="text-align:left;width:120px;" enabled="false"/>
                    <input style="width:100%;display:none" id="settdays" name="settdays"
                           class="mini-textbox mini-mustFill" labelField="true" label="结算天数：" required="true"
                           vtype="maxLength:10" labelStyle="text-align:left;width:120px;" enabled="false"/>
                    <input id="invtype" name="invtype" class="mini-combobox mini-mustFill" labelField="true"
                           label="投资类型：" style="width:100%;" labelStyle="text-align:left;width:120px;"
                           data="CommonUtil.serverData.dictionary.invtype"/>
                    <input style="width:100%" id="cleanPrice" name="cleanPrice" class="mini-spinner mini-mustFill"
                           labelField="true" label="净价(元)：" required="true"  onvaluechanged="onAccuredInterestChanged"
                           minValue="0" maxValue="9999999999999999.9999" format="n4" changeOnMousewheel="false"
                           labelStyle="text-align:left;width:120px;"/>
                    <input style="width:100%" id="yield" name="yield" class="mini-spinner mini-mustFill"
                           labelField="true" label="到期收益率(%)：" required="true"  minValue="0"
                           maxValue="9999999999999999.99999999" format="n8" changeOnMousewheel="false"
                           labelStyle="text-align:left;width:120px;"/>
                    <input style="width:100%" id="accuredInterest" name="accuredInterest"
                           class="mini-spinner mini-mustFill" labelField="true" label="应计利息(元)：" required="true"
                           onvaluechanged="onAccuredInterestChanged" minValue="0" maxValue="9999999999999999.9999"
                           format="n4" changeOnMousewheel="false" labelStyle="text-align:left;width:120px;"/>
                    <input style="width:100%" id="dirtyPrice" name="dirtyPrice" class="mini-spinner mini-mustFill"
                           labelField="true" label="全价(元)：" required="true"  minValue="0"
                           maxValue="9999999999999999.9999" format="n4" changeOnMousewheel="false"
                           labelStyle="text-align:left;width:120px;"/>
                    <input style="width:100%" id="settlementDate" name="settlementDate"
                           class="mini-datepicker mini-mustFill" labelField="true" label="结算日：" required="true"
                           labelStyle="text-align:left;width:120px;" ondrawdate="settlementDate"
                           onvaluechanged="searchProductWeightNew()"/>
                </div>
                <div class="rightarea">
                    <input id="nettingStatus" name="nettingStatus" class="mini-combobox mini-mustFill" required="true"
                           labelField="true" label="净额清算状态：" labelStyle="text-align:left;width:120px;"
                           style="width:100%;" data="CommonUtil.serverData.dictionary.YesNo" showNullItem="true"/>
                    <input style="width:100%" id="bondName" name="bondName" class="mini-textbox mini-mustFill"
                           labelField="true" label="债券名称：" required="true"  vtype="maxLength:10"
                           labelStyle="text-align:left;width:120px;" enabled="false"/>
                    <input style="width:100%" id="totalFaceValue" name="totalFaceValue"
                           class="mini-spinner mini-mustFill" labelField="true" label="券面总额(万元)：" required="true"
                           onvaluechanged="onAccuredInterestChanged" minValue="0" maxValue="9999999999999999.9999"
                           format="n4" changeOnMousewheel="false" labelStyle="text-align:left;width:120px;"
                           onvalidation="zeroValidation"/>
                    <input style="width:100%" id="tradeAmount" name="tradeAmount" class="mini-spinner mini-mustFill"
                           labelField="true" label="交易金额(元)：" required="true"  onvaluechanged="calSettleAmount"
                           minValue="0" maxValue="9999999999999999.9999" format="n4" changeOnMousewheel="false"
                           labelStyle="text-align:left;width:120px;" onvalidation="zeroValidation"/>
                    <input style="width:100%" id="totalAccuredInterest" name="totalAccuredInterest"
                           class="mini-spinner mini-mustFill" labelField="true" label="应计利息总额(元)：" required="true"
                           onvaluechanged="calSettleAmount" minValue="0" maxValue="9999999999999999.9999" format="n4"
                           changeOnMousewheel="false" labelStyle="text-align:left;width:120px;"/>
                    <input style="width:100%" id="settlementAmount" name="settlementAmount"
                           class="mini-spinner mini-mustFill" labelField="true" label="结算金额(元)：" required="true"
                           minValue="0" maxValue="9999999999999999.9999" format="n4" changeOnMousewheel="false"
                           labelStyle="text-align:left;width:120px;" onvalidation="zeroValidation"/>
                    <input style="width:100%" id="settlementMethod" name="settlementMethod"
                           class="mini-combobox mini-mustFill" labelField="true" label="结算方式：" required="true"
                           data="CommonUtil.serverData.dictionary.SettlementMethod"
                           labelStyle="text-align:left;width:120px;"/>
                    <input id="tradingModel" name="tradingModel" class="mini-combobox mini-mustFill" required="true"
                           labelField="true" label="交易模式：" labelStyle="text-align:left;width:120px;" style="width:100%;"
                           data="CommonUtil.serverData.dictionary.TradeModel"/>
                </div>
            </div>
            <%@ include file="../../Common/opicsLessDetail.jsp" %>
            <%@ include file="../../Common/RiskCenterDetail.jsp" %>
            <div class="mini-panel" title="清算信息" style="width:100%" allowResize="true" collapseOnTitleClick="true">
                <div class="leftarea">
                    <fieldset>
                        <legend>本方账户</legend>
                        <input id="buyAccname" name="buyAccname" class="mini-textbox" labelField="true" label="资金账户户名："
                               vtype="maxLength:25" style="width:100%;" labelStyle="text-align:left;width:130px;"/>
                        <input id="buyOpbank" name="buyOpbank" class="mini-textbox" labelField="true" label="资金开户行："
                               vtype="maxLength:25" labelStyle="text-align:left;width:130px;" style="width:100%;"/>
                        <input id="buyAccnum" name="buyAccnum" class="mini-textbox" labelField="true" label="资金账号："
                               onvalidation="onEnglishAndNumberValidation" vtype="maxLength:50"
                               labelStyle="text-align:left;width:130px;" style="width:100%;"/>
                        <input id="buyPsnum" name="buyPsnum" class="mini-textbox" labelField="true" label="支付系统行号："
                               onvalidation="onEnglishAndNumberValidation" vtype="maxLength:20"
                               labelStyle="text-align:left;width:130px;" style="width:100%;"/>
                        <input id="buyCaname" name="buyCaname" class="mini-textbox" labelField="true" label="托管账户户名："
                               vtype="maxLength:25" labelStyle="text-align:left;width:130px;" style="width:100%;"/>
                        <input id="buyCustname" name="buyCustname" class="mini-textbox mini-mustFill"
                               onbuttonclick="onSaccQuery" required="true"  labelField="true" allowInput="false"
                               label="托管机构：" vtype="maxLength:25" labelStyle="text-align:left;width:130px;"
                               style="width:100%;"/>
                        <input id="buyCanum" name="buyCanum" class="mini-textbox" labelField="true" label="托管帐号："
                               onvalidation="onEnglishAndNumberValidation" vtype="maxLength:20"
                               labelStyle="text-align:left;width:130px;" style="width:100%;"/>
                    </fieldset>
                </div>
                <div class="rightarea">
                    <fieldset>
                        <legend>对手方账户</legend>
                        <input id="sellAccname" name="sellAccname" class="mini-textbox" labelField="true"
                               label="资金账户户名：" vtype="maxLength:25" style="width:100%;"
                               labelStyle="text-align:left;width:130px;"/>
                        <input id="sellOpbank" name="sellOpbank" class="mini-textbox" labelField="true" label="资金开户行："
                               vtype="maxLength:25" labelStyle="text-align:left;width:130px;" style="width:100%;"/>
                        <input id="sellAccnum" name="sellAccnum" class="mini-textbox" labelField="true" label="资金账号："
                               onvalidation="onEnglishAndNumberValidation" vtype="maxLength:50"
                               labelStyle="text-align:left;width:130px;" style="width:100%;"/>
                        <input id="sellPsnum" name="sellPsnum" class="mini-textbox" labelField="true" label="支付系统行号："
                               onvalidation="onEnglishAndNumberValidation" vtype="maxLength:20"
                               labelStyle="text-align:left;width:130px;" style="width:100%;"/>
                        <input id="sellCaname" name="sellCaname" class="mini-textbox" labelField="true" label="托管账户户名："
                               vtype="maxLength:25" labelStyle="text-align:left;width:130px;" style="width:100%;"/>
                        <input id="sellCustname" name="sellCustname" class="mini-textbox mini-mustFill"
                               onbuttonclick="onSaccQuery" required="true"  labelField="true" allowInput="false"
                               label="托管机构：" vtype="maxLength:25" labelStyle="text-align:left;width:130px;"
                               style="width:100%;"/>
                        <input id="sellCanum" name="sellCanum" class="mini-textbox" labelField="true" label="托管帐号："
                               onvalidation="onEnglishAndNumberValidation" vtype="maxLength:20"
                               labelStyle="text-align:left;width:130px;" style="width:100%;"/>
                    </fieldset>
                </div>
            </div>
        </div>
        <div id="field_form2" class="mini-fit area" style="background:white">
            <div class="mini-panel" title="债券借贷详情" style="width:100%;" allowResize="true" collapseOnTitleClick="true">
                <div class="leftarea">
                    <input style="width:100%;" id="contractId" name="contractId" class="mini-textbox mini-mustFill"
                           labelField="true" requiredErrorText="该输入项为必输项" required="true"  label="成交单编号："
                           labelStyle="text-align:left;width:130px;" onvalidation="onEnglishAndNumberValidations"
                           vtype="maxLength:20"/>
                </div>
            </div>
            <div class="mini-panel" title="成交双方信息" style="width:100%" allowResize="true" collapseOnTitleClick="true">
                <div class="leftarea">
                    <fieldset>
                        <legend>本方信息</legend>
                        <input id="myDir" name="myDir" class="mini-combobox" labelField="true"
                               onvaluechanged="dirVerify" required="true"  label="本方方向：" style="width:100%;"
                               labelStyle="text-align:left;width:130px;"
                               data="CommonUtil.serverData.dictionary.trading"/>
                        <input id="borrowInst" name="borrowInst" class="mini-textbox" labelField="true" label="机构："
                               required="true"  style="width:100%;" enabled="false"
                               labelStyle="text-align:left;width:130px;"/>
                        <input id="borrowTrader" name="borrowTrader" class="mini-textbox" labelField="true" label="交易员："
                               required="true"  vtype="maxLength:5" enabled="false"
                               labelStyle="text-align:left;width:130px;" style="width:100%;"/>
                    </fieldset>
                </div>
                <div class="rightarea">
                    <fieldset>
                        <legend>对手方信息</legend>
                        <input id="oppoDir" name="oppoDir" class="mini-combobox" enabled="false" labelField="true"
                               required="true"  label="对方方向：" style="width:100%;"
                               labelStyle="text-align:left;width:130px;"
                               data="CommonUtil.serverData.dictionary.trading"/>
                        <input id="lendInst" name="lendInst" class="mini-textbox" required="true"  labelField="true"
                               label="机构：" vtype="maxLength:50" style="width:100%;"
                               labelStyle="text-align:left;width:130px;"/>
                        <input id="lendTrader" name="lendTrader" class="mini-textbox" labelField="true" label="交易员："
                               required="true"  vtype="maxLength:10" labelStyle="text-align:left;width:130px;"
                               style="width:100%;"/>
                    </fieldset>
                </div>
            </div>
            <div class="mini-panel" title="交易主体信息" style="width:100%" allowResize="true" collapseOnTitleClick="true">
                <div class="leftarea">
                    <input style="width:100%;" id="forDate" name="forDate" class="mini-datepicker mini-mustFill"
                           labelField="true" required="true"  label="成交日期：" labelStyle="text-align:left;width:145px;"/>
                    <input id="underlyingSecurityId" name="underlyingSecurityId" class="mini-textbox mini-mustFill"
                           style="width:100%" required="true"  labelField="true" label="标的债券代码：" maxLength="20"
                           onbuttonclick="onSecurityQuery" allowInput="false"
                           labelStyle="text-align:left;width:145px;"/>
                    <input id="bondPort" name="bondPort" class="mini-textbox mini-mustFill"
                           onbuttonclick="onBondPortEdit" style="width:100%;" labelField="true" label="券的投资组合："
                           required="true"  allowInput="false" vtype="maxLength:4"
                           labelStyle="text-align:left;width:145px;">
                    <input id="underlyingQty" name="underlyingQty" class="mini-spinner mini-mustFill" style="width:100%"
                           required="true"  changeOnMousewheel='false' labelField="true" label="标的债券券面总额(万元)："
                           maxValue="9999999999999999.9999" format="n4" labelStyle="text-align:left;width:145px;"
                           required="true"  onvalidation="zeroValidation"/>
                    <input id="investType" name="investType" class="mini-combobox mini-mustFill" labelField="true"
                           label="投资类型：" style="width:100%;" labelStyle="text-align:left;width:145px;"
                           data="CommonUtil.serverData.dictionary.invtype" required="true" />
                    <input id="firstSettlementMethod" name="firstSettlementMethod" class="mini-combobox mini-mustFill"
                           required="true"  style="width:100%" labelField="true" label="首次结算方式：" vtype="maxLength:50"
                           labelStyle="text-align:left;width:145px;"
                           data="CommonUtil.serverData.dictionary.SettlementMethodSl"/>
                    <input id="firstSettlementDate" name="firstSettlementDate" class="mini-datepicker mini-mustFill"
                           required="true"  style="width:100%" labelField="true" label="首次结算日："
                           onvaluechanged="linkageCalculatDay" ondrawdate="onDrawDateFirst"
                           labelStyle="text-align:left;width:145px;"/>
                    <input id="occupancyDays" name="occupancyDays" class="mini-spinner mini-mustFill" style="width:100%"
                           required="true"  changeOnMousewheel='false' labelField="true" label="实际占券天数(天)："
                           maxValue="99999" onvaluechanged="linkageCalculat" labelStyle="text-align:left;width:145px;"
                           required="true" />
                    <input id="miscFeeType" name="miscFeeType" class="mini-spinner mini-mustFill" style="width:100%"
                           required="true"  changeOnMousewheel='false' labelField="true" label="借贷费用(元)："
                           maxValue="9999999999999999.9999" format="n4" labelStyle="text-align:left;width:145px;"
                           onvalidation="zeroValidation"/>
                    <input id="tradingModel" name="tradingModel" class="mini-combobox mini-mustFill" required="true"
                           labelField="true" label="交易模式：" labelStyle="text-align:left;width:145px;" style="width:100%;"
                           data="CommonUtil.serverData.dictionary.TradeModel"/>
                </div>
                <div class="rightarea">
                    <input id="nettingStatus" name="nettingStatus" class="mini-combobox mini-mustFill" required="true"
                           labelField="true" label="净额清算状态：" labelStyle="text-align:left;width:145px;"
                           style="width:100%;" data="CommonUtil.serverData.dictionary.YesNo" showNullItem="true"/>
                    <input id="underlyingSymbol" name="underlyingSymbol" class="mini-textbox mini-mustFill"
                           style="width:100%" required="true"  labelField="true" label="标的债券名称：" vtype="maxLength:25"
                           labelStyle="text-align:left;width:145px;" enabled="false"/>
                    <input style="width:100%;" id="bondCost" name="bondCost" onbuttonclick="onBondCostEdit"
                           allowInput="false" class="mini-textbox mini-mustFill" labelField="true" label="券的成本中心："
                           required="true"  vtype="maxLength:15" labelStyle="text-align:left;width:145px;">
                    <input id="price" name="price" class="mini-spinner mini-mustFill" style="width:100%"
                           changeOnMousewheel='false' required="true"  labelField="true" label="借贷费率(%)：" minValue="0"
                           maxValue="9999999999999999.99999999" format="n8" changeOnMousewheel="false"
                           labelStyle="text-align:left;width:145px;" required="true" />
                    <input id="tradingProduct" name="tradingProduct" class="mini-combobox" style="width:100%"
                           labelField="true" label="交易品种：" vtype="maxLength:10"
                           labelStyle="text-align:left;width:145px;"
                           data="CommonUtil.serverData.dictionary.dealVarietySl"/>
                    <input id="secondSettlementMethod" name="secondSettlementMethod" class="mini-combobox mini-mustFill"
                           required="true"  style="width:100%" labelField="true" label="到期结算方式：" vtype="maxLength:50"
                           labelStyle="text-align:left;width:145px;"
                           data="CommonUtil.serverData.dictionary.SettlementMethodSl"/>
                    <input id="secondSettlementDate" name="secondSettlementDate" class="mini-datepicker mini-mustFill"
                           required="true"  style="width:100%" labelField="true" label="到期结算日："
                           onvaluechanged="linkageCalculatDay" ondrawdate="onDrawDateSecond"
                           labelStyle="text-align:left;width:145px;"/>
                    <input id="tenor" name="tenor" class="mini-spinner mini-mustFill" style="width:100%" required="true"
                           changeOnMousewheel='false' labelField="true" label="借贷期限(天)：" maxValue="99999"
                           labelStyle="text-align:left;width:145px;"/>
                    <input id="basis" name="basis" class="mini-combobox mini-mustFill" required="true"  labelField="true"
                           label="计息基准：" style="width:100%;" data="CommonUtil.serverData.dictionary.Basis"
                           vtype="maxLength:15" labelStyle="text-align:left;width:145px;">
                    <input id="solution" name="solution" class="mini-combobox mini-mustFill" labelField="true"
                           style="width:100%" required="true"  label="争议解决方式：" labelStyle="text-align:left;width:145px;"
                           data="CommonUtil.serverData.dictionary.solution"/>
                </div>
            </div>
            <div class="mini-panel" title="交易明细信息" style="width:100%" allowResize="true" collapseOnTitleClick="true">
                <div class="centerarea" style="height:150px;">
                    <div class="mini-fit">
                        <div id="datagrid1" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id"
                             allowAlternating="true"
                             fitColumns="false" allowResize="true" sortMode="client" allowAlternating="true"
                             showpager="false">
                            <div property="columns">
                                <div type="indexcolumn" width="50px" headerAlign="center">序号</div>
                                <div field="marginSecuritiesId" width="200" align="center" headerAlign="center">质押债券代码
                                </div>
                                <div field="marginSymbol" width="230px" align="center" headerAlign="center">质押债券名称</div>
                                <div field="marginAmt" width="200" align="right" headerAlign="center" allowSort="true"
                                     numberFormat="n4">质押债券券面总额(万元)
                                </div>
                                <div field="invtype" width="150" align="right" headerAlign="center" allowSort="true"
                                     renderer="CommonUtil.dictRenderer" data-options="{dict:'invtype'}">投资类型
                                </div>
                                <div field="costb" width="150" align="right" headerAlign="center" allowSort="true">
                                    成本中心
                                </div>
                                <div field="prodb" width="150" align="right" headerAlign="center" allowSort="true">产品
                                </div>
                                <div field="prodtypeb" width="150" align="right" headerAlign="center" allowSort="true">
                                    产品类型
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mini-panel" title="质押品明细" style="width:100%;" allowResize="true" collapseOnTitleClick="true">
                <div class="leftarea">
                    <input id="marginTotalAmt" name="marginTotalAmt" class="mini-spinner mini-mustFill" required="true"
                           changeOnMousewheel='false' labelField="true" label="质押债券券面总额合计(万元)：" maxValue="999999999999"
                           format="n4" style="width:100%;" labelStyle="text-align:left;width:170px;"
                           onvalidation="zeroValidation"/>
                </div>
                <div class="rightarea">
                    <input id="marginReplacement" name="marginReplacement" class="mini-textbox mini-mustFill"
                           required="true"  labelField="true" label="质押债券置换安排：" vtype="maxLength:10" style="width:100%;"
                           labelStyle="text-align:left;width:170px;"/>
                </div>
            </div>
            <%@ include file="../../Common/opicsLessDetail.jsp" %>
            <div class="mini-panel" title="清算信息" style="width:100%" allowResize="true" collapseOnTitleClick="true">
                <div class="leftarea">
                    <fieldset>
                        <legend>本方账户</legend>
                        <input id="borrowAccname" name="borrowAccname" class="mini-textbox" labelField="true"
                               label="资金账户户名：" vtype="maxLength:50" style="width:100%;"
                               labelStyle="text-align:left;width:130px;"/>
                        <input id="borrowOpenBank" name="borrowOpenBank" class="mini-textbox" labelField="true"
                               label="资金开户行：" vtype="maxLength:50" labelStyle="text-align:left;width:130px;"
                               style="width:100%;"/>
                        <input id="borrowAccnum" name="borrowAccnum" class="mini-textbox" labelField="true"
                               label="资金账号：" maxLength="50" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])"
                               labelStyle="text-align:left;width:130px;" style="width:100%;"/>
                        <input id="borrowPsnum" name="borrowPsnum" class="mini-textbox" labelField="true"
                               label="支付系统行号：" maxLength="30"
                               onvalidation="CommonUtil.onValidation(e,'alphanum',[null])"
                               labelStyle="text-align:left;width:130px;" style="width:100%;"/>
                        <input id="borrowCaname" name="borrowCaname" class="mini-textbox" labelField="true"
                               label="托管账户户名：" vtype="maxLength:50" labelStyle="text-align:left;width:130px;"
                               style="width:100%;"/>
                        <input id="borrowCustname" name="borrowCustname" class="mini-textbox mini-mustFill"
                               onbuttonclick="onSaccQuery" labelField="true" label="托管机构：" required="true"
                               labelStyle="text-align:left;width:130px;" style="width:100%;" allowInput="false"/>
                        <input id="borrowCanum" name="borrowCanum" class="mini-textbox" labelField="true" label="托管帐号："
                               maxLength="30" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])"
                               labelStyle="text-align:left;width:130px;" style="width:100%;"/>
                    </fieldset>
                </div>
                <div class="leftarea">
                    <fieldset>
                        <legend>对手方账户</legend>
                        <input id="lendAccname" name="lendAccname" class="mini-textbox" labelField="true"
                               label="资金账户户名：" vtype="maxLength:50" style="width:100%;"
                               labelStyle="text-align:left;width:130px;"/>
                        <input id="lendOpenBank" name="lendOpenBank" class="mini-textbox" labelField="true"
                               label="资金开户行：" vtype="maxLength:50" labelStyle="text-align:left;width:130px;"
                               style="width:100%;"/>
                        <input id="lendAccnum" name="lendAccnum" class="mini-textbox" labelField="true" label="资金账号："
                               maxLength="50" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])"
                               labelStyle="text-align:left;width:130px;" style="width:100%;"/>
                        <input id="lendPsnum" name="lendPsnum" class="mini-textbox" labelField="true" label="支付系统行号："
                               maxLength="30" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])"
                               labelStyle="text-align:left;width:130px;" style="width:100%;"/>
                        <input id="lendCaname" name="lendCaname" class="mini-textbox" labelField="true" label="托管账户户名："
                               vtype="maxLength:50" labelStyle="text-align:left;width:130px;" style="width:100%;"/>
                        <input id="lendCustname" name="lendCustname" class="mini-textbox mini-mustFill"
                               onbuttonclick="onSaccQuery" labelField="true" label="托管机构：" required="true"
                               labelStyle="text-align:left;width:130px;" style="width:100%;" allowInput="false"/>
                        <input id="lendCanum" name="lendCanum" class="mini-textbox" labelField="true" label="托管帐号："
                               maxLength="30" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])"
                               labelStyle="text-align:left;width:130px;" style="width:100%;"/>
                    </fieldset>
                </div>
            </div>
        </div>

        <div id="field_form3" class="mini-fit area" style="background:white">
            <div class="mini-panel" title="债券发行详情" style="width:100%;" allowResize="true" collapseOnTitleClick="true">
                <div class="leftarea">
                    <input style="width:100%;" id="contractId" name="contractId" class="mini-textbox mini-mustFill"
                           required="true"  labelField="true" requiredErrorText="该输入项为必输项" label="成交单编号："
                           labelStyle="text-align:left;width:130px;" vtype="maxLength:20"/>
                </div>
            </div>
            <div class="mini-panel" title="融入方信息" style="width:100%" allowResize="true" collapseOnTitleClick="true">
                <input id="borrowInst" name="borrowInst" class="mini-textbox mini-mustFill" labelField="true"
                       label="机构：" required="true"  style="width:49.5%;" enabled="false"
                       labelStyle="text-align:left;width:130px;"/>
                <input id="borrowTrader" name="borrowTrader" class="mini-textbox mini-mustFill" labelField="true"
                       label="交易员：" required="true"  vtype="maxLength:5" enabled="false"
                       labelStyle="text-align:left;width:130px;" style="width:49.5%;"/>
            </div>

            <div class="mini-panel" title="交易主体信息" style="width:100%" allowResize="true" collapseOnTitleClick="true">
                <div class="leftarea">
                    <input style="width:100%;" id="forDate" name="forDate" class="mini-datepicker mini-mustFill"
                           labelField="true" required="true"  label="成交日期：" labelStyle="text-align:left;width:120px;"/>
                    <input style="width:100%;" id="depositCode" name="depositCode" class="mini-textbox mini-mustFill"
                           labelField="true" onbuttonclick="onBondQuery" label="债券代码：" required="true"
                           vtype="maxLength:30" onvalidation="onEnglishAndNumberValidations" allowInput="false"
                           labelStyle="text-align:left;width:120px;"/>
                    <input style="width:100%" id="depositName" name="depositName" class="mini-textbox" labelField="true"
                           label="债券简称：" required="true"  vtype="maxLength:40"
                           labelStyle="text-align:left;width:120px;"/>
                    <input style="width:100%" id="planAmount" name="planAmount" class="mini-spinner" labelField="true"
                           label="计划发行量(元)：" required="true"  minValue="0" maxValue="9999999999999.999" format="n4"
                           changeOnMousewheel="false" labelStyle="text-align:left;width:120px;"
                           onvalidation="zeroValidation"/>
                    <input style="width:100%" id="publishPrice" name="publishPrice" class="mini-spinner"
                           labelField="true" label="发行价格(元)：" required="true"  minValue="0" maxValue="999999.999"
                           format="n4" changeOnMousewheel="false" labelStyle="text-align:left;width:120px;"
                           onvalidation="zeroValidation"/>
                    <input style="width:100%" id="valueDate" name="valueDate" class="mini-datepicker mini-mustFill"
                           labelField="true" label="起息日(缴款日期)：" required="true"
                           labelStyle="text-align:left;width:120px;"/>
                    <input style="width:100%" id="currency" name="currency" class="mini-combobox" labelField="true"
                           label="币种：" data="CommonUtil.serverData.dictionary.Currency"
                           labelStyle="text-align:left;width:120px;" value="CNY"/>
                    <input style="width:100%" id="benchmarkcurvename" name="benchmarkcurvename" class="mini-spinner"
                           labelField="true" label="参考收益率(%)：" required="true"  minValue="0" maxValue="999.9999"
                           format="n8" changeOnMousewheel="false" labelStyle="text-align:left;width:120px;"/>
                </div>
                <div class="rightarea">
                    <input id="nettingStatus" name="nettingStatus" class="mini-combobox mini-mustFill" required="true"
                           labelField="true" label="净额清算状态：" labelStyle="text-align:left;width:120px;"
                           style="width:100%;" data="CommonUtil.serverData.dictionary.YesNo" showNullItem="true"/>
                    <input style="width:100%" id="depositAllName" name="depositAllName" class="mini-textbox"
                           labelField="true" label="债券全称：" required="true"  vtype="maxLength:200"
                           labelStyle="text-align:left;width:120px;"/>
                    <input id="interestType" name="interestType" class="mini-combobox mini-mustFill" required="true"
                           labelField="true" label="投资类型：" style="width:100%;" labelStyle="text-align:left;width:120px;"
                           data="CommonUtil.serverData.dictionary.invtype"/>
                    <input style="width:100%" id="actualAmount" name="actualAmount" class="mini-spinner"
                           labelField="true" label="实际发行量(元)：" enabled="false" required="true"  minValue="0"
                           maxValue="9999999999999.999" format="n4" changeOnMousewheel="false"
                           labelStyle="text-align:left;width:120px;"/>
                    <input style="width:100%" id="publishDate" name="publishDate" class="mini-datepicker mini-mustFill"
                           labelField="true" label="发行日期：" required="true"  changeOnMousewheel="false"
                           labelStyle="text-align:left;width:120px;"/>
                    <input style="width:100%" id="duedate" name="duedate" class="mini-datepicker mini-mustFill"
                           labelField="true" label="到期日：" required="true"  labelStyle="text-align:left;width:120px;"/>
                    <input style="width:100%" id="depositTerm" name="depositTerm" class="mini-spinner" labelField="true"
                           label="债券期限：" required="true"  vtype="maxLength:999" labelStyle="text-align:left;width:120px;"
                           changeOnMousewheel="false"/>
                    <input id="tradingModel" name="tradingModel" class="mini-combobox mini-mustFill" required="true"
                           labelField="true" label="交易模式：" labelStyle="text-align:left;width:120px;" style="width:100%;"
                           data="CommonUtil.serverData.dictionary.TradeModel"/>
                </div>
            </div>
            <div class="mini-panel" title="认购人信息" style="width:100%" allowResize="true" collapseOnTitleClick="true">
                <div class="centerarea" style="height:150px;">
                    <div class="mini-fit">
                        <div id="datagrid2" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id"
                             allowAlternating="true"
                             fitColumns="false" allowResize="true" sortMode="client" allowAlternating="true"
                             showpager="false">
                            <div property="columns">
                                <div type="indexcolumn" width="50px" headerAlign="center">序号</div>
                                <div field="lendInst" width="100px" align="right" headerAlign="center">机构</div>
                                <div field="lendTrader" width="100px" align="center" headerAlign="center">认购人</div>
                                <div field="offerAmount" width="100px" align="right" headerAlign="center">认购量(亿元)</div>
                                <div field="shouldPayMoney" width="100px" align="right" headerAlign="center">应缴纳金额(元)
                                </div>
                                <div field="ccysmeans" width="100px" align="right" headerAlign="center">付款方式</div>
                                <div field="ccysacct" width="100px" align="right" headerAlign="center">付款账户</div>
                                <div field="ctrsmeans" width="100px" align="right" headerAlign="center">收款方式</div>
                                <div field="ctrsacct" width="100px" align="right" headerAlign="center">收款账户</div>
                                <div field="dealSource" width="100px" align="right" headerAlign="center">交易来源</div>
                                <div field="port" width="100px" align="right" headerAlign="center">投资组合</div>
                                <div field="cost" width="100px" align="right" headerAlign="center">成本中心</div>
                                <div field="product" width="100px" align="right" headerAlign="center">产品代码</div>
                                <div field="prodType" width="100px" align="right" headerAlign="center">产品类型</div>
                                <div field="lendOpbank" width="100px" align="right" headerAlign="center">资金开户行</div>
                                <div field="lendAccnum" width="100px" align="right" headerAlign="center">资金账号</div>
                                <div field="lendPsnum" width="100px" align="right" headerAlign="center">支付系统行号</div>
                                <div field="lendCaname" width="100px" align="right" headerAlign="center">托管账户户名</div>
                                <div field="lendCustname" width="100px" align="right" headerAlign="center">托管机构</div>
                                <div field="lendCanum" width="100px" align="right" headerAlign="center">托管帐号</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mini-panel" title="清算信息" style="width:100%" allowResize="true" collapseOnTitleClick="true">
                <fieldset>
                    <legend>发行方账户</legend>
                    <div class="leftarea">
                        <input id="sellOpbank" name="sellOpbank" class="mini-textbox" labelField="true" label="资金开户行："
                               vtype="maxLength:25" labelStyle="text-align:left;width:130px;" style="width:100%;"/>
                        <input id="sellPsnum" name="sellPsnum" class="mini-textbox" labelField="true" label="支付系统行号："
                               onvalidation="onEnglishAndNumberValidation" vtype="maxLength:20"
                               labelStyle="text-align:left;width:130px;" style="width:100%;"/>
                        <input id="sellCustname" name="sellCustname" class="mini-textbox" labelField="true"
                               label="托管机构：" vtype="maxLength:25" labelStyle="text-align:left;width:130px;"
                               style="width:100%;"/>
                    </div>
                    <div class="rightarea">
                        <input id="sellAccnum" name="sellAccnum" class="mini-textbox" labelField="true" label="资金账号："
                               onvalidation="onEnglishAndNumberValidation" vtype="maxLength:20"
                               labelStyle="text-align:left;width:130px;" style="width:100%;"/>
                        <input id="sellCaname" name="sellCaname" class="mini-textbox" labelField="true" label="托管账户户名："
                               vtype="maxLength:25" labelStyle="text-align:left;width:130px;" style="width:100%;"/>
                        <input id="sellCanum" name="sellCanum" class="mini-textbox" labelField="true" label="托管帐号："
                               onvalidation="onEnglishAndNumberValidation" vtype="maxLength:20"
                               labelStyle="text-align:left;width:130px;" style="width:100%;"/>
                    </div>
                </fieldset>
            </div>
        </div>

		<div id="field_form4" class="mini-fit area"  style="background:white">
<%--			<input id="sponsor" name="sponsor" class="mini-hidden" />--%>
<%--			<input id="sponInst" name="sponInst" class="mini-hidden" />--%>
<%--			<input id="aDate" name="aDate" class="mini-hidden"/>--%>
<%--			<input id="ticketId" name="ticketId" class="mini-hidden" />--%>

			<div class="mini-panel" title="债券远期详情" style="width:100%;"  allowResize="true" collapseOnTitleClick="true">
				<div class="leftarea">
					<input style="width:100%;" id="contractId" name="contractId" class="mini-textbox mini-mustFill" required="true"  labelField="true" requiredErrorText="该输入项为必输项"  label="成交单编号：" labelStyle="text-align:left;width:130px;"  vtype="maxLength:35" onvalidation="onEnglishAndNumberValidation"/>
				</div>
			</div>
			<div class="mini-panel" title="成交双方信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
				<div class="leftarea">
					<fieldset>
						<legend>本方信息</legend>
						<input id="myDir" name="myDir" class="mini-combobox mini-mustFill"  labelField="true"  label="本方方向："  required="true"  onvaluechanged="dirVerify"  style="width:100%;"  labelStyle="text-align:left;width:130px;" data = "CommonUtil.serverData.dictionary.trading" />
						<input id="buyInst" name="buyInst" class="mini-textbox mini-mustFill"  labelField="true"  label="机构：" required="true"   style="width:100%;" enabled="false" labelStyle="text-align:left;width:130px;"/>
						<input id="buyTrader" name="buyTrader" class="mini-textbox mini-mustFill" labelField="true"  label="交易员："  required="true"  vtype="maxLength:5" enabled="false" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
					</fieldset>
				</div>
				<div class="rightarea">
					<fieldset>
						<legend>对手方信息</legend>
						<input id="oppoDir" name="oppoDir" class="mini-combobox mini-mustFill"  labelField="true"  label="对方方向："   required="true"  enabled="false" style="width:100%;"  labelStyle="text-align:left;width:130px;" data = "CommonUtil.serverData.dictionary.trading" />
						<input id="sellInst4" name="sellInst" class="mini-buttonedit mini-mustFill" onbuttonclick="onButtonEdit"  labelField="true" allowInput="false" label="机构："  required="true"  style="width:100%;"  labelStyle="text-align:left;width:130px;" />
						<input id="sellTrader" name="sellTrader" class="mini-textbox mini-mustFill" labelField="true"  label="交易员：" vtype="maxLength:30" labelStyle="text-align:left;width:130px;" style="width:100%;" required="true"   />
					</fieldset>
				</div>
			</div>
			<div class="mini-panel" title="交易主体信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
				<div class="leftarea">
					<input style="width:100%;" id="forDate" name="forDate" class="mini-datepicker mini-mustFill" labelField="true" required="true"  label="成交日期：" labelStyle="text-align:left;width:130px;" />
					<input style="width:100%" id="bondCode4" name="bondCode" class="mini-buttonedit mini-mustFill" onbuttonclick="onBondQuery" labelField="true"  label="债券代码：" required="true"  allowInput="false" labelStyle="text-align:left;width:130px;" />
					<input style="width:100%;display:none" id="startDate" name="startDate" class="mini-datepicker mini-mustFill" labelField="true"  label="起息日：" required="true"  labelStyle="text-align:left;width:130px;"   />
					<input style="width:100%;display:none" id="settdays" name="settdays" class="mini-textbox mini-mustFill" labelField="true"  label="结算天数：" required="true"  vtype="maxLength:10"   labelStyle="text-align:left;width:130px;"  enabled="false" />
					<input id="invtype" name="invtype" class="mini-combobox mini-mustFill"  labelField="true" required="true"   label="投资类型："  style="width:100%;"  labelStyle="text-align:left;width:130px;" data = "CommonUtil.serverData.dictionary.invtype" /> <!-- onvaluechanged="searchMarketData" -->
					<input style="width:100%" id="cleanPrice" name="cleanPrice" class="mini-spinner mini-mustFill" labelField="true"  label="净价(元)：" required="true"  onvaluechanged="onAccuredInterestChanged" minValue="0" maxValue="99999999999999.9999" format="n4" changeOnMousewheel="false"  labelStyle="text-align:left;width:130px;"  onvalidation="zeroValidation" />
					<input style="width:100%" id="yield" name="yield" class="mini-spinner mini-mustFill" labelField="true"  label="到期收益率(%)：" required="true"   minValue="0" maxValue="999.999999" format="n6" changeOnMousewheel="false"  labelStyle="text-align:left;width:130px;"   />
					<input style="width:100%" id="accuredInterest" name="accuredInterest" class="mini-spinner mini-mustFill" labelField="true"  label="应计利息(元)：" required="true"  onvaluechanged="onAccuredInterestChanged" minValue="0" maxValue="99999999999999.9999" format="n4" changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"   />
					<input style="width:100%" id="dirtyPrice" name="dirtyPrice" class="mini-spinner mini-mustFill" labelField="true"  label="全价(元)：" required="true"  minValue="0" maxValue="99999999999999.9999" format="n4" changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"   onvaluechanged="onAccuredInterestChanged" onvalidation="zeroValidation" />
					<input style="width:100%" id="settlementDate" name="settlementDate" class="mini-datepicker mini-mustFill" labelField="true"  label="结算日：" required="true"   labelStyle="text-align:left;width:130px;"   onvaluechanged = "searchProductWeightNew()"/>
					<input style="width:100%" id="tradingProduct" name="tradingProduct" class="mini-combobox mini-mustFill" required="true"  allowInput="false" labelField="true"  label="交易品种：" labelStyle="text-align:left;width:130px;" data = "CommonUtil.serverData.dictionary.dealVarietyCredit" onvaluechanged="associaDataCalculation"/>
					<input id="dealTransType" name="dealTransType" class="mini-combobox" labelField="true"  style="width:100%;"  label="数据来源：" data="CommonUtil.serverData.dictionary.dealTransType1" vtype="maxLength:10"  labelStyle="text-align:left;width:130px;"  value="1">
				</div>
				<div class="rightarea">
					<input id="nettingStatus" name="nettingStatus" class="mini-combobox mini-mustFill" required="true"  labelField="true"   label="净额清算状态：" labelStyle="text-align:left;width:130px;" style="width:100%;" data="CommonUtil.serverData.dictionary.YesNo" showNullItem="true"/>
					<input style="width:100%" id="bondName" name="bondName" class="mini-textbox mini-mustFill" labelField="true"  label="债券名称：" required="true"  vtype="maxLength:10"   labelStyle="text-align:left;width:130px;"  enabled="false" />
					<input style="width:100%" id="totalFaceValue" name="totalFaceValue" class="mini-spinner mini-mustFill" labelField="true"  label="券面总额(万元)："  required="true"  onvaluechanged="onAccuredInterestChanged"   minValue="0" maxValue="9999999999.9999" format="n4" changeOnMousewheel="false" labelStyle="text-align:left;width:130px;" onvalidation="zeroValidation"  />
					<input style="width:100%" id="tradeAmount" name="tradeAmount" class="mini-spinner mini-mustFill" labelField="true"   label="交易金额(元)："   required="true"  onvaluechanged="calSettleAmount"  minValue="0" maxValue="99999999999999.9999" format="n4" changeOnMousewheel="false" labelStyle="text-align:left;width:130px;" onvalidation="zeroValidation"  />
					<input style="width:100%" id="totalAccuredInterest" name="totalAccuredInterest" class="mini-spinner mini-mustFill" labelField="true"  label="应计利息总额(元)："  required="true"  onvaluechanged="calSettleAmount" minValue="0" maxValue="99999999999999.9999" format="n4" changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"   />
					<input style="width:100%" id="settlementAmount" name="settlementAmount" class="mini-spinner mini-mustFill" labelField="true"  label="结算金额(元)：" required="true"   minValue="0" maxValue="99999999999999.9999" format="n4" changeOnMousewheel="false" labelStyle="text-align:left;width:130px;" onvalidation="zeroValidation"  />
					<input style="width:100%" id="tenor" name="tenor" class="mini-spinner mini-mustFill" labelField="true"  label="远期期限(天)：" minValue="0" required="true"   maxValue="99999"  changeOnMousewheel="false"   labelStyle="text-align:left;width:130px;" />
					<input style="width:100%" id="settlementMethod" name="settlementMethod" class="mini-combobox mini-mustFill" labelField="true"  label="结算方式："  required="true"  data = "CommonUtil.serverData.dictionary.SettlementMethod"  labelStyle="text-align:left;width:130px;"   />
					<input id="tradingModel" name="tradingModel" class="mini-combobox mini-mustFill" required="true"  labelField="true"  label="交易模式：" labelStyle="text-align:left;width:130px;" style="width:100%;"  data="CommonUtil.serverData.dictionary.TradeModel"/>
					<input id="note" name="note" class="mini-textbox" labelField="true" label="备注：" labelStyle="text-align:left;width:130px;" style="width:100%;" vtype="maxLength:16"/>
				</div>
			</div>
			<%@ include file="../../Common/opicsLessDetail.jsp"%>
			<div class="mini-panel" title="清算信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
				<div class="leftarea">
					<fieldset>
						<legend>本方账户</legend>
						<input id="buyAccname" name="buyAccname" class="mini-textbox" labelField="true"  label="资金账户户名："  vtype="maxLength:1333"  style="width:100%;"  labelStyle="text-align:left;width:130px;" />
						<input id="buyOpbank" name="buyOpbank" class="mini-textbox" labelField="true"  label="资金开户行："  vtype="maxLength:1333"  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						<input id="buyAccnum" name="buyAccnum" class="mini-textbox" labelField="true"  label="资金账号："   onvalidation="onEnglishAndNumberValidation" vtype="maxLength:400" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						<input id="buyPsnum" name="buyPsnum" class="mini-textbox" labelField="true"  label="支付系统行号："  onvalidation="onEnglishAndNumberValidation" vtype="maxLength:400"  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						<input id="buyCaname" name="buyCaname" class="mini-textbox" labelField="true"  label="托管账户户名："  vtype="maxLength:1333"  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						<input id="buyCustname4" name="buyCustname4" class="mini-buttonedit mini-mustFill" onbuttonclick="onSaccQuery" required="true"  labelField="true" allowInput="false"  label="托管机构："  vtype="maxLength:1333"  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						<input id="buyCanum" name="buyCanum" class="mini-textbox" labelField="true"  label="托管帐号："   onvalidation="onEnglishAndNumberValidation" vtype="maxLength:400" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
					</fieldset>
				</div>
				<div class="rightarea">
					<fieldset>
						<legend>对手方账户</legend>
						<input id="sellAccname" name="sellAccname" class="mini-textbox" labelField="true"  label="资金账户户名："  vtype="maxLength:1333"  style="width:100%;"  labelStyle="text-align:left;width:130px;" />
						<input id="sellOpbank" name="sellOpbank" class="mini-textbox" labelField="true"  label="资金开户行："  vtype="maxLength:1333"  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						<input id="sellAccnum" name="sellAccnum" class="mini-textbox" labelField="true"  label="资金账号："  onvalidation="onEnglishAndNumberValidation" vtype="maxLength:400" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						<input id="sellPsnum" name="sellPsnum" class="mini-textbox" labelField="true"  label="支付系统行号："   onvalidation="onEnglishAndNumberValidation" vtype="maxLength:400" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						<input id="sellCaname" name="sellCaname" class="mini-textbox" labelField="true"  label="托管账户户名：" vtype="maxLength:1333"   labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						<input id="sellCustname4" name="sellCustname4" class="mini-buttonedit mini-mustFill" onbuttonclick="onSaccQuery" required="true"  labelField="true" allowInput="false"  label="托管机构：" vtype="maxLength:1333"   labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						<input id="sellCanum" name="sellCanum" class="mini-textbox" labelField="true"  label="托管帐号："   onvalidation="onEnglishAndNumberValidation" vtype="maxLength:400" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
					</fieldset>
				</div>
			</div>
		</div>

    </div>
    <div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
        <div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none" style="width:120px;" id="save_btn" onclick="save">保存交易</a></div>
        <div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none" style="width:120px;" id="close_btn" onclick="cancel">关闭界面</a>
        </div>
    </div>
</div>

<script type="text/javascript">
    mini.parse();

    //获取当前tab
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var row = params.selectedData;
    var url = window.location.search;
    var action = CommonUtil.getParam(url, "action");
    var form = new mini.Form("#field_form");
    var form1 = new mini.Form("#field_form1");
    var form2 = new mini.Form("#field_form2");
    var form3 = new mini.Form("#field_form3");
    var form4 = new mini.Form("#field_form4");
    form1.setEnabled(false);
    form2.setEnabled(false);
    form3.setEnabled(false);
    form4.setEnabled(false);
    var grid1 = mini.get("datagrid1");
    var grid2 = mini.get("datagrid2");
    var tradeData = {};
    tradeData.selectData = row;
    tradeData.operType = action;
    tradeData.serial_no = row.ticketId;
    tradeData.task_id = row.taskId;
    var bondmatdt;//债券本身到息日,债券本身到期的交易不能冲销
    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
            initData();
        });
    });

    //初始化数据
    function initData() {
        if ($.inArray(action, ["edit", "detail", "approve"]) > -1) {
            form.setData(row);
            if (mini.get("dealType").getValue() == "IFS_CFETSRMB_CBT") {
                $('#field_form2').hide();
                $('#field_form3').hide();
                $('#field_form4').hide();
                search2();
            } else if (mini.get("dealType").getValue() == "IFS_CFETSRMB_SL") {
                $('#field_form1').hide();
                $('#field_form3').hide();
                $('#field_form4').hide();
                search1();
            } else if (mini.get("dealType").getValue() == "IFS_CFETSRMB_BONDFWD") {
                $('#field_form1').hide();
                $('#field_form2').hide();
                $('#field_form3').hide();
				search4();
            } else {
                $('#field_form1').hide();
                $('#field_form2').hide();
                $('#field_form4').hide();
                search3();
            }
            if ($.inArray(action, ["approve", "detail"]) > -1) {
                mini.get("save_btn").setVisible(false);
                form.setEnabled(false);
                var form11 = new mini.Form("#approve_operate_form");
                form11.setEnabled(true);
            }
        } else { //增加
            form.setData(row);
            mini.get("fedealno").setValue(row.ticketId);
            mini.get("ticketId").setValue("");
            mini.get("sponsor").setValue("<%=__sessionUser.getUserId()%>");
            mini.get("sponInst").setValue("<%=__sessionUser.getInstId() %>");
            if (mini.get("dealType").getValue() == "IFS_CFETSRMB_CBT") {
                $('#field_form2').hide();
                $('#field_form3').hide();
                $('#field_form4').hide();
                search2();
            } else if (mini.get("dealType").getValue() == "IFS_CFETSRMB_SL") {
                $('#field_form1').hide();
                $('#field_form3').hide();
                $('#field_form4').hide();
                search1();
            } else if (mini.get("dealType").getValue() == "IFS_CFETSRMB_BONDFWD") {
                $('#field_form1').hide();
                $('#field_form2').hide();
                $('#field_form3').hide();
                search4();
            } else {
                $('#field_form1').hide();
                $('#field_form2').hide();
                $('#field_form4').hide();
                search3();
            }
        }
    }

    function initBondmatdt(bndcd) {
        CommonUtil.ajax({
            url: "/IfsOpicsBondController/searchOneById",
            data: {"bndcd": bndcd},
            callback: function (data) {
                if (data != null) {
                    bondmatdt = data.matdt;
                } else {
                    //默认当前日期
                    bondmatdt = new Date();
                }
            }
        });
    }

    //保存
    function save() {
        //表单验证！！！
        form.validate();
        if (form.isValid() == false) {
            return;
        }
        //校验opcis
        if (!checkOpics()) {
            return;
        }
        var saveUrl = null;
        saveUrl = "/IfsIsldController/saveIsldParam";
        var data = form.getData(true);
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: saveUrl,
            data: params,
            callback: function (data) {
                if (data.code == 'error.common.0000') {
                    mini.alert("保存成功", '提示信息', function () {
                        top["isldManage"].query();
                        top["win"].closeMenuTab();

                    });
                } else {
                    mini.alert("保存失败");
                }
            }
        });
    }

    function cancel() {
        top["isldManage"].query();
        top["win"].closeMenuTab();

    }

    //英文、数字、下划线 的验证
    function onEnglishAndNumberValidation(e) {
        if (e.value == "" || e.value == null) {//值为空，就不做校验
            return;
        }
        if (e.isValid) {
            if (isEnglishAndNumber(e.value) == false) {
                e.errorText = "必须输入英文或数字";
                e.isValid = false;
            }
        }
    }

    /* 是否英文、数字、下划线 */
    function isEnglishAndNumber(v) {
        var re = new RegExp("^[0-9a-zA-Z\_\-]+$");
        if (re.test(v)) return true;
        return false;
    }

    function nodeclick(e) {
        var oldvalue = e.sender.value;
        var psn = mini.get("psn").getValue();
        var param = mini.encode({"psn": psn}); //序列化成JSON
        CommonUtil.ajax({
            url: "/IfsOpicsProdController/searchProd",
            data: param,
            callback: function (data) {
                var obj = e.sender;
                obj.setData(data.obj);
                obj.setValue(oldvalue);

            }
        });
    }

    function search1() {
        var ticketId = mini.get("fedealno").getValue();
        var data = {};
        data['ticketId'] = ticketId;
        data['branchId'] = branchId;
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: "/IfsCfetsrmbSlController/searchCfetsrmbSlByTicketId",
            data: params,
            callback: function (data) {
                form2.setData(data.obj);
                initBondmatdt(data.obj.underlyingSecurityId);
            }
        });

        //初始化债券信息
        var url = "/IfsCfetsrmbSlController/searchBondSlDetails";
        CommonUtil.ajax({
            url: url,
            data: params,
            callback: function (data) {
                grid1.setData(data.obj.rows);
            }
        });
    }

    //初始化债券认购人信息
    function initGrid(ticketId) {
        var url = "/IfsCfetsrmbDpController/searchOfferDpDetails";
        var data = {};
        data['ticketId'] = ticketId;
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: url,
            data: params,
            callback: function (data) {
                grid2.setData(data.obj.rows);
            }
        });
    }

    function search2() {
        var ticketId = mini.get("fedealno").getValue();
        var data = {};
        data['ticketId'] = ticketId;
        data['branchId'] = branchId;
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: "/IfsCfetsrmbCbtController/searchCfetsrmbCbtByTicketId",
            data: params,
            callback: function (data) {
                form1.setData(data.obj);
                initBondmatdt(data.obj.bondCode);
            }
        });
    }

    function search3() {
        var ticketId = mini.get("fedealno").getValue();
        var data = {};
        data['ticketId'] = ticketId;
        data['branchId'] = branchId;
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: "/IfsCfetsrmbDpController/searchCfetsrmbDpByTicketId",
            data: params,
            callback: function (data) {
                form3.setData(data.obj);
                initBondmatdt(data.obj.depositCode);
            }
        });
        initGrid(ticketId);
    }

	function search4() {
		var ticketId = mini.get("fedealno").getValue();
		var data = {};
		data['ticketId'] = ticketId;
		var params = mini.encode(data);
		CommonUtil.ajax({
			url: "/IfsCfetsrmbBondfwdController/searchCfetsrmbBondfwdByTicketId",
			data: params,
			callback: function (data) {
				form4.setData(data.obj);
				initBondmatdt(data.obj.bondCode);
				mini.get('sellInst4').setText(data.obj.sellInst);
				mini.get('bondCode4').setText(data.obj.bondCode);
				mini.get('buyCustname4').setText(data.obj.buyCustname);
				mini.get('sellCustname4').setText(data.obj.sellCustname);
			}
		});
	}


    function checkOpics() {
        var adate = mini.formatDate(mini.get("adate").getValue(), 'yyyy-MM-dd');
        var opicsFlag = CommonUtil.dateCompare(bondmatdt, adate);
        if (!opicsFlag) {
            mini.alert("债券本身到期的交易不能冲销!");
        }
        return opicsFlag;
    }

    function adateCheck(e) {
        if (mini.parseDate(row.forDate) > e.value) {
            mini.alert("冲销日期不得早于成交日期", "提示");
            mini.get("adate").setValue('');
        }
    }
</script>
<script type="text/javascript" src="../../Common/Flow/MiniApproveOpCommon.js"></script>
</body>
</html>