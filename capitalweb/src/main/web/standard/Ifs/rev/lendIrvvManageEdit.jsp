<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../../global.jsp" %>
<html>
<head>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title></title>
</head>

<body style="width:100%;height:100%;background:white">
<div class="mini-splitter" style="width:100%;height:100%;">
    <div size="90%" showCollapseButton="false">
        <div>
            <div id="field" class="fieldset-body">
                <div id="field_form" class="mini-panel" title="拆借类信息" style="width:100%;" allowResize="true"
                     collapseOnTitleClick="true">
                    <div class="leftarea">
                        <input id="sponsor" name="sponsor" class="mini-hidden"/>
                        <input id="sponInst" name="sponInst" class="mini-hidden"/>
                        <input id="ticketId" name="ticketId" enabled="false" class="mini-textbox" label="审批单流水号："
                               labelField="true" emptyText="系统自动生成" vtype="maxLength:35" style="width:100%"
                               labelStyle="text-align:left;width:140px"/>
                        <input id="fedealno" name="fedealno" class="mini-textbox" enabled="false" label="交易前端流水号："
                               emptyText="请输入交易前端流水号" labelField="true" style="width:100%"
                               labelStyle="text-align:left;width:140px"/>
                        <input id="adate" name="adate" class="mini-datepicker mini-mustFill" label="冲销日期："
                               required="true" emptyText="请输入冲销日期" labelField="true" style="width:100%"
                               labelStyle="text-align:left;width:140px" vtype="maxLength:35"
                               onvaluechanged="adateCheck"/>
                        <input id="revreason" name="revreason" class="mini-textarea" label="冲销原因：" emptyText="请输入冲销原因"
                               labelField="true" style="width:100%" labelStyle="text-align:left;width:140px"
                               vtype="maxLength:255"/>
                    </div>
                    <div class="rightarea">
                        <input style="width:100%;" id="dealType" name="dealType" enabled="false" class="mini-combobox"
                               labelField="true" label="业务类型：" data="CommonUtil.serverData.dictionary.BusinessType"
                               labelStyle="text-align:left;width:130px"/>
                        <input id="dealNo" name="dealNo" enabled="false" class="mini-textbox" label="OPICS交易号："
                               emptyText="请输入OPICS交易号" labelField="true" style="width:100%"
                               labelStyle="text-align:left;width:130px"/>
                        <input id="br" name="br" class="mini-combobox mini-mustFill" label="部门："
                               data="[{id:'01',text:'01'}]" emptyText="请选择部门" labelField="true" style="width:100%"
                               labelStyle="text-align:left;width:130px" vtype="maxLength:50" required="true"/>
                    </div>
                    <%@ include file="../../Common/Flow/MiniApproveOpCommon.jsp" %>
                </div>
                <!-- 外币拆借 -->
                <div id="field_form1" class="mini-fit area" style="background:white;height: 700px">
                    <div class="mini-panel" title="外币拆借详情" style="width:100%;" allowResize="true"
                         collapseOnTitleClick="true">
                        <div class="leftarea">
                            <input style="width:100%;" id="contractId" name="contractId"
                                   class="mini-textbox mini-mustFill" labelField="true" requiredErrorText="该输入项为必输项"
                                   required="true" label="成交单编号：" labelStyle="text-align:left;width:130px;"
                                   onvalidation="onEnglishAndNumberValidations" vtype="maxLength:20"/>
                        </div>
                    </div>
<%--                    <div class="mini-panel" title="参考编号" style="width:100%;" allowResize="true"--%>
<%--                         collapseOnTitleClick="true">--%>
<%--                        <div class="leftarea">--%>
<%--                            <input style="width:100%;" id="refId" name="refId" class="mini-textbox mini-mustFill"--%>
<%--                                   labelField="true" requiredErrorText="该输入项为必输项" label="参考编号：" required="true"--%>
<%--                                   labelStyle="text-align:left;width:130px;" vtype="maxLength:20"/>--%>
<%--                        </div>--%>
<%--                    </div>--%>
                    <div class="mini-panel" title="成交双方信息" style="width:100%" allowResize="true"
                         collapseOnTitleClick="true">
                        <div class="leftarea">
                            <fieldset>
                                <legend>本方信息</legend>
                                <input id="direction" name="direction" class="mini-combobox mini-mustFill"
                                       labelField="true" onvaluechanged="dirVerify" label="本方方向：" required="true"
                                       style="width:100%;" labelStyle="text-align:left;width:120px;"
                                       data="CommonUtil.serverData.dictionary.lending"/>
                                <input id="instId" name="instId" class="mini-textbox mini-mustFill" labelField="true"
                                       label="机构：" style="width:100%;" required="true" enabled="false"
                                       labelStyle="text-align:left;width:120px;"/>
                                <input id="dealer" name="myUserName" class="mini-textbox mini-mustFill"
                                       labelField="true" label="交易员：" vtype="maxLength:5" required="true"
                                       enabled="false" labelStyle="text-align:left;width:120px;" style="width:100%;"/>
                            </fieldset>
                        </div>
                        <div class="rightarea">
                            <fieldset>
                                <legend>对手方信息</legend>
                                <input id="oppoDir" name="oppoDir" class="mini-combobox mini-mustFill" enabled="false"
                                       labelField="true" label="对方方向：" required="true" style="width:100%;"
                                       labelStyle="text-align:left;width:120px;"
                                       data="CommonUtil.serverData.dictionary.lending"/>
                                <input id="counterpartyInstId" name="counterpartyInstId"
                                       class="mini-textbox mini-mustFill" onbuttonclick="onButtonEdit" labelField="true"
                                       required="true" label="机构：" allowInput="false" style="width:100%;"
                                       labelStyle="text-align:left;width:120px;"/>
                                <input id="counterpartyDealer" name="counterpartyDealer"
                                       class="mini-textbox mini-mustFill" labelField="true" label="交易员："
                                       vtype="maxLength:30" labelStyle="text-align:left;width:120px;"
                                       style="width:100%;" required="true"/>
                            </fieldset>
                        </div>
                    </div>

                    <div class="mini-panel" title="交易主体信息" style="width:100%" allowResize="true"
                         collapseOnTitleClick="true">
                        <div class="leftarea">
                            <input id="forDate" name="forDate" class="mini-datepicker" labelField="true" label="交易日期："
                                   labelStyle="text-align:left;width:130px;" style="width:100%;"/>
                            <input style="width:100%;" id="amount" name="amount" class="mini-spinner mini-mustFill"
                                   labelField="true" changeOnMousewheel='false' format="n4" label="拆借金额(元)："
                                   onValuechanged="interestAmount" required="true" maxValue="999999999999999"
                                   labelStyle="text-align:left;width:130px;" onvalidation="zeroValidation"/>
                            <input style="width:100%;" id="valueDate" name="valueDate"
                                   class="mini-datepicker mini-mustFill" labelField="true" onValuechanged="compareDate"
                                   label="起息日：" ondrawdate="onDrawDateStart" required="true"
                                   labelStyle="text-align:left;width:130px;"/>
                            <input style="width:100%" id="tenor" name="tenor" class="mini-spinner mini-mustFill"
                                   labelField="true" label="拆借期限(天)：" minValue="0" required="true" maxValue="99999"
                                   changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"/>
                            <input style="width:100%;" id="basis" name="basis" class="mini-combobox mini-mustFill"
                                   labelField="true" label="计息基准：" onValuechanged="interestAmount"
                                   data="CommonUtil.serverData.dictionary.Basis"
                                   labelStyle="text-align:left;width:130px;" required="true"/>
                            <input style="width:100%;" id="repaymentAmount" name="repaymentAmount"
                                   class="mini-spinner mini-mustFill" labelField="true" changeOnMousewheel='false'
                                   format="n4" label="到期还款金额：" required="true" maxValue="999999999999999"
                                   labelStyle="text-align:left;width:130px;"/>
                            <input id="tradingModel" name="tradingModel" class="mini-combobox mini-mustFill"
                                   required="true" labelField="true" label="交易模式："
                                   labelStyle="text-align:left;width:130px;" style="width:100%;"
                                   data="CommonUtil.serverData.dictionary.TradeModel"/>
                            <input id="note" name="note" class="mini-textbox" labelField="true" label="备注："
                                   labelStyle="text-align:left;width:130px;" style="width:100%;" vtype="maxLength:16"/>
                        </div>
                        <div class="rightarea">
                            <input style="width:100%;" id="currency" name="currency" class="mini-combobox mini-mustFill"
                                   labelField="true" data="CommonUtil.serverData.dictionary.Currency" label="货币："
                                   labelStyle="text-align:left;width:130px;" required="true" allowInput="false"/>
                            <input style="width:100%;" id="rate" name="rate" class="mini-spinner mini-mustFill"
                                   labelField="true" label="拆借利率(%)：" onValuechanged="interestAmount"
                                   changeOnMousewheel='false' required="true" format="n8"
                                   labelStyle="text-align:left;width:130px;"/>
                            <input style="width:100%;" id="maturityDate" name="maturityDate"
                                   class="mini-datepicker mini-mustFill" labelField="true" onValuechanged="compareDate"
                                   label="到期日：" ondrawdate="onDrawDateEnd" required="true"
                                   labelStyle="text-align:left;width:130px;"/>
                            <input style="width:100%;" id="occupancyDays" name="occupancyDays" class="mini-spinner"
                                   labelField="true" minErrorText="0" label="实际占款天数：" onValuechanged="interestAmount"
                                   changeOnMousewheel='false' maxValue="10000"
                                   labelStyle="text-align:left;width:130px;"/>
                            <input style="width:100%;" id="interest" name="interest" class="mini-spinner mini-mustFill"
                                   labelField="true" label="应计利息：" maxValue="999999999999999" changeOnMousewheel='false'
                                   format="n4" required="true" labelStyle="text-align:left;width:130px;"/>
                            <input id="rateCode" name="rateCode" class="mini-textbox mini-mustFill"
                                   onbuttonclick="onRateEdit" labelField="true" label="利率代码：" style="width: 100%;"
                                   labelStyle="text-align:left;width:130px;" allowInput="false"
                                   requiredErrorText="该输入项为必输项" required="true"/>
                            <!-- <input id="nettingStatus" name="nettingStatus" class="mini-combobox mini-mustFill" required="true"  labelField="true" label="净额清算状态：" labelStyle="text-align:left;width:130px;" style="width:100%;" data="CommonUtil.serverData.dictionary.YesNo" showNullItem="true"/> -->
                            <input id="dealTransType" name="dealTransType" class="mini-combobox" labelField="true"
                                   style="width:100%;" label="数据来源："
                                   data="CommonUtil.serverData.dictionary.dealTransType1" vtype="maxLength:10"
                                   labelStyle="text-align:left;width:130px;" value="1">
                        </div>
                    </div>
                    <%@ include file="../../Common/opicsLessDetail.jsp" %>
                    <div class="mini-panel" title="清算信息" style="width:100%" allowResize="true"
                         collapseOnTitleClick="true">
                        <div class="leftarea">
                            <fieldset>
                                <legend>本方账户</legend>
                                <input id="buyCurreny" name="buyCurreny" class="mini-combobox" labelField="true"
                                       label="清算货币：" style="width:100%;" labelStyle="text-align:left;width:120px;"
                                       data="CommonUtil.serverData.dictionary.Currency" enabled="false"/>
                                <input id="bankName1" name="bankName1" class="mini-textbox" labelField="true"
                                       label="开户行名称：" labelStyle="text-align:left;width:120px;" style="width:100%;"
                                       requiredErrorText="该输入项为必输项" vtype="maxLength:60"/>
                                <input id="bankBic1" name="bankBic1" class="mini-textbox" labelField="true"
                                       label="开户行BIC CODE：" labelStyle="text-align:left;width:120px;"
                                       style="width:100%;" requiredErrorText="该输入项为必输项"
                                       onvalidation="onEnglishAndNumberValidation" vtype="maxLength:45"/>
                                <input id="dueBank1" name="dueBank1" class="mini-textbox" labelField="true"
                                       label="收款行名称：" labelStyle="text-align:left;width:120px;" style="width:100%;"
                                       requiredErrorText="该输入项为必输项" vtype="maxLength:60"/>
                                <input id="dueBankName1" name="dueBankName1" class="mini-textbox" labelField="true"
                                       label="收款行BIC CODE：" labelStyle="text-align:left;width:120px;"
                                       style="width:100%;" requiredErrorText="该输入项为必输项"
                                       onvalidation="onEnglishAndNumberValidation" vtype="maxLength:45"/>
                                <input id="dueBankAccount1" name="dueBankAccount1" class="mini-textbox"
                                       labelField="true" label="收款行账号：" labelStyle="text-align:left;width:120px;"
                                       style="width:100%;" requiredErrorText="该输入项为必输项"
                                       onvalidation="onEnglishAndNumberValidation" vtype="maxLength:20"/>
                                <input id="intermediaryBank1" name="intermediaryBank1" class="mini-textbox"
                                       labelField="true" label="中间行名称：" labelStyle="text-align:left;width:120px;"
                                       style="width:100%;" requiredErrorText="该输入项为必输项" vtype="maxLength:60"/>
                                <input id="intermediaryBicCode1" name="intermediaryBicCode1" class="mini-textbox"
                                       labelField="true" label="中间行BIC CODE：" labelStyle="text-align:left;width:120px;"
                                       style="width:100%;" requiredErrorText="该输入项为必输项"
                                       onvalidation="onEnglishAndNumberValidation" vtype="maxLength:45"/>
                            </fieldset>
                        </div>
                        <div class="rightarea">
                            <fieldset>
                                <legend>对手方账户</legend>
                                <input id="sellCurreny" name="sellCurreny" class="mini-combobox" labelField="true"
                                       label="清算货币：" style="width:100%;" labelStyle="text-align:left;width:120px;"
                                       requiredErrorText="该输入项为必输项" data="CommonUtil.serverData.dictionary.Currency"
                                       enabled="false"/>
                                <input id="bankName2" name="bankName2" class="mini-textbox" labelField="true"
                                       label="开户行名称：" labelStyle="text-align:left;width:120px;" style="width:100%;"
                                       requiredErrorText="该输入项为必输项" vtype="maxLength:60"/>
                                <input id="bankBic2" name="bankBic2" class="mini-textbox" labelField="true"
                                       label="开户行BIC CODE：" labelStyle="text-align:left;width:120px;"
                                       style="width:100%;" requiredErrorText="该输入项为必输项"
                                       onvalidation="onEnglishAndNumberValidation" vtype="maxLength:45"/>
                                <input id="dueBank2" name="dueBank2" class="mini-textbox" labelField="true"
                                       label="收款行名称：" labelStyle="text-align:left;width:120px;" style="width:100%;"
                                       requiredErrorText="该输入项为必输项" vtype="maxLength:60"/>
                                <input id="dueBankName2" name="dueBankName2" class="mini-textbox" labelField="true"
                                       label="收款行BIC CODE：" labelStyle="text-align:left;width:120px;"
                                       style="width:100%;" requiredErrorText="该输入项为必输项"
                                       onvalidation="onEnglishAndNumberValidation" vtype="maxLength:45"/>
                                <input id="dueBankAccount2" name="dueBankAccount2" class="mini-textbox"
                                       labelField="true" label="收款行账号：" labelStyle="text-align:left;width:120px;"
                                       style="width:100%;" requiredErrorText="该输入项为必输项"
                                       onvalidation="onEnglishAndNumberValidation" vtype="maxLength:20"/>
                                <input id="intermediaryBank2" name="intermediaryBank2" class="mini-textbox"
                                       labelField="true" label="中间行名称：" labelStyle="text-align:left;width:120px;"
                                       style="width:100%;" requiredErrorText="该输入项为必输项" vtype="maxLength:60"/>
                                <input id="intermediaryBicCode2" name="intermediaryBicCode2" class="mini-textbox"
                                       labelField="true" label="中间行BIC CODE：" labelStyle="text-align:left;width:120px;"
                                       style="width:100%;" requiredErrorText="该输入项为必输项"
                                       onvalidation="onEnglishAndNumberValidation" vtype="maxLength:45"/>
                            </fieldset>
                        </div>
                    </div>
                </div>
                <!-- 同业存款 -->
                <div id="field_form3" class="mini-fit area" style="background:white;height: 700px">
                    <div class="mini-panel" title="同业存款详情" style="width:100%;" allowResize="true"
                         collapseOnTitleClick="true">
                        <div class="leftarea">
                            <%--						<input style="width:100%;" id="ticketId" enabled="false" name="ticketId" class="mini-textbox" labelField="true"   label="审批单编号：" labelStyle="text-align:left;width:130px;"  vtype="maxLength:20" onvalidation="onEnglishAndNumberValidation"/>--%>
                            <%--						<input style="width:100%;" id="contractId" name="contractId" class="mini-textbox mini-mustFill" labelField="true" requiredErrorText="该输入项为必输项" required="true"  label="成交单编号：" labelStyle="text-align:left;width:130px;" onvalidation="onEnglishAndNumberValidations" vtype="maxLength:20"/>--%>
                        </div>
                    </div>
                    <div class="mini-panel" title="参考编号" style="width:100%;" allowResize="true"
                         collapseOnTitleClick="true">
                        <div class="leftarea">
                            <input style="width:100%;" id="refId" name="refId" class="mini-textbox mini-mustFill"
                                   labelField="true" requiredErrorText="该输入项为必输项" label="参考编号：" required="true"
                                   labelStyle="text-align:left;width:130px;" vtype="maxLength:20"/>
                        </div>
                    </div>
                    <div class="mini-panel" title="成交双方信息" style="width:100%" allowResize="true"
                         collapseOnTitleClick="true">
                        <div class="leftarea">
                            <fieldset>
                                <legend>本方信息</legend>
                                <input id="direction" name="direction" class="mini-combobox mini-mustFill"
                                       labelField="true" onvaluechanged="dirVerify" label="本方方向：" required="true"
                                       style="width:100%;" labelStyle="text-align:left;width:120px;"
                                       data="CommonUtil.serverData.dictionary.lending"/>
                                <input id="instId" name="instId" class="mini-textbox mini-mustFill" labelField="true"
                                       label="机构：" style="width:100%;" required="true" enabled="false"
                                       labelStyle="text-align:left;width:120px;"/>
                                <input id="dealer" name="myUserName" class="mini-textbox mini-mustFill"
                                       labelField="true" label="交易员：" vtype="maxLength:5" required="true"
                                       enabled="false" labelStyle="text-align:left;width:120px;" style="width:100%;"/>
                            </fieldset>
                        </div>
                        <div class="rightarea">
                            <fieldset>
                                <legend>对手方信息</legend>
                                <input id="counterpartyInstId" name="counterpartyInstId"
                                       class="mini-buttonedit mini-mustFill" onbuttonclick="onButtonEdit"
                                       labelField="true" allowInput="false" label="客户号：" style="width:100%;"
                                       labelStyle="text-align:left;width:120px;" vtype="maxLength:16"
                                       requiredErrorText="该输入项为必输项" required="true"/>
                                <input id="cname" name="cname" class="mini-textbox mini-mustFill" labelField="true"
                                       label="客户名称：" vtype="maxLength:20" labelStyle="text-align:left;width:120px;"
                                       style="width:100%;" required="true"/>
                                <%--							<input id="oppoDir" name="oppoDir" class="mini-combobox mini-mustFill"  enabled="false" labelField="true"  label="对方方向："  required="true"   style="width:100%;"  labelStyle="text-align:left;width:130px;" data = "CommonUtil.serverData.dictionary.lending" />--%>
                                <%--							<input id="counterpartyInstId" name="counterpartyInstId" class="mini-textbox mini-mustFill" onbuttonclick="onButtonEdit" labelField="true"  required="true"   label="机构：" allowInput="false" style="width:100%;"  labelStyle="text-align:left;width:130px;" />--%>
                                <%--							<input id="counterpartyDealer" name="counterpartyDealer" class="mini-textbox mini-mustFill" labelField="true"  label="交易员：" vtype="maxLength:30" labelStyle="text-align:left;width:130px;" style="width:100%;" required="true"   />--%>
                            </fieldset>
                        </div>
                    </div>

                    <%--				<div class="mini-panel" title="交易主体信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">--%>
                    <%--					<div class="leftarea">--%>
                    <%--						<input id="forDate" name="forDate" class="mini-datepicker" labelField="true"  label="交易日期："  labelStyle="text-align:left;width:130px;" style="width:100%;"   />--%>
                    <%--						<input style="width:100%;" id="amount" name="amount" class="mini-spinner mini-mustFill" labelField="true"  changeOnMousewheel='false'  format="n4" label="拆借金额(元)：" onValuechanged="interestAmount" required="true"  maxValue="999999999999999" labelStyle="text-align:left;width:130px;" onvalidation="zeroValidation"   />--%>
                    <%--						<input style="width:100%;" id="valueDate" name="valueDate" class="mini-datepicker mini-mustFill" labelField="true" onValuechanged="compareDate" label="起息日：" ondrawdate="onDrawDateStart" required="true"   labelStyle="text-align:left;width:130px;" />--%>
                    <%--						<input style="width:100%" id="tenor" name="tenor" class="mini-spinner mini-mustFill" labelField="true"  label="拆借期限(天)：" minValue="0" required="true"   maxValue="99999"  changeOnMousewheel="false"   labelStyle="text-align:left;width:130px;" />--%>
                    <%--						<input style="width:100%;" id="basis" name="basis" class="mini-combobox mini-mustFill" labelField="true"  label="计息基准：" onValuechanged="interestAmount"  data="CommonUtil.serverData.dictionary.Basis" labelStyle="text-align:left;width:130px;" required="true" />--%>
                    <%--						<input style="width:100%;" id="repaymentAmount" name="repaymentAmount" class="mini-spinner mini-mustFill" labelField="true" changeOnMousewheel='false'  format="n4" label="到期还款金额：" required="true"  maxValue="999999999999999" labelStyle="text-align:left;width:130px;" />--%>
                    <%--						<input id="tradingModel" name="tradingModel" class="mini-combobox mini-mustFill" required="true"  labelField="true"   label="交易模式："  labelStyle="text-align:left;width:130px;" style="width:100%;"  data="CommonUtil.serverData.dictionary.TradeModel"/>--%>
                    <%--						<input id="note" name="note" class="mini-textbox" labelField="true" label="备注：" labelStyle="text-align:left;width:130px;" style="width:100%;" vtype="maxLength:16"/>--%>
                    <%--					</div>--%>
                    <%--					<div class="rightarea">--%>
                    <%--						<input style="width:100%;" id="currency" name="currency" class="mini-combobox mini-mustFill" labelField="true"  data="CommonUtil.serverData.dictionary.Currency" label="货币："   labelStyle="text-align:left;width:130px;" required="true"  allowInput="false" />--%>
                    <%--						<input style="width:100%;" id="rate" name="rate" class="mini-spinner mini-mustFill" labelField="true"  label="拆借利率(%)：" onValuechanged="interestAmount"  changeOnMousewheel='false' required="true"  format="n8" labelStyle="text-align:left;width:130px;" />--%>
                    <%--						<input style="width:100%;" id="maturityDate" name="maturityDate" class="mini-datepicker mini-mustFill" labelField="true" onValuechanged="compareDate" label="到期日：" ondrawdate="onDrawDateEnd" required="true"  labelStyle="text-align:left;width:130px;"    />--%>
                    <%--						<input style="width:100%;" id="occupancyDays" name="occupancyDays" class="mini-spinner" labelField="true" minErrorText="0" label="实际占款天数：" onValuechanged="interestAmount" changeOnMousewheel='false' maxValue="10000" labelStyle="text-align:left;width:130px;"    />--%>
                    <%--						<input style="width:100%;" id="interest" name="interest" class="mini-spinner mini-mustFill" labelField="true"  label="应计利息：" maxValue="999999999999999" changeOnMousewheel='false'  format="n4" required="true"  labelStyle="text-align:left;width:130px;"/>--%>
                    <%--						<input id="rateCode" name="rateCode" class="mini-textbox mini-mustFill" onbuttonclick="onRateEdit" labelField="true" label="利率代码：" style="width: 100%;" labelStyle="text-align:left;width:130px;" allowInput="false" requiredErrorText="该输入项为必输项"  required="true" />--%>
                    <%--						<!-- <input id="nettingStatus" name="nettingStatus" class="mini-combobox mini-mustFill" required="true"  labelField="true" label="净额清算状态：" labelStyle="text-align:left;width:130px;" style="width:100%;" data="CommonUtil.serverData.dictionary.YesNo" showNullItem="true"/> -->--%>
                    <%--						<input id="dealTransType" name="dealTransType" class="mini-combobox" labelField="true"  style="width:100%;"  label="数据来源：" data="CommonUtil.serverData.dictionary.dealTransType1" vtype="maxLength:10"  labelStyle="text-align:left;width:130px;"  value="1">--%>
                    <%--					</div>--%>
                    <%--				</div>--%>
                    <div class="mini-panel" title="交易主体信息" style="width:100%" allowResize="true"
                         collapseOnTitleClick="true">
                        <div class="leftarea">
                            <input id="postDate" name="postDate" class="mini-datepicker" labelField="true" label="交易日期："
                                   labelStyle="text-align:left;width:130px;" style="width:100%;"/>
                            <input style="width:100%;" id="amt" name="amt"
                                   class="mini-spinner mini-mustFill input-text-strong" labelField="true"
                                   changeOnMousewheel='false' format="n4" label="存款金额(元)："
                                   onValuechanged="interestAmount" required="true" maxValue="99999999999.9999"
                                   labelStyle="text-align:left;width:130px;" onvalidation="zeroValidation"/>
                            <input style="width:100%;" id="firstSettlementDate" name="firstSettlementDate"
                                   class="mini-datepicker mini-mustFill" labelField="true"
                                   onValuechanged="interestAmount" label="起息日：" ondrawdate="onDrawDateStart"
                                   required="true" labelStyle="text-align:left;width:130px;"/>
                            <input style="width:100%" id="tenor" name="tenor" class="mini-spinner mini-mustFill"
                                   labelField="true" label="存款期限(天)：" minValue="0" required="true" maxValue="99999"
                                   changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"/>
                            <input style="width:100%;" id="settlementAmount" name="settlementAmount"
                                   class="mini-spinner mini-mustFill input-text-strong" labelField="true"
                                   changeOnMousewheel='false' format="n4" label="到期还款金额：" required="true"
                                   maxValue="99999999999999.9999" labelStyle="text-align:left;width:130px;"
                                   onvalidation="zeroValidation"/>
                            <input style="width:100%;" id="basis" name="basis" class="mini-combobox mini-mustFill"
                                   labelField="true" label="计息基准：" onValuechanged="interestAmount"
                                   data="CommonUtil.serverData.dictionary.Basis"
                                   labelStyle="text-align:left;width:130px;" required="true"/>
                            <input style="width:100%;" id="intFeq" name="intFeq" class="mini-combobox mini-mustFill"
                                   labelField="true" data="CommonUtil.serverData.dictionary.CouponFrequently"
                                   label="付息频率：" labelStyle="text-align:left;width:130px;" required="true"
                                   allowInput="false" value="0"/>

                            <input id="note" name="note" class="mini-textbox" labelField="true" label="备注："
                                   labelStyle="text-align:left;width:130px;" style="width:100%;" vtype="maxLength:16"/>
                        </div>
                        <div class="rightarea">
                            <input style="width:100%;" id="currency" name="currency" class="mini-combobox mini-mustFill"
                                   labelField="true" data="CommonUtil.serverData.dictionary.Currency" label="货币："
                                   labelStyle="text-align:left;width:130px;" required="true" allowInput="false"
                                   value="CNY"/>
                            <input style="width:100%;" id="rate" name="rate"
                                   class="mini-spinner mini-mustFill input-text-strong" labelField="true"
                                   label="存款利率(%)：" onValuechanged="interestAmount" changeOnMousewheel='false'
                                   required="true" maxValue="99999.999999" format="n6"
                                   labelStyle="text-align:left;width:130px;" onvalidation="zeroValidation"/>
                            <input style="width:100%;" id="secondSettlementDate" name="secondSettlementDate"
                                   class="mini-datepicker mini-mustFill" labelField="true"
                                   onValuechanged="interestAmount" label="到期日：" ondrawdate="onDrawDateEnd"
                                   required="true" labelStyle="text-align:left;width:130px;"/>
                            <input style="width:100%;" id="occupancyDays" name="occupancyDays" class="mini-spinner"
                                   labelField="true" minErrorText="0" label="实际占款天数：" onValuechanged="interestAmount"
                                   changeOnMousewheel='false' maxValue="10000"
                                   labelStyle="text-align:left;width:130px;"/>
                            <input style="width:100%;" id="accuredInterest" name="accuredInterest"
                                   class="mini-spinner mini-mustFill input-text-strong" labelField="true" label="应计利息："
                                   maxValue="99999999999999.9999" changeOnMousewheel='false' format="n4" required="true"
                                   labelStyle="text-align:left;width:130px;"/>
                            <input id="dealTransType" name="dealTransType" class="mini-combobox" labelField="true"
                                   style="width:100%;" label="数据来源："
                                   data="CommonUtil.serverData.dictionary.dealTransType1" vtype="maxLength:10"
                                   labelStyle="text-align:left;width:130px;" value="1">
                            <input style="width:100%;" id="amtFeq" name="amtFeq" class="mini-combobox mini-mustFill"
                                   labelField="true" data="CommonUtil.serverData.dictionary.CouponFrequently"
                                   label="还本频率：" labelStyle="text-align:left;width:130px;" required="true"
                                   allowInput="false" value="0"/>
                        </div>
                    </div>
                    <%--				<%@ include file="../../Common/opicsLessDetail.jsp"%>--%>
                    <div class="mini-panel" title="清算信息" style="width:100%" allowResize="true"
                         collapseOnTitleClick="true">
                        <div class="leftarea">
                            <fieldset>
                                <legend>本方账户</legend>
                                <input id="buyCurreny" name="buyCurreny" class="mini-combobox" labelField="true"
                                       label="清算货币：" style="width:100%;" labelStyle="text-align:left;width:120px;"
                                       data="CommonUtil.serverData.dictionary.Currency" enabled="false"/>
                                <input id="bankName1" name="bankName1" class="mini-textbox" labelField="true"
                                       label="开户行名称：" labelStyle="text-align:left;width:120px;" style="width:100%;"
                                       requiredErrorText="该输入项为必输项" vtype="maxLength:60"/>
                                <input id="bankBic1" name="bankBic1" class="mini-textbox" labelField="true"
                                       label="开户行BIC CODE：" labelStyle="text-align:left;width:120px;"
                                       style="width:100%;" requiredErrorText="该输入项为必输项"
                                       onvalidation="onEnglishAndNumberValidation" vtype="maxLength:45"/>
                                <input id="dueBank1" name="dueBank1" class="mini-textbox" labelField="true"
                                       label="收款行名称：" labelStyle="text-align:left;width:120px;" style="width:100%;"
                                       requiredErrorText="该输入项为必输项" vtype="maxLength:60"/>
                                <input id="dueBankName1" name="dueBankName1" class="mini-textbox" labelField="true"
                                       label="收款行BIC CODE：" labelStyle="text-align:left;width:120px;"
                                       style="width:100%;" requiredErrorText="该输入项为必输项"
                                       onvalidation="onEnglishAndNumberValidation" vtype="maxLength:45"/>
                                <input id="dueBankAccount1" name="dueBankAccount1" class="mini-textbox"
                                       labelField="true" label="收款行账号：" labelStyle="text-align:left;width:120px;"
                                       style="width:100%;" requiredErrorText="该输入项为必输项"
                                       onvalidation="onEnglishAndNumberValidation" vtype="maxLength:20"/>
                                <input id="intermediaryBank1" name="intermediaryBank1" class="mini-textbox"
                                       labelField="true" label="中间行名称：" labelStyle="text-align:left;width:120px;"
                                       style="width:100%;" requiredErrorText="该输入项为必输项" vtype="maxLength:60"/>
                                <input id="intermediaryBicCode1" name="intermediaryBicCode1" class="mini-textbox"
                                       labelField="true" label="中间行BIC CODE：" labelStyle="text-align:left;width:120px;"
                                       style="width:100%;" requiredErrorText="该输入项为必输项"
                                       onvalidation="onEnglishAndNumberValidation" vtype="maxLength:45"/>
                            </fieldset>
                        </div>
                        <div class="rightarea">
                            <fieldset>
                                <legend>对手方账户</legend>
                                <input id="sellCurreny" name="sellCurreny" class="mini-combobox" labelField="true"
                                       label="清算货币：" style="width:100%;" labelStyle="text-align:left;width:120px;"
                                       requiredErrorText="该输入项为必输项" data="CommonUtil.serverData.dictionary.Currency"
                                       enabled="false"/>
                                <input id="bankName2" name="bankName2" class="mini-textbox" labelField="true"
                                       label="开户行名称：" labelStyle="text-align:left;width:120px;" style="width:100%;"
                                       requiredErrorText="该输入项为必输项" vtype="maxLength:60"/>
                                <input id="bankBic2" name="bankBic2" class="mini-textbox" labelField="true"
                                       label="开户行BIC CODE：" labelStyle="text-align:left;width:120px;"
                                       style="width:100%;" requiredErrorText="该输入项为必输项"
                                       onvalidation="onEnglishAndNumberValidation" vtype="maxLength:45"/>
                                <input id="dueBank2" name="dueBank2" class="mini-textbox" labelField="true"
                                       label="收款行名称：" labelStyle="text-align:left;width:120px;" style="width:100%;"
                                       requiredErrorText="该输入项为必输项" vtype="maxLength:60"/>
                                <input id="dueBankName2" name="dueBankName2" class="mini-textbox" labelField="true"
                                       label="收款行BIC CODE：" labelStyle="text-align:left;width:120px;"
                                       style="width:100%;" requiredErrorText="该输入项为必输项"
                                       onvalidation="onEnglishAndNumberValidation" vtype="maxLength:45"/>
                                <input id="dueBankAccount2" name="dueBankAccount2" class="mini-textbox"
                                       labelField="true" label="收款行账号：" labelStyle="text-align:left;width:120px;"
                                       style="width:100%;" requiredErrorText="该输入项为必输项"
                                       onvalidation="onEnglishAndNumberValidation" vtype="maxLength:20"/>
                                <input id="intermediaryBank2" name="intermediaryBank2" class="mini-textbox"
                                       labelField="true" label="中间行名称：" labelStyle="text-align:left;width:120px;"
                                       style="width:100%;" requiredErrorText="该输入项为必输项" vtype="maxLength:60"/>
                                <input id="intermediaryBicCode2" name="intermediaryBicCode2" class="mini-textbox"
                                       labelField="true" label="中间行BIC CODE：" labelStyle="text-align:left;width:120px;"
                                       style="width:100%;" requiredErrorText="该输入项为必输项"
                                       onvalidation="onEnglishAndNumberValidation" vtype="maxLength:45"/>
                            </fieldset>
                        </div>
                    </div>
                </div>
                <!-- 同业借款 -->
                <div id="field_form4" class="mini-fit area" style="background:white;height: 700px">
                    <div class="mini-panel" title="同业借款详情" style="width:100%;" allowResize="true"
                         collapseOnTitleClick="true">
                        <div class="leftarea">
                            <input style="width:100%;" id="contractId" name="contractId"
                                   class="mini-textbox mini-mustFill" labelField="true" requiredErrorText="该输入项为必输项"
                                   required="true" label="成交单编号：" labelStyle="text-align:left;width:130px;"
                                   onvalidation="onEnglishAndNumberValidations" vtype="maxLength:20"/>
                        </div>
                    </div>
                    <div class="mini-panel" title="成交双方信息" style="width:100%" allowResize="true"
                         collapseOnTitleClick="true">
                        <div class="leftarea">
                            <fieldset>
                                <legend>本方信息</legend>
                                <input id="myDir" name="myDir" class="mini-combobox" labelField="true"
                                       onvaluechanged="dirVerify" label="本方方向：" required="true" style="width:100%;"
                                       labelStyle="text-align:left;width:120px;"
                                       data="CommonUtil.serverData.dictionary.lending"/>
                                <input id="borrowInst" name="borrowInst" class="mini-textbox" labelField="true"
                                       label="机构：" style="width:100%;" required="true" enabled="false"
                                       labelStyle="text-align:left;width:120px;"/>
                                <input id="borrowTrader" name="borrowTrader" class="mini-textbox" labelField="true"
                                       label="交易员：" vtype="maxLength:5" required="true" enabled="false"
                                       labelStyle="text-align:left;width:120px;" style="width:100%;"/>
                            </fieldset>
                        </div>
                        <div class="rightarea">
                            <fieldset>
                                <legend>对手方信息</legend>
                                <input id="oppoDir" name="oppoDir" class="mini-combobox" enabled="false"
                                       labelField="true" label="对方方向：" required="true" style="width:100%;"
                                       labelStyle="text-align:left;width:120px;"
                                       data="CommonUtil.serverData.dictionary.lending"/>
                                <input id="removeInst" name="removeInst" class="mini-textbox" labelField="true"
                                       required="true" label="机构：" style="width:100%;"
                                       labelStyle="text-align:left;width:120px;"/>
                                <input id="removeTrader" name="removeTrader" class="mini-textbox" labelField="true"
                                       label="交易员：" required="true" vtype="maxLength:5"
                                       labelStyle="text-align:left;width:120px;" style="width:100%;"/>
                            </fieldset>
                        </div>
                    </div>

                    <div class="mini-panel" title="交易主体信息" style="width:100%" allowResize="true"
                         collapseOnTitleClick="true">
                        <div class="leftarea">
                            <input style="width:100%;" id="forDate" name="forDate" class="mini-datepicker mini-mustFill"
                                   labelField="true" required="true" label="成交日期："
                                   labelStyle="text-align:left;width:130px;"/>
                            <input style="width:100%" id="amt" name="amt" class="mini-spinner mini-mustFill"
                                   labelField="true" label="拆借金额(万元)：" required="true"
                                   onvaluechanged="associaDataCalculation" required="true" minValue="0"
                                   maxValue="9999999999999999.9999" format="n4" changeOnMousewheel="false"
                                   labelStyle="text-align:left;width:130px;" onvalidation="zeroValidation"/>
                            <input style="width:100%" id="firstSettlementDate" name="firstSettlementDate"
                                   class="mini-datepicker mini-mustFill" labelField="true" required="true"
                                   label="首次结算日：" onvaluechanged="associaDataCalculation" ondrawdate="onDrawDateFirst"
                                   labelStyle="text-align:left;width:130px;"/>
                            <input style="width:100%" id="tenor" name="tenor" class="mini-spinner mini-mustFill"
                                   labelField="true" label="拆借期限(天)：" minValue="0" required="true" maxValue="99999"
                                   changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"/>
                            <input style="width:100%" id="accuredInterest" name="accuredInterest"
                                   class="mini-spinner mini-mustFill" labelField="true" required="true" label="应计利息(元)："
                                   minValue="0" maxValue="9999999999999999.9999" format="n4" changeOnMousewheel="false"
                                   labelStyle="text-align:left;width:130px;"/>
                            <input id="basis" name="basis" class="mini-combobox mini-mustFill" required="true"
                                   labelField="true" label="计息基准：" style="width:100%;"
                                   data="CommonUtil.serverData.dictionary.Basis" onvaluechanged="linkIntCalculat"
                                   vtype="maxLength:15" labelStyle="text-align:left;width:130px;">
                            <input id="tradingModel" name="tradingModel" class="mini-combobox  mini-mustFill"
                                   required="true" labelField="true" label="交易模式："
                                   labelStyle="text-align:left;width:130px;" style="width:100%;"
                                   data="CommonUtil.serverData.dictionary.TradeModel"/>
                        </div>
                        <div class="rightarea">
                            <input style="width:100%" id="tradingProduct" name="tradingProduct"
                                   class="mini-combobox mini-mustFill" required="true" allowInput="false"
                                   labelField="true" label="交易品种：" labelStyle="text-align:left;width:130px;"
                                   data="CommonUtil.serverData.dictionary.dealVarietyCredit"/>
                            <input style="width:100%" id="rate" name="rate" class="mini-spinner mini-mustFill"
                                   labelField="true" label="拆借利率(%)：" required="true" minValue="0"
                                   maxValue="9999999999999999.99999999" format="n8"
                                   onvaluechanged="associaDataCalculation" changeOnMousewheel="false"
                                   labelStyle="text-align:left;width:130px;"/>
                            <input style="width:100%" id="secondSettlementDate" name="secondSettlementDate"
                                   class="mini-datepicker mini-mustFill" labelField="true" required="true"
                                   label="到期结算日：" onvaluechanged="associaDataCalculation" ondrawdate="onDrawDateSecond"
                                   labelStyle="text-align:left;width:130px;"/>
                            <input style="width:100%" id="occupancyDays" name="occupancyDays"
                                   class="mini-spinner mini-mustFill" labelField="true" onvaluechanged="linkageCalculat"
                                   required="true" label="实际占款天数(天)：" minValue="0" maxValue="99999"
                                   changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"/>
                            <input style="width:100%" id="settlementAmount" name="settlementAmount"
                                   class="mini-spinner mini-mustFill" labelField="true" required="true"
                                   label="到期还款金额(元)：" minValue="0" maxValue="9999999999999999.9999" format="n4"
                                   changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"/>
                            <input id="rateCode" name="rateCode" class="mini-textbox mini-mustFill"
                                   onbuttonclick="onRateEdit" labelField="true" label="利率代码：" style="width: 100%;"
                                   labelStyle="text-align:left;width:130px;" allowInput="false"
                                   requiredErrorText="该输入项为必输项" required="true"/>
                            <input id="nettingStatus" name="nettingStatus" class="mini-combobox  mini-mustFill"
                                   required="true" labelField="true" label="净额清算状态："
                                   labelStyle="text-align:left;width:130px;" style="width:100%;"
                                   data="CommonUtil.serverData.dictionary.YesNo" showNullItem="true"/>
                        </div>
                    </div>
                    <%@ include file="../../Common/opicsLessDetail.jsp" %>
                    <%@ include file="../../Common/RiskCenterDetail.jsp" %>
                    <div class="mini-panel" title="清算信息" style="width:100%" allowResize="true"
                         collapseOnTitleClick="true">
                        <div class="leftarea">
                            <fieldset>
                                <legend>本方账户</legend>
                                <input id="borrowAccname" name="borrowAccname" class="mini-textbox" labelField="true"
                                       label="资金账户户名：" vtype="maxLength:25" style="width:100%;"
                                       labelStyle="text-align:left;width:120px;"/>
                                <input id="borrowOpenBank" name="borrowOpenBank" class="mini-textbox" labelField="true"
                                       label="资金开户行：" vtype="maxLength:25" labelStyle="text-align:left;width:120px;"
                                       style="width:100%;"/>
                                <input id="borrowAccnum" name="borrowAccnum" class="mini-textbox" labelField="true"
                                       label="资金账号：" onvalidation="onEnglishAndNumberValidation" vtype="maxLength:50"
                                       labelStyle="text-align:left;width:120px;" style="width:100%;"/>
                                <input id="borrowPsnum" name="borrowPsnum" class="mini-textbox" labelField="true"
                                       label="支付系统行号：" onvalidation="onEnglishAndNumberValidation" vtype="maxLength:20"
                                       labelStyle="text-align:left;width:120px;" style="width:100%;"/>
                            </fieldset>
                        </div>
                        <div class="rightarea">
                            <fieldset>
                                <legend>对手方账户</legend>
                                <input id="removeAccname" name="removeAccname" class="mini-textbox" labelField="true"
                                       label="资金账户户名：" vtype="maxLength:25" style="width:100%;"
                                       labelStyle="text-align:left;width:120px;"/>
                                <input id="removeOpenBank" name="removeOpenBank" class="mini-textbox" labelField="true"
                                       label="资金开户行：" vtype="maxLength:25" labelStyle="text-align:left;width:120px;"
                                       style="width:100%;"/>
                                <input id="removeAccnum" name="removeAccnum" class="mini-textbox" labelField="true"
                                       label="资金账号：" onvalidation="onEnglishAndNumberValidation" vtype="maxLength:50"
                                       labelStyle="text-align:left;width:120px;" style="width:100%;"/>
                                <input id="removePsnum" name="removePsnum" class="mini-textbox" labelField="true"
                                       label="支付系统行号：" onvalidation="onEnglishAndNumberValidation" vtype="maxLength:20"
                                       labelStyle="text-align:left;width:120px;" style="width:100%;"/>
                            </fieldset>
                        </div>
                    </div>
                </div>
                <!-- 信用拆借 -->
                <div id="field_form2" class="mini-fit area" style="background:white;height: 700px">
                    <div class="mini-panel" title="信用拆借详情" style="width:100%;" allowResize="true"
                         collapseOnTitleClick="true">
                        <div class="leftarea">
                            <input style="width:100%;" id="contractId" name="contractId"
                                   class="mini-textbox mini-mustFill" labelField="true" requiredErrorText="该输入项为必输项"
                                   required="true" label="成交单编号：" labelStyle="text-align:left;width:130px;"
                                   onvalidation="onEnglishAndNumberValidations" vtype="maxLength:20"/>
                        </div>
                    </div>
                    <div class="mini-panel" title="成交双方信息" style="width:100%" allowResize="true"
                         collapseOnTitleClick="true">
                        <div class="leftarea">
                            <fieldset>
                                <legend>本方信息</legend>
                                <input id="myDir" name="myDir" class="mini-combobox" labelField="true"
                                       onvaluechanged="dirVerify" label="本方方向：" required="true" style="width:100%;"
                                       labelStyle="text-align:left;width:120px;"
                                       data="CommonUtil.serverData.dictionary.lending"/>
                                <input id="borrowInst" name="borrowInst" class="mini-textbox" labelField="true"
                                       label="机构：" style="width:100%;" required="true" enabled="false"
                                       labelStyle="text-align:left;width:120px;"/>
                                <input id="borrowTrader" name="borrowTrader" class="mini-textbox" labelField="true"
                                       label="交易员：" vtype="maxLength:5" required="true" enabled="false"
                                       labelStyle="text-align:left;width:120px;" style="width:100%;"/>
                            </fieldset>
                        </div>
                        <div class="rightarea">
                            <fieldset>
                                <legend>对手方信息</legend>
                                <input id="oppoDir" name="oppoDir" class="mini-combobox" enabled="false"
                                       labelField="true" label="对方方向：" required="true" style="width:100%;"
                                       labelStyle="text-align:left;width:120px;"
                                       data="CommonUtil.serverData.dictionary.lending"/>
                                <input id="removeInst" name="removeInst" class="mini-textbox" labelField="true"
                                       required="true" label="机构：" style="width:100%;"
                                       labelStyle="text-align:left;width:120px;"/>
                                <input id="removeTrader" name="removeTrader" class="mini-textbox" labelField="true"
                                       label="交易员：" required="true" vtype="maxLength:5"
                                       labelStyle="text-align:left;width:120px;" style="width:100%;"/>
                            </fieldset>
                        </div>
                    </div>

                    <div class="mini-panel" title="交易主体信息" style="width:100%" allowResize="true"
                         collapseOnTitleClick="true">
                        <div class="leftarea">
                            <input style="width:100%;" id="forDate" name="forDate" class="mini-datepicker mini-mustFill"
                                   labelField="true" required="true" label="成交日期："
                                   labelStyle="text-align:left;width:130px;"/>
                            <input style="width:100%" id="amt" name="amt" class="mini-spinner mini-mustFill"
                                   labelField="true" label="拆借金额(万元)：" required="true"
                                   onvaluechanged="associaDataCalculation" required="true" minValue="0"
                                   maxValue="9999999999999999.9999" format="n4" changeOnMousewheel="false"
                                   labelStyle="text-align:left;width:130px;" onvalidation="zeroValidation"/>
                            <input style="width:100%" id="firstSettlementDate" name="firstSettlementDate"
                                   class="mini-datepicker mini-mustFill" labelField="true" required="true"
                                   label="首次结算日：" onvaluechanged="associaDataCalculation" ondrawdate="onDrawDateFirst"
                                   labelStyle="text-align:left;width:130px;"/>
                            <input style="width:100%" id="tenor" name="tenor" class="mini-spinner mini-mustFill"
                                   labelField="true" label="拆借期限(天)：" minValue="0" required="true" maxValue="99999"
                                   changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"/>
                            <input style="width:100%" id="accuredInterest" name="accuredInterest"
                                   class="mini-spinner mini-mustFill" labelField="true" required="true" label="应计利息(元)："
                                   minValue="0" maxValue="9999999999999999.9999" format="n4" changeOnMousewheel="false"
                                   labelStyle="text-align:left;width:130px;"/>
                            <input id="basis" name="basis" class="mini-combobox mini-mustFill" required="true"
                                   labelField="true" label="计息基准：" style="width:100%;"
                                   data="CommonUtil.serverData.dictionary.Basis" onvaluechanged="linkIntCalculat"
                                   vtype="maxLength:15" labelStyle="text-align:left;width:130px;">
                            <input id="tradingModel" name="tradingModel" class="mini-combobox  mini-mustFill"
                                   required="true" labelField="true" label="交易模式："
                                   labelStyle="text-align:left;width:130px;" style="width:100%;"
                                   data="CommonUtil.serverData.dictionary.TradeModel"/>
                        </div>
                        <div class="rightarea">
                            <input style="width:100%" id="tradingProduct" name="tradingProduct"
                                   class="mini-combobox mini-mustFill" required="true" allowInput="false"
                                   labelField="true" label="交易品种：" labelStyle="text-align:left;width:130px;"
                                   data="CommonUtil.serverData.dictionary.dealVarietyCredit"/>
                            <input style="width:100%" id="rate" name="rate" class="mini-spinner mini-mustFill"
                                   labelField="true" label="拆借利率(%)：" required="true" minValue="0"
                                   maxValue="9999999999999999.99999999" format="n8"
                                   onvaluechanged="associaDataCalculation" changeOnMousewheel="false"
                                   labelStyle="text-align:left;width:130px;"/>
                            <input style="width:100%" id="secondSettlementDate" name="secondSettlementDate"
                                   class="mini-datepicker mini-mustFill" labelField="true" required="true"
                                   label="到期结算日：" onvaluechanged="associaDataCalculation" ondrawdate="onDrawDateSecond"
                                   labelStyle="text-align:left;width:130px;"/>
                            <input style="width:100%" id="occupancyDays" name="occupancyDays"
                                   class="mini-spinner mini-mustFill" labelField="true" onvaluechanged="linkageCalculat"
                                   required="true" label="实际占款天数(天)：" minValue="0" maxValue="99999"
                                   changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"/>
                            <input style="width:100%" id="settlementAmount" name="settlementAmount"
                                   class="mini-spinner mini-mustFill" labelField="true" required="true"
                                   label="到期还款金额(元)：" minValue="0" maxValue="9999999999999999.9999" format="n4"
                                   changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"/>
                            <input id="rateCode" name="rateCode" class="mini-textbox mini-mustFill"
                                   onbuttonclick="onRateEdit" labelField="true" label="利率代码：" style="width: 100%;"
                                   labelStyle="text-align:left;width:130px;" allowInput="false"
                                   requiredErrorText="该输入项为必输项" required="true"/>
                            <input id="nettingStatus" name="nettingStatus" class="mini-combobox  mini-mustFill"
                                   required="true" labelField="true" label="净额清算状态："
                                   labelStyle="text-align:left;width:130px;" style="width:100%;"
                                   data="CommonUtil.serverData.dictionary.YesNo" showNullItem="true"/>
                        </div>
                    </div>
                    <%@ include file="../../Common/opicsLessDetail.jsp" %>
                    <%@ include file="../../Common/RiskCenterDetail.jsp" %>
                    <div class="mini-panel" title="清算信息" style="width:100%" allowResize="true"
                         collapseOnTitleClick="true">
                        <div class="leftarea">
                            <fieldset>
                                <legend>本方账户</legend>
                                <input id="borrowAccname" name="borrowAccname" class="mini-textbox" labelField="true"
                                       label="资金账户户名：" vtype="maxLength:25" style="width:100%;"
                                       labelStyle="text-align:left;width:120px;"/>
                                <input id="borrowOpenBank" name="borrowOpenBank" class="mini-textbox" labelField="true"
                                       label="资金开户行：" vtype="maxLength:25" labelStyle="text-align:left;width:120px;"
                                       style="width:100%;"/>
                                <input id="borrowAccnum" name="borrowAccnum" class="mini-textbox" labelField="true"
                                       label="资金账号：" onvalidation="onEnglishAndNumberValidation" vtype="maxLength:50"
                                       labelStyle="text-align:left;width:120px;" style="width:100%;"/>
                                <input id="borrowPsnum" name="borrowPsnum" class="mini-textbox" labelField="true"
                                       label="支付系统行号：" onvalidation="onEnglishAndNumberValidation" vtype="maxLength:20"
                                       labelStyle="text-align:left;width:120px;" style="width:100%;"/>
                            </fieldset>
                        </div>
                        <div class="rightarea">
                            <fieldset>
                                <legend>对手方账户</legend>
                                <input id="removeAccname" name="removeAccname" class="mini-textbox" labelField="true"
                                       label="资金账户户名：" vtype="maxLength:25" style="width:100%;"
                                       labelStyle="text-align:left;width:120px;"/>
                                <input id="removeOpenBank" name="removeOpenBank" class="mini-textbox" labelField="true"
                                       label="资金开户行：" vtype="maxLength:25" labelStyle="text-align:left;width:120px;"
                                       style="width:100%;"/>
                                <input id="removeAccnum" name="removeAccnum" class="mini-textbox" labelField="true"
                                       label="资金账号：" onvalidation="onEnglishAndNumberValidation" vtype="maxLength:50"
                                       labelStyle="text-align:left;width:120px;" style="width:100%;"/>
                                <input id="removePsnum" name="removePsnum" class="mini-textbox" labelField="true"
                                       label="支付系统行号：" onvalidation="onEnglishAndNumberValidation" vtype="maxLength:20"
                                       labelStyle="text-align:left;width:120px;" style="width:100%;"/>
                            </fieldset>
                        </div>
                    </div>
                </div>
                <!-- 同业拆放（对俄） -->
                <div id="field_form5" class="mini-fit area" style="background:white;height: 700px">
                    <div class="mini-panel" title="同业拆放（对俄） 成交单编号" style="width:100%;" allowResize="true" collapseOnTitleClick="true">
                        <div class="leftarea">
                            <input style="width:100%;" id="contractId" name="contractId" class="mini-textbox mini-mustFill"
                                   required="true" labelField="true" requiredErrorText="该输入项为必输项" label="成交单编号："
                                   labelStyle="text-align:left;width:130px;" vtype="maxLength:20"
                                   onvalidation="onEnglishAndNumberValidation"/>
                        </div>
                        <div class="rightarea">
                            <input style="width:100%;" id="cfetsno" name="cfetsno" class="mini-textbox" label="CFETS机构码："
                                   labelField="true" visible="false" enabled="false" labelStyle="text-align:left;width:130px;"/>
                        </div>
                    </div>
                    <div class="mini-panel" title="成交双方信息" style="width:100%" allowResize="true" collapseOnTitleClick="true">
                        <div class="leftarea">
                            <fieldset>
                                <legend>本方信息</legend>
                                <input id="myDir" name="myDir" class="mini-combobox mini-mustFill" labelField="true"
                                       onvaluechanged="dirVerify" label="本方方向：" required="true" style="width:100%;"
                                       labelStyle="text-align:left;width:120px;"
                                       data="CommonUtil.serverData.dictionary.lending"/>
                                <input id="borrowInst" name="borrowInst" class="mini-textbox mini-mustFill" labelField="true"
                                       label="机构：" style="width:100%;" required="true" enabled="false"
                                       labelStyle="text-align:left;width:120px;"/>
                                <input id="borrowTrader" name="borrowTrader" class="mini-textbox mini-mustFill"
                                       labelField="true" label="交易员：" vtype="maxLength:20" required="true" enabled="false"
                                       labelStyle="text-align:left;width:120px;" style="width:100%;"/>
                            </fieldset>
                        </div>
                        <div class="rightarea">
                            <fieldset>
                                <legend>对手方信息</legend>
                                <input id="oppoDir" name="oppoDir" class="mini-combobox mini-mustFill" enabled="false"
                                       labelField="true" label="对方方向：" required="true" style="width:100%;"
                                       labelStyle="text-align:left;width:120px;"
                                       data="CommonUtil.serverData.dictionary.lending"/>
                                <input id="removeInst" name="removeInst" class="mini-textbox mini-mustFill"
                                       onbuttonclick="onButtonEdit" labelField="true" required="true" label="机构："
                                       allowInput="false" style="width:100%;" labelStyle="text-align:left;width:120px;"/>
                                <input id="removeTrader" name="removeTrader" class="mini-textbox mini-mustFill"
                                       labelField="true" label="交易员：" vtype="maxLength:20"
                                       labelStyle="text-align:left;width:120px;" style="width:100%;" required="true"/>
                            </fieldset>
                        </div>
                    </div>
                    <div class="mini-panel" title="交易主体信息" style="width:100%" allowResize="true" collapseOnTitleClick="true">
                        <div class="leftarea">
                            <input style="width:100%;" id="forDate" name="forDate" class="mini-datepicker mini-mustFill"
                                   labelField="true" required="true" label="成交日期：" labelStyle="text-align:left;width:130px;"/>
                            <input style="width:100%;" id="ccy" name="ccy" class="mini-combobox mini-mustFill"
                                   labelField="true" data="CommonUtil.serverData.dictionary.Currency" label="币种："
                                   labelStyle="text-align:left;width:130px;" required="true" allowInput="false"/>
                            <input style="width:100%" id="amt" name="amt" class="mini-spinner mini-mustFill input-text-strong"
                                   labelField="true" label="拆借金额(万元)：" required="true" onvaluechanged="associaDataCalculation"
                                   required="true" minValue="0" maxValue="9999999999.9999" format="n4"
                                   changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"
                                   onvalidation="zeroValidation"/>
                            <input style="width:100%" id="firstSettlementDate" name="firstSettlementDate"
                                   class="mini-datepicker mini-mustFill" labelField="true" required="true" label="首次结算日："
                                   onvaluechanged="associaDataCalculation" ondrawdate="onDrawDateFirst"
                                   labelStyle="text-align:left;width:130px;"/>
                            <input style="width:100%" id="tenor" name="tenor" class="mini-spinner mini-mustFill"
                                   labelField="true" label="拆借期限(天)：" minValue="0" required="true" maxValue="99999"
                                   changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"/>
                            <input style="width:100%" id="accuredInterest" name="accuredInterest"
                                   class="mini-spinner mini-mustFill input-text-strong" labelField="true" required="true"
                                   label="应计利息(元)：" minValue="0" maxValue="99999999999999.9999" format="n4"
                                   changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"/>

                            <input id="basis" name="basis" class="mini-combobox mini-mustFill" required="true" labelField="true"
                                   label="计息基准：" style="width:100%;" data="CommonUtil.serverData.dictionary.Basis"
                                   onvaluechanged="linkIntCalculat" vtype="maxLength:15"
                                   labelStyle="text-align:left;width:130px;">
                            <input style="width:100%" id="tradingProduct" name="tradingProduct"
                                   class="mini-combobox mini-mustFill" required="true" allowInput="false" labelField="true"
                                   label="交易品种：" labelStyle="text-align:left;width:130px;"
                                   data="CommonUtil.serverData.dictionary.dealVarietyCredit"
                                   onvaluechanged="associaDataCalculation"/>
                            <input id="dealTransType" name="dealTransType" class="mini-combobox" labelField="true"
                                   style="width:100%;" label="数据来源：" data="CommonUtil.serverData.dictionary.dealTransType1"
                                   vtype="maxLength:10" labelStyle="text-align:left;width:130px;" value="1"/>
                        </div>
                        <div class="rightarea">
                            <input style="width:100%" id="rate" name="rate" class="mini-spinner mini-mustFill input-text-strong"
                                   labelField="true" label="拆借利率(%)：" required="true" minValue="0" maxValue="99999.999999"
                                   format="n6" onvaluechanged="associaDataCalculation" changeOnMousewheel="false"
                                   labelStyle="text-align:left;width:130px;" onvalidation="zeroValidation"/>
                            <%--                        <input style="width:100%" id="spread8" name="spread8" class="mini-spinner  input-text-strong" labelField="true"  label="利率差(%)："   minValue="0" maxValue="99999.999999" format="n6" onvaluechanged="associaDataCalculation" changeOnMousewheel="false" labelStyle="text-align:left;width:130px;" onvalidation="zeroValidation"  />--%>
                            <input style="width:100%;" id="spread8" name="spread8" class="mini-spinner input-text-strong"
                                   labelField="true" label="利率差(%)：" changeOnMousewheel='false' format="n6"  onvaluechanged="associaDataCalculation"
                                   labelStyle="text-align:left;width:130px" minValue="-100" maxValue="1000000" value="0"/>
                            <input style="width:100%" id="secondSettlementDate" name="secondSettlementDate"
                                   class="mini-datepicker mini-mustFill" labelField="true" required="true" label="到期结算日："
                                   onvaluechanged="associaDataCalculation" ondrawdate="onDrawDateSecond"
                                   labelStyle="text-align:left;width:130px;"/>
                            <input style="width:100%" id="occupancyDays" name="occupancyDays" class="mini-spinner mini-mustFill"
                                   labelField="true" onvaluechanged="linkageCalculat" required="true" enabled="false"
                                   label="实际占款天数(天)：" minValue="0" maxValue="99999" changeOnMousewheel="false"
                                   labelStyle="text-align:left;width:130px;"/>
                            <input style="width:100%" id="settlementAmount" name="settlementAmount"
                                   class="mini-spinner mini-mustFill input-text-strong" labelField="true" required="true"
                                   label="到期还款金额(元)：" onvalidation="zeroValidation" minValue="0" maxValue="99999999999999.9999"
                                   format="n4" changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"/>

                            <%--                            <input style="width:100%" id="lastIntPayDate" name="lastIntPayDate" class="mini-datepicker mini-mustFill" labelField="true" required="true"    label="最后一次付息日：" onvaluechanged="associaDataCalculation"  ondrawdate="onDrawDateSecond" labelStyle="text-align:left;width:130px;"   />--%>
                            <input id="rateCode" name="rateCode" class="mini-textbox mini-mustFill"
                                   onbuttonclick="onRateEdit" labelField="true" label="利率代码：" style="width: 100%;"
                                   labelStyle="text-align:left;width:130px;" allowInput="false" requiredErrorText="该输入项为必输项"
                                   required="true"/>
                            <input id="nettingStatus" name="nettingStatus" class="mini-combobox  mini-mustFill" required="true"
                                   labelField="true" label="净额清算状态：" labelStyle="text-align:left;width:130px;"
                                   style="width:100%;" data="CommonUtil.serverData.dictionary.YesNo" showNullItem="true"/>

                            <input id="tradingModel" name="tradingModel" class="mini-combobox  mini-mustFill" required="true"
                                   labelField="true" label="交易模式：" labelStyle="text-align:left;width:130px;" style="width:100%;"
                                   data="CommonUtil.serverData.dictionary.TradeModel"/>
                            <input id="note" name="note" class="mini-textbox" labelField="true" label="交易事由："
                                   labelStyle="text-align:left;width:130px;" style="width:100%;" vtype="maxLength:16"/>
                        </div>
                    </div>
                    <div class="mini-panel" title="报表要素" style="width:100%;" allowResize="true" collapseOnTitleClick="false">
                        <div class="leftarea">
                            <input id="attributionDept" name="attributionDept" valueField="id" showFolderCheckBox="true"
                                   labelField="true" label="归属机构：" showCheckBox="true" style="width:100%"
                                   labelStyle="text-align:left;width:130px"
                                   showTreeLines="true" showTreeIcon="true" textField="instName" parentField="parentId"
                                   class="mini-textbox" expandOnLoad="true" resultAsTree="false"
                                   valueFromSelect="true" emptyText="请选择机构"/>
                            <input id="accountNature" name="accountNature" class="mini-combobox" labelField="true"
                                   label="账户性质：" labelStyle="text-align:left;width:130px;"
                                   style="width:100%;" data="CommonUtil.serverData.dictionary.accountNature" value="1"/>
                            <input id="pricingStandardType" name="pricingStandardType" class="mini-textbox" labelField="true"
                                   textField="text" valueField="id"
                                   label="定价基准类型：" labelStyle="text-align:left;width:130px;"
                                   style="width:100%;" enabled="false"/>
                            <input id="scheduleType" name="scheduleType" class="mini-combobox  mini-mustFill" required="true"
                                   labelField="true" label="付息类型：" labelStyle="text-align:left;width:130px;" style="width:100%;"
                                   data="CommonUtil.serverData.dictionary.scheduleType" value="D" onvaluechanged="selectCycle"/>
                            <%--                            <input style="width:100%" id="firstIPayDate" name="firstIPayDate" class="mini-datepicker mini-mustFill" labelField="true"  required="true"   label="首次付息日："  ondrawdate="onDrawDateFirst" labelStyle="text-align:left;width:130px;" onvaluechanged="getDayNumber" />--%>
                            <input style="width:100%" id="intPayDay" name="intPayDay" class="mini-spinner input-text-strong"
                                   labelField="true" label="利息付款日："
                                   labelStyle="text-align:left;width:130px;" onvaluechanged="getDayNumber" value="1"/>
                        </div>
                        <div class="rightarea">
                            <input id="fiveLevelClass" name="fiveLevelClass" class="mini-combobox" labelField="true"
                                   requiredErrorText="该输入项为必输项" label="五级分类：" labelStyle="text-align:left;width:130px;"
                                   style="width:100%;" data="CommonUtil.serverData.dictionary.FiveLevelClass" value="0"/>
                            <input style="width:100%;" id="accountUse" name="accountUse" class="mini-textbox" labelField="true"
                                   label="账户用途：" labelStyle="text-align:left;width:130px;"/>
                            <input id="rateFloatFrequency" name="rateFloatFrequency" class="mini-combobox" labelField="true"
                                   label="利率浮动频率：" labelStyle="text-align:left;width:130px;"
                                   style="width:100%;" data="CommonUtil.serverData.dictionary.rateFloatFrequency"/>
                            <input id="intPayCycle" name="intPayCycle" class="mini-combobox " labelField="true" label="付息周期："
                                   labelStyle="text-align:left;width:130px;" style="width:100%;"
                                   data="CommonUtil.serverData.dictionary.intPayCycle" showNullItem="true"/>
                            <input id="intDateRule" name="intDateRule" class="mini-combobox" labelField="true"
                                   required="true" label="利息日期规则：" labelStyle="text-align:left;width:130px;" style="width:100%;"
                                   data="CommonUtil.serverData.dictionary.intPayRule" showNullItem="true"/>
                        </div>
                    </div>
                    <%@ include file="../../Common/opicsLessDetail.jsp" %>
                    <%@ include file="../../Common/RiskCenterDetail.jsp" %>
                    <div class="mini-panel" title="清算信息" style="width:100%" allowResize="true" collapseOnTitleClick="true">
                        <div class="leftarea">
                            <fieldset>
                                <legend>本方账户</legend>
                                <input id="borrowAccname" name="borrowAccname" class="mini-textbox" labelField="true"
                                       label="资金账户户名：" vtype="maxLength:1333" style="width:100%;"
                                       labelStyle="text-align:left;width:120px;"/>
                                <input id="borrowOpenBank" name="borrowOpenBank" class="mini-textbox" labelField="true"
                                       label="资金开户行：" vtype="maxLength:1333" labelStyle="text-align:left;width:120px;"
                                       style="width:100%;"/>
                                <input id="borrowAccnum" name="borrowAccnum" class="mini-textbox" labelField="true"
                                       label="资金账号：" onvalidation="onEnglishAndNumberValidation" vtype="maxLength:400"
                                       labelStyle="text-align:left;width:120px;" style="width:100%;"/>
                                <input id="borrowPsnum" name="borrowPsnum" class="mini-textbox" labelField="true"
                                       label="支付系统行号：" onvalidation="onEnglishAndNumberValidation" vtype="maxLength:400"
                                       labelStyle="text-align:left;width:120px;" style="width:100%;"/>
                            </fieldset>
                        </div>
                        <div class="rightarea">
                            <fieldset>
                                <legend>对手方账户</legend>
                                <input id="removeAccname" name="removeAccname" class="mini-textbox" labelField="true"
                                       label="资金账户户名：" vtype="maxLength:1333" style="width:100%;"
                                       labelStyle="text-align:left;width:120px;"/>
                                <input id="removeOpenBank" name="removeOpenBank" class="mini-textbox" labelField="true"
                                       label="资金开户行：" vtype="maxLength:1333" labelStyle="text-align:left;width:120px;"
                                       style="width:100%;"/>
                                <input id="removeAccnum" name="removeAccnum" class="mini-textbox" labelField="true"
                                       label="资金账号：" onvalidation="onEnglishAndNumberValidation" vtype="maxLength:400"
                                       labelStyle="text-align:left;width:120px;" style="width:100%;"/>
                                <input id="removePsnum" name="removePsnum" class="mini-textbox" labelField="true"
                                       label="支付系统行号：" onvalidation="onEnglishAndNumberValidation" vtype="maxLength:400"
                                       labelStyle="text-align:left;width:120px;" style="width:100%;"/>
                            </fieldset>
                        </div>
                    </div>
                </div>
                <!-- 存放同业（对俄） -->
                <div id="field_form6" class="mini-fit area" style="background:white;height: 700px">
                    <div class="mini-panel" title="存放同业（对俄） 成交单编号" style="width:100%;" allowResize="true" collapseOnTitleClick="false">
                        <div class="leftarea">
                            <input style="width:98%;" id="contractId" name="contractId" class="mini-textbox mini-mustFill"
                                   labelField="true" requiredErrorText="该输入项为必输项" label="成交单编号：" required="true"
                                   labelStyle="text-align:left;width:130px;" vtype="maxLength:20"
                                   onvalidation="onEnglishAndNumberValidation"/>
                        </div>
                        <div class="rightarea">
                            <input style="width:100%;" id="cfetscn" name="cfetscn" class="mini-textbox" label="CFETS机构码："
                                   labelField="true" visible="false" enabled="false" labelStyle="text-align:left;width:130px;"
                                   visible="flase"/>
                        </div>
                    </div>
                    <div class="mini-panel" title="基础信息" style="width:100%" allowResize="true" collapseOnTitleClick="false">
                        <div class="leftarea">
                            <fieldset>
                                <input id="ticketId" name="ticketId" class="mini-textbox" labelField="true" label="合同号："
                                       style="width:100%;" emptyText="系统自动生成" enabled="false"
                                       labelStyle="text-align:left;width:120px;"/>
                                <input id="sponsor" name="sponsor" class="mini-textbox" labelField="true" label="审批发起人："
                                       vtype="maxLength:20" enabled="false" labelStyle="text-align:left;width:120px;"
                                       style="width:100%;"/>
                            </fieldset>
                        </div>
                        <div class="rightarea">
                            <fieldset>
                                <input id="sponInst" name="sponInst" class="mini-textbox" labelField="true" label="审批发起机构："
                                       allowInput="false" style="width:100%;" labelStyle="text-align:left;width:120px;"
                                       enabled="false"/>
                                <input id="aDate" name="aDate"  class="mini-datepicker" labelField="true" label="审批开始日期："
                                       vtype="maxLength:20" labelStyle="text-align:left;width:120px;" style="width:100%;"
                                       enabled="false"/>
                            </fieldset>
                        </div>
                    </div>
                    <div class="mini-panel" title="交易对手信息" style="width:100%" allowResize="true" collapseOnTitleClick="false">
                        <div class="leftarea">
                            <fieldset>
                                <input id="custId" name="custId" class="mini-textbox mini-mustFill"
                                       onbuttonclick="onButtonEdit" labelField="true" required="true" label="客户号："
                                       allowInput="false" style="width:100%;" labelStyle="text-align:left;width:120px;"/>
                                <input id="ctype" name="ctype" class="mini-combobox mini-mustFill" labelField="true"
                                       label="客户类型：" style="width:100%;" required="true"
                                       data="CommonUtil.serverData.dictionary.CType" enabled="false"
                                       labelStyle="text-align:left;width:120px;"/>
                            </fieldset>
                        </div>
                        <div class="rightarea">
                            <fieldset>
                                <input id="custName" name="custName" class="mini-textbox mini-mustFill" labelField="true"
                                       label="客户名称：" style="width:100%;" required="true" enabled="false"
                                       labelStyle="text-align:left;width:120px;"/>
                                <input id="clitype" name="clitype" class="mini-combobox mini-mustFill" labelField="true"
                                       label="金融机构类型：" style="width:100%;" required="true"
                                       data="CommonUtil.serverData.dictionary.clitype" enabled="false"
                                       labelStyle="text-align:left;width:120px;"/>
                            </fieldset>
                        </div>
                    </div>
                    <div class="mini-panel" title="交易主体信息" style="width:100%" allowResize="true" collapseOnTitleClick="false">
                        <div class="leftarea">
                            <input style="width:100%;" id="coreAcctNo" name="coreAcctNo" class="mini-textbox" labelField="true"
                                   label="核心存款账户：" labelStyle="text-align:left;width:130px;"
                                   onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" vtype="maxLength:1333"/>
                            <input style="width:100%;" id="forDate" name="forDate" class="mini-datepicker" labelField="true"
                                   label="交易日期：" labelStyle="text-align:left;width:130px;"/>
                            <input style="width:100%;" id="currency" name="currency" class="mini-combobox mini-mustFill"
                                   labelField="true" data="CommonUtil.serverData.dictionary.Currency" label="币种："
                                   labelStyle="text-align:left;width:130px;" required="true" allowInput="false"/>
                            <input style="width:100%;" id="rateType" name="rateType" class="mini-combobox mini-mustFill"
                                   labelField="true" data="CommonUtil.serverData.dictionary.fixfloat" label="利率类型："
                                   labelStyle="text-align:left;width:130px;" required="true" allowInput="false"/>
                            <input style="width: 100%;" id="basisRateCode" name="basisRateCode"
                                   class="mini-textbox mini-mustFill" onbuttonclick="onRateEdit" labelField="true"
                                   label="基准利率代码：" labelStyle="text-align:left;width:130px;" allowInput="false"
                                   requiredErrorText="该输入项为必输项" required="true"/>
                            <input style="width:100%;" id="firstSettlementDate" name="firstSettlementDate"
                                   class="mini-datepicker mini-mustFill" labelField="true" onValuechanged="compareDate"
                                   label="起息日期：" ondrawdate="onDrawDateStart" required="true"
                                   labelStyle="text-align:left;width:130px;"/>
                            <input style="width:100%" id="tenor" name="tenor" class="mini-textbox mini-mustFill"
                                   labelField="true" label="占款期限(天)：" required="true"
                                   labelStyle="text-align:left;width:130px;"/>
                            <input style="width:100%;" id="accuredInterest" name="accuredInterest"
                                   class="mini-spinner mini-mustFill input-text-strong" labelField="true"
                                   changeOnMousewheel='false' format="n4" label="到期利息(元)：" required="true"
                                   maxValue="99999999999999.9999" labelStyle="text-align:left;width:130px;"
                                   onValuechanged="interestAmount"/>
                            <%--                    <input id="bussInstId" name="bussInstId" class="mini-textbox" labelField="true"  label="业务归属部门：" vtype="maxLength:20" labelStyle="text-align:left;width:130px;" style="width:100%;" />--%>
                            <input id="direction" name="direction" class="mini-textbox" labelField="true" label="交易方向："
                                   vtype="maxLength:20" labelStyle="text-align:left;width:130px;" style="width:100%;"
                                   visible="false" value="S"/>
                            <input id="note" name="note" class="mini-textbox" labelField="true" label="交易事由：" vtype="maxLength:20"
                                   labelStyle="text-align:left;width:130px;" style="width:100%;"/>
                            <input style="width:100%;" id="trad" name="trad" class="mini-textbox" labelField="true"
                                   label="客户经理：" vtype="maxLength:20" labelStyle="text-align:left;width:130px;"/>
                            <%--                    <input style="width:100%" id="baseRate" name="baseRate"--%>
                            <%--                           class="mini-spinner input-text-strong"--%>
                            <%--                           labelField="true" label="基准利率(%)：" minValue="0" format="n4"--%>
                            <%--                           maxValue="99999.9999" changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"/>--%>
                        </div>
                        <div class="rightarea">
                            <input style="width:100%;" id="coreAcctName" name="coreAcctName" class="mini-textbox"
                                   labelField="true" label="核心存款账户名：" labelStyle="text-align:left;width:130px;"
                                   vtype="maxLength:1333"/>
                            <input style="width:100%;" id="amt" name="amt" class="mini-spinner mini-mustFill input-text-strong"
                                   labelField="true" changeOnMousewheel='false' format="n4" label="本金(元)：" required="true"
                                   maxValue="99999999999999.9999" labelStyle="text-align:left;width:130px;"
                                   onValuechanged="interestAmount"/>
                            <input style="width:100%" id="benchmarkSpread" name="benchmarkSpread"
                                   class="mini-spinner input-text-strong" labelField="true" label="利率点差(千分之一)："
                                   minValue="-1000000" required="true" format="n4" maxValue="99999.9999"
                                   changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"
                                   onValuechanged="interestAmount"/>
                            <input style="width:100%" id="rate" name="rate" class="mini-spinner mini-mustFill input-text-strong"
                                   labelField="true" label="利率(%)：" minValue="0" required="true" format="n4"
                                   maxValue="99999.9999" changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"
                                   onValuechanged="interestAmount"/>
                            <input style="width:100%;" id="secondSettlementDate" name="secondSettlementDate"
                                   class="mini-datepicker mini-mustFill" labelField="true" onValuechanged="compareDate"
                                   label="到期日期：" ondrawdate="onDrawDateEnd" required="true"
                                   labelStyle="text-align:left;width:130px;"/>
                            <input style="width:100%;" id="basis" name="basis" class="mini-combobox mini-mustFill"
                                   labelField="true" label="计息基础：" data="CommonUtil.serverData.dictionary.Basis"
                                   labelStyle="text-align:left;width:130px;" required="true" onValuechanged="interestAmount"/>
                            <input style="width:100%" id="rateNote" name="rateNote" class="mini-textbox" labelField="true"
                                   label="利率备注：" labelStyle="text-align:left;width:130px;"/>

                            <input style="width:100%" id="settlementAmount" name="settlementAmount"
                                   class="mini-spinner mini-mustFill input-text-strong" labelField="true" label="到期金额(元)："
                                   minValue="0" required="true" format="n4" maxValue="99999999999999.9999"
                                   changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"/>
                            <input style="width:100%;" id="hasVerify" name="hasVerify" class="mini-combobox" labelField="true"
                                   label="是否开立存款证实书：" vtype="maxLength:20" labelStyle="text-align:left;width:130px;"
                                   data="CommonUtil.serverData.dictionary.YesNo"/>
                            <input id="depositAccount" name="depositAccount" class="mini-textbox" labelField="true" label="存款账号："
                                   labelStyle="text-align:left;width:130px;" style="width:100%;"/>
                        </div>
                    </div>
                    <div class="mini-panel" title="报表要素" style="width:100%;" allowResize="true" collapseOnTitleClick="false">
                        <div class="leftarea">
                            <input id="attributionDept" name="attributionDept" valueField="id" showFolderCheckBox="true"
                                   labelField="true" label="归属机构：" showCheckBox="true" style="width:100%"
                                   labelStyle="text-align:left;width:130px"
                                   showTreeLines="true" showTreeIcon="true" textField="instName" parentField="parentId"
                                   class="mini-textbox" expandOnLoad="true" resultAsTree="false"
                                   valueFromSelect="true" emptyText="请选择机构"/>
                            <input id="accountNature" name="accountNature" class="mini-combobox" labelField="true"
                                   label="账户性质：" labelStyle="text-align:left;width:130px;"
                                   style="width:100%;" data="CommonUtil.serverData.dictionary.accountNature" value="1"/>
                            <input id="pricingStandardType" name="pricingStandardType" class="mini-textbox" labelField="true"
                                   label="定价基准类型：" labelStyle="text-align:left;width:130px;"
                                   style="width:100%;" data="CommonUtil.serverData.dictionary.pricingStandardType" enabled="false"/>
                            <input style="width:100%" id="scheduleType" name="scheduleType" class="mini-combobox"
                                   labelField="true" label="付息类型：" labelStyle="text-align:left;width:130px;"
                                   data="CommonUtil.serverData.dictionary.scheduleType" value="D" onvaluechanged="selectCycle"/>
                            <input style="width:100%" id="intpayday" name="intpayday" class="mini-textbox" labelField="true"
                                   label="利息付款日：" labelStyle="text-align:left;width:130px;" enabled="false" value="1     "/>
                        </div>
                        <div class="rightarea">
                            <input id="fiveLevelClass" name="fiveLevelClass" class="mini-combobox" labelField="true"
                                   requiredErrorText="该输入项为必输项" label="五级分类：" labelStyle="text-align:left;width:130px;"
                                   style="width:100%;" data="CommonUtil.serverData.dictionary.FiveLevelClass" value="0"/>
                            <input style="width:100%;" id="accountUse" name="accountUse" class="mini-textbox" labelField="true"
                                   label="账户用途：" labelStyle="text-align:left;width:130px;"/>
                            <input id="rateFloatFrequency" name="rateFloatFrequency" class="mini-combobox" labelField="true"
                                   label="利率浮动频率：" labelStyle="text-align:left;width:130px;"
                                   style="width:100%;" data="CommonUtil.serverData.dictionary.rateFloatFrequency"/>
                            <input style="width:100%" id="paymentFrequency" name="paymentFrequency"
                                   class="mini-combobox" labelField="true" label="付息频率：" required="true"
                                   labelStyle="text-align:left;width:130px;"
                                   data="CommonUtil.serverData.dictionary.intPayCycle"/>
                            <input style="width:100%;" id="intdaterule" name="intdaterule" class="mini-combobox"
                                   labelField="true" label="付息日调整规则：" data="CommonUtil.serverData.dictionary.intPayRule"
                                   labelStyle="text-align:left;width:130px;" enabled="false"/>
                        </div>
                    </div>
                    <%@ include file="../../Common/opicsLessDetail.jsp" %>
                    <div class="mini-panel" title="清算信息" style="width:100%" allowResize="true" collapseOnTitleClick="true">
                        <div class="leftarea">
                            <fieldset>
                                <legend>本方账户</legend>
                                <input style="width:100%;" id="selfAcccode" name="selfAcccode" class="mini-textbox"
                                       labelField="true" label="本方帐号：" labelStyle="text-align:left;width:120px;"
                                       onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" vtype="maxLength:1333"
                                       value="9001000113901401"/>
                                <input style="width:100%;" id="selfAccname" name="selfAccname" class="mini-textbox"
                                       labelField="true" label="本方帐号名称：" labelStyle="text-align:left;width:120px;"
                                       vtype="maxLength:1333" value="投资类业务"/>
                                <input style="width:100%;" id="selfBankcode" name="selfBankcode" class="mini-textbox"
                                       labelField="true" label="本方开户行行号：" labelStyle="text-align:left;width:120px;"
                                       onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" vtype="maxLength:400"
                                       value="9001000113901401"/>
                                <input style="width:100%;" id="selfBankname" name="selfBankname" class="mini-textbox"
                                       labelField="true" label="本方开户行名称：" labelStyle="text-align:left;width:120px;"
                                       vtype="maxLength:400" value="投资类业务"/>
                            </fieldset>
                        </div>
                        <div class="rightarea">
                            <fieldset>
                                <legend>对手方账户</legend>
                                <input style="width:100%;" id="partyAcccode" name="partyAcccode" class="mini-textbox"
                                       labelField="true" label="对方帐号：" labelStyle="text-align:left;width:120px;"
                                       onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" vtype="maxLength:1333"/>
                                <input style="width:100%;" id="partyAccname" name="partyAccname" class="mini-textbox"
                                       labelField="true" label="对方帐号名称：" labelStyle="text-align:left;width:120px;"
                                       vtype="maxLength:1333"/>
                                <input style="width:100%;" id="partyBankcode" name="partyBankcode" class="mini-textbox"
                                       labelField="true" label="对方开户行行号：" labelStyle="text-align:left;width:120px;"
                                       onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" vtype="maxLength:400"/>
                                <input style="width:100%;" id="partyBankname" name="partyBankname" class="mini-textbox"
                                       labelField="true" label="对方开户行名称：" labelStyle="text-align:left;width:120px;"
                                       vtype="maxLength:400"/>
                            </fieldset>
                        </div>
                    </div>
                </div>
                <!-- 存放同业 -->
                <div id="field_form7" class="mini-fit area" style="background:white;height: 700px">
                    <div class="mini-panel" title="成交单编号" style="width:100%;" allowResize="true" collapseOnTitleClick="false">
                        <div class="leftarea">
                            <input style="width:98%;" id="contractId" name="contractId" class="mini-textbox mini-mustFill"
                                   labelField="true" requiredErrorText="该输入项为必输项" label="成交单编号：" required="true"
                                   labelStyle="text-align:left;width:130px;" vtype="maxLength:20"
                                   onvalidation="onEnglishAndNumberValidation"/>
                        </div>
                        <div class="rightarea">
                            <input style="width:100%;" id="cfetscn" name="cfetscn" class="mini-textbox" label="CFETS机构码："
                                   labelField="true" visible="false" enabled="false" labelStyle="text-align:left;width:130px;"
                                   visible="flase"/>
                        </div>
                    </div>
                    <div class="mini-panel" title="基础信息" style="width:100%" allowResize="true" collapseOnTitleClick="false">
                        <div class="leftarea">
                            <fieldset>
                                <input id="ticketId" name="ticketId" class="mini-textbox" labelField="true" label="合同号："
                                       style="width:100%;" emptyText="系统自动生成" enabled="false"
                                       labelStyle="text-align:left;width:120px;"/>
                                <input id="sponsor" name="sponsor" class="mini-textbox" labelField="true" label="审批发起人："
                                       vtype="maxLength:20" enabled="false" labelStyle="text-align:left;width:120px;"
                                       style="width:100%;"/>
                            </fieldset>
                        </div>
                        <div class="rightarea">
                            <fieldset>
                                <input id="sponInst" name="sponInst" class="mini-textbox" labelField="true" label="审批发起机构："
                                       allowInput="false" style="width:100%;" labelStyle="text-align:left;width:120px;"
                                       enabled="false"/>
                                <input id="aDate" name="aDate" class="mini-datepicker" labelField="true" label="审批开始日期："
                                       vtype="maxLength:20" labelStyle="text-align:left;width:120px;" style="width:100%;"
                                       enabled="false"/>
                            </fieldset>
                        </div>
                    </div>
                    <div class="mini-panel" title="交易对手信息" style="width:100%" allowResize="true" collapseOnTitleClick="false">
                        <div class="leftarea">
                            <fieldset>
                                <input id="custId" name="custId" class="mini-textbox mini-mustFill"
                                       onbuttonclick="onButtonEdit" labelField="true" required="true" label="客户号："
                                       allowInput="false" style="width:100%;" labelStyle="text-align:left;width:120px;"/>
                                <input id="ctype" name="ctype" class="mini-combobox mini-mustFill" labelField="true"
                                       label="客户类型：" style="width:100%;" required="true"
                                       data="CommonUtil.serverData.dictionary.CType" enabled="false"
                                       labelStyle="text-align:left;width:120px;"/>
                            </fieldset>
                        </div>
                        <div class="rightarea">
                            <fieldset>
                                <input id="custName" name="custName" class="mini-textbox mini-mustFill" labelField="true"
                                       label="客户名称：" style="width:100%;" required="true" enabled="false"
                                       labelStyle="text-align:left;width:120px;"/>
                                <input id="clitype" name="clitype" class="mini-combobox mini-mustFill" labelField="true"
                                       label="金融机构类型：" style="width:100%;" required="true"
                                       data="CommonUtil.serverData.dictionary.clitype" enabled="false"
                                       labelStyle="text-align:left;width:120px;"/>
                            </fieldset>
                        </div>
                    </div>
                    <div class="mini-panel" title="交易主体信息" style="width:100%" allowResize="true" collapseOnTitleClick="false">
                        <div class="leftarea">
                            <input style="width:100%;" id="coreAcctNo" name="coreAcctNo" class="mini-textbox mini-mustFill"
                                   labelField="true" label="核心存款账户：" labelStyle="text-align:left;width:130px;"
                                   onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" vtype="maxLength:1333"/>
                            <input style="width:100%;" id="rateType" name="rateType" class="mini-combobox mini-mustFill"
                                   labelField="true" data="CommonUtil.serverData.dictionary.fixfloat" label="利率类型："
                                   labelStyle="text-align:left;width:130px;" required="true" allowInput="false"/>
                            <input style="width:100%" id="benchmarkSpread" name="benchmarkSpread"
                                   class="mini-spinner input-text-strong" labelField="true" label="利率点差(千分之一)："
                                   minValue="-1000000" format="n4" maxValue="99999.9999" changeOnMousewheel="false"
                                   labelStyle="text-align:left;width:130px;" onValuechanged="interestAmount"/>
                            <input style="width:100%;" id="firstSettlementDate" name="firstSettlementDate"
                                   class="mini-datepicker mini-mustFill" labelField="true" onValuechanged="compareDate"
                                   label="起息日期：" ondrawdate="onDrawDateStart" required="true"
                                   labelStyle="text-align:left;width:130px;"/>
                            <input style="width:100%" id="tenor" name="tenor" class="mini-textbox mini-mustFill"
                                   labelField="true" label="存款期限(天)：" required="true"
                                   labelStyle="text-align:left;width:130px;"/>
                            <input style="width:100%;" id="amt" name="amt" class="mini-spinner mini-mustFill input-text-strong"
                                   labelField="true" changeOnMousewheel='false' format="n4" label="本金(元)：" required="true"
                                   maxValue="99999999999999.9999" labelStyle="text-align:left;width:130px;"
                                   onValuechanged="interestAmount"/>
                            <input style="width:100%" id="settlementAmount" name="settlementAmount"
                                   class="mini-spinner mini-mustFill input-text-strong" labelField="true" label="到期金额(元)："
                                   minValue="0" required="true" format="n4" maxValue="99999999999999.9999"
                                   changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"/>
                            <input style="width:100%" id="scheduleType" name="scheduleType" class="mini-combobox mini-mustFill"
                                   labelField="true" label="付息类型：" labelStyle="text-align:left;width:130px;" required="true"
                                   data="CommonUtil.serverData.dictionary.scheduleType" value="D" onvaluechanged="selectCycle"/>
                            <input style="width:100%;" id="firstSettlDay" name="firstSettlDay" class="mini-datepicker" labelField="true"  label="首次结息日："
                                   labelStyle="text-align:left;width:130px;"/>
                            <input style="width:100%;" id="settlDayAgreement" name="settlDayAgreement" class="mini-combobox"
                                   labelField="true" data="CommonUtil.serverData.dictionary.SettAgreement" label="结息日约定："
                                   labelStyle="text-align:left;width:130px;" allowInput="false"/>
                            <input style="width:100%;" id="advAgreement" name="advAgreement" class="mini-combobox mini-mustFill"
                                   labelField="true" data="CommonUtil.serverData.dictionary.AdvAgreement" label="提前支取约定："
                                   labelStyle="text-align:left;width:130px;" required="true" allowInput="false" value="1" onvaluechanged="selectAgreement"/>
                            <input style="width:100%;" id="currency" name="currency" class="mini-combobox mini-mustFill"
                                   labelField="true" data="CommonUtil.serverData.dictionary.Currency" label="币种："
                                   labelStyle="text-align:left;width:130px;" required="true" allowInput="false" enabled="false"
                                   value="CNY"/>
                            <input style="width:100%;" id="forDate" name="forDate" class="mini-datepicker" labelField="true"
                                   label="交易日期：" labelStyle="text-align:left;width:130px;" visible="false"/>
                            <input id="direction" name="direction" class="mini-textbox" labelField="true"  label="交易方向：" vtype="maxLength:20" labelStyle="text-align:left;width:130px;" style="width:100%;" visible="false" value="S"/>

                        </div>
                        <div class="rightarea">
                            <input style="width:100%;" id="coreAcctName" name="coreAcctName" class="mini-textbox mini-mustFill"
                                   labelField="true" label="核心存款账户名：" labelStyle="text-align:left;width:130px;"
                                   vtype="maxLength:1333"/>
                            <input style="width: 100%;" id="basisRateCode" name="basisRateCode"
                                   class="mini-textbox mini-mustFill" onbuttonclick="onRateEdit" labelField="true"
                                   label="基准利率代码：" labelStyle="text-align:left;width:130px;" allowInput="false"
                                   requiredErrorText="该输入项为必输项" required="true"/>
                            <input style="width:100%" id="rate" name="rate" class="mini-spinner mini-mustFill input-text-strong"
                                   labelField="true" label="基准利率(%)：" minValue="0" required="true" format="n4"
                                   maxValue="99999.9999" changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"/>
                            <input style="width:100%;" id="secondSettlementDate" name="secondSettlementDate"
                                   class="mini-datepicker mini-mustFill" labelField="true" onValuechanged="compareDate"
                                   label="到期日期：" ondrawdate="onDrawDateEnd" required="true"
                                   labelStyle="text-align:left;width:130px;"/>
                            <input style="width:100%" id="occupancyDays" name="occupancyDays" class="mini-textbox mini-mustFill"
                                   labelField="true" label="实际占款期限(天)：" required="true"
                                   labelStyle="text-align:left;width:130px;"/>
                            <input style="width:100%;" id="basis" name="basis" class="mini-combobox mini-mustFill"
                                   labelField="true" label="计息基础：" data="CommonUtil.serverData.dictionary.Basis"
                                   labelStyle="text-align:left;width:130px;" required="true" onValuechanged="interestAmount"/>
                            <input style="width:100%;" id="accuredInterest" name="accuredInterest"
                                   class="mini-spinner mini-mustFill input-text-strong" labelField="true"
                                   changeOnMousewheel='false' format="n4" label="到期利息(元)：" required="true"
                                   maxValue="99999999999999.9999" labelStyle="text-align:left;width:130px;"
                                   onValuechanged="interestAmount"/>
                            <input style="width:100%" id="paymentFrequency" name="paymentFrequency" class="mini-combobox"
                                   labelField="true" label="付息频率：" labelStyle="text-align:left;width:130px;"
                                   data="CommonUtil.serverData.dictionary.intPayCycle" enabled="false"/>
                            <input style="width:100%;" id="firstPayDay" name="firstPayDay" class="mini-datepicker" labelField="true" label="首次付息日："
                                   labelStyle="text-align:left;width:130px;"/>
                            <input style="width:100%;" id="payDayAgreement" name="payDayAgreement" class="mini-combobox"
                                   labelField="true" data="CommonUtil.serverData.dictionary.SettAgreement" label="付息日约定："
                                   labelStyle="text-align:left;width:130px;" allowInput="false"/>
                            <input style="width:100%" id="advRate" name="advRate"
                                   class="mini-spinner input-text-strong" labelField="true" label="提前支取预期利率（%）："
                                   minValue="-100" format="n4" maxValue="100" changeOnMousewheel="false"
                                   labelStyle="text-align:left;width:130px;"/>
                            <input id="note" name="note" class="mini-textbox" labelField="true" label="备注：" vtype="maxLength:20"
                                   labelStyle="text-align:left;width:130px;" style="width:100%;"/>
                        </div>
                    </div>
                    <div class="mini-panel" title="报表要素" style="width:100%;" allowResize="true" collapseOnTitleClick="false">
                        <div class="leftarea">
                            <input id="attributionDept" name="attributionDept" valueField="id" showFolderCheckBox="true"
                                   labelField="true" label="归属机构：" showCheckBox="true" style="width:100%"
                                   labelStyle="text-align:left;width:130px"
                                   showTreeLines="true" showTreeIcon="true" textField="instName" parentField="parentId"
                                   class="mini-textbox" expandOnLoad="true" resultAsTree="false"
                                   valueFromSelect="true" emptyText="请选择机构"/>
                            <input id="accountNature" name="accountNature" class="mini-combobox" labelField="true"
                                   label="账户性质：" labelStyle="text-align:left;width:130px;"
                                   style="width:100%;" data="CommonUtil.serverData.dictionary.accountNature" value="1"/>
                            <input id="pricingStandardType" name="pricingStandardType" class="mini-textbox" labelField="true"
                                   label="定价基准类型：" labelStyle="text-align:left;width:130px;"
                                   style="width:100%;" data="CommonUtil.serverData.dictionary.pricingStandardType" enabled="false"/>
                            <input style="width:100%" id="intpayday" name="intpayday" class="mini-textbox" labelField="true"
                                   label="利息付款日：" labelStyle="text-align:left;width:130px;" enabled="false" value="1"/>
                            <input id="depositAccount" name="depositAccount" class="mini-textbox" labelField="true" label="存款账号："
                                   labelStyle="text-align:left;width:130px;" style="width:100%;" />
                            <input style="width:100%;" id="trad" name="trad" class="mini-textbox" labelField="true"
                                   label="客户经理：" vtype="maxLength:20" labelStyle="text-align:left;width:130px;"/>
                        </div>
                        <div class="rightarea">
                            <input id="fiveLevelClass" name="fiveLevelClass" class="mini-combobox" labelField="true"
                                   requiredErrorText="该输入项为必输项" label="五级分类：" labelStyle="text-align:left;width:130px;"
                                   style="width:100%;" data="CommonUtil.serverData.dictionary.FiveLevelClass" value="0"/>
                            <input style="width:100%;" id="accountUse" name="accountUse" class="mini-textbox" labelField="true"
                                   label="账户用途：" labelStyle="text-align:left;width:130px;"/>
                            <input id="rateFloatFrequency" name="rateFloatFrequency" class="mini-combobox" labelField="true"
                                   label="利率浮动频率：" labelStyle="text-align:left;width:130px;"
                                   style="width:100%;" data="CommonUtil.serverData.dictionary.rateFloatFrequency"/>
                            <input style="width:100%;" id="intdaterule" name="intdaterule" class="mini-combobox"
                                   labelField="true" label="付息日调整规则：" data="CommonUtil.serverData.dictionary.intPayRule"
                                   labelStyle="text-align:left;width:130px;" enabled="false"/>
                            <input style="width:100%" id="rateNote" name="rateNote" class="mini-textbox" labelField="true"
                                   label="利率备注：" labelStyle="text-align:left;width:130px;"/>
                            <input style="width:100%;" id="hasVerify" name="hasVerify" class="mini-combobox" labelField="true"
                                   label="是否开立存款证实书：" vtype="maxLength:20" labelStyle="text-align:left;width:130px;"
                                   data="CommonUtil.serverData.dictionary.YesNo"/>
                        </div>
                    </div>
                    <%@ include file="../../Common/opicsLessDetail.jsp" %>
                    <div class="mini-panel" title="清算信息" style="width:100%" allowResize="true" collapseOnTitleClick="true">
                        <div class="leftarea">
                            <fieldset>
                                <legend>本方账户</legend>
                                <input style="width:100%;" id="selfAcccode" name="selfAcccode" class="mini-textbox"
                                       labelField="true" label="本方帐号：" labelStyle="text-align:left;width:120px;"
                                       onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" vtype="maxLength:1333"
                                       value="9001000113901401"/>
                                <input style="width:100%;" id="selfAccname" name="selfAccname" class="mini-textbox"
                                       labelField="true" label="本方帐号名称：" labelStyle="text-align:left;width:120px;"
                                       vtype="maxLength:1333" value="投资类业务"/>
                                <input style="width:100%;" id="selfBankcode" name="selfBankcode" class="mini-textbox"
                                       labelField="true" label="本方开户行行号：" labelStyle="text-align:left;width:120px;"
                                       onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" vtype="maxLength:400"
                                       value="9001000113901401"/>
                                <input style="width:100%;" id="selfBankname" name="selfBankname" class="mini-textbox"
                                       labelField="true" label="本方开户行名称：" labelStyle="text-align:left;width:120px;"
                                       vtype="maxLength:400" value="投资类业务"/>
                            </fieldset>
                        </div>
                        <div class="rightarea">
                            <fieldset>
                                <legend>对手方账户</legend>
                                <input style="width:100%;" id="partyAcccode" name="partyAcccode" class="mini-textbox"
                                       labelField="true" label="对方帐号：" labelStyle="text-align:left;width:120px;"
                                       onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" vtype="maxLength:1333"/>
                                <input style="width:100%;" id="partyAccname" name="partyAccname" class="mini-textbox"
                                       labelField="true" label="对方帐号名称：" labelStyle="text-align:left;width:120px;"
                                       vtype="maxLength:1333"/>
                                <input style="width:100%;" id="partyBankcode" name="partyBankcode" class="mini-textbox"
                                       labelField="true" label="对方开户行行号：" labelStyle="text-align:left;width:120px;"
                                       onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" vtype="maxLength:400"/>
                                <input style="width:100%;" id="partyBankname" name="partyBankname" class="mini-textbox"
                                       labelField="true" label="对方开户行名称：" labelStyle="text-align:left;width:120px;"
                                       vtype="maxLength:400"/>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
        <div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"
                                                                style="width:120px;" id="save_btn"
                                                                onclick="save">保存交易</a></div>
        <div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"
                                                                style="width:120px;" id="close_btn" onclick="cancel">关闭界面</a>
        </div>
    </div>
</div>

<script type="text/javascript">
    mini.parse();

    //获取当前tab
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var row = params.selectedData;
    var url = window.location.search;
    var action = CommonUtil.getParam(url, "action");
    var form = new mini.Form("#field_form");
    var form1 = new mini.Form("#field_form1");
    var form2 = new mini.Form("#field_form2");
    var form3 = new mini.Form("#field_form3");
    var form4 = new mini.Form("#field_form4");
    var form5 = new mini.Form("#field_form5");
    var form6 = new mini.Form("#field_form6");
    var form7 = new mini.Form("#field_form7");

    var tradeData = {};
    tradeData.selectData = row;
    tradeData.operType = action;
    tradeData.serial_no = row.ticketId;
    tradeData.task_id = row.taskId;

    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
            initData();
        });
    });

    //重写方法 防止报错
    function InitTcProductOpics(){}

    //初始化数据
    function initData() {
        if ($.inArray(action, ["edit", "detail", "approve"]) > -1) {
            form.setData(row);
            if ($.inArray(action, ["approve", "detail"]) > -1) {
                mini.get("save_btn").setVisible(false);
                form.setEnabled(false);

                var form11 = new mini.Form("#approve_operate_form");
                form11.setEnabled(true);
            }
            if (mini.get("dealType").getValue() == "IFS_CFETSFX_LEND") {//外币拆借
                $('#field_form2').hide();
                $('#field_form3').hide();
                $('#field_form4').hide();
                $('#field_form5').hide();
                $('#field_form6').hide();
                $('#field_form7').hide();
                form1.setEnabled(false);
                search1();
            } else if (mini.get("dealType").getValue() == "tyck") {//同业存款
                //同业存款
                $('#field_form1').hide();
                $('#field_form2').hide();
                $('#field_form4').hide();
                $('#field_form5').hide();
                $('#field_form6').hide();
                $('#field_form7').hide();
                form3.setEnabled(false);
                search3();
            } else if (mini.get("dealType").getValue() == "tyLoan") {//同业借款
                $('#field_form1').hide();
                $('#field_form2').hide();
                $('#field_form3').hide();
                $('#field_form5').hide();
                $('#field_form6').hide();
                $('#field_form7').hide();
                form4.setEnabled(false);
                search2();
            } else if (mini.get("dealType").getValue() == "creditLoan")  {//同业拆借
                $('#field_form1').hide();
                $('#field_form3').hide();
                $('#field_form4').hide();
                $('#field_form5').hide();
                $('#field_form6').hide();
                $('#field_form7').hide();
                form2.setEnabled(false);
                search2();
            } else if (mini.get("dealType").getValue() == "IFS_CFETSRMB_IBO")  {//同业拆放（对俄）
                $('#field_form1').hide();
                $('#field_form2').hide();
                $('#field_form3').hide();
                $('#field_form4').hide();
                $('#field_form6').hide();
                $('#field_form7').hide();
                form5.setEnabled(false);
                search2();
            } else if (mini.get("dealType").getValue() == "IFS_FX_DEPOSITOUT")  {//存放同业（对俄）
                $('#field_form1').hide();
                $('#field_form2').hide();
                $('#field_form3').hide();
                $('#field_form4').hide();
                $('#field_form5').hide();
                $('#field_form7').hide();
                form6.setEnabled(false);
                search4();
            } else if (mini.get("dealType").getValue() == "IFS_RMB_DEPOSITOUT")  {//存放同业
                $('#field_form1').hide();
                $('#field_form2').hide();
                $('#field_form3').hide();
                $('#field_form4').hide();
                $('#field_form5').hide();
                $('#field_form6').hide();
                form7.setEnabled(false);
                search5();
            }
        } else { //增加
            form.setData(row);
            mini.get("fedealno").setValue(row.ticketId);
            mini.getByName("ticketId", form).setValue("");
            mini.getByName("sponsor", form).setValue("<%=__sessionUser.getUserId()%>");
            mini.getByName("sponInst", form).setValue("<%=__sessionUser.getInstId() %>");
            if (mini.get("dealType").getValue() == "IFS_CFETSFX_LEND") {//外币拆借
                $('#field_form2').hide();
                $('#field_form3').hide();
                $('#field_form4').hide();
                $('#field_form5').hide();
                $('#field_form6').hide();
                $('#field_form7').hide();
                form1.setEnabled(false);
                search1();
            } else if (mini.get("dealType").getValue() == "tyck") {//同业存款
                //同业存款
                $('#field_form1').hide();
                $('#field_form2').hide();
                $('#field_form4').hide();
                $('#field_form5').hide();
                $('#field_form6').hide();
                $('#field_form7').hide();
                form3.setEnabled(false);
                search3();
            } else if (mini.get("dealType").getValue() == "tyLoan") {//同业借款
                $('#field_form1').hide();
                $('#field_form2').hide();
                $('#field_form3').hide();
                $('#field_form5').hide();
                $('#field_form6').hide();
                $('#field_form7').hide();
                form4.setEnabled(false);
                search2();
            } else if (mini.get("dealType").getValue() == "creditLoan")  {//同业拆借
                $('#field_form1').hide();
                $('#field_form3').hide();
                $('#field_form4').hide();
                $('#field_form5').hide();
                $('#field_form6').hide();
                $('#field_form7').hide();
                form2.setEnabled(false);
                search2();
            } else if (mini.get("dealType").getValue() == "IFS_CFETSRMB_IBO")  {//同业拆放（对俄）
                $('#field_form1').hide();
                $('#field_form2').hide();
                $('#field_form3').hide();
                $('#field_form4').hide();
                $('#field_form6').hide();
                $('#field_form7').hide();
                form5.setEnabled(false);
                search2();
            } else if (mini.get("dealType").getValue() == "IFS_FX_DEPOSITOUT")  {//存放同业（对俄）
                $('#field_form1').hide();
                $('#field_form2').hide();
                $('#field_form3').hide();
                $('#field_form4').hide();
                $('#field_form5').hide();
                $('#field_form7').hide();
                form6.setEnabled(false);
                search4();
            } else if (mini.get("dealType").getValue() == "IFS_RMB_DEPOSITOUT")  {//存放同业
                $('#field_form1').hide();
                $('#field_form2').hide();
                $('#field_form3').hide();
                $('#field_form4').hide();
                $('#field_form5').hide();
                $('#field_form6').hide();
                form7.setEnabled(false);
                search5();
            }
        }
    }

    //外币拆借
    function search1() {
        var ticketId = mini.get("fedealno").getValue();
        var data = {};
        data['ticketId'] = ticketId;
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: "/IfsCurrencyController/searchCcyLending",
            data: params,
            callback: function (data) {
                if (data.obj) {
                    form1.setData(data.obj);
                    /* mini.get("counterpartyInstId").setValue(data.obj.counterpartyInstId);
                 queryTextName(data.obj.counterpartyInstId);
                 mini.get("instId").setValue(data.obj.instfullname); */


                    mini.getByName("counterpartyInstId", form1).setValue(data.obj.counterpartyInstId);
                    queryTextName(data.obj.counterpartyInstId, form1);
                    mini.getByName("instId", form1).setValue(data.obj.instfullname);
                    mini.getByName("myUserName", form1).setValue(data.obj.myUserName);
                }
            }
        });
    }

    //信用拆借
    function search2() {
        var ticketId = mini.get("fedealno").getValue();
        //var dealTransType = mini.get("dealTransType").getValue();
        var data = {};
        data['ticketId'] = ticketId;
        //data['dealTransType']=dealTransType;
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: "/IfsCfetsrmbIboController/searchCfetsRmbIboById",
            data: params,
            callback: function (data) {
                if (data.obj) {
                    if (mini.get("dealType").getValue() == "tyLoan") {//同业借款
                        form4.setData(data.obj);
                    } else if (mini.get("dealType").getValue() == "creditLoan") {//信用拆借
                        form2.setData(data.obj);
                    }else if(mini.get("dealType").getValue() == "IFS_CFETSRMB_IBO"){//同业拆放（对俄）
                        form5.setData(data.obj);
                    }
                }
            }
        });
    }

    //同业存款
    function search3() {
        var ticketId = mini.get("fedealno").getValue();
        //var dealTransType = mini.get("dealTransType").getValue();
        var data = {};
        data['ticketId'] = ticketId;
        //data['dealTransType']=dealTransType;
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: "/IfsApprovermbDepositController/searchApprovermbDepositByTicketId",
            data: params,
            callback: function (data) {
                if (data.obj) {
                    form3.setData(data.obj);
                }
            }
        });
    }

    //存放同业（对俄）
    function search4() {
        var ticketId = mini.get("fedealno").getValue();
        var data = {};
        data['ticketId'] = ticketId;
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: "/IfsCCYDepositOutController/searchByTicketId",
            data: params,
            callback: function (data) {
                if (data.obj) {
                    form6.setData(data.obj);
                }
            }
        });
    }

    //存放同业
    function search5() {
        var ticketId = mini.get("fedealno").getValue();
        var data = {};
        data['ticketId'] = ticketId;
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: "/IfsRmbDepositOutController/searchByTicketId",
            data: params,
            callback: function (data) {
                if (data.obj) {
                    form7.setData(data.obj);
                }
            }
        });
    }

    //根据交易对手编号查询全称
    function queryTextName(cno, form) {
        CommonUtil.ajax({
            url: "/IfsOpicsCustController/searchIfsOpicsCust",
            data: {'cno': cno},
            callback: function (data) {
                if (data && data.obj) {
                    mini.getByName("counterpartyInstId", form).setText(data.obj.cliname);
                } else {
                    mini.getByName("counterpartyInstId", form).setText();
                }
            }
        });
    }

    //保存
    function save() {
        //表单验证！！！
        form.validate();
        if (form.isValid() == false) {
            return;
        }
        var saveUrl = null;
        saveUrl = "/IfsRevIrvvController/saveLendParam";
        var data = form.getData(true);
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: saveUrl,
            data: params,
            callback: function (data) {
                if (data.code == 'error.common.0000') {
                    mini.alert("保存成功", '提示信息', function () {
                        top["lendIrvvManage"].query();
                        top["win"].closeMenuTab();

                    });
                } else {
                    mini.alert("保存失败");
                }
            }
        });
    }

    function cancel() {
        top["lendIrvvManage"].query();
        top["win"].closeMenuTab();

    }

    //英文、数字、下划线 的验证
    function onEnglishAndNumberValidation(e) {
        if (e.value == "" || e.value == null) {//值为空，就不做校验
            return;
        }
        if (e.isValid) {
            if (isEnglishAndNumber(e.value) == false) {
                e.errorText = "必须输入英文或数字";
                e.isValid = false;
            }
        }
    }

    /* 是否英文、数字、下划线 */
    function isEnglishAndNumber(v) {
        var re = new RegExp("^[0-9a-zA-Z\_\-]+$");
        if (re.test(v)) return true;
        return false;
    }

    function nodeclick(e) {
        var oldvalue = e.sender.value;
        var psn = mini.get("psn").getValue();
        var param = mini.encode({"psn": psn}); //序列化成JSON
        CommonUtil.ajax({
            url: "/IfsOpicsProdController/searchProd",
            data: param,
            callback: function (data) {
                var obj = e.sender;
                obj.setData(data.obj);
                obj.setValue(oldvalue);

            }
        });
    }

    function adateCheck(e) {
        if (mini.parseDate(row.forDate) > e.value) {
            mini.alert("冲销日期不得早于成交日期", "提示");
            mini.get("adate").setValue('');
        }
    }
</script>
<script type="text/javascript" src="../../Common/Flow/MiniApproveOpCommon.js"></script>
</body>
</html>