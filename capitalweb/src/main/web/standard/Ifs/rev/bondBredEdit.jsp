<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
  <%@ include file="../../global.jsp"%>
    <html>

    <head>
      <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script> 
    <title>存续期管理台 回购类</title>
    </head>
    
    <body style="width:100%;height:100%;background:white">
     
    <div class="mini-splitter" style="width:100%;height:100%;">
        <div size="90%" showCollapseButton="false">
	        <div id="field" class="fieldset-body">
	            <div  id="field_form" class="mini-panel" title="回购类冲销信息" style="width:100%" allowResize="true" collapseOnTitleClick="true">
		        	<div class="leftarea">
		                  <input id="sponsor" name="sponsor" class="mini-hidden" />
		                  <input id="sponInst" name="sponInst" class="mini-hidden" />
		                  <input id="ticketId" name="ticketId" enabled="false" class="mini-textbox" label="审批单流水号：" labelField="true" emptyText="系统自动生成"  vtype="maxLength:35" style="width:100%" labelStyle="text-align:left;width:170px" />
		                  <input id="fedealno" name="fedealno" class="mini-textbox" enabled="false" label="交易前端流水号：" emptyText="请输入交易前端流水号" labelField="true" style="width:100%" labelStyle="text-align:left;width:170px" />
		                  <input id="adate" name="adate" class="mini-datepicker mini-mustFill" label="冲销日期：" required="true"  emptyText="请输入冲销日期" labelField="true"style="width:100%" labelStyle="text-align:left;width:170px" vtype="maxLength:35" onvaluechanged = "adateCheck"/>
		                  <input id="revreason" name="revreason" class="mini-textarea" label="冲销原因：" emptyText="请输入冲销原因" labelField="true"style="width:100%" labelStyle="text-align:left;width:170px" />
		            </div>
		            <div class="rightarea">
		                  <input style="width:100%;" id="dealType" name="dealType" enabled="false" class="mini-combobox" labelField="true"  label="业务类型："  data="CommonUtil.serverData.dictionary.businessType" labelStyle="text-align:left;width:170px" />
		                  <input id="dealNo" name="dealNo" enabled="false" class="mini-textbox" label="OPICS交易号：" emptyText="请输入OPICS交易号" labelField="true"style="width:100%" labelStyle="text-align:left;width:170px" />
		                  <input id="br" name="br" class="mini-combobox mini-mustFill" label="部门："  data="[{id:'01',text:'01'}]" emptyText="请选择部门" labelField="true" style="width:100%" labelStyle="text-align:left;width:170px" vtype="maxLength:50" required="true" />
		            </div>
	            	<%@ include file="../../Common/Flow/MiniApproveOpCommon.jsp"%>
	            </div>
		      	<div  id="field_form1" class="mini-fit area"  style="background:white" >
					<div class="mini-panel" title="质押式回购详情" style="width:100%;"  allowResize="true" collapseOnTitleClick="true">
						<div class="leftarea">
							<input style="width:100%;" id="contractId" name="contractId" class="mini-textbox mini-mustFill" labelField="true" required="true"   requiredErrorText="该输入项为必输项"  label="成交单编号：" labelStyle="text-align:left;width:130px;" vtype="maxLength:20"/>
						</div>
					</div>
					<div class="mini-panel" title="成交双方信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
						<div class="leftarea">
							<fieldset>
								<legend>本方信息</legend>
								<input id="myDir" name="myDir" class="mini-combobox mini-mustFill"  labelField="true"  onvaluechanged="dirVerify" label="本方方向："  style="width:100%;"  required="true"   labelStyle="text-align:left;width:130px;" data = "CommonUtil.serverData.dictionary.repoing" />
								<input id="positiveInst"  name="positiveInst" class="mini-textbox mini-mustFill"  labelField="true"  label="机构："  style="width:100%;" enabled="false" required="true"   labelStyle="text-align:left;width:130px;"/>
								<input id="positiveTrader" name="positiveTrader" class="mini-textbox mini-mustFill" labelField="true"  label="交易员："  required="true"   vtype="maxLength:5" enabled="false" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
							</fieldset>
						</div>
						<div class="rightarea">
							<fieldset>
								<legend>对手方信息</legend>
								<input id="oppoDir" name="oppoDir" class="mini-combobox mini-mustFill"  enabled="false"  labelField="true" label="本方方向："  required="true"   style="width:100%;"  labelStyle="text-align:left;width:130px;" data = "CommonUtil.serverData.dictionary.repoing" />
								<input id="reverseInst" name="reverseInst" class="mini-textbox mini-mustFill" onbuttonclick="onButtonEdit" labelField="true" allowInput="false" required="true"   label="机构：" vtype="maxLength:50"  style="width:100%;"  labelStyle="text-align:left;width:130px;" />
								<input id="reverseTrader" name="reverseTrader" class="mini-textbox mini-mustFill" labelField="true"  label="交易员：" vtype="maxLength:10" labelStyle="text-align:left;width:130px;" style="width:100%;" required="true"   />
							</fieldset>
						</div>
					</div>
					<div class="mini-panel" title="交易主体信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
						<div class="centerarea" style="height:150px;">
							<div class="mini-fit" >
								<div id="datagrid1" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id"  allowAlternating="true"
										fitColumns="false" allowResize="true" sortMode="client" allowAlternating="true" showpager="false">
									<div property="columns">
										<div type="indexcolumn" width="50px" headerAlign="center">序号</div>
									    <div field="bondCode" width="150" align="center" headerAlign="center"  >债券代码</div>
									    <div field="bondName" width="230px" align="center"  headerAlign="center" >债券名称</div> 
									    <div field="totalFaceValue" width="200" align="right" headerAlign="center"  allowSort="true" numberFormat="n4">券面总额(万元)</div>
									    <div field="underlyingStipType" width="120" align="right" headerAlign="center"  allowSort="true" numberFormat="n2">折算比例(%)</div>
									    <div field="prodb" width="100" align="center" headerAlign="center"  allowSort="true" >产品代码</div>
									    <div field="prodtypeb" width="80" align="center" headerAlign="center"  allowSort="true" >产品类型</div>
									    <div field="portb" width="100" align="center" headerAlign="center"  allowSort="true" >投资组合</div>
										<div field="costb" width="100" align="center" headerAlign="center"  allowSort="true">成本中心</div>
										<div field="invtype" width="100" align="center" headerAlign="center"  allowSort="true"  renderer="CommonUtil.dictRenderer" data-options="{dict:'invtype'}">投资类型</div>
									</div>
								</div>
							</div> 
						</div>
						<div class="leftarea">
								<input style="width:100%;" id="forDate" name="forDate" class="mini-datepicker mini-mustFill" labelField="true" required="true"  label="成交日期：" labelStyle="text-align:left;width:130px;" />
								<input style="width:100%" id="underlyingQty" name="underlyingQty" class="mini-spinner mini-mustFill"   changeOnMousewheel='false' labelField="true"  required="true"   label="券面总额合计(万元)："  maxValue="9999999999999999.9999" format="n4" required="true"   labelStyle="text-align:left;width:130px;"  onvalidation="zeroValidation" />
								<input style="width:100%" id="tradingProduct" name="tradingProduct" class="mini-combobox" allowInput="false" labelField="true"  label="交易品种：" labelStyle="text-align:left;width:130px;"  data = "CommonUtil.serverData.dictionary.dealVariety" />
								<input style="width:100%" id="firstSettlementMethod" name="firstSettlementMethod" class="mini-combobox mini-mustFill" labelField="true"  required="true"   label="首次结算方式：" vtype="maxLength:50" data = "CommonUtil.serverData.dictionary.SettlementMethod"   labelStyle="text-align:left;width:130px;"   />
								<input style="width:100%" id="firstSettlementDate" name="firstSettlementDate" class="mini-datepicker mini-mustFill" labelField="true"  required="true"   label="首次结算日：" onvaluechanged="associaDataCalculation" ondrawdate="onDrawDateFirst" labelStyle="text-align:left;width:130px;"   />
								<input style="width:100%" id="tenor" name="tenor" class="mini-spinner mini-mustFill"   changeOnMousewheel='false' labelField="true"  required="true"   label="回购期限(天)：" maxValue="99999"  labelStyle="text-align:left;width:130px;"   />
								<input style="width:100%" id="accuredInterest" name="accuredInterest" class="mini-spinner mini-mustFill"   changeOnMousewheel='false' labelField="true"  required="true"   label="应计利息(元)：" maxValue="9999999999999999.9999" format="n4"   labelStyle="text-align:left;width:130px;"   />
								<input id="basis" name="basis" class="mini-combobox  mini-mustFill" required="true"  labelField="true"  label="计息基准：" style="width:100%;" data = "CommonUtil.serverData.dictionary.Basis" vtype="maxLength:15" onvaluechanged="linkIntCalculat" labelStyle="text-align:left;width:130px;"  >
						</div>
						<div class="rightarea">
								<input id="nettingStatus" name="nettingStatus" class="mini-combobox mini-mustFill" required="true"  labelField="true"   label="净额清算状态：" labelStyle="text-align:left;width:130px;" style="width:100%;" data="CommonUtil.serverData.dictionary.YesNo" showNullItem="true"/>
								<input style="width:100%" id="tradeAmount" name="tradeAmount" class="mini-spinner mini-mustFill"   changeOnMousewheel='false' labelField="true"  required="true"   label="交易金额(元)：" maxValue="9999999999999999.9999" format="n4"  onvaluechanged="associaDataCalculation" labelStyle="text-align:left;width:130px;" onvalidation="zeroValidation"  />
								<input style="width:100%" id="repoRate" name="repoRate" class="mini-spinner mini-mustFill"   changeOnMousewheel='false' labelField="true"  required="true"   label="回购利率(%)：" minValue="0" maxValue="9999999999999999.99999999" format="n8" onvaluechanged="associaDataCalculation" changeOnMousewheel="false" required="true"  labelStyle="text-align:left;width:130px;" />
								<input style="width:100%" id="secondSettlementMethod" name="secondSettlementMethod" class="mini-combobox mini-mustFill" labelField="true"  required="true"   label="到期结算方式：" vtype="maxLength:50" data = "CommonUtil.serverData.dictionary.SettlementMethod"  labelStyle="text-align:left;width:130px;"   />
								<input style="width:100%" id="secondSettlementDate" name="secondSettlementDate" class="mini-datepicker mini-mustFill" labelField="true"  required="true"   onvaluechanged="associaDataCalculation" label="到期结算日：" ondrawdate="onDrawDateSecond" labelStyle="text-align:left;width:130px;"   />
								<input style="width:100%" id="occupancyDays" name="occupancyDays" class="mini-spinner mini-mustFill"   changeOnMousewheel='false' labelField="true"  required="true"   label="实际占款天数(天)：" maxValue="99999"  onvaluechanged="linkageCalculat" labelStyle="text-align:left;width:130px;"   />
								<input style="width:100%" id="secondSettlementAmount" name="secondSettlementAmount" class="mini-spinner mini-mustFill"   changeOnMousewheel='false' required="true"   labelField="true" maxValue="9999999999999999.9999" format="n4"   label="到期结算金额(元)："  labelStyle="text-align:left;width:130px;"   />
								<input id="tradingModel" name="tradingModel" class="mini-combobox  mini-mustFill" required="true"  labelField="true"   label="交易模式："  labelStyle="text-align:left;width:130px;" style="width:100%;"  data="CommonUtil.serverData.dictionary.TradeModel"/>
						</div>
					</div>
					<%@ include file="../../Common/opicsRepoDetail.jsp"%>
					<div class="mini-panel" title="清算信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
						<div class="leftarea">
							<fieldset>
								<legend>本方账户</legend>
								<input id="positiveAccname" name="positiveAccname" class="mini-textbox" labelField="true"  label="资金账户户名："  vtype="maxLength:50" style="width:100%;"  labelStyle="text-align:left;width:130px;" />
								<input id="positiveOpbank" name="positiveOpbank" class="mini-textbox" labelField="true"  label="资金开户行："  vtype="maxLength:50" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
								<input id="positiveAccnum" name="positiveAccnum" class="mini-textbox" labelField="true"  label="资金账号："  vtype="maxLength:50" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
								<input id="positivePsnum" name="positivePsnum" class="mini-textbox" labelField="true"  label="支付系统行号："  vtype="maxLength:30" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
								<input id="positiveCaname" name="positiveCaname" class="mini-textbox" labelField="true"  label="托管账户户名："  vtype="maxLength:50" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
								<input id="positiveCustname" name="positiveCustname" class="mini-textbox mini-mustFill" onbuttonclick="onSaccQuery" required="true"  labelField="true" allowInput="false"  label="托管机构："  vtype="maxLength:50" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
								<input id="positiveCanum" name="positiveCanum" class="mini-textbox" labelField="true"  label="托管帐号："  vtype="maxLength:30" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
							</fieldset>
						</div>
						<div class="rightarea">
							<fieldset>
								<legend>对手方账户</legend>
								<input id="reverseAccname" name="reverseAccname" class="mini-textbox" labelField="true"  label="资金账户户名："  vtype="maxLength:50" style="width:100%;"  labelStyle="text-align:left;width:130px;" />
								<input id="reverseOpbank" name="reverseOpbank" class="mini-textbox" labelField="true"  label="资金开户行："  vtype="maxLength:50" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
								<input id="reverseAccnum" name="reverseAccnum" class="mini-textbox" labelField="true"  label="资金账号："  vtype="maxLength:50" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
								<input id="reversePsnum" name="reversePsnum" class="mini-textbox" labelField="true"  label="支付系统行号："  vtype="maxLength:30" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
								<input id="reverseCaname" name="reverseCaname" class="mini-textbox" labelField="true"  label="托管账户户名："  vtype="maxLength:50" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
								<input id="reverseCustname" name="reverseCustname" class="mini-textbox mini-mustFill" onbuttonclick="onSaccQuery" required="true"  labelField="true" allowInput="false"  label="托管机构："  vtype="maxLength:50" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
								<input id="reverseCanum" name="reverseCanum" class="mini-textbox" labelField="true"  label="托管帐号：" vtype="maxLength:30" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
							</fieldset>
						</div>
					</div>
				</div>
				<div  id="field_form2" class="mini-fit area"  style="background:white">
					
					<div class="mini-panel" title="买断式回购详情" style="width:100%;"  allowResize="true" collapseOnTitleClick="true">
						<div class="leftarea">
							<input style="width:100%;" id="contractId" name="contractId" class="mini-textbox mini-mustFill" labelField="true" required="true"  requiredErrorText="该输入项为必输项"  label="成交单编号：" labelStyle="text-align:left;width:130px;"  vtype="maxLength:20"/>
						</div>
					</div>
					<div class="mini-panel" title="成交双方信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
						<div class="leftarea">
								<fieldset>
									<legend>本方信息</legend>
									<input id="myDir" name="myDir" class="mini-combobox"  labelField="true"  label="本方方向：" required="true"   onvaluechanged="dirVerify" style="width:100%;"  labelStyle="text-align:left;width:130px;" data = "CommonUtil.serverData.dictionary.repoing" />
									<input id="positiveInst" name="positiveInst" class="mini-textbox"  labelField="true"  label="机构：" required="true"   style="width:100%;" enabled="false" labelStyle="text-align:left;width:130px;"/>
									<input id="positiveTrader" name="positiveTrader" class="mini-textbox" labelField="true"  label="交易员："  required="true"  vtype="maxLength:5" enabled="false" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
								</fieldset>
							</div>			
							<div class="leftarea">
								<fieldset>
									<legend>对手方信息</legend>
									<input id="oppoDir" name="oppoDir" class="mini-combobox"  labelField="true"  label="对方方向："  required="true"  enabled="false" style="width:100%;"  labelStyle="text-align:left;width:130px;" data = "CommonUtil.serverData.dictionary.repoing" />
									<input id="reverseInst" name="reverseInst"  class="mini-textbox"  labelField="true"  label="机构：" required="true"  style="width:100%;"  labelStyle="text-align:left;width:130px;" />
									<input id="reverseTrader" name="reverseTrader" class="mini-textbox" labelField="true"  label="交易员："  vtype="maxLength:5" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
								</fieldset>
							</div>	
					</div>		
					
					<div class="mini-panel" title="交易主体信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
						<div class="leftarea">
							<input style="width:100%;" id="forDate" name="forDate" class="mini-datepicker mini-mustFill" labelField="true" required="true"  label="成交日期：" labelStyle="text-align:left;width:130px;" />
							<input style="width:100%" id="bondCode" name="bondCode" class="mini-textbox mini-mustFill" onbuttonclick="onBondQuery" labelField="true"  label="债券代码：" required="true"     labelStyle="text-align:left;width:130px;" />
							<input  style="width:100%;" id="bondProduct" name="bondProduct" onbuttonclick="onBondPrdEdit" allowInput="false" class="mini-textbox mini-mustFill" labelField="true"   label="券的产品代码：" required="true"  maxLength="6"  labelStyle="text-align:left;width:130px;" >
							<input id="bondPort" name="bondPort" class="mini-textbox mini-mustFill" onbuttonclick="onBondPortEdit"  style="width:100%;"  labelField="true"  label="券的投资组合：" required="true"  allowInput="false" vtype="maxLength:4" labelStyle="text-align:left;width:130px;" >
							<input id="invtype" name="invtype" class="mini-combobox mini-mustFill"  labelField="true"   label="券的投资类型："  style="width:100%;"  labelStyle="text-align:left;width:130px;" data = "CommonUtil.serverData.dictionary.invtype" />
							<input style="width:100%" id="totalFaceValue" name="totalFaceValue" class="mini-spinner mini-mustFill" labelField="true"   label="券面总额(万元)：" required="true"  onvaluechanged="onFirstPriceChanged"  minValue="0" maxValue="9999999999999999.9999" format="n4" changeOnMousewheel="false" labelStyle="text-align:left;width:130px;" onvalidation="zeroValidation"  />
							<input style="width:100%" id="firstSettlementDate" name="firstSettlementDate" class="mini-datepicker mini-mustFill" labelField="true"   label="首次结算日："  required="true"  ondrawdate="onDrawDateStart" onvaluechanged="onStartDateChanged" labelStyle="text-align:left;width:130px;"   />
							<input style="width:100%" id="tenor" name="tenor" class="mini-spinner mini-mustFill" labelField="true"   label="回购期限(天)：" required="true"  minValue="0" maxValue="9999999999"  changeOnMousewheel="false"  labelStyle="text-align:left;width:130px;"   />
							<input style="width:100%" id="firstYield" name="firstYield" class="mini-spinner mini-mustFill" labelField="true"   label="首次收益率(%)：" required="true"   onvaluechanged="onFirstYieldChanged" minValue="0" maxValue="9999999999999999.99999999" format="n8" changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"   />
							<input style="width:100%" id="firstCleanPrice" name="firstCleanPrice" class="mini-spinner mini-mustFill" labelField="true"  label="首次净价(元)：" required="true"  onvaluechanged="onFirstPriceChanged" minValue="0" maxValue="9999999999999999.9999" format="n4" changeOnMousewheel="false"   labelStyle="text-align:left;width:130px;"   />
							<input style="width:100%" id="firstAccuredInterest" name="firstAccuredInterest" class="mini-spinner mini-mustFill" labelField="true"   label="首次应计利息(元)：" required="true"  onvaluechanged="onFirstPriceChanged"  minValue="0" maxValue="9999999999999999.9999" format="n5" changeOnMousewheel="false"  labelStyle="text-align:left;width:130px;"   />
							<input style="width:100%" id="firstDirtyPrice" name="firstDirtyPrice" class="mini-spinner mini-mustFill" labelField="true"  label="首次全价(元)：" required="true"   onvaluechanged="onFirstPriceChanged" minValue="0" maxValue="9999999999999999.9999" format="n4" changeOnMousewheel="false"    labelStyle="text-align:left;width:130px;"   />
							<input style="width:100%" id="firstSettlementMethod" name="firstSettlementMethod" class="mini-combobox mini-mustFill" labelField="true"   label="首次结算方式：" required="true"  data = "CommonUtil.serverData.dictionary.SettlementMethod" labelStyle="text-align:left;width:130px;"   />
							<input style="width:100%" id="firstSettlementAmount" name="firstSettlementAmount" class="mini-spinner mini-mustFill"   labelField="true"  label="首次结算金额(元)："  required="true"  minValue="0" maxValue="9999999999999999.9999" format="n4" changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"  onvalidation="zeroValidation" onvaluechanged="sumSecondSettlementAmount"/>
							<input id="basis" name="basis" class="mini-combobox mini-mustFill" required="true"  labelField="true"  label="计息基准：" style="width:100%;" data = "CommonUtil.serverData.dictionary.Basis" vtype="maxLength:15" labelStyle="text-align:left;width:130px;"  onvaluechanged="onFirstPriceChanged">
						</div>
						<div class="rightarea">
							<input id="nettingStatus" name="nettingStatus" class="mini-combobox mini-mustFill" required="true"  labelField="true"   label="净额清算状态：" labelStyle="text-align:left;width:130px;" style="width:100%;" data="CommonUtil.serverData.dictionary.YesNo" showNullItem="true"/>
							<input style="width:100%" id="bondName" name="bondName" class="mini-textbox mini-mustFill" labelField="true"  label="债券名称：" required="true"    labelStyle="text-align:left;width:130px;"   enabled="false"/>
							<input style="width:100%;" id="bondProdType" name="bondProdType" onbuttonclick="onBondTypeEdit" allowInput="false" class="mini-textbox mini-mustFill" labelField="true"  label="券的产品类型：" required="true"  vtype="maxLength:2" labelStyle="text-align:left;width:130px;" >
							<input style="width:100%;" id="bondCost" name="bondCost" onbuttonclick="onBondCostEdit" allowInput="false" class="mini-textbox mini-mustFill" labelField="true"  label="券的成本中心："   required="true"  vtype="maxLength:15" labelStyle="text-align:left;width:130px;"  >
							<input style="width:100%" id="tradingProduct" name="tradingProduct" class="mini-combobox" allowInput="false" labelField="true"  label="交易品种：" labelStyle="text-align:left;width:130px;" data = "CommonUtil.serverData.dictionary.dealVarietyBuy"/>
							<input style="width:100%" id="repoRate" name="repoRate" class="mini-spinner mini-mustFill" labelField="true"  label="回购利率(%)：" required="true"  minValue="0" maxValue="9999999999999999.99999999" format="n8" changeOnMousewheel="false"    labelStyle="text-align:left;width:130px;" onvaluechanged="onFirstPriceChanged" />
							<input style="width:100%;display:none" id="startDate" name="startDate" class="mini-textbox mini-mustFill" labelField="true"  label="起息日：" required="true"  vtype="maxLength:10"  labelStyle="text-align:left;width:130px;"   enabled="false"/>
							<input style="width:100%;display:none" id="matdt" name="matdt" class="mini-textbox mini-mustFill" labelField="true"  label="到期日：" required="true"  vtype="maxLength:10"  labelStyle="text-align:left;width:130px;"   enabled="false"/>
							<input style="width:100%" id="secondSettlementDate" name="secondSettlementDate" class="mini-datepicker mini-mustFill"   labelField="true"  label="到期结算日：" required="true"   ondrawdate="onDrawDateEnd" onvaluechanged="onEndDateChanged" labelStyle="text-align:left;width:130px;"   />
							<input style="width:100%" id="occupancyDays" name="occupancyDays" class="mini-spinner mini-mustFill" labelField="true"   label="实际占款天数(天)：" required="true"  minValue="0" maxValue="99999"  changeOnMousewheel="false"  labelStyle="text-align:left;width:130px;"   onvaluechanged="onFirstPriceChanged"/>
							<input style="width:100%" id="secondYield" name="secondYield" class="mini-spinner mini-mustFill" labelField="true"  label="到期收益率(%)：" required="true"  minValue="0" maxValue="9999999999999999.99999999" format="n8" changeOnMousewheel="false"    labelStyle="text-align:left;width:130px;"   />
							<input style="width:100%" id="secondCleanPrice" name="secondCleanPrice" class="mini-spinner mini-mustFill" labelField="true"   label="到期净价(元)：" required="true"  onvaluechanged="onSecondInterestChanged"  minValue="0" maxValue="9999999999999999.9999" format="n4" changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"   />
							<input style="width:100%" id="secondAccuredInterest" name="secondAccuredInterest" class="mini-spinner mini-mustFill"  labelField="true"  label="到期应计利息(元)：" required="true"  onvaluechanged="onSecondInterestChanged" minValue="0" maxValue="9999999999999999.9999" format="n5" changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"   />
							<input style="width:100%" id="secondDirtyPrice" name="secondDirtyPrice" class="mini-spinner mini-mustFill" labelField="true"   label="到期全价(元)：" required="true"  onvaluechanged="onFirstPriceChanged" minValue="0" maxValue="9999999999999999.9999" format="n4" changeOnMousewheel="false"  labelStyle="text-align:left;width:130px;"   />
							<input style="width:100%" id="secondSettlementMethod" name="secondSettlementMethod" class="mini-combobox mini-mustFill"  labelField="true"  label="到期结算方式："  required="true"  data = "CommonUtil.serverData.dictionary.SettlementMethod" labelStyle="text-align:left;width:130px;"   />
							<input style="width:100%" id="secondSettlementAmount" name="secondSettlementAmount" class="mini-spinner mini-mustFill" labelField="true"   label="到期结算金额(元)：" required="true"  minValue="0" maxValue="9999999999999999.9999" format="n4" changeOnMousewheel="false"  labelStyle="text-align:left;width:130px;"  onvalidation="zeroValidation" />
							<input id="tradingModel" name="tradingModel" class="mini-combobox mini-mustFill" required="true"  labelField="true"   label="交易模式："  labelStyle="text-align:left;width:130px;" style="width:100%;"  data="CommonUtil.serverData.dictionary.TradeModel"/>
						</div>
					</div>
					<%@ include file="../../Common/opicsRepoDetail.jsp"%>
					<%@ include file="../../Common/RiskCenterDetail.jsp"%>	
					
					<div class="mini-panel" title="清算信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
						<div class="leftarea">
							<fieldset>
								<legend>本方账户</legend>
								<input id="positiveAccname" name="positiveAccname" class="mini-textbox" labelField="true"  label="资金账户户名：" vtype="maxLength:25"  style="width:100%;"  labelStyle="text-align:left;width:130px;" />
								<input id="positiveOpenBank" name="positiveOpenBank" class="mini-textbox" labelField="true"  label="资金开户行：" vtype="maxLength:25"  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
								<input id="positiveAccnum" name="positiveAccnum" class="mini-textbox" labelField="true"  label="资金账号：" onvalidation="onEnglishAndNumberValidation" vtype="maxLength:50" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
								<input id="positivePsnum" name="positivePsnum" class="mini-textbox" labelField="true"  label="支付系统行号："  onvalidation="onEnglishAndNumberValidation" vtype="maxLength:20" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
								<input id="positiveCaname" name="positiveCaname" class="mini-textbox" labelField="true"  label="托管账户户名：" vtype="maxLength:25"  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
								<input id="positiveCustname" name="positiveCustname" class="mini-textbox mini-mustFill" onbuttonclick="onSaccQuery" required="true"  labelField="true" allowInput="false"  label="托管机构：" vtype="maxLength:25"  labelStyle="text-align:left;width:130px;" style="width:100%;"/>
								<input id="positiveCanum" name="positiveCanum" class="mini-textbox" labelField="true"  label="托管帐号："  onvalidation="onEnglishAndNumberValidation" vtype="maxLength:20"labelStyle="text-align:left;width:130px;" style="width:100%;"   />
							</fieldset>
						</div>
						<div class="rightarea">
							<fieldset>
								<legend>对手方账户</legend>
								<input id="reverseAccname" name="reverseAccname" class="mini-textbox" labelField="true"  label="资金账户户名：" vtype="maxLength:25"  style="width:100%;"  labelStyle="text-align:left;width:130px;" />
								<input id="reverseOpenBank" name="reverseOpenBank" class="mini-textbox" labelField="true"  label="资金开户行：" vtype="maxLength:25"  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
								<input id="reverseAccnum" name="reverseAccnum" class="mini-textbox" labelField="true"  label="资金账号：" onvalidation="onEnglishAndNumberValidation" vtype="maxLength:50" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
								<input id="reversePsnum" name="reversePsnum" class="mini-textbox" labelField="true"  label="支付系统行号："  onvalidation="onEnglishAndNumberValidation" vtype="maxLength:20" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
								<input id="reverseCaname" name="reverseCaname" class="mini-textbox" labelField="true"  label="托管账户户名："  vtype="maxLength:25"  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
								<input id="reverseCustname" name="reverseCustname" class="mini-textbox mini-mustFill" onbuttonclick="onSaccQuery" required="true"  labelField="true" allowInput="false"  label="托管机构："  vtype="maxLength:25" labelStyle="text-align:left;width:130px;" style="width:100%;"/>
								<input id="reverseCanum" name="reverseCanum" class="mini-textbox" labelField="true"  label="托管帐号："  onvalidation="onEnglishAndNumberValidation" vtype="maxLength:20" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
							</fieldset>
						</div>	
					</div>					
				</div>
			</div>
        </div>
        <div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
	          <div style="margin-bottom:10px; text-align: center;">
	              <a class="mini-button" style="display: none"  style="width:120px;" id="save_btn"  onclick="save">保存</a>
	          </div>
	          <div style="margin-bottom:10px; text-align: center;">
	              <a class="mini-button" style="display: none"  style="width:120px;" id="cancel_btn"  onclick="cancel">取消</a>
	          </div>
        </div>
    </div>
    
      <script type="text/javascript">
        mini.parse();

        //获取当前tab
        var currTab = top["win"].tabs.getActiveTab();
        var params = currTab.params;
        var row = params.selectedData;
        var url = window.location.search;
        var action = CommonUtil.getParam(url, "action");
        var form = new mini.Form("#field_form");
        var form1 = new mini.Form("#field_form1");
        var form2 = new mini.Form("#field_form2");
        form1.setEnabled(false);
	    form2.setEnabled(false);
        var tradeData={};
    	
    	tradeData.selectData=row;
    	tradeData.operType=action;
    	tradeData.serial_no=row.ticketId;
    	tradeData.task_id=row.taskId;

        $(document).ready(function () {
			//控制按钮显示
			$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
				initData();
			});
        });

        //初始化数据
        function initData() {
          if ($.inArray(action, ["edit", "detail","approve"]) > -1) { //修改、详情
        	  form.setData(row);
        	  if($.inArray(action,["approve","detail"])>-1){
	      			mini.get("save_btn").setVisible(false);
	      			form.setEnabled(false);
	      			var form11=new mini.Form("#approve_operate_form");
	      			form11.setEnabled(true);
    			}
        	  if(mini.get("dealType").getValue() != "442"){
				   $('#field_form2').hide();
				   form1.setEnabled(false);
				   search1();
			   }else{
				   $('#field_form1').hide();
				   form2.setEnabled(false);
				   search2();
			   }
          } else { //增加 
            form.setData(row);
            mini.get("fedealno").setValue(row.ticketId);
            mini.get("ticketId").setValue("");
            mini.get("sponsor").setValue("<%=__sessionUser.getUserId()%>");
			mini.get("sponInst").setValue("<%=__sessionUser.getInstId() %>");
			if(mini.get("dealType").getValue() != "442"){
				   $('#field_form2').hide();
				   form1.setEnabled(false);
				   search1();
			   }else{
				   $('#field_form1').hide();
				   form2.setEnabled(false);
				   search2();
			   }
          }
        }
        
      //质押式回购
        function search1(){
     	    var ticketId =  mini.get("fedealno").getValue();
            var data = {};
            data['ticketId']=ticketId;
     	   var params = mini.encode(data);
     	   CommonUtil.ajax({
                url : "/IfsCfetsrmbCrController/searchCfetsrmbCrByTicketId",
                data : params,
                callback : function(data) {
                	if(data.obj){
                   		form1.setData(data.obj);
                	}
                }
            });
     	  initGrid(ticketId);
        }
      //买断式回购
        function search2(){
     	   var ticketId =  mini.get("fedealno").getValue();
     	   //var dealTransType = mini.get("dealTransType").getValue();
            var data = {};
            data['ticketId']=ticketId;
            //data['dealTransType']=dealTransType;
     	   var params = mini.encode(data);
     	   CommonUtil.ajax({
                url : "/IfsCfetsrmbOrController/searchCfetsrmbOrByTicketId",
                data : params,
                callback : function(data) {
                	if(data.obj){
                		form2.setData(data.obj);
                	}
                }
            });
        }
        
      
        
         function save() {
             //表单验证！！！
             form.validate();
             if (form.isValid() == false) {
               return;
             }
             var saveUrl = null;
               saveUrl = "/IfsRevBredController/saveRevBredParam";
               var data = form.getData(true);
               var params = mini.encode(data);
               CommonUtil.ajax({
                 url: saveUrl,
                 data: params,
                 callback: function (data) {
                   if (data.code == 'error.common.0000') {
                     mini.alert("保存成功", '提示信息', function () {
                       top["bondBredManage"].query();
                       top["win"].closeMenuTab();
                     });
                   } else {
                     mini.alert("保存失败");
                   }
                 }
               });
             }
        

        function cancel() {
          top["win"].closeMenuTab();
        }

        //英文、数字、下划线 的验证
        function onEnglishAndNumberValidation(e) {
          if (e.value == "" || e.value == null) { //值为空，就不做校验
            return;
          }
          if (e.isValid) {
            if (isEnglishAndNumber(e.value) == false) {
              e.errorText = "必须输入英文或数字";
              e.isValid = false;
            }
          }
        }

        /* 是否英文、数字、下划线 */
        function isEnglishAndNumber(v) {
          var re = new RegExp("^[0-9a-zA-Z\_\-]+$");
          if (re.test(v)) return true;
          return false;
        }
        
        
        var grid1=mini.get("datagrid1");
      //初始化债券信息
 	   function initGrid(ticketId){
 			var url="/IfsCfetsrmbCrController/searchBondCrDetails";
 			var data={};
 			data['ticketId']=ticketId;
 			var params=mini.encode(data);
 			CommonUtil.ajax({
 				url:url,
 				data:params,
 				callback : function(data) {
 					grid1.setData(data.obj.rows);
 				}
 			});
 		}

		function adateCheck(e){
			if(mini.parseDate(row.forDate) > e.value){
				mini.alert("冲销日期不得早于成交日期","提示");
				mini.get("adate").setValue('');
			}
		}
      </script>
      <script type="text/javascript" src="../../Common/Flow/MiniApproveOpCommon.js"></script>
    </body>

    </html>