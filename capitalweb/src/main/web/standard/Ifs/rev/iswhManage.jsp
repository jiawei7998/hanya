﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
<title>互换类</title>
</head>
<body style="width: 100%; height: 100%; background: white">
	<fieldset class="mini-fieldset">
		<legend>互换类信息查询</legend>
		<div id="search_form" style="width: 100%;">
			<input id="ticketId" name="ticketId" class="mini-textbox"  width="320px"  labelField="true" label="审批单流水号：" emptyText="请填写审批单流水号" labelStyle="text-align:right;width:120px"/>
			<input id="dealno" name="dealno" class="mini-textbox"  width="320px"  labelField="true" label="OPICS交易流水号：" emptyText="请填写OPICS交易流水号" labelStyle="text-align:right;width:120px"/>
			<input id="dealType" name="dealType" class="mini-combobox" width="320px"
				   data="CommonUtil.serverData.dictionary.operationType" emptyText="请选择业务类型" labelField="true"  label="业务类型：" labelStyle="text-align:right;"/>
			<span style="float: right; margin-right: 150px">
				<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
				<a class="mini-button" style="display: none"  id="clear_btn" onclick="clear()">清空</a>
			</span>
		</div>
	</fieldset>
	 <%@ include file="./revBase.jsp"%>  
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
	<div class="mini-fit" style="width:100%;height:100%;">
		<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" sortMode="client" allowAlternating="true"
          idField="userId" onrowdblclick="onRowDblClick" border="true" allowResize="true">
          	<div property="columns">
				<div type="indexcolumn" headerAlign="center" width="40">序号</div> 
				<div field="ticketId"  allowSort="false" headerAlign="center" align="center">审批单流水号</div>
				<div field="dealType"  allowSort="false"  headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'operationType'}">业务类型</div>
				<div field="adate"  allowSort="false"  headerAlign="center" align="center">冲销日期</div>
				<div field="br"  allowSort="false" headerAlign="center" align="center">部门</div>
				<div field="fedealno"  allowSort="false" width="180px"  headerAlign="center" align="center">交易前端流水号</div>
				<div field="dealno"  allowSort="false"  headerAlign="center" align="center" width="150px">OPICS交易流水号</div>
				<div field="revreason" allowSort="false"  headerAlign="center" align="center">冲销原因</div>
				<div field="approveStatus" width="90" align="center" headerAlign="center"  renderer="CommonUtil.dictRenderer" data-options="{dict:'ApproveStatus'}">审批状态</div>
				<div field="sponsor"  allowSort="false" headerAlign="center" align="center">审批发起人</div>
				<div field="instName"  allowSort="false" width="180px" headerAlign="center" align="center" >审批发起机构</div>
				<div field="sponDate"  allowSort="false" width="180px" headerAlign="center" align="center" >审批发起时间</div>
			</div>
		</div>
	</div>
<script type="text/javascript"> 
      mini.parse();

      var url = window.location.search;
  	  var prdNo = CommonUtil.getParam(url, "prdNo");
  	  var prdName = CommonUtil.getParam(url, "prdName");
	  var currTab = top["win"].tabs.getActiveTab();
	  var params = currTab.params;
      var grid = mini.get("datagrid");
      top["iswhManage"] = window;
      
      grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize, pageIndex);
      });
	  
      
      $(document).ready(function(){
		  //控制按钮显示
		  $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
			  initButton();
			  query();
		  });
      });
  
      /* 按钮 查询事件 */
	    function query() {
	        search(grid.pageSize, 0);
	    }
		
	    /* 查询 */
		/* function search(pageSize,pageIndex){
			var form = new mini.Form("#search_form");
			form.validate();
			if(form.isValid()==false){
				mini.alert("信息填写有误，请重新填写","系统提示");
				return;
			}
			var data=form.getData(true);
			data['pageNumber']=pageIndex+1;
			data['pageSize']=pageSize;
			data['branchId']=branchId;
			
			
			var params = mini.encode(data);
			
			CommonUtil.ajax({
				url:"/IfsRevlfxdController/searchPageForexParam",
				data:params,
				callback : function(data) {
					grid.setTotalCount(data.obj.total);
					grid.setPageIndex(pageIndex);
			        grid.setPageSize(pageSize);
					grid.setData(data.obj.rows);
				}
			});
		} */
		
		
		
		/* 查询 */
		function search(pageSize,pageIndex){
			var form = new mini.Form("#search_form");
			form.validate();
			if(form.isValid()==false){
				mini.alert("信息填写有误，请重新填写","系统提示");
				return;
			}

			var data=form.getData(true);
			data['pageNumber']=pageIndex+1;
			data['pageSize']=pageSize;
			data['branchId']=branchId;
			var url=null;
 
			var approveType = mini.get("approveType").getValue();
			if(approveType == "mine"){//我发起的
				url = "/IfsRevIswhController/searchRevIswhMine";
			}else if(approveType == "approve"){//待审批
				url = "/IfsRevIswhController/searchRevIswhUnfinished";
			}else{//已审批
				url = "/IfsRevIswhController/searchRevIswhFinished";
			} 
			
			var params = mini.encode(data);
			CommonUtil.ajax({
				url:url,
				data:params,
				callback : function(data) {
					grid.setTotalCount(data.obj.total);
					grid.setPageIndex(pageIndex);
			        grid.setPageSize(pageSize);
					grid.setData(data.obj.rows);
				}
			});
		}
		
		 //清空
	      function clear() {
	        var form = new mini.Form("search_form");
	        form.clear();
	        query();
	      }
		
		//新增
	      function add(){
				var messageid = mini.loading("系统正在处理...", "请稍后");
				try {
					mini.open({
						url : CommonUtil.baseWebPath() +"/rev/iswhMini.jsp",
						title : "互换即远调业务单据选择",
						width : 850,
						height : 500,
						ondestroy : function(action) {	
							if (action == "ok"){
								var iframe = this.getIFrameEl();
								var data = iframe.contentWindow.GetData();
								data = mini.clone(data); //必须
								var row=data;
								var url = CommonUtil.baseWebPath() +"/rev/iswhEdit.jsp";
								var tab = {
									id :  "_"+"add",
									name : "_"+"add",
									iconCls : "",
									title : "互换类新增",
									url : url,
									showCloseButton:true
								};
								var paramData = {'operType':'A','closeFun':'query','pWinId':'iswhEdit',selectedData:row,action:"add"};
								CommonUtil.openNewMenuTab(tab,paramData);
							}
						}
					});
				} catch (error) {
					
				}
				mini.hideMessageBox(messageid);
			};
			
			//双击详情
		      function onRowDblClick(e) {
		        var row = grid.getSelected();
		        if (row) {
		          var url = CommonUtil.baseWebPath() + "/rev/iswhEdit.jsp?action=detail";
		          var tab = {
		            id: "iswhDetail",
		            name: "iswhDetail",
		            title: "互换类详情",
		            url: url,
		            showCloseButton: true
		          };
		          var paramData = {
		   	           selectedData: row
		   	          };
		          CommonUtil.openNewMenuTab(tab, paramData);
		        } else {
		          mini.alert("请选中一条记录！", "消息提示");
		        }
		      }
		
	    //修改
	      function edit() {
	        var row = grid.getSelected();
	        if (row) {
	          var url = CommonUtil.baseWebPath() + "/rev/iswhEdit.jsp?action=edit";
	          var tab = {
	            id: "iswhEdit",
	            name: "iswhEdit",
	            title: "互换类修改",
	            url: url,
	            showCloseButton: true,
	            parentId: top["win"].tabs.getActiveTab().name
	          };
	          
	          var paramData = {
	           selectedData: row
	          };
	          CommonUtil.openNewMenuTab(tab, paramData);

	        } else {
	          mini.alert("请选中一条记录！", "消息提示");
	        }
	      }
		
	    //删除
	      function del() {
	        var row = grid.getSelected();
	        if (row) {
	          mini.confirm("您确认要删除选中记录?", "系统警告", function (value) {
	            if (value == "ok") {
	              CommonUtil.ajax({
	                url: "/IfsRevIswhController/deleteIswhParam",
	                data: row,
	                callback: function (data) {
	                  if (data.code == 'error.common.0000') {
	                    mini.alert("删除成功!");
	                    grid.reload();
	                  } else {
	                    mini.alert("删除失败!");
	                  }
	                }
	              });
	            }
	          });
	        } else {
	          mini.alert("请选中一条记录！", "消息提示");
	        }
	      }
	    
	    //行点击事件
	      grid.on("select",function(e){
	  		var row=e.record;
	  		mini.get("approve_mine_commit_btn").setEnabled(false);
	  		mini.get("approve_commit_btn").setEnabled(false);
	  		mini.get("edit_btn").setEnabled(false);
	  		mini.get("delete_btn").setEnabled(false);
	  		if(row.approveStatus == "3"){//新建
	  			mini.get("approve_mine_commit_btn").setEnabled(true);
	  			mini.get("approve_commit_btn").setEnabled(false);
	  			mini.get("edit_btn").setEnabled(true);
	  			mini.get("delete_btn").setEnabled(true);
	  			mini.get("approve_log").setEnabled(false);
	  		}
	  		if( row.approveStatus == "6" || row.approveStatus == "5"){//审批通过：6   审批中:5
	  			mini.get("approve_mine_commit_btn").setEnabled(false);
	  			mini.get("approve_commit_btn").setEnabled(true);
	  			mini.get("edit_btn").setEnabled(false);
	  			mini.get("delete_btn").setEnabled(false);
	  		}
	  		if(row.approveStatus != null && row.approveStatus != "3") {
	  			mini.get("approve_log").setEnabled(true);
	  		}
	  		
	  	});
/***********************************  审批    **************************************************/
 
	//提交审批
	function commit(){
		mini.get("approve_mine_commit_btn").setEnabled(false);
		var messageid = mini.loading("系统正在处理...", "请稍后");
		try {
			verify(grid.getSelecteds());
		} catch (error) {
			
		}
		mini.hideMessageBox(messageid);	
		mini.get("approve_mine_commit_btn").setEnabled(true);
	} 
 
	//提交正式审批、待审批
	function verify(selections){
		if(selections.length == 0){
			mini.alert("请选中一条记录！","消息提示");
			return false;
		}else if(selections.length > 1){
			mini.alert("该功能不支持多笔提交，如需多笔提交请点击[批量提交]按钮","系统提示");
			return false;
		}
		if(selections[0]["approveStatus"] != "3" ){
			var url = CommonUtil.baseWebPath() +"/rev/iswhEdit.jsp?action=approve&ticketId="+selections[0].ticketId;
			var tab = {"id": "iswhApprove",name:"iswhApprove",url:url,title:"互换类冲销审批",
						parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
			var paramData = {selectedData:selections[0]};
			CommonUtil.openNewMenuTab(tab,paramData);
		}else{
			prdNo = "441";
			Approve.approveCommit(Approve.FlowType.RevApproveFlow,selections[0]["ticketId"],Approve.OrderStatus.New,"IfsIswhRevService",prdNo,function(){
				search(grid.pageSize,grid.pageIndex);
			});
		}
	};
 
	//审批
	function approve(){
		mini.get("approve_commit_btn").setEnabled(false);
		var messageid = mini.loading("系统正在处理...", "请稍后");
		try {
			verify(grid.getSelecteds());
		} catch (error) {
			
		}
		mini.hideMessageBox(messageid);	
		mini.get("approve_commit_btn").setEnabled(true);
	}
	
	//审批日志
	function searchlog(){
		appLog(grid.getSelecteds());
	}
	//审批日志查看
	function appLog(selections){
		var flow_type = Approve.FlowType.RevApproveFlow;
		if(selections.length <= 0){
			mini.alert("请选择要操作的数据","系统提示");
			return;
		}
		if(selections.length > 1){
			mini.alert("系统不支持多笔操作","系统提示");
			return;
		}
		if(selections[0].tradeSource == "3"){
			mini.alert("初始化导入的业务没有审批日志","系统提示");
			return false;
		}
		Approve.approveLog(flow_type,selections[0].ticketId);
	};
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
</script>
</body>
</html>