﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
    <title></title>
  </head>
  
<body style="width:100%;height:100%;background:white">
	<div class="mini-panel" title="互换即远调业务单据列表（已申请通过）" style="width:100%">
	<div style="width:100%;padding-top:10px;">
		<div id="search_form" class="mini-form" width="100%">
			<input id="dealType"  name="dealType" width="300px" align="center" class="mini-combobox" labelField="true"  label="业务类型：" onValuechanged="interestAmount"  data="CommonUtil.serverData.dictionary.operationType" labelStyle="text-align:right;" />
			<input id="ticketId"  name="ticketId" width="300px" class="mini-textbox" align="center" vtype="maxLength:50" labelField="true"  label="交易前端流水号：" labelStyle="text-align:right;" emptyText="请输入交易前端流水号"/>
			<div>
			<input id="contractId" name="contractId" width="300px" class="mini-textbox" align="center" vtype="maxLength:50" labelField="true"  label="成交单编号：" labelStyle="text-align:right;" emptyText="请输入成交单编号"/>
			<input id="dealNo" name="dealNo" class="mini-textbox" width="300px" align="center" vtype="maxLength:50" labelField="true"  label="opics交易号：" labelStyle="text-align:right;" emptyText="请输入opics交易号"/>
			<span style="float:right;margin-right: 80px">
				<a id="search_btn" class="mini-button" style="display: none"  onclick="query()">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"  onclick="clear()" >清空</a>
			</span>
			</div>
			
			<!-- <input id="approveStatus" name="approveStatus"  class="mini-combobox" data = "CommonUtil.serverData.dictionary.ApproveStatus" labelField="true"  label="审批状态：" labelStyle="text-align:right;" emptyText="请选择审批单状态"> -->
			
			
		</div>
	</div>
	</div>
	<div id="tradeManage" class="mini-fit">
		<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="dealNo" 
		allowAlternating="true" allowResize="true" onrowdblclick="onRowDblClick" border="true" 
		multiSelect="false" sortMode="client">
			<div property="columns">
				<div type="indexcolumn" headerAlign="center" width="50">序号</div>
				<div field="ticketId" width="170" headerAlign="center" align="center" allowSort="true">交易前端流水号</div>
<!-- 				<div field="port"  headerAlign="center" align="center" allowSort="true">投资组合</div>    -->
				<div field="contractId" width="170" headerAlign="center" align="center" allowSort="true">成交单编号</div>  
				<div field="dealNo" width="170" headerAlign="center" align="center" allowSort="true" style="display:none">opics交易号</div> 
<!-- 				<div field="statcode"  headerAlign="center" align="center" allowSort="true">opics错误码</div> -->
<!-- 				<div field="weight"  headerAlign="center" align="center" allowSort="true">权重</div> -->
<!-- 				<div field="dealSource" width="100" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'TradeSource'}" allowSort="true">交易来源</div>   -->
<!-- 				<div field="trader"  headerAlign="center" align="center" allowSort="true">交易员</div> -->
				<div field="forDate" width="180" headerAlign="center" align="center" allowSort="true">成交日期</div>
			</div>
		</div>  
	</div>
	<script type="text/javascript">
		mini.parse();

		var form=new mini.Form("#search_form");
		var grid=mini.get("datagrid");
		top["iswhMini"] = window;
		var rows='';
		var fundCode='';
		mini.get("dealType").select(0);
		
		function interestAmount(){
			query();
		}
		/* 按钮 查询事件 */
	function query(){
		search(grid.pageSize,0);
	}

	/* 查询 */
	function search(pageSize,pageIndex){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统也提示");
			return;
		}
		var data=form.getData(true);
		data['tableName']=mini.get("dealType").getValue();
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		url = "/IfsCfetsrmbIrsController/searchIswhMiniPage";
		data['approveStatus']='6';
		data['statcode']='-4';
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:url,
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	
	//清空
	function clear() {
        mini.get("ticketId").setValue("");
        mini.get("contractId").setValue("");
        mini.get("dealNo").setValue("");
        query();
      }
	
	
	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});
	
	//选中一行将数据展示
	function onRowDblClick(){
		CloseWindow("ok");
	}
	function GetData() {
        var row = grid.getSelected();
        var data = mini.get("dealType").getValue();
        row['dealType']=data;
        return row;
	}
	function SetData(data){
		rows=data.gridData;
		fundCode=data.fundCode;
	}
	function CloseWindow(action) {
        if (window.CloseOwnerWindow)
            return window.CloseOwnerWindow(action);
        else
            window.close();
	}
    //关闭窗口
    function onCancel() {
        CloseWindow("cancel");
    }
	$(document).ready(function(){
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
			search(10, 0);
		});
	})
	</script>
</body>
</html>