<%@ page language="java" pageEncoding="UTF-8"%>
<script src="<%=basePath%>/miniScript/common_mini.js" type="text/javascript"></script>
<span style="margin: 2px; display: block;"> 
	<a   id="add_btn" class="mini-button" style="display: none"   onclick="add()">新增</a>
	<a   id="edit_btn" class="mini-button" style="display: none"   onclick="edit()">修改</a>
	<a   id="delete_btn" class="mini-button" style="display: none"   onclick="del()">删除</a>
	<a   id="approve_commit_btn" class="mini-button" style="display: none"    onclick="approve()">审批</a>
	<a   id="approve_mine_commit_btn" class="mini-button" style="display: none"    onclick="commit()">提交审批</a>
	<a   id="approve_log" class="mini-button" style="display: none"     onclick="searchlog()">审批日志</a>
	<div id = "approveType" name = "approveType" class="mini-checkboxlist" style="float:right;" labelField="true" label="审批列表选择：" labelStyle="text-align:right;border: none;background-color: #fff;" 
		 value="mine" textField="text" valueField="id" multiSelect="false"
		 data="[{id:'mine',text:'我发起的'},{id:'approve',text:'待审批列表'},{id:'finished',text:'已审批列表'}]" onvaluechanged="checkBoxValuechanged">
	</div>  
</span>

<script>

function checkBoxValuechanged(e){
	$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
		search(10, 0);
		var approveType = mini.get("approveType").getValue();
		if (userId != 'admin') {
			if (approveType == "mine") {//我发起的
				initButton();
			} else if (approveType == "finished") {//已审批
				if (visibleBtn.add_btn) mini.get("add_btn").setVisible(false);
				if (visibleBtn.edit_btn) mini.get("edit_btn").setVisible(false);
				if (visibleBtn.delete_btn) mini.get("delete_btn").setVisible(false);
				if (visibleBtn.approve_commit_btn) mini.get("approve_commit_btn").setVisible(false);
				if (visibleBtn.approve_mine_commit_btn) mini.get("approve_mine_commit_btn").setVisible(false);
				if (visibleBtn.approve_log) mini.get("approve_log").setVisible(true);//审批日志
				mini.get("approve_log").setEnabled(true);//审批日志高亮
			} else {//待审批
				if (visibleBtn.add_btn) mini.get("add_btn").setVisible(false);
				if (visibleBtn.edit_btn) mini.get("edit_btn").setVisible(false);
				if (visibleBtn.delete_btn) mini.get("delete_btn").setVisible(false);
				if (visibleBtn.approve_commit_btn) mini.get("approve_commit_btn").setVisible(true);//审批
				mini.get("approve_commit_btn").setEnabled(true);//审批高亮
				if (visibleBtn.approve_mine_commit_btn) mini.get("approve_mine_commit_btn").setVisible(false);//提交审批
				if (visibleBtn.approve_log) mini.get("approve_log").setVisible(true);//审批日志
				mini.get("approve_log").setEnabled(true);//审批日志高亮
			}
		} else {
			if (approveType == "mine") {//我发起的
				initButton();
			} else if (approveType == "finished") {//已审批
				mini.get("add_btn").setVisible(false);
				mini.get("edit_btn").setVisible(false);
				mini.get("delete_btn").setVisible(false);
				mini.get("approve_commit_btn").setVisible(false);
				mini.get("approve_mine_commit_btn").setVisible(false);
				mini.get("approve_log").setVisible(true);//审批日志
				mini.get("approve_log").setEnabled(true);//审批日志高亮
			} else {//待审批
				mini.get("add_btn").setVisible(false);
				mini.get("edit_btn").setVisible(false);
				mini.get("delete_btn").setVisible(false);
				mini.get("approve_commit_btn").setVisible(true);//审批
				mini.get("approve_commit_btn").setEnabled(true);//审批高亮
				mini.get("approve_mine_commit_btn").setVisible(false);//提交审批
				mini.get("approve_log").setVisible(true);//审批日志
				mini.get("approve_log").setEnabled(true);//审批日志高亮
			}
		}
	});
}

/** 初始化按钮 */
function initButton(){
	$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
		if (userId != 'admin') {
			if (visibleBtn.add_btn) mini.get("add_btn").setVisible(true);
			mini.get("add_btn").setEnabled(true);
			if (visibleBtn.edit_btn) mini.get("edit_btn").setVisible(true);
			mini.get("edit_btn").setEnabled(true);
			if (visibleBtn.delete_btn) mini.get("delete_btn").setVisible(true);
			mini.get("delete_btn").setEnabled(true);
			if (visibleBtn.approve_commit_btn) mini.get("approve_commit_btn").setVisible(false);//审批不显示
			if (visibleBtn.approve_mine_commit_btn) mini.get("approve_mine_commit_btn").setVisible(true);//提交审批显示
			mini.get("approve_mine_commit_btn").setEnabled(true);//提交审批高亮
			if (visibleBtn.approve_log) mini.get("approve_log").setVisible(true);//审批日志显示
			mini.get("approve_log").setEnabled(true);//审批日志高亮

		} else {
			mini.get("add_btn").setVisible(true);
			mini.get("add_btn").setEnabled(true);
			mini.get("edit_btn").setVisible(true);
			mini.get("edit_btn").setEnabled(true);
			mini.get("delete_btn").setVisible(true);
			mini.get("delete_btn").setEnabled(true);
			mini.get("approve_commit_btn").setVisible(false);//审批不显示
			mini.get("approve_mine_commit_btn").setVisible(true);//提交审批显示
			mini.get("approve_mine_commit_btn").setEnabled(true);//提交审批高亮
			mini.get("approve_log").setVisible(true);//审批日志显示
			mini.get("approve_log").setEnabled(true);//审批日志高亮

		}
	});
}
































</script>