<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../../global.jsp" %>
<head>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset title">
    <legend>报文反馈查询</legend>
    <div id="QryManageGrid">
        <input id="tradeDate" name="tradeDate" class="mini-datepicker" emptyText="请选择日期..." labelField="true"
               allowInput="false"
               label="成交日期：" labelStyle="text-align:right;"/>
        <input id="execID" name="execID" class="mini-textbox mini-mustFill" labelField="true" label="成交单号："
               labelStyle="text-align:right;" emptyText="请输入成交单号"
        />

        <input id="marketIndicator" name="marketIndicator" class="mini-combobox mini-mustFill" labelField="true"
               label="市场类型：" labelStyle="text-align:right;" emptyText="请选择市场类型"
               data="CommonUtil.serverData.dictionary.MarketIndicator"/>
        <span style="float:right;margin-right: 150px">
            <a class="mini-button" style="display: none" id="search_btn" onclick="search(10,0)">查询</a>
            <a class="mini-button" style="display: none" id="clear_btn" onclick="clear()">清空</a>
            <a class="mini-button" style="display: none" id="confirm_send" onclick="sendConfirmMessage()">发送</a>
        </span>
    </div>
</fieldset>
<div class="mini-fit" style="width:100%;height:100%;">
    <div class="mini-datagrid borderAll" style="width:100%;height:100%;" sortMode="client" allowAlternating="true"
         idField="userId" id="grid" multiSelect="true">
        <div property="columns">
            <div type="checkcolumn"></div>
            <div field="dealno" headerAlign="center" allowSort="true">审批单号</div>
            <div field="execID" headerAlign="center" allowSort="true">成交编号</div>
            <div field="tradeDate" headerAlign="center" allowSort="true">成交日期</div>
            <!--                 <div field="marketIndicator" headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'MarketIndicator'}">市场类型</div> -->
        </div>
    </div>
</div>
</body>
<script>
    mini.parse();

    var grid = mini.get("grid");
    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
            mini.get("tradeDate").setValue("<%=__bizDate%>");
            mini.get("marketIndicator").setValue("12");
            mini.get("marketIndicator").setText("外汇即期");
            grid.on("beforeload", function (e) {
                e.cancel = true;
                var pageIndex = e.data.pageIndex;
                var pageSize = e.data.pageSize;
                search(pageSize, pageIndex);
            });
            search(grid.pageSize, grid.pageIndex);
        });
    });

    //搜索
    function search(pageSize, pageIndex) {
        var form = new mini.Form("QryManageGrid");
        var data = form.getData();
        form.validate();
        if (form.isValid() == false) {
            return;
        }
        data.tradeDate = mini.get("tradeDate").getText();
        data.branchId = branchId;
        data.pageNumber = pageIndex + 1;
        data.pageSize = pageSize;
        var marketIndicator = mini.get("marketIndicator").getValue();
        var tableName = "";
        if (marketIndicator == '12') {
            tableName = "ifs_cfetsfx_spt";
        } else if (marketIndicator == '14') {
            tableName = "ifs_cfetsfx_fwd";
        } else if (marketIndicator == '21') {
            tableName = "ifs_cfetsfx_lend";
        } else if (marketIndicator == '11') {
            tableName = "ifs_cfetsfx_swap";
        } else {
            alert("市场类型错误:[" + marketIndicator + "]");
            return;
        }
        data.tableName = tableName;
        var param = mini.encode(data); //序列化成JSON
        CommonUtil.ajax({
            url: "/QryMsgController/searchNoSendDeal",
            data: param,
            callback: function (data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }

    function clear() {
        var form = new mini.Form("#QryManageGrid");
        form.clear();
        mini.get("marketIndicator").setValue("12");
        mini.get("marketIndicator").setText("外汇即期");
        search(0, 10);
        <%--mini.get("tradeDate").setValue("<%=__bizDate%>");--%>
        <%--mini.get("marketIndicator").setValue("12");--%>
        <%--mini.get("marketIndicator").setText("外汇即期");--%>
    }

    function sendConfirmMessage() {
        var marketIndicator = mini.get("marketIndicator").getValue();
        if (marketIndicator == "" || marketIndicator == null) {
            alert("市场类型不能为空");
            return;
        }
        var form = new mini.Form("QryManageGrid");
        var formData = form.getData(true);
        var rows = grid.getSelecteds();
        var str = "";
        if (rows.length > 0) {
            for (var i = 0; i < rows.length; i++) {
                str = str + rows[i].dealno + ",";
            }
            str = str.substring(0, str.length - 1);
        }
        mini.confirm("您确认要发送报文吗?", "系统警告", function (value) {
            if (value == "ok") {
                CommonUtil.ajax({
                    url: "/QryMsgController/sendConfirmMessage",
                    data: {
                        execID: str,
                        tradeDate: formData["tradeDate"],
                        marketIndicator: marketIndicator,
                        total: rows.length
                    },
                    callback: function (data) {
                        mini.alert("发送成功", "系统提示");
                        search(10, 0);
                    }
                });
            }
        });
    }
</script>