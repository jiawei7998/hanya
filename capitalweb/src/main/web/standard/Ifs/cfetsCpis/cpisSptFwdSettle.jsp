<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<head>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>
<body>
<fieldset class="mini-fieldset title">
    <legend>交易后即远期清算信息配置</legend>
    <div id="search_form" class="search-width" style="width:100%" cols="6">
        <input id="execID" name="execID" class="mini-textbox" labelField="true" label="成交编号：" labelStyle="text-align:right;" emptyText="请输入成交编号" width="320px" />
        <input id="ticketId" name="ticketId" class="mini-textbox" labelField="true" label="审批单号：" emptyText="请填写审批单号" labelStyle="text-align:right;" width="320px"/>
        <input id="dealNo" name="dealNo" class="mini-textbox" labelField="true" label="OPICS交易编号：" labelStyle="text-align:right;" width="320px" emptyText="请输入OPICS交易编号" />
        <nobr>
            <input id="startDate" name="startDate" class="mini-datepicker" width="320px" labelField="true" label="操作时间："  ondrawdate="onDrawDateStart" labelStyle="text-align:right;" emptyText="起始日期" format="yyyy-MM-dd"/>
            <input id="endDate" name="endDate" class="mini-datepicker" width="320px" labelField="true" label="～" labelStyle="text-align:center;"  ondrawdate="onDrawDateEnd" emptyText="结束日期" format="yyyy-MM-dd"/>
        </nobr>
        <span style="float:right;margin-right: 150px">
            <a class="mini-button"   id="search_btn"  onclick="search(10,0)">查询</a>
            <a class="mini-button"   id="clear_btn"  onclick="clear()">清空</a>
        </span>
    </div>
    <span style="margin:2px;display: block;">
            <a class="mini-button"   id="edit_btn"  onClick="modify();">修改</a>
            <a class="mini-button"   id="delete_btn"  onClick="del();">删除</a>
    </span>
</fieldset>
<div class="mini-fit" style="width:100%;height:100%;">
    <div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;"  sortMode="client" allowAlternating="true" idField="userId" onrowdblclick="onRowDblClick"  border="true" allowResize="true">
        <div property="columns">
            <div field="ticketId" headerAlign="center" align="left" width="150px">审批单号</div>
            <div field="execID" headerAlign="center" align="left" width="150px">cfets成交单号</div>
            <div field="dealNo" headerAlign="center" align="left" width="150px">OPICS交易号</div>
            <div field="tradeType" headerAlign="center" align="left" width="150px">交易类型</div>
            <div field="cInstitutionId" headerAlign="center" align="left" width="150px">对手方21位码</div>
            <div field="deliveryType" headerAlign="center" align="left" width="150px">PVP标识</div>
            <div field="cBankname" headerAlign="center" align="left" width="150px">对手方资金开户行</div>
            <div field="bankname" headerAlign="center" align="left" width="150px">本方资金开户行</div>
            <div field="cAcctname" headerAlign="center" align="left" width="150px">对手方资金帐户户名</div>
            <div field="acctname" headerAlign="center" align="left" width="150px">本方资金帐户户名</div>
            <div field="acctopenno" headerAlign="center" align="left" width="150px">对手方支付系统行号</div>
            <div field="cAcctopenno" headerAlign="center" align="left" width="150px">本方支付系统行号</div>
            <div field="acctno" headerAlign="center" align="left" width="150px">对手方资金帐号</div>
            <div field="cAcctno" headerAlign="center" align="left" width="150px">本方资金帐号</div>
            <div field="remark" headerAlign="center" align="left" width="150px">对手方附言</div>
            <div field="cRemark" headerAlign="center" align="left" width="150px">本方附言</div>
            <div field="bankopenno" headerAlign="center" align="left" width="150px">对手方开户行BICCODE</div>
            <div field="cBankopenno" headerAlign="center" align="left" width="150px">本方开户行BICCODE</div>
            <div field="bankacctno" headerAlign="center" align="left" width="150px">对手方开户行帐号</div>
            <div field="cBankacctno" headerAlign="center" align="left" width="150px">对手方开户行帐号</div>
            <div field="acctopenno" headerAlign="center" align="left" width="150px">对手方收款行BICCODE</div>
            <div field="cAcctopenno" headerAlign="center" align="left" width="150px">本方收款行BICCODE</div>
            <div field="intermediarybankname" headerAlign="center" align="left" width="150px">对手方中间行名称</div>
            <div field="cIntermediarybankname" headerAlign="center" align="left" width="150px">本方中间行名称</div>
            <div field="intermediarybankbiccode" headerAlign="center" align="left" width="150px">对手方中间行BICCODE</div>
            <div field="cIntermediarybankbiccode" headerAlign="center" align="left" width="150px">本方中间行BICCODE</div>
            <div field="intermediarybankacctno" headerAlign="center" align="left" width="150px">对手方中间行帐号</div>
            <div field="cIntermediarybankacctno" headerAlign="center" align="left" width="150px">本方中间行帐号</div>
        </div>
    </div>
</div>
</body>
<script>
    mini.parse();

    var url = window.location.search;
    var grid = mini.get("datagrid");
    var userId = '<%=__sessionUser.getUserId()%>';
    var form = new mini.Form("#search_form");

    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize, pageIndex);
    });


    // 查询
    function search(pageSize, pageIndex) {
        var form = new mini.Form("#search_form");
        form.validate();
        if (form.isValid() == false) {
            mini.alert("信息填写有误，请重新填写", "系统也提示");
            return;
        }
        var data = form.getData(true);
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        data['branchId'] = branchId;
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: "/IfsReportCshbnwtzZtctqReportController/searchIfsReportCshbnwtzZtctqReportPage",
            data: params,
            callback: function (data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);

            }
        });
    }

    function query() {
        search(grid.pageSize, 0);
    }

    function clear() {
        var form = new mini.Form("search_form");
        form.clear();
        search(10, 0);
    }

    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
            search(100, 0);
        });
    });

    //交易日期
    function onDrawDateStart(e) {
        var startDate = e.date;
        var endDate= mini.get("endDate").getValue();
        if(CommonUtil.isNull(endDate)){
            return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }

    function onDrawDateEnd(e) {
        var endDate = e.date;
        var startDate = mini.get("startDate").getValue();
        if(CommonUtil.isNull(startDate)){
            return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }

</script>
