<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../../global.jsp" %>
<html>
<head>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script src="<%=basePath%>/miniScript/pinyin.js" type="text/javascript"></script>
    <title>交易后即远期清算信息配置 维护</title>
</head>

<body style="width:100%;height:100%;background:white">
    <div class="mini-splitter" style="width:100%;height:100%;">
        <div size="90%" showCollapseButton="false">
            <h1 style="text-align:center"><strong>交易后即远期清算信息</strong></h1>
            <div id="field_form" class="mini-fit area"  style="background:white">
                <input id="userid" name="userid" class="mini-hidden"/>
                <div class="mini-panel" title="交易后即远期清算信息" style="width:100%;"  allowResize="true" collapseOnTitleClick="true">
                    <div class="leftarea">
                        <input id="dealNo" name="dealNo" class="mini-textbox" labelField="true" width="100%" label="交易单号：" labelStyle="text-align:left;width:120px;" />
                        <input id="cInstitutionId" name="cInstitutionId" class="mini-textbox" labelField="true" width="100%" label="对手方21位码：" labelStyle="text-align:left;width:120px;" />
                        <input id="dealNo" name="dealNo" class="mini-textbox" labelField="true" width="100%" label="本方资金开户行：" labelStyle="text-align:left;width:120px;" />
                        <input id="dealNo" name="dealNo" class="mini-textbox" labelField="true" width="100%" label="本方资金帐户户名：" labelStyle="text-align:left;width:120px;" />
                        <input id="dealNo" name="dealNo" class="mini-textbox" labelField="true" width="100%" label="本方支付系统行号：" labelStyle="text-align:left;width:120px;" />
                        <input id="dealNo" name="dealNo" class="mini-textbox" labelField="true" width="100%" label="本方资金帐号：" labelStyle="text-align:left;width:120px;" />
                        <input id="dealNo" name="dealNo" class="mini-textbox" labelField="true" width="100%" label="本方附言：" labelStyle="text-align:left;width:120px;" />
                        <input id="dealNo" name="dealNo" class="mini-textbox" labelField="true" width="100%" label="本方开户行BICCODE：" labelStyle="text-align:left;width:120px;" />
                        <input id="dealNo" name="dealNo" class="mini-textbox" labelField="true" width="100%" label="本方开户行帐号：" labelStyle="text-align:left;width:120px;" />
                        <input id="dealNo" name="dealNo" class="mini-textbox" labelField="true" width="100%" label="本方收款行BICCODE：" labelStyle="text-align:left;width:120px;" />
                        <input id="dealNo" name="dealNo" class="mini-textbox" labelField="true" width="100%" label="本方中间行名称：" labelStyle="text-align:left;width:120px;" />
                        <input id="dealNo" name="dealNo" class="mini-textbox" labelField="true" width="100%" label="本方中间行BICCODE：" labelStyle="text-align:left;width:120px;" />
                        <input id="dealNo" name="dealNo" class="mini-textbox" labelField="true" width="100%" label="本方中间行帐号：" labelStyle="text-align:left;width:120px;" />
                     </div>
                    <div class="leftarea">
                        <input id="tradeType" name="tradeType" class="mini-textbox" labelField="true" width="100%" label="交易类型：" labelStyle="text-align:left;width:120px;" />
                        <input id="dealNo" name="dealNo" class="mini-textbox" labelField="true" width="100%" label="对手方资金开户行：" labelStyle="text-align:left;width:120px;" />
                        <input id="dealNo" name="dealNo" class="mini-textbox" labelField="true" width="100%" label="对手方资金帐户户名：" labelStyle="text-align:left;width:120px;" />
                        <input id="dealNo" name="dealNo" class="mini-textbox" labelField="true" width="100%" label="对手方支付系统行号：" labelStyle="text-align:left;width:120px;" />
                        <input id="dealNo" name="dealNo" class="mini-textbox" labelField="true" width="100%" label="对手方资金帐号：" labelStyle="text-align:left;width:120px;" />
                        <input id="dealNo" name="dealNo" class="mini-textbox" labelField="true" width="100%" label="对手方附言：" labelStyle="text-align:left;width:120px;" />
                        <input id="dealNo" name="dealNo" class="mini-textbox" labelField="true" width="100%" label="对手方开户行BICCODE：" labelStyle="text-align:left;width:120px;" />
                        <input id="dealNo" name="dealNo" class="mini-textbox" labelField="true" width="100%" label="对手方开户行帐号：" labelStyle="text-align:left;width:120px;" />
                        <input id="dealNo" name="dealNo" class="mini-textbox" labelField="true" width="100%" label="对手方收款行BICCODE：" labelStyle="text-align:left;width:120px;" />
                        <input id="dealNo" name="dealNo" class="mini-textbox" labelField="true" width="100%" label="对手方中间行名称：" labelStyle="text-align:left;width:120px;" />
                        <input id="dealNo" name="dealNo" class="mini-textbox" labelField="true" width="100%" label="对手方中间行BICCODE：" labelStyle="text-align:left;width:120px;" />
                        <input id="dealNo" name="dealNo" class="mini-textbox" labelField="true" width="100%" label="对手方中间行帐号：" labelStyle="text-align:left;width:120px;" />
                        <input id="dealNo" name="dealNo" class="mini-textbox" labelField="true" width="100%" label="插入时间：" labelStyle="text-align:left;width:120px;" />
                    </div>
                </div>
            </div>
        </div>
        <div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
            <div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="save_btn"   onclick="save">保存交易</a></div>
            <div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="close_btn"   onclick="close">关闭界面</a></div>
        </div>
    </div>
<script type="text/javascript">
    mini.parse();

    //获取当前tab
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var row = params.selectData;
    var url = window.location.search;
    var action = CommonUtil.getParam(url, "action");
    var form = new mini.Form("#field_form");

    var tradeData = {};

    tradeData.selectData = row;
    tradeData.operType = action;
    tradeData.serial_no = row.execId;
    tradeData.task_id = row.taskId;

    //保存
    function save() {
        //表单验证！！！
        form.validate();
        if (form.isValid() == false) {
            return;
        }
        if (checkOpics()) {
            return;
        }
        var userId = '<%=__sessionUser.getUserId()%>';
        var data = form.getData(true);
        if (data.status != "C") {
            data['status'] = "A";
        }
        data['userid'] = userId;
        data['issuername'] = mini.get("issuer").getText();
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: "/IfsOpicsBondController/save",
            data: params,
            callback: function (data) {
                mini.alert(data, '提示信息', function () {
                    if (data == '保存成功' || data == '信息修改成功') {
                        top["win"].closeMenuTab();
                    }
                });
            }
        });
    }

    function close() {
        top["win"].closeMenuTab();
    }


    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
            if ($.inArray(action, ["edit", "detail"]) > -1) {

                form.setData(row);
                if ($.inArray(action, ["detail"]) > -1) {
                    mini.get("save_btn").setVisible(false);
                    form.setEnabled(false);
                }
            }
        });
    });

</script>
<script src="<%=basePath%>/miniScript/miniMustFill.js"></script>
</body>
</html>