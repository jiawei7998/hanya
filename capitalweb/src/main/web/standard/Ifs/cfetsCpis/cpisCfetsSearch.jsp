<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<head>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>
<body>
<fieldset class="mini-fieldset title">
    <legend>确认报文查询</legend>
    <div id="search_form" class="search-width" style="width:100%" cols="6">
        <input id="execID" name="execID" class="mini-textbox" labelField="true" label="成交编号：" labelStyle="text-align:right;" emptyText="请输入成交编号" />
        <input id="marketIndicator" name="marketIndicator" class="mini-combobox mini-mustFill" labelField="true" label="交易类型：" labelStyle="text-align:right;" emptyText="请选择交易类型"
               data="CommonUtil.serverData.dictionary.MarketIndicator" onvaluechanged="controQueryExecType"/>
        <input id="queryStartDate" name="queryStartDate" class="mini-datepicker" emptyText="请选择日期..." labelField="true" allowInput="false"
               label="成交起始日：" labelStyle="text-align:right;" />
        <input id="queryEndDate" name="queryEndDate" class="mini-datepicker" emptyText="请选择日期..." labelField="true" allowInput="false"
               label="成交终止日：" labelStyle="text-align:right;" />
        <input id="queryExecType" name="queryExecType" textField="typeValue" valueField="typeId" class="mini-combobox mini-mustFill" width="320px" labelField="true" label="查询成交状态：" labelStyle="text-align:right;" emptyText="请选择查询成交状态"
               />
        <input id="netGrossInd" name="netGrossInd" class="mini-combobox mini-mustFill" labelField="true" label="清算方式：" value="4" labelStyle="text-align:right;" emptyText="请选择清算方式"
               data="CommonUtil.serverData.dictionary.NetGrossInd" />
        <input id="confirmType" name="confirmType" class="mini-combobox mini-mustFill" labelField="true" label="查询类型："  value="1" width="320px" labelStyle="text-align:right;" emptyText="请选择查询类型"
               data="CommonUtil.serverData.dictionary.confirmType"/>
<%--        <input id="marketID" name="marketID" class="mini-combobox mini-mustFill" labelField="true" label="交易场所：" labelStyle="text-align:right;" emptyText="请选择交易场所"--%>
<%--               data="CommonUtil.serverData.dictionary.slSmtMarketID"/>--%>
        <span style="float:right;margin-right: 150px">
            <a class="mini-button" id="searchCfets"  onclick="searchCfets()">同步Cfets</a>
            <a class="mini-button" id="clear_btn"  onclick="clear()">清空</a>
        </span>
    </div>
</fieldset>
</body>
<script>
    mini.parse();
    var form=new mini.Form("#search_form");
    function SetData(data){
        form.setData(data);
        if(mini.get("marketID").getValue()==""){
            mini.get("marketID").setValue("2");
        }
    }

    //搜索
    function searchCfets() {
        form.validate();
        if(form.isValid()==false){
            return;
        }
        var data = form.getData(true);
        data.queryStartDate = mini.get("queryStartDate").getText();
        if(data.queryStartDate) data.queryStartDate = data.queryStartDate.replace(/-/g,'');
        data.queryEndDate = mini.get("queryEndDate").getText();
        if(data.queryEndDate) data.queryEndDate = data.queryEndDate.replace(/-/g,'');

        if(!data.marketIndicator) {
            mini.alert("请选择交易类型","提示");
            return false;
        }

        if(!data.queryStartDate) {
            mini.alert("请选择成交起始日","提示");
            return false;
        }

        if(!data.confirmType) {
            mini.alert("请选择查询类型","提示");
            return false;
        }
        var param = mini.encode(data); //序列化成JSON
        CommonUtil.ajax({
            url: "/QryMsgController/searchQryConfirmMsg",
            data: param,
            callback: function (data) {
                if (data.code == 'error.common.0000') {
                    mini.alert("同步成功。", '提示信息', function () {
                    });
                } else {
                    mini.alert("同步失败。");
                }
            }
        });
    }

    function clear() {
        var form = new mini.Form("#search_form");
        form.clear();
    }

    function controQueryExecType(e){
        var markId = e.value;
        var data=[];
        if("21" == markId){
            //拆借市场
             data =[
                 {'typeId':"106",'typeValue':"106-有效"},
                 {'typeId':"4",'typeValue':"4-撤销"},
                {'typeId':"102",'typeValue':"102-全部"}
            ];
        }else{
             data =[
                 {'typeId':"0",'typeValue':"0-有效"},
                 {'typeId':"4",'typeValue':"4-撤销"},
                {'typeId':"102",'typeValue':"102-全部"}
            ];
        }
        mini.get("queryExecType").setData(data);
        mini.get("queryExecType").setValue("102");

    }
</script>
