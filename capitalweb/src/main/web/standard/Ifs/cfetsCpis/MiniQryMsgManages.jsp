<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>
<body style="width:100%;height:100%;background:white">
    <fieldset class="mini-fieldset title">
    <legend>报文反馈查询</legend>
    <div id="QryManageGrid"  style="width:100%"  cols="6">
    	<input id="confirmID" name="confirmID" class="mini-textbox" width="320px" labelField="true" label="确认报文编号：" labelStyle="text-align:right;" emptyText="请输入确认报文编号"/>
        <input id="execID" name="execID" class="mini-textbox" width="320px" labelField="true" label="成交编号：" labelStyle="text-align:right;" emptyText="请输入成交编号"/>
        <input id="tradeDate" name="tradeDate" class="mini-datepicker" width="320px" emptyText="请选择日期..." labelField="true"
            label="成交日期：" labelStyle="text-align:right;" /> <br>
        <input id="affirmStatus" name="affirmStatus" class="mini-combobox mini-mustFill" width="320px" labelField="true" label="校验状态：" labelStyle="text-align:right;" emptyText="请选择校验状态"
         data="CommonUtil.serverData.dictionary.AffirmStatus" popupWidth="300"/>
        <input id="marketIndicator" name="marketIndicator" class="mini-combobox mini-mustFill" width="320px" labelField="true" label="市场类型：" labelStyle="text-align:right;" emptyText="请选择市场类型"
         data="CommonUtil.serverData.dictionary.MarketIndicator"/>

        <input id="confirmType" name="confirmType" class="mini-combobox mini-mustFill" width="320px" labelField="true" label="查询类型：" value="1" labelStyle="text-align:right;" emptyText="请查询类型"
               data="CommonUtil.serverData.dictionary.confirmType"/>

        <span style="float:right;margin-right: 150px">
            <a class="mini-button" style="display: none"  id="search_btn"  onclick="search(100,0)">查询</a>
            <a class="mini-button" style="display: none"  id="clear_btn"  onclick="clear()">清空</a>
            <a class="mini-button" style="display: none"  id="confirm_send"  onclick="sendConfirmMessage()">报文发送</a>
            <a class="mini-button" style="display: none"  id="confirm_qry"  onclick="sendQryMessage()">同步报文</a>
<!--             <a class="mini-button" style="display: none"  id="sendAgain"  onclick="sendAgain(2)">报文重发</a> -->
        </span>
    </div>
    </fieldset>
    <div class="mini-fit" style="width:100%;height:100%;">
        <div class="mini-datagrid borderAll" style="width:100%;height:100%" sortMode="client" allowAlternating="true"   idField="userId" id="grid"  >
            <div property="columns">
                <div type="indexcolumn" headerAlign="center" align="center">序列</div>
                <div field="confirmID" width="150px" headerAlign="center" >对应确认报文编号</div>
                <div field="execID" width="150px" headerAlign="center" >成交编号</div>
                <div field="tradeDate" headerAlign="center" >成交日期</div>
                <div field="marketIndicator" headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'MarketIndicator'}">市场类型</div>
                <div field="execType" headerAlign="center" width="150px" renderer="CommonUtil.dictRenderer" data-options="{dict:'ExecType'}">成交状态</div>

                <div field="sendstate" headerAlign="center" width="200px" renderer="CommonUtil.dictRenderer" data-options="{dict:'sendState'}">交易后确认状态</div>

                <div field="marketID" headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'AffirmMarketID'}">交易场所</div>
                <div field="affirmStatus" headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'AffirmStatus'}">校验状态</div>
                <div field="text" headerAlign="center" width="350px">未匹配字段</div>
            </div>
        </div>
    </div>
    <fieldset style="width: 100%;border:solid 1px #aaa;margin-top:8px;position:relative;">
		<legend>报文详情</legend>
		<div id="MiniSettleForeigDetail" style="padding:5px;width: 100%;height: 100%;" class="mini-fit area" 
		 allowAlternating="true" allowResize="true" border="true">
		<input id="text" name="text" class="mini-textArea mini-mustFill" labelField="true" 
		label="未匹配字段："  labelStyle="text-align:left;width:130px;" style="width:100%;" />
		</div>
	</fieldset>
</body>
<script>
    mini.parse();

    var grid = mini.get("grid");
    var db = new mini.DataBinding();
    db.bindForm("MiniSettleForeigDetail", grid);
    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
            mini.get("tradeDate").setValue("<%=__bizDate%>");
            grid.on("beforeload", function (e) {
                e.cancel = true;
                var pageIndex = e.data.pageIndex;
                var pageSize = e.data.pageSize;
                search(pageSize, pageIndex);
            });
            search(grid.pageSize, grid.pageIndex);
        });
    });
    //用户状态的渲染
    var userStatus = [{ "id": "0", "text": "注销" }, { "id": "1", "text": "正常" }];
    function onUserStatusRenderer(e) {
        for (var i = 0, l = userStatus.length; i < l; i++) {
            var g = userStatus[i];
            if (g.id == e.value) return g.text;
        }
        return "";
    }
       
    //显示效果
    function successtips() {
        mini.showTips({
            content: '<span class="tips-icontext">操作成功</span>',
            state: "success",
            offset: [0, 0],
            x: "center",		  
            y: "center",
            timeout: 3000
        });
    }
    top['userEdit'] = window;
        function ShowUser() {
            var record = grid.getSelected();
            return record;
        }

    //搜索
    function search(pageSize, pageIndex) {
        var form = new mini.Form("QryManageGrid");
        var data = form.getData();
        form.validate();
        if(form.isValid()==false){
            return;
        }
        data.tradeDate = mini.get("tradeDate").getText();
//         if(data.tradeDate) data.tradeDate = data.tradeDate.replace(/-/g,'');
        data.branchId =branchId;
        data.pageNumber = pageIndex + 1;
        data.pageSize = pageSize;
        var param = mini.encode(data); //序列化成JSON
        CommonUtil.ajax({
            url: "/QryMsgController/searchQryMsg",
            data: param,
            callback: function (data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }
    //查询报文状态
    function sendQryMessage(){
        var data = form.getData(true);

        if(!data.confirmType) {
            mini.alert("请选择查询类型","提示");
            return false;
        }
        var row = grid.getSelected();
        if(row){
            row.confirmType = data.confirmType;
            var param = mini.encode(data); //序列化成JSON
            CommonUtil.ajax({
                url: "/QryMsgController/searchQryConfirmMsg",
                data: param,
                callback: function (data) {
                    if (data.code == 'error.common.0000') {
                        mini.alert("同步成功。", '提示信息', function () {
                        });
                    } else {
                        mini.alert("同步失败。");
                    }
                }
            });
        }else{
            mini.alert("请选中一条记录!","系统提示");
        }
    }

    function clear() {
        var form = new mini.Form("#QryManageGrid");
        form.clear();

        mini.get("confirmType").setValue("1");
        <%--mini.get("tradeDate").setValue("<%=__bizDate%>");--%>
    }     
    
    function sendConfirmMessage(){
    	mini.open({
            url: "./Ifs/cfetsCpis/MiniSendConfirmInfo.jsp",
            title: "确认报文发送",
            width: 700,
            height: 600,
            ondestroy: function (action) {
//                 if (action == "ok") {
                	grid.reload();
//                 }
            }
        });
    }
    	
    </script>