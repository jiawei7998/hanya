<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>外汇头寸估值</title>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
	<legend>外汇头寸估值管理</legend>	
	<div>
		<div id="search_form" style="width:100%" cols="6">
			<input id="fedealno" name="fedealno" class="mini-textbox" labelField="true" label="流水号：" width="320px"
				labelStyle="text-align:center;width:140px" emptyText="请输入流水号"/>
			<input id="cust" name="cust" class="mini-textbox" labelField="true" label="交易对手：" width="320px"
				labelStyle="text-align:center;width:140px" emptyText="请输入交易对手"/>
			<span style="float: right; margin-right: 50px"> 
				<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
			</span>
		</div>
	</div>
</fieldset>

<span style="margin: 2px; display: block;"> 
	<a  id="add_btn" class="mini-button" style="display: none"   onclick="add()">新增</a>
	<a  id="edit_btn" class="mini-button" style="display: none"   onclick="edit()">修改</a>
	<a  id="delete_btn" class="mini-button" style="display: none"   onclick="del()">删除</a>
	<!-- <a  id="sync_btn" class="mini-button" style="display: none"   onclick="sync()">同步至OPICS</a>
	<a  id="refresh_btn" class="mini-button" style="display: none"   onclick="refresh()">刷新状态</a> -->
</span>  

<div class="mini-fit" >
	<div id="acct_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" allowAlternating="true"
		onrowdblclick="onRowDblClick" allowResize="true" sortMode="client" allowAlternating="true" border="true">
		<div property="columns">
			<div type="indexcolumn" width="30px" headerAlign="center">序号</div>
			<div field="br" width="50px" align="center" headerAlign="center">部门</div>
			<div field="fedealno" width="150" headerAlign="center" align="center">流水号</div>
			<div field="localstatus" width="100"  headerAlign="center" align="center" 
				 renderer="CommonUtil.dictRenderer" data-options="{dict:'DealStatus'}">处理状态</div>
			<div field="dealno" width="100px" align="center"  headerAlign="center" >opics编号</div>
			<div field="dealdate" width="100" headerAlign="center" align="center" renderer="onDateRenderer" allowSort="true">交易日期</div>
			<div field="ps" width="80" headerAlign="center" align="center" allowSort="true" renderer="CommonUtil.dictRenderer"
				 data-options="{dict:'trading'}">交易方向</div>
			<div field="ccy" width="100" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer"
				 data-options="{dict:'Currency'}">外币币种</div>
			<div field="ccyamt" width="150" headerAlign="center" align="right" numberFormat="#,0.0000">外币金额</div>
			<div field="ctrccy" width="100" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer"
				 data-options="{dict:'Currency'}">本币币种</div>
			<div field="ctramt" width="150" headerAlign="center" align="right" numberFormat="#,0.0000">本币金额</div>
			<div field="trad" width="150" headerAlign="center" align="center" allowSort="true">交易员</div>
			<div field="cust" width="150" headerAlign="center" align="center">交易对手</div>
			<div field="vdate" width="100" headerAlign="center" align="center" renderer="onDateRenderer" allowSort="true">起息日</div>
			<div field="product" width="80" headerAlign="center" align="center">产品代码</div>
			<div field="prodtype" width="80" headerAlign="center" align="center">产品类型</div>
			<div field="port" width="80" headerAlign="center" align="center">投资组合</div>
			<div field="cost" width="100" headerAlign="center" align="center">成本中心</div>
			<div field="dealtext" width="120" headerAlign="center" align="center">交易来源</div>
			
			<!-- <div field="server" width="120px" align="center" headerAlign="center">server</div>    --> 
			<!-- <div field="seq" width="50" headerAlign="center" align="center">seq</div> -->
			<!-- <div field="inoutind" width="80" headerAlign="center" align="center">inoutind</div> -->
			<!-- <div field="brok" width="50" headerAlign="center" align="center">brok</div> -->
			<!-- <div field="siind" width="80" headerAlign="center" align="center">siind</div>
			<div field="verind" width="80" headerAlign="center" align="center">verind</div>
			<div field="suppayind" width="80" headerAlign="center" align="center">suppayind</div>
			<div field="suprecind" width="80" headerAlign="center" align="center">suprecind</div>
			<div field="supconfind" width="80" headerAlign="center" align="center">supconfind</div>
			<div field="authsi" width="50" headerAlign="center" align="center">authsi</div> -->
		</div>
	</div>
</div>   

<script>
	mini.parse();
	var grid = mini.get("acct_grid");
	var form = new mini.Form("#search_form");
	
	// 加载日期格式
    function onDateRenderer(e) {
		var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)==null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g,'/'));
		if (value) return mini.formatDate(value, 'yyyy-MM-dd');
	}
	
	grid.on("beforeload", function(e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex;
		var pageSize = e.data.pageSize;
		search(pageSize, pageIndex);
	});
	
	grid.on("select",function(e){
		var row = e.record;
		var localstatus = row.localstatus;
		if(localstatus == "0"){ // 未同步
			mini.get("edit_btn").setEnabled(true);
			mini.get("sync_btn").setEnabled(true);
			mini.get("delete_btn").setEnabled(true);
		} else {
			mini.get("edit_btn").setEnabled(false);
			mini.get("sync_btn").setEnabled(false);
			mini.get("delete_btn").setEnabled(false);
		}
		if(localstatus == "2"){ // 同步成功
			mini.get("refresh_btn").setEnabled(false);
		} else {
			mini.get("refresh_btn").setEnabled(true);
		}
	}); 
	
	$(document).ready(function(){
		search(10, 0);
	});
	
	/* 查询 */
	function search(pageSize,pageIndex){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统提示");
			return;
		}
		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:"/IfsOpicsAcupController/searchNbhPage",
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	
	// 添加
	function add(){
		var url = CommonUtil.baseWebPath() +"/../Ifs/ncbs/nbhGatherEdit.jsp?action=add";
		var tab = {id: "nbhGatherAdd",name:"nbhGatherAdd",url:url,title:"外汇头寸估值新增",
					parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
		var paramData = {selectData:""};
		CommonUtil.openNewMenuTab(tab,paramData);
	}
		
	// 修改
	function edit(){
		var row=grid.getSelected();
		if(row){
			var vdate = new Date(row.vdate);
        	row.vdate=vdate;
        	var dealdate = new Date(row.dealdate);
        	row.dealdate=dealdate;
			var url = CommonUtil.baseWebPath() +"/../Ifs/ncbs/nbhGatherEdit.jsp?action=edit";
			var tab = {id: "nbhGatherEdit",name:"nbhGatherEdit",url:url,title:"外汇头寸估值修改",
						parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
			var paramData = {selectData:row};
			CommonUtil.openNewMenuTab(tab,paramData);
		}else{
			mini.alert("请选择一条记录","提示");
		}
	}
	
	// 删除
	function del(){
		var rows=grid.getSelecteds();
		if(rows.length==0){
			mini.alert("请选中一行","提示");
			return;
		}
		mini.confirm("您确认要删除该内部户信息?","系统警告",function(value){   
			if (value=='ok'){   
				var data=rows[0];
				params=mini.encode(data);
				CommonUtil.ajax( {
					url:"/IfsOpicsAcupController/deleteNbhAcct",
					data:params,
					callback : function(data) {
						mini.alert("删除成功.","系统提示");
						search(10,0);
					}
				});
			}
		});
	}

	//查看详情
	function onRowDblClick(e) {
		var row=grid.getSelected();
		var url = CommonUtil.baseWebPath() +"/../Ifs/ncbs/nbhGatherEdit.jsp?action=detail";
		var tab = {id: "nbhGatherDetail",name:"nbhGatherDetail",url:url,title:"外汇头寸估值详情",
					parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
		var vdate = new Date(row.vdate);
    	row.vdate=vdate;
    	var dealdate = new Date(row.dealdate);
    	row.dealdate=dealdate;
		var paramData = {selectData:row};
		CommonUtil.openNewMenuTab(tab,paramData);
	}
	
	//清空
	function clear(){
		var form=new mini.Form("search_form");
		form.clear();
		query();
	}
	
	/* 按钮 查询事件 */
	function query(){
		search(grid.pageSize,0);
	}
	
	//同步至OPICS
	function sync(){
		var rows=grid.getSelecteds();
		if(rows.length==0){
			mini.alert("请选中一行","提示");
			return;
		}
		mini.confirm("您确认要同步该头寸信息?","系统警告",function(value){   
			if (value=='ok'){
				var data=rows[0];
				var vdate = new Date(rows[0].vdate);
				var vate = mini.formatDate(vdate, 'yyyy-MM-dd');
		    	var dealdate = new Date(rows[0].dealdate);
		    	var ddate = mini.formatDate(dealdate, 'yyyy-MM-dd');
		    	data['vdate']=vate;
		    	data['dealdate']=ddate;
				params=mini.encode(data);
				CommonUtil.ajax( {
					url:"/IfsOpicsAcupController/syncNbh",
					data:params,
					callback : function(data) {
						mini.alert(data.obj.retMsg,"系统提示");
						search(10,0);
					}
				});
			}
		});
	}
	
	// 刷新OPICS处理状态
	function refresh(){
		var rows=grid.getSelecteds();
		if(rows.length==0){
			mini.alert("请选中一行","提示");
			return;
		}
		var data=rows[0];
		params=mini.encode(data);
		CommonUtil.ajax( {
			url:"/IfsOpicsAcupController/refresh",
			data:params,
			callback : function(data) {
				if(data == "2"){
					mini.alert("OPICS处理成功");
				} else if(data == "3"){
					mini.alert("OPICS处理失败");
				}
				search(10,0);
			}
		});
	}
</script>
</body>
</html>