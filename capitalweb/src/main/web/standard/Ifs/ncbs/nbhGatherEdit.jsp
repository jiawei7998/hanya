<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%-- <%@ page language="java" pageEncoding="UTF-8"%> --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<%
	SlSession slSession = SessionStaticUtil.getSlSessionByHttp(request);
	TaUser sessionUser = SlSessionHelper.getUser(slSession)==null?new TaUser():SlSessionHelper.getUser(slSession);
%>

<html>
<head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>外汇头寸估值  维护</title>
</head>
<body style="width:100%;height:100%;background:white">
<div class="mini-splitter" style="width:100%;height:100%;">
	<div size="90%" showCollapseButton="false">
		<div id="field_form" class="mini-fit area"  style="background:white">
            <fieldset>
            <legend>内部户信息</legend>
            <div class="leftarea">
                <input id="br" name="br" class="mini-combobox" labelField="true" textField="typeValue" valueField="typeId"
					label="部门：" value="01" labelStyle="text-align:center;width:150px"
					emptyText="请选择部门" required="true"  style="width:100%"/>
				<input id="trad" name="trad" class="mini-textbox" labelField="true" label="交易员：" style="width:100%"
               		emptyText="请输入交易员" required="true"  labelStyle="text-align:center;width:150px" vtype="maxLength:4"/>
				<input id="product" name="product" onbuttonclick="onPrdEdit" allowInput="false"
               		class="mini-buttonedit mini-mustFill" labelField="true"  style="width:100%;"  label="产品代码：" required="true"
               		maxLength="10" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])"
               		labelStyle="text-align:center;width:150px;" emptyText="请输入产品代码"/>
               	<input id="port" name="port" class="mini-buttonedit mini-mustFill" onbuttonclick="onPortEdit"  style="width:100%;"
               		labelField="true" label="投资组合：" required="true"  allowInput="false" vtype="maxLength:4"
               		labelStyle="text-align:center;width:150px;" emptyText="请输入投资组合" value="FXPB" text="FXPB"/>
               	<input id="daeldate" name="dealdate" class="mini-datepicker" labelField="true" label="交易日期：" style="width:100%" value="<%=__bizDate%>"
               		emptyText="请选择交易日期" required="true"  labelStyle="text-align:center;width:150px" renderer="onDateRenderer"/>
               	<input id="dealtext" name="dealtext" class="mini-textbox" labelField="true" label="交易来源：" style="width:100%"
               		emptyText="请输入交易来源" required="true"  labelStyle="text-align:center;width:150px" value="NCBS"/>
				<input id="ccy" name="ccy" class="mini-combobox" labelField="true" label="外币币种：" style="width:100%"
               		emptyText="请选择外币币种" required="true"  labelStyle="text-align:center;width:150px"
               		data="CommonUtil.serverData.dictionary.Currency"/>
				<input id="ctrccy" name="ctrccy" class="mini-combobox" labelField="true" label="本币币种：" style="width:100%"
               		emptyText="请选择本币币种" required="true"  labelStyle="text-align:center;width:150px"
               		data="CommonUtil.serverData.dictionary.Currency" value="CNY" enabled="false"/>
               	
               	<!-- <input id="inoutind" name="inoutind" class="mini-textbox" labelField="true" label="inoutind：" style="width:100%"
               		emptyText="请输入inoutind" required="true"  labelStyle="text-align:center;width:150px" value="I" enabled="false"/> -->
               	<!-- <input id="brok" name="brok" class="mini-textbox" labelField="true" label="brok：" style="width:100%"
               		emptyText="请输入brok" required="true"  labelStyle="text-align:center;width:150px" value="D" enabled="false"/> -->
               	<!-- <input id="siind" name="siind" class="mini-textbox" labelField="true" label="siind：" style="width:100%"
               		emptyText="请输入siind" required="true"  labelStyle="text-align:center;width:150px" value="Y" enabled="false"/>
               	<input id="suppayind" name="suppayind" class="mini-textbox" labelField="true" label="suppayind：" style="width:100%"
               		emptyText="请输入suppayind" required="true"  labelStyle="text-align:center;width:150px" value="N" enabled="false"/> -->
               	
               <!-- 	<input id="authsi" name="authsi" class="mini-textbox" labelField="true" label="authsi：" style="width:100%"
               		emptyText="请输入authsi" required="true"  labelStyle="text-align:center;width:150px" value="Y" enabled="false"/> -->
			</div>			
			<div class="rightarea">
                <!-- <input id="server" name="server" class="mini-textbox" labelField="true" label="server：" style="width:100%"
                	emptyText="请输入server" labelStyle="text-align:center;width:150px" required="true"  value="SL_FUND_IFXD"/>
                <input id="seq" name="seq" class="mini-textbox" label="seq：" emptyText="请选择seq" required="true"
					labelField="true" style="width:100%" labelStyle="text-align:center;width:150px" value="0" enabled="false"/> -->
				<input id="fedealno" name="fedealno" class="mini-textbox" labelField="true" label="流水号：" style="width:100%"
               		emptyText="系统自动生成" required="true"  labelStyle="text-align:center;width:150px" vtype="maxLength:15" enabled="false"/>
               	<input id="cust" name="cust" class="mini-buttonedit" labelField="true" label="交易对手：" style="width:100%" onbuttonclick="onButtonEdit"
               		emptyText="请输入交易对手" required="true"  labelStyle="text-align:center;width:150px" vtype="maxLength:15" allowInput="false"/>
               	<input id="prodtype" name="prodtype" onbuttonclick="onTypeEdit" allowInput="false"
               		class="mini-buttonedit" labelField="true" style="width:100%;" label="产品类型：" required="true"
               		vtype="maxLength:2" labelStyle="text-align:center;width:150px;" emptyText="请输入产品类型"/>
               	<input id="cost" name="cost" onbuttonclick="onCostEdit" allowInput="false" class="mini-buttonedit mini-mustFill"
               		labelField="true" label="成本中心：" style="width:100%;" required="true"  vtype="maxLength:15"
               		labelStyle="text-align:center;width:150px;" emptyText="请输入成本中心" value="1020113000" text="1020113000"/>	
               	<input id="vdate" name="vdate" class="mini-datepicker" labelField="true" label="起息日：" style="width:100%" value="<%=__bizDate%>"
               		emptyText="请选择起息日" required="true"  labelStyle="text-align:center;width:150px" renderer="onDateRenderer"/>
               	<input id="ps" name="ps" class="mini-combobox" labelField="true" label="交易方向：" style="width:100%"
               		emptyText="请输入交易方向" required="true"  labelStyle="text-align:center;width:150px" vtype="maxLength:1"
               		data="CommonUtil.serverData.dictionary.trading"/>
               	<input id="ccyamt" name="ccyamt" class="mini-spinner" labelField="true" label="外币金额：" style="width:100%"
               		emptyText="请输入外币金额" required="true"  labelStyle="text-align:center;width:150px" maxValue="999999999999"
               		format="n4"/>
               	<input id="ctramt" name="ctramt" class="mini-spinner" labelField="true" label="本币金额：" style="width:100%"
               		emptyText="请输入本币金额" required="true"  labelStyle="text-align:center;width:150px" maxValue="999999999999"
               		format="n4"/>
               	
               	<!-- <input id="verind" name="verind" class="mini-textbox" labelField="true" label="verind：" style="width:100%"
               		emptyText="请输入verind" required="true"  labelStyle="text-align:center;width:150px" value="Y" enabled="false"/>
               	<input id="suprecind" name="suprecind" class="mini-textbox" labelField="true" label="suprecind：" style="width:100%"
               		emptyText="请输入suprecind" required="true"  labelStyle="text-align:center;width:150px" value="N" enabled="false"/> -->
               	
               <!-- 	<input id="supconfind" name="supconfind" class="mini-textbox" labelField="true" label="supconfind：" 
               		style="width:100%" emptyText="请输入supconfind" required="true"  labelStyle="text-align:center;width:150px"
               		value="N" enabled="false"/> -->
			</div>			
            </fieldset>  
		</div>
	</div>		
    <div id="functionIds"  style="padding-top:30px;text-align:center;">
        <div style="margin-bottom:10px; text-align: center;">
        	<a class="mini-button" style="display: none"  style="width:120px;" id="save_btn" onclick="save">保存</a>
        </div>
        <div style="margin-bottom:10px; text-align: center;">
        	<a class="mini-button" style="display: none"  style="width:120px;" id="close_btn" onclick="close">关闭</a>
        </div>
    </div> 
</div>
<script type="text/javascript">
    mini.parse();
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var row=params.selectData;
    
    var url=window.location.search;
    var action=CommonUtil.getParam(url,"action");
    var form = new mini.Form("#field_form");
    
    // 加载日期格式
    function onDateRenderer(e) {
		var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)==null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g,'/'));
		if (value) return mini.formatDate(value, 'yyyy-MM-dd');
	}
    
    // 初始化数据
    $(document).ready(function(){
    	var trad ='<%=sessionUser.getEmpId()%>'; // 交易员
    	mini.get("trad").setValue(trad);
    	var jsonData = new Array();
		jsonData.add({'typeId':"01",'typeValue':"01"});
	   	mini.get("br").setData(jsonData);
    	if($.inArray(action,["edit","detail"])>-1){ // 修改
    		mini.get("fedealno").setEnabled(false);
        	form.setData(row);
        	//投资组合
        	mini.get("port").setValue(row.port);
			mini.get("port").setText(row.port);
			//成本中心
			mini.get("cost").setValue(row.cost);
			mini.get("cost").setText(row.cost);
			//产品代码
			mini.get("product").setValue(row.product);
			mini.get("product").setText(row.product);
			//产品类型
			mini.get("prodtype").setValue(row.prodtype);
			mini.get("prodtype").setText(row.prodtype);
			//交易对手
			mini.get("cust").setValue(row.cust);
			mini.get("cust").setText(row.cust);
			
			
			
            if(action == "detail"){ // 详情
            	mini.get("save_btn").setVisible(false);
                $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>外汇头寸估值</a> >> 详情");
                form.setEnabled(false);
            }
		}else{ // 增加 
			$("#labell").html("<a href='javascript:CommonUtil.activeTab();'>外汇头寸估值</a> >> 新增");
        }
	});
  	
  	// 保存
	function save(){
  		var ccyamt = mini.get("ccyamt").getValue();
  		var ctramt = mini.get("ctramt").getValue();
  		if(ccyamt == "0"){
  			mini.alert("外币金额不能为0");
  			return;
  		}
  		if(ctramt == "0"){
  			mini.alert("本币金额不能为0");
  			return;
  		}
  		
	    // 表单验证！！！
	    form.validate();
	    if (form.isValid() == false) {
	        mini.alert("表单填写有误!","提示");
	        return;
	    }
	    var saveUrl = null;
	    if(action == "add"){
	        saveUrl = "/IfsOpicsAcupController/nbhGatherAdd";
	    }else if(action == "edit"){
	        saveUrl = "/IfsOpicsAcupController/nbhGatherEdit";
	    }
	    var data = form.getData(true);
	    var params = mini.encode(data);
	    CommonUtil.ajax({
	        url:saveUrl,
	        data:params,
	        callback:function(data){
	            mini.alert("保存成功",'提示信息',function(){
	                top["win"].closeMenuTab();
	            });
	        }
	    });
	}
  	
  	// 关闭
	function close(){
        top["win"].closeMenuTab();
    }
  	
	/* 产品代码的选择 */
    function onPrdEdit(){
    	var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "/opics/prodMiniLess.jsp",
            title: "选择产品代码",
            width: 900,
            height: 500,
            onload: function () {
                var iframe = this.getIFrameEl();
                iframe.contentWindow.SetData({});
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.pcode);
                        btnEdit.setText(data.pcode);
                        onPrdChanged();
                        btnEdit.focus();
                    }
                }

            }
        });
    }
    function onPrdChanged(){
    	if(mini.get("prodType").getValue()!=""){
    		mini.get("prodType").setValue("");
        	mini.get("prodType").setText("");
        	mini.alert("产品代码已改变，请重新选择产品类型!");
    	}
    }
	
    /* 产品类型的选择 */
    function onTypeEdit(){
    	var prd = mini.get("product").getValue();
    	if(prd == null || prd == ""){
    		mini.alert("请先选择产品代码!");
    		return;
    	}
    	var data={prd:prd};
    	var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "/opics/typeMiniLess.jsp",
            title: "选择产品类型",
            width: 900,
            height: 500,
            onload: function () {
                var iframe = this.getIFrameEl();
                iframe.contentWindow.SetData(data);
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.type);
                        btnEdit.setText(data.type);
                        btnEdit.focus();
                    }
                }
            }
        });
    }
    
    /* 成本中心的选择 */
    function onCostEdit(){
    	var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "/opics/costMiniLess.jsp",
            title: "选择成本中心",
            width: 900,
            height: 500,
            onload: function () {
                var iframe = this.getIFrameEl();
                iframe.contentWindow.SetData({});
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.costcent);
                        btnEdit.setText(data.costcent);
                        btnEdit.focus();
                    }
                }
            }
        });
    }
    
    /* 投资组合的选择 */
    function onPortEdit(){
 	   var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "/opics/portMiniLess.jsp",
            title: "选择投资组合",
            width: 900,
            height: 500,
            onload: function () {
                var iframe = this.getIFrameEl();
                iframe.contentWindow.SetData({});
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.portfolio);
                        btnEdit.setText(data.portfolio);
                        btnEdit.focus();
                    }
                }
            }
        });
     }
    function onButtonEdit(e) {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Common/CustMini.jsp",
            title: "选择对手方列表",
            width: 900,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.cno);
                        btnEdit.setText(data.cno);
                        btnEdit.focus();
                    }
                }

            }
        });
    }
</script>
</body>
</html>