<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/hideBtn.js"></script>
    <title>内部户</title>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
	<legend>内部户查询</legend>	
	<div>
		<div id="search_form" style="width:100%" cols="6">
			<input id="ccy" name="ccy" class="mini-combobox" labelField="true" label="币种：" width="320px"
				labelStyle="text-align:center;width:140px" emptyText="请选择币种" data="CommonUtil.serverData.dictionary.Currency"/>
			<input id="ccyacct" name="ccyacct" class="mini-textbox" labelField="true" label="核心内部户：" width="320px"
				labelStyle="text-align:center;width:140px" emptyText="请输入核心内部户"/>
			<span style="float: right; margin-right: 50px"> 
				<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
			</span>
		</div>
	</div>
</fieldset>

<span style="margin: 2px; display: block;"> 
	<a  id="add_btn" class="mini-button" style="display: none"   onclick="add()">新增</a>
	<a  id="edit_btn" class="mini-button" style="display: none"   onclick="edit()">修改</a>
	<a  id="delete_btn" class="mini-button" style="display: none"   onclick="del()">删除</a>
</span>  

<div class="mini-fit" >
	<div id="acct_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id"  allowAlternating="true"
		onrowdblclick="onRowDblClick" allowResize="true" sortMode="client" allowAlternating="true">
		<div property="columns">
			<div type="indexcolumn" width="30px" headerAlign="center">序号</div>
			<div field="ccy" width="150px" align="center" headerAlign="center" renderer="CommonUtil.dictRenderer" 
				 data-options="{dict:'Currency'}">币种</div>
			<div field="ccyacct" width="180px" align="center" headerAlign="center" >核心内部户</div>    
			<div field="ioper" width="150" headerAlign="center" align="center" >经办人</div>
			<div field="idate" width="100" headerAlign="center" align="center" renderer="onDateRenderer">经办日期</div>
		</div>
	</div>
</div>   

<script>
	mini.parse();
	var grid = mini.get("acct_grid");
	var form = new mini.Form("#search_form");
	
	function onDateRenderer(e) {
		var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)==null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g,'/'));
		if (value) return mini.formatDate(value, 'yyyy-MM-dd');
	}
	
	$(document).ready(function() {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
			search(10, 0);
		});
	});
	
	/* 查询 */
	function search(pageSize,pageIndex){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统提示");
			return;
		}
		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:"/IfsOpicsAcupController/searchData",
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	
	// 添加
	function add(){
		var url = CommonUtil.baseWebPath() +"/../Ifs/ncbs/ncbsAcctEdit.jsp?action=add";
		var tab = {id: "nbhAdd",name:"nbhAdd",url:url,title:"内部户新增",
					parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
		var paramData = {selectData:""};
		CommonUtil.openNewMenuTab(tab,paramData);
	}
		
	// 修改
	function edit(){
		var row=grid.getSelected();
		if(row){
			var url = CommonUtil.baseWebPath() +"/../Ifs/ncbs/ncbsAcctEdit.jsp?action=edit";
			var tab = {id: "nbhEdit",name:"nbhEdit",url:url,title:"内部户修改",
						parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
			var paramData = {selectData:row};
			CommonUtil.openNewMenuTab(tab,paramData);
		}else{
			mini.alert("请选择一条记录","提示");
		}
	}
	
	// 删除
	function del(){
		var rows=grid.getSelecteds();
		if(rows.length==0){
			mini.alert("请选中一行","提示");
			return;
		}
		mini.confirm("您确认要删除该内部户信息?","系统警告",function(value){   
			if (value=='ok'){   
				var data=rows[0];
				params=mini.encode(data);
				CommonUtil.ajax( {
					url:"/IfsOpicsAcupController/deleteNbh",
					data:params,
					callback : function(data) {
						mini.alert("删除成功.","系统提示");
						search(10,0);
					}
				});
			}
		});
	}

	//查看详情
	function onRowDblClick(e) {
		var url = CommonUtil.baseWebPath() +"/../Ifs/ncbs/ncbsAcctEdit.jsp?action=detail";
		var tab = {id: "CustDetail",name:"CustDetail",url:url,title:"内部户详情",
					parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
		var paramData = {selectData:grid.getSelected()};
		CommonUtil.openNewMenuTab(tab,paramData);
	}
	
	//清空
	function clear(){
		var form=new mini.Form("search_form");
		form.clear();
		query();
	}
	
	/* 按钮 查询事件 */
	function query(){
		search(grid.pageSize,0);
	}
</script>
</body>
</html>