<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%-- <%@ page language="java" pageEncoding="UTF-8"%> --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<%
	SlSession slSession = SessionStaticUtil.getSlSessionByHttp(request);
	TaUser sessionUser = SlSessionHelper.getUser(slSession)==null?new TaUser():SlSessionHelper.getUser(slSession);
%>

<html>
<head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>内部户  维护</title>
</head>
<body style="width:100%;height:100%;background:white">
<div class="mini-splitter" style="width:100%;height:100%;">
	<div size="90%" showCollapseButton="false">
		<div id="field_form" class="mini-fit area"  style="background:white">
            <fieldset>
            <legend>内部户信息</legend>
            <div class="leftarea">
                <input id="ccy" name="ccy" class="mini-combobox" labelField="true" label="币种：" 
                	emptyText="请选择币种" required="true"  data="CommonUtil.serverData.dictionary.Currency"
                	style="width:100%" labelStyle="text-align:center;width:150px"/>
               	<input id="ioper" name="ioper" class="mini-textbox" labelField="true" label="经办人：" style="width:100%"
               		emptyText="请输入经办人" required="true"  labelStyle="text-align:center;width:150px"/>
			</div>			
			<div class="rightarea">
                <input id="ccyacct" name="ccyacct" class="mini-textbox" labelField="true" label="核心内部户：" style="width:100%"
                	emptyText="请输入核心内部户" labelStyle="text-align:center;width:150px" required="true" />
                <input id="idate" name="idate" class="mini-datepicker" label="经办日期：" emptyText="请选择经办日期" required="true"
					labelField="true" style="width:100%" labelStyle="text-align:center;width:150px"  renderer="onDateRenderer"/>
			</div>			
            </fieldset>  
		</div>
	</div>		
    <div id="functionIds"  style="padding-top:30px;text-align:center;">
        <div style="margin-bottom:10px; text-align: center;">
        	<a class="mini-button" style="display: none"  style="width:120px;" id="save_btn" onclick="save">保存</a>
        </div>
        <div style="margin-bottom:10px; text-align: center;">
        	<a class="mini-button" style="display: none"  style="width:120px;" id="close_btn" onclick="close">关闭</a>
        </div>
    </div> 
</div>
<script type="text/javascript">
    mini.parse();
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var row=params.selectData;
    
    var url=window.location.search;
    var action=CommonUtil.getParam(url,"action");
    var form = new mini.Form("#field_form");
    
    // 加载日期格式
    function onDateRenderer(e) {
		var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)==null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g,'/'));
		if (value) return mini.formatDate(value, 'yyyy-MM-dd');
	}
    
    // 初始化数据
    $(document).ready(function(){
    	if($.inArray(action,["edit","detail"])>-1){ // 修改
    		mini.get("ccy").setEnabled(false);
        	form.setData(row);
            if(action == "detail"){ // 详情
            	mini.get("save_btn").setVisible(false);
                $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>内部户</a> >> 详情");
                form.setEnabled(false);
            }
		}else{ // 增加 
			$("#labell").html("<a href='javascript:CommonUtil.activeTab();'>内部户</a> >> 新增");
        }
	});
  	
  	// 保存
	function save(){
	    // 表单验证！！！
	    form.validate();
	    if (form.isValid() == false) {
	        mini.alert("表单填写有误!","提示");
	        return;
	    }
	    var saveUrl = null;
	    if(action == "add"){
	        saveUrl = "/IfsOpicsAcupController/nbhAdd";
	    }else if(action == "edit"){
	        saveUrl = "/IfsOpicsAcupController/nbhEdit";
	    }
	    var data = form.getData(true);
	    var params = mini.encode(data);
	    CommonUtil.ajax({
	        url:saveUrl,
	        data:params,
	        callback:function(data){
	            mini.alert("保存成功",'提示信息',function(){
	                top["win"].closeMenuTab();
	            });
	        }
	    });
	}
  	
  	// 关闭
	function close(){
        top["win"].closeMenuTab();
    }
</script>
</body>
</html>