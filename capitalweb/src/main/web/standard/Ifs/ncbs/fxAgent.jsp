<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>外汇代客</title>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
	<legend>外汇代客查询</legend>	
	<div>
		<div id="search_form" style="width:100%" cols="6">
		  <input id='vDate' name='vDate' class='mini-datepicker' style='width:300px' labelField='true' label='起息日：' labelStyle='text-align:right'/>
			<input id="busType" name="busType" class="mini-combobox" labelField="true" label="业务类型：" width="320px" data="CommonUtil.serverData.dictionary.busType"
				labelStyle="text-align:center;width:140px" emptyText="请选择业务类型"/>
			<input id="dealStatus" name="dealStatus" class="mini-combobox" labelField="true" label="交易状态：" width="320px" data="CommonUtil.serverData.dictionary.dealStatus"
				labelStyle="text-align:center;width:140px" emptyText="请选择交易状态"/>
			<input id="syncStatus" name="syncStatus" class="mini-combobox" data="CommonUtil.serverData.dictionary.DealStatus" width="280px" 
				emptyText="请选择同步状态" labelField="true"  label="同步状态：" labelStyle="text-align:right;"/>
			<input id="fedealno" name="fedealno" class="mini-textbox" labelField="true" label="导入opics流水号：" emptyText="请填写导入opics流水号" 
				labelStyle="text-align:right;" width="280px"/>
			<span style="float: right; margin-right: 50px"> 
				<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
			</span>
			<p style="color: red;">规则:晚上六点以后或者币种2按币种合计金额达到一定金额数量(字典AMTFLAG 可配置)才进行同步</p>
		</div>
	</div>
</fieldset>

<div class="mini-fit" >
	<div id="acct_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" allowAlternating="true"
		onrowdblclick="onRowDblClick" allowResize="true" sortMode="client" allowAlternating="true" border="true">
		<div property="columns">
			<div type="indexcolumn" width="30px" headerAlign="center">序号</div>
			<div field="busType" width="160px" align="left" headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'busType'}">业务类型</div>
			<div field="bankTellerNo" width="160" headerAlign="center" align="left">柜员流水</div>
			<div field="dealStatus" width="100"  headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'dealStatus'}">交易状态</div>
			<div field="orgNo" width="100px" align="center"  headerAlign="center" >机构号</div>
			<div field="dealDate" width="100" headerAlign="center" align="center" renderer="onDateRenderer" allowSort="true">成交日期</div>
			<div field="vDate" width="100" headerAlign="center" align="center" renderer="onDateRenderer" allowSort="true">起息日期</div>
			<div field="currencyPair" width="120" headerAlign="center" align="center">货币对</div>
			<div field="direction" width="80" headerAlign="center" align="center" allowSort="true" renderer="CommonUtil.dictRenderer"
				 data-options="{dict:'trading'}">买卖方向</div>
			<div field="ccy1" width="100px" align="center"  headerAlign="center" >币种1</div>
			<div field="ccy2" width="100px" align="center"  headerAlign="center" >币种2</div>
			<div field="ccy1Amt" width="150" headerAlign="center" align="right" numberFormat="#,0.0000">币种1金额</div>
			<div field="ccy2Amt" width="150" headerAlign="center" align="right" numberFormat="#,0.0000">币种2金额</div>
			<div field="syncStatus" width="150" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'DealStatus'}">同步状态</div>
			<div field="fedealno" width="150px" align="center"  headerAlign="center" >导入opics流水号</div>
			<div field="dealno" width="150px" align="center"  headerAlign="center" >dealno号</div>
		</div>
	</div>
</div>   

<script>
	mini.parse();
	var grid = mini.get("acct_grid");
	var form = new mini.Form("#search_form");
	
	// 加载日期格式
    function onDateRenderer(e) {
		var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)==null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g,'/'));
		if (value) return mini.formatDate(value, 'yyyy-MM-dd');
	}
	
	grid.on("beforeload", function(e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex;
		var pageSize = e.data.pageSize;
		search(pageSize, pageIndex);
	});
	
	
	$(document).ready(function(){
		mini.get('vDate').setValue(sysDate);
		search(10, 0);
	});
	
	/* 查询 */
	function search(pageSize,pageIndex){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统提示");
			return;
		}
		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:"/IfsForeignccyDishController/searchPageDish",
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	function query(){
		search(10, 0);
	}
	
	function clear(){
        form.clear();
        search(10,0);
	}
	
</script>
</body>
</html>