<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ include file="../../global.jsp"%>
<script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
<title></title>
</head>
<body style="width:100%;height:100%;background:white">
	<div class="mini-splitter" style="width:100%;height:100%;">
	<div size="90%" showCollapseButton="false">
	<h1 style="text-align:center"><strong>黄金拆借</strong></h1>
	<div  id="field_form"  class="mini-fit area" style="background:white" >
		<input id="sponsor" name="sponsor" class="mini-hidden" />
		<input id="sponInst" name="sponInst" class="mini-hidden" />
		<input id="aDate" name="aDate" class="mini-hidden"/>
		<input id="postDate" name="postDate" class="mini-datepicker"  labelField="true"  width="30%" style="font-size:18px;float:left;margin-left: 100px;"  label="报送日期："  required="true"  labelStyle="text-align:left;" />
		<input id="ticketId" name="ticketId" class="mini-textbox"  labelField="true" width="30%" labelField="true" style="font-size:18px; margin-left: 25%;" enabled="false" label="审批单号：" labelStyle="text-align:left;width:120px" emptyText="由系统自动生成"/> 		
		<div class="centerarea">
	  		<fieldset>
	  			<legend>成交单明细</legend>
	  				<input id="direction" name="direction" class="mini-combobox"  labelField="true" label="交易方向：" labelStyle="text-align:left;width:110px;" data = "CommonUtil.serverData.dictionary.trading" />
					<input id="counterpartyInstId"  name="counterpartyInstId" label="交易对手 ：" onbuttonclick="onButtonEdit" class="mini-buttonedit" labelField="true" labelStyle="text-align:left;width:110px;"/>
					<input id="standardWeight" name="standardWeight" class="mini-spinner" maxValue="99999999999999999.9999" format="n4" labelField="true"  label="拆借标重（千克）："  labelStyle="text-align:left;width:110px;" />
					<input id="annualRate" name="annualRate" class="mini-spinner" format="n8" maxValue="99999999999999999999999.999999999999"  changeOnMousewheel="false" labelField="true"  label="拆借年利率（%）："  labelStyle="text-align:left;width:110px;"  />
					<input id="finalDate" name="finalDate" class="mini-datepicker" labelField="true"  label="拆借到期日："  labelStyle="text-align:left;width:110px;"/>
					<input id="tradingVariety" name="tradingVariety" class="mini-combobox" labelField="true"  label="交易品种："  data="CommonUtil.serverData.dictionary.tradeGoods" labelStyle="text-align:left;width:110px;"/>
					<input id="timeLimit" name="timeLimit" class="mini-combobox" labelField="true"  label="拆借期限："   data="CommonUtil.serverData.dictionary.TimeLimit" labelStyle="text-align:left;width:110px;"/>
					<input id="valueDate" name="valueDate" class="mini-datepicker" labelField="true"  label="拆借起息日："    labelStyle="text-align:left;width:110px;" />
					<input id="interestDay" name="interestDay" class="mini-datepicker" labelField="true"  label="拆借付息日："    labelStyle="text-align:left;width:110px;"/>
					
	 		</fieldset>
		</div>  
		<%@ include file="../../Common/opicsApprove.jsp"%>
	  	<%@ include file="../../Common/Flow/MiniApproveOpCommon.jsp"%>
	</div>
	</div>
	<div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
		<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="save_btn"   onclick="save">保存交易</a></div>
		<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="close_btn"   onclick="close">关闭界面</a></div>
	</div>
	</div>

	<script type="text/javascript">
	mini.parse();
	//获取当前tab
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var row=params.selectData;
	var url = window.location.search;
	var action = CommonUtil.getParam(url, "action");
	var ticketId=CommonUtil.getParam(url, "ticketid");
	
	var tradeData={};
	
	tradeData.selectData=row;
	tradeData.operType=action;
	tradeData.serial_no=row.ticketId;
	tradeData.task_id=row.taskId;
	function inme(){
		if(action=="detail"||action=="edit"||action=="approve"){
		var from = new mini.Form("field_form");
		
		//投资组合
		mini.get("port").setValue(row.port);
		mini.get("port").setText(row.port);
		//对方结构
		mini.get("counterpartyInstId").setValue(row.cno);
		mini.get("counterpartyInstId").setText(row.counterpartyInstId);
		CommonUtil.ajax({
			url:'/IfsApproveGoldLendController/searchGoldLend',
			data:{ticketId:ticketId},
			callback:function(text){
				from.setData(text.obj);
			}
		});
		if(action=="detail"||action=="approve"){
			mini.get("save_btn").hide();
			from.setEnabled(false);
			var form11=new mini.Form("#approve_operate_form");
		      form11.setEnabled(true);
		}
		}else if(action=="add"){
			mini.get("sponsor").setValue("<%=__sessionUser.getUserId()%>");
			mini.get("sponInst").setValue("<%=__sessionUser.getInstId() %>");
			mini.get("postDate").setValue(new Date());
		}
	}
	
	function save(){
		var form = new mini.Form("field_form");
		form.validate();
		if (form.isValid() == false) {
			return;
		}
		var form = new mini.Form("field_form");            
		var data = form.getData(true);      //获取表单多个控件的数据
		data['sponInst']="<%=__sessionInstitution.getInstId()%>";
		data['dealTransType']="0";
		var json = mini.encode(data);   //序列化成JSON
		if(action=="add"){
			CommonUtil.ajax({
			    url: "/IfsApproveGoldLendController/addGoldLend",
			    data:json,
			    callback:function (data) {
			    	mini.alert("保存成功",'提示信息',function(){
						top["win"].closeMenuTab();
					});
			    }
			});
			}else  if(action="edit"){
				CommonUtil.ajax({
					url:"/IfsApproveGoldLendController/editGoldLend",
					data:json,
					callback:function(data){
						if (data.code == 'error.common.0000') {
							mini.alert(data.desc,'提示信息',function(){
								top["win"].closeMenuTab();
							});
						} else {
							mini.alert("保存失败");
				    }
					}
				});
			}
	}
	function close(){
		top["win"].closeMenuTab();
	}
	$(document).ready(function () {
		inme();
	});
	
	
	//文、数字、下划线 的验证
	function onEnglishAndNumberValidations(e) {
		if(e.value == "" || e.value == null){//值为空，就不做校验
			return;
		}
		if (e.isValid) {
			if (isEnglishAndNumbers(e.value) == false) {
				e.errorText = "必须输入英文小写+数字";
				e.isValid = false;
			}
		}
	}
	/* 是否英文+数字 */
	function isEnglishAndNumbers(v) {
		var re = new RegExp("^[0-9a-z\_\-]+$");
		if (re.test(v)) return true;
		return false;
	}	
	 /* 是否英文+数字 */
        function isEnglishAndNumber(v) {
            
            var re = new RegExp("^[0-9a-z\_]+$");
            if (re.test(v)) return true;
            return false;
        }
	
	   function onEnglishAndNumberValidation(e) {
           if (e.isValid) {
               if (isEnglishAndNumber(e.value) == false) {
                   e.errorText = "必须输入英文小写+数字";
                   e.isValid = false;
               }
           }
       }
	   function onButtonEdit(e) {
	        var btnEdit = this;
	        mini.open({
	            url: CommonUtil.baseWebPath() + "../../Common/CustMini.jsp",
	            title: "选择对手方列表",
	            width: 900,
	            height: 600,
	            ondestroy: function (action) {
	                if (action == "ok") {
	                    var iframe = this.getIFrameEl();
	                    var data = iframe.contentWindow.GetData();
	                    data = mini.clone(data);    //必须
	                    if (data) {
	                        btnEdit.setValue(data.cno);
	                        btnEdit.setText(data.cfn);
	                        btnEdit.focus();
	                    }
	                }

	            }
	        });
	    }
	   function  dirVerify(e){
		    if(e.value == "1"){//买入
		        mini.get("direction").setValue("2");
		    }else if(e.value == "2"){//卖出
		        mini.get("direction").setValue("1");
		    }

		}
	</script>
			<script type="text/javascript" src="../../Common/Flow/MiniApproveOpCommon.js"></script>			
	
</body>
</html>