<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../../global.jsp"%>
<html>
    <head>
        <title>债券收盘价信息</title>
        <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    </head>
<body style="width:100%;height:100%;background:white">
   <fieldset class="mini-fieldset title">
    <legend>债券收盘价查询</legend>
    <div id="search_form" style="width: 100%;">
<%--        <input id="fileTime" name="fileTime" class="mini-textbox" labelField="true" label="牌价时间：" labelStyle="text-align:right;" emptyText="请输入牌价时间" />--%>
        <input id="secid" name="secid" class="mini-textbox" width="320px" labelField="true" label="债券代码：" labelStyle="text-align:right;" emptyText="请输入债券代码" />
        <input id="lstmntdte" name="lstmntdte" class="mini-datepicker" width="320px" labelField="true" label="日期：" emptyText="请填写日期" labelStyle="text-align:right;"/>
        <span style="float:right;margin-right: 150px">
            <a class="mini-button" style="display: none"  id="search_btn"  onclick="search()">查询</a>
            <a class="mini-button" style="display: none"  id="clear_btn"  onclick="clear()">清空</a>
            <a class="mini-button" style="display: none"  id="upload_btn"  onclick="ExcelImport()">上传市场数据</a>
           <!-- <a class="mini-button" style="display: none"  id="get_btn"  onclick="get()">获取牌价</a>-->
        </span>
    </div>
    </fieldset>
    
    <div class="mini-fit" style="width:100%;height:100%;">
        <div id="data_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" 
            sortMode="client" allowAlternating="true"   idField="userId" onrowdblclick="onRowDblClick" 
            border="true" allowResize="true" >
            <div property="columns">
                <div type="indexcolumn" headerAlign="center" align="center" width="40px">序列</div>
                <div field="br" headerAlign="center"  align="center" width="100px">分支机构</div>
                <div field="secid" headerAlign="center"  width="150px">债券代码</div>
                <div field="clsgprice_8" headerAlign="center"  width="150px"> 估价净价</div>
                <div field="askprice_8" headerAlign="center"  width="150px">买入价格</div>
                <div field="bidprice_8" headerAlign="center" width="150px">卖出价格</div> -->
                <div field="descr" headerAlign="center" width="150px">债券简称</div> -->
                <div field="remark" headerAlign="center" width="200px" >备注</div>
                <div field="lstmntdte" headerAlign="center" width="150px" >收盘价日期</div>
             </div>
        </div>
    </div>
</body>
<script>
    mini.parse();

    var grid = mini.get("data_grid");
    grid.on("beforeload", function(e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        searchs(pageSize, pageIndex);
    });

    $(document).ready(function() {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
            search();
        });
	});


    function onDateRenderer(e) {
        var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)==null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g,'/'));
        if (value) return mini.formatDate(value, 'yyyy-MM-dd');
    }
    
    
//    function get(){
//    
//        CommonUtil.ajax({
//            url : "/IfsGjPriceController/getPrice",
//            data : {'cno' : 1},
//            callback : function(data) {
//             if(date.code=="error.common.0000") {
//                 mini.alert("发送成功");
//                 search();
//             }else
//             {
//                 mini.alert("发送失败");
//             }
//            }
//        });
//    }
    
    
	function setData(data){
    	var form = new mini.Form("#search_form");
    	form.setData(data);
    }
    
    function searchs(pageSize, pageIndex) {
        var form = new mini.Form("#search_form");
        form.validate();
        if (form.isValid() == false) {
            mini.alert("表单填写错误,请确认!", "提示信息");
            return;
        }

        var data = form.getData();
        
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        data['branchId']=branchId;
        var params = mini.encode(data);
        CommonUtil.ajax({
            url : "/IfsIntfcIselController/searchIsel",
            data : params,
            callback : function(data) {
                var grid = mini.get("data_grid");
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }
  //查询按钮
    function search(){
        searchs(grid.pageSize,0);
    }
    //清空
    function clear(){
        var form=new mini.Form("search_form");
        form.clear();
        search();
    }
    
  	//市场数据文件导入
	function ExcelImport(){
		mini.open({
			showModal: true,
			allowResize: true,
			url: CommonUtil.baseWebPath() +"/../Ifs/marketdata/isel/IntfcIselImport.jsp",
			width: 420,
			height: 300,
			title: "Excel导入",
			ondestroy: function (action) {
				searchs(10,0);//重新加载页面
			}
		});
	}

 
    
    function GetData() {
        var grid = mini.get("data_grid");
        var row = grid.getSelected();
        return row;
    }
    
   
    function CloseWindow(action) {
        if (window.CloseOwnerWindow)
            return window.CloseOwnerWindow(action);
        else
            window.close();
    }

    function onOk() {
        CloseWindow("ok");
    }

    //关闭窗口
    function onCancel() {
        CloseWindow("cancel");
    }
    
</script>
</html>