<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
	<script type="text/javascript" src="<%=basePath%>/standard/Common/RiskCenter.js"></script>
	<script type="text/javascript" src="<%=basePath%>/standard/Ifs/cfetsrmb/rmbVerify.js"></script>
    <title>外币债  维护</title>
    <script type="text/javascript" >
	/**prdCode用于页面投资组合的选择   该全局变量必须为prdCode*/
	var prdCode="SECUR-WB";
	</script>
  </head>

<body style="width:100%;height:100%;background:white">
<div class="mini-splitter" style="width:100%;height:100%;">
		<div size="90%" showCollapseButton="false">
		<h1 style="text-align:center"><strong>外币债</strong></h1>
		<div id="field_form" class="mini-fit area"  style="background:white">
		<input id="sponsor" name="sponsor" class="mini-hidden" />
		<input id="sponInst" name="sponInst" class="mini-hidden" />
		<input id="aDate" name="aDate" class="mini-hidden"/>
		<input id="ticketId" name="ticketId" class="mini-hidden" />
		
		<div class="mini-panel" title="成交单编号" style="width:100%;"  allowResize="true" collapseOnTitleClick="true">
			<div class="leftarea">
				<input style="width:100%;" id="contractId" name="contractId" class="mini-textbox mini-mustFill" labelField="true" requiredErrorText="该输入项为必输项"  label="成交单编号：" labelStyle="text-align:left;width:130px;" required="true"  vtype="maxLength:20"/>
			</div>
			
		</div>
		<div class="mini-panel" title="成交双方信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
		<div class="leftarea">
				<fieldset>
					<legend>本方信息</legend>
					<input id="myDir" name="myDir" class="mini-combobox mini-mustFill"  labelField="true"  label="本方方向："  required="true"  onvaluechanged="onCurrencyPairChange"  style="width:100%;"  labelStyle="text-align:left;width:130px;" data = "CommonUtil.serverData.dictionary.trading" />
					<input id="buyInst" name="buyInst" class="mini-textbox mini-mustFill"  labelField="true"  label="机构：" required="true"   style="width:100%;" enabled="false" labelStyle="text-align:left;width:130px;"/>
					<input id="buyTrader" name="buyTrader" class="mini-textbox mini-mustFill" labelField="true"  label="交易员："  required="true"  vtype="maxLength:5" enabled="false" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
					<!-- <input id="buyTel" name="buyTel" class="mini-textbox" labelField="true"  label="电话："  onvalidation="onEnglishAndNumberValidation" vtype="maxLength:20" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
					<input id="buyFax" name="buyFax" class="mini-textbox" labelField="true"  label="传真："  onvalidation="onEnglishAndNumberValidation" vtype="maxLength:20" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
					<input id="buyCorp" name="buyCorp" class="mini-textbox" labelField="true"  label="法人代表："  vtype="maxLength:5" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
					<input id="buyAddr" name="buyAddr" class="mini-textbox" labelField="true"  label="地址："  vtype="maxLength:50" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
				 -->
				</fieldset>
			</div>			
			<div class="leftarea">
				<fieldset>
					<legend>对手方信息</legend>
					<input id="oppoDir" name="oppoDir" class="mini-combobox mini-mustFill"  labelField="true"  label="对方方向："   required="true"  enabled="false" style="width:100%;"  labelStyle="text-align:left;width:130px;" data = "CommonUtil.serverData.dictionary.trading" />
					<input id="sellInst" name="sellInst" class="mini-buttonedit mini-mustFill" onbuttonclick="onButtonEdit"  labelField="true"  label="机构：" allowInput="false" required="true"  style="width:100%;"  labelStyle="text-align:left;width:130px;" />
					<input id="sellTrader" name="sellTrader" class="mini-textbox mini-mustFill" labelField="true"  label="交易员：" vtype="maxLength:30" labelStyle="text-align:left;width:130px;" style="width:100%;" required="true"   />
					<!-- <input id="sellTel" name="sellTel" class="mini-textbox" labelField="true"  label="电话："  onvalidation="onEnglishAndNumberValidation" vtype="maxLength:20" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
					<input id="sellFax" name="sellFax" class="mini-textbox" labelField="true"  label="传真：" onvalidation="onEnglishAndNumberValidation" vtype="maxLength:20" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
					<input id="sellCorp" name="sellCorp" class="mini-textbox" labelField="true"  label="法人代表："  vtype="maxLength:5" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
					<input id="sellAddr" name="sellAddr" class="mini-textbox" labelField="true"  label="地址："  vtype="maxLength:50" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
				 -->
				</fieldset>
			</div>	
			</div>		
			<div class="mini-panel" title="交易主体信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
				<div class="leftarea">
					<input style="width:100%;" id="forDate" name="forDate" class="mini-datepicker mini-mustFill" labelField="true" required="true"  label="成交日期：" labelStyle="text-align:left;width:120px;" />
					<input style="width:100%" id="bondCode" name="bondCode" class="mini-buttonedit mini-mustFill" onbuttonclick="onBondQuery" labelField="true" allowInput="false" label="债券代码：" required="true"    labelStyle="text-align:left;width:120px;" />
					<input style="width:100%;display:none" id="startDate" name="startDate" class="mini-textbox mini-mustFill" labelField="true"  label="起息日：" required="true"  vtype="maxLength:10"   labelStyle="text-align:left;width:120px;"  enabled="false" />
					<input style="width:100%;display:none" id="settdays" name="settdays" class="mini-textbox mini-mustFill" labelField="true"  label="结算天数：" required="true"  vtype="maxLength:10"   labelStyle="text-align:left;width:120px;"  enabled="false" />
					<input id="invtype" name="invtype" class="mini-combobox mini-mustFill"  labelField="true"   label="投资类型："  style="width:100%;"  labelStyle="text-align:left;width:120px;" data = "CommonUtil.serverData.dictionary.invtype" onvaluechanged="searchMarketData"/>
					<input style="width:100%" id="cleanPrice" name="cleanPrice" class="mini-spinner mini-mustFill" labelField="true"  label="净价(元)：" required="true"  onvaluechanged="onAccuredInterestChanged" minValue="0" maxValue="9999999999999999.9999" format="n4" changeOnMousewheel="false"  labelStyle="text-align:left;width:120px;"   />
					<input style="width:100%" id="yield" name="yield" class="mini-spinner mini-mustFill" labelField="true"  label="到期收益率(%)：" required="true"   minValue="0" maxValue="9999999999999999.99999999" format="n8" changeOnMousewheel="false"  labelStyle="text-align:left;width:120px;"   />
					<input style="width:100%" id="accuredInterest" name="accuredInterest" class="mini-spinner mini-mustFill" labelField="true"  label="应计利息(元)：" required="true"  onvaluechanged="onAccuredInterestChanged" minValue="0" maxValue="9999999999999999.9999" format="n4" changeOnMousewheel="false" labelStyle="text-align:left;width:120px;"   />
					<input style="width:100%" id="dirtyPrice" name="dirtyPrice" class="mini-spinner mini-mustFill" labelField="true"  label="全价(元)：" required="true"  minValue="0" maxValue="9999999999999999.9999" format="n4" changeOnMousewheel="false" labelStyle="text-align:left;width:120px;" onvaluechanged="onAccuredInterestChanged" />
					<input style="width:100%" id="settlementDate" name="settlementDate" class="mini-datepicker mini-mustFill" labelField="true"  label="结算日：" required="true"   labelStyle="text-align:left;width:120px;"  ondrawdate="settlementDate"  />
					<input id="dealTransType" name="dealTransType" class="mini-combobox" labelField="true"  style="width:100%;"  label="数据来源：" data="CommonUtil.serverData.dictionary.dealTransType1" vtype="maxLength:10"  labelStyle="text-align:left;width:130px;"  value="1">
				</div>
				<div class="rightarea">
					<input id="nettingStatus" name="nettingStatus" class="mini-combobox mini-mustFill"  required="true"  labelField="true"   label="净额清算状态：" labelStyle="text-align:left;width:120px;" style="width:100%;" data="CommonUtil.serverData.dictionary.YesNo" showNullItem="true"/>
					<input style="width:100%" id="bondName" name="bondName" class="mini-textbox mini-mustFill" labelField="true"  label="债券名称：" required="true"   labelStyle="text-align:left;width:120px;"  enabled="false" />
					<input style="width:100%" id="totalFaceValue" name="totalFaceValue" class="mini-spinner mini-mustFill" labelField="true"  label="券面总额(万元)："  required="true"  onvaluechanged="onAccuredInterestChanged"   minValue="0" maxValue="9999999999999999.9999"  changeOnMousewheel="false" labelStyle="text-align:left;width:120px;"   />
					<input style="width:100%" id="tradeAmount" name="tradeAmount" class="mini-spinner mini-mustFill" labelField="true"   label="交易金额(元)："   required="true"  onvaluechanged="calSettleAmount"  minValue="0" maxValue="9999999999999999.9999" format="n4" changeOnMousewheel="false" labelStyle="text-align:left;width:120px;"   />
					<input style="width:100%" id="totalAccuredInterest" name="totalAccuredInterest" class="mini-spinner mini-mustFill" labelField="true"  label="应计利息总额(元)："  required="true"  onvaluechanged="calSettleAmount" minValue="0" maxValue="9999999999999999.9999" format="n4" changeOnMousewheel="false" labelStyle="text-align:left;width:120px;"   />
					<input style="width:100%" id="settlementAmount" name="settlementAmount" class="mini-spinner mini-mustFill" labelField="true"  label="结算金额(元)：" required="true"   minValue="0" maxValue="9999999999999999.9999" format="n4" changeOnMousewheel="false" labelStyle="text-align:left;width:120px;"   />
					<input style="width:100%" id="settlementMethod" name="settlementMethod" class="mini-combobox mini-mustFill" labelField="true"  label="结算方式："  required="true"  data = "CommonUtil.serverData.dictionary.SettlementMethod"  labelStyle="text-align:left;width:120px;"   />
					<input id="tradingModel" name="tradingModel" class="mini-combobox mini-mustFill"  required="true"  labelField="true"   label="交易模式："  labelStyle="text-align:left;width:120px;" style="width:100%;"  data="CommonUtil.serverData.dictionary.TradeModel"/>
					<input id="note" name="note" class="mini-textbox" labelField="true" label="备注：" labelStyle="text-align:left;width:120px;" style="width:100%;" vtype="maxLength:16"/>
				</div>
			</div>
			<%@ include file="../../Common/opicsLess.jsp"%>
			<%@ include file="../../Common/RiskCenter.jsp"%>
			<div class="mini-panel" title="清算信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
			<div class="leftarea">
				<fieldset>
					<legend>本方账户</legend>
					<input id="buyAccname" name="buyAccname" class="mini-textbox" labelField="true"  label="资金账户户名："  vtype="maxLength:25"  style="width:100%;"  labelStyle="text-align:left;width:130px;" />
					<input id="buyOpbank" name="buyOpbank" class="mini-textbox" labelField="true"  label="资金开户行："  vtype="maxLength:25"  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
					<input id="buyAccnum" name="buyAccnum" class="mini-textbox" labelField="true"  label="资金账号："   onvalidation="onEnglishAndNumberValidation" vtype="maxLength:50" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
					<input id="buyPsnum" name="buyPsnum" class="mini-textbox" labelField="true"  label="支付系统行号："  onvalidation="onEnglishAndNumberValidation" vtype="maxLength:20"  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
					<input id="buyCaname" name="buyCaname" class="mini-textbox" labelField="true"  label="托管账户户名："  vtype="maxLength:25"  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
					<input id="buyCustname" name="buyCustname" class="mini-buttonedit mini-mustFill" onbuttonclick="onSaccQuery" required="true"  labelField="true" allowInput="false" label="托管机构："  vtype="maxLength:25"  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
					<input id="buyCanum" name="buyCanum" class="mini-textbox" labelField="true"  label="托管帐号："   onvalidation="onEnglishAndNumberValidation" vtype="maxLength:20" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
				</fieldset>
			</div>
			<div class="rightarea">
				<fieldset>
					<legend>对手方账户</legend>
					<input id="sellAccname" name="sellAccname" class="mini-textbox" labelField="true"  label="资金账户户名："  vtype="maxLength:25"  style="width:100%;"  labelStyle="text-align:left;width:130px;" />
					<input id="sellOpbank" name="sellOpbank" class="mini-textbox" labelField="true"  label="资金开户行："  vtype="maxLength:25"  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
					<input id="sellAccnum" name="sellAccnum" class="mini-textbox" labelField="true"  label="资金账号："  onvalidation="onEnglishAndNumberValidation" vtype="maxLength:50" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
					<input id="sellPsnum" name="sellPsnum" class="mini-textbox" labelField="true"  label="支付系统行号："   onvalidation="onEnglishAndNumberValidation" vtype="maxLength:20" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
					<input id="sellCaname" name="sellCaname" class="mini-textbox" labelField="true"  label="托管账户户名：" vtype="maxLength:25"   labelStyle="text-align:left;width:130px;" style="width:100%;"   />
					<input id="sellCustname" name="sellCustname" class="mini-buttonedit mini-mustFill" onbuttonclick="onSaccQuery" required="true"  labelField="true" allowInput="false" label="托管机构：" vtype="maxLength:25"   labelStyle="text-align:left;width:130px;" style="width:100%;"   />
					<input id="sellCanum" name="sellCanum" class="mini-textbox" labelField="true"  label="托管帐号："   onvalidation="onEnglishAndNumberValidation" vtype="maxLength:20" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
				</fieldset>
			</div>
			</div>
			<%@ include file="../../Common/Flow/MiniApproveOpCommon.jsp"%>
			</div>
			
		</div>
		<div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
			<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="save_btn"   onclick="save">保存交易</a></div>
			<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="close_btn"   onclick="close">关闭界面</a></div>
		</div>
	</div>		
<script type="text/javascript">
	mini.parse();
	//获取当前tab
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var row=params.selectData;
	var url=window.location.search;
	var action=CommonUtil.getParam(url,"action");
	var prdNo = CommonUtil.getParam(url, "prdNo");
	
	var form=new mini.Form("#field_form");
    var groupMod = "SECUR-WB";   
	var tradeData={};
	
	tradeData.selectData=row;
	tradeData.operType=action;
	tradeData.serial_no=row.ticketId;
	tradeData.task_id=row.taskId;
	
	//自动计算收益率start
	var basis=360;//计息基础（默认360）
	var couponrate=0;//票面利率
	var bondperiod=0;//债券期限
	var matdt;//到期日
	//自动计算收益率end
	//保存
	function save(){

		//表单验证！！！
		form.validate();
		if (form.isValid() == false) {
			return;
		}
		var data=form.getData(true);
		//对数据做最后的确认
		if(data['dirtyPrice'] != data['cleanPrice'] + data['accuredInterest'] || 
		   data['settlementAmount'] != data['tradeAmount'] + data['totalAccuredInterest'] || 
		   data['tradeAmount'] != data['totalFaceValue'] * data['cleanPrice'] * 100 ||
		   data['totalAccuredInterest'] != data['accuredInterest'] * data['totalFaceValue'] * 100){
				mini.confirm("确认以手输数据为准吗？","确认",function (action) {
					if (action != "ok") {
						return;
					}

					data['buyInst']="<%=__sessionInstitution.getInstId()%>";
// 					data['dealTransType']="M";
					data['sponInst']="<%=__sessionInstitution.getInstId()%>";
					data['buyTrader']="<%=__sessionUser.getUserId() %>";
					var params=mini.encode(data);
					CommonUtil.ajax({
						url:"/IfsCfetsrmbCbtController/saveCbt",
						data:params,
						callback:function(data){
							mini.alert(data,'提示信息',function(){
								if(data=="保存成功"||data=="修改成功"){
									top["win"].closeMenuTab();
								}
							});
						}
					});
					
					
				});
		   }else{
				data['buyInst']="<%=__sessionInstitution.getInstId()%>";
// 				data['dealTransType']="M";
				data['sponInst']="<%=__sessionInstitution.getInstId()%>";
				data['buyTrader']="<%=__sessionUser.getUserId() %>";
				var params=mini.encode(data);
				CommonUtil.ajax({
					url:"/IfsCfetsrmbCbtController/saveCbt",
					data:params,
					callback:function(data){
						mini.alert(data,'提示信息',function(){
							if(data=="保存成功"||data=="修改成功"){
								top["win"].closeMenuTab();
							}
						});
					}
				});

		   }
		
	}
	function close(){
		top["win"].closeMenuTab();
	}
	$(document).ready(function(){
		if($.inArray(action,["approve","detail"])>-1){
			mini.get("save_btn").setVisible(false);
			form.setEnabled(false);
			var form11=new mini.Form("#approve_operate_form");
			form11.setEnabled(true);
			mini.get("forDate").setEnabled(false);
		}
		if($.inArray(action,["edit","approve","detail"])>-1){
			form.setData(row);
			mini.get("forDate").setValue(row.forDate);
			mini.get("ticketId").setValue(row.ticketId);
			//投资组合
			mini.get("port").setValue(row.port);
			mini.get("port").setText(row.port);
			//成本中心
			mini.get("cost").setValue(row.cost);
			mini.get("cost").setText(row.cost);
			//产品代码
			mini.get("product").setValue(row.product);
			mini.get("product").setText(row.product);
			//产品类型
			mini.get("prodType").setValue(row.prodType);
			mini.get("prodType").setText(row.prodType);
			//对手方机构
			mini.get("sellInst").setValue(row.cno);
// 			mini.get("sellInst").setText(row.sellInst);
			queryTextName(row.cno);
			//清算路径
			mini.get("ccysmeans").setValue(row.ccysmeans);
			mini.get("ccysmeans").setText(row.ccysmeans);
			mini.get("ctrsmeans").setValue(row.ctrsmeans);
			mini.get("ctrsmeans").setText(row.ctrsmeans);
			 //债券代码
            mini.get("bondCode").setText(row.bondCode);

			mini.get("buyInst").setValue("<%=__sessionInstitution.getInstFullname()%>");
			mini.get("buyTrader").setValue(row.myUserName);
			mini.get("contractId").setEnabled(false);
			mini.get("buyCustname").setValue(row.buyCustname);
			mini.get("buyCustname").setText(row.buyCustname);
			mini.get("sellCustname").setValue(row.sellCustname);
			mini.get("sellCustname").setText(row.sellCustname);
			//风险中台
			mini.get("applyProd").setValue(row.applyProd);
			mini.get("applyProd").setText(row.applyProd);
			mini.get("applyNo").setValue(row.applyNo);
			mini.get("applyNo").setText(row.applyNo);
			queryCustNo(row.custNo);
			//searchProductWeightNew();
			if(action == "edit" && row.weight ==null ){
				searchProductWeightNew();
			}
			onCurrencyPairChange();
		}else{
			mini.get("sponsor").setValue("<%=__sessionUser.getUserId()%>");
			mini.get("sponInst").setValue("<%=__sessionUser.getInstId() %>");
			mini.get("buyInst").setValue("<%=__sessionInstitution.getInstFullname()%>");
			mini.get("buyTrader").setValue("<%=__sessionUser.getUserName() %>");
			mini.get("forDate").setValue("<%=__bizDate %>");
			//客户特别要求
			mini.get("settlementDate").setValue("<%=__bizDate %>");
			searchProductWeightNew();
		}
		mini.get("dealTransType").setEnabled(false);
	});
	//加载机构树
	function nodeclick(e) {
		
		var oldvalue=e.sender.value;
		var param = mini.encode({"branchId":branchId}); //序列化成JSON
		CommonUtil.ajax({
		  url: "/InstitutionController/searchInstitutionByBranchId",
		  data: param,
		  callback: function (data) {
			var obj=e.sender;
			obj.setData(data.obj);
			obj.setValue(oldvalue);
			
		  }
		});
	}
	//交易方向转换
	function onCurrencyPairChange(e){
		var myDir = mini.get("myDir").getValue();
		if(myDir == 'S'){//卖出
			mini.get("oppoDir").setValue("P");
        	<%-- mini.get("custNo").setValue("<%=__sessionUser.getInstId() %>");//占用授信主体
			mini.get("custNo").setText("<%=__sessionInstitution.getInstFullname()%>");//占用授信主体 --%>
			mini.get("applyNo").setVisible(false);
			mini.get("applyProd").setVisible(false);
		}else if(myDir == 'P'){//买入
			mini.get("oppoDir").setValue("S");
			/* mini.get("custNo").setValue(mini.get("sellInst").getValue());//产品名称
			mini.get("custNo").setText(mini.get("sellInst").getText());//产品名称 */
			mini.get("applyNo").setVisible(true);
			mini.get("applyProd").setVisible(true);
		}
	}
	
	//文、数字、下划线 的验证
	function onEnglishAndNumberValidations(e) {
		if(e.value == "" || e.value == null){//值为空，就不做校验
			return;
		}
		if (e.isValid) {
			if (isEnglishAndNumbers(e.value) == false) {
				e.errorText = "必须输入英文小写+数字";
				e.isValid = false;
			}
		}
	}
	/* 是否英文+数字 */
	function isEnglishAndNumbers(v) {
		var re = new RegExp("^[0-9a-z\_\-]+$");
		if (re.test(v)) return true;
		return false;
	}	
	
	//英文、数字、下划线 的验证
	function onEnglishAndNumberValidation(e) {
		if(e.value == "" || e.value == null){//值为空，就不做校验
			return;
		}
		if (e.isValid) {
			if (isEnglishAndNumber(e.value) == false) {
				e.errorText = "必须输入英文+数字";
				e.isValid = false;
			}
		}
	}
	/* 是否英文+数字 */
	function isEnglishAndNumber(v) {
		var re = new RegExp("^[0-9a-zA-Z\_\-]+$");
		if (re.test(v)) return true;
		return false;
	}

	function onButtonEdit(e) {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Common/CustMini.jsp",
            title: "选择对手方列表",
            width: 900,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.cno);
                        btnEdit.setText(data.cliname);
                        btnEdit.focus();
                       
                       <%--  
                       var myDir = mini.get("myDir").getValue();
                       if(myDir == 'P'){
                        	mini.get("custNo").setValue(data.cno);//占用授信主体
                			mini.get("custNo").setText(data.cliname);//占用授信主体
                        }
                        if(myDir == 'S'){
                        	mini.get("custNo").setValue("<%=__sessionUser.getInstId() %>");//占用授信主体
                			mini.get("custNo").setText("<%=__sessionInstitution.getInstFullname()%>");//占用授信主体
                        } --%>
                        
                    }
                }

            }
        });
	}
	//根据交易对手编号查询全称
	function queryTextName(cno){
		CommonUtil.ajax({
		    url: "/IfsOpicsCustController/searchIfsOpicsCust",
		    data : {'cno' : cno},
		    callback:function (data) {
		    	if (data && data.obj) {
		    		mini.get("sellInst").setText(data.obj.cliname);
				} else {
					mini.get("sellInst").setText();
		    }
		    }
		});
	}
	
function onBondQuery(e) {
        var btnEdit = this;
       /*  var setdate =mini.get("settlementDate").getValue(); */
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Ifs/bond/bondAddMini.jsp",
            title: "选择债券列表",
            width: 700,
            height: 600,
           /*  onload: function () {
				var iframe = this.getIFrameEl();
				var data ={setdate:setdate};
				iframe.contentWindow.SetData(data);
		    }, */
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.bndcd);
                        btnEdit.setText(data.bndcd);
                        mini.get("bondName").setValue(data.bndnm_cn);
                        mini.get("bondName").setText(data.bndnm_cn);
                        mini.get("startDate").setValue(data.issudt);
                        mini.get("settdays").setValue(data.settdays);
//                         mini.get("settlementDate").setValue(null);
						basis = data.basis.substring(1);//计息基础
                        couponrate=data.couponrate*0.01;//票面利率
                        bondperiod=data.bondperiod;//债券期限
                        matdt=data.matdt;//到期日
                        queryCustNo(data.issuer);
                        btnEdit.focus();
                    }
                }

            }
        });
	}
	
	
	
	
	//应计利息、净价  改变时发生  ->  全价=净价+应计利息
	function onAccuredInterestChanged(e){
		var totalFaceValue = mini.get("totalFaceValue").getValue();//券面总额
		var cleanPrice = mini.get("cleanPrice").getValue();//净价
		if(totalFaceValue==0 || cleanPrice==0){
			mini.get("tradeAmount").setValue(0);
			mini.get("accuredInterest").setValue(0);
			mini.get("dirtyPrice").setValue(0);
			mini.get("totalAccuredInterest").setValue(0);
			mini.get("settlementAmount").setValue(0);
			mini.get("yield").setValue(0);
			return;
		}
		/*             交易金额=(券面总额【元】x净价)/100                  */
		var mul = CommonUtil.accMul(totalFaceValue,cleanPrice);//乘积
		var result = CommonUtil.accMul(mul,100);
		mini.get("tradeAmount").setValue(result);//交易金额
		/*          应计利息=全价-净价             */
		var dirtyPrice = mini.get("dirtyPrice").getValue();//全价
		if(dirtyPrice!=0){
			mini.get("accuredInterest").setValue(CommonUtil.accSubtr(dirtyPrice,cleanPrice));//应记利息
		}else{
			/*               全价=净价+应计利息              */
			var sum = CommonUtil.accAdd(mini.get("accuredInterest").getValue(),cleanPrice);
			mini.get("dirtyPrice").setValue(sum);//全价
		}
		var accuredInterest = mini.get("accuredInterest").getValue();//应计利息
		/*             应计利息总额=(应计利息x券面总额【元】)/100                  */
		var mul = CommonUtil.accMul(accuredInterest,totalFaceValue);//乘积
		var result = CommonUtil.accMul(mul,100);
		mini.get("totalAccuredInterest").setValue(result);//应计利息总额
	
		/*             结算金额=交易金额+应计利息总额             */
		calSettleAmount();
	}
	
	
	function fomatDate(dateStr){
		if(dateStr==null || dateStr==''){
			return "";
		}
		var date = new Date(dateStr);
	    var y = date.getFullYear();  
	    var m = date.getMonth() + 1;  
	    m = m < 10 ? ('0' + m) : m;  
	    var d = date.getDate();  
	    d = d < 10 ? ('0' + d) : d;  
	    return y + '-' + m + '-' + d;
	}
	//券面总额  改变时发生  ->  交易金额=(券面总额x净价)/100
	/* function onTotalFaceValueChanged(){
		var totalFaceValue = mini.get("totalFaceValue").getValue();//券面总额
		var cleanPrice = mini.get("cleanPrice").getValue();//净价
		var mul = CommonUtil.accMul(totalFaceValue,cleanPrice);//乘积
		var result = CommonUtil.accDiv(mul,100);
		mini.get("tradeAmount").setValue(result);//交易金额
	} */

	//交易金额、应计利息总额  改变时发生  ->  结算金额=交易金额+应计利息总额 
	function calSettleAmount(){
		
		var tradeAmount = mini.get("tradeAmount").getValue();//交易金额
		var totalIst = mini.get("totalAccuredInterest").getValue();//应计利息总额
		var sum = CommonUtil.accAdd(tradeAmount,totalIst);
		mini.get("settlementAmount").setValue(sum);//结算金额
	}
	function settlementDate(e){
		var date = e.date;
		var startdate = new Date(mini.get("startDate").getValue());
		var settdays = mini.get("settdays").getValue();
		var fordate = new Date(mini.get("forDate").getValue());
		if(startdate!=null && startdate != ""){
			if (date.getTime() < startdate.getTime()-28800000){
				e.allowSelect = false;
			}	
		}
		if(settdays!=null&& settdays!=""){
			if(fordate.getTime() + settdays*28800000*3 <date.getTime()){
				e.allowSelect = false;
			}
		}
		if(fordate!=null&&fordate!=""){
			if(fordate.getTime()>date.getTime()){
				e.allowSelect = false;
			}
		}
	}
	
	 /**   付款路径的选择   */
	  function settleMeans1(){
		 var cpInstId = mini.get("sellInst").getValue();
		if(cpInstId == null || cpInstId == ""){
			mini.alert("请先选择对方机构!");
			return;
		} 
	   	var url;
	   	var data;
   		url="./Ifs/opics/nostMini.jsp";
       	data = { ccy:"" ,cust:cpInstId};
	      
	   	var btnEdit = this;
	       mini.open({
	           url: url,
	           title: "选择清算路径",
	           width: 700,
	           height: 600,
	           onload: function () {
	               var iframe = this.getIFrameEl();
	               iframe.contentWindow.SetData(data);
	           },
	           ondestroy: function (action) {
	               if (action == "ok") {
	                   var iframe = this.getIFrameEl();
	                   var data1 = iframe.contentWindow.GetData();
	                   data1 = mini.clone(data1);    //必须
	                   if (data1) {
	                   	//外币
	                   		mini.get("ccysmeans").setValue($.trim(data1.smeans));
	                   		mini.get("ccysmeans").setText($.trim(data1.smeans));
	                        mini.get("ccysacct").setValue($.trim(data1.nos));
	                       
	                       
	                       btnEdit.focus();
	                   }
	               }

	           }
	       });
	   }
	   
	   /**   收款路径的选择   */
	   function settleMeans2(){
	   	var cpInstId = mini.get("sellInst").getValue();
		if(cpInstId == null || cpInstId == ""){
			mini.alert("请先选择对方机构!");
			return;
		} 
	   	var url;
	   	var data;
	   	//外币
   		url="./Ifs/opics/nostMini.jsp";
       	data = { ccy: "" ,cust:cpInstId};
	   	var btnEdit = this;
	       mini.open({
	           url: url,
	           title: "选择清算路径",
	           width: 700,
	           height: 600,
	           onload: function () {
	               var iframe = this.getIFrameEl();
	               iframe.contentWindow.SetData(data);
	           },
	           ondestroy: function (action) {
	               if (action == "ok") {
	                   var iframe = this.getIFrameEl();
	                   var data1 = iframe.contentWindow.GetData();
	                   data1 = mini.clone(data1);    //必须
	                   if (data1) {
	                   		mini.get("ctrsmeans").setValue($.trim(data1.smeans));
	                   		mini.get("ctrsmeans").setText($.trim(data1.smeans));
	                        mini.get("ctrsacct").setValue($.trim(data1.nos));
	                       btnEdit.focus();
	                   }
	               }

	           }
	       });
	   }
	   //托管机构选择
	function onSaccQuery(e) {
        var btnEdit = this;
        mini.open({
            url: "./Ifs/opics/saccMini.jsp",
            title: "选择托管机构列表",
            width: 700,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.accountno);
                        btnEdit.setText(data.accountno);
                        btnEdit.focus();
                    }
                }

            }
        });
    }
	   
	function searchProductWeightNew(){
    	var form = new mini.Form("field_form");
    	var data = form.getData(true);
    	var mDate = data.settlementDate;
    	var vDate = data.forDate;//交易日期
    	if(CommonUtil.isNull(mDate)||CommonUtil.isNull(vDate)){
			return;
		}
    	searchProductWeight(20,0,prdNo,mDate);
    }   
	
	/* 成功中心的选择 */
    function onCostEdit(){
    	var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "/opics/costMiniLess.jsp",
            title: "选择成本中心",
            width: 700,
            height: 500,
            onload: function () {
                var iframe = this.getIFrameEl();
                iframe.contentWindow.SetData({});
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.costcent);
                        btnEdit.setText(data.costcent);
                        searchMarketData();
                        btnEdit.focus();
                    }
                }
            }
        });
    }
    
    function searchMarketData(){
    	var costCent = mini.get("cost").getValue();//成本中心
        var code = mini.get("invtype").getValue();//交易品种
    	if(CommonUtil.isNull(code) || CommonUtil.isNull(costCent)){
    		return;
    	}
    	queryMarketData(code,costCent);
    }
    
      //获取DV01，久期限，止损值查询
    function queryMarketData(code,costCent){
    	CommonUtil.ajax({
    	    url: "/IfsOpicsCustController/queryMarketData",
    	    data : {'code':code,'costCent':costCent},
    	    callback:function (data) {
    	    	if (data && data.obj) {
    	    		mini.get("longLimit").setValue(data.obj.duration);//久期限
        			mini.get("lossLimit").setValue(data.obj.convexity);//止损
        			mini.get("DV01Risk").setValue(data.obj.delta);//DV01
    	    	} else {
    	    		mini.get("longLimit").setValue();//久期限
        			mini.get("lossLimit").setValue();//止损
        			mini.get("DV01Risk").setValue();//DV01
    	    	}
    	    }
    	});
    }
	   
</script>
<script type="text/javascript" src="../../Common/Flow/MiniApproveOpCommon.js"></script>			
</body>
</html>
