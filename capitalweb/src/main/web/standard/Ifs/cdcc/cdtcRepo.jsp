<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../../global.jsp" %>
<html>
<head>
    <title>回购业务</title>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js"
            type="text/javascript"></script>
</head>
<body style="width: 100%; height: 100%; background: white">
<fieldset class="mini-fieldset title">
    <legend>回购业务查询</legend>
    <div id="search_form" style="width: 100%;">
        <input id="txid" name="txid" class="mini-textbox" width="320px" labelField="true" label="业务标识号："
               labelStyle="text-align:right;"
               emptyText="请输入业务标识号"/>
        <input id="matchingState" name="matchingState" class="mini-textbox" width="320px" labelField="true" label="匹配状态："
               labelStyle="text-align:right;"
               emptyText="请输入匹配状态"/>
        <input id="confirmState" name="confirmState" class="mini-textbox" width="320px" labelField="true" label="确认状态："
               labelStyle="text-align:right;"
               emptyText="请输入确认状态"/>
        <input id="ctrctsts" name="ctrctsts" class="mini-textbox" width="320px" data="CommonUtil.serverData.dictionary.ContractStatusCode" labelField="true" label="合同状态："
               labelStyle="text-align:right;"
               emptyText="请输入合同状态"/>
        <input id="biztp" name="biztp" class="mini-textbox" width="320px" data="CommonUtil.serverData.dictionary.ContractStatusCode" labelField="true" label="合同状态："
               labelStyle="text-align:right;"
               emptyText="请输入业务类型"/>、
        <span style="float: right; margin-right: 150px">
				<a class="mini-button" style="display: none" id="search_btn" onclick="search()">查询</a>
				<a class="mini-button" style="display: none" id="clear_btn" onclick="clear()">清空</a>
                <a class="mini-button" style="display: none" id="send_btn" onclick="send()">手工确认</a>

			</span>
    </div>
</fieldset>
<div class="mini-fit" style="width: 100%; height: 100%;">
    <div id="prod_grid" class="mini-datagrid borderAll"
         style="width: 100%; height: 100%;" sortMode="client"
         allowAlternating="true" idField="userId" border="true" allowResize="true">
        <div property="columns">
            <div type="indexcolumn" headerAlign="center" align="center"
                 width="40px">序号
            </div>
            <div field="txid" headerAlign="center" width="120px" align="center">业务标识号</div>
            <div field="biztp" headerAlign="center" width="120px" align="center">业务类型</div>
            <div field="ctrctid" headerAlign="center" width="100px" align="center">结算标识号</div>
            <div field="givacctnm" headerAlign="center" width="100px" align="center">付款方帐户</div>
            <div field="takacctnm" headerAlign="center" width="100px" align="center">收券方帐户</div>
            <div field="bondid" headerAlign="center" width="100px" align="center">债券代码</div>
            <div field="aggtfaceamt" headerAlign="center" width="100px" align="center">债面总额</div>
            <div field="firstsettledate" headerAlign="center" width="100px" align="center" renderer="onDateTimeRenderer">首次交割日</div>
            <div field="secondsettledate" headerAlign="center" width="100px" align="center" renderer="onDateTimeRenderer">到期交割日</div>
            <div field="val1" headerAlign="center" width="100px" align="center">首期结算金额</div>
            <div field="val2" headerAlign="center" width="100px" align="center">到期结算金额</div>
            <div field="matchingState" headerAlign="center" width="100px" align="center">匹配状态</div>
            <div field="confirmState" headerAlign="center" width="100px" align="center">确认状态</div>
            <div field="contractstatus" headerAlign="center" width="100px" align="center">合同状态</div>
            </div>
        </div>
    </div>
</div>
</body>
<script>
    mini.parse();

    function onDateTimeRenderer(e) {
        var value = new Date(
            /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value) == null ? e.value
                : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(
                    /-/g, '/'));
        if (value)
            return mini.formatDate(value, 'yyyy-MM-dd');
    }

    var grid = mini.get("prod_grid");
    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        searchs(pageSize, pageIndex);
    });

    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function (visibleBtn) {
            search();
        });
    });

    //查询按钮
    function search() {
        searchs(grid.pageSize, 0);
    }

    //清空
    function clear() {
        var form = new mini.Form("search_form");
        form.clear();
        search();
    }

    function searchs(pageSize, pageIndex) {
        var form = new mini.Form("#search_form");
        form.validate();
        if (form.isValid() == false) {
            mini.alert("表单填写错误,请确认!", "提示信息");
            return;
        }

        var data = form.getData();
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        data['branchId'] = branchId;
        data['type'] = "BT02";

        var params = mini.encode(data);
        CommonUtil.ajax({
            url: "/IfsCdccController/getPageIfsCdtcRepo",
            data: params,
            callback: function (data) {
                var grid = mini.get("prod_grid");
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }
</script>
</html>