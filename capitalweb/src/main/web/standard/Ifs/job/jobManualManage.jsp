<%@ page language="java"  contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<div id="opear_form" style="width: 100%;">
<fieldset class="mini-fieldset">
			<legend>定时任务指定日期执行</legend>
			<div>
				<div id="search_form" style="width: 100%" cols="6">
				     <input id="br" name="br" class="mini-combobox mini-mustFill" labelField="true" label="部门：" vtype="maxLength:5" required="true"
	                  emptyText="请填写部门" labelStyle="text-align:right;" width="280px" value='<%=__sessionUser.getOpicsBr()%>'  data="CommonUtil.serverData.dictionary.opicsBr"/>
					<!-- <input id="br" name="br" class="mini-combobox" labelField="true" required="true"  label="部门：" textField="typeValue"
						valueField="typeId" labelStyle="text-align:right;" labelStyle="width:100px" value="01" emptyText="请选择部门" width="280px" /> -->
					 <input id="queryDate" name="queryDate" field="queryDate" class="mini-datepicker" labelField="true"  value="<%=__bizDate%>" label="执行日期："
						 required="true"  labelStyle="text-align:right;" allowinput="false" labelStyle="width:100px" width="280px" />
				</div>
			</div>
</fieldset>
	<a id="get" class="mini-button" style="display: none"   onclick="queryForeignCcy()">代客交易取回</a>
    <a id="send_btn" class="mini-button" style="display: none"   onclick="createGTPFile()">生成数据仓库文件</a>
</div>
<div>
	<p>
	定时任务指定日期执行管理及注意事项：
	</p>
	<p>
	1、【检查执行状态】请检查指定日期没有执行相应定时任务，避免重复执行 !
	</p>
	<p>
	2、【确定执行日期】请确定执行定时任务的日期。<br>
	</p>
</div>
<div>
	<input id="desc" name="desc" class="mini-textarea"
		style="width: 70%; height: 40%" enabled="false"
		borderStyle="border:1;" />
</div>

<script>
	mini.parse();

	var form = new mini.Form("#search_form");
	mini.get("br").setEnabled(false);//不允许修改br

/* 	$(document).ready(function() {
		var jsonData = new Array();
		jsonData.add({'typeId':"01",'typeValue':"01"});
	   	mini.get("br").setData(jsonData);
	}); */
	
	$(document).ready(function() {
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
			getOpicsDate();
		});
	});
	
	//获取OPICS系统日期
	function getOpicsDate() {
	    var data = form.getData(true);
	    data['br'] = opicsBr;
	    var params = mini.encode(data);
	    CommonUtil.ajax({
			url : "/IfsOpicsAcupController/acupFileMaker",
			data : params,
			callback : function(data) {
			    mini.get("queryDate").setValue(data);			
		    }
	    });
	}
	
	
   function queryForeignCcy() {
		mini.confirm("确认取回代客交易？", "系统提示", function (value) {
			if (value == "ok") {
				 form.validate();
				 if (form.isValid() == false) {
						mini.alert("信息填写有误，请重新填写", "系统提示");
						return;
				 }
				 var msg = mini.get("desc").getValue();
				 mini.get("desc").setValue(msg + "正在取回代客交易.........\n");
				 msg += "正在取回代客交易.........\n";
				 var data = form.getData(true);
				 var  queryDate = mini.get("queryDate").getValue();
				 data['queryDate']=queryDate;
				 var params = mini.encode(data);
			     CommonUtil.ajax({
						url : '/IfsJobManualManageController/queryForeignCcy',
						data : params,
						callback : function(data) {
							if (data) {
								var retCode = data.obj.retCode;
								var retMsg = data.obj.retMsg;
							    mini.get("desc").setValue(msg + "已执行取回代客交易任务:  "+ retCode +" 【MSG】: "+ retMsg+"\n" );
							}
						}
		});
			}
		});
	}
   
   function createGTPFile() {
		mini.confirm("确认生成GTP文件吗？", "系统提示", function (value) {
			if (value == "ok") {
				 form.validate();
				 if (form.isValid() == false) {
						mini.alert("信息填写有误，请重新填写", "系统提示");
						return;
				 }
				 var msg = mini.get("desc").getValue();
				 mini.get("desc").setValue(msg + "正在生成GTP文件,请等待3分钟左右.........\n");
				 msg += "正在正在生成GTP文件,请等待3分钟左右.........\n";
				 var data = form.getData(true);
				 var  queryDate = mini.get("queryDate").getValue();
				 data['queryDate']=queryDate;
				 var params = mini.encode(data);
			     CommonUtil.ajax({
						url : '/IfsJobManualManageController/createGTPFile',
						data : params,
						showLoadingMsg: false,
						callback : function(data) {
							if (data) {
								var retCode = data.obj.retCode;
								var retMsg = data.obj.retMsg;
							    mini.get("desc").setValue(msg + "已执行生成GTP文件任务:  "+ retCode +" 【MSG】: "+ retMsg+"\n" );
							}
						}
		});
			}
		});
	}
   
</script>