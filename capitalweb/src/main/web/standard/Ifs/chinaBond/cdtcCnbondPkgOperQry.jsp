<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<title>结算报文查询</title>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js"
	type="text/javascript"></script>
</head>
<body style="width: 100%; height: 100%; background: white">
	<fieldset class="mini-fieldset title">
		<legend>结算报文查询</legend>
		<div id="search_form" style="width: 100%;">
			<input id="txtTxid" name="txtTxid" class="mini-textbox" width="320px" labelField="true" label="业务标识号：" labelStyle="text-align:right;"
				emptyText="请输入业务标识号" /> 
			<input id="txtBiztp" name="txtBiztp" class="mini-combobox" width="320px" data="CommonUtil.serverData.dictionary.BusinessTypeCode"  emptyText="请选择业务类型"
				   labelField="true" label="业务类型：" labelStyle="text-align:right;" />
			<input id="txtSecid" name="txtSecid" class="mini-textbox" width="320px"  labelField="true" label="债券代码："
				labelStyle="text-align:right;" emptyText="请输入债券代码" /> 
			<input id="txtSerchType" name="txtSerchType" class="mini-combobox" width="320px" data="CommonUtil.serverData.dictionary.OperatorStatusCode"
				  emptyText="请选择操作类型" labelField="true" label="操作类型：" labelStyle="text-align:right;" />
			<input id="txtMsgTypeName" name="txtMsgTypeName" class="mini-textbox" width="320px"  labelField="true"
				label="报文名称：" labelStyle="text-align:right;" emptyText="请输入报文名称" />
			<input id="txtMsgType" name="txtMsgType" class="mini-combobox" width="320px"  data="CommonUtil.serverData.dictionary.MsgType"
				emptyText="请选择报文类型" labelField="true" label="报文类型：" labelStyle="text-align:right;" />
			<nobr>
				<input id="txtDateBegin" name="txtDateBegin" class="mini-datepicker" width="320px"  labelField="true" label="日期：" ondrawdate="onDrawDateStart" labelStyle="text-align:right;"
					   emptyText="请输入起始日期" format="yyyy-MM-dd" />
				<input id="txtDateEnd" name="txtDateEnd" class="mini-datepicker" width="320px" labelField="true" label="～" labelStyle="text-align:center;" ondrawdate="onDrawDateEnd" emptyText="请输入截至日期" format="yyyy-MM-dd" />
			</nobr>
			<span style="float: right; margin-right: 150px">
				<a class="mini-button" style="display: none"  id="search_btn" onclick="search()">查询</a>
				<a class="mini-button" style="display: none"  id="clear_btn" onclick="clear()">清空</a>
			</span>
		</div>
	</fieldset>
	<div class="mini-fit" style="width: 100%; height: 100%;">
		<div id="prod_grid" class="mini-datagrid borderAll"
			style="width: 100%; height: 100%;" sortMode="client"
			allowAlternating="true" idField="userId"
			onrowdblclick="onRowDblClick" border="true" allowResize="true">
			<div property="columns">
				<div type="indexcolumn" headerAlign="center" align="center"
					width="40px">序列</div>
				<div field="msgsn" headerAlign="center" width="23	0px"
					align="center">编号</div>
				<div field="msgfilename" headerAlign="center" width="150px"
					align="left">报文名称</div>
				<div field="msgtype" headerAlign="center" width="150px"
					renderer="CommonUtil.dictRenderer" data-options="{dict:'MsgType'}"
					align="center">报文类型</div>
				<div field="msgtime" headerAlign="center" width="120px"
					renderer="onDateTimeRenderer">报文创建时间</div>
				<div field="msgopdt" headerAlign="center" width="120px"
					align="center" renderer="onDateTimeRenderer">报文创建日期</div>
				<div field="msgfedealno" headerAlign="center" width="150px">报文标识号</div>
				<div field="bizTp" headerAlign="center" width="150px"
					renderer="CommonUtil.dictRenderer"
					data-options="{dict:'BusinessTypeCode'}" align="center">业务类型</div>
				<div field="cust" headerAlign="center" width="160px">对手方</div>
			</div>
		</div>
	</div>
</body>
<script>
	mini.parse();

	function onDateTimeRenderer(e) {
		var value = new Date(
				/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value) == null ? e.value
						: /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(
								/-/g, '/'));
		if (value)
			return mini.formatDate(value, 'yyyy-MM-dd');
	}
	var grid = mini.get("prod_grid");
	grid.on("beforeload", function(e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex;
		var pageSize = e.data.pageSize;
		searchs(pageSize, pageIndex);
	});

	$(document).ready(function() {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
			mini.get("txtDateBegin").setValue(new Date());
			mini.get("txtDateEnd").setValue(new Date());
			search();
		});
	});

	//查询按钮
	function search() {
		searchs(grid.pageSize, 0);
	}

	//日期
	function onDrawDateStart(e) {
		var startDate = e.date;
		var endDate = mini.get("txtDateBegin").getValue();
		if (CommonUtil.isNull(endDate)) {
			return;
		}
		if (endDate.getTime() < startDate.getTime()) {
			e.allowSelect = false;
		}
	}

	function onDrawDateEnd(e) {
		var endDate = e.date;
		var startDate = mini.get("txtDateEnd").getValue();
		if (CommonUtil.isNull(startDate)) {
			return;
		}
		if (endDate.getTime() < startDate.getTime()) {
			e.allowSelect = false;
		}
	}
	function searchs(pageSize, pageIndex) {
		var form = new mini.Form("#search_form");
		form.validate();
		if (form.isValid() == false) {
			mini.alert("表单填写错误,请确认!", "提示信息");
			return;
		}

		var data = form.getData();
		data['pageNumber'] = pageIndex + 1;
		data['pageSize'] = pageSize;
		data['branchId'] = branchId;
		//自贸区改造加br
		data['br'] = opicsBr;
		//时间格式化
		data['txtDateBegin'] = mini.get("txtDateBegin").getFormValue().trim();
		data['txtDateEnd'] = mini.get("txtDateEnd").getFormValue().trim();
		
		var params = mini.encode(data);
		CommonUtil.ajax({
			url : "/IfsCdtcCnbondPkgOperController/cdtcCnbondPkgOperQry",
			data : params,
			callback : function(data) {
				if(data ==""){
					return;
				}
				var grid = mini.get("prod_grid");
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
				grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}

	//清空
	function clear() {
		var form = new mini.Form("search_form");
		form.clear();
		search();
	}

	//双击详情
	function onRowDblClick(e) {
		var row = grid.getSelected();
		if (row) {
			var url = CommonUtil.baseWebPath()
					+ "/chinaBond/cdtcCnbondPkgOperQryDetail.jsp?action=detail";
			var tab = {
				id : "cdtcCnbondPkgOperQryDetail",
				name : "cdtcCnbondPkgOperQryDetail",
				title : "结算报文详情",
				url : url,
				showCloseButton : true
			};
			var paramData = {
				selectData : row
			};
			CommonUtil.openNewMenuTab(tab, paramData);
		} else {
			mini.alert("请选中一条记录！", "消息提示");
		}
	}
</script>
</html>