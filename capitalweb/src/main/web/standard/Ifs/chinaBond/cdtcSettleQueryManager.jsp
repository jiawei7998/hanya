<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<title>综合查询</title>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js"
	type="text/javascript"></script>
</head>
<body style="width: 100%; height: 100%; background: white">
	<fieldset class="mini-fieldset title">
		<legend>综合查询</legend>
		<div id="search_form" style="width: 100%;">
			<input id="dealNo" name="dealNo" class="mini-textbox" width="320px" labelField="true" label="交易号：" labelStyle="text-align:right;" emptyText="请输入交易号" />
			<input id="secid" name="secid" class="mini-textbox" width="320px" labelField="true" label="债券标识符：" labelStyle="text-align:right;" emptyText="请输入债券标识符" />
			<input id="secidNm" name="secidNm" class="mini-textbox" width="320px" labelField="true" label="债券名称：" labelStyle="text-align:right;" emptyText="请输入债券名称" />
			<input id="custNm" name="custNm" class="mini-textbox" width="320px" labelField="true" label="客户名：" labelStyle="text-align:right;" emptyText="请输入客户名" />
			<input id="cfetsSn" name="cfetsSn" class="mini-textbox" width="320px" labelField="true" label="交易中心编号：" labelStyle="text-align:right;" emptyText="请输入交易中心编号" />
			<input id="instrSts" name="instrSts" class="mini-combobox" width="320px" data="CommonUtil.serverData.dictionary.InstructionStatusCode" emptyText="请选择指令处理状态"
				   labelField="true" label="指令处理状态：" labelStyle="text-align:right;" />
			<input id="txRsltCd" name="txRsltCd" class="mini-combobox" width="320px" data="CommonUtil.serverData.dictionary.TxRsltCd"  emptyText="请选择指令处理结果返回码"
				   labelField="true" label="指令处理返回码：" labelStyle="text-align:right;" />
			<input id="ctrctSts" name="ctrctSts" class="mini-combobox" width="320px" data="CommonUtil.serverData.dictionary.ContractStatusCode" emptyText="请选择合同处理状态"
				   labelField="true" label="合同处理状态：" labelStyle="text-align:right;" />
			<input id="ctrctBlckSts" name="ctrctBlckSts" class="mini-combobox" width="320px" data="CommonUtil.serverData.dictionary.ContractBlockStatusCode" emptyText="请选择合同冻结状态"
				   labelField="true" label="合同冻结状态：" labelStyle="text-align:right;" />
			<input id="bizTp" name="bizTp" class="mini-combobox" width="320px" data="CommonUtil.serverData.dictionary.BusinessTypeCode"  emptyText="请选择业务类型"
				   labelField="true" label="业务类型：" labelStyle="text-align:right;" />
			<span style="float: right; margin-right: 150px">
				<a class="mini-button" style="display: none"  id="search_btn" onclick="search()">查询</a>
				<a class="mini-button" style="display: none"  id="clear_btn" onclick="clear()">清空</a>
			</span>
		</div>
	</fieldset>
	<div class="mini-fit" style="width: 100%; height: 100%;">
		<div id="prod_grid" class="mini-datagrid borderAll"
			style="width: 100%; height: 100%;" sortMode="client"
			allowAlternating="true" idField="userId"
			onrowdblclick="onRowDblClick" border="true" allowResize="true">
			<div property="columns">
				<div type="indexcolumn" headerAlign="center" align="center"
					width="40px">序号</div>
				<div field="br" headerAlign="center" align="center" width="40px">机构</div>
				<div field="dealNo" headerAlign="center" width="120px"
					align="center">交易号</div>
				<div field="ps" headerAlign="center" width="100px" align="center">ps</div>
				<div field="prodCode" headerAlign="center" width="100px"
					align="center">产品代码</div>
				<div field="secid" headerAlign="center" width="100px" align="center">债券标识符</div>
				<div field="secidNm" headerAlign="center" width="100px"
					align="center">债券名称</div>
				<div field="cif" headerAlign="center" width="100px" align="center">cif</div>
				<div field="custNm" headerAlign="center" width="100px"
					align="center">客户名称</div>
				<div field="cfetsSn" headerAlign="center" width="100px"
					align="center">交易中心编号</div>
				<div field="dealDate" headerAlign="center" width="100px"
					align="center" renderer="onDateTimeRenderer">交易日期</div>
				<div field="vDate" headerAlign="center" width="100px" align="center"
					renderer="onDateTimeRenderer">起息日</div>
				<div field="matDate" headerAlign="center" width="100px"
					align="center" renderer="onDateTimeRenderer">到期日</div>
				<div field="faceAmt" headerAlign="center" width="100px"
					align="center">票面金额</div>
				<div field="comProcdAmt" headerAlign="center" width="150px"
					align="center">首期金额/净价金额</div>
				<div field="matProcdAmt" headerAlign="center" width="150px"
					align="center">到期金额/全价金额</div>
				<div field="comProcdAmtZz" headerAlign="center" width="100px"
					align="center">中债首期交割金额/债券交割金额</div>
				<div field="matProcdAmtZz" headerAlign="center" width="100px"
					align="center">中债到期交割金额</div>
				<div field="comProcdAmtSub" headerAlign="center" width="100px"
					align="center">首期交割金额/债券交割金额差额</div>
				<div field="matProcdAmtSub" headerAlign="center" width="100px"
					align="center">到期交割金额差额</div>
				<div field="dealStatus" headerAlign="center" width="100px"
					align="center">清算状态(交易状态)</div>
				<div field="dealText" headerAlign="center" width="180px"
					align="center">交易来源</div>
				<div field="comCcysMeans" headerAlign="center" width="100px"
					align="center">首期CCY 结算方式</div>
				<div field="matCcysMeans" headerAlign="center" width="100px"
					align="center">到期CCY 结算方式</div>
				<div field="settTypeB" headerAlign="center" width="100px"
					align="center">原始交易单结算方式 首期</div>
				<div field="settDescrB" headerAlign="center" width="180px"
					align="center">原始交易单结算方式描述 首期</div>
				<div field="settTypeE" headerAlign="center" width="100px"
					align="center">原始交易单结算方式 到期</div>
				<div field="settDescrE" headerAlign="center" width="100px"
					align="center">原始交易单结算方式描述 到期</div>
				<div field="xmlMatChid" headerAlign="center" width="100px"
					align="center">xmlMatChid</div>
				<div field="checkSts" headerAlign="center" width="100px"
					align="center">CHECK状态</div>
				<div field="checkTime" headerAlign="center" width="100px"
					align="center" renderer="onDateTimeRenderer">CHECK时间</div>
				<div field="requeStsn" headerAlign="center" width="100px"
					align="center">请求编号</div>
				<div field="sendSts" headerAlign="center" width="100px"
					align="center">发送编号</div>
				<div field="sendTime" headerAlign="center" width="100px"
					align="center" renderer="onDateTimeRenderer">发送时间</div>
				<div field="cnBondRetSts" headerAlign="center" width="100px"
					align="center">发送返回的状态</div>
				<div field="cnBondRetTime" headerAlign="center" width="100px"
					align="center" renderer="onDateTimeRenderer">返回时间</div>
				<div field="acctQryAcctId" headerAlign="center" width="100px"
					align="center">债券所属账户ID</div>
				<div field="instrId" headerAlign="center" width="100px"
					align="center">结算指令标识</div>
				<div field="txFlowID" headerAlign="center" width="100px"
					align="center">交易流水标识</div>
				<div field="instrSts" headerAlign="center" width="120px"
					renderer="CommonUtil.dictRenderer"
					data-options="{dict:'InstructionStatusCode'}" align="center">指令处理状态</div>
				<div field="txRsltCd" headerAlign="center" width="120px"
					renderer="CommonUtil.dictRenderer" data-options="{dict:'TxRsltCd'}"
					align="center">指令处理结果返回码</div>
				<div field="insErrInf" headerAlign="center" width="100px"
					align="center">指令返回信息描述</div>
				<div field="orgtrCnfrmInd" headerAlign="center" width="100px"
					align="center">发令方确认标识</div>
				<div field="ctrCnfrmInd" headerAlign="center" width="100px"
					align="center">对手方确认标识</div>
				<div field="ctrctID" headerAlign="center" width="100px"
					align="center">结算合同标识</div>
				<div field="ctrctSts" headerAlign="center" width="120px"
					renderer="CommonUtil.dictRenderer"
					data-options="{dict:'ContractStatusCode'}" align="center">合同处理状态</div>
				<div field="ctrctBlckSts" headerAlign="center" width="120px"
					renderer="CommonUtil.dictRenderer"
					data-options="{dict:'ContractBlockStatusCode'}" align="center">合同冻结状态</div>
				<div field="ctrctFaildRsn" headerAlign="center" width="100px"
					align="center">合同失败原因</div>
				<div field="queryRetTime" headerAlign="center" width="100px"
					align="center" renderer="onDateTimeRenderer">查询返回时间</div>
			</div>
		</div>
	</div>
</body>
<script>
	mini.parse();

	function onDateTimeRenderer(e) {
		var value = new Date(
				/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value) == null ? e.value
						: /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(
								/-/g, '/'));
		if (value)
			return mini.formatDate(value, 'yyyy-MM-dd');
	}
	var grid = mini.get("prod_grid");
	grid.on("beforeload", function(e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex;
		var pageSize = e.data.pageSize;
		searchs(pageSize, pageIndex);
	});

	$(document).ready(function() {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
			search();
		});
	});

	//查询按钮
	function search() {
		searchs(grid.pageSize, 0);
	}

	//清空
	function clear() {
		var form = new mini.Form("search_form");
		form.clear();
		search();
	}

	function searchs(pageSize, pageIndex) {
		var form = new mini.Form("#search_form");
		form.validate();
		if (form.isValid() == false) {
			mini.alert("表单填写错误,请确认!", "提示信息");
			return;
		}

		var data = form.getData();
		data['pageNumber'] = pageIndex + 1;
		data['pageSize'] = pageSize;
		data['branchId'] = branchId;
		var params = mini.encode(data);
		CommonUtil
				.ajax({
					url : "/IfsCdtcCnBondLinkSecurdealController/searchPageCnBondLinkSecurdeal",
					data : params,
					callback : function(data) {
						var grid = mini.get("prod_grid");
						grid.setTotalCount(data.obj.total);
						grid.setPageIndex(pageIndex);
						grid.setPageSize(pageSize);
						grid.setData(data.obj.rows);
					}
				});
	}
	//双击详情
	function onRowDblClick(e) {
		var row = grid.getSelected();
		if (row) {
			var url = CommonUtil.baseWebPath()
					+ "/chinaBond/cdtcSettleQueryManagerDetail.jsp?action=detail";
			var tab = {
				id : "cdtcSettleQueryManagerDetail",
				name : "cdtcSettleQueryManagerDetail",
				title : "综合查询详情",
				url : url,
				showCloseButton : true
			};
			var paramData = {
				selectData : row
			};
			CommonUtil.openNewMenuTab(tab, paramData);
		} else {
			mini.alert("请选中一条记录！", "消息提示");
		}
	}
</script>
</html>