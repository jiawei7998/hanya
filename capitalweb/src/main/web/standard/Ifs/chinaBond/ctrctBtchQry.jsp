<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<title>合同批量查询</title>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js"
	type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/hideBtn.js"></script>
</head>
<body style="width: 100%; height: 100%; background: white">
	<fieldset class="mini-fieldset title">
		<legend>合同批量查询</legend>
		<div id="search_form" style="width: 100%;">
			<input id="sInstrId " name="sInstrId " class="mini-textbox" width="320px" labelField="true" label="结算指令标识：" labelStyle="text-align:right;" emptyText="请输入结算指令标识" />
			<input id="sTxId" name="sTxId" class="mini-textbox" width="320px" labelField="true" label="业务标识号：" labelStyle="text-align:right;" emptyText="请输入业务标识号" />
			<input id="sCtrctSts" name="sCtrctSts" class="mini-combobox" width="320px" data="CommonUtil.serverData.dictionary.ContractStatusCode" emptyText="请选择合同处理状态"
				   labelField="true" label="合同处理状态：" labelStyle="text-align:right;" />
			<input id="postdate" name="postdate" class="mini-datepicker" width="320px" labelField="true" label="发送日期：" labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请输入发送日期" />
			<input id="bizTp" name="bizTp" class="mini-combobox" width="320px" data="CommonUtil.serverData.dictionary.BusinessTypeCode" emptyText="请选择业务类型" labelField="true" label="业务类型：" labelStyle="text-align:right;" />
			<span
				style="float: right; margin-right: 150px">
				<a class="mini-button" style="display: none"  id="search_btn" onclick="search()">查询</a>
				<a class="mini-button" style="display: none"  id="clear_btn" onclick="clear()">清空</a>
			</span>
		</div>
	</fieldset>
	<div class="mini-fit" style="width: 100%; height: 100%;">
		<div id="prod_grid" class="mini-datagrid borderAll"
			style="width: 100%; height: 100%;" sortMode="client"
			allowAlternating="true" idField="userId"
			onrowdblclick="onRowDblClick" border="true" allowResize="true">
			<div property="columns">
				<div type="indexcolumn" headerAlign="center" align="center"
					width="40px">序号</div>
				<div field="instrId" headerAlign="center" width="120px"
					align="center">结算指令标识</div>
				<div field="ctrctId" headerAlign="center" width="100px"
					align="center">结算合同标识</div>
				<div field="givAcctNm" headerAlign="center" width="100px"
					align="center">付券方账户名称</div>
				<div field="givAcctId" headerAlign="center" width="100px"
					align="center">付券方账户Id</div>
				<div field="takAcctNm" headerAlign="center" width="100px"
					align="center">收券方账户名称</div>
				<div field="takAcctId" headerAlign="center" width="100px"
					align="center">收券方账户id</div>
				<div field="txId" headerAlign="center" width="100px" align="center">业务标识号</div>
				<div field="bizTp" headerAlign="center" width="150px"
					renderer="CommonUtil.dictRenderer"
					data-options="{dict:'BusinessTypeCode'}" align="center">业务类型</div>
				<div field="sttlmTp1" headerAlign="center" width="120px"
					renderer="CommonUtil.dictRenderer"
					data-options="{dict:'BondSettlementType'}" align="center">结算方式1</div>
				<div field="sttlmTp2" headerAlign="center" width="120px"
					renderer="CommonUtil.dictRenderer"
					data-options="{dict:'BondSettlementType'}" align="center">结算方式2</div>
				<div field="bdCnt" headerAlign="center" width="120px" align="center">债券数目</div>
				<div field="aggtFaceAmt" headerAlign="center" width="100px"
					align="center">债券总额</div>
				<div field="val1" headerAlign="center" width="150px" align="center">结算金额1</div>
				<div field="val2" headerAlign="center" width="150px" align="center">结算金额2</div>
				<div field="dt1" headerAlign="center" width="100px" align="center">交割日1</div>
				<div field="dt2" headerAlign="center" width="100px" align="center">交割日2</div>
				<div field="instrSts" headerAlign="center" width="100px"
					renderer="CommonUtil.dictRenderer"
					data-options="{dict:'InstructionStatusCode'}" align="center">指令处理状态</div>
				<div field="ctrctSts" headerAlign="center" width="100px"
					renderer="CommonUtil.dictRenderer"
					data-options="{dict:'ContractStatusCode'}" align="center">合同处理状态</div>
				<div field="ctrctBlckSts" headerAlign="center" width="100px"
					renderer="CommonUtil.dictRenderer"
					data-options="{dict:'ContractBlockStatusCode'}" align="center">合同冻结状态</div>
				<div field="lastUpdTm" headerAlign="center" width="180px"
					align="center">最新更新时间</div>
				<div field="orgtrCnfrmInd" headerAlign="center" width="150px"
					renderer="CommonUtil.dictRenderer"
					data-options="{dict:'InstructionConfirmIndicatorCode'}"
					align="center">发令方确认标识</div>
				<div field="ctrCnfrmInd" headerAlign="center" width="150px"
					renderer="CommonUtil.dictRenderer"
					data-options="{dict:'InstructionConfirmIndicatorCode'}"
					align="center">对手方确认标识</div>
				<div field="instrOrgn" headerAlign="center" width="150px"
					renderer="CommonUtil.dictRenderer"
					data-options="{dict:'InstructionOriginCode'}" align="center">指令来源</div>
				<div field="stdt" headerAlign="center" width="180px" align="center">清算时间</div>
				<div field="iOper" headerAlign="center" width="100px" align="center">经办人员</div>
				<div field="iTime" headerAlign="center" width="100px" align="center"
					renderer="onDateTimeRenderer">经办时间</div>
				<div field="vOper" headerAlign="center" width="100px" align="center">复核人员</div>
				<div field="vTime" headerAlign="center" width="100px" align="center"
					renderer="onDateTimeRenderer">复核时间</div>
				<div field="aDealWith" headerAlign="center" width="100px"
					align="center">报文发送处理标识</div>
				<div field="aDTime" headerAlign="center" width="100px"
					align="center" renderer="onDateTimeRenderer">处理时间</div>
				<div field="rIOper" headerAlign="center" width="100px"
					align="center">撤销经办人</div>
				<div field="rITime" headerAlign="center" width="100px"
					align="center" renderer="onDateTimeRenderer">撤销时间</div>
				<div field="rVOper" headerAlign="center" width="100px"
					align="center">撤销复核人员</div>
				<div field="rVTime" headerAlign="center" width="100px"
					align="center" renderer="onDateTimeRenderer">撤销复核时间</div>
				<div field="rDealWith" headerAlign="center" width="100px"
					align="center">撤销报文处理标识</div>
				<div field="rDTime" headerAlign="center" width="100px"
					align="center" renderer="onDateTimeRenderer">撤销处理时间</div>
				<div field="cSBS001" headerAlign="center" width="100px"
					align="center">CSBS001</div>
				<div field="cSBS003" headerAlign="center" width="100px"
					align="center">CSBS003</div>
			</div>
		</div>
	</div>
</body>
<script>
	mini.parse();
	function onDateRenderer(e) {
		var value = new Date(
				/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value) == null ? e.value
						: /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(
								/-/g, '/'));
		if (value)
			return mini.formatDate(value, 'yyyy-MM-dd');
	}
	function onDateTimeRenderer(e) {
		var value = new Date(
				/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value) == null ? e.value
						: /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(
								/-/g, '/'));
		if (value)
			return mini.formatDate(value, 'yyyy-MM-dd');
	}
	var grid = mini.get("prod_grid");
	grid.on("beforeload", function(e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex;
		var pageSize = e.data.pageSize;
		searchs(pageSize, pageIndex);
	});

	$(document).ready(function() {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
			search();
		});
	});

	//查询按钮
	function search() {
		searchs(grid.pageSize, 0);
	}

	//清空
	function clear() {
		var form = new mini.Form("search_form");
		form.clear();
		search();
	}
	function searchs(pageSize, pageIndex) {
		var form = new mini.Form("#search_form");
		form.validate();
		if (form.isValid() == false) {
			mini.alert("表单填写错误,请确认!", "提示信息");
			return;
		}
		var data = form.getData();
		data['pageNumber'] = pageIndex + 1;
		data['pageSize'] = pageSize;
		data['branchId'] = branchId;
		var params = mini.encode(data);
		CommonUtil.ajax({
			url : "/IfsCdtcCtrctBtchController/ctrctBtchQry",
			data : params,
			callback : function(data) {
				var grid = mini.get("prod_grid");
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
				grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	//双击详情
	function onRowDblClick(e) {
		var row = grid.getSelected();
		if (row) {
			var url = CommonUtil.baseWebPath()
					+ "/chinaBond/ctrctDetail.jsp?action=detail";
			var tab = {
				id : "ctrctDetail",
				name : "ctrctDetail",
				title : "合同查询详情",
				url : url,
				showCloseButton : true
			};
			var paramData = {
				selectData : row
			};
			CommonUtil.openNewMenuTab(tab, paramData);
		} else {
			mini.alert("请选中一条记录！", "消息提示");
		}
	}
</script>
</html>