<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js"
            type="text/javascript"></script>
    <title>连通性测试</title>
</head>

<body style="width: 100%; height: 100%; background: white">
<fieldset class="mini-fieldset">
    <legend>测试</legend>
    <div id="search_form" style="width:100%" cols="6">
        <input id="qryCondDt" name="qryCondDt" class="mini-datepicker" labelField="true"  label="测试日期：" value="<%=__bizDate%>"  width="280px" labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请输入测试日期" />
    </div>
    <span style="float: right; margin-right: 150px">
        <a id="creatXml" class="mini-button"    onclick="create()">生成心跳报文</a>
        <a id="testConnect" class="mini-button"    onclick="test()">测试连通性</a>
	</span>
</fieldset>
<div class="mini-splitter" style="width:100%;height:100%;">
    <div size="50%"  showCollapseButton="true">
        <input id="sendMsgValue" class="mini-textbox" style="display: none" />
        <div style="width:100%;height:100%;" id="sendMsg"  class="mini-textarea">

        </div>
    </div>

    <div size="50%" showCollapseButton="true">
        <div style="width:100%;height:100%;" id="reviceMsg"  class="mini-textarea">

        </div>
    </div>
</div>
</body>
</html>
<script>
    mini.parse();
    
    function test() {
        var sendMsg =   mini.get("sendMsgValue").getValue();
        if(sendMsg==null||sendMsg==""){
            mini.alert("发送报文不可为空，请先生成报文");
        }
        var data ={
            "sendMsg": mini.get("sendMsgValue").getValue()
        }
        CommonUtil.ajax({
            url:"/IfsCdtcTestConnectController/sendHertMessageXml",
            data:data,
            callback : function(data) {
                if(data.code== "error.common.0000"){
                    mini.get("reviceMsg").setValue(formateXml(data.desc));
                }else {
                    mini.alert("发送失败，解析返回报文异常");
                }
            }
        });
    }

    function create(){
        var form=new mini.Form("search_form");
        var data = form.getData();
        var param = mini.encode(data); //序列化成JSON
        CommonUtil.ajax({
            url:"/IfsCdtcTestConnectController/createHertMessageXml",
            data:param,
            callback : function(data) {
                if(data.code== "error.common.0000"){
                    mini.get("sendMsg").setValue(formateXml(data.desc));
                    mini.get("sendMsgValue").setValue(data.desc);
                }else {

                }
            }
        });
    }

    //格式化xml代码
    function formateXml(xmlStr){
        text = xmlStr;
        //使用replace去空格
        text = '\n' + text.replace(/(<\w+)(\s.*?>)/g,function($0, name, props){
            return name + ' ' + props.replace(/\s+(\w+=)/g," $1");
        }).replace(/>\s*?</g,">\n<");
        //处理注释
        text = text.replace(/\n/g,'\r').replace(/<!--(.+?)-->/g,function($0, text){
        var ret = '<!--' + escape(text) + '-->';
        return ret;
        }).replace(/\r/g,'\n');
    //调整格式  以压栈方式递归调整缩进
    var rgx = /\n(<(([^\?]).+?)(?:\s|\s*?>|\s*?(\/)>)(?:.*?(?:(?:(\/)>)|(?:<(\/)\2>)))?)/mg;
    var nodeStack = [];
    var output = text.replace(rgx,function($0,all,name,isBegin,isCloseFull1,isCloseFull2 ,isFull1,isFull2){
        var isClosed = (isCloseFull1 == '/') || (isCloseFull2 == '/' ) || (isFull1 == '/') || (isFull2 == '/');
        var prefix = '';
        if(isBegin == '!'){//!开头
            prefix = setPrefix(nodeStack.length);
        }else {
            if(isBegin != '/'){///开头
                prefix = setPrefix(nodeStack.length);
                if(!isClosed){//非关闭标签
                    nodeStack.push(name);
                }
            }else{
                nodeStack.pop();//弹栈
                prefix = setPrefix(nodeStack.length);
            }
        }
        var ret =  '\n' + prefix + all;
        return ret;
    });
    var prefixSpace = -1;
    var outputText = output.substring(1);
    //还原注释内容
    outputText = outputText.replace(/\n/g,'\r').replace(/(\s*)<!--(.+?)-->/g,function($0, prefix,  text){
    if(prefix.charAt(0) == '\r')
        prefix = prefix.substring(1);
    text = unescape(text).replace(/\r/g,'\n');
    var ret = '\n' + prefix + '<!--' + text.replace(/^\s*/mg, prefix ) + '-->';
    return ret;
    });
    outputText= outputText.replace(/\s+$/g,'').replace(/\r/g,'\r\n');
    return outputText;
    }

    //计算头函数 用来缩进
    function setPrefix(prefixIndex) {
        var result = '';
        var span = '    ';//缩进长度
        var output = [];
        for(var i = 0 ; i < prefixIndex; ++i){
            output.push(span);
        }
        result = output.join('');
        return result;
    }
</script>