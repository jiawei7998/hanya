<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js"
	type="text/javascript"></script>
<title>合同详情</title>
</head>

<body style="width: 100%; height: 100%; background: white">
	<div>
		<span id="labell"><a href="javascript:CommonUtil.activeTab();">合同查询</a>
			>>详情</span>
		<div id="field" class="fieldset-body">
			<table id="field_form"
				style="text-align: left; margin: auto; width: 100%;"
				class="mini-sltable">
				<tr>
					<td style="width: 50%"><input id="instrId" name="instrId"
						class="mini-textbox" label="结算指令标识" emptyText="结算指令标识"
						labelField="true" style="width: 100%"
						labelStyle="text-align:left;width:170px" vtype="maxLength:35"
						required="true"  /></td>
					<td style="width: 50%"><input id="bizTp" name="bizTp"
						class="mini-combobox" label="业务类型" emptyText="请选择业务类型"
						labelField="true" style="width: 100%"
						labelStyle="text-align:left;width:170px" vtype="maxLength:20"
						data="CommonUtil.serverData.dictionary.BusinessTypeCode" /></td>
				</tr>
				<tr>
					<td style="width: 50%"><input id="bizTpcode" name="bizTpcode"
						class="mini-textbox" label="业务类型代码" emptyText="业务类型代码"
						labelField="true" style="width: 100%"
						labelStyle="text-align:left;width:170px" vtype="maxLength:35"
						required="true"  /></td>
					<td style="width: 50%"><input id="txRsltCd" name="txRsltCd"
						class="mini-combobox" label="处理结果状态" emptyText="请选择处理结果状态"
						labelField="true" style="width: 100%"
						labelStyle="text-align:left;width:170px" vtype="maxLength:20"
						data="CommonUtil.serverData.dictionary.TxRsltCd" /></td>
				</tr>
				<tr>
					<td style="width: 50%"><input id="instrSts" name="instrSts"
						class="mini-combobox" label="指令处理状态" emptyText="请选择指令处理状态"
						labelField="true" style="width: 100%"
						labelStyle="text-align:left;width:170px" vtype="maxLength:20"
						data="CommonUtil.serverData.dictionary.InstructionStatusCode" /></td>
					<td style="width: 50%"><input id="instrStscode"
						name="instrStscode" class="mini-textbox" label="指令处理状态代码"
						emptyText="指令处理状态代码" labelField="true" style="width: 100%"
						labelStyle="text-align:left;width:170px" vtype="maxLength:40" /></td>
				</tr>
				<tr>
					<td style="width: 50%"><input id="ctrCnfrmInd"
						name="ctrCnfrmInd" class="mini-combobox" label="对手方确认标识"
						emptyText="对手方确认标识" labelField="true" style="width: 100%"
						labelStyle="text-align:left;width:170px" vtype="maxLength:20"
						data="CommonUtil.serverData.dictionary.InstructionConfirmIndicatorCode" /></td>
					<td style="width: 50%"><input id="ctrCnfrmIndcode"
						name="ctrCnfrmIndcode" class="mini-textbox" label="对手方确认标识代码"
						emptyText="对手方确认标识代码" labelField="true" style="width: 100%"
						labelStyle="text-align:left;width:170px" vtype="maxLength:40" /></td>
				</tr>
				<tr>
					<td style="width: 50%"><input id="orgtrCnfrmInd"
						name="orgtrCnfrmInd" class="mini-combobox" label="发令方确认标识"
						emptyText="发令方确认标识" labelField="true" style="width: 100%"
						labelStyle="text-align:left;width:170px" vtype="maxLength:20"
						data="CommonUtil.serverData.dictionary.InstructionConfirmIndicatorCode" /></td>
					<td style="width: 50%"><input id="orgtrCnfrmIndcode"
						name="发令方确认标识代码" class="mini-textbox" label="发令方确认标识代码"
						emptyText="orgtrCnfrmIndcode" labelField="true"
						style="width: 100%" labelStyle="text-align:left;width:170px"
						vtype="maxLength:40" /></td>
				</tr>
				<tr>
					<td style="width: 50%"><input id="instrOrgn" name="instrOrgn"
						class="mini-combobox" label="指令来源" emptyText="指令来源"
						labelField="true" style="width: 100%"
						labelStyle="text-align:left;width:170px" vtype="maxLength:20"
						data="CommonUtil.serverData.dictionary.InstructionOriginCode" /></td>
					<td style="width: 50%"><input id="instrOrgncode"
						name="instrOrgncode" class="mini-textbox" label="指令来源代码"
						emptyText="指令来源代码" labelField="true" style="width: 100%"
						labelStyle="text-align:left;width:170px" vtype="maxLength:40" /></td>
				</tr>
				<tr>
					<td style="width: 50%"><input id="txId" name="txId"
						class="mini-textbox" label="业务标识号" emptyText="业务标识号"
						labelField="true" style="width: 100%"
						labelStyle="text-align:left;width:170px" vtype="maxLength:35"
						required="true"  /></td>
					<td style="width: 50%"><input id="val1" name="val1"
						class="mini-textbox" label="结算金额1" emptyText="结算金额1"
						labelField="true" style="width: 100%"
						labelStyle="text-align:left;width:170px" vtype="maxLength:40" /></td>
				</tr>
				<tr>
					<td style="width: 50%"><input id="val2" name="val2"
						class="mini-textbox" label="结算金额2" emptyText="结算金额2"
						labelField="true" style="width: 100%"
						labelStyle="text-align:left;width:170px" vtype="maxLength:35"
						required="true"  /></td>
					<td style="width: 50%"><input id="dt1" name="dt1"
						class="mini-textbox" label="交割日1" emptyText="交割日1"
						labelField="true" style="width: 100%"
						labelStyle="text-align:left;width:170px" vtype="maxLength:40" /></td>
				</tr>
				<tr>
					<td style="width: 50%"><input id="dt2" name="dt2"
						class="mini-textbox" label="交割日2 " emptyText="交割日2 "
						labelField="true" style="width: 100%"
						labelStyle="text-align:left;width:170px" vtype="maxLength:35"
						required="true"  /></td>

					<td style="width: 50%"><input id="sttlmTp1" name="sttlmTp1"
						class="mini-combobox" label="结算方式1" emptyText="结算方式1"
						labelField="true" style="width: 100%"
						labelStyle="text-align:left;width:170px" vtype="maxLength:20"
						data="CommonUtil.serverData.dictionary.BondSettlementType" /></td>
				</tr>
				<tr>
					<td style="width: 50%"><input id="sttlmTp1code"
						name="sttlmTp1code" class="mini-textbox" label="结算方式1代码"
						emptyText="结算方式1代码" labelField="true" style="width: 100%"
						labelStyle="text-align:left;width:170px" vtype="maxLength:35"
						required="true"  /></td>
					<td style="width: 50%"><input id="sttlmTp2" name="sttlmTp2"
						class="mini-combobox" label="结算方式1" emptyText="结算方式2"
						labelField="true" style="width: 100%"
						labelStyle="text-align:left;width:170px" vtype="maxLength:20"
						data="CommonUtil.serverData.dictionary.BondSettlementType" /></td>
				</tr>
				<tr>
					<td style="width: 50%"><input id="sttlmTp2code"
						name="sttlmTp2code" class="mini-textbox" label="结算方式2代码"
						emptyText="结算方式2代码" labelField="true" style="width: 100%"
						labelStyle="text-align:left;width:170px" vtype="maxLength:35"
						required="true"  /></td>
					<td style="width: 50%"><input id="takAcctId" name="takAcctId"
						class="mini-textbox" label="收券方账户id" emptyText="收券方账户id"
						labelField="true" style="width: 100%"
						labelStyle="text-align:left;width:170px" vtype="maxLength:40" /></td>
				</tr>
				<tr>
					<td style="width: 50%"><input id="takAcctNm" name="takAcctNm"
						class="mini-textbox" label="收券方账户名称 " emptyText="收券方账户名称 "
						labelField="true" style="width: 100%"
						labelStyle="text-align:left;width:170px" vtype="maxLength:35"
						required="true"  /></td>
					<td style="width: 50%"><input id="givAcctId" name="givAcctId"
						class="mini-textbox" label="付券方账户id" emptyText="付券方账户id"
						labelField="true" style="width: 100%"
						labelStyle="text-align:left;width:170px" vtype="maxLength:40" /></td>
				</tr>
				<tr>
					<td style="width: 50%"><input id="givAcctNm" name="givAcctNm"
						class="mini-textbox" label="付券方账户名称 " emptyText="付券方账户名称 "
						labelField="true" style="width: 100%"
						labelStyle="text-align:left;width:170px" vtype="maxLength:35"
						required="true"  /></td>
					<td style="width: 50%"><input id="chckr" name="chckr"
						class="mini-textbox" label="操作员" emptyText="操作员" labelField="true"
						style="width: 100%" labelStyle="text-align:left;width:170px"
						vtype="maxLength:40" /></td>
				</tr>
				<tr>
					<td style="width: 50%"><input id="oprtr" name="oprtr"
						class="mini-textbox" label="操作员" emptyText="操作员" labelField="true"
						style="width: 100%" labelStyle="text-align:left;width:170px"
						vtype="maxLength:35" required="true"  /></td>
					<td style="width: 50%"><input id="ctrctId" name="ctrctId"
						class="mini-textbox" label="结算合同标识" emptyText="结算合同标识"
						labelField="true" style="width: 100%"
						labelStyle="text-align:left;width:170px" vtype="maxLength:35"
						required="true"  /></td>
				</tr>
				<tr>
					<td style="width: 50%"><input id="ctrctSts" name="ctrctSts"
						class="mini-combobox" label="合同处理状态" emptyText="合同处理状态"
						labelField="true" style="width: 100%"
						labelStyle="text-align:left;width:170px" vtype="maxLength:20"
						data="CommonUtil.serverData.dictionary.ContractStatusCode" /></td>
					<td style="width: 50%"><input id="ctrctBlckSts"
						name="ctrctBlckSts" class="mini-combobox" label="合同冻结状态"
						emptyText="合同冻结状态" labelField="true" style="width: 100%"
						labelStyle="text-align:left;width:170px" vtype="maxLength:20"
						data="CommonUtil.serverData.dictionary.ContractBlockStatusCode" /></td>
				</tr>
				<tr>
					<td style="width: 50%"><input id="ctrctFaildRsn"
						name="ctrctFaildRsn" class="mini-textbox" label="合同失败原因"
						emptyText="合同失败原因" labelField="true" style="width: 100%"
						labelStyle="text-align:left;width:170px" vtype="maxLength:35"
						required="true"  /></td>
					<td style="width: 50%"><input id="ctrctUpdate"
						name="ctrctUpdate" class="mini-textbox" label="合同更新时间"
						emptyText="合同更新时间" labelField="true" style="width: 100%"
						labelStyle="text-align:left;width:170px" vtype="maxLength:35"
						required="true"  /></td>
				</tr>
				<tr>
					<td style="width: 50%"><input id="ctrctQryRsltCd"
						name="ctrctQryRsltCd" class="mini-textbox" label="合同结果返回码"
						emptyText="合同结果返回码" labelField="true" style="width: 100%"
						labelStyle="text-align:left;width:170px" vtype="maxLength:35"
						required="true" /></td>
					<td style="width: 50%"><input id="ctrctQryRsltInf"
						name="ctrctQryRsltInf" class="mini-textbox" label="合同结果返回信息"
						emptyText="合同结果返回信息" labelField="true" style="width: 100%"
						labelStyle="text-align:left;width:170px" vtype="maxLength:35"
						required="true"  /></td>
				</tr>	
			</table>
			<div>
				<a id="cancel_btn"
					style="display: block; margin: 0 auto; width: 80px;"
					class="mini-button" style="display: none"  onclick="cancel">返回</a>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		mini.parse();
		//获取当前tab
		var currTab = top["win"].tabs.getActiveTab();
		var params = currTab.params;
		var row = params.selectData;
		var url = window.location.search;
		var action = CommonUtil.getParam(url, "action");
		var form = new mini.Form("#field_form");

		$(document).ready(function() {
			initData();
		});

		//初始化数据
		function initData() {
			var data = new Object();
			data['qryCondId'] = row.ctrctId;
			var params = mini.encode(data);
			CommonUtil
					.ajax({
						url : "/IfsCdtcCtrctBtchController/ctrctBtchDetail",
						data : params,
						callback : function(data) {
							//data.obj.bizTp = "BT03";
							var bizTp = data.obj.bizTp;
							if (bizTp == "BT01") {
								$("#field_form")
										.append(
												"<tr style=\"color:red;height:40px;\"><td>以下是[质押式回购]特有</td><tr/>");
								$("#field_form")
										.append(
												"<tr><td style=\"width: 50%\"><input id=\"rate\" name=\"rate\" class=\"mini-textbox\" label=\"回购利率\" emptyText=\"回购利率\" labelField=\"true\" style=\"width: 100%\" labelStyle=\"text-align:left;width:170px\" vtype=\"maxLength:35\" required=\"true\" /></td><td style=\"width: 50%\"><input id=\"aggtFaceAmt\" name=\"aggtFaceAmt\" class=\"mini-textbox\" label=\"全面金额 \" emptyText=\"全面金额\" labelField=\"true\" style=\"width: 100%\" labelStyle=\"text-align:left;width:170px\" vtype=\"maxLength:35\" required=\"true\" /></td></tr>");
							} else if (bizTp == "BT02") {
								$("#field_form")
										.append(
												"<tr style=\"color:red;height:40px;\"><td>以下是[基本债券]特有</td><tr/>");
								$("#field_form")
										.append(
												"<tr><td style=\"width: 50%\"><input id=\"pric1\" name=\"pric1\" class=\"mini-textbox\" label=\"净价（百元面值）\" emptyText=\"净价（百元面值）\" labelField=\"true\" style=\"width: 100%\" labelStyle=\"text-align:left;width:170px\" vtype=\"maxLength:35\" required=\"true\" /></td><td style=\"width: 50%\"><input id=\"pric2\" name=\"pric2\" class=\"mini-textbox\" label=\"全价（百元面值） \" emptyText=\"全价（百元面值）\" labelField=\"true\" style=\"width: 100%\" labelStyle=\"text-align:left;width:170px\" vtype=\"maxLength:35\" required=\"true\" /></td></tr>");
								$("#field_form")
										.append(
												"<tr><td style=\"width: 50%\"><input id=\"bdAmt\" name=\"bdAmt\" class=\"mini-textbox\" label=\"债券总额\" emptyText=\"债券总额\" labelField=\"true\" style=\"width: 100%\" labelStyle=\"text-align:left;width:170px\" vtype=\"maxLength:35\" required=\"true\" /></td></tr>");

							} else if (bizTp == "BT03") {
								$("#field_form")
										.append(
												"<tr style=\"color:red;height:40px;\"><td>以下是[买断式回购]特有</td><tr/>");
								$("#field_form")
										.append(
												"<tr><td style=\"width: 50%\"><input id=\"grteTp\" name=\"grteTp\" class=\"mini-combobox\" label=\"担保方式\" emptyText=\"担保方式\" labelField=\"true\" style=\"width: 100%\" labelStyle=\"text-align:left;width:170px\" vtype=\"maxLength:20\" data=\"CommonUtil.serverData.dictionary.GuaranteeTypeCode\" /></td><td style=\"width: 50%\"><input id=\"nBdId\" name=\"nBdId\" class=\"mini-textbox\" label=\"逆回购方保证券 \" emptyText=\"逆回购方保证券\" labelField=\"true\" style=\"width: 100%\" labelStyle=\"text-align:left;width:170px\" vtype=\"maxLength:35\" required=\"true\" /></td></tr>");
								$("#field_form")
										.append(
												"<tr><td style=\"width: 50%\"><input id=\"nBdShrtNm\" name=\"nBdShrtNm\" class=\"mini-textbox\" label=\"逆回购方保证券名\" emptyText=\"逆回购方保证券名\" labelField=\"true\" style=\"width: 100%\" labelStyle=\"text-align:left;width:170px\" vtype=\"maxLength:35\" required=\"true\" /></td><td style=\"width: 50%\"><input id=\"nBdAmt\" name=\"nBdAmt\" class=\"mini-textbox\" label=\"逆回购方保证券额 \" emptyText=\"逆回购方保证券额\" labelField=\"true\" style=\"width: 100%\" labelStyle=\"text-align:left;width:170px\" vtype=\"maxLength:35\" required=\"true\" /></td></tr>");
								$("#field_form")
										.append(
												"<tr><td style=\"width: 50%\"><input id=\"zBdAmt\" name=\"zBdAmt\" class=\"mini-textbox\" label=\"正回购方保证券额\" emptyText=\"正回购方保证券额\" labelField=\"true\" style=\"width: 100%\" labelStyle=\"text-align:left;width:170px\" vtype=\"maxLength:35\" required=\"true\" /></td><td style=\"width: 50%\"><input id=\"zBdShrtNm\" name=\"zBdShrtNm\" class=\"mini-textbox\" label=\"正回购方保证券名 \" emptyText=\"正回购方保证券名\" labelField=\"true\" style=\"width: 100%\" labelStyle=\"text-align:left;width:170px\" vtype=\"maxLength:35\" required=\"true\" /></td></tr>");
								$("#field_form")
										.append(
												"<tr><td style=\"width: 50%\"><input id=\"zBdAmt\" name=\"zBdAmt\" class=\"mini-textbox\" label=\"正回购方保证券额\" emptyText=\"正回购方保证券额\" labelField=\"true\" style=\"width: 100%\" labelStyle=\"text-align:left;width:170px\" vtype=\"maxLength:35\" required=\"true\" /></td></tr>");

							}
							mini.parse();
							form.setData(data.obj);
							form.setEnabled(false);
						}
					});
		}
		function cancel() {
			top["win"].closeMenuTab();
		}
	</script>
	<script src="<%=basePath%>/miniScript/miniMustFill.js"></script>
</body>
</html>