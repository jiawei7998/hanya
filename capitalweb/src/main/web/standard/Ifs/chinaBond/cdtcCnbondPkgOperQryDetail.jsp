<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js"
	type="text/javascript"></script>
<title>结算报文查询</title>
</head>

<body style="width: 100%; height: 100%; background: white">
	<div>
		<span id="labell"><a href="javascript:CommonUtil.activeTab();">结算报文查询</a>
			>> 详情</span>
		<div id="field" class="fieldset-body">
			<table id="field_form"
				style="text-align: left; margin: auto; width: 100%;"
				class="mini-sltable">
				<tr>
					<td style="width: 50%"><input id="instrId" name="instrId"
						class="mini-textbox" label="结算指令标识" emptyText="结算指令标识"
						labelField="true" style="width: 100%"
						labelStyle="text-align:left;width:170px" vtype="maxLength:35"
						required="true"  /></td>

					<td style="width: 50%"><input id="bizTp" name="bizTp"
						class="mini-textbox" label="业务类型" emptyText="业务类型"
						labelField="true" style="width: 100%"
						labelStyle="text-align:left;width:170px" vtype="maxLength:40" /></td>
				</tr>
				<tr>
					<td style="width: 50%"><input id="txRsltCd" name="txRsltCd"
						class="mini-textbox" label="处理结果状态" emptyText="处理结果状态"
						labelField="true" style="width: 100%"
						labelStyle="text-align:left;width:170px" vtype="maxLength:40" /></td>
					<td style="width: 50%"><input id="instrSts" name="instrSts"
						class="mini-textbox" label="指令处理状态" emptyText="指令处理状态"
						labelField="true" style="width: 100%"
						labelStyle="text-align:left;width:170px" vtype="maxLength:35"
						required="true"  /></td>
				</tr>
				<tr>
					<td style="width: 50%"><input id="ctrCnfrmInd"
						name="ctrCnfrmInd" class="mini-textbox" label="对手方确认标识"
						emptyText="对手方确认标识" labelField="true" style="width: 100%"
						labelStyle="text-align:left;width:170px" vtype="maxLength:40" /></td>
					<td style="width: 50%"><input id="orgtrCnfrmInd"
						name="orgtrCnfrmInd" class="mini-textbox" label="发令方确认标识"
						emptyText="发令方确认标识" labelField="true" style="width: 100%"
						labelStyle="text-align:left;width:170px" vtype="maxLength:35"
						required="true"  /></td>
				</tr>
				<tr>
					<td style="width: 50%"><input id="instrOrgn" name="instrOrgn"
						class="mini-textbox" label="指令来源 " emptyText="指令来源 "
						labelField="true" style="width: 100%"
						labelStyle="text-align:left;width:170px" vtype="maxLength:35"
						required="true"  /></td>
					<td style="width: 50%"><input id="txId" name="txId"
						class="mini-textbox" label="业务标识号" emptyText="业务标识号"
						labelField="true" style="width: 100%"
						labelStyle="text-align:left;width:170px" vtype="maxLength:35"
						required="true"  /></td>
				</tr>
				<tr>

					<td style="width: 50%"><input id="val1" name="val1"
						class="mini-textbox" label="结算金额1" emptyText="结算金额1"
						labelField="true" style="width: 100%"
						labelStyle="text-align:left;width:170px" vtype="maxLength:40" /></td>
					<td style="width: 50%"><input id="val2" name="val2"
						class="mini-textbox" label="结算金额2" emptyText="结算金额2"
						labelField="true" style="width: 100%"
						labelStyle="text-align:left;width:170px" vtype="maxLength:35"
						required="true"  /></td>
				</tr>
				<tr>

					<td style="width: 50%"><input id="dt1" name="dt1"
						class="mini-textbox" label="交割日1" emptyText="交割日1"
						labelField="true" style="width: 100%"
						labelStyle="text-align:left;width:170px" vtype="maxLength:40" /></td>
					<td style="width: 50%"><input id="dt2" name="dt2"
						class="mini-textbox" label="交割日2 " emptyText="交割日2 "
						labelField="true" style="width: 100%"
						labelStyle="text-align:left;width:170px" vtype="maxLength:35"
						required="true"  /></td>
				</tr>
				<tr>

					<td style="width: 50%"><input id="sttlmTp1" name="sttlmTp1"
						class="mini-textbox" label="结算方式1" emptyText="结算方式1"
						labelField="true" style="width: 100%"
						labelStyle="text-align:left;width:170px" vtype="maxLength:40" /></td>
					<td style="width: 50%"><input id="sttlmTp2" name="sttlmTp2"
						class="mini-textbox" label="结算方式2" emptyText="结算方式2"
						labelField="true" style="width: 100%"
						labelStyle="text-align:left;width:170px" vtype="maxLength:40" /></td>
				</tr>
				<tr>
					<td style="width: 50%"><input id="takAcctId" name="takAcctId"
						class="mini-textbox" label="收券方账户id" emptyText="收券方账户id"
						labelField="true" style="width: 100%"
						labelStyle="text-align:left;width:170px" vtype="maxLength:40" /></td>
					<td style="width: 50%"><input id="takAcctNm" name="takAcctNm"
						class="mini-textbox" label="收券方账户名称 " emptyText="收券方账户名称 "
						labelField="true" style="width: 100%"
						labelStyle="text-align:left;width:170px" vtype="maxLength:35"
						required="true"  /></td>
				</tr>
				<tr>

					<td style="width: 50%"><input id="givAcctId" name="givAcctId"
						class="mini-textbox" label="付券方账户id" emptyText="付券方账户id"
						labelField="true" style="width: 100%"
						labelStyle="text-align:left;width:170px" vtype="maxLength:40" /></td>
					<td style="width: 50%"><input id="givAcctNm" name="givAcctNm"
						class="mini-textbox" label="付券方账户名称 " emptyText="付券方账户名称 "
						labelField="true" style="width: 100%"
						labelStyle="text-align:left;width:170px" vtype="maxLength:35"
						required="true"  /></td>
				</tr>
				<tr>

					<td style="width: 50%"><input id="chckr" name="chckr"
						class="mini-textbox" label="操作员" emptyText="操作员" labelField="true"
						style="width: 100%" labelStyle="text-align:left;width:170px"
						vtype="maxLength:40" /></td>
					<td style="width: 50%"><input id="oprtr" name="oprtr"
						class="mini-textbox" label="操作员" emptyText="操作员" labelField="true"
						style="width: 100%" labelStyle="text-align:left;width:170px"
						vtype="maxLength:35" required="true"  /></td>
				</tr>
				<tr>
					<td colspan="2" style="text-align: center;"><a id="cancel_btn"
						class="mini-button" style="display: none"  onclick="cancel">返回</a></td>
				</tr>
			</table>
		</div>
	</div>
	<script type="text/javascript">
		mini.parse();
		//获取当前tab
		var currTab = top["win"].tabs.getActiveTab();
		var params = currTab.params;
		var row = params.selectData;
		var url = window.location.search;
		var action = CommonUtil.getParam(url, "action");
		var form = new mini.Form("#field_form");

		$(document).ready(function() {
			initData();
		});

		//初始化数据
		function initData() {
			CommonUtil
					.ajax({
						url : "/IfsCdtcCnbondPkgOperController/cdtcCnbondPkgOperDetail",
						data : row,
						callback : function(data) {
							form.setData(data.obj);
							form.setEnabled(false);
						}
					});
		}
		function cancel() {
			top["win"].closeMenuTab();
		}
	</script>
	<script src="<%=basePath%>/miniScript/miniMustFill.js"></script>
</body>
</html>