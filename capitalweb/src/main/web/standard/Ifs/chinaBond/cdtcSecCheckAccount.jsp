<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<title>债券总对账单查询</title>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js"
	type="text/javascript"></script>
</head>
<body style="width: 100%; height: 100%; background: white">
	<fieldset class="mini-fieldset title">
		<legend>债券总对账单查询</legend>
		<div id="search_form" style="width: 100%;">
			<input id="secidNm" name="secidNm" class="mini-textbox" width="320px"  labelField="true" label="债券简称：" labelStyle="text-align:right;"
				emptyText="请输入债券简称" />
			<input id="secid" name="secid" class="mini-textbox" width="320px"  labelField="true" label="债券代码：" labelStyle="text-align:right;"
				   emptyText="请输入债券代码" />
			<span style="float: right; margin-right: 150px">
				<a class="mini-button" style="display: none"  id="search_btn" onclick="search()">查询</a>
				<a class="mini-button" style="display: none"  id="clear_btn" onclick="clear()">清空</a>
			</span>
		</div>
	</fieldset>
	<div class="mini-fit" style="width: 100%; height: 100%;">
		<div id="prod_grid" class="mini-datagrid borderAll"
			style="width: 100%; height: 100%;" sortMode="client"
			allowAlternating="true" idField="userId" border="true" allowResize="true">
			<div property="columns">
				<div type="indexcolumn" headerAlign="center" align="center"
					width="40px">序号</div>
				<div field="secidNm" headerAlign="center" width="120px"
					align="center">债券简称</div>
				<div field="secid" headerAlign="center" width="100px" align="center">债券代码</div>
				<div field="ableUseAmt" headerAlign="center" width="100px"
					align="center">可用科目 万元</div>
				<div field="waitPayAmt" headerAlign="center" width="100px"
					align="center">待付科目 万元</div>
				<div field="repoZYAmt" headerAlign="center" width="100px"
					align="center">质押科目 万元</div>
				<div field=repoRevAmt headerAlign="center" width="100px"
					align="center">待购回科目 万元</div>
				<div field="frozenAmt" headerAlign="center" width="120px"
					align="center">冻结科目 万元</div>
				<div field="totalAmt" headerAlign="center" width="100px"
					align="center">债券余额 万元</div>
				<div field="cxAmt" headerAlign="center" width="100px" align="center">承销额度
					万元</div>
				<div field="cxWaitPayAmt" headerAlign="center" width="100px"
					align="center">承销额度待付 万元</div>
			</div>
		</div>
	</div>
</body>
<script>
	mini.parse();

	function onDateTimeRenderer(e) {
		var value = new Date(
				/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value) == null ? e.value
						: /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(
								/-/g, '/'));
		if (value)
			return mini.formatDate(value, 'yyyy-MM-dd');
	}
	var grid = mini.get("prod_grid");
	grid.on("beforeload", function(e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex;
		var pageSize = e.data.pageSize;
		searchs(pageSize, pageIndex);
	});

	$(document).ready(function() {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
			search();
		});
	});

	//查询按钮
	function search() {
		searchs(grid.pageSize, 0);
	}

	//清空
	function clear() {
		var form = new mini.Form("search_form");
		form.clear();
		search();
	}

	function searchs(pageSize, pageIndex) {
		var form = new mini.Form("#search_form");
		form.validate();
		if (form.isValid() == false) {
			mini.alert("表单填写错误,请确认!", "提示信息");
			return;
		}

		var data = form.getData();
		data['pageNumber'] = pageIndex + 1;
		data['pageSize'] = pageSize;
		data['branchId'] = branchId;
		var params = mini.encode(data);
		CommonUtil.ajax({
			url : "/IfsCdtcSecCheckAccountController/cdtcSecCheckAccount",
			data : params,
			callback : function(data) {
				var grid = mini.get("prod_grid");
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
				grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
</script>
</html>