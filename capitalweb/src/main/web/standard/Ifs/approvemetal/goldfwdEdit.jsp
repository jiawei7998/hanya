<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
		<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title></title>
  </head>

<body style="width:100%;height:100%;background:white">
	<div class="mini-splitter" style="width:100%;height:100%;">
				<div size="90%" showCollapseButton="false">
		<h1 style="text-align:center"><strong>黄金远期</strong></h1>
		<div id="field_form" class="mini-fit area"  style="background:white" >
		<input id="sponsor" name="sponsor" class="mini-hidden" />
		<input id="sponInst" name="sponInst" class="mini-hidden" />
		<input id="aDate" name="aDate" class="mini-hidden"/>
		<input id="postDate" name="postDate" class="mini-datepicker"  labelField="true"  width="30%" style="font-size:18px;float:left;margin-left: 100px;"  label="报送日期："  required="true"  labelStyle="text-align:left;" />
		<input id="ticketId" name="ticketId" class="mini-textbox"  labelField="true" width="30%" labelField="true" style="font-size:18px; margin-left: 25%;" enabled="false" label="审批单号：" labelStyle="text-align:left;width:120px" emptyText="由系统自动生成"/> 		
		<div class="centerarea">
			<fieldset>
				<legend>审批交易明细</legend>
					<input id="currencyPair"  name="currencyPair" label="货币对：" class="mini-combobox" data="CommonUtil.serverData.dictionary.CurrencyPair"  labelField="true" labelStyle="text-align:left;width:120px;"/>
					<input id="counterpartyInstId"  name="counterpartyInstId" label="交易对手 ：" onbuttonclick="onButtonEdit" class="mini-buttonedit" labelField="true" labelStyle="text-align:left;width:120px;"/>
					<input id="tenor1"  name="tenor1" label="期限：" class="mini-spinner"   changeOnMousewheel='false' maxValue="9999999999"  labelField="true" labelStyle="text-align:left;width:120px;"/>
					<input id="price"  name="price" label="利率(%)：" class="mini-spinner"   changeOnMousewheel='false' maxValue="99999999999999999999999.999999999999" format="n8"  labelField="true" labelStyle="text-align:left;width:120px;"/>
					<input id="valueDate1"  name="valueDate1" label="起息日：" class="mini-datepicker" labelField="true" labelStyle="text-align:left;width:120px;"/>
					<input id="direction1"  name="direction1" label="本方方向："data = "CommonUtil.serverData.dictionary.trading"  vtype="maxLength:6" onvaluechanged="dirVerify" class="mini-combobox"  labelField="true" labelStyle="text-align:left;width:120px;"/>
					<input id="amount1"  name="amount1" label="本方金额：" class="mini-spinner"   changeOnMousewheel='false'  maxValue="99999999999999999.9999" format="n4"  labelField="true" labelStyle="text-align:left;width:120px;"/>
					<input id="direction2"  name="direction2" label="对方方向：" data = "CommonUtil.serverData.dictionary.trading" vtype="maxLength:6" enabled="false" class="mini-combobox"  labelField="true" labelStyle="text-align:left;width:120px;"/>
					<input id="amount2"  name="amount2" label="对方金额：" class="mini-spinner"   changeOnMousewheel='false' maxValue="99999999999999999.9999" format="n4"  labelField="true" labelStyle="text-align:left;width:120px;"/>
					<input id="settleMode"  name="settleMode" label="清算方式：" class="mini-textbox" vtype="maxLength:13"  labelField="true" labelStyle="text-align:left;width:120px;"/>
			</fieldset>
		</div>	
		<%@ include file="../../Common/opicsApprove.jsp"%>		
		<%@ include file="../../Common/Flow/MiniApproveOpCommon.jsp"%>
	</div>
	</div>
			<div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="save_btn"   onclick="save">保存交易</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="close_btn"   onclick="close">关闭界面</a></div>
			</div>
</div>	
<script type="text/javascript">
	mini.parse();
	//获取当前tab
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var row=params.selectData;
	var url=window.location.search;
	var action=CommonUtil.getParam(url,"action");
	var form=new mini.Form("#field_form");
	mini.get("ticketId").setEnabled(false);
	
	var tradeData={};
	
	tradeData.selectData=row;
	tradeData.operType=action;
	tradeData.serial_no=row.ticketId;
	tradeData.task_id=row.taskId;
	//保存
	function save(){
		form.validate();
		if(form.isValid() == false){
			mini.alert("信息填写有误，请重新填写","系统提示");
			return;
		}
		var data=form.getData(true);
		data['type']="fwd";
		data['sponInst']="<%=__sessionInstitution.getInstId()%>";
		data['dealTransType']="0";
		var params=mini.encode(data);
		CommonUtil.ajax({
			url:"/IfsApprovemetalGoldController/saveGold",
			data:params,
			callback:function(data){
				mini.alert("保存成功",'提示信息',function(){
					top["win"].closeMenuTab();
				});
				
			}
		});
	}
	function close(){
		top["win"].closeMenuTab();
	}
	$(document).ready(function(){
		if($.inArray(action,["approve","detail"])>-1){
			mini.get("save_btn").setVisible(false);
			form.setEnabled(false);
			var approForm=new mini.Form("#approve_operate_form");
			approForm.setEnabled(true);
			mini.get("postDate").setEnabled(false);
		}
		if($.inArray(action,["edit","approve","detail"])>-1){
			form.setData(row);
			mini.get("postDate").setValue(row.postDate);
			mini.get("ticketId").setValue(row.ticketId);
			//交易对手
			mini.get("counterpartyInstId").setValue(row.cno);
			mini.get("counterpartyInstId").setText(row.counterpartyInstId);
			//投资组合
			mini.get("port").setValue(row.port);
			mini.get("port").setText(row.port);
			
		}else{
			mini.get("ticketId").setValue("交易编号自动生成");
			mini.get("sponsor").setValue("<%=__sessionUser.getUserId()%>");
			mini.get("sponInst").setValue("<%=__sessionUser.getInstId() %>");
			mini.get("postDate").setValue("<%=__bizDate %>");
		}
	});
	
	
	//文、数字、下划线 的验证
	function onEnglishAndNumberValidations(e) {
		if(e.value == "" || e.value == null){//值为空，就不做校验
			return;
		}
		if (e.isValid) {
			if (isEnglishAndNumbers(e.value) == false) {
				e.errorText = "必须输入英文小写+数字";
				e.isValid = false;
			}
		}
	}
	/* 是否英文+数字 */
	function isEnglishAndNumbers(v) {
		var re = new RegExp("^[0-9a-z\_\-]+$");
		if (re.test(v)) return true;
		return false;
	}	
	
	
	
	 /* 是否英文+数字 */
    function isEnglishAndNumber(v) {
        
        var re = new RegExp("^[0-9a-zA-Z\_]+$");
        if (re.test(v)) return true;
        return false;
    }

   function onEnglishAndNumberValidation(e) {
       if (e.isValid) {
           if (isEnglishAndNumber(e.value) == false) {
               e.errorText = "必须输入英文+数字";
               e.isValid = false;
           }
       }
   }
 //加载机构树
	/* function nodeclick(e) {
		
		var oldvalue=e.sender.value;
		var param = mini.encode({"branchId":branchId}); //序列化成JSON
		CommonUtil.ajax({
		  url: "/InstitutionController/searchInstitutionByBranchId",
		  data: param,
		  callback: function (data) {
			var obj=e.sender;
			obj.setData(data.obj);
			obj.setValue(oldvalue);
			
		  }
		});
	} */
	function onButtonEdit(e) {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Common/CustMini.jsp",
            title: "选择对手方列表",
            width: 900,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.cno);
                        btnEdit.setText(data.cfn);
                        btnEdit.focus();
                    }
                }

            }
        });
    }
	//本方方向 值改变时调用此方法(本方方向改变，则对方方向改变)  
	function  dirVerify(e){
	    if(e.value == "1"){//买入
	        mini.get("direction2").setValue("2");
	    }else if(e.value == "2"){//卖出
	        mini.get("direction2").setValue("1");
	    }

	}
</script>
<script type="text/javascript" src="../../Common/Flow/MiniApproveOpCommon.js"></script>			
</body>
</html>
