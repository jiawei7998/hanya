<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
<title></title>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
	<legend>黄金即期审批单查询</legend>	
	<div>
		<div id="search_form" style="width:100%" cols="6">
			<input id="ticketId" name="ticketId" class="mini-textbox" labelField="true"  label="审批单编号：" width="280px" labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请输入成交单编号" />
			<input id="postDate" name="postDate" class="mini-datepicker" labelField="true"  label="报送日期："   width="280px" labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请输入成交日期" />
			<input id="approveStatus" name="approveStatus" class="mini-combobox" data="CommonUtil.serverData.dictionary.ApproveSta" width="280px" emptyText="请选择审批状态" labelField="true"  label="审批状态：" labelStyle="text-align:right;"/>
			<span style="float: right; margin-right: 150px"> 
				<a id="search_btn" class="mini-button" style="display: none"   onclick="search(10,0)">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
			</span>
		</div>
	</div>
</fieldset> 
<%@ include file="../preReback.jsp"%> 	  
<div class="mini-fit" >
	<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id"  allowAlternating="true"
		onrowdblclick="onRowDblClick" allowResize="true" sortMode="client" allowAlternating="true">
		<div property="columns">
			<div type="indexcolumn" width="50px" headerAlign="center">序号</div>
			<div field="ticketId" width="180" allowSort="false" headerAlign="center" align="">审批单编号</div>
			<div field="approveStatus" width="120" align="center" headerAlign="center"  renderer="CommonUtil.dictRenderer" data-options="{dict:'ApproveStatus'}">审批状态</div>
			<div field="dealTransType" width="90" align="center" headerAlign="center"  renderer="CommonUtil.dictRenderer" data-options="{dict:'dealTransType'}">交易状态</div>
			<div field="counterpartyInstId" width="180px" align="center"  headerAlign="center" >交易对手</div>                            
			<div field="currencyPair" width="100px" align="center"  headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'CurrencyPair'}">货币对</div>
			<div field="price" width="100px" align="right" numberFormat='n8'  headerAlign="center" >利率(%)</div>
			<div field="direction1" width="120px" align="center"  headerAlign="center"  renderer="CommonUtil.dictRenderer" data-options="{dict:'trading'}">本方方向</div>
			<div field="amount1" width="150px" align="right" numberFormat='n4'  headerAlign="center" allowSort="true">本方金额</div>
			<div field="direction2" width="120px" align="center"  headerAlign="center"  renderer="CommonUtil.dictRenderer" data-options="{dict:'trading'}">对方方向</div>
			<div field="amount2" width="150px" align="right" numberFormat='n4'  headerAlign="center" allowSort="true">对方金额</div>
			<div field="valueDate1" width="90px" align="center"  headerAlign="center" allowSort="true">起息日</div>
			<div field="settleMode" width="100px" align="center"  headerAlign="center" allowSort="true">清算方式</div>
			<div field="postDate" width="90px" align="center"  headerAlign="center" allowSort="true">成交日期</div>
			<div field="sponsor" width="100" allowSort="false" headerAlign="center" align="center">审批发起人</div>
			<div field="sponInst" width="100" allowSort="false" headerAlign="center" align="center">审批发起机构</div>
			<div field="aDate" width="180" allowSort="false" headerAlign="center" align="center">审批时间</div>
		</div>
	</div>
</div>   

<script>
	mini.parse();
	var url = window.location.search;
	var prdNo = CommonUtil.getParam(url, "prdNo");
	var prdName = CommonUtil.getParam(url, "prdName");
	var form = new mini.Form("#search_form");
	var grid=mini.get("datagrid");
	var row="";
	//查看详情
	function onRowDblClick(e) {
		var url = CommonUtil.baseWebPath() +"/approvemetal/goldsptEdit.jsp?action=detail";
		var tab = {id: "IrsSptDetail",name:"IrsSptDetail",url:url,title:"黄金即期审批单详情",
					parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
		var paramData = {selectData:grid.getSelected()};
		CommonUtil.openNewMenuTab(tab,paramData);
	}
	//清空
	function clear(){
		var form=new mini.Form("search_form");
		form.clear();
		search(grid.pageSize,0);
	}
	//添加
	function add(){
		var url = CommonUtil.baseWebPath() +"/approvemetal/goldsptEdit.jsp?action=add";
		var tab = {id: "IrsSptAdd",name:"IrsSptAdd",url:url,title:"黄金即期审批单补录",
					parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
		var paramData = {selectData:""};
		CommonUtil.openNewMenuTab(tab,paramData);
	}
	//修改
	function edit(){
		var rows=grid.getSelected();
		if(!rows){
			mini.alert("请选中一行","提示");
			return;
		}
		var url = CommonUtil.baseWebPath() +"/approvemetal/goldsptEdit.jsp?action=edit";
		var tab = {id: "IrsSptEdit",name:"IrsSptEdit",url:url,title:"黄金即期审批单修改",
					parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
		var paramData = {selectData:rows};
		CommonUtil.openNewMenuTab(tab,paramData);
	}
	//删除
	function del(){
		var rows=grid.getSelecteds();
		if(rows.length==0){
			mini.alert("请选中一行","提示");
			return;
		}
		mini.confirm("您确认要删除选中记录?","系统警告",function(value){   
			if (value=='ok'){   
				var data=rows[0];
				params=mini.encode(data);
				CommonUtil.ajax( {
					url:"/IfsApprovemetalGoldController/deleteCfetsmetalGold",
					data:params,
					callback : function(data) {
						mini.alert("删除成功.","系统提示");
						search(10,0);
					}
				});
			}
		});
	}
		function getData(action) {
			var row = null;
			if (action != "add") {
				row = grid.getSelected();
			}
			return row;
		}
	function search(pageSize,pageIndex){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统也提示");
			return;
		}

		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		data['type']="spt";
		var url=null;

		var approveType = mini.get("approveType").getValue();
		if(approveType == "mine"){
			url = "/IfsApprovemetalGoldController/searchMetalGoldSwapMine";
		}else if(approveType == "approve"){
			url = "/IfsApprovemetalGoldController/searchPageMetalGoldUnfinished";
		}else{
			url = "/IfsApprovemetalGoldController/searchPageMetalGoldFinished";
		}
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:url,
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	
	/**************************审批相关****************************/
	//审批日志查看
	function appLog(selections){
		var flow_type = Approve.FlowType.VerifyApproveFlow;
		if(selections.length <= 0){
			mini.alert("请选择要操作的数据","系统提示");
			return;
		}
		if(selections.length > 1){
			mini.alert("系统不支持多笔操作","系统提示");
			return;
		}
		if(selections[0].tradeSource == "3"){
			mini.alert("初始化导入的业务没有审批日志","系统提示");
			return false;
		}
		Approve.approveLog(flow_type,selections[0].ticketId);
	};
	//提交正式审批、待审批
	function verify(selections){
		if(selections.length == 0){
			mini.alert("请选中一条记录！","消息提示");
			return false;
		}else if(selections.length > 1){
			mini.alert("该功能不支持多笔提交，如需多笔提交请点击[批量提交]按钮","系统提示");
			return false;
		}
		if(selections[0]["approveStatus"] != "3" ){
			var url = CommonUtil.baseWebPath() +"/approvemetal/goldsptEdit.jsp?action=approve";
			var tab = {"id": "IrsSptApprove",name:"IrsSptApprove",url:url,title:"黄金即期审批",
						parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
			var paramData = {selectData:selections[0]};
			CommonUtil.openNewMenuTab(tab,paramData);
			// top["win"].showTab(tab);
		}else{
			Approve.approveCommit(Approve.FlowType.VerifyApproveFlow,selections[0]["ticketId"],Approve.OrderStatus.New,"IfsApproveMetalGoldService",prdNo,function(){
				search(grid.pageSize,grid.pageIndex);
			});
		}
	};
	//审批
	function approve(){
		mini.get("approve_commit_btn").setEnabled(false);
		var messageid = mini.loading("系统正在处理...", "请稍后");
		try {
			verify(grid.getSelecteds());
		} catch (error) {
			
		}
		mini.hideMessageBox(messageid);	
		mini.get("approve_commit_btn").setEnabled(true);
	}
	//提交审批
	function commit(){
		mini.get("approve_mine_commit_btn").setEnabled(false);
		var messageid = mini.loading("系统正在处理...", "请稍后");
		try {
			verify(grid.getSelecteds());
		} catch (error) {
			
		}
		mini.hideMessageBox(messageid);	
		mini.get("approve_mine_commit_btn").setEnabled(true);
	}
	//审批日志
	function searchlog(){
		appLog(grid.getSelecteds());
	}
	//打印
    function print(){
		var selections = grid.getSelecteds();
		if(selections == null || selections.length == 0){
			mini.alert('请选择一条要打印的数据！','系统提示');
			return false;
		}else if(selections.length>1){
			mini.alert('暂不支持多条数据同时打印，请选择一条要打印的数据！','系统提示');
			return false;
		}
		var canPrint = selections[0].ticketId;
		
		if(!CommonUtil.isNull(canPrint)){
			var actionStr = CommonUtil.pPath + "/sl/IfsPrintUtilController/exportload/print/"+PrintNo.goldspt+"/" + canPrint;
			$('<form target="_blank" action=' + actionStr + ' method="post"></form>').appendTo('body').submit().remove();
		};
	}
</script>
</body>
</html>
