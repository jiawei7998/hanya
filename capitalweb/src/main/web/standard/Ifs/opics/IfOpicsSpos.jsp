<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>券库管理</title>
</head>
<body style="width: 100%; height: 100%; background: white">
<fieldset class="mini-fieldset">
	<legend>查询条件</legend>
	<div id="search_form" style="width: 70%;">
		<input id="secid" name="secid" class="mini-textbox" labelField="true"  label="债券编号：" labelStyle="text-align:right;"/>
		<input id="DATADATE" name="DATADATE" class="mini-datepicker" labelField="true"  label="持仓日期：" labelStyle="text-align:right;" value="<%=__bizDate%>"/>
		<span style="color: red">注：持仓日期是指抓取大于等于起息日且未到期，支持历史查询</span>
		<span style="float: right; margin-right: 10px">
			<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
			<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
		</span>
	</div>
</fieldset>

<div id="MiniSettleForeignSpot" class="mini-fit" style="margin-top: 2px;">
	<div id="datagrid" class="mini-datagrid borderAll" style="width: 100%; height: 100%;" allowAlternating="true"
		allowResize="true" border="true" sortMode="client" multiSelect="true" onrowdblclick="onRowDblClick">
		<div property="columns">
			<div type="indexcolumn" headerAlign="center" width="40">序号</div>
			<div field="secid"  headerAlign="center" align="center" width="100px">债券代码</div>
			<div field="sname"  headerAlign="center" align="center" width="250px">债券名称</div>
			<div field="vdate"  headerAlign="center" align="center" width="100px">持仓日期</div>
			<div field="mdate"  headerAlign="center" align="center" width="100px">到期日</div>
			<div field="tenor"  headerAlign="center" align="center" width="80px">期限</div>
			<div field="invtype"  headerAlign="center" align="center" width="120px">帐户类型</div>
			<div field="cost"  headerAlign="center" align="center" width="120px">成本中心</div>
			<div field="port"  headerAlign="center" align="center" width="80px">投资组合</div>
			<div field="acctngtype"  headerAlign="center" align="center" width="150px">持仓类别</div>
			<div field="prinamt"  headerAlign="center" align="center" width="150px" numberFormat="#,0.0000">债券面值</div>
			<div field="cuprate"  headerAlign="center" align="center" width="80px">票面利率</div>
			<div field="rbamount"  headerAlign="center" align="center" width="180px" numberFormat="#,0.0000">其中：买断式抵押面额(元)</div>
			<div field="vbamount"  headerAlign="center" align="center" width="180px" numberFormat="#,0.0000">其中：买断式质押面额(元)</div>
			<div field="rpamount"  headerAlign="center" align="center" width="180px" numberFormat="#,0.0000">其中：质押式抵押面额(元)</div>
			<div field="vpamount"  headerAlign="center" align="center" width="180px" numberFormat="#,0.0000">其中：质押式质押面额(元)</div>
			<div field="sbamount"  headerAlign="center" align="center" width="180px" numberFormat="#,0.0000">其中：债券借贷借入面额(元)</div>
			<div field="lbamount"  headerAlign="center" align="center" width="200px" numberFormat="#,0.0000">其中：债券借贷借入抵押面额(元)</div>
			<div field="slamount"  headerAlign="center" align="center" width="200px" numberFormat="#,0.0000">其中：债券借贷借出面额(元)</div>
			<div field="lcamount"  headerAlign="center" align="center" width="200px" numberFormat="#,0.0000">其中：债券借贷借出抵押面额(元)</div>
			<div field="rdamount"  headerAlign="center" align="center" width="200px" numberFormat="#,0.0000">其中：国库定期存款质押面额(元)</div>
			<div field="cbamount"  headerAlign="center" align="center" width="200px" numberFormat="#,0.0000">其中：常备借贷便利质押面额(元)</div>
			<div field="zqamount"  headerAlign="center" align="center" width="200px" numberFormat="#,0.0000">其中：中期借贷便利质押面额(元)</div>
			<div field="yzamount"  headerAlign="center" align="center" width="200px" numberFormat="#,0.0000">其中：押债业务质押面额(元)</div>
			<div field="kyme"  headerAlign="center" align="center" width="180px" numberFormat="#,0.0000">可用面额(元)</div>
			<div field="bz"  headerAlign="center" align="center" width="250px">备注</div>
		</div>
	</div>
</div>

<script>
	/**************************************初始化*******************************************/
	mini.parse();
	var url = window.location.search;
	var grid = mini.get("datagrid");
	var userId='<%=__sessionUser.getUserId()%>';
	grid.on("beforeload", function(e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex;
		var pageSize = e.data.pageSize;
		search(pageSize, pageIndex);
	});

	$(document).ready(function() {
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
			search(10, 0);
		});
	});


	/**************************************按钮方法*******************************************/

	function query() {
		search(grid.pageSize, 0);
	}

	function clear(){
		var form=new mini.Form("search_form");
		form.clear();
		search(10,0);
	}

	// 查询
	function search(pageSize,pageIndex){
		var form = new mini.Form("#search_form");
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统也提示");
			return;
		}
		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:"/IfsOpicsSposController/getSposPage",
			data:params,
			callback : function(data) {
				if(data){
					grid.setTotalCount(data.obj.total);
					grid.setPageIndex(pageIndex);
					grid.setPageSize(pageSize);
					grid.setData(data.obj.rows);
				}
			}
		});
	}

	/**************************************事件方法*******************************************/

</script>
</body>
</html>