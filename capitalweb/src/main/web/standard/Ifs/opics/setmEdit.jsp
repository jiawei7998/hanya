<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>清算方式  维护</title>
  </head>

<body style="width:100%;height:100%;background:white">

    <div class="mini-splitter" style="width:100%;height:100%;">
        <div size="90%" showCollapseButton="false">
            <div id="field_form" class="mini-fit area" style="background:white">
            <div id="bico" class="mini-panel" title="清算方式信息" style="width:100%" allowResize="true" collapseOnTitleClick="true">
              <div class="leftarea">
                  <input id="operator" name="operator" class="mini-hidden" />
                  <input id="status" name="status" class="mini-hidden" />
                  <input id="br" name="br" class="mini-textbox"  label="机构分支" labelField="true" emptyText="请输入机构分支" enabled="false" required="true"  vtype="maxLength:2" style="width:100%"labelStyle="text-align:center;width:170px"/>
                  <input id="settlmeans" name="settlmeans" class="mini-textbox"  label="清算方式" labelField="true"   emptyText="请输入清算方式" required="true"  vtype="maxLength:7" style="width:100%"labelStyle="text-align:center;width:170px" onvaluechanged="checkSetm"/>
                  <input id="descr" name="descr" class="mini-textbox"  label="描述"  required="true"  emptyText="请输入描述" labelField="true"  style="width:100%" labelStyle="text-align:center;width:170px"vtype="maxLength:35" />
              </div>
              <div class="rightarea">                 
                  <input id="lstmntdte" name="lstmntdte" class="mini-datepicker" format="yyyy-MM-dd" label="维护更新日期" emptyText="系统自动生成" labelField="true"   style="width:100%" labelStyle="text-align:center;width:170px" enabled="false"/>
                  <input id="remark" name="remark" class="mini-textarea" label="备注" emptyText="请输入备注" labelField="true"   style="width:100%" labelStyle="text-align:center;width:170px"vtype="maxLength:150"/>
              </div>
            </div>
            
          </div>
        </div>
        <div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
          <div style="margin-bottom:10px; text-align: center;">
              <a class="mini-button" style="display: none"  style="width:120px;" id="save_btn"  onclick="save(1)">保存</a>
          </div>
          <div style="margin-bottom:10px; text-align: center;">
              <a class="mini-button" style="display: none"  style="width:120px;" id="saveAndsync_btn"  onclick="save(2)">保存并同步至opics</a>
          </div>
          <div style="margin-bottom:10px; text-align: center;">
              <a class="mini-button" style="display: none"  style="width:120px;" id="cancel_btn"  onclick="cancel">取消</a>
          </div>
        </div>
    </div>	
    <script type="text/javascript">
        mini.parse();

        //获取当前tab
        var currTab = top["win"].tabs.getActiveTab();
        var params = currTab.params;
        var row=params.selectData;
        var url=window.location.search;
        var action=CommonUtil.getParam(url,"action");
        var form=new mini.Form("#field_form");

        $(document).ready(function(){
            //控制按钮显示
            $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
                initData();
            });
        });

        //初始化数据
        function initData(){
            if($.inArray(action,["edit","detail"])>-1){ //修改、详情
                form.setData(row);
                mini.get("settlmeans").setEnabled(false);
                mini.get("br").setEnabled(false);
                if(row.status == "1"){//已同步，只可以修改描述与备注
                	form.setEnabled(false);
                	mini.get("remark").setEnabled(true);
                	mini.get("saveAndsync_btn").hide();
                }
                
                if(action == "detail"){
                    mini.get("save_btn").hide();
                    mini.get("saveAndsync_btn").hide();
                    form.setEnabled(false);
                }
            }else{//增加 
                mini.get("operator").setValue(userId);
            }
        }
        //保存
         function save(flag){
            //表单验证！！！
            form.validate();
            if (form.isValid() == false) {
            	 mini.alert("表单填写错误,请确认!", "提示信息");
                return;
            }
            var saveUrl=null;
            if(action == "add"){
                saveUrl = "/IfsOpicsSetmController/saveOpicsSetmAdd";
                var data=form.getData(true);
                var list=new Array();
                list.add(data);
                var params=mini.encode(data);
                CommonUtil.ajax({
                    url:saveUrl,
                    data:params,
                    callback:function(data2){
                    	if (data2.code == 'error.common.0000') {
                    		if(flag==1){//弹出保存成功
                    			mini.alert("保存成功!",'提示信息',function(){
        							top["win"].closeMenuTab();
        						});
                    		}else if(flag==2){//弹出保存并同步到opics成功
                    			CommonUtil.ajax({
                                    url: "/IfsOpicsSetmController/syncSetm",
                                    data: list,
                                    callback: function (data1) {
                                        if (data1.code == 'error.common.0000') {
                                            mini.alert("保存并同步成功!",'提示信息',function(){
                                            	top["win"].closeMenuTab();
                                            });
                                            
                                        } else {
                                            mini.alert("保存成功但同步失败!",'提示信息',function(){
                                            	top["win"].closeMenuTab();	
                                            });
                                            
                                        }
                                    }
                                });
                    			
                    		}
    						
    					} else {
    						mini.alert("保存失败!");
    			    	}
                    }
    		    });
            }else if(action == "edit"){
                saveUrl = "/IfsOpicsSetmController/saveOpicsSetmEdit";
                var data=form.getData(true);
                var list=new Array();
                list.add(data);
                var params=mini.encode(data);
                CommonUtil.ajax({
                    url:saveUrl,
                    data:params,
                    callback:function(data2){
                    	if (data2.code == 'error.common.0000') {
                    		if(flag==1){
                    			mini.alert("保存成功!",'提示信息',function(){
           							top["win"].closeMenuTab();
           						});
                    		}else if(flag==2){
                    			CommonUtil.ajax({
                                    url: "/IfsOpicsSetmController/syncSetm",
                                    data: list,
                                    callback: function (data1) {
                                        if (data1.code == 'error.common.0000') {
                                            mini.alert("保存并同步成功!",'提示信息',function(){
                                            	top["win"].closeMenuTab();
                                            });
                                        } else {
                                            mini.alert("保存成功但同步失败!",'提示信息',function(){
                                            	top["win"].closeMenuTab();	
                                            });
                                            
                                        }
                                    }
                                });
                    		}
    					}else {
    						mini.alert("修改失败");
    			    	}
                    }
    		    });
            }

        }
        
        

        function cancel(){
            top["win"].closeMenuTab();
        }

        
        function checkSetm(){
        	var settlmeans = mini.get("settlmeans").getValue();
        	CommonUtil.ajax({
                url: "/IfsOpicsSetmController/searchSetmById",
                data: {settlmeans: settlmeans},
                callback: function (data1) {
                    if (data1.code == 'error.common.0000') {
                    	if(data1.obj != null){
                    		mini.alert("此清算方式'"+settlmeans+"'已存在! 请重新填写!",'提示信息',function(){
                    			mini.get("settlmeans").setValue("");
                            });
                    		
                    	}
                    } else {
                    	
                    }
                }
            });
        	
        }
        
        

    </script>
</body>
</html>