<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
    <head>
        <title>opics系统-清算表-非人民币-MINI页面</title>
        <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    </head>
<body style="width:100%;height:100%;background:white">
    <fieldset class="mini-fieldset title">
    <legend>opics系统 清算信息查询</legend>
    <div id="search_form" style="width: 100%;">
    	<!-- <input id="cust" name="cust" class="mini-textbox" labelField="true" label="交易对手：" labelStyle="text-align:right;" emptyText="请输入交易对手" enabled="false"/> -->
    	<input id="ccy" name="ccy" class="mini-textbox" labelField="true" label="币种：" labelStyle="text-align:right;" emptyText="请输入币种" enabled="false"/>
        <input id="smeans" name="smeans" class="mini-textbox" labelField="true" label="外币清算路径：" labelStyle="text-align:right;" emptyText="请输入外币清算路径" enabled="false"/>
        <input id="nos" name="nos" class="mini-textbox" labelField="true" label="外币清算账户：" labelStyle="text-align:right;" emptyText="请输入外币清算账户" />
        <span style="float:right;margin-right: 150px">
            <a class="mini-button" style="display: none"  id="search_btn"  onclick="search()">查询</a>
            <a class="mini-button" style="display: none"  id="clear_btn"  onclick="clear()">清空</a>
        </span>
    </div>
    </fieldset>
    
    <div class="mini-fit" style="width:100%;height:100%;">
        <div id="nost_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" 
            sortMode="client" allowAlternating="true"   idField="userId" onrowdblclick="onRowDblClick" 
            border="true" allowResize="true" multiSelect="true"  >
            <div property="columns">
            	<div type="checkcolumn"></div>
                <div type="indexcolumn" headerAlign="center" align="center" width="40px">序列</div>
                <div field="smeans" headerAlign="center"  align="center" width="100px" >外币清算路径</div>
                <div field="nos" headerAlign="center"   width="150px" >外币清算账户</div>
                <div field="ccy" headerAlign="center"  width="80px" align="center">币种</div>
                <div field="cust" headerAlign="center" width="120px" align="center">交易对手编号</div>
                <div field="sn" headerAlign="center" width="120px" align="left">交易对手简称</div>
                <div field="cost" headerAlign="center" width="120px" align="center">成本中心</div>
                <div field="status" headerAlign="center" width="100px" renderer="CommonUtil.dictRenderer" data-options="{dict:'DealStatus'}" align="center">同步状态</div>
                <div field="operator" headerAlign="center"  width="120px" align="center">操作人</div>
                <div field="remark" headerAlign="center"  width="200px">备注</div>
                <div field="lstmntdte" headerAlign="center"  align="center" width="160px" renderer="onDateRenderer">最后维护时间</div>
            </div>
        </div>
    </div>
</body>
<script>
    mini.parse();

    $(document).ready(function (){
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn){});
    });

    function onDateRenderer(e) {
    	var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)==null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g,'/'));
        if (value) return mini.formatDate(value, 'yyyy-MM-dd');
    }
    var grid = mini.get("nost_grid");
    grid.on("beforeload", function(e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        searchs(pageSize, pageIndex);
    });
    
    function SetData(data) {
    	mini.get("ccy").setValue(data.ccy);
    	//mini.get("cust").setValue(data.cust);
    	mini.get("smeans").setValue(data.smeans);
    	search();
        
    }
    
    function GetData() {
        var row = grid.getSelected();
        return row;
    }
   
    /* $(document).ready(function() {
		search();
	}); */

    //查询按钮
    function search(){
        searchs(grid.pageSize,0);
    }

    function searchs(pageSize, pageIndex) {
        var form = new mini.Form("#search_form");
        form.validate();
        if (form.isValid() == false) {
            mini.alert("表单填写错误,请确认!", "提示信息");
            return;
        }

        var data = form.getData();
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        data['branchId']=branchId;
        data['status']="1";

        var params = mini.encode(data);

        CommonUtil.ajax({
            url : "/IfsOpicsNostController/searchPageNost",
            data : params,
            callback : function(data) {
                var grid = mini.get("nost_grid");
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }
    

    //清空
    function clear(){
        /* var form=new mini.Form("search_form");
        form.clear(); */
        mini.get("nos").setValue("");
        search();
    }
    
  //双击行选择
    function onRowDblClick(){
        onOk();
    }

    function CloseWindow(action) {
        if (window.CloseOwnerWindow)
            return window.CloseOwnerWindow(action);
        else
            window.close();
    }

    function onOk() {
        CloseWindow("ok");
    }

    //关闭窗口
    function onCancel() {
        CloseWindow("cancel");
    }     

    
  
  
</script>
</html>