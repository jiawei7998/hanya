<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../../global.jsp" %>
<html>
<head>
    <title>opics系统成本中心表</title>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/hideBtn.js"></script>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset title">
    <legend>opics系统成本中心信息查询</legend>
    <div id="search_form" style="width: 100%;">
        <input id="costcent" name="costcent" class="mini-textbox" width="320px" labelField="true" label="成本中心："
               labelStyle="text-align:right;" emptyText="请输入成本中心"/>
        <input id="costdesc" name="costdesc" class="mini-textbox" width="320px" labelField="true" label="成本中心描述："
               labelStyle="text-align:right;" emptyText="请输入成本中心描述"/>
        <input id="status" name="status" class="mini-combobox" width="320px"
               data="CommonUtil.serverData.dictionary.DealStatus" emptyText="请选择同步状态" labelField="true" label="同步状态："
               labelStyle="text-align:right;"/>
        <span style="float:right;margin-right: 150px">
            <a class="mini-button" style="display: none" id="search_btn" onclick="search()">查询</a>
            <a class="mini-button" style="display: none" id="clear_btn" onclick="clear()">清空</a>
        </span>
    </div>
    <span style="margin:2px;display: block;">
<%--            <a class="mini-button" style="display: none"  id="add_btn"  onClick="add();">新增</a>--%>
<%--            <a class="mini-button" style="display: none"  id="edit_btn"  onClick="modify();">修改</a>--%>
<%--            <a class="mini-button" style="display: none"  id="delete_btn"  onClick="del();">删除</a>--%>
<%--            <a class="mini-button" style="display: none"  id="sync"  onClick="sync();">同步至opics</a>--%>
            <a class="mini-button" style="display: none" id="calib" onClick="calibration();">批量校准信息</a>
            <span style="color: red">静态参数以Opics为准，请在opics中添加完成后到前置进行数据批量较准操作</span>
        </span>
</fieldset>

<div class="mini-fit" style="width:100%;height:100%;">
    <div id="prod_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
         sortMode="client" allowAlternating="true" idField="userId" onrowdblclick="onRowDblClick"
         border="true" allowResize="true">
        <div property="columns">
            <div type="indexcolumn" headerAlign="center" align="center" width="40px">序列</div>
            <div field="costcent" headerAlign="center" align="left" width="180px">成本中心</div>
            <div field="costdesc" headerAlign="center" width="200px">成本中心描述</div>
            <%--                <div field="busunit" headerAlign="center" width="120px" align="center">营业单位</div>--%>
            <%--                <div field="orgunit" headerAlign="center" width="120px" align="center">组织单位</div>--%>
            <%--                <div field="costcentpid" headerAlign="center" width="120px" align="center">成本中心父类</div>--%>
            <%--                <div field="costdesccn" headerAlign="center"  width="200px">成本中心中文描述</div>--%>
            <div field="status" headerAlign="center" width="120px" renderer="CommonUtil.dictRenderer"
                 data-options="{dict:'DealStatus'}" align="center">同步状态
            </div>
            <%--                <div field="operator" headerAlign="center"  width="120px" align="center">操作人</div>--%>
            <div field="remark" headerAlign="center" width="200px">备注</div>
            <div field="lstmntdte" headerAlign="center" align="center" width="160px" renderer="onDateRenderer">最后维护时间
            </div>
        </div>
    </div>
</div>
</body>
<script>
    mini.parse();
    var form = new mini.Form("#search_form");
    var grid = mini.get("prod_grid");

    function onDateRenderer(e) {
        var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value) == null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g, '/'));
        if (value) return mini.formatDate(value, 'yyyy-MM-dd');
    }

    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        searchs(pageSize, pageIndex);
    });

    // grid.on("select",function(e){
    // 	var row=e.record;
    // 	if(row.status == "0"){//未处理
    // 		mini.get("sync").setEnabled(true);
    // 		mini.get("delete_btn").setEnabled(true);
    // 	}
    // 	if(row.status == "1"){//已处理
    // 		// mini.get("sync").setEnabled(false);
    // 		mini.get("delete_btn").setEnabled(false);
    // 	}
    // });
    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
            search();
        });
    });

    //查询按钮
    function search() {
        searchs(10, 0);
    }

    function searchs(pageSize, pageIndex) {
        form.validate();
        if (form.isValid() == false) {
            mini.alert("表单填写错误,请确认!", "提示信息");
            return;
        }

        /*  var data = {};
         var costcent =  mini.get("costcent").getValue();
         var status = mini.get("status").getValue();
         data['costcent'] = costcent;
         data['status'] = status; */
        var data = form.getData(true);
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        data['branchId'] = branchId;

        var params = mini.encode(data);

        CommonUtil.ajax({
            url: "/IfsOpicsCostController/searchPageCost",
            data: params,
            callback: function (data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }


    //清空
    function clear() {
        var form = new mini.Form("search_form");
        form.clear();
        search(10, 0);
    }

    //新增
    function add() {
        var url = CommonUtil.baseWebPath() + "/opics/costEdit.jsp?action=add";
        var tab = {
            id: "costAdd", name: "costAdd", title: "成本中心新增",
            url: url, showCloseButton: true, parentId: top["win"].tabs.getActiveTab().name
        };
        var paramData = {selectData: ""};
        CommonUtil.openNewMenuTab(tab, paramData);
    }

    //修改
    function modify() {
        var row = grid.getSelected();
        if (row) {
            var lstmntdte = new Date(row.lstmntdte);
            row.lstmntdte = lstmntdte;
            var url = CommonUtil.baseWebPath() + "/opics/costEdit.jsp?action=edit";
            var tab = {
                id: "costEdit", name: "costEdit", title: "成本中心修改",
                url: url, showCloseButton: true, parentId: top["win"].tabs.getActiveTab().name
            };
            var paramData = {selectData: row};
            CommonUtil.openNewMenuTab(tab, paramData);

        } else {
            mini.alert("请选中一条记录！", "消息提示");
        }
    }

    //删除
    function del() {
        var row = grid.getSelected();
        if (row) {
            mini.confirm("您确认要删除选中记录?", "系统警告", function (value) {
                if (value == "ok") {
                    CommonUtil.ajax({
                        url: "/IfsOpicsCostController/deleteCost",
                        data: {costcent: row.costcent},
                        callback: function (data) {
                            if (data.code == 'error.common.0000') {
                                mini.alert("删除成功!");
                                grid.reload();
                            } else {
                                mini.alert("删除失败!");
                            }
                        }
                    });
                }
            });
        } else {
            mini.alert("请选中一条记录！", "消息提示");
        }

    }


    //双击详情
    function onRowDblClick(e) {
        var row = grid.getSelected();
        var lstmntdte = new Date(row.lstmntdte);
        row.lstmntdte = lstmntdte;
        if (row) {
            var url = CommonUtil.baseWebPath() + "/opics/costEdit.jsp?action=detail";
            var tab = {id: "costDetail", name: "tradeParamDetail", title: "成本中心详情", url: url, showCloseButton: true};
            var paramData = {selectData: row};
            CommonUtil.openNewMenuTab(tab, paramData);
        } else {
            mini.alert("请选中一条记录！", "消息提示");
        }
    }

    //同步
    function sync() {
        var row = grid.getSelected();
        if (row) {
            mini.confirm("您确认要同步选中记录?", "系统警告", function (value) {
                if (value == "ok") {
                    CommonUtil.ajax({
                        url: "/IfsOpicsCostController/syncCost",
                        data: {costcent: row.costcent},
                        callback: function (data) {
                            mini.alert(data.obj.retMsg, data.obj.retCode);
                            grid.reload();
                        }
                    });
                }
            });
        } else {
            mini.alert("请选中一条记录！", "消息提示");
        }

    }

    //批量校准信息
    function calibration() {
        var rows = grid.getSelecteds();
        if (rows.length > 0) { //选中行校准
            var str = "";
            if (rows.length == 1) {
                str = rows[0].costcent;
            } else {
                for (var i = 0; i < rows.length; i++) {
                    if (i == 0) {
                        str = rows[i].costcent + ",";
                    } else {
                        if (i == rows.length - 1) {
                            str = str + rows[i].costcent;
                        } else {
                            str = str + rows[i].costcent + ",";
                        }
                    }
                }
            }
            mini.confirm("您确认要校准选中记录?", "系统警告", function (value) {
                if (value == "ok") {
                    CommonUtil.ajax({
                        url: "/IfsOpicsCostController/batchCalibration",
                        data: {costcent: str, type: 1},
                        callback: function (data) {
                            mini.alert(data.obj.retMsg, data.obj.retCode);
                            grid.reload();
                        }
                    });
                }
            });
        } else { //未选中就全部校准
            mini.confirm("您确认要校准全部记录?", "系统警告", function (value) {
                if (value == "ok") {
                    CommonUtil.ajax({
                        url: "/IfsOpicsCostController/batchCalibration",
                        data: {type: 2},
                        callback: function (data) {
                            mini.alert(data.obj.retMsg, data.obj.retCode);
                            grid.reload();
                        }
                    });
                }
            });
        }
    }
</script>
</html>