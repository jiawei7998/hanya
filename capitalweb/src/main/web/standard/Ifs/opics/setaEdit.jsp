<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>SETA</title>
  </head>

<body style="width:100%;height:100%;background:white">

    <div class="mini-splitter" style="width:100%;height:100%;">
        <div size="90%" showCollapseButton="false">
            <div id="field_form" class="mini-fit area" style="background:white">
            <div id="bico" class="mini-panel" title="SETA信息" style="width:100%" allowResize="true" collapseOnTitleClick="true">
              <div class="leftarea">
              	  <input id="br" name="br" class="mini-textbox"  label="机构分支" labelField="true" emptyText="请输入机构分支" required="true"  vtype="maxLength:2" style="width:100%"labelStyle="text-align:center;width:170px"/>
              	  <input id="smeans" name="smeans" class="mini-textbox"  label="清算路径" labelField="true" emptyText="请输入清算路径" required="true"  vtype="maxLength:7" style="width:100%"labelStyle="text-align:center;width:170px" onvaluechanged="checkSeta"/>
              	  <input id="sacct" name="sacct" class="mini-textbox"  label="清算账户" labelField="true" emptyText="请输入清算账户" required="true"  vtype="maxLength:15" style="width:100%"labelStyle="text-align:center;width:170px" onvaluechanged="checkSeta"/>
              	  <input id="ccy" name="ccy" class="mini-textbox"  label="币种" labelField="true" emptyText="请输入币种" required="true"  vtype="maxLength:3" style="width:100%"labelStyle="text-align:center;width:170px"/>
                  <input id="operator" name="operator" class="mini-hidden" />
                  <input id="status" name="status" class="mini-hidden" />
              </div>
              <div class="rightarea">
                  <input id="costcent" name="costcent" class="mini-textbox"  label="成本中心" labelField="true" emptyText="请输入成本中心" vtype="maxLength:11" style="width:100%"labelStyle="text-align:center;width:170px"/>
                  <input id="cno" name="cno" class="mini-textbox"  label="交易对手" labelField="true" emptyText="请输入交易对手" vtype="maxLength:11" style="width:100%"labelStyle="text-align:center;width:170px"/>
              	  <input id="lstmntdte" name="lstmntdte" class="mini-datepicker" format="yyyy-MM-dd" label="最后维护时间" emptyText="系统自动生成" labelField="true"   style="width:100%" labelStyle="text-align:center;width:170px" enabled="false"/>
              	  <input id="remark" name="remark" class="mini-textarea" label="备注" emptyText="请输入备注" labelField="true"   style="width:100%" labelStyle="text-align:center;width:170px"vtype="maxLength:50"/>		
              </div>
            </div>
            
          </div>
        </div>
        <div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
          <div style="margin-bottom:10px; text-align: center;">
              <a class="mini-button" style="display: none"  style="width:120px;" id="save_btn"  onclick="save(1)">保存</a>
          </div>
          <div style="margin-bottom:10px; text-align: center;">
              <a class="mini-button" style="display: none"  style="width:120px;" id="saveAndsync_btn"  onclick="save(2)">保存并同步至opics</a>
          </div>
          <div style="margin-bottom:10px; text-align: center;">
              <a class="mini-button" style="display: none"  style="width:120px;" id="cancel_btn"  onclick="cancel">取消</a>
          </div>
        </div>
    </div>	
    <script type="text/javascript">
        mini.parse();

        //获取当前tab
        var currTab = top["win"].tabs.getActiveTab();
        var params = currTab.params;
        var row=params.selectData;
        var url=window.location.search;
        var action=CommonUtil.getParam(url,"action");
        var form=new mini.Form("#field_form");

        $(document).ready(function(){
            //控制按钮显示
            $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
                initData();
            });
        });

        //初始化数据
        function initData(){
            if($.inArray(action,["edit","detail"])>-1){ //修改、详情
                form.setData(row);
                mini.get("br").setEnabled(false);
                mini.get("smeans").setEnabled(false);
                mini.get("sacct").setEnabled(false);
                if(row.status == "1"){//已同步，只可以备注
                	form.setEnabled(false);
                	mini.get("remark").setEnabled(true);
                	mini.get("saveAndsync_btn").hide();
                }
                
                if(action == "detail"){
                    mini.get("save_btn").hide();
                    mini.get("saveAndsync_btn").hide();
                    form.setEnabled(false);
                }
            }else{//增加 
                mini.get("operator").setValue(userId);
            }
        }
        //保存
         function save(flag){
            //表单验证！！！
            form.validate();
            if (form.isValid() == false) {
            	 mini.alert("表单填写错误,请确认!", "提示信息");
                return;
            }
            var saveUrl=null;
            if(action == "add"){
                saveUrl = "/IfsOpicsSetaController/saveOpicsSetaAdd";
                var data=form.getData(true);
                var params=mini.encode(data);
                CommonUtil.ajax({
                    url:saveUrl,
                    data:params,
                    callback:function(data2){
                    	if (data2.code == 'error.common.0000') {
                    		if(flag==1){//弹出保存成功
                    			mini.alert("保存成功!",'提示信息',function(){
        							top["win"].closeMenuTab();
        						});
                    		}else if(flag==2){//弹出保存并同步到opics成功
                    			CommonUtil.ajax({
                                    url: "/IfsOpicsSetaController/syncSeta",
                                    data: {smeans: data.smeans,sacct: data.sacct},
                                    callback: function (data1) {
                                        if (data1.code == 'error.common.0000') {
                                            mini.alert("保存并同步成功!",'提示信息',function(){
                                            	top["win"].closeMenuTab();
                                            });
                                            
                                        } else {
                                            mini.alert("保存成功但同步失败!",'提示信息',function(){
                                            	top["win"].closeMenuTab();	
                                            });
                                            
                                        }
                                    }
                                });
                    			
                    		}
    						
    					} else {
    						mini.alert("保存失败!");
    			    	}
                    }
    		    });
            }else if(action == "edit"){
                saveUrl = "/IfsOpicsSetaController/saveOpicsSetaEdit";
                var data=form.getData(true);
                var params=mini.encode(data);
                CommonUtil.ajax({
                    url:saveUrl,
                    data:params,
                    callback:function(data2){
                    	if (data2.code == 'error.common.0000') {
                    		if(flag==1){//弹出保存成功
                    			mini.alert("保存成功!",'提示信息',function(){
        							top["win"].closeMenuTab();
        						});
                    		}else if(flag==2){//弹出保存并同步到opics成功
                    			CommonUtil.ajax({
                                    url: "/IfsOpicsSetaController/syncSeta",
                                    data: {smeans: data.smeans,sacct: data.sacct},
                                    callback: function (data1) {
                                        if (data1.code == 'error.common.0000') {
                                        	mini.alert("保存并同步成功!",'提示信息',function(){
                                            	top["win"].closeMenuTab();
                                            });
                                        } else {
                                        	mini.alert("保存成功但同步失败!",'提示信息',function(){
                                            	top["win"].closeMenuTab();	
                                            });
                                        }
                                    }
                                });
                    			
                    		}
    					} else {
    						mini.alert("修改失败");
    			    	}
                    }
    		    });
            }

           
        }
        
        

        function cancel(){
            top["win"].closeMenuTab();
        }
        
        function checkSeta(){
        	var smeans = mini.get("smeans").getValue();
        	var sacct = mini.get("sacct").getValue();
        	CommonUtil.ajax({
                url: "/IfsOpicsSetaController/searchSetaById",
                data: {smeans: smeans,sacct: sacct},
                callback: function (data1) {
                    if (data1.code == 'error.common.0000') {
                    	if(data1.obj != null){
                    		mini.alert("此SETA'"+"'已存在! 请重新填写!",'提示信息',function(){
                    			mini.get("smeans").setValue("");
                    			mini.get("sacct").setValue("");
                            });
                    		
                    	}
                    } else {
                    	
                    }
                }
            });
        	
        }
    </script>
</body>
</html>