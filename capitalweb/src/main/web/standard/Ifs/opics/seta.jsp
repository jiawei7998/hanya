<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
    <head>
        <title>opics系统-SETA表</title>
        <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
        <script type="text/javascript" src="<%=basePath%>/miniScript/hideBtn.js"></script>
    </head>
<body style="width:100%;height:100%;background:white">
    <fieldset class="mini-fieldset title">
    <legend>opics系统 SETA信息查询</legend>
    <div id="search_form" style="width: 100%;">
        <input id="smeans" name="smeans" class="mini-textbox" labelField="true" label="清算路径：" labelStyle="text-align:right;" emptyText="请输入清算路径" />
        <input id="sacct" name="sacct" class="mini-textbox" labelField="true" label="清算账户：" labelStyle="text-align:right;" emptyText="请输入清算账户" />
       	<input id="status" name="status" class="mini-combobox" data="CommonUtil.serverData.dictionary.DealStatus" width="280px" emptyText="请选择同步状态" labelField="true"  label="同步状态：" labelStyle="text-align:right;"/>
        <span style="float:right;margin-right: 150px">
            <a class="mini-button" style="display: none"  id="search_btn"  onclick="search()">查询</a>
            <a class="mini-button" style="display: none"  id="clear_btn"  onclick="clear()">清空</a>
        </span>
    </div>
    </fieldset>
    <span style="margin:2px;display: block;">
       <!--  <a class="mini-button" style="display: none"  id="add_btn"  onClick="add();">新增</a>
        <a class="mini-button" style="display: none"  id="edit_btn"  onClick="modify();">修改</a>
        <a class="mini-button" style="display: none"  id="delete_btn"  onClick="del();">删除</a>
        <a class="mini-button" style="display: none"  id="sync"  onClick="sync();">同步至opics</a> -->
        <a class="mini-button" style="display: none"  id="calib"  onClick="calibration();">批量校准信息</a>
    </span>
    <div class="mini-fit" style="width:100%;height:100%;">
        <div id="seta_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" 
            sortMode="client" allowAlternating="true"   idField="userId" onrowdblclick="onRowDblClick" 
            border="true" allowResize="true" multiSelect="true"  >
            <div property="columns">
            	<div type="checkcolumn"></div>
                <div type="indexcolumn" headerAlign="center" align="center" width="40px">序列</div>
                <div field="br" headerAlign="center"  align="center" width="100px" >机构分支</div>
                <div field="smeans" headerAlign="center"  width="160px">清算路径</div>
                <div field="sacct" headerAlign="center" width="200px" align="left">清算账户</div>
                <div field="ccy" headerAlign="center"  width="120px" align="center">币种</div>
                <div field="costcent" headerAlign="center"  width="120px" align="center">成本中心</div>
                <div field="cno" headerAlign="center"  width="120px" align="center">交易对手</div>
                <div field="status" headerAlign="center" width="100px" renderer="CommonUtil.dictRenderer" data-options="{dict:'DealStatus'}" align="center">同步状态</div>
                <div field="operator" headerAlign="center"  width="120px" align="center">操作者</div>
                <div field="remark" headerAlign="center"  width="200px">备注</div>
                <div field="lstmntdte" headerAlign="center"  align="center" width="160px" renderer="onDateRenderer">最后维护时间</div>
            </div>
        </div>
    </div>
</body>
<script>
    mini.parse();
    function onDateRenderer(e) {
    	var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)==null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g,'/'));
        if (value) return mini.formatDate(value, 'yyyy-MM-dd');
    }
    var grid = mini.get("seta_grid");
    grid.on("beforeload", function(e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        searchs(pageSize, pageIndex);
    });

    /* grid.on("select",function(e){
		var row=e.record;
		if(row.status == "0"){//未同步
			mini.get("sync").setEnabled(true);
			mini.get("edit_btn").setEnabled(true);
			mini.get("delete_btn").setEnabled(true);
		}
		if(row.status == "1"){//已同步
			mini.get("sync").setEnabled(false);
			mini.get("edit_btn").setEnabled(true);
			mini.get("delete_btn").setEnabled(false);
		}
	});  */
    $(document).ready(function() {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
            search();
        });
	});

    //查询按钮
    function search(){
        searchs(grid.pageSize,0);
    }

    function searchs(pageSize, pageIndex) {
        var form = new mini.Form("#search_form");
        form.validate();
        if (form.isValid() == false) {
            mini.alert("表单填写错误,请确认!", "提示信息");
            return;
        }

        var data = form.getData();
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        data['branchId']=branchId;

        var params = mini.encode(data);

        CommonUtil.ajax({
            url : "/IfsOpicsSetaController/searchPageSeta",
            data : params,
            callback : function(data) {
                var grid = mini.get("seta_grid");
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }
    

    //清空
    function clear(){
        var form=new mini.Form("search_form");
        form.clear();
        search();
    }

    //新增
    function add(){
        var url = CommonUtil.baseWebPath() + "/opics/setaEdit.jsp?action=add";
        var tab = { id: "setaAdd", name: "setaAdd", title: "SETA新增", 
        url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
        var paramData = {selectData:""};
        CommonUtil.openNewMenuTab(tab,paramData);
    }
    
    //修改
    function modify(){
        var row = grid.getSelected();
        if(row){
            var url = CommonUtil.baseWebPath() + "/opics/setaEdit.jsp?action=edit";
            var tab = { id: "setaEdit", name: "setaEdit", title: "SETA修改", 
            url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
            var paramData = {selectData:row};
            CommonUtil.openNewMenuTab(tab,paramData);

        } else {
            mini.alert("请选中一条记录！","消息提示");
        }
    }
    
    //删除
    function del() {
        var row = grid.getSelected();
        if (row) {
            mini.confirm("您确认要删除选中记录?","系统警告",function(value){
                if(value=="ok"){
                    CommonUtil.ajax({
                        url: "/IfsOpicsSetaController/deleteSeta",
                        data: {smeans: row.smeans},
                        callback: function (data) {
                            if (data.code == 'error.common.0000') {
                                mini.alert("删除成功!");
                                grid.reload();
                            } else {
                                mini.alert("删除失败!");
                            }
                        }
                    });
                }
            });
        }else {
            mini.alert("请选中一条记录！", "消息提示");
        }

    }


    //双击详情
    function onRowDblClick(e) {
        var row = grid.getSelected();
        var lstmntdte = new Date(row.lstmntdte);
        row.lstmntdte=lstmntdte;
        if(row){
                var url = CommonUtil.baseWebPath() + "/opics/setaEdit.jsp?action=detail";
                var tab = { id: "setaDetail", name: "setaDetail", title: "SETA详情", url: url ,showCloseButton:true};
                var paramData = {selectData:row};
                CommonUtil.openNewMenuTab(tab,paramData);
        } else {
            mini.alert("请选中一条记录！","消息提示");
        }
    }
    
  //同步
    function sync() {
        var row = grid.getSelected();
        if (row) {
            mini.confirm("您确认要同步选中记录?","系统警告",function(value){
                if(value=="ok"){
                    CommonUtil.ajax({
                        url: "/IfsOpicsSetaController/syncSeta",
                        data: {smeans: row.smeans,sacct: row.sacct},
                        callback: function (data) {
                        	mini.alert(data.obj.retMsg,data.obj.retCode);
                        	grid.reload();
                        }
                    });
                }
            });
        }else {
            mini.alert("请选中一条记录！", "消息提示");
        }

    }
  //批量校准
    function calibration(){
  	  var rows = grid.getSelecteds();
  	  
  	  
  	  if(rows.length>0){//选中行校准
  		  var str="";
  	  	  var str1="";
  	      if(rows.length==1){
  	      	str=rows[0].smeans;	
  	      	str1=rows[0].sacct;
  	      }else{
  	      	for(var i=0;i<rows.length;i++){
  	          	if(i == 0){
  	          		str=rows[i].smeans+",";	
  	          		str1=rows[i].sacct+",";
  	          	}else{
  	          		
  	          		if(i==rows.length-1){
  	          			str=str+rows[i].smeans;
  	          			str1=str1+rows[i]+sacct;
  	          		}else{
  	          			str=str+rows[i].smeans+",";
  	          			str1=str1+rows[i]+sacct+",";
  	          		}
  	          		
  	          	}
  	          	
  	          }
  	      	
  	      }
  		  mini.confirm("您确认要校准选中记录?","系统警告",function(value){
  			  if(value=="ok"){
  				  CommonUtil.ajax({
  			          url: "/IfsOpicsSetaController/batchCalibration",
  			          data: {smeans:str,sacct:str1,type:1},
  			          callback: function (data) {
  			        	mini.alert(data.obj.retMsg,data.obj.retCode);
  			        	grid.reload();
  			          }
  			      });
  				  
  			  }
  		  });//end confirm
  		  
  	  }else{//未选中就全部校准
  		  mini.confirm("您确认要校准全部记录?","系统警告",function(value){
  			  if(value=="ok"){
  				  CommonUtil.ajax({
  			          url: "/IfsOpicsSetaController/batchCalibration",
  			          data: {type:2},
  			          callback: function (data) {
  			        	mini.alert(data.obj.retMsg,data.obj.retCode);
  			        	grid.reload();
  			          }
  			      });
  				  
  			  }
  		  });//end confirm
  		  
  		  
  	  }//end else
  	  
  	 }
</script>
</html>