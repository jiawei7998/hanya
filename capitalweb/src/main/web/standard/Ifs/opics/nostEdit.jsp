<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>NOST  维护</title>
  </head>

<body style="width:100%;height:100%;background:white">

    <div class="mini-splitter" style="width:100%;height:100%;">
        <div size="90%" showCollapseButton="false">
            <div id="field_form" class="mini-fit area" style="background:white">
            <div id="bico" class="mini-panel" title="NOST信息" style="width:100%" allowResize="true" collapseOnTitleClick="true">
              <div class="leftarea">
                  <input id="operator" name="operator" class="mini-hidden" />
                  <input id="status" name="status" class="mini-hidden" />
                  <input id="br" name="br" class="mini-textbox"  label="机构分支" labelField="true" emptyText="请输入机构分支" required="true"  vtype="maxLength:2" style="width:100%"labelStyle="text-align:center;width:170px"/>
                  <input id="smeans" name="smeans" class="mini-textbox"  label="清算路径" labelField="true" emptyText="请输入清算路径" required="true"  vtype="maxLength:7" style="width:100%"labelStyle="text-align:center;width:170px"/>
                  <input id="nos" name="nos" class="mini-textbox"  label="外币清算账户" labelField="true"   emptyText="请输入外币清算账户" required="true"  vtype="maxLength:15" style="width:100%"labelStyle="text-align:center;width:170px" onvaluechanged="checkNos"/>
                  <input id="ccy" name="ccy" class="mini-textbox"  label="币种" labelField="true" emptyText="请输入币种" required="true"  vtype="maxLength:3" style="width:100%"labelStyle="text-align:center;width:170px"/>
              </div>
              <div class="rightarea">
                  <input id="cost" name="cost" class="mini-textbox"  label="成本中心" labelField="true" value="1000000000" readonly="readonly" vtype="maxLength:11"  style="width:100%" labelStyle="text-align:center;width:170px"/>
                  <input id="cust" name="cust" class="mini-textbox"  label="交易对手" labelField="true" emptyText="请输入交易对手" vtype="maxLength:11" style="width:100%"labelStyle="text-align:center;width:170px"/>
              	  <input id="lstmntdte" name="lstmntdte" class="mini-datepicker" format="yyyy-MM-dd" label="最后维护时间" emptyText="系统自动生成" labelField="true"   style="width:100%" labelStyle="text-align:center;width:170px" enabled="false"/>
              	  <input id="genledgno" name="genledgno"  class="mini-textbox"  label="OPICS科目号" labelField="true" value="10101010" readonly="readonly"  style="width:100%" labelStyle="text-align:center;width:170px"/>
              	  <input id="remark" name="remark" class="mini-textarea" label="备注" emptyText="请输入备注" labelField="true"   style="width:100%" labelStyle="text-align:center;width:170px"vtype="maxLength:150"/>
              </div>
            </div>
            
          </div>
        </div>
        <div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
          <div style="margin-bottom:10px; text-align: center;">
              <a class="mini-button" style="display: none"  style="width:120px;" id="save_btn"  onclick="save(1)">保存</a>
          </div>
          <div style="margin-bottom:10px; text-align: center;">
              <a class="mini-button" style="display: none"  style="width:120px;" id="saveAndsync_btn"  onclick="save(2)">保存并同步至opics</a>
          </div>
          <div style="margin-bottom:10px; text-align: center;">
              <a class="mini-button" style="display: none"  style="width:120px;" id="cancel_btn"  onclick="cancel">取消</a>
          </div>
        </div>
    </div>	
    <script type="text/javascript">
        mini.parse();

        //获取当前tab
        var currTab = top["win"].tabs.getActiveTab();
        var params = currTab.params;
        var row=params.selectData;
        var url=window.location.search;
        var action=CommonUtil.getParam(url,"action");
        var form=new mini.Form("#field_form");

        $(document).ready(function(){
            //控制按钮显示
            $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
                initData();
            });
        });

        //初始化数据
        function initData(){
            if($.inArray(action,["edit","detail"])>-1){ //修改、详情
                form.setData(row);
                mini.get("nos").setEnabled(false);
                mini.get("br").setEnabled(false);
                mini.get("smeans").setEnabled(false);
                if(row.status == "1"){//已同步，只可以修改备注
                	form.setEnabled(false);
                	mini.get("remark").setEnabled(true);
                	mini.get("saveAndsync_btn").hide();
                }
                
                if(action == "detail"){
                    mini.get("save_btn").hide();
                    mini.get("saveAndsync_btn").hide();
                    form.setEnabled(false);
                }
            }else{//增加 
                mini.get("operator").setValue(userId);
            }
        }
        //保存
         function save(flag){
            //表单验证！！！
            form.validate();
            if (form.isValid() == false) {
            	 mini.alert("表单填写错误,请确认!", "提示信息");
                return;
            }
            var saveUrl=null;
            if(action == "add"){
                saveUrl = "/IfsOpicsNostController/saveOpicsNostAdd";
                var data=form.getData(true);
                var params=mini.encode(data);
                CommonUtil.ajax({
                    url:saveUrl,
                    data:params,
                    callback:function(data2){
                    	if (data2.code == 'error.common.0000') {
                    		if(flag==1){//弹出保存成功
                    			mini.alert("保存成功!",'提示信息',function(){
        							top["win"].closeMenuTab();
        						});
                    		}else if(flag==2){//弹出保存并同步到opics成功
                    			CommonUtil.ajax({
                                    url: "/IfsOpicsNostController/syncNost",
                                    data: {nos: data.nos},
                                    callback: function (data1) {
                                        if (data1.code == 'error.common.0000') {
                                            mini.alert("保存并同步成功!",'提示信息',function(){
                                            	top["win"].closeMenuTab();
                                            });
                                            
                                        } else {
                                            mini.alert("保存成功但同步失败!",'提示信息',function(){
                                            	top["win"].closeMenuTab();	
                                            });
                                            
                                        }
                                    }
                                });
                    			
                    		}
    						
    					} else {
    						mini.alert("保存失败!");
    			    	}
                    }
    		    });
            }else if(action == "edit"){
                saveUrl = "/IfsOpicsNostController/saveOpicsNostEdit";
                var data=form.getData(true);
                var params=mini.encode(data);
                CommonUtil.ajax({
                    url:saveUrl,
                    data:params,
                    callback:function(data2){
                    	if (data2.code == 'error.common.0000') {
                    		if(flag==1){//弹出保存成功
                    			mini.alert("保存成功!",'提示信息',function(){
        							top["win"].closeMenuTab();
        						});
                    		}else if(flag==2){//弹出保存并同步到opics成功
                    			CommonUtil.ajax({
                                    url: "/IfsOpicsNostController/syncNost",
                                    data: {nos: data.nos},
                                    callback: function (data1) {
                                        if (data1.code == 'error.common.0000') {
                                        	mini.alert("保存并同步成功!",'提示信息',function(){
                                            	top["win"].closeMenuTab();
                                            });
                                        } else {
                                        	mini.alert("保存成功但同步失败!",'提示信息',function(){
                                            	top["win"].closeMenuTab();	
                                            });
                                        }
                                    }
                                });
                    			
                    		}
    					} else {
    						mini.alert("修改失败");
    			    	}
                    }
    		    });
            }

           
        }
        
        

        function cancel(){
            top["win"].closeMenuTab();
        }

        function checkNos(){
        	var nos = mini.get("nos").getValue();
        	CommonUtil.ajax({
                url: "/IfsOpicsNostController/searchNostById",
                data: {nos: nos},
                callback: function (data1) {
                    if (data1.code == 'error.common.0000') {
                    	if(data1.obj != null){
                    		mini.alert("此外币清算账户'"+nos+"'已存在! 请重新填写!",'提示信息',function(){
                    			mini.get("nos").setValue("");
                            });
                    		
                    	}
                    } else {
                    	
                    }
                }
            });
        	
        }
        
        

    </script>
</body>
</html>