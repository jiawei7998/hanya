<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>产品类型  维护</title>
  </head>

<body style="width:100%;height:100%;background:white">
    <div>
        <span id="labell"><a href="javascript:CommonUtil.activeTab();">产品类型</a> >> 修改</span>
        <div id="field" class="fieldset-body">
            <table id="field_form"  style="text-align:left;margin:auto;width:100%;" class="mini-sltable">
                <tr>
                    <td style="width:50%">
                     	<input id="operator" name="operator" class="mini-hidden" />
            			<input id="status" name="status" class="mini-hidden" />
                        <input id="prodcode" name="prodcode" class="mini-buttonedit" onbuttonclick="onButtonEdit"  label="产品代码" labelField="true"   emptyText="请选择产品代码" required="true"  style="width:100%"labelStyle="text-align:left;width:170px" allowInput="false"/>
                    </td>
                    <td style="width:50%">
                        <input id="type" name="type" class="mini-textbox"  label="产品类型" labelField="true"  emptyText="请输入产品类型"  vtype="maxLength:2" required="true"  style="width:100%"labelStyle="text-align:left;width:170px" onvalidation="onEnglishAndNumberValidation"/>
                    </td>
                </tr>
                <tr>
                    <td style="width:50%">
                        <input id="al" name="al" class="mini-radiobuttonlist"  label="所属模块" required="true"   labelField="true"  style="width:100%" labelStyle="text-align:left;width:170px" textField="text" valueField="id" value="N" data="[{id:'N',text:'N/A'},{id:'L',text:'Liability'},{id:'A',text:'Asset'}]"/>
                    </td>
                    <td style="width:50%">
                        <input id="lstmntdte" name="lstmntdte" class="mini-datepicker" format="yyyy-MM-dd" label="维护更新日期" emptyText="系统自动生成" labelField="true"   style="width:100%" labelStyle="text-align:left;width:170px" enabled="false"/>
                    </td>
                </tr>
                <tr>
               		<td style="width:50%">
                        <input id="descr" name="descr" class="mini-textbox" label="产品类型描述" emptyText="请输入产品类型描述" labelField="true"   style="width:100%" labelStyle="text-align:left;width:170px" vtype="maxLength:35" required="true" />
                    </td>
                   <td style="width:50%">
                        <input id="descrcn" name="descrcn" class="mini-textbox"  label="产品类型中文描述" labelField="true" emptyText="请输入产品类型中文描述"  vtype="maxLength:40" style="width:100%"labelStyle="text-align:left;width:170px"/>
                    </td>
                </tr>
                <tr>
                	
                    <td style="width:50%">
                        <input id="remark" name="remark" class="mini-textbox" label="备注" emptyText="请输入备注" labelField="true"   style="width:100%" labelStyle="text-align:left;width:170px" vtype="maxLength:50"/>
                    </td>
                </tr>
                
                <tr>
                    <td colspan="2" style="text-align:center;">
                        <a  id="save_btn" class="mini-button" style="display: none"    onclick="save">保存</a>
                        <a id="cancel_btn" class="mini-button" style="display: none"    onclick="cancel">取消</a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <script type="text/javascript">
        mini.parse();

        //获取当前tab
        var currTab = top["win"].tabs.getActiveTab();
        var params = currTab.params;
        var row=params.selectData;
        var url=window.location.search;
        var action=CommonUtil.getParam(url,"action");
        var form=new mini.Form("#field_form");

        $(document).ready(function(){
            //控制按钮显示
            $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
                initData();
            });
        });

        //初始化数据
        function initData(){
            if($.inArray(action,["edit","detail"])>-1){ //修改、详情
                mini.get("prodcode").setValue(row.prodcode);
     			mini.get("prodcode").setText(row.prodcode);
     			mini.get("prodcode").setEnabled(false);
     			mini.get("type").setEnabled(false);
     			if(row.status=="1"){ //已同步时
               	 	mini.get("al").setEnabled(false);
                    mini.get("descr").setEnabled(false);
               }
     			
     			
            	form.setData(row);
                if(action == "detail"){
                    $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>产品类型</a> >> 详情");
                    mini.get("save_btn").hide();
                    form.setEnabled(false);
                }
            }else{//增加 
            	
                $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>产品类型</a> >> 新增");
                mini.get("operator").setValue(userId);
                
               
            }
        }
        //保存
         function save(){
            //表单验证！！！
            form.validate();
            if (form.isValid() == false) {
                return;
            }
            var saveUrl=null;
            if(action == "add"){
                saveUrl = "/IfsOpicsTypeController/saveOpicsTypeAdd";
                var data=form.getData(true);
                var params=mini.encode(data);
                CommonUtil.ajax({
                    url:saveUrl,
                    data:params,
                    callback:function(data){
                    	if (data.code == 'error.common.0000') {
    						mini.alert("保存成功",'提示信息',function(){
    							top["win"].closeMenuTab();
    						});
    					} else {
    						mini.alert("保存失败");
    			    	}
                    }
    		    });
            }else if(action == "edit"){
                saveUrl = "/IfsOpicsTypeController/saveOpicsTypeEdit";
                var data=form.getData(true);
                var params=mini.encode(data);
                CommonUtil.ajax({
                    url:saveUrl,
                    data:params,
                    callback:function(data){
                    	if (data.code == 'error.common.0000') {
    						mini.alert(data.desc,'提示信息',function(){
    							top["win"].closeMenuTab();
    						});
    					} else {
    						mini.alert("修改失败");
    			    	}
                    }
    		    });
            }

           
        }

        function cancel(){
            top["win"].closeMenuTab();
        }

        //英文、数字、下划线 的验证
        function onEnglishAndNumberValidation(e) {
            if(e.value == "" || e.value == null){//值为空，就不做校验
                return;
            }
            if (e.isValid) {
                if (isEnglishAndNumber(e.value) == false) {
                    e.errorText = "必须输入英文或数字";
                    e.isValid = false;
                }
            }
        }

        /* 是否英文、数字、下划线 */
        function isEnglishAndNumber(v) {
            var re = new RegExp("^[0-9a-zA-Z\_\-]+$");
            if (re.test(v)) return true;
            return false;
        }
        
        function onButtonEdit(e) {
            var btnEdit = this;
            mini.open({
                url: CommonUtil.baseWebPath() + "/opics/typeMini.jsp",
                title: "选择产品列表",
                width: 900,
                height: 600,
                ondestroy: function (action) {
                    if (action == "ok") {
                        var iframe = this.getIFrameEl();
                        var data = iframe.contentWindow.GetData();
                        data = mini.clone(data);    //必须
                        if (data) {
                            btnEdit.setValue(data.prodcode);
                            btnEdit.setText(data.prodcode);
                            btnEdit.focus();
                        }
                    }

                }
            });
        }
        
        
        
        

    </script>
</body>
</html>