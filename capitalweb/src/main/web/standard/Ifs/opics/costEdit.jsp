<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>成本中心  维护</title>
  </head>

<body style="width:100%;height:100%;background:white">
    <div>
        <span id="labell"><a href="javascript:CommonUtil.activeTab();">成本中心</a> >> 修改</span>
        <div id="field" class="fieldset-body">
            <table id="field_form"  style="text-align:left;margin:auto;width:100%;" class="mini-sltable">
                <tr>
                    <td style="width:50%">
                    	<input id="operator" name="operator" class="mini-hidden" />
            			<input id="status" name="status" class="mini-hidden" />
                        <input id="costcent" name="costcent" class="mini-textbox mini-mustFill"  label="成本中心" labelField="true"   emptyText="请输入成本中心" required="true"  vtype="maxLength:10" style="width:100%"labelStyle="text-align:left;width:170px" onvalidation="onEnglishAndNumberValidation"/>
                    </td>
<%--                    <td style="width:50%">--%>
<%--                        <input id="costcentpid" name="costcentpid" class="mini-treeselect"  label="成本中心父类" labelField="true"  emptyText="请选择成本中心父类" style="width:100%"labelStyle="text-align:left;width:170px" valueField="id" textField="text"  onclick="nodeclick"/>--%>
<%--                    </td>--%>
<%--                    <td style="width:50%">--%>
<%--                        <input id="costdesccn" name="costdesccn" class="mini-textbox mini-mustFill" label="成本中心中文描述" emptyText="请输入成本中心中文描述" labelField="true"   style="width:100%" labelStyle="text-align:left;width:170px" vtype="maxLength:40" required="true" />--%>
<%--                    </td>--%>
                    <td style="width:50%">
                        <input id="costdesc" name="costdesc" class="mini-textbox mini-mustFill"  label="成本中心描述" required="true"  emptyText="请输入成本中心描述" labelField="true"  style="width:100%" labelStyle="text-align:left;width:170px"vtype="maxLength:35" onvalidation="onEnglishAndNumberValidation"/>
                    </td>
                </tr>
                <tr>
                    <td style="width:50%">
                        <input id="busunit" name="busunit" class="mini-textbox mini-mustFill"  label="营业单位" emptyText="请输入营业单位" labelField="true" required="true"  style="width:100%" labelStyle="text-align:left;width:170px" vtype="maxLength:10"  onvalidation="onEnglishAndNumberValidation"/>
                    </td>
                    <td style="width:50%">
                        <input id="orgunit" name="orgunit" class="mini-textbox mini-mustFill"  label="组织单位" labelField="true" required="true"  emptyText="请输入组织单位"  vtype="maxLength:10" style="width:100%"labelStyle="text-align:left;width:170px" onvalidation="onEnglishAndNumberValidation"/>
                    </td>
                </tr>
                <tr>
                	
                    <td style="width:50%">
                        <input id="lstmntdte" name="lstmntdte" class="mini-datepicker" format="yyyy-MM-dd" label="维护更新日期" emptyText="系统自动生成" labelField="true"   style="width:100%" labelStyle="text-align:left;width:170px" enabled="false"/>
                    </td>
                    <td style="width:50%">
                        <input id="remark" name="remark" class="mini-textbox" label="备注" emptyText="请输入备注" labelField="true"   style="width:100%" labelStyle="text-align:left;width:170px"vtype="maxLength:50"/>
                    </td>
                </tr>
                
                <tr>
                    <td colspan="2" style="text-align:center;">
                        <a  id="save_btn" class="mini-button" style="display: none"    onclick="save">保存</a>
                        <a id="cancel_btn" class="mini-button" style="display: none"    onclick="cancel">取消</a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <script type="text/javascript">
        mini.parse();

        //获取当前tab
        var currTab = top["win"].tabs.getActiveTab();
        var params = currTab.params;
        var row=params.selectData;
        var url=window.location.search;
        var action=CommonUtil.getParam(url,"action");
        var form=new mini.Form("#field_form");

        $(document).ready(function(){
            //控制按钮显示
            $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
                initData();
            });
        });

        //初始化数据
        function initData(){
            if($.inArray(action,["edit","detail"])>-1){ //修改、详情
                form.setData(row);
                mini.get("costcent").setEnabled(false);
                /* mini.get("costcentpid").setEnabled(false); */
                if(row.status=="1"){ //已同步时
                	 mini.get("costdesc").setEnabled(false);
                     mini.get("busunit").setEnabled(false);
                     mini.get("orgunit").setEnabled(false);
                }
                if(action == "detail"){
                    $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>成本中心</a> >> 详情");
                    mini.get("save_btn").hide();
                    form.setEnabled(false);
                }
            }else{//增加 
                $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>成本中心</a> >> 新增");
                mini.get("operator").setValue(userId);
            }
        }
        //保存
         function save(){
            //表单验证！！！
            form.validate();
            if (form.isValid() == false) {
                return;
            }
            var saveUrl=null;
            if(action == "add"){
                saveUrl = "/IfsOpicsCostController/saveOpicsCostAdd";
                var data=form.getData(true);
                var params=mini.encode(data);
                CommonUtil.ajax({
                    url:saveUrl,
                    data:params,
                    callback:function(data){
                    	if (data.code == 'error.common.0000') {
    						mini.alert("保存成功",'提示信息',function(){
    							top["win"].closeMenuTab();
    						});
    					} else {
    						mini.alert("保存失败");
    			    	}
                    }
    		    });
            }else if(action == "edit"){
                saveUrl = "/IfsOpicsCostController/saveOpicsCostEdit";
                var data=form.getData(true);
                var params=mini.encode(data);
                CommonUtil.ajax({
                    url:saveUrl,
                    data:params,
                    callback:function(data){
                    	if (data.code == 'error.common.0000') {
    						mini.alert(data.desc,'提示信息',function(){
    							top["win"].closeMenuTab();
    						});
    					} else {
    						mini.alert("修改失败");
    			    	}
                    }
    		    });
            }

           
        }

        function cancel(){
            top["win"].closeMenuTab();
        }

        //英文、数字、下划线 的验证
        function onEnglishAndNumberValidation(e) {
            if(e.value == "" || e.value == null){//值为空，就不做校验
                return;
            }
            if (e.isValid) {
                if (isEnglishAndNumber(e.value) == false) {
                    e.errorText = "必须输入英文或数字";
                    e.isValid = false;
                }
            }
        }

        /* 是否英文、数字、下划线 */
        function isEnglishAndNumber(v) {
            var re = new RegExp("^[0-9a-zA-Z\_\-]+$");
            if (re.test(v)) return true;
            return false;
        }
        
        function nodeclick(e) {
    		var oldvalue=e.sender.value;
    		var costcent=mini.get("costcent").getValue();
    		var param = mini.encode({"costcent":costcent}); //序列化成JSON
    		CommonUtil.ajax({
    		  url: "/IfsOpicsCostController/searchCost",
    		  data: param,
    		  callback: function (data) {
    			var obj=e.sender;
    			obj.setData(data.obj);
    			obj.setValue(oldvalue);
    			
    		  }
    		});
    	}

    </script>
    <script src="<%=basePath%>/miniScript/miniMustFill.js"></script>
</body>
</html>