<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
    <head>
        <title>opics系统-清算方式表</title>
        <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
        <script type="text/javascript" src="<%=basePath%>/miniScript/hideBtn.js"></script>
    </head>
<body style="width:100%;height:100%;background:white">
    <fieldset class="mini-fieldset title">
    <legend>opics系统 清算信息查询</legend>
    <div id="search_form" style="width: 100%;">
        <input id="settlmeans" name="settlmeans" class="mini-textbox" labelField="true" label="清算方式：" labelStyle="text-align:right;" emptyText="请输入清算方式" />
       	<input id="status" name="status" class="mini-combobox" data="CommonUtil.serverData.dictionary.DealStatus" width="280px" emptyText="请选择同步状态" labelField="true"  label="同步状态：" labelStyle="text-align:right;"/>
        <span style="float:right;margin-right: 150px">
            <a class="mini-button" style="display: none"  id="search_btn"  onclick="search()">查询</a>
            <a class="mini-button" style="display: none"  id="clear_btn"  onclick="clear()">清空</a>
        </span>
    </div>
    </fieldset>
    <span style="margin:2px;display: block;">
        <!-- <a class="mini-button" style="display: none"  id="add_btn"  onClick="add();">新增</a>
        <a class="mini-button" style="display: none"  id="edit_btn"  onClick="modify();">修改</a>
        <a class="mini-button" style="display: none"  id="delete_btn"  onClick="del();">删除</a>
        <a class="mini-button" style="display: none"  id="sync"  onClick="sync();">同步至opics</a> -->
        <a class="mini-button" style="display: none"  id="calib"  onClick="calibration();">批量校准信息</a>
    </span>
    <div class="mini-fit" style="width:100%;height:100%;">
        <div id="setm_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" 
            sortMode="client" allowAlternating="true"   idField="userId" onrowdblclick="onRowDblClick" 
            border="true" allowResize="true" multiSelect="true"  >
            <div property="columns">
            	<div type="checkcolumn"></div>
                <div type="indexcolumn" headerAlign="center" align="center" width="40px">序列</div>
                <div field="br" headerAlign="center"  align="center" width="100px" >机构分支</div>
                <div field="settlmeans" headerAlign="center"  align="center" width="100px" >清算方式</div>
                <div field="descr" headerAlign="center"  width="160px">描述</div>
                <div field="status" headerAlign="center" width="100px" renderer="CommonUtil.dictRenderer" data-options="{dict:'DealStatus'}" align="center">同步状态</div>
                <div field="operator" headerAlign="center"  width="120px" align="center">操作人</div>
                <div field="remark" headerAlign="center"  width="200px">备注</div>
                <div field="lstmntdte" headerAlign="center"  align="center" width="160px" renderer="onDateRenderer">最后维护时间</div>
            </div>
        </div>
    </div>
</body>
<script>
    mini.parse();
    function onDateRenderer(e) {
    	var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)==null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g,'/'));
        if (value) return mini.formatDate(value, 'yyyy-MM-dd');
    }
    var grid = mini.get("setm_grid");
    grid.on("beforeload", function(e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        searchs(pageSize, pageIndex);
    });

    /* grid.on("select",function(e){
		var row=e.record;
		if(row.status == "0"){//未同步
			mini.get("sync").setEnabled(true);
			mini.get("edit_btn").setEnabled(true);
			mini.get("delete_btn").setEnabled(true);
		}
		if(row.status == "1"){//已同步
			mini.get("sync").setEnabled(false);
			mini.get("edit_btn").setEnabled(true);
			mini.get("delete_btn").setEnabled(false);
		}
	});  */
    $(document).ready(function() {
		search();
	});

    //查询按钮
    function search(){
        searchs(grid.pageSize,0);
    }

    function searchs(pageSize, pageIndex) {
        var form = new mini.Form("#search_form");
        form.validate();
        if (form.isValid() == false) {
            mini.alert("表单填写错误,请确认!", "提示信息");
            return;
        }

        var data = form.getData(true);
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        data['branchId']=branchId;

        var params = mini.encode(data);

        CommonUtil.ajax({
            url : "/IfsOpicsSetmController/searchPageSetm",
            data : params,
            callback : function(data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }
    

    //清空
    function clear(){
        var form=new mini.Form("search_form");
        form.clear();
        search();
    }

    //新增
    function add(){
        var url = CommonUtil.baseWebPath() + "/opics/setmEdit.jsp?action=add";
        var tab = { id: "setmAdd", name: "setmAdd", title: "清算方式新增", 
        url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
        var paramData = {selectData:""};
        CommonUtil.openNewMenuTab(tab,paramData);
    }
    
    //修改
    function modify(){
        var row = grid.getSelected();
        if(row){
            var url = CommonUtil.baseWebPath() + "/opics/setmEdit.jsp?action=edit";
            var tab = { id: "setmEdit", name: "setmEdit", title: "清算方式修改", 
            url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
            var paramData = {selectData:row};
            CommonUtil.openNewMenuTab(tab,paramData);

        } else {
            mini.alert("请选中一条记录！","消息提示");
        }
    }
    
    //删除
    function del() {
        var row = grid.getSelected();
        if (row) {
            mini.confirm("您确认要删除选中记录?","系统警告",function(value){
                if(value=="ok"){
                    CommonUtil.ajax({
                        url: "/IfsOpicsSetmController/deleteSetm",
                        data: row,
                        callback: function (data) {
                            if (data.code == 'error.common.0000') {
                                mini.alert("删除成功!");
                                grid.reload();
                            } else {
                                mini.alert("删除失败!");
                            }
                        }
                    });
                }
            });
        }else {
            mini.alert("请选中一条记录！", "消息提示");
        }

    }


    //双击详情
    function onRowDblClick(e) {
        var row = grid.getSelected();
        var lstmntdte = new Date(row.lstmntdte);
        row.lstmntdte=lstmntdte;
        if(row){
                var url = CommonUtil.baseWebPath() + "/opics/setmEdit.jsp?action=detail";
                var tab = { id: "setmDetail", name: "setmDetail", title: "清算方式详情", url: url ,showCloseButton:true};
                var paramData = {selectData:row};
                CommonUtil.openNewMenuTab(tab,paramData);
        } else {
            mini.alert("请选中一条记录！","消息提示");
        }
    }
    
  //同步
    function sync() {
		var rows = grid.getSelecteds();
		if(rows.length>0){
			mini.confirm("您确认要同步选中记录?","系统警告",function(value){
				if(value=="ok"){
					CommonUtil.ajax({
						url: "/IfsOpicsSetmController/syncSetm",
						data: rows,
						callback: function(data){
							mini.alert(data.obj.retMsg,data.obj.retCode);
							grid.reload();
						}
					});
				}
			});
		}else{
			mini.alert("请至少选中一条记录！","消息提示");
		}
    }
  
  //批量校准
  function calibration(){
	  var rows = grid.getSelecteds();
	  if(rows.length>0){ //选中行校准
		  mini.confirm("您确认要校准选中记录?","系统警告",function(value){
			  if(value=="ok"){
				  CommonUtil.ajax({
			          url: "/IfsOpicsSetmController/batchCalibration",
			          data: rows,
			          callback: function (data) {
			        	  mini.alert(data.obj.retMsg,data.obj.retCode);
			        	  grid.reload();
			          }
			      });
			  }
		  });
		  
	  }else{ //未选中就全部校准
		  mini.confirm("您确认要校准选中记录?","系统警告",function(value){
			  if(value=="ok"){
				  CommonUtil.ajax({
			          url: "/IfsOpicsSetmController/batchCalibration",
			          data: rows,
			          callback: function (data) {
			        	  mini.alert(data.obj.retMsg,data.obj.retCode);
			        	  grid.reload();
			          }
			      });
			  }
		  });
	  }
  
  }
  
</script>
</html>