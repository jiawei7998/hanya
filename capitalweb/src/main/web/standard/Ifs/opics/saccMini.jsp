<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
    <head>
        <title>opics系统-SACC表</title>
        <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    </head>
<body style="width:100%;height:100%;background:white">
    <fieldset class="mini-fieldset title">
    <legend>opics系统 SACC信息查询</legend>
    <div id="search_form" style="width: 100%;">
        <input id="accountno" name="accountno" class="mini-textbox" labelField="true" label="托管机构：" labelStyle="text-align:right;" emptyText="请输入托管机构" />
<%--       	<input id="status" name="status" class="mini-combobox" data="CommonUtil.serverData.dictionary.DealStatus"  emptyText="请选择同步状态" labelField="true"  label="同步状态：" labelStyle="text-align:right;"/>--%>
        <span style="float:right;margin-right: 150px">
            <a class="mini-button" style="display: none"  id="search_btn"  onclick="search()">查询</a>
            <a class="mini-button" style="display: none"  id="clear_btn"  onclick="clear()">清空</a>
        </span>
    </div>
    </fieldset>
    <div class="mini-fit" style="width:100%;height:100%;">
        <div id="sacc_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" 
            sortMode="client" allowAlternating="true"   idField="userId" onrowdblclick="onRowDblClick" 
            border="true" allowResize="true" multiSelect="true"  >
            <div property="columns">
            	<div type="checkcolumn"></div>
                <div type="indexcolumn" headerAlign="center" align="center" width="40px">序列</div>
<%--                <div field="br" headerAlign="center"  align="center" width="100px" >部门</div>--%>
                <div field="accountno" headerAlign="center"  align="center" width="100px" >托管机构</div>
                <div field="accttitle" headerAlign="center" width="250px" align="center">机构名称</div>
<%--                <div field="trad" headerAlign="center"  width="160px">交易员</div>--%>
<%--                <div field="opendate" headerAlign="center" width="200px" align="center" renderer="onDateRenderer">开户日期</div>--%>
<%--                <div field="cost" headerAlign="center" width="120px" align="center">成本中心</div>--%>
<%--                <div field="cno" headerAlign="center" width="120px" align="center">交易对手</div>--%>
                <div field="lstmntdate" headerAlign="center"  align="center" width="160px" renderer="onDateRenderer">最后维护时间</div>
<%--                <div field="port" headerAlign="center" width="120px" align="center">投资组合</div>--%>
                <div field="status" headerAlign="center" width="100px" renderer="CommonUtil.dictRenderer" data-options="{dict:'DealStatus'}" align="center">同步状态</div>
                <div field="operator" headerAlign="center"  width="120px" align="center">操作人</div>
                <div field="remark" headerAlign="center"  width="200px">备注</div>
            </div>
        </div>
    </div>
</body>
<script>
    mini.parse();

    function onDateRenderer(e) {
    	var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)==null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g,'/'));
        if (value) return mini.formatDate(value, 'yyyy-MM-dd');
    }
    var grid = mini.get("sacc_grid");
    grid.on("beforeload", function(e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        searchs(pageSize, pageIndex);
    });

    $(document).ready(function() {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
            search();
        });
	});

    //查询按钮
    function search(){
        searchs(grid.pageSize,0);
    }

    function searchs(pageSize, pageIndex) {
        var form = new mini.Form("#search_form");
        form.validate();
        if (form.isValid() == false) {
            mini.alert("表单填写错误,请确认!", "提示信息");
            return;
        }

        var data = form.getData();
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        data['branchId']=branchId;
        data['status']="1";
        data['trad']="1";

        var params = mini.encode(data);

        CommonUtil.ajax({
            url : "/IfsOpicsSaccController/searchPageSacc",
            data : params,
            callback : function(data) {
                var grid = mini.get("sacc_grid");
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }
    

    //清空
    function clear(){
        var form=new mini.Form("search_form");
        form.clear();
        search();
    }
    

    
    //双击行选择
      function onRowDblClick(){
          onOk();
      }

      function CloseWindow(action) {
          if (window.CloseOwnerWindow)
              return window.CloseOwnerWindow(action);
          else
              window.close();
      }

      function onOk() {
          CloseWindow("ok");
      }

      //关闭窗口
      function onCancel() {
          CloseWindow("cancel");
      }     
      function GetData() {
          var row = grid.getSelected();
          return row;
      }
    
  
  
  
</script>
</html>