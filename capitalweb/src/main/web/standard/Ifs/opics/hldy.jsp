<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
	<%@ include file="../../global.jsp"%>
		<link href="<%=basePath%>/miniScript/calendar/calendar.css" rel="stylesheet"></link>
		<script src="<%=basePath%>/miniScript/calendar/calendarChange.js"></script>
		<script src="<%=basePath%>/miniScript/calendar/calendar.js"></script>
		<script type="text/javascript" src="<%=basePath%>/miniScript/hideBtn.js"></script>
		<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>

<div class="mini-splitter" style="width:100%;height:100%" id="splitter">
	<div showCollapseButton="true" size="60%">
		<div id="StockPriceManage" class="mini-fit">
			<fieldset class="mini-fieldset">
				<legend>查询</legend>
				<div id="search_form" class="mini-form" >
					<input id="year" name="year" class="mini-combobox" width="320px" labelField="true"labelStyle="width:60px;text-align:right;" label="年份选择："/>
					<input id="month" name="month" class="mini-combobox" width="320px" labelField="true"  labelStyle="width:60px;text-align:right;" label="月份选择："/>
					<input id="calendarid" name="calendarid" class="mini-combobox" width="320px" labelField="true"  labelStyle="width:60px;text-align:right;" label="币种选择："
						   data="CommonUtil.serverData.dictionary.Currency" value="CNY"/>
					<input id="startTimeSearch" name="startTimeSearch" value="<%=__bizDate%>" class="mini-datepicker" width="320px" labelField="true" labelStyle="width:60px;text-align:right;" label="开始日期：" />
					<input id="endTimeSearch" name="endTimeSearch" value="<%=__bizDate%>" class="mini-datepicker" width="320px" labelField="true"  labelStyle="width:60px;text-align:right;" label="结束日期：" />

					<span style="float:right;margin-right: 150px">
						<a class="mini-button" style="display: none"  id="calib"        onClick="calibration();">批量校准信息</a>
						<a class="mini-button" style="display: none"  id="search_btn"   onclick="query">查询</a>
						<a class="mini-button" style="display: none"  id="clear_btn"   onclick="clear">清空</a>
						<a class="mini-button" style="display: none"  id="up_btn"   onclick="upload">上传</a>
						<a  id="export_btn" class="mini-button"  style="display: none"  onclick="ExcelExport()">导出模板</a>
						<span style="color: red"> 请先下载模板</span>
					</span>
				</div>
			</fieldset>
			
			<div id="TradeCalendarManage" class="mini-fit" style="margin-top: 2px;">
				<div id="calGrid" class="mini-datagrid borderAll" style="width: 100%; height: 100%;" sizeList="[20,30,50,100]" pageSize="20"
				sortMode="client" allowAlternating="true"   idField="userId" onrowdblclick="onRowDblClick" 
            	border="true" allowResize="true" multiSelect="true" >
					<div property="columns">
<%--						<div type="checkcolumn"></div>--%>
						<div type="indexcolumn" headerAlign="center" align="center" width="40px">序列</div>
						<div field="calendarid" headerAlign="center"  width="80px" align="center" >币种</div>
						<div field="holidate" headerAlign="center" width="100" allowSort="false" align="center" renderer="onDateRenderer">节假日期</div>
<%--						<div field="holidaytype" headerAlign="center" width="100" allowSort="false" align="center" renderer="onGenderRenderer">是否节假日</div>--%>
						<div field="status" headerAlign="center" width="100px"  align="center" renderer="onSyncRenderer">同步状态</div>
                		<div field="operator" headerAlign="center"  width="120px" align="center">操作人</div>
                		<!-- <div field="remark" headerAlign="center"  width="200px">备注</div> -->
                		<div field="lstmntdte" headerAlign="center"  align="center" width="160px" renderer="onDateRenderer">最后维护时间</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div showCollapseButton="true" size="40%">
		<div class="mini-fit">
			<div id="calendar"></div>
			<div style="padding:10px 0 0 15px">
				<a class="mini-button" style="display: none"  id="setHolDay" name="setHolDay"  onclick="setHolidays">设置节假日</a>
				<a class="mini-button" style="display: none"  id="setWorkDay" name="setWorkDay"  onclick="setWorkdays">取消节假日</a>
				<a class="mini-button" style="display: none"  id="setWeekend" name="setWeekend"  onclick="createWeekendHolidays">设置周末为节假日</a>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
		mini.parse();

		var currentDate = null;
		//隐藏
		mini.get("splitter").hidePane(2);

		var bizDate ='<%=__bizDate%>';

		var grid = mini.get("calGrid");
		grid.on("beforeload", function (e) {
			e.cancel = true;
			var pageIndex = e.data.pageIndex;
			var pageSize = e.data.pageSize;
			search(pageSize, pageIndex);
		});
		
		function search(pageSize,pageIndex) {
			var form = new mini.Form("search_form");
			form.validate();
			if (form.isValid == false)
				return;
			//提交数据
			var data = form.getData(true);//获取表单多个控件的数据  
			data['pageNumber'] = pageIndex+1;
			data['pageSize'] = pageSize;
			var param = mini.encode(data); //序列化成JSON
			CommonUtil.ajax({
				url: '/IfsOpicsHldyController/searchPageHldy',
				data: param,
				callback: function (data) {
					grid.setTotalCount(data.obj.total);
					grid.setPageIndex(pageIndex);
					grid.setPageSize(pageSize);
					grid.setData(data.obj.rows);

				}
			});
		}
		
		function query(){
			search(grid.pageSize,0);
		}
		
		function clear() {
			var form = new mini.Form("#search_form");
			form.clear();
		}

		function upload() {
			mini.open({
				showModal: true,
				allowResize: true,
				url: CommonUtil.baseWebPath() + "/opics/hldyImp.jsp",
				width: 420,
				height: 300,
				title: "节假日导入",
				ondestroy: function (action) {
					search(10,0);//重新加载页面
				}
			});
		}


		///////////////////////////////////////////////////////
		var Genders = [{ id: 0, text: '否' }, { id: 1, text: '是' }];
		function onGenderRenderer(e) {
			for (var i = 0, l = Genders.length; i < l; i++) {
				var g = Genders[i];
				if (g.id == e.value) return g.text;
			}
			return " ";
		}
		
		///////////////////////////////////////////////////////
		var syncs = [{ id: 0, text: '未同步' }, { id: 1, text: '已同步' }];
		function onSyncRenderer(e) {
			for (var i = 0, l = syncs.length; i < l; i++) {
				var g = syncs[i];
				if (g.id == e.value) return g.text;
			}
			return " ";
		}

		function getHoliDay(newDate)
		{
			currentDate = newDate;
			if(calendar && calendar.holiDays && calendar.holiDays.date && calendar.holiDays.date.length > 0)
			{
				return 	calendar.holiDays.date;
			}
			var year  = newDate.getFullYear();
			var month = newDate.getMonth() + 1;
			var day = newDate.getDate();
			var date = year + '-' + (month < 10 ? "0" + month : month)  + '-' + (day < 10 ? "0" + day : day);
			var holiDays = [];
			CommonUtil.ajax({
				url: "/IfsOpicsHldyController/getCalendarByMonth",
				data : {'bizDate':date},
				callback: function (data) {
					var date_1 = null;
					for (var index = 0; index < data.obj.length; index++) {
						date_1 = parseDate(onDateRenderer(data.obj[index].holidate));
						holiDays.push(date_1.getTime());
					}
				}
			});
			return holiDays;
		}
		
		 function onDateRenderer(e) {
		    	var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)==null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g,'/'));
		        if (value) return mini.formatDate(value, 'yyyy-MM-dd');
		    }

		$("#calendar").calendar({
				newDate: parseDate(bizDate),
				getHoliDay: getHoliDay
			});
		//yyyy-MM-dd
		function parseDate(dateStr){
			var year = dateStr.substring(0,4); // 2018-01-12
			var month = parseInt(dateStr.substring(5,7)) - 1;
			var day = dateStr.substring(8,10);
			return new Date(year, month, day);
		}
		
		

		var calendar = $("#calendar").data("calendar");
		
		function setWorkdays() {
			if(calendar.focusDays.length == 0){
				mini.alert("请选择日期");
				return;
			}

			var focusDays = {
				date: calendar.focusDays
			}
			CommonUtil.ajax({
				url: "/IfsOpicsHldyController/deleteHldy",
				data: mini.encode(focusDays.date),
				callback: function (data) {
					if (data.code == 'error.common.0000') {
						calendar.setUnHolidays(focusDays);
						mini.alert("设置成功",'提示信息');
						search(grid.pageSize,grid.pageIndex);
					}else{
						mini.alert("已同步的节假日期不可以取消设置!",'提示信息');
					}
				}
			});
		}
		
		function setHolidays() {
			var holiDays = {
				date: calendar.focusDays
			}
			if(holiDays.date.length == 0){
				mini.alert("请选择日期");
			}else{
				CommonUtil.ajax({
					url: "/IfsOpicsHldyController/insertHldy",
					data: mini.encode(holiDays.date),
					callback: function (data) {
						if (data.code == 'error.common.0000') {
							calendar.setHolidays(holiDays);
							mini.alert("设置成功",'提示信息');
							search(grid.pageSize,grid.pageIndex);
						}
					}
				});
			}
			
		}

		//设置周末为节假日
		function createWeekendHolidays(){
			var year  =  currentDate.getFullYear();
			var month =  currentDate.getMonth() + 1;

			var _newDate = new Date(year, month - 1, 1);
			_newDate.setDate(1);
			_newDate.setDate(_newDate.getDate() - 1);
			var holiDays = [];
			//当前月天数 不要要获取
			var monthCount = getMonthDayCount(year,month);
			var holiDay = null;
			for (var i = 0; i < monthCount; i++) {
				_newDate.setDate(_newDate.getDate() + 1);

				if (_newDate.getDay() == 0 || _newDate.getDay() == 6) 
				{
					holiDay = year + '-' + (month < 10 ? '0' + month : month) + '-' 
						+ (_newDate.getDate() < 10 ? '0' + _newDate.getDate() : _newDate.getDate());
					holiDays.push(holiDay);
				}
			}

			CommonUtil.ajax({
				url: "/IfsOpicsHldyController/insertHldy",
				data: mini.encode(holiDays),
				callback: function (data) {
					if (data.code == 'error.common.0000') {
						calendar.setHolidays({date: holiDays});
						mini.alert("设置成功",'提示信息')
						search(grid.pageSize,grid.pageIndex);
					}
				}
			});
		}

		function getMonthDayCount(year, month){
			var d = new Date(year, month, 0);
			return d.getDate();
		}

		mini.get("year").on("valuechanged",function(){
			setSearchConditions();
			query();
		});

		mini.get("month").on("valuechanged",function(){
			setSearchConditions();
			query();
		});

		function setSearchConditions()
		{
			if(CommonUtil.isNotNull(mini.get("year").getValue()) && CommonUtil.isNotNull(mini.get("month").getValue())){
				var year = parseInt(mini.get("year").getValue());
				var month = parseInt(mini.get("month").getValue());
				var monthDayCount = getMonthDayCount(year,month);
				mini.get("startTimeSearch").setValue(year + '-' + month + '-01');
				mini.get("endTimeSearch").setValue(year + '-' + month + '-' +(monthDayCount < 10 ? '0' + monthDayCount : monthDayCount));
				
			}else if(CommonUtil.isNotNull(mini.get("year").getValue()) && CommonUtil.isNull(mini.get("month").getValue())){
				var year = parseInt(mini.get("year").getValue());
				mini.get("startTimeSearch").setValue(year + '-01-01');
				mini.get("endTimeSearch").setValue(year + '-12-31');
			}
			
		}

		$(document).ready(function () {
			$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
				var year = parseInt(bizDate.substring(0, 4));
				var month = parseInt(bizDate.substring(5, 7));
				var monthDayCount = getMonthDayCount(year, month);

				var years = [];
				var year_1 = parseInt(bizDate.substring(0, 4)) - 5; // 2018-01-12
				for (var i = 0; i < 10; i++) {
					years.push({id: year_1, text: year_1 + '年'});
					year_1++;
				}
				mini.get("year").setData(years);
				mini.get("year").setValue(year);

				var months = [
					{id: '1', text: '1月'},
					{id: '2', text: '2月'},
					{id: '3', text: '3月'},
					{id: '4', text: '4月'},
					{id: '5', text: '5月'},
					{id: '6', text: '6月'},
					{id: '7', text: '7月'},
					{id: '8', text: '8月'},
					{id: '9', text: '9月'},
					{id: '10', text: '10月'},
					{id: '11', text: '11月'},
					{id: '12', text: '12月'}
				];
				mini.get("month").setData(months);
				mini.get("month").setValue(month);


				mini.get("startTimeSearch").setValue(year + '-' + month + '-01');
				mini.get("endTimeSearch").setValue(year + '-' + month + '-' + (monthDayCount < 10 ? '0' + monthDayCount : monthDayCount));

				search(grid.pageSize, 0);
			});
		});
		
		//批量校准
		  function calibration(){
			  var rows = grid.getSelecteds();
			  
			  
			  if(rows.length>0){//选中行校准
				  
				  mini.confirm("您确认要校准选中记录?","系统警告",function(value){
					  if(value=="ok"){
						  CommonUtil.ajax({
					          url: "/IfsOpicsHldyController/batchCalibration1",
					          data: rows,
					          callback: function (data) {
					              if (data.code == 'error.common.0000') {
					                  mini.alert("批量校准成功!");
					                  grid.reload();
					              } else {
					                  mini.alert("批量校准失败!");
					              }
					          }
					      });
						  
					  }
				  });//end confirm
				  
			  }else{//未选中就全部校准
				  mini.confirm("您确认要校准全部记录?","系统警告",function(value){
					  if(value=="ok"){
						  CommonUtil.ajax({
					          url: "/IfsOpicsHldyController/batchCalibration2",
					          data: {},
					          callback: function (data) {
					              if (data.code == 'error.common.0000') {
					                  mini.alert("全部校准成功!");
					                  grid.reload();
					              } else {
					                  mini.alert("全部校准失败!");
					              }
					          }
					      });
						  
					  }
				  });//end confirm
				  
				  
			  }//end else
			  
			  
			  
			  
		  }






		//导出模板
		function ExcelExport() {
//        if(grid.totalCount=="0"){
//            mini.alert("表中无数据");
//            return;
//        }
			mini.confirm("您确认要导出Excel吗?","系统提示",
					function (action) {
						if (action == "ok"){
							var form = new mini.Form("#search_form");
							var data = form.getData(true);
							var fields = null;
							for(var id in data){
								fields += '<input type="hidden" id="' + id + '" name="'+ id +'" value="' + data[id] + '">';
							}
							var urls = CommonUtil.pPath + "/sl/IfsLimitController/hldyExcel  ";
							$('<form action="'+ urls +'" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();
						}
					}
			);
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		

	</script>
