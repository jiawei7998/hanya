<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>产品  维护</title>
  </head>

<body style="width:100%;height:100%;background:white">
    <div>
    
    	
    	
        <span id="labell"><a href="javascript:CommonUtil.activeTab();">产品</a> >> 修改</span>
        <div id="field" class="fieldset-body">
            <table id="field_form"  style="text-align:left;margin:auto;width:100%;" class="mini-sltable">
            	<tr>
               		<td style="width:50%">
                        <input id="psn" name="psn" class="mini-textbox" label="产品流水号" emptyText="系统自动生成" labelField="true"   style="width:100%" labelStyle="text-align:left;width:170px" vtype="maxLength:32" required="true"  enabled="false"/>
                    </td>
                    <td style="width:50%">
                        <input id="ppid" name="ppid" class="mini-treeselect"  label="父产品代码" emptyText="请选择父产品代码" labelField="true"  style="width:100%" labelStyle="text-align:left;width:170px" valueField="id" textField="text"  onclick="nodeclick" />
                    </td>
                </tr>
                <tr>
                    <td style="width:50%">
                    <input id="operator" name="operator" class="mini-hidden" />
            		<input id="status" name="status" class="mini-hidden" />
                        <input id="pcode" name="pcode" class="mini-textbox mini-mustFill"  label="产品代码" labelField="true"   emptyText="请输入产品代码" required="true"  vtype="maxLength:6" style="width:100%"labelStyle="text-align:left;width:170px" onvalidation="onEnglishAndNumberValidation"/>
                    </td>
                    <td style="width:50%">
                        <input id="pdesc" name="pdesc" class="mini-textbox mini-mustFill"  label="产品描述" labelField="true"  emptyText="请输入产品描述"  vtype="maxLength:120" required="true"  style="width:100%"labelStyle="text-align:left;width:170px" onvalidation="onEnglishAndNumberValidation"/>
                    </td>
                </tr>
                <tr>
                	<td style="width:50%">
                        <input id="pdesccn" name="pdesccn" class="mini-textbox"  label="产品中文描述" labelField="true" emptyText="请输入产品中文描述"  vtype="maxLength:11" style="width:100%"labelStyle="text-align:left;width:170px"/>
                    </td>
                    <td style="width:50%">
                        <input id="sys" name="sys" class="mini-textbox mini-mustFill"  label="所属模块" required="true"  emptyText="请输入所属模块" labelField="true"  style="width:100%" labelStyle="text-align:left;width:170px"vtype="maxLength:4" onvalidation="onEnglishAndNumberValidation"/>
                    </td>
                </tr>
                
                <tr>
                	 <td style="width:50%">
                        <input id="lstmntdte" name="lstmntdte" class="mini-datepicker" format="yyyy-MM-dd" label="维护更新日期" emptyText="系统自动生成" labelField="true"   style="width:100%" labelStyle="text-align:left;width:170px" enabled="false"/>
                    </td>
                    <td style="width:50%">
                        <input id="remark" name="remark" class="mini-textbox" label="备注" emptyText="请输入备注" labelField="true"   style="width:100%" labelStyle="text-align:left;width:170px"vtype="maxLength:50"/>
                    </td>
                </tr>
                
                <tr>
                    <td colspan="2" style="text-align:center;">
                        <a  id="save_btn" class="mini-button" style="display: none"    onclick="save">保存</a>
                        <a id="cancel_btn" class="mini-button" style="display: none"    onclick="cancel">取消</a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <script type="text/javascript">
        mini.parse();

        //获取当前tab
        var currTab = top["win"].tabs.getActiveTab();
        var params = currTab.params;
        var row=params.selectData;
        var url=window.location.search;
        var action=CommonUtil.getParam(url,"action");
        var form=new mini.Form("#field_form");
        
        
        $(document).ready(function(){
            //控制按钮显示
            $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
                initData();
            });
        });

        //初始化数据
        function initData(){
            if($.inArray(action,["edit","detail"])>-1){ //修改、详情
            	mini.get("pcode").setEnabled(false);
            	/* mini.get("ppid").setEnabled(false); */
            	if(row.status=="1"){ //已同步时
               	 	mini.get("pdesc").setEnabled(false);
                    mini.get("sys").setEnabled(false);
               }
            	
                form.setData(row);
                if(action == "detail"){
                    $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>产品</a> >> 详情");
                    mini.get("save_btn").hide();
                    form.setEnabled(false);
                }
            }else{//增加 
            	mini.get("operator").setValue("<%=__sessionUser.getUserId()%>");
                $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>产品</a> >> 新增");
                mini.get("operator").setValue(userId);
            }
        }
        //保存
         function save(){
            //表单验证！！！
            form.validate();
            if (form.isValid() == false) {
                return;
            }
            var saveUrl=null;
            if(action == "add"){
                saveUrl = "/IfsOpicsProdController/saveOpicsProdAdd";
                var data=form.getData(true);
                var params=mini.encode(data);
                CommonUtil.ajax({
                    url:saveUrl,
                    data:params,
                    callback:function(data){
                    	if (data.code == 'error.common.0000') {
    						mini.alert("保存成功",'提示信息',function(){
    							top["win"].closeMenuTab();
    						});
    					} else {
    						mini.alert("保存失败");
    			    	}
                    }
    		    });
            }else if(action == "edit"){
                saveUrl = "/IfsOpicsProdController/saveOpicsProdEdit";
                var data=form.getData(true);
                var params=mini.encode(data);
                CommonUtil.ajax({
                    url:saveUrl,
                    data:params,
                    callback:function(data){
                    	if (data.code == 'error.common.0000') {
    						mini.alert(data.desc,'提示信息',function(){
    							top["win"].closeMenuTab();
    						});
    					} else {
    						mini.alert("修改失败");
    			    	}
                    }
    		    });
            }

           
        }

        function cancel(){
            top["win"].closeMenuTab();
        }

        //英文、数字、下划线 的验证
        function onEnglishAndNumberValidation(e) {
            if(e.value == "" || e.value == null){//值为空，就不做校验
                return;
            }
            if (e.isValid) {
                if (isEnglishAndNumber(e.value) == false) {
                    e.errorText = "必须输入英文或数字";
                    e.isValid = false;
                }
            }
        }

        /* 是否英文、数字、下划线 */
        function isEnglishAndNumber(v) {
            var re = new RegExp("^[0-9a-zA-Z\_\-]+$");
            if (re.test(v)) return true;
            return false;
        }
	
        function nodeclick(e) {
    		var oldvalue=e.sender.value;
    		var psn=mini.get("psn").getValue();
    		var param = mini.encode({"psn":psn}); //序列化成JSON
    		CommonUtil.ajax({
    		  url: "/IfsOpicsProdController/searchProd",
    		  data: param,
    		  callback: function (data) {
    			var obj=e.sender;
    			obj.setData(data.obj);
    			obj.setValue(oldvalue);
    			
    		  }
    		});
    	}
    </script>
    <script src="<%=basePath%>/miniScript/miniMustFill.js"></script>
</body>
</html>