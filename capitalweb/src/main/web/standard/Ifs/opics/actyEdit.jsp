<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>会计类型  维护</title>
  </head>

<body style="width:100%;height:100%;background:white">
	<div class="mini-splitter" style="width:100%;height:100%;">
		<div size="90%" showCollapseButton="false">
			<div id="field_form" class="mini-fit area" style="background:white">
	  				<div id="ecif" class="mini-panel" title="会计类型信息" style="width:100%" allowResize="true" collapseOnTitleClick="true">
						<div class="leftarea">
							<input id="acctngtype" name="acctngtype" class="mini-textbox" label="会计类型" labelField="true"  
								emptyText="请输入会计类型"  vtype="maxLength:10" required="true"  style="width:100%"
								labelStyle="text-align:left;width:170px" onvalidation="onEnglishAndNumberValidation"
								onvaluechanged="checkacty"/>
							<input id="acctdesccn" name="acctdesccn" class="mini-textbox" label="会计类型中文描述" 
								emptyText="请输入会计类型中文描述" labelField="true" style="width:100%" 
								labelStyle="text-align:left;width:170px" vtype="maxLength:40" onvalidation="onChineseValidation"/>
							<input id="type" name="type" class="mini-combobox" label="会计归属类型" 
								emptyText="请选择会计归属类型" labelField="true" style="width:100%" 
								labelStyle="text-align:left;width:170px" vtype="maxLength:20" data="CommonUtil.serverData.dictionary.actyType"/>
						</div>
						<div class="rightarea">
							<input id="acctdesc" name="acctdesc" class="mini-textbox"  label="会计类型描述" labelField="true"  
								emptyText="请输入会计类型描述"  vtype="maxLength:35" required="true"  style="width:100%"
								labelStyle="text-align:left;width:170px" onvalidation="onEnglishAndNumberValidation"/>
			 				<input id="lstmntdate" name="lstmntdate" class="mini-datepicker" format="yyyy-MM-dd HH:mm:ss"
			 					timeFormat="HH:mm:ss" label="维护更新日期" emptyText="系统自动生成" labelField="true"  
			 					style="width:100%" labelStyle="text-align:left;width:170px" enabled="false"/>
			 				<input id="remark" name="remark" class="mini-textbox" label="备注" emptyText="请输入备注" 
								labelField="true"  style="width:100%" labelStyle="text-align:left;width:170px"
								vtype="maxLength:50"/>
						</div>
     				</div>
    		</div>
   	 	</div>
		<div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
			<div style="margin-bottom:10px; text-align: center;">
				<a id="save_btn" class="mini-button" style="display: none"  width="125px" onclick="save(1)">保&nbsp;&nbsp;&nbsp;&nbsp;存</a>
			</div>
			<div style="margin-bottom:10px; text-align: center;">
				<a id="saveAndSync_btn" class="mini-button" style="display: none"  width="125px" onclick="save(2)">保存并同步至OPICS</a>
			</div>
			<div style="margin-bottom:10px; text-align: center;">
				<a id="cancel_btn" class="mini-button" style="display: none"  width="125px" onclick="cancel">取&nbsp;&nbsp;&nbsp;&nbsp;消</a>
			</div>
  		</div>
 	</div>
    <script type="text/javascript">
        mini.parse();

        //获取当前tab
        var currTab = top["win"].tabs.getActiveTab();
        var params = currTab.params;
        var row=params.selectData;
        var url=window.location.search;
        var action=CommonUtil.getParam(url,"action");
        var form=new mini.Form("#field_form");

        $(document).ready(function(){
			//控制按钮显示
			$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
				initData();
			});
        });

        //初始化数据
        function initData(){
            if($.inArray(action,["edit","detail"])>-1){ //修改、详情
     			mini.get("acctngtype").setEnabled(false); //会计类型设为不可用 
            	form.setData(row);
            	if(row.status=="1"){ //已同步，只可以修改会计类型里的中文描述与备注、会计归属类型
            		form.setEnabled(false);
            		mini.get("acctdesccn").setEnabled(true);
            		mini.get("remark").setEnabled(true);
            		mini.get("type").setEnabled(true);
            		mini.get("saveAndSync_btn").hide();
            	}
     			
                if(action == "detail"){
                    $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>会计类型</a> >> 详情");
                    mini.get("save_btn").hide();
                    form.setEnabled(false);
                }
            }else{//增加 
                $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>会计类型</a> >> 新增");
            }
        }
        
        //保存
        function save(flag){
            //表单验证！！！
            form.validate();
            if (form.isValid() == false) {
            	mini.alert("表单填写错误,请确认!","提示信息");
                return;
            }
            var saveUrl=null;
            if(action == "add"){
                saveUrl = "/IfsOpicsActyController/saveOpicsActyAdd";
                var data=form.getData(true);
                var params=mini.encode(data);
                CommonUtil.ajax({
                    url:saveUrl,
                    data:params,
                    callback:function(data2){
                    	if (data2.code == 'error.common.0000') {
                    		if(flag==1){ //弹出保存成功
                    			mini.alert("保存成功!",'提示信息',function(){
                    				top["win"].closeMenuTab();
                    			});
                    		} else if(flag==2){//弹出保存并同步到OPICS成功
                    			CommonUtil.ajax({
                    				url:"/IfsOpicsActyController/syncActy",
                    				data:{acctngtype:data.acctngtype},
                    				callback:function(data1){
                    					if(data1.retCode == 'OP00000'){
                    						mini.alert("保存并同步成功!",'提示信息',function(){
                                				top["win"].closeMenuTab();
                                			});
                    					} else {
                    						mini.alert("保存成功但同步失败!",'提示信息',function(){
                                				top["win"].closeMenuTab();
                                			});
                    					}
                    				}
                    			});
                    		}
    					} else {
    						mini.alert("保存失败");
    			    	}
                    }
    		    });
            }else if(action == "edit"){
                saveUrl = "/IfsOpicsActyController/saveOpicsActyEdit";
                var data=form.getData(true);
                var params=mini.encode(data);
                CommonUtil.ajax({
                    url:saveUrl,
                    data:params,
                    callback:function(data2){
                    	if (data2.code == 'error.common.0000') {
                    		if(flag==1){ //弹出保存成功
                    			mini.alert("保存成功!","提示信息",function(){
                    				top["win"].closeMenuTab();
                    			});
                    		} else if(flag==2){//弹出保存并同步到OPICS成功
                    			CommonUtil.ajax({
                    				url:"/IfsOpicsActyController/syncActy",
                    				data:{acctngtype:data.acctngtype},
                    				callback:function(data1){
                    					if(data1.retCode == 'OP00000'){
                    						mini.alert("保存并同步成功!","提示信息",function(){
                                				top["win"].closeMenuTab();
                                			});
                    					} else {
                    						mini.alert("保存成功但同步失败!","提示信息",function(){
                                				top["win"].closeMenuTab();
                                			});
                    					}
                    				}
                    			});
                    		}
    					} else {
    						mini.alert("修改失败");
    			    	}
                    }
    		    });
            }
        }

        function cancel(){
            top["win"].closeMenuTab();
        }

        //大写英文、数字、下划线 的验证
        function onEnglishAndNumberValidation(e) {
            if(e.value == "" || e.value == null){//值为空，就不做校验
                return;
            }
            if (e.isValid) {
                if (isEnglishAndNumber(e.value) == false) {
                    e.errorText = "必须输入大写英文或数字";
                    e.isValid = false;
                }
            }
        }

        /* 是否大写英文、数字、下划线 */
        function isEnglishAndNumber(v) {
            var re = new RegExp("^[0-9A-Z\_\-]+$");
            if (re.test(v)) return true;
            return false;
        }
        
        //中文校验
        function onChineseValidation(e) {
            if(e.value == "" || e.value == null){//值为空，就不做校验
                return;
            }
            if (e.isValid) {
                if (isChinese(e.value) == false) {
                    e.errorText = "必须输入中文";
                    e.isValid = false;
                }
            }
        }
        function isChinese(v) {
        	var ch = new RegExp("^[\u4e00-\u9fa5]+$");
            if (ch.test(v)) 
            	return true;
            return false;
        }
        
        //验证会计类型是否存在
        function checkacty(){
        	var acctngtype = mini.get("acctngtype").getValue();
        	CommonUtil.ajax({
        		url:"/IfsOpicsActyController/searchActyById",
        		data:{acctngtype:acctngtype},
        		callback:function(data1){
        			if(data1.code == 'error.common.0000'){
        				if(data1.obj != null){
        					mini.alert("此会计类型'"+acctngtype+"'已存在!请重新填写!",'提示信息',function(){
        						mini.get("acctngtype").setValue("");
        					});
        				}
        			}
        		}
        	});
        }
    </script>
</body>
</html>