<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../../global.jsp" %>
<html>

<head>
    <title>opics系统的产品表</title>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>

<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset title">
    <legend>opics系统的产品信息查询</legend>
    <div id="search_form" style="width: 100%;">
        <input id="pcode" name="pcode" class="mini-textbox" labelField="true" label="产品代码："
               labelStyle="text-align:right;" emptyText="请输入产品代码"/>
        <span style="float:right;margin-right: 20px">
            <a id="search_btn" class="mini-button" style="display: none" id="search_btn" onclick="search()">查询</a>
            <a class="mini-button" style="display: none" id="clear_btn" onclick="clear()">清空</a>
        </span>
    </div>
</fieldset>

<div class="mini-fit" style="width:100%;height:100%;">
    <div id="prod_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" sortMode="client"
         allowAlternating="true" idField="userId" onrowdblclick="onRowDblClick" border="true"
         allowResize="true">
        <div property="columns">
            <div field="pcode" headerAlign="center" align="center" width="100px">产品代码</div>
            <!-- <div field="pdesccn" headerAlign="center"  width="200px">产品中文描述</div> -->
            <div field="pdesc" headerAlign="center" align="left" width="180px">产品描述</div>
        </div>
    </div>
</div>
</body>
<script>
    mini.parse();
    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn){});
    });

    function onDateRenderer(e) {
        var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value) == null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g, '/'));
        if (value) return mini.formatDate(value, 'yyyy-MM-dd');
    }

    var grid = mini.get("prod_grid");
    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        searchs(pageSize, pageIndex);
    });


    //查询按钮
    function search() {
        searchs(grid.pageSize, 0);
    }

    function searchs(pageSize, pageIndex) {
        var form = new mini.Form("#search_form");
        form.validate();
        if (form.isValid() == false) {
            mini.alert("表单填写错误,请确认!", "提示信息");
            return;
        }
        var data = form.getData();
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        data['branchId'] = branchId;
        data['pcodelst'] = pcodelst;
        //同步状态为已同步
        data['status'] = "1";
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: "/IfsOpicsProdController/searchPageProd",
            data: params,
            callback: function (data) {
                var grid = mini.get("prod_grid");
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }

    //清空
    function clear() {
        var form = new mini.Form("search_form");
        form.clear();
        search();
    }

    function GetData() {
        var row = grid.getSelected();
        return row;
    }

    //双击行选择
    function onRowDblClick() {
        onOk();
    }

    function CloseWindow(action) {
        if (window.CloseOwnerWindow)
            return window.CloseOwnerWindow(action);
        else
            window.close();
    }

    function onOk() {
        CloseWindow("ok");
    }

    //关闭窗口
    function onCancel() {
        CloseWindow("cancel");
    }

    var pcodelst;

    //设置iframeData
    function SetData(icData) {
        data = mini.clone(icData);
        pcodelst = data.pcodelst;
        //设置查询条件
        CommonUtil.ajax({
            url: "/IfsOpicsProdController/searchPageProd",
            data: {"pcodelst": pcodelst},
            callback: function (data) {
                var grid = mini.get("prod_grid");
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(0);
                grid.setPageSize(10);
                grid.setData(data.obj.rows);
            }
        });
    }
</script>

</html>