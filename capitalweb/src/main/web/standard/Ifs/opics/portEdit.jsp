<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>投资组合  维护</title>
  </head>

<body style="width:100%;height:100%;background:white">
    <div>
        <span id="labell"><a href="javascript:CommonUtil.activeTab();">投资组合</a> >> 修改</span>
        <div id="field" class="fieldset-body">
            <table id="field_form"  style="text-align:left;margin:auto;width:100%;" class="mini-sltable">
               <!--  <tr>
                    <td style="width:50%">
                        <input id="portid" name="portid" class="mini-textbox"  label="投资组合ID" labelField="true"  emptyText="系统自动生成" style="width:100%"labelStyle="text-align:left;width:170px" enabled="false"/>
                    </td>
                    <td style="width:50%">
                        <input id="portpid" name="portpid" class="mini-treeselect"  label="投资组合父类" labelField="true"  emptyText="请选择投资组合父类"  style="width:100%"labelStyle="text-align:left;width:170px" valueField="id" textField="text"  onclick="nodeclick"/>
                    </td>
                </tr> -->
                <tr>
                    <td style="width:50%">
                    	<input id="operator" name="operator" class="mini-hidden" />
            			<input id="status" name="status" class="mini-hidden" />
                        <input id="br" name="br" class="mini-combobox mini-mustFill"  label="部门" required="true"  emptyText="请输入部门" labelField="true"  style="width:100%" labelStyle="text-align:left;width:170px"vtype="maxLength:10" data="CommonUtil.serverData.dictionary.opicsBr" onvalidation="onEnglishAndNumberValidation" value='<%=__sessionUser.getOpicsBr()%>'/>
                    </td>
                    <td style="width:50%">
                        <input id="portfolio" name="portfolio" class="mini-textbox  mini-mustFill" label="投资组合" emptyText="请输入投资组合" labelField="true"   style="width:100%" labelStyle="text-align:left;width:170px" vtype="maxLength:4" required="true"  onvalidation="onEnglishAndNumberValidation"/>
                    </td>
                   
                </tr>
                <tr>
               		
                    <td style="width:50%">
                        <input id="portdesc" name="portdesc" class="mini-textbox mini-mustFill"  label="投资组合描述" emptyText="投资组合描述" labelField="true"  style="width:100%" labelStyle="text-align:left;width:170px" vtype="maxLength:35"  onvalidation="onEnglishAndNumberValidation" required="true" />
                    </td>
                    <td style="width:50%">
                        <input id="portdesccn" name="portdesccn" class="mini-textbox"  label="投资组合中文描述" emptyText="投资组合中文描述" labelField="true"  style="width:100%" labelStyle="text-align:left;width:170px" vtype="maxLength:40"/>
                    </td>
                    
                    
                     
                </tr>
                <tr>
                	<td style="width:50%">
                        <input id="cost" name="cost" class="mini-buttonedit"  label="成本中心" labelField="true" required="true"  emptyText="请选择成本中心"  style="width:100%"labelStyle="text-align:left;width:170px" onbuttonclick="onButtonEdit" allowInput="false"/>
                    </td>
                    <td>
                     <input id="lstmntdte" name="lstmntdte" class="mini-datepicker" format="yyyy-MM-dd" label="维护更新日期" emptyText="系统自动生成" labelField="true"   style="width:100%" labelStyle="text-align:left;width:170px" enabled="false"/>
                    </td>
                </tr>
                
                 <tr>
                	<td style="width:50%">
                        <input id="text1" name="text1" class="mini-textbox"  label="备注1" emptyText="请输入备注1" labelField="true"   vtype="maxLength:35" style="width:100%"labelStyle="text-align:left;width:170px"/>
                    </td>
                    <td style="width:50%">
                        <input id="text2" name="text2" class="mini-textbox" label="备注2" emptyText="请输入备注2" labelField="true"   style="width:100%" labelStyle="text-align:left;width:170px"vtype="maxLength:35"/>
                    </td>
                </tr>
                 <tr>
                	<td style="width:50%">
                        <input id="date1" name="date1" class="mini-datepicker"  label="日期1" emptyText="请输入日期1" labelField="true"  style="width:100%"labelStyle="text-align:left;width:170px"/>
                    </td>
                    <td style="width:50%">
                        <input id="date2" name="date2" class="mini-datepicker" label="日期2" emptyText="请输入日期2" labelField="true"   style="width:100%" labelStyle="text-align:left;width:170px"/>
                    </td>
                </tr>
                
                
                <tr>
                	<td style="width:50%">
                        <input id="amount1" name="amount1" class="mini-spinner"  label="金额1" emptyText="请输入金额1" labelField="true" required="true"   style="width:100%"labelStyle="text-align:left;width:170px" maxValue="999999999999999" format="n4"/>
                    </td>
                    <td style="width:50%">
                        <input id="amount2" name="amount2" class="mini-spinner" label="金额2" emptyText="请输入金额2" labelField="true"   style="width:100%" labelStyle="text-align:left;width:170px" maxValue="999999999999999" format="n4"/>
                    </td>
                </tr>
                <tr>
                	<td style="width:50%">
                        <input id="updatecounter" name="updatecounter" class="mini-spinner"  label="更改次数" labelField="true" required="true"  emptyText="系统自动生成"  vtype="maxLength:11" style="width:100%"labelStyle="text-align:left;width:170px" enabled="false"/>
                    </td>
                    <td style="width:50%">
                        <input id="remark" name="remark" class="mini-textbox" label="备注" emptyText="请输入备注" labelField="true"   style="width:100%" labelStyle="text-align:left;width:170px"vtype="maxLength:50"/>
                    </td>
                </tr>
                
                
                <tr>
                    <td colspan="2" style="text-align:center;">
                        <a  id="save_btn" class="mini-button" style="display: none"    onclick="save">保存</a>
                        <a id="cancel_btn" class="mini-button" style="display: none"    onclick="cancel">取消</a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <script type="text/javascript">
        mini.parse();

        //获取当前tab
        var currTab = top["win"].tabs.getActiveTab();
        var params = currTab.params;
        var row=params.selectData;
        var url=window.location.search;
        var action=CommonUtil.getParam(url,"action");
        var form=new mini.Form("#field_form");

        $(document).ready(function(){
            //控制按钮显示
            $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
                initData();
            });
        });

        //初始化数据
        function initData(){
            if($.inArray(action,["edit","detail"])>-1){ //修改、详情
            	mini.get("cost").setValue(row.cost);
     			mini.get("cost").setText(row.cost);
                form.setData(row);
                mini.get("br").setEnabled(false);
                mini.get("portfolio").setEnabled(false);
                //mini.get("portid").setEnabled(false);
                /* mini.get("portpid").setEnabled(false); */
                if(row.status=="1"){ //已同步时
               	 	mini.get("portdesc").setEnabled(false);
                    mini.get("cost").setEnabled(false);
                    mini.get("text1").setEnabled(false);
                    mini.get("text2").setEnabled(false);
                    mini.get("date1").setEnabled(false);
                    mini.get("date2").setEnabled(false);
                    mini.get("amount1").setEnabled(false);
                    mini.get("amount2").setEnabled(false);
                    
               }
                
                
                
                if(action == "detail"){
                    $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>投资组合</a> >> 详情");
                    mini.get("save_btn").hide();
                    form.setEnabled(false);
                }
            }else{//增加 
                $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>投资组合</a> >> 新增");
                mini.get("operator").setValue(userId);
            }
        }
        //保存
         function save(){
            //表单验证！！！
            form.validate();
            if (form.isValid() == false) {
                return;
            }
            var saveUrl=null;
            if(action == "add"){
                saveUrl = "/IfsOpicsPortController/saveOpicsPortAdd";
                var data=form.getData(true);
                var params=mini.encode(data);
                CommonUtil.ajax({
                    url:saveUrl,
                    data:params,
                    callback:function(data){
                    	if (data.code == 'error.common.0000') {
    						mini.alert("保存成功",'提示信息',function(){
    							top["win"].closeMenuTab();
    						});
    					} else {
    						mini.alert("保存失败");
    			    	}
                    }
    		    });
            }else if(action == "edit"){
                saveUrl = "/IfsOpicsPortController/saveOpicsPortEdit";
                var data=form.getData(true);
                var params=mini.encode(data);
                CommonUtil.ajax({
                    url:saveUrl,
                    data:params,
                    callback:function(data){
                    	if (data.code == 'error.common.0000') {
    						mini.alert(data.desc,'提示信息',function(){
    							top["win"].closeMenuTab();
    						});
    					} else {
    						mini.alert("修改失败");
    			    	}
                    }
    		    });
            }

           
        }

        function cancel(){
            top["win"].closeMenuTab();
        }

        //英文、数字、下划线 的验证
        function onEnglishAndNumberValidation(e) {
            if(e.value == "" || e.value == null){//值为空，就不做校验
                return;
            }
            if (e.isValid) {
                if (isEnglishAndNumber(e.value) == false) {
                    e.errorText = "必须输入英文或数字";
                    e.isValid = false;
                }
            }
        }

        /* 是否英文、数字、下划线 */
        function isEnglishAndNumber(v) {
            var re = new RegExp("^[0-9a-zA-Z\_\-]+$");
            if (re.test(v)) return true;
            return false;
        }
        function nodeclick(e) {
    		var oldvalue=e.sender.value;
    		var portid=mini.get("portid").getValue();
    		var param = mini.encode({"portid":portid}); //序列化成JSON
    		CommonUtil.ajax({
    		  url: "/IfsOpicsPortController/searchPort",
    		  data: param,
    		  callback: function (data) {
    			var obj=e.sender;
    			obj.setData(data.obj);
    			obj.setValue(oldvalue);
    			
    		  }
    		});
    	}
        
        
        function onButtonEdit(e) {
            var btnEdit = this;
            mini.open({
                url: CommonUtil.baseWebPath() + "/opics/costMini.jsp",
                title: "成本中心",
                width: 900,
                height: 600,
                onload: function () {
                    var iframe = this.getIFrameEl();
                    iframe.contentWindow.SetData({});
                },
                ondestroy: function (action) {
                    if (action == "ok") {
                        var iframe = this.getIFrameEl();
                        var data = iframe.contentWindow.GetData();
                        data = mini.clone(data);    //必须
                        if (data) {
                            btnEdit.setValue(data.costcent);
                            btnEdit.setText(data.costcent);
                            btnEdit.focus();
                        }
                    }

                }
            });
        }
    </script>
    <script src="<%=basePath%>/miniScript/miniMustFill.js"></script>
</body>
</html>