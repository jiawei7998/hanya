<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
    <head>
        <title>opics系统-利率曲线表</title>
        <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    </head>
<body style="width:100%;height:100%;background:white">
    <fieldset class="mini-fieldset title">
    <legend>opics系统 利率曲线信息查询</legend>
    <div id="search_form" style="width: 100%;">
        <input id="yieldcurve" name="yieldcurve" class="mini-textbox" labelField="true" label="利率曲线：" labelStyle="text-align:right;" emptyText="请输入利率曲线" />
        <input id="ccy" name="ccy" class="mini-combobox" labelField="true" label="币种：" labelStyle="text-align:right;" emptyText="请输入币种" data="CommonUtil.serverData.dictionary.Currency"/>
        <span style="float:right;margin-right: 150px">
            <a class="mini-button" style="display: none"  id="search_btn"  onclick="search()">查询</a>
            <a class="mini-button" style="display: none"  id="clear_btn"  onclick="clear()">清空</a>
        </span>
    </div>
    </fieldset>
    <div class="mini-fit" style="width:100%;height:100%;">
        <div id="coun_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" 
            sortMode="client" allowAlternating="true"   idField="userId" onrowdblclick="onRowDblClick" 
            border="true" allowResize="true" multiSelect="true"  >
            <div property="columns">
            	<div type="checkcolumn"></div>
                <div type="indexcolumn" headerAlign="center" align="center" width="40px">序列</div>
                <div field="yieldcurve" headerAlign="center"  align="center" width="100px" >利率曲线</div>
                <div field="br" headerAlign="center" width="180px" align="center">机构</div>
                <div field="yieldcurvecn" headerAlign="center" width="180px" align="center">利率曲线中文描述</div>
                <div field="ccy" headerAlign="center"  width="80px" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'Currency'}">币种</div>
                <div field="descr" headerAlign="center"  width="200px">利率代码英文描述</div>
                <div field="curvetype" headerAlign="center"  width="100px" align="center">曲线类型</div>
                <div field="status" headerAlign="center" width="100px" renderer="CommonUtil.dictRenderer" data-options="{dict:'DealStatus'}" align="center">同步状态</div>
<%--                <div field="operator" headerAlign="center"  width="120px" align="center">操作人</div>--%>
                <div field="remark" headerAlign="center"  width="200px">备注</div>
                <div field="lstmntdte" headerAlign="center"  align="center" width="160px" renderer="onDateRenderer">最后维护时间</div>
            </div>
        </div>
    </div>
</body>
<script>
    mini.parse();

    function onDateRenderer(e) {
    	var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)==null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g,'/'));
        if (value) return mini.formatDate(value, 'yyyy-MM-dd');
    }
    var grid = mini.get("coun_grid");
    grid.on("beforeload", function(e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        searchs(pageSize, pageIndex);
    });

    
    $(document).ready(function() {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
            search();
        });
	});

    //查询按钮
    function search(){
        searchs(grid.pageSize,0);
    }

    function searchs(pageSize, pageIndex) {
        var form = new mini.Form("#search_form");
        form.validate();
        if (form.isValid() == false) {
            mini.alert("表单填写错误,请确认!", "提示信息");
            return;
        }

        var data = form.getData();
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        data['branchId']=branchId;
        data['status']="1";
        var params = mini.encode(data);

        CommonUtil.ajax({
            url : "/IfsOpicsYchdController/searchPageYchd",
            data : params,
            callback : function(data) {
                var grid = mini.get("coun_grid");
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }
    

    //清空
    function clear(){
        var form=new mini.Form("search_form");
        form.clear();
        search();
    }

    function GetData() {
        var row = grid.getSelected();
        return row;
    }

  	//双击行选择
    function onRowDblClick(){
        onOk();
    }

    function CloseWindow(action) {
        if (window.CloseOwnerWindow)
            return window.CloseOwnerWindow(action);
        else
            window.close();
    }

    function onOk() {
        CloseWindow("ok");
    }

    //关闭窗口
    function onCancel() {
        CloseWindow("cancel");
    }
    
   

    
    
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
</script>
</html>