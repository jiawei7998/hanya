<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>记账科目  维护</title>
  </head>

<body style="width:100%;height:100%;background:white">
    <div>
        <span id="labell"><a href="javascript:CommonUtil.activeTab();">记账科目</a> >> 修改</span>
        <div id="field" class="fieldset-body">
            <table id="field_form"  style="text-align:left;margin:auto;width:100%;" class="mini-sltable">
                <tr>
                    <td style="width:50%">
                        <input id="operator" name="operator" class="mini-hidden" />
            			<input id="status" name="status" class="mini-hidden" />
                        <input id="tcode" name="tcode" class="mini-textbox mini-mustFill"  label="新科目号" required="true"  emptyText="请输入新科目号" labelField="true"  style="width:100%" labelStyle="text-align:left;width:170px"vtype="maxLength:35" onvalidation="onEnglishAndNumberValidation"/>
                    </td>
                    <td style="width:50%">
                        <input id="tdescr" name="tdescr" class="mini-textbox mini-mustFill" label="新科目名称" emptyText="请输入新科目名称" labelField="true"   style="width:100%" labelStyle="text-align:left;width:170px" vtype="maxLength:40" required="true" />
                    </td>
                </tr>
                <tr>
                    <td style="width:50%">
                        <input id="comp1" name="comp1" class="mini-textbox mini-mustFill"  label="老科目号" required="true"  emptyText="请输入老科目号" labelField="true"  style="width:100%" labelStyle="text-align:left;width:170px"vtype="maxLength:35" onvalidation="onEnglishAndNumberValidation"/>
                    </td>
                    <td style="width:50%">
                        <input id="comp2" name="comp2" class="mini-textbox " label="老科目号序号" emptyText="请输入老科目号序号" labelField="true"   style="width:100%" labelStyle="text-align:left;width:170px" vtype="maxLength:40"  />
                    </td>
                </tr>
                <tr>
                    <td style="width:50%">
                        <input id="text" name="text" class="mini-textbox "  label="备注" emptyText="请输入备注" labelField="true"  style="width:100%" labelStyle="text-align:left;width:170px" vtype="maxLength:10"  onvalidation="onEnglishAndNumberValidation"/>
                    </td>
                   
                </tr>
     
                
                <tr>
                    <td colspan="2" style="text-align:center;">
                        <a  id="save_btn" class="mini-button" style="display: none"    onclick="save">保存</a>
                        <a id="cancel_btn" class="mini-button" style="display: none"    onclick="cancel">取消</a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <script type="text/javascript">
        mini.parse();

        //获取当前tab
        var currTab = top["win"].tabs.getActiveTab();
        var params = currTab.params;
        var row=params.selectData;
        var url=window.location.search;
        var action=CommonUtil.getParam(url,"action");
        var form=new mini.Form("#field_form");

        $(document).ready(function(){
            //控制按钮显示
            $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
                initData();
            });
        });
        
        //初始化数据
        function initData(){
            if($.inArray(action,["edit","detail"])>-1){ //修改、详情
                form.setData(row);
                if(action == "detail"){
                    $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>记账科目</a> >> 详情");
                    mini.get("save_btn").hide();
                    form.setEnabled(false);
                }
            }else{//增加 
                $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>记账科目</a> >> 新增");
                mini.get("operator").setValue(userId);
            }
        }
        

        
        
        //保存
         function save(){
            //表单验证！！！
            form.validate();
            if (form.isValid() == false) {
                return;
            }
            var saveUrl=null;
            if(action == "add"){
                saveUrl = "/IfsOpicsSubjectController/saveCoreSubjectAdd";
                var data=form.getData(true);
                var params=mini.encode(data);
                CommonUtil.ajax({
                    url:saveUrl,
                    data:params,
                    callback:function(data){
                    	if (data.code == 'error.common.0000') {
    						mini.alert("保存成功",'提示信息',function(){
    							top["win"].closeMenuTab();
    						});
    					} else {
    						mini.alert("保存失败");
    			    	}
                    }
    		    });
            }else if(action == "edit"){
                saveUrl = "/IfsOpicsSubjectController/saveCoreSubjectEdit";
                var data=form.getData(true);
                var params=mini.encode(data);
                CommonUtil.ajax({
                    url:saveUrl,
                    data:params,
                    callback:function(data){
                    	if (data.code == 'error.common.0000') {
    						mini.alert(data.desc,'提示信息',function(){
    							top["win"].closeMenuTab();
    						});
    					} else {
    						mini.alert("修改失败");
    			    	}
                    }
    		    });
            }

           
        }

        function cancel(){
            top["win"].closeMenuTab();
        }

        //英文、数字、下划线 的验证
        function onEnglishAndNumberValidation(e) {
            if(e.value == "" || e.value == null){//值为空，就不做校验
                return;
            }
            if (e.isValid) {
                if (isEnglishAndNumber(e.value) == false) {
                    e.errorText = "必须输入英文或数字";
                    e.isValid = false;
                }
            }
        }

        /* 是否英文、数字、下划线 */
        function isEnglishAndNumber(v) {
            var re = new RegExp("^[0-9a-zA-Z\_\-]+$");
            if (re.test(v)) return true;
            return false;
        }
        

    </script>
    <!-- 样式   必输项 出现 红星 -->
    <script src="<%=basePath%>/miniScript/miniMustFill.js"></script>
</body>
</html>