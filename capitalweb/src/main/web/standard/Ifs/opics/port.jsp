<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
    <head>
        <title>opics系统投资组合表</title>
        <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
        <script type="text/javascript" src="<%=basePath%>/miniScript/hideBtn.js"></script>
    </head>
<body style="width:100%;height:100%;background:white">
    <fieldset class="mini-fieldset title">
    <legend>opics系统投资组合信息查询</legend>
    <div id="search_form" style="width: 100%;">
    	<input id="portid" name="portid" visible="false" class="mini-textbox" width="320px" labelField="true" label="投资组合ID：" labelStyle="text-align:right;" emptyText="请输入投资组合ID" />
        <input id="portfolio" name="portfolio" class="mini-textbox" width="320px" labelField="true" label="投资组合代码：" labelStyle="text-align:right;" emptyText="请输入投资组合代码" />
       	<input id="status" name="status" class="mini-combobox" width="320px" data="CommonUtil.serverData.dictionary.DealStatus" emptyText="请选择同步状态" labelField="true"  label="同步状态：" labelStyle="text-align:right;"/>
        <span style="float:right;margin-right: 150px">
            <a class="mini-button" style="display: none"  id="search_btn"  onclick="search()">查询</a>
            <a class="mini-button" style="display: none"  id="clear_btn"  onclick="clear()">清空</a>
        </span>
    </div>
        <span style="margin:2px;display: block;">
<%--            <a class="mini-button" style="display: none"  id="add_btn"  onClick="add();">新增</a>--%>
<%--            <a class="mini-button" style="display: none"  id="edit_btn"  onClick="modify();">修改</a>--%>
<%--            <a class="mini-button" style="display: none"  id="delete_btn"  onClick="del();">删除</a>--%>
<%--            <a class="mini-button" style="display: none"  id="sync"  onClick="sync();">同步至opics</a>--%>
            <a class="mini-button" style="display: none"  id="calib"  onClick="calibration();">批量校准信息</a>
            <span style="color: red">静态参数以Opics为准，请在opics中添加完成后到前置进行数据批量较准操作</span>
        </span>
    </fieldset>

    <div class="mini-fit" style="width:100%;height:100%;">
        <div id="prod_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" 
            sortMode="client" allowAlternating="true"   idField="userId" onrowdblclick="onRowDblClick" 
            border="true" allowResize="true" >
            <div property="columns">
                <div type="indexcolumn" headerAlign="center" align="center" width="40px">序列</div>
                <!-- <div field="portid" headerAlign="center"  align="center" width="100px" >投资组合ID</div>
                <div field="portpid" headerAlign="center"  width="120px" align="center">投资组合父类</div> -->
                <div field="br" headerAlign="center" width="120px" align="center">部门</div>
                <div field="portfolio" headerAlign="center" width="80px" align="left">投资组合代码</div>
                <div field="portdesc" headerAlign="center" width="200px">投资组合描述</div>
                <div field="portdesccn" headerAlign="center" width="120px">投资组合中文描述</div>
<%--                <div field="cost" headerAlign="center" width="120px" align="center">成本中心</div>--%>
<%--                <div field="text1" headerAlign="center" width="120px">备注1</div>--%>
<%--                <div field="text2" headerAlign="center" width="120px">备注2</div>--%>
<%--                <div field="date1" headerAlign="center" width="120px" renderer="onDateTimeRenderer" align="center">日期1</div>--%>
<%--                <div field="date2" headerAlign="center" width="120px" renderer="onDateTimeRenderer" align="center">日期2</div>--%>
<%--                <div field="amount1" headerAlign="center" width="120px" align="right" allowSort="true" numberFormat="n2">金额1</div>--%>
<%--                <div field="amount2" headerAlign="center" width="120px" align="right" allowSort="true" numberFormat="n2">金额2</div>--%>
<%--                <div field="updatecounter" headerAlign="center" width="120px" align="center">更改次数</div>--%>
                <div field="status" headerAlign="center" width="120px" renderer="CommonUtil.dictRenderer" data-options="{dict:'DealStatus'}" align="center">同步状态</div>
<%--                <div field="operator" headerAlign="center"  width="120px" align="center">操作人</div>--%>
                <div field="remark" headerAlign="center"  width="200px">备注</div>
                <div field="lstmntdte" headerAlign="center"  align="center" width="160px" renderer="onDateTimeRenderer">最后维护时间</div>
            </div>
        </div>
    </div>
</body>
<script>
    mini.parse();
    function onDateRenderer(e) {
    	var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)==null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g,'/'));
        if (value) return mini.formatDate(value, 'yyyy-MM-dd');
    }
    function onDateTimeRenderer(e) {
    	var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)==null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g,'/'));
        if (value) return mini.formatDate(value, 'yyyy-MM-dd');
    }
    var grid = mini.get("prod_grid");
    grid.on("beforeload", function(e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        searchs(pageSize, pageIndex);
    });

    // grid.on("select",function(e){
	// 	var row=e.record;
	// 	if(row.status == "0"){//未处理
	// 		mini.get("sync").setEnabled(true);
	// 		mini.get("delete_btn").setEnabled(true);
	// 	}
	// 	if(row.status == "1"){//已处理
	// 		mini.get("sync").setEnabled(false);
	// 		mini.get("delete_btn").setEnabled(false);
	// 	}
	// });
    $(document).ready(function() {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
            search();
        });
	});

    //查询按钮
    function search(){
        searchs(grid.pageSize,0);
    }

    function searchs(pageSize, pageIndex) {
        var form = new mini.Form("#search_form");
        form.validate();
        if (form.isValid() == false) {
            mini.alert("表单填写错误,请确认!", "提示信息");
            return;
        }

        var data = form.getData();
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        data['branchId']=branchId;
	    //自贸区改造加br
		data['br']=opicsBr;
        var params = mini.encode(data);
        CommonUtil.ajax({
            url : "/IfsOpicsPortController/searchPagePort",
            data : params,
            callback : function(data) {
                var grid = mini.get("prod_grid");
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }
    

    //清空
    function clear(){
        var form=new mini.Form("search_form");
        form.clear();
        search();
    }

    //新增
    function add(){
        var url = CommonUtil.baseWebPath() + "/opics/portEdit.jsp?action=add";
        var tab = { id: "portAdd", name: "portAdd", title: "投资组合新增", 
        url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
        var paramData = {selectData:""};
        CommonUtil.openNewMenuTab(tab,paramData);
    }
    
    //修改
    function modify(){
        var row = grid.getSelected();
        if(row){
        	var lstmntdte = new Date(row.lstmntdte);
        	var date1 = new Date(row.date1);
        	var date2 = new Date(row.date2);
        	row.lstmntdte=lstmntdte;
        	row.date1=date1;
        	row.date2=date2;
            var url = CommonUtil.baseWebPath() + "/opics/portEdit.jsp?action=edit";
            var tab = { id: "portEdit", name: "portEdit", title: "投资组合修改", 
            url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
            var paramData = {selectData:row};
            CommonUtil.openNewMenuTab(tab,paramData);

        } else {
            mini.alert("请选中一条记录！","消息提示");
        }
    }
    
    //删除
    function del() {
        var row = grid.getSelected();
        if (row) {
            mini.confirm("您确认要删除选中记录?","系统警告",function(value){
                if(value=="ok"){
                    CommonUtil.ajax({
                        url: "/IfsOpicsPortController/deletePort",
                        data: {br: row.br,portfolio: row.portfolio},
                        callback: function (data) {
                            if (data.code == 'error.common.0000') {
                                mini.alert("删除成功!");
                                grid.reload();
                            } else {
                                mini.alert("删除失败!");
                            }
                        }
                    });
                }
            });
        }else {
            mini.alert("请选中一条记录！", "消息提示");
        }

    }


    //双击详情
    function onRowDblClick(e) {
        var row = grid.getSelected();
        var lstmntdte = new Date(row.lstmntdte);
        var date1 = new Date(row.date1);
        var date2 = new Date(row.date2);
        row.lstmntdte=lstmntdte;
        row.date1=date1;
        row.date2=date2;
        if(row){
                var url = CommonUtil.baseWebPath() + "/opics/portEdit.jsp?action=detail";
                var tab = { id: "portDetail", name: "portDetail", title: "投资组合详情", url: url ,showCloseButton:true};
                var paramData = {selectData:row};
                CommonUtil.openNewMenuTab(tab,paramData);
        } else {
            mini.alert("请选中一条记录！","消息提示");
        }
    }
    
  //同步
    function sync() {
        var row = grid.getSelected();
        if (row) {
            mini.confirm("您确认要同步选中记录?","系统警告",function(value){
                if(value=="ok"){
                    CommonUtil.ajax({
                        url: "/IfsOpicsPortController/syncPort",
                        data: {br: row.br,portfolio: row.portfolio},
                        callback: function (data) {
                        	mini.alert(data.obj.retMsg,data.obj.retCode);
                        	grid.reload();
                        }
                    });
                }
            });
        }else {
            mini.alert("请选中一条记录！", "消息提示");
        }

    }
  
  //批量校准
    function calibration(){
  	  var rows = grid.getSelecteds();
  	  
  	  
  	  if(rows.length>0){//选中行校准
  		  var str="";
  	  	  var str1="";
  	      if(rows.length==1){
  	      	str=rows[0].br;	
  	      	str1=rows[0].portfolio;
  	      }else{
  	      	for(var i=0;i<rows.length;i++){
  	          	if(i == 0){
  	          		str=rows[i].br+",";	
  	          		str1=rows[i].portfolio+",";
  	          	}else{
  	          		
  	          		if(i==rows.length-1){
  	          			str=str+rows[i].br;
  	          			str1=str1+rows[i]+portfolio;
  	          		}else{
  	          			str=str+rows[i].br+",";
  	          			str1=str1+rows[i]+portfolio+",";
  	          		}
  	          		
  	          	}
  	          	
  	          }
  	      	
  	      }
  		  mini.confirm("您确认要校准选中记录?","系统警告",function(value){
  			  if(value=="ok"){
  				  CommonUtil.ajax({
  			          url: "/IfsOpicsPortController/batchCalibration",
  			          data: {br:str,portfolio:str1,type:1},
  			          callback: function (data) {
  			        	mini.alert(data.obj.retMsg,data.obj.retCode);
  			        	grid.reload();
  			          }
  			      });
  				  
  			  }
  		  });//end confirm
  		  
  	  }else{//未选中就全部校准
  		  mini.confirm("您确认要校准全部记录?","系统警告",function(value){
  			  if(value=="ok"){
  				  CommonUtil.ajax({
  			          url: "/IfsOpicsPortController/batchCalibration",
  			          data: {type:2},
  			          callback: function (data) {
  			        	mini.alert(data.obj.retMsg,data.obj.retCode);
  			        	grid.reload();
  			          }
  			      });
  				  
  			  }
  		  });//end confirm
  		  
  		  
  	  }//end else
  	  
  	 }

</script>
</html>