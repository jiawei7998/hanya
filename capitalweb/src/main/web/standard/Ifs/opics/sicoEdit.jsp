<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>工业码  维护</title>
</head>
<body style="width:100%;height:100%;background:white">
<div class="mini-splitter" style="width:100%;height:100%;">
  <div size="90%" showCollapseButton="false">
  	<div id="field_form" class="mini-fit area" style="background:white">
	  <div id="sico" class="mini-panel" title="工业码信息" style="width:100%" allowResize="true" collapseOnTitleClick="true">
		<div class="leftarea">
            <input id="operator" name="operator" class="mini-hidden" />
            <input id="status" name="status" class="mini-hidden" />
            <input id="sic" name="sic" class="mini-textbox"  label="工业码" labelField="true"   emptyText="请输入工业码" required="true"  vtype="maxLength:10" style="width:100%"labelStyle="text-align:center;width:170px" onvaluechanged="checkSico" onvalidation="onUpperEnglishValidation"/>
			<input id="sdcn" name="sdcn" class="mini-textbox"  label="工业码中文描述"  labelField="true" emptyText="请输入工业码中文描述" required="false"   vtype="maxLength:40" style="width:100%" labelStyle="text-align:center;width:170px" />
            <input id="remark" name="remark" class="mini-textarea" label="备注" emptyText="请输入备注" labelField="true"   style="width:100%" labelStyle="text-align:center;width:170px"vtype="maxLength:50"/>
        </div>
		<div class="rightarea">
			<input id="sd" name="sd" class="mini-textbox"  label="工业码英文描述" labelField="true"  emptyText="请输入工业码英文描述" required="true"  vtype="maxLength:60"  style="width:100%" labelStyle="text-align:center;width:170px" onvalidation="onEnglishAndNumberValidation"/>
            <input id="lstmntdte" name="lstmntdte" class="mini-datepicker" format="yyyy-MM-dd" label="维护更新日期" emptyText="系统自动生成" labelField="true"   style="width:100%" labelStyle="text-align:center;width:170px" enabled="false"/>
        </div>
      </div>
      
    </div>
  </div>
  <div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
	<div style="margin-bottom:10px; text-align: center;">
		<a class="mini-button" style="display: none"  style="width:120px;" id="save_btn"  onclick="save(1)">保存</a>
    </div>
    <div style="margin-bottom:10px; text-align: center;">
		<a class="mini-button" style="display: none"  style="width:120px;" id="saveAndsync_btn"  onclick="save(2)">保存并同步至opics</a>
	</div>
	<div style="margin-bottom:10px; text-align: center;">
		<a class="mini-button" style="display: none"  style="width:120px;" id="cancel_btn"  onclick="cancel">取消</a>
	</div>
  </div>
</div>	
<script type="text/javascript">

        mini.parse();

        //获取当前tab
        var currTab = top["win"].tabs.getActiveTab();
        var params = currTab.params;
        var row=params.selectData;
        var url=window.location.search;
        var action=CommonUtil.getParam(url,"action");
        var form=new mini.Form("#field_form");

        $(document).ready(function(){
            //控制按钮显示
            $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
                initData();
            });
        });

        //初始化数据
        function initData(){
            if($.inArray(action,["edit","detail"])>-1){ //修改、详情
                form.setData(row);
                mini.get("sic").setEnabled(false);
                if(row.status == "1"){//已同步，只可以修改SWIFT码中文描述与备注
                	form.setEnabled(false);
                	mini.get("sdcn").setEnabled(true);
                	mini.get("remark").setEnabled(true);
                	mini.get("saveAndsync_btn").hide();
                }
                
                if(action == "detail"){
                    mini.get("save_btn").hide();
                    mini.get("saveAndsync_btn").hide();
                    form.setEnabled(false);
                }
            }else{//增加 
                mini.get("operator").setValue(userId);
            }
        }
        //保存
         function save(flag){
            //表单验证！！！
            form.validate();
            if (form.isValid() == false) {
            	 mini.alert("表单填写错误,请确认!", "提示信息");
                return;
            }
            var saveUrl=null;
            if(action == "add"){
                saveUrl = "/IfsOpicsSicoController/saveOpicsSicoAdd";
                var data=form.getData(true);
                var params=mini.encode(data);
                CommonUtil.ajax({
                    url:saveUrl,
                    data:params,
                    callback:function(data2){
                    	if (data2.code == 'error.common.0000') {
                    		if(flag==1){//弹出保存成功
                    			mini.alert("保存成功!",'提示信息',function(){
        							top["win"].closeMenuTab();
        						});
                    		}else if(flag==2){//弹出保存并同步到opics成功
                    			CommonUtil.ajax({
                                    url: "/IfsOpicsSicoController/syncSico",
                                    data: {sic: data.sic},
                                    callback: function (data1) {
                                        if (data1.code == 'error.common.0000') {
                                            mini.alert("保存并同步成功!",'提示信息',function(){
                                            	top["win"].closeMenuTab();
                                            });
                                            
                                        } else {
                                            mini.alert("保存成功但同步失败!",'提示信息',function(){
                                            	top["win"].closeMenuTab();	
                                            });
                                            
                                        }
                                    }
                                });
                    			
                    		}
    						
    					} else {
    						mini.alert("保存失败!");
    			    	}
                    }
    		    });
            }else if(action == "edit"){
                saveUrl = "/IfsOpicsSicoController/saveOpicsSicoEdit";
                var data=form.getData(true);
                var params=mini.encode(data);
                CommonUtil.ajax({
                    url:saveUrl,
                    data:params,
                    callback:function(data2){
                    	if (data2.code == 'error.common.0000') {
                    		if(flag==1){//弹出保存成功
                    			mini.alert("保存成功!",'提示信息',function(){
        							top["win"].closeMenuTab();
        						});
                    		}else if(flag==2){//弹出保存并同步到opics成功
                    			CommonUtil.ajax({
                                    url: "/IfsOpicsSicoController/syncSico",
                                    data: {sic: data.sic},
                                    callback: function (data1) {
                                        if (data1.code == 'error.common.0000') {
                                        	mini.alert("保存并同步成功!",'提示信息',function(){
                                            	top["win"].closeMenuTab();
                                            });
                                        } else {
                                        	mini.alert("保存成功但同步失败!",'提示信息',function(){
                                            	top["win"].closeMenuTab();	
                                            });
                                        }
                                    }
                                });
                    			
                    		}
    					} else {
    						mini.alert("修改失败");
    			    	}
                    }
    		    });
            }

           
        }
        
        

        function cancel(){
            top["win"].closeMenuTab();
        }

        //数字的验证
        function onUpperEnglishValidation(e) {
            if(e.value == "" || e.value == null){//值为空，就不做校验
                return;
            }
            if (e.isValid) {
                if (isUpperEnglish(e.value) == false) {
                    e.errorText = "必须输入数字";
                    e.isValid = false;
                }
            }
        }

        /* 是否数字 */
        function isUpperEnglish(v) {
            var re = new RegExp("^[0-9]+$");
            if (re.test(v)) return true;
            return false;
        }
        
      //英文、数字、下划线 的验证
        function onEnglishAndNumberValidation(e) {
            if(e.value == "" || e.value == null){//值为空，就不做校验
                return;
            }
            if (e.isValid) {
                if (isEnglishAndNumber(e.value) == false) {
                    e.errorText = "只能输入英文、数字、下划线('_')、横杠('-')、空格";
                    e.isValid = false;
                }
            }
        }
        
        /* 是否英文、数字、下划线、空格 */
        function isEnglishAndNumber(v) {
            var re = new RegExp("^[0-9a-zA-Z\_\-\\s]+$");
            if (re.test(v)) return true;
            return false;
        }
        
        function checkSico(){
        	var sic = mini.get("sic").getValue();
        	CommonUtil.ajax({
                url: "/IfsOpicsSicoController/searchSicoById",
                data: {sic: sic},
                callback: function (data1) {
                    if (data1.code == 'error.common.0000') {
                    	if(data1.obj != null){
                    		mini.alert("此工业码'"+sic+"'已存在! 请重新填写!",'提示信息',function(){
                    			mini.get("sic").setValue("");
                            });
                    		
                    	}
                    } else {
                    	
                    }
                }
            });
        	
        }
        
   
</script>
</body>
</html>