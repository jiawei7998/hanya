<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
    <head>
        <title>opics系统投资组合表</title>
        <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    </head>
<body style="width:100%;height:100%;background:white">
    <fieldset class="mini-fieldset title">
    <legend>opics系统投资组合信息查询</legend>
    <div id="search_form" style="width: 100%;">
        <input id="portfolio" name="portfolio" class="mini-textbox" labelField="true" label="投资组合：" labelStyle="text-align:right;" emptyText="请输入投资组合" />
       	<input id="status" name="status" class="mini-hidden" value="1"/>
        <span style="float:right;margin-right: 150px">
            <a class="mini-button" style="display: none"  id="search_btn"  onclick="search()">查询</a>
            <a class="mini-button" style="display: none"  id="clear_btn"  onclick="clear()">清空</a>
        </span>
    </div>
    </fieldset>
    
    <div class="mini-fit" style="width:100%;height:100%;">
        <div id="prod_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" 
            sortMode="client" allowAlternating="true"   idField="userId" onrowdblclick="onRowDblClick" 
            border="true" allowResize="true" >
            <div property="columns">
                <div type="indexcolumn" headerAlign="center" align="center" width="40px">序列</div>
                <!-- <div field="portid" headerAlign="center"  align="center" width="180px" >投资组合ID</div>
                <div field="portpid" headerAlign="center"  width="120px" align="center">投资组合父类</div>-->
                <div field="br" headerAlign="center" width="80px" align="center">部门</div> 
                <div field="portfolio" headerAlign="center" width="120px" align="center">投资组合</div>
                <div field="portdesc" headerAlign="center" width="120px">投资组合描述</div>
<%--                <div field="cost" headerAlign="center" width="120px" align="center">成本中心</div>--%>
<%--                <div field="text1" headerAlign="center" width="120px">备注1</div>--%>
<%--                <div field="text2" headerAlign="center" width="120px">备注2</div>--%>
<%--                <div field="date1" headerAlign="center" width="120px" renderer="onDateTimeRenderer" align="center">日期1</div>--%>
<%--                <div field="date2" headerAlign="center" width="120px" renderer="onDateTimeRenderer" align="center">日期2</div>--%>
<%--                <div field="amount1" headerAlign="center" width="120px" align="right" allowSort="true" numberFormat="n2">金额1</div>--%>
<%--                <div field="amount2" headerAlign="center" width="120px" align="right" allowSort="true" numberFormat="n2">金额2</div>--%>
<%--                <div field="updatecounter" headerAlign="center" width="120px" align="center">更改次数</div>--%>
                <div field="status" headerAlign="center" width="120px" renderer="CommonUtil.dictRenderer" data-options="{dict:'DealStatus'}" align="center">同步状态</div>
<%--                <div field="operator" headerAlign="center"  width="120px" align="center">操作人</div>--%>
                <div field="remark" headerAlign="center"  width="200px">备注</div>
                <div field="lstmntdte" headerAlign="center"  align="center" width="160px" renderer="onDateRenderer">最后维护时间</div>
            </div>
        </div>
    </div>
</body>
<script>
    mini.parse();

    function onDateRenderer(e) {
    	var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)==null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g,'/'));
        if (value) return mini.formatDate(value, 'yyyy-MM-dd');
    }
    function onDateTimeRenderer(e) {
    	var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)==null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g,'/'));
        if (value) return mini.formatDate(value, 'yyyy-MM-dd');
    }
    var grid = mini.get("prod_grid");
    grid.on("beforeload", function(e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        searchs(pageSize, pageIndex);
    });

    
    $(document).ready(function() {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
            search();
        });
	});

    //查询按钮
    function search(){
        searchs(grid.pageSize,0);
    }

    function searchs(pageSize, pageIndex) {
        var form = new mini.Form("#search_form");
        form.validate();
        if (form.isValid() == false) {
            mini.alert("表单填写错误,请确认!", "提示信息");
            return;
        }

        var data = form.getData();
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        data['branchId']=branchId;

        var params = mini.encode(data);

        CommonUtil.ajax({
            url : "/IfsOpicsPortController/searchPagePort",
            data : params,
            callback : function(data) {
                var grid = mini.get("prod_grid");
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }
    

    //清空
    function clear(){
        var form=new mini.Form("search_form");
        form.clear();
        search();
    }

    
    function GetData() {
        var row = grid.getSelected();
        return row;
    }

  	//双击行选择
    function onRowDblClick(){
        onOk();
    }

    function CloseWindow(action) {
        if (window.CloseOwnerWindow)
            return window.CloseOwnerWindow(action);
        else
            window.close();
    }

    function onOk() {
        CloseWindow("ok");
    }

    //关闭窗口
    function onCancel() {
        CloseWindow("cancel");
    }
    
    
    


    
    
  

</script>
</html>