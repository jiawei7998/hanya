<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
    <head>
        <title>万得债券信息管理</title>
        <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    </head>
<body style="width:100%;height:100%;background:white">
   <fieldset class="mini-fieldset title">
    <legend>中国债券基本信息查询</legend>
    <div id="search_form" style="width: 100%;">
        <input id="bondCode" name="bondCode" class="mini-textbox" width="320px" labelField="true" label="万得债券代码：" labelStyle="text-align:right;" emptyText="请输入万得债券代码" />
        <input id="interestTypeCode" name="interestTypeCode" class="mini-combobox" width="320px" data="CommonUtil.serverData.dictionary.IntcalRule"  emptyText="请选择付息方式代码" labelField="true"  label="付息方式代码：" labelStyle="text-align:right;"/>
        <span style="float:right;margin-right: 150px">
            <a class="mini-button" style="display: none"  id="search_btn"  onclick="search()">查询</a>
            <a class="mini-button" style="display: none"  id="clear_btn"  onclick="clear()">清空</a>
        </span>
    </div>
    </fieldset>
    
    <div class="mini-fit" style="width:100%;height:100%;">
        <div id="data_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" 
            sortMode="client" allowAlternating="true"   idField="userId"
            border="true" allowResize="true" >
            <div property="columns">
                <div type="indexcolumn" headerAlign="center" align="center" width="40px">序列</div>
                <div field="bondCode" headerAlign="center"  align="center" width="100px" >万得债券代码</div>
                <div field="bondShortName" headerAlign="center"  width="200px"> 债券简称</div>
                <div field="bondLongName" headerAlign="center"  width="480px"> 债券全称</div>
                <div field="market" headerAlign="center" width="120px">一二级市场</div>
                <div field="issuerCode" headerAlign="center" width="100px" >发行人ID</div>
                <div field="issuer" headerAlign="center" width="280px" >发行人</div>
                <div field="varietyCode" headerAlign="center" width="120px">债券品种编号</div> -->
                <!-- <div field="newQty" headerAlign="center"  width="120px">实际发行数量(亿元)</div> -->
                <div field="issuePrice" headerAlign="center"  width="100px">发行价格(元)</div>
                <div field="issueDate" headerAlign="center"  width="100px"> 发行日期</div>
                <!-- <div field="listingdate" headerAlign="center"  width="120px">上市日</div> -->
                <div field="vdate" headerAlign="center"  width="100px"> 起息日</div>
                <div field="mdate" headerAlign="center"  width="100px"> 到期日</div>
                <div field="interestTypeCode" headerAlign="center"  width="100px"> 付息方式代码</div>
                <div field="interestType" headerAlign="center"  width="100px"> 付息方式</div>
                <div field="basisType" headerAlign="center"  width="100px"> 计息方式</div>
                <div field="issuerrating" headerAlign="center"  width="100px">主体信用评级</div>
                <div field="circulationCode" headerAlign="center"  width="100px">流通标志代码</div>
                <div field="circulation" headerAlign="center"  width="80px">流通标志</div>
                <div field="custodian" headerAlign="center"  width="200px">债券托管所</div>
                <div field="basisCode" headerAlign="center"  width="150px">浮动利率基准代码</div>
                <div field="industry" headerAlign="center"  width="120px">债券所属行业</div>
                <div field="newQty" headerAlign="center"  width="80px">最新份额</div>
                <div field="maincorpid" headerAlign="center"  width="120px">组织机构代码</div>
                <div field="rateType" headerAlign="center"  width="80px">利率代码</div>
                <div field="term" headerAlign="center"  width="80px" numberFormat="n4"> 债券期限</div>
                <div field="rate" headerAlign="center"  width="80px" numberFormat="n2"> 票面利率</div>
                <div field="freq" headerAlign="center"  width="80px"> 付息周期</div>
                <div field="spread" headerAlign="center"  width="80px"> 基本利差</div>
                <div field="circuplace" headerAlign="center"  width="80px"> 流通场所</div>
                <div field="circuplaceCode" headerAlign="center"  width="80px"> 流通场所代码</div>
                <div field="ccy" headerAlign="center"  width="80px"> 币种</div>
                <div field="rightType" headerAlign="center"  width="80px"> 含权方式</div>
                <!-- <div field="isin" headerAlign="center"  width="120px"> ISIN码</div> -->
                <div field="variety" headerAlign="center"  width="120px"> 证券分类</div>
                <!--  <div field="depositary" headerAlign="center"  width="120px"> 托管人</div> -->
                <!--  <div field="creditrating1" headerAlign="center"  width="120px"> 债券信用评级</div> -->
                <!--  <div field="creditratingagency1" headerAlign="center"  width="120px"> 债券信用评级机构</div> -->
                <!-- <div field="inputdate" headerAlign="center" align="center" width="160px">录入时间</div> -->
            </div>
        </div>
    </div>
</body>
<script>
    mini.parse();


    var grid = mini.get("data_grid");
    grid.on("beforeload", function(e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        searchs(pageSize, pageIndex);
    });

    $(document).ready(function() {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
            search(10, 0);
        });
	});


    function onDateRenderer(e) {
        var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)==null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g,'/'));
        if (value) return mini.formatDate(value, 'yyyy-MM-dd');
    }
    
    
	function setData(data){
    	var form = new mini.Form("#search_form");
    	form.setData(data);
    }
    
    function searchs(pageSize, pageIndex) {
        var form = new mini.Form("#search_form");
        form.validate();
        if (form.isValid() == false) {
            mini.alert("表单填写错误,请确认!", "提示信息");
            return;
        }

        var data = form.getData();
        
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        data['branchId']=branchId;
        var params = mini.encode(data);
        CommonUtil.ajax({
            url : "/IfsWdSecController/searchPageWdBond",
            data : params,
            callback : function(data) {
                var grid = mini.get("data_grid");
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }
  //查询按钮
    function search(){
        searchs(grid.pageSize,0);
    }
    //清空
    function clear(){
        var form=new mini.Form("search_form");
        form.clear();
        search();
    }

    function GetData() {
        var grid = mini.get("data_grid");
        var row = grid.getSelected();
        return row;
    }
    
   
    function CloseWindow(action) {
        if (window.CloseOwnerWindow)
            return window.CloseOwnerWindow(action);
        else
            window.close();
    }

    function onOk() {
        CloseWindow("ok");
    }

    //关闭窗口
    function onCancel() {
        CloseWindow("cancel");
    }
    
</script>
</html>