<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
    <head>
        <title>债券额外信息管理</title>
        <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    </head>
<body style="width:100%;height:100%;background:white">
   <fieldset class="mini-fieldset title">
    <legend>债券额外信息查询</legend>
    <div id="search_form" style="width: 100%;">
        <input id="bondName" name="bondName" class="mini-textbox" width="320px" labelField="true" label="债券简称：" labelStyle="text-align:right;" emptyText="请输入债券简称" />
        <input id="bondCode" name="bondCode" class="mini-textbox" width="320px" labelField="true" label="债券代码：" labelStyle="text-align:right;" emptyText="请输入债券代码" />
        <span style="float:right;margin-right: 150px">
            <a class="mini-button" style="display: none"  id="search_btn"  onclick="search()">查询</a>
            <a class="mini-button" style="display: none"  id="clear_btn"  onclick="clear()">清空</a>
        </span>
    </div>
    </fieldset>
    
    <div class="mini-fit" style="width:100%;height:100%;">
        <div id="data_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" 
            sortMode="client" allowAlternating="true"   idField="userId"
            border="true" allowResize="true" >
            <div property="columns">
                <div field="bondName" headerAlign="center" align="center" width="120px">债券简称</div>
                <div field="bondCode" headerAlign="center" align="center" width="120px">债券代码</div>
                <div field="faMount" headerAlign="center" align="right" width="120px" numberFormat="n4">首场发行量</div>
                <div field="zaMount" headerAlign="center" align="right" width="120px" numberFormat="n4">追加发行量</div>
                <div field="baMount" headerAlign="center" align="right" width="120px" numberFormat="n4">基本承销量</div>
                <div field="expense" headerAlign="center" align="right" width="120px" numberFormat="n2">债券承揽费</div>
                <div field="zbType" headerAlign="center" align="center" width="120px">招标方式</div>
                <div field="faceRate" headerAlign="center" align="right" width="120px" numberFormat="n4">票面利率</div>
                <div field="fxRate" headerAlign="center" align="right" width="120px" numberFormat="n4">发行利率(加权平均)</div>
                <div field="fxBjRate" headerAlign="center" align="right" width="120px" numberFormat="n4">发行价格(加权平均)</div>
                <div field="fxBjPrice" headerAlign="center" align="right" width="120px" numberFormat="n4">发行边际利率(元)</div>
                <div field="qctb" headerAlign="center" align="right" width="120px" numberFormat="n4">发行边际价格(元)</div>
                <div field="bjtb" headerAlign="center" align="right" width="120px" numberFormat="n4">全场投标倍数</div>
                <div field="ifName" headerAlign="center" align="center" width="200px">发行人</div>
                <div field="zxLevel" headerAlign="center" align="center" width="120px">债项评级</div>
                <div field="dbName" headerAlign="center" align="center" width="120px">担保人</div>
                <div field="dbType" headerAlign="center" align="center" width="120px">担保方式</div>
                <div field="zbDate" headerAlign="center" align="center" width="120px" renderer="onDateRenderers">招投标日期</div>
                <div field="ifDate" headerAlign="center" align="center" width="120px">发行公告日</div>
                <div field="addInfo" headerAlign="center" align="center" width="120px">投标期次</div>
                <div field="spotDate" headerAlign="center" align="center" width="180px" renderer="onDateRenderer">生效日期</div>
                
            </div>
        </div>
    </div>
</body>
<script>
    mini.parse();

    var grid = mini.get("data_grid");
    grid.on("beforeload", function(e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        searchs(pageSize, pageIndex);
    });

    $(document).ready(function() {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
            search(10, 0);
        });
	});
    
    function onDateRenderer(e) {
        var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)==null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g,'/'));
        if (value) return mini.formatDate(value, 'yyyy-MM-dd HH:mm:ss');
    }
	function setData(data){
    	var form = new mini.Form("#search_form");
    	form.setData(data);
    }
    
    function searchs(pageSize, pageIndex) {
        var form = new mini.Form("#search_form");
        form.validate();
        if (form.isValid() == false) {
            mini.alert("表单填写错误,请确认!", "提示信息");
            return;
        }

        var data = form.getData();
        
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        data['branchId']=branchId;
        var params = mini.encode(data);
        CommonUtil.ajax({
            url : "/IfsWdSecController/searchPageWdBondAdd",
            data : params,
            callback : function(data) {
                var grid = mini.get("data_grid");
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }
  //查询按钮
    function search(){
        searchs(grid.pageSize,0);
    }
    //清空
    function clear(){
        var form=new mini.Form("search_form");
        form.clear();
        search();
    }

    function GetData() {
        var grid = mini.get("data_grid");
        var row = grid.getSelected();
        return row;
    }
    


    function CloseWindow(action) {
        if (window.CloseOwnerWindow)
            return window.CloseOwnerWindow(action);
        else
            window.close();
    }

    function onOk() {
        CloseWindow("ok");
    }

    //关闭窗口
    function onCancel() {
        CloseWindow("cancel");
    }
    
</script>
</html>