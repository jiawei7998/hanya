<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
    <head>
        <title>中国共同基金基本资料</title>
        <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    </head>
<body style="width:100%;height:100%;background:white">
   <fieldset class="mini-fieldset title">
    <legend>中国共同基金基本资料查询</legend>
    <div id="search_form" style="width: 100%;">
        <input id="F_INFO_WINDCODE" name="F_INFO_WINDCODE" class="mini-textbox" width="320px" labelField="true" label="Wind基金代码：" labelStyle="text-align:right;" emptyText="请输入Wind基金代码" />
        <span style="float:right;margin-right: 150px">
            <a class="mini-button" style="display: none"  id="search_btn"  onclick="search()">查询</a>
            <a class="mini-button" style="display: none"  id="clear_btn"  onclick="clear()">清空</a>
        </span>
    </div>
    </fieldset>
    
    <div class="mini-fit" style="width:100%;height:100%;">
        <div id="data_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" 
            sortMode="client" allowAlternating="true"   idField="userId"
            border="true" allowResize="true" >
            <div property="columns">
                <div type="indexcolumn" headerAlign="center" align="center" width="40px">序列</div>     
                <div field="f_INFO_WINDCODE" headerAlign="center"  width="120px"> Wind基金代码</div>

                <div field="f_INFO_FULLNAME" headerAlign="center" width="250px">基金全称</div> -->
                <div field="f_INFO_NAME" headerAlign="center" width="120px" >基金简称</div>
                <div field="f_INFO_CORP_FUNDMANAGEMENTCOMP" headerAlign="center" width="120px" >管理人</div>
                <div field="f_INFO_CUSTODIANBANK" headerAlign="center" width="120px">托管人</div>
                <div field="f_INFO_FIRSTINVESTTYPE" headerAlign="center"  width="120px">基金类型</div>
                <div field="f_INFO_SETUPDATE" headerAlign="center"  width="120px" > 成立日期</div>
                <div field="f_ISSUE_TOTALUNIT" headerAlign="center"  width="120px">发行份额(亿)</div>
                <div field="f_INFO_MANAGEMENTFEERATIO" headerAlign="center"  width="120px">管理费率(%)</div>
                <div field="f_INFO_CUSTODIANFEERATIO" headerAlign="center"  width="120px">托管费率(%)</div>
                <div field="cRNY_CODE" headerAlign="center"  width="120px">基金币种</div>
                <div field="f_INFO_PARVALUE" headerAlign="center"  width="120px">面值</div>
                <div field="f_PCHREDM_PCHSTARTDATE" headerAlign="center"  width="120px" >上市日期/申购首日</div>
                <div field="f_INFO_REDMSTARTDATE" headerAlign="center"  width="120px" >日常赎回起始日</div>
                <div field="f_INFO_MINBUYAMOUNT" headerAlign="center"  width="120px">起点金额(元)</div>
                <div field="f_INFO_BENCHMARK" headerAlign="center"  width="120px">业绩比较基准</div>
                <div field="f_INFO_STATUS" headerAlign="center"  width="120px">存续状态</div>
                <div field="f_INFO_EXCHMARKET" headerAlign="center"  width="120px">交易所</div>
                <div field="f_INFO_FIRSTINVESTSTYLE" headerAlign="center"  width="120px">投资风格</div>
                <div field="f_INFO_ISSUEDATE" headerAlign="center"  width="120px" > 发行日期</div>
                <div field="f_INFO_TYPE" headerAlign="center"  width="120px">运作类型</div>
                <div field="f_INFO_ISINITIAL" headerAlign="center"  width="120px" renderer="CommonUtil.dictRenderer" data-options="{dict:'YesNo'}">是否为初始基金</div>
                <div field="f_INFO_PINYIN" headerAlign="center"  width="120px">拼音</div>
                <div field="f_INFO_INVESTSCOPE" headerAlign="center"  width="120px">投资说明</div>
                <div field="f_INFO_INVESTOBJECT" headerAlign="center"  width="120px">投资对象</div>
                <div field="f_INFO_INVESTCONCEPTION" headerAlign="center"  width="120px">投资概念</div>
                <div field="f_INFO_DECISION_BASIS" headerAlign="center"  width="120px"> 管理基准</div>-->
                <div field="iS_INDEXFUND" headerAlign="center"  width="165px" renderer="CommonUtil.dictRenderer" data-options="{dict:'YesNo'}">是否是指数化证券投资基金</div>
                <div field="f_INFO_CORP_FUNDMANAGEMENTID" headerAlign="center"  width="120px">基金管理公司ID</div>
                <div field="f_INFO_CUSTODIANBANKID" headerAlign="center"  width="120px">基金经理</div>
                <!--<div field="mAX_NUM_COLTARGET" headerAlign="center"  width="120px"></div>-->
                <div field="iNVESTSTRATEGY" headerAlign="center"  width="120px">投资策略</div>
                <div field="rISK_RETURN" headerAlign="center"  width="120px">风险收益</div>
                <div field="f_PCHREDM_PCHMINAMT" headerAlign="center"  width="150px">每次申购最低金额(万元)</div>
                <div field="f_PCHREDM_PCHMINAMT_EX" headerAlign="center"  width="150px">每次申购最低范围(万元)</div>
                <div field="f_INFO_LISTDATE" headerAlign="center"  width="120px" >上市日期</div>
                <div field="f_INFO_ANNDATE" headerAlign="center"  width="120px" >公告日期</div>
                <div field="f_INFO_REGISTRANT" headerAlign="center"  width="120px">注册人</div>
                <div field="f_PERSONAL_STARTDATEIND" headerAlign="center"  width="120px" >个人申购起始日</div>
                <div field="f_PERSONAL_ENDDATEIND" headerAlign="center"  width="120px" >个人申购结束日</div>
                <div field="f_INFO_FUND_ID" headerAlign="center"  width="120px">基金ID</div>
                <div field="f_SALES_SERVICE_RATE" headerAlign="center"  width="120px">销售服务速率</div>
                <div field="s_FELLOW_DISTOR" headerAlign="center"  width="120px">总有效收购数</div>
                <div field="f_PERSONAL_SUBTYPE" headerAlign="center"  width="120px">个人投资类型</div>
<%--                <div field="cLOSE_INSTITU_SUBTYPE" headerAlign="center"  width="120px">demo</div>--%>
                <div field="oPDATE" headerAlign="center"  width="120px"  renderer="onDateRenderer">操作日期</div>
                <!--<div field="oPMODE" headerAlign="center"  width="120px">操作类型</div>-->
                <div field="mOPDATE" headerAlign="center"  width="120px">操作时间</div>
             </div>
        </div>
    </div>
</body>
<script>
    mini.parse();

    var grid = mini.get("data_grid");
    grid.on("beforeload", function(e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        searchs(pageSize, pageIndex);
    });

    $(document).ready(function() {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
            search();
        });
	});


    function onDateRenderer(e) {
        var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)==null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g,'/'));
        if (value) return mini.formatDate(value, 'yyyy-MM-dd');
    }
    
    
	function setData(data){
    	var form = new mini.Form("#search_form");
    	form.setData(data);
    }
    
    function searchs(pageSize, pageIndex) {
        var form = new mini.Form("#search_form");
        form.validate();
        if (form.isValid() == false) {
            mini.alert("表单填写错误,请确认!", "提示信息");
            return;
        }

        var data = form.getData();
        
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        data['branchId']=branchId;
        var params = mini.encode(data);
        CommonUtil.ajax({
            url : "/IfsWdSecController/searchFundAllList",
            data : params,
            callback : function(data) {
                var grid = mini.get("data_grid");
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }
  //查询按钮
    function search(){
        searchs(grid.pageSize,0);
    }
    //清空
    function clear(){
        var form=new mini.Form("search_form");
        form.clear();
        search();
    }

    function GetData() {
        var grid = mini.get("data_grid");
        var row = grid.getSelected();
        return row;
    }
    
   
    function CloseWindow(action) {
        if (window.CloseOwnerWindow)
            return window.CloseOwnerWindow(action);
        else
            window.close();
    }

    function onOk() {
        CloseWindow("ok");
    }

    //关闭窗口
    function onCancel() {
        CloseWindow("cancel");
    }
    
</script>
</html>