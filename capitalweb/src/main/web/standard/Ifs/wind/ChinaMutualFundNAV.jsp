<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
    <head>
        <title>万得财富库信息</title>
        <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    </head>
<body style="width:100%;height:100%;background:white">
   <fieldset class="mini-fieldset title">
    <legend>基金净值查询</legend>
    <div id="search_form" style="width: 100%;">
<%--        <input id="OBJECT_ID" name="OBJECT_ID" class="mini-textbox" width="320px" labelField="true" label="对象ID：" labelStyle="text-align:right;" emptyText="请输入对象ID" />--%>
        <input id="F_INFO_WINDCODE" name="F_INFO_WINDCODE" class="mini-textbox" width="320px" labelField="true" label="Wind基金代码：" labelStyle="text-align:right;" emptyText="请输入万得基金代码" />
<%--        <input id="OPMODE" name="OPMODE" class="mini-textbox" width="320px" labelField="true" label="操作类型：" labelStyle="text-align:right;" emptyText="请输入操作类型" />--%>
        <span style="float:right;margin-right: 150px">
            <a class="mini-button" style="display: none"  id="search_btn"  onclick="search()">查询</a>
            <a class="mini-button" style="display: none"  id="clear_btn"  onclick="clear()">清空</a>
        </span>
    </div>
    </fieldset>
    
    <div class="mini-fit" style="width:100%;height:100%;">
        <div id="data_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" 
            sortMode="client" allowAlternating="true"   idField="userId"
            border="true" allowResize="true" >
            <div property="columns">
                <div type="indexcolumn" headerAlign="center" align="center" width="40px">序列</div>
                <div field="f_INFO_WINDCODE" headerAlign="center"  width="100px"> Wind基金代码</div>
                <div field="aNN_DATE" headerAlign="center"  width="100px"> 公告日期</div>
                <div field="pRICE_DATE" headerAlign="center"  width="100px">截止日期</div>
                <div field="f_NAV_UNIT" headerAlign="center" align="right" width="80px" numberFormat="n4">单位净值</div> -->
                <div field="f_NAV_ACCUMULATED" headerAlign="center" align="right" width="80px" numberFormat="n4">累计净值</div>
                <div field="f_NAV_ADJFACTOR" headerAlign="center" align="right" width="120px" numberFormat="n4">复权因子</div>
                <div field="cRNCY_CODE" headerAlign="center" width="80px">货币代码</div>
                <div field="f_NAV_ADJUSTED" headerAlign="center" align="right" width="120px" numberFormat="n4">累计调整值</div>
                <div field="iS_EXDIVIDENDDATE" headerAlign="center"  width="80px"  renderer="CommonUtil.dictRenderer" data-options="{dict:'YesNo'}"> 是否除息</div>
                <div field="f_NAV_DISTRIBUTION" headerAlign="center" align="right"  width="80px" numberFormat="n4"> 累计分销</div>
                <div field="s_INFO_ASHARECODE" headerAlign="center"  width="80px"> 计息方式</div>   
                <div field="oPDATE" headerAlign="center"  width="120px"  renderer="onDateRenderer">操作时间</div>
                <!--<div field="oPMODE" headerAlign="center"  width="80px">操作类型</div>-->
                
                <!--<div field="mOPDATE" headerAlign="center"  width="120px" renderer="onDateRenderer"></div>-->
                
            </div>
        </div>
    </div>
</body>
<script>
    mini.parse();


    var grid = mini.get("data_grid");
    grid.on("beforeload", function(e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        searchs(pageSize, pageIndex);
    });

    $(document).ready(function() {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
            search();
        });
	});


    function onDateRenderer(e) {
        var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)==null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g,'/'));
        if (value) return mini.formatDate(value, 'yyyy-MM-dd HH:mm:ss');
    }
    
    
	function setData(data){
    	var form = new mini.Form("#search_form");
    	form.setData(data);
    }
    
    function searchs(pageSize, pageIndex) {
        var form = new mini.Form("#search_form");
        form.validate();
        if (form.isValid() == false) {
            mini.alert("表单填写错误,请确认!", "提示信息");
            return;
        }

        var data = form.getData();
        
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        data['branchId']=branchId;
        var params = mini.encode(data);
        CommonUtil.ajax({
            url : "/IfsWdSecController/searchFundList",
            data : params,
            callback : function(data) {
                var grid = mini.get("data_grid");
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }
  //查询按钮
    function search(){
        searchs(grid.pageSize,0);
    }
    //清空
    function clear(){
        var form=new mini.Form("search_form");
        form.clear();
        search();
    }

    function GetData() {
        var grid = mini.get("data_grid");
        var row = grid.getSelected();
        return row;
    }
    
   
    function CloseWindow(action) {
        if (window.CloseOwnerWindow)
            return window.CloseOwnerWindow(action);
        else
            window.close();
    }

    function onOk() {
        CloseWindow("ok");
    }

    //关闭窗口
    function onCancel() {
        CloseWindow("cancel");
    }
    
</script>
</html>