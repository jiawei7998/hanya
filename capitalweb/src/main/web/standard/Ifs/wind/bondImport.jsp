<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<script type="text/javascript" src="<%=basePath%>/miniScript/miniui/res/ajaxfileupload.js"></script>
    
<div class="mini-panel" title="债券信息导入" style="width: 100%;height: 100%;">
		<div style="margin: 20px;">导入债券信息到OPICS：
			<input class="mini-htmlfile" name="Fdata"  id="file1" style="width:300px;"/>
			<div style="display: inline;padding-left: 8px;">
				<a class="mini-button sl-linear-buton" iconCls="iconfont icon-bianji3" onclick="ajaxFileUpload">导入</a>
			</div>
		</div>

	</div>
<script>

	  function ajaxFileUpload() {
   
        var inputFile = $("#file1 > input:file")[0];
        var messageid = mini.loading("加载中,请稍后... ", "提示信息");
        $.ajaxFileUpload({
            url: '<%=basePath%>/sl/IfsOpicsBondController/importBondByExcel', //用于文件上传的服务器端请求地址
            fileElementId: inputFile,               //文件上传域的ID
            //data: { a: 1, b: true },            //附加的额外参数
            dataType: 'text',    
			//返回值类型 一般设置为json
		      success: function (data, status)    //服务器成功响应处理函数
		      {
				var str = data.replace(/<.*?>/ig,"");
		    	mini.hideMessageBox(messageid);
              	mini.alert(str);
		
		      },
		      error: function (data, status, e)   //服务器响应失败处理函数
		      {
		      	mini.hideMessageBox(messageid);
		          mini.alert("导入失败: ");
		      },
		      complete: function () {
		          var jq = $("#file1 > input:file");
		          jq.before(inputFile);
		          jq.remove();
		      }
		  });
		}
</script>
      	