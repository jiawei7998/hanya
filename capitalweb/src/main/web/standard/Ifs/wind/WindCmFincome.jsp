<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>

<body style="width: 100%; height: 100%; background: white">
<div class="mini-panel" title="中国国币市场基金收益查询" style="width:100%;"  allowResize="true" collapseOnTitleClick="true">
    <div id="search_form" style="width: 100%;padding-top:10px;">
        <input id="S_INFO_WINDCODE" name="S_INFO_WINDCODE" class="mini-textbox" labelField="true" label="Wind基金代码：" labelStyle="text-align:right;" emptyText="请填写Wind基金代码" width="280px" />

        <nobr>
            <input id="startDate" name="startDate" class="mini-datepicker" labelField="true" label="交易日期："
                   ondrawdate="onDrawDateStart" onvaluechanged='dateCalculation' labelStyle="text-align:right;" emptyText="起始日期" format="yyyy-MM-dd"/>
            <span>~</span>
            <input id="endDate" name="endDate" class="mini-datepicker"
                   ondrawdate="onDrawDateEnd"  onvaluechanged='dateCalculation' emptyText="结束日期" format="yyyy-MM-dd" value="<%=__bizDate%>"/>
        </nobr>

        <span style="float: right; margin-right: 150px">
            <a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
            <a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
        </span>
    </div>
</div>
<div id="grid1" class="mini-datagrid borderAll" style="width:100%;height:80%;" idField="id" multiSelect="true"
     allowAlternating="true" allowResize="true" sortMode="client" allowAlternating="true">
    <div property="columns">
        <div type="indexcolumn" width="50px" headerAlign="center">序号</div>
        <div field="s_INFO_WINDCODE" width="300px" align="left"  headerAlign="center" >Wind基金代码</div>
        <div field="f_INFO_BGNDATE" width="150"  headerAlign="center" align="center" >起始日期</div>
        <div field="f_INFO_ENDDATE" width="120"  headerAlign="center" align="center" >截止日期</div>
        <div field="f_INFO_UNITYIELD" width="120" numberFormat="n4" headerAlign="center" align="center"  >每万份基金单位收益</div>
        <div field="f_INFO_YEARLYROE" width="200" numberFormat="n4" headerAlign="center" align="center" >最近七日收益所算的年资产收益率</div>
        <div field="aNN_DATE" width="120"  headerAlign="center" align="center" >公告日期</div>
<%--        <div field="updateTime" width="120"  headerAlign="center" align="center" >操作时间</div>--%>
    </div>
</div>
<script>
    mini.parse();

    //获取当前tab
    var grid = mini.get("grid1");
    grid.on("beforeload", function(e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize, pageIndex);
    });

    $(document).ready(function() {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
            search(10, 0);
        });
    });

    //交易日期
    function onDrawDateStart(e) {
        var startDate = e.date;
        var endDate= mini.get("endDate").getValue();
        if(CommonUtil.isNull(endDate)){
            return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }

    function onDrawDateEnd(e) {
        var endDate = e.date;
        var startDate = mini.get("startDate").getValue();
        if(CommonUtil.isNull(startDate)){
            return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }

    // 初始化数据
    function query(){
        search(grid.pageSize, 0);
    }
    function search(pageSize,pageIndex){
        var form = new mini.Form("#search_form");

        var data=form.getData(true);
        data['pageNumber']=pageIndex+1;
        data['pageSize']=pageSize;
        var url="/CmFincomeController/getCmFincomePage";
        var params = mini.encode(data);
        CommonUtil.ajax({
            url:url,
            data:params,
            callback : function(data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }

    function clear(){
        var form=new mini.Form("search_form");
        form.clear();
        query();
    }
</script>
</body>
</html>
