<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>大额清算交易统计表</title>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>
<body style="width: 100%; height: 100%; background: white">
<fieldset class="mini-fieldset">
	<legend>查询条件</legend>
	<div id="search_form" style="width: 70%;margin-top:15px">
		<input id="br" name="br" labelField="true" label="用户所属OPICS部门：" vtype="maxLength:5" style="width:200px" labelStyle="text-align:left;" emptyText="请选择用户所属OPICS部门" class="mini-combobox mini-mustFill"
						data="CommonUtil.serverData.dictionary.opicsBr" value="<%=__sessionUser.getOpicsBr()%>" />
		<input id="oper" name="oper" class="mini-buttonedit" onbuttonclick="onButtonEdit"  style="width:300px"labelField="true" label="经办/复核人：" labelStyle="text-align:right;" emptyText="请选择经办/复核人" />
	   <nobr>
			<input id="startDate" name="startDate" class="mini-datepicker" labelField="true" label="复核日期："
						ondrawdate="onDrawDateStart"  onvaluechanged='dateCalculation' labelStyle="text-align:right;" emptyText="起始日期" format="yyyy-MM-dd" value="<%=__bizDate%>"/>
			<span>~</span>
			<input id="endDate" name="endDate" class="mini-datepicker" 
						ondrawdate="onDrawDateEnd"  onvaluechanged='dateCalculation'  emptyText="结束日期" format="yyyy-MM-dd" value="<%=__bizDate%>"/>
	   </nobr>		
	</div>
	<span style="float: right; margin-right: 40px;"> 
		<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
		<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
		<a id="export_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
	</span>
</fieldset>

 <div showCollapseButton="true" style="height:100%;">
        <div class="mini-fit" style="height:100%;">
        	<div id="datagrid" class="mini-datagrid borderAll" style="width: 100%; height: 65%;" allowAlternating="true"
		         allowResize="true" border="true" sortMode="client" multiSelect="true">
			<div property="columns">
				<div type="indexcolumn" headerAlign="center" width="40">序号</div>
				<div field="dealno" name="dealno" width="80" align="center" headerAlign="center" >流水号</div>
				<div field="product" width="80" align="center" headerAlign="center" allowSort="false">产品</div>
				<div field="settflag" width="80" align="center" headerAlign="center" allowSort="true" renderer="CommonUtil.dictRenderer"
				     data-options="{dict:'rbstatus'}">处理状态</div>
			   <div field="dealstatus" width="80" align="center" headerAlign="center" allowSort="true"
				    renderer="CommonUtil.dictRenderer" data-options="{dict:'handleflag'}">清算状态</div>
				<div field="amount" width="120" numberFormat="#,0.00" align="right" headerAlign="center" allowSort="true">结算金额</div>
				<div field="payBankid" width="120" align="center" headerAlign="center" allowSort="false">付款人账号</div>
				<div field="payBankname" width="120" align="center" headerAlign="center" allowSort="false">付款人名称</div>
				<div field="recUserid" width="120" align="center" headerAlign="center" allowSort="false">收款人账号</div>
				<div field="recUsername" width="120" align="center" headerAlign="center" allowSort="false">收款人名称</div>
				<div field="iopername" width="120" align="center" headerAlign="center" allowSort="false">经办人名称</div>
				<div field="itime" width="120" align="center" headerAlign="center" allowSort="false">经办时间</div>
				<div field="vopername" width="120" align="center" headerAlign="center" allowSort="false">复核人名称</div>
				<div field="vtime" width="120" align="center" headerAlign="center" allowSort="false">复核时间</div>
				
			</div>
	     </div>
		<legend>汇总</legend>		
		  	<br />
		  	经办/复核人：<input id="iname" name="iname" size="20" disabled></input>
		  	<br />
		  	<br />
		  	经办交易总数：<input id="icount" name="icount" size="8" disabled></input>
		  	复核交易总数：<input id="vcount" name="vcount" size="8" disabled></input>
        </div>		  			
    </div>        
<script>
	mini.parse();
	var url = window.location.search;
	var grid = mini.get("datagrid");
	var userId='<%=__sessionUser.getUserId()%>';
	
	grid.on("beforeload", function(e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex;
		var pageSize = e.data.pageSize;
		search(pageSize, pageIndex);
	});
	//选择用户
	function onButtonEdit(e) {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Common/MiniUserManagesMini.jsp",
            title: "选择经办/复核列表",
            width: 700,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.userId);
                        btnEdit.setText(data.userName);
                        btnEdit.focus();
                    }
                }

            }
        });
    }
	function countDeal(e){//统计记录
		var list=e;
	    var icount=0;//经办数量
	    var vcount=0;//复核数量
	    var iname=mini.get("oper").getValue();//经办/复核人
	    if(iname!=null&iname!=""){
	    	$("#iname").val(mini.get("oper").getText());
	    }
	    for(var i=0;i<list.length;i++){
	    	if(list[i].ioper==iname){
	    		icount++;
	    	}
	    	if(list[i].voper==iname){
	    		vcount++;
	    	}
	    }
	    $("#icount").val(icount);
	    $("#vcount").val(vcount);
	}
	
	// 查询
	function search(pageSize,pageIndex){
		var form = new mini.Form("#search_form");
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统也提示");
			return;
		}
		dateCalculation();
		var data = form.getData(true); //获取表单数据
		data['pageNumber'] = pageIndex + 1;
		data['pageSize'] = pageSize;
		var param = mini.encode(data); //序列化json格式
		CommonUtil.ajax({
			url : "/TrdSettleController/searchCnapsDealReport",
			data : param,
			callback : function(data) {
				var grid = mini.get("datagrid");
				//设置分页
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
				grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
				countDeal(data.obj.rows);
			}
		});
	}
		
	function query() {
    	search(grid.pageSize, 0);
    }
	
	function clear(){
        var form=new mini.Form("search_form");
        form.clear();
        $("#icount").val("");
	    $("#vcount").val("");
	    $("#iname").val("");
	    initDate();
	    mini.get("br").setValue(opicsBr);//自贸区加br
        search(grid.pageSize, 0);
	}
	
	$(document).ready(function() {
		initDate();
		search(10, 0);
	});
	function initDate(){
		var date=new Date(sysDate.replace(/-/g,'/'));
	    var year=date.getFullYear();
	    var month=date.getMonth()+1;
	    var start=new Array();
	    start.push(year);
	    start.push(month);
	    start.push("1");
	    mini.get("startDate").setValue(start.join("-"));
	    mini.get("endDate").setValue(getLastDay(year,month));
	}
	//获得每月最后一天
	 function getLastDay(year,month){         
         var new_year = year;    //取当前的年份
         var new_month = month++;//取下一个月的第一天，方便计算（最后一天不固定）
         if(month>12) {         
          new_month -=12;        //月份减
          new_year++;            //年份增
         }         
         var new_date = new Date(new_year,new_month,1);                //取当年当月中的第一天
         return (new Date(new_date.getTime()-1000*60*60*24));//获取当月最后一天日期
    } ;

	//日期
	function onDrawDateStart(e) {
        var startDate = e.date;
        var endDate= mini.get("endDate").getValue();
        if(CommonUtil.isNull(endDate)){
        	return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }
	
	function onDrawDateEnd(e) {
        var endDate = e.date;
        var startDate = mini.get("startDate").getValue();
        if(CommonUtil.isNull(startDate)){
        	return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }
	//计算时间段长度
	function dateCalculation(){
		var start = mini.get("startDate").getValue();
		var end= mini.get("endDate").getValue();
		if(CommonUtil.isNull(start)||CommonUtil.isNull(end)){
			return;
		}
		var subDate= Math.abs(parseInt((end.getTime() - start.getTime())/1000/3600/24));
		if(subDate>30){
			mini.alert('查询时间大于一个月，请重新选择');
			return;
		}
	}
	//导出
	function exportExcel(){
		var content = grid.getData();
		if(content.length == 0){
			mini.alert("请先查询数据");
			return;
		}
		mini.confirm("您确认要导出Excel吗?","系统提示", 
			function (action) {
				if (action == "ok"){
			 		var form = new mini.Form("#search_form");
					var data = form.getData(true);
					var fields = null;
					if(data){
						data['oper']=data.oper;
						data['opername']=mini.get("oper").getText();
					}
					for(var id in data){
						fields += '<input type="hidden" id="' + id + '" name="'+ id +'" value="' + data[id] + '">';
					}
					var urls = CommonUtil.pPath + "/sl/IfsExportController/exportCnapsCount";                                                                                                                         
					$('<form action="'+ urls +'" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();   
				}
			}
		);
	}
</script>
</body>
</html>