<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<title>同业存单投资业务限额管理执行表</title>
<head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>

<body style="width:100%;height:100%;background:white">
    <div>
        <div class="mini-panel" title="查询条件" style="width:100%;"  allowResize="true" collapseOnTitleClick="true">
			<div id="search_form" style="width: 100%;padding-top:10px;">
				<input id="oprDate" name="oprDate" class="mini-datepicker" labelField="true" label="限额日期：" labelStyle="text-align:right;" emptyText="请输入限额日期" value="<%=__bizDate%>" />
				<span style="float: right; margin-right: 150px"> 
					<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
					<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
					<a id="export_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
				</span>
			</div>
		</div>
		<br />
		<br />	
        
        <div id="datagrid" class="fieldset-body">
            <table id="field_form" class="form-table" width="80%" border = "1" style ="text-align:center" cellspacing="0">
				<tr border = "0px">
					<th >账户类型</th>
					<th >同业存单限额类型</th>
					<th >限额指标</th>
					<th >当前值(元)</th>
				</tr>
				<tr>
					<td rowspan="6">交易账户</td>
					<td rowspan="2">交易限额</td>
					<td style ="text-align:left">交易账户投资限额</td>
					<td style ="text-align:right">
						<input id="cdxe11" name="cdxe11" readonly="true" class="mini-textbox" enabled="false" style="width:100%" />
					</td>
				</tr>
				<!-- <tr>
					<td style ="text-align:left">交易账户信用债投资限额</td>
					<td style ="text-align:right">
						<input id="cdxe12" name="cdxe12" readonly="true" class="mini-textbox" enabled="false" style="width:100%" />
					</td>
				</tr> -->
				<tr>
					<td style ="text-align:left">交易账户信用债AA级及以下债券余额</td>
					<td style ="text-align:right">
						<input id="cdxe13" name="cdxe13" readonly="true" class="mini-textbox" enabled="false" style="width:100%" />
					</td>
				</tr>
				<tr>
					<td rowspan="2">风险限额</td>
					<td style ="text-align:left">交易账户债券久期限额</td>
					<td style ="text-align:right">
						<input id="cdxe14" name="cdxe14" readonly="true" class="mini-textbox" enabled="false" style="width:100%" />
					</td>
				</tr>
				<tr>
					<td style ="text-align:left">交易账户DV01限额</td>
					<td style ="text-align:right">
						<input id="cdxe15" name="cdxe15" readonly="true" class="mini-textbox" enabled="false" style="width:100%" />
					</td>
				</tr>
				<tr>
					<td rowspan="2">止损限额</td>
					<td style ="text-align:left">交易账户止损限额(年)</td>
					<td style ="text-align:right">
						<input id="cdxe16" name="cdxe16" readonly="true" class="mini-textbox" enabled="false" style="width:100%" />
					</td>
				</tr>
				<tr>
					<td style ="text-align:left">交易账户止损限额(月)</td>
					<td style ="text-align:right">
						<input id="cdxe17" name="cdxe17" readonly="true" class="mini-textbox" enabled="false" style="width:100%" />
					</td>
				</tr>
				
				<tr>
					<td rowspan="6">可供出售账户</td>
					<td rowspan="2">交易限额</td>
					<td style ="text-align:left">可供出售账户投资限额</td>
					<td style ="text-align:right">
						<input id="cdxe21" name="cdxe21" readonly="true" class="mini-textbox" enabled="false" style="width:100%" />
					</td>
				</tr>
				<!-- <tr>
					<td style ="text-align:left">可供出售账户信用债投资限额</td>
					<td style ="text-align:right">
						<input id="cdxe22" name="cdxe22" readonly="true" class="mini-textbox" enabled="false" style="width:100%" />
					</td>
				</tr> -->
				<tr>
					<td style ="text-align:left">可供出售账户信用债AA-级及以下债券余额</td>
					<td style ="text-align:right">
						<input id="cdxe23" name="cdxe23" readonly="true" class="mini-textbox" enabled="false" style="width:100%" />
					</td>
				</tr>
				<tr>
					<td rowspan="2">风险限额</td>
					<td style ="text-align:left">可供出售账户债券久期限额</td>
					<td style ="text-align:right">
						<input id="cdxe24" name="cdxe24" readonly="true" class="mini-textbox" enabled="false" style="width:100%" />
					</td>
				</tr>
				<tr>
					<td style ="text-align:left">可供出售账户DV01限额</td>
					<td style ="text-align:right">
						<input id="cdxe25" name="cdxe25" readonly="true" class="mini-textbox" enabled="false" style="width:100%" />
					</td>
				</tr>
				<tr>
					<td rowspan="2">止损限额</td>
					<td style ="text-align:left">可供出售账户止损限额(年)</td>
					<td style ="text-align:right">
						<input id="cdxe26" name="cdxe26" readonly="true" class="mini-textbox" enabled="false" style="width:100%" />
					</td>
				</tr>
				<tr>
					<td style ="text-align:left">可供出售账户止损限额(月)</td>
					<td style ="text-align:right">
						<input id="cdxe27" name="cdxe27" readonly="true" class="mini-textbox" enabled="false" style="width:100%" />
					</td>
				</tr>
				
				<tr>
					<td rowspan="3">持有至到期账户</td>
					<td rowspan="2">交易限额</td>
					<td style ="text-align:left">持有至到期账户投资限额</td>
					<td style ="text-align:right">
						<input id="cdxe31" name="cdxe31" readonly="true" class="mini-textbox" enabled="false" style="width:100%" />
					</td>
				</tr>
				<!-- <tr>
					<td style ="text-align:left">持有至到期账户信用债投资限额</td>
					<td style ="text-align:right">
						<input id="cdxe32" name="cdxe32" readonly="true" class="mini-textbox" enabled="false" style="width:100%" />
					</td>
				</tr> -->
				<tr>
					<td style ="text-align:left">持有至到期账户信用债AA-级及以下债券余额</td>
					<td style ="text-align:right">
						<input id="cdxe33" name="cdxe33" readonly="true" class="mini-textbox" enabled="false" style="width:100%" />
					</td>
				</tr>
				<tr>
					<td rowspan="1">风险限额</td>
					<td style ="text-align:left">持有至到期账户债券久期限额</td>
					<td style ="text-align:right">
						<input id="cdxe34" name="cdxe34" readonly="true" class="mini-textbox" enabled="false" style="width:100%" />
					</td>
				</tr>
			</table>
			<br />
			<br />
			<br />
			<br />
        </div>
    </div>
    <script type="text/javascript">
        mini.parse();
        //获取当前tab
        var grid = mini.get("datagrid");
        var currTab = top["win"].tabs.getActiveTab();
        var params = currTab.params;
        var row=params.selectData;
        var url=window.location.search;
        var form=new mini.Form("#field_form");

        $(document).ready(function(){
        	search(10, 0);
        }); 
        
        $(document).ready(function() {
			search(10, 0);
		});

     // 初始化数据
    	function query(){
    		search(10, 0);
    	}
        
    	function search(pageSize,pageIndex){
    		var form = new mini.Form("#search_form");
    		form.validate();
    		if(form.isValid()==false){
    			mini.alert("信息填写有误，请重新填写","系统提示");
    			return;
    		}
    		var data=form.getData(true);
    		data['pageNumber']=pageIndex+1;
    		data['pageSize']=pageSize;
    		data['branchId']=branchId;
    		var url="/IfsLimitController/getCdInvestmentLimit";
    		
    		var params = mini.encode(data);
    		CommonUtil.ajax({
    			url:url,
    			data:params,
    			callback : function(data) {
    				var a = data.obj;
        				mini.get("cdxe11").setValue(a[0].cdxe11);
        				mini.get("cdxe13").setValue(a[0].cdxe13);
        				mini.get("cdxe14").setValue(a[0].cdxe14);
        				mini.get("cdxe15").setValue(a[0].cdxe15);
        				mini.get("cdxe16").setValue(a[0].cdxe16);
        				mini.get("cdxe17").setValue(a[0].cdxe17);
        				mini.get("cdxe21").setValue(a[0].cdxe21);
        				mini.get("cdxe23").setValue(a[0].cdxe23);
        				mini.get("cdxe24").setValue(a[0].cdxe24);
        				mini.get("cdxe25").setValue(a[0].cdxe25);
        				mini.get("cdxe26").setValue(a[0].cdxe26);
        				mini.get("cdxe27").setValue(a[0].cdxe27);
        				mini.get("cdxe31").setValue(a[0].cdxe31);
        				mini.get("cdxe33").setValue(a[0].cdxe33);
        				mini.get("cdxe34").setValue(a[0].cdxe34);
    			}
    		
    		});
    	}
    	
    	function clear(){
            var form=new mini.Form("search_form");
            form.clear();
            mini.get("oprDate").setValue("<%=__bizDate%>");
            search(10, 0);
    	}
    	
    	//导出
    	function exportExcel(){
    		/* var content = grid.getData();
    		if(content.length == 0){
    			mini.alert("请先查询数据");
    			return;
    		} */
    		mini.confirm("您确认要导出Excel吗?","系统提示", 
    			function (action) {
    				if (action == "ok"){
    			 		var form = new mini.Form("#search_form");
    					var data = form.getData(true);
    					data["reportDetail"] = mini.get("cdxe11").getValue()+","+mini.get("cdxe13").getValue()+","+mini.get("cdxe14").getValue()+","
		    					+mini.get("cdxe15").getValue()+","+mini.get("cdxe16").getValue()+","+mini.get("cdxe17").getValue()+","
		    					+mini.get("cdxe21").getValue()+","+mini.get("cdxe23").getValue()+","+mini.get("cdxe24").getValue()+","
		    					+mini.get("cdxe25").getValue()+","+mini.get("cdxe26").getValue()+","+mini.get("cdxe27").getValue()+","
		    					+mini.get("cdxe31").getValue()+","+mini.get("cdxe33").getValue()+","+mini.get("cdxe34").getValue()+",";
    					var fields = null;
    					for(var id in data){
    						fields += '<input type="hidden" id="' + id + '" name="'+ id +'" value="' + data[id] + '">';
    					}
    					var urls = CommonUtil.pPath + "/sl/IfsExportController/exportTyCdxeExcel";                                                                                                                         
    					$('<form action="'+ urls +'" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();   
    				}
    			}
    		);
    	}


        //英文、数字、下划线 的验证
        function onEnglishAndNumberValidation(e) {
            if(e.value == "" || e.value == null){//值为空，就不做校验
                return;
            }
            if (e.isValid) {
                if (isEnglishAndNumber(e.value) == false) {
                    e.errorText = "必须输入英文或数字";
                    e.isValid = false;
                }
            }
        }

        /* 是否英文、数字、下划线 */
        function isEnglishAndNumber(v) {
            var re = new RegExp("^[0-9a-zA-Z\_\-]+$");
            if (re.test(v)) return true;
            return false;
        }

    </script>
</body>
</html>