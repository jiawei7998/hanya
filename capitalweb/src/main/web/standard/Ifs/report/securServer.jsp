<%@ page import="com.singlee.capital.common.util.DateUtil" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>
<body style="width: 100%; height: 100%; background: white">
<fieldset class="mini-fieldset">
	<legend>查询条件</legend>
	<div id="search_form" style="width: 70%;">
	    <input id="br" name="br" class="mini-combobox mini-mustFill" width="320px" labelField="true" label="部门：" vtype="maxLength:5" required="true"
	  emptyText="请填写部门" labelStyle="text-align:right;"  value='<%=__sessionUser.getOpicsBr()%>' data="CommonUtil.serverData.dictionary.opicsBr"/>
		<!-- <input id="br" name="br" class="mini-textbox" width="320px" labelField="true" label="部门："
		emptyText="请填写部门" labelStyle="text-align:right;"  value='<%=__sessionUser.getOpicsBr()%>' enabled="false"/>  -->
		<input id="startDate" name="startDate" class="mini-datepicker" labelField="true" label="账务日期："
			   ondrawdate="onDrawDateStart" labelStyle="text-align:right;" emptyText="起始日期" format="yyyy-MM-dd" value="<%=__bizDate%>"/>
		<span>~</span>
		<input id="endDate" name="endDate" class="mini-datepicker"
			   ondrawdate="onDrawDateEnd" emptyText="结束日期" format="yyyy-MM-dd" value="<%=DateUtil.dateAdjust(__bizDate,7)%>"/>
		</nobr>
		<span style="float: right; margin-right: 50px">
			<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
			<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
		</span>
	</div>
</fieldset>
	
<div id="MiniSettleForeignSpot" class="mini-fit" style="margin-top: 2px;">
	<div id="datagrid" class="mini-datagrid borderAll" style="width: 100%; height: 100%;" allowAlternating="true"
		allowResize="true" border="true" sortMode="client" multiSelect="true" onrowdblclick="onRowDblClick" >
		<div property="columns">
			<div type="indexcolumn" headerAlign="center" width="40">序号</div>
			<div field="br" width="60" allowSort="false" headerAlign="center" align="center">部门</div>
			<div field="brprcindte" width="100" allowSort="false" headerAlign="center" align="center" renderer="onDateRenderer">账务日期</div>
			<div field="status" width="60" align="center" headerAlign="center" >交易类型</div>
			<div field="product" width="80" align="center" headerAlign="center" >产品</div>
			<div field="prodtype" width="80" allowSort="true" headerAlign="center" align="center">产品类型</div>
			<div field="dealno" width="80" allowSort="false" headerAlign="center" align="center">交易流水</div>
			<div field="secid" width="80" allowSort="false" headerAlign="center" align="center">债券代码</div>
			<div field="ccy" width="80" allowSort="false" headerAlign="center" align="center">币种</div>
			<div field="cno" width="100" allowSort="false" headerAlign="center" align="center">交易对手编号</div>
			<div field="prinamt" width="140" allowSort="false" headerAlign="center" align="right" numberFormat="#,0.0000">原始本金</div>
			<div field="proceedamt" width="140" allowSort="true" headerAlign="center" align="right" numberFormat="#,0.0000">收益金额</div>
			<div field="purchintamt" width="140" allowSort="false" headerAlign="center" align="right" numberFormat="#,0.0000">利息金额</div>
			<div field="settdate" width="100" allowSort="false" headerAlign="center" align="center" renderer="onDateRenderer">结算日期</div>
			<div field="verind" width="60" allowSort="false" headerAlign="center" align="center" renderer="onFlagRenderer">复核标识</div>
			<div field="verdate" width="80" allowSort="false" headerAlign="center" align="center" renderer="onDateRenderer">复核时间</div>
			<div field="voper" width="60" allowSort="false" headerAlign="center" align="center">复核人</div>
		</div>
	</div>
</div>

<script>
	mini.parse();

	var url = window.location.search;
	var grid = mini.get("datagrid");
	var userId='<%=__sessionUser.getUserId()%>';
	
	grid.on("beforeload", function(e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex;
		var pageSize = e.data.pageSize;
		search(pageSize, pageIndex);
	});
	//交易日期
	function onDrawDateStart(e) {
		var startDate = e.date;
		var endDate= mini.get("endDate").getValue();
		if(CommonUtil.isNull(endDate)){
			return;
		}
		if (endDate.getTime() < startDate.getTime()) {
			e.allowSelect = false;
		}
	}

	function onDrawDateEnd(e) {
		var endDate = e.date;
		var startDate = mini.get("startDate").getValue();
		if(CommonUtil.isNull(startDate)){
			return;
		}
		if (endDate.getTime() < startDate.getTime()) {
			e.allowSelect = false;
		}
	}

	function onDateRenderer(e) {
		if(e.value!=null){
			var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)==null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g,'/'));
	        if (value) return mini.formatDate(value, 'yyyy-MM-dd');
		}
    }
	function onFlagRenderer(e) {
        if (e.value == 1) return "已复核";
        else return "未复核";
    }
	
	// 查询
	function search(pageSize,pageIndex){
		var form = new mini.Form("#search_form");
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统也提示");
			return;
		}
		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		var params = mini.encode(data);
		CommonUtil.ajax({
			//url:"/IfsNettingController/querySecurServerList",
			url:"/SendOtcController/sendOtcChois",
			data:params,
			callback : function(data) {
				if(data){
					grid.setTotalCount(data.obj.total);
					grid.setPageIndex(pageIndex);
			        grid.setPageSize(pageSize);
					grid.setData(data.obj.rows);
				}
			}
		});
	}
		
	function query() {
    	search(grid.pageSize, 0);
    }
	
	function clear(){
        var form=new mini.Form("search_form");
        form.clear();
        mini.get("br").setValue(opicsBr);
        search(10,0);
	}
	
	$(document).ready(function() {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
			search(10, 0);
		});
	});
	
	//查看详情
	function onRowDblClick(e) {
		var row = grid.getSelected();
	 	//var aPPROVAL_DATE = new Date(row.aPPROVAL_DATE);
	 	//row.aPPROVAL_DATE = aPPROVAL_DATE;
        if(row){
        	var url = CommonUtil.baseWebPath() +"../../Ifs/report/securServerEdit.jsp?action=detail";
    		var tab = {id: "SecurServerEdit",name:"SecurServerEdit",url:url,title:"还本付息维护",
    					parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
    		var paramData = {selectData:grid.getSelected()};
    		CommonUtil.openNewMenuTab(tab,paramData);
        }
	}
    
    

	
</script>
</body>
</html>