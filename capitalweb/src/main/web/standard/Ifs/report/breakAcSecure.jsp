<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../../global.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>债券违约处理</title>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>
<body style="width: 100%; height: 100%; background: white">
<fieldset class="mini-fieldset">
    <legend>查询条件</legend>
    <div id="search_form" style="width: 70%;">
        <input id="secid" name="secid" class="mini-textbox" labelField="true" label="债券号："
               labelStyle="text-align:right;"/>
        <span style="float: right; margin-right: 50px">
			<a id="search_btn" class="mini-button" onclick="query()">查询</a>
			<a id="clear_btn" class="mini-button" onclick="clear()">清空</a>
		</span>
    </div>
    <span style="margin: 2px; display: block;">
		<a id="add_btn" class="mini-button" style="display: none" onclick="add()">新增</a>
<%--		<a id="edit_btn" class="mini-button" style="display: none"   onclick="edit()">修改</a>--%>
		<a id="delete_btn" class="mini-button" style="display: none" onclick="del()">删除</a>
	</span>
</fieldset>

<div id="MiniSettleForeignSpot" class="mini-fit" style="margin-top: 2px;">
    <div id="datagrid" class="mini-datagrid borderAll" style="width: 100%; height: 100%;" allowAlternating="true"
         allowResize="true" border="true" sortMode="client" multiSelect="true" onrowdblclick="onRowDblClick">
        <div property="columns">
            <div type="indexcolumn" headerAlign="center" width="40">序号</div>
            <div field="secid" width="200px" align="center" headerAlign="center">债券号</div>
            <div field="br" width="150px" align="center" headerAlign="center">机构</div>
            <div field="cost" width="180px" align="center" headerAlign="center">成本中心</div>
            <div field="ccy" width="120" align="center" headerAlign="center" renderer="CommonUtil.dictRenderer"
                 data-options="{dict:'Currency'}">币种
            </div>
            <div field="invtype" width="100px" allowSort="false" headerAlign="center" align="center">业务类型</div>
            <div field="port" width="150px" align="center" headerAlign="center">交易投组代码</div>
            <div field="prinamt" width="120px" align="center" headerAlign="center" allowSort="true">持仓</div>
            <div field="qty" width="150px" align="center" headerAlign="center" allowSort="true">QTY</div>
            <div field="status" width="120px" align="center" headerAlign="center" allowSort="true">违约标识</div>
            <div field="wydate" width="120px" align="center" headerAlign="center" allowSort="true"
                 renderer="onDateRenderer">违约日期
            </div>
            <div field="amont" width="300px" align="center" headerAlign="center" allowSort="true" numberFormat='n4'>
                违约本金
            </div>
            <div field="int" width="200px" align="center" headerAlign="center" allowSort="true" numberFormat='n4'>利息
            </div>
            <div field="pint" width="200px" align="right" headerAlign="center" allowSort="true" numberFormat='n4'>罚息
            </div>
            <div field="flag" width="110px" align="right" headerAlign="center" allowSort="true">备注</div>
            <div field="flag2" width="120" allowSort="false" headerAlign="center" align="center">备注2</div>
            <div field="inputdate" width="120" allowSort="false" headerAlign="center" align="center"
                 renderer="onDateRenderer">录入日期
            </div>
        </div>
    </div>
</div>

<script>
    mini.parse();

    var url = window.location.search;
    var grid = mini.get("datagrid");
    var userId = '<%=__sessionUser.getUserId()%>';

    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize, pageIndex);
    });

    //成交日期
    function onDrawDateStart(e) {
        var startDate = e.date;
        var endDate = mini.get("endDate").getValue();
        if (CommonUtil.isNull(endDate)) {
            return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }

    function onDrawDateEnd(e) {
        var endDate = e.date;
        var startDate = mini.get("startDate").getValue();
        if (CommonUtil.isNull(startDate)) {
            return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }

    function onDateRenderer(e) {
        var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value) == null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g, '/'));
        if (value) return mini.formatDate(value, 'yyyy-MM-dd');
    }

    // 查询
    function search(pageSize, pageIndex) {
        var form = new mini.Form("#search_form");
        form.validate();
        if (form.isValid() == false) {
            mini.alert("信息填写有误，请重新填写", "系统也提示");
            return;
        }
        var data = form.getData(true);

        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        data['branchId'] = branchId;
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: "/SlFiDefaultServiceController/searchPageSlFi",
            data: params,
            callback: function (data) {
                if (data) {
                    grid.setTotalCount(data.obj.total);
                    grid.setPageIndex(pageIndex);
                    grid.setPageSize(pageSize);
                    grid.setData(data.obj.rows);
                }
            }
        });
    }

    //添加
    function add() {
        var url = CommonUtil.baseWebPath() + "/report/breakAcSecureEdit.jsp?action=add";
        var tab = {
            id: "BAAdd",
            name: "BAAdd",
            url: url,
            title: "违约债券信息添加",
            parentId: top["win"].tabs.getActiveTab().name,
            showCloseButton: true
        };
        var paramData = {selectData: ""};
        CommonUtil.openNewMenuTab(tab, paramData);
    }

    //修改
    function edit() {
        var row = grid.getSelected();

        var wydate = new Date(row.wydate);
        row.wydate = wydate;
        var inputdate = new Date(row.inputdate);
        row.inputdate = inputdate;
        if (row) {
            var url = CommonUtil.baseWebPath() + "/report/breakAcSecureEdit.jsp?action=edit";
            var tab = {
                id: "BAEdit",
                name: "BAEdit",
                url: url,
                title: "违约债券信息修改",
                parentId: top["win"].tabs.getActiveTab().name,
                showCloseButton: true
            };
            var paramData = {selectData: row};
            CommonUtil.openNewMenuTab(tab, paramData);
        } else {
            mini.alert("请选择一条记录", "提示");
        }
    }

    //查看详情
    function onRowDblClick(e) {
        var row = grid.getSelected();

        var wydate = new Date(row.wydate);
        row.wydate = wydate;
        var inputdate = new Date(row.inputdate);
        row.inputdate = inputdate;
        var url = CommonUtil.baseWebPath() + "/report/breakAcSecureEdit.jsp?action=detail";
        var tab = {
            id: "BADetail",
            name: "BADetail",
            url: url,
            title: "违约债券信息详情",
            parentId: top["win"].tabs.getActiveTab().name,
            showCloseButton: true
        };
        var paramData = {selectData: row};
        CommonUtil.openNewMenuTab(tab, paramData);
    }

    function query() {
        search(grid.pageSize, 0);
    }

    function clear() {
        var form = new mini.Form("search_form");
        form.clear();
        search(10, 0);
    }

    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
            search(10, 0);
        });
    });


    //删除
    function del() {
        var rows = grid.getSelecteds();
        if (rows.length == 0) {
            mini.alert("请选中一行", "提示");
            return;
        }
        mini.confirm("您确认要删除吗?", "系统警告", function (value) {
            if (value == 'ok') {
                var data = rows[0];
                params = mini.encode(data);
                CommonUtil.ajax({
                    url: "/SlFiDefaultServiceController/deleteSetm",
                    data: params,
                    callback: function (data) {
                        mini.alert("删除成功.", "系统提示");
                        search(10, 0);
                    }
                });
            }
        });
    }


</script>
</body>
</html>