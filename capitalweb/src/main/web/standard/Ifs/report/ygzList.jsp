<%--
  Created by IntelliJ IDEA.
  User: hspcadmin
  Date: 2021/10/27
  Time: 13:45
  To change this template use File | Settings | File Templates.
--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>营改增</title>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/excelExport.js"></script>
</head>
<body style="width: 100%; height: 100%; background: white">
<fieldset class="mini-fieldset">
    <legend>查询条件</legend>
    <div id="search_form" style="width: 100%;">
    <input id="dealno" name="dealno" class="mini-textbox" width="320px" labelField="true" label="交易号："
           labelStyle="text-align:right;" emptyText="请输入交易号" />
    <input id="product" name="product" class="mini-combobox" data="[{'id':'可供出售','text':'可供出售'},{'id':'持有至到期','text':'持有至到期'},{'id':'交易性','text':'交易性'}]" valueField="id" textField="text" width="320px" emptyText="请选择三分类" labelField="true"  label="三分类：" labelStyle="text-align:right;"/>
    <input id="sectype" name="sectype" class="mini-buttonedit" onbuttonclick="onActyQuery" labelField="true"  label="债券类型：" width="320px"
           labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请输入债券类型" />
    <input id="seccode" name="seccode" class="mini-textbox" labelField="true"  label="债券代码：" width="320px"
           labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请输入债券代码" />
    <input id="secname" name="secname" class="mini-textbox" labelField="true"  label="债券名称：" width="320px"
           labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请输入债券名称" />
    <input id="vstartDate" name="vstartDate" class="mini-datepicker" labelField="true"  label="起息日："
           format="yyyy-MM-dd"  labelStyle="text-align:right;" emptyText="开始日期" />
    <span>~</span>
    <input id="vendDate" name="vendDate" class="mini-datepicker"
           emptyText="结束日期" format="yyyy-MM-dd"/>

    <input id="mstartDate" name="mstartDate" class="mini-datepicker" labelField="true"  label="到期日："
           format="yyyy-MM-dd"   labelStyle="text-align:right;" emptyText="开始日期" />
    <span>~</span>
    <input id="mendDate" name="mendDate" class="mini-datepicker"
           emptyText="结束日期" format="yyyy-MM-dd"/>

    <input id="istartDate" name="istartDate" class="mini-datepicker" labelField="true"  label="生成日期："
           format="yyyy-MM-dd"  labelStyle="text-align:right;" emptyText="开始日期" />
    <span>~</span>
    <input id="iendDate" name="iendDate" class="mini-datepicker"
           emptyText="结束日期" format="yyyy-MM-dd"/>
    <span style="float: right; margin-right: 50px">
			<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
			<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
			<a id="expt_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出Excel</a>
		</span>
</div>
    <span style="margin:2px;display: block;">
			<a id="add_btn" class="mini-button" style="display: none"   onclick="add()">新增</a>
            <a id="edit_btn" class="mini-button" style="display: none"   onclick="edit()">修改</a>
            <a id="delete_btn" class="mini-button"  style="display: none" onclick="del()">删除</a>
		</span>
</fieldset>

<div class="mini-fit" style="margin-top: 2px;">
    <div id="datagrid" class="mini-datagrid borderAll" style="width: 100%; height: 100%;" allowAlternating="true"
         allowResize="true" border="true" sortMode="client" multiSelect="true"sizeList="[10,20,50,100,-1]">
        <div property="columns">
            <div type="indexcolumn" headerAlign="center" width="40">序号</div>
            <div field="dealno" width="120" align="center" headerAlign="center" >交易号</div>
            <div field="invtye" width="120" align="center" headerAlign="center" >三分类</div>
            <div field="sectype" width="120" align="center" headerAlign="center" >债券类型</div>
            <div field="seccode" width="120" align="center" headerAlign="center" >债券代码</div>
            <div field="secname" width="120" align="center" headerAlign="center" >债券名称</div>
            <div field="term" width="120" align="center" headerAlign="center" >期限</div>
            <div field="vdate" width="120" align="center" headerAlign="center" >起息日</div>
            <div field="mdate" width="120" align="center" headerAlign="center" >到期日</div>
            <div field="rate" width="120" align="center" headerAlign="center" >固定利率</div>
            <div field="frate" width="120" align="center" headerAlign="center" >浮动利率</div>
            <div field="intpayrule" width="120" align="center" headerAlign="center" >付息频率</div>
            <div field="saeldate" width="120" align="center" headerAlign="center" >卖出日期</div>
            <div field="samount" width="120" align="center" headerAlign="center" numberFormat="#,0.00">卖出价值</div>
            <div field="sellprice" width="120" align="center" headerAlign="center" numberFormat="#,0.00">卖出价</div>
            <div field="buyavgamt" width="120" align="center" headerAlign="center" numberFormat="#,0.00">买入价</div>
            <div field="diffinprice" width="120" align="right" headerAlign="center" numberFormat="#,0.00">价差收入</div>
            <div field="inputdate" width="120" align="center" headerAlign="center" >生成日期</div>
        </div>
    </div>
</div>

<script>
    mini.parse();

    var url = window.location.search;
    var grid = mini.get("datagrid");
    var userId='<%=__sessionUser.getUserId()%>';

    grid.on("beforeload", function(e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize, pageIndex);
    });

    // 查询
    function search(pageSize,pageIndex){
        var form = new mini.Form("#search_form");
        form.validate();
        if(form.isValid()==false){
            mini.alert("信息填写有误，请重新填写","系统也提示");
            return;
        }
        var data=form.getData(true);
        data['pageNumber']=pageIndex+1;
        data['pageSize']=pageSize;
        data['branchId']=branchId;
        var params = mini.encode(data);
        CommonUtil.ajax({
            url:"/KYgzController/getPageList",
            data:params,
            callback : function(data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }

    function query() {
        search(grid.pageSize, 0);
    }

    function clear(){
        var form=new mini.Form("search_form");
        form.clear();
        search(10,0);
    }

    $(document).ready(function() {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
            search(10, 0);
        });
    });

    //添加
    function add() {
        var url = CommonUtil.baseWebPath() + "/../hrbReport/YgzEdit.jsp";
        var tab = { id: "YgzAdd", name: "YgzAdd", title: "营改增添加", url: url ,parentId:top["win"].tabs.getActiveTab().name,action:"add",showCloseButton:true};
        CommonUtil.openNewMenuTab(tab);
    }
    //修改
    function edit() {
        var row=grid.getSelecteds();
        if(row.length<1){
            mini.alert("请选择要修改的数据!")
            return;
        }
        var url = CommonUtil.baseWebPath() + "/../hrbReport/YgzEdit.jsp";
        var tab = { id: "YgzUpdate", name: "YgzUpdate", title: "营改增修改", url: url ,parentId:top["win"].tabs.getActiveTab().name,action:"edit",showCloseButton:true};
        var paramData = {selectData:row};
        CommonUtil.openNewMenuTab(tab,paramData);
    }
    //删除
    function del() {
        var rows=grid.getSelecteds();
        if(!rows.length>0){
            mini.alert("请选择要删除的数据!");
        }
        CommonUtil.ajax({
            url:"/KYgzController/delete",
            data:rows[0].id,
            callback : function(data) {
                mini.alert("删除成功!");
                query();
            }
        });
    }

    function onActyQuery(e) {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Ifs/opics/actyMini.jsp",
            title: "选择会计类型",
            width: 900,
            height: 600,
            onload: function () {
                var iframe = this.getIFrameEl();
                var data = {type1: "2", type2: "2"};//把会计归属类型传到小页面，"2"表示归属债券类，"3"表示归属客户及债券类
                iframe.contentWindow.SetData(data);
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        debugger;
                        btnEdit.setValue(data.acctdesc);
                        btnEdit.setText(data.acctdesc);
                        btnEdit.focus();
                    }
                }

            }
        });
    }

    //导出
    function exportExcel(){
        var data=grid.getData();
        console.log(data);
        if(data.totalCount=="0"){
            mini.alert("表中无数据");
            return;
        }
        //前台直接导出
        downloadExcel(ExportTable(grid.getData(true),grid.columns),"营改增数据");
    }
</script>
</body>
</html>
