<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>同业存单发行每日余额清单</title>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>
<body style="width: 100%; height: 100%; background: white">
<fieldset class="mini-fieldset">
	<legend>查询条件</legend>
	<div id="search_form" style="width: 100%;">
        <input id="secid" name="secid" class="mini-textbox" labelField="true"  label="存单代码：" width="280px" 
        	labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请输入存单代码" />
        <input id="datadate" name="datadate" class="mini-datepicker" labelField="true" label="数据日期："
         	labelStyle="text-align:right;" emptyText="请选择日期" />
        <span style="float: right; margin-right: 50px"> 
			<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
			<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
			<a id="exportExcel" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
		</span>
	</div>
</fieldset>
<div class="mini-fit" style="margin-top: 2px;">
	<div id="datagrid" class="mini-datagrid borderAll" style="width: 100%; height: 100%;" allowAlternating="true"
		allowResize="true" border="true" sortMode="client" multiSelect="true">
		<div property="columns">
			<div type="indexcolumn" headerAlign="center" width="40">序号</div>
			<div field="datadate" width="120" align="center" headerAlign="center" renderer="onDateRenderer">数据日期</div>
			<div field="inst" width="100" align="center" headerAlign="center" >机构号</div>
			<div field="secid" width="120" align="center" headerAlign="center" >存单代码</div>
			<div field="secidnm" width="180" align="center" headerAlign="center" >存单名称</div>
			<div field="subname" width="180"  headerAlign="center" align="center">认购人名称</div>
			<div field="insttype" width="120"   headerAlign="center" align="center">所属账户分类</div>
			<div field="subjectno" width="120"   headerAlign="center" align="center">核心科目号</div>
			<div field="prodtype" width="120"   headerAlign="center" align="center">产品性质</div>
			<div field="product"  width="120"  headerAlign="center" align="center">产品分类</div>
			<div field="bookbalance" width="120" headerAlign="center" align="right" numberFormat="#,0.0000">账面余额</div>
			<div field="vdate" width="120"  headerAlign="center" align="center" renderer="onDateRenderer">起息日期</div>
			<div field="mdate" width="120"  headerAlign="center" align="center" renderer="onDateRenderer">到期日期</div>
			<div field="facevalue" width="120"  headerAlign="center" align="right" >面值</div>
			<div field="interestadj" width="120"  headerAlign="center" align="right" numberFormat="#,0.0000">利息调整</div>	
		</div>
	</div>
</div>

<script>
	mini.parse();
	var url = window.location.search;
	var grid = mini.get("datagrid");
	var userId='<%=__sessionUser.getUserId()%>';
	
	grid.on("beforeload", function(e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex;
		var pageSize = e.data.pageSize;
		search(pageSize, pageIndex);
	});
	
	function onDateRenderer(e) {
		if(e.value!=null){
			var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)==null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g,'/'));
	        if (value) return mini.formatDate(value, 'yyyy-MM-dd');
		}
    }
	
	// 查询
	function search(pageSize,pageIndex){
		var form = new mini.Form("#search_form");
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统也提示");
			return;
		}
		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:"/IfsReportController/searchCdIssueDayBalanceList",
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
		
	function query() {
    	search(grid.pageSize, 0);
    }
	
	function clear(){
        var form=new mini.Form("search_form");
        form.clear();
        search(10,0);
	}
	
	$(document).ready(function() {
		search(10, 0);
	});
	
	// 导出
	function exportExcel(){
		var content = grid.getData();
		if(content.length == 0){
			mini.alert("请先查询数据");
			return;
		}
		mini.confirm("您确认要导出Excel吗?","系统提示", 
			function (action) {
				if (action == "ok"){
			 		var form = new mini.Form("#search_form");
					var data = form.getData(true);
					var fields = null;
					for(var id in data){
						fields += '<input type="hidden" id="' + id + '" name="'+ id +'" value="' + data[id] + '">';
					}
					var urls = CommonUtil.pPath + "/sl/IfsExportController/exportCdIssueDayBalance";                                                                                                                         
					$('<form action="'+ urls +'" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();   
				}
			}
		);
	}
</script>
</body>
</html>