<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
	<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
	<title>外币限额报表</title>
</head>

<body style="width: 100%; height: 100%; background: white">
	<div class="mini-panel" title="查询条件" style="width:100%;"  allowResize="true" collapseOnTitleClick="true">
		<div id="search_form" style="width: 100%;padding-top:10px;">
			<input id="oprDate" name="oprDate" class="mini-datepicker" labelField="true" label="限额日期：" labelStyle="text-align:right;" emptyText="请输入限额日期" value="<%=__bizDate%>" />
			<span style="float: right; margin-right: 150px"> 
				<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
				<a id="export_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
			</span>
		</div>
	</div>
	<div id="grid" class="mini-datagrid borderAll" style="width:100%;height:80%;" idField="id" multiSelect="true"
		allowAlternating="true" allowResize="true" sortMode="client" allowAlternating="true">
		<div property="columns">
			<div type="indexcolumn" width="50px" headerAlign="center">序号</div>
			<div field="foreignLimitType" width="100" allowSort="true" headerAlign="center"  align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'foreignLimitType'}">限额类型</div>
			<div field="usd" width="100" align="right" headerAlign="center" allowSort="true" numberFormat="n2" >USD</div>
			<div field="eur" width="100" align="right" headerAlign="center" allowSort="true" numberFormat="n2" >EUR</div>
			<div field="jpy" width="100" align="right" headerAlign="center" allowSort="true" numberFormat="n2" >JPY</div>
			<div field="hkd" width="100" align="right" headerAlign="center" allowSort="true" numberFormat="n2" >HKD</div>
			<div field="gbp" width="100" align="right" headerAlign="center" allowSort="true" numberFormat="n2" >GBP</div>
			<div field="total" width="100" align="right" headerAlign="center" allowSort="true" numberFormat="n2" >总计</div>
		</div>
	</div>
<script>
	mini.parse();
	//获取当前tab
	var grid = mini.get("grid");
	grid.on("beforeload", function(e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex;
		var pageSize = e.data.pageSize;
		search(pageSize, pageIndex);
	});
	
	$(document).ready(function() {
		search(10, 0);
	});

	// 初始化数据
	function query(){
		search(grid.pageSize, 0);
	}
	
	function search(pageSize,pageIndex){
		var form = new mini.Form("#search_form");
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统提示");
			return;
		}
		var data=form.getData();
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		var url="/IfsLimitController/getForeignLimit";
		
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:url,
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	
	function clear(){
        var form=new mini.Form("search_form");
        form.clear();
        mini.get("oprDate").setValue("<%=__bizDate%>");
        search(10, 0);
	}
	
	//导出
	function exportExcel(){
		var content = grid.getData();
		if(content.length == 0){
			mini.alert("请先查询数据");
			return;
		}
		mini.confirm("您确认要导出Excel吗?","系统提示", 
			function (action) {
				if (action == "ok"){
			 		var form = new mini.Form("#search_form");
					var data = form.getData(true);
					var fields = null;
					for(var id in data){
						fields += '<input type="hidden" id="' + id + '" name="'+ id +'" value="' + data[id] + '">';
					}
					var urls = CommonUtil.pPath + "/sl/IfsExportController/exportWbxeExcel";                                                                                                                         
					$('<form action="'+ urls +'" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();   
				}
			}
		);
	}
</script>
</body>
</html>