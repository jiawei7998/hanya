<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>债券持仓盈亏统计表</title>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>
<body style="width: 100%; height: 100%; background: white">
<%-- <fieldset class="mini-fieldset">
	<legend>查询条件</legend>
	<div id="search_form" style="width: 70%;">
		<input id="dataDate" name="dataDate" class="mini-datepicker" labelField="true" label="数据日期："
         	labelStyle="text-align:right;" emptyText="请选择日期" value="<%=__bizDate%>"/>
	</div>
	<span style="float: right; margin-right: 50px"> 
			<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
			<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
	</span>
</fieldset> --%>

<fieldset class="mini-fieldset">
	<legend></legend>
	<div id="search_form" style="width: 100%;">
       <span style="float: left; margin-right: 50px">
			<a id="export_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
		</span>
	</div>
</fieldset>
	
<div class="mini-fit" style="margin-top: 2px;margin-bottom: 2px;">
	<div id="datagrid" class="mini-datagrid borderAll" style="width: 100%; height: 100%;" allowAlternating="true"
		allowResize="true" border="true" sortMode="client" multiSelect="true" showPager="false" >
		<div property="columns">
			<div type="indexcolumn" headerAlign="center" width="40">序号</div>
			<div field="invtype" name="invtype" width="120" align="center" headerAlign="center" renderer="onRenderer2">会计分类</div>
            <div field="acctngtype" name="acctngtype" width="120" align="center" headeralign="center" renderer="onRenderer">债券分类</div>
            <div header="名义本金(亿)" headerAlign="center" >
                <div property="columns">
                	<div field="prinamt" width="120" align="right" headerAlign="center" numberFormat="#,0.0000" >当前</div>
                	<div field="yearprinamt" width="120" align="right" headerAlign="center" numberFormat="#,0.0000" >年初</div>
                </div>
            </div>	
            <div header="账面价值(亿)" headerAlign="center" >
                <div property="columns">
                	<div field="tdbookvalue" width="120" align="right" headerAlign="center" numberFormat="#,0.0000" >当前</div>
                	<div field="yearbookvalue" width="120" align="right" headerAlign="center" numberFormat="#,0.0000" >年初</div>
                </div>
            </div>	
            <div header="久期" headerAlign="center" >
                <div property="columns">
                	<div field="duration" width="120" align="right" headerAlign="center" numberFormat="#,0.0000">当前</div>
                	<div field="yearduation" width="120" align="right" headerAlign="center" numberFormat="#,0.0000" >年初</div>
                </div>
            </div>	
            <div header="持有到期收益率%" headerAlign="center" >
                <div property="columns">
                	<div field="rate" width="120" align="right" headerAlign="center" numberFormat="#,0.0000" >当前</div>
                	<div field="yearrate" width="120" align="right" headerAlign="center" numberFormat="#,0.0000" >年初</div>
                </div>
            </div>	
            <div header="估值损益" headerAlign="center" >
                <div property="columns">
                	<div field="diffamt" width="120" align="right" headerAlign="center" numberFormat="#,0.0000" >当前</div>
                	<div field="yeardiffamt" width="120" align="right" headerAlign="center" numberFormat="#,0.0000" >年初</div>
                </div>
            </div>	
            <div field="dayprofitloss" headeralign="center" align="right" numberFormat="#,0.0000" width="120">日损益</div>
            <div field="monthprofitloss" headeralign="center" align="right" numberFormat="#,0.0000" width="120">月损益</div>
            <div field="yearprofitloss" headeralign="center" align="right" numberFormat="#,0.0000" width="120">年损益</div>
		</div>
	</div>
</div>

<script>
	mini.parse();
	var url = window.location.search;
	var grid = mini.get("datagrid");
	var userId='<%=__sessionUser.getUserId()%>';
	
	grid.on("beforeload", function(e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex;
		var pageSize = e.data.pageSize;
		search(pageSize, pageIndex);
	});
	
	function onDateRenderer(e) {
		if(e.value!=null){
			var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)==null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g,'/'));
	        if (value) return mini.formatDate(value, 'yyyy-MM-dd');
		}
    }
	
	var acctngtypes=[{id:'GZ',text:'国债'},{id:'ZCXYH',text:'政策性银行债'},{id:'QTJRJG',text:'其他金融债'},{id:'DFZFZ',text:'地方政府债'},
	{id:'YP',text:'央票'},{id:'JRZ',text:'金融债'},{id:'DR',text:'短期融资券'},{id:'QYZ',text:'企业债'},{id:'GSZ',text:'公司债'},
	{id:'ZP',text:'中票'}];
	
	function onRenderer(e) {
		for (var i = 0, l = acctngtypes.length; i < l; i++) {
            var g = acctngtypes[i];
            if (g.id == e.value) return g.text;
        }
        return "小计";
    }
	
	function onRenderer2(e) {
		if(e.value==null){
			var value = "总计";
	        if (value) return value;
		}else{
			return e.value;
		}
    }
	
	// 查询
	function search(pageSize,pageIndex){
		var form = new mini.Form("#search_form");
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统也提示");
			return;
		}
		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:"/IfsReportController/searchBondProfitLossList",
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				/* grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize); */
				grid.setData(data.obj);
			}
		});
	}
		
	function query() {
    	search(grid.pageSize, 0);
    }
	
	function clear(){
        var form=new mini.Form("search_form");
        form.clear();
        search(10,0);
	}
	
	$(document).ready(function() {
		search(10, 0);
	});
	
	//导出
	function exportExcel(){
		var content = grid.getData();
		if(content.length == 0){
			mini.alert("请先查询数据");
			return;
		}
		mini.confirm("您确认要导出Excel吗?","系统提示", 
			function (action) {
				if (action == "ok"){
			 		var form = new mini.Form("#search_form");
					var data = form.getData(true);
					var fields = null;
					for(var id in data){
						fields += '<input type="hidden" id="' + id + '" name="'+ id +'" value="' + data[id] + '">';
					}
					var urls = CommonUtil.pPath + "/sl/IfsExportController/exportZqccykExcel";                                                                                                                         
					$('<form action="'+ urls +'" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();   
				}
			}
		);
	}
</script>
</body>
</html>