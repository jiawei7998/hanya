<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>

<body style="width: 100%; height: 100%; background: white">
<div class="mini-panel" title="查询条件" style="width:100%;"  allowResize="true" collapseOnTitleClick="true">
		<div id="search_form" style="width: 100%;padding-top:10px;">
			<input id="contractId" name="contractId" class="mini-textbox" labelField="true" label="成交单编号：" labelStyle="text-align:right;" emptyText="请填写成交单编号" width="280px" /> 
			<input id="approveStatus" name="approveStatus" class="mini-combobox" data="CommonUtil.serverData.dictionary.ApproveSta" width="280px" emptyText="请选择审批状态" labelField="true"  label="审批状态：" labelStyle="text-align:right;"/>
			<span style="float: right; margin-right: 150px"> 
				<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
			</span>
		</div>
	</div>
	<div id="grid1" class="mini-datagrid borderAll" style="width:100%;height:80%;" idField="id" multiSelect="true"
		allowAlternating="true" allowResize="true" sortMode="client" allowAlternating="true">
		<div property="columns">
			<div type="indexcolumn" width="50px" headerAlign="center">序号</div>
			<div field="ticketId" width="150px" align="center"  headerAlign="center" >业务编号</div>    
			<div field="contractId" width="150"  headerAlign="center" align="center" >成交单号</div>
			<div field="prod" width="150px" align="center"  headerAlign="center" >产品</div>
			<div field="prodType" width="150px" align="center"  headerAlign="center">产品类型</div>
			<div field="dealNo" width="100px" headerAlign="center" align="center" >交易号</div>
		</div>
	</div>
<script>
	mini.parse();
	//获取当前tab
	var grid = mini.get("grid1");
	grid.on("beforeload", function(e) {
			e.cancel = true;
			var pageIndex = e.data.pageIndex;
			var pageSize = e.data.pageSize;
			search(pageSize, pageIndex);
		});
	$(document).ready(function() {
		search(10, 0);
	});

	// 初始化数据
	function query(){
		search(grid.pageSize, 0);
	}
	function search(pageSize,pageIndex){
		var form = new mini.Form("#search_form");
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统提示");
			return;
		}
		var data=form.getData();
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		var url="/IfsDealDeskReportController/searchPageDealDesk";
		
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:url,
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	
	function clear(){
        var form=new mini.Form("search_form");
        form.clear();
	}
</script>
</body>
</html>