<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>
<body style="width: 100%; height: 100%; background: white">
	<fieldset class="mini-fieldset">
		<legend>查询条件</legend>
		<div id="search_form" style="width: 70%;">
			<input id="forDate" name="forDate" class="mini-datepicker" labelField="true"  label="成交日期：" value="<%=__bizDate%>" width="280px"  labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请输入成交日期" />
			<input id="contractId" name="contractId" class="mini-textbox" labelField="true" label="成交单编号：" labelStyle="text-align:right;" emptyText="请填写成交单编号" width="280px" /> 
			<input id="dealNo" name="dealNo" class="mini-textbox" labelField="true" label="opics单号：" labelStyle="text-align:right;" emptyText="请填写opics单号" width="280px" /> 
		</div>
		<span style="float: right; margin-right: 50px"> 
				<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
				<a id="export_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
		</span>
	</fieldset>
	
<div style="width: 100%;">
	<fieldset class="mini-fieldset">
		<legend>汇总</legend>
		<div id="search_form1" style="width: 100%;">
			<input id="ccycheckSum" name="ccycheckSum" class="mini-textbox" labelField="true" 
			label="CCY校验：" width="300px" 	labelStyle="text-align:right; width:120px" value = "0"/>
	        <input id="ctrcheckSum" name="ctrcheckSum" class="mini-textbox" labelField="true" width="300px"
	        label="CTR校验：" labelStyle="text-align:right; width:120px" value = "0"/> 
	       	<input id="buyAmountSum" name="buyAmountSum" class="mini-textbox" labelField="true" width="300px"
	       	label="本地CCY AMOUNT：" labelStyle="text-align:right; width:120px" value = "0"/> 
	       	<br></br>
	       	<input id="sellAmountSum" name="sellAmountSum" class="mini-textbox" labelField="true" width="300px"
	       	label="本地CTR AMOUNT：" labelStyle="text-align:right; width:120px" value = "0"/> 
	       	<input id="ccyamountSum" name="ccyamountSum" class="mini-textbox" labelField="true" width="300px"
	       	label="CCY AMOUNT：" labelStyle="text-align:right; width:120px" value = "0"/> 
	       	<input id="ctramountSum" name="ctramountSum" class="mini-textbox" labelField="true" width="300px"
	       	label="CTR AMOUNT：" labelStyle="text-align:right; width:120px" value = "0"/> 
		</div>
	</fieldset>
</div>
	
	
	
	<div class="mini-fit" style="margin-top: 2px;">
	<div id="grid1" class="mini-datagrid borderAll" style="width: 100%; height: 100%;" allowAlternating="true"
		allowResize="true" border="true" sortMode="client" multiSelect="true" >
		<div property="columns">
			<div type="indexcolumn" width="50px" headerAlign="center">序号</div>
			<div field="forDate" width="150"  headerAlign="center" align="center" >成交日期</div>
			<div field="product" width="150"  headerAlign="center" align="center" >业务类型</div>
			<div field="contractId" width="150"  headerAlign="center" align="center" >成交单号</div>
			<div field="dealNo" width="150px" align="center"  headerAlign="center" >opics单号</div>
			<div field="ccycheck" width="100px" align="right"  headerAlign="center">CCY校验</div>
			<div field="ctrcheck" width="100px" headerAlign="center" align="right" >CTR校验</div>
			<div field="buyAmount" width="150px" headerAlign="center" align="right" >本地CCY AMOUNT</div>
			<div field="sellAmount" width="150px" headerAlign="center" align="right" >本地CTR AMOUNT</div>
			<div field="ccyamount" width="150px" headerAlign="center" align="right" >CCY AMOUNT</div>
			<div field="ctramount" width="150px" headerAlign="center" align="right" >CTR AMOUNT</div>
		</div>
	</div>
</div>
	
<script>
	mini.parse();
	//获取当前tab
	var grid = mini.get("grid1");
	grid.on("beforeload", function(e) {
			e.cancel = true;
			var pageIndex = e.data.pageIndex;
			var pageSize = e.data.pageSize;
			search(pageSize, pageIndex);
		});
	$(document).ready(function() {
		search(10, 0);
	});

	// 初始化数据
	function query(){
		search(grid.pageSize, 0);
	}
	function search(pageSize,pageIndex){
		var form = new mini.Form("#search_form");
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统提示");
			return;
		}
		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		var url="/IfsReportController/searchSptCheck";
		
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:url,
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
		
		//计算合计
		CommonUtil.ajax({
			url:"/IfsReportController/searchSptCheckTotal",
			data:params,
			callback : function(data) {
				mini.get("ccycheckSum").setValue("0");
				mini.get("ctrcheckSum").setValue("0");
				mini.get("buyAmountSum").setValue("0");
				mini.get("sellAmountSum").setValue("0");
				mini.get("ccyamountSum").setValue("0");
				mini.get("ctramountSum").setValue("0");
				if(data!=null){
					mini.get("ccycheckSum").setValue(data.obj.ccycheck);
					mini.get("ctrcheckSum").setValue(data.obj.ctrcheck);
					mini.get("buyAmountSum").setValue(data.obj.buyAmount);
					mini.get("sellAmountSum").setValue(data.obj.sellAmount);
					mini.get("ccyamountSum").setValue(data.obj.ccyamount);
					mini.get("ctramountSum").setValue(data.obj.ctramount);
				}
			}
		});
	}
	
	function clear(){
        var form=new mini.Form("search_form");
        form.clear();
	}
	
	//导出
	function exportExcel(){
		var content = grid.getData();
		if(content.length == 0){
			mini.alert("请先查询数据");
			return;
		}
		mini.confirm("您确认要导出Excel吗?","系统提示", 
			function (action) {
				if (action == "ok"){
			 		var form = new mini.Form("#search_form");
					var data = form.getData(true);
					var fields = null;
					for(var id in data){
						fields += '<input type="hidden" id="' + id + '" name="'+ id +'" value="' + data[id] + '">';
					}
					var urls = CommonUtil.pPath + "/sl/IfsExportController/exportSptCheckExcel";                                                                                                                         
					$('<form action="'+ urls +'" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();   
				}
			}
		);
	}
</script>
</body>
</html>