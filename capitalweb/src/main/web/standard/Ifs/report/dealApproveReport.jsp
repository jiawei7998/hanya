<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>
<body style="width: 100%; height: 100%; background: white">
	<fieldset class="mini-fieldset">
		<legend>查询条件</legend>
		<div id="search_form" style="width: 70%;">
			<div id="search_form" style="width: 70%;">
			<input id="br" name="br" labelField="true" label="用户所属OPICS部门：" vtype="maxLength:5" style="width:200px" labelStyle="text-align:left;" class="mini-hidden  mini-combobox mini-mustFill"
						data="CommonUtil.serverData.dictionary.opicsBr" value='<%=__sessionUser.getOpicsBr()%>'  />
		<input id="oper" name="oper" class="mini-buttonedit" onbuttonclick="onButtonEdit"  style="width:300px"labelField="true" label="审批人：" labelStyle="text-align:right;" allowinput='false'emptyText="请选择审批人" />
            <nobr>
			<input id="startDate" name="startDate" class="mini-datepicker" labelField="true" label="起息日期："
						ondrawdate="onDrawDateStart"  onvaluechanged='dateCalculation' labelStyle="text-align:right;" emptyText="起始日期" format="yyyy-MM-dd" value="<%=__bizDate%>"/>
			<span>~</span>
			<input id="endDate" name="endDate" class="mini-datepicker" 
						ondrawdate="onDrawDateEnd"  onvaluechanged='dateCalculation'  emptyText="结束日期" format="yyyy-MM-dd" value="<%=__bizDate%>"/>
			</nobr>		
	</div> 
		</div>
		<span style="float: right; margin-right: 60px;margin-top:-30px"> 
				<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
				<a id="export_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
		</span>
	</fieldset>
	
	<div style="width: 100%;">
	<fieldset class="mini-fieldset">
		<legend>汇总</legend>
		<div id="form" style="width: 100%;">
			<input id="user" name="user" class="mini-textbox" labelField="true" 
			label="审批人：" width="300px" 	labelStyle="text-align:right; width:120px" readonly='true'/>
	        <input id="count" name="count" class="mini-textbox" labelField="true" width="300px"
	        label="审批交易笔数：" labelStyle="text-align:right; width:120px" readonly='true' /> 
		</div>
	</fieldset>
</div>

	<div class="mini-fit" style="margin-top: 2px;">
	<div id="grid1" class="mini-datagrid borderAll" style="width: 100%; height: 100%;" allowAlternating="true"
		allowResize="true" border="true" sortMode="client" multiSelect="true">
		<div property="columns">
			<div type="indexcolumn" width="50px" headerAlign="center">序号</div>
			<div field="ticketId" width="130" headerAlign="center" align="left" allowSort="true">审批单编号</div>
                    <div field="contractId" width="150" headerAlign="center" align="left" allowSort="true">成交单编号</div>
                    <div field="tradeDate" width="120" headerAlign="center" align="center" allowSort="true">交易日期</div>
                    <div field="vDate" width="80" allowSort="false" headerAlign="center" align="">起息日</div>
                    <div field="mydircn" width="80" headerAlign="center" align="center" allowSort="true">方向</div>                   
                    <div field="tccy" width="60" headerAlign="center" align="center" allowSort="true">交易货币</div>
                    <div field="pamt" width="120" headerAlign="center" align="left" allowSort="true"  numberFormat="#,0.00">金额1</div>
                    <div field="occy" width="60" headerAlign="center" align="center" allowSort="true">对应货币</div>
                    <div field="ramt" width="120" headerAlign="center" align="left" allowSort="true"  numberFormat="#,0.00">金额2</div>                    
                    <div field="taskName" width="80" allowSort="false" headerAlign="center" align="">审批状态</div>
                    <div field="statcode" width="90" headerAlign="center" align="left" allowSort="true" renderer="CommonUtil.dictRenderer" data-options="{dict:'statcode'}">opics处理状态</div>
                    <div field="dealno" width="120" headerAlign="center" align="left" allowSort="true">opics编号</div>
                    <div field="prdName" width="80" headerAlign="center" >产品名称</div>
                    <div field="sponsor" width="90" headerAlign="center" >审批发起人</div> 
                    <div field="approveList" width="650" headerAlign="center" >审批人列表</div> 
		</div>
	</div>
</div>
	
<script>
	mini.parse();
	//获取当前tab
	var grid = mini.get("grid1");
	grid.on("beforeload", function(e) {
			e.cancel = true;
			var pageIndex = e.data.pageIndex;
			var pageSize = e.data.pageSize;
			search(pageSize, pageIndex);
		});
	$(document).ready(function() {
		initDate();
		search(10, 0);
	});

	// 初始化数据
	function query(){
		search(grid.pageSize, 0);
	}
	function search(pageSize,pageIndex){
		var form = new mini.Form("#search_form");
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统提示");
			return;
		}
		dateCalculation();
		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
     	//data['br']=opicsBr;//自贸区改造加br
		data['userId']=userId;
		var url="/IfsFlowMonitorController/dealCount";
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:url,
			data:params,
			callback : function(data) {
				if(data){
					grid.setTotalCount(data.obj.total);
					grid.setPageIndex(pageIndex);
			        grid.setPageSize(pageSize);
					grid.setData(data.obj.rows);
					if(mini.get("oper").getValue()!=null&mini.get("oper").getValue()!=''){
					   mini.get("user").setValue(mini.get("oper").getText());
					   mini.get("count").setValue(data.obj.total);
					}
				}
			}
		});
	}
	//选择用户
	function onButtonEdit(e) {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Common/MiniUserManagesMini.jsp",
            title: "审核人员列表",
            width: 900,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.userId);
                        btnEdit.setText(data.userName);
                        btnEdit.focus();
                    }
                }

            }
        });
    }
	function clear(){
        var form=new mini.Form("search_form");
        form.clear();
        mini.get("user").setValue();
        mini.get("count").setValue();
        initDate();
        mini.get("br").setValue(opicsBr);//自贸区改造加br
        query();
	}
	function initDate(){
		var date=new Date(sysDate.replace(/-/g,'/'));
	    var year=date.getFullYear();
	    var month=date.getMonth()+1;
	    var start=new Array();
	    start.push(year);
	    start.push(month);
	    start.push("1");
	    mini.get("startDate").setValue(start.join("-"));
	    mini.get("endDate").setValue(getLastDay(year,month));
	}
	//获得每月最后一天
	 function getLastDay(year,month){         
         var new_year = year;    //取当前的年份
         var new_month = month++;//取下一个月的第一天，方便计算（最后一天不固定）
         if(month>12) {         
          new_month -=12;        //月份减
          new_year++;            //年份增
         }         
         var new_date = new Date(new_year,new_month,1);                //取当年当月中的第一天
         return (new Date(new_date.getTime()-1000*60*60*24));//获取当月最后一天日期
    } ;

	//日期
	function onDrawDateStart(e) {
        var startDate = e.date;
        var endDate= mini.get("endDate").getValue();
        if(CommonUtil.isNull(endDate)){
        	return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }
	
	function onDrawDateEnd(e) {
        var endDate = e.date;
        var startDate = mini.get("startDate").getValue();
        if(CommonUtil.isNull(startDate)){
        	return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }
	//计算时间段长度
	function dateCalculation(){
		var start = mini.get("startDate").getValue();
		var end= mini.get("endDate").getValue();
		if(CommonUtil.isNull(start)||CommonUtil.isNull(end)){
			return;
		}
		var subDate= Math.abs(parseInt((end.getTime() - start.getTime())/1000/3600/24));
		if(subDate>30){
			mini.alert('查询时间大于一个月，请重新选择');
			return;
		}
	}
	
	//导出
	function exportExcel(){
		var content = grid.getData();
		if(content.length == 0){
			mini.alert("请先查询数据");
			return;
		}
		mini.confirm("您确认要导出Excel吗?","系统提示", 
			function (action) {
				if (action == "ok"){
			 		var form = new mini.Form("#search_form");
					var data = form.getData(true);
					var fields = null;
					if(data){
						data['oper']=data.oper;
						data['opername']=mini.get("oper").getText();
				 		data['br']=opicsBr;//自贸区改造加br
						data['userId']=userId;
						data['pageNumber']=1;
						data['pageSize']=999999;
					}
					for(var id in data){
						fields += '<input type="hidden" id="' + id + '" name="'+ id +'" value="' + data[id] + '">';
					}
					var urls = CommonUtil.pPath + "/sl/IfsExportController/exportDealAppCount";                                                                                                                         
					$('<form action="'+ urls +'" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();   
				}
			}
		);
	}
</script>
</body>
</html>