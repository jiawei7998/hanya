<%--
  Created by IntelliJ IDEA.
  User: hspcadmin
  Date: 2021/10/27
  Time: 11:28
  To change this template use File | Settings | File Templates.
--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../../global.jsp" %>
<html>
<head>
    <title>DVP凭证打印</title>
    <script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset title">
    <legend>市场部大额凭证</legend>
    <div id="search_form" style="width: 100%;">
        <input id="fedealno" name="fedealno" class="mini-textbox" width="320px" labelField="true" label="成交单号："
               labelStyle="text-align:right;" emptyText="请输入成交单号"/>
        <input id="startDate" name="startDate" class="mini-datepicker" labelField="true" label="划款日期："
               ondrawdate="onDrawDateStart" format="yyyy-MM-dd" value="<%=__bizDate%>" labelStyle="text-align:right;"
               emptyText="开始日期"/>
        <span>~</span>
        <input id="endDate" name="endDate" class="mini-datepicker"
               ondrawdate="onDrawDateEnd" emptyText="结束日期" format="yyyy-MM-dd" value="<%=__bizDate%>"/>
        <input id="type" name="type" class="mini-combobox" data="CommonUtil.serverData.dictionary.busTypes"
               width="280px" emptyText="请选择业务类型" labelField="true" label="业务类型：" labelStyle="text-align:right;"/>
        <input id="cname" name="cname" class="mini-textbox" width="320px" labelField="true" label="交易对手："
               labelStyle="text-align:right;" emptyText="请输入交易对手"/>
        <span style="float:right;margin-right: 150px">
            <a class="mini-button" id="search_btn" onclick="search()">查询</a>
            <a class="mini-button" id="clear_btn" onclick="clear()">清空</a>
            <a class="mini-button" id="print_btn" onclick="print()">打印</a>
        </span>
    </div>
</fieldset>
<%--<%@ include file="../batchReback.jsp"%>--%>
<div class="mini-fit" style="width:100%;height:100%;">
    <div id="datagrid" class="mini-datagrid borderAll" style="height:100%;" idField="setno" allowResize="true"
         multiSelect="true" allowCellEdit="true" border="true" allowAlternating="true">
        <div property="columns">
            <div type="indexcolumn" headerAlign="center" align="center" width="40px">序列</div>
            <div field="type" headerAlign="center" align="center" width="120px" renderer="CommonUtil.dictRenderer"
                 data-options="{dict:'busTypes'}">业务类型
            </div>
            <div field="fedealno" headerAlign="center" align="left" width="120px">成交单编号</div>
            <div field="cname" headerAlign="center" align="center" width="160px">交易对手</div>
            <div field="vdate" width="100" align="center" headerAlign="center" renderer="onDateRenderer">划款日期</div>
            <div field="amount" width="120" numberFormat="#,0.00" headerAlign="center" align="right" allowSort="true">
                划款金额
            </div>
            <div field="ioper" headerAlign="center" align="center" width="120px">经办人员</div>
            <div field="voper" headerAlign="center" align="center" width="120px">复核人员</div>
            <div field="ccyauthoper" headerAlign="center" align="center" width="120px">副总经理审批</div>
            <div field="secauthoper" headerAlign="center" align="center" width="120px">总经理审批</div>
            <div field="authoriza" headerAlign="center" align="center" width="120px">主管行长</div>
        </div>
    </div>
</div>
</body>

<script type="text/javascript">
    mini.parse();
    var LODOP; //声明为全局变量
    var selectRowData;

    var grid = mini.get("datagrid");
    var form = new mini.Form("#search_form");

    function onDateRenderer(e) {
        var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value) == null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g, '/'));
        if (value) return mini.formatDate(value, 'yyyy-MM-dd');
    }

    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        searchs(pageSize, pageIndex);
    });


    function data_string(str) {
        var d = eval('new ' + str.substr(1, str.length - 2));
        var ar_date = [d.getFullYear(), d.getMonth() + 1, d.getDate()];
        for (var i = 0; i < ar_date.length; i++) ar_date[i] = dFormat(ar_date[i]);
        return ar_date.join('-');

        function dFormat(i) {
            return i < 10 ? "0" + i.toString() : i;
        }
    }

    //成交日期
    function onDrawDateStart(e) {
        var startDate = e.date;
        var endDate = mini.get("endDate").getValue();
        if (CommonUtil.isNull(endDate)) {
            return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }

    function onDrawDateEnd(e) {
        var endDate = e.date;
        var startDate = mini.get("startDate").getValue();
        if (CommonUtil.isNull(startDate)) {
            return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }

    //查询按钮
    function search() {
        searchs(grid.pageSize, 0);
    }


    //查询数据
    function searchs(pageSize, pageIndex) {
        var form = new mini.Form("search_form");
        var data = form.getData(true);
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        data['sysDate'] = sysDate;
        url = "/IfsCustDVPController/searchCustDVPInfo";
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: url,
            data: params,
            callback: function (data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }

    //清空
    function clear() {
        var form = new mini.Form("search_form");
        form.clear();

        search();
    }

    // 初始化
    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
            search(10, 0);
        });
    });

    //打印
    function print() {
        var selections = grid.getSelecteds();
        if (selections == null || selections.length == 0) {
            mini.alert('请选择一条要打印的数据！', '系统提示');
            return false;
        } else if (selections.length > 1) {
            mini.alert('暂不支持多条数据同时打印，请选择一条要打印的数据！', '系统提示');
            return false;
        }
        var canPrint = selections[0].fedealno;
        var canPrint1 = selections[0].amount;
        var canPrint2 = canPrint + "-" + canPrint1

        if (CommonUtil.isNotNull(canPrint2)) {
            var actionStr = CommonUtil.pPath + "/sl/IfsPrintUtilController/exportload/print/" + PrintNo.DVP + "/" + canPrint;
            $('<form target="_blank" action=' + actionStr + ' method="post"></form>').appendTo('body').submit().remove();
        }
        ;
    }
</script>
</html>
