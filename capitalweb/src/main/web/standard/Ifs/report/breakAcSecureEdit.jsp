<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../../global.jsp" %>
<html>
<head>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script src="<%=basePath%>/miniScript/pinyin.js" type="text/javascript"></script>
    <title>违约债券信息</title>
</head>

<body style="width:100%;height:100%;background:white">
<div class="mini-splitter" style="width:100%;height:100%;">
    <div size="90%" showCollapseButton="false">
        <h1 style="text-align:center"><strong>违约信息</strong></h1>
        <div id="field_form" class="mini-fit area" style="background:white">
            <div class="mini-panel" title="违约信息" style="width:100%;" allowResize="true" collapseOnTitleClick="true">
                <div class="leftarea">
                    <input id="secid" name="secid" class="mini-buttonedit mini-mustFill" labelField="true" width="100%"
                           label="债券号：" labelStyle="text-align:left;width:120px;" required="true"
                           onbuttonclick="onButtonEditSecid"/>
                    <input id="cost" name="cost" class="mini-textbox mini-mustFill" required="true" labelField="true"
                           width="100%" label="成本中心：" labelStyle="text-align:left;width:120px;" allowInput="false"/>
                    <input id="invtype" name="invtype" class="mini-textbox mini-mustFill" required="true"
                           labelField="true" width="100%" label="业务类型：" labelStyle="text-align:left;width:120px;"
                           allowInput="false"/>
                    <input id="qty" name="qty" class="mini-textbox mini-mustFill" required="true" labelField="true"
                           width="100%" label="QTY：" labelStyle="text-align:left;width:120px;" allowInput="false"/>
                    <input id="status" name="status" class="mini-combobox mini-mustFill"
                           data="CommonUtil.serverData.dictionary.SlStatus" required="true" labelField="true"
                           width="100%" label="违约标识：" labelStyle="text-align:left;width:120px;"/>
                    <input id="amont" name="amont" class="mini-textbox mini-mustFill" required="true" labelField="true"
                           width="100%" label="违约本金：" labelStyle="text-align:left;width:120px;" allowInput="false"/>
                    <input id="pint" name="pint" class="mini-textbox mini-mustFill" required="true" labelField="true"
                           width="100%" label="罚息：" labelStyle="text-align:left;width:120px;"/>
                    <input id="flag2" name="flag2" class="mini-textbox mini-mustFill" required="true" labelField="true"
                           width="100%" label=" 备注2：" labelStyle="text-align:left;width:120px;"/>
                </div>
                <div class="leftarea">
                    <input id="br" name="br" class="mini-textbox mini-mustFill" required="true" labelField="true"
                           width="100%" label="机构：" vtype="maxLength:25" labelStyle="text-align:left;width:120px;"
                           allowInput="false"/>
                    <input id="ccy" name="ccy" class="mini-textbox mini-mustFill" labelField="true" label="币种："
                           width="100%" labelStyle="text-align:left;width:120px;" required="true" allowInput="false"/>
                    <input id="port" name="port" class="mini-textbox mini-mustFill" required="true" labelField="true"
                           width="100%" label="投资组合：" labelStyle="text-align:left;width:120px;" allowInput="false"/>
                    <input id="prinamt" name="prinamt" class="mini-textbox mini-mustFill" required="true"
                           labelField="true" width="100%" label="持仓：" labelStyle="text-align:left;width:120px;"
                           allowInput="false"/>
                    <input id="wydate" name="wydate" class="mini-datepicker mini-mustFill" format="yyyy-MM-dd"
                           required="true" labelField="true" width="100%" label="违约日期："
                           labelStyle="text-align:left;width:120px;"/>
                    <input id="int" name="int" class="mini-textbox mini-mustFill" required="true" labelField="true"
                           width="100%" label="利息：" labelStyle="text-align:left;width:120px;" allowInput="false"/>
                    <input id="flag" name="flag" class="mini-textbox mini-mustFill" required="true" labelField="true"
                           width="100%" label="备注：" labelStyle="text-align:left;width:120px;"/>
                    <input id="inputdate" name="inputdate" class="mini-datepicker mini-mustFill" format="yyyy-MM-dd"
                           required="true" labelField="true" width="100%" label="录入日期："
                           labelStyle="text-align:left;width:120px;"/>
                </div>
            </div>
        </div>
    </div>
    <div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
        <div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"
                                                                style="width:120px;" id="save_btn"
                                                                onclick="save">保存交易</a></div>
        <div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"
                                                                style="width:120px;" id="close_btn"
                                                                onclick="close">关闭界面</a></div>
    </div>
</div>

<script type="text/javascript">
    mini.parse();

    //获取当前tab
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var row = params.selectData;
    var url = window.location.search;
    var action = CommonUtil.getParam(url, "action");
    var form = new mini.Form("#field_form");

    var tradeData = {};

    tradeData.selectData = row;
    tradeData.operType = action;
    tradeData.serial_no = row.execId;
    tradeData.task_id = row.taskId;


    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
            initData();
        });
    });

    //初始化数据
    function initData() {
        if ($.inArray(action, ["detail"]) > -1) {
            form.setEnabled(false);
        }
        if ($.inArray(action, ["add", "edit", "detail"]) > -1) {
            form.setData(row);
            mini.get("secid").setValue(row.secid);
            mini.get("secid").setText(row.secid);
            // mini.get("wydate").setValue(row.wydate);
            // mini.get("inputdate").setValue(row.inputdate);
            mini.get("status").setValue("1");
            mini.get("flag").setEnabled(false);
            mini.get("int").setEnabled(false);
            mini.get("pint").setEnabled(false);
            mini.get("status").setEnabled(false);

        }
        if ($.inArray(action, ["add", "detail"]) > -1) {
        }
        if ($.inArray(action, ["edit", "detail"]) > -1) {
        }
        if ($.inArray(action, ["add"]) > -1) {
            mini.get("flag").setValue("0");
            mini.get("flag").setEnabled(false);
            mini.get("int").setValue("0");
            mini.get("int").setEnabled(false);
            mini.get("pint").setValue("0");
            mini.get("pint").setEnabled(false);

        }

    }


    //保存
    function save() {
        //表单验证！！！
        form.validate();
        if (form.isValid() == false) {
            return;
        }
        var data = form.getData(true);
        var params = mini.encode(data);
        var url = "";
        if (action == 'edit') {
            var url = "/SlFiDefaultServiceController/saveSlFiEdit"
        }
        if (action == 'add') {
            var url = "/SlFiDefaultServiceController/saveSlFiAdd"
        }
        CommonUtil.ajax({
            url: url,
            data: params,
            callback: function (data) {
                if ('error.common.0000' == data.code) {
                    mini.alert("保存成功!", "系统提示", function (value) {
                        close();
                    });

                } else {
                    mini.alert("保存失败,该债券已经存在，请重新选择债券！", "系统提示");
                }
            }
        });
    }

    function close() {
        top["win"].closeMenuTab();
    }

    //英文、数字、下划线 的验证
    function onEnglishAndNumberValidation(e) {
        if (e.value == "" || e.value == null) {//值为空，就不做校验
            return;
        }
        if (e.isValid) {
            if (isEnglishAndNumber(e.value) == false) {
                e.errorText = "必须输入英文+数字";
                e.isValid = false;
            }
        }
    }

    /* 是否英文+数字 */
    function isEnglishAndNumber(v) {
        var re = new RegExp("^[0-9a-zA-Z\_\-]+$");
        if (re.test(v)) return true;
        return false;
    }

    /* 是否汉字 */
    function isChinese(v) {
        var re = new RegExp("^[\u4e00-\u9fa5]+$");
        if (re.test(v)) return true;
        return false;
    }

    function onChineseValidation(e) {
        if (e.isValid) {
            if (isChinese(e.value) == true) {
                e.errorText = "不能输入中文";
                e.isValid = false;
            }
        }
    }

    //债券代码选择
    function onButtonEditSecid(e) {
        mini.open({
            url: "./Ifs/exposure/bondHoldReport.jsp",
            title: "选择债券列表",
            width: 900,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        mini.get("br").setValue("01");
                        mini.get("secid").setValue(data.secid);
                        mini.get("secid").setText(data.secid);
                        mini.get("cost").setValue(data.cost);
                        mini.get("ccy").setValue(data.ccy);
                        mini.get("port").setValue(data.port);
                        mini.get("invtype").setValue(data.invtype);
                        if (data.invtype == '以摊余成本计量金融资产') {
                            mini.get("invtype").setValue("H");
                            mini.get("invtype").setText("以摊余成本计量金融资产");
                        }
                        if (data.invtype == 'FVOCI金融资产') {
                            mini.get("invtype").setValue("A");
                            mini.get("invtype").setText("FVOCI金融资产");
                        }
                        if (data.invtype == '交易性资产') {
                            mini.get("invtype").setValue("T");
                            mini.get("invtype").setText("交易性资产");
                        }
                        mini.get("prinamt").setValue(data.prinamt);
                        mini.get("amont").setValue(data.amt);
                        mini.get("qty").setValue(data.qty);

                        mini.get("br").setEnabled(false);
                        mini.get("cost").setEnabled(false);
                        mini.get("ccy").setEnabled(false);
                        mini.get("port").setEnabled(false);
                        mini.get("invtype").setEnabled(false);
                        mini.get("bndnm_cn").setEnabled(false);
                        mini.get("prinamt").setEnabled(false);
                        mini.get("amont").setEnabled(false);
                        mini.get("qty").setEnabled(false);
                    }
                }
            }
        });
    }


</script>
<script src="<%=basePath%>/miniScript/miniMustFill.js"></script>
</body>
</html>