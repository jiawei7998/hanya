<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../../global.jsp" %>
<html>
<head>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>质押券维护</title>
</head>

<body style="width:100%;height:100%;background:white">
<div class="mini-splitter" style="width:100%;height:100%;">
    <div size="90%" showCollapseButton="false">
        <h1 style="text-align:center"><strong>质押券管理</strong></h1>
        <div id="field_form" class="mini-fit area" style="background:white">

            <div class="mini-panel" title="交易主体信息" style="width:100%" allowResize="true" collapseOnTitleClick="true">
                <div id="search_detail_form">
                    <div class="leftarea">
                        <input style="width:100%" id="ticketId" name="ticketId" class="mini-textbox mini-mustFill"
                               labelField="true" required="true" label="成交单号："
                               labelStyle="text-align:left;width:130px;"/>
                        <input style="width:100%" id="bondCode" name="bondCode" class="mini-buttonedit mini-mustFill"
                               labelField="true" required="true" label="债券代码：" allowInput="false"
                               onbuttonclick="onBondQuery" vtype="maxLength:20"
                               onvalidation="CommonUtil.onValidation(e,'alphanum',[null])"
                               labelStyle="text-align:left;width:130px;"/>
                        <input style="width:100%" id="faceValue" name="faceValue" class="mini-spinner mini-mustFill"
                               changeOnMousewheel='false' required="true" labelField="true" label="券面总额(万元)："
                               maxValue="9999999999.9999" format="n4" required="true"
                               labelStyle="text-align:left;width:130px;"/>
                        <%--                        <input  style="width:100%;" id="prod" name="prod" onbuttonclick="onBondPrdEdit" allowInput="false" class="mini-buttonedit mini-mustFill" labelField="true"   label="产品代码："  maxLength="6"  labelStyle="text-align:left;width:130px;" >--%>
                        <input id="port" name="port" class="mini-buttonedit mini-mustFill"
                               onbuttonclick="onPortCostQuery" style="width:100%;" required="true" labelField="true"
                               label="投资组合：" allowInput="false" vtype="maxLength:4"
                               labelStyle="text-align:left;width:130px;">
                        <input id="vdate" name="vdate" class="mini-datepicker mini-mustFill" style="width:100%;"
                               labelField="true" label="起息日：" labelStyle="text-align:left;width:130px;"
                               allowInput="false">
                    </div>
                    <div class="rightarea">
                        <input style="width:100%" id="cleanPrice" name="cleanPrice" class="mini-spinner mini-mustFill"
                               changeOnMousewheel='false' required="true" labelField="true" label="折算比例(%)："
                               minValue="0" maxValue="100" format="n6" changeOnMousewheel="false"
                               labelStyle="text-align:left;width:130px;"/>
                        <input style="width:100%" id="descr" name="descr" class="mini-textbox" labelField="true"
                               label="债券名称：" enabled="false" vtype="maxLength:25"
                               labelStyle="text-align:left;width:130px;"/>
                        <%--                        <input style="width:100%;" id="prodtype" name="prodtype" onbuttonclick="onBondTypeEdit" allowInput="false" class="mini-buttonedit mini-mustFill" labelField="true"  label="产品类型："  vtype="maxLength:2" labelStyle="text-align:left;width:130px;" >--%>
                        <input style="width:100%;" id="cost" name="cost" onbuttonclick="onBondCostEdit"
                               allowInput="false" required="true" class="mini-buttonedit mini-mustFill"
                               labelField="true" label="成本中心：" vtype="maxLength:15"
                               labelStyle="text-align:left;width:130px;">
                        <input id="invType" name="invType" class="mini-combobox mini-mustFill" labelField="true"
                               required="true" label="投资类型：" style="width:100%;"
                               labelStyle="text-align:left;width:130px;"
                               data="CommonUtil.serverData.dictionary.invtype"/>
                        <input id="mdate" name="mdate" class="mini-datepicker mini-mustFill" labelField="true"
                               label="到期日：" style="width:100%;" labelStyle="text-align:left;width:130px;"
                               allowInput="false"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
        <div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"
                                                                style="width:120px" id="save_btn"
                                                                onclick="save">保存交易</a></div>
        <div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"
                                                                style="width:120px" id="close_btn"
                                                                onclick="close">关闭界面</a></div>
    </div>
</div>
<script type="text/javascript">

    /*****************************初始化*************************************/
    mini.parse();
    //获取当前tab

    var url = window.location.search;
    var action = CommonUtil.getParam(url, "action");
    var ticketId = CommonUtil.getParam(url, "ticketid");
    var prdNo = CommonUtil.getParam(url, "prdNo");
    var prdName = CommonUtil.getParam(url, "prdName");
    var dealType = CommonUtil.getParam(url, "dealType");

    var form = new mini.Form("#field_form");
    var row;
    var tradeData = {};
    if (action != "print") {
        var currTab = top["win"].tabs.getActiveTab();
        var params = currTab.params;
        row = params.selectData;
        tradeData.selectData = row;
        tradeData.operType = action;
        tradeData.serial_no = row.ticketId;
        tradeData.task_id = row.taskId;
    }

    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
            if ($.inArray(action, ["approve", "detail", "print"]) > -1) {
                mini.get("save_btn").setVisible(false);
                form.setEnabled(false);
            }
            if ($.inArray(action, ["add"]) > -1) {
                mini.get("bondCode").setVisible(true);
                mini.get("descr").setVisible(true);
                mini.get("faceValue").setVisible(true);
            }
            if ($.inArray(action, ["edit"]) > -1) {

                mini.get("ticketId").setEnabled(false);
                mini.get("bondCode").setEnabled(false);
                mini.get("port").setEnabled(false);
                mini.get("cost").setEnabled(false);
                mini.get("invType").setEnabled(false);
                // mini.get("descr").setVisible(true);
                // mini.get("faceValue").setEnabled(true);
            }
            if ($.inArray(action, ["edit", "approve", "detail"]) > -1) {//修改、审批、详情
                form.setData(row);
                mini.get("ticketId").setValue(row.ticketId);
                //投资组合
                mini.get("port").setValue(row.port);
                mini.get("port").setText(row.port);
                //成本中心
                mini.get("cost").setValue(row.cost);
                mini.get("cost").setText(row.cost);
                //产品代码
                // mini.get("prod").setValue(row.prod);
                // mini.get("prod").setText(row.prod);
                //产品类型
                // mini.get("prodtype").setValue(row.prodtype);
                // mini.get("prodtype").setText(row.prodtype);
                //债券编号
                mini.get("bondCode").setValue(row.bondCode);
                mini.get("bondCode").setText(row.bondCode);
            }
        });
    });

    /*****************************按钮方法*************************************/
    //保存
    function save() {
        //表单验证！！！
        form.validate();
        if (form.isValid() == false) {
            mini.alert("信息填写有误，请重新填写", "系统提示");
            return;
        }
        var data = form.getData(true);
        var params = mini.encode(data);
        var saveUrl = "";
        if (action == "add") {
            saveUrl = "/IfsBondPledgeController/saveBondPLldge";
        } else if (action == "edit") {
            saveUrl = "/IfsBondPledgeController/updateBondPLldge";
        }
        CommonUtil.ajax({
            url: saveUrl,
            data: params,
            callback: function (data) {
                mini.alert(data.desc, '提示信息', function () {
                    if (data.code == 'error.common.0000') {
                        top["win"].closeMenuTab();//关闭并刷新当前界面
                    }
                });
            }
        });
    }

    function close() {
        top["win"].closeMenuTab();
    }

    /*****************************事件触发*************************************/
    function onBondQuery(e) {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Ifs/bond/bondAddMini.jsp",
            title: "选择债券列表",
            width: 700,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.bndcd);
                        btnEdit.setText(data.bndcd);
                        mini.get("descr").setValue(data.bndnm_cn);
                        mini.get("descr").setText(data.bndnm_cn);
                        btnEdit.focus();
                    }
                }
            }
        });
    }

    function onPortCostQuery() {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "/opics/portMiniLess.jsp",
            title: "选择投资组合",
            width: 500,
            height: 500,
            onload: function () {
                var iframe = this.getIFrameEl();
                iframe.contentWindow.SetData({});
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.portfolio);
                        btnEdit.setText(data.portfolio);
                        btnEdit.focus();
                    }
                }
            }
        });
    }

    /* 券的产品代码的选择 */
    // function onBondPrdEdit(){
    //     var btnEdit = this;
    //     mini.open({
    //         url: CommonUtil.baseWebPath() + "/opics/prodMiniLess.jsp",
    //         title: "选择产品代码",
    //         width: 700,
    //         height: 500,
    //         onload: function () {
    //             var iframe = this.getIFrameEl();
    //             iframe.contentWindow.SetData({});
    //         },
    //         ondestroy: function (action) {
    //             if (action == "ok") {
    //                 var iframe = this.getIFrameEl();
    //                 var data = iframe.contentWindow.GetData();
    //                 data = mini.clone(data);    //必须
    //                 if (data) {
    //                     btnEdit.setValue(data.pcode);
    //                     btnEdit.setText(data.pcode);
    //                     onBondPrdChanged();
    //                     btnEdit.focus();
    //                 }
    //             }
    //         }
    //     });
    // }
    //
    // function onBondPrdChanged(){
    //     if(mini.get("prodtype").getValue()!=""){
    //         mini.get("prodtype").setValue("");
    //         mini.get("prodtype").setText("");
    //         mini.alert("产品代码已改变，请重新选择产品类型!");
    //     }
    // }

    /* 券产品类型的选择 */
    // function onBondTypeEdit(){
    //     var prd = mini.get("prod").getValue();
    //     if(prd == null || prd == ""){
    //         mini.alert("请先选择产品代码!");
    //         return;
    //     }
    //     var data={prd:prd};
    //     var btnEdit = this;
    //     mini.open({
    //         url: CommonUtil.baseWebPath() + "/opics/typeMiniLess.jsp",
    //         title: "选择产品类型",
    //         width: 700,
    //         height: 500,
    //         onload: function () {
    //             var iframe = this.getIFrameEl();
    //             iframe.contentWindow.SetData(data);
    //         },
    //         ondestroy: function (action) {
    //             if (action == "ok") {
    //                 var iframe = this.getIFrameEl();
    //                 var data = iframe.contentWindow.GetData();
    //                 data = mini.clone(data);    //必须
    //                 if (data) {
    //                     btnEdit.setValue(data.type);
    //                     btnEdit.setText(data.type);
    //                     btnEdit.focus();
    //                 }
    //             }
    //         }
    //     });
    // }

    /* 成本中心的选择 */
    function onBondCostEdit() {
        var btnEdit = this;
        var data = {prdName: "SECUR"};
        mini.open({
            url: CommonUtil.baseWebPath() + "/opics/costMiniLess.jsp",
            title: "选择成本中心",
            width: 700,
            height: 500,
            onload: function () {
                var iframe = this.getIFrameEl();
                iframe.contentWindow.SetData(data);
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.costcent);
                        btnEdit.setText(data.costcent);
                        btnEdit.focus();
                    }
                }
            }
        });
    }

</script>
</body>
</html>
