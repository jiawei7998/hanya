<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>客户准入维护</title>
</head>

<body style="width:100%;height:100%;background:white">

<fieldset class="mini-fieldset" style="width:100%;height:45%;">
	<div id="field_form" class="mini-fit area"  style="background:white">
		<fieldset>
        <legend>还本付息详情</legend>
           <div class="leftarea">
               <input id="dealno" name="dealno" class="mini-textbox"   labelField="true"  label="交易流水："  required="true"   enabled="false"  style="width:100%;"  labelStyle="text-align:left;width:130px;"  />
               <input id="product" name="product" class="mini-textbox"   labelField="true"  label="产品："  required="true"   enabled="false"  style="width:100%;"  labelStyle="text-align:left;width:130px;"  />
               <input id="status" name="status" class="mini-textbox"   labelField="true"  label="交易类型："  required="true"   enabled="false"  style="width:100%;"  labelStyle="text-align:left;width:130px;"  />
               
           	   <input id="prinamt" name="prinamt" class="mini-spinner" labelField="true"  label="原始本金：" maxValue="9999999999999999999" minValue = "-999999999999999999999" format="n4" enabled="false" required="true"   labelStyle="text-align:left;width:130px;" style="width:100%;"   />
           	   <input id="purchintamt" name="purchintamt" class="mini-spinner" labelField="true"  label="利息金额：" maxValue="9999999999999999999" minValue = "-999999999999999999999" format="n4" enabled="false" required="true"   labelStyle="text-align:left;width:130px;" style="width:100%;"   />
           </div>			
           <div class="rightarea">
               <input id="secid" name="secid" class="mini-textbox" labelField="true"  label="债券代码："  required="true"    enabled="false"   labelStyle="text-align:left;width:130px;" style="width:100%;"   />
               <input id="prodtype" name="prodtype" class="mini-textbox" labelField="true"  label="产品类型："  required="true"    enabled="false"   labelStyle="text-align:left;width:130px;" style="width:100%;"   />
               <input id="ccy" name="ccy" class="mini-textbox" labelField="true"  label="币种："  required="true"    enabled="false"   labelStyle="text-align:left;width:130px;" style="width:100%;"   />
               <!-- <input id="settdate" name="settdate" class="mini-textbox" labelField="true"  label="结算日期："  required="true"    enabled="false"   labelStyle="text-align:left;width:130px;" style="width:100%;"   /> -->
           	   <input id="proceedamt" name="proceedamt" class="mini-spinner" labelField="true"  label="收益金额：" maxValue="9999999999999999999" minValue="-9999999999999999999" format="n4" enabled="false" required="true"   labelStyle="text-align:left;width:130px;" style="width:100%;"   />
           	   <input id="cno" name="cno" class="mini-textbox"   labelField="true"  label="交易对手："  required="true"   enabled="false"  style="width:100%;"  labelStyle="text-align:left;width:130px;"  />
           </div>		
    	</fieldset>
   </div>
</fieldset>

<div class="mini-splitter" style="width:100%;height:40%;">
	<div size="90%" showCollapseButton="false">
		<div id="field_form2" class="mini-fit area"  style="background:white">
            <fieldset>
             <legend>还本付息维护</legend>
                <div class="leftarea">
                	<input id="prinamtComfire" name="prinamtComfire" class="mini-spinner" labelField="true"  label="原始本金确认：" maxValue="9999999999999999999" minValue = "-999999999999999999999" format="n4" enabled="true" required="true"   labelStyle="text-align:left;width:130px;" style="width:100%;"   />
                	<input id="purchintamtComfire" name="purchintamtComfire" class="mini-spinner" labelField="true"  label="利息金额确认：" maxValue="9999999999999999999" minValue = "-999999999999999999999" format="n4" enabled="true" required="true"   labelStyle="text-align:left;width:130px;" style="width:100%;"   />
                	<input id="status" name="status" class="mini-textbox"   labelField="true"  label="修改状态："  required="true"   enabled="false"  style="width:100%;"  labelStyle="text-align:left;width:130px;"  />
                </div>			
                <div class="rightarea">
                	<input id="proceedamtComfire" name="proceedamtComfire" class="mini-spinner" labelField="true"  label="收益金额确认：" maxValue="9999999999999999999" minValue = "-999999999999999999999" format="n4" enabled="true" required="true"   labelStyle="text-align:left;width:130px;" style="width:100%;" />
                	<input id="operator" name="operator" class="mini-textbox" labelField="true"  label="经办人："  required="true"    enabled="false"   labelStyle="text-align:left;width:130px;" style="width:100%;"   />
                	<input id="existFlag" name="existFlag" class="mini-textbox" enabled="false" visible="false" labelField="true"  label="是否存过数据库标志：" style="width:100%;" labelStyle="text-align:left;width:130px;" value = "0" />
                </div>		
            </fieldset>  
		</div>
	</div>		
    <div id="functionIds"  style="padding-top:30px;text-align:center;">
        <div style="margin-bottom:10px; text-align: center;">
            <a class="mini-button" style="display: none"  style="width:120px;" id="save_btn" onclick="save">保存</a>
        </div>
        <div style="margin-bottom:10px; text-align: center;">
            <a class="mini-button" style="display: none"  style="width:120px;" id="close_btn" onclick="close">关闭</a>
        </div>
    </div>
</div>

<script type="text/javascript">
	mini.parse();

	//获取当前tab
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var row = params.selectData;
	var url = window.location.search;
	var action = CommonUtil.getParam(url,"action"); // 增删改
	var form = new mini.Form("#field_form");
	var form2 = new mini.Form("#field_form2");

	function onDateRenderer(e) {
		var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)==null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g,'/'));
		if (value) return mini.formatDate(value, 'yyyy-MM-dd');
	}
	
	$(document).ready(function(){
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
            initData();
        });
	}); 
	
	//初始化数据
	function initData(){
		/* $(document).ready(function(){
    		queryCustNm();
    	}); */
	    if($.inArray(action,["edit","detail"])>-1){//修改、详情
	    	//mini.get("cORE_CLNT_NO").setEnabled(false); // 客户号不允许修改
	    	//mini.get("bUSS_TP_NM").setEnabled(false); // 业务类型名称不允许修改
	        form.setData(row);
	        if(action == "detail"){
	            $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>还本付息</a> >> 详情");
	            form.setEnabled(false);
                form2.setEnabled(false);
	            //加载修改详情
	            // checkdealno();
	            mini.get("save_btn").setVisible(false);
	        }
	    }
	}
	
	function onDateRenderer(e) {//转换日期格式
		if(e.value!=null){
			var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)==null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g,'/'));
	        if (value) return mini.formatDate(value, 'yyyy-MM-dd');
		}
    }
	
	
	//保存
	function save(){
		var form = new mini.Form("#field_form2");
	    //表单验证！！！
	    form.validate();
	    if (form.isValid() == false) {
	        mini.alert("表单填写有误!","提示");
	        return;
	    }
	    var data = form.getData(true);
 	    data['dealno']=mini.get('dealno').getValue();
 	    data['prinamt']=mini.get('prinamt').getValue();
 	    data['purchintamt']=mini.get('purchintamt').getValue();
 	    data['proceedamt']=mini.get('proceedamt').getValue();
	    
	    var params = mini.encode(data);
	    CommonUtil.ajax({
	    	url:"/IfsSecurServerController/saveByDealNo",
	        data:params,
	        callback:function(data){
	            mini.alert("保存成功",'提示信息',function(){
	                top["win"].closeMenuTab();
	            });
	        }
	    });
	}
	
	function close(){
		top["win"].closeMenuTab();
	}
	
	//
    function checkdealno(){
    	var form = new mini.Form("#field_form2");
    	var data ;
    	var dealno = mini.get("dealno").getValue();
    	data = {dealno : dealno};
    	//var params = mini.encode(data);
    	CommonUtil.ajax({
    		url:"/IfsSecurServerController/getAdjAmtList",
    		data:data,
    		callback:function(data){
    			if(data.obj == null){ // 查询不到客户信息
    				return;
    			}else{
    				var operator = data.obj.operator;
    				if(operator != '<%=__sessionUser.getUserId() %>'){
    					form.setEnabled(false);
    		            mini.get("save_btn").setVisible(false);
    				}
    				form.setData(data.obj);
    			}
    		}
    	});
    }
	
</script>
</body>
</html>