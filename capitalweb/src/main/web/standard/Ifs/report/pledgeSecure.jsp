<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>质押券管理</title>
<%--	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>--%>
</head>
<body style="width: 100%; height: 100%; background: white">
<fieldset class="mini-fieldset">
	<legend>查询条件</legend>
	<div id="search_form" style="width: 70%;">
		<input id="ticketId" name="ticketId" class="mini-textbox" labelField="true"  label="成交单号：" labelStyle="text-align:right;"/>
		<input id="bondCode" name="bondCode" class="mini-textbox" labelField="true"  label="债券编号：" labelStyle="text-align:right;"/>
		<input id="descr" name="descr" class="mini-textbox" labelField="true"  label="债券名称(模糊)：" labelStyle="text-align:right;"/>
		<input id="startDate" name="startDate" class="mini-datepicker" labelField="true"  label="日期：" allowinput="false"
			   ondrawdate="onDrawDateStart" format="yyyy-MM-dd" labelStyle="text-align:right;" emptyText="开始日期" value="<%=__bizDate%>"/>
		<span>~</span>
		<input id="endDate" name="endDate" class="mini-datepicker" allowinput="false"
			   ondrawdate="onDrawDateEnd" emptyText="结束日期" format="yyyy-MM-dd" value="<%=__bizDate%>"/>
		<span style="float: right; margin-right: 10px">
			<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
			<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
		</span>
	</div>
</fieldset>

<span style="margin: 2px; display: block;">
	<a  id="add_btn" class="mini-button" style="display: none"   onclick="add()">新增</a>
	<a  id="edit_btn" class="mini-button" style="display: none"   onclick="edit()">修改</a>
	<a  id="delete_btn" class="mini-button" style="display: none"   onclick="del()">删除</a>
</span>

<div id="MiniSettleForeignSpot" class="mini-fit" style="margin-top: 2px;">
	<div id="datagrid" class="mini-datagrid borderAll" style="width: 100%; height: 100%;" allowAlternating="true"
		allowResize="true" border="true" sortMode="client" multiSelect="true" onrowdblclick="onRowDblClick">
		<div property="columns">
			<div type="indexcolumn" headerAlign="center" width="40">序号</div>
			<div field="ticketId"  headerAlign="center" align="center" width="100px">成交单号</div>
			<div field="bondCode"  headerAlign="center" align="center" width="100px">债券编号</div>
			<div field="descr"  headerAlign="center" align="center" width="100px">债券名称</div>
			<div field="port"  headerAlign="center" align="center" width="100px">投资组合</div>
			<div field="cost"  headerAlign="center" align="center" width="100px">成本中心</div>
			<div field="invType"  headerAlign="center" align="center" width="100px">投资类型</div>
			<div field="faceValue"  headerAlign="center" align="center" width="100px" numberFormat="#,0.0000">券面总额(万元)</div>
			<div field="cleanPrice"  headerAlign="center" align="center" width="100px">折算比例(%)</div>
			<div field="vdate"  headerAlign="center" align="center" width="100px" renderer="onDateRenderer">起息日</div>
			<div field="mdate"  headerAlign="center" align="center" width="100px" renderer="onDateRenderer">到期日</div>
		</div>
	</div>
</div>

<script>
	/**************************************初始化*******************************************/
	mini.parse();

	var url = window.location.search;
	var grid = mini.get("datagrid");
	var userId='<%=__sessionUser.getUserId()%>';
	
	grid.on("beforeload", function(e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex;
		var pageSize = e.data.pageSize;
		search(pageSize, pageIndex);
	});

	$(document).ready(function() {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
			search(10, 0);
		});
	});


	/**************************************按钮方法*******************************************/

	function query() {
		search(grid.pageSize, 0);
	}

	function clear(){
		var form=new mini.Form("search_form");
		form.clear();
		search(10,0);
	}

	// 查询
	function search(pageSize,pageIndex){
		var form = new mini.Form("#search_form");
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统也提示");
			return;
		}
		var data=form.getData(true);

		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:"/IfsBondPledgeController/searchBondPledge",
			data:params,
			callback : function(data) {
				if(data){
					grid.setTotalCount(data.obj.total);
					grid.setPageIndex(pageIndex);
					grid.setPageSize(pageSize);
					grid.setData(data.obj.rows);
				}
			}
		});
	}

	//添加
	function add(){
		var url = CommonUtil.baseWebPath() +"/report/pledgeSecureEdit.jsp?action=add";
		var tab = {id: "pledgeSecureAdd",name:"pledgeSecureAdd",url:url,title:"质押券添加",parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
		var paramData = {selectData:""};
		CommonUtil.openNewMenuTab(tab,paramData);
	}
	//修改
	function edit() {
		var row = grid.getSelected();
		if (row) {
			var url = CommonUtil.baseWebPath() + "/report/pledgeSecureEdit.jsp?action=edit";
			var tab = {id: "pledgeSecureEdit", name: "pledgeSecureEdit", url: url, title: "质押券修改",parentId: top["win"].tabs.getActiveTab().name, showCloseButton: true};
			var paramData = {selectData: row};
			CommonUtil.openNewMenuTab(tab, paramData);
		} else {
			mini.alert("请选择一条记录", "提示");
		}
	}

	//删除
	function del(){
		var row = grid.getSelected();
		if (row) {
			mini.confirm("您确认要删除选中记录?","系统警告",function(value){
				if(value == 'ok'){
					var data=row;
					params=mini.encode(data);
					CommonUtil.ajax( {
						url:"/IfsBondPledgeController/deleteCfetsrmbCr",
						data:params,
						callback : function(data) {
							mini.alert("删除成功.","系统提示");
							search(10,0);
						}
					});
				}
			});
		}else{
			mini.alert("请选择一条记录", "提示");
		}
	}

	/**************************************事件方法*******************************************/
	//成交日期
	function onDrawDateStart(e) {
		var startDate = e.date;
		var endDate= mini.get("endDate").getValue();
		if(CommonUtil.isNull(endDate)){
			return;
		}
		if (endDate.getTime() < startDate.getTime()) {
			e.allowSelect = false;
		}
	}
	function onDrawDateEnd(e) {
		var endDate = e.date;
		var startDate = mini.get("startDate").getValue();
		if(CommonUtil.isNull(startDate)){
			return;
		}
		if (endDate.getTime() < startDate.getTime()) {
			e.allowSelect = false;
		}
	}
	function onDateRenderer(e) {
		if(e.value!=null){
			var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)==null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g,'/'));
	        if (value) return mini.formatDate(value, 'yyyy-MM-dd');
		}
    }

	//查看详情
	function onRowDblClick(e) {
		var url = CommonUtil.baseWebPath() +"/report/pledgeSecureEdit.jsp?action=detail";
		var tab = {id: "pledgeSecureDetail",name:"pledgeSecureDetail",url:url,title:"质押券详情",parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
		var paramData = {selectData:grid.getSelected()};
		CommonUtil.openNewMenuTab(tab,paramData);
	}

</script>
</body>
</html>