<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>债券持仓余额表</title>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>
<body style="width: 100%; height: 100%; background: white">
<fieldset class="mini-fieldset">
	<legend>查询条件</legend>
	<div id="search_form" style="width: 100%;">
		<input id="postdate" name="postdate" class="mini-datepicker" labelField="true" label="数据日期："
         	labelStyle="text-align:right;" emptyText="请选择日期" />
        <input id="secid" name="secid" class="mini-textbox" labelField="true"  label="债券代码：" width="280px" 
        	labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请输入债券代码" />
        <span style="float: right; margin-right: 50px">
			<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
			<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
			<a id="export_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
		</span>
	</div>
</fieldset>
	
<div class="mini-fit" style="margin-top: 2px;">
	<div id="datagrid" class="mini-datagrid borderAll" style="width: 100%; height: 100%;" allowAlternating="true"
		allowResize="true" border="true" sortMode="client" multiSelect="true">
		<div property="columns">
			<div type="indexcolumn" headerAlign="center" width="40">序号</div>
			<div field="secid" width="120" align="center" headerAlign="center" >债券代码</div>
			<div field="issuer" width="120" align="center" headerAlign="center" >发行人</div>
			<div field="acctngtype" width="120" align="center" headerAlign="center" >债券类型</div>
			<div field="cost" width="120" align="center" headerAlign="center" >所属账户</div>
			<div field="port" width="120" align="center" headerAlign="center" >组合分类</div>
			<div field="invtype" width="120" align="center" headerAlign="center" >账户类型</div>
			<div field="ipaydate" width="120" align="center" headerAlign="center" renderer="onDateRenderer">下一付息日</div>
			<div field="paramt" width="120" align="center" headerAlign="center" >券面价值</div>
			<div field="amortamt" width="120" align="right" headerAlign="center" numberFormat="#,0.0000">摊余成本</div>
			<div field="tdyintamt" width="120" align="right" headerAlign="center" numberFormat="#,0.0000">本日计提</div>
			<div field="accrintamt" width="120" align="right" headerAlign="center" numberFormat="#,0.0000">截止本日计提</div>
			<div field="unamortamt" width="120" align="right" headerAlign="center" numberFormat="#,0.0000">未摊余额</div>
			<div field="tdyamortamt" width="120" align="right" headerAlign="center" numberFormat="#,0.0000">本日摊销</div>
			<div field="unitcost" width="120" align="right" headerAlign="center" numberFormat="#,0.0000">单位成本</div>
			<div field="cutpoint" width="120" align="right" headerAlign="center" numberFormat="#,0.0000">止损净价</div>
			<div field="clsgprice" width="120" align="right" headerAlign="center" numberFormat="#,0.0000">公允价格</div>
			<div field="facevalue" width="120" align="right" headerAlign="center" >券面</div>
			<div field="fairvalue" width="120" align="right" headerAlign="center" >公允价值</div>
			<div field="realRate" width="120" align="right" headerAlign="center" >实际收益率</div>
			<div field="diffamt" width="120" align="right" headerAlign="center" >估值损益</div>
			<div field="vdate" width="120" align="center" headerAlign="center" renderer="onDateRenderer">起息日</div>
			<div field="mdate" width="120" align="center" headerAlign="center" renderer="onDateRenderer">到期日</div>
			<div field="couprate" width="120" align="right" headerAlign="center" >票面利率</div>
			<div field="yearstomaturity" width="120" align="right" headerAlign="center" numberFormat="#,0.0000">剩余期限</div>
			<div field="ratetype" width="120" align="center" headerAlign="center" >利率类型</div>
			<div field="intpaycycle" width="120" align="center" headerAlign="center" >付息频率</div>
			<div field="subjectrating" width="120" align="center" headerAlign="center" >主体评级</div>
			<div field="bondtrating" width="120" align="center" headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'Rating'}">债项评级</div>
			<div field="postdate" width="120" align="center" headerAlign="center" renderer="onDateRenderer">数据日期</div>
		</div>
	</div>
</div>

<script>
	mini.parse();

	var url = window.location.search;
	var grid = mini.get("datagrid");
	var userId='<%=__sessionUser.getUserId()%>';
	
	grid.on("beforeload", function(e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex;
		var pageSize = e.data.pageSize;
		search(pageSize, pageIndex);
	});
	
	function onDateRenderer(e) {
		if(e.value!=null){
			var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)==null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g,'/'));
	        if (value) return mini.formatDate(value, 'yyyy-MM-dd');
		}
    }
	
	// 查询
	function search(pageSize,pageIndex){
		var form = new mini.Form("#search_form");
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统也提示");
			return;
		}
		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		data['type']='1';
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:"/IfsReportController/searchBondPositionBalanceList",
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
		
	function query() {
    	search(grid.pageSize, 0);
    }
	
	function clear(){
        var form=new mini.Form("search_form");
        form.clear();
        search(10,0);
	}
	
	$(document).ready(function() {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
			search(10, 0);
		});
	});
	
	//导出
	function exportExcel(){
		var content = grid.getData();
		if(content.length == 0){
			mini.alert("请先查询数据");
			return;
		}
		mini.confirm("您确认要导出Excel吗?","系统提示", 
			function (action) {
				if (action == "ok"){
			 		var form = new mini.Form("#search_form");
					var data = form.getData(true);
					data['type']='1';
					var fields = null;
					for(var id in data){
						fields += '<input type="hidden" id="' + id + '" name="'+ id +'" value="' + data[id] + '">';
					}
					var urls = CommonUtil.pPath + "/sl/IfsExportController/exportCcyeExcel";                                                                                                                         
					$('<form action="'+ urls +'" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();   
				}
			}
		);
	}
</script>
</body>
</html>