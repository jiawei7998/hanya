<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>浮动拆借定息提醒</title>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>
<body style="width: 100%; height: 100%; background: white">
<fieldset class="mini-fieldset">
    <legend>查询条件</legend>
    <div id="search_form" style="width: 70%;">
        <input id="br" name="br" class="mini-combobox mini-mustFill" width="320px" labelField="true" label="部门：" vtype="maxLength:5" required="true"
               emptyText="请填写部门" labelStyle="text-align:right;"  value='<%=__sessionUser.getOpicsBr()%>' data="CommonUtil.serverData.dictionary.opicsBr"/>
        <%--		<input id="ratefixdate" name="ratefixdate" class="mini-datepicker" width="320px" labelField="true" label="重定价格日期："--%>
        <%--         	labelStyle="text-align:right;" emptyText="请选择日期" value="<%=__bizDate%>"/>--%>
        <%--		<span style="color: red">查询数据小于等于当前输入日期</span>--%>
        <input id="startDate" name="startDate" class="mini-datepicker" labelField="true"  label="定息日期：" allowinput="false"
               ondrawdate="onDrawDateStart" format="yyyy-MM-dd" labelStyle="text-align:right;" emptyText="开始日期" value="<%=__bizDate%>" />
        <span>~</span>
        <input id="endDate" name="endDate" class="mini-datepicker" allowinput="false"
               ondrawdate="onDrawDateEnd" emptyText="结束日期" format="yyyy-MM-dd" value="<%=__bizDate%>"/>
        <span style="float: right; margin-right: 50px">
			<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
			<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
		</span>
    </div>
</fieldset>

<div id="MiniSettleForeignSpot" class="mini-fit" style="margin-top: 2px;">
    <div id="datagrid" class="mini-datagrid borderAll" style="width: 100%; height: 100%;" allowAlternating="true"
         allowResize="true" border="true" sortMode="client" multiSelect="true">
        <div property="columns">
            <div type="indexcolumn" headerAlign="center" width="40">序号</div>
            <div field="br" width="50" align="center" headerAlign="center" >部门</div>
            <div field="dealno" width="120" align="center" headerAlign="center" >opics交易号</div>
            <div field="ratecode" width="120" align="center" headerAlign="center" >利率代码</div>
            <div field="rateRevDte" width="100" allowSort="false" headerAlign="center" align="center" renderer="onDateRenderer">利率重置日期</div>
            <div field="nxtRateRev" width="100" allowSort="false" headerAlign="center" align="center" renderer="onDateRenderer">下次利率重置日期</div>
            <div field="product" width="120" align="center" headerAlign="center" >产品</div>
            <div field="prodtype" width="100" allowSort="true" headerAlign="center" align="center">产品类型</div>
            <div field="ccy" width="100" allowSort="true" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'Currency'}">币种</div>
            <div field="ccyamt" width="100" allowSort="true" headerAlign="center" align="center" numberFormat="#,0.00">原始本金</div>
            <div field="vdate" width="100" allowSort="false" headerAlign="center" align="center" renderer="onDateRenderer">起期日</div>
            <div field="mdate" width="100" allowSort="false" headerAlign="center" align="center" renderer="onDateRenderer">到期日</div>
        </div>
    </div>
</div>

<script>
    /****************************初始化***********************************/
    mini.parse();

    var url = window.location.search;
    var grid = mini.get("datagrid");
    var userId='<%=__sessionUser.getUserId()%>';

    $(document).ready(function() {//控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn){});
        //search(10, 0);
    });

    grid.on("beforeload", function(e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize, pageIndex);
    });
    /****************************按钮方法***********************************/

    function query() {
        search(grid.pageSize, 0);
    }

    function clear(){
        var form=new mini.Form("search_form");
        form.clear();
        mini.get("br").setValue(opicsBr);
        search(10,0);
    }

    // 查询
    function search(pageSize,pageIndex){
        var form = new mini.Form("#search_form");
        form.validate();
        if(form.isValid()==false){
            mini.alert("信息填写有误，请重新填写","系统也提示");
            return;
        }
        var data=form.getData(true);
        var ratefixdate=data['ratefixdate'];
        var time =new Date(ratefixdate);
        time=time.getTime()+24*5*3600*1000;
        ratefixdate=mini.formatDate(new Date(time),'yyyy-MM-dd');
        data['ratefixdate']=ratefixdate;
        data['pageNumber']=pageIndex+1;
        data['pageSize']=pageSize;
        data['branchId']=branchId;
        var params = mini.encode(data);
        CommonUtil.ajax({
            url:"/IfsNettingController/queryINORevaluation",
            data:params,
            callback : function(data) {
                if(data){
                    grid.setTotalCount(data.obj.total);
                    grid.setPageIndex(pageIndex);
                    grid.setPageSize(pageSize);
                    grid.setData(data.obj.rows);
                }
            }
        });
    }
    /****************************其他方法***********************************/

    //成交日期
    function onDrawDateStart(e) {
        var startDate = e.date;
        var endDate= mini.get("endDate").getValue();
        if(CommonUtil.isNull(endDate)){
            return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }
    function onDrawDateEnd(e) {
        var endDate = e.date;
        var startDate = mini.get("startDate").getValue();
        if(CommonUtil.isNull(startDate)){
            return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }
    function onDateRenderer(e) {
        if(e.value!=null){
            var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)==null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g,'/'));
            if (value) return mini.formatDate(value, 'yyyy-MM-dd');
        }
    }



</script>
</body>
</html>