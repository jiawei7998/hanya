<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/miniui/res/ajaxfileupload.js"></script>
<title></title>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
	<legend>RateFeed文件上传</legend>	
	<div>
		<div id="search_form" style="width:100%" cols="6">上传文件：
            <input class="mini-htmlfile" name="Fdata" id="Fdata" style="width:300px;"/>
            <a id="import_btn" class="mini-button" style="display: none"    onclick="ajaxFileUpload()">导入</a>
		</div>
	</div>
</fieldset> 	
<script>
	mini.parse();
	function ajaxFileUpload() {
        var fdata = mini.get("Fdata"), value =fdata.getValue(),file = $("input[name='Fdata']")[0];
        if(!value||value==""){
        	mini.alert("请选择上传文件", "消息");
        	return;
        }
        $.ajaxFileUpload({
            url: '<%=basePath%>/sl/IfsOpicsExcelController/uploadRate',               	                               //用于文件上传的服务器端请求地址
            fileElementId:file,                                                              //文件上传域的ID
            dataType: 'text',                                                                      //返回值类型 一般设置为json
            success: function (data, status)                                                       //服务器成功响应处理函数
            {
            	//data.code=="error.common.0000"
            	var json = JSON.parse(data);
				mini.alert(json.desc, "消息", function() {
					fdata.setText("");
				});
			},
			error : function(data, status, e) //服务器响应失败处理函数
			{
				mini.alert("上传失败：" + e, "消息",function(){
					fdata.setText("");
				});
			}/* ,
			complete : function() {
				fdata.setText("");
			} */
		});
	}
</script>  
</body>
</html>
