<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
		<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
		<script type="text/javascript" src="<%=basePath%>/standard/Common/RiskCenter.js"></script>
		<script type="text/javascript" src="<%=basePath%>/standard/uploadAndDownload.js"></script>
		<script type="text/javascript" src="<%=basePath%>/miniScript/miniui/res/ajaxfileupload.js"></script>
    <title></title>
    <script type="text/javascript" >
	/**prdCode用于页面投资组合的选择   该全局变量必须为prdCode*/
	var prdCode="GOLD-FWD";
	</script>
  </head>

<body style="width:100%;height:100%;background:white">
	<div class="mini-splitter" style="width:100%;height:100%;" id="splitter">
				<div size="90%" showCollapseButton="false">
		<h1 style="text-align:center"><strong>黄金远期</strong></h1>
		<div id="field_form" class="mini-fit area"  style="background:white" >
					<input id="sponsor" name="sponsor" class="mini-hidden" />
					<input id="sponInst" name="sponInst" class="mini-hidden" />
					<input id="aDate" name="aDate" class="mini-hidden"/>
					<input id="ticketId" name="ticketId" class="mini-hidden" />
					<input id="opicsccy" name="opicsccy" class="mini-hidden"/>
					<input id="opicsctrccy" name="opicsctrccy" class="mini-hidden"/>
		
		<div class="mini-panel" title="成交单编号" style="width:100%;"  allowResize="true" collapseOnTitleClick="false">
			<div class="leftarea">
				<input style="width:100%;" id="contractId" name="contractId" class="mini-textbox mini-mustFill" labelField="true" requiredErrorText="该输入项为必输项" required="true"  label="成交单编号：" labelStyle="text-align:left;width:130px;"  vtype="maxLength:35"/>
			</div>
		</div>
		<div class="mini-panel" title="成交双方信息" style="width:100%;"  allowResize="true" collapseOnTitleClick="false">
			<div class="leftarea">
				<input id="instId"  name="instId" label="本方机构：" class="mini-textbox" labelField="true" vtype="maxLength:50" style="width:100%;" labelStyle="text-align:left;width:130px;"enabled="false"/>
				<input id="dealer"  name="dealer" label="本方交易员：" class="mini-textbox" labelField="true" vtype="maxLength:10" style="width:100%;" labelStyle="text-align:left;width:130px;"enabled="false"/>
			</div>
			<div class="rightarea">
				<input id="counterpartyInstId"  name="counterpartyInstId" label="对方机构：" class="mini-buttonedit mini-mustFill" onbuttonclick="onButtonEdit" allowInput="false" labelField="true" style="width:100%;" labelStyle="text-align:left;width:130px;"  required="true" />
				<input id="counterpartyDealer"  name="counterpartyDealer" label="对方交易员：" class="mini-textbox" labelField="true" vtype="maxLength:6" style="width:100%;"labelStyle="text-align:left;width:130px;"/>
			</div>
		</div>
		<div class="mini-panel" title="交易主体信息" style="width:100%;"  allowResize="true" collapseOnTitleClick="false">
					<div class="leftarea">
						<input id="forDate"  name="forDate" label="交易日期：" class="mini-datepicker" labelField="true" style="width:100%;"labelStyle="text-align:left;width:130px;"/>
						<input style="width:100%;" id="currencyPair"  name="currencyPair" label="货币对：" class="mini-combobox mini-mustFill" required="true"  data="CommonUtil.serverData.dictionary.CurrencyPair" onvaluechanged="onCurrencyPairChange"  labelField="true" labelStyle="text-align:left;width:130px;"/>
						<input style="width:100%;" id="quantity"  name="quantity" label="数量："   onvaluechanged="quantityMoney"  class="mini-spinner mini-mustFill input-text-strong"  required="true"  changeOnMousewheel='false'  maxValue="9999999999999999.9999" format="n4"  labelField="true" labelStyle="text-align:left;width:130px;"/>
						<input style="width:100%;" id="amount1"  name="amount1" label="金额："  class="mini-spinner mini-mustFill input-text-strong" required="true"   changeOnMousewheel='false'  maxValue="9999999999999999.9999" format="n2"  labelField="true" labelStyle="text-align:left;width:130px;"/>
						<input style="width:100%;" id="direction2"  name="direction2" label="对方方向：" enabled="false" vtype="maxLength:6" class="mini-hidden" labelField="true" labelStyle="text-align:left;width:130px;"data = "CommonUtil.serverData.dictionary.trading" />
						<input id="sellCurreny" name="sellCurreny" class="mini-combobox mini-mustFill" required="true"  labelField="true" requiredErrorText="该输入项为必输项" label="卖出货币："  style="width:100%;"  labelStyle="text-align:left;width:130px;" data="CommonUtil.serverData.dictionary.Currency" enabled="false"/>
						<input id="tradingModel" name="tradingModel" class="mini-combobox mini-mustFill" required="true"  labelField="true"   label="交易模式：" labelStyle="text-align:left;width:130px;" style="width:100%;"  data="CommonUtil.serverData.dictionary.TradeModel"/>
						<input style="width:100%;" id="settleMode"  name="settleMode" label="清算方式：" vtype="maxLength:13" class="mini-textbox"  labelField="true" labelStyle="text-align:left;width:130px;"/>
					</div>
					<div class="rightarea">
						<input id="forTime"  name="forTime" label="交易时间：" class="mini-timespinner" labelField="true" style="width:100%;"labelStyle="text-align:left;width:130px;"/>
						<input style="width:100%;" id="direction1"  name="direction1" label="交易方向："  onvaluechanged="onCurrencyPairChange" vtype="maxLength:6" class="mini-combobox mini-mustFill" required="true"   labelField="true" labelStyle="text-align:left;width:130px;"data = "CommonUtil.serverData.dictionary.trading" />
						<input style="width:100%;" id="prix"  name="prix" label="价格："   onvaluechanged="quantityMoney"  class="mini-spinner mini-mustFill input-text-strong" required="true"   changeOnMousewheel='false'  maxValue="9999999999999999.9999" format="n2"  labelField="true" labelStyle="text-align:left;width:130px;"/>
						<input style="width:100%;" id="valueDate1"  name="valueDate1" label="起息日：" class="mini-datepicker mini-mustFill" labelField="true" required="true"   labelStyle="text-align:left;width:130px;" ondrawdate="onDrawDate"/>
						<input id="buyCurreny" name="buyCurreny" class="mini-combobox mini-mustFill" required="true"  labelField="true" requiredErrorText="该输入项为必输项"   label="买入货币："  style="width:100%;"  labelStyle="text-align:left;width:130px;" data="CommonUtil.serverData.dictionary.Currency"  enabled="false"/>
						<input id="tradingType" name="tradingType" class="mini-combobox mini-mustFill" labelField="true" requiredErrorText="该输入项为必输项" required="true"  label="交易方式："  value="RFQ" labelStyle="text-align:left;width:130px;" style="width:100%;" data="CommonUtil.serverData.dictionary.TradeType"/>
						<input id="dealTransType" name="dealTransType" class="mini-combobox" labelField="true"  style="width:100%;"  label="数据来源：" data="CommonUtil.serverData.dictionary.dealTransType1" vtype="maxLength:10"  labelStyle="text-align:left;width:130px;"  value="1">
					</div>
		</div>
		<%@ include file="../../Common/opicsLess.jsp"%>
		<%@ include file="../cfetsfx/uploadFile.jsp"%>
		
		<%@ include file="../../Common/Flow/MiniApproveOpCommon.jsp"%>
		<%@ include file="../../Common/print/approveFlowLog.jsp"%>
	</div>
	</div>
			<div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="save_btn"   onclick="save">保存交易</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="close_btn"   onclick="close">关闭界面</a></div>
			</div>
</div>	
<script type="text/javascript">
	mini.parse();
	var url=window.location.search;
	var action=CommonUtil.getParam(url,"action");
	var prdNo = CommonUtil.getParam(url, "prdNo");
	var ticketId = CommonUtil.getParam(url, "ticketid");
	var params ={};
	var tradeData={};
	if(action!="print"){
		//获取当前tab
		var currTab = top["win"].tabs.getActiveTab();
		var params = currTab.params;
		var row=params.selectData;
		tradeData.selectData=row;
		tradeData.operType=action;
		tradeData.serial_no=row.ticketId;
		tradeData.task_id=row.taskId;
	}
	var ApproveFlowLog = {};
	var log_grid = mini.get("log_grid");//隐藏日志信息列表，打印时显示
	log_grid.hide();
	/**
	 *   获取审批日志信息 
	*/
	ApproveFlowLog.loadLogInfo = function(){
		CommonUtil.ajax({
			url:"/IfsFlowController/approveLog",
			data:{'serial_no':ticketId},
			callback : function(data) {
				if(data != null){
					log_grid.setData(data.obj);
					if(typeof(FlowDesigner) != "undefined")
					{
						FlowDesigner.addApporveLog(data.obj);
					}
				}
			}
		});
	};
	
	
	//保存
	function save(){
		var form=new mini.Form("#field_form");
		form.validate();
		if(form.isValid() == false){
			mini.alert("信息填写有误，请重新填写","系统提示");
			return;
		}
		var data=form.getData(true);
		data['type']="fwd";
		data['instId']="<%=__sessionInstitution.getInstId()%>";
		data['dealer']="<%=__sessionUser.getUserId() %>";
		data['sponsor'] ="<%=__sessionUser.getUserId() %>";
		data['sponInst']="<%=__sessionUser.getInstId()%>";
		data['dealTransType']="1";//修改后的状态永远为1
		data['sponInst']="<%=__sessionInstitution.getInstId()%>";
		var params=mini.encode(data);
		CommonUtil.ajax({
			url:"/IfsCfetsmetalGoldController/saveGold",
			data:params,
			callback:function(data){
				mini.alert(data,'提示信息',function(){
					if(data=="保存成功"||data=="修改成功"){
						top["win"].closeMenuTab();
					}
				});
				
			}
		});
	}
	function close(){
		top["win"].closeMenuTab();
	}
	$(document).ready(function(){
		inme();
	});
	
	
	function inme() {
		if (action == "detail" || action == "edit"||action=="approve"||action=="print") {
			var from = new mini.Form("field_form");
			CommonUtil.ajax({
				url : '/IfsCfetsmetalGoldController/searchMetalGold',
				data:{ticketId:ticketId},
				callback : function(text) {
					from.setData(text.obj);
					//投资组合
					mini.get("port").setValue(text.obj.port);
					mini.get("port").setText(text.obj.port);
					//成本中心
					mini.get("cost").setValue(text.obj.cost);
					mini.get("cost").setText(text.obj.cost);
					var product=mini.get("product").getValue();
					if(product==null||product==""){
						//产品代码
						mini.get("product").setValue("GOLD");
						mini.get("product").setText("GOLD");
					}else{
						//产品代码
						mini.get("product").setValue(text.obj.product);
						mini.get("product").setText(text.obj.product);
					}
					var prodType=mini.get("prodType").getValue();
					if(prodType==null||prodType==""){
						//产品类型
						mini.get("prodType").setValue("FX");
						mini.get("prodType").setText("FX");
					}else{
						//产品类型
						mini.get("prodType").setValue(text.obj.prodType);
						mini.get("prodType").setText(text.obj.prodType);
					}
					
					mini.get("counterpartyInstId").setValue(text.obj.counterpartyInstId);
					queryTextName(text.obj.counterpartyInstId);
					mini.get("instId").setValue(text.obj.instfullname);
					mini.get("dealer").setValue(text.obj.myUserName);
					
				}
			}); 
			if (action == "detail"||action=="approve") {
				mini.get("save_btn").hide();
				from.setEnabled(false);
				var form1=new mini.Form("approve_operate_form");
				form1.setEnabled(true);
				mini.get("upload_btn").setEnabled(false);
			} 
			if(action=="print"){
				from.setEnabled(false);
				$("#fileMsg").hide();
				mini.get("splitter").hidePane(2);
				
				log_grid.show();
				CommonUtil.ajax({
					url:"/IfsFlowController/getOneFlowDefineBaseInfo",
					data:{serial_no:ticketId},
					callerror: function(data){
						ApproveFlowLog.loadLogInfo();
					},
					callback : function(data) {
						var innerInterval;
						innerInitFunction = function(){
							clearInterval(innerInterval);
							ApproveFlowLog.loadLogInfo();
						},
						innerInterval = setInterval("innerInitFunction()",100);
					}
				});
			}
			
			mini.get("contractId").setEnabled(false);
			
		}else if(action=="add"){
			mini.get("dealTransType").setValue("1");
			mini.get("instId").setValue("<%=__sessionInstitution.getInstFullname()%>");
			mini.get("dealer").setValue("<%=__sessionUser.getUserName() %>");
			mini.get("sponsor").setValue("<%=__sessionUser.getUserId()%>");
			mini.get("sponInst").setValue("<%=__sessionUser.getInstId() %>");
			$("#fileMsg").hide();
			//产品代码
			mini.get("product").setValue("GOLD");
			mini.get("product").setText("GOLD");
			//产品类型
			mini.get("prodType").setValue("FX");
			mini.get("prodType").setText("FX");
		}
		mini.get("dealTransType").setEnabled(false);
	}
	
	mini.get("forDate").setValue("<%=__bizDate %>");
	mini.get("instId").setEnabled(false);
	mini.get("dealer").setEnabled(false);	
	var sss=mini.get("forDate").getValue();
	var result = "";
	result += sss.getFullYear() + "-";
	result += CommonUtil.singleNumberFormatter(sss.getMonth()+1) + "-"; 
	result += CommonUtil.singleNumberFormatter(sss.getDate()+3);
	mini.get("valueDate1").setValue(result);
	
	
	var timestamp = Date.parse(new Date());
	sys(timestamp);
	function sys(stamp){
		var time = new Date(stamp);
		var result = "";
		result += CommonUtil.singleNumberFormatter(time.getHours()) + ":"; 
		result += CommonUtil.singleNumberFormatter(time.getMinutes()) + ":";
		result += CommonUtil.singleNumberFormatter(time.getSeconds());
		mini.get("forTime").setValue(result);
	}
	
	//文、数字、下划线 的验证
	function onEnglishAndNumberValidations(e) {
		if(e.value == "" || e.value == null){//值为空，就不做校验
			return;
		}
		if (e.isValid) {
			if (isEnglishAndNumbers(e.value) == false) {
				e.errorText = "必须输入英文小写+数字";
				e.isValid = false;
			}
		}
	}
	/* 是否英文+数字 */
	function isEnglishAndNumbers(v) {
		var re = new RegExp("^[0-9a-z\_\-]+$");
		if (re.test(v)) return true;
		return false;
	}	
	
	
	
	 /* 是否英文+数字 */
    function isEnglishAndNumber(v) {
        
        var re = new RegExp("^[0-9a-zA-Z\_]+$");
        if (re.test(v)) return true;
        return false;
    }
    
    //交易方向  值改变时发生
function  dirVerify(e){
    if(e.value == "P"){//买入
        mini.get("direction2").setValue("S");
    }else if(e.value == "S"){//卖出
        mini.get("direction2").setValue("P");
    }
}

   function onEnglishAndNumberValidation(e) {
       if (e.isValid) {
           if (isEnglishAndNumber(e.value) == false) {
               e.errorText = "必须输入英文+数字";
               e.isValid = false;
           }
       }
   }
 function onCurrencyPairChange(e){
		var currencyPair = mini.get("currencyPair").getText();
		/***start*******************************/
		var cp = parseCurrencyPair(currencyPair);
		mini.get("opicsccy").setValue(cp.ccy);//币种1  存放直通opics的主要币种
		mini.get("opicsctrccy").setValue(cp.ctr);//币种2  存放直通opics的次要币种
		/***end*******************************/
		var arr=new Array();
		arr=currencyPair.split("/");
		var buyDirection = mini.get("direction1").getValue();
		if(buyDirection=='P'){
			mini.get("direction2").setValue("S");
			mini.get("buyCurreny").setValue(arr[0]);
			mini.get("sellCurreny").setValue(arr[1]);
		}else if(buyDirection=='S'){
			mini.get("direction2").setValue("P");
			mini.get("buyCurreny").setValue(arr[1]);
			mini.get("sellCurreny").setValue(arr[0]);
		}
		
	}
 function onButtonEdit(e) {
	    var btnEdit = this;
	    mini.open({
	        url: CommonUtil.baseWebPath() + "../../Common/CustMini.jsp",
	        title: "选择对手方列表",
	        width: 900,
	        height: 600,
	        ondestroy: function (action) {
	            if (action == "ok") {
	                var iframe = this.getIFrameEl();
	                var data = iframe.contentWindow.GetData();
	                data = mini.clone(data);    //必须
	                if (data) {
	                    btnEdit.setValue(data.cno);
	                    btnEdit.setText(data.cliname);
	                    btnEdit.focus();
	                }
	            }

	        }
	    });
	}

	//根据交易对手编号查询全称
	function queryTextName(cno){
		CommonUtil.ajax({
		    url: "/IfsOpicsCustController/searchIfsOpicsCust",
		    data : {'cno' : cno},
		    callback:function (data) {
		    	if (data && data.obj) {
		    		mini.get("counterpartyInstId").setText(data.obj.cliname);
				} else {
					mini.get("counterpartyInstId").setText();
		    }
		    }
		});
	}

	function quantityMoney(){
		var quantity = mini.get("quantity").getValue();//本方金额
		var prix = mini.get("prix").getValue();//利率
		var mul = CommonUtil.accMul(quantity,prix);//本方金额x利率
		var result = CommonUtil.accDiv(mul,1);
		mini.get("amount1").setValue(result);//设置对方金额
		
	}
		
	//解析货币对
	function parseCurrencyPair(cp){
		var ccy = cp.substr(0,3);
		var ctr = cp.substr(4,7);
		var data = {ccy:ccy,ctr:ctr};
		return data;
	}
	//起息日
	function onDrawDate(e) {
        var date = e.date;
        var d = mini.get("forDate").getValue();
        if (date.getTime() <= d.getTime()+2*24*3600*1000) {
            e.allowSelect = false;
        }
    }
	
//根据英文节点类型返回相应中文
	ApproveFlowLog.ActivityType = function(type){
		var result = null;
		switch(type){
			case "startEvent":
				result = "开始节点";
				break;
			case "exclusiveGateway":
				result = "决策节点";
				break;
			case "userTask":
				result = "人工节点";
				break;	
			case "endEvent":
				result = "结束节点";
				break;	
			default:
				result = type;
		}
		return result;
	};

	function stampToTimeRenderer(e) {
		if(e.value){
			return CommonUtil.stampToTime(e.value);	
		}else{
			return "";
		}
	}

	function timerFormatRenderer(e) {
		if(e.value){
			return CommonUtil.secondFormatter(e.value/1000);
		}else{
			return "";
		}
	}

	function activityTypeRenderer(e){
		if(e.value){
			return e.value;
		}else{
			return ApproveFlowLog.ActivityType(e.value);
		}
	}
	
</script>
<script type="text/javascript" src="../../Common/Flow/MiniApproveOpCommon.js"></script>			
</body>
</html>
