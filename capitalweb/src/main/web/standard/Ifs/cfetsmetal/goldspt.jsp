<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
<title></title>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
	<legend>黄金即期成交单查询</legend>	
	<div>
		<div id="search_form" style="width:100%" cols="6">
			<nobr>
			<input id="startDate" name="startDate" class="mini-datepicker" labelField="true" label="交易日期："
						ondrawdate="onDrawDateStart" labelStyle="text-align:right;" emptyText="起始日期" format="yyyy-MM-dd" value="<%=__bizDate%>"/>
			<span>~</span>
			<input id="endDate" name="endDate" class="mini-datepicker" 
						ondrawdate="onDrawDateEnd" emptyText="结束日期" format="yyyy-MM-dd" value="<%=__bizDate%>"/>
			</nobr>
			<input id="valueDate" name="valueDate" class="mini-datepicker"  labelField="true" label="起息日期：" emptyText="请填写起息日期" labelStyle="text-align:right;" width="280px"format="yyyy-MM-dd" />
			<input id="contractId" name="contractId" class="mini-textbox" labelField="true"  label="成交单编号：" labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请输入成交单编号" width="280px" />
			<!-- <input id="forDate" name="forDate" class="mini-datepicker" labelField="true"  label="成交日期："   labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请输入成交日期" /> -->
			<input id="approveStatus" name="approveStatus" class="mini-combobox" data="CommonUtil.serverData.dictionary.ApproveSta" width="280px" emptyText="请选择审批状态" labelField="true"  label="审批状态：" labelStyle="text-align:right;"/>
			<input id="dealNo" name="dealNo" class="mini-textbox" labelField="true" label="opics交易号：" emptyText="请填写opics交易号" labelStyle="text-align:right;" width="280px"/>
			<span style="float: right; margin-right: 150px"> 
				<a id="search_btn" class="mini-button" style="display: none"   onclick="search(10,0)">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
			</span>
		</div>
	</div>
</fieldset> 
<%@ include file="../batchReback.jsp"%>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script> 	  
<div class="mini-fit" style="margin-top: 2px;">
	<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:70%;" idField="id"  allowAlternating="true" allowResize="true" sortMode="client" allowAlternating="true" 
	multiSelect="true" onrowdblclick="onRowDblClick">
		<div property="columns">
		<div type="checkcolumn"></div>
			<div type="indexcolumn" width="50px" headerAlign="center">序号</div>
			<div field="prdNo" width="100" allowSort="false" headerAlign="center" align="center" visible="false"></div>
			<div field="forDate" width="120px" align="center"  headerAlign="center" allowSort="true">交易日期</div>
			<div field="ticketId" width="200px" allowSort="false" headerAlign="center" align="center">审批单号</div>
			<div field="contractId" width="180px" align="center"  headerAlign="center" >成交单编号</div>
			<div field="taskName" width="120" allowSort="false" headerAlign="center" align="center">审批状态</div> 
			<div field="dealNo" width="120" allowSort="false" headerAlign="center" align="center">OPICS交易号</div>  
			<div field="statcode" width="100px" align="center"  headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'statcode'}">opics处理状态</div>
			<div field="currencyPair" width="100px" align="center"  headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'CurrencyPair'}">货币对</div>
			<div field="quantity" width="150px" align="right"  headerAlign="center" numberFormat="n4">数量</div>
			<div field="prix" width="150px" align="right"  headerAlign="center" numberFormat="#,0.0000">价格</div>
			<div field="amount1" width="150px" align="right"  headerAlign="center" allowSort="true" numberFormat="#,0.0000">金额</div>
			<div field="direction1" width="120px" align="center"  headerAlign="center" allowSort="true" renderer="CommonUtil.dictRenderer" data-options="{dict:'trading'}">交易方向</div>
			<div field="valueDate1" width="120px" align="center"  headerAlign="center" allowSort="true">起息日</div>
			<div field="instId" width="150px" align="center"  headerAlign="center" >本方机构</div>                            
			<div field="counterpartyInstId" width="150px" align="center"  headerAlign="center" >对方机构</div>                            
			<div field="settleMode" width="120px" align="center"  headerAlign="center" allowSort="true">清算方式</div>
			<div field="sponsor" width="120" allowSort="false" headerAlign="center" align="center">审批发起人</div>
			<div field="sponInst" width="120" allowSort="false" headerAlign="center" align="center">审批发起机构</div>
			<div field="aDate" width="180" allowSort="false" headerAlign="center" align="center">审批发起时间</div>
		</div>
	</div>
	<fieldset style="width: 100%;border:solid 1px #aaa;margin-top:8px;position:relative;">
		<legend>黄金即期详情</legend>
			<div id="MiniSettleForeigDetail" style="padding:5px;" class="mini-fit area" tyle="width: 100%; height: 50%;" allowAlternating="true" allowResize="true" border="true" sortMode="client">
			<input class="mini-hidden" name="id"/>
				<div class="mini-panel" title="成交双方信息" style="width:100%;"  allowResize="true" collapseOnTitleClick="true">
					<div class="leftarea">
						<input id="instId"  name="instId" label="本方机构：" class="mini-textbox" labelField="true" vtype="maxLength:50" style="width:100%;" labelStyle="text-align:left;width:130px;"enabled="false"/>
						<input id="dealer"  name="myUserName" label="本方交易员：" class="mini-textbox" labelField="true" vtype="maxLength:10" style="width:100%;" labelStyle="text-align:left;width:130px;"enabled="false"/>
					</div>
					<div class="rightarea">
						<input id="counterpartyInstId"  name="counterpartyInstId" label="对方机构：" class="mini-textbox mini-mustFill" onbuttonclick="onButtonEdit" allowInput="false" labelField="true" style="width:100%;" labelStyle="text-align:left;width:130px;"  required="true" />
						<input id="counterpartyDealer"  name="counterpartyDealer" label="对方交易员：" class="mini-textbox" labelField="true" vtype="maxLength:6" style="width:100%;"labelStyle="text-align:left;width:130px;"/>
					</div>
				</div>
				<div class="mini-panel" title="交易主体信息" style="width:100%;"  allowResize="true" collapseOnTitleClick="true">
					<div class="leftarea">
						<input id="forDate"  name="forDate" label="成交日期：" class="mini-datepicker" labelField="true" style="width:100%;"labelStyle="text-align:left;width:130px;"/>
						<input style="width:100%;" id="currencyPair"  name="currencyPair" label="货币对：" class="mini-combobox mini-mustFill" required="true"  data="CommonUtil.serverData.dictionary.CurrencyPair" onvaluechanged="onCurrencyPairChange"  labelField="true" labelStyle="text-align:left;width:130px;"/>
						<input style="width:100%;" id="quantity"  name="quantity" label="数量："   onvaluechanged="quantityMoney"  class="mini-spinner mini-mustFill"  required="true"  changeOnMousewheel='false'  maxValue="9999999999999999.9999" format="n4"  labelField="true" labelStyle="text-align:left;width:130px;"/>
						<input style="width:100%;" id="amount1"  name="amount1" label="金额："  class="mini-spinner mini-mustFill" required="true"   changeOnMousewheel='false'  maxValue="9999999999999999.9999" format="n4"  labelField="true" labelStyle="text-align:left;width:130px;"/>
						<input style="width:100%;" id="direction2"  name="direction2" label="对方方向：" enabled="false" vtype="maxLength:6" class="mini-hidden" labelField="true" labelStyle="text-align:left;width:130px;"data = "CommonUtil.serverData.dictionary.trading" />
						<input id="sellCurreny" name="sellCurreny" class="mini-combobox mini-mustFill" required="true"  labelField="true" requiredErrorText="该输入项为必输项" required="true"  label="卖出货币："  style="width:100%;"  labelStyle="text-align:left;width:130px;" data="CommonUtil.serverData.dictionary.Currency" />
						<input id="tradingModel" name="tradingModel" class="mini-combobox mini-mustFill" required="true"  labelField="true"   label="交易模式：" labelStyle="text-align:left;width:130px;" style="width:100%;"  data="CommonUtil.serverData.dictionary.TradeModel"/>
						<input style="width:100%;" id="settleMode"  name="settleMode" label="清算方式：" vtype="maxLength:13" class="mini-textbox"  labelField="true" labelStyle="text-align:left;width:130px;"/>
					</div>
					<div class="rightarea">
						<input id="forTime"  name="forTime" label="交易时间：" class="mini-timespinner" labelField="true" style="width:100%;"labelStyle="text-align:left;width:130px;"/>
						<input style="width:100%;" id="direction1"  name="direction1" label="交易方向："  onvaluechanged="onCurrencyPairChange" vtype="maxLength:6" class="mini-combobox mini-mustFill" required="true"   labelField="true" labelStyle="text-align:left;width:130px;"data = "CommonUtil.serverData.dictionary.trading" />
						<input style="width:100%;" id="prix"  name="prix" label="价格："   onvaluechanged="quantityMoney"  class="mini-spinner mini-mustFill" required="true"   changeOnMousewheel='false'  maxValue="9999999999999999.9999" format="n4"  labelField="true" labelStyle="text-align:left;width:130px;"/>
						<input style="width:100%;" id="valueDate1"  name="valueDate1" label="起息日：" class="mini-datepicker mini-mustFill" labelField="true" required="true"   labelStyle="text-align:left;width:130px;"/>
						<input id="buyCurreny" name="buyCurreny" class="mini-combobox mini-mustFill" required="true"  labelField="true" requiredErrorText="该输入项为必输项" required="true"  label="买入货币："  style="width:100%;"  labelStyle="text-align:left;width:130px;" data="CommonUtil.serverData.dictionary.Currency" />
						<input id="tradingType" name="tradingType" class="mini-combobox mini-mustFill" labelField="true" requiredErrorText="该输入项为必输项" required="true"  label="交易方式："  value="RFQ" labelStyle="text-align:left;width:130px;" style="width:100%;" data="CommonUtil.serverData.dictionary.TradeType"/>
					</div>
				</div>
				<%@ include file="../../Common/opicsLessDetail.jsp"%>
				
		    </div>
	</fieldset>
</div>   

<script>
	mini.parse();

	var url = window.location.search;
	var prdNo = CommonUtil.getParam(url, "prdNo");
	var prdName = CommonUtil.getParam(url, "prdName");
	
	var form = new mini.Form("#search_form");
	var grid=mini.get("datagrid");
	//mini.get("applyNo").setVisible(false);//信用风险审查单号：
	//mini.get("applyProd").setVisible(false);//产品名称
	var row="";
	/**************************************点击下面显示详情开始******************************/
	var from = new mini.Form("MiniSettleForeigDetail");
	from.setEnabled(false);
	var grid = mini.get("datagrid");
	//grid.load();
	 //绑定表单
    var db = new mini.DataBinding();
    db.bindForm("MiniSettleForeigDetail", grid);
	$(document).ready(function () {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn){
		});
	});
    /**************************************点击下面显示详情结束******************************/
	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});

	//查看详情
	function onRowDblClick(e) {
		var grid = mini.get("datagrid");
		var row = grid.getSelected();
		if(row){
			var url = CommonUtil.baseWebPath() + "/cfetsmetal/goldsptEdit.jsp?action=detail&ticketid="+row.ticketId+"&prdNo="+prdNo;
			var tab = { id: "MiniGoldSptDetail", name: row.ticketId, title: "黄金即期详情", url: url ,showCloseButton:true};
			var paramData = {selectData:row};
			CommonUtil.openNewMenuTab(tab,paramData);

		} else {
			mini.alert("请选中一条记录！","消息提示");
		}
	}
	//清空
	function clear(){
		var form=new mini.Form("search_form");
		form.clear();
		search(grid.pageSize,0);
	}
	//添加
	function add(){
		var url = CommonUtil.baseWebPath() +"/cfetsmetal/goldsptEdit.jsp?action=add&prdNo="+prdNo;
		var tab = {id: "IrsSptAdd",name:"IrsSptAdd",url:url,title:"黄金即期成交单补录",
					parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
		var paramData = {selectData:""};
		CommonUtil.openNewMenuTab(tab,paramData);
	}
	
	//修改
	function edit(){
		var row = grid.getSelected();
		var selections = grid.getSelecteds();
		if(selections.length>1){
			mini.alert("系统不支持多条数据进行编辑","消息提示");
			return;
		}
		if(row){
			var url = CommonUtil.baseWebPath() + "/cfetsmetal/goldsptEdit.jsp?action=edit&ticketid="+row.ticketId+"&prdNo="+prdNo;
			var tab = { id: "MiniGoldSptEdit", name: "MiniGoldSptEdit", title: "黄金即期修改", url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
			var paramData = {selectData:row};
			CommonUtil.openNewMenuTab(tab,paramData);
		} else {
			mini.alert("请选中一条记录！","消息提示");
		}
	}
	//删除
	function del(){
		var rows=grid.getSelecteds();
		var selections = grid.getSelecteds();
		if(selections.length>1){
			mini.alert("系统不支持多条数据进行删除","消息提示");
			return;
		}
		if(rows.length==0){
			mini.alert("请选中一行","提示");
			return;
		}
		mini.confirm("您确认要删除选中记录?","系统警告",function(value){   
			if (value=='ok'){   
				var data=rows[0];
				params=mini.encode(data);
				CommonUtil.ajax( {
					url:"/IfsCfetsmetalGoldController/deleteCfetsmetalGold",
					data:params,
					callback : function(data) {
						mini.alert("删除成功.","系统提示");
						search(10,0);
					}
				});
			}
		});
	}
	
	grid.on("select",function(e){
		grid.on("select",function(e){
			var rows=grid.getSelecteds();
			for(var i=0;i<rows.length;i++){
				if(rows[i].approveStatus != "3"){
					mini.get("opics_check_btn").setEnabled(false);
					break;
				}
			}
		});
		var row=e.record;
		mini.get("approve_mine_commit_btn").setEnabled(false);
		mini.get("approve_commit_btn").setEnabled(false);
		mini.get("edit_btn").setEnabled(false);
		mini.get("delete_btn").setEnabled(false);
		mini.get("approve_log").setEnabled(true);
		mini.get("recall_btn").setEnabled(true);
		mini.get("batch_commit_btn").setEnabled(true);
		if(row.approveStatus == "3"){//新建
			mini.get("edit_btn").setEnabled(true);
			mini.get("delete_btn").setEnabled(true);
			mini.get("approve_mine_commit_btn").setEnabled(true);
			mini.get("approve_commit_btn").setEnabled(false);
			mini.get("reback_btn").setEnabled(true);
			mini.get("recall_btn").setEnabled(false);
			mini.get("opics_check_btn").setEnabled(true);
		}
		if( row.approveStatus == "6" || row.approveStatus == "7" || row.approveStatus == "8" || row.approveStatus == "17"){//审批通过：6    审批拒绝:5
			mini.get("edit_btn").setEnabled(false);
			mini.get("delete_btn").setEnabled(false);
			mini.get("approve_mine_commit_btn").setEnabled(false);
			mini.get("approve_commit_btn").setEnabled(true);
			mini.get("batch_approve_btn").setEnabled(false);
			mini.get("batch_commit_btn").setEnabled(false);
			mini.get("opics_check_btn").setEnabled(false);
			mini.get("reback_btn").setEnabled(false);
			mini.get("recall_btn").setEnabled(false);
		}
		if(row.approveStatus == "5"){//审批中:5
			mini.get("edit_btn").setEnabled(false);
			mini.get("delete_btn").setEnabled(false);
			mini.get("approve_mine_commit_btn").setEnabled(false);
			mini.get("approve_commit_btn").setEnabled(true);
			mini.get("batch_commit_btn").setEnabled(false);
			mini.get("reback_btn").setEnabled(false);
			mini.get("recall_btn").setEnabled(true);
			mini.get("opics_check_btn").setEnabled(false);
		}
	});

		function getData(action) {
			var row = null;
			if (action != "add") {
				row = grid.getSelected();
			}
			return row;
		}
	function search(pageSize,pageIndex){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统也提示");
			return;
		}

		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		data['type']="spt";
		var url=null;

		var approveType = mini.get("approveType").getValue();
		if(approveType == "mine"){
			url = "/IfsCfetsmetalGoldController/searchMetalGoldSwapMine";
		}else if(approveType == "approve"){
			url = "/IfsCfetsmetalGoldController/searchPageMetalGoldUnfinished";
		}else{
			url = "/IfsCfetsmetalGoldController/searchPageMetalGoldFinished";
		}
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:url,
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	
	/**************************审批相关****************************/
	//审批日志查看
	function appLog(selections){
		var flow_type = Approve.FlowType.VerifyApproveFlow;
		if(selections.length <= 0){
			mini.alert("请选择要操作的数据","系统提示");
			return;
		}
		if(selections.length > 1){
			mini.alert("系统不支持多笔操作","系统提示");
			return;
		}
		if(selections[0].tradeSource == "3"){
			mini.alert("初始化导入的业务没有审批日志","系统提示");
			return false;
		}
		Approve.approveLog(flow_type,selections[0].ticketId);
	};
	//提交正式审批、待审批
	function verify(selections){
		if(selections.length == 0){
			mini.alert("请选中一条记录！","消息提示");
			return false;
		}else if(selections.length > 1){
			mini.alert("该功能不支持多笔提交，如需多笔提交请点击[批量提交]按钮","系统提示");
			return false;
		}
		if(CommonUtil.isNull(selections[0]["dealSource"])||CommonUtil.isNull(selections[0]["port"])||CommonUtil.isNull(selections[0]["cost"])||
	   			 CommonUtil.isNull(selections[0]["product"])||CommonUtil.isNull(selections[0]["prodType"])){
		   		 mini.alert("请先进行opics要素检查更新!","提示");
		   		 return;
	   	}
		if(selections[0]["approveStatus"] != "3" ){
			var searchByIdUrl = "/IfsCfetsrmbDpController/searchCfetsrmbDpAndFlowIdById";
			   Approve.goToApproveJsp(selections[0].taskId,"447","/cfetsmetal/goldsptEdit.jsp", target);
			var target = Approve.approvePage;
			var openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid="+row.ticketId+"&prdNo=goldspt";
			var id="GoldSptApprove";
			var title="黄金即期审批";
			var tab = {"id": id,name:id,url:openJspUrl,title:title,parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
			var paramData = {selectData:selections[0]};
			CommonUtil.openNewMenuTab(tab,paramData);
			// top["win"].showTab(tab);
		}else{
			Approve.approveCommit(Approve.FlowType.VerifyApproveFlow,selections[0]["ticketId"],Approve.OrderStatus.New,"IfsMetalGoldService",prdNo,function(){
				search(grid.pageSize,grid.pageIndex);
			});
		}
	};
	//审批
	function approve(){
		mini.get("approve_commit_btn").setEnabled(false);
		var messageid = mini.loading("系统正在处理...", "请稍后");
		try {
			verify(grid.getSelecteds());
		} catch (error) {
			
		}
		mini.hideMessageBox(messageid);	
		mini.get("approve_commit_btn").setEnabled(true);
	}
	//提交审批
	function commit(){
		mini.get("approve_mine_commit_btn").setEnabled(false);
		var messageid = mini.loading("系统正在处理...", "请稍后");
		try {
			verify(grid.getSelecteds());
		} catch (error) {
			
		}
		mini.hideMessageBox(messageid);	
		mini.get("approve_mine_commit_btn").setEnabled(true);
	}
	//审批日志
	function searchlog(){
		appLog(grid.getSelecteds());
	}
	//打印
   function print(){
		var selections = grid.getSelecteds();
		if(selections == null || selections.length == 0){
		    mini.alert('请选择一条要打印的数据！','系统提示');
		    return false;
		}else if(selections.length>1){
		    mini.alert('暂不支持多条数据同时打印，请选择一条要打印的数据！','系统提示');
		    return false;
		}
			var canPrint = selections[0].ticketId+'|'+prdNo+'|'+prdName;
			if(!CommonUtil.isNull(canPrint)){
				openFullScreen ("../../Common/print/printContractApprove.jsp?ticketId="+canPrint) ;
		};
	}
 //交易日期
	function onDrawDateStart(e) {
       var startDate = e.date;
       var endDate= mini.get("endDate").getValue();
       if(CommonUtil.isNull(endDate)){
       	return;
       }
       if (endDate.getTime() < startDate.getTime()) {
           e.allowSelect = false;
       }
   }
	
	function onDrawDateEnd(e) {
       var endDate = e.date;
       var startDate = mini.get("startDate").getValue();
       if(CommonUtil.isNull(startDate)){
       	return;
       }
       if (endDate.getTime() < startDate.getTime()) {
           e.allowSelect = false;
       }
   }
</script>
</body>
</html>
