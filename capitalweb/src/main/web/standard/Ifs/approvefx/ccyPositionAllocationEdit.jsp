<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<html>
  <head>
    <title>外币头寸调拨</title>
  </head>
<body style="width:100%;height:100%;background:white">
		<div class="mini-splitter" style="width:100%;height:100%;">
				<div size="90%" showCollapseButton="false">
				<h1 style="text-align:center"><strong>外币头寸调拨</strong></h1>
	<div  id="field_form"  class="mini-fit area" style="background:white" >
	<input id="sponsor" name="sponsor" class="mini-hidden" />
	<input id="sponInst" name="sponInst" class="mini-hidden" />
	<input id="aDate" name="aDate" class="mini-hidden"/>
	<input id="dealTransType" name="dealTransType" class="mini-hidden"/>
	<input id="postDate" name="postDate" class="mini-datepicker"  labelField="true" width="30%" style="font-size:18px;float:left;margin-left: 100px;"   label="报送日期：" required="true"  labelStyle="text-align:left;" />
	<input id="ticketId" name="ticketId" class="mini-textbox" labelField="true" width="35%" style="font-size:18px; margin-left: 200px;" label="审批单编号：" emptyText="由系统自动生成"  labelStyle="text-align:left;width:120px" enabled="false"/>
	
	<div class="centerarea">
	  <fieldset>
	  	<legend>交易明细</legend>
	  	<input id="type" name="type" class="mini-combobox" labelField="true"  label="Type" data="CommonUtil.serverData.dictionary.AllotType"  labelStyle="text-align:left;width:170px" style="width:49%;"/>
		<input id="cal" name="cal" class="mini-textbox" labelField="true"  label="Cal"  labelStyle="text-align:left;width:170px" maxLength="15"   style="width:49%;"/>
		<input id="payDate" name="payDate" class="mini-datepicker" labelField="true"  label="Pay date"  labelStyle="text-align:left;width:170px"  style="width:49%;" />
		<input id="ccy" name="ccy" class="mini-combobox" labelField="true"  label="Ccy" data="CommonUtil.serverData.dictionary.Currency" labelStyle="text-align:left;width:170px"  style="width:49%;" />
        <input id="amount" name="amount" class="mini-spinner" labelField="true"  label="Amount" changeOnMousewheel='false'  labelField="true"  format="n4" maxValue="99999999999999999.99999999"   labelStyle="text-align:left;width:170px" style="width:49%;"/>
        <input id="rule" name="rule" class="mini-combobox" labelField="true"  label="Rule" data="CommonUtil.serverData.dictionary.AllotRule" labelStyle="text-align:left;width:170px"   style="width:49%;" />
		<input id="discount" name="discount" class="mini-spinner" labelField="true"  label="Discount" changeOnMousewheel='false'  labelField="true"  format="n8" maxValue="999999999999999999.999999999999"  labelStyle="text-align:left;width:170px"  style="width:49%;" />
		<input id="payType" name="payType" class="mini-textbox" labelField="true"  label="Pay type" labelStyle="text-align:left;width:170px"  maxLength="15" style="width:49%;" />
		<input id="relatedTrade" name="relatedTrade" class="mini-textbox" labelField="true"  label="Related trade"  labelStyle="text-align:left;width:170px"  maxLength="15" style="width:49%;" />
		<input id="relextId" name="relextId" class="mini-textbox" labelField="true"  label="Rel ext ID"  labelStyle="text-align:left;width:170px"  style="width:49%;"  maxLength="15"/>
		<input id="callAccount" name="callAccount" class="mini-textbox" labelField="true"  label="Call account"  labelStyle="text-align:left;width:170px"  style="width:49%;" maxLength="15" />
		<input id="cpty" name="cpty" class="mini-textbox" labelField="true"  label="Cpty"  labelStyle="text-align:left;width:170px"  style="width:49%;"  maxLength="15"/>
		<input id="broker" name="broker" class="mini-textbox" labelField="true"  label="Broker"  labelStyle="text-align:left;width:170px"  style="width:49%;"  maxLength="15"/>
	  </fieldset>
	  </div>
	  <%@ include file="../../Common/opicsApprove.jsp"%>
	  <%@ include file="../../Common/Flow/MiniApproveOpCommon.jsp"%>
</div>	
</div>
<div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
		<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="save_btn"  onclick="save">保存交易</a></div>
		<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="close_btn" onclick="close">关闭界面</a></div>
	</div>
		</div>
		<script type="text/javascript">
			mini.parse();
			//获取当前tab
			var currTab = top["win"].tabs.getActiveTab();
			var params = currTab.params;
			var row=params.selectData;
			var url = window.location.search;
			var action = CommonUtil.getParam(url, "action");
			var ticketId = CommonUtil.getParam(url, "ticketid");
			mini.get("payDate").setValue("<%=__bizDate %>");
			mini.get("postDate").setValue("<%=__bizDate %>");
			var tradeData={};
			
			tradeData.selectData=row;
			tradeData.operType=action;
			tradeData.serial_no=row.ticketId;
			tradeData.task_id=row.taskId;
			
			function inme() {
				if (action == "detail" || action == "edit"||action=="approve") {
					var from = new mini.Form("field_form");
					CommonUtil.ajax({
						url : '/IfsApproveCurrencyController/searchIfsAllocate',
						data : {
							ticketId : ticketId
						},
						callback : function(text) {
							from.setData(text.obj);
							//投资组合
							mini.get("port").setValue(text.obj.port);
							mini.get("port").setText(text.obj.port);
						}
					})
					if (action == "detail"||action=="approve") {
						mini.get("save_btn").hide();
						from.setEnabled(false);
						var form1=new mini.Form("approve_operate_form");
						form1.setEnabled(true);
					} 
				}else if(action=="add"){
					mini.get("sponsor").setValue("<%=__sessionUser.getUserId()%>");
					mini.get("sponInst").setValue("<%=__sessionUser.getInstId() %>");
					mini.get("dealTransType").setValue("0");
				}
			}

			function save() {
				var form = new mini.Form("field_form");
				form.validate();
				if (form.isValid() == false) {
					return;
				}
				var form = new mini.Form("field_form");
				var data = form.getData(true); //获取表单多个控件的数据
				var json = mini.encode(data); //序列化成JSON
				if (action == "add") {
					CommonUtil.ajax({
						url : "/IfsApproveCurrencyController/addIfsAllocate",
						data : json,
						callback : function(data) {
							if (data.code == 'error.common.0000') {
								mini.alert("保存成功",'提示信息',function(){
									top["win"].closeMenuTab();
								});
							} else {
								mini.alert("保存失败");
					   	    }
						}
					});
				} else if (action = "edit") {
					CommonUtil.ajax({
						url : "/IfsApproveCurrencyController/editIfsAllocate",
						data : json,
						callback : function(data) {
							if (data.code == 'error.common.0000') {
								mini.alert(data.desc, '提示信息', function() {
									top["win"].closeMenuTab();
								});
							} else {
								mini.alert("保存失败");
							}
						}
					})
				}
			}
			
			//文、数字、下划线 的验证
	function onEnglishAndNumberValidations(e) {
		if(e.value == "" || e.value == null){//值为空，就不做校验
			return;
		}
		if (e.isValid) {
			if (isEnglishAndNumbers(e.value) == false) {
				e.errorText = "必须输入英文小写+数字";
				e.isValid = false;
			}
		}
	}
	/* 是否英文+数字 */
	function isEnglishAndNumbers(v) {
		var re = new RegExp("^[0-9a-z\_\-]+$");
		if (re.test(v)) return true;
		return false;
	}	
			
			//英文、数字、下划线 的验证
			function onEnglishAndNumberValidation(e) {
				if(e.value == "" || e.value == null){//值为空，就不做校验
					return;
				}
				if (e.isValid) {
					if (isEnglishAndNumber(e.value) == false) {
						e.errorText = "必须输入英文+数字";
						e.isValid = false;
					}
				}
			}
			/* 是否英文+数字 */
			function isEnglishAndNumber(v) {
				var re = new RegExp("^[0-9a-zA-Z\_\-]+$");
				if (re.test(v)) return true;
				return false;
			}
			function close() {
				top["win"].closeMenuTab();
			}
			$(document).ready(function() {
				inme();
			})
		</script>
			<script type="text/javascript" src="../../Common/Flow/MiniApproveOpCommon.js"></script>			
</body>
</html>
