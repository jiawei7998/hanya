<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<title>外币拆借</title>
</head>
<body style="width:100%;height:100%;background:white">
	<div class="mini-splitter" style="width:100%;height:100%;">
			<div size="90%" showCollapseButton="false">
<h1 style="text-align:center"><strong>外币拆借交易</strong></h1>
	<div  id="field_form"  class="mini-fit area" style="background:white" >
	<input id="sponsor" name="sponsor" class="mini-hidden" />
	<input id="sponInst" name="sponInst" class="mini-hidden" />
	<input id="aDate" name="aDate" class="mini-hidden"/>
	<input id="dealTransType" name="dealTransType" class="mini-hidden"/>
	<input id="postDate" name="postDate" class="mini-datepicker"  labelField="true" width="30%" style="font-size:18px;float:left;margin-left: 100px;"   label="报送日期：" required="true"  labelStyle="text-align:left;" />
	<input id="ticketId" name="ticketId" class="mini-textbox" labelField="true" width="35%" style="font-size:18px; margin-left: 200px;" label="审批单编号：" emptyText="由系统自动生成"  labelStyle="text-align:left;width:120px" enabled="false"/>
	
	<div class="centerarea">
	  <fieldset>
	  	<legend>交易明细</legend>
	  	<input id="counterpartyInstId" name="counterpartyInstId" class="mini-buttonedit" labelField="true"  label="交易对手：" onbuttonclick="onButtonEdit" maxLength="25" labelStyle="text-align:left;" required="true"  />
	  	<input id="currency" name="currency" class="mini-combobox" labelField="true"  data="CommonUtil.serverData.dictionary.Currency" label="货币："   labelStyle="text-align:left;" />
		<input id="rate" name="rate" class="mini-spinner" labelField="true"  label="拆借利率(%)：" onValuechanged="interestAmount" maxValue="999999999999999999.999999999999" changeOnMousewheel='false' required="true"  format="n8" labelStyle="text-align:left;"    />
		<input id="basis" name="basis" class="mini-combobox" labelField="true"  label="计息基准：" onValuechanged="interestAmount"  data="CommonUtil.serverData.dictionary.calIrsDays" labelStyle="text-align:left;" />
		<input id="direction" name="direction" class="mini-combobox" labelField="true"  label="交易方向：" data="CommonUtil.serverData.dictionary.trading" labelStyle="text-align:left;"    />
		<input id="tenor" name="tenor" class="mini-combobox" labelField="true"  label="期限：" data="CommonUtil.serverData.dictionary.Term" labelStyle="text-align:left;"    />
		<input id="valueDate" name="valueDate" class="mini-datepicker" labelField="true" onValuechanged="compareDate" label="起息日：" ondrawdate="onDrawDateStart" required="true"   labelStyle="text-align:left;" />
		<input id="maturityDate" name="maturityDate" class="mini-datepicker" labelField="true" onValuechanged="compareDate" label="到期日：" ondrawdate="onDrawDateEnd" required="true"  labelStyle="text-align:left;"    />
		<input id="occupancyDays" name="occupancyDays" class="mini-spinner" labelField="true" minErrorText="0" label="实际占款天数：" onValuechanged="interestAmount" changeOnMousewheel='false' maxValue="10000" labelStyle="text-align:left;"    />
		<input id="amount" name="amount" class="mini-spinner" labelField="true"  changeOnMousewheel='false'  format="n4" label="拆借金额：" onValuechanged="interestAmount" required="true"  maxValue="99999999999999999.99999999" labelStyle="text-align:left;"    />
		<input id="interest" name="interest" class="mini-spinner" labelField="true"  label="应计利息：" maxValue="99999999999999999.99999999" changeOnMousewheel='false'  format="n4" required="true"  labelStyle="text-align:left;" />
		<input id="repaymentAmount" name="repaymentAmount" class="mini-spinner" labelField="true" changeOnMousewheel='false'  format="n4" label="到期还款金额：" required="true"  maxValue="99999999999999999.99999999" labelStyle="text-align:left;"    />
	  </fieldset>
	</div>
		<%@ include file="../../Common/opicsApprove.jsp"%>
		<%@ include file="../../Common/Flow/MiniApproveOpCommon.jsp"%>
	</div>
</div>
<div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
		<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="save_btn"  onclick="save">保存交易</a></div>
		<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="close_btn"  onclick="close">关闭界面</a></div>
	</div>
	</div>
		<script type="text/javascript">
			mini.parse();
			//获取当前tab
			var currTab = top["win"].tabs.getActiveTab();
			var params = currTab.params;
			var row=params.selectData;
			var url = window.location.search;
			var action = CommonUtil.getParam(url, "action");
			var ticketId = CommonUtil.getParam(url, "ticketid");
			mini.get("postDate").setValue("<%=__bizDate %>");
			
			var timestamp = Date.parse(new Date());
			/* sys(timestamp); */
			function sys(stamp){
				var time = new Date(stamp);
				var result = "";
				result += CommonUtil.singleNumberFormatter(time.getHours()) + ":"; 
				result += CommonUtil.singleNumberFormatter(time.getMinutes()) + ":";
				result += CommonUtil.singleNumberFormatter(time.getSeconds());
				mini.get("forTime").setValue(result);
			}

			
			var tradeData={};
			
			tradeData.selectData=row;
			tradeData.operType=action;
			tradeData.serial_no=row.ticketId;
			tradeData.task_id=row.taskId;
			
			function inme() {
				if (action == "detail" || action == "edit"||action=="approve") {
					var from = new mini.Form("field_form");
					CommonUtil.ajax({
						url : '/IfsApproveCurrencyController/searchCcyLending',
						data : {
							ticketId : ticketId
						},
						callback : function(text) {
							//投资组合
							mini.get("port").setValue(text.obj.port);
							mini.get("port").setText(text.obj.port);
							mini.get("counterpartyInstId").setValue(text.obj.counterpartyInstId);
							queryTextName(text.obj.counterpartyInstId);
							/* mini.get("counterpartyInstId").setText(text.obj.counterpartyInstId); */
							from.setData(text.obj);
						}
					}); 
					if (action == "detail"||action=="approve") {
						mini.get("save_btn").hide();
						from.setEnabled(false);
						var form1=new mini.Form("approve_operate_form");
						form1.setEnabled(true);
					} 
				}else if(action=="add"){
					mini.get("sponsor").setValue("<%=__sessionUser.getUserId()%>");
					mini.get("sponInst").setValue("<%=__sessionUser.getInstId() %>");
					mini.get("dealTransType").setValue("0");
				}
			}
			
			function save() {
				var form = new mini.Form("field_form");
				form.validate();
				if (form.isValid() == false) {
					return;
				}
				var form = new mini.Form("field_form");
				var data = form.getData(true); //获取表单多个控件的数据
				var json = mini.encode(data); //序列化成JSON
				if (action == "add") {
					CommonUtil.ajax({
						url : "/IfsApproveCurrencyController/addCcyLending",
						data : json,
						callback : function(data) {
							if (data.code == 'error.common.0000') {
								mini.alert("保存成功",'提示信息',function(){
									top["win"].closeMenuTab();
								});
							} else {
								mini.alert("保存失败");
					   	    }
						}
					});
				} else if (action = "edit") {
					CommonUtil.ajax({
						url : "/IfsApproveCurrencyController/editCcyLending",
						data : json,
						callback : function(data) {
							if (data.code == 'error.common.0000') {
								mini.alert(data.desc, '提示信息', function() {
									top["win"].closeMenuTab();
								});
							} else {
								mini.alert("保存失败");
							}
						}
					})
				}
			}
			function onButtonEdit(e) {
	            var btnEdit = this;
	            mini.open({
	                url: CommonUtil.baseWebPath() + "../../Common/CustMini.jsp",
	                title: "选择对手方列表",
	                width: 900,
	                height: 600,
	                ondestroy: function (action) {
	                    if (action == "ok") {
	                        var iframe = this.getIFrameEl();
	                        var data = iframe.contentWindow.GetData();
	                        data = mini.clone(data);    //必须
	                        if (data) {
	                            btnEdit.setValue(data.cno);
	                            btnEdit.setText(data.cfn);
	                            btnEdit.focus();
	                        }
	                    }

	                }
	            });

	        }
			//根据交易对手编号查询全称
			function queryTextName(cno){
				CommonUtil.ajax({
				    url: "/IfsOpicsCustController/searchIfsOpicsCust",
				    data : {'cno' : cno},
				    callback:function (data) {
				    	if (data && data.obj) {
				    		mini.get("counterpartyInstId").setText(data.obj.cfn);
						} else {
							mini.get("counterpartyInstId").setText();
				    }
				    }
				});
			}
			
	//文、数字、下划线 的验证
	function onEnglishAndNumberValidations(e) {
		if(e.value == "" || e.value == null){//值为空，就不做校验
			return;
		}
		if (e.isValid) {
			if (isEnglishAndNumbers(e.value) == false) {
				e.errorText = "必须输入英文小写+数字";
				e.isValid = false;
			}
		}
	}
	/* 是否英文+数字 */
	function isEnglishAndNumbers(v) {
		var re = new RegExp("^[0-9a-z\_\-]+$");
		if (re.test(v)) return true;
		return false;
	}
	
			
			//英文、数字、下划线 的验证
			function onEnglishAndNumberValidation(e) {
				if(e.value == "" || e.value == null){//值为空，就不做校验
					return;
				}
				if (e.isValid) {
					if (isEnglishAndNumber(e.value) == false) {
						e.errorText = "必须输入英文+数字";
						e.isValid = false;
					}
				}
			}
			/* 是否英文+数字 */
			function isEnglishAndNumber(v) {
				var re = new RegExp("^[0-9a-zA-Z\_\-]+$");
				if (re.test(v)) return true;
				return false;
			}
			function close() {
				top["win"].closeMenuTab();
			}
			$(document).ready(function() {
				inme();
			});
			//日期判断
            function compareDate() {
                var sta = mini.get("valueDate");
                var end = mini.get("maturityDate");
                //mini.alert(sta.getValue());
               // mini.alert(end.getText());
               /*  if (sta.getValue() > end.getValue() && end.getValue("") && sta.getValue() !=null) {
                    mini.alert("起息日不能大于到期日", "系统提示");
                    return end.setValue(sta.getValue());
                } */
              if(sta.getValue()!=null && sta.getValue("")&& end.getValue("") && end.getValue()!=null){
                    sDate1 = Date.parse(sta.getValue());
                    sDate2 = Date.parse(end.getValue());
                    dateSpan = sDate2 - sDate1;
                    dateSpan = Math.abs(dateSpan);
                    iDays = Math.floor(dateSpan / (24 * 3600 * 1000));
                	mini.get("occupancyDays").setValue(iDays);
                }  
            }
        	//到期日 
        	function onDrawDateEnd(e) {
        		var date = e.date;
        		var d = mini.get("valueDate").getValue();
        		if(d==null || d == ""){
        			return;
        		}

        		if (date.getTime() < d.getTime()) {
        			e.allowSelect = false;
        		}
        	}
            //起息日 
        	function onDrawDateStart(e){
        		var date = e.date;
        		var d = mini.get("maturityDate").getValue();
        		if(d==null || d == ""){
        			return;
        		}

        		if (date.getTime() > d.getTime()) {
        			e.allowSelect = false;
        		}
        	}
			//利率计算
			function interestAmount(){
				var a = mini.get("rate").getValue();//利率
				var b = parseInt(mini.get("basis").getValue());//利率基础
				var c = mini.get("occupancyDays").getValue();//占款天数
				var d = mini.get("amount").getValue();//拆借金额
				if(a!="" && a!=null && b!="" && b!=null && c!="" && c!=null && d!="" && d!=null){
					if(b!=1){
						var e = 365;
					}else{
						var e = 360;
					}
					var interest=Number(a)/Number(100)*Number(c)*Number(d)/parseInt(e);
					mini.get("interest").setValue(interest);
					var allamount = interest + Number(d);
					mini.get("repaymentAmount").setValue(allamount);
				}
			}
		</script>
			<script type="text/javascript" src="../../Common/Flow/MiniApproveOpCommon.js"></script>			
</body>
</html>