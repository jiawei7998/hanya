<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ include file="../../global.jsp"%>
<script type="text/javascript" src="../cfetsrmb/rmbVerify.js"></script>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>	 
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
<title></title>
</head>
<body style="width:100%;height:100%;background:white">
	<div  class="mini-splitter" style="width:100%;height:100%;">
		<div size="90%" showCollapseButton="false">
		<h1 style="text-align:center"><strong>外汇即期事前审批</strong></h1>
		<div  id="field_form"  class="mini-fit area" style="background:white" >
	
			<input id="sponsor" name="sponsor" class="mini-hidden" />
			<input id="sponInst" name="sponInst" class="mini-hidden" />
			<input id="aDate" name="aDate" class="mini-hidden"/>
			<input id="dealTransType" name="dealTransType" class="mini-hidden"/>
			<input id="postDate" name="postDate" class="mini-datepicker"  labelField="true" width="30%" style="font-size:18px;float:left;margin-left: 100px;"   label="报送日期：" required="true"  labelStyle="text-align:left;" />
			<input id="ticketId" name="ticketId" class="mini-textbox" labelField="true" width="35%" style="font-size:18px; margin-left: 200px;" label="审批单编号：" emptyText="由系统自动生成"  labelStyle="text-align:left;width:120px" enabled="false"/>
			
			<div class="centerarea">
			 <fieldset>
			  	<legend>交易明细(SPOT)</legend>	
				<input id="tradingModel" name="tradingModel" class="mini-combobox" labelField="true"  label="交易模式："  value="01" labelStyle="text-align:left;"  data="CommonUtil.serverData.dictionary.TradeModel"/>
				<input id="counterpartyInstId" name="counterpartyInstId" class="mini-buttonedit" onbuttonclick="onButtonEdit" labelField="true"  label="交易对手：" style="width:43.5%;" labelStyle="text-align:left" vtype="maxLength:16"/>
				<input id="tradingType" name="tradingType" class="mini-combobox" labelField="true"  label="交易方式："  value="ESP" labelStyle="text-align:left;" data="CommonUtil.serverData.dictionary.TradeType"/>
			  	<input id="currencyPair" name="currencyPair" class="mini-combobox" labelField="true"  label="货币对："    labelStyle="text-align:left;" data="CommonUtil.serverData.dictionary.CurrencyPair"/>
				<input id="direction" name="direction" class="mini-combobox" labelField="true"  label="交易方向："  labelStyle="text-align:left;"  data="CommonUtil.serverData.dictionary.trading"/>
				<input id="buyAmount" name="buyAmount" class="mini-spinner input-text-strong" labelField="true"  label="交易金额：" maxValue="99999999999999999.99999999" changeOnMousewheel='false' format="n4" labelStyle="text-align:left;"  onvaluechanged="getSellAmount"/>
				<input id="price" name="price" class="mini-spinner" labelField="true"  label="汇率："   format="n8" maxValue="999999999999999999.999999999999" changeOnMousewheel="false" labelStyle="text-align:left;" onvaluechanged="getSellAmount"/>
				<input id="sellAmount" name="sellAmount" class="mini-spinner input-text-strong" labelField="true"  label="反向交易金额："  maxValue="99999999999999999.99999999" changeOnMousewheel='false' format="n4" labelStyle="text-align:left;"    />
				<input id="valueDate" name="valueDate" class="mini-datepicker" labelField="true" label="起息日："  labelStyle="text-align:left;"  ondrawdate="onDrawDate"  />
			  </fieldset>
			</div>
		<%@ include file="../../Common/Flow/MiniApproveOpCommon.jsp"%>
		</div>
		</div>
		<div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
			<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="save_btn"   onclick="save">保存交易</a></div>
			<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="close_btn"   onclick="close">关闭界面</a></div>
		</div>
	</div>
<script type="text/javascript">
	mini.parse();
	//获取当前tab
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var row=params.selectData;
	var url = window.location.search;
	var prdNo = CommonUtil.getParam(url, "prdNo");
	var prdName = CommonUtil.getParam(url, "prdName");
	var dealType = CommonUtil.getParam(url, "dealType");
	var action = CommonUtil.getParam(url, "action");
	var ticketId=CommonUtil.getParam(url, "ticketid");
	
	var _bizDate = '<%=__bizDate %>';
	mini.get("postDate").setValue("<%=__bizDate %>");
	var sss=mini.get("postDate").getValue();
	mini.get("valueDate").setValue(sss);
	
	function sys(stamp){
		var time = new Date(stamp);
		var result = "";
		result += CommonUtil.singleNumberFormatter(time.getHours()) + ":"; 
		result += CommonUtil.singleNumberFormatter(time.getMinutes()) + ":";
		result += CommonUtil.singleNumberFormatter(time.getSeconds());
		mini.get("forTime").setValue(result);
	}
	
	$(document).ready(function () {
		inme();
	});
	
	var tradeData={};
	tradeData.selectData=row;
	tradeData.operType=action;
	tradeData.serial_no=row.ticketId;
	tradeData.task_id=row.taskId;
	function inme(){
		if(action=="detail"||action=="edit"||action=="approve"){
		var from = new mini.Form("field_form");
		CommonUtil.ajax({
			url:'/IfsApproveForeignController/searchSpot',
			data:{ticketId:ticketId},
			callback:function(text){
				mini.get("counterpartyInstId").setValue(text.obj.counterpartyInstId);
				queryTextName(text.obj.counterpartyInstId);
				from.setData(text.obj);
			}
		});
		if(action=="detail"||action=="approve"){
			mini.get("save_btn").hide();
			from.setEnabled(false);
			var form11=new mini.Form("#approve_operate_form");
			form11.setEnabled(true);
		}
		}else if(action=="add"){
			mini.get("sponsor").setValue("<%=__sessionUser.getUserId()%>");
			mini.get("sponInst").setValue("<%=__sessionUser.getInstId() %>");
			mini.get("dealTransType").setValue("0");
		}
	}
	
	function save(){
		var form = new mini.Form("field_form");
		form.validate();
		if (form.isValid() == false) {
			mini.alert("信息填写有误，请重新填写","系统提示");
			return;
		}
		var form = new mini.Form("field_form");            
		var data = form.getData(true);      //获取表单多个控件的数据
		var json = mini.encode(data);   //序列化成JSON
		if(toDecimal(data['sellAmount'])!=toDecimal(data['buyAmount']*data['price'])){
			mini.confirm("确认以当前数据为准吗？","确认",function (action) {
			if (action != "ok") {
				return;
			}
			if(action=="add"){
				CommonUtil.ajax({
				    url: "/IfsApproveForeignController/addSpot",
				    data:json,
				    callback:function (data) {
				    	if (data.code == 'error.common.0000') {
							mini.alert("保存成功",'提示信息',function(){
								top["win"].closeMenuTab();
							});
						} else {
							mini.alert("保存失败");
				    }
				    }
				});
			}else  if(action="edit"){
				CommonUtil.ajax({
					url:"/IfsApproveForeignController/editSpot",
					data:json,
					callback:function(data){
						if (data.code == 'error.common.0000') {
							mini.alert(data.desc,'提示信息',function(){
								top["win"].closeMenuTab();
							});
						} else {
							mini.alert("保存失败");
						}
					}
				});
			}
		});
		}else{
			if(action=="add"){
				CommonUtil.ajax({
					url: "/IfsApproveForeignController/addSpot",
					data:json,
					callback:function (data) {
						if (data.code == 'error.common.0000') {
							mini.alert("保存成功",'提示信息',function(){
								top["win"].closeMenuTab();
							});
						} else {
							mini.alert("保存失败");
						}
					}
				});
			}else  if(action="edit"){
				CommonUtil.ajax({
					url:"/IfsApproveForeignController/editSpot",
					data:json,
					callback:function(data){
						if (data.code == 'error.common.0000') {
							mini.alert(data.desc,'提示信息',function(){
								top["win"].closeMenuTab();
							});
						} else {
							mini.alert("保存失败");
						}
					}
				});
			}
		}
	}
	function close(){
		top["win"].closeMenuTab();
	}
	function onButtonEdit(e) {
		var btnEdit = this;
		mini.open({
			url: CommonUtil.baseWebPath() + "../../Common/CustMini.jsp",
			title: "选择对手方列表",
			width: 900,
			height: 600,
			ondestroy: function (action) {
				if (action == "ok") {
					var iframe = this.getIFrameEl();
					var data = iframe.contentWindow.GetData();
					data = mini.clone(data);    //必须
					if (data) {
						btnEdit.setValue(data.cno);
						btnEdit.setText(data.cfn);
						btnEdit.focus();
					}
				}
			}
		});
	}
	//根据交易对手编号查询全称
	function queryTextName(cno){
		CommonUtil.ajax({
			url: "/IfsOpicsCustController/searchIfsOpicsCust",
			data : {'cno' : cno},
			callback:function (data) {
				if (data && data.obj) {
					mini.get("counterpartyInstId").setText(data.obj.cfn);
				} else {
					mini.get("counterpartyInstId").setText();
				}
			}
		});
	}
	//计算反向交易金额
	function getSellAmount(e){
		var buyAmount = mini.get("buyAmount").getValue();
		var price = mini.get("price").getValue();
		if(buyAmount!=null && buyAmount!="" && price!=null && price!=""){
			var sellAmount=Number(buyAmount)*Number(price);
			mini.get("sellAmount").setValue(sellAmount);
		}
	}
	
	function onDrawDate(e) {
		var date = e.date;
		var d = mini.get("postDate").getValue();
		if (date.getTime() > d.getTime()+2*24*3600*1000) {
			e.allowSelect = false;
		}
	}
	function onCurrencyPairChange(e){
		var currencyPair = mini.get("currencyPair").getText();
		var arr=new Array();
		arr=currencyPair.split("/");
		var direction = mini.get("direction").getValue();
		if(direction=='1'){
			mini.get("buyCurreny").setValue(arr[0]);
			mini.get("sellCurreny").setValue(arr[1]);
		}else if(direction=='2'){
			mini.get("buyCurreny").setValue(arr[1]);
			mini.get("sellCurreny").setValue(arr[0]);
		}
	}
	//文、数字、下划线 的验证
	function onEnglishAndNumberValidation(e) {
		if(e.value == "" || e.value == null){//值为空，就不做校验
			return;
		}
		if (e.isValid) {
			if (isEnglishAndNumber(e.value) == false) {
				e.errorText = "必须输入英文+数字";
				e.isValid = false;
			}
		}
	}
	/* 是否英文+数字 */
	function isEnglishAndNumber(v) {
		var re = new RegExp("^[0-9a-zA-Z\_\-]+$");
		if (re.test(v)) return true;
		return false;
	}	
	
</script>
<script type="text/javascript" src="../../Common/Flow/MiniApproveOpCommon.js"></script>
</body>
</html>