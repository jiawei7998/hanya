<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
<title>货币掉期</title>

</head>
<body style="width: 100%; height: 100%; background: white">
	<fieldset class="mini-fieldset">
		<legend>查询</legend>
		<div id="search_form" style="width: 100%;">
			<input id="ticketId" name="ticketId" class="mini-textbox" labelField="true" label="审批单编号：" emptyText="请输入审批单编号" labelStyle="text-align:right;" width="280px" /> 
			<input id="approveStatus" name="approveStatus" class="mini-combobox" data="CommonUtil.serverData.dictionary.ApproveSta" width="280px" emptyText="请选择审批状态" labelField="true"  label="审批状态：" labelStyle="text-align:right;"/>
			<span style="float: right; margin-right: 150px"> 
				<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
			</span>
		</div>
	</fieldset>
	<%@ include file="../preReback.jsp"%>
	<div id="MiniSettleForeignSpot" class="mini-fit" style="margin-top: 2px;">
		<div id="datagrid" class="mini-datagrid borderAll" style="width: 100%; height: 100%;" allowAlternating="true"
			allowResize="true" onrowdblclick="onRowDblClick" border="true" sortMode="client">
			<div property="columns">
				<div type="indexcolumn" headerAlign="center" width="40">序号</div>
				<div field="ticketId" width="200" allowSort="false" headerAlign="center" align="">审批单编号</div>
				<div field="dealTransType" width="100" allowSort="true" headerAlign="center"  align="center"   renderer="CommonUtil.dictRenderer" data-options="{dict:'dealTransType'}">交易状态</div>
				<div field="approveStatus" width="120" align="center" headerAlign="center"  renderer="CommonUtil.dictRenderer" data-options="{dict:'ApproveStatus'}">审批状态</div>
				<div field="counterpartyInstId" width="200" allowSort="false" headerAlign="center" align="center">交易对手</div>
				<div field="postDate" width="200" allowSort="false" headerAlign="center" align="center">报送日期</div>
				<div field="tradingModel" width="150" allowSort="false" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'TradeModel'}">交易模式</div>
				<div field="tradingType" width="200" allowSort="true" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'TradeType'}">交易方式</div>
				<div field="startDate" width="200" allowSort="false" headerAlign="center" align="center">开始日期</div>
				<div field="unadjustmaturityDate" width="200" allowSort="true" headerAlign="center" align="center">未调整到日期</div>
				<div field="leftDirection" width="150" allowSort="false" headerAlign="center" align="" renderer="CommonUtil.dictRenderer" data-options="{dict:'trading'}">付款方向</div>
				<div field="leftNotion" width="200" allowSort="true" headerAlign="center" align="right" numberFormat="#,0.0000">名义本金（左）</div>
				<div field="rightNotion" width="200" allowSort="true" headerAlign="center" align="right" numberFormat="#,0.0000">名义本金（右）</div>			
				<div field="sponsor" width="150" allowSort="false" headerAlign="center" align="center">审批发起人</div>
				<div field="sponInst" width="150" allowSort="false" headerAlign="center" align="center">审批发起机构</div>
				<div field="aDate" width="200" allowSort="false" headerAlign="center" align="center">审批发起时间</div>
			</div>
	</div>
	</div>
	<script>
		mini.parse();
		var url = window.location.search;
		var prdNo = CommonUtil.getParam(url, "prdNo");
		var prdName = CommonUtil.getParam(url, "prdName");
		var grid = mini.get("datagrid");
		
		
		
		function search(pageSize,pageIndex){
			var form = new mini.Form("#search_form");
			form.validate();
			if(form.isValid()==false){
				mini.alert("信息填写有误，请重新填写","系统也提示");
				return;
			}

			var data=form.getData();
			data['pageNumber']=pageIndex+1;
			data['pageSize']=pageSize;
			data['branchId']=branchId;
			var url=null;

			var approveType = mini.get("approveType").getValue();
			if(approveType == "mine"){
				url = "/IfsApproveCurrencyController/searchPageCcySwapMine";
			}else if(approveType == "approve"){
				url = "/IfsApproveCurrencyController/searchPageCcySwapUnfinished";
			}else{
				url = "/IfsApproveCurrencyController/searchPageCcySwapFinished";
			}

			var params = mini.encode(data);
			CommonUtil.ajax({
				url:url,
				data:params,
				callback : function(data) {
					grid.setTotalCount(data.obj.total);
					grid.setPageIndex(pageIndex);
			        grid.setPageSize(pageSize);
					grid.setData(data.obj.rows);
				}
			});
		}
		//增删改查
		function add(){
			var url = CommonUtil.baseWebPath() + "/approvefx/ccyswapEdit.jsp?action=add";
			var tab = { id: "MiniCcySwapAdd", name: "MiniCcySwapAdd", title: "货币掉期添加", url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
			var paramData = {selectData:""};
			CommonUtil.openNewMenuTab(tab,paramData);
		}
		function query() {
            search(grid.pageSize, 0);
        }
		function clear(){
			var form=new mini.Form("search_form");
            form.clear();
            search(10,0);
		}
		function edit()
		{
			var row = grid.getSelected();
			if(row){
				var url = CommonUtil.baseWebPath() + "/approvefx/ccyswapEdit.jsp?action=edit&ticketid="+row.ticketId;
				var tab = { id: "MiniCcySwapEdit", name: "MiniCcySwapEdit", title: "货币掉期修改", url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
				var paramData = {selectData:""};
			    CommonUtil.openNewMenuTab(tab,paramData);

			} else {
				mini.alert("请选中一条记录！","消息提示");
			}
		}

		function onRowDblClick(e) {
			var grid = mini.get("datagrid");
			var row = grid.getSelected();
			if(row){
					var url = CommonUtil.baseWebPath() + "/approvefx/ccyswapEdit.jsp?action=detail&ticketid="+row.ticketId;
					var tab = { id: "MiniCcySwapDetail", name: "MiniCcySwapDetail", title: "货币掉期详情", url: url ,showCloseButton:true};
					var paramData = {selectData:""};
					CommonUtil.openNewMenuTab(tab,paramData);

			} else {
				mini.alert("请选中一条记录！","消息提示");
			}
		}
		//删除
		function del() {
				var grid = mini.get("datagrid");
				var row = grid.getSelected();
				if (row) {
					mini.confirm("您确认要删除选中记录?","系统警告",function(value){
						if(value=="ok"){
						CommonUtil.ajax({
							url: "/IfsApproveCurrencyController/deleteCcySwap",
							
							data: {ticketid: row.ticketId},
							callback: function (data) {
								if (data.code == 'error.common.0000') {
									mini.alert("删除成功");
									grid.reload();
									//search(grid.pageSize,grid.pageIndex);
								} else {
									mini.alert("删除失败");
								}
							}
						});
						}
					});
				}
				else {
					mini.alert("请选中一条记录！", "消息提示");
				}

			}
		/**************************审批相关****************************/
		//审批日志查看
		function appLog(selections){
			var flow_type = Approve.FlowType.VerifyApproveFlow;
			if(selections.length <= 0){
				mini.alert("请选择要操作的数据","系统提示");
				return;
			}
			if(selections.length > 1){
				mini.alert("系统不支持多笔操作","系统提示");
				return;
			}
			if(selections[0].tradeSource == "3"){
				mini.alert("初始化导入的业务没有审批日志","系统提示");
				return false;
			}
			
			Approve.approveLog(flow_type,selections[0].ticketId);
		};
		//提交正式审批、待审批
		function verify(selections){
			var grid = mini.get("datagrid");
			var row = grid.getSelected();
			if(selections.length == 0){
				mini.alert("请选中一条记录！","消息提示");
				return false;
			}else if(selections.length > 1){
				mini.alert("该功能不支持多笔提交，如需多笔提交请点击[批量提交]按钮","系统提示");
				return false;
			}
			if(selections[0]["approveStatus"] != "3" ){
				var url = CommonUtil.baseWebPath() +"/approvefx/ccyswapEdit.jsp?action=approve&ticketid="+row.ticketId;
				var tab = {"id": "CcySwapApprove",name:"CcySwapApprove",url:url,title:"货币掉期审批",
							parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
				var paramData = {selectData:selections[0]};
				CommonUtil.openNewMenuTab(tab,paramData);
				// top["win"].showTab(tab);
			}else{
				Approve.approveCommit(Approve.FlowType.VerifyApproveFlow,selections[0]["ticketId"],Approve.OrderStatus.New,"IfsApproveCswapService",prdNo,function(){
					search(grid.pageSize,grid.pageIndex);
				});
			}
		};
		//审批
		function approve(){
			mini.get("approve_commit_btn").setEnabled(false);
			var messageid = mini.loading("系统正在处理...", "请稍后");
			try {
				verify(grid.getSelecteds());
			} catch (error) {
				
			}
			mini.hideMessageBox(messageid);	
			mini.get("approve_commit_btn").setEnabled(true);
		}
		//提交审批
		function commit(){
			mini.get("approve_mine_commit_btn").setEnabled(false);
			var messageid = mini.loading("系统正在处理...", "请稍后");
			try {
				verify(grid.getSelecteds());
			} catch (error) {
				
			}
			mini.hideMessageBox(messageid);	
			mini.get("approve_mine_commit_btn").setEnabled(true);
		}
		//审批日志
		function searchlog(){
			appLog(grid.getSelecteds());
		}
		//打印
	    function print(){
			var selections = grid.getSelecteds();
			if(selections == null || selections.length == 0){
				mini.alert('请选择一条要打印的数据！','系统提示');
				return false;
			}else if(selections.length>1){
				mini.alert('暂不支持多条数据同时打印，请选择一条要打印的数据！','系统提示');
				return false;
			}
			var canPrint = selections[0].ticketId;
			
			if(!CommonUtil.isNull(canPrint)){
				var actionStr = CommonUtil.pPath + "/sl/IfsPrintUtilController/exportload/print/"+PrintNo.ccyswap+"/"+ canPrint;
				$('<form target="_blank" action=' + actionStr + ' method="post"></form>').appendTo('body').submit().remove();
			};
		}
		
	</script>
</body>
</html>