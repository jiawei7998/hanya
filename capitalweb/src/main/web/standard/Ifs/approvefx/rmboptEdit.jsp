<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ include file="../../global.jsp"%>
<title>人民币期权</title>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>
<body style="width:100%;height:100%;background:white">
		<div class="mini-splitter" style="width:100%;height:100%;">
		<div size="90%" showCollapseButton="false">
		<h1 style="text-align:center"><strong>人民币期权</strong></h1>
		<div  id="field_form"  class="mini-fit area" style="background:white" >
	<input id="sponsor" name="sponsor" class="mini-hidden" />
	<input id="sponInst" name="sponInst" class="mini-hidden" />
	<input id="aDate" name="aDate" class="mini-hidden"/>
	<input id="dealTransType" name="dealTransType" class="mini-hidden"/>
	<input id="postDate" name="postDate" class="mini-datepicker"  labelField="true" width="30%" style="font-size:18px;float:left;margin-left: 100px;"   label="报送日期：" required="true"  labelStyle="text-align:left;" />
	<input id="ticketId" name="ticketId" class="mini-textbox" labelField="true" width="35%" style="font-size:18px; margin-left: 200px;" label="审批单编号：" emptyText="由系统自动生成"  labelStyle="text-align:left;width:120px" enabled="false"/>
	<div class="centerarea">
	  <fieldset>
	  	<legend>交易明细(FX OPTION)</legend>
	  	<input id="counterpartyInstId" name="counterpartyInstId" class="mini-buttonedit" labelField="true"  label="交易对手：" onbuttonclick="onButtonEdit" maxLength="25" labelStyle="text-align:left;" />
	  	<input id="currencyPair" name="currencyPair" class="mini-combobox" labelField="true"  label="货币对："   data="CommonUtil.serverData.dictionary.CurrencyPair" labelStyle="text-align:left;" />
		<input id="direction" name="direction" class="mini-combobox" labelField="true"  label="交易方向：" data="CommonUtil.serverData.dictionary.trading"  labelStyle="text-align:left;" />
		<input id="tradingType" name="tradingType" class="mini-textbox" labelField="true"  label="交易类型：" maxLength="20"  labelStyle="text-align:left;"   />
		<input id="strikePrice" name="strikePrice" class="mini-spinner" changeOnMousewheel='false'  format="n4" labelField="true"  label="执行价：" maxValue="99999999999999999.99999999"  labelStyle="text-align:left;"   />
		<input id="buyNotional" name="buyNotional" class="mini-combobox" labelField="true"  label="名义本金方向：" data="CommonUtil.serverData.dictionary.trading"  labelStyle="text-align:left;"   />
		<input id="buyNotionalAmount" name="buyNotionalAmount" class="mini-spinner" labelField="true"  label="名义本金金额：" changeOnMousewheel='false' maxValue="99999999999999999.99999999" format="n4"  labelStyle="text-align:left;"   />
		<input id="sellNotional" name="sellNotional" class="mini-combobox" labelField="true"  label="名义本金方向：" data="CommonUtil.serverData.dictionary.trading"   labelStyle="text-align:left;"   />
		<input id="sellNotionalAmount" name="sellNotionalAmount" class="mini-spinner" labelField="true"  label="名义本金金额：" changeOnMousewheel='false' maxValue="99999999999999999.99999999" format="n4"  labelStyle="text-align:left;"   />
		<input id="tenor" name="tenor" class="mini-combobox" labelField="true"  label="期限："  data="CommonUtil.serverData.dictionary.Term"  labelStyle="text-align:left;" />
		<input id="spotReference" name="spotReference" class="mini-textbox" labelField="true"  label="即期参考汇率：" maxLength="20" labelStyle="text-align:left;"/>
		<input id="premiumDate" name="premiumDate" class="mini-datepicker" labelField="true"  label="期权费交付日："  ondrawdate="compareDate1"  labelStyle="text-align:left;"   />
		<input id="expiryDate" name="expiryDate" class="mini-datepicker" labelField="true"  label="行权日："  ondrawdate="compareDate2" labelStyle="text-align:left;"   />
		<input id="premiumRate" name="premiumRate" class="mini-spinner" labelField="true"  label="期权费率(%)：" changeOnMousewheel='false' maxValue="999999999999999999.999999999999" format="n8"  labelStyle="text-align:left;"   />
		<input id="premiumType" name="premiumType" class="mini-textbox" labelField="true"  label="期权类型：" maxLength="20"  labelStyle="text-align:left;"   /> 
		<input id="deliveryDate" name="deliveryDate" class="mini-datepicker" labelField="true"  label="交割日：" ondrawdate="compareDate3" labelStyle="text-align:left;"   />
		<input id="premiumAmount" name="premiumAmount" class="mini-spinner" labelField="true"  label="期权费金额：" changeOnMousewheel='false' maxValue="99999999999999999.99999999" format="n4"  labelStyle="text-align:left;" />
		<input id="cutOffTime" name="cutOffTime" class="mini-timespinner" labelField="true"  label="行权截止时间："   labelStyle="text-align:left;" />
		<input id="deliveryType" name="deliveryType" class="mini-combobox" labelField="true"  label="交割方式：" maxLength="20" data="CommonUtil.serverData.dictionary.DeliveryType"  labelStyle="text-align:left;"   />
		<input id="optionStrategy" name="optionStrategy" class="mini-textbox" labelField="true"  label="期权类型：" maxLength="20"  labelStyle="text-align:left;"   />
		<input id="exerciseType" name="exerciseType" class="mini-textbox" labelField="true"  label="执行方式：" maxLength="20"  labelStyle="text-align:left;"   />
		<input id="strategyId" name="strategyId" class="mini-textbox" labelField="true"  label="期权组合号：" maxLength="20"  labelStyle="text-align:left;"   />
	  </fieldset>
	</div>	
	<%@ include file="../../Common/opicsApprove.jsp"%>		
	<%@ include file="../../Common/Flow/MiniApproveOpCommon.jsp"%>
</div>
</div>
<div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
		<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="save_btn"  onclick="save">保存交易</a></div>
		<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="close_btn"  onclick="close">关闭界面</a></div>
	</div>
	</div>
	<script type="text/javascript">
	mini.parse();
	//获取当前tab
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var row=params.selectData;
	var url = window.location.search;
	var action = CommonUtil.getParam(url, "action");
	var ticketId=CommonUtil.getParam(url, "ticketid");
	
	mini.get("postDate").setValue("<%=__bizDate %>");

	/* var timestamp = Date.parse(new Date());
	sys(timestamp); */
	function sys(stamp){
		var time = new Date(stamp);
		var result = "";
		result += CommonUtil.singleNumberFormatter(time.getHours()) + ":"; 
		result += CommonUtil.singleNumberFormatter(time.getMinutes()) + ":";
		result += CommonUtil.singleNumberFormatter(time.getSeconds());
		mini.get("forTime").setValue(result);
	}
	
	var tradeData={};
	
	tradeData.selectData=row;
	tradeData.operType=action;
	tradeData.serial_no=row.ticketId;
	tradeData.task_id=row.taskId;
	
	function inme(){
		if(action=="detail"||action=="edit"||action=="approve"){
		var from = new mini.Form("field_form");
		CommonUtil.ajax({
			url:'/IfsApproveForeignController/searchOption',
			data:{ticketId:ticketId},
			callback:function(text){
				from.setData(text.obj);
				//投资组合
				mini.get("port").setValue(text.obj.port);
				mini.get("port").setText(text.obj.port);
				mini.get("counterpartyInstId").setValue(text.obj.counterpartyInstId);
				queryTextName(text.obj.counterpartyInstId);
				/* mini.get("counterpartyInstId").setText(text.obj.counterpartyInstId); */
			}
		});
		if(action=="detail"||action=="approve"){
			mini.get("save_btn").hide();
			from.setEnabled(false);
			var form1=new mini.Form("approve_operate_form");
			form1.setEnabled(true);
		}
		}else if(action=="add"){
			mini.get("sponsor").setValue("<%=__sessionUser.getUserId()%>");
			mini.get("sponInst").setValue("<%=__sessionUser.getInstId() %>");
			mini.get("dealTransType").setValue("0");
	}
	}
	function save(){
		var form = new mini.Form("field_form");
		form.validate();
		if (form.isValid() == false) {
			return;
		}
		var form = new mini.Form("field_form");            
		var data = form.getData(true);      //获取表单多个控件的数据
		var json = mini.encode(data);   //序列化成JSON
		if(action=="add"){
			CommonUtil.ajax({
			    url: "/IfsApproveForeignController/addOption",
			    data:json,
			    callback:function (data) {
			    	if (data.code == 'error.common.0000') {
						mini.alert("保存成功",'提示信息',function(){
							top["win"].closeMenuTab();
						});
					} else {
						mini.alert("保存失败");
			    	}
			    }
			});
			}else  if(action="edit"){
				CommonUtil.ajax({
					url:"/IfsApproveForeignController/editOption",
					data:json,
					callback:function(data){
						if (data.code == 'error.common.0000') {
							mini.alert(data.desc,'提示信息',function(){
								top["win"].closeMenuTab();
							});
						} else {
							mini.alert("保存失败");
				    }
					}
				});
			}
	}
	function onButtonEdit(e) {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Common/CustMini.jsp",
            title: "选择对手方列表",
            width: 900,
            height: 600,
            onload: function () {//弹出页面加载完成
                var iframe = this.getIFrameEl(); 
                var data = {};
                
                //调用弹出页面方法进行初始化
                //iframe.contentWindow.SetData(data); 
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.cno);
                        btnEdit.setText(data.cfn);
                        btnEdit.focus();
                    }
                }

            }
        });

    }
	//根据交易对手编号查询全称
	function queryTextName(cno){
		CommonUtil.ajax({
		    url: "/IfsOpicsCustController/searchIfsOpicsCust",
		    data : {'cno' : cno},
		    callback:function (data) {
		    	if (data && data.obj) {
		    		mini.get("counterpartyInstId").setText(data.obj.cfn);
				} else {
					mini.get("counterpartyInstId").setText();
		    }
		    }
		});
	}
	//文、数字、下划线 的验证
	function onEnglishAndNumberValidations(e) {
		if(e.value == "" || e.value == null){//值为空，就不做校验
			return;
		}
		if (e.isValid) {
			if (isEnglishAndNumbers(e.value) == false) {
				e.errorText = "必须输入英文小写+数字";
				e.isValid = false;
			}
		}
	}
	/* 是否英文+数字 */
	function isEnglishAndNumbers(v) {
		var re = new RegExp("^[0-9a-z\_\-]+$");
		if (re.test(v)) return true;
		return false;
	}	
	
	
	//英文、数字、下划线 的验证
	function onEnglishAndNumberValidation(e) {
		if(e.value == "" || e.value == null){//值为空，就不做校验
			return;
		}
		if (e.isValid) {
			if (isEnglishAndNumber(e.value) == false) {
				e.errorText = "必须输入英文+数字";
				e.isValid = false;
			}
		}
	}
	/* 是否英文+数字 */
	function isEnglishAndNumber(v) {
		var re = new RegExp("^[0-9a-zA-Z\_\-]+$");
		if (re.test(v)) return true;
		return false;
	}
	//日期判断
    function compareDate1(e) {
		var date = e.date;
		var d = mini.get("expiryDate").getValue();
		var f = mini.get("deliveryDate").getValue();
		if(d==null || d == "" && f=="" || f==null){
			return;
		}
		if(d!=null && d!= ""){
		if (date.getTime() > d.getTime()) {
			e.allowSelect = false;
		}
		}
		if(f!=null && f!= ""){
			if (date.getTime() > f.getTime()) {
				e.allowSelect = false;
			}
		}
	}
    function compareDate2(e) {
		var date = e.date;
		var d = mini.get("premiumDate").getValue();
		var f = mini.get("deliveryDate").getValue();
		if(d==null || d == "" && f=="" || f==null){
			return;
		}
		if(d!=null && d!= ""){
			if (date.getTime() < d.getTime()) {
				e.allowSelect = false;
			}
		}
		if(f!=null && f!= ""){
			if (date.getTime() > f.getTime()) {
				e.allowSelect = false;
			}
		}
		}
    function compareDate3(e) {
		var date = e.date;
		var d = mini.get("premiumDate").getValue();
		var f = mini.get("expiryDate").getValue();
		if(d==null || d == "" && f=="" || f==null){
			return;
		}
		if(d!=null && d!=""){
			if (date.getTime() < d.getTime()) {
				e.allowSelect = false;
			}
		}
		if(f!=null && f!= ""){
			if (date.getTime() < f.getTime()) {
				e.allowSelect = false;
			}
		}
	}
	
	$(document).ready(function () {
		inme();
	});
	
	function close(){
		top["win"].closeMenuTab();
	}
	
	</script>
	<script type="text/javascript" src="../../Common/Flow/MiniApproveOpCommon.js"></script>			
</body>
</html>