<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
<html>
  <head>
    <title></title>
  </head>

<body style="width:100%;height:100%;background:white">
		<div class="mini-splitter" style="width:100%;height:100%;">
				<div size="90%" showCollapseButton="false">
<h1 style="text-align:center">外幣債</h1>
	<div  id="field_form"  class="mini-fit area" style="background:white" >
		<input id="sponsor" name="sponsor" class="mini-hidden" />
		<input id="sponInst" name="sponInst" class="mini-hidden" />
		<input id="aDate" name="aDate" class="mini-hidden"/>
		<input id="dealTransType" name="dealTransType" class="mini-hidden"/>
		<input id="postDate" name="postDate" class="mini-datepicker"  labelField="true" width="30%" style="font-size:18px;float:left;margin-left: 100px;"   label="报送日期：" required="true"  labelStyle="text-align:left;" />
		<input id="ticketId" name="ticketId" class="mini-textbox" labelField="true" width="35%" style="font-size:18px; margin-left: 200px;" label="审批单编号：" emptyText="由系统自动生成"  labelStyle="text-align:left;width:120px" enabled="false"/>
		<div>
			<fieldset>
		  	<legend>交易明细</legend>
		  		<input id="direction" name="direction" class="mini-combobox" labelField="true"  label="S買賣方向：" labelStyle="text-align:left;" data="CommonUtil.serverData.dictionary.BusinessDirection"/>
				<input id="jnumber" name="jnumber" class="mini-Spinner" labelField="true"  label="數量：" maxValue="999999999999999999" onValuechanged="interestAmount" required="true"  changeOnMousewheel="false" decimalPlaces=0 labelStyle="text-align:left;"/>
				<input id="capital" name="capital" class="mini-Spinner" labelField="true"  maxValue="99999999999999999.99999999" format="n4" changeOnMousewheel="false" label="本金：" required="true"  labelStyle="text-align:left;"  />
                <input id="profit" name="profit" class="mini-Spinner" maxValue="99999999999999999.99999999" format="n4" changeOnMousewheel="false" labelField="true"  label="殖利：" required="true"  maxLength="20" labelStyle="text-align:left;"/>
				<input id="countDate" name="countDate" class="mini-datepicker" labelField="true"  label="計算日期："  labelStyle="text-align:left;"  />
                <input id="net" name="net" class="mini-Spinner" maxValue="99999999999999999.99999999" format="n4" changeOnMousewheel="false" labelField="true"  label="凈：" required="true"  maxLength="20"  labelStyle="text-align:left;"/>
				<input id="benchmarkProfit" name="benchmarkProfit" class="mini-textbox" labelField="true"  label="基準/殖利：" maxLength="20" labelStyle="text-align:left;"  />
			
			
				<input id="discountRate" name="discountRate" class="mini-textbox" labelField="true"  label="貼現率：" maxLength="20" labelStyle="text-align:left;"  />
				<input id="price" name="price" class="mini-spinner" maxValue="999999999999999999.999999999999" format="n8" changeOnMousewheel="false" labelField="true"  label="價格：" onValuechanged="interestAmount" required="true"  labelStyle="text-align:left;"  />
				<input id="interest" name="interest" class="mini-spinner" labelField="true"  label="應計息：" maxValue="99999999999999999.99999999" format="n4" required="true"  labelStyle="text-align:left;"  />
				<input id="overweight" name="overweight" class="mini-textbox" labelField="true"  label="加碼：" maxLength="20" labelStyle="text-align:left;"" />
				<input id="benchmarkPrice" name="benchmarkPrice" class="mini-textbox" labelField="true"  label="基準/價格：" maxLength="20" labelStyle="text-align:left;" />
				<input id="benchmarkDiscount" name="benchmarkDiscount" class="mini-textbox" labelField="true"  label="基準/貼現：" maxLength="20" labelStyle="text-align:left;"  />
				</fieldset>
		</div>
			<%@ include file="../../Common/opicsApprove.jsp"%>
			<%@ include file="../../Common/Flow/MiniApproveOpCommon.jsp"%>
		</div>
</div>	

<div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
		<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="save_btn"   onclick="save">保存交易</a></div>
		<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="close_btn"   onclick="close">关闭界面</a></div>
	</div>
	</div>
		<script type="text/javascript">
			mini.parse();
			
			var currTab = top["win"].tabs.getActiveTab();
			var params = currTab.params;
			var row=params.selectData;
			var url = window.location.search;
			var action = CommonUtil.getParam(url, "action");
			var ticketId = CommonUtil.getParam(url, "ticketid");
			mini.get("postDate").setValue("<%=__bizDate %>");
			var tradeData={};
			
			tradeData.selectData=row;
			tradeData.operType=action;
			tradeData.serial_no=row.ticketId;
			tradeData.task_id=row.taskId;
			function inme() {
				if (action == "detail" || action == "edit" ||action=="approve") {
					var from = new mini.Form("field_form");
					CommonUtil.ajax({
						url : '/IfsApproveCurrencyController/searchIfsDebt',
						data : {
							ticketId : ticketId
						},
						callback : function(text) {
							from.setData(text.obj);
							//投资组合
							mini.get("port").setValue(text.obj.port);
							mini.get("port").setText(text.obj.port);
						}
					});
					if (action == "detail" || action=="approve") {
						mini.get("save_btn").hide();
						from.setEnabled(false);
						from1 = new mini.Form("approve_operate_form");
						from1.setEnabled(true);
					}
				}else if(action=="add"){
					mini.get("sponsor").setValue("<%=__sessionUser.getUserId()%>");
					mini.get("sponInst").setValue("<%=__sessionUser.getInstId() %>");
					mini.get("dealTransType").setValue("0");
				} 
			}

			function save() {
				var form = new mini.Form("field_form");
				form.validate();
				if (form.isValid() == false) {
					return;
				}
				var form = new mini.Form("field_form");
				var data = form.getData(true); //获取表单多个控件的数据
				var json = mini.encode(data); //序列化成JSON
				if (action == "add") {
					CommonUtil.ajax({
						url : "/IfsApproveCurrencyController/addIfsDebt",
						data : json,
						callback : function(data) {
							if (data.code == 'error.common.0000') {
								mini.alert("保存成功",'提示信息',function(){
									top["win"].closeMenuTab();
								});
							} else {
								mini.alert("保存失败");
					   	    }
						}
					});
				} else if (action = "edit") {
					CommonUtil.ajax({
						url : "/IfsApproveCurrencyController/editIfsDebt",
						data : json,
						callback : function(data) {
							if (data.code == 'error.common.0000') {
								mini.alert(data.desc, '提示信息', function() {
									top["win"].closeMenuTab();
								});
							} else {
								mini.alert("保存失败");
							}
						}
					})
				}
			}
			function close() {
				top["win"].closeMenuTab();
			}
			$(document).ready(function() {
				inme();
			})
			 
			//文、数字、下划线 的验证
	function onEnglishAndNumberValidations(e) {
		if(e.value == "" || e.value == null){//值为空，就不做校验
			return;
		}
		if (e.isValid) {
			if (isEnglishAndNumbers(e.value) == false) {
				e.errorText = "必须输入英文小写+数字";
				e.isValid = false;
			}
		}
	}
	/* 是否英文+数字 */
	function isEnglishAndNumbers(v) {
		var re = new RegExp("^[0-9a-z\_\-]+$");
		if (re.test(v)) return true;
		return false;
	}	
	
	  
	  
	   //文、数字、下划线 的验证
	   function onEnglishAndNumberValidation(e) {
		   if(e.value == "" || e.value == null){//值为空，就不做校验
			   return;
		   }
		   if (e.isValid) {
			   if (isEnglishAndNumber(e.value) == false) {
				   e.errorText = "必须输入英文+数字";
				   e.isValid = false;
			   }
		   }
	   }
	   /* 是否英文+数字 */
	   function isEnglishAndNumber(v) {
		   var re = new RegExp("^[0-9a-zA-Z\_\-]+$");
		   if (re.test(v)) return true;
		   return false;
	   }	
	 
	   function interestAmount(){
		   var num = mini.get("jnumber").getValue();
		   var price = mini.get("price").getValue();
		   if(num!=null && price != null && num!="" && price!=""){
			   var capital = Number(num)*Number(price);
			   mini.get("capital").setValue(capital);
		   }
	   }
		</script>
			<script type="text/javascript" src="<%=basePath%>/standard/Common/Flow/MiniApproveOpCommon.js"></script>			
		
</body>
</html>
