<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
<title></title>

</head>
<body style="width: 100%; height: 100%; background: white">
	<fieldset class="mini-fieldset">
		<legend>查询</legend>
		<div id="search_form" style="width: 100%;">
			<input id="ticketId" name="ticketId" class="mini-textbox" labelField="true" width="280px" label="审批单编号：" labelStyle="text-align:right;"  labelStyle="width:120px" emptyText="请输入审批单编号"/> 
			<input id="approveStatus" name="approveStatus" class="mini-combobox" data="CommonUtil.serverData.dictionary.ApproveSta" width="280px" emptyText="请选择审批状态" labelField="true"  label="审批状态：" labelStyle="text-align:right;"/>
			<span style="float: right; margin-right: 100px"> 
				<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
			</span>
		</div>
	</fieldset>
	<%@ include file="../preReback.jsp"%>
	<div id="MiniSettleForeignSpot" class="mini-fit" style="margin-top: 2px;">
		<div id="datagrid" class="mini-datagrid borderAll" style="width: 100%; height: 100%;" allowAlternating="true"
			allowResize="true" onrowdblclick="onRowDblClick" border="true" sortMode="client">
			<div property="columns">
				<div type="indexcolumn" headerAlign="center" width="40">序号</div>
				<div field="ticketId" width="200" allowSort="false" headerAlign="center"  >审批单编号</div>
				<div field="dealTransType" width="100" allowSort="true" headerAlign="center"  align="center"   renderer="CommonUtil.dictRenderer" data-options="{dict:'dealTransType'}">交易状态</div>
				<div field="approveStatus" width="120" align="center" headerAlign="center"  renderer="CommonUtil.dictRenderer" data-options="{dict:'ApproveStatus'}">审批状态</div>
				<div field="counterpartyInstId" width="200" allowSort="true" headerAlign="center"  align="center">交易对手</div>
				<div field="postDate" width="200" allowSort="true" headerAlign="center"  align="center">报送日期</div>
				<div field="currency" width="100" allowSort="true" headerAlign="center"  align="center" align="" renderer="CommonUtil.dictRenderer" data-options="{dict:'Currency'}">货币</div>
				<div field="direction" width="100" allowSort="true" headerAlign="center"  align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'trading'}">交易方向</div>
				<div field="amount" width="150" allowSort="true" headerAlign="center"  align="right"  numberFormat="#,0.0000">拆借金额</div>
				<div field="valueDate" width="150" allowSort="true" headerAlign="center"  align="center">起息日</div>
				<div field="maturityDate" width="150" allowSort="true" headerAlign="center"  align="center">到期日</div>
				<div field="sponsor" width="100" allowSort="true" headerAlign="center"  align="center" >审批发起人</div>
				<div field="sponInst" width="150" allowSort="true" align="center"  >审批发起机构</div>
				<div field="aDate" width="180" allowSort="true" align="center">审批发起时间</div>
			</div>
		</div>
	</div>
	<script>
		mini.parse();
		var url = window.location.search;
		var prdNo = CommonUtil.getParam(url, "prdNo");
		var prdName = CommonUtil.getParam(url, "prdName");
		var grid = mini.get("datagrid");
		/* grid.on("beforeload", function(e) {
			e.cancel = true;
			var pageIndex = e.data.pageIndex;
			var pageSize = e.data.pageSize;
			search(pageSize, pageIndex);
		});  */
		/* function checkBoxValuechanged(e){
			search(10,0);
			var approveType=this.getValue();
			if(approveType == "mine"){
				initButton();
			}else if(approveType == "finished"){//已审批
				mini.get("add_btn").setVisible(false);
				mini.get("edit_btn").setVisible(false);
				mini.get("delete_btn").setVisible(false);
				mini.get("approve_commit_btn").setVisible(false);
				mini.get("approve_mine_commit_btn").setVisible(false);
				mini.get("approve_log").setVisible(true);//审批日志
				mini.get("approve_log").setEnabled(true);//审批日志高亮
				mini.get("print_bk_btn").setVisible(false);//打印
				mini.get("print_bk_btn").setEnabled(false);//打印高亮
			}else{
				//待审批
				mini.get("add_btn").setVisible(false);
				mini.get("edit_btn").setVisible(false);
				mini.get("delete_btn").setVisible(false);
				mini.get("approve_commit_btn").setVisible(true);//审批
				mini.get("approve_commit_btn").setEnabled(true);//审批高亮
				mini.get("approve_mine_commit_btn").setVisible(false);//提交审批
				mini.get("approve_log").setVisible(true);//审批日志
				mini.get("approve_log").setEnabled(true);//审批日志高亮
				mini.get("print_bk_btn").setVisible(false);//打印
				mini.get("print_bk_btn").setEnabled(false);//打印高亮
			}
		}
		function initButton(){
			//我发起的
			mini.get("add_btn").setVisible(true);
			mini.get("add_btn").setEnabled(true);
			mini.get("edit_btn").setVisible(true);
			mini.get("edit_btn").setEnabled(true);
			mini.get("delete_btn").setVisible(true);
			mini.get("delete_btn").setEnabled(true);
			mini.get("approve_commit_btn").setVisible(false);
			mini.get("approve_mine_commit_btn").setVisible(true);//提交审批显示
			mini.get("approve_mine_commit_btn").setEnabled(true);//提交审批高亮
			mini.get("approve_log").setVisible(true);//审批日志
			mini.get("approve_log").setEnabled(true);//审批日志高亮
			mini.get("print_bk_btn").setVisible(false);//打印
			mini.get("print_bk_btn").setEnabled(false);//打印高亮
		} */
		/* grid.on("select",function(e){
			var row=e.record;
			if(row.approveStatus == "3"){//新建
				mini.get("approve_mine_commit_btn").setEnabled(true);
				mini.get("approve_commit_btn").setEnabled(false);
				mini.get("approve_log").setEnabled(false);
				mini.get("edit_btn").setEnabled(true);
				mini.get("delete_btn").setEnabled(true);
			}
			if( row.approveStatus == "4" || row.approveStatus == "5"){//待审批：4   审批中:5
				mini.get("approve_mine_commit_btn").setEnabled(false);
				mini.get("approve_commit_btn").setEnabled(true);
				mini.get("edit_btn").setEnabled(false);
				mini.get("delete_btn").setEnabled(false);
			}
			if( row.approveStatus == "6" ){//审批完成：6
				mini.get("approve_mine_commit_btn").setEnabled(false);
				mini.get("approve_commit_btn").setEnabled(true);
				mini.get("edit_btn").setEnabled(false);
				mini.get("delete_btn").setEnabled(false);
			}
			if(row.approveStatus != null && row.approveStatus != "3") {
				mini.get("approve_log").setEnabled(true);
			}
		});  */
		
		
		/* grid.on("select",function(e){
			var row=e.record;
			mini.get("approve_mine_commit_btn").setEnabled(false);
			mini.get("approve_commit_btn").setEnabled(false);
			mini.get("edit_btn").setEnabled(false);
			mini.get("delete_btn").setEnabled(false);
			if(row.approveStatus == "3"){//新建
				mini.get("approve_mine_commit_btn").setEnabled(true);
				mini.get("approve_commit_btn").setEnabled(false);
				mini.get("edit_btn").setEnabled(true);
				mini.get("delete_btn").setEnabled(true);
				mini.get("approve_log").setEnabled(false);
			}
			if( row.approveStatus == "6" || row.approveStatus == "5"){//审批通过：6   审批中:5
				mini.get("approve_mine_commit_btn").setEnabled(false);
				mini.get("approve_commit_btn").setEnabled(true);
				mini.get("edit_btn").setEnabled(false);
				mini.get("delete_btn").setEnabled(false);
			}
			if(row.approveStatus != null && row.approveStatus != "3") {
				mini.get("approve_log").setEnabled(true);
			}
			if(row.dealTransType=="N"){
				mini.get("approve_mine_commit_btn").setEnabled(false);
				mini.get("approve_commit_btn").setEnabled(false);
				mini.get("edit_btn").setEnabled(false);
				mini.get("delete_btn").setEnabled(false);
				mini.get("approve_log").setEnabled(false);
				mini.get("print_bk_btn").setEnabled(false);
				mini.get("reback_btn").setEnabled(false);
			}
			if(row.dealTransType!="N"){
				mini.get("reback_btn").setEnabled(true);
			}
			if(row.approveStatus == "3"){
				mini.get("reback_btn").setEnabled(false);
			}
		}); */
		
		function getData(action) {
			var row = null;
			if (action != "add") {
				row = grid.getSelected();
			}
			return row;
		}
	
		function search(pageSize,pageIndex){
			var form = new mini.Form("#search_form");
			form.validate();
			if(form.isValid()==false){
				mini.alert("信息填写有误，请重新填写","系统提示");
				return;
			}

			var data=form.getData(true);
			data['pageNumber']=pageIndex+1;
			data['pageSize']=pageSize;
			data['branchId']=branchId;
			var url=null;

			var approveType = mini.get("approveType").getValue();
			if(approveType == "mine"){
				url = "/IfsApproveCurrencyController/searchPageCcyLendingMine";
			}else if(approveType == "approve"){
				url = "/IfsApproveCurrencyController/searchPageCcyLendingUnfinished";
			}else{
				url = "/IfsApproveCurrencyController/searchPageCcyLendingFinished";
			}

			var params = mini.encode(data);
			CommonUtil.ajax({
				url:url,
				data:params,
				callback : function(data) {
					grid.setTotalCount(data.obj.total);
					grid.setPageIndex(pageIndex);
			        grid.setPageSize(pageSize);
					grid.setData(data.obj.rows);
				}
			});
		}
		
		function getData(action) {
			var row = null;
			if (action != "add") {
				row = grid.getSelected();
			}
			return row;
		}
		//增删改查
		function add(){
			var url = CommonUtil.baseWebPath() + "/approvefx/ccyLendingEdit.jsp?action=add";
			var tab = { id: "MiniCcyLendingAdd", name: "MiniCcyLendingAdd", title: "外币拆借添加", url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
			var paramData = {selectData:""};
			CommonUtil.openNewMenuTab(tab,paramData);
		}
		function query() {
            search(grid.pageSize, 0);
        }
		function clear(){
            var form=new mini.Form("search_form");
            form.clear();
            search(10,0);

		}
		function edit()
		{
			var row = grid.getSelected();
			if(row){
				var url = CommonUtil.baseWebPath() + "/approvefx/ccyLendingEdit.jsp?action=edit&ticketid="+row.ticketId;
				var tab = { id: "MiniCcyLendingEdit", name: "MiniCcyLendingEdit", title: "外币拆借修改", url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
				var paramData = {selectData:""};
			    CommonUtil.openNewMenuTab(tab,paramData);

			} else {
				mini.alert("请选中一条记录！","消息提示");
			}
		}

		function onRowDblClick(e) {
			var grid = mini.get("datagrid");
			var row = grid.getSelected();
			if(row){
					var url = CommonUtil.baseWebPath() + "/approvefx/ccyLendingEdit.jsp?action=detail&ticketid="+row.ticketId;
					var tab = { id: "MiniCcyLendingDetail", name: "MiniCcyLendingDetail", title: "外币拆借详情", url: url ,showCloseButton:true};
					var paramData = {selectData:""};
					CommonUtil.openNewMenuTab(tab,paramData);

			} else {
				mini.alert("请选中一条记录！","消息提示");
			}
		}
		//删除
		function del() {
				var grid = mini.get("datagrid");
				var row = grid.getSelected();
				if (row) {
					mini.confirm("您确认要删除选中记录?","系统警告",function(value){
						if(value=="ok"){
						CommonUtil.ajax({
							url: "/IfsApproveCurrencyController/deleteCcyLending",
							
							data: {ticketid: row.ticketId},
							callback: function (data) {
								if (data.code == 'error.common.0000') {
									mini.alert("删除成功");
									grid.reload();
									//search(grid.pageSize,grid.pageIndex);
								} else {
									mini.alert("删除失败");
								}
							}
						});
						}
					});
				}
				else {
					mini.alert("请选中一条记录！", "消息提示");
				}

			}
		/**************************审批相关****************************/
		//审批日志查看
		function appLog(selections){
			var flow_type = Approve.FlowType.VerifyApproveFlow;
			if(selections.length <= 0){
				mini.alert("请选择要操作的数据","系统提示");
				return;
			}
			if(selections.length > 1){
				mini.alert("系统不支持多笔操作","系统提示");
				return;
			}
			if(selections[0].tradeSource == "3"){
				mini.alert("初始化导入的业务没有审批日志","系统提示");
				return false;
			}
			Approve.approveLog(flow_type,selections[0].ticketId);
		};
		//提交正式审批、待审批
		function verify(selections){
			var grid = mini.get("datagrid");
			var row = grid.getSelected();
			if(selections.length == 0){
				mini.alert("请选中一条记录！","消息提示");
				return false;
			}else if(selections.length > 1){
				mini.alert("该功能不支持多笔提交，如需多笔提交请点击[批量提交]按钮","系统提示");
				return false;
			}
			if(selections[0]["approveStatus"] != "3" ){
				var url = CommonUtil.baseWebPath() +"/approvefx/ccyLendingEdit.jsp?action=approve&ticketid="+row.ticketId;
				var tab = {"id": "AdvanceCcyLendingApprove",name:"AdvanceCcyLendingApprove",url:url,title:"外币拆借审批",
							parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
				var paramData = {selectData:selections[0]};
				CommonUtil.openNewMenuTab(tab,paramData);
				// top["win"].showTab(tab);
			}else{
				Approve.approveCommit(Approve.FlowType.VerifyApproveFlow,selections[0]["ticketId"],Approve.OrderStatus.New,"IfsApproveLendService",prdNo,function(){
					search(grid.pageSize,grid.pageIndex);
				});
			}
		};
		//审批
		function approve(){
			mini.get("approve_commit_btn").setEnabled(false);
			var messageid = mini.loading("系统正在处理...", "请稍后");
			try {
				verify(grid.getSelecteds());
			} catch (error) {
				
			}
			mini.hideMessageBox(messageid);	
			mini.get("approve_commit_btn").setEnabled(true);
		}
		//提交审批
		function commit(){
			mini.get("approve_mine_commit_btn").setEnabled(false);
			var messageid = mini.loading("系统正在处理...", "请稍后");
			try {
				verify(grid.getSelecteds());
			} catch (error) {
				
			}
			mini.hideMessageBox(messageid);	
			mini.get("approve_mine_commit_btn").setEnabled(true);
		}
		//审批日志
		function searchlog(){
			appLog(grid.getSelecteds());
		}
		//打印
	    function print(){
			var selections = grid.getSelecteds();
			if(selections == null || selections.length == 0){
				mini.alert('请选择一条要打印的数据！','系统提示');
				return false;
			}else if(selections.length>1){
				mini.alert('暂不支持多条数据同时打印，请选择一条要打印的数据！','系统提示');
				return false;
			}
			var canPrint = selections[0].ticketId;
			if(!CommonUtil.isNull(canPrint)){
				var actionStr = CommonUtil.pPath + "/sl/IfsPrintUtilController/exportload/print/"+PrintNo.ccyLending+"/"+ canPrint;
				$('<form target="_blank" action=' + actionStr + ' method="post"></form>').appendTo('body').submit().remove();
			};
		}
		/* $(document).ready(function() {
			initButton();
			search(10, 0);
		}); */
	</script>
</body>
</html>