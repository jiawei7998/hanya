<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
	<script type="text/javascript" src="<%=basePath%>/miniScript/excelExport.js"></script>
<title>总交易查询</title>
</head>
<body style="width: 100%; height: 100%; background: white">
	<fieldset class="mini-fieldset title">
		<legend>查询条件</legend>
		<div id="search_form" style="width: 100%;">
            <input id="startDay" name="startDay" class="mini-datepicker" width="320px"  labelField="true" label="审批发起时间：" labelStyle="text-align:right;"  onValuechanged="compareDate" labelStyle="text-align:left;width:60px;"/>
           	 <input id="endDay" name="endDay" class="mini-datepicker" width="320px"  labelField="true" label="～" labelStyle="text-align:center;"  onValuechanged="compareDate" labelStyle="text-align:left;width:60px;"/>
            <input id="ticketId" name="ticketId" class="mini-textbox"  width="320px" labelField="true" label="审批单编号：" labelStyle="text-align:right;" emptyText="请填写审批单号" width="280px" />
            <input id="contractId" name="contractId" class="mini-textbox" width="320px"  labelField="true" label="成交单编号：" labelStyle="text-align:right;" emptyText="请填写成交单编号" width="280px" />
			<span style="float: right; margin-right: 150px">
				<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
				<a id="export" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
				<a id="approve_log" class="mini-button" style="display: none"   onclick="approveLog()">审批日志</a>
			</span>
		</div>
	</fieldset>

<div class="mini-splitter" style="width:100%;height:95%;">
    <div size="300" showCollapseButton="false">
        <div class="mini-toolbar" style="padding:2px;border-top:0;border-left:0;border-right:0;">
            <span style="padding-left:5px;">交易目录：</span>
        </div>
        <fieldset class="mini-fieldset" style="height: 95%;overflow-y: auto">
<%--                <legend>交易目录</legend>--%>
                    <ul id="treeReport" class="mini-tree" url="listTree.txt" style="width:100%;padding:5px;"
                    showTreeIcon="true" textField="text" idField="id" parentField="pid" resultAsTree="false"
                    contextMenu="#treeMenu" expandOnLoad="true" onnodeselect="onnodeselect" >
                	</ul>
        </fieldset>
    </div>
    <div showCollapseButton="false" style="height:90%;">
        <div style="height:5%;">
        <span id = "dealtype">外汇即期</span>
        </div>
        <div class="mini-fit" style="height:100%;">
        	<div id="datagrid" class="mini-datagrid" style="width:100%;height:80%;"  allowAlternating="true"  multiSelect="true"
				allowResize="true"  onrowdblclick="onRowDblClick" border="true" sortMode="client" sizeList="[10,20,100,-1]" >
				<div property="columns">
				</div>
			</div>
			<legend>交易统计</legend>
		  	<br />
		  	新建：<input id="newBuildData" name="newBuildData" size="8" disabled></input>
		  	审批中:<input id="approvingData" name="approvingData" size="8" disabled></input>
<%--		  	审核中:<input id="checkingData" name="checkingData" size="8" disabled></input>--%>
		  	待结算:<input id="windingData" name="windingData" size="8" disabled></input>
			交易冲正:<input id="flushingData" name="flushingData" size="8" disabled></input>
			审批拒绝:<input id="refuseData" name="refuseData" size="8" disabled></input>
			审批完成:<input id="completedData" name="completedData" size="8" disabled></input>
			已撤销：<input id="repealData" name="repealData" size="8" disabled></input>
		  	<br />
		  	<br />
		  	<br />
		  	<br />
        </div>
    </div>
</div>

	<script>
		mini.parse();

		var url = window.location.search;
		var prdNo = CommonUtil.getParam(url, "prdNo");
		var prdName = CommonUtil.getParam(url, "prdName");

		var grid = mini.get("datagrid");
		var pageIndex = grid.pageIndex;
		var pageSize = grid.pageSize;
		var firstLoad = "0";
		var id = "411";
		grid.on("beforeload", function(e) {
			e.cancel = true;
			pageIndex = e.data.pageIndex;
			pageSize = e.data.pageSize;
			search(pageSize, pageIndex);
		});


		var dictCP = CommonUtil.getDictList("CurrencyPair");//货币对
		var CurrencyPair = [{ id: 'UC', text: dictCP[0].text }, { id: 'AC', text: dictCP[1].text }];
		var dictT = CommonUtil.getDictList("trading");//交易方向
		var Trading = [{ id: 'P', text: dictT[0].text }, { id: 'S', text: dictT[1].text }];
		var dictAS = CommonUtil.getDictList("ApproveStatus");//审批状态
		var ApproveStatus = [{ id: '3', text: dictAS[1].text }, { id: '5', text: '审批中' }, { id: '6', text: dictAS[14].text },{ id: '7', text: dictAS[13].text }];
		var dictDTT = CommonUtil.getDictList("dealTransType");//交易状态
		var DealTransType = [{ id: 'M', text: dictDTT[0].text }, { id: '0', text: dictDTT[1].text }, { id: 'F', text: dictDTT[2].text }, { id: 'G', text: dictDTT[3].text }, { id: 'I', text: dictDTT[4].text }, { id: 'N', text: dictDTT[5].text }];
		var dictTT = CommonUtil.getDictList("TradeType");//交易方式
		var TradeType = [{ id: 'rfq', text: dictTT[0].text }, { id: 'C-Swap', text: dictTT[1].text }];
		var dictDT = CommonUtil.getDictList("DeliveryType");//交割方式
		var DeliveryType = [{ id: '1', text: dictDT[0].text }, { id: '2', text: dictDT[1].text }];
		var dictAR = CommonUtil.getDictList("AllotRule");//Rule
		var AllotRule = [{ id: '1', text: dictAR[0].text }];
		var dictTM = CommonUtil.getDictList("TradeModel");//交易模式
		var TradeModel = [{ id: 'C', text: dictTM[0].text }, { id: 'I', text: dictTM[1].text }, { id: 'IB', text: dictTM[2].text }];
		var dictSM = CommonUtil.getDictList("SettlementMethod");//结算方式
		var SettlementMethod = [{ id: '1', text: dictSM[0].text }];
		var opicsDT = CommonUtil.getDictList("statcode");//结算方式
		var statcode = [{ id: '-1', text: opicsDT[0].text },{ id: '-3', text: opicsDT[1].text },{ id: '-4', text: opicsDT[2].text },{ id: '-6', text: opicsDT[3].text }];

		function search(pageSize,pageIndex){
			var form = new mini.Form("#search_form");
			form.validate();
			if(form.isValid()==false){
				mini.alert("信息填写有误，请重新填写","系统提示");
				return;
			}
			var data=form.getData(true);
			var tree = mini.get("treeReport");
			id='';
			if(tree.getSelectedNode()){
				id = tree.getSelectedNode().id;
			}
			data['pageNumber']=pageIndex+1;
			data['pageSize']=pageSize;
			data['branchId']=branchId;
			data['sponInst']=instId;
			data['id']=id;
			if(firstLoad == "0"){
				var tree = mini.get("treeReport");
				tree.setValue("411");
				url = "/QryTotalDealController/searchTotalDealRMBWBJQ";
				id = "411";
			}else{
				var selectedLeve = tree.getSelectedNode()._level;
			  	if(selectedLeve ==2 ){
					var url=tree.getSelectedNode().url;
			  	}
			}

			var params = mini.encode(data);
			//console.log(data)
			CommonUtil.ajax({
				url:url,
				data:params,
				callback : function(data) {
				    var tableHead = choseTableHeader(id);
					grid.set({//生成交易表头
			        	columns: tableHead
			   		});
					if(data){
						grid.setTotalCount(data.obj.total);
						grid.setPageIndex(pageIndex);
				        grid.setPageSize(pageSize);
						grid.setData(data.obj.rows);
						countDeal(data.obj.rows);
					}

				}
			});
		}

		function query() {

			//return;
			var tree = mini.get("treeReport");
			if(tree.getSelectedNode()._level !=2){//只有树级别为2的才进行查询
				mini.alert("只有选中具体交易类型，才可进行查询","系统提示");
				return;
			}

			onnodeselect();
        }
		function clear(){
            var form=new mini.Form("search_form");
            form.clear();
            search(10,0);

		}

		function getData(action) {
			var row = null;
			if (action != "add") {
				row = grid.getSelected();
			}
			return row;
		}


		$(document).ready(function() {
			//控制按钮显示
			$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
				//initButton();
				search(10, 0);
			});
		});

		//起始日期选择
		function onDrawStartDate(e){
			var date = e.date;
	        var d = new Date();
	        if (date.getTime() > d.getTime()) {
	            e.allowSelect = false;
	        }

	        var startDay=mini.get("startDay").getValue();  //直接获取startDay的值
	        var startTime=mini.parseDate(startDay);   //字符串转化为日期
	        var endDay=mini.get("endDay").getValue();  //直接获取startDay的值
	        var endTime=mini.parseDate(endDay);   //字符串转化为日期
	        if (endTime!=null&&startTime!=null&&startTime.getTime() > endTime.getTime())
	         {
	              e.allowSelect = false;
	         }

		}
		//到期日判断
		function onDrawEndDate(e){
			var date = e.date;
	        var d = new Date();
	        if (date.getTime() > d.getTime()) {
	            e.allowSelect = false;
	        }

	        var startDay=mini.get("startDay").getValue();  //直接获取startDay的值
	        var startTime=mini.parseDate(startDay);   //字符串转化为日期
	        var endDay=mini.get("endDay").getValue();  //直接获取startDay的值
	        var endTime=mini.parseDate(endDay);   //字符串转化为日期
	        if (startTime!=null&&endTime!=null&&startTime.getTime() > endTime.getTime())
	         {
	              e.allowSelect = false;
	         }

		}
		function compareDate() {//判断日期
			var sta = mini.get("startDay");
			var end = mini.get("endDay");
			if(sta != null || end != null || sta != "" ||end != ""){
				if (sta.getValue() > end.getValue() && end.getValue("")) {
					mini.alert("开始时间不能大于结束时间", "系统提示");
					return end.setValue(sta.getValue());
				}
			}

		}

		//选择节点事件
		function onnodeselect(){
			var grid = mini.get("datagrid");
			var form = new mini.Form("#search_form");
			firstLoad = "1";//是否是第一次进入，1不是，0是
			form.validate();
			if(form.isValid()==false){
				mini.alert("信息填写有误，请重新填写","系统提示");
				return;
			}
			var tree = mini.get("treeReport");
			//排除父节点调用报错
			if(!tree.getSelectedNode().url){
				return;
			}
			id = tree.getSelectedNode().id;
			var selectedLeve = tree.getSelectedNode()._level;
			if(selectedLeve ==2 ){
				var url=tree.getSelectedNode().url;
			}
			var data=form.getData();
			//data['pageNumber']=pageIndex;
			//每次翻页后页码还是从1开始
			data['pageNumber']=0;
			data['pageSize']=pageSize;
			data['branchId']=branchId;
			data['sponInst']=instId;
			data['startDay']=mini.encode(mini.get("startDay").getValue(),"yyyyMMdd").replace('"',"").replace('"',"");//直接获取startDay的值
			data['endDay']=mini.encode(mini.get("endDay").getValue(),"yyyyMMdd").replace('"',"").replace('"',"");//直接获取startDay的值
	        data['id']=id;

			var text = tree.getSelectedNode().text;
			$("#dealtype").html(text);

			var params = mini.encode(data);
			CommonUtil.ajax({
				url:url,
				data:params,
				callback : function(data) {

					var tableHead = choseTableHeader(id);
					grid.set({
				        columns: tableHead
				    });

					grid.setTotalCount(data.obj.total);
					grid.setPageIndex(0);
			        grid.setPageSize(pageSize);
					grid.setData(data.obj.rows);
					countDeal(data.obj.rows);
				}
			});

		}

		function countDeal(e){//统计成交的记录
			var num = e;

			var approvingData = 0;//审批中
			// var checkingData = 0;//审核中
			var windingData = 0;//待结算

			var flushingData = 0;//交易冲正
			var refuseData = 0;//审批拒绝
			var completedData = 0;//结算完成
			var newBuildData = 0;//新建
			var repealData = 0;//已撤销

			for(var i=0,l=num.length;i<l;i++){
				var taskName = num[i].taskName;

				if(!CommonUtil.isNull(taskName)){
					if(taskName.indexOf("审批中") != -1){
						approvingData = approvingData +1;
					}else if(taskName.indexOf("待结算") != -1){
						windingData = windingData +1;
					}else if(taskName.indexOf("交易冲正") != -1){
						flushingData = flushingData +1;
					}else if(taskName.indexOf("审批拒绝") != -1){
						refuseData = refuseData +1;
					}else if(taskName.indexOf("审批完成") != -1){
						completedData = completedData +1;
					}else if(taskName.indexOf("新建") != -1){
						newBuildData = newBuildData +1;
					}else if(taskName.indexOf("已撤销") != -1){
						repealData = repealData +1;
					}
					// else if(taskName.indexOf("审核中") != -1){
					// 	checkingData = checkingData +1;
					// }
				}

			}
			$("#approvingData").val(approvingData);
			// $("#checkingData").val(checkingData);
			$("#windingData").val(windingData);

			$("#flushingData").val(flushingData);
			$("#refuseData").val(refuseData);
			$("#completedData").val(completedData);
			$("#newBuildData").val(newBuildData);
			$("#repealData").val(repealData);
		}

		//双击到详情页
		function onRowDblClick(e) {
			var grid = mini.get("datagrid");
			var row = grid.getSelected();
			if(row){
				if(id == '411'){
					var url = CommonUtil.baseWebPath() + "/cfetsfx/rmbsptEdit.jsp?action=detail&ticketid="+row.ticketId+"&fPrdCode="+id+"&prdNo="+'411'+"&dealNo="+row.dealNo;
					var tab = { id: "MinirmbsptDetail", name: row.ticketId, title: "外汇即期详情", url: url ,showCloseButton:true};
					var paramData = {selectData:row};
					CommonUtil.openNewMenuTab(tab,paramData);
				}else if(id == '391'){
					var url = CommonUtil.baseWebPath() + "/cfetsfx/rmfwdEdit.jsp?action=detail&ticketid="+row.ticketId+"&fPrdCode="+id+"&prdNo="+'391'+"&dealNo="+row.dealNo;
					var tab = { id: "MiniRmbfwdDetail", name: row.ticketId, title: "外汇远期详情", url: url ,showCloseButton:true};
					var paramData = {selectData:row};
					CommonUtil.openNewMenuTab(tab,paramData);
				}else if(id == '432'){
					var url = CommonUtil.baseWebPath() + "/cfetsfx/rmswapEdit.jsp?action=detail&ticketid="+row.ticketId+"&fPrdCode="+id+"&prdNo="+'432'+"&dealNo="+row.dealNo;
					var tab = { id: "MiniRmbSwapEdit", name: row.ticketId, title: "外汇掉期详情", url: url ,showCloseButton:true};
					var paramData = {selectData:row};
					CommonUtil.openNewMenuTab(tab,paramData);
				}else if(id == '438'){
					var url = CommonUtil.baseWebPath() + "/cfetsfx/ccyLendingEdit.jsp?action=detail&ticketid="+row.ticketId+"&fPrdCode="+id+"&prdNo="+prdNo;
					var tab = { id: "MiniCcyLendingDetail", name: row.ticketId, title: "外币拆借详情", url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
					var paramData = {selectData:row};
				    CommonUtil.openNewMenuTab(tab,paramData);
				}else if(id == '444'){
					var url = CommonUtil.baseWebPath() +"/cfetsrmb/creditLoanEdit.jsp?action=detail&ticketid="+row.ticketId+"&fPrdCode="+id+"&prdNo="+prdNo;
					var tab = {id: "MiniIboDetail",name:row.ticketId,url:url,title:"信用拆借详情",
								parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
					var paramData = {selectData:grid.getSelected()};
					CommonUtil.openNewMenuTab(tab,paramData);
				}else if(id == '544'){//
					var url = CommonUtil.baseWebPath() +"/cfetsrmb/creditTYLoanEdit.jsp?action=detail&ticketid="+row.ticketId+"&fPrdCode="+id+"&prdNo="+prdNo;
					var tab = {id: "IboTYDetail",name:row.ticketId,url:url,title:"同业借款详情",
								parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
					var paramData = {selectData:grid.getSelected()};
					CommonUtil.openNewMenuTab(tab,paramData);
				}else if(id == '800'){//
					var url = CommonUtil.baseWebPath() +"/cfetsrmb/depositEdit.jsp?action=detail&ticketid="+row.ticketId+"&fPrdCode="+id+"&prdNo="+prdNo;
					var tab = {id: "DepositDetail",name:row.ticketId,url:url,title:"同业存款详情",
								parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
					var paramData = {selectData:grid.getSelected()};
					CommonUtil.openNewMenuTab(tab,paramData);
				}else if(id == '799'){//
					var url = CommonUtil.baseWebPath() +"/cfetsrmb/depositEdit.jsp?action=detail&ticketid="+row.ticketId+"&fPrdCode="+id+"&prdNo="+prdNo;
					var tab = {id: "DepositDetail",name:row.ticketId,url:url,title:"同业存款详情",
						parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
					var paramData = {selectData:grid.getSelected()};
					CommonUtil.openNewMenuTab(tab,paramData);
				}else if(id == '443'){
					var url = CommonUtil.baseWebPath() +"/cfetsrmb/bondTradingEdit.jsp?action=detail&ticketid="+row.ticketId+"&fPrdCode="+id+"&prdNo="+prdNo;
					var tab = {id: "MiniCbtDetail",name:row.ticketId,url:url,title:"现券买卖详情",
								parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
					var paramData = {selectData:grid.getSelected()};
					CommonUtil.openNewMenuTab(tab,paramData);
				}else if(id == '447'){
					var url = CommonUtil.baseWebPath() +"/cfetsrmb/bondFwdEdit.jsp?action=detail&ticketid="+row.ticketId+"&fPrdCode="+id+"&prdNo="+prdNo;
					var tab = {id: "MiniBFWDDetail",name:row.ticketId,url:url,title:"债券远期详情",
								parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
					var paramData = {selectData:grid.getSelected()};
					CommonUtil.openNewMenuTab(tab,paramData);
				}else if(id == '445'){
					var url = CommonUtil.baseWebPath() +"/cfetsrmb/bondLendingEdit.jsp?action=detail&ticketid="+row.ticketId+"&fPrdCode="+id+"&prdNo="+prdNo;
					var tab = {id: "MiniLsDetail",name:row.ticketId,url:url,title:"债券借贷详情",
								parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
					var paramData = {selectData:grid.getSelected()};
					CommonUtil.openNewMenuTab(tab,paramData);
				}else if(id == '452'){
					var url = CommonUtil.baseWebPath() +"/cfetsrmb/depositPublishEdit.jsp?action=detail&ticketid="+row.ticketId+"&fPrdCode="+id+"&prdNo="+prdNo;
					var tab = {id: "MiniDpDetail",name:row.ticketId,url:url,title:"债券发行详情",
								parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
					var paramData = {selectData:grid.getSelected()};
					CommonUtil.openNewMenuTab(tab,paramData);
				}else if(id == '425'){//
					var url = CommonUtil.baseWebPath() +"/cfetsrmb/cdPublishEdit.jsp?action=detail&ticketid="+row.ticketId+"&fPrdCode="+id+"&prdNo="+prdNo;
					var tab = {id: "cdDetail",name:row.ticketId,url:url,title:"存单发行详情",
								parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
					var paramData = {selectData:grid.getSelected()};
					CommonUtil.openNewMenuTab(tab,paramData);
				}else if(id == '471'){
					var url = CommonUtil.baseWebPath() +"/cfetssecur/MBSEdit.jsp?action=detail&ticketid="+row.ticketId+"&fPrdCode="+id+"&prdNo="+prdNo;
					var tab = {id: "MBSDetail",name:row.ticketId,url:url,title:"提前偿还详情",
								parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
					var paramData = {selectData:grid.getSelected()};
					CommonUtil.openNewMenuTab(tab,paramData);
				}else if(id == '446'){
					var url = CommonUtil.baseWebPath() +"/cfetsrmb/pledgeRepoEdit.jsp?action=detail&ticketid="+row.ticketId+"&fPrdCode="+id+"&prdNo="+prdNo;
					var tab = {id: "MiniCrDetail",name:row.ticketId,url:url,title:"质押式回购详情",
								parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
					var paramData = {selectData:grid.getSelected()};
					CommonUtil.openNewMenuTab(tab,paramData);
				}else if(id == '442'){
					var url = CommonUtil.baseWebPath() +"/cfetsrmb/repoEdit.jsp?action=detail&ticketid="+row.ticketId+"&fPrdCode="+id+"&prdNo="+prdNo;
					var tab = {id: "MiniOrDetail",name:row.ticketId,url:url,title:"买断式回购详情",
								parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
					var paramData = {selectData:grid.getSelected()};
					CommonUtil.openNewMenuTab(tab,paramData);
				}else if(id == '467'){
					var url = CommonUtil.baseWebPath() +"/cfetsrmb/nationalDepositEdit.jsp?action=detail&ticketid="+row.ticketId+"&fPrdCode="+id+"&prdNo="+prdNo;
					var tab = {id: "NDDetail",name:row.ticketId,url:url,title:"国库定期存款详情",
								parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
					var paramData = {selectData:grid.getSelected()};
					CommonUtil.openNewMenuTab(tab,paramData);
				}else if(id == '468'){
					var url = CommonUtil.baseWebPath() +"/cfetsrmb/exchangeRepoEdit.jsp?action=detail&ticketid="+row.ticketId+"&fPrdCode="+id+"&prdNo="+prdNo;
					var tab = {id: "EXDetail",name:row.ticketId,url:url,title:"交易所回购详情",
								parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
					var paramData = {selectData:grid.getSelected()};
					CommonUtil.openNewMenuTab(tab,paramData);
				}else if(id == '473'){
					var url = CommonUtil.baseWebPath() +"/cfetsrmb/exchangeRepoBuyEdit.jsp?action=detail&ticketid="+row.ticketId+"&fPrdCode="+id+"&prdNo="+prdNo;
					var tab = {id: "EXDetail",name:row.ticketId,url:url,title:"交易所回购详情",
						parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
					var paramData = {selectData:grid.getSelected()};
					CommonUtil.openNewMenuTab(tab,paramData);
				}
				else if(id == '469'){
					var url = CommonUtil.baseWebPath() +"/cfetsrmb/standingLendingEdit.jsp?action=detail&ticketid="+row.ticketId+"&fPrdCode="+id+"&prdNo="+prdNo;
					var tab = {id: "StandDetail",name:row.ticketId,url:url,title:"常备借贷便利详情",
								parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
					var paramData = {selectData:grid.getSelected()};
					CommonUtil.openNewMenuTab(tab,paramData);
				}else if(id == '470'){
					var url = CommonUtil.baseWebPath() +"/cfetsrmb/MediumLendingEdit.jsp?action=detail&ticketid="+row.ticketId+"&fPrdCode="+id+"&prdNo="+prdNo;
					var tab = {id: "MediumDetail",name:row.ticketId,url:url,title:"中期借贷便利详情",
								parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
					var paramData = {selectData:grid.getSelected()};
					CommonUtil.openNewMenuTab(tab,paramData);
				}else if(id == '472'){
					var url = CommonUtil.baseWebPath() +"/cfetsrmb/MicroLendingEdit.jsp?action=detail&ticketid="+row.ticketId+"&fPrdCode="+id+"&prdNo="+prdNo;
					var tab = {id: "MicroLendingDetail",name:row.ticketId,url:url,title:"线下押券详情",
								parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
					var paramData = {selectData:grid.getSelected()};
					CommonUtil.openNewMenuTab(tab,paramData);
				}else if(id == '441'){
					var url = CommonUtil.baseWebPath() +"/cfetsrmb/IRSEdit.jsp?action=detail&ticketid="+row.ticketId+"&fPrdCode="+id+"&prdNo="+prdNo;
					var tab = {id: "MiniIrsDetail",name:row.ticketId,url:url,title:"利率互换详情",
								parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
					var paramData = {selectData:grid.getSelected()};
					CommonUtil.openNewMenuTab(tab,paramData);
				}else if(id == '801-1'){
					var url = CommonUtil.baseWebPath() +"/refund/CCurrencyFundAfpEdit.jsp?action=detail&ticketid="+row.dealNo+"&prdNo="+prdNo;
					var tab = {id: "MiniFtCurrencyAfpDetail",name:row.ticketId,url:url,title:"货币基金申购详情",
						parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
					var paramData = {selectData:grid.getSelected()};
					CommonUtil.openNewMenuTab(tab,paramData);
				}else if(id == '801-2'){
					var url = CommonUtil.baseWebPath() +"/refund/CCurrencyFundAfpConfEdit.jsp?action=detail&ticketid="+row.dealNo+"&prdNo="+prdNo;
					var tab = {id: "MiniFtCurrencyAfpConfDetail",name:row.ticketId,url:url,title:"货币基金申购确认详情",
						parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
					var paramData = {selectData:grid.getSelected()};
					CommonUtil.openNewMenuTab(tab,paramData);
				}else if(id == '801-3'){
					var url = CommonUtil.baseWebPath() +"/refund/CCurrencyFundRdpEdit.jsp?action=detail&ticketid="+row.dealNo+"&prdNo="+prdNo;
					var tab = {id: "MiniFtCurrencyRdpDetail",name:row.ticketId,url:url,title:"货币基金赎回详情",
						parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
					var paramData = {selectData:grid.getSelected()};
					CommonUtil.openNewMenuTab(tab,paramData);
				}else if(id == '801-4'){
					var url = CommonUtil.baseWebPath() +"/refund/CCurrencyFundRdpConfEdit.jsp?action=detail&ticketid="+row.dealNo+"&prdNo="+prdNo;
					var tab = {id: "MiniFtCurrencyRdpConfDetail",name:row.ticketId,url:url,title:"货币基金赎回确认详情",
						parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
					var paramData = {selectData:grid.getSelected()};
					CommonUtil.openNewMenuTab(tab,paramData);
				}else if(id == '802-1'){
					var url = CommonUtil.baseWebPath() +"/refund/CBondFundAfpEdit.jsp?action=detail&ticketid="+row.dealNo+"&prdNo="+prdNo;
					var tab = {id: "MiniFtBondAfpDetail",name:row.ticketId,url:url,title:"债券基金申购详情",
						parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
					var paramData = {selectData:grid.getSelected()};
					CommonUtil.openNewMenuTab(tab,paramData);
				}else if(id == '802-2'){
					var url = CommonUtil.baseWebPath() +"/refund/CBondFundAfpConfEdit.jsp?action=detail&ticketid="+row.dealNo+"&prdNo="+prdNo;
					var tab = {id: "MiniFtBondAfpConfDetail",name:row.ticketId,url:url,title:"债券基金申购确认详情",
						parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
					var paramData = {selectData:grid.getSelected()};
					CommonUtil.openNewMenuTab(tab,paramData);
				}else if(id == '802-3'){
					var url = CommonUtil.baseWebPath() +"/refund/CBondFundRdpEdit.jsp?action=detail&ticketid="+row.dealNo+"&prdNo="+prdNo;
					var tab = {id: "MiniFtBondRdpDetail",name:row.ticketId,url:url,title:"债券基金赎回详情",
						parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
					var paramData = {selectData:grid.getSelected()};
					CommonUtil.openNewMenuTab(tab,paramData);
				}else if(id == '802-4'){
					var url = CommonUtil.baseWebPath() +"/refund/CBondFundRdpConfEdit.jsp?action=detail&ticketid="+row.dealNo+"&prdNo="+prdNo;
					var tab = {id: "MiniFtBondRdpConfDetail",name:row.ticketId,url:url,title:"债券基金赎回确认详情",
						parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
					var paramData = {selectData:grid.getSelected()};
					CommonUtil.openNewMenuTab(tab,paramData);
				}else if(id == '803-1'){
					var url = CommonUtil.baseWebPath() +"/refund/CFundAfpEdit.jsp?action=detail&ticketid="+row.dealNo+"&prdNo="+prdNo;
					var tab = {id: "MiniFtAfpDetail",name:row.ticketId,url:url,title:"专户基金申购详情",
								parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
					var paramData = {selectData:grid.getSelected()};
					CommonUtil.openNewMenuTab(tab,paramData);
				}else if(id == '803-2'){
					var url = CommonUtil.baseWebPath() +"/refund/CFundAfpConfEdit.jsp?action=detail&ticketid="+row.dealNo+"&prdNo="+prdNo;
					var tab = {id: "MiniFtAfpConfDetail",name:row.ticketId,url:url,title:"专户基金申购确认详情",
								parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
					var paramData = {selectData:grid.getSelected()};
					CommonUtil.openNewMenuTab(tab,paramData);
				}else if(id == '803-3'){
					var url = CommonUtil.baseWebPath() +"/refund/CFundRdpEdit.jsp?action=detail&ticketid="+row.dealNo+"&prdNo="+prdNo;
					var tab = {id: "MiniFtRdpDetail",name:row.ticketId,url:url,title:"专户基金赎回详情",
								parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
					var paramData = {selectData:grid.getSelected()};
					CommonUtil.openNewMenuTab(tab,paramData);
				}else if(id == '803-4'){
					var url = CommonUtil.baseWebPath() +"/refund/CFundRdpConfEdit.jsp?action=detail&ticketid="+row.dealNo+"&prdNo="+prdNo;
					var tab = {id: "MiniFtRdpConfDetail",name:row.ticketId,url:url,title:"专户基金赎回确认详情",
								parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
					var paramData = {selectData:grid.getSelected()};
					CommonUtil.openNewMenuTab(tab,paramData);
				}else if(id == '808'){
					var url = CommonUtil.baseWebPath() +"/refund/CFundReddEdit.jsp?action=detail&ticketid="+row.dealNo+"&prdNo="+prdNo;
					var tab = {id: "MiniFtReddDetail",name:row.ticketId,url:url,title:"基金现金分红详情",
								parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
					var paramData = {selectData:grid.getSelected()};
					CommonUtil.openNewMenuTab(tab,paramData);
				}else if(id == '809'){
					var url = CommonUtil.baseWebPath() +"/refund/CFundReinEdit.jsp?action=detail&ticketid="+row.dealNo+"&prdNo="+prdNo;
					var tab = {id: "MiniFtReinDetail",name:row.ticketId,url:url,title:"基金红利再投详情",
								parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
					var paramData = {selectData:grid.getSelected()};
					CommonUtil.openNewMenuTab(tab,paramData);
				}else if(id == '439'){
					var url = CommonUtil.baseWebPath() + "/cfetsfx/ccyPositionAllocationEdit.jsp?action=detail&ticketid="+row.ticketId+"&fPrdCode="+id+"&prdNo="+prdNo;
					var tab = { id: "MiniAllotDetail", name: row.ticketId, title: "外币头寸调拨详情", url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
					var paramData = {selectData:row};
				    CommonUtil.openNewMenuTab(tab,paramData);
				}else if(id == '431'){
					var url = CommonUtil.baseWebPath() + "/cfetsfx/ccyswapEdit.jsp?action=detail&ticketid="+row.ticketId+"&fPrdCode="+id+"&prdNo="+prdNo;
					var tab = { id: "MiniSwapDetail", name: row.ticketId, title: "货币掉期详情", url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
					var paramData = {selectData:row};
				    CommonUtil.openNewMenuTab(tab,paramData);
				}else if(id == '433'){
					var url = CommonUtil.baseWebPath() + "/cfetsfx/rmboptEdit.jsp?action=detail&ticketid="+row.ticketId+"&fPrdCode="+id+"&prdNo="+prdNo;
					var tab = { id: "MiniOptDetail", name: row.ticketId, title: "人民币期权详情", url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
					var paramData = {selectData:row};
				    CommonUtil.openNewMenuTab(tab,paramData);
				}else if(id == '440'){
					var url = CommonUtil.baseWebPath() + "/cfetsfx/ccypspotEdit.jsp?action=detail&ticketid="+row.ticketId+"&fPrdCode="+id+"&prdNo="+prdNo;
					var tab = { id: "MiniPspotDetail", name: row.ticketId, title: "外汇对即期详情", url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
					var paramData = {selectData:row};
				    CommonUtil.openNewMenuTab(tab,paramData);
				}else if(id == '450'){
					var url = CommonUtil.baseWebPath() +"/cfetsmetal/goldsptEdit.jsp?action=detail&ticketid="+row.ticketId+"&fPrdCode="+id+"&prdNo="+prdNo;
					var tab = {id: "MiniGoldsptDetail",name:row.ticketId,url:url,title:"黄金即期详情",
								parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
					var paramData = {selectData:grid.getSelected()};
					CommonUtil.openNewMenuTab(tab,paramData);
				}else if(id == '448'){
					var url = CommonUtil.baseWebPath() +"/cfetsmetal/goldfwdEdit.jsp?action=detail&ticketid="+row.ticketId+"&fPrdCode="+id+"&prdNo="+prdNo;
					var tab = {id: "MiniGoldfwdDetail",name:row.ticketId,url:url,title:"黄金远期详情",
								parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
					var paramData = {selectData:grid.getSelected()};
					CommonUtil.openNewMenuTab(tab,paramData);
				}else if(id == '449'){
					var url = CommonUtil.baseWebPath() +"/cfetsmetal/goldswapEdit.jsp?action=detail&ticketid="+row.ticketId+"&fPrdCode="+id+"&prdNo="+prdNo;
					var tab = {id: "MiniGoldswapDetail",name:row.ticketId,url:url,title:"黄金掉期详情",
								parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
					var paramData = {selectData:grid.getSelected()};
					CommonUtil.openNewMenuTab(tab,paramData);
				}

			} else {
				mini.alert("请选中一条记录！","消息提示");
			}



		}

		function choseTableHeader(id){
			var columns = null;
			if(id == '411'){//外汇即期rmbspt
				columns = [
							{ type: "indexcolumn", headerAlign:"center", width:"40", header: "序号"},
							{ field: "ticketId", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "审批单编号" },
							{ field: "contractId", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "成交单编号" },
							{ field: "dealNo", width: 80, headerAlign: "center", align:"center", allowSort: false, header: "opics交易号" },
							{ field: "statcode", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "opics处理状态" },
							{ field: "forDate", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "交易日期" },
							{ field: "dealSource", type: "comboboxcolumn", autoShowPopup: true, width: 100, headerAlign: "center", align:"center", header: "交易来源", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.TradeSource} },
							{ field: "currencyPair", type: "comboboxcolumn", autoShowPopup: true, width: 100, headerAlign: "center", align:"center", header: "货币对", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.CurrencyPair} },
							{ field: "buyDirection", type: "comboboxcolumn", autoShowPopup: true, width: 100, headerAlign: "center", align:"center", header: "交易方向", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.trading} },
							{ field: "port", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "投资组合" },
							{ field: "buyAmount", width: 200, headerAlign: "center", align:"right", allowSort: false, numberFormat:"#,0.0000", header: "交易金额" },
							//{ field: "price", width: 100, headerAlign: "center", align:"right", allowSort: false, numberFormat:"n8", header: "成交汇率(%)" },
							{ field: "exchangeRate", width: 100, headerAlign: "center", align:"right", allowSort: false, numberFormat:"n8", header: "成交汇率(%)" },
							{ field: "taskName", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "审批状态" },
							{ field: "counterpartyInstId", width: 250, headerAlign: "center", align:"center", allowSort: false, header: "对方机构" },
							{ field: "valueDate", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "起息日" },
							{ field: "sponsor", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "审批发起人" },
							{ field: "sponInst", width: 250, headerAlign: "center", align:"center", allowSort: false, header: "审批发起机构" },
							{ field: "aDate", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "审批发起时间" }
				        ]
			}else if(id == '391'){//外汇远期rmfwd
				columns = [
							{ type: "indexcolumn", headerAlign:"center", width:"40", header: "序号"},
							{ field: "ticketId", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "审批单编号" },
							{ field: "contractId", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "成交单编号" },
							{ field: "dealNo", width: 80, headerAlign: "center", align:"center", allowSort: false, header: "opics交易号" },
							{ field: "statcode", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "opics处理状态" },
							{ field: "forDate", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "交易日期" },
							{ field: "dealSource", type: "comboboxcolumn", autoShowPopup: true, width: 100, headerAlign: "center", align:"center", header: "交易来源", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.TradeSource} },
				            { field: "currencyPair", type: "comboboxcolumn", autoShowPopup: false, width: 100, headerAlign: "center", align:"center", header: "货币对", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.CurrencyPair} },
				            { field: "buyAmount", width: 200, headerAlign: "center", align:"right", allowSort: false, numberFormat:"#,0.0000", header: "交易金额" },
				            //{ field: "price", width: 100, headerAlign: "center", align:"right", allowSort: false, numberFormat:"n8", header: "成交汇率(%)" },
				            { field: "exchangeRate", width: 100, headerAlign: "center", align:"right", allowSort: false, numberFormat:"n8", header: "成交汇率(%)" },
							{ field: "buyDirection", type: "comboboxcolumn", autoShowPopup: true, width: 100, headerAlign: "center", align:"center", header: "交易方向", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.trading} },
							{ field: "taskName", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "审批状态" },
				            //{ field: "dealTransType", type: "comboboxcolumn", autoShowPopup: true, width: 100, headerAlign: "center",align:"center", header: "交易状态", editor: { type: "combobox", data: DealTransType} },
				            { field: "counterpartyInstId", width: 250, headerAlign: "center", align:"center", allowSort: false, header: "对方机构" },
							{ field: "valueDate", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "起息日" },
				            { field: "sponsor", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "审批发起人" },
				            { field: "sponInst", width: 250, headerAlign: "center", align:"center", allowSort: false, header: "审批发起机构" },
				            { field: "aDate", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "审批发起时间" }
				        ]
			}else if(id == '432'){//外汇掉期rmswap
				columns = [
							{ type: "indexcolumn", headerAlign:"center", width:"40", header: "序号"},
							{ field: "ticketId", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "审批单编号" },
							{ field: "contractId", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "成交单编号" },
							{ field: "dealNo", width: 80, headerAlign: "center", align:"center", allowSort: false, header: "opics交易号" },
							{ field: "farDealNo", width: 80, headerAlign: "center", align:"center", allowSort: false, header: "opics远端交易号" },
							{ field: "statcode", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "opics处理状态" },
							{ field: "forDate", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "交易日期" },
							{ field: "dealSource", type: "comboboxcolumn", autoShowPopup: true, width: 100, headerAlign: "center", align:"center", header: "交易来源", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.TradeSource} },
							{ field: "currencyPair", type: "comboboxcolumn", autoShowPopup: false, width: 100, headerAlign: "center", align:"center", header: "货币对", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.CurrencyPair} },
							{ field: "direction", type: "comboboxcolumn", autoShowPopup: true, width: 100, headerAlign: "center", align:"center", header: "交易方向", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.Swaptrading} },
							{ field: "nearRate", width: 100, headerAlign: "center", align:"right", allowSort: false, numberFormat:"#,0.00000000", header: "近端汇率" },
							{ field: "nearPrice", width: 200, headerAlign: "center", align:"right", allowSort: false, numberFormat:"#,0.0000", header: "交易金额1（近端）" },
							{ field: "nearReversePrice", width: 200, headerAlign: "center", align:"right", allowSort: false, numberFormat:"#,0.0000", header: "交易金额2（近端）" },
							{ field: "nearValuedate", width: 130, headerAlign: "center", align:"center", allowSort: false, header: "起息日（近端）" },
							{ field: "fwdValuedate", width: 130, headerAlign: "center", align:"center", allowSort: false, header: "起息日（远端）" },
							{ field: "counterpartyInstId", width: 250, headerAlign: "center", align:"center", allowSort: false, header: "对方机构" },
							{ field: "taskName", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "审批状态" },
							//{ field: "approveStatus", type: "comboboxcolumn", autoShowPopup: true, width: 100, headerAlign: "center",align:"center", header: "审批状态", editor: { type: "combobox", data: ApproveStatus} },
							//{ field: "dealTransType", type: "comboboxcolumn", autoShowPopup: true, width: 100, headerAlign: "center",align:"center", header: "交易状态", editor: { type: "combobox", data: DealTransType} },
							{ field: "sponsor", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "审批发起人" },
							{ field: "sponInst", width: 250, headerAlign: "center", align:"center", allowSort: false, header: "审批发起机构" },
							{ field: "aDate", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "审批发起时间" }
				        ]
			}else if(id == '438'){//外币拆借ccyLending
				columns = [
							{ type: "indexcolumn", headerAlign:"center", width:"40", header: "序号"},
							{ field: "ticketId", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "审批单编号" },
							{ field: "contractId", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "成交单编号" },
							{ field: "dealNo", width: 80, headerAlign: "center", align:"center", allowSort: false, header: "opics交易号" },
							{ field: "statcode", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "opics处理状态" },
							{ field: "forDate", width: 130, headerAlign: "center", align:"center", allowSort: false, header: "交易日期" },
							{ field: "dealSource", type: "comboboxcolumn", autoShowPopup: true, width: 100, headerAlign: "center", align:"center", header: "交易来源", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.TradeSource} },
							{ field: "currency", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "货币" },
							{ field: "amount", width: 150, headerAlign: "center", align:"right", allowSort: false, numberFormat:"n4", header: "拆借金额" },
							{ field: "direction", type: "comboboxcolumn", autoShowPopup: true, width: 100, headerAlign: "center", align:"center", header: "交易方向", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.lending} },
							{ field: "valueDate", width: 130, headerAlign: "center", align:"center", allowSort: false, header: "起息日" },
							{ field: "maturityDate", width: 130, headerAlign: "center", align:"center", allowSort: false, header: "到期日" },
							{ field: "amount", width: 200, headerAlign: "center", align:"right", allowSort: false, numberFormat:"#,0.0000", header: "拆借金额" },
							{ field: "taskName", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "审批状态" },
							{ field: "instId", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "本方机构" },
							{ field: "counterpartyInstId", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "对手方机构" },
							{ field: "sponsor", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "审批发起人" },
							{ field: "sponInst", width: 250, headerAlign: "center", align:"center", allowSort: false, header: "审批发起机构" },
							{ field: "aDate", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "审批发起时间" }
				        ]
			}else if(id == '444'){//信用拆借
				columns = [
							{ type: "indexcolumn", headerAlign:"center", width:"40", header: "序号"},
							{ field: "ticketId", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批单编号" },
							{ field: "contractId", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "成交单编号" },
							{ field: "dealNo", width: 80, headerAlign: "center", align:"center", allowSort: false, header: "opics交易号" },
							{ field: "statcode", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "opics处理状态" },
							{ field: "taskName", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批状态" },
							{ field: "myDir", type: "comboboxcolumn", autoShowPopup: true, width: 120, headerAlign: "center", align:"center", header: "本方方向", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.trading} },
							{ field: "removeInst", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "对手方机构名称" },
							{ field: "amt", width: 120, headerAlign: "center", align:"center", allowSort: false,numberFormat:"#,0.0000", header: "拆借金额（万元）" },
							{ field: "rate", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "拆借利率" },
							{ field: "firstSettlementDate", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "起息日" },
							{ field: "secondSettlementDate", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "到期结算日" },
							{ field: "tenor", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "期限" },
							{ field: "occupancyDays", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "实际占款天数" },
							{ field: "settlementAmount", width: 180, headerAlign: "center", align:"center", allowSort: false,numberFormat:"#,0.0000", header: "到期结算金额（元）" },
							{ field: "basis", type: "comboboxcolumn", autoShowPopup: true, width: 120, headerAlign: "center", align:"center", header: "计息基准", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.Basis} },
							{ field: "borrowTrader", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "审批发起人" },
							{ field: "aDate", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批发起时间" }
				        ]
			}else if(id == '544'){//同业借款
				columns = [
							{ type: "indexcolumn", headerAlign:"center", width:"40", header: "序号"},
							{ field: "ticketId", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批单编号" },
							{ field: "contractId", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "成交单编号" },
							{ field: "dealNo", width: 80, headerAlign: "center", align:"center", allowSort: false, header: "opics交易号" },
							{ field: "statcode", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "opics处理状态" },
							{ field: "taskName", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批状态" },
							{ field: "myDir", type: "comboboxcolumn", autoShowPopup: true, width: 120, headerAlign: "center", align:"center", header: "本方方向", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.trading} },
							{ field: "removeInst", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "对手方机构名称" },
							{ field: "amt", width: 120, headerAlign: "center", align:"center", allowSort: false, numberFormat:"#,0.0000",header: "拆借金额（万元）" },
							{ field: "rate", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "拆借利率" },
							{ field: "firstSettlementDate", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "起息日" },
							{ field: "secondSettlementDate", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "到期结算日" },
							{ field: "tenor", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "期限" },
							{ field: "occupancyDays", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "实际占款天数" },
							{ field: "settlementAmount", width: 180, headerAlign: "center", align:"center", allowSort: false,numberFormat:"#,0.0000", header: "到期结算金额（元）" },
							{ field: "basis", type: "comboboxcolumn", autoShowPopup: true, width: 120, headerAlign: "center", align:"center", header: "计息基准", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.Basis} },
							{ field: "borrowTrader", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "审批发起人" },
							{ field: "aDate", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批发起时间" }
				        ]
			}else if(id == '800'){//同业存款
				columns = [
							{ type: "indexcolumn", headerAlign:"center", width:"40", header: "序号"},
							{ field: "ticketId", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批单编号" },
							// { field: "contractId", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "成交单号" },
							{ field: "taskName", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批状态" },
							{ field: "postDate", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "交易日期" },
							{ field: "rate", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "存款利率(%)" },
							{ field: "amt", width: 120, headerAlign: "center", align:"center", allowSort: false,numberFormat:"#,0.0000", header: "存款金额（万元）" },
							{ field: "accuredInterest", width: 120, headerAlign: "center", align:"center", allowSort: false,numberFormat:"#,0.0000", header: "利息（元）" },
							{ field: "settlementAmount", width: 180, headerAlign: "center", align:"center", allowSort: false, numberFormat:"#,0.0000",header: "到期还款金额（元）" },
							{ field: "firstSettlementDate", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "起息日" },
							{ field: "secondSettlementDate", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "到期日" },
							{ field: "occupancyDays", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "实际占款天数" },
							{ field: "sponsor", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "审批发起人" },
							{ field: "sponInst", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "审批发起机构" },
							{ field: "aDate", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批发起时间" }
				        ]
			}else if(id == '799'){//同业存款(线下)
				columns = [
					{ type: "indexcolumn", headerAlign:"center", width:"40", header: "序号"},
					{ field: "ticketId", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批单编号" },
					// { field: "contractId", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "成交单号" },
					{ field: "taskName", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批状态" },
					{ field: "postDate", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "交易日期" },
					{ field: "rate", width: 120, headerAlign: "center", align:"center", allowSort: false,numberFormat:"#,0.0000", header: "存款利率(%)" },
					{ field: "amt", width: 120, headerAlign: "center", align:"center", allowSort: false, numberFormat:"#,0.0000",header: "存款金额（万元）" },
					{ field: "accuredInterest", width: 120, headerAlign: "center", align:"center", allowSort: false,numberFormat:"#,0.0000", header: "利息（元）" },
					{ field: "settlementAmount", width: 180, headerAlign: "center", align:"center", allowSort: false,numberFormat:"#,0.0000", header: "到期还款金额（元）" },
					{ field: "firstSettlementDate", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "起息日" },
					{ field: "secondSettlementDate", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "到期日" },
					{ field: "occupancyDays", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "实际占款天数" },
					{ field: "sponsor", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "审批发起人" },
					{ field: "sponInst", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "审批发起机构" },
					{ field: "aDate", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批发起时间" }
				]
			} else if(id == '443'){//现券买卖
				columns = [
							{ type: "indexcolumn", headerAlign:"center", width:"40", header: "序号"},
							{ field: "ticketId", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "审批单编号" },
							{ field: "contractId", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "成交单编号" },
							{ field: "dealNo", width: 80, headerAlign: "center", align:"center", allowSort: false, header: "opics交易号" },
							{ field: "statcode", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "opics处理状态" },
							{ field: "bondName", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "债券简称" },
							{ field: "taskName", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批状态" },
							{ field: "invtype", type: "comboboxcolumn", autoShowPopup: true, width: 100, headerAlign: "center", align:"center", header: "投资类型", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.invtype} },
							{ field: "myDir", type: "comboboxcolumn", autoShowPopup: true, width: 100, headerAlign: "center", align:"center", header: "本方方向", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.trading} },
							{ field: "sellInst", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "对手方机构名称" },
							{ field: "cleanPrice", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "交易净价" },
							{ field: "yield", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "到期收益率（%）" },
							{ field: "tradeAmount", width: 150, headerAlign: "center", align:"center", allowSort: false,numberFormat:"#,0.0000", header: "交易金额（元）" },
							{ field: "forDate", width: 90, headerAlign: "center", align:"center", allowSort: false, header: "交易日" },
							{ field: "settlementAmount", width: 150, headerAlign: "center", align:"center", allowSort: false, numberFormat:"#,0.0000",header: "结算金额（元）" },
							{ field: "settlementDate", width: 90, headerAlign: "center", align:"center", allowSort: false, header: "结算日" },
							{ field: "totalFaceValue", width: 150, headerAlign: "center", align:"center", allowSort: false,numberFormat:"#,0.0000", header: "券面总额（万元）" },
							{ field: "settlementMethod", type: "comboboxcolumn", autoShowPopup: true, width: 90, headerAlign: "center", align:"center", allowSort: false, header: "结算方式" , editor: { type: "combobox", data: CommonUtil.serverData.dictionary.SettlementMethod} },
							{ field: "buyInst", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "买入方机构" },
							{ field: "sponsor", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "审批发起人" },
							{ field: "sponInst", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批发起机构" },
							{ field: "aDate", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批发起时间" }
				        ]
			}else if(id == '447'){//债券远期
				columns = [
							{ type: "indexcolumn", headerAlign:"center", width:"40", header: "序号"},
							{ field: "ticketId", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "审批单编号" },
							{ field: "contractId", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "成交单编号" },
							{ field: "dealNo", width: 80, headerAlign: "center", align:"center", allowSort: false, header: "opics交易号" },
							{ field: "statcode", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "opics处理状态" },
							{ field: "bondName", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "债券简称" },
							{ field: "taskName", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批状态" },
							{ field: "myDir", type: "comboboxcolumn", autoShowPopup: true, width: 100, headerAlign: "center", align:"center", header: "本方方向", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.trading} },
							{ field: "sellInst", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "对手方机构名称" },
							{ field: "price", width: 100, headerAlign: "center", align:"center", allowSort: false,numberFormat:"#,0.0000", header: "价格" },
							{ field: "amount", width: 150, headerAlign: "center", align:"center", allowSort: false, numberFormat:"#,0.0000",header: "金额（元）" },
							{ field: "settlementDate", width: 90, headerAlign: "center", align:"center", allowSort: false, header: "结算日" },
							{ field: "settlementMethod", type: "comboboxcolumn", autoShowPopup: true, width: 90, headerAlign: "center", align:"center", allowSort: false, header: "结算方式" , editor: { type: "combobox", data: CommonUtil.serverData.dictionary.SettlementMethod} },
							{ field: "buyInst", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "买入方机构" },
							{ field: "sponsor", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "审批发起人" },
							{ field: "sponInst", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批发起机构" },
							{ field: "aDate", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批发起时间" }
				        ]
			}else if(id == '445'){//债券借贷
				columns = [
							{ type: "indexcolumn", headerAlign:"center", width:"40", header: "序号"},
							{ field: "ticketId", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "审批单编号" },
							{ field: "contractId", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "成交单编号" },
							{ field: "dealNo", width: 80, headerAlign: "center", align:"center", allowSort: false, header: "opics交易号" },
							{ field: "statcode", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "opics处理状态" },
							{ field: "taskName", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批状态" },
							{ field: "tradingProduct", width: 110, headerAlign: "center", align:"center", allowSort: false, header: "交易品种" },
							{ field: "underlyingSymbol", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "标的债券简称" },
							{ field: "underlyingQty", width: 180, headerAlign: "center", align:"center", allowSort: false,numberFormat:"#,0.0000", header: "标的债券券面总额（万元）" },
							{ field: "myDir", type: "comboboxcolumn", autoShowPopup: true, width: 100, headerAlign: "center", align:"center", header: "本方方向", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.trading} },
							{ field: "lendInst", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "融出方机构" },
							{ field: "firstSettlementDate", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "首次结算日" },
							{ field: "secondSettlementDate", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "到期结算日" },
							{ field: "firstSettlementMethod", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "首次结算方式" },
							{ field: "secondSettlementMethod", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "到期结算方式" },
							{ field: "borrowInst", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "融入方机构" },
							{ field: "forDate", width: 110, headerAlign: "center", align:"center", allowSort: false, header: "成交日期" },
							{ field: "sponsor", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "审批发起人" },
							{ field: "sponInst", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "审批发起机构" },
							{ field: "aDate", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批发起时间" }
				        ]
			}else if(id == '445-2'){//债券借贷事前审批
				columns = [
							{ type: "indexcolumn", headerAlign:"center", width:"40", header: "序号"},
							{ field: "ticketId", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "审批单编号" },
							{ field: "contractId", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "成交单编号" },
							// { field: "dealNo", width: 80, headerAlign: "center", align:"center", allowSort: false, header: "opics交易号" },
							// { field: "statcode", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "opics处理状态" },
							{ field: "taskName", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批状态" },
							// { field: "tradingProduct", width: 110, headerAlign: "center", align:"center", allowSort: false, header: "交易品种" },
							{ field: "secname", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "标的债券简称" },
							{ field: "underlyingQty", width: 180, headerAlign: "center", align:"center", allowSort: false,numberFormat:"#,0.0000", header: "标的债券券面总额（万元）" },
							{ field: "oppoDir", type: "comboboxcolumn", autoShowPopup: true, width: 100, headerAlign: "center", align:"center", header: "本方方向", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.oppoDir} },
							{ field: "lendInst", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "融出方机构" },
							{ field: "firstSettlementDate", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "首次结算日" },
							{ field: "secondSettlementDate", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "到期结算日" },
							// { field: "firstSettlementMethod", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "首次结算方式" },
							// { field: "secondSettlementMethod", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "到期结算方式" },
							{ field: "borrowInst", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "融入方机构" },
							// { field: "forDate", width: 110, headerAlign: "center", align:"center", allowSort: false, header: "成交日期" },
							{ field: "sponsor", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "审批发起人" },
							{ field: "sponInst", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "审批发起机构" },
							{ field: "aDate", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批发起时间" }
				        ]
			}else if(id == '452'){//债券发行
				columns = [
							{ type: "indexcolumn", headerAlign:"center", width:"40", header: "序号"},
							{ field: "ticketId", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批单编号" },
							{ field: "contractId", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "成交单编号" },
							// { field: "dealNo", width: 80, headerAlign: "center", align:"center", allowSort: false, header: "opics交易号" },
							// { field: "statcode", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "opics处理状态" },
							{ field: "forDate", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "成交日期" },
							{ field: "taskName", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批状态" },
							{ field: "depositCode", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "债券代码" },
							{ field: "depositName", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "债券简称" },
							{ field: "depositTerm", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "债券期限" },
							{ field: "interestType", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "息票类型" },
							{ field: "offerAmount", width: 120, headerAlign: "center", align:"center", allowSort: false,numberFormat:"#,0.0000", header: "认购量总额" },
							{ field: "shouldPayMoney", width: 120, headerAlign: "center", align:"center", allowSort: false,numberFormat:"#,0.0000", header: "应缴金额总额" },
							{ field: "publishDate", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "发行日" },
							{ field: "valueDate", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "起息日" },
							{ field: "duedate", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "到期日" },
							{ field: "publishPrice", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "发行价格" },
							{ field: "benchmarkcurvename", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "参考收益率(%)" },
							{ field: "basis", type: "comboboxcolumn", autoShowPopup: true, width: 120, headerAlign: "center", align:"center", header: "计息基准", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.Basis} },
							{ field: "sponsor", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "审批发起人" },
							{ field: "aDate", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批发起时间" }
				        ]
			}else if(id == '425'){//存单发行
				columns = [
							{ type: "indexcolumn", headerAlign:"center", width:"40", header: "序号"},
							{ field: "ticketId", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批单编号" },
							{ field: "contractId", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "成交单编号" },
							// { field: "dealNo", width: 80, headerAlign: "center", align:"center", allowSort: false, header: "opics交易号" },
							// { field: "statcode", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "opics处理状态" },
							{ field: "forDate", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "成交日期" },
							{ field: "taskName", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批状态" },
							{ field: "depositCode", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "存单代码" },
							{ field: "depositName", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "存单简称" },
							{ field: "depositTerm", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "存单期限" },
							{ field: "interestType", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "息票类型" },
							{ field: "offerAmount", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "认购量总额" },
							{ field: "shouldPayMoney", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "应缴金额总额" },
							{ field: "publishDate", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "发行日" },
							{ field: "valueDate", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "起息日" },
							{ field: "duedate", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "到期日" },
							{ field: "publishPrice", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "发行价格" },
							{ field: "benchmarkcurvename", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "参考收益率(%)" },
							{ field: "basis", type: "comboboxcolumn", autoShowPopup: true, width: 120, headerAlign: "center", align:"center", header: "计息基准", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.Basis} },
							{ field: "sponsor", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "审批发起人" },
							{ field: "aDate", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批发起时间" }
				        ]
			}else if(id == '471'){//MBS
				columns = [
							{ type: "indexcolumn", headerAlign:"center", width:"40", header: "序号"},
							{ field: "ticketId", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "审批单编号" },
							{ field: "contractId", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "成交单编号" },
							{ field: "dealNo", width: 80, headerAlign: "center", align:"center", allowSort: false, header: "opics交易号" },
							{ field: "statcode", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "opics处理状态" },
							{ field: "bondName", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "债券名称" },
							{ field: "taskName", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批状态" },
							{ field: "invtype", type: "comboboxcolumn", autoShowPopup: true, width: 100, headerAlign: "center", align:"center", header: "投资类型", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.invtype} },
							{ field: "myDir", type: "comboboxcolumn", autoShowPopup: true, width: 100, headerAlign: "center", align:"center", header: "本方方向", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.trading} },
							{ field: "sellInst", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "对手方机构名称" },
							{ field: "cleanPrice", width: 100, headerAlign: "center", align:"center", allowSort: false,numberFormat:"#,0.0000", header: "交易净价" },
							{ field: "yield", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "到期收益率（%）" },
							{ field: "tradeAmount", width: 150, headerAlign: "center", align:"center", allowSort: false,numberFormat:"#,0.0000", header: "交易金额（元）" },
							{ field: "forDate", width: 90, headerAlign: "center", align:"center", allowSort: false, header: "交易日" },
							{ field: "settlementAmount", width: 150, headerAlign: "center", align:"center", allowSort: false, numberFormat:"#,0.0000",header: "结算金额（元）" },
							{ field: "settlementDate", width: 90, headerAlign: "center", align:"center", allowSort: false, header: "结算日" },
							{ field: "totalFaceValue", width: 150, headerAlign: "center", align:"center", allowSort: false,numberFormat:"#,0.0000", header: "券面总额（万元）" },
							{ field: "settlementMethod", type: "comboboxcolumn", autoShowPopup: true, width: 90, headerAlign: "center", align:"center", allowSort: false, header: "结算方式" , editor: { type: "combobox", data: CommonUtil.serverData.dictionary.SettlementMethod} },
							{ field: "buyInst", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "买入方机构" },
							{ field: "sponsor", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "审批发起人" },
							{ field: "sponInst", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批发起机构" },
							{ field: "aDate", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批发起时间" }
				        ]
			}else if(id == '446'){//质押式回购
				columns = [
							{ type: "indexcolumn", headerAlign:"center", width:"40", header: "序号"},
							{ field: "ticketId", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批单编号" },
							{ field: "contractId", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "成交单编号" },
							{ field: "dealNo", width: 80, headerAlign: "center", align:"center", allowSort: false, header: "opics交易号" },
							{ field: "statcode", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "opics处理状态" },
							{ field: "taskName", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批状态" },
							{ field: "myDir", type: "comboboxcolumn", autoShowPopup: true, width: 120, headerAlign: "center", align:"center", header: "本方方向", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.trading} },
							{ field: "reverseInst", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "对手方机构名称" },
							{ field: "tradeAmount", width: 120, headerAlign: "center", align:"center", allowSort: false,numberFormat:"#,0.0000", header: "交易金额" },
							{ field: "repoRate", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "回购利率(%)" },
							{ field: "forDate", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "起息日" },
							{ field: "secondSettlementDate", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "到期结算日" },
							{ field: "tenor", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "期限" },
							{ field: "occupancyDays", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "实际占款天数" },
							{ field: "secondSettlementAmount", width: 180, headerAlign: "center", align:"center", allowSort: false, numberFormat:"#,0.0000",header: "到期结算金额（元）" },
							{ field: "basis", type: "comboboxcolumn", autoShowPopup: true, width: 120, headerAlign: "center", align:"center", header: "计息基准", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.Basis} },
							{ field: "positiveTrader", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "审批发起人" },
							{ field: "aDate", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批发起时间" }
				        ]
			}else if(id == '442'){//买断式回购
				columns = [
							{ type: "indexcolumn", headerAlign:"center", width:"40", header: "序号"},
							{ field: "ticketId", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批单编号" },
							{ field: "contractId", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "成交单编号" },
							{ field: "dealNo", width: 80, headerAlign: "center", align:"center", allowSort: false, header: "opics交易号" },
							{ field: "statcode", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "opics处理状态" },
							{ field: "taskName", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批状态" },
							{ field: "myDir", type: "comboboxcolumn", autoShowPopup: true, width: 120, headerAlign: "center", align:"center", header: "本方方向", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.trading} },
							{ field: "reverseInst", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "对手方机构名称" },
							{ field: "totalFaceValue", width: 150, headerAlign: "center", align:"center", allowSort: false, numberFormat:"#,0.0000",header: "交易金额（万元）" },
							{ field: "repoRate", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "回购利率(%)" },
							{ field: "forDate", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "起息日" },
							{ field: "secondSettlementDate", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "到期结算日" },
							{ field: "tenor", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "期限" },
							{ field: "occupancyDays", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "实际占款天数" },
							{ field: "secondSettlementAmount", width: 180, headerAlign: "center", align:"center", allowSort: false,numberFormat:"#,0.0000", header: "到期结算金额（元）" },
							{ field: "basis", type: "comboboxcolumn", autoShowPopup: true, width: 120, headerAlign: "center", align:"center", header: "计息基准", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.Basis} },
							{ field: "positiveTrader", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "审批发起人" },
							{ field: "aDate", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批发起时间" }
				        ]
			}else if(id == '467'){//国库定期存款
				columns = [
							{ type: "indexcolumn", headerAlign:"center", width:"40", header: "序号"},
							{ field: "ticketId", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批单编号" },
							{ field: "contractId", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "成交单编号" },
							{ field: "dealNo", width: 80, headerAlign: "center", align:"center", allowSort: false, header: "opics交易号" },
							{ field: "statcode", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "opics处理状态" },
							{ field: "taskName", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批状态" },
							{ field: "myDir", type: "comboboxcolumn", autoShowPopup: true, width: 120, headerAlign: "center", align:"center", header: "本方方向", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.trading} },
							{ field: "reverseInst", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "对手方机构名称" },
							{ field: "tradeAmount", width: 120, headerAlign: "center", align:"center", allowSort: false, numberFormat:"#,0.0000",header: "交易金额" },
							{ field: "repoRate", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "回购利率(%)" },
							{ field: "forDate", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "起息日" },
							{ field: "secondSettlementDate", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "到期结算日" },
							{ field: "tenor", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "期限" },
							{ field: "occupancyDays", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "实际占款天数" },
							{ field: "secondSettlementAmount", width: 180, headerAlign: "center", align:"center", allowSort: false, numberFormat:"#,0.0000",header: "到期结算金额（元）" },
							{ field: "basis", type: "comboboxcolumn", autoShowPopup: true, width: 120, headerAlign: "center", align:"center", header: "计息基准", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.Basis} },
							{ field: "positiveTrader", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "审批发起人" },
							{ field: "aDate", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批发起时间" }
				        ]
			}else if(id == '468'){//交易所回购
				columns = [
							{ type: "indexcolumn", headerAlign:"center", width:"40", header: "序号"},
							{ field: "ticketId", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批单编号" },
							{ field: "contractId", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "成交单编号" },
							{ field: "dealNo", width: 80, headerAlign: "center", align:"center", allowSort: false, header: "opics交易号" },
							{ field: "statcode", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "opics处理状态" },
							{ field: "taskName", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批状态" },
							{ field: "myDir", type: "comboboxcolumn", autoShowPopup: true, width: 120, headerAlign: "center", align:"center", header: "本方方向", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.trading} },
							{ field: "reverseInst", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "对手方机构名称" },
							{ field: "tradeAmount", width: 120, headerAlign: "center", align:"center", allowSort: false, numberFormat:"#,0.0000",header: "交易金额" },
							{ field: "repoRate", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "回购利率(%)" },
							{ field: "forDate", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "起息日" },
							{ field: "secondSettlementDate", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "到期结算日" },
							{ field: "tenor", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "期限" },
							{ field: "occupancyDays", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "实际占款天数" },
							{ field: "secondSettlementAmount", width: 180, headerAlign: "center", align:"center", allowSort: false,numberFormat:"#,0.0000", header: "到期结算金额（元）" },
							{ field: "basis", type: "comboboxcolumn", autoShowPopup: true, width: 120, headerAlign: "center", align:"center", header: "计息基准", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.Basis} },
							{ field: "positiveTrader", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "审批发起人" },
							{ field: "aDate", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批发起时间" }
				        ]
			}else if(id == '469'){//常备借贷便利
				columns = [
							{ type: "indexcolumn", headerAlign:"center", width:"40", header: "序号"},
							{ field: "ticketId", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批单编号" },
							{ field: "contractId", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "成交单编号" },
							{ field: "dealNo", width: 80, headerAlign: "center", align:"center", allowSort: false, header: "opics交易号" },
							{ field: "statcode", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "opics处理状态" },
							{ field: "taskName", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批状态" },
							{ field: "myDir", type: "comboboxcolumn", autoShowPopup: true, width: 120, headerAlign: "center", align:"center", header: "本方方向", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.trading} },
							{ field: "reverseInst", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "对手方机构名称" },
							{ field: "tradeAmount", width: 120, headerAlign: "center", align:"center", allowSort: false,numberFormat:"#,0.0000", header: "交易金额" },
							{ field: "repoRate", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "回购利率(%)" },
							{ field: "forDate", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "起息日" },
							{ field: "secondSettlementDate", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "到期结算日" },
							{ field: "tenor", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "期限" },
							{ field: "occupancyDays", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "实际占款天数" },
							{ field: "secondSettlementAmount", width: 180, headerAlign: "center", align:"center", allowSort: false,numberFormat:"#,0.0000", header: "到期结算金额（元）" },
							{ field: "basis", type: "comboboxcolumn", autoShowPopup: true, width: 120, headerAlign: "center", align:"center", header: "计息基准", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.Basis} },
							{ field: "positiveTrader", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "审批发起人" },
							{ field: "aDate", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批发起时间" }
				        ]
			}else if(id == '470'){//中期借贷便利
				columns = [
							{ type: "indexcolumn", headerAlign:"center", width:"40", header: "序号"},
							{ field: "ticketId", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批单编号" },
							{ field: "contractId", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "成交单编号" },
							{ field: "dealNo", width: 80, headerAlign: "center", align:"center", allowSort: false, header: "opics交易号" },
							{ field: "statcode", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "opics处理状态" },
							{ field: "taskName", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批状态" },
							{ field: "myDir", type: "comboboxcolumn", autoShowPopup: true, width: 120, headerAlign: "center", align:"center", header: "本方方向", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.trading} },
							{ field: "reverseInst", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "对手方机构名称" },
							{ field: "tradeAmount", width: 120, headerAlign: "center", align:"center", allowSort: false, numberFormat:"#,0.0000",header: "交易金额" },
							{ field: "repoRate", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "回购利率(%)" },
							{ field: "forDate", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "起息日" },
							{ field: "secondSettlementDate", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "到期结算日" },
							{ field: "tenor", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "期限" },
							{ field: "occupancyDays", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "实际占款天数" },
							{ field: "secondSettlementAmount", width: 180, headerAlign: "center", align:"center", allowSort: false, numberFormat:"#,0.0000",header: "到期结算金额（元）" },
							{ field: "basis", type: "comboboxcolumn", autoShowPopup: true, width: 120, headerAlign: "center", align:"center", header: "计息基准", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.Basis} },
							{ field: "positiveTrader", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "审批发起人" },
							{ field: "aDate", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批发起时间" }
				        ]
			}else if(id == '472'){//线下押券
				columns = [
							{ type: "indexcolumn", headerAlign:"center", width:"40", header: "序号"},
							{ field: "ticketId", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批单编号" },
							{ field: "contractId", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "成交单编号" },
							{ field: "dealNo", width: 80, headerAlign: "center", align:"center", allowSort: false, header: "opics交易号" },
							{ field: "statcode", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "opics处理状态" },
							{ field: "taskName", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批状态" },
							{ field: "myDir", type: "comboboxcolumn", autoShowPopup: true, width: 120, headerAlign: "center", align:"center", header: "本方方向", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.trading} },
							{ field: "reverseInst", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "对手方机构名称" },
							{ field: "tradeAmount", width: 120, headerAlign: "center", align:"center", allowSort: false,numberFormat:"#,0.0000", header: "交易金额" },
							{ field: "repoRate", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "回购利率(%)" },
							{ field: "forDate", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "起息日" },
							{ field: "secondSettlementDate", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "到期结算日" },
							{ field: "tenor", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "期限" },
							{ field: "occupancyDays", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "实际占款天数" },
							{ field: "secondSettlementAmount", width: 180, headerAlign: "center", align:"center", allowSort: false,numberFormat:"#,0.0000", header: "到期结算金额（元）" },
							{ field: "basis", type: "comboboxcolumn", autoShowPopup: true, width: 120, headerAlign: "center", align:"center", header: "计息基准", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.Basis} },
							{ field: "positiveTrader", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "审批发起人" },
							{ field: "aDate", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批发起时间" }
				        ]
			}else if(id == '473'){//交易所回购(买断式)
				columns = [
					{ type: "indexcolumn", headerAlign:"center", width:"40", header: "序号"},
					{ field: "ticketId", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批单编号" },
					{ field: "contractId", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "成交单编号" },
					{ field: "dealNo", width: 80, headerAlign: "center", align:"center", allowSort: false, header: "opics交易号" },
					{ field: "statcode", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "opics处理状态" },
					{ field: "taskName", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批状态" },
					{ field: "myDir", type: "comboboxcolumn", autoShowPopup: true, width: 120, headerAlign: "center", align:"center", header: "本方方向", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.trading} },
					{ field: "reverseInst", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "对手方机构名称" },
					{ field: "totalFaceValue", width: 150, headerAlign: "center", align:"center", allowSort: false, numberFormat:"#,0.0000",header: "交易金额（万元）" },
					{ field: "repoRate", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "回购利率(%)" },
					{ field: "forDate", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "起息日" },
					{ field: "secondSettlementDate", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "到期结算日" },
					{ field: "tenor", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "期限" },
					{ field: "occupancyDays", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "实际占款天数" },
					{ field: "secondSettlementAmount", width: 180, headerAlign: "center", align:"center", allowSort: false, numberFormat:"#,0.0000",header: "到期结算金额（元）" },
					{ field: "basis", type: "comboboxcolumn", autoShowPopup: true, width: 120, headerAlign: "center", align:"center", header: "计息基准", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.Basis} },
					{ field: "positiveTrader", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "审批发起人" },
					{ field: "aDate", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批发起时间" }
				]
			}else if(id == '441'){//利率互换
				columns = [
							{ type: "indexcolumn", headerAlign:"center", width:"40", header: "序号"},
							{ field: "ticketId", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "审批单编号" },
							{ field: "contractId", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "成交单编号" },
							{ field: "dealNo", width: 80, headerAlign: "center", align:"center", allowSort: false, header: "opics交易号" },
							{ field: "statcode", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "opics处理状态" },
							{ field: "myDir", type: "comboboxcolumn", autoShowPopup: true, width: 100, headerAlign: "center", align:"center", header: "本方方向", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.trading} },
							{ field: "floatInst", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "对手方机构名称" },
							{ field: "lastQty", width: 150, headerAlign: "center", align:"center", allowSort: false,numberFormat:"#,0.0000", header: "名义本金(万元)" },
							{ field: "rateCode", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "利率代码" },
							{ field: "startDate", width: 90, headerAlign: "center", align:"center", allowSort: false, header: "起息日" },
							{ field: "endDate", width: 90, headerAlign: "center", align:"center", allowSort: false, header: "到期日" },
							{ field: "productName", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "产品名称" },
							{ field: "fixedInst", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "固定利率支付方机构" },
							{ field: "calculateAgency", type: "comboboxcolumn", autoShowPopup: true, width: 110, headerAlign: "center", align:"center", header: "计算机构", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.calInst} },
							{ field: "tradeMethod", type: "comboboxcolumn", autoShowPopup: true, width: 100, headerAlign: "center", align:"center", header: "清算方式", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.CleanMethoed} },
							{ field: "forDate", width: 90, headerAlign: "center", align:"center", allowSort: false, header: "成交日期" },
							{ field: "taskName", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批状态" },
							{ field: "sponsor", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "审批发起人" },
							{ field: "sponInst", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "审批发起机构" },
							{ field: "aDate", width: 130, headerAlign: "center", align:"center", allowSort: false, header: "审批发起时间" }
				        ]
			}else if(id == '803-1'||id == '802-1'|| id == '801-1'){//基金申购
				columns = [
							{ type: "indexcolumn", headerAlign:"center", width:"40", header: "序号"},
							{ field: "dealNo", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "审批单编号" },
							{ field: "taskName", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "审批状态" },
							{ field: "fundFullName", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "基金全称" },
							{ field: "cname", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "基金管理人名称" },
							{ field: "contact", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "基金经理" },
							{ field: "ccy", type: "comboboxcolumn", autoShowPopup: true, width: 100, headerAlign: "center", align:"center", header: "基金币种", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.Currency} },
							{ field: "shareAmt", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "申购份额（份）" },
							{ field: "amt", width: 120, headerAlign: "center", align:"center", allowSort: false,numberFormat:"#,0.0000", header: "申购金额（元）" },
							{ field: "price", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "单位净值（元）" },
							{ field: "tdate", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "交易日期" },
							{ field: "invType", type: "comboboxcolumn", autoShowPopup: true, width: 300, headerAlign: "center", align:"center", header: "会计类型", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.tbAccountType} },
							{ field: "sponsor", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "审批发起人" },
							{ field: "sponInstName", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批发起机构" },
							{ field: "aDate", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批发起时间" }
				        ]
			}else if(id == '803-2'||id == '802-2'||id == '801-2'){//基金申购确认
				columns = [
							{ type: "indexcolumn", headerAlign:"center", width:"40", header: "序号"},
							{ field: "dealNo", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "审批单编号" },
							{ field: "refNo", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "关联编号" },
							{ field: "taskName", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "审批状态" },
							{ field: "fundFullName", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "基金全称" },
							{ field: "cname", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "基金管理人名称" },
							{ field: "contact", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "基金经理" },
							{ field: "ccy", type: "comboboxcolumn", autoShowPopup: true, width: 100, headerAlign: "center", align:"center", header: "基金币种", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.Currency} },
							{ field: "shareAmt", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "申购份额（份）" },
							{ field: "amt", width: 120, headerAlign: "center", align:"center", allowSort: false, numberFormat:"#,0.0000",header: "申购金额（元）" },
							{ field: "price", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "单位净值（元）" },
							{ field: "handleAmt", width: 120, headerAlign: "center", align:"center", allowSort: false,numberFormat:"#,0.0000", header: "手续费（元）" },
							{ field: "tdate", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "交易日期" },
							{ field: "invType", type: "comboboxcolumn", autoShowPopup: true, width: 300, headerAlign: "center", align:"center", header: "会计类型", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.tbAccountType} },
							{ field: "sponsor", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "审批发起人" },
							{ field: "sponInstName", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批发起机构" },
							{ field: "aDate", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批发起时间" }
				        ]
			}else if(id == '803-3'||id == '802-3'||id == '801-3'){//基金赎回
				columns = [
							{ type: "indexcolumn", headerAlign:"center", width:"40", header: "序号"},
							{ field: "dealNo", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "审批单编号" },
							{ field: "taskName", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "审批状态" },
							{ field: "fundFullName", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "基金全称" },
							{ field: "cname", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "基金管理人名称" },
							{ field: "contact", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "基金经理" },
							{ field: "ccy", type: "comboboxcolumn", autoShowPopup: true, width: 100, headerAlign: "center", align:"center", header: "基金币种", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.Currency} },
							{ field: "tdate", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "回款日期" },
							{ field: "vdate", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "赎回日期" },
							{ field: "shareAmt", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "赎回份额（份）" },
							{ field: "invType", type: "comboboxcolumn", autoShowPopup: true, width: 300, headerAlign: "center", align:"center", header: "会计类型", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.tbAccountType} },
							{ field: "sponsor", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "审批发起人" },
							{ field: "sponInstName", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批发起机构" },
							{ field: "aDate", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批发起时间" }
				        ]
			}else if(id == '803-4'||id == '802-4'||id == '801-4'){//基金赎回确认
				columns = [
							{ type: "indexcolumn", headerAlign:"center", width:"40", header: "序号"},
							{ field: "dealNo", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "审批单编号" },
							{ field: "refNo", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "关联编号" },
							{ field: "taskName", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "审批状态" },
							{ field: "fundFullName", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "基金全称" },
							{ field: "cname", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "基金管理人名称" },
							{ field: "contact", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "基金经理" },
							{ field: "ccy", type: "comboboxcolumn", autoShowPopup: true, width: 100, headerAlign: "center", align:"center", header: "基金币种", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.Currency} },
							{ field: "tdate", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "回款日期" },
							{ field: "vdate", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "赎回日期" },
							{ field: "shareAmt", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "赎回份额（份）" },
							{ field: "amt", width: 120, headerAlign: "center", align:"center", allowSort: false,numberFormat:"#,0.0000", header: "赎回金额（元）" },
							{ field: "intamt", width: 120, headerAlign: "center", align:"center", allowSort: false,numberFormat:"#,0.0000", header: "赎回红利（元）" },
							{ field: "price", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "单位净值（元）" },
							{ field: "handleAmt", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "手续费（元）" },
							{ field: "invType", type: "comboboxcolumn", autoShowPopup: true, width: 300, headerAlign: "center", align:"center", header: "会计类型", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.tbAccountType} },
							{ field: "sponsor", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "审批发起人" },
							{ field: "sponInstName", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批发起机构" },
							{ field: "aDate", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批发起时间" }
				        ]
			}else if(id == '808'){//基金现金分红
				columns = [
							{ type: "indexcolumn", headerAlign:"center", width:"40", header: "序号"},
							{ field: "dealNo", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "审批单编号" },
							{ field: "taskName", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "审批状态" },
							{ field: "fundFullName", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "基金全称" },
							{ field: "cname", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "基金管理人名称" },
							{ field: "contact", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "基金经理" },
							{ field: "ccy", type: "comboboxcolumn", autoShowPopup: true, width: 100, headerAlign: "center", align:"center", header: "基金币种", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.Currency} },
							{ field: "bshareAmt", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "分红时刻持仓（份）" },
							{ field: "amt", width: 150, headerAlign: "center", align:"center", allowSort: false,numberFormat:"#,0.0000", header: "分红金额（元）" },
							{ field: "price", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "单位净值（元）" },
							{ field: "vdate", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "宣告日期" },
							{ field: "tdate", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "分红日期" },
							{ field: "invType", type: "comboboxcolumn", autoShowPopup: true, width: 300, headerAlign: "center", align:"center", header: "会计类型", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.tbAccountType} },
							{ field: "sponsor", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "审批发起人" },
							{ field: "sponInstName", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批发起机构" },
							{ field: "aDate", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批发起时间" }
				        ]
			}else if(id == '809'){//基金红利再投
				columns = [
							{ type: "indexcolumn", headerAlign:"center", width:"40", header: "序号"},
							{ field: "dealNo", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "审批单编号" },
							{ field: "taskName", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "审批状态" },
							{ field: "fundFullName", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "基金全称" },
							{ field: "cname", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "基金管理人名称" },
							{ field: "contact", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "基金经理" },
							{ field: "ccy", type: "comboboxcolumn", autoShowPopup: true, width: 100, headerAlign: "center", align:"center", header: "基金币种", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.Currency} },
							{ field: "eshareAmt", width: 120, headerAlign: "center", align:"center", allowSort: false, header: "转投份额（份）" },
							{ field: "eAmt", width: 120, headerAlign: "center", align:"center", allowSort: false,numberFormat:"#,0.0000", header: "转投金额（元）" },
							{ field: "vdate", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "转投时间" },
							{ field: "invType", type: "comboboxcolumn", autoShowPopup: true, width: 300, headerAlign: "center", align:"center", header: "会计类型", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.tbAccountType} },
							{ field: "sponsor", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "审批发起人" },
							{ field: "sponInstName", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批发起机构" },
							{ field: "aDate", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批发起时间" }
				        ]
			}else if(id == '439'){//外币头寸调拨 ccyPositionAllocation
				columns = [
							{ type: "indexcolumn", headerAlign:"center", width:"40", header: "序号"},
				           	{ field: "amount", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "Amount" },
				           	{ field: "discount", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "Discount" },
				           	{ field: "cal", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "Cal" },
				           	{ field: "ccy", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "Ccy" },
				           	{ field: "payDate", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "Pay date" },
							{ field: "rule", type: "comboboxcolumn", autoShowPopup: true, width: 100, headerAlign: "center", align:"center", header: "Rule", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.AllotRule} },
							{ field: "contractId", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "成交单编号" },
							{ field: "approveStatus", type: "comboboxcolumn", autoShowPopup: true, width: 120, headerAlign: "center", align:"center", header: "审批状态", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.ApproveStatus} },
							{ field: "dealTransType", type: "comboboxcolumn", autoShowPopup: true, width: 100, headerAlign: "center", align:"center", header: "交易状态", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.dealTransType} },
							{ field: "sponsor", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "审批发起人" },
							{ field: "sponInst", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "审批发起机构" },
							{ field: "aDate", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批发起时间" },
							{ field: "ticketId", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "审批单编号" }
				        ]
			}else if(id == '431'){//货币掉期 ccyswap
				columns = [
							{ type: "indexcolumn", headerAlign:"center", width:"40", header: "序号"},
							{ field: "dealNo", width: 80, headerAlign: "center", align:"center", allowSort: false, header: "opics交易号" },
							{ field: "statcode", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "opics处理状态" },
				           	{ field: "leftNotion", width: 200, headerAlign: "center", align:"center", allowSort: false,numberFormat:"#,0.0000", header: "名义本金（左）" },
				           	{ field: "rightNotion", width: 200, headerAlign: "center", align:"center", allowSort: false,numberFormat:"#,0.0000", header: "名义本金（右）" },
							{ field: "leftDirection", type: "comboboxcolumn", autoShowPopup: true, width: 150, headerAlign: "center", align:"center", header: "付款方向（左）", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.trading} },
							{ field: "contractId", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "成交单编号" },
							{ field: "approveStatus", type: "comboboxcolumn", autoShowPopup: true, width: 120, headerAlign: "center", align:"center", header: "审批状态", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.ApproveStatus} },
							{ field: "dealTransType", type: "comboboxcolumn", autoShowPopup: true, width: 100, headerAlign: "center", align:"center", header: "交易状态", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.dealTransType} },
				           	{ field: "instId", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "本方机构" },
				           	{ field: "counterpartyInstId", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "对方机构" },
				           	{ field: "forDate", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "交易日期" },
				           	{ field: "tradingModel", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "交易模式" },
							{ field: "tradingType", type: "comboboxcolumn", autoShowPopup: true, width: 200, headerAlign: "center", align:"center", header: "交易方式", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.TradeType} },
				           	{ field: "startDate", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "开始日期" },
				           	{ field: "unadjustmaturityDate", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "未调整到日期" },
				           	{ field: "sponsor", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "审批发起人" },
							{ field: "sponInst", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "审批发起机构" },
							{ field: "aDate", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "审批发起时间" },
							{ field: "ticketId", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "审批单编号" }
				        ]
			}else if(id == '433'){//人民币期权 rmbopt
				columns = [
							{ type: "indexcolumn", headerAlign:"center", width:"40", header: "序号"},
				           	{ field: "forDate", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "交易日期" },
				           	{ field: "forTime", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "交易时间" },
				           	{ field: "dealNo", width: 80, headerAlign: "center", align:"center", allowSort: false, header: "opics交易号" },
							{ field: "statcode", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "opics处理状态" },
							{ field: "dealSource", type: "comboboxcolumn", autoShowPopup: true, width: 100, headerAlign: "center", align:"center", header: "交易来源", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.TradeSource} },
							{ field: "contractId", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "成交单编号" },
							{ field: "taskName", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批状态" },
							{ field: "instId", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "本方机构" },
				           	{ field: "counterpartyInstId", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "对方机构" },
							{ field: "direction", type: "comboboxcolumn", autoShowPopup: true, width: 100, headerAlign: "center", align:"center", header: "交易方向", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.trading} },
				           	{ field: "buyNotional", width: 150, headerAlign: "center", align:"center", allowSort: false,numberFormat:"#,0.0000", header: "名义本金（买）" },
				           	{ field: "sellNotional", width: 150, headerAlign: "center", align:"center", allowSort: false,numberFormat:"#,0.0000", header: "名义本金（卖）" },
							{ field: "currencyPair", type: "comboboxcolumn", autoShowPopup: true, width: 100, headerAlign: "center", align:"center", header: "货币对", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.CurrencyPair} },
				           	{ field: "strikePrice", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "执行价" },
				           	{ field: "expiryDate", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "行权日" },
				           	{ field: "deliveryDate", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "交割日" },
							{ field: "deliveryType", type: "comboboxcolumn", autoShowPopup: true, width: 100, headerAlign: "center", align:"center", header: "交割方式", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.DeliveryType} },
				           	{ field: "exerciseType", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "执行方式" },
				           	{ field: "sponsor", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "审批发起人" },
							{ field: "sponInst", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "审批发起机构" },
							{ field: "aDate", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "审批发起时间" },
							{ field: "ticketId", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "审批单号" }
				        ]
			}else if(id == '440'){//外汇对即期 ccypspot
				columns = [
							{ type: "indexcolumn", headerAlign:"center", width:"40", header: "序号"},
							{ field: "currencyPair", type: "comboboxcolumn", autoShowPopup: true, width: 100, headerAlign: "center", align:"center", header: "货币对", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.CurrencyPair} },
							{ field: "direction", type: "comboboxcolumn", autoShowPopup: true, width: 100, headerAlign: "center", align:"center", header: "买卖方向", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.trading} },
				           	{ field: "capital", width: 165, headerAlign: "center", align:"center", allowSort: false, header: "本金" },
				           	{ field: "price", width: 165, headerAlign: "center", align:"center", allowSort: false, header: "价格" },
							{ field: "contractId", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "成交单编号" },
							{ field: "approveStatus", type: "comboboxcolumn", autoShowPopup: true, width: 120, headerAlign: "center", align:"center", header: "审批状态", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.ApproveStatus} },
							{ field: "dealTransType", type: "comboboxcolumn", autoShowPopup: true, width: 100, headerAlign: "center", align:"center", header: "交易状态", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.dealTransType} },
							{ field: "instId", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "本方机构" },
				           	{ field: "counterpartyInstId", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "对方机构" },
				           	{ field: "forDate", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "交易日期" },
							{ field: "tradingModel", type: "comboboxcolumn", autoShowPopup: true, width: 100, headerAlign: "center", align:"center", header: "交易模式", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.TradeModel} },
							{ field: "tradingType", type: "comboboxcolumn", autoShowPopup: true, width: 100, headerAlign: "center", align:"center", header: "交易方式", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.TradeType} },
							{ field: "valueDate", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "起息日" },
				           	{ field: "sponsor", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "审批发起人" },
							{ field: "sponInst", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "审批发起机构" },
							{ field: "aDate", width: 180, headerAlign: "center", align:"center", allowSort: false, header: "审批发起时间" },
							{ field: "ticketId", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "审批单号" }
				        ]
			}else if(id == '450'){//黄金即期
				columns = [
							{ type: "indexcolumn", headerAlign:"center", width:"40", header: "序号"},
							{ field: "ticketId", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "审批单编号" },
							{ field: "contractId", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "成交单编号" },
							{ field: "dealNo", width: 80, headerAlign: "center", align:"center", allowSort: false, header: "opics交易号" },
							{ field: "statcode", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "opics处理状态" },
							{ field: "forDate", width: 130, headerAlign: "center", align:"center", allowSort: false, header: "交易日期" },
							{ field: "dealSource", type: "comboboxcolumn", autoShowPopup: true, width: 100, headerAlign: "center", align:"center", header: "交易来源", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.TradeSource} },
							{ field: "currencyPair", type: "comboboxcolumn", autoShowPopup: false, width: 100, headerAlign: "center", align:"center", header: "货币对", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.CurrencyPair} },
							{ field: "quantity", width: 150, headerAlign: "center", align:"right", allowSort: false, numberFormat:"n4", header: "数量" },
							{ field: "prix", width: 130, headerAlign: "center", align:"center", allowSort: false,numberFormat:"#,0.0000", header: "价格" },
							{ field: "amount1", width: 130, headerAlign: "center", align:"center", allowSort: false,numberFormat:"#,0.0000", header: "金额" },
							{ field: "direction1", type: "comboboxcolumn", autoShowPopup: true, width: 100, headerAlign: "center", align:"center", header: "交易方向", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.trading} },
							{ field: "valueDate1", width: 130, headerAlign: "center", align:"center", allowSort: false, header: "起息日" },
							{ field: "taskName", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "审批状态" },
							{ field: "instId", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "本方机构" },
							{ field: "counterpartyInstId", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "对手方机构" },
							{ field: "sponsor", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "审批发起人" },
							{ field: "sponInst", width: 250, headerAlign: "center", align:"center", allowSort: false, header: "审批发起机构" },
							{ field: "aDate", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "审批发起时间" }
				        ]
			}else if(id == '448'){//黄金远期
				columns = [
							{ type: "indexcolumn", headerAlign:"center", width:"40", header: "序号"},
							{ field: "ticketId", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "审批单编号" },
							{ field: "contractId", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "成交单编号" },
							{ field: "dealNo", width: 80, headerAlign: "center", align:"center", allowSort: false, header: "opics交易号" },
							{ field: "statcode", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "opics处理状态" },
							{ field: "forDate", width: 130, headerAlign: "center", align:"center", allowSort: false, header: "交易日期" },
							{ field: "dealSource", type: "comboboxcolumn", autoShowPopup: true, width: 100, headerAlign: "center", align:"center", header: "交易来源", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.TradeSource} },
							{ field: "currencyPair", type: "comboboxcolumn", autoShowPopup: false, width: 100, headerAlign: "center", align:"center", header: "货币对", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.CurrencyPair} },
							{ field: "quantity", width: 150, headerAlign: "center", align:"right", allowSort: false, numberFormat:"n4", header: "数量" },
							{ field: "prix", width: 130, headerAlign: "center", align:"center", allowSort: false,numberFormat:"#,0.0000", header: "价格" },
							{ field: "amount1", width: 130, headerAlign: "center", align:"center", allowSort: false,numberFormat:"#,0.0000", header: "金额" },
							{ field: "direction1", type: "comboboxcolumn", autoShowPopup: true, width: 100, headerAlign: "center", align:"center", header: "交易方向", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.trading} },
							{ field: "valueDate1", width: 130, headerAlign: "center", align:"center", allowSort: false, header: "起息日" },
							{ field: "taskName", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "审批状态" },
							{ field: "instId", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "本方机构" },
							{ field: "counterpartyInstId", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "对手方机构" },
							{ field: "sponsor", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "审批发起人" },
							{ field: "sponInst", width: 250, headerAlign: "center", align:"center", allowSort: false, header: "审批发起机构" },
							{ field: "aDate", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "审批发起时间" }
				        ]
			}else if(id == '449'){//黄金掉期
				columns = [
							{ type: "indexcolumn", headerAlign:"center", width:"40", header: "序号"},
							{ field: "ticketId", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "审批单编号" },
							{ field: "contractId", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "成交单编号" },
							{ field: "dealNo", width: 80, headerAlign: "center", align:"center", allowSort: false, header: "opics交易号" },
							{ field: "statcode", width: 150, headerAlign: "center", align:"center", allowSort: false, header: "opics处理状态" },
							{ field: "swapDealno", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "远端交易号" },
							{ field: "statcode", type: "comboboxcolumn", autoShowPopup: true, width: 100, headerAlign: "center", align:"center", allowSort: false, header: "opics处理状态" , editor: { type: "combobox", data: CommonUtil.serverData.dictionary.statcode} },
							{ field: "forDate", width: 130, headerAlign: "center", align:"center", allowSort: false, header: "交易日期" },
							{ field: "dealSource", type: "comboboxcolumn", autoShowPopup: true, width: 100, headerAlign: "center", align:"center", header: "交易来源", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.TradeSource} },
							{ field: "currencyPair", type: "comboboxcolumn", autoShowPopup: false, width: 100, headerAlign: "center", align:"center", header: "货币对", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.CurrencyPair} },
							{ field: "quantity", width: 150, headerAlign: "center", align:"right", allowSort: false, numberFormat:"n4", header: "数量" },
							{ field: "direction1", type: "comboboxcolumn", autoShowPopup: true, width: 100, headerAlign: "center", align:"center", header: "交易方向", editor: { type: "combobox", data: CommonUtil.serverData.dictionary.trading} },
							{ field: "prix", width: 130, headerAlign: "center", align:"right", allowSort: false,numberFormat:"#,0.0000", header: "近端价格" },
							{ field: "amount1", width: 130, headerAlign: "center", align:"right", allowSort: false,numberFormat:"#,0.0000", header: "近端金额(即)" },
							{ field: "valueDate1",width: 100, headerAlign: "center", align:"center",allowSort: false, header: "起息日(即)" },
							{ field: "tenor2", width: 130, headerAlign: "center", align:"right", allowSort: false, numberFormat:"#,0.0000", header: "远端价格" },
							{ field: "amount2", width: 130, headerAlign: "center", align:"right", allowSort: false,numberFormat:"#,0.0000", header: "远端金额(远)" },
							{ field: "valueDate2", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "起息日(远)" },
							{ field: "taskName", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "审批状态" },
							{ field: "instId", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "本方机构" },
							{ field: "counterpartyInstId", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "对手方机构" },
							{ field: "sponsor", width: 100, headerAlign: "center", align:"center", allowSort: false, header: "审批发起人" },
							{ field: "sponInst", width: 250, headerAlign: "center", align:"center", allowSort: false, header: "审批发起机构" },
							{ field: "aDate", width: 200, headerAlign: "center", align:"center", allowSort: false, header: "审批发起时间" }
				        ]
			}
			return columns;
		}

		// 导出
		function exportExcel(){
            const form = new mini.Form("#search_form");
            const content = grid.getData();
            if(content.length == 0){
				mini.alert("请先查询数据");
				return;
			}
            const tree = mini.get("treeReport");
            const fileName = tree.getSelectedNode().text;//产品名称
            const flag = tree.getSelectedNode().id; // 产品代码
            const data = form.getData(true);
            data['deal'] = "deal"; // 判断是总交易查询还是审批台查询
            data['branchId']=branchId; // 审批发起机构
            data['sponInst']= instId;
            const queryParam = mini.encode(data); //序列化成JSON
			//前台直接导出
			downloadExcel(ExportTable(grid.getData(true),grid.columns),fileName);

			// mini.confirm("您确认要导出Excel吗?","系统提示", function (action) {
			// 		if (action == "ok"){
			// 			//异步查询需要导出的数据
			// 			CommonUtil.ajax({
			// 				url:tree.getSelectedNode().url,
			// 				data:queryParam,
			// 				callback : function(ret) {
            //                     const expParam = form.getData(true);
            //                     expParam['fileName'] = fileName;// 产品代码
			//
			// 				}
			// 			});
			// 		}
			// 	});//confirm end;
		}
		
		
		function approveLog() {
			var selections = grid.getSelecteds();
			var flow_type = Approve.FlowType.VerifyApproveFlow;
			if(selections.length <= 0){
				mini.alert("请选择要操作的数据","系统提示");
				return;
			}
			if(selections.length > 1){
				mini.alert("系统不支持多笔操作","系统提示");
				return;
			}
			if(selections[0].tradeSource == "3"){
				mini.alert("初始化导入的业务没有审批日志","系统提示");
				return false;
			}
			if(selections[0].ticketId==null||selections[0].ticketId ==undefined ||selections[0].ticketId ==""){
				Approve.approveLog(flow_type,selections[0].dealNo);
			}else {
				Approve.approveLog(flow_type,selections[0].ticketId);
			}
		}


	</script>
</body>
</html>