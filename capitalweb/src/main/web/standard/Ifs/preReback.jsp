<%@ page language="java" pageEncoding="UTF-8"%>
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/miniui/ColumnsMenu.js"></script>
<script src="<%=basePath%>/miniScript/common_mini.js" type="text/javascript"></script>
		
<span style="margin: 2px; display: block;">
	<a  id="add_btn" class="mini-button" style="display: none"   onclick="add()">手工新增</a>
	<a  id="edit_btn" class="mini-button" style="display: none"   onclick="edit()">修改</a>
	<a  id="delete_btn" class="mini-button" style="display: none"   onclick="del()">删除</a>
	<a  id="reback_btn" class="mini-button" style="display: none"   onclick="reback()">撤销</a>
	<a  id="recall_btn" class="mini-button" style="display: none"   onclick="recall()">撤回</a>
	<a  id="approve_commit_btn" class="mini-button" style="display: none"    onclick="approve()">审批</a>
	<a  id="approve_mine_commit_btn" class="mini-button" style="display: none"    onclick="commit()">提交审批</a>
	<a  id="approve_log" class="mini-button" style="display: none"     onclick="searchlog()">审批日志</a>
	<a  id="print_bk_btn" class="mini-button" style="display: none"    onclick="print()">打印</a>
	<a  id="batch_approve_btn" class="mini-button" style="display: none"    onclick="batchApprove()">批量审批</a>
	<a  id="batch_commit_btn" class="mini-button" style="display: none"    onclick="batchCommit()">批量提交</a>
	<a  id="opics_check_btn" class="mini-button" style="display: none"    onclick="opicsCheck()">要素批量更新</a>
	<a  id="export_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
	<div id = "approveType" name = "approveType" class="mini-checkboxlist" style="float:right;" 
		onvaluechanged="checkBoxValuechanged"  multiSelect="false" valueField="id" 
		labelStyle="text-align:right;border: none;background-color: #fff; " value="mine" textField="text"
		data="[{id:'mine',text:'我发起的列表'},{id:'approve',text:'待审批列表'},{id:'finished',text:'已审批列表'}]" >
	</div>
</span>	  	

<script>
	function rebackOld(){
		var rows=grid.getSelecteds();
		if(rows.length==0){
		mini.alert("请选择一条要撤销的交易!","提示");
			return;
		}
		mini.confirm("您确认要撤销该交易吗?","系统警告",function(value){   
         if (value=='ok'){
        	 var tableName="";
        	 switch (prdName){
	        	 case "外汇对即期":
	        		 tableName="IFS_CFETSFX_ONSPOT";
	        	   break;
	        	 case "利率互换":
	        		 tableName="IFS_CFETSRMB_IRS";
	        	   break;
	        	 case "买断式回购":
	        		 tableName="IFS_CFETSRMB_OR";
	        	   break;
	        	 case "现券买卖":
	        		 tableName="IFS_CFETSRMB_CBT";
	        	   break;
	        	 case "信用拆借":
	        		 tableName="IFS_CFETSRMB_IBO";
	        	   break;
	        	 case "债券借贷":
	        		 tableName="IFS_CFETSRMB_SL";
	        	   break;
	        	 case "质押式回购":
	        		 tableName="IFS_CFETSRMB_CR";
	        	   break;
	        	 case "黄金即期":
	        		 tableName="IFS_CFETSMETAL_GOLD";
	        	   break;
	        	 case "黄金远期":
	        		 tableName="IFS_CFETSMETAL_GOLD";
	        	   break;
	        	 case "黄金掉期":
	        		 tableName="IFS_CFETSMETAL_GOLD";
	        	   break;
	        	 case "黄金拆借":
	        		 tableName="IFS_CFETGOLD_LEND";
	        	   break;
	        	 case "外币债":
	        		 tableName="IFS_CFETSFX_DEBT";
	        	   break;
	        	 case "货币掉期":
	        		 tableName="IFS_CFETSFX_CSWAP";
	        	   break;
	        	 case "外汇掉期":
	        		 tableName="IFS_CFETSFX_SWAP";
	        	   break;
	        	 case "人民币期权":
	        		 tableName="IFS_CFETSFX_OPTION";
	        	   break;
	        	 case "外币拆借":
	        		 tableName="IFS_CFETSFX_LEND";
	        	   break;
	        	 case "外币头寸调拨":
	        		 tableName="IFS_CFETSFX_ALLOT";
	        	   break;
	        	 case "外汇远期":
	        		 tableName="IFS_CFETSFX_FWD";
	        	   break;
		         case "外汇即期":
		    		 tableName="IFS_CFETSFX_SPT";
		    	 break;
		    	 }
	             var data=rows[0];
	             data['dealTransType']="N";//交易状态撤销
	             data['tableName']=tableName;
	             params=mini.encode(data);
	             CommonUtil.ajax( {
	                 url:"/IfsCurrencyController/rebackData",
	                 data:params,
	                 callback : function(data) {
	                     mini.alert("已成功撤销!","系统提示");
	                     search(10, 0);
                 	}
            	 });
        	 }
     	});
	}
	
	function recall(){
		var rows=grid.getSelecteds();
		if(rows.length>1){
			mini.alert("系统不支持多条数据撤回!","提示");
			return;
		}
		if(rows.length==0){
			mini.alert("请选择一条要撤回的交易!","提示");
			return;
		}
		mini.confirm("您确认要撤回该交易吗?","系统警告",function(value){   
         if (value=='ok'){
	             var data=rows[0];
	             CommonUtil.ajax( {
	                 url:"/IfsFlowController/recall",
	                 data:{"serial_no":rows[0].ticketId},
	                 callback : function(data) {
	                     mini.alert("已成功撤回!","系统提示");
	                     search(10, 0);
                 	}
            	 });
        	 }
     	});
	}
	
	function reback(){
		var rows=grid.getSelecteds();
		if(rows.length>1){
			mini.alert("系统不支持多条数据进行撤销","消息提示");
			return;
		}
		if(rows.length==0){
			mini.alert("请选择一条要撤销的交易!","提示");
			return;
		}
		mini.confirm("您确认要撤销该交易吗?","系统警告",function(value){   
         if (value=='ok'){
	             var data=rows[0];
	             CommonUtil.ajax( {
	                 url:"/IfsFlowController/reback",
	                 data:{"serial_no":rows[0].ticketId},
	                 callback : function(data) {
	                     mini.alert("已成功撤销!","系统提示");
	                     search(10, 0);
                 	}
            	 });
        	 }
     	});
	}
	
     function checkBoxValuechanged(e){
		 $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
			 search(10, 0);
			 var approveType = mini.get("approveType").getValue();
			 if (userId != 'admin') {
				 if (approveType == "mine") {//我发起的
					 initButton();
				 } else if (approveType == "finished") {//已审批
					 if (visibleBtn.add_btn) mini.get("add_btn").setVisible(false);
					 if (visibleBtn.edit_btn) mini.get("edit_btn").setVisible(false);
					 if (visibleBtn.delete_btn) mini.get("delete_btn").setVisible(false);
					 if (visibleBtn.approve_commit_btn) mini.get("approve_commit_btn").setVisible(false);
					 if (visibleBtn.approve_mine_commit_btn) mini.get("approve_mine_commit_btn").setVisible(false);
					 if (visibleBtn.approve_log) mini.get("approve_log").setVisible(true);//审批日志
					 mini.get("approve_log").setEnabled(true);//审批日志高亮
					 if (visibleBtn.print_bk_btn) mini.get("print_bk_btn").setVisible(true);//打印
					 mini.get("print_bk_btn").setEnabled(true);//打印高亮
					 /*新增批量提交、批量审批、opics要素批量校验  */
					 if (visibleBtn.batch_commit_btn) mini.get("batch_commit_btn").setVisible(false);//批量提交不显示
					 mini.get("batch_commit_btn").setEnabled(false);//批量提交不高亮
					 if (visibleBtn.batch_approve_btn) mini.get("batch_approve_btn").setVisible(false);//批量审批不显示
					 mini.get("batch_approve_btn").setEnabled(false);//批量审批不高亮
					 if (visibleBtn.opics_check_btn) mini.get("opics_check_btn").setVisible(false);//opics要素批量校验 不显示
					 mini.get("opics_check_btn").setEnabled(false);//opics要素批量校验 不高亮
					 if (visibleBtn.reback_btn) mini.get("reback_btn").setVisible(false);//撤销 不显示
					 if (visibleBtn.recall_btn) mini.get("recall_btn").setVisible(false);//撤回 不显示
				 } else {//待审批
					 if (visibleBtn.add_btn) mini.get("add_btn").setVisible(false);
					 if (visibleBtn.edit_btn) mini.get("edit_btn").setVisible(false);
					 if (visibleBtn.delete_btn) mini.get("delete_btn").setVisible(false);
					 if (visibleBtn.approve_commit_btn) mini.get("approve_commit_btn").setVisible(true);//审批
					 mini.get("approve_commit_btn").setEnabled(true);//审批高亮
					 if (visibleBtn.approve_mine_commit_btn) mini.get("approve_mine_commit_btn").setVisible(false);//提交审批
					 if (visibleBtn.approve_log) mini.get("approve_log").setVisible(true);//审批日志
					 mini.get("approve_log").setEnabled(true);//审批日志高亮
					 if (visibleBtn.print_bk_btn) mini.get("print_bk_btn").setVisible(true);//打印
					 mini.get("print_bk_btn").setEnabled(true);//打印高亮
					 /*新增批量提交、批量审批、opics要素批量校验  */
					 if (visibleBtn.batch_commit_btn) mini.get("batch_commit_btn").setVisible(false);//批量提交不显示
					 mini.get("batch_commit_btn").setEnabled(false);//批量提交不高亮
					 if (visibleBtn.batch_approve_btn) mini.get("batch_approve_btn").setVisible(true);//批量审批显示
					 mini.get("batch_approve_btn").setEnabled(true);//批量审批高亮
					 if (visibleBtn.opics_check_btn) mini.get("opics_check_btn").setVisible(false);//opics要素批量校验 不显示
					 mini.get("opics_check_btn").setEnabled(false);//opics要素批量校验 不高亮
					 if (visibleBtn.reback_btn) mini.get("reback_btn").setVisible(false);//撤销 不显示
					 if (visibleBtn.recall_btn) mini.get("recall_btn").setVisible(false);//撤回 不显示
				 }
			 } else {
				 if (approveType == "mine") {//我发起的
					 initButton();
				 } else if (approveType == "finished") {//已审批
					 mini.get("add_btn").setVisible(false);
					 mini.get("edit_btn").setVisible(false);
					 mini.get("delete_btn").setVisible(false);
					 mini.get("approve_commit_btn").setVisible(false);
					 mini.get("approve_mine_commit_btn").setVisible(false);
					 mini.get("approve_log").setVisible(true);//审批日志
					 mini.get("approve_log").setEnabled(true);//审批日志高亮
					 mini.get("print_bk_btn").setVisible(true);//打印
					 mini.get("print_bk_btn").setEnabled(true);//打印高亮
					 /*新增批量提交、批量审批、opics要素批量校验  */
					 mini.get("batch_commit_btn").setVisible(false);//批量提交不显示
					 mini.get("batch_commit_btn").setEnabled(false);//批量提交不高亮
					 mini.get("batch_approve_btn").setVisible(false);//批量审批不显示
					 mini.get("batch_approve_btn").setEnabled(false);//批量审批不高亮
					 mini.get("opics_check_btn").setVisible(false);//opics要素批量校验 不显示
					 mini.get("opics_check_btn").setEnabled(false);//opics要素批量校验 不高亮
					 mini.get("reback_btn").setVisible(false);//撤销 不显示
					 mini.get("recall_btn").setVisible(false);//撤回 不显示
				 } else {//待审批
					 mini.get("add_btn").setVisible(false);
					 mini.get("edit_btn").setVisible(false);
					 mini.get("delete_btn").setVisible(false);
					 mini.get("approve_commit_btn").setVisible(true);//审批
					 mini.get("approve_commit_btn").setEnabled(true);//审批高亮
					 mini.get("approve_mine_commit_btn").setVisible(false);//提交审批
					 mini.get("approve_log").setVisible(true);//审批日志
					 mini.get("approve_log").setEnabled(true);//审批日志高亮
					 mini.get("print_bk_btn").setVisible(true);//打印
					 mini.get("print_bk_btn").setEnabled(true);//打印高亮
					 /*新增批量提交、批量审批、opics要素批量校验  */
					 mini.get("batch_commit_btn").setVisible(false);//批量提交不显示
					 mini.get("batch_commit_btn").setEnabled(false);//批量提交不高亮
					 mini.get("batch_approve_btn").setVisible(true);//批量审批显示
					 mini.get("batch_approve_btn").setEnabled(true);//批量审批高亮
					 mini.get("opics_check_btn").setVisible(false);//opics要素批量校验 不显示
					 mini.get("opics_check_btn").setEnabled(false);//opics要素批量校验 不高亮
					 mini.get("reback_btn").setVisible(false);//撤销 不显示
					 mini.get("recall_btn").setVisible(false);//撤回 不显示
				 }
			 }
		 });
 	}
    
    // 初始化界面
 	function initButton(){
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
			mini.get("export_btn").setVisible(true);
			if (userId != 'admin') {
				if (visibleBtn.add_btn) mini.get("add_btn").setVisible(true);
				mini.get("add_btn").setEnabled(true);
				if (visibleBtn.edit_btn) mini.get("edit_btn").setVisible(true);
				mini.get("edit_btn").setEnabled(true);
				if (visibleBtn.delete_btn) mini.get("delete_btn").setVisible(true);
				mini.get("delete_btn").setEnabled(true);
				if (visibleBtn.approve_commit_btn) mini.get("approve_commit_btn").setVisible(false);
				if (visibleBtn.approve_mine_commit_btn) mini.get("approve_mine_commit_btn").setVisible(true);//提交审批显示
				mini.get("approve_mine_commit_btn").setEnabled(true);//提交审批高亮
				if (visibleBtn.approve_log) mini.get("approve_log").setVisible(true);//审批日志
				mini.get("approve_log").setEnabled(true);//审批日志高亮
				if (visibleBtn.print_bk_btn) mini.get("print_bk_btn").setVisible(true);//打印
				mini.get("print_bk_btn").setEnabled(true);//打印高亮
				//新增批量提交、批量审批、opics要素批量校验
				if (visibleBtn.batch_commit_btn) mini.get("batch_commit_btn").setVisible(true);//批量提交显示
				mini.get("batch_commit_btn").setEnabled(true);//批量提交高亮
				if (visibleBtn.batch_approve_btn) mini.get("batch_approve_btn").setVisible(false);//批量审批不显示
				mini.get("batch_approve_btn").setEnabled(false);//批量审批不高亮
				if (visibleBtn.opics_check_btn) mini.get("opics_check_btn").setVisible(true);//opics要素批量校验 显示
				mini.get("opics_check_btn").setEnabled(true);//opics要素批量校验 高亮
				if (visibleBtn.reback_btn) mini.get("reback_btn").setVisible(true);//撤销 显示
				mini.get("reback_btn").setEnabled(true);//撤销  高亮
				if (visibleBtn.recall_btn) mini.get("recall_btn").setVisible(true);//撤回 显示
				mini.get("recall_btn").setEnabled(true);//撤回  高亮
			} else {
				mini.get("add_btn").setVisible(true);
				mini.get("add_btn").setEnabled(true);
				mini.get("edit_btn").setVisible(true);
				mini.get("edit_btn").setEnabled(true);
				mini.get("delete_btn").setVisible(true);
				mini.get("delete_btn").setEnabled(true);
				mini.get("approve_commit_btn").setVisible(false);
				mini.get("approve_mine_commit_btn").setVisible(true);//提交审批显示
				mini.get("approve_mine_commit_btn").setEnabled(true);//提交审批高亮
				mini.get("approve_log").setVisible(true);//审批日志
				mini.get("approve_log").setEnabled(true);//审批日志高亮
				mini.get("print_bk_btn").setVisible(true);//打印
				mini.get("print_bk_btn").setEnabled(true);//打印高亮
				//新增批量提交、批量审批、opics要素批量校验
				mini.get("batch_commit_btn").setVisible(true);//批量提交显示
				mini.get("batch_commit_btn").setEnabled(true);//批量提交高亮
				mini.get("batch_approve_btn").setVisible(false);//批量审批不显示
				mini.get("batch_approve_btn").setEnabled(false);//批量审批不高亮
				mini.get("opics_check_btn").setVisible(true);//opics要素批量校验 显示
				mini.get("opics_check_btn").setEnabled(true);//opics要素批量校验 高亮
				mini.get("reback_btn").setVisible(true);//撤销 显示
				mini.get("reback_btn").setEnabled(true);//撤销  高亮
				mini.get("search_btn").setVisible(true); //查询 显示
				mini.get("search_btn").setEnabled(true); //查询  高亮
				mini.get("clear_btn").setVisible(true);  //清空 显示
				mini.get("clear_btn").setEnabled(true);  //清空  高亮
				mini.get("recall_btn").setVisible(true);//撤回 显示
				mini.get("recall_btn").setEnabled(true);//撤回  高亮
			}
		});
 	}
 	
 	/***
 	 * 批量审批通过
 	 * 
 	 * @author zcm
 	 * 
 	 */
 	function selectCallBack(prdName){
 		var callBackName="";
      	 switch (prdName)
      	 {
      	 case "外汇对即期":
      		 callBackName="IfsOnspotService";
      	   break;
      	 case "利率互换":
      		 callBackName="IfsRmbIrsService";
      	   break;
      	 case "买断式回购":
      		 callBackName="IfsRmbOrService";
      	   break;
      	 case "现券买卖":
      		 callBackName="IfsRmbCbtService";
      	   break;
      	 case "信用拆借":
      		 callBackName="IfsRmbIboService";
      	   break;
      	 case "债券借贷":
      		 callBackName="IfsRmbSlService";
      	   break;
      	 case "质押式回购":
      		 callBackName="IfsRmbCrService";
      	   break;
      	 case "黄金即期":
      		 callBackName="IfsMetalGoldService";
      	   break;
      	 case "黄金远期":
      		 callBackName="IfsMetalGoldService";
      	   break;
      	 case "黄金掉期":
      		 callBackName="IfsMetalGoldService";
      	   break;
      	 case "黄金拆借":
      		 callBackName="IfsGoldLendService";
      	   break;
      	 case "外币债":
      		 callBackName="IfsDebtService";
      	   break;
      	 case "货币掉期":
      		 callBackName="IfsCswapService";
      	   break;
      	 case "外汇掉期":
      		 callBackName="IfsFlowService";
      	   break;
      	 case "人民币期权":
      		 callBackName="IfsOptionService";
      	   break;
      	 case "外币拆借":
      		 callBackName="IfsLendService";
      	   break;
      	 case "外币头寸调拨":
      		 callBackName="IfsAllocateService";
      	   break;
      	 case "外汇远期":
      		 callBackName="IfsFwdService";
      	   break;
        case "外汇即期":
   		 callBackName="IfsSptService";
   	   break;
   	 }
      	 return callBackName;
 	}
 	
 	function selectIfsController(prdName){
 		var controllerUrl="";
      	 switch (prdName)
      	 {
      	 case "外汇对即期":
      		controllerUrl="/IfsCurrencyController/editCcyOnSpot";
      	   break;
      	 case "利率互换":
      		controllerUrl="/IfsCfetsrmbIrsController/saveIrs";
      	   break;
      	 case "买断式回购":
      		controllerUrl="/IfsCfetsrmbOrController/saveOr";
      	   break;
      	 case "现券买卖":
      		controllerUrl="/IfsCfetsrmbCbtController/saveCbt";
      	   break;
      	 case "信用拆借":
      		controllerUrl="/IfsCfetsrmbIboController/saveIbo";
      	   break;
      	 case "债券借贷":
      		controllerUrl="/IfsCfetsrmbSlController/saveSl";
      	   break;
      	 case "质押式回购":
      		controllerUrl="/IfsCfetsrmbCrController/saveCr";
      	   break;
      	 case "黄金即期":
      		controllerUrl="/IfsCfetsmetalGoldController/saveGold";
      	   break;
      	 case "黄金远期":
      		controllerUrl="/IfsCfetsmetalGoldController/saveGold";
      	   break;
      	 case "黄金掉期":
      		controllerUrl="/IfsCfetsmetalGoldController/saveGold";
      	   break;
      	 case "黄金拆借":
      		controllerUrl="/IfsGoldLendController/editGoldLend";
      	   break;
      	 case "外币债":
      		controllerUrl="/IfsCurrencyController/editIfsDebt";
      	   break;
      	 case "货币掉期":
      		controllerUrl="/IfsCurrencyController/editCcySwap";
      	   break;
      	 case "外汇掉期":
      		controllerUrl="/IfsForeignController/editrmswap";
      	   break;
      	 case "人民币期权":
      		controllerUrl="/IfsForeignController/editOption";
      	   break;
      	 case "外币拆借":
      		controllerUrl="/IfsCurrencyController/editCcyLending";
      	   break;
      	 case "外币头寸调拨":
      		controllerUrl="/IfsCurrencyController/editIfsAllocate";
      	   break;
      	 case "外汇远期":
//       		controllerUrl="/IfsForeignController/editCreditEdit";
      		controllerUrl="/IfsForeignController/editCreditEditUpdate";//批量要素更新
      	   break;
        case "外汇即期":
        	controllerUrl="/IfsForeignController/editSpot";
   	   break;
   	 }
      	 return controllerUrl;
 	}
 	function upData(prdName,data){
      	 switch (prdName)
      	 {
      	 case "利率互换":
      		data["fixedInst"]="<%=__sessionInstitution.getInstId()%>";
      		data["fixedTrader"]="<%=__sessionUser.getUserId() %>";
			data["floatInst"]=data["cno"];
      	   break;
      	 case "买断式回购":
      		data["positiveInst"]="<%=__sessionInstitution.getInstId()%>";
      		data["positiveTrader"]="<%=__sessionUser.getUserId() %>";
      		data["reverseInst"]=data["cno"];
      	   break;
      	 case "现券买卖":
      		data["buyInst"]="<%=__sessionInstitution.getInstId()%>";
      		data["buyTrader"]="<%=__sessionUser.getUserId() %>";
			data["sellInst"]=data["cno"];
      	   break;
      	 case "信用拆借":
      		data["borrowInst"]="<%=__sessionInstitution.getInstId()%>";
      		data["borrowTrader"]="<%=__sessionUser.getUserId() %>";
			data["removeInst"]=data["cno"];
      	   break;
      	 case "债券借贷":
      		data["borrowInst"]="<%=__sessionInstitution.getInstId()%>";
      		data["borrowTrader"]="<%=__sessionUser.getUserId() %>";
			data["lendInst"]=data["cno"];
      	   break;
      	 case "质押式回购":
      		data["positiveInst"]="<%=__sessionInstitution.getInstId()%>";
      		data["positiveTrader"]="<%=__sessionUser.getUserId() %>";
			data["reverseInst"]=data["cno"];
      	   break;
      	 case "黄金拆借":
      		data["myBusinessPerson"]="<%=__sessionUser.getUserId() %>";
      	   break;
      	 case "外币债":
      	   break;
      	 case "外币头寸调拨":
      	   break;
        default:
        	data["instId"]="<%=__sessionInstitution.getInstId()%>";
      		data["dealer"]="<%=__sessionUser.getUserId() %>";
      		data["counterpartyInstId"]=data["cno"];
   	   	break;
   	 	}
      	 data["sponsor"]="<%=__sessionUser.getUserId()%>";
      	 data["sponInst"]="<%=__sessionUser.getInstId() %>";
      	 return data;
 	}
 	
 	/***
 	 * 要素批量更新
 	 * 
 	 * @author zcm
 	 * 
 	 */
 	function opicsCheck(){
 		var rows=grid.getSelecteds();
	     if(rows.length==0){
	         mini.alert("请至少选择一条要检查更新的交易!","提示");
	         return;
	     }
	     var hasOpics=0;
	     for(var i=0;i<rows.length;i++){
	    	 if(!(CommonUtil.isNull(rows[i]["port"])||CommonUtil.isNull(rows[i]["cost"])||
	    			 CommonUtil.isNull(rows[i]["product"])||CommonUtil.isNull(rows[i]["prodType"]))){
	    		 hasOpics=hasOpics+1;
	    	 }
	    	 if(i==rows.length-1 && !(prdName == "外汇即期" || prdName == "外汇远期" || prdName == "外汇掉期" ||
	    			 prdName == "人民币期权" || prdName == "外币拆借")){
	    		 if(hasOpics==rows.length){ // 非外汇交易需要做判断
		    		 mini.alert("当前没有可更新的opics数据");
		    		 return;
	    		 }
	    	 }
	     }
	     mini.open({
	         url: CommonUtil.baseWebPath() + "/../Common/miniOpics.jsp",
	         title: "opics要素填写",
	         width: 700,
	         height: 500,
	         onload: function () { // 弹出页面加载完成
                var iframe = this.getIFrameEl();
            	var data = {"prdName":prdName};
                iframe.contentWindow.SetData(data);
	         },
	         ondestroy: function (action) {
	             if (action == "ok") {
	                 var iframe = this.getIFrameEl();
	                 var data = iframe.contentWindow.GetData();
	                 data['prdName'] = prdName;
	                 data = mini.clone(data); //必须
	                 if (data) {
	                	 var success=0;
	                	 for(var i=0;i<rows.length;i++){
	            	    	 
	            	    		 //rows[i]["dealSource"]=data.dealSource;
	            	    		 rows[i]["port"]=data.port;
	            	    		 rows[i]["cost"]=data.cost;
	            	    		 rows[i]["product"]=data.product;
	            	    		 rows[i]["prodType"]=data.prodType;
	            	    		 //将一些机构等显示的机构名称翻译成机构id包括发起人员信息
	            	    		 rows[i]=upData(prdName,rows[i]);
	            	    		 var controllerUrl=selectIfsController(prdName);
	            	    		 //循环更新每一条opics要素不全的数据
	            	    		 (function(row,url,m,j){
	            	    			 CommonUtil.ajax({
	            	 					url:url,
	            	 					data:row,
	            	 					callback:function(data){
	            	 						success=success+1;
	            	 					}
	            	 				}); 
	            	    		 })(rows[i],controllerUrl,i,rows.length-1);
	            	    		 
	            	    		 if(i==rows.length-1){
         	 						setTimeout(function(){
         	 							var fail=i-success;
	            	 						var message="检查更新成功,其中更新"+success+"条数据,跳过"+fail+"条数据";
	       	            	    			 mini.alert(message,"提示");
	       	            	    			search(grid.pageSize,grid.pageIndex);
     	 							},500);
	            	    		 }
	            	    	 
	            	     }
	                 }
	             }
	         }
	     });
 	}
	
 	/***
 	 * 批量审批通过
 	 * 
 	 * @author zcm
 	 * x
 	 */
 	function batchCommit(){
 		var rows=grid.getSelecteds();
 	     if(rows.length==0){
 	         mini.alert("请至少选择一条要提交的交易!","提示");
 	         return;
 	     }
 	    for(var i=0;i<rows.length;i++){
	    	 if(CommonUtil.isNull(rows[i]["dealSource"])||CommonUtil.isNull(rows[i]["port"])||CommonUtil.isNull(rows[i]["cost"])||
	    			 CommonUtil.isNull(rows[i]["product"])||CommonUtil.isNull(rows[i]["prodType"])){
	    		 mini.alert("请先进行opics要素检查更新!","提示");
	    		 return;
	    	 }
	    }
 	   /*****************  批量提交前验证start   *******************************/
	    var times=0;
	    var arr= new Array();
	    for(var i=0;i<rows.length;i++){
	    
	    	var map = batchCommitVerify(prdNo,rows[i]);
		    if(map.flag == "false"){
		    	times++;
		    	arr.push(map.contractId);
		    }
	    
	    }
	    
	    if(times>0){
	    	mini.alert("成交单编号:"+arr+"必需的要素还未填完，请先维护再提交审批!","提示");
	    	return;
	    }
	    /******************  批量提交前验证end  ******************************/
	 mini.get("batch_commit_btn").setEnabled(true);//禁止多次点击批量提交
     mini.confirm("确认要批量提交吗？","确认",function (action) {
		if (action == "ok") {
			 var success=0;
		     var fall=0;
		     for(var i=0;i<rows.length;i++){
		    	 if(rows[i]["approveStatus"] == "3"){
			      	var callBack=selectCallBack(prdName);
			        Approve.approveCommitAll(Approve.FlowType.VerifyApproveFlow,rows[i]["ticketId"],Approve.OrderStatus.New,callBack,prdNo,function(){
			       		success=success+1;
			       	});
		    	 }
		    	 if(i==rows.length-1){
		    		setTimeout(function(){
		    			var sec=success;
		 	    		fall=rows.length-sec;
		 	    		var data="提交成功"+sec+"条数据，自动跳过"+fall+"条数据";
		 	    		mini.alert(data,"系统提示",function(){
		 	    		search(grid.pageSize,grid.pageIndex); 
		 	    		});mini.get("batch_commit_btn").setEnabled(true);
		    		},500);
		    	 }	 
		     }
		}});
 	}
 	
 /**批量提交前验证必填要素是否已填*/
 function batchCommitVerify(prdNo,row){
 var data={};
 data.flag="true";
 data.contractId=row.contractId;
 	switch (prdNo)
      	 {
      	 case "441"://利率互换
      		if(row.leftDiscountCurve==""||row.leftDiscountCurve==null|| 
      		   row.leftRateCode==""||row.leftRateCode==null||
      		   row.rightDiscountCurve==""||row.rightDiscountCurve==null||
      		   row.rightRateCode==""|| row.rightRateCode==null){
      			data.flag="false";
      			data.contractId = row.contractId;
      			return data;
      		}
      	   break;
      	   case "442"://买断式回购
      		if(row.reverseInst==""||row.reverseInst==null){
      			data.flag="false";
      			data.contractId = row.contractId;
      			return data;
      		}
      	   break;
      	   case "443"://现券买卖
      		if(row.sellInst==""||row.sellInst==null){
      			data.flag="false";
      			data.contractId = row.contractId;
      			return data;
      		}
      	   break;
      	   case "444"://信用拆借
      		if(row.removeInst==""||row.removeInst==null||
      			row.rateCode==""||row.rateCode==null){
      			data.flag="false";
      			data.contractId = row.contractId;
      			return data;
      		}
      	   break;
      	 	/* case "445"://债券借贷
       		if(row.removeInst==""||row.removeInst==null||
       			row.rateCode==""||row.rateCode==null){
       			data.flag="false";
       			data.contractId = row.contractId;
       			return data;
       		}
       	   break; */
      	   
      	   case "446"://质押式回购
      		if(row.reverseInst==""||row.reverseInst==null){
      			data.flag="false";
      			data.contractId = row.contractId;
      			return data;
      		}
      	   break;
   	 }
 	return data;
 }
 
	$(document).ready(function() {
		initButton();
		search(10, 0);
	});

	/***
	 * 批量审批通过
	 * 
	 * @author lij
	 * 
	 */
	function batchApprove(){
		var grid = mini.get("datagrid");
		var rows = grid.getSelecteds();
		if(rows.length==0){
			mini.alert("请至少选中一行要审批的数据!","提示");
			return;
		}
		mini.confirm("确认要批量审批吗？","确认",function (action) {
			if (action != "ok") {
				return;
			}
			mini.open({
					targetWindow: window,
					url: "../../Common/Flow/BatchApprove.jsp",
				 	title: "批量审批",  
					width: 600, 
					height: 400,
					onload: function () {
						var iframe = this.getIFrameEl();
						var data = {rows:rows};
						iframe.contentWindow.SetData(data);
					},
					ondestroy: function (action) {
						grid.reload();
					}
			});
		}); // end confirm
	}

	// 导出报表
   	function exportExcel(){
   		var content = grid.getData();
   		if(content.length == 0){
   			mini.alert("请先查询数据");
   			return;
   		}
   		var params = mini.encode(form.getData());
   		var contractId = JSON.parse(params).contractId; //成交单编号
   		var approveStatus = JSON.parse(params).approveStatus; //审批状态
   		var forDate = null; // 成交日期
   		var currencyPair = null; // 货币对
   		var port = null; // 投资组合
   		var startDate = null; // 起始日期
   		var endDate = null; // 结束日期
   		var dealNo = JSON.parse(params).dealNo; //dealNo
   		if(prdName=="债券发行" || prdName=="利率互换" || prdName=="买断式回购" || prdName=="现券买卖" || prdName=="信用拆借" ||
   				prdName=="债券借贷" || prdName=="质押式回购" || prdName=="外币债"){
   			forDate = JSON.parse(params).forDate; //成交日期
   		} else {
   			forDate = "";
   		}
   		if(prdName=="外汇远期" || prdName=="外汇掉期" || prdName=="人民币期权"){
   			currencyPair = JSON.parse(params).currencyPair; //货币对
   		} else {
   			currencyPair = "";
   		}
   		if(prdName=="外汇即期"){
   			port = JSON.parse(params).port;
   		} else {
   			port = "";
   		}
   		if(prdName=="外汇即期" || prdName=="外汇远期" || prdName=="外汇掉期" || prdName=="人民币期权" || prdName=="外币拆借"
   			|| prdName=="黄金即期"|| prdName=="黄金远期"|| prdName=="黄金掉期"){
   			startDate = JSON.parse(params).startDate;
   			endDate = JSON.parse(params).endDate;
   		} else {
   			startDate = "";
   			endDate = "";
   		}
   		mini.confirm("您确认要导出Excel吗?","系统提示",
   			function (action) {
   				if (action == "ok"){
   					var data = {prdName:prdName,approveType:mini.get("approveType").getValue(),contractId:contractId,
   							approveStatus:approveStatus,currencyPair:currencyPair,forDate:forDate,branchId:branchId,port:port,
   							startDate:startDate,endDate:endDate,dealNo:dealNo};
   					var fields = null;
   					for(var id in data){
   						fields += '<input type="hidden" id="' + id + '" name="'+ id +'" value="' + data[id] + '">';
   					}
   					var urls = CommonUtil.pPath + "/sl/IfsExportController/exportTradeExcel";                                                                                                                         
   					$('<form action="'+ urls +'" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();   
   				}
   			}
   		);
   	}
</script>