<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<title>外币拆借</title>
</head>
<body style="width:100%;height:100%;background:white">
		<%@ include file="../revocation.jsp"%>
<div class="mini-fit" >
	<div id="datagrid1" style="height:50%;width:64%;float:left" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id"  allowAlternating="true"
	  allowResize="true" sortMode="client" allowAlternating="true" multiSelect="true" >
		<div property="columns">
			<div type="checkcolumn"></div> 
			<div field="ticketId" width="180px" align="left"  headerAlign="center" >(审批)成交单编号</div>
			<div field="direction" width="100" allowSort="false" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'Tradings'}">方向</div>
			<div field="counterpartyInstId" width="120px" align="center"  headerAlign="center" >交易对手</div>  
			<div field="currency" width="100" allowSort="true" headerAlign="center"  align="center" align="" renderer="CommonUtil.dictRenderer" data-options="{dict:'Currency'}">货币</div>
			<div field="tenor" width="100" allowSort="true" headerAlign="center"  align="center" align="">期限</div>
			<div field="amount" width="150" allowSort="true" headerAlign="center"  align="right"  numberFormat="#,0.00">拆借金额</div>
			<div field="rate" width="150" allowSort="true" headerAlign="center"  align="right"  numberFormat="#,0.00">拆借利率</div>
		</div>
	</div>
	<div id="datagrid" class="mini-datagrid borderAll" style="height:100%;width:35%;float:right" style="width:100%;height:100%;" idField="id"  allowAlternating="true"
	  allowResize="true" sortMode="client" allowAlternating="true">
		<div property="columns"> 
			<div type="indexcolumn" width="50px" align="center" headerAlign="center">序号</div>
			<div field="approveId" width="150px" align="left"  headerAlign="center" >(审批)成交单编号</div>
			<div field="ticketId" width="150px" align="left"  headerAlign="center" >(CFETS)成交单编号</div>
			<div field="element" width="100px" align="left"  headerAlign="center" allowSort="true" numberFormat="#,0.00">名义本金</div>                                
		</div>
	</div>
	<div id="datagrid2" class="mini-datagrid borderAll" style="height:50%;width:64%;float:left" idField="id"  allowAlternating="true"
	  allowResize="true" sortMode="client" allowAlternating="true" multiSelect="true" >
		<div property="columns">
			<div type="checkcolumn"></div>
			<div field="ticketId" width="180px" align="left"  headerAlign="center" >(CFETS)成交单编号</div>
			<div field="direction" width="100" allowSort="false" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'Tradings'}">方向</div>
			<div field="counterpartyInstId" width="120px" align="center"  headerAlign="center" >交易对手</div>  
			<div field="currency" width="100" allowSort="true" headerAlign="center"  align="center" align="" renderer="CommonUtil.dictRenderer" data-options="{dict:'Currency'}">货币</div>
			<div field="tenor" width="100" allowSort="true" headerAlign="center"  align="center" align="">期限</div>
			<div field="amount" width="150" allowSort="true" headerAlign="center"  align="right"  numberFormat="#,0.00">拆借金额</div>
			<div field="rate" width="150" allowSort="true" headerAlign="center"  align="right"  numberFormat="#,0.00">拆借利率</div>
		</div>
	</div>
</div>   
<script>
	mini.parse();
	var url = window.location.search;
	var prdNo = CommonUtil.getParam(url, "prdNo");
	var prdName = CommonUtil.getParam(url, "prdName");
	var grid1=mini.get("datagrid1");
	var grid2=mini.get("datagrid2");
	var grid=mini.get("datagrid");
	grid1.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});
	grid2.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});
	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});
	grid1.on("update", function (e) {
	     var rows = grid1.findRows(function (row) {
	          return false;
	       });
	       grid1.selects(rows);
	});
	grid2.on("update", function (e) {
	     var rows = grid2.findRows(function (row) {
	          return false;
	       });
	       grid2.selects(rows);
	});
	/* 查询 */
	function search(pageSize,pageIndex){
		var data={};
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		data['approveStatus']="6";
		//已审批通过的才能进行勾兑
		var url = "/IfsApproveCurrencyController/searchPageApprovefxLendPassed";
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:url,
			data:params,
			callback : function(data) {
				grid1.setTotalCount(data.obj.total);
				grid1.setPageIndex(pageIndex);
		        grid1.setPageSize(pageSize);
				grid1.setData(data.obj.rows);
			}
		});
	}
	/* 查询 */
	function query(pageSize,pageIndex){
		var data={};
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		data['approveStatus']="6";
		//已审批通过的才能进行勾兑
		var url = "/IfsCurrencyController/searchPageApprovefxLendPassed";
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:url,
			data:params,
			callback : function(data) {
				grid2.setTotalCount(data.obj.total);
				grid2.setPageIndex(pageIndex);
		        grid2.setPageSize(pageSize);
				grid2.setData(data.obj.rows);
			}
		});
	}
	/* 查询 */
	function searchSuccess(pageSize,pageIndex){
		var data={};
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['proNo']=prdNo;
		//已审批通过的才能进行勾兑
		var url = "/IfsBlendSuccessController/searchPageSuccess";
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:url,
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	//进行人工勾兑
	function blend(){
		var rows=grid1.getSelecteds();
		var selects=grid2.getSelecteds();
		if(rows.length==0||selects.length==0){
			mini.alert("请将需要勾兑的目标数据选择完整！","消息",function(){ });
			return;
		}
		var success=0;
		for(var i=0;i<rows.length;i++){
			for(var j=0;j<selects.length;j++){
				if(rows[i].buyDirection==selects[j].buyDirection&&rows[i].counterpartyInstId==selects[j].counterpartyInstId&&rows[i].amount==selects[j].amount
						&&rows[i].currency==selects[j].currency&&rows[i].rate==selects[j].rate){
					(function(n,m,q,t){
						//匹配成功，添加到匹配成功列表数据中
						var data={};
						data['approveId']=n.ticketId;
						data['ticketId']=m.ticketId;
						data['element']=m.amount;
						data['proNo']=prdNo;
						data['proName']=prdName;
						data['trader']='<%=__sessionUser.getUserId()%>';
						var params=mini.encode(data);
						CommonUtil.ajax({
							url:"/IfsBlendSuccessController/blendSuccessAdd",
							data:params,
							callback:function(data){
								success=success+1;
								if(q==t){
									mini.alert("勾兑成功"+success+"条数据！","消息",function(){ });
									refresh();
								}
							}
						});	
					})(rows[i],selects[j],i,rows.length-1);
				}
			}
		}
		setTimeout(function(){
			if(success==0){
				mini.alert("目前没有可匹配成功的数据！","消息",function(){ });
				refresh();
			}
		},500);
	}
	//刷新所有列表
	function refresh(){
		search(10, 0);
		query(10, 0);
		searchSuccess(10, 0);
	}
	$(document).ready(function() {
		refresh();
	});

</script>
</body>
</html>
