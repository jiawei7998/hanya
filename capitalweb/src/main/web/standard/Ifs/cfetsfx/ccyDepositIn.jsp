<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
    <title>同业存放(对俄)</title>
    <script type="text/javascript" >
        /**prdCode用于页面投资组合的选择   该全局变量必须为prdCode*/
        var prdCode="MM";
    </script>
</head>
<body style="width: 100%; height: 100%; background: white">
<fieldset class="mini-fieldset">
    <legend>查询</legend>
    <div id="search_form" style="width: 100%;">
        <nobr>
            <input id="startDate" name="startDate" class="mini-datepicker" labelField="true" label="交易日期：" value="<%=__bizDate%>"
                   ondrawdate="onDrawDateStart" labelStyle="text-align:right;" emptyText="起始日期" format="yyyy-MM-dd"/>
            <span>~</span>
            <input id="endDate" name="endDate" class="mini-datepicker" value="<%=__bizDate%>"
                   ondrawdate="onDrawDateEnd" emptyText="结束日期" format="yyyy-MM-dd"/>
        </nobr>
        <input id="contractId" name="contractId" class="mini-textbox" labelField="true" width="280px" label="成交单编号：" labelStyle="text-align:right;"  labelStyle="width:120px" emptyText="请输入成交单编号"/>
        <input id="ticketId" name="ticketId" class="mini-textbox" labelField="true" label="审批单号：
" labelStyle="text-align:right;" emptyText="请填写审批单号" width="280px" />
        <input id="dealNo" name="dealNo" class="mini-textbox" labelField="true" label="opics交易号：" emptyText="请填写opics交易号" labelStyle="text-align:right;" width="280px"/>
        <input id="approveStatus" name="approveStatus" class="mini-combobox" data="CommonUtil.serverData.dictionary.ApproveSta" width="280px" emptyText="请选择审批状态" labelField="true"  label="审批状态：" labelStyle="text-align:right;"/>
        <span style="float: right; margin-right: 100px">
            <a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
            <a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
        </span>
    </div>
</fieldset>
<%@ include file="../batchReback.jsp"%>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>

<div id="MiniSettleForeignSpot" class="mini-fit" style="margin-top: 2px;">
    <div id="datagrid" class="mini-datagrid borderAll" style="width: 100%; height: 70%;" allowAlternating="true"  multiSelect="true"
         allowResize="true" border="true" sortMode="client" onrowdblclick="onRowDblClick">
        <div property="columns">
            <div type="checkcolumn"></div>
            <div type="indexcolumn" headerAlign="center" width="40">序号</div>
            <div field="taskName" width="120px" allowSort="false" headerAlign="center" align="center">审批状态</div>
<%--            <div field="dealTransType" width="100px" allowSort="false" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'dealTransType'}">交易状态</div>--%>
            <div field="ticketId" width="100px" allowSort="false" headerAlign="center" align="center">交易单号</div>
            <div field="contractId" width="100px" allowSort="false" headerAlign="center" align="center">成交单号</div>
            <div field="sponsor" width="100px" allowSort="false" headerAlign="center" align="center">审批发起人</div>
            <div field="sponInst" width="100px" allowSort="false" headerAlign="center" align="center">审批发起机构</div>
            <div field="aDate" width="100px" allowSort="false" headerAlign="center" align="center">审批开始时间</div>
            <div field="custId" width="100px" allowSort="false" headerAlign="center" align="center">客户号</div>
            <div field="custName" width="100px" allowSort="false" headerAlign="center" align="center">客户名称</div>
            <div field="ctype" width="100px" allowSort="false" headerAlign="center" align="center">客户类型</div>
            <div field="cliType" width="100px" allowSort="false" headerAlign="center" align="center">金融机构类型</div>
            <div field="forDate" width="100px" allowSort="false" headerAlign="center" align="center">成交日期</div>
            <div field="firstSettlementDate" width="100px" allowSort="false" headerAlign="center" align="center">协议开始日期</div>
            <div field="secondSettlementDate" width="100px" allowSort="false" headerAlign="center" align="center">协议到期日期</div>
            <div field="currency" width="100px" allowSort="false" headerAlign="center" align="center">币种</div>
            <div field="amt" width="100px" allowSort="false" headerAlign="center" align="right"  numberFormat="#,0.0000">本金(元)</div>
            <div field="rateType" width="100px" allowSort="false" headerAlign="center" align="center">利率类型</div>
            <div field="basisRateCode" width="100px" allowSort="false" headerAlign="center" align="center">基准利率代码</div>
            <div field="benchmarkSpread" width="100px" allowSort="false" headerAlign="center"  align="right"  numberFormat="#,0.0000">利率点差(千分之一‰)</div>
            <div field="rate" width="100px" allowSort="false" headerAlign="center"  align="right"  numberFormat="#,0.0000">利率(%)</div>
            <div field="basis" width="100px" allowSort="false" headerAlign="center" align="center">计息基础</div>
            <div field="tenor" width="100px" allowSort="false" headerAlign="center" align="center">存期</div>
            <div field="punishRate" width="100px" allowSort="false" headerAlign="center"  align="right"  numberFormat="#,0.0000">提前支取利率(%)</div>
            <div field="paymentFrequency" width="100px" allowSort="false" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'intPayCycle'}">付息频率</div>
            <div field="trad" width="100px" allowSort="false" headerAlign="center" align="center">客户经理</div>
            <div field="payIncomWay" width="100px" allowSort="false" headerAlign="center" align="center">支取方式</div>
            <div field="autoTransInd" width="100px" allowSort="false" headerAlign="center" align="center">自动转存标志</div>
            <div field="bussInstId" width="100px" allowSort="false" headerAlign="center" align="center">业务归属部门</div>
            <div field="note" width="100px" allowSort="false" headerAlign="center" align="center">备注</div>
            <div field="selfAcccode" width="100px" allowSort="false" headerAlign="center" align="center">付款账号</div>
            <div field="selfAccname" width="100px" allowSort="false" headerAlign="center" align="center">付款账户名称</div>
            <div field="selfBankcode" width="100px" allowSort="false" headerAlign="center" align="center">付款开户行大额行号</div>
            <div field="selfBankname" width="100px" allowSort="false" headerAlign="center" align="center">付款开户行名称</div>
            <div field="partyAcccode" width="100px" allowSort="false" headerAlign="center" align="center">收款账号</div>
            <div field="partyAccname" width="100px" allowSort="false" headerAlign="center" align="center">收款账户名称</div>
            <div field="partyBankcode" width="100px" allowSort="false" headerAlign="center" align="center">收款开户行大额行号</div>
            <div field="partyBankname" width="100px" allowSort="false" headerAlign="center" align="center">收款开户行名称</div>
        </div>
    </div>

    <fieldset style="width: 100%;border:solid 1px #aaa;margin-top:8px;position:relative;">
        <legend>同业存放(对俄)详情</legend>
        <div id="MiniSettleForeigDetail" style="padding:5px;" class="mini-fit area" tyle="width: 100%; height: 50%;" allowAlternating="true" allowResize="true" border="true" sortMode="client">
            <input class="mini-hidden" name="id"/>
            <div class="mini-panel" title="成交单编号" style="width:100%;"  allowResize="true" collapseOnTitleClick="false">
                <div class="leftarea">
                    <input style="width:98%;" id="contractId" name="contractId" class="mini-textbox mini-mustFill" labelField="true" requiredErrorText="该输入项为必输项" label="成交单编号：" required="true"  labelStyle="text-align:left;width:130px;"  vtype="maxLength:20" onvalidation="onEnglishAndNumberValidation"/>
                </div>
                <div class="rightarea">
                    <input  style="width:100%;" id="cfetscn" name="cfetscn" class="mini-textbox" label="CFETS机构码：" labelField="true" visible="false" enabled="false"  labelStyle="text-align:left;width:130px;" visible="flase"/>
                </div>
            </div>
            <div class="mini-panel" title="基础信息" style="width:100%"  allowResize="true" collapseOnTitleClick="false">
                <div class="leftarea">
                    <fieldset>
                        <input id="ticketId" name="ticketId" class="mini-textbox"  labelField="true"  label="合同号："  style="width:100%;" emptyText="系统自动生成" enabled="false" labelStyle="text-align:left;width:120px;"/>
                        <input id="sponsor" name="sponsor" class="mini-textbox" labelField="true"  label="审批发起人："  vtype="maxLength:20"  enabled="false" labelStyle="text-align:left;width:120px;" style="width:100%;"   />
                    </fieldset>
                </div>
                <div class="rightarea">
                    <fieldset>
                        <input id="sponInst" name="sponInst" class="mini-textbox" labelField="true" label="审批发起机构：" allowInput="false" style="width:100%;"  labelStyle="text-align:left;width:120px;"  enabled="false"/>
                        <input id="aDate" name="aDate" class="mini-datepicker" labelField="true"  label="审批开始日期：" vtype="maxLength:20" labelStyle="text-align:left;width:120px;" style="width:100%;" enabled="false"/>
                    </fieldset>
                </div>
            </div>
            <div class="mini-panel" title="交易对手信息" style="width:100%"  allowResize="true" collapseOnTitleClick="false">
                <div class="leftarea">
                    <fieldset>
                        <input id="custId" name="custId" class="mini-textbox mini-mustFill"  labelField="true" required="true" label="客户号：" allowInput="false" style="width:100%;" labelStyle="text-align:left;width:120px;" />
                        <input id="ctype" name="ctype" class="mini-combobox mini-mustFill" labelField="true"  label="客户类型："  style="width:100%;" required="true" data="CommonUtil.serverData.dictionary.CType" enabled="false" labelStyle="text-align:left;width:120px;"/>
                    </fieldset>
                </div>
                <div class="rightarea">
                    <fieldset>
                        <input id="custName" name="custName" class="mini-textbox mini-mustFill"  labelField="true"  label="客户名称："  style="width:100%;" required="true"   enabled="false" labelStyle="text-align:left;width:120px;"/>
                        <input id="cliType" name="cliType" class="mini-combobox mini-mustFill"  labelField="true"  label="金融机构类型："  style="width:100%;" required="true" data="CommonUtil.serverData.dictionary.clitype"  enabled="false" labelStyle="text-align:left;width:120px;"/>
                    </fieldset>
                </div>
            </div>
            <div class="mini-panel" title="交易主体信息" style="width:100%"  allowResize="true" collapseOnTitleClick="false">
                <div class="leftarea">
                    <input id="forDate" name="forDate" class="mini-datepicker" labelField="true"  label="交易日期："  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
                    <input style="width:100%;" id="firstSettlementDate" name="firstSettlementDate" class="mini-datepicker mini-mustFill" labelField="true" onValuechanged="compareDate" label="协议开始日期：" ondrawdate="onDrawDateStart" required="true"   labelStyle="text-align:left;width:130px;" />
                    <input style="width:100%;" id="basis" name="basis" class="mini-combobox mini-mustFill" labelField="true"  label="计息基准："   data="CommonUtil.serverData.dictionary.Basis" labelStyle="text-align:left;width:130px;" required="true" /><!--onValuechanged="interestAmount"-->
                    <input style="width:100%;" id="rateType" name="rateType" class="mini-combobox mini-mustFill" labelField="true"  data="CommonUtil.serverData.dictionary.Currency" label="利率类型："   labelStyle="text-align:left;width:130px;" required="true"  allowInput="false"/>
                    <input style="width:100%" id="benchmarkSpread" name="benchmarkSpread" class="mini-spinner mini-mustFill input-text-strong" labelField="true"  label="利率点差(千分之一)：" minValue="0" required="true" format="n4" maxValue="99999.9999"  changeOnMousewheel="false"   labelStyle="text-align:left;width:130px;" />
                    <input style="width:100%" id="tenor" name="tenor" class="mini-textbox mini-mustFill" labelField="true"  label="存期(天)：" required="true"   labelStyle="text-align:left;width:130px;" />
                    <input style="width:100%" id="paymentFrequency" name="paymentFrequency" class="mini-combobox mini-mustFill" labelField="true"  label="付息频率：" required="true"   labelStyle="text-align:left;width:130px;" data="CommonUtil.serverData.dictionary.intPayCycle"/>
                    <input id="payIncomWay" name="payIncomWay" class="mini-textbox" labelField="true"  label="支取方式：" vtype="maxLength:20" labelStyle="text-align:left;width:120px;" style="width:100%;" />
                    <input id="bussInstId" name="bussInstId" class="mini-textbox" labelField="true"  label="业务归属部门：" vtype="maxLength:20" labelStyle="text-align:left;width:120px;" style="width:100%;" />
                    <input style="width:100%;" id="attributionDept" name="attributionDept" class="mini-textbox" labelField="true"  label="归属机构：" vtype="maxLength:20" labelStyle="text-align:left;width:130px;"  />
                    <input style="width:100%;" id="fiveLevelClass" name="fiveLevelClass" class="mini-textbox" labelField="true"  label="五级分类：" vtype="maxLength:20" labelStyle="text-align:left;width:130px;"  />
                </div>
                <div class="rightarea">
                    <input style="width:100%;" id="currency" name="currency" class="mini-combobox mini-mustFill" labelField="true"  data="CommonUtil.serverData.dictionary.Currency" label="币种："   labelStyle="text-align:left;width:130px;" required="true"  allowInput="false"/>
                    <input style="width:100%;" id="secondSettlementDate" name="secondSettlementDate" class="mini-datepicker mini-mustFill" labelField="true" onValuechanged="compareDate" label="协议到期日期：" ondrawdate="onDrawDateEnd" required="true"  labelStyle="text-align:left;width:130px;"    />
                    <input style="width:100%;" id="amt" name="amt" class="mini-spinner mini-mustFill input-text-strong" labelField="true"  changeOnMousewheel='false'  format="n4" label="本金(元)：" required="true"  maxValue="99999999999999.9999" labelStyle="text-align:left;width:130px;" onvalidation="zeroValidation"   /><!--onValuechanged="interestAmount"-->
                    <input id="basisRateCode" name="basisRateCode" class="mini-textbox mini-mustFill"  labelField="true" label="基准利率代码："  style="width: 100%;" labelStyle="text-align:left;width:130px;" allowInput="false" requiredErrorText="该输入项为必输项"  required="true" />
                    <input style="width:100%" id="rate" name="rate" class="mini-spinner mini-mustFill input-text-strong" labelField="true"  label="利率(%)：" minValue="0" required="true"  format="n4" maxValue="99999.9999"  changeOnMousewheel="false"   labelStyle="text-align:left;width:130px;" />
                    <input style="width:100%" id="punishRate" name="punishRate" class="mini-spinner mini-mustFill input-text-strong" labelField="true"  label="提前支取利率(%)：" minValue="0" required="true" format="n4" maxValue="99999.9999" changeOnMousewheel="false"   labelStyle="text-align:left;width:130px;" />
                    <input id="trad" name="trad" class="mini-textbox" labelField="true"  label="客户经理：" vtype="maxLength:20" labelStyle="text-align:left;width:120px;" style="width:100%;" />
                    <input id="autoTransInd" name="autoTransInd" class="mini-textbox" labelField="true"  label="自动转存标志：" vtype="maxLength:20" labelStyle="text-align:left;width:120px;" style="width:100%;" />
                    <input id="note" name="note" class="mini-textbox" labelField="true"  label="备注：" vtype="maxLength:20" labelStyle="text-align:left;width:120px;" style="width:100%;" />
                    <input style="width:100%;" id="depositBalance" name="depositBalance" class="mini-spinner input-text-strong" labelField="true"  label="同业存款余额(元)：" vtype="maxLength:20" labelStyle="text-align:left;width:130px;"  />
                </div>
            </div>
            <div class="mini-panel" title="清算信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
                <div class="leftarea">
                    <fieldset>
                        <legend>本方账户</legend>
                        <input	style="width:100%;" id="selfAcccode" name="selfAcccode" class="mini-textbox" labelField="true"  label="本方帐号：" 	labelStyle="text-align:left;width:120px;"	onvalidation="CommonUtil.onValidation(e,'alphanum',[null])"  vtype="maxLength:1333" value = "9001000113901401"/>
                        <input	style="width:100%;" id="selfAccname" name="selfAccname" class="mini-textbox" labelField="true"  label="本方帐号名称："  labelStyle="text-align:left;width:120px;"	vtype="maxLength:1333" value = "投资类业务"/>
                        <input	style="width:100%;" id="selfBankcode" name="selfBankcode" class="mini-textbox" labelField="true"  label="本方开户行行号："  labelStyle="text-align:left;width:120px;"	onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" vtype="maxLength:400" value = "9001000113901401"/>
                        <input	style="width:100%;" id="selfBankname" name="selfBankname" class="mini-textbox" labelField="true"  label="本方开户行名称："  labelStyle="text-align:left;width:120px;"	vtype="maxLength:400" value = "投资类业务" />
                    </fieldset>
                </div>
                <div class="rightarea">
                    <fieldset>
                        <legend>对手方账户</legend>
                        <input	style="width:100%;" id="partyAcccode" name="partyAcccode" class="mini-textbox" labelField="true"  label="对方帐号："  labelStyle="text-align:left;width:120px;"	onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" vtype="maxLength:1333" />
                        <input	style="width:100%;" id="partyAccname" name="partyAccname" class="mini-textbox" labelField="true"  label="对方帐号名称："  labelStyle="text-align:left;width:120px;"	vtype="maxLength:1333" />
                        <input	style="width:100%;" id="partyBankcode" name="partyBankcode" class="mini-textbox" labelField="true"  label="对方开户行行号：" 	labelStyle="text-align:left;width:120px;"	onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" vtype="maxLength:400" />
                        <input	style="width:100%;" id="partyBankname" name="partyBankname" class="mini-textbox" labelField="true"  label="对方开户行名称："  labelStyle="text-align:left;width:120px;"	vtype="maxLength:400" />
                    </fieldset>
                </div>
            </div>
        </div>

    </fieldset>
</div>
<script>
    /**************************************初始化*********************************************************/
    mini.parse();
    var form = new mini.Form("#search_form");

    var url = window.location.search;
    var prdNo = CommonUtil.getParam(url, "prdNo");
    var fPrdCode = CommonUtil.getParam(url, "fPrdCode");
    var prdName = CommonUtil.getParam(url, "prdName");
    var dealType = CommonUtil.getParam(url, "dealType");


    /**************************************点击下面显示详情开始******************************/
    var MiniSettleForeigDetail = new mini.Form("MiniSettleForeigDetail");
    MiniSettleForeigDetail.setEnabled(false);
    var grid = mini.get("datagrid");
    //绑定表单
    var db = new mini.DataBinding();
    db.bindForm("MiniSettleForeigDetail", grid);
    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn){
            initButton();
            search(10,0);
        });
    });
    /**************************************点击下面显示详情结束******************************/

    // $(document).ready(function() {
    //     initButton();
    //     search(10, 0);
    // });

    grid.on("beforeload", function(e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize, pageIndex);
    });

    grid.on("select",function(e){
        grid.on("select",function(e){
            var rows=grid.getSelecteds();
            for(var i=0;i<rows.length;i++){
                if(rows[i].approveStatus != "3"){
                    mini.get("opics_check_btn").setEnabled(false);
                    break;
                }
            }
        });
        var row=e.record;
        mini.get("approve_mine_commit_btn").setEnabled(false);
        mini.get("approve_commit_btn").setEnabled(false);
        mini.get("edit_btn").setEnabled(false);
        mini.get("delete_btn").setEnabled(false);
        mini.get("approve_log").setEnabled(true);
        mini.get("recall_btn").setEnabled(true);
        mini.get("batch_commit_btn").setEnabled(true);
        if(row.approveStatus == "3"){//新建
            mini.get("edit_btn").setEnabled(true);
            mini.get("delete_btn").setEnabled(true);
            mini.get("approve_mine_commit_btn").setEnabled(true);
            mini.get("approve_commit_btn").setEnabled(false);
            mini.get("reback_btn").setEnabled(false);
            mini.get("recall_btn").setEnabled(false);
            mini.get("opics_check_btn").setEnabled(true);
        }
        if( row.approveStatus == "6" || row.approveStatus == "7" || row.approveStatus == "8" || row.approveStatus == "17"){//审批通过：6    审批拒绝:5
            mini.get("edit_btn").setEnabled(false);
            mini.get("delete_btn").setEnabled(false);
            mini.get("approve_mine_commit_btn").setEnabled(false);
            mini.get("approve_commit_btn").setEnabled(true);
            mini.get("batch_approve_btn").setEnabled(false);
            mini.get("batch_commit_btn").setEnabled(false);
            mini.get("opics_check_btn").setEnabled(false);
            mini.get("reback_btn").setEnabled(false);
            mini.get("recall_btn").setEnabled(false);
        }
        if(row.approveStatus == "5"){//审批中:5
            mini.get("edit_btn").setEnabled(false);
            mini.get("delete_btn").setEnabled(false);
            mini.get("approve_mine_commit_btn").setEnabled(false);
            mini.get("approve_commit_btn").setEnabled(true);
            mini.get("batch_commit_btn").setEnabled(false);
            mini.get("reback_btn").setEnabled(true);
            mini.get("recall_btn").setEnabled(true);
            mini.get("opics_check_btn").setEnabled(false);
        }

    });


    /**************************************按钮方法*********************************************************/

    function getData(action) {
        var row = null;
        if (action != "add") {
            row = grid.getSelected();
        }
        return row;
    }

    function search(pageSize,pageIndex){
        form.validate();
        if(form.isValid()==false){
            mini.alert("信息填写有误，请重新填写","系统提示");
            return;
        }
        var data=form.getData(true);
        data['pageNumber']=pageIndex+1;
        data['pageSize']=pageSize;
        data['branchId']=branchId;
        data['fPrdCode']=fPrdCode;
        var url=null;
        var approveType = mini.get("approveType").getValue();
        if(approveType == "mine"){
            url = "/IfsCCYDepositInController/searchPageCcyDepositInMine";
        }else if(approveType == "approve"){
            url = "/IfsCCYDepositInController/searchPageCcyDepositInUnfinished";
        }else{
            url = "/IfsCCYDepositInController/searchPageCcyDepositInFinished";
        }
        var params = mini.encode(data);
        CommonUtil.ajax({
            url:url,
            data:params,
            callback : function(data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }


    //增删改查
    function add(){
        var url = CommonUtil.baseWebPath() + "/cfetsfx/ccyDepositInEdit.jsp?action=add&prdNo="+prdNo+"&prdName="+prdName+"&dealType="+dealType+"&fPrdCode="+fPrdCode;
        var tab = { id: "MiniccyDepositInAdd", name: "MiniccyDepositInAdd", title: "同业存放(对俄)补录", url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
        var paramData = {selectData:""};
        CommonUtil.openNewMenuTab(tab,paramData);
    }

    function query() {
        search(grid.pageSize, 0);
    }

    function clear(){
        // MiniSettleForeigDetail.clear();
        form.clear();
        search(10,0);
    }

    function edit(){
        var row = grid.getSelected();
        var selections = grid.getSelecteds();
        if(selections.length>1){
            mini.alert("系统不支持多条数据进行编辑","消息提示");
            return;
        }
        if(row){
            var url = CommonUtil.baseWebPath() + "/cfetsfx/ccyDepositInEdit.jsp?action=edit&ticketid="+row.ticketId+"&prdNo="+prdNo+"&prdName="+prdName+"&dealType="+dealType+"&fPrdCode="+fPrdCode;
            var tab = { id: "MiniccyDepositInEdit", name: "MiniccyDepositInEdit", title: "同业存放(对俄)修改", url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
            var paramData = {selectData:row};
            CommonUtil.openNewMenuTab(tab,paramData);
        } else {
            mini.alert("请选中一条记录！","消息提示");
        }
    }

    function onRowDblClick(e) {
        var row = grid.getSelected();
        if(row){
            var url = CommonUtil.baseWebPath() + "/cfetsfx/ccyDepositInEdit.jsp?action=detail&ticketid="+row.ticketId+"&prdNo="+prdNo+"&prdName="+prdName+"&dealType="+dealType+"&fPrdCode="+fPrdCode;
            var tab = { id: "MiniccyDepositInDetail", name: row.ticketId, title: "同业存放(对俄)详情", url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
            var paramData = {selectData:row};
            CommonUtil.openNewMenuTab(tab,paramData);
        } else {
            mini.alert("请选中一条记录！","消息提示");
        }
    }

    //删除
    function del() {
        var grid = mini.get("datagrid");
        var row = grid.getSelected();
        var selections = grid.getSelecteds();
        if(selections.length>1){
            mini.alert("系统不支持多条数据进行删除","消息提示");
            return;
        }
        if (row) {
            mini.confirm("您确认要删除选中记录?","系统警告",function(value){
                if(value=="ok"){
                    CommonUtil.ajax({
                        url: "/IfsCCYDepositInController/deleteCcyDepositIn",
                        data: {ticketid: row.ticketId},
                        callback: function (data) {
                            if (data.code == 'error.common.0000') {
                                mini.alert("删除成功");
                                grid.reload();
                                //search(grid.pageSize,grid.pageIndex);
                            } else {
                                mini.alert("删除失败");
                            }
                        }
                    });
                }
            });
        }
        else {
            mini.alert("请选中一条记录！", "消息提示");
        }
    }

    /**************************审批相关****************************/
    //审批日志查看
    function appLog(selections){
        var flow_type = Approve.FlowType.VerifyApproveFlow;
        if(selections.length <= 0){
            mini.alert("请选择要操作的数据","系统提示");
            return;
        }
        if(selections.length > 1){
            mini.alert("系统不支持多笔操作","系统提示");
            return;
        }
        if(selections[0].tradeSource == "3"){
            mini.alert("初始化导入的业务没有审批日志","系统提示");
            return false;
        }
        Approve.approveLog(flow_type,selections[0].ticketId);
    };

    //提交正式审批、待审批
    function verify(selections){
        var grid = mini.get("datagrid");
        var row = grid.getSelected();
        if(selections.length == 0){
            mini.alert("请选中一条记录！","消息提示");
            return false;
        }else if(selections.length > 1){
            mini.alert("该功能不支持多笔提交，如需多笔提交请点击[批量提交]按钮","系统提示");
            return false;
        }
        if(selections[0]["approveStatus"] != "3" ){
            var searchByIdUrl = "/IfsCCYDepositInController/searchByTicketId";
            Approve.goToApproveJsp(selections[0].taskId,prdNo,"/cfetsfx/ccyDepositInEdit.jsp", target);
            var target = Approve.approvePage;
            var openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid="+row.ticketId+"&prdNo=ccyDepositIn"+"&fPrdCode="+fPrdCode;
            var id="AdvanceCcyDepositInApprove";
            var title="同业存放(对俄)审批";
            var tab = {"id": id,name:id,url:openJspUrl,title:title,parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
            var paramData = {selectData:selections[0]};
            CommonUtil.openNewMenuTab(tab,paramData);
            // top["win"].showTab(tab);
        }else{
            Approve.approveCommit(Approve.FlowType.VerifyApproveFlow,selections[0]["ticketId"],Approve.OrderStatus.New,"IfsCcyDepositInService",prdNo,function(){
                search(grid.pageSize,grid.pageIndex);
            });
        }
    };

    //审批
    function approve(){
        mini.get("approve_commit_btn").setEnabled(false);
        var messageid = mini.loading("系统正在处理...", "请稍后");
        try {
            verify(grid.getSelecteds());
        } catch (error) {

        }
        mini.hideMessageBox(messageid);
        mini.get("approve_commit_btn").setEnabled(true);
    }

    //提交审批
    function commit(){
        mini.get("approve_mine_commit_btn").setEnabled(false);
        var messageid = mini.loading("系统正在处理...", "请稍后");
        try {
            verify(grid.getSelecteds());
        } catch (error) {

        }
        mini.hideMessageBox(messageid);
        mini.get("approve_mine_commit_btn").setEnabled(true);
    }

    //审批日志
    function searchlog(){
        appLog(grid.getSelecteds());
    }

    //打印
    function print(){
        var selections = grid.getSelecteds();
        if(selections == null || selections.length == 0){
            mini.alert('请选择一条要打印的数据！','系统提示');
            return false;
        }else if(selections.length>1){
            mini.alert('暂不支持多条数据同时打印，请选择一条要打印的数据！','系统提示');
            return false;
        }
        var canPrint = selections[0].ticketId;

        if(!CommonUtil.isNull(canPrint)){
            var actionStr = CommonUtil.pPath + "/sl/IfsPrintUtilController/exportload/print/"+PrintNo.ccyDepositIn+"/" + canPrint;
            $('<form target="_blank" action=' + actionStr + ' method="post"></form>').appendTo('body').submit().remove();
        };
    }

    //导出报表
    function exportExcel(){
        exportExcelManin("jywbcj.xls","jywbcj");
    }

    //交易日期
    function onDrawDateStart(e) {
        var startDate = e.date;
        var endDate= mini.get("endDate").getValue();
        if(CommonUtil.isNull(endDate)){
            return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }

    function onDrawDateEnd(e) {
        var endDate = e.date;
        var startDate = mini.get("startDate").getValue();
        if(CommonUtil.isNull(startDate)){
            return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }


</script>
</body>
</html>