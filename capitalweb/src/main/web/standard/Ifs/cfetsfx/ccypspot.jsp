<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
<title>外汇对即期</title>
<script type="text/javascript" >
	/**prdCode用于页面投资组合的选择   该全局变量必须为prdCode*/
	var prdCode="FXD";
</script>
</head>
<body style="width: 100%; height: 100%; background: white">
	<fieldset class="mini-fieldset">
		<legend>查询</legend>
		<div id="search_form" style="width: 100%;">
			<input id="contractId" name="contractId" class="mini-textbox" labelField="true" label="成交单编号：" labelStyle="text-align:right;" width="280px"  labelStyle="width:100px" emptyText="请输入成交单编号" /> 
			<input id="forDate" name="forDate" class="mini-datepicker" labelField="true"  label="成交日期："   width="280px" labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请输入成交日期" />
			<input id="approveStatus" name="approveStatus" class="mini-combobox" data="CommonUtil.serverData.dictionary.ApproveSta" width="280px" emptyText="请选择审批状态" labelField="true"  label="审批状态：" labelStyle="text-align:right;"/>
			<span style="float: right; margin-right: 100px"> 
				<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
			</span>
		</div>
	</fieldset>
	<%@ include file="../batchReback.jsp"%> 
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
	
	<div id="MiniSettleForeignSpot" class="mini-fit"
		style="margin-top: 2px;">
		<div id="datagrid" class="mini-datagrid borderAll" style="width: 100%; height: 50%;" allowAlternating="true"  multiSelect="true"
			allowResize="true"  onrowdblclick="onRowDblClick" border="true" sortMode="client">
			<div property="columns">
			<div type="checkcolumn"></div>
				<div type="indexcolumn" headerAlign="center" width="40">序号</div>
				<div field="prdNo" width="100" allowSort="false" headerAlign="center" align="center" visible="false"></div>
				<div field="currencyPair" width="100" allowSort="false" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'CurrencyPair'}">货币对</div>
				<div field="direction" width="100" allowSort="false" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'trading'}">买卖方向</div>
				<div field="capital" width="165" allowSort="false" headerAlign="center" align="right" numberFormat="#,0.0000">本金</div>
				<div field="price" width="165" allowSort="false" headerAlign="center" align="right" numberFormat="#,0.00000000">价格</div>
				<div field="contractId" width="200" allowSort="false" headerAlign="center" align="">成交单编号</div>
				<div field="approveStatus" width="120" align="center" headerAlign="center"  renderer="CommonUtil.dictRenderer" data-options="{dict:'ApproveStatus'}">审批状态</div>
				<div field="dealTransType" width="100" allowSort="true" headerAlign="center"  align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'dealTransType'}">交易状态</div>
				<div field="instId" width="180" allowSort="false" headerAlign="center" align="center">本方机构</div>
				<div field="counterpartyInstId" width="180" allowSort="false" headerAlign="center" align="center">对方机构</div>
				<div field="forDate" width="150" allowSort="false" headerAlign="center" align="center">交易日期</div>
				<div field="tradingModel" width="100" allowSort="true" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'TradeModel'}">交易模式</div>
				<div field="tradingType" width="100" allowSort="true" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'TradeType'}">交易方式</div>
				<div field="valueDate" width="150" allowSort="false" headerAlign="center" align="center">起息日</div>
				<div field="sponsor" width="100" allowSort="false" headerAlign="center" align="center">审批发起人</div>
				<div field="sponInst" width="100" allowSort="false" headerAlign="center" align="center">审批发起机构</div>
				<div field="aDate" width="180" allowSort="false" headerAlign="center" align="center">审批发起时间</div>
				<div field="ticketId" width="200" allowSort="false" headerAlign="center" align="center">审批单号</div>
			</div>
		</div>
		<fieldset style="width: 100%;border:solid 1px #aaa;margin-top:8px;position:relative;">
		<legend>外汇对即期详情</legend>
			<div id="MiniSettleForeigDetail" style="padding:5px;" class="mini-fit area" tyle="width: 100%; height: 50%;" allowAlternating="true" allowResize="true" border="true" sortMode="client">
	<input class="mini-hidden" name="id"/>
	<div class="mini-panel" title="成交双方信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">	
		<div class="leftarea">
			<input id="instId" name="instId" class="mini-textbox" labelField="true"  label="本方机构：" maxLength="30"	 style="width:100%;"  labelStyle="text-align:left;width:130px;" />
			<input id="dealer" name="dealer" class="mini-textbox" labelField="true"  label="本方交易员："  maxLength="20" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
			<input id="forDate" name="forDate" class="mini-datepicker" labelField="true"  label="交易日期：" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
			<input id="tradingModel" name="tradingModel" class="mini-combobox" labelField="true"  label="交易模式：" value="C-Swap" data="CommonUtil.serverData.dictionary.TradeModel" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
		</div>
		<div class="rightarea">
			<input id="counterpartyInstId" name="counterpartyInstId" class="mini-textbox" labelField="true"  label="对方机构："  maxLength="30" style="width:100%;"  labelStyle="text-align:left;width:130px;" />
			<input id="counterpartyDealer" name="counterpartyDealer" class="mini-textbox" labelField="true"  label="对方交易员：" maxLength="20" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
			<input id="forTime" name="forTime" class="mini-timespinner" labelField="true" format="H:mm:ss" label="交易时间："  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
			<input id="tradingType" name="tradingType" class="mini-combobox" labelField="true"  label="交易方式："  value="C-Swap" data="CommonUtil.serverData.dictionary.TradeType"  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
		</div>
	</div>
	<%@ include file="../../Common/opicsDetail.jsp"%>
	<div class="mini-panel" title="交易主体信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
		<div class="leftarea">
			<input style="width:100%;" id="currencyPair" name="currencyPair" class="mini-combobox" labelField="true"  label="货币对："  data="CommonUtil.serverData.dictionary.CurrencyPair" labelStyle="text-align:left;width:130px;"/>
			<input style="width:100%;" id="capital" name="capital" class="mini-spinner" labelField="true"  label="本金：" required="true"  onValuechanged="interestAmount" changeOnMousewheel='false' maxValue="999999999999999" format="n4" labelStyle="text-align:left;width:130px;"  />
			<input style="width:100%;" id="valueDate" name="valueDate" class="mini-datepicker" labelField="true" label="起息日："  labelStyle="text-align:left;width:130px;" />
		</div>
		<div class="rightarea">
			<input style="width:100%;" id="direction" name="direction" class="mini-combobox" labelField="true"  label="买卖方向："  data="CommonUtil.serverData.dictionary.trading" labelStyle="text-align:left;width:130px;"/>
			<input style="width:100%;" id="price" name="price" class="mini-spinner" labelField="true"  label="价格：" required="true"  onValuechanged="interestAmount" changeOnMousewheel='false' maxValue="999999999999999" format="n8" labelStyle="text-align:left;width:130px;"/>
			<input style="width:100%;" id="settlementAmount" name="settlementAmount" class="mini-spinner" labelField="true"  label="结算金额：" required="true"  changeOnMousewheel='false' maxValue="999999999999999" format="n4" labelField="true"  labelStyle="text-align:left;width:130px;" />
		</div>
	</div>
	<%@ include file="./accountInfor.jsp"%>
	
	</div>
	</fieldset>
	</div>
	<script>
		mini.parse();

		var url = window.location.search;
		var prdNo = CommonUtil.getParam(url, "prdNo");
		var prdName = CommonUtil.getParam(url, "prdName");
		var grid = mini.get("datagrid");
		/**************************************点击下面显示详情开始******************************/
		var from = new mini.Form("MiniSettleForeigDetail");
		from.setEnabled(false);
		var grid = mini.get("datagrid");
		//grid.load();
		 //绑定表单
	    var db = new mini.DataBinding();
	    db.bindForm("MiniSettleForeigDetail", grid);
	    /**************************************点击下面显示详情结束******************************/
		grid.on("beforeload", function(e) {
			e.cancel = true;
			var pageIndex = e.data.pageIndex;
			var pageSize = e.data.pageSize;
			search(pageSize, pageIndex);
		});
		grid.on("select",function(e){
			var row=e.record;
			mini.get("approve_mine_commit_btn").setEnabled(false);
			mini.get("approve_commit_btn").setEnabled(false);
			mini.get("edit_btn").setEnabled(false);
			mini.get("delete_btn").setEnabled(false);
			mini.get("approve_log").setEnabled(true);
			mini.get("recall_btn").setEnabled(true);
			mini.get("batch_commit_btn").setEnabled(true);
			if(row.approveStatus == "3"){//新建
				mini.get("edit_btn").setEnabled(true);
				mini.get("delete_btn").setEnabled(true);
				mini.get("approve_mine_commit_btn").setEnabled(true);
				mini.get("approve_commit_btn").setEnabled(false);
				mini.get("reback_btn").setEnabled(true);
				mini.get("recall_btn").setEnabled(false);
			}
			if( row.approveStatus == "6" || row.approveStatus == "7"){//审批通过：6    审批拒绝:5
				mini.get("edit_btn").setEnabled(false);
				mini.get("delete_btn").setEnabled(false);
				mini.get("approve_mine_commit_btn").setEnabled(false);
				mini.get("approve_commit_btn").setEnabled(true);
				mini.get("batch_approve_btn").setEnabled(false);
				mini.get("batch_commit_btn").setEnabled(false);
				mini.get("opics_check_btn").setEnabled(false);
				mini.get("reback_btn").setEnabled(false);
				mini.get("recall_btn").setEnabled(false);
			}
			if(row.approveStatus == "5"){//审批中:5
				mini.get("edit_btn").setEnabled(false);
				mini.get("delete_btn").setEnabled(false);
				mini.get("approve_mine_commit_btn").setEnabled(false);
				mini.get("approve_commit_btn").setEnabled(true);
				mini.get("batch_commit_btn").setEnabled(false);
				mini.get("reback_btn").setEnabled(false);
				mini.get("recall_btn").setEnabled(true);
			}
			
		});

		function search(pageSize,pageIndex){
			var form = new mini.Form("#search_form");
			form.validate();
			if(form.isValid()==false){
				mini.alert("信息填写有误，请重新填写","系统提示");
				return;
			}

			var data=form.getData(true);
			data['pageNumber']=pageIndex+1;
			data['pageSize']=pageSize;
			data['branchId']=branchId;
			var url=null;

			var approveType = mini.get("approveType").getValue();
			if(approveType == "mine"){
				url = "/IfsCurrencyController/searchPageCcyOnSpotMine";
			}else if(approveType == "approve"){
				url = "/IfsCurrencyController/searchPageCcyOnSpotUnfinished";
			}else{
				url = "/IfsCurrencyController/searchPageCcyOnSpotFinished";
			}

			var params = mini.encode(data);
			CommonUtil.ajax({
				url:url,
				data:params,
				callback : function(data) {
					grid.setTotalCount(data.obj.total);
					grid.setPageIndex(pageIndex);
			        grid.setPageSize(pageSize);
					grid.setData(data.obj.rows);
				}
			});
		}
		
		function getData(action) {
			var row = null;
			if (action != "add") {
				row = grid.getSelected();
			}
			return row;
		}
		
		function onRowDblClick(e) {
			var grid = mini.get("datagrid");
			var row = grid.getSelected();
			if(row){
					var url = CommonUtil.baseWebPath() + "/cfetsfx/ccypspotEdit.jsp?action=detail&ticketid="+row.ticketId;
					var tab = { id: "MiniRmbfwdDetail", name: "MiniRmbfwdDetail", title: "外汇对即期详情", url: url ,showCloseButton:true};
					var paramData = {selectData:row};
					CommonUtil.openNewMenuTab(tab,paramData);

			} else {
				mini.alert("请选中一条记录！","消息提示");
			}
		}
		
		//增删改查
		function add(){
			var url = CommonUtil.baseWebPath() + "/cfetsfx/ccypspotEdit.jsp?action=add&prdNo="+prdNo;
			var tab = { id: "MiniCcySpotAdd", name: "MiniCcySpotAdd", title: "外汇对即期补录", url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
			var paramData = {selectData:""};
			CommonUtil.openNewMenuTab(tab,paramData);
		}
		function query() {
            search(grid.pageSize, 0);
        }
		function clear(){
            var form=new mini.Form("search_form");
            form.clear();
            search(10,0);

		}
		function edit()
		{
			var row = grid.getSelected();
			if(row){
				var url = CommonUtil.baseWebPath() + "/cfetsfx/ccypspotEdit.jsp?action=edit&ticketid="+row.ticketId;
				var tab = { id: "MiniCcySpotEdit", name: "MiniCcySpotEdit", title: "外汇对即期修改", url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
				var paramData = {selectData:""};
			    CommonUtil.openNewMenuTab(tab,paramData);

			} else {
				mini.alert("请选中一条记录！","消息提示");
			}
		}

		function onRowDblClick(e) {
			var grid = mini.get("datagrid");
			var row = grid.getSelected();
			if(row){
					var url = CommonUtil.baseWebPath() + "/cfetsfx/ccypspotEdit.jsp?action=detail&ticketid="+row.ticketId;
					var tab = { id: "MiniCcySpotDetail", name: "MiniCcySpotDetail", title: "外汇对即期详情", url: url ,showCloseButton:true};
					var paramData = {selectData:""};
					CommonUtil.openNewMenuTab(tab,paramData);

			} else {
				mini.alert("请选中一条记录！","消息提示");
			}
		}
		//删除
		function del() {
				var grid = mini.get("datagrid");
				var row = grid.getSelected();
				if (row) {
					mini.confirm("您确认要删除选中记录?","系统警告",function(value){
						if(value=="ok"){
						CommonUtil.ajax({
							url: "/IfsCurrencyController/deleteCcyOnSpot",
							
							data: {ticketid: row.ticketId},
							callback: function (data) {
								if (data.code == 'error.common.0000') {
									mini.alert("删除成功");
									grid.reload();
									//search(grid.pageSize,grid.pageIndex);
								} else {
									mini.alert("删除失败");
								}
							}
						});
						}
					});
				}
				else {
					mini.alert("请选中一条记录！", "消息提示");
				}

			}
		
		/**************************审批相关****************************/
		//审批日志查看
		function appLog(selections){
			var flow_type = Approve.FlowType.VerifyApproveFlow;
			if(selections.length <= 0){
				mini.alert("请选择要操作的数据","系统提示");
				return;
			}
			if(selections.length > 1){
				mini.alert("系统不支持多笔操作","系统提示");
				return;
			}
			if(selections[0].tradeSource == "3"){
				mini.alert("初始化导入的业务没有审批日志","系统提示");
				return false;
			}
			Approve.approveLog(flow_type,selections[0].ticketId);
		};
		//提交正式审批、待审批
		function verify(selections){
			var grid = mini.get("datagrid");
			var row = grid.getSelected();
			if(selections.length == 0){
				mini.alert("请选中一条记录！","消息提示");
				return false;
			}else if(selections.length > 1){
				mini.alert("该功能不支持多笔提交，如需多笔提交请点击[批量提交]按钮","系统提示");
				return false;
			}
			if(selections[0]["approveStatus"] != "3" ){
				var url = CommonUtil.baseWebPath() +"/cfetsfx/ccypspotEdit.jsp?action=approve&ticketid="+row.ticketId;
				var tab = {"id": "AdvanceCcySpotApprove",name:"AdvanceCcySpotApprove",url:url,title:"外汇对即期审批",
							parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
				var paramData = {selectData:selections[0]};
				CommonUtil.openNewMenuTab(tab,paramData);
				// top["win"].showTab(tab);
			}else{
				Approve.approveCommit(Approve.FlowType.VerifyApproveFlow,selections[0]["ticketId"],Approve.OrderStatus.New,"IfsOnspotService",prdNo,function(){
					search(grid.pageSize,grid.pageIndex);
				});
			}
		};
		//审批
		function approve(){
			mini.get("approve_commit_btn").setEnabled(false);
			var messageid = mini.loading("系统正在处理...", "请稍后");
			try {
				verify(grid.getSelecteds());
			} catch (error) {
				
			}
			mini.hideMessageBox(messageid);	
			mini.get("approve_commit_btn").setEnabled(true);
		}
		//提交审批
		function commit(){
			mini.get("approve_mine_commit_btn").setEnabled(false);
			var messageid = mini.loading("系统正在处理...", "请稍后");
			try {
				verify(grid.getSelecteds());
			} catch (error) {
				
			}
			mini.hideMessageBox(messageid);	
			mini.get("approve_mine_commit_btn").setEnabled(true);
		}
		//审批日志
		function searchlog(){
			appLog(grid.getSelecteds());
		}
		//打印
	    function print(){
			var selections = grid.getSelecteds();
			if(selections == null || selections.length == 0){
				mini.alert('请选择一条要打印的数据！','系统提示');
				return false;
			}else if(selections.length>1){
				mini.alert('暂不支持多条数据同时打印，请选择一条要打印的数据！','系统提示');
				return false;
			}
			var canPrint = selections[0].ticketId;
			
			if(!CommonUtil.isNull(canPrint)){
				var actionStr = CommonUtil.pPath + "/sl/IfsPrintUtilController/exportload/print/"+PrintNo.ccypspot+"/"+ canPrint;
				$('<form target="_blank" action=' + actionStr + ' method="post"></form>').appendTo('body').submit().remove();
			};
		}
		$(document).ready(function() {
			//控制按钮显示
			$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
				initButton();
				search(10, 0);
			});
		});
	</script>
</body>
</html>