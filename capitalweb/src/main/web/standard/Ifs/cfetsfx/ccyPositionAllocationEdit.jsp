<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
	<script type="text/javascript" src="<%=basePath%>/standard/Common/RiskCenter.js"></script>
<html>
  <head>
    <title>外币头寸调拨</title>
    <script type="text/javascript" >
	/**prdCode用于页面投资组合的选择   该全局变量必须为prdCode*/
	var prdCode="";
	</script>
  </head>
<body style="width:100%;height:100%;background:white">
		<div class="mini-splitter" style="width:100%;height:100%;">
				<div size="90%" showCollapseButton="false">
				<h1 style="text-align:center"><strong>外币头寸调拨</strong></h1>
	<div  id="field_form"  class="mini-fit area" style="background:white" >
	<input id="dealTransType" name="dealTransType" class="mini-hidden"/>
	<input id="sponsor" name="sponsor" class="mini-hidden" />
	<input id="sponInst" name="sponInst" class="mini-hidden" />
	<input id="ticketId" name="ticketId" class="mini-hidden" />
	<input id="aDate" name="aDate" class="mini-hidden"/>
	<div class="mini-panel" title="成交单编号" style="width:100%;"  allowResize="true" collapseOnTitleClick="true">
			<div class="leftarea">
				<input style="width:100%;" id="contractId" name="contractId" class="mini-textbox " labelField="true" requiredErrorText="该输入项为必输项" label="成交单编号：" labelStyle="text-align:left;width:130px;" onvalidation="onEnglishAndNumberValidations" vtype="maxLength:20"/>
			</div>
		</div>	
	<%@ include file="../../Common/opicsLess.jsp"%>
	
	<%@ include file="../../Common/RiskCenter.jsp"%>
	<div class="mini-panel" title="交易主体信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
		<div class="leftarea">
			<input id="type" name="type" class="mini-combobox" labelField="true"  label="Type" data="CommonUtil.serverData.dictionary.AllotType"  labelStyle="text-align:left;width:130px;" style="width:100%;"/>
			<input id="payDate" name="payDate" class="mini-datepicker" labelField="true"  label="Pay date"  labelStyle="text-align:left;width:130px;"  style="width:100%;" />
			<input id="amount" name="amount" class="mini-spinner" labelField="true"  label="Amount" changeOnMousewheel='false'  labelField="true"  format="n4" maxValue="999999999999999"   labelStyle="text-align:left;width:130px;" style="width:100%;"/>
			<input id="discount" name="discount" class="mini-spinner" labelField="true"  label="Discount" changeOnMousewheel='false'  labelField="true"  format="n8" maxValue="999999999999999"  labelStyle="text-align:left;width:130px;"  style="width:100%;" />
			<input id="relatedTrade" name="relatedTrade" class="mini-textbox" labelField="true"  label="Related trade"  labelStyle="text-align:left;width:130px;"  maxLength="15" style="width:100%;" />
			<input id="callAccount" name="callAccount" class="mini-textbox" labelField="true"  label="Call account"  labelStyle="text-align:left;width:130px;"  style="width:100%;" maxLength="15" />
			<input id="broker" name="broker" class="mini-textbox" labelField="true"  label="Broker"  labelStyle="text-align:left;width:130px;"  style="width:100%;"  maxLength="15"/>
		</div>
		<div class="rightarea">
			<input id="cal" name="cal" class="mini-textbox" labelField="true"  label="Cal"  labelStyle="text-align:left;width:130px;" maxLength="15"   style="width:100%;"/>
			<input id="ccy" name="ccy" class="mini-combobox" labelField="true"  label="Ccy" data="CommonUtil.serverData.dictionary.Currency" labelStyle="text-align:left;width:130px;"  style="width:100%;" />
        	<input id="rule" name="rule" class="mini-combobox" labelField="true"  label="Rule" data="CommonUtil.serverData.dictionary.AllotRule" labelStyle="text-align:left;width:130px;"   style="width:100%;" />
			<input id="payType" name="payType" class="mini-textbox" labelField="true"  label="Pay type" labelStyle="text-align:left;width:130px;"  maxLength="15" style="width:100%;" />
			<input id="relextid" name="relextid" class="mini-textbox" labelField="true"  label="Rel ext ID"  labelStyle="text-align:left;width:130px;"  style="width:100%;"  maxLength="15"/>
			<input id="cpty" name="cpty" class="mini-textbox" labelField="true"  label="Cpty"  labelStyle="text-align:left;width:130px;"  style="width:100%;"  maxLength="15"/>
		</div>
	</div>		
	<%@ include file="../../Common/Flow/MiniApproveOpCommon.jsp"%>
</div>	
</div>
<div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
		<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="save_btn"  onclick="save">保存交易</a></div>
		<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="close_btn" onclick="close">关闭界面</a></div>
	</div>
		</div>
		<script type="text/javascript">
			mini.parse();
			//获取当前tab
			var currTab = top["win"].tabs.getActiveTab();
			var params = currTab.params;
			var row=params.selectData;
			var url = window.location.search;
			var action = CommonUtil.getParam(url, "action");
			var ticketId = CommonUtil.getParam(url, "ticketid");
			mini.get("payDate").setValue("<%=__bizDate %>");
			
			var prdNo = CommonUtil.getParam(url, "prdNo");
			
			
			var tradeData={};
			
			tradeData.selectData=row;
			tradeData.operType=action;
			tradeData.serial_no=row.ticketId;
			tradeData.task_id=row.taskId;
			
			function inme() {
				if (action == "detail" || action == "edit"||action=="approve") {
					var from = new mini.Form("field_form");
					mini.get("applyNo").setVisible(false);//信用风险审查单号：
					mini.get("applyProd").setVisible(false);//产品名称
					CommonUtil.ajax({
						url : '/IfsCurrencyController/searchIfsAllocate',
						data : {
							ticketId : ticketId
						},
						callback : function(text) {
							from.setData(text.obj);
							//投资组合
							mini.get("port").setValue(text.obj.port);
							mini.get("port").setText(text.obj.port);
							//清算路径
							mini.get("ccysmeans").setValue(text.obj.ccysmeans);
							mini.get("ccysmeans").setText(text.obj.ccysmeans);
							mini.get("ctrsmeans").setValue(text.obj.ctrsmeans);
							mini.get("ctrsmeans").setText(text.obj.ctrsmeans);
							mini.get("applyNo").setValue(text.obj.applyNo);
							mini.get("applyNo").setText(text.obj.applyNo);
							mini.get("custNo").setValue(text.obj.custNo);
							queryCustNo(text.obj.custNo);
							mini.get("weight").setValue(text.obj.weight);
						}
					})
					if (action == "detail"||action=="approve") {
						mini.get("save_btn").hide();
						from.setEnabled(false);
						var form1=new mini.Form("approve_operate_form");
						form1.setEnabled(true);
					} 
					mini.get("contractId").setEnabled(false);
				}else if(action=="add"){
					mini.get("dealTransType").setValue("M");
					mini.get("sponsor").setValue("<%=__sessionUser.getUserId()%>");
					mini.get("sponInst").setValue("<%=__sessionUser.getInstId() %>");
					mini.get("applyNo").setVisible(false);//信用风险审查单号：
					mini.get("applyProd").setVisible(false);//产品名称
				}
			}

			function save() {
				var form = new mini.Form("field_form");
				form.validate();
				if (form.isValid() == false) {
					return;
				}
				var form = new mini.Form("field_form");
				var data = form.getData(true); //获取表单多个控件的数据
				var json = mini.encode(data); //序列化成JSON
				if (action == "add") {
					CommonUtil.ajax({
						url : "/IfsCurrencyController/addIfsAllocate",
						data : json,
						callback : function(data) {
							mini.alert(data,'提示信息',function(){
								if(data=='保存成功'){
								top["win"].closeMenuTab();
								}
							});
						}
					});
				} else if (action = "edit") {
					CommonUtil.ajax({
						url : "/IfsCurrencyController/editIfsAllocate",
						data : json,
						callback : function(data) {
							if (data.code == 'error.common.0000') {
								mini.alert(data.desc, '提示信息', function() {
									top["win"].closeMenuTab();
								});
							} else {
								mini.alert("保存失败");
							}
						}
					})
				}
			}
			
			//文、数字、下划线 的验证
	function onEnglishAndNumberValidations(e) {
		if(e.value == "" || e.value == null){//值为空，就不做校验
			return;
		}
		if (e.isValid) {
			if (isEnglishAndNumbers(e.value) == false) {
				e.errorText = "必须输入英文小写+数字";
				e.isValid = false;
			}
		}
	}
	/* 是否英文+数字 */
	function isEnglishAndNumbers(v) {
		var re = new RegExp("^[0-9a-z\_\-]+$");
		if (re.test(v)) return true;
		return false;
	}	
			
			//英文、数字、下划线 的验证
			function onEnglishAndNumberValidation(e) {
				if(e.value == "" || e.value == null){//值为空，就不做校验
					return;
				}
				if (e.isValid) {
					if (isEnglishAndNumber(e.value) == false) {
						e.errorText = "必须输入英文+数字";
						e.isValid = false;
					}
				}
			}
			/* 是否英文+数字 */
			function isEnglishAndNumber(v) {
				var re = new RegExp("^[0-9a-zA-Z\_\-]+$");
				if (re.test(v)) return true;
				return false;
			}
			function close() {
				top["win"].closeMenuTab();
			}
			$(document).ready(function() {
				inme();
			})
			
			/**   付款路径的选择   */
			function settleMeans1(){
				 /* var cpInstId = mini.get("counterpartyInstId").getValue();
				if(cpInstId == null || cpInstId == ""){
					mini.alert("请先选择对方机构!");
					return;
				}  */
				
				/* var ps = mini.get("leftDirection").getValue();
				if(ps == null || ps == ""){
					mini.alert("请先选择付款方向!");
					return;
				} */
				var curreny = mini.get("ccy").getValue();
				if(curreny == null || curreny == ""){
					mini.alert("请先选择Ccy!");
					return;
				}
				
				
				var url;
				var data;
				if(curreny!="CNY"){//外币
					url="./Ifs/opics/nostMini.jsp";
		        	data = { ccy: curreny ,cust:""};
		        }else if(sellCurreny=="CNY"){//人民币
		        	data = { ccy: curreny ,cust:""};
		        	url="./Ifs/opics/setaMini.jsp";
		        }
				
				var btnEdit = this;
		        mini.open({
		            url: url,
		            title: "选择清算路径",
		            width: 900,
		            height: 600,
		            onload: function () {
		                var iframe = this.getIFrameEl();
		                iframe.contentWindow.SetData(data);
		            },
		            ondestroy: function (action) {
		                if (action == "ok") {
		                    var iframe = this.getIFrameEl();
		                    var data1 = iframe.contentWindow.GetData();
		                    data1 = mini.clone(data1);    //必须
		                    if (data1) {
		                    	if(curreny!="CNY"){//外币
		                    		mini.get("ccysmeans").setValue($.trim(data1.smeans));
		                    		mini.get("ccysmeans").setText($.trim(data1.smeans));
		                            mini.get("ccysacct").setValue($.trim(data1.nos));
		        		        }else if(curreny=="CNY"){//人民币
		        		        	 mini.get("ccysmeans").setValue($.trim(data1.smeans));
			                    	 mini.get("ccysmeans").setText($.trim(data1.smeans));
			                         mini.get("ccysacct").setValue($.trim(data1.sacct)); 
		        		        	
		        		        }
		                    	
		                        btnEdit.focus();
		                    }
		                }

		            }
		        });
				
				
			}	
			
			
			/**   收款路径的选择   */
			function settleMeans2(){
				
				var curreny = mini.get("ccy").getValue();
				if(curreny == null || curreny == ""){
					mini.alert("请先选择Ccy!");
					return;
				}
				
				
				var url;
				var data;
				if(curreny!="CNY"){//外币
					url="./Ifs/opics/nostMini.jsp";
		        	data = { ccy: curreny ,cust:""};
		        }else if(curreny=="CNY"){//人民币
		        	data = { ccy: curreny ,cust:""};
		        	url="./Ifs/opics/setaMini.jsp";
		        }
				
				var btnEdit = this;
		        mini.open({
		            url: url,
		            title: "选择清算路径",
		            width: 900,
		            height: 600,
		            onload: function () {
		                var iframe = this.getIFrameEl();
		                iframe.contentWindow.SetData(data);
		            },
		            ondestroy: function (action) {
		                if (action == "ok") {
		                    var iframe = this.getIFrameEl();
		                    var data1 = iframe.contentWindow.GetData();
		                    data1 = mini.clone(data1);    //必须
		                    if (data1) {
		                    	if(curreny!="CNY"){//外币
		                    		mini.get("ctrsmeans").setValue($.trim(data1.smeans));
		                    		mini.get("ctrsmeans").setText($.trim(data1.smeans));
		                            mini.get("ctrsacct").setValue($.trim(data1.nos));
		        		        }else if(curreny=="CNY"){//人民币
		        		        	 mini.get("ctrsmeans").setValue($.trim(data1.smeans));
			                    	 mini.get("ctrsmeans").setText($.trim(data1.smeans));
			                         mini.get("ctrsacct").setValue($.trim(data1.sacct)); 
		        		        	
		        		        }
		                    	
		                        btnEdit.focus();
		                    }
		                }

		            }
		        });
				
				
			}	
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
		</script>
			<script type="text/javascript" src="../../Common/Flow/MiniApproveOpCommon.js"></script>			
</body>
</html>
