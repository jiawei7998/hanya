<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
	<title></title>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
	 <script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
	 <script type="text/javascript" src="<%=basePath%>/standard/Common/RiskCenter.js"></script>
	 <script type="text/javascript" src="../cfetsrmb/rmbVerify.js"></script>
<script type="text/javascript" >
	/**prdCode用于页面投资组合的选择   该全局变量必须为prdCode*/
	var prdCode="FXD-SWAP";
</script>
</head>
<body style="width:100%;height:100%;background:white">
		<div class="mini-splitter" style="width:100%;height:100%;">
		<div size="90%" showCollapseButton="false">
		<h1 style="text-align:center;padding-top:5px;font-size:18px;"><strong>外汇掉期</strong></h1>
	<div  id="field_form"  class="mini-fit area" style="background:white" >
	<input id="dealTransType" name="dealTransType" class="mini-hidden"/>
	<input id="sponsor" name="sponsor" class="mini-hidden" />
	<input id="sponInst" name="sponInst" class="mini-hidden" />
	<input id="aDate" name="aDate" class="mini-hidden"/>
	<input id="ticketId" name="ticketId" class="mini-hidden"/>
	<input id="opicsccy" name="opicsccy" class="mini-hidden"/>
	<input id="opicsctrccy" name="opicsctrccy" class="mini-hidden"/>
	<div class="mini-panel" title="成交单编号" style="width:100%;"  allowResize="true" collapseOnTitleClick="true">
			<div class="leftarea">
				<input style="width:100%;" id="contractId" name="contractId" class="mini-textbox mini-mustFill" labelField="true" requiredErrorText="该输入项为必输项"  label="成交单编号：" labelStyle="text-align:left;width:130px;" required="true"   vtype="maxLength:35"/>
			</div>
		</div>
	<div class="mini-panel" title="成交双方信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
		<div class="leftarea">
			<input id="instId" name="instId" class="mini-textbox mini-mustFill" labelField="true"  label="本方机构：" style="width:100%;"  labelStyle="text-align:left;width:130px"requiredErrorText="该输入项为必输项" required="true"  vtype="maxLength:16"/>
			<input id="dealer" name="dealer" class="mini-textbox mini-mustFill" labelField="true"  label="本方交易员："  labelStyle="text-align:left;width:130px"style="width:100%;"   requiredErrorText="该输入项为必输项" required="true"  vtype="maxLength:20" onvalidation="onEnglishAndNumberValidation" />
		</div>
		<div class="rightarea">
			<input id="counterpartyInstId" name="counterpartyInstId" class="mini-buttonedit mini-mustFill" requiredErrorText="该输入项为必输项" required="true"  onbuttonclick="onButtonEdit" labelField="true" allowInput="false" label="对方机构："  style="width:100%;"  labelStyle="text-align:left;width:130px" vtype="maxLength:16"/>
			<input id="counterpartyDealer" name="counterpartyDealer" class="mini-textbox mini-mustFill" labelField="true"  label="对方交易员：" labelStyle="text-align:left;width:130px"style="width:100%;"   vtype="maxLength:30" required="true" />
		</div>
	</div>			
	<div class="mini-panel" title="交易主体信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
		<div class="leftarea">
			<input id="forDate" name="forDate" class="mini-datepicker mini-mustFill" labelField="true" requiredErrorText="该输入项为必输项" required="true"  label="交易日期："  labelStyle="text-align:left;width:130px"style="width:100%;"   />
			<input style="width:100%;"id="currencyPair" name="currencyPair" class="mini-combobox mini-mustFill" labelField="true"  label="货币对："   requiredErrorText="该输入项为必输项" required="true"  labelStyle="text-align:left;width:130px" data="CommonUtil.serverData.dictionary.CurrencyPair" onvaluechanged="onCurrencyPairChange"/>
		</div>
		<div class="rightarea">
			<input id="forTime" name="forTime" class="mini-timespinner mini-mustFill" labelField="true"  label="交易时间："  labelStyle="text-align:left;width:130px"requiredErrorText="该输入项为必输项" required="true"  style="width:100%;"   />
			<input style="width:100%;"id="direction" name="direction" class="mini-combobox mini-mustFill" labelField="true"  label="交易方向：" requiredErrorText="该输入项为必输项" required="true"  labelStyle="text-align:left;width:130px"    data="CommonUtil.serverData.dictionary.Swaptrading" onvaluechanged="onCurrencyPairChange"/>
		</div>
		<div class="centerarea">
			<input style="width:50%;" id="price"  name="price"  class="mini-spinner mini-mustFill" labelField="true"  label="SPOT汇率："  format="n8" maxValue="999999999999" changeOnMousewheel="false"   labelStyle="text-align:left;width:130px;"  onvaluechanged="getReversePrice" requiredErrorText="该输入项为必输项" required="true" />
		</div>
		
		<div class="leftarea">
			<input style="width:100%;"id="nearValuedate" name="nearValuedate" class="mini-datepicker mini-mustFill" labelField="true"  label="近端起息日：" requiredErrorText="该输入项为必输项" required="true"  ondrawdate="onDrawDateNear" onvaluechanged="associaDateCalculation" labelStyle="text-align:left;width:130px"    />
			<input style="width:100%;"id="nearPrice" name="nearPrice" class="mini-spinner mini-mustFill" labelField="true"  label="近端交易金额1：" requiredErrorText="该输入项为必输项" required="true"  maxValue="999999999999" changeOnMousewheel='false' format="n4"  labelStyle="text-align:left;width:130px"  onvaluechanged="getReversePrice" onvalidation="zeroValidation"/>
			<input style="width:100%;"id="nearSpread" name="nearSpread" class="mini-spinner mini-mustFill" labelField="true"  label="近端点：" changeOnMousewheel='false' format="n8" requiredErrorText="该输入项为必输项" required="true"  labelStyle="text-align:left;width:130px" onvaluechanged="getReversePrice" minValue="-1000000" maxValue="1000000" value="0"/>
			<input style="width:100%;"id="nearRate" name="nearRate" class="mini-spinner mini-mustFill" labelField="true"  label="近端成交汇率："  requiredErrorText="该输入项为必输项" required="true"  format="n8" changeOnMousewheel="false" maxValue="999999999999" labelStyle="text-align:left;width:130px" enabled="false"/>
			<input style="width:100%;"id="nearReversePrice" name="nearReversePrice" class="mini-spinner mini-mustFill" labelField="true"  label="近端交易金额2：" requiredErrorText="该输入项为必输项" required="true"  maxValue="999999999999" changeOnMousewheel='false' format="n4"  labelStyle="text-align:left;width:130px"  onvalidation="zeroValidation"  />
			<input style="width:100%;"id="tenor" name="tenor" class="mini-spinner" labelField="true"  label="期限：" requiredErrorText="该输入项为必输项"  enabled="false" maxValue="99999" changeOnMousewheel='false' labelStyle="text-align:left;width:130px"    />
			<input id="nettingStatus" name="nettingStatus" class="mini-combobox mini-mustFill" required="true"  labelField="true"   label="净额清算状态：" labelStyle="text-align:left;width:130px;" style="width:100%;"  data="CommonUtil.serverData.dictionary.YesNo" showNullItem="true" onvaluechanged = "showOrHideNetting()"/>
			<!-- <input id="delaydElivind" name="delaydElivind" class="mini-combobox" labelField="true"   label="延期交易：" labelStyle="text-align:left;width:130px;" style="width:100%;" data="CommonUtil.serverData.dictionary.delaydElivind" value='0'/> -->
		</div>
		<div class="rightarea">
			<input style="width:100%;"id="fwdValuedate" name="fwdValuedate" class="mini-datepicker mini-mustFill" labelField="true"  label="远端起息日："  labelStyle="text-align:left;width:130px" requiredErrorText="该输入项为必输项" ondrawdate="onDrawDateFar" onvaluechanged="associaDateCalculation" required="true"    />
			<input style="width:100%;"id="fwdPrice" name="fwdPrice" class="mini-spinner mini-mustFill" labelField="true"  label="远端交易金额1：" requiredErrorText="该输入项为必输项" required="true"  maxValue="999999999999" changeOnMousewheel='false'  format="n4" labelStyle="text-align:left;width:130px"  onvaluechanged="getFwdReversePrice" onvalidation="zeroValidation" />
			<input style="width:100%;"id="spread" name="spread" class="mini-spinner mini-mustFill" labelField="true"  label="远端点：" changeOnMousewheel='false' format="n8" requiredErrorText="该输入项为必输项" required="true"  labelStyle="text-align:left;width:130px" onvaluechanged="getReversePrice" minValue="-1000000" maxValue="1000000" value="0"/>
			<input style="width:100%;"id="fwdRate" name="fwdRate" class="mini-spinner mini-mustFill" labelField="true"  label="远端成交汇率："  format="n8" changeOnMousewheel="false" requiredErrorText="该输入项为必输项" required="true"  maxValue="999999999999" labelStyle="text-align:left;width:130px"   data="CommonUtil.serverData.dictionary.trading"  enabled="false"/>
			<input style="width:100%;"id="fwdReversePrice" name="fwdReversePrice" class="mini-spinner mini-mustFill" labelField="true"  label="远端交易金额2：" maxValue="999999999999" requiredErrorText="该输入项为必输项" required="true"  changeOnMousewheel='false'  format="n4" labelStyle="text-align:left;width:130px"  onvalidation="zeroValidation"  />
			<input id="tradingModel" name="tradingModel" class="mini-combobox mini-mustFill" labelField="true" requiredErrorText="该输入项为必输项" required="true"  label="交易模式：" labelStyle="text-align:left;width:130px"style="width:100%;" data="CommonUtil.serverData.dictionary.TradeModel" />
			<input id="tradingType" name="tradingType" class="mini-combobox mini-mustFill" labelField="true"  label="交易方式：" requiredErrorText="该输入项为必输项" required="true"  value="RFQ" labelStyle="text-align:left;width:130px"style="width:100%;" data="CommonUtil.serverData.dictionary.TradeType"/>
		</div>
	</div>  	
	<%@ include file="../../Common/opicsLess.jsp"%>
	<%@ include file="../../Common/RiskCenter.jsp"%>	
	<div id="isNetting" class="mini-panel" title="清算账户信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
	  <div class="leftarea">	
		  	<input id="nettingAddr" name="nettingAddr" class="mini-textbox" labelField="true" label="清算路径："  labelStyle="text-align:left;width:130px;" style="width:100%;" enabled="false" value="上海清算所净额清算"/>
		</div>
	</div>
	<%@ include file="./accountInforSwap.jsp"%>
	
	<%@ include file="../../Common/Flow/MiniApproveOpCommon.jsp"%>		
	
</div>
		</div>
		<div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="save_btn"   onclick="save">保存交易</a></div>
				<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="close_btn"   onclick="close">关闭界面</a></div>
		</div>
	</div>
	
		<script type="text/javascript">
	mini.parse();
	//获取当前tab
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var row=params.selectData;

	var url = window.location.search;
	var action = CommonUtil.getParam(url, "action");
	var ticketId = CommonUtil.getParam(url, "ticketid");
	var _bizDate = '<%=__bizDate %>';
	mini.get("instId").setEnabled(false);
	mini.get("dealer").setEnabled(false);
	mini.get("weight").setEnabled(false);
	mini.get("weight").setLabel("风险暴露系数：");
	mini.get("profitLoss").setVisible(true);
	
	var prdNo = CommonUtil.getParam(url, "prdNo");
	var dealNo = CommonUtil.getParam(url, "dealNo");
	
	mini.get("forDate").setValue("<%=__bizDate %>");

	var timestamp = Date.parse(new Date());
	sys(timestamp);
	function sys(stamp){
		var time = new Date(stamp);
		var result = "";
		result += CommonUtil.singleNumberFormatter(time.getHours()) + ":"; 
		result += CommonUtil.singleNumberFormatter(time.getMinutes()) + ":";
		result += CommonUtil.singleNumberFormatter(time.getSeconds());
		mini.get("forTime").setValue(result);
	}

	var tradeData={};
	
	tradeData.selectData=row;
	tradeData.operType=action;
	tradeData.serial_no=row.ticketId;
	tradeData.task_id=row.taskId;
	
	function inme(){
		if(action=="detail"||action=="edit"||action=="approve"){
			var from = new mini.Form("field_form");
			var from1 = new mini.Form("ApproveOperate_div");
			mini.get("applyNo").setVisible(false);//信用风险审查单号：
			mini.get("applyProd").setVisible(false);//产品名称
			CommonUtil.ajax({
				url:'/IfsForeignController/searchrmswap',
				data:{ticketId:ticketId},
				callback:function(text){
					//投资组合
					mini.get("port").setValue(text.obj.port);
					mini.get("port").setText(text.obj.port);
					//成本中心
					mini.get("cost").setValue(row.cost);
					mini.get("cost").setText(row.cost);
					//产品代码
					mini.get("product").setValue(row.product);
					mini.get("product").setText(row.product);
					//产品类型
					mini.get("prodType").setValue(row.prodType);
					mini.get("prodType").setText(row.prodType);
					mini.get("counterpartyInstId").setValue(text.obj.counterpartyInstId);
					queryCustNo(text.obj.custNo);
					/* mini.get("counterpartyInstId").setText(text.obj.counterpartyInstId); */
					queryTextName(text.obj.counterpartyInstId);
					from.setData(text.obj);
					//清算路径
					mini.get("ccysmeans").setValue(text.obj.ccysmeans);
					mini.get("ccysmeans").setText(text.obj.ccysmeans);
					mini.get("ctrsmeans").setValue(text.obj.ctrsmeans);
					mini.get("ctrsmeans").setText(text.obj.ctrsmeans);
					mini.get("applyNo").setValue(text.obj.applyNo);
					mini.get("applyNo").setText(text.obj.applyNo);
					//mini.get("weight").setValue(text.obj.weight);
					searchProductWeightNew();
					// mini.get("ticketId").setValue(text.obj.ticketId);
					
					mini.get("instId").setValue("<%=__sessionInstitution.getInstFullname()%>");
					mini.get("dealer").setValue(row.myUserName);
					//mini.get("dealSource").setValue('D');
					changeAcct();
				}
			});
			if(action=="detail" || action=="approve"){
				mini.get("save_btn").hide();
				from.setEnabled(false);
				/* var form11=new mini.Form("#approve_operate_form");
			      form11.setEnabled(true); */
				from1.setEnabled(true);
			}

			
			mini.get("contractId").setEnabled(false);	
			showOrHideNetting(row.nettingStatus);
		
		}else if(action=="add"){
			//mini.get("dealSource").setValue('D');
			mini.get("dealTransType").setValue("M");
			mini.get("instId").setValue("<%=__sessionInstitution.getInstFullname()%>");
			mini.get("dealer").setValue("<%=__sessionUser.getUserName() %>");
			mini.get("sponsor").setValue("<%=__sessionUser.getUserId()%>");
			mini.get("sponInst").setValue("<%=__sessionUser.getInstId() %>");
			//mini.get("aDate").setValue("<%=__bizDate %>");
			mini.get("applyNo").setVisible(false);//信用风险审查单号：
			mini.get("applyProd").setVisible(false);//产品名称
			showOrHideNetting();
		}
	}
	
	function save(){
		var form = new mini.Form("field_form");
		form.validate();
		if (form.isValid() == false) {
			mini.alert("信息填写有误，请重新填写","系统提示");
			return;
		}
		var form = new mini.Form("field_form");            
		var data = form.getData(true);      //获取表单多个控件的数据
		data['instId']="<%=__sessionInstitution.getInstId()%>";
		data['dealer']="<%=__sessionUser.getUserId() %>";
		data['sponsor'] ="<%=__sessionUser.getUserId() %>";
		data['sponInst']="<%=__sessionUser.getInstId()%>";
		data['dealTransType']="1";//修改后的状态永远为1
		var json = mini.encode(data);   //序列化成JSON
			if(toDecimal(data['nearReversePrice'])!=toDecimal(data['nearPrice']*data['nearRate'])||toDecimal(data['fwdReversePrice'])!=toDecimal(data['fwdPrice']*data['fwdRate'])){
			mini.confirm("确认以当前数据为准吗？","确认",function (actions) {
				mini.alert(action);
				if (actions != "ok") {
					return;
				}
				if(action=="add"){
					CommonUtil.ajax({
					    url: "/IfsForeignController/addrmswap",
					    data:json,
					    callback:function (data) {
					    	if (data.code == 'error.common.0000') {
								mini.alert("保存成功",'提示信息',function(){
									top["win"].closeMenuTab();
								});
							} else {
								mini.alert("保存失败");
					    	}
					    }
					});
					}else  if(action=="edit"){
						CommonUtil.ajax({
							url:"/IfsForeignController/editrmswap",
							data:json,
							callback:function(data){
								if (data.code == 'error.common.0000') {
									mini.alert(data.desc,'提示信息',function(){
										top["win"].closeMenuTab();
									});
								} else {
									mini.alert("保存失败");
						    }
							}
						});
					}
			});
		}else{
			if(action=="add"){
				CommonUtil.ajax({
				    url: "/IfsForeignController/addrmswap",
				    data:json,
				    callback:function (data) {
				    	if (data.code == 'error.common.0000') {
							mini.alert("保存成功",'提示信息',function(){
								top["win"].closeMenuTab();
							});
						} else {
							mini.alert("保存失败");
				    	}
				    }
				});
				}else  if(action=="edit"){
					CommonUtil.ajax({
						url:"/IfsForeignController/editrmswap",
						data:json,
						callback:function(data){
							if (data.code == 'error.common.0000') {
								mini.alert(data.desc,'提示信息',function(){
									top["win"].closeMenuTab();
								});
							} else {
								mini.alert("保存失败");
					    }
						}
					});
				}
		}
	}
	
	function onButtonEdit(e) {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Common/CustMini.jsp",
            title: "选择对手方列表",
            width: 900,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.cno);
                        btnEdit.setText(data.cliname);
                        queryCustNo(data.creditsub);
                        btnEdit.focus();
                    }
                }

            }
        });
    }
	
	//根据交易对手编号查询全称
	function queryTextName(cno){
		CommonUtil.ajax({
		    url: "/IfsOpicsCustController/searchIfsOpicsCust",
		    data : {'cno' : cno},
		    callback:function (data) {
		    	if (data && data.obj) {
		    		mini.get("counterpartyInstId").setText(data.obj.cliname);
				} else {
					mini.get("counterpartyInstId").setText();
		    }
		    }
		});
	}
	
	function close(){
		top["win"].closeMenuTab();
	}
	$(document).ready(function () {
		inme();
	});
	//计算方向交易金额
	function getReversePrice(e){
		var price = mini.get("price").getValue();
		var nearPrice = mini.get("nearPrice").getValue();
		var nearRate = mini.get("nearRate").getValue();
		var nearSpread = mini.get("nearSpread").getValue();
		nearRate=CommonUtil.accAdd(price,nearSpread);
		mini.get("nearRate").setValue(nearRate);
		if(nearPrice!=null && nearPrice!="" && nearRate!=null && nearRate!=""){
			var nearReversePrice=Number(nearPrice)*Number(nearRate);
			mini.get("nearReversePrice").setValue(nearReversePrice);
		}
		
		var fwdPrice = mini.get("fwdPrice").getValue();
		var fwdRate = mini.get("fwdRate").getValue();
		var spread = mini.get("spread").getValue();
		fwdRate=CommonUtil.accAdd(price,spread);
		mini.get("fwdRate").setValue(fwdRate);
		if(fwdPrice!=null && fwdPrice!="" && fwdRate!=null && fwdRate!=""){
			var fwdReversePrice=Number(fwdPrice)*Number(fwdRate);
			mini.get("fwdReversePrice").setValue(fwdReversePrice);
		}
		
	}
	
	function getspread() {
			var fwdRate = mini.get("fwdRate").getValue();
			var nearRate = mini.get("nearRate").getValue();
				var spread = fwdRate - nearRate;
				mini.get("spread").setValue(spread);
		}
	function getFwdReversePrice(e){
		var fwdPrice = mini.get("fwdPrice").getValue();
		var fwdRate = mini.get("fwdRate").getValue();
			var fwdReversePrice=Number(fwdPrice)*Number(fwdRate);
			mini.get("fwdReversePrice").setValue(fwdReversePrice);
	}
	
	function onCurrencyPairChange(e){
		var currencyPair = mini.get("currencyPair").getText();
		/***start*******************************/
		var cp = parseCurrencyPair(currencyPair);
		mini.get("opicsccy").setValue(cp.ccy);//币种1  存放直通opics的主要币种
		mini.get("opicsctrccy").setValue(cp.ctr);//币种2  存放直通opics的次要币种
		/***end*******************************/
		var arr=new Array();
		arr=currencyPair.split("/");
		var direction = mini.get("direction").getValue();
		if(direction=='P'){
			mini.get("buyCurreny").setValue(arr[0]);
			mini.get("sellCurreny").setValue(arr[1]);
			mini.get("buyCurreny1").setValue(arr[1]);
			mini.get("sellCurreny1").setValue(arr[0]);
		}else if(direction=='S'){
			mini.get("buyCurreny").setValue(arr[1]);
			mini.get("sellCurreny").setValue(arr[0]);
			mini.get("buyCurreny1").setValue(arr[0]);
			mini.get("sellCurreny1").setValue(arr[1]);
		}
		changeAcct();
	}
	
	function onDrawDateNear(e) {
        var nearValuedate = e.date;
        var fwdValuedate= mini.get("fwdValuedate").getValue();
        if(CommonUtil.isNull(fwdValuedate)){
        	return;
        }
        if (fwdValuedate.getTime() < nearValuedate.getTime()) {
            e.allowSelect = false;
        }
    }
    
	function onDrawDateFar(e) {
        var fwdValuedate = e.date;
        var nearValuedate = mini.get("nearValuedate").getValue();
        if(CommonUtil.isNull(nearValuedate)){
        	return;
        }
        if (fwdValuedate.getTime() < nearValuedate.getTime()) {
            e.allowSelect = false;
        }
    }
	
	function associaDateCalculation(e){
		var nearValuedate = mini.get("nearValuedate").getValue();
		var fwdValuedate= mini.get("fwdValuedate").getValue();
		if(CommonUtil.isNull(nearValuedate)||CommonUtil.isNull(fwdValuedate)){
			return;
		}
		var subDate= Math.abs(parseInt((fwdValuedate.getTime() - nearValuedate.getTime())/1000/3600/24));
		mini.get("tenor").setValue(subDate);
		
		searchProductWeightNew();
	}
	
	//文、数字、下划线 的验证
	function onEnglishAndNumberValidations(e) {
		if(e.value == "" || e.value == null){//值为空，就不做校验
			return;
		}
		if (e.isValid) {
			if (isEnglishAndNumbers(e.value) == false) {
				e.errorText = "必须输入英文小写+数字";
				e.isValid = false;
			}
		}
	}
	/* 是否英文+数字 */
	function isEnglishAndNumbers(v) {
		var re = new RegExp("^[0-9a-z\_\-]+$");
		if (re.test(v)) return true;
		return false;
	}	
	
	
	//文、数字、下划线 的验证
	function onEnglishAndNumberValidation(e) {
		if(e.value == "" || e.value == null){//值为空，就不做校验
			return;
		}
		if (e.isValid) {
			if (isEnglishAndNumber(e.value) == false) {
				e.errorText = "必须输入英文+数字";
				e.isValid = false;
			}
		}
	}
	/* 是否英文+数字 */
	function isEnglishAndNumber(v) {
		var re = new RegExp("^[0-9a-zA-Z\_\-]+$");
		if (re.test(v)) return true;
		return false;
	}
	//不能输入中文
	function chineseValidation(e){
		var re = new RegExp("^[^\u4e00-\u9fa5]{0,}$");
        if (re.test(e.value)) {
        	return true;
        }else{
            e.errorText = "不能输入中文";
            e.isValid = false;
        }
	}
	
	/**   付款路径的选择   */
	function settleMeans1(){
		 var cpInstId = mini.get("counterpartyInstId").getValue();
		if(cpInstId == null || cpInstId == ""){
			mini.alert("请先选择对方机构!");
			return;
		} 
		
		/* var ps = mini.get("direction").getValue();
		if(ps == null || ps == ""){
			mini.alert("请先选择交易方向!");
			return;
		} */
		var currencyPair = mini.get("currencyPair").getText();
		if(currencyPair == null || currencyPair == ""){
			mini.alert("请先选择货币对!");
			return;
		}
		//解析货币对
		var ret = parseCurrencyPair(currencyPair);
		var url;
		var data;
		
		if(ret.ccy!="CNY"){//外币
			url="./Ifs/opics/nostMini.jsp";
        	data = { ccy: ret.ccy ,cust:cpInstId};
        }else if(ret.ccy=="CNY"){//人民币
        	data = { ccy: ret.ccy ,cust:cpInstId};
        	url="./Ifs/opics/setaMini.jsp";
        }
		
		var btnEdit = this;
        mini.open({
            url: url,
            title: "选择清算路径",
            width: 700,
            height: 600,
            onload: function () {
                var iframe = this.getIFrameEl();
                iframe.contentWindow.SetData(data);
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data1 = iframe.contentWindow.GetData();
                    data1 = mini.clone(data1);    //必须
                    if (data1) {
                    	if(ret.ccy!="CNY"){//外币
                    		mini.get("ccysmeans").setValue($.trim(data1.smeans));
                    		mini.get("ccysmeans").setText($.trim(data1.smeans));
                            mini.get("ccysacct").setValue($.trim(data1.nos));
                        }else if(ret.ccy=="CNY"){//人民币
                        	mini.get("ccysmeans").setValue($.trim(data1.smeans));
	                    	 mini.get("ccysmeans").setText($.trim(data1.smeans));
	                         mini.get("ccysacct").setValue($.trim(data1.sacct)); 
                        }
                        
                        btnEdit.focus();
                    }
                }

            }
        });
		
		
	}
	//解析货币对
	function parseCurrencyPair(cp){
		var ccy = cp.substr(0,3);
		var ctr = cp.substr(4,7);
		var data = {ccy:ccy,ctr:ctr};
		return data;
		
		
	}
	
	/**   清算路径2的选择   */
	function settleMeans2(){
		 var cpInstId = mini.get("counterpartyInstId").getValue();
		if(cpInstId == null || cpInstId == ""){
			mini.alert("请先选择对方机构!");
			return;
		} 
		
		/* var ps = mini.get("direction").getValue();
		if(ps == null || ps == ""){
			mini.alert("请先选择交易方向!");
			return;
		} */
		var currencyPair = mini.get("currencyPair").getText();
		if(currencyPair == null || currencyPair == ""){
			mini.alert("请先选择货币对!");
			return;
		}
		/* ps = mini.get("direction").getValue();
		if(ps=="1"){
			ps=2;
		}else{
			ps=1;
		} */
		//解析货币对
		var ret = parseCurrencyPair(currencyPair);
		var url;
		var data;
		
		if(ret.ctr!="CNY"){//外币
			url="./Ifs/opics/nostMini.jsp";
        	data = { ccy: ret.ctr ,cust:cpInstId};
        }else if(ret.ctr=="CNY"){//人民币
        	data = { ccy: ret.ctr ,cust:cpInstId};
        	url="./Ifs/opics/setaMini.jsp";
        }
		
		var btnEdit = this;
        mini.open({
            url: url,
            title: "选择清算路径",
            width: 700,
            height: 600,
            onload: function () {
                var iframe = this.getIFrameEl();
                iframe.contentWindow.SetData(data);
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data1 = iframe.contentWindow.GetData();
                    data1 = mini.clone(data1);    //必须
                    if (data1) {
                    	if(ret.ctr!="CNY"){//外币
                    		mini.get("ctrsmeans").setValue($.trim(data1.smeans));
                    		mini.get("ctrsmeans").setText($.trim(data1.smeans));
                            mini.get("ctrsacct").setValue($.trim(data1.nos));
                        }else if(ret.ctr=="CNY"){//人民币
                        	mini.get("ctrsmeans").setValue($.trim(data1.smeans));
	                    	 mini.get("ctrsmeans").setText($.trim(data1.smeans));
	                         mini.get("ctrsacct").setValue($.trim(data1.sacct)); 
                        }
                        
                        btnEdit.focus();
                    }
                }

            }
        });
		
	}
	
	function searchProductWeightNew(){
    	var form = new mini.Form("field_form");
    	var data = form.getData(true);
    	var mDate = data.fwdValuedate;
    	searchProductWeight(20,0,prdNo,mDate);
    	
    }
	function showOrHideNetting(val){
		   var nettingStatus=mini.get("nettingStatus").getValue();
		   if(val!=null){
			   nettingStatus=val;
		   }
		   if(nettingStatus=='1'){
			   mini.get("isNetting").show();
			   mini.get("isNoNetting").hide();
			   mini.get("nettingAddr").setValue("上海清算所净额清算.外汇中央对手净额清算");
		   }else{
			   mini.get("isNetting").hide();
			   mini.get("isNoNetting").show();
		   }
	   }
	   function changeAcct(){
		   var buyCurreny=mini.get("buyCurreny").getValue();
		   var sellCurreny=mini.get("sellCurreny").getValue();
		   $("#bankBic1").show();
		   $("#intermediaryBankName1").show();
		   $("#intermediaryBankBicCode1").show();
		   $("#bankBic2").show();
		   $("#intermediaryBankName2").show();
		   $("#intermediaryBankBicCode2").show();
		   if(buyCurreny=="CNY"){
			   mini.get("bankName1").setLabel("资金账户户名：");
			   mini.get("dueBank1").setLabel("资金开户行：");
			   mini.get("dueBankName1").setLabel("支付系统行号：");
			   mini.get("dueBankAccount1").setLabel("资金账号：");
			   $("#bankBic1").hide();
			   $("#intermediaryBankName1").hide();
			   $("#intermediaryBankBicCode1").hide();
			   
		   }else if(sellCurreny=="CNY"){
			   mini.get("bankName2").setLabel("资金账户户名：");
			   mini.get("dueBank2").setLabel("资金开户行：");
			   mini.get("dueBankName2").setLabel("支付系统行号：");
			   mini.get("dueBankAccount2").setLabel("资金账号：");
			   $("#bankBic2").hide();
			   $("#intermediaryBankName2").hide();
			   $("#intermediaryBankBicCode2").hide();
		   }
		   
		   var buyCurreny1=mini.get("buyCurreny1").getValue();
		   var sellCurreny1=mini.get("sellCurreny1").getValue();
		   $("#bankBic3").show();
		   $("#intermediaryBankName3").show();
		   $("#intermediaryBankBicCode3").show();
		   $("#bankBic4").show();
		   $("#intermediaryBankName4").show();
		   $("#intermediaryBankBicCode4").show();
		   if(buyCurreny1=="CNY"){
			   mini.get("bankName3").setLabel("资金账户户名：");
			   mini.get("dueBank3").setLabel("资金开户行：");
			   mini.get("dueBankName3").setLabel("支付系统行号：");
			   mini.get("dueBankAccount3").setLabel("资金账号：");
			   $("#bankBic3").hide();
			   $("#intermediaryBankName3").hide();
			   $("#intermediaryBankBicCode3").hide();
			   
		   }else if(sellCurreny1=="CNY"){
			   mini.get("bankName4").setLabel("资金账户户名：");
			   mini.get("dueBank4").setLabel("资金开户行：");
			   mini.get("dueBankName4").setLabel("支付系统行号：");
			   mini.get("dueBankAccount4").setLabel("资金账号：");
			   $("#bankBic4").hide();
			   $("#intermediaryBankName4").hide();
			   $("#intermediaryBankBicCode4").hide();
		   }
	   }
	   
	function getLossLimit(e){//外汇远期的估值为近端估值加上远端估值
		var param = mini.encode({'dealno' : dealNo,'prdNo':prdNo}); 
		CommonUtil.ajax({
		    url: "/IfsLimitController/getLossLimitForWap",
		    data : param,
		    callback:function (data) {
		    	if (data != null && data != "" && data !="0" && data !="null") {
		    		mini.get("profitLoss").setValue(data);
				} else {
					mini.get("profitLoss").setValue();
		    	}
		    }
		});
	}
	</script>
	<script type="text/javascript" src="../../Common/Flow/MiniApproveOpCommon.js"></script>			
</body>
</html>