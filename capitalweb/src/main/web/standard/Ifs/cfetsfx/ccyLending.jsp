<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
<title>外币拆借</title>
<script type="text/javascript" >
	/**prdCode用于页面投资组合的选择   该全局变量必须为prdCode*/
	var prdCode="MM";
</script>
</head>
<body style="width: 100%; height: 100%; background: white">
	<fieldset class="mini-fieldset">
		<legend>查询</legend>
		<div id="search_form" style="width: 100%;">
			<nobr>
			<input id="startDate" name="startDate" class="mini-datepicker" labelField="true" label="交易日期：" value="<%=__bizDate%>"
						ondrawdate="onDrawDateStart" labelStyle="text-align:right;" emptyText="起始日期" format="yyyy-MM-dd"/>
			<span>~</span>
			<input id="endDate" name="endDate" class="mini-datepicker" value="<%=__bizDate%>"
						ondrawdate="onDrawDateEnd" emptyText="结束日期" format="yyyy-MM-dd"/>
			</nobr>
			<input id="contractId" name="contractId" class="mini-textbox" labelField="true" width="280px" label="成交单编号：" labelStyle="text-align:right;"  labelStyle="width:120px" emptyText="请输入成交单编号"/>
			<input id="ticketId" name="ticketId" class="mini-textbox" labelField="true" label="审批单号：
" labelStyle="text-align:right;" emptyText="请填写审批单号" width="280px" />
			<input id="dealNo" name="dealNo" class="mini-textbox" labelField="true" label="opics交易号：" emptyText="请填写opics交易号" labelStyle="text-align:right;" width="280px"/>
			<input id="approveStatus" name="approveStatus" class="mini-combobox" data="CommonUtil.serverData.dictionary.ApproveSta" width="280px" emptyText="请选择审批状态" labelField="true"  label="审批状态：" labelStyle="text-align:right;"/>
			<span style="float: right; margin-right: 100px"> 
				<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
			</span>
		</div>
	</fieldset>
	<%@ include file="../batchReback.jsp"%>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
	 
	<div id="MiniSettleForeignSpot" class="mini-fit" style="margin-top: 2px;">
		<div id="datagrid" class="mini-datagrid borderAll" style="width: 100%; height: 70%;" allowAlternating="true"  multiSelect="true"
			allowResize="true" border="true" sortMode="client" onrowdblclick="onRowDblClick">
			<div property="columns">
			<div type="checkcolumn"></div>
				<div type="indexcolumn" headerAlign="center" width="40">序号</div>
				<div field="prdNo" width="100px" allowSort="false" headerAlign="center" align="center" visible="false"></div>
				<div field="forDate" width="100px" allowSort="false" headerAlign="center" align="center">交易日期</div>
				<div field="forTime" width="100px" allowSort="false" headerAlign="center" align="center">交易时间</div>	
				<div field="taskName" width="120px" allowSort="false" headerAlign="center" align="center">审批状态</div>
				<div field="contractId" width="160px" allowSort="false" headerAlign="center" align="center" >成交单编号</div>			
				<div field="ticketId" width="160px" allowSort="false" headerAlign="center" align="center">审批单号</div>
				<div field="dealNo" width="100px" align="center"  headerAlign="center" >opics交易号</div>
				<div field="statcode" width="160px" align="center"  headerAlign="center" >opics处理状态</div> 
				<div field="counterpartyInstId" width="200px" allowSort="true" headerAlign="center"  align="center">对手方机构</div>
				<div field="currency" width="100px" allowSort="true" headerAlign="center"  align="center" align="">货币</div>
				<div field="direction" width="100px" allowSort="true" headerAlign="center"  align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'lending'}">交易方向</div>
				<div field="amount" width="140px" allowSort="true" headerAlign="center"  align="right"  numberFormat="#,0.0000">拆借金额</div>
				<div field="rate" width="140px" allowSort="true" headerAlign="center"  align="right"  numberFormat="#,0.000000">拆借利率(%)</div>
				<div field="interest" width="140px" allowSort="true" headerAlign="center"  align="right"  numberFormat="#,0.0000">应计利息</div>
				<div field="repaymentAmount" width="140px" allowSort="true" headerAlign="center"  align="right"  numberFormat="#,0.0000">到期还款金额</div>
				<div field="valueDate" width="100px" allowSort="true" headerAlign="center"  align="center">起息日</div>
				<div field="maturityDate" width="100px" allowSort="true" headerAlign="center"  align="center">到期日</div>
				<div field="dealSource" width="100px" align="center"  headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'TradeSource'}">交易来源</div>
				<div field="sponsor" width="120px" allowSort="true" headerAlign="center"  align="center" >审批发起人</div>
				<div field="sponInst" width="120px" allowSort="true" align="center"  >审批发起机构</div>
				<div field="aDate" width="160px" allowSort="true" align="center">审批发起时间</div>
			</div>
		</div>
		
		<fieldset style="width: 100%;border:solid 1px #aaa;margin-top:8px;position:relative;">
		<legend>外币拆借详情</legend>
			<div id="MiniSettleForeigDetail" style="padding:5px;" class="mini-fit area" tyle="width: 100%; height: 50%;" allowAlternating="true" allowResize="true" border="true" sortMode="client">
			<input class="mini-hidden" name="id"/>
			<div class="mini-panel" title="成交双方信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
				<div class="leftarea">
					<fieldset>
						<legend>本方信息</legend>
						<input id="direction" name="direction" class="mini-combobox mini-mustFill"  labelField="true"  onvaluechanged="dirVerify"  label="本方方向："  required="true"   style="width:100%;"  labelStyle="text-align:left;width:120px;" data = "CommonUtil.serverData.dictionary.lending" />
						<input id="instId" name="instId" class="mini-textbox mini-mustFill"  labelField="true"  label="机构："  style="width:100%;" required="true"   enabled="false" labelStyle="text-align:left;width:120px;"/>
						<input id="dealer" name="myUserName" class="mini-textbox mini-mustFill" labelField="true"  label="交易员："  vtype="maxLength:5" required="true"   enabled="false" labelStyle="text-align:left;width:120px;" style="width:100%;"   />
					</fieldset>
				</div>
				<div class="rightarea">
					<fieldset>
						<legend>对手方信息</legend>
						<input id="oppoDir" name="oppoDir" class="mini-combobox mini-mustFill"  enabled="false" labelField="true"  label="对方方向："  required="true"   style="width:100%;"  labelStyle="text-align:left;width:120px;" data = "CommonUtil.serverData.dictionary.lending" />
						<input id="counterpartyInstId" name="counterpartyInstId" class="mini-textbox mini-mustFill" onbuttonclick="onButtonEdit" labelField="true"  required="true"   label="机构：" allowInput="false" style="width:100%;"  labelStyle="text-align:left;width:120px;" />
						<input id="counterpartyDealer" name="counterpartyDealer" class="mini-textbox mini-mustFill" labelField="true"  label="交易员：" vtype="maxLength:30" labelStyle="text-align:left;width:120px;" style="width:100%;"   />
					</fieldset>
				</div>
			</div>
			<div class="mini-panel" title="交易主体信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
				<div class="leftarea">
					<input id="forDate" name="forDate" class="mini-datepicker" labelField="true"  label="交易日期："  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
					<input style="width:100%;" id="amount" name="amount" class="mini-spinner mini-mustFill" labelField="true"  changeOnMousewheel='false'  format="n4" label="拆借金额(元)：" onValuechanged="interestAmount" required="true"  maxValue="999999999999999" labelStyle="text-align:left;width:130px;" onvalidation="zeroValidation"   />
					<input style="width:100%;" id="valueDate" name="valueDate" class="mini-datepicker mini-mustFill" labelField="true" onValuechanged="compareDate" label="起息日：" ondrawdate="onDrawDateStart" required="true"   labelStyle="text-align:left;width:130px;" />
					<input style="width:100%" id="tenor" name="tenor" class="mini-spinner mini-mustFill" labelField="true"  label="拆借期限(天)：" minValue="0" required="true"   maxValue="99999"  changeOnMousewheel="false"   labelStyle="text-align:left;width:130px;" />
					<input style="width:100%;" id="basis" name="basis" class="mini-combobox mini-mustFill" labelField="true"  label="计息基准：" onValuechanged="interestAmount"  data="CommonUtil.serverData.dictionary.Basis" labelStyle="text-align:left;width:130px;" required="true" />
					<input style="width:100%;" id="repaymentAmount" name="repaymentAmount" class="mini-spinner mini-mustFill" labelField="true" changeOnMousewheel='false'  format="n4" label="到期还款金额：" required="true"  maxValue="999999999999999" labelStyle="text-align:left;width:130px;" />
					<input id="tradingModel" name="tradingModel" class="mini-combobox mini-mustFill" required="true"  labelField="true"   label="交易模式："  labelStyle="text-align:left;width:130px;" style="width:100%;"  data="CommonUtil.serverData.dictionary.TradeModel"/>
					<input id="note" name="note" class="mini-textbox" labelField="true" label="交易事由：" labelStyle="text-align:left;width:130px;" style="width:100%;" vtype="maxLength:16"/>
				</div>
				<div class="rightarea">
					<input style="width:100%;" id="currency" name="currency" class="mini-combobox mini-mustFill" labelField="true"  data="CommonUtil.serverData.dictionary.Currency" label="货币："   labelStyle="text-align:left;width:130px;" required="true"  allowInput="false" />
					<input style="width:100%;" id="rate" name="rate" class="mini-spinner mini-mustFill" labelField="true"  label="拆借利率(%)：" onValuechanged="interestAmount"  changeOnMousewheel='false' required="true"  format="n8" labelStyle="text-align:left;width:130px;" />
					<input style="width:100%;" id="maturityDate" name="maturityDate" class="mini-datepicker mini-mustFill" labelField="true" onValuechanged="compareDate" label="到期日：" ondrawdate="onDrawDateEnd" required="true"  labelStyle="text-align:left;width:130px;"    />
					<input style="width:100%;" id="occupancyDays" name="occupancyDays" class="mini-spinner" labelField="true" minErrorText="0" label="实际占款天数：" onValuechanged="interestAmount" changeOnMousewheel='false' maxValue="10000" labelStyle="text-align:left;width:130px;"    />
					<input style="width:100%;" id="interest" name="interest" class="mini-spinner mini-mustFill" labelField="true"  label="应计利息：" maxValue="999999999999999" changeOnMousewheel='false'  format="n4" required="true"  labelStyle="text-align:left;width:130px;"/>
					<input id="rateCode" name="rateCode" class="mini-textbox mini-mustFill" onbuttonclick="onRateEdit" labelField="true" label="利率代码：" style="width: 100%;" labelStyle="text-align:left;width:130px;" allowInput="false" requiredErrorText="该输入项为必输项"  required="true" />
					<!-- <input id="nettingStatus" name="nettingStatus" class="mini-combobox mini-mustFill" required="true"  labelField="true" label="净额清算状态：" labelStyle="text-align:left;width:130px;" style="width:100%;" data="CommonUtil.serverData.dictionary.YesNo" showNullItem="true"/> -->
					<input id="dealTransType" name="dealTransType" class="mini-combobox" labelField="true"  style="width:100%;"  label="数据来源：" data="CommonUtil.serverData.dictionary.dealTransType1" vtype="maxLength:10"  labelStyle="text-align:left;width:130px;"  value="1">
				</div>
			</div>
			<%@ include file="../../Common/opicsLessDetail.jsp"%>
			<div class="mini-panel" title="清算信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
				<div class="leftarea">
					<fieldset >
						<legend>本方账户</legend>
						<input id="buyCurreny" name="buyCurreny" class="mini-combobox" labelField="true"  label="清算货币："  style="width:100%;"  labelStyle="text-align:left;width:120px;" data="CommonUtil.serverData.dictionary.Currency"/>
						<input id="bankName1" name="bankName1" class="mini-textbox" labelField="true"  label="开户行名称："  labelStyle="text-align:left;width:120px;" style="width:100%;" requiredErrorText="该输入项为必输项"    vtype="maxLength:60"  />
						<input id="bankBic1" name="bankBic1" class="mini-textbox" labelField="true"  label="开户行BIC CODE："  labelStyle="text-align:left;width:120px;" style="width:100%;"  requiredErrorText="该输入项为必输项"     onvalidation="onEnglishAndNumberValidation"  vtype="maxLength:12"/>
						<input id="bankAccount1" name="bankAccount1" class="mini-textbox" labelField="true"  label="开户行行号："  labelStyle="text-align:left;width:120px;" style="width:100%;" requiredErrorText="该输入项为必输项"   onvalidation="onEnglishAndNumberValidation" vtype="maxLength:32" />
						<input id="dueBank1" name="dueBank1" class="mini-textbox" labelField="true"  label="收款行名称："  labelStyle="text-align:left;width:120px;" style="width:100%;"  requiredErrorText="该输入项为必输项"  vtype="maxLength:60"/>
						<input id="dueBankName1" name="dueBankName1" class="mini-textbox" labelField="true"  label="收款行BIC CODE：" labelStyle="text-align:left;width:120px;" style="width:100%;"  requiredErrorText="该输入项为必输项"    onvalidation="onEnglishAndNumberValidation" vtype="maxLength:12"/>
						<input id="borrowAccnum" name="borrowAccnum" class="mini-textbox" labelField="true"  label="收款行账号："  labelStyle="text-align:left;width:120px;" style="width:100%;" requiredErrorText="该输入项为必输项"   onvalidation="onEnglishAndNumberValidation" vtype="maxLength:32" />
						<input id="intermediaryBank1" name="intermediaryBank1" class="mini-textbox" labelField="true"  label="中间行名称："  labelStyle="text-align:left;width:120px;" style="width:100%;"  requiredErrorText="该输入项为必输项"  vtype="maxLength:60"/>
						<input id="intermediaryBicCode1" name="intermediaryBicCode1" class="mini-textbox" labelField="true"  label="中间行BIC CODE："  labelStyle="text-align:left;width:120px;" style="width:100%;"  requiredErrorText="该输入项为必输项" onvalidation="onEnglishAndNumberValidation"  vtype="maxLength:45"/>
						<input id="intermediaryBankAcctNo1" name="intermediaryBankAcctNo1" class="mini-textbox" labelField="true"  label="中间行行号："  labelStyle="text-align:left;width:120px;" style="width:100%;" requiredErrorText="该输入项为必输项"   onvalidation="onEnglishAndNumberValidation" vtype="maxLength:32" />
					</fieldset>
				</div>
				<div class="rightarea">
					<fieldset>
						<legend>对手方账户</legend>
						<input id="sellCurreny" name="sellCurreny" class="mini-combobox" labelField="true"  label="清算货币："  style="width:100%;"  labelStyle="text-align:left;width:120px;" requiredErrorText="该输入项为必输项"  data="CommonUtil.serverData.dictionary.Currency"/>
						<input id="bankName2" name="bankName2" class="mini-textbox" labelField="true"  label="开户行名称："  labelStyle="text-align:left;width:120px;" style="width:100%;"  requiredErrorText="该输入项为必输项"  vtype="maxLength:60" />
						<input id="bankBic2" name="bankBic2" class="mini-textbox" labelField="true"  label="开户行BIC CODE："  labelStyle="text-align:left;width:120px;" style="width:100%;"  requiredErrorText="该输入项为必输项"   onvalidation="onEnglishAndNumberValidation"  vtype="maxLength:12" />
						<input id="bankAccount2" name="bankAccount2" class="mini-textbox" labelField="true"  label="开户行行号："  labelStyle="text-align:left;width:120px;" style="width:100%;" requiredErrorText="该输入项为必输项"   onvalidation="onEnglishAndNumberValidation" vtype="maxLength:32" />
						<input id="dueBank2" name="dueBank2" class="mini-textbox" labelField="true"  label="收款行名称："  labelStyle="text-align:left;width:120px;" style="width:100%;"  requiredErrorText="该输入项为必输项"  vtype="maxLength:60"/>
						<input id="dueBankName2" name="dueBankName2"  class="mini-textbox" labelField="true"  label="收款行BIC CODE："  labelStyle="text-align:left;width:120px;" style="width:100%;" requiredErrorText="该输入项为必输项"   onvalidation="onEnglishAndNumberValidation"  vtype="maxLength:12"/>
						<input id="removeAccnum" name="removeAccnum" class="mini-textbox" labelField="true"  label="收款行账号："  labelStyle="text-align:left;width:120px;" style="width:100%;" requiredErrorText="该输入项为必输项"  onvalidation="onEnglishAndNumberValidation"  vtype="maxLength:32" />
						<input id="intermediaryBank2" name="intermediaryBank2" class="mini-textbox" labelField="true"  label="中间行名称："  labelStyle="text-align:left;width:120px;" style="width:100%;"  requiredErrorText="该输入项为必输项"  vtype="maxLength:60"/>
						<input id="intermediaryBicCode2" name="intermediaryBicCode2" class="mini-textbox" labelField="true"  label="中间行BIC CODE："  labelStyle="text-align:left;width:120px;" style="width:100%;"  requiredErrorText="该输入项为必输项" onvalidation="onEnglishAndNumberValidation"  vtype="maxLength:45"/>
						<input id="intermediaryBankAcctNo2" name="intermediaryBankAcctNo2" class="mini-textbox" labelField="true"  label="中间行行号："  labelStyle="text-align:left;width:120px;" style="width:100%;" requiredErrorText="该输入项为必输项"   onvalidation="onEnglishAndNumberValidation" vtype="maxLength:32" />
					</fieldset>
				</div>
			</div>
			</div>
	</fieldset>
	</div>
	<script>
		mini.parse();
		var form = new mini.Form("#search_form");

		var url = window.location.search;
		var prdNo = CommonUtil.getParam(url, "prdNo");
		var fPrdCode = CommonUtil.getParam(url, "fPrdCode");
	    var prdName = CommonUtil.getParam(url, "prdName");
	    var dealType = CommonUtil.getParam(url, "dealType");
	    
		var grid = mini.get("datagrid");
		
		/**************************************点击下面显示详情开始******************************/
		var from = new mini.Form("MiniSettleForeigDetail");
		from.setEnabled(false);
		var grid = mini.get("datagrid");
		//grid.load();
		 //绑定表单
	    var db = new mini.DataBinding();
	    db.bindForm("MiniSettleForeigDetail", grid);
	    /**************************************点击下面显示详情结束******************************/
	    
		grid.on("beforeload", function(e) {
			e.cancel = true;
			var pageIndex = e.data.pageIndex;
			var pageSize = e.data.pageSize;
			search(pageSize, pageIndex);
		});

		grid.on("select",function(e){
			grid.on("select",function(e){
				var rows=grid.getSelecteds();
				for(var i=0;i<rows.length;i++){
					if(rows[i].approveStatus != "3"){
						mini.get("opics_check_btn").setEnabled(false);
						break;
					}
				}
			});
			var row=e.record;
			mini.get("approve_mine_commit_btn").setEnabled(false);
			mini.get("approve_commit_btn").setEnabled(false);
			mini.get("edit_btn").setEnabled(false);
			mini.get("delete_btn").setEnabled(false);
			mini.get("approve_log").setEnabled(true);
			mini.get("recall_btn").setEnabled(true);
			mini.get("batch_commit_btn").setEnabled(true);
			if(row.approveStatus == "3"){//新建
				mini.get("edit_btn").setEnabled(true);
				mini.get("delete_btn").setEnabled(true);
				mini.get("approve_mine_commit_btn").setEnabled(true);
				mini.get("approve_commit_btn").setEnabled(false);
				mini.get("reback_btn").setEnabled(false);
				mini.get("recall_btn").setEnabled(false);
				mini.get("opics_check_btn").setEnabled(true);
			}
			if( row.approveStatus == "6" || row.approveStatus == "7" || row.approveStatus == "8" || row.approveStatus == "17"){//审批通过：6    审批拒绝:5
				mini.get("edit_btn").setEnabled(false);
				mini.get("delete_btn").setEnabled(false);
				mini.get("approve_mine_commit_btn").setEnabled(false);
				mini.get("approve_commit_btn").setEnabled(true);
				mini.get("batch_approve_btn").setEnabled(false);
				mini.get("batch_commit_btn").setEnabled(false);
				mini.get("opics_check_btn").setEnabled(false);
				mini.get("reback_btn").setEnabled(false);
				mini.get("recall_btn").setEnabled(false);
			}
			if(row.approveStatus == "5"){//审批中:5
				mini.get("edit_btn").setEnabled(false);
				mini.get("delete_btn").setEnabled(false);
				mini.get("approve_mine_commit_btn").setEnabled(false);
				mini.get("approve_commit_btn").setEnabled(true);
				mini.get("batch_commit_btn").setEnabled(false);
				mini.get("reback_btn").setEnabled(true);
				mini.get("recall_btn").setEnabled(true);
				mini.get("opics_check_btn").setEnabled(false);
			}
			
		});
		
		function getData(action) {
			var row = null;
			if (action != "add") {
				row = grid.getSelected();
			}
			return row;
		}
	
		function search(pageSize,pageIndex){
			form.validate();
			if(form.isValid()==false){
				mini.alert("信息填写有误，请重新填写","系统提示");
				return;
			}
			var data=form.getData(true);
			data['pageNumber']=pageIndex+1;
			data['pageSize']=pageSize;
			data['branchId']=branchId;
			data['fPrdCode']=fPrdCode;
			var url=null;
			var approveType = mini.get("approveType").getValue();
			if(approveType == "mine"){
				url = "/IfsCurrencyController/searchPageCcyLendingMine";
			}else if(approveType == "approve"){
				url = "/IfsCurrencyController/searchPageCcyLendingUnfinished";
			}else{
				url = "/IfsCurrencyController/searchPageCcyLendingFinished";
			}
			var params = mini.encode(data);
			CommonUtil.ajax({
				url:url,
				data:params,
				callback : function(data) {
					grid.setTotalCount(data.obj.total);
					grid.setPageIndex(pageIndex);
			        grid.setPageSize(pageSize);
					grid.setData(data.obj.rows);
				}
			});
		}
		
		function getData(action) {
			var row = null;
			if (action != "add") {
				row = grid.getSelected();
			}
			return row;
		}
		
		//增删改查
		function add(){
			var url = CommonUtil.baseWebPath() + "/cfetsfx/ccyLendingEdit.jsp?action=add&prdNo="+prdNo+"&prdName="+prdName+"&dealType="+dealType+"&fPrdCode="+fPrdCode;
			var tab = { id: "MiniCcyLendingAdd", name: "MiniCcyLendingAdd", title: "外币拆借补录", url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
			var paramData = {selectData:""};
			CommonUtil.openNewMenuTab(tab,paramData);
		}
		
		function query() {
            search(grid.pageSize, 0);
        }
		
		function clear(){
			var MiniSettleForeigDetail=new mini.Form("MiniSettleForeigDetail");
			MiniSettleForeigDetail.clear();
            form.clear();
            search(10,0);
		}
		
		function edit(){
			var row = grid.getSelected();
			var selections = grid.getSelecteds();
			if(selections.length>1){
				mini.alert("系统不支持多条数据进行编辑","消息提示");
				return;
			}
			if(row){
				var url = CommonUtil.baseWebPath() + "/cfetsfx/ccyLendingEdit.jsp?action=edit&ticketid="+row.ticketId+"&prdNo="+prdNo+"&prdName="+prdName+"&dealType="+dealType+"&fPrdCode="+fPrdCode;
				var tab = { id: "MiniCcyLendingEdit", name: "MiniCcyLendingEdit", title: "外币拆借修改", url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
				var paramData = {selectData:row};
			    CommonUtil.openNewMenuTab(tab,paramData);
			} else {
				mini.alert("请选中一条记录！","消息提示");
			}
		}

		function onRowDblClick(e) {
			var row = grid.getSelected();
			if(row){
				var url = CommonUtil.baseWebPath() + "/cfetsfx/ccyLendingEdit.jsp?action=detail&ticketid="+row.ticketId+"&prdNo="+prdNo+"&prdName="+prdName+"&dealType="+dealType+"&fPrdCode="+fPrdCode;
				var tab = { id: "MiniCcyLendingDetail", name: row.ticketId, title: "外币拆借详情", url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
				var paramData = {selectData:row};
			    CommonUtil.openNewMenuTab(tab,paramData);
			} else {
				mini.alert("请选中一条记录！","消息提示");
			}
		}
		
		//删除
		function del() {
			var grid = mini.get("datagrid");
			var row = grid.getSelected();
			var selections = grid.getSelecteds();
			if(selections.length>1){
				mini.alert("系统不支持多条数据进行删除","消息提示");
				return;
			}
			if (row) {
				mini.confirm("您确认要删除选中记录?","系统警告",function(value){
					if(value=="ok"){
					CommonUtil.ajax({
						url: "/IfsCurrencyController/deleteCcyLending",
						data: {ticketid: row.ticketId},
						callback: function (data) {
							if (data.code == 'error.common.0000') {
								mini.alert("删除成功");
								grid.reload();
								//search(grid.pageSize,grid.pageIndex);
							} else {
								mini.alert("删除失败");
							}
						}
					});
					}
				});
			}
			else {
				mini.alert("请选中一条记录！", "消息提示");
			}
		}
		
		/**************************审批相关****************************/
		//审批日志查看
		function appLog(selections){
			var flow_type = Approve.FlowType.VerifyApproveFlow;
			if(selections.length <= 0){
				mini.alert("请选择要操作的数据","系统提示");
				return;
			}
			if(selections.length > 1){
				mini.alert("系统不支持多笔操作","系统提示");
				return;
			}
			if(selections[0].tradeSource == "3"){
				mini.alert("初始化导入的业务没有审批日志","系统提示");
				return false;
			}
			Approve.approveLog(flow_type,selections[0].ticketId);
		};
		
	//提交正式审批、待审批
	function verify(selections){
		var grid = mini.get("datagrid");
		var row = grid.getSelected();
		if(selections.length == 0){
			mini.alert("请选中一条记录！","消息提示");
			return false;
		}else if(selections.length > 1){
			mini.alert("该功能不支持多笔提交，如需多笔提交请点击[批量提交]按钮","系统提示");
			return false;
		}
		if(CommonUtil.isNull(selections[0]["dealSource"])||CommonUtil.isNull(selections[0]["port"])||CommonUtil.isNull(selections[0]["cost"])||
   			 CommonUtil.isNull(selections[0]["product"])||CommonUtil.isNull(selections[0]["prodType"])){
	   		 mini.alert("请先进行opics要素检查更新!","提示");
	   		 return;
   		}
		if(selections[0]["approveStatus"] != "3" ){
			var searchByIdUrl = "/IfsCfetsrmbDpController/searchCfetsrmbDpAndFlowIdById";
			   Approve.goToApproveJsp(selections[0].taskId,"438","/cfetsfx/ccyLendingEdit.jsp", target);
			var target = Approve.approvePage;
			var openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid="+row.ticketId+"&prdNo=ccyLending"+"&fPrdCode="+fPrdCode;
			var id="AdvanceCcyLendingApprove";
			var title="外币拆借审批";
			var tab = {"id": id,name:id,url:openJspUrl,title:title,parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
			var paramData = {selectData:selections[0]};
			CommonUtil.openNewMenuTab(tab,paramData);
			// top["win"].showTab(tab);
		}else{
			Approve.approveCommit(Approve.FlowType.VerifyApproveFlow,selections[0]["ticketId"],Approve.OrderStatus.New,"IfsLendService",prdNo,function(){
				search(grid.pageSize,grid.pageIndex);
			});
		}
	};
		
	//审批
	function approve(){
		mini.get("approve_commit_btn").setEnabled(false);
		var messageid = mini.loading("系统正在处理...", "请稍后");
		try {
			verify(grid.getSelecteds());
		} catch (error) {
			
		}
		mini.hideMessageBox(messageid);	
		mini.get("approve_commit_btn").setEnabled(true);
	}
		
	//提交审批
	function commit(){
		mini.get("approve_mine_commit_btn").setEnabled(false);
		var messageid = mini.loading("系统正在处理...", "请稍后");
		try {
			verify(grid.getSelecteds());
		} catch (error) {
			
		}
		mini.hideMessageBox(messageid);	
		mini.get("approve_mine_commit_btn").setEnabled(true);
	}
		
	//审批日志
	function searchlog(){
		appLog(grid.getSelecteds());
	}

	//打印
	function print(){
		var selections = grid.getSelecteds();
		if(selections == null || selections.length == 0){
			mini.alert('请选择一条要打印的数据！','系统提示');
			return false;
		}else if(selections.length>1){
			mini.alert('暂不支持多条数据同时打印，请选择一条要打印的数据！','系统提示');
			return false;
		}
		var canPrint = selections[0].ticketId;

		if(!CommonUtil.isNull(canPrint)){
			var actionStr = CommonUtil.pPath + "/sl/IfsPrintUtilController/exportload/print/"+PrintNo.ccyLending+"/" + canPrint;
			$('<form target="_blank" action=' + actionStr + ' method="post"></form>').appendTo('body').submit().remove();
		};
	}

		//导出报表
		function exportExcel(){
			exportExcelManin("jywbcj.xls","jywbcj");
		}
		
	//交易日期
	function onDrawDateStart(e) {
		var startDate = e.date;
		var endDate= mini.get("endDate").getValue();
		if(CommonUtil.isNull(endDate)){
			return;
		}
		if (endDate.getTime() < startDate.getTime()) {
			e.allowSelect = false;
		}
	}
	  
	function onDrawDateEnd(e) {
		var endDate = e.date;
		var startDate = mini.get("startDate").getValue();
		if(CommonUtil.isNull(startDate)){
			return;
		}
		if (endDate.getTime() < startDate.getTime()) {
			e.allowSelect = false;
		}
	}
		
	$(document).ready(function() {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
			initButton();
			search(10, 0);
		});
	});
</script>
</body>
</html>