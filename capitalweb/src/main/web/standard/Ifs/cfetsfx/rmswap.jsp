<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
	<!-- <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> -->
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
<script type="text/javascript" >
	/**prdCode用于页面投资组合的选择   该全局变量必须为prdCode*/
	var prdCode="FXD-SWAP";
</script>
<title>外汇掉期</title>
</head>
<body style="width: 100%; height: 100%; background: white">
	<fieldset class="mini-fieldset">
		<legend>查询</legend>
		<div id="search_form" style="width: 100%;">
			<nobr>
			<input id="startDate" name="startDate" class="mini-datepicker" labelField="true" label="交易日期：" value="<%=__bizDate%>"
						ondrawdate="onDrawDateStart" labelStyle="text-align:right;" emptyText="起始日期" format="yyyy-MM-dd"/>
			<span>~</span>
			<input id="endDate" name="endDate" class="mini-datepicker" value="<%=__bizDate%>"
						ondrawdate="onDrawDateEnd" emptyText="结束日期" format="yyyy-MM-dd"/>
			</nobr>
			<input id="contractId" name="contractId" class="mini-textbox" labelField="true" label="成交单编号：" emptyText="请填写成交单编号" labelStyle="text-align:right;" width="280px"/>
			<input id="ticketId" name="ticketId" class="mini-textbox" labelField="true" label="审批单号：
				" labelStyle="text-align:right;" emptyText="请填写审批单号" width="280px" />
			<input id="dealNo" name="dealNo" class="mini-textbox" labelField="true" label="opics交易号：" emptyText="请填写opics交易号" labelStyle="text-align:right;" width="280px"/>
			<input id="approveStatus" name="approveStatus" class="mini-combobox" data="CommonUtil.serverData.dictionary.ApproveSta" width="280px" emptyText="请选择审批状态" labelField="true"  label="审批状态：" labelStyle="text-align:right;"/>
			<span style="float: right; margin-right: 150px"> 
				<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
			</span>
		</div>
	</fieldset>
	<%@ include file="../batchReback.jsp"%>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
	 
	<div id="MiniSettleForeignSpot" class="mini-fit" style="margin-top: 2px;">
		<div id="datagrid" class="mini-datagrid borderAll" style="width: 100%; height: 70%;" allowAlternating="true"  multiSelect="true"
			allowResize="true" border="true" sortMode="client" onrowdblclick="onRowDblClick">
			<div property="columns">
			<div type="checkcolumn"></div>
				<div type="indexcolumn" headerAlign="center" width="40">序号</div>
				<div field="prdNo" width="100px" allowSort="false" headerAlign="center" align="center" visible="false"></div>
				<div field="forDate" width="100px" allowSort="false" headerAlign="center" align="center">交易日期</div>
				<div field="forTime" width="100px" allowSort="false" headerAlign="center" align="center">交易时间</div>
				<div field="taskName" width="120px" allowSort="false" headerAlign="center" align="center">审批状态</div>
				<div field="contractId" width="160px" allowSort="false" headerAlign="center" align="center">成交单编号</div>
				<div field="ticketId" width="160px" allowSort="false" headerAlign="center" align="center">审批单号</div>
				<div field="dealNo" width="100px" align="center"  headerAlign="center" >opics交易号</div>
				<div field="farDealNo" width="100px" align="center"  headerAlign="center" >opics远端交易号</div>
				<div field="statcode" width="160px" align="center"  headerAlign="center" >opics处理状态</div>
				<div field="counterpartyInstId" width="200px" allowSort="false" headerAlign="center" align="center">对方机构</div>
				<div field="currencyPair" width="100px" allowSort="false" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'CurrencyPair'}">货币对</div>				
				<div field="opicsccy" width="100px" allowSort="false" headerAlign="center" align="center">交易货币</div>				
				<div field="direction" width="100px" allowSort="true" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'Swaptrading'}">交易方向</div>
				<div field="nearRate" width="120px" allowSort="false" headerAlign="center" align="right" numberFormat="#,0.00000000">近端汇率</div>
				<div field="nearPrice" width="180px" allowSort="true" headerAlign="center" align="right" numberFormat="#,0.0000">交易金额1（近端）</div>
				<div field="nearReversePrice" width="180px" allowSort="false" headerAlign="center" align="right" numberFormat="#,0.0000">交易金额2（近端）</div>
				<div field="nearValuedate" width="100px" allowSort="false" headerAlign="center" align="center">起息日（近端）</div>
				<div field="fwdValuedate" width="100px" allowSort="false" headerAlign="center" align="center">起息日（远端）</div>
				<div field="fwdRate" width="120px" allowSort="false" headerAlign="center" align="right" numberFormat="#,0.00000000">远端汇率</div>
				<div field="fwdPrice" width="180px" allowSort="true" headerAlign="center" align="right" numberFormat="#,0.0000">交易金额1（远端）</div>
				<div field="fwdReversePrice" width="180px" allowSort="false" headerAlign="center" align="right" numberFormat="#,0.0000">交易金额2（远端）</div>			
				<div field="dealSource" width="100px" align="center"  headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'TradeSource'}">交易来源</div>
				<div field="transKind" width="100px" allowSort="true" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'transCode'}">购售业务种类</div>
				<div field="purposeCode" width="100px" allowSort="true" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'purposeType'}">购售用途</div>
				<div field="sponsor" width="120px" allowSort="false" headerAlign="center" align="center">审批发起人</div>
				<div field="sponInst" width="120px" allowSort="false" headerAlign="center" align="center">审批发起机构</div>
				<div field="aDate" width="160px" allowSort="false" headerAlign="center" align="center">审批发起时间</div>
				
			</div>
		</div>
	<fieldset style="width: 100%;border:solid 1px #aaa;margin-top:8px;position:relative;">
		<legend>外汇掉期详情</legend>
			<div id="MiniSettleForeigDetail" style="padding:5px;" class="mini-fit area" tyle="width: 100%; height: 100%;" allowAlternating="true" allowResize="true" border="true" sortMode="client">
			<input class="mini-hidden" name="id"/>
				<div class="mini-panel" title="成交双方信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
					<div class="leftarea">
						<input id="instId" name="instId" class="mini-textbox mini-mustFill" labelField="true"  label="本方机构：" style="width:100%;"  labelStyle="text-align:left;width:130px"requiredErrorText="该输入项为必输项" required="true"  vtype="maxLength:16"/>
						<input id="dealer" name="myUserName" class="mini-textbox mini-mustFill" labelField="true"  label="本方交易员："  labelStyle="text-align:left;width:130px"style="width:100%;"   requiredErrorText="该输入项为必输项" required="true"  vtype="maxLength:20" onvalidation="onEnglishAndNumberValidation" />
					</div>
					<div class="rightarea">
						<input id="counterpartyInstId" name="counterpartyInstId" class="mini-textbox mini-mustFill" requiredErrorText="该输入项为必输项" required="true"  onbuttonclick="onButtonEdit" labelField="true" allowInput="false" label="对方机构："  style="width:100%;"  labelStyle="text-align:left;width:130px" vtype="maxLength:16"/>
						<input id="counterpartyDealer" name="counterpartyDealer" class="mini-textbox mini-mustFill" labelField="true"  label="对方交易员：" labelStyle="text-align:left;width:130px"style="width:100%;"   vtype="maxLength:30" required="true" />
					</div>
				</div>			
				<div class="mini-panel" title="交易主体信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
					<div class="leftarea">
						<input id="forDate" name="forDate" class="mini-datepicker mini-mustFill" labelField="true" requiredErrorText="该输入项为必输项" required="true"  label="交易日期："  labelStyle="text-align:left;width:130px"style="width:100%;"   />
						<input style="width:100%;"id="currencyPair" name="currencyPair" class="mini-combobox mini-mustFill" labelField="true"  label="货币对："   requiredErrorText="该输入项为必输项" required="true"  labelStyle="text-align:left;width:130px" data="CommonUtil.serverData.dictionary.CurrencyPair" onvaluechanged="onCurrencyPairChange"/>
						<input style="width:100%;"  id="opicsccy" name="opicsccy" class="mini-combobox mini-mustFill" labelField="true"  label="交易货币："  requiredErrorText="该输入项为必输项" required="true"   labelStyle="text-align:left;width:130px;"/>
					</div>
					<div class="rightarea">
						<input id="forTime" name="forTime" class="mini-timespinner mini-mustFill" labelField="true"  label="交易时间："  labelStyle="text-align:left;width:130px"requiredErrorText="该输入项为必输项" required="true"  style="width:100%;"   />
						<input style="width:100%;"id="direction" name="direction" class="mini-combobox mini-mustFill" labelField="true"  label="交易方向：" requiredErrorText="该输入项为必输项" required="true"  labelStyle="text-align:left;width:130px"    data="CommonUtil.serverData.dictionary.Swaptrading" onvaluechanged="onCurrencyPairChange"/>
					</div>
					<div class="centerarea">
						<input style="width:50%;" id="price"  name="price"  class="mini-spinner mini-mustFill" labelField="true"  label="SPOT汇率："  format="n8" maxValue="999999999999" changeOnMousewheel="false"   labelStyle="text-align:left;width:130px;"  onvaluechanged="getReversePrice" requiredErrorText="该输入项为必输项" required="true" />
					</div>
					<div class="leftarea">
						<input style="width:100%;"id="nearValuedate" name="nearValuedate" class="mini-datepicker mini-mustFill" labelField="true"  label="近端起息日：" requiredErrorText="该输入项为必输项" required="true"  ondrawdate="onDrawDateNear" onvaluechanged="associaDateCalculation" labelStyle="text-align:left;width:130px"    />
						<input style="width:100%;"id="nearPrice" name="nearPrice" class="mini-spinner mini-mustFill" labelField="true"  label="近端交易金额1：" requiredErrorText="该输入项为必输项" required="true"  maxValue="999999999999" changeOnMousewheel='false' format="n4"  labelStyle="text-align:left;width:130px"  onvaluechanged="getReversePrice" onvalidation="zeroValidation"/>
						<input style="width:100%;"id="nearSpread" name="nearSpread" class="mini-spinner mini-mustFill" labelField="true"  label="近端点：" changeOnMousewheel='false' format="n8" requiredErrorText="该输入项为必输项" required="true"  labelStyle="text-align:left;width:130px" onvaluechanged="getReversePrice" minValue="-1000000" maxValue="1000000" value="0"/>
						<input style="width:100%;"id="nearRate" name="nearRate" class="mini-spinner mini-mustFill" labelField="true"  label="近端成交汇率："  requiredErrorText="该输入项为必输项" required="true"  format="n8" changeOnMousewheel="false" maxValue="999999999999" labelStyle="text-align:left;width:130px" enabled="false"/>
						<input style="width:100%;"id="nearReversePrice" name="nearReversePrice" class="mini-spinner mini-mustFill" labelField="true"  label="近端交易金额2：" requiredErrorText="该输入项为必输项" required="true"  maxValue="999999999999" changeOnMousewheel='false' format="n4"  labelStyle="text-align:left;width:130px"  onvalidation="zeroValidation"  />
						<input style="width:100%;"id="tenor" name="tenor" class="mini-spinner" labelField="true"  label="期限：" requiredErrorText="该输入项为必输项"  enabled="false" maxValue="99999" changeOnMousewheel='false' labelStyle="text-align:left;width:130px"    />
						<input id="nettingStatus" name="nettingStatus" class="mini-combobox mini-mustFill" required="true"  labelField="true"   label="净额清算状态：" labelStyle="text-align:left;width:130px;" style="width:100%;"  data="CommonUtil.serverData.dictionary.YesNo" showNullItem="true"/>
						<input id="tradingType" name="tradingType" class="mini-combobox mini-mustFill" labelField="true"  label="交易方式：" requiredErrorText="该输入项为必输项" required="true"  value="RFQ" labelStyle="text-align:left;width:130px"style="width:100%;" data="CommonUtil.serverData.dictionary.TradeType"/>
						<input id="note" name="note" class="mini-textbox" labelField="true" label="交易事由：" labelStyle="text-align:left;width:130px;" style="width:100%;" vtype="maxLength:16"/>
<!-- 						<input id="delaydElivind" name="delaydElivind" class="mini-combobox" labelField="true"   label="延期交易：" labelStyle="text-align:left;width:130px;" style="width:100%;" data="CommonUtil.serverData.dictionary.delaydElivind" value='0'/> -->
					</div>
					<div class="rightarea">
						<input style="width:100%;"id="fwdValuedate" name="fwdValuedate" class="mini-datepicker mini-mustFill" labelField="true"  label="远端起息日："  labelStyle="text-align:left;width:130px" requiredErrorText="该输入项为必输项" ondrawdate="onDrawDateFar" onvaluechanged="associaDateCalculation" required="true"    />
						<input style="width:100%;"id="fwdPrice" name="fwdPrice" class="mini-spinner mini-mustFill" labelField="true"  label="远端交易金额1：" requiredErrorText="该输入项为必输项" required="true"  maxValue="999999999999" changeOnMousewheel='false'  format="n4" labelStyle="text-align:left;width:130px"  onvaluechanged="getFwdReversePrice" onvalidation="zeroValidation" />
						<input style="width:100%;"id="spread" name="spread" class="mini-spinner mini-mustFill" labelField="true"  label="远端点：" changeOnMousewheel='false' format="n8" requiredErrorText="该输入项为必输项" required="true"  labelStyle="text-align:left;width:130px" onvaluechanged="getReversePrice" minValue="-1000000" maxValue="1000000" value="0"/>
						<input style="width:100%;"id="fwdRate" name="fwdRate" class="mini-spinner mini-mustFill" labelField="true"  label="远端成交汇率："  format="n8" changeOnMousewheel="false" requiredErrorText="该输入项为必输项" required="true"  maxValue="999999999999" labelStyle="text-align:left;width:130px"   data="CommonUtil.serverData.dictionary.trading"  enabled="false"/>
						<input style="width:100%;"id="fwdReversePrice" name="fwdReversePrice" class="mini-spinner mini-mustFill" labelField="true"  label="远端交易金额2：" maxValue="999999999999" requiredErrorText="该输入项为必输项" required="true"  changeOnMousewheel='false'  format="n4" labelStyle="text-align:left;width:130px"  onvalidation="zeroValidation"  />
						<input id="tradingModel" name="tradingModel" class="mini-combobox mini-mustFill" labelField="true" requiredErrorText="该输入项为必输项" required="true"  label="交易模式：" labelStyle="text-align:left;width:130px"style="width:100%;" data="CommonUtil.serverData.dictionary.TradeModel" />
						<input id="dealTransType" name="dealTransType" class="mini-combobox" labelField="true"  style="width:100%;"  label="数据来源：" data="CommonUtil.serverData.dictionary.dealTransType1" vtype="maxLength:10"  labelStyle="text-align:left;width:130px;"  value="1">
					</div>
				</div>  	
				<%@ include file="../../Common/opicsLessDetail.jsp"%>
				<%@ include file="./accountInforSwap.jsp"%>
			</div>
	</fieldset>
	</div>
	<script>
		mini.parse();
		var form = new mini.Form("#search_form");

		var url = window.location.search;
		var prdNo = CommonUtil.getParam(url, "prdNo");
		var fPrdCode = CommonUtil.getParam(url, "fPrdCode");
	    var prdName = CommonUtil.getParam(url, "prdName");
	    var dealType = CommonUtil.getParam(url, "dealType");
	    
		var grid = mini.get("datagrid");
		/**************************************点击下面显示详情开始******************************/
		var from = new mini.Form("MiniSettleForeigDetail");
		from.setEnabled(false);
		var grid = mini.get("datagrid");
		//grid.load();
		 //绑定表单
        var db = new mini.DataBinding();
        db.bindForm("MiniSettleForeigDetail", grid);

        /**************************************点击下面显示详情结束******************************/
		grid.on("beforeload", function(e) {
			e.cancel = true;
			var pageIndex = e.data.pageIndex;
			var pageSize = e.data.pageSize;
			search(pageSize, pageIndex);
		});

		grid.on("select",function(e){
			
			grid.on("select",function(e){
				var rows=grid.getSelecteds();
				for(var i=0;i<rows.length;i++){
					if(rows[i].approveStatus != "3"){
						mini.get("opics_check_btn").setEnabled(false);
						break;
					}
				}
			});
			var row=e.record;
			changeAcct(row.buyCurreny,row.sellCurreny,row.buyCurreny1,row.sellCurreny1)
			mini.get("approve_mine_commit_btn").setEnabled(false);
			mini.get("approve_commit_btn").setEnabled(false);
			mini.get("edit_btn").setEnabled(false);
			mini.get("delete_btn").setEnabled(false);
			mini.get("approve_log").setEnabled(true);
			mini.get("recall_btn").setEnabled(true);
			mini.get("batch_commit_btn").setEnabled(true);
			if(row.approveStatus == "3"){//新建
				mini.get("edit_btn").setEnabled(true);
				mini.get("delete_btn").setEnabled(true);
				mini.get("approve_mine_commit_btn").setEnabled(true);
				mini.get("approve_commit_btn").setEnabled(false);
				mini.get("reback_btn").setEnabled(false);
				mini.get("recall_btn").setEnabled(false);
				mini.get("opics_check_btn").setEnabled(true);
			}
			if( row.approveStatus == "6" || row.approveStatus == "7" || row.approveStatus == "8" || row.approveStatus == "17"){//审批通过：6    审批拒绝:5
				mini.get("edit_btn").setEnabled(false);
				mini.get("delete_btn").setEnabled(false);
				mini.get("approve_mine_commit_btn").setEnabled(false);
				mini.get("approve_commit_btn").setEnabled(true);
				mini.get("batch_approve_btn").setEnabled(false);
				mini.get("batch_commit_btn").setEnabled(false);
				mini.get("opics_check_btn").setEnabled(false);
				mini.get("reback_btn").setEnabled(false);
				mini.get("recall_btn").setEnabled(false);
			}
			if(row.approveStatus == "5"){//审批中:5
				mini.get("edit_btn").setEnabled(false);
				mini.get("delete_btn").setEnabled(false);
				mini.get("approve_mine_commit_btn").setEnabled(false);
				mini.get("approve_commit_btn").setEnabled(true);
				mini.get("batch_commit_btn").setEnabled(false);
				mini.get("reback_btn").setEnabled(true);
				mini.get("recall_btn").setEnabled(true);
				mini.get("opics_check_btn").setEnabled(false);
			}
			
		});

		function search(pageSize, pageIndex) {
			form.validate();
			if (form.isValid() == false) {
				mini.alert("表单填写错误,请确认!", "提示信息");
				return;
			}
			var data = form.getData(true);
			data['pageNumber'] = pageIndex + 1;
			data['pageSize'] = pageSize;
			data['branchId']=branchId;
			data['fPrdCode']=fPrdCode;
			var url=null;
			var approveType = mini.get("approveType").getValue();
			if(approveType == "mine"){//我发起的
				url = "/IfsForeignController/searchPageFxSwapMine";
			}else if(approveType == "approve"){//待审批
				url = "/IfsForeignController/searchPageFxSwapUnfinished";
			}else{//已审批
				url = "/IfsForeignController/searchPageFxSwapFinished";
			}
			var params = mini.encode(data);
			CommonUtil.ajax({
				url : url,
				data : params,
				callback : function(data) {
					var grid = mini.get("datagrid");
					grid.setTotalCount(data.obj.total);
					grid.setPageIndex(pageIndex);
					grid.setPageSize(pageSize);
					grid.setData(data.obj.rows);
				}
			});
		}
		
		function getData(action) {
			var row = null;
			if (action != "add") {
				row = grid.getSelected();
			}
			return row;
		}
		
		//增删改查
		function add(){
			//var row = grid.getSelected();
			var url = CommonUtil.baseWebPath() + "/cfetsfx/rmswapEdit.jsp?action=add&prdNo="+prdNo+"&prdName="+prdName+"&dealType="+dealType+"&fPrdCode="+fPrdCode;
			var tab = { id: "MiniRmbSwapEdit", name: "MiniRmbSwapEdit", title: "外汇掉期补录", 
			url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
			var paramData = {selectData:""};
			CommonUtil.openNewMenuTab(tab,paramData);
		}
		
		function query() {
            search(grid.pageSize, 0);
        }
		
		function clear(){
			var MiniSettleForeigDetail=new mini.Form("MiniSettleForeigDetail");
			MiniSettleForeigDetail.clear();
			mini.get("spread").setValue(0);
			mini.get("nearSpread").setValue(0);
            form.clear();
            search(10,0);
		}
		
		function edit(){
			var row = grid.getSelected();
			var selections = grid.getSelecteds();
			if(selections.length>1){
				mini.alert("系统不支持多条数据进行编辑","消息提示");
				return;
			}
			if(row){
				var url = CommonUtil.baseWebPath() + "/cfetsfx/rmswapEdit.jsp?action=edit&ticketid="+row.ticketId+"&prdNo="+prdNo+"&prdName="+prdName+"&dealType="+dealType+"&fPrdCode="+fPrdCode;
				var tab = { id: "MiniRmbSwapEdit", name: "MiniRmbSwapEdit", title: "外汇掉期修改", url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
				var paramData = {selectData:row};
				CommonUtil.openNewMenuTab(tab,paramData);
			} else {
				mini.alert("请选中一条记录！","消息提示");
			}
		}

		function onRowDblClick(e) {
			var grid = mini.get("datagrid");
			var row = grid.getSelected();
			if(row){
				var url = CommonUtil.baseWebPath() + "/cfetsfx/rmswapEdit.jsp?action=detail&ticketid="+row.ticketId+"&prdNo="+prdNo+"&dealNo="+row.dealNo+"&fPrdCode="+fPrdCode;
				var tab = { id: "MiniRmbSwapEdit", name: row.ticketId, title: "外汇掉期详情", url: url ,showCloseButton:true};
				//top["win"].openNewTab(tab, row);
				var paramData = {selectData:row};
				CommonUtil.openNewMenuTab(tab,paramData);
			} else {
				mini.alert("请选中一条记录！","消息提示");
			}
		}
		
		//删除
		function del() {
			var grid = mini.get("datagrid");
			var row = grid.getSelected();
			var selections = grid.getSelecteds();
			if(selections.length>1){
				mini.alert("系统不支持多条数据进行删除","消息提示");
				return;
			}
			if (row) {
				mini.confirm("您确认要删除选中记录?","系统警告",function(value){
					if(value=="ok"){
					CommonUtil.ajax({
						url: "/IfsForeignController/deletermswap",
						
						data: {ticketid: row.ticketId},
						callback: function (data) {
							if (data.code == 'error.common.0000') {
								mini.alert("删除成功");
								grid.reload();
								//search(grid.pageSize,grid.pageIndex);
							} else {
								mini.alert("删除失败");
							}
						}
					});
					}
				});
			}
			else {
				mini.alert("请选中一条记录！", "消息提示");
			}
		}
		
		$(document).ready(function() {
			//控制按钮显示
			$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
				initButton();
				search(10, 0);
			});
		});

		//提交审批
		function commit(){
			mini.get("approve_mine_commit_btn").setEnabled(false);
			var messageid = mini.loading("系统正在处理...", "请稍后");
			try {
				verify(grid.getSelecteds());
			} catch (error) {
				
			}
			mini.hideMessageBox(messageid);	
			mini.get("approve_mine_commit_btn").setEnabled(true);
		}
		
		//提交正式审批、待审批
		function verify(selections){
			var grid = mini.get("datagrid");
			var row = grid.getSelected();
			if(selections.length == 0){
				mini.alert("请选中一条记录！","消息提示");
				return false;
			}else if(selections.length > 1){
				mini.alert("该功能不支持多笔提交，如需多笔提交请点击[批量提交]按钮","系统提示");
				return false;
			}
			if(CommonUtil.isNull(selections[0]["dealSource"])||CommonUtil.isNull(selections[0]["port"])||CommonUtil.isNull(selections[0]["cost"])||
	    			 CommonUtil.isNull(selections[0]["product"])||CommonUtil.isNull(selections[0]["prodType"])){
	    		 mini.alert("请先进行opics要素检查更新!","提示");
	    		 return;
	    	}
			if(selections[0]["approveStatus"] != "3" ){
				var searchByIdUrl = "/IfsCfetsrmbDpController/searchCfetsrmbDpAndFlowIdById";
				   Approve.goToApproveJsp(selections[0].taskId,"432","/cfetsfx/rmswapEdit.jsp", target);
				var target = Approve.approvePage;
				var openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid="+row.ticketId+"&prdNo=rmbswap"+"&fPrdCode="+fPrdCode;
				var id="rmsSwapApprove";
				var title="外汇掉期审批";
				var tab = {"id": id,name:id,url:openJspUrl,title:title,parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
				var paramData = {selectData:selections[0]};
				CommonUtil.openNewMenuTab(tab,paramData);
				// top["win"].showTab(tab);
			}else{
				Approve.approveCommit(Approve.FlowType.VerifyApproveFlow,selections[0]["ticketId"],Approve.OrderStatus.New,"IfsFlowService",prdNo,function(){
					search(grid.pageSize,grid.pageIndex);
				});
			}
		};

		//审批日志
		function searchlog(){
			appLog(grid.getSelecteds());
		}

		//审批日志查看
		function appLog(selections){
			var flow_type = Approve.FlowType.VerifyApproveFlow;
			if(selections.length <= 0){
				mini.alert("请选择要操作的数据","系统提示");
				return;
			}
			if(selections.length > 1){
				mini.alert("系统不支持多笔操作","系统提示");
				return;
			}
			if(selections[0].tradeSource == "3"){
				mini.alert("初始化导入的业务没有审批日志","系统提示");
				return false;
			}
			Approve.approveLog(flow_type,selections[0].ticketId);
		};



		//审批
		function approve(){
			mini.get("approve_commit_btn").setEnabled(false);
			var messageid = mini.loading("系统正在处理...", "请稍后");
			try {
				verify(grid.getSelecteds());
			} catch (error) {
				
			}
			mini.hideMessageBox(messageid);	
			mini.get("approve_commit_btn").setEnabled(true);
		}
		
		//交易日期
		function onDrawDateStart(e) {
	        var startDate = e.date;
	        var endDate= mini.get("endDate").getValue();
	        if(CommonUtil.isNull(endDate)){
	        	return;
	        }
	        if (endDate.getTime() < startDate.getTime()) {
	            e.allowSelect = false;
	        }
	    }
		
		function onDrawDateEnd(e) {
	        var endDate = e.date;
	        var startDate = mini.get("startDate").getValue();
	        if(CommonUtil.isNull(startDate)){
	        	return;
	        }
	        if (endDate.getTime() < startDate.getTime()) {
	            e.allowSelect = false;
	        }
	    }
		
		function changeAcct(buyCurreny,sellCurreny,buyCurreny1,sellCurreny1){
			   /* var buyCurreny=mini.get("buyCurreny").getValue();
			   var sellCurreny=mini.get("sellCurreny").getValue(); */
			   $("#bankBic1").show();
			   $("#intermediaryBankName1").show();
			   $("#intermediaryBankBicCode1").show();
			   $("#bankBic2").show();
			   $("#intermediaryBankName2").show();
			   $("#intermediaryBankBicCode2").show();
			   $("#bankAccount1").show();
			   $("#bankAccount2").show();
			   $("#intermediaryBankAcctNo1").show();
			   $("#intermediaryBankAcctNo2").show();
			   if(buyCurreny=="CNY"){
				   mini.get("bankName1").setLabel("资金账户户名：");
				   mini.get("dueBank1").setLabel("资金开户行：");
				   mini.get("dueBankName1").setLabel("支付系统行号：");
				   mini.get("dueBankAccount1").setLabel("资金账号：");
				   $("#bankBic1").hide();
				   $("#intermediaryBankName1").hide();
				   $("#intermediaryBankBicCode1").hide();
				   $("#bankAccount1").hide();
				   $("#intermediaryBankAcctNo1").hide();
				   
			   }else if(sellCurreny=="CNY"){
				   mini.get("bankName2").setLabel("资金账户户名：");
				   mini.get("dueBank2").setLabel("资金开户行：");
				   mini.get("dueBankName2").setLabel("支付系统行号：");
				   mini.get("dueBankAccount2").setLabel("资金账号：");
				   $("#bankBic2").hide();
				   $("#intermediaryBankName2").hide();
				   $("#intermediaryBankBicCode2").hide();
				   $("#bankAccount2").hide();
				   $("#intermediaryBankAcctNo2").hide();
			   }
			   
			   /* var buyCurreny1=mini.get("buyCurreny1").getValue();
			   var sellCurreny1=mini.get("sellCurreny1").getValue(); */
			   $("#bankBic3").show();
			   $("#intermediaryBankName3").show();
			   $("#intermediaryBankBicCode3").show();
			   $("#bankBic4").show();
			   $("#intermediaryBankName4").show();
			   $("#intermediaryBankBicCode4").show();
			   $("#bankAccount3").show();
			   $("#bankAccount4").show();
			   $("#intermediaryBankAcctNo3").show();
			   $("#intermediaryBankAcctNo4").show();
			   if(buyCurreny1=="CNY"){
				   mini.get("bankName3").setLabel("资金账户户名：");
				   mini.get("dueBank3").setLabel("资金开户行：");
				   mini.get("dueBankName3").setLabel("支付系统行号：");
				   mini.get("dueBankAccount3").setLabel("资金账号：");
				   $("#bankBic3").hide();
				   $("#intermediaryBankName3").hide();
				   $("#intermediaryBankBicCode3").hide();
				   $("#bankAccount3").hide();
				   $("#intermediaryBankAcctNo3").hide();
				   
			   }else if(sellCurreny1=="CNY"){
				   mini.get("bankName4").setLabel("资金账户户名：");
				   mini.get("dueBank4").setLabel("资金开户行：");
				   mini.get("dueBankName4").setLabel("支付系统行号：");
				   mini.get("dueBankAccount4").setLabel("资金账号：");
				   $("#bankBic4").hide();
				   $("#intermediaryBankName4").hide();
				   $("#intermediaryBankBicCode4").hide();
				   $("#bankAccount4").hide();
				   $("#intermediaryBankAcctNo4").hide();
			   }
		   }
		//打印
		function print(){
			var selections = grid.getSelecteds();
			if(selections == null || selections.length == 0){
				mini.alert('请选择一条要打印的数据！','系统提示');
				return false;
			}else if(selections.length>1){
				mini.alert('暂不支持多条数据同时打印，请选择一条要打印的数据！','系统提示');
				return false;
			}
			var canPrint = selections[0].ticketId;

			if(!CommonUtil.isNull(canPrint)){
				var actionStr = CommonUtil.pPath + "/sl/IfsPrintUtilController/exportload/print/"+PrintNo.SWAP+"/" + canPrint;
				$('<form target="_blank" action=' + actionStr + ' method="post"></form>').appendTo('body').submit().remove();
			};
		}

		//导出报表
		function exportExcel(){
			exportExcelManin("jywhdq.xls","jywhdq");
		}
	</script>
</body>
</html>