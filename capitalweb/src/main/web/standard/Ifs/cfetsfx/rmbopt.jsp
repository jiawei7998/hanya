<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
<title>外汇期权</title>
<script type="text/javascript" >
	/**prdCode用于页面投资组合的选择   该全局变量必须为prdCode*/
	var prdCode="OTC";
</script>
</head>
<body style="width: 100%; height: 100%; background: white">
	<fieldset class="mini-fieldset">
		<legend>查询</legend>
		<div id="search_form" style="width: 100%;">
			<nobr>
			<input id="startDate" name="startDate" class="mini-datepicker" labelField="true" label="交易日期：" value="<%=__bizDate%>"
						ondrawdate="onDrawDateStart" labelStyle="text-align:right;" emptyText="起始日期" format="yyyy-MM-dd"/>
			<span>~</span>
			<input id="endDate" name="endDate" class="mini-datepicker" value="<%=__bizDate%>"
						ondrawdate="onDrawDateEnd" emptyText="结束日期" format="yyyy-MM-dd"/>
			</nobr>
			<input id="contractId" name="contractId" class="mini-textbox" labelField="true" label="成交单编号："  width="280px" labelStyle="text-align:right;" emptyText="请输入成交单编号"/> 
			<input id="approveStatus" name="approveStatus" class="mini-combobox"  data="CommonUtil.serverData.dictionary.ApproveSta" width="280px" emptyText="请选择审批状态" labelField="true"  label="审批状态：" labelStyle="text-align:right;"/>
			<input id="currencyPair" name="currencyPair" class="mini-combobox"  data="CommonUtil.serverData.dictionary.CurrencyPair" width="280px" emptyText="请选择货币对" labelField="true"  label="货币对：" labelStyle="text-align:right;"/>
			
			<span style="float: right; margin-right: 100px"> 
				<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
			</span>
		</div>
	</fieldset>
	<%@ include file="../batchReback.jsp"%>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
	 
	<div id="MiniSettleForeignSpot" class="mini-fit" style="margin-top: 2px;">
		<div id="datagrid" class="mini-datagrid borderAll" style="width: 100%; height: 50%;" allowAlternating="true"  multiSelect="true"
			allowResize="true" onrowdblclick="onRowDblClick" border="true" sortMode="client">
			<div property="columns">
			<div type="checkcolumn"></div>
				<div type="indexcolumn" headerAlign="center" width="40">序号</div>
				<div field="prdNo" width="100" allowSort="false" headerAlign="center" align="center" visible="false"></div>
				<div field="forDate" width="150" allowSort="false" headerAlign="center" align="center">交易日期</div>
				<div field="forTime" width="100" allowSort="false" headerAlign="center" align="center">交易时间</div>
				<div field="dealNo" width="100px" align="center"  headerAlign="center" >opics交易号</div>
				<div field="dealSource" width="100px" align="center"  headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'TradeSource'}">交易来源</div>
				<div field="contractId" width="200" allowSort="false" headerAlign="center" align="">成交单编号</div>
				<div field="taskName" width="180" allowSort="false" headerAlign="center" align="">审批状态</div>
<!-- 				<div field="approveStatus" width="120" align="center" headerAlign="center"  renderer="CommonUtil.dictRenderer" data-options="{dict:'ApproveStatus'}">审批状态</div> -->
<!-- 				<div field="dealTransType" width="100" allowSort="true" headerAlign="center"  align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'dealTransType'}">交易状态</div> -->
				<div field="instId" width="200" allowSort="false" headerAlign="center" align="center">本方机构</div>
				<div field="counterpartyInstId" width="200" allowSort="false" headerAlign="center" align="center">对方机构</div>
				<div field="direction" width="100" allowSort="true" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'trading'}">交易方向</div>
				<div field="buyNotional" width="150" allowSort="false" headerAlign="center" align="right" numberFormat="n4">名义本金（买）</div>
				<div field="sellNotional" width="150" allowSort="true" headerAlign="center" align="right" numberFormat="n4">名义本金（卖）</div>
				<div field="currencyPair" width="100" allowSort="false" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'CurrencyPair'}">货币对</div>
				<div field="strikePrice" width="180" allowSort="false" headerAlign="center" align="right" numberFormat="n4">执行价</div>
				<div field="expiryDate" width="150" allowSort="false" headerAlign="center" align="center">行权日</div>
				<div field="deliveryDate" width="150" allowSort="true" headerAlign="center" align="center">交割日</div>
				<div field="deliveryType" width="100" allowSort="false" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'DeliveryType'}">交割方式</div>
				<div field="exerciseType" width="100" allowSort="true" headerAlign="center" align="center">执行方式</div>
				<div field="sponsor" width="100" allowSort="false" headerAlign="center" align="center">审批发起人</div>
				<div field="sponInst" width="100" allowSort="false" headerAlign="center" align="center" >审批发起机构</div>
				<div field="aDate" width="150" allowSort="false" headerAlign="center" align="center">审批发起时间</div>
				<div field="ticketId" width="200" allowSort="false" headerAlign="center" align="center">审批单号</div>
			</div>
		</div>
	<fieldset style="width: 100%;border:solid 1px #aaa;margin-top:8px;position:relative;">
		<legend>外汇期权详情</legend>
			<div id="MiniSettleForeigDetail" style="padding:5px;" class="mini-fit area" tyle="width: 100%; height: 50%;" allowAlternating="true" allowResize="true" border="true" sortMode="client">
			<input class="mini-hidden" name="id"/>
				<div class="mini-panel" title="成交双方信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
					<div class="leftarea" >
						<input id="instId" name="instId" class="mini-textbox" labelField="true"  label="本方机构：" maxLength="25"	 style="width:100%;"  labelStyle="text-align:left;width:130px;" />
						<input id="dealer" name="dealer" class="mini-textbox" labelField="true"  label="本方交易员：" maxLength="20"	 labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						<input id="forDate" name="forDate" class="mini-datepicker" labelField="true"  label="交易日期："  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
					</div>
					<div class="rightarea">
						<input id="counterpartyInstId" name="counterpartyInstId" class="mini-textbox" labelField="true"  label="对方机构：" onbuttonclick="onButtonEdit" maxLength="25"	 style="width:100%;"  labelStyle="text-align:left;width:130px;" />
						<input id="counterpartyDealer" name="counterpartyDealer" class="mini-textbox" labelField="true"  label="对方交易员：" maxLength="20"	 labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						<input id="forTime" name="forTime" class="mini-timespinner" labelField="true"  label="交易时间："  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
					</div>
				<%@ include file="../../Common/opicsDetail.jsp"%>
				<div class="mini-panel" title="交易主体信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
					<div class="leftarea">	
						<input style="width:100%" id="currencyPair" name="currencyPair" class="mini-combobox" labelField="true"  label="货币对："   data="CommonUtil.serverData.dictionary.CurrencyPair" labelStyle="text-align:left;width:130px;" />
						<input style="width:100%" id="tradingType" name="tradingType" class="mini-textbox" labelField="true"  label="交易类型：" maxLength="20"  labelStyle="text-align:left;width:130px;"   />
						<input style="width:100%" id="buyNotional" name="buyNotional" class="mini-combobox" labelField="true"  label="名义本金方向：" data="CommonUtil.serverData.dictionary.trading"  labelStyle="text-align:left;width:130px;"   />
						<input style="width:100%" id="sellNotional" name="sellNotional" class="mini-combobox" labelField="true"  label="名义本金方向：" data="CommonUtil.serverData.dictionary.trading"   labelStyle="text-align:left;width:130px;"   />
						<input style="width:100%" id="tenor" name="tenor" class="mini-combobox" labelField="true"  label="期限："  data="CommonUtil.serverData.dictionary.Term"  labelStyle="text-align:left;width:130px;" />
						<input style="width:100%" id="premiumDate" name="premiumDate" class="mini-datepicker" labelField="true"  label="期权费交付日："  ondrawdate="compareDate1"  labelStyle="text-align:left;width:130px;"   />
						<input style="width:100%" id="premiumRate" name="premiumRate" class="mini-spinner" labelField="true"  label="期权费率：" changeOnMousewheel='false' maxValue="1" format="n4"  labelStyle="text-align:left;width:130px;"   />
						<input style="width:100%" id="deliveryDate" name="deliveryDate" class="mini-datepicker" labelField="true"  label="交割日：" ondrawdate="compareDate3" labelStyle="text-align:left;width:130px;"   />
						<input style="width:100%" id="cutOffTime" name="cutOffTime" class="mini-timespinner" labelField="true"  label="行权截止时间："   labelStyle="text-align:left;width:130px;" />
						<input style="width:100%" id="optionStrategy" name="optionStrategy" class="mini-textbox" labelField="true"  label="期权类型：" maxLength="20"  labelStyle="text-align:left;width:130px;"   />
						<input style="width:100%" id="strategyId" name="strategyId" class="mini-textbox" labelField="true"  label="期权组合号：" maxLength="20"  labelStyle="text-align:left;width:130px;"   />
					    <input style="width:100%" id="plmethod" name="plmethod" class="mini-combobox" labelField="true" label="损益计算方法："  labelStyle="text-align:left;width:130px;"  vtype="maxLength:15" data="CommonUtil.serverData.dictionary.acctMethod" requiredErrorText="该输入项为必输项" required="true" />
					</div>
					<div class="rightarea">
						<input style="width:100%" id="direction" name="direction" class="mini-combobox" labelField="true"  label="交易方向：" data="CommonUtil.serverData.dictionary.trading"  labelStyle="text-align:left;width:130px;" />
						<input style="width:100%" id="strikePrice" name="strikePrice" class="mini-spinner" changeOnMousewheel='false'  format="n4" labelField="true"  label="执行价：" maxValue="999999999999999"  labelStyle="text-align:left;width:130px;"   />
						<input style="width:100%" id="buyNotionalAmount" name="buyNotionalAmount" class="mini-spinner" labelField="true"  label="名义本金金额：" changeOnMousewheel='false' maxValue="999999999999999" format="n4"  labelStyle="text-align:left;width:130px;"   />
						<input style="width:100%" id="sellNotionalAmount" name="sellNotionalAmount" class="mini-spinner" labelField="true"  label="名义本金金额：" changeOnMousewheel='false' maxValue="999999999999999" format="n4"  labelStyle="text-align:left;width:130px;"   />
						<input style="width:100%" id="spotReference" name="spotReference" class="mini-textbox" labelField="true"  label="即期参考汇率：" maxLength="20"  labelStyle="text-align:left;width:130px;" />
						<input style="width:100%" id="expiryDate" name="expiryDate" class="mini-datepicker" labelField="true"  label="行权日："  ondrawdate="compareDate2" labelStyle="text-align:left;width:130px;"   />
						<input style="width:100%" id="premiumType" name="premiumType" class="mini-textbox" labelField="true"  label="期权类型：" maxLength="20"  labelStyle="text-align:left;width:130px;"   /> 
						<input style="width:100%" id="premiumAmount" name="premiumAmount" class="mini-spinner" labelField="true"  label="期权费金额：" changeOnMousewheel='false' maxValue="999999999999999" format="n4"  labelStyle="text-align:left;width:130px;" />
						<input style="width:100%" id="deliveryType" name="deliveryType" class="mini-combobox" labelField="true"  label="交割方式：" maxLength="20" data="CommonUtil.serverData.dictionary.DeliveryType"  labelStyle="text-align:left;width:130px;"   />
						<input style="width:100%" id="exerciseType" name="exerciseType" class="mini-textbox" labelField="true"  label="执行方式：" maxLength="20"  labelStyle="text-align:left;width:130px;"   />
					</div>
				</div>
			</div>
			<%@ include file="./accountInfor.jsp"%>
			
			</div>
	</fieldset>
	</div>
	<script>
		mini.parse();
		var form=new mini.Form("search_form");

		var url = window.location.search;
		var prdNo = CommonUtil.getParam(url, "prdNo");
		var prdName = CommonUtil.getParam(url, "prdName");
		var grid = mini.get("datagrid");
		
		/**************************************点击下面显示详情开始******************************/
		var from = new mini.Form("MiniSettleForeigDetail");
		from.setEnabled(false);
		var grid = mini.get("datagrid");
		//grid.load();
		 //绑定表单
        var db = new mini.DataBinding();
        db.bindForm("MiniSettleForeigDetail", grid);
        /**************************************点击下面显示详情结束******************************/	
		grid.on("beforeload", function(e) {
			e.cancel = true;
			var pageIndex = e.data.pageIndex;
			var pageSize = e.data.pageSize;
			search(pageSize, pageIndex);
		});
        
		grid.on("select",function(e){
			grid.on("select",function(e){
				var rows=grid.getSelecteds();
				for(var i=0;i<rows.length;i++){
					if(rows[i].approveStatus != "3"){
						mini.get("opics_check_btn").setEnabled(false);
						break;
					}
				}
			});
			var row=e.record;
			mini.get("approve_mine_commit_btn").setEnabled(false);
			mini.get("approve_commit_btn").setEnabled(false);
			mini.get("edit_btn").setEnabled(false);
			mini.get("delete_btn").setEnabled(false);
			mini.get("approve_log").setEnabled(true);
			mini.get("recall_btn").setEnabled(true);
			mini.get("batch_commit_btn").setEnabled(true);
			if(row.approveStatus == "3"){//新建
				mini.get("edit_btn").setEnabled(true);
				mini.get("delete_btn").setEnabled(true);
				mini.get("approve_mine_commit_btn").setEnabled(true);
				mini.get("approve_commit_btn").setEnabled(false);
				mini.get("reback_btn").setEnabled(true);
				mini.get("recall_btn").setEnabled(false);
				mini.get("opics_check_btn").setEnabled(true);
			}
			if( row.approveStatus == "6" || row.approveStatus == "7" || row.approveStatus == "8" || row.approveStatus == "17"){//审批通过：6    审批拒绝:5
				mini.get("edit_btn").setEnabled(false);
				mini.get("delete_btn").setEnabled(false);
				mini.get("approve_mine_commit_btn").setEnabled(false);
				mini.get("approve_commit_btn").setEnabled(true);
				mini.get("batch_approve_btn").setEnabled(false);
				mini.get("batch_commit_btn").setEnabled(false);
				mini.get("opics_check_btn").setEnabled(false);
				mini.get("reback_btn").setEnabled(false);
				mini.get("recall_btn").setEnabled(false);
			}
			if(row.approveStatus == "5"){//审批中:5
				mini.get("edit_btn").setEnabled(false);
				mini.get("delete_btn").setEnabled(false);
				mini.get("approve_mine_commit_btn").setEnabled(false);
				mini.get("approve_commit_btn").setEnabled(true);
				mini.get("batch_commit_btn").setEnabled(false);
				mini.get("reback_btn").setEnabled(false);
				mini.get("recall_btn").setEnabled(true);
				mini.get("opics_check_btn").setEnabled(false);
			}
			
		});
		
		function getData(action) {
			var row = null;
			if (action != "add") {
				row = grid.getSelected();
			}
			return row;
		}
		
		function search(pageSize,pageIndex){
			form.validate();
			if(form.isValid()==false){
				mini.alert("信息填写有误，请重新填写","系统提示");
				return;
			}

			var data=form.getData(true);
			data['pageNumber']=pageIndex+1;
			data['pageSize']=pageSize;
			data['branchId']=branchId;
			var url=null;

			var approveType = mini.get("approveType").getValue();
			if(approveType == "mine"){
				url = "/IfsForeignController/searchPageOptionMine";
			}else if(approveType == "approve"){
				url = "/IfsForeignController/searchPageOptionUnfinished";
			}else{
				url = "/IfsForeignController/searchPageOptionFinished";
			}
			var params = mini.encode(data);
			CommonUtil.ajax({
				url:url,
				data:params,
				callback : function(data) {
					grid.setTotalCount(data.obj.total);
					grid.setPageIndex(pageIndex);
			        grid.setPageSize(pageSize);
					grid.setData(data.obj.rows);
				}
			});
		}
		//增删改查
		function add(){
			var url = CommonUtil.baseWebPath() + "/cfetsfx/rmboptEdit.jsp?action=add&prdNo="+prdNo;
			var tab = { id: "MiniRmBoptAdd", name: "MiniRmBoptAdd", title: "外汇期权补录", url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
			var paramData = {selectData:""};
			CommonUtil.openNewMenuTab(tab,paramData);
		}
		
		function query() {
            search(grid.pageSize, 0);
        }
		
		function clear(){
            form.clear();
            search(10,0);
		}
		
		function edit(){
			var row = grid.getSelected();
			if(row){
				var url = CommonUtil.baseWebPath() + "/cfetsfx/rmboptEdit.jsp?action=edit&ticketid="+row.ticketId+"&prdNo="+prdNo;
				var tab = { id: "MiniRmBoptEdit", name: "MiniRmBoptEdit", title: "外汇期权修改", url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
				var paramData = {selectData:row};
				CommonUtil.openNewMenuTab(tab,paramData);
			} else {
				mini.alert("请选中一条记录！","消息提示");
			}
		}

		function onRowDblClick(e) {
			var grid = mini.get("datagrid");
			var row = grid.getSelected();
			if(row){
					var url = CommonUtil.baseWebPath() + "/cfetsfx/rmboptEdit.jsp?action=detail&ticketid="+row.ticketId+"&prdNo="+prdNo+"&dealNo="+row.dealNo;
					var tab = { id: "MiniRmBoptDetail", name: "MiniRmBoptDetail", title: "外汇期权详情", url: url ,showCloseButton:true};
					var paramData = {selectData:row};
					CommonUtil.openNewMenuTab(tab,paramData);
			} else {
				mini.alert("请选中一条记录！","消息提示");
			}
		}
		//删除
		function del() {
				var grid = mini.get("datagrid");
				var row = grid.getSelected();
				if (row) {
					mini.confirm("您确认要删除选中记录?","系统警告",function(value){
						if(value=="ok"){
						CommonUtil.ajax({
							url: "/IfsForeignController/deleteOption",
							
							data: {ticketid: row.ticketId},
							callback: function (data) {
								if (data.code == 'error.common.0000') {
									mini.alert("删除成功");
									grid.reload();
									//search(grid.pageSize,grid.pageIndex);
								} else {
									mini.alert("删除失败");
								}
							}
						});
						}
					});
				}
				else {
					mini.alert("请选中一条记录！", "消息提示");
				}

			}
		/**************************审批相关****************************/
		//审批日志查看
		function appLog(selections){
			var flow_type = Approve.FlowType.VerifyApproveFlow;
			if(selections.length <= 0){
				mini.alert("请选择要操作的数据","系统提示");
				return;
			}
			if(selections.length > 1){
				mini.alert("系统不支持多笔操作","系统提示");
				return;
			}
			if(selections[0].tradeSource == "3"){
				mini.alert("初始化导入的业务没有审批日志","系统提示");
				return false;
			}
			Approve.approveLog(flow_type,selections[0].ticketId);
		};
		//提交正式审批、待审批
		function verify(selections){
			var grid = mini.get("datagrid");
			var row = grid.getSelected();
			if(selections.length == 0){
				mini.alert("请选中一条记录！","消息提示");
				return false;
			}else if(selections.length > 1){
				mini.alert("该功能不支持多笔提交，如需多笔提交请点击[批量提交]按钮","系统提示");
				return false;
			}
			if(selections[0]["approveStatus"] != "3" ){
				var searchByIdUrl = "/IfsCfetsrmbDpController/searchCfetsrmbDpAndFlowIdById";
				   Approve.goToApproveJsp(selections[0].taskId,"433","/cfetsfx/rmboptEdit.jsp", target);
				var target = Approve.approvePage;
				var openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid="+row.ticketId+"&prdNo=rmbopt";
				var id="AdvanceRmBoptApprove";
				var title="外汇期权审批";
				var tab = {"id": id,name:id,url:openJspUrl,title:title,parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
				var paramData = {selectData:selections[0]};
				CommonUtil.openNewMenuTab(tab,paramData);
				// top["win"].showTab(tab);
			}else{
				Approve.approveCommit(Approve.FlowType.VerifyApproveFlow,selections[0]["ticketId"],Approve.OrderStatus.New,"IfsOptionService",prdNo,function(){
					search(grid.pageSize,grid.pageIndex);
				});
			}
		};
		//审批
		function approve(){
			mini.get("approve_commit_btn").setEnabled(false);
			var messageid = mini.loading("系统正在处理...", "请稍后");
			try {
				verify(grid.getSelecteds());
			} catch (error) {
				
			}
			mini.hideMessageBox(messageid);	
			mini.get("approve_commit_btn").setEnabled(true);
		}
		//提交审批
		function commit(){
			mini.get("approve_mine_commit_btn").setEnabled(false);
			var messageid = mini.loading("系统正在处理...", "请稍后");
			try {
				verify(grid.getSelecteds());
			} catch (error) {
				
			}
			mini.hideMessageBox(messageid);	
			mini.get("approve_mine_commit_btn").setEnabled(true);
		}
		
		//审批日志
		function searchlog(){
			appLog(grid.getSelecteds());
		}
		
		//打印
	    function print(){
			var selections = grid.getSelecteds();
			if(selections == null || selections.length == 0){
				mini.alert('请选择一条要打印的数据！','系统提示');
				return false;
			}else if(selections.length>1){
				mini.alert('暂不支持多条数据同时打印，请选择一条要打印的数据！','系统提示');
				return false;
			}
			var canPrint = selections[0].ticketId;
			var nettingStatus = selections[0].nettingStatus; // 净额清算状态
			if(!CommonUtil.isNull(canPrint)){
				if(nettingStatus == "0"){ // 否
					var actionStr = CommonUtil.pPath + "/sl/IfsPrintUtilController/exportload/print/"+PrintNo.rmbopt+"/" + canPrint;
					$('<form target="_blank" action=' + actionStr + ' method="post"></form>').appendTo('body').submit().remove();
				} else { // 1-是
					var actionStr = CommonUtil.pPath + "/sl/IfsPrintUtilController/exportload/print/"+PrintNo.isrmbopt+"/" + canPrint;
					$('<form target="_blank" action=' + actionStr + ' method="post"></form>').appendTo('body').submit().remove();		
				}
			};
		}
		
	  //交易日期
		function onDrawDateStart(e) {
	        var startDate = e.date;
	        var endDate= mini.get("endDate").getValue();
	        if(CommonUtil.isNull(endDate)){
	        	return;
	        }
	        if (endDate.getTime() < startDate.getTime()) {
	            e.allowSelect = false;
	        }
	    }
	  
		function onDrawDateEnd(e) {
	        var endDate = e.date;
	        var startDate = mini.get("startDate").getValue();
	        if(CommonUtil.isNull(startDate)){
	        	return;
	        }
	        if (endDate.getTime() < startDate.getTime()) {
	            e.allowSelect = false;
	        }
	    }
		
		$(document).ready(function() {
			//控制按钮显示
			$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
				initButton();
				search(10, 0);
			});
		});
	</script>
</body>
</html>