<%@ page language="java" pageEncoding="UTF-8"%>
<div id="isNoNetting" class="mini-panel" title="清算账户信息" style="width:100%"  allowResize="true" collapseOnTitleClick="false">	
	<div class="leftarea">
		<fieldset >
			<legend>本方账户</legend>
			<input id="buyCurreny" name="buyCurreny" class="mini-combobox" labelField="true"  label="清算货币："  style="width:100%;"  labelStyle="text-align:left;width:120px;" data="CommonUtil.serverData.dictionary.Currency" enabled="false"/>
			<input id="bankName1" name="bankName1" class="mini-textbox" labelField="true"  label="开户行名称："  labelStyle="text-align:left;width:120px;" style="width:100%;" requiredErrorText="该输入项为必输项"    vtype="maxLength:60"  />
			<input id="bankBic1" name="bankBic1" class="mini-textbox" labelField="true"  label="开户行BIC CODE："  labelStyle="text-align:left;width:120px;" style="width:100%;"  requiredErrorText="该输入项为必输项"     onvalidation="onEnglishAndNumberValidation"  vtype="maxLength:12"/>
			<input id="bankAccount1" name="bankAccount1" class="mini-textbox input-text-strong" labelField="true"  label="开户行账号："  labelStyle="text-align:left;width:120px;" style="width:100%;" requiredErrorText="该输入项为必输项"   onvalidation="onEnglishAndNumberValidation" vtype="maxLength:32" />
			<input id="dueBank1" name="dueBank1" class="mini-textbox" labelField="true"  label="收款行名称："  labelStyle="text-align:left;width:120px;" style="width:100%;"  requiredErrorText="该输入项为必输项"  vtype="maxLength:60"/>
			<input id="dueBankName1" name="dueBankName1" class="mini-textbox" labelField="true"  label="收款行BIC CODE：" labelStyle="text-align:left;width:120px;" style="width:100%;"  requiredErrorText="该输入项为必输项"    onvalidation="onEnglishAndNumberValidation" vtype="maxLength:12"/>
			<input id="capitalAccount1" name="capitalAccount1" class="mini-textbox input-text-strong" labelField="true"  label="收款行账号："  labelStyle="text-align:left;width:120px;" style="width:100%;" requiredErrorText="该输入项为必输项"   onvalidation="onEnglishAndNumberValidation" vtype="maxLength:32" />
			<input id="intermediaryBankName1" name="intermediaryBankName1" class="mini-textbox" labelField="true"  label="中间行名称："  labelStyle="text-align:left;width:120px;" style="width:100%;"  requiredErrorText="该输入项为必输项"  vtype="maxLength:60"/>
			<input id="intermediaryBankBicCode1" name="intermediaryBankBicCode1" class="mini-textbox" labelField="true"  label="中间行BIC CODE："  labelStyle="text-align:left;width:120px;" style="width:100%;"  requiredErrorText="该输入项为必输项" onvalidation="onEnglishAndNumberValidation"  vtype="maxLength:45"/>
			<input id="intermediaryBankAcctNo1" name="intermediaryBankAcctNo1" class="mini-textbox" labelField="true"  label="中间行行号："  labelStyle="text-align:left;width:120px;" style="width:100%;" requiredErrorText="该输入项为必输项"   onvalidation="onEnglishAndNumberValidation" vtype="maxLength:32" />
		</fieldset>
	</div>
	<div class="rightarea">
		<fieldset>
			<legend>对手方账户</legend>
			<input id="sellCurreny" name="sellCurreny" class="mini-combobox" labelField="true"  label="清算货币："  style="width:100%;"  labelStyle="text-align:left;width:120px;" requiredErrorText="该输入项为必输项"  data="CommonUtil.serverData.dictionary.Currency" enabled="false"/>
			<input id="bankName2" name="bankName2" class="mini-textbox" labelField="true"  label="开户行名称："  labelStyle="text-align:left;width:120px;" style="width:100%;"  requiredErrorText="该输入项为必输项"  vtype="maxLength:60" />
			<input id="bankBic2" name="bankBic2" class="mini-textbox" labelField="true"  label="开户行BIC CODE："  labelStyle="text-align:left;width:120px;" style="width:100%;"  requiredErrorText="该输入项为必输项"   onvalidation="onEnglishAndNumberValidation"  vtype="maxLength:12" />
			<input id="bankAccount2" name="bankAccount2" class="mini-textbox input-text-strong" labelField="true"  label="开户行账号："  labelStyle="text-align:left;width:120px;" style="width:100%;" requiredErrorText="该输入项为必输项"   onvalidation="onEnglishAndNumberValidation" vtype="maxLength:32" />
			<input id="dueBank2" name="dueBank2" class="mini-textbox" labelField="true"  label="收款行名称："  labelStyle="text-align:left;width:120px;" style="width:100%;"  requiredErrorText="该输入项为必输项"  vtype="maxLength:60"/>
			<input id="dueBankName2" name="dueBankName2"  class="mini-textbox" labelField="true"  label="收款行BIC CODE："  labelStyle="text-align:left;width:120px;" style="width:100%;" requiredErrorText="该输入项为必输项"   onvalidation="onEnglishAndNumberValidation"  vtype="maxLength:12"/>
			<input id="capitalAccount2" name="capitalAccount2" class="mini-textbox input-text-strong" labelField="true"  label="收款行账号："  labelStyle="text-align:left;width:120px;" style="width:100%;" requiredErrorText="该输入项为必输项"  onvalidation="onEnglishAndNumberValidation"  vtype="maxLength:32" />
			<input id="intermediaryBankName2" name="intermediaryBankName2" class="mini-textbox" labelField="true"  label="中间行名称："  labelStyle="text-align:left;width:120px;" style="width:100%;"  requiredErrorText="该输入项为必输项"  vtype="maxLength:60"/>
			<input id="intermediaryBankBicCode2" name="intermediaryBankBicCode2" class="mini-textbox" labelField="true"  label="中间行BIC CODE："  labelStyle="text-align:left;width:120px;" style="width:100%;"  requiredErrorText="该输入项为必输项" onvalidation="onEnglishAndNumberValidation"  vtype="maxLength:45"/>
			<input id="intermediaryBankAcctNo2" name="intermediaryBankAcctNo2" class="mini-textbox" labelField="true"  label="中间行行号："  labelStyle="text-align:left;width:120px;" style="width:100%;" requiredErrorText="该输入项为必输项"   onvalidation="onEnglishAndNumberValidation" vtype="maxLength:32" />
		</fieldset>
	</div>	
</div>