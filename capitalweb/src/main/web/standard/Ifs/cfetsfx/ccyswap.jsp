<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
<title>货币掉期</title>
<script type="text/javascript" >
	/**prdCode用于页面投资组合的选择   该全局变量必须为prdCode*/
	var prdCode="SWAP";
</script>
</head>
<body style="width: 100%; height: 100%; background: white">
	<fieldset class="mini-fieldset">
		<legend>查询</legend>
		<div id="search_form" style="width: 100%;">
			<input id="contractId" name="contractId" class="mini-textbox" labelField="true" label="成交单编号：" labelStyle="text-align:right;" width="280px" /> 
			<input id="approveStatus" name="approveStatus" class="mini-combobox" data="CommonUtil.serverData.dictionary.ApproveSta" width="280px" emptyText="请选择审批状态" labelField="true"  label="审批状态：" labelStyle="text-align:right;"/>
			<span style="float: right; margin-right: 150px"> 
				<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
			</span>
		</div>
	</fieldset>
	<%@ include file="../batchReback.jsp"%>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
	
	<div id="MiniSettleForeignSpot" class="mini-fit" style="margin-top: 2px;">
		<div id="datagrid" class="mini-datagrid borderAll" style="width: 100%; height: 50%;" allowAlternating="true"  multiSelect="true"
			allowResize="true" onrowdblclick="onRowDblClick" border="true" sortMode="client">
			<div property="columns">
			<div type="checkcolumn"></div>
				<div type="indexcolumn" headerAlign="center" width="40">序号</div>
				<div field="prdNo" width="100" allowSort="false" headerAlign="center" align="center" visible="false"></div>
				<div field="leftNotion" width="200" allowSort="true" headerAlign="center" align="right" numberFormat="#,0.0000">名义本金（左）</div>
				<div field="rightNotion" width="200" allowSort="true" headerAlign="center" align="right" numberFormat="#,0.0000">名义本金（右）</div>	
				<div field="leftDirection" width="150" allowSort="false" headerAlign="center" align="" renderer="CommonUtil.dictRenderer" data-options="{dict:'trading'}">付款方向（左）</div>
				<div field="contractId" width="200" allowSort="false" headerAlign="center" align="">成交单编号</div>
				<div field="approveStatus" width="120" align="center" headerAlign="center"  renderer="CommonUtil.dictRenderer" data-options="{dict:'ApproveStatus'}">审批状态</div>
				<div field="dealTransType" width="100" allowSort="true" headerAlign="center"  align="center"   renderer="CommonUtil.dictRenderer" data-options="{dict:'dealTransType'}">交易状态</div>
				<div field="instId" width="200" allowSort="false" headerAlign="center" align="center">本方机构</div>
				<div field="counterpartyInstId" width="200" allowSort="false" headerAlign="center" align="center">对方机构</div>
				<div field="forDate" width="200" allowSort="false" headerAlign="center" align="center">交易日期</div>
				<div field="tradingModel" width="150" allowSort="false" headerAlign="center" align="center" >交易模式</div>
				<div field="tradingType" width="200" allowSort="true" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'TradeType'}">交易方式</div>
				<div field="startDate" width="200" allowSort="false" headerAlign="center" align="center">开始日期</div>
				<div field="unadjustmaturityDate" width="200" allowSort="true" headerAlign="center" align="center">未调整到日期</div>
				<div field="sponsor" width="150" allowSort="false" headerAlign="center" align="center">审批发起人</div>
				<div field="sponInst" width="150" allowSort="false" headerAlign="center" align="center">审批发起机构</div>
				<div field="aDate" width="200" allowSort="false" headerAlign="center" align="center">审批发起时间</div>
				<div field="ticketId" width="200" allowSort="false" headerAlign="center" align="center">审批单号</div>
			</div>
	</div>
	<fieldset style="width: 100%;border:solid 1px #aaa;margin-top:8px;position:relative;">
		<legend>外汇交易货币掉期详情</legend>
			<div id="MiniSettleForeigDetail" style="padding:5px;" class="mini-fit area" tyle="width: 100%; height: 50%;" allowAlternating="true" allowResize="true" border="true" sortMode="client">
			<input class="mini-hidden" name="id"/>
				<div class="mini-panel" title="成交双方信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
					<div class="leftarea" >
						<input id="instId" name="instId" class="mini-textbox" labelField="true"  label="本方机构："  style="width:100%;"  labelStyle="text-align:left;width:130px;" vtype="maxLength:16" allowInput="false"/>
						<input id="dealer" name="dealer" class="mini-textbox" labelField="true" label="本方交易员："  labelStyle="text-align:left;width:130px;" style="width:100%;"   requiredErrorText="该输入项为必输项" required="true"  vtype="maxLength:6" allowInput="false"/>
						<input id="forDate" name="forDate" class="mini-datepicker" labelField="true" label="交易日期："  labelStyle="text-align:left;width:130px;" style="width:100%;"  requiredErrorText="该输入项为必输项" required="true"  allowInput="false"/>
						<input id="tradingModel" name="tradingModel" class="mini-combobox" labelField="true"   label="交易模式："  value="C-Swap" labelStyle="text-align:left;width:130px;" style="width:100%;"  data="CommonUtil.serverData.dictionary.TradeModel" enabled="false"/>
					</div>
					<div class="rightarea">
						<input id="counterpartyInstId"  name="counterpartyInstId" class="mini-textbox"  labelField="true"  label="对方机构："  style="width:100%;"  labelStyle="text-align:left;width:130px;"   vtype="maxLength:16"/>
						<input id="counterpartyDealer" name="counterpartyDealer" class="mini-textbox" labelField="true" label="对方交易员："  labelStyle="text-align:left;width:130px;" style="width:100%;"   vtype="maxLength:6" allowInput="false"/>
						<input id="forTime" name="forTime" class="mini-timespinner" labelField="true" label="交易时间："  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						<input id="tradingType" name="tradingType" class="mini-combobox" labelField="true"  label="交易方式："  value="C-Swap" labelStyle="text-align:left;width:130px;" style="width:100%;" data="CommonUtil.serverData.dictionary.TradeType" enabled="false"/>
					</div>
				<%@ include file="../../Common/opicsDetail.jsp"%>
				<div class="mini-panel" title="交易主体信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
					<div class="leftarea">	
						<input style="width:100%" id="startDate" name="startDate" class="mini-datepicker" labelField="true" label="开始日期："  labelStyle="text-align:left;width:130px;" requiredErrorText="该输入项为必输项" required="true" />
						<input style="width:100%" id="stub" name="stub" class="mini-textbox" labelField="true" label="残段："  labelStyle="text-align:left;width:130px;"  vtype="maxLength:30" requiredErrorText="该输入项为必输项" required="true" /></td>
						<input style="width:100%" id="spotRate" name="spotRate" class="mini-Spinner" labelField="true" label="即期汇率(%)：" format="n4" maxValue="99999.9999" labelStyle="text-align:left;width:130px;" requiredErrorText="该输入项为必输项" required="true" />
						<input style="width:100%" id="describe" name="describe" class="mini-textbox" labelField="true" label="描述："  labelStyle="text-align:left;width:130px;" vtype="maxLength:80" />
						<input style="width:100%" id="fixedFloatDir" name="fixedFloatDir" class="mini-combobox mini-mustFill" labelField="true" label="固定/浮动方向："  labelStyle="text-align:left;width:130px;"  required="true"  data="CommonUtil.serverData.dictionary.fixFltDir"/>
					</div>
					<div class="rightarea">
						<input style="width:100%" id="notionalExchange" name="notionalExchange" class="mini-textbox" labelField="true" label="本金交换："  labelStyle="text-align:left;width:130px;"  vtype="maxLength:30" requiredErrorText="该输入项为必输项" required="true" />
						<input style="width:100%" id="unadjustmaturityDate" name="unadjustmaturityDate" class="mini-datepicker" labelField="true" label="未调整到日期：" labelStyle="text-align:left;width:130px;" requiredErrorText="该输入项为必输项" required="true" />
						<input style="width:100%" id="nonVanilla" name="nonVanilla" class="mini-combobox" labelField="true" label="是否标准协议："  labelStyle="text-align:left;width:130px;"  vtype="maxLength:30" data="CommonUtil.serverData.dictionary.YesNo" requiredErrorText="该输入项为必输项" required="true" />
						<input style="width:100%" id="accountingMethod" name="accountingMethod" class="mini-combobox mini-mustFill" labelField="true" label="交易类型："  labelStyle="text-align:left;width:130px;"  vtype="maxLength:20" data="CommonUtil.serverData.dictionary.acctMethod" requiredErrorText="该输入项为必输项" required="true" />
					</div>
				</div>
			</div>
			<div class="mini-panel" title="交易明细信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
				<div class="leftarea">
					<fieldset>
						<legend>左端(First Leg)</legend>
							<input id="leftDirection" name="leftDirection" class="mini-combobox" labelField="true" label="付款方向：" requiredErrorText="该输入项为必输项" required="true"  style="width: 100%;" labelStyle="text-align:left;width:130px;" data="CommonUtil.serverData.dictionary.trading" onvaluechanged="onLeftDirectionChange"/>
							<input id="leftNotion" name="leftNotion" class="mini-spinner" labelField="true" label="名义本金：" requiredErrorText="该输入项为必输项" required="true"  style="width: 100%;" labelStyle="text-align:left;width:130px;" maxValue="999999999999999" changeOnMousewheel='false' format="n4"/>
							<input id="leftRate" name="leftRate" class="mini-Spinner" labelField="true" format="n8" maxValue="999.9999" label="固定/浮动利率：" requiredErrorText="该输入项为必输项" required="true"  style="width: 100%;" labelStyle="text-align:left;width:130px;" changeOnMousewheel='false' />
							<input id="leftFrequency" name="leftFrequency" class="mini-combobox" labelField="true" label="定息周期：" style="width: 100%;" labelStyle="text-align:left;width:130px;" data="CommonUtil.serverData.dictionary.Term" requiredErrorText="该输入项为必输项" required="true" />
							<input id="leftRule" name="leftRule" class="mini-combobox" labelField="true" label="定息规则：" style="width: 100%;" labelStyle="text-align:left;width:130px;"/> 
							<input id="leftBasis" name="leftBasis" class="mini-combobox" labelField="true" label="计息方式：" style="width: 100%;" labelStyle="text-align:left;width:130px;" data="CommonUtil.serverData.dictionary.calIrsDays" requiredErrorText="该输入项为必输项" required="true" />
							<input id="leftPayment" name="leftPayment" class="mini-combobox" labelField="true" label="付息周期：" style="width: 100%;" labelStyle="text-align:left;width:130px;" data="CommonUtil.serverData.dictionary.Term" requiredErrorText="该输入项为必输项" required="true" />
							<input id="leftConvention" name="leftConvention" class="mini-combobox" labelField="true" label="起息计算方式：" style="width: 100%;" labelStyle="text-align:left;width:130px;" data="CommonUtil.serverData.dictionary.AccMethod" requiredErrorText="该输入项为必输项" required="true" />
							<input id="leftDiscountCurve" name="leftDiscountCurve" class="mini-buttonedit mini-mustFill" onbuttonclick="onYchdEdit" labelField="true" label="利率曲线：" style="width: 100%;" labelStyle="text-align:left;width:130px;"  requiredErrorText="该输入项为必输项" required="true" />
							<input id="leftRateCode" name="leftRateCode" class="mini-buttonedit mini-mustFill" onbuttonclick="onRateEdit" labelField="true" label="利率代码：" style="width: 100%;" labelStyle="text-align:left;width:130px;"  requiredErrorText="该输入项为必输项" required="true" />
					</fieldset>
				</div>
				<div class="rightarea">
					<fieldset>
						<legend>右端(Second Leg)</legend>
							<input id="rightDirection" name="rightDirection" class="mini-combobox" labelField="true" label="付款方向：" style="width: 100%;" labelStyle="text-align:left;width:130px;" data="CommonUtil.serverData.dictionary.trading" enabled="false" requiredErrorText="该输入项为必输项" required="true" />
							<input id="rightNotion" name="rightNotion" class="mini-spinner" labelField="true" label="名义本金：" style="width: 100%;" labelStyle="text-align:left;width:130px;" maxValue="999999999999999" changeOnMousewheel='false' format="n4" requiredErrorText="该输入项为必输项" required="true" />
							<input id="rightRate" name="rightRate" class="mini-Spinner" labelField="true" format="n8" maxValue="999.9999" label="固定/浮动利率：" changeOnMousewheel='false' style="width: 100%;" labelStyle="text-align:left;width:130px;" requiredErrorText="该输入项为必输项" required="true"  />
							<input id="rightFrequency" name="rightFrequency" class="mini-combobox" labelField="true" label="定息周期：" style="width: 100%;" labelStyle="text-align:left;width:130px;" data="CommonUtil.serverData.dictionary.Term" requiredErrorText="该输入项为必输项" required="true" />
							<input id="rightRule" name="rightRule" class="mini-combobox" labelField="true" label="定息规则：" style="width: 100%;" labelStyle="text-align:left;width:130px;" />
							<input id="rightBasis" name="rightBasis" class="mini-combobox" labelField="true" label="计息方式：" style="width: 100%;" labelStyle="text-align:left;width:130px;" data="CommonUtil.serverData.dictionary.calIrsDays" requiredErrorText="该输入项为必输项" required="true" />
							<input id="rightPayment" name="rightPayment" class="mini-combobox" labelField="true" label="付息周期：" style="width: 100%;" labelStyle="text-align:left;width:130px;" data="CommonUtil.serverData.dictionary.Term" requiredErrorText="该输入项为必输项" required="true" />
							<input id="rightConvention" name="rightConvention" class="mini-combobox" labelField="true" label="起息计算方式：" style="width: 100%;" labelStyle="text-align:left;width:130px;" data="CommonUtil.serverData.dictionary.AccMethod" requiredErrorText="该输入项为必输项" required="true" />
							<input id="rightDiscountCurve" name="rightDiscountCurve" class="mini-buttonedit mini-mustFill" onbuttonclick="onYchdEdit" labelField="true" label="利率曲线：" style="width: 100%;" labelStyle="text-align:left;width:130px;"  requiredErrorText="该输入项为必输项" required="true" />
							<input id="rightRateCode" name="rightRateCode" class="mini-buttonedit mini-mustFill" onbuttonclick="onRateEdit" labelField="true" label="利率代码：" style="width: 100%;" labelStyle="text-align:left;width:130px;"  requiredErrorText="该输入项为必输项" required="true" />
					</fieldset>
				</div>
			</div>
			<div class="mini-panel" title="清算信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
				<div class="leftarea">
					<input id="sellCurreny" name="sellCurreny" class="mini-combobox" labelField="true" label="清算货币：" style="width: 100%;" labelStyle="text-align:left;width:130px;" data="CommonUtil.serverData.dictionary.Currency" requiredErrorText="该输入项为必输项" required="true" />
					<input id="bankName1" name="bankName1" class="mini-textbox" labelField="true" label="开户行名称：" labelStyle="text-align:left;width:130px;" style="width: 100%;"  vtype="maxLength:40" requiredErrorText="该输入项为必输项" required="true"  />
					<input id="bankBic1" name="bankBic1" class="mini-textbox" labelField="true" label="开户行BIC CODE：" labelStyle="text-align:left;width:130px;" style="width: 100%;" onvalidation="onEnglishAndNumberValidation"  vtype="maxLength:45" requiredErrorText="该输入项为必输项" required="true" />
					<input id="dueBank1" name="dueBank1" class="mini-textbox" labelField="true" label="收款行名称" labelStyle="text-align:left;width:130px;" style="width: 100%;" vtype="maxLength:40" requiredErrorText="该输入项为必输项" required="true" />
					<input id="dueBankName1" name="dueBankName1" class="mini-textbox" labelField="true" label="收款行名称BIC CODE：" labelStyle="text-align:left;width:130px;" style="width: 100%;" onvalidation="onEnglishAndNumberValidation" vtype="maxLength:45" requiredErrorText="该输入项为必输项" required="true" />
					<input id="dueBankAccount1" name="dueBankAccount1" class="mini-textbox" labelField="true" label="收款行账号：" labelStyle="text-align:left;width:130px;" style="width: 100%;" onvalidation="onEnglishAndNumberValidation"  vtype="maxLength:30" requiredErrorText="该输入项为必输项" required="true" />
					<input id="bankName2" name="bankName2" class="mini-textbox" labelField="true" label="开户行名称：" labelStyle="text-align:left;width:130px;" style="width: 100%;"  vtype="maxLength:40" requiredErrorText="该输入项为必输项" required="true" />
					<input id="bankBic2" name="bankBic2" class="mini-textbox" labelField="true" label="开户行BIC CODE：" labelStyle="text-align:left;width:130px;" style="width: 100%;" onvalidation="onEnglishAndNumberValidation"  vtype="maxLength:45" requiredErrorText="该输入项为必输项" required="true" />
					<input id="dueBank2" name="dueBank2" class="mini-textbox" labelField="true" label="收款行名称：" labelStyle="text-align:left;width:130px;" style="width: 100%;"  vtype="maxLength:40" requiredErrorText="该输入项为必输项" required="true" />
					<input id="dueBankName2" name="dueBankName2" class="mini-textbox" labelField="true" label="收款行名称BIC CODE：" labelStyle="text-align:left;width:130px;" style="width: 100%;" onvalidation="onEnglishAndNumberValidation"  vtype="maxLength:45" requiredErrorText="该输入项为必输项" required="true" />
					<input id="dueBankAccount2" name="dueBankAccount2" class="mini-textbox" labelField="true" label="收款行账号：" labelStyle="text-align:left;width:130px;" style="width: 100%;" onvalidation="onEnglishAndNumberValidation" vtype="maxLength:45"  requiredErrorText="该输入项为必输项" required="true" />
				</div>
				<div class="rightarea">
					<input id="buyCurreny" name="buyCurreny" class="mini-combobox" labelField="true" label="清算货币：" style="width: 100%;" labelStyle="text-align:left;width:130px;" data="CommonUtil.serverData.dictionary.Currency" requiredErrorText="该输入项为必输项" required="true" />
					<input id="capitalBank1" name="capitalBank1" class="mini-textbox" labelField="true" label="资金开户：" labelStyle="text-align:left;width:130px;" style="width: 100%;"  vtype="maxLength:40" requiredErrorText="该输入项为必输项" required="true"  />
					<input id="capitalBankName1" name="capitalBankName1" class="mini-textbox" labelField="true" label="资金账户户名：" labelStyle="text-align:left;width:130px;" style="width: 100%;"  vtype="maxLength:40" requiredErrorText="该输入项为必输项" required="true" />
					<input id="cnapsCode1" name="cnapsCode1" class="mini-textbox" labelField="true" label="支付系统行号：" labelStyle="text-align:left;width:130px;" style="width: 100%;" onvalidation="onEnglishAndNumberValidation"  vtype="maxLength:30"  requiredErrorText="该输入项为必输项" required="true" />
					<input id="capitalAccount1" name="capitalAccount1" class="mini-textbox" labelField="true" label="资金账号：" labelStyle="text-align:left;width:130px;" style="width: 100%;" onvalidation="onEnglishAndNumberValidation"  vtype="maxLength:45"  requiredErrorText="该输入项为必输项" required="true" />
					<input id="capitalBank2" name="capitalBank2" class="mini-textbox" labelField="true" label="资金开户行：" labelStyle="text-align:left;width:130px;" style="width: 100%;"  vtype="maxLength:40" requiredErrorText="该输入项为必输项" required="true"  />
					<input id="capitalBankName2" name="capitalBankName2" class="mini-textbox" labelField="true" label="资金账户户名：" labelStyle="text-align:left;width:130px;" style="width: 100%;"  vtype="maxLength:40"  requiredErrorText="该输入项为必输项" required="true" />
					<input id="cnapsCode2" name="cnapsCode2" class="mini-textbox" labelField="true" label="支付系统行号：" labelStyle="text-align:left;width:130px;" style="width: 100%;" onvalidation="onEnglishAndNumberValidation"  vtype="maxLength:30" requiredErrorText="该输入项为必输项" required="true"  />
					<input id="capitalAccount2" name="capitalAccount2" class="mini-textbox" labelField="true" label="资金账号：" labelStyle="text-align:left;width:130px;" style="width: 100%;" onvalidation="onEnglishAndNumberValidation"  vtype="maxLength:45"  requiredErrorText="该输入项为必输项" required="true" />
				</div>
			</div>
			</div>
	</fieldset>
	</div>
	<script>
		mini.parse();

		var url = window.location.search;
		var prdNo = CommonUtil.getParam(url, "prdNo");
		var prdName = CommonUtil.getParam(url, "prdName");
		var grid = mini.get("datagrid");
		/**************************************点击下面显示详情开始******************************/
		var from = new mini.Form("MiniSettleForeigDetail");
		from.setEnabled(false);
		var grid = mini.get("datagrid");
		//grid.load();
		 //绑定表单
        var db = new mini.DataBinding();
        db.bindForm("MiniSettleForeigDetail", grid);
        /**************************************点击下面显示详情结束******************************/
		grid.on("beforeload", function(e) {
			e.cancel = true;
			var pageIndex = e.data.pageIndex;
			var pageSize = e.data.pageSize;
			search(pageSize, pageIndex);
		});
		function getData(action) {
			var row = null;
			if (action != "add") {
				row = grid.getSelected();
			}
			return row;
		}
		
		grid.on("select",function(e){
			var row=e.record;
			mini.get("approve_mine_commit_btn").setEnabled(false);
			mini.get("approve_commit_btn").setEnabled(false);
			mini.get("edit_btn").setEnabled(false);
			mini.get("delete_btn").setEnabled(false);
			mini.get("approve_log").setEnabled(true);
			mini.get("recall_btn").setEnabled(true);
			mini.get("batch_commit_btn").setEnabled(true);
			if(row.approveStatus == "3"){//新建
				mini.get("edit_btn").setEnabled(true);
				mini.get("delete_btn").setEnabled(true);
				mini.get("approve_mine_commit_btn").setEnabled(true);
				mini.get("approve_commit_btn").setEnabled(false);
				mini.get("reback_btn").setEnabled(true);
				mini.get("recall_btn").setEnabled(false);
			}
			if( row.approveStatus == "6" || row.approveStatus == "7"){//审批通过：6    审批拒绝:5
				mini.get("edit_btn").setEnabled(false);
				mini.get("delete_btn").setEnabled(false);
				mini.get("approve_mine_commit_btn").setEnabled(false);
				mini.get("approve_commit_btn").setEnabled(true);
				mini.get("batch_approve_btn").setEnabled(false);
				mini.get("batch_commit_btn").setEnabled(false);
				mini.get("opics_check_btn").setEnabled(false);
				mini.get("reback_btn").setEnabled(false);
				mini.get("recall_btn").setEnabled(false);
			}
			if(row.approveStatus == "5"){//审批中:5
				mini.get("edit_btn").setEnabled(false);
				mini.get("delete_btn").setEnabled(false);
				mini.get("approve_mine_commit_btn").setEnabled(false);
				mini.get("approve_commit_btn").setEnabled(true);
				mini.get("batch_commit_btn").setEnabled(false);
				mini.get("reback_btn").setEnabled(false);
				mini.get("recall_btn").setEnabled(true);
			}
			
		});
		function search(pageSize,pageIndex){
			var form = new mini.Form("#search_form");
			form.validate();
			if(form.isValid()==false){
				mini.alert("信息填写有误，请重新填写","系统也提示");
				return;
			}

			var data=form.getData();
			data['pageNumber']=pageIndex+1;
			data['pageSize']=pageSize;
			data['branchId']=branchId;
			var url=null;

			var approveType = mini.get("approveType").getValue();
			if(approveType == "mine"){
				url = "/IfsCurrencyController/searchPageCcySwapMine";
			}else if(approveType == "approve"){
				url = "/IfsCurrencyController/searchPageCcySwapUnfinished";
			}else{
				url = "/IfsCurrencyController/searchPageCcySwapFinished";
			}

			var params = mini.encode(data);
			CommonUtil.ajax({
				url:url,
				data:params,
				callback : function(data) {
					grid.setTotalCount(data.obj.total);
					grid.setPageIndex(pageIndex);
			        grid.setPageSize(pageSize);
					grid.setData(data.obj.rows);
				}
			});
		}
		//增删改查
		function add(){
			var url = CommonUtil.baseWebPath() + "/cfetsfx/ccyswapEdit.jsp?action=add&prdNo="+prdNo;
			var tab = { id: "MiniCcySwapAdd", name: "MiniCcySwapAdd", title: "货币掉期补录", url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
			var paramData = {selectData:""};
			CommonUtil.openNewMenuTab(tab,paramData);
		}
		function query() {
            search(grid.pageSize, 0);
        }
		function clear(){
			var form=new mini.Form("search_form");
            form.clear();
            search(10,0);
		}
		function edit()
		{
			var row = grid.getSelected();
			if(row){
				var url = CommonUtil.baseWebPath() + "/cfetsfx/ccyswapEdit.jsp?action=edit&ticketid="+row.ticketId;
				var tab = { id: "MiniCcySwapEdit", name: "MiniCcySwapEdit", title: "货币掉期修改", url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
				var paramData = {selectData:""};
			    CommonUtil.openNewMenuTab(tab,paramData);

			} else {
				mini.alert("请选中一条记录！","消息提示");
			}
		}

		function onRowDblClick(e) {
			var grid = mini.get("datagrid");
			var row = grid.getSelected();
			if(row){
					var url = CommonUtil.baseWebPath() + "/cfetsfx/ccyswapEdit.jsp?action=detail&ticketid="+row.ticketId;
					var tab = { id: "MiniCcySwapDetail", name: "MiniCcySwapDetail", title: "货币掉期详情", url: url ,showCloseButton:true};
					var paramData = {selectData:""};
					CommonUtil.openNewMenuTab(tab,paramData);

			} else {
				mini.alert("请选中一条记录！","消息提示");
			}
		}
		//删除
		function del() {
				var grid = mini.get("datagrid");
				var row = grid.getSelected();
				if (row) {
					mini.confirm("您确认要删除选中记录?","系统警告",function(value){
						if(value=="ok"){
						CommonUtil.ajax({
							url: "/IfsCurrencyController/deleteCcySwap",
							
							data: {ticketid: row.ticketId},
							callback: function (data) {
								if (data.code == 'error.common.0000') {
									mini.alert("删除成功");
									grid.reload();
									//search(grid.pageSize,grid.pageIndex);
								} else {
									mini.alert("删除失败");
								}
							}
						});
						}
					});
				}
				else {
					mini.alert("请选中一条记录！", "消息提示");
				}

			}
		/**************************审批相关****************************/
		//审批日志查看
		function appLog(selections){
			var flow_type = Approve.FlowType.VerifyApproveFlow;
			if(selections.length <= 0){
				mini.alert("请选择要操作的数据","系统提示");
				return;
			}
			if(selections.length > 1){
				mini.alert("系统不支持多笔操作","系统提示");
				return;
			}
			if(selections[0].tradeSource == "3"){
				mini.alert("初始化导入的业务没有审批日志","系统提示");
				return false;
			}
			
			Approve.approveLog(flow_type,selections[0].ticketId);
		};
		//提交正式审批、待审批
		function verify(selections){
			var grid = mini.get("datagrid");
			var row = grid.getSelected();
			if(selections.length == 0){
				mini.alert("请选中一条记录！","消息提示");
				return false;
			}else if(selections.length > 1){
				mini.alert("该功能不支持多笔提交，如需多笔提交请点击[批量提交]按钮","系统提示");
				return false;
			}
			if(selections[0]["approveStatus"] != "3" ){
				var searchByIdUrl = "/IfsCfetsrmbDpController/searchCfetsrmbDpAndFlowIdById";
				   Approve.goToApproveJsp(selections[0].taskId,"433","/cfetsfx/ccyswapEdit.jsp", target);
				var target = Approve.approvePage;
				var openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid="+row.ticketId+"&prdNo=ccyswap";
				var id="CcySwapApprove";
				var title="货币掉期审批";
				var tab = {"id": id,name:id,url:openJspUrl,title:title,parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
				var paramData = {selectData:selections[0]};
				CommonUtil.openNewMenuTab(tab,paramData);
				// top["win"].showTab(tab);
			}else{
				Approve.approveCommit(Approve.FlowType.VerifyApproveFlow,selections[0]["ticketId"],Approve.OrderStatus.New,"IfsCswapService",prdNo,function(){
					search(grid.pageSize,grid.pageIndex);
				});
			}
		};
		//审批
		function approve(){
			mini.get("approve_commit_btn").setEnabled(false);
			var messageid = mini.loading("系统正在处理...", "请稍后");
			try {
				verify(grid.getSelecteds());
			} catch (error) {
				
			}
			mini.hideMessageBox(messageid);	
			mini.get("approve_commit_btn").setEnabled(true);
		}
		//提交审批
		function commit(){
			mini.get("approve_mine_commit_btn").setEnabled(false);
			var messageid = mini.loading("系统正在处理...", "请稍后");
			try {
				verify(grid.getSelecteds());
			} catch (error) {
				
			}
			mini.hideMessageBox(messageid);	
			mini.get("approve_mine_commit_btn").setEnabled(true);
		}
		//审批日志
		function searchlog(){
			appLog(grid.getSelecteds());
		}
		//打印
	    function print(){
			var selections = grid.getSelecteds();
			if(selections == null || selections.length == 0){
				mini.alert('请选择一条要打印的数据！','系统提示');
				return false;
			}else if(selections.length>1){
				mini.alert('暂不支持多条数据同时打印，请选择一条要打印的数据！','系统提示');
				return false;
			}
			var canPrint = selections[0].ticketId;
			
			if(!CommonUtil.isNull(canPrint)){
				var actionStr = CommonUtil.pPath + "/sl/IfsPrintUtilController/exportload/print/"+PrintNo.ccyswap+"/"+ canPrint;
				$('<form target="_blank" action=' + actionStr + ' method="post"></form>').appendTo('body').submit().remove();
			};
		}
		$(document).ready(function() {
			//控制按钮显示
			$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
				initButton();
				search(10, 0);
			});
		});
	</script>
</body>
</html>