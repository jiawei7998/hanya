<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../../global.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/standard/Common/RiskCenter.js"></script>
    <script type="text/javascript" src="<%=basePath%>/standard/uploadAndDownload.js"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/miniui/res/ajaxfileupload.js"></script>
    <script type="text/javascript" src="../cfetsrmb/rmbVerify.js"></script>
    <title>外币拆借</title>
    <script type="text/javascript">
        /**prdCode用于页面投资组合的选择   该全局变量必须为prdCode*/
        var prdCode = "MM";
    </script>
</head>
<body style="width:100%;height:100%;background:white">
<div class="mini-splitter" style="width:100%;height:100%;" id="splitter">
    <div size="90%" showCollapseButton="false">
        <h1 style="text-align:center"><strong>外币拆借</strong></h1>
        <div id="field_form" class="mini-fit area" style="background:white">
            <input id="sponsor" name="sponsor" class="mini-hidden"/>
            <input id="sponInst" name="sponInst" class="mini-hidden"/>
            <input id="ticketId" name="ticketId" class="mini-hidden"/>
            <input id="aDate" name="aDate" class="mini-hidden"/>
            <div class="mini-panel" title="成交单编号" style="width:100%;" allowResize="true" collapseOnTitleClick="false">
                <div class="leftarea">
                    <input style="width:98%;" id="contractId" name="contractId" class="mini-textbox mini-mustFill"
                           labelField="true" requiredErrorText="该输入项为必输项" label="成交单编号：" required="true"
                           labelStyle="text-align:left;width:130px;" vtype="maxLength:20"
                           onvalidation="onEnglishAndNumberValidation"/>
                </div>
                <div class="rightarea">
                    <input style="width:100%;" id="cfetscn" name="cfetscn" class="mini-textbox" label="CFETS机构码："
                           labelField="true" visible="false" enabled="false" labelStyle="text-align:left;width:130px;"/>
                </div>
            </div>
            <div class="mini-panel" title="成交双方信息" style="width:100%" allowResize="true" collapseOnTitleClick="false">
                <div class="leftarea">
                    <fieldset>
                        <legend>本方信息</legend>
                        <input id="direction" name="direction" class="mini-combobox mini-mustFill" labelField="true"
                               onvaluechanged="dirVerify" label="本方方向：" required="true" style="width:100%;"
                               labelStyle="text-align:left;width:120px;"
                               data="CommonUtil.serverData.dictionary.lending"/>
                        <input id="instId" name="instId" class="mini-textbox mini-mustFill" labelField="true"
                               label="机构：" style="width:100%;" required="true" enabled="false"
                               labelStyle="text-align:left;width:120px;"/>
                        <input id="dealer" name="dealer" class="mini-textbox mini-mustFill" labelField="true"
                               label="交易员：" vtype="maxLength:20" required="true" enabled="false"
                               labelStyle="text-align:left;width:120px;" style="width:100%;"/>
                        <input id="forDate" name="forDate" class="mini-datepicker" labelField="true" label="交易日期："
                               labelStyle="text-align:left;width:120px;" style="width:100%;"/>
                    </fieldset>
                </div>
                <div class="rightarea">
                    <fieldset>
                        <legend>对手方信息</legend>
                        <input id="oppoDir" name="oppoDir" class="mini-combobox mini-mustFill" enabled="false"
                               labelField="true" label="对方方向：" required="true" style="width:100%;"
                               labelStyle="text-align:left;width:120px;"
                               data="CommonUtil.serverData.dictionary.lending"/>
                        <input id="counterpartyInstId" name="counterpartyInstId" class="mini-buttonedit mini-mustFill"
                               onbuttonclick="onButtonEdit" labelField="true" required="true" label="机构："
                               allowInput="false" style="width:100%;" labelStyle="text-align:left;width:120px;"/>
                        <input id="counterpartyDealer" name="counterpartyDealer" class="mini-textbox" labelField="true" label="交易员：" vtype="maxLength:20"
                               labelStyle="text-align:left;width:120px;" style="width:100%;"/>
                        <input id="forTime" name="forTime" class="mini-timespinner" labelField="true" label="交易时间："
                               labelStyle="text-align:left;width:120px;" style="width:100%;"/>
                    </fieldset>
                </div>
            </div>
            <div class="mini-panel" title="交易主体信息" style="width:100%" allowResize="true" collapseOnTitleClick="false">
                <div class="leftarea">
                    <input id="tradingModel" name="tradingModel" class="mini-combobox mini-mustFill" required="true"
                           labelField="true" label="交易模式：" labelStyle="text-align:left;width:130px;" style="width:100%;"
                           data="CommonUtil.serverData.dictionary.TradeModel"/>
                    <input style="width:100%;" id="currency" name="currency" class="mini-combobox mini-mustFill"
                           labelField="true" data="CommonUtil.serverData.dictionary.Currency" label="货币："
                           labelStyle="text-align:left;width:130px;" required="true" allowInput="false"
                           onvaluechanged="onCurrencyChange"/>
                    <input id="rateCode" name="rateCode" class="mini-buttonedit mini-mustFill"
                           onbuttonclick="onRateEdit" labelField="true" label="利率代码：" style="width: 100%;"
                           labelStyle="text-align:left;width:130px;" allowInput="false" requiredErrorText="该输入项为必输项"
                           required="true"/>
                    <input style="width:100%" id="tenorCode" name="tenorCode" class="mini-textbox"labelField="true" label="CFETS期限代码："
                           labelStyle="text-align:left;width:130px;" enabled="false"/>
                    <input style="width:100%;" id="occupancyDays" name="occupancyDays" class="mini-spinner"
                           labelField="true" minErrorText="0" label="实际占款天数：" onValuechanged="interestAmount"
                           changeOnMousewheel='false' maxValue="10000" labelStyle="text-align:left;width:130px;"/>
                    <input style="width:100%;" id="maturityDate" name="maturityDate"
                           class="mini-datepicker mini-mustFill" labelField="true" onValuechanged="compareDate"
                           label="到期日：" ondrawdate="onDrawDateEnd" required="true"
                           labelStyle="text-align:left;width:130px;"/>
                    <input style="width:100%;" id="interest" name="interest"
                           class="mini-spinner mini-mustFill input-text-strong" labelField="true" label="应计利息："
                           maxValue="99999999999999.9999" changeOnMousewheel='false' format="n4" required="true"
                           labelStyle="text-align:left;width:130px;"/>
                    <input id="note" name="note" class="mini-textbox" labelField="true" label="备注："
                           labelStyle="text-align:left;width:130px;" style="width:100%;" vtype="maxLength:16"/>
                </div>
                <div class="rightarea">
                    <input id="tradingType" name="tradingType" class="mini-combobox mini-mustFill" labelField="true"
                           label="交易方式：" requiredErrorText="该输入项为必输项" required="true" value="RFQ"
                           labelStyle="text-align:left;width:130px" style="width:100%;"
                           data="CommonUtil.serverData.dictionary.TradeType"/>
                    <input style="width:100%;" id="basis" name="basis" class="mini-combobox mini-mustFill"
                           labelField="true" label="计息基准：" onValuechanged="interestAmount"
                           data="CommonUtil.serverData.dictionary.Basis" labelStyle="text-align:left;width:130px;"
                           required="true"/>
                    <input style="width:100%;" id="rate" name="rate"
                           class="mini-spinner mini-mustFill input-text-strong" labelField="true" label="拆借利率(%)："
                           onValuechanged="interestAmount" onvalidation="zeroValidation" changeOnMousewheel='false'
                           required="true" maxValue="99999.999999" format="n6"
                           labelStyle="text-align:left;width:130px;"/>
                    <input style="width:100%" id="tenor" name="tenor" class="mini-spinner mini-mustFill"
                           labelField="true" label="拆借期限(天)：" minValue="0" required="true" maxValue="99999"
                           changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"/>
                    <input style="width:100%;" id="valueDate" name="valueDate" class="mini-datepicker mini-mustFill"
                           labelField="true" onValuechanged="compareDate" label="起息日：" ondrawdate="onDrawDateStart"
                           required="true" labelStyle="text-align:left;width:130px;"/>
                    <input style="width:100%;" id="amount" name="amount"
                           class="mini-spinner mini-mustFill input-text-strong" labelField="true"
                           changeOnMousewheel='false' format="n4" label="拆借金额(元)：" onValuechanged="interestAmount"
                           required="true" maxValue="99999999999999.9999" labelStyle="text-align:left;width:130px;"
                           onvalidation="zeroValidation"/>
                    <input style="width:100%;" id="repaymentAmount" name="repaymentAmount"
                           class="mini-spinner mini-mustFill input-text-strong" labelField="true"
                           changeOnMousewheel='false' format="n4" label="到期还款金额：" onvalidation="zeroValidation"
                           required="true" maxValue="99999999999999.9999" labelStyle="text-align:left;width:130px;"/>
                </div>
            </div>
            <div class="mini-panel" title="报表信息" style="width:100%" allowResize="true" collapseOnTitleClick="false">
                <div class="leftarea">
                    <input id="dealTransType" name="dealTransType" class="mini-combobox" labelField="true"
                           style="width:100%;" label="数据来源：" data="CommonUtil.serverData.dictionary.dealTransType1"
                           vtype="maxLength:10" labelStyle="text-align:left;width:130px;" value="1">
                    <input id="nettingStatus" name="nettingStatus" class="mini-combobox mini-mustFill" required="true"
                           labelField="true" label="净额清算状态：" labelStyle="text-align:left;width:130px;"
                           style="width:100%;" data="CommonUtil.serverData.dictionary.YesNo" showNullItem="true"/>

                </div>
                <div class="rightarea">
                    <input id="fiveLevelClass" name="fiveLevelClass" class="mini-combobox" labelField="true"
                           requiredErrorText="该输入项为必输项" label="五级分类：" labelStyle="text-align:left;width:130px;"
                           style="width:100%;" data="CommonUtil.serverData.dictionary.FiveLevelClass" value="0"/>
                    <input id="attributionDept" name="attributionDept" valueField="id" showFolderCheckBox="true"
                           labelField="true" label="归属机构：" required="true" showCheckBox="true" style="width:100%"
                           labelStyle="text-align:left;width:130px"
                           showTreeLines="true" showTreeIcon="true" textField="instName" parentField="parentId"
                           class="mini-treeselect mini-mustFill" expandOnLoad="true" resultAsTree="false"
                           valueFromSelect="true" emptyText="请选择机构"/>
                </div>
            </div>
            <%@ include file="../../Common/opicsLess.jsp" %>
            <%@ include file="../../Common/custLimit.jsp" %>
            <%@ include file="./accountInfor.jsp" %>
            <%@ include file="./uploadFile.jsp" %>
            <%@ include file="../../Common/Flow/MiniApproveOpCommon.jsp" %>
            <%@ include file="../../Common/print/approveFlowLog.jsp" %>
        </div>
    </div>
    <div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
        <div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"
                                                                style="width:120px;" id="save_btn"
                                                                onclick="save">保存交易</a></div>
        <div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"
                                                                style="width:120px;" id="close_btn"
                                                                onclick="close">关闭界面</a></div>
    </div>
</div>
<script type="text/javascript">
    mini.parse();
    var detailData;

    var url = window.location.search;
    var action = CommonUtil.getParam(url, "action");
    var ticketId = CommonUtil.getParam(url, "ticketid");
    var prdNo = CommonUtil.getParam(url, "prdNo");
    var fPrdCode = CommonUtil.getParam(url, "fPrdCode");
    var prdName = CommonUtil.getParam(url, "prdName");
    var dealType = CommonUtil.getParam(url, "dealType");

    var params = {};
    var tradeData = {};
    if (action != "print") {
        //获取当前tab
        var currTab = top["win"].tabs.getActiveTab();
        var params = currTab.params;
        var row = params.selectData;
        tradeData.selectData = row;
        tradeData.operType = action;
        tradeData.serial_no = row.ticketId;
        tradeData.task_id = row.taskId;
    }
    var ApproveFlowLog = {};
    var log_grid = mini.get("log_grid");//隐藏日志信息列表，打印时显示
    log_grid.hide();
    /**
     *   获取审批日志信息
     */
    ApproveFlowLog.loadLogInfo = function () {
        CommonUtil.ajax({
            url: "/IfsFlowController/approveLog",
            data: {'serial_no': ticketId},
            callback: function (data) {
                if (data != null) {
                    log_grid.setData(data.obj);
                    if (typeof (FlowDesigner) != "undefined") {
                        FlowDesigner.addApporveLog(data.obj);
                    }
                }
            }
        });
    };
    mini.get("forDate").setValue("<%=__bizDate %>");
    mini.get("instId").setEnabled(false);
    mini.get("dealer").setEnabled(false);
    var timestamp = Date.parse(new Date());
    sys(timestamp);

    function sys(stamp) {
        var time = new Date(stamp);
        var result = "";
        result += CommonUtil.singleNumberFormatter(time.getHours()) + ":";
        result += CommonUtil.singleNumberFormatter(time.getMinutes()) + ":";
        result += CommonUtil.singleNumberFormatter(time.getSeconds());
        mini.get("forTime").setValue(result);
    }

    /**
     *   获取机构
     */
    function loadInstitutionTree() {
        CommonUtil.ajax({
            data: {"branchId": branchId},
            url: "/InstitutionController/searchInstitutions",
            callback: function (data) {
                mini.get("attributionDept").setData(data.obj);
                if (detailData != null && detailData.attributionDept != null) {
                    mini.get("attributionDept").setValue(detailData.attributionDept);
                } else {
                    mini.get("attributionDept").setValue("<%=__sessionInstitution.getInstId()%>");
                }
            }
        });
    }

    function inme() {
        if (action == "detail" || action == "edit" || action == "approve" || action == "print") {
            var from = new mini.Form("field_form");
            CommonUtil.ajax({
                url: '/IfsCurrencyController/searchCcyLending',
                data: {
                    ticketId: ticketId
                },
                callback: function (text) {
                    detailData = text.obj;
                    from.setData(text.obj);
                    //投资组合
                    mini.get("port").setValue(text.obj.port);
                    mini.get("port").setText(text.obj.port);
                    //成本中心
                    mini.get("cost").setValue(text.obj.cost);
                    mini.get("cost").setText(text.obj.cost);
                    var product = mini.get("product").getValue();
                    if (product == null || product == "") {
                        //产品代码
                        mini.get("product").setValue("MM");
                        mini.get("product").setText("MM");
                    } else {
                        //产品代码
                        mini.get("product").setValue(text.obj.product);
                        mini.get("product").setText(text.obj.product);
                    }
                    //产品类型
                    mini.get("prodType").setValue(text.obj.prodType);
                    mini.get("prodType").setText(text.obj.prodType);

                    mini.get("counterpartyInstId").setValue(text.obj.counterpartyInstId);
                    queryTextName(text.obj.counterpartyInstId);

                    mini.get("rateCode").setValue(text.obj.rateCode);
                    mini.get("rateCode").setText(text.obj.rateCode);
                    dirVerify();
                    mini.get("instId").setValue(text.obj.instfullname);
                    mini.get("dealer").setValue(text.obj.myUserName);

                    //买入出钱设置授信主体
                    if (text.obj.direction == 'S') {
                        creditsubvalue(text.obj.custNo, "438", null);
                    }

                }
            });
            if (action == "detail" || action == "approve") {
                mini.get("save_btn").hide();
                from.setEnabled(false);
                var form1 = new mini.Form("approve_operate_form");
                form1.setEnabled(true);
                mini.get("upload_btn").setEnabled(false);
            }
            if (action == "print") {
                from.setEnabled(false);
                $("#fileMsg").hide();
                mini.get("splitter").hidePane(2);

                log_grid.show();
                CommonUtil.ajax({
                    url: "/IfsFlowController/getOneFlowDefineBaseInfo",
                    data: {serial_no: ticketId},
                    callerror: function (data) {
                        ApproveFlowLog.loadLogInfo();
                    },
                    callback: function (data) {
                        var innerInterval;
                        innerInitFunction = function () {
                            clearInterval(innerInterval);
                            ApproveFlowLog.loadLogInfo();
                        },
                            innerInterval = setInterval("innerInitFunction()", 100);
                    }
                });
            }

            mini.get("contractId").setEnabled(false);
        } else if (action == "add") {
            mini.get("dealTransType").setValue("1");
            mini.get("instId").setValue("<%=__sessionInstitution.getInstFullname()%>");
            mini.get("dealer").setValue("<%=__sessionUser.getUserName() %>");
            mini.get("sponsor").setValue("<%=__sessionUser.getUserId()%>");
            mini.get("sponInst").setValue("<%=__sessionUser.getInstId() %>");
            $("#fileMsg").hide();
            //产品代码
            mini.get("product").setValue("MM");
            mini.get("product").setText("MM");
        }
        mini.get("dealTransType").setEnabled(false);
    }

    function save() {
        var form = new mini.Form("field_form");
        form.validate();
        if (form.isValid() == false) {
            mini.alert("信息填写有误，请重新填写", "系统提示");
            return;
        }
        var form = new mini.Form("field_form");
        var data = form.getData(true); //获取表单多个控件的数据
        data['instId'] = "<%=__sessionInstitution.getInstId()%>";
        data['dealer'] = "<%=__sessionUser.getUserId() %>";
        data['fPrdCode']=fPrdCode;
        //data['dealTransType']="1";//修改后的状态永远为1
        var json = mini.encode(data); //序列化成JSON
        mini.confirm("确认以当前数据为准吗？", "确认", function (actions) {
            if (actions != "ok") {
                return;
            }
            CommonUtil.ajax({
                url: action == "add" ? "/IfsCurrencyController/addCcyLending" : "/IfsCurrencyController/editCcyLending",
                data: json,
                callback: function (data) {
                    mini.alert(data.desc, '提示信息', function () {//提示弹框并执行回调
                        if (data.code == 'error.common.0000') {
                            top["win"].closeMenuTab();//关闭并刷新当前界面
                        }
                    });
                }
            });
        });
    }

    function onButtonEdit(e) {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Common/CustMini.jsp",
            title: "选择对手方列表",
            width: 900,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.cno);
                        btnEdit.setText(data.cliname);

                        //拆出出钱设置授信主体
                        var myDir = mini.get("direction").getValue();
                        if (myDir == 'S') {
                            creditsubvalue(data.cno, "438", null);
                        }

                        btnEdit.focus();
                    }
                }
            }
        });
    }

    function onCreditTypeChange() {
        var cno = mini.get("counterpartyInstId").getValue();
        creditsubvalue(cno, "438", null);
    }

    //根据交易对手编号查询全称
    function queryTextName(cno) {
        CommonUtil.ajax({
            url: "/IfsOpicsCustController/searchIfsOpicsCust",
            data: {'cno': cno},
            callback: function (data) {
                if (data && data.obj) {
                    mini.get("counterpartyInstId").setText(data.obj.cliname);
                } else {
                    mini.get("counterpartyInstId").setText();
                }
            }
        });
    }


    //利率代码的选择
    function onRateEdit() {
        var btnEdit = this;
        var ccy = mini.get("currency").getValue();
        mini.open({
            url: CommonUtil.baseWebPath() + "/opics/rateMini.jsp?ccy=" + ccy,
            title: "选择利率代码",
            width: 900,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.ratecode);
                        btnEdit.setText(data.ratecode);
                        interestAmount();
                        btnEdit.focus();
                    }
                }

            }
        });
    }


    function onCurrencyChange(e) {
        var currency = mini.get("currency").getValue();
        mini.get("buyCurreny").setValue(currency);
        mini.get("sellCurreny").setValue(currency);
        changeAcct();
    }

    function changeAcct() {
        var buyCurreny = mini.get("buyCurreny").getValue();
        var sellCurreny = mini.get("sellCurreny").getValue();
        $("#bankBic1").show();
        $("#intermediaryBankName1").show();
        $("#intermediaryBankBicCode1").show();
        $("#bankBic2").show();
        $("#intermediaryBankName2").show();
        $("#intermediaryBankBicCode2").show();
        $("#bankAccount1").show();
        $("#bankAccount2").show();
        $("#intermediaryBankAcctNo1").show();
        $("#intermediaryBankAcctNo2").show();

        if (buyCurreny == "CNY") {
            mini.get("bankName1").setLabel("资金账户户名：");
            mini.get("dueBank1").setLabel("资金开户行：");
            mini.get("dueBankName1").setLabel("支付系统行号：");
            mini.get("capitalAccount1").setLabel("资金账号：");
            $("#bankBic1").hide();
            $("#intermediaryBankName1").hide();
            $("#intermediaryBankBicCode1").hide();
            $("#bankAccount1").hide();
            $("#intermediaryBankAcctNo1").hide();

        }
        if (sellCurreny == "CNY") {
            mini.get("bankName2").setLabel("资金账户户名：");
            mini.get("dueBank2").setLabel("资金开户行：");
            mini.get("dueBankName2").setLabel("支付系统行号：");
            mini.get("capitalAccount2").setLabel("资金账号：");
            $("#bankBic2").hide();
            $("#intermediaryBankName2").hide();
            $("#intermediaryBankBicCode2").hide();
            $("#bankAccount2").hide();
            $("#intermediaryBankAcctNo2").hide();
        }
    }

    //文、数字、下划线 的验证
    function onEnglishAndNumberValidations(e) {
        if (e.value == "" || e.value == null) {//值为空，就不做校验
            return;
        }
        if (e.isValid) {
            if (isEnglishAndNumbers(e.value) == false) {
                e.errorText = "必须输入英文小写+数字";
                e.isValid = false;
            }
        }
    }

    /* 是否英文+数字 */
    function isEnglishAndNumbers(v) {
        var re = new RegExp("^[0-9a-z\_\-]+$");
        if (re.test(v)) return true;
        return false;
    }


    //英文、数字、下划线 的验证
    function onEnglishAndNumberValidation(e) {
        if (e.value == "" || e.value == null) {//值为空，就不做校验
            return;
        }
        if (e.isValid) {
            if (isEnglishAndNumber(e.value) == false) {
                e.errorText = "必须输入英文+数字";
                e.isValid = false;
            }
        }
    }

    /* 是否英文+数字 */
    function isEnglishAndNumber(v) {
        var re = new RegExp("^[0-9a-zA-Z\_\-]+$");
        if (re.test(v)) return true;
        return false;
    }

    function close() {
        top["win"].closeMenuTab();
    }

    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
            inme();
            loadInstitutionTree();
        });
    });

    //日期判断
    function compareDate() {
        var sta = mini.get("valueDate");
        var end = mini.get("maturityDate");
        if (sta.getValue() != null && sta.getValue("") && end.getValue("") && end.getValue() != null) {
            sDate1 = Date.parse(sta.getValue());
            sDate2 = Date.parse(end.getValue());
            dateSpan = sDate2 - sDate1;
            dateSpan = Math.abs(dateSpan);
            iDays = Math.floor(dateSpan / (24 * 3600 * 1000));
            mini.get("occupancyDays").setValue(iDays);
            mini.get("tenor").setValue(iDays);
            interestAmount();
        }
    }

    //到期日
    function onDrawDateEnd(e) {
        var date = e.date;
        var d = mini.get("valueDate").getValue();
        if (d == null || d == "") {
            return;
        }

        if (date.getTime() < d.getTime()) {
            e.allowSelect = false;
        }
    }

    //起息日
    function onDrawDateStart(e) {
        var date = e.date;
        var d = mini.get("maturityDate").getValue();
        if (d == null || d == "") {
            return;
        }

        if (date.getTime() > d.getTime()) {
            e.allowSelect = false;
        }
    }

    //利率计算
    function interestAmount() {
        var a = mini.get("rate").getValue();//利率
        var b = mini.get("basis").getValue();//利率基础
        var c = mini.get("occupancyDays").getValue();//占款天数
        var d = mini.get("amount").getValue();//拆借金额
        var ccy = mini.get("currency").getValue();//币种
        if (ccy == "JPY") {//交易币种是日元时无角分
            d = Math.ceil(d);
            mini.get("amount").setValue(d);
        }
        if (a != "" && a != null && b != "" && b != null && c != "" && c != null && d != "" && d != null) {
            if (b != 'A360') {
                var e = 365;
            } else {
                var e = 360;
            }
            var interest = Number(a) * Number(c) * Number(d) / (parseInt(e) * 100);
            if (ccy == "JPY") {  // 交易币种是日元时无角分
                interest = Math.ceil(interest);
            }
            mini.get("interest").setValue(interest);
            var allamount = interest + Number(d);
            mini.get("repaymentAmount").setValue(allamount);
        }
    }


    function dirVerify(e) {
        var direction = mini.get("direction").getValue();
        if (direction == 'S') {//拆出
            mini.get("panel1").setVisible(true);
            mini.get("oppoDir").setValue("P");

            var cno = mini.get("counterpartyInstId").getValue();
            creditsubvalue(cno, "438", null);
        } else if (direction == 'P') {//拆入
            mini.get("panel1").setVisible(false);
            mini.get("oppoDir").setValue("S");
        }
    }


    //根据英文节点类型返回相应中文
    ApproveFlowLog.ActivityType = function (type) {
        var result = null;
        switch (type) {
            case "startEvent":
                result = "开始节点";
                break;
            case "exclusiveGateway":
                result = "决策节点";
                break;
            case "userTask":
                result = "人工节点";
                break;
            case "endEvent":
                result = "结束节点";
                break;
            default:
                result = type;
        }
        return result;
    };

    function stampToTimeRenderer(e) {
        if (e.value) {
            return CommonUtil.stampToTime(e.value);
        } else {
            return "";
        }
    }

    function timerFormatRenderer(e) {
        if (e.value) {
            return CommonUtil.secondFormatter(e.value / 1000);
        } else {
            return "";
        }
    }

    function activityTypeRenderer(e) {
        if (e.value) {
            return e.value;
        } else {
            return ApproveFlowLog.ActivityType(e.value);
        }
    }
</script>
<script type="text/javascript" src="../../Common/Flow/MiniApproveOpCommon.js"></script>
</body>
</html>