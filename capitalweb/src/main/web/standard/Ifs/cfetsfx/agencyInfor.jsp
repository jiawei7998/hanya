<%@ page language="java" pageEncoding="UTF-8"%>
<div class="mini-panel" title="成交双方信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
		<div class="leftarea" >
				<input id="instId" name="instId" class="mini-textbox" labelField="true"  label="本方机构："  style="width:100%;"  labelStyle="text-align:left;width:130px;" requiredErrorText="该输入项为必输项" required="true"  vtype="maxLength:16"/>
				<input id="dealer" name="dealer" class="mini-textbox" labelField="true" requiredErrorText="该输入项为必输项" required="true"   label="本方交易员："  labelStyle="text-align:left;width:130px;" style="width:100%;"   requiredErrorText="该输入项为必输项" required="true"  vtype="maxLength:6"/>
				<input id="forDate" name="forDate" class="mini-datepicker" labelField="true" label="交易日期："  labelStyle="text-align:left;width:130px;" style="width:100%;"  requiredErrorText="该输入项为必输项" required="true"  />
				<input id="tradingModel" name="tradingModel" class="mini-combobox" labelField="true"   label="交易模式："  value="C-Swap" labelStyle="text-align:left;width:130px;" style="width:100%;"  data="CommonUtil.serverData.dictionary.TradeModel"/>
		</div>
		<div class="rightarea">
				<input id="counterpartyInstId" name="counterpartyInstId" class="mini-buttonedit" onbuttonclick="onButtonEdit" labelField="true"  label="对方机构："  style="width:100%;"  labelStyle="text-align:left;width:130px;" vtype="maxLength:16" requiredErrorText="该输入项为必输项" required="true" />
				<input id="counterpartyDealer" name="counterpartyDealer" class="mini-textbox" labelField="true"  label="对方交易员："  labelStyle="text-align:left;width:130px;" style="width:100%;"   vtype="maxLength:6"/>
				<input id="forTime" name="forTime" class="mini-timespinner" labelField="true" requiredErrorText="该输入项为必输项" required="true"   label="交易时间："  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
				<input id="tradingType" name="tradingType" class="mini-combobox" labelField="true" requiredErrorText="该输入项为必输项" required="true"  label="交易方式："  value="C-Swap" labelStyle="text-align:left;width:130px;" style="width:100%;" data="CommonUtil.serverData.dictionary.TradeType"/>
		</div>
</div>


