<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ include file="../../global.jsp"%>
<title>外汇期权</title>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
	<script type="text/javascript" src="<%=basePath%>/standard/Common/RiskCenter.js"></script>
	 <script type="text/javascript" src="../cfetsrmb/rmbVerify.js"></script>
	<script type="text/javascript" >
	/**prdCode用于页面投资组合的选择   该全局变量必须为prdCode*/
	var prdCode="OTC";
	</script>
	
</head>
<body style="width:100%;height:100%;background:white">
		<div class="mini-splitter" style="width:100%;height:100%;">
		<div size="90%" showCollapseButton="false">
		<h1 style="text-align:center"><strong>外汇期权</strong></h1>
		<div  id="field_form"  class="mini-fit area" style="background:white" >
	<input id="dealTransType" name="dealTransType" class="mini-hidden"/>
	<input id="sponsor" name="sponsor" class="mini-hidden" />
	<input id="sponInst" name="sponInst" class="mini-hidden" />
	<input id="ticketId" name="ticketId" class="mini-hidden" />
	<input id="aDate" name="aDate" class="mini-hidden"/>
	<input id="ccy" name="ccy" class="mini-hidden" />
	<input id="ctrccy" name="ctrccy" class="mini-hidden" />
	
	<div class="mini-panel" title="成交单编号" style="width:100%;"  allowResize="true" collapseOnTitleClick="true">
			<div class="leftarea">
				<input style="width:100%;" id="contractId" name="contractId" class="mini-textbox mini-mustFill" labelField="true" label="成交单编号：" labelStyle="text-align:left;width:130px;" onvalidation="chineseValidation" vtype="maxLength:35"/>
			</div>
			<div class="rightarea">
				<input style="width:100%;" id="delta" name="delta" class="mini-textbox mini-mustFill" labelField="true"  required="true"  label="Delta交换交易号：" labelStyle="text-align:left;width:130px;" required="true"  vtype="maxLength:20"/>
			</div>
		</div>
	<div class="mini-panel" title="成交双方信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
		<div class="leftarea">
			<input id="instId" name="instId" class="mini-textbox" labelField="true"  label="本方机构：" maxLength="25"	 style="width:100%;"  labelStyle="text-align:left;width:130px;" />
			<input id="dealer" name="dealer" class="mini-textbox" labelField="true"  label="本方交易员：" maxLength="20"	 labelStyle="text-align:left;width:130px;" style="width:100%;"   />
		</div>
		<div class="rightarea">
			<input id="counterpartyInstId" name="counterpartyInstId" class="mini-buttonedit mini-mustFill" labelField="true"  label="对方机构：" onbuttonclick="onButtonEdit" maxLength="25"	allowInput="false" style="width:100%;" required="true"  labelStyle="text-align:left;width:130px;" />
			<input id="counterpartyDealer" name="counterpartyDealer" class="mini-textbox mini-mustFill" labelField="true"  label="对方交易员：" maxLength="30"	 labelStyle="text-align:left;width:130px;" style="width:100%;" required="true"   />
		</div>	
	</div>		
	<div class="mini-panel" title="交易主体信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
		<div class="leftarea">
			<input id="forDate" name="forDate" class="mini-datepicker" labelField="true"  label="交易日期："  labelStyle="text-align:left;width:130px;" style="width:100%;"  onvaluechanged="onSetTenor" />
			<input style="width:100%" id="currencyPair" name="currencyPair" class="mini-combobox" labelField="true"  label="货币对："   data="CommonUtil.serverData.dictionary.CurrencyPair" labelStyle="text-align:left;width:130px;" onvaluechanged="onCurrencyPairChange"/>
			<input style="width:100%" id="tradingType" name="tradingType" class="mini-combobox" labelField="true"  label="交易类型：" maxLength="20"  labelStyle="text-align:left;width:130px;" data="CommonUtil.serverData.dictionary.tradingTypeOtc"  />
			<!-- <input style="width:100%" id="buyNotional" name="buyNotional" class="mini-combobox" labelField="true"  label="名义本金方向：" data="CommonUtil.serverData.dictionary.trading"  labelStyle="text-align:left;width:130px;"   />
			<input style="width:100%" id="sellNotional" name="sellNotional" class="mini-combobox" labelField="true"  label="名义本金方向：" data="CommonUtil.serverData.dictionary.trading"   labelStyle="text-align:left;width:130px;"   />
			 -->
		    <input style="width:100%" id="strikePrice" name="strikePrice" class="mini-spinner" changeOnMousewheel='false'  format="n4" labelField="true"  label="执行价：" maxValue="999999999999999"  labelStyle="text-align:left;width:130px;" onvaluechanged="setSellAmount" onvalidation="zeroValidation" />
			<input style="width:100%" id="expiryDate" name="expiryDate" class="mini-datepicker" labelField="true"  label="行权日："  ondrawdate="compareDate2" labelStyle="text-align:left;width:130px;"   onvaluechanged="onSetTenor"/>
			<input style="width:100%" id="otcfeeCcy" name="otcfeeCcy" class="mini-combobox" labelField="true"  label="期权费币种："  labelStyle="text-align:left;width:130px;"  data="CommonUtil.serverData.dictionary.Currency"  />
			<input style="width:100%" id="premiumDate" name="premiumDate" class="mini-datepicker" labelField="true"  label="期权费交付日："  ondrawdate="compareDate1"  labelStyle="text-align:left;width:130px;"   />
			<input style="width:100%" id="deliveryDate" name="deliveryDate" class="mini-datepicker" labelField="true"  label="交割日：" ondrawdate="compareDate3" labelStyle="text-align:left;width:130px;" onvaluechanged = "searchProductWeightNew()"/>
			<input style="width:100%" id="cutOffTime" name="cutOffTime" class="mini-timespinner" labelField="true"  label="行权截止时间："   labelStyle="text-align:left;width:130px;" />
			<input style="width:100%" id="optionStrategy" name="optionStrategy" class="mini-combobox" labelField="true"  label="期权类型：" maxLength="20"  labelStyle="text-align:left;width:130px;"  data="CommonUtil.serverData.dictionary.otcType" />
			<input id="tradingModel" name="tradingModel" class="mini-combobox mini-mustFill" required="true"  labelField="true"   label="交易模式："  labelStyle="text-align:left;width:130px;" style="width:100%;"  data="CommonUtil.serverData.dictionary.TradeModel"/>
			<input style="width:100%" id="strategyId" name="strategyId" class="mini-textbox" labelField="true"  label="期权组合号：" maxLength="20"  labelStyle="text-align:left;width:130px;"   />
			<input id="delaydElivind" name="delaydElivind" class="mini-combobox" labelField="true"   label="延期交易：" labelStyle="text-align:left;width:130px;" style="width:100%;" data="CommonUtil.serverData.dictionary.delaydElivind" value='0'/>
		</div>
		<div class="rightarea">
			<input id="forTime" name="forTime" class="mini-timespinner" labelField="true"  label="交易时间："  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
			<input style="width:100%" id="direction" name="direction" class="mini-combobox" labelField="true"  label="交易方向：" data="CommonUtil.serverData.dictionary.trading"  labelStyle="text-align:left;width:130px;" onvaluechanged="onCurrencyPairChange"/>
			<input style="width:100%" id="buyNotionalAmount" name="buyNotionalAmount" class="mini-spinner" labelField="true"  label="名义本金金额1：" changeOnMousewheel='false' maxValue="999999999999999" format="n4"  labelStyle="text-align:left;width:130px;"  onvalidation="zeroValidation" />
		    <input style="width:100%" id="sellNotionalAmount" name="sellNotionalAmount" class="mini-spinner" labelField="true"  label="名义本金金额2：" changeOnMousewheel='false' maxValue="999999999999999" format="n4"  labelStyle="text-align:left;width:130px;" onvaluechanged="setPrice" onvalidation="zeroValidation" />
			<input style="width:100%" id="tenor" name="tenor" class="mini-textbox" labelField="true"  label="期限(天)：" labelStyle="text-align:left;width:130px;" />
			<input style="width:100%" id="premiumAmount" name="premiumAmount" class="mini-spinner" labelField="true"  label="期权费金额：" changeOnMousewheel='false' maxValue="999999999999999" format="n4"  labelStyle="text-align:left;width:130px;" onvalidation="zeroValidation"/>
			<input style="width:100%" id="otcfeeDirection" name="otcfeeDirection" class="mini-combobox" labelField="true"  label="期权费收付方向："  labelStyle="text-align:left;width:130px;"  data="CommonUtil.serverData.dictionary.irsing"  />
			<!-- <input style="width:100%" id="premiumType" name="premiumType" class="mini-textbox" labelField="true"  label="期权类型：" maxLength="20"  labelStyle="text-align:left;width:130px;"/>  -->
			<input style="width:100%" id="deliveryType" name="deliveryType" class="mini-combobox" labelField="true"  label="交割方式：" maxLength="20" data="CommonUtil.serverData.dictionary.OtcDeliveryType"  labelStyle="text-align:left;width:130px;"   />
			<input style="width:100%" id="exerciseType" name="exerciseType" class="mini-combobox" labelField="true"  label="执行类型：" maxLength="20"  labelStyle="text-align:left;width:130px;"  data="CommonUtil.serverData.dictionary.ExerciseType" />
		    <input style="width:100%" id="plmethod" name="plmethod" class="mini-combobox mini-mustFill" labelField="true" label="损益计算方法："  labelStyle="text-align:left;width:130px;"  vtype="maxLength:15" data="CommonUtil.serverData.dictionary.acctMethod" requiredErrorText="该输入项为必输项" required="true" />
			<input id="nettingStatus" name="nettingStatus" class="mini-combobox mini-mustFill" required="true"  labelField="true"   label="净额清算状态：" labelStyle="text-align:left;width:130px;" style="width:100%;" data="CommonUtil.serverData.dictionary.YesNo" showNullItem="true" onvaluechanged = "showOrHideNetting()"/>
			<input style="width:100%" id="spotReference" name="spotReference" class="mini-textbox" labelField="true"  label="即期参考汇率(%)：" maxLength="20"  labelStyle="text-align:left;width:130px;" />
		</div>	
	</div>
	<%@ include file="../../Common/opicsLess.jsp"%>
	<%@ include file="../../Common/RiskCenter.jsp"%>
	<div id="isNetting" class="mini-panel" title="清算账户信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
	  <div class="leftarea">	
		  	<input id="nettingAddr" name="nettingAddr" class="mini-textbox" labelField="true" label="清算路径："  labelStyle="text-align:left;width:130px;" style="width:100%;" enabled="false" value="上海清算所净额清算"/>
		</div>
	</div>
	<%@ include file="./accountInfor.jsp"%>		
	<%@ include file="../../Common/Flow/MiniApproveOpCommon.jsp"%>
</div>
</div>
<div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
		<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="save_btn"  onclick="save">保存交易</a></div>
		<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="close_btn"  onclick="close">关闭界面</a></div>
	</div>
	</div>
	<script type="text/javascript">
	mini.parse();
	//获取当前tab
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var row=params.selectData;
	var url = window.location.search;
	var action = CommonUtil.getParam(url, "action");
	var ticketId=CommonUtil.getParam(url, "ticketid");
	mini.get("instId").setEnabled(false);
	mini.get("dealer").setEnabled(false);
	mini.get("weight").setEnabled(false);
	mini.get("weight").setLabel("风险暴露系数：");
	mini.get("profitLoss").setVisible(true);
	
	var prdNo = CommonUtil.getParam(url, "prdNo");
	var dealNo = CommonUtil.getParam(url, "dealNo");
	
	mini.get("forDate").setValue("<%=__bizDate %>");

	var timestamp = Date.parse(new Date());
	sys(timestamp);
	function sys(stamp){
		var time = new Date(stamp);
		var result = "";
		result += CommonUtil.singleNumberFormatter(time.getHours()) + ":"; 
		result += CommonUtil.singleNumberFormatter(time.getMinutes()) + ":";
		result += CommonUtil.singleNumberFormatter(time.getSeconds());
		mini.get("forTime").setValue(result);
	}
	
	var tradeData={};
	
	tradeData.selectData=row;
	tradeData.operType=action;
	tradeData.serial_no=row.ticketId;
	tradeData.task_id=row.taskId;
	
	function inme(){
		if(action=="detail"||action=="edit"||action=="approve"){
		var from = new mini.Form("field_form");
		mini.get("applyNo").setVisible(false);//信用风险审查单号：
		mini.get("applyProd").setVisible(false);//产品名称
		CommonUtil.ajax({
			url:'/IfsForeignController/searchOption',
			data:{ticketId:ticketId},
			callback:function(text){
				from.setData(text.obj);
				//投资组合
				mini.get("port").setValue(text.obj.port);
				mini.get("port").setText(text.obj.port);
				//成本中心
				mini.get("cost").setValue(row.cost);
				mini.get("cost").setText(row.cost);
				//产品代码
				mini.get("product").setValue(row.product);
				mini.get("product").setText(row.product);
				//产品类型
				mini.get("prodType").setValue(row.prodType);
				mini.get("prodType").setText(row.prodType);
				mini.get("counterpartyInstId").setValue(text.obj.counterpartyInstId);
				queryCustNo(text.obj.custNo);
				
				queryTextName(text.obj.counterpartyInstId);
				/* mini.get("counterpartyInstId").setText(text.obj.counterpartyInstId); */
				mini.get("instId").setValue("<%=__sessionInstitution.getInstName()%>");
				//清算路径
				mini.get("ccysmeans").setValue(text.obj.ccysmeans);
				mini.get("ccysmeans").setText(text.obj.ccysmeans);
				mini.get("ctrsmeans").setValue(text.obj.ctrsmeans);
				mini.get("ctrsmeans").setText(text.obj.ctrsmeans);
				mini.get("applyNo").setValue(text.obj.applyNo);
				mini.get("applyNo").setText(text.obj.applyNo);
				//mini.get("weight").setValue(text.obj.weight);权重值修改为实时更新
				searchProductWeightNew();
				
				mini.get("instId").setValue("<%=__sessionInstitution.getInstFullname()%>");
				mini.get("dealer").setValue(row.myUserName);
				//mini.get("dealSource").setValue('D');
				changeAcct();
			}
		});
		if(action=="detail"||action=="approve"){
			mini.get("save_btn").hide();
			from.setEnabled(false);
			var form1=new mini.Form("approve_operate_form");
			form1.setEnabled(true);
		}

		mini.get("contractId").setEnabled(false);
		showOrHideNetting(row.nettingStatus);
		}else if(action=="add"){
			//mini.get("dealSource").setValue('D');
			mini.get("dealTransType").setValue("M");
			mini.get("instId").setValue("<%=__sessionInstitution.getInstFullname()%>");
			mini.get("dealer").setValue("<%=__sessionUser.getUserName() %>");
			mini.get("sponsor").setValue("<%=__sessionUser.getUserId()%>");
			mini.get("sponInst").setValue("<%=__sessionUser.getInstId() %>");
			mini.get("instId").setText("<%=__sessionUser.getInstId()%>");
			//mini.get("aDate").setValue("<%=__bizDate %>");
			mini.get("applyNo").setVisible(false);//信用风险审查单号：
			mini.get("applyProd").setVisible(false);//产品名称
			showOrHideNetting();
		}
	}
	
	function save(){
		var form = new mini.Form("field_form");
		form.validate();
		if (form.isValid() == false) {
			return;
		}
		var form = new mini.Form("field_form");            
		var data = form.getData(true);      //获取表单多个控件的数据
		data['instId']="<%=__sessionInstitution.getInstId()%>";
		data['dealer']="<%=__sessionUser.getUserId() %>";
		data['sponsor'] ="<%=__sessionUser.getUserId() %>";
		data['sponInst']="<%=__sessionUser.getInstId()%>";		
		data['dealTransType']="1";//修改后的状态永远为1
		var json = mini.encode(data);   //序列化成JSON
		if(action=="add"){
			CommonUtil.ajax({
			    url: "/IfsForeignController/addOption",
			    data:json,
			    callback:function (data) {
			    	mini.alert(data,'提示信息',function(){
						if(data=='保存成功'){
						top["win"].closeMenuTab();
						}
					});
			    }
			});
			}else  if(action="edit"){
				CommonUtil.ajax({
					url:"/IfsForeignController/editOption",
					data:json,
					callback:function(data){
						if (data.code == 'error.common.0000') {
							mini.alert(data.desc,'提示信息',function(){
								top["win"].closeMenuTab();
							});
						} else {
							mini.alert("保存失败");
				    }
					}
				});
			}
	}
	function onButtonEdit(e) {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Common/CustMini.jsp",
            title: "选择对手方列表",
            width: 900,
            height: 600,
            onload: function () {//弹出页面加载完成
                var iframe = this.getIFrameEl(); 
                var data = {};
                
                //调用弹出页面方法进行初始化
                //iframe.contentWindow.SetData(data); 
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.cno);
                        btnEdit.setText(data.cliname);
                        queryCustNo(data.creditsub);
                        btnEdit.focus();
                    }
                }

            }
        });

    }
	//根据交易对手编号查询全称
	function queryTextName(cno){
		CommonUtil.ajax({
		    url: "/IfsOpicsCustController/searchIfsOpicsCust",
		    data : {'cno' : cno},
		    callback:function (data) {
		    	if (data && data.obj) {
		    		mini.get("counterpartyInstId").setText(data.obj.cliname);
				} else {
					mini.get("counterpartyInstId").setText();
		    }
		    }
		});
	}
	//文、数字、下划线 的验证
	function onEnglishAndNumberValidations(e) {
		if(e.value == "" || e.value == null){//值为空，就不做校验
			return;
		}
		if (e.isValid) {
			if (isEnglishAndNumbers(e.value) == false) {
				e.errorText = "必须输入英文小写+数字";
				e.isValid = false;
			}
		}
	}
	/* 是否英文+数字 */
	function isEnglishAndNumbers(v) {
		var re = new RegExp("^[0-9a-z\_\-]+$");
		if (re.test(v)) return true;
		return false;
	}	
	
	
	//英文、数字、下划线 的验证
	function onEnglishAndNumberValidation(e) {
		if(e.value == "" || e.value == null){//值为空，就不做校验
			return;
		}
		if (e.isValid) {
			if (isEnglishAndNumber(e.value) == false) {
				e.errorText = "必须输入英文+数字";
				e.isValid = false;
			}
		}
	}
	/* 是否英文+数字 */
	function isEnglishAndNumber(v) {
		var re = new RegExp("^[0-9a-zA-Z\_\-]+$");
		if (re.test(v)) return true;
		return false;
	}
	//不能输入中文
	function chineseValidation(e){
		var re = new RegExp("^[^\u4e00-\u9fa5]{0,}$");
        if (re.test(e.value)) {
        	return true;
        }else{
            e.errorText = "不能输入中文";
            e.isValid = false;
        }
	}
	//日期判断
    function compareDate1(e) {
		var date = e.date;
		var d = mini.get("expiryDate").getValue();
		var f = mini.get("deliveryDate").getValue();
		if(d==null || d == "" && f=="" || f==null){
			return;
		}
		if(d!=null && d!= ""){
		if (date.getTime() > d.getTime()) {
			e.allowSelect = false;
		}
		}
		if(f!=null && f!= ""){
			if (date.getTime() > f.getTime()) {
				e.allowSelect = false;
			}
		}
	}
    function compareDate2(e) {
		var date = e.date;
		var d = mini.get("premiumDate").getValue();
		var f = mini.get("deliveryDate").getValue();
		if(d==null || d == "" && f=="" || f==null){
			return;
		}
		if(d!=null && d!= ""){
			if (date.getTime() < d.getTime()) {
				e.allowSelect = false;
			}
		}
		if(f!=null && f!= ""){
			if (date.getTime() > f.getTime()) {
				e.allowSelect = false;
			}
		}
		}
    function compareDate3(e) {
		var date = e.date;
		var d = mini.get("premiumDate").getValue();
		var f = mini.get("expiryDate").getValue();
		if(d==null || d == "" && f=="" || f==null){
			return;
		}
		if(d!=null && d!=""){
			if (date.getTime() < d.getTime()) {
				e.allowSelect = false;
			}
		}
		if(f!=null && f!= ""){
			if (date.getTime() < f.getTime()) {
				e.allowSelect = false;
			}
		}
	}
	function close(){
		top["win"].closeMenuTab();
	}
	$(document).ready(function () {
		inme();
	})
	
	
	/**   付款路径的选择   */
	function settleMeans1(){
		 var cpInstId = mini.get("counterpartyInstId").getValue();
		if(cpInstId == null || cpInstId == ""){
			mini.alert("请先选择对方机构!");
			return;
		} 
		
		/* var ps = mini.get("direction").getValue();
		if(ps == null || ps == ""){
			mini.alert("请先选择交易方向!");
			return;
		} */
		var currencyPair = mini.get("currencyPair").getText();
		if(currencyPair == null || currencyPair == ""){
			mini.alert("请先选择货币对!");
			return;
		}
		//解析货币对
		var ret = parseCurrencyPair(currencyPair);
		var url;
		var data;
		
		if(ret.ccy!="CNY"){//外币
			url="./Ifs/opics/nostMini.jsp";
        	data = { ccy: ret.ccy ,cust:cpInstId};
        }else if(ret.ccy=="CNY"){//人民币
        	data = { ccy: ret.ccy ,cust:cpInstId};
        	url="./Ifs/opics/setaMini.jsp";
        }
		
		var btnEdit = this;
        mini.open({
            url: url,
            title: "选择清算路径",
            width: 700,
            height: 600,
            onload: function () {
                var iframe = this.getIFrameEl();
                iframe.contentWindow.SetData(data);
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data1 = iframe.contentWindow.GetData();
                    data1 = mini.clone(data1);    //必须
                    if (data1) {
                    	if(ret.ccy!="CNY"){//外币
                    		mini.get("ccysmeans").setValue($.trim(data1.smeans));
                    		mini.get("ccysmeans").setText($.trim(data1.smeans));
                            mini.get("ccysacct").setValue($.trim(data1.nos));
                        }else if(ret.ccy=="CNY"){//人民币
                        	mini.get("ccysmeans").setValue($.trim(data1.smeans));
	                    	 mini.get("ccysmeans").setText($.trim(data1.smeans));
	                         mini.get("ccysacct").setValue($.trim(data1.sacct)); 
                        }
                        
                        btnEdit.focus();
                    }
                }

            }
        });
		
		
	}
	//解析货币对
	function parseCurrencyPair(cp){
		var ccy = cp.substr(0,3);
		var ctr = cp.substr(4,7);
		var data = {ccy:ccy,ctr:ctr};
		return data;
		
		
	}
	
	/**   收款路径的选择   */
	function settleMeans2(){
		 var cpInstId = mini.get("counterpartyInstId").getValue();
		if(cpInstId == null || cpInstId == ""){
			mini.alert("请先选择对方机构!");
			return;
		} 
		
		/* var ps = mini.get("direction").getValue();
		if(ps == null || ps == ""){
			mini.alert("请先选择交易方向!");
			return;
		} */
		var currencyPair = mini.get("currencyPair").getText();
		if(currencyPair == null || currencyPair == ""){
			mini.alert("请先选择货币对!");
			return;
		}
		/* ps = mini.get("direction").getValue();
		if(ps=="1"){
			ps=2;
		}else{
			ps=1;
		} */
		//解析货币对
		var ret = parseCurrencyPair(currencyPair);
		var url;
		var data;
		
		if(ret.ctr!="CNY"){//外币
			url="./Ifs/opics/nostMini.jsp";
        	data = { ccy: ret.ctr ,cust:cpInstId};
        }else if(ret.ctr=="CNY"){//人民币
        	data = { ccy: ret.ctr ,cust:cpInstId};
        	url="./Ifs/opics/setaMini.jsp";
        }
		
		var btnEdit = this;
        mini.open({
            url: url,
            title: "选择清算路径",
            width: 700,
            height: 600,
            onload: function () {
                var iframe = this.getIFrameEl();
                iframe.contentWindow.SetData(data);
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data1 = iframe.contentWindow.GetData();
                    data1 = mini.clone(data1);    //必须
                    if (data1) {
                    	if(ret.ctr!="CNY"){//外币
                    		mini.get("ctrsmeans").setValue($.trim(data1.smeans));
                    		mini.get("ctrsmeans").setText($.trim(data1.smeans));
                            mini.get("ctrsacct").setValue($.trim(data1.nos));
                        }else if(ret.ctr=="CNY"){//人民币
                        	mini.get("ctrsmeans").setValue($.trim(data1.smeans));
	                    	 mini.get("ctrsmeans").setText($.trim(data1.smeans));
	                         mini.get("ctrsacct").setValue($.trim(data1.sacct)); 
                        }
                        
                        btnEdit.focus();
                    }
                }

            }
        });
		
		
	}
	//货币对改变时发生，给ccy、ctrccy赋值
	function onCPChanged(){
		var currencyPair = mini.get("currencyPair").getText();
		//解析货币对
		var ret = parseCurrencyPair(currencyPair);
		//给ccy赋值
		mini.get("ccy").setValue(ret.ccy);
		//给ctrccy赋值
		mini.get("ctrccy").setValue(ret.ctr);
		
		
	}
	
	
	function onCurrencyPairChange(e){
		var currencyPair = mini.get("currencyPair").getText();
		/***start*******************************/
		var cp = parseCurrencyPair(currencyPair);
		mini.get("ccy").setValue(cp.ccy);//币种1  存放直通opics的主要币种
		mini.get("ctrccy").setValue(cp.ctr);//币种2  存放直通opics的次要币种
		/***end*******************************/
		var arr=new Array();
		arr=currencyPair.split("/");
		var direction = mini.get("direction").getValue();
		if(direction=='P'){
			mini.get("buyCurreny").setValue(arr[0]);
			mini.get("sellCurreny").setValue(arr[1]);
		}else if(direction=='S'){
			mini.get("buyCurreny").setValue(arr[1]);
			mini.get("sellCurreny").setValue(arr[0]);
		}
		changeAcct();
	}
	
	//计算反向交易金额
	function setSellAmount(){
		var buyNotionalAmount = mini.get("buyNotionalAmount").getValue();
		var strikePrice = mini.get("strikePrice").getValue();
		if(buyNotionalAmount!=null && buyNotionalAmount!="" && strikePrice!=null && strikePrice!=""){
			var sellNotionalAmount=CommonUtil.accMul(buyNotionalAmount,strikePrice);
			mini.get("sellNotionalAmount").setValue(sellNotionalAmount);
		}
	}
	function setPrice(){
		var buyNotionalAmount = mini.get("buyNotionalAmount").getValue();
		var sellNotionalAmount = mini.get("sellNotionalAmount").getValue();
		if(buyNotionalAmount!=null && buyNotionalAmount!="" && sellNotionalAmount!=null && sellNotionalAmount!=""){
			var strikePrice=CommonUtil.accDiv(sellNotionalAmount,buyNotionalAmount);
			mini.get("strikePrice").setValue(strikePrice);
		}
	}
	function searchProductWeightNew(){
    	var form = new mini.Form("field_form");
    	var data = form.getData(true);
    	var mDate = data.deliveryDate;
    	searchProductWeight(20,0,prdNo,mDate);
    }
	function onSetTenor(){
		var expiryDate= mini.get("expiryDate").getValue();
		var forDate = mini.get("forDate").getValue();
		var subDate= Math.abs(parseInt((expiryDate.getTime() - forDate.getTime())/1000/3600/24));
		mini.get("tenor").setValue(subDate);
	}
	function showOrHideNetting(val){
		   var nettingStatus=mini.get("nettingStatus").getValue();
		   if(val!=null){
			   nettingStatus=val;
		   }
		   if(nettingStatus=='1'){
			   mini.get("isNetting").show();
			   mini.get("isNoNetting").hide();
			   mini.get("nettingAddr").setValue("上海清算所净额清算.外汇中央对手净额清算");
		   }else{
			   mini.get("isNetting").hide();
			   mini.get("isNoNetting").show();
		   }
	   }
	function changeAcct(){
		   var buyCurreny=mini.get("buyCurreny").getValue();
		   var sellCurreny=mini.get("sellCurreny").getValue();
		   $("#bankBic1").show();
		   $("#intermediaryBankName1").show();
		   $("#intermediaryBankBicCode1").show();
		   $("#bankBic2").show();
		   $("#intermediaryBankName2").show();
		   $("#intermediaryBankBicCode2").show();
		   if(buyCurreny=="CNY"){
			   mini.get("bankName1").setLabel("资金账户户名：");
			   mini.get("dueBank1").setLabel("资金开户行：");
			   mini.get("dueBankName1").setLabel("支付系统行号：");
			   mini.get("dueBankAccount1").setLabel("资金账号：");
			   $("#bankBic1").hide();
			   $("#intermediaryBankName1").hide();
			   $("#intermediaryBankBicCode1").hide();
			   
		   }else if(sellCurreny=="CNY"){
			   mini.get("bankName2").setLabel("资金账户户名：");
			   mini.get("dueBank2").setLabel("资金开户行：");
			   mini.get("dueBankName2").setLabel("支付系统行号：");
			   mini.get("dueBankAccount2").setLabel("资金账号：");
			   $("#bankBic2").hide();
			   $("#intermediaryBankName2").hide();
			   $("#intermediaryBankBicCode2").hide();
		   }
	   }
	   
	function getLossLimit(e){
		var param = mini.encode({'dealno' : dealNo,'prdNo':prdNo}); 
		CommonUtil.ajax({
		    url: "/IfsLimitController/getLossLimit",
		    data : param,
		    callback:function (data) {
		    	if (data != null && data != "" && data !="0" && data !="null") {
		    		mini.get("profitLoss").setValue(data);
				} else {
					mini.get("profitLoss").setValue();
		    	}
		    }
		});
	}
	</script>
	
	<script type="text/javascript" src="../../Common/Flow/MiniApproveOpCommon.js"></script>			
</body>
</html>