<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
<title>外币头寸调拨</title>
<script type="text/javascript" >
	/**prdCode用于页面投资组合的选择   该全局变量必须为prdCode*/
	var prdCode="";
</script>
</head>
<body style="width: 100%; height: 100%; background: white">
	<fieldset class="mini-fieldset">
		<legend>查询</legend>
		<div id="search_form" style="width: 100%;">
		<input id="contractId" name="contractId" class="mini-textbox" labelField="true" width="280px" label="成交单编号：" labelStyle="text-align:right;"  labelStyle="width:120px" emptyText="请输入成交单编号"/> 
			<input id="tradeDate" name="tradeDate" class="mini-datepicker" labelField="true"  label="成交日期："   width="280px" labelStyle="text-align:right;" labelStyle="width:120px" emptyText="请输入成交日期" />
			<input id="approveStatus" name="approveStatus" class="mini-combobox" data="CommonUtil.serverData.dictionary.ApproveSta" width="280px" emptyText="请选择审批状态" labelField="true"  label="审批状态：" labelStyle="text-align:right;"/>
			<span style="float: right; margin-right: 100px"> 
				<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
			</span>
		</div>
	</fieldset>
	<%@ include file="../batchReback.jsp"%>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
	 
	<div id="MiniSettleForeignSpot" class="mini-fit" style="margin-top: 2px;">
		<div id="datagrid" class="mini-datagrid borderAll" style="width: 100%; height: 50%;" allowAlternating="true"  multiSelect="true"
			allowResize="true" onrowdblclick="onRowDblClick" border="true" sortMode="client">
			<div property="columns">
			<div type="checkcolumn"></div>
				<div type="indexcolumn" headerAlign="center" width="40">序号</div>
				<div field="prdNo" width="100" allowSort="false" headerAlign="center" align="center" visible="false"></div>
				<div field="amount" width="180" allowSort="false" headerAlign="center" align="right" numberFormat="#,0.0000">Amount</div>
				<div field="discount" width="200" allowSort="true" headerAlign="center" align="right" numberFormat="#,0.00000000">Discount</div>
				<div field="cal" width="100" allowSort="false" headerAlign="center" align="center">Cal</div>
				<div field="ccy" width="100" allowSort="false" headerAlign="center" align="center">Ccy</div>
				<div field="payDate" width="150" allowSort="false" headerAlign="center" align="center">Pay date</div>
				<div field="rule" width="100" allowSort="false" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'AllotRule'}">Rule</div>
				<div field="contractId" width="200" allowSort="false" headerAlign="center" align="">成交单编号</div>
				<div field="approveStatus" width="120" align="center" headerAlign="center" align="center"  renderer="CommonUtil.dictRenderer" data-options="{dict:'ApproveStatus'}">审批状态</div>
				<div field="dealTransType" width="100" allowSort="true" headerAlign="center"  align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'dealTransType'}">交易状态</div>
				<div field="sponsor" width="100" allowSort="false" headerAlign="center" align="center">审批发起人</div>
				<div field="sponInst" width="100" allowSort="false" headerAlign="center" align="center">审批发起机构</div>
				<div field="aDate" width="180" allowSort="false" headerAlign="center" align="center">审批发起时间</div>
				<div field="ticketId" width="200" allowSort="false" headerAlign="center" align="center">审批单号</div>
			</div>
		</div>
		<fieldset style="width: 100%;border:solid 1px #aaa;margin-top:8px;position:relative;">
		<legend>外币头寸调拨详情</legend>
			<div id="MiniSettleForeigDetail" style="padding:5px;" class="mini-fit area" tyle="width: 100%; height: 50%;" allowAlternating="true" allowResize="true" border="true" sortMode="client">
			<input class="mini-hidden" name="id"/>
			<%@ include file="../../Common/opicsDetail.jsp"%>
				<div class="mini-panel" title="交易主体信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
		<div class="leftarea">
			<input id="type" name="type" class="mini-combobox" labelField="true"  label="Type" data="CommonUtil.serverData.dictionary.AllotType"  labelStyle="text-align:left;width:130px;" style="width:100%;"/>
			<input id="payDate" name="payDate" class="mini-datepicker" labelField="true"  label="Pay date"  labelStyle="text-align:left;width:130px;"  style="width:100%;" />
			<input id="amount" name="amount" class="mini-spinner" labelField="true"  label="Amount" changeOnMousewheel='false'  labelField="true"  format="n4" maxValue="999999999999999"   labelStyle="text-align:left;width:130px;" style="width:100%;"/>
			<input id="discount" name="discount" class="mini-spinner" labelField="true"  label="Discount" changeOnMousewheel='false'  labelField="true"  format="n8" maxValue="999999999999999"  labelStyle="text-align:left;width:130px;"  style="width:100%;" />
			<input id="relatedTrade" name="relatedTrade" class="mini-textbox" labelField="true"  label="Related trade"  labelStyle="text-align:left;width:130px;"  maxLength="15" style="width:100%;" />
			<input id="callAccount" name="callAccount" class="mini-textbox" labelField="true"  label="Call account"  labelStyle="text-align:left;width:130px;"  style="width:100%;" maxLength="15" />
			<input id="broker" name="broker" class="mini-textbox" labelField="true"  label="Broker"  labelStyle="text-align:left;width:130px;"  style="width:100%;"  maxLength="15"/>
		</div>
		<div class="rightarea">
			<input id="cal" name="cal" class="mini-textbox" labelField="true"  label="Cal"  labelStyle="text-align:left;width:130px;" maxLength="15"   style="width:100%;"/>
			<input id="ccy" name="ccy" class="mini-combobox" labelField="true"  label="Ccy" data="CommonUtil.serverData.dictionary.Currency" labelStyle="text-align:left;width:130px;"  style="width:100%;" />
        	<input id="rule" name="rule" class="mini-combobox" labelField="true"  label="Rule" data="CommonUtil.serverData.dictionary.AllotRule" labelStyle="text-align:left;width:130px;"   style="width:100%;" />
			<input id="payType" name="payType" class="mini-textbox" labelField="true"  label="Pay type" labelStyle="text-align:left;width:130px;"  maxLength="15" style="width:100%;" />
			<input id="relextid" name="relextid" class="mini-textbox" labelField="true"  label="Rel ext ID"  labelStyle="text-align:left;width:130px;"  style="width:100%;"  maxLength="15"/>
			<input id="cpty" name="cpty" class="mini-textbox" labelField="true"  label="Cpty"  labelStyle="text-align:left;width:130px;"  style="width:100%;"  maxLength="15"/>
		</div>
	</div>		
			</div>
	</fieldset>
	</div>
	<script>
		mini.parse();

		var url = window.location.search;
		var prdNo = CommonUtil.getParam(url, "prdNo");
		var prdName = CommonUtil.getParam(url, "prdName");
		var grid = mini.get("datagrid");
		/**************************************点击下面显示详情开始******************************/
		var from = new mini.Form("MiniSettleForeigDetail");
		from.setEnabled(false);
		var grid = mini.get("datagrid");
		//grid.load();
		 //绑定表单
	    var db = new mini.DataBinding();
	    db.bindForm("MiniSettleForeigDetail", grid);
	    /**************************************点击下面显示详情结束******************************/
		grid.on("beforeload", function(e) {
			e.cancel = true;
			var pageIndex = e.data.pageIndex;
			var pageSize = e.data.pageSize;
			search(pageSize, pageIndex);
		});
		grid.on("select",function(e){
			var row=e.record;
			if(row.approveStatus == "3"){//新建
				mini.get("approve_mine_commit_btn").setEnabled(true);
				mini.get("approve_log").setEnabled(false);
				mini.get("approve_commit_btn").setEnabled(false);
				mini.get("edit_btn").setEnabled(true);
				mini.get("delete_btn").setEnabled(true);
			}
			if( row.approveStatus == "4" || row.approveStatus == "5"){//待审批：4   审批中:5
				mini.get("approve_mine_commit_btn").setEnabled(false);
				mini.get("approve_commit_btn").setEnabled(true);
				mini.get("edit_btn").setEnabled(false);
				mini.get("delete_btn").setEnabled(false);
			}
			if( row.approveStatus == "6" ){//审批完成：6
				mini.get("approve_mine_commit_btn").setEnabled(false);
				mini.get("approve_commit_btn").setEnabled(true);
				mini.get("edit_btn").setEnabled(false);
				mini.get("delete_btn").setEnabled(false);
			}
			if(row.approveStatus != null && row.approveStatus != "3") {
				mini.get("approve_log").setEnabled(true);
			}
			if(row.dealTransType=="N"){
				mini.get("approve_mine_commit_btn").setEnabled(false);
				mini.get("approve_commit_btn").setEnabled(false);
				mini.get("edit_btn").setEnabled(false);
				mini.get("delete_btn").setEnabled(false);
				mini.get("approve_log").setEnabled(false);
				mini.get("print_bk_btn").setEnabled(false);
				mini.get("reback_btn").setEnabled(false);
			}
			if(row.dealTransType!="N"){
				mini.get("reback_btn").setEnabled(true);
			}
			if(row.approveStatus == "3"){
				mini.get("reback_btn").setEnabled(false);
			}
		});

		function search(pageSize,pageIndex){
			var form = new mini.Form("#search_form");
			form.validate();
			if(form.isValid()==false){
				mini.alert("信息填写有误，请重新填写","系统提示");
				return;
			}

			var data=form.getData(true);
			data['pageNumber']=pageIndex+1;
			data['pageSize']=pageSize;
			data['branchId']=branchId;
			var url=null;

			var approveType = mini.get("approveType").getValue();
			if(approveType == "mine"){
				url = "/IfsCurrencyController/searchPageAllocateMine";
			}else if(approveType == "approve"){
				url = "/IfsCurrencyController/searchPageAllocateUnfinished";
			}else{
				url = "/IfsCurrencyController/searchPageAllocateFinished";
			}

			var params = mini.encode(data);
			CommonUtil.ajax({
				url:url,
				data:params,
				callback : function(data) {
					grid.setTotalCount(data.obj.total);
					grid.setPageIndex(pageIndex);
			        grid.setPageSize(pageSize);
					grid.setData(data.obj.rows);
				}
			});
		}
		//增删改查
		function add(){
			var url = CommonUtil.baseWebPath() + "/cfetsfx/ccyPositionAllocationEdit.jsp?action=add&prdNo"+prdNo;
			var tab = { id: "MiniAllcationAdd", name: "MiniAllcationAdd", title: "外币头寸调拨补录", url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
			var paramData = {selectData:""};
			CommonUtil.openNewMenuTab(tab,paramData);
		}
		function query() {
            search(grid.pageSize, 0);
        }
		
		function getData(action) {
			var row = null;
			if (action != "add") {
				row = grid.getSelected();
			}
			return row;
		}
		
		function clear(){
			var form=new mini.Form("search_form");
            form.clear();
            search(10,0);

		}
		function edit()
		{
			var row = grid.getSelected();
			if(row){
				var url = CommonUtil.baseWebPath() + "/cfetsfx/ccyPositionAllocationEdit.jsp?action=edit&ticketid="+row.ticketId;
				var tab = { id: "MiniAllcationEdit", name: "MiniAllcationEdit", title: "外币头寸调拨修改", url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
				var paramData = {selectData:""};
				CommonUtil.openNewMenuTab(tab,paramData);

			} else {
				mini.alert("请选中一条记录！","消息提示");
			}
		}

		function onRowDblClick(e) {
			var grid = mini.get("datagrid");
			var row = grid.getSelected();
			if(row){
					var url = CommonUtil.baseWebPath() + "/cfetsfx/ccyPositionAllocationEdit.jsp?action=detail&ticketid="+row.ticketId;
					var tab = { id: "MiniAllcationDetail", name: "MiniAllcationDetail", title: "外币头寸调拨详情", url: url ,showCloseButton:true};
					var paramData = {selectData:""};
					CommonUtil.openNewMenuTab(tab,paramData);

			} else {
				mini.alert("请选中一条记录！","消息提示");
			}
		}
		//删除
		function del() {
				var grid = mini.get("datagrid");
				var row = grid.getSelected();
				if (row) {
					mini.confirm("您确认要删除选中记录?","系统警告",function(value){
						if(value=="ok"){
						CommonUtil.ajax({
							url: "/IfsCurrencyController/deleteIfsAllocate",
							
							data: {ticketid: row.ticketId},
							callback: function (data) {
								if (data.code == 'error.common.0000') {
									mini.alert("删除成功");
									grid.reload();
									//search(grid.pageSize,grid.pageIndex);
								} else {
									mini.alert("删除失败");
								}
							}
						});
						}
					});
				}
				else {
					mini.alert("请选中一条记录！", "消息提示");
				}

			}
		
		/**************************审批相关****************************/
		//审批日志查看
		function appLog(selections){
			var flow_type = Approve.FlowType.VerifyApproveFlow;
			if(selections.length <= 0){
				mini.alert("请选择要操作的数据","系统提示");
				return;
			}
			if(selections.length > 1){
				mini.alert("系统不支持多笔操作","系统提示");
				return;
			}
			if(selections[0].tradeSource == "3"){
				mini.alert("初始化导入的业务没有审批日志","系统提示");
				return false;
			}
			Approve.approveLog(flow_type,selections[0].ticketId);
		};
		//提交正式审批、待审批
		function verify(selections){
			var grid = mini.get("datagrid");
			var row = grid.getSelected();
			if(selections.length == 0){
				mini.alert("请选中一条记录！","消息提示");
				return false;
			}else if(selections.length > 1){
				mini.alert("该功能不支持多笔提交，如需多笔提交请点击[批量提交]按钮","系统提示");
				return false;
			}
			if(selections[0]["approveStatus"] != "3" ){
				var url = CommonUtil.baseWebPath() +"/cfetsfx/ccyPositionAllocationEdit.jsp?action=approve&ticketid="+row.ticketId;
				var tab = {"id": "AdvanceAllcationApprove",name:"AdvanceAllcationApprove",url:url,title:"外币头寸调拨审批",
							parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
				var paramData = {selectData:selections[0]};
				CommonUtil.openNewMenuTab(tab,paramData);
				// top["win"].showTab(tab);
			}else{
				Approve.approveCommit(Approve.FlowType.VerifyApproveFlow,selections[0]["ticketId"],Approve.OrderStatus.New,"IfsAllocateService",prdNo,function(){
					search(grid.pageSize,grid.pageIndex);
				});
			}
		};
		//审批
		function approve(){
			mini.get("approve_commit_btn").setEnabled(false);
			var messageid = mini.loading("系统正在处理...", "请稍后");
			try {
				verify(grid.getSelecteds());
			} catch (error) {
				
			}
			mini.hideMessageBox(messageid);	
			mini.get("approve_commit_btn").setEnabled(true);
		}
		//提交审批
		function commit(){
			mini.get("approve_mine_commit_btn").setEnabled(false);
			var messageid = mini.loading("系统正在处理...", "请稍后");
			try {
				verify(grid.getSelecteds());
			} catch (error) {
				
			}
			mini.hideMessageBox(messageid);	
			mini.get("approve_mine_commit_btn").setEnabled(true);
		}
		//审批日志
		function searchlog(){
			appLog(grid.getSelecteds());
		}
		//打印
	    function print(){
			var selections = grid.getSelecteds();
			if(selections == null || selections.length == 0){
				mini.alert('请选择一条要打印的数据！','系统提示');
				return false;
			}else if(selections.length>1){
				mini.alert('暂不支持多条数据同时打印，请选择一条要打印的数据！','系统提示');
				return false;
			}
			var canPrint = selections[0].ticketId;
			
			if(!CommonUtil.isNull(canPrint)){
				var actionStr = CommonUtil.pPath + "/sl/IfsPrintUtilController/exportload/print/"+PrintNo.ccyPositionAllocation+"/" + canPrint;
				$('<form target="_blank" action=' + actionStr + ' method="post"></form>').appendTo('body').submit().remove();
			};
		}
		$(document).ready(function() {
			//控制按钮显示
			$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
				initButton();
				search(10, 0);
			});
		});
	</script>
</body>
</html>