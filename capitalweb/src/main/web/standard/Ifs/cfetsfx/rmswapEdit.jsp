<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../../global.jsp" %>
<html>
<head>
    <title></title>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
    <script type="text/javascript" src="<%=basePath%>/standard/Common/RiskCenter.js"></script>
    <script type="text/javascript" src="<%=basePath%>/standard/uploadAndDownload.js"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/miniui/res/ajaxfileupload.js"></script>
    <script type="text/javascript" src="../cfetsrmb/rmbVerify.js"></script>
    <script type="text/javascript">
        /**prdCode用于页面投资组合的选择   该全局变量必须为prdCode*/
        var prdCode = "FXD-SWAP";
    </script>
</head>
<body style="width:100%;height:100%;background:white">
<div class="mini-splitter" style="width:100%;height:100%;" id="splitter">
    <div size="90%" showCollapseButton="false">
        <h1 style="text-align:center;padding-top:5px;font-size:18px;"><strong>外汇掉期</strong></h1>
        <div id="field_form" class="mini-fit area" style="background:white">
            <input id="sponsor" name="sponsor" class="mini-hidden"/>
            <input id="sponInst" name="sponInst" class="mini-hidden"/>
            <input id="aDate" name="aDate" class="mini-hidden"/>
            <input id="ticketId" name="ticketId" class="mini-hidden"/>
            <div class="mini-panel" title="成交单编号" style="width:100%;" allowResize="true" collapseOnTitleClick="false">
                <div class="leftarea">
                    <input style="width:100%;" id="contractId" name="contractId" class="mini-textbox mini-mustFill"
                           labelField="true" requiredErrorText="该输入项为必输项" label="成交单编号："
                           labelStyle="text-align:left;width:130px;" required="true" vtype="maxLength:35"
                           onvalidation="onEnglishAndNumberValidation"/>
                </div>
                <div class="rightarea">
                    <input style="width:100%;" id="cfetscn" name="cfetscn" class="mini-textbox" label="CFETS机构码："
                           labelField="true" visible="false" enabled="false" labelStyle="text-align:left;width:130px;"/>
                </div>
            </div>
            <div class="mini-panel" title="成交双方信息" style="width:100%" allowResize="true" collapseOnTitleClick="false">
                <div class="leftarea">
                    <input id="instId" name="instId" class="mini-textbox mini-mustFill" labelField="true" label="本方机构："
                           style="width:100%;" labelStyle="text-align:left;width:130px" requiredErrorText="该输入项为必输项"
                           required="true" vtype="maxLength:16"/>
                    <input id="dealer" name="dealer" class="mini-textbox mini-mustFill" labelField="true" label="本方交易员："
                           labelStyle="text-align:left;width:130px" style="width:100%;" requiredErrorText="该输入项为必输项"
                           required="true" vtype="maxLength:20"/>
                    <input id="forDate" name="forDate" class="mini-datepicker mini-mustFill" labelField="true"
                           requiredErrorText="该输入项为必输项" required="true" label="交易日期："
                           labelStyle="text-align:left;width:130px" style="width:100%;"/>
                </div>
                <div class="rightarea">
                    <input id="counterpartyInstId" name="counterpartyInstId" class="mini-buttonedit mini-mustFill"
                           requiredErrorText="该输入项为必输项" required="true" onbuttonclick="onButtonEdit" labelField="true"
                           allowInput="false" label="对方机构：" style="width:100%;" labelStyle="text-align:left;width:130px"
                           vtype="maxLength:16"/>
                    <input id="counterpartyDealer" name="counterpartyDealer" class="mini-textbox" labelField="true" label="对方交易员："
                           labelStyle="text-align:left;width:130px" style="width:100%;" vtype="maxLength:20"/>
                    <input id="forTime" name="forTime" class="mini-timespinner" labelField="true" label="交易时间："
                           labelStyle="text-align:left;width:130px" style="width:100%;"/>
                </div>
            </div>
            <div class="mini-panel" title="交易主体信息" style="width:100%" allowResize="true" collapseOnTitleClick="false">
                <div class="leftarea">
                    <input id="tradingModel" name="tradingModel" class="mini-combobox mini-mustFill" labelField="true"
                           requiredErrorText="该输入项为必输项" required="true" label="交易模式："
                           labelStyle="text-align:left;width:130px" style="width:100%;"
                           data="CommonUtil.serverData.dictionary.TradeModel"/>
                    <input style="width:100%;" id="currencyPair" name="currencyPair" class="mini-combobox mini-mustFill"
                           labelField="true" label="货币对：" requiredErrorText="该输入项为必输项" required="true"
                           labelStyle="text-align:left;width:130px" data="CommonUtil.serverData.dictionary.CurrencyPair"
                           onvaluechanged="onCurrencyPairChange"/>
                    <fieldset>
                        <legend>近端明细</legend>
                        <input style="width:100%;" id="nearSpread" name="nearSpread"
                               class="mini-spinner mini-mustFill input-text-strong" labelField="true" label="近端点："
                               changeOnMousewheel='false' format="n6" requiredErrorText="该输入项为必输项" required="true"
                               labelStyle="text-align:left;width:130px" onvaluechanged="getReversePrice" minValue="-1000000"
                               maxValue="1000000" value="0"/>
                        <input style="width:100%;" id="nearRate" name="nearRate"
                               class="mini-spinner mini-mustFill input-text-strong" labelField="true" label="近端成交汇率："
                               requiredErrorText="该输入项为必输项" required="true" format="n6" changeOnMousewheel="false"
                               maxValue="99999.999999" labelStyle="text-align:left;width:130px" enabled="false"/>
                        <input style="width:100%;" id="direction" name="direction" class="mini-combobox mini-mustFill"
                               labelField="true" label="近端交易方向：" requiredErrorText="该输入项为必输项" required="true"
                               labelStyle="text-align:left;width:130px" data="CommonUtil.serverData.dictionary.Swaptrading"
                               onvaluechanged="onCurrencyPairChange"/>
                        <input style="width:100%;" id="opicsccy" name="opicsccy" class="mini-combobox mini-mustFill"
                               labelField="true" label="近端交易货币：" requiredErrorText="该输入项为必输项" required="true"
                               labelStyle="text-align:left;width:130px;" onvaluechanged="onCurrencyChange"/>
                        <input style="width:100%;" id="nearPrice" name="nearPrice"
                               class="mini-spinner mini-mustFill input-text-strong" labelField="true" label="近端交易金额1："
                               requiredErrorText="该输入项为必输项" required="true" maxValue="99999999999999.9999"
                               onvalidation="zeroValidation" changeOnMousewheel='false' format="n4"
                               labelStyle="text-align:left;width:130px" onblur="getReversePrice"/>
                        <input style="width:100%;" id="opicsctrccy" name="opicsctrccy" class="mini-combobox mini-mustFill"
                               labelField="true" label="近端对应货币：" requiredErrorText="该输入项为必输项" required="true"
                               labelStyle="text-align:left;width:130px;"/>
                        <input style="width:100%;" id="nearReversePrice" name="nearReversePrice"
                               class="mini-spinner mini-mustFill input-text-strong" labelField="true" label="近端交易金额2："
                               requiredErrorText="该输入项为必输项" required="true" maxValue="99999999999999.999999"
                               changeOnMousewheel='false' format="n6" labelStyle="text-align:left;width:130px"
                               onvalidation="zeroValidation"/>
                        <input style="width:100%" id="tenorCode" name="tenorCode" class="mini-textbox"labelField="true" label="CFETS近端期限代码："
                               labelStyle="text-align:left;width:130px;" enabled="false"/>
                        <input style="width:100%;" id="nearValuedate" name="nearValuedate"
                               class="mini-datepicker mini-mustFill" labelField="true" label="近端起息日："
                               requiredErrorText="该输入项为必输项" required="true" ondrawdate="onDrawDateNear"
                               onvaluechanged="associaDateCalculation" labelStyle="text-align:left;width:130px"/>
                    </fieldset>
                    <input style="width:100%;" id="tenor" name="tenor" class="mini-spinner" labelField="true"
                           label="期限：" requiredErrorText="该输入项为必输项" enabled="false" maxValue="99999"
                           changeOnMousewheel='false' labelStyle="text-align:left;width:130px"/>
                </div>
                <div class="rightarea">
                    <input id="tradingType" name="tradingType" class="mini-combobox mini-mustFill" labelField="true"
                           label="交易方式：" requiredErrorText="该输入项为必输项" required="true" value="RFQ"
                           labelStyle="text-align:left;width:130px" style="width:100%;"
                           data="CommonUtil.serverData.dictionary.TradeType"/>
                    <input style="width:100%;" id="price" name="price"
                           class="mini-spinner mini-mustFill input-text-strong" labelField="true" label="SPOT汇率："
                           format="n6" maxValue="99999999999.999999" onvalidation="zeroValidation"
                           changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"
                           onvaluechanged="getReversePrice" requiredErrorText="该输入项为必输项" required="true"/>
                    <fieldset>
                        <legend>远端明细</legend>
                        <input style="width:100%;" id="spread" name="spread"
                               class="mini-spinner mini-mustFill input-text-strong" labelField="true" label="远端点："
                               changeOnMousewheel='false' format="n6" requiredErrorText="该输入项为必输项" required="true"
                               labelStyle="text-align:left;width:130px" onvaluechanged="getReversePrice" minValue="-1000000"
                               maxValue="1000000" value="0"/>
                        <input style="width:100%;" id="fwdRate" name="fwdRate"
                               class="mini-spinner mini-mustFill input-text-strong" labelField="true" label="远端成交汇率："
                               format="n6" changeOnMousewheel="false" requiredErrorText="该输入项为必输项" required="true"
                               maxValue="99999.999999" labelStyle="text-align:left;width:130px"
                               data="CommonUtil.serverData.dictionary.trading" enabled="false"/>
                        <input style="width:100%;" id="directionfwd" name="directionfwd" class="mini-combobox"
                               labelField="true" label="远端交易方向："
                               labelStyle="text-align:left;width:130px" data="CommonUtil.serverData.dictionary.Swaptrading" enabled="false" />
                        <input style="width:100%;" id="opicsccyfwd" name="opicsccyfwd" class="mini-combobox"
                               labelField="true" label="远端交易货币："
                               labelStyle="text-align:left;width:130px;" enabled="false"/>
                        <input style="width:100%;" id="fwdPrice" name="fwdPrice"
                               class="mini-spinner mini-mustFill input-text-strong" labelField="true" label="远端交易金额1："
                               requiredErrorText="该输入项为必输项" required="true" maxValue="99999999999999.9999"
                               onvalidation="zeroValidation" changeOnMousewheel='false' format="n4"
                               labelStyle="text-align:left;width:130px" onvaluechanged="getFwdReversePrice"
                               onvalidation="zeroValidation" enabled="false"/>
                        <input style="width:100%;" id="opicsctrccyfwd" name="opicsctrccyfwd" class="mini-combobox"
                               labelField="true" label="远端对应货币："
                               labelStyle="text-align:left;width:130px;" enabled="false"/>
                        <input style="width:100%;" id="fwdReversePrice" name="fwdReversePrice"
                               class="mini-spinner mini-mustFill input-text-strong" labelField="true" label="远端交易金额2："
                               maxValue="99999999999999.999999" requiredErrorText="该输入项为必输项" required="true"
                               changeOnMousewheel='false' format="n6" labelStyle="text-align:left;width:130px"
                               onvalidation="zeroValidation"/>
                        <input style="width:100%" id="fwdTenorCode" name="fwdTenorCode" class="mini-textbox"labelField="true" label="CFETS远端期限代码："
                               labelStyle="text-align:left;width:130px;" enabled="false"/>
                        <input style="width:100%;" id="fwdValuedate" name="fwdValuedate"
                               class="mini-datepicker mini-mustFill" labelField="true" label="远端起息日："
                               labelStyle="text-align:left;width:130px" requiredErrorText="该输入项为必输项"
                               ondrawdate="onDrawDateFar" onvaluechanged="associaDateCalculation" required="true"/>
                    </fieldset>
                </div>
            </div>
            <div class="mini-panel" title="报表信息" style="width:100%" allowResize="true" collapseOnTitleClick="false">
                <div class="leftarea">
                    <input id="dealTransType" name="dealTransType" class="mini-combobox" labelField="true"
                           style="width:100%;" label="数据来源：" data="CommonUtil.serverData.dictionary.dealTransType1"
                           vtype="maxLength:10" labelStyle="text-align:left;width:130px;" value="1">
                    <input id="nettingStatus" name="nettingStatus" class="mini-combobox mini-mustFill" required="true"
                           labelField="true" label="净额清算状态：" labelStyle="text-align:left;width:130px;"
                           style="width:100%;" data="CommonUtil.serverData.dictionary.YesNo" showNullItem="true"/>
                    <input id="note" name="note" class="mini-textbox" labelField="true" label="交易事由："
                           labelStyle="text-align:left;width:130px;" style="width:100%;" vtype="maxLength:16"/>
                    <!-- 			<input id="delaydElivind" name="delaydElivind" class="mini-combobox" labelField="true"   label="延期交易：" labelStyle="text-align:left;width:130px;" style="width:100%;" data="CommonUtil.serverData.dictionary.delaydElivind" value='0'/> -->
                    <input id="attributionDept" name="attributionDept" valueField="id" showFolderCheckBox="true"
                           labelField="true" label="归属机构：" required="true" showCheckBox="true" style="width:100%"
                           labelStyle="text-align:left;width:130px"
                           showTreeLines="true" showTreeIcon="true" textField="instName" parentField="parentId"
                           class="mini-treeselect mini-mustFill" expandOnLoad="true" resultAsTree="false"
                           valueFromSelect="true" emptyText="请选择机构"/>
                </div>
                <div class="rightarea">
                    <input id="transKind" name="transKind" class="mini-combobox mini-mustFill" labelField="true"
                           requiredErrorText="该输入项为必输项" required="true" label="购售业务种类：" value="0001"
                           labelStyle="text-align:left;width:130px;" style="width:100%;"
                           data="CommonUtil.serverData.dictionary.transCode"/>
                    <input id="purposeCode" name="purposeCode" class="mini-combobox mini-mustFill" labelField="true"
                           requiredErrorText="该输入项为必输项" required="true" label="购售用途：" value="1001"
                           labelStyle="text-align:left;width:130px;" style="width:100%;"
                           data="CommonUtil.serverData.dictionary.purposeType"/>
                    <input id="fiveLevelClass" name="fiveLevelClass" class="mini-combobox" labelField="true"
                           requiredErrorText="该输入项为必输项" label="五级分类：" value="0"
                           labelStyle="text-align:left;width:130px;" style="width:100%;"
                           data="CommonUtil.serverData.dictionary.FiveLevelClass"/>
                </div>
            </div>
            <%@ include file="../../Common/opicsLess.jsp" %>
            <%@ include file="../../Common/custLimit.jsp" %>
            <%@ include file="./accountInforSwap.jsp" %>
            <%@ include file="./uploadFile.jsp" %>

            <%@ include file="../../Common/Flow/MiniApproveOpCommon.jsp" %>
            <%@ include file="../../Common/print/approveFlowLog.jsp" %>

        </div>
    </div>
    <div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
        <div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"
                                                                style="width:120px;" id="save_btn"
                                                                onclick="save">保存交易</a></div>
        <div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"
                                                                style="width:120px;" id="close_btn"
                                                                onclick="close">关闭界面</a></div>
    </div>
</div>

<script type="text/javascript">
    mini.parse();
    var detailData;

    var url = window.location.search;
    var action = CommonUtil.getParam(url, "action");
    var ticketId = CommonUtil.getParam(url, "ticketid");
    var prdNo = CommonUtil.getParam(url, "prdNo");
    var fPrdCode = CommonUtil.getParam(url, "fPrdCode");
    var prdName = CommonUtil.getParam(url, "prdName");
    var dealType = CommonUtil.getParam(url, "dealType");

    var params = {};
    var tradeData = {};
    if (action != "print") {
        //获取当前tab
        var currTab = top["win"].tabs.getActiveTab();
        var params = currTab.params;
        var row = params.selectData;
        tradeData.selectData = row;
        tradeData.operType = action;
        tradeData.serial_no = row.ticketId;
        tradeData.task_id = row.taskId;
    }
    var ApproveFlowLog = {};
    var log_grid = mini.get("log_grid");//隐藏日志信息列表，打印时显示
    log_grid.hide();
    /**
     *   获取审批日志信息
     */
    ApproveFlowLog.loadLogInfo = function () {
        CommonUtil.ajax({
            url: "/IfsFlowController/approveLog",
            data: {'serial_no': ticketId},
            callback: function (data) {
                if (data != null) {
                    log_grid.setData(data.obj);
                    if (typeof (FlowDesigner) != "undefined") {
                        FlowDesigner.addApporveLog(data.obj);
                    }
                }
            }
        });
    };

    var _bizDate = '<%=__bizDate %>';
    mini.get("instId").setEnabled(false);
    mini.get("dealer").setEnabled(false);
    mini.get("opicsctrccy").setEnabled(false);
    mini.get("forDate").setValue("<%=__bizDate %>");

    var timestamp = Date.parse(new Date());
    sys(timestamp);

    function sys(stamp) {
        var time = new Date(stamp);
        var result = "";
        result += CommonUtil.singleNumberFormatter(time.getHours()) + ":";
        result += CommonUtil.singleNumberFormatter(time.getMinutes()) + ":";
        result += CommonUtil.singleNumberFormatter(time.getSeconds());
        mini.get("forTime").setValue(result);
    }

    /**
     *   获取机构
     */
    function loadInstitutionTree() {
        CommonUtil.ajax({
            data: {"branchId": branchId},
            url: "/InstitutionController/searchInstitutions",
            callback: function (data) {
                mini.get("attributionDept").setData(data.obj);
                if (detailData != null && detailData.attributionDept != null) {
                    mini.get("attributionDept").setValue(detailData.attributionDept);
                } else {
                    mini.get("attributionDept").setValue("<%=__sessionInstitution.getInstId()%>");
                }
            }
        });
    }

    function inme() {
        if (action == "detail" || action == "edit" || action == "approve" || action == "print") {
            var from = new mini.Form("field_form");
            var from1 = new mini.Form("ApproveOperate_div");
            CommonUtil.ajax({
                url: '/IfsForeignController/searchrmswap',
                data: {ticketId: ticketId},
                callback: function (text) {
                    detailData = text.obj;
                    from.setData(text.obj);
                    //投资组合
                    mini.get("port").setValue(text.obj.port);
                    mini.get("port").setText(text.obj.port);
                    //成本中心
                    mini.get("cost").setValue(text.obj.cost);
                    mini.get("cost").setText(text.obj.cost);
                    var product = mini.get("product").getValue();
                    if (product == null || product == "") {
                        //产品代码
                        mini.get("product").setValue("FXD");
                        mini.get("product").setText("FXD");
                    } else {
                        //产品代码
                        mini.get("product").setValue(text.obj.product);
                        mini.get("product").setText(text.obj.product);
                    }
                    var prodType = mini.get("prodType").getValue();
                    if (prodType == null || prodType == "") {
                        //产品类型
                        mini.get("prodType").setValue("SW");
                        mini.get("prodType").setText("SW");
                    } else {
                        //产品类型
                        mini.get("prodType").setValue(text.obj.prodType);
                        mini.get("prodType").setText(text.obj.prodType);
                    }
                    mini.get("counterpartyInstId").setValue(text.obj.counterpartyInstId);

                    queryTextName(text.obj.counterpartyInstId);
                    mini.get("instId").setValue(text.obj.instfullname);
                    mini.get("dealer").setValue(text.obj.myUserName);
                    changeAcct();
                    initCurrencyPair();

                    mini.get("opicsccyfwd").setValue(text.obj.opicsccy);
                    mini.get("opicsctrccyfwd").setValue(text.obj.opicsctrccy);
                    if(text.obj.direction == 'P'){
                        mini.get("directionfwd").setValue('S');
                    }else if(text.obj.direction == 'S'){
                        mini.get("directionfwd").setValue('P');
                    }

                    creditsubvalue(text.obj.custNo, "432", null);
                }
            });
            if (action == "detail" || action == "approve") {
                mini.get("save_btn").hide();
                from.setEnabled(false);
                from1.setEnabled(true);
                mini.get("upload_btn").setEnabled(false);
            }
            if (action == "print") {
                from.setEnabled(false);
                $("#fileMsg").hide();
                mini.get("splitter").hidePane(2);

                log_grid.show();
                CommonUtil.ajax({
                    url: "/IfsFlowController/getOneFlowDefineBaseInfo",
                    data: {serial_no: ticketId},
                    callerror: function (data) {
                        ApproveFlowLog.loadLogInfo();
                    },
                    callback: function (data) {
                        var innerInterval;
                        innerInitFunction = function () {
                            clearInterval(innerInterval);
                            ApproveFlowLog.loadLogInfo();
                        },
                            innerInterval = setInterval("innerInitFunction()", 100);
                    }
                });
            }

            mini.get("contractId").setEnabled(false);

        } else if (action == "add") {
            mini.get("dealTransType").setValue("1");
            mini.get("instId").setValue("<%=__sessionInstitution.getInstFullname()%>");
            mini.get("dealer").setValue("<%=__sessionUser.getUserName() %>");
            mini.get("sponsor").setValue("<%=__sessionUser.getUserId()%>");
            mini.get("sponInst").setValue("<%=__sessionUser.getInstId() %>");
            //mini.get("aDate").setValue("<%=__bizDate %>");
            $("#fileMsg").hide();
            //产品代码
            mini.get("product").setValue("FXD");
            mini.get("product").setText("FXD");
            //产品类型
            mini.get("prodType").setValue("SW");
            mini.get("prodType").setText("SW");
        }
        mini.get("dealTransType").setEnabled(false);
        loadInstitutionTree();
    }

    function save() {
        var form = new mini.Form("field_form");
        form.validate();
        if (form.isValid() == false) {
            mini.alert("信息填写有误，请重新填写", "系统提示");
            return;
        }
        var form = new mini.Form("field_form");
        var data = form.getData(true);      //获取表单多个控件的数据
        data['instId'] = "<%=__sessionInstitution.getInstId()%>";
        data['dealer'] = "<%=__sessionUser.getUserId() %>";
        data['sponsor'] = "<%=__sessionUser.getUserId() %>";
        data['sponInst'] = "<%=__sessionUser.getInstId()%>";
        data['fPrdCode']=fPrdCode;
        //data['dealTransType']="1";//修改后的状态永远为1
        var json = mini.encode(data);   //序列化成JSON
        if (toDecimal(data['nearReversePrice']) != toDecimal(data['nearPrice'] * data['nearRate']) || toDecimal(data['fwdReversePrice']) != toDecimal(data['fwdPrice'] * data['fwdRate'])) {
            mini.confirm("确认以当前数据为准吗？", "确认", function (actions) {
                if (actions != "ok") {
                    return;
                }
                CommonUtil.ajax({
                    url: action == "add" ? "/IfsForeignController/addrmswap" : "/IfsForeignController/editrmswap",
                    data: json,
                    callback: function (data) {
                        mini.alert(data.desc, '提示信息', function () {//提示弹框并执行回调
                            if (data.code == 'error.common.0000') {
                                top["win"].closeMenuTab();//关闭并刷新当前界面
                            }
                        });
                    }
                });
            });
        } else {
            CommonUtil.ajax({
                url: action == "add" ? "/IfsForeignController/addrmswap" : "/IfsForeignController/editrmswap",
                data: json,
                callback: function (data) {
                    mini.alert(data.desc, '提示信息', function () {//提示弹框并执行回调
                        if (data.code == 'error.common.0000') {
                            top["win"].closeMenuTab();//关闭并刷新当前界面
                        }
                    });
                }
            });
        }
    }

    function onButtonEdit(e) {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Common/CustMini.jsp",
            title: "选择对手方列表",
            width: 900,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.cno);
                        btnEdit.setText(data.cliname);
                        creditsubvalue(data.cno, "432", null);
                        btnEdit.focus();
                    }
                }

            }
        });
    }

    function onCreditTypeChange() {
        var cno = mini.get("counterpartyInstId").getValue();
        creditsubvalue(cno, "432", null);
    }

    //根据交易对手编号查询全称
    function queryTextName(cno) {
        CommonUtil.ajax({
            url: "/IfsOpicsCustController/searchIfsOpicsCust",
            data: {'cno': cno},
            callback: function (data) {
                if (data && data.obj) {
                    mini.get("counterpartyInstId").setText(data.obj.cliname);
                } else {
                    mini.get("counterpartyInstId").setText();
                }
            }
        });
    }

    function close() {
        top["win"].closeMenuTab();
    }

    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
            inme();
            loadInstitutionTree();
        });
    });

    //==========================JS计算============================
    //1.根据交易币种-货币对计算对应币种金额
    function getReversePrice(e) {
        var price = mini.get("price").getValue(); // SPOT汇率
        var nearPrice = mini.get("nearPrice").getValue();// 近端交易金额1
        var nearRate = mini.get("nearRate").getValue();// 近端成交汇率
        var nearSpread = mini.get("nearSpread").getValue();// 近端点
        var currencyPair = mini.get("currencyPair").getValue();
        var fwdPrice = mini.get("fwdPrice").getValue();// 远端交易金额
        var fwdRate = mini.get("fwdRate").getValue();// 远端成交汇率
        var spread = mini.get("spread").getValue();// 远端点差
        var opicsccy = mini.get("opicsccy").getValue();//交易货币
        var arr = new Array();
        arr = currencyPair.split(".");//货币对1

        nearRate = CommonUtil.accAdd(price, nearSpread); // 赋值近端成交汇率
        mini.get("nearRate").setValue(nearRate);
        fwdRate = CommonUtil.accAdd(price, spread);
        mini.get("fwdRate").setValue(fwdRate);// 赋值远端成交汇率
        if (nearPrice != null && nearPrice != "") {
            mini.get("fwdPrice").setValue(currencyPair.indexOf("JPY") != -1 ? CommonUtil.fomatFloat(nearPrice, 0) : nearPrice);
        }
        if (arr[0] == opicsccy) { //交易货币为分子
            // 计算近端金额
            if (nearPrice != null && nearPrice != "" && nearRate != null && nearRate != "") {
                var nearReversePrice = CommonUtil.accMul(nearPrice, nearRate);
                mini.get("nearReversePrice").setValue(currencyPair.indexOf("JPY") != -1 ? CommonUtil.fomatFloat(nearReversePrice, 0) : nearReversePrice);
            }
            // 计算远端金额
            if (fwdPrice != null && fwdPrice != "" && fwdRate != null && fwdRate != "") {
                var fwdReversePrice = CommonUtil.accMul(fwdPrice, fwdRate);
                mini.get("fwdReversePrice").setValue(currencyPair.indexOf("JPY") != -1 ? CommonUtil.fomatFloat(fwdReversePrice, 0) : fwdReversePrice);
            }
        } else {//交易货币为分母
            // 计算近端金额
            if (nearPrice != null && nearPrice != "" && nearRate != null && nearRate != "") {
                var nearReversePrice = CommonUtil.accDiv(nearPrice, nearRate);
                mini.get("nearReversePrice").setValue(currencyPair.indexOf("JPY") != -1 ? CommonUtil.fomatFloat(nearReversePrice, 0) : nearReversePrice);
            }
            // 计算远端金额
            if (fwdPrice != null && fwdPrice != "" && fwdRate != null && fwdRate != "") {
                var fwdReversePrice = CommonUtil.accDiv(fwdPrice, fwdRate);
                mini.get("fwdReversePrice").setValue(currencyPair.indexOf("JPY") != -1 ? CommonUtil.fomatFloat(fwdReversePrice, 0) : fwdReversePrice);
            }
        }
    }

    //2.根据交易货币-货币对计算交易汇率
    function getspread() {
        var fwdRate = mini.get("fwdRate").getValue();
        var nearRate = mini.get("nearRate").getValue();
        var spread = fwdRate - nearRate;
        mini.get("spread").setValue(spread);
    }

    function getFwdReversePrice(e) {
        var fwdPrice = mini.get("fwdPrice").getValue();
        var fwdRate = mini.get("fwdRate").getValue();
        var fwdReversePrice = Number(fwdPrice) * Number(fwdRate);
        mini.get("fwdReversePrice").setValue(fwdReversePrice);
    }

    //3.根据交易快捷金额输入
    $("#nearPrice").mouseleave(function () {
        var buyAmountText = mini.get("nearPrice").getText();
        var len = buyAmountText.length;
        var val = buyAmountText.substring(0, len - 1);
        var dw = buyAmountText.substring(len - 1, len);
        if (dw == 'k' || dw == 'K') {
            mini.get("nearPrice").setValue(val * 1000);
        } else if (dw == 'm' || dw == 'M') {
            mini.get("nearPrice").setValue(val * 1000000);
        } else if (dw == 'b' || dw == 'B') {
            mini.get("nearPrice").setValue(val * 1000000000);
        }
    });
    $("#nearReversePrice").mouseleave(function () {
        var sellAmountText = mini.get("nearReversePrice").getText();
        var len = sellAmountText.length;
        var val = sellAmountText.substring(0, len - 1);
        var dw = sellAmountText.substring(len - 1, len);
        if (dw == 'k' || dw == 'K') {
            mini.get("nearReversePrice").setValue(val * 1000);
            setPrice();
        } else if (dw == 'm' || dw == 'M') {
            mini.get("nearReversePrice").setValue(val * 1000000);
            setPrice();
        } else if (dw == 'b' || dw == 'B') {
            mini.get("nearReversePrice").setValue(val * 1000000000);
            setPrice();
        }
    });
    $("#fwdPrice").mouseleave(function () {
        var buyAmountText = mini.get("fwdPrice").getText();
        var len = buyAmountText.length;
        var val = buyAmountText.substring(0, len - 1);
        var dw = buyAmountText.substring(len - 1, len);
        if (dw == 'k' || dw == 'K') {
            mini.get("fwdPrice").setValue(val * 1000);
        } else if (dw == 'm' || dw == 'M') {
            mini.get("fwdPrice").setValue(val * 1000000);
        } else if (dw == 'b' || dw == 'B') {
            mini.get("fwdPrice").setValue(val * 1000000000);
        }
    });
    $("#fwdReversePrice").mouseleave(function () {
        var sellAmountText = mini.get("fwdReversePrice").getText();
        var len = sellAmountText.length;
        var val = sellAmountText.substring(0, len - 1);
        var dw = sellAmountText.substring(len - 1, len);
        if (dw == 'k' || dw == 'K') {
            mini.get("fwdReversePrice").setValue(val * 1000);
            setPrice();
        } else if (dw == 'm' || dw == 'M') {
            mini.get("fwdReversePrice").setValue(val * 1000000);
            setPrice();
        } else if (dw == 'b' || dw == 'B') {
            mini.get("fwdReversePrice").setValue(val * 1000000000);
            setPrice();
        }
    });

    //============================================================

    function onCurrencyPairChange(e) {
        var direction = mini.get("direction").getValue();
        if(direction == 'P'){
            mini.get("directionfwd").setValue('S');
        }else if(direction == 'S'){
            mini.get("directionfwd").setValue('P');
        }
        var currencyPair = mini.get("currencyPair").getValue();
        var arr = new Array();
        arr = currencyPair.split(".");//货币对1
        mini.get("buyCurreny").setValue(arr[0]);
        mini.get("sellCurreny").setValue(arr[1]);
        mini.get("buyCurreny1").setValue(arr[1]);
        mini.get("sellCurreny1").setValue(arr[0]);
        var ccyJson = "[{id:'" + arr[0] + "',text:'" + arr[0] + "'},{id:'" + arr[1] + "',text:'" + arr[1] + "'}]";
        mini.get("opicsccy").setData(ccyJson);
        mini.get("opicsccy").setValue(arr[0]);
        mini.get("opicsctrccy").setData(ccyJson);
        mini.get("opicsctrccy").setValue(arr[1]);
        mini.get("opicsccyfwd").setData(ccyJson);
        mini.get("opicsccyfwd").setValue(arr[0]);
        mini.get("opicsctrccyfwd").setData(ccyJson);
        mini.get("opicsctrccyfwd").setValue(arr[1]);
        getReversePrice();
        changeAcct();
    }

    function initCurrencyPair() {
        var currencyPair = mini.get("currencyPair").getValue();
        var arr = new Array();
        arr = currencyPair.split(".");//货币对1
        var ccyJson = "[{id:'" + arr[0] + "',text:'" + arr[0] + "'},{id:'" + arr[1] + "',text:'" + arr[1] + "'}]";
        mini.get("opicsccy").setData(ccyJson);
        mini.get("opicsctrccy").setData(ccyJson);
        mini.get("opicsccyfwd").setData(ccyJson);
        mini.get("opicsctrccyfwd").setData(ccyJson);
        changeAcct();
    }

    function onCurrencyChange(e) {
        var arr = new Array();
        arr = mini.get("currencyPair").getValue().split(".");
        var opicsccy = mini.get("opicsccy");
        var opicsctrccy = mini.get("opicsctrccy");
        if (opicsccy.getValue() == arr[0]) {
            opicsctrccy.setValue(arr[1]);
            mini.get("opicsccyfwd").setValue(arr[0]);
            mini.get("opicsctrccyfwd").setValue(arr[1]);
            mini.get("buyCurreny").setValue(arr[0]);
            mini.get("sellCurreny").setValue(arr[1]);
            mini.get("buyCurreny1").setValue(arr[1]);
            mini.get("sellCurreny1").setValue(arr[0]);

        } else {
            opicsctrccy.setValue(arr[0]);
            mini.get("opicsccyfwd").setValue(arr[1]);
            mini.get("opicsctrccyfwd").setValue(arr[0]);
            mini.get("buyCurreny").setValue(arr[1]);
            mini.get("sellCurreny").setValue(arr[0]);
            mini.get("buyCurreny1").setValue(arr[0]);
            mini.get("sellCurreny1").setValue(arr[1]);
        }
        getReversePrice();
        changeAcct();
    }

    function onDrawDateNear(e) {
        var nearValuedate = e.date;
        var fwdValuedate = mini.get("fwdValuedate").getValue();
        if (CommonUtil.isNull(fwdValuedate)) {
            return;
        }
        if (fwdValuedate.getTime() < nearValuedate.getTime()) {
            e.allowSelect = false;
        }
    }

    function onDrawDateFar(e) {
        var fwdValuedate = e.date;
        var nearValuedate = mini.get("nearValuedate").getValue();
        if (CommonUtil.isNull(nearValuedate)) {
            return;
        }
        if (fwdValuedate.getTime() < nearValuedate.getTime()) {
            e.allowSelect = false;
        }
    }

    function associaDateCalculation(e) {
        var nearValuedate = mini.get("nearValuedate").getValue();
        var fwdValuedate = mini.get("fwdValuedate").getValue();
        if (CommonUtil.isNull(nearValuedate) || CommonUtil.isNull(fwdValuedate)) {
            return;
        }
        var subDate = Math.abs(parseInt((fwdValuedate.getTime() - nearValuedate.getTime()) / 1000 / 3600 / 24));
        mini.get("tenor").setValue(subDate);
    }

    //文、数字、下划线 的验证
    function onEnglishAndNumberValidations(e) {
        if (e.value == "" || e.value == null) {//值为空，就不做校验
            return;
        }
        if (e.isValid) {
            if (isEnglishAndNumbers(e.value) == false) {
                e.errorText = "必须输入英文小写+数字";
                e.isValid = false;
            }
        }
    }

    /* 是否英文+数字 */
    function isEnglishAndNumbers(v) {
        var re = new RegExp("^[0-9a-z\_\-]+$");
        if (re.test(v)) return true;
        return false;
    }


    //文、数字、下划线 的验证
    function onEnglishAndNumberValidation(e) {
        if (e.value == "" || e.value == null) {//值为空，就不做校验
            return;
        }
        if (e.isValid) {
            if (isEnglishAndNumber(e.value) == false) {
                e.errorText = "必须输入英文+数字";
                e.isValid = false;
            }
        }
    }

    /* 是否英文+数字 */
    function isEnglishAndNumber(v) {
        var re = new RegExp("^[0-9a-zA-Z\_\-]+$");
        if (re.test(v)) return true;
        return false;
    }

    //不能输入中文
    function chineseValidation(e) {
        var re = new RegExp("^[^\u4e00-\u9fa5]{0,}$");
        if (re.test(e.value)) {
            return true;
        } else {
            e.errorText = "不能输入中文";
            e.isValid = false;
        }
    }

    //解析货币对
    function parseCurrencyPair(cp) {
        var ccy = cp.substr(0, 3);
        var ctr = cp.substr(4, 7);
        var data = {ccy: ccy, ctr: ctr};
        return data;
    }


    function changeAcct() {
        var buyCurreny = mini.get("buyCurreny").getValue();
        var sellCurreny = mini.get("sellCurreny").getValue();
        $("#bankBic1").show();
        $("#intermediaryBankName1").show();
        $("#intermediaryBankBicCode1").show();
        $("#bankBic2").show();
        $("#intermediaryBankName2").show();
        $("#intermediaryBankBicCode2").show();
        $("#bankAccount1").show();
        $("#bankAccount2").show();
        $("#intermediaryBankAcctNo1").show();
        $("#intermediaryBankAcctNo2").show();
        if (buyCurreny == "CNY") {
            mini.get("bankName1").setLabel("资金账户户名：");
            mini.get("dueBank1").setLabel("资金开户行：");
            mini.get("dueBankName1").setLabel("支付系统行号：");
            mini.get("dueBankAccount1").setLabel("资金账号：");
            $("#bankBic1").hide();
            $("#intermediaryBankName1").hide();
            $("#intermediaryBankBicCode1").hide();
            $("#bankAccount1").hide();
            $("#intermediaryBankAcctNo1").hide();

        } else if (sellCurreny == "CNY") {
            mini.get("bankName2").setLabel("资金账户户名：");
            mini.get("dueBank2").setLabel("资金开户行：");
            mini.get("dueBankName2").setLabel("支付系统行号：");
            mini.get("dueBankAccount2").setLabel("资金账号：");
            $("#bankBic2").hide();
            $("#intermediaryBankName2").hide();
            $("#intermediaryBankBicCode2").hide();
            $("#bankAccount2").hide();
            $("#intermediaryBankAcctNo2").hide();
        }

        var buyCurreny1 = mini.get("buyCurreny1").getValue();
        var sellCurreny1 = mini.get("sellCurreny1").getValue();
        $("#bankBic3").show();
        $("#intermediaryBankName3").show();
        $("#intermediaryBankBicCode3").show();
        $("#bankBic4").show();
        $("#intermediaryBankName4").show();
        $("#intermediaryBankBicCode4").show();
        $("#bankAccount3").show();
        $("#bankAccount4").show();
        $("#intermediaryBankAcctNo3").show();
        $("#intermediaryBankAcctNo4").show();
        if (buyCurreny1 == "CNY") {
            mini.get("bankName3").setLabel("资金账户户名：");
            mini.get("dueBank3").setLabel("资金开户行：");
            mini.get("dueBankName3").setLabel("支付系统行号：");
            mini.get("dueBankAccount3").setLabel("资金账号：");
            $("#bankBic3").hide();
            $("#intermediaryBankName3").hide();
            $("#intermediaryBankBicCode3").hide();
            $("#bankAccount3").hide();
            $("#intermediaryBankAcctNo3").hide();

        } else if (sellCurreny1 == "CNY") {
            mini.get("bankName4").setLabel("资金账户户名：");
            mini.get("dueBank4").setLabel("资金开户行：");
            mini.get("dueBankName4").setLabel("支付系统行号：");
            mini.get("dueBankAccount4").setLabel("资金账号：");
            $("#bankBic4").hide();
            $("#intermediaryBankName4").hide();
            $("#intermediaryBankBicCode4").hide();
            $("#bankAccount4").hide();
            $("#intermediaryBankAcctNo4").hide();
        }
    }

    //根据英文节点类型返回相应中文
    ApproveFlowLog.ActivityType = function (type) {
        var result = null;
        switch (type) {
            case "startEvent":
                result = "开始节点";
                break;
            case "exclusiveGateway":
                result = "决策节点";
                break;
            case "userTask":
                result = "人工节点";
                break;
            case "endEvent":
                result = "结束节点";
                break;
            default:
                result = type;
        }
        return result;
    };

    function stampToTimeRenderer(e) {
        if (e.value) {
            return CommonUtil.stampToTime(e.value);
        } else {
            return "";
        }
    }

    function timerFormatRenderer(e) {
        if (e.value) {
            return CommonUtil.secondFormatter(e.value / 1000);
        } else {
            return "";
        }
    }

    function activityTypeRenderer(e) {
        if (e.value) {
            return e.value;
        } else {
            return ApproveFlowLog.ActivityType(e.value);
        }
    }

</script>
<script type="text/javascript" src="../../Common/Flow/MiniApproveOpCommon.js"></script>
</body>
</html>