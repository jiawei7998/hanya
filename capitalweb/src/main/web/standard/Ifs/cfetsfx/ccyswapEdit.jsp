<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
	<script type="text/javascript" src="<%=basePath%>/standard/Common/RiskCenter.js"></script>
	<script type="text/javascript" src="../cfetsrmb/rmbVerify.js"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title></title>
	<script type="text/javascript" >
	/**prdCode用于页面投资组合的选择   该全局变量必须为prdCode*/
	var prdCode="SWAP";
</script>
</head>
<body style="width: 100%; height: 100%; background: white">
<div class="mini-splitter" style="width:100%;height:100%;">
		<div size="90%" showCollapseButton="false">
		<h1 style="text-align:center"><strong>外汇交易货币掉期（SWAP）</strong></h1>
	<div  id="field_form"  class="mini-fit area" style="background:white" >
	<input id="dealTransType" name="dealTransType" class="mini-hidden"/>
	<input id="sponsor" name="sponsor" class="mini-hidden" />
	<input id="sponInst" name="sponInst" class="mini-hidden" />
	<input id="aDate" name="aDate" class="mini-hidden"/>
	<input id="ticketId" name="ticketId" class="mini-hidden"/>
	<div class="mini-panel" title="成交单编号" style="width:100%;"  allowResize="true" collapseOnTitleClick="true">
			<div class="leftarea">
				<input style="width:100%;" id="contractId" name="contractId" class="mini-textbox mini-mustFill" labelField="true" requiredErrorText="该输入项为必输项" label="成交单编号：" labelStyle="text-align:left;width:130px;" vtype="maxLength:35" required="true" />
			</div>
	</div>	
	<div class="mini-panel" title="成交双方信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
		<div class="leftarea">
				<input id="instId"	name="instId" class="mini-textbox mini-mustFill" labelField="true" label="本方机构：" style="width: 100%;" labelStyle="text-align:left;width:130px;" requiredErrorText="该输入项为必输项" required="true"  vtype="maxLength:16"/>
				<input id="dealer" name="dealer" class="mini-textbox mini-mustFill" labelField="true" label="本方交易员：" labelStyle="text-align:left;width:130px;" style="width: 100%;"requiredErrorText="该输入项为必输项" required="true"  vtype="maxLength:20"/>
				<input id="forDate" name="forDate" class="mini-datepicker mini-mustFill" labelField="true" label="交易日期："  requiredErrorText="该输入项为必输项" required="true"  labelStyle="text-align:left;width:130px;" style="width: 100%;" />
				<input id="tradingModel" name="tradingModel" class="mini-combobox mini-mustFill" labelField="true" label="交易模式：" requiredErrorText="该输入项为必输项" required="true"  value="C-Swap"  labelStyle="text-align:left;width:130px;" style="width: 100%;" data="CommonUtil.serverData.dictionary.TradeModel" />
		</div>
		<div class="rightarea">
				<input id="counterpartyInstId" name="counterpartyInstId" class="mini-buttonedit mini-mustFill" allowInput="false" onbuttonclick="onButtonEdit" labelField="true" label="对方机构：" requiredErrorText="该输入项为必输项" required="true"  style="width: 100%;" labelStyle="text-align:left;width:130px;" vtype="maxLength:16"/>
				<input id="counterpartyDealer" name="counterpartyDealer" class="mini-textbox mini-mustFill" labelField="true" label="对方交易员：" labelStyle="text-align:left;width:130px;" style="width: 100%;"  vtype="maxLength:10" required="true"  />
				<input id="forTime" name="forTime" class="mini-timespinner mini-mustFill"  labelField="true" label="交易时间：" labelStyle="text-align:left;width:130px;" style="width: 100%;" requiredErrorText="该输入项为必输项" required="true" />
				<input id="tradingType" name="tradingType" class="mini-combobox mini-mustFill" labelField="true"  value="C-Swap" label="交易方式：" labelStyle="text-align:left;width:130px;" style="width: 100%;" data="CommonUtil.serverData.dictionary.TradeType" requiredErrorText="该输入项为必输项" required="true" />
		</div>		
	</div>
	<div class="mini-panel" title="交易主体信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
		<div class="leftarea">
			<input style="width:100%" id="startDate" name="startDate" class="mini-datepicker mini-mustFill" labelField="true" label="开始日期："  labelStyle="text-align:left;width:130px;" requiredErrorText="该输入项为必输项" required="true" />
			<input style="width:100%" id="stub" name="stub" class="mini-textbox mini-mustFill" labelField="true" label="残段："  labelStyle="text-align:left;width:130px;"  vtype="maxLength:30" requiredErrorText="该输入项为必输项" required="true" /></td>
			<input style="width:100%" id="spotRate" name="spotRate" class="mini-Spinner mini-mustFill" labelField="true" label="即期汇率(%)：" format="n4" maxValue="99999.9999" changeOnMousewheel='false' labelStyle="text-align:left;width:130px;" requiredErrorText="该输入项为必输项" required="true" />
			<input style="width:100%" id="describe" name="describe" class="mini-textbox" labelField="true" label="描述："  labelStyle="text-align:left;width:130px;" vtype="maxLength:80" />
			<input style="width:100%" id="fixedFloatDir" name="fixedFloatDir" class="mini-combobox mini-mustFill" labelField="true" label="固定/浮动方向："  labelStyle="text-align:left;width:130px;"  required="true"  data="CommonUtil.serverData.dictionary.fixFltDir" enabled="false"/>
		</div>
		<div class="rightarea">
			<input style="width:100%" id="notionalExchange" name="notionalExchange" class="mini-textbox mini-mustFill" labelField="true" label="本金交换："  labelStyle="text-align:left;width:130px;"  vtype="maxLength:30" requiredErrorText="该输入项为必输项" required="true" />
			<input style="width:100%" id="unadjustmaturityDate" name="unadjustmaturityDate" class="mini-datepicker mini-mustFill" labelField="true" label="未调整到日期：" labelStyle="text-align:left;width:130px;" requiredErrorText="该输入项为必输项" required="true" />
			<input style="width:100%" id="nonVanilla" name="nonVanilla" class="mini-combobox mini-mustFill" labelField="true" label="是否标准协议："  labelStyle="text-align:left;width:130px;"  vtype="maxLength:30" data="CommonUtil.serverData.dictionary.YesNo" requiredErrorText="该输入项为必输项" required="true" />
			<input style="width:100%" id="accountingMethod" name="accountingMethod" class="mini-combobox mini-mustFill" labelField="true" label="交易类型："  labelStyle="text-align:left;width:130px;"  vtype="maxLength:20" data="CommonUtil.serverData.dictionary.acctMethod" requiredErrorText="该输入项为必输项" required="true" />
			<input id="nettingStatus" name="nettingStatus" class="mini-combobox" labelField="true"   label="净额清算状态：" labelStyle="text-align:left;width:130px;" style="width:100%;" data="CommonUtil.serverData.dictionary.YesNo" showNullItem="true"/>
		</div>
	</div>
	<div class="mini-panel" title="交易明细信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
		<div class="leftarea">
			<fieldset>
				<legend>左端(First Leg)</legend>
				<input id="leftDirection" name="leftDirection" class="mini-combobox mini-mustFill" labelField="true" label="付款方向：" requiredErrorText="该输入项为必输项" required="true"  style="width: 100%;" labelStyle="text-align:left;width:130px;" data="CommonUtil.serverData.dictionary.trading" onvaluechanged="onLeftDirectionChange" onvalidation="onFixFloatDirValidation"/>
				<input id="leftNotion" name="leftNotion" class="mini-spinner mini-mustFill" labelField="true" label="名义本金：" requiredErrorText="该输入项为必输项" required="true"  style="width: 100%;" labelStyle="text-align:left;width:130px;" maxValue="999999999999999" changeOnMousewheel='false' format="n4" onvalidation="zeroValidation"/>
				<input id="leftFixedFloat" name="leftFixedFloat" class="mini-combobox mini-mustFill" labelField="true" label="固定/浮动：" data = "CommonUtil.serverData.dictionary.fixfloat" required="true"  style="width:100%" labelStyle="text-align:left;width:130px;" changeOnMousewheel='false' onvalidation="onFixFloatDirValidation"/>
				<input id="leftRate" name="leftRate" class="mini-Spinner mini-mustFill" labelField="true" format="n8" maxValue="999.9999" label="固定/浮动利率：" requiredErrorText="该输入项为必输项" required="true"  style="width: 100%;" labelStyle="text-align:left;width:130px;" changeOnMousewheel='false' />
				<input id="leftFrequency" name="leftFrequency" class="mini-combobox mini-mustFill" labelField="true" label="定息周期：" style="width: 100%;" labelStyle="text-align:left;width:130px;" data="CommonUtil.serverData.dictionary.payCycle" requiredErrorText="该输入项为必输项" required="true" />
				<input id="leftRule" name="leftRule" class="mini-textbox" labelField="true" label="定息规则：" style="width: 100%;" labelStyle="text-align:left;width:130px;"/> 
				<input id="leftBasis" name="leftBasis" class="mini-combobox mini-mustFill" labelField="true" label="计息方式：" style="width: 100%;" labelStyle="text-align:left;width:130px;" data="CommonUtil.serverData.dictionary.calIrsDays" requiredErrorText="该输入项为必输项" required="true" />
				<input id="leftPayment" name="leftPayment" class="mini-combobox mini-mustFill" labelField="true" label="付息周期：" style="width: 100%;" labelStyle="text-align:left;width:130px;" data="CommonUtil.serverData.dictionary.payCycle" requiredErrorText="该输入项为必输项" required="true" />
				<input id="leftConvention" name="leftConvention" class="mini-combobox mini-mustFill" labelField="true" label="起息计算方式：" style="width: 100%;" labelStyle="text-align:left;width:130px;" data="CommonUtil.serverData.dictionary.AccMethod" requiredErrorText="该输入项为必输项" required="true" />
				<input id="leftDiscountCurve" name="leftDiscountCurve" class="mini-buttonedit mini-mustFill" onbuttonclick="onYchdEdit" labelField="true" label="利率曲线：" style="width: 100%;" labelStyle="text-align:left;width:130px;" allowInput="false" requiredErrorText="该输入项为必输项" required="true" />
				<input id="leftRateCode" name="leftRateCode" class="mini-buttonedit mini-mustFill" onbuttonclick="onRateEdit" labelField="true" label="利率代码：" style="width: 100%;" labelStyle="text-align:left;width:130px;" allowInput="false" requiredErrorText="该输入项为必输项" required="true" />
			</fieldset>
		</div>
		<div class="rightarea">
			<fieldset>
				<legend>右端(Second Leg)</legend>
				<input id="rightDirection" name="rightDirection" class="mini-combobox mini-mustFill" labelField="true" label="付款方向：" style="width: 100%;" labelStyle="text-align:left;width:130px;" data="CommonUtil.serverData.dictionary.trading" enabled="false" requiredErrorText="该输入项为必输项" required="true" />
				<input id="rightNotion" name="rightNotion" class="mini-spinner mini-mustFill" labelField="true" label="名义本金：" style="width: 100%;" labelStyle="text-align:left;width:130px;" maxValue="999999999999999" changeOnMousewheel='false' format="n4" requiredErrorText="该输入项为必输项" required="true"  onvalidation="zeroValidation"/>
				<input id="rightFixedFloat" name="rightFixedFloat" class="mini-combobox mini-mustFill" labelField="true" label="固定/浮动：" data = "CommonUtil.serverData.dictionary.fixfloat" required="true"  style="width:100%" labelStyle="text-align:left;width:130px;" changeOnMousewheel='false' onvalidation="onFixFloatDirValidation"/>
				<input id="rightRate" name="rightRate" class="mini-Spinner mini-mustFill" labelField="true" format="n8" maxValue="999.9999" label="固定/浮动利率：" changeOnMousewheel='false' style="width: 100%;" labelStyle="text-align:left;width:130px;" requiredErrorText="该输入项为必输项" required="true"  />
				<input id="rightFrequency" name="rightFrequency" class="mini-combobox mini-mustFill" labelField="true" label="定息周期：" style="width: 100%;" labelStyle="text-align:left;width:130px;" data="CommonUtil.serverData.dictionary.payCycle" requiredErrorText="该输入项为必输项" required="true" />
				<input id="rightRule" name="rightRule" class="mini-textbox" labelField="true" label="定息规则：" style="width: 100%;" labelStyle="text-align:left;width:130px;" />
				<input id="rightBasis" name="rightBasis" class="mini-combobox mini-mustFill" labelField="true" label="计息方式：" style="width: 100%;" labelStyle="text-align:left;width:130px;" data="CommonUtil.serverData.dictionary.calIrsDays" requiredErrorText="该输入项为必输项" required="true" />
				<input id="rightPayment" name="rightPayment" class="mini-combobox mini-mustFill" labelField="true" label="付息周期：" style="width: 100%;" labelStyle="text-align:left;width:130px;" data="CommonUtil.serverData.dictionary.payCycle" requiredErrorText="该输入项为必输项" required="true" />
				<input id="rightConvention" name="rightConvention" class="mini-combobox mini-mustFill" labelField="true" label="起息计算方式：" style="width: 100%;" labelStyle="text-align:left;width:130px;" data="CommonUtil.serverData.dictionary.AccMethod" requiredErrorText="该输入项为必输项" required="true" />
				<input id="rightDiscountCurve" name="rightDiscountCurve" class="mini-buttonedit mini-mustFill" onbuttonclick="onYchdEdit" labelField="true" label="利率曲线：" style="width: 100%;" labelStyle="text-align:left;width:130px;" allowInput="false" requiredErrorText="该输入项为必输项" required="true" />
				<input id="rightRateCode" name="rightRateCode" class="mini-buttonedit mini-mustFill" onbuttonclick="onRateEdit" labelField="true" label="利率代码：" style="width: 100%;" labelStyle="text-align:left;width:130px;" allowInput="false" requiredErrorText="该输入项为必输项" required="true" />
			</fieldset>
		</div>
	</div>
	<%@ include file="../../Common/opicsLess.jsp"%>
	<%@ include file="../../Common/RiskCenter.jsp"%>
	<div class="mini-panel" title="清算信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
		<div class="leftarea">
			<input id="sellCurreny" name="sellCurreny" class="mini-combobox mini-mustFill" labelField="true" label="清算货币：" style="width: 100%;" labelStyle="text-align:left;width:130px;" data="CommonUtil.serverData.dictionary.Currency" requiredErrorText="该输入项为必输项" required="true" />
			<input id="bankName1" name="bankName1" class="mini-textbox mini-mustFill" labelField="true" label="开户行名称：" labelStyle="text-align:left;width:130px;" style="width: 100%;"  vtype="maxLength:40" requiredErrorText="该输入项为必输项" required="true"  />
			<input id="bankBic1" name="bankBic1" class="mini-textbox mini-mustFill" labelField="true" label="开户行BIC CODE：" labelStyle="text-align:left;width:130px;" style="width: 100%;" onvalidation="onEnglishAndNumberValidation"  vtype="maxLength:45" requiredErrorText="该输入项为必输项" required="true" />
			<input id="dueBank1" name="dueBank1" class="mini-textbox mini-mustFill" labelField="true" label="收款行名称" labelStyle="text-align:left;width:130px;" style="width: 100%;" vtype="maxLength:40" requiredErrorText="该输入项为必输项" required="true" />
			<input id="dueBankName1" name="dueBankName1" class="mini-textbox mini-mustFill" labelField="true" label="收款行名称BIC CODE：" labelStyle="text-align:left;width:130px;" style="width: 100%;" onvalidation="onEnglishAndNumberValidation" vtype="maxLength:45" requiredErrorText="该输入项为必输项" required="true" />
			<input id="dueBankAccount1" name="dueBankAccount1" class="mini-textbox mini-mustFill" labelField="true" label="收款行账号：" labelStyle="text-align:left;width:130px;" style="width: 100%;" onvalidation="onEnglishAndNumberValidation"  vtype="maxLength:25;int" requiredErrorText="该输入项为必输项" required="true" />
			<input id="bankName2" name="bankName2" class="mini-textbox mini-mustFill" labelField="true" label="开户行名称：" labelStyle="text-align:left;width:130px;" style="width: 100%;"  vtype="maxLength:40" requiredErrorText="该输入项为必输项" required="true" />
			<input id="bankBic2" name="bankBic2" class="mini-textbox mini-mustFill" labelField="true" label="开户行BIC CODE：" labelStyle="text-align:left;width:130px;" style="width: 100%;" onvalidation="onEnglishAndNumberValidation"  vtype="maxLength:45" requiredErrorText="该输入项为必输项" required="true" />
			<input id="dueBank2" name="dueBank2" class="mini-textbox mini-mustFill" labelField="true" label="收款行名称：" labelStyle="text-align:left;width:130px;" style="width: 100%;"  vtype="maxLength:40" requiredErrorText="该输入项为必输项" required="true" />
			<input id="dueBankName2" name="dueBankName2" class="mini-textbox mini-mustFill" labelField="true" label="收款行名称BIC CODE：" labelStyle="text-align:left;width:130px;" style="width: 100%;" onvalidation="onEnglishAndNumberValidation"  vtype="maxLength:45" requiredErrorText="该输入项为必输项" required="true" />
			<input id="dueBankAccount2" name="dueBankAccount2" class="mini-textbox mini-mustFill" labelField="true" label="收款行账号：" labelStyle="text-align:left;width:130px;" style="width: 100%;" onvalidation="onEnglishAndNumberValidation" vtype="maxLength:25;int"  requiredErrorText="该输入项为必输项" required="true" />
		</div>
		<div class="rightarea">
			<input id="buyCurreny" name="buyCurreny" class="mini-combobox mini-mustFill" labelField="true" label="清算货币：" style="width: 100%;" labelStyle="text-align:left;width:130px;" data="CommonUtil.serverData.dictionary.Currency" requiredErrorText="该输入项为必输项" required="true" />
			<input id="capitalBank1" name="capitalBank1" class="mini-textbox mini-mustFill" labelField="true" label="资金开户：" labelStyle="text-align:left;width:130px;" style="width: 100%;"  vtype="maxLength:40" requiredErrorText="该输入项为必输项" required="true"  />
			<input id="capitalBankName1" name="capitalBankName1" class="mini-textbox mini-mustFill" labelField="true" label="资金账户户名：" labelStyle="text-align:left;width:130px;" style="width: 100%;"  vtype="maxLength:40" requiredErrorText="该输入项为必输项" required="true" />
			<input id="cnapsCode1" name="cnapsCode1" class="mini-textbox mini-mustFill" labelField="true" label="支付系统行号：" labelStyle="text-align:left;width:130px;" style="width: 100%;" onvalidation="onEnglishAndNumberValidation"  vtype="maxLength:25;int"  requiredErrorText="该输入项为必输项" required="true" />
			<input id="capitalAccount1" name="capitalAccount1" class="mini-textbox mini-mustFill" labelField="true" label="资金账号：" labelStyle="text-align:left;width:130px;" style="width: 100%;" onvalidation="onEnglishAndNumberValidation"  vtype="maxLength:25;int"  requiredErrorText="该输入项为必输项" required="true" />
			<input id="capitalBank2" name="capitalBank2" class="mini-textbox mini-mustFill" labelField="true" label="资金开户行：" labelStyle="text-align:left;width:130px;" style="width: 100%;"  vtype="maxLength:40" requiredErrorText="该输入项为必输项" required="true"  />
			<input id="capitalBankName2" name="capitalBankName2" class="mini-textbox mini-mustFill" labelField="true" label="资金账户户名：" labelStyle="text-align:left;width:130px;" style="width: 100%;"  vtype="maxLength:40"  requiredErrorText="该输入项为必输项" required="true" />
			<input id="cnapsCode2" name="cnapsCode2" class="mini-textbox mini-mustFill" labelField="true" label="支付系统行号：" labelStyle="text-align:left;width:130px;" style="width: 100%;" onvalidation="onEnglishAndNumberValidation"  vtype="maxLength:25;int" requiredErrorText="该输入项为必输项" required="true"  />
			<input id="capitalAccount2" name="capitalAccount2" class="mini-textbox mini-mustFill" labelField="true" label="资金账号：" labelStyle="text-align:left;width:130px;" style="width: 100%;" onvalidation="onEnglishAndNumberValidation"  vtype="maxLength:25;int"  requiredErrorText="该输入项为必输项" required="true" />
		</div>
	</div>
	<%@ include file="../../Common/Flow/MiniApproveOpCommon.jsp"%>
	</div>
	</div>
	<div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
			<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="save_btn"   onclick="save">保存交易</a></div>
			<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="close_btn"   onclick="close">关闭界面</a></div>
	</div>
</div> 
		<script type="text/javascript">
			mini.parse();
			//获取当前tab
			var currTab = top["win"].tabs.getActiveTab();
			var params = currTab.params;
			var row=params.selectData;
			var url = window.location.search;
			var action = CommonUtil.getParam(url, "action");
			var ticketId = CommonUtil.getParam(url, "ticketid");
			var prdNo = CommonUtil.getParam(url, "prdNo");
			mini.get("instId").setEnabled(false);
			//mini.get("dealer").setEnabled(false);
	
			mini.get("forDate").setValue("<%=__bizDate %>");
			
			var timestamp = Date.parse(new Date());
			sys(timestamp);
			function sys(stamp){
			var time = new Date(stamp);
			var result = "";
			result += CommonUtil.singleNumberFormatter(time.getHours()) + ":"; 
			result += CommonUtil.singleNumberFormatter(time.getMinutes()) + ":";
			result += CommonUtil.singleNumberFormatter(time.getSeconds());
			mini.get("forTime").setValue(result);
			}
			var tradeData={};
			
			tradeData.selectData=row;
			tradeData.operType=action;
			tradeData.serial_no=row.ticketId;
			tradeData.task_id=row.taskId;
			
			function inme() {
				if (action == "detail" || action == "edit"||action=="approve") {
					var from = new mini.Form("field_form");
					var from1 = new mini.Form("ApproveOperate_div");
					mini.get("applyNo").setVisible(false);//信用风险审查单号：
					mini.get("applyProd").setVisible(false);//产品名称
					CommonUtil.ajax({
						url : '/IfsCurrencyController/searchCcySwap',
						data : {
							ticketId : ticketId
						},
						callback : function(text) {
							from.setData(text.obj);
							//投资组合
							mini.get("port").setValue(text.obj.port);
							mini.get("port").setText(text.obj.port);
							mini.get("counterpartyInstId").setValue(text.obj.counterpartyInstId);
							queryTextName(text.obj.counterpartyInstId);
							//清算路径
							mini.get("ccysmeans").setValue(text.obj.ccysmeans);
							mini.get("ccysmeans").setText(text.obj.ccysmeans);
							mini.get("ctrsmeans").setValue(text.obj.ctrsmeans);
							mini.get("ctrsmeans").setText(text.obj.ctrsmeans);
							mini.get("applyNo").setValue(text.obj.applyNo);
							mini.get("applyNo").setText(text.obj.applyNo);
							mini.get("custNo").setValue(text.obj.custNo);
							queryCustNo(text.obj.custNo);
							mini.get("weight").setValue(text.obj.weight);
						}
					});
					if (action == "detail"||action=="approve") {
						mini.get("save_btn").hide();
						from.setEnabled(false);
						/* var form11=new mini.Form("#approve_operate_form");
					      form11.setEnabled(true); */
						from1.setEnabled(true);
					} 
					mini.get("contractId").setEnabled(false);
				}else if(action=="add"){
					mini.get("dealTransType").setValue("M");
					mini.get("instId").setValue("<%=__sessionInstitution.getInstName()%>");
					mini.get("dealer").setValue("<%=__sessionUser.getUserId() %>");
					mini.get("sponsor").setValue("<%=__sessionUser.getUserId()%>");
					mini.get("sponInst").setValue("<%=__sessionUser.getInstId() %>");
					mini.get("applyNo").setVisible(false);//信用风险审查单号：
					mini.get("applyProd").setVisible(false);//产品名称
				}
			}

			function save() {
				var form = new mini.Form("field_form");
				form.validate();
				if (form.isValid() == false) {
					mini.alert("信息填写有误，请重新填写","系统提示");
					return;
				}
				var form = new mini.Form("field_form");
				var data = form.getData(true); //获取表单多个控件的数据
				var json = mini.encode(data); //序列化成JSON
				if (action == "add") {
					CommonUtil.ajax({
						url : "/IfsCurrencyController/addCcySwap",
						data : json,
						callback : function(data) {
							mini.alert(data,'提示信息',function(){
								if(data=='保存成功'){
								top["win"].closeMenuTab();
								}
							});
						}
					});
				} else if (action = "edit") {
					CommonUtil.ajax({
						url : "/IfsCurrencyController/editCcySwap",
						data : json,
						callback : function(data) {
							if (data.code == 'error.common.0000') {
								mini.alert(data.desc, '提示信息', function() {
									top["win"].closeMenuTab();
								});
							} else {
								mini.alert("保存失败");
							}
						}
					});
				}
			}
			function close() {
				top["win"].closeMenuTab();
			}
	function onButtonEdit(e) {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Common/CustMini.jsp",
            title: "选择对手方列表",
            width: 900,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.cno);
                        btnEdit.setText(data.cname);
                        btnEdit.focus();
                    }
                }

            }
        });
    }
	//根据交易对手编号查询全称
	function queryTextName(cno){
		CommonUtil.ajax({
		    url: "/IfsOpicsCustController/searchIfsOpicsCust",
		    data : {'cno' : cno},
		    callback:function (data) {
		    	if (data && data.obj) {
		    		mini.get("counterpartyInstId").setText(data.obj.cfn);
				} else {
					mini.get("counterpartyInstId").setText();
		    }
		    }
		});
	}
			
			//文、数字、下划线 的验证
	function onEnglishAndNumberValidations(e) {
		if(e.value == "" || e.value == null){//值为空，就不做校验
			return;
		}
		if (e.isValid) {
			if (isEnglishAndNumbers(e.value) == false) {
				e.errorText = "必须输入英文小写+数字";
				e.isValid = false;
			}
		}
	}
	/* 是否英文+数字 */
	function isEnglishAndNumbers(v) {
		var re = new RegExp("^[0-9a-z\_\-]+$");
		if (re.test(v)) return true;
		return false;
	}	
			
			
			//英文、数字、下划线 的验证
			function onEnglishAndNumberValidation(e) {
				if(e.value == "" || e.value == null){//值为空，就不做校验
					return;
				}
				if (e.isValid) {
					if (isEnglishAndNumber(e.value) == false) {
						e.errorText = "必须输入英文+数字";
						e.isValid = false;
					}
				}
			}
			/* 是否英文+数字 */
			function isEnglishAndNumber(v) {
				var re = new RegExp("^[0-9a-zA-Z\_\-]+$");
				if (re.test(v)) return true;
				return false;
			}		
	
			//不能输入中文
			function chineseValidation(e){
				var re = new RegExp("^[^\u4e00-\u9fa5]{0,}$");
		        if (re.test(e.value)) {
		        	return true;
		        }else{
		            e.errorText = "不能输入中文";
		            e.isValid = false;
		        }
			}
			
	function onLeftDirectionChange(e){
		var leftDirection = mini.get("leftDirection").getValue();
		if(leftDirection=='P'){
			mini.get("rightDirection").setValue("S");
		}else if(leftDirection=='S'){
			mini.get("rightDirection").setValue("P");
		}
		
	}
			$(document).ready(function() {
				inme();
			});
			
			/**   付款路径的选择   */
			function settleMeans1(){
				 var cpInstId = mini.get("counterpartyInstId").getValue();
				if(cpInstId == null || cpInstId == ""){
					mini.alert("请先选择对方机构!");
					return;
				} 
				
				/* var ps = mini.get("leftDirection").getValue();
				if(ps == null || ps == ""){
					mini.alert("请先选择付款方向!");
					return;
				} */
				var sellCurreny = mini.get("sellCurreny").getValue();
				if(sellCurreny == null || sellCurreny == ""){
					mini.alert("请先选择左端清算货币!");
					return;
				}
				
				
				var url;
				var data;
				if(sellCurreny!="CNY"){//外币
					url="./Ifs/opics/nostMini.jsp";
		        	data = { ccy: sellCurreny ,cust:cpInstId};
		        }else if(sellCurreny=="CNY"){//人民币
		        	data = { ccy: sellCurreny ,cust:cpInstId};
		        	url="./Ifs/opics/setaMini.jsp";
		        }
				
				var btnEdit = this;
		        mini.open({
		            url: url,
		            title: "选择清算路径",
		            width: 700,
		            height: 600,
		            onload: function () {
		                var iframe = this.getIFrameEl();
		                iframe.contentWindow.SetData(data);
		            },
		            ondestroy: function (action) {
		                if (action == "ok") {
		                    var iframe = this.getIFrameEl();
		                    var data1 = iframe.contentWindow.GetData();
		                    data1 = mini.clone(data1);    //必须
		                    if (data1) {
		                    	if(sellCurreny!="CNY"){//外币
		                    		mini.get("ccysmeans").setValue($.trim(data1.smeans));
		                    		mini.get("ccysmeans").setText($.trim(data1.smeans));
		                            mini.get("ccysacct").setValue($.trim(data1.nos));
		        		        }else if(sellCurreny=="CNY"){//人民币
		        		        	 mini.get("ccysmeans").setValue($.trim(data1.smeans));
			                    	 mini.get("ccysmeans").setText($.trim(data1.smeans));
			                         mini.get("ccysacct").setValue($.trim(data1.sacct)); 
		        		        	
		        		        }
		                    	
		                        btnEdit.focus();
		                    }
		                }

		            }
		        });
				
				
			}	
			
			
			
			/**   收款路径的选择   */
			function settleMeans2(){
				 var cpInstId = mini.get("counterpartyInstId").getValue();
				if(cpInstId == null || cpInstId == ""){
					mini.alert("请先选择对方机构!");
					return;
				} 
				
				/* var ps = mini.get("leftDirection").getValue();
				if(ps == null || ps == ""){
					mini.alert("请先选择付款方向!");
					return;
				} */
				
				
				 var buyCurreny = mini.get("buyCurreny").getValue();
				if(buyCurreny == null || buyCurreny == ""){
					mini.alert("请先选择右端清算货币!");
					return;
				} 
				/* var ps1 = mini.get("rightDirection").getValue(); */
				
				var url;
				var data;
				if(sellCurreny!="CNY"){//外币
					url="./Ifs/opics/nostMini.jsp";
		        	data = { ccy: buyCurreny ,cust:cpInstId};
		        }else if(sellCurreny=="CNY"){//人民币
		        	data = { ccy: buyCurreny ,cust:cpInstId};
		        	url="./Ifs/opics/setaMini.jsp";
		        }
				
				var btnEdit = this;
		        mini.open({
		            url: url,
		            title: "选择清算路径",
		            width: 700,
		            height: 600,
		            onload: function () {
		                var iframe = this.getIFrameEl();
		                iframe.contentWindow.SetData(data);
		            },
		            ondestroy: function (action) {
		                if (action == "ok") {
		                    var iframe = this.getIFrameEl();
		                    var data1 = iframe.contentWindow.GetData();
		                    data1 = mini.clone(data1);    //必须
		                    if (data1) {
		                    	if(sellCurreny!="CNY"){//外币
		                    		mini.get("ctrsmeans").setValue($.trim(data1.smeans));
		                    		mini.get("ctrsmeans").setText($.trim(data1.smeans));
		                            mini.get("ctrsacct").setValue($.trim(data1.nos));
		        		        }else if(sellCurreny=="CNY"){//人民币
		        		        	 mini.get("ctrsmeans").setValue($.trim(data1.smeans));
			                    	 mini.get("ctrsmeans").setText($.trim(data1.smeans));
			                         mini.get("ctrsacct").setValue($.trim(data1.sacct)); 
		        		        	
		        		        }
		                    	
		                        btnEdit.focus();
		                    }
		                }

		            }
		        });
				
				
			}	
			
			//利率代码的选择
			function onRateEdit(){
				var btnEdit = this;
		        mini.open({
		            url: CommonUtil.baseWebPath() + "/opics/rateMini.jsp",
		            title: "选择利率代码",
		            width: 700,
		            height: 600,
		            ondestroy: function (action) {
		                if (action == "ok") {
		                    var iframe = this.getIFrameEl();
		                    var data = iframe.contentWindow.GetData();
		                    data = mini.clone(data);    //必须
		                    if (data) {
		                        btnEdit.setValue(data.ratecode);
		                        btnEdit.setText(data.ratecode);
		                        btnEdit.focus();
		                    }
		                }

		            }
		        });
			}
			
			//利率曲线的选择
			function onYchdEdit(){
				var btnEdit = this;
		        mini.open({
		            url: CommonUtil.baseWebPath() + "/opics/ychdMini.jsp",
		            title: "选择利率曲线",
		            width: 700,
		            height: 600,
		            ondestroy: function (action) {
		                if (action == "ok") {
		                    var iframe = this.getIFrameEl();
		                    var data = iframe.contentWindow.GetData();
		                    data = mini.clone(data);    //必须
		                    if (data) {
		                        btnEdit.setValue(data.yieldcurve);
		                        btnEdit.setText(data.yieldcurve);
		                        btnEdit.focus();
		                    }
		                }

		            }
		        });
			}
			/* 是否英文 */
		    function englishValidation(e) {
	            	var re = new RegExp("^[a-zA-Z]+$");
	                if (re.test(e.value)) {
	                	return true;
	                }else{
	                    e.errorText = "只能输入英文";
	                    e.isValid = false;
	                }
		    }
			
			//根据付款方向和固定/浮动计算出固定/浮动方向
			function onFixFloatDirValidation(){
				var leftDir=mini.get("leftDirection").getValue();
				var leftFixFlt=mini.get("leftFixedFloat").getValue();
				var rightFixFlt=mini.get("rightFixedFloat").getValue();
				if(leftDir == "P"){//买入
					if(leftFixFlt == "Fixed" && rightFixFlt == "Float"){//买fixed卖float
						mini.get("fixedFloatDir").setValue("R");
					}else if(leftFixFlt == "Float" && rightFixFlt == "Fixed"){//买float卖fixed
						mini.get("fixedFloatDir").setValue("P");
					}
				}else if(leftDir == "S"){//卖出
					if(leftFixFlt == "Fixed" && rightFixFlt == "Float"){//卖fixed买float
						mini.get("fixedFloatDir").setValue("P");
					}else if(leftFixFlt == "Float" && rightFixFlt == "Fixed"){//卖flaot买fixed
						mini.get("fixedFloatDir").setValue("R");
					}
				}
				if(leftFixFlt == "Fixed" && rightFixFlt == "Fixed"){//买卖都是fixed
					mini.get("fixedFloatDir").setValue("X");
				}
				if(leftFixFlt == "Float" && rightFixFlt == "Float"){//买卖都是float
					mini.get("fixedFloatDir").setValue("L");
				}
				
			}
			
		</script>
	<script type="text/javascript" src="../../Common/Flow/MiniApproveOpCommon.js"></script>			
</body>
</html>