<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
	<script type="text/javascript" >
	/**prdCode用于页面投资组合的选择   该全局变量必须为prdCode*/
	var prdCode="FXD";
	</script>
</head>
<body style="width:100%;height:100%;background:white">
<div class="mini-splitter" style="width:100%;height:100%;">
	<div size="90%" showCollapseButton="false">
	<h1 style="text-align:center"><strong>外币交易即期（SPOT）</strong></h1>
	<div  id="field_form"  class="mini-fit area" style="background:white" >
	<input id="dealTransType" name="dealTransType" class="mini-hidden"/>
	<input id="sponsor" name="sponsor" class="mini-hidden" />
	<input id="sponInst" name="sponInst" class="mini-hidden" />
	<input id="ticketId" name="ticketId" class="mini-hidden" />
	<input id="aDate" name="aDate" class="mini-hidden"/>
	<div class="mini-panel" title="成交单编号" style="width:100%;"  allowResize="true" collapseOnTitleClick="true">
			<div class="leftarea">
				<input style="width:100%;" id="contractId" name="contractId" class="mini-textbox mini-mustFill" labelField="true" requiredErrorText="该输入项为必输项" label="成交单编号：" required="true"  labelStyle="text-align:left;width:130px;"  vtype="maxLength:20"/>
			</div>
		</div>
	
	<div class="mini-panel" title="成交双方信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">	
		<div class="leftarea">
			<input id="instId" name="instId" class="mini-textbox" labelField="true"  label="本方机构：" maxLength="30"	 style="width:100%;"  labelStyle="text-align:left;width:130px;" />
			<input id="dealer" name="dealer" class="mini-textbox" labelField="true"  label="本方交易员："  maxLength="20" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
			<input id="forDate" name="forDate" class="mini-datepicker" labelField="true"  label="交易日期：" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
			<input id="tradingModel" name="tradingModel" class="mini-combobox" labelField="true"  label="交易模式：" value="C-Swap" data="CommonUtil.serverData.dictionary.TradeModel" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
		</div>
		<div class="rightarea">
			<input id="counterpartyInstId" name="counterpartyInstId" class="mini-buttonedit  mini-mustFill" labelField="true" allowInput="false" label="对方机构：" onbuttonclick="onButtonEdit" maxLength="30" style="width:100%;"  labelStyle="text-align:left;width:130px;"  required="true" />
			<input id="counterpartyDealer" name="counterpartyDealer" class="mini-textbox mini-mustFill" labelField="true"  label="对方交易员：" maxLength="20" labelStyle="text-align:left;width:130px;" style="width:100%;"  required="true"  />
			<input id="forTime" name="forTime" class="mini-timespinner" labelField="true" format="H:mm:ss" label="交易时间："  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
			<input id="tradingType" name="tradingType" class="mini-combobox" labelField="true"  label="交易方式："  value="C-Swap" data="CommonUtil.serverData.dictionary.TradeType"  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
		</div>
	</div>
	<%@ include file="../../Common/opicsLess.jsp"%>
	
	<div class="mini-panel" title="交易主体信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
		<div class="leftarea">
			<input style="width:100%;" id="currencyPair" name="currencyPair" class="mini-combobox" labelField="true"  label="货币对："  data="CommonUtil.serverData.dictionary.CurrencyPair" labelStyle="text-align:left;width:130px;" onvaluechanged="onCurrencyPairChange"/>
			<input style="width:100%;" id="capital" name="capital" class="mini-spinner mini-mustFill" labelField="true"  label="本金：" required="true"  onValuechanged="interestAmount" changeOnMousewheel='false' maxValue="999999999999999" format="n4" labelStyle="text-align:left;width:130px;"  />
			<input style="width:100%;" id="valueDate" name="valueDate" class="mini-datepicker" labelField="true" label="起息日："  labelStyle="text-align:left;width:130px;" />
		</div>
		<div class="rightarea">
			<input style="width:100%;" id="direction" name="direction" class="mini-combobox" labelField="true"  label="买卖方向："  data="CommonUtil.serverData.dictionary.trading" labelStyle="text-align:left;width:130px;" onvaluechanged="onCurrencyPairChange"/>
			<input style="width:100%;" id="price" name="price" class="mini-spinner mini-mustFill" labelField="true"  label="价格：" required="true"  onValuechanged="interestAmount" changeOnMousewheel='false' maxValue="999999999999999" format="n8" labelStyle="text-align:left;width:130px;"/>
			<input style="width:100%;" id="settlementAmount" name="settlementAmount" class="mini-spinner mini-mustFill" labelField="true"  label="结算金额：" required="true"  changeOnMousewheel='false' maxValue="999999999999999" format="n4" labelField="true"  labelStyle="text-align:left;width:130px;" />
			<input id="nettingStatus" name="nettingStatus" class="mini-combobox" labelField="true"   label="净额清算状态：" labelStyle="text-align:left;width:130px;" style="width:100%;" data="CommonUtil.serverData.dictionary.YesNo" showNullItem="true" />
		</div>
	</div>
		<%@ include file="./accountInfor.jsp"%>	
		<%@ include file="../../Common/Flow/MiniApproveOpCommon.jsp"%>
	</div>
</div>
<div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
		<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="save_btn"  onclick="save">保存交易</a></div>
		<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="close_btn"  onclick="close">关闭界面</a></div>
	</div>
	</div>
	<script type="text/javascript">
	mini.parse();
	//获取当前tab
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var row=params.selectData;
	var url = window.location.search;
	var action = CommonUtil.getParam(url, "action");
	var ticketId=CommonUtil.getParam(url, "ticketid");
	mini.get("instId").setEnabled(false);
	mini.get("dealer").setEnabled(false);

	mini.get("forDate").setValue("<%=__bizDate %>");

	var prdNo = CommonUtil.getParam(url, "prdNo");
	
	var timestamp = Date.parse(new Date());
	sys(timestamp);
	function sys(stamp){
		var time = new Date(stamp);
		var result = "";
		result += CommonUtil.singleNumberFormatter(time.getHours()) + ":"; 
		result += CommonUtil.singleNumberFormatter(time.getMinutes()) + ":";
		result += CommonUtil.singleNumberFormatter(time.getSeconds());
		mini.get("forTime").setValue(result);
	}
	
	var tradeData={};
	
	tradeData.selectData=row;
	tradeData.operType=action;
	tradeData.serial_no=row.ticketId;
	tradeData.task_id=row.taskId;
	
	function inme(){
		if(action=="detail"||action=="edit"||action=="approve"){
		var from = new mini.Form("field_form");
		CommonUtil.ajax({
			url:'/IfsCurrencyController/searchCcyOnSpot',
			data:{ticketId:ticketId},
			callback:function(text){
				from.setData(text.obj);
				//投资组合
				mini.get("port").setValue(text.obj.port);
				mini.get("port").setText(text.obj.port);
				mini.get("counterpartyInstId").setValue(text.obj.counterpartyInstId);
				queryTextName(text.obj.counterpartyInstId);
				/* mini.get("counterpartyInstId").setText(text.obj.counterpartyInstId); */
				mini.get("instId").setValue("<%=__sessionInstitution.getInstName()%>");
				//清算路径
				mini.get("ccysmeans").setValue(text.obj.ccysmeans);
				mini.get("ccysmeans").setText(text.obj.ccysmeans);
				mini.get("ctrsmeans").setValue(text.obj.ctrsmeans);
				mini.get("ctrsmeans").setText(text.obj.ctrsmeans);
				mini.get("applyNo").setValue(text.obj.applyNo);
				mini.get("applyNo").setText(text.obj.applyNo);
				mini.get("custNo").setValue(text.obj.custNo);
				queryCustNo(text.obj.custNo);
				mini.get("weight").setValue(text.obj.weight);
			}
		})
		if(action=="detail"||action=="approve"){
			mini.get("save_btn").hide();
			from.setEnabled(false);
			var form1=new mini.Form("approve_operate_form");
			form1.setEnabled(true);
		}
		mini.get("contractId").setEnabled(false);
		}else if(action=="add"){
			mini.get("dealTransType").setValue("M");
			mini.get("instId").setValue("<%=__sessionInstitution.getInstName()%>");
			mini.get("dealer").setValue("<%=__sessionUser.getUserId() %>");
			mini.get("sponsor").setValue("<%=__sessionUser.getUserId()%>");
			mini.get("sponInst").setValue("<%=__sessionUser.getInstId() %>");
			//mini.get("aDate").setValue("<%=__bizDate %>");
		}
	}
	
	function save(){
		var form = new mini.Form("field_form");
		form.validate();
		if (form.isValid() == false) {
			return;
		}
		var form = new mini.Form("field_form");            
		var data = form.getData(true);      //获取表单多个控件的数据
		data['instId']="<%=__sessionInstitution.getInstId()%>";
		var json = mini.encode(data);   //序列化成JSON
		if(action=="add"){
			CommonUtil.ajax({
			    url: "/IfsCurrencyController/addCcyOnSpot",
			    data:json,
			    callback:function (data) {
			    	mini.alert(data,'提示信息',function(){
						if(data=='保存成功'){
						top["win"].closeMenuTab();
						}
					});
			    }
			});
			}else  if(action="edit"){
				CommonUtil.ajax({
					url:"/IfsCurrencyController/editCcyOnSpot",
					data:json,
					callback:function(data){
						if (data.code == 'error.common.0000') {
							mini.alert(data.desc,'提示信息',function(){
								top["win"].closeMenuTab();
							});
						} else {
							mini.alert("保存失败");
				    }
					}
				})
			}
	}
	function onButtonEdit(e) {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Common/CustMini.jsp",
            title: "选择对手方列表",
            width: 900,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.cno);
                        btnEdit.setText(data.cname);
                        btnEdit.focus();
                    }
                }

            }
        });

    }
	//根据交易对手编号查询全称
	function queryTextName(cno){
		CommonUtil.ajax({
		    url: "/IfsOpicsCustController/searchIfsOpicsCust",
		    data : {'cno' : cno},
		    callback:function (data) {
		    	if (data && data.obj) {
		    		mini.get("counterpartyInstId").setText(data.obj.cfn);
				} else {
					mini.get("counterpartyInstId").setText();
		    }
		    }
		});
	}
	
	//文、数字、下划线 的验证
	function onEnglishAndNumberValidations(e) {
		if(e.value == "" || e.value == null){//值为空，就不做校验
			return;
		}
		if (e.isValid) {
			if (isEnglishAndNumbers(e.value) == false) {
				e.errorText = "必须输入英文小写+数字";
				e.isValid = false;
			}
		}
	}
	/* 是否英文+数字 */
	function isEnglishAndNumbers(v) {
		var re = new RegExp("^[0-9a-z\_\-]+$");
		if (re.test(v)) return true;
		return false;
	}	
	
	
	//英文、数字、下划线 的验证
	function onEnglishAndNumberValidation(e) {
		if(e.value == "" || e.value == null){//值为空，就不做校验
			return;
		}
		if (e.isValid) {
			if (isEnglishAndNumber(e.value) == false) {
				e.errorText = "必须输入英文+数字";
				e.isValid = false;
			}
		}
	}
	/* 是否英文+数字 */
	function isEnglishAndNumber(v) {
		var re = new RegExp("^[0-9a-zA-Z\_\-]+$");
		if (re.test(v)) return true;
		return false;
	}
	function interestAmount(){
		var price = mini.get("price").getValue();
		var capital = mini.get("capital").getValue();
		if(price!=null && capital!=null){
			var allamount = Number(price)*Number(capital);
			mini.get("settlementAmount").setValue(allamount);
		}
	}
	function close(){
		top["win"].closeMenuTab();
	}
	$(document).ready(function () {
		inme();
	})
	
	/**   付款路径的选择   */
	function settleMeans1(){
		 var cpInstId = mini.get("counterpartyInstId").getValue();
		if(cpInstId == null || cpInstId == ""){
			mini.alert("请先选择对方机构!");
			return;
		} 
		
		/* var ps = mini.get("direction").getValue();
		if(ps == null || ps == ""){
			mini.alert("请先选择交易方向!");
			return;
		} */
		var currencyPair = mini.get("currencyPair").getText();
		if(currencyPair == null || currencyPair == ""){
			mini.alert("请先选择货币对!");
			return;
		}
		//解析货币对
		var ret = parseCurrencyPair(currencyPair);
		var url;
		var data;
		
		if(ret.ccy!="CNY"){//外币
			url="./Ifs/opics/nostMini.jsp";
        	data = { ccy: ret.ccy ,cust:cpInstId};
        }else if(ret.ccy=="CNY"){//人民币
        	data = { ccy: ret.ccy ,cust:cpInstId};
        	url="./Ifs/opics/setaMini.jsp";
        }
		
		var btnEdit = this;
        mini.open({
            url: url,
            title: "选择清算路径",
            width: 700,
            height: 600,
            onload: function () {
                var iframe = this.getIFrameEl();
                iframe.contentWindow.SetData(data);
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data1 = iframe.contentWindow.GetData();
                    data1 = mini.clone(data1);    //必须
                    if (data1) {
                    	if(ret.ccy!="CNY"){//外币
                    		mini.get("ccysmeans").setValue($.trim(data1.smeans));
                    		mini.get("ccysmeans").setText($.trim(data1.smeans));
                            mini.get("ccysacct").setValue($.trim(data1.nos));
                        }else if(ret.ccy=="CNY"){//人民币
                        	mini.get("ccysmeans").setValue($.trim(data1.smeans));
	                    	 mini.get("ccysmeans").setText($.trim(data1.smeans));
	                         mini.get("ccysacct").setValue($.trim(data1.sacct)); 
                        }
                        
                        btnEdit.focus();
                    }
                }

            }
        });
		
		
	}
	//解析货币对
	function parseCurrencyPair(cp){
		var ccy = cp.substr(0,3);
		var ctr = cp.substr(4,7);
		var data = {ccy:ccy,ctr:ctr};
		return data;
		
		
	}
	
	/**   收款路径的选择   */
	function settleMeans2(){
		 var cpInstId = mini.get("counterpartyInstId").getValue();
		if(cpInstId == null || cpInstId == ""){
			mini.alert("请先选择对方机构!");
			return;
		} 
		
		/* var ps = mini.get("direction").getValue();
		if(ps == null || ps == ""){
			mini.alert("请先选择交易方向!");
			return;
		} */
		var currencyPair = mini.get("currencyPair").getText();
		if(currencyPair == null || currencyPair == ""){
			mini.alert("请先选择货币对!");
			return;
		}
		
		//解析货币对
		var ret = parseCurrencyPair(currencyPair);
		var url;
		var data;
		
		if(ret.ctr!="CNY"){//外币
			url="./Ifs/opics/nostMini.jsp";
        	data = { ccy: ret.ctr ,cust:cpInstId};
        }else if(ret.ctr=="CNY"){//人民币
        	data = { ccy: ret.ctr ,cust:cpInstId};
        	url="./Ifs/opics/setaMini.jsp";
        }
		
		var btnEdit = this;
        mini.open({
            url: url,
            title: "选择清算路径",
            width: 700,
            height: 600,
            onload: function () {
                var iframe = this.getIFrameEl();
                iframe.contentWindow.SetData(data);
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data1 = iframe.contentWindow.GetData();
                    data1 = mini.clone(data1);    //必须
                    if (data1) {
                    	if(ret.ctr!="CNY"){//外币
                    		mini.get("ctrsmeans").setValue($.trim(data1.smeans));
                    		mini.get("ctrsmeans").setText($.trim(data1.smeans));
                            mini.get("ctrsacct").setValue($.trim(data1.nos));
                        }else if(ret.ctr=="CNY"){//人民币
                        	mini.get("ctrsmeans").setValue($.trim(data1.smeans));
	                    	 mini.get("ctrsmeans").setText($.trim(data1.smeans));
	                         mini.get("ctrsacct").setValue($.trim(data1.sacct)); 
                        }
                        
                        btnEdit.focus();
                    }
                }

            }
        });
	}
	
	
	function onCurrencyPairChange(e){
		var currencyPair = mini.get("currencyPair").getText();
		var arr=new Array();
		arr=currencyPair.split("/");
		var direction = mini.get("direction").getValue();
		if(direction=='P'){
			mini.get("buyCurreny").setValue(arr[0]);
			mini.get("sellCurreny").setValue(arr[1]);
		}else if(direction=='S'){
			mini.get("buyCurreny").setValue(arr[1]);
			mini.get("sellCurreny").setValue(arr[0]);
		}
		
	}
	</script>
		<script type="text/javascript" src="../../Common/Flow/MiniApproveOpCommon.js"></script>			
</body>
</html>