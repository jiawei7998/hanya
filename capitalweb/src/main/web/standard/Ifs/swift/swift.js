function add(){
	CommonUtil.ajax({
		url: "/IfsSwiftController/updateSwiftStatus",
		data: {batchNo: row.batchNo,userid:userId,verind:"1"},
		callback: function (data) {
			if (data.code == "error.common.0000") {
				mini.alert("经办成功",'提示信息',function(){
					top["win"].closeMenuTab();
				});
			} else {
				mini.alert("经办失败");
			}
		}
	});
}
function approve(){
	if(row.iOper == userId){
		mini.alert("经办用户和复核用户不能是同一人",'提示信息');
		return;
	}else{
		CommonUtil.ajax({
			url: "/IfsSwiftController/updateSwiftStatus",
			data: {batchNo: row.batchNo,userid:userId,verind:"2"},
			callback: function (data) {
				if (data.desc == "发送成功") {
					mini.alert("发送成功",'提示信息',function(){
						top["win"].closeMenuTab();
					});
				} else {
					mini.alert("发送失败");
				}
			}
		});		
	}
}