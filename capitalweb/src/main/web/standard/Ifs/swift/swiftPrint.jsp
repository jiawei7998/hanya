<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
	<title></title>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
	 <script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
	 <style>
 .header {
	    height: 35px;
	    /* background: #95B8E7; */
	    color: #404040;
	    background: linear-gradient(to bottom,#EFF5FF 0,#E0ECFF 100%);
	    background-repeat: repeat-x;
	    line-height:20px; /*设置line-height与父级元素的height相等*/
	    text-align: center; /*设置文本水平居中*/
	    font-size:18px;
		}
@page { margin: 0;}
	</style>
</head>
<body style="width:100%;height:100%;background:white">
<div class="mini-splitter" style="width:100%;height:100%;">
	<div size="90%" showCollapseButton="false">
		<h1 style="text-align:center;padding-top:5px;font-size:18px;"><strong>报文清算审批单</strong></h1>
	
		<div class="mini-fit"  style="background:white" id="fieldform">
			<table id="field_form" style="text-align:center;margin:auto;width:100%" class="mini-sltable">
			<tr>
			    <td colspan="2">
			        <input id="br" name="br" class="mini-textbox" labelField="true"  label="部门：" labelStyle="text-align:left;width:140px" style="width:45%" >
			        <input id="dealNo" name="dealNo" class="mini-textbox" labelField="true"  label="交易编号：" labelStyle="text-align:left;width:140px" style="width:45%" >
			        <input id="batchNo" name="batchNo" class="mini-textbox" labelField="true"  label="流水号：" labelStyle="text-align:left;width:140px" style="width:45%" >
			        <input id="prodcode" name="prodcode" class="mini-textbox" labelField="true"  label="产品代码：" labelStyle="text-align:left;width:140px" style="width:45%" >
			        <input id="prodType" name="prodType" class="mini-textbox" labelField="true"  label="产品类型：" labelStyle="text-align:left;width:140px" style="width:45%" >
			        <input id="msgType" name="msgType" class="mini-textbox" labelField="true"  label="报文类型  ：" labelStyle="text-align:left;width:140px" style="width:45%" >
			        <!-- <input id="verind" name="verind" class="mini-combobox" labelField="true"  label="流程状态：" labelStyle="text-align:left;width:170px" style="width:45%" data="CommonUtil.serverData.dictionary.verind" > -->
			        <!-- <input id="lstmntDate" name="lstmntDate" class="mini-textbox" labelField="true"  label="创建时间：" labelStyle="text-align:left;width:170px" style="width:45%" > -->
			        <input id="swftcDate" name="swftcDate" class="mini-textbox" labelField="true"  label="报文生成日期：" labelStyle="text-align:left;width:140px" style="width:45%">
			        <input id="cDate" name="cDate" class="mini-textbox" labelField="true"  label="报文处理日期：" labelStyle="text-align:left;width:140px" style="width:45%" >
<%--			        <input id="oper" name="oper" class="mini-textbox" labelField="true"  label="经办用户：" labelStyle="text-align:left;width:140px" style="width:45%" >--%>
<%--			        <input id="operDate" name="operDate" class="mini-textbox" labelField="true"  label="经办时间：" labelStyle="text-align:left;width:140px" style="width:45%" > --%>
			        <input id="verifyOper" name="verifyOper" class="mini-textbox" labelField="true"  label="操作用户：" labelStyle="text-align:left;width:140px" style="width:45%" >
			        <input id="verDate" name="verDate" class="mini-textbox" labelField="true"  label="操作时间：" labelStyle="text-align:left;width:140px" style="width:45%" >
			    </td>
			</tr>
			<tr>
				<td colspan="2" >
			    <input id="msg" name="msg" class="mini-textarea" labelField="true"  label="报文：" labelStyle="text-align:left;width:140px" style="width:90%;" >
				</td>
			</tr>
			</table>
		</div>
	</div>
	<div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
		<div style="margin-bottom:10px; text-align: center;">
			<a class="mini-button" style="display: none"  style="width:120px" id="close_btn" onclick="printdiv()">打印</a>
		</div>
		<!-- <div style="margin-bottom:10px; text-align: center;">
			<a class="mini-button" style="display: none"  style="width:120px" id="add_btn" onclick="add">经&nbsp;&nbsp;&nbsp;&nbsp;办</a>
		</div>
		<div style="margin-bottom:10px; text-align: center;">
			<a class="mini-button" style="display: none"  style="width:120px" id="approve_btn" onclick="approve">发&nbsp;&nbsp;&nbsp;&nbsp;送</a>
		</div>
		<div style="margin-bottom:10px; text-align: center;">
			<a class="mini-button" style="display: none"  style="width:120px" id="back_btn" onclick="back">退回经办</a>
		</div> -->
	</div>
</div>
	
<script type="text/javascript">
	mini.parse();
    var decode_url = decodeURI(location.href);
	var afterUrl= decode_url.substring(1);
	var params= (afterUrl.substring(afterUrl.indexOf('=')+1)).split('|');
	var batchNo=params[0];
	var dealNo=params[1];
	var msgType=params[2];
	function search(pageSize,pageIndex){
		var data={};
		data['batchNo']=batchNo;
		data['dealNo']=dealNo;
		data['msgType']=msgType;
		var params=mini.encode(data);
		CommonUtil.ajax({
			url:"/IfsSwiftController/editHandling",
			data:params,
			callback:function(data){
				// console.log(data);
				var from=new mini.Form("field_form");
				var respDate=data.obj.rows[0];
				//做时间格式化
				respDate.swftcDate=onDateRenderer(respDate.swftcDate);
				respDate.cDate=onDateRenderer(respDate.cDate);
				// console.log(respDate);
				from.setData(respDate);
				var str=respDate.msg.split("\n");
				document.getElementById("msg").style.height=str.length*30+"px";
				document.getElementById("msg$text").style.height=str.length*30+"px";
			}
		});
	}	
	function close(){
		top["win"].closeMenuTab();
	}
	
	$(document).ready(function() {
		var from = new mini.Form("field_form");
		from.setEnabled(false);
		search(10,0);
		//console.log(mini.get("msg")+"+");
	});
	 /***去除页眉页脚***/
	  function remove_ie_header_and_footer() {
		  var hkey_path;
		  hkey_path = "HKEY_CURRENT_USER\\Software\\Microsoft\\Internet Explorer\\PageSetup\\";
		  try {
		   var RegWsh = new ActiveXObject("WScript.Shell");
		   RegWsh.RegWrite(hkey_path + "header", "");
		   RegWsh.RegWrite(hkey_path + "footer", "");
		  } catch (e) {
		  }
		 }
	function onDateRenderer(time) {
		if(time!=null){
			// var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g,'/'));
			var value=new Date(time);
			if (value) return mini.formatDate(value, 'yyyy-MM-dd');
		}
	}
	/***打印***/
	function printdiv(){
		var iframe = $("#field_form");
		var elements=iframe[0].getElementsByTagName("input");
		var text=iframe[0].getElementsByTagName("textarea");
		for(var i=0; i< elements.length; i++){
           elements[i].setAttribute("value",elements[i].value);
      }
		for(var i=0; i< text.length; i++){
			text[i].innerHTML=text[i].value;
			// var id=(text[i].id).substring(0,(text[i].id).indexOf('$'));
		}
	   document.body.innerHTML='<br/><h2 style="text-align:center">报文清算审批单</h2>'+iframe.html();
		
	    if (!!window.ActiveXObject || "ActiveXObject" in window) { //是否ie
				remove_ie_header_and_footer();
			}
		window.print(); 
		window.location.reload(true);//刷新页面使页面功能正常
		return false;
	};
	var msg=mini.get("msg").getValue();
	//console.log(msg);
	//退回经办
	function back(){
		CommonUtil.ajax({
			url:"/IfsSwiftController/backHandle",
			data: {batchNo: row.batchNo},
			callback : function(data) {
				if(data != "0"){
					mini.alert("该笔已发送，不允许退回经办");
				} else {
					mini.confirm("确认退回经办？","系统警告",function(value){ 
						if (value=='ok'){
							CommonUtil.ajax({
								url:"/IfsSwiftController/backHandle",
								data: {flag: "0",batchNo: row.batchNo},
								callback : function(data) {
									if(data = "0"){
										mini.alert("退回经办成功!",'提示信息',function(){
			                				top["win"].closeMenuTab();
			                			});
									}
								}
							});
						}
					});
				}
			}
		});
	}
</script>

</body>
</html>