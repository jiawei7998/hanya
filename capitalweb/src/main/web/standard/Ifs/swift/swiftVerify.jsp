<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
  <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>
<body style="width: 100%; height: 100%; background: white">
<fieldset class="mini-fieldset">
  <legend>查询条件</legend>
  <div id="search_form" style="width: 100%;">
    <input id="dealNo" name="dealNo" class="mini-textbox" width="320px" labelField="true" label="opics编号："
      labelStyle="text-align:right;"  emptyText="opics编号" />
    <input id="msgType" name="msgType" class="mini-textbox" width="320px"  emptyText="报文类型" labelField="true"  label="报文类型："
      labelStyle="text-align:right;"/>
    <input id="settway" name="settway" class="mini-combobox" width="320px"  emptyText="清算方式" labelField="true"  label="清算方式："
      data="[{text:'NOS',id:'NOS'}]" labelStyle="text-align:right;"/>
    <input id="settFlag" name="settFlag" class="mini-combobox"  width="320px" emptyText="清算状态" labelField="true"  label="清算状态："
      data="CommonUtil.serverData.dictionary.SwiftSendFlag" labelStyle="text-align:right;"/>
<%--    <input id="settFlag" name="settFlag" class="mini-combobox" width="320px"  emptyText="清算状态：" labelField="true"  label="清算状态："--%>
<%--      data="[{text:'删除待复核',id:'4'},{text:'已删除',id:'5'},{text:'删除驳回',id:'6'}]" labelStyle="text-align:right;"/>  --%>
    <input id="swftcDate" name="swftcDate" class="mini-datepicker" width="320px" labelField="true" label="报文生成日期："
          labelStyle="text-align:right;" emptyText="请选择报文生成日期" value="<%=__bizDate%>" />
<%--    <input id="swftcDate" name="swftcDate" class="mini-datepicker"  width="320px" labelField="true" label="报文处理日期："--%>
<%--          labelStyle="text-align:right;" emptyText="请选择报文处理日期" />--%>
     <nobr>
		<input id="startDate" name="startDate" class="mini-datepicker"  width="320px" labelField="true" label="报文发送日期："
						ondrawdate="onDrawDateStart" labelStyle="text-align:right;" emptyText="起始日期" format="yyyy-MM-dd"/>
		<input id="endDate" name="endDate" class="mini-datepicker"  width="320px" labelField="true" label="～"
						ondrawdate="onDrawDateEnd" labelStyle="text-align:center;" emptyText="结束日期" format="yyyy-MM-dd" />
	</nobr>
    <!-- <input id="verind" name="verind" class="mini-combobox"  width="320px"  emptyText="流程状态" labelField="true"  label="流程状态："
      data="[{text:'经办',id:'0'},{text:'已经办待复核',id:'1'},{text:'已复核',id:'2'}]" labelStyle="text-align:right;"/> -->
  </div>
  <span style="float: right; margin-right: 50px"> 
      <a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
      <a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
  </span>
</fieldset>
<fieldset>
    <div id="find2" class="fieldset-body">
		<table id="field_form" style="margin: 0 auto;width:100%" class="mini-sltable" width="100%">
		  <tr>
			<td>交易编号：</td>
			<td>
				<input id="dealNo" name="dealNo" class="mini-textbox" readonly="true"  required="true"  style="width: 90%" allowInput="true"/>
			</td>
			<td>流水号：</td>
            <td>
                <input id="batchNo" name="batchNo" style="width:90%" readonly="true" class="mini-textbox"  />
            </td>
		 </tr>
		 <tr>
            <td>产品代码：</td>
               <td>
                  <input id="prodcode" name="prodcode" style="width:90%"  readonly="true" class="mini-textbox" />
               </td>
             <td>产品类型：</td>
                <td>
                 <input id="prodType" name="prodType" style="width:90%" readonly="true" class="mini-textbox" />
             </td>
		</tr> 
		<tr>
           <td>报文类型：</td>
           <td>
               <input id="msgType" name="msgType" style="width:90%"  readonly="true"class="mini-textbox" />
            </td>
            <td>报文生成日期：</td>
            <td>
                <input id="swftcDate" name="swftcDate" style="width:90%" readonly="true" class="mini-datepicker"  />
            </td>
        </tr> 
		<tr>
            <td>报文：</td>
            <td colspan="3">
                <input id="msg" name="msg" style="width:96%;height:120px"  readonly="true" class="mini-textarea" />
            </td>
		</tr>
		</table>
		<span class="btn_div little-tip" style="float: left;margin: 15px;">
		* 双击列表将清算信息详情展示后再进行交易签发处理。
	</span>
	<div class="btn_div" style="float: right;margin: 15px">
       <a id="submit_btn" class="mini-button" style="display: none"   onclick="send">复核</a>
       <a id="refuse_btn" class="mini-button" style="display: none"   onclick="returnConfirm">驳回</a>
       <a id="expt_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出</a>
        <a id="print_btn" class="mini-button" style="display: none"   onclick="print()">打印</a>
       <a id="check_btn" class="mini-button" style="display: none"   onclick="searchCheck()">批量查询处理状态</a>
	</div>
	</div>
</fieldset>

  <div id="approveType" name="approveType" labelField="true" label="处理列表选择：" labelStyle="border: none;background-color: #fff;"
	class="mini-checkboxlist" repeatItems="0" repeatLayout="flow" value="0" textField="text" valueField="id"
	multiSelect="false" data="[{id:'0',text:'待处理列表'},{id:'1',text:'已处理列表'}]" onvaluechanged="checkBoxValuechanged" style='margin:2px 0 12px 0'>
</div>

  <div id="datagrid" class="mini-datagrid borderAll" style="width: 100%; height: 400px;" allowAlternating="true"
    allowResize="true" border="true" sortMode="client" multiSelect="false">
    <div property="columns">
      <div type="checkcolumn"></div>
      <div type="indexcolumn" headerAlign="center" width="40">序号</div>
      <div field="br" width="50" allowSort="false" headerAlign="center" align="center">部门</div>
      <div field="dealNo" width="100" align="center" headerAlign="center" >opics编号</div>
      <div field="batchNo" width="120" allowSort="true" headerAlign="center" align="center">流水号</div>
      <div field="prodcode" width="80" allowSort="false" headerAlign="center" align="center">产品代码</div>
      <div field="msgType" width="140" allowSort="false" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'swiftCode'}">报文类型</div>
      <div field="swftcDate" width="120" allowSort="false" headerAlign="center" align="center" renderer="onDateRenderer">报文生成日期</div>
      <div field="cDate" width="120" allowSort="false" headerAlign="center" align="center" renderer="onTimeRenderer">报文生成时间</div>
      <div field="prodType" width="80" allowSort="true" headerAlign="center" align="center" >产品类型</div>
      <div field="settway" width="80" allowSort="false" headerAlign="center" align="center">清算方式</div>
      <div field="settFlag" width="140" allowSort="true" headerAlign="center" align="center"
        renderer="CommonUtil.dictRenderer" data-options="{dict:'SwiftSendFlag'}">清算状态</div>
      <div field="oper" width="80" allowSort="false" headerAlign="center" align="center">经办人</div>
      <div field="operDate" width="140" allowSort="false" headerAlign="center" align="center">经办时间</div>
      <div field="verifyOper" width="80" allowSort="false" headerAlign="center" align="center">复核操作员</div>
      <div field="verDate" width="140" allowSort="false" headerAlign="center" align="center">复核时间</div>
      <!-- <div field="dealDate"  name="dealDate" qwidth="140" allowSort="false" headerAlign="center" align="center">报文发送时间</div> -->
      <div field="swiftStatus" width="140" allowSort="false" renderer="CommonUtil.dictRenderer" data-options="{dict:'swiftStatu'}" headerAlign="center" align="center">swift状态</div>
      <!-- <div field="verind" width="100" allowSort="true" headerAlign="center" align="center" 
        renderer="CommonUtil.dictRenderer" data-options="{dict:'verind'}">流程状态</div> -->
    </div>
  </div>

<script>
  mini.parse();
  var flashFlag="1";//是否从0开始  ,0是，1不是

  var url = window.location.search;
  var prdNo = CommonUtil.getParam(url, "prdNo");
  var prdName= CommonUtil.getParam(url, "prdName");
  var grid = mini.get("datagrid");
  var userId='<%=__sessionUser.getUserId()%>';
  
	function onDateRenderer(e) {
	if(e.value!=null){
	    // var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g,'/'));
        var value=new Date(e.value);
	    if (value) return mini.formatDate(value, 'yyyy-MM-dd');
    }
  }

  function onTimeRenderer(e) {
      if(e.value!=null){
          // var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g,'/'));
          var value=new Date(e.value);
          if (value) return mini.formatDate(value, 'HH:mm:ss');
      }
  }
  
  grid.on("beforeload", function(e) {
    e.cancel = true;
    flashFlag="0";
    var pageIndex = e.data.pageIndex;
    var pageSize = e.data.pageSize;
    search(pageSize, pageIndex);
  });
//选中某一条，显示对应数据
  grid.on("select",function(e){
    var row=e.record;
	 //资金编号
	var dealNo=mini.get("dealNo");
	dealNo.setValue(row.dealNo);
	dealNo.setText(row.dealNo);
	//流水
	var batchNo=mini.get("batchNo");
	batchNo.setValue(row.batchNo);
	batchNo.setText(row.batchNo);
	//产品代码
	var prodcode=mini.get("prodcode");
	prodcode.setValue(row.prodcode);
	prodcode.setText(row.prodcode);
	//产品类型
	var prodType=mini.get("prodType");
	prodType.setValue(row.prodType);
	prodType.setText(row.prodType);
    //报文类型
    var msgType=mini.get("msgType");
    msgType.setValue(row.msgType);
    msgType.setText(row.msgType);
    //报文生成日期
    var swftcDate=mini.get("swftcDate");
      swftcDate.setValue(new Date(row.swftcDate));
    //报文
    var msg=mini.get("msg");
    msg.setValue(row.msg);
    msg.setText(row.msg);
  }); 
  
  //处理列表选择
	function checkBoxValuechanged(){
		boxChange();
		flashFlag="0";
		search(grid.pageSize,0);
	}
//待处理或者已处理列表
	function boxChange(){
		var approveType = mini.get("approveType").getValue();
		if(approveType == "0"){ // 待处理
			mini.get("submit_btn").setEnabled(true);
			mini.get("refuse_btn").setEnabled(true);
		    mini.get("expt_btn").setEnabled(false);
		    mini.get("check_btn").setEnabled(false);
		    mini.get("print_btn").setEnabled(false);

		    //grid.hideColumn("dealDate");
		}else{ // 已处理
			mini.get("submit_btn").setEnabled(false);
			mini.get("refuse_btn").setEnabled(false);
			mini.get("expt_btn").setEnabled(true);
		    mini.get("check_btn").setEnabled(true);
		    mini.get("print_btn").setEnabled(true);

		    //grid.showColumn("dealDate");
		}
	}
	//日期区间
	function onDrawDateStart(e) {
        var startDate = e.date;
        var endDate= mini.get("endDate").getValue();
        if(CommonUtil.isNull(endDate)){
        	return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }
	
	function onDrawDateEnd(e) {
        var endDate = e.date;
        var startDate = mini.get("startDate").getValue();
        if(CommonUtil.isNull(startDate)){
        	return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }
  //打印
  function print(){
	  var selections=grid.getSelecteds();
	  if(selections==null||selections.length==0){
		  mini.alert('请选择一条要打印的数据！','系统提示');
		  return;
	  }else if(selections.length>1){
		  mini.alert('系统不支持多条数据同时打印！','系统提示');
		  return;
	  }
	  var canPrint=selections[0].batchNo+'|'+selections[0].dealNo.trim()+'|'+selections[0].msgType;
	  if(!CommonUtil.isNull(canPrint)){
		  openFullScreen ("swiftPrint.jsp?params="+canPrint);
	  }
  }
  // 查询
  function search(pageSize,pageIndex){
    var form = new mini.Form("#search_form");
    form.validate();
    if(form.isValid()==false){
      mini.alert("信息填写有误，请重新填写","系统提示");
      return;
    }
    if(flashFlag=="0"){
      //是否从0开始  ,0是，1不是
      flashFlag="1";
    }else{
      pageIndex=grid.pageIndex;
    }
    var verind=null;
    var approveType=mini.get("approveType").getValue();
    if(approveType=='0'){
    	verind='1';
    }else if(approveType=='1'){
    	verind='2';
    }
    var data=form.getData();
    data['pageNumber']=pageIndex+1;
    data['pageSize']=pageSize;
    data['branchId']=branchId;
    data['verind']=verind;
	//自贸区改造加br
	data['br']=opicsBr;
    var params = mini.encode(data);
    CommonUtil.ajax({
      url:"/IfsSwiftController/editHandling",//verifyHandling
      data:params,
      callback : function(data) {
        grid.setTotalCount(data.obj.total);
        grid.setPageIndex(pageIndex);
        grid.setPageSize(pageSize);
        grid.setData(data.obj.rows);
      }
    });
  }
  
  // 双击查看详细内容
  function onRowDblClick(e) {
    var grid = mini.get("datagrid");
    var row = grid.getSelected();
    var url = CommonUtil.baseWebPath() + "/swift/swiftEdit.jsp?prdNo="+prdNo;
    var tab = { id: "MiniSwiftDetail", name: "MiniSwiftDetail", title: "Swift收/付信息", 
        url: url ,showCloseButton:true,parentId:top["win"].tabs.getActiveTab().name};
    var paramData = {selectData:row};
    CommonUtil.openNewMenuTab(tab,paramData);
  }
    
  function query() {
    flashFlag="0";
      search(grid.pageSize, 0);
    }
  
  function clear(){
     flashFlag="0";
     var form=new mini.Form("search_form");
     form.clear();
     var find2 = new mini.Form("#find2");
     find2.clear();
     search(grid.pageSize,0);
  }
  
  $(document).ready(function() {
      //控制按钮显示
      $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
          checkBoxValuechanged();
      });
  });
  
  // 发送
  function send(){
    //获取界面
    var grid = mini.get("datagrid");
    var row = grid.getSelected();
    var rows = grid.getSelecteds();
    if(rows == 0){
      mini.alert("请先选择要发送的数据");
      return;
    }
    if(row.settFlag != "2"){ // 未发送
      mini.alert("未发送状态下方可发送");
      return;
    }
   if(row.oper == userId){
        mini.alert("经办用户和复核用户不能是同一人",'提示信息');
       return;
      }
        mini.confirm("确认发送?","系统提示",function(value){
        if (value == "ok") {
          CommonUtil.ajax({
            url: "/IfsSwiftController/sendMsg",
            data: {batchNo:row.batchNo,br:row.br},
            callback: function (data) {
              mini.alert(data.desc);
              search(grid.pageSize, 0);
            }
          });
        }
      });
  }
  
  // 导出
  function exportExcel(){
    var content = grid.getData();
    if(content.length == 0){
      mini.alert("请先查询数据");
      return;
    }



    mini.confirm("您确认要导出Excel吗?","系统提示", 
      function (action) {
        if (action == "ok"){
          var form = new mini.Form("#search_form");
          var data = form.getData(true);
          var fields = null;
          for(var id in data){
            fields += '<input type="hidden" id="' + id + '" name="'+ id +'" value="' + data[id] + '">';
          }
          var approveType = mini.get("approveType").getValue();
          var verind;
          if(approveType=='0'){
            verind='1';
          }else if(approveType=='1'){
            verind='2';
          }
          fields += '<input type="hidden" id="verind" name="verind" value="' + verind + '">';
          var urls = CommonUtil.pPath + "/sl/IfsExportController/exportSwiftMsg";                                                                                                                         
          $('<form action="'+ urls +'" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();   
        }
      }
    );
  }
  
  //删除复核
function delConfirm() {
    var grid = mini.get("datagrid");
    var rows = grid.getSelecteds();
    var row = "";
    var msg = "0";//审批列表中不存在经办用户和复核用户是同一人
	for(var i=0;i<rows.length;i++){
		var oper = rows[i].oper;
		if(oper != userId){
			if(i==rows.length-1){
				row = row + rows[i].batchNo;
			} else {
				row = row + rows[i].batchNo + ",";
			}
		}else{
			msg = "1";
		}
	}
	
    if (rows) {
    	if(row == ""){
    		mini.alert("经办和复核的操作人员不能为同一个人");
    		return;
    	}else{
    		mini.confirm("您确认要复核选中记录?","系统警告",function(value){
   		      if(value=="ok"){
   		        CommonUtil.ajax({
   			        url: "/IfsSwiftController/updateSwiftComfirm",
   			        data: {row: row,sendFlag:"5"},
   			        callback: function (data) {
   			          if (data.code == 'error.common.0000') {
   			        	  if(msg == "1"){
   			        		  mini.alert("有部分数据的经办和复核人员一样");
   			        	  }else{
   			        		  mini.alert("复核成功");
   			        	  }
   			              grid.reload();
   			          } else {
   			              mini.alert("复核失败");
   			          }
   			        }
   		          });
   		       }
   		    });
    	}
    }else {
      mini.alert("请选中一条记录！", "消息提示");
    }
}
  
   //驳回
function returnConfirm() {
    var grid = mini.get("datagrid");
    var row = grid.getSelected();
    var msg = "0";//审批列表中不存在经办用户和复核用户是同一人
	if(row.oper == userId){
		msg = "1";
    }
    if (row) {
    	if(msg == "1"){
    		mini.alert("经办和复核的操作人员不能为同一个人");
    		return;
    	}else{
    		mini.confirm("您确认要驳回选中记录?","系统警告",function(value){
   		      if(value=="ok"){
   		        CommonUtil.ajax({
   			        url: "/IfsSwiftController/backHandle",
   			        data:row,
   			        callback: function (data) {
   			          if (data == '0') {
   			        	  mini.alert("驳回成功");
   			              grid.reload();
   			          } else {
   			              mini.alert("驳回失败");
   			          }
   			        }
   		          });
   		       }
   		    });
    	}
    }else {
      mini.alert("请选中一条记录！", "消息提示");
    }
}
//查询ftp发送状态
  function searchCheck(){
	    	 CommonUtil.ajax({
			        url: "/IfsSwiftController/searchSwiftStatu",
			        data:{},
			        callback: function (data) {
			        	 mini.alert(data.desc);
			        	 query();
			        }
		          });
  }
</script>
</body>
</html>