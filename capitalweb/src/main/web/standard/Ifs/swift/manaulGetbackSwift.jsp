<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
    <head>
        <title>opics系统-swift报文</title>
        <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    </head>
<body style="width:100%;height:100%;background:white">
    <fieldset class="mini-fieldset title">
    <legend>opics系统 swift报文信息查询</legend>
    <div id="search_form" style="width: 100%;">
        <input id="br" name="br" class="mini-combobox mini-mustFill" labelField="true" label="部门：" vtype="maxLength:5" required="true"
	       emptyText="请填写部门" labelStyle="text-align:right;" width="280px" value='<%=__sessionUser.getOpicsBr()%>' data="CommonUtil.serverData.dictionary.opicsBr"/>
	    <!-- <input id="br" name="br" class="mini-combobox mini-mustFill" labelField="true" label="部门：" value='<%=__sessionUser.getOpicsBr()%>' data="CommonUtil.serverData.dictionary.opicsBr"
			labelStyle="text-align:right;" width="280px" emptyText="部门"/> -->
		<input id="dealno" name="dealno" class="mini-textbox mini-mustFill" labelField="true" label="opics编号：" 
			labelStyle="text-align:right;" width="280px" emptyText="opics编号"/>
		<input id="product" name="product" class="mini-buttonedit mini-mustFill" allowInput="false" labelField="true" label="产品：" 
			onbuttonclick="onPrdEdit" labelStyle="text-align:right;" width="280px" emptyText="产品" />
		<input id="prodtype" name="prodtype" class="mini-buttonedit mini-mustFill" labelField="true" label="产品类型："
			onbuttonclick="onTypeEdit" allowInput="false" 
			labelStyle="text-align:right;" width="280px" emptyText="产品类型" />
		<input id="forDate" name="forDate" class="mini-datepicker" labelField="true" label="交易日期：" value="<%=__bizDate%>"
          labelStyle="text-align:right;" width="280px" emptyText="请选择交易日期" />	
        <span style="float:right;margin-right: 150px">
            <a class="mini-button"  onclick="manualGetback()">手动取回</a>
            <a class="mini-button"  onclick="clear()">清空</a>
        </span>
    </div>
    </fieldset>
</body>
<script>
    mini.parse();
    function manualGetback(){
    	var form = new mini.Form("#search_form");
		form.validate();
		if(form.isValid()==false){
			mini.alert(form.errorText+"不能为空","系统也提示");
			return;
		}
		var data=form.getData(true);
		if(data.dealno == "" && data.product == "" && data.prodtype == "" && data.forDate == "" ){
			mini.alert("opics编号、产品、产品类型、报文生成日期不能同时为空！");
			return;
		}
		var params = mini.encode(data);
		CommonUtil.ajax({
			url: "/IfsSwiftController/manualGetbackSwift",
			data: params,
			callback: function (data) {
                if(data.code!="RuntimeException"){
                    setTimeout(function(){
                        mini.alert(data.desc,"系统提示",function(){
                            onOk();
                        });
                    },1000);
                }
			}
		});
	}
    
    //清空
    function clear(){
        var form=new mini.Form("search_form");
        form.clear();
    }

    function CloseWindow(action) {
        if (window.CloseOwnerWindow)
            return window.CloseOwnerWindow(action);
        else
            window.close();
    }

    function onOk() {
        CloseWindow("ok");
    }

    //关闭窗口
    function onCancel() {
        CloseWindow("cancel");
    }
    
    function onPrdEdit(){
    	var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "/opics/prodMiniLess.jsp",
            title: "选择产品代码",
            width: 900,
            height: 500,
            onload: function () {
                var iframe = this.getIFrameEl();
                iframe.contentWindow.SetData({});
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.pcode);
                        btnEdit.setText(data.pcode);
                        onPrdChanged();
                        btnEdit.focus();
                    }
                }

            }
        });
    }
    function onPrdChanged(){
    	if(mini.get("prodtype").getValue()!=""){
    		mini.get("prodtype").setValue("");
        	mini.get("prodtype").setText("");
        	mini.alert("产品代码已改变，请重新选择产品类型!");
    	}
    }
    function onTypeEdit(){
    	var prd = mini.get("product").getValue();
    	if(prd == null || prd == ""){
    		mini.alert("请先选择产品代码!");
    		return;
    	}
    	var data={prd:prd};
    	var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "/opics/typeMiniLess.jsp",
            title: "选择产品类型",
            width: 900,
            height: 500,
            onload: function () {
                var iframe = this.getIFrameEl();
                iframe.contentWindow.SetData(data);
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.type);
                        btnEdit.setText(data.type);
                        btnEdit.focus();
                    }
                }

            }
        });
    }
</script>
</html>