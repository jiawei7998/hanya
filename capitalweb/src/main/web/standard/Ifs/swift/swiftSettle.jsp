<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
  <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>
<body style="width: 100%; height: 100%; background: white">
<fieldset class="mini-fieldset">
  <legend>查询条件</legend>
  <div id="search_form" style="width: 100%;">
    <input id="sendBank" name="sendBank" class="mini-textbox" width="320px"  emptyText="发报行" labelField="true"  label="发报行："
          labelStyle="text-align:right;" />
    <input id="reciverBank" name="reciverBank" class="mini-textbox"  width="320px"  emptyText="收报行" labelField="true"  label="收报行："
          labelStyle="text-align:right;" />
    <input id="msgtype" name="msgtype" class="mini-textbox"  width="320px"  emptyText="报文类型" labelField="true"  label="报文类型："
          labelStyle="text-align:right;" />
    <nobr>
			<input id="startDate" name="startDate" class="mini-datepicker" width="320px"  labelField="true" label="报文发送日期："
						ondrawdate="onDrawDateStart" labelStyle="text-align:right;" emptyText="起始日期" format="yyyy-MM-dd"  />
			<input id="endDate" name="endDate" class="mini-datepicker"  width="320px"  labelField="true" label="～"
                   ondrawdate="onDrawDateEnd" labelStyle="text-align:center;" emptyText="结束日期" format="yyyy-MM-dd" />
	</nobr>
	<input id="swiftStatus" name="swiftStatus" class="mini-combobox"  width="320px"  data="CommonUtil.serverData.dictionary.swiftStatu" emptyText="报文状态" labelField="true"  label="报文状态："
          labelStyle="text-align:right;" />
    <input id="amount" name="amount" class="mini-textbox"  width="320px" labelField="true" label="金额："
          labelStyle="text-align:right;"  emptyText="金额"/>
   
    <input id="tag20" name="tag20" class="mini-textbox"  width="320px" emptyText="报文20项" labelField="true"  label="报文20项："
          labelStyle="text-align:right;" />
    <input id="tag21" name="tag21" class="mini-textbox"  width="320px" emptyText="报文21项" labelField="true"  label="报文21项："
          labelStyle="text-align:right;" />
    <!-- <input id="swiftInst" name="swiftInst" class="mini-textbox"  width="320px"  emptyText="报文归属机构" labelField="true"  label="报文归属机构："
          labelStyle="text-align:right;" />
       -->
  <span style="float: right; margin-right: 50px">
      <a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
      <a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
  </span>
  </div>

</fieldset>
 <div id="datagrid" class="mini-datagrid borderAll" style="width: 100%; height:400px;" allowAlternating="true"
    allowResize="true" border="true" sortMode="client" multiSelect="false">
    <div property="columns">
      <div type="indexcolumn" headerAlign="center" width="40">序号</div>
      <div field="msgtype" width="80" allowSort="false" headerAlign="center" align="center">报文类型</div>
      <div field="swiftflag" width="80" allowSort="false" headerAlign="center" align="center">IO标记</div>
      <div field="sendDate" width="120" allowSort="false" headerAlign="center" align="center"
        renderer="onDateRenderer">报文发送日期</div>
      <div field="swiftStatus" width="100" allowSort="true" renderer="CommonUtil.dictRenderer" data-options="{dict:'swiftStatu'}"  headerAlign="center" align="center">报文状态</div>
      <div field="sendBank" width="140" allowSort="false" headerAlign="center" align="center">发报行</div>
      <div field="reciverBank" width="140" allowSort="false" headerAlign="center" align="center">收报行</div>
      <div field="ccy" width="140" allowSort="false" headerAlign="center" align="center">币种</div>
      <div field="amount" width="140" allowSort="false" headerAlign="center" align="center"  numberFormat="n2">金额</div>
     <!--  
     <div field="swiftInst" width="140" allowSort="false" headerAlign="center" align="center">报文归属机构</div>
    -->
    </div>
  </div>
<fieldset>
    <div id="find2" class="fieldset-body">
		<table id="field_form" style="margin:auto;width:100%" class="mini-sltable" width="100%">
		  <tr>
			 <td>报文发送日期：</td>
            <td>
                <input id="cDate" name="cDate" style="width:90%" readonly="true" class="mini-textbox"  />
            </td>
             <td>报文类型：</td>
           <td>
               <input id="msgType" name="msgType" style="width:90%"  readonly="true"class="mini-textbox" />
            </td>
			</tr>
		 <tr>
            <td>IO标记：</td>
               <td>
                  <input id="ioflag" name="ioflag" style="width:90%"  readonly="true" class="mini-textbox" />
               </td>
                <td>报文状态：</td>
           <td>
               <input id="swiftstatu" name="swiftstatu"  style="width:90%"  readonly="true"class="mini-textbox" />
            </td>
		</tr> 
		<tr>
           <td>发报行：</td>
           <td>
               <input id="sendbank" name="sendbank" style="width:90%"  readonly="true"class="mini-textbox" />
            </td>
            <td>收报行：</td>
            <td>
                <input id="reciverbank" name="reciverbank" style="width:90%" readonly="true" class="mini-textbox"  />
            </td>
        </tr> 
        <tr>
           <td>币种：</td>
           <td>
               <input id="ccy" name="ccy" style="width:90%"  readonly="true"class="mini-textbox" />
            </td>
             <td>金额：</td>
            <td>
                <input id="amt" name="amt" style="width:90%" readonly="true"  class="mini-spinner"  format="n2"  />
            </td>
        </tr>
        <tr>
           <td>报文20项：</td>
           <td>
               <input id="stag20" name="stag20" style="width:90%"  readonly="true"class="mini-textbox" />
            </td>
            <td>报文21项：</td>
            <td>
                <input id="stag21" name="stag21" style="width:90%" readonly="true" class="mini-textbox"  />
            </td>
        </tr>  
		<tr>
            <td>报文：</td>
            <td colspan="3">
                <input id="msg" name="msg" style="width:96%;height:120px"  readonly="true" class="mini-textarea" />
            </td>
		</tr>
		</table>
		<span class="btn_div little-tip" style="float: left;margin: 15px;">
		* 双击列表将清算信息详情展示
	</span>
	</div>
</fieldset>
 
 

<script>
  mini.parse();

  var flashFlag="1";//是否从0开始  ,0是，1不是
  var url = window.location.search;
  var prdNo = CommonUtil.getParam(url, "prdNo");
  var prdName= CommonUtil.getParam(url, "prdName");
  var grid = mini.get("datagrid");
  var userId='<%=__sessionUser.getUserId()%>';
  
  function onDateRenderer(e) {
	  if(e.value!=null){
			var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g,'/'));
	        if (value) return mini.formatDate(value, 'yyyy-MM-dd');
		}
  }
  
  grid.on("beforeload", function(e) {
    e.cancel = true;
    flashFlag="0";
    var pageIndex = e.data.pageIndex;
    var pageSize = e.data.pageSize;
    search(pageSize, pageIndex);
  });
//选中某一条，显示对应数据
  grid.on("select",function(e){
    var row=e.record;
    //报文类型
    var msgType=mini.get("msgType");
    msgType.setValue(row.msgtype);
    msgType.setText(row.msgtype);
    //报文生成日期
    var cDate=mini.get("cDate");
    cDate.setValue(row.sendDate);
    cDate.setText(row.sendDate);
    //报文
    var msg=mini.get("msg");
    msg.setValue(row.msg);
    msg.setText(row.msg);
    //21项
    var stag21=mini.get("stag21");
     stag21.setValue(row.tag21);
     stag21.setText(row.tag21);
    //20项
     var stag20=mini.get("stag20");
     stag20.setValue(row.tag20);
     stag20.setText(row.tag20);
    //报文状态
    var swiftstatu=mini.get("swiftstatu");
    swiftstatu.setValue(row.swiftStatus);
    var arry=CommonUtil.serverData.dictionary.swiftStatu;
    for(var i=0;i<arry.length;i++){
    	if(arry[i].id==row.swiftStatus){
        	swiftstatu.setText(arry[i].text);
    	}
    }
    
    //收报行
    var reciverbank=mini.get("reciverbank");
    reciverbank.setValue(row.reciverBank);
    reciverbank.setText(row.reciverBank);
    //发报行
     var sendbank=mini.get("sendbank");
     sendbank.setValue(row.sendBank);
     sendbank.setText(row.sendBank);
    //金额
     var amt=mini.get("amt");
     amt.setValue(row.amount);
     amt.setText(row.amount);
    //IO标记
    var ioflag=mini.get("ioflag");
    ioflag.setValue(row.swiftflag);
    ioflag.setText(row.swiftflag);
  //ccy
    var ccy=mini.get("ccy");
    ccy.setValue(row.ccy);
    ccy.setText(row.ccy);
  }); 
  // 查询
  function search(pageSize,pageIndex){
    var form = new mini.Form("#search_form");
    form.validate();
    if(form.isValid()==false){
      mini.alert("信息填写有误，请重新填写","系统提示");
      return;
    }
    if(flashFlag=="0"){
      //是否从0开始  ,0是，1不是
      flashFlag="1";
    }else{
      pageIndex=grid.pageIndex;
    }
    var data=form.getData();
    data['pageNumber']=pageIndex+1;
    data['pageSize']=pageSize;
    data['branchId']=branchId;
	//自贸区改造加br
	data['br']=opicsBr;
    var params = mini.encode(data);
    CommonUtil.ajax({
      url:"/IfsSwiftSettleController/getSettleInfo",
      data:params,
      callback : function(data) {
        grid.setTotalCount(data.obj.total);
        grid.setPageIndex(pageIndex);
        grid.setPageSize(pageSize);
        grid.setData(data.obj.rows);
      }
    });
  }
  
  function query() {
    flashFlag="0";
      search(grid.pageSize, 0);
    }
  
  function clear(){
      flashFlag="0";
      var form=new mini.Form("search_form");
      form.clear();
      var find2=new mini.Form("find2");
      find2.clear();
      //initDate();
      search(grid.pageSize,0);
  }
  $(document).ready(function() {
      //控制按钮显示
      $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
          initDate();
          search(10, 0);
      });
  });
  
  function initDate(){
	    mini.get("startDate").setValue(sysDate);
	    mini.get("endDate").setValue(sysDate);
	}
  
  
  //截止日期
	function onDrawDateEnd(e) {
		var date = e.date;
		var d = mini.get("startDate").getValue();
		if(d==null || d == ""){
			return;
		}

		if (date.getTime() < d.getTime()) {
			e.allowSelect = false;
		}
	}
   //开始日期
	function onDrawDateStart(e){
		var date = e.date;
		var d = mini.get("endDate").getValue();
		if(d==null || d == ""){
			return;
		}

		if (date.getTime() > d.getTime()) {
			e.allowSelect = false;
		}
	}
</script>
</body>
</html>