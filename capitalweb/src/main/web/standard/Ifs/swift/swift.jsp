<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
  <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
  <script type="text/javascript" src="<%=basePath%>/miniScript/hideBtn.js"></script>
</head>
<body style="width: 100%; height: 100%; background: white">
<fieldset class="mini-fieldset">
  <legend>查询条件</legend>
  <div id="search_form" style="width: 100%;">
    <input id="dealNo" name="dealNo" class="mini-textbox" width="320px" labelField="true" label="opics编号："
      labelStyle="text-align:right;" emptyText="opics编号" />
    <input id="msgType" name="msgType" class="mini-textbox" width="320px"  emptyText="报文类型" labelField="true"  label="报文类型："
      labelStyle="text-align:right;"/>
    <input id="settway" name="settway" class="mini-combobox" width="320px"  emptyText="清算方式" labelField="true"  label="清算方式："
      data="[{text:'NOS',id:'NOS'}]" labelStyle="text-align:right;"/>
    <input id="settFlag" name="settFlag" class="mini-combobox" width="320px"  emptyText="清算状态" labelField="true"  label="清算状态："
      data="CommonUtil.serverData.dictionary.SwiftSendFlag" labelStyle="text-align:right;"/>
    <input id="swftcDate" name="swftcDate" class="mini-datepicker" width="320px" labelField="true" label="报文生成日期：" value="<%=__bizDate%>"
          labelStyle="text-align:right;" emptyText="请选择报文生成日期" />
<%--        <input id="swftcDate" name="swftcDate" class="mini-datepicker" width="320px" labelField="true" label="报文处理日期：" value="<%=__bizDate%>"--%>
<%--          labelStyle="text-align:right;" emptyText="请选择报文处理日期" />--%>
    <!-- <input id="verind" name="verind" class="mini-combobox"  emptyText="流程状态" labelField="true"  label="流程状态：" 
      data="[{text:'经办',id:'0'},{text:'已经办待复核',id:'1'},{text:'已复核',id:'2'}]" labelStyle="text-align:right;"/> -->
  </div>
  <span style="float: left; margin-right: 50px">
     <!--  <a id="expt_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出</a>-->
     <!--  <a id="delete_btn" class="mini-button" style="display: none"   onclick="del">删除</a>-->
  </span>
  
  <span style="float: right; margin-right: 50px"> 
      <a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
      <a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
      <a id="update" class="mini-button" style="display: none"   onclick="manualGetback()">手动取回</a>
  </span>
</fieldset>
<fieldset>
    <div id="find2" class="fieldset-body">
		<table id="field_form" style="margin:auto;width:100%" class="mini-sltable"width="100%">
		  <tr>
			<td>交易编号：</td>
			<td>
				<input id="dealNo" name="dealNo" class="mini-textbox" readonly="true" required="true" style="width: 90%" allowInput="true"/>
			</td>
			<td>流水号：</td>
            <td>
                <input id="batchNo" name="batchNo" style="width:90%" readonly="true" class="mini-textbox"  />
            </td>
		 </tr>
		 <tr>
            <td>产品代码：</td>
               <td>
                  <input id="prodcode" name="prodcode" style="width:90%"  readonly="true" class="mini-textbox" />
               </td>
             <td>产品类型：</td>
                <td>
                 <input id="prodType" name="prodType" style="width:90%" readonly="true" class="mini-textbox" />
             </td>
		</tr> 
		<tr>
           <td>报文类型：</td>
           <td>
               <input id="msgType" name="msgType" style="width:90%"  readonly="true"class="mini-textbox" />
            </td>
            <td>报文生成日期：</td>
            <td>
                <input id="swftcDate" name="swftcDate" style="width:90%" readonly="true" class="mini-datepicker" />
            </td>
        </tr> 
		<tr>
            <td>报文：</td>
            <td colspan="3">
                <input id="msg" name="msg" style="width:96%;height:120px"  readonly="true" class="mini-textarea" />
            </td>
		</tr>
		</table>
		<span class="btn_div little-tip" style="float: left;margin: 15px;">
		* 双击列表将清算信息详情展示后再进行交易签发处理。
	</span>
	<div class="btn_div" style="float: right;margin: 15px">
      <a id="commit_btn" class="mini-button" style="display: none"   onclick="send()">发送经办</a>
	</div>
	</div>
</fieldset>
	
    <div id="datagrid" class="mini-datagrid borderAll" style="width: 100%; height: 400px;" allowAlternating="true"
        allowResize="true" border="true" sortMode="client" multiSelect="false">
        <div property="columns">
        <div type="checkcolumn"></div>
        <div type="indexcolumn" headerAlign="center" width="40">序号</div>
        <div field="br" width="50" allowSort="false" headerAlign="center" align="center">部门</div>
        <div field="dealNo" width="100" align="center" headerAlign="center" >opics编号</div>
        <div field="batchNo" width="120" allowSort="true" headerAlign="center" align="center">流水号</div>
        <div field="prodcode" width="80" allowSort="false" headerAlign="center" align="center">产品代码</div>
        <div field="msgType" width="160" allowSort="false" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'swiftCode'}">报文类型</div>
        <div field="swftcDate" width="120" allowSort="false" headerAlign="center" align="center" renderer="onDateRenderer">报文生成日期</div>
        <div field="cDate" width="120" allowSort="false" headerAlign="center" align="center" renderer="onTimeRenderer">报文生成时间</div>
        <div field="prodType" width="80" allowSort="true" headerAlign="center" align="center" >产品类型</div>
        <div field="settway" width="80" allowSort="false" headerAlign="center" align="center">清算方式</div>
        <div field="settFlag" width="100" allowSort="true" headerAlign="center" align="center"
            renderer="CommonUtil.dictRenderer" data-options="{dict:'SwiftSendFlag'}">清算状态</div>
        <div field="verind" width="100" allowSort="true" headerAlign="center" align="center"
            renderer="CommonUtil.dictRenderer" data-options="{dict:'verind'}">流程状态</div>
    </div>
</div>

<script>
  mini.parse();

  var flashFlag="1";//是否从0开始  ,0是，1不是
  var url = window.location.search;
  var prdNo = CommonUtil.getParam(url, "prdNo");
  var prdName= CommonUtil.getParam(url, "prdName");
  var grid = mini.get("datagrid");
  var userId='<%=__sessionUser.getUserId()%>';
  
  function onDateRenderer(e) {
      if(e.value!=null){
          // var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)==null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g,'/'));
          var value= new Date(e.value);
          if (value) return mini.formatDate(value, 'yyyy-MM-dd');
      }

  }

  function onTimeRenderer(e) {
      if(e.value!=null){
          // var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g,'/'));
          var value=new Date(e.value);
          if (value) return mini.formatDate(value, 'HH:mm:ss');
      }
  }
  
  grid.on("beforeload", function(e) {
    e.cancel = true;
    flashFlag="0";
    var pageIndex = e.data.pageIndex;
    var pageSize = e.data.pageSize;
    search(pageSize, pageIndex);
  });
//选中某一条，显示对应数据
  grid.on("select",function(e){
    var row=e.record;
	 //资金编号
	var dealNo=mini.get("dealNo");
	dealNo.setValue(row.dealNo);
	dealNo.setText(row.dealNo);
	//流水
	var batchNo=mini.get("batchNo");
	batchNo.setValue(row.batchNo);
	batchNo.setText(row.batchNo);
	//产品代码
	var prodcode=mini.get("prodcode");
	prodcode.setValue(row.prodcode);
	prodcode.setText(row.prodcode);
	//产品类型
	var prodType=mini.get("prodType");
	prodType.setValue(row.prodType);
	prodType.setText(row.prodType);
    //报文类型
    var msgType=mini.get("msgType");
    msgType.setValue(row.msgType);
    msgType.setText(row.msgType);
    //报文生成日期
    var swftcDate=mini.get("swftcDate");
    swftcDate.setValue(new Date(row.swftcDate));
    //报文
    var msg=mini.get("msg");
    msg.setValue(row.msg);
    msg.setText(row.msg);
  }); 
  
  // 查询
  function search(pageSize,pageIndex){
    var form = new mini.Form("#search_form");
    form.validate();
    if(form.isValid()==false){
      mini.alert("信息填写有误，请重新填写","系统也提示");
      return;
    }
    if(flashFlag=="0"){
      //是否从0开始  ,0是，1不是
      flashFlag="1";
    }else{
      pageIndex=grid.pageIndex;
    }
    var verind="0";
    var data=form.getData();
    data['pageNumber']=pageIndex+1;
    data['pageSize']=pageSize;
    data['branchId']=branchId;
    data['verind']=verind;
	//自贸区改造加br
	data['br']=opicsBr;
    var params = mini.encode(data);
    CommonUtil.ajax({
      url:"/IfsSwiftController/editHandling",
      data:params,
      callback : function(data) {
        grid.setTotalCount(data.obj.total);
        grid.setPageIndex(pageIndex);
        grid.setPageSize(pageSize);
        grid.setData(data.obj.rows);
      }
    });
  }
  
  // 双击查看详细内容
  function onRowDblClick(e) {
    var grid = mini.get("datagrid");
    var row = grid.getSelected();
    var url = CommonUtil.baseWebPath() + "/swift/swiftEdit.jsp?prdNo="+prdNo;
    var tab = { id: "MiniSwiftDetail", name: "MiniSwiftDetail", title: "Swift收/付信息", 
        url: url ,showCloseButton:true,parentId:top["win"].tabs.getActiveTab().name};
    var paramData = {selectData:row};
    CommonUtil.openNewMenuTab(tab,paramData);
  }
    
  function query() {
    flashFlag="0";
      search(grid.pageSize, 0);
    }
  
  function clear(){
    flashFlag="0";
        var form=new mini.Form("search_form");
        form.clear();
        var find2=new mini.Form("find2");
        find2.clear();
        search(10,0);
  }
  
  $(document).ready(function() {
      //控制按钮显示
      $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
          search(10, 0);
      });
  });
  
  // 发送 经办
  function send(){
    //获取界面
    var grid = mini.get("datagrid");
    var row = grid.getSelected();
    if(row == 0){
      mini.alert("请先选择要发送的数据");
      return;
    }
    if(row.settFlag != "2" && row.settFlag != "6"){ // 未发送
      mini.alert("未发送、删除驳回状态下方可发送");
      return;
    }
	   mini.confirm("确认经办?","系统提示",function(value){
	        if (value == "ok") {
	          CommonUtil.ajax({
	            url: "/IfsSwiftController/subSwiftSettle",
	            data:row,
	            callback: function (data) {
	              mini.alert(data.desc);
	              search(10, 0);
	            }
	          });
	        }
	      });
  }
  
  // 手动取回
  function manualGetback(){
    mini.open({
            url: CommonUtil.baseWebPath() + "/swift/manaulGetbackSwift.jsp",
            title: "手动取回报文",
            width: 900,
            height: 500,
            ondestroy: function (action) {
              flashFlag="0";
              search(10,0);
            }
        });
  }
  
  // 导出
  function exportExcel(){
    var content = grid.getData();
    if(content.length == 0){
      mini.alert("请先查询数据");
      return;
    }
    mini.confirm("您确认要导出Excel吗?","系统提示", 
      function (action) {
        if (action == "ok"){
          var form = new mini.Form("#search_form");
          var data = form.getData(true);
          var fields = null;
          for(var id in data){
            fields += '<input type="hidden" id="' + id + '" name="'+ id +'" value="' + data[id] + '">';
          }
          var urls = CommonUtil.pPath + "/sl/IfsExportController/exportSwiftMsg";                                                                                                                         
          $('<form action="'+ urls +'" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();   
        }
      }
    );
  }
  
  //删除
  function del() {
    var grid = mini.get("datagrid");
    var rows = grid.getSelecteds();
    var row = "";
    if(rows.length == 1){
		row = rows[0].batchNo;
	} else {
		for(var i=0;i<rows.length;i++){
			if(i==rows.length-1){
				row = row + rows[i].batchNo;
			} else {
				row = row + rows[i].batchNo + ",";
			}
		}
	}
    
    if (rows) {
      mini.confirm("您确认要删除选中记录?","系统警告",function(value){
      if(value=="ok"){
        CommonUtil.ajax({
        url: "/IfsSwiftController/deleteSwift",
        data: {row: row},
        callback: function (data) {
          if (data.code == 'error.common.0000') {
              mini.alert("删除成功");
              grid.reload();
          } else {
              mini.alert("删除失败");
          }
        }
          });
          }
        });
      }else {
        mini.alert("请选中一条记录！", "消息提示");
      }
    }
  
  
</script>
</body>
</html>