<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
	<title></title>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
	 <script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
</head>
<body style="width:100%;height:100%;background:white">
		<div  class="mini-splitter" style="width:100%;height:100%;" id="splitter">
				<div size="90%" showCollapseButton="false" >
				<h1 style="text-align:center;padding-top:5px;font-size:18px;"><strong>债券交易299报文</strong></h1>
		<div  id="field_form"  class="mini-fit area" style="background:white;">
			<div class="mini-panel" title="债券交易编号" style="width:100%;"  allowResize="true" collapseOnTitleClick="false">
			<div class="leftarea">
				<input style="width:96%;margin:5px" id="dealNo" name="dealNo" class="mini-textbox mini-mustFill" labelField="true" requiredErrorText="该输入项为必输项" label="opics编号：" labelStyle="text-align:left;width:110px;" required="true"  vtype="maxLength:8"/>
			</div>
			<div class='rightarea'>
						<div style="margin:5px 0 0 12px; text-align: left;"><a class="mini-button" style="display: none"  style="width:80px;" id="search_btn"   onclick="search">查询</a></div>
			</div>
		</div>
		 <div class="mini-panel" title="MT299报文参考编号" style="width:100%;"  allowResize="true" collapseOnTitleClick="false">
			<div class="leftarea">
				<input style="width:96%;margin:5px" id="refNo" name="refNo" class="mini-textbox mini-mustFill" labelField="true" requiredErrorText="该输入项为必输项" label="参考编号：" labelStyle="text-align:left;width:110px;" required="true"  vtype="maxLength:20"/>
			</div>
		</div>
	<div class="mini-panel" title="收/发报文方" style="width:100%"  allowResize="true" collapseOnTitleClick="false">
		<div class="leftarea" >
				<input id="sendBank" name="sendBank" class="mini-textbox mini-mustFill" labelField="true"  label="发报行："  style="width:96%;margin:5px"  labelStyle="text-align:left;width:110px;" requiredErrorText="该输入项为必输项" required="true"  vtype="maxLength:16" enabled='false'/>
		</div>
		<div class="rightarea">
				<input id="recivBank" name="recivBank" class="mini-buttonedit mini-mustFill" onbuttonclick="onButtonEdit" labelField="true"  label="收报行："  style="width:96%;margin:5px"  labelStyle="text-align:left;width:110px;" requiredErrorText="该输入项为必输项" required="true"  />
		</div>
	</div>
    <div class="mini-panel" title="报文信息" id='swiftmsg' style="width:100%"  allowResize="true" collapseOnTitleClick="false">
      <input id="msg"  name="msg"  class="mini-textarea " labelField="true"  label="报文信息：" style="width:98%;height:200px;"  labelStyle="text-align:left;width:110px;" required="true" />
	</div>
	</div>
	
</div>
		<div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
			<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:100px;" id="save_btn"   onclick="save">生成报文并添加到发送队列</a></div>
			<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:100px;" id="close_btn"   onclick="close">关闭界面</a></div>
		</div>
	</div>
	<script type="text/javascript">
	mini.parse();

	var url = window.location.search;
	var action = CommonUtil.getParam(url, "action");
	var _bizDate = '<%=__bizDate %>';
	$(document).ready(function () {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
			mini.get("sendBank").setValue("DLCBCNBD");
		});
	});
	//收报行
	function onButtonEdit(e) {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Common/CustMini.jsp",
            title: "选择收报行",
            width: 900,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.bic);
                        btnEdit.setText(data.bic);
                        btnEdit.focus();
                    }
                }

            }
        });
    }
	
	function search(){
		var dealNo=mini.get("dealNo").getValue();
		if(dealNo==null || dealNo==''){
			mini.alert('请输入opics交易单号进行查询');
			return;
		}
		var arry = {};      //获取表单多个控件的数据
		arry['dealNo']=dealNo;
		arry['msgType']='299';
		//data['pageNumber']=0;
		//data['pageSize']=999999;
		//自贸区改造加br
	    arry['br']=opicsBr;
		 var params = mini.encode(arry);
		CommonUtil.ajax({
		    url: "/IfsSwiftController/swiftBondSearch",
		    data:params,
		    callback:function (data) {
		    	if(JSON.stringify(data)!='{}'){
		    		if(data.settflag=='1'){
		    			mini.alert('债券交易编号为'+dealNo+'的299报文已发送不可修改');
		    		}else{
		    			var form = new mini.Form("field_form");
		    			data.sendBank='DLCBCNBD';
				    	form.setData(data);
				    	mini.get("recivBank").setText(data.recivBank);
		    		}
		    	}else{
		    		mini.alert('未找到债券交易编号为'+dealNo+'的299报文');
		    	}
				
		    }
		});
	}
    function close(){
		top["win"].closeMenuTab();
	}
	
	function save(){
		var form = new mini.Form("field_form");
		form.validate();
		if (form.isValid() == false) {
			mini.alert("信息填写有误，请重新填写","系统提示");
			return;
		}
		var data = form.getData(true);      //获取表单多个控件的数据
		data['br']=opicsBr;
		data['msgType']='299';
		var json = mini.encode(data);   //序列化成JSON
		CommonUtil.ajax({
		   url: "/IfsSwiftController/swiftBondSearch",
		   data:json,
		   callback:function (data) {
					  if(JSON.stringify(data)!='{}'){
						  if(data.settflag=='1'){
				    			mini.alert('债券交易编号为'+data.dealNo+'的299报文已发送不可修改');
				    			return;
				    		}
						  mini.confirm("确认修改该笔报文","操作提示",function(value){
								if (value=="ok") {
									CommonUtil.ajax({
									    url: "/IfsSwiftController/swiftBondCreat",
									    data:json,
									    callback:function (data) {
									    	mini.alert(data.desc,'提示信息',function(){
												if(data=='保存成功'){
												top["win"].closeMenuTab();
												}
											});
									    }
									});
								}
						});
					  }else{
						  mini.confirm("确认保存该笔报文","操作提示",function(value){
								if (value=="ok") {
									CommonUtil.ajax({
									    url: "/IfsSwiftController/swiftBondCreat",
									    data:json,
									    callback:function (data) {
									    	mini.alert(data.desc,'提示信息',function(){
												if(data=='保存成功'){
												top["win"].closeMenuTab();
												}
											});
									    }
									});
						       }
					 });
				 }
			}
		});
		}
	
	
	</script>
	
</body>
</html>