<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<div id="opear_form" style="width: 100%;">
	<a id="startfx_btn" class="mini-button" style="display: none"  onclick="startFx()">启动CSTP-FX-CLIENT接口服务</a>
	<a id="stopfx_btn" class="mini-button" style="display: none"  onclick="stopFx()">停止CSTP-FX-CLIENT接口服务</a>
	<a id="statusfx_btn" class="mini-button" style="display: none"  onclick="statusFx()">查询CSTP-FX-CLIENT接口状态</a>
	<a id="startrmb_btn" class="mini-button" style="display: none"  onclick="startRmb()">启动CSTP-RMB-CLIENT接口服务</a>
	<a id="stoprmb_btn" class="mini-button" style="display: none"  onclick="stopRmb()">停止CSTP-RMB-CLIENT接口服务</a>
	<a id="statusrmb_btn" class="mini-button" style="display: none"  onclick="statusRmb()">查询CSTP-RMB-CLIENT接口状态</a>
</div>
<div id="opear_form1" style="width: 100%;margin-top: 10px;">
	<a id="startcpis_btn" class="mini-button" style="display: none"  onclick="startCpis()">启动CPIS-CLIENT接口服务</a>
	<a id="stoppcpis_btn" class="mini-button" style="display: none"  onclick="stopCpis()">停止CPIS-CLIENT接口服务</a>
	<a id="statuscpis_btn" class="mini-button" style="display: none"  onclick="statusCpis()">查询CPIS-CLIENT接口状态</a>
</div>
<div>
	<p>
	CFETS接口服务启停管理及注意事项：
	</p>
	<p>
	1、【启动CSTP接口服务】启动CFETS的AGENT和客户端(仅执行一次,如果执行多次，请先停止)。
	</p>
	<p>
	2、【停止CSTP接口服务】启动CFETS的AGENT和客户端(可执行多次)。<br>
	     停止进程按照端口进行停止,涉及端口：9888、9889
	</p>
	<p>
	3、【手工查询接口服务启停状态】手工查询启动CFETS的AGENT和客户端状态(需要多次刷新)。</br>
	   视情况而定,如果是本币、外币、客户端都存在.则会看到3个以上java相关进程。
	</p>
</div>
<div>
	<input id="desc" name="desc" class="mini-textarea"
		style="width: 70%; height: 40%" enabled="false"
		borderStyle="border:1;" />
</div>

<script>
	mini.parse();
	$(function (){
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn){});
		statusFx();
	});
	$();
	function startFx() {
		CommonUtil.ajax({
			url : '/IfsCfetsManageController/startCstpFxClient',
			callback : function(data) {
				if (data) {
				    mini.get("desc").setValue("已发送外币启动命令,请等待5秒自动刷新或手动刷新!");
					setTimeout(statusFx,5000);
				}

			}
		});
	}

	function stopFx() {
		CommonUtil.ajax({
			url : '/IfsCfetsManageController/stopCstpFxClient',
			callback : function(data) {
				if (data) {
					mini.get("desc").setValue("已发送外币停止命令,请等待1秒自动刷新或手动刷新!");
					setTimeout(statusFx,1000);
				} 
			}
		});
	}
	
	function statusFx() {
		CommonUtil.ajax({
			url : '/IfsCfetsManageController/getCstpFxStatus',
			callback : function(data) {
				if (data) {
					mini.get("desc").setValue("外币命令执行成功,查询结果："+ (!data.desc ? "无活动进程!" : data.desc));
				}
			}
		});
	}
	
	function startRmb() {
		CommonUtil.ajax({
			url : '/IfsCfetsManageController/startCstpRmbClient',
			callback : function(data) {
				if (data) {
				    mini.get("desc").setValue("已发送本币启动命令,请等待5秒自动刷新或手动刷新!");
					setTimeout(statusFx,5000);
				}

			}
		});
	}

	function stopRmb() {
		CommonUtil.ajax({
			url : '/IfsCfetsManageController/stopCstpRmbClient',
			callback : function(data) {
				if (data) {
					mini.get("desc").setValue("已发送本币停止命令,请等待1秒自动刷新或手动刷新!");
					setTimeout(statusFx,1000);
				} 
			}
		});
	}
	
	function statusRmb() {
		CommonUtil.ajax({
			url : '/IfsCfetsManageController/getCstpRmbStatus',
			callback : function(data) {
				if (data) {
					mini.get("desc").setValue("本币命令执行成功,查询结果："+ (!data.desc ? "无活动进程!" : data.desc));
				}
			}
		});
	}


	function startCpis(){
		CommonUtil.ajax({
			url : '/IfsCfetsManageController/startCPISClient',
			callback : function(data) {
				if (data) {
					mini.get("desc").setValue("已发送交易后启动命令,请等待5秒自动刷新或手动刷新!");
					setTimeout(statusFx,5000);
				}

			}
		});
	}

	function stopCpis(){
		CommonUtil.ajax({
			url : '/IfsCfetsManageController/stopCpisClient',
			callback : function(data) {
				if (data) {
					mini.get("desc").setValue("已发送交易后停止命令,请等待1秒自动刷新或手动刷新!");
					setTimeout(statusFx,1000);
				}
			}
		});
	}

	function statusCpis(){
		CommonUtil.ajax({
			url : '/IfsCfetsManageController/getCpisStatus',
			callback : function(data) {
				if (data) {
					mini.get("desc").setValue("交易后确认命令执行成功,查询结果："+ (!data.desc ? "无活动进程!" : data.desc));
				}
			}
		});
	}
</script>