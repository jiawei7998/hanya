<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../../global.jsp" %>
<script type="text/javascript" src="<%=basePath%>/miniScript/miniui/res/ajaxfileupload.js"></script>

<div>
    <br/><br/>
    基金信息导入：<input class="mini-htmlfile" name="Fdata" id="file1" style="width:300px;"/>
    <br/><br/>
    &nbsp;&nbsp;&nbsp;&nbsp;<input type="button" value="上传" onclick="ajaxFileUpload()"/>
</div>
<script>

    function ajaxFileUpload() {
        var inputFile = $("#file1 > input:file")[0];
        var messageid = mini.loading("加载中,请稍后... ", "提示信息");
        $.ajaxFileUpload({
            url: '<%=basePath%>/sl/IfsWdSecController/ImportFundByExcel', //用于文件上传的服务器端请求地址
            fileElementId: inputFile,               //文件上传域的ID
            //data: { a: 1, b: true },            //附加的额外参数
            dataType: 'text',
            //返回值类型 一般设置为json
            success: function (data, status)    //服务器成功响应处理函数
            {
                var result = "导入模板不匹配";
                if (data != "") {
                    result = data;
                }
                mini.hideMessageBox(messageid);
                mini.alert(result, "消息", function () {
                    //关闭自己
                    window.CloseOwnerWindow("ok");
                });

            },
            error: function (data, status, e)   //服务器响应失败处理函数
            {
                mini.hideMessageBox(messageid);
                mini.alert(e);
            },
            complete: function () {
                var jq = $("#file1 > input:file");
                jq.before(inputFile);
                jq.remove();
            }
        });
    }
</script> 	