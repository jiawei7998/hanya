<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
  	<title>货币基金赎回维护</title>
	<script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
	<script type="text/javascript" src="../cfetsrmb/rmbVerify.js"></script>
	<script type="text/javascript" src="<%=basePath%>/standard/Common/RiskCenter.js"></script>
  </head>

<body style="width:100%;height:100%;background:white">
<div class="mini-splitter" style="width:100%;height:100%;">
	<div size="90%" showCollapseButton="false">
		<h1 style="text-align:center"><strong>货币基金赎回</strong></h1>
		<div id="field_form" class="mini-fit area"  style="background:white">
		
			<input id="sponsor" name="sponsor" class="mini-hidden" />
			<input id="sponInst" name="sponInst" class="mini-hidden" />
			<input id="aDate" name="aDate" class="mini-hidden"/>
		
			<div class="mini-panel" title="审批单号" style="width:100%;"  allowResize="true" collapseOnTitleClick="true">
				<div class="leftarea">
					<input style="width:100%;" id="dealNo" name="dealNo" class="mini-textbox" enabled="false" required="true"  labelField="true"  label="审批单号：" labelStyle="text-align:left;width:130px;"/>
				</div>
			</div>
			
			<div class="mini-panel" title="基金概况" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
				<div class="leftarea">
					<input style="width:100%" id="fundCode" name="fundCode" class="mini-buttonedit mini-mustFill" labelField="true" label="基金代码：" labelStyle="text-align:left;width:130px;" required="true"  allowInput="false" onbuttonclick="onFundName" />
					<input style="width:100%" id="fundName" name="fundName" class="mini-textbox" labelField="true" label="基金简称：" labelStyle="text-align:left;width:130px;" enabled="false" />
					<input style="width:100%" id="totalQty" name="totalQty" class="mini-spinner input-text-strong" labelField="true" label="基金规模（亿元）：" labelStyle="text-align:left;width:130px;" maxValue="9999999999999999.99" format="n2" enabled="false" />
					<input style="width:100%" id="cno" name="cno" class="mini-textbox" labelField="true" label="基金管理人编号：" labelStyle="text-align:left;width:130px;" enabled="false" />
					<input style="width:100%" id="contact" name="contact" class="mini-textbox" labelField="true" label="基金经理：" labelStyle="text-align:left;width:130px;" enabled="false" />
				</div>
				<div class="rightarea">
					<input style="width:100%" id="ccy" name="ccy" class="mini-combobox" labelField="true" label="基金币种：" labelStyle="text-align:left;width:130px;" data = "CommonUtil.serverData.dictionary.Currency" value="CNY" enabled="false" />
					<input style="width:100%" id="fundFullName" name="fundFullName" class="mini-textbox" labelField="true" label="基金全称：" labelStyle="text-align:left;width:130px;" enabled="false" />
					<input style="width:100%" id="estDate" name="estDate" class="mini-datepicker" labelField="true" label="基金成立日期：" labelStyle="text-align:left;width:130px;" enabled="false" />
					<input style="width:100%" id="cname" name="cname" class="mini-textbox" labelField="true" label="基金管理人名称："	labelStyle="text-align:left;width:130px;" enabled="false" />
					<input style="width:100%" id="contactPhone" name="contactPhone" class="mini-textbox" labelField="true" label="联系电话：" labelStyle="text-align:left;width:130px;" enabled="false" />
				</div>
			</div>
				
			<div class="mini-panel" title="交易主体信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
				<div class="leftarea">
					<input style="width:100%" id="tdate" name="tdate" class="mini-datepicker" labelField="true" label="回款日期：" labelStyle="text-align:left;width:130px;" />
					<input style="width:100%" id="utQty" name="utQty" class="mini-spinner input-text-strong"  labelField="true" label="持有份额（份）：" labelStyle="text-align:left;width:130px;" enabled="false" maxValue="9999999999999999.99" format="n2"/>
					<input style="width:100%" id="shareAmt" name="shareAmt" class="mini-spinner mini-mustFill input-text-strong" labelField="true" label="赎回份额（份）：" labelStyle="text-align:left;width:130px;" required="true"  changeOnMousewheel="false" onvalidation="zeroValidation" maxValue="9999999999999999.99" format="n2" maxLength:"20"/>
					<input style="width:100%" id="remark" name="remark" class="mini-textbox" labelField="true" label="交易事由：" labelStyle="text-align:left;width:130px;" />
				</div>
				<div class="rightarea">
					<input style="width:100%" id="vdate" name="vdate" class="mini-datepicker" labelField="true" label="赎回日期：" labelStyle="text-align:left;width:130px;" />
					<input style="width:100%" id="invType" name="invType" class="mini-combobox" labelField="true" label="会计类型："  labelStyle="text-align:left;width:130px;" data="CommonUtil.serverData.dictionary.tbAccountType" value="2" >
					<input style="width:100%" id="handleAmt" name="handleAmt" class="mini-spinner input-text-strong" labelField="true"  label="手续费（元）：" labelStyle="text-align:left;width:130px;" required="true" changeOnMousewheel="false"  maxValue="9999999999999999.99" format="n2" maxLength:"20"/>
				</div>
			</div>
			
			<%@ include file="../../Common/Flow/MiniApproveOpCommon.jsp"%>
		</div>	
	</div>
	<div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
		<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="save_btn"   onclick="save">保存交易</a></div>
		<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="close_btn"   onclick="close">关闭界面</a></div>
	</div>
</div>		
<script type="text/javascript">

	mini.parse();

	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var row=params.selectData;
	var url=window.location.search;
	var action=CommonUtil.getParam(url,"action");
	var prdNo = CommonUtil.getParam(url, "prdNo");
	var prdName = CommonUtil.getParam(url, "prdName");
	var form=new mini.Form("#field_form");
	
	var tradeData={};
	tradeData.selectData=row;
	tradeData.operType=action;
	tradeData.serial_no=row.dealNo;
	tradeData.task_id=row.taskId;
	
	//保存
	function save(){
		//表单验证！！！
		form.validate();
		if (form.isValid() == false) {
			return;
		}
		
		var utQty = mini.get("utQty").getValue();
		var shareAmt = mini.get("shareAmt").getValue();
		if(utQty < shareAmt){
			mini.alert("赎回份额不能大于持有份额！，请重新填写");
			return;
		}
		
		if(shareAmt <= 0){
		    mini.alert("确认份额");
		    return;
	    }
		
		var data=form.getData(true);
		//对数据做最后的确认
		mini.confirm("确认以手输数据为准吗？","确认",function (action) {
			if (action != "ok") {
				return;
			}
			data['prdNo']=prdNo;
			data['sponsor']="<%=__sessionUser.getUserId() %>";
			data['sponInst']="<%=__sessionInstitution.getInstId()%>";
			var params=mini.encode(data);
			CommonUtil.ajax({
				url:"/CFtRdpController/saveOrUpdateCFtRdp",
				data:params,
				callback:function(data){
					if(data.code == 'error.common.0000') {
						mini.alert(data.desc,'提示信息',function(){
							top["win"].closeMenuTab();
						});
					}else {
						mini.alert(data.desc);
					}
				}
			});
		});
	}
	//关闭页面
	function close(){
		top["win"].closeMenuTab();
	}
	//初始化页面
	$(document).ready(function(){
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
			if ($.inArray(action, ["approve", "detail"]) > -1) {
				mini.get("save_btn").setVisible(false);
				form.setEnabled(false);

				var form11 = new mini.Form("#approve_operate_form");
				form11.setEnabled(true);

			}
			if ($.inArray(action, ["edit", "approve", "detail"]) > -1) {
				form.setData(row);

				mini.get("fundCode").setEnabled(false);
				mini.get("fundCode").setValue(row.fundCode);
				mini.get("fundCode").setText(row.fundCode);

			} else {
				mini.get("sponsor").setValue(userId);
				mini.get("sponInst").setValue(instId);
				mini.get("tdate").setValue(sysDate);
				mini.get("vdate").setValue(sysDate);
			}
		});
	});
	function onFundName(e) {
        var btnEdit = this;
		var data = { prdNo: prdNo };
        mini.open({
            url: CommonUtil.baseWebPath() + "/refund/CFundTposMini.jsp",
            title: "选择基金代码",
            width: 900,
            height: 600,
			onload: function () {
				var iframe = this.getIFrameEl();
				iframe.contentWindow.SetData(data);
			},
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.fundCode);
                        btnEdit.setText(data.fundCode);
                        
                        mini.get("fundName").setValue(data.fundName);
                        mini.get("fundFullName").setValue(data.fundFullName);
                        mini.get("totalQty").setValue(data.totalQty);
                        mini.get("estDate").setValue(data.estDate);
                        mini.get("ccy").setValue(data.ccy);

                        mini.get("cno").setValue(data.managComp);
                        mini.get("contact").setValue(data.managerMen);
                        mini.get("cname").setValue(data.managCompNm);
                        mini.get("contactPhone").setValue(data.managerMenPhone);

                        mini.get("utQty").setValue(data.utQty);
                        mini.get("invType").setValue(data.invtype);
                        btnEdit.focus();
                    }
                }

            }
        });
	}
</script>
<script type="text/javascript" src="../../Common/Flow/MiniApproveOpCommon.js"></script>			
</body>
</html>
