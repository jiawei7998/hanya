<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
    <title>日终对账信息</title>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
</head>
<body style="width:100%;height:100%;background:white">
    <fieldset class="mini-fieldset title">
	    <legend>日终对账查询</legend>
		<div>
			<div id="search_form" style="width:80%" cols="6">
				<input id="newNo" name="newNo" class="mini-textbox"  width="320px"  labelField="true"  label="新核心号："  labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请输入新核心号" />
				<input id="drcrind" name="drcrind" class="mini-combobox" width="320px" data="[{'id':'CR','text':'贷'},{'id':'DR','text':'借'}]" label="借贷标识：" labelField="true" labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请选择借贷方向"/>
<%--				<input id="postdate" name="postdate" class="mini-datepicker" width="320px" labelField="true"  label="账务日期：" value="<%=__bizDate%>" labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请输入账务日期" />--%>
				<input id="postDate" name="postDate" class="mini-datepicker" labelField="true" label="对账日期："
					   labelStyle="text-align:right;" emptyText="对账日期" format="yyyy-MM-dd" />
<%--				<input id="InTime" name="InputTime" class="mini-datepicker" labelField="true" label="插入时间："--%>
<%--					   labelStyle="text-align:right;" emptyText="插入时间" format="yyyy-MM-dd" />--%>

				<span style="float: right; margin-right: 100px">
					<a id="search_btn" class="mini-button"   onclick="search()">查询</a>
					<a id="clear_btn" class="mini-button"   onclick="clear()">清空</a>
				</span>
			</div>
		</div>
    </fieldset>
    <div class="mini-fit" style="width:100%;height:100%;">
        <div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" 
            sortMode="client" allowAlternating="true"  idField="fundCode" onrowdblclick="onRowDblClick" allowResize="true" >
            <div property="columns">
	            <div type="indexcolumn" width="50px" headerAlign="center">序号</div>
				<div field="coreno" width="150px" align="center"  headerAlign="center" >核心科目号</div>
				<div field="newno" width="100px" align="center"  headerAlign="center" >新核心号</div>
				<div field="drcrname" width="100px" align="center"  headerAlign="center" >科目名称</div>
				<div field="drcrind" width="100px" align="center"  headerAlign="center" >借贷标识</div>
				<div field="bcoreamt" width="100px"   headerAlign="center" align="right" numberFormat="#,0.00">核心余额</div>
				<div field="bopicsamt" width="100px"  headerAlign="center" align="right" numberFormat="#,0.00">OPICS余额</div>
				<div field="bamt" width="100px"   headerAlign="center" align="right" numberFormat="#,0.00">余额差额</div>
				<div field="pcoreamt" width="100px"   headerAlign="center" align="right" numberFormat="#,0.00">核心发生额</div>
				<div field="popicsamt" width="100px"  headerAlign="center" align="right" numberFormat="#,0.00">OPICS发生额</div>
				<div field="pamt" width="100px"   headerAlign="center" align="right" numberFormat="#,0.00" >发生额差额</div>
				<div field="postdate" width="100px" align="center"  headerAlign="center" >对账日期</div>
				<div field="intime" width="100px" align="center"  headerAlign="center" renderer="onDateRenderer">插入时间</div>
            </div>
        </div>
    </div>
</body>
<script>
    mini.parse();
    var form = new mini.Form("#search_form");
    var grid = mini.get("datagrid");
    var url=window.location.search;
    
    grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		searchs(pageSize,pageIndex);
	});

    $(document).ready(function() {
    	search();
	});

    function search(){
    	 searchs(10,0);
    }
	function onDateRenderer(e) {
		var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)==null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g,'/'));
		if (value) return mini.formatDate(value, 'yyyy-MM-dd');
	}
    
    function searchs(pageSize, pageIndex) {
        form.validate();
        if (form.isValid() == false) {
            mini.alert("表单填写错误,请确认!", "提示信息");
            return;
        }
        var data = form.getData(true);
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        data['branchId']=branchId;
        var params = mini.encode(data);
        CommonUtil.ajax({
            url : "/IfsCoreaccountController/searchpage",
            data : params,
            callback : function(data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }
    
    function clear(){
        var form=new mini.Form("search_form");
        form.clear();
        search(10,0);
    }

    
    function onRowDblClick(e) {
		var row = grid.getSelected();
	    var lstmntdte = new Date(row.lstmntdte);
	    row.lstmntdte=lstmntdte;
	    if(row){
	            var url = CommonUtil.baseWebPath() + "/refund/ReconciliationEdit.jsp?action=detail";
	            var tab = { id: "ReconciliationDetail", name: "ReconciliationDetail", title: "日终对账详情", url: url ,showCloseButton:true};
	            var paramData = {selectData:row};
	            CommonUtil.openNewMenuTab(tab,paramData);
	    } else {
	        mini.alert("请选中一条记录！","消息提示");
	    }
    }
    
</script>
</html>