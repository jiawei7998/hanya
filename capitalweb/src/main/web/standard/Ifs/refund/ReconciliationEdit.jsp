<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
  <title>日终对账信息</title>
  <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>
<body style="width:100%;height:100%;background:white">
<div class="mini-splitter" style="width:100%;height:100%;">
	<div size="90%" showCollapseButton="false">
		<h1 style="text-align:center"><strong>日终对账</strong></h1>
		<div id="field_form" class="mini-fit area"  style="background:white">
			<div class="mini-panel" title="日终对账概况" style="width:100%;"  allowResize="true" collapseOnTitleClick="true">
				<div class="leftarea">
					<input style="width:100%;" id="coreno" name="coreno" class="mini-textbox" labelField="true" label="核心科目号：" labelStyle="text-align:left;width:130px;"  />
					<input style="width:100%;" id="drcrname" name="drcrname" class="mini-textbox" required="true"  labelField="true" label="科目名称：" labelStyle="text-align:left;width:130px;"  allowInput="false"/>
					<input style="width:100%;" id="bcoreamt" name="bcoreamt" class="mini-spinner input-text-strong" labelField="true" label="核心余额：" changeOnMousewheel="false" maxValue="9999999999999999.99" format="n2" labelStyle="text-align:left;width:130px;" />
					<input style="width:100%;" id="bamt" name="bamt" class="mini-spinner input-text-strong" labelField="true" label="余额差额：" changeOnMousewheel="false" maxValue="9999999999999999.99" format="n2" labelStyle="text-align:left;width:130px;" />
					<input style="width:100%;" id="pcoreAmt" name="pcoreAmt" class="mini-spinner input-text-strong" labelField="true" label="核心发生额：" changeOnMousewheel="false" maxValue="9999999999999999.99" format="n2" labelStyle="text-align:left;width:130px;" />
					<input style="width:100%;" id="pamt" name="pamt" class="mini-spinner input-text-strong" labelField="true" label="发生额差额：" changeOnMousewheel="false" maxValue="9999999999999999.99" format="n2" labelStyle="text-align:left;width:130px;" />
				</div>
				<div class="leftarea">
					<input style="width:100%;" id="newno" name="newno" class="mini-textbox" labelField="true"  label="新核心号："  labelStyle="text-align:left;width:130px;"/>
					<input style="width:100%;" id="drcrind" name="drcrind" class="mini-textbox" labelField="true" label="借贷标识："  labelStyle="text-align:left;width:130px;"/>
					<input style="width:100%;" id="bopicsamt" name="bopicsamt" class="mini-spinner input-text-strong" labelField="true" label="OPICS余额：" changeOnMousewheel="false" maxValue="9999999999999999.99" format="n2" labelStyle="text-align:left;width:130px;" />
					<input style="width:100%;" id="popicsamt" name="popicsamt" class="mini-spinner input-text-strong" labelField="true" label="opics发生额：" changeOnMousewheel="false" maxValue="9999999999999999.99" format="n2" labelStyle="text-align:left;width:130px;" />
					<input style="width:100%;" id="postdate" name="postdate" class="mini-datepicker" labelField="true"  label="对账日期：" labelStyle="text-align:left;width:130px;"/>
					<input style="width:100%;" id="intime" name="intime" class="mini-datepicker" format="yyyy-MM-dd" labelField="true" label="插入时间："   labelStyle="text-align:left;width:130px;">
				</div>
			</div>
		</div>	
	</div>
	<div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
		<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="width:120px;display: none" id="close_btn"   onclick="close">关闭界面</a></div>
	</div>
</div>		
<script type="text/javascript">
	mini.parse();
	top['CFundTposEdit'] = window;
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var row=params.selectData;
	var url=window.location.search;
	var action=CommonUtil.getParam(url,"action");
	var form =new mini.Form("#field_form");

	function close(){
		top["win"].closeMenuTab();
	}
	$(document).ready(function(){
		if($.inArray(action,["approve","detail"])>-1){
			form.setData(row);
			//mini.get("intime").setValue(mini.formatDate(new Date(row.intime), 'yyyy-MM-dd'));
			mini.get("intime").setText("2021-01-01");
			form.setEnabled(false);
		}
		if($.inArray(action,["edit","approve","detail"])>-1){
			form.setData(row);

			// mini.get("fundCode").setValue(row.fundCode);
			// mini.get("fundCode").setText(row.fundName);
			
		}
	});

	
</script>
<script src="<%=basePath%>/miniScript/miniMustFill.js"></script>	
</body>
</html>