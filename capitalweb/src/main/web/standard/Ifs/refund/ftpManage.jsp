<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
    <head>
        <title>FTP价格管理表</title>
        <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
        <script type="text/javascript" src="<%=basePath%>/miniScript/hideBtn.js"></script>
    </head>
<body style="width:100%;height:100%;background:white">
    <fieldset class="mini-fieldset title">
    	<legend>FTP价格信息查询</legend>
	    <div id="search_form" style="width: 100%;">
	        <input id="ftpType" name="ftpType" class="mini-combobox" data="CommonUtil.serverData.dictionary.AT0002" labelField="true" label="曲线类型：" labelStyle="text-align:right;" emptyText="请选择曲线类型" />
	       	<span style="float:right;margin-right: 150px">
	            <a class="mini-button" style="display: none"  id="search_btn"  onclick="search()">查询</a>
	            <a class="mini-button" style="display: none"  id="clear_btn"  onclick="clear()">清空</a>
	        </span>
	    </div>
    </fieldset>
    <span style="margin:2px;display: block;">
        <a class="mini-button" style="display: none"  id="add_btn"  onClick="add();">新增</a>
        <a class="mini-button" style="display: none"  id="edit_btn"  onClick="modify();">修改</a>
        <a class="mini-button" style="display: none"  id="delete_btn"  onClick="del();">删除</a>
    </span>
    <div class="mini-fit" style="width:100%;height:100%;">
        <div id="prod_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" 
            sortMode="client" allowAlternating="true"   idField="userId" onrowdblclick="onRowDblClick" 
            border="true" allowResize="true" >
            <div property="columns">
                <div type="indexcolumn" headerAlign="center" align="center" width="40px">序列</div>
				<div field="id" width="50px" align="center" headerAlign="center" allowSort="false">唯一Id</div>
				<div field="effectiveDate" width="150px" align="center"  headerAlign="center" allowSort="true">生效日期</div>
                <div field="ftpType" width="120px" align="center" headerAlign="center"  renderer="CommonUtil.dictRenderer" data-options="{dict:'AT0002'}">曲线类型</div>
                <div field="yesOrNo" width="120px" align="center" headerAlign="center"  renderer="CommonUtil.dictRenderer" data-options="{dict:'AT0002'}">是否生效</div>
				<div field="oneDay" width="120px" align="center"  headerAlign="center" numberFormat="n4">一天（元）</div>
				<div field="sevenDay" width="120px" align="center"  headerAlign="center" numberFormat="n4">七天（元）</div>
				<div field="fourteenDay" width="120px" align="center"  headerAlign="center" numberFormat="n4">十四天（元）</div>
				<div field="oneMonth" width="120px" align="center"  headerAlign="center" numberFormat="n4">一个月（元）</div>
				<div field="threeMonth" width="120px" align="center"  headerAlign="center" numberFormat="n4">三个月（元）</div>
				<div field="sixMonth" width="120px" align="center"  headerAlign="center" numberFormat="n4">六个月（元）</div>
				<div field="nineMonth" width="120px" align="center"  headerAlign="center" numberFormat="n4">九个月（元）</div>
				<div field="oneYear" width="120px" align="center"  headerAlign="center" numberFormat="n4">一年（元）</div>
				<div field="twoYear" width="120px" align="center"  headerAlign="center" numberFormat="n4">两年（元）</div>
				<div field="threeYear" width="120px" align="center"  headerAlign="center" numberFormat="n4">三年（元）</div>
				<div field="fourYear" width="120px" align="center"  headerAlign="center" numberFormat="n4">四年（元）</div>
				<div field="fiveYear" width="120px" align="center"  headerAlign="center" numberFormat="n4">五年（元）</div>
				<div field="sixYear" width="120px" align="center"  headerAlign="center" numberFormat="n4">六年（元）</div>
				<div field="sevenYear" width="120px" align="center"  headerAlign="center" numberFormat="n4">七年（元）</div>
				<div field="eightYear" width="120px" align="center"  headerAlign="center" numberFormat="n4">八年（元）</div>
				<div field="nineeYear" width="120px" align="center"  headerAlign="center" numberFormat="n4">九年（元）</div>
				<div field="tenYear" width="120px" align="center"  headerAlign="center" numberFormat="n4">十年（元）</div>
				<div field="fifteenYear" width="120px" align="center"  headerAlign="center" numberFormat="n4">十五年（元）</div>
				<div field="twentyYear" width="120px" align="center"  headerAlign="center" numberFormat="n4">二十年（元）</div>
				<div field="thirtyYear" width="120px" align="center"  headerAlign="center" numberFormat="n4">三十年（元）</div>
				<div field="effectiveDate" width="150px" align="center"  headerAlign="center" allowSort="true">操作时间</div>
            </div>
        </div>
    </div>
</body>

<script>
    mini.parse();
    var form = new mini.Form("#search_form");
    var grid = mini.get("prod_grid");
    
    grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		searchs(pageSize,pageIndex);
	});
    $(document).ready(function() {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
			search();
		});
	});

    //查询按钮
     function search(){
    	 searchs(10,0);
    } 
    function searchs(pageSize, pageIndex) {
        form.validate();
        if (form.isValid() == false) {
            mini.alert("表单填写错误,请确认!", "提示信息");
            return;
        }
        var data = form.getData(true);
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        var params = mini.encode(data);
        CommonUtil.ajax({
            url : "/FtpManageController/searchFtpManageList",
            data : params,
            callback : function(data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }
    //清空
    function clear(){
        var form=new mini.Form("search_form");
        form.clear();
        search(10,0);
    }
    //新增
    function add(){
        var url = CommonUtil.baseWebPath() + "/refund/ftpManageEdit.jsp?action=add";
        var tab = { id: "ftpAdd", name: "ftpAdd", title: "FTP价格管理新增", 
        url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
        var paramData = {selectData:""};
        CommonUtil.openNewMenuTab(tab,paramData);
    }
    //修改
    function modify(){
        var row = grid.getSelected();
        if(row){
        	var lstmntdte = new Date(row.lstmntdte);
        	row.lstmntdte=lstmntdte;
            var url = CommonUtil.baseWebPath() + "/refund/ftpManageEdit.jsp?action=edit";
            var tab = { id: "ftpEdit", name: "ftpEdit", title: "FTP价格管理修改", 
            url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
            var paramData = {selectData:row};
            CommonUtil.openNewMenuTab(tab,paramData);
        } else {
            mini.alert("请选中一条记录！","消息提示");
        }
    }
    //删除
    function del() {
        var row = grid.getSelected();
        if (row) {
            mini.confirm("您确认要删除选中记录?","系统警告",function(value){
                if(value=="ok"){
                    CommonUtil.ajax({
                        url: "/FtpManageController/deleteFtpManage",
                        data: {id: row.id},
                        callback: function (data) {
                            if (data.code == 'error.common.0000') {
                                mini.alert("删除成功!");
                                grid.reload();
                            } else {
                                mini.alert("删除失败!");
                            }
                        }
                    });
                }
            });
        }else {
            mini.alert("请选中一条记录！", "消息提示");
        }

    }
    //双击详情
    function onRowDblClick(e) {
        var row = grid.getSelected();
        var lstmntdte = new Date(row.lstmntdte);
        row.lstmntdte=lstmntdte;
        if(row){
                var url = CommonUtil.baseWebPath() + "/refund/ftpManageEdit.jsp?action=detail";
                var tab = { id: "ftpDetail", name: "ftpDetail", title: "FTP价格管理详情", url: url ,showCloseButton:true};
                var paramData = {selectData:row};
                CommonUtil.openNewMenuTab(tab,paramData);
        } else {
            mini.alert("请选中一条记录！","消息提示");
        }
    }
</script>
</html>