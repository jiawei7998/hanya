<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
    <title>基金账号信息</title>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
</head>
<body style="width:100%;height:100%;background:white">
    <fieldset class="mini-fieldset title">
	    <legend>基金账号信息查询</legend>	
		<div>
			<div id="search_form" style="width:100%" cols="6">
				<input id="managcomp" name="managcomp" class="mini-textbox" width="320px" labelField="true"  label="基金管理人编号："  labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请输入基金管理人编号" />
				<input id="managcompnm" name="managcompnm" class="mini-textbox" width="320px" labelField="true"  label="基金管理人简称：" labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请输入基金管理人简称" />
				<input id="fundaccount" name="fundaccount" class="mini-textbox" width="320px" labelField="true"  label="基金账号："  labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请输入基金账号" />
				<input id="fundtradeaccount" name="fundtradeaccount" class="mini-textbox" width="320px" labelField="true"  label="交易账号："  labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请输入交易账号" />
				<span style="float: right; margin-right: 150px"> 
					<a id="search_btn" class="mini-button" style="display: none"   onclick="search()">查询</a>
					<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
				</span>
			</div>
			<span style="margin:2px;display: block;">
				<a class="mini-button" style="display: none"  id="add_btn"  onClick="add();">新增</a>
				<a class="mini-button" style="display: none"  id="edit_btn"  onClick="modify();">修改</a>
				<a class="mini-button" style="display: none"  id="delete_btn"  onClick="del();">删除</a>
			</span>
		</div>
    </fieldset>

    <div class="mini-fit" style="width:100%;height:100%;">
        <div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" 
            sortMode="client" allowAlternating="true"  idField="fundCode" onrowdblclick="onRowDblClick" allowResize="true" >
            <div property="columns">
	            <div type="indexcolumn" width="50px" headerAlign="center">序号</div>
				<div field="managcomp" width="100px" align="center"  headerAlign="center" >基金管理人编号</div>
				<div field="managcompnm" width="100px" align="center"  headerAlign="center" >基金管理人简称</div>
				<div field="managcompfnm" width="100px" align="center"  headerAlign="center" >基金管理人全称</div>
				<div field="fundaccount" width="100px" align="center"  headerAlign="center" >基金账号</div>
				<div field="fundtradeaccount" width="100px" align="center"  headerAlign="center" >交易账号</div>
				<div field="fundaccountname" width="100px" align="center"  headerAlign="center" >户名</div>
				<div field="fundbankname" width="100px" align="center"  headerAlign="center" >开户银行名称</div>
				<div field="rhbankno" width="100px" align="center"  headerAlign="center" >银行账号</div>
				<div field="fundbankno" width="100px" align="center"  headerAlign="center" >人行支付系统行号</div>
<%--				<div field="movie" width="100px" align="center"  headerAlign="center" >影像上传</div>--%>
            </div>
        </div>
    </div>
</body>
<script>
    mini.parse();

    var form = new mini.Form("#search_form");
    var grid = mini.get("datagrid");
    var url=window.location.search;
	var action=CommonUtil.getParam(url,"action");
    
    
    grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		searchs(pageSize,pageIndex);
	});
    $(document).ready(function() {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
			search();
		});
	});

    //查询按钮
     function search(){
    	 searchs(10,0);
    } 
    function searchs(pageSize, pageIndex) {
        form.validate();
        if (form.isValid() == false) {
            mini.alert("表单填写错误,请确认!", "提示信息");
            return;
        }
        var data = form.getData(true);
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        data['branchId']=branchId;
        var params = mini.encode(data);
        CommonUtil.ajax({
            url : "/CFtFundaccountInfoController/searchCFtFundaccountInfoList",
            data : params,
            callback : function(data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }
    //清空
    function clear(){
        var form=new mini.Form("search_form");
        form.clear();
        search(10,0);
    }
    //新增
    function add(){
        var url = CommonUtil.baseWebPath() + "/refund/CFundaccountInfoEdit.jsp?action=add";
        var tab = { id: "AccountAdd", name: "AccountAdd", title: "基金账号信息新增", 
        url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
        var paramData = {selectData:""};
        CommonUtil.openNewMenuTab(tab,paramData);
    }
    //修改
    function modify(){
        var row = grid.getSelected();
        if(row){
        	var lstmntdte = new Date(row.lstmntdte);
        	row.lstmntdte=lstmntdte;
            var url = CommonUtil.baseWebPath() + "/refund/CFundaccountInfoEdit.jsp?action=edit";
            var tab = { id: "AccountEdit", name: "AccountEdit", title: "基金账号信息修改", 
            url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
            var paramData = {selectData:row};
            CommonUtil.openNewMenuTab(tab,paramData);
        } else {
            mini.alert("请选中一条记录！","消息提示");
        }
    }
    //删除
    function del(){
		var rows=grid.getSelecteds();
		if(rows.length==0){
			mini.alert("请选中一行","提示");
			return;
		}
		mini.confirm("您确认要删除选中记录?","系统警告",function(value){   
			if (value=='ok'){   
				var data=rows[0];
				params=mini.encode(data);
				CommonUtil.ajax( {
					url:"/CFtFundaccountInfoController/deleteCFtFundaccountInfo",
					data:params,
					callback : function(data) {
						mini.alert(data.desc);
						search();
					}
				});
			}
		});
	}	
    //双击详情
    function onRowDblClick(e) {
		var row = grid.getSelected();
        var lstmntdte = new Date(row.lstmntdte);
        row.lstmntdte=lstmntdte;
        if(row){
                var url = CommonUtil.baseWebPath() + "/refund/CFundaccountInfoEdit.jsp?action=detail";
                var tab = { id: "AccountDetail", name: "AccountDetail", title: "基金账号信息详情", url: url ,showCloseButton:true};
                var paramData = {selectData:row};
                CommonUtil.openNewMenuTab(tab,paramData);
        } else {
            mini.alert("请选中一条记录！","消息提示");
        }
    }
</script>
</html>