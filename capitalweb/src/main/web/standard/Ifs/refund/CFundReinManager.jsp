<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../../global.jsp" %>
<html>
<head>
    <title>红利再投</title>
    <script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
    <script type="text/javascript">
        /**prdCode用于页面投资组合的选择   该全局变量必须为prdCode*/
        var prdCode = "SECUR-ZY";
    </script>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
    <legend>红利再投查询</legend>
    <div>
        <div id="search_form" style="width:100%" cols="6">
            <input id="dealNo" name="dealNo" class="mini-textbox" labelField="true" label="审批单号：" width="280px"
                   labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请输入审批单号"/>
            <input id="fundFullName" name="fundFullName" class="mini-textbox" labelField="true" label="基金名称："
                   width="280px" labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请输入基金名称"/>
            <input id="approveStatus" name="approveStatus" class="mini-combobox"
                   data="CommonUtil.serverData.dictionary.ApproveSta" width="280px" emptyText="请选择审批状态"
                   labelField="true" label="审批状态：" labelStyle="text-align:right;"/>
            <span style="float: right; margin-right: 150px">
					<a id="search_btn" class="mini-button" style="display: none" onclick="query()">查询</a>
					<a id="clear_btn" class="mini-button" style="display: none" onclick="clear()">清空</a>
				</span>
        </div>
    </div>
</fieldset>

<%@ include file="../batchReback.jsp" %>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>

<div class="mini-fit">
    <div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:70%;" idField="id"
         allowAlternating="true" allowResize="true" sortMode="client" allowAlternating="true" multiSelect="true"
         onrowdblclick="onRowDblClick">
        <div property="columns">
            <div type="checkcolumn"></div>
            <div type="indexcolumn" width="50px" headerAlign="center">序号</div>
            <div field="prdNo" width="100" align="center" headerAlign="center" allowSort="false" visible="false"></div>
            <div field="dealNo" width="150px" align="center" headerAlign="center" allowSort="true">审批单号</div>
            <div field="taskName" width="100px" align="center" headerAlign="center">审批状态</div>
            <div field="userName" width="100px" allowSort="false" headerAlign="center" align="center">交易员</div>
            <div field="fundFullName" width="250px" align="center" headerAlign="center">基金全称</div>
            <div field="cname" width="100px" align="center" headerAlign="center">基金管理人名称</div>
            <div field="contact" width="100px" align="center" headerAlign="center">基金经理</div>
            <div field="ccy" width="100px" align="center" headerAlign="center" renderer="CommonUtil.dictRenderer"
                 data-options="{dict:'Currency'}">基金币种
            </div>
            <div field="eshareAmt" width="120px" align="center" headerAlign="center">转投份额（份）</div>
            <div field="eAmt" width="120px" align="center" headerAlign="center" numberFormat="n2">转投金额（元）</div>
            <div field="vdate" width="180px" allowSort="false" headerAlign="center" align="center">转投时间</div>
            <div field="invType" width="300px" align="center" headerAlign="center" renderer="CommonUtil.dictRenderer"
                 data-options="{dict:'tbAccountType'}">会计类型
            </div>
            <div field="sponInstName" width="180px" allowSort="false" headerAlign="center" align="center">审批发起机构</div>
            <div field="aDate" width="180px" allowSort="false" headerAlign="center" align="center">审批发起时间</div>
        </div>
    </div>

    <fieldset style="width: 100%;border:solid 1px #aaa;margin-top:8px;position:relative;">
        <legend>红利再投详情</legend>
        <div id="MiniSettleForeigDetail" style="padding:5px;" class="mini-fit area" tyle="width: 100%; height: 50%;"
             allowAlternating="true" allowResize="true" border="true" sortMode="client">
            <input class="mini-hidden" name="id"/>
            <div class="mini-panel" title="审批单号" style="width:100%;" allowResize="true" collapseOnTitleClick="true">
                <div class="leftarea">
                    <input style="width:100%;" id="dealNo" name="dealNo" class="mini-textbox mini-mustFill"
                           required="true" labelField="true" requiredErrorText="该输入项为必输项" label="审批单号："
                           labelStyle="text-align:left;width:130px;" maxLength:"20"/>
                </div>
            </div>

            <div class="mini-panel" title="基金概况" style="width:100%" allowResize="true" collapseOnTitleClick="true">
                <div class="leftarea">
                    <input style="width:100%" id="fundCode" name="fundCode" class="mini-textbox mini-mustFill"
                           labelField="true" label="基金代码：" labelStyle="text-align:left;width:130px;" required="true"/>
                    <input style="width:100%" id="fundName" name="fundName" class="mini-textbox" labelField="true"
                           label="基金简称：" labelStyle="text-align:left;width:130px;" enabled="false"/>
                    <input style="width:100%" id="totalQty" name="totalQty" class="mini-spinner input-text-strong"
                           labelField="true" label="基金规模（亿元）：" labelStyle="text-align:left;width:130px;"
                           maxValue="9999999999999999.99" format="n2" enabled="false"/>
                    <input style="width:100%" id="cno" name="cno" class="mini-textbox" labelField="true"
                           label="基金管理人编号：" labelStyle="text-align:left;width:130px;" enabled="false"/>
                    <input style="width:100%" id="contact" name="contact" class="mini-textbox" labelField="true"
                           label="基金经理：" labelStyle="text-align:left;width:130px;" enabled="false"/>
                </div>
                <div class="rightarea">
                    <input style="width:100%" id="ccy" name="ccy" class="mini-combobox" labelField="true" label="基金币种："
                           labelStyle="text-align:left;width:130px;" data="CommonUtil.serverData.dictionary.Currency"
                           value="CNY" enabled="false"/>
                    <input style="width:100%" id="fundFullName" name="fundFullName" class="mini-textbox"
                           labelField="true" label="基金全称：" labelStyle="text-align:left;width:130px;" enabled="false"/>
                    <input style="width:100%" id="estDate" name="estDate" class="mini-datepicker" labelField="true"
                           label="基金成立日期：" labelStyle="text-align:left;width:130px;" enabled="false"/>
                    <input style="width:100%" id="cname" name="cname" class="mini-textbox" labelField="true"
                           label="基金管理人名称：" labelStyle="text-align:left;width:130px;" enabled="false"/>
                    <input style="width:100%" id="contactPhone" name="contactPhone" class="mini-textbox"
                           labelField="true" label="联系电话：" labelStyle="text-align:left;width:130px;" enabled="false"/>
                </div>
            </div>

            <div class="mini-panel" title="交易主体信息" style="width:100%" allowResize="true" collapseOnTitleClick="true">
                <div class="leftarea">
                    <input style="width:100%" id="eAmt" name="eAmt" class="mini-spinner mini-mustFill input-text-strong"
                           labelField="true" label="转投红利（元）：" labelStyle="text-align:left;width:130px;" required="true"
                           changeOnMousewheel="false" onvalidation="zeroValidation" maxValue="9999999999999999.99"
                           format="n2" maxLength:"20"/>
                    <input style="width:100%" id="eshareAmt" name="eshareAmt"
                           class="mini-spinner mini-mustFill input-text-strong" labelField="true" label="转投份额（份）："
                           labelStyle="text-align:left;width:130px;" required="true" changeOnMousewheel="false"
                           onvalidation="zeroValidation" maxValue="9999999999999999.99" format="n2" maxLength:"20"/>
                    <input style="width:100%" id="vdate" name="vdate" class="mini-datepicker" labelField="true"
                           label="转投日期：" labelStyle="text-align:left;width:130px;"/>
                </div>
                <div class="rightarea">
                    <input style="width:100%" id="price" name="price"
                           class="mini-spinner mini-mustFill input-text-strong" labelField="true" label="单位净值（元）："
                           labelStyle="text-align:left;width:130px;" required="true" changeOnMousewheel="false"
                           onvalidation="zeroValidation" maxValue="9999999999999999.9999" format="n4" maxLength:"20"/>
                    <input style="width:100%" id="invType" name="invType" class="mini-combobox" labelField="true"
                           label="会计类型：" labelStyle="text-align:left;width:130px;"
                           data="CommonUtil.serverData.dictionary.tbAccountType" value="0">
                </div>
            </div>

        </div>
    </fieldset>
</div>

<script>
    mini.parse();

    var url = window.location.search;
    var prdName = CommonUtil.getParam(url, "prdName");
    var form = new mini.Form("#search_form");
    var grid = mini.get("datagrid");
    var row = "";
    var prdNo = "";

    /**************************************点击下面显示详情开始******************************/
    var from = new mini.Form("MiniSettleForeigDetail");
    from.setEnabled(false);
    var grid = mini.get("datagrid");
    var db = new mini.DataBinding();
    db.bindForm("MiniSettleForeigDetail", grid);
    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn){
            initButton();
            search(10,0);
        });
    });
    /**************************************点击下面显示详情结束******************************/
    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize, pageIndex);
    });

    //查看详情
    function onRowDblClick(e) {
        var url = CommonUtil.baseWebPath() + "/refund/CFundReinEdit.jsp?action=detail";
        var tab = {
            id: "ReinDetail", name: "ReinDetail", url: url, title: "红利再投详情",
            parentId: top["win"].tabs.getActiveTab().name, showCloseButton: true
        };
        var paramData = {selectData: grid.getSelected()};
        CommonUtil.openNewMenuTab(tab, paramData);
    }

    //清空
    function clear() {
        var MiniSettleForeigDetail = new mini.Form("MiniSettleForeigDetail");
        MiniSettleForeigDetail.clear();
        var form = new mini.Form("search_form");
        form.clear();
        query();
    }

    function getData(action) {
        var row = null;
        if (action != "add") {
            row = grid.getSelected();
        }
        return row;
    }

    //添加
    function add() {
        var url = CommonUtil.baseWebPath() + "/refund/CFundReinEdit.jsp?action=add";
        var tab = {
            id: "ReinAdd", name: "ReinAdd", url: url, title: "红利再投补录单",
            parentId: top["win"].tabs.getActiveTab().name, showCloseButton: true
        };
        var paramData = {selectData: ""};
        CommonUtil.openNewMenuTab(tab, paramData);
    }

    //修改
    function edit() {
        var row = grid.getSelected();
        if (row) {
            var url = CommonUtil.baseWebPath() + "/refund/CFundReinEdit.jsp?action=edit";
            var tab = {
                id: "ReinEdit", name: "ReinEdit", url: url, title: "红利再投补录单修改",
                parentId: top["win"].tabs.getActiveTab().name, showCloseButton: true
            };
            var paramData = {selectData: row};
            CommonUtil.openNewMenuTab(tab, paramData);
        } else {
            mini.alert("请选择一条记录", "提示");

        }
    }

    //删除
    function del() {
        var rows = grid.getSelecteds();
        if (rows.length == 0) {
            mini.alert("请选中一行", "提示");
            return;
        }
        if (rows.length > 1) {
            mini.alert("系统不支持多条数据进行删除", "消息提示");
            return;
        }
        mini.confirm("您确认要删除选中记录?", "系统警告", function (value) {
            if (value == 'ok') {
                var data = rows[0];
                params = mini.encode(data);
                CommonUtil.ajax({
                    url: "/CFtReinController/deleteCFtRein",
                    data: params,
                    callback: function (data) {
                        mini.alert(data.desc);
                        query();
                    }
                });
            }
        });
    }

    //按钮设置
    grid.on("select", function (e) {
        var row = e.record;
        mini.get("approve_mine_commit_btn").setEnabled(false);
        mini.get("approve_commit_btn").setEnabled(false);
        mini.get("edit_btn").setEnabled(false);
        mini.get("delete_btn").setEnabled(false);
        mini.get("approve_log").setEnabled(true);
        mini.get("recall_btn").setEnabled(true);
        mini.get("batch_commit_btn").setEnabled(true);
        if (row.approveStatus == "3") {//新建
            mini.get("edit_btn").setEnabled(true);
            mini.get("delete_btn").setEnabled(true);
            mini.get("approve_mine_commit_btn").setEnabled(true);
            mini.get("approve_commit_btn").setEnabled(false);
            mini.get("reback_btn").setEnabled(false);
            mini.get("recall_btn").setEnabled(false);
        }
        if (row.approveStatus == "6" || row.approveStatus == "7" || row.approveStatus == "8" || row.approveStatus == "17") {//审批通过：6    审批拒绝:5
            mini.get("edit_btn").setEnabled(false);
            mini.get("delete_btn").setEnabled(false);
            mini.get("approve_mine_commit_btn").setEnabled(false);
            mini.get("approve_commit_btn").setEnabled(true);
            mini.get("batch_approve_btn").setEnabled(false);
            mini.get("batch_commit_btn").setEnabled(false);
            mini.get("opics_check_btn").setEnabled(false);
            mini.get("reback_btn").setEnabled(false);
            mini.get("recall_btn").setEnabled(false);
        }
        if (row.approveStatus == "5") {//审批中:5
            mini.get("edit_btn").setEnabled(false);
            mini.get("delete_btn").setEnabled(false);
            mini.get("approve_mine_commit_btn").setEnabled(false);
            mini.get("approve_commit_btn").setEnabled(true);
            mini.get("batch_commit_btn").setEnabled(false);
            mini.get("reback_btn").setEnabled(true);
            mini.get("recall_btn").setEnabled(true);
        }

    });

    /* 查询 按钮事件 */
    function query() {
        search(grid.pageSize, 0);
    }

    /* 查询 */
    function search(pageSize, pageIndex) {
        form.validate();
        if (form.isValid() == false) {
            mini.alert("信息填写有误，请重新填写", "系统也提示");
            return;
        }
        var data = form.getData(true);
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        data['branchId'] = branchId;
        var url = null;
        var approveType = mini.get("approveType").getValue();
        if (approveType == "mine") {
            url = "/CFtReinController/searchCFtReinListMine";
        } else if (approveType == "approve") {
            url = "/CFtReinController/searchCFtReinListUnfinished";
        } else {
            url = "/CFtReinController/searchCFtReinListFinished";
        }
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: url,
            data: params,
            callback: function (data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }

    /**************************审批相关****************************/
    //审批日志查看
    function appLog(selections) {
        var flow_type = Approve.FlowType.VerifyApproveFlow;
        if (selections.length <= 0) {
            mini.alert("请选择要操作的数据", "系统提示");
            return;
        }
        if (selections.length > 1) {
            mini.alert("系统不支持多笔操作", "系统提示");
            return;
        }
        if (selections[0].tradeSource == "3") {
            mini.alert("初始化导入的业务没有审批日志", "系统提示");
            return false;
        }
        Approve.approveLog(flow_type, selections[0].dealNo);
    };

    //提交正式审批、待审批
    function verify(selections) {
        if (selections.length == 0) {
            mini.alert("请选中一条记录！", "消息提示");
            return false;
        } else if (selections.length > 1) {
            mini.alert("该功能不支持多笔提交，如需多笔提交请点击[批量提交]按钮", "系统提示");
            return false;
        }
        if (selections[0]["approveStatus"] != "3") {
            var url = CommonUtil.baseWebPath() + "/refund/CFundReinEdit.jsp??action=approve&ticketId=" + row.dealNo;
            var tab = {
                "id": "ReinApprove", name: "ReinApprove", url: url, title: "红利再投审批",
                parentId: top["win"].tabs.getActiveTab().name, showCloseButton: true
            };
            var paramData = {selectData: selections[0]};
            CommonUtil.openNewMenuTab(tab, paramData);
        } else {
            Approve.approveCommit(Approve.FlowType.FundReddinFlow, selections[0]["dealNo"], Approve.OrderStatus.New, "CFtReinService", selections[0]["prdNo"], function () {
                search(grid.pageSize, grid.pageIndex);
            });
        }
    };

    //审批
    function approve() {
        mini.get("approve_commit_btn").setEnabled(false);
        var messageid = mini.loading("系统正在处理...", "请稍后");
        try {
            verify(grid.getSelecteds());
        } catch (error) {

        }
        mini.hideMessageBox(messageid);
        mini.get("approve_commit_btn").setEnabled(true);
    }

    //提交审批
    function commit() {
        mini.get("approve_mine_commit_btn").setEnabled(false);
        var messageid = mini.loading("系统正在处理...", "请稍后");
        try {
            verify(grid.getSelecteds());
        } catch (error) {

        }
        mini.hideMessageBox(messageid);
        mini.get("approve_mine_commit_btn").setEnabled(true);
    }

    //审批日志
    function searchlog() {
        appLog(grid.getSelecteds());
    }

    //批量提交
    function batchCommit() {
        var rows = grid.getSelecteds();
        if (rows.length == 0) {
            mini.alert("请至少选择一条要提交的交易!", "提示");
            return;
        }
        var messageid = mini.loading("系统正在处理...", "请稍后");
        var callBack = "CFtReinService";
        var batchParams = [];
        for (var i = 0; i < rows.length; i++) {
            batchParams[i] = {
                "flow_type": Approve.FlowType.FundReddinFlow, // 流程类型
                "serial_no": rows[i].dealNo, // 业务编号
                "product_type": rows[i].prdNo, // 每个界面都有的变量
                "order_status": Approve.OrderStatus.New, // 新发起流程
                "callBackService": callBack // 回调函数
            }
        }
        mini.hideMessageBox(messageid);
        CommonUtil.ajax({
            url: "/IfsFlowController/approveBatch",
            data: {"batchParams": batchParams},
            loadingMsg: "批量提交审批中...<br>逐笔中台风控处理中,请勿关闭刷新!",
            callback: function (data) {
                if (data.code != "RuntimeException") {
                    setTimeout(function () {
                        mini.alert(data.desc, "系统提示", function () {
                            search(grid.pageSize, grid.pageIndex);
                        });
                    }, 1000);
                }
            }
        });
    }

    //撤回
    function recall() {
        var rows = grid.getSelecteds();
        if (rows.length == 0) {
            mini.alert("请选择一条要撤回的交易!", "提示");
            return;
        }
        var recallParams = new Array();
        for (var i = 0; i < rows.length; i++) {
            recallParams.push(rows[i].dealNo);
        }
        mini.confirm("您确认要撤回该交易吗?", "系统警告", function (value) {
            if (value == 'ok') {
                var data = rows[0];
                CommonUtil.ajax({
                    url: "/IfsFlowController/recall",
                    data: {"serial_nos": recallParams},
                    loadingMsg: "批量撤回中...<br>逐笔处理中,请勿关闭刷新!",
                    callback: function (data) {
                        if (data.code != "RuntimeException") {
                            setTimeout(function () {
                                mini.alert(data.desc, "系统提示", function () {
                                    search(grid.pageSize, grid.pageIndex);
                                });
                            }, 1000);
                        }
                    }
                });
            }
        });
    }

    //撤销
    function reback() {
        var rows = grid.getSelecteds();
        if (rows.length > 1) {
            mini.alert("系统不支持多条数据进行撤销", "消息提示");
            return;
        }
        if (rows.length == 0) {
            mini.alert("请选择一条要撤销的交易!", "提示");
            return;
        }
        var recallParams=new Array();
        for (var i = 0; i < rows.length; i++) {
            recallParams.push(rows[i].dealNo);
        }
        mini.confirm("您确认要撤销该交易吗?", "系统警告", function (value) {
            if (value == 'ok') {
                var data = rows[0];
                CommonUtil.ajax({
                    url: "/IfsFlowController/reback",
                    data:{"serial_nos":recallParams},
                    callback: function (data) {
                        mini.alert("已成功撤销!", "系统提示");
                        search(10, 0);
                    }
                });
            }
        });
    }

    //打印
    function print() {
        var selections = grid.getSelecteds();
        if (selections == null || selections.length == 0) {
            mini.alert('请选择一条要打印的数据！', '系统提示');
            return false;
        } else if (selections.length > 1) {
            mini.alert('暂不支持多条数据同时打印，请选择一条要打印的数据！', '系统提示');
            return false;
        }
        var canPrint = selections[0].dealNo;

        if (!CommonUtil.isNull(canPrint)) {
            var actionStr = CommonUtil.pPath + "/sl/IfsPrintUtilController/exportload/print/" + PrintNo.CFUNDREIN + "/" + canPrint;
            $('<form target="_blank" action=' + actionStr + ' method="post"></form>').appendTo('body').submit().remove();
        }
        ;
    }

    //导出报表
    function exportExcel() {
        exportExcelManin("jyhlzt.xls", "jyhlzt");
    }

</script>
</body>
</html>
