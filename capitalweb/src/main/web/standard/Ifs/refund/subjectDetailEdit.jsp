<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../../global.jsp" %>
<html>
<head>
    <title>opics余额信息</title>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>
<body style="width:100%;height:100%;background:white">
<div class="mini-splitter" style="width:100%;height:100%;">
    <div size="90%" showCollapseButton="false">
        <h1 style="text-align:center"><strong>opics余额</strong></h1>
        <div id="field_form" class="mini-fit area" style="background:white">
            <div class="mini-panel" title="opics余额信息概况" style="width:100%;" allowResize="true"
                 collapseOnTitleClick="true">
                <div class="leftarea">
                    <input style="width:100%;" id="type" name="type" class="mini-textbox" labelField="true"
                           label="核算类型：" labelStyle="text-align:left;width:130px;"/>
                    <input style="width:100%;" id="subname" name="subname" class="mini-textbox" required="true"
                           labelField="true" label="科目名称：" labelStyle="text-align:left;width:130px;"/>
                    <input style="width:100%;" id="coreno" name="coreno" class="mini-textbox" labelField="true"
                           label="老核心号：" labelStyle="text-align:left;width:130px;"/>
                    <input style="width:100%;" id="drcr" name="drcr" class="mini-combobox" labelField="true"
                           label="借贷标识：" labelStyle="text-align:left;width:130px;"
                           data="CommonUtil.serverData.dictionary.BorrowingDir"/>
                    <%--					<input style="width:100%;" id="drcrname" name="drcrname" class="mini-textbox" labelField="true"  label="借贷方向：" labelStyle="text-align:left;width:130px;" data="CommonUtil.serverData.dictionary.BorrowingDir"/>--%>
                    <input style="width:100%;" id="prepostdate" name="prepostdate" name="postDate"
                           class="mini-datepicker" labelField="true" label="批前时间："
                           labelStyle="text-align:left;width:130px;" format="yyyy-MM-dd"/>
                    <%--					<input style="width:100%;" id="utime" name="utime" class="mini-datepicker" labelField="true" label="更新时间：" labelStyle="text-align:left;width:130px;"/>--%>
                    <%--					data="CommonUtil.serverData.dictionary.BorrowingDir"--%>
                </div>
                <div class="leftarea">
                    <input style="width:100%;" id="sectype" name="sectype" class="mini-textbox" labelField="true"
                           label="债券类型：" labelStyle="text-align:left;width:130px;"/>
                    <input style="width:100%;" id="newno" name="newno" class="mini-textbox" labelField="true"
                           label="新核心号：" labelStyle="text-align:left;width:130px;"/>
                    <input style="width:100%;" id="postdate" name="postdate" class="mini-datepicker" labelField="true"
                           label="系统时间：" labelStyle="text-align:left;width:130px;" format="yyyy-MM-dd"/>
                    <input style="width:100%;" id="balanceamt" name="balanceamt" class="mini-spinner input-text-strong"
                           labelField="true" label="余额：" changeOnMousewheel="false" minValue="-9999999999999999.99"
                           maxValue="9999999999999999.99" format="n2" labelStyle="text-align:left;width:130px;"
                           maxLength:"18"/>
                    <input style="width:100%;" id="sign" name="sign" class="mini-textbox" labelField="true"
                           label="账号类型：" labelStyle="text-align:left;width:130px;"/>
                </div>
                <%--				<div style="text-align:center;">--%>
                <%--					<a id="save_btn" class="mini-button" style="display: none"    onclick="save();">保存</a>--%>
                <%--					<a id="close_btn" class="mini-button" style="display: none"    onclick="cancel">取消</a>--%>
                <%--				</div>--%>
                <div style="text-align:center;">
                    <a id="save_btn" class="mini-button" onclick="save();">保存</a>
                    <a id="close_btn" class="mini-button" onclick="cancel">取消</a>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    mini.parse();
    top['CFundTposEdit'] = window;
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var url = window.location.search;
    var action = CommonUtil.getParam(url, "action");
    var form = new mini.Form("#field_form");

    //保存操作
    function save() {
        // var balanceamt = mini.get("balanceamt").getValue();
        // row.balanceamt=balanceamt;
        var param = form.getData();
        var url = '';
        if (action == 'edit') {
            var balanceamt = mini.get("balanceamt").getValue();
            param.balanceamt = balanceamt;
            url = '/IfsSubjectdetailController/updateOpicsBalance';
        } else {
            console.log(param)
            url = '/IfsSubjectdetailController/addOpicsBalance';
        }
        CommonUtil.ajax({
            url: url,
            data: param,
            callback: function (data) {
                if (data.code == 'error.common.0000') {
                    mini.alert("操作成功", "系统提示", function () {
                        setTimeout(function () {
                            top["win"].closeMenuTab()
                        }, 10);

                    });
                } else {
                    mini.alert("操作失败", "系统提示");
                }
            }
        });
    }

    //取消按钮
    function cancel() {
        window.CloseOwnerWindow();
    }

    function close() {
        top["win"].closeMenuTab();
    }

    $(document).ready(function () {
        if ($.inArray(action, ["approve", "detail"]) > -1) {
            form.setData(params.selectData);
            form.setEnabled(false);
        }
        if ($.inArray(action, ["edit", "approve", "detail"]) > -1) {
            form.setData(params.selectData);
            form.setEnabled(false);
            mini.get("balanceamt").setEnabled(true);
        }
    })


</script>
<script src="<%=basePath%>/miniScript/miniMustFill.js"></script>
</body>
</html>