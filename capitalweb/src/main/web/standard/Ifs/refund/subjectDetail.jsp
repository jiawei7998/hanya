<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
    <title>OPICS余额信息</title>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
</head>
<body style="width:100%;height:100%;background:white">
    <fieldset class="mini-fieldset title">
	    <legend>OPICS余额查询</legend>
		<div>
			<div id="search_form" style="width:80%" cols="6">
				<input id="type" name="type" class="mini-textbox"  width="320px"  labelField="true"  label="核算类型："  labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请输入新核心号" />
				<input id="sectype" name="secType" class="mini-textbox"  width="320px"  labelField="true"  label="债券类型："  labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请输入新核心号" />
				<input id="subname" name="subName" class="mini-textbox"  width="320px"  labelField="true"  label="科目名称："  labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请输入新核心号" />
				<input id="newno" name="newNo" class="mini-textbox"  width="320px"  labelField="true"  label="新核心账号："  labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请输入新核心号" />
				<span style="float: right; margin-right: 100px">
					<a id="search_btn" class="mini-button" style="display: none"   onclick="search()">查询</a>
					<a id="add_btn" class="mini-button" style="display: none"   onclick="add()">添加</a>
					<a id="edit_btn"   class="mini-button" style="display: none"    onClick="edit();">修改</a>
					<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
				</span>
			</div>
		</div>
    </fieldset>
    <div class="mini-fit" style="width:100%;height:100%;">
        <div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" 
            sortMode="client" allowAlternating="true"  idField="fundCode" onrowdblclick="onRowDblClick" allowResize="true" >
            <div property="columns">
	            <div type="indexcolumn" width="50px" headerAlign="center">序号</div>
				<div field="type" width="150px" align="center"  headerAlign="center" >核算类型</div>
				<div field="sectype" width="100px" align="center"  headerAlign="center" >债券类型</div>
				<div field="subname" width="100px" align="center"  headerAlign="center" >科目名称</div>
				<div field="newno" width="100px" align="center"  headerAlign="center" >新核心号</div>
				<div field="coreno" width="80px" align="center"  headerAlign="center" >老核心号</div>
				<div field="drcrname" width="300px" align="center"  headerAlign="center" >借贷方向</div>
				<div field="drcr" width="80px" align="center"  headerAlign="center" renderer="rendererDrcr">借贷标识</div>
				<div field="balanceamt" width="100px" align="center"  headerAlign="center" >余额</div>
				<div field="prepostdate" width="100px" align="center"  headerAlign="center" >批前日期</div>
				<div field="postdate" width="100px" align="center"  headerAlign="center" >系统时间</div>
				<div field="utime" width="100px" align="center"  headerAlign="center" renderer="onDateRenderer" >更新时间</div>
				<div field="sign" width="100px" align="center"  headerAlign="center" >账号类型</div>
            </div>
        </div>
    </div>
</body>
<script>
    mini.parse();

    var form = new mini.Form("#search_form");
    var grid = mini.get("datagrid");
    var url=window.location.search;
    
    grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		searchs(pageSize,pageIndex);
	});
	// function rendererDrcr(e) {
	// 	if(e!=null){
	// 		if(e.value=='0'){
	// 			return "DR"
	// 		}else{
	// 			return "CR"
	// 		}
	// 	}
	// 	return "";
	// };
    $(document).ready(function() {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
			search();
		});
	});

    function search(){
    	 searchs(10,0);
    } 
    
    function searchs(pageSize, pageIndex) {
        form.validate();
        if (form.isValid() == false) {
            mini.alert("表单填写错误,请确认!", "提示信息");
            return;
        }
        var data = form.getData(true);
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        data['branchId']=branchId;
        var params = mini.encode(data);
        CommonUtil.ajax({
            url : "/IfsSubjectdetailController/searchpage",
            data : params,
            callback : function(data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }
	function onDateRenderer(e) {
		var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)==null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g,'/'));
		if (value) return mini.formatDate(value, 'yyyy-MM-dd');
	}


    function clear(){
        var form=new mini.Form("search_form");
        form.clear();
        search(10,0);
    }

	/**
	 * 添加
	 */
	function add() {
		var url = CommonUtil.baseWebPath() + "/refund/subjectDetailEdit.jsp?action=add";
		var tab = { id: "subjectDetailAdd", name: "subjectDetailAdd", text: "OPICS科目余额添加", url: url ,showCloseButton:true};
		top['win'].showTab(tab);
	}
	//修改
	function edit() {
		var row = grid.getSelected();
		if(row){
			var url = CommonUtil.baseWebPath() + "/refund/subjectDetailEdit.jsp?action=edit";
			var tab = { id: "subjectDetail", name: "subjectDetail", title: "OPICS余额详情", url: url ,showCloseButton:true};
			var paramData = {selectData:row};
			CommonUtil.openNewMenuTab(tab,paramData);
		} else {
			mini.alert("请选中一条记录！","消息提示");
		}
	}

    function onRowDblClick(e) {
		var row = grid.getSelected();
	    var lstmntdte = new Date(row.lstmntdte);
	    row.lstmntdte=lstmntdte;
	    if(row){
	            var url = CommonUtil.baseWebPath() + "/refund/subjectDetailEdit.jsp?action=detail";
	            var tab = { id: "subjectDetail", name: "subjectDetail", title: "OPICS余额详情", url: url ,showCloseButton:true};
	            var paramData = {selectData:row};
	            CommonUtil.openNewMenuTab(tab,paramData);
	    } else {
	        mini.alert("请选中一条记录！","消息提示");
	    }
    }
    
</script>
</html>