<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
    <title>基金基本信息</title>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
</head>
<body style="width:100%;height:100%;background:white">
    <fieldset class="mini-fieldset title">
	    <legend>基金基本信息查询</legend>	
		<div>
			<div id="search_form" style="width:100%" cols="6">
				<input id="fundCode" name="fundCode" class="mini-textbox" labelField="true"  label="基金代码：" width="320px" labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请输入基金代码" />
				<input id="fundName" name="fundName" class="mini-textbox" labelField="true"  label="基金全称：" width="320px" labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请输入基金名称" />
				<input id="fundType" name="fundType" class="mini-combobox" data="CommonUtil.serverData.dictionary.FundType" width="320px" emptyText="请选择基金类型" labelField="true"  label="基金类型：" labelStyle="text-align:right;"/>
				<input id="managCompNm" name="managCompNm" class="mini-textbox" labelField="true"  label="基金管理人名称：" width="320px" labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请输入基金管理人名称" />
				<span style="float: right; margin-right: 150px"> 
					<a id="search_btn" class="mini-button" style="display: none"   onclick="search()">查询</a>
					<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
				</span>
			</div>
		</div>
		<span style="margin:2px;display: block;">
			<a class="mini-button" style="display: none"  id="add_btn"  onClick="add();">新增</a>
			<a class="mini-button" style="display: none"  id="edit_btn"  onClick="modify();">修改</a>
			<a class="mini-button" style="display: none"  id="delete_btn"  onClick="del();">删除</a>
			<a class="mini-button" style="display: none"  id="get_btn"  onClick="get();">同步万德基金信息</a>
			<a  id="export_btn" class="mini-button"   style="display: none"  onclick="ExcelExport()">下载模板</a>
			<a  id="upload_btn" class="mini-button"  style="display: none"  onclick="ExcelImport()">上传基金信息</a>
		</span>
    </fieldset>

    <div class="mini-fit" style="width:100%;height:100%;">
        <div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" 
            sortMode="client" allowAlternating="true"  idField="fundCode" onrowdblclick="onRowDblClick" allowResize="true" >
            <div property="columns">
	            <div type="indexcolumn" width="50px" headerAlign="center">序号</div>
				<div field="fundCode" width="100px" align="center"  headerAlign="center" >基金代码</div>
				<div field="fundName" width="100px" align="center"  headerAlign="center" >基金简称</div>
				<div field="fundFullName" width="150px" align="center"  headerAlign="center" >基金全称</div>
				<div field="fundType" width="100px" align="center"  headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'FundType'}">基金类型</div>
				<div field="ccy" width="100px" align="center"  headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'Currency'}">基币币种</div>
				<div field="managCompNm" width="100px" align="center"  headerAlign="center" >基金管理人名称</div>
				<div field="vType" width="100px" align="center"  headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'ValueType'}">起息规则</div>
				<div field="managerMen" width="100px" align="center"  headerAlign="center" >基金经理</div>
				<div field="coverMode" width="100px" align="center"  headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'coverMode'}">份额结转方式</div>
				<div field="coverDay" width="100px" align="center"  headerAlign="center" >份额结转日期</div>
            </div>
        </div>
    </div>
</body>
<script>
    mini.parse();

    var form = new mini.Form("#search_form");
    var grid = mini.get("datagrid");
    var url=window.location.search;
	var action=CommonUtil.getParam(url,"action");
    
    
    grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		searchs(pageSize,pageIndex);
	});
    $(document).ready(function() {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
			search();
		});
	});

    //查询按钮
     function search(){
    	 searchs(10,0);
    } 
    function searchs(pageSize, pageIndex) {
        form.validate();
        if (form.isValid() == false) {
            mini.alert("表单填写错误,请确认!", "提示信息");
            return;
        }
        var data = form.getData(true);
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        data['branchId']=branchId;
        var params = mini.encode(data);
        CommonUtil.ajax({
            url : "/CFtInfoController/searchCFtInfoList",
            data : params,
            callback : function(data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }
    //清空
    function clear(){
        var form=new mini.Form("search_form");
        form.clear();
        search(10,0);
    }
    //新增
    function add(){
        var url = CommonUtil.baseWebPath() + "/refund/CFundInfoEdit.jsp?action=add";
        var tab = { id: "InfoAdd", name: "InfoAdd", title: "基金基本信息新增", 
        url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
        var paramData = {selectData:""};
        CommonUtil.openNewMenuTab(tab,paramData);
    }
    //修改
    function modify(){
        var row = grid.getSelected();
        if(row){
        	var lstmntdte = new Date(row.lstmntdte);
        	row.lstmntdte=lstmntdte;
            var url = CommonUtil.baseWebPath() + "/refund/CFundInfoEdit.jsp?action=edit";
            var tab = { id: "InfoEdit", name: "InfoEdit", title: "基金基本信息修改", 
            url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
            var paramData = {selectData:row};
            CommonUtil.openNewMenuTab(tab,paramData);
        } else {
            mini.alert("请选中一条记录！","消息提示");
        }
    }
    //删除
    function del(){
		var rows=grid.getSelecteds();
		if(rows.length==0){
			mini.alert("请选中一行","提示");
			return;
		}
		mini.confirm("您确认要删除选中记录?","系统警告",function(value){   
			if (value=='ok'){   
				var data=rows[0];
				params=mini.encode(data);
				CommonUtil.ajax( {
					url:"/CFtInfoController/deleteCFtInfo",
					data:params,
					callback : function(data) {
						mini.alert(data.desc);
						search();
					}
				});
			}
		});
	}	
    //获取市场数据
    function get(){
        
        mini.confirm("您确认要更新?","系统警告",function(value){   
            if (value=='ok'){
                
                CommonUtil.ajax( {
                    url:"/IfsWdSecController/updateFund",
                    data:{getA:1},
                    callback : function(data) {
                        if(data.code== "error.common.0000") {
                            mini.alert("市场数据更新成功！","消息提示"); 
                        }else {
                            
                            mini.alert("市场数据更新失败！","消息提示");  
                        }
                        search(); 
                    }
                }); 
                
                
                
            }
        });
        
        
        
        
                
            }
      
   
    
    
    
    
    
    //双击详情
    function onRowDblClick(e) {
		var row = grid.getSelected();
        var lstmntdte = new Date(row.lstmntdte);
        row.lstmntdte=lstmntdte;
        if(row){
                var url = CommonUtil.baseWebPath() + "/refund/CFundInfoEdit.jsp?action=detail";
                var tab = { id: "InfoDetail", name: "InfoDetail", title: "基金基本信息详情", url: url ,showCloseButton:true};
                var paramData = {selectData:row};
                CommonUtil.openNewMenuTab(tab,paramData);
        } else {
            mini.alert("请选中一条记录！","消息提示");
        }
    }


	//基金信息文件导入
	function ExcelImport(){
		mini.open({
			showModal: true,
			allowResize: true,
			url: CommonUtil.baseWebPath() +"/../Ifs/refund/CFundInfoManagerImport.jsp",
			width: 420,
			height: 300,
			title: "Excel导入",
			ondestroy: function (action) {
				search(10,0);//重新加载页面
			}
		});
	}




	//导出模板
	function ExcelExport() {
//        if(grid.totalCount=="0"){
//            mini.alert("表中无数据");
//            return;
//        }
		mini.confirm("您确认要导出Excel吗?","系统提示",
				function (action) {
					if (action == "ok"){
						var form = new mini.Form("#search_form");
						var data = form.getData(true);
						var fields = null;
						for(var id in data){
							fields += '<input type="hidden" id="' + id + '" name="'+ id +'" value="' + data[id] + '">';
						}
						var urls = CommonUtil.pPath + "/sl/IfsWdSecController/cFundInfoExcel  ";
						$('<form action="'+ urls +'" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();
					}
				}
		);
	}



</script>
</html>