<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
    <title>基金持仓信息</title>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
</head>
<body style="width:100%;height:100%;background:white">
    <fieldset class="mini-fieldset title">
	    <legend>基金持仓查询</legend>
		<div>
			<div id="search_form" style="width:80%" cols="6">
				<input id="prdNo" name="prdNo" class="mini-combobox"  width="320px" data="CommonUtil.serverData.dictionary.FundType" labelField="true"  label="产品名称："  labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请输入产品代码" />
				<input id="invtype" name="invtype" class="mini-combobox" width="320px" data="CommonUtil.serverData.dictionary.tbAccountType" label="三分类：" labelField="true" labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请选择三分类"/>
<%--				<input id="postdate" name="postdate" class="mini-datepicker" width="320px" labelField="true"  label="账务日期：" value="<%=__bizDate%>" labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请输入账务日期" />--%>
				<input id="startDate" name="startDate" class="mini-datepicker" labelField="true" label="账务日期：" value="<%=__bizDate%>"
					   ondrawdate="onDrawDateStart" labelStyle="text-align:right;" emptyText="起始日期" format="yyyy-MM-dd" />
				<span>~</span>
				<input id="endDate" name="endDate" class="mini-datepicker"
					   ondrawdate="onDrawDateEnd" emptyText="结束日期" format="yyyy-MM-dd" value="<%=__bizDate%>"/>
				</nobr>
				<input id="fundCode" name="fundCode" class="mini-buttonedit" width="320px" labelField="true"  label="基金名称：" onbuttonclick="onFundName" allowInput="false"  labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请选择基金名称" />

				<span style="float: right; margin-right: 100px">
					<a id="search_btn" class="mini-button" style="display: none"   onclick="search()">查询</a>
					<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
				</span>
			</div>
		</div>
    </fieldset>
    <div class="mini-fit" style="width:100%;height:100%;">
        <div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" 
            sortMode="client" allowAlternating="true"  idField="fundCode" onrowdblclick="onRowDblClick" allowResize="true" >
            <div property="columns">
	            <div type="indexcolumn" width="50px" headerAlign="center">序号</div>
				<div field="fundFullName" width="150px" align="center"  headerAlign="center" >基金名称</div>
				<div field="sponInstName" width="100px" align="center"  headerAlign="center" >所属机构</div>
				<div field="prdNo" width="100px" align="center"  headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'FundType'}">产品名称</div>
				<div field="postdate" width="100px" align="center"  headerAlign="center" >账务日期</div>
				<div field="utQty" width="80px" align="center"  headerAlign="center" >持仓份额</div>
				<div field="invtype" width="300px" align="center"  headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'tbAccountType'}">三分类</div>
				<div field="ccy" width="80px" align="center"  headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'Currency'}">币种</div>
				<div field="afpDate" width="100px" align="center"  headerAlign="center" >首次申购日</div>
				<div field="utAmt" width="100px" align="center"  headerAlign="center" >赎回成本</div>
				<div field="afpAmt" width="100px" align="center"  headerAlign="center" >累计申购金额</div>
				<div field="tUnplAmt" width="100px" align="center"  headerAlign="center" >累计总估值</div>
				
				<!-- <div field="revalAmt" width="80px" align="center"  headerAlign="center" >估值</div>
				<div field="isAssmt" width="100px" align="center"  headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'AT0002'}">是否估值</div>	
				<div field="ncvAmt" width="100px" align="center"  headerAlign="center" >累计已转投金额</div>
				<div field="shAmt" width="100px" align="center"  headerAlign="center" >累计已分红金额</div>
				<div field="yUnplAmt" width="100px" align="center"  headerAlign="center" >累计未付收益</div>
				<div field="newQty" width="100px" align="center"  headerAlign="center" >赎回份额-NEW</div>
				<div field="appQty" width="100px" align="center"  headerAlign="center" >赎回份额-APP</div>
				<div field="ucCost" width="100px" align="center"  headerAlign="center" >基金未确认成本</div>
				<div field="cost" width="100px" align="center"  headerAlign="center" >基金投资成本</div>
				<div field="rdpIntAmt" width="100px" align="center"  headerAlign="center" >赎回累计红利</div>
				<div field="handleAmt" width="100px" align="center"  headerAlign="center" >累计手续费</div> -->
            </div>
        </div>
    </div>
</body>
<script>
    mini.parse();

    var form = new mini.Form("#search_form");
    var grid = mini.get("datagrid");
    var url=window.location.search;
    
    grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		searchs(pageSize,pageIndex);
	});
	//成交日期
	function onDrawDateStart(e) {
		var startDate = e.date;
		var endDate= mini.get("endDate").getValue();
		if(CommonUtil.isNull(endDate)){
			return;
		}
		if (endDate.getTime() < startDate.getTime()) {
			e.allowSelect = false;
		}
	}
	function onDrawDateEnd(e) {
		var endDate = e.date;
		var startDate = mini.get("startDate").getValue();
		if(CommonUtil.isNull(startDate)){
			return;
		}
		if (endDate.getTime() < startDate.getTime()) {
			e.allowSelect = false;
		}
	}
    $(document).ready(function() {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
			search();
		});
	});

    function search(){
    	 searchs(10,0);
    } 
    
    function searchs(pageSize, pageIndex) {
        form.validate();
        if (form.isValid() == false) {
            mini.alert("表单填写错误,请确认!", "提示信息");
            return;
        }
        var data = form.getData(true);
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        data['branchId']=branchId;
        var params = mini.encode(data);
        CommonUtil.ajax({
            url : "/CFtTposController/searchCFtTposList",
            data : params,
            callback : function(data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }
    
    function clear(){
        var form=new mini.Form("search_form");
        form.clear();
        search(10,0);
    }
    
	function onFundName(e) {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "/refund/CFundInfoMini.jsp",
            title: "选择基金代码",
            width: 900,
            height: 600,
			onload: function () {
				var iframe = this.getIFrameEl();
				iframe.contentWindow.SetData({});
			},
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    
                    if (data) {
                        btnEdit.setValue(data.fundCode);
                        btnEdit.setText(data.fundFullName);
                        btnEdit.focus();
                    }
                }

            }
        });
	}
    
    function onRowDblClick(e) {
		var row = grid.getSelected();
	    var lstmntdte = new Date(row.lstmntdte);
	    row.lstmntdte=lstmntdte;
	    if(row){
	            var url = CommonUtil.baseWebPath() + "/refund/CFundTposEdit.jsp?action=detail";
	            var tab = { id: "TposDetail", name: "TposDetail", title: "基金持仓详情", url: url ,showCloseButton:true};
	            var paramData = {selectData:row};
	            CommonUtil.openNewMenuTab(tab,paramData);
	    } else {
	        mini.alert("请选中一条记录！","消息提示");
	    }
    }
    
</script>
</html>