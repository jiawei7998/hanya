<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
  <title>基金持仓信息</title>
  <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>
<body style="width:100%;height:100%;background:white">
<div class="mini-splitter" style="width:100%;height:100%;">
	<div size="90%" showCollapseButton="false">
		<h1 style="text-align:center"><strong>基金持仓信息</strong></h1>
		<div id="field_form" class="mini-fit area"  style="background:white">
			<div class="mini-panel" title="基金持仓概况" style="width:100%;"  allowResize="true" collapseOnTitleClick="true">
				<div class="leftarea">
					<input style="width:100%;" id="prdNo" name="prdNo" class="mini-combobox" labelField="true" label="产品名称：" labelStyle="text-align:left;width:130px;" data = "CommonUtil.serverData.dictionary.FundType" value="801" />
					<input style="width:100%;" id="fundCode" name="fundCode" class="mini-buttonedit" required="true"  labelField="true" label="基金名称：" labelStyle="text-align:left;width:130px;" onbuttonclick="onFundName" allowInput="false"/>
					<input style="width:100%;" id="postdate" name="postdate" class="mini-datepicker" labelField="true" label="账务日期：" labelStyle="text-align:left;width:130px;"/>	
					<input style="width:100%;" id="utQty" name="utQty" class="mini-spinner input-text-strong" labelField="true"  label="持仓份额：" labelStyle="text-align:left;width:130px;" changeOnMousewheel="false" maxValue="9999999999999999.99" format="n2" maxLength:"18"/>	
					<input style="width:100%;" id="handleAmt" name="handleAmt" class="mini-spinner input-text-strong" labelField="true"  label="累计手续费：" changeOnMousewheel="false" maxValue="9999999999999999.99" format="n2" labelStyle="text-align:left;width:130px;" maxLength:"19"/>
					<input style="width:100%;" id="isAssmt" name="isAssmt" class="mini-combobox" labelField="true" label="是否估值：" labelStyle="text-align:left;width:130px;" data="CommonUtil.serverData.dictionary.AT0002" value="1"/>	
					<input style="width:100%;" id="tUnplAmt" name="tUnplAmt" class="mini-spinner input-text-strong" labelField="true" label="累计总估值：" changeOnMousewheel="false" maxValue="9999999999999999.99" format="n2" labelStyle="text-align:left;width:130px;" maxLength:"18"/>
					<input style="width:100%;" id="ccy" name="ccy" class="mini-combobox" required="true"  labelField="true" label="币种：" labelStyle="text-align:left;width:130px;" data = "CommonUtil.serverData.dictionary.Currency" value="CNY" />
				</div>
				<div class="leftarea">
					<input style="width:100%;" id="invtype" name="invtype" class="mini-combobox" labelField="true"  label="三分类：" data = "CommonUtil.serverData.dictionary.tbAccountType" value="0" labelStyle="text-align:left;width:130px;"/>
					<input style="width:100%;" id="managCompNm" name="managCompNm" class="mini-textbox" labelField="true" label="基金管理人名称："  labelStyle="text-align:left;width:130px;"/>
					<input style="width:100%;" id="sponInstName" name="sponInstName" class="mini-textbox" labelField="true"  label="所属机构：" labelStyle="text-align:left;width:130px;"  maxLength:"20"/>
					<input style="width:100%;" id="yield8" name="yield8" class="mini-spinner input-text-strong" labelField="true" label="万份收益率净值：" labelStyle="text-align:left;width:130px;" changeOnMousewheel="false" maxValue="9999999999999999.9999" format="n4" maxLength:"9"/>
					<input style="width:100%;" id="afpDate" name="afpDate" class="mini-datepicker" labelField="true"  label="首次申购日：" labelStyle="text-align:left;width:130px;"/>		
					<input style="width:100%;" id="revalAmt" name="revalAmt" class="mini-spinner input-text-strong" labelField="true" label="估值：" labelStyle="text-align:left;width:130px;" changeOnMousewheel="false" maxValue="9999999999999999.99" format="n2" maxLength:"18"/>	
					<input style="width:100%;" id="yUnplAmt" name="yUnplAmt" class="mini-spinner input-text-strong" labelField="true"  label="累计未付收益：" changeOnMousewheel="false" maxValue="9999999999999999.99" format="n2" labelStyle="text-align:left;width:130px;" maxLength:"18"/>
				</div>	
			</div>
			<div class="mini-panel" title="申购相关" style="width:100%;"  allowResize="true" collapseOnTitleClick="true">
				<div class="leftarea">	
					<input style="width:100%;" id="cost" name="cost" class="mini-spinner input-text-strong" labelField="true"  label="基金投资成本：" labelStyle="text-align:left;width:130px;" changeOnMousewheel="false" maxValue="9999999999999999.99" format="n2" maxLength:"18"/>
					<input style="width:100%;" id="afpAmt" name="afpAmt" class="mini-spinner input-text-strong" labelField="true"  label="累计申购金额：" changeOnMousewheel="false" maxValue="9999999999999999.99" format="n2" labelStyle="text-align:left;width:130px;" maxLength:"19"/>					
				</div>
				<div class="leftarea">	
					<input style="width:100%;" id="ucCost" name="ucCost" class="mini-spinner input-text-strong" labelField="true" label="基金未确认成本：" labelStyle="text-align:left;width:130px;" changeOnMousewheel="false" maxValue="9999999999999999.99" format="n2" maxLength:"18"/>	
				</div>	
			</div>
			<div class="mini-panel" title="赎回相关" style="width:100%;"  allowResize="true" collapseOnTitleClick="true">
				<div class="leftarea">	
					<input style="width:100%;" id="newQty" name="newQty" class="mini-spinner input-text-strong" labelField="true"  label="赎回份额-NEW：" changeOnMousewheel="false" maxValue="9999999999999999.99" format="n2" labelStyle="text-align:left;width:130px;" maxLength:"19"/>	
					<input style="width:100%;" id="utAmt" name="utAmt" class="mini-spinner input-text-strong" labelField="true" label="赎回成本：" changeOnMousewheel="false" maxValue="9999999999999999.99" format="n2" labelStyle="text-align:left;width:130px;" maxLength:"18"/>
				</div>
				<div class="leftarea">			
					<input style="width:100%;" id="appQty" name="appQty" class="mini-spinner input-text-strong" labelField="true" label="赎回份额-APP：" changeOnMousewheel="false" maxValue="9999999999999999" labelStyle="text-align:left;width:130px;" maxLength:"19"/>	
					<input style="width:100%;" id="rdpIntAmt" name="rdpIntAmt" class="mini-spinner input-text-strong" labelField="true" label="赎回累计红利：" changeOnMousewheel="false" maxValue="9999999999999999.99" format="n2" labelStyle="text-align:left;width:130px;" maxLength:"19"/>
				</div>	
			</div>
			<div class="mini-panel" title="分红相关" style="width:100%;"  allowResize="true" collapseOnTitleClick="true">
				<div class="leftarea">	
					<input style="width:100%;" id="ncvAmt" name="ncvAmt" class="mini-spinner input-text-strong" labelField="true"  label="累计已转投金额：" changeOnMousewheel="false" maxValue="9999999999999999.99" format="n2" labelStyle="text-align:left;width:130px;" maxLength:"18"/>
				</div>
				<div class="leftarea">	
					<input style="width:100%;" id="shAmt" name="shAmt" class="mini-spinner input-text-strong" labelField="true"  label="累计已分红金额：" changeOnMousewheel="false" maxValue="9999999999999999.99" format="n2" labelStyle="text-align:left;width:130px;" maxLength:"18"/>
				</div>	
			</div>
		</div>	
	</div>
	<div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
		<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="close_btn"   onclick="close">关闭界面</a></div>
	</div>
</div>		
<script type="text/javascript">
	mini.parse();
	top['CFundTposEdit'] = window;
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var row=params.selectData;
	var url=window.location.search;
	var action=CommonUtil.getParam(url,"action");
	var form =new mini.Form("#field_form");

	function close(){
		top["win"].closeMenuTab();
	}
	$(document).ready(function(){
		if($.inArray(action,["approve","detail"])>-1){
			form.setData(row);
			form.setEnabled(false);
		}
		if($.inArray(action,["edit","approve","detail"])>-1){
			form.setData(row);
			
			mini.get("fundCode").setValue(row.fundCode);
			mini.get("fundCode").setText(row.fundName);
			
		}
	})
	
	
</script>
<script src="<%=basePath%>/miniScript/miniMustFill.js"></script>	
</body>
</html>