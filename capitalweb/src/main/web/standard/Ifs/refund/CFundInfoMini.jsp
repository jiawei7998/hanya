<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
    <title>基金基本信息</title>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
</head>
<body style="width:100%;height:100%;background:white">
    <fieldset class="mini-fieldset title">
	    <legend>基金基本信息查询</legend>	
		<div>
			<div id="search_form" style="width:100%" cols="6">
				<input id="fundName" name="fundName" class="mini-textbox" labelField="true"  label="基金全称：" width="280px" labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请输入基金名称" />
				<input id="fundCode" name="fundCode" class="mini-textbox" labelField="true"  label="基金代码：" width="280px" labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请输入基金代码" />
				<input id="fundType" name="fundType" class="mini-combobox" data="CommonUtil.serverData.dictionary.FundType" width="280px" emptyText="请选择基金类型" labelField="true"  label="基金类型：" labelStyle="text-align:right;"/>
				<span style="float: right; margin-right: 150px"> 
					<a id="search_btn" class="mini-button" style="display: none"   onclick="search()">查询</a>
					<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
				</span>
			</div>
		</div>
    </fieldset>
    <div class="mini-fit" style="width:100%;height:100%;">
        <div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" 
            sortMode="client" allowAlternating="true"  idField="fundCode" onrowdblclick="onRowDblClick" allowResize="true" >
            <div property="columns">
	            <div type="indexcolumn" width="50px" headerAlign="center">序号</div>
				<div field="fundCode" width="100px" align="center"  headerAlign="center" >基金代码</div>
				<div field="fundName" width="150px" align="center"  headerAlign="center" >基金简称</div>
				<div field="fundFullName" width="250px" align="center"  headerAlign="center" >基金全称</div>
				<div field="fundType" width="100px" align="center"  headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'FundType'}">基金类型</div>
				<div field="ccy" width="100px" align="center"  headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'Currency'}">基币币种</div>
				<div field="managCompNm" width="100px" align="center"  headerAlign="center" >基金管理人名称</div>
				<div field="vType" width="100px" align="center"  headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'ValueType'}">起息规则</div>
				<div field="managerMen" width="100px" align="center"  headerAlign="center" >基金经理</div>
				<div field="coverMode" width="100px" align="center"  headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'coverMode'}">份额结转方式</div>
				<div field="coverDay" width="100px" align="center"  headerAlign="center" >份额结转日期</div>
            </div>
        </div>
    </div>
</body>
<script>
    mini.parse();

	$(document).ready(function () {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn){});
	});
    var form = new mini.Form("#search_form");
    var grid = mini.get("datagrid");
    var url=window.location.search;
	var action=CommonUtil.getParam(url,"action");
    
    
    grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		searchs(pageSize,pageIndex);
	});

    //查询按钮
     function search(){
    	 searchs(10,0);
    } 
    function searchs(pageSize, pageIndex) {
        form.validate();
        if (form.isValid() == false) {
            mini.alert("表单填写错误,请确认!", "提示信息");
            return;
        }
        var data = form.getData(true);
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        data['branchId']=branchId;
        var params = mini.encode(data);
        CommonUtil.ajax({
            url : "/CFtInfoController/searchCFtInfoList",
            data : params,
            callback : function(data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }
    //清空
    function clear(){
        var form=new mini.Form("search_form");
        form.clear();
        search(10,0);
    }
    //双击
    function onRowDblClick(e) {
    	onOk();
    }
    //初始化
	function SetData(data) {
		mini.get("fundType").setValue(data.fundType);
		search();
	}
    
    function GetData() {
        var row = grid.getSelected();
        return row;
    }
    function CloseWindow(action) {
        if (window.CloseOwnerWindow)
            return window.CloseOwnerWindow(action);
        else
            window.close();
    }
    function onOk() {
        CloseWindow("ok");
    }
</script>
</html>