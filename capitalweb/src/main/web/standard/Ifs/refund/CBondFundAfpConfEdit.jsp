<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
  	<title>债券基金申购确认维护</title>
	<script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
	<script type="text/javascript" src="../cfetsrmb/rmbVerify.js"></script>
	<script type="text/javascript" src="<%=basePath%>/standard/Common/RiskCenter.js"></script>
  </head>

<body style="width:100%;height:100%;background:white">
<div class="mini-splitter" style="width:100%;height:100%;">
	<div size="90%" showCollapseButton="false">
		<h1 style="text-align:center"><strong>债券基金申购确认单</strong></h1>
		<div id="field_form" class="mini-fit area"  style="background:white">
		
			<input id="sponsor" name="sponsor" class="mini-hidden" />
			<input id="sponInst" name="sponInst" class="mini-hidden" />
			<input id="aDate" name="aDate" class="mini-hidden"/>
			<input id="refNo" name="refNo" class="mini-hidden"/>
		
			<div class="mini-panel" title="审批单号" style="width:100%;"  allowResize="true" collapseOnTitleClick="true">
				<div class="leftarea">
					<input style="width:100%;" id="dealNo" name="dealNo" class="mini-textbox mini-mustFill" required="true"  labelField="true" requiredErrorText="该输入项为必输项"  label="审批单号：" labelStyle="text-align:left;width:130px;"  maxLength:"20"/>
				</div>
			</div>
			
			<div class="mini-panel" title="基金概况" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
				<div class="leftarea">
					<input style="width:100%" id="fundCode" name="fundCode" class="mini-buttonedit mini-mustFill" labelField="true" label="基金代码：" labelStyle="text-align:left;width:130px;" required="true"  allowInput="false" onbuttonclick="onFundName" />
					<input style="width:100%" id="fundName" name="fundName" class="mini-textbox" labelField="true" label="基金简称：" labelStyle="text-align:left;width:130px;" enabled="false" />
					<input style="width:100%" id="totalQty" name="totalQty" class="mini-spinner input-text-strong" labelField="true" label="基金规模（亿元）：" labelStyle="text-align:left;width:130px;" maxValue="9999999999999999.99" format="n2" enabled="false" />
					<input style="width:100%" id="cno" name="cno" class="mini-textbox" labelField="true" label="基金管理人编号：" labelStyle="text-align:left;width:130px;" enabled="false" />
					<input style="width:100%" id="contact" name="contact" class="mini-textbox" labelField="true" label="基金经理：" labelStyle="text-align:left;width:130px;" enabled="false" />		
					<input style="width:100%" id="yield" name="yield" class="mini-spinner input-text-strong"  labelField="true" label="万份收益："  labelStyle="text-align:left;width:130px;" changeOnMousewheel="false" value="0" maxValue="9999999999999999.99" format="n2" maxLength:"20"/>		
				</div>
				<div class="rightarea">
					<input style="width:100%" id="ccy" name="ccy" class="mini-combobox" labelField="true" label="基金币种：" labelStyle="text-align:left;width:130px;" data = "CommonUtil.serverData.dictionary.Currency" value="CNY" enabled="false" />
					<input style="width:100%" id="fundFullName" name="fundFullName" class="mini-textbox" labelField="true" label="基金全称：" labelStyle="text-align:left;width:130px;" enabled="false" />
					<input style="width:100%" id="estDate" name="estDate" class="mini-datepicker" labelField="true" label="基金成立日期：" labelStyle="text-align:left;width:130px;" enabled="false" />
					<input style="width:100%" id="cname" name="cname" class="mini-textbox" labelField="true" label="基金管理人名称："	labelStyle="text-align:left;width:130px;" enabled="false" />
					<input style="width:100%" id="contactPhone" name="contactPhone" class="mini-textbox" labelField="true" label="联系电话：" labelStyle="text-align:left;width:130px;" enabled="false" />		
					<input style="width:100%" id="sevendayRate" name="sevendayRate" class="mini-spinner input-text-strong"  labelField="true" label="七日年化收益率："  labelStyle="text-align:left;width:130px;" changeOnMousewheel="false" value="0" maxValue="9999999999999999.99" format="n2" maxLength:"20"/>			
				</div>
			</div>
			
			<div class="mini-panel" title="交易主体信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
				<div class="leftarea">
					<input style="width:100%" id="tdate" name="tdate" class="mini-datepicker" labelField="true"label="交易日期：" labelStyle="text-align:left;width:130px;" />
<%--					<input style="width:100%" id="atpShareAmt" name="atpShareAmt" class="mini-spinner input-text-strong" labelField="true"  label="申购份额（份）：" labelStyle="text-align:left;width:130px;" enabled="false" maxValue="9999999999999999.99" format="n2" />--%>
					<input style="width:100%" id="shareAmt" name="shareAmt" class="mini-spinner mini-mustFill input-text-strong"  labelField="true"  label="确认份额（份）：" labelStyle="text-align:left;width:130px;" required="true"  changeOnMousewheel="false" onvalidation="zeroValidation" onvaluechanged="countAmt" maxValue="9999999999999999.99" format="n2" maxLength:"20"/>
<%--					<input style="width:100%" id="ftpflag" name="ftpflag" class="mini-combobox" labelField="true" label="FTP参考曲线：" labelStyle="text-align:left;width:130px;"  data="CommonUtil.serverData.dictionary.AT0002"  value="1">--%>
					<input style="width:100%" id="handleAmt" name="handleAmt" class="mini-spinner mini-mustFill input-text-strong"  labelField="true" label="手续费（元）："  labelStyle="text-align:left;width:130px;" required="true"   changeOnMousewheel="false" onvaluechanged="actualAmountReceived"  maxValue="9999999999999999.99" format="n2" maxLength:"20"/>
					<input style="width:100%" id="invType" name="invType" class="mini-combobox" labelField="true" label="会计类型："  labelStyle="text-align:left;width:130px;" data="CommonUtil.serverData.dictionary.tbAccountType" value="0">
					<input style="width:100%" id="trem" name="trem" class="mini-spinner input-text-strong" labelField="true"  label="期限（天）：" labelStyle="text-align:left;width:130px;" maxValue="9999999999999999" />
					<input style="width:100%" id="remark" name="remark" class="mini-textbox" labelField="true"  label="交易事由：" labelStyle="text-align:left;width:130px;"  maxLength:"66"/>
				</div>
				<div class="rightarea">
					<input style="width:100%" id="price" name="price" class="mini-spinner mini-mustFill input-text-strong" labelField="true" label="单位净值（元）：" labelStyle="text-align:left;width:130px;" required="true"  changeOnMousewheel="false" onvalidation="zeroValidation" onvaluechanged="countAmt" maxValue="99999999999999.9999" format="n4" maxLength:"20"/>
					<input style="width:100%" id="atpAmt" name="atpAmt" class="mini-spinner input-text-strong" labelField="true" label="申购金额（元）：" labelStyle="text-align:left;width:130px;" enabled="false" maxValue="9999999999999999.99" format="n2"/>
					<input style="width:100%" id="amt" name="amt" class="mini-spinner mini-mustFill input-text-strong" required="true"   labelField="true" label="确认金额（元）：" labelStyle="text-align:left;width:130px;" changeOnMousewheel="false" onvalidation="zeroValidation" onvaluechanged="countPrice" maxValue="9999999999999999.99" format="n2" maxLength:"20"/>
					<input style="width:100%" id="actualAmountReceived" name="actualAmountReceived"
						   class="mini-spinner mini-mustFill input-text-strong" required="true"   labelField="true"
						   label="实际记账金额（元）：" labelStyle="text-align:left;width:130px;" changeOnMousewheel="false"
						   onvalidation="zeroValidation"  maxValue="9999999999999999.99" format="n2" maxLength="20"/>
					<input style="width:100%" id="handleRate" name="handleRate" class="mini-spinner mini-mustFill input-text-strong"  labelField="true"  label="费率（%）：" labelStyle="text-align:left;width:130px;" required="true"  changeOnMousewheel="false" onvaluechanged="handleRateChange"  maxValue="9999999999999999.99" format="n2" maxLength:"20"/>
<%--					<input style="width:100%" id="ftpprice" name="ftpprice" class="mini-spinner input-text-strong"  labelField="true" label="FTP价格（元）："  labelStyle="text-align:left;width:130px;" changeOnMousewheel="false" value="0" maxValue="99999999999999.9999" format="n4" maxLength:"25"/>--%>
				</div>
			</div>
			
			<div class="mini-panel" title="清算信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
				<div class="leftarea">
					<fieldset>
						<legend>本方账户</legend>
						<input	style="width:100%;" id="selfAcccode" name="selfAcccode" class="mini-textbox" labelField="true"  label="本方帐号：" 	labelStyle="text-align:left;width:120px;"	onvalidation="CommonUtil.onValidation(e,'alphanum',[null])"  maxLength:"1333" />
						<input	style="width:100%;" id="selfAccname" name="selfAccname" class="mini-textbox" labelField="true"  label="本方帐号名称："  labelStyle="text-align:left;width:120px;"	maxLength:"1333" />
						<input	style="width:100%;" id="selfBankcode" name="selfBankcode" class="mini-textbox" labelField="true"  label="本方开户行行号："  labelStyle="text-align:left;width:120px;"	onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" maxLength:"400" />
						<input	style="width:100%;" id="selfBankname" name="selfBankname" class="mini-textbox" labelField="true"  label="本方开户行名称："  labelStyle="text-align:left;width:120px;"	maxLength:"400" />
					</fieldset>
				</div>
				<div class="rightarea">
					<fieldset>
						<legend>对手方账户</legend>
						<input	style="width:100%;" id="partyAcccode" name="partyAcccode" class="mini-textbox" labelField="true"  label="对方帐号："  labelStyle="text-align:left;width:120px;"	onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" maxLength:"1333" />
						<input	style="width:100%;" id="partyAccname" name="partyAccname" class="mini-textbox" labelField="true"  label="对方帐号名称："  labelStyle="text-align:left;width:120px;"	maxLength:"1333" />
						<input	style="width:100%;" id="partyBankcode" name="partyBankcode" class="mini-textbox" labelField="true"  label="对方开户行行号：" 	labelStyle="text-align:left;width:120px;"	onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" maxLength:"400" />
						<input	style="width:100%;" id="partyBankname" name="partyBankname" class="mini-textbox" labelField="true"  label="对方开户行名称："  labelStyle="text-align:left;width:120px;"	maxLength:"400" />
					</fieldset>
				</div>
			</div>
			
			<%@ include file="../../Common/Flow/MiniApproveOpCommon.jsp"%>
		</div>	
	</div>
	<div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
		<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="save_btn"   onclick="save">保存交易</a></div>
		<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="close_btn"   onclick="close">关闭界面</a></div>
	</div>
</div>		
<script type="text/javascript">

	mini.parse();

	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var row=params.selectData;
	var url=window.location.search;
	var action=CommonUtil.getParam(url,"action");
	var prdNo = CommonUtil.getParam(url, "prdNo");
	var prdName = CommonUtil.getParam(url, "prdName");
	var form=new mini.Form("#field_form");
	
	var tradeData={};
	tradeData.selectData=row;
	tradeData.operType=action;
	tradeData.serial_no=row.dealNo;
	tradeData.task_id=row.taskId;
	
	//保存
	function save(){
		//表单验证！！！
		form.validate();
		if (form.isValid() == false) {
			return;
		}
		
		var amt = mini.get("amt").getValue();
		var price = mini.get("price").getValue();
		var shareAmt = mini.get("shareAmt").getValue();
		var atpAmt = mini.get("atpAmt").getValue();
		if(atpAmt < amt){
			mini.alert("确认金额超过申购金额，请重新填写");
			return;
		}

		if(amt <= 0||price <= 0){
			mini.alert("确认金额或单位净值不可为零");
			return;
		}
		if(shareAmt <= 0){
		    mini.alert("确认份额");
		    return;
	    }
		
		var data=form.getData(true);
		//对数据做最后的确认
		mini.confirm("确认以手输数据为准吗？","确认",function (action) {
			if (action != "ok") {
				return;
			}
			data['prdNo']=prdNo;
			data['sponsor']="<%=__sessionUser.getUserId() %>";
			data['sponInst']="<%=__sessionInstitution.getInstId()%>";
			var params=mini.encode(data);
			CommonUtil.ajax({
				url:"/CFtAfpConfController/saveOrUpdateCFtAfpConf",
				data:params,
				callback:function(data){
					if(data.code == 'error.common.0000') {
						mini.alert(data.desc,'提示信息',function(){
							top["win"].closeMenuTab();
						});
					}else {
						mini.alert(data.desc);
					}
				}
			});
		});
	}
	/**
	 * 计算实际到账金额
	 * @param e
	 */
	function actualAmountReceived(obj) {
		var amt=mini.get("amt").getValue();
		mini.get("actualAmountReceived").setValue(amt+obj.value);

		//计算费率
		mini.get("handleRate").setValue((obj.value)/(amt));
	}
	//关闭页面
	function close(){
		top["win"].closeMenuTab();
	}
	//初始化页面
	$(document).ready(function(){
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
			if ($.inArray(action, ["approve", "detail"]) > -1) {
				mini.get("save_btn").setVisible(false);
				form.setEnabled(false);
				var form11 = new mini.Form("#approve_operate_form");
				form11.setEnabled(true);

			}
			if ($.inArray(action, ["edit", "approve", "detail"]) > -1) {
				form.setData(row);
				mini.get("dealNo").setEnabled(false);
				mini.get("fundCode").setEnabled(false);

				mini.get("fundCode").setValue(row.fundCode);
				mini.get("fundCode").setText(row.fundCode);

				//初始化实际到账金额
				var amt = mini.get("amt");
				var handleAmt = mini.get("handleAmt");
				mini.get("actualAmountReceived").setValue(amt.value + handleAmt.value);

			} else {
				mini.get("sponsor").setValue(userId);
				mini.get("sponInst").setValue(instId);
				mini.get("tdate").setValue(sysDate);
			}
		});
	});
	function onFundName(e) {
        var btnEdit = this;
		var data = { fundType: prdNo };
        mini.open({
            url: CommonUtil.baseWebPath() + "/refund/CFundInfoMini.jsp",
            title: "选择基金代码",
            width: 900,
            height: 600,
			onload: function () {
				var iframe = this.getIFrameEl();
				iframe.contentWindow.SetData(data);
			},
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.fundCode);
                        btnEdit.setText(data.fundCode);
                        
                        mini.get("fundName").setValue(data.fundName);
                        mini.get("fundFullName").setValue(data.fundFullName);
                        mini.get("totalQty").setValue(data.totalQty);
                        mini.get("estDate").setValue(data.estDate);
                        mini.get("ccy").setValue(data.ccy);

                        mini.get("cno").setValue(data.managComp);
                        mini.get("contact").setValue(data.managerMen);
                        mini.get("cname").setValue(data.managCompNm);
                        mini.get("contactPhone").setValue(data.managerMenPhone);

                        mini.get("partyAcccode").setValue(data.partyAcccode);
                        mini.get("partyAccname").setValue(data.partyAccname);
                        mini.get("partyBankcode").setValue(data.partyBankcode);
                        mini.get("partyBankname").setValue(data.partyBankname);
                        
                        btnEdit.focus();
                    }
                }

            }
        });
	}
	
	function countAmt(e) {
		var shareAmt = mini.get("shareAmt").getValue();
		var price = mini.get("price").getValue();
		if(shareAmt != "" && price != "") {
			mini.get("amt").setValue(CommonUtil.accMul(shareAmt,price));
		}
		//计算实际金额
		var amt=mini.get("amt");
		var handleAmt=mini.get("handleAmt");
		mini.get("actualAmountReceived").setValue(amt.value+handleAmt.value);

		//计算费率
		mini.get("handleRate").setValue((handleAmt.value)/(amt.value)*100);
	}
	
	function countPrice(e) {
		var price = mini.get("price").getValue();
		var amt = mini.get("amt").getValue();
		if(price != "" && amt != "") {
			mini.get("shareAmt").setValue(CommonUtil.accDiv(amt,price));
		}
	}
	
	function handleAmtChange() {
		var handleAmt = mini.get("handleAmt").getValue();
		var amt = mini.get("amt").getValue();
		if(handleAmt>0){
			mini.get("handleRate").setValue(CommonUtil.accDiv(handleAmt,amt)*100);
		}
	}
	function handleRateChange() {
		var handleRate = mini.get("handleRate").getValue();
		var amt = mini.get("amt").getValue();
		if(handleRate>0){
			mini.get("handleAmt").setValue(CommonUtil.accMul(amt,handleRate)/100);
		}
	}
	
</script>
<script type="text/javascript" src="../../Common/Flow/MiniApproveOpCommon.js"></script>			
</body>
</html>
