<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
  <title>基金账号信息</title>
	<script type="text/javascript" src="<%=basePath%>/miniScript/miniui/res/ajaxfileupload.js"></script>
  	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>
<body style="width:100%;height:100%;background:white">
<div class="mini-splitter" style="width:100%;height:100%;">
	<div size="90%" showCollapseButton="false">
		<h1 style="text-align:center"><strong>基金账号信息</strong></h1>
		<div id="field_form" class="mini-fit area"  style="background:white">
			<div class="mini-panel" title="基金账号概况" style="width:100%;"  allowResize="true" collapseOnTitleClick="true">
				<div class="leftarea">
					<input style="width:100%;" id="managcomp" name="managcomp" class="mini-textbox mini-mustFill" required="true"  labelField="true" label="基金管理人编号：" labelStyle="text-align:left;width:130px;"  maxLength="50" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" />
					<input style="width:100%;" id="managcompfnm" name="managcompfnm" class="mini-textbox  mini-mustFill" required="true"  labelField="true" label="基金管理人全称：" labelStyle="text-align:left;width:130px;"  maxLength="66"/>
					<input style="width:100%;" id="fundtradeaccount" name="fundtradeaccount" class="mini-textbox  mini-mustFill" required="true"  labelField="true" label="交易账号：" labelStyle="text-align:left;width:130px;"  maxLength="50" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])"/>
					<input style="width:100%;" id="fundaccount" name="fundaccount" class="mini-textbox  mini-mustFill" required="true"  labelField="true" label="基金账号：" labelStyle="text-align:left;width:130px;"  maxLength="50" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" />
					<input style="width:100%;" id="fundaccountname" name="fundaccountname" class="mini-textbox mini-mustFill" required="true"  labelField="true" label="户名：" labelStyle="text-align:left;width:130px;"  maxLength="50" />
				</div>
				<div class="leftarea">
					<input style="width:100%;" id="managcompnm" name="managcompnm" class="mini-textbox  mini-mustFill" required="true"  labelField="true" label="基金管理人简称：" labelStyle="text-align:left;width:130px;"  maxLength="66"/>
					<input style="width:100%;" id="rhbankno" name="rhbankno" class="mini-textbox mini-mustFill" required="true"  labelField="true" label="人行支付系统行号：" labelStyle="text-align:left;width:130px;"  maxLength="50" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])"/>
					<input style="width:100%;" id="fundbankno" name="fundbankno" class="mini-textbox mini-mustFill" required="true"  labelField="true" label="银行账号：" labelStyle="text-align:left;width:130px;"  maxLength="100" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])"/>
					<input style="width:100%;" id="fundbankname" name="fundbankname" class="mini-textbox mini-mustFill" required="true"  labelField="true" label="开户银行名称：" labelStyle="text-align:left;width:130px;"  maxLength="50"/>
					<input style="width:66%;"  id="file" name="Fdata" class="mini-htmlfile" labelField="true" label="文件上传：" labelStyle="text-align:left;width:130px;"  maxLength="100"/><span style="color: red">只能上传PDF格式文件</span>
				</div>
			</div>		
		</div>

<%--		<%@ include file="../cfetsfx/uploadFile.jsp"%>--%>
	</div>
	<div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
		<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="save_btn"   onclick="save">保存交易</a></div>
		<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="close_btn"   onclick="close">关闭界面</a></div>
	</div>
</div>		
<script type="text/javascript">
	mini.parse();

	top['CFundAccountInfoEdit'] = window;
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var row=params.selectData;
	var url=window.location.search;
	var action=CommonUtil.getParam(url,"action");
	var form =new mini.Form("#field_form");

	// function save(){
	// 	//表单验证！！！
	// 	form.validate();
	// 	if (form.isValid() == false) {
	// 		return;
	// 	}
	// 	var data=form.getData(true);
	// 	var params=mini.encode(data);
	// 	CommonUtil.ajax({
	// 		url:"/CFtFundaccountInfoController/saveOrUpdateCFtFundaccountInfo",
	// 		data:params,
	// 		callback:function(data){
	// 			if(data.code == 'error.common.0000') {
	// 				mini.alert(data.desc,'提示信息',function(){
	// 					top["win"].closeMenuTab();
	// 				});
	// 			}else {
	// 				mini.alert(data.desc);
	// 			}
	// 		}
	// 	});
	// }

	function save(){
		var inputFile = $("#file > input:file")[0];

		var param = {};
		form.validate();
		if (form.isValid() == false){//表单验证
			mini.alert("请输入有效数据！","系统提示");
			return;
		}
		param = form.getData();
		$.ajaxFileUpload({
			url: '<%=basePath%>/sl/CFtFundaccountInfoController/saveOrUpdateCFtFundaccountInfo', //用于文件上传的服务器端请求地址
			fileElementId: inputFile,
			data:param,  //文件上传域的ID
			dataType: 'text', //返回值类型 一般设置为json
			success: function (data, status) //服务器成功响应处理函数
			{
				mini.alert(data,"消息",function(){
					//关闭自己
					window.CloseOwnerWindow("ok");
				});

			},
			error: function (data, status, e) //服务器响应失败处理函数
			{
				mini.alert("保存失败："+e,"消息");
			},
			complete: function () {
				var jq = $("#file > input:file");
				jq.before(inputFile);
				jq.remove();
			}
		});

	}

	function close(){
		top["win"].closeMenuTab();
	}
	$(document).ready(function(){
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
			if ($.inArray(action, ["approve", "detail"]) > -1) {
				form.setData(row);
				form.setEnabled(false);
				mini.get("save_btn").setVisible(false);
			}
			if ($.inArray(action, ["edit", "approve", "detail"]) > -1) {
				mini.get("managcomp").setEnabled(false);
				form.setData(row);
			}
		});
	})
	
</script>
<script src="<%=basePath%>/miniScript/miniMustFill.js"></script>	
</body>
</html>
