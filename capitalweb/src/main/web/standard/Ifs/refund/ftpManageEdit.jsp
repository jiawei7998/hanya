<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>FTP价格管理  维护</title>
  </head>

<body style="width:100%;height:100%;background:white">
    <div>
        <span id="labell"><a href="javascript:CommonUtil.activeTab();">FTP价格管理</a> >> 修改</span>
        <div id="field" class="fieldset-body">
            <table id="field_form"  style="text-align:left;margin:auto;width:100%;" class="mini-sltable">
                <tr>
                    <td style="width:50%">
                    	<!-- <input id="operator" name="operator" class="mini-hidden" />
            			<input id="status" name="status" class="mini-hidden" /> -->
                        <input id="id" name="id" class="mini-textbox mini-mustFill" enabled="false"  label="唯一Id" labelField="true" required="true"   style="width:100%" labelStyle="text-align:left;width:100px"/>
                    </td>
                    <td style="width:50%">
                        <input id="effectiveDate" name="effectiveDate" class="mini-datepicker mini-mustFill" required="true"   label="生效日期" labelField="true" style="width:100%" labelStyle="text-align:left;width:100px"/>
                    </td>
                </tr>
                <tr>
                    <td style="width:50%">
                        <input id="ftpType" name="ftpType" class="mini-combobox mini-mustFill"  data = "CommonUtil.serverData.dictionary.AT0002" label="曲线类型" required="true"  emptyText="请选择曲线类型" labelField="true"  style="width:100%" labelStyle="text-align:left;width:100px"/>
                    </td>
                    <td style="width:50%">
                        <input id="yesOrNo" name="yesOrNo" class="mini-combobox mini-mustFill" data = "CommonUtil.serverData.dictionary.AT0002" label="是否生效" emptyText="请选择是否生效" value="1" labelField="true" required="true"    style="width:100%" labelStyle="text-align:left;width:100px" />
                    </td>
                    
                </tr>
                <tr>
                    <td style="width:50%">
                        <input id="oneDay" name="oneDay" class="mini-spinner"  label="一天 "  labelField="true" style="width:100%" labelStyle="text-align:left;width:100px" maxValue="9999999999999999.9999" format="n4" vtype="maxLength:19"/>
                    </td>
                    <td style="width:50%">
                        <input id="sevenDay" name="sevenDay" class="mini-spinner"  label="七天"   labelField="true"  style="width:100%" labelStyle="text-align:left;width:100px" maxValue="9999999999999999.9999" format="n4" vtype="maxLength:19"/>
                    </td>
                </tr>
                <tr>
	                <td style="width:50%">
	                    <input id="fourteenDay" name="fourteenDay" class="mini-spinner"  label="十四天 "  labelField="true" style="width:100%" labelStyle="text-align:left;width:100px" maxValue="9999999999999999.9999" format="n4" vtype="maxLength:19"/>
	                </td>
	                <td style="width:50%">
	                    <input id="oneMonth" name="oneMonth" class="mini-spinner"  label="一个月"   labelField="true"  style="width:100%" labelStyle="text-align:left;width:100px" maxValue="9999999999999999.9999" format="n4" vtype="maxLength:19"/>
	                </td>
	            </tr>

                <tr>
                    <td style="width:50%">
                        <input id="threeMonth" name="threeMonth" class="mini-spinner"  label="三个月 "  labelField="true" style="width:100%" labelStyle="text-align:left;width:100px" maxValue="9999999999999999.9999" format="n4" vtype="maxLength:19"/>
                    </td>
                    <td style="width:50%">
                        <input id="sixMonth" name="sixMonth" class="mini-spinner"  label="六个月"   labelField="true"  style="width:100%" labelStyle="text-align:left;width:100px" maxValue="9999999999999999.9999" format="n4" vtype="maxLength:19"/>
                    </td>
                </tr>

                <tr>
                    <td style="width:50%">
                        <input id="nineMonth" name="nineMonth" class="mini-spinner"  label="九个月 "  labelField="true" style="width:100%" labelStyle="text-align:left;width:100px" maxValue="9999999999999999.9999" format="n4" vtype="maxLength:19"/>
                    </td>
                    <td style="width:50%">
                        <input id="oneYear" name="oneYear" class="mini-spinner"  label="一年"   labelField="true"  style="width:100%" labelStyle="text-align:left;width:100px" maxValue="9999999999999999.9999" format="n4" vtype="maxLength:19"/>
                    </td>
                </tr>

                <tr>
                    <td style="width:50%">
                        <input id="twoYear" name="twoYear" class="mini-spinner"  label="两年 "  labelField="true" style="width:100%" labelStyle="text-align:left;width:100px" maxValue="9999999999999999.9999" format="n4" vtype="maxLength:19"/>
                    </td>
                    <td style="width:50%">
                        <input id="threeYear" name="threeYear" class="mini-spinner"  label="三年"   labelField="true"  style="width:100%" labelStyle="text-align:left;width:100px" maxValue="9999999999999999.9999" format="n4" vtype="maxLength:19"/>
                    </td>
                </tr>

                <tr>
                    <td style="width:50%">
                        <input id="fourYear" name="fourYear" class="mini-spinner"  label="四年 "  labelField="true" style="width:100%" labelStyle="text-align:left;width:100px" maxValue="9999999999999999.9999" format="n4" vtype="maxLength:19"/>
                    </td>
                    <td style="width:50%">
                        <input id="fiveYear" name="fiveYear" class="mini-spinner"  label="五年"   labelField="true"  style="width:100%" labelStyle="text-align:left;width:100px" maxValue="9999999999999999.9999" format="n4" vtype="maxLength:19"/>
                    </td>
                </tr>

                <tr>
                    <td style="width:50%">
                        <input id="sixYear" name="sixYear" class="mini-spinner"  label="六年"  labelField="true" style="width:100%" labelStyle="text-align:left;width:100px" maxValue="9999999999999999.9999" format="n4" vtype="maxLength:19"/>
                    </td>
                    <td style="width:50%">
                        <input id="sevenYear" name="sevenYear" class="mini-spinner"  label="七年"   labelField="true"  style="width:100%" labelStyle="text-align:left;width:100px" maxValue="9999999999999999.9999" format="n4" vtype="maxLength:19"/>
                    </td>
                </tr>

                <tr>
                    <td style="width:50%">
                        <input id="eightYear" name="eightYear" class="mini-spinner"  label="八年 "  labelField="true" style="width:100%" labelStyle="text-align:left;width:100px" maxValue="9999999999999999.9999" format="n4" vtype="maxLength:19"/>
                    </td>
                    <td style="width:50%">
                        <input id="nineeYear" name="nineeYear" class="mini-spinner"  label="九年"   labelField="true"  style="width:100%" labelStyle="text-align:left;width:100px" maxValue="9999999999999999.9999" format="n4" vtype="maxLength:19"/>
                    </td>
                </tr>

                <tr>
                    <td style="width:50%">
                        <input id="tenYear" name="tenYear" class="mini-spinner"  label="十年 "  labelField="true" style="width:100%" labelStyle="text-align:left;width:100px" maxValue="9999999999999999.9999" format="n4" vtype="maxLength:19"/>
                    </td>
                    <td style="width:50%">
                        <input id="fifteenYear" name="fifteenYear" class="mini-spinner"  label="十五年"   labelField="true"  style="width:100%" labelStyle="text-align:left;width:100px" maxValue="9999999999999999.9999" format="n4" vtype="maxLength:19"/>
                    </td>
                </tr>

                <tr>
                    <td style="width:50%">
                        <input id="twentyYear" name="twentyYear" class="mini-spinner"  label="二十年 "  labelField="true" style="width:100%" labelStyle="text-align:left;width:100px" maxValue="9999999999999999.9999" format="n4" vtype="maxLength:19"/>
                    </td>
                    <td style="width:50%">
                        <input id="thirtyYear" name="thirtyYear" class="mini-spinner"  label="三十年"   labelField="true"  style="width:100%" labelStyle="text-align:left;width:100px" maxValue="9999999999999999.9999" format="n4" vtype="maxLength:19"/>
                    </td>
                </tr>
                
                <tr>
	                <td style="width:50%">
		                <input id="operTime" name="operTime" class="mini-datepicker"  label="操作时间" labelField="true"   style="width:100%" labelStyle="text-align:left;width:100px" enabled="false"/>
		            </td>
	            </tr>
                
                <tr>
                    <td colspan="2" style="text-align:center;">
                        <a  id="save_btn" class="mini-button" style="display: none"    onclick="save">保存</a>
                        <a id="cancel_btn" class="mini-button" style="display: none"    onclick="cancel">取消</a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <script type="text/javascript">
        mini.parse();

        var currTab = top["win"].tabs.getActiveTab();
        var params = currTab.params;
        var row=params.selectData;
        var url=window.location.search;
        var action=CommonUtil.getParam(url,"action");
        var form=new mini.Form("#field_form");

        $(document).ready(function(){
            //控制按钮显示
            $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
                initData();
            });
        });
        //初始化数据
        function initData(){
            if($.inArray(action,["edit","detail"])>-1){
                form.setData(row);
                if(action == "detail"){
                    $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>FTP价格管理</a> >> 详情");
                    mini.get("save_btn").hide();
                    form.setEnabled(false);
                }
            }else{
                $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>FTP价格管理</a> >> 新增");
                mini.get("effectiveDate").setValue(sysDate);
            }
        }
        //保存
         function save(){
            //表单验证！！！
            form.validate();
            if (form.isValid() == false) {
                return;
            }
            var saveUrl=null;
            if(action == "add"){
                saveUrl = "/FtpManageController/saveOrUpdateFtpManage";
                var data=form.getData(true);
                var params=mini.encode(data);
                CommonUtil.ajax({
                    url:saveUrl,
                    data:params,
                    callback:function(data){
                    	if (data.code == 'error.common.0000') {
    						mini.alert("保存成功",'提示信息',function(){
    							top["win"].closeMenuTab();
    						});
    					} else {
    						mini.alert("保存失败");
    			    	}
                    }
    		    });
            }else if(action == "edit"){
            	saveUrl = "/FtpManageController/saveOrUpdateFtpManage";
                var data=form.getData(true);
                var params=mini.encode(data);
                CommonUtil.ajax({
                    url:saveUrl,
                    data:params,
                    callback:function(data){
                    	if (data.code == 'error.common.0000') {
    						mini.alert(data.desc,'提示信息',function(){
    							top["win"].closeMenuTab();
    						});
    					} else {
    						mini.alert("修改失败");
    			    	}
                    }
    		    });
            }

           
        }
        function cancel(){
            top["win"].closeMenuTab();
        }
    </script>
    <script src="<%=basePath%>/miniScript/miniMustFill.js"></script>
</body>
</html>