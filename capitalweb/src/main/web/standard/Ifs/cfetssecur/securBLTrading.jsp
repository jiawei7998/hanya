<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
<title>借贷便利</title>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
	<legend>借贷便利成交单查询</legend>	
	<div>
		<div id="search_form" style="width:100%" cols="6">
			<input id="contractId" name="contractId" class="mini-textbox" labelField="true"  label="成交单编号：" width="280px" labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请输入成交编号" />
			<input id="forDate" name="forDate" class="mini-datepicker" labelField="true"  label="成交日期："  width="280px"  labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请输入成交日期" />
			<input id="approveStatus" name="approveStatus" class="mini-combobox" data="CommonUtil.serverData.dictionary.ApproveSta" width="280px" emptyText="请选择审批状态" labelField="true"  label="审批状态：" labelStyle="text-align:right;"/>
			<span style="float: right; margin-right: 150px"> 
				<a id="search_btn" class="mini-button" style="display: none"   onclick="searchs()">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
			</span>
		</div>
	</div>
</fieldset> 
<%@ include file="../batchReback.jsp"%> 
<div class="mini-fit" >
	<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:50%;" idField="id"  allowAlternating="true" allowResize="true" sortMode="client" allowAlternating="true" multiSelect="true">
		<div property="columns">
		<div type="checkcolumn"></div>
			<div type="indexcolumn" width="50px" headerAlign="center">序号</div>
			<div field="prdNo" width="100" allowSort="false" headerAlign="center" align="center" visible="false"></div>
			<!-- <div field="bondName" width="120px" align="left"  headerAlign="center" >债券名称</div> -->
			<div field="tradeAmount" width="120px" align="right"  headerAlign="center" allowSort="true" numberFormat="n4">交易金额</div>                                
			<div field="repoRate" width="120px" align="right"  headerAlign="center" allowSort="true" numberFormat="n8">回购利率（%）</div>
			<div field="contractId" width="180px" align="center"  headerAlign="center" >成交单编号</div>    
			<div field="positiveInst" width="180px" align="center"  headerAlign="center"  >正回购方机构</div>                            
			<div field="reverseInst" width="180px" align="center"  headerAlign="center" >逆回购方机构</div>                            
			<div field="firstSettlementDate" width="120px" align="center"  headerAlign="center" allowSort="true">首次结算日</div>
			<div field="secondSettlementDate" width="120px" align="center"  headerAlign="center" allowSort="true">到期结算日</div>
			<div field="firstSettlementMethod" width="120px" align="center"  headerAlign="center" allowSort="true">首次结算方式</div>
			<div field="secondSettlementMethod" width="120px" align="center"  headerAlign="center" allowSort="true">到期结算方式</div>
			<div field="approveStatus" width="120" align="center" headerAlign="center"  renderer="CommonUtil.dictRenderer" data-options="{dict:'ApproveStatus'}">审批状态</div>
			<div field="dealTransType" width="120" align="center" headerAlign="center"  renderer="CommonUtil.dictRenderer" data-options="{dict:'dealTransType'}">交易状态</div>
			<div field="forDate" width="90px" align="center"  headerAlign="center" allowSort="true">成交日期</div>
			<div field="sponsor" width="120" allowSort="false" headerAlign="center" align="center">审批发起人</div>
			<div field="sponInst" width="120" allowSort="false" headerAlign="center" align="center">审批发起机构</div>
			<div field="aDate" width="180" allowSort="false" headerAlign="center" align="center">审批发起时间</div>
		</div>
	</div>
	<fieldset style="width: 100%;border:solid 1px #aaa;margin-top:8px;position:relative;">
		<legend>借贷便利详情</legend>
			<div id="MiniSettleForeigDetail" style="padding:5px;" class="mini-fit area" tyle="width: 100%; height: 50%;" allowAlternating="true" allowResize="true" border="true" sortMode="client">
			<input class="mini-hidden" name="id"/>
				<div class="mini-panel" title="成交双方信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
					<div class="leftarea">
						<fieldset>
						<legend>正回购方信息</legend>
						<input id="myDir" name="myDir" class="mini-combobox"  labelField="true"  onvaluechanged="dirVerify" label="本方方向："  style="width:100%;"  required="true"   labelStyle="text-align:left;width:130px;" data = "CommonUtil.serverData.dictionary.trading" />
						<input id="positiveInst"  name="positiveInst" class="mini-textbox"  labelField="true"  label="机构："  style="width:100%;" enabled="false" required="true"   labelStyle="text-align:left;width:130px;"/>
						<input id="positiveTrader" name="positiveTrader" class="mini-textbox" labelField="true"  label="交易员："  required="true"   vtype="maxLength:5" enabled="false" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						<input id="positiveTel" name="positiveTel" class="mini-textbox" labelField="true"  label="电话：" onvalidation="CommonUtil.onValidation(e,'phone',[null])"  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						<input id="positiveFax" name="positiveFax" class="mini-textbox" labelField="true"  label="传真："  onvalidation="CommonUtil.onValidation(e,'phone',[null])"  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						<input id="positiveCorp" name="positiveCorp" class="mini-textbox" labelField="true"  label="法人代表："  vtype="maxLength:10" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						<input id="positiveAddr" name="positiveAddr" class="mini-textbox" labelField="true"  label="地址："  vtype="maxLength:50"  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						</fieldset>
					</div>			
					<div class="rightarea">
					<fieldset>
						<legend>逆回购方信息</legend>
						<input id="oppoDir" name="oppoDir" class="mini-combobox"  enabled="false"  labelField="true" label="本方方向："  required="true"   style="width:100%;"  labelStyle="text-align:left;width:130px;" data = "CommonUtil.serverData.dictionary.trading" />
						<input id="reverseInst" name="reverseInst" class="mini-textbox" onbuttonclick="onButtonEdit" labelField="true"  required="true"   label="机构：" vtype="maxLength:50"  style="width:100%;"  labelStyle="text-align:left;width:130px;" />
						<input id="reverseTrader" name="reverseTrader" class="mini-textbox" labelField="true"  label="交易员："  required="true"  vtype="maxLength:10" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						<input id="reverseTel" name="reverseTel" class="mini-textbox" labelField="true"  label="电话："  onvalidation="CommonUtil.onValidation(e,'phone',[null])"  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						<input id="reverseFax" name="reverseFax" class="mini-textbox" labelField="true"  label="传真："  onvalidation="CommonUtil.onValidation(e,'phone',[null])"  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						<input id="reverseCorp" name="reverseCorp" class="mini-textbox" labelField="true"  label="法人代表："  vtype="maxLength:10" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
						<input id="reverseAddr" name="reverseAddr" class="mini-textbox" labelField="true"  label="地址："  vtype="maxLength:50"  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
					</fieldset>
					</div>
				<%@ include file="../../Common/opicsDetail.jsp"%>
					<div class="mini-panel" title="交易主体信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
					 <div class="centerarea" style="height:150px;">
						<div class="mini-fit" >
							<div id="datagrid1" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id"  allowAlternating="true"
								fitColumns="false" allowResize="true" sortMode="client" allowAlternating="true" showpager="false">
								<div property="columns">
									<div type="indexcolumn" width="50px" headerAlign="center">序号</div>
						            <div field="bondCode" width="200" align="center" headerAlign="center"  >债券代码</div>
						            <div field="bondName" width="230px" align="center"  headerAlign="center" >债券名称</div> 
						            <div field="totalFaceValue" width="200" align="right" headerAlign="center"  allowSort="true" numberFormat="n4">券面总额(万元)</div>
						            <div field="underlyingStipType" width="120" align="right" headerAlign="center"  allowSort="true" numberFormat="n2">折算比例(%)</div>
									<div field="invtype" width="120" align="right" headerAlign="center"  allowSort="true"  renderer="CommonUtil.dictRenderer" data-options="{dict:'invtype'}">债券类型</div>
									<div field="portb" width="120" align="right" headerAlign="center"  allowSort="true" >投资组合</div>
									<div field="costb" width="120" align="right" headerAlign="center"  allowSort="true">成本中心</div>
								</div>
							</div>
						</div> 
					</div>
					<div class="leftarea">
						<input style="width:100%" id="underlyingQty" name="underlyingQty" class="mini-spinner"   changeOnMousewheel='false' labelField="true"  required="true"   label="券面总额合计(万元)："  maxValue="9999999999999999.9999" format="n4" required="true"   labelStyle="text-align:left;width:130px;"   />
						<input style="width:100%" id="tradingProduct" name="tradingProduct" class="mini-textbox" labelField="true"  label="交易品种："  required="true"  vtype="maxLength:10"  labelStyle="text-align:left;width:130px;"   />
						<input style="width:100%" id="firstSettlementMethod" name="firstSettlementMethod" class="mini-textbox" labelField="true"  required="true"   label="首次结算方式：" vtype="maxLength:50"   labelStyle="text-align:left;width:130px;"   />
						<input style="width:100%" id="firstSettlementDate" name="firstSettlementDate" class="mini-datepicker" labelField="true"  required="true"   label="首次结算日：" onvaluechanged="linkageCalculatDay" ondrawdate="onDrawDateFirst" labelStyle="text-align:left;width:130px;"   />
						<input style="width:100%" id="tenor" name="tenor" class="mini-spinner"   changeOnMousewheel='false' labelField="true"  required="true"   label="回购期限(天)：" maxValue="99999"  labelStyle="text-align:left;width:130px;"   />
						<input style="width:100%" id="accuredInterest" name="accuredInterest" class="mini-spinner"   changeOnMousewheel='false' labelField="true"  required="true"   label="应计利息(元)：" maxValue="9999999999999999.9999" format="n4"   labelStyle="text-align:left;width:130px;"   />
					</div>			
					<div class="rightarea">
						<input style="width:100%" id="tradeAmount" name="tradeAmount" class="mini-spinner"   changeOnMousewheel='false' labelField="true"  required="true"   label="交易金额(元)：" maxValue="9999999999999999.9999" format="n4"   labelStyle="text-align:left;width:130px;"   />
						<input style="width:100%" id="repoRate" name="repoRate" class="mini-spinner"   changeOnMousewheel='false' labelField="true"  required="true"   label="回购利率(%)：" minValue="0" maxValue="9999999999999999.99999999" format="n8" onvaluechanged="linkageCalculatRate" changeOnMousewheel="false" required="true"  labelStyle="text-align:left;width:130px;" />
						<input style="width:100%" id="secondSettlementMethod" name="secondSettlementMethod" class="mini-textbox" labelField="true"  required="true"   label="到期结算方式：" vtype="maxLength:50"  labelStyle="text-align:left;width:130px;"   />
						<input style="width:100%" id="secondSettlementDate" name="secondSettlementDate" class="mini-datepicker" labelField="true"  required="true"   onvaluechanged="linkageCalculatDay" label="到期结算日：" ondrawdate="onDrawDateSecond" labelStyle="text-align:left;width:130px;"   />
						<input style="width:100%" id="occupancyDays" name="occupancyDays" class="mini-spinner"   changeOnMousewheel='false' labelField="true"  required="true"   label="实际占款天数(天)：" maxValue="99999"  onvaluechanged="linkageCalculat" labelStyle="text-align:left;width:130px;"   />
						<input style="width:100%" id="secondSettlementAmount" name="secondSettlementAmount" class="mini-spinner"   changeOnMousewheel='false' required="true"   labelField="true" maxValue="9999999999999999.9999" format="n4"   label="到期结算金额(元)："  labelStyle="text-align:left;width:130px;"   />
					</div>
			</div>
			<div class="mini-panel" title="清算信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
		<div class="leftarea">
			<fieldset>
				<legend>正回购方账户</legend>
				<input id="positiveAccname" name="positiveAccname" class="mini-textbox" labelField="true"  label="资金账户户名："  vtype="maxLength:50" style="width:100%;"  labelStyle="text-align:left;width:130px;" />
				<input id="positiveOpbank" name="positiveOpbank" class="mini-textbox" labelField="true"  label="资金开户行："  vtype="maxLength:50" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
				<input id="positiveAccnum" name="positiveAccnum" class="mini-textbox" labelField="true"  label="资金账号："  vtype="maxLength:30" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
				<input id="positivePsnum" name="positivePsnum" class="mini-textbox" labelField="true"  label="支付系统行号："  vtype="maxLength:30" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
				<input id="positiveCaname" name="positiveCaname" class="mini-textbox" labelField="true"  label="托管账户户名："  vtype="maxLength:50" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
				<input id="positiveCustname" name="positiveCustname" class="mini-textbox" labelField="true"  label="托管机构："  vtype="maxLength:50" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
				<input id="positiveCanum" name="positiveCanum" class="mini-textbox" labelField="true"  label="托管帐号："  vtype="maxLength:30" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
			</fieldset>
		</div>
		<div class="rightarea">
			<fieldset>
				<legend>逆回购方账户</legend>
				<input id="reverseAccname" name="reverseAccname" class="mini-textbox" labelField="true"  label="资金账户户名："  vtype="maxLength:50" style="width:100%;"  labelStyle="text-align:left;width:130px;" />
				<input id="reverseOpbank" name="reverseOpbank" class="mini-textbox" labelField="true"  label="资金开户行："  vtype="maxLength:50" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
				<input id="reverseAccnum" name="reverseAccnum" class="mini-textbox" labelField="true"  label="资金账号："  vtype="maxLength:30" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
				<input id="reversePsnum" name="reversePsnum" class="mini-textbox" labelField="true"  label="支付系统行号："  vtype="maxLength:30" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
				<input id="reverseCaname" name="reverseCaname" class="mini-textbox" labelField="true"  label="托管账户户名："  vtype="maxLength:50" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
				<input id="reverseCustname" name="reverseCustname" class="mini-textbox" labelField="true"  label="托管机构："  vtype="maxLength:50" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
				<input id="reverseCanum" name="reverseCanum" class="mini-textbox" labelField="true"  label="托管帐号：" vtype="maxLength:30" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
			</fieldset>
		</div>
		</div>
			</div>
			</div>
	</fieldset>
</div>   

<script>
	mini.parse();
	var url = window.location.search;
	var prdNo = CommonUtil.getParam(url, "prdNo");
	var prdName = CommonUtil.getParam(url, "prdName");
	var form = new mini.Form("#search_form");
	var grid=mini.get("datagrid");
	var grid1=mini.get("datagrid1");
	var row="";
	/**************************************点击下面显示详情开始******************************/
	var from = new mini.Form("MiniSettleForeigDetail");
	from.setEnabled(false);
	var grid = mini.get("datagrid");
	//grid.load();
	 //绑定表单
    var db = new mini.DataBinding();
    db.bindForm("MiniSettleForeigDetail", grid);
    /**************************************点击下面显示详情结束******************************/
	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});
	//初始化债券信息
	   function initGrid(ticketId){
			var url="/IfsCfetsrmbCrController/searchBondCrDetails";
			var data={};
			data['ticketId']=ticketId;
			var params=mini.encode(data);
			CommonUtil.ajax({
				url:url,
				data:params,
				callback : function(data) {
					grid1.setData(data.obj.rows);
				}
			});
		}
	//查看详情
	function onRowDblClick(e) {
		var url = CommonUtil.baseWebPath() +"/cfetssecur/securBLTradingEdit.jsp?action=detail";
		var tab = {id: "CrDetail",name:"CrDetail",url:url,title:"借贷便利成交单详情",
					parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
		var paramData = {selectData:grid.getSelected()};
		CommonUtil.openNewMenuTab(tab,paramData);
	}
	//清空
	function clear(){
		var form=new mini.Form("search_form");
		form.clear();
		searchs();
	}
	//添加
	function add(){
		var url = CommonUtil.baseWebPath() +"/cfetssecur/securBLTradingEdit.jsp?action=add?t="+new Date();
		var tab = {id: "CrAdd",name:"CrAdd",url:url,title:"借贷便利成交单补录",
					parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
		var paramData = {selectData:""};
		CommonUtil.openNewMenuTab(tab,paramData);
	}
	//修改
	function edit(){
		var row=grid.getSelected();
		if(row){
		var url = CommonUtil.baseWebPath() +"/cfetssecur/securBLTradingEdit.jsp?action=edit";
		var tab = {id: "CrEdit",name:"CrEdit",url:url,title:"借贷便利成交单修改",
					parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
		var paramData = {selectData:row};
		CommonUtil.openNewMenuTab(tab,paramData);
		}else{
			mini.alert("请选择一条记录","提示");
		}
	}
	//删除
	function del(){
		var rows=grid.getSelecteds();
		if(rows.length==0){
			mini.alert("请选中一行","提示");
			return;
		}
		mini.confirm("您确认要删除选中记录?","系统警告",function(value){   
			if (value=='ok'){   
				var data=rows[0];
				params=mini.encode(data);
				CommonUtil.ajax( {
					url:"/IfsCfetsrmbCrController/deleteCfetsrmbCr",
					data:params,
					callback : function(data) {
						mini.alert("删除成功.","系统提示");
						search(10,0);
					}
				});
			}
		});
	}
	
	
	grid.on("select",function(e){
		var row=e.record;
		initGrid(row.ticketId);
		mini.get("approve_mine_commit_btn").setEnabled(false);
		mini.get("approve_commit_btn").setEnabled(false);
		mini.get("edit_btn").setEnabled(false);
		mini.get("delete_btn").setEnabled(false);
		if(row.approveStatus == "3"){//新建
		  mini.get("approve_mine_commit_btn").setEnabled(true);
		  mini.get("approve_commit_btn").setEnabled(false);
		  mini.get("edit_btn").setEnabled(true);
		  mini.get("delete_btn").setEnabled(true);
		  mini.get("approve_log").setEnabled(false);
		}
		if( row.approveStatus == "4" || row.approveStatus == "5"){//待审批：4   审批中:5
		  mini.get("approve_mine_commit_btn").setEnabled(false);
		  mini.get("approve_commit_btn").setEnabled(true);
		  mini.get("edit_btn").setEnabled(false);
		  mini.get("delete_btn").setEnabled(false);
		}
		if(row.approveStatus != null && row.approveStatus != "3") {
		  mini.get("approve_log").setEnabled(true);
		  mini.get("approve_mine_commit_btn").setEnabled(false);
		  mini.get("approve_commit_btn").setEnabled(true);
		  mini.get("edit_btn").setEnabled(false);
		}
		if(row.dealTransType=="N"){
			mini.get("approve_mine_commit_btn").setEnabled(false);
			mini.get("approve_commit_btn").setEnabled(false);
			mini.get("edit_btn").setEnabled(false);
			mini.get("delete_btn").setEnabled(false);
			mini.get("approve_log").setEnabled(false);
			mini.get("print_bk_btn").setEnabled(false);
			mini.get("reback_btn").setEnabled(false);
		}
		if(row.dealTransType!="N"){
			mini.get("reback_btn").setEnabled(true);
		}
		if(row.approveStatus == "3"){
			mini.get("reback_btn").setEnabled(false);
		}	
		});

	//查询 按钮
	function searchs(){
		search(grid.pageSize,0);
	}

	function search(pageSize,pageIndex){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统也提示");
			return;
		}

		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		var url=null;

		var approveType = mini.get("approveType").getValue();
		if(approveType == "mine"){
			url = "/IfsCfetsrmbCrController/searchRmbCrSwapMine";
		}else if(approveType == "approve"){
			url = "/IfsCfetsrmbCrController/searchPageRmbCrUnfinished";
		}else{
			url = "/IfsCfetsrmbCrController/searchPageRmbCrFinished";
		}
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:url,
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	/**************************审批相关****************************/
	//审批日志查看
	function appLog(selections){
		var flow_type = Approve.FlowType.VerifyApproveFlow;
		if(selections.length <= 0){
			mini.alert("请选择要操作的数据","系统提示");
			return;
		}
		if(selections.length > 1){
			mini.alert("系统不支持多笔操作","系统提示");
			return;
		}
		if(selections[0].tradeSource == "3"){
			mini.alert("初始化导入的业务没有审批日志","系统提示");
			return false;
		}
		Approve.approveLog(flow_type,selections[0].ticketId);
	};
	//提交正式审批、待审批
	function verify(selections){
		if(selections.length == 0){
			mini.alert("请选中一条记录！","消息提示");
			return false;
		}else if(selections.length > 1){
			mini.alert("该功能不支持多笔提交，如需多笔提交请点击[批量提交]按钮","系统提示");
			return false;
		}
		if(selections[0]["approveStatus"] != "3" ){
			var url = CommonUtil.baseWebPath() +"/cfetsrmb/securBLTradingEdit.jsp?action=approve&ticketId="+row.ticketId;
			var tab = {"id": "pledgeRepoApprove",name:"pledgeRepoApprove",url:url,title:"借贷便利审批",
						parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
			var paramData = {selectData:selections[0]};
			CommonUtil.openNewMenuTab(tab,paramData);
			// top["win"].showTab(tab);
		}else{
			Approve.approveCommit(Approve.FlowType.VerifyApproveFlow,selections[0]["ticketId"],Approve.OrderStatus.New,"IfsRmbCrService",prdNo,function(){
				search(grid.pageSize,grid.pageIndex);
			});
		}
	};
	//审批
	function approve(){
		mini.get("approve_commit_btn").setEnabled(false);
		var messageid = mini.loading("系统正在处理...", "请稍后");
		try {
			verify(grid.getSelecteds());
		} catch (error) {
			
		}
		mini.hideMessageBox(messageid);	
		mini.get("approve_commit_btn").setEnabled(true);
	}
	//提交审批
	function commit(){
		mini.get("approve_mine_commit_btn").setEnabled(false);
		var messageid = mini.loading("系统正在处理...", "请稍后");
		try {
			verify(grid.getSelecteds());
		} catch (error) {
			
		}
		mini.hideMessageBox(messageid);	
		mini.get("approve_mine_commit_btn").setEnabled(true);
	}
	//审批日志
	function searchlog(){
		appLog(grid.getSelecteds());
	}
	
	function getData(action) {
			var row = null;
			if (action != "add") {
				row = grid.getSelected();
			}
			return row;
		}
	//打印
    function print(){
		var selections = grid.getSelecteds();
		if(selections == null || selections.length == 0){
			mini.alert('请选择一条要打印的数据！','系统提示');
			return false;
		}else if(selections.length>1){
			mini.alert('暂不支持多条数据同时打印，请选择一条要打印的数据！','系统提示');
			return false;
		}
		var canPrint = selections[0].ticketId;
		
		if(!CommonUtil.isNull(canPrint)){
			var actionStr = CommonUtil.pPath + "/sl/IfsPrintUtilController/exportload/print/"+PrintNo.pledgeRepo+"/" + canPrint;
			$('<form target="_blank" action=' + actionStr + ' method="post"></form>').appendTo('body').submit().remove();
		};
	}
	
</script>
</body>
</html>
