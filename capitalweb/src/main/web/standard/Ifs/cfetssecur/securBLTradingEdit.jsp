<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
    <script type="text/javascript" src="./rmbVerify.js"></script>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>借贷便利维护</title>
    <script type="text/javascript" >
	/**prdCode用于页面投资组合的选择   该全局变量必须为prdCode*/
	var prdCode="REPO-LF";
	</script>
  </head>

<body style="width:100%;height:100%;background:white">
<div class="mini-splitter" style="width:100%;height:100%;">
	<div size="90%" showCollapseButton="false">
		<h1 style="text-align:center"><strong>借贷便利</strong></h1>
		<div  id="field_form" class="mini-fit area"  style="background:white" >
		<input id="sponsor" name="sponsor" class="mini-hidden" />
		<input id="sponInst" name="sponInst" class="mini-hidden" />
		<input id="aDate" name="aDate" class="mini-hidden"/>
		<input id="ticketId" name="ticketId" class="mini-hidden" />
		
		<div class="mini-panel" title="成交单编号" style="width:100%;"  allowResize="true" collapseOnTitleClick="true">
			<div class="leftarea">
				<input style="width:100%;" id="forDate" name="forDate" class="mini-datepicker mini-mustFill" labelField="true" required="true"  label="成交日期：" labelStyle="text-align:left;width:130px;" />
			</div>
			<div class="rightarea">
				<input style="width:100%;" id="contractId" name="contractId" class="mini-textbox mini-mustFill" labelField="true" requiredErrorText="该输入项为必输项" required="true"  label="成交单编号：" labelStyle="text-align:left;width:130px;" onvalidation="onEnglishAndNumberValidations" vtype="maxLength:20"/>
			</div>
			
		</div>
		<div class="mini-panel" title="成交双方信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
		<div class="leftarea">
			<fieldset>
				<legend>正回购方信息</legend>
				<input id="myDir" name="myDir" class="mini-combobox mini-mustFill"  labelField="true"  onvaluechanged="dirVerify" label="本方方向："  style="width:100%;"  required="true"   labelStyle="text-align:left;width:130px;" data = "CommonUtil.serverData.dictionary.tradingRepo" />
				<input id="positiveInst"  name="positiveInst" class="mini-textbox mini-mustFill"  labelField="true"  label="机构："  style="width:100%;" enabled="false" required="true"   labelStyle="text-align:left;width:130px;"/>
				<input id="positiveTrader" name="positiveTrader" class="mini-textbox mini-mustFill" labelField="true"  label="交易员："  required="true"   vtype="maxLength:5" enabled="false" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
				<input id="positiveTel" name="positiveTel" class="mini-textbox" labelField="true"  label="电话：" onvalidation="CommonUtil.onValidation(e,'phone',[null])"  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
				<input id="positiveFax" name="positiveFax" class="mini-textbox" labelField="true"  label="传真："  onvalidation="CommonUtil.onValidation(e,'phone',[null])"  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
				<input id="positiveCorp" name="positiveCorp" class="mini-textbox" labelField="true"  label="法人代表："  vtype="maxLength:10" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
				<input id="positiveAddr" name="positiveAddr" class="mini-textbox" labelField="true"  label="地址："  vtype="maxLength:50"  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
			</fieldset>
		</div>
		<div class="rightarea">
			<fieldset>
				<legend>逆回购方信息</legend>
				<input id="oppoDir" name="oppoDir" class="mini-combobox mini-mustFill"  enabled="false"  labelField="true" label="本方方向："  required="true"   style="width:100%;"  labelStyle="text-align:left;width:130px;" data = "CommonUtil.serverData.dictionary.trading" />
				<input id="reverseInst" name="reverseInst" class="mini-buttonedit mini-mustFill" onbuttonclick="onButtonEdit" labelField="true"  required="true"   label="机构：" vtype="maxLength:50"  style="width:100%;"  labelStyle="text-align:left;width:130px;" />
				<input id="reverseTrader" name="reverseTrader" class="mini-textbox mini-mustFill" labelField="true"  label="交易员："  required="true"  vtype="maxLength:10" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
				<input id="reverseTel" name="reverseTel" class="mini-textbox" labelField="true"  label="电话："  onvalidation="CommonUtil.onValidation(e,'phone',[null])"  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
				<input id="reverseFax" name="reverseFax" class="mini-textbox" labelField="true"  label="传真："  onvalidation="CommonUtil.onValidation(e,'phone',[null])"  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
				<input id="reverseCorp" name="reverseCorp" class="mini-textbox" labelField="true"  label="法人代表："  vtype="maxLength:10" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
				<input id="reverseAddr" name="reverseAddr" class="mini-textbox" labelField="true"  label="地址："  vtype="maxLength:50"  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
			</fieldset>
		</div>
		</div>
		<%@ include file="../../Common/opicsLess.jsp"%>
		<div class="mini-panel" title="交易主体信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
			<div id="search_detail_form">
			<div class="leftarea">
				<input style="width:100%" id="bondCode" name="bondCode" class="mini-buttonedit" labelField="true"  label="债券代码："  onbuttonclick="onBondQuery"   vtype="maxLength:20" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])"  labelStyle="text-align:left;width:130px;" />
				<input style="width:100%" id="totalFaceValue" name="totalFaceValue" class="mini-spinner mini-mustFill"   changeOnMousewheel='false'  required="true"  labelField="true"  label="券面总额(万元)：" maxValue="9999999999999999.9999" format="n4" required="true"  labelStyle="text-align:left;width:130px;"   />
				<input id="portb" name="portb" class="mini-buttonedit" onbuttonclick="onPortCostQuery"  style="width:100%;"  labelField="true"  label="投资组合："  allowInput="false" vtype="maxLength:4" labelStyle="text-align:left;width:130px;" >
        		<input id="costb" name="costb" class="mini-textbox" labelField="true"  label="成本中心：" style="width:100%;" enabled="false" vtype="maxLength:15" labelStyle="text-align:left;width:130px;"  >
			</div>
			<div class="rightarea">	
				<input style="width:100%" id="bondName" name="bondName" class="mini-textbox" labelField="true"  label="债券名称："  enabled="false"    labelStyle="text-align:left;width:130px;"   />
				<input id="invtype" name="invtype" class="mini-combobox mini-mustFill"  labelField="true"   label="投资类型："  style="width:100%;"  labelStyle="text-align:left;width:130px;" allowInput="false"  data = "CommonUtil.serverData.dictionary.invtype" />
				<input style="width:100%" id="underlyingStipType" name="underlyingStipType" class="mini-spinner mini-mustFill"   changeOnMousewheel='false' required="true"   labelField="true"  label="折算比例(%)：" minValue="0" onvaluechanged="linkageCalculatAmount" maxValue="100" format="n2" changeOnMousewheel="false" labelStyle="text-align:left;width:130px;"   />
			</div>
			</div>
			<div class="centerarea" style="margin-top:5px;">
		<span style="float:left;">
		</span>
		
			<div class="mini-toolbar" style="padding:0px;border-bottom:0;" id="toolbar"> 
			<table style="width:100%;">
	            <tr>
	                <td style="width:100%;">                
	                    <a  id="add_btn" class="mini-button" style="display: none"   onclick="add()">添加</a>
					    <a  id="edit_btn" class="mini-button" style="display: none"   onclick="edit()">修改</a>
						<a  id="delete_btn" class="mini-button" style="display: none"   onclick="del()">删除</a>
					    <a  id="saves_btn" class="mini-button" style="display: none"    onclick="saves()" enabled="false" >保存</a>
	                </td>
	            </tr>
	        </table>
	        </div>
			 </div> 
			 <div class="centerarea" style="height:150px;">
			<div class="mini-fit" >
				<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id"  allowAlternating="true"
					fitColumns="false" allowResize="true" sortMode="client" allowAlternating="true" showpager="false">
					<div property="columns">
						<div type="indexcolumn" width="50px" headerAlign="center">序号</div>
			            <div field="bondCode" width="200" align="center" headerAlign="center"  >债券代码</div>
			            <div field="bondName" width="230px" align="center"  headerAlign="center" >债券名称</div> 
			            <div field="totalFaceValue" width="200" align="right" headerAlign="center"  allowSort="true" numberFormat="n4">券面总额(万元)</div>
			            <div field="underlyingStipType" width="120" align="right" headerAlign="center"  allowSort="true" numberFormat="n2">折算比例(%)</div>
						<div field="invtype" width="120" align="right" headerAlign="center"  allowSort="true"  renderer="CommonUtil.dictRenderer" data-options="{dict:'invtype'}">债券类型</div>
						<div field="portb" width="120" align="right" headerAlign="center"  allowSort="true" >投资组合</div>
						<div field="costb" width="120" align="right" headerAlign="center"  allowSort="true">成本中心</div>
					
					</div>
				</div>
			</div> 
			</div>
			<div class="leftarea">
				<input style="width:100%" id="underlyingQty" name="underlyingQty" class="mini-spinner mini-mustFill"   changeOnMousewheel='false' labelField="true"  required="true"   label="券面总额合计(万元)："  maxValue="9999999999999999.9999" format="n4" required="true"   labelStyle="text-align:left;width:130px;"   />
				<input style="width:100%" id="tradingProduct" name="tradingProduct" class="mini-textbox mini-mustFill" labelField="true"  label="交易品种："  required="true"  vtype="maxLength:10"  labelStyle="text-align:left;width:130px;"   />
				<input style="width:100%" id="firstSettlementMethod" name="firstSettlementMethod" class="mini-combobox mini-mustFill" labelField="true"  required="true"   label="首次结算方式：" vtype="maxLength:50" data = "CommonUtil.serverData.dictionary.SettlementMethod"   labelStyle="text-align:left;width:130px;"   />
				<input style="width:100%" id="firstSettlementDate" name="firstSettlementDate" class="mini-datepicker mini-mustFill" labelField="true"  required="true"   label="首次结算日：" onvaluechanged="linkageCalculatDay" ondrawdate="onDrawDateFirst" labelStyle="text-align:left;width:130px;"   />
				<input style="width:100%" id="tenor" name="tenor" class="mini-spinner mini-mustFill"   changeOnMousewheel='false' labelField="true"  required="true"   label="回购期限(天)：" maxValue="99999"  labelStyle="text-align:left;width:130px;"   />
				<input style="width:100%" id="accuredInterest" name="accuredInterest" class="mini-spinner mini-mustFill"   changeOnMousewheel='false' labelField="true"  required="true"   label="应计利息(元)：" maxValue="9999999999999999.9999" format="n4"   labelStyle="text-align:left;width:130px;"   />
			</div>
			<div class="rightarea">
				<input style="width:100%" id="tradeAmount" name="tradeAmount" class="mini-spinner mini-mustFill"   changeOnMousewheel='false' labelField="true"  required="true"   label="交易金额(元)：" maxValue="9999999999999999.9999" format="n4"   labelStyle="text-align:left;width:130px;"   />
				<input style="width:100%" id="repoRate" name="repoRate" class="mini-spinner mini-mustFill"   changeOnMousewheel='false' labelField="true"  required="true"   label="回购利率(%)：" minValue="0" maxValue="9999999999999999.99999999" format="n8" onvaluechanged="linkageCalculatRate" changeOnMousewheel="false" required="true"  labelStyle="text-align:left;width:130px;" />
				<input style="width:100%" id="secondSettlementMethod" name="secondSettlementMethod" class="mini-combobox mini-mustFill" labelField="true"  required="true"   label="到期结算方式：" vtype="maxLength:50" data = "CommonUtil.serverData.dictionary.SettlementMethod"  labelStyle="text-align:left;width:130px;"   />
				<input style="width:100%" id="secondSettlementDate" name="secondSettlementDate" class="mini-datepicker mini-mustFill" labelField="true"  required="true"   onvaluechanged="linkageCalculatDay" label="到期结算日：" ondrawdate="onDrawDateSecond" labelStyle="text-align:left;width:130px;"   />
				<input style="width:100%" id="occupancyDays" name="occupancyDays" class="mini-spinner mini-mustFill"   changeOnMousewheel='false' labelField="true"  required="true"   label="实际占款天数(天)：" maxValue="99999"  onvaluechanged="linkageCalculat" labelStyle="text-align:left;width:130px;"   />
				<input style="width:100%" id="secondSettlementAmount" name="secondSettlementAmount" class="mini-spinner mini-mustFill"   changeOnMousewheel='false' required="true"   labelField="true" maxValue="9999999999999999.9999" format="n4"   label="到期结算金额(元)："  labelStyle="text-align:left;width:130px;"   />
			</div>
		</div>
		
		<div class="mini-panel" title="清算信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
		<div class="leftarea">
			<fieldset>
				<legend>正回购方账户</legend>
				<input id="positiveAccname" name="positiveAccname" class="mini-textbox" labelField="true"  label="资金账户户名："  vtype="maxLength:50" style="width:100%;"  labelStyle="text-align:left;width:130px;" />
				<input id="positiveOpbank" name="positiveOpbank" class="mini-textbox" labelField="true"  label="资金开户行："  vtype="maxLength:50" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
				<input id="positiveAccnum" name="positiveAccnum" class="mini-textbox" labelField="true"  label="资金账号："  vtype="maxLength:30" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
				<input id="positivePsnum" name="positivePsnum" class="mini-textbox" labelField="true"  label="支付系统行号："  vtype="maxLength:30" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
				<input id="positiveCaname" name="positiveCaname" class="mini-textbox" labelField="true"  label="托管账户户名："  vtype="maxLength:50" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
				<input id="positiveCustname" name="positiveCustname" class="mini-buttonedit mini-mustFill" onbuttonclick="onSaccQuery" required="true"  labelField="true"  label="托管机构："  vtype="maxLength:50" allowInput="false" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
				<input id="positiveCanum" name="positiveCanum" class="mini-textbox" labelField="true"  label="托管帐号："  vtype="maxLength:30" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
			</fieldset>
		</div>
		<div class="rightarea">
			<fieldset>
				<legend>逆回购方账户</legend>
				<input id="reverseAccname" name="reverseAccname" class="mini-textbox" labelField="true"  label="资金账户户名："  vtype="maxLength:50" style="width:100%;"  labelStyle="text-align:left;width:130px;" />
				<input id="reverseOpbank" name="reverseOpbank" class="mini-textbox" labelField="true"  label="资金开户行："  vtype="maxLength:50" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
				<input id="reverseAccnum" name="reverseAccnum" class="mini-textbox" labelField="true"  label="资金账号："  vtype="maxLength:30" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
				<input id="reversePsnum" name="reversePsnum" class="mini-textbox" labelField="true"  label="支付系统行号："  vtype="maxLength:30" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
				<input id="reverseCaname" name="reverseCaname" class="mini-textbox" labelField="true"  label="托管账户户名："  vtype="maxLength:50" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
				<input id="reverseCustname" name="reverseCustname" class="mini-buttonedit mini-mustFill" onbuttonclick="onSaccQuery" required="true"  labelField="true"  label="托管机构："  vtype="maxLength:50" allowInput="false" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
				<input id="reverseCanum" name="reverseCanum" class="mini-textbox" labelField="true"  label="托管帐号：" vtype="maxLength:30" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
			</fieldset>
		</div>
		</div>
		<%@ include file="../../Common/Flow/MiniApproveOpCommon.jsp"%>
		</div>
	</div>
	<div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
		<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="save_btn"   onclick="save">保存交易</a></div>
		<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="close_btn"   onclick="close">关闭界面</a></div>
	</div>
</div>	
<script type="text/javascript">
mini.parse();
	//获取当前tab
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var row=params.selectData;
	var url=window.location.search;
	var action=CommonUtil.getParam(url,"action");
	var form=new mini.Form("#field_form");
	var grid=mini.get("datagrid");

	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});
	var tradeData={};
	
	tradeData.selectData=row;
	tradeData.operType=action;
	tradeData.serial_no=row.ticketId;
	tradeData.task_id=row.taskId;
	//加载机构树
	function nodeclick(e) {
		var oldvalue=e.sender.value;
		var param = mini.encode({"branchId":branchId}); //序列化成JSON
		var oldData=e.sender.data;
		if(oldData==null||oldData==""){
			CommonUtil.ajax({
				url: "/InstitutionController/searchInstitutionByBranchId",
				data: param,
				callback: function (data) {
					var obj=e.sender;
					obj.setData(data.obj);
					obj.setValue(oldvalue);
				}
			});
		}
	}
	function onBondQuery(e) {
        var btnEdit = this;
       /*  var startdate = mini.get("firstSettlementDate").getValue();
        var enddate =mini.get("secondSettlementDate").getValue(); */
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Ifs/bond/bondAddMini.jsp",
            title: "选择债券列表",
            width: 700,
            height: 600,
            /* onload: function () {
				var iframe = this.getIFrameEl();
				var data ={startdate:startdate,enddate:enddate};
				iframe.contentWindow.SetData(data);
		    }, */
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.bndcd);
                        btnEdit.setText(data.bndcd);
                        mini.get("bondName").setValue(data.bndnm_cn);
                        mini.get("bondName").setText(data.bndnm_cn);
                        btnEdit.focus();
                    }
                }

            }
        });
	}
	function  onPortCostQuery(){
	    var btnEdit = this;
	    mini.open({
	        url : CommonUtil.baseWebPath() +"/../Common/OpicsTradeParamMini.jsp",
	        title : "投资组合选择",
	        width : 900,
	        height : 500,
	        
	        onload: function () {//弹出页面加载完成
				var iframe = this.getIFrameEl(); 
				var data1 = {prdCode:prdCode};
	            
	            //调用弹出页面方法进行初始化
	            iframe.contentWindow.SetData(data1); 
	        },
	        ondestroy : function(action) {	
	            if (action == "ok") 
	            {
	                var iframe = this.getIFrameEl();
	                var data = iframe.contentWindow.GetData();
	                data = mini.clone(data); //必须
	                if (data) {
	                    btnEdit.setValue(data.port);
	                    btnEdit.setText(data.port);
	                    mini.get("costb").setValue(data.cost);//成本中心
	                    btnEdit.focus();
	                }

	            }

	        }
	    });
	}
	//添加债券信息
    function add(){
        var detailForm = new mini.Form("#search_detail_form");
		var detailData=detailForm.getData(true);
		if(CommonUtil.isNull(detailData.bondCode)||CommonUtil.isNull(detailData.portb)
				||CommonUtil.isNull(detailData.costb)||CommonUtil.isNull(detailData.invtype)){
			mini.alert("请将债券信息填写完整","系统提示");
			return;
		}
        addRow(detailData);

    }
    function addRow(detailData){    
        var row = {
        		bondCode:detailData.bondCode,
        		bondName:detailData.bondName,
        		totalFaceValue:detailData.totalFaceValue,
        		portb:detailData.portb,
        		costb:detailData.costb,
        		invtype:detailData.invtype,
        		underlyingStipType:detailData.underlyingStipType
        };
        grid.addRow(row);
        var detailForm = new mini.Form("#search_detail_form");
        detailForm.clear();
        //重新计算券面总额合计(万元)
        calcAmtSum(grid.getData());     
    }
    /**计算 券面总额合计*/
	function calcAmtSum (rows){
		var underlyingQty=0;
		for(var i=0 ; i <rows.length ; i++){
			underlyingQty=underlyingQty + parseFloat(rows[i].totalFaceValue);
		}
        mini.get("underlyingQty").setValue(underlyingQty);	
	}; 
	//删除
	   function del(){
	       var row = grid.getSelected();
	       if(!row){
	           mini.alert("请选中要移除的一行","提示");
	           return;
	       }
	        grid.removeRow(row);
	        //重新计算授信额度总和
	        calcAmtSum(grid.getData());
	   }

	   //修改
	   function edit(){
	        var row = grid.getSelected();
	        if(!row){
	            mini.alert("请选中一行进行修改!","提示");
	            return;
	        }
	        var detailForm = new mini.Form("#search_detail_form");
	        detailForm.setData(row);
	        mini.get("add_btn").setEnabled(false);
	        mini.get("edit_btn").setEnabled(false);
	        mini.get("saves_btn").setEnabled(true);
	   }

	   //修改保存
	   function saves(){
	        del();
	        add();
	        mini.get("add_btn").setEnabled(true);
	        mini.get("edit_btn").setEnabled(true);
	        mini.get("saves_btn").setEnabled(false);
	   }
	   //初始化债券信息
	   function initGrid(){
			var url="/IfsCfetsrmbCrController/searchBondCrDetails";
			var data={};
			data['ticketId']=mini.get("ticketId").getValue();
			var params=mini.encode(data);
			CommonUtil.ajax({
				url:url,
				data:params,
				callback : function(data) {
					grid.setData(data.obj.rows);
				}
			});
		}
	//保存
	function save(){
		//表单验证！！！
		form.validate();
		if (form.isValid() == false) {
			return;
		}
		var data=form.getData(true);
		var rows = grid.getData();
		if(rows.length==0){
			mini.alert("债券信息不能为空","系统提示");
			return;
		}
		if(toDecimal(data['tradeAmount'])!=toDecimal(data['underlyingStipType']*data['underlyingQty']*10000/100)||data['tenor']!=data['occupancyDays']||toDecimal(data['accuredInterest'])!=toDecimal(data['tradeAmount']*data['repoRate']*data['occupancyDays']/100/365)
				||toDecimal(data['secondSettlementAmount'])!=parseFloat(toDecimal((data['tradeAmount'])))+parseFloat(toDecimal((data['accuredInterest'])))!="0.00"){
			mini.confirm("确认以当前数据为准吗？","确认",function (action) {
				if (action != "ok") {
					return;
				}
				data['positiveInst']="<%=__sessionInstitution.getInstId()%>";
				data['sponInst']="<%=__sessionInstitution.getInstId()%>";
				data['dealTransType']="M";
				data['bondDetailsList']=rows;
				var params=mini.encode(data);
				CommonUtil.ajax({
					url:"/IfsCfetsrmbCrController/saveCr",
					data:params,
					callback:function(data){
						mini.alert(data,'提示信息',function(){
							if(data=="保存成功"||data=="修改成功"){
								top["win"].closeMenuTab();
							}
						});
					}
				});
			});
		}else{
			data['positiveInst']="<%=__sessionInstitution.getInstId()%>";
			data['sponInst']="<%=__sessionInstitution.getInstId()%>";
			data['dealTransType']="M";
			data['bondDetailsList']=rows;
			var params=mini.encode(data);
			CommonUtil.ajax({
				url:"/IfsCfetsrmbCrController/saveCr",
				data:params,
				callback:function(data){
					mini.alert(data,'提示信息',function(){
						if(data=="保存成功"||data=="修改成功"){
							top["win"].closeMenuTab();
						}
					});
				}
			});
		}
	}
	
	//文、数字、下划线 的验证
	function onEnglishAndNumberValidations(e) {
		if(e.value == "" || e.value == null){//值为空，就不做校验
			return;
		}
		if (e.isValid) {
			if (isEnglishAndNumbers(e.value) == false) {
				e.errorText = "必须输入英文小写+数字";
				e.isValid = false;
			}
		}
	}
	/* 是否英文+数字 */
	function isEnglishAndNumbers(v) {
		var re = new RegExp("^[0-9a-z\_\-]+$");
		if (re.test(v)) return true;
		return false;
	}	
	
	//英文、数字、下划线 的验证
	function onEnglishAndNumberValidation(e) {
		if(e.value == "" || e.value == null){//值为空，就不做校验
			return;
		}
		if (e.isValid) {
			if (isEnglishAndNumber(e.value) == false) {
				e.errorText = "必须输入英文+数字";
				e.isValid = false;
			}
		}
	}
	/* 是否英文+数字 */
	function isEnglishAndNumber(v) {
		var re = new RegExp("^[0-9a-zA-Z\_\-]+$");
		if (re.test(v)) return true;
		return false;
	}
	function close(){
		top["win"].closeMenuTab();
	}
	$(document).ready(function(){
		if($.inArray(action,["approve","detail"])>-1){
			mini.get("save_btn").setVisible(false);
			form.setEnabled(false);
			var approForm=new mini.Form("#approve_operate_form");
			approForm.setEnabled(true);
			mini.get("forDate").setEnabled(false);
			mini.get("toolbar").setVisible(false);
			mini.get("bondCode").setVisible(false);
			mini.get("bondName").setVisible(false);
			mini.get("totalFaceValue").setVisible(false);
			mini.get("underlyingStipType").setVisible(false);
		}
		if($.inArray(action,["add","edit"])>-1){
			mini.get("toolbar").setVisible(true);
			mini.get("bondCode").setVisible(true);
			mini.get("bondName").setVisible(true);
			mini.get("totalFaceValue").setVisible(true);
			mini.get("underlyingStipType").setVisible(true);
		}
		if($.inArray(action,["edit","approve","detail"])>-1){//修改、审批、详情
			form.setData(row);
			initGrid();
			mini.get("positiveInst").setValue(row.positiveInst);
			mini.get("forDate").setValue(row.forDate);
			mini.get("ticketId").setValue(row.ticketId);
			//投资组合
			mini.get("port").setValue(row.port);
			mini.get("port").setText(row.port);
			//清算路径
			mini.get("ccysmeans").setValue(row.ccysmeans);
			mini.get("ccysmeans").setText(row.ccysmeans);
			mini.get("ctrsmeans").setValue(row.ctrsmeans);
			mini.get("ctrsmeans").setText(row.ctrsmeans);
			//对方机构
			mini.get("reverseInst").setValue(row.cno);
			mini.get("reverseInst").setText(row.reverseInst);
			 //托管机构
			mini.get("positiveCustname").setValue(row.positiveCustname);
			mini.get("positiveCustname").setText(row.positiveCustname);
			mini.get("reverseCustname").setValue(row.reverseCustname);
			mini.get("reverseCustname").setText(row.reverseCustname); 
			
			mini.get("positiveInst").setValue("<%=__sessionInstitution.getInstName()%>");
			mini.get("contractId").setEnabled(false);
		}else{//增加 
			mini.get("sponsor").setValue("<%=__sessionUser.getUserId()%>");
			mini.get("sponInst").setValue("<%=__sessionUser.getInstId() %>");
			mini.get("positiveInst").setValue("<%=__sessionInstitution.getInstName()%>");
			mini.get("positiveTrader").setValue("<%=__sessionUser.getUserId() %>");
			mini.get("forDate").setValue("<%=__bizDate %>");
		}
	});
	function onButtonEdit(e) {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Common/CustMini.jsp",
            title: "选择对手方列表",
            width: 900,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.cno);
                        btnEdit.setText(data.cfn);
                        btnEdit.focus();
                    }
                }

            }
        });
    }
	function onDrawDateFirst(e) {
        var firstDate = e.date;
        var secondDate= mini.get("secondSettlementDate").getValue();
        if(CommonUtil.isNull(secondDate)){
        	return;
        }
        if (secondDate.getTime() < firstDate.getTime()) {
            e.allowSelect = false;
        }     
    }
	function onDrawDateSecond(e) {
        var secondDate = e.date;
        var firstDate = mini.get("firstSettlementDate").getValue();
        if(CommonUtil.isNull(firstDate)){
        	return;
        }
        if (secondDate.getTime() < firstDate.getTime()) {
            e.allowSelect = false;
        }
    }
	function linkageCalculatDay(e){
		var firstDate = mini.get("firstSettlementDate").getValue();
		var secondDate= mini.get("secondSettlementDate").getValue();
		if(CommonUtil.isNull(firstDate)||CommonUtil.isNull(secondDate)){
			return;
		}
		var subDate= Math.abs(parseInt((secondDate.getTime() - firstDate.getTime())/1000/3600/24));
		mini.get("tenor").setValue(subDate);
		mini.get("occupancyDays").setValue(subDate);
		var occupancyDays=mini.get("occupancyDays").getValue();
		var rate=mini.get("repoRate").getValue();
		var tradeAmount=mini.get("tradeAmount").getValue();
		mini.get("accuredInterest").setValue(tradeAmount*rate*occupancyDays/100/365);
		mini.get("secondSettlementAmount").setValue(tradeAmount+tradeAmount*rate*occupancyDays/100/365);
	}
    function linkageCalculatAmount(e){
		var underlyingStip=e.value;
		var qty=mini.get("underlyingQty").getValue();
		mini.get("tradeAmount").setValue(underlyingStip*qty*10000/100);
	}
   function linkageCalculat(e){
		var occupancyDays=e.value;
		var rate=mini.get("repoRate").getValue();
		var tradeAmount=mini.get("tradeAmount").getValue();
		mini.get("accuredInterest").setValue(tradeAmount*rate*occupancyDays/100/365);
		mini.get("secondSettlementAmount").setValue(tradeAmount+tradeAmount*rate*occupancyDays/100/365);
	} 
   function linkageCalculatRate(e){
	    var rate=e.value;
		var occupancyDays=mini.get("occupancyDays").getValue();
		var tradeAmount=mini.get("tradeAmount").getValue();
		mini.get("accuredInterest").setValue(tradeAmount*rate*occupancyDays/100/365);
		mini.get("secondSettlementAmount").setValue(tradeAmount+tradeAmount*rate*occupancyDays/100/365);
   }
   
   /**   清算路径1的选择   */
	function settleMeans1(){
		 var cpInstId = mini.get("reverseInst").getValue();
		if(cpInstId == null || cpInstId == ""){
			mini.alert("请先选择对方机构!");
			return;
		} 
		var ccy="CNY";
		var url="./Ifs/opics/setaMini.jsp";
		var data = { ccy: ccy ,cust:cpInstId};
		
		var btnEdit = this;
       mini.open({
           url: url,
           title: "选择清算路径",
           width: 700,
           height: 600,
           onload: function () {
               var iframe = this.getIFrameEl();
               iframe.contentWindow.SetData(data);
           },
           ondestroy: function (action) {
               if (action == "ok") {
                   var iframe = this.getIFrameEl();
                   var data1 = iframe.contentWindow.GetData();
                   data1 = mini.clone(data1);    //必须
                   if (data1) {
                       mini.get("ccysmeans").setValue($.trim(data1.smeans));
	                    mini.get("ccysmeans").setText($.trim(data1.smeans));
	                    mini.get("ccysacct").setValue($.trim(data1.sacct)); 
                       btnEdit.focus();
                   }
               }
           }
       });
	}
	
	/**   清算路径2的选择   */
	function settleMeans2(){
		 var cpInstId = mini.get("reverseInst").getValue();
		if(cpInstId == null || cpInstId == ""){
			mini.alert("请先选择对方机构!");
			return;
		} 
		var ccy="CNY";
		var url="./Ifs/opics/setaMini.jsp";
		var data = { ccy: ccy ,cust:cpInstId};
		
		var btnEdit = this;
       mini.open({
           url: url,
           title: "选择清算路径",
           width: 700,
           height: 600,
           onload: function () {
               var iframe = this.getIFrameEl();
               iframe.contentWindow.SetData(data);
           },
           ondestroy: function (action) {
               if (action == "ok") {
                   var iframe = this.getIFrameEl();
                   var data1 = iframe.contentWindow.GetData();
                   data1 = mini.clone(data1);    //必须
                   if (data1) {
                       mini.get("ctrsmeans").setValue($.trim(data1.smeans));
	                    mini.get("ctrsmeans").setText($.trim(data1.smeans));
	                    mini.get("ctrsacct").setValue($.trim(data1.sacct)); 
                       btnEdit.focus();
                   }
               }

           }
       });
	}
	//托管机构选择
	function onSaccQuery(e) {
        var btnEdit = this;
        mini.open({
            url: "./Ifs/opics/saccMini.jsp",
            title: "选择托管机构列表",
            width: 700,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.accountno);
                        btnEdit.setText(data.accountno);
                        btnEdit.focus();
                    }
                }

            }
        });
    }
</script>
<script type="text/javascript" src="../../Common/Flow/MiniApproveOpCommon.js"></script>			
</body>
</html>
