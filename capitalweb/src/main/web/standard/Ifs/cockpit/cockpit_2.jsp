<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
    <link rel="stylesheet" href="css/cockpit_style.css">

    <title>额度管理员视角</title>
    <style type="text/css">
        .myrow
        {
            background-color:#fff8f0;font-weight:bold;
        }
    </style>
</head>
<body style="width:100%;height:100%;background:#f2f2f2;">
<div class="main_box">
    <div class="cockpit_1 clearfix">
        <div class="mini-panel cockpit_common" style="width:50%;height:400px;padding-bottom:15px;position:relative" title="客户额度总量">
            <div class="type_select">                    
                    <input labelField="true" label="客户额度种类：" name="Country" showNullItem="true" class="mini-combobox"
                     value="cn" textField="text" valueField="id" />
            </div>
            <div id="container_creditType" style="height:100%;overflow: hidden;"></div>
            <div id="pager1" class="mini-pager"  borderStyle="border:0"sizeList="[5,10,20,100]"
            totalCount="" onpagechanged="onPageChanged"></div>
        </div>
        <div class="mini-panel cockpit_common" style="width:50%;height:400px;"  title="额度类型占比">
                <div id="container_customerCredit" style="height:100%;overflow: hidden;"></div>
        </div>
    </div>



    <div class="mini-panel cockpit_common2" style="width:100%;height:360px;" title="交易详情">
        <div class="mini-fit" >
        <div id="datagrid3" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id" 
            allowResize="true"    multiSelect="true" sortMode="client" allowAlternating="true">
            <div property="columns">
                <div type="indexcolumn">序号</div>
                <div field="ecifNum" width="120" headerAlign="center" allowSort="true">客户号</div>   
                <div field="customer" width="120" headerAlign="center" >客户名称</div>    
                <div field="loanAmt" width="120" headerAlign="center" allowSort="true" align="right" numberFormat="n2">授信额度(元)</div>
                <div field="avlAmt" width="120" headerAlign="center" allowSort="true" align="right" numberFormat="n2">可用额度(元)</div>
                <div field="creditName"   headerAlign="center" width="120" >额度种类</div>
                <div field="opTime"   headerAlign="center" allowSort="true" width="120" >交易时间</div>
                <div field="dueDate"  headerAlign="center" allowSort="true" width="120" >到期时间</div>
                <div field="leftDays"   headerAlign="center" allowSort="true" width="120" >剩余期限(天)</div>
                <div field="ifOverDue"   headerAlign="center" allowSort="true" width="120" >状态</div>            
                
            </div>
        </div>
        </div>
    </div>



    <div class="cockpit_1 clearfix">
        <div class="mini-panel cockpit_common2" style="width:38%;height:360px;" title="交易员剩余额度">
            <div id="container_restCredit" style="height: 100%;overflow: hidden"></div>
        </div>
       
        <div class="mini-panel cockpit_common2" style="width:38%;height:360px;" title="预警信息">
            <div class="mini-fit" >   
                <div id="datagrid1" class="mini-datagrid borderAll" style="width:100%;height:100%;padding-right:0"  idField="id" allowResize="true" pageSize="8" 
                     multiSelect="true" sortMode="client" allowAlternating="true">
                    <div property="columns">
                        <div type="indexcolumn">序号</div>
                        <div field="trader"   headerAlign="center"  width="100" >交易员</div>
                        <div field="product"  width="100" headerAlign="center"  >产品种类</div> 
                        <div field="quotaAmt"  width="100" headerAlign="center" align="right" numberFormat="n2">授信额度(元)</div>            
                        <div field="avlAmt"  width="100" headerAlign="center" align="right" numberFormat="n2">可用额度(元)</div>    
                        <div field="traderStatus" width="100" headerAlign="center">状态</div>                        
                    </div>
                </div> 
            </div>          
        </div>
        <div class="mini-panel cockpit_common2" style="width:24%;height:360px;" title="交易工具">
                <div class="clearfix">
                    <a class="tools_box" href="#">
                        <i class="tools_1"></i>
                        <p>我的工具</p>
                    </a>
                    <a class="tools_box">
                            <i class="tools_1"></i>
                            <p>我的工具</p>
                    </a>
                    <a class="tools_box">
                        <i class="tools_1"></i>
                        <p>我的工具</p>
                    </a>
                    <a class="tools_box">
                        <i class="tools_1"></i>
                        <p>我的工具</p>
                    </a>
                    <a class="tools_box">
                            <i class="tools_1"></i>
                            <p>我的工具</p>
                    </a>
                    <a class="tools_box">
                        <i class="tools_1"></i>
                        <p>我的工具</p>
                    </a>




                </div>
                
        </div>
    </div>

    <div class="mini-panel cockpit_common2" style="width:100%;height:200px;" title="系统公告">
        <div class="sys_list">
            <span class="point"></span>
            <span class="sys_title">系统公告标题</span><a href="">为给客户提供更优质的服务，我公司将于2018年4月27日1:00至6:00进行业务支撑系统升级，调整期间系统切换至应急状态。</a>
        </div>
        <div class="sys_list">
                <span class="point"></span>
                <span class="sys_title">系统公告标题</span><a href="">为给客户提供更优质的服务，我公司将于2018年4月27日1:00至6:00进行业务支撑系统升级，调整期间系统切换至应急状态。</a>
        </div>
        <div class="sys_list">
                <span class="point"></span>
                <span class="sys_title">系统公告标题</span><a href="">为给客户提供更优质的服务，我公司将于2018年4月27日1:00至6:00进行业务支撑系统升级，调整期间系统切换至应急状态。</a>
        </div>
        <div class="sys_list">
                <span class="point"></span>
                <span class="sys_title">系统公告标题</span><a href="">为给客户提供更优质的服务，我公司将于2018年4月27日1:00至6:00进行业务支撑系统升级，调整期间系统切换至应急状态。</a>
        </div>
    </div>

</div>
    
</body>
</html>
       <script type="text/javascript" src="http://echarts.baidu.com/gallery/vendors/echarts/echarts.min.js"></script>
       <script type="text/javascript" src="http://echarts.baidu.com/gallery/vendors/echarts-gl/echarts-gl.min.js"></script>
       <script type="text/javascript" src="http://echarts.baidu.com/gallery/vendors/echarts-stat/ecStat.min.js"></script>
       <script type="text/javascript" src="http://echarts.baidu.com/gallery/vendors/echarts/extension/dataTool.min.js"></script>
       <script type="text/javascript" src="http://echarts.baidu.com/gallery/vendors/echarts/map/js/china.js"></script>
       <script type="text/javascript" src="http://echarts.baidu.com/gallery/vendors/echarts/map/js/world.js"></script>
       <script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=ZUONbpqGBsYGXNIYHicvbAbM"></script>
       <script type="text/javascript" src="http://echarts.baidu.com/gallery/vendors/echarts/extension/bmap.min.js"></script>
       <script type="text/javascript" src="http://echarts.baidu.com/gallery/vendors/simplex.js"></script>   
       <script src="theme/vintage.js"></script>
       <script src="theme/macarons.js"></script>
       <script src="theme/macarons1.js"></script>        
       <script src="theme/shine.js"></script>
       <script src="theme/infographic.js"></script>
       <script src="theme/roma.js"></script>     
       
       


       <script type="text/javascript"> 
       mini.parse();
       




/********************************增加方法************************************/
$(document).ready(function() {
    var pageSize = mini.get("pager1").pageSize;
    var pageIndex = mini.get("pager1").pageIndex;
   
    searchCustEduSpitDetail(pageSize,pageIndex);
    initEduType();//初始化额度类型占比
    getEduSpitInit3(10,0);//初始化客户额度列表
    initTraderEdu(10,0);//初始化交易员额度
    initTraderDatagrid(10,0,"");//初始化交易员列表

});
var grid3=mini.get("datagrid3");
var grid1=mini.get("datagrid1");
var pager1=mini.get("pager1");

    grid3.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		setDatagrid(pageSize,pageIndex,"");
    });
    grid1.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		initTraderDatagrid(pageSize,pageIndex,"");
    });
    pager1.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		searchCustEduSpitDetail(pageSize,pageIndex);
	});
	

function onPageChanged(e){
  
    searchCustEduSpitDetail(e.pageSize,e.pageIndex);
    

}
grid3.on("drawcell", function (e) {
    
    var record = e.record;
    //设置行样式
   
    if(parseInt(record.leftDays)<0){
        e.rowCls = "myrow";
    }

});


function searchCustEduSpitDetail (pageSize,pageIndex){
        var data={};
        data.pageSize=pageSize;
        data.pageIndex=pageIndex+1;
        /* data.productType=productType; */
        var params = mini.encode(data);
		CommonUtil.ajax( {
			url : "/edCustController/getEdCustTotalAndAvlPage",
			data:params,
			callback:function(data){
                if(data.obj){
                    mini.get("pager1").setTotalCount(data.obj.total);
                    if(data.obj.rows != null){
                        var custName=new Array();
                        var eduAmt=new Array();
                        var avlAmt=new Array();
                        var len=data.obj.rows.length;
                        var arr=data.obj.rows;
                        for(var i=0;i<len;i++){
                            
                            custName.push(arr[i].CUSTOMER+'('+arr[i].ECIF_NUM+')');
                            //custName.push(arr[i].CUSTOMER);
                            eduAmt.push(parseFloat(arr[i].TOTALAMT)/10000);
                            avlAmt.push(parseFloat(arr[i].AVLAMT)/10000);
                        }
                        eduEchart(custName,eduAmt,avlAmt);

                    }
                }

				
			}
		});
	};

function  eduEchart(custName,eduAmt,avlAmt){
        //客户额度总量
        var myChart = echarts.init(document.getElementById('container_creditType'), 'macarons');
        var app = {};
        option = null;
        app.title = '客户额度条形图';
        
        option = {
            tooltip : {
                trigger: 'axis',
                axisPointer : {            // 坐标轴指示器，坐标轴触发有效
                    type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
                }
            },
            legend: {
                data: ['总可用额度','总授信额度']
            },
            grid: {
                left: '1%',
                right: '4%',
                bottom: '10%',
                containLabel: true
            },
            xAxis:  {
                type: 'value'
            },
            yAxis: {
                type: 'category',
                data: custName
            },
            series: [
                {
                    name: '总可用额度',
                    type: 'bar',
                    stack: '总量',
                    label: {
                        normal: {
                            show: true,
                            position: 'insideRight'
                        }
                    },
                    data: avlAmt
                },
                {
                    name: '总授信额度',
                    type: 'bar',
                    stack: '总量',
                    label: {
                        normal: {
                            show: true,
                            position: 'insideRight'
                        }
                    },
                    data: eduAmt
                }
            ]
        };

        if (option && typeof option === "object") {
            myChart.setOption(option, true);
        } 

}
/***********************柱形图点击事件****************************************************/
        var myChart1 = echarts.init(document.getElementById('container_creditType'), 'macarons');
        var myChartTrader = echarts.init(document.getElementById("container_restCredit"),'macarons1');
        myChart1.on("click", eConsole);
        myChartTrader.on("click", traderClick);

        function eConsole(param) {
            var ecifNum = spitStr(param.name);//拆分字符串
            getEduSpitByEcifNum(ecifNum);
            setDatagrid(10,0,ecifNum);
        }
        function traderClick(param) {
            var trader = param.name;//
            initTraderDatagrid(10,0,trader)
        }
        //拆分字符串 例如:客户1(1025463) 取出1025463
        function spitStr(str){
            if(str==""||str==null){
                return;
            }
            var st = str.indexOf("(")+1;
            var ed = str.indexOf(")");
            var ecifNum = str.substring(st,ed);
            return ecifNum;

        }
        //根据客户编号查询客户中类额度具体分布
        function getEduSpitByEcifNum(ecifNum){
            
            CommonUtil.ajax( {
                url : "/edCustController/getEdCustRiskListByEcifNum",
                data:{ecifNum:ecifNum},
                callback:function(data){
                    if(data.obj){
                        var eduType=new Array();
                        var valueMap=new Array();
                        var len=data.obj.length;
                        var arr=data.obj;
                        var custNmTitle="";
                        for(var i=0;i<len;i++){
                            eduType.push(arr[i].creditName);
                            var map={};
                            map["value"]=arr[i].loanAmt;
                            map["name"]=arr[i].creditName;
                            valueMap.push(map);
                            custNmTitle=arr[i].customer+"额度类型占比";
                        }
                        typeChart(eduType,valueMap,custNmTitle);
                       
                    }

                }
            });


        }
        //额度中类占比
        function typeChart(eduType,valueMap,custNmTitle){
             //额度类型占比
            var myChart = echarts.init(document.getElementById('container_customerCredit'), 'macarons');
                
            var app = {};
            option = null;
            app.title = '客户额度条形图';
                
        option = {
            title : {
                text: custNmTitle,
                textStyle: {
                    color: '#333',
                    fontStyle: 'normal'},
                //subtext: '纯属虚构',
                x:'center',
                
            },
            tooltip : {
                trigger: 'item',
                formatter: "{a} <br/>{b} : {c} ({d}%)"
            },
            legend: {
                orient: 'vertical',
                x:'left',
                left: 'left',
                data: eduType
            },
            series : [
                {
                    name: '该种类占比',
                    type: 'pie',
                    radius : ['50%','70%'],
                    center: ['44%', '50%'],
                    data:valueMap,
                    itemStyle: {
                        emphasis: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                        }
                    }
                }
            ]
        };
            if (option && typeof option === "object") {
                myChart.setOption(option, true);
            }
        }
        //设置客户详情列表
        function setDatagrid(pageSize,pageIndex,ecifNum){
            var data={};
            data['pageNumber']=pageIndex+1;
            data['pageSize']=pageSize;
            data['branchId']=branchId;
            data['ecifNum']=ecifNum;
            var params = mini.encode(data);
            CommonUtil.ajax({
                url:"/edCustController/getPageEdCustByEcifNum",
                data:params,
                callback : function(data) {
                    var dataArr = data.obj.rows;
                    var groupList = [];
                    var myDate = new Date();
                    var currDate = myDate.toLocaleDateString(); //当前日期 
                    for(var i=0;i<dataArr.length;i++){
                        var group = dataArr[i];
                        
                        var a = {
                            "ecifNum":group.ecifNum,
                            "customer":group.customer,
                            "loanAmt":group.loanAmt,
                            "avlAmt":group.avlAmt,
                            "creditName":group.creditName,
                            "opTime":group.opTime,
                            "dueDate":group.dueDate,
                            "leftDays":CommonUtil.dateDiff(currDate,group.dueDate),
                            "ifOverDue":(CommonUtil.dateDiff(currDate,group.dueDate))>0?"正常":"已过期",

                        };

                        groupList.push(a);
                    }
                    var grid3=mini.get("datagrid3");
                    grid3.setTotalCount(data.obj.total);
                    grid3.setPageIndex(pageIndex);
                    grid3.setPageSize(pageSize);
                    grid3.setData(groupList);
                   
                }
            });
        

        }


/*************************************初始化函数*******************************************************/
//初始化客户额度列表-授信额度大的排在前面
function getEduSpitInit3(pageSize,pageIndex){
        var data={};
        data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		var url="/edCustController/getPageEdCustByEcifNum";
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:url,
			data:params,
			callback : function(data) {
                var dataArr = data.obj.rows;
                    var groupList = [];
                    var myDate = new Date();
                    var currDate = myDate.toLocaleDateString(); //当前日期 
                    for(var i=0;i<dataArr.length;i++){
                        var group = dataArr[i];
                        
                        var a = {
                            "ecifNum":group.ecifNum,
                            "customer":group.customer,
                            "loanAmt":group.loanAmt,
                            "avlAmt":group.avlAmt,
                            "creditName":group.creditName,
                            "opTime":group.opTime,
                            "dueDate":group.dueDate,
                            "leftDays":CommonUtil.dateDiff(currDate,group.dueDate),
                            "ifOverDue":(CommonUtil.dateDiff(currDate,group.dueDate))>0?"正常":"已过期",

                        };

                        groupList.push(a);
                    }
                var grid3=mini.get("datagrid3");
				grid3.setTotalCount(data.obj.total);
				grid3.setPageIndex(pageIndex);
				grid3.setPageSize(pageSize);
				grid3.setData(groupList);
			}
		});


}
//初始化额度类型占比-放置总授信额度最大的客户信息
function initEduType(){
    CommonUtil.ajax({
			url:"/edCustController/getMaxLoanAmt",
			data:{},
			callback : function(data) {
				if(data.obj){
                    var ecifNum = data.obj.ECIF_NUM;
                    getEduSpitByEcifNum(ecifNum);
                }
			}
	});
}
//交易员总额度及总可用额度初始化
function initTraderEdu(pageSize,pageIndex){
    var data={};
    data['pageNumber']=pageIndex+1;
    data['pageSize']=pageSize;
    data['branchId']=branchId;
    var url="/IfsLimitController/searchTraderTotalAndAvlAmt";
    var params = mini.encode(data);
    CommonUtil.ajax({
			url:url,
			data:params,
			callback : function(data) {
				if(data.obj.rows){
                    var traderName=new Array();//交易员代号
                    var totalAmt=new Array();//交易员总授信额度
                    var avlAmt=new Array();//交易员总可用额度
                    var len=data.obj.rows.length;
                    var arr=data.obj.rows;
                    
                    for(var i=len-1;i>=0;i--){
                        traderName.push(arr[i].TRADER);
                        totalAmt.push(parseFloat(arr[i].TOTALAMT)/10000);
                        avlAmt.push(parseFloat(arr[i].AVLAMT)/10000);
                        
                    }
                    traderChart(traderName,totalAmt,avlAmt);
                    
                }
			}
	});

}
 
//交易员柱形图
function traderChart(traderName,totalAmt,avlAmt){
     //交易员剩余额度
    var myChart = echarts.init(document.getElementById("container_restCredit"),'macarons1');
    var app = {};
    option = null;
    app.title = '堆叠柱状图';       

    option = {
        tooltip : {
            trigger: 'axis',
            axisPointer : {            // 坐标轴指示器，坐标轴触发有效
                type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
            }
        },
        legend: {
            data:['总授信额度','总可用额度']
        },
        grid: {
            left: '4%',
            right: '6%',
            bottom: '20%',
            containLabel: true
        },
        xAxis : [
            {
                type : 'category',
                data : traderName
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : [
        {
                name:'总授信额度',
                type:'bar',
                stack: '额度',
                data:totalAmt
            },
            {
                name:'总可用额度',
                type:'bar',
                stack: '额度',
                data:avlAmt
            }
        ]
    };
    if (option && typeof option === "object") {
        myChart.setOption(option, true);
    }
}
//初始化交易员列表
function initTraderDatagrid(pageSize,pageIndex,trader){
    var data={};
    data['pageNumber']=pageIndex+1;
    data['pageSize']=pageSize;
    data['branchId']=branchId;
    data['trader']=trader;
    var url="/IfsLimitController/searchTraderEdu";
    var params = mini.encode(data);
    CommonUtil.ajax({
			url:url,
			data:params,
			callback : function(data) {
                if(data.obj.rows){
                    var dataArr = data.obj.rows;
                    var groupList = [];
                    
                    for(var i=0;i<dataArr.length;i++){
                        var group = dataArr[i];
                        
                        var a = {
                            "trader":group.trader,
                            "product":group.product,
                            "quotaAmt":group.quotaAmount,
                            "avlAmt":group.availAmount,
                            "traderStatus":parseFloat(group.availAmount)>0?"正常":"没有剩余额度",
                            

                        };

                        groupList.push(a);
                    }
                    var grid1=mini.get("datagrid1");
                    grid1.setTotalCount(data.obj.total);
                    grid1.setPageIndex(pageIndex);
                    grid1.setPageSize(pageSize);
                    grid1.setData(groupList);

                }
				
			}
	});

}
</script>
