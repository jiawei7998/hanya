<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
    <link rel="stylesheet" href="css/cockpit_style.css">

    <title>交易员视角</title>
</head>
<body style="width:100%;height:100%;background:#f2f2f2;">
<div class="main_box">
    <div class="cockpit_1 clearfix">
        <div class="mini-panel cockpit_common" style="width:50%;height:360px;padding-bottom:15px;position:relative" title="我的交易">
                <div id="search_form" style="width:330px;">
                        <table id="" style="width:100%;">
                            <input id="startVdate" name="startVdate" class="mini-datepicker" labelField="false" label="" onValuechanged="compareDate" labelStyle="text-align:right;" emptyText="开始日期" />
                            <input id="endVdate" name="endVdate" class="mini-datepicker" labelField="false" label="" onValuechanged="compareDate" labelStyle="text-align:center;" emptyText="结束日期" />
                            <!-- <span style="float:right;margin-right:150px">
                                <a id="search_btn" class="mini-button" style="display: none"    onclick="">查询</a>
                                <a id="clear_btn" class="mini-button" style="display: none"    onClick="clear">清空</a>
                            </span> -->
                        </table>
                    </div>
                <div class="myBusiness_select">                    
                        <input type="button" value="事前">
                        <input type="button" value="正式">
                </div>
            <div id="container_myBusiness" style="height:100%;overflow: hidden;"></div>
        </div>
        <div class="mini-panel cockpit_common" style="width:50%;height:360px;"  title="交易详情——即期交易">
                <div id="datagrid1" class="mini-datagrid" style="width:100%;" 
                    url="data/1.txt" idField="id" 
                    allowResize="true" pageSize="8" 
                    allowCellEdit="true" allowCellSelect="true" multiSelect="true" 
                    editNextOnEnterKey="true"  editNextRowCell="true"
                    
                >
                    <div property="columns">
                        <div type="indexcolumn"></div>
                        <!-- <div type="checkcolumn"></div> -->
                        <div name="money"  field="money" headerAlign="center" allowSort="true" width="120" >金额</div>
                        <div name="state" field="state" width="100" headerAlign="center" allowSort="true" >审批状态</div>            
                        <div name="direction" field="direction" width="100" headerAlign="center" allowSort="true" dateFormat="yyyy-MM-dd">交易方向</div>    
                        <div field="limittime" width="100" headerAlign="center" allowSort="true">剩余期限</div>
                        <div type="comboboxcolumn" field="rival" width="100" headerAlign="center" >交易对手</div>
                        <div type="comboboxcolumn" field="x_institutions" width="100" headerAlign="center" >对方机构</div>    
                    </div>
                </div>
        </div>
    </div>
    
    

    <div class="cockpit_1 clearfix">
        <div class="mini-panel cockpit_common2" style="width:33%;height:360px;" title="成交量占比">
            <div id="container_finishTask" style="height: 100%"></div>
        </div>
        <div class="mini-panel cockpit_common2" style="width:34%;height:360px;" title="指导价格">
                        <div id="datagrid2" class="mini-datagrid" style="width:100%;"
                            url="data/2.txt" idField="id" 
                            allowResize="true" pageSize="8" 
                            allowCellEdit="true" allowCellSelect="true" multiSelect="true" 
                            editNextOnEnterKey="true"  editNextRowCell="true"
                            
                        >
                            <div property="columns">
                                <div type="indexcolumn">序号</div>
                                <!-- <div type="checkcolumn"></div> -->
                                <div field="loginname" width="120" headerAlign="center" allowSort="true">同业存款</div>    
                                <div field="name" width="120" headerAlign="center" allowSort="true">票据</div>
                                <div name="abcde"  field="abcde" headerAlign="center" allowSort="true" width="120" >远期交易</div>
                                <div name="abcdef"  field="abcdef" headerAlign="center" allowSort="true" width="120" >掉期交易</div>
                            </div>
                        </div>
        </div>
        <div class="mini-panel cockpit_common2" style="width:33%;height:360px;" title="交易工具">
                <div class="clearfix">
                        <div class="tools_box">
                            <i class="tools_1"></i>
                            <p>我的工具</p>
                        </div>
                        <div class="tools_box">
                                <i class="tools_1"></i>
                                <p>我的工具</p>
                        </div>
                        <div class="tools_box">
                            <i class="tools_1"></i>
                            <p>我的工具</p>
                        </div>
                        <div class="tools_box">
                            <i class="tools_1"></i>
                            <p>我的工具</p>
                        </div>
                        <div class="tools_box">
                                <i class="tools_1"></i>
                                <p>我的工具</p>
                        </div>
                        <div class="tools_box">
                            <i class="tools_1"></i>
                            <p>我的工具</p>
                        </div>
                    </div>
                
        </div>
    </div>

    <div class="mini-panel cockpit_common2" style="width:100%;height:200px;" title="系统公告">
        <div class="sys_list">
            <span class="point"></span>
            <span class="sys_title">系统公告标题</span><a href="">为给客户提供更优质的服务，我公司将于2018年4月27日1:00至6:00进行业务支撑系统升级，调整期间系统切换至应急状态。</a>
        </div>
        <div class="sys_list">
                <span class="point"></span>
                <span class="sys_title">系统公告标题</span><a href="">为给客户提供更优质的服务，我公司将于2018年4月27日1:00至6:00进行业务支撑系统升级，调整期间系统切换至应急状态。</a>
        </div>
        <div class="sys_list">
                <span class="point"></span>
                <span class="sys_title">系统公告标题</span><a href="">为给客户提供更优质的服务，我公司将于2018年4月27日1:00至6:00进行业务支撑系统升级，调整期间系统切换至应急状态。</a>
        </div>
        <div class="sys_list">
                <span class="point"></span>
                <span class="sys_title">系统公告标题</span><a href="">为给客户提供更优质的服务，我公司将于2018年4月27日1:00至6:00进行业务支撑系统升级，调整期间系统切换至应急状态。</a>
        </div>
    </div>

</div>
    
</body>
</html>
       <script type="text/javascript" src="http://echarts.baidu.com/gallery/vendors/echarts/echarts.min.js"></script>
       <script type="text/javascript" src="http://echarts.baidu.com/gallery/vendors/echarts-gl/echarts-gl.min.js"></script>
       <script type="text/javascript" src="http://echarts.baidu.com/gallery/vendors/echarts-stat/ecStat.min.js"></script>
       <script type="text/javascript" src="http://echarts.baidu.com/gallery/vendors/echarts/extension/dataTool.min.js"></script>
       <script type="text/javascript" src="http://echarts.baidu.com/gallery/vendors/echarts/map/js/china.js"></script>
       <script type="text/javascript" src="http://echarts.baidu.com/gallery/vendors/echarts/map/js/world.js"></script>
       <script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=ZUONbpqGBsYGXNIYHicvbAbM"></script>
       <script type="text/javascript" src="http://echarts.baidu.com/gallery/vendors/echarts/extension/bmap.min.js"></script>
       <script type="text/javascript" src="http://echarts.baidu.com/gallery/vendors/simplex.js"></script>   
       <script src="theme/vintage.js"></script>
       <script src="theme/macarons.js"></script>       
       <script src="theme/shine.js"></script>
       <script src="theme/infographic.js"></script>
       <script src="theme/roma.js"></script>      
       <script type="text/javascript">              

        var myChart = echarts.init(document.getElementById('container_myBusiness'), 'macarons');
        //var dom = document.getElementById("container_myBusiness");
        //var myChart = echarts.init(dom);
        var app = {};
        option = null;
        app.title = '我的交易条形图';
        
        option = {
            tooltip : {
                trigger: 'axis',
                axisPointer : {            // 坐标轴指示器，坐标轴触发有效
                    type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
                }
            },
            legend: {
                data: ['已完成', '未完成']
            },
            grid: {
                left: '1%',
                right: '4%',
                bottom: '7%',
                containLabel: true
            },
            xAxis:  {
                type: 'value'
            },
            yAxis: {
                type: 'category',
                data: ['即期交易','远期交易','掉期交易','外汇交易','黄金','同业拆借','外汇对即期']
            },
            series: [
                {
                    name: '已完成',
                    type: 'bar',
                    stack: '总量',
                    label: {
                        normal: {
                            show: true,
                            position: 'insideRight'
                        }
                    },
                    data: [320, 302, 301, 334, 390, 330, 320]
                },
                {
                    name: '未完成',
                    type: 'bar',
                    stack: '总量',
                    label: {
                        normal: {
                            show: true,
                            position: 'insideRight'
                        }
                    },
                    data: [120, 132, 101, 134, 90, 230, 210]
                }
            ]
        };;
        if (option && typeof option === "object") {
            myChart.setOption(option, true);
        }



        // 交易详情
       
        mini.parse();
        var grid = mini.get("datagrid1");
        grid.load();
    




//成交量
// var dom = echarts.init(document.getElementById('container_finishTask'), 'vintage');
// var dom = document.getElementById("container_finishTask");
var myChart = echarts.init(document.getElementById('container_finishTask'), 'macarons');
var app = {};
option = null;
app.title = '环形图';

option = {
    tooltip: {
        trigger: 'item',
        formatter: "{a} <br/>{b}: {c} ({d}%)"
    },
    legend: {
        orient: 'vertical',
        x: 'left',
        data:['即期交易','远期交易','掉期交易','同业拆借','黄金交易']
    },
    series: [
        {
            name:'成交量环形图',
            type:'pie',
            radius: ['50%', '70%'],
            //center: ['50%', '10%'],
            avoidLabelOverlap: false,
            label: {
                normal: {
                    show: false,
                    position: 'center'
                },
                emphasis: {
                    show: true,
                    textStyle: {
                        fontSize: '30',
                        fontWeight: 'bold'
                    }
                }
            },
            labelLine: {
                normal: {
                    show: false
                }
            },
            data:[
                {value:335, name:'即期交易'},
                {value:310, name:'远期交易'},
                {value:234, name:'掉期交易'},
                {value:135, name:'同业拆借'},
                {value:1548, name:'黄金交易'}
            ]
        }
    ]
};
if (option && typeof option === "object") {
    myChart.setOption(option, true);
}

//我的交易图表宽度初始化

// var myBusiness_child =$('#container_myBusiness')[0];
// $(myBusiness_child).width(100%); 


    </script>


