<%@ page language="java" pageEncoding="UTF-8"%>
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/miniui/ColumnsMenu.js"></script>
<script src="<%=basePath%>/miniScript/common_mini.js" type="text/javascript"></script>
		


<span style="margin: 2px; display: block;">
	<a  id="add_btn" class="mini-button" style="display: none"   onclick="add()">手工新增</a>
	<a  id="addsl_btn" class="mini-button" style="display: none"   onclick="addsl()">添加事前审批</a>
	<a  id="affirm_btn" class="mini-button" style="display: none"   onclick="affirm()">确认选择</a>
	<a  id="edit_btn" class="mini-button" style="display: none"   onclick="edit()">修改</a>
	<a  id="delete_btn" class="mini-button" style="display: none"   onclick="del()">删除</a>
	<a  id="reback_btn" class="mini-button" style="display: none"   onclick="reback()">撤销</a>
	<a  id="recall_btn" class="mini-button" style="display: none"   onclick="recall()">撤回</a>
	<a  id="approve_commit_btn" class="mini-button" style="display: none"    onclick="approve()">审批</a>
	<a  id="approve_mine_commit_btn" class="mini-button" style="display: none"    onclick="commit()">提交审批</a>
	<a  id="approve_log" class="mini-button" style="display: none"     onclick="searchlog()">审批日志</a>
	<a  id="print_bk_btn" class="mini-button" style="display: none"    onclick="print()">打印</a>
	<a  id="batch_approve_btn" class="mini-button" style="display: none"    onclick="batchApprove()">批量审批</a>
	<a  id="batch_commit_btn" class="mini-button" style="display: none"    onclick="batchCommit()">批量提交</a>
	<a  id="opics_check_btn" class="mini-button" style="display: none"    onclick="opicsCheck()">要素批量更新</a>
	<a  id="export_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
	<div id = "approveType" name = "approveType" class="mini-checkboxlist" style="float:right;" 
		onvaluechanged="checkBoxValuechanged"  multiSelect="false" valueField="id" 
		labelStyle="text-align:right;border: none;background-color: #fff; " value="mine" textField="text"
		data="[{id:'finished',text:'已审批列表'}]" >
	</div>
</span>	  	

<script>
	
	

     function checkBoxValuechanged(e){
		 $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
			 search(10, 0);
			 var approveType = mini.get("approveType").getValue();
			 if (userId != 'admin') {
				 if (approveType == "finished") {//已审批
					 if (visibleBtn.add_btn) mini.get("add_btn").setVisible(false);
					 if (visibleBtn.addsl_btn) mini.get("addsl_btn").setVisible(false);
					 if (visibleBtn.edit_btn) mini.get("edit_btn").setVisible(false);
					 if (visibleBtn.delete_btn) mini.get("delete_btn").setVisible(false);
					 if (visibleBtn.approve_commit_btn) mini.get("approve_commit_btn").setVisible(false);
					 if (visibleBtn.approve_mine_commit_btn) mini.get("approve_mine_commit_btn").setVisible(false);
					 if (visibleBtn.approve_log) mini.get("approve_log").setVisible(true);//审批日志
					 mini.get("approve_log").setEnabled(true);//审批日志高亮
					 if (visibleBtn.print_bk_btn) mini.get("print_bk_btn").setVisible(true);//打印
					 mini.get("print_bk_btn").setEnabled(true);//打印高亮
					 /*新增批量提交、批量审批、opics要素批量校验  */
					 if (visibleBtn.batch_commit_btn) mini.get("batch_commit_btn").setVisible(false);//批量提交不显示
					 mini.get("batch_commit_btn").setEnabled(false);//批量提交不高亮
					 if (visibleBtn.batch_approve_btn) mini.get("batch_approve_btn").setVisible(false);//批量审批不显示
					 mini.get("batch_approve_btn").setEnabled(false);//批量审批不高亮
					 if (visibleBtn.opics_check_btn) mini.get("opics_check_btn").setVisible(false);//opics要素批量校验 不显示
					 mini.get("opics_check_btn").setEnabled(false);//opics要素批量校验 不高亮
					 if (visibleBtn.reback_btn) mini.get("reback_btn").setVisible(false);//撤销 不显示
					 if (visibleBtn.recall_btn) mini.get("recall_btn").setVisible(false);//撤回 不显示
				 }
			 } else {
				 if (approveType == "finished") {//已审批
					 mini.get("add_btn").setVisible(false);
					 mini.get("addsl_btn").setVisible(false);
					 mini.get("edit_btn").setVisible(false);
					 mini.get("delete_btn").setVisible(false);
					 mini.get("approve_commit_btn").setVisible(false);
					 mini.get("approve_mine_commit_btn").setVisible(false);
					 mini.get("approve_log").setVisible(false);//审批日志
					 mini.get("approve_log").setEnabled(false);//审批日志高亮
					 mini.get("print_bk_btn").setVisible(true);//打印
					 mini.get("print_bk_btn").setEnabled(true);//打印高亮
					 /*新增批量提交、批量审批、opics要素批量校验  */
					 mini.get("batch_commit_btn").setVisible(false);//批量提交不显示
					 mini.get("batch_commit_btn").setEnabled(false);//批量提交不高亮
					 mini.get("batch_approve_btn").setVisible(false);//批量审批不显示
					 mini.get("batch_approve_btn").setEnabled(false);//批量审批不高亮
					 mini.get("opics_check_btn").setVisible(false);//opics要素批量校验 不显示
					 mini.get("opics_check_btn").setEnabled(false);//opics要素批量校验 不高亮
					 mini.get("reback_btn").setVisible(false);//撤销 不显示
					 mini.get("recall_btn").setVisible(false);//撤回 不显示
				 }
			 }
		 });
 	}
    
    // 初始化界面
 	function initButton(){
 		mini.get("export_btn").setVisible(false);
 		mini.get("add_btn").setVisible(false);
 		mini.get("affirm_btn").setVisible(true);
 		mini.get("addsl_btn").setVisible(false);
 		mini.get("edit_btn").setVisible(false);
 		mini.get("delete_btn").setVisible(false);
 		mini.get("search_btn").setVisible(true); //查询 显示
	 		mini.get("search_btn").setEnabled(true); //查询  高亮 
	 		mini.get("clear_btn").setVisible(true);  //清空 显示
	 		mini.get("clear_btn").setEnabled(true);  //清空  高亮
 	}
 	
 	
 
 	
 	/***
 	 * 批量审批通过
 	 * 
 	 * @author zcm
 	 * x
 	 */
 	function batchCommit(){
 		var rows=grid.getSelecteds();
 	     if(rows.length==0){
 	         mini.alert("请至少选择一条要提交的交易!","提示");
 	         return;
 	     }
 	    for(var i=0;i<rows.length;i++){
	    	 if(CommonUtil.isNull(rows[i]["dealSource"])||CommonUtil.isNull(rows[i]["port"])||CommonUtil.isNull(rows[i]["cost"])||
	    			 CommonUtil.isNull(rows[i]["product"])||CommonUtil.isNull(rows[i]["prodType"])){
	    		 mini.alert("请先进行opics要素检查更新!","提示");
	    		 return;
	    	 }
	    }
 	   /*****************  批量提交前验证start   *******************************/
	    var times=0;
	    var arr= new Array();
	    for(var i=0;i<rows.length;i++){
	    
	    	var map = batchCommitVerify(prdNo,rows[i]);
		    if(map.flag == "false"){
		    	times++;
		    	arr.push(map.contractId);
		    }
	    
	    }
	    
	    if(times>0){
	    	mini.alert("成交单编号:"+arr+"必需的要素还未填完，请先维护再提交审批!","提示");
	    	return;
	    }
	    /******************  批量提交前验证end  ******************************/
	 mini.get("batch_commit_btn").setEnabled(true);//禁止多次点击批量提交
     mini.confirm("确认要批量提交吗？","确认",function (action) {
		if (action == "ok") {
			 var success=0;
		     var fall=0;
		     for(var i=0;i<rows.length;i++){
		    	 if(rows[i]["approveStatus"] == "3"){
			      	var callBack=selectCallBack(prdName);
			        Approve.approveCommitAll(Approve.FlowType.VerifyApproveFlow,rows[i]["ticketId"],Approve.OrderStatus.New,callBack,prdNo,function(){
			       		success=success+1;
			       	});
		    	 }
		    	 if(i==rows.length-1){
		    		setTimeout(function(){
		    			var sec=success;
		 	    		fall=rows.length-sec;
		 	    		var data="提交成功"+sec+"条数据，自动跳过"+fall+"条数据";
		 	    		mini.alert(data,"系统提示",function(){
		 	    		search(grid.pageSize,grid.pageIndex); 
		 	    		});mini.get("batch_commit_btn").setEnabled(true);
		    		},500);
		    	 }	 
		     }
		}});
 	}
 	

 
	$(document).ready(function() {
		initButton();
		search(10, 0);
	});

	
	
</script>