<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
<title></title>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
	<legend>总账结果查询</legend>	
	<div>
		<div id="search_form" style="width:100%" cols="6">
			<input id="br" name="br" class="mini-combobox" labelField="true" required="true"  textField="typeValue" valueField="typeId"
			label="部门：" labelStyle="text-align:right;" value="01" labelStyle="width:100px" emptyText="请选择部门" width="280px" />
			<input id="seetstatus" name="seetstatus" class="mini-combobox" onValuechanged='statusChanged' data="[{id:'1',text:'记账成功'},{id:'2',text:'记账失败'}]" width="280px" emptyText="请选择查询状态" labelField="true"  label="查询状态：" labelStyle="text-align:right;"/>
			<span style="float: right; margin-right: 150px"> 
				<a id="search_btn" class="mini-button" style="display: none"   onclick="search(10,0)">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
			</span>
		</div>
	</div>
</fieldset> 	  
<div class="mini-fit" >
	<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id"  allowAlternating="true"
			onshowrowdetail="onShowRowDetail" allowResize="true" sortMode="client" allowAlternating="true">
		<div property="columns">
			<div type="indexcolumn" width="30px" headerAlign="center">序号</div>
			<div type="expandcolumn"></div>
			<div field="setNo" width="180px" align="center"  headerAlign="center" >核心记账流水</div>    
			<div field="br" width="80px" align="left"  headerAlign="center" >部门</div>                            
			<div field="dealno" width="180px" align="left"  headerAlign="center" >交易号</div>                            
			<div field="product" width="100px" align="right"   headerAlign="center" >产品代码</div>
			<div field="type" width="120px" align="center"  headerAlign="center" allowSort="true">产品类型</div>
			<div name="retCode" field="retcode" width="150px" align="right"   headerAlign="center" allowSort="true">核心记账状态</div>
			<div name="retMsg" field="retmsg" width="120px" align="center"  headerAlign="center" allowSort="true">核心返回码</div>
			<!-- <div field="errorCode" width="150px" align="right"   headerAlign="center" allowSort="true">核心返回错误码</div> -->
			<!-- <div name="cnretmsg" field="cnretmsg" width="120px" align="center"  headerAlign="center" allowSort="true">核心返回中文描述</div> -->
			<div name="errCode" field="errcode" width="120px" align="center"  headerAlign="center" allowSort="true">转化失败代码</div>
			<div name="errMsg" field="errmsg" width="120px" align="center"  headerAlign="center" allowSort="true">转化失败原因</div>
		</div>
	</div>
	<div id="childGrid" class="mini-datagrid borderAll" style="display: none;" showpager="false" sortMode="client" allowAlternating="true">
		<div property="columns">
			<div field="seqNo" width="50" align="left"  headerAlign="center">处理序号</div>
			<div field="br" width="50" headerAlign="center" align="left">部门号</div>    
			<div field="seq" width="80" headerAlign="center" align="left">序号</div>                            
			<div field="glno" width="80" headerAlign="center" align="left">opics科目号</div>                            
			<div field="ccy" width="100" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'Currency'}">币种</div>
			<div field="effDate" width="80" headerAlign="center" align="left">生效日期</div>                            
			<div field="postDate" width="80" headerAlign="center" align="left">过账日期</div>                            
			<div field="cmne" width="80" headerAlign="center" align="left">英文简称</div>                            
			<div field="drcrind" width="80" headerAlign="center" align="left">借贷标志</div>                            
			<div field="amount" width="80" headerAlign="center" align="left">金额</div>                            
			<div field="smeans" width="80" headerAlign="center" align="left">结算方式</div>                            
			<div field="sacct" width="80" headerAlign="center" align="left">结算账号</div>                            
			<div field="intglno" width="70"  headerAlign="center" align="right" >核心科目号</div>                                
			<div field="subject" width="100"  headerAlign="center" align="center" >核心账号</div>
			<div field="subbr" width="100"  headerAlign="center" align="center" >记账网点</div>
		</div>
	</div>
</div>   

<script>
	mini.parse();
	var form = new mini.Form("#search_form");
	var grid=mini.get("datagrid");
	var childgrid=mini.get("childGrid");

	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});

	
	//清空
	function clear(){
		var form=new mini.Form("search_form");
		form.clear();
	}

	function search(pageSize,pageIndex){
		form.validate();
		if(form.isValid()==false){
			mini.alert("请选择查询状态","系统提示");
			return;
		}
		var data=form.getData();
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		data['seetstatus']=mini.get("seetstatus").getValue();
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:"/IfsOpicsAcupController/searchAcupList",
			data:params,
			callback : function(data) {
				if(data.obj.rows.length==0){
					mini.alert("无此记录");
				}else{
					grid.setTotalCount(data.obj.total);
					grid.setPageIndex(pageIndex);
			        grid.setPageSize(pageSize);
					grid.setData(data.obj.rows);
				}
			}
		});
	}
	
	function statusChanged(e){
		/* var value=e.value;
		if(value=="1"){
			grid.hideColumn("cnretmsg");
			grid.hideColumn("errCode");
			grid.hideColumn("errMsg");
			grid.showColumn("retCode");
			grid.showColumn("retMsg");
		}else if(value=="2"){
			grid.hideColumn("cnretmsg");
			grid.hideColumn("errCode");
			grid.hideColumn("errMsg");
			grid.showColumn("retCode");
			grid.showColumn("retMsg");
		}else{
			grid.hideColumn("cnretmsg");
			grid.showColumn("errCode");
			grid.showColumn("errMsg");
			grid.hideColumn("retCode");
			grid.hideColumn("retMsg");
		} */
		//search(grid.pageSize,grid.pageIndex);
	}

	function onShowRowDetail(e){
		var row=e.record;
		if(!row.setNo){
			mini.alert("核心记账流水为空！","系统提示");
			return;
		}
		var td = grid.getRowDetailCellEl(row);
		var gridEl=childgrid.getEl();
		td.appendChild(gridEl);
		gridEl.style.display = "block";
		searchChild(10,0,row.setNo);
	} 
	
	function searchChild(pageSize,pageIndex,setNo){
		var data={'setNo':setNo};
		var params=mini.encode(data);
		CommonUtil.ajax({
			url:'/IfsOpicsAcupController/searchAcupById',
			data:params,
			callback : function(data) {
				//console.log(data);
// 				childgrid.setTotalCount(data.obj.length);
// 		        childgrid.setPageIndex(pageIndex);
// 		        childgrid.setPageSize(pageSize);
 		        childgrid.setData(data.obj);
			}
		});
	}
	
	$(document).ready(function() {
		var jsonData = new Array();
		jsonData.add({'typeId':"01",'typeValue':"01"});
	   	mini.get("br").setData(jsonData);
	   	
	   	grid.hideColumn("cnretmsg");
		grid.hideColumn("errCode");
		grid.hideColumn("errMsg");
		grid.showColumn("retCode");
		grid.showColumn("retMsg");
		
		//search(10, 0);
		grid.hideColumn("errcode");
		grid.hideColumn("errmsg");
	});
</script>
</body>
</html>