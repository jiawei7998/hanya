<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
	<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
</head>
<input id="nettingstatus" name="nettingstatus" class="mini-textbox" labelField="true" label="净额清算状态：" style="width:85%;"
				labelStyle="text-align:center;width:150px;" vtype="maxLength:16" enabled="false"/>
<body style="width: 100%; height: 800px; background: white">
<input id="prdname" name="prdname" class="mini-textbox" labelField="true" enabled="false"
			label="产品名称：" labelStyle="text-align:center;width:150px;" style="width:100%;"/>
<fieldset style="width: 100%; border:solid 1px #aaa; margin-top:5px;position:relative; height: 33%;">
<a id="look_btn" class="mini-button" style="display: none"  >详情</a>
	<legend>详细信息</legend>
		<div id="grid3" class="mini-datagrid borderAll" style="width:100%;height:80%;" idField="id" multiSelect="true"
			allowAlternating="true" allowResize="true" sortMode="client" allowAlternating="true" onrowclick="setOtherGrid">
			<div property="columns">
				<div type="indexcolumn" width="50px" headerAlign="center">序号</div>
<!-- 				<div field="br" width="50px" align="center"  headerAlign="center">部门</div>     -->
				<div field="ticketid" width="150"  headerAlign="center" align="center" >审批单编号</div>
				<div field="product" width="150px" align="center"  headerAlign="center" >产品代码</div>
				<div field="prodtype" width="150px" align="center"  headerAlign="center">产品类型</div>
				<div field="pccy" width="100px" headerAlign="center" align="center" >付款币种</div>
				<div field="rccy" width="150px" headerAlign="center" align="center" >收款币种</div>
				<div field="cno" width="150px" headerAlign="center" align="center" >交易对手</div>
				<div field="fordate" width="150px" headerAlign="center" align="center" >交易日期</div>
				<div field="safekeepacct" width="100px" headerAlign="center" align="center" >托管账户</div>
			</div>
		</div>
	</div>
</fieldset>

<fieldset style="width: 100%;border:solid 1px #aaa;margin-top:5px;position:relative;height: 33%;">
	<legend>付款路径</legend>
	<div id="paypath" style="padding:5px;" class="mini-fit area" tyle="width: 100%; height: 50%;" 
		allowAlternating="true" allowResize="true" border="true" sortMode="client">
		<div class="leftarea">
			<input id="psettmeans" name="psettmeans" class="mini-textbox" labelField="true" style="width:100%;" required="true"
				label="清算方式：" labelStyle="text-align:center;width:150px;" vtype="maxLength:35" enabled="false"/>
		</div>
		<div class="rightarea">
			<input id="psetacct" name="psetacct" class="mini-textbox" labelField="true" label="清算账户：" style="width:75%;"
				labelStyle="text-align:center;width:150px;" vtype="maxLength:16" enabled="false" required="true" />
			<span style="margin-left: 20px"> 
				<a id="refresh" class="mini-button" style="display: none"   onclick="refresh()">刷新</a>
			</span>
		</div>
		
		<div id="grid1" class="mini-datagrid borderAll" style="width:100%;height:80%;" idField="id" multiSelect="true"
			allowAlternating="true" allowResize="true" sortMode="client" allowAlternating="true">
			<div property="columns">
				<div type="indexcolumn" width="50px" headerAlign="center">序号</div>
				<div field="acctno" width="150px" align="center"  headerAlign="center" >账号</div>    
				<!-- <div field="delrecind" width="150px" align="center"  headerAlign="center" >收付款标识</div> -->
				<div field="bic" width="150px" align="center"  headerAlign="center">SWIFT编码</div>
				<div field="dcc" width="100px" headerAlign="center" align="center" >DCC</div>
				<div field="c1" width="150px" headerAlign="center" align="center" >C1</div>
				<div field="c2" width="150px" headerAlign="center" align="center" >C2</div>
				<div field="c3" width="150px" headerAlign="center" align="center" >C3</div>
				<div field="c4" width="100px" headerAlign="center" align="center" >C4</div>
			</div>
		</div>
	</div>
</fieldset>

<fieldset style="width: 100%;border:solid 1px #aaa;margin-top:5px;position:relative;height: 33%;">
	<legend>收款路径</legend>
	<div id="receivepath" style="padding:5px;" class="mini-fit area" tyle="width: 100%; height: 50%;" 
		allowAlternating="true" allowResize="true" border="true" sortMode="client">
		<div class="leftarea">
			<input id="rsettmeans" name="rsettmeans" class="mini-textbox" labelField="true" style="width:100%;" required='true'
				label="清算方式：" labelStyle="text-align:center;width:150px;" vtype="maxLength:35" enabled="false"/>
		</div>
		<div class="rightarea">
			<input id="rsetacct" name="rsetacct" class="mini-textbox" labelField="true" label="清算账户：" style="width:85%;"
				labelStyle="text-align:center;width:150px;" vtype="maxLength:16" enabled="false" required='true'/>
		</div>
		
		<div id="grid2" class="mini-datagrid borderAll" style="width:100%;height:80%;" idField="id" multiSelect="true"
			allowAlternating="true" allowResize="true" sortMode="client" allowAlternating="true">
			<div property="columns">
				<div type="indexcolumn" width="50px" headerAlign="center">序号</div>
				<div field="acctno" width="150px" align="center"  headerAlign="center" >账号</div>    
				<!-- <div field="delrecind" width="150px" align="center"  headerAlign="center" >收付款标识</div> -->
				<div field="bic" width="150px" align="center"  headerAlign="center">SWIFT编码</div>
				<div field="dcc" width="100"  headerAlign="center" align="center" >DCC</div>
				<div field="c1" width="150px" align="center"  headerAlign="center">C1</div>
				<div field="c2" width="150"  headerAlign="center" align="center" >C2</div>
				<div field="c3" width="150"  headerAlign="center" align="center" >C3</div>
				<div field="c4" width="100"  headerAlign="center" align="center">C4</div>
			</div>
		</div>
	</div>
</fieldset>

<div class="centerarea" style='background:white;' width="100%">
	<div class="mini-panel" title="审批面板*" width="100%" allowResize="true" collapseOnTitleClick="true">
		<table id="approve_operate_form" width="100%" height="100%" cols="4">
			<tr>
				<td width="10%" id="td_user">指定审批人</td>
				<td width="40%">
					<input id="user" name="user" class="mini-combobox" style='width:70%;'/>
					<span id="td_user_text"></span>
				</td>
				<td width="10%">常用语</td>
				<td width="40%">
					<input id="add_text_combobox" class="mini-combobox" style='width:70%;' 
						data="[{'id':'1','text':'同意'},{'id':'2','text':'已阅'},{'id':'3','text':'不同意'},{'id':'4','text':'退回修改'},{'id':'5','text':'请领导审批'}]"/>
					<a  id="add_text_btn" class="mini-button" style="display: none"  >添加</a>
				</td>
			</tr>

			<tr>
				<td style="width:100%;" colspan="4">
					<div title="审批意见">
						<input id="approve_msg" class='mini-textarea' required='true'  vtype="maxLength:500" style="width:100%;height:60px;" value="同意"/>
					</div>
				</td>
			</tr>

			<tr>
				<td style="width:100%;" colspan="6">
					<a id="pass_btn"   class="mini-button" style="display: none"   visible="true">直接通过</a>
					<a id="continue_btn" class="mini-button" style="display: none"   visi.ble="true">通过</a>
					<a id="skip_btn"   class="mini-button" style="display: none"   visible="false">加签</a>
					<a id="redo_btn"   class="mini-button" style="display: none"   visible="true">退回发起</a>
					<a id="back_btn"   class="mini-button" style="display: none"   visible="false">驳回</a>
					<a id="nopass_btn" class="mini-button" style="display: none"   visible="true">撤销</a>
				</td>
			</tr>
		</table>
	</div>
</div>
<script>
	mini.parse();
	//获取当前tab
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var row = params.selectData;
	var tradeData = {};
	tradeData.selectData = row;
	tradeData.operType = action;
	tradeData.serial_no = row.ticketId;
	tradeData.task_id = row.taskId;
	var url = window.location.search;
	var action = CommonUtil.getParam(url, "action");
	var ticketId = CommonUtil.getParam(url, "ticketid");
	var prdNo = CommonUtil.getParam(url, "prdNo");
	
	$(document).ready(function() {
		$('#prdname').hide();
		initData();
	});

	// 初始化数据
	function initData(){
		$('#nettingstatus').hide(); // 净额清算状态隐藏,不做展示
		CommonUtil.ajax({
			url:"/IfsOpicsSettleManageController/searchSettInfoList",
			data:ticketId,
			callback : function(data) {
				mini.get("grid3").setData(data);
				mini.get("grid3").select(0);
				setOtherGrid();
			}
		});
		refresh();
	}
	
	function setOtherGrid(){
		//生成参数
		var row = mini.get("grid3").getSelected();
		var data={"ticketid":row.ticketid,"cno":row.cno,"pccy":row.pccy,"rccy":row.rccy,"product":row.product,
				"prodtype":row.prodtype,"safekeepacct":row.safekeepacct};
		CommonUtil.ajax({
			url:"/IfsOpicsSettleManageController/searchSettInfoListDetail",
			data:data,
			callback : function(data) {
				mini.get("psettmeans").setValue("");
				mini.get("psetacct").setValue("");
				mini.get("rsettmeans").setValue("");
				mini.get("rsetacct").setValue("");
				if(data.ifsSdvpHead != null){
					for(var i=0;i<data.ifsSdvpHead.length;i++){
						if(data.ifsSdvpHead[i].delrecind=="D"){
							mini.get("rsettmeans").setValue(data.ifsSdvpHead[i].settmeans);
							mini.get("rsetacct").setValue(data.ifsSdvpHead[i].settacct);
						}else if(data.ifsSdvpHead[i].delrecind=="R"){
							mini.get("psettmeans").setValue(data.ifsSdvpHead[i].settmeans);
							mini.get("psetacct").setValue(data.ifsSdvpHead[i].settacct);
						}
					}
					if(data.payIfsSdvpDetail != null){
						mini.get("grid2").setData(data.payIfsSdvpDetail);
					}
					if(data.recIfsSdvpDetail != null){
						mini.get("grid1").setData(data.recIfsSdvpDetail);
					}
				}
			}
		});
	}
	function refresh(){
		CommonUtil.ajax({
			url:"/IfsOpicsSettleManageController/checkSearchSettInfo",
			data:ticketId,
			callback : function(data) {
				if(data!=""){
					mini.get("continue_btn").setEnabled(false);
					alert(data);
				}else{
					mini.get("continue_btn").setEnabled(true);
				}
			}
		});
	}
	
	//查看
    mini.get("look_btn").on("click",function(){
		queryDealTail();
	}); 
function queryDealTail(){
	var tradeId=ticketId;
    var searchByIdUrl = "";
    var openJspUrl = "";
    var id = "";
    var title = "";
    var datas={};
    datas['ticketId'] = tradeId;
    	 if(prdNo == "irs"){//利率互换
             searchByIdUrl = "/IfsCfetsrmbIrsController/searchCfetsrmbIrsAndFlowIdById";
             openJspUrl = CommonUtil.baseWebPath() + "/cfetsrmb/IRSEdit.jsp?action=detail&ticketid="+tradeId;
             id = "IRSApproveDetail";
             title = "利率互换详情";
         }else if(prdNo == "or"){//买断式回购
             searchByIdUrl = "/IfsCfetsrmbOrController/searchCfetsrmbOrAndFlowIdById";
             openJspUrl = CommonUtil.baseWebPath() + "/cfetsrmb/repoEdit.jsp?action=detail&ticketid="+tradeId;
             id = "repoApproveDetail";
             title = "买断式回购详情";
         }else if(prdNo == "cbt"){//现券买卖
             searchByIdUrl = "/IfsCfetsrmbCbtController/searchCfetsrmbCbtAndFlowIdById";
             openJspUrl = CommonUtil.baseWebPath() + "/cfetsrmb/bondTradingEdit.jsp?action=detail&ticketid="+tradeId;
             id = "bondTradingApproveDetail";
             title = "现券买卖详情";
         }else if(prdNo == "ibo"){//信用拆借
             searchByIdUrl = "/IfsCfetsrmbIboController/searchCfetsrmbIboAndFlowIdById";
             openJspUrl = CommonUtil.baseWebPath() + "/cfetsrmb/creditLoanEdit.jsp?action=detail&ticketid="+tradeId;
             id = "creditLoanApproveDetail";
             title = "信用拆借详情";
         }else if(prdNo == "sl"){//债券借贷
             datas['execId'] = tradeId;
             searchByIdUrl = "/IfsCfetsrmbSlController/searchCfetsrmbSlAndFlowIdById";
             openJspUrl = CommonUtil.baseWebPath() + "/cfetsrmb/bondLendingEdit.jsp?action=detail&ticketid="+tradeId;
             id = "bondLendingApproveDetail";
             title = "债券借贷详情";
         }else if(prdNo == "cr"){//质押式回购
             searchByIdUrl = "/IfsCfetsrmbCrController/searchCfetsrmbCrAndFlowIdById";
             openJspUrl = CommonUtil.baseWebPath() + "/cfetsrmb/pledgeRepoEdit.jsp?action=detail&ticketid="+tradeId;
             id = "pledgeRepoApproveDetail";
             title = "质押式回购详情";
         }else if(prdNo == "goldspt"){//黄金即期
             searchByIdUrl = "/IfsCfetsmetalGoldController/searchCfetsmetalGoldAndFlowById";
             openJspUrl = CommonUtil.baseWebPath() + "/sge/goldsptEdit.jsp?action=detail&ticketid="+tradeId;
             id = "IrsSptApproveDetail";
             title = "黄金即期详情";
         }else if(prdNo == "goldfwd"){//黄金远期
             searchByIdUrl = "/IfsCfetsmetalGoldController/searchCfetsmetalGoldAndFlowById";
             openJspUrl = CommonUtil.baseWebPath() + "/sge/goldfwdEdit.jsp?action=detail&ticketid="+tradeId;
             id = "IrsFwdApproveDetail";
             title = "黄金远期详情";
         }else if(prdNo == "goldswap"){//黄金掉期
             searchByIdUrl = "/IfsCfetsmetalGoldController/searchCfetsmetalGoldAndFlowById";
             openJspUrl = CommonUtil.baseWebPath() + "/sge/goldswapEdit.jsp?action=detail&ticketid="+tradeId;
             id = "IrsSwapApproveDetail";
             title = "黄金掉期详情";
         }else if(prdNo == "goldLend"){//黄金拆借
             searchByIdUrl = "/IfsGoldLendController/searchCfetsGoldLendAndFlowById";
             openJspUrl = CommonUtil.baseWebPath() + "/sge/GoldLendingEdit.jsp?action=detail&ticketid="+tradeId;
             id = "goldLendingApproveDetail";
             title = "黄金拆借详情";
         }else if(prdNo == "debt"){//外币债
        	 searchByIdUrl = "/IfsCfetsrmbCbtController/searchCfetsrmbCbtAndFlowIdById";
             openJspUrl = CommonUtil.baseWebPath() + "/bloomberg/fxccydebtEdit.jsp?action=detail&ticketid="+tradeId;
             id = "fxccydebtApproveDetail";
             title = "外币债详情";
         }else if(prdNo == "rmbspt"){//外汇即期
             searchByIdUrl = "/IfsForeignController/searchSpotAndFlowIdById";
             openJspUrl = CommonUtil.baseWebPath() + "/cfetsfx/rmbsptEdit.jsp?action=detail&ticketid="+tradeId;
             id = "RmbSptApproveDetail";
             title = "人民币即期详情";
         }else if(prdNo == "rmbfwd"){//外汇远期
             searchByIdUrl = "/IfsForeignController/searchFwdAndFlowIdById";
             openJspUrl = CommonUtil.baseWebPath() + "/cfetsfx/rmfwdEdit.jsp?action=detail&ticketid="+tradeId;
             id = "rmfwdApproveDetail";
             title = "人民币远期详情";
         }else if(prdNo == "rmbswap"){//外汇掉期
             searchByIdUrl = "/IfsForeignController/searchRmbSwapAndFlowIdById";
             openJspUrl = CommonUtil.baseWebPath() + "/cfetsfx/rmswapEdit.jsp?action=detail&ticketid="+tradeId;
             id = "rmsSwapApproveDetail";
             title = "外汇掉期详情";
         }else if(prdNo == "ccyswap"){//货币掉期
             searchByIdUrl = "/IfsCurrencyController/searchCcySwapAndFlowIdById";
             openJspUrl = CommonUtil.baseWebPath() + "/cfetsfx/ccyswapEdit.jsp?action=detail&ticketid="+tradeId;
             id = "CcySwapApproveDetail";
             title = "货币掉期详情";
         }else if(prdNo == "rmbopt"){//人民币期权
             searchByIdUrl = "/IfsForeignController/searchRmbOptAndFlowIdById";
             openJspUrl = CommonUtil.baseWebPath() + "/cfetsfx/rmboptEdit.jsp?action=detail&ticketid="+tradeId;
             id = "AdvanceRmBoptApproveDetail";
             title = "人民币期权详情";
         }else if(prdNo == "ccyLending"){//外币拆借
             searchByIdUrl = "/IfsForeignController/searchRmbCcylendingAndFlowIdById";
             openJspUrl = CommonUtil.baseWebPath() + "/cfetsfx/ccyLendingEdit.jsp?action=detail&ticketid="+tradeId;
             id = "AdvanceCcyLendingApproveDetail";
             title = "外币拆借详情";
         }else if(prdNo == "ccypa"){//外币头寸调拨
             searchByIdUrl = "/IfsForeignController/searchRmbCcypaAndFlowIdById";
             openJspUrl = CommonUtil.baseWebPath() + "/cfetsfx/ccyPositionAllocationEdit.jsp?action=detail&ticketid="+tradeId;
             id = "AdvanceAllcationApproveDetail";
             title = "外币头寸调拨详情";
         }else if(prdNo == "ccypspot"){//外汇对即期
             searchByIdUrl = "/IfsForeignController/searchRmbSpotAndFlowIdById";
             openJspUrl = CommonUtil.baseWebPath() + "/cfetsfx/ccypspotEdit.jsp?action=detail&ticketid="+tradeId;
             id = "AdvanceCcySpotApproveDetail";
             title = "外汇对即期详情";
         }else if(prdNo == "dp"){//存单发行
             searchByIdUrl = "/IfsCfetsrmbDpController/searchCfetsrmbDpAndFlowIdById";
             openJspUrl = CommonUtil.baseWebPath() + "/cfetsfx/depositPublishEdit.jsp?action=detail&ticketid="+tradeId;
             id = "depositPublishApproveDetail";
             title = "债券发行详情";
         }
    datas.userId=userId;
    datas.branchId=branchId;
    var params=mini.encode(datas);
    CommonUtil.ajax({
        url:searchByIdUrl,
        data:params,
        callback:function(data){
            var url = openJspUrl;
            var tab = {"id": id,name:id,url:url,title:title,
                        parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
            var paramData = {selectData:data.obj};
            CommonUtil.openNewMenuTab(tab,paramData);
        }
    });
}
</script>
<script type="text/javascript" src="../../Common/Flow/generalCommon.js"></script>
</body>
</html>