<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
<title></title>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
	<legend>参数查询</legend>
	<div>
		<div id="search_form" style="width:100%" cols="6">
			<input id="paramod" name="paramod" class="mini-textbox" width="320px" labelField="true"  label="模块:"
				labelStyle="text-align:center;width:100px"  emptyText="请输入模块"/>
		    <input id="paraid" name="paraid" class="mini-textbox" width="320px" labelField="true"  label="参数id:"
				labelStyle="text-align:center;width:100px"  emptyText="请输入参数"/>
			<span style="float: right; margin-right: 50px"> 
				<a id="search_btn" class="mini-button" style="display: none"   onclick="search(10,0)">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
			</span>
		</div>
	</div>
	<span style="margin:2px;display: block;">
            <a class="mini-button" style="display: none" id="add_btn" onClick="add();">新增</a>
            <a class="mini-button" style="display: none" id="edit_btn" onClick="edit();">修改</a>
            <a class="mini-button" style="display: none" id="delete_btn" onClick="del();">删除</a>
        </span>
</fieldset>
   <div>
	<p>
	GENT参数注意事项：
	</p>
	<p>
	1、paramod为模块，模块分为SL_SPSH（债券账户维护），SL_IINTFC_DATA（市场数据维护），SL_FXDH（外汇货币对维护），SL_CCYP（币种维护），SL_NOSTRO（清算信息维护）
	</p>
	
</div>	  
<div class="mini-fit" >
	<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id"  multiSelect="true"
			allowResize="true" sortMode="client" allowAlternating="true" onrowdblclick="onRowDblClick">
		<div property="columns">
			<div type="checkcolumn"></div> 
			<div field="paramod" width="70" align="center"  headerAlign="center" allowSort="true" >paramod</div>
			<div field="br" width="70" align="center" headerAlign="center" allowSort="true" >br</div>
			<div field="paraid" width="70" headerAlign="center" align="center" allowSort="true" >paraid</div>
			<div field="part1" width="70" align="center"   headerAlign="center" >part1</div>
			<div field="part2" width="60px" align="center"  headerAlign="center" allowSort="true">part2</div>
			<div field="object" width="80" headerAlign="center" align="center">object</div>   
			<div field="operid" width="100" headerAlign="center" align="center">operid</div>                         
			<div field="note" width="70" headerAlign="center" align="center" >note</div>
			<div field="lstdate" width="80" headerAlign="center" align="center" renderer="forma">lstdate</div>
		</div>
	</div>
</div>   

<script>
	mini.parse();

	var form = new mini.Form("#search_form");
	var grid=mini.get("datagrid");
	
	function forma(e) {
		if(e.value!=null){
			var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)==null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g,'/'));
	        if (value) return mini.formatDate(value, 'yyyy-MM-dd');
		}
    }
	
	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});

	//清空
	function clear(){
		form.clear();
		search(10,0);
	}
	
	$(document).ready(function() {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
			search(10, 0);
		});
	});
	
	//查询
	function search(pageSize,pageIndex){
		var data=form.getData();
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
 		var paramod = mini.get("paramod").getValue();
		var paraid = mini.get("paraid").getValue();
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:"/SlGentController/slGentQuery",
			data:params,
			callback : function(data) {
				if(data.obj.rows.length==0){
					/* mini.alert("无符合查询条件的记录"); */
					grid.setData("");
				} else {
					grid.setTotalCount(data.obj.total);
					grid.setPageIndex(pageIndex);
			        grid.setPageSize(pageSize);
					grid.setData(data.obj.rows);
				}
			}
		});
	}
	
	// 新增
	function add(){
		var url = CommonUtil.baseWebPath() + "/../Ifs/acup/SlGentEdits.jsp?action=add";
		var tab = { id: "gentAdd", name: "gentAdd", title: "SlGent新增", url: url,
				showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
		var paramData = {selectData:""};
		CommonUtil.openNewMenuTab(tab,paramData);
	}
	
	// 修改
	function edit(){
		var row = grid.getSelected();
		if(row){
			var lstdate = new Date(row.lstdate);
        	row.lstdate = lstdate;
			var url = CommonUtil.baseWebPath() +"/../Ifs/acup/SlGentEdits.jsp?action=edit";
			var tab = {id: "gentEdit",name:"gentEdit",url:url,title:"SlGent修改",
						parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
			var paramData = {selectData:row};
			CommonUtil.openNewMenuTab(tab,paramData);
		}else{
			mini.alert("请选择一条记录","提示");
		}
	}
	
	function onRowDblClick(e){
		var row = grid.getSelected();
		if(row){
			var lstdate = new Date(row.lstdate);
        	row.lstdate = lstdate;
			var url = CommonUtil.baseWebPath() +"/../Ifs/acup/SlGentEdits.jsp?action=detail";
			var tab = {id: "gentDetail",name:"gentDetail",url:url,title:"SlGent详情",
				parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
			var paramData = {selectData:row};
			CommonUtil.openNewMenuTab(tab,paramData);
		}else{
			mini.alert("请选择一条记录","提示");
		}
	}
	
	// 删除
	function del(){
		var rows=grid.getSelecteds();
		if(rows.length==0){
			mini.alert("请选中一行","提示");
			return;
		}
		mini.confirm("您确认要删除该SlGent信息?","系统警告",function(value){   
			if (value=='ok'){   
				var data=rows[0];
				params = mini.encode(data);
				CommonUtil.ajax( {
					url:"/SlGentController/deleteSlGent",
					data:params,
					callback : function(data) {
						mini.alert("删除成功.","系统提示");
						search(10,0);
					}
				});
			}
		});
	}

</script>
</body>
</html>