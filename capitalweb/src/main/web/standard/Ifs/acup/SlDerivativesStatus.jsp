<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
<title></title>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
	<legend>外汇估值查询</legend>
	<div>
		<div id="search_form" style="width:100%" cols="6">
			<!-- <input id="dealno" name="dealno" class="mini-textbox" width="320px" labelField="true"  label="交易号："
				labelStyle="text-align:center;width:100px"  emptyText="请输入交易号"/>
		    <input id="tablename" name="tablename" class="mini-textbox" width="320px" labelField="true"  label="表名："
				labelStyle="text-align:center;width:100px"  emptyText="请输入表名"/> -->
			<input id="postdate" name="postdate" field="postdate" class="mini-datepicker" width="320px" labelField="true"   label="日期："
						required="true"  labelStyle="text-align:right;" allowinput="false" labelStyle="width:100px"   />
			
			</nobr>
			<span style="float: right; margin-right: 50px"> 
				<a id="search_btn" class="mini-button" style="display: none"   onclick="search(10,0)">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
				<a id="send_btn" class="mini-button" style="display: none"  onclick="send()">发送</a>
			</span>
		</div>
	</div>
</fieldset>
<div>
	<p>
	注意事项：
	</p>
	<p>
	1、估值发送成功后显示条数会减少。
	</p>
	
</div>	   	  
<div class="mini-fit" >
	<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id"  multiSelect="true"
			allowResize="true" sortMode="client" allowAlternating="true">
		<div property="columns">
			<div type="checkcolumn"></div> 
			<div field="fXFIG_SVCN" width="70" align="center"  headerAlign="center" allowSort="true" >FXFIG_SVCN</div>
			<div field="fXFIG_IBCD" width="70" align="center" headerAlign="center" allowSort="true" >FXFIG_IBCD</div>
			<div field="fXFIG_GWAM" width="70" headerAlign="center" align="center" allowSort="true" >FXFIG_GWAM</div>
			<div field="fXFIG_GEOR" width="70" align="center"   headerAlign="center" >FXFIG_GEOR</div>
			<div field="fXFIG_OPNO" width="60px" align="center"  headerAlign="center" renderer="onDateRenderer" allowSort="true">FXFIG_OPNO</div>
			<div field="fIFE14_OPICS_REF_NO" width="80" headerAlign="center" align="center">FIFE14_OPICS_REF_NO</div>   
			<div field="fIFE14_DEAL_IL" width="100" headerAlign="center" align="center">FIFE14_DEAL_IL</div>                         
			<div field="fIFE14_CCY" width="70" headerAlign="center" align="center" >FIFE14_CCY</div>
			<div field="fIFE14_REVALU_AMT" width="80" headerAlign="center" align="center" renderer="onDateRenderer">FIFE14_REVALU_AMT</div>
		</div>
	</div>
</div>   

<script>
	mini.parse();

	var form = new mini.Form("#search_form");
	var grid=mini.get("datagrid");
	
	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});

	//清空
	function clear(){
		form.clear();
		search(10,0);
	}
	
	$(document).ready(function() {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
			search(10, 0);
		});
	});
	
	//查询
	function search(pageSize,pageIndex){
		var data=form.getData();
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
 		var time = mini.get("postdate").getValue();
		var inputtime = mini.formatDate(time,'yyyy-MM-dd');
		data['postdate'] = inputtime;
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:"/SlDerivativesStatusController/slDerivativesStatusQuery",
			data:params,
			callback : function(data) {
				
				if(data.obj.rows.length==0){
					/* mini.alert("无符合查询条件的记录"); */
					grid.setData("");
				} else {
					grid.setTotalCount(data.obj.total);
					grid.setPageIndex(pageIndex);
			        grid.setPageSize(pageSize);
					grid.setData(data.obj.rows);
				}
			}
		});
	}
	
	
	
	
	 function send() {
		var grid = mini.get("datagrid");
		var row = grid.getSelected();
		form.validate();
	    if (form.isValid() == false) {
			mini.alert("信息填写有误，请重新填写", "系统提示");
			return;
	    }
		
		
		var data=form.getData();
	    var date = mini.get("postdate").getValue();
		var postDate = mini.formatDate(date,'yyyy-MM-dd');
		data['postdate'] = postDate; 
		var params = mini.encode(data);
		CommonUtil.ajax({
			url : "/SendRevaluationChoisController/sendChois",
			data : params,
			callback : function(data) {
				search(10,0);
				/* if(data.obj.ret=="1"){
					mini.alert("发送成功" ,"提示");
					//mini.get("desc").setValue(msg+data.obj.retMsg+"\n");
					//return;
					search(10,0);
				}else{
					mini.alert("存在发送失败的估值" ,"提示");
					search(10,0);
				} */
			}
		});
		
	} 
</script>
</body>
</html>