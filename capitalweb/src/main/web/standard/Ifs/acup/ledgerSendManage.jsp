 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
<title></title>
</head>
<body style="width: 100%; height: 90%; background: white">
	<fieldset class="mini-fieldset">
		<legend>核心账务发送管理</legend>
		<div>
			<div id="search_form" style="width: 100%" cols="6">
			    <input id="br" name="br" class="mini-combobox mini-mustFill" width="320px"  labelField="true" label="部门："  required="true"  vtype="maxLength:5"  labelStyle="text-align:right;"
					 labelStyle="width:100px" data="CommonUtil.serverData.dictionary.opicsBr" value="<%=__sessionUser.getOpicsBr()%>" emptyText="请选择部门"  />
				<!-- <input id="br" name="br" class="mini-combobox" width="320px"  labelField="true" required="true"  label="部门：" textField="typeValue"
					valueField="typeId" labelStyle="text-align:right;" labelStyle="width:100px" value="01" emptyText="请选择部门" /> -->
				<input id="postdate" name="postdate" field="postdate" class="mini-datepicker" width="320px" labelField="true" label="OPICS账务日期："
					required="true"  labelStyle="text-align:right;" allowinput="false" labelStyle="width:100px" />
				<input id="effDate" name="effDate" field="effDate" class="mini-datepicker" width="320px" labelField="true" label="核心账务日期："
					   required="true"  labelStyle="text-align:right;" allowinput="false" labelStyle="width:100px" />
				<!-- <span style="float: right; margin-right: 150px">
					<a id="clear_btn" class="mini-button" style="display: none"  onclick="clear()">清除</a>
				</span> -->
			</div>
		</div>
	</fieldset>
	<span style="margin: 2px; display: block;">
		<span style="margin: 2px; display: block;">
		  <!--   <a id="opicsBsys" class="mini-button" style="display: none"  onclick="opicsBsys()">OPICS自动跑批</a> -->
			<a id="create" class="mini-button" style="display: none"  onclick="create()">生成</a>
			<a id="send" class="mini-button" style="display: none"  onclick="createFile()">发送</a>
			<a id="get" class="mini-button" style="display: none"  onclick="getMsg()">取回</a>
			<a id="clear_btn" class="mini-button" style="display: none"  onclick="empty()">清空</a>
		</span>
	</span>
	<div>
	<p>
	总账流程及注意事项：
	</p>
	<p>
	1、【生成总账数据】请确认OPICS账务日期和部门，该步骤等待系统处理成功即可(耗时较长)。
	</p>
	<p>
	2、系统已控制确认所有部门账务生成账务成功，方可进行【核心送账】操作。
	</p>
	<p>
	3、系统已控制【核心送账】成功，方可进行【总账文件取回】操作，该步骤为异步处理，直到查询成功为止(系统已控制)。
	</p>
	</div>
	<div class="mini-fit">
		<input id="desc" name="desc" class="mini-textarea" style="width: 50%; height: 60%" enabled="false" 
			borderStyle="border:1;"/>
	</div>

<script>
	mini.parse();

	var form = new mini.Form("#search_form");
	var grid = mini.get("postdate");
	$(document).ready(function() {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
			getOpicsDate();
		});
	});
	
	//获取OPICS系统日期
	function getOpicsDate() {
    /* 	var jsonData = new Array();
	    jsonData.add({'typeId' : "01",'typeValue' : "01"});
	    mini.get("br").setData(jsonData); */
	    var data = form.getData(true);
	    //行方要求，页面加载时，部门不能有默认值
	    data['br'] = "01";
	    var params = mini.encode(data);
	    CommonUtil.ajax({
			url : "/IfsOpicsAcupController/acupFileMaker",
			data : params,
			callback : function(data) {
			    mini.get("postdate").setValue(data);
				mini.get("effDate").setValue(data);
			}
	    });
	}


	// OPICS自动跑批
	/* function opicsBsys() {
		mini.confirm("确认进行OPICS跑批？", "系统提示", function (value) {
			if (value == "ok") {
			    form.validate();
			    if (form.isValid() == false) {
					mini.alert("信息填写有误，请重新填写", "系统提示");
					return;
			    }
			    var msg = mini.get("desc").getValue();
			    mini.get("desc").setValue(msg + "正在进行OPICS自动跑批.........\n");
			    msg += "正在进行OPICS自动跑批.........\n";
			    var data1 = form.getData(true);
			    var params = mini.encode(data1);
			    CommonUtil.ajax({
					url : "/IfsOpicsAcupController/opicsBsys",
					data : params,
					complete : function(data) {
						var text = data.responseText;
		            	var desc = JSON.parse(text);
		            	var content = desc.obj.retMsg;
		           		mini.get("desc").setValue(msg + content);
					}
			    });
			}
		});		    
	} */
	
	// 生成总账数据
	function create() {
		mini.confirm("确认生成？", "系统提示", function (value) {
			if (value == "ok") {
			    form.validate();
			    if (form.isValid() == false) {
					mini.alert("信息填写有误，请重新填写", "系统提示");
					return;
			    }
			    var msg = mini.get("desc").getValue();
			    mini.get("desc").setValue(msg + "正在生成总账.........\n");
			    msg += "正在生成总账.........\n";
			    var data1 = form.getData(true);
			    var params = mini.encode(data1);
			    CommonUtil.ajax({
					url : "/IfsOpicsAcupController/createAcup",
					data : params,
					complete : function(data) {
						var text = data.responseText;
		            	var desc = JSON.parse(text);
		            	var content = desc.obj.retMsg;
		           		mini.get("desc").setValue(msg + content);
		        		mini.alert(content);
					}
			    });
			}
		});		    
	}
	
	// 发送
	function createFile() {
	    form.validate();
	    if (form.isValid() == false) {
			mini.alert("信息填写有误，请重新填写", "系统提示");
			return;
	    }
	    var data = form.getData(true);
	    var params = mini.encode(data);
	    var msg = mini.get("desc").getValue();
	    mini.get("desc").setValue(msg);
	    CommonUtil.ajax({
			url : "/IfsOpicsAcupController/checkSendFlag",
			data : params,
			callback : function(data) {
			    if (data.obj.retMsg == "repeat") {
			    	mini.confirm("总账已发送，确认重新发送?","系统警告",function(value){
						if (value == "ok") {
						    var data = form.getData(true);
						    var params = mini.encode(data);
						    var msg = mini.get("desc").getValue();
						    msg += "正在通知核心记账...\n";
						    mini.get("desc").setValue(msg);
						    	CommonUtil.ajax({
								url : "/IfsOpicsAcupController/createDlAcupFile",
								data : params,
								complete : function(data) {
									var text = data.responseText;
					            	var desc = JSON.parse(text);
					            	var content = desc["desc"];
					           		mini.get("desc").setValue(msg + content + "\n");
								}
						    });
						}
					});
			    } else if(data.obj.retMsg == "empty"){
			    	mini.alert("文件未生成，请先生成文件");
			    	mini.get("desc").setValue(msg + "文件未生成，请先生成文件" + "\n");
			    }else if(data.obj.retMsg == "dataEmpty"){
			    	mini.alert("数据为空,不允许发送");
			    	mini.get("desc").setValue(msg + "数据为空,不允许发送" + "\n");
			    }  else {
			    	mini.confirm("确认发送？", "系统提示", function (value) {
						if (value == "ok") {
							var data = form.getData(true);
							var params = mini.encode(data);
							var msg = mini.get("desc").getValue();
							msg += "正在通知核心记账...\n";
							mini.get("desc").setValue(msg);
							CommonUtil.ajax({
							    url : "/IfsOpicsAcupController/createDlAcupFile",
							    data : params,
							    complete : function(data) {
							    	var text = data.responseText;
					            	var desc = JSON.parse(text);
					            	var content = desc["desc"];
					           		mini.get("desc").setValue(msg + content + "\n");
							    }
							});
						}
					});					
			    }
			}
	    });
	}
	
	// 取回
	function getMsg() {
		mini.confirm("确认取回？", "系统提示", function (value) {
			if (value == "ok") {
			    form.validate();
			    if (form.isValid() == false) {
					mini.alert("信息填写有误，请重新填写", "系统提示");
					return;
			    }
			    var data = form.getData(true);
			    var params = mini.encode(data);
			    var msg = mini.get("desc").getValue();
			    msg += "正在取回文件并解析.........\n";
			    mini.get("desc").setValue(msg);
			    CommonUtil.ajax({
					url : "/IfsOpicsAcupController/getDlAcupResultPath",
					data : params,
					complete : function(data) {
						var text = data.responseText;
		            	var desc = JSON.parse(text);
		            	var content = desc["desc"];
		           		mini.get("desc").setValue(msg + content + "\n");
		           		// updateBondLimitDetail(); // 更新债券存单限额
		           		// updateSDInverstReport(); //获取债券投资业务限额管理
					}
			    });
			}
		});    
	}
	
	// 清空
	function clear() {
	    form.clear();
	}
	
	// 清除文本域信息
	function empty() {
		mini.get("desc").setValue("");
	}
	
	function updateBondLimitDetail(){
		form.validate();
	    if (form.isValid() == false) {
			mini.alert("信息填写有误，请重新填写", "系统提示");
			return;
	    }
	    var data = form.getData(true);
	    var params = mini.encode(data);
	    var url = "/IfsLimitController/updateBondLimitDetail";
	    
		CommonUtil.ajax({
			url:url,
			data:params,
			callback : function(data) {
				
			}
		});
	}
	
	function updateSDInverstReport(){//限额表报生成
		form.validate();
	    if (form.isValid() == false) {
			mini.alert("信息填写有误，请重新填写", "系统提示");
			return;
	    }
	    var data = form.getData(true);
	    var params = mini.encode(data);
	    var url = "/IfsLimitController/updateSDInverstReport";
	    
		CommonUtil.ajax({
			url:url,
			data:params,
			callback : function(data) {
				
			}
		});
	}
	
</script>
</body>
</html>