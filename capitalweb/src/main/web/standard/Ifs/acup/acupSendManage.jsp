<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js"
	type="text/javascript"></script>
<script type="text/javascript"
	src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
<title></title>
</head>
<body style="width: 100%; height: 100%; background: white">
	<fieldset class="mini-fieldset">
		<legend>总账发送管理</legend>
		<div>
			<div id="search_form" style="width: 100%" cols="6">
				<!-- <input id="br" name="br" class="mini-combobox" labelField="true"
					required="true"  label="部门：" textField="BR"  multiSelect="true" showNullItem="false
					valueField="BR" labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请选择部门" width="280px" /> -->
				<!-- <div class="mini-combobox" name="br" id="br" width="280px" popupWidth="170" textField="BR" valueField="BR" multiSelect="true" labelField="true"
				label="部门：" labelStyle="text-align:right;" labelStyle="width:100px" >
					<div property="columns">
						<div header="all" field="BR"></div>
					</div>
				</div> -->
				<input id="postdate" name="postdate" field="postdate"
					class="mini-datepicker" labelField="true" label="OPICS账务日期："
					required="true"  labelStyle="text-align:right;" allowinput="false"
					labelStyle="width:100px" ondrawdate="postdateController"  width="280px" /> <span
					style="float: right; margin-right: 150px"> 
					<a id="clear_btn"class="mini-button" style="display: none"  onclick="clear()">清空</a> 
					<a id="search_btn" class="mini-button" style="display: none"  onclick="query">查询</a>
					</span>
			  </div>
		</div>
	</fieldset>
	<span style="margin: 2px; display: block;"> 
		<a id="search_btn" class="mini-button" style="display: none"  onclick="generateData()">生成总账数据</a>
		<a id="search_btn" class="mini-button" style="display: none"  onclick="sendCore()">核心送账</a>
		<!--<a id="search_btn" class="mini-button" style="display: none"  onclick="getMsg()">总账文件取回</a> </span>
		 <a id="export_btn" class="mini-button" style="display: none"   onclick="exportAcup()">导出报表</a>  -->
		<!-- <a id="search_btn" class="mini-button" style="display: none"   onclick="report()">生成报表</a> -->
	</span>
	<div id="tradeManage" class="mini-fit">
		<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="dealNo" 
		allowAlternating="true" allowResize="true" onrowdblclick="onRowDblClick" border="true" 
		multiSelect="false" sortMode="client">
			<div property="columns">
				<div type="DEALNO" headerAlign="center" width="50">OPICS交易号</div>
				<div field="DEALTYPE" width="180" headerAlign="center" align="center" allowSort="true">交易类型</div>   
				<div field="BSPL" width="70"  align="center" headerAlign="center">BSPL</div>
				<div field="SUBJECT" width="70"  align="center" headerAlign="center">记账科目</div>
				<div field="AMOUNT" width="200" headerAlign="center" allowSort="true">金额T</div> 
				<div field="CCY" width="100" headerAlign="center" align="center" allowSort="true">币种</div>   
				<div field="DRCRIND" width="80" headerAlign="center" align="center" allowSort="true">借贷标记</div>  
				<div field="DEALFLAG" width="280" headerAlign="center" align="center" allowSort="true">发送标识</div>
			
			</div>
		</div>  
	</div>
	<!-- <div>
	<p>
	总账流程及注意事项：
	</p>
	<p>
	1、【生成总账数据】请确认OPICS账务日期和部门，该步骤等待系统处理成功即可(耗时较长)。
	</p>
	<p>
	2、系统已控制确认所有部门账务生成账务成功，方可进行【核心送账】操作。
	</p>
	<p>
	3、系统已控制【核心送账】成功，方可进行【总账文件取回】操作，该步骤为异步处理，直到查询成功为止(系统已控制)。
	</p>
	</div> -->
	<div class="mini-fit">
		<input id="desc" name="desc" class="mini-textarea"
			style="width: 80%; height: 60%" enabled="false"
			borderStyle="border:1;" />
	</div>
	

	<script>
	mini.parse();
	var form = new mini.Form("#search_form");
	var today = "";
	var grid=mini.get("datagrid");
	$(document).ready(function() {
		//init();
		search(grid.pageSize,0);
	});
	function query(){
		search(grid.pageSize,0);
	}
	function search(pageSize,pageIndex) {	 //页面加载时，部门无默认值
		//var params ={"br":"01"};
	    CommonUtil.ajax({
			url : "/SendAcupController/queryAcup",
			data : params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			    //getOpicsBr();
			}
	    });
	}

	function getOpicsBr(){
		var params={};
		CommonUtil.ajax({
			url : "/SendAcupController/acupOpicsBr",
			data : params,
			callback : function(data) {
			    mini.get("br").setData(data.obj);
			    var value="";
			    $.each(data.obj,function(i,v){
					value +=v.BR+",";
			    });
			    mini.get("br").selects(value);
			    var json = {
			    	"postdate":mini.get("postdate").getValue()
			    };
			    queryStatus(json,initCallBack);
			}
	    });
	}
	
	function queryStatus(json,callback){
	    var params = mini.encode(json,"yyyy-MM-dd");
		CommonUtil.ajax({
			url : "/IfsOpicsAcupController/queryOperaRec",
			data : params,
			callback : function(data) {
		     	if(callback){
		     		callback(data);
		     	}
			}
	    });
	}
	
	var initCallBack = function(data){
		var msg = "";
     	var first = "正在生成总账...\n";
     	var second = "正在通知核心记账...\n";
     	var third = "正在取回文件并解析...\n";
     	var records = data.obj.dataMap.records;
     	$.each(records,function(i,v){
 			if(v.currStep=="1"){
 				msg +="机构："+v.br+v.retMsg+"\n";
 			}else if(v.currStep=="2"){
 				msg +=second+v.retMsg+"\n";
 			}else{
 				msg +=third + v.retMsg+"\n";
 			}
     	});
     	if(msg!=""){
	     	mini.get("desc").setValue(first+msg);
     	}
	};
	
	function postdateController(e){
		var date = e.date;
		var postdate = mini.parseDate(today);
		if(postdate && postdate!=""){
			if(postdate.getTime()<date.getTime()){
				e.allowSelect = false;
			}
			
		   	if(date.getTime()<(postdate.getTime()-86400000)){
				e.allowSelect = false;
			}   
		}
	} 

	//生成总账数据
	function generateData() {
	    form.validate();
	    if (form.isValid() == false) {
			mini.alert("信息填写有误，请重新填写", "系统提示");
			return;
	    }
	    var data = form.getData(true);
	    var msg = "正在生成总账...\n";
	    mini.get("desc").setValue(msg);
	    var params = mini.encode(data,"yyyy-MM-dd");
	    debugger;
	    CommonUtil.ajax({
			url : "/SendAcupController/acupCreate",
			data : params,
			callback : function(data) {
					if(data.obj.desc=="999"){
					mini.alert("总账生成成功" ,"提示");
					//mini.get("desc").setValue(msg+data.obj.retMsg+"\n");
					//return;
					}else{
						mini.alert("生成失败，错误代码为："+data.obj.desc ,"提示");
					}
				
			}
			  //  mini.get("desc").setValue(msg + data.obj.retMsg);
			    //mini.alert(data.obj.retMsg, "提示");
			
	    });
	    /* var qJson = {
	    	"postdate":mini.get("postdate").getValue(),
	    	"status":"F",
	    	"currstep":"1"
	    };
	    
	    var data = form.getData(true);
	    queryStatus(qJson,function(qData){
	    	var failBr = "";
	    	var records = qData.obj.dataMap.records;
	    	$.each(records,function(i,v){
	    		if(data.br.indexOf(v.br)>-1){
	    			failBr = v.br+",";	
	    		}
	 	    });
	    	 */
	    /* 	if(failBr==""){
	    		generate(data);
        	}else{
        		failBr = failBr.substr(0,failBr.length-1);
        		mini.confirm("机构:"+failBr+"总账数据生成有误，是否重新生成?","系统提示",function(value){
        			if(value=="ok"){
        				generate(data);
        			}
        		});
        	} */
	    
	}
	
	function generate(data){
	   var msg = "正在生成总账...\n";
	    mini.get("desc").setValue(msg);
	    var params = mini.encode(data,"yyyy-MM-dd");
	    CommonUtil.ajax({
			url : "/IfsOpicsAcupController/generateData",
			data : params,
			callback : function(data) {
			    mini.get("desc").setValue(msg + data.obj.retMsg);
			    //mini.alert(data.obj.retMsg, "提示");
			}
	    });
	}
	
	//发送
	function sendCore() {
	    form.validate();
	    if (form.isValid() == false) {
			mini.alert("信息填写有误，请重新填写", "系统提示");
			return;
	    }
	    //var msg = mini.get("desc").getValue();
		//msg +="正在通知核心记账...\n";
		//mini.get("desc").setValue(msg);
	    //校验部门账务是否都发送
	    //var json = {postdate:mini.get("postdate").getValue()};
	    //var params = mini.encode(json,"yyyy-MM-dd");
		CommonUtil.ajax({
			url : "/SendAcupController/sendAcupChois",
			data : params,
			callback : function(data) {
				if(data.obj.ret=="T"){
					mini.alert("总账发送成功" ,"提示");
					//mini.get("desc").setValue(msg+data.obj.retMsg+"\n");
					//return;
				}else{
					mini.alert("总账发送失败" ,"提示");
				}
				
				/* var repeat = data.obj.dataMap.repeat;
				var confirmMsg = confirmMsgHandler(data.obj.dataMap.records);
				if(repeat){
					mini.prompt("总账数据已发送至核心,请输入'ok'确定重新上送","提示",function(action, value) {
						if (action != "ok") {
							return;
						}
						if(value!="ok"){
							 mini.alert("输入有误,请重新点击","提示");
							 return;
						}
					    data['Status'] = "1";
					    send(json,confirmMsg);
					});
				}else{
					send(json,confirmMsg);
				} */
			}
		}); 
	}
	
	function confirmMsgHandler(records){
		var setCount=0;
		var succCount = 0;
		var failCount = 0;
		$.each(records,function(i,v){
			if(v.currStep=="1"){
				setCount += parseInt(v.setNo);
				succCount += parseInt(v.succCount);
				failCount += parseInt(v.failCount);
			}
		});
		var msg = "当前总账生成套数"+setCount+",其中成功"+succCount+"条,失败"+failCount+"条,是否发送核心记账?";
		return msg;
	}
	
	function send(data,confirMsg){
		mini.confirm(confirMsg,"系统提示",function(value){
			if(value!="ok"){
				return;
			}
			var params = mini.encode(data,"yyyy-MM-dd");
			var msg = mini.get("desc").getValue();
			CommonUtil.ajax( {
				url:"/IfsOpicsAcupController/sendCore",
				data:params,
				callback : function(data) {
					mini.get("desc").setValue(msg+data.obj.retMsg+"\n");
					//mini.alert(data.desc,"提示");
				}
			});
		});
	} 
	
	function getMsg() {
	    form.validate();
	    if (!form.isValid()) {
			mini.alert("信息填写有误，请重新填写", "系统提示");
			return;
	    }
	    var json = {postdate:mini.get("postdate").getValue()};
	    var params = mini.encode(json,"yyyy-MM-dd");
	    var msg = mini.get("desc").getValue();
	    msg += "正在取回文件并解析...\n";
	    mini.get("desc").setValue(msg);
	    CommonUtil.ajax({
			url : "/IfsOpicsAcupController/getAcupResultPath",
			data : params,
			callback : function(data) {
				mini.get("desc").setValue(msg+data.obj.retMsg);
			    //mini.alert(data.desc, "提示");
			}
	    });
	}
	
	//导出报表
	function exportAcup() {
	    mini.confirm("您确认要导出Excel吗?","系统提示",
	    function(action) {
			if (action == "ok") {
			    var data = {};
			    data['reportStatus'] = mini.get("reportStatus").getValue();
			    data['postdate'] = mini.get("postdate").getFormValue();
			    var fields = '';
			    for ( var id in data) {
					fields += '<input type="hidden" id="' + id + '" name="' + id + '" value="' + data[id] + '">';
			    }
			    $('<form action="' + CommonUtil.pPath+ "/sl/IfsOpicsAcupController/expAcup" + '" method="post"> '+ fields + '</form>').appendTo('body').submit().remove();
			}
	    });
	}
	
	//清空
	function clear() {
	    form.clear();
	}
    </script>
</body>
</html>