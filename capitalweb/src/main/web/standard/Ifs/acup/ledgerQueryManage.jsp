<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
<title></title>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
	<legend>核心账务查询</legend>
	<div>
		<div id="search_form" style="width:100%" cols="6">
			<input id="br" name="br" class="mini-combobox mini-mustFill"  width="320px" labelField="true" label="部门："  required="true"  vtype="maxLength:5"  labelStyle="text-align:center;"
			    labelStyle="width:100px" data="CommonUtil.serverData.dictionary.opicsBr" value="<%=__sessionUser.getOpicsBr()%>" emptyText="请选择部门"   />
			<input id="dealflag" name="dealflag" class="mini-combobox" width="320px" labelStyle="text-align:center;"  value="" label="发送标志："
				emptyText="请选择查询条件" labelField="true" data="[{id:'T',text:'记账成功'},{id:'F',text:'记账失败'},{id:'',text:'全部'}]"/>
			<!-- <input id="setno" name="setno" class="mini-textbox" width="320px" labelField="true"  label="套号："
				labelStyle="text-align:center;width:100px"  emptyText="请输入套号"/> -->
			<input id="dealno" name="dealno" class="mini-textbox" width="320px" labelField="true"  label="交易号："
				labelStyle="text-align:center;width:100px"  emptyText="请输入交易号"/>
			<input id="postdate" name="postdate" field="postdate" class="mini-datepicker" width="320px" labelField="true"   label="OPICS账务日期："
						required="true"  labelStyle="text-align:right;" allowinput="false" labelStyle="width:100px"   />
			
			<span style="float: right; margin-right: 50px"> 
				<a id="search_btn" class="mini-button" style="display: none"   onclick="search(10,0)">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
				 <a id="generateData" class="mini-button" style="display: none"  onclick="generateData()">生成总账数据</a>
				<a id="sendCore" class="mini-button" style="display: none"  onclick="sendCore()">核心送账</a>
				<!-- <a id="exportHistory" class="mini-button" style="display: none"   onclick="exportHistory()">导出历史报表</a> -->
			</span>
		</div>
	</div>
</fieldset> 
<div>
	<p>
	总账流程及注意事项：
	</p>
	<p>
	1、选择【OPICS账务日期】，点击【生成总账数据】，该步骤等待系统处理成功即可(耗时较长)。
	</p>
	<p>
	2、在账务生成账务成功，方可进行【核心送账】操作。
	</p>
	<p>
	3、【核心送账】成功后，未发送为X,发送标识为T,失败则为F；选择“发送标志”查询是否发送成功。
	</p>
</div>	  
<div class="mini-fit" >
	<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id"  multiSelect="true"
			allowResize="true" sortMode="client" allowAlternating="true">
		<div property="columns">
			<div type="checkcolumn"></div> 
				<div field="dealno" headerAlign="center" width="80">OPICS交易号</div>
				<div field="choisDealRefno" headerAlign="center" width="80">CHOIS参考号</div>
				<div field="setno" headerAlign="center" width="80">OPICS套号</div>
				<div field="postdate" width="90" headerAlign="center" allowSort="true">日期</div> 
				<div field="dealtype" width="80" headerAlign="center" align="center" allowSort="true">交易类型</div>   
				<div field="bspl" width="70"  align="center" headerAlign="center">BSPL</div>
				<div field="subject" width="90"  align="center" headerAlign="center">记账科目</div>
				<div field="amount" width="90" headerAlign="center" allowSort="true">金额</div> 
				<div field="ccy" width="90" headerAlign="center" align="center" allowSort="true">币种</div>   
				<div field="drcrind" width="80" headerAlign="center" align="center" allowSort="true">借贷标记</div>  
				<div field="dealflag" width="80" headerAlign="center" align="center" allowSort="true">发送标识</div>
			
		
			
		</div>
	</div>
</div>   

<script>
	mini.parse();

	var form = new mini.Form("#search_form");
	var grid=mini.get("datagrid");
	//var childgrid=mini.get("childGrid");
	
	function onDateRenderer(e) {
		var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)==null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g,'/'));
		if (value) return mini.formatDate(value, 'yyyy-MM-dd');
	}
	
	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});

	//清空
	function clear(){
		form.clear();
		//getOpicsDate();
		mini.get("br").setValue(opicsBr);//自贸区加br
		search(10,0);
	}
	
	$(document).ready(function() {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
			search(10, 0);
		});
	});
	//amount类型转换
	function TypeChange(e){
		var num=parseFloat(e.value);
		return  num.toFixed(2);
	}
	

	//$(document).ready(function() {
	    //getOpicsDate();
	//});
	
	//获取OPICS系统日期
	function getOpicsDate() {
	   /*  var jsonData = new Array();
	    jsonData.add({'typeId' : "01",'typeValue' : "01"});
	    mini.get("br").setData(jsonData); */
	    var data = form.getData(true);
	    //行方要求，页面加载时，部门不能有默认值
	    data['br'] = "01";
	    var params = mini.encode(data);
	    CommonUtil.ajax({
			url : "/IfsOpicsAcupController/acupFileMaker",
			data : params,
			callback : function(data) {
			    mini.get("startDate").setValue(data);
			    mini.get("endDate").setValue(data);
			}
	    });
	}

	//查询
	/* function search(pageSize,pageIndex){
		var data=form.getData();
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		data['seetstatus']=mini.get("seetstatus").getValue();
		var date = mini.get("postDate").getValue();
		var postDate = mini.formatDate(date,'yyyy-MM-dd');
		data['postDate'] = postDate; 
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:"/IfsOpicsAcupController/ledgerQuery",
			data:params,
			callback : function(data) {
				if(data.obj.rows.length==0){
					 mini.alert("无符合查询条件的记录"); 
					grid.setData("");
				} else {
					grid.setTotalCount(data.obj.total);
					grid.setPageIndex(pageIndex);
			        grid.setPageSize(pageSize);
					grid.setData(data.obj.rows);
				}
			}
		});
	} */
	function search(pageSize,pageIndex) {	 //页面加载时，部门无默认值
		
		var data=form.getData();
		var date = mini.get("postdate").getValue();
		var postDate = mini.formatDate(date,'yyyy-MM-dd');
		var dealflag = mini.get("dealflag").getValue();
		var dealno = mini.get("dealno").getValue();
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['postdate'] = postDate; 
		data['dealno'] = dealno; 
		data['dealflag'] = dealflag; 
		//var params ={"br":"01"};
		var params = mini.encode(data);
	    CommonUtil.ajax({
			url : "/SendAcupController/queryAcup",
			data : params,
			callback : function(data) {
				
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			    //getOpicsBr();
			}
	    });
	}
	  /*重发出现错误的套号 */
	function retry(){
		var rows = grid.getSelecteds();
    	if(rows.length > 0){ //选中行执行
    		var data=form.getData();
    		var setno ="";
    		var postdate ="";
    		for(var i=0;i<rows.length;i++){
    			 if (i == 0) {
    					setno =  rows[i].setNo ;
					} else {
						setno = setno + "," + rows[i].setNo ;
					} 
    		}
    		var postdate = rows[0].postDate;
    		data['setno']=setno;
    		data['postdate']=postdate;
    		var params = mini.encode(data);
			mini.confirm("您确认要重发选中记录吗?","系统警告",function(value){
				if(value=="ok"){
					CommonUtil.ajax({
						url:"/IfsOpicsAcupController/retrysendAcup",
						data: params,
						callback : function(data) {
							var text = data.responseText;
			            	var desc = JSON.parse(text);
							var content = desc.obj.retMsg;
								search(10,0);
						
						}
					});
				}
			});
    	} else {
    		mini.alert("请选中一行执行!");
    	}
	   }
	


	//生成总账数据
	function generateData() {
		 form.validate();
		    if (form.isValid() == false) {
				mini.alert("信息填写有误，请重新填写", "系统提示");
				return;
		    }
	    var data = form.getData(true);
	   // var msg = "正在生成总账...\n";
	    //mini.get("desc").setValue(msg);
	    var date = mini.get("postdate").getValue();
		var postDate = mini.formatDate(date,'yyyy-MM-dd');
		data['postdate'] = postDate; 
		var params = mini.encode(data);
	    CommonUtil.ajax({
			url : "/SendAcupController/acupCreate",
			data : params,
			callback : function(data) {
					if(data.desc=="999"){
					mini.alert("总账生成成功" ,"提示");
					//mini.get("desc").setValue(msg+data.obj.retMsg+"\n");
					//return;
					search(10,0);
					}else{
						mini.alert("生成失败，错误代码为："+data.desc ,"提示");
					}
				}
			 
			
	    });
	   
	    
	}
	
	  //发送
		function sendCore() {
		    form.validate();
		    if (form.isValid() == false) {
				mini.alert("信息填写有误，请重新填写", "系统提示");
				return;
		    }
		   
		    var data=form.getData();
		    var date = mini.get("postdate").getValue();
			var postDate = mini.formatDate(date,'yyyy-MM-dd');
			data['postdate'] = postDate; 
			var params = mini.encode(data);
			CommonUtil.ajax({
				url : "/SendAcupController/sendAcupChois",
				data : params,
				callback : function(data) {
				 if(data.obj=="3"){
						mini.alert("总账已经发送过，请勿重复发送" ,"提示");
						//mini.get("desc").setValue(msg+data.obj.retMsg+"\n");
						//return;
					}
					/*else if(data.obj=="1"){
						mini.alert("总账发送成功" ,"提示");
					}else{
						mini.alert("存在发送失败的账务" ,"提示");
					} */
					
					search(10,0);
				}
			}); 
		}
	
	
	
	
	/* $(document).ready(function() {
		var jsonData = new Array();
		jsonData.add({'typeId':"01",'typeValue':"01"});
	   	mini.get("br").setData(jsonData);
	}); */
	
	//导出
	function exportAcup(){
		var content = grid.getData();
		if(content.length == 0){
			mini.alert("请先查询数据");
			return;
		}
		mini.confirm("您确认要导出Excel吗?","系统提示", 
			function (action) {
				if (action == "ok"){
			 		var form = new mini.Form("#search_form");
					var data = form.getData(true);
					var fields = null;
					for(var id in data){
						fields += '<input type="hidden" id="' + id + '" name="'+ id +'" value="' + data[id] + '">';
					}
					var urls = CommonUtil.pPath + "/sl/IfsOpicsAcupController/exportExcel";                                                                                                                         
					$('<form action="'+ urls +'" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();   
				}
			}
		);
	}
	
	//过账日期
	function onDrawDateStart(e) {
        var startDate = e.date;
        var endDate= mini.get("endDate").getValue();
        if(CommonUtil.isNull(endDate)){
        	return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }
	function onDrawDateEnd(e) {
        var endDate = e.date;
        var startDate = mini.get("startDate").getValue();
        if(CommonUtil.isNull(startDate)){
        	return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }
	//导出历史送账记录
	/* function exportHistory(){
		var content = grid.getData();
		if(content.length == 0){
			mini.alert("请先查询数据");
			return;
		}
		mini.confirm("您确认要导出历史送账Excel吗?","系统提示", 
			function (action) {
				if (action == "ok"){
			 		var form = new mini.Form("#search_form");
					var data = form.getData(true);
					var fields = null;
					for(var id in data){
						fields += '<input type="hidden" id="' + id + '" name="'+ id +'" value="' + data[id] + '">';
					}
					var urls = CommonUtil.pPath + "/sl/IfsOpicsAcupController/exportHistory";                                                                                                                         
					$('<form action="'+ urls +'" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();   
				}
			}
		);
	} */
</script>
</body>
</html>