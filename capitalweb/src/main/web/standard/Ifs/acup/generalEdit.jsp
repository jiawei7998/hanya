<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>清算路径维护</title>
</head>

<body style="width:100%;height:100%;background:white">
<div class="mini-splitter" style="width:100%;height:100%;">
	<div size="90%" showCollapseButton="false">
		<div id="field_form" class="mini-fit area"  style="background:white">
            <fieldset>
            <legend>清算路径信息</legend>
            <div class="leftarea">
                <input id="rsettmeans" name="rsettmeans" class="mini-textbox" labelField="true" label="清算方式：" 
                	emptyText="请输入清算方式" data="CommonUtil.serverData.dictionary.limitType" required="true"
                	style="width:100%" labelStyle="text-align:center;width:150px" onvaluechanged="checkCoreNo"/>
			</div>			
			<div class="rightarea">
                <input id="rsetacct" name="rsetacct" class="mini-textbox" labelField="true" label="清算账户：" required="true"
                	emptyText="请输入清算账户" style="width:100%" labelStyle="text-align:center;width:150px" />
			</div>			
            </fieldset>  
		</div>
	</div>		
    <div id="functionIds"  style="padding-top:30px;text-align:center;">
        <div style="margin-bottom:10px; text-align: center;">
        	<a class="mini-button" style="display: none"  style="width:120px;" id="save_btn" onclick="save">保存</a>
        </div>
        <div style="margin-bottom:10px; text-align: center;">
        	<a class="mini-button" style="display: none"  style="width:120px;" id="close_btn" onclick="close">关闭</a>
        </div>    
    </div> 
</div>
<script type="text/javascript">
	mini.parse();
	//获取当前tab
	var url = window.location.search;
	var action = CommonUtil.getParam(url,"action"); // 增删改
	var form = new mini.Form("#field_form");
	
	$(document).ready(function(){
	    initData();
	}); 
	
	//初始化数据
	function initData(){
	}
	
	//保存
	function save(){
	    var way = mini.get("rsettmeans").getValue();
		var url = CommonUtil.baseWebPath() + "/../Ifs/acup/general.jsp";
		var tab = { id: "generalAdd", name: "generalAdd", title: "清算路径新增", url: url,
				showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
		var paramData = {selectData:way};
		CommonUtil.openNewMenuTab(tab,paramData);
		top["win"].closeMenuTab();
	}
	
	function close(){
		top["win"].closeMenuTab();
	}
	
	
 	// 下拉值校验
    function onComboValidation(e) {
        var items = this.findItems(e.value);
        if (!items || items.length == 0) {
            e.isValid = false;
            e.errorText = "输入值不在下拉数据中";
        }
    }
</script>
</body>
</html>