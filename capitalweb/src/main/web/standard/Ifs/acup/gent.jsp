<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js"
		type="text/javascript"></script>
	<script type="text/javascript"
		src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
	<title></title>
</head>

<body style="width: 100%; height: 100%; background: white">
<fieldset class="mini-fieldset">
	<legend>账务信息</legend>
	<div>
		<div id="search_form" style="width: 100%" cols="6">
			<input id="tableid" name="tableid" class="mini-textbox" labelField="true" label="TABLEID：" 
				labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请输入TABLEID" width="250px" />
			<input id="br" name="br" class="mini-combobox" labelField="true" label="BR：" textField="typeValue"
				valueField="typeId" labelStyle="text-align:right;" labelStyle="width:100px" value="01" emptyText="请选择部门"
				width="250px" />
			<input id="tablevalue" name="tablevalue" class="mini-textbox" labelField="true" label="TABLEVALUE：" 
				labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请输入TABLEVALUE" width="250px" />
			<input id="tabletext" name="tabletext" class="mini-textbox" labelField="true" label="TABLETEXT："
				   labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请输入TABLETEXT" width="250px" />
			<input id="text" name="text" class="mini-textbox" labelField="true" label="TEXT："
				   labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请输入TEXT" width="250px" />
			<input id="text1" name="text1" class="mini-textbox" labelField="true" label="TEXT1："
				   labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请输入TEXT1" width="250px" />
			<span style="float: right; margin-right: 150px">
				<a id="search_btn" class="mini-button"    onclick="query()">查询</a>
				<a id="clear_btn" class="mini-button"   onclick="clear()">清除</a>
			</span>
		</div>
	</div>
</fieldset>

<span style="margin: 2px; display: block;"> 
	<a  id="add_btn" class="mini-button"    onclick="add()">新增</a>
	<a  id="edit_btn" class="mini-button"    onclick="edit()">修改</a>
	<a  id="delete_btn" class="mini-button" style="display: none"   onclick="del()">删除</a>
</span>

<div class="mini-fit" style="width:100%;height:100%;">
	<div id="grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id" allowAlternating="true" onrowdblclick="onRowDblClick"
		allowResize="true" sortMode="client" allowAlternating="true" >
       	<div property="columns">
            <div type="indexcolumn" headerAlign="center" align="center">序号</div>
            <div field="tableid" headerAlign="center" allowSort="true" width="80px" align="center">TABLEID</div>
            <div field="br" headerAlign="center" width="80px" align="center" >BR</div>
            <div field="tablevalue" headerAlign="center" width="80px" align="center">TABLEVALUE</div>
            <div field="tabletext" headerAlign="center"  width="100px" align="center">TABLETEXT</div>
            <div field="text" headerAlign="center"  width="80px" align="center">TEXT</div>
            <div field="text1" headerAlign="center" width="100px" allowSort="true" align="center">TEXT1</div>
            <div field="text2" headerAlign="center" width="150px" allowSort="true" align="center">TEXT2</div>
            <div field="text3" headerAlign="center" width="150px" allowSort="true" align="center">TEXT3</div>
            <div field="lstmntdate" width="100" headerAlign="center" align="center" renderer="onDateRenderer">LSTMNTDATE</div>
       	</div>
   	</div>
</div>
	
<script>
	mini.parse();
	var form = new mini.Form("#search_form");
	var grid = mini.get("grid");
	
	function onDateRenderer(e) {
		var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)==null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g,'/'));
		if (value) return mini.formatDate(value, 'yyyy-MM-dd');
	}
	
	$(document).ready(function() {
		var jsonData = new Array();
	    jsonData.add({'typeId' : "01",'typeValue' : "01"});
	    mini.get("br").setData(jsonData);
		search(10, 0);
	});
	
	/* 查询 */
	function search(pageSize,pageIndex){
		var data = form.getData(true);
		data['pageNumber'] = pageIndex+1;
		data['pageSize'] = pageSize;
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:"/IfsGentController/searchPageGent",
			data:params,
			callback : function(data) {
				if(data.obj.rows.length == "0"){
					mini.alert("查无记录");
				}
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	
	/* 按钮 查询事件 */
	function query(){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统提示");
			return;
		}
		search(grid.pageSize,0);
	}
	
	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});
	
	// 新增
	function add(){
		var url = CommonUtil.baseWebPath() + "/../Ifs/acup/gentEdit.jsp?action=add";
		var tab = { id: "gentAdd", name: "gentAdd", title: "gent新增", url: url,
				showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
		var paramData = {selectData:""};
		CommonUtil.openNewMenuTab(tab,paramData);
	}
	
	// 修改
	function edit(){
		var row = grid.getSelected();
		if(row){
			var LSTMNTDATE = new Date(row.LSTMNTDATE);
        	row.LSTMNTDATE = LSTMNTDATE;
			var url = CommonUtil.baseWebPath() +"/../Ifs/acup/gentEdit.jsp?action=edit";
			var tab = {id: "gentEdit",name:"gentEdit",url:url,title:"gent修改",
						parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
			var paramData = {selectData:row};
			CommonUtil.openNewMenuTab(tab,paramData);
		}else{
			mini.alert("请选择一条记录","提示");
		}
	}

	function onRowDblClick(e){
		var row = grid.getSelected();
		if(row){
			var LSTMNTDATE = new Date(row.LSTMNTDATE);
        	row.LSTMNTDATE = LSTMNTDATE;
			var url = CommonUtil.baseWebPath() +"/../Ifs/acup/gentEdit.jsp?action=detail";
			var tab = {id: "gentDetail",name:"gentDetail",url:url,title:"gent详情",
				parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
			var paramData = {selectData:row};
			CommonUtil.openNewMenuTab(tab,paramData);
		}else{
			mini.alert("请选择一条记录","提示");
		}
	}
	
	// 删除
	function del(){
		var rows=grid.getSelecteds();
		if(rows.length==0){
			mini.alert("请选中一行","提示");
			return;
		}
		mini.confirm("您确认要删除该gent信息?","系统警告",function(value){   
			if (value=='ok'){   
				var data=rows[0];
				params = mini.encode(data);
				CommonUtil.ajax( {
					url:"/IfsGentController/deleteSlGent",
					data:params,
					callback : function(data) {
						mini.alert("删除成功.","系统提示");
						search(10,0);
					}
				});
			}
		});
	}

	//清空
	function clear() {
	    form.clear();
	    search(10,0);
	}
</script>
</body>
</html>