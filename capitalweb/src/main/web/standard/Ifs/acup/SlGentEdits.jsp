<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../../global.jsp" %>
<html>
<head>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>SlGent维护</title>
</head>

<body style="width:100%;height:100%;background:white">
<div class="mini-splitter" style="width:100%;height:100%;">
    <div size="90%" showCollapseButton="false">
        <div id="field_form" class="mini-fit area" style="background:white">
            <fieldset>
                <legend>SlGent信息</legend>
                <div class="leftarea">
                    <input id="paramod" name="paramod" class="mini-textbox" labelField="true" label="PARAMOD:"
                           emptyText="请输入paramod" required="true"
                           style="width:100%" labelStyle="text-align:center;width:150px" onvaluechanged="checkProd"/>
                    <input id="paraid" name="paraid" class="mini-textbox" labelField="true" label="PARAID:"
                           emptyText="请输入paraid" required="true" style="width:100%"
                           labelStyle="text-align:center;width:150px"/>
                    <input id="part1" name="part1" class="mini-textbox" labelField="true" label="PART1:"
                           emptyText="请输入part1" style="width:100%" labelStyle="text-align:center;width:150px"/>
                    <input id="part2" name="part2" class="mini-textbox" labelField="true" label="PART2:"
                           emptyText="请输入part2" style="width:100%" labelStyle="text-align:center;width:150px"/>
                    <input id="lstdate" name="lstdate" class="mini-datepicker" label="LSTDATE:"
                           emptyText="请选择lstdate"
                           labelField="true" style="width:100%" labelStyle="text-align:center;width:150px"
                           renderer="onDateRenderer"/>
                </div>
                <div class="rightarea">
                    <input id="br" name="br" class="mini-combobox" labelField="true" label="BR:" required="true"
                           value="01"
                           emptyText="请输入BR" style="width:100%" labelStyle="text-align:center;width:150px"
                           textField="typeValue" valueField="typeId"/>
                    <input id="object" name="object" class="mini-textbox" labelField="true" label="OBJECT:"
                           emptyText="请输入TABLETEXT" style="width:100%" labelStyle="text-align:center;width:150px"/>
                    <input id="note" name="note" class="mini-textbox" labelField="true" label="NOTE:"
                           emptyText="请输入TEXT1" style="width:100%" labelStyle="text-align:center;width:150px"/>
                    <input id="operid" name="operid" class="mini-textbox" label="OPERID:" emptyText="请输入operid"
                           labelField="true" style="width:100%" labelStyle="text-align:center;width:150px"/>
                </div>
            </fieldset>
        </div>
    </div>
    <div id="functionIds" style="padding-top:30px;text-align:center;">
        <div style="margin-bottom:10px; text-align: center;">
            <a class="mini-button" style="width:120px;" id="save_btn" onclick="save">保存</a>
        </div>
        <div style="margin-bottom:10px; text-align: center;">
            <a class="mini-button" style="width:120px;" id="close_btn" onclick="close">关闭</a>
        </div>
    </div>
</div>
<script type="text/javascript">
    mini.parse();
    //获取当前tab
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var row = params.selectData;
    var url = window.location.search;
    var action = CommonUtil.getParam(url, "action"); // 增删改
    var form = new mini.Form("#field_form");

    function onDateRenderer(e) {
        var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value) == null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g, '/'));
        if (value) return mini.formatDate(value, 'yyyy-MM-dd');
    }

    $(document).ready(function () {
        initData();
        var jsonData = new Array();
        jsonData.add({'typeId': "01", 'typeValue': "01"});
        mini.get("br").setData(jsonData);
    });

    //初始化数据
    function initData() {
        if ($.inArray(action, ["edit", "detail"]) > -1) {//修改、详情
        	mini.get("paramod").setEnabled(false);
        	mini.get("br").setEnabled(false);
        	mini.get("paraid").setEnabled(false);
        	//mini.get("part1").setEnabled(false);
           // mini.get("part2").setEnabled(false);
            form.setData(row);
            if (action == "detail") {
                $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>SlGent</a> >> 详情");
                form.setEnabled(false);
                mini.get("save_btn").setVisible(false);
            }
        }
    }

    //保存
    function save() {
        //表单验证！！！
        form.validate();
        if (form.isValid() == false) {
            mini.alert("表单填写有误!", "提示");
            return;
        }
        var saveUrl = null;
        if (action == "add") { // 新增时保存
            saveUrl = "/SlGentController/gentAdd";
        } else if (action == "edit") { // 修改时保存
            saveUrl = "/SlGentController/gentEdit";
        }
        var data = form.getData(true);
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: saveUrl,
            data: params,
            callback: function (data) {
                mini.alert("保存成功", '提示信息', function () {
                    top["win"].closeMenuTab();
                });
            }
        });
        calibration();
    }

    //批量校准信息
//     function calibration() {
//         CommonUtil.ajax({
//             url: "/IfsOpicsCustController/batchCalibration",
//             data: {type: 2},
//             callback: function (data) {
//                 mini.alert(data.obj.retMsg, data.obj.retCode);
//             }
//         });
//     }

    function close() {
        top["win"].closeMenuTab();
    }

    // 验证数据是否存在
//     function check() {
//         var tableid = mini.get("tableid").getValue();
//         var tablevalue = mini.get("tablevalue").getValue();
//         CommonUtil.ajax({
//             url: "/IfsGentController/searchIfsOpicsGent",
//             data: {tableid: tableid, tablevalue: tablevalue},
//             callback: function (data1) {
//                 if (data1.code == "error.common.0000") {
//                     if (data1.obj != null) {
//                         mini.alert("此数据已存在!请重新填写!", '提示信息', function () {
//                             mini.get("tableid").setValue("");
//                             mini.get("tablevalue").setValue("");
//                         });
//                     }
//                 }
//             }
//         });
//     }
</script>
</body>
</html>