<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
<title></title>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
	<legend>核心账务发送状态</legend>
	<div>
		<div id="search_form" style="width:100%" cols="6">
			
			<input id="postdate" name="postdate" field="postdate" class="mini-datepicker" width="320px" labelField="true"   label="OPICS账务日期："
						required="true"  labelStyle="text-align:right;" allowinput="false" labelStyle="width:100px"   />
			
			<span style="float: right; margin-right: 50px"> 
				<a id="search_btn" class="mini-button" style="display: none"   onclick="search(10,0)">查询</a>
				<a id="deal_btn" class="mini-button" style="display: none"   onclick="del()">删除</a>
				
			</span>
		</div>
	</div>
</fieldset> 
<div>
	<p>
	注意事项：
	</p>
	<p>
	1、如果存在失败账务需要重发，选择【OPICS账务日期】，点击【查询】。
	</p>
	<p>
	2、选择查询出来的数据，点击【删除】操作。
	</p>
	<p>
	3、【删除】成功后，回到“核心账务查询”页面，重新点击【核心送账】。
	</p>
</div>	  
<div class="mini-fit" >
	<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id"  multiSelect="true"
			allowResize="true" sortMode="client" allowAlternating="true">
		<div property="columns">
			<div type="checkcolumn"></div> 
				<div field=postdate width="90" headerAlign="center" allowSort="true">日期</div> 
			
		</div>
	</div>
</div>   

<script>
	mini.parse();

	var form = new mini.Form("#search_form");
	var grid=mini.get("datagrid");
	//var childgrid=mini.get("childGrid");
	
	function onDateRenderer(e) {
		var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)==null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g,'/'));
		if (value) return mini.formatDate(value, 'yyyy-MM-dd');
	}
	
	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});

	
	
	$(document).ready(function() {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
			search(10, 0);
		});
	});
	//amount类型转换
	function TypeChange(e){
		var num=parseFloat(e.value);
		return  num.toFixed(2);
	}
	

	//$(document).ready(function() {
	    //getOpicsDate();
	//});
	
	
	function search(pageSize,pageIndex) {	 //页面加载时，部门无默认值
		
		var data=form.getData();
		var date = mini.get("postdate").getValue();
		var postDate = mini.formatDate(date,'yyyy-MM-dd');
		
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['postdate'] = postDate; 
		
		//var params ={"br":"01"};
		var params = mini.encode(data);
	    CommonUtil.ajax({
			url : "/SendAcupController/queryAcupSendFlag",
			data : params,
			callback : function(data) {
				
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			    //getOpicsBr();
			}
	    });
	}
	
	
	  function del() {
			var row = grid.getSelected();
			if (row) {
				mini.confirm("您确认要删除选中记录?","系统警告",function(value){
					if(value=="ok"){
					CommonUtil.ajax({
						url: "/SendAcupController/deleteAcupSendFlag",
						data: {postdate: row.postdate},
						callback: function (data) {
							if (data.code == 'error.common.0000') {
								mini.alert("删除成功")
								search(10,0);
                              //search(grid.pageSize,grid.pageIndex);
							} else {
								mini.alert("删除失败");
							}
						}
					});
					}
				});
			}
			else {
				mini.alert("请选中一条记录！", "消息提示");
			}

		}   
</script>
</body>
</html>