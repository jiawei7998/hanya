<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="../../global.jsp"%>
<html>
<head>
    <title>总账重发</title>
    <script src="<%=basePath %>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
</head>

<body style="width:100%;height:100%;background:white">

<fieldset id="fd" class="mini-fieldset">
	<legend>
		<label>总账重发</label>
	</legend>
	<div id='search_form' style='width:70%'>
	<!-- 这里必须是value="01"并隐藏  -->
	 <input id="br" name="br" class="mini-hidden  mini-combobox mini-mustFill" width="320px" labelField="true" label="部门："  required="true"  vtype="maxLength:5"  labelStyle="text-align:right;"
					   labelStyle="width:100px"  value="01" emptyText="请选择部门"   onvaluechanged="query"  />
	 <input id='postdate' name='postdate' class='mini-datepicker'  width="320px"  labelField='true' label='账务日期：'  onvaluechanged="query" labelStyle='text-align:right'/>
	  <input id="setno" name="setno" class="mini-textbox"  width="320px"  labelField="true"  label="套号："
				labelStyle="text-align:center;width:100px"  emptyText="请输入套号"/>
	  <input id="dealno" name="dealno" class="mini-textbox"  width="320px"  labelField="true"  label="交易号："
				labelStyle="text-align:center;width:100px"  emptyText="请输入交易号"/>
        <input id='effDate' name='effDate' class='mini-datepicker'  width="320px"  labelField='true' label='核心账务日期：'  onvaluechanged="query" labelStyle='text-align:right'/>

	</div>
	<span style="float:right;margin-right: 60px;margin-top:-30px">
	
		 <a id="search_btn" class="mini-button" style="display: none"    onClick="query()">查询</a>
         <a id="clear_btn" class="mini-button"  style="display: none"   onClick="clear()">清空</a>
		 <a id="save_btn" class="mini-button" style="display: none"    onClick="save()">保存</a>
		 <a id="retry_btn" class="mini-button" style="display: none"    onClick="retrySend()">重发</a>
	</span>
</fieldset>
<div id="MiniSettleDayAccount" class="mini-fit" style="margin-top: 2px;">
  <div id="datagrid" class="mini-datagrid borderAll" style="height:100%;" idField="setNo" allowResize="true" multiSelect="true"  allowCellEdit="true"  
			border="true" onshowrowdetail="onShowRowDetail" >
    <div property="columns">
      <div type="checkcolumn"></div>
      <div type="indexcolumn" headerAlign="center" width="30">序号</div>
      <div type="expandcolumn"></div>
      <div field="setNo" width="70" allowSort="false" headerAlign="center" align="center">套号</div>
      <div field="dealno" width="70" allowSort="false" headerAlign="center" align="center">交易号</div>
      <div field="product"  width="70" allowSort="false" headerAlign="center" align="center">产品代码</div>
      <div field="type" width="70" allowSort="false" headerAlign="center" align="center">产品类型</div>
      <div field="retCode" width="70" allowSort="false" headerAlign="center" align="center">核心返回码</div>
      <div field="effDate" width="70" allowSort="false"  align="center" headerAlign="center" renderer="onDateRenderer" >生效日期</div>
      <div field="postDate"  width="70"  allowSort="false" align="center" headerAlign="center" renderer="onDateRenderer" >过账日期</div>
      <div field="cmne" width="70" allowSort="false" headerAlign="center" align="center">交易对手</div>
      <!--  <div field="voucher" width="120" allowSort="false" headerAlign="center" align="center">核心流水</div> -->
    </div>
  </div>
  <div id="accDetail" class="mini-datagrid borderAll" style="display: none;" showpager='false' allowAlternating='true'
 idField="id"   allowCellEdit="true" allowCellSelect="true" multiSelect="true"  editNextOnEnterKey="true"  editNextRowCell="true" >
		<div property="columns">
			<div type="indexcolumn"></div>
			<div field="seqNo" width="50" headerAlign="center" align="center"  allowSort="false" >处理序号</div> 
			<div field="setNo" width="60" headerAlign="center" align="center"  allowSort="false" >套号</div> 
			<div name="glno"  field="glno" headerAlign="center" width="100"  allowSort="false" >GLNO
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="200" />
            </div>
            <div name="costcent"  field="costcent" headerAlign="center"  width="100"   allowSort="false" >成本中心
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="200" />
            </div>
			<div name="subbr"  field="subbr" headerAlign="center"  width="100"  allowSort="false" >记账网点
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="200" />
            </div>
            <div name="intglno"  field="intglno" headerAlign="center"  width="100"  allowSort="false" >核心科目号
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="200" />
            </div>
            <div name="subject"  field="subject" headerAlign="center"  width="100" allowSort="false"  >核心账号
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="200" />
            </div>
            <div field="drcrind" width="80" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'DebitCreditFlag'}"  allowSort="false" >借贷标志</div>                            
            <div name="amount"  field="amount" headerAlign="center"  width="100"  allowSort="false"  >金额
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="200" />
            </div>
            <div field="ccy" width="70" allowSort="false" headerAlign="center" align="center"  renderer="CommonUtil.dictRenderer" data-options="{dict:'Currency'}">币种</div>
            <div field="retCode" width="70" align="center"   headerAlign="center"  allowSort="false" >核心返回码</div>
			<div field="retMsg" width="300" align="center"   headerAlign="center"  allowSort="false" >核心返回信息</div> 
		</div>
	</div>
</div>
    
<script type="text/javascript">   
	mini.parse();

	var grid = mini.get("datagrid");
	var form=new mini.Form("#search_form");
	// 初始化
	$(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
            mini.get('postdate').setValue(sysDate);
            mini.get('effDate').setValue(sysDate);
            search(10, 0);
        });
	});
	
	//日期格式转换
	function onDateRenderer(e) {
		var value =e.value.replace("/-/g","/");
		value =new Date(value);
		if (value) return mini.formatDate(value,'yyyy-MM-dd');
	}
	
	//列表刷新
	grid.on("beforeload", function(e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex;
		var pageSize = e.data.pageSize;
		search(pageSize, pageIndex);
	});

	
	function clear(){
		var form = new mini.Form("#search_form");
		form.clear();
		search(grid.pageSize,0);
	}
            
	function query(){
		search(grid.pageSize,0);
	}
        
	//查询返回列表信息
	function search(pageSize,pageIndex){
		var form=new mini.Form("#search_form");
		var data = form.getData(true);
   		data['pageNumber'] = pageIndex+1;
   		data['pageSize'] = pageSize;
   		data['sysDate'] = sysDate;
   		data['queryFlag'] = "setnoQuery";
   		
   		var date = mini.get("postdate").getValue();
		var postDate = mini.formatDate(date,'yyyy-MM-dd');
		if(postDate.length == 0){
			mini.alert("请输入OPICS账务日期");
			return;
		}
		data['postdate'] = postDate; 
		
   		url="/IfsOpicsAcupController/ledgerQuery";
   		var params = mini.encode(data);
   		CommonUtil.ajax({
			url:url,
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
	            grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
			
	function onShowRowDetail(e){
		var grid=e.sender;
		var row=e.record;
		var td = grid.getRowDetailCellEl(row);
		var childGrid=mini.get("accDetail");
		var gridEl=childGrid.getEl();
		td.appendChild(gridEl);
		gridEl.style.display = "block";
		searchLog(10,0,row.setNo);
	}
	//查询账务明细
	function searchLog(pageSize,pageIndex,setNo){
		var data={};
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['setNo']=setNo;
		data['queryFlag'] = "setnoDetailQuery";
		var params=mini.encode(data);
		CommonUtil.ajax({
			url:'/IfsOpicsAcupController/ledgerQuery',
			data:params,
			showLoadingMsg: false,
			callback : function(data) {
				var gridDetail =mini.get("accDetail");
				gridDetail.setTotalCount(data.obj.total);
				gridDetail.setPageIndex(pageIndex);
				gridDetail.setPageSize(pageSize);
				gridDetail.setData(data.obj.rows);
			}
		});
	}
	
	//保存单套账务信息  {acupDetail:childGridData}
	function save(){
		var childGrid=mini.get("accDetail");
		var  childGridData = childGrid.getData();
		if(childGridData.length==0){
	         mini.alert("请至少选择一条要保存的账务!","提示");
	         return;
	     }
		//套号
		var  setNo = childGridData[0].setNo;
		
   		var params = mini.encode(childGridData);
   		CommonUtil.ajax({
			url:"/IfsOpicsAcupController/saveAcupDetail",
			data: params,
			callback : function(data) {
				//查询
				searchLog(10,0,setNo);
			}
		});
	}
	
   /*重发出现错误的套号 */
	function retrySend(){
		var rows = grid.getSelecteds();
    	if(rows.length > 0){ //选中行执行
    		var data=form.getData(true);
    		var setno ="";
    		var postdate ="";
    		for(var i=0;i<rows.length;i++){
    			 if (i == 0) {
    					setno =  rows[i].setNo ;
					} else {
						setno = setno + "," + rows[i].setNo ;
					}
    		}
    		var postdate = rows[0].postDate;
    		data['setno']=setno;
    		data['postdate']=postdate;
    		var params = mini.encode(data);
			mini.confirm("您确认要重发选中记录吗?","系统警告",function(value){
				if(value=="ok"){
					mini.get("retry_btn").setEnabled(false);
					CommonUtil.ajax({
						url:"/IfsOpicsAcupController/retrysendAcup",
						data: params,
						showLoadingMsg: false,
						callback : function(data) {
						mini.get("retry_btn").setEnabled(true);
						var desc = data.desc;
							mini.alert(desc);	
								search(10,0);
								
						}
					});
				}
			});
    	} else {
    		mini.alert("请选中一行执行!");
    	}
	   }
	
	

</script>
</body>
</html>