<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
<title></title>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
	<legend>往来账结果查询</legend>	
	<div>
		<div id="search_form" style="width:100%" cols="6">
			<input id="br" name="br" class="mini-combobox" labelField="true" required="true"  data="CommonUtil.serverData.dictionary.dp"  label="部门：" labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请选择部门" width="280px" />
			<input id="seetstatus" name="seetstatus" class="mini-combobox"  data="[{id:'1',text:'发送成功'},{id:'2',text:'发送失败'},{id:'3',text:'所有账务'}]" value="3" width="280px" emptyText="请选择成功状态" labelField="true"  label="查询状态：" labelStyle="text-align:right;"/>
			<span style="float: right; margin-right: 150px"> 
				<a id="search_btn" class="mini-button" style="display: none"   onclick="search(10,0)">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
			</span>
		</div>
	</div>
</fieldset> 	  
<div class="mini-fit" >
	<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id"  allowAlternating="true"
			onshowrowdetail="onShowRowDetail" allowResize="true" sortMode="client" allowAlternating="true">
		<div property="columns">
			<div type="indexcolumn" width="30px" headerAlign="center">序号</div>
			<div type="expandcolumn"></div>
			<div field="dealNo" width="180px" align="center"  headerAlign="center" >交易号</div>    
			<div field="cNo" width="80px" align="left"  headerAlign="center" >部门</div>                            
			<div field="valdate" width="180px" align="left"  headerAlign="center" >起息日</div>                            
			<div field="ccy" width="100px" align="right"   headerAlign="center" >币种</div>
			<div field="setNo" width="120px" align="center"  headerAlign="center" allowSort="true">套号</div>
			<div name="amount" field="amount" width="150px" align="right"   headerAlign="center" allowSort="true">金额</div>
			<div name="errMsg" field="errMsg" width="120px" align="center"  headerAlign="center" allowSort="true">核心返回码</div>
			<div name="issuccess" field="revFlag" width="120px" align="center"  headerAlign="center" allowSort="true">成功标志</div>
			<!-- <div field="errorCode" width="150px" align="right"   headerAlign="center" allowSort="true">核心返回错误码</div> -->
		</div>
	</div>
	<div id="childGrid" class="mini-datagrid borderAll" style="display: none;" showpager="false" sortMode="client" allowAlternating="true">
		<div property="columns">
			<div field="setNo" width="50" align="left"  headerAlign="center">套号</div>
			<div field="seqNo" width="50" align="left"  headerAlign="center">序号</div>
			<div field="core_dealno" width="100"  headerAlign="center" align="center" >核心流水</div>
			<div field="cNo" width="50" headerAlign="center" align="left">部门号</div>    
			<div field="account" width="80" headerAlign="center" align="left">科目号</div>                            
			<div field="ccy" width="100" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'Currency'}">币种</div>
			<div field="valdate" width="80" headerAlign="center" align="left">生效日期</div>                            
			<div field="postDate" width="80" headerAlign="center" align="left">账务生成日期</div>                            
			<div field="cmne" width="80" headerAlign="center" align="left">英文简称</div>                            
			<div field="drcrind" width="80" headerAlign="center" align="left">借贷标志</div>                            
			<div field="amount" width="80" headerAlign="center" align="left">金额</div>                            
		</div>
	</div> 
</div>   

<script>
	mini.parse();
	var form = new mini.Form("#search_form");
	var grid=mini.get("datagrid");
	var childgrid=mini.get("childGrid");

	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});

	
	//清空
	function clear(){
		var form=new mini.Form("search_form");
		form.clear();
	}

	function search(pageSize,pageIndex){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统提示");
			return;
		}
		var data=form.getData();
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		data['seetstatus']=mini.get("seetstatus").getValue();
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:"/IfsOpicsNupdController/searchNupdList",
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	
	function statusChanged(e){
		var value=e.value;
		if(value=="1"){
			grid.hideColumn("cnretmsg");
			grid.hideColumn("errcode");
			grid.hideColumn("errmsg");
			grid.showColumn("retcode");
			grid.showColumn("retmsg");
		}else if(value=="2"){
			grid.showColumn("cnretmsg");
			grid.hideColumn("errcode");
			grid.hideColumn("errmsg");
			grid.showColumn("retcode");
			grid.showColumn("retmsg");
		}else{
			grid.hideColumn("cnretmsg");
			grid.showColumn("errcode");
			grid.showColumn("errmsg");
			grid.hideColumn("retcode");
			grid.hideColumn("retmsg");
		}
		search(grid.pageSize,grid.pageIndex);

	}

	function onShowRowDetail(e){
		var row=e.record;
		if(!row.setNo){
			mini.alert("核心记账流水为空！","系统提示");
			return;
		}
		var td = grid.getRowDetailCellEl(row);
		var gridEl=childgrid.getEl();
		td.appendChild(gridEl);
		gridEl.style.display = "block";
		searchChild(10,0,row.setNo);
	} 
	
	function searchChild(pageSize,pageIndex,setNo){
		var data={"setNo":setNo};
		var params=mini.encode(data);
		CommonUtil.ajax({
			url:'/IfsOpicsNupdController/searchNupdById',
			data:params,
			callback : function(data) {
				//console.log(data);
// 				childgrid.setTotalCount(data.obj.length);
// 		        childgrid.setPageIndex(pageIndex);
// 		        childgrid.setPageSize(pageSize);
			    childgrid.setData(data.obj);
			}
		});
	}
	
	$(document).ready(function() {
		//search(10, 0);
		grid.hideColumn("errcode");
		grid.hideColumn("errmsg");
	});
</script>
</body>
</html>
