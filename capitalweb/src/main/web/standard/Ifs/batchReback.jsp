<%@ page language="java" pageEncoding="UTF-8"%>
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/miniui/ColumnsMenu.js"></script>
<script src="<%=basePath%>/miniScript/common_mini.js" type="text/javascript"></script>

<span style="margin: 2px; display: block;height: 32px;">
	<a  id="add_btn" class="mini-button" style="display: none"   onclick="add()">手工新增</a>
	<a  id="addsl_btn" class="mini-button" style="display: none"   onclick="addsl()">添加事前审批</a>
	<!-- <a  id="copy_btn" class="mini-button" style="display: none"   onclick="copy()">拷贝新增</a> -->
	<a  id="edit_btn" class="mini-button" style="display: none"   onclick="edit()">修改</a>
	<a  id="delete_btn" class="mini-button" style="display: none"   onclick="del()">删除</a>
	<a  id="reback_btn" class="mini-button" style="display: none"   onclick="reback()">撤销</a>
	<a  id="recall_btn" class="mini-button" style="display: none"   onclick="recall()">撤回</a>
	<a  id="approve_commit_btn" class="mini-button" style="display: none"    onclick="approve()">审批</a>
	<a  id="approve_mine_commit_btn" class="mini-button" style="display: none"    onclick="commit()">提交审批</a>
	<a  id="approve_log" class="mini-button" style="display: none"     onclick="searchlog()">审批日志</a>
	<a  id="print_bk_btn" class="mini-button" style="display: none"    onclick="print()">打印</a>
	<a  id="batch_approve_btn" class="mini-button" style="display: none"    onclick="batchApprove()">批量审批</a>
	<a  id="batch_commit_btn" class="mini-button" style="display: none"    onclick="batchCommit()">批量提交</a>
	<a  id="opics_check_btn" class="mini-button" style="display: none"    onclick="opicsCheck()">要素批量更新</a>
	<a  id="export_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
	<a  id="status_btn" class="mini-button" style="display: none"   onclick="updateStatus()">同步Opics状态</a>
	<div id = "approveType" name = "approveType" class="mini-checkboxlist" style="float:right;" labelField="true"
		onvaluechanged="checkBoxValuechanged"  multiSelect="false" valueField="id"  value="mine" textField="text"
		data="[{id:'mine',text:'我发起的'},{id:'approve',text:'待审批列表'},{id:'finished',text:'已审批列表'}]" >
	</div>
</span>

<script>

/**
 * 流程审批定义值
 * 1.tableName 对应的表名称
 * 2.callFunction 对应的回调函数服务类名称
 **/
function batchConstant(prdName){
	var flowVar = {
		"外汇即期":{"tableName":"IFS_CFETSFX_SPT","callBackName":"IfsSptService","controllerUrl":"/IfsForeignController/editSpot","upDataParam":"instId,dealer,counterpartyInstId"},
		"外汇远期":{"tableName":"IFS_CFETSFX_FWD","callBackName":"IfsFwdService","controllerUrl":"/IfsForeignController/editCreditEditUpdate","upDataParam":"instId,dealer,counterpartyInstId"},
		"外汇掉期":{"tableName":"IFS_CFETSFX_SWAP","callBackName":"IfsFlowService","controllerUrl":"/IfsForeignController/editrmswap","upDataParam":"instId,dealer,counterpartyInstId"},
		"外币拆借":{"tableName":"IFS_CFETSFX_LEND","callBackName":"IfsLendService","controllerUrl":"/IfsCurrencyController/editCcyLending","upDataParam":"instId,dealer,counterpartyInstId"},
		"信用拆借":{"tableName":"IFS_CFETSRMB_IBO","callBackName":"IfsRmbIboService","controllerUrl":"/IfsCfetsrmbIboController/saveIbo","upDataParam":"borrowInst,borrowTrader,removeInst"},
		"同业借款":{"tableName":"IFS_CFETSRMB_IBO","callBackName":"IfsRmbIboService","controllerUrl":"/IfsCfetsrmbIboController/saveIbo","upDataParam":"borrowInst,borrowTrader,removeInst"},
		"同业存款":{"tableName":"IFS_APPROVERMB_DEPOSIT","callBackName":"IfsRmbDepositService","controllerUrl":"","upDataParam":""},
		"现券买卖":{"tableName":"IFS_CFETSRMB_CBT","callBackName":"IfsRmbCbtService","controllerUrl":"/IfsCfetsrmbCbtController/saveCbt","upDataParam":"buyInst,buyTrader,sellInst"},
		"债券借贷":{"tableName":"IFS_CFETSRMB_SL","callBackName":"IfsRmbSlService","controllerUrl":"/IfsCfetsrmbSlController/saveSl","upDataParam":"borrowInst,borrowTrader,lendInst"},
		"债券发行":{"tableName":"IFS_CFETSRMB_DP","callBackName":"IfsRmbDpService","controllerUrl":"/IfsCfetsrmbDpController/saveDp","upDataParam":""},
		"存单发行":{"tableName":"IFS_CFETSRMB_DP","callBackName":"IfsRmbDpService","controllerUrl":"/IfsCfetsrmbDpController/saveDp","upDataParam":""},
		"MBS":{"tableName":"IFS_CFETSRMB_CBT","callBackName":"IfsRmbCbtService","controllerUrl":"/IfsCfetsrmbCbtController/saveCbt","upDataParam":"buyInst,buyTrader,sellInst"},
		"质押式回购":{"tableName":"IFS_CFETSRMB_CR","callBackName":"IfsRmbCrService","controllerUrl":"/IfsCfetsrmbCrController/saveCr","upDataParam":"positiveInst,positiveTrader,reverseInst"},
		"买断式回购":{"tableName":"IFS_CFETSRMB_OR","callBackName":"IfsRmbOrService","controllerUrl":"/IfsCfetsrmbOrController/saveOr","upDataParam":"positiveInst,positiveTrader,reverseInst"},
		"国库定期存款":{"tableName":"IFS_CFETSRMB_CR","callBackName":"IfsRmbCrService","controllerUrl":"/IfsCfetsrmbCrController/saveCr","upDataParam":"positiveInst,positiveTrader,reverseInst"},
		"交易所回购":{"tableName":"IFS_CFETSRMB_CR","callBackName":"IfsRmbCrService","controllerUrl":"/IfsCfetsrmbCrController/saveCr","upDataParam":"positiveInst,positiveTrader,reverseInst"},
		"常备借贷便利":{"tableName":"IFS_CFETSRMB_CR","callBackName":"IfsRmbCrService","controllerUrl":"/IfsCfetsrmbCrController/saveCr","upDataParam":"positiveInst,positiveTrader,reverseInst"},
		"中期借贷便利":{"tableName":"IFS_CFETSRMB_CR","callBackName":"IfsRmbCrService","controllerUrl":"/IfsCfetsrmbCrController/saveCr","upDataParam":"positiveInst,positiveTrader,reverseInst"},
		"线下押券":{"tableName":"IFS_CFETSRMB_CR","callBackName":"IfsRmbCrService","controllerUrl":"/IfsCfetsrmbCrController/saveCr","upDataParam":"positiveInst,positiveTrader,reverseInst"},
		"利率互换":{"tableName":"IFS_CFETSRMB_IRS","callBackName":"IfsRmbIrsService","controllerUrl":"/IfsCfetsrmbIrsController/saveIrs","upDataParam":"fixedInst,fixedTrader,floatInst"},
		"债券远期":{"tableName":"IFS_CFETSRMB_BONDFWD","callBackName":"IfsRmbBondFwdService","controllerUrl":"/IfsCfetsrmbBondFwdController/saveCfetsrmbBondfwd","upDataParam":"buyInst,buyTrader,sellInst"},
		"债券借贷事前审批":{"tableName":"IFS_APPROVERMB_SLA","callBackName":"IfsApproveRmbSlService","controllerUrl":"","upDataParam":""},
		"基金申购":{"tableName":"FT_AFP","callBackName":"CFtAfpService","controllerUrl":"","upDataParam":""},
		"基金申购确认":{"tableName":"FT_AFP_CONF","callBackName":"CFtAfpConfService","controllerUrl":"","upDataParam":""},
		"基金赎回":{"tableName":"FT_RDP","callBackName":"CFtRdpService","controllerUrl":"","upDataParam":""},
		"基金赎回确认":{"tableName":"FT_RDP_CONF","callBackName":"CFtRdpConfService","controllerUrl":"","upDataParam":""},
		"基金赎回确认":{"tableName":"FT_REIN","callBackName":"CFtReinService","controllerUrl":"","upDataParam":""},
		"红利再投":{"tableName":"FT_REIN","callBackName":"CFtReinService","controllerUrl":"","upDataParam":""},
		"同业存放(外币)":{"tableName":"IFS_FX_DEPOSITIN","callBackName":"IfsCcyDepositInService","controllerUrl":"/IfsCCYDepositInController/addCcyDepositIn","upDataParam":""},
		"存放同业(外币)":{"tableName":"IFS_FX_DEPOSITOUT","callBackName":"IfsCcyDepositoutService","controllerUrl":"/IfsCCYDepositOutController/addCcyDepositout","upDataParam":""},
		"同业存放":{"tableName":"IFS_RMB_DEPOSITIN","callBackName":"ifsRmbDepositInService","controllerUrl":"/IfsRmbDepositInController/addRmbDepositIn","upDataParam":""},
		"存放同业":{"tableName":"IFS_RMB_DEPOSITOUT","callBackName":"ifsRmbDepositOutService","controllerUrl":"/IfsRmbDepositOutController/addRmbDepositOut","upDataParam":""}
	};
	return flowVar[prdName];
}


/**
 * 交易撤销函数[已废弃,由reback替代]
 *
 **/
function rebackOld(){
	var rows=grid.getSelecteds();
	if(rows.length==0){
	mini.alert("请选择一条要撤销的交易!","提示");
		return;
	}
	mini.confirm("您确认要撤销该交易吗?","系统警告",function(value){
		if (value=='ok'){
			var tableName= batchConstant(prdName)['tableName'];//获取tableName变量;
				var data=rows[0];
				data['dealTransType']="N";//交易状态撤销
				data['tableName']=tableName;
				params=mini.encode(data);
				CommonUtil.ajax( {
					url:"/IfsCurrencyController/rebackData",
					data:params,
					callback : function(data) {
						mini.alert("已成功撤销!","系统提示");
						search(10, 0);
				}
				});
			}
	});
}
/**
 * 交易单笔撤销函数
 *
 **/
function reback(){
	var rows=grid.getSelecteds();
	if(rows.length==0){
		mini.alert("请选择一条要撤销的交易!","提示");
		return;
	}
	var rebackParams=new Array();
	for (var i = 0; i < rows.length; i++) {
		rebackParams.push(rows[i].ticketId);
	}
	mini.confirm("您确认要撤销[不要]该交易吗?","系统警告",function(value){
		if (value=='ok'){
				var data=rows[0];
				CommonUtil.ajax( {
					url:"/IfsFlowController/reback",
					data:{"serial_nos":rebackParams},
					callback : function(data) {
						mini.alert("已成功撤销!","系统提示");
						search(10, 0);
				}
				});
			}
	});
}

/**
 *交易批量撤回
 **/
function recall(){
	var rows=grid.getSelecteds();
	if(rows.length==0){
		mini.alert("请选择一条要撤回的交易!","提示");
		return;
	}
	var recallParams=new Array();
	for (var i = 0; i < rows.length; i++) {
		recallParams.push(rows[i].ticketId);
	}
	mini.confirm("您确认要撤回该交易吗?","系统警告",function(value){
		if (value=='ok'){
			var data=rows[0];
			CommonUtil.ajax({
				url:"/IfsFlowController/recall",
				data:{"serial_nos":recallParams},
				loadingMsg:"批量撤回中...<br>逐笔处理中,请勿关闭刷新!",
				callback : function(data) {
					if(data.code!="RuntimeException"){
					setTimeout(function(){
					mini.alert(data.desc,"系统提示",function(){
						search(grid.pageSize,grid.pageIndex);
						});
					},1000);
				}
				}
			});
		}
	});
}


/**
 * 按钮是否可以编辑V1.0
 **/
function checkBoxValuechanged(e){
	$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
		search(10, 0);
		var approveType = mini.get("approveType").getValue();
		if (userId != 'admin') {
			if (approveType == "mine") {//我发起的
				initButton();
			} else if (approveType == "finished") {//已审批
				if (visibleBtn.add_btn) mini.get("add_btn").setVisible(false);
				if (visibleBtn.addsl_btn) mini.get("addsl_btn").setVisible(false);
				//if(visibleBtn.copy_btn) mini.get("copy_btn").setVisible(false);
				if (visibleBtn.edit_btn) mini.get("edit_btn").setVisible(false);
				if (visibleBtn.delete_btn) mini.get("delete_btn").setVisible(false);
				if (visibleBtn.approve_commit_btn) mini.get("approve_commit_btn").setVisible(false);
				if (visibleBtn.approve_mine_commit_btn) mini.get("approve_mine_commit_btn").setVisible(false);
				if (visibleBtn.approve_log) mini.get("approve_log").setVisible(true);//审批日志
				mini.get("approve_log").setEnabled(true);//审批日志高亮
				if (visibleBtn.print_bk_btn) mini.get("print_bk_btn").setVisible(true);//打印
				mini.get("print_bk_btn").setEnabled(true);//打印高亮
				/*新增批量提交、批量审批、opics要素批量校验  */
				if (visibleBtn.batch_commit_btn) mini.get("batch_commit_btn").setVisible(false);//批量提交不显示
				mini.get("batch_commit_btn").setEnabled(false);//批量提交不高亮
				if (visibleBtn.batch_approve_btn) mini.get("batch_approve_btn").setVisible(false);//批量审批不显示
				mini.get("batch_approve_btn").setEnabled(false);//批量审批不高亮
				if (visibleBtn.opics_check_btn) mini.get("opics_check_btn").setVisible(false);//opics要素批量校验 不显示
				mini.get("opics_check_btn").setEnabled(false);//opics要素批量校验 不高亮
				if (visibleBtn.reback_btn) mini.get("reback_btn").setVisible(false);//撤销 不显示
				if (visibleBtn.recall_btn) mini.get("recall_btn").setVisible(false);//撤回 不显示
				//mini.get("status_btn").setVisible(true);// 更新Opics状态按钮
			} else {//待审批
				if (visibleBtn.add_btn) mini.get("add_btn").setVisible(false);
				if (visibleBtn.addsl_btn) mini.get("addsl_btn").setVisible(false);
				//if(visibleBtn.copy_btn) mini.get("copy_btn").setVisible(false);
				if (visibleBtn.edit_btn) mini.get("edit_btn").setVisible(false);
				if (visibleBtn.delete_btn) mini.get("delete_btn").setVisible(false);
				if (visibleBtn.approve_commit_btn) mini.get("approve_commit_btn").setVisible(true);//审批
				mini.get("approve_commit_btn").setEnabled(true);//审批高亮
				if (visibleBtn.approve_mine_commit_btn) mini.get("approve_mine_commit_btn").setVisible(false);//提交审批
				if (visibleBtn.approve_log) mini.get("approve_log").setVisible(true);//审批日志
				mini.get("approve_log").setEnabled(true);//审批日志高亮
				if (visibleBtn.print_bk_btn) mini.get("print_bk_btn").setVisible(true);//打印
				mini.get("print_bk_btn").setEnabled(true);//打印高亮
				/*新增批量提交、批量审批、opics要素批量校验  */
				if (visibleBtn.batch_commit_btn) mini.get("batch_commit_btn").setVisible(false);//批量提交不显示
				mini.get("batch_commit_btn").setEnabled(false);//批量提交不高亮
				if (visibleBtn.batch_approve_btn) mini.get("batch_approve_btn").setVisible(true);//批量审批显示
				mini.get("batch_approve_btn").setEnabled(true);//批量审批高亮
				if (visibleBtn.opics_check_btn) mini.get("opics_check_btn").setVisible(false);//opics要素批量校验 不显示
				mini.get("opics_check_btn").setEnabled(false);//opics要素批量校验 不高亮
				if (visibleBtn.reback_btn) mini.get("reback_btn").setVisible(false);//撤销 不显示
				if (visibleBtn.recall_btn) mini.get("recall_btn").setVisible(false);//撤回 不显示
				//mini.get("status_btn").setVisible(true);// 更新Opics状态按钮
			}
		} else {
			if (approveType == "mine") {//我发起的
				initButton();
			} else if (approveType == "finished") {//已审批
				mini.get("add_btn").setVisible(false);
				mini.get("addsl_btn").setVisible(false);
				// mini.get("copy_btn").setVisible(false);
				mini.get("edit_btn").setVisible(false);
				mini.get("delete_btn").setVisible(false);
				mini.get("approve_commit_btn").setVisible(false);
				mini.get("approve_mine_commit_btn").setVisible(false);
				mini.get("approve_log").setVisible(true);//审批日志
				mini.get("approve_log").setEnabled(true);//审批日志高亮
				mini.get("print_bk_btn").setVisible(true);//打印
				mini.get("print_bk_btn").setEnabled(true);//打印高亮
				/*新增批量提交、批量审批、opics要素批量校验  */
				mini.get("batch_commit_btn").setVisible(false);//批量提交不显示
				mini.get("batch_commit_btn").setEnabled(false);//批量提交不高亮
				mini.get("batch_approve_btn").setVisible(false);//批量审批不显示
				mini.get("batch_approve_btn").setEnabled(false);//批量审批不高亮
				mini.get("opics_check_btn").setVisible(false);//opics要素批量校验 不显示
				mini.get("opics_check_btn").setEnabled(false);//opics要素批量校验 不高亮
				mini.get("reback_btn").setVisible(false);//撤销 不显示
				mini.get("recall_btn").setVisible(false);//撤回 不显示
				//mini.get("status_btn").setVisible(true);// 更新Opics状态按钮
			} else {//待审批
				mini.get("add_btn").setVisible(false);
				mini.get("addsl_btn").setVisible(false);
				// mini.get("copy_btn").setVisible(false);
				mini.get("edit_btn").setVisible(false);
				mini.get("delete_btn").setVisible(false);
				mini.get("approve_commit_btn").setVisible(true);//审批
				mini.get("approve_commit_btn").setEnabled(true);//审批高亮
				mini.get("approve_mine_commit_btn").setVisible(false);//提交审批
				mini.get("approve_log").setVisible(true);//审批日志
				mini.get("approve_log").setEnabled(true);//审批日志高亮
				mini.get("print_bk_btn").setVisible(true);//打印
				mini.get("print_bk_btn").setEnabled(true);//打印高亮
				/*新增批量提交、批量审批、opics要素批量校验  */
				mini.get("batch_commit_btn").setVisible(false);//批量提交不显示
				mini.get("batch_commit_btn").setEnabled(false);//批量提交不高亮
				mini.get("batch_approve_btn").setVisible(true);//批量审批显示
				mini.get("batch_approve_btn").setEnabled(true);//批量审批高亮
				mini.get("opics_check_btn").setVisible(false);//opics要素批量校验 不显示
				mini.get("opics_check_btn").setEnabled(false);//opics要素批量校验 不高亮
				mini.get("reback_btn").setVisible(false);//撤销 不显示
				mini.get("recall_btn").setVisible(false);//撤回 不显示
				//mini.get("status_btn").setVisible(true);// 更新Opics状态按钮
			}
		}
		if (prdNo == '452') {
			//mini.get("copy_btn").setVisible(false);
			//mini.get("copy_btn").setEnabled(false);
		}
	});
}

/**
 * 初始化界面
 **/
function initButton(){
	$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn){
		mini.get("export_btn").setVisible(true);
		if(userId!='admin'){
			if(visibleBtn.add_btn) mini.get("add_btn").setVisible(true);
			mini.get("add_btn").setEnabled(true);
			if(visibleBtn.addsl_btn) mini.get("addsl_btn").setVisible(true);
			mini.get("addsl_btn").setEnabled(true);
			//if(visibleBtn.copy_btn) mini.get("copy_btn").setVisible(true);
			//mini.get("copy_btn").setEnabled(true);
			if(visibleBtn.edit_btn) mini.get("edit_btn").setVisible(true);
			mini.get("edit_btn").setEnabled(true);
			if(visibleBtn.delete_btn) mini.get("delete_btn").setVisible(true);
			mini.get("delete_btn").setEnabled(true);
			if(visibleBtn.approve_commit_btn) mini.get("approve_commit_btn").setVisible(false);
			if(visibleBtn.approve_mine_commit_btn) mini.get("approve_mine_commit_btn").setVisible(true);//提交审批显示
			mini.get("approve_mine_commit_btn").setEnabled(true);//提交审批高亮
			if(visibleBtn.approve_log) mini.get("approve_log").setVisible(true);//审批日志
			mini.get("approve_log").setEnabled(true);//审批日志高亮
			if(visibleBtn.print_bk_btn) mini.get("print_bk_btn").setVisible(true);//打印
			mini.get("print_bk_btn").setEnabled(true);//打印高亮
			//新增批量提交、批量审批、opics要素批量校验
			if(visibleBtn.batch_commit_btn) mini.get("batch_commit_btn").setVisible(true);//批量提交显示
			mini.get("batch_commit_btn").setEnabled(true);//批量提交高亮
			if(visibleBtn.batch_approve_btn) mini.get("batch_approve_btn").setVisible(false);//批量审批不显示
			mini.get("batch_approve_btn").setEnabled(false);//批量审批不高亮
			if(visibleBtn.opics_check_btn) mini.get("opics_check_btn").setVisible(true);//opics要素批量校验 显示
			mini.get("opics_check_btn").setEnabled(true);//opics要素批量校验 高亮
			if(visibleBtn.reback_btn) mini.get("reback_btn").setVisible(true);//撤销 显示
			mini.get("reback_btn").setEnabled(true);//撤销  高亮
			if(visibleBtn.recall_btn) mini.get("recall_btn").setVisible(true);//撤回 显示
			mini.get("recall_btn").setEnabled(true);//撤回  高亮
		}else{
			mini.get("add_btn").setVisible(true);
			mini.get("add_btn").setEnabled(true);
			mini.get("addsl_btn").setVisible(true);
			mini.get("addsl_btn").setEnabled(true);
			//mini.get("copy_btn").setVisible(true);
			//mini.get("copy_btn").setEnabled(true);
			mini.get("edit_btn").setVisible(true);
			mini.get("edit_btn").setEnabled(true);
			mini.get("delete_btn").setVisible(true);
			mini.get("delete_btn").setEnabled(true);
			mini.get("approve_commit_btn").setVisible(false);
			mini.get("approve_mine_commit_btn").setVisible(true);//提交审批显示
			mini.get("approve_mine_commit_btn").setEnabled(true);//提交审批高亮
			mini.get("approve_log").setVisible(true);//审批日志
			mini.get("approve_log").setEnabled(true);//审批日志高亮
			mini.get("print_bk_btn").setVisible(true);//打印
			mini.get("print_bk_btn").setEnabled(true);//打印高亮
			//新增批量提交、批量审批、opics要素批量校验
			mini.get("batch_commit_btn").setVisible(true);//批量提交显示
			mini.get("batch_commit_btn").setEnabled(true);//批量提交高亮
			mini.get("batch_approve_btn").setVisible(false);//批量审批不显示
			mini.get("batch_approve_btn").setEnabled(false);//批量审批不高亮
			mini.get("opics_check_btn").setVisible(true);//opics要素批量校验 显示
			mini.get("opics_check_btn").setEnabled(true);//opics要素批量校验 高亮
			mini.get("reback_btn").setVisible(true);//撤销 显示
			mini.get("reback_btn").setEnabled(true);//撤销  高亮
			mini.get("search_btn").setVisible(true); //查询 显示
			mini.get("search_btn").setEnabled(true); //查询  高亮
			mini.get("clear_btn").setVisible(true);  //清空 显示
			mini.get("clear_btn").setEnabled(true);  //清空  高亮
			mini.get("recall_btn").setVisible(true);//撤回 显示
			mini.get("recall_btn").setEnabled(true);//撤回  高亮
		}
		//mini.get("status_btn").setVisible(mini.get("approve_log").getVisible());// 更新Opics状态按钮
		if(prdNo == '452'){
			//mini.get("copy_btn").setVisible(false);
			//mini.get("copy_btn").setEnabled(false);
		}
	});
}

/***
 * 批量审批通过
 *
 * @author zcm
 *
 */
function selectCallBack(prdName){
	return batchConstant(prdName)['callBackName'];//获取callBackName变量;;
}
/**
 * 批量要素更新方法
 **/
function selectIfsController(prdName){
		return batchConstant(prdName)['controllerUrl'];//获取controllerUrl变量;;
}
/**
 * 批量要素更新方法
 **/
function upData(prdName,data){
	var upDataParam = batchConstant(prdName)['upDataParam'].split(",");
	if(typeof upDataParam[0] !="undefined") data[upDataParam[0]] = instId;//机构编码
	if(typeof upDataParam[1] !="undefined") data[upDataParam[1]] = userId;//用户编码
	if(typeof upDataParam[2] !="undefined") data[upDataParam[2]] = data['cno'];//交易对手
	data["sponsor"]=instId;
	data["sponInst"]=userId;
	return data;
}

/***
 * 要素批量更新
 *
 * @author zcm
 *
 */
function opicsCheck(){
var rows=grid.getSelecteds();
	if(rows.length==0){
		mini.alert("请至少选择一条要检查更新的交易!","提示");
		return;
	}
	var hasOpics=0;
	for(var i=0;i<rows.length;i++){
		if(!(CommonUtil.isNull(rows[i]["port"])||CommonUtil.isNull(rows[i]["cost"])||
				CommonUtil.isNull(rows[i]["product"])||CommonUtil.isNull(rows[i]["prodType"]))){
			hasOpics=hasOpics+1;
		}
		if(i==rows.length-1 && !(prdName == "外汇即期" || prdName == "外汇远期" || prdName == "外汇掉期" ||
				prdName == "人民币期权" || prdName == "外币拆借")){
			if(hasOpics==rows.length){ // 非外汇交易需要做判断
				mini.alert("当前没有可更新的opics数据");
				return;
			}
		}
	}
	var opicsCheckParams=new Array();
	for (var i = 0; i < rows.length; i++) {
		opicsCheckParams.push(rows[i]);
	}

	mini.open({
		url: CommonUtil.baseWebPath() + "/../Common/miniOpics.jsp",
		title: "opics要素填写",
		width: 900,
		height: 500,
		onload: function () { // 弹出页面加载完成
		var iframe = this.getIFrameEl();
		var data = {"prdName":prdName};
		iframe.contentWindow.SetData(data);
		},
		ondestroy: function (action) {
			if (action == "ok") {
				var iframe = this.getIFrameEl();
				var data = iframe.contentWindow.GetData();
				data['prdName'] = prdName;
				data = mini.clone(data); //必须
				if (data) {
					var success=0;
					for(var i=0;i<rows.length;i++){
						//rows[i]["dealSource"]=data.dealSource;
						rows[i]["port"]=data.port;
						rows[i]["cost"]=data.cost;
						rows[i]["product"]=data.product;
						rows[i]["prodType"]=data.prodType;
						//将一些机构等显示的机构名称翻译成机构id包括发起人员信息
						rows[i]=upData(prdName,rows[i]);
						var controllerUrl=selectIfsController(prdName);
						//循环处理
						(function(row,url){
							CommonUtil.ajax({
								url:url,
								data:row,
								callback:function(data){
									success=success+1;
									//最后一条更新
									if(i==rows.length-1){
										setTimeout(function(){
											var fail=i-success;
											var message="检查更新成功,其中更新"+success+"条数据,跳过"+fail+"条数据";
											mini.alert(message,"提示");
											search(grid.pageSize,grid.pageIndex);
										},500);
									}
								}
							});
						})(rows[i],controllerUrl);
					}
				}
			}
		}
	});
}

/***
 * 批量审批通过
 *
 * @author zcm
 * modify by zfl for RCXQ-4868  20200618
 */
function batchCommit(){
	var rows=grid.getSelecteds();
	if(rows.length==0){
		mini.alert("请至少选择一条要提交的交易!","提示");
		return;
	}
	var messageid = mini.loading("系统正在处理...", "请稍后");
	//modify by zfl for RCXQ-4868  20200313
	/*  else if(rows.length > 11){
			mini.alert("只能选择10条进行操作!","提示");
			return;
		}
	*/
	/*****************  批量提交前验证start   *******************************/
	var times=0;
	var arr= new Array();
	var flow_type= Approve.FlowType.VerifyApproveFlow;
	for(var i=0;i<rows.length;i++){
		if(rows[i]["approveStatus"] != "3"){
			mini.hideMessageBox(messageid);
			mini.alert("审批状态不是新建的交易不能提交审批，请先检查！","提示");
			return;
		}
		if(prdNo=="446"||prdNo=="442"||prdNo=="467"||prdNo=="468"||prdNo=="469"||prdNo=="470"||prdNo=="472"){//质押式回购、买断式回购不用校验投资组合port
			if(CommonUtil.isNull(rows[i]["dealSource"])||CommonUtil.isNull(rows[i]["cost"])||CommonUtil.isNull(rows[i]["product"])||CommonUtil.isNull(rows[i]["prodType"])){
					mini.hideMessageBox(messageid);
					mini.alert("请先进行opics要素检查更新!","提示");
					arr=[];
					return;
				}
		}else if(prdNo=="425" || prdNo=="452"|| prdNo=="800"|| prdNo=="801"|| prdNo=="802"||prdName=="债券借贷事前审批"|| prdNo=="436"|| prdNo=="790") {
			if(prdName.indexOf("申购") > -1 && prdName.indexOf("申购份额确认") <= -1){
				flow_type= Approve.FlowType.FundAfpFlow;
			} else if(prdName.indexOf("申购份额确认") > -1){
				flow_type= Approve.FlowType.FundAfpConfFlow;
			}else if(prdName.indexOf("赎回") > -1 && prdName.indexOf("赎回份额确认") <= -1){
				flow_type= Approve.FlowType.FundRdpFlow;
			}else if(prdName.indexOf("赎回份额确认") > -1 ){
				flow_type= Approve.FlowType.FundRdpConfFlow;
			}else if(prdName.indexOf("现金分红") > -1 || prdName.indexOf("红利再投") > -1 ){
				flow_type= Approve.FlowType.FundReddinFlow;
			}

		    //债券发行、存单发行、同业存款、基金不校验
		}else{
			if(CommonUtil.isNull(rows[i]["dealSource"])||CommonUtil.isNull(rows[i]["port"])||CommonUtil.isNull(rows[i]["cost"])||CommonUtil.isNull(rows[i]["product"])||CommonUtil.isNull(rows[i]["prodType"])){
					mini.hideMessageBox(messageid);
					mini.alert("请先进行opics要素检查更新!","提示");
					arr=[];
					return;
				}
		}
		var map = batchCommitVerify(prdNo,rows[i]);
		if(map.flag == "false"){
			times++;
			arr.push(map.contractId);
		}
	}
	if(times>0){
		mini.hideMessageBox(messageid);
		mini.alert("成交单编号:"+arr+"必需的要素还未填完，请先维护再提交审批!","提示");
		return;
	}
	/******************  批量提交前验证end  ******************************/
	var callBack=selectCallBack(prdName);
	var batchParams=[];
	for (var i = 0; i < rows.length; i++) {
		batchParams[i]={
			"flow_type":flow_type, // 流程类型
			"serial_no":rows[i].ticketId, // 业务编号
			"product_type":prdNo, // 每个界面都有的变量
			"order_status":Approve.OrderStatus.New, // 新发起流程
			"callBackService":callBack // 回调函数
		}
	}
	mini.hideMessageBox(messageid);
	CommonUtil.ajax({
		url:"/IfsFlowController/approveBatch",
		data:{"batchParams":batchParams},
		loadingMsg:"批量提交审批中...<br>逐笔处理中,请勿关闭刷新!",
		callback : function(data) {
			if(data.code!="RuntimeException"){
				setTimeout(function(){
				mini.alert(data.desc,"系统提示",function(){
					search(grid.pageSize,grid.pageIndex);
					});
				},1000);
			}
		}
	});
}

 /**
  *批量提交前验证必填要素是否已填
  **/
 function batchCommitVerify(prdNo,row){
 var data={};
 data.flag="true";
 data.contractId=row.contractId;
 	switch (prdNo)
      	 {
      	 case "441"://利率互换
      		if(row.leftDiscountCurve==""||row.leftDiscountCurve==null||row.leftRateCode==""||row.leftRateCode==null||row.rightDiscountCurve==""||row.rightDiscountCurve==null||row.rightRateCode==""|| row.rightRateCode==null){
      			data.flag="false";
      			data.contractId = row.contractId;
      			return data;
      		}
      	   break;
      	   case "442"://买断式回购
      		if(row.reverseInst==""||row.reverseInst==null){
      			data.flag="false";
      			data.contractId = row.contractId;
      			return data;
      		}
      	   break;
      	   case "443"://现券买卖
      		if(row.sellInst==""||row.sellInst==null){
      			data.flag="false";
      			data.contractId = row.contractId;
      			return data;
      		}
      	   break;
      	   case "444"://信用拆借
      		if(row.removeInst==""||row.removeInst==null||
      			row.rateCode==""||row.rateCode==null){
      			data.flag="false";
      			data.contractId = row.contractId;
      			return data;
      		}
      	   break;
      	 	/* case "445"://债券借贷
       		if(row.removeInst==""||row.removeInst==null||
       			row.rateCode==""||row.rateCode==null){
       			data.flag="false";
       			data.contractId = row.contractId;
       			return data;
       		}
       	   break; */

      	   case "446"://质押式回购
      		if(row.reverseInst==""||row.reverseInst==null){
      			data.flag="false";
      			data.contractId = row.contractId;
      			return data;
      		}
      	   break;
   	 }
 	return data;
 }



/***
 * 批量审批通过/退回
 *
 * @author lij
 *
 */
function batchApprove(){
	var grid = mini.get("datagrid");
	var rows = grid.getSelecteds();
	if(rows.length==0){
		mini.alert("请至少选中一行要审批的数据!","提示");
		return;
	}
	mini.confirm("确认要批量审批吗？","确认",function (action) {
		if (action != "ok") {
			return;
		}
		mini.open({
				targetWindow: window,
				url: "../../Common/Flow/BatchApprove.jsp",
				title: "批量审批",
				width: 600,
				height: 400,
				onload: function () {
					var iframe = this.getIFrameEl();
					var data = {rows:rows};
					iframe.contentWindow.SetData(data);
				},
				ondestroy: function (action) {
					if(action =="close" || action == undefined){
						grid.reload();
						return;
					}
					var iframe = this.getIFrameEl();
					var batchParams = mini.clone(action);    //必须。克隆数据。
					//流程审批
					CommonUtil.ajax({
						url:"/IfsFlowController/approveBatchPass",
						data:{"batchParams":batchParams},
						loadingMsg:"批量提交审批中...<br>逐笔处理中,请勿关闭刷新!",
						callback:function(data) {
							if(data.code!="RuntimeException"){
								setTimeout(function(){
									mini.alert(data.desc,"系统提示",function(){
										grid.reload();
									});
								},200);
							}
						}
					});
				}
		});
	}); // end confirm
}

// 导出报表
function exportExcelManin(execlName,id){
	var params = mini.encode(form.getData(true));
	var contractId = ""; //成交单编号
	var approveStatus = JSON.parse(params).approveStatus; //审批状态
	var approveType = mini.get("approveType").getValue()
	var queryDate = ""; // 报表日期
	var startDate = ""; // 起始日期
	var endDate = ""; // 结束日期
	var forDate = ""; // 成交日期

	if(prdName=="外汇即期" || prdName=="外汇远期" || prdName=="外汇掉期" || prdName=="人民币期权" || prdName=="外币拆借"){
		contractId = JSON.parse(params).contractId;
		startDate = JSON.parse(params).startDate;
		endDate = JSON.parse(params).endDate;
		queryDate = startDate;
	}else if(prdName=="同业存款"|| prdName=="同业存款（线下）"){
		contractId = JSON.parse(params).ticketId;
		startDate = JSON.parse(params).startDate;
		endDate = JSON.parse(params).endDate;
		queryDate = startDate;
	}else if(prdName.includes("货币基金")|| prdName.includes("债券基金") || prdName.includes("专户基金") || prdName.includes("现金分红") || prdName.includes("红利再投")){
		contractId = JSON.parse(params).dealNo;
		forDate = JSON.parse(params).tdate;
		queryDate = forDate;
	}else{
		contractId = JSON.parse(params).contractId;
		forDate = !JSON.parse(params).forDate ? "" : JSON.parse(params).forDate;
		queryDate = forDate;
	}

	mini.confirm("您确认要导出Excel吗?","系统提示",
			function (action) {
				if (action == "ok"){
					var data = form.getData(true);
					var fields = null;
					var  fields = "<input type='hidden' id='excelName' name='excelName' value='"+execlName+"'>";
							fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
							fields += "<input type='hidden' id='queryDate' name='queryDate' value='"+queryDate+"'>";
							fields += "<input type='hidden' id='startDate' name='startDate' value='"+startDate+"'>";
							fields += "<input type='hidden' id='endDate' name='endDate' value='"+endDate+"'>";
							fields += "<input type='hidden' id='forDate' name='forDate' value='"+forDate+"'>";
							fields += "<input type='hidden' id='contractId' name='contractId' value='"+contractId+"'>";
							fields += "<input type='hidden' id='approveStatus' name='approveStatus' value='"+approveStatus+"'>";
							fields += "<input type='hidden' id='approveType' name='approveType' value='"+approveType+"'>";
							fields += "<input type='hidden' id='area' name='area' value='"+"哈巴区"+"'>";
							fields += "<input type='hidden' id='pageNumber' name='pageNumber' value='1'>";
							fields += "<input type='hidden' id='pageSize' name='pageSize' value='999'>";
					var urls = CommonUtil.pPath + "/sl/HrbReportManageController/exportExcel";
					$('<form action="'+ urls +'" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();
				}
			}
	);
}

/**
 * 更新Opics状态
 **/
function updateStatus(){
	var rows=grid.getSelecteds();
	if(rows.length==0){
			mini.alert("请至少选择一条要提交的交易!","提示");
			return;
		}
	//console.log(rows);
	var queryParams = [];
	var batchParams = [];
	for (var i = 0; i < rows.length; i++) {
		queryParams[i]={
			"fedealno":rows[i].ticketId, // 业务编号
		}
	}
	////console.log(queryParams);
	////console.log(prdName);
	CommonUtil.ajax({
		url:"/IfsUpdateStatcode/queryStatcode",
		data:{"queryParams":queryParams,
				"type":prdName},
		loadingMsg:"更新Opics状态中...<br>请勿关闭刷新!",
		callback : function(data) {
			// //console.log(data);
			if(data.code!="RuntimeException"){
				setTimeout(function(){
				mini.alert(data.desc,"系统提示",function(){
					search(grid.pageSize,grid.pageIndex);
					});
				},200);
			}
			else
			{
				setTimeout(function(){
				mini.alert(data.desc,"系统提示",function(){
					search(grid.pageSize,grid.pageIndex);
					});
				},200);
			}
		}
	});
}
</script>