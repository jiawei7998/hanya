<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
<title>GRSS报表查询</title>
</head>
<body style="width:100%;height:100%;background:white">
<div class="mini-splitter" style="width:100%;height:100%;">
        <div size="15%" showCollapseButton="false">   
            <fieldset class="mini-fieldset" style="height: 98%">
                <legend>报表目录</legend>
                    <ul id="treeReport" class="mini-tree" url="listTree.txt" style="width:100%;padding:5px;" 
                    showTreeIcon="true" textField="text" idField="id" parentField="pid" resultAsTree="false"          
                    contextMenu="#treeMenu" expandOnLoad="true" onnodeselect="onnodeselect">
                </ul>    
                <ul id="treeMenu" class="mini-contextmenu"  onbeforeopen="onBeforeOpen">        
                    <li name="add" iconCls="icon-add" onclick="onAddNode">新增节点</li>
                    <li name="remove" iconCls="icon-remove" onclick="onRemoveNode">删除节点</li>        
                </ul>
            </fieldset>
       </div>
        <div id="functionSQL" showCollapseButton="true">
                <fieldset class="mini-fieldset" style="height: 93%">
                        <legend>查询SQL语句</legend>
                        <input id="searchTable"  labelField="true" style="width:30%;" label="表名："  name="searchTable" class="mini-autocomplete"  emptyText="表名查找..." searchField="tabId" style="width:200px;" 
                        onbeforeload="onBeforeLoad" ajaxType="post" url="<%=basePath%>/sl/IfsReportController/getUserTabComments" dataField="obj" valueField="tabId" textField="tabId" onvaluechanged="onValueChanged" />
                        <a class="mini-button" style="display: none"  style="width:120px;" id="save_btn"   onclick="save">列出语句</a>
                        <a class="mini-button" style="display: none"  style="width:120px;" id="runsql_btn"   onclick="runsql">展示数据</a>
                        <textarea  id ="sqlId"  style="height: 100%;width: 100%;"  name="sqlId" class="mini-textarea" emptyText="请输入SQL"></textarea>
                </fieldset>
        </div>    
</div>
</body>
        
<script type="text/javascript">
    
    mini.parse();
    var treeReport = mini.get("treeReport");
    //添加文件或者文件夹
    function onAddNode(e) {
        var node = treeReport.getSelectedNode();
        if(node._level >1){//不允许三级结构增加
            return;
        }
        mini.prompt("请输名称：", "请输入",
            function (action, value) {
                if (action == "ok") {
                   //调用程序增加文件夹或者文件
                    alert(value);

                } 
            }
        );

     
    }

  

    //删除文件或者文件夹
    function onRemoveNode(e) {
        var node = treeReport.getSelectedNode();

    }

//选择节点事件
function onnodeselect(e){
  
  if(e.selected._level ==2 && e.selected.text.indexOf(".sql")>0){//当选择的名称结尾是.sql的时候

        //根据条件查询
        CommonUtil.ajax({
        url:"/IfsReportController/getSqlfromFile",
        data:{'path':"./Report/"+e.selected.pid+"/"+e.selected.id},
        callback:function(data){
           
            mini.get("sqlId").setValue(data.desc);
        }});
  }

}
  //运行sql
function runsql(e){
        //根据条件查询
    CommonUtil.ajax({
    url:"/IfsReportController/runSql",
    data:{'sql':mini.get("sqlId").getValue()},
    callback:function(data){
    }});
}

function onBeforeOpen(e) {
var menu = e.sender;
var node = treeReport.getSelectedNode();
if (!node) {
    e.cancel = true;
    return;
}
if (node && node.text == "Report") {
    e.cancel = true;
    //阻止浏览器默认右键菜单
    e.htmlEvent.preventDefault();
    return;
}}
function onBeforeLoad(e){
    e.contentType="application/json;charset=UTF-8";
    e.data =$.toJSON(e.data);
}
</script>