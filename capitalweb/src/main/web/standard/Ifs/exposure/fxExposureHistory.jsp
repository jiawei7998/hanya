<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>历史即期外汇敞口查询</title>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>
<body style="width: 100%; height: 100%; background: white">
<fieldset class="mini-fieldset">
	<legend>查询条件</legend>
	<div id="search_form" style="width: 100%;">
        <input id="br" name="br" class="mini-hidden mini-combobox mini-mustFill" labelField="true" label="部门：" vtype="maxLength:5" required="true"
	  emptyText="请填写部门" labelStyle="text-align:right;" width="280px" value='<%=__sessionUser.getOpicsBr()%>' data="CommonUtil.serverData.dictionary.opicsBr"/>
	 	 <input id="postdate" name="postdate" field="postdate" class="mini-datepicker" labelField="true"  value="<%=__bizDate%>" label="账务日期："
	  required="true"  labelStyle="text-align:right;" allowinput="false" labelStyle="width:100px" width="280px" onvaluechanged="search(10,0)"  />
       
        <span style="float: right; margin-right: 50px">
			<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
			<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
			<a id="export_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
		</span>
	</div>
</fieldset>
	
<div class="mini-fit" style="margin-top: 2px;">
	<div id="datagrid" class="mini-datagrid borderAll" style="width: 100%; height: 100%;" allowAlternating="true"
		allowResize="true" border="true" sortMode="client" multiSelect="true">
		<div property="columns">
			<div type="indexcolumn" headerAlign="center" width="40">序号</div>
			<div field="br" width="120" align="center" headerAlign="center" >部门</div>
			<div field="prod" width="120" align="center" headerAlign="center" >敞口类型</div>
			<div field="port" width="120" align="center" headerAlign="center" >投资组合</div>
			<div field="trad" width="120" align="center" headerAlign="center" >交易员</div>
			<div field="ccy" width="120" align="center" headerAlign="center" >币种</div>
			<div field="ccy1amt" width="120" align="right" headerAlign="center" numberFormat="#,0.0000">币种1金额</div>
			<div field="ccy2amt" width="120" align="right" headerAlign="center" numberFormat="#,0.0000">币种2金额</div>
			<div field="bidprice" width="120" align="right" headerAlign="center" numberFormat="#,0.00000000">成本汇率</div>
			<div field="rmbprice" width="120" align="right" headerAlign="center" numberFormat="#,0.00000000">市场汇率</div>
			<div field="usdamt" width="120" align="right" headerAlign="center" numberFormat="#,0.0000">折美元浮动金额</div>
		</div>
	</div>
</div>

<script>
	mini.parse();
	var url = window.location.search;
	var grid = mini.get("datagrid");
	var userId='<%=__sessionUser.getUserId()%>';
	
	grid.on("beforeload", function(e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex;
		var pageSize = e.data.pageSize;
		search(pageSize, pageIndex);
	});
		
	// 查询
	function search(pageSize,pageIndex){
		var form = new mini.Form("#search_form");
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统也提示");
			return;
		}
		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:"/IfsExposureController/searchFxExposureHistory",
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
		
	function query() {
    	search(grid.pageSize, 0);
    }
	
	function clear(){
        var form=new mini.Form("search_form");
        form.clear();
       //自贸区改造加br
        mini.get("br").setValue(opicsBr);
        search(10,0);
	}
	
	$(document).ready(function() {
		search(10, 0);
	});
	
	//导出
	function exportExcel(){
		var content = grid.getData();
		if(content.length == 0){
			mini.alert("请先查询数据");
			return;
		}
		mini.confirm("您确认要导出Excel吗?","系统提示", 
			function (action) {
				if (action == "ok"){
			 		var form = new mini.Form("#search_form");
					var data = form.getData(true);
					var fields = null;
					for(var id in data){
						fields += '<input type="hidden" id="' + id + '" name="'+ id +'" value="' + data[id] + '">';
					}
					var urls = CommonUtil.pPath + "/sl/IfsExportController/exportFxExposureHistoryExcel";                                                                                                                         
					$('<form action="'+ urls +'" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();   
				}
			}
		);
	}
	

</script>
</body>
</html>