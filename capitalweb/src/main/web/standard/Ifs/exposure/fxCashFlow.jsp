<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>外汇现金流</title>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>
<body style="width: 100%; height: 100%; background: white">
<fieldset class="mini-fieldset">
	<legend>查询条件</legend>
	<div id="search_form" style="width: 100%;">
        <input id="br" name="br" class=" mini-hidden mini-combobox mini-mustFill" labelField="true" label="部门：" vtype="maxLength:5" required="true"
	     emptyText="请填写部门" labelStyle="text-align:right;" width="280px" value='<%=__sessionUser.getOpicsBr()%>' data="CommonUtil.serverData.dictionary.opicsBr"/>
        <input id="portfolio" name="port" class="mini-combobox" labelField="true"  label="投资组合：" emptyText="请选择投资组合" 
        	labelStyle="text-align:right;" width="280px" textField="typeValue" valueField="typeId"/> 
        <input id="ccy" name="ccy" class="mini-combobox"  data="CommonUtil.serverData.dictionary.Currency" width="280px" emptyText="请选择币种" labelField="true"  label="币种：" labelStyle="text-align:right;"/>
        <input id="prod" name="prod" class="mini-combobox" labelField="true" label="产品类型：" labelStyle="text-align:right;" emptyText="请选择产品类型" data="[{text:'外汇',id:'外汇'},{text:'结售汇',id:'结售汇'}]"/>
         	
        <span style="float: right; margin-right: 50px">
			<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
			<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
			<a id="export_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
		</span>
	</div>
</fieldset>
	
<div class="mini-fit" style="margin-top: 2px;">
	<div id="datagrid" class="mini-datagrid borderAll" style="width: 100%; height: 100%;" allowAlternating="true"
		allowResize="true" border="true" sortMode="client" multiSelect="true" onshowrowdetail="onShowRowDetail">
		<div property="columns">
			<div type="indexcolumn" headerAlign="center" width="40">序号</div>
			<div type="expandcolumn"></div>
			<div field="br" width="120" align="center" headerAlign="center" >部门</div>
			<div field="prod" width="120" align="center" headerAlign="center" >产品类型</div>
			<div field="port" width="120" align="center" headerAlign="center" >投资组合</div>
			<div field="trad" width="70" headerAlign="center" align="center">交易员</div>
			<div field="vdate" width="120" align="center" headerAlign="center" renderer="onDateRenderer">起息日</div>
			<div field="ccy1" width="120" align="center" headerAlign="center" >币种1</div>
			<div field="ccy1amt" width="120" align="right" headerAlign="center" numberFormat="#,0.0000">币种1金额</div>
            <div field="ccy2" width="120" align="center" headerAlign="center" >币种2</div>
			<div field="ccy2amt" width="120" align="right" headerAlign="center" numberFormat="#,0.0000">币种2金额</div>
            	
		</div>
	</div>
	
	<div id="detailgrid" class="mini-datagrid borderAll" style="display: none;" showpager='false' allowAlternating='true'>
		<div property="columns">
			<div type="indexcolumn"></div>
			<div field="dealno" width="70" headerAlign="center" align="center">交易编号</div> 
			<div field="trad" width="70" headerAlign="center" align="center">交易员</div>
			<div field="cust" width="100" headerAlign="center" align="center">交易对手</div>
			<div field="prodcode" width="60" headerAlign="center" align="center">产品</div>
			<div field="prodtype" width="50" headerAlign="center" align="center">产品类型</div>
			<div field="cost" width="100" headerAlign="center" align="center">成本中心</div>
			<div field="port" width="100" headerAlign="center" align="center">投资组合</div>
			<div field="dealdate" width="80" headerAlign="center" align="center" renderer="onDateRenderer">交易日</div>
			<div field="vdate" width="80" headerAlign="center" align="center" renderer="onDateRenderer">起息日</div>                                       
			<div field="ccy" width="100" headerAlign="center" align="center">币种1</div>
			<div field="ccyamt" width="100" headerAlign="center" align="right" numberFormat="#,0.0000">币种1金额</div>
			<div field="ctrccy" width="100" headerAlign="center" align="center">币种2</div> 
			<div field="ctramt" width="100" headerAlign="center" align="right" numberFormat="#,0.0000">币种2金额</div>
			
		</div>
	</div>
</div>

<script>
	mini.parse();
	var url = window.location.search;
	var grid = mini.get("datagrid");
	var userId='<%=__sessionUser.getUserId()%>';
	
	grid.on("beforeload", function(e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex;
		var pageSize = e.data.pageSize;
		search(pageSize, pageIndex);
	});
	
	function onDateRenderer(e) {
		if(e.value!=null){
			var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)==null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g,'/'));
	        if (value) return mini.formatDate(value, 'yyyy-MM-dd');
		}
    }
	
	// 查询
	function search(pageSize,pageIndex){
		var form = new mini.Form("#search_form");
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统也提示");
			return;
		}
		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:"/IfsExposureController/searchFxCashFlow",
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	
	function onShowRowDetail(e){
		var grid=e.sender;
		var row=e.record;
		var td = grid.getRowDetailCellEl(row);
		var childGrid=mini.get("detailgrid");
		var gridEl=childGrid.getEl();
		td.appendChild(gridEl);
		gridEl.style.display = "block";
		searchDetail(10,0,row.br,row.vdate,row.port,row.ccy1,row.ccy2,row.trad);
	}
	
	//查询账务明细
	function searchDetail(pageSize,pageIndex,br,vdate,port,ccy,ctrccy,trad){
		var data={};
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['br']=br;
		data['vdate']=vdate.substring(0,10);
		if(port=='自营'){
			port='FXZY';
		}else if(port=='代客'){
			port='FXDK';
		}
		data['port']=port;
		data['ccy']=ccy;
		data['ctrccy']=ctrccy;
		data['trad']=trad;
		var params=mini.encode(data);
		//发送ajax请求
		CommonUtil.ajax({
			url:'/IfsExposureController/getFxCashFlowDetail',
			data:params,
			callback : function(data) {
				var grid=mini.get("detailgrid");
				grid.setData(data.obj);
			}
		});
	}
		
	function query() {
    	search(grid.pageSize, 0);
    }
	
	function clear(){
        var form=new mini.Form("search_form");
        form.clear();
        mini.get("br").setValue(opicsBr);  //自贸区改造加br
        search(10,0);
	}
	
	$(document).ready(function() {
		search(10, 0);
		queryPort();
	});
	
	//导出
	function exportExcel(){
		var content = grid.getData();
		if(content.length == 0){
			mini.alert("请先查询数据");
			return;
		}
		mini.confirm("您确认要导出Excel吗?","系统提示", 
			function (action) {
				if (action == "ok"){
			 		var form = new mini.Form("#search_form");
					var data = form.getData(true);
					var fields = null;
					for(var id in data){
						fields += '<input type="hidden" id="' + id + '" name="'+ id +'" value="' + data[id] + '">';
					}
					var urls = CommonUtil.pPath + "/sl/IfsExportController/exportFxCashFlowExcel";                                                                                                                         
					$('<form action="'+ urls +'" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();   
				}
			}
		);
	}
	//查询投资组合
    function queryPort(){
    	CommonUtil.ajax({
            url:"/IfsOpicsPortController/searchAllOpicsPort",
            data:{},
            callback:function(data){
            	var length = data.length;
            	var jsonData = new Array();
			   	for(var i=0;i<length;i++){
			   		var status = data[i].status;
			   		var tyId = data[i].portfolio;
			   		if(status=="1"){ //已同步的插入
			   			jsonData.add({'typeId':tyId,'typeValue':tyId});
			   		}
			   	};
			   	mini.get("portfolio").setData(jsonData);
            }
  		});
    }
</script>
</body>
</html>