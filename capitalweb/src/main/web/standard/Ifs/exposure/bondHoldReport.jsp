<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>
<body style="width: 100%; height: 100%; background: white">
	<fieldset class="mini-fieldset">
		<legend>债券持仓查询</legend>
		<div id="search_form" style="width: 100%;margin:5px 0 5px 0">
			<input id="br" name="br" class="mini-combobox mini-mustFill" labelField="true" label="部门：" vtype="maxLength:5" required="true"
	     emptyText="请填写部门" labelStyle="text-align:right;" width="280px" value='<%=__sessionUser.getOpicsBr()%>' data="CommonUtil.serverData.dictionary.opicsBr"/>
            <input id="secid" name="secid" class="mini-textbox" labelField="true" label="债券代码："  required="true"
                   emptyText="请填写债券代码" labelStyle="text-align:right;" width="280px"  />

            <span style="float: right; margin-left: 20px;margin-top:6px">
		        <a id="search_btn" class="mini-button"   onclick="query()">查询</a>
			    <a id="clear_btn" class="mini-button"    onclick="clear()">清空</a>
				<a id="export_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
		</span>
		</div>
	</fieldset>
	
	<div class="mini-fit" style="margin-top: 2px;"> 
	<div id="grid1" class="mini-datagrid borderAll" style="width: 100%; height: 100%;" allowAlternating="true"
		allowResize="true" border="true" sortMode="client" multiSelect="true" onrowdblclick="onRowDblClick" >
		<div property="columns">
		      	  <div type="indexcolumn" width="50px" headerAlign="center">序号</div>
		      	  <div field="br" width="50px" headerAlign="center">机构</div>
		      	  <div field="port" width="120px" headerAlign="center">部门</div>
		      	  <div field="acctngtype" width="100px" headerAlign="center" align="center">债券类型</div>
			      <div field="secid" width="110" headerAlign="center" align="center"allowSort="true">债券代码</div>
                  <div field="descr" width="200" headerAlign="center" align="left" allowSort="true">债券名称</div>
                  <div field="invtype" width="200" allowSort="false" align="left" headerAlign="center" >账户分类</div>
                  <div field="cmne" width="100" headerAlign="center" align="center" allowSort="true">发行人</div>
                  <div field="tenor" width="100" headerAlign="center" align="center" allowSort="true" numberFormat="#,0.00">剩余年限</div>
                  <div field="intday" width="90" headerAlign="center"align="center" align="center" >付息剩余天数 </div>
                  <div field="intpaycycle" width="100" headerAlign="center"align="center" >付息方式 </div> 
                  <div field="ipaydate" width="120" headerAlign="center" align="center" renderer='onDateRenderer'>下个付息日</div> 
                  <div field="years" width="80" headerAlign="center" align="center" >年限</div> 
                  <div field="rating" width="100" headerAlign="center"align="center" >债券信用级别</div> 
                  <div field="weightedclass" width="80" headerAlign="center" align="center" >含权类</div>
                  <div field="exercisedate" width="80" headerAlign="center"align="center" >行权日</div> 
                  <div field="exerciseyears" width="120" headerAlign="center" align="center">行权剩余年限</div> 
                  <div field="vdate" width="100" headerAlign="center" align="center" renderer='onDateRenderer'>起息日</div> 
                  <div field="mdate" width="100" headerAlign="center"align="center" renderer='onDateRenderer'>到期日</div> 
                  <div field="ratecode" width="80" headerAlign="center" align="center">利率方式</div>
                  <div field="subjectrating" width="120" headerAlign="center"align="center" >债券主体评级</div> 
                  <div field="couprate_8" width="80" headerAlign="center" align="center" numberFormat="#,0.00">票面利率(%)</div> 
                  <div field="prinamt" width="120" headerAlign="center" align="right" numberFormat="#,0.00"> 持仓面额(元)</div> 
                  <div field="pledgedfaceamt" width="100" headerAlign="center"align="center" numberFormat="#,0.00">已质押面额</div> 
                  <div field="fullcost" width="100" headerAlign="center" align="center" numberFormat="#,0.0000">全价成本(元)</div> 
                  <div field="netcost" width="100" headerAlign="center" align="center" numberFormat="#,0.0000">净价成本(元)</div> 
                  <div field="unamortamt" width="120" headerAlign="center" align="center" numberFormat="#,0.00">公允价值调整</div>
                  <div field="settavgcost" width="120" headerAlign="center" align="center" numberFormat="#,0.0000">账面余额(元)</div>
                  <div field="accr_intamt" width="120" headerAlign="center" align="center" numberFormat="#,0.00">应收利息</div>
                  <div field="amt" width="120" headerAlign="center" align="center" numberFormat="#,0.0000">本金</div>
                  <div field="unitcost" width="120" headerAlign="center" align="center" numberFormat="#,0.0000">百元净价成本</div>
                  <div field="netpricecost" width="120" headerAlign="center" align="center" numberFormat="#,0.0000">百元全价成本</div>
                  <div field="tclsgprice_8" width="120" headerAlign="center" align="center" >到期收益率</div>
                  <div field="sclsgprice_8" width="120" headerAlign="center" align="center" numberFormat="#,0.0000">百元净价估值</div>
                  <div field="sclsgprice2_8" width="120" headerAlign="center" align="center" numberFormat="#,0.0000">百元全价估值</div>
                  <div field="diff_amt" width="120" headerAlign="center" align="center" numberFormat="#,0.0000">净价浮盈(元)</div>
                  <div field="floatamt" width="120" headerAlign="center" align="center" >浮动盈亏比</div>                
		</div>
	</div>
</div>
	
<script>
	mini.parse();
	var grid = mini.get("grid1");
	grid.on("beforeload", function(e) {
			e.cancel = true;
			var pageIndex = e.data.pageIndex;
			var pageSize = e.data.pageSize;
			search(pageSize, pageIndex);
		});
	$(document).ready(function() {
		query();
	});
	
	//日期格式转换
	function onDateRenderer(e) {
		var value =e.value.replace("/-/g","/");
		value =new Date(value);
		if (value) return mini.formatDate(value,'yyyyMMdd');
	}
	
	
	// 初始化数据
	function query(){
		search(grid.pageSize, 0);
	}
	function search(pageSize,pageIndex){
		var form = new mini.Form("#search_form");
		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		var url="/IfsBondHoldController/searchBondHoldCount";
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:url,
			data:params,
			callback : function(data) {
				if(data){
					grid.setTotalCount(data.obj.total);
					grid.setPageIndex(pageIndex);
			        grid.setPageSize(pageSize);
					grid.setData(data.obj.rows);
				}
			}
		});
	}
	
	function clear(){
        var form=new mini.Form("search_form");
        form.clear();
        mini.get("br").setValue(opicsBr);//自贸区改造加br
        query();
	}
		
	//导出
	function exportExcel(){
		var content = grid.getData();
		if(content.length == 0){
			mini.alert("请先查询数据");
			return;
		}
		mini.confirm("您确认要导出Excel吗?","系统提示", 
			function (action) {
				if (action == "ok"){
					var form = new mini.Form("#search_form");
					var data=form.getData(true);
					var fields = null;
					for(var id in data){
						fields += '<input type="hidden" id="' + id + '" name="'+ id +'" value="' + data[id] + '">';
					}
					var urls = CommonUtil.pPath + "/sl/IfsBondExportController/exportBondHoldReport";                                                                                                                         
					$('<form action="'+ urls +'" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();   
				}
			}
		);
	}
    function GetData() {
        var grid = mini.get("grid1");
        var row = grid.getSelected();
        return row;
    }


    //双击行选择
    function onRowDblClick(){
        var row = grid.getSelected();
        onOk();

    }

    function CloseWindow(action) {
        if (window.CloseOwnerWindow)
            return window.CloseOwnerWindow(action);
        else
            window.close();
    }

    function onOk() {
        CloseWindow("ok");
    }

    //关闭窗口
    function onCancel() {
        CloseWindow("cancel");
    }





</script>
</body>
</html>