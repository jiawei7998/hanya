<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>
<body style="width: 100%; height: 100%; background: white">
	<fieldset class="mini-fieldset"> 
		<legend>债券交易查询</legend>
		<div id="search_form" style="width: 100%;margin:5px 0 5px 0">
			<input id="br" name="br" class="mini-combobox mini-mustFill" labelField="true" label="部门：" vtype="maxLength:5" required="true"
	     emptyText="请填写部门" labelStyle="text-align:right;" width="280px" value='<%=__sessionUser.getOpicsBr()%>' data="CommonUtil.serverData.dictionary.opicsBr"/>
	     	<input id="dealno" name="dealno" class="mini-textbox" labelField="true" label="交易单号：" emptyText="请填写交易单号" labelStyle="text-align:right;" width="280px"/>
	     	<input id="startDate" name="startDate" class="mini-datepicker" labelField="true" label="交易日期："
						ondrawdate="onDrawDateStart"  onvaluechanged='dateCalculation' labelStyle="text-align:right;" emptyText="起始日期" format="yyyy-MM-dd" value="<%=__bizDate%>"/>
			<span>~</span>
			<input id="endDate" name="endDate" class="mini-datepicker" 
						ondrawdate="onDrawDateEnd"  onvaluechanged='dateCalculation'  emptyText="结束日期" format="yyyy-MM-dd" value="<%=__bizDate%>"/>
		<span style="float: right; margin-left: 20px;margin-top:6px">
		        <a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
			    <a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
				<a id="export_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
		</span>
		</div>
	</fieldset>
	
	<div class="mini-fit" style="margin-top: 2px;">
	<div id="grid1" class="mini-datagrid borderAll" style="width: 100%; height: 100%;" allowAlternating="true"
		allowResize="true" border="true" sortMode="client" multiSelect="true">
		<div property="columns">
		      	  <div type="indexcolumn" width="50px" headerAlign="center">序号</div>
		      	  <div field="dealno" width="100px" headerAlign="center">交易单号</div>
			      <div field="status" width="100" headerAlign="center" align="center"allowSort="true">状态</div>
                  <div field="cmne" width="100" headerAlign="center" align="center" allowSort="true">交易对手</div>
                  <div field="seq" width="150" allowSort="false" align="center" headerAlign="center" >交易序号</div>
                  <div field="invtype" width="100" headerAlign="center" align="center" allowSort="true" >账户</div>
                  <div field="dealdate" width="100" headerAlign="center" align="center" allowSort="true"  renderer='onDateRenderer'>交易日</div>
                  <div field="acctngtype" width="80" headerAlign="center"align="center" align="center" >债券类型 </div>
                  <div field="market" width="100" headerAlign="center"align="center" >市场 </div> 
                  <div field="secid" width="120" headerAlign="center" align="center" >代码</div> 
                  <div field="descr" width="150" headerAlign="center" align="center" >名称</div> 
                  <div field="ps" width="80" headerAlign="center"align="center" >方向</div> 
                  <div field="method" width="80" headerAlign="center" align="center" >清算方式</div>
                  <div field="dvp" width="80" headerAlign="center"align="center" >结算方式</div> 
                  <div field="price" width="120" headerAlign="center" align="center"  numberFormat="#,0.0000">净价(元)</div> 
                  <div field="settdate" width="120" headerAlign="center" align="center" renderer='onDateRenderer'>结算日</div> 
                  <div field="yield_8" width="120" headerAlign="center"align="center" numberFormat="#,0.00" >收益率(%)</div> 
                  <div field="price_8" width="80" headerAlign="center" align="center" numberFormat="#,0.0000">全价(元)</div>
                  <div field="faceamt" width="120" headerAlign="center"align="center" renderer='TypeChange' numberFormat="#,0">成交面额(万元)</div> 
                  <div field="settamt" width="120" headerAlign="center" align="center"  numberFormat="#,0.00">成交金额(元)</div> 
                  <div field="cashsettamt" width="120" headerAlign="center" align="right" numberFormat="#,0.00" >现金流(元)</div> 
                  <div field="costamt" width="100" headerAlign="center"align="center" numberFormat="#,0">净价金额(元)</div> 
                  <div field="purchintamt" width="100" headerAlign="center" align="center" numberFormat="#,0">利息(元)</div> 
                  <div field="trad" width="80" headerAlign="center" align="center">发起人</div> 
                  <div field="dealtext" width="120" headerAlign="center" align="center">备注</div> 
		</div>
	</div>
</div>
	
<script>
	mini.parse();
	var grid = mini.get("grid1");
	grid.on("beforeload", function(e) {
			e.cancel = true;
			var pageIndex = e.data.pageIndex;
			var pageSize = e.data.pageSize;
			search(pageSize, pageIndex);
		});
	$(document).ready(function() {
		query();
	});
	//日期格式转换
	function onDateRenderer(e) {
		var value =e.value.replace("/-/g","/");
		value =new Date(value);
		if (value) return mini.formatDate(value,'yyyy-MM-dd');
	}
	//日期格式转换
	function onDateRenderer(e) {
		var value =e.value.replace("/-/g","/");
		value =new Date(value);
		if (value) return mini.formatDate(value,'yyyy-MM-dd');
	}
	//amount类型转换
	function TypeChange(e){
		var num=parseFloat(e.value);
		return  num/10000;
	}
	// 初始化数据
	function query(){
		search(grid.pageSize, 0);
	}
	function search(pageSize,pageIndex){
		var form = new mini.Form("#search_form");
		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		var url="/IfsBondTradeController/searchBondTradeCount";
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:url,
			data:params,
			callback : function(data) {
				if(data){
					grid.setTotalCount(data.obj.total);
					grid.setPageIndex(pageIndex);
			        grid.setPageSize(pageSize);
					grid.setData(data.obj.rows);
				}
			}
		});
	}
	
	function clear(){
        var form=new mini.Form("search_form");
        form.clear();
        mini.get("br").setValue(opicsBr);//自贸区改造加br
        mini.get("startDate").setValue(sysDate);
        mini.get("endDate").setValue(sysDate);
        query();
	}
	

	//日期
	function onDrawDateStart(e) {
        var startDate = e.date;
        var endDate= mini.get("endDate").getValue();
        if(CommonUtil.isNull(endDate)){
        	return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }
	
	function onDrawDateEnd(e) {
        var endDate = e.date;
        var startDate = mini.get("startDate").getValue();
        if(CommonUtil.isNull(startDate)){
        	return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }
	//计算时间段长度
	function dateCalculation(){
		var start = mini.get("startDate").getValue();
		var end= mini.get("endDate").getValue();
		if(CommonUtil.isNull(start)||CommonUtil.isNull(end)){
			return;
		}
		var subDate= Math.abs(parseInt((end.getTime() - start.getTime())/1000/3600/24));
		if(subDate>30){
			mini.alert('查询时间大于一个月，请重新选择');
			return;
		}
	}
	
	//导出
	function exportExcel(){
		var content = grid.getData();
		if(content.length == 0){
			mini.alert("请先查询数据");
			return;
		}
		mini.confirm("您确认要导出Excel吗?","系统提示", 
			function (action) {
				if (action == "ok"){
					var form = new mini.Form("#search_form");
					var data=form.getData(true);
					var fields = null;
					for(var id in data){
						fields += '<input type="hidden" id="' + id + '" name="'+ id +'" value="' + data[id] + '">';
					}
					var urls = CommonUtil.pPath + "/sl/IfsBondExportController/exportBondTradeReport";                                                                                                                         
					$('<form action="'+ urls +'" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();   
				}
			}
		);
	}
</script>
</body>
</html>