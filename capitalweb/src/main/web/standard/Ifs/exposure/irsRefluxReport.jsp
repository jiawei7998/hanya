<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>回流文件查询</title>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>
<body style="width: 100%; height: 100%; background: white">
<fieldset class="mini-fieldset">
	<legend>查询条件</legend>
	<div id="search_form" style="width: 100%;">
	 	 <input id="postdate" name="postdate" field="postdate" class="mini-datepicker" labelField="true"  value="<%=__bizDate%>" label="日期："
	  required="true"  labelStyle="text-align:center;" allowinput="false" labelStyle="width:100px" width="280px" onvaluechanged="search(10,0)"  />
       
        <span style="float: center; margin-center: 50px">
			<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
			<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
			<a id="export_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
		</span>
	</div>
</fieldset>
	
<div class="mini-fit" style="margin-top: 2px;">
	<div id="datagrid" class="mini-datagrid borderAll" style="width: 100%; height: 100%;" allowAlternating="true"
		allowResize="true" border="true" sortMode="client" multiSelect="true">
		<div property="columns">
			<div type="indexcolumn" headerAlign="center" width="40">序号</div>
		
			<div field="data_dt" width="150" align="center" headerAlign="center" >数据日期</div>
			<div field="batch_id" width="120" align="center" headerAlign="center" >批次号</div>
			<div field="br" width="60" align="center" headerAlign="center" >部门</div>
			<div field="secid" width="120" align="center" headerAlign="center" >债券号</div>
			<div field="irs9_cost" width="120" align="center" headerAlign="center" >投资组合1</div>
			<div field="invtype" width="60" align="center" headerAlign="center" >交易类</div>
			<div field="port" width="80" align="center" headerAlign="center" >投资组合2</div>
			<div field="asset_no" width="250" align="center" headerAlign="center" >资产编号</div>
			<div field="biz_type_cd" width="120" align="center" headerAlign="center" >业务条线代码</div>
			<div field="product_level_one_cd" width="120" align="center" headerAlign="center" >产品大类代码</div>
			<div field="busi_type_cd" width="120" align="center" headerAlign="center" >业务产品类型代码</div>
			<div field="stage_rslt_sys" width="120" align="center" headerAlign="center" >系统判断阶段结果</div>
			<div field="stage_rslt_affirm" width="120" align="center" headerAlign="center" >人工认定阶段结果</div>
			<div field="stage_rslt_final" width="120" align="center" headerAlign="center" >最终阶段判断结果</div>
			<div field="pd" width="120" align="center" headerAlign="center" >违约概率</div>
			<div field="pd_pre_adj" width="150" align="center" headerAlign="center" >前瞻性调整后违约概率</div>
			<div field="lgd" width="120" align="center" headerAlign="center" numberFormat="#,0.0000">违约损失率</div>
			<div field="ead" width="120" align="center" headerAlign="center" >违约风险暴露</div>
			<div field="ecl_prin" width="120" align="center" headerAlign="center" >本金计提金额</div>
			<div field="ecl_int" width="120" align="center" headerAlign="center" >利息计提金额</div>
			<div field="ecl_out" width="120" align="center" headerAlign="center" >表外计提金额</div>
			<div field="ecl_add" width="120" align="center" headerAlign="center" >增提金额</div>
			<div field="ecl_add_ratio" width="120" align="center" headerAlign="center" >增提比例</div>
			<div field="ecl_sys" width="120" align="center" headerAlign="center" >系统计算计提金额</div>
			<div field="ecl_affirm" width="120" align="center" headerAlign="center" >人工认定计提金额</div>
			<div field="ecl_bf_add" width="120" align="center" headerAlign="center" >增提前计提金额</div>
			<div field="ecl_final" width="120" align="center" headerAlign="center" >最终结果计提金额</div>
			<div field="asset_bal" width="120" align="center" headerAlign="center" >资产余额</div>
			<div field="currency_cd" width="120" align="center" headerAlign="center" >币种代码</div>
			<div field="customer_no" width="120" align="center" headerAlign="center" >客户编号</div>
			<div field="customer_name" width="120" align="center" headerAlign="center" >客户名称</div>
			<div field="rating_level_cd" width="120" align="center" headerAlign="center" >信用等级代码</div>
			<div field="apply_rating_level_cd" width="140" align="center" headerAlign="center" >申请时点信用等级代码</div>
			<div field="contract_no" width="120" align="center" headerAlign="center" >合同编号</div>
			<div field="org_no" width="120" align="center" headerAlign="center" >业务所在机构编号</div>
			<div field="org_name" width="120" align="center" headerAlign="center" >业务所在机构名称</div>
			<div field="is_forward_adj" width="120" align="center" headerAlign="center" >是否前瞻性调整</div>
			<div field="putout_dt" width="140" align="center" headerAlign="center" >发放日期</div>
			<div field="maturity" width="120" align="center" headerAlign="center" >到期日期</div>
			<div field="overdue_days" width="100" align="center" headerAlign="center" >逾期天数</div>
			<div field="loan_cls_cd" width="120" align="center" headerAlign="center" >资产分类结果代码</div>
			<div field="acct_subject_no" width="120" align="center" headerAlign="center" >会计科目编号</div>
			<div field="asset_three_class_cd" width="120" align="center" headerAlign="center" >金融资产三分类</div>
			<div field="overdue_status_cd" width="120" align="center" headerAlign="center" >逾期状态代码</div>
			<div field="customer_size_cd" width="120" align="center" headerAlign="center" >企业规模代码</div>
			<div field="industry_cd" width="120" align="center" headerAlign="center" >国标行业类型代码</div>
			<div field="industry_level_one_cd" width="120" align="center" headerAlign="center" >国标行业门类代码</div>
			<div field="industry_level_two_cd" width="120" align="center" headerAlign="center" >国标行业大类代码</div>
			<div field="industry_level_three_cd" width="120" align="center" headerAlign="center" >国标行业中类代码</div>
			<div field="exec_int_rate" width="120" align="center" headerAlign="center" >执行利率</div>
			<div field="src_sys" width="120" align="center" headerAlign="center" >来源系统</div>
			<div field="create_user" width="120" align="center" headerAlign="center" >创建人</div>
			<div field="create_time" width="120" align="center" headerAlign="center" >创建时间</div>
			<div field="update_user" width="120" align="center" headerAlign="center" >更新人</div>
			<div field="update_time" width="120" align="center" headerAlign="center" >更新时间</div>
			<div field="tenant_id" width="120" align="center" headerAlign="center" >多实体标识</div>
			<div field="irs9_version" width="120" align="center" headerAlign="center" >框架版本号</div>
		</div>
	</div>
</div>

<script>
	mini.parse();
	var url = window.location.search;
	var grid = mini.get("datagrid");
	var userId='<%=__sessionUser.getUserId()%>';
	
	grid.on("beforeload", function(e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex;
		var pageSize = e.data.pageSize;
		search(pageSize, pageIndex);
	});
		
	// 查询
	function search(pageSize,pageIndex){
		var form = new mini.Form("#search_form");
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统也提示");
			return;
		}
		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:"/IrsRefluxController/searchRefluxFileCount",
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
		
	function query() {
    	search(grid.pageSize, 0);
    }
	
	function clear(){
        var form=new mini.Form("search_form");
        form.clear();
       //自贸区改造加br
        mini.get("br").setValue(opicsBr);
        search(10,0);
	}
	
	$(document).ready(function() {
		search(10, 0);
	});
	
	//导出
	function exportExcel(){
		var content = grid.getData();
		if(content.length == 0){
			mini.alert("请先查询数据");
			return;
		}
		mini.confirm("您确认要导出Excel吗?","系统提示", 
			function (action) {
				if (action == "ok"){
			 		var form = new mini.Form("#search_form");
					var data = form.getData(true);
					var fields = null;
					for(var id in data){
						fields += '<input type="hidden" id="' + id + '" name="'+ id +'" value="' + data[id] + '">';
					}
					var urls = CommonUtil.pPath + "/sl/RefluxFileExportController/exportRefluxFileReport";                                                                                                                         
					$('<form action="'+ urls +'" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();   
				}
			}
		);
	}
	

</script>
</body>
</html>