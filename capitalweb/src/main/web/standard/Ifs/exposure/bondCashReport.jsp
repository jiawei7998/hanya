<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>
<body style="width: 100%; height: 100%; background: white">
	<fieldset class="mini-fieldset">
		<legend>债券头寸查询</legend>
		<div id="search_form" style="width: 100%;margin:5px 0 5px 0">
			<input id="br" name="br" class="mini-combobox mini-mustFill" labelField="true" label="部门：" vtype="maxLength:5" required="true"
	     emptyText="请填写部门" labelStyle="text-align:right;" width="280px" value='<%=__sessionUser.getOpicsBr()%>' data="CommonUtil.serverData.dictionary.opicsBr"/>
			<!-- <nobr>
			<input id="startDate" name="startDate" class="mini-datepicker" labelField="true" label="起息日期："
						ondrawdate="onDrawDateStart"  onvaluechanged='dateCalculation' labelStyle="text-align:right;" emptyText="起始日期" format="yyyy-MM-dd" value="<%=__bizDate%>"/>
			<span>~</span>
			<input id="endDate" name="endDate" class="mini-datepicker" 
						ondrawdate="onDrawDateEnd"  onvaluechanged='dateCalculation'  emptyText="结束日期" format="yyyy-MM-dd" value="<%=__bizDate%>"/>
			</nobr>
			 -->
		<span style="float: right; margin-left: 20px;margin-top:6px">
		        <a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
			    <a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
				<a id="export_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
		</span>
		</div>
	</fieldset>
	
	<div class="mini-fit" style="margin-top: 2px;">
	<div id="grid1" class="mini-datagrid borderAll" style="width: 100%; height: 100%;" allowAlternating="true"
		allowResize="true" border="true" sortMode="client" multiSelect="true">
		<div property="columns">
		      	  <div type="indexcolumn" width="50px" headerAlign="center">序号</div>
			      <div field="secid" width="100" headerAlign="center" align="center"allowSort="true">债券ISIN</div>
                  <div field="cost" width="100" headerAlign="center" align="center" allowSort="true">成本中心</div>
                  <div field="port" width="80" allowSort="false" align="center" headerAlign="center" >投资组合</div>
                  <div field="invtype" width="60" headerAlign="center" align="center" allowSort="true" >资产类型</div>
                  <div field="ipaydate" width="80" headerAlign="center" align="center" allowSort="true"  renderer='onDateRenderer'>收款日</div>
                  <div field="paramt" width="80" headerAlign="center"align="center" align="center" >票面价值</div>
                  <div field="amortamt" width="120" headerAlign="center"align="center" renderer='TypeChange' >成本</div> 
                  <div field="tdyintamt" width="120" headerAlign="center" align="center" >计提</div> 
                  <div field="accrintamt" width="120" headerAlign="center" align="center" >截止上个付息日利息</div> 
                  <div field="unamortamt" width="120" headerAlign="center"align="center"  renderer='TypeChange' >未摊销金额</div> 
                  <div field="unitcost" width="80" headerAlign="center" align="center"  renderer='TypeChange' >成本价</div>
                  <div field="clsgprice" width="80" headerAlign="center"align="center" >收盘价</div> 
                  <div field="facevalue" width="120" headerAlign="center" align="center">面值</div> 
                  <div field="fairvalue" width="120" headerAlign="center" align="center" >市场价值</div> 
                  <div field="diffamt" width="120" headerAlign="center"align="center" >估值</div> 
                  <div field="vdate" width="80" headerAlign="center" align="center" renderer='onDateRenderer' >起息日</div>
                  <div field="mdate" width="80" headerAlign="center"align="center" renderer='onDateRenderer' >到期日</div> 
                  <div field="couprate" width="80" headerAlign="center" align="center" >票面利率</div> 
                  <div field="yearstomaturity" width="80" headerAlign="center" align="right" numberFormat="#,0.0000" >到期收益率</div> 
                  <div field="ratetype" width="80" headerAlign="center"align="center">计息规则</div> 
                  <div field="intpaycycle" width="80" headerAlign="center" align="center">付息频率</div> -->
		</div>
	</div>
</div>
	
<script>
	mini.parse();
	//获取当前tab renderer='TypeChange'
	var grid = mini.get("grid1");
	grid.on("beforeload", function(e) {
			e.cancel = true;
			var pageIndex = e.data.pageIndex;
			var pageSize = e.data.pageSize;
			search(pageSize, pageIndex);
		});
	$(document).ready(function() {
		//initDate();
		query();
	});
	//日期格式转换
	function onDateRenderer(e) {
		var value =e.value.replace("/-/g","/");
		value =new Date(value);
		if (value) return mini.formatDate(value,'yyyy-MM-dd');
	}
	//amount类型转换
	function TypeChange(e){
		var num=parseFloat(e.value);
		return  num.toFixed(2);
	}
	// 初始化数据
	function query(){
		search(grid.pageSize, 0);
	}
	function search(pageSize,pageIndex){
		var form = new mini.Form("#search_form");
		var data=form.getData(true);
		//dateCalculation();
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		var url="/IfsExposureController/searchBondCashCount";
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:url,
			data:params,
			callback : function(data) {
				if(data){
					grid.setTotalCount(data.obj.total);
					grid.setPageIndex(pageIndex);
			        grid.setPageSize(pageSize);
					grid.setData(data.obj.rows);
				}
			}
		});
	}
	
	function clear(){
        var form=new mini.Form("search_form");
        form.clear();
        mini.get("br").setValue(opicsBr);  //自贸区改造加br
       // initDate();
        query();
	}
	function initDate(){
		var date=new Date(sysDate.replace(/-/g,'/'));
	    var year=date.getFullYear();
	    var month=date.getMonth()+1;
	    var start=new Array();
	    start.push(year);
	    start.push(month);
	    start.push("1");
	    mini.get("startDate").setValue(start.join("-"));
	    mini.get("endDate").setValue(getLastDay(year,month));
	}
	//获得每月最后一天
	 function getLastDay(year,month){         
         var new_year = year;    //取当前的年份
         var new_month = month++;//取下一个月的第一天，方便计算（最后一天不固定）
         if(month>12) {         
          new_month -=12;        //月份减
          new_year++;            //年份增
         }         
         var new_date = new Date(new_year,new_month,1);                //取当年当月中的第一天
         return (new Date(new_date.getTime()-1000*60*60*24));//获取当月最后一天日期
    } ;

	//日期
	function onDrawDateStart(e) {
        var startDate = e.date;
        var endDate= mini.get("endDate").getValue();
        if(CommonUtil.isNull(endDate)){
        	return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }
	
	function onDrawDateEnd(e) {
        var endDate = e.date;
        var startDate = mini.get("startDate").getValue();
        if(CommonUtil.isNull(startDate)){
        	return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }
	//计算时间段长度
	function dateCalculation(){
		var start = mini.get("startDate").getValue();
		var end= mini.get("endDate").getValue();
		if(CommonUtil.isNull(start)||CommonUtil.isNull(end)){
			return;
		}
		var subDate= Math.abs(parseInt((end.getTime() - start.getTime())/1000/3600/24));
		if(subDate>30){
			mini.alert('查询时间大于一个月，请重新选择');
			return;
		}
	}
	
	//导出
	function exportExcel(){
		var content = grid.getData();
		if(content.length == 0){
			mini.alert("请先查询数据");
			return;
		}
		mini.confirm("您确认要导出Excel吗?","系统提示", 
			function (action) {
				if (action == "ok"){
					var form = new mini.Form("#search_form");
					var data=form.getData(true);
					var fields = null;
					for(var id in data){
						fields += '<input type="hidden" id="' + id + '" name="'+ id +'" value="' + data[id] + '">';
					}
					var urls = CommonUtil.pPath + "/sl/IfsExportController/exportBondCashReport";                                                                                                                         
					$('<form action="'+ urls +'" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();   
				}
			}
		);
	}
</script>
</body>
</html>