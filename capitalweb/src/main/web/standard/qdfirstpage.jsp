﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="./global.jsp"%>
<head>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
    <link href="<%=basePath%>/miniScript/css/firstpage.css" rel="stylesheet" type="text/css" />
    <style>
        body
        {
            padding-left:5px; 
            height:100%;
            background:#f5f5f5;
            line-height: 30px!important;
            box-sizing: border-box;
        }
    </style>
</head>
<body>
    <div class="select_bar" style="width:100%;height:125px;padding-top:10px;">
        <table style="width:100%;">
            <tr>
                <td>
                    <div class="panel_btn a" onclick="changeBar('trade');">
                        <div class="panel_btn_lf">
                            <a class="panel_btn_text">交易待办</a><br />
                            <span id="t_num">0</span>
                        </div>
                        <div class="panel_btn_rt"></div>
                    </div>
                </td>
                <td>
                    <div class="panel_btn b" onclick="changeBar('duration');">
                        <div class="panel_btn_lf">
                            <a class="panel_btn_text">存续期待办</a><br />
                            <span id="d_num">0</span>
                        </div>
                        <div class="panel_btn_rt"></div>
                    </div>
                </td>
                <!-- 
                <td>
                    <div class="panel_btn c" onclick="changeBar('plan');">
                        <div class="panel_btn_lf">
                            <a class="panel_btn_text">来账分拣待办</a><br />
                            <span id="p_num">0</span>
                        </div>
                        <div class="panel_btn_rt"></div>
                    </div>
                </td>
                -->
                <td>
                    <div class="panel_btn d" onclick="settle();">
                        <div class="panel_btn_lf">
                            <a class="panel_btn_text">清算待办</a><br />
                            <span id="s_num">0</span>
                        </div>
                        <div class="panel_btn_rt"></div>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <div id="title" class="select_content_title">
        交易待办事项
    </div>
    <div class="mini-fit">
        <div style="height:100%;">
            <div class="mini-toolbar" width="80%">
                <table style="width:100%;">
                    <tr>
                        <td>
                            <a id="verify_commit_btn" class="mini-button" style="display: none"   >审批</a>
                            <a id="refresh_btn" class="mini-button" style="display: none"   onclick="refresh">刷新</a>
                            <a id="export" class="mini-button" style="display: none"   onclick="exportExcel">导出报表</a>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="mini-fit">
            <div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" allowCellWrap="true"
                allowAlternating="true" allowResize="true" border="true" multiSelect="false" sortMode="client" sizeList="[10,20]">
                <div property="columns">
                    <div type="indexcolumn" headerAlign="center" width="30">序号</div>
                    <div field="ticketId" width="120" headerAlign="center" align="left" allowSort="true">审批单编号</div>
                    <div field="contractId" width="120" headerAlign="center" align="left" allowSort="true">成交单编号</div>
                    <div field="taskName" width="180" allowSort="false" headerAlign="center" align="">审批状态</div>
                   <!--  <div field="approveStatus" width="100" headerAlign="center"
                  		renderer="CommonUtil.dictRenderer" data-options="{dict:'ApproveStatu'}">审批状态</div> -->
                   	<div field="prdName" width="150" headerAlign="center" >产品名称</div>
                    <div field="tradeType" width="100" headerAlign="center" align="center" allowSort="true"
                    	renderer="CommonUtil.dictRenderer" data-options="{dict:'TradeFlowType'}" >操作类型</div>
                    <div field="custName" width="200" headerAlign="center" >客户名称</div>
                    <div field="tradeDate" width="80" headerAlign="center" align="center" allowSort="true">成交日期</div>
                   <!-- <div field="prdNo" width="100" headerAlign="center" align="center">产品类型</div>
                   <div field="ticketId" width="100" headerAlign="center" visible="false">内部id</div>  -->
                    <div field="mydircn" width="100" headerAlign="center">本方方向</div>
                    <div field="settmeans" width="100" headerAlign="center">清算方式</div>
                    <div field="ccyPair" width="100" headerAlign="center">货币对</div>
                    <div field="dealPrice" width="100" headerAlign="center">价格</div>
                    <div field="occupyCustNo" width="100" headerAlign="center">授信占用主体</div>
                    <div field="weight" width="100" headerAlign="center">权重</div>
                    <div field="mydealer" width="100" headerAlign="center">本方交易员</div>
                    <div field="dealPort" width="100" headerAlign="center">投资组合</div>
                    <div field="myInstId" width="100" headerAlign="center">本方机构</div>
                  	<div field="counterpartyInstId" width="100" headerAlign="center">对方机构</div>
                  	<div field="amt" numberFormat="#,0.00" align="right" width="120" headerAlign="center" 
                   		allowSort="true">本金(元)</div>
					<div field="rate" numberFormat="#,0.0000" headerAlign="center" align="right" width="80"
                    	allowSort="true">利率(%)</div>
                   	<div field="sponsor" width="100" headerAlign="center" >审批发起人</div>
                  	<!-- <div field="vDate" width="100" allowSort="false" headerAlign="center" align="center">起息日</div>
                  	<div field="mDate" width="100" allowSort="false" headerAlign="center" align="center">到期日</div>
                  	<div field="tenor" width="120px" align="center"  headerAlign="center" allowSort="true">期限</div>
                  	<div field="occupancyDays" width="120px" align="right"  headerAlign="center" allowSort="true">实际占款天数</div>
                  	<div field="settlementAmount" width="180px" align="right"  headerAlign="center" allowSort="true"
                  		numberFormat="n4">到期结算金额（元）</div>
               		<div field="basis" width="120px" align="right"  headerAlign="center" renderer="CommonUtil.dictRenderer"
               			data-options="{dict:'calIntDays'}" allowSort="true">计息基准</div>
                    <div field="aDate" width="180" allowSort="false" headerAlign="center" align="center">审批发起时间</div>
                    <div field="tradeElements" width="150" headerAlign="center" >审批要素组合</div> -->
                </div>
            </div>
        </div>
        </div>
    </div>
</body>
    <script>
        mini.parse();
        
        $(document).ready(function(){
            search(grid.pageSize,0);
		});
        
        top["qdfirstpage"] = window;

        var type = "trade";

        var grid = mini.get("datagrid");
        grid.on("beforeload", function (e) {
			e.cancel = true;
			var pageIndex = e.data.pageIndex; 
			var pageSize = e.data.pageSize;
			search(pageSize,pageIndex);
        });
        
		function search(pageSize,pageIndex){
            var param = {};
			param.pageNumber = pageIndex+1;
			param.pageSize = pageSize;
            param.userId=userId;

            var url = "/IfsFlowMonitorController/searchPageFlowMonitor";

			CommonUtil.ajax({
				url:url,
				data:param,
				showLoadingMsg:true,
				messageId:mini.loading('正在查询数据...', "请稍后"),
				callback : function(data) {
                    var dataArray = data.obj.rows;
                  
					grid.setTotalCount(data.obj.total);
		        	grid.setPageIndex(pageIndex);
                    grid.setPageSize(pageSize);
                    grid.setData(dataArray);
                    
                    if(type == 'trade'){
                        $("#t_num").text(data.obj.total);
                    }
				}
			});
        }// end search
        
		//正式交易 审批
		mini.get("verify_commit_btn").on("click",function(){
            var selections = grid.getSelecteds();
            if(selections.length == 0){
				mini.alert("请选中一条记录！","消息提示");
				return false;
			}else if(selections.length > 1){
				mini.alert("暂不支持多笔提交","系统提示");
				return false;
            }
            verify_trade(selections);
			mini.get("verify_commit_btn").setEnabled(true);
        }); 

        function verify_trade(selections){
            var prdNo = selections[0]["prdNo"];
            var tradeId = selections[0]["ticketId"];
            var tradeType = selections[0]["tradeType"];
            
            if(prdNo == null || prdNo == ""){
                mini.alert("抱歉,该笔交易暂时无法在此审批，请进入具体交易界面进行审批！");
                return;
            }
            productType(tradeType,prdNo,tradeId);
        }

        function productType(tradeType,prdNo,tradeId){
            var searchByIdUrl = "";
            var openJspUrl = "";
            var id = "";
            var title = "";
            var datas={};
            datas['ticketId'] = tradeId;
            var selected = mini.get("datagrid").getSelected();
            if(tradeType=="1"){//正式交易
            
            if(prdNo == "irs"){//利率互换
                searchByIdUrl = "/IfsCfetsrmbIrsController/searchCfetsrmbIrsAndFlowIdById";
                Approve.goToApproveJsp(selected.taskId,"441","/cfetsrmb/IRSEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid="+tradeId;
                id = "IRSApprove";
                title = "利率互换审批";
            }else if(prdNo == "irsDerive"){//结构衍生
                searchByIdUrl = "/IfsCfetsrmbIrsController/searchCfetsrmbIrsAndFlowIdById";
                Approve.goToApproveJsp(selected.taskId,"400","/cfetsrmb/IrsDeriveEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid="+tradeId;
                id = "IRSDeriveApprove";
                title = "结构衍生审批";
            }else if(prdNo == "or"){//买断式回购
                searchByIdUrl = "/IfsCfetsrmbOrController/searchCfetsrmbOrAndFlowIdById";
                Approve.goToApproveJsp(selected.taskId,"442","/cfetsrmb/repoEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid="+tradeId;
                id = "repoApprove";
                title = "买断式回购审批";
            }else if(prdNo == "cbt"){//现券买卖
                searchByIdUrl = "/IfsCfetsrmbCbtController/searchCfetsrmbCbtAndFlowIdById";
                Approve.goToApproveJsp(selected.taskId,"443","/cfetsrmb/bondTradingEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid="+tradeId;
                id = "bondTradingApprove";
                title = "现券买卖审批";
            }else if(prdNo == "ibo"){//信用拆借
                searchByIdUrl = "/IfsCfetsrmbIboController/searchCfetsrmbIboAndFlowIdById";
                Approve.goToApproveJsp(selected.taskId,"444","/cfetsrmb/creditLoanEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid="+tradeId;
                id = "creditLoanApprove";
                title = "信用拆借审批";
            }else if(prdNo == "sl"){//债券借贷
                datas['execId'] = tradeId;
                searchByIdUrl = "/IfsCfetsrmbSlController/searchCfetsrmbSlAndFlowIdById";
                Approve.goToApproveJsp(selected.taskId,"445","/cfetsrmb/bondLendingEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid="+tradeId;
                id = "bondLendingApprove";
                title = "债券借贷审批";
            }else if(prdNo == "cr"){//质押式回购
                searchByIdUrl = "/IfsCfetsrmbCrController/searchCfetsrmbCrAndFlowIdById";
                Approve.goToApproveJsp(selected.taskId,"446","/cfetsrmb/pledgeRepoEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid="+tradeId;
                id = "pledgeRepoApprove";
                title = "质押式回购审批";
            }else if(prdNo == "goldspt"){//黄金即期
                searchByIdUrl = "/IfsCfetsmetalGoldController/searchCfetsmetalGoldAndFlowById";
                Approve.goToApproveJsp(selected.taskId,"447","/sge/goldsptEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid="+tradeId;
                id = "IrsSptApprove";
                title = "黄金即期审批";
            }else if(prdNo == "goldfwd"){//黄金远期
                searchByIdUrl = "/IfsCfetsmetalGoldController/searchCfetsmetalGoldAndFlowById";
                Approve.goToApproveJsp(selected.taskId,"448","/sge/goldfwdEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid="+tradeId;
                id = "IrsFwdApprove";
                title = "黄金远期审批";
            }else if(prdNo == "goldswap"){//黄金掉期
                searchByIdUrl = "/IfsCfetsmetalGoldController/searchCfetsmetalGoldAndFlowById";
                Approve.goToApproveJsp(selected.taskId,"449","/sge/goldswapEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid="+tradeId;
                id = "IrsSwapApprove";
                title = "黄金掉期审批";
            }else if(prdNo == "goldLend"){//黄金拆借
                searchByIdUrl = "/IfsGoldLendController/searchCfetsGoldLendAndFlowById";
                Approve.goToApproveJsp(selected.taskId,"450","/sge/GoldLendingEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid="+tradeId;
                id = "goldLendingApprove";
                title = "黄金拆借审批";
            }else if(prdNo == "debt"){//外币债
                searchByIdUrl = "IfsCfetsrmbCbtController/searchCfetsrmbCbtAndFlowIdById";
//                 searchByIdUrl = "/IfsCurrencyController/searchCurrencyAndFlowById";
                Approve.goToApproveJsp(selected.taskId,"451","/bloomberg/fxccydebtEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid="+tradeId;
                id = "fxccydebtApprove";
                title = "外币债审批";
            }else if(prdNo == "rmbspt"){//外汇即期
                searchByIdUrl = "/IfsForeignController/searchSpotAndFlowIdById";
                Approve.goToApproveJsp(selected.taskId,"411","/cfetsfx/rmbsptEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid="+tradeId;
                id = "RmbSptApprove";
                title = "外汇即期审批";
            }else if(prdNo == "rmbfwd"){//外汇远期
                searchByIdUrl = "/IfsForeignController/searchFwdAndFlowIdById";
                Approve.goToApproveJsp(selected.taskId,"391","/cfetsfx/rmfwdEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid="+tradeId;
                id = "rmfwdApprove";
                title = "外汇远期审批";
            }else if(prdNo == "rmbswap"){//外汇掉期
                searchByIdUrl = "/IfsForeignController/searchRmbSwapAndFlowIdById";
                Approve.goToApproveJsp(selected.taskId,"432","/cfetsfx/rmswapEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid="+tradeId;
                id = "rmsSwapApprove";
                title = "外汇掉期审批";
            }else if(prdNo == "ccyswap"){//货币掉期
                searchByIdUrl = "/IfsCurrencyController/searchCcySwapAndFlowIdById";
                Approve.goToApproveJsp(selected.taskId,"431","/cfetsfx/ccyswapEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid="+tradeId;
                id = "CcySwapApprove";
                title = "货币掉期审批";
            }else if(prdNo == "rmbopt"){//人民币期权
                searchByIdUrl = "/IfsForeignController/searchRmbOptAndFlowIdById";
                Approve.goToApproveJsp(selected.taskId,"433","/cfetsfx/rmboptEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid="+tradeId;
                id = "AdvanceRmBoptApprove";
                title = "外汇期权审批";
            }else if(prdNo == "ccyLending"){//外币拆借
                searchByIdUrl = "/IfsForeignController/searchRmbCcylendingAndFlowIdById";
                Approve.goToApproveJsp(selected.taskId,"438","/cfetsfx/ccyLendingEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid="+tradeId;
                id = "AdvanceCcyLendingApprove";
                title = "外币拆借审批";
            }else if(prdNo == "ccypa"){//外币头寸调拨
                searchByIdUrl = "/IfsForeignController/searchRmbCcypaAndFlowIdById";
                Approve.goToApproveJsp(selected.taskId,"439","/cfetsfx/ccyPositionAllocationEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid="+tradeId;
                id = "AdvanceAllcationApprove";
                title = "外币头寸调拨审批";
            }else if(prdNo == "ccypspot"){//外汇对即期
                searchByIdUrl = "/IfsForeignController/searchRmbSpotAndFlowIdById";
                Approve.goToApproveJsp(selected.taskId,"440","/cfetsfx/ccypspotEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid="+tradeId;
                id = "AdvanceCcySpotApprove";
                title = "外汇对即期审批";
            }else if(prdNo == "dp"){//存单发行
                searchByIdUrl = "/IfsCfetsrmbDpController/searchCfetsrmbDpAndFlowIdById";
                Approve.goToApproveJsp(selected.taskId,"452","/cfetsrmb/depositPublishEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid="+tradeId;
                id = "depositPublishApprove";
                title = "债券发行审批";
            }
            }else if(tradeType=="0"){//事前交易
            	
            	if(prdNo == "irs"){//利率互换
                    searchByIdUrl = "/IfsApprovermbIrsController/searchCfetsrmbIrsAndFlowIdById";
                    openJspUrl = CommonUtil.baseWebPath() +"/approvermb/IRSEdit.jsp?action=approve&ticketid="+tradeId;
                    id = "IRSApprove";
                    title = "利率互换审批";
            	}else if(prdNo == "irsDerive"){//结构衍生
                    searchByIdUrl = "/IfsCfetsrmbIrsController/searchCfetsrmbIrsAndFlowIdById";
                    Approve.goToApproveJsp(selected.taskId,"400","/cfetsrmb/IrsDeriveEdit.jsp", target);
                    var target = Approve.approvePage;
                    openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid="+tradeId;
                    id = "IRSDeriveApprove";
                    title = "结构衍生审批";
                }else if(prdNo == "or"){//买断式回购
                    searchByIdUrl = "/IfsApprovermbOrController/searchCfetsrmbOrAndFlowIdById";
                    openJspUrl = CommonUtil.baseWebPath() +"/approvermb/repoEdit.jsp?action=approve&ticketid="+tradeId;
                    id = "repoApprove";
                    title = "买断式回购审批";
                }else if(prdNo == "cbt"){//现券买卖
                    searchByIdUrl = "/IfsApprovermbCbtController/searchCfetsrmbCbtAndFlowIdById";
                    openJspUrl = CommonUtil.baseWebPath() +"/approvermb/bondTradingEdit.jsp?action=approve&ticketid="+tradeId;
                    id = "bondTradingApprove";
                    title = "现券买卖审批";
                }else if(prdNo == "ibo"){//信用拆借
                    searchByIdUrl = "/IfsApprovermbIboController/searchCfetsrmbIboAndFlowIdById";
                    openJspUrl = CommonUtil.baseWebPath() +"/approvermb/creditLoanEdit.jsp?action=approve&ticketid="+tradeId;
                    id = "creditLoanApprove";
                    title = "信用拆借审批";
                }else if(prdNo == "sl"){//债券借贷
                    searchByIdUrl = "/IfsApprovermbSlController/searchCfetsrmbSlAndFlowIdById";
                    openJspUrl = CommonUtil.baseWebPath() +"/approvermb/bondLendingEdit.jsp?action=approve&ticketid="+tradeId;
                    id = "bondLendingApprove";
                    title = "债券借贷审批";
                }else if(prdNo == "cr"){//质押式回购
                    searchByIdUrl = "/IfsApprovermbCrController/searchCfetsrmbCrAndFlowIdById";
                    openJspUrl = CommonUtil.baseWebPath() +"/approvermb/pledgeRepoEdit.jsp?action=approve&ticketid="+tradeId;
                    id = "pledgeRepoApprove";
                    title = "质押式回购审批";
                }else if(prdNo == "goldspt"){//黄金即期
                    searchByIdUrl = "/IfsApprovemetalGoldController/searchCfetsmetalGoldAndFlowById";
                    openJspUrl = CommonUtil.baseWebPath() +"/approvemetal/goldsptEdit.jsp?action=approve&ticketid="+tradeId;
                    id = "IrsSptApprove";
                    title = "黄金即期审批";
                }else if(prdNo == "goldfwd"){//黄金远期
                    searchByIdUrl = "/IfsApprovemetalGoldController/searchCfetsmetalGoldAndFlowById";
                    openJspUrl = CommonUtil.baseWebPath() +"/approvemetal/goldfwdEdit.jsp?action=approve&ticketid="+tradeId;
                    id = "IrsFwdApprove";
                    title = "黄金远期审批";
                }else if(prdNo == "goldswap"){//黄金掉期
                    searchByIdUrl = "/IfsApprovemetalGoldController/searchCfetsmetalGoldAndFlowById";
                    openJspUrl = CommonUtil.baseWebPath() +"/approvemetal/goldswapEdit.jsp?action=approve&ticketid="+tradeId;
                    id = "IrsSwapApprove";
                    title = "黄金掉期审批";
                }else if(prdNo == "goldLend"){//黄金拆借
                    searchByIdUrl = "/IfsApproveGoldLendController/searchCfetsGoldLendAndFlowById";
                    openJspUrl = CommonUtil.baseWebPath() +"/approvesge/GoldLendingEdit.jsp?action=approve&ticketid="+tradeId;
                    id = "goldLendingApprove";
                    title = "黄金拆借审批";
                }else if(prdNo == "debt"){//外币债
                    searchByIdUrl = "/IfsApproveCurrencyController/searchCurrencyAndFlowById";
                    openJspUrl = CommonUtil.baseWebPath() +"/approvefx/fxccydebtEdit.jsp?action=approve&ticketid="+tradeId;
                    id = "fxccydebtApprove";
                    title = "外币债审批";
                }else if(prdNo == "rmbspt"){//外汇即期
                    searchByIdUrl = "/IfsApproveForeignController/searchSpotAndFlowIdById";
                    openJspUrl = CommonUtil.baseWebPath() +"/approvefx/rmbsptEdit.jsp?action=approve&ticketid="+tradeId;
                    id = "RmbSptApprove";
                    title = "外汇即期审批";
                }else if(prdNo == "rmbfwd"){//外汇远期
                    searchByIdUrl = "/IfsApproveForeignController/searchFwdAndFlowIdById";
                    openJspUrl = CommonUtil.baseWebPath() +"/approvefx/rmfwdEdit.jsp?action=approve&ticketid="+tradeId;
                    id = "rmfwdApprove";
                    title = "外汇远期审批";
                }else if(prdNo == "rmbswap"){//外汇掉期
                    searchByIdUrl = "/IfsApproveForeignController/searchRmbSwapAndFlowIdById";
                    openJspUrl = CommonUtil.baseWebPath() +"/approvefx/rmswapEdit.jsp?action=approve&ticketid="+tradeId;
                    id = "rmsSwapApprove";
                    title = "外汇掉期审批";
                }else if(prdNo == "ccyswap"){//货币掉期
                    searchByIdUrl = "/IfsApproveCurrencyController/searchCcySwapAndFlowIdById";
                    openJspUrl = CommonUtil.baseWebPath() +"/approvefx/ccyswapEdit.jsp?action=approve&ticketid="+tradeId;
                    id = "CcySwapApprove";
                    title = "货币掉期审批";
                }else if(prdNo == "rmbopt"){//人民币期权
                    searchByIdUrl = "/IfsApproveForeignController/searchRmbOptAndFlowIdById";
                    openJspUrl = CommonUtil.baseWebPath() +"/approvefx/rmboptEdit.jsp?action=approve&ticketid="+tradeId;
                    id = "AdvanceRmBoptApprove";
                    title = "外汇期权审批";
                }else if(prdNo == "ccyLending"){//外币拆借
                    searchByIdUrl = "/IfsApproveForeignController/searchRmbCcylendingAndFlowIdById";
                    openJspUrl = CommonUtil.baseWebPath() +"/approvefx/ccyLendingEdit.jsp?action=approve&ticketid="+tradeId;
                    id = "AdvanceCcyLendingApprove";
                    title = "外币拆借审批";
                }else if(prdNo == "ccypa"){//外币头寸调拨
                    searchByIdUrl = "/IfsApproveForeignController/searchRmbCcypaAndFlowIdById";
                    openJspUrl = CommonUtil.baseWebPath() +"/approvefx/ccyPositionAllocationEdit.jsp?action=approve&ticketid="+tradeId;
                    id = "AdvanceAllcationApprove";
                    title = "外币头寸调拨审批";
                }else if(prdNo == "ccypspot"){//外汇对即期
                    searchByIdUrl = "/IfsApproveForeignController/searchRmbSpotAndFlowIdById";
                    openJspUrl = CommonUtil.baseWebPath() +"/approvefx/ccypspotEdit.jsp?action=approve&ticketid="+tradeId;
                    id = "AdvanceCcySpotApprove";
                    title = "外汇对即期审批";
                }else if(prdNo == "dp"){//存单发行
                    searchByIdUrl = "/IfsCfetsrmbDpController/searchCfetsrmbDpAndFlowIdById";
                    openJspUrl = CommonUtil.baseWebPath() +"/cfetsrmb/depositPublishEdit.jsp?action=approve&ticketid="+tradeId;
                    id = "depositPublishApprove";
                    title = "债券发行审批";
                }
            }
            openJspUrl=openJspUrl+"&prdNo="+prdNo;
            datas.userId=userId;
            datas.branchId=branchId;
            var params=mini.encode(datas);
            CommonUtil.ajax({
                url:searchByIdUrl,
                data:params,
                callback:function(data){
                    var url = openJspUrl;
                    var tab = {"id": id,name:id,url:url,title:title,
                                parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
                    var paramData = {selectData:data.obj};
                    CommonUtil.openNewMenuTab(tab,paramData);
                    
                }
            });
        }
        
     //刷新
     function refresh(){
         search(grid.pageSize,0);
     }
        
    // 导出报表
   	function exportExcel(){
   		var content = grid.getData();
   		if(content.length == 0){
   			mini.alert("请先查询数据");
   			return;
   		}
   		mini.confirm("您确认要导出Excel吗?","系统提示", 
   			function (action) {
   				if (action == "ok"){
   					var data = {userId:userId,approveType:"approve"};
   					var fields = null;
   					for(var id in data){
   						fields += '<input type="hidden" id="' + id + '" name="'+ id +'" value="' + data[id] + '">';
   					}
   					var urls = CommonUtil.pPath + "/sl/IfsExportController/exportExcel";                                                                                                                         
   					$('<form action="'+ urls +'" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();   
   				}
   			}
   		);
   	}
</script>
</html>