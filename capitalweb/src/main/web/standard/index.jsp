﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="./global.jsp"%>
<head>
	<title>资金管理系统</title>
    <link href="<%=basePath%>/miniScript/menu/menu.css" rel="stylesheet" type="text/css" />
    <script src="<%=basePath%>/miniScript/menu/menu.js" type="text/javascript"></script>
    <script src="<%=basePath%>/miniScript/menu/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="<%=basePath%>/miniScript/jscookie.min.js" type="text/javascript"></script>
	<!-- start do not delete -->
	<!--[if lt IE 10]>
		<script type="text/javascript" src="<%=basePath%>/miniScript/pie/PIE.js"></script>
	<![endif]-->
</head>
<body>
    <div class="header">
        <div class="logo">
        	<%-- <div class="header-top">
        		 <p class="version">资金前置<i class="version-num"></i></p>
        		<span>业务管理系统2019-01</span>
        		<span><%=__brcc.getBranchTitle()%></span>
        	</div> --%>
                <!-- 哈尔滨要求修改20210810 -->
<%--            <div class="sidebar-toggle"><i class="iconfont icon-qiehuan fz-20"></i></div>--%>
                <div class="sidebar-toggle"></div>
            <div class="user_img" >
                <img src="<%=basePath%>/miniScript/images/users/<%=__sessionUser.getBranchId()%>-<%=__sessionUser.getUserId()%>.jpg" alt="">
            </div>
            <%-- <div class="inst" title="<%=__sessionInstitution.getInstName()%>">
            	<%=__sessionInstitution.getInstName()%>
             </div> --%>
            <div class="user" title="<%=__sessionUser.getUserName()%>(<%=__sessionUser.getUserId()%>)">
               <%=__sessionUser.getUserName()%>
                (<span><%=__sessionUser.getUserId()%></span>)
            </div>


        </div>
        <div class="header-main">
        	<%-- <div style="margin-top:14px;margin-left:15px;float:left;">
        		<img alt="" draggable="false" src="<%=basePath%>/miniScript/logo/<%=__brcc.getBranchLogo()%>.png" style="height:30px">
            </div> --%>
             <div style="margin-top:17px;margin-left: 12px;float:left;width: 2px;height: 25px; background: #999;"></div>
            <div class="system" style="margin-top:6px;color: #333333;font-size: 18px;font-family: arial;margin-left:10px;float:left;text-align: center;">
                <span style="text-align:left;overflow: hidden;font-family: cursive;font-size: 12px;"><%=__brcc.getSystemName()%></span><br/>
                <span style="text-align:center;overflow: hidden;font-family: cursive;font-size: 12px;"> Capital Trade Management System</span>

            </div>
            <!--
            <input id="searchMenu" name="searchMenu" class="mini-autocomplete"  emptyText="功能搜索..." searchField="moduleName" style="width:200px;" onbeforeload="onBeforeLoad"
                    ajaxType="post" url="<%=basePath%>/sl/deskMenuController/getAllModuleWhitOutDeskId" dataField="obj" valueField="moduleId" textField="moduleName" onvaluechanged="onValueChanged" />
            -->
<%--            <div class="setting">--%>
<%--                <a class="mini-menubutton primary" iconCls="iconfont icon-xitong3 xitong" menu="#popupMenu" >更多设置</a>--%>
<%--                <ul id="popupMenu" class="mini-menu" style="display:none;">--%>
<%--                    <li class="uploadPhoto">上传工作照</li>--%>
<%--                    <li class="separator"></li>--%>

<%--                    <li class="separator"></li>--%>

<%--                    <!-- <li class="systemStyle">系统风格</li>-->--%>
<%--                    <!-- <li class="separator"></li> -->--%>
<%--                    <li class="menuStyle">菜单风格</li>--%>
<%--                    <li class="myWork">我的工作台</li>--%>
<%--                    <!-- <li class="switchRoleInst">角色机构切换</li> -->--%>
<%--                    <!--<li>清除缓存</li>-->--%>
<%--                </ul>--%>
<%--            </div>--%>
            <div class="exit" title="安全退出系统"><i  class="iconfont iconfontmenu icon-tuichu" >&nbsp;&nbsp;</i></div>
            <div class="uploadPhoto" title="上传我的工作照"><i  class="iconfont iconfontmenu icon-yonghujiaosefenpei" >&nbsp;&nbsp;</i></div>
            <div class="menuStyle" title="菜单风格"><i  class="iconfont iconfontmenu icon-caidanxinxi" >&nbsp;&nbsp;</i></div>
            <div class="myWork" title="我的工作台"><i  class="iconfont iconfontmenu icon-wodegongzuotai" >&nbsp;&nbsp;</i></div>

            <div class="pwdUpdate" title="密码修改"><i class="iconfont iconfontmenu icon-mima">&nbsp;&nbsp;</i></div>
            <div class="welcom" title="登录用户"><i class="sl-icon sl-user-black iconfontmenu"></i>&nbsp;<%=__sessionUser.getUserName()%></div>
           <%--  <div class="inst1" title="登录机构"><i class="sl-icon sl-dashboard-black iconfontmenu"></i>&nbsp;<%=__sessionInstitution.getInstName()%></div>
            <div class="time"><i class="sl-icon sl-dashboard-black iconfontmenu"></i>系统账务日期:&nbsp;<span id="bizDate"></span></div> --%>

<%--            <div class="time1" id="time1"><i class="sl-icon sl-dashboard-black iconfontmenu"></i></div>--%>
<%--            <div class="time"><i class="sl-icon sl-dashboard-black iconfontmenu"></i>系统日期:<span id="bizDate"></span></div>--%>


        </div>
    </div>

    <div style="height:45px;display:inline-block;"></div>
    <div class="left">
        <div id="left-menu" style="width:100%;"></div>
    </div>
    <div class="right">
        <div class="mini-fit">
            <div id="tabs1" class="mini-tabs" activeIndex="0" style="width:100%;height:100%;" plain="true" contextMenu="#tabsMenu" >
           <!--  <div name="first" id="first" title="我的工作台" url="approve.jsp"></div> -->
            <div name="first" id="first" title="我的工作台" ></div>
            </div>
        </div>
    </div>
    <div class="footer" >

    <div>

    </div></div>
    <ul id="tabsMenu" class="mini-contextmenu" onbeforeopen="onBeforeOpen">
        <li onclick="closeTab">关闭标签页</li>
	    <li onclick="closeAllBut">关闭其他标签页</li>
	    <li onclick="closeAll">关闭所有标签页</li>
        <li onclick="closeAllButFirst">关闭其他[首页除外]</li>
    </ul>
</body>
</html>
<script>

    mini.parse();
    top["win"] = window;

    var tabs = mini.get("tabs1");
    //tab监听事件
    tabs.on("tabdestroy",function(e){
        if(e.tab.parentId && tabs.getTabIFrameEl(e.tab.parentId) && typeof(tabs.getTabIFrameEl(e.tab.parentId).contentWindow.search)=="function"){
            try {
                tabs.getTabIFrameEl(e.tab.parentId).contentWindow.search(10,0);
            } catch (error) {

            }
        }else if(e.tab.params && e.tab.params.pWinId && e.tab.params.closeFun){
            try {
                eval('top["'+e.tab.params.pWinId+'"].'+e.tab.params.closeFun+'()');
            } catch (error) {

            }

        }
    });
    var now = "";

    $(document).ready(function(){
    	//查询日志
    	//getOpicsDate();
        // setTimeout("showTime()",1000);
        //根据条件查询台子
        CommonUtil.ajax({
        url:"/deskMenuController/getDeskMenus",
        data:{'branchId':branchId,'userId':userId,'instId':instId,'roleIds':roleIds},
        callback:function(data){
        var jq = $("#left-menu").menu({
               mode: 'inline',
                items: data.obj,
                theme: 'light',
                iconfont:false
            });
//         $(".menuStyle").click();
        }});
        //连接websocket
        CommonUtil.connectWs("/ctms/sl/stomp");
    });

    <!-- 哈尔滨要求修改20210810 -->
    // $('body').toggleClass('compact');
    // $('.user_img').hide();
    $('.left').css('top','173px');

function onValueChanged(e) {
        var item = e.selected;
        if (item) {
            var tab ={
				id:'quickSearch',
				name:'quickSearch',
				title:item.moduleName,
				url: CommonUtil.baseWebPath() + item.moduleUrl,
				showCloseButton:true,
				parentId:top['win'].tabs.getActiveTab().name
			};
			var param ={};
			//打开Tabs
            CommonUtil.openNewMenuTab(tab,param);
        }
    }

function onBeforeLoad(e){
    e.contentType="application/json;charset=UTF-8";
    e.data.userId =userId;
    e.data.instId =instId;
    e.data.roleIds =roleIds;
    e.data.branchId = branchId;
    e.data =$.toJSON(e.data);
}
/**
* 切换角色机构
**/
$(".switchRoleInst").click(function(){
    var win = mini.open({
       title:'切换角色机构',
       showModal: true,
       allowResize: true,
       height:400,
       width:500,
       url:'./switchRoleInst.jsp',
       ondestroy:function(action){
         if(action == "ok"){
            window.location.reload();
         }
       }
  });
});

/**
 * 上传头像 uploadPhoto
 * */
 $(".uploadPhoto").click(function () {
   var win = mini.open({
       title:'上传工作照',
       showModal: true,
       allowResize: true,
       height:400,
       width:500,
       url:'./userPhoto.jsp',
       ondestroy:function(action){
         if(action == "ok"){
            window.location.reload();
         }
       }
  });
});
/**
 * 我的工作台 myWork
 * */
 $(".myWork").click(function () {
    var url =  "./approve.jsp";
    var tab = { id: "first", name: "first", text: "我的工作台", url: url};
    if (CommonUtil.isNull(tabs.getTab(tab.name)))
    {
        top['win'].showTab(tab);
    }else{
        tabs.activeTab(tabs.getTab(tab.name));
    }
});
/**
 * 密码修改
 * */
 $(".pwdUpdate").click(function () {
    mini.open({
        title:'修改密码',
        showModal: true,
        allowResize: true,
        height:400,width:500,
        url:'./PasswordChange.jsp',
       ondestroy:function(action){
         if(action == "ok"){
             //退出登录
            CommonUtil.ajax({
                url:"/UserController/logout",
                callback : function(data) {
                   window.location.href=".././"; //哈尔滨银行去掉standard后缀,请勿调整!!!
                }
            });
         }
       }
    });
});

/**
 *切换系统风格
 */
  $(".systemStyle").click(function () {
     mini.open({
         title:'系统风格',
         showModal: true,
         allowResize: true,
         height:400,width:500,
         url:'./systemStyle.jsp',
        ondestroy:function(action){
          if(action == "ok"){
              //刷新系统
        	  window.location.reload();
          }
        }
     });
 });


/**
 * 系统安全退出
 * */
$(".exit").click(function () {
    mini.confirm('您确认要退出系统吗？','系统提示',function(r){
        if (r=='ok'){
            CommonUtil.ajax({
                url:"/UserController/logout",
                relbtn:'top_exit',
                callback : function(data) {
                    window.location.href=".././"; //哈尔滨银行去掉standard后缀,请勿调整!!!
                    Cookies.remove('userId');
                }
            });
        }
    });

});

//获取日志
function getOpicsDate(){
   CommonUtil.ajax({
       		url : "/IfsOpicsAcupController/acupOpicsDate",
			data : {"br":opicsBr},
			callback : function(data) {
			 $("#bizDate").text(data);
		}
    });
}

/**
 * 更新Opics日期
 * */
$(".time").click(function () {
  getOpicsDate();
});


/***************************************************
 *                                                *
 *               以下为功能性切换                   *
 * ===============================================*
 *                                                *
 *                                                *
 *************************************************/
 $(document.body).on("click", ".ant-menu-item", function () {
        var node = $(this)[0];
        node.text = node.innerText;
        node.url = $(node).attr("url");
        node.id = $(node).attr("uid");
        node.name = $(node).attr("name");
        node.branchId = branchId;
        node.userId =userId;
        node.instId =instId;
        node.roleIds =roleIds;
        var tabs = mini.get("tabs1");
        showTab(node);

    });

    function showTab(node) {
        var id = "tab$" + node.id;
        var tab = tabs.getTab(id);
        if (!tab) {
            tab = {};
            tab.name = id;
            tab.title = node.text;
            tab.showCloseButton = true;
            tab.url = node.url;
            tab.deskId =node.name;
            tab.parentId = node.parentId;
            tabs.addTab(tab);
        }
        tabs.activeTab(tab);
    }

    function openNewTab(tab,params) {
        if (CommonUtil.isNull(tabs.getTab(tab.name))){
            tab.params = params;
            tabs.addTab(tab);
		}
		tabs.activeTab(tabs.getTab(tab.name));
    }

    /**
     * 关闭Tab 并刷新父窗体.需要设置parentId
     **/
    function closeMenuTab(){
        var activeTab = top["win"].tabs.getActiveTab();
        if(activeTab){
            tabs.removeTab(activeTab.name);
        }

	}

    var tabs = mini.get("tabs1");
    var currentTab = null;

    function onBeforeOpen(e) {
        currentTab = tabs.getTabByEvent(e.htmlEvent);
        if (!currentTab) {
            e.cancel = true;
        }
    }
	/**
	 *菜单风格切换
	**/
    $(".menuStyle").click(function () {
        var menu = $("#left-menu").data("menu");
        var left = $(".left");
        $('body').removeClass('compact');
        left.toggleClass("change");
        if (left.hasClass("change")) {
            $(document.body).addClass("change");
        }
        else {
            $(document.body).removeClass("change");
        }
        var menuop = menu.options;

        mini.layout();
        if (menuop.mode == "inline") {
            menu.setMode("horizontal");
            $(".user_img").css("display","none");
            $(".sidebar-toggle").css("display","none");
            $('.left').removeAttr('style');
        } else {
            menu.setMode("inline");
            $(".user_img").css("display","block");
            $(".sidebar-toggle").css("display","block");
            $('.left').css('top','173px');
        }

    });



    var tabs = mini.get("tabs1");
    var currentTab = null;

    function onBeforeOpen(e) {
        currentTab = tabs.getTabByEvent(e.htmlEvent);
        if (!currentTab) {
            e.cancel = true;
        }
    }

    ///////////////////////////
    function closeTab() {
        tabs.removeTab(currentTab);
    }
    function closeAllBut() {
        tabs.removeAll(currentTab);
    }
    function closeAll() {
        tabs.removeAll();
    }
    function closeAllButFirst() {
        var but = [currentTab];
        but.push(tabs.getTab("first"));
        tabs.removeAll(but);
    }

    var leftwidth = $(".left").width();
    if (leftwidth == 198) {
        $("#left-menu").slimScroll({
            width: "100%",
            height: "100%"
        });
    }

    <!-- 哈尔滨要求修改20210810 -->
    // $(".sidebar-toggle").click(function () {
    //     $('body').toggleClass('compact');
    //     $('.user_img').show();
    //     $('.left').css('top','173px');
    //     mini.layout();
    //     if ($('body').hasClass('compact')) {
    //     	$('.user_img').hide();
    //         $('.left').css('top','108px');
    //     }
    //
    // });

    /**
     * 打开提示框
     */
    function tipshbsxtx(){
        CommonUtil.openNewMenuTab({id:'302-1-1',name:'302-1-1',title:'还本收付息提醒',url:'Ifs/report/securServer.jsp',showCloseButton:true},{selectedData:''});
        //移除
        $(".toast-item-wrapper").remove();
        //移除回调函数

    }

</script>