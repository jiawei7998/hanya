<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<head>
	<script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
	<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
	<script type="text/javascript" src="<%=basePath%>/miniScript/miniui/res/ajaxfileupload.js"></script>
	<title></title>
</head>
<html>
<body style="width:100%;height:80%;background:white">
<div  class="mini-fit" style="margin: 20px;">导入IFRS9会计配置表：
	<input class="mini-htmlfile" name="Fdata"  id="file1" style="width:300px;"/>

	<div style="display: inline;">
		<a id="import_btn" class="mini-button" style="display: none"  onclick="ajaxFileUpload" style="width:55px;">导入</a>
	</div>
	<div id="Ifrs9ExcelConfigManage" class="mini-fit" style="width:100%;height:80%">
		<div>
			<div id="Ifrs9_tabs" class="mini-fit" >
				<div title="操作说明" id="Operate_div" style="padding:20px;">
					<div id="container">
						<%-- S item --%>
						<div class="item">
							<div class="inner">
								<h2>第一步</h2>
								<p>需要在数据库设定好会计的维度字典表；维度的Key即为表单字段的JAVA驼峰值；值为该维度key的数据字典取值。</p>
								<p>需要在数据库定义维度的“数据字典”取值</p>
							</div>
						</div>
						<%-- E item --%>

						<%-- S item --%>
						<div class="item">
							<div class="inner">
								<h2>第二步</h2>
								<p>需要通过“IFRS读取EXCEL初始参数”维护完整的读取Excel会计分录配置表的读取规则。</p>
								<p>逐一检查完整性【读取IFRS9会计参数】</p>
							</div>
						</div>
						<%-- E item --%>

						<%-- S item --%>
						<div class="item">
							<div class="inner">
								<h2>第三步</h2>
								<p>点击[导入IFRS9会计配置表]按钮, 完成Excel的会计配置;</p>
								<p>点击[导入]按钮，等进度条消失，显示“成功”</p>
							</div>
						</div>
						<div class="item">
							<div class="inner">
								<h2>第四步</h2>
								<p>查看点击【查看IFRS9导入结果】依次检查产品的会计配置完整性;</p>
							</div>
						</div>
						<%-- E item --%>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>


<script type="text/javascript">
	mini.parse();

	//模板导入
	function ajaxFileUpload() {

		var inputFile = $("#file1 > input:file")[0];
		var messageid = mini.loading("加载中,请稍后... ", "提示信息");

		$.ajaxFileUpload({
			url: '<%=basePath%>/sl/ReadIfrs9ExcelController/importIfrs9Excel', //用于文件上传的服务器端请求地址
			fileElementId: inputFile,               //文件上传域的ID
			dataType: 'text',
			//返回值类型 一般设置为json
			success: function (data, status)    //服务器成功响应处理函数
			{
				mini.hideMessageBox(messageid);
				mini.alert("导入成功: " +data);

			},
			error: function (data, status, e)   //服务器响应失败处理函数
			{
				mini.hideMessageBox(messageid);
				mini.alert("导入失败: " + data);
			},
			complete: function () {
				var jq = $("#file1 > input:file");
				jq.before(inputFile);
				jq.remove();
			}
		});
	}

	$(document).ready(function () {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn){});
	});
</script>
</body>

</html>