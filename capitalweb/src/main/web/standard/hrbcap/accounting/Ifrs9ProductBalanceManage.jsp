<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
	<%@ include file="../../global.jsp"%>
<html>
<head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
	<style>
		table th{
		text-align:left;
		padding-left:10px;
		width:120px;
	    border-right: 0;
	    }
	    .header {
	    height: 28px;
	    /* background: #95B8E7; */
	    color: #404040;
	    background: linear-gradient(to bottom,#EFF5FF 0,#E0ECFF 100%);
	    background-repeat: repeat-x;
	    line-height:28px; /*设置line-height与父级元素的height相等*/
	    text-align:left; /*设置文本水平居中*/
	    font-size:14px;
		}
	</style>
</head>
<body style="width:100%;height:100%;background:white">
	<div class="mini-panel" title="会计分录查询" style="width:100%;">
		<div style="width:100%;padding-top:10px;padding-bottom:10px">
			<div id="search_form" >
				<!-- <input id="dealNo" name="dealNo" class="mini-textbox" labelField="true" label="业务编号：" labelStyle="text-align:right;" emptyText="请输入交易流水号"/> -->
				<input id="subjCode" name="subjCode" class="mini-textbox" labelField="true" label="科目号：" labelStyle="text-align:right;" emptyText="请输入科目号"/>
				<!-- <input id="subName" name="subName" class="mini-textbox" labelField="true" label="科目名称：" labelStyle="text-align:right;" emptyText="请输入科目名称"/> -->
				<input id="prdNo" name="prdNo" class="mini-combobox" labelField="true" label="产品类型：" labelStyle="text-align:right;"  data="CommonUtil.serverData.dictionary.MonitorPrdType" emptyText="请选择产品类型"/>
				<input id="postDate" name=""postDate"" value="<%=__bizDate%>" class="mini-datepicker" labelField="true" label="开始日期：" valueType="string" labelStyle="text-align:right;"
				 emptyText="选择日期" />

				<span style="float:right;margin-right: 50px">
					<a id="search_btn" class="mini-button" style="display: none"   onclick="query">查询</a>
					<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear">清空</a>
					<!-- <a id="export_btn" class="mini-button" style="display: none"   onclick="clear">分页导出</a> -->
					<!-- <a id="export_total_btn" class="mini-button" style="display: none"   onclick="clear">全量导出</a> -->
				</span>
			</div>
		</div>
	</div>
	<div id="querys" class="mini-fit" style="margin-top: 5px;">
		<div id="form_grid" class="mini-datagrid borderAll" allowAlternating="true" style="width:100%;height:100%;" sortMode="client" fitColumns="false">
			<div property="columns">
				<div field="prdNo" width="120" allowSort="false" headerAlign="center"  >产品编号</div>
				<div field="prdName" width="120" allowSort="false" headerAlign="center" align="center">产品名称</div>
				<div field="subjCode" width="120" allowSort="true" headerAlign="center" align="left">科目号</div>
				<div field="subjName" width="310" allowSort="false" headerAlign="center">科目名称</div>
				<div field="debitValue" width="120" numberFormat="#,0.00" headerAlign="center" align="right" allowSort="true">借方余额</div>
				<div field="creditValue" width="120" numberFormat="#,0.00" headerAlign="center" align="right" allowSort="true">贷方余额</div>
				<div field="postDate" width="80" allowSort="true" headerAlign="center" align="center">日期</div>
				<div field="instId" width="120" allowSort="true" headerAlign="center" align="center">机构号</div>
				<div field="instName" width="120" allowSort="true" headerAlign="center" align="center">机构名称</div>
			</div>
		</div>
	</div>
<script type="text/javascript">
	mini.parse();
	var grid = mini.get("form_grid");
	var form = new mini.Form("search_form");
	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex;
		var pageSize = e.data.pageSize;
		search(pageSize, pageIndex);
	});
	function search(pageSize, pageIndex) {
		var form = new mini.Form("search_form");
		if (form.isValid == false)
			return;
		//提交数据
		var data = form.getData();//获取表单多个控件的数据  
		data['pageNumber'] = pageIndex + 1;
		data['pageSize'] = pageSize;
		var param = mini.encode(data); //序列化成JSON
		CommonUtil.ajax({
			url: '/Ifrs9BookkeepingController/queryProductBal',
			data: param,
			callback: function (data) {
				var grid = mini.get("form_grid");
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
				grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	/*mini.get("export_btn").on("click",function(){
		mini.confirm("您确认要导出Excel吗?","系统提示",function(action){
			if(action=='ok'){
				var params = form.getData();
				params.pageNumber = grid.getPageIndex();
				params.pageSize = grid.getPageSize();
				params.queryBalance = 'product';
				var urls = "../../../sl/Ifrs9ReportsController/expQueryBalanceManage";
				var inputs = ''; 
				for (var key in params){
					if(!CommonUtil.isNull(params[key])){
						inputs+='<input type="hidden" name="'+ key +'" value="'+ params[key] +'" />'; 
					}
				}
				jQuery('<form action="'+ urls +'" method="post">'+inputs+'</form>').appendTo('body').submit().remove();				
			}
			
		})
	});*/
	/*mini.get("export_total_btn").on("click",function(){
		mini.confirm("您确认要全量导出Excel吗?","系统提示",function(action){
			if(action=='ok'){
				var params = form.getData();
				params.fileType = 'ProductBalanceManage';
				CommonUtil.ajax({
					url:"/Ifrs9ReportsController/downLoadExcelTota",
					data: params,
					callback:function(data){
						mini.alert(data.desc,"提示");
					}
				});			
			}
			
		})
	});*/
	function clear() {
		var form = new mini.Form("#search_form");
		form.clear();
		query();
	}

	function compareDate() {
			var sta = mini.get("entry_date_begin");
			var end = mini.get("entry_date_end");
			if (sta.getValue() > end.getValue() && end.getValue("")) {
				mini.alert("开始时间不能大于结束时间", "系统提示");
				return end.setValue(sta.getValue());
			}
		}	


	//查询的方法
	function query() {
		var grid = mini.get("form_grid");
		search(grid.pageSize, 0);
	}

	$(document).ready(function () {
		search(10, 0);
	})

</script>
</body>
</html>