<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
	<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
	<%@ include file="../../global.jsp"%>
		<html>

<head>
<script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>

	<style>
	table th{
	text-align:left;
	padding-left:10px;
	width:180px;
    border-right: 0;
    }
    .header {
    height: 32px;
    /* background: #95B8E7; */
    color: #404040;
    background: linear-gradient(to bottom,#EFF5FF 0,#E0ECFF 100%);
    background-repeat: repeat-x;
    line-height:32px; /*设置line-height与父级元素的height相等*/
    text-align: center; /*设置文本水平居中*/
    font-size:13px;font-weight:bold;
	}
	</style>
</head>

<body style="width:100%;height:100%;background:white">
	<div class="mini-fit">
    <div  class="mini-panel" id="field_form" title="分录基本信息" style="width:100%;"  allowResize="true" collapseOnTitleClick="false">
		<div class="leftarea" style="margin-top:5px;">
			<input id="dealNo" name="dealNo" class="mini-textbox"  labelField="true" label="交易流水号："   labelStyle="text-align:left;width:130px;margin-left:15px;"   required="true"   style="width:95%;" emptyText="请输入交易单号"/>
			<input id="sponInst" name="sponInst" class="mini-buttonedit"  emptyText="请选择机构" allowInput="false"  labelField="true" label="交易机构："  labelStyle="text-align:left;width:130px;margin-left:15px;"  onbuttonclick="onInsQuery"  style="width:95%;" />
		</div>
		<div class="rightarea" style="margin-top:5px;">
			<input id="ccy" name="ccy" class="mini-combobox"  labelField="true"  style="width:95%;" data="CommonUtil.serverData.dictionary.Currency"  label="币种："  labelStyle="text-align:left;margin-left:15px;width:130px;" emptyText="请选择币种……" required="true" />
			<input id="eventType" name="eventType" class="mini-hidden"  value="9015">
		</div>
		<div class="centerarea">
			<input id="note" name="note" class="mini-textbox"  enabled="false" labelField="true" label="调账备注："  labelStyle="text-align:left;width:130px;margin-left:15px;"   style="width:97%;"/>
		</div>
	 </div>  


	<div id="Ifrs9ManualAdjustmentEdit" class="mini-datagrid borderAll" style="width:100%;height:48%;" allowResize="true" showPager="false" showHeader="true" 
    ondrawgroup="onDrawGroup"  showColumnsMenu="true" collapseGroupOnLoad="false" enableGroupOrder="false" allowAlternating="true"
     allowCellEdit="true" allowCellSelect="true"  fitColumns="false" title="调账列表">
		<div property="columns">
			<div type="indexcolumn" headerAlign="center" align="center" width="50px">序号</div>
			
			<div field="debitCreditFlag" name="debitCreditFlag" width="150" align="left" headerAlign="center" allowSort="false" renderer="CommonUtil.dictRenderer" data-options="{dict:'DebitCreditConfigFlag'}" >借贷方向
			<input property="editor" class="mini-combobox"  style="width:100%;" minWidth="150" data="CommonUtil.serverData.dictionary.DebitCreditConfigFlag" />
			</div>
			
			<div field="subjCode" name="subjCode" width="150" align="right" headerAlign="center" allowSort="false"  >科目代码
			<input property="editor" class="mini-buttonedit" onbuttonclick="onSubjCodeQuery" style="width:100%;" minWidth="150" />
         			</div>
			<div field="subjName" name="subjName" width="150" align="right" headerAlign="center" allowSort="false"  >科目名称
			<input property="editor" class="mini-textbox" style="width:100%;" minWidth="150" />
         			</div>
			<div field="value" name="value" width="150" align="center" headerAlign="center" allowSort="false"  >金额(元)
			<input property="editor" class="mini-textbox" style="width:100%;" minWidth="150" />
         			</div>
              
		</div>
	</div>

	</div>  
    <%@ include file="../../Common/Flow/MiniApproveOpCommon.jsp"%>
<script type="text/javascript">
//获取当前tab
	mini.parse();
	var currTab = top["win"].tabs.getActiveTab();
	
	var params = currTab.params;
	var action =params.action;
	var grid = mini.get("Ifrs9ManualAdjustmentEdit");
	
	var field_form = new mini.Form("field_form");
	var row=params.selectedData;
	var tradeData={};
	if(row){
		tradeData.selectData=row;
		tradeData.operType=action;	
		tradeData.serial_no = row.flowId;
		tradeData.task_id=row.taskId1;				
	}
	
	
	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex;
		var pageSize = e.data.pageSize;
		search(pageSize, pageIndex);
	});
	
	
	$(document).ready(function(){
			init();
			field_form.setEnabled(false);
	})
	//渲染数据
	function init() {
		search(grid.pageSize, 0);
		var form11=new mini.Form("#approve_operate_form");
		form11.setEnabled(true);
	}
	function search(pageSize, pageIndex) {
		var data = {};      //获取表单数据
		data['pageNumber'] = pageIndex + 1;
		data['pageSize'] = 99999999;
		
		var row=params.selectedData;
		flowId =row.flowId;
		if(CommonUtil.isNotNull(CommonUtil.isNotNull(flowId)))
		{
			data['flowId'] = flowId;
			var param = mini.encode(data);   //序列化json格式
			CommonUtil.ajax({
				url:"/TbManualEntryController/selectManualEntryVo",
				data: param,
				callback: function (data) {
					var grid = mini.get("Ifrs9ManualAdjustmentEdit");
					//设置分页
					if(data != null){
						grid.setData(data.constact_list);
						field_form.setData(data);
						
						var sponInst=mini.get("sponInst");
						sponInst.setValue(data.sponInst);
						sponInst.setText(data.inst_fullname);								
					}
				}
			});
		}
	}
	</script>
</body>
<script type="text/javascript" src="../../Common/Flow/MiniApproveOpCommon.js"></script>	
		</html>