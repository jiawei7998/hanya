<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<%
    String branchId =__sessionUser.getBranchId();
    String adminUserId =__sessionUser.getUserId();
    %>
<html>
  <head>
  </head>
  
<body style="width:100%;height:100%;background:white">
	<fieldset class="mini-fieldset">
    	<legend>科目查询</legend>
		<div showCollapseButton="false" showHeader="true">
			<table id="subject_search_form" style="width:100%;">
				<tr>
					<td>
						<input id="subjName" name="subjName" class="mini-textbox" labelField="true"  label="科目名称:" style="width:90%;" emptyText="请输入科目名称"/>
					</td>
					<td>
						<a class="mini-button" style="display: none"   id="search_btn" onclick="search();">科目查询</a>
					</td>
				</tr>
			</table>
		</div> 
	</fieldset>
	<div class="mini-fit">
		<div id="treegrid" class="mini-treegrid" showTreeIcon="true" treeColumn="subjName" idField="subjCode" parentField="parentSubjCode" 
			resultAsTree="true" allowResize="false" expandOnLoad="true" onrowdblclick="onRowDblClick"   fitColumns="false"
			allowAlternating="true" sortMode="client">
			<div property="columns">
				<div type="indexcolumn" width="50">序号</div>
				<div name="subjName" field="subjName" width="200">科目名称</div>
				<div field="subjCode"   width="60">科目号</div>
			</div>
		</div>
	</div>

	


<script type="text/javascript">
	mini.parse();
	$(document).ready(function(){
		search();
	});
	function search() {
		var form = new mini.Form("subject_search_form");
		form.validate();
		//提交数据
		var data = form.getData();//获取表单多个控件的数据
		var params = mini.encode(data); //序列化成JSON
		CommonUtil.ajax({
			url : '/TbSubjectDefController/searchPageTbSubjectDefForTree',
			data : params,
			callback : function(data) 
			{
				var grid = mini.get("treegrid");
				grid.setData(data.obj);
			}
		}); 
	}

	function GetData() {
		var grid = mini.get("treegrid");
		var row = grid.getSelected();
		return row;
	}

	function onRowDblClick(e) {
		onOk();
	}
	function CloseWindow(action) {
		if (window.CloseOwnerWindow)
			return window.CloseOwnerWindow(action);
		else
			window.close();
	}

	function onOk() {
		CloseWindow("ok");
	}
	function onCancel() {
		CloseWindow("cancel");
	}
		
</script>
</body>
</html>