<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
	<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
    <title></title>
  </head>
  
<body style="width:100%;height:100%;background:white">
	<div class="mini-panel" title="交易科目余额查询" style="width:100%;">
	<div style="width:100%;padding-top:10px;padding-bottom:10px">
		<div id="search_form" class="mini-form" width="80%">
			<input id="dealNo" name="dealNo" class="mini-textbox"  width="320px"  labelField="true"  label="交易流水号：" labelStyle="text-align:right;"  emptyText="请输入交易流水号"/>
			<span style="float:right;margin-right: 150px">
				<a id="search_btn" class="mini-button" style="display: none"  onclick="query">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"  onclick="clear">清空</a>
				<!-- <a id="export_btn" class="mini-button" style="display: none"  onclick="clear">下载excel</a> -->
			</span>
		</div>
	</div>
	</div>
	
	<div id="tradeManage" class="mini-fit" style="padding-top:10px;">
		<div id="grid" class="mini-datagrid borderAll" style="width:100%;height:97%;" idField="dealNo" 
		allowAlternating="true" allowResize="true"   border="true" 
		multiSelect="false" sortMode="client">
			<div property="columns">
				<div field="dealNo" name="dealNo" width="150" headerAlign="center" align="center" allowSort="true">交易流水号</div>				
				<div field="subjCode" name="subjCode" width="150" headerAlign="center" align="center" allowSort="true">核心科目号</div>
				<div field="subjName" name="subjName" width="200" headerAlign="center" align="left" allowSort="true">核心科目名称</div>
				<div field="debitValue" name="debitValue" numberFormat="#,0.00" width="100" align="center" headerAlign="center" allowSort="true">借方余额</div>				
				<div field="creditValue" name="creditValue" numberFormat="#,0.00" width="100" headerAlign="center" align="center" allowSort="true">贷方余额</div>				
				<div field="receiveValue" name="receiveValue" numberFormat="#,0.00" width="100" headerAlign="center" align="center" allowSort="true">收方余额</div>				
				<div field="payValue" name="payValue" numberFormat="#,0.00" width="100" headerAlign="center" align="center" allowSort="true">付方余额</div>				
				<div field="lstDate" name="lstDate" width="100" headerAlign="center" align="center" allowSort="true">最后修改时间</div>				
			</div>	
		</div>  
	</div>
<script type="text/javascript">
	mini.parse();

	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var form=new mini.Form("#search_form");
	var grid = mini.get("grid");
	top["TbDealBalanceManage"] = window;
	 //清空
	function clear(){
			var form=new mini.Form("search_form");
			form.clear();
			query();
		}
		/* 按钮 查询事件 */
	function query(){
		search(grid.pageSize,0);
	}
	/* 查询 */
	function search(pageSize,pageIndex){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统提示");
			return;
		}
	
		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		var params = mini.encode(data);
	    CommonUtil.ajax({
			url:"/Ifrs9BookkeepingController/queryDealBal",
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	/*mini.get("export_btn").on("click",function(){
		mini.confirm("您确认要导出Excel吗?","系统提示",function(action){
			if(action=='ok'){
				var params = form.getData();
				params.pageNumber = grid.getPageIndex();
				params.pageSize = grid.getPageSize();
				var urls = "../../../sl/Ifrs9ReportsController/expDealBalanceManage";
				var inputs = ''; 
				for (var key in params){
					if(!CommonUtil.isNull(params[key])){
						inputs+='<input type="hidden" name="'+ key +'" value="'+ params[key] +'" />'; 
					}
				}
				jQuery('<form action="'+ urls +'" method="post">'+inputs+'</form>').appendTo('body').submit().remove();				
			}
			
		})
	});*/
	
	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});
	$(document).ready(function(){
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
			search(10, 0);
		});
	})
</script>
</body>
</html>