<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
	<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
	<%@ include file="../../global.jsp"%>
		<html>

<head>
<script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
	<style>
	table th{
	text-align:left;
	padding-left:10px;
	width:180px;
    border-right: 0;
    }
    .header {
    height: 32px;
    /* background: #95B8E7; */
    color: #404040;
    background: linear-gradient(to bottom,#EFF5FF 0,#E0ECFF 100%);
    background-repeat: repeat-x;
    line-height:32px; /*设置line-height与父级元素的height相等*/
    text-align: center; /*设置文本水平居中*/
    font-size:13px;font-weight:bold;
	}
	</style>
</head>
	<div class="mini-fit">
	<body style="width:100%;height:100%;background:white">
    <div  class="mini-panel" id="field_form" title="分录基本信息" style="width:100%;"  allowResize="true" collapseOnTitleClick="false">
		<div class="leftarea" style="margin-top:5px;">
			<input id="dealNo" name="dealNo" class="mini-textbox"  labelField="true" label="交易流水号："   labelStyle="text-align:left;width:130px;margin-left:15px;"   required="true"   style="width:95%;" emptyText="请输入交易单号"/>
			<input id="sponInst" name="sponInst" class="mini-buttonedit"  emptyText="请选择机构" allowInput="false"  labelField="true" label="交易机构："  labelStyle="text-align:left;width:130px;margin-left:15px;"  onbuttonclick="onInsQuery"  style="width:95%;" />
		</div>
		<div class="rightarea" style="margin-top:5px;">
			<input id="ccy" name="ccy" class="mini-combobox"  labelField="true"  style="width:95%;" data="CommonUtil.serverData.dictionary.Currency"  label="币种："  labelStyle="text-align:left;margin-left:15px;width:130px;" emptyText="请选择币种……" required="true" />
			<input id="eventType" name="eventType" class="mini-hidden"  value="9015">
		</div>
		<div class="centerarea">
			<input id="note" name="note" class="mini-textbox"  enabled="false" labelField="true" label="调账备注："  labelStyle="text-align:left;width:130px;margin-left:15px;"   style="width:97%;"/>
		</div>
	 </div>  
	 	<span  id="buttons" style="margin:2px;display: block;">
			<a class="mini-button" style="display: none"  id="add_btn" name="add_btn"  onclick="add"  >新增</a>
			<a class="mini-button" style="display: none"  id="delete_btn" name="delete_btn"  onClick="del" >删除</a>
	</span>

			<div id="Ifrs9ManualAdjustmentEdit" class="mini-datagrid borderAll" style="width:100%;height:60%;" allowResize="true" showPager="false" showHeader="true" 
		    ondrawgroup="onDrawGroup"  showColumnsMenu="true" collapseGroupOnLoad="false" enableGroupOrder="false" allowAlternating="true"
		     allowCellEdit="true" allowCellSelect="true"  fitColumns="false" title="调账列表">
				<div property="columns" >
					<div type="indexcolumn" headerAlign="center" align="center" width="50px">序号</div>
					
					<div field="debitCreditFlag"   name="debitCreditFlag" width="150" align="left" headerAlign="center" allowSort="false" renderer="CommonUtil.dictRenderer" data-options="{dict:'DebitCreditConfigFlag'}" >借贷方向
					<input property="editor" class="mini-combobox"  style="width:100%;" minWidth="150" data="CommonUtil.serverData.dictionary.DebitCreditConfigFlag" />
					</div>
					
					<div field="subjCode"  name="subjCode" width="150" align="right" headerAlign="center" allowSort="false"  >科目代码
					<input property="editor"  class="mini-buttonedit" onbuttonclick="onSubjCodeQuery" style="width:100%;" minWidth="150" />
           			</div>
					<div field="subjName"  name="subjName" width="150" align="right" headerAlign="center" allowSort="false"  >科目名称
					<input property="editor"  class="mini-textbox" style="width:100%;" minWidth="150" />
           			</div>
					<div field="value"  name="value" width="150" align="center" headerAlign="center" allowSort="false"  >金额(元)
					<input property="editor" class="mini-textbox" style="width:100%;" minWidth="150" />
           			</div>
                
				</div>
			</div>

			<div id="functionIds" style="text-align: center;">
				<a class="mini-button" style="display: none"  style="width: 150px;" id="save_btn"
					onclick="save">保存</a>
				<a class="mini-button" style="display: none"  style="width: 150px;" id="cancel"
					onclick="cancel">取消</a>
			</div>
		</div>  
<script type="text/javascript">
//获取当前tab
	mini.parse();
	var currTab = top["win"].tabs.getActiveTab();
	
	var params = currTab.params;
		//var dealNo = params.dealNo;
	var action =params.action;
	var grid = mini.get("Ifrs9ManualAdjustmentEdit");
	
	var field_form = new mini.Form("field_form");
	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex;
		var pageSize = e.data.pageSize;
		search(pageSize, pageIndex);
	});
	$(document).ready(function(){
		if($.inArray(action,['detail','approve','edit'])>-1)
			init();
		if($.inArray(action,['detail','approve'])>-1){
			field_form.setEnabled(false);
			$("#buttons").hide();
			$("#functionIds").hide();
		}
		if($.inArray(action,['add','edit'])>-1)
			mini.get("note").setEnabled(true);
	})
	//渲染数据
	function init() {
		search(grid.pageSize, 0);
	}

	function search(pageSize, pageIndex) {
		
		var data = {};      //获取表单数据
		data['pageNumber'] = pageIndex + 1;
		data['pageSize'] = 99999999;
		
		var row=params.selectedData;
		flowId =row.flowId;
		if(CommonUtil.isNotNull(CommonUtil.isNotNull(flowId)))
		{
			
			data['flowId'] = flowId;
			
			var param = mini.encode(data);   //序列化json格式
			CommonUtil.ajax({
				url:"/TbManualEntryController/selectManualEntryVo",
				data: param,
				callback: function (data) {
					var grid = mini.get("Ifrs9ManualAdjustmentEdit");
					//设置分页
					
					if(data != null){
						grid.setData(data.constact_list);
						field_form.setData(data);
											
						var sponInst=mini.get("sponInst");
						sponInst.setValue(data.sponInst);
						sponInst.setText(data.inst_fullname);
					}
					
					
				}
			});
			
		}
		
	}
	
	
	function save(){
	
		//表单验证
		field_form.validate();
		if (field_form.isValid() == false) {
			return;
		}
		
		var conObj=grid.getData();
		if(conObj.length==0){
			mini.alert("请添加借贷","系统提示");
			return false;
		}
		for(var i = 0 ;i<conObj.length; i++){
			
			var v =typeof(conObj[i].debitCreditFlag);
			if(typeof(conObj[i].debitCreditFlag)== "undefined"){
				
				mini.alert("收付方向不能为空","系统提示");
					return false;
			}
			if(typeof(conObj[i].subjCode)== "undefined" ){
				mini.alert("科目不能为空","系统提示");
					return false;
			}
			if(typeof(conObj[i].value)== "undefined" ){
				mini.alert("金额不能为空","系统提示");
					return false;
			}
	
			
		}
		var total1=0;
		var total2=0;
		
		for(var i = 0 ;i<conObj.length; i++){
			if(conObj[i].debitCreditFlag == "C"){
					total1=total1 + parseFloat(conObj[i].value);
			}
			if(conObj[i].debitCreditFlag == "D"){
					total2=total2 + parseFloat(conObj[i].value);
			}
		}
		if(total1 != total2){
			mini.alert("借贷不平衡","系统提示");
			return false;
		}
		var cpObj=field_form.getData(true);
		cpObj.constact_list = conObj;	
		var row=params.selectedData;
		cpObj['flowId'] = "";
		
		if(action=="edit"){//当修改的时候才获取
		if(row){
		flowId =row.flowId;
		cpObj['flowId'] = flowId;
		}
		}
		CommonUtil.ajax({
			url:"/TbManualEntryController/addEntryList",
			data:cpObj,
			callback : function(data) {
				
				if (data.code == 'error.common.0000') {
					mini.alert("保存成功","系统提示",function(){setTimeout(function(){top["win"].closeMenuTab()},10);});
				} else {
					mini.alert("保存失败","系统提示");
				}
				
			}
		});
		
		
	}

 //选择
 function onInsQuery(e) {
	var btnEdit = this;
	mini.open({
		url : CommonUtil.baseWebPath() +"InterBank/mini_system/MiniInstitutionSelectManages.jsp",
		title : "机构选择",
		width : 900,
		height : 600,
		ondestroy : function(action) {	
			if (action == "ok") 
			{
				var iframe = this.getIFrameEl();
				var data = iframe.contentWindow.GetData();
				data = mini.clone(data); //必须
				if (data) {
					btnEdit.setValue(data.instId);
					btnEdit.setText(data.instName);
					btnEdit.focus();
				}
			}

		}
	});
	}
      //弹窗科目选择
      function onSubjCodeQuery(e) {
          var btnEdit = this;
          var currentRow = grid.getSelected();
          mini.open({
              url: CommonUtil.baseWebPath()+"ifrs9/accounting/SubjectSelectManages.jsp",
              showMaxButton:false,
              title: "选择树",
              width: 590,
              height: 400,
              ondestroy: function (action) {
                  if (action == "ok") {
                      var iframe = this.getIFrameEl();
                      var data = iframe.contentWindow.GetData();
                      data = mini.clone(data);    
                      if (data) {
                      	
                      	grid.updateRow(currentRow,{subjName:data.subjName})
                      	btnEdit.setValue(data.subjCode);
                          btnEdit.setText(data.subjCode);
                          
                          btnEdit.focus();
                      }
                  }
              }
          });
      } 
 //添加记录
	function add(){
	                  
		var newRow = { name: "New Row" };
	     grid.addRow(newRow, 0);
	     grid.setSelected(newRow);
	     grid.beginEditCell(newRow, "subjCode");
	     
	  			
	}
	function cancel(){
		top["win"].closeMenuTab();
	}
	//清空
	function clear(){
		field_form.clear();
	}
//删除
	function del(){
		var grid=mini.get('Ifrs9ManualAdjustmentEdit');
		var row=grid.getSelected();
		if(row){
		mini.confirm("确定删除该条数据？","系统提示",function(action){
			if(action=='ok'){
				grid.removeRow(row);
			}
		})
		}else{
			mini.alert("请选择一条数据","系统提示");
		}
	}


</script>
</body>

</html>