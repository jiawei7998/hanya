<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
	<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
    <title></title>
  </head>
  
<body style="width:100%;height:100%;background:white">
	<div class="mini-panel" title="手工调账" style="width:100%;">
	<div style="width:100%;padding-top:10px;">
		<div id="search_form" class="mini-form" width="80%">
			<input id="subjCode" name="subjCode" class="mini-textbox" vtype="maxLength:50" labelField="true"  label="科目号：" labelStyle="text-align:right;"  emptyText="请输入科目号"/>
			<input id="subjName" name="subjName" class="mini-textbox" labelField="true"  label="科目名称：" labelStyle="text-align:right;" emptyText="请输入科目名称"/>
			<input id="bkkpgOrgId" name="bkkpgOrgId" class="mini-buttonedit" onbuttonclick="onInsQuery" labelField="true"  label="机构：" labelStyle="text-align:right;" allowInput="false" emptyText="请选择机构"/>
			<input id="eventType" type="hidden" value="9015">
			<span style="float:right;margin-right: 150px">
				<a id="search_btn" class="mini-button" style="display: none"  onclick="query">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"  onclick="clear">清空</a>
			</span>
		</div>
	</div>

	<table width="100%">
		<tr>
			<td width="60%" style="padding:10px 0 5px 5px">
					<a id="add_btn" class="mini-button" style="display: none"  onclick="add()">新增</a>
					<a id="edit_btn" class="mini-button" style="display: none"  onclick="edit()">修改</a>
					<a id="delete_btn" class="mini-button" style="display: none"  onclick="del()">删除</a>
					<a id="approve_commit_btn" class="mini-button" style="display: none"   onclick="approve()">审批</a>
					<!-- <a id="confirm_btn" class="mini-button" style="display: none"   >申购确认</a> -->
					<a id="approve_mine_commit_btn" class="mini-button" style="display: none"  onclick="commint()">提交预审</a>
					<a id="approve_log" class="mini-button" style="display: none"   onclick="approveLog()">审批日志</a>
					<a id="send_btn" class="mini-button" style="display: none"   onclick="sendCore()">发送核心</a>
					<a id="print_bk_btn" class="mini-button" style="display: none"    onclick="print()">打印</a>
			</td>

			<td width="40%">
				<div id = "approveType" name = "approveType" class="mini-checkboxlist" style="float:right;width:100%; " labelField="true" 
					label="审批列表选择：" labelStyle="text-align:right;" 
					value="approve" textField="text" valueField="id" multiSelect="false"
					data="[{id:'approve',text:'待审批列表'},{id:'finished',text:'已审批列表'},{id:'mine',text:'我发起的'}]" 
					onvaluechanged="checkBoxValuechanged">
				</div>
			</td>
		</tr>
	</table>
	</div>
	
	<div id="tradeManage" class="mini-fit">
		<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="dealNo" 
		allowAlternating="true" allowResize="true" onrowdblclick="onRowDblClick" border="true"  
		multiSelect="false" sortMode="client">
			<div property="columns">
<!-- 			    <div type="indexcolumn" headerAlign="center" width="50">序号</div> -->
				<div field="flowId" name="flowId"width="100" headerAlign="center" allowSort="true">账务流水号</div>				
				<div field="approveStatus" name="approveStatus" width="70" renderer="CommonUtil.dictRenderer" data-options="{dict:'ApproveStatus'}" align="center" headerAlign="center">审批状态</div>
				<!-- <div field="taskName" width="100" headerAlign="center" allowSort="true">当前环节</div>  -->
				<div field="flowSeq" width="100" headerAlign="center" align="center" allowSort="true">序号</div>   
				<div field="postDate" width="80" headerAlign="center" align="center" allowSort="true">账务日期</div>  
				<div field="dealNo" width="100" headerAlign="center" align="center" allowSort="true">交易流水号</div>				
				<!-- <div field="outDate" width="100" headerAlign="center" allowSort="true"renderer="CommonUtil.dictRenderer" data-options="{dict:'YesNo'}">是否逾期</div> --> 
				<div field="prdName" width="100" headerAlign="center" align="center" allowSort="true">产品类型</div>   
<!-- 				<div field="fundName" width="80" headerAlign="center" align="center" allowSort="true">基金名称</div> -->
				<div field="sponInst" visible="false" width="80" headerAlign="center" align="center" allowSort="true">交易机构</div>  
				<div field="inst_fullname"  width="100" headerAlign="center" align="center" allowSort="true">交易机构</div>  
				<div field="bkkpgOrgId" width="80" headerAlign="center" align="center" allowSort="true">记账机构</div>
				<div field="ccy" width="80" headerAlign="center" align="center" allowSort="true" renderer="CommonUtil.dictRenderer" data-options="{dict:'Currency'}">币种</div>
				<div field="debitCreditFlag" width="100" headerAlign="center" allowSort="true" renderer="CommonUtil.dictRenderer" data-options="{dict:'DebitCreditFlag'}">借贷标识</div>   
				<div field="subjCode" width="100" headerAlign="center" allowSort="true">科目号</div>
				<div field="subjName" width="100" headerAlign="center" align="center" allowSort="true">科目名称</div>
				<div field="value"  width="100" align="right"  headerAlign="center" allowSort="true">实际金额</div>
				<div field="sendFlag"  name="sendFlag" renderer="CommonUtil.dictRenderer" data-options="{dict:'EntrySendFlag'}" width="100" align="right"  headerAlign="center" allowSort="true">发送标志</div>
				<div field="updateTime" width="100" headerAlign="center" allowSort="true">更新日期</div>   
				
			</div>	
		</div>  
	</div>
	<script type="text/javascript">
	mini.parse();
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var prdNo='9015';
	var prdName='手工调账';
	var form=new mini.Form("#search_form");
	var grid=mini.get("datagrid");
	top["fundRddManager"] = window;
	function functionMain(url,prdName,title,details){
		var url = CommonUtil.baseWebPath() + url;
		var tab = {
			id : details,
			name : details,
			iconCls : "",
			title : prdName + title,
			url : url,
			showCloseButton:true
		};
		var paramData = {'operType':'A','closeFun':'query','pWinId':'fundRddManager',selectedData:grid.getSelected(),action:details};
		
		CommonUtil.openNewMenuTab(tab,paramData);
	}
	function  onRowDblClick(e) {
		var messageid = mini.loading("系统正在处理...", "请稍后");
		try {
		functionMain("/ifrs9/accounting/Ifrs9ManualAdjustmentEdit.jsp","手工调账","详情","detail");
		} catch (error) {
			
		}
		mini.hideMessageBox(messageid);
	}
	function add(){
		var messageid = mini.loading("系统正在处理...", "请稍后");
		try {
			functionMain("/ifrs9/accounting/Ifrs9ManualAdjustmentEdit.jsp","手工调账","新增","add");
		} catch (error) {
			
		}
		mini.hideMessageBox(messageid);
	};

	//修改
	function edit(){
		var messageid = mini.loading("系统正在处理...", "请稍后");
		try {
			functionMain("/ifrs9/accounting/Ifrs9ManualAdjustmentEdit.jsp","手工调账","修改","edit");
		} catch (error) {
			
		}
		mini.hideMessageBox(messageid);
	}
	//删除
	function del(){
		var rows=grid.getSelecteds();
		if(rows.length==0){
			mini.alert("请选中一行","提示");
			return;
		}
		mini.confirm("您确认要删除选中记录?","系统警告",function(value){   
			if (value=='ok'){   
				var data=rows[0];
				
				
				params=mini.encode(data);
				
				CommonUtil.ajax( {
					url:"/TbManualEntryController/deleteManualEntry",
					data:params,
					callback : function(data) {
						query();
		    			mini.alert("系统提示","删除成功.");

						}
					});
			}
		});
	}
  //清空
	function clear(){
			var form=new mini.Form("search_form");
			form.clear();
			query();
		}
		/* 按钮 查询事件 */
	function query(){
		
		search(grid.pageSize,0);
	}
	/* 查询 */
	function search(pageSize,pageIndex){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统也提示");
			return;
		}

		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['trdtype']=ApproveTrdType.FundTrdType.Rdd;//交易操作类型
		data['dealType']='1'
		
		var approveType = mini.get("approveType").getValue();
		if(approveType == "mine"){//我发起的
			data['approveType']=1;
			url ="/TbManualEntryController/searchPageTbkYearEntryByMyself";
		}else if(approveType == "approve"){//待审批
			data['approveType']=2;
			url  ="/TbManualEntryController/searchPageTbkYearEntryUnfinishedHand";
		}else{//已审批
			data['approveType']=3;
			url ="/TbManualEntryController/searchPageTbkYearEntryFinished";
		}
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:url,
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
				
				grid.mergeColumns(["flowId"]);
			}
		});
	}
	function sendCore(){
		var row = grid.getSelected();
		if(!row){
			mini.alert("请选择一条记录","系统提示");
			return;
		}
		mini.confirm("您确认要发送核心吗?","系统提示",function(action){
			if(action=="ok"){
				CommonUtil.ajax({
					url:url,
					data:{flowId:row.flowId,dealNo:row.dealNo},
					callback : function(data) {
						grid.setTotalCount(data.obj.total);
						grid.setPageIndex(pageIndex);
				        grid.setPageSize(pageSize);
						grid.setData(data.obj.rows);
						
						grid.mergeColumns(["flowId"]);
					}
				});
			}
		})
	}	
	function onInsQuery(e) {
		var btnEdit = this;
		mini.open({
			url : CommonUtil.baseWebPath() +"InterBank/mini_system/MiniInstitutionSelectManages.jsp",
			title : "机构选择",
			width : 900,
			height : 500,
			ondestroy : function(action) {	
				if (action == "ok") 
				{
					var iframe = this.getIFrameEl();
					var data = iframe.contentWindow.GetData();
					data = mini.clone(data); //必须
					if (data) {
						btnEdit.setValue(data.instId);
						btnEdit.setText(data.instName);
						btnEdit.focus();
					}
				}

			}
		});
	}
	

	grid.on("beforeload", function (e) {
		e.cancel = true;
		
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
		
	});
	function checkBoxValuechanged(e){
		search(10,0);
		var approveType=this.getValue();
		if(approveType == "approve"){//待审批
			grid.hideColumn("sendFlag");
			initButton();
		}else if(approveType == "finished"){//已审批
			mini.get("add_btn").setVisible(false);
			mini.get("edit_btn").setVisible(false);
			mini.get("delete_btn").setVisible(false);
			mini.get("approve_commit_btn").setVisible(false);
			mini.get("approve_mine_commit_btn").setVisible(false);
			mini.get("approve_log").setVisible(true);//审批日志
			mini.get("approve_log").setEnabled(true);//审批日志高亮
			mini.get("print_bk_btn").setVisible(true);//打印
			mini.get("print_bk_btn").setEnabled(true);//打印高亮
			mini.get("send_btn").setEnabled(true);
			mini.get("send_btn").setVisible(false);
			grid.hideColumn("sendFlag");
			/* mini.get("confirm_btn").setVisible(false); */
		}else{//我发起的
			mini.get("add_btn").setVisible(true);
			mini.get("add_btn").setEnabled(true);
			mini.get("edit_btn").setVisible(true);
			mini.get("edit_btn").setEnabled(true);
			mini.get("delete_btn").setVisible(true);
			mini.get("delete_btn").setEnabled(true);
			mini.get("approve_commit_btn").setVisible(false);
			mini.get("approve_mine_commit_btn").setVisible(true);//提交审批显示
			mini.get("approve_mine_commit_btn").setEnabled(true);//提交审批高亮
			mini.get("approve_log").setVisible(true);//审批日志
			mini.get("approve_log").setEnabled(true);//审批日志高亮
			mini.get("print_bk_btn").setVisible(true);//打印
			mini.get("print_bk_btn").setEnabled(true);//打印高亮
			mini.get("send_btn").setEnabled(true);
			mini.get("send_btn").setVisible(true);
			/* mini.get("confirm_btn").setVisible(true); */
		}
	}
	function initButton(){
		mini.get("add_btn").setVisible(false);
		mini.get("edit_btn").setVisible(false);
		mini.get("delete_btn").setVisible(false);
		mini.get("approve_commit_btn").setVisible(true);//审批
		mini.get("approve_commit_btn").setEnabled(true);//审批高亮
		mini.get("approve_mine_commit_btn").setVisible(false);//提交审批
		mini.get("approve_log").setVisible(true);//审批日志
		mini.get("approve_log").setEnabled(true);//审批日志高亮
		mini.get("print_bk_btn").setVisible(true);//打印
		mini.get("print_bk_btn").setEnabled(true);//打印高亮
		mini.get("send_btn").setEnabled(true);
		mini.get("send_btn").setVisible(false);
		/* mini.get("confirm_btn").setVisible(false);
		mini.get("confirm_btn").setEnabled(false); */
	}
	grid.on("select",function(e){
		var row=e.record;
		mini.get("approve_mine_commit_btn").setEnabled(false);
		mini.get("approve_commit_btn").setEnabled(false);
		mini.get("edit_btn").setEnabled(false);
		mini.get("delete_btn").setEnabled(false);
		mini.get("send_btn").setEnabled(false);
		/* mini.get("confirm_btn").setEnabled(false); */
		if(row.approveStatus == "3"){//新建
			mini.get("approve_mine_commit_btn").setEnabled(true);
			mini.get("approve_commit_btn").setEnabled(false);
			mini.get("edit_btn").setEnabled(true);
			mini.get("delete_btn").setEnabled(true);
			mini.get("approve_log").setEnabled(false);
		}
		if( row.approveStatus == "6" || row.approveStatus == "5"){//审批通过：6   审批中:5
			mini.get("approve_mine_commit_btn").setEnabled(false);
			mini.get("approve_commit_btn").setEnabled(true);
			mini.get("edit_btn").setEnabled(false);
			mini.get("delete_btn").setEnabled(false);
		}
		if(row.approveStatus != null && row.approveStatus != "3") {
			mini.get("approve_log").setEnabled(true);
		}
		if(row.approveStatus == "6" && row.sendFlag !='1' ){
			mini.get("send_btn").setEnabled(true); 
		}
	});

	/*****************************审批相关操作*******************************/
	//审批日志查看
	function appLog(selections){
		var flow_type = Approve.FlowType.VerifyApproveFlow;
		if(selections.length <= 0){
			mini.alert("请选择要操作的数据","系统提示");
			return;
		}
		if(selections.length > 1){
			mini.alert("系统不支持多笔操作","系统提示");
			return;
		}
		if(selections[0].tradeSource == "3"){
			mini.alert("初始化导入的业务没有审批日志","系统提示");
			return false;
		}
		Approve.approveLog(flow_type,selections[0].flowId);
	};
	//提交正式审批、待审批
	function verify(selections){
		if(selections.length == 0){
			mini.alert("请选中一条记录！","消息提示");
			return false;
		}else if(selections.length > 1){
			mini.alert("暂不支持多笔提交","系统提示");
			return false;
		}				
		
		if(selections[0]["approveStatus"] == "3" ){
			Approve.approveCommit(Approve.FlowType.ManualAdjFlow,selections[0]["flowId"],Approve.OrderStatus.New,"tbManualEntryService",prdNo,function(){
			search(grid.pageSize,grid.pageIndex);
			});
		}else{
			functionMain("/ifrs9/accounting/Ifrs9ManualAdjustmentApprove.jsp","手工调账","审批","approve");
// 			functionMain("/ifrs9/accounting/Ifrs9ManualAdjustmentEdit.jsp","手工调账","修改","edit");
		}
	};
	//审批
	function approve(){
		mini.get("approve_commit_btn").setEnabled(false);
		var messageid = mini.loading("系统正在处理...", "请稍后");
		try {
			verify(grid.getSelecteds());
		} catch (error) {
			
		}
		mini.hideMessageBox(messageid);	
		mini.get("approve_commit_btn").setEnabled(true);
	};
	//提交审批
	function commint(){
		mini.get("approve_mine_commit_btn").setEnabled(false);
		var messageid = mini.loading("系统正在处理...", "请稍后");
		try {
			verify(grid.getSelecteds());
		} catch (error) {
			
		}
		mini.hideMessageBox(messageid);	
		mini.get("approve_mine_commit_btn").setEnabled(true);
	};
	//审批日志
	function approveLog(){
		appLog(grid.getSelecteds());
	};
	$(document).ready(function(){
		search(10,0);
		initButton();
	})

	</script>
</body>
</html>