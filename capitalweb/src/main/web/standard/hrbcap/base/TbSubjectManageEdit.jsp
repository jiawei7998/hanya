<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
    <%@ include file="../../global.jsp"%>
        <html>

        <head>
            <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
            <title></title>
        </head>
            <style>
            .mini-labelfield-label{
left:30px;
}
table th{
text-align:left;
padding-left:10px;
width:120px;
   border-right: 0;
   }
   .header {
   height: 40px;
   /* background: #95B8E7; */
   color: #404040;
   background: linear-gradient(to bottom,#EFF5FF 0,#E0ECFF 100%);
   background-repeat: repeat-x;
   line-height:40px; /*设置line-height与父级元素的height相等*/
   text-align: center; /*设置文本水平居中*/
   font-size:14px;
   margin-top: -16px;
}
</style>

        <body style="width:100%;height:100%;background:white">
        <div class="header"  height="55" showSplit="false" showHeader="true">
<span id="labell"><h3 >科目代码信息维护-修改</h3></span>
</div>
   <div id="form1">
       <div id="MiniInstitutionAddEdits" class="fieldset-body">
           <table id="field_form" style="text-align:left;margin:auto;width:100%;" class="mini-sltable">
               <tr>
                   <td style="width:50%">
                       <input id="subjName" name="subjName" requiredErrorText="该输入项为必输项" class="mini-textbox"  labelField="true"
                           label="科目名称：" emptyText="请输入科目名称" style="width:100%" labelStyle="text-align:left;width:170px"
                           vtype="maxLength:50" required="true" />
                   </td>
                   <td style="width:50%">
                       <input id="subjCode" name="subjCode" class="mini-textbox" requiredErrorText="该输入项为必输项" vtype="rangeLength:1,125" labelField="true"
                           label="科目号：" emptyText="请输入科目号" style="width:100%" labelStyle="text-align:left;width:170px"
                           vtype="maxLength:20" required="true"  onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" />
                   </td>
               </tr>
             <!--   <tr>
                   <td style="width:50%">
                       <input id="subjType" name="subjType" class="mini-ComboBox" requiredErrorText="该输入项为必输项"  data="CommonUtil.serverData.dictionary.subjType"
                           labelField="true" label="科目类型：" emptyText="请输入科目类型" style="width:100%" labelStyle="text-align:left;width:170px"
                       />
                   </td>
                   <td style="width:50%">
                       <input id="subjNature" name="subjNature" class="mini-textbox"  labelField="true" label="科目性质："
                           emptyText="请输入科目性质" style="width:100%" labelStyle="text-align:left;width:170px" />
                   </td>
               </tr> -->
               <tr>
                   <td style="width:50%">
                       <input id="debitCredit" name="debitCredit" class="mini-textbox" vtype="" requiredErrorText="该输入项为必输项"
                           emptyText="请输入余额方向..." labelField="true" label="余额方向：" style="width:100%" labelStyle="text-align:left;width:170px"
                       />
                   </td>
                   <td style="width:50%">
                       <input id="payRecive" name="payRecive" class="mini-textbox" vtype="" requiredErrorText="该输入项为必输项" 
                           emptyText="请输入余额收付方向..." labelField="true" label="余额收付方向："  style="width:100%"
                           labelStyle="text-align:left;width:170px" />
                   </td>
               </tr>
               <tr>
                   <!-- <td style="width:50%">
			<input id="instType" name="instType" class="mini-ComboBox" vtype="" requiredErrorText="该输入项为必输项" data="CommonUtil.serverData.dictionary.InstitutionType"
			  emptyText="请选择上级科目..." labelField="true" label="上级科目：" style="width:100%" labelStyle="text-align:left;width:170px"/>
				</td> -->
                   <!-- <td style="width:50%">
                       <input id="parentSubjCode" name="parentSubjCode" class="mini-treeselect"  multiSelect="false" 
                            parentField="parentSubjCode" checkRecursive="true" showFolderCheckBox="false" valueField="subjCode" textField="subjName"
                           expandOnLoad="true" oncloseclick="onCloseClick"  labelField="true"
                           label="上级科目：" style="width:100%" labelStyle="text-align:left;width:170px" />
                   </td> -->

                 <!--   <td style="width:50%">											
                       <input id="parentSubjCode" name="parentSubjCode" class="mini-buttonedit searchbox" onbuttonclick="onInsQuery"  labelField="true" label="上级科目："
                        emptyText="请选择上级科目..." style="width:100%" labelStyle="text-align:left;width:170px"
                        allowinput='false'/>
                   </td> -->
               </tr>
               <tr>
                   <td colspan="3" style="text-align:center;">
                       <a id="save_btn" class="mini-button" style="display: none"  onclick="add">保存</a>
                       <a id="cancel_btn" class="mini-button" style="display: none"  onclick="close">取消</a>
                   </td>
               </tr>
           </table>
       </div>
   </div>
   <script type="text/javascript">
 		mini.parse();
		var url = window.location.search;
		var action = CommonUtil.getParam(url, "action");
		var param = top["TbSubjectManage"].getData(action);
		var form = new mini.Form("#field_form");

        function add() {
            form.validate();
            if (form.isValid() == false){
                mini.alert("信息填写有误，请重新填写!", "消息提示")
                return;
            }
            //提交数据
            var saveUrl = $.inArray(action, ["add"]) > -1 ? "/TbSubjectDefController/saveTbSubjectDef" : "/TbSubjectDefController/updateTbSubjectDef";
            var data = form.getData(true); //获取表单多个控件的数据  
            var param = mini.encode(data); //序列化成JSON
            CommonUtil.ajax({
                url: saveUrl,
                   data: param,
                callback: function (data) {
                    if (data.code == 'error.common.0000') {
                        mini.alert("保存成功", '提示信息', function () {
                        	top['TbSubjectManage'].clear();
                            setTimeout(function(){top["win"].closeMenuTab('ok')},10);
                        });
                    } else {
                        mini.alert("保存成功");
                    }
                }
            });
        }

        function initform() {
            if (action == "edit") {
                var form = new mini.Form("#field_form");
                mini.get("subjCode").setEnabled(false);
                form.setData(param);
                mini.get("parentSubjCode").setValue(param.parentSubjCode);
                //mini.get("parentSubjCode").setdata(param.parentSubjCode);
                mini.get("parentSubjCode").setText(param.parentSubjName);
            } else if (action == "add") {
            	$("#labell").html("<h3 >科目代码信息维护-新增</h3>");
            }
        }
		            // 查询科目的方法
		function onInsQuery(e) {
			var btnEdit = this;
			mini.open({
				url: CommonUtil.baseWebPath()+"ifrs9/accounting/SubjectSelectManages.jsp",
				title: "科目选择",
				width: 700,
				height: 600,
				ondestroy: function (action) {
					if (action == "ok") {
						var iframe = this.getIFrameEl();
						var data = iframe.contentWindow.GetData();
						data = mini.clone(data); //必须
						if (data) {
							btnEdit.setValue(data.subjCode);
							btnEdit.setText(data.subjName);
							btnEdit.focus();
						}
					}
		
				}
			});
		}

        function close() {
            top["win"].closeMenuTab();
        }


        $(document).ready(function () {
            initform();
        })
	</script>
</body>
 </html>