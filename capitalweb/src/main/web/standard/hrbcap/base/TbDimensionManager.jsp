<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
	<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
    <title></title>
    <style>
	table th{
	text-align:left;
	padding-left:10px;
	width:120px;
    border-right: 0;
    }
    .header {
    height: 40px;
    /* background: #95B8E7; */
    color: #404040;
    background: linear-gradient(to bottom,#EFF5FF 0,#E0ECFF 100%);
    background-repeat: repeat-x;
    line-height:40px; /*设置line-height与父级元素的height相等*/
    text-align: center; /*设置文本水平居中*/
    font-size:14px;
	}
	</style>
  </head>
<body style="width:100%;height:100%;background:white">
<div class="header" region="north" height="45" showSplit="false" showHeader="false">
<div id="JobManage">
    <div id="search_form" class="mini-form" >
		<input class="mini-textbox" id="dimensionId" name="dimensionId" labelField="true" label="维度编号：" labelStyle="text-align:right;" emptyText="请输入维度编号" >
		<input class="mini-textbox" id="dimensionName" name="dimensionName" labelField="true" label="维度名称：" labelStyle="text-align:right;" emptyText="请输入维度名称" >
		<span style="float:right;margin-right: 150px">
			<a  id="search_btn" class="mini-button" style="display: none"   onclick="search(10,0)">查询</a>
			<a  id="clear_btn" class="mini-button" style="display: none"   onclick='clear()'>清空</a>
			<a id="add_btn" class="mini-button" style="display: none"  onclick="addJob()"  >新增</a>
			<a id="edit_btn" class="mini-button" style="display: none"  onclick="editJob()"  >修改</a>
			<a id="delete_btn" onclick="deleteJob()" class="mini-button" style="display: none"   >删除</a>
		</span>		
	</div>  
</div>
</div>
<div class="mini-fit">
	<div id="datagrid" class="mini-datagrid borderAll" style="height:98%;" allowResize="true" >
		<div property="columns">
			<div type="indexcolumn" width="50" headerAlign="center" align="center">序号</div>
			<div field="dbId" width="60" headerAlign="center" align="center">DbID</div>    
		    <div field="dimensionId" width="60" headerAlign="center" align="center">维度ID</div>                      
			<div field="dimensionSort" width="60"  headerAlign="center" align="center" >维度排序</div>
			<div field="dimensionName"   headerAlign="center" align="center" >维度名称</div>                                
			<div field="isActive" width="100"  headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'YesNo'}" >是否启用</div>
			<div field="remark"   headerAlign="center" align="center" >备注</div>   
		</div>
	</div>
</div>
<script type="text/javascript">
	mini.parse();
	top['TbDimensionManager']=window;
	var grid=mini.get("datagrid");
	  $(document).ready(function () {
	        grid.on("beforeload", function (e) {
	            e.cancel = true;
	            var pageIndex = e.data.pageIndex;
	            var pageSize = e.data.pageSize;
	            search(pageSize, pageIndex);
	        });
	        search(grid.pageSize, grid.pageIndex);
	    })
	function search(pageSize,pageIndex){
		var form=new mini.Form("#search_form");
		form.validate();
		if(form.isValid==false) return;
		var data=form.getData();
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		var params=mini.encode(data);
		CommonUtil.ajax( {
			url:"/TbDimensionController/searchTbDimensionPage",
			data:params,
			callback : function(data) {
				
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
				grid.setPageSize(pageSize);
				grid.setData(data.obj.list);
			}
		});
		
	}
	// 传递参数
	function getData(){
		var grid=mini.get("datagrid");
		var row = grid.getSelected();
		return row;
	}
	function query(){
		search(10,0);
	}
	//清空
	function clear(){
		var form=new mini.Form("#search_form");
		form.clear();
	}
	function functionMain(prdNo,url,prdName,title,details){
		var url = CommonUtil.baseWebPath() + url;
		var tab = {
			id : prdNo + "_"+details,
			name : prdNo + "_"+details,
			iconCls : "",
			title : prdName + title,
			url : url,
			showCloseButton:true
		};
		var paramData = {'operType':'A','closeFun':'query','pWinId':'TbDimensionManager',selectedData:grid.getSelected(),action:details};
		
		CommonUtil.openNewMenuTab(tab,paramData);
	}
	//添加
	function addJob(){
		try {
			functionMain('TbDimension',"/../ifrs9/base/TbDimensionEdit.jsp","维度码表","新增","add");
		} catch (error) {
		}
	}
	//修改
	function editJob(){
		var grid=mini.get("datagrid");
		var row = grid.getSelected();
		if(row){
			try {
				functionMain('TbDimension',"/../ifrs9/base/TbDimensionEdit.jsp","维度码表","修改","edit");
			} catch (error) {
			}
		} else {
			mini.alert("请选中一条记录！","消息提示");
		}
	}
	//删除
	function deleteJob(){
		var grid=mini.get("datagrid");
		var rows=grid.getSelecteds();
		if(rows.length==0){
			mini.alert("请选中一行","提示");
			return;
		}var data=rows[0];
		params=mini.encode(data);
		mini.confirm("您确认要删除选中记录？","确认", function(r){
			if (r=="ok"){
				CommonUtil.ajax( {
					url:"/TbDimensionController/deleteTbDimension",
					data:params,
					callback : function(data) {
						mini.alert("删除成功","系统提示");
						search(10,0);
					}
				});	
			}
		});
	}

	$(document).ready(function(){
		search(10,0);
	})
</script>
</body>
</html>