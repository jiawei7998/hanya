<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<style>
table th{
text-align:left;
padding-left:10px;
width:120px;
   border-right: 0;
   }
   .header {
   height: 40px;
   color: #404040;
   background: linear-gradient(to bottom,#EFF5FF 0,#E0ECFF 100%);
   background-repeat: repeat-x;
   line-height:40px; /*设置line-height与父级元素的height相等*/
   text-align: center; /*设置文本水平居中*/
   font-size:14px;
}
</style>
<body style="width:100%;height:100%;background:white">
    <div id="TbAmountManageGrid">
    	<div class="header"  height="55" showSplit="false" showHeader="true">
        <input id="amountId" name="amountId" class="mini-textbox" labelField="true" label="金额代码：" labelStyle="text-align:right;" emptyText="请输入金额代码" />
        <span style="float:right;margin-right: 150px">
            <a class="mini-button" style="display: none"  id="search"  onclick="query()">查询</a>
            <a class="mini-button" style="display: none"  id="clear_btn"  onclick="clear()">清空</a>
            <a class="mini-button" style="display: none"  id="add"  onClick="add();">新增</a>
        	<a class="mini-button" style="display: none"  id="modify"  onClick="modify();">修改</a>
        	<a class="mini-button" style="display: none"  id="delete"  onClick="deleteAmount();">删除</a>
        </span>
    </div>
</div>
    <div class="mini-fit" style="width:100%;height:100%;">
        <div class="mini-datagrid borderAll" style="width:100%;height:98%;" sortMode="client" allowAlternating="true"   idField="amountId" id="grid"  >
            <div property="columns">
                <div type="indexcolumn" headerAlign="center" width="50">序号</div>
				<div field="amountId" width="160" headerAlign="center" align="center" allowSort="true">金额代码</div>
				<div field="amountName" width="100" headerAlign="center" align="center" allowSort="true">金额名称</div>
				<div field="amountFormula" width="200" headerAlign="center" align="center" allowSort="true">金额公式</div>
				<div field="amountDesc" width="220" headerAlign="center" align="center" allowSort="true">金额描述</div>
            </div>
        </div>
    </div>
</body>
<script>
    mini.parse();
    var grid = mini.get("grid");
    $(document).ready(function () {
        grid.on("beforeload", function (e) {
            e.cancel = true;
            var pageIndex = e.data.pageIndex;
            var pageSize = e.data.pageSize;
            search(pageSize, pageIndex);
        });
        search(grid.pageSize, grid.pageIndex);
    })
       
    top['TbAmountManager'] = window;
		function functionMain(prdNo,url,prdName,title,details){
			var url = CommonUtil.baseWebPath() + url;
			var tab = {
				id : prdNo + "_"+details,
				name : prdNo + "_"+details,
				iconCls : "",
				title : prdName + title,
				url : url,
				showCloseButton:true
			};
			var paramData = {'operType':'A','closeFun':'query','pWinId':'TbAmountManager',selectedData:grid.getSelected(),action:details};
			
			CommonUtil.openNewMenuTab(tab,paramData);
		}
        
    //用户的修改
    function modify() {
		try {
			functionMain('TbAmount',"/../ifrs9/base/TbAmountEdit.jsp","会计金额","修改","edit");
		} catch (error) {
		}
    }
    
    //用户的删除
    function deleteAmount() {
        var record = grid.getSelecteds();
        if (record) {
            var data=record[0];
			params=mini.encode(data);
            mini.confirm("您确定要删除<span style='color:#FF3333;font-weight:bold;'>" + data.amountName + "</span>吗?", "系统提示", function (r) {
                if (r == 'ok') {
                    CommonUtil.ajax({
                        url: "/TbAmountController/deleteAmount",
                        data: params,
                        callback: function (data) {
                            if (data.code == 'error.common.0000') {
                            	mini.alert("删除成功.","系统提示");
                                search(10, 0);
                            } else {
                                mini.alert("删除失败", "系统提示");
                            }
                        }
                    });
                }
            })
        } else {
            mini.alert("请选择要删除的记录", "系统提示");
        }
    }
        
    //搜索
    function search(pageSize, pageIndex) {
        var form = new mini.Form("TbAmountManageGrid");
        var data = form.getData();
        form.validate();
        if(form.isValid()==false){
            return;
        }
        data.branchId =branchId;
        data.pageNumber = pageIndex + 1;
        data.pageSize = pageSize;
        var param = mini.encode(data); //序列化成JSON
        CommonUtil.ajax({
            url: "/TbAmountController/searchAmountPointerPage",
            data: param,
            callback: function (data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.list);
            }
        });
    }

    //新增用户
    function add() {
    	var messageid = mini.loading("系统正在处理...", "请稍后");
		try {
			functionMain('TbAmount',"/../ifrs9/base/TbAmountEdit.jsp","会计金额","新增","add");
		} catch (error) {
		}
		mini.hideMessageBox(messageid);
        }
    
    function clear() {
        var form = new mini.Form("#TbAmountManageGrid");
        form.clear();
        query();
        }
    
    function query(){
		search(grid.pageSize,0);
	}
    </script>