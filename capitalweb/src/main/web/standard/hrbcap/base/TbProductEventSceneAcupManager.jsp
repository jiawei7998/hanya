<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
	<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
    <title></title>
    <style>
		table th{
		    border-top:2px solid #428bca;
		    text-align:left;
			padding-left:10px;
			border: solid 1px #d9d9d9;
			border-right: 0;
			background-color: #f3f3f3;
		}
  </style>
</head>
<body style="width:100%;height:99%;background:white">
<div class="mini-splitter" style="width:100%;height:100%;">
    <div size="260" showCollapseButton="true" >
    <div id="panel1" class="mini-panel" title="场景列表（产品-事件-维度）" style="width:100%;height:100%;"collapseOnTitleClick="true">
        <div class="mini-fit" >
            <ul id="productTree" class="mini-tree" expandOnLoad="true" style="width:100%;"
                showTreeIcon="true" textField="pValueTxt" idField="groupId" parentField="" resultAsTree="false" >        
                <input id="branchId" class="mini-hidden" name="branchId" value="<%=__sessionUser.getBranchId()%>" />
            </ul>
        </div>
     </div>
    </div>
    <div showCollapseButton="true">
    		<div id="panel1" class="mini-panel" title="场景科目列表" style="width:100%;height:100%;"  allowResize="false" collapseOnTitleClick="false">
    			<div id="panel1" class="mini-panel" title="已分配科目" style="width:48%;height:98%;float: left"  allowResize="false" collapseOnTitleClick="false">
		            <span style='text-align:left;padding-top:5px;padding-right:10px;display:inline-block;width:97%;' colspan="10">  
		               <a id="Add_Button"    class="mini-button" style="display: none"  onclick="openadd">增加</a>
					   <a id="Update_Button" class="mini-button" style="display: none"  onclick="openupdate">修改</a>
					 </span>
		        <div id="leftList" class="mini-datagrid" title="全部"  allowResize="true" showPager="false" multiSelect="true" style="width:97%;height:89%;float: left;margin-top:4px" >
		            <div property="columns">
		                <div type="checkcolumn"></div>
		                <div type="indexcolumn" headerAlign="center" align="center" width="30">序列</div>
		              <div field="subject" header="科目号" headerAlign="center" width="105"></div>
		                <div field="subjDesc" header="科目名称" headerAlign="center"  width="135"></div>
		                <div field="drcrInd" header="借贷标识" headerAlign="center"  width="60" renderer="CommonUtil.dictRenderer" data-options="{dict:'debitCreditFlag'}"></div>
		                <div field="subjAmt" header="金额表达式" headerAlign="center"  width="70"></div>
		                <div field="subjSeq" header="排序" headerAlign="center"  width="30"></div>
		            </div>
		        </div>
		     </div>
		        <div style="float: left;width: 8%;text-align:center; ">
		            <div style="margin-bottom:10px; margin-top:50px"><a class="mini-button" style="display: none"     onclick="add" style="width:50px;">&gt;</a></div>
		            <div style="margin-bottom:10px; "><a class="mini-button" style="display: none"     onclick="addAll" style="width:50px;">&gt;&gt;</a></div>
		            <div style="margin-bottom:10px; "><a class="mini-button" style="display: none"     onclick="removes" style="width:50px;">&lt;</a></div>
		            <div style="margin-bottom:10px; "><a class="mini-button" style="display: none"     onclick="removeAll" style="width:50px;">&lt;&lt;</a></div>
		            <div style="margin-bottom:10px; margin-top:50px"><a class="mini-button" style="display: none"     onclick="saveData" style="width:50px;">保存</a></div>
		        </div>
		      <div id="panel1" class="mini-panel" title="未分配科目" style="width:42%;height:98%;float: left"  allowResize="true" collapseOnTitleClick="false">
		            <span style="text-align:left;padding-top:5px;padding-right:10px;display:inline-block;width:97%;" colspan="10">  
		               <a id="Delete_Button"    class="mini-button" style="display: none"  onclick="deleteRow">删除</a>
					 </span>
		        <div id="rightList" class="mini-datagrid" style="width:98%;height:89%;float: left;margin-top:4px" showPager="false" multiSelect="true" allowResize="true" textField="subjDesc" valueField="subject" >
		            <div property="columns">
		                <div type="checkcolumn"></div>
		                <div type="indexcolumn" headerAlign="center" align="center" width="30">序列</div>
		                <div field="subject" header="科目号" headerAlign="center" width="105"></div>
		                <div field="subjDesc" header="科目名称" headerAlign="center"  width="135"></div>
		                <div field="drcrInd" header="借贷标识" headerAlign="center"  width="60" renderer="CommonUtil.dictRenderer" data-options="{dict:'debitCreditFlag'}"></div>
		                <div field="subjAmt" header="金额表达式" headerAlign="center"  width="70"></div>
		                <div field="subjSeq" header="排序" headerAlign="center"  width="30"></div>
		                
		            </div>
		        </div> 
		     </div>
		 </div>
    </div>      
</div>
    <!-- 弹窗 -->
    <div id="editWindow" class="mini-window" title="场景-科目详情" style="width:550px;" 
    showModal="true" allowResize="true" allowDrag="true">
    <div id="editform" class="form" >
        <input class="mini-hidden" name="id"/>
        <input class="mini-hidden"  type="hidden"  name="action"  id="action" />
        <table style="width:100%;">
            <tr>
                <td style="width:82px;">科目代码：</td>
                <td style="width:140px;"><input class="mini-buttonedit" onbuttonclick="onButtonEdit" name="subject" id="subject"  required="true" /></td>
                <td style="width:82px;">科目名称：</td>
                <td style="width:140px;"><input name="subjDesc"  id="subjDesc"class="mini-textbox" required="true"  /></td>
                </tr>
            <tr>
                <td style="width:82px;">借贷标识：</td>
                <td style="width:140px;"><input name="drcrInd" class="mini-combobox"  data = "CommonUtil.serverData.dictionary.debitCreditFlag"/></td>
                <td style="width:82px;">金额代码：</td>
                <td style="width:140px;"><input id='amount' name="subjAmt" textField="amountName" valueField="amountId"class="mini-combobox" /></td>
            </tr>
            <tr>
                <td style="text-align:right;padding-top:5px;padding-right:20px;" colspan="6">
                    <a id="Update_Button" class="mini-button" style="display: none" href="javascript:saveRow()">保存</a>
                    <a id="Cancel_Button" class="mini-button" style="display: none" href="javascript:cancelRow()">取消</a>
                </td>                
            </tr>
        </table>
    </div>
</div>
    
    <script type="text/javascript">
        mini.parse();
        var tree = mini.get("productTree");
        var branchId = "<%=__sessionUser.getBranchId()%>";
        var amo=mini.get("#amount");
        var editWindow = mini.get("editWindow");
        var action=mini.get("action");
        var aleftList=mini.get("leftList");
        var arightList=mini.get("rightList");
        //弹窗科目选择
        function onButtonEdit(e) {
            var btnEdit = this;
            var subjN=mini.get("subjDesc");
            mini.open({
                url: CommonUtil.baseWebPath()+"ifrs9/accounting/SubjectSelectManages.jsp",
                showMaxButton:false,
                title: "选择树",
                width: 590,
                height: 400,
                ondestroy: function (action) {
                    if (action == "ok") {
                        var iframe = this.getIFrameEl();
                        var data = iframe.contentWindow.GetData();
                        data = mini.clone(data);    //必须
                        if (data) {
                        	btnEdit.setValue(data.subjCode);
                            btnEdit.setText(data.subjCode);
                            subjN.setValue(data.subjName);
                            subjN.setText(data.subjName);
                        }
                    }
                }
            });
        }
      /******获得金额代码列表******/
        CommonUtil.ajax({
            url: "/TbAmountController/searchAmountPointerPage",
            data:{},
            callback: function (data) {
            	   var json=mini.decode(data);
            		amo.setData(json.obj.list);
            }
        });
        /******获得所有场景******/
        CommonUtil.ajax({
            url: "/TbProductEventSceneAcupController/getSceneResultList",
            data: { },
            callback: function (data) {
            	
            	  var dataArray = data.obj; /// List数组 信息
            	  var resultArray = new Array();
            	  for (var index = 0; index < dataArray.length; index++) {
            	    var element = dataArray[index];
            	    element.pValueTxt = element.dimensionDescs;
            	    if (resultArray.length === 0 || element.prdName != resultArray[resultArray.length - 1].prdName) {
            	      $str = JSON.stringify(element);//系列化对象
            	      newobj = JSON.parse($str);//还原 达到深拷贝效果,防止后面发生自己添加自己
            	      newobj.pValueTxt = newobj.prdName;// 显示父节点的内容 没有其他用途
            	      newobj.children = [];
            	      resultArray.push(newobj);
            	    }
            	    ////console.log(resultArray);
            	     var list = resultArray[resultArray.length - 1].children;
            	    if (list.length === 0 || element.eventName != list[list.length - 1].eventName) {
            	      $str = JSON.stringify(element);
            	      newobj = JSON.parse($str);
            	      newobj.pValueTxt = newobj.eventName;
            	      newobj.children = [];
            	      list.push(newobj);
            	    }
            	    list[list.length - 1].children.push(element);
            	  }
            	 
                 tree.setData(resultArray);
            }
        });
        tree.on('nodeclick',function(e){
            var node = e.node;
	        var isLeaf = e.isLeaf;
	        	if(isLeaf){
	        		
                    searchL(node.groupId);
                    searchR(node.groupId);
	        	}
        });
        $(document).ready(function(){
        	
        	searchR();
        });
        
        //查询
          function searchL(groupId){
              var data = {};
              data['groupId'] = groupId;
              var param = mini.encode(data);
              CommonUtil.ajax({
                  url: "/TbProductEventSceneAcupController/getSceneAcupListByGroupId",
                  data: {'groupId':groupId},
                  callback: function (data) {
                	  aleftList.setData(data.obj);
                  }
              });
          }
        
          function searchR(groupId){
        	  CommonUtil.ajax({
                  url: "/TbProductEventSceneAcupController/getSceneAcupListByGroupId",
                  data: {'groupId':groupId+'untied'},
                  callback: function (data) {
                  	arightList.setData(data.obj);
                  }
              });
          }
        //新增数据
       function openadd(){
          if(typeof(tree.getSelected())=="undefined"){
        		mini.alert("未选择关联场景科目", "消息提示");
      			return;
        	}
    	   editWindow.show();
    	   var form = new mini.Form("#editform");
           form.clear();
     	   action.setValue("new");
     	   mini.get("subject").setEnabled(true);
       }
        //修改数据
        function openupdate(){
           if(typeof(tree.getSelected())=="undefined"){
         		mini.alert("未选择关联场景", "消息提示");
       			return;
         	}
      	 var datas=aleftList.getSelecteds();
      	 if(datas.length>1){
      		mini.alert("请勿选择多条数据", "消息提示");
  			return;
      	 }
      	 if(datas.length<1){
      		mini.alert("请选择修改数据", "消息提示");
  			return;
      	 }
      	 editWindow.show();
     	 var form = new mini.Form("#editform");
     	 form.clear();
         form.setData(datas[0]);
         mini.get("subject").setValue(datas[0].subject);
         mini.get("subject").setText(datas[0].subject);
         mini.get("subject").setEnabled(false);
       	 action.setValue("updata");
        }
       //保存数据
     
      function saveRow(){
    	  var form = new mini.Form("#editform");
          var groupId=tree.getSelected().groupId;
          form.validate();
  		if (form.isValid() == false) {
  			mini.alert("信息填写有误，请重新填写!", "消息提示");
  			return;
  		}
  		 var data = form.getData(true); //获取表单多个控件的数据  
          data['groupId'] = groupId;
          var param = mini.encode(data); //序列化成JSON
          if(action.value=="new"){//新增数据
        	  CommonUtil.ajax({
                  url:"/TbProductEventSceneAcupController/insertTbProductEventSceneAcup",
                  data: param,
                  callback: function (data_1) {
                      if (data_1.code == 'error.common.0000') {
                          mini.alert("保存成功");
                          searchL(tree.getSelected().groupId);
                      } else {
                          mini.alert("保存失败");
                      }
                  }
              });
          }else if(action.value=="updata"){//修改数据
        	  CommonUtil.ajax({
                  url:"/TbProductEventSceneAcupController/updateTbProductEventSceneAcup",
                  data: param,
                  callback: function (data_1) {
                      if (data_1.code == 'error.common.0000') {
                          mini.alert("保存成功");
                          searchL(tree.getSelected().groupId);
                      } else {
                          mini.alert("保存失败");
       
                          searchL(tree.getSelected().groupId);
                      }
                  }
              });
          }
          editWindow.hide(); 
      }
      //取消操作
      function cancelRow() {
            editWindow.hide();
        }
      //删除
      function deleteRow(){
    	 var datas=arightList.getSelecteds();
       	 if(datas.length>1){
       		mini.alert("请勿选择多条数据", "消息提示");
   			return;
       	 }
       	 if(datas.length<1){
       		mini.alert("请选择删除数据", "消息提示");
   			return;
       	 }
       	 var param = mini.encode(datas[0]);
       	 CommonUtil.ajax({
       		url:"/TbProductEventSceneAcupController/deleteTbProductEventSceneAcup/",
             data: param,
             callback: function (data_1) {
                 if (data_1.code == 'error.common.0000') {
                     mini.alert("删除成功");
                     searchR(tree.getSelected().groupId);
                 } else {
                     mini.alert("删除失败");
                     searchR(tree.getSelected().groupId);
                 }
             }
         });
      }
       function add() {
           var items = aleftList.getSelecteds();
           aleftList.removeRows(items);
           arightList.addRows(items);
       }
       function addAll() {        
           var items = aleftList.getData();
           aleftList.removeRows(items);
           arightList.addRows(items);
       }
       function removes() {
           var items = arightList.getSelecteds();
           arightList.removeRows(items);
           aleftList.addRows(items);
       }
       function removeAll() {
           var items = arightList.getData();
           arightList.removeRows(items);
           aleftList.addRows(items);
       }
       function arrfor(alist,blist){//找出左右列表中相同的科目配置，返回位置下标数组
    	   var arry=[];
			  for(var i=0;i<alist.length;i++){
				  if(blist.length>0){
					  for(var j=0;j<blist.length;j++){
						  if((alist[i].subject==blist[j].subject)&&(alist[i].drcrInd==blist[j].drcrInd)&&(alist[i].subjAmt==blist[j].subjAmt)){
							  var a=0;
							  arry[a]=i;
							  a++;
						  }
		             }
		         }
		    }
			 return arry; 
         }
         
       //解绑，绑定  --保存
      function saveData() {
    	   if(typeof(tree.getSelected())=="undefined"){
       		mini.alert("未选择关联场景", "消息提示");
       	    searchR();
       	    addAll();
     		return;
        	}
          var saveObjL = {};
          var saveObj1 = {};
          var eventList=aleftList.getData();
          var Rlist=arightList.getData();
		  for(var i=0;i<eventList.length;i++){//检查已分配列表是否存在相同科目配置
			   for(var j=eventList.length-1;j>i;j--){
			     if((eventList[i].subject==eventList[j].subject) && (eventList[i].drcrInd==eventList[j].drcrInd) && (eventList[i].subjAmt==eventList[j].subjAmt)){
			      mini.alert("存在多条相同科目配置，不可操作", "消息提示");
			      searchL(tree.getSelected().groupId);
     		      searchR(tree.getSelected().groupId);
     		      return;
			     }
			  }
			}
			  for(var i=0;i<Rlist.length;i++){//检查未分配列表是否存在相同科目配置
			   for(var j=Rlist.length-1;j>i;j--){
			     if((Rlist[i].subject==Rlist[j].subject)&&(Rlist[i].drcrInd==Rlist[j].drcrInd)&&(Rlist[i].subjAmt==Rlist[j].subjAmt)){
			      mini.alert("存在多条相同科目配置，不可操作", "消息提示");
			      searchL(tree.getSelected().groupId);
     		      searchR(tree.getSelected().groupId);
     		      return;
			     }
			  }
			}
			var saveObjR={};
			var larry=arrfor(eventList,Rlist);
			  for(var i=0;i<eventList.length;i++){
				  if(larry.length>0){
					  for(var k=0;k<larry.length;k++){
						  if(i!=larry[k]){
							  var s={};
		                      s.subject=eventList[i].subject;
		                      s.subjDesc=eventList[i].subjDesc;
		                      s.drcrInd=eventList[i].drcrInd;
		                      s.subjAmt=eventList[i].subjAmt;
		                      s.groupId=eventList[i].groupId;
		                      saveObjL[i]=s;  
						  }
					  }
				  }else{
					  var s={};
                      s.subject=eventList[i].subject;
                      s.subjDesc=eventList[i].subjDesc;
                      s.drcrInd=eventList[i].drcrInd;
                      s.subjAmt=eventList[i].subjAmt;
                      s.groupId=eventList[i].groupId;
                      saveObjL[i]=s;  
				  }
				 }
		    var rarry=arrfor(Rlist,eventList);
		    for(var j=0;j<Rlist.length;j++){
			  if(rarry.length>0){
				 for(var k=0;k<larry.length;k++){
					 if(j!=rarry[k]){
					var s={};
                    s.subject=Rlist[j].subject;
                    s.subjDesc=Rlist[j].subjDesc;
                    s.drcrInd=Rlist[j].drcrInd;
                    s.subjAmt=Rlist[j].subjAmt;
                    s.groupId=Rlist[j].groupId;
                    saveObjR[j]=s;
				  }
		   	  }
		   }else{
			   var s={};
               s.subject=Rlist[j].subject;
               s.subjDesc=Rlist[j].subjDesc;
               s.drcrInd=Rlist[j].drcrInd;
               s.subjAmt=Rlist[j].subjAmt;
               s.groupId=Rlist[j].groupId;
               saveObjR[j]=s;
		   }
		}
		  
				  saveObj1['contentL']=saveObjL;//已配置列表(绑定)
				  saveObj1['contentR']=saveObjR;//未配置列表(解绑)
         var param = mini.encode(saveObj1);
           CommonUtil.ajax({
                url: "/TbProductEventSceneAcupController/saveChange",
                data: param,
                callback: function (data_1) {
                    if (data_1.code == 'error.common.0000') {
                        mini.alert("保存成功");
                        searchL(tree.getSelected().groupId);
	        		    searchR(tree.getSelected().groupId);
                    } else {
                        mini.alert("保存失败");
                        searchL(tree.getSelected().groupId);
	        		    searchR(tree.getSelected().groupId);
                    }
                }
            });
       }
    </script>
</body>
</html>