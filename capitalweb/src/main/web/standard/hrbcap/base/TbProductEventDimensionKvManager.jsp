<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
	<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
    <title></title>
        <style>

table th{
	border-top:2px solid #428bca;
	text-align:left;
	padding-left:10px;
    border: solid 1px #d9d9d9;
    border-right: 0;
    background-color: #f3f3f3;
    }
</style>
</head>
<body style="width:100%;height:99%;background:white">
<div class="mini-splitter" style="width:100%;height:100%;">

    <div size="350" showCollapseButton="true" >
        <div id="panel1" class="mini-panel" title="产品列表" style="width:100%;height:99%;"  allowResize="true" collapseOnTitleClick="false">
            <div class="mini-fit" >
                <ul id="tree1" class="mini-tree" url="" style="width:100%;;height:100%;"
                    showTreeIcon="true" textField="prdName" idField="prdNo" parentField="" resultAsTree="false" >        
                </ul>
            </div>
        </div>
    </div>
    <div showCollapseButton="false">
    		<div class="mini-splitter" style="width:100%;height:100%;">
    			<div size="340" showCollapseButton="true" >
                    <div id="panel2" class="mini-panel" title="事件列表" style="width:100%;height:99%;"  allowResize="true" collapseOnTitleClick="false">
                        <div class="mini-fit" >
                            <div id="eventList" class="mini-listbox" title="全部" textField="roleName" valueField="roleId" style="width:99%;height:99%;float: left"
                                showCheckBox="flase" multiSelect="false">
                                <div property="columns">
                                    <div type="indexcolumn" headerAlign="center" align="center" width="50">序列</div>
                                    <div field="eventId" header="事件代码" headerAlign="center" align="left" width="80"></div>
                                    <div field="eventName" header="事件名称" headerAlign="center" align="center" width="180"></div>
                                </div>
                            </div>
                        </div>
                    </div>
			    </div>
			    <div showCollapseButton="false">
                    
                    <div id="panel1" class="mini-panel" title="该产品-事件项下勾选维度以增加场景" style="width:100%;height:35%;"  collapseOnTitleClick="false">
                        <a id="Add_Button" class="mini-button" style="display: none"  onclick="addProductEventParam">增加</a>
                        <ul id="paramsList" class="mini-tree" url="" expandOnLoad="true" style="width:100%;" showTreeIcon="true" textField="pValueTxt" idField="paramId"
                            resultAsTree="false" showCheckBox="true">
                        </ul>
                    </div>
                    <div id="panel1" class="mini-panel" title="该产品-事件项下的已配置的维度场景列表" style="width:100%;height:65%;" allowResize="true" collapseOnTitleClick="false">
                        <!-- <a id="Qry_Button"    class="mini-button" style="display: none"  >刷新</a> -->
                        <a id="Del_Button" class="mini-button" style="display: none"  onclick="deleteTbkProductEventParam">删除</a>
                        <div id="datagrid1" class="mini-datagrid" style="width:99.9%;height:85%;" fitColumns="false" url="" idField="id" pageSize="10"
                            allowCellEdit="true" allowCellSelect="true" multiSelect="true" showPager="false" editNextOnEnterKey="true" editNextRowCell="true"
                            allowResize="true">
                            <div property="columns">
                                <div type="indexcolumn" width="30"></div>
                                <div name="groupId" field="groupId" headerAlign="center" width="200">维度组合代码</div>
                                <div field="dimensionDescs" name="dimensionDescs" width="300" headerAlign="center">维度组合名称</div>
                            </div>
                        </div>
                    </div>
		 		</div>
    		</div>
    	</div>      
</div>
    
    <script type="text/javascript">
        mini.parse();

        var tree1 = mini.get("tree1");
        var eventList = mini.get("eventList");
        var paramsList = mini.get("paramsList");
        var grid = mini.get("datagrid1");
        var tree = mini.get("treegrid1");
        tree1.on('nodeclick',function(e){
	        var node = e.node;
	        var isLeaf = e.isLeaf;
                paramsList.setData([]);
                grid.setData([]);
	        	if(isLeaf){
	        		searchProductEvent(9999,0,node.prdNo);
	        	}
        });

        eventList.on('itemclick', function (e) {
            var data = {};
            var treeSelected = tree1.getSelectedNode(); 
            var EventSelected = eventList.getSelected(); 
            data['eventId'] = EventSelected.eventId; 
            data['prdNo'] = treeSelected.prdNo; 
            data['pageSize'] = 999;
            var param = mini.encode(data);
            CommonUtil.ajax({
                url: "/TbProductEventDimensionKvController/searchTbDimensionValue",
                data: param,
                callback: function (data) {
                	
                    var dataArray = data.obj; // List数组 维度信息
                    var resultArray = [];
                    // for循环出父节点和children,显示在tree树形中,以paramName分类
                    // 注意 数组必须有序 order by PARAM_QZ, PARAM_ID
                    for (var index = 0; index < dataArray.length; index++) {
                        var element = dataArray[index];
                        element.pValueTxt = element.dimensionDesc;
                        // 如果是空数组,或者将要添加的Element和已经存在的父节点内容不相同,
                        // 则表明即将添加的Element为一个新的分类
                        if (resultArray.length === 0 || element.dimensionId != resultArray[resultArray.length-1].dimensionId) {
                            $str = JSON.stringify(element); //系列化对象
                            newobj = JSON.parse($str); //还原 达到深拷贝效果,防止后面发生自己添加自己
                            newobj.pValueTxt = newobj.dimensionName; // 显示父节点的内容 没有其他用途
                            newobj.isParent = 1; // 表明是父节点 方便后面操作
                            resultArray.push( 
                                (function (newobj) {
                                    newobj.children = []; // children为总的分类下的子数组 先声明
                                    return newobj
                                })(newobj)
                            );
                        }
                        resultArray[resultArray.length-1].children.push( // 添加最新分类下的维度
                            (function (element) {
                                return element
                            })(element)
                        );
                    }
                    
                    paramsList.setData(resultArray);
                    // 用于显示已选择维度
                    searchParamsMapPageSelected(9999, 0, tree1.getSelectedNode().prdNo, e.item.eventId);
                }
            });
            // 显示已经存在的维度组合
            searchTbkProductEventParam(9999, 0, tree1.getSelectedNode().prdNo, e.item.eventId);
        })
        
        // 查找组合代码
        function searchTbkProductEventParam(pageSize, pageIndex, prdNo, eventId) {
            var data = {};
            data['pageNumber'] = pageIndex + 1;
            data['pageSize'] = pageSize;
            data['prdNo'] = prdNo; // 产品id
            data['eventId'] = eventId; // 事件id
            var param = mini.encode(data);
            CommonUtil.ajax({
                url: "/TbProductEventDimensionKvController/getSceneResultList",
                data: param,
                callback: function (data) {
                    grid.setData(data.obj)
                }
            });
        }

        // 查找已经选择的维度信息
        function searchParamsMapPageSelected(pageSize, pageIndex, prdNo, eventId) {
                var data = {};
                data['pageNumber'] = pageIndex + 1;
                data['pageSize'] = pageSize;
                data['prdNo'] = prdNo; // 产品id
                data['eventId'] = eventId; // 事件id
                var param = mini.encode(data);
                CommonUtil.ajax({
                    url: "/TbProductEventDimensionKvController/getSceneResultList",
                    data: param,
                    callback: function (data) {
                    	
                        // 将数据库已经选择的维度信息,在paramsList中打勾
                        var paramsListNodes = paramsList.getAllChildNodes(); // 获取tree中所有维度数组
                        var checkNodes = new Array(); // 声明需要打勾的维度数组
                        for (var index = 0; index < paramsListNodes.length; index++) {
                            var elementOfNodes = paramsListNodes[index];
                           
                            for (var j = 0; j < data.obj.length; j++) {
                                var elementOfRows = data.obj[j];
                                
                                var arr = elementOfRows.dimensionDescs.split(",");
                                $.each(arr,function(i,item){
                                if (elementOfNodes.pValueTxt === item) {                               	
                                    checkNodes.push(elementOfNodes);
                                    
                                }
                                });
                            }
                        }
                        
                        paramsList.checkNodes(checkNodes);
                    }
                });
            }

        //根据产品来获取相关事件
       function searchProductEvent(pageSize,pageIndex,prdNo){
           var data = {};
           data['pageNumber'] = pageIndex + 1;
           data['pageSize'] = pageSize;
           data['prdNo'] = prdNo;
           data['type'] = 1;
           var param = mini.encode(data);
           CommonUtil.ajax({
               url: "/TbEventController/getProductEvents",
               data: param,
               callback: function (data) {
            	   eventList.setData(data.obj);
               }
           });
       }

        // 点击增加按钮
        function addProductEventParam() {
        	
            var treeSelected = tree1.getSelectedNode(); // 获取选中的产品(Object)
            var EventSelected = eventList.getSelected(); // 获取选中的事件(Object)
            var paramSelecteds = paramsList.getCheckedNodes(false); // 获取选中的维度(Array)
            
            if (paramSelecteds.length === 0) {
                mini.alert("没有选中项，请重新选择", "系统提示"); 
                return;
            }
            var str = "";
            
            for (var index = 0; index < paramSelecteds.length; index++) {
                var element = paramSelecteds[index];
                str += element.dimensionId + " : " + element.dimensionValue + "<br/>";
                
            }
            
            mini.confirm(str, "确认信息", function (messageId) { 
            	
                if (messageId === "ok") {
                    var data = {};
                    data['eventId'] = EventSelected.eventId; // 事件id
//                     data['eventName'] = EventSelected.eventName;
                    data['prdNo'] = treeSelected.prdNo; // 产品id
//                     data['prdName'] = treeSelected.prdName;
                    data['dimensionKvList'] = paramSelecteds; // 维度数组
                    data['sceneResultBeanList'] = paramSelecteds; // 维度数组
                    var param = mini.encode(data);
                  
                    CommonUtil.ajax({
                        url: "/TbProductEventDimensionKvController/setTbProductEventDimension",
                        data: param,
                        callback: function (data) {
                            searchTbkProductEventParam(9999, 0, tree1.getSelectedNode().prdNo, eventList.getSelected().eventId)
                        }
                    });
                } else {
                    return;
                }
            })
        }
        // 删除单条数据
        function deleteTbkProductEventParam() {
            var row = grid.getSelected();
            if (!row) {
                mini.alert("请先选择数据");
            }
            var data = {};
            data['groupId'] = row.groupId; // 事件id
            var param = mini.encode(data);
            CommonUtil.ajax({
                url: "/TbProductEventDimensionKvController/deleteTbProductEventDimensionKv",
                data: param,
                callback: function (data) {
                    searchTbkProductEventParam(9999, 0, tree1.getSelectedNode().prdNo, eventList.getSelected().eventId)
                }
            });
        }
        
        // 获取所有产品信息
        function getproduct(){
            var branchId = "<%=__sessionUser.getBranchId()%>";
            CommonUtil.ajax({
            url: "/ProductController/searchProduct",
            data: {branchId:branchId, prdType:"7000",isAcup:'1'},
            callback: function (data) {
            	tree1.setData(data.obj);
            }
        });
        }
   
        $(document).ready(function() {
			getproduct();
		});
   
    </script>
</body>
</html>