<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
	<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
    <title></title>
        <style>

table th{
	border-top:2px solid #428bca;
	text-align:left;
	padding-left:10px;
    border: solid 1px #d9d9d9;
    border-right: 0;
    background-color: #f3f3f3;
    }
</style>
</head>
<body style="width:100%;height:99%;background:white">
<div class="mini-splitter" style="width:100%;height:100%;">

    <div size="380" showCollapseButton="true" >
    <div id="panel1" class="mini-panel" title="产品/工具列表" style="width:100%;height:99%;"  allowResize="true" collapseOnTitleClick="false">
        <div class="mini-fit" >
            <ul id="productTree" class="mini-tree"  style="width:100%;"
                showTreeIcon="true" textField="prdName" idField="prdNo" parentField="" resultAsTree="false" >        
                <input id="branchId" class="mini-hidden" name="branchId" value="<%=__sessionUser.getBranchId()%>" />
            </ul>
        </div>
     </div>
    </div>
    <div showCollapseButton="true">
    		<div id="panel1" class="mini-panel" title="产品事件列表" style="width:100%;height:98%;"  allowResize="true" collapseOnTitleClick="false">
    			<div id="panel1" class="mini-panel" title="已分配产品事件列表" style="width:46%;height:97%;float: left"  allowResize="true" collapseOnTitleClick="false">
		        <div id="leftList" class="mini-listbox" title="全部"  textField="roleName" valueField="roleId" style="width:99%;height:99%;float: left"  showCheckBox="true" multiSelect="true">
		            <div property="columns">
		                <div type="indexcolumn" headerAlign="center" align="center" width="50">序列</div>
		                <div field="eventId" header="事件代码" headerAlign="center" width="80"></div>
		                <div field="eventName" header="事件名称" headerAlign="center"  width="160"></div>
		            </div>
		        </div>
		     </div>
		        <div style="float: left;width: 5%;text-align:center; ">
		            <div style="margin-bottom:10px; margin-top:50px"><a class="mini-button" style="display: none"     onclick="add" style="width:50px;">&gt;</a></div>
		            <div style="margin-bottom:10px; "><a class="mini-button" style="display: none"     onclick="addAll" style="width:50px;">&gt;&gt;</a></div>
		            <div style="margin-bottom:10px; "><a class="mini-button" style="display: none"     onclick="removes" style="width:50px;">&lt;</a></div>
		            <div style="margin-bottom:10px; "><a class="mini-button" style="display: none"     onclick="removeAll" style="width:50px;">&lt;&lt;</a></div>
		            
		            <div style="margin-bottom:10px; margin-top:50px"><a class="mini-button" style="display: none"     onclick="saveData" style="width:50px;">保存</a></div>
		        </div>
		      <div id="panel1" class="mini-panel" title="未分配产品事件列表" style="width:46%;height:98%;float: left;margin-left:15px"  allowResize="true" collapseOnTitleClick="false">
		        <div id="rightList" class="mini-listbox" style="width:99%;height:97%;float: left"  textField="roleName" valueField="roleId" showCheckBox="true" multiSelect="true"  fitColumns="false">
		            <div property="columns">
		                <div field="eventId" header="事件代码" headerAlign="center" width="80"></div>
		                <div field="eventName" header="事件名称" headerAlign="center"  width="160"></div>
		            </div>
		        </div> 
		       </div>
		    </div>
    </div>      
</div>
    
    <script type="text/javascript">
        mini.parse();
        var tree = mini.get("productTree");
        var branchId = "<%=__sessionUser.getBranchId()%>";
        CommonUtil.ajax({
            url: "/ProductController/searchProduct",
            data: {branchId:branchId, prdType:"7000",isAcup:'1'},
            callback: function (data) {
            	tree.setData(data.obj);
            }
        });
        tree.on('nodeclick',function(e){
            var node = e.node;
	        var isLeaf = e.isLeaf;
	        	if(isLeaf){
	        		searchLeft(9999,0,node.prdNo);
	        		searchRight(9999,0,node.prdNo);
	        	}
        });
      //查询
       var aleftList = mini.get("leftList");
       function searchLeft(pageSize,pageIndex,prdNo){
           var data = {};
           data['pageNumber'] = pageIndex + 1;
           data['pageSize'] = pageSize;
           data['prdNo'] = prdNo;
           data['type'] = 1;
           var param = mini.encode(data);
           CommonUtil.ajax({
               url: "/TbEventController/getProductEvents",
               data: param,
               callback: function (data) {
            	   aleftList.setData(data.obj);
               }
           });
       }
       var arightList = mini.get("rightList");
       function searchRight(pageSize,pageIndex,prdNo){
           var data = {};
           data['pageNumber'] = pageIndex + 1;
           data['pageSize'] = pageSize;
           data['prdNo'] = prdNo;
           data['type'] = 0;
           var param = mini.encode(data);
           CommonUtil.ajax({
               url: "/TbEventController/getProductEvents",
               data: param,
               callback: function (data) {
            	   arightList.setData(data.obj);
               }
           });
       }
       
       
       function add() {
           var items = aleftList.getSelecteds();
           aleftList.removeItems(items);
           arightList.addItems(items);
       }
       function addAll() {        
           var items = aleftList.getData();
           aleftList.removeItems(items);
           arightList.addItems(items);
       }
       function removes() {
           var items = arightList.getSelecteds();
           arightList.removeItems(items);
           aleftList.addItems(items);
       }
       function removeAll() {
           var items = arightList.getData();
           arightList.removeItems(items);
           aleftList.addItems(items);
       }
       function upItem() {
           var items = arightList.getSelecteds();
           for (var i = 0, l = items.length; i < l; i++) {
               var item = items[i];
               var index = arightList.indexOf(item);
               arightList.moveItem(item, index-1);
           }
       }
       function downItem() {            
           var items = arightList.getSelecteds();            
           for (var i = items.length-1; i >=0; i--) {
               var item = items[i];
               var index = arightList.indexOf(item);
               arightList.moveItem(item, index + 1);
           }
       }
       function saveData() {
           var saveObj = {};
          if(aleftList.length != 0){
              var eventList = aleftList.getData();
              var str = "";
              $.each(eventList, function (i, n) {
            str += n.eventId+ ",";
        });
          saveObj.prdNo = tree.getSelected().prdNo;
          saveObj.eventId = str;
          } else {
            mini.alert("请选择一个事件","系统提示");
            return;
          }
          var param = mini.encode(saveObj);
           CommonUtil.ajax({
                url: "/TbProductEventController/saveTbProductEvent",
                data: param,
                callback: function (data_1) {
                    if (data_1.code == 'error.common.0000') {
                        mini.alert("保存成功");
                        searchLeft(9999,0,tree.getSelected().prdNo);
	        		    searchRight(9999,0,tree.getSelected().prdNo);
                    } else {
                        mini.alert("保存失败");
                    }
                }
            });
       
       
       }
    </script>
</body>
</html>