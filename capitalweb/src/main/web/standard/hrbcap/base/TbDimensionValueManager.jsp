<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
	<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
    <title></title>
        <style>

table th{
	border-top:2px solid #428bca;
	text-align:left;
	padding-left:10px;
    border: solid 1px #d9d9d9;
    border-right: 0;
    background-color: #f3f3f3;
    }
</style>
</head>
<body style="width:100%;height:99%;background:white">
<div class="mini-splitter" style="width:100%;height:100%;">
	
    <div size="340" showCollapseButton="true" >
    <div id="panel1" class="mini-panel" title="维度代码列表" style="width:100%;height:99%;"  allowResize="true" collapseOnTitleClick="false">
        <div class="mini-fit" >
            <ul id="paramsTree" class="mini-tree"  style="width:100%;height:100%;"
                showTreeIcon="true" textField="dimensionName" idField="dimensionId" parentField="" resultAsTree="false" >        
            </ul>
        </div>
     </div>
    </div>
    <div showCollapseButton="true">
    		<div id="panel1" class="mini-panel" title="维度代码值列表" style="width:100%;height:99%;"  allowResize="true" collapseOnTitleClick="false"  fitColumns="false">
         <div id="tbDimValueGrid" class="mini-datagrid"  style="width:99.9%;height:68%;"  fitColumns="false"
		      url="" idField="id" 
		      pageSize="10" 
		      allowCellEdit="true" allowCellSelect="true" multiSelect="true" 
		      editNextOnEnterKey="true"  editNextRowCell="true" onRowclick="amonutPointer">
		      <div property="columns" width="500px;">
		          <div type="indexcolumn" width="30">序号</div>
		          <div  field="dimensionValue" name="dimensionValue" headerAlign="center" allowSort="true" width="80" >参数值</div>
		          <div field="dimensionDesc" name="dimensionDesc" width="500" headerAlign="center" >参数值名称</div>
		          <div field="isActive" name="isActive" width="70" renderer="CommonUtil.dictRenderer" data-options="{dict:'YesNo'}" align="center" headerAlign="center">是否启用</div>
		      </div>
		  </div>
		  <div id="panel1" class="mini-panel" title="维度参数值详情" style="width:100%;height:28%;padding:0px;"  allowResize="true" collapseOnTitleClick="false">
		          <table id="tbDimValueForm"  class="form-table" width="100%">
		          	<tr>
					<th>参数值：</th>
					<td>
						<input name="dimensionValue" id="dimensionValue" class="mini-textbox" style="width:100%" required="true"  maxLength="35" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])"/>
					</td>
					<th>参数值名称：</th>
					<td>
						<input name="dimensionDesc" id="dimensionDesc" class="mini-textbox"  style="width:100%" required="true"  vtype="maxLength:20"/>
					</td>
					</tr>
					<tr>
					<th>是否启用：</th>
					<td>
						<input  name="isActive" id="isActive"  class="mini-combobox"  style="width:100%" required="true"  labelStyle="text-align:left;width:170px" data = "CommonUtil.serverData.dictionary.YesNo" value="1" emptyText="请选择..."/>
					</td>
					</tr>
		              <tr>
		                  <td style="text-align:right;padding-top:5px;padding-right:20px;" colspan="4">
		                  	   <a id="Add_Button"    class="mini-button" style="display: none"  onclick="add">增加</a>
					           <a id="Update_Button" class="mini-button" style="display: none"  onclick="update">修改</a>
					           <a id="Cancel_Button" class="mini-button" style="display: none"  onclick="del">删除</a>
					           <a id="Clear_Button" class="mini-button" style="display: none"  onclick="clear">清空</a>
		                  </td>                
		              </tr>
	          	</table>
		           
		</div>
    		</div>  
    </div>      
</div>
    
    <script type="text/javascript">
        mini.parse();
		var form = new mini.Form("#tbDimValueForm");
        var tree = mini.get("paramsTree");
        CommonUtil.ajax({
            url: "/TbDimensionController/searchTbDimensionPage",
            data: {pageSize:9999},
            callback: function (data) {
            	tree.setData(data.obj.list);
            }
        });
        tree.on('nodeclick',function(e){
	        var node = e.node;
	        var isLeaf = e.isLeaf;
	        	if(isLeaf)
        			search(10,0,node.dimensionId);
        });
        var grid = mini.get("tbDimValueGrid");
        $(document).ready(function(){
        		grid.on("beforeload",function(e){
                e.cancel = true;
                var pageIndex = e.data.pageIndex; 
                var pageSize = e.data.pageSize;
                var dimensionId=typeof(tree.getSelected())=="undefined"?'null':tree.getSelected().dimensionId;
                search(pageSize,pageIndex,dimensionId);
            });
            search(10,0,'NULL');
        })
      	//查询
        function search(pageSize,pageIndex,dimensionId){
            var data = {};
            data['pageNumber'] = pageIndex + 1;
            data['pageSize'] = pageSize;
            data['dimensionId'] = dimensionId;
            var param = mini.encode(data);
            CommonUtil.ajax({
                url: "/TbDimensionValueController/searchTbDimensionValueList",
                data: param,
                callback: function (data) {
                	grid.setTotalCount(data.obj.total);
                	grid.setPageIndex(pageIndex);
                	grid.setPageSize(pageSize);
                	grid.setData(data.obj);
                }
            });
        }
        
        function add() {
			var row = tree.getSelected();
			var dimensionId = row.dimensionId;
			var dimensionName = row.dimensionName;
		var form = new mini.Form("#tbDimValueForm");
		form.validate();
		if (form.isValid() == false) {
			mini.alert("信息填写有误，请重新填写!", "消息提示")
			return;
		}
		//提交数据																					
		var data = form.getData(true); //获取表单多个控件的数据  
		data['dimensionId'] = dimensionId;
		data['dimensionName'] = dimensionName;
		var param = mini.encode(data); //序列化成JSON
		CommonUtil.ajax({
                url: "/TbDimensionValueController/createTbDimensionValue",
                data: param,
                callback: function (data_1) {
                    if (data_1.code == 'error.common.0000') {
                        mini.alert("保存成功");
						clear();
						query();
					} else {
                        mini.alert("保存失败");
                    }
                }
            });
	}
        
    function update() {
		var form = new mini.Form("tbDimValueForm");
		var row = grid.getSelected();
		if(!row){
			mini.alert("请选中需要修改的列","系统提示");
		}
		var dimensionId = row.dimensionId;
		var dimensionName = row.dimensionName;
		var dbId = row.dbId;
		form.validate();
		if (form.isValid() == false) {
			mini.alert("信息填写有误，请重新填写!", "消息提示")
			return;
		}
		//提交数据																					
		var data = form.getData(true); //获取表单多个控件的数据  
		data['dbId'] = dbId;
		data['dimensionId'] = dimensionId;
		data['dimensionName'] = dimensionName;
		var param = mini.encode(data); //序列化成JSON
		CommonUtil.ajax({
                url: "/TbDimensionValueController/updateTbDimensionValue",
                data: param,
                callback: function (data_1) {
                    if (data_1.code == 'error.common.0000') {
                        mini.alert(data.desc);
						clear();
						query();
					} else {
                        mini.alert("修改失败");
                    }
                }
            });
	}

      function del() {
				var row = grid.getSelected();
				if (row) {
					mini.confirm("您确认要删除选中记录?","系统警告",function(value){
						if(value=="ok"){
						CommonUtil.ajax({
							url: "/TbDimensionValueController/deleteTbDimensionValue/",
							data: {dbId: row.dbId},
							callback: function (data) {
								if (data.code == 'error.common.0000') {
									mini.alert("删除成功")
                        			clear();
									query();
								} else {
									mini.alert("删除失败");
								}
							}
						});
						}
					});
				}
				else {
					mini.alert("请选中一条记录！", "消息提示");
				}

			}   
        //绑定表单
        // var db = new mini.DataBinding();
        // db.bindForm("tbDimValueForm", grid);
			
		function query(){
			search(grid.pageSize,0,tree.getSelected().dimensionId);
		}  	
        function clear(){
			var form = new mini.Form("#tbDimValueForm");
			form.clear();
			mini.get("dimensionValue").setEnabled(true);
		}
    
		function amonutPointer(){
            var datas = grid.getSelected();
            form.setData(datas);
            mini.get("dimensionValue").setEnabled(false);
        }
	
	</script>
</body>
</html>