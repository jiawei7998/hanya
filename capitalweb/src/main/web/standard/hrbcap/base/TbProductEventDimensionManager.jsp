<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
	<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
    <title></title>
        <style>

table th{
	border-top:2px solid #428bca;
	text-align:left;
	padding-left:10px;
    border: solid 1px #d9d9d9;
    border-right: 0;
    background-color: #f3f3f3;
    }
</style>
</head>
<body style="width:100%;height:99%;background:white">
<div class="mini-splitter" style="width:100%;height:100%;">

    <div size="350" showCollapseButton="true" >
        <div id="panel1" class="mini-panel" title="产品列表" style="width:100%;height:99%;"  allowResize="true" collapseOnTitleClick="false">
            <div class="mini-fit" >
                <ul id="tree1" class="mini-tree" url="" style="width:100%;;height:100%;"
                    showTreeIcon="true" textField="prdName" idField="prdNo" parentField="" resultAsTree="false" >        
                </ul>
            </div>
        </div>
    </div>
    <div showCollapseButton="false">
    		<div class="mini-splitter" style="width:100%;height:100%;">
    			<div size="340" showCollapseButton="true" >
                    <div id="panel2" class="mini-panel" title="事件列表" style="width:100%;height:99%;"  allowResize="true" collapseOnTitleClick="false">
                        <div class="mini-fit" >
                            <div id="eventList" class="mini-listbox" title="全部" textField="roleName" valueField="roleId" style="width:99%;height:99%;float: left"
                                showCheckBox="flase" multiSelect="false">
                                <div property="columns">
                                    <div type="indexcolumn" headerAlign="center" align="center" width="50">序列</div>
                                    <div field="eventId" header="事件代码" headerAlign="center" align="left" width="80"></div>
                                    <div field="eventName" header="事件名称" headerAlign="center" align="center" width="180"></div>
                                </div>
                            </div>
                        </div>
                    </div>
			    </div>
			    <div showCollapseButton="false">
                    
                    <div id="panel1" class="mini-panel" title="该产品-事件项下勾选维度" style="width:100%;height:100%;"  collapseOnTitleClick="false">
                        <a id="Add_Button" class="mini-button" style="display: none"  onclick="addProductEventParam">保存</a>
                        <ul id="paramsList" class="mini-tree" url="" style="width:100%;" showTreeIcon="true" textField="pValueTxt" idField="paramId"
                            resultAsTree="false" showCheckBox="true">
                        </ul>
                    </div>
                    
		 		</div>
    		</div>
    	</div>      
</div>
    
    <script type="text/javascript">
        mini.parse();

        var tree1 = mini.get("tree1");
        var eventList = mini.get("eventList");
        var paramsList = mini.get("paramsList");
       
        var tree = mini.get("treegrid1");
        tree1.on('nodeclick',function(e){
	        var node = e.node;
	        var isLeaf = e.isLeaf;
                paramsList.setData([]);
	        	if(isLeaf){
	        		searchProductEvent(9999,0,node.prdNo);
	        	}
        });

        eventList.on('itemclick', function (e) {
            var data = {};
            data['pageSize'] = 999;
            var param = mini.encode(data);
            CommonUtil.ajax({
                url: "/TbDimensionController/searchTbDimensionPage",
                data: param,
                callback: function (data) {
                    var dataArray = data.obj.list; // List数组 维度信息
                    var data = new Array();
    				//数据格式转化
    				$.each(dataArray,function(i,item){
    				data.push({"id":item.dimensionId,"pValueTxt":item.dimensionName});
    				});
                    paramsList.setData(data);
                    // 用于显示已选择维度
                    searchParamsMapPageSelected(9999, 0, tree1.getSelectedNode().prdNo, e.item.eventId);
                }
            });
        })
        
        // 查找已经选择的维度信息
        function searchParamsMapPageSelected(pageSize, pageIndex, prdNo, eventId) {
                var data = {};
                data['pageNumber'] = pageIndex + 1;
                data['pageSize'] = pageSize;
                data['prdNo'] = prdNo; // 产品id
                data['eventId'] = eventId; // 事件id
                var param = mini.encode(data);
                CommonUtil.ajax({
                    url: "/TbProductEventDimensionController/searchTbProductEventDimensionList",
                    data: param,
                    callback: function (data) {
                    	
                        // 将数据库已经选择的维度信息,在paramsList中打勾
                        var paramsListNodes = paramsList.getAllChildNodes(); // 获取tree中所有维度数组
                        var checkNodes = new Array(); // 声明需要打勾的维度数组
                        for (var index = 0; index < paramsListNodes.length; index++) {
                            var elementOfNodes = paramsListNodes[index];
                            for (var j = 0; j < data.obj.length; j++) {
                                var elementOfRows = data.obj[j];
                                if (elementOfNodes.id === elementOfRows.dimensionId) {
                                    checkNodes.push(elementOfNodes);
                                    
                                }
                                
                            }
                        }
                        
                        paramsList.checkNodes(checkNodes);
                    }
                });
            }
        // 点击增加按钮
        function addProductEventParam() {
        	
            var treeSelected = tree1.getSelectedNode(); // 获取选中的产品(Object)
            var EventSelected = eventList.getSelected(); // 获取选中的事件(Object)
            var paramSelecteds = paramsList.getCheckedNodes(false); // 获取选中的维度(Array)
            
            if (paramSelecteds.length === 0) {
                mini.alert("没有选中项，请重新选择", "系统提示"); 
                return;
            }
                	//重新构建数据传输对象
                	var saveObject = {};
            		saveObject.prdNo = treeSelected.prdNo; // 产品id
            		saveObject.eventId = EventSelected.eventId; // 事件id
            		saveObject.dimensionIds = "";
            		//将选中的dimensionId用,分割
            		
            		$.each(paramSelecteds,function(i,item){
            			saveObject.dimensionIds += item.id;
            			saveObject.dimensionIds += i==paramSelecteds.length-1?"":",";
            		});
                  
                    CommonUtil.ajax({
                        url: "/TbProductEventDimensionController/setTbProductEventDimension",
                        data: saveObject,
                        callback: function (data) {
                        	$.messager.alert("提示信息","保存成功。");
                        }
                    });
        }
        
        // 获取所有产品信息
        function getproduct(){
            var branchId = "<%=__sessionUser.getBranchId()%>";
            CommonUtil.ajax({
            url: "/ProductController/searchProduct",
            data: {branchId:branchId, prdType:"7000",isAcup:'1'},
            callback: function (data) {
            	tree1.setData(data.obj);
            }
        });
        }
   
        $(document).ready(function() {
			getproduct();
		});
        //根据产品来获取相关事件
        function searchProductEvent(pageSize,pageIndex,prdNo){
            var data = {};
            data['pageNumber'] = pageIndex + 1;
            data['pageSize'] = pageSize;
            data['prdNo'] = prdNo;
            data['type'] = 1;
            var param = mini.encode(data);
            CommonUtil.ajax({
                url: "/TbEventController/getProductEvents",
                data: param,
                callback: function (data) {
             	   eventList.setData(data.obj);
                }
            });
        }
    </script>
</body>
</html>