<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title></title>
    <style>
.mini-labelfield-label{
left:30px;
}
table th{
text-align:left;
padding-left:10px;
width:120px;
   border-right: 0;
   }
   .header {
   height: 40px;
   /* background: #95B8E7; */
   color: #404040;
   background: linear-gradient(to bottom,#EFF5FF 0,#E0ECFF 100%);
   background-repeat: repeat-x;
   line-height:40px; /*设置line-height与父级元素的height相等*/
   text-align: center; /*设置文本水平居中*/
   font-size:14px;
   margin-top: -16px;
}
</style>
  </head>
  
<body style="width:100%;height:100%;background:white">
<div class="header"  height="55" showSplit="false" showHeader="true">
<span id="labell"><h3 >维度码表信息维护-修改</h3></span>
</div>
<div id="job_form">
	<table class="mini-sltable" style="text-align:left;margin:auto;width:100%;">
		<tr id="pk">
			<td style="width:50%"><input id="dbId" name="dbId"  label="dbId" labelField="true"   style="width:100%" labelStyle="text-align:left;width:170px" class="mini-textbox" required="true"  emptyText="请输入数字"/></td>
			<td style="width:50%"><input id="dimensionSort" name="dimensionSort"  label="维度排序" labelField="true"   style="width:100%" labelStyle="text-align:left;width:170px" class="mini-textbox"required="true"  /></td>
		</tr> 
		<tr>
			<td style="width:50%"><input id="dimensionId" name="dimensionId"  label="维度ID" labelField="true"   style="width:100%" labelStyle="text-align:left;width:170px" class="mini-textbox"  required="true"  /></td>
			<td style="width:50%"><input id="dimensionName" name="dimensionName"  label="维度名称" labelField="true"   style="width:100%" labelStyle="text-align:left;width:170px" class="mini-textbox"  required="true"  /></td>
		</tr>
		<tr>
			<td style="width:50%"><input id="isActive" name="isActive"  label="是否启用" labelField="true"   style="width:100%" labelStyle="text-align:left;width:170px" class="mini-combobox" required="true"  data = "CommonUtil.serverData.dictionary.YesNo" value="0" showNullItem="false"  allowInput="false" emptyText="请选择..."/></td>
			<td style="width:50%"><input id="remark" name="remark"  label="备注" labelField="true"   style="width:100%" labelStyle="text-align:left;width:170px" class="mini-textbox"  required="true"  /></td>
		</tr>
	</table>
</div>

<div  class="mini-toolbar" style="text-align:center" >
	<a onclick="save()" id="save_btn" class="mini-button" style="display: none"  >保存</a>
	<a onclick="colse()" id="close_btn" class="mini-button" style="display: none"  >关闭</a>
</div>
<script type="text/javascript">
	mini.parse();
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var row=params.selectedData;
	var action =params.action;
	var saveurl="";
	//表单赋值
	function initData(){
		if(action=="add"){
			$("#labell").html("<h3 >维度码表信息维护-新增</h3>");
			saveurl="/TbDimensionController/createTbDimension";
		}else{
			saveurl="/TbDimensionController/updateTbDimension";
			var form=new mini.Form("#job_form");
			form.setData(row);
		}
	}
	//保存
	function save(){
		var form=new mini.Form("#job_form");
		form.validate();
		if(form.isValid==false) return ;
		var data=form.getData();
		params =mini.encode(data);
		CommonUtil.ajax({
			url:saveurl,
			data:params,
			callback:function(data){
				top["win"].closeMenuTab();
				mini.alert("保存成功.","提示");
			}
		});
	}
	//关闭表单
	function colse(){
		top["win"].closeMenuTab();
	}
	$(document).ready(function(){
		initData();
	});
</script>
</body>
</html>