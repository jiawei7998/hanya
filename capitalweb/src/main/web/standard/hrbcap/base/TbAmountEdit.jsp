<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="../../global.jsp"%>
<style>
.mini-labelfield-label{
left:30px;
}
table th{
text-align:left;
padding-left:10px;
width:120px;
   border-right: 0;
   }
   .header {
   height: 40px;
   /* background: #95B8E7; */
   color: #404040;
   background: linear-gradient(to bottom,#EFF5FF 0,#E0ECFF 100%);
   background-repeat: repeat-x;
   line-height:40px; /*设置line-height与父级元素的height相等*/
   text-align: center; /*设置文本水平居中*/
   font-size:14px;
}
</style>
<body>
<div id="TbAmountEdit"  style="width: 100%; height: 100%;">
<div class="header"  height="55" showSplit="false" showHeader="true">
<span id="labell"><span style="font-size:18px;font-weight:550">会计金额维护</span></span>
</div>
	<div id="TbAmountInfo" style="padding:5px;">
		<input class="mini-hidden" name="id" />
			<table   style="text-align:left;margin:auto;width:100%;" class="mini-sltable">
				<tr>
					<td style="width:50%">
						<input id="amountId" name="amountId" class="mini-textbox" labelField="true" label="金额代码:" emptyText="请输入金额代码" required="true"  style="width:100%" labelStyle="text-align:left;width:170px"  requiredErrorText="金额代码不能为空" />
					</td>
					<td style="width:50%">
						<input id="amountName" name="amountName" class="mini-textbox" labelField="true" label="金额名称：" style="width:100%" labelStyle="text-align:left;width:170px" required="true"  vtype="maxLength:15" requiredErrorText="金额名称不能为空" emptyText="请输入金额名称" />
					</td>
				</tr>
				<tr>
					<td style="width:50%">
						<input id="amountFormula" name="amountFormula" class="mini-textbox" labelField="true" label="金额公式:" emptyText="请输入金额公式" required="true"  style="width:100%" labelStyle="text-align:left;width:170px"  requiredErrorText="金额公式不能为空" />
					</td>
					<td style="width:50%">
						<input id="amountDesc" name="amountDesc" labelField="true" label="金额描述：" vtype="maxLength:50" style="width:100%" labelStyle="text-align:left;width:170px" emptyText="请输入金额描述" class="mini-textarea"/>
					</td>
				</tr>
				<tr>
					<td colspan="3" style="text-align:center;">
						<a class="mini-button" style="display: none"    onClick="save()" id="save_btn">保存</a>
						<a id="cancel_btn" class="mini-button" style="display: none"    onclick="cancel">取消</a>
					</td>
				</tr>
		</table>
	</div>
</div>
</body>
<script>
	mini.parse();
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var row=params.selectedData;
	var action =params.action;
	$(document).ready(function(){
		if(action=="edit"){
			$("#labell").html("<span style='font-size:18px;font-weight:550'>会计金额维护</span >");
			init();
		}
	})

	//渲染数据
	function init() {
		if(action=='edit'){
			var form =new mini.Form("TbAmountInfo");
			mini.get("amountId").setEnabled(false);
			form.setData(row);
		}
	}

	//保存
	function save() {
		var url = '';
		var form = new mini.Form("TbAmountEdit");
		form.validate();
		if (form.isValid() == false) {
			return;
		}
		var data = form.getData();
		if (action == "edit") {
			url = "/TbAmountController/updateAmount";
		} else {
			url = "/TbAmountController/createAmount";
		}
		var param = mini.encode(data); //序列化成JSON
		CommonUtil.ajax({
			url: url,
			data: param,
			callback: function (data) {
				if (data.code == 'error.common.0000') {
					mini.alert("操作成功","系统提示",function(){setTimeout(function(){top["win"].closeMenuTab()},10);});
				} else {
					mini.alert("操作失败","系统提示");
				}
			}
		});
	}

	//取消
	function onCancel(e) {
		window.CloseOwnerWindow();
	}
	
	//取消按钮
	function cancel(){
		window.CloseOwnerWindow();
	}
</script>