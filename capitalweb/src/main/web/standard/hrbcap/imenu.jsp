<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<head>

</head>
<body style="width:100%;height:100%;background:white">
<div id="layout1" class="mini-splitter" style="width:100%;height:100%;">
    <div size="200" showCollapseButton="true" >
    	<div id="panel1" class="mini-panel" title="产品/工具列表" style="width:100%;height:99%;"  allowResize="true" collapseOnTitleClick="false">
        	<div class="mini-fit" >
            	<ul id="productTree" class="mini-tree"  style="width:100%;"
                	showTreeIcon="true" textField="prdName" idField="prdNo" parentField="" resultAsTree="false" >        
                	<input id="branchId" class="mini-hidden" name="branchId" value="<%=__sessionUser.getBranchId()%>" />
            	</ul>
        	</div>
     	</div>
    </div>
    <div showCollapseButton="true">
      	<div id="fundManage" class="mini-fit">   
          	<!-- 待审批  -->
        	<div id="datagrid" class="mini-datagrid" style="width:100%;height:98%;" idField="dealNo" 
          		allowAlternating="true" allowResize="true" border="true"  title="会计维度分录配置列表" showHeader="true"
          		multiSelect="false" sortMode="client"  fitColumns="false" showPager="false">
          		<div property="columns">
            		<div type="indexcolumn" headerAlign="center" width="50">序号</div>
            		<div field="prdName" width="120" headerAlign="center" align="left" allowSort="false">产品名称</div>
            		<div field="eventName" width="90" headerAlign="center" align="left" allowSort="false" >事件名称</div>
            		<div field="dimensionDescs" width="150" headerAlign="center" align="left" allowSort="false" >维度列表</div>
            		<div field="drcrInd" width="80" headerAlign="center" align="left" allowSort="false">借贷标识</div>
					<div field="subjTrans" width="120" headerAlign="center" align="left" allowSort="false">科目号</div>
					<div field="subjCom" width="80" headerAlign="center" align="left" allowSort="false">COM2</div>
					<div field="subject" width="120" headerAlign="center" align="left" allowSort="false">科目配置</div>
            		<div field="subjDesc" width="400" headerAlign="center" align="left" allowSort="false">科目名称</div>
            		<div field="subjAmt" width="150" headerAlign="left" allowSort="true">金额表达式</div> 
            		<div field="dimensionKeys" width="220" headerAlign="left" allowSort="true">关联属性</div> 
            		<div field="dimensionValues" width="120" headerAlign="left" allowSort="true">属性取值</div> 
          	   </div>
          </div>  
       </div>
    </div>
</div>
</body>
<script>
	mini.parse();
	var tree = mini.get("productTree");
 	var branchId = "<%=__sessionUser.getBranchId()%>";

	//获取参数传递  
	var url = window.location.search;
	var type = CommonUtil.getParam(url,"type");
	//获取当前活动ID
	var activeTab = top["win"].tabs.getActiveTab();
	
	CommonUtil.ajax({
        url: "/ProductController/searchProduct",
        data: {branchId:branchId, prdType:"7000",isAcup:'1'},
        callback: function (data) {
             tree.setData(data.obj);
        }
    });
	
	tree.on('nodeclick',function(e){
        var node = e.node;
	    var isLeaf = e.isLeaf;
	        if(isLeaf){
	        	//查询相应产品配置
	        	query(node.prdNo);
	        }
    });
	
	function init(param){}



//打开菜单链接
function openUrl(param){
  var url = encodeURI(param.moduleUrl);
  var tab = {
    id : param.moduleId,
    name : param.moduleId,
    title : param.moduleName,
    url : url,
    showCloseButton:true
  };

  var paramData ={dealType:type};
  //解析URL中的参数存入paramData
  CommonUtil.getUrlParms(paramData,param.moduleUrl);

  CommonUtil.openNewMenuTab(tab,paramData);
}


var grid = mini.get("datagrid");


function search(pageSize,pageIndex,value)
{
  
  var data = {};
  data['prdNo'] = value;
  data['pageNumber'] = pageIndex+1;
  data['pageSize'] = pageSize;
  var url = "/TbProductEventSceneAcupController/selectAllSceneForProdNo";
  
  var params = mini.encode(data);
  CommonUtil.ajax({
    url:url,
    data:params,
    showLoadingMsg:false,
    messageId:mini.loading('正在查询数据...', "请稍后"),
    callback : function(data) {
      	grid.setData(data.obj);
      	grid.mergeColumns([1, 2,3,8,9]);
    }
  });
  
  
}// end search

function query(value){
  search(999,0,value);
}

</script>

</html>
