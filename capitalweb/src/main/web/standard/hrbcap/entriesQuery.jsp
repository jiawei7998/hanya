<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
<head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>
<body style="width:100%;height:100%;background:white">
<div class="mini-panel" title="分录查询" style="width:100%;">
	<div style="width:100%;padding-top:10px;padding-bottom:10px">
		<div id="search_form" >
			<input id="dealNo" name="dealNo" class="mini-textbox" width="320px"  labelField="true" label="交易单号：" labelStyle="text-align:right;" emptyText="请输入交易单号"/>
			<input id="subjCode" name="subjCode" class="mini-textbox" width="320px"  labelField="true" label="科目号：" labelStyle="text-align:right;" emptyText="请输入科目号"/>
<%--			<input id="prdNo" name="prdNo" class="mini-combobox" width="320px"  labelField="true" label="产品类型：" labelStyle="text-align:right;"   data="CommonUtil.serverData.dictionary.FundType" emptyText="请选择产品类型"/>--%>
			<input id="prdNo" name="prdNo" class="mini-combobox" width="320px"  labelField="true" label="产品类型：" labelStyle="text-align:right;" textfield="text" valuefield="id" data="[
{id:'801',text:'货币基金'},
{id:'802',text:'债券基金'},
{id:'803',text:'专户基金'},
{id:'799',text:'同业存款（线下）'}]" emptyText="请选择产品类型"/>
			<input id="eventId" name="eventId" class="mini-combobox" width="320px"  labelField="true"  label="会计事件：" labelStyle="text-align:right;"  emptyText="请选择会计事件"/>
			<nobr>
				<input id="entry_date_begin" name="entry_date_begin" class="mini-datepicker" width="320px"
					   labelField="true" label="账务日期：" valueType="string" labelStyle="text-align:right;"
					   emptyText="请选择账务开始日期..." />
				<input id="entry_date_end" name="entry_date_end" class="mini-datepicker" width="320px"
					   labelField="true" label="~" valueType="string" onValuechanged="compareDate" labelStyle="text-align:center;"
					   emptyText="请选择账务结束日期..." />
			</nobr>
			<input id="sendFlag" name="sendFlag" class="mini-combobox" width="320px"  labelField="true" label="发送状态：" labelStyle="text-align:right;"   data="CommonUtil.serverData.dictionary.EntrySendFlag"/>
			<span style="float:right;">
					<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
					<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
			</span>
		</div>
	</div>
</div>
<div id="EntriesQuerys" class="mini-fit" style="margin-top: 5px;">
	<div id="form_grid" class="mini-datagrid borderAll" allowAlternating="true" style="width:100%;height:97.5%;" sortMode="client" fitColumns="false" frozenStartColumn="0" multiSelect="true"  frozenEndColumn="1">
		<div property="columns">
		<div type="checkcolumn"></div>
			<div field="sendFlag" width="150" allowSort="false" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'EntrySendFlag'}">发送状态</div>
			<div field="qzDate" width="150" allowSort="false" headerAlign="center" align="center">发送日期</div>
			<div field="dealNo" width="200" allowSort="false" headerAlign="center" align="center">交易单号</div>
			<div field="prdName" width="200" allowSort="false" headerAlign="center" align="center">产品类型</div>
			<%--<div field="flowSeq" width="50" allowSort="true" headerAlign="center" align="center">序号</div>--%>
			<div field="ccy" width="60" allowSort="false" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'Currency'}">币种</div>
			<div field="debitCreditFlag" width="80" allowSort="false" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'DebitCreditFlag'}" >借贷标识</div>
			<div field="subjCode" width="150" allowSort="true" headerAlign="center" align="center">科目号</div>
			<div field="subjName" width="310" allowSort="false" headerAlign="center">科目名称</div>
			<div field="value" width="120" numberFormat="#,0.00" headerAlign="center" align="right" allowSort="true">金额</div>
			<div field="postDate" width="100" allowSort="true" headerAlign="center" align="center">账务日期</div>
			<!-- <div field="taskName" width="120" allowSort="false" headerAlign="center" align="center">所属帐套</div> -->
			<div field="prdName" width="100" allowSort="false" headerAlign="center" align="center" >产品类型</div>
			<div field="sponInstName" width="100" allowSort="false" headerAlign="center" align="center">交易机构</div>
			<div field="eventName" width="100" allowSort="false" headerAlign="center" align="center">会计事件</div>
			<!-- <div field="core_acct_code" width="100" allowSort="false" headerAlign="center" align="center">核心账号</div> -->
			<div field="flowId" width="120" allowSort="true" headerAlign="center" align="center">账务流水号</div>
			<div field="qzDealno" width="120" allowSort="true" headerAlign="center" align="center">核心响应流水号</div>
			<div field="errMsg" width="200" allowSort="true" headerAlign="center" align="center">失败原因</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	mini.parse();

	var grid = mini.get("form_grid");
	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex;
		var pageSize = e.data.pageSize;
		search(pageSize, pageIndex);
	});
	function search(pageSize, pageIndex) {
		var form = new mini.Form("search_form");
		if (form.isValid == false)
			return;
		//提交数据
		var data = form.getData();//获取表单多个控件的数据
		data['pageNumber'] = pageIndex + 1;
		data['pageSize'] = pageSize;
		var param = mini.encode(data); //序列化成JSON
		CommonUtil.ajax({
			url: '/TbEntryController/searchPageEntries',
			data: param,
			callback: function (data) {
				var grid = mini.get("form_grid");
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
				grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}

	

	function clear() {
		var form = new mini.Form("#search_form");
		form.clear();
		query();

	}

	function init(){
		var form = new mini.Form("search_form");
		//提交数据
		var data = form.getData();//获取表单多个控件的数据
		var param = mini.encode(data); //序列化成JSON
		CommonUtil.ajax({
			url: '/TbEntryController/searchEvent',
			data: param,
			callback: function (data) {
				mini.get("eventId").load(data.obj);
			}
		});
	}

	function compareDate() {
		var sta = mini.get("entry_date_begin");
		var end = mini.get("entry_date_end");
		if (sta.getValue() > end.getValue() && end.getValue("")) {
			mini.alert("开始时间不能大于结束时间", "系统提示");
			return end.setValue(sta.getValue());
		}
	}

	//查询的方法
	function query() {
		var grid = mini.get("form_grid");
		search(grid.pageSize, 0);
	}

	$(document).ready(function () {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
			search(10, 0);
			init(); //初始化事件下拉
		});
	})
</script>
</body>
</html>