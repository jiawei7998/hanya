<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="../global.jsp" %>
<html>

<head>
    <style>
        table th {
            text-align: left;
            padding-left: 10px;
            width: 120px;
            border-right: 0;
        }

        .header {
            height: 30px;
            /* background: #95B8E7; */
            color: #404040;
            background: linear-gradient(to bottom, #EFF5FF 0, #E0ECFF 100%);
            background-repeat: repeat-x;
            line-height: 30px; /*设置line-height与父级元素的height相等*/
            text-align: center; /*设置文本水平居中*/
            font-size: 14px;
        }
    </style>
</head>

<body style="width:100%;height:100%;background:white">
<div id="layout1" class="mini-layout" style="width:100%;height:100%;">

    <div class="header" region="north" height="38" showSplit="false" showHeader="false">
        <div id="search_form" style="width:100%;padding-top:3px;">
            <table id="search_form" style="width:100%;">
                <input id="dealno" name="dealNo_s" class="mini-textbox" labelField="true" label="业务编号："
                       labelStyle="text-align:right;" emptyText=""/>
                <input id="cname" name="cno_s" class="mini-textbox" labelField="true" label="客户名称："
                       onbuttonclick="onButtonEdit" labelStyle="text-align:right;" emptyText="模糊查询"/>
                <input id="product" name="product_s" class="mini-combobox" labelField="true" label="产品类型："
                       labelStyle="text-align:right;" emptyText=""/>
                <input id="vdate" name="vdate" class="mini-datepicker" labelField="true" label="汇款日期"
                       labelStyle="text-align:center;"/>
                <span style="float:right;margin-right:80px">
							<a id="search_btn" class="mini-button" style="display: none" onclick="query()">查询</a>
							<a id="clear_btn" class="mini-button" style="display: none" onClick="clear">清空</a>
						</span>
            </table>
        </div>
    </div>
    <div title="center" region="center" bodyStyle="overflow:hidden;">
        <div id="SettleCnapsManages" class="mini-datagrid borderAll" style="width:100%;height:99%;" allowResize="true"
             pageSize="10"
             sortMode="client" allowAlternating="true" frozenStartColumn="0" frozenEndColumn="1">
            <div property="columns">
                <div type="indexcolumn" headerAlign="center" align="center" width="50px">序号</div>
                <div field="amount" width="120" numberFormat="#,0.00" align="right" headerAlign="center"
                     allowSort="true">结算金额
                </div>
                <div field="cname" width="120" align="left" headerAlign="center" allowSort="false">客户名称</div>
                <div field="vdate" width="120" align="center" headerAlign="center" allowSort="false">结算日期</div>
                <div field="payrecind" width="80" align="center" headerAlign="center" allowSort="false"
                     renderer="payrecnd">方向
                </div>
                <div field="ccy" width="80" align="left" headerAlign="center" allowSort="true">币种</div>
                <div field="recBankid" width="120" align="left" headerAlign="center" allowSort="false">收款行行号</div>
                <div field="recBankname" width="120" align="left" headerAlign="center" allowSort="false">收款行名称</div>
                <div field="recUserid" width="120" align="left" headerAlign="center" allowSort="false">收款人账号</div>
                <div field="recUsername" width="120" align="left" headerAlign="center" allowSort="false">收款人名称</div>
                <div field="dealno" width="180" align="center" headerAlign="center" allowSort="false">流水号</div>
                <div field="prodtype" width="120" align="left" headerAlign="center" allowSort="false">产品类型</div>
            </div>
        </div>

    </div>
    <div title="south" region="south" showSplit="false" showHeader="false" height="380px;"
         style="line-height:38px;text-align: center;">
        <div class="mini-panel" title="支付信息详情" style="width:100%;" allowResize="true" collapseOnTitleClick="true">
            <div id="find2" class="fieldset-body">
                <table id="field_form" class="form-table" width="100%">
                    <tr>
                        <th>收款人：</th>
                        <td>
                            <input id="cno" name="cno" readonly="true" class="mini-textbox" enabled="false"
                                   style="width:90%"/>
                        </td>
                        <th>资金业务编号：</th>
                        <td>
                            <input id="dealno" name="dealno" readonly="true" class="mini-textbox" enabled="false"
                                   style="width:90%"/>
                        </td>

                    </tr>
                    <tr>
                        <th>证件发行国家：</th>
                        <td>
                            <input id="cerIssuerCountry" name="cerIssuerCountry" style="width:90%" value="IC-资金系统组织机构代码"
                                   readonly="true" class="mini-textbox"
                                   enabled="false"/>
                        </td>
                        <th>证件类型：</th>
                        <td>
                            <input id="cerType" name="cerType" style="width:90%" value="CN-中华人民共和国" readonly="true"
                                   class="mini-textbox" enabled="false"/>
                        </td>
                    </tr>
                    <tr>
                        <th>付款人账号：</th>
                        <td>
                            <input id="payUserid" name="payUserid" style="width:90%" readonly="true"
                                   class="mini-textbox" enabled="false" enabled="false"/>
                        </td>
                        <th>付款人账户名</th>
                        <td>
                            <input id="payUsername" name="payUsername" style="width:90%" readonly="true"
                                   class="mini-textbox" enabled="false" enabled="false"/>
                        </td>
                    </tr>
                    <tr>
                        <th>付款人开户行号：</th>
                        <td>
                            <input id="payBankid" name="payBankid" style="width:90%" required="true"
                                   class="mini-textbox" maxLength="14" enabled="false"
                                   onvalidation="CommonUtil.onValidation(e,'alphanum',[null])"
                            />
                        </td>
                        <th>付款人开户行名：</th>
                        <td>
                            <input id="payBankname" name="payBankname" style="width:90%" class="mini-textbox"
                                   required="true" vtype="maxLength:50" enabled="false"/>
                        </td>
                    </tr>
                    <tr>
                        <th>收款人账号：</th>
                        <td>
                            <input id="recUserid" name="recUserid" style="width:90%" class="mini-textbox"
                                   onbuttonclick="recUserEdit" allowInput="true"
                                   maxLength="32" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])"
                                   required="true" enabled="false"/>
                        </td>
                        <th>收款人账号名：</th>
                        <td>
                            <input id="recUsername" name="recUsername" style="width:90%" class="mini-textbox"
                                   required="true" vtype="maxLength:50" enabled="false"/>
                        </td>

                    </tr>
                    <tr>
                        <th>收款人开户行号：</th>
                        <td>
                            <input id="recBankid" name="recBankid" style="width:90%" readonly="true"
                                   class="mini-textbox" enabled="false"/>
                        </td>
                        <th>收款人开户行名：</th>
                        <td>
                            <input id="recBankname" name="recBankname" style="width:90%" readonly="true"
                                   class="mini-textbox" enabled="false"/>
                        </td>

                    </tr>
                    <tr>
                        <th>币种代码：</th>
                        <td>
                            <input id="ccy" name="ccy" readonly="true" style="width:90%" value="RMB=人民币"
                                   class="mini-textbox" enabled="false"/>
                        </td>
                        <th>金额：</th>
                        <td>
                            <input id="amount" name="amount" style="width:90%" format="#,0.00" class="mini-spinner"
                                   changeOnMousewheel="false" enabled="false"
                            />
                        </td>
                    </tr>
                    <tr>
                        <th>附言：</th>
                        <td>
                            <input id="remarkFy" name="remarkFy" style="width:90%" class="mini-textbox"
                                   vtype="maxLength:50"/>
                        </td>
                    </tr>
                </table>
            </div>
            <!-- <span class="btn_div" style="float: left;margin: 15px; color:#0065d2;">
                * 双击列表将清算信息详情展示后再进行交易经办处理。
            </span>
            <div class="btn_div" style="float: right;margin: 15px">
                <a id="send_btn" class="mini-button" style="display: none"    onClick="btn_send">交易经办</a>
            </div> -->
        </div>
    </div>
</div>
<script type="text/javascript">
    mini.parse();
    var grid = mini.get("SettleCnapsManages");
    var form1 = new mini.Form("field_form");
    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize, pageIndex);
    });

    function query() {
        search(grid.pageSize, 0);
    }

    function search(pageSize, pageIndex) {
        var form = new mini.Form("search_form");
        form.validate();              //表单验证
        if (form.isValid() == false) {
            mini.alert("表单填写错误,请确认!", "提示信息")
            return;
        }
        var data = form.getData();      //获取表单数据
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        var param = mini.encode(data);   //序列化json格式
        CommonUtil.ajax({
            url: "/FundSettelsController/getFtSettlesList",
            data: param,
            callback: function (data) {
                var grid = mini.get("SettleCnapsManages");
                //设置分页
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }

    var payRecive = [{
        id: 'P',
        text: '付'
    }, {
        id: 'R',
        text: '收'
    }];

    function payrecnd(e) {
        for (var i = 0, l = payRecive.length; i < 2; i++) {
            var g = payRecive[i];
            if (g.id == e.value)
                return g.text;
        }
        return " ";
    }

    function clear() {
        var form = new mini.Form("#search_form");
        form.clear();
    }

    grid.on("select", function (e) {
        var row = e.record;
        var cno = mini.get("cno");
        cno.setValue(row.cno + "." + row.cname);
        cno.setText(row.cno + "." + row.cname);
        //资金编号
        var dealno = mini.get("dealno");
        dealno.setValue(row.dealno);
        dealno.setText(row.dealno);
        //申请人账号
        var payUserid = mini.get("payUserid");
        payUserid.setValue(row.payUserid);
        payUserid.setText(row.payUserid);
        //申请人名称
        var payUsername = mini.get("payUsername");
        payUsername.setValue(row.payUsername);
        payUsername.setText(row.payUsername);

        //申请行账号
        var payBankid = mini.get("payBankid");
        payBankid.setValue(row.payBankid);
        payBankid.setText(row.payBankid);
        //申请行名称
        var payBankname = mini.get("payBankname");
        payBankname.setValue(row.payBankname);
        payBankname.setText(row.payBankname);

        //收款人账号
        var recUserid = mini.get("recUserid");
        recUserid.setValue(row.recUserid);
        recUserid.setText(row.recUserid);
        //收款人名称
        var recUsername = mini.get("recUsername");
        recUsername.setValue(row.recUsername);
        recUsername.setText(row.recUsername);
        //接收行行号
        var recBankid = mini.get("recBankid");
        recBankid.setValue(row.recBankid);
        recBankid.setText(row.recBankid);
        //接收行名称
        var recBankname = mini.get("recBankname");
        recBankname.setValue(row.recBankname);
        recBankname.setText(row.recBankname);
        //金额
        var amount = mini.get("amount");
        amount.setValue(row.amount);
        amount.setText(row.amount);
        var remarkFy = mini.get("remarkFy");
        remarkFy.setValue(row.remarkFy);
        remarkFy.setText(row.remarkFy);
    });


    $(document).ready(function () {
        search(10, 0);
    });
</script>
</body>

</html>