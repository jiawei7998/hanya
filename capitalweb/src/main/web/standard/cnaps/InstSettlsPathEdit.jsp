<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="../global.jsp" %>
<html>
<head>
    <title>Insert title here</title>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>
<body>
<style>
    table th {
        text-align: left;
        padding-left: 10px;
        width: 120px;
        border-right: 0;
    }

    .header {
        height: 30px;
        /* background: #95B8E7; */
        color: #404040;
        background: linear-gradient(to bottom, #EFF5FF 0, #E0ECFF 100%);
        background-repeat: repeat-x;
        line-height: 30px; /*设置line-height与父级元素的height相等*/
        text-align: center; /*设置文本水平居中*/
        font-size: 18px;
    }
</style>
</head>
<body style="width:100%;height:100%;background:white">
<div id="layout1" class="mini-layout" style="width:100%;height:100%;">

    <div class="header" region="north" height="32" showSplit="false" showHeader="false">
        （本方）机构清算路径信息
    </div>
    <div title="center" region="center" bodyStyle="overflow:hidden;">
        <table id="field_form" style="text-align:left;margin:auto;width:100%;" class="mini-sltable">
            <h3 style="text-align:left;" id="labell"><a href="javascript:CommonUtil.activeTab();">机构清算信息</a> >> 修改</h3>
            <tr>
                <th style="width:15%">机构：</th>
                <td style="width:35%">
                    <input id="instId" name="instId" allowInput="false" class="mini-buttonedit"
                           onbuttonclick="onInsQuery" required="true" emptyText="请选择机构..." style="width:95%"
                           labelStyle="text-align:left;width:170px"/></td>
                <th style="width:15%">业务类别：</th>
                <td style="width:35%">
                    <input id="busiType" name="busiType" class="mini-combobox" emptyText="请输入业务类别" required="true"
                           data="CommonUtil.serverData.dictionary.busiType" style="width:95%"
                           labelStyle="text-align:left;width:170px"/></td>
            </tr>
            <tr>
                <th>币种代码：</th>
                <td>
                    <input id="ccy" name="ccy" class="mini-combobox" emptyText="" required="true"
                           data="CommonUtil.serverData.dictionary.Currency" value="CNY" enabled="false"
                           style="width:95%" labelStyle="text-align:left;width:170px"/></td>
                <th>账户号：</th>
                <td>
                    <input id="instAcctNo" name="instAcctNo" class="mini-textbox" emptyText="请输入账号" required="true"
                           maxLength="32" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" style="width:95%"
                           labelStyle="text-align:left;width:170px"/></td>
            </tr>
            <tr>
                <th>账户名：</th>
                <td>
                    <input id="instAcctNm" name="instAcctNm" class="mini-textbox" emptyText="请输入户名" required="true"
                           vtype="maxLength:26" style="width:95%" labelStyle="text-align:left;width:170px"/></td>
                <th>账号类型：</th>
                <td>
                    <input id="mediumType" name="mediumType" class="mini-combobox" emptyText="请输入账号类型" required="true"
                           data="CommonUtil.serverData.dictionary.mediumType" value="A" enabled="false"
                           style="width:95%" labelStyle="text-align:left;width:170px"/></td>
            </tr>
            <tr>
                <th>开户行号：</th>
                <td>
                    <input id="instAcctPbocNo" name="instAcctPbocNo" required="true" class="mini-textbox"
                           emptyText="请输入开户行号" maxLength="14"
                           onvalidation="CommonUtil.onValidation(e,'alphanum',[null]) " style="width:95%"
                           labelStyle="text-align:left;width:170px"/></td>
                <th>开户行名：</th>
                <td>
                    <input id="instAcctBknm" name="instAcctBknm" class="mini-textbox" required="true"
                           emptyText="请输入开户行名" vtype="maxLength:26" style="width:95%"
                           labelStyle="text-align:left;width:170px"/></td>
            </tr>
            <tr>
                <th>大额支付账号类型：</th>
                <td>
                    <input id="payerAccType" name="payerAccType" class="mini-combobox" emptyText="请输入大额支付账号类型"
                           required="true" data="CommonUtil.serverData.dictionary.payerAccType" value="1"
                           enabled="false" style="width:95%" labelStyle="text-align:left;width:170px"/></td>
                <th>是否启用：</th>
                <td>
                    <input id="isActive" name="isActive" style="width:95%" labelStyle="text-align:left;width:170px"
                           class="mini-combobox" emptyText="" value="1" required="true"
                           data="CommonUtil.serverData.dictionary.isActive"/></td>
            </tr>
            <tr>

                <th>备注：</th>
                <td colspan="3">
                    <input id="instRemark" name="instRemark" style="width:98%" labelStyle="text-align:left;width:170px"
                           class="mini-textbox" emptyText="请输入备注" vtype="maxLength:95"/></td>
            </tr>
        </table>
    </div>
    <div title="south" region="south" showSplit="false" showHeader="false" height="39px;"
         style="line-height:38px;text-align: center;">
        <div id="functionIds">
            <a id="save_btn" class="mini-button" style="display: none" onclick="add">保存</a>
            <a id="cancel_btn" class="mini-button" style="display: none" onclick="close()">取消</a>
        </div>
    </div>
</div>
<script>
    mini.parse();
    var url = window.location.search;
    var action = CommonUtil.getParam(url, "action");
    var param = top['InsitutionSettles'].getData(action);
    var form = new mini.Form("#field_form");

    //关闭页面
    function close() {
        top['win'].closeMenuTab();
    }

    $(document).ready(function () {
        initForm();
        if (action == "detail") {
            $("#labell").html("<a href=javascript:CommonUtil.activeTab();>机构清算信息</a>>>详情");
            mini.get("save_btn").setVisible(false);
            form.setEnabled(false);
        } else if (action == "add") {
            $("#labell").html("<a href=javascript:CommonUtil.activeTab();>机构清算信息</a>>>新增")
        }
    });

    //赋值给form表单
    function initForm() {
        if ($.inArray(action, ['edit', 'detail']) > -1) {
            form.setData(param);
            var instId = mini.get("instId");
            instId.setValue(param.instId);
            instId.setText(param.instFullname);
        }
    }

    //保存
    function add() {
        form.validate();
        if (form.isValid() == false) {
            mini.alert("表单填写错误,请确认!", "提示信息")
            return;
        }
        var params = form.getData();

        var saveUrl = $.inArray(action, ["add"]) > -1 ? "/InstitutionSettlsController/addInstitutionSettl" : "/InstitutionSettlsController/updateInstitutionSettl";
        if ($.inArray(action, ['edit']) > -1) {
            params.settlId = param.settlId;
        }
        var data = mini.encode(params);
        CommonUtil.ajax({
            url: saveUrl,
            data: data,
            callback: function (data) {
                if ('error.common.0000' == data.code) {
                    mini.alert("保存成功", "提示", function () {
                        setTimeout(function () {
                            top["win"].closeMenuTab()
                        }, 10);
                    });

                } else {
                    mini.alert("保存失败");
                }
            }
        });
    }

    // 查询机构的方法
    function onInsQuery(e) {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "/../Common/MiniInstitutionSelectManages.jsp",
            title: "机构选择",
            width: 700,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data); //必须
                    if (data) {
                        btnEdit.setValue(data.instId);
                        btnEdit.setText(data.instName);
                        btnEdit.focus();
                    }
                }

            }
        });
    }

</script>

</body>
</html>