﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="./global.jsp"%>
<html>
<head>
<title>新利资金业务管理系统</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="<%=basePath%>/miniScript/css/logn.css" rel="stylesheet" type="text/css" />
<script src="<%=basePath%>/miniScript/sha512.js" type="text/javascript" ></script>
<script src="<%=basePath%>/miniScript/jscookie.min.js" type="text/javascript" ></script>     
</head>
<body id="mainBox" style="box-sizing:border-box;">
    <div id="main_bg"><!-- 横 --></div>
    <!-- 竖+logo -->
    <div id="login_bg">
        <div class="login_logo">
        <div class="login-title">
    </div>
    <div class="login-text"></div>	
    </div>
    <div id="login_form_outer">
    <div id="login_form_inner">
    <div id="banner"></div>
    <div id="login_form">    <!-- login_form -->
    <div class="circle" id="loginForm">
    <div class="main" >
    <table class="form_bar" style="height:auto;">
        <tr style="vertical-align:middle;">
            <td align="center">
                <div id="bankId">
                    <div class="input_bar">
                        <div class="name_text">银行：</div>
                        <input class="mini-combobox" id="branchId" name="branchId" textField="branchName" valueField="branchId"  emptyText="" errorMode="border" required="true"   onvaluechanged="bankchaneg"/>
                    </div>
                </div>
            </td>
        </tr>

        <tr style="vertical-align:middle;">
            <td align="center">
                <div style="position:relative;">
                    <div class="input_bar">
                        <div class="password_img">用户名：</div>
                        <div><input class="mini-textbox"  emptyText="" id="userId" name="userId" errorMode="border" required="true" /></div>

                    </div>
                    <div class="arrow arrow1" onclick="add_input()"/></div>
                </div>
            </td>

        </tr>
       <tr class="hideinput" style="display:none;">
            <td align="center"  >
                <div >
                    <div class="input_bar">
                        <div class="password_img">机构：</div>
                        <div><input class="mini-combobox" emptyText="" id="instId" name="instId" textField="instName" valueField="instId" emptyText="请选择机构名称" errorMode="border" required="true" /></div>
                    </div>
                </div>
            </td>

        </tr>
        <tr class="hideinput" style="display:none;" >
            <td align="center" >
                <div >
                    <div class="input_bar">
                        <div class="password_img">角色：</div>
                        <div><input class="mini-combobox" emptyText="" id="roleId" name="roleId" textField="roleName" valueField="roleId" errorMode="border" required="true" /></div>

                    </div>
                </div>
            </td>

        </tr>
        <tr class="hideinput" style="display:none;">
            <td align="center" >
                <div style="position:relative;">
                    <div class="input_bar">
                        <div class="password_img">密码：</div>
                        <div><input class="mini-password" id="passwd" name="passwd" emptyText="" errorMode="border" required="true"  /></div>

                    </div>
                    <div class="arrow" onclick="open_index()"></div>
                    
                </div>
            </td>

        </tr>
    </table>
</div>
</div>
</body>
</html>
<script type="text/javascript">
  mini.parse();


  $(document).ready(function(){
    
    $('#userId').keydown(function (e){
           if(e.keyCode ==13){
               add_input();
           }
       });
    $('#passwd').keydown(function (e){
        if(e.keyCode ==13){
            open_index();
        }
    });
    
    CommonUtil.ajax({
		url:"/brccController/getBrccs",
        callback:function(data){
            var branchId =  mini.get('branchId');
            branchId.setData(data.obj);
                if(data.obj.length==1){//only one hidden
                    branchId.select(0);
                    //branchId.setEnabled(false);
                    $('#bankId').css("display", "none");
                    //js设置选择值不会触发事件,需要主动调用
                    $(".login-title").css('background','url("<%=basePath%>/miniScript/css/images/'+data.obj[0].branchLogo+'.png") no-repeat center');
                    $(".login-text").text(data.obj[0].systemName);
                    mini.get('userId').focus();
                }
			}
		});
  });
  
  function bankchaneg(e){
	  $(".login-title").css('background','url("<%=basePath%>/miniScript/css/images/'+e.selected.branchLogo+'.png") no-repeat center');
      $(".login-text").text(e.selected.systemName);
  }
  
  //只允许数字加小写字母
  mini.get("userId").on('blur',function(res){
	  //mini.get("userId").setValue(res.source.value.replace(/[\W]/g,'').toLocaleUpperCase()){
	 mini.get("userId").setValue(res.source.value.replace(/[\W]/g,''))
  })
  
  function add_input() {
    var form = new mini.Form("#loginForm");
    	form.validate();
    if (form.isValid() == false) return;
        var data = form.getData(); //获取表单多个控件的数据
        var param = mini.encode(data); //序列化成JSON
    //检查用户是否存在
    CommonUtil.ajax({
        url:"/UserController/checkUserByBranchId",
        data:param,
        callback:function(data){
			 if(data.obj){
                 //设置样式
                // $(".main").css("top", "200px");
                // $(".form_bar").css("height", "350px");
                $(".hideinput").css("display", "table-row");
                $(".arrow1").css("display", "none");
                //选中机构后屏蔽编辑
                mini.get("branchId").setEnabled(false);
                mini.get("userId").setEnabled(false);
                //查询机构
                CommonUtil.ajax({
                    url:"/InstitutionController/getInstByUser",
                    data:param,
                    callback:function(data){
                        mini.get('instId').setData(data.obj);
                        mini.get('instId').setText(data.obj[0].text);
                        mini.get('instId').setValue(data.obj[0].instId);
                        onAutoDeptChanged();
                        mini.get("passwd").focus();
                        }
                    });
             }else{
                mini.alert('用户不存在!');
             }
			}
		});
  }

  function onDeptChanged(){
        var form = new mini.Form("#loginForm");
        var data = form.getData(); //获取表单多个控件的数据
        var param = mini.encode(data); //序列化成JSON
        CommonUtil.ajax({
                    url:"/RoleController/getRoleByUserIdAndInstId",
                    data:param,
                    callback:function(data){
                        mini.get('roleId').setData(data.obj);
                        }
                    });
      
  }

  function onAutoDeptChanged(){
      var form = new mini.Form("#loginForm");
      var data = form.getData(); //获取表单多个控件的数据
      var param = mini.encode(data); //序列化成JSON
      CommonUtil.ajax({
                  url:"/RoleController/getRoleByUserIdAndInstId",
                  data:param,
                  callback:function(data){
                      mini.get('roleId').setData(data.obj);
                      mini.get('roleId').setText(data.obj[0].roleName);
                      mini.get('roleId').setValue(data.obj[0].roleId);
                      }
                  });
    
}
  
  function open_index() {
    var form = new mini.Form("#loginForm");
    form.validate();
    if (form.isValid() == false) return;
	var data = form.getData(); //获取表单多个控件的数据
    if (typeof(Cookies.get('userId')) != "undefined" && Cookies.get('userId') != data.userId){ //不存在
    	mini.alert('已登录其它用户,不允许多个用户登录!');
    	return;
    }
       //data.passwd = hex_md5(data.passwd);
       data.passwd = hex_sha512(data.passwd);
       var param = mini.encode(data); //序列化成JSON
       CommonUtil.ajax({
            url:"/UserController/checkLogin",
            data:param,
            callback : function(ret) {
            	if(ret.desc!="error.system.0000"){
            		mini.confirm('用户已在其他地址登录，您确认要继续登录系统吗？','系统提示',function(r){   
                    if (r=='ok'){ 
				       CommonUtil.ajax({
				                   url:"/UserController/login",
				                   data:param,
				                   callback:function(data){
				                	    //写入本地cookie
				                        Cookies.set('userId', data.detail, {expires: 0.01});
				                        window.location.href =data.desc;
				                       }
				                   });
				             }
	                    });
	            	}else{
	            		CommonUtil.ajax({
	                        url:"/UserController/login",
	                        data:param,
	                        callback:function(data){
	                             window.location.href =data.desc;
	                        }                      
	                    });
	            	}
	            }
	        });
  }
  
  //高度自适应屏幕高度
  function autoHeight(){
	    if (window.innerHeight){
	    nowHeight = window.innerHeight;
	    }else{
	    nowHeight = document.documentElement.clientHeight;
	    }
	    var jianHeight = 60;
	    if(nowHeight > jianHeight){
	    document.getElementById('mainBox').style.height = nowHeight - jianHeight + 'px';
	    }else{
	    document.getElementById('mainBox').style.height = jianHeight + 'px';
	    }
	    }
	    autoHeight();
	    window.onresize = autoHeight;

</script>