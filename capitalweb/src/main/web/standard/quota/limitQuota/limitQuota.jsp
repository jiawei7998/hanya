<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
    <head>
        <title>用户限额管理表</title>
        <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
        <script type="text/javascript" src="<%=basePath%>/miniScript/hideBtn.js"></script>
    </head>
<body style="width:100%;height:100%;background:white">
    <fieldset class="mini-fieldset title">
    	<legend>用户限额查询</legend>
	    <div id="search_form" style="width: 100%;">
	        <input id="userName" name="userName" class="mini-buttonedit" width="330px" onbuttonclick="onUserQuery" labelField="true" label="交易员："  labelStyle="text-align:right;" emptyText="请输入交易员" />
	        <input id="product" name="product" class="mini-buttonedit" width="330px" onbuttonclick="onPrdQuery" labelField="true" label="产品：" labelStyle="text-align:right;" emptyText="请输入产品" />
	        <input id="prodType" name="prodType" class="mini-buttonedit" width="330px" onbuttonclick="onPrdTypeQuery" labelField="true"  label="产品类型代码：" labelStyle="text-align:right;" emptyText="请输入产品类型代码" />
	        <input id="dealSource" name="dealSource" class="mini-combobox"  width="330px" data="CommonUtil.serverData.dictionary.dealSource" width="320px" emptyText="请选择账簿划分" labelField="true"  label="账簿划分："labelStyle="text-align:right;" />
	       	<span style="float:right;margin-right: 150px">
	            <a class="mini-button" style="display: none"  id="search_btn"  onclick="search()">查询</a>
	            <a class="mini-button" style="display: none"  id="clear_btn"  onclick="clear()">清空</a>
	        </span>
	    </div>
    </fieldset>
    <span style="margin:2px;display: block;">
        <a class="mini-button" style="display: none"  id="add_btn"  onClick="add();">新增</a>
        <a class="mini-button" style="display: none"  id="edit_btn"  onClick="modify();">修改</a>
        <a class="mini-button" style="display: none"  id="delete_btn"  onClick="del();">删除</a>
    </span>
    <div class="mini-fit" style="width:100%;height:100%;">
        <div id="prod_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" 
            sortMode="client" allowAlternating="true"   idField="userId" onrowdblclick="onRowDblClick" 
            border="true" allowResize="true" >
            <div property="columns">
                <div type="indexcolumn" headerAlign="center" align="center" width="40px">序列</div>
                <div field="limitId" headerAlign="center" allowSort="true" width="150px" align="center">业务流水号</div>
                <div field="ctlType" headerAlign="center"  width="100px" data-options="{dict:'ctlType'}" renderer="CommonUtil.dictRenderer">检测控制</div>
                <div field="limitType" headerAlign="center"  width="100px" align="center" data-options="{dict:'limitType'}" renderer="CommonUtil.dictRenderer" >限额类型</div>
                <div field="currency" headerAlign="center"  width="100px" data-options="{dict:'Currency'}" renderer="CommonUtil.dictRenderer">限额币种</div>
                <div field="ccy" width="100" headerAlign="center" align="center">交易</div>
                <div field="tradName" headerAlign="center" width="100px" align="center" >交易员</div>
                <div field="prd" headerAlign="center"  width="100px">产品</div>
                <div field="prdTypeCode" headerAlign="center"  width="100px" align="center">产品类型代码</div>
                <div field="prdType"  headerAlign="center"  width="100px">产品类型描述</div>
                <div field="cost" headerAlign="center"  width="100px">成本中心代码</div>
                <div field="costName" visible="false" headerAlign="center"  width="100px">成本中心</div>
                <div field="dealSource"  headerAlign="center"  width="120px" data-options="{dict:'dealSource'}" renderer="CommonUtil.dictRenderer">账簿划分</div>
                <div field="accType" headerAlign="center"  width="100px">会计类型</div>
                <div field="acctngtypeName" headerAlign="center"  width="100px">债券类型</div>
                <div field="quotaAmountU" headerAlign="center" allowSort="true" align="center" numberFormat="#,0.00" width="120px">限额下限(美元)</div>
                <div field="availAmountU" headerAlign="center"  allowSort="true" align="center" numberFormat="#,0.00" width="200px">限额上限(美元)</div>
                <div field="quotaAmount" headerAlign="center" numberFormat="#,0.00"  width="120px">折人民币下限(元)</div>
                <div field="availAmount"  headerAlign="center" numberFormat="#,0.00"  width="200px">折人民币上限(元)</div>


                <div field="bondType" visible="false" headerAlign="center"  width="100px">债券类型</div>
                <div field="bondTypeCode" visible="false" headerAlign="center"  width="100px">债券类型</div>
                <div field="limitCount" visible="false"  headerAlign="center"  width="100px">累计笔数</div>
                <div field="ccyCode" visible="false"  headerAlign="center"  width="100px">币种代码</div>
                <div field="rate" visible="false"  headerAlign="center"  width="100px">汇率</div>
                <div field="direction" visible="false" headerAlign="center"  width="100px">交易方向</div>
                <div field="trad" visible="false" headerAlign="center"  width="100px">用户名ID</div>
                <div field="availAmount" visible="false" headerAlign="center"  width="100px">折人民币上线(元)</div>
                <div field="term" visible="false" headerAlign="center"  width="100px">期限</div>
                <div field="prdCode" visible="false" headerAlign="center"  width="100px">产品代码</div>
                <div field="totalAmount" visible="false" headerAlign="center"  width="100px">累计额度</div>
            </div>
        </div>
    </div>
</body>

<script>
    mini.parse();
    var form = new mini.Form("#search_form");
    var grid = mini.get("prod_grid");
    
    grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		searchs(pageSize,pageIndex);
	});
    $(document).ready(function() {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
            search();
        });
	});

    //查询按钮
     function search(){
    	 searchs(10,0);
    } 
    function searchs(pageSize, pageIndex) {
        form.validate();
        if (form.isValid() == false) {
            mini.alert("表单填写错误,请确认!", "提示信息");
            return;
        }
        var data = form.getData(true);
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        var params = mini.encode(data);
        CommonUtil.ajax({
            url : "/IfsLimitTotalController/searchPageLimit",
            data : params,
            callback : function(data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }
    //清空
    function clear(){
        var form=new mini.Form("search_form");
        form.clear();
        search(10,0);
    }
    //新增
    function add(){
        var url = CommonUtil.baseWebPath() + "../../quota/limitQuota/limitQuotaEdit.jsp?action=add";
        var tab = { id: "limitQuotaAdd", name: "limitQuotaAdd", title: "用户限额新增", 
        url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
        var paramData = {selectData:""};
        CommonUtil.openNewMenuTab(tab,paramData);
    }
    //修改
    function modify(){
        var row = grid.getSelected();
        if(row){
        	var lstmntdte = new Date(row.lstmntdte);
        	row.lstmntdte=lstmntdte;
            var url = CommonUtil.baseWebPath() + "../../quota/limitQuota/limitQuotaEdit.jsp?action=edit";
            var tab = { id: "limitQuotaEdit", name: "limitQuotaEdit", title: "用户限额修改", 
            url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
            var paramData = {selectData:row};
            CommonUtil.openNewMenuTab(tab,paramData);
        } else {
            mini.alert("请选中一条记录！","消息提示");
        }
    }
    //删除
    function del() {
        var row = grid.getSelected();
        if (row) {
            mini.confirm("您确认要删除选中记录?","系统警告",function(value){
                if(value=="ok"){
                    CommonUtil.ajax({
                        url: "/IfsLimitTotalController/deleteLimit",
                        data: {limitId: row.limitId},
                        callback: function (data) {
                            if (data.code == 'error.common.0000') {
                                mini.alert("删除成功!");
                                grid.reload();
                            } else {
                                mini.alert("删除失败!");
                            }
                        }
                    });
                }
            });
        }else {
            mini.alert("请选中一条记录！", "消息提示");
        }

    }
    //双击详情
    function onRowDblClick(e) {
        var row = grid.getSelected();
        var lstmntdte = new Date(row.lstmntdte);
        row.lstmntdte=lstmntdte;
        if(row){
                var url = CommonUtil.baseWebPath() + "../../quota/limitQuota/limitQuotaEdit.jsp?action=detail";
                var tab = { id: "limitQuotaDetail", name: "limitQuotaDetail", title: "用户限额详情", url: url ,showCloseButton:true};
                var paramData = {selectData:row};
                CommonUtil.openNewMenuTab(tab,paramData);
        } else {
            mini.alert("请选中一条记录！","消息提示");
        }
    }

    //产品查询
    function onPrdQuery(e){
        var btnEdit = this;
        var data ={};
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Ifs/opics/prodMiniLess.jsp",
            title: "选择产品代码",
            width: 700,
            height: 500,
            onload: function () {
                var iframe = this.getIFrameEl();
                iframe.contentWindow.SetData(data);
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.pcode);
                        btnEdit.setText(data.pcode);
                        btnEdit.focus();
                    }
                }

            }
        });

    }

    //类型查询
    function onPrdTypeQuery(e){
        var btnEdit = this;
        var prd = mini.get("product").getValue();
        var data = { prd: prd };
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Ifs/opics/typeMiniLess.jsp",
            title: "选择产品类型",
            width: 700,
            height: 500,
            onload: function () {
                var iframe = this.getIFrameEl();
                iframe.contentWindow.SetData(data);
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.type);
                        btnEdit.setText(data.type);
                        btnEdit.focus();
                    }
                }

            }
        });
    }

    function onUserQuery(e){
        var btnEdit = this;
        var data ={};
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Common/limit/LimitUser.jsp",
            title: "选择用户",
            width: 900,
            height: 600,
            onload: function () {
            },
            ondestroy: function (action) {

                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.userName);
                        btnEdit.setText(data.userName);
                        btnEdit.focus();
                    }
                }

            }
        });
    }
</script>
</html>