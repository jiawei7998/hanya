<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
  <title>用户限额维护</title>
  <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
	<script type="text/javascript" src="../../Ifs/cfetsrmb/rmbVerify.js"></script>
</head>
<body style="width:100%;height:100%;background:white">
<div class="mini-splitter" style="width:100%;height:100%;">
	<div size="90%" showCollapseButton="false">
		<h1 style="text-align:center"><strong>用户限额</strong></h1>
		<div id="field_form" class="mini-fit area"  style="background:white">
			<div class="mini-panel" title="基金概况" style="width:100%;"  allowResize="true" collapseOnTitleClick="true">
				<div class="leftarea">
					<input style="width:100%;" id="userId" name="userId" class="mini-buttonedit mini-mustFill" required="true"  labelField="true" label="用户名称：" onbuttonclick="onUserIdChange" allowInput="false" labelStyle="text-align:left;width:130px;"/>
			        <input style="width:100%;" id="product" name="product" class="mini-buttonedit mini-mustFill" required="true"  labelField="true" label="产品代码："  onbuttonclick="onPrdEdit" allowInput="false" vtype="maxLength:10" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" labelStyle="text-align:left;width:130px;" >
			    	<input style="width:100%;" id="cost" name="cost" class="mini-buttonedit mini-mustFill" required="true"  labelField="true"  label="成本中心："  onbuttonclick="onCostEdit" allowInput="false"  vtype="maxLength:15" labelStyle="text-align:left;width:130px;"  >
					<input style="width:100%;" id="lev" name="lev" class="mini-combobox mini-mustFill" required="true"  labelField="true" label="级别：" labelStyle="text-align:left;width:130px;"  data = "CommonUtil.serverData.dictionary.Level"/>
				</div>
				<div class="leftarea">
					<input style="width:100%;" id="instId" name="instId" class="mini-textbox mini-mustFill" required="true"   labelField="true" label="所属机构：" labelStyle="text-align:left;width:130px;" enabled="false"/>
		        	<input style="width:100%;" id="prodType" name="prodType" class="mini-buttonedit mini-mustFill" required="true"  labelField="true" label="产品类型：" onbuttonclick="onTypeEdit" allowInput="false" vtype="maxLength:2" labelStyle="text-align:left;width:130px;" >
					<input style="width:100%;" id="path" name="path" class="mini-combobox mini-mustFill" required="true"  labelField="true" label="方向：" labelStyle="text-align:left;width:130px;"  data = "CommonUtil.serverData.dictionary.BusinessDirection"/>
					<input style="width:100%"  id="quota" name="quota" class="mini-spinner mini-mustFill input-text-strong" required="true"   labelField="true"   label="限制金额（亿）：" changeOnMousewheel="false" onvalidation="zeroValidation" maxValue="9999999999999999.99" format="n2" labelStyle="text-align:left;width:130px;" vtype="maxLength:20"/>
				</div>
			</div>
		</div>	
	</div>
	<div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
		<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="save_btn"   onclick="save">保存交易</a></div>
		<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="close_btn"   onclick="close">关闭界面</a></div>
	</div>
</div>		
<script type="text/javascript">
	mini.parse();

	top['CFundInfoEdit'] = window;
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var row=params.selectData;
	var url=window.location.search;
	var action=CommonUtil.getParam(url,"action");
	var form =new mini.Form("#field_form");

	function save(){
		//表单验证！！！
		form.validate();
		if (form.isValid() == false) {
			return;
		}
		var data=form.getData(true);
		var params=mini.encode(data);
		CommonUtil.ajax({
			url:"/IfsQuotaController/saveOrUpdateIfsQuota",
			data:params,
			callback:function(data){
				if(data.obj == true) {
					mini.alert(data.desc,'提示信息',function(){
						top["win"].closeMenuTab();
					});
				}else {
					mini.alert(data.desc);
				}
			}
		});
	}
	function close(){
		top["win"].closeMenuTab();
	}
	$(document).ready(function(){
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
			if ($.inArray(action, ["approve", "detail"]) > -1) {
				mini.get("save_btn").setVisible(false);
				form.setEnabled(false);
				form.setData(row);
			}
			if ($.inArray(action, ["edit", "approve", "detail"]) > -1) {
				form.setData(row);
				mini.get("userId").setEnabled(false);
				mini.get("product").setEnabled(false);
				mini.get("prodType").setEnabled(false);
				mini.get("cost").setEnabled(false);

				mini.get("userId").setValue(row.userId);
				mini.get("userId").setText(row.userName);

				mini.get("product").setValue(row.product);
				mini.get("product").setText(row.product);

				mini.get("prodType").setValue(row.prodType);
				mini.get("prodType").setText(row.prodType);

				mini.get("cost").setValue(row.cost);
				mini.get("cost").setText(row.cost);
			}
		});
	})
	function onUserIdChange(e) {
        var btnEdit = this;
        mini.open({
        	url: CommonUtil.baseWebPath() + "../../Common/MiniUserManagesMini.jsp",
            title: "选择用户",
            width: 700,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.userId);
                        btnEdit.setText(data.userName);

                        mini.get("instId").setValue(data.instId);
						mini.get("instId").setText(data.insts);
                        btnEdit.focus();
                    }
                }

            }
        });
    }
	function onPrdEdit(){
    	var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "/opics/prodMiniLess.jsp",
            title: "选择产品代码",
            width: 700,
            height: 500,
            onload: function () {
                var iframe = this.getIFrameEl();
                iframe.contentWindow.SetData({});
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.pcode);
                        btnEdit.setText(data.pcode);
                        onPrdChanged();
                        btnEdit.focus();
                    }
                }

            }
        });
    	
    }
	function onPrdChanged(){
    	if(mini.get("prodType").getValue()!=""){
    		mini.get("prodType").setValue("");
        	mini.get("prodType").setText("");
        	mini.alert("产品代码已改变，请重新选择产品类型!");
    	}
    }
	function onTypeEdit(){
	 	var prd = mini.get("product").getValue();
	 	var direction="";
	 	var al="";
	 	if(prd == null || prd == ""){
	 		mini.alert("请先选择产品代码!");
	 		return;
	 	}
	 	if(prd =="MM"){
	 	  direction = mini.get("direction").getValue();
	 	  if(direction!="" && direction=='S'){//拆出-资产
	 	  	al='A';
	 	  }else if(direction!="" && direction=='P'){
	 	  	al='L';
	 	  }else{
	 	  	al="";
	 	  }
	 	}
	 	var data={prd:prd,al:al};
	 	var btnEdit = this;
	     mini.open({
	         url: CommonUtil.baseWebPath() + "/opics/typeMiniLess.jsp",
	         title: "选择产品类型",
	         width: 700,
	         height: 500,
	         onload: function () {
	             var iframe = this.getIFrameEl();
	             iframe.contentWindow.SetData(data);
	         },
	         ondestroy: function (action) {
	             if (action == "ok") {
	                 var iframe = this.getIFrameEl();
	                 var data = iframe.contentWindow.GetData();
	                 data = mini.clone(data);    //必须
	                 if (data) {
	                     btnEdit.setValue(data.type);
	                     btnEdit.setText(data.type);
	                     btnEdit.focus();
	                 }
	             }
	
	         }
	     });
	 }
	function onCostEdit(){
    	var btnEdit = this;     
    	var data={prdName:null,type:null,flag:null};
        mini.open({
            url: CommonUtil.baseWebPath() + "/opics/costMiniLess.jsp",
            title: "选择成本中心",
            width: 700,
            height: 500,
            onload: function () {
                var iframe = this.getIFrameEl();
                iframe.contentWindow.SetData(data);
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.costcent);
                        btnEdit.setText(data.costcent);
                        btnEdit.focus();
                    }
                }

            }
        });
    }
	
</script>
<script src="<%=basePath%>/miniScript/miniMustFill.js"></script>	
</body>
</html>
