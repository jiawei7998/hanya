<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../../global.jsp" %>
<html>
<head>
    <title>用户限额维护</title>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../Ifs/cfetsrmb/rmbVerify.js"></script>
</head>
<body style="width:100%;height:100%;background:white">
<div class="mini-splitter" style="width:100%;height:100%;">
    <div size="90%" showCollapseButton="false">
        <h1 style="text-align:center"><strong>用户限额</strong></h1>
        <div id="field_form" class="mini-fit area" style="background:white">
            <div class="mini-panel" title="基本信息" style="width:100%;" allowResize="true" collapseOnTitleClick="true">
                <div class="leftarea">
                    <input id="limitType" name="limitType" required="true" class="mini-combobox mini-mustFill"
                           labelField="true" label="限额类型：" emptyText="请选择限额类型"
                           data="CommonUtil.serverData.dictionary.limitType" required="true" style="width:100%"
                           labelStyle="align:left;width:130px" onvaluechanged="onCurrencyPairChange"/>
                    <input id="ctlType" name="ctlType" required="true" class="mini-combobox mini-mustFill"
                           labelField="true" label="检测控制：" emptyText="请选择" value="2"
                           data="CommonUtil.serverData.dictionary.ctlType" required="true" style="width:100%"
                           labelStyle="align:left;width:130px"/>

                    <input id="prd" name="prd" required="true" class="mini-buttonedit mini-mustFill"
                           onbuttonclick="onPrdQuery" labelField="true" label="产品：" required="false" emptyText="请选择产品"
                           style="width:100%" labelStyle="align:left;width:130px" onvaluechanged="changeProd"/>
                    <input id="cost" name="cost" required="true" class="mini-textbox" labelField="true" label="成本中心："
                           required="false" style="width:100%" labelStyle="align:left;width:130px"/>
                    <input id="costName" name="costName" class="mini-buttonedit" onbuttonclick="loadCostList"
                           labelField="true" label="成本中心：" emptyText="请选择成本中心" required="false" style="width:100%"
                           labelStyle="align:left;width:130px"/>

                    <input id="bondType" name="bondType" class="mini-buttonedit" onbuttonclick="onChangeBondType"
                           labelField="true" label="债券类型：" required="false" emptyText="请选择债券类型" style="width:100%"
                           labelStyle="align:left;width:130px"/>
                    <input id="bondTypeCode" name="bondTypeCode" class="mini-textbox" labelField="true" label="债券类型："
                           style="width:100%" labelStyle="align:left;width:130px"/>

                    <input id="acctngtypeName" name="acctngtypeName" class="mini-buttonedit" emptyText="请选择会计类型"
                           labelField="true" label="会计类型：" style="width:100%" labelStyle="align:left;width:130px"
                           onbuttonclick="onActyQuery" required="false"/>

                    <input id="acctngtype" name="acctngtype" class="mini-textbox" labelField="true" label="会计类型："
                           style="width:100%" labelStyle="align:left;width:130px" required="false"/>

                    <input id="quotaAmountU" name="quotaAmountU" class="mini-spinner mini-mustFill input-text-strong"
                           labelField="true" label="美元限额下限(美元)：" enabled="true" emptyText="请输入可用金额"
                           maxValue="999999999999999" minValue="-999999999999999" changeOnMousewheel='false' format="n2"
                           style="width:100%" labelStyle="align:left;width:130px" onvaluechanged="changeUSDToCNY"/>
                    <input id="quotaAmount" name="quotaAmount" required="true"
                           class="mini-spinner mini-mustFill input-text-strong" labelField="true" label="折人民币下限(元)："
                           enabled="false" emptyText="请输入可用金额" maxValue="9999999999999999" minValue="-9999999999999999"
                           changeOnMousewheel='false' format="n2" style="width:100%"
                           labelStyle="align:left;width:130px"/>
                    <input id="limitCount" name="limitCount" class="mini-textbox" labelField="true" label="累计笔数："
                           enabled="true" emptyText="请输入需要限制的笔数" maxValue="999999999999" changeOnMousewheel='false'
                           style="width:100%" labelStyle="align:left;width:130px"/>

                    <input id="ccyCode" name="ccyCode" class="mini-textbox" labelField="true" label="币种代码："
                           style="width:100%" labelStyle="align:left;width:130px"/>
                    <input id="rate" name="rate" class="mini-textbox" labelField="true" label="汇率：" style="width:100%"
                           labelStyle="align:left;width:130px"/>
                    <input id="direction" name="direction" class="mini-combobox" labelField="true" label="交易方向："
                           emptyText="请选择交易方向" data="CommonUtil.serverData.dictionary.trading" required="false"
                           style="width:100%" labelStyle="align:left;width:130px"/>

                    <input id="dealSource" name="dealSource" class="mini-combobox" labelField="true" label="账簿划分："
                           emptyText="请选择账簿划分" data="CommonUtil.serverData.dictionary.dealSource" required="false"
                           style="width:100%" labelStyle="align:left;width:130px"/>
                </div>
                <div class="rightarea">
                    <input id="currency" name="currency" class="mini-combobox mini-mustFill" labelField="true"
                           value="USD" data="[{id:'USD',text:'美元'},{id:'CNY',text:'人民币'}]" label="限额币种："
                           labelStyle="align:left;width:130px" required="true" allowInput="false" style="width:100%;"
                           onvaluechanged="changeCurrency"/>

                    <input id="tradName" required="true" name="tradName" class="mini-buttonedit"
                           onbuttonclick="onPrdTradQuery" labelField="true" label="交易员：" emptyText="请选择交易员"
                           required="false" style="width:100%" labelStyle="align:left;width:130px"/>
                    <input id="trad" name="trad" class="mini-textbox" labelField="true" label="交易员ID："
                           emptyText="请选择交易员" required="false" style="width:100%" labelStyle="align:left;width:130px"/>
                    <input id="prdType" name="prdType" class="mini-buttonedit" onbuttonclick="onPrdTypeQuery"
                           labelField="true" label="产品类型：" emptyText="请选择产品类型" required="false" style="width:100%"
                           labelStyle="align:left;width:130px"/>
                    <input id="prdTypeCode" name="prdTypeCode" class="mini-textbox" labelField="true" label="类型代码："
                           style="width:100%" labelStyle="align:left;width:130px"/>

                    <input id="ccy" name="ccy" class="mini-buttonedit" onbuttonclick="onCcyQuery" labelField="true"
                           label="交易币种：" emptyText="请选择币种" required="false" style="width:100%"
                           labelStyle="align:left;width:130px"/>

                    <input id="accType" name="accType" class="mini-buttonedit" onbuttonclick="onAccTypeQuery"
                           labelField="true" label="账户类型：" emptyText="请选择账户类型" required="false" style="width:100%"
                           labelStyle="align:left;width:130px"/>

                    <input id="availAmountU" name="availAmountU" class="mini-spinner mini-mustFill input-text-strong"
                           labelField="true" label="美元限额上限(美元)：" emptyText="请输入限额金额" maxValue="999999999999999"
                           minValue="-999999999999999" changeOnMousewheel='false' format="n2" style="width:100%"
                           labelStyle="align:left;width:130px" onvaluechanged="changeUSDToCNY"/>
                    <input id="availAmount" name="availAmount" required="true"
                           class="mini-spinner mini-mustFill input-text-strong" labelField="true" enabled="false"
                           label="折人民币上限(元)：" emptyText="请输入限额金额" maxValue="9999999999999999" changeOnMousewheel='false'
                           format="n2" style="width:100%" labelStyle="align:left;width:130px"/>

                    <input id="term" name="term" class="mini-spinner" labelField="true" label="期限：" emptyText="请输入期限"
                           maxValue="999999999999" minValue="-9999999999999999" changeOnMousewheel='false'
                           decimalplaces="2" style="width:100%" labelStyle="align:left;width:130px"/>
                    <input id="prdCode" name="prdCode" class="mini-textbox" labelField="true" label="产品代码："
                           style="width:100%" labelStyle="align:left;width:130px"/>
                    <input id="limitId" name="limitId" class="mini-textbox" labelField="true" label="流水号："
                           style="width:100%" labelStyle="align:left;width:130px"/>
                    <input id="totalAmount" name="totalAmount" class="mini-textbox" labelField="true" label="累计额度: "
                           style="width:100%" labelStyle="align:left;width:130px"/>

                </div>
            </div>
        </div>
    </div>

    <div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
        <div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"
                                                                style="width:120px;" id="save_btn"
                                                                onclick="save">保存交易</a></div>
        <div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"
                                                                style="width:120px;" id="close_btn"
                                                                onclick="close">关闭界面</a></div>
    </div>
</div>
<script type="text/javascript">
    mini.parse();

    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var row = params.selectData;
    var url = window.location.search;
    var action = CommonUtil.getParam(url, "action");
    var form = new mini.Form("#field_form");
    var rate = searchRate();
    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
            visableBtn();
            initData();
        });
    });


    /******************************* 初始化按钮设置 *******************************************/
    function visableBtn() {
        mini.get("ccyCode").setVisible(false);
        mini.get("prdTypeCode").setVisible(false);
        mini.get("prdCode").setVisible(false);
        mini.get("limitId").setVisible(false);
        mini.get("rate").setVisible(false);
        mini.get("direction").setVisible(false);
        mini.get("dealSource").setVisible(true);
        mini.get("totalAmount").setVisible(false);
        mini.get("trad").setVisible(false);
        mini.get("cost").setVisible(false);
        mini.get("limitCount").setVisible(false);
        mini.get("term").setVisible(false);
        mini.get("bondTypeCode").setVisible(false);
        mini.get("bondType").setVisible(false);


        mini.get("accType").setVisible(false);
        mini.get("acctngtype").setVisible(false);
        mini.get("acctngtypeName").setVisible(false);

    }

    /******************************* 初始化数据 *******************************************/
    function initData() {
        if ($.inArray(action, ["edit", "detail"]) > -1) {//修改、详情
            form.setData(row);
            mini.get("ccy").setText(row.ccy);
            mini.get("prd").setText(row.prd);
            mini.get("prdType").setText(row.prdType);
            mini.get("tradName").setValue(row.tradName);
            mini.get("tradName").setText(row.tradName);
            mini.get("costName").setValue(row.costName);
            mini.get("costName").setText(row.costName);
            mini.get("limitCount").setValue(row.limitCount);
            mini.get("bondType").setText(row.bondType);
            mini.get("bondType").setValue(row.bondType);
            mini.get("accType").setText(row.accType);
            mini.get("accType").setValue(row.accType);

            var currency = mini.get("currency").getValue();
            if (currency == "CNY") {
                mini.get("availAmount").setEnabled(true);
                mini.get("quotaAmount").setEnabled(true);
                mini.get("availAmountU").setEnabled(false);
                mini.get("quotaAmountU").setEnabled(false);
            } else {
                mini.get("availAmount").setEnabled(false);
                mini.get("quotaAmount").setEnabled(false);
                mini.get("availAmountU").setEnabled(true);
                mini.get("quotaAmountU").setEnabled(true);
            }

            var prdCode = mini.get("prdCode").getValue();
            if (prdCode.indexOf("SECUR") != -1) {
                mini.get("accType").setVisible(true);
                mini.get("acctngtypeName").setVisible(true);
                mini.get("acctngtypeName").setText(row.acctngtypeName);
            } else {
                mini.get("accType").setVisible(false);
                mini.get("acctngtypeName").setVisible(false);
            }

            onCurrencyPairChange();
            if (action == "detail") {
                $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>外汇限额</a> >> 详情");
                form.setEnabled(false);
                mini.get("save_btn").setVisible(false);
            }
        }
    }

    /******************************* 按钮方法 *******************************************/
    //保存
    function save() {
        //表单验证！！！
        form.validate();
        if (form.isValid() == false) {
            mini.alert("表单填写有误!", "提示");
            return;
        }

        var saveUrl = null;
        if (action == "add") {
            saveUrl = "/IfsLimitTotalController/LimitConditionAdd";
        } else if (action == "edit") {
            saveUrl = "/IfsLimitTotalController/saveLimitConditionEdit";
        }
        var data = form.getData(true);
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: saveUrl,
            data: params,
            callback: function (data) {
                mini.alert("保存成功", '提示信息', function () {
                    top["win"].closeMenuTab();
                });
            }
        });
    }

    function close() {
        top["win"].closeMenuTab();
    }

    //取消
    function cancel() {
        top["win"].closeMenuTab();
    }

    /******************************* 选择框方法 *******************************************/
    //交易员信息
    function onPrdTradQuery() {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Common/limit/LimitTemplateTrad.jsp",
            title: "选择交易员信息列表",
            width: 900,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data == undefined) {
                        return;
                    }
                    var limitTemplate = "";
                    var limitTemplateCode = "";
                    if (data) {
                        for (var i = 0; i < data.length; i++) {
                            if (i == data.length - 1) {
                                //limitTemplate += data[i].dictValue+"||"+data[i].dictKey;
                                limitTemplate += data[i].dictValue + ",";
                                limitTemplateCode += data[i].dictKey + ",";
                            } else {
                                //limitTemplate += data[i].dictValue+"||"+data[i].dictKey+ ",";
                                limitTemplate += data[i].dictValue + ",";
                                limitTemplateCode += data[i].dictKey + ",";
                            }

                        }
                    }
                    //limitTemplate ="{"+limitTemplate+"}";
                    btnEdit.setValue(limitTemplate);
                    btnEdit.setText(limitTemplate);
                    mini.get("trad").setValue(limitTemplateCode);
                    btnEdit.focus();
                }
            }
        });
    }

    //限额类型控制
    function onCurrencyPairChange() {

    }

    //选择产品
    function onPrdQuery(e) {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Common/limit/LimitTemplatePrd.jsp",
            title: "选择产品列表",
            width: 900,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data == undefined) {
                        return;
                    }
                    var limitTemplate = "";
                    var limitTemplateCode = "";
                    if (data) {
                        for (var i = 0; i < data.length; i++) {

                            if (data[i].pcode == "SECUR") {
                                mini.get("accType").setVisible(true);
                                mini.get("acctngtypeName").setVisible(true);
                            } else {
                                mini.get("accType").setVisible(false);
                                mini.get("acctngtypeName").setVisible(false);
                            }

                            if (i == data.length - 1) {
                                limitTemplate += data[i].pdesccn;
                                limitTemplateCode += data[i].pcode + ",";
                            } else {
                                limitTemplate += data[i].pdesccn + ",";
                                limitTemplateCode += data[i].pcode + ",";
                            }

                        }
                    }
                    //limitTemplate ="{"+limitTemplate+"}";
                    btnEdit.setValue(limitTemplateCode);//产品中文描述
                    btnEdit.setText(limitTemplateCode);
                    mini.get("prdCode").setValue(limitTemplateCode);//产品代码
                    btnEdit.focus();
                }
            }
        });
    }

    //选择产品类型
    function onPrdTypeQuery(e) {
        var btnEdit = this;
        var prd = mini.get("prdCode").getValue();
        if (prd == "" || prd == null) {
            mini.alert("请先选择产品")
            return
        }

        mini.open({
            url: CommonUtil.baseWebPath() + "../../Common/limit/LimitTemplateType.jsp?action=" + prd,
            title: "选择产品类型列表",
            width: 900,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data == undefined) {
                        return;
                    }
                    var limitTemplate = "";
                    var limitTemplateCode = "";
                    if (data) {
                        for (var i = 0; i < data.length; i++) {
                            if (i == data.length - 1) {
                                limitTemplate += data[i].type + ",";
                                limitTemplateCode += data[i].descr + ",";
                            } else {
                                limitTemplate += data[i].type + ",";
                                limitTemplateCode += data[i].descr + ",";
                            }

                        }
                    }
                    mini.get("prdTypeCode").setValue(limitTemplate);
                    //limitTemplate ="{"+limitTemplate+"}";
                    btnEdit.setValue(limitTemplateCode);
                    btnEdit.setText(limitTemplateCode);
                    btnEdit.focus();
                }
            }
        });
    }

    //美元转人民币
    function changeUSDToCNY(e) {
        var availAmountU = mini.get("availAmountU").getValue();
        var quotaAmountU = mini.get("quotaAmountU").getValue();
        var rate = mini.get("rate").getValue();


        if (availAmountU != null && availAmountU != "" && rate != null && rate != "") {
            var availAmount = Number(availAmountU) * Number(rate);
            mini.get("availAmount").setValue(availAmount);
        }
        if (quotaAmountU != null && quotaAmountU != "" && rate != null && rate != "") {
            var quotaAmount = Number(quotaAmountU) * Number(rate);
            mini.get("quotaAmount").setValue(quotaAmount);
        }
        if (availAmountU == 0) {
            var availAmount = Number(availAmountU) * Number(rate);
            mini.get("availAmount").setValue(availAmount);
        }
        if (quotaAmountU == 0) {
            var quotaAmount = Number(quotaAmountU) * Number(rate);
            mini.get("quotaAmount").setValue(quotaAmount);
        }

        if (Number(availAmountU) != 0 && availAmountU < quotaAmountU) {
            mini.alert("限额上限要大于限额下限!", "提示");
            return;
        }
        // setChinese();
    }

    //选择币种
    function onCcyQuery(e) {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Common/limit/LimitTemplateCcy.jsp",
            title: "选择币种列表",
            width: 900,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data == undefined) {
                        return;
                    }
                    var limitTemplate = "";
                    var limitTemplateCode = "";
                    if (data) {
                        for (var i = 0; i < data.length; i++) {
                            if (i == data.length - 1) {
                                //limitTemplate += data[i].dictValue+"||"+data[i].dictKey;
                                limitTemplate += data[i].dictValue;
                                limitTemplateCode += data[i].dictKey + ",";
                            } else {
                                //limitTemplate += data[i].dictValue+"||"+data[i].dictKey+ ",";
                                limitTemplate += data[i].dictValue + ",";
                                limitTemplateCode += data[i].dictKey + ",";
                            }

                        }
                    }
                    //limitTemplate ="{"+limitTemplate+"}";
                    btnEdit.setValue(limitTemplate);
                    btnEdit.setText(limitTemplate);
                    mini.get("ccyCode").setValue(limitTemplateCode);
                    btnEdit.focus();
                }
            }
        });
    }

    //加载成本中心列表
    function loadCostList() {
        var btnEdit = this;
        var prd = mini.get("prdCode").getValue();
        var type = mini.get("prdTypeCode").getValue();
        var data = {prd: prd, type: type};
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Common/limit/LimitTemplateCost.jsp",
            title: "选择成本中心列表",
            width: 900,
            height: 600,
            onload: function () {
                var iframe = this.getIFrameEl();
                iframe.contentWindow.SetData(data);
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data == undefined) {
                        return;
                    }
                    var limitTemplate = "";
                    var limitTemplateCode = "";
                    if (data) {
                        for (var i = 0; i < data.length; i++) {
                            if (i == data.length - 1) {
                                //limitTemplate += data[i].dictValue+"||"+data[i].dictKey;
                                limitTemplate += data[i].dictValue + ",";
                                limitTemplateCode += data[i].dictKey + ",";
                            } else {
                                //limitTemplate += data[i].dictValue+"||"+data[i].dictKey+ ",";
                                limitTemplate += data[i].dictValue + ",";
                                limitTemplateCode += data[i].dictKey + ",";
                            }

                        }
                    }
                    //limitTemplate ="{"+limitTemplate+"}";
                    btnEdit.setValue(limitTemplate);
                    btnEdit.setText(limitTemplate);
                    mini.get("cost").setValue(limitTemplateCode);
                    btnEdit.focus();
                }
            }
        });
    }

    //选择账户类型
    function onAccTypeQuery() {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Common/limit/LimitTemplateAccType.jsp",
            title: "选择账户类型列表",
            width: 900,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data == undefined) {
                        return;
                    }
                    var limitTemplate = "";
                    var limitTemplateCode = "";
                    if (data) {
                        for (var i = 0; i < data.length; i++) {
                            if (i == data.length - 1) {
                                //limitTemplate += data[i].dictValue+"||"+data[i].dictKey;
                                limitTemplate += data[i].dictValue + ",";
                                limitTemplateCode += data[i].dictKey + ",";
                            } else {
                                //limitTemplate += data[i].dictValue+"||"+data[i].dictKey+ ",";
                                limitTemplate += data[i].dictValue + ",";
                                limitTemplateCode += data[i].dictKey + ",";
                            }

                        }
                    }
                    //limitTemplate ="{"+limitTemplate+"}";
                    btnEdit.setValue(limitTemplate);
                    btnEdit.setText(limitTemplate);
                    btnEdit.focus();
                }
            }
        });
    }

    //选择债券类型
    function onChangeBondType(e) {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Common/limit/LimitTemplateBond.jsp",
            title: "选择债券类型",
            width: 900,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data == undefined) {
                        return;
                    }
                    var limitTemplate = "";
                    var limitTemplateCode = "";
                    if (data) {
                        //console.log(data);
                        for (var i = 0; i < data.length; i++) {
                            if (i == data.length - 1) {
                                //limitTemplate += data[i].dictValue+"||"+data[i].dictKey;
                                limitTemplate += data[i].dictValue + ",";
                                limitTemplateCode += data[i].dictKey + ",";
                            } else {
                                //limitTemplate += data[i].dictValue+"||"+data[i].dictKey+ ",";
                                limitTemplate += data[i].dictValue + ",";
                                limitTemplateCode += data[i].dictKey + ",";
                            }

                        }
                    }
                    //limitTemplate ="{"+limitTemplate+"}";
                    btnEdit.setValue(limitTemplateCode);
                    btnEdit.setText(limitTemplateCode);
                    mini.get("bondType").setValue(limitTemplateCode);

                    mini.get("bondTypeCode").setValue(limitTemplate);
                    btnEdit.focus();
                }
            }
        });
    }

    //选择限额币种变化
    function changeCurrency(e) {
        if (e.value == "CNY") {
            mini.get("availAmount").setEnabled(true);
            mini.get("quotaAmount").setEnabled(true);
            mini.get("availAmountU").setEnabled(false);
            mini.get("quotaAmountU").setEnabled(false);
        } else {
            mini.get("availAmount").setEnabled(false);
            mini.get("quotaAmount").setEnabled(false);
            mini.get("availAmountU").setEnabled(true);
            mini.get("quotaAmountU").setEnabled(true);
        }
    }

    //选择会计类型
    function onActyQuery(e) {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Common/limit/LimitActy.jsp",
            title: "选择会计类型",
            width: 900,
            height: 600,
            onload: function () {
                var iframe = this.getIFrameEl();
                var data = {type1: "2", type2: "3"};//把会计归属类型传到小页面，"2"表示归属债券类，"3"表示归属客户及债券类
                iframe.contentWindow.SetData(data);
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    var limitTemplate = "";
                    var limitTemplateCode = "";
                    if (data) {
                        for (var i = 0; i < data.length; i++) {
                            if (i == data.length - 1) {
                                limitTemplate += data[i].acctdesc;
                                limitTemplateCode += data[i].acctngtype + ",";
                            } else {
                                limitTemplate += data[i].acctdesc + ",";
                                limitTemplateCode += data[i].acctngtype + ",";
                            }
                        }
                        mini.get("acctngtype").setValue(limitTemplateCode);

                        btnEdit.setValue(limitTemplate);
                        btnEdit.setText(limitTemplate);

                        btnEdit.focus();
                    }
                }

            }
        });
    }

    //产品表改变
    function changeProd(e) {
        mini.get("prdType").setValue("");
        mini.get("prdType").setText("");
    }

    /******************************* 功能方法 *******************************************/

    function searchRate(e) {
        CommonUtil.ajax({
            data: {},
            url: '/IfsLimitController/searchRate',
            callback: function (data) {
                mini.get("rate").setValue(data);
            }
        });
    }

    //生成中文金额
    function setChinese() {
        var fields = ['quotaAmountU', 'quotaAmount', 'availAmountU', 'availAmount'];
        for (element of fields) {
            doSetChinese(element);
        }
    }

    //生成中文金额
    function doSetChinese(field) {
        var fieldMIni = mini.get(field);
        var amt = CommonUtil.chineseNumber(fieldMIni.getValue());
        fieldMIni.setText(fieldMIni.getValue() + '      ' + amt);
    }
</script>