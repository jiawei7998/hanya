<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>产品与额度中类匹配关系</title>
  </head>

<body style="width:100%;height:100%;background:white">
<div class="mini-splitter" style="width:100%;height:100%;">
	<div size="80%" showCollapseButton="false">
		<fieldset class="mini-fieldset title">
		<legend>产品与额度中类匹配关系</legend>
		</fieldset>
		
		<div id="field_form" class="mini-fit area"  style="background:white">
            <fieldset>
            <div class="leftarea">
            	<input id="productCode" name="productCode"    labelStyle="text-align:right;" label="选择产品：" width="500px" class="mini-combobox"   labelField="true" onValueChanged="loadProductList"    required="true"   />
                <input id="creditNames" name="creditNames"  labelStyle="text-align:right;"class="mini-textbox" width="500px" labelField="true"  label="额度名称："    />
			</div>			
			<div class="rightarea">
                <input id="creditIds" name="creditIds"  labelStyle="text-align:right;"class="mini-buttonedit" width="500px" labelField="true" label="额度品种：" onbuttonclick="onButtonEdit"/>
                <input id="propertyLs" name="propertyLs"  labelStyle="text-align:right;"class="mini-textbox" width="500px" labelField="true"  label="优先级："  />
			</div>			
		</div>
		</fieldset>
	</div>		
    <div id="functionIds"  style="padding-top:50px;text-align:center;">
        <div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="save_btn"   onclick="save">保存</a></div>
        <div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="close_btn"   onclick="close">关闭</a></div>
    </div> 
</div>
<script type="text/javascript">

mini.parse();
//获取当前tab
var currTab = top["win"].tabs.getActiveTab();
var params = currTab.params;
var row=params.selectData;
var url=window.location.search;
var action=CommonUtil.getParam(url,"action");
var form=new mini.Form("#field_form");
mini.get("propertyLs").setVisible(false);
$(document).ready(function(){
    initData();
    loadProductList();
}); 

//初始化数据
function initData(){
	if($.inArray(action,["approve","detail"])>-1){
		mini.get("save_btn").setVisible(false);
		form.setEnabled(false);
	}

	if($.inArray(action,["edit","approve","detail"])>-1){
		mini.get("creditIds").setText(row.creditIds);
        form.setData(row);
	}
}

function save(){
    form.validate();
	if (form.isValid() == false) {
        mini.alert("信息填写有误，请重新填写","系统提示");
		return;
	}

    var saveUrl="";
    if(action == "add"){
        saveUrl="/edProdController/addRproductCredit";
    }else if(action == "edit"){
        saveUrl="/edProdController/updateRproductCredit";
    }
    var data=form.getData(true);
    var params=mini.encode(data);
    CommonUtil.ajax({
        url:saveUrl,
        data:params,
        callback:function(data){
            mini.alert('保存成功','提示信息',function(){
                top["win"].closeMenuTab();
            });
        }
	});

}

function onButtonEdit(e) {
    var btnEdit = this;
    mini.open({
        url: CommonUtil.baseWebPath() + "../../quota/config/PrdAndTypeRelationMini.jsp",
        title: " 额度品种",
        width: 1000,
        height: 800,
        ondestroy: function (action) {
        	var iframe = this.getIFrameEl();
            var data = iframe.contentWindow.GetData();
            data = mini.clone(data);        
	    	if( data== undefined){return;}
	    	var creditIds = "";
    		var creditNames = "";
    		var propertyLs = "";
	    	if( data){
	    		for(var i = 0; i <  data.length; i++){
	    			if(i ==  data.length - 1){
	    				creditIds +=  data[i].creditId;
	    				creditNames +=  data[i].creditName;
	    				propertyLs += i + 1 + "";
	    			}else{
	    				creditIds +=  data[i].creditId + ",";
	    				creditNames +=  data[i].creditName + ",";
	    				propertyLs += i + 1 + ",";
	    			}
	    		}
	    	}
	    	 btnEdit.setValue(creditIds);
             btnEdit.setText(creditIds);
            mini.get("creditNames").setValue(creditNames);
            mini.get("propertyLs").setValue(propertyLs);
        }
    });

}

//关闭
function close(){
    top["win"].closeMenuTab();
}

//加载成本中心列表
function loadProductList(){
    CommonUtil.ajax({
        data:{branchId:branchId},
    	url:'/ProductController/searchProduct',
        callback:function(data){
            var prdNo = mini.get("productCode");
            prdNo.setData(data.obj);
        }
    });	
}
</script>
</body>
</html>