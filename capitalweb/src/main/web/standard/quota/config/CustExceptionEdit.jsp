<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>额度调整维护</title>
  </head>

<body style="width:100%;height:100%;background:white">


	<div id="TcInstCreditEdit"  style="width: 100%; height: 100%;">
		<div id="field" style="padding:5px;">
			
			<table id="instCredit_form" width:100%;" class="mini-sltable">
				<tr>
					<td style="width:50%">
						<input id="instId" name="instId"    labelStyle="text-align:right;"label="客户编号：" width="330px" class="mini-buttonedit""  labelField="true"  onbuttonclick="onButtoninstId"   />       
                        
                    </td>
					<td style="width:50%">
						<input id="instName" name="instName"  labelStyle="text-align:right;"class="mini-textbox" width="330px" labelField="true" label="客户名称："   required="true"     />
					</td>
				</tr>
				
            </table>
               
                <table width:100%;>
                <tr>
                <td style="width:50%">
                   <input id="subInstId" name="subInstId"  labelStyle="text-align:right;"class="mini-buttonedit" width="330px" labelField="true"  label="减额度客户编号："onbuttonclick="onButtonsubInstId"    />
               </td>
                <td style="width:50%">
                   <input id="subInstName" name="subInstName"  labelStyle="text-align:right;"class="mini-textbox" width="330px" labelField="true"  label="减额度客户名称：" required="true"   />
               </td>
              
               </tr>
                </table>
                <table>
                 <tr>
                 <td style="width:50%">
                 <input id="comm" name="comm"  labelStyle="text-align:right;"class="mini-textbox" width="330px" labelField="true"  label="备注："  />
                   
                 </td>
                 <td>
                 <input id="creditNo" type="hidden"/>
		       	<input id="subInstId_old" type="hidden"/>
                 </td>
                 </tr>
                
                </table> 
    
				<table style="text-align:left;margin:auto;width:100%;" class="mini-sltable">
                    <tr>
                        <td colspan="3" style="text-align:center;">
                                <a class="mini-button" style="display: none"    onClick="save()" id="save_btn">保存</a>
                                <a id="cancel_btn" class="mini-button" style="display: none"    onclick="cancel()">取消</a>
                        </td>
                    </tr>
                </table>
			
		</div>
</div>
<script type="text/javascript">

mini.parse();

//获取当前tab
var currTab = top["win"].tabs.getActiveTab();
var params = currTab.params;
var row=params.selectData;
var url=window.location.search;
var action=CommonUtil.getParam(url,"action");
var form=new mini.Form("#field");

$(document).ready(function(){
	if($.inArray(action,["approve","detail"])>-1){
		mini.get("save_btn").setVisible(false);
		form.setEnabled(false);
	}

	if($.inArray(action,["edit","approve","detail"])>-1){
        form.setData(row);
        //额度中类编号
		mini.get("subInstId_old").setValue(row.subInstId);
		mini.get("creditId").setText(row.creditId);
		
	}else{//增加 
		
	}
});


function save(){
    form.validate();
	if (form.isValid() == false) {
        mini.alert("信息填写有误，请重新填写","系统提示");
		return;
	}

    var saveUrl="";
    if(action == "add"){
        saveUrl="/CreditManagerController/addInstCredit";
        
    }else if(action == "edit"){
        saveUrl="/CreditManagerController/updateInstCredit";
    }

    var data=form.getData(true);
    var params=mini.encode(data);
    CommonUtil.ajax({
        url:saveUrl,
        data:params,
        callback:function(data){
            mini.alert('保存成功','提示信息',function(){
                top["win"].closeMenuTab();
            });
        }
	});

}


//取消
function cancel(){
    top["win"].closeMenuTab();
}
function onButtoninstId(e) {
  

    var btnEdit = this;
    mini.open({
        url: CommonUtil.baseWebPath() + "../../quota/config/CustExceptionMini.jsp",
        title: "客户选择",
        width: 800,
        height: 600,
        ondestroy: function (action) {
        
            if (action == "ok") {
                var iframe = this.getIFrameEl();
                var data = iframe.contentWindow.GetData();
                data = mini.clone(data);    //必须
                if (data) {
                    btnEdit.setValue(data.party_id);
                    btnEdit.setText(data.party_id);
                    mini.get("instName").setValue(data.party_name);
                    search(10,0);
                }
            }

        }
    });

}
function onButtonsubInstId(e) {
    var btnEdit = this;
    mini.open({
        url: CommonUtil.baseWebPath() + "../../quota/config/CustExceptionMini.jsp",
        title: "客户选择",
        width: 800,
        height: 600,
        ondestroy: function (action) {
            if (action == "ok") {
                var iframe = this.getIFrameEl();
                var data = iframe.contentWindow.GetData();
                data = mini.clone(data);    //必须
                if (data) {
                	btnEdit.setValue(data.party_id);
                    btnEdit.setText(data.party_id);
                    mini.get("subInstName").setValue(data.party_name);
                    btnEdit.focus();
                }
            }

        }
    });

}




</script>
</body>
</html>