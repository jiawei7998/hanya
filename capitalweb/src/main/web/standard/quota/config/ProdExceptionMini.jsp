<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>产品信息</title>
</head>
<body style="width:100%;height:100%;background:white">
    <fieldset class="mini-fieldset title">
    <legend>信息查询</legend>
    <div id="search_form"> 
     <input id="party_name"  class="mini-textbox"   label="产品名称："  labelField="true" labelStyle="text-align:right;" width="330px" 
        />       
        <input id="party_shortname" class="mini-textbox"   label="产品编号：" labelField="true"   width="330px" labelStyle="text-align:right;"          	
         />   
        <span style="float:right;margin-right: 150px">
            <a class="mini-button" style="display: none"  id="search_btn"  onclick="query()">查询</a>
            <a class="mini-button" style="display: none"  id="clear_btn"  onclick="clear()">清空</a>
        </span>
    </div>
    </fieldset>     

   <div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:80%;" idField="id"  allowAlternating="true"
		 allowResize="true" sortMode="client" allowAlternating="true" onrowdblclick="onRowDblClick">
        <div property="columns">
			<div type="indexcolumn" width="50px" headerAlign="center">序号</div>
			<div field="prdName" width="200px" align="center"  headerAlign="center">产品名称</div> 
			<div field="id" width="200px" align="center"  headerAlign="center" >产品编号</div>     
		</div>
	</div>
</div> 

<script>

mini.parse();


var url = window.location.search;
var prdNo = CommonUtil.getParam(url, "prdNo");
var prdName = CommonUtil.getParam(url, "prdName");
var form = new mini.Form("#search_form");
var grid= mini.get("datagrid");
var editForm = document.getElementById("editForm1");  
var row="";


//绑定表单
grid.load();
var db = new mini.DataBinding();
db.bindForm("editForm", grid);

//初始化
$(document).ready(function() {
	query();
});
grid.on("beforeload", function (e) {
	e.cancel = true;
	 var pageIndex = e.data.pageIndex; 
	 var pageSize = e.data.pageSize;
	 search(pageSize,pageIndex);
});



// 按钮查询
function query(){
		search(grid.pageSize,0);
	}



function clear(){
    var form=new mini.Form("search_form");
    form.clear();
    search(10,0);

}


/* 查询 */
	function search(pageSize,pageIndex){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统提示");
			return;
		}

		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		data['partyName']=mini.get('party_name').getValue();
		data['partyShortname']=mini.get('party_shortname').getValue();
		var params = mini.encode(data);		 
		CommonUtil.ajax({
		url:"/TdCustEcifController/searchAllProtectName",
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
function GetData() {
	var grid= mini.get("datagrid");
        var row = grid.getSelected();
        return row;
    }
    
    //双击行选择
    function onRowDblClick(){
        onOk();
    }

    function CloseWindow(action) {
        if (window.CloseOwnerWindow)
            return window.CloseOwnerWindow(action);
        else
            window.close();
    }

    function onOk() {
        CloseWindow("ok");
    }

    //关闭窗口
    function onCancel() {
        CloseWindow("cancel");
    }
 
</script>
 </body>

</html>