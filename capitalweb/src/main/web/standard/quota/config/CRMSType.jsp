<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/hideBtn.js"></script>
    <title>额度CRMS中类品种</title>
</head>
<body style="width:100%;height:100%;background:white">
    <fieldset class="mini-fieldset title">
    <legend>信息查询</legend>
    <div id="search_form">        
        <input id="creditId"  class="mini-textbox"   label="额度代码："  labelField="true" labelStyle="text-align:right;" width="330px" 
        />
        <input id="creditName" class="mini-textbox"   label="额度名称：" labelField="true"   width="330px" labelStyle="text-align:right;" 
        />
      
        <span style="float:right;margin-right: 150px">
            <a class="mini-button" style="display: none"  id="search_btn"  onclick="query()">查询</a>
            <a class="mini-button" style="display: none"  id="clear_btn"  onclick="clear()">清空</a>
        </span>
    </div>
    </fieldset>
    <span style="margin:2px;display: block;">
        <a class="mini-button" style="display: none"  id="add_btn"  onClick="add();">新增</a>
        <a class="mini-button" style="display: none"  id="edit_btn"  onClick="modify();">修改</a>
        <a class="mini-button" style="display: none"  id="delete_btn"  onClick="del();">删除</a>
    </span>
    <div class="mini-fit" >
	<div id="datagrid" class="mini-datagrid borderAll" style="width:80%;height:100%;" idField="id"  allowAlternating="true"
		onrowdblclick="onRowDblClick" allowResize="true" sortMode="client" allowAlternating="true">
		<div property="columns">
			<div type="indexcolumn" width="50px" headerAlign="center">序号</div>
			<div field="creditId" width="100px" align="center"  headerAlign="center" >额度代码</div>     
			<div field="creditName" width="100px" align="center"  headerAlign="center" >额度名称</div>     
			<!-- <div field="riskLevel" width="100px" align="center"  headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'riskLevel'}" >风险级别</div> -->     				
		</div>
	</div>
</div>   



<script>

mini.parse();

var url = window.location.search;
var prdNo = CommonUtil.getParam(url, "prdNo");
var prdName = CommonUtil.getParam(url, "prdName");
var form = new mini.Form("#search_form");
var grid=mini.get("datagrid");
var row="";

$(document).ready(function() {	
	query();
});
grid.on("beforeload", function (e) {
	e.cancel = true;
	var pageIndex = e.data.pageIndex; 
	var pageSize = e.data.pageSize;
	search(pageSize,pageIndex);
});




//增删改查
function add(){
	var url = CommonUtil.baseWebPath() + "../../quota/config/CRMSTypeEdit.jsp?action=add";
	var tab = { id: "CRMSTypeEditadd", name: "CRMSTypeEditadd", title: "额度中类新增", url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
	var paramData = {selectData:""};
	CommonUtil.openNewMenuTab(tab,paramData);
}
//详情
function onRowDblClick(e) {
	var url = CommonUtil.baseWebPath() +"../../quota/config/CRMSTypeEdit.jsp?action=detail";
	var tab = {id: "CRMSTypeEditDetail",name:"CRMSTypeEditDetail",url:url,title:"额度中类品种详情",
				parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
	var paramData = {selectData:grid.getSelected()};
	CommonUtil.openNewMenuTab(tab,paramData);
}
//清空
function clear(){
    form.clear();
    query();

}


//修改
function modify() {
   var row = grid.getSelected(); 
    if (row) {
        var url = CommonUtil.baseWebPath() + "../../quota/config/CRMSTypeEdit.jsp?action=edit";
        var tab = { id: "CRMSTypeEditedit", name: "CRMSTypeEditedit", title: "额度中类修改", url: url, parentId: top["win"].tabs.getActiveTab().name,showCloseButton:true };
        var paramData = {selectData:row};
		CommonUtil.openNewMenuTab(tab,paramData);
    } else {
        mini.alert("请选择一条数据");
    }
}



//按钮查询
 
function query(){
		search(grid.pageSize,0);
	}
/* 查询 */
	function search(pageSize,pageIndex){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统提示");
			return;
		}
		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:"/edRiskController/getTcEdRisksPage",
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}

    //删除
    function del(){
        var rows=grid.getSelecteds();
        if(rows.length==0){
            mini.alert("请选中一行","提示");
            return;
        }
        mini.confirm("您确认要删除吗?","系统警告",function(value){   
            if (value=='ok'){   
                var data=rows[0];
                params=mini.encode(data);
                CommonUtil.ajax( {
                    url:"/edRiskController/deleteTcEdRisk/",
                    data:params,
                    callback : function(data) {
                        mini.alert("删除成功.","系统提示");
                        search(10,0);
                    }
                });
            }
        });
    }

 
</script>
 </body>

</html>