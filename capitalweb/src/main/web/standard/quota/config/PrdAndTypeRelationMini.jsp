<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%> 
<html>
<head>
    <title>额度产品配置列表</title>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script> 
</head>  
<body style="width:100%;height:100%;background:white">
      <table>
          <tr>
            <td style="float:left;margin:5%px;">      
            <a id="save_btn" class='mini-button blue'    onclick="save">保存</a>
            
          </td>
          </tr>
          
      </table>
     <div class="mini-fit" >
          
            <fieldset   style="width:45%;height:99%;float:left;margin:0%px;padding:0%px;">
                <legend><label id="labell">已选表单</label></legend>
                
                <div class="mini-toolbar" >
                    <a id="up_move" class='mini-button'   onclick="upItem">上移</a>
                    <a id="down_move" class='mini-button'    onclick="downItem">下移</a>
                </div>
                    <div id="listbox1" class="mini-listbox"  showCheckBox="true" multiSelect="true"
                    >
                        <div property="columns">
                            <div type="indexcolumn" headerAlign="center" align="center" >序号</div>
                            <div field="creditId" header="额度品种："headerAlign="center"  align="center" width="40px"></div>
                            <div field="creditName" header="额度名称：" headerAlign="center" align="center" ></div>
                            
                        </div>
                    </div>
            </fieldset>
            
            <div style="width:5%;height:99%;float:left;text-align:center;">             
            <div style="margin-bottom:10px; margin-top:50px"><a class="mini-button" style="display: none"  style="width: 40px;"  onclick="add">&lt;</a></div>
            <!-- 
            <div style="margin-bottom:10px; "><a class="mini-button" style="display: none"  style="width: 40px;" onclick="addAll">&lt;&lt;</a></div>
            <div style="margin-bottom:10px; "><a class="mini-button" style="display: none"  style="width: 40px;" onclick="removeAll">&gt;&gt;</a></div>
             -->
            <div style="margin-bottom:10px; "><a class="mini-button" style="display: none"  style="width: 40px;" onclick="removes">&gt;</a></div>
            </div>
               
            <fieldset style="width:45%;height:99%;float:right;margin:0px;padding:0px;">
                <legend><label id="labell">可选表单</label></legend>
                <table id="formOptional_grid" >
                    <tr >
                        <td>
                            <input id="creditId" name="creditId" class="mini-textbox" 
                            vtype="maxLength:100" labelField="true"  label="额度品种："
                             labelStyle="text-align:right;width:50%px;"  width="100%"/>
                        </td>
                        <td>
                            <input id="creditName" name="creditName" class="mini-textbox"  labelField="true"  
                            label="额度名称：" labelStyle="text-align:right;width:50%px;"  width="100%"
                            />
                        </td> 
                    </tr>
                    <tr>
                        <td>
                            <a class='mini-button'  style='vertical-align:middle;'  onclick="search">查询</a>
                            <a class='mini-button'  style='vertical-align:middle;'  onclick="clear">清空</a>
                           
                        </td>
                    </tr>
                </table>
                
                <div id="listbox2" class="mini-listbox" style="height:50%px;" 
                    showCheckBox="true" multiSelect="false"  onbeforeactivechanged="before">
                   
                    <div property="columns">
                        <div type="indexcolumn" headerAlign="center" align="center">序号</div>
                        <div field="creditId" header="额度品种：" align="center" headerAlign="center" width="40px"></div>
                        <div field="creditName" header="额度名称：" headerAlign="center" align="center" ></div>
                        
                    </div>
                </div>
            </fieldset>
          </div>
            </div>
        </div>
    
 </body>

</body>
<script>
	$(document).ready(function() {
		mini.parse();

		init();

	});

	function init() {
		//初始化可选表单
		search();
		//初始化已选表单列表
		searchSelectedForm();
	}

	function before() {
		//查询可选表单
		search();
	}

	//上移
	function upItem() {
		var listbox1 = mini.get("listbox1");
		var items = listbox1.getSelecteds();
		for (var i = 0, l = items.length; i < l; i++) {
			var item = items[i];
			var index = listbox1.indexOf(item);
			listbox1.moveItem(item, index - 1);
		}
	}
	//下移
	function downItem() {
		var listbox1 = mini.get("listbox1");
		var items = listbox1.getSelecteds();
		for (var i = items.length - 1; i >= 0; i--) {
			var item = items[i];
			var index = listbox1.indexOf(item);
			listbox1.moveItem(item, index + 1);
		}
	}

	//向左移动一个（listbox2==》listbox1）
	function add() {
		var listbox1 = mini.get("listbox1");
		var listbox2 = mini.get("listbox2");
		var items = listbox2.getSelecteds();
		listbox2.removeItems(items);
		listbox1.addItems(items);
	}

	//向左移动全部（listbox2==》listbox1） 
	function addAll() {
		var listbox1 = mini.get("listbox1");
		var listbox2 = mini.get("listbox2");
		var items = listbox2.getData();
		listbox2.removeItems(items);
		listbox1.addItems(items);
	}

	//向右移动一个（listbox1==》listbox2） 
	function removes() {
		var listbox1 = mini.get("listbox1");
		var listbox2 = mini.get("listbox2");
		var items = listbox1.getSelecteds();
		listbox1.removeItems(items);
		listbox2.addItems(items);
	}

	//向右移动全部（listbox1==》listbox2） 
	function removeAll() {
		var listbox1 = mini.get("listbox1");
		var listbox2 = mini.get("listbox2");
		var items = listbox1.getData();
		listbox1.removeItems(items);
		listbox2.addItems(items);
	}

	//初始化已选表单列表
	function searchSelectedForm() {
		var creditId = mini.get("creditId").getValue();

		CommonUtil.ajax({
			url : "/edRiskController/getChooseEdRisks",
			data : {
				"creditId" : creditId
			},
			callback : function(data) {
				var grid = mini.get("listbox1");
				grid.setData(data.obj);

				var filterList = new Array();
				$.each(data.obj, function(i, n) {
					filterList.push(n.creditId);
				});
				searchOptionalForm(1000, 0, filterList);
			}
		});
	}

	//按钮事件---查询可选表单
	function search() {
		var filterList = new Array();
		var listbox1 = mini.get("listbox1").getData();
		if (listbox1.length == 0) {
			searchOptionalForm(1000, 0, null);
		} else {
			$.each(listbox1, function(i, n) {
				filterList.push(n.creditId);
				searchOptionalForm(1000, 0, filterList);
			});
		}

	}

	//初始化/查询 可选表单
	function searchOptionalForm(pageSize, pageIndex) {
		var form = new mini.Form("formOptional_grid");
		form.validate();
		if (form.isValid() == false)
			return;//表单验证
		var data = form.getData();//获取表单数据
		data['pageNumber'] = pageIndex + 1;
		data['pageSize'] = 1000;
		data['branchId'] = top['win'].branchId;
		data['useFlag'] = 1;
		var param = mini.encode(data); //序列化成JSON
		CommonUtil.ajax({
			url : "/edRiskController/getTcEdRisksPage",
			data : param,
			callback : function(data) {
				var grid = mini.get("listbox2");
				//设置数据
				grid.setData(data.obj.rows);
			}
		});
	}

	//清空
	function clear() {
		var form = new mini.Form("formOptional_grid");
		form.clear();
		search();
	}

	function GetData() {
		var grid = mini.get("listbox1");
		var row = grid.getData();
		return row;
	}

	//保存
	function save() {
		onOk();

	}
	function onOk() {
		CloseWindow("ok");
	}
	//关闭窗口
	function onCancel() {
		CloseWindow("cancel");
	}
	function CloseWindow(action) {
		if (window.CloseOwnerWindow)
			return window.CloseOwnerWindow(action);
		else{
			window.close();
		}
	}
</script>

</html>