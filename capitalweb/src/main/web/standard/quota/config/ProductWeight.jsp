<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/hideBtn.js"></script>
<title>产品分配权重</title>
</head>
<body style="width:100%;height:100%;background:white">

<div>
	<fieldset class="mini-fieldset">
		<legend>权重规则</legend> 
			<div id="search_detail_form" style="width:100%" cols="6">
				<div class="leftarea">
					<input id="productCode" name="productCode"  class="mini-buttonedit"  labelField="true"  label="选择产品："  required="true"  labelStyle="text-align:left;width:130px;" style="width:100%;" onbuttonclick="onButtonsubInstId"/>
					<input id="productName" name="productName"  class="mini-hidden"  labelField="true"  label="产品名字："  required="true"  labelStyle="text-align:left;width:130px;" style="width:100%;"/>
					<input id="ruleMin" name="ruleMin" class="mini-spinner"  labelField="true"  label="规则最小值(天)："  minValue="0" maxValue="9999999999999"  changeOnMousewheel="false" required="true"  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
					<input id="containsMin" name="containsMin"  class="mini-combobox" labelField="true"  label="是否包含最小值："  value="0" required="true"     labelStyle="text-align:left;width:130px;" style="width:100%;"  data = "CommonUtil.serverData.dictionary.YesNo" />
					
				</div>
				<div class="rightarea">
					<!-- <input id="dueDate" name="dueDate" class="mini-datepicker" labelField="true"  label="到期日："  required="true"       labelStyle="text-align:left;width:130px;" style="width:100%;"   /> -->
					<input id="weight" name="weight" class="mini-spinner" labelField="true"  minValue="0" maxValue="100" label="权重值%：" required="true"  labelStyle="text-align:left;width:130px;" style="width:100%;" labelStyle="text-align:right;" emptyText="请输入一个0-100之间的权重值" />
					<input id="ruleMax" name="ruleMax" class="mini-spinner"  labelField="true"  label="规则最大值(天)："  minValue="0" maxValue="9999999999999"  changeOnMousewheel="false" required="true"  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
					<input id="containsMax" name="containsMax"  class="mini-combobox" labelField="true"  label="是否包含最大值："  value="0" required="true"     labelStyle="text-align:left;width:130px;" style="width:100%;"  data = "CommonUtil.serverData.dictionary.YesNo" />
				</div>
			</div>
	</fieldset>
</div>

<span style="margin: 2px; display: block;"> 
    <a  id="commit_btn" class="mini-button" style="display: none"    onclick="commit()">保存</a>
	<a  id="add_btn" class="mini-button" style="display: none"   onclick="add()">新增</a>
    <a  id="edit_btn" class="mini-button" style="display: none"   onclick="edit()">修改</a>
	<a  id="delete_btn" class="mini-button" style="display: none"   onclick="del()">删除</a>
	<a id="export_btn" class="mini-button" style="display: none"  onclick="exportData()">导出</a>
	<a  id="update" class="mini-button" style="display: none"  onClick="update()">更新衍生品授信权重</a>
	<!-- 为了生产中现劵买卖的历史数据，上新系统时占用授信 -->
<!-- 	<a  id="start_btn" class="mini-button" style="display: none"  onClick="updateBond()" >占用现劵买卖数据</a> -->

 </span> 
<div class="mini-fit" >
	<div id="datagrid" class="mini-datagrid" style="width:100%;height:100%;" allowResize="true" sizeList="[20,50,100]" pageSize="20"
        allowCellEdit="true" allowCellSelect="true" multiSelect="true" editNextOnEnterKey="true"  editNextRowCell="true">
		<div property="columns">
			<div type="indexcolumn" width="50px" headerAlign="center">序号</div>
            <div field="productCode" width="80" align="center" headerAlign="center"  >产品编号</div>
            <div field="productName" width="140" align="center" headerAlign="center"  >产品名称</div>
            <div field="ruleMin" width="140px" align="center" headerAlign="center" >规则最小值(天)</div> 
            <div field="containsMin" width="120" align="center" headerAlign="center"  renderer="CommonUtil.dictRenderer" data-options="{dict:'YesNo'}">是否包含最小值</div>
            <div field="ruleMax" width="120" align="center" headerAlign="center">规则最大值(天)</div>
            <div field="containsMax" width="120px" align="center"  headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'YesNo'}">是否包含最大值</div>                            
<!--             <div field="dueDate" width="150" align="center" allowSort="true" headerAlign="center">到期日</div>
 -->            <div field="weight" width="150px" align="center"  headerAlign="center" allowSort="true">权重值%</div>
            <div field="operator" width="150" align="center" allowSort="true" headerAlign="center">操作员</div>
            <div field="dateTime" width="150px" align="center"  headerAlign="center" allowSort="true">操作时间</div>
		</div>
	</div>
</div>   

<script>
	mini.parse();
	var url = window.location.search;
	var prdNo = CommonUtil.getParam(url, "prdNo");
	var prdName = CommonUtil.getParam(url, "prdName");
	var form = new mini.Form("#search_detail_form");
	var grid = mini.get("datagrid");
	var row = "";
	mini.get('weight').setValue("100");
	grid.on("beforeload", function(e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex;
		var pageSize = '50';
		search(pageSize, pageIndex);
	});

	$(document).ready(function() {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
			//loadProductList();
		});
	});
	
	//增加
	function add() {
		var detailForm = new mini.Form("#search_detail_form");
		var flag = detailForm.validate();
		if (detailForm.isValid() == false) {
			mini.alert("信息填写有误，请重新填写", "系统提示");
			return;
		}
		var detailData = detailForm.getData(true);
		
		//比较
		var conData = grid.getData();
		
		var ruleNewMin = parseFloat(detailData.ruleMin);
		var ruleNewMax = parseFloat(detailData.ruleMax);
		var containsNewMin = detailData.containsMin;
		var containsNewMax = detailData.containsMax;
		var ruleMin;
		var ruleMax;
		var containsMin;
		var containsMax;
		var addFlag = true;//能否添加
		if(ruleNewMax != 0 && ruleNewMin >= ruleNewMax){
			mini.alert("规则最大值要大于规则最小值", "系统提示");
			return;
		}
		if(ruleNewMin == 0 && ruleNewMax == 0){
			mini.alert("规则最大值和规则最小值不能同时为0", "系统提示");
			return;
		}
        for(var i=0;i<conData.length;i++){
        	ruleMin = parseFloat(conData[i].ruleMin);
        	ruleMax = parseFloat(conData[i].ruleMax);
        	containsMin = conData[i].containsMin;
        	containsMax = conData[i].containsMax;
        	
        	
        	if(ruleNewMin == 0){//只有规则最大值 即X<ruleNewMax
        		if(ruleMin == 0){
        			addFlag = false;
    				break;
        		}else if(ruleNewMax > ruleMin){
       				addFlag = false;
       				break;
    			}else if(ruleNewMax == ruleMin){
        			if(containsMin == '1' && containsNewMax == '1'){
        				addFlag = false;
        				break;
        			}
        		}
        		continue;
        	}
        	
        	if(ruleNewMax == 0 ){//只有规则最小值 即X>ruleNewMin
        		if(ruleMax == 0){
        			addFlag = false;
    				break;
        		}else if(ruleNewMin < ruleMax){
       				addFlag = false;
       				break;
    			}else if(ruleNewMin == ruleMax){
        			if(containsMax == '1' && containsNewMin == '1'){
        				addFlag = false;
        				break;
        			}
        		}
        		continue;
        	}
        	
        	if(ruleNewMin != 0 && ruleNewMax != 0){
        		
        		if(ruleNewMin < ruleMin){ //最小值小于已经录入最小值
            		if(ruleNewMax < ruleMin){
            			continue;
            		}else if(ruleNewMax == ruleMin){
            			if(containsNewMax == '1' && containsMin == '1'){
            				addFlag = false;
            				break;
            			}
            		}else if(ruleNewMin > ruleMax){
            			addFlag = false;
        				break;
            		}
            	}else if(ruleNewMin == ruleMin){
           			addFlag = false;
       				break;
            	}else if(ruleNewMin > ruleMin && ruleNewMin <ruleMax){
            		addFlag = false;
    				break;
            	}else if(ruleNewMin == ruleMax){
            		if(containsMax == '1' && containsNewMin == '1'){
        				addFlag = false;
        				break;
        			}
            	}
        	}
        	
        }
        if(!addFlag){
        	mini.alert("录入信息有重叠，请重新录入规则明细。", "系统提示");
			return;
        }
        mini.get("commit_btn").setEnabled(true);
		mini.get("edit_btn").setEnabled(true);
		mini.get("delete_btn").setEnabled(true);
		
		addRow(detailData);

	}
	function addRow(detailData) {
		var arr = searchEduTermType();
		if (arr == null || arr.length == 0) {
			return;
		}
		var termTypeChinese;
		for (var i = 0; i < arr.length; i++) {
			if (detailData.termType == arr[i].id) {
				termTypeChinese = arr[i].text;
			}
		}
		var dateTime = getNowFormatDate();
		var row = {
			productCode : detailData.productCode,
			productName : detailData.productName,
			ruleMin : detailData.ruleMin,
			containsMin : detailData.containsMin,
			ruleMax : detailData.ruleMax,
			containsMax : detailData.containsMax,
			dueDate : detailData.dueDate,
			dateTime : dateTime,
			operator : '<%=__sessionUser.getUserId() %>',
			weight : detailData.weight
		};
		grid.addRow(row);
		
		//var detailForm = new mini.Form("#search_detail_form");
		//detailForm.clear();
	}

	function searchEduTermType() {
		var termArr = CommonUtil.serverData.dictionary.EduTermType;
		return termArr;
	}

	//提交
	function commit() {
		form.validate();
		if (form.isValid() == false) {
			mini.alert("信息表填写有误，请重新填写", "系统提示");
			return;
		}
		var param = form.getData(true);
		var rows = grid.getData();
		if (rows.length == 0) {
			return;
		}
		var params = new Array();
		
		for (var int = 0; int < rows.length; int++) {
			var paramSub = {};
			paramSub["productCode"] = rows[int].productCode;
			paramSub["productName"] = rows[int].productName;
			paramSub["ruleMin"] = rows[int].ruleMin;
			paramSub["containsMin"] = rows[int].containsMin;
			paramSub["ruleMax"] = rows[int].ruleMax;
			paramSub["containsMax"] = rows[int].containsMax;
			paramSub["dueDate"] = rows[int].dueDate;
			paramSub["operator"] = rows[int].operator;
			paramSub["weight"] = rows[int].weight;
			paramSub["dateTime"] =   rows[int].dateTime;
			params.push(paramSub);
		}
		param["ProductWeight"] = params;
		param["branchId"] = branchId;
		param = mini.encode(param);
		CommonUtil.ajax({
			url : "/edCustController/productWeightSave",
			data : param,
			callback : function(data) {
				mini.alert("保存成功", "系统提示");
			}
		});
	}

	//删除
	function del() {
		var row = grid.getSelected();
		if (!row) {
			mini.alert("请选中要移除的一行", "提示");
			return;
		}
		grid.removeRow(row);
	}

	//修改
	function edit() {
		var row = grid.getSelected();
		if (!row) {
			mini.alert("请选中一行进行修改!", "提示");
			return;
		}
		//修改时删除此行信息
		grid.removeRow(row);
		
		mini.get("commit_btn").setEnabled(false);
		mini.get("edit_btn").setEnabled(false);
		mini.get("delete_btn").setEnabled(false);
		
		var detailForm = new mini.Form("#search_detail_form");
		detailForm.setData(row);

	}

	//加载成本中心列表
	function loadProductList() {
		CommonUtil.ajax({
			data : {
				branchId : branchId
			},
			url : '/ProductController/searchProduct',
			callback : function(data) {
				var prdNo = mini.get("productCode");
				prdNo.setData(data.obj);
			}
		});
	}
	
	function onButtonsubInstId(e) {
	    var btnEdit = this;
	    mini.open({
	        url: CommonUtil.baseWebPath() + "../../quota/config/ProdExceptionMini.jsp",
	        title: "产品选择",
	        width: 800,
	        height: 500,
	        ondestroy: function (action) {
	            if (action == "ok") {
	                var iframe = this.getIFrameEl();
	                var data = iframe.contentWindow.GetData();
	                data = mini.clone(data);    //必须
	                if (data) {
	                	btnEdit.setValue(data.prdNo);
	                    btnEdit.setText(data.prdName);
	                    /* mini.get("productCode").setValue(data.party_name);*/
	                    //mini.get("productName").setValue(data.moduleName); 
	                    mini.get("productName").setValue(data.prdName);
	                    search(20,0,data.prdNo);
	                    btnEdit.focus();
	                }
	            }
	        }
	    });
	}
	

	function search(pageSize,pageIndex,productCode){

		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		data['productCode']=productCode;
		var params = mini.encode(data);		 
		CommonUtil.ajax({
			url:"/TdCustEcifController/searchSingleProtectName",
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	//获取当前时间，格式YYYY-MM-DD
    function getNowFormatDate() {
        var date = new Date();
        var seperator1 = "-";
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        var strDate = date.getDate();
        if (month >= 1 && month <= 9) {
            month = "0" + month;
        }
        if (strDate >= 0 && strDate <= 9) {
            strDate = "0" + strDate;
        }
        var currentdate = year + seperator1 + month + seperator1 + strDate;
        return currentdate;
    }
  	//导出
	function exportData() {
		mini.confirm("您确认要导出Excel吗?","系统提示", 
			function (action) {
				if (action == "ok"){
			 		var form = new mini.Form("#search_detail_form");
					var data = form.getData(true);
					var fields = null;
					for(var id in data){
						fields += '<input type="hidden" id="' + id + '" name="'+ id +'" value="' + data[id] + '">';
					}
					var urls = CommonUtil.pPath + "/sl/TdCustEcifController/exportExcel";                                                                                                                         
					$('<form action="'+ urls +'" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();   
				}
			}
		);
	}
  	
  	//更新授信占用权重的比例
	function update(){
		mini.confirm("您确认要更新衍生品占用授信?","系统警告",function(value){
			if(value=="ok"){
				var form = new mini.Form("#search_detail_form");
				var data = form.getData(true);
				
				CommonUtil.ajax({
					url:"/TdCustEcifController/updateCustLoanAmt",
					data:data,
					callback : function(data) {
						if(data){
  		            		mini.alert("更新成功!");
   		            	} else {
   		            		mini.alert("更新失败!");
   		            	}
					}
				});
			}
		});
    }
  	
  	
	function updateBond(){
		var form = new mini.Form("#search_detail_form");
		var data = form.getData(true);
		
		CommonUtil.ajax({
			url:"/TdCustEcifController/updateCustBondAmt",
			data:data,
			callback : function(data) {
	            mini.alert("更新成功!");
			}
		});
	}
	
</script>
</body>
</html>