<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/hideBtn.js"></script>
<title>风险管理</title>
</head>
<body style="width:100%;height:100%;background:white">

<div>
	<fieldset class="mini-fieldset">
		<legend>风险管理</legend> 
			<div id="search_detail_form" style="width:100%" cols="6">
				<div class="leftarea">
					<input id="productCode" name="productCode"  class="mini-buttonedit"  labelField="true"  label="选择产品："  required="true"  labelStyle="text-align:left;width:130px;" style="width:100%;" onbuttonclick="onButtonsubInstId"/>
					<input id="checkLimit" name="checkLimit" class="mini-combobox"  labelField="true"  value="0" label="校验授信："   required="true"  labelStyle="text-align:left;width:130px;" style="width:100%;"  data = "CommonUtil.serverData.dictionary.YesNo"  />
				</div>
				<div class="rightarea">
					<input id="checkAccess" name="checkAccess" class="mini-combobox"  labelField="true"  value="0" label="校验准入："   required="true"  labelStyle="text-align:left;width:130px;" style="width:100%;"  data = "CommonUtil.serverData.dictionary.YesNo"  />
					<input id="checkRisk" name="checkRisk" class="mini-combobox"  labelField="true"  	value="0" label="校验单笔风险："   required="true"  labelStyle="text-align:left;width:130px;" style="width:100%;"  data = "CommonUtil.serverData.dictionary.YesNo"  />
				</div>
			</div>
	</fieldset>
</div>

<span style="margin: 2px; display: block;"> 
    <a  id="save_btn" class="mini-button" style="display: none"    onclick="commit()">保存</a>
	<a  id="add_btn" class="mini-button" style="display: none"   onclick="add()">增加</a>
    <a  id="edit_btn" class="mini-button" style="display: none"   onclick="edit()">修改</a>
	<a  id="delete_btn" class="mini-button" style="display: none"   onclick="del()">删除</a>
	<a  id="expt_btn" class="mini-button" style="display: none"  onclick="exportAcup()">导出</a>
	
 </span> 
<div class="mini-fit" >
	<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id"  allowAlternating="true"
		onrowdblclick="onRowDblClick" allowResize="true" sortMode="client" allowAlternating="true" sizeList="[20,50,100]" pageSize="50">
		<div property="columns">
			<div type="indexcolumn" width="50px" headerAlign="center">序号</div>
            <div field="productCode" width="80" align="center" headerAlign="center"  >产品编号</div>
            <div field="productName" width="80" align="center" headerAlign="center"  >产品名称</div>
            <div field="checkAccess" width="120px" align="center"  headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'YesNo'}">校验准入</div>
            <div field="checkLimit" width="120px" align="center"  headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'YesNo'}">校验授信</div>
            <div field="checkRisk" width="120px" align="center"  headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'YesNo'}">校验单笔风险</div>
            <div field="operator" width="80" align="center" headerAlign="center"  >操作员</div>
            <div field="dateTime" width="80" align="center" headerAlign="center"  >保存时间</div>
		</div>
	</div>
</div>   
<script>
	mini.parse();
	var url = window.location.search;
	var prdNo = CommonUtil.getParam(url, "prdNo");
	var prdName = CommonUtil.getParam(url, "prdName");
	var form = new mini.Form("#search_detail_form");
	var grid = mini.get("datagrid");
	var row = "";
	grid.on("beforeload", function(e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex;
		var pageSize = e.data.pageSize;
		search(pageSize, pageIndex);
	});

	$(document).ready(function() {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
			search(grid.pageSize, 0);
		});
	});
	
	//增加
	function add() {
		mini.get("edit_btn").setEnabled(true);
		mini.get("save_btn").setEnabled(true);
		mini.get("delete_btn").setEnabled(true);
		mini.get("expt_btn").setEnabled(true);
		var detailForm = new mini.Form("#search_detail_form");
		var flag = detailForm.validate();
		if (detailForm.isValid() == false) {
			mini.alert("信息填写有误，请重新填写", "系统提示");
			return;
		}
		var detailData = detailForm.getData(true);
		var conData = grid.getData();
		if(conData.length>0){
			for(var i=0;i<conData.length;i++){
				productCode = conData[i].productCode
				if(productCode == detailData.productCode){
					mini.alert("已有该业务的风险信息，请重新填写", "系统提示");
					return;
				}
			}
		}
		addRow(detailData);
	}
	function addRow(detailData) {
		var proudctName = mini.get("productCode").getText();
		var arr = searchEduTermType();
		if (arr == null || arr.length == 0) {
			return;
		}
		var termTypeChinese;
		for (var i = 0; i < arr.length; i++) {
			if (detailData.termType == arr[i].id) {
				termTypeChinese = arr[i].text;
			}
		}
		var dateTime = getNowFormatDate();
		var row = {
			productCode : detailData.productCode,
			productName : proudctName,
			checkAccess : detailData.checkAccess,
			checkLimit : detailData.checkLimit,
			checkRisk : detailData.checkRisk,
			dateTime : dateTime,
			operator : '<%=__sessionUser.getUserId() %>'
		};
		grid.addRow(row);
		var detailForm = new mini.Form("#search_detail_form");
		detailForm.clear();
	}

	function searchEduTermType() {
		var termArr = CommonUtil.serverData.dictionary.EduTermType;
		return termArr;
	}

	//提交
	function commit() {
		var param = form.getData(true);
		var rows = grid.getData();
		if (rows.length == 0) {
			return;
		}
		var params = new Array();
		for (var int = 0; int < rows.length; int++) {
			var paramSub = {};
			paramSub["productCode"] = rows[int].productCode;
			paramSub["productName"] = rows[int].productName;
			paramSub["checkAccess"] = rows[int].checkAccess;
			paramSub["checkLimit"] =  rows[int].checkLimit;
			paramSub["checkRisk"] =   rows[int].checkRisk;
			paramSub["operator"] =   rows[int].operator;
			paramSub["dateTime"] =   rows[int].dateTime;
			params.push(paramSub);
		}
		param["OpicsRisk"] = params;
		param = mini.encode(param);
		CommonUtil.ajax({
			url : "/edCustController/riskManagerSave",
			data : param,
			callback : function(data) {
				//mini.alert(data.desc, "系统提示");
				mini.alert("保存成功！","系统提示");
			}
		});
	}

	//删除
	function del() {
		var row = grid.getSelected();
		if (!row) {
			mini.alert("请选中要移除的一行", "提示");
			return;
		}
		grid.removeRow(row);
	}

	//修改
	function edit() {
		var row = grid.getSelected();
		if (!row) {
			mini.alert("请选中一行进行修改!", "提示");
			return;
		}
		grid.removeRow(row);
		var detailForm = new mini.Form("#search_detail_form");
		detailForm.setData(row);
		mini.get("productCode").setValue(row.productCode);
		mini.get("productCode").setText(row.productName);
		mini.get("checkAccess").setValue(row.checkAccess);
		mini.get("checkLimit").setValue(row.checkLimit);
		mini.get("checkRisk").setValue(row.checkRisk);
		//mini.get("add_btn").setEnabled(false);
		mini.get("edit_btn").setEnabled(false);
		mini.get("save_btn").setEnabled(false);
		mini.get("delete_btn").setEnabled(false);
		mini.get("expt_btn").setEnabled(false);
	}

	//修改保存
	function save() {
		del();
		add();
		mini.get("add_btn").setEnabled(true);
		mini.get("edit_btn").setEnabled(true);
		mini.get("save_btn").setEnabled(false);
	}
	
	function onButtonsubInstId(e) {
	    var btnEdit = this;
	    mini.open({
	        url: CommonUtil.baseWebPath() + "../../quota/config/ProdExceptionMini.jsp",
	        title: "产品选择",
	        width: 800,
	        height: 600,
	        ondestroy: function (action) {
	            if (action == "ok") {
	                var iframe = this.getIFrameEl();
	                var data = iframe.contentWindow.GetData();
	                data = mini.clone(data);    //必须
	                if (data) {
	                	btnEdit.setValue(data.id);
	                    btnEdit.setText(data.moduleName);
	                    btnEdit.focus();
	                }
	            }

	        }
	    });
	}
	
	function search(pageSize, pageIndex){
		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		var params = mini.encode(data);		 
		CommonUtil.ajax({
			url:"/edCustController/searchRiskManager",
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	//获取当前时间，格式YYYY-MM-DD
    function getNowFormatDate() {
        var date = new Date();
        var seperator1 = "-";
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        var strDate = date.getDate();
        if (month >= 1 && month <= 9) {
            month = "0" + month;
        }
        if (strDate >= 0 && strDate <= 9) {
            strDate = "0" + strDate;
        }
        var currentdate = year + seperator1 + month + seperator1 + strDate;
        return currentdate;
    }
	
  //导出
	function exportAcup(){
		var content = grid.getData();
		if(content.length == 0){
			mini.alert("请先查询数据");
			return;
		}
		mini.confirm("您确认要导出Excel吗?","系统提示" ,
			function (action) {
				if (action == "ok"){
					var form = new mini.Form("#search_detail_form");
					var data = form.getData(true);
					var fields = null;
					for(var id in data){
						fields += '<input type="hidden" id="' + id + '" name="'+ id +'" value="' + data[id] + '">';
					}
					var urls = CommonUtil.pPath + "/sl/edCustController/exportExcel";                                                                                                                         
					$('<form action="'+ urls +'" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();   
				}
		});
	}
</script>
</body>
</html>