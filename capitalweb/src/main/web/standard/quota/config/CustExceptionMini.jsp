<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>客户信息</title>
</head>
<body style="width:100%;height:100%;background:white">
    <fieldset class="mini-fieldset title">
    <legend>信息查询</legend>
    <div id="search_form"> 
     <input id="party_name"  class="mini-textbox"   label="客户名称："  labelField="true" labelStyle="text-align:right;" width="330px" 
        />       
        <input id="party_shortname" class="mini-textbox"   label="客户代码：" labelField="true"   width="330px" labelStyle="text-align:right;"          	
         />   
        <span style="float:right;margin-right: 150px">
            <a class="mini-button" style="display: none"  id="search_btn"  onclick="query()">查询</a>
            <a class="mini-button" style="display: none"  id="clear_btn"  onclick="clear()">清空</a>
        </span>
    </div>
    </fieldset>     

   <div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id"  allowAlternating="true"
		 allowResize="true" sortMode="client" allowAlternating="true" onrowdblclick="onRowDblClick"  onshowrowdetail="onShowRowDetail" 
		 >
        <div property="columns">
		     <div type="expandcolumn" >#</div>		
			<div type="indexcolumn" width="50px" headerAlign="center">序号</div>
			<div field="party_id" width="200px" align="center"  headerAlign="center" >客户代码</div>     
			<div field="party_name" width="200px" align="center"  headerAlign="center">CN客户名称</div>     
			<div field="party_code" width="200px" align="center"  headerAlign="center"  data="CommonUtil.serverData.dictionary.CustType " >客户类型</div>     
			<div field="core_custno" width="200px" align="center"  headerAlign="center"  data="CommonUtil.serverData.dictionary.TYCUSTID ">同业客户标识</div>     
			<div field= "sf_cpty_country"width="200px" align="center"  headerAlign="center" data="CommonUtil.serverData.dictionary.CDDZ0012" >国别</div>     
			<div field="at_type" width="200px" align="center"  headerAlign="center"  data="CommonUtil.serverData.dictionary.AT0001" >准入级别</div>     
			<div field="at_date" width="200px" align="center"  headerAlign="center"  >准入级别有效期</div>     
			<div field="fcci_codeName" width="200px" align="center"  headerAlign="center" >所属一级法人</div>     
			
				
		</div>
	</div>
  
    
    <div id="editForm1" style="display:none;"> 
        <div  class="mini-tabs" style="width:100%;" activeIndex="0">
            <div title="基本信息">
                <input class="mini-hidden" name="id"/>
                <table style="width:100%;">
            <tr>
                <td style="width:80px;">客户代码：</td>
                <td style="width:150px;"><input  field="custmerCode"  name="custmerCode"  class="mini-textbox" /></td>
                <td style="width:80px;">联系人姓名：</td>
                <td style="width:150px;"><input field="name" name="name" class="mini-textbox" /></td>
                <td style="width:80px;">手机：</td>
                <td style="width:150px;"><input   field="cellphone"   name="cellphone" class="mini-textbox" /></td>
              </tr>
              <tr> 
                <td style="width:80px;">邮箱：</td>
                <td style="width:150px;"><input   field="email"   name="email" class="mini-textbox" /></td>
                <td style="width:80px;">地址：</td>
                <td style="width:150px;"><input   field="address"   name="address" class="mini-textbox" /></td>
                <td style="width:80px;">邮编：</td>
                <td style="width:150px;"><input   field="postcode"   name="postcode" class="mini-textbox" /></td>
            </tr>
            <tr>
                <td style="width:80px;">职位：</td>
                <td style="width:150px;"><input  field="position" name="position" class="mini-textbox" data="Genders"/></td>
                <td style="width:80px;">固定电话：</td>
                <td style="width:150px;"><input name="tel"  field="tel"class="mini-textbox" minValue="0" maxValue="200" value="25"  /></td> 
             </tr>
            
        </table>
            </div>
           
        </div>
    </div>
</div> 

<script>

mini.parse();


var url = window.location.search;
var prdNo = CommonUtil.getParam(url, "prdNo");
var prdName = CommonUtil.getParam(url, "prdName");
var form = new mini.Form("#search_form");
var grid= mini.get("datagrid");
var editForm = document.getElementById("editForm1");  
var row="";


//绑定表单
grid.load();
var db = new mini.DataBinding();
db.bindForm("editForm", grid);

//初始化
$(document).ready(function() {
	
	query();
});
grid.on("beforeload", function (e) {
	e.cancel = true;
	 var pageIndex = e.data.pageIndex; 
	 var pageSize = e.data.pageSize;
	 search(pageSize,pageIndex);
});

function onShowRowDetail(e) {
    var row = e.record;
    
    //将editForm元素，加入行详细单元格内
    var td = grid.getRowDetailCellEl(row);
    td.appendChild(editForm);
    editForm.style.display = "";

    //表单基本信息
   
    if (grid.isNewRow(row)) {                
        form.reset();
    } else {
        grid.loading();
    
        CommonUtil.ajax({
        	url : "/TdCustContactsController/searchTdCustContactsPage", 
        	 type: "post",
            success: function (text) {
             var form = new mini.Form("editForm1");
                var o = mini.decode(text);
                form.setData(o);
                grid.unmask();//不遮罩
            }
        });
    }
}



// 按钮查询
function query(){
		search(grid.pageSize,0);
	}



function clear(){
    var form=new mini.Form("search_form");
    form.clear();
    search(10,0);

}


/* 查询 */
	function search(pageSize,pageIndex){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统提示");
			return;
		}

		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		var params = mini.encode(data);		 
		CommonUtil.ajax({
		url:"/TdCustEcifController/searchTdCustEcifPaged",
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
function GetData() {
	var grid= mini.get("datagrid");
        var row = grid.getSelected();
        return row;
    }
    
    //双击行选择
    function onRowDblClick(){
        onOk();
    }

    function CloseWindow(action) {
        if (window.CloseOwnerWindow)
            return window.CloseOwnerWindow(action);
        else
            window.close();
    }

    function onOk() {
        CloseWindow("ok");
    }

    //关闭窗口
    function onCancel() {
        CloseWindow("cancel");
    }
 
</script>
 </body>

</html>