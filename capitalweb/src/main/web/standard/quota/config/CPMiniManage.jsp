<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>客户管理</title>
</head>
<body style="width:100%;height:100%;background:white">
    <fieldset class="mini-fieldset title">
    <legend>信息查询</legend>
    <div id="search_form">  
          
        <input id="party_shortname" name="party_shortname" class="mini-textbox"   label="Ecif编号：" labelField="true"   width="330px" labelStyle="text-align:right;" />
        <input id="party_name"  name="party_name" class="mini-textbox"   label="客户名称："  labelField="true" labelStyle="text-align:right;" width="330px" />
        <input id="cName"  name="cName" class="mini-textbox"   label="交易对手代号："  labelField="true" labelStyle="text-align:right;" width="330px" />
      
        <span style="float:right;margin-right: 150px">
            <a class="mini-button" style="display: none"  id="search_btn"  onclick="query()">查询</a>
            <a class="mini-button" style="display: none"  id="clear_btn"  onclick="clear()">清空</a>
        </span>
    </div>
    </fieldset>
   
    <div class="mini-fit"  title="客户列表">
	<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id"  allowAlternating="true"
		onrowdblclick="onRowDblClick" allowResize="true" sortMode="client" allowAlternating="true">
		<div property="columns">
			<div field="clino" width="120px" align="center"  headerAlign="center" >Ecif编号</div>     
			<div field="cliname" width="200px" align="center"  headerAlign="center" >CN客户名称</div>
			<div field="cno" width="120px" align="center"  headerAlign="center" >OPICS编号</div>     
            <div field="cname" width="100px" align="center"  headerAlign="center" >交易对手代号</div>
            <div field="sn" width="100px" align="center"  headerAlign="center" >交易对手简称</div>     
			<div field="cfn" width="100px" align="center"  headerAlign="center" >交易对手全称</div>     
		</div>
	</div>
</div>   



<script>

mini.parse();

var url = window.location.search;
var prdNo = CommonUtil.getParam(url, "prdNo");
var prdName = CommonUtil.getParam(url, "prdName");
var form = new mini.Form("#search_form");
var grid=mini.get("datagrid");
var row="";

grid.on("beforeload", function (e) {
	e.cancel = true;
	var pageIndex = e.data.pageIndex; 
	var pageSize = e.data.pageSize;
	search(pageSize,pageIndex);
});



$(document).ready(function() {
	query();
});

//查询按钮
function query(){
    search(grid.pageSize,0);
}

function search(pageSize, pageIndex) {
    var form = new mini.Form("#search_form");
    form.validate();
    if (form.isValid() == false) {
        mini.alert("表单填写错误,请确认!", "提示信息");
        return;
    }

    var data = form.getData();
    data['pageNumber'] = pageIndex + 1;
    data['pageSize'] = pageSize;
    data['branchId']=branchId;

    var params = mini.encode(data);

    CommonUtil.ajax({
    	url :"/TdCustEcifController/showAllIfsOpicsCust" ,
        data : params,
        callback : function(data) { 
            grid.setTotalCount(data.obj.total);
            grid.setPageIndex(pageIndex);
            grid.setPageSize(pageSize);
            grid.setData(data.obj.rows);
        }
    });
}

	//清空
	function clear(){
		var form=new mini.Form("search_form");
		form.clear();
		query();
	}

    function GetData() {
        var row = grid.getSelected();
        return row;
    }
    
    //双击行选择
    function onRowDblClick(){
        onOk();
    }

    function CloseWindow(action) {
        if (window.CloseOwnerWindow)
            return window.CloseOwnerWindow(action);
        else
            window.close();
    }

    function onOk() {
        CloseWindow("ok");
    }

    //关闭窗口
    function onCancel() {
        CloseWindow("cancel");
    }

 

 
</script>
 </body>

</html>