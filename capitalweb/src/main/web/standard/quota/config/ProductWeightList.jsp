<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>衍生品权重详情列表</title>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
	<legend>衍生品权重详情列表</legend>	
	<div>
		<div id="search_form" style="width:100%" cols="6">     
	       <!--  <input id="productCode" name="productCode"  class="mini-buttonedit"  labelField="true"  label="选择产品："  required="true"  width="330px"  onbuttonclick="onButtonsubInstId"/>
			<input id="productName" name="productName"  class="mini-hidden"  labelField="true"  label="产品名字："  required="true"  width="330px"/>
	        <span style="float:right;margin-right: 100px">
	        	<a class="mini-button" style="display: none"  id="search_btn"  onclick="query()">查询</a>
	            <a id="clear_btn" class="mini-button" style="display: none"  onclick="clear()">清空</a>
	            <a id="export_btn" class="mini-button" style="display: none"  onclick="exportData()">导出</a>
	        </span> -->
	    </div>
	</div>
</fieldset> 
    

<div class="mini-fit" >
	<div id="datagrid" class="mini-datagrid" style="width:100%;height:100%;" allowResize="true" sizeList="[20,50,100]" pageSize="50"
        allowCellEdit="true" allowCellSelect="true" multiSelect="true" editNextOnEnterKey="true"  editNextRowCell="true">
		<div property="columns">
			<div type="indexcolumn" width="50px" headerAlign="center">序号</div>
			<div field="productName" width="140px" align="center" allowSort="true" headerAlign="center" >产品名称</div> 
            <div field="weightOne" width="140px" align="center" allowSort="true" headerAlign="center" >1年以下(%)</div> 
            <div field="weightTwo" width="140px" align="center" allowSort="true" headerAlign="center" >1-5年(%)</div> 
            <div field="weightThree" width="140px" align="center" allowSort="true" headerAlign="center" >5年以上(%)</div> 
		</div>
	</div>
</div>   

<script>
	mini.parse();
	
	var url = window.location.search;
	var prdNo = CommonUtil.getParam(url, "prdNo");
	var prdName = CommonUtil.getParam(url, "prdName");
	var form = new mini.Form("#search_form");
	var grid=mini.get("datagrid");
	var row="";
	
	$(document).ready(function() {	
		query();
	});
	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});
	
	//按钮查询
	function query(){
		search(grid.pageSize,0);
	}
	
	/* 查询所有 */
	function search(pageSize,pageIndex){
		var data = form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:"/TdCustEcifController/searchAllForeignProtectName",
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	
	// 清空(查询所有)
	function clear(){
	    var form=new mini.Form("search_form");
	    form.clear();
	    search(50,0);
	}
	
	//查询单个产品
	function onButtonsubInstId(e) {
	    var btnEdit = this;
	    mini.open({
	        url: CommonUtil.baseWebPath() + "../../quota/config/ProdExceptionMini.jsp",
	        title: "产品选择",
	        width: 800,
	        height: 500,
	        ondestroy: function (action) {
	            if (action == "ok") {
	                var iframe = this.getIFrameEl();
	                var data = iframe.contentWindow.GetData();
	                data = mini.clone(data);    //必须
	                if (data) {
	                	btnEdit.setValue(data.prdNo);
	                    btnEdit.setText(data.prdName);
	                    /* mini.get("productCode").setValue(data.party_name);*/
	                    //mini.get("productName").setValue(data.moduleName); 
	                    mini.get("productName").setValue(data.prdName);
	                    search(50,0,data.prdNo);
	                    btnEdit.focus();
	                }
	            }
	        }
	    });
	}
	
	//导出
	function exportData() {
		mini.confirm("您确认要导出Excel吗?","系统提示", 
			function (action) {
				if (action == "ok"){
			 		var form = new mini.Form("#search_form");
					var data = form.getData(true);
					var fields = null;
					for(var id in data){
						fields += '<input type="hidden" id="' + id + '" name="'+ id +'" value="' + data[id] + '">';
					}
					var urls = CommonUtil.pPath + "/sl/TdCustEcifController/exportExcel";                                                                                                                         
					$('<form action="'+ urls +'" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();   
				}
			}
		);
	}
</script>
</body>

</html>