<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>额度冻结维护</title>
  </head>

<body style="width:100%;height:100%;background:white">
<div class="mini-splitter" style="width:100%;height:100%;">
	<div size="90%" showCollapseButton="false">
		<div id="field_form" class="mini-fit area"  style="background:white">
            <input id="institution" name="institution" class="mini-hidden" value="<%=__sessionInstitution.getInstId()%>"> 
            <input id="oldInstitution" name="oldInstitution" class="mini-hidden" value="<%=__sessionInstitution.getInstId()%>"> 
            <input id="state" name="state" class="mini-hidden" value="1">
            
            <fieldset>
                <legend>审批信息</legend>
                    <div class="leftarea">
                        <input id="dealNo" name="dealNo" class="mini-textbox" labelField="true"  label="审批单号：" emptyText="保存后自动生成" required="true"   enabled="false"    labelStyle="text-align:left;width:130px;" style="width:100%;"/>
                        <input id="sponInstName" name="sponInstName" class="mini-textbox" labelField="true"  label="审批发起机构："  enabled="false"  value='<%=__sessionInstitution.getInstName() %>' vtype="maxLength:10"   labelStyle="text-align:left;width:130px;" style="width:100%;"/>
                    </div>
                    <div class="rightarea">
                        <input id="ioper" name="ioper" class="mini-textbox" labelField="true"  label="审批发起人："   enabled="false" value='<%=__sessionUser.getUserId() %>'     labelStyle="text-align:left;width:130px;" style="width:100%;"/>
                        <input id="inputDate" name="inputDate" class="mini-datepicker" labelField="true"  label="审批日期："   enabled="false" value='<%=__bizDate %>'   labelStyle="text-align:left;width:130px;" style="width:100%;"/>
                    </div>
            </fieldset>   
            
            <fieldset>
            <legend>冻结要素</legend>
                <div class="leftarea">
                    <input id="creditId" name="creditId" class="mini-buttonedit"  onbuttonclick="onButtonEdit" labelField="true"  label="额度品种：" required="true"   style="width:100%;"  labelStyle="text-align:left;width:130px;"  />
                    <input id="custNo" name="custNo" class="mini-textbox"  labelField="true"  label="客户编号：" enabled="false"  style="width:100%;"  labelStyle="text-align:left;width:130px;"/>
                    <input id="term" name="term" class="mini-textbox" labelField="true"  label="额度期限：" enabled="false"   labelStyle="text-align:left;width:130px;" style="width:100%;"   />
                    <input id="quotaAvlAmt" name="quotaAvlAmt" class="mini-spinner" labelField="true"  label="可用额度(元)：" minValue="0" maxValue="9999999999999" format="n2" changeOnMousewheel="false" enabled="false"  vtype="maxLength:20" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
                    <input id="vdate" name="vdate" class="mini-datepicker" labelField="true"  label="冻结起始日："  format="yyyy-MM-dd" required="true"   labelStyle="text-align:left;width:130px;" style="width:100%;"   />
                	<input id="dueDate" name="dueDate" class="mini-datepicker" labelField="true"  label="额度起始日：" format="yyyy-MM-dd" enabled="false"    labelStyle="text-align:left;width:130px;" style="width:100%;"   />
                	<input id="frozenAmt" name="frozenAmt" class="mini-spinner" labelField="true"  label="冻结金额(元)：" minValue="-9999999999999" maxValue="9999999999999" format="n2" changeOnMousewheel="false" required="true"   labelStyle="text-align:left;width:130px;" style="width:100%;"   />
                </div>			
                <div class="rightarea">
                    <input id="creditName" name="creditName" class="mini-textbox"  labelField="true"  label="额度名称："   enabled="false" style="width:100%;"  labelStyle="text-align:left;width:130px;"  />
                    <input id="custName" name="custName"  class="mini-textbox" labelField="true"  label="客户名称："  enabled="false" style="width:100%;"  labelStyle="text-align:left;width:130px;" />
                    <input id="creditAmt" name="creditAmt" class="mini-spinner" labelField="true"  label="授信额度(元)：" minValue="0" maxValue="9999999999999" format="n2" changeOnMousewheel="false" enabled="false" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
                    <input id="quotaFrozenAmt" name="quotaFrozenAmt" class="mini-spinner" labelField="true"  label="已冻结额度(元)：" minValue="0" maxValue="9999999999999" format="n2" changeOnMousewheel="false" enabled="false"   labelStyle="text-align:left;width:130px;" style="width:100%;"   />
                    <input id="mdate" name="mdate" class="mini-datepicker" labelField="true"  label="冻结截止日：" format="yyyy-MM-dd" required="true"  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
               		<input id="dueDateLast" name="dueDateLast" class="mini-datepicker" labelField="true"  label="额度有效期：" format="yyyy-MM-dd" enabled="false"    labelStyle="text-align:left;width:130px;" style="width:100%;"   />
                </div>			
                
                <div class="centerarea">
                    <input id="reason" name="reason" class="mini-textarea" labelField="true"  label="调整说明：" required="false"   vtype="maxLength:50"  labelStyle="text-align:left;width:130px;" style="width:100%;"/>
                </div>
                <div class="leftarea">
                    <input id="custCreditId" name="custCreditId" class="mini-textbox" labelField="true"  label="唯一授信额度编号："  enabled="false"   style="width:100%;"  labelStyle="text-align:left;width:130px;" />
                </div>
            </fieldset>
            <%@ include file="../../Common/Flow/MiniApproveOpCommon.jsp"%>
		</div>
	</div>		
    <div id="functionIds"  style="padding-top:30px;text-align:center;">
        <div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="save_btn"   onclick="save">保存</a></div>
        <div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="close_btn"   onclick="close">关闭</a></div>
    </div> 
</div>
<script type="text/javascript">

    mini.parse();   
	//获取当前tab
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var row=params.selectData;
	var url=window.location.search;
	var action=CommonUtil.getParam(url,"action");
    var form=new mini.Form("#field_form");
    
    var tradeData={};
	
	tradeData.selectData=row;
	tradeData.operType=action;
	tradeData.serial_no=row.dealNo;
	tradeData.task_id=row.taskId;

   

    $(document).ready(function(){
		if($.inArray(action,["approve","detail"])>-1){//审批、详情
			mini.get("save_btn").setVisible(false);
            form.setEnabled(false);
            var form11=new mini.Form("#approve_operate_form");
			form11.setEnabled(true);
		}

		if($.inArray(action,["edit","approve","detail"])>-1){//修改、审批、详情
            form.setData(row);
            //额度中类编号
			mini.get("creditId").setValue(row.creditId);
			mini.get("creditId").setText(row.creditId);
			
		}else{//增加 
			
		}
	});

    function close(){
		top["win"].closeMenuTab();
	}

    function save(){
        form.validate();
		if (form.isValid() == false) {
            mini.alert("信息填写有误，请重新填写","系统提示");
			return;
        }
        /*************** 验证开始*****************/
        var vdate = mini.get("vdate").getValue().toLocaleDateString();
        var mdate = mini.get("mdate").getValue().toLocaleDateString();
        var dueDate = mini.get("dueDate").getValue().toLocaleDateString();
        var dueDateLast = mini.get("dueDateLast").getValue().toLocaleDateString();
        if(CommonUtil.dateCompare(vdate,mdate) || vdate==mdate){
            mini.alert("冻结截止日期必须大于冻结起始日期!","系统提示");
            return false;
		}
        if(CommonUtil.dateCompare(dueDate,vdate)){ 
            mini.alert("冻结起始日期必须大于额度起始期日!","系统提示");
            return false;
        }
        if(CommonUtil.dateCompare(mdate,dueDateLast)){
            mini.alert("冻结截止日期必须小于额度有效期日!","系统提示");
            return false;
        }
        
        var frozenAmt = mini.get("frozenAmt").getValue();
        frozenAmt = parseFloat(frozenAmt);
        var quotaAvlAmt = mini.get("quotaAvlAmt").getValue();
        quotaAvlAmt = parseFloat(quotaAvlAmt);
        if(frozenAmt == 0.00){
            mini.alert('冻结金额不能为0','提示信息');
            return false;
        }else if(frozenAmt>0){
            if(frozenAmt>quotaAvlAmt){
                mini.alert('冻结金额不能大于可用金额','提示信息');
                return false;
            }

        }else{
            mini.alert('冻结金额不能小于0','提示信息');
            return false;
        }
        
       
        /*************** 验证结束*****************/

        var saveUrl="";
        if(action == "add"){
            saveUrl="/CustCreditFrozenController/addCustCreditFrozen";
        }else if(action == "edit"){
            saveUrl="/CustCreditFrozenController/updateCreditFrozen";
        }

        var data=form.getData(true);
        data['state']='1';
        var params=mini.encode(data);
        CommonUtil.ajax({
            url:saveUrl,
            data:params,
            callback:function(data){
                mini.alert('保存成功','提示信息',function(){
                    top["win"].closeMenuTab();
                });
            }
		});

    }

    function onButtonEdit(){
		var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../quota/operate/mini/CustCreditMini.jsp",
            title: "选择额度品种",
            width: 700,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.productType);
                        btnEdit.setText(data.productType);

                        mini.get("custNo").setValue(data.ecifNum);//客户编号
                        mini.get("custName").setValue(data.cnName);//客户名称
                        mini.get("creditName").setValue(data.creditName);//额度名称
                        mini.get("creditAmt").setValue(data.loanAmt);
                        mini.get("quotaAvlAmt").setValue(data.avlAmt);
                        mini.get("quotaFrozenAmt").setValue(data.frozenAmt);
                        mini.get("custCreditId").setValue(data.custCreditId);
                        mini.get("dueDate").setValue(data.dueDate);
                        mini.get("dueDateLast").setValue(data.dueDateLast);
                        mini.get("term").setValue(data.termName);

                        btnEdit.focus();
                    }
                }

            }
        });
	}





</script>
<script type="text/javascript" src="../../Common/Flow/MiniApproveOpCommon.js"></script>	
</body>
</html>