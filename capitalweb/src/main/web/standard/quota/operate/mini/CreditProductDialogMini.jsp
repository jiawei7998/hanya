<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../../global.jsp"%>
<html>
<head>
    <title>查询产品 小页面(用于额度占用)</title>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
	<legend>产品查询</legend>	
	<div>
		<div id="search_form" style="width:100%" cols="6">
			<input id="productCode" name="productCode" class="mini-textbox"   label="产品代码：" labelField="true"    width="240px" labelStyle="text-align:right;"  emptyText="请输入产品代码" />
			<input id="productName" name="productName" class="mini-textbox" label="产品名称："  labelField="true"  width="240px" labelStyle="text-align:right;" emptyText="请输入产品名称" />
            
            <span style="float: right; margin-right: 100px">
				<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
			</span>
		</div>
	</div>
</fieldset> 

<div class="mini-fit" >
	<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id"  allowAlternating="true"
		onrowdblclick="onRowDblClick" allowResize="true" sortMode="client" allowAlternating="true">
		<div property="columns">
            <div type="indexcolumn" width="50px" headerAlign="center">序号</div>
            
			<div field="productCode" width="100px" align="center"  headerAlign="center" >产品代码</div>    
            <div field="productName" width="180" align="center" headerAlign="center"  >产品名称</div>
			
		</div>
	</div>
</div>   

<script>
    mini.parse();

    var url = window.parent.tabs.getActiveTab().url;
	var prdNo = CommonUtil.getParam(url, "prdNo");
	var prdName = CommonUtil.getParam(url, "prdName");
	var isoccupy = CommonUtil.getParam(url, "isoccupy");
	var form = new mini.Form("#search_form");
	var grid=mini.get("datagrid");
	var row="";

	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});

	$(document).ready(function() {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
			search(10, 0);
		});
    });
	
	//清空
	function clear(){
		var form=new mini.Form("search_form");
		form.clear();
		query();
    }

	function query(){
		search(10, 0); 
	}
    
    /* 查询 */
	function search(pageSize,pageIndex){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统也提示");
			return;
		}

		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
        data['pageSize']=pageSize;
        data['branchId']=branchId;
        data['isoccupy']=isoccupy;
		var params = mini.encode(data);

		CommonUtil.ajax({
			url:"/CreditManagerController/creditProductList",
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
    }
    
    function GetData() {
        
        var row = grid.getSelected();
        return row;
    }
    
    //双击行选择
    function onRowDblClick(){
        onOk();
    }

    function CloseWindow(action) {
        if (window.CloseOwnerWindow)
            return window.CloseOwnerWindow(action);
        else
            window.close();
    }

    function onOk() {
        CloseWindow("ok");
    }

    //关闭窗口
    function onCancel() {
        CloseWindow("cancel");
    }



</script>
</body>
</html>