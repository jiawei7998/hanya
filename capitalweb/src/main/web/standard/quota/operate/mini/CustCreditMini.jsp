<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../../global.jsp"%>
<html>
<head>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
    <title>额度品种 小页面(用于额度调整,额度冻结)</title>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
	<legend>额度品种查询</legend>	
	<div>
		<div id="search_form" style="width:100%" cols="6">
			<input id="custName" name="custName" class="mini-textbox"   label="客户名称：" labelField="true"    width="280px" labelStyle="text-align:right;"  emptyText="请输入客户名称" />
			<input id="creditName" name="creditName" class="mini-textbox" label="额度名称："  labelField="true"  width="280px" labelStyle="text-align:right;" emptyText="请输入额度名称" />
            
            <span style="float: right; margin-right: 150px"> 
				<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
			</span>
		</div>
	</div>
</fieldset> 

<div class="mini-fit" >
	<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id"  allowAlternating="true"
		onrowdblclick="onRowDblClick" allowResize="true" sortMode="client" allowAlternating="true">
		<div property="columns">
            <div type="indexcolumn" width="50px" headerAlign="center">序号</div>
            <div field="productType" width="120px" align="center"  headerAlign="center" >额度品种</div> 
			<div field="creditName" width="140px" align="center"  headerAlign="center" >额度名称</div>    
			
			<div field="ecifNum" width="100px" align="center"  headerAlign="center" >客户编号</div>
            <div field="cnName" width="180px" align="left"  headerAlign="center" >客户名称</div>
			<div field="loanAmt" width="120" align="right" headerAlign="center"  allowSort="true" numberFormat="n2">授信额度</div>
			<div field="avlAmt" width="120px" align="right"  headerAlign="center" allowSort="true" numberFormat="n2">可用额度</div>                            
			<div field="adjAmt" width="120px" align="right"  headerAlign="center" allowSort="true" numberFormat="n2">额度调整</div>                            
			<div field="frozenAmt" width="120px" align="right"  headerAlign="center" allowSort="true" numberFormat="n2">冻结金额</div>
			<div field="termName" width="100" align="center" headerAlign="center"  >期限</div>
			<div field="dueDateLast" width="120px" align="center"  headerAlign="center" allowSort="true" >额度到期日</div>
			                               
			
			<div field="custCreditId" width="150px" align="center"  headerAlign="center" >客户额度编号</div>
			
			
		</div>
	</div>
</div>   

<script>
    mini.parse();
	var url = window.location.search;
	var prdNo = CommonUtil.getParam(url, "prdNo");
	var prdName = CommonUtil.getParam(url, "prdName");
	var form = new mini.Form("#search_form");
	var grid=mini.get("datagrid");
	var row="";

	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});

	$(document).ready(function() {
		search(10, 0);
    });
	
	//清空
	function clear(){
		var form=new mini.Form("search_form");
		form.clear();
		query();
    }

	function query(){
		search(10, 0); 
	}
    
    /* 查询 */
	function search(pageSize,pageIndex){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统也提示");
			return;
		}

		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
        data['pageSize']=pageSize;
        data['branchId']=branchId;
		var params = mini.encode(data);

		CommonUtil.ajax({
			url:"/edCustController/getEdCustForHandlePage",
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
    }
    
    function GetData() {
        
        var row = grid.getSelected();
        return row;
    }
    
    //双击行选择
    function onRowDblClick(){
        onOk();
    }

    function CloseWindow(action) {
        if (window.CloseOwnerWindow)
            return window.CloseOwnerWindow(action);
        else
            window.close();
    }

    function onOk() {
        CloseWindow("ok");
    }

    //关闭窗口
    function onCancel() {
        CloseWindow("cancel");
    }



</script>
</body>
</html>