<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../../global.jsp"%>
<html>
<head>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>额度中类 mini 用于额度切分</title>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
	<legend>额度中类查询</legend>	
	<div>
		<div id="search_form" style="width:100%" cols="6">
			<input id="creditId"  class="mini-textbox"   label="额度代码："  labelField="true" labelStyle="text-align:right;" width="330px" />
        	<input id="creditName" class="mini-textbox"   label="额度名称：" labelField="true"   width="330px" labelStyle="text-align:right;" />
			<input id="ecifName" class="mini-textbox"   label="ecif客户号：" labelField="true"   width="330px" labelStyle="text-align:right;" />
			
			<span style="float: right; margin-right: 150px"> 
				<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
			</span>
		</div>
	</div>
</fieldset> 

<div class="mini-fit" >
	<div id="cust_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id"  allowAlternating="true"
		onrowdblclick="onRowDblClick" allowResize="true" sortMode="client" allowAlternating="true">
		<div property="columns">
			<div type="indexcolumn" width="50px" headerAlign="center">序号</div>

			<div field="creditId" width="180px" align="center"  headerAlign="center" >授信额度代码</div>    
			<div field="creditName" width="150" align="center" headerAlign="center"  >授信额度名称</div>
			<div field="opnLmt" width="150" align="center" headerAlign="center" numberFormat="n2" >基础授信额度(元)</div>
			<div field="opnLmt" width="150" align="center" headerAlign="center" numberFormat="n2" >可用基础授信额度(元)</div>
			<div field="crdtLmt" width="150" align="center" headerAlign="center" numberFormat="n2" >授信额度(元)</div>
			<div field="crdtLmt" width="150" align="center" headerAlign="center" numberFormat="n2" >可用授信额度(元)</div>
<!-- 			<div field="riskLevel" width="120" align="center" headerAlign="center"  renderer="CommonUtil.dictRenderer" data-options="{dict:'riskLevel'}">风险级别-1最高</div>
 -->		</div>
	</div>
</div>   

<script>
	mini.parse();
	
	var form = new mini.Form("#search_form");
	var grid=mini.get("cust_grid");
	var url = window.location.search;
	var action = CommonUtil.getParam(url, "action");
	mini.get("ecifName").setValue(action);
	mini.get("ecifName").setVisible(false);

	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});

	
	
	//清空
	function clear(){
		var form=new mini.Form("search_form");
		form.clear();
		query();
	}

	/* 按钮 查询事件 */
	function query(){
		search(grid.pageSize,0);
	}

	/* 查询 */
	function search(pageSize,pageIndex){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统也提示");
			return;
		}

		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['custNo']=mini.get("ecifName").getValue();
		var params = mini.encode(data);

		CommonUtil.ajax({
			url:"/edRiskController/getTcEdLimitPage",
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.length);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj);
			}
		});
	}
	
	$(document).ready(function() {
		search(10, 0);
    });
    

    function GetData() {
        
        var row = grid.getSelected();
        return row;
    }
    
    //双击行选择
    function onRowDblClick(){
        onOk();
    }

    function CloseWindow(action) {
        if (window.CloseOwnerWindow)
            return window.CloseOwnerWindow(action);
        else
            window.close();
    }

    function onOk() {
        CloseWindow("ok");
    }

    //关闭窗口
    function onCancel() {
        CloseWindow("cancel");
    }
</script>
</body>
</html>
