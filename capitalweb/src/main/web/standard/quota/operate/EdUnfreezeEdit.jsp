<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>额度解冻维护</title>
  </head>

<body style="width:100%;height:100%;background:white">
<div class="mini-splitter" style="width:100%;height:100%;">
	<div size="90%" showCollapseButton="false">
		<div id="field_form" class="mini-fit area"  style="background:white">
            <input id="oldInstitution" name="oldInstitution" class="mini-hidden"/>
            <input id="institution" name="institution" class="mini-hidden" value='<%=__sessionUser.getInstId() %>'/>
            
            <fieldset>
                <legend>原冻结交易要素</legend>
                    <div class="leftarea">
                        <input id="oldDealNo" name="oldDealNo" class="mini-buttonedit"  onbuttonclick="onButtonEdit"  labelField="true"  label="冻结审批单号：" required="true"   style="width:100%;"  labelStyle="text-align:left;width:130px;"  />
                        <input id="creditId" name="creditId" class="mini-textbox"  labelField="true"  label="额度品种：" enabled="false"  style="width:100%;"  labelStyle="text-align:left;width:130px;"  />
                        <input id="custNo" name="custNo" class="mini-textbox"  labelField="true"  label="客户编号：" enabled="false"  style="width:100%;"  labelStyle="text-align:left;width:130px;"/>
                        <input id="term" name="term" class="mini-textbox" labelField="true"  label="额度期限：" enabled="false"   labelStyle="text-align:left;width:130px;" style="width:100%;"   />
                        <input id="quotaAvlAmt" name="quotaAvlAmt" class="mini-spinner" labelField="true"  label="可用额度(元)：" minValue="0" maxValue="9999999999999" format="n2" changeOnMousewheel="false" enabled="false"  vtype="maxLength:20" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
                        <input id="vdate" name="vdate" class="mini-datepicker" labelField="true"  label="冻结起始日："  enabled="false"   labelStyle="text-align:left;width:130px;" style="width:100%;"   />
                        <input id="dueDate" name="dueDate" class="mini-datepicker" labelField="true"  label="额度有效期："  enabled="false"  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
                    </div>			
                    <div class="rightarea">
                        <input id="custCreditId" name="custCreditId" class="mini-textbox" labelField="true"  label="唯一授信额度编号："  enabled="false" style="width:100%;"  labelStyle="text-align:left;width:130px;" />
                        <input id="creditName" name="creditName" class="mini-textbox"  labelField="true"  label="额度名称："   enabled="false" style="width:100%;"  labelStyle="text-align:left;width:130px;"  />
                        <input id="custName" name="custName"  class="mini-textbox" labelField="true"  label="客户名称：" enabled="false" style="width:100%;"  labelStyle="text-align:left;width:130px;" />
                        <input id="creditAmt" name="creditAmt" class="mini-spinner" labelField="true"  label="授信额度(元)：" minValue="0" maxValue="9999999999999" format="n2" changeOnMousewheel="false" enabled="false"  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
                        <input id="quotaFrozenAmt" name="quotaFrozenAmt" class="mini-spinner" labelField="true"  label="已冻结额度(元)："minValue="0" maxValue="9999999999999" format="n2" changeOnMousewheel="false" enabled="false"    labelStyle="text-align:left;width:130px;" style="width:100%;"   />
                        <input id="mdate" name="mdate" class="mini-datepicker" labelField="true"  label="冻结截止日："   enabled="false"  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
                        <input id="frozenAmt" name="frozenAmt" class="mini-spinner" labelField="true"  label="冻结金额(元)：" minValue="0" maxValue="9999999999999" format="n2" changeOnMousewheel="false" enabled="false"   labelStyle="text-align:left;width:130px;" style="width:100%;"   />
                    </div>			
                    <div class="centerarea">
                        <input id="reason" name="reason" class="mini-textarea" labelField="true"  label="冻结备注："enabled="false"    labelStyle="text-align:left;width:130px;" style="width:100%;"/>
                    </div>
                    <div class="leftarea">
                        <input id="oldIoper" name="oldIoper" class="mini-textbox"  labelField="true"  label="经办人：" enabled="false"  style="width:100%;"  labelStyle="text-align:left;width:130px;"  />
                    </div>
                    <div class="rightarea">
                        <input id="odlSponInstName" name="odlSponInstName" class="mini-textbox"  labelField="true"  label="经营单位：" enabled="false" style="width:100%;"  labelStyle="text-align:left;width:130px;"  />
                    </div>
            </fieldset> 
            <!-- <fieldset>
                <legend>解冻要素</legend>
                    <div class="centerarea">
                        <input id="dealNo" name="dealNo" class="mini-textbox" labelField="true"  label="审批单号：" required="true"   enabled="false" vtype="maxLength:10"   labelStyle="text-align:left;width:130px;" style="width:50%;"/><span class='tips'>&nbsp;&nbsp;保存后自动生成
                        <input id="ioper" name="ioper" class="mini-textbox" value="<%=__sessionUser.getUserId() %>" text="<%=__sessionUser.getUserName() %>" labelField="true"  label="审批发起人："   enabled="false" vtype="maxLength:10"   labelStyle="text-align:left;width:130px;" style="width:50%;"/><span class='tips'>&nbsp;&nbsp;当前登录的客户经理
                        <input id="sponInstName" name="sponInstName" class="mini-textbox" value="<%=__sessionInstitution.getInstName() %>" text="<%=__sessionInstitution.getInstName() %>" labelField="true"  label="审批发起机构："  enabled="false"  vtype="maxLength:10"   labelStyle="text-align:left;width:130px;" style="width:50%;"/><span class='tips'>&nbsp;&nbsp;客户经理所属的机构
                        <input id="aDate" name="aDate" class="mini-datepicker" value='<%=__bizDate %>' labelField="true"  label="审批日期："   enabled="false" vtype="maxLength:10"   labelStyle="text-align:left;width:130px;" style="width:50%;"/><span class='tips'>&nbsp;&nbsp;发起审批单据具体时间
                        <input id="remark" name="remark" class="mini-textarea" labelField="true"  label="解冻备注："  vtype="maxLength:33" enabled="true" vtype="maxLength:10"   labelStyle="text-align:left;width:130px;" style="width:100%;"/>
                    </div>
            </fieldset> -->
            <fieldset>
                <legend>解冻要素</legend>
                    <div class="leftarea">
                        <input id="dealNo" name="dealNo" class="mini-textbox" labelField="true"  label="审批单号：" emptyText="保存后自动生成" required="true"   enabled="false"    labelStyle="text-align:left;width:130px;" style="width:100%;"/>
                        <input id="sponInstName" name="sponInstName" class="mini-textbox" labelField="true"  label="审批发起机构："  enabled="false"  value='<%=__sessionInstitution.getInstName() %>' vtype="maxLength:10"   labelStyle="text-align:left;width:130px;" style="width:100%;"/>
                    </div>
                    <div class="rightarea">
                        <input id="ioper" name="ioper" class="mini-textbox" labelField="true"  label="审批发起人："   enabled="false" value='<%=__sessionUser.getUserId() %>'     labelStyle="text-align:left;width:130px;" style="width:100%;"/>
                        <input id="inputDate" name="inputDate" class="mini-datepicker" labelField="true"  label="审批日期："   enabled="false" value='<%=__bizDate %>'   labelStyle="text-align:left;width:130px;" style="width:100%;"/>
                    </div>
                    <div class="centerarea">
                        <input id="remark" name="remark" class="mini-textarea" labelField="true"  label="解冻备注："  vtype="maxLength:33" enabled="true" vtype="maxLength:10"   labelStyle="text-align:left;width:130px;" style="width:100%;"/>
                    </div>
            </fieldset>    
        
            <%@ include file="../../Common/Flow/MiniApproveOpCommon.jsp"%>
		</div>
	</div>		
    <div id="functionIds"  style="padding-top:30px;text-align:center;">
        <div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="save_btn"   onclick="save">保存</a></div>
        <div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="close_btn"   onclick="close">关闭</a></div>
    </div> 
</div>
<script type="text/javascript">
    mini.parse();   
	//获取当前tab
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var row=params.selectData;
	var url=window.location.search;
	var action=CommonUtil.getParam(url,"action");
    var form=new mini.Form("#field_form");

    var tradeData={};
	
	tradeData.selectData=row;
	tradeData.operType=action;
	tradeData.serial_no=row.dealNo;
	tradeData.task_id=row.taskId;
    
    $(document).ready(function(){
		if($.inArray(action,["approve","detail"])>-1){//审批、详情
			mini.get("save_btn").setVisible(false);
            form.setEnabled(false);
            var form11=new mini.Form("#approve_operate_form");
			form11.setEnabled(true);
		}

		if($.inArray(action,["edit","approve","detail"])>-1){//修改、审批、详情
            form.setData(row);
            //冻结审批单号
			mini.get("oldDealNo").setValue(row.oldDealNo);
			mini.get("oldDealNo").setText(row.oldDealNo);
			
		}else{//增加 
			
		}
    });
    
    function close(){
		top["win"].closeMenuTab();
	}

    function save(){
        form.validate();
		if (form.isValid() == false) {
            mini.alert("信息填写有误，请重新填写","系统提示");
			return;
		}

        var saveUrl="";
        if(action == "add"){
            saveUrl="/CustCreditFrozenController/addCustCreditFrozen";
        }else if(action == "edit"){
            saveUrl="/CustCreditFrozenController/updateCreditFrozen";
        }

        var data=form.getData(true);
        data['state']='2';
        var params=mini.encode(data);

        CommonUtil.ajax({
            url:saveUrl,
            data:params,
            callback:function(data){
                mini.alert('保存成功','提示信息',function(){
                    top["win"].closeMenuTab();
                });
            }
		});

    }

    function onButtonEdit(e) {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../quota/operate/mini/DealCreditFrozenMini.jsp",
            title: "冻结交易选择",
            width: 700,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.dealNo);
                        btnEdit.setText(data.dealNo);
                        mini.get("custNo").setValue(data.custNo);
                        mini.get("custName").setValue(data.custName);
                        mini.get("creditId").setValue(data.creditId);
                        mini.get("creditName").setValue(data.creditName);
                        mini.get("creditAmt").setValue(data.creditAmt);
                        mini.get("quotaAvlAmt").setValue(data.quotaAvlAmt);
                        mini.get("quotaFrozenAmt").setValue(data.quotaFrozenAmt);
                        mini.get("custCreditId").setValue(data.custCreditId);
                        mini.get("dueDate").setValue(data.dueDate);
                        mini.get("term").setValue(data.term);
                        mini.get("vdate").setValue(data.vdate);
                        mini.get("mdate").setValue(data.mdate);
                        mini.get("frozenAmt").setValue(data.frozenAmt);

                        mini.get("oldIoper").setValue(data.ioper);
                        mini.get("odlSponInstName").setValue(data.sponInstName);
                        mini.get("reason").setValue(data.reason);
                        mini.get("oldInstitution").setValue(data.institution);



                        btnEdit.focus();
                    }
                }

            }
        });

	}




</script>
<script type="text/javascript" src="../../Common/Flow/MiniApproveOpCommon.js"></script>	
</body>
</html>