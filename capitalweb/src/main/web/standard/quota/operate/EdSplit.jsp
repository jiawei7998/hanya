<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/hideBtn.js"></script>
<title>额度切分</title>
</head>
<body style="width:100%;height:100%;background:white">

<div>
    <fieldset class="mini-fieldset">
    <legend>客户额度切分总和</legend> 	
		<div id="search_form" style="width:100%" cols="6">
            <div class="leftarea">
                <input id="ecifNum" name="ecifNum" class="mini-buttonedit"  onbuttonclick="onCPSearch"  labelField="true"  label="客户编号："  required="true"    style="width:100%;"  labelStyle="text-align:left;width:130px;"  />
                <input id="loanAmtSum" name="loanAmtSum" class="mini-spinner"    labelField="true"  label="基础授信额度："   minValue="0" maxValue="9999999999999" format="n2"changeOnMousewheel="false" enabled="false" style="width:100%;"  labelStyle="text-align:left;width:130px;"  />
            	<input id="loanAmtSumAll" name="loanAmtSumAll" class="mini-spinner"    labelField="true"  label="授信额度："   minValue="0" maxValue="9999999999999" format="n2"changeOnMousewheel="false" enabled="false" style="width:100%;"  labelStyle="text-align:left;width:130px;"  />
            </div>			
            <div class="rightarea">
                <input id="customer" name="customer" class="mini-textbox"   labelField="true"  label="客户名称："   enabled="false"  style="width:100%;"  labelStyle="text-align:left;width:130px;"  />
                <input id="avlAmtSum" name="avlAmtSum" class="mini-spinner" labelField="true"  label="可用基础额度："   minValue="0" maxValue="9999999999999" format="n2"  changeOnMousewheel="false" enabled="false"  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
           		<input id="avlAmtSumAll" name="avlAmtSumAll" class="mini-spinner"    labelField="true"  label="授信可用额度："   minValue="0" maxValue="9999999999999" format="n2"changeOnMousewheel="false" enabled="false" style="width:100%;"  labelStyle="text-align:left;width:130px;"  />
            </div>	
			
        </div>
    </fieldset>
    <fieldset class="mini-fieldset">
    <legend>额度切分详细</legend> 
        <div id="search_detail_form" style="width:100%" cols="6">
                <div class="leftarea">
                        <input id="productType" name="productType" class="mini-buttonedit" onbuttonclick="onPrdSearch"  labelField="true"  label="品种编号：" required="true"    style="width:100%;"  labelStyle="text-align:left;width:130px;"/>
                        <input id="loanAmt" name="loanAmt" class="mini-buttonedit" onbuttonclick="onLimitQuery" labelField="true"  label="基础授信额度(元)："  minValue="0" maxValue="9999999999999" changeOnMousewheel="false" required="true"  labelStyle="text-align:left;width:130px;" style="width:100%;" format="n2" numberFormat="#,0.00" />
                        <input id="loanAmtAll" name="loanAmtAll" class="mini-spinner" labelField="true"  label="授信额度(元)："   minValue="0" maxValue="9999999999999" format="n2" changeOnMousewheel="false"   enabled="true"  labelStyle="text-align:left;width:130px;" style="width:100%;" numberFormat="#,0.00"  />
                        <input style="width:100%;"id="dueDate" name="dueDate" class="mini-datepicker mini-mustFill" labelField="true"  label="起始日期：" requiredErrorText="该输入项为必输项" required="true"  ondrawdate="onDrawDateNear" onvaluechanged="associaDateCalculation" labelStyle="text-align:left;width:130px"    />
                        <input id="term" name="term" class="mini-spinner" labelField="true"  label="期限：" maxValue="9999999999" required="true"    labelStyle="text-align:left;width:130px;" style="width:100%;"  enabled="false" />
                		<input id="termType" name="termType" class="mini-combobox" labelField="true"  label="期限类型："  value="05" required="true"   labelStyle="text-align:left;width:130px;" style="width:100%;"   data = "CommonUtil.serverData.dictionary.EduTermType" enabled="false"/>
                		<input id="currency" name="currency" class="mini-combobox" labelField="true"  label="币种："  value="CNY" required="true"     labelStyle="text-align:left;width:130px;" style="width:100%;"  data = "CommonUtil.serverData.dictionary.Currency" />
                </div>
                <div class="rightarea">
                        <input id="creditName" name="creditName"  class="mini-textbox" labelField="true"  label="额度品种："  enabled="false"  style="width:100%;"  labelStyle="text-align:left;width:130px;" />
                        <input id="avlAmt" name="avlAmt" class="mini-spinner" labelField="true"  label="可用基础授信额度(元)："   minValue="0" maxValue="9999999999999" format="n2" changeOnMousewheel="false"   enabled="false"  labelStyle="text-align:left;width:130px;" style="width:100%;" numberFormat="#,0.00"  />
                        <input id="avlAmtAll" name="avlAmtAll" class="mini-spinner" labelField="true"  label="可用授信额度(元)："   minValue="0" maxValue="9999999999999" format="n2" changeOnMousewheel="false"   enabled="false"  labelStyle="text-align:left;width:130px;" style="width:100%;" numberFormat="#,0.00"  />
                        <input style="width:100%;"id="dueDateLast" name="dueDateLast" class="mini-datepicker mini-mustFill" labelField="true"  label="到期日期："  labelStyle="text-align:left;width:130px" requiredErrorText="该输入项为必输项" ondrawdate="onDrawDateFar" onvaluechanged="associaDateCalculation" required="true"    />
                        
                </div>
        </div>
    </fieldset>
</div>
<!-- </fieldset>  -->

<span style="margin: 2px; display: block;"> 
	<a  id="add_btn" class="mini-button" style="display: none"   onclick="add()">新增</a>
    <a  id="edit_btn" class="mini-button" style="display: none"   onclick="edit()">修改</a>
    <a  id="save_btn" class="mini-button" style="display: none"    onclick="save()" enabled="false" >保存</a>
	<a  id="delete_btn" class="mini-button" style="display: none"   onclick="del()">删除</a>
	<a  id="commit_btn" class="mini-button" style="display: none"    onclick="commit()">提交</a>
	<a  id="synch_btn" class="mini-button" style="display: none"    onclick="synch()">同步数据</a>
 </span> 
<div class="mini-fit" >
	<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id"  allowAlternating="true"
		onrowdblclick="onRowDblClick" allowResize="true" sortMode="client" allowAlternating="true" sizeList="[20,50,100]" pageSize="50">
		<div property="columns">
			<div type="indexcolumn" width="50px" headerAlign="center">序号</div>
			 
            <div field="productType" width="180" align="" headerAlign="center"  >授信品种编号</div>
            <div field="creditName" width="140px" align=""  headerAlign="center" >授信额度品种</div> 
            <div field="loanAmt" width="120" align="right" headerAlign="center"  allowSort="true" numberFormat="n2">基础授信额度(元)</div>
            <div field="avlAmt" width="130" align="right" headerAlign="center"  allowSort="true" numberFormat="n2">可用基础授信额度(元)</div>
            <div field="loanAmtAll" width="120" align="right" headerAlign="center"  allowSort="true" numberFormat="n2">授信额度(元)</div>
            <div field="avlAmtAll" width="120" align="right" headerAlign="center"  allowSort="true" numberFormat="n2">可用授信额度(元)</div>
            <div field="dueDate" width="150px" align="center"  headerAlign="center" >起始日期</div>
            <div field="dueDateLast" width="150px" align="center"  headerAlign="center" >到期日期</div>
            <!-- <div field="term" width="120px" align="center"  headerAlign="center" allowSort="true">期限</div>                            
            <div field="termType" width="120px" align="center"  headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'EduTermType'}">期限类型</div> -->                            
            <div field="currency" width="100px" align="center"  headerAlign="center"  renderer="CommonUtil.dictRenderer" data-options="{dict:'Currency'}">币种</div>
            <!-- <div field="termName" width="120px" align="center"  headerAlign="center" >额度期限</div> -->
		</div>
	</div>
</div>   

<script>
	mini.parse();
	var url = window.location.search;
	var prdNo = CommonUtil.getParam(url, "prdNo");
	var prdName = CommonUtil.getParam(url, "prdName");
	var form = new mini.Form("#search_form");
	var grid = mini.get("datagrid");
	mini.get("term").setVisible(false);
	mini.get("termType").setVisible(false);
	var row = "";
	grid.on("beforeload", function(e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex;
		var pageSize = e.data.pageSize;
		search(pageSize, pageIndex);
	});

	$(document).ready(function() {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
            //searchEduTermType();
        });
	});

	//客户编号的查询
	function onCPSearch(e) {
		var btnEdit = this;
		mini.open({
			url : CommonUtil.baseWebPath()
					+ "../../quota/config/CPMiniManage.jsp",
			title : "选择客户列表",
			width : 900,
			height : 600,
			ondestroy : function(action) {
				if (action == "ok") {
					var iframe = this.getIFrameEl();
					var data = iframe.contentWindow.GetData();
					data = mini.clone(data); //必须
					if (data) {
						btnEdit.setValue(data.clino);
						btnEdit.setText(data.clino);
						mini.get("customer").setValue(data.cliname);
						if(data.clino == null){
							mini.alert("该客户ecif编号不存在！", "系统提示");
							return;
						}
						searchDetail(data.clino);
						btnEdit.focus();
					}
				}

			}
		});

	}

	function searchDetail(ecifNum) {
		CommonUtil.ajax({
			url : "/edCustController/getEdCustRiskListByEcifNum",
			data : {
				ecifNum : ecifNum
			},
			callback : function(data) {
				if (data.obj != null) {
					//TdEduSplit.datagrid.datagrid('loadData',data.obj);
					calcAmtSum(data.obj);
					grid.setTotalCount(data.obj.length);
					grid.setPageIndex(0);
					grid.setPageSize(50);
					grid.setData(data.obj);
				}
			}
		});
	};

	/**计算 授信额度 总和*/
	function calcAmtSum(rows) {
		var loanAmtSum = 0;
		var avlAmtSum = 0;
		var loanAmtSumAll = 0;
		var avlAmtSumAll = 0;
		for (var i = 0; i < rows.length; i++) {
			loanAmtSum = loanAmtSum + parseFloat(rows[i].loanAmt);
			avlAmtSum = avlAmtSum + parseFloat(rows[i].avlAmt);
			loanAmtSumAll = loanAmtSumAll + parseFloat(rows[i].loanAmtAll);
			avlAmtSumAll = avlAmtSumAll + parseFloat(rows[i].avlAmtAll);
		}
		mini.get("loanAmtSum").setValue(loanAmtSum);
		mini.get("avlAmtSum").setValue(avlAmtSum);
		mini.get("loanAmtSumAll").setValue(loanAmtSumAll);
		mini.get("avlAmtSumAll").setValue(avlAmtSumAll);
	};

	//品种编号的查询
	function onPrdSearch(e) {
		var btnEdit = this;
		mini.open({
			url : CommonUtil.baseWebPath()
					+ "../../quota/operate/mini/TcMiniCreditQuery.jsp",
			title : "额度中类选择",
			width : 900,
			height : 600,
			ondestroy : function(action) {
				if (action == "ok") {
					var iframe = this.getIFrameEl();
					var data = iframe.contentWindow.GetData();
					data = mini.clone(data); //必须
					if (data) {
						btnEdit.setValue(data.creditId);
						btnEdit.setText(data.creditId);
						mini.get("creditName").setValue(data.creditName);//额度品种

						btnEdit.focus();
					}
				}
			}
		});
	}
	
	//查询信贷额度编号
	function onLimitQuery(e) {
		var custNo = mini.get("ecifNum").getValue();
		var btnEdit = this;
		mini.open({
			url : CommonUtil.baseWebPath()
					+ "../../quota/operate/mini/TcLimitTypeQuery.jsp?action="+custNo,
			title : "额度选择",
			width : 900,
			height : 600,
			ondestroy : function(action) {
				if (action == "ok") {
					var iframe = this.getIFrameEl();
					var data = iframe.contentWindow.GetData();
					data = mini.clone(data); //必须
					if (data) {
						btnEdit.setValue(data.opnLmt);
						btnEdit.setText(data.opnLmt);//基础授信
						mini.get("avlAmt").setValue(data.opnLmt);//额度品种基础授信金额
						
						mini.get("loanAmtAll").setValue(data.crdtLmt);//授信额度
						mini.get("avlAmtAll").setValue(data.crdtLmt);//可用授信授信
						
						btnEdit.focus();
					}
				}
			}
		});
	}
	
	//同步授信数据
	function synch(e) {
		var custNo = mini.get("ecifNum").getValue();
		if(custNo == ""){
			mini.alert("请输入客户编号", "系统提示");
			return;
		}
		
		CommonUtil.ajax({
			url:"/edRiskController/synchData",
			data:custNo,
			callback : function(data) {
				if(data.obj == ""){
					mini.alert("授信没有相应产品！");
				}
				grid.setTotalCount(data.obj.length);
				grid.setPageIndex(0);
		        grid.setPageSize(50);
				grid.setData(data.obj);
				calcAmtSum(grid.getData());
				//commit();
			}
		});
	}
	
	//增加
	function add() {
		var detailForm = new mini.Form("#search_detail_form");
		detailForm.validate();
		if (detailForm.isValid() == false) {
			mini.alert("信息填写有误，请重新填写", "系统提示");
			return;
		}
		var detailData = detailForm.getData(true);
		addRow(detailData);

	}
	function addRow(detailData) {
		var arr = searchEduTermType();
		if (arr == null || arr.length == 0) {
			return;
		}
		var termTypeChinese;
		for (var i = 0; i < arr.length; i++) {
			if (detailData.termType == arr[i].id) {
				termTypeChinese = arr[i].text;
			}
		}

		var conData = grid.getData();
		if(conData.length>0){
			for(var i=0;i<conData.length;i++){
				productCode = conData[i].productType;
				if(productCode == detailData.productType){
					mini.alert("已有对 "+detailData.creditName+" 进行额度切分，请对原额度进行修改", "系统提示");
					return;
				}
			}
		}
		
		var row = {
			productType : detailData.productType,
			creditName : detailData.creditName,
			loanAmt : detailData.loanAmt,
			avlAmt : detailData.loanAmt,
			loanAmtAll : detailData.loanAmtAll,
			avlAmtAll : detailData.loanAmtAll,
			term : detailData.term,
			termType : detailData.termType,
			termName : detailData.term + termTypeChinese,
			currency : detailData.currency,
			dueDate : detailData.dueDate,
			dueDateLast : detailData.dueDateLast
		};
		grid.addRow(row);
		var detailForm = new mini.Form("#search_detail_form");
		detailForm.clear();
		mini.get("currency").setValue("CNY");
		mini.get("termType").setValue("05");
		//重新计算授信额度总和
		calcAmtSum(grid.getData());
	}

	function searchEduTermType() {
		var termArr = CommonUtil.serverData.dictionary.EduTermType;
		return termArr;
	}

	//提交
	function commit() {
		form.validate();
		if (form.isValid() == false) {
			mini.alert("客户额度切分总和信息表填写有误，请重新填写", "系统提示");
			return;
		}
		var param = form.getData(true);
		var rows = grid.getData();
		if (rows.length == 0) {
			return;
		}

		mini.confirm("确认给客户[<font style='color:red;'><strong>" + param.ecifNum
				+ "</strong></font>]切分额度吗？", "系统确认", function(value) {
			if (value == 'ok') {
				var params = new Array();
				for (var int = 0; int < rows.length; int++) {
					var paramSub = {};
					paramSub["sysId"] = "01";
					paramSub["ecifNum"] = param.ecifNum;
					paramSub["customer"] = param.customer;
					paramSub["productType"] = rows[int].productType;
					paramSub["creditName"] = rows[int].creditName;
					paramSub["loanAmt"] = rows[int].loanAmt;
					paramSub["loanAmtAll"] = rows[int].loanAmtAll;
					paramSub["term"] = rows[int].term;
					paramSub["termType"] = rows[int].termType;
					paramSub["currency"] = rows[int].currency;
					paramSub["dueDate"] = rows[int].dueDate;
					paramSub["dueDateLast"] = rows[int].dueDateLast;
					paramSub["cntrStatus"] = "02";
					params.push(paramSub);
				}
				param["EdCust"] = params;

				param = mini.encode(param);
				CommonUtil.ajax({
					url : "/edCustController/edCustSplitSave",
					data : param,
					callback : function(data) {
						mini.alert(data.obj.desc, "系统提示");
					}
				});
			}
		});

	}

	//删除
	function del() {
		var row = grid.getSelected();
		if (!row) {
			mini.alert("请选中要移除的一行", "提示");
			return;
		}
		grid.removeRow(row);
		//重新计算授信额度总和
		calcAmtSum(grid.getData());
	}

	//修改
	function edit() {
		var row = grid.getSelected();
		if (!row) {
			mini.alert("请选中一行进行修改!", "提示");
			return;
		}
		var detailForm = new mini.Form("#search_detail_form");
		detailForm.setData(row);
		mini.get("productType").setValue(row.productType);
		mini.get("productType").setText(row.productType);
		mini.get("loanAmt").setText(row.loanAmt);
		mini.get("loanAmt").setText(row.loanAmt);
		mini.get("add_btn").setEnabled(false);
		mini.get("edit_btn").setEnabled(false);
		mini.get("save_btn").setEnabled(true);
		mini.get("commit_btn").setEnabled(false);
		mini.get("synch_btn").setEnabled(false);
	}

	//修改保存
	function save() {
		del();
		add();
		mini.get("add_btn").setEnabled(true);
		mini.get("edit_btn").setEnabled(true);
		mini.get("save_btn").setEnabled(false);
		mini.get("commit_btn").setEnabled(true);
		mini.get("synch_btn").setEnabled(true);
	}
	
	function onDrawDateNear(e) {
        var nearValuedate = e.date;
        var fwdValuedate= mini.get("dueDateLast").getValue();
        if(CommonUtil.isNull(fwdValuedate)){
        	return;
        }
        if (fwdValuedate.getTime() < nearValuedate.getTime()) {
            e.allowSelect = false;
        }
    }
    
	function onDrawDateFar(e) {
        var fwdValuedate = e.date;
        var nearValuedate = mini.get("dueDate").getValue();
        if(CommonUtil.isNull(nearValuedate)){
        	return;
        }
        if (fwdValuedate.getTime() < nearValuedate.getTime()) {
            e.allowSelect = false;
        }
    }
	
	function associaDateCalculation(e){
		var nearValuedate = mini.get("dueDate").getValue();
		var fwdValuedate= mini.get("dueDateLast").getValue();
		if(CommonUtil.isNull(nearValuedate)||CommonUtil.isNull(fwdValuedate)){
			return;
		}
		var subDate= Math.abs(parseInt((fwdValuedate.getTime() - nearValuedate.getTime())/1000/3600/24));
		mini.get("term").setValue(subDate);
	}
	
	
</script>
</body>
</html>