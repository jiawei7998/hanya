<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
<title>匹配关系列表</title>
</head>
<body style="width:100%;height:100%;background:white">


<div class="mini-fit" >
	<div class="mini-panel" title="匹配关系列表" style="width:100%;height:100%;" showToolbar="true" showCloseButton="false" showFooter="true">
	<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id"  allowAlternating="true"
		onrowdblclick="onRowDblClick" allowResize="true" sortMode="client" allowAlternating="true">
		<div property="columns">
			<div type="indexcolumn" width="50px" headerAlign="center">序号</div>
			<div field="dealNo" width="120px" align="center"  headerAlign="center" >流水号</div>    
			<div field="dealType" width="120px" align="center" headerAlign="center" >业务类型</div>
			<div field="prdName" width="120px" align="center" headerAlign="center" >业务品种</div>
			<div field="vDate" width="120px" align="center"  headerAlign="center" >起息日</div> 
			<div field="mDate" width="120px" align="center"  headerAlign="center" >到期日</div> 
			<!-- <div field="sponName" width="120px" align="center"  headerAlign="center" >经营单位</div> --> 
			<div field="edOpType" width="120px" align="center"  headerAlign="center" >额度操作</div> 
			<div field="cnName" width="120px" align="center"  headerAlign="center" >客户名称</div> 
			<div field="creditName" width="120px" align="center"  headerAlign="center" >额度中类</div> 
			<div header="额度期限" headerAlign="center">
                <div property="columns">
                <div field="termId" width="80" align="center" headerAlign="center" >期限</div>
                <div field="termType" width="80" align="center" headerAlign="center"  renderer="CommonUtil.dictRenderer" data-options="{dict:'EduTermType'}" >类型</div>
                </div>
			</div>
			<div field="edOpAmt" width="120px" align="right"  headerAlign="center" allowSort="true" numberFormat="n2">额度金额</div> 
			<div field="batchId" width="120px" align="center"  headerAlign="center">批次ID</div> 
		</div>
	</div>
	</div>
</div>   

<script>
	mini.parse();
	var url = window.location.search;
	var ecifNo = CommonUtil.getParam(url, "ecifNo");
	var custCreditId = CommonUtil.getParam(url, "custCreditId");
	var edOpType = CommonUtil.getParam(url, "edOpType");
	
	var grid=mini.get("datagrid");

	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});
	
	$(document).ready(function() {
		search(grid.pageSize,0);
	});
	/* 查询 */
	function search(pageSize,pageIndex){
		var data={};
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['ecifNo']=ecifNo;
		data['custCreditId']=custCreditId;
		data['edOpType']=edOpType;
		
		
		data['branchId']=branchId;
		var url="/edCustBatchLogController/getTdEdCustBatchLogPage";

		var params = mini.encode(data);
		CommonUtil.ajax({
			url:url,
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	

</script>
</body>
</html>
