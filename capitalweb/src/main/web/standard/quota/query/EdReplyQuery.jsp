<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/hideBtn.js"></script>
<title>额度批复信息列表</title>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
	<legend>额度批复查询</legend>	
	<div>
		<div id="search_form" style="width:100%" cols="6">
			<input id="partyName" name="partyName" class="mini-buttonedit" onbuttonclick="onButtonEdit"  label="客户名称：" labelField="true"    width="280px" labelStyle="text-align:right;"  emptyText="请输入客户名称" />
			<input id="clientNo" name="clientNo" class="mini-textbox" labelField="true" label="客户编号：" labelStyle="text-align:right;" emptyText="请输入客户编号" enabled="false"/>
			<input id="approveId" name="approveId" class="mini-textbox" labelField="true" label="审批编号：" labelStyle="text-align:right;" emptyText="请输入审批编号" />
			<span style="float: right; margin-right: 150px"> 
				<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
				<a id="synch_btn" class="mini-button" style="display: none"   onclick="synch()">同步数据</a>
			</span>
		</div>
	</div>
</fieldset> 

<div class="mini-fit" >
	<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id"  allowAlternating="true"
		allowResize="true" sortMode="client" allowAlternating="true" multiSelect="true" >
		<div property="columns">
			<div type="indexcolumn" width="50px" headerAlign="center">序号</div>
			<div type="checkcolumn"></div>
			<div field="partyName" width="120px" align="center"  headerAlign="center" >客户名称</div>    
			<div field="clientNo" width="120px" align="center" headerAlign="center" >客户编号</div>
			<div field="orgNum" width="120px" align="center" headerAlign="center" >送审单位</div>
			<div field="userNum" width="120px" align="center"  headerAlign="center" >客户经理</div> 
			<div field="approveId" width="120px" align="center"  headerAlign="center" >批复编号</div> 
			<div field="oriBatchNo" width="120px" align="center"  headerAlign="center" >原批复编号</div> 
			<div field="productType" width="120px" align="center"  headerAlign="center" >业务品种</div> 
			<div field="accountProperty" width="120px" align="center"  headerAlign="center" >业务发生性质</div> 
			<div field="tenor" width="120px" align="center"  headerAlign="center" >期限</div> 
			<div type="comboboxcolumn" autoShowPopup="true" name="extenTermUnit" field="extenTermUnit" width="100" allowSort="true"  align="center" headerAlign="center">期限类型
                <input property="editor" class="mini-combobox" style="width:100%;" data="ExtenTermUnits" />                
            </div>
			<div field="loanAmt" width="120px" align="right"  headerAlign="center" allowSort="true" numberFormat="n2">授信额度</div> 
			<div field="loanCreditAmt" width="120px" align="right"  headerAlign="center" allowSort="true" numberFormat="n2">敞口额度</div> 
			<div field="lyed" width="120px" align="right"  headerAlign="center" allowSort="true" numberFormat="n2">备用额度</div>
			<div field="loanApplyed" width="120px" align="right"  headerAlign="center" allowSort="true" numberFormat="n2">再融资额度</div>
			<div field="currency" width="120px" align="center"  headerAlign="center" >币种</div>
			<div field="sameCustName" width="120px" align="center"  headerAlign="center" >所属平台客户名称</div>
			<div field="validDate" width="120px" align="center"  headerAlign="center" >批复生效日期</div>
			<div field="rateType" width="120px" align="center"  headerAlign="center" >利率类型</div>
			<div field="rateReceiveType" width="120px" align="center"  headerAlign="center" >利息收取方式</div>
			<div field="rate" width="120px" align="right"  headerAlign="center" allowSort="true" numberFormat="n2">利率</div>
			<div field="finalApprvResult" width="120px" align="center"  headerAlign="center" >审批结论</div>
			<div field="limitType" width="120px" align="center"  headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'YesNo'}">是否循环</div>
			<div field="loanAssuKind" width="120px" align="center"  headerAlign="center" >担保方及内容</div>
			<div field="siteNo" width="120px" align="center"  headerAlign="center" >终批机构</div>
			
		</div>
	</div>
</div>   

<script>
	var ExtenTermUnits = [{ id: 01, text: '年' },{ id: 02, text: '半年'},{ id: 03, text: '季'},{ id: 04, text: '月'}, { id: 05, text: '天'}];
	mini.parse();
	var form = new mini.Form("#search_form");
	var grid=mini.get("datagrid");
	var row="";

	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});
	
	grid.on("update", function (e) {
		 var rows = grid.findRows(function (row) {
		      return true;
		    
		   });
		   grid.selects(rows);
		});
	
	
	$(document).ready(function() {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
			query();
		});
	});
	//清空
	function clear(){
		form.clear();
		query();
	}

	/* 查询 按钮事件 */
	function query(){
		search(grid.pageSize,0);
	}
	/* 查询 */
	function search(pageSize,pageIndex){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统提示");
			return;
		}

		var data=form.getData();
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		var url="/CustCreditController/getCreditApproveInfoList";

		var params = mini.encode(data);
		CommonUtil.ajax({
			url:url,
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	var progressbar={
		    init:function(){
		        var fill=document.getElementById('fill');
		        var count=0;
		    //通过间隔定时器实现百分比文字效果,通过计算CSS动画持续时间进行间隔设置
		        var timer=setInterval(function(e){
		            count++;
		            fill.innerHTML=count+'%';
		            if(count===100) clearInterval(timer);
		        },17);
		    }
		};
	//显示效果
    function successtips() {
        mini.showTips({
            content: '<div id="wrapper">'+'<div id="progressbar">'+' <div id="fill"></div></div></div>',
           state: "success",
            offset: [0, 0],
            x: "center",		  
            y: "center",
            timeout: 3000 
        });
    	progressbar.init();
    }
	function synch(){
		var data=form.getData();
		if(data['partyName']==""){
			mini.alert("请选择客户名称","提示");
			return false;
		}
		if(data['clientNo']==""){
			mini.alert("客户编号不能为空","提示");
			return false;
		}
		/* successtips(); */
		
		mini.mask({
	            el: document.body,
	            cls: 'mini-mask-loading',
	            html: '数据正在同步，请稍后 ...'
	        });
		setTimeout(function(){
			mini.unmask(document.body);
			var url="/CustCreditController/synchCreditApproveInfo";
			var params = mini.encode(data);
			CommonUtil.ajax({
				url:url,
				data:params,
				showLoadingMsg:false,
				callback : function(data) {
					if(data.desc == "数据同步成功"){
						mini.alert(data.desc,"提示");
						query();
					}else{
						mini.alert(data.desc,"提示");
					}
				}
			});
		},2000);
	}
	function onButtonEdit(){
		var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../quota/config/CPMiniManage.jsp",
            title: "选择客户列表",
            width: 700,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.party_name);
						btnEdit.setText(data.party_name);
						mini.get("clientNo").setValue(data.party_id);
                        btnEdit.focus();
                    }
                }

            }
        });
	}
</script>
<style type="text/css">
#wrapper{
    position: relative;
    width:162px;
    height:20px;
    border:1px solid darkgray;
}
#progressbar{
    position: absolute;
    top:50%;
    left:50%;
    margin-left:-82px;
    margin-top:-10px;
    width:164px;
}
/*在进度条元素上调用动画,但是如果加动画效果的话，进度条会加到100%再倒回来*/
#fill{
   animation: move 2s; 
    text-align: center;
    background-color: green;
    height: 22px;
    color: white;
}
/*实现元素宽度的过渡动画效果*/
@keyframes move {
    0%{
        width:0;

    }
    100%{
        width:100%;
    }
}
</style>
</body>
</html>
