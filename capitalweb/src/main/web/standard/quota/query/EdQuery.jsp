<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/hideBtn.js"></script>
<style type="text/css">
	.myrow
    {
        background-color:#fff8f0;font-weight:bold;
    }
</style>

<title>额度信息列表</title>
</head>

<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
	<legend>额度查询</legend>	
	<div>
		<div id="search_form" style="width:100%" cols="6">
			<input id="custName" name="custName" class="mini-textbox"  label="客户名称（模糊）：" labelField="true"    width="280px" labelStyle="text-align:right;width:120px;"  emptyText="请输入客户名称" />
			<span style="float: right; margin-right: 150px"> 
				<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
			</span>
		</div>
	</div>
</fieldset> 

<div class="mini-fit" >
	<div id="credit_query_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id"  allowAlternating="true"
	onshowrowdetail="onShowRowDetail" allowResize="true" sortMode="client" allowAlternating="true">
		<div property="columns">
			<div type="indexcolumn" width="50px" headerAlign="center">序号</div>
			<div type="expandcolumn" ></div>
			<div field="ecifNum" width="120px" align="center"  headerAlign="center" >客户编号</div>    
			<div field="cnName" width="120px" align="center" headerAlign="center" >客户名称</div>
			<div field="loanAmt" width="120px" align="right" headerAlign="center" allowSort="true" numberFormat="n2">授信额度（元 ）</div>
			<div field="avlAmt" width="120px" align="right"  headerAlign="center" allowSort="true" numberFormat="n2">可用金额（元 ）</div> 
			<div field="dueDate" width="120px" align="center"  headerAlign="center" >到期日</div> 
		</div>
	</div>
</div>   


<div id="detailGrid_Form" style="display:none;">
    <div id="detail_grid" class="mini-datagrid" style="width:80%;height:250px;" allowResize="true" sortMode="client" allowAlternating="true">
        <div property="columns">
			<div field="productType" width="120px" align="center"  headerAlign="center" >品种编号</div>
			<div field="creditName" width="120px" align="center"  headerAlign="center" >额度品种</div>
			<div field="loanAmt" width="120px" align="right"  headerAlign="center" allowSort="true" numberFormat="n2">授信额度（元）</div>
			<div field="avlAmt" width="120px" align="right"  headerAlign="center" allowSort="true" numberFormat="n2">可用金额（元 ）</div>
			<div field="termName" width="120px" align="center"  headerAlign="center" >业务期限</div>
        </div>
    </div>    
</div>








<script>
	mini.parse();
	var form = new mini.Form("#search_form");
	var row="";
	var credit_query_grid = mini.get("credit_query_grid");
    var detail_grid = mini.get("detail_grid");
    var detailGrid_Form = document.getElementById("detailGrid_Form");

	function onShowRowDetail(e) {
            var grid = e.sender;
            var row = e.record;
            var td = grid.getRowDetailCellEl(row);
            td.appendChild(detailGrid_Form);
            detailGrid_Form.style.display = "block";
            searchDetail(10,0,row.ecifNum);
    		
    }
	function searchDetail(pageSize,pageIndex,ecifNum){
        var data = {};
        data["ecifNum"]=ecifNum;
        data['pageNumber']=pageIndex+1;
        data['pageSize']=pageSize;
        
        var param = mini.encode(data);

        CommonUtil.ajax( {
            url:"/edCustController/getEdCustForQueryDetailPage",
            data:param,
            callback : function(data) {
                
                var grid =mini.get("detail_grid");
                //设置分页
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                //设置数据
                grid.setData(data.obj.rows);
                
            }
        });

    }
    
    
	credit_query_grid.on("drawcell", function (e) {
        var record = e.record;
        //设置行样式
        if (CommonUtil.dateCompare(sysDate,record.dueDate)){
            e.rowCls = "myrow";
        }

    });

	
	credit_query_grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});
	detail_grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		var row = credit_query_grid.getSelected();
		searchDetail(pageSize,pageIndex,row.ecifNum);
	});
	
	//清空
	function clear(){
		form.clear();
		query();
	}

	/* 查询 按钮事件 */
	function query(){
		search(credit_query_grid.pageSize,0);
	}
	/* 查询 */
	function search(pageSize,pageIndex){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统提示");
			return;
		}

		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		var url="/edCustController/getEdCustForQueryPage";
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:url,
			data:params,
			callback : function(data) {
				credit_query_grid.setTotalCount(data.obj.total);
				credit_query_grid.setPageIndex(pageIndex);
				credit_query_grid.setPageSize(pageSize);
				credit_query_grid.setData(data.obj.rows);
			}
		});
	}
	
	
	$(document).ready(function() {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
			query();
		});
	});

</script>
</body>
</html>
