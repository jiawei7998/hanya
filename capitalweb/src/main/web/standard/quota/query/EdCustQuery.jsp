<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/hideBtn.js"></script>
<style type="text/css">
	.progressbar
    {
        position:relative;background:#bbb;width:100%;height:16px;overflow:hidden;
    }
    .progressbar-percent
    {
        position:absolute;height:18px;background:#ffe48d;left:0;top:0px;overflow:hidden;
        z-index:1;
    }
    .progressbar-label
    {
        position:absolute;left:0;top:0;width:100%;font-size:13px;
        z-index:10;
        text-align:center;
        height:16px;line-height:16px;
    }
</style>
<title>客户额度信息列表</title>
</head>

<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
	<legend>客户额度查询</legend>	
	<div>
		<div id="search_form" style="width:100%" cols="6">
			<input id="custNo" name="custNo" class="mini-textbox" labelField="true" label="客户编号：" labelStyle="text-align:right;" emptyText="请输入客户编号" />
			<input id="partyName" name="partyName" class="mini-textbox"  label="客户名称：" labelField="true"    width="280px" labelStyle="text-align:right;"  emptyText="请输入客户名称" />
			<input id="partyId" name="partyId" class="mini-hidden">
			<input id="expDate" name="expDate" class="mini-datepicker" labelField="true" label="额度到期日：" labelStyle="text-align:right;" emptyText="请输入额度到期日" />
			<span style="float: right; margin-right: 150px"> 
				<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
			</span>
		</div>
	</div>
</fieldset> 



<div id="tabs1" class="mini-tabs" activeIndex="0" style="width:100%;height:100%;" arrowPosition="side" showNavMenu="true">
<div name="tab1" title="额度总览">

<div class="mini-splitter" vertical="true" style="width:100%;height:100%;" handlerSize="2px" allowResize="false">
    <div size="40%" >
        <div class="mini-fit" style="width:100%;height:100%;" >
                <div id="credit_query_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" allowResize="true" ondrawcell="onDrawCell" 
                    idField="id" pageSize="10" multiSelect="false" sortMode="client" allowAlternating="true" onselectionchanged="onSelectionChangedSearchDetail">
                    <div property="columns">
                        <div type="indexcolumn" width="50px" headerAlign="center">序号</div>
                        <div field="ecifNum" width="120px" align="center"  headerAlign="center" >客户编号</div> 
						<div field="cnName" width="120px" align=""  headerAlign="center" >客户名称</div>    
						<div field="loanAmt" width="120px" align="right" headerAlign="center" allowSort="true" numberFormat="n2">基础授信额度(元)</div>
						<div field="avlAmt" width="120px" align="right"  headerAlign="center" allowSort="true" numberFormat="n2">可用基础授信额度(元)</div> 
						<div field="loanAmtAll" width="120px" align="right" headerAlign="center" allowSort="true" numberFormat="n2">授信额度(元)</div>
						<div field="avlAmtAll" width="120px" align="right"  headerAlign="center" allowSort="true" numberFormat="n2">可用授信额度(元)</div>
						<div field="percent" width="120px" align="right"  headerAlign="center" >额度余量(%)</div> 
                    </div>
                </div>
        </div>
	</div>
	
    <div>
        <div class="mini-fit" style="width:100%;height:100%;" >
            <div class="mini-panel" title="额度中类明细" style="width:100%;height:100%;" showToolbar="true" showCloseButton="false" showFooter="true">
    			
    			<div property="toolbar" class="mini-toolbar">
    			<span class="separator"></span>
    			<a class="mini-button" style="display: none"  plain="true" onclick="addTab('1','占用明细')">额度占用明细</a>
    			</div>

                <div id="quota_detail_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" allowResize="true"
                    idField="id" pageSize="10" multiSelect="false" sortMode="client" allowAlternating="true">
					<div property="columns">
                        <div type="indexcolumn" width="50px" headerAlign="center">序号</div>
						<div field="cnName" width="120px" align=""  headerAlign="center" >客户名称</div>    
						<div field="creditName" width="120px" align="center" headerAlign="center" >额度品种</div>
						<div field="term" width="120px" align="center" headerAlign="center" >期限</div>
						<div field="termType" width="120px" align="center"  headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'EduTermType'}">单位</div>
						<div header="额度中类-总览" headerAlign="center">
                			<div property="columns">
	                			<div field="loanAmt" width="180" align="right" headerAlign="center"  allowSort="true" numberFormat="n2">基础授信额度(元)</div>
		                			<div field="avlAmt" width="180" align="right" headerAlign="center" allowSort="true" numberFormat="n2" >可用基础授信额度(元)</div>
	                			<div field="loanAmtAll" width="180" align="right" headerAlign="center"  allowSort="true" numberFormat="n2">授信额度(元)</div>
	                			<div field="avlAmtAll" width="180" align="right" headerAlign="center" allowSort="true" numberFormat="n2" >可用授信额度(元)</div>
               			    </div>
						</div>
						<div field="dueDate" width="180" align="center" headerAlign="center"  >额度起始日期</div>
						<div field="dueDateLast" width="180" align="center" headerAlign="center"  >额度到期日</div>

						<div header="申请-额度" headerAlign="center">
                			<div property="columns">

                			<div field="preUsedAmt" width="180" align="right" headerAlign="center"  allowSort="true" numberFormat="n2">已用金额(元)</div>
               			    </div>
						</div>
					</div>
				</div>
            </div>
        </div>
    </div>
</div>
				
    </div>
	</div>

<script>
	mini.parse();
	var form = new mini.Form("#search_form");
	var row="";
	
	var grid =mini.get("credit_query_grid");
    grid.on("beforeload",function(e){
        e.cancel = true;
        var pageIndex = e.data.pageIndex; 
        var pageSize = e.data.pageSize;
        search(pageSize,pageIndex);
    });

    var quota_detail_grid =mini.get("quota_detail_grid");
    quota_detail_grid.on("beforeload",function(e){
         e.cancel = true;
         var pageIndex = e.data.pageIndex; 
         var pageSize = e.data.pageSize;
         var row = grid.getSelected();
         searchDetail(pageSize,pageIndex,row.ecifNum);
     });
	
	$(document).ready(function() {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
			query();
		});
	});
	
	
	//清空
	function clear(){
		form.clear();
		query();
	}

	/* 查询 按钮事件 */
	function query(){
		search(grid.pageSize,0);
	}
	/* 查询 */
	function search(pageSize,pageIndex){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统提示");
			return;
		}
		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		var url="/edCustController/getTdEdCustsPage";
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:url,
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	
	
	//额度总览选中=》出现额度中类明细
    function onSelectionChangedSearchDetail(e){
        var credit_query_grid = e.sender;
        var record = credit_query_grid.getSelected();
        if(!record){
		     return false;
	    }
        row=record;
        searchDetail(10,0,row.ecifNum);
    }
    function searchDetail(pageSize,pageIndex,ecifNum){
        var data = {};
        data["custNo"]=ecifNum;
        data['pageNumber']=pageIndex+1;
        data['pageSize']=pageSize;
        
        var param = mini.encode(data);

        CommonUtil.ajax( {
            url:"/edCustController/getTdEdCustDetailPage",
            data:param,
            callback : function(data) {
                
                var grid =mini.get("quota_detail_grid");
                //设置分页
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                //设置数据
                grid.setData(data.obj.rows);
                
            }
        });

    }
	
	
    function addTab(edOpType,title) {
        var tabs = mini.get("tabs1");
        var detailGrid =mini.get("quota_detail_grid");
        var record = detailGrid.getSelected();
        if(!record){
		     return false;
	    }
        var idStr = record.custCreditId+record.term+record.termType+edOpType;
		var array=tabs.getTabs();
		for (var int = 0; int < array.length; int++) {
			if(array[int].name==idStr){
				tabs.removeTab(idStr);
			}
		}
        //add tab
        var tab = {title: '['+record.cnName+']-['+record.creditName+']-['+record.term+record.termType+']-'+title,showCloseButton:true,name:idStr,
        		url:CommonUtil.baseWebPath()+'../../quota/query/EdCustQueryList.jsp?custCreditId='+record.custCreditId+'&ecifNo='+record.ecifNum+'&edOpType='+edOpType+'&ClickName='+idStr};
        tab = tabs.addTab(tab);
        //active tab
        tabs.activeTab(tab);
    }
	
    function addFrozenTab(edOpType,title) {
        var tabs = mini.get("tabs1");
        var detailGrid =mini.get("quota_detail_grid");
        var record = detailGrid.getSelected();
        if(!record){
		     return false;
	    }
        var idStr = record.custCreditId+record.term+record.termType+edOpType;
		var array=tabs.getTabs();
		for (var int = 0; int < array.length; int++) {
			if(array[int].name==idStr){
				tabs.removeTab(idStr);
			}
		}
        //add tab
        var tab = {title: '['+record.cnName+']-['+record.creditName+']-['+record.term+record.termType+']-'+title,showCloseButton:true,name:idStr,
        		url:CommonUtil.baseWebPath()+'../../quota/query/EdCustFrozenQueryList.jsp?custCreditId='+record.custCreditId+'&ecifNo='+record.ecifNum+'&edOpType='+edOpType+'&ClickName='+idStr};
        tab = tabs.addTab(tab);
        //active tab
        tabs.activeTab(tab);

    }
	/* function progressbar(e){
		var htmlstr = '<div id="wrapper">'+'<div id="progressbar">'+'<div id="fill" style="width:'+e.value+'">'+e.value+'</div>'+'</div>'+'</div>';
		return htmlstr;
	} */
	
	function onDrawCell(e){
        var field = e.field,
            value = e.value;
        //额度余量
        if (field == "percent") {
            e.cellHtml = '<div class="progressbar">'
                            + '<div class="progressbar-percent" style="width:' + value + ';"></div>'
                            + '<div class="progressbar-label">' + value + '</div>'
                        +'</div>';
        }
    };
	
	
	
</script>
	<!-- <style type="text/css">
	 #wrapper{
	    position: relative;
	    width:162px;
	    height:15px;
	    border:1px solid darkgray;
	}
	#progressbar{
	    position: absolute;
	    top:50%;
	    left:50%;
	    margin-left:-82px;
	    margin-top:-8px;
	    width:164px;
	} 
	/*在进度条元素上调用动画,但是如果加动画效果的话，进度条会加到100%再倒回来*/
	#fill{
	   /*  animation: move 2s; */
	    text-align: center;
	    background-color: #ffe48d;
	    height: 17px;
	}
	/*实现元素宽度的过渡动画效果*/
	@keyframes move {
	    0%{
	        width:0;
	
	    }
	    100%{
	        width:100%;
	    }
	}
	</style> -->

</body>
</html>
