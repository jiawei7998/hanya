<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
<title>匹配关系列表</title>
</head>
<body style="width:100%;height:100%;background:white">


<div class="mini-fit" >
	<div class="mini-panel" title="匹配关系列表" style="width:100%;height:100%;" showToolbar="true" showCloseButton="false" showFooter="true">
	<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id"  allowAlternating="true"
		onrowdblclick="onRowDblClick" allowResize="true" sortMode="client" allowAlternating="true">
		<div property="columns">
			<div type="indexcolumn" width="50px" headerAlign="center">序号</div>
			<div field="state" width="120px" align="center"  headerAlign="center" >操作类型</div>    
			<div field="dealNo" width="120px" align="center" headerAlign="center" >'审批流水号</div>
			<div field="custName" width="120px" align="center" headerAlign="center" >客户名称</div>
			<div field="vDate" width="120px" align="center"  headerAlign="center" >冻结起息日</div> 
			<div field="mDate" width="120px" align="center"  headerAlign="center" >冻结到期日</div> 
			<div field="creditAmt" width="120px" align="right"  headerAlign="center" allowSort="true" numberFormat="n2">授信额度(元)</div> 
			<div field="quotaAvlAmt" width="120px" align="right"  headerAlign="center" allowSort="true" numberFormat="n2">可用额度(元)</div> 
			<div field="frozenAmt" width="120px" align="right"  headerAlign="center" allowSort="true" numberFormat="n2">冻结金额(元)</div> 
			<div field="ioper" width="120px" align="center"  headerAlign="center" >经办人</div> 
			<div field="sponInstName" width="120px" align="center"  headerAlign="center" >经营单位</div> 
			<div field="approveStatus" width="120px" align="center"  headerAlign="center">审批状态</div>
			<div field="reason" width="120px" align="center"  headerAlign="center">备注</div>
		</div>
	</div>
	</div>
</div>   

<script>
	mini.parse();
	var url = window.location.search;
	var ecifNo = CommonUtil.getParam(url, "ecifNo");
	var custCreditId = CommonUtil.getParam(url, "custCreditId");
	var edOpType = CommonUtil.getParam(url, "edOpType");
	
	var grid=mini.get("datagrid");

	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});
	
	$(document).ready(function() {
		search(grid.pageSize,0);
	});
	/* 查询 */
	function search(pageSize,pageIndex){
		var data={};
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['ecifNo']=ecifNo;
		data['custCreditId']=custCreditId;
		data['edOpType']=edOpType;
		
		
		data['branchId']=branchId;
		var url="/CustCreditFrozenController/getCreditFrozenListForDetail";

		var params = mini.encode(data);
		CommonUtil.ajax({
			url:url,
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	

</script>
</body>
</html>
