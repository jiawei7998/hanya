<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
    <title>买入返售及卖出回购余额信息</title>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
</head>
<body style="width:100%;height:100%;background:white">
    <fieldset class="mini-fieldset title">
	    <legend>买入返售及卖出回购余额信息查询</legend>
		<div>
			<div id="search_form" style="width:80%" >
				<input id="postdate" name="postdate" class="mini-datepicker" labelField="true"  label="账务日期：" width="320px" labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请输入账务日期" />
				<input id="cno" name="cno" class="mini-textbox"  width="320px"  labelField="true"  label="客户号："  labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请输入客户号" />
				<input id="dealno" name="dealno" class="mini-textbox"  width="320px"  labelField="true"  label="业务编码："  labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请输入业务编码" />
				<input id="ccy" name="ccy" class="mini-combobox"  data="CommonUtil.serverData.dictionary.Currency" width="320px"  labelField="true"  label="币种："  labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请输入币种" />
				<span style="float: right; margin-right: 100px">
					<a id="search_btn" class="mini-button" style="display: none"   onclick="search()">查询</a>
					<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
				</span>
			</div>
		</div>
    </fieldset>
    <div class="mini-fit" style="width:100%;height:100%;">
        <div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" 
            sortMode="client" allowAlternating="true"    allowResize="true" >
            <div property="columns">
				<div field="postdate" width="100px" allowSort="false" headerAlign="center" align="center" renderer="onDateRenderer">账务日期</div>
				<div field="cno" width="100px" allowSort="false" headerAlign="center" align="center">客户号</div>
				<div field="br" width="100px" allowSort="false" headerAlign="center" align="center">内部机构号</div>
				<div field="dealno" width="100px" allowSort="false" headerAlign="center" align="center">业务编码</div>
				<div field="ccy" width="100px" allowSort="false" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer"
					 data-options="{dict:'Currency'}">币种</div>
				<div field="notamt" width="100px" allowSort="false" headerAlign="center" align="center" align="right"  numberFormat="#,0.0000">余额</div>
			</div>
        </div>
    </div>
</body>
<script>
    mini.parse();

    var form = new mini.Form("#search_form");
    var grid = mini.get("datagrid");
    var url=window.location.search;
    
    grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		searchs(pageSize,pageIndex);
	});

    $(document).ready(function() {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
			search();
		});
	});

    function search(){
    	 searchs(10,0);
    } 
    
    function searchs(pageSize, pageIndex) {
        form.validate();
        if (form.isValid() == false) {
            mini.alert("表单填写错误,请确认!", "提示信息");
            return;
        }
        var data = form.getData(true);
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        data['branchId']=branchId;
        var params = mini.encode(data);
        CommonUtil.ajax({
            url : "/IfsSubjectdetailController/searchpage",
            data : params,
            callback : function(data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }
	function onDateRenderer(e) {
		var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)==null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g,'/'));
		if (value) return mini.formatDate(value, 'yyyy-MM-dd');
	}


    function clear(){
        var form=new mini.Form("search_form");
        form.clear();
        search(10,0);
    }

    
    // function onRowDblClick(e) {
	// 	var row = grid.getSelected();
	//     var lstmntdte = new Date(row.lstmntdte);
	//     row.lstmntdte=lstmntdte;
	//     if(row){
	//             var url = CommonUtil.baseWebPath() + "/refund/subjectDetailEdit.jsp.jsp?action=detail";
	//             var tab = { id: "subjectDetail", name: "subjectDetail", title: "OPICS余额详情", url: url ,showCloseButton:true};
	//             var paramData = {selectData:row};
	//             CommonUtil.openNewMenuTab(tab,paramData);
	//     } else {
	//         mini.alert("请选中一条记录！","消息提示");
	//     }
    // }
    
</script>
</html>