<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../global.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>同业客户-基本信息表</title>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
</head>
<body style="width: 100%; height: 100%; background: white">
<fieldset class="mini-fieldset">
    <div id="search_form" style="width: 50%;">
        <legend>查询条件</legend>
        <input id="queryDate" name="queryDate" class="mini-datepicker mini-mustFill" required="true" labelField="true"
               label="日期：" required="true" labelStyle="text-align:right;" emptyText="起始日期" format="yyyy-MM-dd"
               value="<%=__bizDate%>"/>
        <span style="float:right;">
			<a id="search_btn" class="mini-button" style="display: none" onclick="create()">查询</a>
			<a id="export_btn" class="mini-button" style="display: none" onclick="exportExcel()">导出报表</a>
		</span>
    </div>
</fieldset>

<div class="mini-fit" style="margin-top: 2px;margin-bottom: 2px;">
    <div id="datagrid" class="mini-datagrid borderAll" style="width: 100%; height: 100%;" allowAlternating="true" allowHeaderWrap="true" allowCellWrap ="true"
         multiSelect="true" allowResize="true" border="true" sortMode="client" multiSelect="true">
        <div property="columns">
            <div type="indexcolumn" headerAlign="center" width="40">序号</div>
            <div field="khmc" name="khmc" width="150" align="center" headerAlign="center" allowSort="false">客户名称</div>
            <div field="khdm" name="khdm" width="120" align="center" headerAlign="center" allowSort="false">客户代码</div>
            <div field="khlb" name="khlb" width="100" align="center" headerAlign="center" allowSort="false">客户类别</div>
            <div field="shdm" name="shdm" width="250" align="center" headerAlign="center" allowSort="false">统一信用社会代码/组织机构代码证代码</div>
            <div field="gbjdq" name="gbjdq" width="100" align="center" headerAlign="center" allowSort="false">国别及地区</div>
            <div field="nbpj" name="nbpj" width="100" align="center" headerAlign="center" allowSort="false">内部评级</div>
            <div field="wbpj" name="wbpj" width="100" align="center" headerAlign="center" allowSort="false">外部评级</div>
            <div field="chafty" name="chafty" width="100" align="right" headerAlign="center" allowSort="false" numberFormat="n2">拆放同业（元）</div>
            <div field="cunfty" name="cunfty" width="100" align="right" headerAlign="center" allowSort="false" numberFormat="n2">存放同业（元）</div>
            <div field="zqhgmrfs" name="zqhgmrfs" width="180" align="right" headerAlign="center" allowSort="false" numberFormat="n2">债券回购（元）-买入返售</div>
            <div field="gpzydk" name="gpzydk" width="140" align="right" headerAlign="center" allowSort="false" numberFormat="n2">股票质押贷款（元）</div>
            <div field="mrfszc" name="mrfszc" width="140" align="right" headerAlign="center" allowSort="false" numberFormat="n2">买入返售资产（元）</div>
            <div field="mdsztx" name="mdsztx" width="140" align="right" headerAlign="center" allowSort="false" numberFormat="n2">买断式转贴现（元）</div>
            <div field="cyzq" name="cyzq" width="100" align="right" headerAlign="center" allowSort="false" numberFormat="n2">持有债券（元）</div>
            <div field="gqtz" name="gqtz" width="100" align="right" headerAlign="center" allowSort="false" numberFormat="n2">股权投资（元）</div>
            <div field="tydf" name="tydf" width="100" align="right" headerAlign="center" allowSort="false" numberFormat="n2">同业代付（元）</div>
            <div field="mchgzc" name="mchgzc" width="140" align="right" headerAlign="center" allowSort="false" numberFormat="n2">卖出回购资产（元）</div>
            <div field="qtbnywcdtz" name="qtbnywcdtz" width="160" align="right" headerAlign="center" allowSort="false" numberFormat="n2">其他表内业务-存单投资</div>
        </div>
    </div>
</div>

<script>
    mini.parse();
    var url = window.location.search;
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var id = params.id;
    var grid = mini.get("datagrid");
    var userId = '<%=__sessionUser.getUserId()%>';
    var form = new mini.Form("#search_form");

    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize, pageIndex);
    });


    // 查询
    function search(pageSize, pageIndex) {
        var form = new mini.Form("#search_form");
        form.validate();
        if (form.isValid() == false) {
            mini.alert("信息填写有误，请重新填写", "系统也提示");
            return;
        }
        var data = form.getData(true);
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        data['branchId'] = branchId;
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: "/IfsReportTykhjbxxReportController/searchIfsReportTykhjbxxReportPage",
            data: params,
            callback: function (data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }

    function query() {
        search(grid.pageSize, 0);
    }

    function clear() {
        var form = new mini.Form("search_form");
        form.clear();
        search(10, 0);
    }

    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
            search(100, 0);
        });
    });


    //交易日期
    function onDrawDateStart(e) {
        var startDate = e.date;
        var endDate = mini.get("edate").getValue();
        if (CommonUtil.isNull(endDate)) {
            return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }

    function onDrawDateEnd(e) {
        var endDate = e.date;
        var startDate = mini.get("sdate").getValue();
        if (CommonUtil.isNull(startDate)) {
            return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }

    //生成报表
    function create() {
        form.validate();
        if (form.isValid() == false) {
            mini.alert("信息填写有误，请重新填写", "系统提示");
            return;
        }
        var data = form.getData(true);
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: "/IfsReportTykhjbxxReportController/createIfsReportTykhjbxx",
            data: params,
            complete: function (data) {
                var text = data.responseText;
                var desc = JSON.parse(text);
                var content = desc.obj.retMsg;
                mini.alert(content);
                search(100, 0);
            }
        });
    }

    //导出
    function exportExcel(){
        var data = form.getData(true);
        var fields = null;
        var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
        fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
        exportHrbExcel(fields);
    }
</script>
</body>
</html>