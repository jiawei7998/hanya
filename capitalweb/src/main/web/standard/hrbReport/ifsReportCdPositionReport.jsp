<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
    <title>G14台账</title>
</head>
<body>
<fieldset class="mini-fieldset title">
    <div id="search_form" style="width:100%">
        <legend>报表查询</legend>
        <input id="queryDate" name="queryDate" class="mini-monthpicker" labelField="true" label="交易日期:"
               value="<%=__bizDate%>" labelSyle="text-align:right;" emptyText="请输入"></input>
        <span>
            <a class="mini-button" style="display: none"  id="clear_btn" onclick="clear()">清空</a>
            <a class="mini-button" style="display: none"  id="create_btn" onclick="query()">查询</a>
            <a id="export_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
        </span>
    </div>
</fieldset>
<div class="mini-fit" style="width:100%;height:100%;">
    <div id="data_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
         sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
         border="true" allowResize="true">
        <div property="columns">
            <div field="seq" headerAlign="center" align="center" width="50px">序号</div>
            <div field="secid" headerAlign="center" align="center" width="150px">存单代码</div>
            <div field="acctngtype" headerAlign="center" align="center" width="150px">分类</div>
            <div field="cust" headerAlign="center" align="center" width="150px">认购人</div>
            <div field="faceAmt" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">存单面值</div>
            <div field="amt" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">应缴纳金额</div>
            <div field="interestAdj" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">利息调整</div>
            <div field="unamortamt" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">利息调整余额</div>
            <div field="vdate" headerAlign="center" align="center" width="150px">起息日</div>
            <div field="mdate" headerAlign="center" align="center" width="150px">到期日</div>
            <div field="issDays" headerAlign="center" align="center" width="150px">到期日</div>
            <div field="amortizeStraight" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">直线摊销法摊销</div>
            <div field="amortize" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">已摊销</div>
            <div field="days" headerAlign="center" align="center" width="150px">剩余天数</div>
            <div field="daysRange" headerAlign="center" align="center" width="150px">剩余天数区间</div>
            <div field="mon" headerAlign="center" align="center" width="0px">报送月份</div>
        </div>
    </div>
</div>
<script>
    mini.parse();

    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var id = params.id;

    var grid = mini.get("data_grid");
    var form = new mini.Form("#search_form");

    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize,pageIndex);
    });

    $(document).ready(function() {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
            query();
        });
    });

    //查询按钮
    function query(){
        search(100,0);
    }

    function search(pageSize, pageIndex) {
        var searchUrl="/IfsReportCdPositionController/searchAllPage";

        var data = form.getData(true);
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;

        var params = mini.encode(data);

        CommonUtil.ajax({
            url:searchUrl,
            data:params,
            callback:function(data){
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }

    //清空按钮
    function clear() {
        form.clear();
        query();
    }

    function exportExcel(){
        var data = form.getData(true);
        var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
        fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
        exportHrbExcel(fields);
    }

</script>
</body>
</html>
