<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
    <title>G14台账</title>
</head>
<body>
<fieldset class="mini-fieldset title">
    <div id="search_form" style="width:100%">
        <legend>报表查询</legend>
        <input id="queryDate" name="queryDate" class="mini-datepicker" labelField="true" label="交易日期:"
               value="<%=__bizDate%>" labelSyle="text-align:right;" emptyText="请输入"></input>
        <span>
            <a class="mini-button" style="display: none"  id="clear_btn" onclick="clear()">清空</a>
            <a class="mini-button" style="display: none"  id="create_btn" onclick="query()">查询</a>
            <a id="export_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
        </span>
    </div>
</fieldset>
<div class="mini-fit" style="width:100%;height:100%;">
    <div id="data_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
         sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
         border="true" allowResize="true">
        <div property="columns">
            <div type="indexcolumn" width="50px" headerAlign="center">序号</div>
            <div field="custName" headerAlign="center" align="center" width="150px">客户名称</div>
            <div field="amt" headerAlign="center" align="center" width="150px" numberFormat="#,0.0000">余额/金额</div>
            <div field="decrease" headerAlign="center" align="center" width="150px" numberFormat="#,0.0000">减值准备</div>
            <div header="考虑风险缓释作用的风险暴露" headerAlign="center">
                <div property="columns">
                    <div field="genRiskSum" headerAlign="center" align="center" width="150px" numberFormat="#,0.0000">一般风险暴露</div>
                    <div field="specRiskSum" headerAlign="center" align="center" width="150px" numberFormat="#,0.0000">特定风险暴露</div>
                    <div field="tradeRiskExp" headerAlign="center" align="center" width="150px" numberFormat="#,0.0000">交易账簿风险暴露</div>
                    <div field="custRiskSum" headerAlign="center" align="center" width="150px" numberFormat="#,0.0000">交易对手信用风险暴露</div>
                    <div field="qzRiskExp" headerAlign="center" align="center" width="150px" numberFormat="#,0.0000">潜在风险暴露</div>
                    <div field="qtRiskExp" headerAlign="center" align="center" width="150px" numberFormat="#,0.0000">其他风险暴露</div>
                </div>
            </div>
            <div field="bkhmRiskExp" headerAlign="center" align="center" width="150px" numberFormat="#,0.0000">不可豁免风险暴露</div>
            <div field="revRiskEffect" headerAlign="center" align="center" width="150px" numberFormat="#,0.0000">风险缓释作用</div>
            <div field="includRevRisk" headerAlign="center" align="center" width="150px" numberFormat="#,0.0000">考虑风险缓释作用的风险暴露合计</div>
            <div field="revRiskExp" headerAlign="center" align="center" width="150px" numberFormat="#,0.0000">风险缓释转出的风险暴露（转入为负数）</div>
            <div field="noneRevRisk" headerAlign="center" align="center" width="150px" numberFormat="#,0.0000">不考虑风险缓释作用的风险暴露合计</div>
            <div field="riskSec" headerAlign="center" align="center" width="150px" numberFormat="#,0.0000">其中：债券投资</div>
            <div field="riskZcgl" headerAlign="center" align="center" width="150px" numberFormat="#,0.0000">其中：资产管理产品</div>
            <div header="考虑风险缓释作用的风险暴露" headerAlign="center">
                <div property="columns">
                    <div field="riskZcglXt" headerAlign="center" align="center" width="150px" numberFormat="#,0.0000">其中:信托产品</div>
                    <div field="riskZcglLc" headerAlign="center" align="center" width="150px" numberFormat="#,0.0000">其中:非保本理财</div>
                    <div field="riskZcglZq" headerAlign="center" align="center" width="150px" numberFormat="#,0.0000">其中:证券业资管产品</div>
                </div>
            </div>
            <div field="riskAbs" headerAlign="center" align="center" width="150px" numberFormat="#,0.0000">资产证券化产品</div>
            <div field="remarkSecType" headerAlign="center" align="center" width="150px">补充1(债券类型)</div>
            <div field="remarkGroup" headerAlign="center" align="center" width="150px">补充2(所属集团)</div>
            <div field="remarkCustType" headerAlign="center" align="center" width="150px">补充3(客户类型)</div>
        </div>
    </div>
</div>
<script>
    mini.parse();

    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var id = params.id;

    var grid = mini.get("data_grid");
    var form = new mini.Form("#search_form");

    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize,pageIndex);
    });

    $(document).ready(function() {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
            query();
        });
    });

    //查询按钮
    function query(){
        search(100,0);
    }

    function search(pageSize, pageIndex) {
        var searchUrl="/IfsReportG14DetailController/searchAllPage";

        var data = form.getData(true);
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;

        var params = mini.encode(data);

        CommonUtil.ajax({
            url:searchUrl,
            data:params,
            callback:function(data){
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }

    //清空按钮
    function clear() {
        form.clear();
        query();
    }

    function exportExcel(){
        var data = form.getData(true);
        var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
        fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
        exportHrbExcel(fields);
    }

</script>
</body>
</html>
