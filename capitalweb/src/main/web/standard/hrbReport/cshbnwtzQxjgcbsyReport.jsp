<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../global.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>城商行表内外投资业务和同业交易对手情况表 表内外投资业务期限结构及成本收益表</title>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
</head>
<body style="width: 100%; height: 100%; background: white">
<fieldset class="mini-fieldset">
    <div id="search_form" style="width: 50%;">
        <legend>查询条件</legend>
        <input id="queryDate" name="queryDate" class="mini-datepicker mini-mustFill" required="true" labelField="true"
               label="日期：" required="true" labelStyle="text-align:right;" emptyText="起始日期" format="yyyy-MM-dd"
               value="<%=__bizDate%>"/>
        <span style="float:right;">
			<a id="search_btn" class="mini-button" style="display: none" onclick="create()">查询</a>
			<a id="export_btn" class="mini-button" style="display: none" onclick="exportExcel()">导出报表</a>
		</span>
    </div>
    <span style="float:right;">单位:万元，%</span>
</fieldset>

<div class="mini-fit" style="margin-top: 2px;margin-bottom: 2px;">
    <div id="datagrid" class="mini-datagrid borderAll" style="width: 100%; height: 100%;" allowAlternating="true"
         multiSelect="true" allowResize="true" border="true" sortMode="client" multiSelect="true">
        <div property="columns">
            <div type="indexcolumn" headerAlign="center" width="40" height="100">序号</div>

            <div field="product" name="product" width="80" align="center" headerAlign="center" allowSort="false">项目
            </div>

            <div header="资金来源" headerAlign="center">
                <div property="columns">
                    <div field="zjlyQx" name="zjlyQx" width="120" align="left" headerAlign="center" allowSort="false">
                        期限
                    </div>
                    <div width="120" headerAlign="center">余额
                        <div property="columns">
                            <div field="zjlyYeBjmAmt" name="zjlyYeBjmAmt" width="180" align="right" headerAlign="center"
                                 allowSort="false" numberFormat="n4">本季末数额
                            </div>
                            <div field="zjlyYeSjmAmt" name="zjlyYeSjmAmt" width="180" align="right" headerAlign="center"
                                 allowSort="false" numberFormat="n4">上季末数额
                            </div>
                        </div>
                    </div>
                    <div width="120" headerAlign="center">平均年化资金成本
                        <div property="columns">
                            <div field="zjlyNhcbCzbjmljAmt" name="zjlyNhcbCzbjmljAmt" width="180" align="right"
                                 headerAlign="center" allowSort="false" numberFormat="n4">年初至本季末累计资金成本率
                            </div>
                            <div field="zjlyNhcbCzsjmljAmt" name="zjlyNhcbCzsjmljAmt" width="180" align="right"
                                 headerAlign="center" allowSort="false" numberFormat="n4">年初至上季末累计资金成本率
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div header="资金运用" headerAlign="center">
                <div property="columns">
                    <div field="zjyyDqqx" name="zjyyDqqx" width="120" align="left" headerAlign="center"
                         allowSort="false">到期期限
                    </div>
                    <div width="120" headerAlign="center">余额
                        <div property="columns">
                            <div field="zjyyYeBjmAmt" name="zjyyYeBjmAmt" width="180" align="right" headerAlign="center"
                                 allowSort="false" numberFormat="n4">本季末数额
                            </div>
                            <div field="zjyyYeSjmAmt" name="zjyyYeSjmAmt" width="180" align="right" headerAlign="center"
                                 allowSort="false" numberFormat="n4">上季末数额
                            </div>
                        </div>
                    </div>
                    <div width="120" headerAlign="center">平均年化收益水平
                        <div property="columns">
                            <div field="zjyyNhsyCzbjmljAmt" name="zjyyNhsyCzbjmljAmt" width="180" align="right"
                                 headerAlign="center" allowSort="false" numberFormat="n4">年初至本季末累计年化收益率
                            </div>
                            <div field="zjyyNhsyCzsjmljAmt" name="zjyyNhsyCzsjmljAmt" width="180" align="right"
                                 headerAlign="center" allowSort="false" numberFormat="n4">年初至上季末累计年化收益率
                            </div>
                        </div>
                    </div>
                    <div width="120" headerAlign="center">其中：非标债权
                        <div property="columns">
                            <div field="zjyyFbzqBjmAmt" name="zjyyFbzqBjmAmt" width="180" align="right"
                                 headerAlign="center" allowSort="false" numberFormat="n4">本季末数额
                            </div>
                            <div field="zjyyFbzqSjmAmt" name="zjyyFbzqSjmAmt" width="180" align="right"
                                 headerAlign="center" allowSort="false" numberFormat="n4">上季末数额
                            </div>
                        </div>
                    </div>
                    <div width="120" headerAlign="center">其中：非标债权平均年化收益水平
                        <div property="columns">
                            <div field="zjyyFbzqnhCzbjmljAmt" name="zjyyFbzqnhCzbjmljAmt" width="180" align="right"
                                 headerAlign="center" allowSort="false" numberFormat="n4">年初至本季末累计年化收益率
                            </div>
                            <div field="zjyyFbzqnhCzsjmljAmt" name="zjyyFbzqnhCzsjmljAmt" width="180" align="right"
                                 headerAlign="center" allowSort="false" numberFormat="n4">年初至上季末累计年化收益率
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<script>
    mini.parse();
    var url = window.location.search;
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var id = params.id;
    var grid = mini.get("datagrid");
    var userId = '<%=__sessionUser.getUserId()%>';
    var form = new mini.Form("#search_form");

    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize, pageIndex);
    });


    // 查询
    function search(pageSize, pageIndex) {
        var form = new mini.Form("#search_form");
        form.validate();
        if (form.isValid() == false) {
            mini.alert("信息填写有误，请重新填写", "系统也提示");
            return;
        }
        var data = form.getData(true);
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        data['branchId'] = branchId;
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: "/IfsReportCshbnwtzQxjgcbsyReportController/searchIfsReportCshbnwtzQxjgcbsyReportPage",
            data: params,
            callback: function (data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);

                var marges = [
                    {rowIndex: 0, columnIndex: 1, rowSpan: 10, colSpan: 1}//表内资金
                    , {rowIndex: 10, columnIndex: 1, rowSpan: 13, colSpan: 1}//表外资金
                ];
                grid.mergeCells(marges);
            }
        });
    }

    function query() {
        search(grid.pageSize, 0);
    }

    function clear() {
        var form = new mini.Form("search_form");
        form.clear();
        search(10, 0);
    }

    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
            search(100, 0);
        });
    });


    //交易日期
    function onDrawDateStart(e) {
        var startDate = e.date;
        var endDate = mini.get("edate").getValue();
        if (CommonUtil.isNull(endDate)) {
            return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }

    function onDrawDateEnd(e) {
        var endDate = e.date;
        var startDate = mini.get("sdate").getValue();
        if (CommonUtil.isNull(startDate)) {
            return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }

    //生成报表
    function create() {
        form.validate();
        if (form.isValid() == false) {
            mini.alert("信息填写有误，请重新填写", "系统提示");
            return;
        }
        var data = form.getData(true);
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: "/IfsReportCshbnwtzQxjgcbsyReportController/createIfsReportCshbnwtzQxjgcbsy",
            data: params,
            complete: function (data) {
                var text = data.responseText;
                var desc = JSON.parse(text);
                var content = desc.obj.retMsg;
                mini.alert(content);
                search(100, 0);
            }
        });
    }

    //导出
    function exportExcel(){
        var data = form.getData(true);
        var fields = null;
        var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
        fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
        exportHrbExcel(fields);
    }
</script>
</body>
</html>