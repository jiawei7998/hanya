<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../global.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>地方法人金融机构月度监测表（表二）</title>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
</head>
<body style="width: 100%; height: 100%; background: white">
<fieldset class="mini-fieldset">
    <div id="search_form" style="width: 50%;">
        <legend>查询条件</legend>
        <input id="queryDate" name="queryDate" class="mini-datepicker mini-mustFill" required="true" labelField="true"
               label="日期：" required="true" labelStyle="text-align:right;" emptyText="起始日期" format="yyyy-MM-dd"
               value="<%=__bizDate%>"/>
        <span style="float:right;">
			<a id="search_btn" class="mini-button" style="display: none" onclick="create()">查询</a>
			<a id="export_btn" class="mini-button" style="display: none" onclick="exportExcel()">导出报表</a>
		</span>
    </div>
    <span style="float:right;">单位:%</span>
</fieldset>

<div class="mini-fit" style="margin-top: 2px;margin-bottom: 2px;">
    <div id="datagrid" class="mini-datagrid borderAll" style="width: 100%; height: 100%;" allowAlternating="true" allowHeaderWrap="true" allowCellWrap ="true"
         multiSelect="true" allowResize="true" border="true" sortMode="client" multiSelect="true">
        <div property="columns">
            <div type="indexcolumn" headerAlign="center" width="40">序号</div>
            <div field="jglb" name="jglb" width="60" align="center" headerAlign="center" allowSort="false">机构类别</div>
            <div field="jgmc" name="jgmc" width="60" align="center" headerAlign="center" allowSort="false">机构名称</div>
            <div header="同业情况-计财" headerAlign="center">
                <div property="columns">
                    <div field="tyqkTrjtcczfz" name="tyqkTrjtcczfz" width="60" align="right" headerAlign="center"  allowSort="false" numberFormat="n4">（同业融入+同业存单）/总负债(%)</div>
                    <div field="tyqkZddjtcbl" name="tyqkZddjtcbl" width="60" align="right" headerAlign="center" allowSort="false" numberFormat="n4">最大单家同业融出比例(%)</div>
                </div>
            </div>
            <div header="资产质量-授信、财会" headerAlign="center">
                <div property="columns">
                    <div field="zczlYq90zbldkbl" name="zczlYq90zbldkbl" width="60" align="right" headerAlign="center"  allowSort="false" numberFormat="n4">逾期90天以上贷款占不良贷款比例(%)</div>
                    <div field="zczlBldkl" name="zczlBldkl" width="60" align="right" headerAlign="center" allowSort="false" numberFormat="n4">不良贷款率(%)</div>
                    <div field="zczlDksszbczl" name="zczlDksszbczl" width="60" align="right" headerAlign="center"  allowSort="false" numberFormat="n4">贷款损失准备充足率(%)</div>
                    <div field="zczlBbfgl" name="zczlBbfgl" width="60" align="right" headerAlign="center" allowSort="false" numberFormat="n4">拨备覆盖率(%)</div>
                    <div field="zczlDkbbl" name="zczlDkbbl" width="60" align="right" headerAlign="center"  allowSort="false" numberFormat="n4">贷款拨备率(%)</div>
                </div>
            </div>
            <div header="利润情况-财会" headerAlign="center">
                <div property="columns">
                    <div field="lrqkCbsrb" name="lrqkCbsrb" width="60" align="right" headerAlign="center"  allowSort="false" numberFormat="n4">成本收入比(%)</div>
                    <div field="lrqkZblrl" name="lrqkZblrl" width="60" align="right" headerAlign="center" allowSort="false" numberFormat="n4">资本利润率(%)</div>
                    <div field="lrqkZclrl" name="lrqkZclrl" width="60" align="right" headerAlign="center"  allowSort="false" numberFormat="n4">资产利润率(%)</div>
                </div>
            </div>
            <div header="流动性-资负、财会" headerAlign="center">
                <div property="columns">
                    <div field="ldxRmbcebfjl" name="ldxRmbcebfjl" width="60" align="right" headerAlign="center"  allowSort="false" numberFormat="n4">人民币超额备付金率(%)</div>
                    <div field="ldxCdkbl" name="ldxCdkbl" width="60" align="right" headerAlign="center" allowSort="false" numberFormat="n4">存贷款比例(%)</div>
                    <div field="ldxLdxbl" name="ldxLdxbl" width="60" align="right" headerAlign="center"  allowSort="false" numberFormat="n4">流动性比例(%)</div>
                    <div field="ldxLdxfgl" name="ldxLdxfgl" width="60" align="right" headerAlign="center" allowSort="false" numberFormat="n4">流动性覆盖率(%)</div>
                    <div field="ldxJwdzjbl" name="ldxJwdzjbl" width="60" align="right" headerAlign="center"  allowSort="false" numberFormat="n4">净稳定资金比例(%)</div>
                    <div field="ldxLdxqkl" name="ldxLdxqkl" width="60" align="right" headerAlign="center" allowSort="false" numberFormat="n4">流动性缺口率(%)</div>
                    <div field="ldxHxfzycd" name="ldxHxfzycd" width="60" align="right" headerAlign="center" allowSort="false" numberFormat="n4">核心负债依存度(%)</div>
                    <div field="ldxLdxppl" name="ldxLdxppl" width="60" align="right" headerAlign="center"  allowSort="false" numberFormat="n4">流动性匹配率(%)</div>
                    <div field="ldxYzldxzcczl" name="ldxYzldxzcczl" width="60" align="right" headerAlign="center" allowSort="false" numberFormat="n4">优质流动性资产充足率(%)</div>
                </div>
            </div>
            <div header="集中度-授信、金融市场部" headerAlign="center">
                <div property="columns">
                    <div field="jzdDykhsxjzd" name="jzdDykhsxjzd" width="60" align="right" headerAlign="center"  allowSort="false" numberFormat="n4">单一客户授信集中度(%)</div>
                    <div field="jzdZdsjjtkhsxjzd" name="jzdZdsjjtkhsxjzd" width="60" align="right" headerAlign="center" allowSort="false" numberFormat="n4">最大十家集团客户授信集中度(%)</div>
                    <div field="jzdZdftyfxblzyjzbjebl" name="jzdZdftyfxblzyjzbjebl" width="60" align="right" headerAlign="center"  allowSort="false" numberFormat="n4">最大单家非同业单一客户风险暴露占一级资本净额的比例(%)</div>
                    <div field="jzdZdftydkyezzbjebl" name="jzdZdftydkyezzbjebl" width="60" align="right" headerAlign="center" allowSort="false" numberFormat="n4">最大单家非同业单一客户贷款余额占资本净额的比例(%)</div>
                    <div field="jzdZdtydyfxblzyjzbjebl" name="jzdZdtydyfxblzyjzbjebl" width="60" align="right" headerAlign="center"  allowSort="false" numberFormat="n4">最大单家同业单一客户风险暴露占一级资本净额的比例(%)</div>
                    <div field="jzdZdtyjtfxblzyjzbjebl" name="jzdZdtyjtfxblzyjzbjebl" width="60" align="right" headerAlign="center" allowSort="false" numberFormat="n4">最大单家同业集团客户风险暴露占一级资本净额的比例(%)</div>
                </div>
            </div>
            <div header="资本充足情况-财会" headerAlign="center">
                <div property="columns">
                    <div field="zbczqkZbczl" name="zbczqkZbczl" width="60" align="right" headerAlign="center"  allowSort="false" numberFormat="n4">资本充足率(%)</div>
                    <div field="zbczqkYjzbczl" name="zbczqkYjzbczl" width="60" align="right" headerAlign="center" allowSort="false" numberFormat="n4">一级资本充足率(%)</div>
                    <div field="zbczqkHxyjzbczl" name="zbczqkHxyjzbczl" width="60" align="right" headerAlign="center"  allowSort="false" numberFormat="n4">核心一级资本充足率(%)</div>
                    <div field="zbczqkGgl" name="zbczqkGgl" width="60" align="right" headerAlign="center" allowSort="false" numberFormat="n4">杠杆率(%)</div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    mini.parse();
    var url = window.location.search;
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var id = params.id;
    var grid = mini.get("datagrid");
    var userId = '<%=__sessionUser.getUserId()%>';
    var form = new mini.Form("#search_form");

    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize, pageIndex);
    });


    // 查询
    function search(pageSize, pageIndex) {
        var form = new mini.Form("#search_form");
        form.validate();
        if (form.isValid() == false) {
            mini.alert("信息填写有误，请重新填写", "系统也提示");
            return;
        }
        var data = form.getData(true);
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        data['branchId'] = branchId;
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: "/IfsReportDffrjrjgydjcbeeReportController/searchIfsReportDffrjrjgydjcbeeReportPage",
            data: params,
            callback: function (data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }

    function query() {
        search(grid.pageSize, 0);
    }

    function clear() {
        var form = new mini.Form("search_form");
        form.clear();
        search(10, 0);
    }

    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
            search(100, 0);
        });
    });


    //交易日期
    function onDrawDateStart(e) {
        var startDate = e.date;
        var endDate = mini.get("edate").getValue();
        if (CommonUtil.isNull(endDate)) {
            return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }

    function onDrawDateEnd(e) {
        var endDate = e.date;
        var startDate = mini.get("sdate").getValue();
        if (CommonUtil.isNull(startDate)) {
            return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }

    //生成报表
    function create() {
        form.validate();
        if (form.isValid() == false) {
            mini.alert("信息填写有误，请重新填写", "系统提示");
            return;
        }
        var data = form.getData(true);
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: "/IfsReportDffrjrjgydjcbeeReportController/createIfsReportDffrjrjgydjcbee",
            data: params,
            complete: function (data) {
                var text = data.responseText;
                var desc = JSON.parse(text);
                var content = desc.obj.retMsg;
                mini.alert(content);
                search(100, 0);
            }
        });
    }

    //导出
    function exportExcel(){
        var data = form.getData(true);
        var fields = null;
        var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
        fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
        exportHrbExcel(fields);
    }
</script>
</body>
</html>