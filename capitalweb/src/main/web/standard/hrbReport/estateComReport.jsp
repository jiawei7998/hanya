<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
<title>房地产融资风险监测</title>
</head>
<body>
    <fieldset class="mini-fieldset title">
        <div id="search_form" style="width:100%">
            <legend>报表查询</legend>
            <input id="queryDate" name="queryDate" class="mini-datepicker" labelField="true" label="交易日期:" 
                    value="<%=__bizDate%>" labelSyle="text-align:right;" emptyText="请输入"></input>
            <span>
                <a class="mini-button" style="display: none"  id="search_btn" onclick="query()">查询</a>
                <a class="mini-button" style="display: none"  id="clear_btn" onclick="clear()">清空</a>
                <a class="mini-button" style="display: none"  id="create_btn" onclick="createData()">生成</a>
                <a id="export_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
            </span>
        </div>
    </fieldset>
    <div class="mini-fit" style="width:100%;height:100%;">
        <div id="estate_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
            sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
            border="true" allowResize="true">
            <div property="columns">
                <div type="indexcolumn" headerAlign="center" align="center" width="30px">序号</div>
                <div field="xm" width="200px" allowSort="false" headerAlign="center" align="left">项目</div>
                <div field="bq" width="100px" allowSort="false" headerAlign="center" align="center" numberFormat="#,0.00">本期(元)</div>
                <div field="sntq" width="100px" allowSort="false" headerAlign="center" align="center" numberFormat="#,0.00">上年同期(元)</div>
                <div field="dyxffje" width="100px" allowSort="false" headerAlign="center" align="center" numberFormat="#,0.00">当月新发放金额</div>
                <div field="dyhsje" width="100px" allowSort="false" headerAlign="center" align="center" numberFormat="#,0.00">当月收回金额</div>
                <div field="zcdk" width="100px" allowSort="false" headerAlign="center" align="center" numberFormat="#,0.00">正常贷款</div>
                <div field="zcl" width="100px" allowSort="false" headerAlign="center" align="center" numberFormat="#,0.00">正常类</div>
                <div field="gzl" width="100px" allowSort="false" headerAlign="center" align="center" numberFormat="#,0.00">关注类</div>
                <div field="bldk" width="100px" allowSort="false" headerAlign="center" align="center" numberFormat="#,0.00">不良贷款</div>
                <div field="cjl" width="100px" allowSort="false" headerAlign="center" align="center" numberFormat="#,0.00">次级类</div>
                <div field="kyl" width="100px" allowSort="false" headerAlign="center" align="center" numberFormat="#,0.00">可疑类</div>
                <div field="ssl" width="100px" allowSort="false" headerAlign="center" align="center" numberFormat="#,0.00">损失类</div>
                <div field="zq" width="100px" allowSort="false" headerAlign="center" align="center" numberFormat="#,0.00">展期</div>
                <div field="yqye" width="100px" allowSort="false" headerAlign="center" align="center" numberFormat="#,0.00">逾期余额</div>
                <div field="yq1" width="100px" allowSort="false" headerAlign="center" align="center" numberFormat="#,0.00">逾期1-30天</div>
                <div field="yq31" width="100px" allowSort="false" headerAlign="center" align="center" numberFormat="#,0.00">逾期31-60天</div>
                <div field="yq61" width="100px" allowSort="false" headerAlign="center" align="center" numberFormat="#,0.00">逾期61-90天</div>
                <div field="yq91" width="100px" allowSort="false" headerAlign="center" align="center" numberFormat="#,0.00">逾期91-180天</div>
                <div field="yq181" width="100px" allowSort="false" headerAlign="center" align="center" numberFormat="#,0.00">逾期181-360天 </div>
                <div field="yq361" width="100px" allowSort="false" headerAlign="center" align="center" numberFormat="#,0.00">逾期361天以上</div>
            </div>
        </div>
    </div>
    <script>
        mini.parse();

        var currTab = top["win"].tabs.getActiveTab();
        var params = currTab.params;
        var id = params.id;

        var grid = mini.get("estate_grid");
        var form = new mini.Form("#search_form");
        
        grid.on("beforeload", function (e) {
            e.cancel = true;
            var pageIndex = e.data.pageIndex; 
            var pageSize = e.data.pageSize;
            search(pageSize,pageIndex);
        });
        
        $(document).ready(function() {
            //控制按钮显示
            $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
                query();
            });
        });
    
        //查询按钮
        function query(){
            search(100,0);
        }

        function search(pageSize, pageIndex) {
            var searchUrl="/EstateComSecController/selectAllReport";
             
            var data = form.getData(true);
            data['pageNum'] = pageIndex + 1;
            data['pageSize'] = pageSize;
    
            var params = mini.encode(data);
     
            CommonUtil.ajax({
                url:searchUrl,
                data:params,
                callback:function(data){
                    grid.setTotalCount(data.obj.total);
                    grid.setPageIndex(pageIndex);
                    grid.setPageSize(pageSize);
                    grid.setData(data.obj.rows);
                }
            });
        }
        
        //清空按钮
        function clear() {
            form.clear();
            query();
        }

        function createData(){
            mini.confirm("确认生成？", "系统提示", function (value) {
                if (value == "ok") {
                    form.validate();
                    if (form.isValid() == false) {
                        mini.alert("信息填写有误，请重新填写", "系统提示");
                        return;
                    }
                    var data1 = form.getData(true);
                    var params = mini.encode(data1);
                    CommonUtil.ajax({
                        url : "/EstateComSecController/createEstate",
                        data : params,
                        complete : function(data) {
                            var text = data.responseText;
                            var desc = JSON.parse(text);
                            // var content = desc.obj.retMsg;
                            // mini.alert(content);
                            search(100,0);
                        }
                    });
                }
            });
        }

        function exportExcel(){
            var data = form.getData(true);
            var fields = null;
            var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
            fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
            exportHrbExcel(fields);
        }
        
    </script>
</body>
</html>
