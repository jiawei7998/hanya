<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../global.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>母行同业负债情况周报表</title>
</head>
<body>
<fieldset class="mini-fieldset title">
    <div id="search_form" style="width:100%">
        <legend>报表查询</legend>
        <input id="queryDate" name="queryDate" field="queryDate" class="mini-datepicker" labelField="true"
               value="<%=__bizDate%>" label="日期："
               required="true" labelStyle="text-align:center;" allowinput="false" labelStyle="width:100px" width="280px"
               onvaluechanged="search(10,0)"/>
        <span>
                <a class="mini-button" style="display: none" id="search_btn" onclick="query()">查询</a>
                <a class="mini-button" style="display: none" id="clear_btn" onclick="clear()">清空</a>
                <a id="create_btn" class="mini-button" style="display: none" onclick="getDate()">生成数据</a>
                <a id="export_btn" class="mini-button" style="display: none" onclick="exportExcel()">导出报表</a>
            </span>
    </div>
</fieldset>
<span style="float:right;">单位:亿元</span>
<div class="mini-fit" style="width:100%;height:100%;">
    <div id="loan_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
         sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
         border="true" allowResize="true">
        <div property="columns">
            <div field="PRODUCT" width="500" headerAlign="center" align="center">部门</div>

            <div header="实际数" headerAlign="center" align="center">
                <div property="columns">
                    <div field="AMT" headerAlign="center" align="center" width="120px">年初数</div>
                    <div field="TENOR_1M" headerAlign="center" align="center" width="120px">1月末</div>
                    <div field="TENOR_2M" headerAlign="center" align="center" width="120px">2月末</div>
                    <div field="TENOR_3M" headerAlign="center" align="center" width="120px">3月末</div>
                    <div field="TENOR_4M" headerAlign="center" align="center" width="120px">4月末</div>
                    <div field="TENOR_5M" headerAlign="center" align="center" width="120px">5月末</div>
                    <div field="TENOR_6M" headerAlign="center" align="center" width="120px">6月末</div>
                    <div field="TENOR_7M" headerAlign="center" align="center" width="120px">7月末</div>
                    <div field="TENOR_8M" headerAlign="center" align="center" width="120px">8月末</div>
                    <div field="TENOR_9M" headerAlign="center" align="center" width="120px">9月末</div>
                    <div field="TENOR_10M" headerAlign="center" align="center" width="120px">10月末</div>
                    <div field="TENOR_11M" headerAlign="center" align="center" width="120px">11月末</div>
                    <div field="TENOR_12M" headerAlign="center" align="center" width="120px">12月末</div>
                </div>
            </div>

            <div header="下周（本周四-下周三）预计情况" headerAlign="center" align="center">
                <div property="columns">
                    <div field="BZSYE" headerAlign="center" align="center" width="120px">本周三余额</div>
                    <div field="XZSYE" headerAlign="center" align="center" width="120px">下周三余额</div>
                    <div field="XZJZ" headerAlign="center" align="center" width="120px">下周净增</div>
                </div>
            </div>

        </div>
    </div>
</div>
<script>

    mini.parse();

    var grid = mini.get("loan_grid");
    var form = new mini.Form("#search_form");

    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize, pageIndex);
    });

    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
            query();
        });
    });

    //查询按钮
    function query() {
        search(20, 0);
    }

    function search(pageSize, pageIndex) {
        var searchUrl = "/CapitalTurnoverController/search";

        var data = form.getData(true);
        data['pageNum'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        data['FZ'] = '1';
        var params = mini.encode(data);

        CommonUtil.ajax({
            url: searchUrl,
            data: params,
            callback: function (data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }

    //清空按钮
    function clear() {
        form.clear();
        query();
    }

    function getDate() {
        var data = form.getData(true);
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: "/CapitalTurnoverController/call",
            data: params,
            callback: function (data) {
                if (data.code == 'error.common.0000') {
                    mini.alert("生成成功", "系统提示", function () {
                        search(20, 0);
                    });
                } else {
                    mini.alert("生成失败", "系统提示");
                }
            }
        });
    }


    //  //导出
    function exportExcel() {
        var content = grid.getData();
        if (content.length == 0) {
            mini.alert("请先查询数据");
            return;
        }


        mini.confirm("您确认要导出Excel吗?", "系统提示",
            function (action) {
                if (action == "ok") {
                    var data = form.getData(true);
                    var fields = null;

                    var fields = "<input type='hidden' id='dealNo' name='dealNo' value='" + data.dealNo + "'>";
                    fields += "<input type='hidden' id='queryDate' name='queryDate' value='" + data.queryDate + "'>";
                    fields += "<input type='hidden' id='excelName' name='excelName' value='zjyw.xls'>";
                    fields += "<input type='hidden' id='id' name='id' value='zjyw'>";
                    var urls = CommonUtil.pPath + "/sl/HrbReportManageController/exportExcel";
                    $('<form action="' + urls + '" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();
                }
            }
        );
    }


</script>
</body>
</html>
