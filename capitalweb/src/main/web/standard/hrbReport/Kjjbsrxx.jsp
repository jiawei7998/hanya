<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
    <title>人民币购售业务报送信息-20210520</title>
</head>
<body>
    <fieldset class="mini-fieldset title">
        <div id="search_form" style="width:100%">
            <legend>报表查询</legend>
            <input id="queryDate" name="queryDate" class="mini-datepicker" labelField="true" label="交易日期:"
                    labelSyle="text-align:right;" emptyText="请输入" format="yyyy-MM-dd" value="<%=__bizDate%>"></input>
                <a class="mini-button" style="display: none"  id="search_btn" onclick="search()">查询</a>
<%--                <a class="mini-button" style="display: none"  id="clear_btn" onclick="clear()">清空</a>--%>
                <a id="export_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
            </span>
        </div>
    </fieldset>
    <span style="float:right;">单位:万元</span>
    <div class="mini-fit" style="width:100%;height:100%;">
        
        <div id="fund_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
             allowAlternating="true" border="true" allowResize="true" showPager="false" >
            <div property="columns" autoEscape="true">
               <div field="sbhm" headerAlign="center" align="left" width="100px">申报号码</div>
               <div field="czlx" headerAlign="center" align="left" width="100px">操作类型</div>
               <div field="bgyy" headerAlign="center" align="left" width="100px">变更/撤销原因</div>
               <div field="yhjgdm" headerAlign="center" align="left" width="100px">银行机构代码</div>
               <div field="skrmc" headerAlign="center" align="left" width="100px">收款人名称</div>
               <div field="skrsx" headerAlign="center" align="left" width="100px">收款人属性</div>
               <div field="skrjgdm" headerAlign="center" align="left" width="100px">收款人机构代码或身份证件号码</div>
               <div field="skrmc2" headerAlign="center" align="left" width="100px">付款人名称</div>
               <div field="skhhh" headerAlign="center" align="left" width="100px">付款行行号</div>
               <div field="zje" headerAlign="center" align="left" width="100px">总金额</div>
               <div field="skrq" headerAlign="center" align="left" width="100px">收款日期</div>
               <div field="jsfs" headerAlign="center" align="left" width="100px">结算方式</div>
               <div field="skbz" headerAlign="center" align="left" width="100px">收款币种</div>
                <div field=yhbh" headerAlign="center" align="left" width="100px">银行业务编号</div>

            </div>
        </div>
    </div>
    <script>
        mini.parse();
        var url = window.location.search;
        var currTab = top["win"].tabs.getActiveTab();
        var params = currTab.params;
        var id = params.id;
        var implClass = params.implClass;
        var grid = mini.get("fund_grid");
        var form = new mini.Form("#search_form");

        grid.on("beforeload", function (e) {
            e.cancel = true;
            var pageIndex = e.data.pageIndex;
            var pageSize = e.data.pageSize;
            search(pageSize,pageIndex);
        });

        $(document).ready(function() {
            //控制按钮显示
            $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
                search(10,0);
            });
        });

        //查询按钮
        function search(pageSize, pageIndex)  {
            var searchUrl="/KjjbsrxxController/callKjjbsrxx";
            var data = form.getData(true);
            var params = mini.encode(data);
            CommonUtil.ajax({
                url:searchUrl,
                data:params,
                callback:function(data){
                    grid.setTotalCount(data.obj.total);
                    grid.setPageIndex(pageIndex);
                    grid.setPageSize(pageSize);
                    grid.setData(data.obj.rows);
                }
            });
        }

        function exportExcel(){
            var data = form.getData(true);
            var fields = null;
            var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
            fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
            exportHrbExcel(fields);
        }

    </script>
</body>
</html>
