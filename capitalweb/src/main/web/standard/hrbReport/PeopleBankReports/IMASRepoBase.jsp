<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>买入返售及卖出回购基础信息表</title>
</head>
<body>
<fieldset class="mini-fieldset title">
    <div id="search_form" style="width:100%">
        <legend>报表查询</legend>
        <input id="queryDate" name="queryDate" class="mini-datepicker" labelField="true" label="交易日期:"
               value="<%=__bizDate%>" labelSyle="text-align:right;" emptyText="请输入"></input>
        <span>
            <a class="mini-button" style="display: none"  id="search_btn" onclick="query()">查询</a>
            <a class="mini-button" style="display: none"  id="clear_btn" onclick="clear()">清空</a>
            <a id="export_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
        </span>
    </div>
</fieldset>
<div class="mini-fit" style="width:100%;height:100%;">
    <div id="data_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
         sortMode="client" allowAlternating="true"
         border="true" allowResize="true">
        <div property="columns">
            <div type="indexcolumn" headerAlign="center" align="center" width="30px">序号</div>
            <div field="postdate"  headerAlign="center" align="center" width="50px">数据日期</div>
            <div field="instID"  headerAlign="center" align="center" width="50px">内部机构号</div>
            <div field="cno"  headerAlign="center" align="center" width="50px">客户号</div>
            <div field="sic"  headerAlign="center" align="center" width="50px">金融机构类型代码</div>
            <div field="dealno"  headerAlign="center" align="center" width="50px">业务编码</div>
            <div field="prdType"  headerAlign="center" align="center" width="50px">回购业务类型</div>
            <div field="subType"  headerAlign="center" align="center" width="50px">标的物类型</div>
            <div field="vdate"  headerAlign="center" align="center" width="50px">起始日期</div>
            <div field="mdate"  headerAlign="center" align="center" width="50px">到期日期</div>
            <div field="actMatdate"  headerAlign="center" align="center" width="50px">实际终止日期</div>
            <div field="tenor"  headerAlign="center" align="center" width="50px">回购期限类型</div>
            <div field="rateCode"  headerAlign="center" align="center" width="50px">借贷定价基准类型</div>
            <div field="rateType"  headerAlign="center" align="center" width="50px">利率类型</div>
            <div field="repoRate"  headerAlign="center" align="center" width="50px">实际利率</div>
            <div field="baseRate"  headerAlign="center" align="center" width="50px">基准利率</div>
            <div field="intPayCycl"  headerAlign="center" align="center" width="50px">计息方式</div>
            <div field="intCycl"  headerAlign="center" align="center" width="50px">利率浮动频率</div>
        </div>
    </div>
</div>
<script>

    mini.parse();

    var grid = mini.get("data_grid");
    var form = new mini.Form("#search_form");

    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize,pageIndex);
    });

    $(document).ready(function() {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
            query();
        });
    });

    //查询按钮
    function query(){
        search(10,0);
    }

    function search(pageSize, pageIndex) {
        var searchUrl="//";

        var data = form.getData(true);
        data['pageNum'] = pageIndex + 1;
        data['pageSize'] = pageSize;

        var params = mini.encode(data);

        CommonUtil.ajax({
            url:searchUrl,
            data:params,
            callback:function(data){
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }

    //清空按钮
    function clear() {
        form.clear();
        query();
    }

    function exportExcel(){
        var content = grid.getData();
        if(content.length == 0){
            mini.alert("请先查询数据");
            return;
        }

        mini.confirm("您确认要导出Excel吗?","系统提示",
            function (action) {
                if (action == "ok"){
                    var data = form.getData(true);
                    var fields = null;

                    var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
                    fields += "<input type='hidden' id='excelName' name='excelName' value='.xls'>";
                    fields += "<input type='hidden' id='id' name='id' value=''>";
                    var urls = CommonUtil.pPath + "/sl/HrbReportManageController/exportExcel";
                    $('<form action="'+ urls +'" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();
                }
            }
        );
    }

</script>
</body>
</html>
