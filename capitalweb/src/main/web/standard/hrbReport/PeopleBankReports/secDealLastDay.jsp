<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>存量债券投资信息</title>
</head>
<body>
<fieldset class="mini-fieldset title">
    <div id="search_form" style="width:100%">
        <legend>报表查询</legend>
        <input id="queryDate" name="queryDate" class="mini-datepicker" labelField="true" label="交易日期:"
               value="<%=__bizDate%>" labelSyle="text-align:right;" emptyText="请输入"></input>
        <span>
            <a class="mini-button" style="display: none"  id="search_btn" onclick="query()">查询</a>
            <a class="mini-button" style="display: none"  id="clear_btn" onclick="clear()">清空</a>
            <a id="export_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
        </span>
    </div>
</fieldset>
<div class="mini-fit" style="width:100%;height:100%;">
    <div id="data_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
         sortMode="client" allowAlternating="true"
         border="true" allowResize="true">
        <div property="columns">
            <div type="indexcolumn" headerAlign="center" align="center" width="30px">序号</div>
            <div field="finaInstID"  headerAlign="center" align="center" width="50px">金融机构代码</div>
            <div field="instID"  headerAlign="center" align="center" width="50px">内部机构号</div>
            <div field="secid"  headerAlign="center" align="center" width="50px">债券代码</div>
            <div field="custodianCode"  headerAlign="center" align="center" width="50px">债券总托管机构</div>
            <div field="varietyCode"  headerAlign="center" align="center" width="50px">债券品种</div>
            <div field="bondLevel"  headerAlign="center" align="center" width="50px">债券信用级别</div>
            <div field="CCY"  headerAlign="center" align="center" width="50px">币种</div>
            <div field="prinamt"  headerAlign="center" align="center" width="50px">债券余额</div>
            <div field="CNYAMT"  headerAlign="center" align="center" width="50px">债券余额折人名币</div>
            <div field="rightRegisDate"  headerAlign="center" align="center" width="50px">债权债务登记日</div>
            <div field="vdate"  headerAlign="center" align="center" width="50px">起息日</div>
            <div field="mdate"  headerAlign="center" align="center" width="50px">兑付日期</div>
            <div field="coupRate"  headerAlign="center" align="center" width="50px">票面利率</div>
            <div field="maincorpid"  headerAlign="center" align="center" width="50px">发行人证件代码</div>
            <div field="location"  headerAlign="center" align="center" width="50px">发行人地区代码</div>
            <div field="industry"  headerAlign="center" align="center" width="50px">发行人行业</div>
            <div field="issSize"  headerAlign="center" align="center" width="50px">发行人企业规模</div>
            <div field="ecotype"  headerAlign="center" align="center" width="50px">发行人经济成分</div>
            <div field="acctngtype"  headerAlign="center" align="center" width="50px">发行人国民经济部门</div>
        </div>
    </div>
</div>
<script>

    mini.parse();

    var grid = mini.get("data_grid");
    var form = new mini.Form("#search_form");

    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize,pageIndex);
    });

    $(document).ready(function() {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
            query();
        });
    });

    //查询按钮
    function query(){
        search(10,0);
    }

    function search(pageSize, pageIndex) {
        var searchUrl="//";

        var data = form.getData(true);
        data['pageNum'] = pageIndex + 1;
        data['pageSize'] = pageSize;

        var params = mini.encode(data);

        CommonUtil.ajax({
            url:searchUrl,
            data:params,
            callback:function(data){
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }

    //清空按钮
    function clear() {
        form.clear();
        query();
    }

    function exportExcel(){
        var content = grid.getData();
        if(content.length == 0){
            mini.alert("请先查询数据");
            return;
        }

        mini.confirm("您确认要导出Excel吗?","系统提示",
            function (action) {
                if (action == "ok"){
                    var data = form.getData(true);
                    var fields = null;

                    var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
                    fields += "<input type='hidden' id='excelName' name='excelName' value='.xls'>";
                    fields += "<input type='hidden' id='id' name='id' value=''>";
                    var urls = CommonUtil.pPath + "/sl/HrbReportManageController/exportExcel";
                    $('<form action="'+ urls +'" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();
                }
            }
        );
    }

</script>
</body>
</html>
