<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../../global.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
    <title>人行利率报备</title>
</head>
<body>
<fieldset class="mini-fieldset title">
    <div id="search_form" style="width:100%">
        <legend>报表查询</legend>
        <input id="queryDate" name="queryDate" class="mini-datepicker" labelField="true" label="交易日期:"
               labelSyle="text-align:right;" emptyText="请输入" value="<%=__bizDate%>"/>
        <input id="type" name="type" class="mini-combobox" width="320px"
               data="CommonUtil.serverData.dictionary.peopleBankType" emptyText="请选择报表" labelField="true"
               label="报表类型："
               labelStyle="text-align:right;" onvaluechanged="showDataGrid" value="1"/>
        <a class="mini-button" style="display: none" id="search_btn" onclick="showDataGrid()">查询</a>
        <%--                <a class="mini-button" style="display: none"  id="clear_btn" onclick="clear()">清空</a>--%>
        <a id="export_btn" class="mini-button" style="display: none" onclick="exportExcel()">导出报表</a>
        </span>
    </div>
</fieldset>
<span style="float:right;">单位:元</span>
<div class="mini-fit" style="width:100%;height:100%;" id="parent">
    <div id="dataGrid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
         allowAlternating="true" border="true" allowResize="true">
    </div>
</div>

<script>
    mini.parse();
    var url = window.location.search;
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var id = params.id;
    var implClass = params.implClass;
    debugger
    var dataGrid = mini.get("dataGrid");
    var form = new mini.Form("#search_form");
    var pageIndex;
    var pageSize;
    var columns1 = [
        {field: "dataDate", width: 120, headerAlign: "center", header: '数据日期', align: "center"},
        {field: "cno", width: 120, headerAlign: "center", header: '客户号', align: "center"},
        {field: "insideCode", width: 120, headerAlign: "center", header: '内部机构号', align: "center"},
        {field: "cctDesc", width: 120, headerAlign: "center", header: '国民经济部门分类', align: "center"},
        {field: "sic", width: 120, headerAlign: "center", header: '金融机构代码', align: "center"},
        {field: "comscale", width: 120, headerAlign: "center", header: '企业规模', align: "center"},
        {field: "holeType", width: 120, headerAlign: "center", header: '控股类型', align: "center"},
        {field: "flag", width: 120, headerAlign: "center", header: '境内境外标志', align: "center"},
        {field: "cliType", width: 120, headerAlign: "center", header: '经营所在地行政区划代码', align: "center"},
        {field: "regPlace", width: 120, headerAlign: "center", header: '注册地址', align: "center"},
        {field: "loanAmt", width: 120, headerAlign: "center", header: '授信额度', align: "center"},
        {field: "frozenAmt", width: 120, headerAlign: "center", header: '已用额度', align: "center"},
        {field: "indType", width: 120, headerAlign: "center", header: '所属行业', align: "center"},
        {field: "ruciFlag", width: 120, headerAlign: "center", header: '农村城市标志', align: "center"},
    ];
    var columns2 = [
        {field: "dataDate", width: 120, headerAlign: "center", header: '数据日期', align: "center"},
        {field: "insideCode", width: 120, headerAlign: "center", header: '内部机构号', align: "center"},
        {field: "cno", width: 120, headerAlign: "center", header: '客户号', align: "center"},
        {field: "sic", width: 120, headerAlign: "center", header: '金融机构代码', align: "center"},
        {field: "depositAccountCode", width: 120, headerAlign: "center", header: '存款账户编码', align: "center"},
        {field: "prodType", width: 120, headerAlign: "center", header: '存放业务类型', align: "center"},
        {field: "vDate", width: 120, headerAlign: "center", header: '起始日期', align: "center"},
        {field: "mDate", width: 120, headerAlign: "center", header: '到期日期', align: "center"},
        {field: "depositTimeType", width: 120, headerAlign: "center", header: '存款期限类型', align: "center"},
        {field: "pricingStandardType", width: 120, headerAlign: "center", header: '定价基准类型', align: "center"},
        {field: "rateCode", width: 120, headerAlign: "center", header: '利率类型', align: "center"},
        {field: "intRate", width: 120, headerAlign: "center", header: '实际利率', align: "center"},
        {field: "standardRate", width: 120, headerAlign: "center", header: '基准利率', align: "center"},
        {field: "rateFloatFrequency", width: 120, headerAlign: "center", header: '利率浮动频率', align: "center"},
    ];
    var columns3 = [
        {field: "dataDate", width: 120, headerAlign: "center", header: '数据日期', align: "center"},
        {field: "depositAccountCode", width: 120, headerAlign: "center", header: '存款账户编码', align: "center"},
        {field: "insideCode", width: 120, headerAlign: "center", header: '内部机构号', align: "center"},
        {field: "cno", width: 120, headerAlign: "center", header: '客户号', align: "center"},
        {field: "ccy", width: 120, headerAlign: "center", header: '币种', align: "center"},
        {field: "amt", width: 120, headerAlign: "center", header: '余额', align: "center"},
    ];
    var columns4 = [
        {field: "dataDate", width: 120, headerAlign: "center", header: '数据日期', align: "center"},
        {field: "depositAccountCode", width: 120, headerAlign: "center", header: '存款账户编码', align: "center"},
        {field: "insideCode", width: 120, headerAlign: "center", header: '内部机构号', align: "center"},
        {field: "cno", width: 120, headerAlign: "center", header: '客户号', align: "center"},
        {field: "ticketId", width: 120, headerAlign: "center", header: '交易流水号', align: "center"},
        {field: "postDate", width: 120, headerAlign: "center", header: '交易日期', align: "center"},
        {field: "ccy", width: 120, headerAlign: "center", header: '币种', align: "center"},
        {field: "rate", width: 120, headerAlign: "center", header: '实际利率', align: "center"},
        {field: "standardRate", width: 120, headerAlign: "center", header: '基准利率', align: "center"},
        {field: "amount", width: 120, headerAlign: "center", header: '发生金额', align: "center"},
        {field: "dir", width: 120, headerAlign: "center", header: '交易方向', align: "center"},

    ];
    var columns5 = [
        {field: "dataDate", width: 120, headerAlign: "center", header: '数据日期', align: "center"},
        {field: "insideCode", width: 120, headerAlign: "center", header: '内部机构号', align: "center"},
        {field: "cno", width: 120, headerAlign: "center", header: '客户号', align: "center"},
        {field: "sic", width: 120, headerAlign: "center", header: '金融机构代码', align: "center"},
        {field: "dealNo", width: 120, headerAlign: "center", header: '业务编码', align: "center"},
        {field: "prodType", width: 120, headerAlign: "center", header: '借贷业务类型', align: "center"},
        {field: "vDate", width: 120, headerAlign: "center", header: '起始日期', align: "center"},
        {field: "mDate", width: 120, headerAlign: "center", header: '到期日期', align: "center"},
        {field: "actualEndDate", width: 120, headerAlign: "center", header: '实际终止日期', align: "center"},
        {field: "depositTimeType", width: 120, headerAlign: "center", header: '同业借贷期限类型', align: "center"},
        {field: "rateCode", width: 120, headerAlign: "center", header: '利率类型', align: "center"},
        {field: "intRate", width: 120, headerAlign: "center", header: '实际利率', align: "center"},
        {field: "pricingStandardType", width: 120, headerAlign: "center", header: '借贷定价基准类型', align: "center"},
        {field: "standardRate", width: 120, headerAlign: "center", header: '基准利率', align: "center"},
        {field: "schedulType", width: 120, headerAlign: "center", header: '计息方式', align: "center"},
        {field: "rateFloatFrequency", width: 120, headerAlign: "center", header: '利率浮动频率', align: "center"}
    ];
    var columns6 = [
        {field: "dataDate", width: 120, headerAlign: "center", header: '数据日期', align: "center"},
        {field: "dealNo", width: 120, headerAlign: "center", header: '业务编码', align: "center"},
        {field: "insideCode", width: 120, headerAlign: "center", header: '内部机构号', align: "center"},
        {field: "cno", width: 120, headerAlign: "center", header: '客户号', align: "center"},
        {field: "ccy", width: 120, headerAlign: "center", header: '币种', align: "center"},
        {field: "amt", width: 120, headerAlign: "center", header: '余额', align: "center"},
    ];
    var columns7 = [
        {field: "dataDate", width: 120, headerAlign: "center", header: '数据日期', align: "center"},
        {field: "dealNo", width: 120, headerAlign: "center", header: '业务编码', align: "center"},
        {field: "insideCode", width: 120, headerAlign: "center", header: '内部机构号', align: "center"},
        {field: "cno", width: 120, headerAlign: "center", header: '客户号', align: "center"},
        {field: "ticketId", width: 120, headerAlign: "center", header: '交易流水号', align: "center"},
        {field: "postDate", width: 120, headerAlign: "center", header: '交易日期', align: "center"},
        {field: "ccy", width: 120, headerAlign: "center", header: '币种', align: "center"},
        {field: "rate", width: 120, headerAlign: "center", header: '实际利率', align: "center"},
        {field: "standardRate", width: 120, headerAlign: "center", header: '基准利率', align: "center"},
        {field: "amount", width: 120, headerAlign: "center", header: '发生金额', align: "center"},
        {field: "dir", width: 120, headerAlign: "center", header: '交易方向', align: "center"},

    ];
    var columns8 = [
        {field: "dataDate", width: 120, headerAlign: "center", header: '数据日期', align: "center"},
        {field: "insideCode", width: 120, headerAlign: "center", header: '内部机构号', align: "center"},
        {field: "cno", width: 120, headerAlign: "center", header: '客户号', align: "center"},
        {field: "sic", width: 120, headerAlign: "center", header: '金融机构代码', align: "center"},
        {field: "dealNo", width: 120, headerAlign: "center", header: '业务编码', align: "center"},
        {field: "prodType", width: 120, headerAlign: "center", header: '回购业务类型', align: "center"},
        {field: "secType", width: 120, headerAlign: "center", header: '标的物类型', align: "center"},
        {field: "vDate", width: 120, headerAlign: "center", header: '起始日期', align: "center"},
        {field: "matDate", width: 120, headerAlign: "center", header: '到期日期', align: "center"},
        {field: "actualEndDate", width: 120, headerAlign: "center", header: '实际终止日期', align: "center"},
        {field: "tenor", width: 120, headerAlign: "center", header: '回购期限类型', align: "center"},
        {field: "pricingStandardType", width: 120, headerAlign: "center", header: '定价基准类型', align: "center"},
        {field: "rateCode", width: 120, headerAlign: "center", header: '利率类型', align: "center"},
        {field: "rate", width: 120, headerAlign: "center", header: '实际利率', align: "center"},
        {field: "standardRate", width: 120, headerAlign: "center", header: '基准利率', align: "center"},
        {field: "schedulType", width: 120, headerAlign: "center", header: '计息方式', align: "center"},
        {field: "rateFloatFrequency", width: 120, headerAlign: "center", header: '利率浮动频率', align: "center"}
    ];
    var columns9 = [
        {field: "dataDate", width: 120, headerAlign: "center", header: '数据日期', align: "center"},
        {field: "dealNo", width: 120, headerAlign: "center", header: '业务编码', align: "center"},
        {field: "insideCode", width: 120, headerAlign: "center", header: '内部机构号', align: "center"},
        {field: "cno", width: 120, headerAlign: "center", header: '客户号', align: "center"},
        {field: "ccy", width: 120, headerAlign: "center", header: '币种', align: "center"},
        {field: "amt", width: 120, headerAlign: "center", header: '余额', align: "center"},
    ];
    var columns10 = [
        {field: "dataDate", width: 120, headerAlign: "center", header: '数据日期', align: "center"},
        {field: "dealNo", width: 120, headerAlign: "center", header: '业务编码', align: "center"},
        {field: "insideCode", width: 120, headerAlign: "center", header: '内部机构号', align: "center"},
        {field: "cno", width: 120, headerAlign: "center", header: '客户号', align: "center"},
        {field: "dealNo", width: 120, headerAlign: "center", header: '交易流水号', align: "center"},
        {field: "ccy", width: 120, headerAlign: "center", header: '币种', align: "center"},
        {field: "postDate", width: 120, headerAlign: "center", header: '交易日期', align: "center"},
        {field: "amount", width: 120, headerAlign: "center", header: '发生金额', align: "center"},
        {field: "rate", width: 120, headerAlign: "center", header: '实际利率', align: "center"},
        {field: "standardRate", width: 120, headerAlign: "center", header: '基准利率', align: "center"},
        {field: "dir", width: 120, headerAlign: "center", header: '交易方向', align: "center"}
    ];
    var columns = [columns1, columns2, columns3, columns4, columns5, columns6, columns7, columns8, columns9, columns10]

    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
            query(10, 0);
        });
    });

    dataGrid.on("beforeload", function (e) {
        e.cancel = true;
        pageIndex = e.data.pageIndex;
        pageSize = e.data.pageSize;
        query(pageSize, pageIndex);
    });

    //下拉选择需要展示的表格
    function showDataGrid() {
        query(10, 0);
    }

    //查询按钮
    function query(pageSize, pageIndex) {
        var type = mini.get("type").getValue();
        dataGrid.set({
            columns: columns[type - 1]
        });
        var searchUrl = "/ExportDataController/getExportPageData";
        var data = form.getData(true);
        data.implClass = implClass;
        data.type = type;
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: searchUrl,
            data: params,
            callback: function (data) {
                dataGrid.setTotalCount(data.obj.total);
                dataGrid.setPageIndex(pageIndex);
                dataGrid.setPageSize(pageSize);
                dataGrid.setData(data.obj.rows);
            }
        });
    }


    function exportExcel(){
        var type=mini.get("type").getValue();
        var typeText=mini.get("type").getText();
        var data = form.getData(true);
        var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
        fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
        fields += "<input type='hidden' id='type' name='type' value='"+type+"'>";
        fields += "<input type='hidden' id='sheetType' name='sheetType' value='"+type+"'>";
        fields += "<input type='hidden' id='sheetTypeValue' name='sheetTypeValue' value='"+typeText+"'>";
        exportHrbExcel(fields);
    }

</script>
</body>
</html>
