<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
    <title>业务明细</title>
</head>
<body>
    <fieldset class="mini-fieldset title">
        <div id="search_form" style="width:100%">
            <legend>报表查询</legend>
            <input id="queryDate" name="queryDate" class="mini-datepicker" labelField="true" label="交易日期:" 
                    labelSyle="text-align:right;" emptyText="请输入"></input>
            <input id="sheetType" name="sheetType"  class="mini-combobox" data="CommonUtil.serverData.dictionary.DEALDETATYPE" labelField="true"
                label="交易类型:" labelSyle="text-align:right;" emptyText="请输入"></input>
            <span>
                <a class="mini-button" style="display: none"  id="search_btn" onclick="query()">查询</a>
                <a class="mini-button" style="display: none"  id="clear_btn" onclick="clear()">清空</a>
                <a id="export_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
            </span>
        </div>
    </fieldset>
    <div class="mini-fit" style="width:100%;height:100%;">
        
        <div id="pub_fund_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
            sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
            border="true" allowResize="true">
            <div property="columns">
                <div type="indexcolumn" headerAlign="center" align="center" width="30px">序号</div>
                <div field="fundManager" headerAlign="center" align="center" width="150px">基金管理人</div>
                <div field="fundName" headerAlign="center" align="center" width="150px">基金简称</div>
                <div field="fundType" headerAlign="center" align="center" width="150px">基金类型</div>
                <div field="fundCode" headerAlign="center" align="center" width="150px">基金代码</div>
                <div field="dealDir" headerAlign="center" align="center" width="150px">交易方向</div>
                <div field="amt" headerAlign="center" align="center" width="150px" numberFormat="#,0.0000">余额（万元）</div>
                <div field="purchDate" headerAlign="center" align="center" width="150px">申购日</div>
            </div>
        </div>
        <div id="spe_fund_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
             sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
             border="true" allowResize="true">
            <div property="columns">
                <div type="indexcolumn" headerAlign="center" align="center" width="30px">序号</div>
                <div field="fundManager" headerAlign="center" align="center" width="150px">基金管理人</div>
                <div field="fundName" headerAlign="center" align="center" width="150px">基金简称</div>
                <div field="fundCode" headerAlign="center" align="center" width="150px">基金代码</div>
                <div field="dealDir" headerAlign="center" align="center" width="150px">交易方向</div>
                <div field="amt" headerAlign="center" align="center" width="150px" numberFormat="#,0.0000">余额（万元）</div>
                <div field="purchDate" headerAlign="center" align="center" width="150px">申购日</div>
            </div>
        </div>
        <div id="cd_sub_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
             sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
             border="true" allowResize="true">
            <div property="columns">
                <div type="indexcolumn" headerAlign="center" align="center" width="30px">序号</div>
                <div field="cust" headerAlign="center" align="center" width="150px">申购机构</div>
                <div field="mdate" headerAlign="center" align="center" width="150px">到期日</div>
                <div field="vdate" headerAlign="center" align="center" width="150px">起息日</div>
                <div field="amt" headerAlign="center" align="center" width="150px">成交量（亿元）</div>
                <div field="isfund" headerAlign="center" align="center" width="150px">申购机构是否是货币基金</div>
            </div>
        </div>
        <div id="cd_tpos_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
             sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
             border="true" allowResize="true">
            <div property="columns">
                <div type="indexcolumn" headerAlign="center" align="center" width="30px">序号</div>
                <div field="secid" headerAlign="center" align="center" width="150px">债券代码</div>
                <div field="secName" headerAlign="center" align="center" width="150px">债券简称</div>
                <div field="vdate" headerAlign="center" align="center" width="150px">起息日</div>
                <div field="tenor" headerAlign="center" align="center" width="150px">期限</div>
                <div field="mdate" headerAlign="center" align="center" width="150px">到期日</div>
                <div field="secValue" headerAlign="center" align="center" width="150px">账面价值</div>
                <div field="expYield" headerAlign="center" align="center" width="150px">预期收益</div>
            </div>
        </div>
        <div id="rvrepo_spv_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
             sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
             border="true" allowResize="true">
            <div property="columns">
                <div type="indexcolumn" headerAlign="center" align="center" width="30px">序号</div>
                <div field="cust" headerAlign="center" align="center" width="150px">交易对手</div>
                <div field="vdate" headerAlign="center" align="center" width="150px">首期交割日</div>
                <div field="tenor" headerAlign="center" align="center" width="150px">期限</div>
                <div field="mdate" headerAlign="center" align="center" width="150px">到期交割日</div>
                <div field="rate" headerAlign="center" align="center" width="150px">利率（%）</div>
                <div field="amt" headerAlign="center" align="center" width="150px">首期结算额</div>
            </div>
        </div>
        <div id="repo_spv_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
             sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
             border="true" allowResize="true">
            <div property="columns">
                <div type="indexcolumn" headerAlign="center" align="center" width="30px">序号</div>
                <div field="cust" headerAlign="center" align="center" width="150px">交易对手</div>
                <div field="vdate" headerAlign="center" align="center" width="150px">首期交割日</div>
                <div field="tenor" headerAlign="center" align="center" width="150px">期限</div>
                <div field="mdate" headerAlign="center" align="center" width="150px">到期交割日</div>
                <div field="rate" headerAlign="center" align="center" width="150px">利率（%）</div>
                <div field="amt" headerAlign="center" align="center" width="150px">首期结算额</div>
            </div>
        </div>
        <div id="rvrepo_insure_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
             sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
             border="true" allowResize="true">
            <div property="columns">
                <div type="indexcolumn" headerAlign="center" align="center" width="30px">序号</div>
                <div field="cust" headerAlign="center" align="center" width="150px">交易对手</div>
                <div field="vdate" headerAlign="center" align="center" width="150px">首期交割日</div>
                <div field="tenor" headerAlign="center" align="center" width="150px">期限</div>
                <div field="mdate" headerAlign="center" align="center" width="150px">到期交割日</div>
                <div field="rate" headerAlign="center" align="center" width="150px">利率（%）</div>
                <div field="amt" headerAlign="center" align="center" width="150px">首期结算额</div>
            </div>
        </div>
        <div id="repo_insure_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
             sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
             border="true" allowResize="true">
            <div property="columns">
                <div type="indexcolumn" headerAlign="center" align="center" width="30px">序号</div>
                <div field="cust" headerAlign="center" align="center" width="150px">交易对手</div>
                <div field="vdate" headerAlign="center" align="center" width="150px">首期交割日</div>
                <div field="tenor" headerAlign="center" align="center" width="150px">期限</div>
                <div field="mdate" headerAlign="center" align="center" width="150px">到期交割日</div>
                <div field="rate" headerAlign="center" align="center" width="150px">利率（%）</div>
                <div field="amt" headerAlign="center" align="center" width="150px">首期结算额</div>
            </div>
        </div>
        <div id="rvrepo_settle_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
             sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
             border="true" allowResize="true">
            <div property="columns">
                <div type="indexcolumn" headerAlign="center" align="center" width="30px">序号</div>
                <div field="cust" headerAlign="center" align="center" width="150px">交易对手</div>
                <div field="vdate" headerAlign="center" align="center" width="150px">首期交割日</div>
                <div field="tenor" headerAlign="center" align="center" width="150px">期限</div>
                <div field="mdate" headerAlign="center" align="center" width="150px">到期交割日</div>
                <div field="rate" headerAlign="center" align="center" width="150px">利率（%）</div>
                <div field="amt" headerAlign="center" align="center" width="150px">首期结算额</div>
            </div>
        </div>
        <div id="rvrepo_bank_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
             sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
             border="true" allowResize="true">
            <div property="columns">
                <div type="indexcolumn" headerAlign="center" align="center" width="30px">序号</div>
                <div field="cust" headerAlign="center" align="center" width="150px">交易对手</div>
                <div field="vdate" headerAlign="center" align="center" width="150px">首期交割日</div>
                <div field="tenor" headerAlign="center" align="center" width="150px">期限</div>
                <div field="mdate" headerAlign="center" align="center" width="150px">到期交割日</div>
                <div field="rate" headerAlign="center" align="center" width="150px">利率（%）</div>
                <div field="amt" headerAlign="center" align="center" width="150px">首期结算额</div>
            </div>
        </div>
        <div id="repo_bank_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
             sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
             border="true" allowResize="true">
            <div property="columns">
                <div type="indexcolumn" headerAlign="center" align="center" width="30px">序号</div>
                <div field="cust" headerAlign="center" align="center" width="150px">交易对手</div>
                <div field="vdate" headerAlign="center" align="center" width="150px">首期交割日</div>
                <div field="tenor" headerAlign="center" align="center" width="150px">期限</div>
                <div field="mdate" headerAlign="center" align="center" width="150px">到期交割日</div>
                <div field="rate" headerAlign="center" align="center" width="150px">利率（%）</div>
                <div field="amt" headerAlign="center" align="center" width="150px">首期结算额</div>
            </div>
        </div>
        <div id="rvrepo_stock_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
             sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
             border="true" allowResize="true">
            <div property="columns">
                <div type="indexcolumn" headerAlign="center" align="center" width="30px">序号</div>
                <div field="cust" headerAlign="center" align="center" width="150px">交易对手</div>
                <div field="vdate" headerAlign="center" align="center" width="150px">首期交割日</div>
                <div field="tenor" headerAlign="center" align="center" width="150px">期限</div>
                <div field="mdate" headerAlign="center" align="center" width="150px">到期交割日</div>
                <div field="rate" headerAlign="center" align="center" width="150px">利率（%）</div>
                <div field="amt" headerAlign="center" align="center" width="150px">首期结算额</div>
            </div>
        </div>
        <div id="repo_stock_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
             sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
             border="true" allowResize="true">
            <div property="columns">
                <div type="indexcolumn" headerAlign="center" align="center" width="30px">序号</div>
                <div field="cust" headerAlign="center" align="center" width="150px">交易对手</div>
                <div field="vdate" headerAlign="center" align="center" width="150px">首期交割日</div>
                <div field="tenor" headerAlign="center" align="center" width="150px">期限</div>
                <div field="mdate" headerAlign="center" align="center" width="150px">到期交割日</div>
                <div field="rate" headerAlign="center" align="center" width="150px">利率（%）</div>
                <div field="amt" headerAlign="center" align="center" width="150px">首期结算额</div>
            </div>
        </div>
        <div id="repo_centbank_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
             sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
             border="true" allowResize="true">
            <div property="columns">
                <div type="indexcolumn" headerAlign="center" align="center" width="30px">序号</div>
                <div field="cust" headerAlign="center" align="center" width="150px">交易对手</div>
                <div field="vdate" headerAlign="center" align="center" width="150px">首期交割日</div>
                <div field="tenor" headerAlign="center" align="center" width="150px">期限</div>
                <div field="mdate" headerAlign="center" align="center" width="150px">到期交割日</div>
                <div field="rate" headerAlign="center" align="center" width="150px">利率（%）</div>
                <div field="amt" headerAlign="center" align="center" width="150px">首期结算额</div>
            </div>
        </div>
        <div id="money_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
             sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
             border="true" allowResize="true">
            <div property="columns">
                <div type="indexcolumn" headerAlign="center" align="center" width="30px">序号</div>
                <div field="fundManager" headerAlign="center" align="center" width="150px">理财管理人</div>
                <div field="fundName" headerAlign="center" align="center" width="150px">理财全称</div>
                <div field="fundCode" headerAlign="center" align="center" width="150px">理财登记代码</div>
                <div field="dealDir" headerAlign="center" align="center" width="150px">交易方向</div>
                <div field="amt" headerAlign="center" align="center" width="150px">交易余额（万元）</div>
                <div field="purchDate" headerAlign="center" align="center" width="150px">申购日</div>
            </div>
        </div>
    </div>
    <script>
        
        mini.parse();
        var currTab = top["win"].tabs.getActiveTab();
        var params = currTab.params;
        var id = params.id;

        var pubfundgrid         = mini.get("pub_fund_grid");//公墓基金
        var spefundgrid         = mini.get("spe_fund_grid");//专户基金
        var cdsubgrid           = mini.get("cd_sub_grid");//
        var cdtposgrid          = mini.get("cd_tpos_grid");
        var rvrepospvgrid       = mini.get("rvrepo_spv_grid");
        var repospvgrid         = mini.get("repo_spv_grid");
        var rvrepoinsuregrid    = mini.get("rvrepo_insure_grid");
        var repoinsuregrid      = mini.get("repo_insure_grid");
        var rvreposettlegrid    = mini.get("rvrepo_settle_grid");
        var rvrepobankgrid      = mini.get("rvrepo_bank_grid");
        var repobankgrid        = mini.get("repo_bank_grid");
        var rvrepostockgrid     = mini.get("rvrepo_stock_grid");
        var repostockgrid       = mini.get("repo_stock_grid");
        var repocentbankgrid    = mini.get("repo_centbank_grid");
        var moneygrid           = mini.get("money_grid");


        var form = new mini.Form("#search_form");


        pubfundgrid.on("beforeload", function (e) {
            e.cancel = true;
            var pageIndex = e.data.pageIndex;
            var pageSize = e.data.pageSize;
            searchpubfund(pageSize,pageIndex);
        });
        spefundgrid.on("beforeload", function (e) {
            e.cancel = true;
            var pageIndex = e.data.pageIndex;
            var pageSize = e.data.pageSize;
            searchspefund(pageSize,pageIndex);
        });
        cdsubgrid.on("beforeload", function (e) {
            e.cancel = true;
            var pageIndex = e.data.pageIndex;
            var pageSize = e.data.pageSize;
            searchcdsub(pageSize,pageIndex);
        });
        cdtposgrid.on("beforeload", function (e) {
            e.cancel = true;
            var pageIndex = e.data.pageIndex;
            var pageSize = e.data.pageSize;
            searchcdtpos(pageSize,pageIndex);
        });
        rvrepospvgrid.on("beforeload", function (e) {
            e.cancel = true;
            var pageIndex = e.data.pageIndex;
            var pageSize = e.data.pageSize;
            searchrvrepospv(pageSize,pageIndex);
        });
        repospvgrid.on("beforeload", function (e) {
            e.cancel = true;
            var pageIndex = e.data.pageIndex;
            var pageSize = e.data.pageSize;
            searchrepospv(pageSize,pageIndex);
        });
        rvrepoinsuregrid.on("beforeload", function (e) {
            e.cancel = true;
            var pageIndex = e.data.pageIndex;
            var pageSize = e.data.pageSize;
            searchrvrepoinsure(pageSize,pageIndex);
        });
        repoinsuregrid.on("beforeload", function (e) {
            e.cancel = true;
            var pageIndex = e.data.pageIndex;
            var pageSize = e.data.pageSize;
            searchrepoinsure(pageSize,pageIndex);
        });
        rvreposettlegrid.on("beforeload", function (e) {
            e.cancel = true;
            var pageIndex = e.data.pageIndex;
            var pageSize = e.data.pageSize;
            searchrvreposettle(pageSize,pageIndex);
        });
        rvrepobankgrid.on("beforeload", function (e) {
            e.cancel = true;
            var pageIndex = e.data.pageIndex;
            var pageSize = e.data.pageSize;
            searchrvrepobank(pageSize,pageIndex);
        });
        repobankgrid.on("beforeload", function (e) {
            e.cancel = true;
            var pageIndex = e.data.pageIndex;
            var pageSize = e.data.pageSize;
            searchrepobank(pageSize,pageIndex);
        });
        rvrepostockgrid.on("beforeload", function (e) {
            e.cancel = true;
            var pageIndex = e.data.pageIndex;
            var pageSize = e.data.pageSize;
            searchrvrepostock(pageSize,pageIndex);
        });
        repostockgrid.on("beforeload", function (e) {
            e.cancel = true;
            var pageIndex = e.data.pageIndex;
            var pageSize = e.data.pageSize;
            searchrepostock(pageSize,pageIndex);
        });
        repocentbankgrid.on("beforeload", function (e) {
            e.cancel = true;
            var pageIndex = e.data.pageIndex;
            var pageSize = e.data.pageSize;
            searchrepocentbank(pageSize,pageIndex);
        });
        moneygrid.on("beforeload", function (e) {
            e.cancel = true;
            var pageIndex = e.data.pageIndex;
            var pageSize = e.data.pageSize;
            searchmoney(pageSize,pageIndex);
        });
        // sheetgrid.on("beforeload", function (e) {
        //     e.cancel = true;
        //     var pageIndex = e.data.pageIndex;
        //     var pageSize = e.data.pageSize;
        //     queryPage(pageSize,pageIndex);
        // });
        
        $(document).ready (function() {
            //控制按钮显示
            $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
                clear();
                mini.get("queryDate").setValue(sysDate);
                mini.get("sheetType").setValue("pub_fund");
                query();
            });
        });
    
        //查询按钮
        function query(){
            var dealtype = mini.get("sheetType").value;
            if(dealtype == 'pub_fund') {
                searchpubfund(10,0);
            }if(dealtype == 'spe_fund') {
                searchspefund(10,0);
            }if(dealtype == 'cd_sub') {
                searchcdsub(10,0);
            }if(dealtype == 'cd_tpos') {
                searchcdtpos(10,0);
            }if(dealtype == 'rvrepo_spv') {
                searchrvrepospv(10,0);
            }if(dealtype == 'repo_spv') {
                searchrepospv(10,0);
            }if(dealtype == 'rvrepo_insure') {
                searchrvrepoinsure(10,0);
            }if(dealtype == 'repo_insure') {
                searchrepoinsure(10,0);
            }if(dealtype == 'rvrepo_settle') {
                searchrvreposettle(10,0);
            }if(dealtype == 'rvrepo_bank') {
                searchrvrepobank(10,0);
            }if(dealtype == 'repo_bank') {
                searchrepobank(10,0);
            }if(dealtype == 'rvrepo_stock') {
                searchrvrepostock(10,0);
            }if(dealtype == 'repo_stock') {
                searchrepostock(10,0);
            }if(dealtype == 'repo_centbank') {
                searchrepocentbank(10,0);
            }if(dealtype == 'money') {
                searchmoney(10,0);
            }
        }

        //查询
        function searchpubfund(pageSize, pageIndex) {
            var searchUrl="/DealDetailController/searchDetailPage";
            var data = form.getData(true);
            data['pageNumber'] = pageIndex + 1;
            data['pageSize'] = pageSize;
            var params = mini.encode(data);
            CommonUtil.ajax({
                url:searchUrl,
                data:params,
                callback:function(data){
                    pubfundgrid.setTotalCount(data.obj.total);
                    pubfundgrid.setPageIndex(pageIndex);
                    pubfundgrid.setPageSize(pageSize);
                    pubfundgrid.setData(data.obj.rows);
                    hideOther(pubfundgrid);
                }
            });
        }

        function searchspefund(pageSize, pageIndex) {
            var searchUrl="/DealDetailController/searchDetailPage";
            var data = form.getData(true);
            data['pageNumber'] = pageIndex + 1;
            data['pageSize'] = pageSize;
            var params = mini.encode(data);
            CommonUtil.ajax({
                url:searchUrl,
                data:params,
                callback:function(data){
                    spefundgrid.setTotalCount(data.obj.total);
                    spefundgrid.setPageIndex(pageIndex);
                    spefundgrid.setPageSize(pageSize);
                    spefundgrid.setData(data.obj.rows);
                    hideOther(spefundgrid);
                }
            });
        }


        function searchcdsub(pageSize, pageIndex) {
            var searchUrl="/DealDetailController/searchDetailPage";
            var data = form.getData(true);
            data['pageNumber'] = pageIndex + 1;
            data['pageSize'] = pageSize;
            var params = mini.encode(data);
            CommonUtil.ajax({
                url:searchUrl,
                data:params,
                callback:function(data){
                    cdsubgrid.setTotalCount(data.obj.total);
                    cdsubgrid.setPageIndex(pageIndex);
                    cdsubgrid.setPageSize(pageSize);
                    cdsubgrid.setData(data.obj.rows);
                    hideOther(cdsubgrid);
                }
            });
        }

        function searchcdtpos(pageSize, pageIndex) {
            var searchUrl="/DealDetailController/searchDetailPage";
            var data = form.getData(true);
            data['pageNumber'] = pageIndex + 1;
            data['pageSize'] = pageSize;
            var params = mini.encode(data);
            CommonUtil.ajax({
                url:searchUrl,
                data:params,
                callback:function(data){
                    cdtposgrid.setTotalCount(data.obj.total);
                    cdtposgrid.setPageIndex(pageIndex);
                    cdtposgrid.setPageSize(pageSize);
                    cdtposgrid.setData(data.obj.rows);
                    hideOther(cdtposgrid);
                }
            });
        }

        function searchrvrepospv(pageSize, pageIndex) {
            var searchUrl="/DealDetailController/searchDetailPage";
            var data = form.getData(true);
            data['pageNumber'] = pageIndex + 1;
            data['pageSize'] = pageSize;
            var params = mini.encode(data);
            CommonUtil.ajax({
                url:searchUrl,
                data:params,
                callback:function(data){
                    rvrepospvgrid.setTotalCount(data.obj.total);
                    rvrepospvgrid.setPageIndex(pageIndex);
                    rvrepospvgrid.setPageSize(pageSize);
                    rvrepospvgrid.setData(data.obj.rows);
                    hideOther(rvrepospvgrid);
                }
            });
        }

        function searchrepospv(pageSize, pageIndex) {
            var searchUrl="/DealDetailController/searchDetailPage";
            var data = form.getData(true);
            data['pageNumber'] = pageIndex + 1;
            data['pageSize'] = pageSize;
            var params = mini.encode(data);
            CommonUtil.ajax({
                url:searchUrl,
                data:params,
                callback:function(data){
                    repospvgrid.setTotalCount(data.obj.total);
                    repospvgrid.setPageIndex(pageIndex);
                    repospvgrid.setPageSize(pageSize);
                    repospvgrid.setData(data.obj.rows);
                    hideOther(repospvgrid);
                }
            });
        }

        function searchrvrepoinsure(pageSize, pageIndex) {
            var searchUrl="/DealDetailController/searchDetailPage";
            var data = form.getData(true);
            data['pageNumber'] = pageIndex + 1;
            data['pageSize'] = pageSize;
            var params = mini.encode(data);
            CommonUtil.ajax({
                url:searchUrl,
                data:params,
                callback:function(data){
                    rvrepoinsuregrid.setTotalCount(data.obj.total);
                    rvrepoinsuregrid.setPageIndex(pageIndex);
                    rvrepoinsuregrid.setPageSize(pageSize);
                    rvrepoinsuregrid.setData(data.obj.rows);
                    hideOther(rvrepoinsuregrid);
                }
            });
        }

        function searchrepoinsure(pageSize, pageIndex) {
            var searchUrl="/DealDetailController/searchDetailPage";
            var data = form.getData(true);
            data['pageNumber'] = pageIndex + 1;
            data['pageSize'] = pageSize;
            var params = mini.encode(data);
            CommonUtil.ajax({
                url:searchUrl,
                data:params,
                callback:function(data){
                    repoinsuregrid.setTotalCount(data.obj.total);
                    repoinsuregrid.setPageIndex(pageIndex);
                    repoinsuregrid.setPageSize(pageSize);
                    repoinsuregrid.setData(data.obj.rows);
                    hideOther(repoinsuregrid);
                }
            });
        }

        function searchrvreposettle(pageSize, pageIndex) {
            var searchUrl="/DealDetailController/searchDetailPage";
            var data = form.getData(true);
            data['pageNumber'] = pageIndex + 1;
            data['pageSize'] = pageSize;
            var params = mini.encode(data);
            CommonUtil.ajax({
                url:searchUrl,
                data:params,
                callback:function(data){
                    rvreposettlegrid.setTotalCount(data.obj.total);
                    rvreposettlegrid.setPageIndex(pageIndex);
                    rvreposettlegrid.setPageSize(pageSize);
                    rvreposettlegrid.setData(data.obj.rows);
                    hideOther(rvreposettlegrid);
                }
            });
        }

        function searchrvrepobank(pageSize, pageIndex) {
            var searchUrl="/DealDetailController/searchDetailPage";
            var data = form.getData(true);
            data['pageNumber'] = pageIndex + 1;
            data['pageSize'] = pageSize;
            var params = mini.encode(data);
            CommonUtil.ajax({
                url:searchUrl,
                data:params,
                callback:function(data){
                    rvrepobankgrid.setTotalCount(data.obj.total);
                    rvrepobankgrid.setPageIndex(pageIndex);
                    rvrepobankgrid.setPageSize(pageSize);
                    rvrepobankgrid.setData(data.obj.rows);
                    hideOther(rvrepobankgrid);
                }
            });
        }

        function searchrepobank(pageSize, pageIndex) {
            var searchUrl="/DealDetailController/searchDetailPage";
            var data = form.getData(true);
            data['pageNumber'] = pageIndex + 1;
            data['pageSize'] = pageSize;
            var params = mini.encode(data);
            CommonUtil.ajax({
                url:searchUrl,
                data:params,
                callback:function(data){
                    repobankgrid.setTotalCount(data.obj.total);
                    repobankgrid.setPageIndex(pageIndex);
                    repobankgrid.setPageSize(pageSize);
                    repobankgrid.setData(data.obj.rows);
                    hideOther(repobankgrid);
                }
            });
        }

        function searchrvrepostock(pageSize, pageIndex) {
            var searchUrl="/DealDetailController/searchDetailPage";
            var data = form.getData(true);
            data['pageNumber'] = pageIndex + 1;
            data['pageSize'] = pageSize;
            var params = mini.encode(data);
            CommonUtil.ajax({
                url:searchUrl,
                data:params,
                callback:function(data){
                    rvrepostockgrid.setTotalCount(data.obj.total);
                    rvrepostockgrid.setPageIndex(pageIndex);
                    rvrepostockgrid.setPageSize(pageSize);
                    rvrepostockgrid.setData(data.obj.rows);
                    hideOther(rvrepostockgrid);
                }
            });
        }

        function searchrepostock(pageSize, pageIndex) {
            var searchUrl="/DealDetailController/searchDetailPage";
            var data = form.getData(true);
            data['pageNumber'] = pageIndex + 1;
            data['pageSize'] = pageSize;
            var params = mini.encode(data);
            CommonUtil.ajax({
                url:searchUrl,
                data:params,
                callback:function(data){
                    repostockgrid.setTotalCount(data.obj.total);
                    repostockgrid.setPageIndex(pageIndex);
                    repostockgrid.setPageSize(pageSize);
                    repostockgrid.setData(data.obj.rows);
                    hideOther(repostockgrid);
                }
            });
        }

        function searchrepocentbank(pageSize, pageIndex) {
            var searchUrl="/DealDetailController/searchDetailPage";
            var data = form.getData(true);
            data['pageNumber'] = pageIndex + 1;
            data['pageSize'] = pageSize;
            var params = mini.encode(data);
            CommonUtil.ajax({
                url:searchUrl,
                data:params,
                callback:function(data){
                    repocentbankgrid.setTotalCount(data.obj.total);
                    repocentbankgrid.setPageIndex(pageIndex);
                    repocentbankgrid.setPageSize(pageSize);
                    repocentbankgrid.setData(data.obj.rows);
                    hideOther(repocentbankgrid);
                }
            });
        }

        function searchmoney(pageSize, pageIndex) {
            var searchUrl="/DealDetailController/searchDetailPage";
            var data = form.getData(true);
            data['pageNumber'] = pageIndex + 1;
            data['pageSize'] = pageSize;
            var params = mini.encode(data);
            CommonUtil.ajax({
                url:searchUrl,
                data:params,
                callback:function(data){
                    moneygrid.setTotalCount(data.obj.total);
                    moneygrid.setPageIndex(pageIndex);
                    moneygrid.setPageSize(pageSize);
                    moneygrid.setData(data.obj.rows);
                    hideOther(moneygrid);
                }
            });
        }


        //传入gird，隐藏其他
        function hideOther(grid){
            pubfundgrid.hide();
            spefundgrid.hide();
            cdsubgrid.hide();
            cdtposgrid.hide();
            rvrepospvgrid.hide();
            repospvgrid.hide();
            rvrepoinsuregrid.hide();
            repoinsuregrid.hide();
            rvreposettlegrid.hide();
            rvrepobankgrid.hide();
            repobankgrid.hide();
            rvrepostockgrid.hide();
            repostockgrid.hide();
            repocentbankgrid.hide();
            moneygrid.hide();
            grid.show();
        }

        //清空按钮
        //form清空   下拉框默认公墓基金  查询公墓基金
        function clear() {
            form.clear();
            pubfundgrid.hide();
            spefundgrid.hide();
            cdsubgrid.hide();
            cdtposgrid.hide();
            rvrepospvgrid.hide();
            repospvgrid.hide();
            rvrepoinsuregrid.hide();
            repoinsuregrid.hide();
            rvreposettlegrid.hide();
            rvrepobankgrid.hide();
            repobankgrid.hide();
            rvrepostockgrid.hide();
            repostockgrid.hide();
            repocentbankgrid.hide();
            moneygrid.hide();
            query();
        }


        function exportExcel(){
            var data = form.getData(true);
            var sheetTypeText=mini.get("sheetType").getText();
            var fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
            fields += "<input type='hidden' id='sheetType' name='sheetType' value='"+data.sheetType+"'>";
            fields += "<input type='hidden' id='sheetTypeValue' name='sheetTypeValue' value='"+sheetTypeText+"'>";
            fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
            exportHrbExcel(fields);
        }
    
    </script>
</body>
</html>
