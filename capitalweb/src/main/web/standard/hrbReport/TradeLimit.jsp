<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
<head>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>

<body style="width: 100%; height: 100%; background: white">
    <div id="search_div" style="width: 100%;">
        <fieldset class="mini-fieldset title">
            <legend>信息查询</legend>
            <div id="search_form">
                <input id="acctngtype" name="acctngtype" class="mini-textbox" labelField="true" label="限额类型："
                        labelStyle="text-align:right;" emptyText="请输入限额类型" />
                <input id="inlevel" name="inlevel" class="mini-textbox" labelField="true" label="交易账户发行人主体外部评级： "
                       width="400px" labelStyle="text-align:right;width:180px" emptyText="交易账户发行人主体外部评级" />
                <input id="outlevel" name="outlevel" class="mini-textbox" labelField="true" label="交易账户债券债项外部评级："
                       width="400px" labelStyle="text-align:right;width:180px" emptyText="交易账户债券债项外部评级" />
                <span style="float:right;margin-right: 100px">
						<a id="search_btn" class="mini-button"    onclick="query()">查询</a>
                        <a id="clear_btn" class="mini-button"    onclick="clear()">清空</a>
                </span>
            </div>
            <span style="margin:2px;display: block;">
			<a id="add_btn" class="mini-button" style="display: none"   onclick="add()">新增</a>
            <a id="edit_btn" class="mini-button" style="display: none"   onclick="edit()">修改</a>
            <a id="delete_btn" class="mini-button"  style="display: none" onclick="del()">删除</a>
		</span>
        </fieldset>
    </div>

<div id="grid" class="mini-datagrid borderAll" style="width:100%;height:80%;" idField="id"
     allowAlternating="true" allowResize="true"
      multiSelect="true" >
    <div property="columns">
        <div type="checkcolumn"></div>
        <div type="indexcolumn" width="50px" headerAlign="center">序号</div>
        <div name="id" field="id"align="left" headerAlign="center">编号</div>
        <div name="acctngtype" width="170px" field="acctngtype"  align="left" headerAlign="center" >限额类型</div>
        <div name="deallmt" width="170px" field="deallmt"  align="left" headerAlign="center" numberFormat="#,0.00">交易账户交易限额</div>
        <div name="lostlmt" width="170px"field="lostlmt"  align="left" headerAlign="center" numberFormat="#,0.00">交易账户浮亏止损限额(%)</div>
        <div name="ratelmt" width="170px"field="ratelmt"  align="left" headerAlign="center" numberFormat="#,0.00">交易账户单支券占比限额(%)</div>
        <div name="inlevel" width="180px"field="inlevel"  align="left" headerAlign="center">交易账户发行人主体外部评级</div>
        <div name="outlevel" width="180px"field="outlevel"  align="left" headerAlign="center">交易账户债券债项外部评级</div>
        <div name="ratelmtb" width="180px"field="ratelmtb"  align="left" headerAlign="center" numberFormat="#,0.00">银行账户单支券占比限额(%)</div>
        <div name="inlevelb" width="180px"field="inlevelb"  align="left" headerAlign="center">银行账户发行人主体外部评级</div>
        <div name="outlevelb"width="170px" field="outlevelb"  align="left" headerAlign="center">银行账户债券债项外部评级</div>
        <div name="pvbp" width="170px"field="pvbp"  align="left" headerAlign="center" numberFormat="#,0.0000">估价基点价值限额(%)</div>
        <div name="duration" width="170px"field="duration"  align="left" headerAlign="center"numberFormat="#,0.00">久期限额</div>
        <div name="createTime" width="170px"field="createTime"  align="left" headerAlign="center">创建时间</div>
    </div>
</div>
<script>
    mini.parse();
    var visibleBtn = CommonUtil.hideBtn();
    //获取当前tab
    var grid = mini.get("grid");
    grid.on("beforeload", function(e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize, pageIndex);
    });

    $(document).ready(function() {
        search(10, 0);
    });

    top["taHrbReportManger"]=window;
    function getData(){
        var grid = mini.get("grid");
        var record =grid.getSelected();
        return record;
    }
    // 初始化数据
    function query(){
        search(grid.pageSize, 0);
    }
    function search(pageSize,pageIndex){
        var form = new mini.Form("#search_form");
        
        var data=form.getData();
        data['pageNumber']=pageIndex+1;
        data['pageSize']=pageSize;
        data['branchId']=branchId;
        var url="/IfsTradeLimitController/getDataPage";

        var params = mini.encode(data);
        CommonUtil.ajax({
            url:url,
            data:params,
            callback : function(data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }

    //添加
    function add() {
        var url = CommonUtil.baseWebPath() + "/../hrbReport/TradeLimitEdit.jsp";
        var tab = { id: "TradeLimitAdd", name: "TradeLimitAdd", title: "交易限额添加", url: url ,parentId:top["win"].tabs.getActiveTab().name,action:"add",showCloseButton:true};
        CommonUtil.openNewMenuTab(tab);
    }
    //修改
    function edit() {
        var row=grid.getSelecteds();
        if(row.length<1){
            mini.alert("请选择要修改的数据!")
            return;
        }
        var url = CommonUtil.baseWebPath() + "/../hrbReport/TradeLimitEdit.jsp";
        var tab = { id: "TradeLimitUpdate", name: "TradeLimitUpdate", title: "交易限额修改", url: url ,parentId:top["win"].tabs.getActiveTab().name,action:"edit",showCloseButton:true};
        var paramData = {selectData:row};
        CommonUtil.openNewMenuTab(tab,paramData);
    }

    //删除
    function del() {
        var rows=grid.getSelecteds();
        if(!rows.length>0){
            mini.alert("请选择要删除的数据!");
        }
        var param=new Array();
        rows.forEach(e=>{param.push(e)})
        CommonUtil.ajax({
            url:"/IfsTradeLimitController/delData",
            data:param,
            callback : function(data) {
                mini.alert("删除成功!");
                query();
            }
        });
    }

    function clear() {
        var form = new mini.Form("#search_form");
        form.clear();
        query();
    }
</script>
</body>
</html>