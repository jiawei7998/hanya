<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
<title>同业存单发行情况统计表</title>
</head>
<body>
    <fieldset class="mini-fieldset title">
        <div id="search_form" style="width:100%">
            <legend>报表查询</legend>
            <input id="queryDate" name="queryDate" class="mini-monthpicker" format="yyyy" labelField="true" allowInput="false" label="查询年份:"
                    value="<%=__bizDate%>" labelSyle="text-align:right;" emptyText="请输入"></input>
            <span>
                <a id="send_btn" class="mini-button" style="display: none"   onclick="query()">生成数据</a>
                <a id="export_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
            </span>
        </div>
    </fieldset>
    <div class="mini-fit" style="width:100%;height:100%;">
        <div id="slcd_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
            sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
            border="true" allowResize="true" showPager="false">
            <div property="columns">
                <div type="indexcolumn" headerAlign="center" align="center" width="30px">序号</div>
                <div field="PUB_MONTH"  headerAlign="center" align="center" width="50px">月份</div>
                <div field="TOTAL_LOSE_AMOUNT"  headerAlign="center" align="center" width="50px" numberFormat="#,0.00">总负债(万元)</div>
                <div field="SAME_LOSE_AMOUNT"  headerAlign="center" align="center" width="50px" numberFormat="#,0.00">同业负债(万元)</div>
                <div field="SAME_DPSUM"  headerAlign="center" align="center" width="50px" >同业存单发行笔数</div>
                <div field="SAME_DPMONEY"  headerAlign="center" align="center" width="50px" numberFormat="#,0.0000">同业存单发行金额(万元)</div>
                <div field="SAME_DPBALANCE"  headerAlign="center" align="center" width="51px" numberFormat="#,0.0000">同业存单余额(万元)</div>
            </div>
        </div>
    </div>
    <script>
        
        mini.parse();

        var grid = mini.get("slcd_grid");
        var form = new mini.Form("#search_form");
        var currTab = top["win"].tabs.getActiveTab();
        var params = currTab.params;
        var id = params.id;
        grid.on("beforeload", function (e) {
            e.cancel = true;
            var pageIndex = e.data.pageIndex;
            var pageSize = e.data.pageSize;
            search(pageSize,pageIndex);
        });
        
        $(document).ready(function() {
            //控制按钮显示
            $.when(CommonUtil.hideBtn(currTab.parentId)).done(function(visibleBtn){});
        });
    

        //生成数据
        function query() {
            var searchUrl="/ReportDptotalController/searchRepoDptotalPage";
            var data = form.getData(true);
            var params = mini.encode(data);
            CommonUtil.ajax({
                url:searchUrl,
                data:params,
                callback:function(data){
                    grid.setTotalCount(data.obj.total);
                    grid.setData(data.obj.rows);
                }
            });
        }

        //导出
        function exportExcel(){
            var content = grid.getData();

            if(content.length === 0){
                mini.alert("请先生成数据!");
                return;
            }

            var data = form.getData(true);
            var fields = null;
            var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
            fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
            exportHrbExcel(fields);
        }
    
    </script>
</body>
</html>
