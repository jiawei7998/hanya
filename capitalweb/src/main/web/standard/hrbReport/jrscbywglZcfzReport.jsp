<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../global.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>金融市场部业务管理报表  资产负债数据表</title>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
</head>
<body style="width: 100%; height: 100%; background: white">
<fieldset class="mini-fieldset">
    <div id="search_form" style="width: 50%;">
        <legend>查询条件</legend>
        <input id="queryDate" name="queryDate" class="mini-datepicker mini-mustFill" required="true" labelField="true"
               label="日期：" required="true" labelStyle="text-align:right;" emptyText="起始日期" format="yyyy-MM-dd"
               value="<%=__bizDate%>"/>
        <span style="float:right;">
			<a id="search_btn" class="mini-button" style="display: none" onclick="create()">查询</a>
			<a id="export_btn" class="mini-button" style="display: none" onclick="exportExcel()">导出报表</a>
		</span>
    </div>
    <span style="float:right;">单位:亿元，%</span>
</fieldset>

<div class="mini-fit" style="margin-top: 2px;margin-bottom: 2px;">
    <div id="datagrid" class="mini-datagrid borderAll" style="width: 100%; height: 100%;" allowAlternating="true" allowHeaderWrap="true" allowCellWrap ="true"
         multiSelect="true" allowResize="true" border="true" sortMode="client" multiSelect="true">
        <div property="columns">
            <div type="indexcolumn" headerAlign="center" width="40">序号</div>

            <div header="资产" headerAlign="center">
                <div property="columns">
                    <div field="zc" name="zc" width="100" align="left" headerAlign="center"  allowSort="false"></div>
                    <div field="zco" name="zco" width="200" align="left" headerAlign="center" allowSort="false"></div>
                </div>
            </div>
            <div field="zcYe" name="zcYe" width="120" align="right" headerAlign="center" allowSort="false"  numberFormat="n8">余额</div>
            <div field="zcSyl" name="zcSyl" width="120" align="right" headerAlign="center" allowSort="false" numberFormat="n8">收益率%</div>
            <div field="zcLxsr" name="zcLxsr" width="120" align="right" headerAlign="center" allowSort="false"  numberFormat="n8">利息收入</div>
            <div field="zcSylhyj" name="zcSylhyj" width="150" align="right" headerAlign="center" allowSort="false" numberFormat="n8">收益率（含溢价）%</div>
            <div field="zcSjyj" name="zcSjyj" width="120" align="right" headerAlign="center" allowSort="false"  numberFormat="n8">实际溢(折)价</div>
            <div field="zcSylhyjsy" name="zcSylhyjsy" width="170" align="right" headerAlign="center" allowSort="false" numberFormat="n8">收益率（含溢价、损益）%</div>
            <div field="zcGyjzbdsy" name="zcGyjzbdsy" width="150" align="right" headerAlign="center" allowSort="false"  numberFormat="n8">公允价值变动损益</div>
            <div field="zcSyye" name="zcSyye" width="120" align="right" headerAlign="center" allowSort="false" numberFormat="n8">上月余额</div>
            <div field="zcJsy" name="zcJsy" width="100" align="center" headerAlign="center" allowSort="false"  numberFormat="n8">较上月(+/-)</div>
            <div field="zcJsyzf" name="zcJsyzf" width="150" align="right" headerAlign="center" allowSort="false" numberFormat="n8">较上月增幅%</div>
            <div field="zcNcye" name="zcNcye" width="120" align="right" headerAlign="center" allowSort="false"  numberFormat="n8">年初余额</div>
            <div field="zcJnc" name="zcJnc" width="100" align="center" headerAlign="center" allowSort="false" numberFormat="n8">较年初(+/-)</div>
            <div field="zcJnczf" name="zcJnczf" width="150" align="right" headerAlign="center" allowSort="false"  numberFormat="n8">较年初增幅%</div>
            <div field="zcRje" name="zcRje" width="120" align="right" headerAlign="center" allowSort="false" numberFormat="n8">日均额</div>

            <div header="负债" headerAlign="center">
                <div property="columns">
                    <div field="fz" name="fz" width="120" align="left" headerAlign="center"  allowSort="false"></div>
                    <div field="fzo" name="fzo" width="120" align="left" headerAlign="center" allowSort="false"></div>
                </div>
            </div>
            <div field="fzYe" name="fzYe" width="120" align="right" headerAlign="center" allowSort="false"  numberFormat="n8">余额</div>
            <div field="fzFxl" name="fzFxl" width="120" align="right" headerAlign="center" allowSort="false" numberFormat="n8">付息率%</div>
            <div field="fzLxzc" name="fzLxzc" width="120" align="right" headerAlign="center" allowSort="false"  numberFormat="n8">利息支出</div>
            <div field="fzSyye" name="fzSyye" width="120" align="right" headerAlign="center" allowSort="false" numberFormat="n8">上月余额</div>
            <div field="fzJsy" name="fzJsy" width="100" align="center" headerAlign="center" allowSort="false"  numberFormat="n8">较上月(+/-)</div>
            <div field="fzJsyzf" name="fzJsyzf" width="150" align="right" headerAlign="center" allowSort="false" numberFormat="n8">较上月增幅%</div>
            <div field="fzNcye" name="fzNcye" width="120" align="right" headerAlign="center" allowSort="false"  numberFormat="n8">年初余额</div>
            <div field="fzJnc" name="fzJnc" width="100" align="center" headerAlign="center" allowSort="false" numberFormat="n8">较年初(+/-)</div>
            <div field="fzJnczf" name="fzJnczf" width="150" align="right" headerAlign="center" allowSort="false"  numberFormat="n8">较年初增幅%</div>
            <div field="fzRje" name="fzRje" width="120" align="right" headerAlign="center" allowSort="false" numberFormat="n8">日均额</div>

        </div>
    </div>
</div>

<script>
    mini.parse();
    var url = window.location.search;
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var id = params.id;
    var grid = mini.get("datagrid");
    var userId = '<%=__sessionUser.getUserId()%>';
    var form = new mini.Form("#search_form");

    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize, pageIndex);
    });


    // 查询
    function search(pageSize, pageIndex) {
        var form = new mini.Form("#search_form");
        form.validate();
        if (form.isValid() == false) {
            mini.alert("信息填写有误，请重新填写", "系统也提示");
            return;
        }
        var data = form.getData(true);
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        data['branchId'] = branchId;
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: "/IfsReportJrscbywglZcfzReportController/searchIfsReportJrscbywglZcfzReportPage",
            data: params,
            callback: function (data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);

                var marges = [
                    {rowIndex: 0, columnIndex: 1, rowSpan: 1, colSpan: 2}//存放同业款项（只含定期）
                    ,{rowIndex: 0, columnIndex: 17, rowSpan: 1, colSpan: 2}//同业存放款项
                    ,{rowIndex: 1, columnIndex: 1, rowSpan: 1, colSpan: 2}//拆放同业（只含境内）
                    ,{rowIndex: 1, columnIndex: 17, rowSpan: 1, colSpan: 2}//同业拆入
                    ,{rowIndex: 2, columnIndex: 1, rowSpan: 2, colSpan: 1}//买入返售债券
                    ,{rowIndex: 2, columnIndex: 17, rowSpan: 2, colSpan: 1}//卖出回购债券
                    ,{rowIndex: 4, columnIndex: 1, rowSpan: 7, colSpan: 1}//债券投资（如考虑免税政策，则综合收益率为5.07%）
                    ,{rowIndex: 4, columnIndex: 17, rowSpan: 1, colSpan: 2}//MLF
                    ,{rowIndex: 5, columnIndex: 17, rowSpan: 2, colSpan: 1}//发行债券
                    ,{rowIndex: 7, columnIndex: 17, rowSpan: 1, colSpan: 2}//发行同业存单
                    ,{rowIndex: 11, columnIndex: 1, rowSpan: 4, colSpan: 1}//基金投资（如考虑免税政策，则综合收益率为4.14%）
                    ,{rowIndex: 15, columnIndex: 1, rowSpan: 1, colSpan: 2}//债券+基金
                    ,{rowIndex: 16, columnIndex: 1, rowSpan: 1, colSpan: 2}//票据转贴现
                    ,{rowIndex: 16, columnIndex: 17, rowSpan: 1, colSpan: 2}//再贴现
                    ,{rowIndex: 17, columnIndex: 1, rowSpan: 1, colSpan: 2}//买入返售票据
                    ,{rowIndex: 17, columnIndex: 17, rowSpan: 1, colSpan: 2}//卖出回购票据
                    ,{rowIndex: 18, columnIndex: 1, rowSpan: 1, colSpan: 2}//资产总计
                    ,{rowIndex: 18, columnIndex: 17, rowSpan: 1, colSpan: 2}//负债总计
                ];
                grid.mergeCells(marges);
            }
        });
    }

    function query() {
        search(grid.pageSize, 0);
    }

    function clear() {
        var form = new mini.Form("search_form");
        form.clear();
        search(10, 0);
    }

    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
            search(100, 0);
        });
    });


    //交易日期
    function onDrawDateStart(e) {
        var startDate = e.date;
        var endDate = mini.get("edate").getValue();
        if (CommonUtil.isNull(endDate)) {
            return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }

    function onDrawDateEnd(e) {
        var endDate = e.date;
        var startDate = mini.get("sdate").getValue();
        if (CommonUtil.isNull(startDate)) {
            return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }

    //生成报表
    function create() {
        form.validate();
        if (form.isValid() == false) {
            mini.alert("信息填写有误，请重新填写", "系统提示");
            return;
        }
        var data = form.getData(true);
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: "/IfsReportJrscbywglZcfzReportController/createIfsReportJrscbywglZcfz",
            data: params,
            complete: function (data) {
                var text = data.responseText;
                var desc = JSON.parse(text);
                var content = desc.obj.retMsg;
                mini.alert(content);
                search(100, 0);
            }
        });
    }

    //导出
    function exportExcel(){
        var data = form.getData(true);
        var fields = null;
        var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
        fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
        exportHrbExcel(fields);
    }
</script>
</body>
</html>