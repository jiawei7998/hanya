<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
<head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
	<script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
</head>
<body style="width: 100%; height: 100%; background: white">
	<fieldset class="mini-fieldset"> 
		<legend>D02明细表</legend>
		<div id="search_form" style="width: 100%;margin:5px 0 5px 0">
			<input id="queryDate" name="queryDate" class="mini-datepicker" labelField="true" label="账务日期：" emptyText="账务日期" format="yyyy-MM-dd" value="<%=__bizDate%>"/>
			<span style="float: right; margin-left: 20px;margin-top:6px">
			        <a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
				    <a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
					<a id="export_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
			</span>
		</div>
	</fieldset>

<%--	<div class="mini-fit" style="margin-top: 2px;">--%>
<%--	<span style="float:right;">单位:万元</span>--%>
	<div id="grid1" class="mini-datagrid borderAll" style="width: 100%; height: 100%;" allowAlternating="true"
		allowResize="true" border="true" sortMode="client" multiSelect="true">
		<div property="columns">
			<div field="custName" headerAlign="center" align="center" width="120px">交易对手中文名称</div>
			<div field="tradeStartDate" headerAlign="center" align="center" width="120px">业务开始日期</div>
			<div field="tradeEndDate" headerAlign="center" align="center" width="120px">业务结束日期</div>
			<div field="rate" headerAlign="center" align="center" width="120px">利率</div>
			<div field="ccy" headerAlign="center" align="center" width="120px">币种</div>
			<div field="exchangeRate" headerAlign="center" align="center" width="120px">汇率</div>
			<div field="originalAmount" headerAlign="center" align="center" width="120px">原币金额</div>
			<div field="entrustedLoan" headerAlign="center" align="center" width="120px">是否委托贷款</div>
			<div field="custCountry" headerAlign="center" align="center" width="120px">对方国家</div>
			<div field="originalTerm" headerAlign="center" align="center" width="120px">原始期限</div>
			<div field="principalLastmonth" headerAlign="center" align="center" width="120px">上月末本金余额</div>
			<div field="interestReceivableLastmonth" headerAlign="center" align="center" width="120px">上月末应收利息余额</div>
			<div field="principalThismonth" headerAlign="center" align="center" width="120px">本月末本金余额</div>
			<div field="interestReceivableThismonth" headerAlign="center" align="center" width="120px">本月末应收利息余额</div>
			<div field="interestIncomThismonth" headerAlign="center" align="center" width="120px">本月利息收入</div>
			<div field="netBalanceThismonth" headerAlign="center" align="center" width="120px">本月净发生额</div>
		</div>
	</div>
</div>
	
<script>
	mini.parse();
	var url = window.location.search;
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var id = params.id;
	var implClass = params.implClass;

	var grid = mini.get("grid1");
	grid.on("beforeload", function(e) {
			e.cancel = true;
			var pageIndex = e.data.pageIndex;
			var pageSize = e.data.pageSize;
			search(pageSize, pageIndex);
		});
	$(document).ready(function() {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
			query();
		});
	});
	// 初始化数据
	function query(){
		search(grid.pageSize, 0);
	}
	function search(pageSize,pageIndex){
		var form = new mini.Form("#search_form");
		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		data.implClass=implClass;
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:"/ExportDataController/getExportPageData",
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
				grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	
	function clear(){
        var form=new mini.Form("search_form");
        form.clear();
        // mini.get("startDate").setValue(sysDate);
        // mini.get("endDate").setValue(sysDate);
        query();
	}
	
	//日期
	function onDrawDateStart(e) {
        var startDate = e.date;
        var endDate= mini.get("endDate").getValue();
        if(CommonUtil.isNull(endDate)){
        	return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }
	
	function onDrawDateEnd(e) {
        var endDate = e.date;
        var startDate = mini.get("startDate").getValue();
        if(CommonUtil.isNull(startDate)){
        	return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }
	//导出
	function exportExcel(){
		var form=new mini.Form("search_form");
		var data = form.getData(true);
		var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
		fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
		exportHrbExcel(fields);
	}
</script>
</body>
</html>