<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
<head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
	<script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>

</head>
<body style="width: 100%; height: 100%; background: white">
	<fieldset class="mini-fieldset"> 
		<legend>人行季度报表</legend>
		<div id="search_form" style="width: 100%;margin:5px 0 5px 0">
			<input id="queryDate" name="queryDate" class="mini-datepicker" labelField="true" label="账务日期：" emptyText="账务日期" format="yyyy-MM-dd" value="<%=__bizDate%>"/>
			<span style="float: right; margin-left: 20px;margin-top:6px">
			        <a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
				    <a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
					<a id="export_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
			</span>
		</div>
	</fieldset>

<%--	<div class="mini-fit" style="margin-top: 2px;">--%>
<%--	<span style="float:right;">单位:万元</span>--%>
	<div id="grid1" class="mini-datagrid borderAll" style="width: 100%; height: 100%;" allowAlternating="true"
		allowResize="true" border="true" sortMode="client" multiSelect="true">
		<div property="columns">
			<div field="" headerAlign="center" align="center">类别</div>
			<div header="正回购" headerAlign="center">
				<div property="columns">
					<div header="质押式正回购" headerAlign="center">
						<div property="columns">
							<div field="positiveCrBjfs" headerAlign="center" align="center">本季发生</div>
							<div field="positiveCrBtq" headerAlign="center" align="center">比同期</div>
							<div field="positiveCrZncljfs" headerAlign="center" align="center">自年初累计发生</div>
							<div field="positiveCrBjbs" headerAlign="center" align="center">本季笔数</div>
							<div field="positiveCrZncljbs" headerAlign="center" align="center">自年初累计笔数</div>
							<div field="positiveCrJyje" headerAlign="center" align="center">交易余额</div>
						</div>
					</div>
					<div header="买断式正回购" headerAlign="center">
						<div property="columns">
							<div field="positiveOrBjfs" headerAlign="center" align="center">本季发生</div>
							<div field="positiveOrBtq" headerAlign="center" align="center">比同期</div>
							<div field="positiveOrZncljfs" headerAlign="center" align="center">自年初累计发生</div>
							<div field="positiveOrBjbs" headerAlign="center" align="center">本季笔数</div>
							<div field="positiveOrZncljbs" headerAlign="center" align="center">自年初累计笔数</div>
						</div>
					</div>
				</div>
			</div>
			<div header="逆回购" headerAlign="center">
				<div property="columns">
					<div header="质押式逆回购" headerAlign="center">
						<div property="columns">
							<div field="inverseCrBjfs" headerAlign="center" align="center">本季发生</div>
							<div field="inverseCrBtq" headerAlign="center" align="center">比同期</div>
							<div field="inverseCrZncljfs" headerAlign="center" align="center">自年初累计发生</div>
							<div field="inverseCrBjbs" headerAlign="center" align="center">本季笔数</div>
							<div field="inverseCrZncljbs" headerAlign="center" align="center">自年初累计笔数</div>
							<div field="inverseCrJyje" headerAlign="center" align="center">交易余额</div>
						</div>
					</div>
					<div header="买断式逆回购" headerAlign="center">
						<div property="columns">
							<div field="inverseOrBjfs" headerAlign="center" align="center">本季发生</div>
							<div field="inverseOrBtq" headerAlign="center" align="center">比同期</div>
							<div field="inverseOrZncljfs" headerAlign="center" align="center">自年初累计发生</div>
							<div field="inverseOrBjbs" headerAlign="center" align="center">本季笔数</div>
							<div field="inverseOrZncljbs" headerAlign="center" align="center">自年初累计笔数</div>
						</div>
					</div>
				</div>
			</div>
			<div header="现券交易" headerAlign="center">
				<div property="columns">
					<div field="cbtBjfs" headerAlign="center" align="center">本季发生</div>
					<div field="cbtBtq" headerAlign="center" align="center">比同期</div>
					<div field="cbtZncljfs" headerAlign="center" align="center">自年初累计发生</div>
					<div field="cbtBjbs" headerAlign="center" align="center">本季笔数</div>
					<div field="cbtZncljbs" headerAlign="center" align="center">自年初累计笔数</div>
				</div>
			</div>
			<div header="承分销债券" headerAlign="center">
				<div property="columns">
					<div header="承分央票" headerAlign="center">
						<div property="columns">
							<div field="cfypBjfs" headerAlign="center" align="center">本季发生</div>
							<div field="cfypBtq" headerAlign="center" align="center">比同期</div>
							<div field="cfypZncljfs" headerAlign="center" align="center">自年初累计发生</div>
						</div>
					</div>
					<div header="承分政策性金融债" headerAlign="center">
						<div property="columns">
							<div field="cfzcxjrqBjfs" headerAlign="center" align="center">本季发生</div>
							<div field="cfzcxjrqBtq" headerAlign="center" align="center">比同期</div>
							<div field="cfzcxjrqZncljfs" headerAlign="center" align="center">自年初累计发生</div>
						</div>
					</div>
					<div header="承分其它" headerAlign="center">
						<div property="columns">
							<div field="cfqtBjfs" headerAlign="center" align="center">本季发生</div>
							<div field="cfqtBtq" headerAlign="center" align="center">比同期</div>
							<div field="cfqtZncljfs" headerAlign="center" align="center">自年初累计发生</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
	
<script>
	mini.parse();
	var url = window.location.search;
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var id = params.id;
	var implClass = params.implClass;

	var grid = mini.get("grid1");
	grid.on("beforeload", function(e) {
			e.cancel = true;
			var pageIndex = e.data.pageIndex;
			var pageSize = e.data.pageSize;
			search(pageSize, pageIndex);
		});
	$(document).ready(function() {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
			query();
		});
	});
	// 初始化数据
	function query(){
		search(grid.pageSize, 0);
	}
	function search(pageSize,pageIndex){
		var form = new mini.Form("#search_form");
		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		data.implClass=implClass;
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:"/ExportDataController/getExportPageData",
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
				grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	
	function clear(){
        var form=new mini.Form("search_form");
        form.clear();
        // mini.get("startDate").setValue(sysDate);
        // mini.get("endDate").setValue(sysDate);
        query();
	}
	
	//日期
	function onDrawDateStart(e) {
        var startDate = e.date;
        var endDate= mini.get("endDate").getValue();
        if(CommonUtil.isNull(endDate)){
        	return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }
	
	function onDrawDateEnd(e) {
        var endDate = e.date;
        var startDate = mini.get("startDate").getValue();
        if(CommonUtil.isNull(startDate)){
        	return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }
	//导出
	function exportExcel(){
		var form=new mini.Form("search_form");
		var data = form.getData(true);
		var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
		fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
		exportHrbExcel(fields);
	}
</script>
</body>
</html>