<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<title>优质流动性资产台账</title>
</head>
<body>
    <fieldset class="mini-fieldset title">
        <div id="search_form" style="width:100%">
            <legend>报表查询</legend>
            <input id="queryDate" name="queryDate" class="mini-datepicker" labelField="true" label="交易日期:" labelSyle="text-align:right;" emptyText="请输入"></input>
            <input id="seSicType" name="seSicType" class="mini-combobox" labelField="true" label="债券类型:" labelSyle="text-align:right;" data="CommonUtil.serverData.dictionary.seSicType" emptyText="请输入"></input>
            <span>
                <a class="mini-button" style="display: none"  id="search_btn" onclick="query()">查询</a>
                <a class="mini-button" style="display: none"  id="clear_btn" onclick="clear()">清空</a>
                <a id="export_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
            </span>
        </div>
    </fieldset>
    <div class="mini-fit" style="width:100%;height:100%;">
        <div id="curasset_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
            sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
            border="true" allowResize="true">
            <div property="columns">
                <div type="indexcolumn" headerAlign="center" align="center" width="30px">序号</div>
                <div field="secid"  headerAlign="center" align="center" width="50px">债券代码</div>
                <div field="descr"  headerAlign="center" align="center" width="50px">债券名称</div>
                <div field="secType"  headerAlign="center" align="center" width="50px">债券种类</div>
                <div field="assetType"  headerAlign="center" align="center" width="50px">LCR资产等级</div>
                <div field="prinamt"  headerAlign="center" align="center" width="50px">面值（万元）</div>
                <div field="rate"  headerAlign="center" align="center" width="50px">票面利率</div>
                <div field="subjectrating"  headerAlign="center" align="center" width="50px">主体评级</div>
                <div field="crating"  headerAlign="center" align="center" width="50px">债项评级</div>
                <div field="vdate"  headerAlign="center" align="center" width="50px">发行日</div>
                <div field="mdate"  headerAlign="center" align="center" width="50px">到期日</div>
                <div field="exerciseyears"  headerAlign="center" align="center" width="50px">剩余期限(年)</div>
                <div field="pledgeState"  headerAlign="center" align="center" width="50px">质押状态</div>
                <div field="unPleAmt"  headerAlign="center" align="center" width="50px">未质押账面价值（万元）</div>
                <div field="pleAmt"  headerAlign="center" align="center" width="50px">已质押账面价值（万元）</div>
                <div field="pleExeYears"  headerAlign="center" align="center" width="50px">已质押剩余期限(年) </div>
                <div field="pleMDate"  headerAlign="center" align="center" width="50px">质押到期日</div>
                <div field="pleRate"  headerAlign="center" align="center" width="50px">人行借款 质押率（%）</div>
                <div field="val"  headerAlign="center" align="center" width="50px">评估价值（万元）</div>
            </div>
        </div>
    </div>
    <script>
        
        mini.parse();

        var grid = mini.get("curasset_grid");
        var form = new mini.Form("#search_form");
        
        grid.on("beforeload", function (e) {
            e.cancel = true;
            var pageIndex = e.data.pageIndex; 
            var pageSize = e.data.pageSize;
            search(pageSize,pageIndex);
        });
        
        $(document).ready(function() {
            //控制按钮显示
            $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
                mini.get("queryDate").setValue('2019-10-08');
                mini.get("seSicType").setValue('1');
                query();
            });
        });
    
        //查询按钮
        function query(){
            search(10,0);
        }
        
        function search(pageSize, pageIndex) {
            var searchUrl="/CurAssetController/searchCurrentAssets";
             
            var data = form.getData(true);
            data['pageNumber'] = pageIndex + 1;
            data['pageSize'] = pageSize;
    
            var params = mini.encode(data);
     
            CommonUtil.ajax({
                url:searchUrl,
                data:params,
                callback:function(data){
                    grid.setTotalCount(data.obj.total);
                    grid.setPageIndex(pageIndex);
                    grid.setPageSize(pageSize);
                    grid.setData(data.obj.rows);
                }
            });
        }
        
        //清空按钮
        function clear() {
            form.clear();
            query();
        }
        
        function exportExcel(){
            var content = grid.getData();
            if(content.length == 0){
                mini.alert("请先查询数据");
                return;
            }
            
            mini.confirm("您确认要导出Excel吗?","系统提示", 
                function (action) {
                    if (action == "ok"){
                        var data = form.getData(true);
                        var fields = null;
                        
                        var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
                            fields += "<input type='hidden' id='seSicType' name='seSicType' value='"+data.seSicType+"'>";
                          fields += "<input type='hidden' id='excelName' name='excelName' value='yzldzc.xls'>";
                          fields += "<input type='hidden' id='id' name='id' value='yzldzc'>";
                        var urls = CommonUtil.pPath + "/sl/HrbReportManageController/exportExcel";
                        $('<form action="'+ urls +'" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();
                    }
                }
            );
        }
    
    </script>
</body>
</html>
