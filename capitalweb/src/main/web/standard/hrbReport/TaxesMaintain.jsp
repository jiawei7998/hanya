<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
<head>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>

<body style="width: 100%; height: 100%; background: white">
    <div id="search_div" style="width: 70%;">
        <fieldset class="mini-fieldset title">
            <legend>信息查询</legend>
            <div id="search_form">
                <input id="startDate" name="startDate" class="mini-datepicker" labelField="true" label="交易日期："
                       ondrawdate="onDrawDateStart" labelStyle="text-align:right;" emptyText="起始日期" format="yyyy-MM-dd" />
                <span>~</span>
                <input id="endDate" name="endDate" class="mini-datepicker"
                       ondrawdate="onDrawDateEnd" emptyText="结束日期" format="yyyy-MM-dd" />
                <span style="float:right;margin-right: 150px">
						<a id="search_btn" class="mini-button"    onclick="query()">查询</a>
                        <a id="clear_btn" class="mini-button"    onclick="clear()">清空</a>
                </span>
            </div>
            <span style="margin:2px;display: block;">
			<a id="add_btn" class="mini-button" style="display: none"   onclick="add()">新增</a>
            <a id="edit_btn" class="mini-button" style="display: none"   onclick="edit()">修改</a>
            <a id="delete_btn" class="mini-button"  style="display: none" onclick="del()">删除</a>
<%--			<a id="save_btn" class="mini-button"   style="display: none" onclick="save()">保存</a>--%>
		</span>
        </fieldset>
    </div>

<div id="grid" class="mini-datagrid borderAll" style="width:100%;height:80%;" idField="id"
     allowAlternating="true" allowResize="true"
      multiSelect="true" >
    <div property="columns">
        <div type="checkcolumn"></div>
        <div type="indexcolumn" width="50px" headerAlign="center">序号</div>
        <div header="上清所" headerAlign="center">
            <div property="columns">
                <div header="现券" headerAlign="center">
                    <div property="columns">
                        <div name="id" field="id"align="left" headerAlign="center">编号</div>
                        <div name="sqsBondSettlement1" field="sqsBondSettlement1"  align="left" headerAlign="center" numberFormat="#,0.00">纯券过户
                            <input property="editor" class="mini-spinner" maxValue="99999999999999.99" format="n2" style="width:100%;" minWidth="100" />
                        </div>
                        <div name="sqsBondSettlement2" field="sqsBondSettlement2"  align="left" headerAlign="center" numberFormat="#,0.00" >非纯券过户
                            <input property="editor" class="mini-spinner"  maxValue="99999999999999.99" format="n2"style="width:100%;" minWidth="100" />
                        </div>
                    </div>
                </div>
                <div header="质押式回购" headerAlign="center">
                    <div property="columns">
                        <div field="sqsCrSettlement2Single"  align="left" headerAlign="center" numberFormat="#,0.00" >单券
                            <input property="editor" class="mini-spinner"maxValue="99999999999999.99" format="n2" style="width:100%;" minWidth="100" />
                        </div>
                        <div field="sqsCrSettlement2Multiple"  align="left" headerAlign="center" numberFormat="#,0.00" >多券
                            <input property="editor" class="mini-spinner" maxValue="99999999999999.99" format="n2"style="width:100%;" minWidth="100" />
                        </div>
                    </div>
                </div>
                <div field="sqsOrSettlement2"  align="left" headerAlign="center" numberFormat="#,0.00" >买断式
                    <input property="editor" class="mini-spinner"maxValue="99999999999999.99" format="n2" style="width:100%;" minWidth="100" />
                </div>
                <div field="sqsForwardSettlement2"  align="left" headerAlign="center" numberFormat="#,0.00" >远期
                    <input property="editor" class="mini-spinner"maxValue="99999999999999.99" format="n2" style="width:100%;" minWidth="100" />
                </div>
                <div field="sqsFeeClassA" width="150px" align="left" headerAlign="center" numberFormat="#,0.00" >账户维护(大于200)
                    <input property="editor" class="mini-spinner" maxValue="99999999999999.99" format="n2"style="width:100%;" minWidth="100" />
                </div>
            </div>
        </div>
        <div header="中债" headerAlign="center">
            <div property="columns">
                <div header="现券" headerAlign="center">
                    <div property="columns">
                        <div field="zzBondSettlement1"  align="left" headerAlign="center" numberFormat="#,0.00" >纯券过户
                            <input property="editor" class="mini-spinner"maxValue="99999999999999.99" format="n2" style="width:100%;" minWidth="100" />
                        </div>
                        <div field="zzBondSettlement2"  align="left" headerAlign="center" numberFormat="#,0.00" >非纯券过户
                            <input property="editor" class="mini-spinner"maxValue="99999999999999.99" format="n2" style="width:100%;" minWidth="100" />
                        </div>
                    </div>
                </div>
                <div header="质押式回购" headerAlign="center">
                    <div property="columns">
                        <div field="zzCrSettlement2Single"  align="left" headerAlign="center" numberFormat="#,0.00" >单券
                            <input property="editor" class="mini-spinner" maxValue="99999999999999.99" format="n2"style="width:100%;" minWidth="100" />
                        </div>
                        <div field="zzCrSettlement2Multiple"  align="left" headerAlign="center" numberFormat="#,0.00" >多券
                            <input property="editor" class="mini-spinner"maxValue="99999999999999.99" format="n2" style="width:100%;" minWidth="100" />
                        </div>
                    </div>
                </div>
                <div field="zzOrSettlement2"  align="left" headerAlign="center" numberFormat="#,0.00" >买断式
                    <input property="editor" class="mini-spinner"maxValue="99999999999999.99" format="n2" style="width:100%;" minWidth="100" />
                </div>
                <div field="zzForwardSettlement2"  align="left" headerAlign="center" numberFormat="#,0.00" >远期
                    <input property="editor" class="mini-spinner"maxValue="99999999999999.99" format="n2" style="width:100%;" minWidth="100" />
                </div>

                <div header="账户维护" headerAlign="center">
                    <div property="columns">
                        <div field="zzFeeClassFirst1"  align="left" headerAlign="center" numberFormat="#,0.00" >大于200
                            <input property="editor" class="mini-spinner"maxValue="99999999999999.99" format="n2" style="width:100%;" minWidth="100" />
                        </div>
                        <div field="zzFeeClassFirst2"  align="left" headerAlign="center" numberFormat="#,0.00" >小于200
                            <input property="editor" class="mini-spinner"maxValue="99999999999999.99" format="n2" style="width:100%;" minWidth="100" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div header="前台税费[CFETS]" headerAlign="center">
            <div property="columns">
                <div header="融资类" headerAlign="center">
                    <div property="columns">
                        <div field="qtsfFinancing1" width="150px" align="left" headerAlign="center" numberFormat="#,0.0000000" renderer="formatNum"> 1天(含隔夜)
                        </div>
                        <div field="qtsfFinancing2" width="150px"  align="left" headerAlign="center" numberFormat="#,0.0000000" >2天以上（含两天）
                        </div>
                    </div>
                </div>
                <div field="qtsfBs"  align="left" headerAlign="center" numberFormat="#,0.0000000" >买卖类
                </div>
                <div field="qtsfTerminal"  align="left" headerAlign="center"  >终端数
                </div>
            </div>
        </div>
        <div field="createTime"  align="left" width="150px" headerAlign="center">创建时间</div>
    </div>
</div>
<script>
    mini.parse();
    var visibleBtn = CommonUtil.hideBtn();
    //获取当前tab
    var grid = mini.get("grid");
    grid.on("beforeload", function(e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize, pageIndex);
    });

    $(document).ready(function() {
        search(10, 0);
    });

    top["taHrbReportManger"]=window;
    function getData(){
        var grid = mini.get("grid");
        var record =grid.getSelected();
        return record;
    }
    // 初始化数据
    function query(){
        search(grid.pageSize, 0);
    }
    function search(pageSize,pageIndex){
        var form = new mini.Form("#search_form");
        
        var data=form.getData();
        data['pageNumber']=pageIndex+1;
        data['pageSize']=pageSize;
        data['branchId']=branchId;
        var url="/IfsTaxesController/getDataPage";

        var params = mini.encode(data);
        CommonUtil.ajax({
            url:url,
            data:params,
            callback : function(data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }

    function onDateRenderer(e) {
        if (e.value != null) {
            // var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g,'/'));
            var value = new Date(e.value);
            if (value) return mini.formatDate(value, 'yyyy-MM-dd');
        }
    }
    //添加
    function add() {
        var url = CommonUtil.baseWebPath() + "/../hrbReport/TaxesMaintainEdit.jsp";
        var tab = { id: "TaxesMaintainAdd", name: "TaxesMaintainAdd", title: "税费添加", url: url ,parentId:top["win"].tabs.getActiveTab().name,action:"add",showCloseButton:true};
        CommonUtil.openNewMenuTab(tab);
    }
    //修改
    function edit() {
        var row=grid.getSelecteds();
        if(row.length<1){
            mini.alert("请选择要修改的数据!")
            return;
        }
        var url = CommonUtil.baseWebPath() + "/../hrbReport/TaxesMaintainEdit.jsp";
        var tab = { id: "TaxesMaintainUpdate", name: "TaxesMaintainUpdate", title: "税费修改", url: url ,parentId:top["win"].tabs.getActiveTab().name,action:"edit",showCloseButton:true};
        var paramData = {selectData:row};
        CommonUtil.openNewMenuTab(tab,paramData);
    }

    //保存
    function save(){
        var param=new Array();
        grid.getChanges().forEach(e=>{param.push(e)})
        CommonUtil.ajax({
            url:"/IfsTaxesController/addData",
            data:param,
            callback : function(data) {
                mini.alert("保存成功!");
                query();
            }
        });

    }
    
    //删除
    function del() {
        var rows=grid.getSelecteds();
        if(!rows.length>0){
            mini.alert("请选择要删除的数据!");
        }
        var param=new Array();
        rows.forEach(e=>{param.push(e)})
        CommonUtil.ajax({
            url:"/IfsTaxesController/delData",
            data:param,
            callback : function(data) {
                mini.alert("删除成功!");
                query();
            }
        });
    }

    //将科学计数法转化为普通数字
    function formatNum(e){
        for (const key in e.row) {
            if(e.row[key]){
                if(e.row[key].toString().indexOf("e")!=-1){
                    return e.row[key].toFixed(7);
                }
            }

        }
    }
    function clear() {
        var form = new mini.Form("#search_form");
        form.clear();
        query();
    }
    //交易日期
    function onDrawDateStart(e) {
        var startDate = e.date;
        var endDate= mini.get("endDate").getValue();
        if(CommonUtil.isNull(endDate)){
            return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }
    function onDrawDateEnd(e) {
        var endDate = e.date;
        var startDate = mini.get("startDate").getValue();
        if(CommonUtil.isNull(startDate)){
            return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }

</script>
</body>
</html>