<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
<title>业务明细</title>
</head>
<body>
    <fieldset class="mini-fieldset title">
        <div id="search_form" style="width:100%">
            <legend>报表查询</legend>
            <input id="queryDate" name="queryDate" class="mini-datepicker" labelField="true" label="交易日期:"
                    labelSyle="text-align:right;" emptyText="请输入" value="<%=__bizDate%>"></input>
                <a class="mini-button" style="display: none"  id="search_btn" onclick="query()">查询</a>
<%--                <a class="mini-button" style="display: none"  id="clear_btn" onclick="clear()">清空</a>--%>
                <a id="export_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
            </span>
        </div>
    </fieldset>
    <span style="float:right;">单位:万元</span>
    <div class="mini-fit" style="width:100%;height:100%;">
        
        <div id="fund_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
             allowAlternating="true" border="true" allowResize="true" showPager="false" >
            <div property="columns" autoEscape="true">
                <<div type="indexcolumn" headerAlign="center" align="left" width="40px">序号</div>
                <div field="project" headerAlign="center" width="220px" align="lfet">项目</div>
                <div header="金额（按剩余期限）" headerAlign="center">
                    <div property="columns">
                        <div field="amountRemain6m" headerAlign="center" align="center" numberFormat="#,0.0000"><6个月</div>
                        <div field="amountRemain6m12m" headerAlign="center" align="center" numberFormat="#,0.0000">6-12个月</div>
                        <div field="amountRemain1yUnlimit" headerAlign="center" align="center" numberFormat="#,0.0000">≥1年</div>
                    </div>
                </div>
                <div header="折算率(%)" headerAlign="center">
                    <div property="columns">
                        <div field="rateRemain6m" headerAlign="center" align="center" ><6个月</div>
                        <div field="rateRemain6m12m" headerAlign="center" align="center" >6-12个月</div>
                        <div field="rateRemain1yUnlimit" headerAlign="center" align="center">≥1年</div>
                    </div>
                </div>
                <div header="折算后金额" headerAlign="center">
                    <div property="columns">
                        <div field="afterRateRemain6m" headerAlign="center" align="center" numberFormat="#,0.0000"><6个月</div>
                        <div field="afterRateRemain6m12m" headerAlign="center" align="center" numberFormat="#,0.0000">6-12个月</div>
                        <div field="afterRateRemain1yUnlimit" headerAlign="center" align="center" numberFormat="#,0.0000">≥1年</div>
                    </div>
                </div>
                <div field="total" headerAlign="center" align="center" numberFormat="#,0.0000">合计</div>
            </div>
        </div>
    </div>
    <script>
        mini.parse();
        var url = window.location.search;
        var currTab = top["win"].tabs.getActiveTab();
        var params = currTab.params;
        var id = params.id;
        var implClass = params.implClass;

        var fundgrid = mini.get("fund_grid");
        var form = new mini.Form("#search_form");

        $(document).ready(function() {
            //控制按钮显示
            $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
                query();
            });
        });
        //查询按钮
        function query(){
            var searchUrl="/ExportDataController/getExportListData";
            var data = form.getData(true);
            data.implClass=implClass;
            var params = mini.encode(data);
            CommonUtil.ajax({
                url:searchUrl,
                data:params,
                callback:function(data){
                    fundgrid.setData(data.obj);
                }
            });
        }

        function exportExcel(){
            var data = form.getData(true);
            var fields = null;
            var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
            fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
            exportHrbExcel(fields);
        }
    
    </script>
</body>
</html>
