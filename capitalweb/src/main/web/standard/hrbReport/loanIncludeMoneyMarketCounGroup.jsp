<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<title>贷款拆放含拆放银行同业及联行资产表按国别按币种</title>
</head>
<body>
    <fieldset class="mini-fieldset title">
        <div id="search_form" style="width:100%">
            <legend>报表查询</legend>
            <input id="queryDate" name="queryDate" class="mini-monthpicker" labelField="true" label="查询日期:" 
                    value="<%=__bizDate%>" labelSyle="text-align:right;" emptyText="请输入"></input>
            <span>
                <a class="mini-button" style="display: none"  id="search_btn" onclick="query()">查询</a>
                <a class="mini-button" style="display: none"  id="clear_btn" onclick="clear()">清空</a>
            </span>
        </div>
    </fieldset>
    <span style="float:right;">单位:元</span>
    <div class="mini-fit" style="width:100%;height:100%;">
        <div id="loan_coun_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
            sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
            border="true" allowResize="true">
            <div property="columns">
                <div type="indexcolumn" headerAlign="center" align="center" width="30px">序列</div>
                <div field="coun" headerAlign="center" align="center" width="100">国别</div>
                <div field="ccy" headerAlign="center" align="center" width="100px">币种</div>
                <div field="repMonth" headerAlign="center" align="center" width="100">报告期</div>
                <div field="lastAmt" headerAlign="center" align="center" width="100" numberFormat="#,0.00">上月末本金余额</div>
                <div field="lastAccroutst" headerAlign="center" align="center" width="100" numberFormat="#,0.00">上月末应收利息余额</div>
                <div field="ccyAmt" headerAlign="center" align="center" width="100" numberFormat="#,0.00">本月末本金余额</div>
                <div field="loneYearAmt" headerAlign="center" align="center" width="200" numberFormat="#,0.00">本月末本金余额:其中剩余期限在一年及以下</div>
                <div field="accroutst" headerAlign="center" align="center" width="100" numberFormat="#,0.00">本月末应收利息余额</div>
                <div field="changeAmt" headerAlign="center" align="center" width="100" numberFormat="#,0.00">本月非交易变动</div>
                <div field="accroutstAmt" headerAlign="center" align="center" width="100" numberFormat="#,0.00">本月净发生额</div>
                <div field="mtdIncexp" headerAlign="center" align="center" width="100" numberFormat="#,0.00">本月利息收入</div>
                <div field="desc" headerAlign="center" align="center" width="100" numberFormat="#,0.00">备注</div>
            </div>
        </div>
    </div>
    <script>
        
        mini.parse();

        var grid = mini.get("loan_coun_grid");
        var form = new mini.Form("#search_form");
        
        grid.on("beforeload", function (e) {
            e.cancel = true;
            var pageIndex = e.data.pageIndex; 
            var pageSize = e.data.pageSize;
            search(pageSize,pageIndex);
        });
        
        $(document).ready(function() {
            //控制按钮显示
            $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
                query();
            });
        });
    
        //查询按钮
        function query(){
            search(10,0);
        }
        
        function search(pageSize, pageIndex) {
            var searchUrl="/LoanIncludeMoneyMarketController/searchCoun";
             
            var data = form.getData(true);
            data['pageNum'] = pageIndex + 1;
            data['pageSize'] = pageSize;
    
            var params = mini.encode(data);
     
            CommonUtil.ajax({
                url:searchUrl,
                data:params,
                callback:function(data){
                    grid.setTotalCount(data.obj.total);
                    grid.setPageIndex(pageIndex);
                    grid.setPageSize(pageSize);
                    grid.setData(data.obj.rows);
                }
            });
        }
        
        //清空按钮
        function clear() {
            form.clear();
            query();
        }
        
    
    </script>
</body>
</html>
