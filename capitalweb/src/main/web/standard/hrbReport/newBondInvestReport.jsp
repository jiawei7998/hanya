<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>新债券投资情况表</title>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
	<script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
</head>
<body style="width: 100%; height: 100%; background: white">
<fieldset class="mini-fieldset">
	<div id="search_form" style="width: 50%;">
		<legend>查询条件</legend>
		<input id="queryDate" name="queryDate" class="mini-datepicker mini-mustFill" required="true" labelField="true" label="日期：" required="true"	labelStyle="text-align:right;" emptyText="起始日期" format="yyyy-MM-dd" value="<%=__bizDate%>"/>
		<span style="float:right;">
			<a id="search_btn" class="mini-button" style="display: none"   onclick="create()">查询</a>
			<a id="export_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
		</span>
	</div>
	<span style="float:right;">单位:元</span>
</fieldset>

<div class="mini-fit" style="margin-top: 2px;margin-bottom: 2px;">
	<div id="datagrid" class="mini-datagrid borderAll" style="width: 100%; height: 100%;" allowAlternating="true" multiSelect="true"
		allowResize="true" border="true" sortMode="client"  multiSelect="true">
		<div property="columns">
			<div type="indexcolumn" headerAlign="center" width="40">序号</div>
			<div field="product" name="product" width="100" align="center" headerAlign="center" allowSort="false">项目</div>

			<div header="券种" headerAlign="center">
				<div property="columns">
					<div field="qz" name="qz" width="100" align="center" headerAlign="center" allowSort="false"></div>
					<div field="qzo" name="qzo" width="200" align="center" headerAlign="center" allowSort="false"></div>
					<div field="qzt" name="qzt" width="200" align="center" headerAlign="center" allowSort="false"></div>
				</div>
			</div>

			<div field="fxqz" name="fxqz" width="80" align="center" headerAlign="center" allowSort="false">风险权重</div>
			<div header="交易账户" headerAlign="center">
				<div property="columns">
					<div field="jyzhYe" name="jyzhYe" width="120" align="right" headerAlign="center" allowSort="false" numberFormat="n4">余额</div>
					<div field="jyzhYslx" name="jyzhYslx" width="120" align="right" headerAlign="center" allowSort="false" numberFormat="n4">应收利息</div>
				</div>
			</div>
			<div header="银行账户" headerAlign="center">
				<div property="columns">
					<div field="yhzhYe" name="yhzhYe" width="120" align="right" headerAlign="center" allowSort="false" numberFormat="n4">余额</div>
					<div field="yhzhYslx" name="yhzhYslx" width="120" align="right" headerAlign="center" allowSort="false" numberFormat="n4">应收利息</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	mini.parse();
	var url = window.location.search;
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var id = params.id;
	var grid = mini.get("datagrid");
	var userId='<%=__sessionUser.getUserId()%>';
	var form = new mini.Form("#search_form");
	
	grid.on("beforeload", function(e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex;
		var pageSize = e.data.pageSize;
		search(pageSize, pageIndex);
	});
	
	
	// 查询
	function search(pageSize,pageIndex){
		var form = new mini.Form("#search_form");
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统也提示");
			return;
		}
		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:"/NewBondInvestReportController/searchRepoReportPage",
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);

				var marges = [
					{ rowIndex: 0, columnIndex: 1, rowSpan: 22, colSpan: 1 }//债券投资
					,{ rowIndex: 0, columnIndex: 2, rowSpan: 1, colSpan: 3 }//（一）国债
					,{ rowIndex: 1, columnIndex: 2, rowSpan: 1, colSpan: 3 }//（二）地方政府债
					,{ rowIndex: 2, columnIndex: 2, rowSpan: 1, colSpan: 3 }//（三）央票
					,{ rowIndex: 3, columnIndex: 2, rowSpan: 16, colSpan: 1 }//（四）金融债
					,{ rowIndex: 3, columnIndex: 3, rowSpan: 1, colSpan: 2 }//政策性银行债（不含次级债）
					,{ rowIndex: 4, columnIndex: 3, rowSpan: 2, colSpan: 1 }//商业银行债
					,{ rowIndex: 6, columnIndex: 3, rowSpan: 2, colSpan: 1 }//商业银行发行的同业存单
					,{ rowIndex: 8, columnIndex: 3, rowSpan: 2, colSpan: 1 }//其他金融机构发行的同业存单
					,{ rowIndex: 10, columnIndex: 3, rowSpan: 3, colSpan: 1 }//次级债、二级资本债
					,{ rowIndex: 13, columnIndex: 3, rowSpan: 3, colSpan: 1 }//权益工具（包括优先股、永续债）待确认
					,{ rowIndex: 16, columnIndex: 3, rowSpan: 1, colSpan: 2 }//我国中央政府投资的金融资产管理公司发行的债券
					,{ rowIndex: 17, columnIndex: 3, rowSpan: 1, colSpan: 2 }//我国中央政府投资的金融资产管理公司为收购国有银行不良贷款而定向发行的债券
					,{ rowIndex: 18, columnIndex: 3, rowSpan: 1, colSpan: 2 }//其他金融机构发行的债券
					,{ rowIndex: 19, columnIndex: 2, rowSpan: 1, colSpan: 3 }//（五）公用企业债 政府支持机构债券
					,{ rowIndex: 20, columnIndex: 2, rowSpan: 1, colSpan: 3 }//（六）一般企业发行的权益工具
					,{ rowIndex: 21, columnIndex: 2, rowSpan: 1, colSpan: 3 }//（七）其他企业债 除上面统计之外企业发行的债券
					,{ rowIndex: 22, columnIndex: 1, rowSpan: 1, colSpan: 5 }//
				];
				grid.mergeCells(marges);
			}
		});
	}
		
	function query() {
    	search(grid.pageSize, 0);
    }
	
	function clear(){
        var form=new mini.Form("search_form");
        form.clear();
        search(10,0);
	}
	
	$(document).ready(function() {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
			search(100, 0);
		});
	});


	//交易日期
	function onDrawDateStart(e) {
        var startDate = e.date;
        var endDate= mini.get("edate").getValue();
        if(CommonUtil.isNull(endDate)){
        	return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }
	function onDrawDateEnd(e) {
        var endDate = e.date;
        var startDate = mini.get("sdate").getValue();
        if(CommonUtil.isNull(startDate)){
        	return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }

    //生成报表
	function create() {
		form.validate();
		if (form.isValid() == false) {
			mini.alert("信息填写有误，请重新填写", "系统提示");
			return;
		}
		var data = form.getData(true);
		var params = mini.encode(data);
		CommonUtil.ajax({
			url : "/NewBondInvestReportController/createIfsReportXzqtz",
			data : params,
			complete : function(data) {
				var text = data.responseText;
				var desc = JSON.parse(text);
				var content = desc.obj.retMsg;
				mini.alert(content);
				search(100, 0);
			}
		});
	}

	function exportExcel(){
		var data = form.getData(true);
		var fields = null;
		var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
		fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
		exportHrbExcel(fields);
	}

</script>
</body>
</html>