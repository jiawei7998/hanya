<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>拆借交易台账</title>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>
<body style="width: 100%; height: 100%; background: white">
<fieldset class="mini-fieldset">
	<legend>查询条件</legend>
	<div id="search_form" style="width: 70%;">
		<nobr>
		<input id="sdate" name="sdate" class="mini-datepicker mini-mustFill" labelField="true" label="日期：" required="true"
					ondrawdate="onDrawDateStart" labelStyle="text-align:right;" emptyText="起始日期" format="yyyy-MM-dd" value="<%=__bizDate%>"/>
		<span>至</span>
		<input id="edate" name="edate" class="mini-datepicker  mini-mustFill" required="true"
					ondrawdate="onDrawDateEnd" emptyText="结束日期" format="yyyy-MM-dd" value="<%=__bizDate%>"/>
		</nobr>
		<input id="type" name="type" class="mini-combobox"  data="CommonUtil.serverData.dictionary.DldtType" value="CC" emptyText="请选择拆借类型"  label="拆借类型：" width="280px" labelField="true" labelStyle="text-align:right;"/>
		<input id="ccy" name="ccy" class="mini-combobox"  data="CommonUtil.serverData.dictionary.Currency" value="CNY" emptyText="请选择币种"  label="币种：" width="280px" labelField="true" labelStyle="text-align:right;"/>
		<input id="br" name="br" class="mini-combobox"  data="CommonUtil.serverData.dictionary.BrType" value="01" emptyText="请选择分行"  label="分行：" width="280px" labelField="true" labelStyle="text-align:right;"/>
		<span style="float: right; margin-right: 50px">
			<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
			<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
			<a id="export_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
		</span>
	</div>
</fieldset>

	
<div class="mini-fit" style="margin-top: 2px;margin-bottom: 2px;">
	<div id="datagrid" class="mini-datagrid borderAll" style="width: 100%; height: 100%;" allowAlternating="true" multiSelect="true"
		allowResize="true" border="true" sortMode="client" multiSelect="true">
		<div property="columns">
			<div type="indexcolumn" headerAlign="center" width="40">序号</div>
			<div field="dealNo" name="dealNo" width="100" align="center" headerAlign="center" allowSort="false">成交单号</div>
			<div field="costDesc" name="costDesc" width="350" align="center" headerAlign="center" allowSort="false">成本中心</div>
			<div field="cname" name="cname" width="200" align="center" headerAlign="center" allowSort="false">对手方</div>
			<div field="acctDesc" name="acctDesc" width="150" align="center" headerAlign="center" allowSort="false">对手属性</div>
			<div field="acctType" name="acctType" width="100" align="center" headerAlign="center" allowSort="false">账户类型</div>
			<div field="descr" name="descr" width="100" align="center" headerAlign="center" allowSort="false">拆借(回购)方式</div>
<%--			<div field="ccy" name="ccy" width="100" align="center" headerAlign="center" allowSort="false">币种</div>--%>
			<div field="vDate" name="vDate" width="100" align="center" headerAlign="center" allowSort="false">首期交割日</div>
			<div field="term" name="term" width="80" align="center" headerAlign="center" allowSort="false">期限</div>
			<div field="mDate" name="mDate" width="100" align="center" headerAlign="center" allowSort="false">到期交割日</div>
			
			<div field="intrate" name="intrate" width="80" align="right" headerAlign="center" allowSort="false">利率（%）</div>
			<div field="ccyAmtAbs" name="ccyAmtAbs" width="120" align="right" headerAlign="center" allowSort="false">券面（拆借）总额</div>
			<div field="ccyAmt" name="ccyAmt" width="120" align="right" headerAlign="center" allowSort="false">首期结算额</div>
			<div field="totpayAmt" name="totpayAmt" width="120" align="right" headerAlign="center" allowSort="false">到期结算额</div>
			<div field="totaLint" name="totaLint" width="120" align="right" headerAlign="center" allowSort="false">利息支额</div>
			<div field="occupyAmt" name="occupyAmt" width="120" align="right" headerAlign="center" allowSort="false">资金占用</div>
			<div field="handAmt" name="handAmt" width="120" align="right" headerAlign="center" allowSort="false">交易手续费</div>
			<div field="overTerm" name="overTerm" width="80" align="center" headerAlign="center" allowSort="false">剩余期限</div>
			<div field="rpLint" name="rpLint" width="120" align="right" headerAlign="center" allowSort="false">应收（付）利息额</div>
			<div field="overLint" name="overLint" width="120" align="right" headerAlign="center" allowSort="false">剩余利息额</div>
			<div field="proLoss" name="proLoss" width="120" align="right" headerAlign="center" allowSort="false">已实现损益</div>
			
			
		</div>
	</div>
</div>

<script>
	mini.parse();

	var url = window.location.search;
	var grid = mini.get("datagrid");
	var userId='<%=__sessionUser.getUserId()%>';
	var form = new mini.Form("#search_form");
	
	grid.on("beforeload", function(e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex;
		var pageSize = e.data.pageSize;
		search(pageSize, pageIndex);
	});
	
	
	// 查询
	function search(pageSize,pageIndex){
		var form = new mini.Form("#search_form");
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统也提示");
			return;
		}
		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:"/DldtReportController/searchDldtReportPage",
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
		
	function query() {
    	search(grid.pageSize, 0);
    }
	
	function clear(){
        var form=new mini.Form("search_form");
        form.clear();
        // search(10,0);
	}
	
	$(document).ready(function() {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
			search(10, 0);
		});
	});
	
	//交易日期
	function onDrawDateStart(e) {
        var startDate = e.date;
        var endDate= mini.get("edate").getValue();
        if(CommonUtil.isNull(endDate)){
        	return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }
	function onDrawDateEnd(e) {
        var endDate = e.date;
        var startDate = mini.get("sdate").getValue();
        if(CommonUtil.isNull(startDate)){
        	return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }
	
	//导出
	function exportExcel(){
		var content = grid.getData();
		if(content.length == 0){
			mini.alert("请先查询数据");
			return;
		}
		mini.confirm("您确认要导出Excel吗?","系统提示", 
	        function (action) {
	          if (action == "ok"){
	            var data = form.getData(true);
	            var fields = null;
	            
	            var  fields = "<input type='hidden' id='sdate' name='sdate' value='"+data.sdate+"'>";
		            fields += "<input type='hidden' id='edate' name='edate' value='"+data.edate+"'>";
		            fields += "<input type='hidden' id='type' name='type' value='"+data.type+"'>";
		            fields += "<input type='hidden' id='br' name='br' value='"+data.br+"'>";
		            
	              fields += "<input type='hidden' id='excelName' name='excelName' value='dldttz.xls'>";
	              fields += "<input type='hidden' id='id' name='id' value='dldttz'>";
	            var urls = CommonUtil.pPath + "/sl/HrbReportManageController/exportExcel";                                                                                                                         
	            $('<form action="'+ urls +'" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();   
	          }
	        }
		);
	}
</script>
</body>
</html>