<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
<head>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>

<body style="width: 100%; height: 100%; background: white">
    <div id="search_div" style="width: 100%;">
        <fieldset class="mini-fieldset title">
            <legend>信息查询</legend>
            <div id="search_form">
                <input id="startDate" name="startDate" class="mini-datepicker" labelField="true" label="生效日期："
                       ondrawdate="onDrawDateStart" labelStyle="text-align:right;" emptyText="起始日期" format="yyyy-MM-dd"/>
                <span>~</span>
                <input id="endDate" name="endDate" class="mini-datepicker"
                       ondrawdate="onDrawDateEnd" emptyText="结束日期" format="yyyy-MM-dd" />
                <span style="float:right;margin-right: 100px">
						<a id="search_btn" class="mini-button"  style="display: none"  onclick="query()">查询</a>
                        <a id="clear_btn" class="mini-button"  style="display: none"  onclick="clear()">清空</a>
                </span>
            </div>
            <span style="margin:2px;display: block;">
			<a id="add_btn" class="mini-button" style="display: none"   onclick="add()">新增</a>
            <a id="edit_btn" class="mini-button" style="display: none"   onclick="edit()">修改</a>
            <a id="delete_btn" class="mini-button"  style="display: none" onclick="del()">删除</a>
		</span>
        </fieldset>
    </div>

<div id="grid" class="mini-datagrid borderAll" style="width:100%;height:80%;" idField="id"
     allowAlternating="true" allowResize="true"
      multiSelect="true" >
    <div property="columns">
        <div type="checkcolumn"></div>
        <div type="indexcolumn" width="50px" headerAlign="center">序号</div>
        <div name="guozhai" width="170px" field="guozhai"  align="left" headerAlign="center" numberFormat="#,0.00">国债</div>
        <div name="zhengfuzhai" width="170px" field="zhengfuzhai"  align="left" headerAlign="center" numberFormat="#,0.00">政府债</div>
        <div name="tdz" width="170px"field="tdz"  align="left" headerAlign="center" numberFormat="#,0.00">铁道税</div>
        <div name="yingyeshui" width="170px"field="yingyeshui"  align="left" headerAlign="center" numberFormat="#,0.00">营业税</div>
        <div name="createdate" width="180px"field="createdate"  align="left" headerAlign="center">生效日期</div>
    </div>
</div>
<script>
    mini.parse();
    var visibleBtn = CommonUtil.hideBtn();
    //获取当前tab
    var grid = mini.get("grid");
    grid.on("beforeload", function(e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize, pageIndex);
    });

    $(document).ready(function() {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
            search(10, 0);
        });
    });

    top["taHrbReportManger"]=window;
    function getData(){
        var grid = mini.get("grid");
        var record =grid.getSelected();
        return record;
    }
    // 初始化数据
    function query(){
        search(grid.pageSize, 0);
    }
    function search(pageSize,pageIndex){
        var form = new mini.Form("#search_form");
        
        var data=form.getData();
        data['pageNumber']=pageIndex+1;
        data['pageSize']=pageSize;
        data['branchId']=branchId;
        var url="/KWabacusGuozhaimianshuiController/getPageList";

        var params = mini.encode(data);
        CommonUtil.ajax({
            url:url,
            data:params,
            callback : function(data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }

    //添加
    function add() {
        var url = CommonUtil.baseWebPath() + "/../hrbReport/GuozhaimianshuiEdit.jsp";
        var tab = { id: "GuozhaimianshuiAdd", name: "GuozhaimianshuiAdd", title: "国债免税添加", url: url ,parentId:top["win"].tabs.getActiveTab().name,action:"add",showCloseButton:true};
        CommonUtil.openNewMenuTab(tab);
    }
    //修改
    function edit() {
        var row=grid.getSelecteds();
        if(row.length<1){
            mini.alert("请选择要修改的数据!")
            return;
        }
        var url = CommonUtil.baseWebPath() + "/../hrbReport/GuozhaimianshuiEdit.jsp";
        var tab = { id: "GuozhaimianshuiUpdate", name: "GuozhaimianshuiUpdate", title: "国债免税修改", url: url ,parentId:top["win"].tabs.getActiveTab().name,action:"edit",showCloseButton:true};
        var paramData = {selectData:row};
        CommonUtil.openNewMenuTab(tab,paramData);
    }

    //删除
    function del() {
        var rows=grid.getSelecteds();
        if(!rows.length>0){
            mini.alert("请选择要删除的数据!");
        }
        CommonUtil.ajax({
            url:"/KWabacusGuozhaimianshuiController/delete",
            data:rows[0].id,
            callback : function(data) {
                mini.alert("删除成功!");
                query();
            }
        });
    }

    function clear() {
        var form = new mini.Form("#search_form");
        form.clear();
        query();
    }
</script>
</body>
</html>