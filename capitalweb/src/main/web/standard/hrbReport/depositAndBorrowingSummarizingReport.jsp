<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<title>存款含银行同业和联行存放负债-汇总表</title>
</head>
<body>
    <fieldset class="mini-fieldset title">
        <div id="search_form" style="width:100%">
            <legend>报表查询</legend>
            <input id="queryDate" name="queryDate" class="mini-monthpicker" labelField="true" label="交易日期:" 
                    value="<%=__bizDate%>" labelSyle="text-align:right;" emptyText="请输入"></input>
            <span>
                <a class="mini-button" style="display: none"  id="search_btn" onclick="query()">查询</a>
                <a class="mini-button" style="display: none"  id="clear_btn" onclick="clear()">清空</a>
                <a id="export_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
            </span>
        </div>
    </fieldset>
    <span style="float:left;">单位:元</span>
    <div id = "approveType" name = "approveType" class="mini-checkboxlist" style="float:right;" labelField="true"
         onvaluechanged="checkBoxValuechanged" label="审批列表选择：" multiSelect="false" valueField="id"
         labelStyle="text-align:right;border: none;background-color: #fff;" value="fore" textField="text"
         data="[{id:'fore',text:'存款含银行同业和联行存放负债-境外机构存款表'},{id:'branch',text:'存款含银行同业和联行存放负债-分行上传表'},{id:'sum',text:'存款含银行同业和联行存放负债-汇总表'}]" >
    </div>
    <div class="mini-fit" style="width:100%;height:100%;">
        <div id="loan_fore_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
             sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
             border="true" allowResize="true">
            <div property="columns">
                <div type="indexcolumn" headerAlign="center" align="center" width="30px">序号</div>
                <div field="dataNo" headerAlign="center" align="center" width="50px">数据自编码</div>
                <div field="orgNo" headerAlign="center" align="center" width="50px">填报机构代码</div>
                <div field="repMonth" headerAlign="center" align="center" width="50px">报告期</div>
                <div field="cndeNo" headerAlign="center" align="center" width="50px">外债编号</div>
                <div field="accroutst" headerAlign="center" align="center" width="50px">上月末应付利息余额</div>
                <div field="amount" headerAlign="center" align="center" width="50px">本月末本金余额其中剩余期限在一年及以下</div>
                <div field="effordamt" headerAlign="center" align="center" width="50px">本月末应付利息余额</div>
                <div field="balanceAmt" headerAlign="center" align="center" width="50px">本月净发生额</div>
                <div field="effordedamt" headerAlign="center" align="center" width="50px">本月利息支出</div>
                <div field="desc" headerAlign="center" align="center" width="50px">备注</div>
            </div>
        </div>
        <div id="loan_branch_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
             sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
             border="true" allowResize="true">
            <div property="columns">
                <div type="indexcolumn" headerAlign="center" align="center" width="30px">序号</div>
                <div field="dataNo" headerAlign="center" align="center" width="50px">数据自编码</div>
                <div field="orgNo" headerAlign="center" align="center" width="50px">填报机构代码</div>
                <div field="repMonth" headerAlign="center" align="center" width="50px">报告期</div>
                <div field="coun" headerAlign="center" align="center" width="50px">国别</div>
                <div field="ccy" headerAlign="center" align="center" width="50px">币种</div>
                <div field="cndeNo" headerAlign="center" align="center" width="50px">外债编号</div>
                <div field="accroutst" headerAlign="center" align="center" width="50px">上月末应付利息余额</div>
                <div field="amount" headerAlign="center" align="center" width="50px">本月本金余额</div>
                <div field="effordamt" headerAlign="center" align="center" width="50px">本月末应付利息余额</div>
                <div field="balanceamt" headerAlign="center" align="center" width="50px">本月净发生额</div>
                <div field="effordedamt" headerAlign="center" align="center" width="50px">本月利息支出</div>
                <div field="descs" headerAlign="center" align="center" width="50px">备注</div>
            </div>
        </div>
        <div id="loan_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
            sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
            border="true" allowResize="true">
            <div property="columns">
                <div type="indexcolumn" headerAlign="center" align="center" width="30px">序号</div>
                <div field="coun" headerAlign="center" align="center" width="50px">国别</div>
                <div field="ccy" headerAlign="center" align="center" width="50px">币种</div>
                <div field="repMonth" headerAlign="center" align="center" width="50px">报告期</div>
                <div field="accroutst" headerAlign="center" align="center" width="50px">上月末应付利息余额</div>
                <div field="amount" headerAlign="center" align="center" width="50px">本月末本金余额</div>
                <div field="effordamt" headerAlign="center" align="center" width="50px">本月末应付利息余额</div>
                <div field="balanceAmt" headerAlign="center" align="center" width="50px">本月净发生额</div>
                <div field="effordedAmt" headerAlign="center" align="center" width="50px">本月利息支出</div>
            </div>
        </div>
    </div>
    <script>
        
        mini.parse();

        var foreGrid = mini.get("loan_fore_grid");
        var branchGrid = mini.get("loan_branch_grid");
        var grid = mini.get("loan_grid");
        var form = new mini.Form("#search_form");

        foreGrid.on("beforeload", function (e) {
            e.cancel = true;
            var pageIndex = e.data.pageIndex; 
            var pageSize = e.data.pageSize;
            search(pageSize,pageIndex);
        });
        branchGrid.on("beforeload", function (e) {
            e.cancel = true;
            var pageIndex = e.data.pageIndex;
            var pageSize = e.data.pageSize;
            search(pageSize,pageIndex);
        });
        grid.on("beforeload", function (e) {
            e.cancel = true;
            var pageIndex = e.data.pageIndex;
            var pageSize = e.data.pageSize;
            search(pageSize,pageIndex);
        });
        
        $(document).ready(function() {
            //控制按钮显示
            $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
                query();
            });
        });
    
        //查询按钮
        function query(){
            search(10,0);
        }
        
        function search(pageSize, pageIndex) {
            var approveType = mini.get("approveType").getValue();
            var searchUrl;
            if(approveType == "fore"){
                searchUrl="/DepositAndBorrowingController/searchForeInvestor";
                foreGrid.show();
                branchGrid.hide();
                grid.hide();
            }else if(approveType == "branch"){
                searchUrl="/DepositAndBorrowingController/searchBranchUpload";
                foreGrid.hide();
                branchGrid.show();
                grid.hide();
            }else if(approveType == "sum"){
                searchUrl="/DepositAndBorrowingController/searchSummarizing";
                foreGrid.hide();
                branchGrid.hide();
                grid.show();
            }

            var data = form.getData(true);
            data['pageNum'] = pageIndex + 1;
            data['pageSize'] = pageSize;
    
            var params = mini.encode(data);
     
            CommonUtil.ajax({
                url:searchUrl,
                data:params,
                callback:function(data){
                    grid.setTotalCount(data.obj.total);
                    grid.setPageIndex(pageIndex);
                    grid.setPageSize(pageSize);
                    grid.setData(data.obj.rows);
                }
            });
        }
        
        //清空按钮
        function clear() {
            form.clear();
            query();
        }
        
        function exportExcel(){
            var content = grid.getData();
            if(content.length == 0){
                mini.alert("请先查询数据");
                return;
            }
            
            mini.confirm("您确认要导出Excel吗?","系统提示", 
                function (action) {
                    if (action == "ok"){
                        var data = form.getData(true);
                        var fields = null;
                        
                        var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
                          fields += "<input type='hidden' id='excelName' name='excelName' value='ckty.xls'>";
                          fields += "<input type='hidden' id='id' name='id' value='ckty'>";
                        var urls = CommonUtil.pPath + "/sl/HrbReportManageController/exportExcel";
                        $('<form action="'+ urls +'" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();
                    }
                }
            );
        }


        function checkBoxValuechanged(){
            search(10,0);
        }
    
    </script>
</body>
</html>
