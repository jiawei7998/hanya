<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<title>资金交易信息表</title>
</head>
<body>
    <fieldset class="mini-fieldset title">
        <div id="search_form" style="width:100%">
            <legend>报表查询</legend>
            <input id="queryDate" name="queryDate" field="queryDate" class="mini-datepicker" labelField="true"  value="<%=__bizDate%>" label="日期："
            required="true"  labelStyle="text-align:center;" allowinput="false" labelStyle="width:100px" width="280px" onvaluechanged="search(10,0)"  />

            <span>
                <a class="mini-button" style="display: none"  id="search_btn" onclick="query()">查询</a>
                <a class="mini-button" style="display: none"  id="clear_btn" onclick="clear()">清空</a>
                <a id="export_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
            </span>
        </div>
    </fieldset>
    <div class="mini-fit" style="width:100%;height:100%;">
        <div id="loan_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
            sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
            border="true" allowResize="true">
            <div property="columns">
            <div field="br" headerAlign="center" align="center" width="100px">银行机构代码</div>
            <div field="finance" headerAlign="center" align="center" width="100px">金融许可证号</div>
            <div field="br2" headerAlign="center" align="center" width="100px">内部机构号</div>
            <div field="glno" headerAlign="center" align="center" width="100px">明细科目编号</div>
            <div field="banksn" headerAlign="center" align="center" width="100px">银行机构名称</div>
            <div field="glno2" headerAlign="center" align="center" width="100px">明细科目名称</div>
            <div field="dealno" headerAlign="center" align="center" width="100px">交易编号</div>
            <div field="prodcode" headerAlign="center" align="center" width="100px">理财产品登记编码</div>
            <div field="type" headerAlign="center" align="center" width="100px">资金交易类型</div>
            <div field="subtype" headerAlign="center" align="center" width="100px">资金交易子类</div>
            <div field="financeCode" headerAlign="center" align="center" width="100px">金融工具编号</div>
            <div field="acctngtype" headerAlign="center" align="center" width="100px">帐户类型</div>
            <div field="contractCode" headerAlign="center" align="center" width="100px">合同号</div>
            <div field="contractAmt" headerAlign="center" align="center" width="100px">合同金额</div>
            <div field="ccy" headerAlign="center" align="center" width="100px">币种</div>
            <div field="custname" headerAlign="center" align="center" width="100px">基础资产客户名称</div>
            <div field="custIndustry" headerAlign="center" align="center" width="100px">基础资产所属行业</div>
            <div field="isbankcust" headerAlign="center" align="center" width="100px">基础资产是否为本行客户</div>
            <div field="credittype" headerAlign="center" align="center" width="100px">基础资产增信方式</div>
            <div field="crediter" headerAlign="center" align="center" width="100px">基础资产增信人</div>
            <div field="trad" headerAlign="center" align="center" width="100px">交易柜员</div>
            <div field="voper" headerAlign="center" align="center" width="100px">审核人</div>
            <div field="cno" headerAlign="center" align="center" width="100px">交易对手代码</div>
            <div field="sn" headerAlign="center" align="center" width="100px">交易对手名称</div>
            <div field="dealdate" headerAlign="center" align="center" width="100px">交易日期</div>
            <div field="vdate" headerAlign="center" align="center" width="100px">起始日期</div>
            <div field="mdate" headerAlign="center" align="center" width="100px">到期日期</div>
            <div field="ps" headerAlign="center" align="center" width="100px">买卖标志</div>
            <div field="spotfwdind" headerAlign="center" align="center" width="100px">即远期标志</div>
            <div field="ccy2" headerAlign="center" align="center" width="100px">买入币种</div>
            <div field="ccyamt" headerAlign="center" align="center" width="100px">买入金额</div>
            <div field="ctrccy" headerAlign="center" align="center" width="100px">卖出币种</div>
            <div field="ctramt" headerAlign="center" align="center" width="100px">卖出金额</div>
            <div field="dealamt" headerAlign="center" align="center" width="100px">成交价格</div>
            <div field="dealtype" headerAlign="center" align="center" width="100px">交易状态</div>
            <div field="verdate" headerAlign="center" align="center" width="100px">复核日期</div>
            <div field="revdate" headerAlign="center" align="center" width="100px">取消日期</div>
            <div field="ccysettdate" headerAlign="center" align="center" width="100px">实际交割日期</div>
            <div field="netsi" headerAlign="center" align="center" width="100px">清算标志</div>
            <div field="debtorAccount" headerAlign="center" align="center" width="100px">借方账号</div>
            <div field="creditAccount" headerAlign="center" align="center" width="100px">贷方账号</div>
            <div field="debtorAmt" headerAlign="center" align="center" width="100px">借方金额</div>
            <div field="creditAmt" headerAlign="center" align="center" width="100px">贷方金额</div>
            <div field="debtorCcy" headerAlign="center" align="center" width="100px">借方币种</div>
            <div field="creditCcy" headerAlign="center" align="center" width="100px">贷方币种</div>
            <div field="debtorRate" headerAlign="center" align="center" width="100px">借方利率</div>
            <div field="creditRate" headerAlign="center" align="center" width="100px">贷方利率</div>
            <div field="earnestTrade " headerAlign="center" align="center" width="100px">保证金交易标志</div>
            <div field="earnestAccount" headerAlign="center" align="center" width="100px">关联保证金账户</div>
            <div field="prodNumber " headerAlign="center" align="center" width="100px">关联业务编号</div>
            <div field="systemName" headerAlign="center" align="center" width="100px">外部关联系统名称</div>
            </div>
        </div>
    </div>
    <script>
        
        mini.parse();

        var grid = mini.get("loan_grid");
        var form = new mini.Form("#search_form");
        
        grid.on("beforeload", function (e) {
            e.cancel = true;
            var pageIndex = e.data.pageIndex; 
            var pageSize = e.data.pageSize;
            search(pageSize,pageIndex);
        });
        
        $(document).ready(function() {
            //控制按钮显示
            $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
                query();
            });
        });
    
        //查询按钮
        function query(){
            search(10,0);
        }
        
        function search(pageSize, pageIndex) {
            var searchUrl="/CapitalTransactionCotroller/search";
             
            var data = form.getData(true);
            data['pageNum'] = pageIndex + 1;
            data['pageSize'] = pageSize;
    
            var params = mini.encode(data);
     
            CommonUtil.ajax({
                url:searchUrl,
                data:params,
                callback:function(data){
                    grid.setTotalCount(data.obj.total);
                    grid.setPageIndex(pageIndex);
                    grid.setPageSize(pageSize);
                    grid.setData(data.obj.rows);
                }
            });
        }
        
        //清空按钮
        function clear() {
            form.clear();
            query();
        }
    
        
        
        //导出
        function exportExcel(){
            var content = grid.getData();
            if(content.length == 0){
                mini.alert("请先查询数据");
                return;
            }

        
        mini.confirm("您确认要导出Excel吗?","系统提示", 
                function (action) {
                  if (action == "ok"){
                    var data = form.getData(true);
                    var fields = null;
                    
                    var  fields = "<input type='hidden' id='dealNo' name='dealNo' value='"+data.dealNo+"'>";
                      
                      fields += "<input type='hidden' id='excelName' name='excelName' value='zjjy.xls'>";
                      fields += "<input type='hidden' id='id' name='id' value='zjjy'>";
                    var urls = CommonUtil.pPath + "/sl/HrbReportManageController/exportExcel";                                                                                                                         
                    $('<form action="'+ urls +'" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();   
                  }
                }
        );
        }
        
        
        
        
    </script>
</body>
</html>
