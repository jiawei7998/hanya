<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../global.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
    <title>G15最大十家关联方关联交易情况表</title>
</head>
<body>
<fieldset class="mini-fieldset title">
    <div id="search_form" style="width:100%">
        <legend>报表查询</legend>
        <input id="queryDate" name="queryDate" class="mini-datepicker" labelField="true" label="交易日期:"
               labelSyle="text-align:right;" emptyText="请输入" format="yyyy-MM-dd" value="<%=__bizDate%>"></input>
        <a class="mini-button" style="display: none" id="search_btn" onclick="search()">查询</a>
        <%--                <a class="mini-button" style="display: none"  id="clear_btn" onclick="clear()">清空</a>--%>
        <a id="export_btn" class="mini-button" style="display: none" onclick="exportExcel()">导出报表</a>
        </span>
    </div>
</fieldset>
<span style="float:right;">单位:万元</span>
<div class="mini-fit" style="width:100%;height:100%;">
    <div id="fund_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
         allowAlternating="true" border="true" allowResize="true" showPager="false">
        <div property="columns" autoEscape="true">
            <div field="ser" headerAlign="center" align="right" width="200px">排序</div>
            <div field="glfmc" headerAlign="center" align="center" width="100px">关联方名称</div>
            <div field="ghdm" headerAlign="center" align="center" width="100px">客户代码</div>
            <div field="glflx" headerAlign="center" align="center" width="100px">关联方类型</div>
            <div field="cgbl" headerAlign="center" align="center" width="100px">持股比例(%)</div>
            <div header="报告期内最高风险额" headerAlign="center">
                <div property="columns">
                    <div field="je" headerAlign="center" align="center" width="100px">净额</div>
                    <div field="zzbjebl" headerAlign="center" align="center" width="100px">占资本净额比例</div>
                </div>
            </div>
            <div header="表内授信" headerAlign="center">
                <div property="columns">
                    <div field="gxdk" headerAlign="center" align="center" width="100px">各项贷款</div>
                    <div field="zqtz" headerAlign="center" align="center" width="100px">债券投资</div>
                    <div field="tdmdzt" headerAlign="center" align="center" width="150px">特定目的载体投资</div>
                    <div field="qtbnsx" headerAlign="center" align="center" width="100px">其他表内授信</div>
                </div>
            </div>
            <div header="表外授信" headerAlign="center">
                <div property="columns">
                    <div field="bkcdfz" headerAlign="center" align="center" width="250px">不可撤销的承诺及或有负债</div>
                    <div field="fbsx" headerAlign="center" align="center" width="250px">本行非保本理财产品进行的授信</div>
                    <div field="qitabwsx" headerAlign="center" align="center" width="100px">其他表外授信</div>
                </div>
            </div>
            <div header="表内外授信合计" headerAlign="center">
                <div property="columns">
                    <div field="jz" headerAlign="center" align="center" width="100px">净额</div>
                    <div field="bzj" headerAlign="center" align="center" width="250px">保证金、银行存单、国债</div>
                    <div field="bl" headerAlign="center" align="center" width="100px">占资本净额比例</div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    mini.parse();
    var url = window.location.search;
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var id = params.id;
    var implClass = params.implClass;
    var grid = mini.get("fund_grid");
    var form = new mini.Form("#search_form");

    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize,pageIndex);
    });

    $(document).ready(function() {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
            search(10,0);
        });
    });

    //查询按钮
    function search(pageSize, pageIndex)  {
        var searchUrl="/ZdsjgljxController/callZdsjgljx";
        var data = form.getData(true);
        var params = mini.encode(data);
        CommonUtil.ajax({
            url:searchUrl,
            data:params,
            callback:function(data){
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }

    function exportExcel(){
        var data = form.getData(true);
        var fields = null;
        var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
        fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
        exportHrbExcel(fields);
    }
</script>
</body>
</html>
