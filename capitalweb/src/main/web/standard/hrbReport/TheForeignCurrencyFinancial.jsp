<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../global.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
    <title>哈尔滨银行本外币同业台账-金融市场部</title>
</head>
<body>
<fieldset class="mini-fieldset title">
    <div id="search_form" style="width:100%">
        <legend>报表查询</legend>
        <input id="queryDate" name="queryDate" class="mini-datepicker" labelField="true" label="交易日期:"
               value="<%=__bizDate%>" labelSyle="text-align:right;" emptyText="请输入"></input>

        <input id="dealtype" name="dealtype" class="mini-combobox" data="CommonUtil.serverData.dictionary.dealtype"
               labelField="true"
               label="报表类型:" labelSyle="text-align:right;" emptyText="请输入" width="400px" onvaluechanged="search(10,0)"
               value="BTYCF"></input>

        <span>
                <a class="mini-button" style="display: none" id="search_btn" onclick="query()">查询</a>
<%--                <a class="mini-button" style="display: none" id="clear_btn" onclick="clear()">清空</a>--%>
                <a id="export_btn" class="mini-button" style="display: none" onclick="exportExcel()">导出报表</a>
            </span>
    </div>
</fieldset>
<div class="mini-fit" style="width:100%;height:100%;">
    <div id="loan_grid1" class="mini-datagrid borderAll" style="width:100%;height:100%;"
         sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
         border="true" allowResize="true">
        <div property="columns">
            <div type="indexcolumn" headerAlign="center" align="center" width="30px">序号</div>
            <div field="fhnc" headerAlign="center" align="center" width="150px">分行名称（含总行）</div>
            <div field="zhmc" headerAlign="center" align="center" width="150px">支行名称（含总行部门）</div>
            <div field="gsbm" headerAlign="center" align="center" width="150px">归属部门</div>
            <div field="jydszbqc" headerAlign="center" align="center" width="150px">交易对手总部全称</div>
            <div field="jydsldjg" headerAlign="center" align="center" width="150px">交易对手落地机构</div>
            <div field="jydsdy" headerAlign="center" align="center" width="150px">交易对手地域</div>
            <div field="qxlx" headerAlign="center" align="center" width="150px">活期/定期</div>
            <div field="ywkssj" headerAlign="center" align="center" width="150px">业务开始日期</div>
            <div field="ywjssj" headerAlign="center" align="center" width="150px">业务结束日期</div>
            <div field="rate" headerAlign="center" align="center" width="150px">利率 （%）</div>
            <div field="ratebz" headerAlign="center" align="center" width="150px">利率备注</div>
            <div field="amt" headerAlign="center" align="center" width="150px" numberFormat="#,0.00">金额（元）</div>
            <div field="ye" headerAlign="center" align="center" width="150px" numberFormat="#,0.00">余额（元）</div>
            <div field="jzkm" headerAlign="center" align="center" width="150px">记账科目</div>
            <div field="zhxz" headerAlign="center" align="center" width="150px">账户性质</div>
            <div field="zhyt" headerAlign="center" align="center" width="150px">账户用途</div>
            <div field="zhzt" headerAlign="center" align="center" width="150px">账户状态</div>
            <div field="sfkwy" headerAlign="center" align="center" width="150px">是否开网银</div>
            <div field="lxr" headerAlign="center" align="center" width="150px">联系人</div>
            <div field="phone" headerAlign="center" align="center" width="150px">电话</div>
            <div field="beizhu" headerAlign="center" align="center" width="150px">备注</div>
            <div field="jyds" headerAlign="center" align="center" width="150px">交易对手（全称）</div>
            <div field="bfqszh" headerAlign="center" align="center" width="150px">本方清算账号(必输)</div>
            <div field="bfqshh" headerAlign="center" align="center" width="150px">本方清算行号</div>
            <div field="dfqszh" headerAlign="center" align="center" width="150px">对方清算账号(必输)</div>
            <div field="dfqshh" headerAlign="center" align="center" width="150px">对方清算行号</div>
            <div field="dealno" headerAlign="center" align="center" width="150px">交易编号</div>
        </div>
    </div>
    <div id="loan_grid2" class="mini-datagrid borderAll" style="width:100%;height:100%;"
         sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
         border="true" allowResize="true">
        <div property="columns">
            <div type="indexcolumn" headerAlign="center" align="center" width="30px">序号</div>
            <div field="fhnc" headerAlign="center" align="center" width="150px">分行名称（含总行）</div>
            <div field="zhmc" headerAlign="center" align="center" width="150px">支行名称（含总行部门）</div>
            <div field="gsbm" headerAlign="center" align="center" width="150px">归属部门</div>
            <div field="jydszbqc" headerAlign="center" align="center" width="150px">交易对手总部全称</div>
            <div field="jydsldjg" headerAlign="center" align="center" width="150px">交易对手落地机构</div>
            <div field="jydsdy" headerAlign="center" align="center" width="150px">交易对手地域</div>
            <div field="qxlx" headerAlign="center" align="center" width="150px">活期/定期</div>
            <div field="ywkssj" headerAlign="center" align="center" width="150px">业务开始日期</div>
            <div field="ywjssj" headerAlign="center" align="center" width="150px">业务结束日期</div>
            <div field="wbzh" headerAlign="center" align="center" width="150px">外部账号</div>
            <div field="nbzh" headerAlign="center" align="center" width="150px">内部账号</div>
            <div field="rate" headerAlign="center" align="center" width="150px">利率（%）</div>
            <div field="amt" headerAlign="center" align="center" width="150px" numberFormat="#,0.00">金额（元）</div>
            <div field="ye" headerAlign="center" align="center" width="150px" numberFormat="#,0.00">余额（元）</div>
            <div field="xe" headerAlign="center" align="center" width="150px" numberFormat="#,0.00">限额（元）</div>
            <div field="xebz" headerAlign="center" align="center" width="150px">限额备注</div>
            <div field="jzkm" headerAlign="center" align="center" width="150px">记账科目</div>
            <div field="zhxz" headerAlign="center" align="center" width="150px">账户性质</div>
            <div field="zhyt" headerAlign="center" align="center" width="150px">账户用途</div>
            <div field="zhzt" headerAlign="center" align="center" width="150px">账户状态</div>
            <div field="sfkwy" headerAlign="center" align="center" width="150px">是否开网银</div>
            <div field="lxr" headerAlign="center" align="center" width="150px">联系人</div>
            <div field="phone" headerAlign="center" align="center" width="150px">电话</div>
            <div field="beizhu" headerAlign="center" align="center" width="150px">备注</div>
            <div field="jyds" headerAlign="center" align="center" width="150px">交易对手（全称）</div>
            <div field="bfqszh" headerAlign="center" align="center" width="150px">本方清算账号(必输)</div>
            <div field="bfqshh" headerAlign="center" align="center" width="150px">本方清算行号</div>
            <div field="dfqszh" headerAlign="center" align="center" width="150px">对方清算账号(必输)</div>
            <div field="dfqshh" headerAlign="center" align="center" width="150px">对方清算行号</div>
            <div field="dealno" headerAlign="center" align="center" width="150px">交易编号</div>
            <div field="shklckzss" headerAlign="center" align="center" width="150px">是否开立存款证实书</div>
            <div field="ckzhbh" headerAlign="center" align="center" width="150px">存款账户编码</div>
        </div>
    </div>

    <div id="loan_grid3" class="mini-datagrid borderAll" style="width:100%;height:100%;"
         sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
         border="true" allowResize="true">
        <div property="columns">
            <div type="indexcolumn" headerAlign="center" align="center" width="30px">序号</div>
            <div field="fhnc" headerAlign="center" align="center" width="150px">分行名称（含总行）</div>
            <div field="zhmc" headerAlign="center" align="center" width="150px">支行名称（含总行部门）</div>
            <div field="gsbm" headerAlign="center" align="center" width="150px">归属部门</div>
            <div field="jydszbqc" headerAlign="center" align="center" width="150px">交易对手总部全称</div>
            <div field="jydsldjg" headerAlign="center" align="center" width="150px">交易对手落地机构</div>
            <div field="jydsdy" headerAlign="center" align="center" width="150px">交易对手地域</div>
            <div field="qxlx" headerAlign="center" align="center" width="150px">活期/定期</div>
            <div field="ywkssj" headerAlign="center" align="center" width="150px">业务开始日期</div>
            <div field="ywjssj" headerAlign="center" align="center" width="150px">业务结束日期</div>
            <div field="rate" headerAlign="center" align="center" width="150px">利率（%）</div>
            <div field="amt" headerAlign="center" align="center" width="150px" numberFormat="#,0.00">金额（元）</div>
            <div field="ye" headerAlign="center" align="center" width="150px" numberFormat="#,0.00">余额（元）</div>
            <div field="jzkm" headerAlign="center" align="center" width="150px">记账科目</div>
            <div field="zhxz" headerAlign="center" align="center" width="150px">账户性质</div>
            <div field="zhyt" headerAlign="center" align="center" width="150px">账户用途</div>
            <div field="zhzt" headerAlign="center" align="center" width="150px">账户状态</div>
            <div field="sfkwy" headerAlign="center" align="center" width="150px">是否开网银</div>
            <div field="lxr" headerAlign="center" align="center" width="150px">联系人/电话</div>
            <div field="beizhu" headerAlign="center" align="center" width="150px">备注</div>
            <div field="jyds" headerAlign="center" align="center" width="150px">交易对手（全称）</div>
            <div field="bfqszh" headerAlign="center" align="center" width="150px">本方清算账号(必输)</div>
            <div field="bfqshh" headerAlign="center" align="center" width="150px">本方清算行号</div>
            <div field="dfqszh" headerAlign="center" align="center" width="150px">对方清算账号(必输)</div>
            <div field="dfqshh" headerAlign="center" align="center" width="150px">对方清算行号</div>
            <div field="dealno" headerAlign="center" align="center" width="150px">交易编号</div>
            <div field="yulx" headerAlign="center" align="center" width="150px">业务类型</div>
        </div>
    </div>

    <div id="loan_grid4" class="mini-datagrid borderAll" style="width:100%;height:100%;"
         sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
         border="true" allowResize="true">
        <div property="columns">
            <div type="indexcolumn" headerAlign="center" align="center" width="30px">序号</div>
            <div field="fhnc" headerAlign="center" align="center" width="150px">分行名称（含总行）</div>
            <div field="zhmc" headerAlign="center" align="center" width="150px">支行名称（含总行部门）</div>
            <div field="gsbm" headerAlign="center" align="center" width="150px">归属部门</div>
            <div field="jydszbqc" headerAlign="center" align="center" width="150px">交易对手总部全称</div>
            <div field="jydsldjg" headerAlign="center" align="center" width="150px">交易对手落地机构</div>
            <div field="jydsdy" headerAlign="center" align="center" width="150px">交易对手地域</div>
            <div field="qxlx" headerAlign="center" align="center" width="150px">活期/定期</div>
            <div field="ywkssj" headerAlign="center" align="center" width="150px">业务开始日期</div>
            <div field="ywjssj" headerAlign="center" align="center" width="150px">业务结束日期</div>
            <div field="rate" headerAlign="center" align="center" width="150px">利率（%）</div>
            <div field="ratebz" headerAlign="center" align="center" width="150px">利率备注</div>
            <div field="amt" headerAlign="center" align="center" width="150px" numberFormat="#,0.00">金额（元）</div>
            <div field="ye" headerAlign="center" align="center" width="150px" numberFormat="#,0.00">余额（元）</div>
            <div field="jzkm" headerAlign="center" align="center" width="150px">记账科目</div>
            <div field="zhxz" headerAlign="center" align="center" width="150px">账户性质</div>
            <div field="zhyt" headerAlign="center" align="center" width="150px">账户用途</div>
            <div field="zhzt" headerAlign="center" align="center" width="150px">账户状态</div>
            <div field="sfkwy" headerAlign="center" align="center" width="150px">是否开网银</div>
            <div field="lxr" headerAlign="center" align="center" width="150px">联系人/电话</div>
            <div field="beizhu" headerAlign="center" align="center" width="150px">备注</div>
            <div field="jyds" headerAlign="center" align="center" width="150px">交易对手（全称）</div>
            <div field="bfqszh" headerAlign="center" align="center" width="150px">本方清算账号(必输)</div>
            <div field="bfqshh" headerAlign="center" align="center" width="150px">本方清算行号</div>
            <div field="dfqszh" headerAlign="center" align="center" width="150px">对方清算账号(必输)</div>
            <div field="dfqshh" headerAlign="center" align="center" width="150px">对方清算行号</div>
            <div field="dealno" headerAlign="center" align="center" width="150px">交易编号</div>
            <div field="yulx" headerAlign="center" align="center" width="150px">业务类型</div>
        </div>
    </div>

    <div id="loan_grid5" class="mini-datagrid borderAll" style="width:100%;height:100%;"
         sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
         border="true" allowResize="true">
        <div property="columns">
            <div type="indexcolumn" headerAlign="center" align="center" width="30px">序号</div>
            <div field="fhnc" headerAlign="center" align="center" width="150px">分行名称（含总行）</div>
            <div field="zhmc" headerAlign="center" align="center" width="150px">支行名称（含总行部门）</div>
            <div field="gsbm" headerAlign="center" align="center" width="150px">归属部门</div>
            <div field="jydszbqc" headerAlign="center" align="center" width="150px">交易对手总部全称</div>
            <div field="jydsldjg" headerAlign="center" align="center" width="150px">交易对手落地机构</div>
            <div field="jydsdy" headerAlign="center" align="center" width="150px">交易对手地域</div>
            <div field="qxlx" headerAlign="center" align="center" width="150px">活期/定期</div>
            <div field="ywkssj" headerAlign="center" align="center" width="150px">业务开始日期</div>
            <div field="ywjssj" headerAlign="center" align="center" width="150px">业务结束日期</div>
            <div field="rate" headerAlign="center" align="center" width="150px">利率（%）</div>
            <div field="ccy" headerAlign="center" align="center" width="150px">币种</div>
            <div field="amt" headerAlign="center" align="center" width="150px" numberFormat="#,0.00">原币金额 （元）</div>
            <div field="hl" headerAlign="center" align="center" width="150px">汇率</div>
            <div field="cnyamt" headerAlign="center" align="center" width="150px" numberFormat="#,0.00">折人民币金额（元）</div>
            <div field="jzkm" headerAlign="center" align="center" width="150px">记账科目</div>
            <div field="zhxz" headerAlign="center" align="center" width="150px">账户性质</div>
            <div field="zhyt" headerAlign="center" align="center" width="150px">账户用途</div>
            <div field="zhzt" headerAlign="center" align="center" width="150px">账户状态</div>
            <div field="sfkwy" headerAlign="center" align="center" width="150px">是否开网银</div>
            <div field="lxr" headerAlign="center" align="center" width="150px">联系人/电话</div>
            <div field="beizhu" headerAlign="center" align="center" width="150px">备注</div>
            <div field="ymhl" headerAlign="center" align="center" width="150px">月末汇率(查询日汇率中间价)</div>
            <div field="ybye" headerAlign="center" align="center" width="150px">原币余额</div>
            <div field="rmbye" headerAlign="center" align="center" width="150px">折人民币余额</div>
        </div>
    </div>

    <div id="loan_grid6" class="mini-datagrid borderAll" style="width:100%;height:100%;"
         sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
         border="true" allowResize="true">
        <div property="columns">
            <div type="indexcolumn" headerAlign="center" align="center" width="30px">序号</div>
            <div field="fhnc" headerAlign="center" align="center" width="150px">分行名称（含总行）</div>
            <div field="zhmc" headerAlign="center" align="center" width="150px">支行名称（含总行部门）</div>
            <div field="gsbm" headerAlign="center" align="center" width="150px">归属部门</div>
            <div field="jydszbqc" headerAlign="center" align="center" width="150px">交易对手总部全称</div>
            <div field="jydsldjg" headerAlign="center" align="center" width="150px">交易对手落地机构</div>
            <div field="jydsdy" headerAlign="center" align="center" width="150px">交易对手地域</div>
            <div field="qxlx" headerAlign="center" align="center" width="150px">活期/定期</div>
            <div field="ywkssj" headerAlign="center" align="center" width="150px">业务开始日期</div>
            <div field="ywjssj" headerAlign="center" align="center" width="150px">业务结束日期</div>
            <div field="rate" headerAlign="center" align="center" width="150px">利率（%）</div>
            <div field="ratebz" headerAlign="center" align="center" width="150px">利率备注</div>
            <div field="ccy" headerAlign="center" align="center" width="150px">币种</div>
            <div field="amt" headerAlign="center" align="center" width="150px" numberFormat="#,0.00">原币金额 （元）</div>
            <div field="hl" headerAlign="center" align="center" width="150px">汇率</div>
            <div field="cnyamt" headerAlign="center" align="center" width="150px" numberFormat="#,0.00">折人民币金额（元）</div>
            <div field="jzkm" headerAlign="center" align="center" width="150px">记账科目</div>
            <div field="zhxz" headerAlign="center" align="center" width="150px">账户性质</div>
            <div field="zhyt" headerAlign="center" align="center" width="150px">账户用途</div>
            <div field="zhzt" headerAlign="center" align="center" width="150px">账户状态</div>
            <div field="sfkwy" headerAlign="center" align="center" width="150px">是否开网银</div>
            <div field="lxr" headerAlign="center" align="center" width="150px">联系人/电话</div>
            <div field="beizhu" headerAlign="center" align="center" width="150px">备注</div>
            <div field="ymhl" headerAlign="center" align="center" width="150px">月末汇率(查询日汇率中间价)</div>
            <div field="ybye" headerAlign="center" align="center" width="150px">原币余额</div>
            <div field="rmbye" headerAlign="center" align="center" width="150px">折人民币余额</div>
        </div>
    </div>


    <div id="loan_grid7" class="mini-datagrid borderAll" style="width:100%;height:100%;"
         sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
         border="true" allowResize="true">
        <div property="columns">
            <div type="indexcolumn" headerAlign="center" align="center" width="30px">序号</div>
            <div field="fhnc" headerAlign="center" align="center" width="150px">分行名称（含总行）</div>
            <div field="zhmc" headerAlign="center" align="center" width="150px">支行名称（含总行部门）</div>
            <div field="gsbm" headerAlign="center" align="center" width="150px">归属部门</div>
            <div field="jydszbqc" headerAlign="center" align="center" width="150px">交易对手总部全称</div>
            <div field="jydsldjg" headerAlign="center" align="center" width="150px">交易对手落地机构</div>
            <div field="jydsdy" headerAlign="center" align="center" width="150px">交易对手地域</div>
            <div field="qxlx" headerAlign="center" align="center" width="150px">活期/定期</div>
            <div field="ywkssj" headerAlign="center" align="center" width="150px">业务开始日期</div>
            <div field="ywjssj" headerAlign="center" align="center" width="150px">业务结束日期</div>
            <div field="rate" headerAlign="center" align="center" width="150px">利率（%）</div>
            <div field="ccy" headerAlign="center" align="center" width="150px">币种</div>
            <div field="amt" headerAlign="center" align="center" width="150px" numberFormat="#,0.00">原币金额 （元）</div>
            <div field="hl" headerAlign="center" align="center" width="150px">汇率</div>
            <div field="cnyamt" headerAlign="center" align="center" width="150px" numberFormat="#,0.00">折人民币金额（元）</div>
            <div field="jzkm" headerAlign="center" align="center" width="150px">记账科目</div>
            <div field="zhxz" headerAlign="center" align="center" width="150px">账户性质</div>
            <div field="zhyt" headerAlign="center" align="center" width="150px">账户用途</div>
            <div field="zhzt" headerAlign="center" align="center" width="150px">账户状态</div>
            <div field="sfkwy" headerAlign="center" align="center" width="150px">是否开网银</div>
            <div field="lxr" headerAlign="center" align="center" width="150px">联系人/电话</div>
            <div field="beizhu" headerAlign="center" align="center" width="150px">备注</div>
            <div field="ymhl" headerAlign="center" align="center" width="150px">月末汇率(查询日汇率中间价)</div>
            <div field="ybye" headerAlign="center" align="center" width="150px">原币余额</div>
            <div field="rmbye" headerAlign="center" align="center" width="150px">折人民币余额</div>
            <div field="yulx" headerAlign="center" align="center" width="150px">业务类型</div>
            <div field="bfqszh" headerAlign="center" align="center" width="150px">本方清算账号(必输)</div>
            <div field="bfqshh" headerAlign="center" align="center" width="150px">本方清算行号</div>
            <div field="dfqszh" headerAlign="center" align="center" width="150px">对方清算账号(必输)</div>
            <div field="dfqshh" headerAlign="center" align="center" width="150px">对方清算行号</div>
            <div field="dealno" headerAlign="center" align="center" width="150px">交易编号</div>
        </div>
    </div>


    <div id="loan_grid8" class="mini-datagrid borderAll" style="width:100%;height:100%;"
         sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
         border="true" allowResize="true">
        <div property="columns">
            <div type="indexcolumn" headerAlign="center" align="center" width="30px">序号</div>
            <div field="fhnc" headerAlign="center" align="center" width="150px">分行名称（含总行）</div>
            <div field="zhmc" headerAlign="center" align="center" width="150px">支行名称（含总行部门）</div>
            <div field="gsbm" headerAlign="center" align="center" width="150px">归属部门</div>
            <div field="jydszbqc" headerAlign="center" align="center" width="150px">交易对手总部全称</div>
            <div field="jydsldjg" headerAlign="center" align="center" width="150px">交易对手落地机构</div>
            <div field="jydsdy" headerAlign="center" align="center" width="150px">交易对手地域</div>
            <div field="qxlx" headerAlign="center" align="center" width="150px">活期/定期</div>
            <div field="ywkssj" headerAlign="center" align="center" width="150px">业务开始日期</div>
            <div field="ywjssj" headerAlign="center" align="center" width="150px">业务结束日期</div>
            <div field="rate" headerAlign="center" align="center" width="150px">利率（%）</div>
            <div field="ccy" headerAlign="center" align="center" width="150px">币种</div>
            <div field="amt" headerAlign="center" align="center" width="150px" numberFormat="#,0.00">原币金额 （元）</div>
            <div field="hl" headerAlign="center" align="center" width="150px">汇率</div>
            <div field="cnyamt" headerAlign="center" align="center" width="150px" numberFormat="#,0.00">折人民币金额（元）</div>
            <div field="jzkm" headerAlign="center" align="center" width="150px">记账科目</div>
            <div field="zhxz" headerAlign="center" align="center" width="150px">账户性质</div>
            <div field="zhyt" headerAlign="center" align="center" width="150px">账户用途</div>
            <div field="zhzt" headerAlign="center" align="center" width="150px">账户状态</div>
            <div field="sfkwy" headerAlign="center" align="center" width="150px">是否开网银</div>
            <div field="lxr" headerAlign="center" align="center" width="150px">联系人/电话</div>
            <div field="beizhu" headerAlign="center" align="center" width="150px">备注</div>
            <div field="ymhl" headerAlign="center" align="center" width="150px">月末汇率(查询日汇率中间价)</div>
            <div field="ybye" headerAlign="center" align="center" width="150px">原币余额</div>
            <div field="rmbye" headerAlign="center" align="center" width="150px">折人民币余额</div>
            <div field="yulx" headerAlign="center" align="center" width="150px">业务类型</div>
            <div field="bfqszh" headerAlign="center" align="center" width="150px">本方清算账号(必输)</div>
            <div field="bfqshh" headerAlign="center" align="center" width="150px">本方清算行号</div>
            <div field="dfqszh" headerAlign="center" align="center" width="150px">对方清算账号(必输)</div>
            <div field="dfqshh" headerAlign="center" align="center" width="150px">对方清算行号</div>
            <div field="dealno" headerAlign="center" align="center" width="150px">交易编号</div>
        </div>
    </div>


</div>
<script>

    mini.parse();

    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var id = params.id;
    var implClass = params.implClass;

    var grid1 = mini.get("loan_grid1");
    var grid2 = mini.get("loan_grid2");
    var grid3 = mini.get("loan_grid3");
    var grid4 = mini.get("loan_grid4");
    var grid5 = mini.get("loan_grid5");
    var grid6 = mini.get("loan_grid6");
    var grid7 = mini.get("loan_grid7");
    var grid8 = mini.get("loan_grid8");

    var form = new mini.Form("#search_form");

    grid1.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize, pageIndex);
    });
    grid2.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize, pageIndex);
    });
    grid3.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize, pageIndex);
    });
    grid4.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize, pageIndex);
    });
    grid5.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize, pageIndex);
    });
    grid6.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize, pageIndex);
    });
    grid7.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize, pageIndex);
    });
    grid8.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize, pageIndex);
    });

    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function (visibleBtn) {
            query();
        });
    });

    //页面隐藏展示以及塞值
    function showTable(e, data, pageSize, pageIndex) {
        grid1.hide();
        grid2.hide();
        grid3.hide();
        grid4.hide();
        grid5.hide();
        grid6.hide();
        grid7.hide();
        grid8.hide();
        e.show();
        e.setTotalCount(data.obj.total);
        e.setPageIndex(pageIndex);
        e.setPageSize(pageSize);
        e.setData(data.obj.rows);
    }

    //查询按钮
    function query() {
        search(10, 0);
    }

    function search(pageSize, pageIndex) {
        var dealtype = mini.get("dealtype").getValue();
        var searchUrl = "";

        if (dealtype == 'BTYCF') {
            //本币-同业存放

            searchUrl = "/TheForeignCurrencyFinancialController/call1";
        }
        if (dealtype == 'BCFTY') {
            //本币-存放同业

            searchUrl = "/TheForeignCurrencyFinancialController/call2";
        }


        if (dealtype == 'BTYCJ') {
            //本币-同业拆入
            searchUrl = "/TheForeignCurrencyFinancialController/call3";

        }
        if (dealtype == 'BTYCC') {
            //本币-同业拆出
            searchUrl = "/TheForeignCurrencyFinancialController/call4";
        }
        if (dealtype == 'WTYCF') {
            //外币-同业存放
            searchUrl = "/TheForeignCurrencyFinancialController/call5";
        }
        if (dealtype == 'WCFTY') {
            //外币-存放同业
            searchUrl = "/TheForeignCurrencyFinancialController/call6";
        }
        if (dealtype == 'WTYCR') {
            //外币-同业拆入
            searchUrl = "/TheForeignCurrencyFinancialController/call7";
        }
        if (dealtype == 'WCFTY2') {
            //外币-拆放同业

            searchUrl = "/TheForeignCurrencyFinancialController/call8";
        }

        var data = form.getData(true);
        data['pageNum'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        data['sheetType'] = dealtype;
        data['sheetTypeValue'] = dealtype;
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: searchUrl,
            data: params,
            callback: function (data) {
                if (dealtype == 'BTYCJ') {
                    //本币-同业拆入
                    showTable(grid3, data, pageSize, pageIndex);

                }
                if (dealtype == 'BTYCC') {
                    //本币-同业拆出
                    showTable(grid4, data, pageSize, pageIndex);
                }
                if (dealtype == 'WTYCF') {
                    //外币-同业存放
                    showTable(grid5, data, pageSize, pageIndex);
                }
                if (dealtype == 'WCFTY') {
                    //外币-存放同业
                    showTable(grid6, data, pageSize, pageIndex);
                }
                if (dealtype == 'WTYCR') {
                    //外币-同业拆入
                    showTable(grid7, data, pageSize, pageIndex);
                }
                if (dealtype == 'WCFTY2') {
                    //外币-同业拆入

                    showTable(grid8, data, pageSize, pageIndex);
                }

                if (dealtype == 'BTYCF') {
                    //本币-同业存放

                    showTable(grid1, data, pageSize, pageIndex);
                }
                if (dealtype == 'BCFTY') {
                    //本币-存放同业

                    showTable(grid2, data, pageSize, pageIndex);
                }

            }
        });
    }

    // //清空按钮
    // function clear() {
    //     form.clear();
    //     query();
    // }

    function exportExcel() {
        var dealtype = mini.get("dealtype").getValue();
        var data = form.getData(true);
        var fields = null;
        var fields = "<input type='hidden' id='queryDate' name='queryDate' value='" + data.queryDate + "'>";
        fields += "<input type='hidden' id='id' name='id' value='" + id + "'>";
        fields += "<input type='hidden' id='sheetType' name='sheetType' value='" + dealtype + "'>";
        fields += "<input type='hidden' id='sheetTypeValue' name='sheetTypeValue' value='" + dealtype + "'>";
        exportHrbExcel(fields);
    }

</script>
</body>
</html>
