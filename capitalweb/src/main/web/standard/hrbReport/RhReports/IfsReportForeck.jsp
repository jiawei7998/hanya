<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../../global.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
    <title>外汇敞口情况表</title>
</head>
<body>
<fieldset class="mini-fieldset title">
    <div id="search_form" style="width:100%">
        <legend>报表查询</legend>
        <input id="queryDate" name="queryDate" class="mini-datepicker" labelField="true" allowInput="false"
               label="查询日期:"
               value="<%=__bizDate%>" labelSyle="text-align:right;" emptyText="请输入"/>
        <input id="ccytype" name="ccytype" class="mini-combobox"  data="CommonUtil.serverData.dictionary.ccytype" value="CNY" emptyText="请选择币种"  label="币种：" width="280px" labelField="true" labelStyle="text-align:right;"/>
        <input id="unit" name="unit" class="mini-combobox"  data="CommonUtil.serverData.dictionary.unit" value="1" emptyText="请选择单位"  label="单位：" width="280px" labelField="true" labelStyle="text-align:right;"/>
        <span>
                <a id="send_btn" class="mini-button" style="display: none" onclick="query()">生成数据</a>
                <a id="export_btn" class="mini-button" style="display: none" onclick="exportExcel()">导出报表</a>
            </span>
    </div>
</fieldset>
<div class="mini-fit" style="height:100%;">
    <div id="slcd_grid" class="mini-datagrid borderAll" style="height:100%;"
         sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
         border="true" allowResize="true" showPager="false">
        <div property="columns" autoEscape="true">
            <div type="indexcolumn" headerAlign="center" width="40">序号</div>
            <div field="product" name="product"  width="180" align="center" headerAlign="center" allowSort="false" >业务类型</div>
            <div field="bz" width="150" align="center" headerAlign="center" allowSort="false" >币种</div>
            <div header="即期" headerAlign="center">
                <div property="columns">
                    <div field="gqdt"  width="150" align="center" headerAlign="center" allowSort="false" numberFormat="#,0.00">多头</div>
                    <div field="gqkt" width="150" align="center" headerAlign="center" allowSort="false" numberFormat="#,0.00">空头</div>
                </div>
            </div>
            <div header="远期" headerAlign="center">
                <div property="columns">
                    <div field="yqdt"  width="150" align="center" headerAlign="center" allowSort="false" numberFormat="#,0.00">多头</div>
                    <div field="yqkt" width="150" align="center" headerAlign="center" allowSort="false" numberFormat="#,0.00">空头</div>
                </div>
            </div>
            <div field="jck" width="150" align="center" headerAlign="center" allowSort="false" numberFormat="#,0.00">净敞口</div>
            <div field="zjck" width="150" align="center" headerAlign="center" allowSort="false" numberFormat="#,0.00">总净敞口</div>
        </div>
    </div>
</div>
<script>

    mini.parse();
    var grid = mini.get("slcd_grid");
    var form = new mini.Form("#search_form");
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var id = params.id;

    /**时间格式化**/
    function onDateRenderer(e) {
        var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value) == null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g, '/'));
        if (value) return mini.formatDate(value, 'yyyy/MM/dd');
    }

    $(document).ready(function() {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(currTab.parentId)).done(function(visibleBtn) {
        });
    });
    //生成数据
    function query(){
        queryPage();
    }
    //生成数据
    function queryPage() {
        debugger;
        var searchUrl = "/CombinePositionReportController/searchIfsReportForeck";
        var data = form.getData(true);
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: searchUrl,
            data: params,
            callback: function(data) {
                grid.setData(data.obj);
                grid.mergeColumns(["product"]);
            }
        });
    }

    //清空按钮
    function clear() {
        form.clear();
    }

    //导出
    function exportExcel(){
        var content = grid.getData();

        if(content.length === 0){
            mini.alert("请先生成数据!");
            return;
        }

        var data = form.getData(true);
        var textValue = mini.get("unit").getText();
        console.log(textValue);
        var fields = null;
        var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
        fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
        fields += "<input type='hidden' id='id' name='id' value='"+data.ccytype+"'>";
        fields += "<input type='hidden' id='id' name='id' value='"+textValue+"'>";
        exportHrbExcel(fields);
    }

</script>
</body>
</html>
