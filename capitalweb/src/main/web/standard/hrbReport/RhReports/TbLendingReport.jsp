<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../../global.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
    <title>拆借交易台账</title>
</head>
<body>
<fieldset class="mini-fieldset title">
    <div id="search_form" style="width:100%">
        <legend>报表查询</legend>
        <nobr>
            <input id="startDate" name="startDate" class="mini-datepicker mini-mustFill" labelField="true" label="日期：" required="true"
                   ondrawdate="onDrawDateStart" labelStyle="text-align:right;" emptyText="起始日期" format="yyyy-MM-dd" value="<%=__bizDate%>"/>
            <span>至</span>
            <input id="endDate" name="endDate" class="mini-datepicker  mini-mustFill" required="true"
                   ondrawdate="onDrawDateEnd" emptyText="结束日期" format="yyyy-MM-dd" value="<%=__bizDate%>"/>
        </nobr>
        <input id="type" name="type" class="mini-combobox"  data="CommonUtil.serverData.dictionary.DldtType" value="CC" emptyText="请选择拆借类型"  label="拆借类型：" width="280px" labelField="true" labelStyle="text-align:right;"/>
        <input id="ccy" name="ccy" class="mini-combobox"  data="CommonUtil.serverData.dictionary.Currency" value="CNY" emptyText="请选择币种"  label="币种：" width="280px" labelField="true" labelStyle="text-align:right;"/>
        <input id="br" name="br" class="mini-combobox"  data="CommonUtil.serverData.dictionary.BrType" value="01" emptyText="请选择分行"  label="分行：" width="280px" labelField="true" labelStyle="text-align:right;"/>
        <span>
                <a id="send_btn" class="mini-button" style="display: none" onclick="query()">生成数据</a>
                <a id="export_btn" class="mini-button" style="display: none" onclick="exportExcel()">导出报表</a>
            </span>
    </div>
</fieldset>
<div class="mini-fit" style="height:100%;">
    <div id="slcd_grid" class="mini-datagrid borderAll" style="height:100%;"
         sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
         border="true" allowResize="true" showPager="true">
        <div property="columns" autoEscape="true">
            <div type="indexcolumn" headerAlign="center" width="40">序号</div>
            <div field="dealno"  width="120" align="center" headerAlign="center" allowSort="false">成交单号</div>
            <div field="costdesc" width="180" align="center" headerAlign="center" allowSort="false">成本中心</div>
            <div field="cname"  width="240" align="center" headerAlign="center" allowSort="false">对手方</div>
            <div field="acctdesc"  width="200" align="center" headerAlign="center" allowSort="false">对手属性</div>
            <div field="accttype" width="120" align="center" headerAlign="center" allowSort="false">账户类型</div>
            <div field="descr"  width="180" align="center" headerAlign="center" allowSort="false">拆借(回购)方式</div>
            <%--			<div field="ccy" name="ccy" width="100" align="center" headerAlign="center" allowSort="false">币种</div>--%>
            <div field="vdate"  width="120" align="center" headerAlign="center" allowSort="false" renderer="onDateRenderer">首期交割日</div>
            <div field="term"  width="80" align="center" headerAlign="center" allowSort="false">期限</div>
            <div field="mdate" width="120" align="center" headerAlign="center" allowSort="false" renderer="onDateRenderer">到期交割日</div>

            <div field="intrate" width="120" align="right" headerAlign="center" allowSort="false" numberFormat="#,0.00">利率（%）</div>
            <div field="ccyamtabs"  width="120" align="right" headerAlign="center" allowSort="false" numberFormat="#,0.00">券面（拆借）总额</div>
            <div field="ccyamt" width="120" align="right" headerAlign="center" allowSort="false" numberFormat="#,0.00">首期结算额</div>
            <div field="totpayamt"  width="120" align="right" headerAlign="center" allowSort="false" numberFormat="#,0.00">到期结算额</div>
            <div field="totalint"  width="120" align="right" headerAlign="center" allowSort="false" numberFormat="#,0.00">利息支额</div>
            <div field="occupyamt" width="180" align="right" headerAlign="center" allowSort="false" numberFormat="#,0.00">资金占用</div>
            <div field="handamt"  width="120" align="right" headerAlign="center" allowSort="false" numberFormat="#,0.00">交易手续费</div>
            <div field="overterm" width="80" align="center" headerAlign="center" allowSort="false">剩余期限</div>
            <div field="rplint"  width="120" align="right" headerAlign="center" allowSort="false" numberFormat="#,0.00">应收（付）利息额</div>
            <div field="overlint" width="120" align="right" headerAlign="center" allowSort="false" numberFormat="#,0.00">剩余利息额</div>
            <div field="proloss"  width="120" align="right" headerAlign="center" allowSort="false" numberFormat="#,0.00">已实现损益</div>
        </div>
    </div>
</div>
<script>

    mini.parse();
    var grid = mini.get("slcd_grid");
    var form = new mini.Form("#search_form");
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var id = params.id;
    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize, pageIndex);
    });

    /**时间格式化**/
    function onDateRenderer(e) {
        var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value) == null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g, '/'));
        if (value) return mini.formatDate(value, 'yyyy-MM-dd');
    }

    //交易日期
    function onDrawDateStart(e) {
        var startDate = e.date;
        var endDate= mini.get("endDate").getValue();
        if(CommonUtil.isNull(endDate)){
            return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }
    function onDrawDateEnd(e) {
        var endDate = e.date;
        var startDate = mini.get("startDate").getValue();
        if(CommonUtil.isNull(startDate)){
            return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }

    $(document).ready(function() {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(currTab.parentId)).done(function(visibleBtn) {
        });
    });

    //生成数据
    function query(){
        search(10,0);
    }
    //生成数据
    function search(pageSize,pageIndex) {
        var searchUrl = "/CombinePositionReportController/searchTbLending";

        var data = form.getData(true);
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        var params = mini.encode(data);

        CommonUtil.ajax({
            url: searchUrl,
            data: params,
            callback: function (data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }

    //导出
    function exportExcel(){
        var content = grid.getData();

        if(content.length === 0){
            mini.alert("请先生成数据!");
            return;
        }

        var data = form.getData(true);
        var fields = null;
        var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.endDate+"'>";
        fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
        exportHrbExcel(fields);
    }
</script>
</body>
</html>
