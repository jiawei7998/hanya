<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../../global.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
    <title>本币交易情况表</title>
</head>
<body>
<fieldset class="mini-fieldset title">
    <div id="search_form" style="width:100%">
        <legend>报表查询</legend>
        <nobr>
            <input id="sdate" name="sdate" class="mini-datepicker mini-mustFill" labelField="true" label="日期：" required="true"
                   ondrawdate="onDrawDateStart" labelStyle="text-align:right;" emptyText="起始日期" format="yyyy-MM-dd" value="<%=__bizYesterdayDate%>"/>
            <span>至</span>
            <input id="edate" name="edate" class="mini-datepicker  mini-mustFill" required="true"
                   ondrawdate="onDrawDateEnd" emptyText="结束日期" format="yyyy-MM-dd" value="<%=__bizYesterdayDate%>"/>
        </nobr>
        <input id="trad" name="trad" class="mini-combobox"  data="CommonUtil.serverData.dictionary.trad" value="3" emptyText="请选择账薄类型"  label="账簿类型：" width="280px" labelField="true" labelStyle="text-align:right;"/>
        <span>
                <a id="send_btn" class="mini-button" style="display: none" onclick="query()">生成数据</a>
                <a id="export_btn" class="mini-button" style="display: none" onclick="exportExcel()">导出报表</a>
            </span>
    </div>
</fieldset>
<div class="mini-fit" style="height:100%;">
    <div id="slcd_grid" class="mini-datagrid borderAll" style="width:1600px;height:100%;"
         sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
         border="true" allowResize="true" showPager="true">
        <div property="columns" autoEscape="true">
            <div field="accountType" headerAlign="center" align="center" width="120px">会计账户类型</div>
            <div field="businessType" headerAlign="center" align="center" width="120px" >业务类型</div>
            <div field="combinationType" headerAlign="center" align="center" width="120px" >组合类型</div>
            <div field="dealType" headerAlign="center" align="center" width="150px" >交易类型</div>
            <div field="dealdate" headerAlign="center" align="center" width="100px" renderer="onDateRenderer">交割日期</div>
            <div field="buyCount" headerAlign="center" align="center" width="100px" >买入交易笔数</div>
            <div field="sellCount" headerAlign="center" align="center" width="100px" >卖出交易笔数</div>
            <div field="dealCount" headerAlign="center" align="center" width="100px" >总交易笔数</div>
            <div field="buyThanTen" headerAlign="center" align="center" width="180px" >单笔大于10亿小于等于15亿</div>
            <div field="buyThanFifteen" headerAlign="center" align="center" width="100px" >大于15亿</div>
            <div field="buyTotal" headerAlign="center" align="right" width="120px"  numberFormat="#,0.0000">买入交易量(万元)</div>
            <div field="sellTotal" headerAlign="center" align="right" width="120px"  numberFormat="#,0.0000">卖出交易量(万元)</div>
            <div field="total" headerAlign="center" align="right" width="120px"  numberFormat="#,0.0000">总交易量(万元)</div>
        </div>
    </div>
</div>
<script>

    mini.parse();
    var grid = mini.get("slcd_grid");
    var form = new mini.Form("#search_form");
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var id = params.id;

    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        queryPage(pageSize, pageIndex);
    });

    $(document).ready(function() {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(currTab.parentId)).done(function(visibleBtn) {

        });
    });
    //交易日期
    function onDrawDateStart(e) {
        var startDate = e.date;
        var endDate= mini.get("edate").getValue();
        if(CommonUtil.isNull(endDate)){
            return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }
    function onDrawDateEnd(e) {
        var endDate = e.date;
        var startDate = mini.get("sdate").getValue();
        if(CommonUtil.isNull(startDate)){
            return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }

    //生成数据
    function query(){
        queryPage(10,0);
    }
    //生成数据
    function queryPage(pageSize,pageIndex) {
        debugger;
        var searchUrl = "/CombinePositionReportController/searchTbRmbDeal";

        var data = form.getData(true);
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        var params = mini.encode(data);

        CommonUtil.ajax({
            url: searchUrl,
            data: params,
            callback: function (data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }

    //导出
    function exportExcel(){
        var content = grid.getData();

        if(content.length === 0){
            mini.alert("请先生成数据!");
            return;
        }

        var data = form.getData(true);
        var fields = null;
        var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.edate+"'>";
        fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
        fields += "<input type='hidden' id='sdate' name='sdate' value='"+data.sdate+"'>";
        exportHrbExcel(fields);
    }


</script>
</body>
</html>
