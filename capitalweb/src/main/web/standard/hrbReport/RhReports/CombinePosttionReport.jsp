<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
    <title>合并持仓业务明细</title>
</head>
<body>
<fieldset class="mini-fieldset title">
    <div id="search_form" style="width:100%">
        <legend>报表查询</legend>
        <input id="queryMonth" name="queryMonth" class="mini-monthpicker" labelField="true" allowInput="false"
               label="查询年月:"
               value="<%=__bizDate%>" labelSyle="text-align:right;"  allowInput="false" />
        <input id="dealtype" name="dealtype"  class="mini-combobox" data="CommonUtil.serverData.dictionary.busHBCCType" labelField="true"
               label="投资类型:" labelSyle="text-align:right;" width="300px" emptyText="请输入" />
        <span>
                <a class="mini-button" style="display: none"  id="search_btn" onclick="query()">查询</a>
                <a class="mini-button" style="display: none"  id="clear_btn" onclick="clear()">清空</a>
                <a id="export_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
            </span>
    </div>
</fieldset>
<div class="mini-fit" style="width:100%;height:100%;">
    <span style="color: red">上列表为新增，下列表为到期（卖出/赎回）</span>
    <div id="bondpay_grid" class="mini-datagrid borderAll" style="height:50%;"
         sortMode="client" allowAlternating="true" border="true" allowResize="true" onrowdbclick="onRowDblClick">
        <div property="columns">
            <div type="indexcolumn" headerAlign="center" align="center" width="50px">序号</div>
            <div field="bustype" headerAlign="center" align="center" width="150px">账户类型</div>
            <div field="acctdesc" headerAlign="center" align="center" width="120px">债券类型</div>
            <div field="issuer" headerAlign="center" align="center" width="120px">债券发行人</div>
            <div field="secid" headerAlign="center" align="center" width="100px">债券代码</div>
            <div field="descr" headerAlign="center" align="center" width="120px">债券名称</div>
            <div name="port" field="port" headerAlign="center" align="center" width="120px">投资组合</div>
            <div field="vdate" headerAlign="center" align="center" width="100px" renderer="onDateRenderer">起息日</div>
            <div field="teno" headerAlign="center" align="center" width="80px" >期限</div>
            <div field="mdate" headerAlign="center" align="center" width="100px" renderer="onDateRenderer">到期日</div>
            <div name="faceamt" field="faceamt" headerAlign="center" align="center" width="150px" numberFormat="#,0.00">债券面值</div>
            <div name="couprate" field="couprate" headerAlign="center" align="center" width="80px" numberFormat="#,0.00">票面利率</div>
            <div name="settamt" field="settamt" headerAlign="center" align="center" width="150px" numberFormat="#,0.00">账面价值（元）</div>
        </div>
    </div>
    <div id="bondsell_grid" class="mini-datagrid borderAll" style="height:50%;" sortMode="client" allowAlternating="true"
         border="true" allowResize="true">
        <div property="columns">
            <div type="indexcolumn" headerAlign="center" align="center" width="50px">序号</div>
            <div field="bustype" headerAlign="center" align="center" width="150px">账户类型</div>
            <div field="acctdesc" headerAlign="center" align="center" width="120px">债券类型</div>
            <div field="issuer" headerAlign="center" align="center" width="120px">债券发行人</div>
            <div field="secid" headerAlign="center" align="center" width="100px">债券代码</div>
            <div field="descr" headerAlign="center" align="center" width="120px">债券名称</div>
            <div name="port" field="port" headerAlign="center" align="center" width="120px">投资组合</div>
            <div field="vdate" headerAlign="center" align="center" width="100px" renderer="onDateRenderer">起息日</div>
            <div field="teno" headerAlign="center" align="center" width="80px" >期限</div>
            <div field="mdate" headerAlign="center" align="center" width="100px" renderer="onDateRenderer">到期日</div>
            <div name="faceamt" field="faceAmt" headerAlign="center" align="center" width="150px" numberFormat="#,0.00">债券面值</div>
            <div name="couprate" field="coupRate" headerAlign="center" align="center" width="80px" numberFormat="#,0.00">票面利率</div>
            <div name="settamt" field="settAmt" headerAlign="center" align="center" width="150px" numberFormat="#,0.00">账面价值（元）</div>
        </div>
    </div>
    <div id="fundpay_grid" class="mini-datagrid borderAll" style="height:50%;"
         sortMode="client" allowAlternating="true" border="true" allowResize="true" onrowdbclick="onRowDblClick">
        <div property="columns">
            <div type="indexcolumn" headerAlign="center" align="center" width="50px">序号</div>
            <div field="issuer" headerAlign="center" align="center" width="150px">基金管理人</div>
            <div field="descr" headerAlign="center" align="center" width="150px">基金简称</div>
            <div field="secid" headerAlign="center" align="center" width="150px">基金代码</div>
            <div field="bustype" headerAlign="center" align="center" width="100px">基金方向</div>
            <div field="settamt" headerAlign="center" align="center" width="150px" numberFormat="#,0.00">交易金额</div>
            <div field="vdate" headerAlign="center" align="center" width="150px" renderer="onDateRenderer">申购日</div>
            <div field="port" headerAlign="center" align="center" width="150px" renderer="CommonUtil.dictRenderer"
                 data-options="{dict:'FundType'}" >基金类型</div>
        </div>
    </div>
    <div id="fundsell_grid" class="mini-datagrid borderAll" style="height:50%;" sortMode="client" allowAlternating="true"
         border="true" allowResize="true">
        <div property="columns">
            <div type="indexcolumn" headerAlign="center" align="center" width="50px">序号</div>
            <div field="issuer" headerAlign="center" align="center" width="150px">基金管理人</div>
            <div field="descr" headerAlign="center" align="center" width="150px">基金简称</div>
            <div field="secid" headerAlign="center" align="center" width="150px">基金代码</div>
            <div field="bustype" headerAlign="center" align="center" width="100px">基金方向</div>
            <div field="settamt" headerAlign="center" align="center" width="150px" numberFormat="#,0.00">交易金额</div>
            <div field="vdate" headerAlign="center" align="center" width="150px" renderer="onDateRenderer">申购日</div>
            <div field="port" headerAlign="center" align="center" width="150px" renderer="CommonUtil.dictRenderer"
                 data-options="{dict:'FundType'}">基金类型</div>
        </div>
    </div>
</div>
<script>

    mini.parse();
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var id = params.id;
    var bondpaygrid = mini.get("bondpay_grid");
    var bondsellgrid = mini.get("bondsell_grid");
    var fundpaygrid = mini.get("fundpay_grid");
    var fundsellgrid = mini.get("fundsell_grid");

    var form = new mini.Form("#search_form");
    /**分页**/
    bondpaygrid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        queryPage(pageSize,pageIndex,bondpaygrid);
    });

    bondsellgrid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        queryPage(pageSize,pageIndex,bondsellgrid);
    });
    fundpaygrid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        queryPage(pageSize,pageIndex,fundpaygrid);
    });
    fundsellgrid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        queryPage(pageSize,pageIndex,fundsellgrid);
    });

    $(document).ready(function() {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
            clear();
        });
    });
    /**时间格式化**/
    function onDateRenderer(e) {
        var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value) == null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g, '/'));
        if (value) return mini.formatDate(value, 'yyyy-MM-dd');
    }

    //查询按钮
    function query(){
        var dealtype = mini.get("dealtype").value;
        var queryMonth= mini.get("queryMonth").value;
        if (dealtype.length == 0||queryMonth.length==0){
            mini.alert("请填写查询条件!")
        }else{
            if (dealtype == 'A'||dealtype == 'T'||dealtype == 'H'||dealtype == 'I'){
                bondpaygrid.show();
                bondsellgrid.show();
                fundpaygrid.hide();
                fundsellgrid.hide();
                selectstyle(dealtype);
                queryPage(10,0,bondpaygrid);
                queryPage(10,0,bondsellgrid);
            }else if (dealtype=='fund'){
                bondpaygrid.hide();
                bondsellgrid.hide();
                fundpaygrid.show();
                fundsellgrid.show();
                queryPage(10,0,fundpaygrid);
                queryPage(10,0,fundsellgrid);
            }
        }
    }

    //分页查询
    function queryPage(pageSize,pageIndex,getgrid){
        debugger;
        var searchUrl = "/CombinePositionReportController/searchCombinePositionPage";
        var data = form.getData(true);
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        if (getgrid==bondpaygrid){
            data['ps']='P';
        }else if(getgrid==bondsellgrid){
            data['ps']='S';
        }else if(getgrid==fundpaygrid){
            data['fundside']='pay'
        }else if(getgrid==fundsellgrid){
            data['fundside']='sell'
        }
        var params = mini.encode(data);
        var grid = getgrid;
        CommonUtil.ajax({
            url: searchUrl,
            data: params,
            callback: function (data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });

    }
    //字段隐藏
    function selectstyle(dealtype) {
        if (dealtype=='H'){
            bondpaygrid.hideColumn("port");
            bondsellgrid.hideColumn("port");
        }else if (dealtype=='A'){
            bondpaygrid.hideColumn("port");
            bondsellgrid.hideColumn("port");
            bondpaygrid.hideColumn("faceAmt");
            bondsellgrid.hideColumn("faceAmt");
        }else if (dealtype=='I'){
            bondpaygrid.hideColumn("port");
            bondsellgrid.hideColumn("port");
            bondpaygrid.hideColumn("settAmt");
            bondsellgrid.hideColumn("settAmt");
            bondpaygrid.hideColumn("coupRate");
            bondsellgrid.hideColumn("coupRate");
        }
    }


    //清空按钮
    function clear() {
        form.clear();
        bondpaygrid.hide();
        bondsellgrid.hide();
        fundpaygrid.hide();
        fundsellgrid.hide();
    }


    //导出
    function exportExcel(){
        var data = form.getData(true);
        var fields = null;
        var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryMonth+"'>";
        fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
        fields += "<input type='hidden' id='sheetType' name='sheetType' value='"+data.dealtype+"'>";
        exportHrbExcel(fields);
    }

</script>
</body>
</html>
