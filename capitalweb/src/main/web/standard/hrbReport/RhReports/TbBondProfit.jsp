<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../../global.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
    <title>利润测算表</title>
</head>
<body>
<fieldset class="mini-fieldset title">
    <div id="search_form" style="width:100%">
        <legend>报表查询</legend>
        <input id="queryDate" name="queryDate" class="mini-datepicker" labelField="true" allowInput="false"
               label="测算截止日期:"
               value="<%=__bizDate%>" labelSyle="text-align:right;" emptyText="请输入"/>
        <input id="profittype" name="profittype" class="mini-combobox"  data="CommonUtil.serverData.dictionary.profittype" value="all" emptyText="请选择业务类型"  label="业务类型：" width="280px" labelField="true" labelStyle="text-align:right;"/>
        <span>
                <a id="send_btn" class="mini-button" style="display: none" onclick="query()">生成数据</a>
                <a id="export_btn" class="mini-button" style="display: none" onclick="exportExcel()">导出报表</a>
            </span>
    </div>
</fieldset>
<div class="mini-fit" style="height:100%;">
    <div id="slcd_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
         sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
         border="true" allowResize="true" showPager="true">
        <div property="columns" autoEscape="true">
            <div field="assetsClassification" headerAlign="center" align="center" width="150px">资产分类</div>
            <div field="bondType" headerAlign="center" align="center" width="150px" >债券分类</div>
            <div field="bondCode" headerAlign="center" align="center" width="150px" >债券代码</div>
            <div field="bondName" headerAlign="center" align="center" width="150px" >债券名称</div>
            <div field="faceamt" headerAlign="center" align="center" width="150px" numberFormat="#,0.00">持仓面额</div>
            <div field="rateChange" headerAlign="center" align="center" width="150px" numberFormat="#,0.00" >利息变动额</div>

        </div>
    </div>
</div>
<script>

    mini.parse();
    var grid = mini.get("slcd_grid");
    var form = new mini.Form("#search_form");
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var id = params.id;
    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        queryPage(pageSize, pageIndex);
    });

    /**时间格式化**/
    function onDateRenderer(e) {
        var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value) == null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g, '/'));
        if (value) return mini.formatDate(value, 'yyyy-MM-dd');
    }
    $(document).ready(function() {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(currTab.parentId)).done(function(visibleBtn) {

        });
    });


    //生成数据
    function query(){
        queryPage(10,0);
    }
    //生成数据
    function queryPage(pageSize,pageIndex) {
        debugger;
        var searchUrl = "/CombinePositionReportController/searchTbBondProfit";

        var data = form.getData(true);
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        var params = mini.encode(data);

        CommonUtil.ajax({
            url: searchUrl,
            data: params,
            callback: function (data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }

    //导出
    function exportExcel(){
        var content = grid.getData();

        if(content.length === 0){
            mini.alert("请先生成数据!");
            return;
        }

        var data = form.getData(true);
        var fields = null;
        var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
        fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
        exportHrbExcel(fields);
    }


</script>
</body>
</html>
