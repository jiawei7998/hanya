<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../../global.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
    <title>外汇自营交易限额执行情况表</title>
</head>
<body>
<fieldset class="mini-fieldset title">
    <div id="search_form" style="width:100%">
        <legend>报表查询</legend>
        <nobr>
            <input id="sdate" name="sdate" class="mini-datepicker mini-mustFill" labelField="true" label="日期：" required="true"
                   ondrawdate="onDrawDateStart" labelStyle="text-align:right;" emptyText="起始日期" format="yyyy-MM-dd" value="<%=__bizYesterdayDate%>"/>
            <span>至</span>
            <input id="edate" name="edate" class="mini-datepicker  mini-mustFill" required="true"
                   ondrawdate="onDrawDateEnd" emptyText="结束日期" format="yyyy-MM-dd" value="<%=__bizYesterdayDate%>"/>
        </nobr>
        <input id="systemDate" name="systemDate" class="mini-datepicker" labelField="true" allowInput="false"
               label="系统时间" value="<%=__bizDate%>" labelSyle="text-align:right;" style="display: none"/>
        <input id="wd1" name="wd1" class="mini-textbox" value="1000"  label="维度1：" width="250px" labelField="true" labelStyle="text-align:right;"/>
        <input id="wd2" name="wd2" class="mini-textbox" value="1500"  label="维度2：" width="250px" labelField="true" labelStyle="text-align:right;"/>
        <input id="wd3" name="wd3" class="mini-textbox" value="2000"  label="维度3：" width="250px" labelField="true" labelStyle="text-align:right;"/>
        <span>
                <a id="send_btn" class="mini-button" style="display: none" onclick="query()">生成数据</a>
                <a id="export_btn" class="mini-button" style="display: none" onclick="exportExcel()">导出报表</a>
            </span>
    </div>
</fieldset>
<div class="mini-fit" style="height:100%;">
    <div id="slcd_grid" class="mini-datagrid borderAll" style="height:100%;"
         sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
         border="true" allowResize="true" showPager="false">
        <div property="columns" autoEscape="true">
            <div field="currencypair" width="150" align="center" headerAlign="center" allowSort="false" >货币对</div>
            <div field="amount1" id="amount1" width="150" align="center" headerAlign="center" allowSort="false" >单笔<=1000万美元笔数</div>
            <div field="amount2" width="150" align="center" headerAlign="center" allowSort="false" >1000万美元<单笔<=1500万美元笔数</div>
            <div field="amount3"  width="150" align="center" headerAlign="center" allowSort="false" >1500万美元<单笔<=2000万美元笔数</div>
            <div field="amount4"  width="150" align="center" headerAlign="center" allowSort="false" >单笔>2000万美元笔数</div>
        </div>
    </div>
</div>
<script>

    mini.parse();
    var grid = mini.get("slcd_grid");
    var form = new mini.Form("#search_form");
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var id = params.id;

    /**时间格式化**/
    function onDateRenderer(e) {
        var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value) == null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g, '/'));
        if (value) return mini.formatDate(value, 'yyyy/MM/dd');
    }

    $(document).ready(function() {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(currTab.parentId)).done(function(visibleBtn) {
        });
    });

    //交易日期
    function onDrawDateStart(e) {
        var startDate = e.date;
        var endDate= mini.get("edate").getValue();
        if(CommonUtil.isNull(endDate)){
            return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }
    function onDrawDateEnd(e) {
        var endDate = e.date;
        var startDate = mini.get("sdate").getValue();
        var systemDate = mini.get("systemDate").getValue();
        if(CommonUtil.isNull(startDate)){
            return;
        }
        if (endDate.getTime() >= systemDate.getTime()){
            e.allowSelect = false;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }

    // 根据维度设置列名
    function setRowName(){
        var wd1=mini.get("wd1").getValue();
        if (wd1===""){
            wd1=1000;
        }
        var div1 = "单笔<="+wd1+"万美元笔数";
        grid.getColumn(1).header=div1;
        var wd2=mini.get("wd2").getValue();
        if (wd2===""){
            wd2=1500;
        }
        var div2 =wd1+"万美元<单笔<="+wd2+"万美元笔数";
        grid.getColumn(2).header=div2;
        var wd3=mini.get("wd3").getValue();
        if (wd3===""){
            wd3=2000;
        }
        var div3 =wd2+"万美元<单笔<="+wd3+"万美元笔数";
        grid.getColumn(3).header=div3;
        var div4 ="单笔>"+wd3+"万美元笔数";
        grid.getColumn(4).header=div4;
    }
    //生成数据
    function query() {
        setRowName();
        var searchUrl = "/CombinePositionReportController/searchIfsReportForezyjyxe";

        var data = form.getData(true);
        var params = mini.encode(data);

        CommonUtil.ajax({
            url: searchUrl,
            data: params,
            callback: function (data) {
                grid.setData(data.obj);
            }
        });
    }


    //导出
    function exportExcel(){
        var content = grid.getData();

        if(content.length === 0){
            mini.alert("请先生成数据!");
            return;
        }

        var data = form.getData(true);
        var fields = null;
        var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.edate+"'>";
        fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
        fields += "<input type='hidden' id='sdate' name='sdate' value='"+data.sdate+"'>";
        fields += "<input type='hidden' id='wd1' name='wd1' value='"+data.wd1+"'>";
        fields += "<input type='hidden' id='wd2' name='wd2' value='"+data.wd2+"'>";
        fields += "<input type='hidden' id='wd3' name='wd3' value='"+data.wd3+"'>";
        exportHrbExcel(fields);
    }

</script>
</body>
</html>
