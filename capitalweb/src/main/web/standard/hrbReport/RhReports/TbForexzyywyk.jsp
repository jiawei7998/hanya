<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../../global.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
    <title>外汇自营业务浮盈亏表</title>
</head>
<body>
<fieldset class="mini-fieldset title">
    <div id="search_form" style="width:100%">
        <legend>报表查询</legend>
        <input id="queryDate" name="queryDate" class="mini-datepicker" labelField="true" allowInput="false"
               label="查询日期:"
               value="<%=__bizDate%>" labelSyle="text-align:right;" emptyText="请输入"/>
        <span>
                <a id="send_btn" class="mini-button" style="display: none" onclick="query()">生成数据</a>
                <a id="export_btn" class="mini-button" style="display: none" onclick="exportExcel()">导出报表</a>
            </span>
    </div>
</fieldset>
<div class="mini-fit" style="height:100%;">
    <div id="slcd_grid" class="mini-datagrid borderAll" style="height:100%;"
         sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
         border="true" allowResize="true" showPager="false">
        <div property="columns" autoEscape="true">
            <div type="indexcolumn" headerAlign="center" width="40">序号</div>
            <div field="product" name="product"  width="300" align="center" headerAlign="center" allowSort="false" >业务层级</div>
            <div field="currencypair" width="150" align="center" headerAlign="center" allowSort="false" >货币对/合约</div>
            <div header="损益（人民币）" headerAlign="center">
                <div property="columns">
                    <div field="dayLossgain"  width="150" align="center" headerAlign="center" allowSort="false" numberFormat="#,0.00">日损益（元）</div>
                    <div field="weekLossgain" width="150" align="center" headerAlign="center" allowSort="false" numberFormat="#,0.00">周损益（元）</div>
                    <div field="monthLossgain"  width="150" align="center" headerAlign="center" allowSort="false" numberFormat="#,0.00">当月损益（元）</div>
                    <div field="quarterLossgain"  width="150" align="center" headerAlign="center" allowSort="false" numberFormat="#,0.00">季度损益（元）</div>
                    <div field="yearLossgain"  width="150" align="center" headerAlign="center" allowSort="false" numberFormat="#,0.00">年度损益（元）</div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

    mini.parse();
    var grid = mini.get("slcd_grid");
    var form = new mini.Form("#search_form");
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var id = params.id;


    /**时间格式化**/
    function onDateRenderer(e) {
        var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value) == null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g, '/'));
        if (value) return mini.formatDate(value, 'yyyy/MM/dd');
    }

    $(document).ready(function() {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(currTab.parentId)).done(function(visibleBtn) {
        });
    });
    function query() {
        search();
    }

    //生成数据
    function search() {
        var searchUrl = "/CombinePositionReportController/searchIfsReportForexzyywyk";

        var data = form.getData(true);
        var params = mini.encode(data);

        CommonUtil.ajax({
            url: searchUrl,
            data: params,
            callback: function (data) {
                grid.setTotalCount(data.obj.total);
                grid.setData(data.obj.rows);
                grid.mergeColumns(["product"]);
            }
        });
    }

    //清空按钮
    function clear() {
        form.clear();
    }

    //导出
    function exportExcel(){
        var content = grid.getData();

        if(content.length === 0){
            mini.alert("请先生成数据!");
            return;
        }

        var data = form.getData(true);
        var fields = null;
        var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
        fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
        exportHrbExcel(fields);
    }

</script>
</body>
</html>
