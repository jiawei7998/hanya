<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../../global.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
    <title>交易账户债券业务已实现损益情况</title>
</head>
<body>
<fieldset class="mini-fieldset title">
    <div id="search_form" style="width:100%">
        <legend>报表查询</legend>
        <input id="queryDate" name="queryDate" class="mini-datepicker" labelField="true" allowInput="false"
               label="查询日期:"
               value="<%=__bizDate%>" labelSyle="text-align:right;" emptyText="请输入"/>
        <input id="br" name="br" class="mini-combobox"  data="CommonUtil.serverData.dictionary.brtype" value="01" emptyText="请选择部门"  label="部门：" width="280px" labelField="true" labelStyle="text-align:right;"/>
        <input id="trad" name="trad" class="mini-combobox"  data="CommonUtil.serverData.dictionary.trad" value="3" emptyText="请选择账薄类型"  label="账簿类型：" width="280px" labelField="true" labelStyle="text-align:right;"/>
        <span>
                <a id="send_btn" class="mini-button" style="display: none" onclick="query()">生成数据</a>
                <a id="export_btn" class="mini-button" style="display: none" onclick="exportExcel()">导出报表</a>
            </span>
    </div>
</fieldset>
<div class="mini-fit" style="height:100%;">
    <div id="slcd_grid" class="mini-datagrid borderAll" style="width:1400px;"
         sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
         border="true" allowResize="true" showPager="false">
        <div property="columns" autoEscape="true">
            <div field="businessType" name="businessType"  width="160" align="center" headerAlign="center" allowSort="false">业务类型</div>
            <div field="combinationType" width="120" align="center" headerAlign="center" allowSort="false">组合类型</div>
            <div field="dayGainsLosses"  width="100" align="right" headerAlign="center" allowSort="false" numberFormat="#,0.00">当日损益</div>
            <div field="weekGainsLosses"  width="100" align="right" headerAlign="center" allowSort="false" numberFormat="#,0.00">周累计损益</div>
            <div field="monthGainsLosses" width="100" align="right" headerAlign="center" allowSort="false" numberFormat="#,0.00">当月累计损益</div>
            <div field="seasonGainsLosses"   width="100" align="right" headerAlign="center" allowSort="false" numberFormat="#,0.00">当季累计损益</div>
            <div field="yearGainsLosses" width="100" align="right" headerAlign="center" allowSort="false" numberFormat="#,0.00">当年累计损益</div>
        </div>
    </div>
</div>
<script>

    mini.parse();
    var grid = mini.get("slcd_grid");
    var form = new mini.Form("#search_form");
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var id = params.id;

    $(document).ready(function() {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(currTab.parentId)).done(function(visibleBtn) {
        });
    });

    //生成数据
    function query() {
        var searchUrl = "/CombinePositionReportController/searchIfsReportBondtraded";
        var data = form.getData(true);
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: searchUrl,
            data: params,
            callback: function (data) {
                grid.setData(data.obj);
                grid.mergeColumns(["businessType"]);
            }
        });
    }

    //导出
    function exportExcel(){
        var content = grid.getData();

        if(content.length === 0){
            mini.alert("请先生成数据!");
            return;
        }

        var data = form.getData(true);
        var fields = null;
        var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
        fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
        exportHrbExcel(fields);
    }

</script>
</body>
</html>
