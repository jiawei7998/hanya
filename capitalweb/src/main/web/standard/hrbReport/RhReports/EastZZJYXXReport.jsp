<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../../global.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
    <title>East资金交易信息表</title>
</head>
<body>
<fieldset class="mini-fieldset title">
    <div id="search_form" style="width:100%">
        <legend>报表查询</legend>
        <input id="queryDate" name="queryDate" class="mini-datepicker" labelField="true" allowInput="false"
               label="查询日期:"
               value="<%=__bizDate%>" labelSyle="text-align:right;" emptyText="请输入"/>
        <span>
                <a id="send_btn" class="mini-button" style="display: none" onclick="query()">生成数据</a>
                <a id="export_btn" class="mini-button" style="display: none" onclick="exportExcel()">导出报表</a>
            </span>
    </div>
</fieldset>
<div class="mini-fit" style="height:100%;">
    <div id="slcd_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
         sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
         border="true" allowResize="true" showPager="true">
        <div property="columns" autoEscape="true">
            <div type="indexcolumn" headerAlign="center" align="center" width="60px">序号</div>
            <div field="yhjgdm" headerAlign="center" align="center" width="100px">银行机构代码</div>
            <div field="jrxkzh" headerAlign="center" align="right" width="240px" >金融许可证号</div>
            <div field="nbjgh" headerAlign="center" align="right" width="100px" >内部机构号</div>
            <div field="mxkmbh" headerAlign="center" align="right" width="100px" >明细科目编号</div>
            <div field="yxjgmc" headerAlign="center" align="center" width="100px">银行机构名称</div>
            <div field="mxkmmc" headerAlign="center"  width="240px" >明细科目名称</div>
            <div field="jybh" headerAlign="center" align="right" width="100px" >交易编号</div>
            <div field="lccpdjbm" headerAlign="center" align="right" width="100px" >理财产品登记编码</div>
            <div field="jylx" headerAlign="center" align="center" width="100px">资金交易类型</div>
            <div field="jyzl" headerAlign="center"  width="240px" >资金交易子类</div>
            <div field="jrgjbh" headerAlign="center" align="right" width="100px" >金融工具编号</div>
            <div field="jyzhlx" headerAlign="center" align="right" width="100px" >账户类型</div>
            <div field="hth" headerAlign="center" align="center" width="100px">合同号</div>
            <div field="htje" headerAlign="center" align="right" width="240px" numberFormat="#,0.00">合同金额</div>
            <div field="bz" headerAlign="center" align="right" width="100px" >币种</div>
            <div field="jczckhmc" headerAlign="center" align="right" width="100px" >基础资产客户名称</div>
            <div field="jczcsshy" headerAlign="center" align="center" width="100px">基础资产所属行业</div>
            <div field="jczcsfwbhkh" headerAlign="center"  width="240px" >基础资产是否为本行客户</div>
            <div field="jczczxfs" headerAlign="center" align="right" width="100px" >基础资产增信方式</div>
            <div field="jczczxr" headerAlign="center" align="right" width="100px">基础资产增信人</div>
            <div field="jygy" headerAlign="center" align="center" width="100px">交易柜员</div>
            <div field="spr" headerAlign="center" align="center" width="240px" >审批人</div>
            <div field="jydsdm" headerAlign="center" align="right" width="100px" >交易对手代码</div>
            <div field="jydsmc" headerAlign="center" align="right" width="100px" >交易对手名称</div>
            <div field="jyrq" headerAlign="center" align="center" width="100px" renderer="onDateRenderer">交易日期</div>
            <div field="qxrq" headerAlign="center"  width="240px" renderer="onDateRenderer">起始日期</div>
            <div field="dqrq" headerAlign="center" align="right" width="100px" renderer="onDateRenderer">到期日期</div>
            <div field="mmbz" headerAlign="center" align="right" width="100px">买卖标志</div>
            <div field="jyqbz" headerAlign="center" align="center" width="100px">即远期标志</div>
            <div field="mrbz" headerAlign="center"  width="240px" >买入币种</div>
            <div field="mrje" headerAlign="center" align="right" width="100px" numberFormat="#,0.00">买入金额</div>
            <div field="mcbz" headerAlign="center" align="right" width="100px" >卖出币种</div>
            <div field="mcje" headerAlign="center" align="center" width="100px" numberFormat="#,0.00">卖出金额</div>
            <div field="cjjg" headerAlign="center"  width="240px" >成交价格</div>
            <div field="jyzt" headerAlign="center" align="center" width="100px">交易状态</div>
            <div field="fhrq" headerAlign="center"  width="240px" renderer="onDateRenderer">复核日期</div>
            <div field="qxrq" headerAlign="center" align="center" width="100px" renderer="onDateRenderer">取消日期</div>
            <div field="sjjgrq" headerAlign="center" align="center"  width="240px" renderer="onDateRenderer">实际交割日期</div>
            <div field="qsbz" headerAlign="center" align="center" width="100px">清算标志</div>
            <div field="jfzh" headerAlign="center" align="center" width="240px" >借方账号</div>
            <div field="dfzh" headerAlign="center" align="center" width="100px">贷方账号</div>
            <div field="jfje" headerAlign="center" align="center" width="240px" numberFormat="#,0.00">借方金额</div>
            <div field="dfje" headerAlign="center" align="center" width="100px" numberFormat="#,0.00">贷方金额</div>
            <div field="jfbz" headerAlign="center" align="center" width="240px" >借方币种</div>
            <div field="dfbz" headerAlign="center" align="center" width="100px">贷方币种</div>
            <div field="jfll" headerAlign="center" align="center" width="240px" numberFormat="#,0.00">借方利率</div>
            <div field="dfll" headerAlign="center" align="center" width="100px" numberFormat="#,0.00">贷方利率</div>
            <div field="bzjjybz" headerAlign="center"  width="240px" >保证金交易标志</div>
            <div field="glbzjzh" headerAlign="center" align="center" width="100px">关联业务编号</div>
            <div field="wbglxtmc" headerAlign="center"  width="240px" >外部关联系统名称</div>
            <div field="cjrq" headerAlign="center" align="center" width="240px" >采集日期</div>
        </div>
    </div>
</div>
<script>

    mini.parse();
    var grid = mini.get("slcd_grid");
    var form = new mini.Form("#search_form");
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var id = params.id;
    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        queryPage(pageSize, pageIndex);
    });

    /**时间格式化**/
    function onDateRenderer(e) {
        var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value) == null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g, '/'));
        if (value) return mini.formatDate(value, 'yyyy-MM-dd');
    }
    $(document).ready(function() {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(currTab.parentId)).done(function(visibleBtn) {

        });
    });


    //生成数据
    function query(){
        queryPage(10,0);
    }
    //生成数据
    function queryPage(pageSize,pageIndex) {
        debugger;
        var searchUrl = "/CombinePositionReportController/searchEastPositionPage1";

        var data = form.getData(true);
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        var params = mini.encode(data);

        CommonUtil.ajax({
            url: searchUrl,
            data: params,
            callback: function (data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }

    //导出
    function exportExcel(){
        var content = grid.getData();

        if(content.length === 0){
            mini.alert("请先生成数据!");
            return;
        }

        var data = form.getData(true);
        var fields = null;
        var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
        fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
        exportHrbExcel(fields);
    }


</script>
</body>
</html>
