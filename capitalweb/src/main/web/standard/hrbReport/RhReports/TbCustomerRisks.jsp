<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../../global.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
    <title>opics-新版客户风险需求（交易账户）</title>
</head>
<body>
<fieldset class="mini-fieldset title">
    <div id="search_form" style="width:100%">
        <legend>报表查询</legend>
        <input id="queryDate" name="queryDate" class="mini-datepicker" labelField="true" allowInput="false"
               label="查询日期:"
               value="<%=__bizDate%>" labelSyle="text-align:right;" emptyText="请输入"/>
        <span>
                <a id="send_btn" class="mini-button" style="display: none" onclick="query()">生成数据</a>
                <a id="export_btn" class="mini-button" style="display: none" onclick="exportExcel()">导出报表</a>
            </span>
    </div>
</fieldset>
<div class="mini-fit" style="height:100%;">
    <div id="slcd_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
         sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
         border="true" allowResize="true" showPager="true">
        <div property="columns" autoEscape="true">
            <div type="indexcolumn" headerAlign="center" align="center" width="60px">序号</div>
            <div field="clientName" headerAlign="center" align="center" width="200px">客户名称</div>
            <div field="clientCode" headerAlign="center" align="center" width="150px" >客户代码</div>
            <div field="countryCode" headerAlign="center" align="center" width="100px" >国家代码</div>
            <div field="institutionCode" headerAlign="center" align="center" width="150px" >非现场监管统计机构编码</div>
            <div field="organizationInstitutionCode" headerAlign="center" align="center" width="150px">组织机构代码</div>
            <div field="clientType" headerAlign="center" align="center" width="150px" >客户类别</div>
            <div field="internalRating" headerAlign="center" align="center" width="100px" >内部评级</div>
            <div field="withoutRating" headerAlign="center" align="center" width="100px" >外部评级</div>
            <div field="loanTrade" headerAlign="center" align="right" width="120px" numberFormat="#,0.00">拆放同业</div>
            <div field="interbankLoan" headerAlign="center" align="right" width="120px" numberFormat="#,0.00">存放同业</div>
            <div field="bondRepurchase" headerAlign="center" align="right" width="120px" numberFormat="#,0.00" >债券回购</div>
            <div field="stockPledgeLoan" headerAlign="center" align="right" width="120px" numberFormat="#,0.00">股票质押贷款</div>
            <div field="buyingResold" headerAlign="center" align="right" width="120px" numberFormat="#,0.00">买入返售资产</div>
            <div field="buyoutDiscount" headerAlign="center" align="right" width="120px" numberFormat="#,0.00">买断式转贴现</div>
            <div field="hoidingBond" headerAlign="center" align="right" width="120px" numberFormat="#,0.00" >持有债券</div>
            <div field="equityInvestment" headerAlign="center" align="right" width="120px" numberFormat="#,0.00">股权投资</div>
            <div field="tradePayment" headerAlign="center" align="right" width="120px" numberFormat="#,0.00">同业代付</div>
            <div field="otherServicesTable" headerAlign="center" align="right" width="120px" numberFormat="#,0.00">其他表内业务</div>
            <div field="sellingRepurchasedAssets" headerAlign="center" align="right" width="120px" numberFormat="#,0.00">卖出回购资产</div>
            <div field="commitmentDebt" headerAlign="center" align="right" width="120px" numberFormat="#,0.00">不可撤销的承诺及或有负债</div>
            <div field="otherServicesWithout" headerAlign="center" align="right" width="120px" numberFormat="#,0.00">其他表外业务</div>

        </div>
    </div>
</div>
<script>

    mini.parse();
    var grid = mini.get("slcd_grid");
    var form = new mini.Form("#search_form");
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var id = params.id;
    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        queryPage(pageSize, pageIndex);
    });

    /**时间格式化**/
    function onDateRenderer(e) {
        var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value) == null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g, '/'));
        if (value) return mini.formatDate(value, 'yyyy-MM-dd');
    }
    $(document).ready(function() {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(currTab.parentId)).done(function(visibleBtn) {

        });
    });


    //生成数据
    function query(){
        queryPage(10,0);
    }
    //生成数据
    function queryPage(pageSize,pageIndex) {
        debugger;
        var searchUrl = "/CombinePositionReportController/searchTbCustomerRisks";

        var data = form.getData(true);
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        var params = mini.encode(data);

        CommonUtil.ajax({
            url: searchUrl,
            data: params,
            callback: function (data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }

    //导出
    function exportExcel(){
        var content = grid.getData();

        if(content.length === 0){
            mini.alert("请先生成数据!");
            return;
        }

        var data = form.getData(true);
        var fields = null;
        var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
        fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
        exportHrbExcel(fields);
    }


</script>
</body>
</html>
