<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../../global.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
    <title>交易账户债券投资结构情况</title>
</head>
<body>
<fieldset class="mini-fieldset title">
    <div id="search_form" style="width:100%">
        <legend>报表查询</legend>
        <nobr>
            <input id="sdate" name="sdate" class="mini-datepicker mini-mustFill" labelField="true" label="日期："
                   required="true"
                   ondrawdate="onDrawDateStart" labelStyle="text-align:right;" emptyText="日期1" format="yyyy-MM-dd"
                   value="<%=__bizYesterdayDate%>"/>
            <span>至</span>
            <input id="edate" name="edate" class="mini-datepicker  mini-mustFill" required="true"
                   ondrawdate="onDrawDateEnd" emptyText="日期2" format="yyyy-MM-dd" value="<%=__bizYesterdayDate%>"/>
        </nobr>
        <input id="br" name="br" class="mini-combobox"  data="CommonUtil.serverData.dictionary.brtype" value="01" emptyText="请选择部门"  label="部门：" width="280px" labelField="true" labelStyle="text-align:right;"/>
        <span>
                <a id="send_btn" class="mini-button" style="display: none" onclick="query()">生成数据</a>
                <a id="export_btn" class="mini-button" style="display: none" onclick="exportExcel()">导出报表</a>
            </span>
    </div>
</fieldset>
<div class="mini-fit" style="height:100%;">
    <div id="slcd_grid" class="mini-datagrid borderAll" style="height:100%;"
         sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
         border="true" allowResize="true" showPager="false">
        <div property="columns" autoEscape="true">
            <div field="bondType" name="product" id="product" width="180" align="center" headerAlign="center" allowSort="false">
                债券类别
            </div>
            <div header="2021/10/14" headerAlign="center">
                <div property="columns">
                    <div field="bondStartProportion" name="bondStartProportion" id="bondStartProportion" width="150" align="center" headerAlign="center" allowSort="false"
                         numberFormat="#,0.00">占比(%)
                    </div>
                </div>
            </div>
            <div header="2021/10/14" headerAlign="center">
                <div property="columns">
                    <div field="bondEndProportion"  name="bondEndProportion" id="bondEndProportion" width="150" align="center" headerAlign="center" allowSort="false"
                         numberFormat="#,0.00">占比(%)
                    </div>
                </div>
            </div>
            <div field="fxztwbpj" name="fxztwbpj" width="150" align="center" headerAlign="center" allowSort="false">
                发行主题外部评级
            </div>
            <div field="qxpj" width="150" align="center" headerAlign="center" allowSort="false">债项评级</div>
            <div header="对应评级项下债券余额面值(亿元)" headerAlign="center">
                <div property="columns">
                    <div header="2021/10/14" headerAlign="center">
                        <div property="columns">
                            <div field="lastFaceamt" width="150" align="center" headerAlign="center" allowSort="false"
                                 numberFormat="#,0.00">余额面值
                            </div>
                            <div field="lastProportion" width="150" align="center" headerAlign="center"
                                 allowSort="false" numberFormat="#,0.00">占比(%)
                            </div>
                            <div field="zjc" width="150" align="center" headerAlign="center" allowSort="false"
                                 numberFormat="#,0.00">增/减/持
                            </div>
                        </div>
                    </div>
                    <div header="2021/10/14" headerAlign="center">
                        <div property="columns">
                            <div field="endFaceamt" width="150" align="center" headerAlign="center" allowSort="false"
                                 numberFormat="#,0.00">余额面值
                            </div>
                            <div field="endProportion" width="150" align="center" headerAlign="center" allowSort="false"
                                 numberFormat="#,0.00">占比(%)
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

    mini.parse();
    var grid = mini.get("slcd_grid");
    var form = new mini.Form("#search_form");
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var id = params.id;

    //动态设置列日期
    function setRowDate() {
        var sdate = mini.get("sdate").getText();
        var edate = mini.get("edate").getText();
        grid.columns[1].header = sdate;
        grid.columns[2].header = edate;
        grid.columns[5].columns[0].header = sdate;
        grid.columns[5].columns[1].header = edate;
    }

    //交易日期
    function onDrawDateStart(e) {
        var startDate = e.date;
        var endDate = mini.get("edate").getValue();
        if (CommonUtil.isNull(endDate)) {
            return;
        }
    }

    function onDrawDateEnd(e) {
        var endDate = e.date;
        var startDate = mini.get("sdate").getValue();
        if (CommonUtil.isNull(startDate)) {
            return;
        }
    }

    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(currTab.parentId)).done(function (visibleBtn) {
        });
        setRowDate();
    });

    //生成数据
    function query() {
        setRowDate();
        queryPage();
    }

    //生成数据
    function queryPage() {

        var searchUrl = "/CombinePositionReportController/searchTbTradingAccountBondinvest";

        var data = form.getData(true);
        var params = mini.encode(data);

        CommonUtil.ajax({
            url: searchUrl,
            data: params,
            callback: function (data) {
                grid.setTotalCount(data.obj.total);
                grid.setData(data.obj.rows);
                // grid.mergeColumns(["product"]);
                combineColumns();
            }
        });

    }
    //合并单元格
    function combineColumns(){
        debugger
        var marges = null;
        var content = grid.getData();
        for (let i = 0; i < content.length; i++) {
            for (let j = 0; j < content.length; j++) {
                if (content[i].bondType===content[j].bondType&&i!==j){
                    if (i<j){
                        marges = [
                            { rowIndex: i, columnIndex: 1, rowSpan:  Math.abs(j-i)+1, colSpan: 1 },
                            { rowIndex: i, columnIndex: 2, rowSpan:  Math.abs(j-i)+1, colSpan: 1 },
                            { rowIndex: i, columnIndex: 0, rowSpan:  Math.abs(j-i)+1, colSpan: 1 }
                        ];
                    }else{
                        marges = [
                            { rowIndex: j, columnIndex: 1, rowSpan:  Math.abs(j-i)+1, colSpan: 1 },
                            { rowIndex: j, columnIndex: 2, rowSpan:  Math.abs(j-i)+1, colSpan: 1 },
                            { rowIndex: j, columnIndex: 0, rowSpan:  Math.abs(j-i)+1, colSpan: 1 }
                        ];
                    }
                }
            }
        }
        console.log(marges);
        grid.mergeCells(marges);
    }

    //导出
    function exportExcel() {
        var content = grid.getData();
        if (content.length === 0) {
            mini.alert("请先生成数据!");
            return;
        }
        var data = form.getData(true);
        var fields = null;
        var fields = "<input type='hidden' id='queryDate' name='queryDate' value='" + data.edate + "'>";
        fields += "<input type='hidden' id='id' name='id' value='" + id + "'>";
        fields += "<input type='hidden' id='sdate' name='sdate' value='" + data.sdate + "'>";
        exportHrbExcel(fields);
    }

</script>
</body>
</html>
