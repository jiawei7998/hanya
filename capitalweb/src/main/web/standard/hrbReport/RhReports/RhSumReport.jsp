<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
    <title>人行利率</title>
</head>
<body>
<fieldset class="mini-fieldset title">
    <div id="search_form" style="width:100%">
        <legend>报表查询</legend>
        <input id="queryDate" name="queryDate" class="mini-monthpicker" labelField="true" allowInput="false"
               label="查询年月:"
               value="<%=__bizDate%>" labelSyle="text-align:right;"   allowInput="false" />
        <input id="dealtype" name="dealtype"  class="mini-combobox" data="CommonUtil.serverData.dictionary.RhDealType" labelField="true"
               label="报表名称:" labelSyle="text-align:right;" width="320px" emptyText="请输入" value="1"/>
        <span>
                <a class="mini-button" style="display: none"  id="search_btn" onclick="query()">生成数据</a>
                <a id="export_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
            </span>
    </div>
</fieldset>
<div class="mini-fit" style="width:100%;height:100%;">
        <%--存量同业借贷--%>
    <div id="one_grid" class="mini-datagrid borderAll" style="height:100%;display:none"
         sortMode="client" allowAlternating="true" border="true" allowResize="true" onrowdbclick="onRowDblClick">
        <div property="columns">
            <div type="indexcolumn" headerAlign="center" align="center" width="50px">序号</div>
            <div field="BANKINISTCODE" headerAlign="center" align="center" width="150px">金融机构代码</div>
            <div field="INSIDEINISTCODE" headerAlign="center" align="center" width="120px">内部机构号</div>
            <div field="SHCODE" headerAlign="center" align="center" width="150px">交易对手代码</div>
            <div field="SHTYPE" headerAlign="center" align="center" width="150px">交易对手代码类别</div>
            <div field="DEALNO" headerAlign="center" align="center" width="120px">合同编号</div>
            <div field="AL" headerAlign="center" align="center" width="120px">资产负债类型</div>
            <div field="PRODUCT" headerAlign="center" align="center" width="120px">产品类型</div>
            <div field="VDATE" headerAlign="center" align="center" width="150px" renderer="onDateRenderer">合同起息日期</div>
            <div field="MDATE" headerAlign="center" align="center" width="150px" renderer="onDateRenderer">合同到期日期</div>
            <div field="CCY" headerAlign="center" align="center" width="80px">币种</div>
            <div field="CCYAMT" headerAlign="center" align="center" width="120px" numberFormat="#,0.00">合同余额</div>
            <div field="SPOTRATE_8" headerAlign="center" align="center" width="120px" numberFormat="#,0.00">合同折人民币(元)</div>
            <div field="ISFIXED" headerAlign="center" align="center" width="150px">利率是否固定</div>
            <div field="INTRATE" headerAlign="center" align="center" width="120px" numberFormat="#,0.00">利率水平</div>
            <div field="INTRATETYPE" headerAlign="center" align="center" width="150px">定价基准类型</div>
            <div field="ACRATE" headerAlign="center" align="center" width="120px" numberFormat="#,0.00">基准利率</div>
            <div field="INTPAYCYCLE" headerAlign="center" align="center" width="120px">计息方式</div>
        </div>
    </div>
        <%--同业借贷发生额--%>
    <div id="two_grid" class="mini-datagrid borderAll" style="height:100%;display:none"
         sortMode="client" allowAlternating="true" border="true" allowResize="true" onrowdbclick="onRowDblClick">
        <div property="columns">
            <div type="indexcolumn" headerAlign="center" align="center" width="50px">序号</div>
            <div field="BANKINISTCODE" headerAlign="center" align="center" width="150px">金融机构代码</div>
            <div field="INSIDEINISTCODE" headerAlign="center" align="center" width="120px">内部机构号</div>
            <div field="shcode" headerAlign="center" align="center" width="150px">交易对手代码</div>
            <div field="shtype" headerAlign="center" align="center" width="180px">交易对手代码类别</div>
            <div field="DEALNO" headerAlign="center" align="center" width="100px">合同编号</div>
            <div field="FEDEALNO" headerAlign="center" align="center" width="150px">交易流水号</div>
            <div field="AL" headerAlign="center" align="center" width="100px">资产负债类型</div>
            <div field="PRODUCT" headerAlign="center" align="center" width="100px">产品类型</div>
            <div field="VDATE" headerAlign="center" align="center" width="150px" renderer="onDateRenderer">合同起息日期</div>
            <div field="MDATE" headerAlign="center" align="center" width="150px" renderer="onDateRenderer">合同到期日期</div>
            <div field="AMDATE" headerAlign="center" align="center" width="150px" renderer="onDateRenderer">合同实际终止日期</div>
            <div field="CCY" headerAlign="center" align="center" width="100px">币种</div>
            <div field="CCYAMT" headerAlign="center" align="center" width="150px" numberFormat="#,0.00">发生金额</div>
            <div field="SPOTRATE" headerAlign="center" align="center" width="150px" numberFormat="#,0.00">发生金额折人民币(元)</div>
            <div field="ISFIXED" headerAlign="center" align="center" width="120px">利率是否固定</div>
            <div field="INTRATE" headerAlign="center" align="center" width="100px" numberFormat="#,0.00">利率水平</div>
            <div field="INTRATETYPE" headerAlign="center" align="center" width="100px">定价基准类型</div>
            <div field="ACRATE" headerAlign="center" align="center" width="120px" numberFormat="#,0.00">基准利率</div>
            <div field="INTPAYCYCLE" headerAlign="center" align="center" width="120px">计息方式</div>
            <div field="LOGO" headerAlign="center" align="center" width="120px">发生结清标识</div>
        </div>
    </div>

        <%--存量同业存款--%>
   <div id="three_grid" class="mini-datagrid borderAll" style="height:100%;display:none"
             sortMode="client" allowAlternating="true" border="true" allowResize="true" onrowdbclick="onRowDblClick">
            <div property="columns">
                <div type="indexcolumn" headerAlign="center" align="center" width="50px">序号</div>
                <div field="BANKINISTCODE" headerAlign="center" align="center" width="150px">金融机构代码</div>
                <div field="INSIDEINISTCODE" headerAlign="center" align="center" width="120px">内部机构号</div>
                <div field="PRODTYPE" headerAlign="center" align="center" width="120px">业务类型</div>
                <div field="SHCODE" headerAlign="center" align="center" width="150px">交易对手代码</div>
                <div field="SHTYPE" headerAlign="center" align="center" width="150px">交易对手代码类别</div>
                <div field="ACCOUNTCODE" headerAlign="center" align="center" width="120px">存款账户编码</div>
                <div field="PROTOCOLCODE" headerAlign="center" align="center" width="120px">存款账户代码</div>
                <div field="VDATE" headerAlign="center" align="center" width="150px" renderer="onDateRenderer">协议起始日期</div>
                <div field="MDATE" headerAlign="center" align="center" width="150px" renderer="onDateRenderer">协议到期日期</div>
                <div field="CCY" headerAlign="center" align="center" width="80px">币种</div>
                <div field="CCYAMT" headerAlign="center" align="center" width="120px" numberFormat="#,0.00">存款余额</div>
                <div field="SPOTRATE" headerAlign="center" align="center" width="120px" numberFormat="#,0.00">存款余额折人民币(元)
                </div>
                <div field="INTRATE" headerAlign="center" align="center" width="120px" numberFormat="#,0.00">利率水平</div>
                <div field="PAYWAY" headerAlign="center" align="center" width="150px">缴存准备进方式</div>
            </div>
        </div>
        <%--同业存款发生额--%>
   <div id="four_grid" class="mini-datagrid borderAll" style="height:100%;display:none"
             sortMode="client" allowAlternating="true" border="true" allowResize="true" onrowdbclick="onRowDblClick">
            <div property="columns">
                <div type="indexcolumn" headerAlign="center" align="center" width="50px">序号</div>
                <div field="BANKINISTCODE" headerAlign="center" align="center" width="150px">金融机构代码</div>
                <div field="INSIDEINISTCODE" headerAlign="center" align="center" width="120px">内部机构号</div>
                <div field="PRODTYPE" headerAlign="center" align="center" width="120px">业务类型</div>
                <div field="SHCODE" headerAlign="center" align="center" width="150px">交易对手代码</div>
                <div field="SHTYPE" headerAlign="center" align="center" width="150px">交易对手代码类别</div>
                <div field="ACCOUNTCODE" headerAlign="center" align="center" width="120px">存款账户编码</div>
                <div field="PROTOCOLCODE" headerAlign="center" align="center" width="120px">存款账户代码</div>
                <div field="VDATE" headerAlign="center" align="center" width="150px" renderer="onDateRenderer">协议起始日期</div>
                <div field="MDATE" headerAlign="center" align="center" width="150px" renderer="onDateRenderer">协议到期日期</div>
                <div field="CCY" headerAlign="center" align="center" width="80px">币种</div>
                <div field="CCYAMT" headerAlign="center" align="center" width="120px" numberFormat="#,0.00">存款余额</div>
                <div field="SPOTRATE" headerAlign="center" align="center" width="180px" numberFormat="#,0.00">存款余额折人民币(元)</div>
                <div field="DEALDATE" headerAlign="center" align="center" width="150px" renderer="onDateRenderer">交易日期</div>
                <div field="INTRATE" headerAlign="center" align="center" width="120px" numberFormat="#,0.00">利率水平</div>
                <div field="PAYUSERID" headerAlign="center" align="center" width="150px">大额支付账号</div>
                <div field="PAYBANKID" headerAlign="center" align="center" width="150px">大额支付行号</div>
                <div field="RECUSERID" headerAlign="center" align="center" width="150px">交易对手支付账号</div>
                <div field="LOGO" headerAlign="center" align="center" width="120px">交易方向</div>
            </div>
        </div>
        <%--同业客户基本信息--%>
   <div id="five_grid" class="mini-datagrid borderAll" style="height:100%;display:none"
             sortMode="client" allowAlternating="true" border="true" allowResize="true" onrowdbclick="onRowDblClick">
            <div property="columns">
                <div type="indexcolumn" headerAlign="center" align="center" width="50px">序号</div>
                <div field="BANKINISTCODE" headerAlign="center" align="center" width="150px">金融机构代码</div>
                <div field="CFN" headerAlign="center" align="center" width="220px">客户名称</div>
                <div field="SHCODE" headerAlign="center" align="center" width="150px">客户代码</div>
                <div field="INSTITUCODE" headerAlign="center" align="center" width="180px">客户金融机构编码</div>
                <div field="CNO" headerAlign="center" align="center" width="100px">客户内部编码</div>
                <div field="BASICACCOUNT" headerAlign="center" align="center" width="150px">基本存款账号</div>
                <div field="BASICACCOUNTNAME" headerAlign="center" align="center" width="150px">基本账号开户行名称</div>
                <div field="CA" headerAlign="center" align="center" width="100px">注册地址</div>
                <div field="CCODE" headerAlign="center" align="center" width="150px" >地区代码</div>
                <div field="FINTYPE" headerAlign="center" align="center" width="150px" renderer="CommonUtil.dictRenderer"
                     data-options="{dict:'newFintype'}">客户类别</div>
                <div field="FOUNDDATE" headerAlign="center" align="center" width="100px" renderer="onDateRenderer">成立日期</div>
                <div field="ISACC" headerAlign="center" align="center" width="150px" renderer="CommonUtil.dictRenderer"
                     data-options="{dict:'YesNo'}">是否关联方</div>
                <div field="ECING" headerAlign="center" align="center" width="150px" renderer="CommonUtil.dictRenderer"
                     data-options="{dict:'CustEcing'}">客户经济成分</div>
                <div field="ECOTYPE" headerAlign="center" align="center" width="120px" renderer="CommonUtil.dictRenderer"
                     data-options="{dict:'CustEcotype'}">客户国名经济部门</div>
                <div field="CRE" headerAlign="center" align="center" width="100px" >客户信用级别总等级数</div>
                <div field="CRELEVEL" headerAlign="center" align="center" width="100px" >客户信用评级</div>
            </div>
        </div>
        <%--存量债券投资信息--%>
    <div id="six_grid" class="mini-datagrid borderAll" style="height:100%;display:none"
         sortMode="client" allowAlternating="true" border="true" allowResize="true" onrowdbclick="onRowDblClick">
        <div property="columns">
            <div type="indexcolumn" headerAlign="center" align="center" width="50px">序号</div>
            <div field="bankInistCode" headerAlign="center" align="center" width="150px">金融机构代码</div>
            <div field="InsideInistCode" headerAlign="center" align="center" width="120px">内部机构号</div>
            <div field="BNDCD" headerAlign="center" align="center" width="150px">债券代码</div>
            <div field="ACCOUNTNO" headerAlign="center" align="center" width="150px">债券总托管机构</div>
            <div field="BONDPROPERTIES" headerAlign="center" align="center" width="200px" renderer="CommonUtil.dictRenderer"
                 data-options="{dict:'BondPro'}">债券品种</div>
            <div field="BONDRATING" headerAlign="center" align="center" width="120px" renderer="CommonUtil.dictRenderer"
                 data-options="{dict:'Rating'}">债券信用级别</div>
            <div field="CCY" headerAlign="center" align="center" width="120px">币种</div>
            <div field="PRINAMT" headerAlign="center" align="center" width="120px" numberFormat="#,0.00">债券余额</div>
            <div field="SPOTRATE" headerAlign="center" align="center" width="180px" numberFormat="#,0.00">债券余额折人民币(元)</div>
            <div field="BOND_RIGHT_DEBT_DATE" headerAlign="center" align="center" width="180px" renderer="onDateRenderer">债券债务登记日</div>
            <div field="VDATE" headerAlign="center" align="center" width="150px" renderer="onDateRenderer">起息日</div>
            <div field="MDATE" headerAlign="center" align="center" width="150px" renderer="onDateRenderer">兑付日期</div>
            <div field="COUPRATE_8" headerAlign="center" align="center" width="80px" numberFormat="#,0.00">票面利率</div>
            <div field="SHTYPE" headerAlign="center" align="center" width="180px">发行人证件代码</div>
            <div field="CCODE" headerAlign="center" align="center" width="180px">发行人地区代码</div>
            <div field="INDTYPE" headerAlign="center" align="center" width="180px" renderer="CommonUtil.dictRenderer"
                 data-options="{dict:'industtype'}">发行人行业</div>
            <div field="COMSCALE" headerAlign="center" align="center" width="180px" renderer="CommonUtil.dictRenderer"
                 data-options="{dict:'CustComScale'}">发行人企业规模</div>
            <div field="ECING" headerAlign="center" align="center" width="180px" renderer="CommonUtil.dictRenderer"
                 data-options="{dict:'CustEcing'}">发行人经济成分</div>
            <div field="ECOTYPE" headerAlign="center" align="center" width="180px" renderer="CommonUtil.dictRenderer"
                 data-options="{dict:'CustEcotype'}">发行人国民经济部门</div>
        </div>
    </div>
        <%--债券投资信息发生额--%>
    <div id="seven_grid" class="mini-datagrid borderAll" style="height:100%;display:none"
         sortMode="client" allowAlternating="true"
         border="true" allowResize="true">
        <div property="columns">
            <div type="indexcolumn" headerAlign="center" align="center" width="50px">序号</div>
            <div field="BANKINISTCODE" headerAlign="center" align="center" width="150px">金融机构代码</div>
            <div field="INSIDEINISTCODE" headerAlign="center" align="center" width="120px">内部机构号</div>
            <div field="BNDCD" headerAlign="center" align="center" width="150px">债券代码</div>
            <div field="ACCOUNTNO" headerAlign="center" align="center" width="150px">债券总托管机构</div>
            <div field="BONDPROPERTIES" headerAlign="center" align="center" width="200px" renderer="CommonUtil.dictRenderer"
                 data-options="{dict:'BondPro'}">债券品种</div>
            <div field="BONDRATING" headerAlign="center" align="center" width="120px" renderer="CommonUtil.dictRenderer"
                 data-options="{dict:'Rating'}">债券信用级别</div>
            <div field="CCY" headerAlign="center" align="center" width="120px">币种</div>
            <div field="PRINAMT" headerAlign="center" align="center" width="180px" numberFormat="#,0.00">成交金额</div>
            <div field="SPOTRATE" headerAlign="center" align="center" width="180px" numberFormat="#,0.00">成交余额折人民币(元)</div>
            <div field="BOND_RIGHT_DEBT_DATE" headerAlign="center" align="center" width="180px" renderer="onDateRenderer">债券债务登记日</div>
            <div field="VDATE" headerAlign="center" align="center" width="150px" renderer="onDateRenderer">起息日</div>
            <div field="MDATE" headerAlign="center" align="center" width="150px" renderer="onDateRenderer">兑付日期</div>
            <div field="COUPRATE_8" headerAlign="center" align="center" width="80px" numberFormat="#,0.00">票面利率</div>
            <div field="SHTYPE" headerAlign="center" align="center" width="150px" renderer="CommonUtil.dictRenderer"
                 data-options="{dict:'GidType'}">发行人证件代码</div>
            <div field="CCODE" headerAlign="center" align="center" width="180px">发行人地区代码</div>
            <div field="INDTYPE" headerAlign="center" align="center" width="180px" renderer="CommonUtil.dictRenderer"
                 data-options="{dict:'industtype'}">发行人行业</div>
            <div field="COMSCALE" headerAlign="center" align="center" width="180px" renderer="CommonUtil.dictRenderer"
                 data-options="{dict:'CustComScale'}">发行人企业规模</div>
            <div field="ECING" headerAlign="center" align="center" width="180px" renderer="CommonUtil.dictRenderer"
                 data-options="{dict:'CustEcing'}">发行人经济成分</div>
            <div field="ECOTYPE" headerAlign="center" align="center" width="180px" renderer="CommonUtil.dictRenderer"
                 data-options="{dict:'CustEcotype'}">发行人国民经济部门</div>
            <div field="DEALDATE" headerAlign="center" align="center" width="150px" renderer="onDateRenderer">交易日期</div>
            <div field="FEDEALNO" headerAlign="center" align="center" width="150px" >交易流水号</div>
            <div field="LOGO" headerAlign="center" align="center" width="150px" >买入卖出标记</div>
        </div>
    </div>
        <%--存量债券发行信息--%>
    <div id="eight_grid" class="mini-datagrid borderAll" style="height:100%;display:none"
         sortMode="client" allowAlternating="true"
         border="true" allowResize="true">
        <div property="columns">
            <div type="indexcolumn" headerAlign="center" align="center" width="50px">序号</div>
            <div field="BANKINISTCODE" headerAlign="center" align="center" width="150px">金融机构代码</div>
            <div field="INSIDEINISTCODE" headerAlign="center" align="center" width="120px">内部机构号</div>
            <div field="BNDCD" headerAlign="center" align="center" width="150px">债券代码</div>
            <div field="ACCOUNTNO" headerAlign="center" align="center" width="150px">债券总托管机构</div>
            <div field="BONDPROPERTIES" headerAlign="center" align="center" width="200px" renderer="CommonUtil.dictRenderer"
                 data-options="{dict:'BondPro'}">债券品种</div>
            <div field="BONDRATING" headerAlign="center" align="center" width="120px" renderer="CommonUtil.dictRenderer"
                 data-options="{dict:'Rating'}">债券信用级别</div>
            <div field="REISSUE_FREQUENCY" headerAlign="center" align="center" width="120px">续发次数</div>
            <div field="CCY" headerAlign="center" align="center" width="120px">币种</div>
            <div field="PRINAMT" headerAlign="center" align="center" width="180px" numberFormat="#,0.00">期末债券面值</div>
            <div field="SPOTRATE1" headerAlign="center" align="center" width="180px" numberFormat="#,0.00">期末债券面值折人民币(元)</div>
            <div field="SETTAVGCOST" headerAlign="center" align="center" width="180px" numberFormat="#,0.00">期末债券余额</div>
            <div field="SPOTRATE2" headerAlign="center" align="center" width="180px" numberFormat="#,0.00">期末债券余额折人民币(元)</div>
            <div field="BOND_RIGHT_DEBT_DATE" headerAlign="center" align="center" width="180px" renderer="onDateRenderer">债券债务登记日</div>
            <div field="VDATE" headerAlign="center" align="center" width="150px" renderer="onDateRenderer">起息日</div>
            <div field="MDATE" headerAlign="center" align="center" width="150px" renderer="onDateRenderer">兑付日期</div>
            <div field="INTCALCRULE" headerAlign="center" align="center" width="150px" >付息方式</div>
            <div field="COUPRATE_8" headerAlign="center" align="center" width="80px" numberFormat="#,0.00">票面利率</div>
        </div>
    </div>
        <%--债券发行发生额信息--%>
    <div id="nine_grid" class="mini-datagrid borderAll" style="height:100%;display:none"
         sortMode="client" allowAlternating="true"
         border="true" allowResize="true">
        <div property="columns">
            <div type="indexcolumn" headerAlign="center" align="center" width="50px">序号</div>
            <div field="BANKINISTCODE" headerAlign="center" align="center" width="150px">金融机构代码</div>
            <div field="BNDCD" headerAlign="center" align="center" width="150px">债券代码</div>
            <div field="ACCOUNTNO" headerAlign="center" align="center" width="150px">债券总托管机构</div>
            <div field="BONDPROPERTIES" headerAlign="center" align="center" width="200px" renderer="CommonUtil.dictRenderer"
                 data-options="{dict:'BondPro'}">债券品种</div>
            <div field="BONDRATING" headerAlign="center" align="center" width="120px" renderer="CommonUtil.dictRenderer"
                 data-options="{dict:'Rating'}">债券信用级别</div>
            <div field="REISSUE_FREQUENCY" headerAlign="center" align="center" width="120px">续发次数</div>
            <div field="CCY" headerAlign="center" align="center" width="120px">币种</div>
            <div field="PRINAMT" headerAlign="center" align="center" width="180px" numberFormat="#,0.00">期末债券面值</div>
            <div field="SPOTRATE1" headerAlign="center" align="center" width="180px" numberFormat="#,0.00">期末债券面值折人民币(元)</div>
            <div field="PROCEEDAMT" headerAlign="center" align="center" width="180px" numberFormat="#,0.00">期末债券余额</div>
            <div field="SPOTRATE2" headerAlign="center" align="center" width="180px" numberFormat="#,0.00">期末债券余额折人民币(元)</div>
            <div field="BOND_RIGHT_DEBT_DATE" headerAlign="center" align="center" width="180px" renderer="onDateRenderer">债券债务登记日</div>
            <div field="VDATE" headerAlign="center" align="center" width="150px" renderer="onDateRenderer">起息日</div>
            <div field="MDATE" headerAlign="center" align="center" width="150px" renderer="onDateRenderer">兑付日期</div>
            <div field="DEALDATE" headerAlign="center" align="center" width="150px" renderer="onDateRenderer">交易日期</div>
            <div field="COUPRATE_8" headerAlign="center" align="center" width="80px" numberFormat="#,0.00">票面利率</div>
            <div field="LOGO" headerAlign="center" align="center" width="150px" renderer="CommonUtil.dictRenderer"
                 data-options="{dict:'logo1'}" >发行兑付标识</div>
        </div>
    </div>
        <%-- 存量特定目的载体信息   --%>
    <div id="ten_grid" class="mini-datagrid borderAll" style="height:100%;display:none"
         sortMode="client" allowAlternating="true"
         border="true" allowResize="true">
        <div property="columns">
            <div type="indexcolumn" headerAlign="center" align="center" width="50px">序号</div>
            <div field="BANKINISTCODE" headerAlign="center" align="center" width="150px">金融机构代码</div>
            <div field="INSIDEINISTCODE" headerAlign="center" align="center" width="150px">内部机构号</div>
            <div field="FUNDTYPES" headerAlign="center" align="center" width="150px">特定目的载体类型</div>
            <div field="MANAGE_PRODUCT_TYPE" headerAlign="center" align="center" width="120px">资管产品统计编码</div>
            <div field="FUND_CODE" headerAlign="center" align="center" width="120px" >特定目的载体代码</div>
            <div field="ISSUERCODE" headerAlign="center" align="center" width="120px">发行人代码</div>
            <div field="ISSUERAREA" headerAlign="center" align="center" width="120px">发行人地区代码</div>
            <div field="NET_WORTH_TYPE" headerAlign="center" align="center" width="180px" renderer="CommonUtil.dictRenderer"
                 data-options="{dict:'moneyFundType'}">运行方式</div>
            <div field="AFP_DATE" headerAlign="center" align="center" width="120px"  renderer="onDateRenderer" >认购日期</div>
            <div field="EARLIEST_REDEMPTION_DATE" headerAlign="center" align="center" width="120px"  renderer="onDateRenderer" >到期日期</div>
            <div field="CCY" headerAlign="center" align="center" width="180px">币种</div>
            <div field="TOTALAMT" headerAlign="center" align="center" width="180px" numberFormat="#,0.00">投资余额</div>
            <div field="CONVERTAMT" headerAlign="center" align="center" width="150px" numberFormat="#,0.00">投资余额折人民币</div>
        </div>
    </div>
        <%-- 特定目的载体发生额信息   --%>
    <div id="eleven_grid" class="mini-datagrid borderAll" style="height:100%;display:none"
         sortMode="client" allowAlternating="true"
         border="true" allowResize="true">
        <div property="columns">
            <div type="indexcolumn" headerAlign="center" align="center" width="50px">序号</div>
            <div field="BANKINISTCODE" headerAlign="center" align="center" width="150px">金融机构代码</div>
            <div field="INSIDEINISTCODE" headerAlign="center" align="center" width="150px">内部机构号</div>
            <div field="FUNDTYPES" headerAlign="center" align="center" width="150px">特定目的载体类型</div>
            <div field="MANAGE_PRODUCT_TYPE" headerAlign="center" align="center" width="120px">资管产品统计编码</div>
            <div field="FUND_CODE" headerAlign="center" align="center" width="120px" >特定目的载体代码</div>
            <div field="ISSUERCODE" headerAlign="center" align="center" width="120px">发行人代码</div>
            <div field="ISSUERAREA" headerAlign="center" align="center" width="120px">发行人地区代码</div>
            <div field="NET_WORTH_TYPE" headerAlign="center" align="center" width="180px" renderer="CommonUtil.dictRenderer"
                 data-options="{dict:'moneyFundType'}" >运行方式</div>
            <div field="AFP_DATE" headerAlign="center" align="center" width="120px"  renderer="onDateRenderer" >认购日期</div>
            <div field="TDATE" headerAlign="center" align="center" width="120px"  renderer="onDateRenderer" >交易日期</div>
            <div field="EARLIEST_REDEMPTION_DATE" headerAlign="center" align="center" width="120px"  renderer="onDateRenderer" >到期日期</div>
            <div field="CCY" headerAlign="center" align="center" width="180px">币种</div>
            <div field="TOTALAMT" headerAlign="center" align="center" width="180px" numberFormat="#,0.00">交易金额</div>
            <div field="CONVERTAMT" headerAlign="center" align="center" width="150px" numberFormat="#,0.00">交易金额折人民币</div>
            <div field="TRADEDIC" headerAlign="center" align="center" width="150px" >交易方向</div>
        </div>
    </div>
</div>
<script>

    mini.parse();
    var grid=null;
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var id = params.id;
    var type = mini.get("dealtype").getValue();

    var onegrid = mini.get("one_grid");
    var twogrid = mini.get("two_grid");
    var threegrid = mini.get("three_grid");
    var fourgrid = mini.get("four_grid");
    var fivegrid = mini.get("five_grid");
    var sixgrid = mini.get("six_grid");
    var sevengrid = mini.get("seven_grid");
    var eightgrid = mini.get("eight_grid");
    var ninegrid = mini.get("nine_grid");
    var tengrid = mini.get("ten_grid");
    var elevengrid = mini.get("eleven_grid");
    var form = new mini.Form("#search_form");

    var arr= [onegrid,twogrid,threegrid,fourgrid,fivegrid,sixgrid,sevengrid,eightgrid,ninegrid,tengrid,elevengrid];
    onegrid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        queryPage(pageSize,pageIndex);
    });
    twogrid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        queryPage(pageSize,pageIndex);
    });
    threegrid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        queryPage(pageSize,pageIndex);
    });
    fourgrid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        queryPage(pageSize,pageIndex);
    });
    fivegrid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        queryPage(pageSize,pageIndex);
    });
    sixgrid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        queryPage(pageSize,pageIndex);
    });
    sevengrid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        queryPage(pageSize,pageIndex);
    });
    eightgrid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        queryPage(pageSize,pageIndex);
    });
    ninegrid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        queryPage(pageSize,pageIndex);
    });
    tengrid.on("beforeload", function (e) {
        console.log(grid);
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        queryPage(pageSize,pageIndex);
    });
    elevengrid.on("beforeload", function (e) {
        console.log(grid);
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        queryPage(pageSize,pageIndex);
    });

    $(document).ready(function() {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
            query();
        });

    });

    function onDateRenderer(e) {
        var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value) == null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g, '/'));
        if (value) return mini.formatDate(value, 'yyyy-MM-dd');
    }

    //查询按钮
    function query(){
        debugger;
        clearstyle();
        type = mini.get("dealtype").getValue();
        grid=arr[Number(type)-1];
        grid.show();
        queryPage(10,0);
    }

    //分页查询
    function queryPage(pageSize,pageIndex){
        var searchUrl = "/RhSalesReportController/searchRhSalesReportPage";
        var data = form.getData(true);
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: searchUrl,
            data: params,
            callback: function (data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }
    //清空样式
    function clearstyle() {
        onegrid.hide();
        threegrid.hide();
        twogrid.hide();
        fourgrid.hide();
        fivegrid.hide();
        sixgrid.hide();
        sevengrid.hide();
        eightgrid.hide();
        ninegrid.hide();
        tengrid.hide();
        elevengrid.hide();
    }

    //导出
    function exportExcel(){
        debugger;
        var content = grid.getData();

        if(content.length === 0){
            mini.alert("请先生成数据!");
            return;
        }

        var data = form.getData(true);
        var sheetTypeText = mini.get("dealtype").getText();
        var fields = null;
        var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
        fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
        fields += "<input type='hidden' id='sheetType' name='sheetType' value='"+data.dealtype+"'>";
        fields += "<input type='hidden' id='sheetTypeValue' name='sheetTypeValue' value='"+sheetTypeText+"'>";
        exportHrbExcel(fields);
    }

</script>
</body>
</html>
