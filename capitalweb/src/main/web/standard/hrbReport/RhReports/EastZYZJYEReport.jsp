<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../../global.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
    <title>East自营资金业务余额表</title>
</head>
<body>
<fieldset class="mini-fieldset title">
    <div id="search_form" style="width:100%">
        <legend>报表查询</legend>
        <input id="queryDate" name="queryDate" class="mini-datepicker" labelField="true" allowInput="false"
               label="查询日期:"
               value="<%=__bizDate%>" labelSyle="text-align:right;" emptyText="请输入"/>
        <span>
                <a id="send_btn" class="mini-button" style="display: none" onclick="query()">生成数据</a>
                <a id="export_btn" class="mini-button" style="display: none" onclick="exportExcel()">导出报表</a>
            </span>
    </div>
</fieldset>
<div class="mini-fit" style="height:100%;">
    <div id="slcd_grid" class="mini-datagrid borderAll" style="height:100%;"
         sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
         border="true" allowResize="true" showPager="true">
        <div property="columns" autoEscape="true">
            <div type="indexcolumn" headerAlign="center" align="center" width="30px">序号</div>
            <div field="cjrq" headerAlign="center" align="center" width="100px">采集日期</div>
            <div field="clrq" headerAlign="center" align="right" width="240px" >存量日期</div>
            <div field="yxjgdm" headerAlign="center" align="right" width="100px" >银行机构代码</div>
            <div field="jrxkzh" headerAlign="center" align="right" width="100px" >金融许可证号</div>
            <div field="yxjgmc" headerAlign="center" align="center" width="100px">银行机构名称</div>
            <div field="nbjgh" headerAlign="center"  width="240px" >内部机构号</div>
            <div field="hnssbm" headerAlign="center" align="right" width="100px" >行内归属部门</div>
            <div field="jyzhlx" headerAlign="center" align="right" width="100px" >账户类型</div>
            <div field="jybh" headerAlign="center" align="center" width="100px">交易编号</div>
            <div field="ywzl" headerAlign="center"  width="240px" >业务中类</div>
            <div field="ywxl" headerAlign="center" align="right" width="100px" >业务小类</div>
            <div field="yhcp" headerAlign="center" align="right" width="100px" >银行产品</div>
            <div field="ywye" headerAlign="center" align="center" width="100px">业务余额</div>
            <div field="bz" headerAlign="center" align="right" width="240px" numberFormat="#,0.00">币种</div>
            <div field="jczcbh" headerAlign="center" align="right" width="100px" >基础资产编号</div>
            <div field="jczcmc" headerAlign="center" align="right" width="100px" >基础资产名称</div>
            <div field="jczcpj" headerAlign="center" align="center" width="100px">基础资产评级</div>
            <div field="jczcpjjg" headerAlign="center"  width="240px" >基础资产评级机构</div>
            <div field="jczckhbh" headerAlign="center" align="right" width="100px" >基础资产客户编号</div>
            <div field="jczckhmc" headerAlign="center" align="right" width="100px">基础资产客户名称</div>
            <div field="jczckhgj" headerAlign="center" align="center" width="100px">基础资产客户国家</div>
            <div field="jczckhpj" headerAlign="center" align="center" width="240px" >基础资产客户评级</div>
            <div field="jckhpjjg" headerAlign="center" align="right" width="100px" >基础客户评级机构</div>
            <div field="jczchkhhy" headerAlign="center" align="right" width="100px" >基础资产或客户行业</div>
            <div field="zztxlx" headerAlign="center" align="center" width="100px" renderer="onDateRenderer">最终投向类型</div>
            <div field="zztxhy" headerAlign="center"  width="240px" renderer="onDateRenderer">最终投向行业</div>
            <div field="xyfxqz" headerAlign="center" align="right" width="100px" renderer="onDateRenderer">信用风险权重</div>
            <div field="wjfl" headerAlign="center" align="right" width="100px">五级分类</div>
            <div field="jzzb" headerAlign="center" align="center" width="100px">减值准备</div>
            <div field="qxr" headerAlign="center"  width="240px" >起息日</div>
            <div field="dqr" headerAlign="center" align="right" width="100px" numberFormat="#,0.00">到期日</div>
            <div field="jyfx" headerAlign="center" align="right" width="100px" >交易方向</div>
            <div field="symz" headerAlign="center" align="center" width="100px" numberFormat="#,0.00">剩余面值</div>
            <div field="zmye" headerAlign="center"  width="240px" >账面余额</div>
            <div field="wyje" headerAlign="center" align="center" width="100px">违约金额</div>
            <div field="nhll" headerAlign="center"  width="240px" renderer="onDateRenderer">年华利率</div>
            <div field="spr" headerAlign="center" align="center" width="100px" renderer="onDateRenderer">审批人</div>
            <div field="jyy" headerAlign="center" align="center"  width="240px" renderer="onDateRenderer">交易员</div>
        </div>
    </div>
</div>
<script>

    mini.parse();
    var grid = mini.get("slcd_grid");
    var form = new mini.Form("#search_form");
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var id = params.id;
    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        queryPage(pageSize, pageIndex);
    });

    /**时间格式化**/
    function onDateRenderer(e) {
        var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value) == null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g, '/'));
        if (value) return mini.formatDate(value, 'yyyy-MM-dd');
    }
    $(document).ready(function() {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(currTab.parentId)).done(function(visibleBtn) {
        });
    });
    //生成数据
    function query(){
        queryPage(10,0);
    }
    //生成数据
    function queryPage(pageSize,pageIndex) {
        var searchUrl = "/CombinePositionReportController/searchEastPositionPage3";

        var data = form.getData(true);
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        var params = mini.encode(data);

        CommonUtil.ajax({
            url: searchUrl,
            data: params,
            callback: function (data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }

    // //清空按钮
    // function clear() {
    //     form.clear();
    //     query();
    // }

    //导出
    function exportExcel(){
        var content = grid.getData();

        if(content.length === 0){
            mini.alert("请先生成数据!");
            return;
        }

        var data = form.getData(true);
        var fields = null;
        var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
        fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
        exportHrbExcel(fields);
    }

</script>
</body>
</html>
