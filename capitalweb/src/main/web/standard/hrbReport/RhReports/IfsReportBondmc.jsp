<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../../global.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
    <title>债券卖出、兑付</title>
</head>
<body>
<fieldset class="mini-fieldset title">
    <div id="search_form" style="width:100%">
        <legend>报表查询</legend>
        <nobr>
            <input id="sdate" name="sdate" class="mini-datepicker mini-mustFill" labelField="true" label="日期：" required="true"
                   ondrawdate="onDrawDateStart" labelStyle="text-align:right;" emptyText="起始日期" format="yyyy-MM-dd" value="<%=__bizYesterdayDate%>"/>
            <span>至</span>
            <input id="edate" name="edate" class="mini-datepicker  mini-mustFill" required="true"
                   ondrawdate="onDrawDateEnd" emptyText="结束日期" format="yyyy-MM-dd" value="<%=__bizYesterdayDate%>"/>
        </nobr>
        <span>
                <a id="send_btn" class="mini-button" style="display: none" onclick="query()">生成数据</a>
                <a id="export_btn" class="mini-button" style="display: none" onclick="exportExcel()">导出报表</a>
            </span>
    </div>
</fieldset>
<div class="mini-fit" style="height:100%;">
    <div id="slcd_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
         sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
         border="true" allowResize="true" showPager="true">
        <div property="columns" autoEscape="true">
            <div type="indexcolumn" headerAlign="center" align="center" width="60px">序号</div>
            <div field="dealno" headerAlign="center" align="center" width="150px">交易号</div>
            <div field="fxzhlx" headerAlign="center" align="center" width="120px" >风险账户类型</div>
            <div field="kjzhlx" headerAlign="center" align="center" width="120px" >汇集账户类型</div>
            <div field="ywlx" headerAlign="center" align="center" width="120px" >业务类型</div>
            <div field="zqlx" headerAlign="center" align="center" width="150px">债券类型</div>
            <div field="zqhm" headerAlign="center" align="center" width="150px" >债券号码</div>
            <div field="zqmc" headerAlign="center" align="center" width="240px" >债券名称</div>
            <div field="pmrate" headerAlign="center" align="center" width="100px" numberFormat="#,0.00">票面利率</div>
            <div field="tcclr" headerAlign="center" align="center" width="120px" allowSort="false" >头寸处理日</div>
            <div field="mcr" headerAlign="center" align="center" width="120px"  allowSort="false">卖出（兑付）日</div>
            <div field="fhr" headerAlign="center" align="center" width="120px" allowSort="false" >复核日</div>
            <div field="cxr" headerAlign="center" align="center" width="120px" allowSort="false" >撤销日</div>
            <div field="mcj" headerAlign="center" align="right" width="120px"  numberFormat="#,0.00">卖出（兑付）价</div>
            <div field="zqmcmz" headerAlign="center" align="right" width="120px" numberFormat="#,0.0000">债券卖出（兑付）面值（万元）</div>
            <div field="zqjjcbj" headerAlign="center" align="right" width="120px" >债券净价成本价</div>
            <div field="zqjjcb" headerAlign="center" align="right" width="120px" numberFormat="#,0.0000">债券净价成本(万元)</div>
            <div field="zqqjcbj" headerAlign="center" align="right"  width="120px" >债券全价成本价</div>
            <div field="zqqjcb" headerAlign="center" align="right" width="120px"  numberFormat="#,0.0000">债券全价成本(万元)</div>
            <div field="zqjjmcjg" headerAlign="center" align="right" width="120px" numberFormat="#,0.00">债券净价卖出价格</div>
            <div field="zqjjmce" headerAlign="center" align="right" width="120px" numberFormat="#,0.0000">债券净价卖出额(万元)</div>
            <div field="zqqjmcjg" headerAlign="center" align="right" width="120px" numberFormat="#,0.00">债券全价卖出价格</div>
            <div field="zqqjmce" headerAlign="center" align="right" width="120px" numberFormat="#,0.0000">债券全价卖出额（万元）</div>
            <div field="jcsr" headerAlign="center" align="right" width="120px" numberFormat="#,0.00">价差收入（元）</div>
            <div field="lxsr" headerAlign="center" align="right" width="120px" numberFormat="#,0.00">利息收入（元）</div>
        </div>
    </div>
</div>
<script>

    mini.parse();
    var grid = mini.get("slcd_grid");
    var form = new mini.Form("#search_form");
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var id = params.id;
    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        queryPage(pageSize, pageIndex);
    });

    /**时间格式化**/
    function onDateRenderer(e) {
        var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value) == null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g, '/'));
        if (value) return mini.formatDate(value, 'yyyy-MM-dd');
    }

    //交易日期
    function onDrawDateStart(e) {
        var startDate = e.date;
        var endDate= mini.get("edate").getValue();
        if(CommonUtil.isNull(endDate)){
            return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }
    function onDrawDateEnd(e) {
        var endDate = e.date;
        var startDate = mini.get("sdate").getValue();
        if(CommonUtil.isNull(startDate)){
            return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }

    $(document).ready(function() {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(currTab.parentId)).done(function(visibleBtn) {

        });
    });


    //生成数据
    function query(){
        queryPage(10,0);
    }
    //生成数据
    function queryPage(pageSize,pageIndex) {
        debugger;
        var searchUrl = "/CombinePositionReportController/searchIfsReportBondmc";

        var data = form.getData(true);
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        var params = mini.encode(data);

        CommonUtil.ajax({
            url: searchUrl,
            data: params,
            callback: function (data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }

    //导出
    function exportExcel(){
        var content = grid.getData();

        if(content.length === 0){
            mini.alert("请先生成数据!");
            return;
        }

        var data = form.getData(true);
        var fields = null;
        var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.edate+"'>";
        fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
        fields += "<input type='hidden' id='sdate' name='sdate' value='"+data.sdate+"'>";
        exportHrbExcel(fields);
    }


</script>
</body>
</html>
