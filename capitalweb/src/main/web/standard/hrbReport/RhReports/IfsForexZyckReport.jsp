<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../../global.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
    <title>外汇自营敞口情况表</title>
</head>
<body>
<fieldset class="mini-fieldset title">
    <div id="search_form" style="width:100%">
        <legend>报表查询</legend>
        <nobr>
            <input id="sdate" name="sdate" class="mini-datepicker mini-mustFill" labelField="true" label="日期：" required="true"
                   ondrawdate="onDrawDateStart" labelStyle="text-align:right;" emptyText="起始日期" format="yyyy-MM-dd" value="<%=__bizDate%>"/>
            <span>至</span>
            <input id="edate" name="edate" class="mini-datepicker  mini-mustFill" required="true"
                   ondrawdate="onDrawDateEnd" emptyText="结束日期" format="yyyy-MM-dd" value="<%=__bizDate%>"/>
        </nobr>
        <input id="ccytype" name="ccytype" class="mini-combobox"  data="CommonUtil.serverData.dictionary.ccytype" value="CNY" emptyText="请选择币种"  label="币种：" width="280px" labelField="true" labelStyle="text-align:right;"/>
        <input id="unit" name="unit" class="mini-combobox"  data="CommonUtil.serverData.dictionary.unit" value="1" emptyText="请选择单位"  label="单位：" width="280px" labelField="true" labelStyle="text-align:right;"/>
        <span>
                <a id="send_btn" class="mini-button" style="display: none" onclick="query()">生成数据</a>
                <a id="export_btn" class="mini-button" style="display: none" onclick="exportExcel()">导出报表</a>
            </span>
    </div>
</fieldset>
<div class="mini-fit" style="height:100%;">
    <div id="slcd_grid" class="mini-datagrid borderAll" style="height:100%;"
         sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
         border="true" allowResize="true" showPager="true">
        <div property="columns" autoEscape="true">
            <div type="indexcolumn" headerAlign="center" width="40">序号</div>
            <div field="zydate"  width="150" align="center" headerAlign="center" allowSort="false" renderer="onDateRenderer">日期</div>
            <div field="usd" width="150" align="center" headerAlign="center" allowSort="false" numberFormat="#,0.00">USD</div>
            <div field="eur"  width="150" align="center" headerAlign="center" allowSort="false" numberFormat="#,0.00">EUR</div>
            <div field="gbp"  width="150" align="center" headerAlign="center" allowSort="false" numberFormat="#,0.00">GBP</div>
            <div field="rub" width="150" align="center" headerAlign="center" allowSort="false" numberFormat="#,0.00">RUB</div>
            <div field="nzd"  width="150" align="center" headerAlign="center" allowSort="false" numberFormat="#,0.00">NZD</div>
            <div field="jpy"  width="150" align="center" headerAlign="center" allowSort="false" numberFormat="#,0.00">JPY</div>
            <div field="hkd"  width="150" align="center" headerAlign="center" allowSort="false" numberFormat="#,0.00">HKD</div>
            <div field="cad" width="150" align="center" headerAlign="center" allowSort="false" numberFormat="#,0.00">CAD</div>
            <div field="aud" width="150" align="center" headerAlign="center" allowSort="false" numberFormat="#,0.00">AUD</div>
        </div>
    </div>
</div>
<script>

    mini.parse();
    var grid = mini.get("slcd_grid");
    var form = new mini.Form("#search_form");
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var id = params.id;
    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        queryPage(pageSize, pageIndex);
    });

    /**时间格式化**/
    function onDateRenderer(e) {
        var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value) == null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g, '/'));
        if (value) return mini.formatDate(value, 'yyyy/MM/dd');
    }



    $(document).ready(function() {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(currTab.parentId)).done(function(visibleBtn) {
        });
    });

    //生成数据
    function query(){
        queryPage(10,0);
    }
    //生成数据
    function queryPage(pageSize,pageIndex) {
        var searchUrl = "/CombinePositionReportController/searchIfsForexZyck";

        var data = form.getData(true);
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        var params = mini.encode(data);

        CommonUtil.ajax({
            url: searchUrl,
            data: params,
            callback: function (data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }

    //交易日期
    function onDrawDateStart(e) {
        var startDate = e.date;
        var endDate= mini.get("edate").getValue();
        if(CommonUtil.isNull(endDate)){
            return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }
    function onDrawDateEnd(e) {
        var endDate = e.date;
        var startDate = mini.get("sdate").getValue();
        if(CommonUtil.isNull(startDate)){
            return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }

    //导出
    function exportExcel(){
        var content = grid.getData();

        if(content.length === 0){
            mini.alert("请先生成数据!");
            return;
        }

        var data = form.getData(true);
        var fields = null;
        var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.edate+"'>";
        fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
        fields += "<input type='hidden' id='sdate' name='sdate' value='"+data.sdate+"'>";
        fields += "<input type='hidden' id='ccytype' name='ccytype' value='"+data.ccytype+"'>";
        exportHrbExcel(fields);
    }

</script>
</body>
</html>
