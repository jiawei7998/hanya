<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../../global.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
    <title>市场风险标准法资本要求情况表（一般利率风险-到期日法）</title>
</head>
<body>
<fieldset class="mini-fieldset title">
    <div id="search_form" style="width:100%">
        <legend>报表查询</legend>
        <input id="queryDate" name="queryDate" class="mini-datepicker" labelField="true" allowInput="false"
               label="查询日期:"
               value="<%=__bizDate%>" labelSyle="text-align:right;" emptyText="请输入"/>
        <span>
                <a id="send_btn" class="mini-button" style="display: none" onclick="query()">生成数据</a>
                <a id="export_btn" class="mini-button" style="display: none" onclick="exportExcel()">导出报表</a>
            </span>
    </div>
</fieldset>
<div class="mini-fit" style="height:100%;">
    <div id="slcd_grid" class="mini-datagrid borderAll" style="width:1730px;height: 100%"
         sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
         border="true" allowResize="true" showPager="false">
        <div property="columns" autoEscape="true">
            <div field="sort" headerAlign="center" align="center" width="40">序号</div>
            <div field="timeframe"  width="200"  headerAlign="center" align="left" allowSort="false">时段</div>
            <div field="annualratel" width="150" align="center" headerAlign="center" allowSort="false">年息票率大于等于3%</div>
            <div field="annualrates"  width="120" align="center" headerAlign="center" allowSort="false">年息票率小于3%</div>
            <div header="债券及债务相关衍生工具合约" headerAlign="center">
                <div property="columns">
                    <div field="zqdt"  width="180" align="center" headerAlign="center" allowSort="false" numberFormat="#,0.0000">债券多头（万元）</div>
                    <div field="zqkt" width="180" align="center" headerAlign="center" allowSort="false" numberFormat="#,0.0000">债券空头（万元）</div>
                </div>
            </div>
            <div header="利率衍生工具合约" headerAlign="center">
                <div property="columns">
                    <div field="lldt"  width="180" align="center" headerAlign="center" allowSort="false" numberFormat="#,0.0000">利率多头（万元）</div>
                    <div field="llkt" width="180" align="center" headerAlign="center" allowSort="false" numberFormat="#,0.0000">利率空头（万元）</div>
                </div>
            </div>
            <div header="总额" headerAlign="center">
                <div property="columns">
                    <div field="zedt"  width="220" align="center" headerAlign="center" allowSort="false" numberFormat="#,0.0000">总额多头(万元)</div>
                    <div field="zekt" width="240" align="center" headerAlign="center" allowSort="false" numberFormat="#,0.0000">总额空头(万元)</div>
                </div>
            </div>
            <div field="fxqz" width="240" align="center" headerAlign="center" allowSort="false" >风险权重</div>
            <div header="风险加权头寸" headerAlign="center">
                <div property="columns">
                    <div field="fxdt"  width="325" align="center" headerAlign="center" allowSort="false" numberFormat="#,0.0000">风险多头（万元）</div>
                    <div field="fxkt" width="220" align="center" headerAlign="center" allowSort="false" numberFormat="#,0.0000">风险空头（万元）</div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    mini.parse();
    var grid = mini.get("slcd_grid");
    var form = new mini.Form("#search_form");
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var id = params.id;
    /**时间格式化**/
    function onDateRenderer(e) {
        var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value) == null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g, '/'));
        if (value) return mini.formatDate(value, 'yyyy-MM-dd');
    }

    $(document).ready(function() {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(currTab.parentId)).done(function(visibleBtn) {
        });
    });

    //生成数据
    function query() {
        var searchUrl = "/CombinePositionReportController/searchIfsReportGeneralRaterisk";
        var data = form.getData(true);
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: searchUrl,
            data: params,
            callback: function (data) {
                console.log(data)
                grid.setData(data.obj.rows);
                grid.mergeColumns(["leadDepart","assortDepart"]);
            }
        });
    }

    //导出
    function exportExcel(){
        var content = grid.getData();

        if(content.length === 0){
            mini.alert("请先生成数据!");
            return;
        }

        var data = form.getData(true);
        var fields = null;
        var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
        fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
        exportHrbExcel(fields);
    }

</script>
</body>
</html>
