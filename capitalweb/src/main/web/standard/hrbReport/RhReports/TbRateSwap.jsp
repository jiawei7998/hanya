<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../../global.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
    <title>利率互换业务明细表</title>
</head>
<body>
<fieldset class="mini-fieldset title">
    <div id="search_form" style="width:100%">
        <legend>报表查询</legend>
        <input id="queryDate" name="queryDate" class="mini-datepicker" labelField="true" allowInput="false"
               label="查询日期:"
               value="<%=__bizDate%>" labelSyle="text-align:right;" emptyText="请输入"/>
        <span>
                <a id="send_btn" class="mini-button" style="display: none" onclick="query()">生成数据</a>
                <a id="export_btn" class="mini-button" style="display: none" onclick="exportExcel()">导出报表</a>
            </span>
    </div>
</fieldset>
<div class="mini-fit" style="height:100%;">
    <div id="slcd_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
         sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
         border="true" allowResize="true" showPager="true">
        <div property="columns" autoEscape="true">
            <%--            <div type="indexcolumn" headerAlign="center" align="center" width="60px">序号</div>--%>
            <div field="dealno" headerAlign="center" align="center" width="100px">合同号</div>
            <div field="accountType" headerAlign="center" align="center" width="120px" >风险账户类型</div>
            <div field="dealType" headerAlign="center" align="center" width="120px" >交易类型</div>
            <div field="dealDate" headerAlign="center" align="center" width="120px" renderer="onDateRenderer">成交日期</div>
            <div field="vdate" headerAlign="center" align="center" width="120px" renderer="onDateRenderer">起息日</div>
            <div field="firstDate" headerAlign="center" align="center" width="120px" renderer="onDateRenderer">首期起息日</div>
            <div field="mdate" headerAlign="center" align="center" width="120px" renderer="onDateRenderer">到期日</div>
            <div field="timeLimit" headerAlign="center" align="center" width="120px" >合约期限（天）</div>
            <div field="yearLimit" headerAlign="center" align="center" width="120px" >剩余期限（年）</div>
            <div field="direction" headerAlign="center" align="center" width="120px"  >买卖方向</div>
            <div field="notionalPrincipal" headerAlign="center" align="right" width="180px"  numberFormat="#,0.00">名义本金（元）</div>
            <div field="cno" headerAlign="center" align="center" width="240px"  numberFormat="#,0.0000">交易对手</div>
            <div field="cnoCreditGrade" headerAlign="center" align="center" width="120px"  >交易对手信用评级</div>
            <div field="interestDay" headerAlign="center" align="center" width="120px"  >计息天数调整</div>
            <div field="clearType" headerAlign="center" align="center" width="100px"  >清算方式</div>
            <div field="fixedRate" headerAlign="center" align="right" width="100px"  >固定利率</div>
            <div field="fixedRatePayCycle" headerAlign="center" align="center" width="140px"  >固定利率支付周期</div>
            <div field="floatRateWay" headerAlign="center" align="center" width="140px"  >浮动利率计息方法</div>
            <div field="referRate" headerAlign="center" align="right" width="100px"  numberFormat="#,0.00">参考利率</div>
            <div field="resetFrequency" headerAlign="center" align="center" width="100px"  numberFormat="#,0.00">重置频率</div>
            <div field="floatRatePayCycle" headerAlign="center" align="center" width="140px"  >浮动利率支付周期</div>
            <div field="valueamt" headerAlign="center" align="right" width="150px"  numberFormat="#,0.00">估值（元）</div>
        </div>
    </div>
</div>
<script>

    mini.parse();
    var grid = mini.get("slcd_grid");
    var form = new mini.Form("#search_form");
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var id = params.id;
    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        queryPage(pageSize, pageIndex);
    });

    /**时间格式化**/
    function onDateRenderer(e) {
        var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value) == null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g, '/'));
        if (value) return mini.formatDate(value, 'yyyy-MM-dd');
    }
    $(document).ready(function() {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(currTab.parentId)).done(function(visibleBtn) {

        });
    });


    //生成数据
    function query(){
        queryPage(10,0);
    }
    //生成数据
    function queryPage(pageSize,pageIndex) {
        debugger;
        var searchUrl = "/CombinePositionReportController/searchTbRateSwap";

        var data = form.getData(true);
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        var params = mini.encode(data);

        CommonUtil.ajax({
            url: searchUrl,
            data: params,
            callback: function (data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }

    //导出
    function exportExcel(){
        var content = grid.getData();

        if(content.length === 0){
            mini.alert("请先生成数据!");
            return;
        }

        var data = form.getData(true);
        var fields = null;
        var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
        fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
        exportHrbExcel(fields);
    }


</script>
</body>
</html>
