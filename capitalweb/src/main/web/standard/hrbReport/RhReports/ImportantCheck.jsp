<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../../global.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
    <title>重点业务对账表</title>
</head>
<body>
<fieldset class="mini-fieldset title">
    <div id="search_form" style="width:100%">
        <legend>报表查询</legend>
        <input id="queryDate" name="queryDate" class="mini-datepicker" labelField="true" allowInput="false"
               label="查询日期:"
               value="<%=__bizDate%>" labelSyle="text-align:right;" emptyText="请输入"/>
        <span>
                <a id="send_btn" class="mini-button" style="display: none" onclick="query()">生成数据</a>
                <a id="export_btn" class="mini-button" style="display: none" onclick="exportExcel()">导出报表</a>
            </span>
    </div>
</fieldset>
<div class="mini-fit" style="height:100%;">
    <span style="float:left;"><b>单位:元</b></span>
    <span style="float:right;"><b>填报日期:<%=__bizDate%></b></span>
    <div id="slcd_grid" class="mini-datagrid borderAll" style="width:1730px;"
         sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
         border="true" allowResize="true" showPager="false">
        <div property="columns" autoEscape="true">
<%--            <div type="indexcolumn" headerAlign="center" width="40">序号</div>--%>
            <div field="product"  width="150"  headerAlign="center" allowSort="false">项目</div>
            <div field="totcalData" width="150" align="center" headerAlign="center" allowSort="false">总账数据</div>
            <div field="listData"  width="150" align="center" headerAlign="center" allowSort="false">业务明细数据</div>
            <div field="differenceData"  width="180" align="center" headerAlign="center" allowSort="false">差异</div>
            <div field="differenceReason" width="250" align="center" headerAlign="center" allowSort="false">差异原因</div>
            <div field="leadDepart" name="leadDepart"  width="180" align="center" headerAlign="center" allowSort="false" >牵头报送部门</div>
            <div field="assortDepart" name="assortDepart" width="280" align="center" headerAlign="center" allowSort="false">配合部门</div>

        </div>
    </div>
</div>
<script>

    mini.parse();
    var grid = mini.get("slcd_grid");
    var form = new mini.Form("#search_form");
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var id = params.id;
    /**时间格式化**/
    function onDateRenderer(e) {
        var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value) == null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g, '/'));
        if (value) return mini.formatDate(value, 'yyyy-MM-dd');
    }

    $(document).ready(function() {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(currTab.parentId)).done(function(visibleBtn) {
        });
    });

    //生成数据
    function query() {
        var searchUrl = "/CombinePositionReportController/searchIfsReportImportantCheck";
        var data = form.getData(true);
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: searchUrl,
            data: params,
            callback: function (data) {
                console.log(data)
                grid.setData(data.obj.rows);
                grid.mergeColumns(["leadDepart","assortDepart"]);
            }
        });
    }

    //导出
    function exportExcel(){
        var content = grid.getData();

        if(content.length === 0){
            mini.alert("请先生成数据!");
            return;
        }

        var data = form.getData(true);
        var fields = null;
        var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
        fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
        exportHrbExcel(fields);
    }

</script>
</body>
</html>
