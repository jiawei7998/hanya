<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../../global.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
    <title>利率特定风险计算表</title>
</head>
<body>
<fieldset class="mini-fieldset title">
    <div id="search_form" style="width:100%">
        <legend>报表查询</legend>
        <input id="queryDate" name="queryDate" class="mini-datepicker" labelField="true" allowInput="false"
               label="查询日期:"
               value="<%=__bizDate%>" labelSyle="text-align:right;" emptyText="请输入"/>
        <span>
                <a id="send_btn" class="mini-button" style="display: none" onclick="query()">生成数据</a>
                <a id="export_btn" class="mini-button" style="display: none" onclick="exportExcel()">导出报表</a>
            </span>
    </div>
</fieldset>
<div class="mini-fit" style="height:100%;">
    <div id="slcd_grid" class="mini-datagrid borderAll" style="height: 100%;"
         sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
         border="true" allowResize="true" showPager="false">
        <div property="columns" autoEscape="true">
            <div type="indexcolumn" headerAlign="center" width="40">序号</div>
            <div field="sort0" name="sort0" width="180" align="center" headerAlign="center" allowSort="false">类别</div>
            <div field="sort"  width="150" align="center" headerAlign="center" allowSort="false">类别</div>
            <div field="bondid" width="150" align="center" headerAlign="center" allowSort="false">债券ID</div>
            <div field="zqms"  width="200" align="center" headerAlign="center" allowSort="false">债券描述</div>
            <div field="zqmc"  width="180" align="center" headerAlign="center" allowSort="false">债券名称</div>
            <div field="fxr" width="200" align="center" headerAlign="center" allowSort="false">发行人</div>
            <div field="ztpj"   width="120" align="center" headerAlign="center" allowSort="false" >主体评级</div>
            <div field="dqr" width="180" align="center" headerAlign="center" allowSort="false" renderer="onDateRenderer">到期日或(最近一次付息日)</div>
            <div field="syqx" width="120" align="center" headerAlign="center" allowSort="false" >剩余期限(年)</div>
            <div field="fxqzhgzq" width="150" align="center" headerAlign="center" allowSort="false">风险权重(合格证券)</div>
            <div field="fxqzqtzq" width="150" align="center" headerAlign="center" allowSort="false">风险权重(其他证券)</div>
            <div field="fxqzhh" width="150" align="center" headerAlign="center" allowSort="false">风险权重(混合)</div>
            <div field="zqsz" width="150" align="center" headerAlign="center" allowSort="false" numberFormat="#,0.0000">债券市值(万元)</div>
            <div field="zbzyhgzq" width="180" align="center" headerAlign="center" allowSort="false" numberFormat="#,0.0000">资本占用(合格证券)(万元)</div>
            <div field="zbzyqtzq" width="180" align="center" headerAlign="center" allowSort="false" numberFormat="#,0.0000">资本占用(其他证券)(万元)</div>
            <div field="zbzyhhf" width="180" align="center" headerAlign="center" allowSort="false" numberFormat="#,0.0000">资本占用(混合法)(万元)</div>

        </div>
    </div>
</div>
<script>

    mini.parse();
    var grid = mini.get("slcd_grid");
    var form = new mini.Form("#search_form");
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var id = params.id;
    /**时间格式化**/
    function onDateRenderer(e) {
        var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value) == null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g, '/'));
        if (value) return mini.formatDate(value, 'yyyy-MM-dd');
    }

    $(document).ready(function() {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(currTab.parentId)).done(function(visibleBtn) {
        });
    });

    //生成数据
    function query() {
        debugger;
        var searchUrl = "/CombinePositionReportController/searchIfsReportRatetdfx";
        var data = form.getData(true);
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: searchUrl,
            data: params,
            callback: function (data) {
                console.log(data)
                grid.setData(data.obj);
                grid.mergeColumns(["sort0"]);
            }
        });
    }

    //导出
    function exportExcel(){
        var content = grid.getData();

        if(content.length === 0){
            mini.alert("请先生成数据!");
            return;
        }

        var data = form.getData(true);
        var fields = null;
        var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
        fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
        exportHrbExcel(fields);
    }

</script>
</body>
</html>
