<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../../global.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
    <title>合并持仓明细分析报表【含逾期】-给于泽</title>
</head>
<body>
<fieldset class="mini-fieldset title">
    <div id="search_form" style="width:100%">
        <legend>报表查询</legend>
        <input id="queryDate" name="queryDate" class="mini-datepicker" labelField="true" allowInput="false"
               label="查询日期:"
               value="<%=__bizDate%>" labelSyle="text-align:right;" emptyText="请输入"/>
        <span>
                <a id="send_btn" class="mini-button" style="display: none" onclick="query()">生成数据</a>
                <a id="export_btn" class="mini-button" style="display: none" onclick="exportExcel()">导出报表</a>
            </span>
    </div>
</fieldset>
<div class="mini-fit" style="height:100%;">
    <div id="slcd_grid" class="mini-datagrid borderAll" style="height:100%;"
         sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
         border="true" allowResize="true" showPager="true">
        <div property="columns" autoEscape="true">
            <div type="indexcolumn" headerAlign="center" width="40">序号</div>
            <div field="cost" name="cost" width="300" align="center" headerAlign="center" allowSort="false">成本中心</div>
            <div field="port" name="port" width="80" align="center" headerAlign="center" allowSort="false">投资组合</div>
            <div field="jiuqi" name="jiuqi" width="80" align="right" headerAlign="center" allowSort="false">久期</div>
            <div field="pvbpx" name="pvbpx" width="80" align="right" headerAlign="center" allowSort="false">PVBP值</div>
            <div field="pv01x" name="pv01x" width="100" align="right" headerAlign="center" allowSort="false">PV01值</div>
            <div field="varx" name="varx" width="80" align="right" headerAlign="center" allowSort="false">VAR值</div>
            <div field="fxzl" name="fxzl" width="120" align="right" headerAlign="center" allowSort="false">债券发行总量(万)</div>
            <div field="sshy" name="sshy" width="120" align="center" headerAlign="center" allowSort="false">债券所属行业</div>
            <div field="ztlevel" name="ztlevel" width="100" align="center" headerAlign="center" allowSort="false">最新主体评级</div>
            <div field="zxlevel" name="zxlevel" width="100" align="center" headerAlign="center" allowSort="false">最新债项评级</div>
            <div field="fxztlevel" name="fxztlevel" width="100" align="center" headerAlign="center" allowSort="false">发行主体评级</div>
            <div field="fxzxlevel" name="fxzxlevel" width="100" align="center" headerAlign="center" allowSort="false">发行债项评级</div>
            <div field="acctngtype" name="acctngtype" width="120" align="center" headerAlign="center" allowSort="false">持仓类别</div>
            <div field="cccb" name="cccb" width="120" align="right" headerAlign="center" allowSort="false">持仓成本(元)</div>
            <div field="yslx" name="yslx" width="120" align="right" headerAlign="center" allowSort="false">应收利息(元)</div>

            <div field="zmjz" name="zmjz" width="120" align="right" headerAlign="center" allowSort="false">账面价值(元)</div>
            <div field="yjlx" name="yjlx" width="100" align="right" headerAlign="center" allowSort="false">应计利息(元)</div>
            <div field="dnljlxsr" name="dnljlxsr" width="120" align="right" headerAlign="center" allowSort="false">计算期利息收入(元)</div>
            <div field="syljlxsr" name="syljlxsr" width="120" align="right" headerAlign="center" allowSort="false">持有期利息收入(元)</div>
            <div field="tdymtm" name="tdymtm" width="120" align="right" headerAlign="center" allowSort="false">公允价值变动(元)</div>
            <div field="tjpmtm" name="tjpmtm" width="150" align="right" headerAlign="center" allowSort="false">公允价值变动损益(元)</div>
            <div field="avgcost" name="avgCost" width="120" align="right" headerAlign="center" allowSort="false">债券买入成本</div>
            <div field="settavgcost" name="settAvgCost" width="120" align="right" headerAlign="center" allowSort="false">债券投资成本</div>
            <div field="gz" name="gz" width="100" align="right" headerAlign="center" allowSort="false">估值</div>
            <div field="opicsjjsz" name="opicsJjsz" width="120" align="right" headerAlign="center" allowSort="false">账面净价市值(元)</div>
            <div field="jjsz" name="jjsz" width="120" align="right" headerAlign="center" allowSort="false">净价市值(元)</div>
            <div field="yzj" name="yzj" width="120" align="right" headerAlign="center" allowSort="false">OPICS_溢折价(元）</div>
            <div field="windyzj" name="windYzj" width="120" align="right" headerAlign="center" allowSort="false">溢折价(元）</div>
            <div field="lxtz" name="lxtz" width="100" align="right" headerAlign="center" allowSort="false">利息调整(元)</div>
            <div field="syl" name="syl" width="100" align="right" headerAlign="center" allowSort="false">到期收益率</div>
            <div field="jqcbper" name="jqcbper" width="100" align="right" headerAlign="center" allowSort="false">百元加权成本</div>
            <div field="jjcbper" name="jjcbper" width="100" align="right" headerAlign="center" allowSort="false">百元净价成本</div>
            <div field="yslxper" name="yslxper" width="100" align="right" headerAlign="center" allowSort="false">百元应收利息</div>

            <div field="yjlxper" name="yjlxper" width="100" align="right" headerAlign="center" allowSort="false">百元应计利息</div>
            <div field="tyjj" name="tyjj" width="120" align="right" headerAlign="center" allowSort="false">摊余净价(元)</div>
            <div field="jjfy" name="jjfy" width="120" align="right" headerAlign="center" allowSort="false">OPICS_净价浮赢(元)</div>
            <div field="windJjfy" name="yslxPer" width="120" align="right" headerAlign="center" allowSort="false">净价浮赢(元)</div>
            <div field="synx" name="synx" width="80" align="right" headerAlign="center" allowSort="false">剩余年限</div>
            <div field="fxSyts" name="fxSyts" width="100" align="center" headerAlign="center" allowSort="false">付息剩余天数</div>
            <div field="cdjr" name="cdjr" width="120" align="center" headerAlign="center" allowSort="false">最近利率重定价日</div>
            <div field="nx" name="nx" width="80" align="center" headerAlign="center" allowSort="false">年限</div>
            <div field="fxrq" name="fxrq" width="120" align="center" headerAlign="center" allowSort="false">发行日期</div>
            <div field="fxl" name="fxl" width="80" align="center" headerAlign="center" allowSort="false">付息类</div>
            <div field="fxpl" name="fxpl" width="80" align="center" headerAlign="center" allowSort="false">付息频率</div>

            <div field="fdlx" name="fdlx" width="80" align="center" headerAlign="center" allowSort="false">浮动类型</div>
            <div field="jzll" name="jzll" width="80" align="right" headerAlign="center" allowSort="false">基准利率</div>
            <div field="fdjd" name="fdjd" width="80" align="right" headerAlign="center" allowSort="false">浮动基点</div>
            <div field="lc" name="lc" width="80" align="right" headerAlign="center" allowSort="false">利差</div>

            <div field="hqz" name="hqz" width="80" align="center" headerAlign="center" allowSort="false">含权类</div>
            <div field="ifName" name="ifName" width="250" align="center" headerAlign="center" allowSort="false">发行人</div>
            <div field="ptz" name="ptz" width="100" align="center" headerAlign="center" allowSort="false">是否平台债</div>
            <div field="db" name="db" width="80" align="center" headerAlign="center" allowSort="false">是否担保</div>
            <div field="dbfs" name="dbfs" width="100" align="center" headerAlign="center" allowSort="false">担保方式</div>
            <div field="dbName" name="dbName" width="100" align="center" headerAlign="center" allowSort="false">担保人</div>
            <div field="rbamount" name="rbamount" width="120" align="right" headerAlign="center" allowSort="false">买断抵押面额(元)</div>
            <div field="rpamount" name="rpamount" width="120" align="right" headerAlign="center" allowSort="false">质押面额(元)</div>
            <div field="rdamount" name="rdamount" width="120" align="right" headerAlign="center" allowSort="false">国库定期存款质押面额</div>
            <div field="me" name="me" width="120" align="right" headerAlign="center" allowSort="false">可用面额(元)</div>
            <div field="bz" name="bz" width="100" align="center" headerAlign="center" allowSort="false">备注</div>
        </div>
    </div>
</div>
<script>

    mini.parse();
    var grid = mini.get("slcd_grid");
    var form = new mini.Form("#search_form");
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var id = params.id;
    var currTab = top["win"].tabs.getActiveTab();
    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        queryPage(pageSize, pageIndex);
    });

    /**时间格式化**/
    function onDateRenderer(e) {
        var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value) == null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g, '/'));
        if (value) return mini.formatDate(value, 'yyyy-MM-dd');
    }

    $(document).ready(function() {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(currTab.parentId)).done(function(visibleBtn) {
        });
    });



    function query(){
        queryPage(10,0);
    }
    //生成数据
    function queryPage(pageSize,pageIndex) {
        var searchUrl = "/CombinePositionReportController/searchIfsReportHbyuze";

        var data = form.getData(true);
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        var params = mini.encode(data);

        CommonUtil.ajax({
            url: searchUrl,
            data: params,
            callback: function (data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }

    //清空按钮
    function clear() {
        form.clear();
    }

    //导出
    function exportExcel(){
        var content = grid.getData();

        if(content.length === 0){
            mini.alert("请先生成数据!");
            return;
        }

        var data = form.getData(true);
        var fields = null;
        var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
        fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
        exportHrbExcel(fields);
    }

</script>
</body>
</html>
