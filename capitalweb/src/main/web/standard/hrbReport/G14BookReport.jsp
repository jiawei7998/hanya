<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<title>G14台账明细</title>
</head>
<body>
    <fieldset class="mini-fieldset title">
        <div id="search_form" style="width:100%">
            <legend>报表查询</legend>
            <input id="queryDate" name="queryDate" field="queryDate" class="mini-datepicker" labelField="true"  value="<%=__bizDate%>" label="日期："
            required="true"  labelStyle="text-align:center;" allowinput="false" labelStyle="width:100px" width="280px" onvaluechanged="search(10,0)"  />
            <span>
                <a class="mini-button" style="display: none"  id="search_btn" onclick="query()">查询</a>
                <a class="mini-button" style="display: none"  id="clear_btn" onclick="clear()">清空</a>
                <a id="export_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
            </span>
        </div>
    </fieldset>
    <div class="mini-fit" style="width:100%;height:100%;">
        <div id="loan_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
            sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
            border="true" allowResize="true">
            <div property="columns">
                <div type="sn" headerAlign="center" align="center" width="30px">客户全称</div>
                <div field="settamt" headerAlign="center" align="center" width="50px">余额/金额</div>
                <div field="acctngtype" headerAlign="center" align="center" width="50px">债券类型</div>
                <div field="cfn1" headerAlign="center" align="center" width="50px">所属集团</div>
                <div field="sic" headerAlign="center" align="center" width="50px">客户类型</div>
                <div field="bood" headerAlign="center" align="center" width="50px">债券投资</div>
                <div field="absamt" headerAlign="center" align="center" width="50px">资产证券化产品(abs)</div>
                
                </div>
        </div>
    </div>
    <script>
        
        mini.parse();

        var grid = mini.get("loan_grid");
        var form = new mini.Form("#search_form");
        
        grid.on("beforeload", function (e) {
            e.cancel = true;
            var pageIndex = e.data.pageIndex; 
            var pageSize = e.data.pageSize;
            search(pageSize,pageIndex);
        });
        
        $(document).ready(function() {
            //控制按钮显示
            $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
                query();
            });
        });
    
        //查询按钮
        function query(){
            search(10,0);
        }
        
        function search(pageSize, pageIndex) {
            var searchUrl="/G14BookReportCotroller/search";
             
            var data = form.getData(true);
            data['pageNum'] = pageIndex + 1;
            data['pageSize'] = pageSize;
    
            var params = mini.encode(data);
     
            CommonUtil.ajax({
                url:searchUrl,
                data:params,
                callback:function(data){
                    grid.setTotalCount(data.obj.total);
                    grid.setPageIndex(pageIndex);
                    grid.setPageSize(pageSize);
                    grid.setData(data.obj.rows);
                }
            });
        }
        
        //清空按钮
        function clear() {
            form.clear();
            query();
        }
    
        
        //导出
        function exportExcel(){
            var content = grid.getData();
            if(content.length == 0){
                mini.alert("请先查询数据");
                return;
            }

        
        mini.confirm("您确认要导出Excel吗?","系统提示", 
                function (action) {
                  if (action == "ok"){
                    var data = form.getData(true);
                    var fields = null;
                    
                    var  fields = "<input type='hidden' id='dealNo' name='dealNo' value='"+data.dealNo+"'>";
                      
                      fields += "<input type='hidden' id='excelName' name='excelName' value='g14zt.xls'>";
                      fields += "<input type='hidden' id='id' name='id' value='g14zt'>";
                    var urls = CommonUtil.pPath + "/sl/HrbReportManageController/exportExcel";                                                                                                                         
                    $('<form action="'+ urls +'" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();   
                  }
                }
        );
        }
           
        
        
    </script>
</body>
</html>
