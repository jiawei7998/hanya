<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
<head>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>

<body style="width: 100%; height: 100%; background: white">
<div class="mini-panel" title="查询条件" style="width:100%;"  allowResize="true" collapseOnTitleClick="true">
    <div id="search_form" style="width: 100%;padding-top:10px;">
        <input id="reportCode" name="reportCode" class="mini-textbox" width="320px"  labelField="true" label="报表编号：" labelStyle="text-align:right;" emptyText="请填写报表关键编号" />
        <input id="reportNmae" name="reportNmae" class="mini-textbox" width="320px"  labelField="true" label="报表名称：" labelStyle="text-align:right;" emptyText="请填写报表关键名称" />
        <input id="reportInst" name="reportInst" class="mini-textbox" width="320px"  labelField="true" label="报表机构：" labelStyle="text-align:right;" emptyText="请填写报表报表机构" />

        <span style="float: right; margin-right: 150px">
            <a id="search_btn" class="mini-button" style=""   onclick="query()">查询</a>

            <a id="clear_btn" class="mini-button" style=""   onclick="clear()">清空</a>

        </span>
    </div>
</div>
<div class="mini-fit">
<div id="grid1" class="mini-datagrid borderAll" style="width:80%;height:100%;" idField="id" multiSelect="true"
     allowAlternating="true" allowResize="true" sortMode="client" allowAlternating="true">
    <div property="columns">
<%--        <div type="indexcolumn" width="50px" headerAlign="center">序号</div>--%>
        <div field="reportCode" width="100px" align="left"  headerAlign="center" >报表编号</div>
        <div field="reportNmae" width="350px" align="left"  headerAlign="center" >报表名称</div>
<%--        <div field="moduleUrl" width="150"  headerAlign="center" align="center" >报表名称</div>--%>
        <div field="reportInst" width="150px"  headerAlign="center" align="center" >机构</div>
    <%--        <div field="reportType" width="120"  headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'YesNo'}">是否有详情展示</div>--%>
<%--        <div field="activeFlag" width="120"  headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'YesNo'}">是否启用</div>--%>
<%--        <div field="reportServer" width="150"  headerAlign="center" align="center" >报表实现类</div>--%>
    </div>
</div>
</div>
<script>
    mini.parse();

    //获取当前tab
    var grid = mini.get("grid1");
    grid.on("beforeload", function(e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize, pageIndex);
    });

    grid.on("drawcell",function(e){
        var record = e.record,
            column = e.column,
            field = e.field,
            value = e.value;
        // if (field == "moduleUrl"&&value!=""&&value!=null) {
        if (field == "reportNmae"&&value!=""&&value!=null) {
            e.cellStyle = "text-align:center";
            e.cellHtml = '<a href="javascript:openTab(\'' +record.moduleUrl+','+record.reportNmae+','+record.id+','+record.reportServer+ '\')">'+value+'</a>&nbsp';
        }
    });
    $(document).ready(function() {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
            search(10, 0);
        });
    });

    top["taHrbReportManger"]=window;
    function getData(){
        var grid = mini.get("grid1");
        var record =grid.getSelected();
        return record;
    }
    // 初始化数据
    function query(){
        search(grid.pageSize, 0);
    }
    function search(pageSize,pageIndex){
        var form = new mini.Form("#search_form");
        
        var data=form.getData();
        data['pageNumber']=pageIndex+1;
        data['pageSize']=pageSize;
        data['branchId']=branchId;
        var url="/TaHrbReportController/searchTaHrbReportList";

        var params = mini.encode(data);
        CommonUtil.ajax({
            url:url,
            data:params,
            callback : function(data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }

    /**
     * 修改报表信息
     */
    function edit() {
        var grid = mini.get("grid1");
        var  record =grid.getSelected();
        if(record){
            var url =  '<%=basePath%>'+ "/standard/hrbReport/TaHrbReportEdit.jsp?action=edit";
            var tab = { id: "taHrbReportEdit", name: "taHrbReportEdit", text: "报表模板修改", url: url ,showCloseButton:true,parentId:top['win'].tabs.getActiveTab().name};
            top['win'].showTab(tab);
        }else{
            mini.alert("请选中一条记录","系统提示");
        }
    }
    function clear(){
        var form=new mini.Form("search_form");
        form.clear();
        query();
        // mini.get('reportNmae').setValue('');
    }

    function openTab(data){

        var str = data.split(",")
        var url = encodeURI(str[0]);
        var tab = {
            title : str[1],
            url : url,
            showCloseButton:true
        };
        var paramData ={
            id:str[2],
            implClass:str[3]
        };
        //解析URL中的参数存入paramData
        CommonUtil.getUrlParms(paramData,url);

        CommonUtil.openNewMenuTab(tab,paramData);
    }

</script>
</body>
</html>