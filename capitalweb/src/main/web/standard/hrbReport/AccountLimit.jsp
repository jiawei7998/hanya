<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
    <title>交易浮亏止损限额表</title>
</head>
<body>
    <fieldset class="mini-fieldset title">
        <div id="search_form" style="width:100%">
            <legend>报表查询</legend>
            <input id="queryDate" name="queryDate" class="mini-datepicker" labelField="true" label="交易日期:"
                    labelSyle="text-align:right;" emptyText="请输入" format="yyyy-MM-dd" value="<%=__bizDate%>"></input>
                <a class="mini-button" style="display: none"  id="search_btn" onclick="query()">查询</a>
<%--                <a class="mini-button" style="display: none"  id="clear_btn" onclick="clear()">清空</a>--%>
                <a id="export_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
            </span>
        </div>
    </fieldset>
<%--    <span style="float:right;">单位:万元</span>--%>
    <div class="mini-fit" style="width:100%;height:100%;">
        
        <div id="fund_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
             allowAlternating="true" border="true" allowResize="true" showPager="false" >
            <div property="columns" autoEscape="true">
                <div field="bondType" headerAlign="center" align="center">债券类型</div>
                <div field="faceAmt" headerAlign="center" align="center">面额</div>
                <div field="cost" headerAlign="center" align="center" >成本</div>
                <div field="marketValue" headerAlign="center" align="center" >市值</div>
                <div field="yzj" headerAlign="center" align="center" >溢折价</div>
                <div field="jcz" headerAlign="center" align="center">监测值(%)</div>
                <div field="xez" headerAlign="center" align="center" >限额值</div>
                <div field="xeyj" headerAlign="center" align="center" >限额预警(%)</div>
            </div>
        </div>
    </div>
    <script>
        mini.parse();
        var url = window.location.search;
        var currTab = top["win"].tabs.getActiveTab();
        var params = currTab.params;
        var id = params.id;
        var implClass = params.implClass;
        var fundgrid = mini.get("fund_grid");
        var form = new mini.Form("#search_form");

        $(document).ready(function() {
            //控制按钮显示
            $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
                query();
            });
        });
        //查询按钮
        function query(){
            var searchUrl="/ExportDataController/getExportListData";
            var data = form.getData(true);
            data.implClass=implClass;
            var params = mini.encode(data);
            CommonUtil.ajax({
                url:searchUrl,
                data:params,
                callback:function(data){
                    fundgrid.setData(data.obj);
                }
            });
        }

        function exportExcel(){
            var data = form.getData(true);
            var fields = null;
            var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
            fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
            exportHrbExcel(fields);
        }
    
    </script>
</body>
</html>
