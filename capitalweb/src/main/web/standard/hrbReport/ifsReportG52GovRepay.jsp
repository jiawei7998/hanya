<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
    <title>G14台账</title>
</head>
<body>
<fieldset class="mini-fieldset title">
    <div id="search_form" style="width:100%">
        <legend>报表查询</legend>
        <input id="queryDate" name="queryDate" class="mini-datepicker" labelField="true" label="交易日期:"
               value="<%=__bizDate%>" labelSyle="text-align:right;" emptyText="请输入"></input>
        <input id="sheetType" name="sheetType"  class="mini-combobox" data="CommonUtil.serverData.dictionary.dataType" labelField="true"
               label="类型:" labelSyle="text-align:right;" emptyText="请输入" ></input>
        <span>
            <a class="mini-button" style="display: none"  id="clear_btn" onclick="clear()">清空</a>
            <a class="mini-button" style="display: none"  id="create_btn" onclick="query()">查询</a>
            <a id="export_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
        </span>
    </div>
</fieldset>
<div class="mini-fit" style="width:100%;height:100%;">
    <div id="data_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
         sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
         border="true" allowResize="true">
        <div property="columns">
            <div field="seq" headerAlign="center" align="center" width="50px">序号</div>
            <div field="area" headerAlign="center" align="center" width="150px">地区</div>
            <div field="sum" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">合计</div>
            <div header="债务人层级" headerAlign="center">
                <div property="columns">
                    <div field="issLvProvince" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">省级</div>
                    <div field="issLvCity" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">地市级</div>
                    <div field="issLvCountry" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">区县级</div>
                </div>
            </div>
            <div header="资金来源" headerAlign="center">
                <div property="columns">
                    <div field="sourceLoan" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">贷款余额</div>
                    <div header="投资债券类融资工具" headerAlign="center">
                        <div property="columns">
                            <div field="sourceSecSum" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">小计</div>
                            <div header="按剩余期限" headerAlign="center">
                                <div property="columns">
                                    <div field="sourceSec1y" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">一年以内（含一年）</div>
                                    <div field="sourceSec5y" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">一年至五年（含五年）</div>
                                    <div field="sourceSec10y" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">五年至十年（含十年）</div>
                                    <div field="sourceSec10yup" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">十年以上</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div field="sourceBnQtxy" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">表内其他信用资产余额</div>
                    <div field="sourceBwXyye" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">表外信用余额</div>
                    <div header="资金来源" headerAlign="center">
                        <div property="columns">
                            <div field="sourceLcSum" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">小计</div>
                            <div header="资金来源" headerAlign="center">
                                <div property="columns">
                                    <div field="sourceLcSec" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">债券类</div>
                                    <div field="sourceLcUnstand" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">非标准化债权类资产</div>
                                    <div field="sourceLcRight" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">股权类</div>
                                    <div field="sourceLcOther" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">其他类</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div field="sourceLcXt" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">信托</div>
                    <div field="sourceRzzl" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">融资租赁</div>
                </div>
            </div>
            <div header="贷款情况" headerAlign="center">
                <div property="columns">
                    <div header="资产质量" headerAlign="center">
                        <div property="columns">
                            <div field="loanLvNormal" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">正常</div>
                            <div field="loanLvAttention" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">关注</div>
                            <div field="loanLvSub" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">次级</div>
                            <div field="loanLvKy" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">可疑</div>
                            <div field="loanLvLoss" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">损失</div>
                            <div field="loanLvPreloss" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">贷款损失准备金余额</div>
                        </div>
                    </div>
                    <div header="资产质量" headerAlign="center">
                        <div property="columns">
                            <div field="loanTenor1y" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">一年以内（含一年）</div>
                            <div field="loanTenor5y" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">一年至五年（含五年）</div>
                            <div field="loanTenor10y" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">五年至十年（含十年）</div>
                            <div field="loanTenor10yup" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">十年以上</div>
                        </div>
                    </div>
                    <div header="资产质量" headerAlign="center">
                        <div property="columns">
                            <div field="loanZjtxRailway" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">铁路</div>
                            <div field="loanZjtxRoad" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">公路</div>
                            <div field="loanZjtxHighspeed" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">其中：高速公路</div>
                            <div field="loanZjtxAirport" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">机场</div>
                            <div field="loanZjtxSzjs" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">市政建设</div>
                            <div field="loanZjtxLand" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">土地储备</div>
                            <div field="loanZjtxHouse" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">保障性住房</div>
                            <div field="loanZjtxPhq" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">其中：棚户区改造</div>
                            <div field="loanZjtxEnvir" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">生态建设和环境保护</div>
                            <div field="loanZjtxZqjs" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">政权建设</div>
                            <div field="loanZjtxEdu" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">教育</div>
                            <div field="loanZjtxSince" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">科学</div>
                            <div field="loanZjtxCul" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">文化</div>
                            <div field="loanZjtxYlws" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">医疗卫生</div>
                            <div field="loanZjtxShbz" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">社会保障</div>
                            <div field="loanZjtxFood" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">粮油物资储备</div>
                            <div field="loanZjtxNlsl" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">农林水利建设</div>
                            <div field="loanZjtxPort" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">港口</div>
                            <div field="loanZjtxGyx" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">其他公益性项目</div>
                            <div field="loanZjtxFgyx" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">非公益性项目</div>
                            <div field="loanZjtxFxm" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">非项目</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    mini.parse();

    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var id = params.id;

    var grid = mini.get("data_grid");
    var form = new mini.Form("#search_form");

    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize,pageIndex);
    });

    $(document).ready(function() {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
            mini.get("sheetType").setValue("1");
            query();
        });
    });

    //查询按钮
    function query(){
        search(100,0);
    }

    function search(pageSize, pageIndex) {
        var searchUrl="/IfsReportG52GovRepayController/searchAllPage";

        var data = form.getData(true);
        data['pageNum'] = pageIndex + 1;
        data['pageSize'] = pageSize;

        var params = mini.encode(data);

        CommonUtil.ajax({
            url:searchUrl,
            data:params,
            callback:function(data){
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }

    //清空按钮
    function clear() {
        form.clear();
        mini.get("sheetType").setValue("1");
        query();
    }

    function exportExcel(){
        var data = form.getData(true);
        var sheetTypeText=mini.get("sheetType").getText();
        var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
        fields += "<input type='hidden' id='sheetType' name='sheetType' value='"+data.sheetType+"'>";
        fields += "<input type='hidden' id='sheetTypeValue' name='sheetTypeValue' value='"+sheetTypeText+"'>";
        fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
        exportHrbExcel(fields);
    }

</script>
</body>
</html>
