<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
    <title>申购信息查询结果</title>
</head>
<body>
<fieldset class="mini-fieldset title">
    <div id="search_form" style="width:100%">
        <legend>报表查询</legend>
        <input id="queryDate" name="queryDate" class="mini-datepicker" labelField="true" label="交易日期:"
               value="<%=__bizDate%>" labelSyle="text-align:right;" emptyText="请输入"></input>
        <span>
            <a class="mini-button" style="display: none"  id="clear_btn" onclick="clear()">清空</a>
            <a class="mini-button" style="display: none"  id="create_btn" onclick="createData()">查询</a>
            <a id="export_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
        </span>
    </div>
</fieldset>
<div class="mini-fit" style="width:100%;height:100%;">
    <div id="data_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
         sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
         border="true" allowResize="true">
        <div property="columns">
            <div type="indexcolumn" width="50px" headerAlign="center">序号</div>
            <div field="depositCode" width="100" headerAlign="center" align="center"allowSort="true">工具代码</div>
            <div field="depositName" width="150" headerAlign="center" align="center" allowSort="true">工具简称</div>
            <div field="lendInst" width="150" allowSort="false" align="center" headerAlign="center" >申购机构</div>
            <div field="finType" width="100" headerAlign="center" align="center" allowSort="true" >申购机构类型</div>
            <div field="prodtype" width="100" headerAlign="center" align="center" allowSort="true" >工具类型</div>
            <div field="currency" width="100" headerAlign="center"align="center" align="center" >币种 </div>
            <div field="publishDate" width="100" headerAlign="center"align="center"  >发行日 </div>
            <div field="payDate" width="100" headerAlign="center" align="center"  >缴款日</div>
            <div field="duedate" width="100" headerAlign="center" align="center" >到期日</div>
            <div field="valueDate" width="100" headerAlign="center"align="center" >起息日</div>
            <div field="circulation" width="80" headerAlign="center" align="center" >流通范围</div>
            <div field="issMethod" width="80" headerAlign="center" align="center" >发行方式</div>
            <div field="inviMethod" width="80" headerAlign="center" align="center" >招标方式</div>
            <div field="depositTerm" width="80" headerAlign="center"align="center" >期限</div>
            <div field="interestType" width="80" headerAlign="center"align="center" >票息类型</div>
            <div field="actualAmount" width="120" headerAlign="center" align="center"  numberFormat="#,0.0000">实际发行量（亿元）)</div>
            <div field="totalTenderSuccessAmt" width="120" headerAlign="center" align="center" numberFormat="#,0.0000">成交量（亿元）</div>
            <div field="tenderMultiples" width="80" headerAlign="center"align="center" >投标倍数</div>
            <div field="publishPrice" width="120" headerAlign="center"align="right" numberFormat="#,0.0000" >发行价格（元）</div>
            <div field="compareRate" width="120" headerAlign="center"align="right" numberFormat="#,0.0000" >参考收益率</div>
            <div field="faceRate" width="120" headerAlign="center"align="right" numberFormat="#,0.0000" >票面利率(%)</div>
            <div field="baseRate" width="120" headerAlign="center"align="right" numberFormat="#,0.0000" >基准利率</div>
            <div field="spread" width="120" headerAlign="center"align="right" numberFormat="#,0.0000" >利差(BP)</div>
            <div field="lendTrad" width="100" headerAlign="center" align="center">提交用户</div>
            <div field="lendTel" width="120" headerAlign="center" align="center">电话</div>
            <div field="subject" width="120" headerAlign="center" align="center">科目字段</div>
            <div field="depositSubId" width="120" headerAlign="center" align="center">存款协议代码</div>
            <div field="interestAdj" width="120" headerAlign="center"align="right" numberFormat="#,0.0000" >利息调整</div>
            <div field="faceAmt" width="120" headerAlign="center"align="right" numberFormat="#,0.0000" >账面</div>
        </div>
    </div>
</div>
<script>
    mini.parse();

    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var id = params.id;

    var grid = mini.get("data_grid");
    var form = new mini.Form("#search_form");

    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize,pageIndex);
    });

    $(document).ready(function() {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
            query();
        });
    });

    //查询按钮
    function query(){
        search(100,0);
    }

    function search(pageSize, pageIndex) {
        var searchUrl="/IfsReportDepositInvestorReportController/searchDepositInvestor";

        var data = form.getData(true);
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;

        var params = mini.encode(data);

        CommonUtil.ajax({
            url:searchUrl,
            data:params,
            callback:function(data){
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }

    //清空按钮
    function clear() {
        form.clear();
        query();
    }

    function createData(){
        mini.confirm("确认生成？", "系统提示", function (value) {
            if (value == "ok") {
                form.validate();
                if (form.isValid() == false) {
                    mini.alert("信息填写有误，请重新填写", "系统提示");
                    return;
                }
                var data1 = form.getData(true);
                var params = mini.encode(data1);
                CommonUtil.ajax({
                    url : "/IfsReportDepositInvestorReportController/creatDepositInvestor",
                    data : params,
                    complete : function(data) {
                        var text = data.responseText;
                        var desc = JSON.parse(text);
                        // var content = desc.obj.retMsg;
                        // mini.alert(content);
                        search(100,0);
                    }
                });
            }
        });
    }

    function exportExcel(){
        var data = form.getData(true);
        var fields = null;
        var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
        fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
        exportHrbExcel(fields);
    }

</script>
</body>
</html>
