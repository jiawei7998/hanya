<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../global.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>限额预风险</title>
</head>
<body>
<fieldset class="mini-fieldset title">
    <div id="search_form" style="width:100%">
        <legend>报表查询</legend>
        <span>
                <a class="mini-button" id="save_btn" onclick="saveData()">保存</a>
                <a id="export_btn" class="mini-button" onclick="exportExcel()">导出报表</a>
            </span>
    </div>
</fieldset>
<div class="mini-fit" style="width:100%;height:100%;">
    <div id="curasset_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
         sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
         border="true" allowResize="true"
         idField="id"
         allowResize="true"
         allowCellEdit="true" allowCellSelect="true" multiSelect="true"
         editNextOnEnterKey="true" editNextRowCell="true"
    >
        <div property="columns">
            <div type="indexcolumn" headerAlign="center" align="center" width="30px">序号</div>
            <div field="secmType" headerAlign="center" align="center" width="50px">债券分类</div>
            <div header="组合久期">
                <div property="columns">
                    <div field="durationTest" headerAlign="center" align="center" width="50px">检测值
                        <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
                    </div>

                    <div field="durationLimit" headerAlign="center" align="center" width="50px">限额值
                        <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
                    </div>
                </div>
            </div>
            <div header="组合PVBP(%)">
                <div property="columns">
                    <div field="bpTest" headerAlign="center" align="center" width="50px">检测值
                        <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
                    </div>

                    <div field="bpLmit" headerAlign="center" align="center" width="50px">限额值
                        <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
                    </div>
                </div>
            </div>
            <div header="组合PV01(万元)">
                <div property="columns">
                    <div field="pvTest" headerAlign="center" align="center" width="50px">检测值
                        <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
                    </div>

                    <div field="pvLimit" headerAlign="center" align="center" width="50px">限额值
                        <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

    mini.parse();

    var grid = mini.get("curasset_grid");
    var form = new mini.Form("#search_form");

    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize, pageIndex);
    });

    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
            search(50, 0);
        });
    });

    function search(pageSize, pageIndex) {
        var searchUrl = "/LimitRiskController/searchPageLimit";

        var data = form.getData(true);
        data['pageNum'] = pageIndex + 1;
        data['pageSize'] = pageSize;

        var params = mini.encode(data);

        CommonUtil.ajax({
            url: searchUrl,
            data: params,
            callback: function (data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }


    //保存
    function saveData() {
        saveUrl = "/LimitRiskController/saveLimitRisk";
        var data = grid.getChanges();
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: saveUrl,
            data: params,
            callback: function (data) {
                mini.alert("保存成功", '提示信息', function () {
                    search(50, 0);
                });
            }
        });
    }


    // //清空按钮
    // function clear() {
    //     form.clear();
    //     query();
    // }


    // grid.on("celleditenter", function (e) {
    //     var index = grid.indexOf(e.record);
    //     if (index == grid.getData().length - 1) {
    //         var row = {};
    //         grid.addRow(row);
    //     }
    // });
    //
    // grid.on("beforeload", function (e) {
    //     if (grid.getChanges().length > 0) {
    //         if (confirm("有增删改的数据未保存，是否取消本次操作？")) {
    //             e.cancel = true;
    //         }
    //     }
    // });


    function exportExcel() {
        var content = grid.getData();
        if (content.length == 0) {
            mini.alert("请先查询数据");
            return;
        }
        mini.confirm("您确认要导出Excel吗?", "系统提示",
            function (action) {
                if (action == "ok") {
                    var data = form.getData(true);
                    var fields = null;

                    var fields = "<input type='hidden' id='excelName' name='excelName' value='xefx.xls'>";
                    fields += "<input type='hidden' id='id' name='id' value='xefx'>";
                    var urls = CommonUtil.pPath + "/sl/HrbReportManageController/exportExcel";
                    $('<form action="' + urls + '" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();
                }
            }
        );
    }

</script>
</body>
</html>
