<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
<head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>
<body style="width: 100%; height: 100%; background: white">
	<fieldset class="mini-fieldset"> 
		<legend>基金申购交易查询</legend>
		<div id="search_form" style="width: 100%;margin:5px 0 5px 0">
			<input id="dealno" name="dealno" class="mini-textbox" labelField="true" label="交易单号：" emptyText="请填写交易单号" labelStyle="text-align:right;" width="280px"/>
	        <input id="queryDate" name="queryDate" class="mini-datepicker" labelField="true" label="账务日期：" emptyText="账务日期" format="yyyy-MM-dd" value="<%=__bizDate%>"/>
	            		<span style="float: right; margin-left: 20px;margin-top:6px">
		        <a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
			    <a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
				<a id="export_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
		</span>
		</div>
	</fieldset>
	
	<div class="mini-fit" style="margin-top: 2px;">
	<div id="grid1" class="mini-datagrid borderAll" style="width: 100%; height: 100%;" allowAlternating="true"
		allowResize="true" border="true" sortMode="client" multiSelect="true">
		<div property="columns">
		      	  <div type="indexcolumn" width="50px" headerAlign="center">序号</div>
		      	  <div field="dealno" width="100px" headerAlign="center">交易单号</div>
		      	  <div field="fundCode" width="100" headerAlign="center" align="center"allowSort="true">工具代码</div>
                  <div field="fundName" width="150" headerAlign="center" align="center" allowSort="true">工具简称</div>
                  <div field="afpInst" width="150" allowSort="false" align="center" headerAlign="center" >申购机构</div>
                  <div field="afpInstType" width="100" headerAlign="center" align="center" allowSort="true" >申购机构类型</div>
                  <div field="fundType" width="100" headerAlign="center" align="center" allowSort="true" >工具类型</div>
                  <div field="ccy" width="100" headerAlign="center"align="center" align="center" >币种 </div>
                  <div field="releaseDate" width="100" headerAlign="center"align="center"  >发行日 </div> 
                  <div field="payMentDate" width="100" headerAlign="center" align="center"  >缴款日</div> 
                  <div field="secondSettlementDate" width="100" headerAlign="center" align="center" >到期日</div> 
                  <div field="firstSettlementDate" width="100" headerAlign="center"align="center" >起息日</div> 
                  <div field="issueType" width="80" headerAlign="center" align="center" >发行方式</div>
                  <div field="term" width="80" headerAlign="center"align="center" >期限</div> 
                  <div field="totalQty" width="120" headerAlign="center" align="center"  numberFormat="#,0.0000">实际发行量（亿元）)</div> 
                  <div field="trueQty" width="120" headerAlign="center" align="center" numberFormat="#,0.0000">成交量（亿元）</div>
                  <div field="pric" width="120" headerAlign="center"align="right" numberFormat="#,0.0000" >发行价格（元）</div> 
                  <div field="userName" width="100" headerAlign="center" align="center">提交用户</div> 
                  <div field="phone" width="120" headerAlign="center" align="center">电话</div> 
		</div>
	</div>
</div>
	
<script>
	mini.parse();

	var grid = mini.get("grid1");
	grid.on("beforeload", function(e) {
			e.cancel = true;
			var pageIndex = e.data.pageIndex;
			var pageSize = e.data.pageSize;
			search(pageSize, pageIndex);
		});
	$(document).ready(function() {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
			query();
		});
	});
	// 初始化数据
	function query(){
		search(grid.pageSize, 0);
	}
	function search(pageSize,pageIndex){
		var form = new mini.Form("#search_form");
		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		var url="/FundAfpReportController/search";
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:url,
			data:params,
			callback : function(data) {
				if(data){
					grid.setTotalCount(data.obj.total);
					grid.setPageIndex(pageIndex);
			        grid.setPageSize(pageSize);
					grid.setData(data.obj.rows);
				}
			}
		});
	}
	
	function clear(){
        var form=new mini.Form("search_form");
        form.clear();
        // mini.get("startDate").setValue(sysDate);
        // mini.get("endDate").setValue(sysDate);
        query();
	}
	
	//日期
	function onDrawDateStart(e) {
        var startDate = e.date;
        var endDate= mini.get("endDate").getValue();
        if(CommonUtil.isNull(endDate)){
        	return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }
	
	function onDrawDateEnd(e) {
        var endDate = e.date;
        var startDate = mini.get("startDate").getValue();
        if(CommonUtil.isNull(startDate)){
        	return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }
	//计算时间段长度
	function dateCalculation(){
		var start = mini.get("startDate").getValue();
		var end= mini.get("endDate").getValue();
		if(CommonUtil.isNull(start)||CommonUtil.isNull(end)){
			return;
		}
		var subDate= Math.abs(parseInt((end.getTime() - start.getTime())/1000/3600/24));
		if(subDate>30){
			mini.alert('查询时间大于一个月，请重新选择');
			return;
		}
	}
	
	//导出
	function exportExcel(){
		var content = grid.getData();
		if(content.length == 0){
			mini.alert("请先查询数据");
			return;
		}
		mini.confirm("您确认要导出Excel吗?","系统提示", 
			function (action) {
				if (action == "ok"){
					var form = new mini.Form("#search_form");
					var data=form.getData(true);
					var fields = null;
					
					var	fields = '<input type="hidden" id="' + id + '" name="'+ id +'" value="' + data[id] + '">';
					 fields += "<input type='hidden' id='excelName' name='excelName' value='hbjjsg.xls'>";
                     fields += "<input type='hidden' id='id' name='id' value='hbjjsg'>";
                     fields += "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
					
                     var urls = CommonUtil.pPath + "/sl/HrbReportManageController/exportExcel";                                                                                                                        
					$('<form action="'+ urls +'" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();   
				}
			}
		);
	}
</script>
</body>
</html>