<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
    <title>G1405</title>
</head>
<body>
<fieldset class="mini-fieldset title">
    <div id="search_form" style="width:100%">
        <legend>报表查询</legend>
        <input id="queryDate" name="queryDate" class="mini-datepicker" labelField="true"
               value="<%=__bizDate%>" label="交易日期:" labelSyle="text-align:right;" emptyText="请输入"></input>
        <span>
<%--            <a class="mini-button" style="display: none"  id="search_btn" onclick="query()">查询</a>--%>
            <a class="mini-button" style="display: none"  id="clear_btn" onclick="clear()">清空</a>
            <a id="create_btn" class="mini-button" style="display: none"   onclick="creatData()">查询</a>
            <a id="export_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
        </span>
    </div>
</fieldset>
<div class="mini-fit" style="width:100%;height:100%;">
    <span style="float:right;">单位:万元</span>
    <div id="revrepo_grid" class="mini-datagrid borderAll" style="width:100%;height:98%;"
         sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
         border="true" allowResize="true">
        <div property="columns" autoEscape="true">
            <div type="indexcolumn" headerAlign="center" align="center" width="30px">序号</div>
            <div field="custType"  headerAlign="center" align="left" width="200px"  allowSort="false">客户类型</div>
            <div field="custName"  headerAlign="center" align="left" width="200px"  allowSort="false">客户名称</div>
            <div field="custOrgcode"  headerAlign="center" align="left" width="200px"  allowSort="false">客户代码</div>
            <div header="考虑风险缓释作用的风险暴露">
                <div property="columns">
                    <div width="120" headerAlign="center">风险暴露总和
                        <div property="columns">
                            <div field="riskExpSum"  headerAlign="center" align="right" width="100px" numberFormat="#,0.00"  allowSort="false">合计</div>
                            <div field="riskExp"  headerAlign="center" align="right" width="150px" numberFormat="#,0.00"  allowSort="false">其中:不可豁免风险暴漏</div>
                        </div>
                    </div>
                    <div width="120" headerAlign="center">占一级资本净额比例
                        <div property="columns">
                            <div field="netCapSum"  headerAlign="center" align="right" width="100px" numberFormat="#,0.00"  allowSort="false">合计</div>
                            <div field="netCap"  headerAlign="center" align="right" width="150px" numberFormat="#,0.00"  allowSort="false">其中:不可豁免风险暴露</div>
                        </div>
                    </div>
                    <div width="120" headerAlign="center">风险暴露构成
                        <div property="columns">
                            <div field="genRiskSum"  headerAlign="center" align="right" width="100px" numberFormat="#,0.00"  allowSort="false">一般风险暴露</div>
                            <div field="specRiskSum"  headerAlign="center" align="right" width="100px" numberFormat="#,0.00"  allowSort="false">特定风险暴露</div>
                            <div field="tradeRiskExp"  headerAlign="center" align="right" width="120px" numberFormat="#,0.00"  allowSort="false">交易账簿风险暴露</div>
                            <div field="custRiskSum"  headerAlign="center" align="right" width="100px" numberFormat="#,0.00"  allowSort="false">交易对手信用风险暴露</div>
                            <div field="qzRiskExp"  headerAlign="center" align="right" width="100px" numberFormat="#,0.00"  allowSort="false">潜在风险暴露</div>
                            <div field="qtRiskExp"  headerAlign="center" align="right" width="100px" numberFormat="#,0.00"  allowSort="false">其他风险暴露</div>
                        </div>
                    </div>
                </div>
            </div>
            <div field="revRiskExp"  headerAlign="center" align="right" width="200px" numberFormat="#,0.00"  allowSort="false">风险缓释转出的风险暴露（转入为负数）</div>
            <div header="不考虑风险缓释作用的风险暴露">
                <div property="columns">
                    <div field="noneRevRisk"  headerAlign="center" align="right" width="200px" numberFormat="#,0.00"  allowSort="false">风险暴露总和</div>
                    <div field="noneRevRiskPer"  headerAlign="center" align="right" width="200px" numberFormat="#,0.00"  allowSort="false">占一级资本净额比例</div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

    mini.parse();
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var id = params.id;
    var grid = mini.get("revrepo_grid");
    var form = new mini.Form("#search_form");

    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize,pageIndex);
    });

    $(document).ready(function() {
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
            query();
        });
    });

    //查询按钮
    function query(){
        search(50,0);
    }

    function search(pageSize, pageIndex) {
        var searchUrl="/IfsReportG1402Controller/searchIfsReportG1402";

        var data = form.getData(true);
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;

        var params = mini.encode(data);

        CommonUtil.ajax({
            url:searchUrl,
            data:params,
            callback:function(data){
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);

                var marges =[
                    { rowIndex: 0, columnIndex: 1, rowSpan: 1, colSpan: 3 }
                ]
                grid.mergeCells(marges);
            }
        });
    }

    //清空按钮
    function clear() {
        form.clear();
        query();
    }

    function creatData() {
        mini.confirm("确认生成？", "系统提示", function (value) {
            if (value == "ok") {
                form.validate();
                if (form.isValid() == false) {
                    mini.alert("信息填写有误，请重新填写", "系统提示");
                    return;
                }
                var data1 = form.getData(true);
                var params = mini.encode(data1);
                CommonUtil.ajax({
                    url : "/IfsReportG1402Controller/creatG1402",
                    data : params,
                    complete : function(data) {
                        var text = data.responseText;
                        var desc = JSON.parse(text);
                        search(50,0);
                    }
                });
            }
        });
    }

    function exportExcel(){
        var data = form.getData(true);
        var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
        fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
        exportHrbExcel(fields);
    }

</script>
</body>
</html>
