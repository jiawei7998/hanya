<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<title>自营资金交易信息表</title>
</head>
<body>
    <fieldset class="mini-fieldset title">
    <div id="search_form" style="width:100%">
    <legend>报表查询</legend>
    <input id="queryDate" name="queryDate" field="queryDate" class="mini-datepicker" labelField="true"  value="<%=__bizDate%>" label="日期："
    required="true"  labelStyle="text-align:center;" allowinput="false" labelStyle="width:100px" width="280px" onvaluechanged="search(10,0)"  />
    <span>
        <a class="mini-button" style="display: none"  id="search_btn" onclick="query()">查询</a>
        <a class="mini-button" style="display: none"  id="clear_btn" onclick="clear()">清空</a>
        <a id="export_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
    </span>
</div>
    </fieldset>
    <div id="loan_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
            sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
            border="true" allowResize="true">
            <div property="columns">
            <div field="sysday" headerAlign="center" align="center" width="100px">采集日期</div>
            <div field="startdate" headerAlign="center" align="center" width="100px">起始日期</div>
            <div field="enddate" headerAlign="center" align="center" width="100px">到期日期</div>
            <div field="br" headerAlign="center" align="center" width="100px">银行机构代码</div>
            <div field="finance" headerAlign="center" align="center" width="100px">金融许可证号</div>
            <div field="br2" headerAlign="center" align="center" width="100px">内部机构号</div>
            <div field="banksn" headerAlign="center" align="center" width="120px">银行机构名称</div>
            <div field="branch" headerAlign="center" align="center" width="100px">行内归属部门</div>
            <div field="acctngtype" headerAlign="center" align="center" width="100px">帐户类型</div>
            <div field="dealno" headerAlign="center" align="center" width="100px">交易编号</div>
            <div field="business1" headerAlign="center" align="center" width="100px">业务中类</div>
            <div field="business2" headerAlign="center" align="center" width="100px">业务小类</div>
            <div field="type" headerAlign="center" align="center" width="100px">资金交易类型</div>
            <div field="prodtype" headerAlign="center" align="center" width="100px">银行产品</div>
            <div field="ccy" headerAlign="center" align="center" width="100px">币种</div>
            <div field="cno" headerAlign="center" align="center" width="100px">交易对手编号</div>
            <div field="finance" headerAlign="center" align="center" width="100px">交易对手金融机构许可证号</div>
            <div field="br3" headerAlign="center" align="center" width="100px">组织机构代码</div>
            <div field="taxes" headerAlign="center" align="center" width="100px">纳税识别码</div>
            <div field="sn" headerAlign="center" align="center" width="100px">交易对手名称</div>
            <div field="ccode" headerAlign="center" align="center" width="100px">交易对手国家</div>
            <div field="sd" headerAlign="center" align="center" width="100px">交易对手行业</div>
            <div field="custGrade" headerAlign="center" align="center" width="100px">交易对手评级</div>
            <div field="custGradeOrgan" headerAlign="center" align="center" width="100px">交易对手评级机构</div>
            <div field="baseno" headerAlign="center" align="center" width="100px">基础资产编号</div>
            <div field="baseName" headerAlign="center" align="center" width="100px">基础资产名称</div>
            <div field="baseGrade" headerAlign="center" align="center" width="100px">基础资产评级</div>
            <div field="baseGradeOrgan" headerAlign="center" align="center" width="100px">基础资产评级机构</div>
            <div field="baseCustno" headerAlign="center" align="center" width="100px">基础资产客户编号</div>
            <div field="baseCustsn" headerAlign="center" align="center" width="100px">基础资产客户名称</div>
            <div field="baseCustState" headerAlign="center" align="center" width="100px">基础资产客户国家</div>
            <div field="baseCustGrade" headerAlign="center" align="center" width="100px">基础资产客户评级</div>
            <div field="baseGrade" headerAlign="center" align="center" width="100px">基础客户评级机构</div>
            <div field="baseTrade" headerAlign="center" align="center" width="100px">基础资产或客户行业</div>
            <div field="flowType" headerAlign="center" align="center" width="100px">最终投向类型</div>
            <div field="flowTrade" headerAlign="center" align="center" width="100px">最终投向行业</div>
            <div field="dealdate" headerAlign="center" align="center" width="100px">交易日期</div>
            <div field="dealtime" headerAlign="center" align="center" width="100px">交易时间</div>
            <div field="ccysettdate" headerAlign="center" align="center" width="100px">交割日期</div>
            <div field="vdate" headerAlign="center" align="center" width="100px">起息日</div>
            <div field="mdate" headerAlign="center" align="center" width="100px">到期日</div>
            <div field="al" headerAlign="center" align="center" width="100px">交易方向</div>
            <div field="ccyamt" headerAlign="center" align="center" width="100px">成交面额</div>
            <div field="contracAmt" headerAlign="center" align="center" width="100px">合同金额</div>
            <div field="intrate" headerAlign="center" align="center" width="100px">年化利率</div>
            <div field="voper" headerAlign="center" align="center" width="100px">审核人</div>
            <div field="trad" headerAlign="center" align="center" width="100px">交易柜员</div>
            <div field="Clno" headerAlign="center" align="center" width="100px">本方清算账号</div>
            <div field="Clbr" headerAlign="center" align="center" width="100px">本方清算行号</div>
            <div field="OClno" headerAlign="center" align="center" width="100px">对方清算账号</div>
            <div field="OClbr" headerAlign="center" align="center" width="100px">对方清算行号</div>
        
            
        

            </div>
        </div>
    </div>
    <script>
        
        mini.parse();

        var grid = mini.get("loan_grid");
        var form = new mini.Form("#search_form");
        
        grid.on("beforeload", function (e) {
            e.cancel = true;
            var pageIndex = e.data.pageIndex; 
            var pageSize = e.data.pageSize;
            search(pageSize,pageIndex);
        });
        
        $(document).ready(function() {
            //控制按钮显示
            $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
                query();
            });
        });

        //查询按钮
        function query(){
            search(10,0);
        }
        
        function search(pageSize, pageIndex) {
            var searchUrl="/PCapitalTransactionCotroller/search";
             
            var data = form.getData(true);
            data['pageNum'] = pageIndex + 1;
            data['pageSize'] = pageSize;
    
            var params = mini.encode(data);
     
            CommonUtil.ajax({
                url:searchUrl,
                data:params,
                callback:function(data){
                    grid.setTotalCount(data.obj.total);
                    grid.setPageIndex(pageIndex);
                    grid.setPageSize(pageSize);
                    grid.setData(data.obj.rows);
                }
            });
        }
        
        //清空按钮
        function clear() {
            form.clear();
            query();
        }
    
        //导出
        function exportExcel(){
            var content = grid.getData();
            if(content.length == 0){
                mini.alert("请先查询数据");
                return;
            }

        
        mini.confirm("您确认要导出Excel吗?","系统提示", 
                function (action) {
                  if (action == "ok"){
                    var data = form.getData(true);
                    var fields = null;
                    
                    var  fields = "<input type='hidden' id='dealNo' name='dealNo' value='"+data.dealNo+"'>";
                      
                      fields += "<input type='hidden' id='excelName' name='excelName' value='zyzjjy.xls'>";
                      fields += "<input type='hidden' id='id' name='id' value='zyzjjy'>";
                    var urls = CommonUtil.pPath + "/sl/HrbReportManageController/exportExcel";                                                                                                                         
                    $('<form action="'+ urls +'" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();   
                  }
                }
        );
        }    
        
        
        
        
    </script>
</body>
</html>
