<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../global.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
    <title>主要资产和负债业务周计划表</title>
</head>
<body>
<fieldset class="mini-fieldset title">
    <div id="search_form" style="width:100%">
        <legend>报表查询</legend>

        <input id="tzType" name="tzType"  class="mini-combobox" data="CommonUtil.serverData.dictionary.tzType" labelField="true"
               label="报表类型:" labelSyle="text-align:right;" emptyText="请输入"  width="400px" onvaluechanged="query()" value="KH"></input>


        <input id="queryDate" name="queryDate" field="queryDate" class="mini-datepicker" labelField="true"
               value="<%=__bizDate%>" label="日期："
               required="true" labelStyle="text-align:center;" allowinput="false" labelStyle="width:100px" width="280px"
              />
        <span>
<%--                <a class="mini-button" style="display: none" id="clear_btn" onclick="clear()">清空</a>--%>
                <a id="create_btn" class="mini-button" style="display: none" onclick="query()">查询</a>
                <a id="export_btn" class="mini-button" style="display: none" onclick="exportExcel()">导出报表</a>
            </span>
    </div>
</fieldset>
<span style="float:right;">单位:亿元</span>
<div class="mini-fit" style="width:100%;height:100%;">
    <div id="loan_grid1" class="mini-datagrid borderAll" style="width:100%;height:100%;"
         sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
         border="true" allowResize="true">
        <div property="columns">

            <div field="PRODUCT" width="100" headerAlign="center" align="center">部门</div>
            <div field="SRYE" width="150" headerAlign="center" align="center">上日余额（本周三）</div>
            <div field="BZRYE" width="150" headerAlign="center" align="center">本周日余额（预计数）</div>
            <div field="XZTFE" width="150" headerAlign="center" align="center">下周投放额（下周一-下周日）</div>
            <div field="XZDQE" width="150" headerAlign="center" align="center">下周到期额（下周一-下周日）</div>
            <div field="BNHSJE" width="250" headerAlign="center" align="center">其中：预计到期不能收回资金额度</div>
            <div field="XZMYE" width="100" headerAlign="center" align="center">下周末余额（下周日）</div>
            <div field="XZJZ" width="100" headerAlign="center" align="center">下周净增</div>
            <div field="SZRYE" width="100" headerAlign="center" align="center">上周日余额</div>
        </div>
    </div>


    <div id="loan_grid2" class="mini-datagrid borderAll" style="width:100%;height:100%;"
         sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
         border="true" allowResize="true">
        <div property="columns">
            <div field="PRODUCT" width="100" headerAlign="center" align="center">部门</div>
            <div field="SRYE" width="150" headerAlign="center" align="center">上日余额（本周三）</div>
            <div field="BZRYE" width="150" headerAlign="center" align="center">本周日余额（预计数）</div>
            <div field="XZMYE" width="100" headerAlign="center" align="center">下周末余额（下周日）</div>
            <div field="XZJZ" width="100" headerAlign="center" align="center">下周净增</div>
        </div>
    </div>


    <div id="loan_grid3" class="mini-datagrid borderAll" style="width:100%;height:100%;"
         sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
         border="true" allowResize="true">
        <div property="columns">
            <div field="PRODUCT" width="100" headerAlign="center" align="center">部门</div>
            <div field="SRYE" width="150" headerAlign="center" align="center">上日余额（本周三）</div>
            <div field="BZRYE" width="150" headerAlign="center" align="center">本周日余额（预计数）</div>
            <div field="XZZJE" width="100" headerAlign="center" align="center">下周增加额</div>
            <div field="XZMYE" width="100" headerAlign="center" align="center">下周末余额（下周日）</div>
            <div field="XZJZ" width="100" headerAlign="center" align="center">下周净增</div>
        </div>
    </div>

</div>F
<script>

    mini.parse();

    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var id = params.id;
    var implClass = params.implClass;


    var grid1 = mini.get("loan_grid1");
    var grid2 = mini.get("loan_grid2");
    var grid3 = mini.get("loan_grid3");
    var form = new mini.Form("#search_form");

    grid1.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search1(pageSize, pageIndex);
    });

    grid2.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search2(pageSize, pageIndex);
    });
    grid3.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search3(pageSize, pageIndex);
    });



    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
            query();
        });
    });

    //查询按钮
    function query() {
        var tableType = mini.get("tzType").value;

        if(tableType=='FZ'){
            search1(20,0)
        }
        if (tableType=='GBM'){
            search2(20, 0);
        }
        if (tableType=='KH'){
            search3(20, 0);
        }
    }



    function search1(pageSize, pageIndex) {
        var searchUrl = "/NewCapitalTurnoverController/search";
        var dealType=mini.get("tzType").getValue();
        var data = form.getData(true);
        data['pageNum'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        data['dealType'] = dealType;
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: searchUrl,
            data: params,
            callback: function (data) {
                grid1.setTotalCount(data.obj.total);
                grid1.setPageIndex(pageIndex);
                grid1.setPageSize(pageSize);
                grid1.setData(data.obj.rows);
                grid3.hide();
                grid2.hide();
                grid1.show();
            }
        });
    }

    function search2(pageSize, pageIndex) {
        var searchUrl = "/NewCapitalTurnoverController/search";
        var dealType=mini.get(tzType).getValue();
        var data = form.getData(true);
        data['pageNum'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        data['dealType'] = dealType;
        var params = mini.encode(data);

        CommonUtil.ajax({
            url: searchUrl,
            data: params,
            callback: function (data) {
                grid2.setTotalCount(data.obj.total);
                grid2.setPageIndex(pageIndex);
                grid2.setPageSize(pageSize);
                grid2.setData(data.obj.rows);
                grid1.hide();
                grid3.hide();
                grid2.show();
            }
        });
    }

    function search3(pageSize, pageIndex) {
        var searchUrl = "/NewCapitalTurnoverController/search";
        var dealType=mini.get(tzType).getValue();
        var data = form.getData(true);
        data['pageNum'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        data['dealType'] = dealType;
        var params = mini.encode(data);

        CommonUtil.ajax({
            url: searchUrl,
            data: params,
            callback: function (data) {
                grid3.setTotalCount(data.obj.total);
                grid3.setPageIndex(pageIndex);
                grid3.setPageSize(pageSize);
                grid3.setData(data.obj.rows);
                grid1.hide();
                grid2.hide();
                grid3.show();
            }
        });
    }



    // //清空按钮
    // function clear() {
    //     form.clear();
    //     query();
    // }

    // function getDate() {
    //     var data = form.getData(true);
    //     var params = mini.encode(data);
    //     CommonUtil.ajax({
    //         url: "/NewCapitalTurnoverController/call",
    //         data: params,
    //         callback: function (data) {
    //             if (data.code == 'error.common.0000') {
    //                 mini.alert("数据生成成功", "系统提示", function () {
    //                     query();
    //                 });
    //             } else {
    //                 mini.alert("数据生成失败", "系统提示");
    //             }
    //         }
    //     });
    // }


    //导出
  function exportExcel(){
            var dealtype = mini.get("tzType").getValue();
            var data = form.getData(true);
            var fields = null;
            var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
            fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
            fields += "<input type='hidden' id='sheetType' name='sheetType' value='" + dealtype + "'>";
            fields += "<input type='hidden' id='sheetTypeValue' name='sheetTypeValue' value='" + dealtype + "'>";
            exportHrbExcel(fields);
        }

</script>
</body>
</html>
