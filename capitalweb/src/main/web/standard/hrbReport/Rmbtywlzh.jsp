<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../global.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
    <title>2104人民币同业往来账户信息</title>
</head>
<body>
<fieldset class="mini-fieldset title">
    <div id="search_form" style="width:100%">
        <legend>报表查询</legend>
        <input id="queryDate" name="queryDate" class="mini-datepicker" labelField="true" label="交易日期:"
               labelSyle="text-align:right;" emptyText="请输入" format="yyyy-MM-dd" value="<%=__bizDate%>"></input>
        <a class="mini-button" style="display: none" id="search_btn" onclick="search()">查询</a>
        <a class="mini-button" id="save_btn" style="display: none" onclick="saveData()">保存</a>
        <a id="export_btn" class="mini-button" style="display: none" onclick="exportExcel()">导出报表</a>
        </span>
    </div>
</fieldset>
<span style="float:right;">单位:万元</span>
<div class="mini-fit" style="width:100%;height:100%;">

    <div id="fund_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
         sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
         border="true" allowResize="true"
         idField="id"
         allowResize="true"
         allowCellEdit="true" allowCellSelect="true" multiSelect="true"
         editNextOnEnterKey="true" editNextRowCell="true"
    >
        <div property="columns" autoEscape="true">
            <div field="sbhm" headerAlign="center" align="left" width="100px">申报号码</div>
            <div field="czlx" headerAlign="center" align="left" width="100px">操作类型
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="bgyy" headerAlign="center" align="left" width="100px">变更/撤销原因
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="yhjgdm" headerAlign="center" align="left" width="100px">银行机构代码
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="zhhzh" headerAlign="center" align="left" width="100px">主账户账号
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="bz" headerAlign="center" align="left" width="100px">币种
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="zhzt" headerAlign="center" align="left" width="100px">账户状态
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="ywsx" headerAlign="center" align="left" width="100px">业务属性
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="khrq" headerAlign="center" align="left" width="100px">开户日期
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="czrq" headerAlign="center" align="left" width="100px">操作日期
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="barq" headerAlign="center" align="left" width="100px">备案日期
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="mxczlx" headerAlign="center" align="left" width="100px">明细操作类型
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="xh" headerAlign="center" align="left" width="100px">序号
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="zzhzt" headerAlign="center" align="left" width="100px">账户状态
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="zzhzh" headerAlign="center" align="left" width="100px">子账户账号
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="zzhbz" headerAlign="center" align="left" width="100px">子账户币种
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="zzhkhrq" headerAlign="center" align="left" width="100px">子账户开户日期
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="zzhczrq" headerAlign="center" align="left" width="100px">子账户操作日期
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="gmjjbmfl" headerAlign="center" align="left" width="100px">国民经济部门分类
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="yhjgmc" headerAlign="center" align="left" width="100px">银行机构名称
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="yhjgyw" headerAlign="center" align="left" width="100px">银行机构英文名称
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="zcgj" headerAlign="center" align="left" width="100px">注册国家（地区）代码
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="yhjg" headerAlign="center" align="left" width="100px">境外银行机构SWIFT
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="lxrmc" headerAlign="center" align="left" width="100px">境外联系人名称
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="lxdh" headerAlign="center" align="left" width="100px">境外联系电话
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="jwbz" headerAlign="center" align="left" width="100px">境外备注
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="yhjgyb" headerAlign="center" align="left" width="100px">银行机构邮编
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="lxdz" headerAlign="center" align="left" width="100px">境外联系地址
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="yhbic" headerAlign="center" align="left" width="100px">银行SWIFT
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="jnlxrmc" headerAlign="center" align="left" width="100px">境内联系人名称
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="jnlxrdh" headerAlign="center" align="left" width="100px">境内联系人名称
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="jnlxrdz" headerAlign="center" align="left" width="100px">境内联系地址
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="yb" headerAlign="center" align="left" width="100px">境内邮编
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="dzyjdz" headerAlign="center" align="left" width="100px">境内电子邮件地址
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>

        </div>
    </div>
</div>
<script>
    mini.parse();
    var url = window.location.search;
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var id = params.id;
    var implClass = params.implClass;
    var grid = mini.get("fund_grid");
    var form = new mini.Form("#search_form");

    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize, pageIndex);
    });

    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function (visibleBtn) {
            search(10, 0);
        });
    });

    //查询按钮
    function search(pageSize, pageIndex) {
        var searchUrl = "/RmbtywlzhController/callRmbtywlzh";
        var data = form.getData(true);
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: searchUrl,
            data: params,
            callback: function (data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }

    //保存
    function saveData() {
        saveUrl = "/RmbtywlzhController/upDateRmbtywlzh";
        var data = grid.getChanges();
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: saveUrl,
            data: params,
            callback: function (data) {
                mini.alert("保存成功", '提示信息', function () {
                    search(50, 0);
                });
            }
        });
    }


    function exportExcel() {
        var data = form.getData(true);
        var fields = null;
        var fields = "<input type='hidden' id='queryDate' name='queryDate' value='" + data.queryDate + "'>";
        fields += "<input type='hidden' id='id' name='id' value='" + id + "'>";
        exportHrbExcel(fields);
    }

</script>
</body>
</html>
