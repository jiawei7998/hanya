<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
    <title>G14台账</title>
</head>
<body>
<fieldset class="mini-fieldset title">
    <div id="search_form" style="width:100%">
        <legend>报表查询</legend>
        <input id="queryDate" name="queryDate" class="mini-datepicker" labelField="true" label="交易日期:"
               value="<%=__bizDate%>" labelSyle="text-align:right;" emptyText="请输入"></input>
        <input id="sheetType" name="sheetType"  class="mini-combobox" data="CommonUtil.serverData.dictionary.InBankDeal" labelField="true"
               label="类型:" labelSyle="text-align:right;" emptyText="请输入" ></input>
        <span>
            <a class="mini-button" style="display: none"  id="clear_btn" onclick="clear()">清空</a>
            <a class="mini-button" style="display: none"  id="create_btn" onclick="query()">查询</a>
            <a id="export_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
        </span>
    </div>
</fieldset>
<div class="mini-fit" style="width:100%;height:100%;">
    <div id="repo_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
         sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
         border="true" allowResize="true">
        <div property="columns">
            <div field="seq" headerAlign="center" align="center" width="50px">序号</div>
            <div field="dealDir" headerAlign="center" align="center" width="150px">交易方向</div>
            <div field="vdate" headerAlign="center" align="center" width="150px">首次结算日</div>
            <div field="tenor" headerAlign="center" align="center" width="150px">实际占款天数</div>
            <div field="mdate" headerAlign="center" align="center" width="150px">到期结算日</div>
            <div field="repoRate" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">回购利率</div>
            <div field="amt" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">交易金额</div>
            <div field="pledgeDetail" headerAlign="center" align="center" width="150px">质押物明细</div>
        </div>
    </div>

    <div id="rvrepo_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
         sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
         border="true" allowResize="true">
        <div property="columns">
            <div field="seq" headerAlign="center" align="center" width="50px">序号</div>
            <div field="dealDir" headerAlign="center" align="center" width="150px">交易方向</div>
            <div field="vdate" headerAlign="center" align="center" width="150px">首次结算日</div>
            <div field="tenor" headerAlign="center" align="center" width="150px">实际占款天数</div>
            <div field="mdate" headerAlign="center" align="center" width="150px">到期结算日</div>
            <div field="repoRate" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">回购利率</div>
            <div field="repoDay" headerAlign="center" align="center" width="150px">回购天数</div>
            <div field="amt" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">交易金额</div>
            <div field="pledgeDetail" headerAlign="center" align="center" width="150px">质押物明细</div>
        </div>
    </div>

    <div id="sectpos_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
         sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
         border="true" allowResize="true">
        <div property="columns">
            <div field="seq" headerAlign="center" align="center" width="50px">序号</div>
            <div field="secid" headerAlign="center" align="center" width="150px">债券代码</div>
            <div field="secName" headerAlign="center" align="center" width="150px">债券名称</div>
            <div field="vdate" headerAlign="center" align="center" width="150px">起息日</div>
            <div field="tenor" headerAlign="center" align="center" width="150px">到期日</div>
            <div field="faceAmt" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">债券面值</div>
            <div field="faceRate" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">票面利率</div>
            <div field="isPledge" headerAlign="center" align="center" width="150px">是否质押</div>
            <div field="secType" headerAlign="center" align="center" width="150px">债券类型</div>
        </div>
    </div>

    <div id="fund_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
         sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
         border="true" allowResize="true">
        <div property="columns">
            <div field="seq" headerAlign="center" align="center" width="50px">序号</div>
            <div field="fundManager" headerAlign="center" align="center" width="150px">基金管理人</div>
            <div field="fundName" headerAlign="center" align="center" width="150px">基金简称</div>
            <div field="fundCode" headerAlign="center" align="center" width="150px">基金代码</div>
            <div field="dealDir" headerAlign="center" align="center" width="150px">交易方向</div>
            <div field="dealDate" headerAlign="center" align="center" width="150px">交易日期</div>
            <div field="amt" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">余额(万元)</div>
        </div>
    </div>

    <div id="cdtpos_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
         sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
         border="true" allowResize="true">
        <div property="columns">
            <div field="seq" headerAlign="center" align="center" width="50px">序号</div>
            <div field="secid" headerAlign="center" align="center" width="150px">存单代码</div>
            <div field="secName" headerAlign="center" align="center" width="150px">存单名称</div>
            <div field="vdate" headerAlign="center" align="center" width="150px">起息日</div>
            <div field="tenor" headerAlign="center" align="center" width="150px">期限</div>
            <div field="mdate" headerAlign="center" align="center" width="150px">到期日</div>
            <div field="faceAmt" headerAlign="center" align="center" width="150px">债券面值</div>
            <div field="field" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">到期收益率</div>
        </div>
    </div>

    <div id="deposit_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
         sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
         border="true" allowResize="true">
        <div property="columns">
            <div field="seq" headerAlign="center" align="center" width="50px">序号</div>
            <div field="cust"  headerAlign="center" align="center" width="50px">交易对手</div>
            <div field="vdate"  headerAlign="center" align="center" width="50px">业务开始日期</div>
            <div field="mdate"  headerAlign="center" align="center" width="50px">业务结束日期</div>
            <div field="rate"  headerAlign="center" align="center" width="50px" numberFormat="n8">利率(%)</div>
            <div field="amt"  headerAlign="center" align="center" width="50px" numberFormat="#,0.00">金额(元)</div>
        </div>
    </div>

    <div id="cdiss_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
         sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
         border="true" allowResize="true">
        <div property="columns">
            <div field="seq" headerAlign="center" align="center" width="50px">序号</div>
            <div field="secid" headerAlign="center" align="center" width="150px">存单代码</div>
            <div field="vdate" headerAlign="center" align="center" width="150px">起息日</div>
            <div field="mdate" headerAlign="center" align="center" width="150px">到期日</div>
            <div field="tenor" headerAlign="center" align="center" width="150px">期限</div>
            <div field="field" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">到期收益率</div>
            <div field="issAmt" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">实际发行量(亿元)</div>
        </div>
    </div>
</div>
<script>
    mini.parse();

    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var id = params.id;

    var repo_grid = mini.get("repo_grid");
    var rvrepo_grid = mini.get("rvrepo_grid");
    var sectpos_grid = mini.get("sectpos_grid");
    var fund_grid = mini.get("fund_grid");
    var cdtpos_grid = mini.get("cdtpos_grid");
    var deposit_grid = mini.get("deposit_grid");
    var cdiss_grid = mini.get("cdiss_grid");
    var form = new mini.Form("#search_form");

    repo_grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        searchRepo(pageSize,pageIndex);
    });

    rvrepo_grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        searchRvRepo(pageSize,pageIndex);
    });

    sectpos_grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        searchSectpos(pageSize,pageIndex);
    });

    fund_grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        searchFund(pageSize,pageIndex);
    });

    cdtpos_grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        searchCdtpos(pageSize,pageIndex);
    });

    deposit_grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        searchDeposit(pageSize,pageIndex);
    });

    cdiss_grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        searchCdiss(pageSize,pageIndex);
    });


    $(document).ready(function() {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
            mini.get("sheetType").setValue("FUND");
            query();
        });
    });

    //查询按钮
    function query(){
        var dealtype = mini.get("sheetType").value;
        if(dealtype == 'REPO') {
            //卖出回购
            searchRepo(10,0);
        }
        if(dealtype == 'REVREPO') {
            //买入返售
            searchRvRepo(10,0);
        }
        if(dealtype == 'CDTPOS') {
            //同业存单投资
            searchCdtpos(10,0);
        }
        if(dealtype == 'SEC') {
            //债券投资
            searchSectpos(10,0);
        }
        if(dealtype == 'CT' ||dealtype == 'TC' ||dealtype == 'CC' ||dealtype == 'CR') {
            //拆借/存款
            searchDeposit(10,0);
        }
        if(dealtype == 'CDISS' ) {
            //同业存单发行
            searchCdiss(10,0);
        }
        if(dealtype == 'FUND' ) {
            //公募基金
            searchFund(10,0);
        }
    }

    function searchRepo(pageSize, pageIndex) {
        var searchUrl="/YjInnerbankController/searchRepoAllPage";

        var data = form.getData(true);
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;

        var params = mini.encode(data);

        CommonUtil.ajax({
            url:searchUrl,
            data:params,
            callback:function(data){
                repo_grid.setTotalCount(data.obj.total);
                repo_grid.setPageIndex(pageIndex);
                repo_grid.setPageSize(pageSize);
                repo_grid.setData(data.obj.rows);
                showTable(repo_grid);
            }
        });
    }
    function searchRvRepo(pageSize, pageIndex) {
        var searchUrl="/YjInnerbankController/searchRvrepoAllPage";

        var data = form.getData(true);
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;

        var params = mini.encode(data);

        CommonUtil.ajax({
            url:searchUrl,
            data:params,
            callback:function(data){
                rvrepo_grid.setTotalCount(data.obj.total);
                rvrepo_grid.setPageIndex(pageIndex);
                rvrepo_grid.setPageSize(pageSize);
                rvrepo_grid.setData(data.obj.rows);
                showTable(rvrepo_grid);
            }
        });
    }
    function searchSectpos(pageSize, pageIndex) {
        var searchUrl="/YjInnerbankController/searchSecTposAllPage";

        var data = form.getData(true);
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;

        var params = mini.encode(data);

        CommonUtil.ajax({
            url:searchUrl,
            data:params,
            callback:function(data){
                sectpos_grid.setTotalCount(data.obj.total);
                sectpos_grid.setPageIndex(pageIndex);
                sectpos_grid.setPageSize(pageSize);
                sectpos_grid.setData(data.obj.rows);
                showTable(sectpos_grid);
            }
        });
    }
    function searchFund(pageSize, pageIndex) {
        var searchUrl="/YjInnerbankController/searchFundAllPage";

        var data = form.getData(true);
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;

        var params = mini.encode(data);

        CommonUtil.ajax({
            url:searchUrl,
            data:params,
            callback:function(data){
                fund_grid.setTotalCount(data.obj.total);
                fund_grid.setPageIndex(pageIndex);
                fund_grid.setPageSize(pageSize);
                fund_grid.setData(data.obj.rows);
                showTable(fund_grid);
            }
        });
    }
    function searchCdtpos(pageSize, pageIndex) {
        var searchUrl="/YjInnerbankController/searchCdTposAllPage";

        var data = form.getData(true);
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;

        var params = mini.encode(data);

        CommonUtil.ajax({
            url:searchUrl,
            data:params,
            callback:function(data){
                cdtpos_grid.setTotalCount(data.obj.total);
                cdtpos_grid.setPageIndex(pageIndex);
                cdtpos_grid.setPageSize(pageSize);
                cdtpos_grid.setData(data.obj.rows);
                showTable(cdtpos_grid);
            }
        });
    }
    function searchDeposit(pageSize, pageIndex) {
        var searchUrl="/YjInnerbankController/searchDepositAllPage";

        var data = form.getData(true);
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;

        var params = mini.encode(data);

        CommonUtil.ajax({
            url:searchUrl,
            data:params,
            callback:function(data){
                deposit_grid.setTotalCount(data.obj.total);
                deposit_grid.setPageIndex(pageIndex);
                deposit_grid.setPageSize(pageSize);
                deposit_grid.setData(data.obj.rows);
                showTable(deposit_grid);
            }
        });
    }
    function searchCdiss(pageSize, pageIndex) {
        var searchUrl="/YjInnerbankController/searchCdIssAllPage";

        var data = form.getData(true);
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;

        var params = mini.encode(data);

        CommonUtil.ajax({
            url:searchUrl,
            data:params,
            callback:function(data){
                cdiss_grid.setTotalCount(data.obj.total);
                cdiss_grid.setPageIndex(pageIndex);
                cdiss_grid.setPageSize(pageSize);
                cdiss_grid.setData(data.obj.rows);
                showTable(cdiss_grid);
            }
        });
    }

    //清空按钮
    function clear() {
        form.clear();
        repo_grid.hide();
        rvrepo_grid.hide();
        sectpos_grid.hide();
        fund_grid.hide();
        cdtpos_grid.hide();
        deposit_grid.hide();
        cdiss_grid.hide();
        mini.get("sheetType").setValue("FUND");
        query();
    }

    //控制表格显示
    function showTable(grid) {
        repo_grid.hide();
        rvrepo_grid.hide();
        sectpos_grid.hide();
        fund_grid.hide();
        cdtpos_grid.hide();
        deposit_grid.hide();
        cdiss_grid.hide();
        grid.show();
    }

    function exportExcel(){
        var data = form.getData(true);
        var sheetTypeText=mini.get("sheetType").getText();
        var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
        fields += "<input type='hidden' id='sheetType' name='sheetType' value='"+data.sheetType+"'>";
        fields += "<input type='hidden' id='sheetTypeValue' name='sheetTypeValue' value='"+sheetTypeText+"'>";
        fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
        exportHrbExcel(fields);
    }

</script>
</body>
</html>