<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../global.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
    <title>中俄月报</title>
</head>
<body>
<fieldset class="mini-fieldset title">
    <div id="search_form" style="width:100%">
        <legend>报表查询</legend>
        <input id="queryDate" name="queryDate" field="queryDate" class="mini-datepicker" labelField="true"
               value="<%=__bizDate%>" label="日期："
               required="true" labelStyle="text-align:center;" allowinput="false" labelStyle="width:100px" width="280px"
               onvaluechanged="search(20,0)"/>

        <input id="area" name="area" class="mini-combobox" data="CommonUtil.serverData.dictionary.area" width="320px" emptyText="请选择地区" labelField="true"  label="地区：" value="滨海边疆" labelStyle="text-align:right;" />
        <span>
                <a class="mini-button" style="display: none" id="search_btn" onclick="query()">查询</a>
                <a class="mini-button" style="display: none" id="clear_btn" onclick="clear()">清空</a>
                <a id="create_btn" class="mini-button" style="display: none" onclick="getDate()">生成数据</a>
                <a id="export_btn" class="mini-button" style="display: none" onclick="exportExcel()">导出报表</a>
        </span>
    </div>
</fieldset>
<div class="mini-fit" style="width:100%;height:100%;">
    <div id="loan_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
         sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
         border="true" allowResize="true">
        <div property="columns">



            <div header="序号" headerAlign="center" align="center">
                <div property="columns">
                    <div field="SERIALNUMBER" headerAlign="center" align="center" width="120px"></div>
                </div>
            </div>

            <div header="" headerAlign="center" align="center">
                <div property="columns">
                    <div field="INDEX1" headerAlign="center" align="center" width="200px"></div>
                </div>
            </div>

            <div header="指标" headerAlign="center" align="center">
                <div property="columns">
                    <div field="INDEX2" headerAlign="center" align="center" width="120px"></div>
                </div>
            </div>
            <div header="" headerAlign="center" align="center">
                <div property="columns">
                    <div field="ROUBLE" headerAlign="center" align="center" width="120px">卢布</div>
                </div>
            </div>
            <div header="币种" headerAlign="center" align="center">
                <div property="columns">
                    <div field="YUAN" headerAlign="center" align="center" width="120px">元</div>
                </div>
            </div>
            <div header="" headerAlign="center" align="center">
                <div property="columns">
                    <div field="KHBZ" headerAlign="center" align="center" width="120px">可兑换币种</div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

    mini.parse();

    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var id = params.id;
    var implClass = params.implClass;
    var grid = mini.get("loan_grid");
    var form = new mini.Form("#search_form");

    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize, pageIndex);
    });

    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
            query();
        });
    });

    //查询按钮
    function query() {
        search(20, 0);
    }

    function search(pageSize, pageIndex) {
        var searchUrl = "/ChinaRussiaReportController/search";

        var data = form.getData(true);
        data['pageNum'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: searchUrl,
            data: params,
            callback: function (data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }

    //清空按钮
    function clear() {
        form.clear();
        query();
    }

    function getDate() {
        var data = form.getData(true);
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: "/ChinaRussiaReportController/call",
            data: params,
            callback: function (data) {
                if (data.code == 'error.common.0000') {
                    mini.alert("生成成功", "系统提示", function () {
                        search(20, 0);
                    });
                } else {
                    mini.alert("生成失败", "系统提示");
                }
            }
        });
    }


    //导出
    function exportExcel(){
        var data = form.getData(true);
        var fields = null;
        var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
        fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
         exportHrbExcel(fields);
    }

</script>
</body>
</html>
