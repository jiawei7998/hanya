<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
<head>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>

<body style="width: 100%; height: 100%; background: white">
<h1 style="text-align:center;padding-top:5px;font-size:18px;"><strong>国债免税配置</strong></h1>
<div id="field_form" class="mini-fit area" style="background:white;left: 25%">
    <div class="mini-panel" title="国债免税" style="width:50%;" allowResize="true" collapseOnTitleClick="true" autoEscape="true">
            <input style="width:80%;" id="createdate" name="createdate" class="mini-datepicker"
                   labelField="true"  label="生效日期：" labelStyle="text-align:left;width:180px;"/>
            <input style="width:80%;" id="bondType" name="bondType" class="mini-textbox"
                   labelField="true"  label="国债免税-债券类别" labelStyle="text-align:left;width:180px;" value="减免比例(%)" enabled="false"/>
            <input style="width:80%;" id="guozhai" name="guozhai" class="mini-spinner"
                   labelField="true"  label="&emsp;&emsp;国债：" labelStyle="text-align:left;width:180px;"/>
            <input style="width:80%;" id="zhengfuzhai" name="zhengfuzhai" class="mini-spinner"
                   labelField="true"  label="&emsp;&emsp;地方政府债：" labelStyle="text-align:left;width:180px;"/>
            <input style="width:80%;" id="tdz" name="tdz" class="mini-spinner"
                   labelField="true"  label="&emsp;&emsp;铁道债：" labelStyle="text-align:left;width:180px;"/>
            <input style="width:80%;" id="yingYesShui" name="yingYesShui" class="mini-textbox"
                   labelField="true"  label="营业税：" labelStyle="text-align:left;width:180px;" value="营业税率(%)" enabled="false"/>
            <input style="width:80%;" id="yingyeshui" name="yingyeshui" class="mini-spinner"
               labelField="true"  label="&emsp;&emsp;营业税：" labelStyle="text-align:left;width:180px;"/>
    </div>
</div>

<span style="margin:5px;display: block;">
        <a id="save_btn" class="mini-button" onclick="save()">保存</a>
    </span>
<script>
    mini.parse();
    var visibleBtn = CommonUtil.hideBtn();
    //获取当前tab
    var currTab = top["win"].tabs.getActiveTab();
    var param=currTab.params;
    var row=param?param.selectData[0]:{};
    var form = new mini.Form("field_form");
    $(document).ready(function() {

        if(row.id){
            row.bondType="减免比例(%)";
            row.yingYesShui="营业税率(%)";
            form.setData(row);
        }
    });

    //保存
    function save(){
        debugger;
        var data = form.getData(true);
        data.id=row.id;
        data.version=row.version;
        CommonUtil.ajax({
            url:"/KWabacusGuozhaimianshuiController/addOrUpdate",
            data:data,
            callback : function(data) {
                mini.alert("保存成功!");
                top["win"].closeMenuTab();
                // query();
            }
        });

    }

</script>
</body>
</html>