<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../global.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
    <title>G18_III地方政府自主发行债券持有情况统计表</title>
</head>
<body>
<fieldset class="mini-fieldset title">
    <div id="search_form" style="width:100%">
        <legend>报表查询</legend>
        <input id="queryDate" name="queryDate" class="mini-datepicker" labelField="true" label="交易日期:"
               labelSyle="text-align:right;" emptyText="请输入" format="yyyy-MM-dd" value="<%=__bizDate%>"></input>
        <a class="mini-button" style="display: none" id="search_btn" onclick="search()">查询</a>
        <%--                <a class="mini-button" style="display: none"  id="clear_btn" onclick="clear()">清空</a>--%>
        <a id="export_btn" class="mini-button" style="display: none" onclick="exportExcel()">导出报表</a>
        </span>
    </div>
</fieldset>
<span style="float:right;">单位:万元</span>
<div class="mini-fit" style="width:100%;height:100%;">
    <div id="fund_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
         allowAlternating="true" border="true" allowResize="true" showPager="false">
        <div property="columns" autoEscape="true">
            <div field="ser" headerAlign="center" align="left" width="100px">序号</div>
            <div field="dq" headerAlign="center" align="left" width="100px">地区</div>
            <div field="ye" headerAlign="center" align="left" width="100px">余额</div>
            <div header="按资金用途" headerAlign="center">
                <div property="columns">
                    <div field="ybzq" headerAlign="center" align="left" width="100px">一般债券</div>
                    <div field="zxzq" headerAlign="center" align="left" width="100px">专项债券</div>
                </div>
            </div>
            <div header="按发行方式" headerAlign="center">
                <div property="columns">
                    <div field="gkfxzq" headerAlign="center" align="left" width="100px">公开发行债券</div>
                    <div field="dxcxzq" headerAlign="center" align="left" width="100px">定向承销债券</div>
                </div>
            </div>
            <div header="按利率水平" headerAlign="center">
                <div property="columns">
                    <div field="l15" headerAlign="center" align="left" width="250px">较基准国债平均收益率上浮15%以内（含15%）</div>
                    <div field="b30" headerAlign="center" align="left" width="250px">较基准国债平均收益率上浮15%至30%（含30%）</div>
                    <div field="g30" headerAlign="center" align="left" width="250px">较基准国债平均收益率上浮30%以上</div>
                </div>
            </div>
            <div header="按剩余期限" headerAlign="center">
                <div property="columns">
                    <div field="y1" headerAlign="center" align="left" width="150px">一年以内（含一年）</div>
                    <div field="y5" headerAlign="center" align="left" width="150px">一年至五年（含五年）</div>
                    <div field="y10" headerAlign="center" align="left" width="150px">五年至十年（含十年）</div>
                    <div field="gy10" headerAlign="center" align="left" width="150px">十年以上</div>
                </div>
            </div>


        </div>
    </div>
</div>
<script>
    mini.parse();
    var url = window.location.search;
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var id = params.id;
    var implClass = params.implClass;
    var grid = mini.get("fund_grid");
    var form = new mini.Form("#search_form");

    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize,pageIndex);
    });

    $(document).ready(function() {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
            search(10,0);
        });
    });

    //查询按钮
    function search(pageSize, pageIndex)  {
        var searchUrl="/DfzfzuController/callDfzfzu";
        var data = form.getData(true);
        var params = mini.encode(data);
        CommonUtil.ajax({
            url:searchUrl,
            data:params,
            callback:function(data){
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }

    function exportExcel(){
        var data = form.getData(true);
        var fields = null;
        var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
        fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
        exportHrbExcel(fields);
    }
</script>
</body>
</html>
