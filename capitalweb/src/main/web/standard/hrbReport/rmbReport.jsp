<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>本币交易情况报表</title>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
	<script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>

</head>
<body style="width: 100%; height: 100%; background: white">
<fieldset class="mini-fieldset">
	<legend>查询条件</legend>
	<div id="search_form" style="width: 70%;">
		<nobr>
		<input id="sdate" name="sdate" class="mini-datepicker" labelField="true" label="日期：" required="true"
					ondrawdate="onDrawDateStart" labelStyle="text-align:right;" emptyText="起始日期" format="yyyy-MM-dd" value="<%=__bizDate%>"/>
		<span>至</span>
		<input id="edate" name="edate" class="mini-datepicker"  required="true"
					ondrawdate="onDrawDateEnd" emptyText="结束日期" format="yyyy-MM-dd" value="<%=__bizDate%>"/>
		</nobr>
		<input id="type" name="type" class="mini-combobox"  data="CommonUtil.serverData.dictionary.DoubleSubject" value="1" emptyText="请选择账户类型"  label="账户类型：" width="280px" labelField="true" labelStyle="text-align:right;"/>
		<span style="float: right; margin-right: 50px">
			<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
			<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
			<a id="export_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
		</span>
	</div>
</fieldset>

	
<div class="mini-fit" style="margin-top: 2px;margin-bottom: 2px;">
	<div id="datagrid" class="mini-datagrid borderAll" style="width: 100%; height: 100%;" allowAlternating="true" multiSelect="true"
		allowResize="true" border="true" sortMode="client" multiSelect="true">
		<div property="columns">
			<div field="id" name="id" width="80" align="center" headerAlign="center" allowSort="false">交易号 </div>
			<div field="mdate" name="mdate" width="120" align="center" headerAlign="center" allowSort="false">交割日期  </div>
			<div field="product" name="product" width="100" align="center" headerAlign="center" allowSort="false">产品类型  </div>
			<div field="type" name="type" width="250" align="center" headerAlign="center" allowSort="false">业务类型  </div>
			<div field="acctdesc" name="acctdesc" width="150" align="center" headerAlign="center" allowSort="false">债券类别  </div>
			<div field="costdesc" name="costdesc" width="250" align="center" headerAlign="center" allowSort="false">成本中心  </div>
			<div field="dealtype" name="dealtype" width="100" align="center" headerAlign="center" allowSort="false">交易类型  </div>
			<div field="market" name="market" width="100" align="center" headerAlign="center" allowSort="false">市场  </div>
			<div field="secid" name="secid" width="100" align="center" headerAlign="center" allowSort="false">代码  </div>
			<div field="secname" name="secname" width="120" align="center" headerAlign="center" allowSort="false">名称  </div>
			<div field="ps" name="ps" width="80" align="center" headerAlign="center" allowSort="false">买卖方向  </div>
			<div field="smeans" name="smeans" width="100" align="center" headerAlign="center" allowSort="false">结算方式  </div>
			<div field="faceamt" name="faceamt" width="120" align="right" headerAlign="center" allowSort="false" numberFormat="#,0.0000">成交面额(亿) </div>
			<div field="cno" name="cno" width="200" align="center" headerAlign="center" allowSort="false">交易对手  </div>
			<div field="dealno" name="dealno" width="80" align="center" headerAlign="center" allowSort="false">成交单编号 </div>
			<div field="trad" name="trad" width="80" align="center" headerAlign="center" allowSort="false">交易员 </div>
			<div field="acctype" name="acctype" width="120" align="center" headerAlign="center" allowSort="false">账户类型  </div>
			<div field="inacctype" name="inacctype" width="120" align="center" headerAlign="center" allowSort="false">会计账户类型  </div>
			<div field="dealdate" name="dealdate" width="120" align="center" headerAlign="center" allowSort="false">交易日期  </div>
			<div field="intsmeans" name="intsmeans" width="100" align="center" headerAlign="center" allowSort="false">清算方式  </div>
			<div field="clprice" name="clprice" width="100" align="right" headerAlign="center" allowSort="false" numberFormat="#,0.0000">成交净价  </div>
			<div field="price" name="price" width="100" align="right" headerAlign="center" allowSort="false" numberFormat="#,0.0000">成交全价  </div>
			<div field="rate" name="rate" width="100" align="right" headerAlign="center" allowSort="false" numberFormat="#,0.0000">收益率(%)  </div>
			<div field="amt" name="amt" width="120" align="right" headerAlign="center" allowSort="false" numberFormat="#,0.0000">成交金额</div>
		</div>
	</div>
</div>

<script>
	mini.parse();
	var url = window.location.search;
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var id = params.id;
	var implClass = params.implClass;
	var grid = mini.get("datagrid");
	var userId='<%=__sessionUser.getUserId()%>';
	var form = new mini.Form("#search_form");
	
	grid.on("beforeload", function(e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex;
		var pageSize = e.data.pageSize;
		search(pageSize, pageIndex);
	});
	
	
	// 查询
	function search(pageSize,pageIndex){
		var form = new mini.Form("#search_form");
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统也提示");
			return;
		}
		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		data['implClass']=implClass;
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:"/ExportDataController/getExportPageData",
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
		
	function query() {
    	search(grid.pageSize, 0);
    }
	
	function clear(){
        var form=new mini.Form("search_form");
        form.clear();
        // search(10,0);
	}
	
	$(document).ready(function() {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn){
			search(10, 0);
		});
	});
	
	//交易日期
	function onDrawDateStart(e) {
        var startDate = e.date;
        var endDate= mini.get("edate").getValue();
        if(CommonUtil.isNull(endDate)){
        	return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }
	function onDrawDateEnd(e) {
        var endDate = e.date;
        var startDate = mini.get("sdate").getValue();
        if(CommonUtil.isNull(startDate)){
        	return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }
	
	//导出
	function exportExcel(){
		var data = form.getData(true);
		var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
		fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
		fields += "<input type='hidden' id='sdate' name='sdate' value='"+data.sdate+"'>";
		fields += "<input type='hidden' id='edate' name='edate' value='"+data.edate+"'>";
		fields += "<input type='hidden' id='type' name='type' value='"+data.type+"'>";

		exportHrbExcel(fields);
	}
</script>
</body>
</html>