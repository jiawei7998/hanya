<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../global.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
    <title>2102人民币购售业务信息</title>
</head>
<body>
<fieldset class="mini-fieldset title">
    <div id="search_form" style="width:100%">
        <legend>报表查询</legend>
        <input id="queryDate" name="queryDate" class="mini-datepicker" labelField="true" label="交易日期:"
               labelSyle="text-align:right;" emptyText="请输入" format="yyyy-MM-dd" value="<%=__bizDate%>"/>

        </input>

        <input id="fxdhType" name="fxdhType"  class="mini-combobox" data="CommonUtil.serverData.dictionary.fxdhType" labelField="true"
               label="报表类型:" labelSyle="text-align:right;" emptyText="请输入"  width="400px" onvaluechanged="query()" value="0001"></input>

        <a class="mini-button" style="display: none" id="search_btn" onclick="search()">查询</a>
        <a class="mini-button" id="save_btn" style="display: none" onclick="saveData()">保存</a>
        <a id="export_btn" class="mini-button" style="display: none" onclick="exportExcel()">导出报表</a>
        </span>
    </div>
</fieldset>
<span style="float:right;">单位:万元</span>
<div class="mini-fit" style="width:100%;height:100%;">
    <div id="fund_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
         sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
         border="true" allowResize="true"
         idField="id"
         allowResize="true"
         allowCellEdit="true" allowCellSelect="true" multiSelect="true"
         editNextOnEnterKey="true" editNextRowCell="true"
    >

        <div property="columns" autoEscape="true">
            <div field="sbhm" headerAlign="center" align="center" width="100px">申报号码</div>
            <div field="czlx" headerAlign="center" align="center" width="100px">操作类型   <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
                    </div>
            <div field="bgyy" headerAlign="center" align="center" width="100px">变更/撤销原因   <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
                    </div>
            <div field="sbrq" headerAlign="center" align="center" width="100px">申报日期   <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
                    </div>
            <div field="jnjgdm" headerAlign="center" align="center" width="100px">境内机构代码   <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
                    </div>
            <div field="jxlx" headerAlign="center" align="center" width="100px">交易类型
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px" data="CommonUtil.serverData.dictionary.fxdhType"/>
            </div>
            <div field="jnjgmc" headerAlign="center" align="center" width="100px">境内机构名称   <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
                    </div>
            <div field="jnjglx" headerAlign="center" align="center" width="100px">境内机构类型   <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
                    </div>
            <div field="jnztlx" headerAlign="center" align="center" width="100px">境外主体类型   <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
                    </div>
            <div field="jwztdm" headerAlign="center" align="center" width="100px">境外主体代码   <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
                    </div>
            <div field="jwztmc" headerAlign="center" align="center" width="100px">境外主体名称   <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
                    </div>
            <div field="rmbzhzh" headerAlign="center" align="center" width="100px">人民币账   <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
                    </div>
            <div field="rmbkjgslx" headerAlign="center" align="center" width="100px">人民币跨境购售类型   <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
                    </div>
            <div field="gsyt" headerAlign="center" align="center" width="100px">购售用途   <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
                    </div>
            <div field="ywlx" headerAlign="center" align="center" width="100px">业务种类   <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
                    </div>
            <div field="yhywbh" headerAlign="center" align="center" width="150PX">银行业务编号   <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
                    </div>
            <div field="jyfy" headerAlign="center" align="center" width="100px">交易附言   <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
                    </div>
            <div field="jqmxczlx" headerAlign="center" align="center" width="150PX">即期明细操作类型   <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
                    </div>
            <div field="jqxh" headerAlign="center" align="center" width="100px">即期序号   <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
                    </div>
            <div field="jqbz" headerAlign="center" align="center" width="150PX">即期买入币种   <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
                    </div>
            <div field="jqje" headerAlign="center" align="center" width="150PX">即期买入金额   <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
                    </div>
            <div field="jqjg" headerAlign="center" align="center" width="100px">即期价格   <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
                    </div>
            <div field="jqmcbz" headerAlign="center" align="center" width="150PX">即期卖出币种   <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
                    </div>
            <div field="jqmcje" headerAlign="center" align="center" width="150PX">即期卖出金额   <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
                    </div>
            <div field="jyr" headerAlign="center" align="center" width="100px">即期交易日   <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
                    </div>
            <div field="jsr" headerAlign="center" align="center" width="100px">即期结算日   <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
                    </div>
            <div field="yqmxczlx" headerAlign="center" align="center" width="150PX">远期明细操作类型   <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
                    </div>
            <div field="yqxh" headerAlign="center" align="center" width="100px">远期序号   <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
                    </div>
            <div field="yqmrbz" headerAlign="center" align="center" width="150PX">远期买入币种   <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
                    </div>
            <div field="yqmrje" headerAlign="center" align="center" width="150PX">远期买入金额   <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
                    </div>
            <div field="yqjg" headerAlign="center" align="center" width="100px">远期价格   <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
                    </div>
            <div field="yqmcbz" headerAlign="center" align="center" width="150PX">远期卖出币种   <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
                    </div>
            <div field="yqmcje" headerAlign="center" align="center" width="150PX">远期卖出金额   <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
                    </div>
            <div field="yqjyr" headerAlign="center" align="center" width="100px">远期交易日   <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
                    </div>
            <div field="yqdqr" headerAlign="center" align="center" width="100px">远期到期日   <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
                    </div>
            <div field="yqjsr" headerAlign="center" align="center" width="100px">远期结算日   <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
                    </div>
            <div field="dqczmx" headerAlign="center" align="center" width="150PX">掉期明细操作类型   <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
                    </div>
            <div field="dqxh" headerAlign="center" align="center" width="100px">掉期序号   <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
                    </div>
            <div field="dqjyr" headerAlign="center" align="center" width="100px">掉期交易日   <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
                    </div>
            <div field="dqydmrbz" headerAlign="center" align="center" width="150PX">远端买入(名义本金)币种   <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
                    </div>
            <div field="dqydmrje" headerAlign="center" align="center" width="150PX">远端买入(名义本金)金额   <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
                    </div>
            <div field="dqydmcbz" headerAlign="center" align="center" width="150PX">远端卖出(名义本金)币种   <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
                    </div>
            <div field="dqmcje" headerAlign="center" align="center" width="150PX">远端卖出(名义本金)金额   <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
                    </div>
            <div field="dqjdjgr" headerAlign="center" align="center" width="100px">近端交割日   <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
                    </div>
            <div field="dqjdjyjg" headerAlign="center" align="center" width="100px">近端交易价格   <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
                    </div>
            <div field="dqydjgr" headerAlign="center" align="center" width="100px">远端交割日   <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
                    </div>
            <div field="dqydjyjg" headerAlign="center" align="center" width="150PX">远端交易价格   <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
                    </div>

        </div>
    </div>
</div>
<script>
    mini.parse();
    var url = window.location.search;
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var id = params.id;
    var implClass = params.implClass;
    var grid = mini.get("fund_grid");
    var form = new mini.Form("#search_form");

    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize,pageIndex);
    });

    $(document).ready(function() {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
            search(10,0);
        });
    });
    //查询按钮
    function search(pageSize, pageIndex)  {
        var fxdh =mini.get("fxdhType").getValue();
        var searchUrl="/RmbgsywController/callRmbgsyw";
        var data = form.getData(true);
        data['fxdh']=fxdh;
        var params = mini.encode(data);

        CommonUtil.ajax({
            url:searchUrl,
            data:params,
            callback:function(data){
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }
    //保存
    function saveData() {
        saveUrl = "/RmbgsywController/upDateRmbgsyw";
        var data = grid.getChanges();
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: saveUrl,
            data: params,
            callback: function (data) {
                mini.alert("保存成功", '提示信息', function () {
                    search(50, 0);
                });
            }
        });
    }
    function exportExcel(){
        var data = form.getData(true);
        var fields = null;
        var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
        fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
        exportHrbExcel(fields);
    }

</script>
</body>
</html>
