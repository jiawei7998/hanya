<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../global.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
    <title>母行资产投放周报表(人民币)</title>
</head>
<body>
<fieldset class="mini-fieldset title">
    <div id="search_form" style="width:100%">
        <legend>报表查询</legend>

        <input id="tableType" name="tableType"  class="mini-combobox" data="CommonUtil.serverData.dictionary.tableType" labelField="true"
               label="报表类型:" labelSyle="text-align:right;" emptyText="请输入"  width="400px" onvaluechanged="getDate()" value="zc"></input>


        <input id="queryDate" name="queryDate" field="queryDate" class="mini-datepicker" labelField="true"
               value="<%=__bizDate%>" label="日期："
               required="true" labelStyle="text-align:center;" allowinput="false" labelStyle="width:100px" width="280px"
               onvaluechanged="getDate()"/>


        <span>
<%--                <a class="mini-button" style="display: none" id="search_btn" onclick="query()">查询</a>--%>
                <a class="mini-button" style="display: none" id="clear_btn" onclick="clear()">清空</a>
                <a id="create_btn" class="mini-button" style="display: none" onclick="getDate()">查询</a>
                <a id="export_btn" class="mini-button" style="display: none" onclick="exportExcel()">导出报表</a>
            </span>
    </div>
</fieldset>
<span style="float:right;">单位:亿元</span>
<div class="mini-fit" style="width:100%;height:100%;">
    <div id="loan_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
         sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
         border="true" allowResize="true">
        <div property="columns">
            <div field="PRODUCT" width="500" headerAlign="center" align="center">部门</div>
            <div header="实际数" headerAlign="center" align="center">
                <div property="columns">
                    <div field="AMT" headerAlign="center" align="center" width="120px">年初数</div>
                    <div field="TENOR_1M" headerAlign="center" align="center" width="120px">1月末</div>
                    <div field="TENOR_2M" headerAlign="center" align="center" width="120px">2月末</div>
                    <div field="TENOR_3M" headerAlign="center" align="center" width="120px">3月末</div>
                    <div field="TENOR_4M" headerAlign="center" align="center" width="120px">4月末</div>
                    <div field="TENOR_5M" headerAlign="center" align="center" width="120px">5月末</div>
                    <div field="TENOR_6M" headerAlign="center" align="center" width="120px">6月末</div>
                    <div field="TENOR_7M" headerAlign="center" align="center" width="120px">7月末</div>
                    <div field="TENOR_8M" headerAlign="center" align="center" width="120px">8月末</div>
                    <div field="TENOR_9M" headerAlign="center" align="center" width="120px">9月末</div>
                    <div field="TENOR_10M" headerAlign="center" align="center" width="120px">10月末</div>
                    <div field="TENOR_11M" headerAlign="center" align="center" width="120px">11月末</div>
                    <div field="TENOR_12M" headerAlign="center" align="center" width="120px">12月末</div>
                </div>
            </div>

            <div header="下周（本周四-下周三）预计情况" headerAlign="center" align="center">
                <div property="columns">
                    <div field="BZSYE" headerAlign="center" align="center" width="120px">本周三余额</div>
                    <div field="XZTFE" headerAlign="center" align="center" width="120px">下周投放额</div>
                    <div field="XZDQHSE" headerAlign="center" align="center" width="120px">下周到期回收额</div>
                    <div field="XZJZ" headerAlign="center" align="center" width="120px">下周净增</div>
                </div>
            </div>
            <div header="本周（上周四-本周三）实际情况与计划差异" headerAlign="center" align="center">
                <div property="columns">
                    <div field="BZSJTF" headerAlign="center" align="center" width="120px">本周实际投放</div>
                    <div field="BZSJDQHS" headerAlign="center" align="center" width="120px">本周实际到期回收</div>
                    <div field="BZSJJZ" headerAlign="center" align="center" width="120px">本周实际净增</div>
                    <div field="SZSYE" headerAlign="center" align="center" width="120px">上周三余额</div>
                    <div field="YBDBZTF" headerAlign="center" align="center" width="120px">预报的本周投放</div>
                    <div field="YBDBZDQHS" headerAlign="center" align="center" width="120px">预报的本周到期回收</div>
                    <div field="YBDBZJZ" headerAlign="center" align="center" width="120px">预报的本周净增</div>
                    <div field="TFCY" headerAlign="center" align="center" width="120px">投放差异</div>
                    <div field="HSCY" headerAlign="center" align="center" width="120px">收回差异</div>
                    <div field="JZCY" headerAlign="center" align="center" width="120px">净增差异</div>
                    <div field="CYYY" headerAlign="center" align="center" width="120px">差异原因</div>
                </div>
            </div>
        </div>
    </div>


    <div id="l_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
         sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
         border="true" allowResize="true">
        <div property="columns">
            <div field="PRODUCT" width="500" headerAlign="center" align="center">部门</div>

            <div header="实际数" headerAlign="center" align="center">
                <div property="columns">
                    <div field="AMT" headerAlign="center" align="center" width="120px">年初数</div>
                    <div field="TENOR_1M" headerAlign="center" align="center" width="120px">1月末</div>
                    <div field="TENOR_2M" headerAlign="center" align="center" width="120px">2月末</div>
                    <div field="TENOR_3M" headerAlign="center" align="center" width="120px">3月末</div>
                    <div field="TENOR_4M" headerAlign="center" align="center" width="120px">4月末</div>
                    <div field="TENOR_5M" headerAlign="center" align="center" width="120px">5月末</div>
                    <div field="TENOR_6M" headerAlign="center" align="center" width="120px">6月末</div>
                    <div field="TENOR_7M" headerAlign="center" align="center" width="120px">7月末</div>
                    <div field="TENOR_8M" headerAlign="center" align="center" width="120px">8月末</div>
                    <div field="TENOR_9M" headerAlign="center" align="center" width="120px">9月末</div>
                    <div field="TENOR_10M" headerAlign="center" align="center" width="120px">10月末</div>
                    <div field="TENOR_11M" headerAlign="center" align="center" width="120px">11月末</div>
                    <div field="TENOR_12M" headerAlign="center" align="center" width="120px">12月末</div>
                </div>
            </div>

            <div header="下周（本周四-下周三）预计情况" headerAlign="center" align="center">
                <div property="columns">
                    <div field="BZSYE" headerAlign="center" align="center" width="120px">本周三余额</div>
                    <div field="XZSYE" headerAlign="center" align="center" width="120px">下周三余额</div>
                    <div field="XZJZ" headerAlign="center" align="center" width="120px">下周净增</div>
                </div>
            </div>

        </div>
    </div>

</div>
<script>

    mini.parse();

    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var id = params.id;
    var implClass = params.implClass;


    var grid = mini.get("loan_grid");
    var grid2 = mini.get("l_grid");
    var form = new mini.Form("#search_form");

    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search1(pageSize, pageIndex);
    });

    grid2.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search2(pageSize, pageIndex);
    });



    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
            getDate();
        });
    });

    //查询按钮
    function query() {
        var tableType = mini.get("tableType").value;

        if(tableType=='zc'){
            search1(20,0)
        }
        if (tableType=='fz'){
            search2(20, 0);
        }
    }



    function search1(pageSize, pageIndex) {
        var searchUrl = "/CapitalTurnoverController/search";

        var data = form.getData(true);
        data['pageNum'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        data['FZ'] = '0';
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: searchUrl,
            data: params,
            callback: function (data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
                grid2.hide();
                grid.show();
            }
        });
    }

    function search2(pageSize, pageIndex) {
        var searchUrl = "/CapitalTurnoverController/search";

        var data = form.getData(true);
        data['pageNum'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        data['FZ'] = '1';
        var params = mini.encode(data);

        CommonUtil.ajax({
            url: searchUrl,
            data: params,
            callback: function (data) {
                grid2.setTotalCount(data.obj.total);
                grid2.setPageIndex(pageIndex);
                grid2.setPageSize(pageSize);
                grid2.setData(data.obj.rows);
                grid.hide();
                grid2.show();
            }
        });
    }



    //清空按钮
    function clear() {
        form.clear();
        query();
    }

    function getDate() {
        var data = form.getData(true);
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: "/CapitalTurnoverController/call",
            data: params,
            callback: function (data) {
                if (data.code == 'error.common.0000') {
                    mini.alert("数据生成成功", "系统提示", function () {
                        query();
                    });
                } else {
                    mini.alert("数据生成失败", "系统提示");
                }
            }
        });
    }


    //  //导出
  function exportExcel(){
            var dealtype = mini.get("tableType").getValue();
            var data = form.getData(true);
            var fields = null;
            var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
            fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
            fields += "<input type='hidden' id='sheetType' name='sheetType' value='" + dealtype + "'>";
            fields += "<input type='hidden' id='sheetTypeValue' name='sheetTypeValue' value='" + dealtype + "'>";
            exportHrbExcel(fields);
        }

</script>
</body>
</html>
