<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<title>哈尔滨银行本外币同业台账</title>
</head>
<body>
    <fieldset class="mini-fieldset title">
        <div id="search_form" style="margin-top: 2px;">
            <legend>报表查询</legend>
          
            <input id="dealtype" name="dealtype"  class="mini-combobox" data="CommonUtil.serverData.dictionary.dealtype" labelField="true" 
                label="交易类型:" labelSyle="text-align:right;" emptyText="请输入"></input>
      
            <span>
                <a class="mini-button" style="display: none"  id="search_btn" onclick="query()">查询</a>
                <a class="mini-button" style="display: none"  id="clear_btn" onclick="clear()">清空</a>
                <a id="export_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
            </span>
        </div>
    </fieldset>
    
 
   
    <div class="mini-fit" style="margin-top: 2px;">     
    <span style="float:right;">单位:元</span>
        <div id="sec_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"  allowAlternating="true"
                allowResize="true" border="true" sortMode="client" multiSelect="true">
     
            <div property="columns">
                <div field="id" headerAlign="center" align="center" width="30px">序号</div>
                <div field="br" headerAlign="center" align="center" width="50px">分行名称（含总行）</div>
                <div field="sn" headerAlign="center" align="center" width="50px">交易对手总部全称</div>
                <div field="ptype" headerAlign="center" align="center" width="30px">活期/定期</div>
                <div field="vDate" headerAlign="center" align="center" width="50px">业务开始日期</div>
                <div field="mdate" headerAlign="center" align="center" width="50px">业务结束日期</div>
                <div field="intrate" headerAlign="center" align="center" width="50px">利率（%）</div>
                <div field="ccyamt" headerAlign="center" align="center" width="50px">金额</div>
                <div field="balance" headerAlign="center" align="center" width="50px">余额</div>
                <div field="subject" headerAlign="center" align="center" width="50px">记账科目</div>
                <div field="remark" headerAlign="center" align="center" width="50px">备注</div>
           </div>
        </div>
        <div id="repo_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"  style="width:100%;height:100%;" 
                allowAlternating="true"
                allowResize="true" border="true" sortMode="client" multiSelect="true">
            <div property="columns">
                <div field="id" headerAlign="center" align="center" width="30px">序号</div>
                <div field="br" headerAlign="center" align="center" width="120px">分行名称（含总行）</div>
                <div field="br2" headerAlign="center" align="center" width="120px">支行名称（含总行部门）</div>
                <div field="branch" headerAlign="center" align="center" width="120px">归属部门</div>
                <div field="sn" headerAlign="center" align="center" width="120px">交易对手全称</div>
                <div field="cfn1" headerAlign="center" align="center" width="120px">交易对手落地机构</div>
                <div field="ca4" headerAlign="center" align="center" width="120px">交易对手地域</div>
                <div field="ptype" headerAlign="center" align="center" width="120px">活期/定期</div>
                <div field="vDate" headerAlign="center" align="center" width="120px">业务开始日期</div>
                <div field="mdate" headerAlign="center" align="center" width="120px">业务结束日期</div>
                <div field="intrate" headerAlign="center" align="center" width="120px">利率</div>
                <div field="ccy" headerAlign="center" align="center" width="120px">货币类型</div>
                <div field="ccyamt" headerAlign="center" align="center" width="120px">原币金额</div>
                <div field="spotrate_8" headerAlign="center" align="center" width="120px">汇率</div>
                <div field="rmbamt" headerAlign="center" align="center" width="120px">折人民币金额（元）</div>
                <div field="subject" headerAlign="center" align="center" width="120px">记账科目</div>
                <div field="nature" headerAlign="center" align="center" width="120px">账户性质</div>
                <div field="purpose" headerAlign="center" align="center" width="120px">账户用途</div>
                <div field="status" headerAlign="center" align="center" width="120px">账户状态</div>
                <div field="Internetbank" headerAlign="center" align="center" width="120px">是否开网银</div>
                <div field="contactNumber" headerAlign="center" align="center" width="120px">联系人/电话</div>
                <div field="remark" headerAlign="center" align="center" width="120px">备注</div>
                <div field=" exchangeRate" headerAlign="center" align="center" width="120px">月末汇率</div>
                <div field="ccyBalance" headerAlign="center" align="center" width="120px">原币余额</div>
                <div field="rmbBalance" headerAlign="center" align="center" width="120px">折人民币余额</div>
                
                </div>
        </div>
       
    <script>
        
        mini.parse();

        var secgrid = mini.get("sec_grid");
        var repogrid = mini.get("repo_grid");
        var form = new mini.Form("#search_form");
        
        repogrid.on("beforeload", function (e) {
            e.cancel = true;
            var pageIndex = e.data.pageIndex; 
            var pageSize = e.data.pageSize;
            searchA1(pageSize,pageIndex);
        });
        
        $(document).ready(function() {
            //控制按钮显示
            $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
                mini.get("dealtype").setValue('BTYCJ');
                searchA1(10, 0);
            });
        });
    
        //查询按钮
        function query(){
            var dealtype = mini.get("dealtype").value;
           
            if(dealtype == 'BTYCJ' ) {
                //本币-同业拆入
                searchA1(10,0);
            }
            if(dealtype == 'BTYCC' ) {
                //本币-同业拆出
                searchA2(10,0);
            }
            if(dealtype == 'WTYCF' ) {
                //外币-同业存放
                searchA3(10,0);
            }
            if(dealtype == 'WCFTY' ) {
                //外币-存放同业
                searchA4(10,0);
            }
            if(dealtype == 'WTYCR' ) {
                //外币-同业拆入
                searchA5(10,0);
            }
            if(dealtype == 'WCFTY2' ) { 
                //外币-拆放同业
                searchA6(10,0);
            } 
            
            if(dealtype == 'BTYCF' ) {
                //本币-同业存放
                searchA7(10,0);
            }
            if(dealtype == 'BCFTY' ) { 
                //本币-存放同业
                searchA8(10,0);
            } 
        }
        
      //本币-同业拆入
        function searchA1(pageSize, pageIndex) {
            var searchUrl="/TheForeignCurrencyReportCotroller/search1";
             
            var data = form.getData(true);
            data['pageNum'] = pageIndex + 1;
            data['pageSize'] = pageSize;
            
            var params = mini.encode(data);
            CommonUtil.ajax({
                url:searchUrl,
                data:params,
                callback:function(data){
                    secgrid.setTotalCount(data.obj.total);
                    secgrid.setPageIndex(pageIndex);
                    secgrid.setPageSize(pageSize);
                    secgrid.setData(data.obj.rows);
                    repogrid.hide();
                    secgrid.show();
                }
            });
        }
      //本币-同业拆出
        function searchA2(pageSize, pageIndex) {
            var searchUrl="/TheForeignCurrencyReportCotroller/search2";
             
            var data = form.getData(true);
            data['pageNum'] = pageIndex + 1;
            data['pageSize'] = pageSize;
            
            var params = mini.encode(data);
            CommonUtil.ajax({
                url:searchUrl,
                data:params,
                callback:function(data){
                    secgrid.setTotalCount(data.obj.total);
                    secgrid.setPageIndex(pageIndex);
                    secgrid.setPageSize(pageSize);
                    secgrid.setData(data.obj.rows);
                    repogrid.hide();
                    secgrid.show();
                }
            });
        }
        
        
        
        
        //外币-同业存放
        function searchA3(pageSize, pageIndex) {
            var searchUrl="/TheForeignCurrencyReportCotroller/search3";
             
            var data = form.getData(true);
            data['pageNum'] = pageIndex + 1;
            data['pageSize'] = pageSize;
    
            var params = mini.encode(data);
     
            CommonUtil.ajax({
                url:searchUrl,
                data:params,
                callback:function(data){
                repogrid.setTotalCount(data.obj.total);
                repogrid.setPageIndex(pageIndex);
                repogrid.setPageSize(pageSize);
                repogrid.setData(data.obj.rows);
                secgrid.hide();
                repogrid.show();
                }
            });
        }
        //外币-存放同业
        function searchA4(pageSize, pageIndex) {
            var searchUrl="/TheForeignCurrencyReportCotroller/search4";
             
            var data = form.getData(true);
            data['pageNum'] = pageIndex + 1;
            data['pageSize'] = pageSize;
    
            var params = mini.encode(data);
     
            CommonUtil.ajax({
                url:searchUrl,
                data:params,
                callback:function(data){
                repogrid.setTotalCount(data.obj.total);
                repogrid.setPageIndex(pageIndex);
                repogrid.setPageSize(pageSize);
                repogrid.setData(data.obj.rows);
                secgrid.hide();
                repogrid.show();
                }
            });
        }
        
      //外币-同业拆入
        function searchA5(pageSize, pageIndex) {
            var searchUrl="/TheForeignCurrencyReportCotroller/search5";
             
            var data = form.getData(true);
            data['pageNum'] = pageIndex + 1;
            data['pageSize'] = pageSize;
    
            var params = mini.encode(data);
     
            CommonUtil.ajax({
                url:searchUrl,
                data:params,
                callback:function(data){
                repogrid.setTotalCount(data.obj.total);
                repogrid.setPageIndex(pageIndex);
                repogrid.setPageSize(pageSize);
                repogrid.setData(data.obj.rows);
                secgrid.hide();
                repogrid.show();
                }
            });
        }
        
        //外币-拆放同业
        function searchA6(pageSize, pageIndex) {
            var searchUrl="/TheForeignCurrencyReportCotroller/search6";
             
            var data = form.getData(true);
            data['pageNum'] = pageIndex + 1;
            data['pageSize'] = pageSize;
    
            var params = mini.encode(data);
     
            CommonUtil.ajax({
                url:searchUrl,
                data:params,
                callback:function(data){
                repogrid.setTotalCount(data.obj.total);
                repogrid.setPageIndex(pageIndex);
                repogrid.setPageSize(pageSize);
                repogrid.setData(data.obj.rows);
                secgrid.hide();
                repogrid.show();
                }
            });
        }
        
        //本币-同业存放
        function searchA7(pageSize, pageIndex) {
            var searchUrl="/TheForeignCurrencyReportCotroller/search7";
             
            var data = form.getData(true);
            data['pageNum'] = pageIndex + 1;
            data['pageSize'] = pageSize;
            
            var params = mini.encode(data);
            CommonUtil.ajax({
                url:searchUrl,
                data:params,
                callback:function(data){
                    secgrid.setTotalCount(data.obj.total);
                    secgrid.setPageIndex(pageIndex);
                    secgrid.setPageSize(pageSize);
                    secgrid.setData(data.obj.rows);
                    repogrid.hide();
                    secgrid.show();
                }
            });
        }
        
      //本币-存放同业
        function searchA8(pageSize, pageIndex) {
            var searchUrl="/TheForeignCurrencyReportCotroller/search8";
             
            var data = form.getData(true);
            data['pageNum'] = pageIndex + 1;
            data['pageSize'] = pageSize;
            
            var params = mini.encode(data);
            CommonUtil.ajax({
                url:searchUrl,
                data:params,
                callback:function(data){
                    secgrid.setTotalCount(data.obj.total);
                    secgrid.setPageIndex(pageIndex);
                    secgrid.setPageSize(pageSize);
                    secgrid.setData(data.obj.rows);
                    repogrid.hide();
                    secgrid.show();
                }
            });
        }
        
        //清空按钮
        function clear() {
            form.clear();
            secgrid.hide();
            repogrid.hide();

        }
        
      //导出
        function exportExcel(){
          var dealtype = mini.get("dealtype").value;
            
            if(dealtype == 'BTYCJ'||dealtype == 'BTYCC'||dealtype == 'BTYCF'||dealtype == 'BCFTY' ) {
                var content = secgrid.getData();
            }
            if(dealtype == 'WTYCF'||dealtype == 'WCFTY' || dealtype == 'WTYCR' || dealtype == 'WCFTY2' ) {
                var content = repogrid.getData();
            } 
            if(content.length == 0){
                mini.alert("请先查询数据");
                return;
            }

        
        mini.confirm("您确认要导出Excel吗?","系统提示", 
                function (action) {
                  if (action == "ok"){
                    var data = form.getData(true);
                    var fields = null;
                  
                    var  fields = "<input type='hidden' id='dealNo' name='dealNo' value='"+data.dealNo+"'>";
                    fields += "<input type='hidden' id='dealtype' name='dealtype' value='"+data.dealtype+"'>";
//                    if(dealtype == 'BTYCJ'||dealtype == 'BTYCC'||dealtype == 'BTYCF'||dealtype == 'BCFTY' ) {
                     
                        fields += "<input type='hidden' id='excelName' name='excelName' value='bwbty.xls'>";
                        fields += "<input type='hidden' id='id' name='id' value='bwbty'>";
                        var urls = CommonUtil.pPath + "/sl/HrbReportManageController/exportExcel"; 
                   
//                    if(dealtype == 'WTYCF'||dealtype == 'WCFTY' || dealtype == 'WTYCR' || dealtype == 'WCFTY2' ) {
//                        fields += "<input type='hidden' id='excelName' name='excelName' value='wbty.xls'>";
//                        fields += "<input type='hidden' id='id' name='id' value='wbty'>";
//                        var urls = CommonUtil.pPath + "/sl/HrbReportManageController/exportExcel";   } 
                    
                        

                    $('<form action="'+ urls +'" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();   
        } 
                }
        );
        }
      
    </script>
</body>
</html>
