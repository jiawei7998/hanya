<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
<title>业务明细</title>
</head>
<body>
    <fieldset class="mini-fieldset title">
        <div id="search_form" style="width:100%">
            <legend>报表查询</legend>
            <input id="queryDate" name="queryDate" class="mini-datepicker" labelField="true" label="交易日期:"
                    labelSyle="text-align:right;" emptyText="请输入" value="<%=__bizDate%>"></input>
                <a class="mini-button" style="display: none"  id="search_btn" onclick="query()">查询</a>
<%--                <a class="mini-button" style="display: none"  id="clear_btn" onclick="clear()">清空</a>--%>
                <a id="export_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
            </span>
        </div>
    </fieldset>
    <span style="float:right;">单位:万元</span>
    <div class="mini-fit" style="width:100%;height:100%;">
        
        <div id="fund_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
             allowAlternating="true" border="true" allowResize="true" showPager="false" >
            <div property="columns" autoEscape="true">
                <div type="indexcolumn" headerAlign="center" align="left" width="40px">序号</div>
                <div field="product" headerAlign="center" width="220px" align="lfet">项目</div>
                <div header="A" headerAlign="center">
                    <div property="columns">
                        <div field="bookAmt" headerAlign="center" align="center" numberFormat="#,0.0000">金额（账面）</div>
                    </div>
                </div>
                <div header="B" headerAlign="center">
                    <div property="columns">
                        <div field="zslRate" headerAlign="center" align="center" numberFormat="#,0.00">折算率</div>
                    </div>
                </div>
                <div header="C" headerAlign="center">
                    <div property="columns">
                        <div field="zslAmt" headerAlign="center" align="center" numberFormat="#,0.0000">折算后金额</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        mini.parse();
        var url = window.location.search;
        var currTab = top["win"].tabs.getActiveTab();
        var params = currTab.params;
        var id = params.id;
        var implClass = params.implClass;

        var fundgrid = mini.get("fund_grid");
        var form = new mini.Form("#search_form");

        $(document).ready(function() {
            //控制按钮显示
            $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
                query();
            });
        });
        //查询按钮
        function query(){
            var searchUrl="/ExportDataController/getExportListData";
            var data = form.getData(true);
            data.implClass="ifsReportG2501ServiceImpl";
            var params = mini.encode(data);
            CommonUtil.ajax({
                url:searchUrl,
                data:params,
                callback:function(data){
                    // console.log(data)
                    fundgrid.setData(data.obj);
                    // var marges = [
                    //     { rowIndex: 0, columnIndex: 0, rowSpan: 1, colSpan: 13 }
                    //     // { rowIndex: 46, columnIndex: 0, rowSpan: 1, colSpan: 13 },
                    //     // { rowIndex: 51, columnIndex: 0, rowSpan: 1, colSpan: 13 },
                    // ];
                    // fundgrid.margeCells(marges);
                }
            });
        }

        function getColumns(columns) {
            var cols = [];
            for (var i = 0; i < columns.length; i++) {
                var column = columns[i];

                var col = { header: column.header, field: column.field, type: column.type };
                if (column.columns) {
                    col.columns = getColumns(column.columns);
                }
                cols.push(col);

            }
            return cols;
        }

        //导出
        function exportExcel(){
            // var columns = getColumns(fundgrid.columns);
            var data = form.getData(true);
            var fields = null;
            var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
            fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
            exportHrbExcel(fields);
        }
    
    </script>
</body>
</html>
