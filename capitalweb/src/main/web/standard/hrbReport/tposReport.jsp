<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>合并持仓分析报表</title>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>
<body style="width: 100%; height: 100%; background: white">
<fieldset class="mini-fieldset">
	<legend>查询条件</legend>
	<div id="search_form" style="width: 70%;">
		<nobr>
		<input id="sdate" name="sdate" class="mini-datepicker" labelField="true" label="日期：" required="true"
					ondrawdate="onDrawDateStart" labelStyle="text-align:right;" emptyText="起始日期" format="yyyy-MM-dd" value="<%=__bizDate%>"/>
		<span>至</span>
		<input id="edate" name="edate" class="mini-datepicker" required="true"
					ondrawdate="onDrawDateEnd" emptyText="结束日期" format="yyyy-MM-dd" value="<%=__bizDate%>"/>
		</nobr>
		<input id="br" name="br" class="mini-combobox"  data="CommonUtil.serverData.dictionary.BrType" value="01" emptyText="请选择分行"  label="分行：" width="280px" labelField="true" labelStyle="text-align:right;"/>
		<input id="type" name="type" class="mini-combobox"  data="CommonUtil.serverData.dictionary.AccType" value="ALL" emptyText="请选择账户类型"  label="账户类型：" width="280px" labelField="true" labelStyle="text-align:right;"/>
		<span style="float: right; margin-right: 50px">
			<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
			<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
			<a id="export_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
		</span>
	</div>
</fieldset>

	
<div class="mini-fit" style="margin-top: 2px;margin-bottom: 2px;">
	<div id="datagrid" class="mini-datagrid borderAll" style="width: 100%; height: 100%;" allowAlternating="true" multiSelect="true"
		allowResize="true" border="true" sortMode="client" multiSelect="true">
		<div property="columns">
			<div type="indexcolumn" headerAlign="center" width="40">序号</div>
			<div field="invType" name="invType" width="100" align="center" headerAlign="center" allowSort="false">会计账户类型</div>
			<div field="sd" name="sd" width="300" align="center" headerAlign="center" allowSort="false">债券类型</div>
			<div field="issuer" name="issuer" width="250" align="center" headerAlign="center" allowSort="false">债券发行人</div>
			<div field="cost" name="cost" width="300" align="center" headerAlign="center" allowSort="false">成本中心</div>
			<div field="secId" name="secId" width="80" align="center" headerAlign="center" allowSort="false">债券代码</div>
			<div field="port" name="port" width="80" align="center" headerAlign="center" allowSort="false">投资组合</div>
			<div field="jiuQi" name="jiuQi" width="80" align="right" headerAlign="center" allowSort="false">久期</div>
			<div field="pvbpx" name="pvbpx" width="80" align="right" headerAlign="center" allowSort="false">PVBP值</div>
			<div field="pv01x" name="pv01x" width="100" align="right" headerAlign="center" allowSort="false">PV01值</div>
			<div field="varx" name="varx" width="80" align="right" headerAlign="center" allowSort="false">VAR值</div>
			<div field="fxzl" name="fxzl" width="120" align="right" headerAlign="center" allowSort="false">债券发行总量(万)</div>
			<div field="sshy" name="sshy" width="120" align="center" headerAlign="center" allowSort="false">债券所属行业</div>
			<div field="ztLevel" name="ztLevel" width="100" align="center" headerAlign="center" allowSort="false">最新主体评级</div>
			<div field="zxLevel" name="zxLevel" width="100" align="center" headerAlign="center" allowSort="false">最新债项评级</div>
			<div field="fxZtLevel" name="fxZtLevel" width="100" align="center" headerAlign="center" allowSort="false">发行主体评级</div>
			<div field="fxZxLevel" name="fxZxLevel" width="100" align="center" headerAlign="center" allowSort="false">发行债项评级</div>
			<div field="sName" name="sName" width="150" align="center" headerAlign="center" allowSort="false">债券名称</div>
			<div field="vDate" name="vDate" width="100" align="center" headerAlign="center" allowSort="false">起息日</div>
			<div field="tenor" name="tenor" width="80" align="center" headerAlign="center" allowSort="false">期限</div>
			<div field="mDate" name="mDate" width="100" align="center" headerAlign="center" allowSort="false">到期日</div>
			<div field="prinAmt" name="prinAmt" width="120" align="right" headerAlign="center" allowSort="false">债券面值</div>
			<div field="coupRate" name="coupRate" width="80" align="right" headerAlign="center" allowSort="false">票面利率</div>
			<div field="acctngType" name="acctngType" width="120" align="center" headerAlign="center" allowSort="false">持仓类别</div>
			<div field="cccb" name="cccb" width="120" align="right" headerAlign="center" allowSort="false">持仓成本(元)</div>
			<div field="yslx" name="yslx" width="120" align="right" headerAlign="center" allowSort="false">应收利息(元)</div>

			<div field="zmjz" name="zmjz" width="120" align="right" headerAlign="center" allowSort="false">账面价值(元)</div>
			<div field="yjlx" name="yjlx" width="100" align="right" headerAlign="center" allowSort="false">应计利息(元)</div>
			<div field="dnljlxsr" name="dnljlxsr" width="120" align="right" headerAlign="center" allowSort="false">计算期利息收入(元)</div>
			<div field="syljlxsr" name="syljlxsr" width="120" align="right" headerAlign="center" allowSort="false">持有期利息收入(元)</div>
			<div field="tdymtm" name="tdymtm" width="120" align="right" headerAlign="center" allowSort="false">公允价值变动(元)</div>
			<div field="tjpmtm" name="tjpmtm" width="150" align="right" headerAlign="center" allowSort="false">公允价值变动损益(元)</div>
			<div field="avgCost" name="avgCost" width="120" align="right" headerAlign="center" allowSort="false">债券买入成本</div>
			<div field="settAvgCost" name="settAvgCost" width="120" align="right" headerAlign="center" allowSort="false">债券投资成本</div>
			<div field="gz" name="gz" width="100" align="right" headerAlign="center" allowSort="false">估值</div>
			<div field="opicsJjsz" name="opicsJjsz" width="120" align="right" headerAlign="center" allowSort="false">账面净价市值(元)</div>
			<div field="jjsz" name="jjsz" width="120" align="right" headerAlign="center" allowSort="false">净价市值(元)</div>
			<div field="yzj" name="yzj" width="120" align="right" headerAlign="center" allowSort="false">OPICS_溢折价(元）</div>
			<div field="windYzj" name="windYzj" width="120" align="right" headerAlign="center" allowSort="false">溢折价(元）</div>
			<div field="lxtz" name="lxtz" width="100" align="right" headerAlign="center" allowSort="false">利息调整(元)</div>
			<div field="syl" name="syl" width="100" align="right" headerAlign="center" allowSort="false">到期收益率</div>
			<div field="jqcbPer" name="jqcbPer" width="100" align="right" headerAlign="center" allowSort="false">百元加权成本</div>
			<div field="jjcbPer" name="jjcbPer" width="100" align="right" headerAlign="center" allowSort="false">百元净价成本</div>
			<div field="yslxPer" name="yslxPer" width="100" align="right" headerAlign="center" allowSort="false">百元应收利息</div>

			<div field="yjlxPer" name="yjlxPer" width="100" align="right" headerAlign="center" allowSort="false">百元应计利息</div>
			<div field="tyjj" name="tyjj" width="120" align="right" headerAlign="center" allowSort="false">摊余净价(元)</div>
			<div field="jjfy" name="jjfy" width="120" align="right" headerAlign="center" allowSort="false">OPICS_净价浮赢(元)</div>
			<div field="windJjfy" name="yslxPer" width="120" align="right" headerAlign="center" allowSort="false">净价浮赢(元)</div>
			<div field="synx" name="synx" width="80" align="right" headerAlign="center" allowSort="false">剩余年限</div>
			<div field="fxSyts" name="fxSyts" width="100" align="center" headerAlign="center" allowSort="false">付息剩余天数</div>
			<div field="cdjr" name="cdjr" width="120" align="center" headerAlign="center" allowSort="false">最近利率重定价日</div>
			<div field="nx" name="nx" width="80" align="center" headerAlign="center" allowSort="false">年限</div>
			<div field="fxrq" name="fxrq" width="120" align="center" headerAlign="center" allowSort="false">发行日期</div>
			<div field="fxl" name="fxl" width="80" align="center" headerAlign="center" allowSort="false">付息类</div>
			<div field="fxpl" name="fxpl" width="80" align="center" headerAlign="center" allowSort="false">付息频率</div>

			<div field="fdlx" name="fdlx" width="80" align="center" headerAlign="center" allowSort="false">浮动类型</div>
			<div field="jzll" name="jzll" width="80" align="right" headerAlign="center" allowSort="false">基准利率</div>
			<div field="fdjd" name="fdjd" width="80" align="right" headerAlign="center" allowSort="false">浮动基点</div>
			<div field="lc" name="lc" width="80" align="right" headerAlign="center" allowSort="false">利差</div>

			<div field="hqz" name="hqz" width="80" align="center" headerAlign="center" allowSort="false">含权类</div>
			<div field="ifName" name="ifName" width="250" align="center" headerAlign="center" allowSort="false">发行人</div>
			<div field="ptz" name="ptz" width="100" align="center" headerAlign="center" allowSort="false">是否平台债</div>
			<div field="db" name="db" width="80" align="center" headerAlign="center" allowSort="false">是否担保</div>
			<div field="dbfs" name="dbfs" width="100" align="center" headerAlign="center" allowSort="false">担保方式</div>
			<div field="dbName" name="dbName" width="100" align="center" headerAlign="center" allowSort="false">担保人</div>
			<div field="rbaMount" name="rbaMount" width="120" align="right" headerAlign="center" allowSort="false">买断抵押面额(元)</div>
			<div field="rpaMount" name="rpaMount" width="120" align="right" headerAlign="center" allowSort="false">质押面额(元)</div>
			<div field="rdaMount" name="rdaMount" width="120" align="right" headerAlign="center" allowSort="false">国库定期存款质押面额</div>
			<div field="me" name="me" width="120" align="right" headerAlign="center" allowSort="false">可用面额(元)</div>
			<div field="bz" name="bz" width="100" align="center" headerAlign="center" allowSort="false">备注</div>
		</div>
	</div>
</div>

<script>
	mini.parse();

	var url = window.location.search;
	var grid = mini.get("datagrid");
	var userId='<%=__sessionUser.getUserId()%>';
	var form = new mini.Form("#search_form");
	
	grid.on("beforeload", function(e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex;
		var pageSize = e.data.pageSize;
		search(pageSize, pageIndex);
	});
	
	
	// 查询
	function search(pageSize,pageIndex){
		var form = new mini.Form("#search_form");
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统也提示");
			return;
		}
		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:"/TposReportController/searchTposReportPage",
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
		
	function query() {
    	search(grid.pageSize, 0);
    }
	
	function clear(){
        var form=new mini.Form("search_form");
        form.clear();
        // search(10,0);
	}
	
	$(document).ready(function() {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
			search(10, 0);
		});
	});
	
	//交易日期
	function onDrawDateStart(e) {
        var startDate = e.date;
        var endDate= mini.get("edate").getValue();
        if(CommonUtil.isNull(endDate)){
        	return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }
	function onDrawDateEnd(e) {
        var endDate = e.date;
        var startDate = mini.get("sdate").getValue();
        if(CommonUtil.isNull(startDate)){
        	return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }
	
	//导出
	function exportExcel(){
		var content = grid.getData();
		if(content.length == 0){
			mini.alert("请先查询数据");
			return;
		}
		mini.confirm("您确认要导出Excel吗?","系统提示", 
	        function (action) {
	          if (action == "ok"){
	            var data = form.getData(true);
	            var fields = null;
	            
	            var  fields = "<input type='hidden' id='sdate' name='sdate' value='"+data.sdate+"'>";
		            fields += "<input type='hidden' id='edate' name='edate' value='"+data.edate+"'>";
		            fields += "<input type='hidden' id='type' name='type' value='"+data.type+"'>";
		            fields += "<input type='hidden' id='br' name='br' value='"+data.br+"'>";
	              
	              fields += "<input type='hidden' id='excelName' name='excelName' value='tpos.xls'>";
	              fields += "<input type='hidden' id='id' name='id' value='tpos'>";
	            var urls = CommonUtil.pPath + "/sl/HrbReportManageController/exportExcel";                                                                                                                         
	            $('<form action="'+ urls +'" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();   
	          }
	        }
		);
	}
</script>
</body>
</html>