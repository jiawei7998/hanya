<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
    <title>税费报表查询</title>
</head>
<body>
    <fieldset class="mini-fieldset title">
        <div id="search_form" style="width:100%">
            <legend>报表查询</legend>
            <input id="year" name="year" class="mini-monthpicker" labelField="true" label="年份:"
                    labelSyle="text-align:right;" emptyText="请输入" format="yyyy" value="<%=__bizDate%>"/>
            <input id="quarterly" name="quarterly" class="mini-combobox" width="320px"
                   data="[
                   {'v':'03-31','t':'第一季度'},
                   {'v':'06-30','t':'第二季度'},
                   {'v':'09-30','t':'第三季度'},
                   {'v':'12-31','t':'第四季度'}]" valueField="v" textField="t" emptyText="请选择季度" labelField="true"
                   label="季度："
                   labelStyle="text-align:right;" onvaluechanged="showData" value="03-31"/>
                <a class="mini-button" style="display: none"  id="search_btn" onclick="query()">查询</a>
<%--                <a class="mini-button" style="display: none"  id="clear_btn" onclick="clear()">清空</a>--%>
                <a id="export_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
            </span>
        </div>
    </fieldset>
<%--    <span style="float:right;">单位:万元</span>--%>
    <div class="mini-fit" style="width:100%;height:100%;">
        
        <div id="fund_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
             allowAlternating="true" border="true" allowResize="true" showPager="false" >
            <div property="columns" autoEscape="true">
                <div field="tradePlatform" headerAlign="center" align="center">交易平台</div>
                <div field="class" headerAlign="center" align="center">类别</div>
                <div field="term" headerAlign="center" align="center" >期限</div>
                <div field="tradeNum" headerAlign="center" align="center" >交易笔数</div>
                <div field="tradeAmt" headerAlign="center" align="center" numberFormat="#,0.00">交易量</div>
                <div field="beforeTaxes" headerAlign="center" width="220px" align="center"numberFormat="#,0.00">前台税费(元)</div>
                <div field="cqghbs" headerAlign="center" align="center" >纯券过户笔数</div>
                <div field="afterTaxesCqgh" headerAlign="center" align="center" >后台税费-纯券过户</div>
                <div field="fcqghbs" headerAlign="center" align="center" >非纯券过户笔数</div>
                <div field="afterTaxesFcqgh" headerAlign="center" align="center" numberFormat="#,0.00">后台税费-非纯券过户</div>
                <div field="taxesAmt" headerAlign="center" align="center" numberFormat="#,0.00">税费金额</div>
            </div>
        </div>
    </div>
    <script>
        mini.parse();
        var url = window.location.search;
        var currTab = top["win"].tabs.getActiveTab();
        var params = currTab.params;
        var id = params.id;
        var implClass = params.implClass;
        var fundgrid = mini.get("fund_grid");
        var form = new mini.Form("#search_form");
        var queryDate="";

        $(document).ready(function() {
            //控制按钮显示
            $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
                query();
            });
        });
        //查询按钮
        function query(){
            var searchUrl="/ExportDataController/getExportListData";
            var data = form.getData(true);
            data.queryDate=getQueryDate();
            data.implClass=implClass;
            var params = mini.encode(data);
            CommonUtil.ajax({
                url:searchUrl,
                data:params,
                callback:function(data){
                    fundgrid.setData(data.obj);
                }
            });
        }

        function exportExcel(){
            var data = form.getData(true);
            var fields = null;
            var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
            fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
            exportHrbExcel(fields);
        }

        /**
         * 算出查询时间
         */
        function getQueryDate() {
            var year=mini.get("year").getText();
            var quarterly=mini.get("quarterly").getValue();
            return year+'-'+quarterly;
        }

        //下拉选择需要展示的表格
        function showData() {
            query(10, 0);
        }
    
    </script>
</body>
</html>
