<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>回购交易台账-计财</title>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
	<script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
</head>
<body style="width: 100%; height: 100%; background: white">
<fieldset class="mini-fieldset">
	<legend>查询条件</legend>
	<div id="search_form" style="width: 70%;">
		<nobr>
		<input id="sdate" name="sdate" class="mini-datepicker" labelField="true" label="日期：" required="true"
					ondrawdate="onDrawDateStart" labelStyle="text-align:right;" emptyText="起始日期" format="yyyy-MM-dd" value="<%=__bizDate%>"/>
		<span>至</span>
		<input id="edate" name="edate" class="mini-datepicker"  required="true"
					ondrawdate="onDrawDateEnd" emptyText="结束日期" format="yyyy-MM-dd" value="<%=__bizDate%>"/>
		</nobr>
		<input id="type" name="type" class="mini-combobox"  data="CommonUtil.serverData.dictionary.RepoType" value="ZH" emptyText="请选择回购类型"  label="回购类型：" width="280px" labelField="true" labelStyle="text-align:right;"/>
		<input id="ps" name="ps" class="mini-combobox"  data="CommonUtil.serverData.dictionary.ProdType" value="01" emptyText="请选择产品类型"  label="产品类型：" width="280px" labelField="true" labelStyle="text-align:right;"/>
		<input id="br" name="br" class="mini-combobox"  data="CommonUtil.serverData.dictionary.BrType" value="01" emptyText="请选择分行"  label="分行：" width="280px" labelField="true" labelStyle="text-align:right;"/>
		
	</div>
	<span style="float: right; margin-right: 50px"> 
			<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
			<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
			<a id="export_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
	</span>
</fieldset>

	
<div class="mini-fit" style="margin-top: 2px;margin-bottom: 2px;">
	<div id="datagrid" class="mini-datagrid borderAll" style="width: 100%; height: 100%;" allowAlternating="true" multiSelect="true"
		allowResize="true" border="true" sortMode="client" multiSelect="true">
		<div property="columns">
			<div type="indexcolumn" headerAlign="center" width="40">序号</div>
			<div field="delNo" name="delNo" width="100" align="center" headerAlign="center" allowSort="false">成交单号</div>
			<div field="costCenter" name="costCenter" width="200" align="center" headerAlign="center" allowSort="false">成本中心</div>
			<div field="cname" name="cname" width="200" align="center" headerAlign="center" allowSort="false">对手方</div>
			<div field="acctDesc" name="acctDesc" width="150" align="center" headerAlign="center" allowSort="false">对手属性</div>
			<div field="acctType" name="acctType" width="100" align="center" headerAlign="center" allowSort="false">账户类型</div>
			<div field="descr" name="descr" width="100" align="center" headerAlign="center" allowSort="false">回购方式</div>
			<div field="vDate" name="vDate" width="100" align="center" headerAlign="center" allowSort="false">首期交割日</div>
			<div field="term" name="term" width="50" align="center" headerAlign="center" allowSort="false">期限</div>
			<div field="matDate" name="matDate" width="100" align="center" headerAlign="center" allowSort="false">到期交割日</div>
			
			<div field="repoRate" name="repoRate" width="80" align="right" headerAlign="center" allowSort="false">回购利率</div>		
			<div field="amt" name="amt" width="120" align="right" headerAlign="center" allowSort="false">券面（拆借）总额</div>
			<div field="comProcdAmt" name="comProcdAmt" width="120" align="right" headerAlign="center" allowSort="false">首期结算额</div>
			<div field="matProcdAmt" name="matProcdAmt" width="120" align="right" headerAlign="center" allowSort="false">到期结算额</div>
			<div field="totaLint" name="totaLint" width="120" align="right" headerAlign="center" allowSort="false">利息支额</div>
			<div field="occupyAmt" name="occupyAmt" width="120" align="right" headerAlign="center" allowSort="false">资金占用</div>
			<div field="handAmt" name="handAmt" width="120" align="right" headerAlign="center" allowSort="false">交易手续费</div>
			<div field="overTerm" name="overTerm" width="80" align="center" headerAlign="center" allowSort="false">剩余期限</div>
			<div field="rpLint" name="rpLint" width="120" align="right" headerAlign="center" allowSort="false">应收（付）利息额</div>
			<div field="overLint" name="overLint" width="120" align="right" headerAlign="center" allowSort="false">剩余利息额</div>
			<div field="proLoss" name="proLoss" width="120" align="right" headerAlign="center" allowSort="false">已实现损益</div>
			<div field="secId" name="secId" width="200" align="center" headerAlign="center" allowSort="false">回购用券</div>
			<div field="faceAmt" name="faceAmt" width="200" align="right" headerAlign="center" allowSort="false">压券面值</div>
			<div field="deprice" name="deprice" width="200" align="right" headerAlign="center" allowSort="false">折算比例</div>
			<div field="zlLevel" name="zlLevel" width="200" align="center" headerAlign="center" allowSort="false">债券评级</div>
		</div>
	</div>
</div>

<script>
	mini.parse();

	var url = window.location.search;
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var id = params.id;
	var implClass = params.implClass;
	var grid = mini.get("datagrid");
	var userId='<%=__sessionUser.getUserId()%>';
	var form = new mini.Form("#search_form");
	
	grid.on("beforeload", function(e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex;
		var pageSize = e.data.pageSize;
		search(pageSize, pageIndex);
	});
	
	
	// 查询
	function search(pageSize,pageIndex){
		var form = new mini.Form("#search_form");
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统也提示");
			return;
		}
		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		data['implClass']=implClass;
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:"/ExportDataController/getExportPageData",
			data:params,
			callback : function(data) {
				//grid.setTotalCount(data.obj.total);
				//grid.setData(data.obj);
				
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
		
	function query() {
    	search(grid.pageSize, 0);
    }
	
	function clear(){
        var form=new mini.Form("search_form");
        form.clear();
        // search(10,0);
	}
	
	$(document).ready(function() {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
			search(10, 0);
		});
	});
	
	//交易日期
	function onDrawDateStart(e) {
        var startDate = e.date;
        var endDate= mini.get("edate").getValue();
        if(CommonUtil.isNull(endDate)){
        	return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }
	function onDrawDateEnd(e) {
        var endDate = e.date;
        var startDate = mini.get("sdate").getValue();
        if(CommonUtil.isNull(startDate)){
        	return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }
	
	//导出
	function exportExcel(){
		var data = form.getData(true);
		var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
		fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
		fields += "<input type='hidden' id='sdate' name='sdate' value='"+data.sdate+"'>";
		fields += "<input type='hidden' id='edate' name='edate' value='"+data.edate+"'>";
		fields += "<input type='hidden' id='type' name='type' value='"+data.type+"'>";
		fields += "<input type='hidden' id='br' name='br' value='"+data.br+"'>";
		exportHrbExcel(fields);

	}
</script>
</body>
</html>