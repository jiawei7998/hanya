<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<title>计提本金变动情况表</title>
</head>
<body>
    <fieldset class="mini-fieldset title">
        <div id="search_form" style="width:100%">
            <legend>报表查询</legend>
            <input id="queryDate" name="queryDate" class="mini-monthpicker" labelField="true" label= "查询日期:" 
                    value="<%=__bizDate%>" labelSyle="text-align:right;" emptyText="请输入"></input>
            <input id="CCY" name="CCY" class="mini-combobox" labelField="true" label="币种:" labelSyle="text-align:right;" data="CommonUtil.serverData.dictionary.Currency" emptyText="请输入"></input>
            <span>
                <a class="mini-button" style="display: none"  id="search_btn" onclick="query()">查询</a>
                <a class="mini-button" style="display: none"  id="clear_btn" onclick="clear()">清空</a>
                <a id="export_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
            </span>
        </div>
    </fieldset>
    <span style="float:right;">单位:元</span>
    <div class="mini-fit" style="width:100%;height:100%;">
        <div id="loan_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
            sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
            border="true" allowResize="true">
            <div property="columns">
                <div type="indexcolumn" headerAlign="center" align="center" width="30">序列</div>
                <div field="cmne" headerAlign="center" align="center" width="100"></div>
                <div field="date1" headerAlign="center" align="center" width="100" numberFormat="#,0.00">日期1</div>
                <div field="date2" headerAlign="center" align="center" width="100" numberFormat="#,0.00">日期2</div>
                <div field="date3" headerAlign="center" align="center" width="100" numberFormat="#,0.00">日期3</div>
                <div field="date4" headerAlign="center" align="center" width="100" numberFormat="#,0.00">日期4</div>
                <div field="date5" headerAlign="center" align="center" width="100" numberFormat="#,0.00">日期5</div>
                <div field="date6" headerAlign="center" align="center" width="100" numberFormat="#,0.00">日期6</div>
                <div field="date7" headerAlign="center" align="center" width="100" numberFormat="#,0.00">日期7</div>
                <div field="date8" headerAlign="center" align="center" width="100" numberFormat="#,0.00">日期8</div>
                <div field="date9" headerAlign="center" align="center" width="100" numberFormat="#,0.00">日期9</div>
                <div field="date10" headerAlign="center" align="center" width="100" numberFormat="#,0.00">日期10</div>
                <div field="date11" headerAlign="center" align="center" width="100" numberFormat="#,0.00">日期11</div>
                <div field="date12" headerAlign="center" align="center" width="100" numberFormat="#,0.00">日期12</div>
                <div field="date13" headerAlign="center" align="center" width="100" numberFormat="#,0.00">日期13</div>
                <div field="date14" headerAlign="center" align="center" width="100" numberFormat="#,0.00">日期14</div>
                <div field="date15" headerAlign="center" align="center" width="100" numberFormat="#,0.00">日期15</div>
                <div field="date16" headerAlign="center" align="center" width="100" numberFormat="#,0.00">日期16</div>
                <div field="date17" headerAlign="center" align="center" width="100" numberFormat="#,0.00">日期17</div>
                <div field="date18" headerAlign="center" align="center" width="100" numberFormat="#,0.00">日期18</div>
                <div field="date19" headerAlign="center" align="center" width="100" numberFormat="#,0.00">日期19</div>
                <div field="date20" headerAlign="center" align="center" width="100" numberFormat="#,0.00">日期20</div>
                <div field="date21" headerAlign="center" align="center" width="100" numberFormat="#,0.00">日期21</div>
                <div field="date22" headerAlign="center" align="center" width="100" numberFormat="#,0.00">日期22</div>
                <div field="date23" headerAlign="center" align="center" width="100" numberFormat="#,0.00">日期23</div>
                <div field="date24" headerAlign="center" align="center" width="100" numberFormat="#,0.00">日期24</div>
                <div field="date25" headerAlign="center" align="center" width="100" numberFormat="#,0.00">日期25</div>
                <div field="date26" headerAlign="center" align="center" width="100" numberFormat="#,0.00">日期26</div>
                <div field="date27" headerAlign="center" align="center" width="100" numberFormat="#,0.00">日期27</div>
                <div field="date28" headerAlign="center" align="center" width="100" numberFormat="#,0.00">日期28</div>
                <div field="date29" headerAlign="center" align="center" width="100" numberFormat="#,0.00">日期29</div>
                <div field="date30" headerAlign="center" align="center" width="100" numberFormat="#,0.00">日期30</div>
                <div field="date31" headerAlign="center" align="center" width="100" numberFormat="#,0.00">日期31</div>
                <div field="sumamt" headerAlign="center" align="center" width="100" numberFormat="#,0.00">合计</div>
                <div field="balamt" headerAlign="center" align="center" width="100" numberFormat="#,0.00">平均值</div>
            </div>
        </div>
    </div>
    <script>
        
        mini.parse();

        var grid = mini.get("loan_grid");
        var form = new mini.Form("#search_form");
        
        grid.on("beforeload", function (e) {
            e.cancel = true;
            var pageIndex = e.data.pageIndex; 
            var pageSize = e.data.pageSize;
            search(pageSize,pageIndex);
        });
        
        $(document).ready(function() {
            //控制按钮显示
            $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
                mini.get("CCY").setValue('CNY');
                query();
            });
        });
    
        //查询按钮
        function query(){
            search(10,0);
        }
        
        function search(pageSize, pageIndex) {
            var searchUrl="/AccrualPrincipalChangeController/search";
             
            var data = form.getData(true);
            data['pageNum'] = pageIndex + 1;
            data['pageSize'] = pageSize;
    
            var params = mini.encode(data);
     
            CommonUtil.ajax({
                url:searchUrl,
                data:params,
                callback:function(data){
                    grid.setTotalCount(data.obj.total);
                    grid.setPageIndex(pageIndex);
                    grid.setPageSize(pageSize);
                    grid.setData(data.obj.rows);
                }
            });
        }
        
        //清空按钮
        function clear() {
            form.clear();
            query();
        }
        
        function exportExcel(){
            var content = grid.getData();
            if(content.length == 0){
                mini.alert("请先查询数据");
                return;
            }
            
            mini.confirm("您确认要导出Excel吗?","系统提示", 
                function (action) {
                    if (action == "ok"){
                        var data = form.getData(true);
                        var fields = null;
                        
                        var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
                            fields += "<input type='hidden' id='CCY' name='CCY' value='"+data.CCY+"'>";
                          fields += "<input type='hidden' id='excelName' name='excelName' value='bjjt.xls'>";
                          fields += "<input type='hidden' id='id' name='id' value='bjjt'>";
                        var urls = CommonUtil.pPath + "/sl/HrbReportManageController/exportExcel";
                        $('<form action="'+ urls +'" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();
                    }
                }
            );
        }
    
    </script>
</body>
</html>
