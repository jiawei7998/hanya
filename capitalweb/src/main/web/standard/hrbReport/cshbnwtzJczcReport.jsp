<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../global.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>城商行表内外投资业务和同业交易对手情况表 表内外投资业务基础资产情况表</title>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
</head>
<body style="width: 100%; height: 100%; background: white">
<fieldset class="mini-fieldset">
    <div id="search_form" style="width: 50%;">
        <legend>查询条件</legend>
        <input id="queryDate" name="queryDate" class="mini-datepicker mini-mustFill" required="true" labelField="true"
               label="日期：" required="true" labelStyle="text-align:right;" emptyText="起始日期" format="yyyy-MM-dd"
               value="<%=__bizDate%>"/>
        <span style="float:right;">
			<a id="search_btn" class="mini-button" style="display: none" onclick="create()">查询</a>
			<a id="export_btn" class="mini-button" style="display: none" onclick="exportExcel()">导出报表</a>
		</span>
    </div>
    <span style="float:right;">单位:万元</span>
</fieldset>
<div class="mini-fit" style="margin-top: 2px;margin-bottom: 2px;">
    <div id="datagrid" class="mini-datagrid borderAll" style="width: 100%; height: 100%;" allowAlternating="true"
         multiSelect="true" allowResize="true" border="true" sortMode="client" multiSelect="true">
        <div property="columns">
            <div type="indexcolumn" headerAlign="center" width="40">序号</div>
            <div header="底层项目" headerAlign="center">
                <div property="columns">
                    <div field="product" name="product" width="300" align="left" headerAlign="center" allowSort="false">
                        可以实时统计基础资产的产品
                    </div>
                </div>
            </div>
            <div header="表外理财投资产品形式及余额" headerAlign="center">
                <div property="columns">
                    <div width="120" headerAlign="center">债券
                        <div property="columns">
                            <div field="bwZqBjmAmt" name="bwZqBjmAmt" width="120" align="center" headerAlign="center"
                                 allowSort="false" numberFormat="n4">本季末数额
                            </div>
                            <div field="bwZqSjmAmt" name="bwZqSjmAmt" width="120" align="center" headerAlign="center"
                                 allowSort="false" numberFormat="n4">上季末数额
                            </div>
                        </div>
                    </div>
                    <div width="120" headerAlign="center">非标准化债权类投资
                        <div property="columns">
                            <div field="bwFbzzqBjmAmt" name="bwFbzzqBjmAmt" width="140" align="center"
                                 headerAlign="center" allowSort="false" numberFormat="n4">本季末数额
                            </div>
                            <div field="bwFbzzqSjmAmt" name="bwFbzzqSjmAmt" width="140" align="center"
                                 headerAlign="center" allowSort="false" numberFormat="n4">上季末数额
                            </div>
                        </div>
                    </div>
                    <div width="120" headerAlign="center">其他（请列明）
                        <div property="columns">
                            <div field="bwQtBjmAmt" name="bwQtBjmAmt" width="120" align="center" headerAlign="center"
                                 allowSort="false" numberFormat="n4">本季末数额
                            </div>
                            <div field="bwQtSjmAmt" name="bwQtSjmAmt" width="120" align="center" headerAlign="center"
                                 allowSort="false" numberFormat="n4">上季末数额
                            </div>
                        </div>
                    </div>
                    <div width="120" headerAlign="center">总计
                        <div property="columns">
                            <div field="bwZjBjmAmt" name="bwZjBjmAmt" width="120" align="center" headerAlign="center"
                                 allowSort="false" numberFormat="n4">本季末数额
                            </div>
                            <div field="bwZjSjmAmt" name="bwZjSjmAmt" width="120" align="center" headerAlign="center"
                                 allowSort="false" numberFormat="n4">上季末数额
                            </div>
                        </div>
                    </div>
                    <div field="bwBz" name="bwBz" width="120" align="center" headerAlign="center" allowSort="false">备注
                    </div>
                </div>
            </div>
            <div header="表内资金投资产品形式及余额" headerAlign="center">
                <div property="columns">
                    <div width="120" headerAlign="center">债券
                        <div property="columns">
                            <div field="bnZqBjmAmt" name="bnZqBjmAmt" width="120" align="center" headerAlign="center"
                                 allowSort="false" numberFormat="n4">本季末数额
                            </div>
                            <div field="bnZqSjmAmt" name="bnZqSjmAmt" width="120" align="center" headerAlign="center"
                                 allowSort="false" numberFormat="n4">上季末数额
                            </div>
                        </div>
                    </div>
                    <div width="120" headerAlign="center">非标准化债权类投资
                        <div property="columns">
                            <div field="bnFbzzqBjmAmt" name="bnFbzzqBjmAmt" width="140" align="center"
                                 headerAlign="center" allowSort="false" numberFormat="n4">本季末数额
                            </div>
                            <div field="bnFbzzqSjmAmt" name="bnFbzzqSjmAmt" width="140" align="center"
                                 headerAlign="center" allowSort="false" numberFormat="n4">上季末数额
                            </div>
                        </div>
                    </div>
                    <div width="120" headerAlign="center">其他（请列明）
                        <div property="columns">
                            <div field="bnQtBjmAmt" name="bnQtBjmAmt" width="120" align="center" headerAlign="center"
                                 allowSort="false" numberFormat="n4">本季末数额
                            </div>
                            <div field="bnQtSjmAmt" name="bnQtSjmAmt" width="120" align="center" headerAlign="center"
                                 allowSort="false" numberFormat="n4">上季末数额
                            </div>
                        </div>
                    </div>
                    <div width="120" headerAlign="center">总计
                        <div property="columns">
                            <div field="bnZjBjmAmt" name="bnZjBjmAmt" width="120" align="center" headerAlign="center"
                                 allowSort="false" numberFormat="n4">本季末数额
                            </div>
                            <div field="bnZjSjmAmt" name="bnZjSjmAmt" width="120" align="center" headerAlign="center"
                                 allowSort="false" numberFormat="n4">上季末数额
                            </div>
                        </div>
                    </div>
                    <div field="bnBz" name="bnBz" width="120" align="center" headerAlign="center" allowSort="false">备注
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<script>
    mini.parse();
    var url = window.location.search;
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var id = params.id;
    var grid = mini.get("datagrid");
    var userId = '<%=__sessionUser.getUserId()%>';
    var form = new mini.Form("#search_form");

    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize, pageIndex);
    });


    // 查询
    function search(pageSize, pageIndex) {
        var form = new mini.Form("#search_form");
        form.validate();
        if (form.isValid() == false) {
            mini.alert("信息填写有误，请重新填写", "系统也提示");
            return;
        }
        var data = form.getData(true);
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        data['branchId'] = branchId;
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: "/IfsReportCshbnwtzJczcReportController/searchIfsReportCshbnwtzJczcReportPage",
            data: params,
            callback: function (data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);

                var marges = [
                    {rowIndex: 30, columnIndex: 1, rowSpan: 3, colSpan: 1}//难以实时统计基础资产的产品

                    , {rowIndex: 30, columnIndex: 2, rowSpan: 1, colSpan: 2}//公募基金
                    , {rowIndex: 30, columnIndex: 4, rowSpan: 1, colSpan: 2}//无法穿透的结构化业务（不含资产支持证券）
                    , {rowIndex: 30, columnIndex: 6, rowSpan: 1, colSpan: 2}//其他（请列明）
                    , {rowIndex: 30, columnIndex: 8, rowSpan: 1, colSpan: 2}//总计
                    , {rowIndex: 30, columnIndex: 10, rowSpan: 2, colSpan: 1}//备注
                    , {rowIndex: 30, columnIndex: 11, rowSpan: 1, colSpan: 2}//公募基金
                    , {rowIndex: 30, columnIndex: 13, rowSpan: 1, colSpan: 2}//无法穿透的结构化业务（不含资产支持证券）
                    , {rowIndex: 30, columnIndex: 15, rowSpan: 1, colSpan: 2}//其他（请列明）
                    , {rowIndex: 30, columnIndex: 17, rowSpan: 1, colSpan: 2}//总计
                    , {rowIndex: 30, columnIndex: 19, rowSpan: 2, colSpan: 1}//备注

                    , {rowIndex: 33, columnIndex: 1, rowSpan: 3, colSpan: 1}//重点关注领域（可存在重叠）

                    , {rowIndex: 34, columnIndex: 2, rowSpan: 1, colSpan: 2}//债券
                    , {rowIndex: 34, columnIndex: 4, rowSpan: 1, colSpan: 2}//非标准化债权类投资
                    , {rowIndex: 34, columnIndex: 6, rowSpan: 1, colSpan: 2}//其他（请列明）
                    , {rowIndex: 34, columnIndex: 8, rowSpan: 1, colSpan: 2}//总计
                    , {rowIndex: 34, columnIndex: 10, rowSpan: 2, colSpan: 1}//备注
                    , {rowIndex: 34, columnIndex: 11, rowSpan: 1, colSpan: 2}//债券
                    , {rowIndex: 34, columnIndex: 13, rowSpan: 1, colSpan: 2}//非标准化债权类投资
                    , {rowIndex: 34, columnIndex: 15, rowSpan: 1, colSpan: 2}//其他（请列明）
                    , {rowIndex: 34, columnIndex: 17, rowSpan: 1, colSpan: 2}//总计
                    , {rowIndex: 34, columnIndex: 19, rowSpan: 2, colSpan: 1}//备注
                ];
                grid.mergeCells(marges);
            }
        });
    }

    function query() {
        search(grid.pageSize, 0);
    }

    function clear() {
        var form = new mini.Form("search_form");
        form.clear();
        search(10, 0);
    }

    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
            search(100, 0);
        });
    });


    //交易日期
    function onDrawDateStart(e) {
        var startDate = e.date;
        var endDate = mini.get("edate").getValue();
        if (CommonUtil.isNull(endDate)) {
            return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }

    function onDrawDateEnd(e) {
        var endDate = e.date;
        var startDate = mini.get("sdate").getValue();
        if (CommonUtil.isNull(startDate)) {
            return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }

    //生成报表
    function create() {
        form.validate();
        if (form.isValid() == false) {
            mini.alert("信息填写有误，请重新填写", "系统提示");
            return;
        }
        var data = form.getData(true);
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: "/IfsReportCshbnwtzJczcReportController/createIfsReportCshbnwtzJczc",
            data: params,
            complete: function (data) {
                var text = data.responseText;
                var desc = JSON.parse(text);
                var content = desc.obj.retMsg;
                mini.alert(content);
                search(100, 0);
            }
        });
    }

    //导出
    function exportExcel(){
        var data = form.getData(true);
        var fields = null;
        var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
        fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
        exportHrbExcel(fields);
    }
</script>
</body>
</html>