<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
    <title>G14台账</title>
</head>
<body>
<fieldset class="mini-fieldset title">
    <div id="search_form" style="width:100%">
        <legend>报表查询</legend>
        <input id="queryDate" name="queryDate" class="mini-datepicker" labelField="true" label="日期:"
               value="<%=__bizDate%>" labelSyle="text-align:right;" emptyText="请输入"></input>
        <input id="sheetType" name="sheetType"  class="mini-combobox" data="CommonUtil.serverData.dictionary.curType" labelField="true"
               label="类型:" labelSyle="text-align:right;" emptyText="请输入" value="1"></input>
        <span>
<%--            <a class="mini-button" style="display: none"  id="clear_btn" onclick="clear()">清空</a>--%>
            <a class="mini-button" style="display: none"  id="create_btn" onclick="query()">查询</a>
            <a id="export_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
        </span>
    </div>
</fieldset>
<div class="mini-fit" style="width:100%;height:100%;">
    <div id="data_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
         sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
         border="true" allowResize="true">
        <div property="columns">
            <div field="seq" headerAlign="center" align="center" width="50px">序号</div>
            <div field="secid" headerAlign="center" align="center" width="150px">债券代码</div>
            <div field="descr" headerAlign="center" align="center" width="150px">债券名称</div>
            <div field="secType" headerAlign="center" align="center" width="150px">债券种类</div>
            <div field="assetType" headerAlign="center" align="center" width="150px">LCR资产等级</div>
            <div field="prinamt" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">面值(万元)</div>
            <div field="rate" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">票面利率(%)</div>
            <div field="lev" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">主题评级</div>
            <div field="crating" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">债项评级</div>
            <div field="vdate" headerAlign="center" align="center" width="150px">发行日</div>
            <div field="mdate" headerAlign="center" align="center" width="150px">到期日</div>
            <div field="exerviseyears" headerAlign="center" align="center" width="150px">剩余期限(年)</div>
            <div field="pledgeState" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">质押状态</div>
            <div field="unpleAmt" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">未质押账面价值(万元)(可计入LCR金额)</div>
            <div field="pleAmt" headerAlign="center" align="center" width="150px">已质押账面价值(万元)</div>
            <div field="pleExeYears" headerAlign="center" align="center" width="150px">已质押剩余期限(年)</div>
            <div field="pldMdate" headerAlign="center" align="center" width="150px"  numberFormat="#,0.0000">质押到期日</div>
            <div field="pleRate" headerAlign="center" align="center" width="150px">人行借款 质押率(%)</div>
            <div field="val" headerAlign="center" align="center" width="150px">评估价值(万元)</div>
        </div>
    </div>
</div>
<script>
    mini.parse();

    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var id = params.id;

    var grid = mini.get("data_grid");
    var form = new mini.Form("#search_form");

    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize,pageIndex);
    });

    $(document).ready(function() {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
            query();
        });
    });

    //查询按钮
    function query(){
        search(100,0);
    }

    function search(pageSize, pageIndex) {
        var searchUrl="/IfsReportCurAssetController/searchAllPage";

        var data = form.getData(true);
        data['pageNum'] = pageIndex + 1;
        data['pageSize'] = pageSize;

        var params = mini.encode(data);

        CommonUtil.ajax({
            url:searchUrl,
            data:params,
            callback:function(data){
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }

    //清空按钮
    // function clear() {
    //     form.clear();
    //     query();
    // }

    function exportExcel(){
        var data = form.getData(true);
        var sheetTypeText=mini.get("sheetType").getText();
        var fields = null;
        var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
        fields += "<input type='hidden' id='sheetType' name='sheetType' value='"+data.sheetType+"'>";
        fields += "<input type='hidden' id='sheetTypeValue' name='sheetTypeValue' value='"+sheetTypeText+"'>";
        fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
        exportHrbExcel(fields);
    }

</script>
</body>
</html>
