<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>外债签约</title>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>
<body style="width: 100%; height: 100%; background: white">
<fieldset class="mini-fieldset">
	<legend>查询条件</legend>
	<div id="search_form" style="width: 100%;">
	 	 <input id="queryDate" name="queryDate" field="queryDate" class="mini-datepicker" labelField="true"  value="<%=__bizDate%>" label="日期："
	  required="true"  labelStyle="text-align:center;" allowinput="false" labelStyle="width:100px" width="280px" onvaluechanged="search(10,0)"  />
       
        <span>
			<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
			<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
			<a id="export_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
		</span>
	</div>
</fieldset>
	
<div class="mini-fit" style="margin-top: 2px;">
	<div id="datagrid" class="mini-datagrid borderAll" style="width: 100%; height: 100%;" allowAlternating="true"
		allowResize="true" border="true" sortMode="client" multiSelect="true">
		<div property="columns">
    	  <div field="dataNo" width="250px" headerAlign="center">外债编号</div>
			<div field="swiftcod" width="100px" headerAlign="center">swift编码</div>
	      <div field="orgNo" width="100px" headerAlign="center" align="center">外债人代码</div>
        <div field="debtType" width="100px" headerAlign="center" align="center" >债务类型</div>
        <div field="subDebtType" width="120px"  align="center" headerAlign="center" >二级债务类型</div>
        <div field="ccy" width="100px" headerAlign="center" align="center"  >货币类型</div>
        <div field="openDate" width="100px" headerAlign="center" align="center"  >起息日</div>
        <div field="baseArea" width="250px" headerAlign="center"align="center">债权人总部所在国家(地区)代码 </div>
        <div field="factoryArea" width="250px" headerAlign="center"align="center" >债权人经营地所在国家(地区)代码 </div>
        <div field="typeCode" width="120px" headerAlign="center" align="center" >债权人类型代码</div>
    	  <div field="subTypeCode" width="150px" headerAlign="center">债权人类型二级代码</div>
	      <div field="code" width="150px" headerAlign="center" align="center">债权人代码</div>
        <div field="cnName" width="150px" headerAlign="center" align="center" >债权人中文名</div>
        <div field="sn" width="150px"  align="center" headerAlign="center" >债权人英文名</div>
        <div field="sign" width="250px" headerAlign="center" align="center"  >是否不纳入跨境融资风险加权余额计算</div>
        <div field="rateSign" width="100px" headerAlign="center" align="center"  >是否浮动利率</div>
        <div field="rate" width="80px" headerAlign="center"align="center">年化利率值 </div>
        <div field="dealType" width="100px" headerAlign="center"align="center" >存款业务类型 </div>
        <div field="link" width="120px" headerAlign="center" align="center" >对方与本方的关系</div>
        <div field="seDate" width="80px" headerAlign="center"align="center" >原始期限 </div>
        <div field="desc" width="80px" headerAlign="center" align="center" >备注</div>
		</div>
	</div>
</div>

<script>
	mini.parse();

	var url = window.location.search;
	var grid = mini.get("datagrid");
	var userId='<%=__sessionUser.getUserId()%>';
	var form = new mini.Form("#search_form");
	grid.on("beforeload", function(e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex;
		var pageSize = e.data.pageSize;
		search(pageSize, pageIndex);
	});
		
	// 查询
	function search(pageSize,pageIndex){
		var form = new mini.Form("#search_form");
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统也提示");
			return;
		}
		var data=form.getData(true);
		data['pageNum']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:"/ForeDebtContractController/getDataSet",
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
		
	function query() {
    	search(grid.pageSize, 0);
    }
	
	function clear(){
        var form=new mini.Form("search_form");
        form.clear();
       //自贸区改造加br
        mini.get("br").setValue(opicsBr);
        search(10,0);
	}
	
	$(document).ready(function() {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
			search(10, 0);
		});
	});
	

	//导出
    function exportExcel(){
        var content = grid.getData();
        if(content.length == 0){
            mini.alert("请先查询数据");
            return;
        }

    
    mini.confirm("您确认要导出Excel吗?","系统提示", 
            function (action) {
              if (action == "ok"){
                var data = form.getData(true);
                var fields = null;
                
                var  fields = "<input type='hidden' id='dealNo' name='dealNo' value='"+data.dealNo+"'>";
                fields += "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
                  fields += "<input type='hidden' id='excelName' name='excelName' value='wzqy.xls'>";
                  fields += "<input type='hidden' id='id' name='id' value='wzqy'>";
                var urls = CommonUtil.pPath + "/sl/HrbReportManageController/exportExcel";
                $('<form action="'+ urls +'" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();   
              }
            }
    );
    }
    

</script>
</body>
</html>