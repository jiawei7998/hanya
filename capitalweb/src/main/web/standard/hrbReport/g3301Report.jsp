<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../global.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>G3301银行账簿利率风险计量报表</title>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
</head>
<body style="width: 100%; height: 100%; background: white">
<fieldset class="mini-fieldset">
    <div id="search_form" style="width: 50%;">
        <legend>查询条件</legend>
        <input id="queryDate" name="queryDate" class="mini-datepicker mini-mustFill" required="true" labelField="true"
               label="日期：" required="true" labelStyle="text-align:right;" emptyText="起始日期" format="yyyy-MM-dd"
               value="<%=__bizDate%>"/>
        <span style="float:right;">
			<a id="search_btn" class="mini-button" style="display: none" onclick="create()">查询</a>
			<a id="export_btn" class="mini-button" style="display: none" onclick="exportExcel()">导出报表</a>
		</span>
    </div>
    <span style="float:right;">单位:万元</span>
</fieldset>

<div class="mini-fit" style="margin-top: 2px;margin-bottom: 2px;">
    <div id="datagrid" class="mini-datagrid borderAll" style="width: 100%; height: 100%;" allowAlternating="true" allowHeaderWrap="true" allowCellWrap ="true"
         multiSelect="true" allowResize="true" border="true" sortMode="client" multiSelect="true">
        <div property="columns">
            <div type="indexcolumn" headerAlign="center" width="40">序号</div>
            <div field="product" name="product" width="480" align="left" headerAlign="center"  allowSort="false">项目</div>
            <div field="amt" name="amt" width="120" align="right" headerAlign="center"  allowSort="false"  numberFormat="n2">合计金额</div>
            <div field="tenor1d" name="tenor1d" width="120" align="right" headerAlign="center"  allowSort="false"  numberFormat="n2">隔夜</div>
            <div field="tenor1m" name="tenor1m" width="120" align="right" headerAlign="center"  allowSort="false"  numberFormat="n2">隔夜-一个月（含）</div>
            <div field="tenor3m" name="tenor3m" width="120" align="right" headerAlign="center"  allowSort="false"  numberFormat="n2">1个月-3个月（含）</div>
            <div field="tenor6m" name="tenor6m" width="120" align="right" headerAlign="center"  allowSort="false"  numberFormat="n2">3个月-6个月（含）</div>
            <div field="tenor9m" name="tenor9m" width="120" align="right" headerAlign="center"  allowSort="false"  numberFormat="n2">6个月-9个月(含)</div>
            <div field="tenor1y" name="tenor1y" width="120" align="right" headerAlign="center"  allowSort="false"  numberFormat="n2">9个月-1年(含)</div>
            <div field="tenor1d5y" name="tenor1d5y" width="120" align="right" headerAlign="center"  allowSort="false"  numberFormat="n2">1年-1.5年(含)</div>
            <div field="tenor2y" name="tenor2y" width="120" align="right" headerAlign="center"  allowSort="false"  numberFormat="n2">1.5年-2年(含)</div>
            <div field="tenor3y" name="tenor3y" width="120" align="right" headerAlign="center"  allowSort="false"  numberFormat="n2">2年-3年(含)</div>
            <div field="tenor4y" name="tenor4y" width="120" align="right" headerAlign="center"  allowSort="false"  numberFormat="n2">3年-4年(含)</div>
            <div field="tenor5y" name="tenor5y" width="120" align="right" headerAlign="center"  allowSort="false"  numberFormat="n2">4年-5年(含)</div>
            <div field="tenor6y" name="tenor6y" width="120" align="right" headerAlign="center"  allowSort="false"  numberFormat="n2">5年-6年(含)</div>
            <div field="tenor7y" name="tenor7y" width="120" align="right" headerAlign="center"  allowSort="false"  numberFormat="n2">6年-7年(含)</div>
            <div field="tenor8y" name="tenor8y" width="120" align="right" headerAlign="center"  allowSort="false"  numberFormat="n2">7年-8年(含)</div>
            <div field="tenor9y" name="tenor9y" width="120" align="right" headerAlign="center"  allowSort="false"  numberFormat="n2">8年-9年(含)</div>
            <div field="tenor10y" name="tenor10y" width="120" align="right" headerAlign="center"  allowSort="false"  numberFormat="n2">9年-10年(含)</div>
            <div field="tenor15y" name="tenor15y" width="120" align="right" headerAlign="center"  allowSort="false"  numberFormat="n2">10年-15年(含)</div>
            <div field="tenor20y" name="tenor20y" width="120" align="right" headerAlign="center"  allowSort="false"  numberFormat="n2">15年-20年(含)</div>
            <div field="tenorRemaing" name="tenorRemaing" width="120" align="right" headerAlign="center"  allowSort="false"  numberFormat="n2">20年以上</div>
          </div>
    </div>
</div>

<script>
    mini.parse();
    var url = window.location.search;
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var id = params.id;
    var grid = mini.get("datagrid");
    var userId = '<%=__sessionUser.getUserId()%>';
    var form = new mini.Form("#search_form");

    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize, pageIndex);
    });


    // 查询
    function search(pageSize, pageIndex) {
        var form = new mini.Form("#search_form");
        form.validate();
        if (form.isValid() == false) {
            mini.alert("信息填写有误，请重新填写", "系统也提示");
            return;
        }
        var data = form.getData(true);
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        data['branchId'] = branchId;
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: "/IfsReportG3301ReportController/searchIfsReportG3301ReportPage",
            data: params,
            callback: function (data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }

    function query() {
        search(grid.pageSize, 0);
    }

    function clear() {
        var form = new mini.Form("search_form");
        form.clear();
        search(10, 0);
    }

    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
            search(100, 0);
        });
    });


    //交易日期
    function onDrawDateStart(e) {
        var startDate = e.date;
        var endDate = mini.get("edate").getValue();
        if (CommonUtil.isNull(endDate)) {
            return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }

    function onDrawDateEnd(e) {
        var endDate = e.date;
        var startDate = mini.get("sdate").getValue();
        if (CommonUtil.isNull(startDate)) {
            return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }

    //生成报表
    function create() {
        form.validate();
        if (form.isValid() == false) {
            mini.alert("信息填写有误，请重新填写", "系统提示");
            return;
        }
        var data = form.getData(true);
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: "/IfsReportG3301ReportController/createIfsReportG3301",
            data: params,
            complete: function (data) {
                var text = data.responseText;
                var desc = JSON.parse(text);
                var content = desc.obj.retMsg;
                mini.alert(content);
                search(100, 0);
            }
        });
    }

    //导出
    function exportExcel(){
        var data = form.getData(true);
        var fields = null;
        var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
        fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
        exportHrbExcel(fields);
    }
</script>
</body>
</html>