<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<title>同业客户基本信息</title>
</head>
<body>
    <fieldset class="mini-fieldset title">
        <div id="search_form" style="width:100%">
            <legend>报表查询</legend>
            <input id="queryDate" name="queryDate" class="mini-datepicker" labelField="true" 
                    value="<%=__bizDate%>" label="交易日期:" labelSyle="text-align:right;" emptyText="请输入"></input>
            <span>
                <a class="mini-button" style="display: none"  id="search_btn" onclick="query()">查询</a>
                <a class="mini-button" style="display: none"  id="clear_btn" onclick="clear()">清空</a>
                <a id="export_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
            </span>
        </div>
    </fieldset>
    <div class="mini-fit" style="width:100%;height:100%;">
        <div id="cust_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
            sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
            border="true" allowResize="true">
            <div property="columns">
                <div type="indexcolumn" headerAlign="center" align="center" width="30px">序号</div>
                <div field="custName"  headerAlign="center" align="center" width="50px">客户名称</div>
                <div field="custNo"  headerAlign="center" align="center" width="50px">客户代码</div>
                <div field="orgCode"  headerAlign="center" align="center" width="50px">组织机构代码</div>
                <div field="custType"  headerAlign="center" align="center" width="50px" renderer="CommonUtil.dictRenderer" data-options="{dict:'CType'}">客户类别</div>
                <div field="coun"  headerAlign="center" align="center" width="50px">国别及地区</div>
                <div field="inLevel"  headerAlign="center" align="center" width="50px">内部评级</div>
                <div field="outLevel"  headerAlign="center" align="center" width="50px">外部评级</div>
                <div field="ldAmt"  headerAlign="center" align="center" width="50px">拆放同业</div>
                <div field="ctAmt"  headerAlign="center" align="center" width="50px">存放同业</div>
                <div field="revRepoAmt"  headerAlign="center" align="center" width="50px">债券回购-买入返售</div>
                <div field="loanBySecurity"  headerAlign="center" align="center" width="50px">股票质押贷款</div>
                <div field="revRepoAsset"  headerAlign="center" align="center" width="50px">买入返售资产</div>
                <div field="buyDiscount"  headerAlign="center" align="center" width="50px">买断式转贴现</div>
                <div field="secAmt"  headerAlign="center" align="center" width="50px">持有债券</div>
                <div field="stock"  headerAlign="center" align="center" width="50px">股权投资</div>
                <div field="payForOth"  headerAlign="center" align="center" width="50px">同业代付</div>
                <div field="otherAmt"  headerAlign="center" align="center" width="50px">其他表内业务-存单投资</div>
            </div>
        </div>
    </div>
    <script>
        
        mini.parse();

        var grid = mini.get("cust_grid");
        var form = new mini.Form("#search_form");
        
        grid.on("beforeload", function (e) {
            e.cancel = true;
            var pageIndex = e.data.pageIndex; 
            var pageSize = e.data.pageSize;
            search(pageSize,pageIndex);
        });
        
        $(document).ready(function() {
            //控制按钮显示
            $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
                query();
            });
        });
    
        //查询按钮
        function query(){
            search(10,0);
        }
        
        function search(pageSize, pageIndex) {
            var searchUrl="/SameBasicInfoController/queryBasicInfo";
             
            var data = form.getData(true);
            data['pageNum'] = pageIndex + 1;
            data['pageSize'] = pageSize;
    
            var params = mini.encode(data);
     
            CommonUtil.ajax({
                url:searchUrl,
                data:params,
                callback:function(data){
                    grid.setTotalCount(data.obj.total);
                    grid.setPageIndex(pageIndex);
                    grid.setPageSize(pageSize);
                    grid.setData(data.obj.rows);
                }
            });
        }
        
        //清空按钮
        function clear() {
            form.clear();
            query();
        }
        
        function exportExcel(){
            var content = grid.getData();
            if(content.length == 0){
                mini.alert("请先查询数据");a
                return;
            }
            
            mini.confirm("您确认要导出Excel吗?","系统提示", 
                function (action) {
                    if (action == "ok"){
                        var data = form.getData(true);
                        var fields = null;
                        
                        var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
                          fields += "<input type='hidden' id='excelName' name='excelName' value='tyxx.xls'>";
                          fields += "<input type='hidden' id='id' name='id' value='tyxx'>";
                        var urls = CommonUtil.pPath + "/sl/HrbReportManageController/exportExcel";
                        $('<form action="'+ urls +'" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();
                    }
                }
            );
        }
    </script>
</body>
</html>
