<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../global.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
    <title>人行利率报备</title>
</head>
<body>
<fieldset class="mini-fieldset title">
    <div id="search_form" style="width:100%">
        <legend>报表查询</legend>
        <input id="queryDate" name="queryDate" class="mini-datepicker" labelField="true" label="交易日期:"
               labelSyle="text-align:right;" emptyText="请输入" value="<%=__bizDate%>"/>
        <input id="sheetType" name="sheetType" class="mini-combobox" width="320px"
               data="CommonUtil.serverData.dictionary.rhInbankDeal" emptyText="请选择报表" labelField="true"
               label="报表类型："
               labelStyle="text-align:right;" onvaluechanged="showDataGrid" value="1"/>
        <span>
        <a class="mini-button" style="display: none" id="search_btn" onclick="showDataGrid()">查询</a>
        <%--                <a class="mini-button" style="display: none"  id="clear_btn" onclick="clear()">清空</a>--%>
        <a id="export_btn" class="mini-button" style="display: none" onclick="exportExcel()">导出报表</a>
        </span>
    </div>
</fieldset>
<span style="float:right;">单位:元</span>
<div class="mini-fit" style="width:100%;height:100%;" id="parent">
    <div id="dataGrid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
         allowAlternating="true" border="true" allowResize="true">
    </div>
</div>

<script>
    mini.parse();
    var url = window.location.search;
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var id = params.id;
    var dataGrid = mini.get("dataGrid");
    var form = new mini.Form("#search_form");
    var pageIndex;
    var pageSize;
    var columns1 = [
        {field: "SEQ", width: 20, headerAlign: "center", header: '序号', align: "center"},
        {field: "SECID", width: 120, headerAlign: "center", header: '存单代码', align: "center"},
        {field: "CUST_NAME", width: 120, headerAlign: "center", header: '交易对手', align: "center"},
        {field: "VDATE", width: 120, headerAlign: "center", header: '起息日', align: "center"},
        {field: "MDATE", width: 120, headerAlign: "center", header: '到期日', align: "center"},
        {field: "FIELD", width: 120, headerAlign: "center", header: '到期收益率', align: "center"},
        {field: "DEAL_AMT", width: 120, headerAlign: "center", header: '交易金额', align: "center"}
    ];
    var columns2 = [
        {field: "SEQ", width: 20, headerAlign: "center", header: '序号', align: "center"},
        {field: "SECID", width: 120, headerAlign: "center", header: '存单代码', align: "center"},
        {field: "SEC_NAME", width: 120, headerAlign: "center", header: '存单名称', align: "center"},
        {field: "VDATE", width: 120, headerAlign: "center", header: '起息日', align: "center"},
        {field: "TENOR", width: 120, headerAlign: "center", header: '期限', align: "center"},
        {field: "MDATE", width: 120, headerAlign: "center", header: '到期日', align: "center"},
        {field: "FACE_AMT", width: 120, headerAlign: "center", header: '债券面值', align: "center"},
        {field: "FIELD", width: 120, headerAlign: "center", header: '到期收益率', align: "center"}
    ];
    var columns3 = [
        {field: "SEQ", width: 20, headerAlign: "center", header: '序号', align: "center"},
        {field: "CUST", width: 120, headerAlign: "center", header: '交易对手', align: "center"},
        {field: "VDATE", width: 120, headerAlign: "center", header: '业务开始日期', align: "center"},
        {field: "MDATE", width: 120, headerAlign: "center", header: '业务结束日期', align: "center"},
        {field: "RATE", width: 120, headerAlign: "center", header: '利率', align: "center"},
        {field: "AMT", width: 120, headerAlign: "center", header: '金额', align: "center"}
    ];
    var columns4 = [
        {field: "SEQ", width: 120, headerAlign: "center", header: '序号', align: "center"},
        {field: "CUST", width: 120, headerAlign: "center", header: '交易对手', align: "center"},
        {field: "VDATE", width: 120, headerAlign: "center", header: '业务开始日期', align: "center"},
        {field: "MDATE", width: 120, headerAlign: "center", header: '业务结束日期', align: "center"},
        {field: "RATE", width: 120, headerAlign: "center", header: '利率', align: "center"},
        {field: "AMT", width: 120, headerAlign: "center", header: '金额', align: "center"}
    ];
    var columns5 = [
        {field: "SEQ", width: 20, headerAlign: "center", header: '序号', align: "center"},
        {field: "DEAL_DIR", width: 120, headerAlign: "center", header: '交易方向', align: "center"},
        {field: "CUST", width: 120, headerAlign: "center", header: '交易对手', align: "center"},
        {field: "VDATE", width: 120, headerAlign: "center", header: '首次结算日', align: "center"},
        {field: "MDATE", width: 120, headerAlign: "center", header: '到期结算日', align: "center"},
        {field: "TENOR", width: 120, headerAlign: "center", header: '实际占款天数', align: "center"},
        {field: "REPO_RATE", width: 120, headerAlign: "center", header: '回购利率', align: "center"},
        {field: "AMT", width: 120, headerAlign: "center", header: '交易金额', align: "center"}
    ];
    var columns6 = [
        {field: "SEQ", width: 20, headerAlign: "center", header: '序号', align: "center"},
        {field: "SECID", width: 120, headerAlign: "center", header: '债券代码', align: "center"},
        {field: "SEC_NAME", width: 120, headerAlign: "center", header: '债券名称', align: "center"},
        {field: "VDATE", width: 120, headerAlign: "center", header: '起息日', align: "center"},
        {field: "TENOR", width: 120, headerAlign: "center", header: '期限', align: "center"},
        {field: "MDATE", width: 120, headerAlign: "center", header: '到期日', align: "center"},
        {field: "FACE_AMT", width: 120, headerAlign: "center", header: '债券面值', align: "center"},
        {field: "FACE_RATE", width: 120, headerAlign: "center", header: '票面利率', align: "center"}
    ];
    var columns7 = [
        {field: "SEQ", width: 20, headerAlign: "center", header: '序号', align: "center"},
        {field: "CUST", width: 120, headerAlign: "center", header: '交易对手', align: "center"},
        {field: "VDATE", width: 120, headerAlign: "center", header: '首次结算日', align: "center"},
        {field: "DEAL_DIR", width: 120, headerAlign: "center", header: '交易方向', align: "center"},
        {field: "MDATE", width: 120, headerAlign: "center", header: '到期结算日', align: "center"},
        {field: "TENOR", width: 120, headerAlign: "center", header: '实际占款天数', align: "center"},
        {field: "REPO_RATE", width: 120, headerAlign: "center", header: '回购利率', align: "center"},
        {field: "REPO_DAY", width: 120, headerAlign: "center", header: '回购天数', align: "center"},
        {field: "AMT", width: 120, headerAlign: "center", header: '交易金额', align: "center"}
    ];
    var columns = [columns1, columns2, columns3, columns4, columns5, columns6,columns7];

    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
            query(10, 0);
        });
    });

    dataGrid.on("beforeload", function (e) {
        e.cancel = true;
        pageIndex = e.data.pageIndex;
        pageSize = e.data.pageSize;
        query(pageSize, pageIndex);
    });

    //下拉选择需要展示的表格
    function showDataGrid() {
        query(10, 0);
    }

    //查询按钮
    function query(pageSize, pageIndex) {
        var sheetType = mini.get("sheetType").getValue();
        dataGrid.set({
            columns: columns[sheetType - 1]
        });
        var searchUrl = "/RhInnerbankController/searchAllPage";
        var data = form.getData(true);
        data.sheetType = sheetType;
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: searchUrl,
            data: params,
            callback: function (data) {
                dataGrid.setTotalCount(data.obj.total);
                dataGrid.setPageIndex(pageIndex);
                dataGrid.setPageSize(pageSize);
                dataGrid.setData(data.obj.rows);
            }
        });
    }


    function exportExcel(){
        var sheetType=mini.get("sheetType").getValue();
        var sheetTypeText=mini.get("sheetType").getText();
        var data = form.getData(true);
        var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
        fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
        fields += "<input type='hidden' id='sheetType' name='sheetType' value='"+sheetType+"'>";
        fields += "<input type='hidden' id='sheetTypeValue' name='sheetTypeValue' value='"+sheetTypeText+"'>";
        exportHrbExcel(fields);
    }

</script>
</body>
</html>
