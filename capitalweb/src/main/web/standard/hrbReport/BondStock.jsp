<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
<head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
	<script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>

</head>
<body style="width: 100%; height: 100%; background: white">
	<fieldset class="mini-fieldset"> 
		<legend>债券业务存量表</legend>
		<div id="search_form" style="width: 100%;margin:5px 0 5px 0">
			<input id="queryDate" name="queryDate" class="mini-datepicker" labelField="true" label="账务日期：" emptyText="账务日期" format="yyyy-MM-dd" value="<%=__bizDate%>"/>
			<span style="float: right; margin-left: 20px;margin-top:6px">
			        <a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
				    <a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
					<a id="export_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
			</span>
		</div>
	</fieldset>

<%--	<div class="mini-fit" style="margin-top: 2px;">--%>
<%--	<span style="float:right;">单位:万元</span>--%>
	<div id="grid1" class="mini-datagrid borderAll" style="width: 100%; height: 100%;" allowAlternating="true"
		allowResize="true" border="true" sortMode="client" multiSelect="true">
		<div property="columns">
			<div field="bondIssuer" headerAlign="center" align="center" width="120px">债券发行人</div>
			<div field="valueDate" headerAlign="center" align="center" width="120px">起息日</div>
			<div field="expireDate" headerAlign="center" align="center" width="120px">到期日</div>
			<div field="bookValue" headerAlign="center" align="center" width="120px">账面价值(元)</div>
		</div>
	</div>
</div>
	
<script>
	mini.parse();
	var url = window.location.search;
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var id = params.id;
	var implClass = params.implClass;

	var grid = mini.get("grid1");
	grid.on("beforeload", function(e) {
			e.cancel = true;
			var pageIndex = e.data.pageIndex;
			var pageSize = e.data.pageSize;
			search(pageSize, pageIndex);
		});
	$(document).ready(function() {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
			query();
		});
	});
	// 初始化数据
	function query(){
		search(grid.pageSize, 0);
	}
	function search(pageSize,pageIndex){
		var form = new mini.Form("#search_form");
		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		data.implClass=implClass;
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:"/ExportDataController/getExportPageData",
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
				grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	
	function clear(){
        var form=new mini.Form("search_form");
        form.clear();
        // mini.get("startDate").setValue(sysDate);
        // mini.get("endDate").setValue(sysDate);
        query();
	}
	
	//日期
	function onDrawDateStart(e) {
        var startDate = e.date;
        var endDate= mini.get("endDate").getValue();
        if(CommonUtil.isNull(endDate)){
        	return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }
	
	function onDrawDateEnd(e) {
        var endDate = e.date;
        var startDate = mini.get("startDate").getValue();
        if(CommonUtil.isNull(startDate)){
        	return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }
	//导出
	function exportExcel(){
		var form=new mini.Form("search_form");
		var data = form.getData(true);
		var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
		fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
		exportHrbExcel(fields);
	}
</script>
</body>
</html>