<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
<head>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>

<body style="width: 100%; height: 100%; background: white">
<h1 style="text-align:center;padding-top:5px;font-size:18px;"><strong>税费维护界面</strong></h1>

<div id="grid" class="mini-datagrid borderAll" style="width:100%;height:80%;" idField="id"
     allowAlternating="true" allowResize="true" showPager="false"  multiSelect="true" allowCellSelect="true" allowCellEdit="true">
    <div property="columns">
        <div name="tradeType" field="tradeType"  align="left" headerAlign="center" >交易品种</div>
        <div name="settlementMethod" field="settlementMethod"  align="center" headerAlign="center" >结算方式</div>
        <div name="chargeObject" field="chargeObject"  align="center" headerAlign="center" >收费对象</div>
        <div name="chargeable" field="chargeable"  align="center" headerAlign="center" >收费标的</div>

        <div header="收费标准" headerAlign="center">
            <div property="columns">
                <div name="amount" field="amount"  align="center" headerAlign="center">(元)
                    <input property="editor" class="mini-spinner" maxValue="99999999999999.9999999" format="n7" style="width:100%;" minWidth="100" />
                </div>
            </div>
        </div>
    </div>
</div>
<span style="margin:5px;display: block;">
        <a id="save_btn" class="mini-button" onclick="save()">保存</a>
    </span>
<script>
    mini.parse();
    var visibleBtn = CommonUtil.hideBtn();
    //获取当前tab
    var currTab = top["win"].tabs.getActiveTab();
    var param=currTab.params;
    var row=param?param.selectData[0]:{};
    var grid = mini.get("grid");
    grid.on("beforeload", function(e) {
    });

    grid.on("drawcell",function(e){
        var record = e.record,
            column = e.column,
            field = e.field,
            value = e.value;
        debugger
        if (field != "amount"&&(value!="上清所" && value!="中债" &&value!="前台税费[CFETS]")) {
            e.cellStyle = "background-color:#efefef";
        }
    });

    //定义初始化数据
    var gridData=[
        {tradeType:"上清所",settlementMethod:"",chargeObject:"",chargeable:"",amount:"",name:""},
        {tradeType:"现券",settlementMethod:"纯券过户",chargeObject:"结算双方",chargeable:"笔",amount:"",name:"sqsBondSettlement1"},
        {tradeType:"现券",settlementMethod:"DVP、PAD、DAP",chargeObject:"结算双方",chargeable:"笔",amount:"",name:"sqsBondSettlement2"},
        {tradeType:"质押式回购",settlementMethod:"DVP、PAD、DAP",chargeObject:"结算双方",chargeable:"单券种/笔",amount:"",name:"sqsCrSettlement2Single"},
        {tradeType:"质押式回购",settlementMethod:"DVP、PAD、DAP",chargeObject:"结算双方",chargeable:"多券种/笔",amount:"",name:"sqsCrSettlement2Multiple"},
        {tradeType:"买断式回购",settlementMethod:"DVP、PAD、DAP",chargeObject:"结算双方",chargeable:"笔",amount:"",name:"sqsOrSettlement2"},
        {tradeType:"远期交易",settlementMethod:"DVP、PAD、DAP",chargeObject:"结算双方",chargeable:"笔",amount:"",name:"sqsForwardSettlement2"},
        {tradeType:"账户维护费",settlementMethod:"",chargeObject:"A类",chargeable:"元/月/户",amount:"",name:"sqsFeeClassA"},
        {tradeType:"中债",settlementMethod:"",chargeObject:"",chargeable:"",amount:"",name:""},
        {tradeType:"现券",settlementMethod:"纯券过户",chargeObject:"结算双方",chargeable:"笔",amount:"",name:"zzBondSettlement1"},
        {tradeType:"现券",settlementMethod:"DVP、PAD、DAP",chargeObject:"结算双方",chargeable:"笔",amount:"",name:"zzBondSettlement2"},
        {tradeType:"质押式回购",settlementMethod:"DVP、PAD、DAP",chargeObject:"结算双方",chargeable:"单券种/笔",amount:"",name:"zzCrSettlement2Single"},
        {tradeType:"质押式回购",settlementMethod:"DVP、PAD、DAP",chargeObject:"结算双方",chargeable:"多券种/笔",amount:"",name:"zzCrSettlement2Multiple"},
        {tradeType:"买断式回购",settlementMethod:"DVP、PAD、DAP",chargeObject:"结算双方",chargeable:"笔",amount:"",name:"zzOrSettlement2"},
        {tradeType:"远期交易",settlementMethod:"DVP、PAD、DAP",chargeObject:"结算双方",chargeable:"笔",amount:"",name:"zzForwardSettlement2"},
        {tradeType:"账户维护费",settlementMethod:"",chargeObject:"甲类",chargeable:"超过200亿元 （元/月/户 ）",amount:"",name:"zzFeeClassFirst1"},
        {tradeType:"账户维护费",settlementMethod:"",chargeObject:"甲类",chargeable:"小于200亿元 （元/月/户 ）",amount:"",name:"zzFeeClassFirst2"},
        {tradeType:"前台税费[CFETS]",settlementMethod:"",chargeObject:"",chargeable:"",amount:"",name:"中债"},
        {tradeType:"融资类交易",settlementMethod:"1天(含隔夜)",chargeObject:"",chargeable:"交易金额",amount:"",name:"qtsfFinancing1"},
        {tradeType:"融资类交易",settlementMethod:"2天以上（含两天）",chargeObject:"",chargeable:"交易金额",amount:"",name:"qtsfFinancing2"},
        {tradeType:"买卖类交易",settlementMethod:"",chargeObject:"",chargeable:"交易金额",amount:"",name:"qtsfBs"},
        {tradeType:"终端费",settlementMethod:"",chargeObject:"",chargeable:"终端数",amount:"",name:"qtsfTerminal"},

    ];

    $(document).ready(function() {
        gridData.forEach(e=>{
            e.amount=row[e.name];
            if (e.amount) {
                if (e.amount.toString().indexOf("e") != -1) {
                    e.amount = e.amount.toFixed(7);
                }
            }
        })
        grid.setData(gridData);
        var marges = [
            { rowIndex: 0, columnIndex: 0, rowSpan: 1, colSpan: 5 },
            { rowIndex: 8, columnIndex: 0, rowSpan: 1, colSpan: 5 },
            { rowIndex: 17, columnIndex: 0, rowSpan: 1, colSpan: 5 },
            { rowIndex: 1, columnIndex: 0, rowSpan: 2, colSpan: 1 },
            { rowIndex: 1, columnIndex: 2, rowSpan: 2, colSpan: 1 },
            { rowIndex: 1, columnIndex: 3, rowSpan: 2, colSpan: 1 },
            { rowIndex: 3, columnIndex: 0, rowSpan: 2, colSpan: 1 },
            { rowIndex: 3, columnIndex: 1, rowSpan: 2, colSpan: 1 },
            { rowIndex: 3, columnIndex: 2, rowSpan: 2, colSpan: 1 },
            { rowIndex: 9, columnIndex: 0, rowSpan: 2, colSpan: 1 },
            { rowIndex: 9, columnIndex: 2, rowSpan: 2, colSpan: 1 },
            { rowIndex: 9, columnIndex: 3, rowSpan: 2, colSpan: 1 },
            { rowIndex: 11, columnIndex: 0, rowSpan: 2, colSpan: 1 },
            { rowIndex: 11, columnIndex: 1, rowSpan: 2, colSpan: 1 },
            { rowIndex: 11, columnIndex: 2, rowSpan: 2, colSpan: 1 },
            { rowIndex: 15, columnIndex: 0, rowSpan: 2, colSpan: 1 },
            { rowIndex: 15, columnIndex: 1, rowSpan: 2, colSpan: 1 },
            { rowIndex: 15, columnIndex: 2, rowSpan: 2, colSpan: 1 },
            { rowIndex: 18, columnIndex: 0, rowSpan: 2, colSpan: 1 },
            { rowIndex: 18, columnIndex: 1, rowSpan: 2, colSpan: 1 },
            { rowIndex: 18, columnIndex: 2, rowSpan: 2, colSpan: 1 },


        ];
        grid.margeCells(marges);
    });



    //保存
    function save(){
        var param={id:row.id};
        var params= new Array();
        grid.getData().forEach(e=>{
            param[e.name]=e.amount
        })
        params.push(param)
        CommonUtil.ajax({
            url:"/IfsTaxesController/addData",
            data:params,
            callback : function(data) {
                mini.alert("保存成功!");
                top["win"].closeMenuTab();
                // query();
            }
        });

    }

</script>
</body>
</html>