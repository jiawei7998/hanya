<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
<head>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>

<body style="width: 100%; height: 100%; background: white">
<h1 style="text-align:center;padding-top:5px;font-size:18px;"><strong>营改增编辑页面</strong></h1>
<div id="field_form" class="mini-fit area" style="background:white;">
    <div class="mini-panel" title="营改增" style="width:100%;" allowResize="true" collapseOnTitleClick="true">
        <div class="leftarea">
            <input style="width:80%;" id="dealno" name="dealno" class="mini-textbox"
                   labelField="true"  label="交易号：" labelStyle="text-align:left;width:180px;"/>
            <input style="width:80%;" id="sectype" name="sectype" class="mini-textbox"
                   labelField="true"  label="债券类型：" labelStyle="text-align:left;width:180px;" />
            <input style="width:80%;" id="secname" name="secname" class="mini-textbox"
                   labelField="true"  label="债券名称：" labelStyle="text-align:left;width:180px;"/>
            <input style="width:80%;" id="vdate" name="vdate" class="mini-datepicker"
                   labelField="true"  label="起息日：" labelStyle="text-align:left;width:180px;"/>
            <input style="width:80%;" id="rate" name="rate" class="mini-textbox"
                   labelField="true"  label="固定利率：" labelStyle="text-align:left;width:180px;"/>
            <input style="width:80%;" id="intpayrule" name="intpayrule" class="mini-textbox"
                   labelField="true"  label="付息频率：" labelStyle="text-align:left;width:180px;"/>
            <input style="width:80%;" id="samount" name="samount" class="mini-textbox"
                   labelField="true"  label="券面价值：" labelStyle="text-align:left;width:180px;"/>
            <input style="width:80%;" id="buyavgamt" name="buyavgamt" class="mini-textbox"
                   labelField="true"  label="买入价：" labelStyle="text-align:left;width:180px;"/>
            <input style="width:80%;" id="inputdate" name="inputdate" class="mini-datepicker"
                   labelField="true"  label="生成日期：" labelStyle="text-align:left;width:180px;"/>
        </div>
        <div class="rightarea">
            <input style="width:80%;" id="invtye" name="invtye" class="mini-textbox"
                   labelField="true"  label="三分类：" labelStyle="text-align:left;width:180px;"/>
            <input style="width:80%;" id="seccode" name="seccode" class="mini-textbox"
                   labelField="true"  label="债券代码：" labelStyle="text-align:left;width:180px;"/>
            <input style="width:80%;" id="term" name="term" class="mini-textbox"
                   labelField="true"  label="期限：" labelStyle="text-align:left;width:180px;"/>
            <input style="width:80%;" id="mdate" name="mdate" class="mini-datepicker"
                   labelField="true"  label="到期日：" labelStyle="text-align:left;width:180px;"/>
            <input style="width:80%;" id="frate" name="frate" class="mini-textbox"
                   labelField="true"  label="浮动利率：" labelStyle="text-align:left;width:180px;"/>
            <input style="width:80%;" id="saeldate" name="saeldate" class="mini-datepicker"
                   labelField="true"  label="卖出日期：" labelStyle="text-align:left;width:180px;"/>
            <input style="width:80%;" id="sellprice" name="sellprice" class="mini-textbox"
                   labelField="true"  label="卖出价：" labelStyle="text-align:left;width:180px;"/>
            <input style="width:80%;" id="diffinprice" name="diffinprice" class="mini-textbox"
                   labelField="true"  label="价差收入：" labelStyle="text-align:left;width:180px;"/>
        </div>
    </div>
</div>


<span style="margin:5px;display: block;">
        <a id="save_btn" class="mini-button" onclick="save()">保存</a>
    </span>
<script>
    mini.parse();
    var visibleBtn = CommonUtil.hideBtn();
    //获取当前tab
    var currTab = top["win"].tabs.getActiveTab();
    var param=currTab.params;
    var row=param?param.selectData[0]:{};
    var form = new mini.Form("field_form");
    $(document).ready(function() {
        if(row.id){
            console.log(row)
            form.setData(row);
        }
    });

    //保存
    function save(){
        var data = form.getData(true);
        data.id=row.id;
        data.version=row.version;
        CommonUtil.ajax({
            url:"/KYgzController/addOrUpdate",
            data:data,
            callback : function(data) {
                mini.alert("保存成功!");
                top["win"].closeMenuTab();
                // query();
            }
        });

    }

</script>
</body>
</html>