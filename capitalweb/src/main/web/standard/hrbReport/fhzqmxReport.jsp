<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../global.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>分行债券明细表</title>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
</head>
<body style="width: 100%; height: 100%; background: white">
<fieldset class="mini-fieldset">
    <div id="search_form" style="width: 50%;">
        <legend>查询条件</legend>
        <input id="queryDate" name="queryDate" class="mini-datepicker mini-mustFill" required="true" labelField="true"
               label="日期：" required="true" labelStyle="text-align:center;" emptyText="起始日期" format="yyyy-MM-dd"
               value="<%=__bizDate%>"/>
        <span style="float:right;">
			<a id="search_btn" class="mini-button" style="display: none" onclick="create()">查询</a>
			<a id="export_btn" class="mini-button" style="display: none" onclick="exportExcel()">导出报表</a>
		</span>
    </div>
</fieldset>

<div class="mini-fit" style="margin-top: 2px;margin-bottom: 2px;">
    <div id="datagrid" class="mini-datagrid borderAll" style="width: 100%; height: 100%;" allowAlternating="true" allowHeaderWrap="true" allowCellWrap ="true"
         multiSelect="true" allowResize="true" border="true" sortMode="client" multiSelect="true">
        <div property="columns">
            <div type="indexcolumn" headerAlign="center" width="40">序号</div>
            <div field="fgbm" name="fgbm" width="130" align="center" headerAlign="center" allowSort="false">分管部门</div>
            <div field="njfh" name="njfh" width="130" align="center" headerAlign="center" allowSort="false">哪家分行</div>
            <div field="zqlx" name="zqlx" width="130" align="center" headerAlign="center" allowSort="false">债券类型</div>
            <div field="zqmc" name="zqmc" width="130" align="center" headerAlign="center" allowSort="false">债券名称</div>
            <div field="zqdm" name="zqdm" width="130" align="center" headerAlign="center" allowSort="false">债券代码</div>
            <div field="qxr" name="qxr" width="130" align="center" headerAlign="center" allowSort="false">起息日</div>
            <div field="dqr" name="dqr" width="130" align="center" headerAlign="center" allowSort="false">到期日</div>
            <div field="zqmz" name="zqmz" width="130" align="right" headerAlign="center" allowSort="false" numberFormat="n2">债券面值</div>
            <div field="pmll" name="pmll" width="130" align="right" headerAlign="center" allowSort="false"  numberFormat="n2">票面利率</div>
            <div field="jssyqjksr" name="jssyqjksr" width="130" align="center" headerAlign="center" allowSort="false">计算收益区间开始日</div>
            <div field="jssyqjdqr" name="jssyqjdqr" width="130" align="center" headerAlign="center" allowSort="false">计算收益区间到期日</div>
            <div field="jsts" name="jsts" width="130" align="center" headerAlign="center" allowSort="false">计算天数</div>
            <div field="sy" name="sy" width="130" align="right" headerAlign="center" allowSort="false"  numberFormat="n2">收益</div>
            <div field="zzs" name="zzs" width="130" align="right" headerAlign="center" allowSort="false"  numberFormat="n2">增值税</div>
            <div field="xhfhsy" name="xhfhsy" width="130" align="right" headerAlign="center" allowSort="false"  numberFormat="n2">下划分行收益</div>
            <div field="jssyqjsjsdlx" name="jssyqjsjsdlx" width="180" align="right" headerAlign="center" allowSort="false"  numberFormat="n2">计算收益区间实际收到利息</div>
        </div>
    </div>
</div>

<script>
    mini.parse();
    var url = window.location.search;
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var id = params.id;
    var grid = mini.get("datagrid");
    var userId = '<%=__sessionUser.getUserId()%>';
    var form = new mini.Form("#search_form");

    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize, pageIndex);
    });


    // 查询
    function search(pageSize, pageIndex) {
        var form = new mini.Form("#search_form");
        form.validate();
        if (form.isValid() == false) {
            mini.alert("信息填写有误，请重新填写", "系统也提示");
            return;
        }
        var data = form.getData(true);
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        data['branchId'] = branchId;
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: "/IfsReportFhzqmxReportController/searchIfsReportFhzqmxReportPage",
            data: params,
            callback: function (data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }

    function query() {
        search(grid.pageSize, 0);
    }

    function clear() {
        var form = new mini.Form("search_form");
        form.clear();
        search(10, 0);
    }

    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
            search(100, 0);
        });
    });


    //交易日期
    function onDrawDateStart(e) {
        var startDate = e.date;
        var endDate = mini.get("edate").getValue();
        if (CommonUtil.isNull(endDate)) {
            return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }

    function onDrawDateEnd(e) {
        var endDate = e.date;
        var startDate = mini.get("sdate").getValue();
        if (CommonUtil.isNull(startDate)) {
            return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }

    //生成报表
    function create() {
        form.validate();
        if (form.isValid() == false) {
            mini.alert("信息填写有误，请重新填写", "系统提示");
            return;
        }
        var data = form.getData(true);
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: "/IfsReportFhzqmxReportController/createIfsReportFhzqmx",
            data: params,
            complete: function (data) {
                var text = data.responseText;
                var desc = JSON.parse(text);
                var content = desc.obj.retMsg;
                mini.alert(content);
                search(100, 0);
            }
        });
    }

    //导出
    function exportExcel(){
        var data = form.getData(true);
        var fields = null;
        var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
        fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
        exportHrbExcel(fields);
    }
</script>
</body>
</html>