<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../global.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>金融市场部业务管理报表  债券投资账户结构表(按会计账户）</title>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
</head>
<body style="width: 100%; height: 100%; background: white">
<fieldset class="mini-fieldset">
    <div id="search_form" style="width: 50%;">
        <legend>查询条件</legend>
        <input id="queryDate" name="queryDate" class="mini-datepicker mini-mustFill" required="true" labelField="true"
               label="日期：" required="true" labelStyle="text-align:right;" emptyText="起始日期" format="yyyy-MM-dd"
               value="<%=__bizDate%>"/>
        <span style="float:right;">
			<a id="search_btn" class="mini-button" style="display: none" onclick="create()">查询</a>
			<a id="export_btn" class="mini-button" style="display: none" onclick="exportExcel()">导出报表</a>
		</span>
    </div>
    <span style="float:right;">单位:亿元，%</span>
</fieldset>

<div class="mini-fit" style="margin-top: 2px;margin-bottom: 2px;">
    <div id="datagrid" class="mini-datagrid borderAll" style="width: 100%; height: 100%;" allowAlternating="true" allowHeaderWrap="true" allowCellWrap ="true"
         multiSelect="true" allowResize="true" border="true" sortMode="client" multiSelect="true">
        <div property="columns">
            <div type="indexcolumn" headerAlign="center" width="40">序号</div>
            <div field="zhtype" name="zhtype" width="150" align="left" headerAlign="center" allowSort="false">账户类型</div>
            <div field="ye" name="ye" width="130" align="right" headerAlign="center" allowSort="false"  numberFormat="n8">余额</div>
            <div field="rje" name="rje" width="130" align="right" headerAlign="center" allowSort="false" numberFormat="n8">日均额</div>
            <div field="dcq" name="dcq" width="130" align="right" headerAlign="center" allowSort="false"  numberFormat="n2">待偿期（年）</div>
            <div field="syl" name="syl" width="130" align="right" headerAlign="center" allowSort="false" numberFormat="n8">收益率</div>
            <div field="hyjsyl" name="hyjsyl" width="130" align="right" headerAlign="center" allowSort="false"  numberFormat="n8">含溢价收益率</div>
            <div field="fy" name="fy" width="130" align="right" headerAlign="center" allowSort="false" numberFormat="n8">浮盈（亏）</div>
            <div field="lxsr" name="lxsr" width="130" align="right" headerAlign="center" allowSort="false"  numberFormat="n8">利息收入</div>
            <div field="czsy" name="czsy" width="130" align="right" headerAlign="center" allowSort="false" numberFormat="n8">处置收益</div>
            <div field="zjywsr" name="zjywsr" width="130" align="center" headerAlign="center" allowSort="false"  numberFormat="n8">中间业务收入</div>
            <div field="syye" name="syye" width="130" align="right" headerAlign="center" allowSort="false" numberFormat="n8">上月余额</div>
            <div field="jsy" name="jsy" width="130" align="right" headerAlign="center" allowSort="false" numberFormat="n8">较上月(+/-)</div>
            <div field="jsyzf" name="jsyzf" width="130" align="right" headerAlign="center" allowSort="false" numberFormat="n8">较上月增幅</div>
            <div field="ncye" name="ncye" width="130" align="right" headerAlign="center" allowSort="false" numberFormat="n8">年初余额</div>
            <div field="jnc" name="jnc" width="130" align="right" headerAlign="center" allowSort="false" numberFormat="n8">较年初(+/-)</div>
            <div field="jnczf" name="jnczf" width="130" align="right" headerAlign="center" allowSort="false" numberFormat="n8">较年初增幅</div>
        </div>
    </div>
</div>

<script>
    mini.parse();
    var url = window.location.search;
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var id = params.id;
    var grid = mini.get("datagrid");
    var userId = '<%=__sessionUser.getUserId()%>';
    var form = new mini.Form("#search_form");

    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize, pageIndex);
    });


    // 查询
    function search(pageSize, pageIndex) {
        var form = new mini.Form("#search_form");
        form.validate();
        if (form.isValid() == false) {
            mini.alert("信息填写有误，请重新填写", "系统也提示");
            return;
        }
        var data = form.getData(true);
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        data['branchId'] = branchId;
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: "/IfsReportJrscbywglZqtzzhazhReportController/searchIfsReportJrscbywglZqtzzhazhReportPage",
            data: params,
            callback: function (data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }

    function query() {
        search(grid.pageSize, 0);
    }

    function clear() {
        var form = new mini.Form("search_form");
        form.clear();
        search(10, 0);
    }

    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
            search(100, 0);
        });
    });


    //交易日期
    function onDrawDateStart(e) {
        var startDate = e.date;
        var endDate = mini.get("edate").getValue();
        if (CommonUtil.isNull(endDate)) {
            return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }

    function onDrawDateEnd(e) {
        var endDate = e.date;
        var startDate = mini.get("sdate").getValue();
        if (CommonUtil.isNull(startDate)) {
            return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }

    //生成报表
    function create() {
        form.validate();
        if (form.isValid() == false) {
            mini.alert("信息填写有误，请重新填写", "系统提示");
            return;
        }
        var data = form.getData(true);
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: "/IfsReportJrscbywglZqtzzhazhReportController/createIfsReportJrscbywglZqtzzhazh",
            data: params,
            complete: function (data) {
                var text = data.responseText;
                var desc = JSON.parse(text);
                var content = desc.obj.retMsg;
                mini.alert(content);
                search(100, 0);
            }
        });
    }

    //导出
    function exportExcel(){
        var data = form.getData(true);
        var fields = null;
        var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
        fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
        exportHrbExcel(fields);
    }
</script>
</body>
</html>