<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>地方政府债券明细</title>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
	<script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
</head>
<body style="width: 100%; height: 100%; background: white">
<fieldset class="mini-fieldset">
	<legend>查询条件</legend>
	<div id="search_form" style="width: 100%;">
	 	 <input id="queryDate" name="queryDate" field="queryDate" class="mini-datepicker" labelField="true"  value="<%=__bizDate%>" label="日期："
	  required="true"  labelStyle="text-align:center;" allowinput="false" labelStyle="width:100px" width="280px" onvaluechanged="search(10,0)"  />
		<input id="type" name="type" class="mini-combobox" width="320px"
			   data="CommonUtil.serverData.dictionary.localGovType" emptyText="请选择报表" labelField="true" label="报表类型："
			   labelStyle="text-align:right;" onvaluechanged="showDataGrid" value="1"/>
        <span style="float: center; margin-center: 50px">
			<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
			<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
			<a id="export_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
		</span>
	</div>
</fieldset>
	
<div class="mini-fit" style="margin-top: 2px;">
	<div id="datagrid" class="mini-datagrid borderAll" style="width: 100%; height: 50%;" allowAlternating="true"
		allowResize="true" border="true" sortMode="client" multiSelect="true">
		<div property="columns">

		</div>
	</div>
	<div id="datagrid2" class="mini-datagrid borderAll" style="width: 100%; height: 50%;" allowAlternating="true"
		 allowResize="true" border="true" sortMode="client" multiSelect="true">
		<div property="columns">

		</div>
	</div>
</div>

<script>
	mini.parse();
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var id = params.id;
	var implClass = params.implClass;
	var url = window.location.search;
	var grid = mini.get("datagrid");
	var grid2 = mini.get("datagrid2");
	var userId='<%=__sessionUser.getUserId()%>';
	var form = new mini.Form("#search_form");

	var columns1=[
		{field: "invtype", width: 100, headerAlign: "center", header: '账户类型', align: "center"},
		{field: "acctngtype", width: 100, headerAlign: "center", header: '债券类型', align: "center"},
		{field: "issuer", width: 100, headerAlign: "center", header: '债券发行人', align: "center"},
		{field: "secid", width: 100, headerAlign: "center", header: '债券代码', align: "center"},
		{field: "descr", width: 100, headerAlign: "center", header: '债券名称', align: "center"},
		{field: "vadate", width: 100, headerAlign: "center", header: '起息日', align: "center"},
		{field: "tenor", width: 100, headerAlign: "center", header: '期限', align: "center"},
		{field: "mdate", width: 100, headerAlign: "center", header: '到期日', align: "center"},
		{field: "paramt", width: 100, headerAlign: "center", header: '债券面值', align: "center"},
		{field: "couprate_8", width: 100, headerAlign: "center", header: '票面利率', align: "center"},
		{field: "redempamt", width: 100, headerAlign: "center", header: '账面价值(元)', align: "center"},
		{field: "yield_8", width: 100, headerAlign: "center", header: '到期收益率', align: "center"},
	]
	var columns2=[
		{field: "invtype", width: 100, headerAlign: "center", header: '账户类型', align: "center"},
		{field: "acctngtype", width: 100, headerAlign: "center", header: '债券类型', align: "center"},
		{field: "secid", width: 100, headerAlign: "center", header: '债券代码', align: "center"},
		{field: "descr", width: 100, headerAlign: "center", header: '债券名称', align: "center"},
		{field: "vadate", width: 100, headerAlign: "center", header: '起息日', align: "center"},
		{field: "tenor", width: 100, headerAlign: "center", header: '期限', align: "center"},
		{field: "mdate", width: 100, headerAlign: "center", header: '到期日', align: "center"},
		{field: "paramt", width: 100, headerAlign: "center", header: '债券面值', align: "center"},
		{field: "couprate_8", width: 100, headerAlign: "center", header: '票面利率', align: "center"},
		{field: "redempamt", width: 100, headerAlign: "center", header: '账面价值(元)', align: "center"},
		{field: "yield_8", width: 100, headerAlign: "center", header: '到期收益率', align: "center"},
	]
	grid.on("beforeload", function(e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex;
		var pageSize = e.data.pageSize;
		search(pageSize, pageIndex);
	});
	grid2.on("beforeload", function(e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex;
		var pageSize = e.data.pageSize;
		search(pageSize, pageIndex);
	});

	//下拉选择需要展示的表格
	function showDataGrid() {
		var type = mini.get("type").getValue();
		if("1"==type){
			grid2.setVisible(false);
			grid.set({
				columns: columns1
			});
			search(10, 0);
		}else{
			grid2.setVisible(true);
			grid.set({
				columns: columns2
			});
			grid2.set({
				columns: columns2
			});
			search(10, 0);
			search2(10, 0);
		}

	}
	// 查询
	function search(pageSize,pageIndex){
		debugger;
		var data=form.getData(true);
		data['pageNum']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		data['implClass']=implClass;
		if("1"==mini.get("type").getValue()){
			data['type']="1";
		}else{
			data['type']="2";
		}

		var params = mini.encode(data);
		CommonUtil.ajax({
			url:"/ExportDataController/getExportPageData",
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	function search2(pageSize,pageIndex){
		debugger;
		var data=form.getData(true);
		data['pageNum']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		data['implClass']=implClass;
		data['type']="3";
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:"/ExportDataController/getExportPageData",
			data:params,
			callback : function(data) {
				grid2.setTotalCount(data.obj.total);
				grid2.setPageIndex(pageIndex);
				grid2.setPageSize(pageSize);
				grid2.setData(data.obj.rows);
			}
		});
	}
		
	function query() {
		if("1"==mini.get("type").getValue()){
			search(10, 0);
		}else {
			search(10, 0);
			search2(10, 0);
		}
    }
	
	function clear(){
        var form=new mini.Form("search_form");
        form.clear();
       //自贸区改造加br
        mini.get("br").setValue(opicsBr);
        search(10,0);
	}
	
	$(document).ready(function() {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
			showDataGrid();
			// search(10, 0);
		});
	});
	
//	//导出
	function exportExcel(){
		var type=mini.get("type").getValue();
		var typeText=mini.get("type").getText();
		var data = form.getData(true);
		var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
		fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
		fields += "<input type='hidden' id='type' name='type' value='"+type+"'>";
		fields += "<input type='hidden' id='sheetTypeValue' name='sheetTypeValue' value='"+typeText+"'>";
		if("1"!=type){
			fields += "<input type='hidden' id='sheetType' name='sheetType' value='BH'>";
		}
		exportHrbExcel(fields);
	}
	
	

</script>
</body>
</html>