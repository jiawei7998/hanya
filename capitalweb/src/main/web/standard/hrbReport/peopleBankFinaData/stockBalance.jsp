<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>特定目的载体投资发生额信息</title>
</head>
<body>
<fieldset class="mini-fieldset title">
    <div id="search_form" style="width:100%">
        <legend>特定目的载体投资发生额信息查询</legend>
        <input id="subdate" name="subdate" class="mini-datepicker" labelField="true"  label="认购日期：" width="320px" labelStyle="text-align:right;" labelStyle="width:150px" emptyText="请输入认购日期" />
        <input id="type" name="type" class="mini-textbox"  width="320px"  labelField="true"  label="资管产品统计编码："  labelStyle="text-align:right;" labelStyle="width:150px" emptyText="请输入资管产品统计编码" />
        <span>
            <a class="mini-button" style="display: none"  id="search_btn" onclick="query()">查询</a>
            <a class="mini-button" style="display: none"  id="clear_btn" onclick="clear()">清空</a>
            <a id="export_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
        </span>
    </div>
</fieldset>
<div class="mini-fit" style="width:100%;height:100%;">
    <div id="data_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
         sortMode="client" allowAlternating="true"
         border="true" allowResize="true">
        <div property="columns">
            <div field="sic" width="150px" allowSort="false" headerAlign="center" align="center">金融机构类型代码</div>
            <div field="br" width="150px" allowSort="false" headerAlign="center" align="center">内部机构号</div>
            <div field="type" width="150px" allowSort="false" headerAlign="center" align="center" >特定目的载体类型</div>
            <div field="statisticalCode" width="150px" allowSort="false" headerAlign="center" align="center">资管产品统计编码</div>
            <div field="code" width="150px" allowSort="false" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer"
                 data-options="{dict:'stockType'}">特定目的载体代码</div>
            <div field="issuerCode" width="150px" allowSort="false" headerAlign="center" align="center">发行人代码</div>
            <div field="location" width="150px" allowSort="false" headerAlign="center" align="center">发行人地区代码</div>
            <div field="runMode" width="150px" allowSort="false" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer"
                 data-options="{dict:'runMode'}" >运行方式</div>
            <div field="subdate" width="150px" allowSort="false" headerAlign="center" align="center" renderer="onDateRenderer">认购日期</div>
            <div field="mdate" width="150px" allowSort="false" headerAlign="center" align="center" renderer="onDateRenderer">到期日期</div>
            <div field="ccy" width="150px" allowSort="false" headerAlign="center" align="center">币种</div>
            <div field="amt" width="150px" allowSort="false" headerAlign="center" align="center">交易金额</div>
            <div field="cnyAmt" width="150px" allowSort="false" headerAlign="center" align="center">交易金额折人民币</div>
            <div field="ps" width="150px" allowSort="false" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer"
                 data-options="{dict:'stockPS'}">交易方向</div>
        </div>
    </div>
</div>
<script>

    mini.parse();

    var grid = mini.get("data_grid");
    var form = new mini.Form("#search_form");

    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize,pageIndex);
    });

    $(document).ready(function() {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
            query();
        });
    });

    //查询按钮
    function query(){
        search(10,0);
    }

    function search(pageSize, pageIndex) {
        var searchUrl="//";

        var data = form.getData(true);
        data['pageNum'] = pageIndex + 1;
        data['pageSize'] = pageSize;

        var params = mini.encode(data);

        CommonUtil.ajax({
            url:searchUrl,
            data:params,
            callback:function(data){
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }

    //清空按钮
    function clear() {
        form.clear();
        query();
    }

    function exportExcel(){
        var content = grid.getData();
        if(content.length == 0){
            mini.alert("请先查询数据");
            return;
        }

        mini.confirm("您确认要导出Excel吗?","系统提示",
            function (action) {
                if (action == "ok"){
                    var data = form.getData(true);
                    var fields = null;

                    var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
                    fields += "<input type='hidden' id='excelName' name='excelName' value='.xls'>";
                    fields += "<input type='hidden' id='id' name='id' value=''>";
                    var urls = CommonUtil.pPath + "/sl/HrbReportManageController/exportExcel";
                    $('<form action="'+ urls +'" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();
                }
            }
        );
    }

</script>
</body>
</html>
