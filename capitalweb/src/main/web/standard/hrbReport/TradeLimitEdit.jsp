<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
<head>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>

<body style="width: 100%; height: 100%; background: white">
<h1 style="text-align:center;padding-top:5px;font-size:18px;"><strong>交易限额修改</strong></h1>
<div id="field_form" class="mini-fit area" style="background:white;">
    <div class="mini-panel" title="交易限额" style="width:100%;" allowResize="true" collapseOnTitleClick="true">
        <div class="leftarea">
            <input style="width:80%;" id="acctngtype" name="acctngtype" class="mini-textbox"
                   labelField="true"  label="限额类型：" labelStyle="text-align:left;width:180px;"/>
            <input style="width:80%;" id="lostlmt" name="lostlmt" class="mini-spinner"format="n2"
                   labelField="true"  label="交易账户浮亏止损限额(%)：" labelStyle="text-align:left;width:180px;" />
            <input style="width:80%;" id="inlevel" name="inlevel" class="mini-textbox"
                   labelField="true"  label="交易账户发行人主体外部评级：" labelStyle="text-align:left;width:180px;"/>
            <input style="width:80%;" id="ratelmtb" name="ratelmtb" class="mini-spinner"format="n2"
                   labelField="true"  label="银行账户单支券占比限额(%)：" labelStyle="text-align:left;width:180px;"/>
            <input style="width:80%;" id="outlevelb" name="outlevelb" class="mini-textbox"
                   labelField="true"  label="银行账户债券债项外部评级：" labelStyle="text-align:left;width:180px;"/>
            <input style="width:80%;" id="duration" name="duration" class="mini-spinner"format="n2"
                   labelField="true"  label="久期限额：" labelStyle="text-align:left;width:180px;"/>
        </div>
        <div class="rightarea">
            <input style="width:80%;" id="deallmt" name="deallmt" class="mini-spinner"format="n2" maxValue="999999999999999999.99"
                   labelField="true"  label="交易账户交易限额：" labelStyle="text-align:left;width:180px;"/>
            <input style="width:80%;" id="ratelmt" name="ratelmt" class="mini-spinner"format="n2"
                   labelField="true"  label="交易账户单支券占比限额(%)：" labelStyle="text-align:left;width:180px;"/>
            <input style="width:80%;" id="outlevel" name="outlevel" class="mini-textbox"
                   labelField="true"  label="交易账户债券债项外部评级：" labelStyle="text-align:left;width:180px;"/>
            <input style="width:80%;" id="inlevelb" name="inlevelb" class="mini-textbox"
                   labelField="true"  label="银行账户发行人主体外部评级：" labelStyle="text-align:left;width:180px;"/>
            <input style="width:80%;" id="pvbp" name="pvbp" class="mini-spinner"format="n4"
                   labelField="true"  label="估价基点价值限额(%)：" labelStyle="text-align:left;width:180px;"/>
        </div>
    </div>
</div>


<span style="margin:5px;display: block;">
        <a id="save_btn" class="mini-button" onclick="save()">保存</a>
    </span>
<script>
    mini.parse();
    var visibleBtn = CommonUtil.hideBtn();
    //获取当前tab
    var currTab = top["win"].tabs.getActiveTab();
    var param=currTab.params;
    var row=param?param.selectData[0]:{};
    var form = new mini.Form("field_form");
    $(document).ready(function() {
        if(row){
            form.setData(row);
        }
    });

    //保存
    function save(){
        var data = form.getData(true);
        data.id=row.id;
        console.log(data);
        CommonUtil.ajax({
            url:"/IfsTradeLimitController/addData",
            data:data,
            callback : function(data) {
                mini.alert("保存成功!");
                top["win"].closeMenuTab();
                // query();
            }
        });

    }

</script>
</body>
</html>