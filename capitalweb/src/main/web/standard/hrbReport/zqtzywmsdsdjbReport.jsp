<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../global.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>债券投资业务免所得税登记表</title>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
</head>
<body style="width: 100%; height: 100%; background: white">
<fieldset class="mini-fieldset">
    <div id="search_form" style="width: 50%;">
        <legend>查询条件</legend>
        <input id="queryDate" name="queryDate" class="mini-datepicker mini-mustFill" required="true" labelField="true"
               label="日期：" required="true" labelStyle="text-align:right;" emptyText="起始日期" format="yyyy-MM-dd"
               value="<%=__bizDate%>"/>
        <span style="float:right;">
			<a id="search_btn" class="mini-button" style="display: none" onclick="create()">查询</a>
			<a id="export_btn" class="mini-button" style="display: none" onclick="exportExcel()">导出报表</a>
		</span>
    </div>
</fieldset>

<div class="mini-fit" style="margin-top: 2px;margin-bottom: 2px;">
    <div id="datagrid" class="mini-datagrid borderAll" style="width: 100%; height: 100%;" allowAlternating="true" allowHeaderWrap="true" allowCellWrap ="true"
         multiSelect="true" allowResize="true" border="true" sortMode="client" multiSelect="true">
        <div property="columns">
            <div type="indexcolumn" headerAlign="center" width="40">序号</div>
            <div field="jjxh" name="jjxh" width="100" align="center" headerAlign="center" allowSort="false">交易序号</div>
            <div field="zhlx" name="zhlx" width="100" align="center" headerAlign="center" allowSort="false">账户类型</div>
            <div field="zqlx" name="zqlx" width="100" align="center" headerAlign="center" allowSort="false">债券类型</div>
            <div field="zqdm" name="zqdm" width="100" align="center" headerAlign="center" allowSort="false">债券代码</div>
            <div field="zqmc" name="zqmc" width="100" align="center" headerAlign="center" allowSort="false">债券名称</div>
            <div field="qx" name="qx" width="100" align="center" headerAlign="center" allowSort="false">期限</div>
            <div field="zqqxr" name="zqqxr" width="100" align="center" headerAlign="center" allowSort="false">债券起息日</div>
            <div field="zqdqr" name="zqdqr" width="100" align="center" headerAlign="center" allowSort="false">债券到期日</div>
            <div header="票面利率" headerAlign="center">
                <div property="columns">
                    <div field="pmlvGdlv" name="pmlvGdlv" width="100" align="right" headerAlign="center" allowSort="false" numberFormat="n4">固定利率</div>
                    <div field="pmlvFdlv" name="pmlvFdlv" width="100" align="right" headerAlign="center" allowSort="false" numberFormat="n4">浮动利率</div>
                </div>
            </div>
            <div field="fxpl" name="fxpl" width="100" align="center" headerAlign="center" allowSort="false">付息频率</div>
            <div field="fxrq" name="fxrq" width="100" align="center" headerAlign="center" allowSort="false">付息日期</div>
            <div field="mrrq" name="mrrq" width="100" align="center" headerAlign="center" allowSort="false">买入日期</div>
            <div field="mrpmje" name="mrpmje" width="100" align="right" headerAlign="center" allowSort="false" numberFormat="n2">买入票面金额</div>
            <div field="mrqj" name="mrqj" width="100" align="right" headerAlign="center" allowSort="false" numberFormat="n2">买入全价</div>
            <div field="mrjj" name="mrjj" width="100" align="right" headerAlign="center" allowSort="false" numberFormat="n2">买入净价</div>
            <div field="mryjlx" name="mryjlx" width="100" align="right" headerAlign="center" allowSort="false" numberFormat="n2">买入应计利息</div>
            <div field="mcrq" name="mcrq" width="100" align="center" headerAlign="center" allowSort="false">卖出日期</div>
            <div field="mcpmje" name="mcpmje" width="100" align="right" headerAlign="center" allowSort="false" numberFormat="n2">卖出票面金额</div>
            <div field="mcqj" name="mcqj" width="100" align="right" headerAlign="center" allowSort="false" numberFormat="n2">卖出全价</div>
            <div field="mcjj" name="mcjj" width="100" align="right" headerAlign="center" allowSort="false" numberFormat="n2">卖出净价</div>
            <div field="mcyjlx" name="mcyjlx" width="100" align="right" headerAlign="center" allowSort="false" numberFormat="n2">卖出应计利息</div>
            <div field="sjsxrq" name="sjsxrq" width="100" align="center" headerAlign="center" allowSort="false">实际收息日期</div>
            <div field="sjsxje" name="sjsxje" width="100" align="right" headerAlign="center" allowSort="false" numberFormat="n2">实际收息金额</div>
            <div field="dnsslx" name="dnsslx" width="100" align="right" headerAlign="center" allowSort="false" numberFormat="n2">当年实收利息</div>
            <div field="msdsgzlxsr" name="msdsgzlxsr" width="150" align="right" headerAlign="center" allowSort="false" numberFormat="n2">免所得税国债利息收入</div>
            <div field="sjmlxsr" name="sjmlxsr" width="100" align="right" headerAlign="center" allowSort="false" numberFormat="n2">实际免利息收入</div>
        </div>
    </div>
</div>

<script>
    mini.parse();
    var url = window.location.search;
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var id = params.id;
    var grid = mini.get("datagrid");
    var userId = '<%=__sessionUser.getUserId()%>';
    var form = new mini.Form("#search_form");

    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize, pageIndex);
    });


    // 查询
    function search(pageSize, pageIndex) {
        var form = new mini.Form("#search_form");
        form.validate();
        if (form.isValid() == false) {
            mini.alert("信息填写有误，请重新填写", "系统也提示");
            return;
        }
        var data = form.getData(true);
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        data['branchId'] = branchId;
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: "/IfsReportZqtzywmsdsdjbReportController/searchIfsReportZqtzywmsdsdjbReportPage",
            data: params,
            callback: function (data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }

    function query() {
        search(grid.pageSize, 0);
    }

    function clear() {
        var form = new mini.Form("search_form");
        form.clear();
        search(10, 0);
    }

    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
            search(100, 0);
        });
    });


    //交易日期
    function onDrawDateStart(e) {
        var startDate = e.date;
        var endDate = mini.get("edate").getValue();
        if (CommonUtil.isNull(endDate)) {
            return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }

    function onDrawDateEnd(e) {
        var endDate = e.date;
        var startDate = mini.get("sdate").getValue();
        if (CommonUtil.isNull(startDate)) {
            return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }

    //生成报表
    function create() {
        form.validate();
        if (form.isValid() == false) {
            mini.alert("信息填写有误，请重新填写", "系统提示");
            return;
        }
        var data = form.getData(true);
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: "/IfsReportZqtzywmsdsdjbReportController/createIfsReportZqtzywmsdsdjb",
            data: params,
            complete: function (data) {
                var text = data.responseText;
                var desc = JSON.parse(text);
                var content = desc.obj.retMsg;
                mini.alert(content);
                search(100, 0);
            }
        });
    }

    //导出
    function exportExcel(){
        var data = form.getData(true);
        var fields = null;
        var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
        fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
        exportHrbExcel(fields);
    }
</script>
</body>
</html>