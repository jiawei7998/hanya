<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../global.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
    <title>2103跨境同业融资业务信息</title>
</head>
<body>
<fieldset class="mini-fieldset title">
    <div id="search_form" style="width:100%">
        <legend>报表查询</legend>
        <input id="queryDate" name="queryDate" class="mini-datepicker" labelField="true" label="交易日期:"
               labelSyle="text-align:right;" emptyText="请输入" format="yyyy-MM-dd" value="<%=__bizDate%>"></input>
        <a class="mini-button" style="display: none" id="search_btn" onclick="search()">查询</a>
        <a class="mini-button" id="save_btn" style="display: none" onclick="saveData()">保存</a>
        <a id="export_btn" class="mini-button" style="display: none" onclick="exportExcel()">导出报表</a>
        </span>
    </div>
</fieldset>
<span style="float:right;">单位:万元</span>
<div class="mini-fit" style="width:100%;height:100%;">
    <div id="fund_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
         sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
         border="true" allowResize="true"
         idField="id"
         allowResize="true"
         allowCellEdit="true" allowCellSelect="true" multiSelect="true"
         editNextOnEnterKey="true" editNextRowCell="true"
    >

        <div property="columns" autoEscape="true">
            <div field="sbhm" headerAlign="center" align="left" width="100px">申报号码</div>
            <div field="czlx" headerAlign="center" align="left" width="100px">操作类型
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="bgyy" headerAlign="center" align="left" width="100px">变更/撤销原因
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="bsjgdm" headerAlign="center" align="left" width="100px">报送机构代码
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="zmqyw" headerAlign="center" align="left" width="100px">是否自贸区业务
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="fzhsdy" headerAlign="center" align="left" width="100px">是否分账核算单元
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="ywsx" headerAlign="center" align="left" width="100px">业务属性
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="ywzl" headerAlign="center" align="left" width="100px">业务种类
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="rcjgdm" headerAlign="center" align="left" width="100px">融出机构代码
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="rcjgmc" headerAlign="center" align="left" width="100px">融出机构名称
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="skrq" headerAlign="center" align="left" width="100px">融出机构类型
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="rcjglx" headerAlign="center" align="left" width="100px">融出机构所在国家（地区）代码
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="rcjgszgj" headerAlign="center" align="left" width="100px">融入机构代码
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="rrjgdm" headerAlign="center" align="left" width="100px">融入机构名称
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="rrjgmc" headerAlign="center" align="left" width="100px">融入机构类型
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="rrjglx" headerAlign="center" align="left" width="100px">融入机构所在国家（地区）代码
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="rrjgszhj" headerAlign="center" align="left" width="100px">融资类型
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="rzlx" headerAlign="center" align="left" width="100px">融资币种
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="rzjb" headerAlign="center" align="left" width="100px">融资金额
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="sflx" headerAlign="center" align="left" width="100px">是否联行及附属机构往来
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="jye" headerAlign="center" align="left" width="100px">交易日
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="qxr" headerAlign="center" align="left" width="100px">起息日
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="dqr" headerAlign="center" align="left" width="100px">到期日
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="ll" headerAlign="center" align="left" width="100px">利率
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="lllx" headerAlign="center" align="left" width="100px">利率类型
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="llqxdw" headerAlign="center" align="left" width="100px">利率期限单位
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="llcxdjqx" headerAlign="center" align="left" width="100px">利率重新定价期限
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="fdlljz" headerAlign="center" align="left" width="100px">浮动利率基准
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="fdlljd" headerAlign="center" align="left" width="100px">浮动利率加点
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="rzzt" headerAlign="center" align="left" width="100px">融资（贷款）状态
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="yqwhk" headerAlign="center" align="left" width="100px">逾期未还款金额
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="yhywbh" headerAlign="center" align="left" width="100px">银行业务编号
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
            <div field="jyfy" headerAlign="center" align="left" width="100px">交易附言
                <input property="editor" class="mini-textbox" style="width:100%;" minWidth="50px"/>
            </div>
        </div>
    </div>
</div>
<script>
    mini.parse();
    var url = window.location.search;
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var id = params.id;
    var implClass = params.implClass;
    var grid = mini.get("fund_grid");
    var form = new mini.Form("#search_form");

    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize, pageIndex);
    });

    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function (visibleBtn) {
            search(10, 0);
        });
    });

    //查询按钮
    function search(pageSize, pageIndex) {
        var searchUrl = "/KjtyrzywController/callKjtyrzyw";
        var data = form.getData(true);
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: searchUrl,
            data: params,
            callback: function (data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }


    //保存
    function saveData() {
        saveUrl = "/KjtyrzywController/upDateKjtyrzyw";
        var data = grid.getChanges();
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: saveUrl,
            data: params,
            callback: function (data) {
                mini.alert("保存成功", '提示信息', function () {
                    search(50, 0);
                });
            }
        });
    }

    function exportExcel() {
        var data = form.getData(true);
        var fields = null;
        var fields = "<input type='hidden' id='queryDate' name='queryDate' value='" + data.queryDate + "'>";
        fields += "<input type='hidden' id='id' name='id' value='" + id + "'>";
        exportHrbExcel(fields);
    }


</script>
</body>
</html>
