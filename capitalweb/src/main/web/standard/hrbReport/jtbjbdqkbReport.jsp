<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../global.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>计提本金变动情况报表</title>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
</head>
<body style="width: 100%; height: 100%; background: white">
<fieldset class="mini-fieldset">
    <div id="search_form" style="width: 50%;">
        <legend>查询条件</legend>
        <input id="queryDate" name="queryDate" class="mini-datepicker mini-mustFill" required="true" labelField="true"
               label="日期：" required="true" labelStyle="text-align:right;" emptyText="起始日期" format="yyyy-MM-dd"
               value="<%=__bizDate%>"/>
        <span style="float:right;">
			<a id="search_btn" class="mini-button" style="display: none" onclick="create()">查询</a>
			<a id="export_btn" class="mini-button" style="display: none" onclick="exportExcel()">导出报表</a>
		</span>
    </div>
</fieldset>

<div class="mini-fit" style="margin-top: 2px;margin-bottom: 2px;">
    <div id="datagrid" class="mini-datagrid borderAll" style="width: 100%; height: 100%;" allowAlternating="true" allowHeaderWrap="true" allowCellWrap ="true"
         multiSelect="true" allowResize="true" border="true" sortMode="client" multiSelect="true">
        <div property="columns">
            <div type="indexcolumn" headerAlign="center" width="40">序号</div>
            <div field="product" name="product" width="100" align="center" headerAlign="center" allowSort="false">项目</div>
            <div field="date1" name="date1" width="100" align="right" headerAlign="center" allowSort="false" numberFormat="n2">日期1</div>
            <div field="date2" name="date2" width="100" align="right" headerAlign="center" allowSort="false" numberFormat="n2">日期2</div>
            <div field="date3" name="date3" width="100" align="right" headerAlign="center" allowSort="false" numberFormat="n2">日期3</div>
            <div field="date4" name="date4" width="100" align="right" headerAlign="center" allowSort="false" numberFormat="n2">日期4</div>
            <div field="date5" name="date5" width="100" align="right" headerAlign="center" allowSort="false" numberFormat="n2">日期5</div>
            <div field="date6" name="date6" width="100" align="right" headerAlign="center" allowSort="false" numberFormat="n2">日期6</div>
            <div field="date7" name="date7" width="100" align="right" headerAlign="center" allowSort="false" numberFormat="n2">日期7</div>
            <div field="date8" name="date8" width="100" align="right" headerAlign="center" allowSort="false" numberFormat="n2">日期8</div>
            <div field="date9" name="date9" width="100" align="right" headerAlign="center" allowSort="false" numberFormat="n2">日期9</div>
            <div field="date10" name="date10" width="100" align="right" headerAlign="center" allowSort="false" numberFormat="n2">日期10</div>
            <div field="date11" name="date11" width="100" align="right" headerAlign="center" allowSort="false" numberFormat="n2">日期11</div>
            <div field="date12" name="date12" width="100" align="right" headerAlign="center" allowSort="false" numberFormat="n2">日期12</div>
            <div field="date13" name="date13" width="100" align="right" headerAlign="center" allowSort="false" numberFormat="n2">日期13</div>
            <div field="date14" name="date14" width="100" align="right" headerAlign="center" allowSort="false" numberFormat="n2">日期14</div>
            <div field="date15" name="date15" width="100" align="right" headerAlign="center" allowSort="false" numberFormat="n2">日期15</div>
            <div field="date16" name="date16" width="100" align="right" headerAlign="center" allowSort="false" numberFormat="n2">日期16</div>
            <div field="date17" name="date17" width="100" align="right" headerAlign="center" allowSort="false" numberFormat="n2">日期17</div>
            <div field="date18" name="date18" width="100" align="right" headerAlign="center" allowSort="false" numberFormat="n2">日期18</div>
            <div field="date19" name="date19" width="100" align="right" headerAlign="center" allowSort="false" numberFormat="n2">日期19</div>
            <div field="date20" name="date20" width="100" align="right" headerAlign="center" allowSort="false" numberFormat="n2">日期20</div>
            <div field="date21" name="date21" width="100" align="right" headerAlign="center" allowSort="false" numberFormat="n2">日期21</div>
            <div field="date22" name="date22" width="100" align="right" headerAlign="center" allowSort="false" numberFormat="n2">日期22</div>
            <div field="date23" name="date23" width="100" align="right" headerAlign="center" allowSort="false" numberFormat="n2">日期23</div>
            <div field="date24" name="date24" width="100" align="right" headerAlign="center" allowSort="false" numberFormat="n2">日期24</div>
            <div field="date25" name="date25" width="100" align="right" headerAlign="center" allowSort="false" numberFormat="n2">日期25</div>
            <div field="date26" name="date26" width="100" align="right" headerAlign="center" allowSort="false" numberFormat="n2">日期26</div>
            <div field="date27" name="date27" width="100" align="right" headerAlign="center" allowSort="false" numberFormat="n2">日期27</div>
            <div field="date28" name="date28" width="100" align="right" headerAlign="center" allowSort="false" numberFormat="n2">日期28</div>
            <div field="date29" name="date29" width="100" align="right" headerAlign="center" allowSort="false" numberFormat="n2">日期29</div>
            <div field="date30" name="date30" width="100" align="right" headerAlign="center" allowSort="false" numberFormat="n2">日期30</div>
            <div field="date31" name="date31" width="100" align="right" headerAlign="center" allowSort="false" numberFormat="n2">日期31</div>
            <div field="datehj" name="datehj" width="100" align="right" headerAlign="center" allowSort="false" numberFormat="n2">合计</div>
            <div field="datepjz" name="datepjz" width="100" align="right" headerAlign="center" allowSort="false" numberFormat="n2">平均值</div>
        </div>
    </div>
</div>

<script>
    mini.parse();
    var url = window.location.search;
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var id = params.id;
    var grid = mini.get("datagrid");
    var userId = '<%=__sessionUser.getUserId()%>';
    var form = new mini.Form("#search_form");

    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize, pageIndex);
    });


    // 查询
    function search(pageSize, pageIndex) {
        var form = new mini.Form("#search_form");
        form.validate();
        if (form.isValid() == false) {
            mini.alert("信息填写有误，请重新填写", "系统也提示");
            return;
        }
        var data = form.getData(true);
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        data['branchId'] = branchId;
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: "/IfsReportJtbjbdqkbReportController/searchIfsReportJtbjbdqkbReportPage",
            data: params,
            callback: function (data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }

    function query() {
        search(grid.pageSize, 0);
    }

    function clear() {
        var form = new mini.Form("search_form");
        form.clear();
        search(10, 0);
    }

    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
            search(100, 0);
        });
    });


    //交易日期
    function onDrawDateStart(e) {
        var startDate = e.date;
        var endDate = mini.get("edate").getValue();
        if (CommonUtil.isNull(endDate)) {
            return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }

    function onDrawDateEnd(e) {
        var endDate = e.date;
        var startDate = mini.get("sdate").getValue();
        if (CommonUtil.isNull(startDate)) {
            return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }

    //生成报表
    function create() {
        form.validate();
        if (form.isValid() == false) {
            mini.alert("信息填写有误，请重新填写", "系统提示");
            return;
        }
        var data = form.getData(true);
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: "/IfsReportJtbjbdqkbReportController/createIfsReportJtbjbdqkb",
            data: params,
            complete: function (data) {
                var text = data.responseText;
                var desc = JSON.parse(text);
                var content = desc.obj.retMsg;
                mini.alert(content);
                search(100, 0);
            }
        });
    }

    //导出
    function exportExcel(){
        var data = form.getData(true);
        var fields = null;
        var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
        fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
        exportHrbExcel(fields);
    }
</script>
</body>
</html>