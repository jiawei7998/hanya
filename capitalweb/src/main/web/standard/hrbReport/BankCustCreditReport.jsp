<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
<head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
	<script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>

</head>
<body style="width: 100%; height: 100%; background: white">
<fieldset class="mini-fieldset">
	<legend>同业授信</legend>
	<div id="search_form" style="width: 100%;margin:5px 0 5px 0">
		<input id="queryDate" name="queryDate" class="mini-datepicker" labelField="true" label="账务日期：" emptyText="账务日期" format="yyyy-MM-dd" value="<%=__bizDate%>"/>
		<span style="float: right; margin-left: 20px;margin-top:6px">
			        <a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
				    <a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
					<a id="export_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
			</span>
	</div>
</fieldset>

<%--	<div class="mini-fit" style="margin-top: 2px;">--%>
<%--	<span style="float:right;">单位:万元</span>--%>
<div id="grid1" class="mini-datagrid borderAll" style="width: 100%; height: 100%;" allowAlternating="true"
	 allowResize="true" border="true" sortMode="client" multiSelect="true">
	<div property="columns">
		<div field="custCreditId" headerAlign="center" align="center" width="0px">客户额度唯一编号</div>
		<div field="ecifNum" width="120px" align="center"  headerAlign="center" >客户编号</div>
		<div field="customer" width="120px" align="center" headerAlign="center" >客户名称</div>
		<div field="currency" width="120px" align="center" headerAlign="center"  renderer="CommonUtil.dictRenderer" data-options="{dict:'Currency'}">币种</div>
		<div field="creditType" width="120px" align="center" headerAlign="center"  renderer="onTypeRenderer" >额度种类</div>
		<div field="loanAmt" width="120px" align="right" headerAlign="center" allowSort="true" numberFormat="n2">授信额度（元 ）</div>
		<div field="avlAmt" width="120px" align="right"  headerAlign="center" allowSort="true" numberFormat="n2">剩余可用金额（元 ）</div>
		<div field="frozenAmt" width="120px" align="right"  headerAlign="center" allowSort="true" numberFormat="n2">已使用额度（元 ）</div>
		<div field="dueDate" width="120px" align="center"  headerAlign="center" >起始日</div>
		<div field="dueDateLast" width="120px" align="center"  headerAlign="center" >到期日</div>
	</div>
</div>
</div>

<script>
	mini.parse();
	var url = window.location.search;
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var id = params.id;
	var implClass = params.implClass;
	var jsonData = [];

	var grid = mini.get("grid1");
	grid.on("beforeload", function(e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex;
		var pageSize = e.data.pageSize;
		search(pageSize, pageIndex);
	});
	$(document).ready(function() {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
			queryCreditType();
			query();
		});
	});
	// 初始化数据
	function query(){
		search(grid.pageSize, 0);
	}
	function search(pageSize,pageIndex){
		var form = new mini.Form("#search_form");
		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		data.implClass=implClass;
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:"/hrbCreditCustController/getHrbCreditQueryPage",
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
				grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}

	function clear(){
		var form=new mini.Form("search_form");
		form.clear();
		// mini.get("startDate").setValue(sysDate);
		// mini.get("endDate").setValue(sysDate);
		query();
	}

	//查询额度种类配置
	function queryCreditType(){
		CommonUtil.ajax({
			url:"/HrbCreditTypeController/queryAllCrediyType",
			data:{},
			callback:function(data){
				var length = data.length;
				for(var i=0;i<length;i++){
					var creType = data[i].creType;
					var creTypeName = data[i].creTypeName;
					jsonData.add({'creType':creType,'creTypeName':creTypeName});
				}
			}
		});
	}

	function onTypeRenderer(e) {
		var length = jsonData.length;
		for(var i=0;i<length;i++){
			if(e.value == jsonData[i].creType){
				return jsonData[i].creTypeName;
			}
		}
	}

	//日期
	function onDrawDateStart(e) {
		var startDate = e.date;
		var endDate= mini.get("endDate").getValue();
		if(CommonUtil.isNull(endDate)){
			return;
		}
		if (endDate.getTime() < startDate.getTime()) {
			e.allowSelect = false;
		}
	}

	function onDrawDateEnd(e) {
		var endDate = e.date;
		var startDate = mini.get("startDate").getValue();
		if(CommonUtil.isNull(startDate)){
			return;
		}
		if (endDate.getTime() < startDate.getTime()) {
			e.allowSelect = false;
		}
	}
	//导出
	function exportExcel(){
		var form=new mini.Form("search_form");
		var data = form.getData(true);
		var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
		fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
		exportHrbExcel(fields);
	}
</script>
</body>
</html>