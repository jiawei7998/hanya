<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
    <title>交易所回购</title>
</head>
<body>
    <fieldset class="mini-fieldset title">
        <div id="search_form" style="width:100%">
            <legend>报表查询</legend>
            <input id="queryDate" name="queryDate" class="mini-datepicker" labelField="true" label="交易日期:" 
                    value="<%=__bizDate%>" labelSyle="text-align:right;" emptyText="请输入"></input>
            <span>
                <a class="mini-button" style="display: none"  id="search_btn" onclick="query()">查询</a>
                <a class="mini-button" style="display: none"  id="clear_btn" onclick="clear()">清空</a>
                <a id="export_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
            </span>
        </div>
    </fieldset>
    <div class="mini-fit" style="width:100%;height:100%;">
        <div id="repo_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
            sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
            border="true" allowResize="true">
            <div property="columns">
                <div type="indexcolumn" headerAlign="center" align="center" width="30px">序号</div>
                <div field="secName" headerAlign="center" align="center" width="50px">证券名称</div>
                <div field="vdate" headerAlign="center" align="center" width="50px">首期结算日</div>
                <div field="tenor" headerAlign="center" align="center" width="50px">期限</div>
                <div field="matdate" headerAlign="center" align="center" width="50px">到期结算日</div>
                <div field="comProcdAmt" headerAlign="center" align="center" width="50px">首期结算额</div>
                <div field="rate" headerAlign="center" align="center" width="50px">利率</div>
                <div field="matProcdAmt" headerAlign="center" align="center" width="50px">到期结算额</div>
                <div field="cname" headerAlign="center" align="center" width="50px">交易对手</div>
            </div>
        </div>
    </div>
    <script>
        
        mini.parse();

        var grid = mini.get("repo_grid");
        var form = new mini.Form("#search_form");
        var currTab = top["win"].tabs.getActiveTab();
        var params = currTab.params;
        var id = params.id;
        var implClass = params.implClass;

        grid.on("beforeload", function (e) {
            e.cancel = true;
            var pageIndex = e.data.pageIndex; 
            var pageSize = e.data.pageSize;
            search(pageSize,pageIndex);
        });
        
        $(document).ready(function() {
            //控制按钮显示
            $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
                query();
            });
        });
    
        //查询按钮
        function query(){
            search(10,0);
        }
        
        function search(pageSize, pageIndex) {
            var searchUrl="/ExportDataController/getExportPageData";
            var data = form.getData(true);
            data['pageNum'] = pageIndex + 1;
            data['pageSize'] = pageSize;
            data.implClass=implClass;
            var params = mini.encode(data);
     
            CommonUtil.ajax({
                url:searchUrl,
                data:params,
                callback:function(data){
                    grid.setTotalCount(data.obj.total);
                    grid.setPageIndex(pageIndex);
                    grid.setPageSize(pageSize);
                    grid.setData(data.obj.rows);
                }
            });
        }
        
        //清空按钮
        function clear() {
            form.clear();
            query();
        }
        
        function exportExcel(){
            var form=new mini.Form("search_form");
            var data = form.getData(true);
            var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
            fields += "<input type='hidden' id='id' name='id' value='"+id+"'>";
            exportHrbExcel(fields);
        }
    
    </script>
</body>
</html>
