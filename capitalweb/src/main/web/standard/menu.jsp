﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="./global.jsp"%>
<head>
	<link href="<%=basePath%>/miniScript/css/business.css?654646848654" rel="stylesheet" type="text/css" />
	<style type="text/css">
	  body{
            background:#fff;
            padding:0px 10px;   
			box-sizing: border-box;
        }
	  a{font-family:微软雅黑 ;font-size:14px;text-decoration:none;cursor:pointer;color: black}
	</style>
</head>
<body>
	<div id = "menuDiv"></div>
</body>
<script>
	//获取参数传递	
	var url = window.location.search;
	var type = CommonUtil.getParam(url,"type");
	//获取当前活动ID
	var activeTab = top["win"].tabs.getActiveTab();

	//获取当前活动ID
	CommonUtil.ajax({
		url: "/deskMenuController/getDeskMenuPage",
		data: {
				deskId:activeTab.deskId,
				branchId:top["win"].branchId,
				userId:top["win"].userId,
				instId:top["win"].instId,
				roleIds:top["win"].roleIds //modify by shenzl 支持多角色设置
			},
		callback:function(data){
			//处理我的工作台
			loadMenus(data.obj);
		}
	});
	
	function init(param){}

	//生成菜单列表
	function loadMenus(datas){
		var moduleType = null;
		var menus = null;

		var html = ""; 
		var table = "";
		$.each(datas,function(i,item){
			moduleType = item.moduleType;
			menus = item.menus;
			html = html;

			table ="<div class='box1'><div class='box2'><h3><span class='fk'></span>"+moduleType+"</h3><div class='box3 clearfix'>";
			
			$.each(menus,function(i,menu){
				table +="<div class='product clearfix' onclick='openUrl({moduleId : \"" + menu.moduleId+"\", moduleName : \"" + menu.moduleName +"\", moduleUrl : \"" + menu.moduleUrl + "\",obj:this})'>";
				table +="<div class=' product_lf fl '><i class='iconfont "+menu.moduleIcon+"' style='font-size: 42px;'></i><div class='product_rt fl'><a href='#'>"  + menu.moduleName + "</a></div></div></div>";
			});
			table += " </div></div></div>";
			html += table;
		});// end for
		$('#menuDiv').append(html);  
	}// end loadMenus
	
	//打开菜单链接
	function openUrl(param){
		var url = encodeURI(param.moduleUrl);
		var tab = {
			id : param.moduleId,
			name : param.moduleId,
			title : param.moduleName,
			url : url,
			showCloseButton:true
		};

		var paramData ={dealType:type};
		//解析URL中的参数存入paramData
		CommonUtil.getUrlParms(paramData,param.moduleUrl);

		CommonUtil.openNewMenuTab(tab,paramData);
	}

</script>

</html>
