<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%> 

<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<link href="<%=basePath%>/miniScript/css/business.css" rel="stylesheet" type="text/css" />
<style type="text/css">
    body{
            background:#fff;
            padding:0px 10px;   
        }
    a{font-family:微软雅黑 ;font-size:14px;text-decoration:none;cursor:pointer;color: black}
</style>
<body>
    <div id = "formDiv"></div>
</body>
<script type="text/javascript">
    $(document).ready(function(){
        //取项目模板
        CommonUtil.ajax({
            url:"/FormController/searchPageForm",
            data:{"isTemplate":"1","useFlag":"1"},
            callback:function(data){

                var html ="";
                var table ="";
                var rows = data.obj.rows;
                table ="<div class='box1'><div class='box2'><h3 style='margin:5px;color:black;bold:true'><span class='fk'></span>模版操作项</h3><div class='box3 clearfix'>";
                table+="<div class='product clearfix' onclick='templateSelect(&#39;0&#39;,&#39;addWithout&#39;)'>";
                table+="<div class='product_lf fl product_configuration_9'></div><div class='product_rt fl'><a href='#'>创建模版</a></div></div></div>";
                table +="<div class='box2'><h3 style='margin:5px;color:black;bold:true'><span class='fk'></span>已存在表单模版</h3><div class='box3 clearfix'>";
                $.each(rows,function(i,item){
                    table +="<div class='product clearfix' onclick='templateSelect(" + item.formNo + ",&#39;addWith&#39;)'>";
                    table +="<div class='product_lf fl product_configuration_9'></div><div class='product_rt fl'><a href='#'>"  + item.formName + "&#151表单模版</a></div></div>";
                });
                table += "</div></div></div>";
                html += table;
                
                $('#formDiv').append(html); 
            }
        }); 
    });

    function templateSelect(formNo, type){
        var tab ={
            id:'formDiv_tabEdite_add',
            name:'formDiv_tabEdite_add',
            iconCls:null,
            title:'表单新增',
            url: CommonUtil.baseWebPath() +'/mini_base/MiniFormEdit.jsp',
            showCloseButton:true
        };
        var param ={'formNo':formNo,'type':type};
        //打开Tabs
		CommonUtil.openNewMenuTab(tab,param);
	}
</script>