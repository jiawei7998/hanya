<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<link href="<%=basePath%>/miniScript/dragform/core.css" rel="stylesheet" type="text/css" />
<script src="<%=basePath%>/miniScript/dragform/dragbase.js" type="text/javascript"></script>
<script src="<%=basePath%>/miniScript/dragform/fieldcontainer.js" type="text/javascript"></script>

<style>
    body 
    {
        font-size:12px;
        font-family:Sans-Serif;
        background: #FFF;
    }
    
    #filePre #filesetPre
    {

        overflow:auto;
        display:block; 
        padding:10px;
        height: 100%;
        width: 100%;
        
    } 
    #filesetPre
    {
        margin-top:10px;       
    }   
    
    .dragZone
    {
        float:left;
        height: 750px;
        width: 300px;
        text-align:center;
    }
    .dropZone
    {
        display:inline-block;
        height: 100%;
        width: 100%;
        overflow:auto;
        padding:10px;
        text-align:center;
    }
    .asLabel .mini-textbox-border,
    .asLabel .mini-textbox-input,
    .asLabel .mini-buttonedit-border,
    .asLabel .mini-buttonedit-input,
    .asLabel .mini-textboxlist-border
    {
        border:0;background:none;cursor:default;
    }
    .asLabel .mini-buttonedit-button,
    .asLabel .mini-textboxlist-close
    {
        display:none;
    }
    .asLabel .mini-textboxlist-item
    {
        padding-right:8px;
    } 
    </style>

<style>
    .mini-tabs-body-left {
        border-color: #FFF;
    }
    .mini-tabs-body{
        background: #FFF;
    }
</style>

<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<body>
<div class="mini-layout" style="width:100%;height:100%;">
    <div region="center">
        <div class="mini-fit">
                <div id="formEditTabs" class="mini-tabs" onactivechanged="onActiveChanged" style="height:100%; border:0px"  tabPosition="left">
                    <div title="表单基本信息">
                        <table id="form_form"  width="100%" class="mini-table">
                            <tr>
                                    <!-- class="form_table" -->
                                <td class="red-star">表单名称<input id="formNo" name="formNo" class="mini-hidden"/></td>
                                <td><input id="formName" name="formName" class="mini-textbox"  required="true"   vtype="maxLength:100" /></td>
                                <td class="red-star">所属机构</td>
                                <td><input id="formInst" name="formInst" textname="formInstName"  class="mini-buttonedit" required="true"  onbuttonclick="CommonUtil.getInstList" /></td>
                                <td class="red-star">表单类型</td>
                                <td><input id="formType" name="formType" class="mini-combobox"  required="true"   data = "CommonUtil.serverData.dictionary.formType" value="1"  emptyText="---请选择---"/></td>
                            </tr>
                            <tr>
                                <td>表单描述</td>
                                <td><input id="formDesc" name="formDesc" class="mini-textbox"  vtype="maxLength:1024"/></td>
                                <td class="red-star">是否模版</td>
                                <td><input id="isTemplate" name="isTemplate" class="mini-combobox" required="true"  data = "CommonUtil.serverData.dictionary.YesNo" value="0" emptyText="---请选择---" /></td>
                                <td class="red-star">启用标志</td>
                                <td><input id="useFlag" name="useFlag" class="mini-combobox" required="true"  data = "CommonUtil.serverData.dictionary.Status" value="1"  emptyText="---请选择---" /></td>
                            </tr>
                        </table>
                    </div>
                    <div id="PreDefinedFieldConfiguration" title="预定义字段配置">
                        <div class="mini-splitter" allowResize="true" style="width:100%;height:100%;">
                            <div size="40%">
                                <div class="mini-fit">
                                    <div  class="mini-tabs dragZone" width="100%" height="100%" buttons="#tabsButtonsPre"   buttonsAlign="right">
                                        <div title="字段分组">
                                            <div id="filesetPre" class="fileset"></div>
                                        </div>
                                        <div title="字段">
                                            <div id="filePre" class="file"></div>
                                        </div>
                                    </div>
                                    <div id="tabsButtonsPre">
                                        <a id="writeBtnPre" class="mini-button" style="display: none"   onclick="changeMode('Pre')">字段编辑</a>
                                        <a id="addBtnPre" class="mini-button" style="display: none"  >维护</a>
                                        <a id="refreshBtnPre" class="mini-button" style="display: none"   >刷新</a>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="mini-fit">
                                    <div id="dropZonePre" class="dropZone" style="border:2px;float: left;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="CustomizedFieldConfiguration" title="自定义字段配置">
                        <div class="mini-splitter" allowResize="true" style="width:100%;height:100%;">
                            <div size="40%">
                                <div class="mini-fit">
                                    <div class="mini-tabs dragZone" width="100%" height="100%" buttons="#tabsButtonsCust"  buttonsAlign="right">
                                        <div title="字段分组">
                                            <div id="filesetCust" class="fileset"></div>
                                        </div>
                                        <div title="字段">
                                            <div id="fileCust"  class="file"></div>
                                        </div>
                                    </div>
                                    <div id="tabsButtonsCust">
                                        <a id="writeBtnCust" class="mini-button" style="display: none"   onclick="changeMode('Cust')">字段编辑</a>
                                        <a id="addBtnCust" class="mini-button" style="display: none"  >维护</a>
                                        <a id="refreshBtnCust" class="mini-button" style="display: none"   >刷新</a>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="mini-fit"><div id="dropZoneCust" class="dropZone" style="border:2px;float: left;"></div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
            <a  id="save_btn" class="mini-button" style="display: none"   >保存</a>
    </div>
</div>
</body>
<script>
    mini.parse();
    top['formEdit'] = window
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var type = params.type;//操作id
    var formNo = params.formNo;//表单ID
    //mini-tabs 主要显示面板
    var formEditTabs = mini.get('formEditTabs');
    var formEditForm =  new mini.Form("#formEditTabs");


//解析字段配置
parseFieldConfiguration=function(id,drzpId,formNo,fdsType,fldType){
    CommonUtil.ajax({
        url:"/FormFieldsetController/searchFormFieldsetList",
        data:{formNo: formNo, fdsType: fdsType},
        callback : function(data) {
            var fieldsetFilterList = new Array();
            var fieldFilterList = new Array();
            window['dropZone'+drzpId] = new FieldContainer("#dropZone"+drzpId, {
                fieldset: true,
                fieldsetDraggable: true,
                data: data.obj,
                onFieldClick: function (e) {//增加监听事件
                    if (!this.options.draggble) {
                        mini.open({
                                title:'已配置字段属性编辑',
                                showModal: true,
                                allowResize: true,
                                height:260,width:800,
                                url:CommonUtil.baseWebPath() + '/mini_base/MiniConfiguredFiledPropertyEdit.jsp?drzpId='+drzpId+"&fldNo="+e.field.fldNo,
                                loadOnRefresh: false,       //true每次刷新都激发onload事件
                                onload: function () {       //弹出页面加载完成
                                    var iframe = this.getIFrameEl();  
                                    //调用弹出页面方法进行初始化
                                    iframe.contentWindow.SetData(e.field);
                                                
                                },
                                ondestroy:function(action){
                                    //做些事情
                                }
                            });

                    }
                }
            });
            $.each(data.obj, function(i, fieldset){
                
                fieldsetFilterList.push(fieldset.fdsNo);
                if (fieldset.fields.length != 0) {
			    		$.each(fieldset.fields, function(j, field){
                            fieldFilterList.push(field.fldNo);})}});
      
         praseUnconfigedField(id,drzpId, fdsType, fldType, fieldsetFilterList, fieldFilterList);   
        }
    });
 }
//解析未分配字段(单独抽取以便维护后刷新)
 praseUnconfigedField = function(id,drzpId, fdsType, fldType, fieldsetFilterList, fieldFilterList){
    
    CommonUtil.ajax({
        url:"/FieldsetController/searchFieldsetList",
        data:{fdsType: fldType, useFlag: '1',filterList:fieldsetFilterList},
        callback : function(data) {
            
        window['fileset'+drzpId] = new FieldContainer("#fileset"+drzpId, {
                fieldset: true,
                fieldsetDraggable: true,
                data: data.obj
            });
            CommonUtil.ajax({
                url:"/FieldController/searchFieldList",
                data:{fldType: fldType, useFlag:"1",filterList:fieldFilterList},
                callback : function(data) {
                window['file'+drzpId] = new FieldContainer("#file"+drzpId, {
                        data: data.obj
                    });
                }
            });    
        }
    });
 }

 
	//获取已配置的数据,进行二次加工
	getConfiguredData = function (fieldsets) {
		
        var fieldsetsObj = new Array();
		var fieldsetObj;
		$.each(fieldsets, function(i, fieldset) {
            
			fieldsetObj = fieldset;
			var fields = fieldset.fields;
            var fieldsObj = new Array();
			$.each(fields, function(j, field){
                
			    var fieldObj = field;
				fieldObj.formIndex = j + 1;
			    fieldsObj.push(fieldObj);
            });
			fieldsetObj["formFieldsetFields"] = fieldsObj;
			fieldsetObj.formIndex = i + 1;
			fieldsetsObj.push(fieldsetObj);
        });
		return fieldsetsObj;
	};


    /*******************************************************************
	 *事件定义
	 *******************************************************************/
	//按钮事件：保存
    mini.get('#save_btn').on('click',function(){
    //获取当前formData
    var param = formEditForm.getData();
    //获取预定义值                     
    param["predefinedFieldsets"] = getConfiguredData(window['dropZonePre'].getData());
    //获取自定义值
    param["customizedFieldsets"] = getConfiguredData(window['dropZoneCust'].getData()); 
    var saveUrl = $.inArray(type,["addWith", "addWithout"]) > -1 ? "/FormController/addForm" : "/FormController/editForm";
    CommonUtil.ajax({
            url:saveUrl,
            data:param,
            showLoadingMsg:false,
            callback:function(data) {
                mini.alert("保存成功.","提示",function(){
                    setTimeout(function () {top["win"].closeMenuTab();}, 10);
                });
                
            }
        });
    });

	/*******************************************************************
	 *数据初始化 
	 *******************************************************************/
     if($.inArray(type,["addWith"]) > -1){
		CommonUtil.ajax({
			url:"/FormController/searchFormById",
			data:{formNo:formNo},
			callback:function(data){
				if(data.obj){
                     formEditForm.setData(data.obj);
				}
			}
		});
    }
    if($.inArray(type,["edit","detail"]) > -1){
        var row = top['formManage'].grid.getSelected();
            formEditForm.setData(row);
            
         if(type =="detail"){
            var fields = formEditForm.getFields();
            for (var i = 0, l = fields.length; i < l; i++) {
                var c = fields[i];
                if (c.setReadOnly) c.setReadOnly(true);     //只读
                if (c.setIsValid) c.setIsValid(true);      //去除错误提示
                if (c.addCls) c.addCls("asLabel");          //增加asLabel外观
            }
            //设置按钮不可用
            mini.get("save_btn").setEnabled(false);
         }   
	}
    $(document).ready(function(){
        //预定义
        parseFieldConfiguration('PreDefinedFieldConfiguration','Pre',formNo,'0','0');
        //自定义
        parseFieldConfiguration('CustomizedFieldConfiguration','Cust',formNo,'1','1');
    });




//切换编辑模式
function changeMode(drzpId){
    if(window['file'+drzpId].options.draggble && window['fileset'+drzpId].options.draggble && window['dropZone'+drzpId].options.draggble){
        mini.alert("已切换为字段编辑模式，可单击右侧已配置字段进行编辑","提示",function(){
            window['file'+drzpId].options.draggble=false;
            window['fileset'+drzpId].options.draggble=false;
            window['dropZone'+drzpId].options.draggble=false;
            mini.get("writeBtn"+drzpId).setText("字段拖拽");

        });
    }else{
        mini.alert("已切换为字段拖拽模式，可进行字段配置","提示",function(){
            window['file'+drzpId].options.draggble=true;
            window['fileset'+drzpId].options.draggble=true;
            window['dropZone'+drzpId].options.draggble=true;
            mini.get("writeBtn"+drzpId).setText("字段编辑");
        });
    }
}

function onActiveChanged(e) {
        mini.layout();
    }
</script>