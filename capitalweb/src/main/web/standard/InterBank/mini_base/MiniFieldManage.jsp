<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%> 
<html>
<head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script> 
	<title>字段</title>
</head>
<body style="width:100%;height:100%;background:white">
	<fieldset id="fd2" class="mini-fieldset">
		<legend><label>查询条件</label></legend>
		<div id="find" class="fieldset-body">
			<div id="search_form"   >
					
                <input id="fldName" name="fldName" labelField="true" label="字段名称：" 
                class="mini-textbox" emptyText="请输入模糊字段名称" vtype="maxLength:100"
                labelStyle="text-align:right;"/>
            
                <input id="formName" name="formName" labelField="true" label="表单名称：" 
                class="mini-textbox" emptyText="请输入模糊表单名称" vtype="maxLength:100"
                labelStyle="text-align:right;"/>
            
                <input id="fldType" name="fldType" labelField="true" label="字段种类：" 
                class="mini-combobox" emptyText="请选择字段种类" labelStyle="text-align:right;"
                data="CommonUtil.serverData.dictionary.fldType"/>
            
                <input id="useFlag" name="useFlag" labelField="true" label="启用标志：" 
                class="mini-combobox" emptyText="请选择启用标志" labelStyle="text-align:right;"
                data="CommonUtil.serverData.dictionary.Status"/>
                <span style="float:right;margin-right: 150px">
                    <a id="search_btn" class="mini-button" style="display: none"    onclick="search1()">查询</a>
                    <a id="clear_btn" class="mini-button" style="display: none"    onClick="clear">清空</a>
                    
                </span>
                
            </div>
            
		</div>
    </fieldset>
    <span style="margin:2px;display: block;">
        <a id="add_btn" class="mini-button" style="display: none"   onclick="add">新增</a>
        <a id="edit_btn" class="mini-button" style="display: none"    onclick="modifyedit()">修改</a>
        <a id="delete_btn" class="mini-button" style="display: none"    onclick="removeRows">删除</a>
    </span>


<div class="mini-fit" style="margin-top: 2px;">
    <div id="field_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" 
    idField="fldNo"  pageSize="10" multiSelect="true" onRowdblclick="custDetail" sortMode="client"
    allowAlternating="true">
		<div property="columns" >
            <div type="indexcolumn" headerAlign="center"  align="center" width="40px">序号</div>
            <div type="checkcolumn"></div>
			<div field="fldNo" headerAlign="center"  align="center" width="80px" allowSort="true" dataType="int">字段编号</div>    
			<div field="fldName" headerAlign="center" width="150px">字段名称</div>                            
			<div field="fldType"  headerAlign="center"   width="150px"  renderer="CommonUtil.dictRenderer" data-options="{dict:'fldType'}">字段种类</div>
			<div field="formName"    headerAlign="center"  width="150px">表单名称</div>                                
			<div field="formTips"  headerAlign="center" width="180px">表单提示</div>
			<div field="formType"   headerAlign="center"  renderer="CommonUtil.dictRenderer" data-options="{dict:'fldFormType'}">字段表单类型</div>
			<div field="formCols"  headerAlign="center"   width="80px" allowSort="true" dataType="int">表单占列</div>
			<div field="formRows"  headerAlign="center"  width="80px" allowSort="true" dataType="int">表单占行</div>
			<div field="formLength"  headerAlign="center"   width="80px" allowSort="true" dataType="int">表单长度</div>
			<div field="formMultiple"   headerAlign="center"  align="center" width="60px" renderer="CommonUtil.dictRenderer" data-options="{dict:'YesNo'}">是否多选</div>                
			<div field="formMin"  headerAlign="center"   width="80px" allowSort="true" dataType="int">最小值</div>
            <div field="formMax"  headerAlign="center"   width="100px" allowSort="true" dataType="int">最大值</div> 
            <div field="formPrecision"  headerAlign="center"   width="80px" allowSort="true" dataType="int">显示精度</div>
            <div field="formValid"    headerAlign="center"   renderer="CommonUtil.dictRenderer" data-options="{dict:'formValid'}">表单验证</div>
            <div field="validParam"   headerAlign="center"  width="100px">验证参数</div>
            <div field="isMandatory"  headerAlign="center"  align="center" width="60px" renderer="CommonUtil.dictRenderer" data-options="{dict:'YesNo'}">是否必填</div>
            <div field="isDisabled"    headerAlign="center"  align="center" width="60px" renderer="CommonUtil.dictRenderer" data-options="{dict:'YesNo'}">是否禁用</div>
            <div field="defKey"   headerAlign="center">字典项</div>
            <div field="defValue"   headerAlign="center">默认值</div>
            <div field="useFlag"   headerAlign="center"  align="center" width="60px" renderer="CommonUtil.dictRenderer" data-options="{dict:'Status'}">启用标志</div>
            <div field="lastUpdate" headerAlign="center" align="center" width="150px" allowSort="true" dataType="date">更新时间</div>
            <div field="defText" headerAlign="center" width="150px">默认文本</div>                                                                                                 
		</div>
    </div>
 </div>   
    <script>

        mini.parse();

		$(document).ready(function(){
            //控制按钮显示
            $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
                var grid = mini.get("field_grid");
                grid.on("beforeload", function (e) {
                    e.cancel = true;
                    var pageIndex = e.data.pageIndex;
                    var pageSize = e.data.pageSize;
                    search(pageSize, pageIndex);
                });
                search(grid.pageSize, 0);
            });
		});

        function search1(){
            var grid =mini.get("field_grid");
            search(grid.pageSize,0); 
        }

        function search(pageSize,pageIndex){
            var form =new mini.Form("search_form");
            form.validate();
            if (form.isValid() == false) return;//表单验证
            var data =form.getData();//获取表单数据
            data['pageNumber']=pageIndex+1;
            data['pageSize']=pageSize;
            var param = mini.encode(data); //序列化成JSON
            CommonUtil.ajax({
                url:"/FieldController/searchFieldPage",
                data:param,
                callback:function(data){
                        
                    var grid =mini.get("field_grid");
                    //设置分页
                    grid.setTotalCount(data.obj.total);
                    grid.setPageIndex(pageIndex);
                    grid.setPageSize(pageSize);
                    //设置数据
                    grid.setData(data.obj.rows);
                }
            });
        } 
        

   

    top["fieldManage"]=window;
    function getData(){
        var grid =mini.get("field_grid");
        var record =grid.getSelected(); 
        return record;
    }

    //新增
    function add(){
        var url = CommonUtil.baseWebPath() + "/mini_base/MiniFieldEdit.jsp?action=new";
        var tab = { id: "fieldManageAdd", name: "fieldManageAdd", text: "字段新增", url: url,showCloseButton:true,parentId:top['win'].tabs.getActiveTab().name };
        top['win'].showTab(tab);
    }
    //修改
    function modifyedit(){
        var grid =mini.get("field_grid");
        var  record =grid.getSelected();
       
        if(record){
            var url = CommonUtil.baseWebPath() + "/mini_base/MiniFieldEdit.jsp?action=edit";
            var tab = { id: "fieldManageEdit", name: "fieldManageEdit", text: "字段修改", url: url,showCloseButton:true,parentId:top['win'].tabs.getActiveTab().name };
            top['win'].showTab(tab);
        }else{
            mini.alert("请选中一条记录!","系统提示"); 
        }
    }
    //双击一行打开详情信息
    function custDetail(e){
        var grid = e.sender;
        var row = grid.getSelected();
        if(row){
            var url = CommonUtil.baseWebPath() + "/mini_base/MiniFieldEdit.jsp?action=detail";
            var tab = { id: "fieldManageDetail", name: "fieldManageDetail", text: "字段详情", url: url,showCloseButton:true,parentId:top['win'].tabs.getActiveTab().name };
            top['win'].showTab(tab);
        }
    }

   

    //清空
    function clear(){
        var form =new mini.Form("search_form");
        form.clear();
        search1();
        
    }

    //删除
    function removeRows() {
        var grid =mini.get("field_grid");
        //获取选中行
        var rows =grid.getSelecteds();
        
        if (rows.length>0) {
            var fldNos = new Array();
            var type0Flag = 0;
            for(var i=0;i<rows.length;i++){
                if(rows[i].fldType == 0){//如果字段种类是预定义字段
                    type0Flag = 1;
                }
                fldNos.push(rows[i].fldNo);
            }
            
            mini.confirm(type0Flag==0?"您确认要删除选中记录?":"您选中的记录中含有不可删除的预定义字段,是否跳过预定义字段继续执行删除操作?","系统警告",function(value){   
                if (value == "ok"){   
                    CommonUtil.ajax({
                        url : '/FieldController/removeField/',
                        data : fldNos,
                        callback : function(data){
                            if("error.common.0000" == data.code){
                                mini.alert("删除成功！","系统提示");
                                search1();
                            }
                        }
                    });
                
                }   
            });       
                
        }else{
            mini.alert("请至少选择一条记录!","系统提示");
        }
    }

    
   
    
    </script>
    
</body>
</html>