<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title></title>
  </head>
  
<body style="width:100%;height:100%;background:white">
		<span id="labell"><a href="javascript:CommonUtil.activeTab();">节点</a> >> 修改</span>
<fieldset class="mini-fieldset">
	<legend>节点信息</legend>
<div id="AttachNodeEdit">
	<table id="field_form" class="mini-table" style="width:100%">
		<tr>
			<td><input id="nodeNames" name="nodeNames" class="mini-textbox"  labelField="true" vtype="rangeLength:1,100" required="true"   label="节点名称" style="width:100%;" labelStyle = "width:110px;" />
				<input id="nodeId" name="nodeId" class="mini-hidden"/></td>
			<td><input id="nodeMustFlag" name="nodeMustFlag" class="mini-combobox"  labelField="true"  required="true"  data = "CommonUtil.serverData.dictionary.YesNo" label="是否为必须" style="width:100%;" labelStyle = "width:110px;" /></td>
			<td><input id="nodeAttsize" name="nodeAttsize" class="mini-textbox"  labelField="true" vtype="rangeLength:1,50" required="true"   label="文档大小" style="width:100%;" labelStyle = "width:110px;" /></td>
		</tr>
		<tr>
			<td><input id="nodeAtttype" name="nodeAtttype" class="mini-textbox"  labelField="true" vtype="rangeLength:1,50" required="true"   label="文档类型" style="width:100%;" labelStyle = "width:110px;" /></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td colspan="3"><input id="nodeAttmark" name="nodeAttmark" class="mini-textarea"  labelField="true"  label="文档注释" style="width:100%;" labelStyle = "width:110px;" /></td>
		</tr>
	</table>
</div>
</fieldset>
<div  class="mini-toolbar" style="text-align:left" >
	<a onclick="save()" id="save_btn" class="mini-button" style="display: none"   >保存</a>
	<a onclick="colse()" id="close_btn" class="mini-button" style="display: none"   >关闭</a>
</div>
<script type="text/javascript">
	mini.parse();
	var url=window.location.search;
	var action=CommonUtil.getParam(url,"action");
	var row=top["attachNodeManage"].getData(action);
	var form=new mini.Form("#field_form");
	//加载信息
	function initform(){
		if($.inArray(action,["edit","detail"])>-1){
			$("#labell").html("<a href='javascript:CommonUtil.activeTab();'>节点</a> >> 详情");
			form.setData(row);
			mini.get("nodeNames").setValue(row.nodeName);
		}else{
			$("#labell").html("<a href='javascript:CommonUtil.activeTab();'>节点</a> >> 新增");
		}
	}
	//保存信息
	function save(){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统也提示");
			return;
		}
		var messageid = mini.loading("保存中, 请稍等 ...", "Loading");
		var data=form.getData(true,false);
		data['nodeName']=mini.get("nodeNames").getValue();
		var saveUrl = $.inArray(action,["add"]) > -1 ? "/AttachNodeController/createAttachNode" : "/AttachNodeController/updateAttachNode";
		var params=mini.encode(data);
		CommonUtil.ajax({
			url:saveUrl,
			data:params,
			callback:function(data){
				mini.hideMessageBox(messageid);
				mini.alert("保存成功","提示",function(){
					top["win"].closeMenuTab();
				});
			}
		});
	}
	//关闭表单
	function colse(){
		top["win"].closeMenuTab();
	}
	$(document).ready(function(){
		if($.inArray(action,["detail"])>-1){
			mini.get("save_btn").setVisible(false);
			form.setEnabled(false);
		}
		initform();
	});
</script>
</body>
</html>
