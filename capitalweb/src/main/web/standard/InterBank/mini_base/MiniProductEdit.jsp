<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%> 
<html>
<head>
    <title>产品维护</title>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script> 
</head>
<body style="width:100%;height:100%;background:white">
    <div  style="float:left;margin:5px;">
        <a id="save_btn" class='mini-button blue'    onclick="save">保存</a>
    </div>
    <div id="tabs1" class="mini-tabs"  plain="false"  
    style="width:100%; height:90%;" onbeforeactivechanged="before" >
        <div id="tab1" name="tab1" title="产品基本信息" >
            <table id="search_form"  width="100%" class="mini-table" >
                <tr>
                    <td><input id="prdName" name="prdName" class="mini-textbox"  
                        labelField="true"  label="产品名称" emptyText="请输入产品名称"  
                        vtype="maxLength:50" width="300px"/>
                        <input id="prdNo" name="prdNo" class="mini-hidden"/>
                    </td>
                    <td><input id="prdInst" name="prdInst" class="mini-buttonedit" 
                        onbuttonclick="onInsQuery" labelField="true"  label="所属机构"  
                        emptyText="请选择所属机构" width="300px" allowInput="false"/>
                    </td>
                    <td><input id="prdType" name="prdType" class="mini-treeselect" 
                        labelField="true"  label="产品类型"  emptyText="请选择产品类型" 
                        width="300px"/>
                    </td>
                </tr>
                <tr>
                    
                    <td><input id="prdProp" name="prdProp" class="mini-combobox" 
                        labelField="true"  label="产品性质"  emptyText="请选择产品性质" 
                        data="CommonUtil.serverData.dictionary.prdProp" required="true"
                        width="300px"/>
                    </td>
                    <td><input id="prdTerm" name="prdTerm" class="mini-combobox" 
                        labelField="true"  label="产品期限"  emptyText="请选择产品期限" 
                        data="CommonUtil.serverData.dictionary.prodTerm" 
                        width="300px"/>
                    </td>
                    <td><input id="isTemplate" name="isTemplate" class="mini-combobox" 
                        labelField="true"  label="是否模板"  emptyText="请选择是否模板" 
                        data="CommonUtil.serverData.dictionary.YesNo" required="true"
                        width="300px"/>
                    </td>
                </tr>
                
                <tr>
                    <td><input id="useFlag" name="useFlag" class="mini-combobox"  
                        labelField="true"  label="启用标志"  emptyText="请选择启用标志"  
                        data="CommonUtil.serverData.dictionary.Status" required="true"
                        width="300px"/>
                    </td>
                </tr>
            </table> 
        </div>
        <div id="tab2" name="tab2" title="产品表单配置" style="width:100%;">
            <div class="mini-fit" >
            <fieldset style="width:45%;height:99%;float:left;margin:0px;padding:0px;">
                <legend><label id="labell">已选表单</label></legend>
                <div class="mini-toolbar" >
                    <a id="up_move" class='mini-button'   onclick="upItem">上移</a>
                    <a id="down_move" class='mini-button'    onclick="downItem">下移</a>
                </div>
                    <div id="listbox1" class="mini-listbox"  showCheckBox="true" multiSelect="true"
                    style="height:520px;" >
                        <div property="columns">
                            <div type="indexcolumn" headerAlign="center" align="center" >序号</div>
                            <div field="formNo" header="表单编号"headerAlign="center"  align="center" width="40px"></div>
                            <div field="formInstName" header="所属机构" headerAlign="center" align="center" ></div>
                            <div field="formName" header="表单名称" headerAlign="center" ></div>
                            <div field="formType"  header="表单类型" headerAlign="center" align="center" width="40px" renderer="renderer" data-options="{dict:'formType'}"></div>
                        </div>
                    </div>
            </fieldset>
            <div style="width:5%;height:100%;float:left;">
                <div style="margin-left:10px; margin-top:250px"><a  id="left_move" class="mini-button" style="display: none"    onclick="add()" ></a></div>
                
                <div style="margin-left:10px; margin-top:20px"><a  id="left_moveAll" class="mini-button" style="display: none"    onclick="addAll()" ></a></div>
                
                <div style="margin-left:10px; margin-top:20px"><a id="right_moveAll" class="mini-button" style="display: none"    onclick="removeAll()" ></a></div>
               
                <div style="margin-left:10px; margin-top:20px"><a id="right_move" class="mini-button" style="display: none"     onclick="removes()" ></a></div>
                
            </div>
            <fieldset style="width:45%;height:99%;float:left;margin:0px;padding:0px;">
                <legend><label id="labell">可选表单</label></legend>
                <table id="formOptional_grid" >
                    <tr>
                        <td>
                            <input id="formName" name="formName" class="mini-textbox" 
                            vtype="maxLength:100" labelField="true"  label="表单名称："
                            emptyText="请输入表单名称" labelStyle="text-align:right;width:60px;" width="220px"/>
                        </td>
                        <td>
                            <input id="formType" name="formType" class="mini-combobox"  labelField="true"  
                            label="表单类型："  emptyText="请选择表单类型" labelStyle="text-align:right;width:60px;"
                            data="CommonUtil.serverData.dictionary.formType" width="220px"/>
                        </td> 
                    </tr>
                    <tr>
                        <td>
                            <a class='mini-button'  style='vertical-align:middle;'  onclick="search">查询</a>
                            <a class='mini-button'  style='vertical-align:middle;'  onclick="clear">清空</a>
                            <a class='mini-button'  style='vertical-align:middle;'   onclick="gotoForm">维护</a>
                        </td>
                    </tr>
                </table>
                <div id="listbox2" class="mini-listbox" style="height:490px;"
                    showCheckBox="true" multiSelect="true" >
                    <div property="columns">
                        <div type="indexcolumn" headerAlign="center" align="center">序号</div>
                        <div field="formNo" header="表单编号" align="center" headerAlign="center" width="40px"></div>
                        <div field="formInstName" header="所属机构" headerAlign="center" align="center" ></div>
                        <div field="formName" header="表单名称" headerAlign="center" ></div>
                        <div field="formType"  header="表单类型" width="40px" headerAlign="center"  align="center" renderer="renderer" data-options="{dict:'formType'}"></div>
                    </div>
                </div>
            </fieldset>
            </div>
        </div>
    </div>  
</body>
<script>
        $(document).ready(function(){
            mini.parse();
            prdType1();
            init();
            
        });
        function renderer(e){
            return CommonUtil.dictRenderer(e);
        }

        var url = window.location.search;
        var action = CommonUtil.getParam(url,"action");
        
        function init(){
            if(action == "new"){
                //初始化可选表单
                search();

            }else if(action == "edit"|| action == "detail"){
                var form1 = new mini.Form("#search_form");
                var data=top["prd"].getData();
                form1.setData(data);
                
                mini.get("prdInst").setValue(data.prdInst);
                mini.get("prdInst").setText(data.prdInstName);
                //初始化已选表单列表
                searchSelectedForm();

                if(action == "detail"){
                    //设置表单控件禁用只读
                    form1.setEnabled(false);
                    //设置按钮不可用
                    mini.get("left_move").setEnabled(false);
                    mini.get("left_moveAll").setEnabled(false);
                    mini.get("right_moveAll").setEnabled(false);
                    mini.get("right_move").setEnabled(false);
                    mini.get("up_move").setEnabled(false);
                    mini.get("down_move").setEnabled(false);
                    //设置保存按钮隐藏
                    document.getElementById("save_btn").style.display="none";
                }
            }
        }

        function before(){
            //查询可选表单
            search();
        }
        
       
        //上移
        function upItem() {
            var listbox1 = mini.get("listbox1");
            var items = listbox1.getSelecteds();
            for (var i = 0, l = items.length; i < l; i++) {
                var item = items[i];
                var index = listbox1.indexOf(item);
                listbox1.moveItem(item, index-1);
            }
        }
        //下移
        function downItem() {            
            var listbox1 = mini.get("listbox1");
            var items = listbox1.getSelecteds();            
            for (var i = items.length-1; i >=0; i--) {
                var item = items[i];
                var index = listbox1.indexOf(item);
                listbox1.moveItem(item, index + 1);
            }
        }


        //向左移动一个（listbox2==》listbox1）
        function add() {
            var listbox1 = mini.get("listbox1");
            var listbox2 = mini.get("listbox2");
            var items = listbox2.getSelecteds();
            listbox2.removeItems(items);
            listbox1.addItems(items);
        }

        //向左移动全部（listbox2==》listbox1） 
        function addAll() {
            var listbox1 = mini.get("listbox1");
            var listbox2 = mini.get("listbox2");
            var items = listbox2.getData();
            listbox2.removeItems(items);
            listbox1.addItems(items);
        }

        //向右移动一个（listbox1==》listbox2） 
        function removes() {
            var listbox1 = mini.get("listbox1");
            var listbox2 = mini.get("listbox2");
            var items = listbox1.getSelecteds();
            listbox1.removeItems(items);
            listbox2.addItems(items);
        }

        //向右移动全部（listbox1==》listbox2） 
        function removeAll() {
            var listbox1 = mini.get("listbox1");
            var listbox2 = mini.get("listbox2");
            var items = listbox1.getData();
            listbox1.removeItems(items);
            listbox2.addItems(items);
        }

        //初始化已选表单列表
        function searchSelectedForm(){
            var prdNo=mini.get("prdNo").getValue();
            
            CommonUtil.ajax({
                url:"/ProductFormController/searchFormList",
                data: {"prdNo":prdNo},
                callback: function (data) {
                    var grid = mini.get("listbox1");
                    grid.setData(data.obj);

                    var filterList= new Array();
                    $.each(data.obj, function(i, n){
						filterList.push(n.formNo);
					});

                    searchOptionalForm(1000,0,filterList);
                    
                }
            });
        }

        
        //按钮事件---查询可选表单
        function search(){
            var filterList = new Array();
            var listbox1=mini.get("listbox1").getData();
            if(listbox1.length==0){
                searchOptionalForm(1000,0,null);
            }else{
                $.each(listbox1, function(i, n) {
                    filterList.push(n.formNo);
                    searchOptionalForm(1000,0,filterList);
                });
            }
            
        }


        //初始化/查询 可选表单
        function searchOptionalForm(pageSize,pageIndex,filterList){
            var form =new mini.Form("formOptional_grid");
            form.validate();
            if (form.isValid() == false) return;//表单验证
            var data =form.getData();//获取表单数据
            data['pageNumber']=pageIndex+1;
            data['pageSize']=1000;
            data['branchId']=top['win'].branchId;
            data['useFlag']=1;
            if(filterList!=null && filterList!=''){
                data['filterList']=filterList.toString();
            }
            
            var param = mini.encode(data); //序列化成JSON
            CommonUtil.ajax({
                url:"/FormController/searchPageForm",
                data:param,
                callback:function(data){
                        
                    var grid =mini.get("listbox2");
                    
                    //设置数据
                    grid.setData(data.obj.rows);
                }
            });
        }

        //清空
        function clear(){
            var form =new mini.Form("formOptional_grid");
            form.clear();
            search(); 
        }
        //维护（进入表单页面）
        function gotoForm(){
            
            var url = CommonUtil.baseWebPath() + "/mini_base/MiniFormManage.jsp?action=new";
            var tab = { id: "UserResourceAdd", name: "UserResourceAdd", text: "表单", url: url };
            top['win'].showTab(tab);
        }
        
        
        //产品类型的查询与赋值
        function prdType1() {
            var param = mini.encode({}); //序列化成JSON
            CommonUtil.ajax({
                url: "/ProductTypeController/searchProductType",
                data: param,
                callback: function (data) {
                    var grid = mini.get("prdType");
                    var val = grid.getValue();
                    grid.setData(data.obj);
                    grid.setValue(val);
                }
            });
        
        }

        //所属机构的查询
        function onInsQuery(e) {
			var btnEdit = this;
			mini.open({
				url : CommonUtil.baseWebPath() +"/mini_system/MiniInstitutionSelectManages.jsp",
				title : "机构选择",
				width : 900,
				height : 600,
				ondestroy : function(action) {	
					if (action == "ok") 
					{
						var iframe = this.getIFrameEl();
						var data = iframe.contentWindow.GetData();
						data = mini.clone(data); //必须
						if (data) {
							btnEdit.setValue(data.instId);
							btnEdit.setText(data.instName);
							btnEdit.focus();
						}
					}

				}
			});
		}

        //保存
        function save(){
            //1.获取产品基本信息数据
            var form = new mini.Form("search_form");
            form.validate();
	        if (form.isValid() == false){//表单验证
                mini.alert("请输入有效数据！","系统提示");
                return;
            } 
            var data = form.getData();
            //2.获取产品表单配置
            var listbox1 = mini.get("listbox1");
            var dataForm = listbox1.getData();

            //3.同种类型的表单只允许配置一个
            var count1 = 0;
			var count2 = 0;
            for(var i=0;i<dataForm.length;i++){
                
                if(dataForm[i].formType == "1"){
                    count1++;
                }else if(dataForm[i].formType == "2"){
                    count2++;
                }
            }
            if (count1 > 1 || count2 > 1) {
					mini.alert("同种类型的表单只允许配置一个","系统警告" );
					return false;
				}
            //4.产品基本信息+产品表单配置
                data["forms"]=dataForm;
            
            var saveUrl = "";
                 
            if(action == "new"){
                saveUrl="/ProductController/addProduct";
                data['branchId']=branchId;
            }else if(action == "edit"){
                saveUrl="/ProductController/editProduct";
                data['branchId']=branchId;
            }
            //5.序列化成JSON
            var param = mini.encode(data); 

            CommonUtil.ajax({
				url:saveUrl,
                data:param,
            	callback:function(data){
					if('error.common.0000' == data.code){
				        mini.alert("保存成功!","系统提示",function(value){
                            //关闭并刷新
                            setTimeout(function(){ top["win"].closeMenuTab() },100);
                        });
				
                    }else{
                        mini.alert("保存失败！","系统提示");
                            
                       
                    }	
                }
			});
        }
    </script>

</html>