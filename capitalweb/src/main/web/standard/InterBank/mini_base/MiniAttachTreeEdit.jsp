<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%> 
<html>
<head>
    <title>附件树维护</title>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script> 
</head>
<body style="width:100%;height:100%;background:white">
        <div id="tabs1" class="mini-tabs"  plain="false"  style="width:100%; height:100%;background:white" onbeforeactivechanged="before" >
            <div id="tab1" name="tab1" title="附件树基本信息" style="width:100%;background:white">
                <span id="labell"><a href="javascript:CommonUtil.activeTab();">附件树</a> >> 修改</span>
                <table id="search_form"  width="100%" class="mini-table" >
                    <tr>
                        <td><input id="treeId" name="treeId" class="mini-textbox"  
                            labelField="true"  label="附件树编号" emptyText="附件树编号自动生成"  
                            vtype="maxLength:32" width="300px" enabled="false"/>
                        </td>
                        <td><input id="treeName" name="treeName" class="mini-textbox" 
                             labelField="true"  label="附件树名称"  required="true"
                            emptyText="请输入附件树名称" width="300px" vtype="maxLength:50"/>
                        </td>
                        <td><input id="refType" name="refType" class="mini-combobox" 
                            labelField="true"  label="关联类型"  emptyText="请选择关联类型" 
                            width="300px" data="CommonUtil.serverData.dictionary.refType"
                            required="true"   value="1" />
                        </td>
                    </tr>
                    
                    <tr>
                        <td><input id="treeDesc" name="treeDesc" class="mini-textbox"  
                            labelField="true"  label="附件树描述"  emptyText="请输入附件树描述"  
                            vtype="maxLength:512" width="300px"/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" style="text-align:center;">
                            <a  id="save_btn" class="mini-button" style="display: none"    onclick="save">保存</a>
                            <a id="cancel_btn" class="mini-button" style="display: none"    onclick="cancel">取消</a>
                        </td>
                    </tr>
                </table> 
            </div>
            <div id="tab2" name="tab2" title="附件树配置" style="width:100%;background:white">
                
                <fieldset style="width:45%;height:99%;float:left;margin:0px;padding:0px;">
                    <legend><label>已选节点</label></legend>
                    <div id="listbox1" class="mini-listbox"  showCheckBox="true" multiSelect="true"
                        style="height:670px" >
                        <div property="columns">
                            <div type="indexcolumn" headerAlign="center" align="center" width="15px" >序号</div>
                            <div field="parId" header="附件树ID" headerAlign="center"  align="center" width="30px"></div>
                            <div field="attachId" header="节点ID" headerAlign="center" align="center" width="30px"></div>
                            <div field="nodeName"  header="节点名称" headerAlign="center"  ></div>
                        </div>
                    </div>
                </fieldset>
                <div style="width:5%;height:100%;float:left;">
                    <div style="margin-left:10px; margin-top:250px"><a  id="left_move" class="mini-button" style="display: none"    onclick="add()" ></a></div>
                    
                    <div style="margin-left:10px; margin-top:20px"><a  id="left_moveAll" class="mini-button" style="display: none"    onclick="addAll()" ></a></div>
                    
                    <div style="margin-left:10px; margin-top:20px"><a id="right_moveAll" class="mini-button" style="display: none"    onclick="removeAll()" ></a></div>
                   
                    <div style="margin-left:10px; margin-top:20px"><a id="right_move" class="mini-button" style="display: none"     onclick="removes()" ></a></div>
                    
                </div>
                <fieldset style="width:45%;height:99%;float:left;margin:0px;padding:0px;">
                    <legend><label>可选节点</label></legend>
                    <table id="optional_node" >
                        <tr>
                            <td>
                                <input id="nodName" name="nodName" class="mini-textbox" 
                                vtype="maxLength:100" labelField="true"  label="节点名称："
                                emptyText="请输入节点名称" labelStyle="text-align:right;width:60px;" width="220px"/>
                            </td>
                            <td>
                                <a class='mini-button'  style='vertical-align:middle;'  onclick="search">查询</a>
                                <a class='mini-button'  style='vertical-align:middle;'  onclick="clear">清空</a>
                            </td>
                            
                        </tr>
                        
                    </table>
                    <div id="listbox2" class="mini-listbox" style="height:620px;" showCheckBox="true" multiSelect="true" >
                        <div property="columns">
                            <div type="indexcolumn" headerAlign="center" align="center" width="15px">序号</div>
                            <div field="id" header="节点ID" align="center" headerAlign="center" width="30px"></div>
                            <div field="name" header="节点名称" headerAlign="center" ></div>
                        </div>
                    </div>
                </fieldset>
                
            </div>
        </div>  

</body>
<script type="text/javascript">
    var url = window.location.search;
    var action = CommonUtil.getParam(url,"action");
    
    $(document).ready(function(){
        mini.parse();
        init();
    });

    function before(){
        var treeId = mini.get("treeId").getValue();
        searchOptionalNode(treeId);
    }

    

    function init(){
        if(action == "new"){
            $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>附件树</a> >> 新增");
            var data = {};
            data = {refId:null, refType:null};
            //获取树编号
            CommonUtil.ajax({
                url:"/AttachTreeController/searchAttachTree",
                data:data,
                callback:function(data1){
                    
                    mini.get("treeId").setValue(data1.obj[0].treeId);
                    
                    searchOptionalNode(data1.obj[0].treeId);
                }
            });
        }else if(action == "edit"|| action == "detail"){
            var form1 = new mini.Form("#search_form");
            var data=top["attachTreeManage"].getData();
            form1.setData(data);
            var treeId = data.treeId;
            searchOptionalNode(treeId);
            

             if(action == "detail"){
                $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>附件树</a> >> 详情");
                //设置表单控件禁用只读
                form1.setEnabled(false);
                //设置按钮不可用
                mini.get("left_move").setEnabled(false);
                mini.get("left_moveAll").setEnabled(false);
                mini.get("right_moveAll").setEnabled(false);
                mini.get("right_move").setEnabled(false);
                
                //设置保存按钮、取消按钮隐藏
                document.getElementById("save_btn").style.display="none";
                document.getElementById("cancel_btn").style.display="none";
            } 
        }
    }


    //修改时，根据treeId查询已选节点、可选节点
    function searchOptionalNode(treeId){
        CommonUtil.ajax({
            url:"/AttachNodeController/searchAttachNodeRelationList",
            data: {"treeId":treeId},
            callback: function (data) {
                //给已选节点赋值
                var grid = mini.get("listbox1");
                grid.setData(data.obj);
                //将已选节点的节点ID放在数组中，作为可选节点的过滤查询
                var filterList= new Array();
                $.each(data.obj, function(i, n){
					filterList.push("'"+n.attachId+"'");
				});

                //根据filterList查询可选节点
                var optional_node = new mini.Form("#optional_node");
                var nodeData = optional_node.getData();

                nodeData.pageSize = 100000;
                nodeData.pageIndex = 0;
                nodeData.filterList = filterList.toString();
                var nodeParam = mini.encode(nodeData);
                CommonUtil.ajax({
                    url:"/AttachTreeController/getTcAttachTreeNodeVo",
                    data: nodeParam,
                    callback:function(data) {
						mini.get("listbox2").setData(data.obj.rows);
					}
                });
            }
        });
    }
    
    //左移
    function add(){
        var treeId = mini.get("treeId").getValue();
        var listbox2 = mini.get("listbox2");
        var rows = listbox2.getSelecteds();
        var array_id = new Array();
        var attachType_arr = new Array();
        $.each(rows, function(i, n) {
            array_id.push(rows[i].id);
            attachType_arr.push(rows[i].type);
        });

        CommonUtil.ajax({
            url:"/AttachNodeController/addAttachRelation",
            data:{treeId:treeId, filterList:array_id.toString(),attachTypes:attachType_arr.toString()},
            callback:function(data) {
                searchOptionalNode(treeId);
            }
        });  

    }

    //左移全部
    function addAll(){
        var treeId = mini.get("treeId").getValue();
        var listbox2 = mini.get("listbox2");
        var rowsAll = listbox2.getData();
        var array_id = new Array();
		var attachType_arr = new Array();
        $.each(rowsAll, function(i, n) {
            array_id.push(rowsAll[i].id);
            attachType_arr.push(rowsAll[i].type);
        });
        CommonUtil.ajax({
            url:"/AttachNodeController/addAttachRelation",
            data:{treeId:treeId,filterList:array_id.toString(),attachTypes:attachType_arr.toString()},
            callback:function(data) {
                searchOptionalNode(treeId);
            }
        });  

    }
    //右移全部
    function removeAll(){
        var treeId = mini.get("treeId").getValue();
        var listbox1 = mini.get("listbox1");
        var rowsAll = listbox1.getData();
        var array_id = new Array();
        $.each(rowsAll, function(i, n) {
            array_id.push("'"+rowsAll[i].attachId+"'");
        });
        CommonUtil.ajax({
            url:"/AttachNodeController/delAttachRelation",
            data:{treeId:treeId, filterList:array_id.toString()},
            callback:function(data) {
                searchOptionalNode(treeId);
            }
        });  
    }
    //右移
    function removes(){
        var treeId = mini.get("treeId").getValue();
        var listbox1 = mini.get("listbox1");
        var rows = listbox1.getSelecteds();
        var array_id = new Array();
        $.each(rows, function(i, n) {
            array_id.push("'"+rows[i].attachId+"'");
        });
        if (array_id.length>0){
            CommonUtil.ajax({
                url:"/AttachNodeController/delAttachRelation",
                data:{treeId:treeId,filterList:array_id.toString()},
                callback:function(data) {
                    searchOptionalNode(treeId);
                }
            });  
        }else{
            mini.alert("请选择记录操作!","系统提示");
        }
    }

    function search(){
        var treeId = mini.get("treeId").getValue(); 
        searchOptionalNode(treeId); 
    }

    //清空
    function clear(){
        var treeId = mini.get("treeId").getValue();
        var optional_node = new mini.Form("#optional_node");
        optional_node.clear();
        searchOptionalNode(treeId);
    }

    //附件树基本信息保存
    function save(){
        //1.附件树基本信息验证
        var form = new mini.Form("search_form");
        form.validate();
        if (form.isValid() == false){//表单验证
            mini.alert("请输入有效数据！","系统提示");
            return;
        }
        //2.获取数据
        var data = form.getData();
        //3.序列化成JSON
        var param = mini.encode(data);
        //4.提交路径
        var saveUrl = "";
        if(action == "new"){
            saveUrl="/AttachTreeController/createAttachTree";
        }else if(action == "edit"){
            saveUrl="/AttachTreeController/editAttachTree";
        } 
        //
        CommonUtil.ajax({
            url:saveUrl,
            data:param,
            callback:function(data) {
                mini.alert("保存成功!","系统提示",function(value){
                    //关闭并刷新
                    setTimeout(function(){ top["win"].closeMenuTab() },100);
				});
                
            }
            
        }); 

    } 

    //取消按钮
    function cancel(){
        window.CloseOwnerWindow();
    }
</script>
</html>