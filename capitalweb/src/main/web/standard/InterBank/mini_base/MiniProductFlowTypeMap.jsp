<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%> 
<html>
<head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script> 
	<title>业务审批分配</title>
</head>
<body style="width:100%;height:100%;background:white">
    <fieldset>
        <a class='mini-button blue' id="save_btn"    onclick="save">保存</a>
    </fieldset>
    <div class="mini-fit">
        <div class="mini-splitter" style="width:100%;height:100%;" allowResize="false" borderStyle="border:solid 1px #aaa;">
        <div size="50%">
            <div title="产品树" class="mini-panel" style="width:100%;height:100%;">
                <ul id="prd_tree" class="mini-tree"  style="width:100%;height:100%;" 
                    showTreeIcon="true" textField="text" idField="id" value="base"
                        showTreeLines="false" onselectionchanged="onSelectionChangedSearchTypeTree">        
                </ul>
            </div>

        </div>
        <div size="50%">
            <div title="流程类型树" class="mini-panel" style="width:100%;height:100%;">
                <ul id="type_tree" class="mini-tree" style="width:100%;height:100%;"
                    showTreeIcon="true" textField="text" idField="id" value="base"
                    showCheckBox="true" showTreeLines="false" >        
                </ul>
            </div>
        </div>
        </div>
    </div>
    <script>
        mini.parse();

        $(document).ready(function(){
            //控制按钮显示
            $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
                //加载产品树
                searchPrdTree();
                //加载未check的流程类型
                searchTypeTree();
            });
        });

        //加载产品树
        function searchPrdTree(){
            CommonUtil.ajax({
                data:{},
                url:"/ProductController/getProductAndExt",
                callback:function(data){
                    var prd_tree = mini.get("prd_tree");
                    prd_tree.setData(data.obj);
                }
            });
        }

        //加载未check的流程类型
        function searchTypeTree(){
            var typeTree =CommonUtil.getDictList("FlowType");
            var type_tree = mini.get("type_tree");
            type_tree.setData(typeTree);

        }

        //点击产品，出现流程类型
        function onSelectionChangedSearchTypeTree(e){
            var prd_tree = e.sender;
            var record = prd_tree.getSelected();
            if(!record){
			     return false;
		    }
            CommonUtil.ajax({
                url:"/ProductTypeController/getFlowTypeByProductType",
                data:{"prdTypeNo":record.prdNo},
                callback : function(data) {
                    //取消选中的所有流程类型节点
                    var type_tree=mini.get("type_tree");
                    type_tree.uncheckAllNodes();
                    //循环判断，还原产品的流程类型
                    var roots = type_tree.getData();
                    $.each(data.obj, function(i,item){
                        $.each(roots, function(j,item2){
                            if(item.flowTypeNo == item2.id){
                                type_tree.checkNode(roots[j]);
                            }
                        });
                    });


                }
            });
        }
        //保存按钮
        function save(){
            var saveObject = {};
            //1.读取选择的产品,若未选择，返回
            var prd_tree = mini.get("prd_tree");
            var record = prd_tree.getSelected();
            if(!record){
                mini.alert("请选择产品!","系统提示");
                return;
            }
            //2.重新构建数据传输对象
            saveObject.prdTypeNo = record.prdNo;
            saveObject.flowTypeNos = "";
            //3.获取选中的流程类型节点
            var type_tree=mini.get("type_tree");
            var flowTypeList=type_tree.getCheckedNodes();
            //4.将流程类型节点整理成NO1，NO2,NO3,...
            $.each(flowTypeList,function(i,item){
                saveObject.flowTypeNos += item.id;
                saveObject.flowTypeNos += i==flowTypeList.length-1?"":",";
            });
            //5.数据保存
            CommonUtil.ajax({
                url:"/ProductTypeController/saveFlowTypeByProductType",
                data:saveObject,
                callback:function(data){
                    if('error.common.0000' == data.code){
                        mini.alert("保存成功!","系统提示",function(value){
                        });
                    }else{
                        mini.alert("保存失败！","系统提示");
                    }	
                }
            });


        }

    </script>


</body>
</html>