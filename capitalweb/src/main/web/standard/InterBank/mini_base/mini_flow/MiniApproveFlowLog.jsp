<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../../global.jsp"%>
<html>
<head>
	<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
</head>
  
<body style="width:100%;height:100%;background:white">
	<div class="mini-toolbar" style="border-bottom:0;padding:0px;" style="width:100%;">
		<table style="width:100%;">
			<tr>
				<td>
					<a class="mini-button" style="display: none"  onclick="showLog()" >审批流程图</a>
				</td>
			</tr>
		</table>           
	</div>
	<div class="mini-fit">
		<fieldset style="width:100%;height:90%;">
			<legend>审批日志详情</legend>
			<div id="log_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" 
				allowAlternating="true" allowResize="true" border="true" multiSelect="false" showPager="false">
				<div property="columns">
					<div type="indexcolumn" headerAlign="center" width="50">序号</div>
					<div field="ActivityName" width="150" headerAlign="center" allowSort="false" renderer="activityTypeRenderer">节点名称</div>   
					<div field="TaskId" width="100" align="center" headerAlign="center" allowSort="false">任务ID</div>
					<div field="Assignee" width="100" align="center" headerAlign="center" allowSort="false">用户</div>
					<div field="StartTime" width="145" headerAlign="center" allowSort="false" renderer="stampToTimeRenderer">开始时间</div> 
					<div field="EndTime" width="145" headerAlign="center" allowSort="false" renderer="stampToTimeRenderer">结束时间</div>    
					<div field="Duration" width="100" headerAlign="center" allowSort="false" renderer="timerFormatRenderer">审批时长</div>
					<div field="Message" width="300" headerAlign="center" allowSort="false">备注</div>
				</div>
			</div>
		</fieldset>
	</div>
</body>
<script type="text/javascript">
	mini.parse();
	//serial_no  提交审批的流水号，一般为审批单编号
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var serial_no = params.serial_no;

	var ApproveFlowLog = {};

	var grid = mini.get("log_grid");
	/**
	 *   获取审批日志信息 
	*/
	ApproveFlowLog.loadLogInfo = function(){
		CommonUtil.ajax({
			url:"/FlowController/approveLog",
			data:{'serial_no':serial_no},
			callback : function(data) {
				if(data != null){
					grid.setData(data.obj);
					if(typeof(FlowDesigner) != "undefined")
					{
						FlowDesigner.addApporveLog(data.obj);
					}
				}
			}
		});
	}

	//显示流程跟踪图
	function showLog(){
		var param = {
			"serial_no": serial_no
		};
		CommonUtil.ajax({
			url:"/FlowController/getFlowIdAndInstanceIdBySerialNo",
			data:param,
			callback : function(data) {
				if(data != null){
				    if(CommonUtil.isIE(6) || CommonUtil.isIE(7) || CommonUtil.isIE(8) || CommonUtil.isIE(9)){
				    	window.open(baseUrl+'slbpm/processDefinition_temp/showProcessInstanceImage?processDefinitionId='+data.obj.flow_id+'&processInstanceId='+data.obj.instance_id);
				    }else{
				    	window.open(baseUrl+'slbpm/diagram-viewer/index.html?processDefinitionId='+data.obj.flow_id+'&processInstanceId='+data.obj.instance_id);
				    }
				}
			}
		});
	}

	//根据英文节点类型返回相应中文
	ApproveFlowLog.ActivityType = function(type){
		var result = null;
		switch(type){
			case "startEvent":
				result = "开始节点";
				break;
			case "exclusiveGateway":
				result = "决策节点";
				break;
			case "userTask":
				result = "人工节点";
				break;	
			case "endEvent":
				result = "结束节点";
				break;	
			default:
				result = type;
		}
		return result;
	}

	function stampToTimeRenderer(e) {
		if(e.value){
			return CommonUtil.stampToTime(e.value);	
		}else{
			return "";
		}
	}

	function timerFormatRenderer(e) {
		if(e.value){
			return CommonUtil.secondFormatter(e.value/1000);
		}else{
			return "";
		}
	}

	function activityTypeRenderer(e){
		if(e.value){
			return e.value;
		}else{
			return ApproveFlowLog.ActivityType(e.value);
		}
	}

	$(document).ready(function(){
		CommonUtil.ajax({
			url:"/FlowController/getOneFlowDefineBaseInfo",
			data:{serial_no:serial_no},
			callerror: function(data){
				ApproveFlowLog.loadLogInfo();
			},
			callback : function(data) {
				var innerInterval;
				innerInitFunction = function(){
					clearInterval(innerInterval);
					ApproveFlowLog.loadLogInfo();
				},
				innerInterval = setInterval("innerInitFunction()",100);
			}
		});
	});
</script>
</html>