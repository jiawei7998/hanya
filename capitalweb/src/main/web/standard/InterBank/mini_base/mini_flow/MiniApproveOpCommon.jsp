<%@ page language="java" pageEncoding="UTF-8"%>
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
<div id="ApproveOperate_div"  style='background:white;' width="100%">
	<fieldset><legend>审批</legend>
		<table id="approve_operate_form" width="100%" height="100%" cols="4">
			<tr>
				<td width="10%" id="td_user">指定审批人11111</td>
				<td width="40%">
					<input id="user" name="user" class="mini-combobox" style='width:70%;'/>
					<span id="td_user_text"></span>
				</td>
				<td width="10%">常用语</td>
				<td width="40%">
					<input id="add_text_combobox" class="mini-combobox" style='width:70%;' 
						data="[{'id':'1','text':'同意'},{'id':'2','text':'已阅'},{'id':'3','text':'不同意'},{'id':'4','text':'退回修改'},{'id':'5','text':'请领导审批'}]"/>
					<a  id="add_text_btn" class="mini-button" style="display: none"   >添加</a>
				</td>
			</tr>

			<tr>
				<td style="width:100%;" colspan="4">
					<div title="审批意见">
						<input id="approve_msg" class='mini-textarea' required='true'  vtype="maxLength:500" style="width:100%;height:60px;"/>
					</div>
				</td>
			</tr>

			<tr>
				<td style="width:100%;" colspan="4">
					<a id="pass_btn"     class="mini-button" style="display: none"   visible="false">直接通过</a>
					<a id="continue_btn" class="mini-button" style="display: none"   visible="false">通过</a>
					<a id="skip_btn"   class="mini-button" style="display: none"   visible="false">加签</a>
					<a id="redo_btn"   class="mini-button" style="display: none"   visible="false">退回发起</a>
					<a id="back_btn"   class="mini-button" style="display: none"   visible="false">驳回</a>
					<a id="nopass_btn" class="mini-button" style="display: none"   visible="false">撤销</a>
				</td>
			</tr>
		</table>
	</fieldset>
</div>
