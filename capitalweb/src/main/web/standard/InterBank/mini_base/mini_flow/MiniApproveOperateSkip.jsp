<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
  </head>
  
<body style="width:100%;height:100%;background:white">
	<fieldset class="mini-fieldset">
		<legend>查询</legend>
		<div id="search_form"  style="width:100%;height:15px;"></div>
			<table style="width:100%;height:100%;">
				<tr>
					<input id="isActive" name="isActive" value="1" class="mini-hidden"/>

					<td>
						<input id="userId" name="userId" class="mini-textbox" labelField="true"  label="用户代码"  emptyText="请输入用户代码"/>
					</td>
					
					<td>
						<input id="userName" name="userName" class="mini-textbox" labelField="true"  label="用户名称" emptyText="请输入用户名称"/>
					</td>
				</tr>

				<tr>
					<td width="100%">
						<a	class="mini-button" style="display: none"   id="search_btn" onclick="query();">刷新</a>
						<a	class="mini-button" style="display: none"   id="save_btn" onclick="save();">确认</a>
					</td>
				</tr>
			</table>
		</div>
	</fieldset>

	<div class="mini-fit">
		<div id="user_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" allowResize="true" height="100%">
			<div property="columns">
				<div type="indexcolumn" width="50">序号</div>
				<div name="userName" field="userName" width="150">用户代码</div>
				<div name="instName" field="instName" width="180">机构名称</div>
				<div name="isActive" field="isActive" width="100" renderer="CommonUtil.dictRenderer" data-options="{dict:'YesNo'}">是否启用</div>
			</div>
		</div>
	</div>

<script type="text/javascript">
	mini.parse();

	var grid = mini.get("user_grid");
	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});

	function search(pageSize,pageIndex)
	{
		var form = new mini.Form("#search_form");
		form.validate();
		if(form.isValid() == false){
			mini.alert("表单填写错误,请确认!","提示信息")
			return;
		}

		var data = form.getData();
		data['pageNumber'] = pageIndex+1;
		data['pageSize'] = pageSize;

		var url = "/UserController/searchInvitationUser";
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:url,
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setData(data.obj.rows);
				grid.setPageIndex(pageIndex);
				grid.setPageSize(pageSize);
			}
		});
		
	}// end search

	function query(){
		search(grid.pageSize,0);
	}
	
	var data = null;
	function save(){
		var selections = grid.getSelecteds();
		if(!selections){
			mini.alert("请选择数据！");
			return false;
		}
		var userIds = "";
		$.each(selections,function(i,item){
			userIds += item.userId;
			if(i < selections.length - 1){
				userIds += ",";
			}
		});
		data = userIds;
		CloseWindow("ok");
	}

	function CloseWindow(action) {
		if (window.CloseOwnerWindow){
				return window.CloseOwnerWindow(action);
		}else{ 
			window.close();
		}
	}
	
	function getData(){
		return data;
	}

	$(document).ready(function(){
		query();
	});
</script>

</body>
</html>