<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../../global.jsp"%> 
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"/>
<script type="text/javascript" src="<%=basePath%>/miniScript/baseUrl.js"/>
<div id="ApproveOperate_div" class="easyui-panel" style='border:0' data-options="fit:true">
	<div id="log_tabs">  
	    <div title="审批详细信息"  border="false" >  
	    	<div id="op_layout"> 
		    	<div style="width:100%;height:150px;">
					<div style="height:65%;width:100%">
						<div class="mini-toolbar" style="border-bottom:0;padding:0px;">
							<table style="width:100%;">
								<tr>
									<td style="width:100%;">
										<a id="show_btn" class="mini-button" style="display: none"   onclick="ApproveOperate.showLog" plain="true">查看日志</a>
									</td>
								</tr>
							</table>           
						</div>

						<div id="tradeManage" class="mini-fit" style="margin-top: 2px;">      
							<div id="log_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="dealNo" allowAlternating="true" allowResize="true" onrowdblclick="onRowDblClick" border="true"  frozenStartColumn="0" frozenEndColumn="2">
								<div property="columns">
									<div type="indexcolumn" headerAlign="center" width="50">序号</div>
									<div field="ActivityName" width="85" headerAlign="center" allowSort="true">节点名称</div>   
									<div field="TaskId" width="70" align="center" headerAlign="center">任务ID</div>
									<div field="Assignee" width="80" align="center" headerAlign="center">用户UM</div>
									<div field="AssigneeName" width="80" headerAlign="center" allowSort="true">用户姓名</div>   
									<div field="StartTime" width="145" headerAlign="center" allowSort="true">开始时间</div>   
									<div field="EndTime" width="145" headerAlign="center" allowSort="true">结束时间</div>   
									<div field="Duration" width="80" headerAlign="center" allowSort="true">审批时长</div>   
									<div field="Message" width="350" allowSort="true">备注</div>
								</div>
							</div>
						</div>
					</div>
					<!--
					<div style="height:35%;width:100%">
						<table id="s_grid"></table>
					</div>
					-->
				</div>
	    		<div style="width:100%;height:150px;">
	    			<table id="approve_operate_form" class="form_table" data-options="width:'100%'" cols="4">
						<tr>
						  	<td>指定审批人</td>
	    					<td id="td_user"><input id="user" class="easyui-combobox" /></td>
						</tr>
	    				<tr>
	    					<td>常用语</td>
	    					<td> 
						  		<select id="add_text_combobox" class="easyui-slcombobox">
				  					<option>同意</option>
				  					<option>已阅</option>
				  					<option>不同意</option>
				  					<option>退回修改</option>
				  					<option>请领导审批</option>
				  		        </select>
						  		<a href="javascript:void(0)" id="add_text_btn" class="easyui-linkbutton" >添加</a>
						  	</td>
						</tr>
						<tr>
							<td style="width:100%;"></td>
								<input id="approve_msg" class='mini-textarea' required="true" />
							</td>
						</tr>
						<tr>
							<td style="width:100%;">
								<a id="pass_btn"     class="mini-button" style="display: none"   visible="false" onclick="query">直接通过</a>
								<a id="continue_btn" class="mini-button" style="display: none"   visible="false" onclick="query">通过</a>
								<a id="skip_btn"   class="mini-button" style="display: none"   visible="false" onclick="query">加签</a>
								<a id="redo_btn"   class="mini-button" style="display: none"   visible="false" onclick="query">退回发起</a>
								<a id="back_btn"   class="mini-button" style="display: none"   visible="false" onclick="query">驳回</a>
								<a id="nopass_btn" class="mini-button" style="display: none"   visible="false" onclick="query">撤销</a>
							</td>
						</tr>
	    			</table>
	    		</div>
	    	</div>
	    </div> 
	</div>
</div>

<script type="text/javascript">
	var ApproveOperate = {};
	var temp_log_data = null;
	var task_id = null;
	var flow_type = null;
	var serial_no = null;
	var isLogPage = null;
	var isNeedAuth = null;
	var authType = null;
	var temp_flow_info = null;

	mini.parse();
	
	//显示流程跟踪图
	ApproveOperate.showLog = function(){
		var param = {
			"serial_no": serial_no
		};
		CommonUtil.ajax( {
			url:"/FlowController/getFlowIdAndInstanceIdBySerialNo",
			data:param,
			callback : function(data) {
				if(data != null){
				    if(CommonUtil.isIE(6) || CommonUtil.isIE(7) || CommonUtil.isIE(8) || CommonUtil.isIE(9)){
				    	window.open(baseUrl+'slbpm/processDefinition_temp/showProcessInstanceImage?processDefinitionId='+data.obj.flow_id+'&processInstanceId='+data.obj.instance_id);
				    }else{
				    	window.open(baseUrl+'slbpm/diagram-viewer/index.html?processDefinitionId='+data.obj.flow_id+'&processInstanceId='+data.obj.instance_id);
				    }
				}
			}
		});
	}

	//根据英文节点类型返回相应中文
	ApproveOperate.ActivityType = function(type){
		var result = null;
		switch(type){
			case "startEvent":
				result = "开始节点";
				break;
			case "exclusiveGateway":
				result = "决策节点";
				break;
			case "userTask":
				result = "人工节点";
				break;	
			case "endEvent":
				result = "结束节点";
				break;	
			default:
				result = type;
		}
		return result;
	}
	
	ApproveOperate.loadLogInfo = function(){
		var param={};
		param.serial_no = serial_no;
		CommonUtil.ajax( {
			url:"/FlowController/approveLog",
			data:param,
			callback : function(data) {
				if(data != null){
					ApproveOperate.log_grid.datagrid('loadData',data.obj);
					temp_log_data = data.obj;
					if(typeof(FlowDesigner) != "undefined"){
						FlowDesigner.addApporveLog(data.obj);
					}
					if(temp_flow_info && temp_flow_info.flow_id && !isLogPage){
						ApproveOperate.getUserNextStep(temp_flow_info.ActivitiId,task_id,serial_no);
					}
				}
			}
		});
		//挂起日志
		CommonUtil.ajax( {
			url:"/FlowController/getSuspendlog",
			data:param,
			callback : function(data) {
				if(data.obj){
					ApproveOperate.s_grid.datagrid('loadData',data.obj);
				}
			}
		});
	}
	
	//获取下一人工任务节点审批人
	//edit by wangchen on 2017/5/23
	ApproveOperate.getUserNextStep = function(flow_id,task_id,serial_no){
		CommonUtil.ajax({
			url:"/FlowController/getNextStepCandidatesList",
  			data:{
  				flow_id:flow_id,
  				step_id:task_id,
  				serial_no:serial_no
  			},
  			callback : function(data) {
  				if(data.obj){
  					if (data.obj.length > 0) {  						
	  					//将用户格式转化为tree可接受的格式
	  					var userData = new Array();
	  					userData.push({"value":"","text":"---不指定下一步审批人---"});
	  					$.each(data.obj,function(i,item){
	  						userData.push({"value":item.userId,"text":item.userName});
	  					});
						//载入combotree
						mini.get("user").setData(userData);  
	  				}
  					else {
						//未查询到待指派人则视为下个节点非人工审批节点，无法指派待审批人
						mini.get("user").setVisible(false);
	  					$("#ApproveOperate_div #td_user").append("<span style='color:#ff3333;font-weight:bold;'>*&nbsp;下一个节点非人工审批节点，因此无法指定审批人。");
	  				}
  				}
  				else {
  					//未查询到待指派人则视为下个节点非人工审批节点，无法指派待审批人
  					mini.get("user").setVisible(false);
  					$("#ApproveOperate_div #td_user").append("<span style='color:#ff3333;font-weight:bold;'>*&nbsp;下一个节点非人工审批节点，因此无法指定审批人。");
  				}
  			}
  		});
	}
	
$(document).ready(function(){
	if(Approve.FlowType.SettleApproveFlow == flow_type){
	 	//结算不允许拒绝
		mini.get("nopass_btn").setVisible(false);
	}
	mini.get("pass_btn").setVisible(false);

	if(isLogPage){
		$("#"+ApproveOperate.tabOptions.id).find("#ApproveOperate_div").find("#ApproveOperate_form").remove();
		$("#"+ApproveOperate.tabOptions.id).find("#ApproveOperate_div").find("#ApproveOperate_btn").remove();
		$("#"+ApproveOperate.tabOptions.id).find("#ApproveOperate_div").find("#op_layout").layout('remove','east');
	}
	
	// add by wangchen on 2017/5/22 
	//审批权限控制按钮显示
	//start
	if(!ApproveOperate.tabOptions.param.isLogPage){
		CommonUtil.ajax({
			data:{"task_id":task_id},
			url:"/FlowRoleController/getUserFlowOperations",
			callback:function(data){
				if(data.obj){
					//可视的审批权限为：4.审批;7.加签;8.退回;9.驳回;10.撤销;11.挂起.
					$.each(data.obj,function(i,item){
						switch(item){
							case "7":
								//7.加签
								mini.get("skip_btn").setVisible(true);
								break;
							case "8":
								//8.退回
								mini.get("redo_btn").setVisible(true);
								break;
							case "9":
								//9.驳回
								mini.get("back_btn").setVisible(true);
								break;
							case "10":
								//10.拒绝
								mini.get("nopass_btn").setVisible(true);
								break;
						}
					});
				}
			}
		});
	}
	//end
	
	ApproveOperate.tt_tabs = $("#"+ApproveOperate.tabOptions.id).find('#ApproveOperate_div #log_tabs').sltabs({
		tabPosition:'left',
		border:false,
		fit:true
	});
	
	//控件定义：审批日志详情
	ApproveOperate.log_grid = mini.get("log_grid");
	
	
	//控件定义：挂起日志详情
	ApproveOperate.s_grid = $("#"+ApproveOperate.tabOptions.id).find('#ApproveOperate_div #s_grid').datagrid({
		fit:true,
		rownumbers:true, 
		border:false,
		pagination:false,
		singleSelect:true,
		title:"挂起日志",
		columns:[[   
	        {field:'op_desc',title:'挂起操作',width:180},
	        {field:'user_id',title:'操作人UM',width:150}, 
	        {field:'user_name',title:'操作人姓名',width:150},   
	        {field:'op_time',title:'操作时间',width:180},   
	        {field:'reason',title:'理由',width:200}
        ]],
		onDblClickRow:function(){}
	});
	
	/*----------------事件注册    开始--------------------*/
	//事件注册： 通过
	mini.get("pass_btn").on("click",function(){
		var approve_msg = mini.get("approve_msg");
		approve_msg.validate();
		if(!approve_msg.isValid()){
			return false;
		}

		mini.confirm("确认通过该笔审批吗？","确认",function (action) {
			if (action != "ok") {
				return;
			} 
			if(isNeedAuth){
				var authPageUrl = CommonUtil.baseWebPath() + '/mini_settle/MiniSettleAuthEdit.jsp?authKey1=' + serial_no + '&authType=' + authType;
				mini.open({
					url : authPageUrl,
					title : "请授权",
					width:300,
					height:150,
					onload : function(e){
						var param = {
							authKey1:serial_no,
							authType:authType
						};
						var iframe = this.getIFrameEl();
						try{
							if(iframe.contentWindow.init && typeof(iframe.contentWindow.init)=="function"){
								iframe.contentWindow.init(param);
							}
						}catch(e){
							
						}

					},
					ondestroy : function(action) {		
						if (action == "ok") 
						{
							Approve.approveOperator({
								task_id : task_id,
								specific_user : mini.get("user").getValue(),
								msg : mini.get("approve_msg").getValue(),
								op_type : Approve.OperatorType.Pass,
								success:function(){
									CommonUtil.closeMenuTab("task_"+task_id,null);
									//ApproveOperate.tabOptions.parentObj.loadData("待审批列表");
								}
							});
						}

					}
				});

			}else{
				Approve.approveOperator({
					task_id : task_id,
					specific_user : mini.get("user").getValue(),
					msg : mini.get("approve_msg").getValue(),
					op_type : Approve.OperatorType.Pass,
					success:function(){
						CommonUtil.closeMenuTab("task_"+task_id,null);
						//ApproveOperate.tabOptions.parentObj.loadData("待审批列表");
					}
				});
			}
		}); // end confirm
	});// end pass_btn

	//事件注册： 同意继续
	mini.get("continue_btn").on("click",function(){
		var approve_msg = mini.get("approve_msg");
		approve_msg.validate();
		if(!approve_msg.isValid()){
			return false;
		}

		mini.confirm("确认通过该笔审批吗？","确认",function (action) {
			if (action != "ok") {
				return;
			} 
			if(isNeedAuth){
				var authPageUrl = CommonUtil.baseWebPath() + '/mini_settle/MiniSettleAuthEdit.jsp?authKey1=' + serial_no + '&authType=' + authType;
				mini.open({
					url : authPageUrl,
					title : "请授权",
					width:300,
					height:150,
					onload : function(e){
						var param = {
							authKey1:serial_no,
							authType:authType
						};
						var iframe = this.getIFrameEl();
						try{
							if(iframe.contentWindow.init && typeof(iframe.contentWindow.init)=="function"){
								iframe.contentWindow.init(param);
							}
						}catch(e){
							
						}

					},
					ondestroy : function(action) {		
						if (action == "ok") 
						{
							Approve.approveOperator({
								task_id : task_id,
								specific_user : mini.get("user").getValue(),
								msg : mini.get("approve_msg").getValue(),
								op_type : Approve.OperatorType.Continue,
								success:function(){
									CommonUtil.closeMenuTab("task_"+task_id,null);
									//ApproveOperate.tabOptions.parentObj.loadData("待审批列表");
								}
							});
						}

					}
				});

			}else{
				Approve.approveOperator({
					task_id : task_id,
					specific_user : mini.get("user").getValue(),
					msg : mini.get("approve_msg").getValue(),
					op_type : Approve.OperatorType.Continue,
					success:function(){
						CommonUtil.closeMenuTab("task_"+task_id,null);
						//ApproveOperate.tabOptions.parentObj.loadData("待审批列表");
					}
				});
			}
		}); // end confirm
	});// end continue_btn

	//事件注册： 拒绝
	mini.get("nopass_btn").on("click",function(){
		var approve_msg = mini.get("approve_msg");
		approve_msg.validate();
		if(!approve_msg.isValid()){
			return false;
		}

		mini.confirm("确认拒绝该笔审批吗？","确认",function (action) {
			if (action != "ok") {
				return;
			} 
			Approve.approveOperator({
				task_id : task_id,
				specific_user : "",
				msg : mini.get("approve_msg").getValue(),
				op_type : Approve.OperatorType.NoPass,
				success:function(){
					CommonUtil.closeMenuTab("task_"+task_id,null);
					//ApproveOperate.tabOptions.parentObj.loadData("待审批列表");
				}
			});
		}); // end confirm
	});// end nopass_btn

	//事件注册： 驳回
	mini.get("back_btn").on("click",function(){
		var approve_msg = mini.get("approve_msg");
		approve_msg.validate();
		if(!approve_msg.isValid()){
			return false;
		}

		mini.confirm("确认驳回该笔审批吗？","确认",function (action) {
			if (action != "ok") {
				return;
			} 
			Approve.approveOperator({
				task_id : task_id,
				specific_user : "",
				msg : mini.get("approve_msg").getValue(),
				op_type : Approve.OperatorType.Back,
				success:function(){
					CommonUtil.closeMenuTab("task_"+task_id,null);
					//ApproveOperate.tabOptions.parentObj.loadData("待审批列表");
				}
			});
		}); // end confirm
	});// end back_btn

	//事件注册： 退回发起
	mini.get("redo_btn").on("click",function(){
		var approve_msg = mini.get("approve_msg");
		approve_msg.validate();
		if(!approve_msg.isValid()){
			return false;
		}

		mini.confirm("确认退回发起该笔审批吗？","确认",function (action) {
			if (action != "ok") {
				return;
			} 
			Approve.approveOperator({
				task_id : task_id,
				specific_user : "",
				msg : mini.get("approve_msg").getValue(),
				op_type : Approve.OperatorType.Redo,
				success:function(){
					CommonUtil.closeMenuTab("task_"+task_id,null);
					//ApproveOperate.tabOptions.parentObj.loadData("待审批列表");
				}
			});
		}); // end confirm
	});// end redo_btn
	
	//添加快捷信息
	mini.get("add_text_btn").on("click",function(e){
		var value = mini.get("approve_msg").getValue() + mini.get("add_text_combobox").getFormValue();
		mini.get("approve_msg").setValue(value);
	});
	
	
	//事件注册： 查看审批日志
	mini.get("log_btn").on("click",function(e){
		Approve.approveLog(flow_type,serial_no);
	});

	var messageid = mini.loading("正在获取流程信息……", "请稍后");
	var temp_json_data = null;
	CommonUtil.ajax({
		url:"/FlowController/getOneFlowDefineBaseInfo",
		data:{serial_no:serial_no},
		callerror: function(data){
			mini.hideMessageBox(messageid);
			ApproveOperate.loadLogInfo();
		},
		callback : function(data) {
			mini.hideMessageBox(messageid);

			temp_flow_info = data.obj;
			var innerInterval;
			innerInitFunction = function(){
				clearInterval(innerInterval);
				ApproveOperate.loadLogInfo();

				if(typeof(FlowDesigner) != 'undefined'){
					if($("#"+ApproveOperate.tabOptions.id).find("#log_tabs").tabs('getSelected').find('#FlowDesigner_div').length == 1){
						FlowDesigner.createAllNode($.parseJSON(data.obj.json_data));
					}
					else{
						temp_json_data = $.parseJSON(data.obj.json_data);
					}
				}
			},
			innerInterval = setInterval("innerInitFunction()",100);
		}
	});
	
	/***
	$("#"+ApproveOperate.tabOptions.id).find("#log_tabs").tabs({
		onSelect:function(title,index){
			if(index == 1 && temp_json_data != null){
				FlowDesigner.createAllNode(temp_json_data);
				temp_json_data = null;
				
				FlowDesigner.addApporveLog(temp_log_data);
				temp_log_data = null;
			}
		}
	});***/
	
	//加签
	mini.get("skip_btn").on("click",function(){
		var approve_msg = mini.get("approve_msg");
		approve_msg.validate();
		if(!approve_msg.isValid()){
			return false;
		}

		mini.open({
			url : CommonUtil.baseWebPath() +'/base/flow/ApproveOperateSkip.jsp',
			title : "指派加签人",
			height:450,
		    width:660,
			onload : function(e){
				var param = {
					authKey1:serial_no,
					authType:authType
				};
				var iframe = this.getIFrameEl();
				try{
					if(iframe.contentWindow.init && typeof(iframe.contentWindow.init)=="function"){
						iframe.contentWindow.init(param);
					}
				}catch(e){
					
				}

			},
			ondestroy : function(action) {		
				if (action == "ok") 
				{
					CommonUtil.ajax({
						data:{"task_id":task_id,"user_id":record},
						url:"/FlowController/invitationWithTask",
						callback:function(data){
							$.messager.alert("提示","操作成功");
							CommonUtil.closeTab(null,"task_"+task_id);
							ApproveOperate.tabOptions.parentObj.loadData("待审批列表");
						}
					});
				}
			}
		});

		/***
		CommonUtil.dialog({
			title:"指派加签人",
		    modal:true,
		    resizable:true,
		    height:450,
		    width:660,
		    urls:'./base/flow/ApproveOperateSkip.jsp',
		    onCloseed:function(record){
				if(record){
					CommonUtil.ajax({
						data:{"task_id":task_id,"user_id":record},
						url:"/FlowController/invitationWithTask",
						callback:function(data){
							$.messager.alert("提示","操作成功");
							CommonUtil.closeTab(null,"task_"+task_id);
							ApproveOperate.tabOptions.parentObj.loadData("待审批列表");
						}
					});
				}
		    }
		});
		***/
	});// end skip_btn
	

});

</script>
