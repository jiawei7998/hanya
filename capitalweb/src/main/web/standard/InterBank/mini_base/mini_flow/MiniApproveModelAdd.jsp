<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>

<%@ include file="../../../global.jsp"%> 
<html>
<head>
    <title>审批模型新增</title>
</head>
<body>
    <div>
        <span id="labell"><a href="javascript:CommonUtil.activeTab();">审批模型</a> >> 新增</span>
        <div id="field" class="fieldset-body">
            <table id="field_form"  width="100%" class="mini-table">
                <tr>
                    <td>
                        <input id="name" name="name" class="mini-textbox"  labelField="true"  
                        label="名称" emptyText="请输入名称" vtype="maxLength:30" required="true"
                        style="width:250px;" labelStyle="width:40px;"/>
                    </td>
                    <td>
                        <input id="key" name="key" class="mini-textbox"  labelField="true" 
                         label="KEY" emptyText="请输入KEY" vtype="maxLength:30" required="true"
                         style="width:250px;" labelStyle="width:40px;"/>
                    </td>
                    <td>
                        <input id="description" name="description" class="mini-textarea" 
                            labelField="true"  label="描述" emptyText="请输入描述" vtype="maxLength:30"
                            style="width:250px;" labelStyle="width:40px;" />
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="text-align:center;">
                        <a  id="save_btn" class="mini-button" style="display: none"    onclick="save">创建</a>
                        <a id="cancel_btn" class="mini-button" style="display: none"    onclick="cancel">取消</a>
                    </td>
                </tr>
                
            </table>
        </div>
    </div>
    
    <script>

        mini.parse();

        //创建
        function save(){
            //验证表单
            var form = new mini.Form("field_form");
            form.validate();
	        if (form.isValid() == false){//表单验证
                mini.alert("请输入有效数据！","系统提示");
                return;
            }

            //获取表单数据 
            var name = mini.get("name").getValue();
            var key = mini.get("key").getValue();
            var description = mini.get("description").getValue();
           
            var url = baseUrl + 'slbpm/model/createModel?name=' + name + '&key=' + key + '&description='+ description;
            window.open(url);

            //关闭并刷新
			setTimeout(function(){ top["win"].closeMenuTab() },100);
        }
        
        //取消按钮
        function cancel(){
            window.CloseOwnerWindow();
        }
    
    </script>




</body>
</html>   