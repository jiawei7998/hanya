<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../../global.jsp"%>

<body style="width:100%;height:100%;background:white">
    <div id="toolbar1" class="mini-toolbar" style="margin-left: 20px;">
        <table style="width:100%;">
            <tr>
            <td style="width:100%;">
                <a id="search_btn" class="mini-button" style="display: none"     onclick="search(10,0)">刷新</a>
                <a id="add_btn" class="mini-button" style="display: none"    onclick="add">新增</a>
                <a id="edit_btn" class="mini-button" style="display: none"   onclick="modifyedit()">修改</a>
                
            </td>
            </tr>
        </table>
    </div>
<div class="mini-fit" style="margin-top: 2px;">
    <div id="model_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" 
        idField="id"  pageSize="10" showPager="false"	sortMode="client" allowAlternating="true">
        <div property="columns" >
            <div type="indexcolumn" headerAlign="center" align="center" width="40">序号</div>
            <div field="id" width="60" headerAlign="center" align="center" allowSort="true" dataType="int">模型ID</div>    
            <div field="name"  width="140" headerAlign="center">模型名称</div>
            <div field="key" width="60" headerAlign="center" align="center">模型关键字</div>    
            <div field="version"  width="50" headerAlign="center" align="center" allowSort="true">模型版本</div>  
            <div field="createTime" width="110" headerAlign="center" align="center" allowSort="true">创建时间</div>    
            <div field="lastUpdateTime"  width="110" headerAlign="center" align="center" allowSort="true">更新时间</div>  
            <div field="metaInfo" width="160" headerAlign="center">详情</div> 
            <div field="option" width="70" headerAlign="center" align="center">操作</div>   
                                         
                                                                                                         
        </div>
    </div>
</div>
</body>
<script type="text/javascript">
    mini.parse();
    $(document).ready(function(){
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
            var grid = mini.get("model_grid");
            grid.on("beforeload", function (e) {
                e.cancel = true;
                var pageIndex = e.data.pageIndex;
                var pageSize = e.data.pageSize;
                search(pageSize, pageIndex);
            });
            search(grid.pageSize, 0);
        });
    });

    
    function search(pageSize,pageIndex){
        CommonUtil.ajax({
            url:"/FlowModelController/getModelList",
            data:{},
            callback:function(result){
                var data = result.modelList;
                var groupList = [];
                for(var i=0;i<data.length;i++){
                    var group = data[i];
                    var a = {
                        "id":group.id,
                        "name":group.name,
                        "key":group.key,
                        "version":group.version,
                        "createTime":formatDateTime(group.createTime),
                        "lastUpdateTime":formatDateTime(group.lastUpdateTime),
                        "metaInfo":group.metaInfo,
                        "option":'<a  class="mini-button" style="display: none"    onclick="showDeployModelDiv('+group.id+')">部署</a>'+
                                '&nbsp;&nbsp;<a  class="mini-button" style="display: none"    onclick="deleteModel('+group.id+')">删除</a>',

                    };

		            groupList.push(a);
                }
                    
                var grid =mini.get("model_grid");
                //设置分页
                grid.setTotalCount(result.modelList.length);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                //设置数据
                grid.setData(groupList);
            }
        });
    }

    //时间戳转年月日 时分秒
    function formatDateTime(inputTime) {
        if(inputTime){
            var date = new Date(inputTime);
            var y = date.getFullYear();
            var m = date.getMonth() + 1;
            m = m < 10 ? ('0' + m) : m;
            var d = date.getDate();
            d = d < 10 ? ('0' + d) : d;
            var h = date.getHours();
            h = h < 10 ? ('0' + h) : h;
            var minute = date.getMinutes();
            var second = date.getSeconds();
            minute = minute < 10 ? ('0' + minute) : minute;
            second = second < 10 ? ('0' + second) : second;
            return y + '-' + m + '-' + d+' '+h+':'+minute+':'+second;
        }else{
            return "";
        }
    }

    //新增模型
    function add(){
        var url = CommonUtil.baseWebPath() + "/mini_base/mini_flow/MiniApproveModelAdd.jsp?action=new";
        var tab = { id: "ApproveModelManageAdd", name: "ApproveModelManageAdd", text: "模型新增", url: url,showCloseButton:true,parentId:top['win'].tabs.getActiveTab().name };
        top['win'].showTab(tab);
    }

    //修改模型
    function modifyedit(){
        var grid = mini.get("model_grid");
        var  record =grid.getSelected();
        if(record){
            var url = baseUrl+ "slbpm/modeler.jsp?modelId="+record.id;
            window.open(url);
            
        }else{
            mini.alert("请选中一条记录","系统提示"); 
        }
    }

    //删除模型
    function deleteModel(id){
        var grid =mini.get("model_grid");
        mini.confirm("是否需要删除ID为" + id + "的模型？","系统提示",function(value){
            if (value == "ok"){   
                   
                CommonUtil.ajax({
                    url : '/FlowModelController/deleteModel',
                    data : {"modelId":id},
                    callback : function(data){
                        if(data){
                            mini.alert("删除成功！","系统提示");
                            search(grid.pageSize,0);
                        }
                    }
                });
                
            }   
        });
        
        
    }

    //打开部署页面
    function showDeployModelDiv(modelid){
        var url = CommonUtil.baseWebPath() + "/mini_base/mini_flow/MiniApproveModelDeploy.jsp?type=deploy&modelid="+modelid;
        var tab = { id: "approveModelManageDeploy", name: "approveModelManageDeploy", text: "模型部署", url: url,showCloseButton:true,parentId:top['win'].tabs.getActiveTab().name };
        top['win'].showTab(tab);


    }
</script>
