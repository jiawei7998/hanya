<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../../global.jsp"%> 
<html>
<head>
    <title>审批角色定义</title>
</head>
<body style="width:100%;height:100%;background:white">
<div class="mini-fit" style="width:100%;height:100%;">
    <div class="mini-splitter" style="width:100%;height:100%;" allowResize="false" borderStyle="border:solid 1px #aaa;">
    <div size="50%">
        <fieldset>
            <legend><label>审批角色</label></legend>
                <div id="search_form"  >
                    <input id="role_name" name="role_name" class="mini-textbox" vtype="maxLength:100" labelField="true"  label="角色名称：" style="width:310px;" emptyText="请输入角色名称" labelStyle="text-align:right;"/>
                    <span style="float:right;margin-right: 60px">
                        <a id="search_btn" class="mini-button" style="display: none"   onclick="search(10,0)">查询</a>
                        <a id="clear_btn" class="mini-button" style="display: none"    onClick="clear">清空</a>
                    </span>
                </div>
               
        </fieldset>
        <span style="margin:2px;display: block;">
                <a id="add_btn" class="mini-button" style="display: none"    onclick="add">新增角色</a>
                <a id="edit_btn" class="mini-button" style="display: none"    onclick="modifyedit()">修改角色</a>
        </span>
        <div class="mini-fit">
            <div id="role_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" 
                idField="p_code" allowResize="true" pageSize="10"  allowAlternating="true"
                onselectionchanged="onSelectionChangedSearchUser"
                onRowdblclick="custDetail" sortMode="client">
                    <div property="columns" >
                        <div type="indexcolumn" align="center" headerAlign="center">序号</div>
                        <div field="role_id" width="60" align="center" headerAlign="center" allowSort="true">角色ID</div>    
                        <div field="role_name"  width="100" headerAlign="center">角色名称</div>
                        <div field="memo"  width="150" headerAlign="center">备注</div>                                                                                                                        
                    </div>
            </div>
        </div>
    </div>
    <div size="50%">
        <fieldset>
            <legend><label>审批用户关联</label></legend>
            <div id="search_inst" >
                <input id="instId" name="instId" class="mini-buttonedit" allowInput="false" onbuttonclick="onInsQuery" labelField="true"  label="所属机构：" style="width:310px;" emptyText="请选择所属机构" labelStyle="text-align:right;"/>
                <span style="float:right;margin-right: 60px">
                    <a id="search_btn" class="mini-button" style="display: none"   onclick="search2(10,0)">查询</a>
                    <a id="clear_btn" class="mini-button" style="display: none"    onClick="clear2">清空</a>
                </span>
                
            </div>
            <div>
                    
                <a id="user_add_btn" class="mini-button" style="display: none"   onclick="add_user">新增角色用户</a>
                <a id="user_delete_btn" class="mini-button" style="display: none"    onclick="deleteUsers">删除角色用户</a>
                <!-- <a  id="save_btn" class="mini-button" style="display: none"    onclick="save">保存角色用户</a> -->
                   
            </div>
        </fieldset>
        <div class="mini-fit">
            <div id="user_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" allowAlternating="true"
                idField="p_code" allowResize="true" pageSize="10" multiSelect="true" sortMode="client">
                <div property="columns" >
                    <div type="indexcolumn" align="center" headerAlign="center">序号</div>
                    <div type="checkcolumn"></div>
                    <div field="userId" width="70" align="center" headerAlign="center" allowSort="true">用户代码</div>    
                    <div field="userName" width="50" headerAlign="center">用户名称</div>
                    <div field="instName"  width="150" headerAlign="center">机构名称</div>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
</body>
<script>
    mini.parse();

    top["flowRole"]=window;
    function getData(){
        var grid = mini.get("role_grid");
        var record =grid.getSelected(); 
        return record;
    }

    var user_grid =mini.get("user_grid");
    //新增角色
    function add(){
        var url = CommonUtil.baseWebPath() + "/mini_base/mini_flow/MiniFlowRoleEdit.jsp?action=new";
        var tab = { id: "flowRoleManageAdd", name: "flowRoleManageAdd", text: "角色新增", url: url,showCloseButton:true,parentId:top['win'].tabs.getActiveTab().name };
        top['win'].showTab(tab);
    }
    //修改
    function modifyedit(){
        var grid = mini.get("role_grid");
        var  record =grid.getSelected();
        if(record){
            var url = CommonUtil.baseWebPath() + "/mini_base/mini_flow/MiniFlowRoleEdit.jsp?action=edit";
            var tab = { id: "flowRoleManageEdit", name: "flowRoleManageEdit", text: "角色修改", url: url,showCloseButton:true,parentId:top['win'].tabs.getActiveTab().name };
            top['win'].showTab(tab);
        }else{
            mini.alert("请选中一条记录","系统提示"); 
        }
    }

    //双击一行打开详情信息
    function custDetail(e){
        var grid = e.sender;
        var row = grid.getSelected();
        if(row){
            var url = CommonUtil.baseWebPath() + "/mini_base/mini_flow/MiniFlowRoleEdit.jsp?action=detail";
            var tab = { id: "flowRoleManageDetail", name: "flowRoleManageDetail", text: "角色详情", url: url,showCloseButton:true,parentId:top['win'].tabs.getActiveTab().name };
            top['win'].showTab(tab);
        }
    }

    

     //查询(带分页)
     function search(pageSize,pageIndex){
            var form =new mini.Form("search_form");
            form.validate();
            if (form.isValid() == false) return;//表单验证
            var data =form.getData();//获取表单数据
            data['pageNumber']=pageIndex+1;
            data['pageSize']=pageSize;
            var param = mini.encode(data); //序列化成JSON
            CommonUtil.ajax({
                url:"/FlowRoleController/listFlowRolePage",
                data:param,
                callback:function(data){
                        
                    var grid =mini.get("role_grid");
                    //设置分页
                    grid.setTotalCount(data.obj.total);
                    grid.setPageIndex(pageIndex);
                    grid.setPageSize(pageSize);
                    //设置数据
                    grid.setData(data.obj.rows);
                }
            });
        }
        /* function search2(pageSize,pageIndex){
            var form=new mini.Form("#search_inst");
            form.validate();
            if(form.isValid) return false;
            var data=form.getData();
            data['flowRoleId']=row.role_id;
            var param=mini.encode(data);
            CommonUtil.ajax({
                url:"/UserController/selectUserWithRoleName",
                data:param,
                callback : function(data){
                    var grid =mini.get("user_grid");
                    //设置分页
                    grid.setTotalCount(data.obj.total);
                    grid.setPageIndex(pageIndex);
                    grid.setPageSize(pageSize);
                    //设置数据
                    grid.setData(data.obj);
                }
            })
        }
 */
       

        var row="";
        function searchUser(pageSize,pageIndex){
            var user_grid =mini.get("user_grid");
            var form =new mini.Form("search_inst");
            var data = form.getData();//获取机构数据
            data["flowRoleId"]=row.role_id;//角色id
            data['branchId']=top['win'].branchId;
            data['pageNumber']=pageIndex+1;
            data['pageSize']=pageSize;
            
            var param = mini.encode(data);

            CommonUtil.ajax( {
                url:"/UserController/selectUserByRoleNameAndInstId",
                data:param,
                callback : function(data) {
                    var grid =mini.get("user_grid");
                    //设置分页
                    grid.setTotalCount(data.obj.total);
                    grid.setPageIndex(pageIndex);
                    grid.setPageSize(pageSize);
                    //设置数据
                    grid.setData(data.obj.rows);
                }
            });

        }

        //角色选中=》出现用户
        function onSelectionChangedSearchUser(e){
            var role_grid = e.sender;
            var record = role_grid.getSelected();
            if(!record){
			     return false;
		    }
            row=record;
            var user_grid =mini.get("user_grid");
            searchUser(user_grid.pageSize,0);
        }

         //清空
         function clear(){
            var form=new mini.Form("search_form");
            form.clear();
            search(10,0);
            
        }
        //清空
        function clear2(){
            var  form=new mini.Form("#search_inst");
            form.clear();
        }
        function search2(){
            searchUser(user_grid.pageSize,0);
        }

        function onInsQuery(e) {
			var btnEdit = this;
			mini.open({
				url : CommonUtil.baseWebPath() +"/mini_system/MiniInstitutionSelectManages.jsp",
				title : "机构选择",
				width : 900,
				height : 600,
				ondestroy : function(action) {	
					if (action == "ok") 
					{
						var iframe = this.getIFrameEl();
						var data = iframe.contentWindow.GetData();
						data = mini.clone(data); //必须
						if (data) {
							btnEdit.setValue(data.instId);
							btnEdit.setText(data.instName);
							btnEdit.focus();
						}

                        //searchUser(user_grid.pageSize,0);
					}

				}
			});
		}

        //新增用户
        function add_user(){
            var grid = mini.get("role_grid");
            var roleRow = grid.getSelected();
            if(!roleRow){
                mini.alert("请选择一个审批角色!","系统提示");
                return false;
            }
        
             //弹出子页面
             mini.open({
                 url:CommonUtil.baseWebPath()+"/mini_system/MiniUserDualMiniManage.jsp",
                 title: "审批成员新增", 
                 width: "790px", 
                 height: "520px",
                 allowResize: true,       //允许尺寸调节
                 allowDrag: true,         //允许拖拽位置
                 showCloseButton: true,   //显示关闭按钮
                 showMaxButton: true, 
                 onload: function () {
                     var iframe = this.getIFrameEl();
                     var data = { action: "new" };
                     iframe.contentWindow.SetData(data);
                 },
                 ondestroy: function (action) {
                     if(action=='add'){
                        var iframe = this.getIFrameEl();
                        //获取新添加的用户
                        var usersData=iframe.contentWindow.GetUsersData();
                        usersData = mini.clone(usersData);
                        var dataAll =new Array();
                        var grid =mini.get("user_grid");
                        var oldUser=grid.getData();
                        for(var i=0;i<oldUser.length;i++){
                            dataAll.push(oldUser[i]);
                        }

                        for(var i=0;i<usersData.rows.length;i++){
                            dataAll.push(usersData.rows[i]);
                        }
                        //校验新增加的用户是否已存在
                        // var flag=check(dataAll);
                        // if(flag>0){
                        //     return;
                        // }
                        grid.setData(dataAll);
                        save();
                     }
                }
            });
        
        }
        //校验数组中重复的数据
        function check(data){
            var flag = 0;
            for (var i = 0; i < data.length; i++) {
                var temp=data[i].userId;
                var count=0;
                for (var j = i+1; j < data.length; j++) {
                    var temp2=data[j].userId;
                    if(temp==temp2){
                        mini.alert("用户"+data[j].userName+"[用户代码"+data[j].userId+"]"+"已存在!","系统提示");
                        flag ++;
                    }
                }
            }
            return flag;
        }
        

        //删除用户
        function deleteUsers(){
            var grid = mini.get("user_grid"); 
            var rows =grid.getSelecteds();
            if(rows.length<=0){
                mini.alert("请至少选择一条记录!","系统提示");
                return; 
            }
            var userIds="";
            $.each(rows,function(i,item){
                userIds += item.userId;
                userIds += i==rows.length-1?"":",";
            });
            var role_grid = mini.get("role_grid");
            var roleRow = role_grid.getSelected();
            if(!roleRow){
                mini.alert("请选择一个审批角色!","系统提示");
                return false;
            }
            var param = {role_id:roleRow.role_id,user_ids:userIds};
            // grid.removeRows(rows,true);
            CommonUtil.ajax({
                url:"/FlowRoleController/deleteRoleUserMap",
                data:param,
                callback:function(data){
                    if('error.common.0000' == data.code){
                        mini.alert("保存成功.");
                        search2(10,0);
                
                    }else{
                        mini.alert(data.desc);
                    }	
                }
            });
        }

        //保存
        function save(){
            var user_grid = mini.get("user_grid"); 
            var userData = user_grid.getData();
            if(userData == null || userData == ''){
                mini.alert("请选择要保存的用户!","系统提示");
                return; 
            }
            var userIds="";
            $.each(userData,function(i,item){
                userIds += item.userId;
                userIds += i==userData.length-1?"":",";
            });

            var role_grid = mini.get("role_grid");
            var roleRow = role_grid.getSelected();
            if(!roleRow){
                mini.alert("请选择一个审批角色!","系统提示");
                return false;
            }


            var param = {role_id:roleRow.role_id,user_ids:userIds};
            CommonUtil.ajax({
                url:"/FlowRoleController/modifyRoleUserMap",
                data:param,
                callback:function(data){
                    if('error.common.0000' == data.code){
                        mini.alert("保存成功.");
                        search2(10,0);
                
                    }else{
                        mini.alert(data.desc);
                    }	
                }
            });
        }

    $(document).ready(function(){
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
            var grid = mini.get("role_grid");
            grid.on("beforeload", function (e) {
                e.cancel = true;
                var pageIndex = e.data.pageIndex;
                var pageSize = e.data.pageSize;
                search(pageSize, pageIndex);
            });

            var user_grid = mini.get("user_grid");
            user_grid.on("beforeload", function (e) {
                e.cancel = true;
                var pageIndex = e.data.pageIndex;
                var pageSize = e.data.pageSize;
                searchUser(pageSize, pageIndex);
            });
            search(grid.pageSize, 0);
        });
    });

</script>

</html>