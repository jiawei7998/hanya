<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
	<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
    <title></title>
  </head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
	<legend>节点查询</legend>
<div id="AttachNodeManage" >   
	<div id="search_form" class="mini-form" width="80%">
		<input id="nodeNames" name="nodeNames" class="mini-textbox" labelField="true"  label="节点名称：" labelStyle="text-align:right;" />
		<input id="nodeId" name="nodeId" class="mini-textbox" labelField="true"  label="节点编号：" labelStyle="text-align:right;"  />
		<span style="float:right;margin-right: 150px">
			<a  id="search_btn" class="mini-button" style="display: none"   onclick="search(10,0)">查询</a>
			<a  id="clear_btn" class="mini-button" style="display: none"   onclick='clear()'>清空</a>
		</span>
	</div>
</div>
</fieldset>
<span style="margin:2px;display: block;">
	<a  id="add_btn" class="mini-button" style="display: none"   onclick="add()">新增</a>
	<a  id="edit_btn" class="mini-button" style="display: none"   onclick="edit()">修改</a>
	<a  id="delete_btn" class="mini-button" style="display: none"   onclick="del()">删除</a>
</span>	
<div id="AttachNodeManage" class="mini-fit">  
	<div id="datagrid"class="mini-datagrid borderAll" style="width:100%;height:100%;" sortMode="client" allowAlternating="true"
	idField="dealNo" allowResize="true" multiSelect="true" onrowdblclick="onRowDblClick" >
		<div property="columns" >
			<div type="indexcolumn" width="50" align="left"  headerAlign="center">序号</div>
			<div field="nodeId" width="70" align="left"  headerAlign="center" allowSort="true">节点编号</div>    
			<div field="nodeName" width="450" align="left"  headerAlign="center" allowSort="true">节点名称</div>    
			<div field="nodeMustFlag" width="60" align="center"  headerAlign="center" allowSort="true" renderer="CommonUtil.dictRenderer" data-options="{dict:'YesNo'}">是否必须</div>                            
			<div field="nodeAttsize" width="120" align="left" headerAlign="center" >文档大小</div>
			<div field="nodeAtttype" width="230" align="left" headerAlign="center" >文档类型</div>   
			<div field="nodeAttmark" width="180" align="left" headerAlign="center" >文档注释</div>
		</div>
	</div>
</div>
<script>
	mini.parse();
	top['attachNodeManage']=window;
	var row="";
	var grid = mini.get("datagrid");
	var form=new mini.Form("#search_form");
	//查询
	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});
	function search(pageSize,pageIndex){
		
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统也提示");
			return;
		}
		var data={};
		data['nodeName']=mini.get("nodeNames").getValue();
		data['nodeId']=mini.get("nodeId").getValue();
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		var url="/AttachNodeController/searchAttachNode";
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:url,
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	
	//查看详情
	function onRowDblClick(){
		var url=CommonUtil.baseWebPath() +"/mini_base/MiniAttachNodeEdit.jsp?action=detail";
		var tab={id:"AttachNodeDetail",name:"AttachNodeDetail",text:"节点详情",url:url,parentId:"11-1-3"};
		top["win"].showTab(tab);
	}
	// 传递参数
	function getData(action){
		row = grid.getSelected();
		return row;
	}
	/**************************事件定义********************************************/
	//清空
	function clear(){
		form.clear();
	}
	//删除
	function del(){
		var rows=grid.getSelecteds();
		if(rows.length==0){
			mini.alert("请选中一行","提示");
			return;
		}
		mini.confirm("您确认要删除选中记录?","系统警告",function(value){   
			if (value=='ok'){   
				var data={nodeId:rows[0].nodeId};
				params=mini.encode(data);
				CommonUtil.ajax( {
					url:"/AttachNodeController/deleteAttachNode",
					data:params,
					callback : function(data) {
						mini.alert("删除成功.","系统提示");
						search(10,0);
					}
				});
			}
		});
	}
	//修改
	function edit(){
		row = grid.getSelected();
		if(row){
			var url = CommonUtil.baseWebPath() +"/mini_base/MiniAttachNodeEdit.jsp?action=edit";
			var tab = {id:"AttachNodeEdit",name: "AttachNodeEdit",url:url,text:"节点修改",parentId:"11-1-3"};
			top["win"].showTab(tab);
		} else {
			mini.alert("请选中一条记录！","消息提示");
		}

	}
	//新增
	function add(e){
		var url=CommonUtil.baseWebPath() +"/mini_base/MiniAttachNodeEdit.jsp?action=add";
		var tab={id:"AttachNodeAdd",name:"AttachNodeAdd",text:"节点新增",url:url,parentId:"11-1-3"};
		top["win"].showTab(tab);

	}
	$(document).ready(function(){
		search(10,0);
	});
</script>
</body>
</html>