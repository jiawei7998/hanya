<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>

<%@ include file="../../global.jsp"%> 
<html>
<head>
    <title>字段维护</title> 
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script> 
</head>
<body>
    <div id="fd2" >
        <span id="labell"><a href="javascript:CommonUtil.activeTab();">字段</a> >> 修改</span>
        <div id="field" class="fieldset-body">
        <table id="field_form"  width="100%" class="mini-table">
            <tr>
                <td>
                    <input id="fldNo" name="fldNo" class="mini-textbox"  labelField="true"  
                    label="字段编号" emptyText="字段编号自动生成" enabled="false" width="300px"/>
                </td>
                <td>
                    <input id="fldName" name="fldName" class="mini-textbox"  labelField="true" 
                     label="字段名称" emptyText="请输入字段名称" required="true"
                      vtype="maxLength:100" onvalidation='onEnglishAndNumberValidation' width="300px"/>
                </td>
                <td><input id="fldType" name="fldType" class="mini-combobox" 
                    required="true"   data = "CommonUtil.serverData.dictionary.fldType"
                    emptyText="请选择字段种类" labelField="true"  label="字段种类" width="300px"/>
                </td>
            </tr>
            
            <tr>
                <td><input id="formName"name="formName" class="mini-textbox" 
                    required="true"  vtype="maxLength:50" labelField="true"  label="表单名称"
                    emptyText="请输入表单名称" width="300px"/>
                </td>
                <td><input id="formType" name="formType" class="mini-combobox" 
                    required="true"  data = "CommonUtil.serverData.dictionary.fldFormType"
                    emptyText="请选择字段表单类型"  onvaluechanged="onFormTypeChanged"
                    labelField="true"  label="字段表单类型" width="300px"/>
                </td>
                
                <td><input id="componet" name="componet" class="mini-combobox" 
                    data = "CommonUtil.serverData.dictionary.componet" enabled="false"
                    emptyText="请选择预定义组件" labelField="true"  label="预定义组件" width="300px"/>
                </td>
            </tr>
            <tr>
                
                <td><input id="isMandatory" name="isMandatory" class="mini-combobox"
                    required="true"  data = "CommonUtil.serverData.dictionary.YesNo"
                    emptyText="请选择是否必填" labelField="true"  label="是否必填" width="300px"/>
                </td>
                <td><input id="isDisabled"  name="isDisabled" class="mini-combobox"
                    required="true"  data = "CommonUtil.serverData.dictionary.YesNo"
                    emptyText="请选择是否禁用" labelField="true"  label="是否禁用" width="300px"/>
                </td>
                <td><input id="defValue" name="defValue" class="mini-textbox"
                    vtype="maxLength:128"  labelField="true"  label="默认值" emptyText="请输入默认值" 
                    width="300px"/>
               </td>

            </tr>
            <tr id="append_td">
                <td><input id="useFlag" name="useFlag" class="mini-combobox"
                    required="true"  data = "CommonUtil.serverData.dictionary.Status"
                    emptyText="请选择启用标志" labelField="true"  label="启用标志" width="300px"/>
                </td>
                <td><input id="formTips" name="formTips" class="mini-textarea"
                    vtype="maxLength:100"   height="84" labelField="true"  label="表单提示" 
                    emptyText="请输入表单提示" width="300px"/>
               </td>
               
            </tr>
           

        </table>
        <table id="append_form"  width="100%" class="mini-table">

        </table>
        <table class="btn_div" width="100%" class="mini-table">
            <tr>
                <td colspan="3" style="text-align:center;">
                    <a  id="save_btn" class="mini-button" style="display: none"    onclick="save">保存</a>
                    <a id="cancel_btn" class="mini-button" style="display: none"    onclick="cancel">取消</a>
                </td>
            </tr>
        </table>
        </div>
    </div>
    
    
    <script>
        $(document).ready(function(){
            mini.parse();
            init();
        });
        
        
        var url = window.location.search;
        var action = CommonUtil.getParam(url,"action");
        


        var formCols = "<td><input id='formCols' name='formCols' class='mini-spinner'  changeOnMousewheel='false' labelField='true'  label='表单占列'  required='true' minValue='1' maxValue='2'  width='300px'/></td>";
        var formRows = "<td><input id='formRows' name='formRows' labelField='true'  label='表单占行' class='mini-spinner' changeOnMousewheel='false'  required='true' minValue='1' maxValue='10' width='300px'/></td>";
        var formLength = "<td><input id='formLength' name='formLength' labelField='true'  label='表单长度' class='mini-spinner' changeOnMousewheel='false' required='true' minValue='1' maxValue='2000' width='300px'/></td>";
        var defKey = "<td><input id='defKey' name='defKey' class='mini-textbox' labelField='true'  label='字典项' emptyText='请输入字典项' required='true' width='300px' onvalidation='onEnglishAndNumberValidation' vtype='maxLength:32'></td>";
        var formMin = "<td><input id='formMin' name='formMin' labelField='true'  label='最小值' class='mini-spinner' changeOnMousewheel='false' required='true'  minValue='-100' maxValue='999999999999'  width='300px'/></td>";
        var formMax = "<td><input id='formMax' name='formMax' labelField='true'   label='最大值' class='mini-spinner' changeOnMousewheel='false' required='true' minValue='0' maxValue='999999999999'  width='300px'/></td>";
        var formPrecision = "<td><input id='formPrecision' name='formPrecision' labelField='true'   label='显示精度' class='mini-spinner' changeOnMousewheel='false' required='true'  minValue='0' maxValue='10'  width='300px'/></td>";
        var formMultiple = "<td><input id='formMultiple' name='formMultiple' class='mini-combobox' labelField='true'  label='是否多选' required='true' data = 'CommonUtil.serverData.dictionary.YesNo' emptyText='请选择是否多选'  width='300px'/></td>";
        var forValid = "<td><input id='formValid' name='formValid' labelField='true'   label='表单验证' class='mini-combobox' data = 'CommonUtil.serverData.dictionary.formValid' emptyText='请选择表单验证'width='300px'/></td>";
        var validParam = "<td><input id='validParam' name='validParam' labelField='true'   label='验证参数' class='mini-textbox' emptyText='请输入验证参数' vtype='maxLength:50' width='300px' /></td>";
        var formSplit = "<td><input id='formSplit' name='formSplit' labelField='true'  label='表单分割符' class='mini-textbox' emptyText='请输入表单分割符' vtype='rangeChar:0,1' width='300px'/></td> ";
        var formTimes = "<td><input id='formTimes' name='formTimes'  labelField='true'   label='数字倍数' class='mini-spinner' changeOnMousewheel='false'  minValue='0' maxValue='99999999' allowNull='true' value='null'  width='300px'/></td>";
        var defText = "<td><input id='defText' name='defText' labelField='true'   label='默认文本' class='mini-textbox' emptyText='请输入默认文本' vtype='maxLength:128' width='300px'/></td>";

        var deUrl = "";
        var empty = "<td><input  class='mini-hidden' label='数字倍数' labelField='true' width='300px'/></td><td><input  class='mini-hidden' label='数字倍数' labelField='true' width='300px'/></td>";
       
        
        
        function init(){
            
            if(action == "new"){
                $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>字段</a> >> 新增");
                
            }else if(action == "edit" || action == "detail"){
                var form1 = new mini.Form("field");
                var data=top["fieldManage"].getData();
               
                var table = $("#field").find("#append_form");
			    table.empty();
                //获取“字段表单类型”的值
                var formType = data.formType;
                var content=formTypeValueChange(formType);
                table.append(content);
                mini.parse();

                form1.setData(data);
                
                if(action == "detail"){
                    $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>字段</a> >> 详情");
                    form1.setEnabled(false);
                    document.getElementById("save_btn").style.display="none";
                    //document.getElementById("cancel_btn").style.display="none";
                   
                }
            }
        }
        

        //字段表单类型的值改变时发生
        function onFormTypeChanged(){
            var table = $("#field").find("#append_form");
			table.empty();
            //获取“字段表单类型”的值
            var formTypeValue =mini.get("formType").getValue();
			var content=formTypeValueChange(formTypeValue);
            if(content != 'null'){
                table.append(content); 
            }
            
            mini.parse();
        }

        function formTypeValueChange(type){
            var content;
            if (type == "textbox") {//文本框
               
				content = "<tr>" + formCols + formRows + formLength + "</tr>";
				content = content + "<tr>" + forValid + validParam + defText +"</tr>";
                mini.get("componet").disable();
				
			} else if (type == "numberbox") {//数字框
				content = "<tr>" + formMin + formMax + formPrecision + "</tr>";
				content = content + "<tr>" +  formCols + formSplit + formTimes + "</tr>";
                mini.get("componet").disable();
                
				
			} else if (type == "datebox") {//日期框
				content = "<tr style='float:left;margin-left:24px;'>" + formCols + empty +"</tr>";
                
                mini.get("componet").disable();
               
			} else if (type == "combobox") {//下拉框
				content = "<tr>" + defKey + formCols + defText +"</tr>";
                mini.get("componet").disable();
				
			} else if (type == "combotree") {//树形框
				content = "<tr>" + defKey + formMultiple + formCols + "</tr>";
				content = content + "<tr>" + defText + empty + "</tr>";
                mini.get("componet").disable();
            } else if (type == "buttonedit") {//弹出选择框
				content = "<tr>" + formCols + formRows + formLength + "</tr>";
				content = content + "<tr>" + forValid + validParam + defText +"</tr>";
                mini.get("componet").enable();
                
            }else{
                mini.get("componet").disable();
            }

            return content;
        }

        //保存
        function save(){
            var form = new mini.Form("field");
            form.validate();
	        if (form.isValid() == false){//表单验证
                mini.alert("请输入有效数据！","系统提示");
                return;
            } 
	        var data = form.getData();
            
            var param = mini.encode(data); //序列化成JSON
            var saveUrl="";
            if(action == "new"){
                saveUrl = "/FieldController/addField";
            }else if(action == "edit"){
                saveUrl = "/FieldController/editField";
            }
            CommonUtil.ajax({
				url:saveUrl,
                data:param,
            	callback:function(data){
					if('error.common.0000' == data.code){
				        mini.alert("保存成功!","系统提示",function(value){
                            //关闭并刷新
						    setTimeout(function(){ top["win"].closeMenuTab() },100);
                        });
                        
                    }else{
                        mini.alert("保存失败！","系统提示");
                    }	
                }
			});
        }
        //取消按钮
        function cancel(){
            window.CloseOwnerWindow();

        }

        function onEnglishAndNumberValidation(e) {
            if (e.isValid) {
                if (isEnglishAndNumber(e.value) == false) {
                    e.errorText = "只允许输入英文、数字、下划线";
                    e.isValid = false;
                }
            }
        }

        /* 是否英文+数字 */
        function isEnglishAndNumber(v) {
            
            var re = new RegExp("^[0-9a-zA-Z\_]+$");
            if (re.test(v)) return true;
            return false;
        }

       
    
    </script>
</body>
</html>