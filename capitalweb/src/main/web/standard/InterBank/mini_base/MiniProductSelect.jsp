
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>产品选择</title>
  </head>
  
<body style="width:100%;height:100%;background:white">
	<fieldset class="mini-fieldset">
	<legend>查询</legend>
	<div id="search_form" style="width:100%;">
		<input id="prdName" name="prdName" class="mini-textbox" labelField="true" label="产品名称：" labelStyle="text-align:right;" emptyText=""
		style="width:260px"/>
		<input id="prdInst" name="prdInst" class="mini-buttonedit searchbox" onbuttonclick="onInsQuery" labelField="true" label="所属机构：" labelStyle="text-align:right;" emptyText=""
		style="width:260px"/>
		<input id="prdType" name="prdType" class="mini-treeselect" textField="text" expandOnNodeClick="true" valueField="id" labelField="true" label="产品类型：" labelStyle="text-align:right;" emptyText=""
		style="width:260px"/>
		<span style="float:right;margin-right: 200px">
			<a id="search_btn" class="mini-button" style="display: none"   onclick="query">查询</a>
			<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear">清空</a>
		</span>
	</div>
	</fieldset>
	<div id="AttachTree" class="mini-fit" style="margin-top: 2px;">      
		<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" allowAlternating="true" allowResize="true" onrowdblclick="onRowDblClick" border="true" sortMode="client">
			<div property="columns">
				<div type="indexcolumn" headerAlign="center" width="40">序号</div>
				<div field="prdNo" width="80" allowSort="true" headerAlign="center" align="center">产品编号</div>
				<div field="prdInstName" width="150" allowSort="false" headerAlign="center" align="">所属机构</div>
				<div field="prdName" width="150" allowSort="false" headerAlign="center" align="">产品名称</div>
				<div field="prdTypeName" width="150" allowSort="false" headerAlign="center" align="">产品类型</div>
			</div>
		</div>  
	</div>

	<script>
		mini.parse();
		var grid = mini.get("datagrid");
		grid.on("beforeload", function (e) {
			e.cancel = true;
			var pageIndex = e.data.pageIndex; 
			var pageSize = e.data.pageSize;
			search(pageSize,pageIndex);
		});

		function search(pageSize,pageIndex)
		{
			
			var form = new mini.Form("#search_form");
			form.validate();
			if(form.isValid() == false){
				mini.alert("表单填写错误,请确认!","提示信息")
				return;
			}

			var data = form.getData();
			data['pageNumber'] = pageIndex+1;
			data['pageSize'] = pageSize;
			var params = mini.encode(data);
			CommonUtil.ajax({
				url:'/ProductController/searchPageProduct',
				data:params,
				callback : function(data) {
					var grid = mini.get("datagrid");
					grid.setTotalCount(data.obj.total);
					grid.setPageIndex(pageIndex);
					grid.setPageSize(pageSize);
					grid.setData(data.obj.rows);
				}
			});
		}
		
		function onInsQuery(e) {
				var btnEdit = this;
				mini.open({
					url: CommonUtil.baseWebPath() + "/mini_system/MiniInstitutionSelectManages.jsp",
					title: "机构选择",
					width: 700,
					height: 600,
					ondestroy: function (action) {
						if (action == "ok") {
							var iframe = this.getIFrameEl();
							var data = iframe.contentWindow.GetData();
							data = mini.clone(data);
							if (data) {
								btnEdit.setValue(data.instId);
								btnEdit.setText(data.instName);
								btnEdit.focus();
							}
						}

					}
				});
			}
		
		function selectPrdtype(){
			CommonUtil.ajax({
				url:'/ProductTypeController/searchProductType',
				data:{},
				callback:function(data){
					mini.get("prdType").setData(data.obj);
				}
			});
		}
		
		function GetData() {
				var row = grid.getSelected();
				return row;
			}

		function onRowDblClick(e) {
				CloseWindow("ok");
			}

		function CloseWindow(action) {
			if (window.CloseOwnerWindow)
				return window.CloseOwnerWindow(action);
			else
				window.close();
			}
			function checkBoxValuechanged() {
				search(grid.pageSize, 0);
			}

			
		function query(){
			search(grid.pageSize,0);
		}
		
		function clear(){
			var form = new mini.Form("#search_form");
			form.clear();
		}

		$(document).ready(function()
		{
			search(10,0);
			selectPrdtype();
		});
		
	</script>
</body>
</html>
