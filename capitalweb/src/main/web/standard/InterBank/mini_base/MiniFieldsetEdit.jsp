<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>

<%@ include file="../../global.jsp"%> 
<html>
<head>
    <title>字段分组维护</title>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script> 
	
</head>
<body>

    <div id="fd2">
        <span id="labell"><a href="javascript:CommonUtil.activeTab();">字段分组</a> >> 修改</span>
        <div id="field" class="fieldset-body">
        <table id="field_form"   width="100%" class="mini-table">
            <tr>
                <td>
                    <input id="fdsNo" name="fdsNo" class="mini-textbox"  labelField="true" 
                     label="字段分组编号"  emptyText='分组编号自动生成' enabled="false"
                     width="300px"/>
                </td>
                <td>
                    <input id="fdsName" name="fdsName" class="mini-textbox"  labelField="true" 
                     label="字段分组名称" emptyText='请输入字段分组名称' required="true"
                     vtype="maxLength:50" width="300px"/>
                </td>
                <td><input id="fdsType" name="fdsType" class="mini-combobox" 
                    required="true"   data = "CommonUtil.serverData.dictionary.fldType"
                    emptyText="请选择字段分组种类" labelField="true"  label="字段分组种类"
                    width="300px"/>
                </td>
                
            </tr>
            <tr>
                
                <td><input id="useFlag" name="useFlag" class="mini-combobox"
                    required="true"  data = "CommonUtil.serverData.dictionary.Status"
                    emptyText="请选择启用标志" labelField="true"  label="启用标志" value="1"
                    width="300px"/>
                </td>

            </tr>
            <tr>
                <td colspan="3" style="text-align:center;">
                    <a  id="save_btn" class="mini-button" style="display: none"    onclick="save">保存</a>
                    <a id="cancel_btn" class="mini-button" style="display: none"    onclick="cancel">取消</a>
                </td>
            </tr>
        </table>
        </div>
    </div>
    
    <script>
        $(document).ready(function(){
            mini.parse();
            init();
            
        });
        var url = window.location.search;
        var action = CommonUtil.getParam(url,"action");

        function init(){
            
            if(action == "new"){
                $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>字段分组</a> >> 新增");
                
            }else if(action == "edit" || action == "detail"){
                var form1 = new mini.Form("field");
                var data=top["fieldSetManage"].getData();
                form1.setData(data);
                
                if(action == "detail"){
                    $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>字段分组</a> >> 详情");
                    form1.setEnabled(false);
                    document.getElementById("save_btn").style.display="none";
                    //document.getElementById("cancel_btn").style.display="none";
                }
            }
        }

        
        
        //保存
        function save(){
            var form = new mini.Form("field");
            form.validate();
	        if (form.isValid() == false){//表单验证
                mini.alert("请输入有效数据！","系统提示");
                return;
            } 
	        var data = form.getData();
            var param = mini.encode(data); //序列化成JSON

            var saveUrl="";
            if(action == "new"){
                saveUrl = "/FieldsetController/addFieldset";
            }else if(action == "edit"){
                saveUrl = "/FieldsetController/editFieldset";
            }
            CommonUtil.ajax({
				url:saveUrl,
                data:param,
            	callback:function(data){
					if('error.common.0000' == data.code){
                        mini.alert("保存成功!","系统提示",function(value){
                            //关闭并刷新
						 setTimeout(function(){ top["win"].closeMenuTab() },100);
                        });
				
                    }else{
                        mini.alert("保存失败！","系统提示");
                    }	
                }
			});
        }
        //取消按钮
        function cancel(){
            window.CloseOwnerWindow();

        }
    </script>
</body>
</html>