<!-- 债券台账明细  -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title></title>
  </head>
  
<body style="width:100%;height:100%;background:white">
	<fieldset class="mini-fieldset title">
    	<legend>查询</legend>
	<div id="searchForm" style="width:100%;">
		<table id="search_form">
			<tr>
				<td><input id="sponInst" name="sponInst" class="mini-buttonedit" onbuttonclick="onInsQuery" labelField="true"  label="所属机构" style="width:100%;" emptyText="请选择机构"/></td>
				<td><input id="dealNo" name="dealNo" class="mini-textbox" vtype="maxLength:50" labelField="true"  label="交易单号" style="width:100%;" emptyText="请输入交易单号"/></td>
				<td><input id="cNo" name="cNo" class="mini-textbox" labelField="true"  label="客户号" style="width:100%;" emptyText="请输入客户号"/></td>
			</tr>
			<tr>
				<td><input id="amtFrom" name="amtFrom" class="mini-textbox" vtype="float" labelField="true"  label="本金区间从" style="width:100%;" emptyText="请输入金额起始区间"/></td>
				<td><input id="amtTo" name="amtTo" class="mini-textbox" vtype="float" labelField="true"  label="到" style="width:100%;" emptyText="请输入金额到期区间"/></td>
				<td><input id="approveStatus" name="approveStatus" class="mini-combobox" data = "CommonUtil.serverData.dictionary.ApproveStatus" labelField="true"  label="审批状态" style="width:100%;" emptyText="请选择审批单状态"></td>
			</tr>
		</table>
		<div id="toolbar1" class="mini-toolbar" style="margin-left: 20px;">
			<table style="width:100%;">
				<tr>
				<td style="width:100%;">
					<a id="search_btn" class="mini-button" style="display: none"  onclick="query">查询</a>
		    		<a id="add_btn" class="mini-button" style="display: none"  onclick="add">新增</a>
					<a id="clear_btn" class="mini-button" style="display: none"  onclick="clear">清空</a>
					<a id="edit_btn" class="mini-button" style="display: none"  onclick="update">修改</a>
					<a id="delete_btn" class="mini-button" style="display: none"  onclick="del">删除</a>
					<a id="verify_commit_btn" class="mini-button" style="display: none"  onclick="verify">放款</a>
					<a id="verify_mine_commit_btn" class="mini-button" style="display: none"  onclick="approveCommit">提交正式审批</a>
					<a id="approve_log" class="mini-button" style="display: none"  onclick="approveLog">审批日志</a>
					<a id="print_bk_btn" class="mini-button" style="display: none"  onclick="print">打印</a>
				</td>
				</tr>
			</table>
		</div>
	</div>
	<div id = "approveType" name = "approveType" labelField="true" label="审批列表选择:"  class="mini-checkboxlist"  repeatItems="0" repeatLayout="flow" 
	  	value="approve" textField="text" valueField="id" multiSelect="false" style="margin-left: 20px;margin-top: 5px;"
	  data="[{id:'approve',text:'待审批列表'},{id:'finished',text:'已审批列表'},{id:'mine',text:'我发起的'}]" onvaluechanged="checkBoxValuechanged">
	</div>
	</fieldset>
	<div id="tradeManage" class="mini-fit" style="margin-top: 2px;">      
		<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="dealNo" allowAlternating="true" allowResize="true" onrowdblclick="onRowDblClick" border="true"  frozenStartColumn="0" frozenEndColumn="1">
			<div property="columns">
				<div type="indexcolumn" headerAlign="center" width="50">序号</div>
				<div field="dealNo" width="180" headerAlign="center" allowSort="true">交易单号</div>   
				<div field="dealType" width="70" renderer="CommonUtil.dictRenderer" data-options="{dict:'DealType'}" align="center" headerAlign="center">审批类型</div>
				<div field="approveStatus" width="70" renderer="CommonUtil.dictRenderer" data-options="{dict:'ApproveStatus'}" align="center" headerAlign="center">审批状态</div>
				<div field="counterParty.party_name" width="140" headerAlign="center" allowSort="true">客户名称</div>   
				<div field="prdName" width="140" headerAlign="center" allowSort="true">产品名称</div>   
				<div field="aDate" width="100" headerAlign="center" allowSort="true">审批开始日期</div>   
				<div field="ccy" width="80" headerAlign="center" allowSort="true">币种</div>   
				<div field="amt" numberFormat="#,0.00" align="right" width="80" allowSort="true">本金</div>
				<div field="rateType" width="60" renderer="CommonUtil.dictRenderer" data-options="{dict:'rateType'}" align="center" headerAlign="center">利率类型</div>
				<div field="contractRate" numberFormat="p" align="right" width="80" allowSort="true">合同利率(%)</div>
				<div field="acturalRate" numberFormat="p" align="right" width="80" allowSort="true">实际利率(%)</div>
				<div field="vDate" width="80" headerAlign="center" allowSort="true">起息日期</div>   
				<div field="mDate" width="80" headerAlign="center" allowSort="true">到期日期</div>   
				<div field="user.userName" width="100" headerAlign="center" allowSort="true">审批发起人</div>   
				<div field="institution.instName" width="120" headerAlign="center" allowSort="true">审批发起机构</div>   
				<div field="lastUpdate" width="130" headerAlign="center" allowSort="true">更新时间</div>
			</div>
		</div>  
	</div>


	<script>
		var url = window.location.search;
		var prdNo = CommonUtil.getParam(url,"prdNo");
		var prdName = CommonUtil.getParam(url,"prdName");

		//初始化方法 页面加载完成后被调用
		function init(param){

		}
		
		mini.parse();

		var grid = mini.get("datagrid");
		grid.on("beforeload", function (e) {
			e.cancel = true;
			var pageIndex = e.data.pageIndex; 
			var pageSize = e.data.pageSize;
			search(pageSize,pageIndex);
		});

		function search(pageSize,pageIndex)
		{
			var form = new mini.Form("#search_form");
			form.validate();
			if(form.isValid() == false){
				mini.alert("表单填写错误,请确认!","提示信息")
				return;
			}

			var data = form.getData();
			data['pageNumber'] = pageIndex+1;
			data['pageSize'] = pageSize;
			data['prdNo'] = prdNo;
			data['dealType'] = '2';
			var url = null;

			var approveType = mini.get("approveType").getValue();
			if(approveType == "mine"){
				url = "/ProductApproveController/searchPageProductApproveByMyself";

			}else if(approveType == "approve"){
				url = "/ProductApproveController/searchPageProductApproveUnfinished";

			}else{
				url = "/ProductApproveController/searchPageProductApproveFinished";
			}

			var params = mini.encode(data);
			CommonUtil.ajax({
				url:url,
				data:params,
				callback : function(data) {
					var grid = mini.get("datagrid");
					grid.setTotalCount(data.obj.total);
					grid.setData(data.obj.rows);
				}
			});
			
		}// end search

		function query(){
			search(grid.pageSize,0);
		}

		function checkBoxValuechanged(){
			search(grid.pageSize,0);
		}

			
		function onRowDblClick(e) {
			var row = grid.getSelected();
			if(row){
				var url = CommonUtil.baseWebPath() + "/mini_trade/MiniTradeEditTabs.jsp?prdNo=" + prdNo + "&prdName=" + prdName + "&prdProp=1&prdRootType=3000&operType=D&dealNo="+row.dealNo;
				var tab = {"id": "trade_detail_2_" + prdNo, iconCls: "fa fa-send-o", text: prdName + "_放款审批详情" ,url:url,innerText:prdName + "_放款审批详情"};
				top["win"].showTab(tab);

			} else {
				mini.alert("请选中一条记录！","消息提示");
			}
		}

		function add(){
			var url = CommonUtil.baseWebPath() + "/mini_trade/MiniTradeEditTabs.jsp?prdNo=" + prdNo + "&prdName=" + prdName + "&prdProp=1&prdRootType=3000";
			var tab = {"id": "trade_add_2_" + prdNo, iconCls: "fa fa-send-o", text: prdName + "_放款审批新增" ,url:url,innerText:prdName + "_放款审批新增"};
			top["win"].showTab(tab);
		}

		function update()
		{
			var row = grid.getSelected();
			if(row){
				var url = CommonUtil.baseWebPath() + "/mini_trade/MiniTradeEditTabs.jsp?prdNo=" + prdNo + "&prdName=" + prdName + "&prdProp=1&prdRootType=3000&operType=U&dealNo="+row.dealNo;
				var tab = {"id": "trade_update_2_" + prdNo, iconCls: "fa fa-send-o", text: prdName + "_放款审批修改" ,url:url,innerText:prdName + "_放款审批新增"};
				top["win"].showTab(tab);

			} else {
				mini.alert("请选中一条记录！","消息提示");
			}
		}

		function clear(){
			var form = new mini.Form("#search_form");
			form.clear();
		}
		//删除
		
		function del(){
			
		}

		//放款
		function verify(){
			
		}

		//提交正式审批
		function approveCommit(){
			
		}

		//审批日志
		function approveLog(){
			
		}

		//打印
		function print(){
			
		}

		function onInsQuery(e) {
			var btnEdit = this;
			mini.open({
				url : CommonUtil.baseWebPath() +"/mini_system/MiniInstitutionSelectManages.jsp",
				title : "机构选择",
				width : 900,
				height : 600,
				ondestroy : function(action) {	
					if (action == "ok") 
					{
						var iframe = this.getIFrameEl();
						var data = iframe.contentWindow.GetData();
						data = mini.clone(data); //必须
						if (data) {
							btnEdit.setValue(data.instId);
							btnEdit.setText(data.instName);
							btnEdit.focus();
						}
					}

				}
			});
		}

		$(document).ready(function()
		{
			search(10,0);
		});
		
	</script>
</body>
</html>
