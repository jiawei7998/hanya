<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
	<%@ include file="../../../global.jsp"%>
		<html>
		<head>
			<script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
			<title>报表查询</title>
		</head>

		<body style="width:100%;height:100%;background:white">
			<fieldset class="mini-fieldset">
				<legend>查询</legend>
				<div id="search_form" style="width:100%;">
					<input id="title_search" name="title_search" class="mini-textbox" labelField="true" label="报表标题：" style="width:300px;" emptyText="请输入报表标题"
					/>
					<input id="branch_id" class="mini-hidden" name="branch_id" value="<%=__sessionUser.getBranchId()%>" />
					<input id="is_active" name="is_active" class="mini-combobox" data="CommonUtil.serverData.dictionary.YesNo" labelField="true"
					 label="启用状态：" style="width:300px;" emptyText="请选择..." />
					<span style="float:right;margin-right: 300px">
						<a id="search_btn" class="mini-button" style="display: none"   onclick="query">查询</a>
						<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear">清空</a>
					</span>
				</div>
			</fieldset>
			<span style="margin:2px;display: block;">
				<a id="add_btn" class="mini-button" style="display: none"   onclick="add">新增</a>
				<a id="edit_btn" class="mini-button" style="display: none"   onclick="update">修改</a>
				<a id="delete_btn" class="mini-button" style="display: none"   onclick="del">删除</a>
				<a class="mini-button" style="display: none"    id="download_btn" onclick="Import();">导入</a>
				<a class="mini-button" style="display: none"    id="upload_btn" onclick="Export();">导出</a>
			</span>
			<div id="ReportCustomManages" class="mini-fit" style="margin-top: 5px;">
				<div id="form_grid" class="mini-datagrid borderAll" width="100%" height="100%" sizeList="[10,20,30,50,100]" pageSize="10"
				 multiSelect="true" onrowdblclick="onRowDblClick" allowAlternating="true" sortMode="client">
					<div property="columns">
						<div type="indexcolumn"></div>
						<div type="checkcolumn"></div>
						<div field="custid" width="100" headerAlign="center" align="center" allowSort="true">报表编号</div>
						<div field="reporttype" width="100" headerAlign="center" align="center">报表类型</div>
						<div field="title" width="150" headerAlign="center">报表标题</div>
						<div field="is_active" width="150" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'YesNo'}">启用状态</div>
						<div field="operator_name" width="200" headerAlign="center" align="">提交人</div>
						<div field="operate_time" width="100" headerAlign="center" align="center" allowSort="true">提交时间</div>
					</div>
				</div>
			</div>
			<script type="text/javascript">
				mini.parse();
				top['ReportCustomManages'] = window;
				var grid = mini.get("form_grid");
				grid.on("beforeload", function (e) {
					e.cancel = true;
					var pageIndex = e.data.pageIndex;
					var pageSize = e.data.pageSize;
					search(pageSize, pageIndex);
				});

				function search(pageSize, pageIndex) {
					var form = new mini.Form("search_form");
					form.validate();
					//提交数据
					var data = form.getData();//获取表单多个控件的数据  
					data['pageNumber'] = pageIndex + 1;
					data['pageSize'] = pageSize;
					var param = mini.encode(data); //序列化成JSON
					CommonUtil.ajax({					
						url: '/ReportManageController/searchReportList',
						data: param,
						callback: function (data) {
							var grid = mini.get("form_grid");
							grid.setTotalCount(data.obj.total);
							grid.setPageIndex(pageIndex);
							grid.setPageSize(pageSize);
							grid.setData(data.obj.rows);
						}
					});
				}

				function add() {
					var url = CommonUtil.baseWebPath() + "/mini_base/mini_report/MiniReportCustomEdit.jsp?action=add";
					var tab = { id: "ReportCustomAdd", name: "ReportCustomAdd", text: "报表新增", url: url, showCloseButton: true, parentId: top["win"].tabs.getActiveTab().name };
					top['win'].showTab(tab);
				}

				function update() {
					var row = grid.getSelected();
					if (row) {
						var url = CommonUtil.baseWebPath() + "/mini_base/mini_report/MiniReportCustomEdit.jsp?action=edit";
						var tab = { id: "ReportCustomEdit", name: "ReportCustomEdit", title: "报表修改", url: url, showCloseButton: true, parentId: top["win"].tabs.getActiveTab().name };
						top["win"].openNewTab(tab, row);

					} else {
						mini.alert("请选中一条记录！", "消息提示");
					}
				}

				function onRowDblClick(e) {
					var row = grid.getSelected();
					if (row) {
						var url = CommonUtil.baseWebPath() + "/mini_base/mini_report/MiniReportCustomEdit.jsp?action=detail";
						var tab = { id: "ReportCustomDetail", name: "ReportCustomDetail", title: "报表详情", url: url, showCloseButton: true, parentId: top["win"].tabs.getActiveTab().name };
						top["win"].openNewTab(tab, row);

					} else {
						mini.alert("请选中一条记录！", "消息提示");
					}
				}


				function del() {
					var grid = mini.get("form_grid");
					var row = grid.getSelected();
					if (row) {
						mini.confirm("您确认要删除选中记录?", "系统警告", function (value) {
							if (value == "ok") {
								CommonUtil.ajax({
									url: "/ReportManageController/delReport",
									data: { custids: row.custid },
									callback: function (data) {
										if (data.code == 'error.common.0000') {
											mini.alert("删除成功")
											search(grid.pageSize, grid.pageIndex);
										} else {
											mini.alert("删除失败");
										}
									}
								});
							}
						});
					}
					else {
						mini.alert("请选中一条记录！", "消息提示");
					}

				}
				function checkBoxValuechanged() {
					search(grid.pageSize, 0);
				}

				function getData(action) {
					var grid = mini.get("form_grid");
					var row = null;
					if (action != "add") {
						row = grid.getSelected();
					}
					return row;
				}

				function clear() {
					var form = new mini.Form("#search_form");
					form.clear();
				}
				function query() {
					search(grid.pageSize, 0);
				}

				//导入
				function Import() {
					mini.open({
						url: CommonUtil.baseWebPath() +"/mini_base/mini_report/MiniSqlTextFileImport.jsp",
						width: 350,
						height: 260,
						title: "Sql text文件导入",
						ondestroy: function (action) {
							search(10, 0);//重新加载页面

						}
					});
				}


				//导出
				function Export() {
					var grid = mini.get("form_grid");
					var rows = grid.getSelecteds();
					var custids = "";
					if (rows.length > 0) {
						mini.confirm("您确认要导出选中记录?", "系统警告", function (value) {
							if (value == "ok") {
								for (var i = 0; i < rows.length; i++) {
									custids += rows[i].custid;
									if (i != rows.length - 1) {
										custids += ",";
									}
								}
								var urls = CommonUtil.pPath + "/sl/ReportManageController/exportSQLText";
								$('<form action="' + urls + '" method="post"> <input type="hidden" id="custids" name="custids" value="' + custids + '"></form>').appendTo('body').submit().remove();
								search(10, 0);
							}
						})
					} else {
						mini.alert("请选中一条记录！", "消息提示");
					}
				}


				$(document).ready(function () {
					search(10, 0);
				})

			</script>
		</body>
		</html>