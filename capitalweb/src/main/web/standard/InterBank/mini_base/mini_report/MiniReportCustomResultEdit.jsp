<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
  <%@ include file="../../../global.jsp"%>
    <html>
    <head>
      <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
      <title>报表查询结果编辑</title>
    </head>

    <body style="width:100%;height:100%;background:white">

      <fieldset class="mini-fieldset">
        <legend></legend>
        <table class="mini_table" id="accout_form" fit="true">
          <tr>
            <td>
              <input id="result_value" name="result_value" class="mini-textbox" required="true"  vtype="maxLength:30" labelField="true"
                label="名称:" style="width:300px;" requiredErrorText="该输入项为必输项" emptyText="请输入名称" />
              <br />
              <font color="red">说明：名称字段需与执行的SQL语句返回字段名称相同（所有字母要求小写驼峰）</font>
            </td>
          </tr>
          <tr>
            <td>
              <input id="result_label" name="result_label" requiredErrorText="该输入项为必输项" class="mini-textbox" vtype="maxLength:30" labelField="true"
                label="标签:" style="width:300px;" emptyText="请输入标签" />
            </td>
          </tr>
          <tr>
            <td>
              <input id="result_formatter" name="result_formatter" class="mini-textbox" labelField="true" label="格式化函数:" style="width:300px;"
                emptyText="" />
              <br />
              <font color="red">说明：1.格式化函数统一在“/jsp/report/ResulstFormatterJS.js”文件下增加，内容填写如下：ResultUtil.test(value);</font>
              <br>
              <font color="red"> 2.格式化函数仅用于网页显示，需要字典翻译和金额格式化时请使用oracle函数</font>
            </td>
          </tr>
          <tr>
            <td>
              <input id="column_align" name="column_align" class="mini-combobox" labelField="true" label="显示位置:" data = "CommonUtil.serverData.dictionary.显示位置" style="width:300px;" emptyText="请选择..."
              />
            </td>
          </tr>
          <tr>
            <td>
              <input id="column_width" name="column_width" value="100" requiredErrorText="该输入项为必输项" class="mini-textbox" vtype="maxLength:10" labelField="true"
                label="宽度:" style="width:300px;" emptyText="请输入宽度" />
            </td>
          </tr>
          <tr>
            <td>
              <input id="detail_show" name="detail_show" class="mini-combobox" labelField="true" label="明细:" data = "CommonUtil.serverData.dictionary.YesNo"  style="width:300px;" emptyText="请选择..."
              />
            </td>
          </tr>
        </table>
      </fieldset>
      <div style="float: right; margin: 10px">
        <a class="mini-button" style="display: none"  id="save_btn"  onclick="save()">保存</a>
        <a class="mini-button" style="display: none"  id="save_btn" onclick="cancel()">取消</a>
      </div>
      <script type="text/javascript">
        mini.parse();
        function save() {
          CloseWindow("ok");
        }

        function GetData() {
          var form = new mini.Form("accout_form");
          form.validate();
          if (form.isValid() == false) {
            return;
          }
          var data = form.getData(true); //获取表单多个控件的数据  
          return data;
        }

        
        function SetDataObj(row) {
          var data = mini.clone(row);
          var form = new mini.Form("#accout_form");
          form.setData(data);
        }

        function CloseWindow(action) {
          if (window.CloseOwnerWindow)
            return window.CloseOwnerWindow(action);
          else
            window.close();
        }

      </script>
    </body>
    </html>