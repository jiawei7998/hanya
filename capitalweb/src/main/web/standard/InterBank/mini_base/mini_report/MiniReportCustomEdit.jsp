﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
	<%@ include file="../../../global.jsp"%>
		<html>

		<head>
			<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
			<title>报表新增</title>
		</head>

		<body style="width: 100%; height: 100%; background: white">
			<div id="tabs1" class="mini-tabs">
				<div title="基本信息">
					<div class="mini-splitter" style="width:1050px;height:600px">
						<div showCollapseButton="true" size="50%">
							<div id="ReportCustomEditV1" class="mini-fit">
								<div id="p1" class="mini-panel" title="基础信息"  style="width:100%;height:100%">
									<table id="">
										<tr>
											<td>
												<input id="reporttype_name" name="" class="mini-textbox" readonly="true" labelField="true" label="报表类型" style="width:300px;"
												 value="客户报表" />
												<input id="reporttype" type="hidden" name="reporttype" />
												<input id="branch_id" class="mini-hidden" name="branch_id" value="<%=__sessionUser.getBranchId()%>" />
											</td>
										</tr>
										<tr>
											<td>
												<input id="custid" name="custid" class="mini-textbox" type="text" Enabled="false" labelField="true" label="编号" style="width:300px;"
												 emptyText="自动生成" />
											</td>
										</tr>
										<tr>
											<td>
												<input id="title" name="title" class="mini-textbox" emptyText="请输入报表名称" required="true"  labelField="true" label="报表名称" vtype="maxLength:30"
												 style="width:300px;" requiredErrorText="该输入项为必输项" />
											</td>
										</tr>
										<tr>
											<td>
												<input id="is_active" name="is_active" class="mini-combobox" required="true"  labelField="true" label="启用状态" data="CommonUtil.serverData.dictionary.isActive"
												 style="width:300px;" requiredErrorText="该输入项为必输项" emptyText="请选择..." />
											</td>
										</tr>
										<tr>
											<td> 
											<font color="red">说明：不上传模板则默认导出网格的数据.</font>
											<div id="excelModule_file_panel" >
												<form accept="*/*" id="uploadForm" method="post" encoding="multipart/form-data" enctype="multipart/form-data" > 
													Excel导出模板
													<input type="file" id="files" name="files"  labelField="true" label=""  labelStyle="width:600px;"/>
												</form>
												<div id="fileDown"></div>
											</div>
											<input type="hidden" id="template_file" />
										</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
						<div showCollapseButton="true" size="50%">
							<div id="p1" class="mini-panel" title="明细信息"  style="width:100%;height:100%">
								<table>
									<tr>
										<td>
											<input id="dialogUrl" name="dialogUrl" class="mini-textbox" labelField="true" label="明细页面路径" style="width:300px;" vtype="rangeLength:0,30"
											 emptyText="请输入明细页面路径" />
											<br />
											<font id="dialogUrlTip" color="red">说明：自动生成的明细页面路径为jsp/report/ReportShowDetail.jsp，查询参数根据具体报表的需要添加.</font>
										</td>
									</tr>
									<tr>
										<td>
											<input id="dialogWidth" name="dialogWidth" class="mini-textbox" labelField="true" label="弹出设置" style="width:300px;" vtype="rangeLength:0,200"
											 emptyText="请输入弹出设置" /> *
											<input id="dialogHight" name="dialogHight" class="mini-textbox" labelField="true" label="" style="width:300px;" vtype="rangeLength:0,200"
											 emptyText="请输入弹出设置" />
										</td>
									</tr>
									<tr>
										<td>
											<input id="dialogSQL" name="dialogSQL" class="mini-textarea" labelField="true" label="明细数据集" style="width:500px;height:200px">
											<br>
											<font color="red">多组数据集时，用分号进行分割.</font>
										</td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div title="数据集">
					<div class="mini-splitter" style="width:1050px;height:600px">
						<div showCollapseButton="true" size="50%">
							<div id="p1" class="mini-panel" title="数据集合"  style="width:100%;height:100%">
								<input id="execSQL" name="execSQL" class="mini-textarea" width="500px" ; height="300px"></input>
								<br>
								<font color="red">
									<br> 1.自定义报表中所使用所有字段名均为大写.
									<br> 2."数据集合"用于报表首页网格数据填充的数据源,支持多数据来源.
									<br> 3.多条数据来源使用";"进行分割,其用于网格填充的数据必须为第一条SQL语句.
									<br> 4."控件生成"按钮，系统根据数据源自动生成控件id,需用户手动调整控件属性.
									<br/>
									<br/>
								</font>
								<a id="auto_controls_btn" class="mini-button" style="display: none"   onclick="autoCreateControls">控件生成</a>
								<a id="auto_template_btn" class="mini-button" style="display: none"   onclick="autoCreateTemplate">模板生成</a>
							</div>
						</div>
						<div showCollapseButton="true" size="50%">
							<div id="layout1" class="mini-layout" style="width:100%;height:100%;">
								<div title="报表查询条件" region="north" width="auto" height="300px">
									<div class="mini-toolbar" style="padding:0px;">
										<a id="add_btn" class="mini-button" style="display: none"   onclick="add">新增</a>
										<a id="edit_btn" class="mini-button" style="display: none"   onclick="update">修改</a>
										<a id="delete_btn" class="mini-button" style="display: none"   onclick="del">删除</a>
									</div>
									<div id="search_grid_table" class="mini-fit" style="margin-top: 0px;">
										<div id="datagrid1" class="mini-datagrid borderAll" style="width:100%;height:100%;" showPager="false">
											<div property="columns">
												<div type="indexcolumn" headerAlign="center" width="50"></div>
												<div field="search_value" width="80" headerAlign="center"  align="center">字段名</div>
												<div field="search_label" width="70" align="center" headerAlign="center">标签</div>
												<div field="search_type" width="70" renderer="CommonUtil.dictRenderer" data-options="{dict:'JasperSearchType'}" align="center"
												 headerAlign="center"  align="center">类型</div>
												<div field="search_default" width="70" align="center" headerAlign="center">默认值</div>
											</div>
										</div>
									</div>

								</div>
								<div title="报表显示结果" region="center" width="auto" showHeader="true">
									<div class="mini-toolbar" style="padding:0px;">
										<a id="add_btn" class="mini-button" style="display: none"   onclick="adds">新增</a>
										<a id="edit_btn" class="mini-button" style="display: none"   onclick="updates">修改</a>
										<a id="delete_btn" class="mini-button" style="display: none"   onclick="dels">删除</a>
									</div>
									<div id="result_grid_table" class="mini-fit" style="margin-top: 0px;">
										<div id="datagrid2" class="mini-datagrid borderAll" style="width:100%;height:100%;" showPager="false">
											<div property="columns">
												<div type="indexcolumn" headerAlign="center" width="50"></div>
												<div field="result_label" width="80" headerAlign="center"  align="center">显示名称</div>
												<div field="result_value" width="70" align="center" headerAlign="center">对应字段</div>
												<div field="column_width" width="70" align="center" headerAlign="center">宽度</div>
												<div field="result_formatter" width="70" align="center" headerAlign="center">格式化函数</div>
												<div field="column_align" width="70" align="center" headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'显示位置'}">显示位置</div>
												<div field="detail_show" width="70" align="center" headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'YesNo'}">明细</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div title="菜单信息">
					<table class="" style="width:auto;height:600px;">
						<tr>
							<td>菜单名称</td>
							<td>
								<input id="module_name" name="module_name" class="mini-textbox" />
							</td>
						</tr>
						<tr>
							<td>父菜单</td>
							<td>
								<input id="branchId" class="mini-hidden" name="branchId" value="<%=__sessionUser.getBranchId()%>" />
								<ul id="menu_ul"  class="mini-tree" showTreeIcon="true" textField="text" idField="id" parentField="pid" resultAsTree="true"
								 showArrow="true" expandOnNodeClick="true" ShowCheckBox="true"></ul>
							</td>
						</tr>
					</table>
				</div>
				<div title="说明">
					<div style="width:auto;height:600px;">
						<br>参数说明：</br>
						<br> _USER_ID：当前用户id</br>
						<br> _INST_ID：当前用户机构id</br>
						<br> _HEAD_INST_ID：总行机构id</br>
						<br> _SETTLE_DATE：当前业务日</br>
						<br> _SETTLE_DATE-1：当前业务日前一天</br>
						<br> _NOW_DATE：当前日期</br>
						<br> 使用in的SQL语句时，用'$' + { } 传递参数.如：AND ('#' + {ACCID} IS NULL OR POSITION.INSECUACCID IN ('$' + {ACCID}))</br>
					</div>
				</div>
			</div>
			<div class="mini-toolbar" style="margin-right: 20px; margin-top: 5px;">
				<a id="save_btn" class="mini-button" style="display: none"    onclick="saves()">保存</a>
				<a id="cancel_btn" class="mini-button" style="display: none"    onclick="close()">取消</a>
			</div>



			<script>
				mini.parse();
				var url = window.location.search;
				var action = CommonUtil.getParam(url, "action");
				var param = top["ReportCustomManages"].getData(action);
				var form = new mini.Form("#tabs1");
				$(document).ready(function () {
					initModuleTree();
					initform();
				})

				//加载菜单的方法
				function initModuleTree() {
					 	var data = {};
					    //data.roleId = GetQueryString("roleId");
					    data.branchId = mini.get("branchId").getValue();
					    var param = mini.encode(data);
					    CommonUtil.ajax({
					        url: "/RoleController/getAllResourceByBranchId",
					        data: param,
					        callback: function (data) {
					        	var grid = mini.get("menu_ul");
								grid.setData(data.obj);
					            //showHistory();
					        }
					    });
				}

				function close() {
							top["win"].closeMenuTab();
						}
				function add() {
					var btnEdit = this;
					mini.open({
						url: CommonUtil.baseWebPath() +"/mini_base/mini_report/MiniReportCustomSearchEdit.jsp",
						title: "查询条件新增",
						width: 400,
						height: 400,
						ondestroy: function (action) {
							if (action == "ok") {
								var iframe = this.getIFrameEl();
								var data = iframe.contentWindow.GetData();
								var record = mini.clone(data); //必须
								if (record) {
									var row = { "search_value": record.search_value, "search_label": record.search_label, "search_type": record.search_type, "search_default": record.search_default };
									var grid = mini.get("datagrid1");
									grid.addRow(row);
								}
							}
						}
					});
				}


				function update() {
					var grid = mini.get("datagrid1");
					var record = grid.getSelected();
					if (record) {
						mini.open({
							url: CommonUtil.baseWebPath() +"/mini_base/mini_report/MiniReportCustomSearchEdit.jsp",
							title: "查询条件编辑",
							width: 400,
							height: 400,
							onload: function () {
								var iframe = this.getIFrameEl();
								var row = { "search_value": record.search_value, "search_label": record.search_label, "search_type": record.search_type, "search_default": record.search_default };
								iframe.contentWindow.SetDataObj(row);

							},
							ondestroy: function (action) {
								if (action == "ok") {
									var iframe = this.getIFrameEl();
									var data = iframe.contentWindow.GetData();
									var records = mini.clone(data); //必须
									if (records) {
										var grid = mini.get("datagrid1");
										grid.updateRow(record, records)
									}
								}

							}
						});
					} else {
						mini.alert("请选中一条记录！", "消息提示");
					}


				}


				function del() {
					var grid = mini.get("datagrid1");
					var row = grid.getSelected();
					if (row) {
						mini.confirm("您确认要删除选中记录?", "系统警告", function (value) {
							if (value == 'ok') {
								var grid = mini.get("datagrid1");
								var row = grid.getSelected();
								grid.removeRow(row)
							}
						});
					} else {
						mini.alert("请选中一条记录！", "消息提示");
					}

				}

				function moveUp() {
					var row = grid.getSelected();
					if (row) {
						mini.confirm("您确认要移动选中记录?", "系统警告", function (value) {
							if (value == 'ok') {
								var grid = mini.get("datagrid1");
								var rows = grid.getSelecteds();
								var Array = [];
								Array.push(rows);
								grid.moveUp(Array);
							}
						});
					} else {
						mini.alert("请选中一条记录！", "消息提示");
					}

				}

				function adds() {
					mini.open({
						url:  CommonUtil.baseWebPath() +"/mini_base/mini_report/MiniReportCustomResultEdit.jsp",
						title: "查询结果新增",
						width: 500,
						height: 500,
						ondestroy: function (action) {
							if (action == "ok") {
								var iframe = this.getIFrameEl();
								var data = iframe.contentWindow.GetData();
								var record = mini.clone(data); //必须
								if (record) {
									var row = {
										"result_value": record.result_value, "result_label": record.result_label, "result_formatter": record.result_formatter,
										"column_align": record.column_align, "column_width": record.column_width, "detail_show": record.detail_show
									};
									var grid = mini.get("datagrid2");
									grid.addRow(row);
								}
							}
						}
					});
				}


				function updates() {
					var grid = mini.get("datagrid2");
					var record = grid.getSelected();
					if (record) {
						mini.open({
							url:  CommonUtil.baseWebPath() +"/mini_base/mini_report/MiniReportCustomResultEdit.jsp",
							title: "查询结果修改",
							width: 500,
							height: 500,
							onload: function () {
								var iframe = this.getIFrameEl();
								var row = {
									"result_value": record.result_value, "result_label": record.result_label, "result_formatter": record.result_formatter,
									"column_align": record.column_align, "column_width": record.column_width, "detail_show": record.detail_show
								};
								iframe.contentWindow.SetDataObj(row);
							},
							ondestroy: function (action) {
								if (action == "ok") {
									var iframe = this.getIFrameEl();
									var data = iframe.contentWindow.GetData();
									var records = mini.clone(data); //必须
									if (records) {
										var grid = mini.get("datagrid2");
										grid.updateRow(record, records)
									}
								}

							}
						});
					} else {
						mini.alert("请选中一条记录！", "消息提示");
					}


				}


				function dels() {
					var grid = mini.get("datagrid2");
					var row = grid.getSelected();
					if (row) {
						mini.confirm("您确认要删除选中记录?", "系统警告", function (value) {
							if (value == 'ok') {
								var row = grid.getSelected();
								grid.removeRow(row)
							}
						});
					} else {
						mini.alert("请选中一条记录！", "消息提示");
					}

				}

				function autoCreateControls(){
					var execSQL = mini.get("execSQL").value;
					var dialogSQL = mini.get("dialogSQL").value;
					var search_list=null;
					var result_list=null;
					if (execSQL ==''){
						mini.alert("执行数据集不允许为空");
						return
					}
					var obj = {
						execSQL: execSQL,
						dialogSQL: dialogSQL,
						search_list: mini.get("datagrid1").getData(),
						result_list: mini.get("datagrid2").getData()
					};
					CommonUtil.ajax({
						url: '/ReportManageController/autoCreateControls',
						data: obj,
						callback: function (data) {
							var grid1 = mini.get("datagrid1");
							var grid2 = mini.get("datagrid2");
							grid1.setData(data.search_list);
							grid2.setData(data.result_list);
						}
					});
				
				}
				
				//生成模板
				function autoCreateTemplate(){
					var result_grid_list=mini.get("datagrid2").getData();
					var result_list = [];
					for(var i =0;i<result_grid_list.length;i++){
						if(result_grid_list[i].detail_show=="0"){
							result_list.push(result_grid_list[i]);
						}
					}
					
					if (!result_list || result_list.length <= 0) {
						mini.alert("提示信息", "无显示结果字段!");
						return false;
					}
					var title = mini.get("title").value;
					var $autoCreateTemplate = $("<form action='<%=basePath%>/sl/ReportManageController/autoCreateTemplate' method='post'></form>");
					var $result_list = $("<input id='result_list_json' type='hidden' name='result_list' value="+JSON.stringify(result_list)+" />");
					var $title = $("<input id='title_auto' type='hidden' name='title' value="+title+" />");
					$autoCreateTemplate.append($result_list);
					$autoCreateTemplate.append($title);
					$autoCreateTemplate.appendTo("body").submit().remove();
				}
				
				
				function saves() {
					form.validate();
					if (form.isValid() == false) {
						mini.alert("信息填写有误，请重新填写!", "消息提示")
						return;
					}
					var saveUrl = "/ReportManageController/saveReportInfo";
					var data = form.getData(true); //获取表单多个控件的数据  
					var grid1 = mini.get("datagrid1");
					var grid2 = mini.get("datagrid2");
					var pid = null; 
					
					data["search_list"] = grid1.getData(true);
					data["result_list"] = grid2.getData(true);
					//data["module_pid"] = mini.get("menu_ul").getSelectedNode ().id;
					data["types"] = action;
					var param = mini.encode(data); //序列化成JSON
					CommonUtil.ajax({
						url: saveUrl,
						data: param,
						callback: function (data) {
							if (data.code == 'error.common.0000') {
								mini.alert("保存成功", '提示信息', function () {
									top["win"].closeMenuTab();
								});
							} else {
								mini.alert("保存失败");
							}
						}
					});
				}

				function initform() {
					var form = new mini.Form("tabs1");
					if (action == "edit") {
						var reportclob = $.parseJSON(param.reportclob);
						mini.get("reporttype_name").setValue("客户报表");
						mini.get("custid").setValue(param.custid);
						mini.get("title").setValue(param.title);
						mini.get("is_active").setValue(param.is_active);
						mini.get("dialogUrl").setValue(reportclob.dialogUrl);
						mini.get("dialogWidth").setValue(reportclob.dialogWidth);
						mini.get("dialogHight").setValue(reportclob.dialogHight);
						mini.get("execSQL").setValue(reportclob.execSQL);
						mini.get("dialogSQL").setValue(reportclob.dialogSQL);
						var grid = mini.get("datagrid1");
						var grid1 = mini.get("datagrid2");
						grid.setData(reportclob.search_list);
						grid1.setData(reportclob.result_list);
					} else if (action == "detail") {
						var reportclob = $.parseJSON(param.reportclob);
						mini.get("reporttype_name").setValue("客户报表");
						mini.get("custid").setValue(param.custid);
						mini.get("title").setValue(param.title);
						mini.get("is_active").setValue(param.is_active);
						
						mini.get("dialogUrl").setValue(reportclob.dialogUrl);
						mini.get("dialogWidth").setValue(reportclob.dialogWidth);
						mini.get("dialogHight").setValue(reportclob.dialogHight);
						mini.get("execSQL").setValue(reportclob.execSQL);
						mini.get("dialogSQL").setValue(reportclob.dialogSQL);
						var grid = mini.get("datagrid1");
						var grid1 = mini.get("datagrid2");
						grid.setData(reportclob.search_list);
						grid1.setData(reportclob.result_list);
						form.setEnabled(false);
						mini.get("save_btn").setEnabled(false);
						mini.get("cancel_btn").setEnabled(false);
					} else {
						//form.setData(param, false);
					}
				}
			</script>
		</body>
		</html>