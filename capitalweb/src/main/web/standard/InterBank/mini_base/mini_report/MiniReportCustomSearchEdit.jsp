<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
	<%@ include file="../../../global.jsp"%>
		<html>

		<head>
			<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
			<title>报表查询条件编辑</title>
		</head>

		<body style="width:100%;height:100%;background:white">

			<fieldset class="mini-fieldset">
				<legend></legend>
				<table class="mini_table" id="accout_form" fit="true">
					<tr>
						<td>
							<input id="search_value" name="search_value" class="mini-textbox" required="true"  vtype="maxLength:30" labelField="true" label="字段名:" style="width:300px;"
							requiredErrorText="该输入项为必输项" emptyText="请输入字段名" />
						</td>
					</tr>
					<tr>
						<td>
							<input id="search_label" name="search_label" requiredErrorText="该输入项为必输项" class="mini-textbox" vtype="maxLength:30"
							 labelField="true" label="标签:" style="width:300px;" emptyText="请输入标签" />
						</td>
					</tr>
					<tr>
						<td>
							<input id="search_type" name="search_type"  class="mini-combobox" data = "CommonUtil.serverData.dictionary.JasperSearchType"
							 labelField="true" label="控件类型:" style="width:300px;" emptyText="请选择..." />
						</td>
					</tr>
					<tr>
						<td>
							<input id="search_default" name="search_default"  class="mini-textbox" vtype="maxLength:30"
							 labelField="true" label="默认值:" style="width:300px;" emptyText="请输入默认值" />
						</td>
					</tr>
				</table>
			</fieldset>
			<div style="float: right; margin: 10px">
				<a class="mini-button" style="display: none"  id="save_btn"  onclick="save()">保存</a>
				<a class="mini-button" style="display: none"  id="save_btn"  onclick="cancel()">取消</a>
			</div>
			<script type="text/javascript">
				mini.parse();
				function save() {
					CloseWindow("ok");
				}
				
				function GetData() {
						var form = new mini.Form("accout_form");
						form.validate();
						if (form.isValid() == false) {
							return;
						}
						var data = form.getData(true); //获取表单多个控件的数据  
						return data;
					}
			
					function SetDataObj(row) {
							var data = mini.clone(row);
							var form = new mini.Form("#accout_form");
							form.setData(data);
						}
			
			function CloseWindow(action) {
				if (window.CloseOwnerWindow)
					return window.CloseOwnerWindow(action);
				else
					window.close();
			}
			
			</script>
		</body>
		</html>