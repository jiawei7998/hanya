<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%> 
<html>
<head>
    <title>附件树</title>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script> 
</head>
<body style="width:100%;height:100%;background:white">
	<fieldset id="fd2" class="mini-fieldset">
		<legend><label>查询条件</label></legend>
		<div id="find" class="fieldset-body">
			<div id="search_form"   >
                <input id="treeId" name="treeId" class="mini-textbox"  labelField="true"  
                    label="附件树编号："  emptyText="请输入附件树编号" labelStyle="text-align:right;"/>
                <input id="treeName" name="treeName" class="mini-textbox"  labelField="true"  
                    label="附件树名称："  emptyText="请输入附件树名称" labelStyle="text-align:right;"/>
                <input id="refType" name="refType" class="mini-combobox"  labelField="true"  
                    label="关联类型："  emptyText="请选择关联类型"  
                    data="CommonUtil.serverData.dictionary.refType" labelStyle="text-align:right;"/>
                <span style="float:right;margin-right: 150px">
                    <a id="search_btn" class="mini-button" style="display: none"     onclick="search1()">查询</a>
                    <a id="clear_btn" class="mini-button" style="display: none"     onClick="clear">清空</a>
                </span>
            </div>
            
		</div>
    </fieldset>
    <span style="margin:2px;display: block;">
        <a id="add_btn" class="mini-button" style="display: none"    onclick="add">新增</a>
        <a id="edit_btn" class="mini-button" style="display: none"    onclick="modifyedit()">修改</a>
        <a id="delete_btn" class="mini-button" style="display: none"    onclick="removeRows">删除</a>
    </span>
    <div id="AttachTreeManage" class="mini-fit" style="margin-top: 2px;">
        <div id="attachTree_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" 
        idField="" allowResize="true" pageSize="10" multiSelect="true" onRowdblclick="custDetail"
        sortMode="client" allowAlternating="true">
            <div property="columns" >
                <div type="indexcolumn" headerAlign="center"  align="center" width="40px">序号</div>
                <div type="checkcolumn"></div>
                <div field="treeId" headerAlign="center"  align="center" width="80px" allowSort="true">附件树编号</div>    
                <div field="treeName" headerAlign="center"   width="200px">附件树名称</div> 
                <div field="refType" headerAlign="center" align="center" width="80px" renderer="CommonUtil.dictRenderer" data-options="{dict:'refType'}">关联类型</div>                           
                <div field="treeDesc"  headerAlign="center"width="200px">附件树描述</div>
                <div field="createUserName" headerAlign="center"  align="center"  width="100px">创建用户</div>
                <div field="createInstName"   headerAlign="center"   width="180px">创建机构</div>
                <div field="createTime"   headerAlign="center"  align="center" width="150px" allowSort="true">创建时间</div>                            
                <div field="updateUserName"  headerAlign="center"  align="center"  width="100px" >更新用户</div>
                <div field="updateInstName"   headerAlign="center"  align="center"  width="150px" >更新机构</div>
                <div field="updateTime"  headerAlign="center"  align="center"  width="150px" allowSort="true">更新时间</div>                                                                                               
            </div>
        </div>
    </div>
</body>
<script type="text/javascript">

        mini.parse();

		top["attachTreeManage"]=window;

        function getData(){
            var grid =mini.get("attachTree_grid");
            var record =grid.getSelected(); 
            return record;
        }

		$(document).ready(function(){
            //控制按钮显示
            $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
                var grid = mini.get("attachTree_grid");
                grid.on("beforeload", function (e) {
                    e.cancel = true;
                    var pageIndex = e.data.pageIndex;
                    var pageSize = e.data.pageSize;
                    search(pageSize, pageIndex);
                });
                search(grid.pageSize, 0);
            });
		});

        function search1(){
            var grid =mini.get("attachTree_grid");
            search(grid.pageSize,0); 
        }

        function search(pageSize,pageIndex){
            var form =new mini.Form("search_form");
            form.validate();
            if (form.isValid() == false) return;//表单验证
            var data =form.getData();//获取表单数据
            data['pageNumber']=pageIndex+1;
            data['pageSize']=pageSize;
            var param = mini.encode(data); //序列化成JSON
            CommonUtil.ajax({
                url:"/AttachTreeController/searchAttachTreeList",
                data:param,
                callback:function(data){
                        
                    var grid =mini.get("attachTree_grid");
                    //设置分页
                    grid.setTotalCount(data.obj.total);
                    grid.setPageIndex(pageIndex);
                    grid.setPageSize(pageSize);
                    //设置数据
                    grid.setData(data.obj.rows);
                }
            });
        } 

    //清空
    function clear(){
        var form =new mini.Form("search_form");
        form.clear();
        search1(); 
    }

    //新增
    function add(){
        var url = CommonUtil.baseWebPath() + "/mini_base/MiniAttachTreeEdit.jsp?action=new";
        var tab = { id: "attachTreeManageAdd", name: "attachTreeManageAdd", text: "附件树新增", url: url ,showCloseButton:true,parentId:top['win'].tabs.getActiveTab().name};
        top['win'].showTab(tab);
    }


    //修改
    function modifyedit(){
        var grid =mini.get("attachTree_grid");
        var  record =grid.getSelected();
        if(record){
            var url = CommonUtil.baseWebPath() + "/mini_base/MiniAttachTreeEdit.jsp?action=edit";
            var tab = { id: "attachTreeManageEdit", name: "attachTreeManageEdit", text: "附件树修改", url: url ,showCloseButton:true,parentId:top['win'].tabs.getActiveTab().name};
            top['win'].showTab(tab);
        }else{
            mini.alert("请选中一条记录!","系统提示"); 
        }
    }

    //双击一行打开详情信息
    function custDetail(e){
        var grid = e.sender;
        var row = grid.getSelected();
        if(row){
            var url = CommonUtil.baseWebPath() + "/mini_base/MiniAttachTreeEdit.jsp?action=detail";
            var tab = { id: "attachTreeManageDetail", name: "attachTreeManageDetail", text: "附件树详情", url: url,showCloseButton:true,parentId:top['win'].tabs.getActiveTab().name };
            top['win'].showTab(tab);
        }
    }

    //删除
    function removeRows() {
        var grid =mini.get("attachTree_grid");
        //获取选中行
        var rows =grid.getSelecteds();
        
        if (rows.length>0) {
            var treeIds = new Array();
			$.each(rows, function(i, n){
				treeIds.push(n.treeId);
			});
            mini.confirm("您确认要删除选中记录?","系统警告",function(value){   
                if (value == "ok"){   
                    CommonUtil.ajax({
                        url : '/AttachTreeController/removeAttachTree',
                        data : treeIds,
                        callback : function(data){
                            if("error.common.0000" == data.code){
                                mini.alert("删除成功！","系统提示");
                                search1();
                            }
                        }
                    });
                
                }   
            });       
                
        }else{
            mini.alert("请至少选择一条记录!","系统提示");
        }
    }



</script>
</html>