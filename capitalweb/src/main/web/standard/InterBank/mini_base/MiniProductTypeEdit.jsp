<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%> 
<html>
<head>
    <title>产品类型</title>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script> 
</head>
<body>
    <div>
        <span id="labell"><a href="javascript:CommonUtil.activeTab();">产品类型</a> >> 修改</span>
        <div id="searchForm" style="width:100%;">
            <table id="search_form" width="100%" class="mini-table">
                <tr>
                    <td><input id="prdTypeNo" name="prdTypeNo" class="mini-textbox"  
                        vtype="float" vtype="maxLength:20" labelField="true"  
                        label="产品类型代码"  emptyText="请输入产品类型代码" required="true"
                        width="300px"/>
                    </td>
                    <td>
                        <input id="prdTypeName" name="prdTypeName" class="mini-textbox" 
                        vtype="maxLength:50" labelField="true"  label="产品类型名称" 
                        emptyText="请输入产品类型名称" required="true"
                        width="300px"/>
                    </td>
                    <td><input id="supPrdTypeNo" name="supPrdTypeNo" class="mini-treeselect" 
                        labelField="true"  label="归属产品类型"  emptyText="请选择归属产品类型" 
                        showTreeLines="true" showTreeIcon="true" multiSelect="false" resultAsTree="false"
                        width="300px"/>
                    </td>
                </tr>
                
                <tr>
                    <td><input id="prdTypeDesc" name="prdTypeDesc" class="mini-textarea"  
                        labelField="true"  label="产品类型描述" style="height:80px;" 
                        emptyText="请输入产品类型描述" width="300px"  vtype="maxLength:1024"/>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="text-align:center;">
                        <a id="save_btn" class="mini-button" style="display: none"    onclick="save">保存</a>
                        <a id="cancel_btn" class="mini-button" style="display: none"    onclick="cancel">取消</a>
                    </td>
                </tr>
            </table>
            
        </div>
    </div>
    <script>
         mini.parse();

        $(document).ready(function(){
            init();
            supPrdTypeNo();
            
        });
        //归属产品类型的查询与赋值
        function supPrdTypeNo() {
            var param = mini.encode({}); //序列化成JSON
            mini.parse();
            CommonUtil.ajax({
                url: "/ProductTypeController/searchProductType",
                data: param,
                callback: function (data) {
                    var grid = mini.get("supPrdTypeNo");
                    var val = grid.getValue();
                    if(data && data.obj){
                        data.obj.push({'id':'2','text':'无'});
                    }
                    grid.setData(data.obj);
                    grid.setValue(val);
                }
            });
        
        }


        var url = window.location.search;
        var action = CommonUtil.getParam(url,"action");
       
        
        function init(){
            if(action == "new"){
                $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>产品类型</a> >> 新增");
            }else if(action == "edit" || action == "detail"){
                
                var form1 = new mini.Form("search_form");
                var data=top["productTypeManage"].getData();
                form1.setData(data);
                mini.get("prdTypeNo").disable();
                

                if(action == "detail"){
                    $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>产品类型</a> >> 详情");
                    form1.setEnabled(false);
                    document.getElementById("save_btn").style.display="none";
                    //document.getElementById("cancel_btn").style.display="none";
                }
                

            }
        }
         //
         function save(){
            var form = new mini.Form("search_form");
            form.validate();
	        if (form.isValid() == false){//表单验证
                mini.alert("请输入有效数据！","系统提示");
                return;
            } 
	        var data = form.getData();
            if(data.supPrdTypeNo == null || data.supPrdTypeNo == ''){
                data['supPrdTypeNo'] = 2;
            }
            var param = mini.encode(data); //序列化成JSON
            
            var saveUrl="";
            if(action == "new"){
                saveUrl = "/ProductTypeController/addProductType";
            }else if(action == "edit"){
                saveUrl = "/ProductTypeController/editProductType";
            }
            CommonUtil.ajax({
				url:saveUrl,
                data:param,
            	callback:function(data){
					if('error.common.0000' == data.code){
				        mini.alert("保存成功!","系统提示",function(value){
                            //关闭并刷新
						    setTimeout(function(){ top["win"].closeMenuTab() },100);
                        });
                    }else{
                        mini.alert("保存失败！","系统提示");
                    }	
                }
			});
        }
        //取消按钮
        function cancel(){
            window.CloseOwnerWindow();
        }
    </script>
</body>
</html>