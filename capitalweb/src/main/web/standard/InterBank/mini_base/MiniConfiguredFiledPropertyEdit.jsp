<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%> 
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<div>
	<table id="field_form" class="form_table" width="100%" cols="4">
		<tr>
			<td>表单名称</td>
			<td><input id="formName" name="formName" class="mini-textbox"  data="CommonUtil.serverData.dictionary.YesNo"  emptyText="---请选择---"/></td>
			<td>字典项</td>
			<td><input id="defKey" name="defKey" class="mini-combobox" data="CommonUtil.serverData.dictionary.dict"  emptyText="---请选择---"/></td>
		</tr>
		<tr>
			<td>是否必填</td>
			<td><input id="isMandatory" name="isMandatory" class="mini-combobox" data="CommonUtil.serverData.dictionary.YesNo"  emptyText="---请选择---" /></td>
			<td>是否禁用</td>
			<td><input id="isDisabled" name="isDisabled" class="mini-combobox" data="CommonUtil.serverData.dictionary.YesNo"  emptyText="---请选择---" /></td>
		</tr>
		<tr id="nonComponet">
			<td>默认值</td>
			<td><input id="defValue" name="defValue" class="mini-textbox" vtype="maxLength:256"/></td>
			<td>默认文本</td>
			<td><input id="defText" name="defText" class="mini-textbox" vtype="maxLength:256"/></td>
		</tr>
		<tr id="attach" style="display:none;">
			<td>附件树模版</td>
			<td><input id="attachTreeTemp" name="attachTreeTemp" class="mini-combobox" /></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td>表单提示</td>
            <td colspan="3"><textarea id="formTips" name="formTips" class="mini-textarea"  vtype="maxLength:200" emptyText="---请选择---"></textarea></td>
		</tr>
    </table>
    <a  id="save_btn" class="mini-button" style="display: none"    onclick="saveEdit">暂存</a>
</div>
<script>
    mini.parse();
    var url = window.location.search;
    var drzpId = CommonUtil.getParam(url,"drzpId");
    var fldNo= CommonUtil.getParam(url,"fldNo");
    //初始化设置值
    function SetData(data,drzpId){
	var form = new mini.Form("#field_form");
		//跨页面传递的数据对象，克隆后才可以安全使用
        form.setData(mini.clone(data));
    }

    function saveEdit(){
        var data = new mini.Form("#field_form").getData();
		top['formEdit'].window['dropZone'+drzpId].updateItem(fldNo,data);
		//关闭窗口
		mini.alert('暂存成功!','提示',function(){
			window.CloseOwnerWindow("ok");
		});
    }

</script>