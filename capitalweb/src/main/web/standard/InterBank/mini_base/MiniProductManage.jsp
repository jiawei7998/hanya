<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%> 
<html>
<head>
    <title>产品</title>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script> 
</head>
<body style="width:100%;height:100%;background:white">
	<fieldset id="fd2" class="mini-fieldset">
		<legend><label>查询条件</label></legend>
		<div id="find" class="fieldset-body">
			<div id="search_form"   >
                    <input id="prdName" name="prdName" class="mini-textbox" width="320px" vtype="maxLength:100" labelField="true"  label="产品名称："  emptyText="请输入产品名称" labelStyle="text-align:right;"/>
					<input id="prdInst" name="prdInst" class="mini-buttonedit" width="320px" onbuttonclick="onInsQuery" labelField="true"  label="所属机构："  emptyText="请选择所属机构" labelStyle="text-align:right;" allowInput="false"/>
<%--					<input id="prdType" name="prdType" class="mini-treeselect" width="320px" labelField="true"  label="产品类型："  emptyText="请选择产品类型" labelStyle="text-align:right;"/>--%>
<%--                    <input id="prdTerm" name="prdTerm" class="mini-combobox" width="320px" labelField="true"  label="产品期限："  emptyText="请选择产品期限" data="CommonUtil.serverData.dictionary.prodTerm" labelStyle="text-align:right;"/>--%>
<%--					<input id="prdProp" name="prdProp" class="mini-combobox" width="320px" labelField="true"  label="产品性质：" emptyText="请选择产品性质" data="CommonUtil.serverData.dictionary.prdProp" labelStyle="text-align:right;"/>--%>
					<input id="isTemplate" name="isTemplate" class="mini-combobox" width="320px" labelField="true"  label="是否模板："  emptyText="请选择是否模板" data="CommonUtil.serverData.dictionary.YesNo" labelStyle="text-align:right;"/>
                    <input id="useFlag" name="useFlag" class="mini-combobox" width="320px"  labelField="true"  label="启用标志："  emptyText="请选择启用标志"  data="CommonUtil.serverData.dictionary.Status" labelStyle="text-align:right;"/>
                    <span style="float:right;margin-right: 150px">
                        <a id="search_btn" class="mini-button" style="display: none"     onclick="search1()">查询</a>
                        <a id="clear_btn" class="mini-button" style="display: none"     onClick="clear">清空</a>
                    </span>
            </div>
            
		</div>
    </fieldset>
    <span style="margin:2px;display: block;">
<%--        <a id="add_btn" class="mini-button" style="display: none"    onclick="add">新增</a>--%>
<%--        <a id="edit_btn" class="mini-button" style="display: none"    onclick="modifyedit()">修改</a>--%>
<%--        <a id="delete_btn" class="mini-button" style="display: none"    onclick="removeRows">删除</a>--%>
    </span>
    <div id="prdManage" class="mini-fit" style="margin-top: 2px;">
        <div id="prd_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" 
        idField="p_code" allowResize="true" pageSize="10" multiSelect="true"
<%--             onRowdblclick="custDetail"--%>
        sortMode="client" allowAlternating="true">
            <div property="columns" >
                <div type="indexcolumn" headerAlign="center"  align="center" width="40px">序号</div>
<%--                <div type="checkcolumn"></div>--%>
                <div field="prdNo" headerAlign="center"  align="center" width="80px" allowSort="true" dataType="int">产品编号</div>    
                <div field="prdInstName" headerAlign="center" width="120px">所属机构名称</div>
                <div field="prdName"  headerAlign="center"width="200px">产品名称</div>
<%--                <div field="prdType" headerAlign="center"   renderer="onPrdType" >产品类型</div>--%>
<%--                <div field="prdTerm"   headerAlign="center"  align="center"  width="60px" renderer="CommonUtil.dictRenderer" data-options="{dict:'prodTerm'}">产品期限</div>--%>
                <div field="prdProp"   headerAlign="center"   width="100px" renderer="CommonUtil.dictRenderer" data-options="{dict:'prdProp'}">产品性质</div>                            
                <div field="isTemplate"  headerAlign="center"  align="center"  width="60px" renderer="CommonUtil.dictRenderer" data-options="{dict:'YesNo'}">是否模板</div>
                <div field="useFlag"   headerAlign="center"  align="center"  width="60px" renderer="CommonUtil.dictRenderer" data-options="{dict:'Status'}">启用标志</div>
                <div field="lastUpdate"  headerAlign="center"  align="center"  width="130px" allowSort="true" dataType="date">更新时间</div>
                <div field="prdInst" headerAlign="center"  align="center" width="0px">所属机构</div>
            </div>
        </div>
    </div>
    <script>

        mini.parse();

        var prdType1=new Array();
        $(document).ready(function(){
            //控制按钮显示
            $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
                // prdType();
                var grid = mini.get("prd_grid");
                grid.on("beforeload", function (e) {
                    e.cancel = true;
                    var pageIndex = e.data.pageIndex;
                    var pageSize = e.data.pageSize;
                    search(pageSize, pageIndex);
                });

                search(grid.pageSize, 0);
            });
       });
       function search(pageSize,pageIndex){
            var form =new mini.Form("search_form");
            form.validate();
            if (form.isValid() == false){
                mini.alert("请输入有效数据!");
                return;//表单验证
            }
            var data =form.getData();//获取表单数据
            data['pageNumber']=pageIndex+1;
            data['pageSize']=pageSize;
            data['branchId']=top['win'].branchId;
            var param = mini.encode(data); //序列化成JSON
            CommonUtil.ajax({
                url:"/ProductController/searchPageProduct",
                data:param,
                callback:function(data){
                        
                    var grid =mini.get("prd_grid");
                    //设置分页
                    grid.setTotalCount(data.obj.total);
                    grid.setPageIndex(pageIndex);
                    grid.setPageSize(pageSize);
                    //设置数据
                    grid.setData(data.obj.rows);
                }
            });
        }

        function search1(){
            var grid =mini.get("prd_grid");
            search(grid.pageSize,0); 
        } 
        
        top["prd"]=window;
        function getData(){
            var grid =mini.get("prd_grid");
            var record =grid.getSelected(); 
            return record;
        }
         
        //产品类型的查询与赋值
        // function prdType() {
        //     var param1 = mini.encode({}); //序列化成JSON
        //     mini.parse();
        //     CommonUtil.ajax({
        //         url: "/ProductTypeController/searchProductType",
        //         data: param1,
        //         callback: function (data) {
        //             var grid = mini.get("prdType");
        //             grid.setData(data.obj);
        //             prdType1=data.obj;
        //
        //         }
        //     });
        //
        // }
        //产品类型的显示
        function onPrdType(e){
            for(var i=0,len=prdType1.length;i<len;i++){
                if(prdType1[i].value == e.value){
                    return prdType1[i].text;
                }
            }
        }
        //所属机构的查询
        function onInsQuery(e) {
			var btnEdit = this;
			mini.open({
				url : CommonUtil.baseWebPath() +"/../Common/MiniInstitutionSelectManages.jsp",
				title : "机构选择",
				width : 900,
				height : 600,
				ondestroy : function(action) {	
					if (action == "ok") 
					{
						var iframe = this.getIFrameEl();
						var data = iframe.contentWindow.GetData();
						data = mini.clone(data); //必须
						if (data) {
							btnEdit.setValue(data.instId);
							btnEdit.setText(data.instName);
							btnEdit.focus();
						}
					}

				}
			});
		}

        
        //清空
        function clear(){
            var form =new mini.Form("search_form");
            form.clear();
            search1(); 
        }

        //删除
        function removeRows() {
            var grid = mini.get("prd_grid");
            //获取选中行
            var rows =grid.getSelecteds();
            
            if (rows.length>0) {
                var prdNos = new Array();
               
                for(var i=0;i<rows.length;i++){
                    
                    prdNos.push(rows[i].prdNo);
                }
                
                mini.confirm("您确认要删除选中记录?","系统警告",function(value){   
                    if (value == "ok"){   
                        CommonUtil.ajax({
                            url : '/ProductController/removeProduct',
                            data : prdNos,
                            callback : function(data){
                                if("error.common.0000" == data.code){
                                    mini.alert("删除成功！","系统提示");
                                    search1();
                                }
                            }
                        });
                    
                    }   
                });       
                    
            }else{
                mini.alert("请至少选择一条记录!","系统提示");
            }
        }

         //新增
         function add(){
            var url = CommonUtil.baseWebPath() + "/mini_base/MiniProductEdit.jsp?action=new";
            var tab = { id: "productManageAdd", name: "productManageAdd", text: "产品新增", url: url ,showCloseButton:true,parentId:top['win'].tabs.getActiveTab().name};
            top['win'].showTab(tab);
        }
        //修改
        function modifyedit(){
            var grid = mini.get("prd_grid");
            var  record =grid.getSelected();
            if(record){
                var url = CommonUtil.baseWebPath() + "/mini_base/MiniProductEdit.jsp?action=edit";
                var tab = { id: "productManageEdit", name: "productManageEdit", text: "产品修改", url: url ,showCloseButton:true,parentId:top['win'].tabs.getActiveTab().name};
                top['win'].showTab(tab);
            }else{
                mini.alert("请选中一条记录","系统提示"); 
            }
        }

        //双击一行打开详情信息
        function custDetail(e){
            var grid = e.sender;
            var row = grid.getSelected();
            if(row){
                var url = CommonUtil.baseWebPath() + "/mini_base/MiniProductEdit.jsp?action=detail";
                var tab = { id: "productManageDetail", name: "productManageDetail", text: "产品详情", url: url,showCloseButton:true,parentId:top['win'].tabs.getActiveTab().name };
                top['win'].showTab(tab);
            }
        }
    </script>
</body>
</html>