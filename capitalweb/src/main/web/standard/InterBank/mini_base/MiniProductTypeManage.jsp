<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%> 
<html>
<head>
    <title>产品类型</title>
</head>
<body style="width:100%;height:100%;background:white">
        <div id="toolbar1" class="mini-toolbar" style="margin-left: 20px;">
            <table style="width:100%;">
                <tr>
                <td style="width:100%;">
                    <a   id="search_btn" class="mini-button" style="display: none"     onclick="search">刷新</a>
                    <a   id="add_btn" class="mini-button" style="display: none"     onclick="add">新增</a>
                    <a   id="edit_btn" class="mini-button" style="display: none"     onclick="edit">修改</a>
                    <a   id="collapse_btn" class="mini-button" style="display: none"     onclick="collapse">收起</a>
                </td>
                </tr>
            </table>
        </div>
            
        
    
        <div id="tradeManage" class="mini-fit" style="margin-top: 2px;">  
            <div id="treegrid1" class="mini-treegrid borderAll" showTreeIcon="true" 
                treeColumn="prdTypeName" idField="prdTypeNo" parentField="supPrdTypeNo" resultAsTree="false"
                expandOnLoad="true" style="width:100%;height:100%;"  onRowdblclick="custDetail"
                sortMode="client" allowAlternating="true">
                <div property="columns">
                    <div type="indexcolumn" headerAlign="center" align="center" width="20">序号</div>                        
                    <div field="prdTypeNo" name="prdTypeNo" width="60" headerAlign="center" align="center" allowSort="true" dataType="int">产品类型代码</div>
                    <div field="prdTypeName" name="prdTypeName" width="150" headerAlign="center" >产品类型名称</div>
                    <div field="prdTypeDesc" name="prdTypeDesc" width="150"headerAlign="center" >产品种类描述</div>
                </div>
            </div>
        </div>

    <script>
        mini.parse();

        $(document).ready(function(){
            //控制按钮显示
            $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
                search();
            });
        });

        top["productTypeManage"]=window;
        function getData(){
            var grid = mini.get("treegrid1");
            var record =grid.getSelected(); 
            return record;
        }
        //全部收起
        function collapse(){
            var treegrid1 = mini.get("treegrid1");
            treegrid1.collapseAll();
        }

        //查询
        function search(){
            CommonUtil.ajax({
                url:"/ProductTypeController/searchProductType",
                callback:function(data){
                    var grid = mini.get("treegrid1");
                    grid.setData(data.obj);
                }
            });
        }

         
        function add() {
            var grid = mini.get("treegrid1");
            var  record =grid.getSelected();
            
            var url = CommonUtil.baseWebPath() + "/mini_base/MiniProductTypeEdit.jsp?action=new";
            var tab = { id: "ProductTypeManageAdd", name: "ProductTypeManageAdd", text: "产品类型新增", url: url,showCloseButton:true,parentId:top['win'].tabs.getActiveTab().name };
            top['win'].showTab(tab);
		}
        
        function edit(){
            var grid = mini.get("treegrid1");
            var  record =grid.getSelected();
            if(record){
                var url = CommonUtil.baseWebPath() + "/mini_base/MiniProductTypeEdit.jsp?action=edit";
                var tab = { id: "ProductTypeManageEdit", name: "ProductTypeManageEdit", text: "产品类型修改", url: url,showCloseButton:true,parentId:top['win'].tabs.getActiveTab().name };
                top['win'].showTab(tab);
            }else{
                mini.alert("请选中一条记录!","系统提示"); 
            }
        }
        //双击一行打开详情信息
        function custDetail(e){
            var grid = e.sender;
            var row = grid.getSelected();
            if(row){
                var url = CommonUtil.baseWebPath() + "/mini_base/MiniProductTypeEdit.jsp?action=detail";
                var tab = { id: "ProductTypeManageDetail", name: "ProductTypeManageDetail", text: "产品类型详情", url: url,showCloseButton:true,parentId:top['win'].tabs.getActiveTab().name };
                top['win'].showTab(tab);
            }
        }

        

    </script>


</body>
</html>