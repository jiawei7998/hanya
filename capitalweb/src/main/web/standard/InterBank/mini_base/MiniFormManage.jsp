<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%> 
<html>
<head>
	<title>表单</title>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script> 
</head>
<body style="width:100%;height:100%;background:white">
	<fieldset id="fd2" class="mini-fieldset">
		<legend><label>查询条件</label></legend>
		<div id="find" class="fieldset-body">
			<div id="search_form"   >
					<input id="formInst" name="formInst" class="mini-buttonedit" 
					onbuttonclick="CommonUtil.getInstList" labelField="true"  label="所属机构："  
					emptyText="请选择机构" labelStyle="text-align:right;"/>
					<input id="formName" name="formName" class="mini-textbox" labelField="true"  
					label="表单名称："  emptyText="请输入表单名称" vtype="maxLength:100"
					labelStyle="text-align:right;"/>
				
					<input id="formType" name="formType" class="mini-combobox" labelField="true"  
					label="表单类型："  emptyText="请选择表单类型" labelStyle="text-align:right;"
					data="CommonUtil.serverData.dictionary.formType"/>
					<input id="isTemplate" name="isTemplate" class="mini-combobox" labelField="true"  
					label="是否模板："  emptyText="请选择是否模板" data="CommonUtil.serverData.dictionary.YesNo"
					labelStyle="text-align:right;"/>
					<input id="useFlag" name="useFlag" class="mini-combobox" labelField="true"  
					label="启用标志："  emptyText="请选择启用标志" data="CommonUtil.serverData.dictionary.Status"
					labelStyle="text-align:right;"/>
					<span style="float:right;margin-right: 150px">
						<a id="search_btn" class="mini-button" style="display: none"    onclick="search1">查询</a>
						<a id="clear_btn" class="mini-button" style="display: none"    onClick="clear">清空</a>
					</span>
			</div>
			
		</div>
	</fieldset>
	<span style="margin:2px;display: block;">
		<a id="add_btn" class="mini-button" style="display: none"    onclick="add">新增</a>
		<a id="edit_btn" class="mini-button" style="display: none"    onclick="modifyedit">修改</a>
		<a id="delete_btn" class="mini-button" style="display: none"    onclick="removeRows">删除</a>
	</span>

	
	<div id="tradeManage" class="mini-fit" style="margin-top: 2px;"> 
		<div id="form_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" 
		idField="formNo" allowResize="true" pageSize="10" multiSelect="true" onRowdblclick="formDetail"
		sortMode="client" allowAlternating="true">
			<div property="columns" >
				<div type="indexcolumn"  headerAlign="center" align="center" width="40px">序号</div>
				<div type="checkcolumn"></div>
				<div field="formNo" headerAlign="center"  align="center" width="80px" allowSort="true" dataType="int">表单编号</div>    
				<div field="formInstName"  headerAlign="center" align="center" width="120px">所属机构</div>                            
				<div field="formName"  headerAlign="center" width="220px">表单名称</div>
				<div field="formType"   headerAlign="center"  align="center" width="80px"renderer="CommonUtil.dictRenderer" data-options="{dict:'formType'}">表单类型</div>                                
				<div field="formDesc"   headerAlign="center" width="220px">表单描述</div>
				<div field="isTemplate"  headerAlign="center"  align="center" width="60px" renderer="CommonUtil.dictRenderer" data-options="{dict:'YesNo'}">是否模版</div>
				<div field="useFlag"     headerAlign="center"  align="center"width="60px" renderer="CommonUtil.dictRenderer" data-options="{dict:'Status'}">启用标志</div>
				<div field="lastUpdate" headerAlign="center"  align="center" width="140px" allowSort="true" dataType="date">更新时间</div>                                                                                               
			</div>
		</div>
	</div>
</body>
    <script>
		
		mini.parse();
		var grid =mini.get("form_grid");

		top['formManage'] = window;

		$(document).ready(function(){
			//控制按钮显示
			$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
				var grid = mini.get("form_grid");
				grid.on("beforeload", function (e) {
					e.cancel = true;
					var pageIndex = e.data.pageIndex;
					var pageSize = e.data.pageSize;
					search(pageSize, pageIndex);
				});
				search(grid.pageSize, 0);
			});
		});

        function search1(){
            var grid =mini.get("form_grid");
            search(grid.pageSize,0); 
        }

		function search(pageSize,pageIndex){
            var form =new mini.Form("search_form");
            form.validate();
            if (form.isValid() == false) return;//表单验证
            var data =form.getData();//获取表单数据
            data['pageNumber']=pageIndex+1;
            data['pageSize']=pageSize;
            var param = mini.encode(data); //序列化成JSON
            CommonUtil.ajax({
                url:"/FormController/searchPageForm",
                data:param,
                callback:function(data){
                        
                    var grid =mini.get("form_grid");
                    //设置分页
                    grid.setTotalCount(data.obj.total);
                    grid.setPageIndex(pageIndex);
                    grid.setPageSize(pageSize);
                    //设置数据
                    grid.setData(data.obj.rows);
                }
            });
        } 


		

		//清空
		function clear(){
			var form =new mini.Form("search_form");
			form.clear();
			search1();
			
    	}

		//删除
	function removeRows() {
        var grid = mini.get("form_grid");
        //获取选中行
        var rows =grid.getSelecteds();
        
        if (rows.length>0) {
            var formNos = new Array();
            
            for(var i=0;i<rows.length;i++){
                
                formNos.push(rows[i].formNo);
            }
            
            mini.confirm("您确认要删除选中记录?","系统警告",function(value){   
                if (value == "ok"){   
                    CommonUtil.ajax({
                        url : '/FormController/removeForm/',
                        data : formNos,
                        callback : function(data){
                            if("error.common.0000" == data.code){
                                mini.alert("删除成功！","系统提示");
                                search1();
                            }
                        }
                    });
                
                }   
            });       
                
        }else{
            mini.alert("请至少选择一条记录!","系统提示");
        }
    }

		
		//新增
		function add(){
			var tab = {
					id:"formAdd", 
					name:"formAdd",
					iconCls:null, 
					title: "选择模版" ,
					url:CommonUtil.baseWebPath() + "/mini_base/MiniFormList.jsp",
					showCloseButton:true
				};
			//打开Tabs
			CommonUtil.openNewMenuTab(tab,{});
		}
		//修改
		function modifyedit(){
			var grid = mini.get("form_grid");
			//获取选中行
			var rows =grid.getSelecteds();
			if(rows.length != 1){
				mini.alert('请选择一条数据','提示');
				return;
			};
			var tab ={
				id:'formEdit',
				name:'formEdit',
				iconCls:null,
				title:'表单修改',
				url: CommonUtil.baseWebPath() +'/mini_base/MiniFormEdit.jsp',
				showCloseButton:true,
				parentId:top['win'].tabs.getActiveTab().name 
			};
			var param ={'type':'edit',formNo:rows[0].formNo};
			//打开Tabs
			CommonUtil.openNewMenuTab(tab,param);
		}
		//详情
		function formDetail(){
			var grid = mini.get("form_grid");
			//获取选中行
			var rows =grid.getSelecteds();
			if(rows.length != 1){
				mini.alert('请选择一条数据','提示');
				return;
			};
			var tab ={
				id:'formEdit',
				name:'formEdit',
				iconCls:null,
				title:'表单详情',
				url: CommonUtil.baseWebPath() +'/mini_base/MiniFormEdit.jsp',
				showCloseButton:true,
				parentId:top['win'].tabs.getActiveTab().name 
			};
			
			var param ={'type':'detail',formNo:rows[0].formNo};
			//打开Tabs
			CommonUtil.openNewMenuTab(tab,param);
		}

    </script>


</html>