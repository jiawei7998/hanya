﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
	<%@ include file="../../global.jsp"%>
		<html>
		<head>
			<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
			<title></title>
		</head>

		<body style="width:100%;height:100%;background:white">
			<fieldset class="mini-fieldset">
				<legend>附件关联配置</legend>
				<div id="accout_form" style="width:100%;">
					<table  width="100%" class="mini-table">
					<tr>
						<td>
							<input id="prtype" name="prtype" class="mini-combobox" data="CommonUtil.serverData.dictionary.treeRefType" value="1" labelField="true"
							 labelStyle="text-align:right;" label="关联类型选择："  emptyText="" style="width:320px" required="true"  requiredErrorText="该输入项为必输项">
							<input id="refType" name="refType" class="mini-hidden" value="" />
							<input id="refName" name="refName" class="mini-hidden" />
							<input id="refId" name="refId" class="mini-hidden" />
							<input id="cno" name="cno" class="mini-buttonedit searchbox" labelField="true" onbuttonclick="onButtonEdit" label="请选择："
							 labelStyle="text-align:right;" emptyText="" value="请选择" style="width:320px" required="true"  requiredErrorText="该输入项为必输项"/>
							<input id="treeId" name="treeId" class="mini-buttonedit searchbox" labelField="true" label="附件树选择：" onbuttonclick="onButtonEdits"
							 labelStyle="text-align:right;" emptyText="" style="width:320px" required="true"  requiredErrorText="该输入项为必输项"/>
							<input id="treeName" name="treeName" class="mini-hidden" />
						</td>
					</tr>
					<tr>
						<td colspan="3" style="text-align:center;">
							<a id="save_btn" class="mini-button" style="display: none"    onclick="save">保存</a>
						</td>
					</tr>
				</table>
				</div>
			</fieldset>
			<div class="mini-fit">
			<fieldset class="mini-fieldset" style="height:90%">
				<legend>对应附件树信息</legend>
				<div class="mini-fit">
					<br/>
					<ul id="file_tree" class="mini-tree" showTreeIcon="true" textField="text" idField="id" value="nodeName"
					 expandOnNodeClick="true" expandOnLoad="true">
					</ul>
					<br/>
				</div>
			</fieldset>
		</div>
		<script>
			mini.parse();
			var deptCombo = mini.get("prtype");
			var positionCombo = mini.get("cno");
			
			function save() {
				var form = new mini.Form("#accout_form");
				form.validate();
				if (form.isValid() == false) {
					mini.alert("交易验证失败,请检查表单是否填写正确!", "消息提示")
					return;
				}

				//提交数据
				var data = form.getData(true); //获取表单多个控件的数据  
				var param = mini.encode(data); //序列化成JSON
				CommonUtil.ajax({
					url: "/TcAttachTreeRefController/insertTcAttachTreeFef",
					data: param,
					callback: function (data) {
						if (data.code == 'error.common.0000') {
							mini.alert("保存成功")
						} else {
							mini.alert("保存失败");
						}
					}
				});
			}
			function onButtonEdit(e) {
				var selectUrl = "";
				var title = "";
				var id = deptCombo.getValue();
				if(id=="1"){
					selectUrl = CommonUtil.baseWebPath() +"/mini_cp/MiniCoreCustQueryEdits.jsp";
					title = "客户选择";
				}else{
					selectUrl = CommonUtil.baseWebPath() +"/mini_base/MiniProductSelect.jsp";
					title = "产品选择";
				}
				var btnEdit = this;
				mini.open({
					url: selectUrl,
					title: title,
					width: 600,
					height: 500,
					ondestroy: function (action) {
						if (action == "ok") {
							var iframe = this.getIFrameEl();
							var data = iframe.contentWindow.GetData();
							data = mini.clone(data); 
							if (data) {
								if (id=="1"){
									mini.get("refId").setValue(data.party_id);
									mini.get("refName").setValue(data.party_name);
									mini.get("refType").setValue(id);
									btnEdit.setText(data.party_name);
								}else{
									mini.get("refType").setValue(id);
									mini.get("refId").setValue(data.prdNo);
									mini.get("refName").setValue(data.prdName);
									btnEdit.setText(data.prdName);
								}
							}
							btnEdit.focus();
						}
					}
				});
			}
			
			function onButtonEdits(e) {
				var btnEdit = this;
				mini.open({
					url: CommonUtil.baseWebPath() +"/mini_base/MiniAttachTreeSelect.jsp",
					title: "附件树选择",
					width: 600,
					height: 500,
					ondestroy: function (action) {
						if (action == "ok") {
							var iframe = this.getIFrameEl();
							var data = iframe.contentWindow.GetData();
							data = mini.clone(data); 
							if (data) {
								btnEdit.setValue(data.treeId);
								btnEdit.setText(data.treeName);
								mini.get("treeName").setValue(data.treeName);
								showTree();
							}
							btnEdit.focus();
						}

					}
				
				});
			}
		
		function showTree(){
			var treeId = mini.get("treeId").getValue();
			CommonUtil.ajax({
				url: '/TcAttachTreeRefController/searchTcAttachTreeRef',
				data: {"treeId":treeId},
				callback: function (data) {
					mini.get("file_tree").setData(data.obj);
				}
			});
		}
			
	</script>
</body>
</html>