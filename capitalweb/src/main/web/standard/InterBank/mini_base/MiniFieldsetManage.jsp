<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%> 
<html>
<head>
    <title>字段分组</title>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script> 
</head>
<body style="width:100%;height:100%;background:white">
	<fieldset id="fd2" class="mini-fieldset">
		<legend><label>查询条件</label></legend>
		<div id="find" class="fieldset-body">
			<div id="search_form" >
                <input id="fdsName" name="fdsName" class="mini-textbox" vtype="maxLength:100" 
                labelField="true"  label="字段分组名称："  emptyText="请输入字段分组名称"
                labelStyle="text-align:right;"/>
                <input id="fdsType" name="fdsType" class="mini-combobox" labelField="true"  
                label="字段分组种类：" emptyText="请选择字段分组种类" labelStyle="text-align:right;"
                data="CommonUtil.serverData.dictionary.fldType"/>
            
                <input id="useFlag" name="useFlag" class="mini-combobox" labelField="true" 
                label="启用标志："  emptyText="请选择启用标志" labelStyle="text-align:right;"
                data="CommonUtil.serverData.dictionary.Status"/>

                <span style="float:right;margin-right: 150px">
                    <a id="search_btn" class="mini-button" style="display: none"    onclick="search1()">查询</a>
                    <a id="clear_btn" class="mini-button" style="display: none"    onClick="clear">清空</a>
                    
                </span>
                        
            </div>
            
		</div>
    </fieldset>
    <span style="margin:2px;display: block;">
        <a id="add_btn" class="mini-button" style="display: none"    onclick="add">新增</a>
        <a id="edit_btn" class="mini-button" style="display: none"    onclick="modifyedit()">修改</a>
        <a id="delete_btn" class="mini-button" style="display: none"    onclick="removeRows">删除</a>
    </span>

	
    <div id="tradeManage" class="mini-fit" style="margin-top: 2px;">
        <div id="field_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" 
        idField="p_code" allowResize="true" pageSize="10" multiSelect="true"
         onRowdblclick="custDetail" sortMode="client" allowAlternating="true">
            <div property="columns" >
                <div type="indexcolumn" headerAlign="center" width="20px" align="center">序号</div>
                <div type="checkcolumn" width="20px" ></div>
                <div field="fdsNo"  headerAlign="center" width="50px" align="center" allowSort="true" dataType="int">字段分组编号</div>    
                <div field="fdsName"  headerAlign="center" width="100px">字段分组名称</div>                            
                <div field="fdsType"   headerAlign="center"  width="80px" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'fldType'}">字段分组种类</div>
                <div field="useFlag"   headerAlign="center" width="40px" align="center"renderer="CommonUtil.dictRenderer" data-options="{dict:'Status'}">启用标志</div>
                <div field="lastUpdate"  headerAlign="center" width="160px" align="center" allowSort="true" dataType="date">更新时间</div>                                                                                               
            </div>
        </div>
    </div>
    <script>

        mini.parse();
		
		$(document).ready(function(){
            //控制按钮显示
            $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
                var grid = mini.get("field_grid");
                grid.on("beforeload", function (e) {
                    e.cancel = true;
                    var pageIndex = e.data.pageIndex;
                    var pageSize = e.data.pageSize;
                    search(pageSize, pageIndex);
                });
                search(grid.pageSize, 0);
            });
		});

        function search(pageSize,pageIndex){
			var form =new mini.Form("find");
			form.validate();
			if (form.isValid() == false) return;//表单验证
			var data =form.getData();//获取表单数据
			data['pageNumber']=pageIndex+1;
			data['pageSize']=pageSize;
			var param = mini.encode(data); //序列化成JSON
			CommonUtil.ajax({
				url:"/FieldsetController/searchFieldsetPage",
				data:param,
				callback:function(data){
						var grid =mini.get("field_grid");
						//设置分页
						grid.setTotalCount(data.obj.total);
						grid.setPageIndex(pageIndex);
						grid.setPageSize(pageSize);
						//设置数据
						grid.setData(data.obj.rows);
					}
				});
		}

        function search1(){
            var grid =mini.get("field_grid");
            search(grid.pageSize,0); 
        }

        //清空
        function clear(){
            var form =new mini.Form("find");
            form.clear();
            search1();
        }
         //删除
        function removeRows() {
            var grid =mini.get("field_grid");
            //获取选中行
            var rows =grid.getSelecteds();
            
            if (rows.length>0) {
                var fldNos = new Array();
                var type0Flag = 0;
                for(var i=0;i<rows.length;i++){
                    if(rows[i].fdsType == 0){//如果字段种类是预定义字段
                        type0Flag = 1;
                    }
                    fldNos.push(rows[i].fdsNo);
                }
                
                mini.confirm(type0Flag==0?"您确认要删除选中记录?":"您选中的记录中含有不可删除的预定义字段,是否跳过预定义字段继续执行删除操作?","系统警告",function(value){   
                    if (value == "ok"){   
                        CommonUtil.ajax({
                            url : '/FieldsetController/removeFieldset/',
                            data : fldNos,
                            callback : function(data){
                                if("error.common.0000" == data.code){
                                    mini.alert("删除成功！","系统提示");
                                    search1();
                                }
                            }
                        });
                    
                    }   
                });       
                    
            }else{
                mini.alert("请至少选择一条记录!","系统提示");
            }
        }

    top["fieldSetManage"]=window;
    function getData(){
        var grid =mini.get("field_grid");
        var record =grid.getSelected(); 
        return record;
    }

    //新增
    function add(){
        var url = CommonUtil.baseWebPath() + "/mini_base/MiniFieldsetEdit.jsp?action=new";
        var tab = { id: "fieldsetManageAdd", name: "fieldsetManageAdd", text: "字段分组新增", url: url,showCloseButton:true,parentId:top['win'].tabs.getActiveTab().name };
        top['win'].showTab(tab);
    }
    //修改
    function modifyedit(){
        var grid =mini.get("field_grid");
        var  record =grid.getSelected();
        if(record){
            var url = CommonUtil.baseWebPath() + "/mini_base/MiniFieldsetEdit.jsp?action=edit";
            var tab = { id: "fieldsetManageEdit", name: "fieldsetManageEdit", text: "字段分组修改", url: url,showCloseButton:true,parentId:top['win'].tabs.getActiveTab().name };
            top['win'].showTab(tab);
        }else{
            mini.alert("请选中一条记录!","系统提示"); 
        }
    }
    //双击一行打开详情信息
    function custDetail(e){
        var grid = e.sender;
        var row = grid.getSelected();
        if(row){
            var url = CommonUtil.baseWebPath() + "/mini_base/MiniFieldsetEdit.jsp?action=detail";
            var tab = { id: "fieldsetManageDetail", name: "fieldsetManageDetail", text: "字段分组详情", url: url ,showCloseButton:true,parentId:top['win'].tabs.getActiveTab().name};
            top['win'].showTab(tab);
        }
    }

     
    </script>
</body>
</html>