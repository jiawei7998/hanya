<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
	<%@ include file="../../../global.jsp"%>
		<html>
		<head>
			<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
			<title>会计查询会计分录查询</title>
		</head>

		<body style="width:100%;height:100%;background:white">
			<fieldset class="mini-fieldset title">
				<legend>查询</legend>
				<div id="search_form" style="width:100%;">
					<input id="dealNo" name="dealNo" class="mini-textbox" labelField="true" label="交易流水号：" labelStyle="text-align:right;" emptyText="请输入交易流水号"
					/>
					<input id="subjCode" name="subjCode" class="mini-textbox" labelField="true" label="科目号：" labelStyle="text-align:right;" emptyText="请输入科目号"
					/>
					<input id="eventId" name="eventId" class="mini-ComboBox" emptyText="请选择会计事件..." textField="eventName" valueField="eventId"
					 labelField="true" label="会计事件：" labelStyle="text-align:right;" />
					<input id="taskId" name="taskId" class="mini-ComboBox" emptyText="请选择分录帐套..." textField="taskName" valueField="taskId" labelField="true"
					 label="所属帐套：" labelStyle="text-align:right;" />
					<input id="entry_date_begin" name="entry_date_begin" class="mini-datepicker" labelField="true" label="账务日期：" valueType="string" labelStyle="text-align:right;"
					 emptyText="请选择账务开始日期..." />
					<input id="entry_date_end" name="entry_date_end" class="mini-datepicker" labelField="true" label="~" valueType="string" onValuechanged="compareDate" labelStyle="text-align:center;"
					 emptyText="请选择账务结束日期..." />
					<span style="float:right;margin-right: 160px">
						<a id="search_btn" class="mini-button" style="display: none"   onclick="query">查询</a>
						<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear">清空</a>
					</span>
				</div>
			</fieldset>
			<div id="EntriesQuerys" class="mini-fit" style="margin-top: 5px;">
				<div id="form_grid" class="mini-datagrid borderAll" allowAlternating="true" style="width:100%;height:100%;" sortMode="client">
					<div property="columns">
						<div type="indexcolumn" headerAlign="center" align="center" width="50px">序号</div>
						<div field="flowId" width="100" allowSort="true" headerAlign="center" align="center">账务流水号</div>
						<div field="flowSeq" width="100" allowSort="true" headerAlign="center" align="center">序号</div>
						<div field="taskName" width="120" allowSort="false" headerAlign="center" align="center">所属帐套</div>
						<div field="postDate" width="120" allowSort="true" headerAlign="center" align="center">账务日期</div>
						<div field="dealNo" width="200" allowSort="false" headerAlign="center" align="center">交易流水号</div>
						<div field="prdNo" width="100" allowSort="false" headerAlign="center" align="center">产品类型</div>
						<div field="sponInst" width="100" allowSort="false" headerAlign="center" align="center">交易机构</div>
						<div field="eventName" width="100" allowSort="false" headerAlign="center" align="">会计事件</div>
						<div field="ccy" width="100" allowSort="false" headerAlign="center" align="left" renderer="CommonUtil.dictRenderer" data-options="{dict:'Currency'}">币种</div>
						<div field="debitCreditFlag" width="100" allowSort="false" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'DebitCreditFlag'}" >借贷标识</div>
						<div field="subjCode" width="100" allowSort="true" headerAlign="center" align="center">科目号</div>
						<div field="subjName" width="250" allowSort="false" headerAlign="center">科目名称</div>
						<div field="value" width="100" numberFormat="#,0.00" headerAlign="center" align="right" allowSort="true">金额</div>
						<div field="core_acct_code" width="100" allowSort="false" headerAlign="center" align="true">核心账号</div>
						<div field="updateTime" width="150" allowSort="false" headerAlign="center" align="true">更新日期</div>
					</div>
				</div>
			</div>
			<script type="text/javascript">
				mini.parse();
				var grid = mini.get("form_grid");
				grid.on("beforeload", function (e) {
					e.cancel = true;
					var pageIndex = e.data.pageIndex;
					var pageSize = e.data.pageSize;
					search(pageSize, pageIndex);
				});
				function search(pageSize, pageIndex) {
					var form = new mini.Form("search_form");
					if (form.isValid == false)
						return;
					//提交数据
					var data = form.getData();//获取表单多个控件的数据  
					data['pageNumber'] = pageIndex + 1;
					data['pageSize'] = pageSize;
					var param = mini.encode(data); //序列化成JSON
					CommonUtil.ajax({
						url: '/TbkEntryController/searchPageEntries',
						data: param,
						callback: function (data) {
							var grid = mini.get("form_grid");
							grid.setTotalCount(data.obj.total);
							grid.setPageIndex(pageIndex);
							grid.setPageSize(pageSize);
							grid.setData(data.obj.rows);
						}
					});
				}

				function clear() {
					var form = new mini.Form("#search_form");
					form.clear();
				}

				function loadtaskName() {
					var param = mini.encode({}); //序列化成JSON
					CommonUtil.ajax({
						url: '/TtAccountingTaskController/selectAccountingTaskList',
						data: param,
						callback: function (data) {
							var grid = mini.get("taskId");
							grid.setData(data.obj);
						}
					});
				}

				function compareDate() {
						var sta = mini.get("entry_date_begin");
						var end = mini.get("entry_date_end");
						if (sta.getValue() > end.getValue() && end.getValue("")) {
							mini.alert("开始时间不能大于结束时间", "系统提示");
							return end.setValue(sta.getValue());
						}
					}	
				function loadTbkEvent() {
					var param = mini.encode({}); //序列化成JSON
					CommonUtil.ajax({
						url: '/TbkEntrySceneTreeController/searchPageTtAccountingEvent',
						data: param,
						callback: function (data) {
							var grid = mini.get("eventId");
							grid.setData(data.obj.rows);
						}
					});
				}

				//查询的方法
				function query() {
					var grid = mini.get("form_grid");
					search(grid.pageSize, 0);
				}

				$(document).ready(function () {
					search(10, 0);
					loadtaskName();
					loadTbkEvent();
				})

			</script>
		</body>
		</html>