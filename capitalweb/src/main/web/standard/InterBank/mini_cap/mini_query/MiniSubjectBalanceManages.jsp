<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
	<%@ include file="../../../global.jsp"%>
		<html>
		<head>
			<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
			<title>科目余额查询</title>
		</head>

		<body style="width:100%;height:100%;background:white">
			<fieldset class="mini-fieldset title">
				<legend>查询</legend>
				<div id="search_form" style="width:100%;">
					<input id="subjCode" name="subjCode" class="mini-textbox" labelField="true" label="科目代码：" labelStyle="text-align:right;"
					 emptyText="请输入科目代码" />
					<input id="subjName" name="subjName" class="mini-textbox" labelField="true" label="科目名称：" labelStyle="text-align:right;"
					 emptyText="请输入科目名称" />
					<input id="taskId" name="taskId" class="mini-ComboBox" emptyText="请选择所属账套..." textField="taskName" valueField="taskId" labelField="true"
					 label="所属账套：" labelStyle="text-align:right;"/>
					<span style="float:right;margin-right: 160px">
						<a id="search_btn" class="mini-button" style="display: none"   onclick="query">查询</a>
						<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear">清空</a>
					</span>
			</fieldset>
			<div id="tradeManage" class="mini-fit" style="margin-top: 5px;">
				<div id="subjectBalabce_grid" class="mini-datagrid borderAll" allowAlternating="true" style="width:100%;height:100%;" sortMode="client">
					<div property="columns">
						<div type="indexcolumn"></div>
						<div field="taskName" width="80" allowSort="false" headerAlign="center" align="center">所属账套</div>
						<div field="ccy" width="80" allowSort="false" headerAlign="center" align="left" renderer="CommonUtil.dictRenderer" data-options="{dict:'Currency'}">币种</div>
						<div field="subjCode" width="120" allowSort="true" headerAlign="center" align="center">科目代码</div>
						<div field="subjName" width="200" allowSort="false" headerAlign="center">科目名称</div>
						<div field="debitValue" width="80" numberFormat="#,0.00" headerAlign="center" align="right" allowSort="true">借方余额</div>
						<div field="creditValue" width="80" numberFormat="#,0.00" headerAlign="center" align="right" allowSort="true">贷方余额</div>
						<div field="receiveValue" width="80" numberFormat="#,0.00" headerAlign="center" align="right" allowSort="true">收方余额</div>
						<div field="payValue" width="80" numberFormat="#,0.00" headerAlign="center" align="right" allowSort="true">付方余额</div>
						<div field="bDebitValue" width="80" numberFormat="#,0.00" headerAlign="center" align="right" allowSort="true">期初借方余额</div>
						<div field="bCreditValue" width="80" numberFormat="#,0.00" headerAlign="center" align="right" allowSort="true">期初贷方余额</div>
						<div field="bReceiveValue" width="80" numberFormat="#,0.00" headerAlign="center" align="right" allowSort="true">期初收方余额</div>
						<div field="bPayValue" width="80" numberFormat="#,0.00" headerAlign="center" align="right" allowSort="true">期初付方余额</div>
						<div field="updateTime" width="120" allowSort="false" headerAlign="center" align="center" allowSort="true">更新时间</div>
					</div>
				</div>
			</div>
			<script type="text/javascript">
				mini.parse();
				var grid = mini.get("subjectBalabce_grid");
				grid.on("beforeload", function (e) {
					e.cancel = true;
					var pageIndex = e.data.pageIndex;
					var pageSize = e.data.pageSize;
					search(pageSize, pageIndex);
				});
				function search(pageSize, pageIndex) {
					var form = new mini.Form("search_form");
					if (form.isValid == false)
						return;
					//提交数据
					var data = form.getData();//获取表单多个控件的数据  
					data['pageNumber'] = pageIndex + 1;
					data['pageSize'] = pageSize;
					var param = mini.encode(data); //序列化成JSON
					CommonUtil.ajax({
						url: '/TbkSubjectBalanceSumController/searchPageTbkSubjectBalanceSum',
						data: param,
						callback: function (data) {
							var grid = mini.get("subjectBalabce_grid");
							grid.setTotalCount(data.obj.total);
							grid.setPageIndex(pageIndex);
							grid.setPageSize(pageSize);
							grid.setData(data.obj.rows);
						}
					});
				}
				function clear() {
					var form = new mini.Form("#search_form");
					form.clear();
				}
				function loadtaskName() {
					var param = mini.encode({}); //序列化成JSON
					CommonUtil.ajax({
						url: '/TtAccountingTaskController/selectAccountingTaskList',
						data: param,
						callback: function (data) {
							var grid = mini.get("taskId");
							grid.setData(data.obj);
						}
					});
				}
				//查询的方法
				function query() {
					var grid = mini.get("subjectBalabce_grid");
					search(grid.pageSize, 0);
				}

				$(document).ready(function () {
					search(10, 0);
					loadtaskName();
				})
			</script>
		</body>
		</html>