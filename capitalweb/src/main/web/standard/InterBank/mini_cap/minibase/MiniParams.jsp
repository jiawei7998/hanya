<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
	<%@ include file="../../../global.jsp"%>
		<html>
		<head>
			<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
			<title>维度权重管理</title>
		</head>

		<body style="width:100%;height:100%;background:white">
			<fieldset class="mini-fieldset">
				<legend>查询</legend>
				<div id="search_form" style="width:100%;">
					<input id="paramId" name="paramId" class="mini-textbox" labelField="true" label="维度编号：" labelStyle="text-align:right;" emptyText="请输入维度编号"
					/>
					<input id="paramName" name="paramName" class="mini-textbox" labelField="true" label="维度名称：" labelStyle="text-align:right;"
					 emptyText="请输入维度名称" />
					<input id="paramQz" name="paramQz" class="mini-textbox" labelField="true" label="维度权重：" labelStyle="text-align:right;" emptyText="请输入维度权重"
					/>
					<span style="float:right;margin-right: 160px">
						<a id="search_btn" class="mini-button" style="display: none"   onclick="query">查询</a>
						<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear">清空</a>
					</span>

				</div>
			</fieldset>
			<span style="margin:2px;display: block;">
				<a id="add_btn" class="mini-button" style="display: none"   onclick="add">新增</a>
				<a id="edit_btn" class="mini-button" style="display: none"   onclick="active">是否/启用</a>
				<a id="delete_btn" class="mini-button" style="display: none"   onclick="del">删除</a>
			</span>
			<div id="Params" class="mini-fit" style="margin-top: 2px;">
				<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" allowAlternating="true" allowResize="true"
				 onrowdblclick="onRowDblClick" border="true" sortMode="client">
					<div property="columns">
						<div type="indexcolumn" headerAlign="center" width="30">序号</div>
						<div field="paramId" width="" headerAlign="center" allowSort="true" align="">维度编号</div>
						<div field="paramName" width="" align="" headerAlign="center" allowSort="">维度名称</div>
						<div field="paramQz" width="" align="" headerAlign="center" allowSort="">维度权重</div>
						<div field="paramSource" width="" align="" headerAlign="center" allowSort="">维度字段</div>
						<div field="active" width="" align="center" headerAlign="center" allowSort="" renderer="CommonUtil.dictRenderer" data-options="{dict:'YesNo'}">是否启用</div>
						<div field="lastUpdate" width="" align="center" headerAlign="center" allowSort="true">最后更新时间</div>
					</div>
				</div>
			</div>

			<script>
				mini.parse();
				top['Params'] = window;
				var grid = mini.get("datagrid");
				grid.on("beforeload", function (e) {
					e.cancel = true;
					var pageIndex = e.data.pageIndex;
					var pageSize = e.data.pageSize;
					search(pageSize, pageIndex);
				});

				function search(pageSize, pageIndex) {
					var form = new mini.Form("#search_form");
					form.validate();
					if (form.isValid() == false) {
						mini.alert("表单填写错误,请确认!", "提示信息")
						return;
					}

					var data = form.getData();
					data['pageNumber'] = pageIndex + 1;
					data['pageSize'] = pageSize;
					var params = mini.encode(data);
					CommonUtil.ajax({
						url: '/ParamsController/searchParamsPage',
						data: params,
						callback: function (data) {
							var grid = mini.get("datagrid");
							grid.setTotalCount(data.obj.total);
							grid.setPageIndex(pageIndex);
							grid.setPageSize(pageSize);
							grid.setData(data.obj.rows);
						}
					});

				}


				function checkBoxValuechanged() {
					search(grid.pageSize, 0);
				}


				//双击详情页
				function onRowDblClick(e) {
					var row = grid.getSelected();
					if (row) {
						var url = CommonUtil.baseWebPath() + "/mini_cap/minibase/MiniParamsEdits.jsp?action=detail";
						var tab = { id: "ParamsDetail", name: "ParamsDetail", title: "维度详情", url: url, showCloseButton: true };
						top["win"].openNewTab(tab, row);

					} else {
						mini.alert("请选中一条记录！", "消息提示");
					}
				}

				//新增方法
				function add() {
					var url = CommonUtil.baseWebPath() + "/mini_cap/minibase/MiniParamsEdits.jsp?action=add";
					var tab = { id: "ParamsAdd", name: "ParamsAdd", text: "维度新增", url: url, showCloseButton: true, parentId: top["win"].tabs.getActiveTab().name };
					top['win'].showTab(tab);
				}

				//查询的方法
				function query() {
					search(grid.pageSize, 0);
				}

				function clear() {
					var form = new mini.Form("#search_form");
					form.clear();
				}
				//删除
				function del() {
					var row = grid.getSelected();
					if (row) {
						mini.confirm("您确认要删除选中记录?", "系统警告", function (value) {
							if (value == "ok") {
								CommonUtil.ajax({
									url: "/ParamsController/deleteParams/",
									data: { paramId: row.paramId },
									callback: function (data) {
										if (data.code == 'error.common.0000') {
											mini.alert("删除成功")
											search(grid.pageSize, grid.pageIndex);
										} else {
											mini.alert("删除失败");
										}
									}
								});
							}
						});

					}

					else {
						mini.alert("请选中一条记录！", "消息提示");
					}

				}

				function getData(action) {
					if (action != "add") {
						var row = null;
						row = grid.getSelected();
					}
					return row;
				}

				//改变启用状态的方法
				function active() {
					var grid = mini.get("datagrid");
					var row = grid.getSelected();
					if (row) {
						mini.confirm("您确认要切换维度的状态?", "系统警告", function (value) {
							if (value == "ok") {
								CommonUtil.ajax({
									url: "/ParamsController/changeParamsStatus/",
									data: { paramId: row.paramId },
									callback: function (data) {
										mini.alert("已成功切换状态", "系统提示");
										search(grid.pageSize, grid.pageIndex);
									}
								});

							}
						});
					}

					else {
						mini.alert("请选中一条记录！", "消息提示");
					}

				}

				$(document).ready(function () {
					search(10, 0);
				});

			</script>
		</body>
		</html>