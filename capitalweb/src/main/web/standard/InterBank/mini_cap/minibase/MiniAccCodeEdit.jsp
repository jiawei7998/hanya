<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
	<%@ include file="../../../global.jsp"%>
		<html>
		<head>
			<script src="<%=basePath%>sl/TaDictController/dictionary.js" type="text/javascript"></script>
			<title></title>
		</head>
		<body>
			<div id="form1">
				<div id="AccCodeManager" class="fieldset-body">
						<span id="labell"><a href="javascript:CommonUtil.activeTab();">事件</a> >> 修改</span>
					<table id="accout_form" width="100%" class="mini-table">
						<tr>
							<td>
								<input id="eventId" name="eventId" requiredErrorText="该输入项为必输项" class="mini-textbox" required="true"  vtype="int;rangeLength:1,50"
								 labelField="true" label="事件代码：" emptyText="请输入事件代码" style="width:300px"/>
							</td>
							<td>
								<input id="eventName" name="eventName" requiredErrorText="该输入项为必输项" class="mini-textbox" required="true"
								 vtype="rangeLength:1,25" labelField="true" style="width:300px" label="事件名称：" emptyText="请输入事件名称" />
							</td>
                        </tr>
                        <tr>
							<td>
								<input id="eventDesc" name="eventDesc" style="width:300px" vtype="rangeLength:1,100"  class="mini-textarea" labelField="true" label="事件描述："/>
							</td>  
                        </tr>
                        <tr>
                            <td colspan="3" style="text-align:center;">
                                <a id="save_btn" class="mini-button" style="display: none"    onclick="add">保存</a>
                                <a id="cancel_btn" class="mini-button" style="display: none"    onclick="close">取消</a>
                            </td>
                        </tr>
                    </table>
				</div>
			</div>
			<script type="text/javascript">
				mini.parse();
				var url = window.location.search;
				var action = CommonUtil.getParam(url, "action");
				var param = top["AccCodeManage"].getData(action);
				var form = new mini.Form("#accout_form");
				//保存的方法  
				function add() {
					var form = new mini.Form("#accout_form");
					form.validate();
					if (form.isValid() == false) {
						mini.alert("信息填写有误，请重新填写!", "消息提示")
						return;
					}
					var saveUrl = $.inArray(action, ["add"]) > -1 ? "/TbkEventController/createTbkEvent" : "/TbkEventController/updateTbkEvent";
					//提交数据
					var data = form.getData(true); //获取表单多个控件的数据  
					var param = mini.encode(data); //序列化成JSON
					CommonUtil.ajax({
						url: saveUrl,
						data: param,
						callback: function (data) {
							if (data.code == 'error.common.0000') {
								mini.alert("保存成功", '提示信息', function () {
									top["win"].closeMenuTab();
								});
							} else {
								mini.alert("保存失败");
							}
						}
					});

				}
				function close() {
							top["win"].closeMenuTab();
						}
				function initform() {
					if (action == "edit") {
						form.setData(param);
						mini.get("eventId").setEnabled(false);
					} else if (action == "detail") {
						$("#labell").html("<a href='javascript:CommonUtil.activeTab();'>事件</a>>>详情");
						form.setData(param);
						form.setEnabled(false);
						//"取消"可以用，并且保存按钮不可用直接隐藏掉
						mini.get("save_btn").setVisible(false);
						//mini.get("save_btn").setEnabled(false);
						//mini.get("cancel_btn").setEnabled(false);
					} else {
						$("#labell").html("<a href='javascript:CommonUtil.activeTab();'>事件</a>>>新增");
					}
				}

				$(document).ready(function () {
					initform();
				})
			</script>
		</body>
		</html>