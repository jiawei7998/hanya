<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title></title>
  </head>
  
<body style="width:100%;height:100%;background:white">
	<fieldset class="mini-fieldset">
    <legend>查询</legend>
	<div id="search_form" style="width:100%;">
			<input id="eventName" name="eventName" class="mini-textbox"  labelField="true"  label="事件名称：" labelStyle="text-align:right;" emptyText="请输入事件名称"/>
			<span style="float:right;margin-right: 670px">
				<a id="search_btn" class="mini-button" style="display: none"   onclick="query">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear">清空</a>
			</span>
		</div>
	</fieldset>
	<span style="margin:2px;display: block;">
		<a id="add_btn" class="mini-button" style="display: none"   onclick="add">新增</a>
		<a id="edit_btn" class="mini-button" style="display: none"   onclick="update">修改</a>
		<a id="delete_btn" class="mini-button" style="display: none"   onclick="del">删除</a>
	</span>
	<div id="AccCodeManage" class="mini-fit" style="margin-top: 2px;">      
			<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" allowAlternating="true" allowResize="true" onrowdblclick="onRowDblClick" border="true" sortMode="client">
				<div property="columns">
					<div type="indexcolumn" headerAlign="center" width="30">序号</div>
					<div field="eventId" width="" headerAlign="center" allowSort="true" align="center">事件编号</div>   
					<div field="eventName" width=""   align="" headerAlign="center" allowSort="">事件名称</div>
					<div field="eventDesc" width="" align="" headerAlign="center" allowSort="">事件描述</div>
				</div>
			</div>  
		</div>

	<script>
		mini.parse();
		top['AccCodeManage']=window;
		var grid = mini.get("datagrid");
		var form = new mini.Form("#search_form");
		grid.on("beforeload", function (e) {
			e.cancel = true;
			var pageIndex = e.data.pageIndex; 
			var pageSize = e.data.pageSize;
			search(pageSize,pageIndex);
		});

		function search(pageSize,pageIndex)
		{  
			form.validate();
			if(form.isValid() == false){
				mini.alert("表单填写错误,请确认!","提示信息")
				return;
			}

			var data = form.getData();
			data['pageNumber'] = pageIndex+1;
			data['pageSize'] = pageSize;
			var params = mini.encode(data);
			CommonUtil.ajax({
				url:'/TbkEventController/searchTbkEventPage',
				data:params,
				callback : function(data) {
					var grid = mini.get("datagrid");
					grid.setTotalCount(data.obj.total);
					grid.setPageIndex(pageIndex);
					grid.setPageSize(pageSize);
					grid.setData(data.obj.rows);
				}
			});
			
		}


		function checkBoxValuechanged(){
			search(grid.pageSize,0);
		}

		function add(){
			var url = CommonUtil.baseWebPath() + "/mini_cap/minibase/MiniAccCodeEdit.jsp?action=add";
			var tab = { id: "MiniAccCodeAdd", name: "MiniAccCodeAdd", text: "事件新增", url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
			top['win'].showTab(tab);
		}

		function update()
		{
			var row = grid.getSelected();
			if(row){
				var url = CommonUtil.baseWebPath() + "/mini_cap/minibase/MiniAccCodeEdit.jsp?action=edit";
				var tab = { id: "MiniAccCodeEdit", name: "MiniAccCodeEdit", title: "事件修改", url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
				top["win"].openNewTab(tab, row);

			} else {
				mini.alert("请选中一条记录！","消息提示");
			}
		}

		function onRowDblClick(e) {
			var grid = mini.get("datagrid");
			var row = grid.getSelected();
			if(row){
					var url = CommonUtil.baseWebPath() + "/mini_cap/minibase/MiniAccCodeEdit.jsp?action=detail";
					var tab = { id: "MiniAccCodeDetail", name: "MiniAccCodeDetail", title: "会计事件详情", url: url ,showCloseButton:true,parentId:top["win"].tabs.getActiveTab().name };
					top["win"].openNewTab(tab, row);

			} else {
				mini.alert("请选中一条记录！","消息提示");
			}
		}
		
		function query(){
			search(grid.pageSize,0);
		}
		
		function clear(){
			var form = new mini.Form("#search_form");
			form.clear();
		}
		
		//删除
		function del(){
			var row = grid.getSelected();
			if (row) {
				mini.confirm("您确认要删除选中记录?", "系统警告", function (value) {
					if (value == "ok") {
						CommonUtil.ajax({
							url: "/TbkEventController/deleteTbkEvent/",
							data: { eventId: row.eventId },
							callback: function (data) {
								if (data.code == 'error.common.0000') {
									mini.alert("删除成功")
									search(grid.pageSize,grid.pageIndex);
								} else {
									mini.alert("删除失败");
								}
							}
						});
					}
				});
			} else {
				mini.alert("请选中一条记录！","消息提示");
			}
		}
		
		function getData(action){
		if(action != "add"){
			var row = null;
			row = grid.getSelected();
		}
		return row;
	}

		$(document).ready(function()
		{
			search(10,0);
		});
		
	</script>
</body>
</html>
