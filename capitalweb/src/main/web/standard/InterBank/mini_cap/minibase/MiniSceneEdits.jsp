﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
	<%@ include file="../../../global.jsp"%>
		<html>
		<head>
			<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
			<title>场景管理新增页面</title>
</head>
			<body>
				<div id="form1">
					<!-- <span id="labell"><a href="javascript:CommonUtil.activeTab();">场景管理</a> >> 修改</span> -->
					<span id="labell"><a href="javascript:CommonUtil.activeTab();">场景管理</a> >> 新增</span>
					<div id="sceneInfo_1_form" class="fieldset-body">
					</div>
				</div>

				<script type="text/javascript">
					mini.parse();
					var url = window.location.search;
					var action = CommonUtil.getParam(url, "action");
					var params = top["SceneManage"].getData(action);
					var form = new mini.Form("sceneInfo_1_form");
					function save() {
						form.validate();
						if (form.isValid() == false) {
							mini.alert("信息填写有误，请重新填写!", "消息提示")
							return;
						}

						//提交数据
						var saveUrl = $.inArray(action, ["add"]) > -1 ? "/TbkEntrySceneController/createEntryScene" : "/TbkEntrySceneController/editEntryScene";;
						var data = form.getData(true); //获取表单多个控件的数据  
						
						var obj = {};
						var a=0;
						var length=0;
						for (var i in data) {
							length+=1;
							if(CommonUtil.isNull(mini.get(i).getValue())){
								a+=1;
							}
							if (CommonUtil.isNotNull(mini.get(i))) {
								obj[i] = mini.get(i).getValue();
								if (mini.get(i).getText) {
									obj[i + '_text'] = mini.get(i).getText();
								}
							}
						}
						if(action=='edit'){
							if(mini.get("sceneDescr").getValue()==""){
								length=length-1;
							}else{
								length=length-2;
							}
						}
						if(a==length){
							mini.alert("场景不能为空，请添加至少一条场景！","系统提示");
							return ;
						}
						//ajax请求
						CommonUtil.ajax({
							url: saveUrl,
							data: obj,
							callback: function (data) {
								if (data.code == 'error.common.0000') {
									mini.alert("保存成功", '提示信息', function () {
										setTimeout(function(){top["win"].closeMenuTab()},10);
									});
								} else {
									mini.alert("保存失败");
								}
							}
						});
					}



					function creatTable() {
						CommonUtil.ajax({
							url: "/ParamsController/getParamsActiveList",
							callback: function (data) {
								var rows = data.obj;
								var tr = "";
								var table = $("#sceneInfo_1_form");
								tr = "<table id='accout_form' width='100%' class='mini-table'>";
								for (var i = 0; i < rows.length; i++) {
									var n = rows[i];
									if (i % 3 == 0) {
										tr += "<tr>";
									}
									tr += "<td><input id='" + n.paramSource + "' name='" + n.paramSource + "' class='mini-combobox' labelField='true' style='width:300px' label='" + n.paramName + "：'   textField='pValueTxt' valueField='pValue' emptyText='请选择...' multiSelect='true' /></td>";
									if (i == "2" || i == "5") {
										tr = tr + "</tr>";
									}

									if (i == rows.length - 1) {
										tr = tr + "</tr>";
									}

								}
								tr += "<tr> ";
								tr += " <td> ";
								tr += " <input id='sceneDescr' name='sceneDescr' class='mini-textarea' labelField='true'  style='width:300px; height:100px' label='场景描述：' required='' Enabled='false'>  ";
								tr += " <input id='sceneId' name='sceneId' class='mini-hidden'/> "
								tr += " </td> ";
								tr += " </tr>";
								tr += "<tr><td colspan = '3' style = 'text-align:center;'><a id='save_btn' class='mini-button blue'   onclick='save'>保存</a> <a id='cancel_btn' class='mini-button blue'   onclick='close'>取消</a></td ></tr ></table>"
								table.append(tr);
								mini.parse();

								for (var i = 0; i < rows.length; i++) {
									var n = rows[i];
									CommonUtil.ajax({
										url: "/ParamsMapController/searchPvalueTxtList",
										data: { paramId: n.paramId },
										callback: function (data) {
											var combobox = mini.get(n.paramSource);
											combobox.setData(data.obj);
										}

									});
								}

								initform();
							}
						});
					}
					function close() {
							top["win"].closeMenuTab();
						}
					function initform() {
						if (action == "edit") {
							$("#labell").html("<a href='javascript:CommonUtil.activeTab();'>场景管理</a>>>修改");
							var form = new mini.Form("sceneInfo_1_form");
							form.setData(params);
							////console.log(params.sceneDescr);
						} else if (action == "detail") {
							$("#labell").html("<a href='javascript:CommonUtil.activeTab();'>场景管理</a>>>详情");	
							var form = new mini.Form("sceneInfo_1_form");
							form.setData(params);
							form.setEnabled(false);
							//"取消"可以用，并且保存按钮不可用直接隐藏掉
							mini.get("save_btn").setVisible(false);
							//mini.get("save_btn").setEnabled(false); 
							//mini.get("cancel_btn").setEnabled(false);
						} else if(action == "add") {
						}
					}

					$(document).ready(function () {
						creatTable();

					});

				</script>
			</body>
		</html>