<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
	<%@ include file="../../../global.jsp"%>
		<html>
		<head>
			<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
			<title>参数维度值管理页面</title>
		</head>
		<body style="width:100%;height:100%;background:white">
			<fieldset class="mini-fieldset">
				<legend>查询</legend>
				<div id="search_form" style="width:100%;">
					<input id="paramId" name="paramId" class="mini-textbox" labelField="true" label="维度编号：" labelStyle="text-align:right;" emptyText="请输入维度编号"
					/>
					<input id="paramName" name="paramName" class="mini-textbox" labelField="true" label="维度名称：" labelStyle="text-align:right;"
					 emptyText="请输入维度名称" />
					<input id="pValue" name="pValue" class="mini-textbox" labelField="true" label="参数值：" labelStyle="text-align:right;" emptyText="请输入参数值"
					/>
					<input id="pValueTxt" name="pValueTxt" class="mini-textbox" labelField="true" label="参数值名称：" labelStyle="text-align:right;"
					 emptyText="请输入参数值名称" />
					<span style="float:right;margin-right: 160px">
						<a id="search_btn" class="mini-button" style="display: none"   onclick="query">查询</a>
						<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear">清空</a>
					</span>
				</div>
			</fieldset>
			<span style="margin:2px;display: block;">
				<a id="add_btn" class="mini-button" style="display: none"   onclick="add">新增</a>
				<a id="edit_btn" class="mini-button" style="display: none"   onclick="update">修改</a>
				<a id="delete_btn" class="mini-button" style="display: none"   onclick="del">删除</a>
			</span>
			<div id="ParamsMap" class="mini-fit" style="margin-top: 2px;">
				<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" allowAlternating="true" allowResize="true"
				 onrowdblclick="onRowDblClick" border="true" sortMode="client">
					<div property="columns">
						<div type="indexcolumn" headerAlign="center" width="30">序号</div>
						<div field="pKey" width="50" headerAlign="center" allowSort="true" align="">参数key</div>
						<div field="paramId" width="100" headerAlign="center" allowSort="" align="">维度编号</div>
						<div field="paramName" width="150" align="" headerAlign="center" allowSort="">维度名称</div>
						<div field="pValue" width="50" align="" headerAlign="center" allowSort="true">参数值</div>
						<div field="pValueTxt" width="150" align="" headerAlign="center" allowSort="">参数值名称</div>
					</div>
				</div>
			</div>

			<script>
				//初始化方法 页面加载完成后被调用
				mini.parse();
				top['paramMapManager'] = window;
				var grid = mini.get("datagrid");
				grid.on("beforeload", function (e) {
					e.cancel = true;
					var pageIndex = e.data.pageIndex;
					var pageSize = e.data.pageSize;
					search(pageSize, pageIndex);
				});

				function search(pageSize, pageIndex) {
					var form = new mini.Form("#search_form");
					form.validate();
					if (form.isValid() == false) {
						mini.alert("信息填写有误，请重新填写!", "消息提示")
						return;
					}

					var data = form.getData();
					data['pageNumber'] = pageIndex + 1;
					data['pageSize'] = pageSize;
					var params = mini.encode(data);
					CommonUtil.ajax({
						url: '/ParamsMapController/searchParamsMapPage',
						data: params,
						callback: function (data) {
							var grid = mini.get("datagrid");
							grid.setTotalCount(data.obj.total);
							grid.setPageIndex(pageIndex);
							grid.setPageSize(pageSize);
							grid.setData(data.obj.rows);
						}
					});

				}


				function checkBoxValuechanged() {
					search(grid.pageSize, 0);
				}



				function add() {
					var url = CommonUtil.baseWebPath() + "/mini_cap/minibase/MiniParamsMapEdits.jsp?action=add";
					var tab = { id: "MiniParamsMapAdd", name: "MiniParamsMapAdd", text: "维度值新增", url: url, showCloseButton: true, parentId: top["win"].tabs.getActiveTab().name };
					top['win'].showTab(tab);
				}

				function update() {
					var row = grid.getSelected();
					if (row) {							
						var url = CommonUtil.baseWebPath() + "/mini_cap/minibase/MiniParamsMapEdits.jsp?action=edit"
						var tab = { id: "MiniParamsMapEdit", name: "MiniParamsMapEdit", title: "维度值修改", url: url, showCloseButton: true, parentId: top["win"].tabs.getActiveTab().name };
						top["win"].openNewTab(tab, row);
					} else {
						mini.alert("请选中一条记录！", "消息提示");
					}
				}

				function onRowDblClick(e) {
					var grid = mini.get("datagrid");
					var row = grid.getSelected();
					if (row) {
						var url = CommonUtil.baseWebPath() + "/mini_cap/minibase/MiniParamsMapEdits.jsp?action=detail";
						var tab = { id: "MiniParamsMapDetail", name: "MiniParamsMapDetail", title: "维度值详情", url: url, showCloseButton: true, parentId: top["win"].tabs.getActiveTab().name };
						top["win"].openNewTab(tab, row);

					} else {
						mini.alert("请选中一条记录！", "消息提示");
					}
				}

				function query() {
					search(grid.pageSize, 0);
				}

				function getData(action) {
					if (action != "add") {
						var row = null;
						row = grid.getSelected();
					}
					return row;
				}
				function clear() {
					var form = new mini.Form("#search_form");
					form.clear();
				}
				//删除

				function del() {
					var grid = mini.get("datagrid");
					var row = grid.getSelected();
					if (row) {
						mini.confirm("您确认要删除选中记录?", "系统警告", function (value) {
							if (value == "ok") {
								CommonUtil.ajax({
									url: "/ParamsMapController/deleteParamsMap/",
									data: { pKey: row.pKey },
									callback: function (data) {
										if (data.code == 'error.common.0000') {
											mini.alert("删除成功")
											search(grid.pageSize, grid.pageIndex);
										} else {
											mini.alert("删除失败");
										}
									}
								});
							}
						});
					}
					else {
						mini.alert("请选中一条记录！", "消息提示");
					}
				}

				$(document).ready(function () {
					search(10, 0);
				});

			</script>
		</body>
		</html>