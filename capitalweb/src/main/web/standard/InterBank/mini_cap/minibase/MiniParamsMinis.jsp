<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
	<%@ include file="../../../global.jsp"%>
		<html>
		<head>
			<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
			<title>参数选择页面</title>
		</head>

		<body style="width:100%;height:100%;background:white">
			<fieldset class="mini-fieldset title">
				<legend>查询</legend>

				<div height="60px" showCollapseButton="false" showHeader="true">
					<table id="search_form">
						<tr>
							<td>
								<input id="paramId" name="paramId" class="mini-textbox" labelField="true" label="维度编号" style="width:300px;" emptyText="请输入维度编号"
								/>
							</td>

							<td>
								<input id="paramName" name="paramName" class="mini-textbox" labelField="true" label="维度名称" style="width:300px;" emptyText="请输入维度名称"
								/>
						</tr>
					</table>
				</div>
				<div id="toolbar1" class="mini-toolbar" style="margin-left: 20px;">
					<table style="width:100%;">
						<tr>
							<td style="width:100%;">
								<a id="search_btn" class="mini-button" style="display: none"   onclick="query">查询</a>
								<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear">清空</a>
							</td>
						</tr>
					</table>
				</div>
			</fieldset>
			<div class="mini-fit" style="margin-top: 2px;">
				<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" allowResize="true" height="100%" onrowdblclick="onRowDblClick">
					<div property="columns">
						<div type="indexcolumn" width="30"></div>
						<div field="paramId" width="">维度编号</div>
						<div field="paramName" width="">维度名称</div>
						<div field="paramQz" width="">维度权重</div>
						<div field="paramSource" width="">维度字段</div>
						<div field="lastUpdate" width="">最后更新时间</div>
					</div>
				</div>
			</div>


			<script type="text/javascript">
				mini.parse();
				var grid = mini.get("datagrid");
				grid.on("beforeload", function (e) {
					e.cancel = true;
					var pageIndex = e.data.pageIndex;
					var pageSize = e.data.pageSize;
					search(pageSize, pageIndex);
				});
				
				function search(pageSize, pageIndex) {
					var form = new mini.Form("search_form");
					form.validate();
					//提交数据
					var data = form.getData();//获取表单多个控件的数据  
					data['pageNumber'] = pageIndex+1;
					data['pageSize'] = pageSize;
					var params = mini.encode(data); //序列化成JSON
					CommonUtil.ajax({
						url: '/ParamsController/searchParamsPage',
						data: params,
						callback: function (data) {
							var grid = mini.get("datagrid");
							grid.setTotalCount(data.obj.total);
							grid.setPageIndex(pageIndex);
							grid.setPageSize(pageSize);
							grid.setData(data.obj.rows);
						}
					});
				}

				function GetData() {
					var grid = mini.get("datagrid");
					var row = grid.getSelected();
					return row;
				}

				function query(){
					search(grid.pageSize,0);
				}
				function onRowDblClick(e) {
					onOk();
				}
				function CloseWindow(action) {
					if (window.CloseOwnerWindow)
						return window.CloseOwnerWindow(action);
					else
						window.close();
				}

				function clear() {
					var form = new mini.Form("#search_form");
					form.clear();
				}

				function onOk() {
					CloseWindow("ok");
				}
				function onCancel() {
					CloseWindow("cancel");
				}
				$(document).ready(function () {
					search(10,0);
				});	
			</script>
		</body>
		</html>