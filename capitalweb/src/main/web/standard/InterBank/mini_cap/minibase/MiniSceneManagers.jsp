﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
	<%@ include file="../../../global.jsp"%>
		<html>
		<head>
			<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
			<title>场景管理页面</title>
		</head>

		<body style="width:100%;height:100%;background:white">
			<fieldset class="mini-fieldset">
				<legend>查询</legend>
				<div id="search_form" style="width:100%;">
					<input id="sceneDescr" name="sceneDescr" class="mini-textbox" labelField="true" label="场景描述：" labelStyle="text-align:right;"
					/>
					<span style="float:right;margin-right: 670px">
						<a id="search_btn" class="mini-button" style="display: none"   onclick="query">查询</a>
						<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear">清空</a>
					</span>
				</div>
			</fieldset>
			<span style="margin:2px;display: block;">
				<a id="add_btn" class="mini-button" style="display: none"   onclick="add">新增</a>
				<a id="edit_btn" class="mini-button" style="display: none"   onclick="update">修改</a>
				<a id="delete_btn" class="mini-button" style="display: none"   onclick="del">删除</a>
			</span>
			<!-- 此处加了2个属性，idFiled  和pagesize -->
			<div id="SceneManage" class="mini-fit" style="margin-top: 2px;">
				<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" allowAlternating="true" allowResize="true"
			idFiled="sceneId"  pageSize="10" 
			onrowdblclick="onRowDblClick"  sortMode="client">
					<div property="columns">
						<div type="indexcolumn" headerAlign="center" width="30">序号</div>
						<div field="sceneId" width="" headerAlign="center" allowSort="true" align="center">场景ID</div>
						<div field="sceneDescr" width="300" headerAlign="center" allowSort="true">场景描述</div>
						<div field="containEntry" width="" align="center" renderer="onGenderRenderers" headerAlign="center" allowSort="true">是否包含分录</div>
					</div>
				</div>
			</div>

			<script>
				mini.parse();
				top['SceneManage'] = window;
				var grid = mini.get("datagrid");
				grid.on("beforeload", function (e) {
					e.cancel = true;
					var pageIndex = e.data.pageIndex;
					var pageSize = e.data.pageSize;
					search(pageSize, pageIndex);
				});

				function search(pageSize, pageIndex) {
					var form = new mini.Form("#search_form");
					form.validate();
					if (form.isValid() == false) {
						mini.alert("表单填写错误,请确认!", "提示信息")
						return;
					}

					var data = form.getData();
					data['pageNumber'] = pageIndex + 1;
					data['pageSize'] = pageSize;
					var params = mini.encode(data);
					CommonUtil.ajax({
						url: '/TbkEntrySceneController/searchPageTbkEntrySceneInfo',
						data: params,
						callback: function (data) {
							var grid = mini.get("datagrid");
							grid.setTotalCount(data.obj.total);
							grid.setPageIndex(pageIndex);
							grid.setPageSize(pageSize);
							grid.setData(data.obj.rows);
						}
					});

				}


				function checkBoxValuechanged() {
					search(grid.pageSize, 0);
				}

				function onGenderRenderers(e) {
					var Genders = [{
						id: 0,
						text: '否'
					}, {
						id: 1,
						text: '是'
					}
					];
					for (var i = 0, l = Genders.length; i < 2; i++) {
						var g = Genders[i];
						if (g.id == e.value)
							return g.text;
					}
					return " ";
				}
				//新增的方法
				function add() {
					var url = CommonUtil.baseWebPath() + "/mini_cap/minibase/MiniSceneEdits.jsp?action=add";
					var tab = { id: "SceneAdd", name: "SceneAdd", text: "场景新增", url: url, showCloseButton: true,parentId: top["win"].tabs.getActiveTab().name };
					top['win'].showTab(tab);
				}
				//更新的方法
				function update() {
					var row = grid.getSelected();
					if (row) {
						var url = CommonUtil.baseWebPath() + "/mini_cap/minibase/MiniSceneEdits.jsp?action=edit";
						var tab = { id: "SceneEdit", name: "SceneEdit", title: "场景修改", url: url, showCloseButton: true,parentId: top["win"].tabs.getActiveTab().name };
						top["win"].openNewTab(tab, row);

					} else {
						mini.alert("请选中一条记录！", "消息提示");
					}
				}
				//双击详情的方法
				function onRowDblClick(e) {
					var row = grid.getSelected();
					if (row) {
						var url = CommonUtil.baseWebPath() + "/mini_cap/minibase/MiniSceneEdits.jsp?action=detail";
						var tab = { id: "SceneDetail", name: "SceneDetail", title: "场景详情", url: url, showCloseButton: true,parentId: top["win"].tabs.getActiveTab().name };
						top["win"].openNewTab(tab, row);

					} else {
						mini.alert("请选中一条记录！", "消息提示");
					}
				}

				function query() {
					search(grid.pageSize, 0);
				}

				function clear() {
					var form = new mini.Form("#search_form");
					form.clear();
				}
				//删除

				function del() {
					var row = grid.getSelected();
					if (row) {
						mini.confirm("您确认要删除选中记录?", "系统警告", function (value) {
							if (value == 'ok') {
								CommonUtil.ajax({
									url: "/TbkEntrySceneController/deleteEntryScene/",
									data: { sceneId: row.sceneId },
									callback: function (data) {
										if (data.code == 'error.common.0000') {
											mini.alert("删除成功")
											search(grid.pageSize,grid.pageIndex);
										} else {
											mini.alert("删除失败");
										}
									}
								});
							}
						});
					} else {
						mini.alert("请选中一条记录！", "消息提示");
					}

				}

				function getData(action) {
					var row = null;
					if (action != "add") {
						row = grid.getSelected();
					}
					return row;
				}


				$(document).ready(function () {
					search(10, 0);
				});

			</script>
		</body>
		</html>