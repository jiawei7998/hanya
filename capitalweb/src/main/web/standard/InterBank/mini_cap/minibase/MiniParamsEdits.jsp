<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
	<%@ include file="../../../global.jsp"%>
		<html>
		<head>
			<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
			<title>维度权重信息</title>
		</head>

		<body>
			<div id="form1">
					<span id="labell"><a href="javascript:CommonUtil.activeTab();">维度权重信息</a> >> 新增</span>
				<div id="Param" class="fieldset-body">
					<table id="accout_form" width="100%" class="mini-table">
						<tr>
							<td>
								<input id="paramId" name="paramId" class="mini-textbox" Enabled="false" vtype="rangeLength:1,40" labelField="true" label="维度编号:"
								  emptyText="自动生成" />
							</td>
							<td>
								<input id="paramName" name="paramName" requiredErrorText="该输入项为必输项" class="mini-buttonedit" onbuttonclick="onInsQuery" vtype="rangeLength:1,40"
								 labelField="true" label="维度名称:" allowinput="false"  emptyText="请选择维度名称..." required="true" />
							</td>
							<td>
								<input id="paramSource" name="paramSource" class="mini-textbox" labelField="true" label="维度字段:" Enabled="false" 
								 emptyText="自动生成" />
							</td>
						</tr>
						<tr>
							<td>
								<input id="active" name="active" class="mini-combobox" data="CommonUtil.serverData.dictionary.YesNo" value="1" labelField="true" label="是否启用:"
								  emptyText="请选择是否启用..." />
							</td>
							<td>
								<input id="lastUpdate" name="lastUpdate" class="mini-textbox" labelField="true" Enabled="false" label="最后更新时间:" 
								 emptyText="自动生成" />
							</td>
							<td>
								<input id="paramQz" name="paramQz" class="mini-textbox" labelField="true" vtype="int;maxLength:9" label="维度权重:"  emptyText="请输入维度权重"
								/>
							</td>

						</tr>
						<tr>
                            <td colspan="3" style="text-align:center;">
                                <a id="save_btn" class="mini-button" style="display: none"    onclick="add">保存</a>
                                <a id="cancel_btn" class="mini-button" style="display: none"    onclick="close">取消</a>
                            </td>
                        </tr>
					</table>
					
					
					<script type="text/javascript">
						mini.parse();
						var url = window.location.search;
						var action = CommonUtil.getParam(url, "action");
						var param = top["Params"].getData(action);
						function add() {
							var form = new mini.Form("accout_form");
							form.validate();
							if (form.isValid() == false) {
								mini.alert("信息填写有误，请重新填写!", "消息提示")
								return;
							}												
							var data = form.getData(true); //获取表单多个控件的数据  
							// data['pageNumber'] = pageIndex+1;
							// data['pageSize'] = pageSize;
							var param = mini.encode(data); //序列化成JSON
							CommonUtil.ajax({
								url: "/ParamsController/createParams",
								data: param,
								callback: function (data) {
									if (data.code == 'error.common.0000') {
										mini.alert("保存成功", '提示信息', function () {
											top["win"].closeMenuTab();
										});
									} else {
										mini.alert("保存失败");
									}
								}
							});

						}
						function close() {
							top["win"].closeMenuTab();
						}
						function initform() {
							if (action == "edit") {
								var form = new mini.Form("#accout_form");
								mini.get("paramName").setText(param.paramName);
								mini.get("paramName").setValue(param.paramName);
								form.setData(param);
							} else if (action == "detail") {
								$("#labell").html("<a href='javascript:CommonUtil.activeTab();'>维度权重信息</a>>>详情");
								var form = new mini.Form("#accout_form");
								mini.get("paramName").setText(param.paramName);
								mini.get("paramName").setValue(param.paramName);
								form.setData(param);
								form.setEnabled(false);
								//"取消"可以用，并且保存按钮不可用直接隐藏掉
								mini.get("save_btn").setVisible(false);
								//mini.get("save_btn").setEnabled(false);
								//mini.get("cancel_btn").setEnabled(false);
							} else {
								var form = new mini.Form("#accout_form");
								form.setData(param, false);
							}


						}
						//查询维度名称的方法
						function onInsQuery(e) {
							var btnEdit = this;
							mini.open({
								url: CommonUtil.baseWebPath() + "/mini_cap/minibase/MiniParamDicMinis.jsp",
								title: "参数选择",
								width: 700,
								height: 600,
								ondestroy: function (action) {
									if (action == "ok") {
										var iframe = this.getIFrameEl();
										var data = iframe.contentWindow.GetData();
										data = mini.clone(data); //必须
										if (data) {
											mini.get("paramId").setText(data.paramId);
											mini.get("paramId").setValue(data.paramId);
											mini.get("paramSource").setText(data.paramSource);
											mini.get("paramSource").setValue(data.paramSource);
											btnEdit.setValue(data.paramName);
											btnEdit.setText(data.paramName);
											btnEdit.validate();
											btnEdit.focus();
										}
									}
								}
							});
						}

						$(document).ready(function () {
							initform();
						})
					</script>
		</body>
		</html>