<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
	<%@ include file="../../../global.jsp"%>
		<html>

		<head>
			<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
			<title>参数维度值编辑页面</title>
		</head>

		<body>
			<div id="form1">
						<span id="labell"><a href="javascript:CommonUtil.activeTab();">参数维度值管理</a> >> 新增</span>
				<div id="MiniParamMap" class="fieldset-body">
					<table id="accout_form" width="100%" class="mini-table">
						<tr>
							<td>
								<input id="pKey" name="pKey" class="mini-hidden" labelField="true" label="参数key:"/>
								<input id="paramId" name="paramId" requiredErrorText="该输入项为必输项" class="mini-textbox" labelField="true" Enabled="false" label="维度编号:"
								 emptyText="自动生成" />
							</td>
							<td>
								<input id="paramName" name="paramName" labelField="true" label="维度名称:" allowinput="false" required="true"  emptyText="请选择..." class="mini-buttonedit"
								 onbuttonclick="onInsQuery" emptyText="请选择维度名称..." />
							</td>
							<td>
								<input id="pValue" name="pValue" class="mini-textbox mini-validatebox" labelField="true" label="参数值:" required="true"
								 requiredErrorText="该输入项为必输项" vtype="int;rangeLength:1,40" emptyText="请输入参数值" />
							</td>
						</tr>
						<tr>
							<td>
								<input id="pValueTxt" name="pValueTxt" class="mini-textbox mini-validatebox" labelField="true" required="true"  label="参数值名称:"
								  requiredErrorText="该输入项为必输项" vtype="rangeLength:1,20" emptyText="请输入参数值名称" />
							</td>
						</tr>
						<tr>
                            <td colspan="3" style="text-align:center;">
                                <a id="save_btn" class="mini-button" style="display: none"    onclick="add">保存</a>
                                <a id="cancel_btn" class="mini-button" style="display: none"    onclick="close">取消</a>
                            </td>
                        </tr>
					</table>
				</div>
			</div>
			<script type="text/javascript">
				mini.parse();
				var url = window.location.search;
				var action = CommonUtil.getParam(url, "action");
				var param = top["paramMapManager"].getData(action);
				var form = new mini.Form("#accout_form");
				function initform() {
					if (action == "edit") {
						$("#labell").html("<a href='javascript:CommonUtil.activeTab();'>参数维度值管理</a>>>修改");
						mini.get("paramName").setText(param.paramName);
						mini.get("paramName").setValue(param.paramName);
						form.setData(param);
						mini.get("paramName").setEnabled(false);
						mini.get("pValue").setEnabled(false);
					} else if (action == "detail") {
						$("#labell").html("<a href='javascript:CommonUtil.activeTab();'>参数维度值管理</a>>>详情");
						mini.get("paramName").setText(param.paramName);
						mini.get("paramName").setValue(param.paramName);
						form.setData(param);
						form.setEnabled(false);
						//"取消"可以用，并且保存按钮不可用直接隐藏掉
						mini.get("save_btn").setVisible(false);
						//mini.get("save_btn").setEnabled(false);
						//mini.get("cancel_btn").setEnabled(false);
					} else {

					}

				}

				function close() {
							top["win"].closeMenuTab();
						}
				//保存的方法  
				function add() {
					var form = new mini.Form("#accout_form");
					form.validate();
					if (form.isValid() == false) {
						mini.alert("交易验证失败,请检查表单是否填写正确!", "消息提示")
						return;
					}
					//提交数据
					var saveUrl = $.inArray(action, ["add"]) > -1 ? "/ParamsMapController/createParamsMap" : "/ParamsMapController/updateParamsMap";
					var data = form.getData(true); //获取表单多个控件的数据  
					var param = mini.encode(data); //序列化成JSON
					CommonUtil.ajax({
						url: saveUrl,
						data: param,
						callback: function (data) {
							if (data.code == 'error.common.0000') {
								mini.alert("保存成功", '提示信息', function () {
									top["win"].closeMenuTab();
								});
							} else {
								mini.alert("保存失败");
							}
						}
					});

				}

				//查询维度名称的方法
				function onInsQuery(e) {
					var btnEdit = this;
					mini.open({
						url: CommonUtil.baseWebPath() + "/mini_cap/minibase/MiniParamsMinis.jsp",
						title: "参数选择",
						width: 700,
						height: 600,
						ondestroy: function (action) {
							if (action == "ok") {
								var iframe = this.getIFrameEl();
								var data = iframe.contentWindow.GetData();
								data = mini.clone(data); 
								if (data) {
									mini.get("paramId").setText(data.paramId);
									mini.get("paramId").setValue(data.paramId);
									btnEdit.setValue(data.paramName);
									btnEdit.setText(data.paramName);
									btnEdit.focus();
								}
							}
						}
					});
				}

				$(document).ready(function () {
					initform();
				})

			</script>
		</body>
		</html>