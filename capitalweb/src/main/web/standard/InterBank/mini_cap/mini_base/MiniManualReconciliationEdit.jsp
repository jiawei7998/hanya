<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../../global.jsp"%>
<script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
        <legend>手工调账</legend>
        <table id="manual_form" fit="true" width="100%" cols="4">
            <tr>
                <td>
                        <input id="sponInst" name="sponInst" class="mini-buttonedit" onbuttonclick="onInsQuery" labelField="true" required="true"  label="所属机构" style="width:350px;" allowInput='false' emptyText="请选择所属机构" />
                        <input id="flowId" name ="flowId" class="mini-textbox"  value=""></td>
                </td>
                <td>
                        <input id="ccy" name="ccy" value="CNY" class="mini-combobox" width="300px" required="true"  labelField="true" label="币种:" data="CommonUtil.serverData.dictionary.Currency" />
                </td>
            </tr>
            <tr>
                <td>
                        <input id="dealNo" name="dealNo" class="mini-textbox" vtype="maxLength:32" width="350px" labelField="true" label="交易流水:" required="true"   emptyText='请输入交易流水'/>
                </td>
                <td>
                        <input id="outDate" name="outDate" class="mini-combobox" width="300px" labelField="true" required="true"  label="是否逾期:" emptyText='请选择是否逾期' data="CommonUtil.serverData.dictionary.YesNo"/>
                </td>
            </tr>
        </table>
    </fieldset>
    <fieldset class="mini-fieldset">
        <legend>新增借贷</legend>
        <table id="borrowing_form" fit="true" width="100%" cols="4">
            <tr>
                <td>
                    <input class="mini-combobox" id="debitCreditFlag" name="debitCreditFlag" required="true"  labelField="true" label="借贷方向:" emptyText='请选择借贷方向'  data="CommonUtil.serverData.dictionary.DebitCreditFlag" />
                </td>
                <td>
                    <input class="mini-buttonedit" id="subjCode" name="subjCode" allowInput="false"  required="true"  onbuttonclick="searchSubCode" labelField="true" label="科目代码:" emptyText='请选择科目代码' />
                </td>
                <td>
                    <input class="mini-spinner" id="value" name="value" minValue="-100000000000" maxValue="100000000000" changeOnMousewheel="false" labelField="true"  label="金额:" emptyText='请输入金额'  format="n2"/>
                </td>
            </tr>
        </table>
    </fieldset>

<fieldset class="mini-fieldset">
    <div style="margin-bottom:10px;">
        <a id="add_btn" class="mini-button" style="display: none"    onclick="add()">新增</a>
        <a id="delete_btn" class="mini-button" style="display: none"    onclick="del()">删除</a>
        <a id="save_btn" class="mini-button" style="display: none"    onclick="save()">保存</a>
    </div>
    <div id="manualGrid" class="mini-datagrid  borderAll" style="width:100%;height:200px;" allowResize="true" allowRowSelect="true"
        showPager="false" pageSize="100" multiSelect="true"   allowCellEdit="true" allowCellSelect="true" allowSortColumn="false" editNextOnEnterKey="true">
        <div property="columns">
            <div type="indexcolumn" headerAlign="center"  width="40">序号</div>
            <div field="debitCreditFlag" headerAlign="center"  width="100" renderer="CommonUtil.dictRenderer" data-options="{dict:'DebitCreditFlag'}" align="center">借贷方向 </div>
            <div field="subjCode" headerAlign="center" width="100" align="center">科目代码</div>
            <div field="subjName" headerAlign="center" allowSort="true" width="100" align="center">科目名称</div>
            <div field="value" headerAlign="center"  width="100" align="center">金额</div>
        </div>
    </div>
   
</fieldset>
<div class="mini-fit">
<%@ include file="../../mini_base/mini_flow/MiniApproveOpCommon.jsp"%>
</div>
</body>
<script>
mini.parse();
var grid=mini.get("manualGrid");
mini.get("flowId").setVisible(false);
var inst='';
var tradeData = {};
if (GetQueryString("action") == 'approve' && GetQueryString("flowId") != null) {
    tradeData.flow_type =GetQueryString("flow_type");
    tradeData.task_id = GetQueryString("task_id");
    tradeData.serial_no = GetQueryString("flowId");
    tradeData.operType = GetQueryString("action");
}
var sub=mini.get("subjCode");
$(document).ready(function(){
    //修改时初始化数据
    if(GetQueryString("action")=='edit'){
        init();
    }

    if(GetQueryString("action")=='approve' && GetQueryString("flowId") !=null){
        //设置按钮不可点的属性
        notUse();
        init();
    }
})
function notUse(){
    mini.get("sponInst").setEnabled(false);
    mini.get("ccy").setEnabled(false);
    mini.get("dealNo").setEnabled(false);
    mini.get("outDate").setEnabled(false);
    mini.get("debitCreditFlag").setEnabled(false);
    mini.get("subjCode").setEnabled(false);
    mini.get("value").setEnabled(false);
    mini.get("add_btn").setEnabled(false);
    mini.get("delete_btn").setEnabled(false);
    mini.get("save_btn").setEnabled(false);
}
function GetQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if (r != null)
        return unescape(r[2]);
    return null;
}
//页面数据初始化
function init(){
    
    //根据flowId 获取相应的信息
    CommonUtil.ajax({
        url: "/TbkManualEntryController/selectManualEntryVo",
        data: mini.encode({"flowId":GetQueryString("flowId")}),
        callback: function (data) {
            
           if(data){
               var manual_form=new mini.Form("manual_form");
              
               if(data.sponInst){
                   //获取机构信息
                   CommonUtil.ajax({
                       url: "/InstitutionController/getInstById",
                       data: mini.encode({"instId":data.sponInst}),
                       callback: function (data) {
                        var sponInst=mini.get("sponInst");
                        sponInst.setValue(data.obj.instId);
                        sponInst.setText(data.obj.instName);
                       }
                   });
               }
               
               
               manual_form.setData(data);
               //对 manualGrid  赋值
               if(data.constact_list!=null && data.constact_list.length>0){
                grid.setData(data.constact_list);

               }
           }
        }
    });
}

function searchSubCode(e) {
    var btnEdit = this;
    mini.open({
        url: CommonUtil.baseWebPath() + "/mini_cap/mini_base/MiniSubjAndConfig.jsp",
        title: "科目选择",
        width: 700,
        height: 400,
        ondestroy: function (action) {
            var iframe = this.getIFrameEl();
            var data = iframe.contentWindow.GetData();
            data = mini.clone(data);
            if (data) {
                btnEdit.setValue(data.subjCode);
                btnEdit.setText(data.subjName);
                btnEdit.setIsValid(true);
            }
        }
    });
   
}    
//机构的查询  
function onInsQuery(e) {
    var btnEdit = this;
    mini.open({
        url: CommonUtil.baseWebPath() + "/mini_system/MiniInstitutionSelectManages.jsp",
        title: "机构选择",
        width: 700,
        height: 600,
        ondestroy: function (action) {
            if (action == "ok") {
                var iframe = this.getIFrameEl();
                var data = iframe.contentWindow.GetData();
                data = mini.clone(data); //必须
                if (data) {
                    btnEdit.setValue(data.instId);
                    btnEdit.setText(data.instName);
                    btnEdit.setIsValid(true);
                    btnEdit.focus();
                }
            }
        }
    });
} 
//保存
function save(){
    var records=grid.getData();
  if(records.length>0){
    //保存操作
    var form =new mini.Form("manual_form");
    form.validate();
    if (form.isValid() == false){
        mini.alert("请检查填写内容","系统提示");
        return;//表单验证
    } 
    var dataObj = form.getData();//获取表单数据
    var Ctotel =0;
    var Dtotel=0;
    for(var i=0;i<records.length;i++){
        delete records[i]["_id"];
        delete records[i]["_state"];
        delete records[i]["_uid"];
       if(records[i].debitCreditFlag=="C"){
        Ctotel=Ctotel+records[i].value;
       }
       if((records[i].debitCreditFlag=="D")){
        Dtotel=Dtotel+records[i].value;
       }
    }
    if(Ctotel==Dtotel){
        dataObj.constact_list=records;
        CommonUtil.ajax({
        url: "/TbkManualEntryController/addEntryList",
        data: mini.encode(dataObj),
        callback: function (data) {
            if (data.code == 'error.common.0000') {
                mini.alert("操作成功","系统提示",function(){setTimeout(function(){
                    top["win"].closeMenuTab()
                },10);
                    
                });
            } else {
                mini.alert("操作失败","系统提示");
            }
        }
    });
    }else{
        mini.alert("借贷不平衡","系统提示");
    }
  }else{
      mini.alert("请增加列表内容再保存","系统提示");
  }
}
//新增一行
function add() {
    var form =new mini.Form("borrowing_form");
    form.validate();
    if (form.isValid() == false){
        mini.alert("请检查输入信息");
        return;
    };//表单验证
    var record =form.getData();
    if(record.value==0){
        mini.alert("金额不能为0","系统提示");
        return;
    }
    if(record.value>0){
        var row = {"debitCreditFlag":record.debitCreditFlag,"subjCode":record.subjCode,"subjName":sub.getText(),"value":record.value};
        grid.addRow(row);
    }else{
        mini.alert("金额不能为0","系统提示");
    }
    
} 
//删除一行
function del() {
    var row = grid.getSelected();
    if (row) {
        grid.removeRow(row, true);
    } else {
        mini.alert("请选择要删除的行!", "消息提示");
    }
}
</script>    
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
<script type="text/javascript" src="../../mini_base/mini_flow/MiniApproveOpCommon.js"></script>