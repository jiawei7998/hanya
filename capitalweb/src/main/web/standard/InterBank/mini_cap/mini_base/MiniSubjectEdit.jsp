<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>

<%@ include file="../../../global.jsp"%> 
<html>
<head>
    <title>科目管理维护</title>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script> 
</head>
<body>

    <div>
        <span id="labell"><a href="javascript:CommonUtil.activeTab();">科目管理</a> >> 修改</span>
        <div id="field" class="fieldset-body">
        <table id="subject_form"   width="100%" class="mini-table">
            <tr>
                <td><input id="taskId" name="taskId" class="mini-combobox"  labelField="true"  required="true"
                    label="账套名称" emptyText='请选择账套名称' valueField="taskId" textField="taskName"
                    width="300px"/>
                    <input id="dbId" name="dbId" class="mini-hidden"/></td>
                <td><input id="subjType" name="subjType" class="mini-combobox"  labelField="true" 
                    label="科目类型" emptyText='请选择科目类型' required="true"  width="300px"
                    data = "CommonUtil.serverData.dictionary.subjType"/>
                </td>
                <td><input id="subjCode" name="subjCode" class="mini-textbox" 
                    required="true"   emptyText="请输入科目号" width="300px"
                    labelField="true"  label="科目号" vtype="maxLength:20;float;"/>
                </td>
                
            </tr>
            
            <tr>
                <td><input id="subjName" name="subjName" class="mini-textbox"
                    required="true"  emptyText="请输入科目名称" width="300px"
                    labelField="true"  label="科目名称" vtype="maxLength:50"/>
                </td>
                <td><input id="ifParentAll" name="ifParentAll" class="mini-combobox" 
                    required="true"   data = "CommonUtil.serverData.dictionary.YesNo"
                    emptyText="请选择是否根科目" labelField="true"  label="是否根科目"
                    onvaluechanged="ifParentAllChanged" width="300px"/>
                </td>
                
                
                <td><input id="parentSubjName" name="parentSubjName" class="mini-buttonedit"
                    required="true"  vtype="maxLength:100" width="300px"
                    emptyText="请选择上级科目" labelField="true"  label="上级科目名称" 
                    onbuttonclick="onSubjNameQuery"/>
                    <input id="parentSubjCode" name="parentSubjCode" class="mini-hidden" 
                    labelField="true"  label="上级科目号" enabled="true"/>
                </td>
            </tr>
            <tr>
                <td><input id="debitCredit" name="debitCredit" class="mini-combobox" 
                    required="true"   data = "CommonUtil.serverData.dictionary.DebitCreditFlag"
                    emptyText="请选择余额借贷方向" labelField="true"  label="余额借贷方向"
                    width="300px"/>
                </td>
                <td><input id="subjNature" name="subjNature" class="mini-combobox"
                    required="true"  data = "CommonUtil.serverData.dictionary.subjType"
                    emptyText="请选择科目性质" labelField="true"  label="科目性质" width="300px"/>
                </td>
                <td><input id="payRecive" name="payRecive" class="mini-combobox" 
                    required="true"   data = "CommonUtil.serverData.dictionary.DebitCreditFlag"
                    emptyText="请选择余额收付方向" labelField="true"  label="余额收付方向" width="300px"/>
                </td>
            </tr>
            <tr>
                
                <td><input id="direction" name="direction" class="mini-combobox"
                    required="true"  data = "CommonUtil.serverData.dictionary.DebitCreditFlag"
                    emptyText="请选择收支方向" labelField="true"  label="收支方向" width="300px"/>
                    <input id ="source" name ="source" value = "1" class = "mini-hidden">
                </td>
            </tr>
            <tr>
                <td colspan="3" style="text-align:center;">
                    <a  id="save_btn" class="mini-button" style="display: none"    onclick="save">保存</a>
                    <a id="cancel_btn" class="mini-button" style="display: none"    onclick="cancel">取消</a>
                </td>
            </tr>
        </table>
        </div>
    </div>
    

    <script>
        $(document).ready(function(){
            loadTaskName();
            init();
        });
        mini.parse();



        var url = window.location.search;
        var action = CommonUtil.getParam(url,"action");

        function init(){
            
            if(action == "new"){
                $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>科目管理</a> >> 新增");
                
            }else if(action == "edit" || action == "detail"){
                var form1 = new mini.Form("subject_form");
                var data=top["subjectManage"].getData();
                form1.setData(data);
                //修改时，科目号不可修改,账套不可修改
                mini.get("subjCode").disable();
                mini.get("taskId").disable();

                //是根科目，上级科目控件禁用
                var ifParentAll =mini.get("ifParentAll").getValue();
                if(ifParentAll == "是"){
                    mini.get("parentSubjName").disable();
                }else if(ifParentAll == "否"){
                    mini.get("parentSubjName").setValue(data.parentSubjCode);
                    mini.get("parentSubjName").setText(data.parentSubjName);

                }
                
                if(action == "detail"){
                    $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>科目管理</a> >> 详情");
                    form1.setEnabled(false);
                    document.getElementById("save_btn").style.display="none";
                    //"取消"可以用，并且保存按钮不可用直接隐藏掉
                    //document.getElementById("cancel_btn").style.display="none";
                }
            }
        }

        //加载账套信息
        function loadTaskName(){
            CommonUtil.ajax({
                url:"/TtAccountingTaskController/selectAccountingTaskList",
                data:mini.encode({defaultTask:"1"}),
                
                callback:function(data){
                        
                    var taskId =mini.get("taskId");
                    
                    //设置数据
                    taskId.setData(data.obj);
                    
                }
            });
        }

        //保存
        function save(){
            var form = new mini.Form("subject_form");
            form.validate();
	        if (form.isValid() == false){//表单验证
                mini.alert("请输入有效数据","系统提示");
                return;
            } 
            var data = form.getData();
            if(data.subjName==data.parentSubjName){
                mini.alert("科目名称不能和上级科目名称相同","系统提示");
                return;
            }
            var param = mini.encode(data); //序列化成JSON
            var saveUrl="";
            if(action == "new"){
                saveUrl = "/TbkSubjectDefController/saveTbkSubjectDef";
            }else if(action == "edit"){
                saveUrl = "/TbkSubjectDefController/updateTbkSubjectDef";
            }
            CommonUtil.ajax({
				url:saveUrl,
                data:param,
            	callback:function(data){
					if('error.common.0000' == data.code){
				        mini.alert("保存成功!","系统提示",function(value){
                            //关闭并刷新
                            top['win'].closeMenuTab();
                        });
				
                    }else{
                        mini.alert("保存失败！","系统提示");
                    }	
                }
			});
        }
        //是否根科目 值改变时发生
        function ifParentAllChanged(e){
            //获取上级科目名称
            var parentSubjName=mini.get("parentSubjName");
            //获取上级科目号
            var parentSubjCode=mini.get("parentSubjCode");
            if("0" == e.value){//不是根科目=>上级科目名称 启用
                parentSubjCode.setValue("");
                //上级科目名称 启用
                parentSubjName.enable();
                parentSubjName.setRequired(true);
                parentSubjName.validate();

            }else if("1" == e.value){//是根科目=>上级科目名称 禁用
                //上级科目号 设置为-1
                parentSubjCode.setValue("-1");
                //上级科目名称 禁用
                parentSubjName.disable();
                //上级科目名称设置为空
                parentSubjName.setValue("");
                parentSubjName.setText("");
                parentSubjName.setRequired(false);
                parentSubjName.validate();
            }
        }
        //上级科目名称的查询
        function onSubjNameQuery(e){
            var taskId = mini.get("taskId").getValue();
            var btnEdit = this;
			mini.open({
				url : CommonUtil.baseWebPath() +"/mini_cap/mini_base/MiniSubjectMiniManager.jsp?taskIdStr="+taskId,
				title : "上级科目名称",
				width : 900,
				height : 600,
				ondestroy : function(action) {	
					if (action == "ok") 
					{
						var iframe = this.getIFrameEl();
						var data = iframe.contentWindow.GetData();
						data = mini.clone(data); //必须
						if (data) {
							btnEdit.setValue(data.subjName);
							btnEdit.setText(data.subjName);
                            var parentSubjCode = mini.get("parentSubjCode");
                            parentSubjCode.setValue(data.subjCode);
                            btnEdit.validate();
							btnEdit.focus();
						}

                        
					}

				}
			});
        }

        //取消按钮
        function cancel(){
            window.CloseOwnerWindow();

        }
    </script>
</body>
</html>