<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../../global.jsp"%> 
<html>
<head>
    <title>科目管理</title>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script> 
</head>
<body style="width:100%;height:100%;background:white">
	<fieldset id="fd2" class="mini-fieldset">
		<legend><label>查询条件</label></legend>
		<div id="find" class="fieldset-body">
			<div id="search_form"  >
					<input id="taskId" name="taskId" class="mini-combobox" valueField="taskId" textField="taskName" labelField="true"  label="所属账套："  emptyText="请选择所属账套" labelStyle="text-align:right;"/>
                    <input id="subjCode" name="subjCode" class="mini-textbox" labelField="true"  label="科目号："  emptyText="请输入科目号" labelStyle="text-align:right;" vtype="maxLength:20;float;"/>
                    <input id="subjName" name="subjName" class="mini-textbox" labelField="true"  label="科目名称："  emptyText="请输入科目名称" labelStyle="text-align:right;" vtype="maxLength:100"/>
                    <span style="float:right;margin-right: 100px">
                        <a id="search_btn" class="mini-button" style="display: none"    onclick="search(10,0)">查询</a>
                        <a id="clear_btn" class="mini-button" style="display: none"    onClick="clear">清空</a>
                    </span>
            </div>
            
		</div>
    </fieldset>
    <span style="margin:2px;display: block;">
        <a id="add_btn" class="mini-button" style="display: none"   onclick="add">新增</a>
        <a id="edit_btn" class="mini-button" style="display: none"    onclick="modifyedit()">修改</a>
        <a id="delete_btn" class="mini-button" style="display: none"    onclick="removeRows">删除</a>
    </span>
    <div id="tradeManage" class="mini-fit" style="margin-top: 2px;">
        <div id="subject_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" 
        idField="subjCode"  pageSize="10"  onRowdblclick="custDetail"  allowAlternating="true">
            <div property="columns" >
                <div type="indexcolumn" headerAlign="center" align="center">序号</div>
                <div field="subjCode"  headerAlign="center" align="center" width="100px" allowSort="true">科目号</div>    
                <div field="subjName"   headerAlign="center" width="180px">科目名称</div>                            
                <div field="subjType"   headerAlign="center"   width="100px"  renderer="CommonUtil.dictRenderer" data-options="{dict:'subjType'}">科目类型</div>
                <div field="debitCredit"  headerAlign="center" align="center"  width="100px" renderer="CommonUtil.dictRenderer" data-options="{dict:'DebitCreditFlag'}" >余额借贷方向</div>
                <div field="taskId"   visible="false">账套编号</div>
                <div field="taskName"  visible="false">账套名称</div>    
                <div field="parentSubjCode"   headerAlign="center"  width="100px"  allowSort="true">上级科目号</div>
                <div field="parentSubjName"    headerAlign="center"  width="180px">上级科目名称</div>
                <div field="subjNature"   visible="false">科目性质</div> 
                <div field="payRecive"   visible="false">余额收付方向</div>
                <div field="direction"   visible="false">收支方向</div> 
                <div field="ifParentAll"   headerAlign="center" align="center"  width="100px">是否根科目</div> 

            </div>
        </div>
    </div>

    <script>
        mini.parse();
        $(document).ready(function(){
            //控制按钮显示
            $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {

                var grid = mini.get("subject_grid");
                grid.on("beforeload", function (e) {
                    e.cancel = true;
                    var pageIndex = e.data.pageIndex;
                    var pageSize = e.data.pageSize;
                    search(pageSize, pageIndex);
                });
                search(grid.pageSize, 0);
                loadTaskName();
            });
        });


        top["subjectManage"]=window;
        function getData(){
            var grid = mini.get("subject_grid");
            var record =grid.getSelected(); 
            return record;
        }
       

        //新增
        function add(){
            var url = CommonUtil.baseWebPath() + "/mini_cap/mini_base/MiniSubjectEdit.jsp?action=new";
            var tab = { id: "subjectManageAdd", name: "subjectManageAdd", text: "科目管理新增", url: url,showCloseButton:true,parentId:top['win'].tabs.getActiveTab().name };
            top['win'].showTab(tab);
        }
        //修改
        function modifyedit(){
            var grid = mini.get("subject_grid");
            var  record =grid.getSelected();
            if(record){
                var url = CommonUtil.baseWebPath() + "/mini_cap/mini_base/MiniSubjectEdit.jsp?action=edit";
                var tab = { id: "subjectManageEdit", name: "subjectManageEdit", text: "科目管理修改", url: url,showCloseButton:true,parentId:top['win'].tabs.getActiveTab().name };
                top['win'].showTab(tab);
            }else{
                mini.alert("请选中一条记录","系统提示"); 
            }
        }

        //双击一行打开详情信息
        function custDetail(e){
            var grid = e.sender;
            var row = grid.getSelected();
            if(row){
                var url = CommonUtil.baseWebPath() + "/mini_cap/mini_base/MiniSubjectEdit.jsp?action=detail";
                var tab = { id: "subjectManageDetail", name: "subjectManageDetail", text: "科目管理详情", url: url,showCloseButton:true,parentId:top['win'].tabs.getActiveTab().name };
                top['win'].showTab(tab);
            }
        }


        //加载账套信息
        function loadTaskName(){
            CommonUtil.ajax({
                url:"/TtAccountingTaskController/selectAccountingTaskList",
                data:{},
                
                callback:function(data){
                        
                    var taskId =mini.get("taskId");
                    
                    //设置数据
                    taskId.setData(data.obj);
                    
                }
            });
        }

        //清空
        function clear(){
            var grid =mini.get("subject_grid");
            var form =new mini.Form("search_form");
            form.clear();
            search(grid.pageSize,0);
            
        }

        //查询(带分页)
        function search(pageSize,pageIndex){
            var form =new mini.Form("search_form");
            form.validate();
            if (form.isValid() == false) return;//表单验证
            var data =form.getData();//获取表单数据
            data['pageNumber']=pageIndex+1;
            data['pageSize']=pageSize;
            var param = mini.encode(data); //序列化成JSON
            CommonUtil.ajax({
                url:"/TbkSubjectDefController/searchPageSubject",
                data:param,
                
                callback:function(data){
                    if("error.common.0000" == data.code){
                    var subjects=data.obj.rows;
                    if(subjects.length>0){
                        var newSubjs = new Array();
                        for(var i=0;i<subjects.length;i++){
                            var group = subjects[i];
                            var a={
                                "subjCode":group.subjCode,
                                "subjName":group.subjName,
                                "subjType":group.subjType,
                                "debitCredit":group.debitCredit,
                                "taskId":group.taskId,
                                "taskName":group.taskName,
                                "parentSubjCode":group.parentSubjCode,
                                "parentSubjName":group.parentSubjName,
                                "subjNature":group.subjNature,
                                "payRecive":group.payRecive,
                                "direction":group.direction,
                                "ifParentAll":group.parentSubjCode==-1?"是":"否",
                            };
                            newSubjs.push(a);
                        }
                        
                        var grid =mini.get("subject_grid");
                        //设置分页
                        grid.setTotalCount(data.obj.total);
                        grid.setPageIndex(pageIndex);
                        grid.setPageSize(pageSize);
                        //设置数据
                        grid.setData(newSubjs);
                            
                    }else{
                        mini.alert("没有查询到该账套相关的科目信息","系统提示");
                        var grid =mini.get("subject_grid");
                        //设置分页
                        grid.setTotalCount(data.obj.total);
                        grid.setPageIndex(pageIndex);
                        grid.setPageSize(pageSize);
                        //设置数据
                        grid.setData(data.obj.rows);
                    }
                }
                }
            });
        }

          //删除
          function removeRows() {
            var grid = mini.get("subject_grid");
            //获取选中行
            var rows =grid.getSelected();
            
            if (rows) {
                
                var data={};
                data['taskId']=rows.taskId;
                data['subjCode']=rows.subjCode;
                var param = mini.encode(data);
                var str="";
                str=rows.subjName+"("+ rows.subjCode+")";
                
                mini.confirm("您确认要删除科目<span style='color:#FF3333;font-weight:bold;'>" +str+"</span>吗?","系统警告",function(value){   
                    if (value == "ok"){   
                        CommonUtil.ajax({
                            url : '/TbkSubjectDefController/delTbkSubjectDefByTaskIdAndSubjCode/',
                            data : param,
                            callback : function(data){
                                if("error.common.0000" == data.code){
                                    mini.alert("删除成功！","系统提示");
                                    var grid =mini.get("subject_grid");
                                    search(grid.pageSize,0);
                                }
                            }
                        });
                    
                    }   
                });       
                    
            }else{
                mini.alert("请选择一条记录!","系统提示");
            }
        }







    </script>

</body>
</html>
