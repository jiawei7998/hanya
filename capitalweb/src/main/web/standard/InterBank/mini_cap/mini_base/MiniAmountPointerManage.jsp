<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="../../../global.jsp"%>
<fieldset class="mini-fieldset title">
    <legend>条件查询</legend>
    <div id="search_form">
            <input id="amountName" name="amountName" class="mini-textbox" labelField="true" label="金额名称：" labelStyle="text-align:right;" style="width:310px;"
            emptyText="请输入金额代码名称" />
            <span style="float:right;margin-right: 150px">
                <a class="mini-button" style="display: none"  id="search_btn"  onclick="search(10,0)">查询</a>
                <a class="mini-button" style="display: none"  id="clear_btn"  onclick="clear()">清空</a>
            </span>
    </div>
</fieldset>
<span style="margin:2px;display: block;">
    <a id="add_btn" class="mini-button" style="display: none"   onclick="add()">新增</a>
    <a id="edit_btn" class="mini-button" style="display: none"   onclick="edit()">修改</a>
    <a id="delete_btn" class="mini-button" style="display: none"   onclick="removeObj()">删除</a>
</span>
<div id="amountPointerManager" class="mini-fit" style="margin-top: 2px;width:100%;height:100%;">
<div id="amountPointer" class="mini-datagrid borderAll" style="width:100%;height:100%;" allowResize="true" idField="amountId"
    pageSize="10" multiSelect="true" allowAlternating="true" onRowdblclick="amonutPointer" >
    <div property="columns">
        <div type="indexcolumn" width="12px">序号</div>
        <div field="amountId" headerAlign="center"   width="80px">金额代码</div>
        <div field="amountName" headerAlign="center"  width="60px">金额名称</div>
        <div field="amountFormula" headerAlign="center"  >金额公式</div>
        <div field="amountDesc" headerAlign="center" >金额描述</div>
    </div>
</div>
</div>
<script>
mini.parse();

var amount=mini.get("amountPointer");
$(document).ready(function(){
    //控制按钮显示
    $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
        amount.on("beforeload", function (e) {
            e.cancel = true;
            var pageIndex = e.data.pageIndex;
            var pageSize = e.data.pageSize;
            search(pageSize, pageIndex);
        });
        search(10, 0);
    });
})
//查询
function search(pageSize,pageIndex){
    var form = new mini.Form("search_form");
    form.validate();
    if (form.isValid() == false) return;//表单验证
    var data = form.getData();//获取表单数据
    data['pageNumber'] = pageIndex + 1;
    data['pageSize'] = pageSize;
    var param = mini.encode(data);
    CommonUtil.ajax({
        url: "/TbkAmountController/searchAmountPointerPage",
        data: param,
        callback: function (data) {
            amount.setTotalCount(data.obj.total);
            amount.setPageIndex(pageIndex);
            amount.setPageSize(pageSize);
            amount.setData(data.obj.rows);
        }
    });
}
//清空
function clear() {
    var form = new mini.Form("search_form");
    form.clear();
}
//新增
function add(){
    var url = CommonUtil.baseWebPath() + "/mini_cap/mini_base/MiniAmountPointerEdit.jsp?action=add";
    var tab = { id: "amountPointAdd", name: "amountPointAdd", text: "金额代码新增", url: url,  parentId: top["win"].tabs.getActiveTab().name };
    top['win'].showTab(tab);
}
//修改
top["amountPoint"] = window;
function getData(action) {
    if(action=="edit" || action=="detail"){
    var record = amount.getSelected();
    return record;
    }
    return '';
}
function edit(){
    var record =amount.getSelected();
    if(record){
        var url = CommonUtil.baseWebPath() + "/mini_cap/mini_base/MiniAmountPointerEdit.jsp?action=edit";
        var tab = { id: "amountPointEdit", name: "amountPointEdit", text: "金额代码修改", url: url, parentId: top["win"].tabs.getActiveTab().name };
        top['win'].showTab(tab);
    }else{
        mini.alert("请选择数据进行修改","温馨提示");
    }
} 
//详情  新加的
 function amonutPointer(e){
    var grid=e.sender;
    var row=grid.getSelected();
    if(row){
        var url=CommonUtil.baseWebPath() +"/mini_cap/mini_base/MiniAmountPointerEdit.jsp?action=detail";
        var tab={id:"amountDetail" ,name:"amountDetail",text:"金额代码详情",url:url,parentId: top["win"].tabs.getActiveTab().name };
        top['win'].showTab(tab);
     }
    }
//删除
function removeObj() {
    var record = amount.getSelected();
    if (record) {
        mini.confirm("你确定要删除吗", "系统提示", function (r) {
            if (r == 'ok') {
                CommonUtil.ajax({
                    url: "/TbkAmountController/deleteAmount",
                    data: mini.encode({ "amountId": record.amountId }),
                    callback: function (data) {
                        if (data.code == "error.common.0000") {
                            mini.alert("删除成功", "系统提示", function () {
                                search(10, 0);
                            });
                        } else {
                            mini.alert("删除失败", "系统提示");
                        }
                    }
                });
            }
        })
    } else {
        mini.alert("请选择要删除的数据", "温馨提示");
    }
}
</script>