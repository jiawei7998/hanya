<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../../global.jsp"%>
<script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
<fieldset class="mini-fieldset">
<legend>调账查询</legend>
<div id="search_form">
        <input id="flowId" name="flowId" class="mini-textbox"  labelField="true"  label="账务流水号：" labelStyle="text-align:right;"  emptyText="请输入账务流水号"/>
        <input id="subjName" name="subjName" class="mini-textbox" vtype="maxLength:50" labelField="true"  label="科目名称：" labelStyle="text-align:right;" emptyText="请输入科目名称"/>
        <input id="sponInst" width="300px" name="sponInst" valueField="id" labelField="true" label="所属机构："  labelStyle="text-align:right;"
        showTreeLines="true" showTreeIcon="true" textField="instName" parentField="parentId" class="mini-treeselect" resultAsTree="false"
        valueFromSelect="true" multiSelect="false" emptyText="请选择机构" />
        <span style="float:right;margin-right: 150px">
            <a class="mini-button" style="display: none"  id="search_btn"  onclick="search(10,0)">查询</a>
            <a class="mini-button" style="display: none"  id="clear_btn"  onclick="clear()">清空</a>
        </span>
</div>
</fieldset>
<span style="margin:2px;display: block;">
    <a id="add_btn" class="mini-button" style="display: none"   onclick="add()">新增</a>
    <a id="edit_btn" class="mini-button" style="display: none"   onclick="edit()">修改</a>
    <a id="delete_btn" class="mini-button" style="display: none"   onclick="del()">删除</a>
    <a id="approve_commit_btn" class="mini-button" style="display: none"    onclick="approve()">审批</a>
    <a id="approve_mine_commit_btn" class="mini-button" style="display: none"    onclick="commit()">提交审批</a>
    <a id="approve_log" class="mini-button" style="display: none"    onclick="logAll()">审批日志</a>
    <div id="approveType" name="approveType" labelField="true" label="审批列表选择:" class="mini-checkboxlist" repeatItems="0" repeatLayout="flow"
    value="approve" textField="text" valueField="id" multiSelect="false"  style="float:right;margin-right: 50px;" data="[{id:'approve',text:'待审批列表'},{id:'finished',text:'已审批列表'},{id:'mine',text:'我发起的'}]"
    labelStyle="text-align:right;"  onvaluechanged="checkBoxValuechanged">
</div>
</span>

<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:85%;" idField="flowId" allowResize="true" allowAlternating="true"  multiSelect="false">
    <div property="columns">
        <div field="flowId" name="ck" type="checkcolumn" headerAlign="center"></div>
        <div field="flowId" name="flowId" width="108" headerAlign="center" allowSort="true">账务流水号</div>   
        <div field="approveStatus" width="90" renderer="CommonUtil.dictRenderer" data-options="{dict:'ApproveStatus'}" align="center" headerAlign="center">审批状态</div>
        <div field="taskName" width="100" headerAlign="center" allowSort="true">当前环节</div>   
        <div field="flowSeq" width="60" headerAlign="center" allowSort="true">序号</div>   
        <div field="postDate" width="100" headerAlign="center" allowSort="true">账务日期</div>   
        <div field="dealNo" width="220" headerAlign="center" allowSort="true">交易流水号</div>   
        <div field="outDate"  align="right" headerAlign="center" width="80" renderer="CommonUtil.dictRenderer" data-options="{dict:'YesNot'}" allowSort="true">是否逾期</div>
        <div field="prdName" width="160"  headerAlign="center">产品类型</div>
        <div field="fundCode"  align="right" width="80" headerAlign="center" allowSort="true">基金代码</div>
        <div field="sponInst"   headerAlign="center" width="80" allowSort="true">交易机构</div>
        <div field="bkkpgOrgId" width="80" headerAlign="center" allowSort="true">记账机构</div>   
        <div field="ccy" width="80" headerAlign="center"  allowSort="true">币种</div>   
        <div field="debitCreditFlag" width="100" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'DebitCreditFlag'}" allowSort="true">借贷标识</div>   
        <div field="subjCode" width="120" headerAlign="center" allowSort="true">科目号</div>   
        <div field="subjName" width="200" headerAlign="center" allowSort="true">科目名称</div>
        <div field="value" headerAlign="center" width="200" align="right" allowSort="true" numberFormat="#,0.00">实际金额</div>
        <div field="updateTime" headerAlign="center" align="center"  allowSort="true">更新日期</div>
    </div>
</div>  

<script>
mini.parse();

var instId =mini.get("sponInst");
var approveType=mini.get("approveType");
var grid = mini.get("datagrid");
$(document).ready(function(){
    //控制按钮显示
    $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
        grid.on("beforeload", function (e) {
            e.cancel = true;
            var pageIndex = e.data.pageIndex;
            var pageSize = e.data.pageSize;
            search(pageSize, pageIndex);
        })
        institution();
        checkBoxValuechanged();
    });
})
//提交审批
function commit(){
    if(approveType.getValue()=='mine'){
    var record =grid.getSelected();
    if(record){
        if (record.approveStatus == '3') {
            Approve.approveCommit(Approve.FlowType.ManualAdjFlow, record.flowId, Approve.OrderStatus.New,
                function () { checkBoxValuechanged(); }, null, '');
        } else {
            mini.alert("新建状态才能提交审批", "系统提示");
        }
        
    }else{
        mini.alert("请选择一条数据","温馨提示");
    }
    }
}


//审批
function approve(){
    if(approveType.getValue()=='approve'){
        var record =grid.getSelected();
        if(record){
            var url = CommonUtil.baseWebPath() +"/mini_cap/mini_base/MiniManualReconciliationEdit.jsp?action=approve&flowId=" + record.flowId+"&task_id="+record.taskId1+"&flow_type="+Approve.FlowType.ManualAdjFlow;
			var tab = {id: "ManualReconciliationApprove",name:"ManualReconciliationApprove",url:url,text:"手工调账审批",parentId:top["win"].tabs.getActiveTab().name };
			top["win"].showTab(tab);
        }else{
            mini.alert("请选择要审批的数据","系统提示");
        }

    }

}

//查看日志
function logAll(){
    var record=grid.getSelected();
    if(record){
        Approve.approveLog(Approve.FlowType.ManualAdjFlow,record.flowId);
    }else{
        mini.alert("请选择一条数据","系统提示");
    }
}

//新增
function add(){
    var url = CommonUtil.baseWebPath() + "/mini_cap/mini_base/MiniManualReconciliationEdit.jsp?action=add";
    var tab = { id: "manualAdd", name: "manualAdd", text: "手工调账新增", url: url,  parentId: top["win"].tabs.getActiveTab().name };
    top['win'].showTab(tab);
}
//修改
function edit(){
    var records=grid.getSelected();
    if(records){
        if(approveType.getValue()=='mine'){
            if(records.approveStatus == '3'){
                var url = CommonUtil.baseWebPath() + "/mini_cap/mini_base/MiniManualReconciliationEdit.jsp?action=edit&flowId=" + records.flowId;
                var tab = { id: "manualEdit", name: "manualEdit", text: "手工调账修改", url: url, parentId: top["win"].tabs.getActiveTab().name };
                top['win'].showTab(tab);
            }else{
                mini.alert("新建状态才能修改","系统提示");
            }
           
        }
       
    }else{
        mini.alert("请选择要修改的数据","温馨提示");
    }
    
}
//删除
function del() {
    var record = grid.getSelected();
    if (record) {
        if(record.approveStatus == '3'){
            mini.confirm("您确定要删除吗？", "系统提示", function (r) {
            if (r == 'ok') {
                CommonUtil.ajax({
                    url: '/TbkManualEntryController/deleteManualEntry',
                    data: mini.encode({ "flowId": record.flowId }),
                    callback: function (data) {
                        if (data.code = "error.common.0000") {
                            mini.alert("删除成功", "温馨提示", function () {
                                search(10, 0);
                            });
                        }
                    }
                });
            }
        })
        }else{
            mini.alert("新建状态才能删除！","系统提示");
        }
    } else {
        mini.alert("请选择一条记录删除", "温馨提示");
    }

}
//查询
function search(pageSize,pageIndex){
    var form =new mini.Form("search_form"); 
    var formData=form.getData();
    form.validate();
    if (form.isValid() == false) {
        mini.alert("表单填写错误,请确认!", "提示信息")
        return;
    }
    var url ='';
    if(approveType.getValue()=='approve'){//待审批
        url='/TbkManualEntryController/searchPageTbkYearEntryUnfinishedHand';

    }else if(approveType.getValue()=='finished'){//已审批
        url='/TbkManualEntryController/searchPageTbkYearEntryFinished';
    }else{//我发起的
        url='/TbkManualEntryController/searchPageTbkYearEntryByMyself';
    }
    formData['pageNumber'] = pageIndex + 1;
    formData['pageSize'] = pageSize;
    CommonUtil.ajax({
        url: url,
        data: mini.encode(formData),
        callback: function (data) {
            grid.setTotalCount(data.obj.total);
            grid.setPageIndex(pageIndex);
            grid.setPageSize(pageSize);
            grid.setData(data.obj.rows);
            grid.mergeColumns(['flowId','ck']);
           
        }
    });
}
//加载机构树
function institution() {
	var param = mini.encode({ branchId:branchId }); //序列化成JSON
	CommonUtil.ajax({
		url: "/InstitutionController/searchInstitutionByBranchId",
		data: param,
		callback: function (data) {
			instId.setData(data.obj);
		}
	});
}
//按钮的控制
function checkBoxValuechanged() {
    setBtnVisibleByTab();
    search(10,0);
}
function setBtnVisibleByTab(){
    $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
        var approveType = mini.get("approveType").getValue();
        if (approveType == "approve") {
            if (visibleBtn.clear_btn) mini.get("clear_btn").setVisible(true);
            if (visibleBtn.search_btn) mini.get("search_btn").setVisible(true);
            if (visibleBtn.approve_commit_btn) mini.get("approve_commit_btn").setVisible(true);
            if (visibleBtn.approve_mine_commit_btn) mini.get("approve_mine_commit_btn").setVisible(false);
            if (visibleBtn.approve_log) mini.get("approve_log").setVisible(true);
            if (visibleBtn.add_btn) mini.get("add_btn").setVisible(false);
            if (visibleBtn.edit_btn) mini.get("edit_btn").setVisible(false);
            if (visibleBtn.delete_btn) mini.get("delete_btn").setVisible(false);

        } else if (approveType == "finished" || approveType == "抄送我的") {
            if (visibleBtn.clear_btn) mini.get("clear_btn").setVisible(true);
            if (visibleBtn.search_btn) mini.get("search_btn").setVisible(true);
            if (visibleBtn.approve_commit_btn) mini.get("approve_commit_btn").setVisible(false);
            if (visibleBtn.approve_mine_commit_btn) mini.get("approve_mine_commit_btn").setVisible(false);
            if (visibleBtn.approve_log) mini.get("approve_log").setVisible(true);
            if (visibleBtn.add_btn) mini.get("add_btn").setVisible(false);
            if (visibleBtn.edit_btn) mini.get("edit_btn").setVisible(false);
            if (visibleBtn.delete_btn) mini.get("delete_btn").setVisible(false);
        } else {
            if (visibleBtn.clear_btn) mini.get("clear_btn").setVisible(true);
            if (visibleBtn.search_btn) mini.get("search_btn").setVisible(true);
            if (visibleBtn.approve_commit_btn) mini.get("approve_commit_btn").setVisible(false);
            if (visibleBtn.approve_mine_commit_btn) mini.get("approve_mine_commit_btn").setVisible(true);
            if (visibleBtn.approve_log) mini.get("approve_log").setVisible(true);
            if (visibleBtn.add_btn) mini.get("add_btn").setVisible(true);
            if (visibleBtn.edit_btn) mini.get("edit_btn").setVisible(true);
            if (visibleBtn.delete_btn) mini.get("delete_btn").setVisible(true);
        }
    });
} 
 //清空
 function clear(){
    var form = new mini.Form("#search_form");
     form.clear();
 }              
</script>    