<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="../../../global.jsp" %>
<html>
<head>
    <title>维度字典管理</title>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset id="fd2" class="mini-fieldset">
    <legend><label>查询条件</label></legend>
    <div id="search_form" width="100%">
        <input id="paramName" name="paramName" class="mini-textbox" labelField="true" label="维度名称："
               labelStyle="text-align:right;" emptyText="请输入维度名称"/>
        <input id="paramId" name="paramId" class="mini-textbox" labelField="true" label="维度编号："
               labelStyle="text-align:right;" emptyText="请输入维度编号"/>
        <input id="paramSource" name="paramSource" class="mini-textbox" labelField="true" label="维度字段："
               labelStyle="text-align:right;" emptyText="请输入维度字段"/>
        <span style="float:right;margin-right: 150px">
                        <a id="search_btn" class="mini-button" style="display: none" onclick="search(10,0)">查询</a>
                        <a id="clear_btn" class="mini-button" style="display: none" onclick='clear()'>清空</a>
                </span>
        <div id="toolbar1" class="mini-toolbar" style="margin-left: 20px;">
            <!--  <table style="width:100%;">
                 <tr>
                 <td style="width:100%;">
                     <a id="clear_btn" class="mini-button" style="display: none"     onClick="clear">清空</a>
                     <a id="search_btn" class="mini-button" style="display: none"     onclick="search(10,0)">查询</a>

                 </td>
                 </tr>
             </table> -->
            <!--  </div> -->
        </div>
</fieldset>
<span style="margin:2px;display:block">
        <a id="add_btn" class="mini-button" style="display: none" onclick="add">新增</a>
                        <a id="edit_btn" class="mini-button" style="display: none" onclick="edit()">修改</a>
                        <a id="delete_btn" class="mini-button" style="display: none" onclick="removeRows">删除</a>

    </span>

<div id="paramManage" class="mini-fit" style="margin-top: 2px;">
    <div id="paramManageGrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" allowAlternating="true"
         idField="paramId" allowResize="true" pageSize="10" multiSelect="true" onRowdblclick="custDetail"
         sortMode="client">
        <div property="columns">
            <!-- <div type="checkcolumn"></div> -->
            <div type="indexcolumn" headerAlign="center" align="center" width="50px">序号</div>
            <div field="paramId" width="150" align="left" headerAlign="center" allowSort="false">维度编号</div>
            <div field="paramName" width="150" align="left" headerAlign="center" allowSort="false">维度名称</div>
            <div field="paramSource" width="150" align="left" headerAlign="center" allowSort="false">维度字段</div>
            <div field="remark" width="150" align="left" headerAlign="center" allowSort="false">维度说明</div>
            <!-- <div field="lastUpdate" width="150" align="center"  headerAlign="center" allowSort="false">最后更新时间</div>-->
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        mini.parse();
        var grid = mini.get("paramManageGrid");
        grid.on("beforeload", function (e) {
            e.cancel = true;
            var pageIndex = e.data.pageIndex;
            var pageSize = e.data.pageSize;
            search(pageSize, pageIndex);
        });

        search(10, 0);

    });
    top["paramManageGrid"] = window;

    //查询
    function search(pageSize, pageIndex) {
        var form = new mini.Form("search_form");
        form.validate();
        if (form.isValid() == false) return;//表单验证
        var data = form.getData();//获取表单数据
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        // data['branchId']=top['win'].branchId;
        var param = mini.encode(data); //序列化成JSON
        ////console.log(param);
        CommonUtil.ajax({
            url: '/ParamsDicController/searchParamsDicPage',
            data: param,
            //relbtn:'mini-btn',
            callback: function (data) {

                var grid = mini.get("paramManageGrid");
                //设置分页
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                //设置数据
                grid.setData(data.obj.rows);
            }
        });
    }

    //清空
    function clear() {
        var form = new mini.Form("search_form");
        form.clear();
    }

    //删除
    function removeRows() {
        var grid = mini.get("paramManageGrid");
        //获取选中行
        var rows = grid.getSelected();
        //获取行索引号,行索引号从0开始
        if (rows) {
            mini.confirm("您确认要删除选中记录?", "系统警告", function (value) {
                if (value == "ok") {
                    CommonUtil.ajax({
                        url: "/ParamsDicController/deleteParamsDic",
                        data: {paramId: rows.paramId},
                        callback: function (data) {
                            if (data.code == 'error.common.0000') {
                                mini.alert("删除成功")
                                search(grid.pageSize, grid.pageIndex);
                            } else {
                                mini.alert("删除失败");
                            }
                        }
                    });
                }
            });
        } else {
            mini.alert("请选中一条记录！", "消息提示");
        }
    }

    function add() {
        var url = CommonUtil.baseWebPath() + "/mini_cap/mini_base/MiniParamsEdits.jsp?action=add";
        var tab = {
            id: "MiniParamsAdd",
            name: "MiniParamsAdd",
            text: "维度字典新增",
            url: url,
            showCloseButton: true,
            parentId: top["win"].tabs.getActiveTab().name
        };
        top['win'].showTab(tab);
    }

    //修改
    function edit() {
        var grid = mini.get("paramManageGrid");
        var record = grid.getSelected();
        if (record) {
            var url = CommonUtil.baseWebPath() + "/mini_cap/mini_base/MiniParamsEdits.jsp?action=edit";
            var tab = {
                id: "paramsEdit",
                name: "paramsEdit",
                text: "维度字典修改",
                url: url,
                parentId: top['win'].tabs.getActiveTab().name
            };

            top['win'].showTab(tab);
        } else {
            mini.alert("请选中一条记录", "系统提示");
        }
    }

    //双击一行打开详情信息
    function custDetail(e) {
        var grid = e.sender;
        var row = grid.getSelected();
        if (row) {
            var url = CommonUtil.baseWebPath() + "/mini_cap/mini_base/MiniParamsEdits.jsp?action=detail";
            var tab = {
                id: "paramsDetail",
                name: "paramsDetail",
                text: "维度详情",
                url: url,
                parentId: top['win'].tabs.getActiveTab().name
            };
            top['win'].showTab(tab);
        }
    }

    function getData(action) {
        var grid = mini.get("paramManageGrid");
        if (action != "add") {
            var row = null;
            row = grid.getSelected();
        }
        return row;
    }


</script>
</body>
</html>