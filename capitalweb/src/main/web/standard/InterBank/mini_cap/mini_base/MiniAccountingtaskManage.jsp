<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="../../../global.jsp"%>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<body>

<fieldset id="fd2" class="mini-fieldset">
    <legend><label>查询条件</label></legend>
    <div id="search_form">
            <input id="taskName" name="taskName" class="mini-textbox" labelField="true"  label="账套名称：" labelStyle="text-align:right;"  emptyText="请输入账套名称"/>
            <input id="taskType" name="taskType" class="mini-combobox" labelField="true"  label="账套类型：" labelStyle="text-align:right;"  width="200px"  data="CommonUtil.serverData.dictionary.AcTaskType" emptyText="请输入账套类型" />
            <span style="float:right;margin-right: 150px">
                <a class="mini-button" style="display: none"  id="search_btn"  onclick="search(10,0)">查询</a>
                <a class="mini-button" style="display: none"  id="clear_btn"  onclick="clear()">清空</a>
            </span>

    </div>
</fieldset>
<span style="margin:2px;display: block;">
    <a id="add_btn" class="mini-button" style="display: none"    onclick="add">新增</a>
    <a id="edit_btn" class="mini-button" style="display: none"    onclick="edit()">修改</a>
    <a id="delete_btn" class="mini-button" style="display: none"    onclick="remove">删除</a>
</span>
    

<div id="paramManage" class="mini-fit" style="margin-top: 2px;">
    <div id="accountTaskList" class="mini-datagrid borderAll" style="width:100%;height:100%;" 
    idField="taskId"  sortMode="client" multiSelect="true" allowAlternating="true" onRowdblclick="paramDetail">
        <div property="columns" >
            <div type="indexcolumn">序号</div>
            <div field="taskId"   headerAlign="center">账套ID</div>    
            <div field="taskType"   align="center"  renderer="CommonUtil.dictRenderer" data-options="{dict:'AcTaskType'}" headerAlign="center" >账套类型</div> 
            <div field="taskName"    headerAlign="center" >账套名称</div>                           
            <div field="taskCurrentDate"  align="center"  headerAlign="center" >当前账务日期</div> 
            <div field="bkkpgOrgId"  headerAlign="center" >记账机构</div> 
            <div field="bkMainNm"    headerAlign="center" >交易主体</div>                           
            <div field="instIdNm"   headerAlign="center" >交易机构</div> 
            <div field="defaultTask" align="center" renderer="onYesOrNoRenderer"  headerAlign="center">是否启用</div>  
            <div field="updateTime" headerAlign="center">最后修改时间</div>                                                                                             
        </div>
    </div>
</div>
    
</body>
<script>
mini.parse();

var searchForm =mini.get("search_form");
var accountTask=mini.get("accountTaskList");
$(document).ready(function(){
    //控制按钮显示
    $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
        accountTask.on("beforeload", function (e) {
            e.cancel = true;
            var pageIndex = e.data.pageIndex;
            var pageSize = e.data.pageSize;
            search(pageSize, pageIndex);
        });
        search(10, 0);
    });
})
//查询
function search(pageSize,pageIndex){
    var form = new mini.Form("search_form");
    form.validate();
    if (form.isValid() == false) return;//表单验证
    var data = form.getData();//获取表单数据
    data['pageNumber'] = pageIndex + 1;
    data['pageSize'] = pageSize;
    var param = mini.encode(data);
    CommonUtil.ajax({
        url: "/TtAccountingTaskController/selectPageAccountingTask",
        data: param,
        callback: function (data) {
            accountTask.setTotalCount(data.obj.total);
            accountTask.setPageIndex(pageIndex);
            accountTask.setPageSize(pageSize);
            accountTask.setData(data.obj.rows);
        }
    });
}
//清空
function clear() {
    var form = new mini.Form("search_form");
    form.clear();
}
//新增
function add(){
    var url = CommonUtil.baseWebPath() + "/mini_cap/mini_base/MiniAccountingTaskEdit.jsp?action=add";
    var tab = { id: "accountingAdd", name: "accountingAdd", text: "套账新增", url: url,  parentId: top["win"].tabs.getActiveTab().name };
    top['win'].showTab(tab);
}
    top["account"] = window;
    function getData() {
        var record = accountTask.getSelected();
        return record;
    }
//修改
function edit(){
    var record =accountTask.getSelected();
    if(record){
        var url = CommonUtil.baseWebPath() + "/mini_cap/mini_base/MiniAccountingTaskEdit.jsp?action=edit";
        var tab = { id: "accountingEdit", name: "accountingEdit", text: "套账修改", url: url, parentId: top["win"].tabs.getActiveTab().name };
        top['win'].showTab(tab);
    }else{
        mini.alert("请选择数据进行修改","温馨提示");
    }
}
//详情  新增的
function paramDetail(e){
    accountTask=e.sender;
    var row=accountTask.getSelected();
    if(row){
        var url=CommonUtil.baseWebPath() + "/mini_cap/mini_base/MiniAccountingTaskEdit.jsp?action=detail";
        var tab= {id:"accountTaskDetail",name:"accountTaskDetail",text:"套账详情",url:url,parentId:top['win'].tabs.getActiveTab().name};
        top['win'].showTab(tab);
    }
}
//删除
function remove(){
    var record=accountTask.getSelected();
    if(record){
        var list =new Array();
        list.push(record.taskId);
        mini.confirm("您确定要删除？","系统提示",function(r){
            if(r=='ok'){
                CommonUtil.ajax({
                    url: "/TtAccountingTaskController/delTacTask?",
                    data: mini.encode(list),
                    callback: function (data) {
                        if(data.code=="error.common.0000"){
                            mini.alert("删除成功","系统提示",function(){
                                search(10,0);
                            });
                        }
                    }
                });
            }
        })
    }else{
        mini.alert("请选择一个数据删除","温馨提示");
    }
}
var YesOrNo = [{ "id": "1", "text": "是" }, { "id": "0", "text": "否" }];
    //渲染启用状态
    function onYesOrNoRenderer(e) {
        for (var i = 0, l = YesOrNo.length; i < l; i++) {
            var g = YesOrNo[i];
            if (g.id == e.value) return g.text;
        }
    }  
</script>