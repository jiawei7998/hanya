<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>

<%@ include file="../../../global.jsp"%> 
<html>
<head>
    <title>查询上级科目名称</title>
</head>
<body>
    <fieldset id="fd2" class="mini-fieldset" width="100%" height="400px">
        <legend><label id="labell">查询条件</label></legend>
        <div id="field" class="fieldset-body">
            <div id="subject_form" >
                    <input id="subjCode" name="subjCode" class="mini-textbox"  labelField="true"  
                        label="科目号：" emptyText='请输入科目号' labelStyle="text-align:right;"/>
                    
                    <input id="subjName" name="subjName" class="mini-textbox"  labelField="true" 
                        label="科目名称：" emptyText='请输入科目名称' labelStyle="text-align:right;"/>
                    <span style="float:right;margin-right: 100px">
                        <a id="search_btn" class="mini-button" style="display: none"    onclick="search(10,0)">查询</a>
                    </span>
                    
            </div>
            
        </div>
       
    </fieldset>
    
    <div class="mini-fit">
       
        <div id="subject_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" 
        idField="p_code" allowResize="true" pageSize="10"  onRowdblclick="onRowDblClick" sortMode="client"
        allowAlternating="true">
            <div property="columns">
                <div type="indexcolumn" headerAlign="center" align="center" >序号</div>
                <div field="subjCode" headerAlign="center" align="center" width="100px" allowSort="true">科目号</div>
                <div field="subjName" headerAlign="center" width="180px">科目名称</div>
                <div field="parentSubjCode"  headerAlign="center"  align="center"  width="80px" allowSort="true">上级科目号</div>
                <div field="parentSubjName" headerAlign="center" width="" visible="false">上级科目名称</div>
                <div field="subjType"  headerAlign="center"  width="100px"  renderer="CommonUtil.dictRenderer" data-options="{dict:'subjType'}">科目类型</div>
                <div field="debitCredit"  headerAlign="center"  width="60px"  renderer="CommonUtil.dictRenderer" data-options="{dict:'DebitCreditFlag'}">余额借贷方向</div>
                <div field="payRecive" headerAlign="center"  width="" visible="false">余额收付方向</div>
                
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function(){
             var grid =mini.get("subject_grid");
            grid.on("beforeload",function(e){
                 e.cancel = true;
                 var pageIndex = e.data.pageIndex; 
                 var pageSize = e.data.pageSize;
                 search(pageSize,pageIndex);
             });

            search(10,0);
        });


        mini.parse();


        function search(pageSize,pageIndex){
             var form =new mini.Form("subject_form");
             form.validate();
             if (form.isValid() == false) return;//表单验证
             var data =form.getData();//获取表单数据
             data['pageNumber']=pageIndex+1;
             data['pageSize']=pageSize;
            var param = mini.encode(data); //序列化成JSON
            CommonUtil.ajax({					
                url : '/TbkSubjectDefController/searchPageSubject',
                data : param,
                callback : function(data) 
                {
                    var grid = mini.get("subject_grid");
                    //设置分页
                     grid.setTotalCount(data.obj.total);
                     grid.setPageIndex(pageIndex);
                     grid.setPageSize(pageSize);
                    grid.setData(data.obj.rows);
                }
		    }); 
        }

        function GetData() {
            var grid = mini.get("subject_grid");
            var row = grid.getSelected();
            return row;
        }

        function onRowDblClick(e) {
            window.CloseOwnerWindow("ok");
        }
        
    </script>

</body>
</html>