<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="../../../global.jsp" %>
<html>
<head>
    <title>分录凭证管理</title>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/sl/AccountJsController/accounting.js"></script>
</head>
<body>
<span id="labell"><a href="javascript:CommonUtil.activeTab();">分录凭证管理</a> >> 修改</span>
<div id="editForm" class="fieldset-body">
    <!-- <fieldset class="mini-fieldset">
            <legend>参数信息</legend> -->
    <table id="accout_form" class="mini-table" width="100%">
        <tr>
            <td>

                <input id="settingGroupId" name="settingGroupId" class="mini-textbox" enabled="false"
                       vtype="rangeLength:1,100"
                       labelField="true" label="套账分录ID：" emptyText="自动生成"/>
            </td>
            <td>
                <input id="eventId" name="eventId" class="mini-combobox" valueField="eventId" labelField="true"
                       textField="eventName"
                       showNullItem="false" allowInput="false" label="会计事件：" required="true">
            </td>
            <td>
                <input id="settingDesc" name="settingDesc" class="mini-textbox mini-validatebox" labelField="true"
                       label="套账分录描述：" vtype="rangeLength:1,100"/>
            </td>
        </tr>
        <!-- </table> -->
        <!--  </fieldset> -->
        <!-- <fieldset class="mini-fieldset">
                <legend>新增借贷信息</legend>
                <table id="borrowing_form" class="mini-table" width="100%"> -->
        <tr>
            <td>
                <input class="mini-combobox" id="debitCreditFlag" name="debitCreditFlag" labelField="true" label="借贷标识："
                       data="CommonUtil.serverData.dictionary.DebitCreditConfigFlag"/>
            </td>
            <td>
                <input class="mini-buttonedit" id="subjCode" name="subjCode" onbuttonclick="searchSubCode"
                       labelField="true" label="科目号：" vtype="maxLength:32" allowInput="false"/>
            </td>
            <td>
                <input class="mini-textbox" id="subjName" name="subjName" labelField="true" label="科目名称："
                       enabled="false" vtype="maxLength:32" allowInput="false"/>
            </td>
        </tr>
        <tr>
            <td>
                <input class="mini-combobox" id="expression" name="expression" labelField="true" label="金额代码："
                       required="true" onclick="ititExpression" allowInput="false" vtype="maxLength:100"/>
            </td>

        </tr>
        <tr>
            <td colspan="3" style="text-align:center;">
                <a id="add_btn" class="mini-button" style="display: none" onclick="add()">新增</a>
                <a id="delete_btn" class="mini-button" style="display: none" onclick="del()">删除</a>
                <a id="save_btn" class="mini-button" style="display: none" onclick="save()">保存</a>
                <a id="cancel_btn" class="mini-button" style="display: none" onclick="close()">取消</a>
            </td>
        </tr>
    </table>
    <!--  </fieldset> -->
</div>


<div id="entrySetting" class="mini-fit">
    <div id="grid1" class="mini-datagrid  borderAll" style="width:100%;height:100%;" allowResize="true"
         allowRowSelect="true" allowAlternating="true"
         showPager="false" pageSize="100" multiSelect="true" allowCellSelect="true" allowSortColumn="false">
        <div property="columns">
            <div type="indexcolumn" headerAlign="center" align="center" width="50px">序号</div>
            <!--  <div type="checkboxcolumn" headerAlign="center"  width="40"></div> -->
            <div field="debitCreditFlag" headerAlign="center" align="center" width="100" align="center"
                 renderer="CommonUtil.dictRenderer" data-options="{dict:'DebitCreditConfigFlag'}">借贷方向
            </div>
            <div field="subjCode" headerAlign="center" width="100" align="center" vtype="maxLength:32">科目代码
            </div>
            <div field="subjName" headerAlign="center" align="left" allowSort="true" width="100" align="center"
                 vtype="maxLength:32">科目名称
            </div>
            <div field="expression" headerAlign="center" align="left" width="100" vtype="maxLength:32"
                 renderer="renderer">金额代码
            </div>
        </div>
    </div>
</div>

<script>
    mini.parse();
    var url = window.location.search;
    var action = CommonUtil.getParam(url, "action");
    var param = top["entrySettingManageGrid"].getData(action);


    var grid = mini.get("grid1");
    var form = new mini.Form("#accout_form");
    var sub = mini.get("subjCode");
    var eventId = mini.get("eventId");


    $(document).ready(function () {
        //修改时初始化数据
        /* if (GetQueryString("action") == 'edit'|| GetQueryString("action") == 'detail' ) {
            if (GetQueryString("action") == 'detail') {
                //var form2=new mini.Form("borrowing_form");
               //form2.setEnabled(false);
                form.setEnabled(false);
                mini.get("add_btn").setVisible(false);
                mini.get("delete_btn").setVisible(false);
                mini.get("save_btn").setVisible(false);
            }
            initForm();
                } */
        if (action == "edit") {
            form.setData(param);
            initNull();
            initForm();
        } else if (action == "detail") {
            $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>分录凭证管理</a>>> 详情");
            form.setData(param);
            form.setEnabled(false);
            mini.get("save_btn").setVisible(false);
            mini.get("add_btn").setVisible(false);
            mini.get("delete_btn").setVisible(false);
            initForm();
            initNull();
        } else if (action = "add") {
            $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>分录凭证管理</a>>> 新增");
        }

        //会计事件
        initEventId();
        //金额代码
        ititExpression();
    });

    //在修改和详情时，设置借贷标识，金额代码，科目代码为空
    function initNull() {
        mini.get("debitCreditFlag").setValue("");
        mini.get("subjCode").setValue("");
        mini.get("expression").setValue("");
        mini.get("subjName").setValue("");
    }


    //关闭页面
    function close() {
        top['win'].closeMenuTab();
    }

    function GetQueryString(name) {
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
        var r = window.location.search.substr(1).match(reg);
        if (r != null)
            return unescape(r[2]);
        return null;
    }

    //金额代码
    function ititExpression() {
        var data = EntrySettingEdit.getList("amountList", "amountId", "amountName");
        var param = mini.encode(data);
        var expression = mini.get("expression");
        expression.setData(param);

    }

    /**
     *获取字典项列表，用于下拉框组件
     * @param  {dictId}  字典ID
     * @return {String}
     */
    var dataArray = new Array();
    var EntrySettingEdit = {};
    EntrySettingEdit.getList = function (dictId, id, text) {
        $.each(CommonUtil.serverData.accounting[dictId], function (i, n) {
            dataArray.push({"id": n[id], "value": n[id], "text": n[text]});
        });
        return dataArray;
    }
    //在表格里面的金额代码
    var amoutName = dataArray;

    function renderer(e) {
        for (var i = 0, l = amoutName.length; i < l; i++) {
            var a = amoutName[i];
            if (a.id == e.value) return a.text;

        }
        return "";
    }

    //会计事件
    function initEventId() {
        var param = mini.encode({});//  序列化json
        CommonUtil.ajax({
            url: "/TbkEntrySceneTreeController/searchPageTtAccountingEvent",
            data: param,
            callback: function (data) {
                eventId.setData(data.obj.rows);
            }
        })
    }

    //var  settingGroupId=param.settingGroupId;
    function initForm() {
        CommonUtil.ajax({
            url: "/TbkEntrySceneTreeController/selectEntrySettingVo",
            data: mini.encode({"settingGroupId": param.settingGroupId}),
            callback: function (data) {
                //form.setData(data);
                if (data != null) {
                    grid.setData(data.constact_list);

                }
            }
        })
    }

    //保存
    function save() {
        form.validate();
        if (form.isValid == false) {
            mini.alert("信息填写有误，请重新填写", "系统提示");
            return;
        }
        var messageid = mini.loading("保存中, 请稍等 ...", "Loading");
        if (grid.data.length <= 0) {
            mini.alert("请先新增一条数据到列表中", "操作提示");
            mini.hideMessageBox(messageid);
            return;
        }
        var data = grid.getData(true, false);
        var param = form.getData();
        param['constact_list'] = data;
        var params = mini.encode(param);
        var saveUrl = "/TbkEntrySceneTreeController/saveEntrySetting";
        CommonUtil.ajax({
            url: saveUrl,
            data: params,
            callback: function (data) {
                if (data.code = "error.common.0000") {
                    mini.alert("保存成功", "温馨提示", function () {
                        setTimeout(function () {
                            top["win"].closeMenuTab()
                        }, 10);
                    });
                } else {
                    mini.alert("保存失败", "系统提示");
                }

            }
        });

    }

    //科目代码
    function searchSubCode(e) {
        // //debugger
        var btnEdit = this;
        var subjName = mini.get("subjName");
        mini.open({
            url: CommonUtil.baseWebPath() + "/mini_cap/mini_base/MiniSubjAndConfig.jsp",
            title: "参数选择",
            width: 700,
            height: 500,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data); //必须
                    if (data) {
                        btnEdit.setValue(data.subjCode);
                        btnEdit.setText(data.subjCode);
                        subjName.setValue(data.subjName);
                        subjName.setText(data.subjName);
                    }
                }
            }
        });
    }

    //新增一行
    function add() {
        //var form2 =new mini.Form("borrowing_form");
        form.validate();
        if (form.isValid() == false) {
            mini.alert("请检查输入信息", "提示!");
            return;
        }
        ;//表单验证
        var record = form.getData();
        var row = {
            "debitCreditFlag": record.debitCreditFlag,
            "subjCode": record.subjCode,
            "subjName": record.subjName,
            "expression": record.expression
        };
        grid.addRow(row);
    }

    //删除一行
    function del() {
        var row = grid.getSelected();
        if (row) {
            grid.removeRow(row, true);
        } else {
            mini.alert("请选择要删除的列!", "消息提示");
        }
    }

</script>

</body>
</html>