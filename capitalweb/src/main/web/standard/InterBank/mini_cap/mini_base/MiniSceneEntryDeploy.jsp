<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../../global.jsp"%> 
<script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
<div style="margin-left: 20px;">
    <table>
        <tr>
            <td>
                <a class="mini-button" style="display: none"  id="save_btn"   onClick="save()">保存</a>
            </td>
        </tr>
    </table>
</div>

<div class="mini-fit">
    <div class="mini-panel" title="场景管理" showCloseButton="false" style="width: 30%;float: left; height: 100%;" showToolbar="true"
        showFooter="true">
        <div id="Scene" class="mini-datagrid borderAll" style="width: 100%;height: 100%; " allowResize="true" onrowclick="ShowInfo"
            idField="id" multiSelect="false" sortMode="client" allowAlternating="true">
            <div property="columns">
                <div field="sceneId"  headerAlign="center"  allowSort="true" width="100">场景Id</div>
                <div field="sceneDescr"  headerAlign="center"  allowSort="true" width="120">场景描述</div>
                <div field="containEntry"  headerAlign="center"  align="center" renderer="onYesOrNoRenderer" width="50">是否含分录</div>
            </div>
        </div>
    </div>

    <div class="mini-panel" title="分录管理"  showCloseButton="false" style="width: 70%;float: left; height: 100%;" 
        showFooter="flase">
        <div id="entrySetting" idField="settingGroupId" sortMode="client" class="mini-datagrid borderAll" style="width: 100%;height: 100%; "  multiSelect="true"  showPager="false" allowAlternating="true">
            <div property="columns">
                <div field="settingGroupId"  name="ck" type="checkcolumn" align="center" ></div>
                <div field="settingGroupId" name="settingGroupId" headerAlign="center" allowSort="false" align="center" width="40">套账序号</div>
                <div field="settingGroupId" displayField="settingDesc"  name="settingDesc"  headerAlign="center"  allowSort="true" >套账分录描述</div>
                <div field="settingGroupId" displayField="eventName"   name="eventName" headerAlign="center" width="40">会计事件</div>
                <div field="debitCreditFlag" headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'DebitCreditFlag'}" align="center" width="40">借贷方向</div>
                <div field="subjCode"  headerAlign="center" width="80">科目号</div>
                <div field="subjName"  headerAlign="center"  width="120">科目名称</div>
                <div field="amountName" headerAlign="center" width="50">金额代码</div>
            </div>
        </div>
    </div>
</div>

<script>
    mini.parse();
    var gridEntrySetting = mini.get("entrySetting");
    var grid =mini.get("Scene");
    grid.on("beforeload", function (e) {
            e.cancel = true;
            var pageIndex = e.data.pageIndex;
            var pageSize = e.data.pageSize;
            searchScene(pageSize, pageIndex);
        });
    $(document).ready(function(){
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
            //初始化会计配置
            searchEntrySetting();
            //初始化场景
            searchScene(10, 0);
        });
    })
    //保存场景设置
    function save(){
        var param={};
        var str = "";
        var subjCode = "";
        var record=grid.getSelected();
        if(record){
            param.sceneId=record.sceneId;
            var entrySettingRecord=gridEntrySetting.getSelecteds();
            if(entrySettingRecord){
                $.each(entrySettingRecord, function(i, n) {
                    str += n.settingGroupId + ",";
                });
                str=str.substr(0,str.length-1);
                var settingGroupIdValue=unique5(str.split(","));
                var reg = /,$/gi;
                settingGroupIdValue = settingGroupIdValue.replace(reg, "");
                param.settingGroupId = settingGroupIdValue;
                CommonUtil.ajax({
                    url: '/TbkEntrySceneConfigController/configEntryScene',
                    data: mini.encode(param),
                    callback: function (data) {
                        if(data.code=='error.common.0000'){
                            mini.alert("操作成功","温馨提示",function(){
                                searchEntrySetting();
                                searchScene(10,0);
                            });
                        }else{
                            mini.alert("操作失败");
                        }
                    }
                });
            }else{
                mini.alert("请选择分录管理数据");
            }
        }else{
            mini.alert("请选择场景管理数据");
        }
    }
        //显示信息
    function ShowInfo(){
    var record =grid.getSelected();
    gridEntrySetting.clearSelect(true);
    var sceneId=record.sceneId;
    CommonUtil.ajax( {
        data:mini.encode({"sceneId":sceneId}),
        url:"/TbkEntrySceneConfigController/searchEntrySceneConfig",
        callback : function(data) {
            var list =new Array();
            if(data.obj){
                $.each(data.obj.rows, function(i, n) {
                    list.push(n.settingGroupId);
                });
                $.each(list,function(i,n){
                    var rows = gridEntrySetting.findRows(function (row) {
                        if (row.settingGroupId == n) return true;
                    })
                    gridEntrySetting.select(rows[0]);
                });
            }
        }
    });
    }   
    //获取场景信息
    function searchScene(pageSize, pageIndex){
        var data={};
        data.pageNumber = pageIndex + 1;
        data.pageSize = pageSize;
        var param = mini.encode(data); //序列化成JSON
        CommonUtil.ajax({
            url: "/TbkEntrySceneController/searchPageTbkEntrySceneInfo",
            data: param,
            callback: function (data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }
    //获取分录信息
    function searchEntrySetting(){
        CommonUtil.ajax({
            url: "/TbkEntrySceneTreeController/searchBkEntrySettingVoPage2",
            data: mini.encode({}),
            callback: function (data) {
                
                gridEntrySetting.setData(data.obj.rows);
                //合并单元格
                gridEntrySetting.mergeColumns(['settingGroupId','settingDesc','eventName','ck']);
            }
        });
    }    
        //启用状态字典
    var YesOrNo = [{ "id": "1", "text": "是" }, { "id": "0", "text": "否" }];
    //渲染启用状态
    function onYesOrNoRenderer(e) {
    for (var i = 0, l = YesOrNo.length; i < l; i++) {
        var g = YesOrNo[i];
        if (g.id == e.value) return g.text;
    }
    return "";
    }
//去重
function unique5(array) {
    var r = "";
    for (var i = 0, l = array.length; i < l; i++) {
        for (var j = i + 1; j < l; j++)
            if (array[i] === array[j]) j = ++i;
        r += array[i] + ","
    }
    return r;
}
    </script>
    
