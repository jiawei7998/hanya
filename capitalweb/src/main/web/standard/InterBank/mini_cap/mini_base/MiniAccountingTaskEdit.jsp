<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="../../../global.jsp"%>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<span id="labell"><a href="javascript:CommonUtil.activeTab();">账套管理</a> >> 新增</span>
    <table  id="accout_form" fit="true" class="mini-table" width="100%" cols="4">
        <tr>
            <td>
                 <input id="taskId" name="taskId" requiredErrorText="该输入项为必输项" class="mini-textbox" required="true"  vtype="rangeLength:1,10"
                 labelField="true"  label="套账Id"  emptyText="请输入套账Id"  />
            </td>
            <td>
                <input id="taskType" name="taskType"  class="mini-combobox" required="true"
                 labelField="true"  label="账套类型:" emptyText="请选择套账类型" data="CommonUtil.serverData.dictionary.AcTaskType"   />
            </td>
            <td>
                    <input id="taskName" name="taskName" class="mini-textbox" emptyText="请输入套账名称" labelField="true" label="账套名称:" required="true"   requiredErrorText="该输入项为必输项" vtype="rangeLength:1,25"  />
                </td>
        </tr>
        <tr>
            <td>
                <input id="taskCurrentDate" name="taskCurrentDate" class="mini-datepicker" emptyText="请选择套账日期" required="true"   labelField="true" valueType="string"
                    vtype="date:yyyy-MM-dd" label="当前账务日期:" vtype="rangeLength:1,40" />
            </td>
            <td>
                <input id="bkMain" name="bkMain" class="mini-combobox" textField="pValueTxt" valueField="pValue" labelField="true" label="交易主体" required="true"
                    emptyText="请选择交易主体" showCheckBox="true" multiSelect="true">
                </input>
            </td>
            <td>
                <input id="instId" name="instId" valueField="id" showFolderCheckBox="true" labelField="true" label="所属机构" showCheckBox="true"
                    showTreeLines="true" showTreeIcon="true" textField="instName" parentField="parentId" class="mini-treeselect" resultAsTree="false" required="true"
                    valueFromSelect="true" multiSelect="true" emptyText="请选择机构" />
            </td>
        </tr>
        <tr>
            <td>
                <input id="bkkpgOrgId" name="bkkpgOrgId" class="mini-textbox" labelField="true" label="记账机构:" emptyText="请输入记账机构" required="true"    vtype="rangeLength:1,16"  />
            </td>
            <td>
                <input id="defaultTask" name="defaultTask" class="mini-combobox" labelField="true" required="true"   label="是否启用" emptyText="请选择是否启用" data="CommonUtil.serverData.dictionary.YesNo"/>
            </td>
            <td>

            </td>
        </tr>
        <tr>
            <td colspan="3" style="text-align:center;">
                <a  id="save_btn" class="mini-button" style="display: none"    onclick="save()">保存</a>
                <a  id="cancel_btn" class="mini-button" style="display: none"    onclick="cancel()">取消</a>
            </td>
            <td>

            </td>
            <td>

            </td>
        </tr>
    </table>
<script>
mini.parse();
var instId = mini.get("instId");
var bkMain=mini.get("bkMain");
var instIdValue="";
$(document).ready(function(){
    institution();
    searchBkMain();
    if(GetQueryString("action")=='edit'){
        $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>账套管理</a> >> 修改");
        init();
    }else  if(GetQueryString("action") == "detail"){
        $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>账套管理</a> >> 详情");
        init();
        var form =new mini.Form("accout_form");
        form.setEnabled(false);
        mini.get("save_btn").setVisible(false);
    }
})
//取消
function cancel(){
    top['win'].closeMenuTab();
}
function GetQueryString(name) {
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
	var r = window.location.search.substr(1).match(reg);
	if (r != null)
		return unescape(r[2]);
	return null;
}
//初始化数据
function init(){
    var form =new mini.Form("accout_form");
    var data=top["account"].getData();
    instIdValue=data.instId;
    form.setData(data);
    mini.get("taskId").setEnabled(false);
    mini.get("taskType").setEnabled(false);
    mini.get("taskCurrentDate").setEnabled(false);

}
//加载机构
function institution() {
        var param = mini.encode({ branchId: $("#branchId").val() }); //序列化成JSON
        CommonUtil.ajax({
            url: "/InstitutionController/searchInstitutionByBranchId",
            data: param,
            callback: function (data) {
                instId.setData(data.obj);
                if(GetQueryString("action")=='edit'){
                instId.setValue(instIdValue);
                }else if(GetQueryString("action") == "detail"){
                    instId.setValue(instIdValue);
                }
            }
        });
    }
//加载交易主体
function  searchBkMain(){
    CommonUtil.ajax({
            url: "/ParamsMapController/searchPvalueTxtList",
            data: mini.encode({paramId:"prdNo"}),
            callback: function (data) {
                bkMain.setData(data.obj);
            }
        });
}
//保存
function save(){
    var form = new mini.Form("accout_form");
    form.validate();
    if (form.isValid() == false) return;//表单验证
    var record = form.getData();//获取表单数据
    
    record.bkMain_text=bkMain.getText();
    record.instId_text=instId.getText();
    var url="";
    if(GetQueryString("action")=='edit'){
      url="/TtAccountingTaskController/editTacTask";
    }else{
        url="/TtAccountingTaskController/addTacTask";
    }
    mini.confirm("您确定要保存吗","温馨提示",function(r){
        if(r=='ok'){
            CommonUtil.ajax({
            url: url,
            data: mini.encode(record),
            callback: function (data) {
                if (data.code == 'error.common.0000') {
                    mini.alert("操作成功","系统提示",function(){setTimeout(function(){
                        top["win"].closeMenuTab()
                    },10);
                        
                    });
                } else {
                    mini.alert("操作失败","系统提示");
                }
            }
        });


        }
    })
}   
</script>