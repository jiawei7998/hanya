<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="../../../global.jsp"%>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset title">
	<legend>科目信息查询</legend>
	<div id="accout_form">
			<input id="subjCode" name="subjCode" class="mini-textbox" vtype="rangeLength:1,100" labelField="true" label="科目号:" emptyText='请输入科目号' style="width:300px;"
			/>
			<input id="subjName" name="subjName" class="mini-textbox" vtype="rangeLength:1,40" labelField="true" label="科目名称:" emptyText='请输入科目名称' style="width:300px;"
			/>
		<span style="float:right;margin-right: 150px">
			<a class="mini-button" style="display: none"  id="search_btn"  onclick="search(10,0)">查询</a>
			<a class="mini-button" style="display: none"  id="clear_btn"  onclick="clear()">清空</a>
		</span>
	</div>
</fieldset>
<div id="entrySettingManageGrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="settingGroupId"
 allowResize="true" pageSize="10" multiSelect="true" onRowdblclick="custDetail">
	<div property="columns">
		<div type="indexcolumn">序号</div>
		<div field="subjCode" width="150" align="center" headerAlign="center" allowSort="true">科目号</div>
		<div field="subjName" width="150" align="left" headerAlign="center" allowSort="true">科目名称</div>
	</div>
</div>
                
<script>
mini.parse();
var grid = mini.get("entrySettingManageGrid");
$(document).ready(function () {
	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex;
		var pageSize = e.data.pageSize;
		search(pageSize, pageIndex);
	});
	search(10, 0);
})

function search(pageSize, pageIndex) {
	var form = new mini.Form("accout_form");
	form.validate();
	if (form.isValid() == false) {
		return;
	}
	
	var data = form.getData();//获取表单多个控件的数据  
	data.pageNumber = pageIndex + 1;
	data.pageSize = pageSize;
	var params = mini.encode(data); //序列化成JSON
	CommonUtil.ajax({
		url: "/TbkEntrySettingConfigController/searchSubjAndConfig",
		data: params,
		callback: function (data) {
			var grid = mini.get("entrySettingManageGrid");
			grid.setTotalCount(data.obj.total);
			grid.setPageIndex(pageIndex);
			grid.setPageSize(pageSize);
			grid.setData(data.obj.rows);
		}
	});
}

function GetData() {
	var grid = mini.get("entrySettingManageGrid");
	var row = grid.getSelected();
	return row;
}
function custDetail(e) {
	onOk();
}
function CloseWindow(action) {
	if (window.CloseOwnerWindow)
		return window.CloseOwnerWindow(action);
	else
		window.close();
}
function onOk() {
CloseWindow("ok");
}




function clear() {
	var form = new mini.Form("#accout_form");
	form.clear();
}

</script>
</body>
