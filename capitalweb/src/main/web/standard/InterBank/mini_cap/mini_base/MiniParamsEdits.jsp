<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="../../../global.jsp"%>
<html>
<head>
<title></title>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>
<body >
       <!--  <fieldset class="mini-fieldset"> -->
				<!-- <legend>参数维度信息</legend> -->
				<span id="labell"><a href="javascript:CommonUtil.activeTab();">维度字典管理</a> >> 修改</span>
				<table  id="accout_form" class="mini-table" width="100%" >
					<tr>
						<td>
							 <input id="paramId" name="paramId" requiredErrorText="该输入项为必输项" class="mini-textbox" required="true"  vtype="rangeLength:1,50"
							 labelField="true"  label="维度编号:" style="width:300px;"   />
						</td>
						<td>
							<input id="paramName" name="paramName" requiredErrorText="该输入项为必输项" class="mini-textbox" required="true"  vtype="rangeLength:1,20"
							 labelField="true"  label="维度名称:" style="width:300px;"   />
						</td>
						<td>
								<input id="paramSource" name="paramSource" class="mini-textbox" labelField="true" label="维度字段:" required="true"  style="width:300px" requiredErrorText="该输入项为必输项" vtype="rangeLength:1,25" emptyText="" />
						</td>
					</tr>
					<tr>
						
						<td>
							<input id="lastUpdate" name="lastUpdate" class="mini-textbox mini-validatebox" labelField="true"  enabled="false" label="最后更新时间:" style="width:300px"  vtype="rangeLength:1,20"
							vtype="rangeLength:1,40" emptyText="自动生成"/>
						</td>
						<td>
								<input id="remark" name="remark" class="mini-textbox mini-validatebox" labelField="true" label="维度说明:"  style="width:300px" requiredErrorText="该输入项为必输项" vtype="rangeLength:1,25"  />
							</td>
                    </tr>
                    <tr>
                            
					
					<td colspan="3" style="text-align:center;">
							<a id="save_btn" class="mini-button" style="display: none"    onclick="save()">保存</a>
							<a id="cancel_btn" class="mini-button" style="display: none"    onclick="close()">取消</a>
						</td>
					</tr>
				</table>
			<!-- </fieldset> -->
			<!-- <div  style="float: right; margin: 10px">
				<a class="mini-button" style="display: none"  id="save_btn"   onclick="save()">保存</a>
            </div> -->
            
            <script>
				$(document).ready(function(){
					initform();
        });
            mini.parse();
				var url = window.location.search;
				var action = CommonUtil.getParam(url, "action");
				var param = top["paramManageGrid"].getData(action);
				var form = new mini.Form("#accout_form");
				
				function initform() {
					if (action == "edit") {
						form.setData(param);
						mini.get("paramId").setEnabled(false);
					} else if (action == "detail") {
						$("#labell").html("<a href='javascript:CommonUtil.activeTab();'>维度字典管理</a> >> 详情");
						form.setData(param);
						form.setEnabled(false);
						mini.get("save_btn").setVisible(false);
					} else {
						$("#labell").html("<a href='javascript:CommonUtil.activeTab();'>维度字典管理</a> >> 新增");
					}
				}
				//关闭页面
				function close(){
					top['win'].closeMenuTab();
				}
					//保存
		function save(){
            var form = new mini.Form("accout_form");
            form.validate();
	        if (form.isValid() == false){//表单验证
                mini.alert("请输入有效数据","系统提示");
                return;
            } 
	        var data = form.getData();
            var param = mini.encode(data); //序列化成JSON
            ////console.log("要发送的参数："+param);
            var saveUrl = $.inArray(action, ["add"]) > -1 ? "/ParamsDicController/createParamsDic": "/ParamsDicController/updateParamsDic";
            CommonUtil.ajax({
				url:saveUrl,
                data:param,
            	callback:function(data){
                    ////console.log("返回的数据："+data.code);
					if('error.common.0000' == data.code){
				        mini.alert("保存成功!","系统提示",function(value){
                            
                            //关闭并刷新
							setTimeout(function(){top["win"].closeMenuTab()},10);
                        });
				
                    }else{
                        mini.alert("保存失败！","系统提示");
                    }	
                }
			});
        }
					
            
            </script>

</body>
</html>