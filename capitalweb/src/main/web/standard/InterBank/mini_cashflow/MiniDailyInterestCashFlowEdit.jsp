<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
<html>
<body style="width:100%;height:100%;background:white">
	<fieldset class="mini-fieldset">
		<legend>计提查询</legend>
		<div id="TradeManage">
			<div id="search_form" class="mini-form" width="80%">
				<input id="startDate" name="startDate" class="mini-datepicker"  labelField="true" labelStyle="text-align:right;"  label="开始日期："
					emptyText="请输入开始日期"/>
				<input id="endDate" name="endDate" class="mini-datepicker" labelField="true" labelStyle="text-align:right;"  label="结束日期："
					emptyText="请输入结束日期"/>
				<span style="float:right;margin-right: 100px">
					<a  id="search_btn" class="mini-button" style="display: none"   onclick="search(10,0)">查询</a>
					<a  id="clear_btn" class="mini-button" style="display: none"   onclick='clear()'>清空</a>
				</span>
			</div>
		</div>
	</fieldset>
	<div class="mini-fit">
		<div id="grid1" class="mini-datagrid  borderAll" style="width:100%;height:100%;" 
			allowResize="true"  border="true" sortMode="client"
			pageSize="10" sizeList="[10,20]"  allowAlternating="true" multiSelect="false">
			<div property="columns">
				<div type="indexcolumn" headerAlign="center"  allowSort="false" width="40" >序号</div>
				<div field="dayCounter" headerAlign= "center" allowSort= "true" width="80" renderer="CommonUtil.dictRenderer" data-options="{dict:'DayCounter'}" align="center">计息基准</div> 
				<div field="actualRate"  headerAlign= "center" allowSort= "true" width="80" align = "right" numberFormat="p4">计提利率</div>
				<div field="refBeginDate" headerAlign= "center" allowSort= "true" width="80" dateFormat="yyyy-MM-dd" align="center">开始日期</div> 
				<div field="refEndDate" headerAlign= "center" allowSort= "true" width="80" dateFormat="yyyy-MM-dd" align="center">结束日期</div>
				<div field="principal" headerAlign= "center" allowSort= "true" width="100" numberFormat="n2" align="right">计提本金</div>
				<div field="interest" headerAlign= "center" allowSort= "true" width="80" align="right" numberFormat="#,0.00">应计利息</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		mini.parse();
		var url=window.location.search;
		var dealNo=CommonUtil.getParam(url,"dealNo");
		var version=CommonUtil.getParam(url,"version");
		var dealType=CommonUtil.getParam(url,"dealType");
		var form=new mini.Form("#search_form");
		var grid=mini.get("grid1");
		//清空
		function clear(){
			form.clear();
		}
		//查询
		function search(pageSize,pageIndex){
			form.validate();
			if(form.isValid()==false){
				mini.alert("信息填写有误","系统提示");
				return ;
			}
			var data=form.getData(true);
			data['pageSize']=pageSize;
			data['pageNumber']=pageIndex+1;
			data['dealNo']=dealNo;
			data['branchId']=branchId;
			data['version']=version;
			data['dealType']=dealType;
			var params=mini.encode(data);
			CommonUtil.ajax({
				url:"/tmCalCashFlowController/searchPageIntDailyInterest",
				data:params,
				callback : function(data) {
					grid.setTotalCount(data.obj.total);
					grid.setPageIndex(pageIndex);
					grid.setPageSize(pageSize);
					grid.setData(data.obj.rows);
				}
			});
		}

		grid.on("beforeload",function(e){
			e.cancel = true;
			var pageSize=e.data.pageSize;
			var pageIndex=e.data.pageIndex;
			search(pageSize,pageIndex);
		});

		$(document).ready(function(){
			search(grid.pageSize,0);
		});
	</script>
	</body>
</html>
