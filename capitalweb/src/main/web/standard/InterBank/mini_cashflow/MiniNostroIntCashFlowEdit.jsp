<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<html>
<body style="width:100%;height:100%;background:white">
	<fieldset><legend>存放同业活期费用</legend>
		<div id="search_form">
			<input class="mini-hidden" id="refNo" name="refNo" />
			<input class="mini-hidden" id="dealType" name="dealType" />
			<table width="100%">
				<tr>
					<td colspan="4">
						<a id="refresh_btn"  class="mini-button" style="display: none"    onclick="query">刷新</a>
					</td>
				</tr>
			</table>
		</div>
	</fieldset>
	<div class="mini-fit">
		<div id="interestCashFlowEdit_grid" class="mini-datagrid  borderAll" style="width:100%;height:100%;" 
		allowResize="true" border="true" sortMode="client" showPager="true"
		pageSize="10"  multiSelect="false" allowAlternating="true" 
		multiSelect="false">
			<div property="columns">
				<div type="indexcolumn"     headerAlign= "center" allowSort="false" width="40" >序号</div>
				<div field="dayCounter"     headerAlign= "center" width="80" renderer="CommonUtil.dictRenderer" data-options="{dict:'DayCounter'}" align="center">费用基准</div>
				<div field="executeRate"    headerAlign= "center" numberFormat="p4"  width="80" align="right" allowSort="true">费用利率</div>
				<div field="refBeginDate"   headerAlign= "center" allowSort= "true" width="120" align="center">起息日期</div>
				<div field="refEndDate"     headerAlign= "center" allowSort= "true" width="120" align="center">扣划日期</div>
				<div field="interestAmt"    headerAlign= "center" allowSort= "true" width="120" align="right" numberFormat="#,0.00">应收利息金额(元)</div>
			</div>
		</div>
	</div>

<script type="text/javascript">
	var url = window.location.search;
	var prdNo = CommonUtil.getParam(url,"prdNo");
	var operType = CommonUtil.getParam(url,"operType");// A新增 D详情 U更新 approve审批
	var dealNo = CommonUtil.getParam(url,"dealNo");

	mini.parse();

	function tradeManagerWin()
	{
		if(operType == 'A'){
			return top["tradeEditTabs_" + prdNo + "_" + operType];
		} else {
			return top["tradeEditTabs_" + prdNo + "_" + operType + "_" + dealNo];
		}
	}
	
	//父页面的参数值
	var tradeData = tradeManagerWin().param;

	var grid = mini.get("interestCashFlowEdit_grid");
	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});

	function search(pageSize,pageIndex)
	{
		var form = new mini.Form("#search_form");
		form.validate();
		if(form.isValid() == false){
			mini.alert("表单填写错误,请确认!","提示信息")
			return;
		}

		var data = form.getData();
		data['pageNumber'] = pageIndex+1;
		data['pageSize'] = pageSize;
		data['prdNo'] = prdNo;
		data['dealType'] = tradeData.dealType;
		data['branchId'] = branchId;
		data['dealNo'] = tradeData.dealNo;
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:"/TdCashflowInterestController/searchFeeByPage",
			data:params,
			showLoadingMsg:false,
			messageId:mini.loading('正在查询数据...', "请稍后"),
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
				grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
		
	}// end search

	function query(){
		search(grid.pageSize,0);
	}

	function clear(){
		var form = new mini.Form("#search_form");
		form.clear();
	}

	$(document).ready(function()
	{
		query();
	});
</script>
</body>
</html>