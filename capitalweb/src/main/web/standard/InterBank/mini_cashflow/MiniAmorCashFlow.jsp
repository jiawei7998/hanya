<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<body style="width:100%;height:100%;background:white">
	<fieldset><legend>摊销计划表</legend>
	<div id="search_form">
		<input class="mini-hidden" id="refNo" name="refNo" />
		<input class="mini-hidden" id="dealType" name="dealType" />
		<table width="100%">
			<tr>
				<td>开始日期：</td>
				<td>
					<input id="startDate" name="startDate" class="mini-datepicker" required="false"/>
				</td>
				<td>结束日期：</td>
				<td>
					<input id="endDate" name="endDate" class="mini-datepicker" required="false"/>
				</td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td colspan="4">
					<a id="search_btn"  class="mini-button" style="display: none"   onclick="query">查询</a>
					<a id="clear_btn"   class="mini-button" style="display: none"     onclick="clear">清空</a>
				</td>
			</tr>
		</table>
	</div>
	</fieldset>
	<div class="mini-fit">
		<div id="data_grid_amor" class="mini-datagrid  borderAll" style="width:100%;height:100%;" 
		allowResize="true" border="true" sortMode="client"
		pageSize="10" sizeList="[10,20]" multiSelect="false" allowAlternating="true">
			<div property="columns">
				<div field="sheOrder" 	 headerAlign= "center" allowSort= "true" width="60" align="center">期数</div>
				<div field="postdate"    headerAlign= "center" allowSort= "true" width="80" align="center">摊销日期</div> 
				<div field="dp1"         headerAlign= "center" allowSort= "true" width="120" align="right" numberFormat="#,##.000000000000">全价1(100)</div>
				<!--
				<div field="yeild"       headerAlign= "center" allowSort= "true" width="120" align="right" numberFormat="#,##.000000000000">收益率(100)</div>
				-->
				<div field="dp2"         headerAlign= "center" allowSort= "true" width="120" align="right" numberFormat="#,##.000000000000">全价2(100)</div>
				<div field="amorAmt"     headerAlign= "center" allowSort= "true" width="120" align="right" numberFormat="#,##.000000000000">摊销金额(100)</div>
				<div field="interestAmt"  headerAlign= "center" allowSort= "true" width="120" align="right" numberFormat="#,##.000000000000">应付利息(100)</div>
				<!--
				<div field="actualIntAmt" headerAlign= "center" allowSort= "true" width="120" align="right" numberFormat="#,##.000000000000">实际利息(100)</div>
				-->
				<div field="unAmorAmt"    headerAlign= "center" allowSort= "true" width="120" align="right" numberFormat="#,##.000000000000">未摊销金额(100)</div>
				<div field="settAmt"      headerAlign= "center" allowSort= "true" width="120" align="right" numberFormat="#,##.000000000000">摊余成本(100)</div>
				<div field="actAmorAmt"   headerAlign= "center" allowSort= "true" width="120" align="right" numberFormat="#,0.00">摊销金额(P)</div>
				<!--
				<div field="adjAmorAmt"   headerAlign= "center" allowSort= "true" width="120" align="right" numberFormat="#,0.00">摊销调整(P)</div>			
				-->
			</div>
		</div>
	</div>


<script type="text/javascript">
	var url = window.location.search;
	var prdNo = CommonUtil.getParam(url,"prdNo");
	var operType = CommonUtil.getParam(url,"operType");// A新增 D详情 U更新 approve审批
	var dealNo = CommonUtil.getParam(url,"dealNo");

	mini.parse();

	function tradeManagerWin()
	{
		if(operType == 'A'){
			return top["tradeEditTabs_" + prdNo + "_" + operType];
		} else {
			return top["tradeEditTabs_" + prdNo + "_" + operType + "_" + dealNo];
		}
	}
	
	//父页面的参数值
	var tradeData = tradeManagerWin().param;

	var grid = mini.get("data_grid_amor");
	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});

	function search(pageSize,pageIndex)
	{
		var form = new mini.Form("#search_form");
		form.validate();
		if(form.isValid() == false){
			mini.alert("表单填写错误,请确认!","提示信息")
			return;
		}

		var data = form.getData();
		data['pageNumber'] = pageIndex+1;
		data['pageSize'] = pageSize;
		data['prdNo'] = prdNo;
		data['dealType'] = tradeData.dealType;
		data['branchId'] = branchId;
		data['dealNo'] = tradeData.dealNo;
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:"/tmCalCashFlowController/queryAmorList",
			data:params,
			showLoadingMsg:false,
			messageId:mini.loading('正在查询数据...', "请稍后"),
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
				grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
		
	}// end search

	function query(){
		search(grid.pageSize,0);
	}

	function clear(){
		var form = new mini.Form("#search_form");
		form.clear();
	}

	$(document).ready(function()
	{
		query();
	});
</script>
</body>
</html>