<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
<html>
<body style="width:100%;height:100%;background:white">
	<fieldset><legend>还本计划表</legend>
		<div id="data_form">
			<input class="mini-hidden" id="refNo" name="refNo" />
			<input class="mini-hidden" id="dealType" name="dealType" />
			
			<table width="100%" cols='4'>
				<tr>
					<td class='red-star'>现金流类型</td>
					<td>
						<input id="cfType" name="cfType" class="mini-combobox"
							enabled="false" data="CommonUtil.serverData.dictionary.cfType" value="Principal" width="60%" />
					</td>
					<td class='red-star'>收付方向</td>
					<td>
						<input id="payDirection" name='payDirection' class="mini-combobox"
							enabled="false" data="CommonUtil.serverData.dictionary.feeSituation" value="Recevie" width="60%" />
					</td>
				</tr>
				<tr>
					<td class='red-star'>还本日期（计划）</td>
					<td>
						<input id="repaymentSdate" name="repaymentSdate" class="mini-datepicker"  required="true"  width="60%" />
					</td>
					<td class='red-star'>还本金额（计划）</td>
					<td>
						<input id="repaymentSamt" name="repaymentSamt" class="mini-spinner" required="true"
							minValue="0" maxValue="999999999999999" format="#,0.00" width="60%" changeOnMousewheel="false"/>
					</td>
				</tr>
				<tr>
					<td class='red-star'>还本日期（实际）</td>
					<td>
						<input id="repaymentTdate" name="repaymentTdate" class="mini-datepicker" width="60%" enabled="false" />
					</td>
					<td class='red-star'>还本金额（实际）</td>
					<td>
						<input id="repaymentTamt" name="repaymentTamt" class="mini-spinner" 
							minValue="0" maxValue="999999999999999" format="#,0.00" width="60%" enabled="false" />
					</td>
				</tr>
				<tr>
					<td colspan="4">
						<a id="fresh_btn"    class="mini-button" style="display: none"    onclick="loadData">刷新计划</a>
						<a id="add_btn"    class="mini-button" style="display: none"   onclick="add">新增</a>
						<!-- 
						<a id="update_btn" class="mini-button" style="display: none"   onclick="update">修改</a>-->
						<a id="delete_btn" class="mini-button" style="display: none"    onclick="del">删除</a>
					</td>
				</tr>
			</table>
		</div>
	</fieldset>
	<div class="mini-fit">
		<div id="grid1" class="mini-datagrid  borderAll" style="width:100%;height:100%;" 
			allowResize="true" onrowdblclick="onRowDblClick" border="true" sortMode="client" showPager="false"
			pageSize="100"  multiSelect="false" 
			allowCellEdit="true" allowCellSelect="true" multiSelect="false" editNextOnEnterKey="true"  editNextRowCell="true">

			<div property="columns">
				<div type="indexcolumn"     headerAlign="center"  allowSort="false" width="40" >序号</div>
				
				<div field="cfType" name="cfType" headerAlign= "center" allowSort= "true" width="120" 
					renderer="CommonUtil.dictRenderer" data-options="{dict:'cfType'}" align="center">现金流类型
				</div> 
				
				<div field="payDirection" name="payDirection" headerAlign= "center" allowSort= "true" width="120" align="center"
					renderer="CommonUtil.dictRenderer" data-options="{dict:'feeSituation'}" align="center">收付方向
				</div> 
				
				<div field="repaymentSdate" headerAlign= "center" allowSort= "false" width="100" dateFormat="yyyy-MM-dd" align="center">还本日期(计划)
					<input class="mini-datepicker"  required="true"  property="editor"/>
				</div> 
				
				<div field="repaymentSamt"  headerAlign= "center" allowSort= "true" width="120" align = "right" dataType="float" numberFormat="#,0.00">还本金额(计划)
					<input class="mini-spinner" required="true"  property="editor"
						minValue="0" maxValue="999999999999999" changeOnMousewheel="false"/>
				</div>             
				
				<div field="repaymentTdate" headerAlign= "center" allowSort= "false" width="100" dateFormat="yyyy-MM-dd" align="center">还本日期(实际)
					<input class="mini-datepicker" property="editor" enabled="false"/>
				</div>                          
				
				<div field="repaymentTamt"  headerAlign= "center" allowSort= "true" width="120" align = "right" dataType="float" numberFormat="#,0.00" >还本金额(实际)
					<input class="mini-spinner" property="editor"
						minValue="0" maxValue="999999999999999" changeOnMousewheel="false" enabled="false"/>
				</div>                              
			</div>
		</div>
	</div>
	<script type="text/javascript">
		var url = window.location.search;
		var prdNo = CommonUtil.getParam(url,"prdNo");
		//操作类型
		var operType = CommonUtil.getParam(url,"operType");// A新增 D详情 U更新 approve审批
		var dealNo = CommonUtil.getParam(url,"dealNo");
		
		mini.parse();
		
		function tradeManagerWin()
		{
			if(operType == 'A'){
				return top["tradeEditTabs_" + prdNo + "_" + operType];
			} else {
				return top["tradeEditTabs_" + prdNo + "_" + operType + "_" + dealNo];
			}
		}

		//获取miniUI组件
		var grid = mini.get("grid1");
		//父页面的参数值
		var tradeData = tradeManagerWin().param;
		
		if($.inArray(operType,['A','U']) == -1){//不是新增和修改 禁用按钮
			mini.get("add_btn").setEnabled(false);
			mini.get("delete_btn").setEnabled(false);
		}
		//设置隐藏域
		mini.get("refNo").setValue(tradeData.dealNo);
		mini.get("dealType").setValue(tradeData.dealType);
		
		function loadData(){
			var data = {dealNo:tradeData.dealNo,dealType:tradeData.dealType};
			var json = mini.encode(data);
			//初始化数据
			CommonUtil.ajax({
				url:'/tmCalCashFlowController/getCapitalCashFlowForApprove',
				data:json,
				callback:function(data){
					grid.setData(data.obj);
				}
			});
		}

		loadData();

		function save() {
			if(grid.data.length <= 0){
				mini.alert("请先维护还本计划!","提示");
				return;
			}else{
				var vDate1 = tradeManagerWin().getFieldValue("tadeInfo","vDate");
				var mDate1 = tradeManagerWin().getFieldValue("tadeInfo","mDate");
				var repaymentSdate = null;
				var totalAmt = 0;
				var repaymentSamt = 0;
				var tempDateArr = {};
				var flag = false;
				for(var i = 0;i < grid.data.length; i++){
					if(CommonUtil.isNull(grid.data[i].repaymentSdate))
					{
						mini.alert("请填写计划还本日期!","提示");
						return;
					}
					repaymentSdate =  CommonUtil.stampToDate(grid.data[i].repaymentSdate);
					if(repaymentSdate == mDate1){
						flag = true;
						if(parseFloat(grid.data[i].repaymentSamt) <= 0){
							mini.alert("到期日的还本计划金额必须大于0!","提示");
							return;
						}
					}

					if(repaymentSdate <= vDate1){
						mini.alert("还本日期必须大于交易起息日!", "提示");
						return false;
					}
					if(mDate1 != null && mDate1 != "" && repaymentSdate > mDate1){
						mini.alert("还本日期必须小于或等于交易到期日!", "提示");
						return false;
					}

					if(CommonUtil.isNotNull(tempDateArr[repaymentSdate]))
					{
						mini.alert("计划还本日期["+repaymentSdate+"]不能重复!","提示");
						return false;
					}else{
						tempDateArr[repaymentSdate] = 1;
					}

					repaymentSamt = parseFloat(grid.data[i].repaymentSamt);
					totalAmt += repaymentSamt;
				}// end for

				if(flag == false){
					mini.alert("到期日必须填写还本计划!", "提示");
					return false;
				}

				var tradeAmt = tradeManagerWin().getFieldValue("tadeInfo","amt");
				if(CommonUtil.isNotNull(tradeAmt)){
					if(totalAmt != parseFloat(tradeAmt)){
						mini.alert("还款计划总还款金额必须等于本金!<br/>剩余:<font size=3 color=red>"+(tradeAmt - totalAmt)+"</font>请检查后重新调整!","提示");
						return;
					}
				}
			}
			var param = {};
			param["tmCashflowCapitals"] = grid.data;
			param["dealNo"]= tradeData.dealNo;
			param["dealType"]= tradeData.dealType;
			
			var json = mini.encode(param);
			CommonUtil.ajax({
				url:"/tmCalCashFlowController/updateCapitalCashFlowsForHandle",
				data:json,
				callback:function(data){
					mini.alert("保存成功.","提示");
					loadData();
				}
			});
		}

		function add(){
			var form = new mini.Form("data_form");
			form.validate();
			if(form.isValid() == false){
				mini.alert("交易验证失败,请检查表单是否填写正确!","消息提示")
				return;
			};
			var repaymentSdate = mini.get("repaymentSdate").getFormValue();
			var vDate = tradeManagerWin().getFieldValue("tadeInfo","vDate");
			var mDate = tradeManagerWin().getFieldValue("tadeInfo","mDate");
			if(repaymentSdate < vDate || repaymentSdate > mDate){
				mini.alert("计划日期必须介于起息日和到期日之间!","消息提示")
				return;
			}
			var row = form.getData(true);
			grid.addRow(row, grid.data.length);
		}

		function del(){
			var rows = grid.getSelecteds();
			if (rows.length > 0) {
				mini.confirm("确定删除记录？", "确定?",
					function (action) {
						if (action == "ok") 
						{
							grid.removeRows(rows, true);
							mini.alert("删除成功!","消息提示");
						} 
					}
				);
				
			}else{
				mini.alert("请选择要删除的列!","消息提示");
			}
		}

		function onRowDblClick()
		{
			var form = new mini.Form("data_form");
			var row =  grid.getSelected();
			form.setData(row,true,false);
			form.setIsValid(true);

			var controls = mini.findControls(function(control){
				if(control.type == "buttonedit") return true;    //获取所有mini-textbox控件
			});
			for (var i = 0; i< controls.length; i++) 
			{
				if(CommonUtil.isNotNull(controls[i].setText) && CommonUtil.isNotNull(controls[i].getValue)){
					controls[i].setText(controls[i].getValue());
				} 
			}
		}

		function update(){
			var row =  grid.getSelected();
			if(row){
				var form = new mini.Form("data_form");
				form.validate();
				if(form.isValid() == false){
					mini.alert("交易验证失败,请检查表单是否填写正确!","消息提示")
					return
				};
				var repaymentSdate = mini.get("repaymentSdate").getFormValue();
				var vDate = tradeManagerWin().getFieldValue("tadeInfo","vDate");
				var mDate = tradeManagerWin().getFieldValue("tadeInfo","mDate");
				if(repaymentSdate < vDate || repaymentSdate > mDate){
					mini.alert("计划日期必须介于起息日和到期日之间!","消息提示")
					return;
				}

				var data = form.getData(true);
				
				grid.updateRow(row,data);

				mini.alert("修改成功!","消息提示");
			}else{
				mini.alert("请选择要修改的列!","消息提示");
			}
		}
	</script>
	</body>
</html>
