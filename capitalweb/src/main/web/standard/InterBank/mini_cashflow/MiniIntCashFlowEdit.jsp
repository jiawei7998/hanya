<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<html>
<body style="width:100%;height:100%;background:white">
	<fieldset><legend>收息计划表</legend>
	<div id="data_form">	
		<input class="mini-hidden" id="refNo" name="refNo" />
		<input class="mini-hidden" id="dealType" name="dealType" />
		<table width="100%">
			<tr>
				<td class="red-star">现金流类型</td>
				<td>
					<input id="cfType" name="cfType" class="mini-combobox"
						enabled="false" data="CommonUtil.serverData.dictionary.cfType" value="Interest" width="60%" />
				</td>
				<td class="red-star">计息基础</td>
				<td>
					<input id="dayCounter" name="dayCounter" class="mini-combobox" required="true"
						data="CommonUtil.serverData.dictionary.DayCounter" width="60%" enabled="false"/>
				</td>
				<td class="red-star">利率</td>
				<td>
					<input id="executeRate" name="executeRate"  class="mini-spinner" required="true"  changeOnMousewheel="false" enabled="false"
						format="p4" width="60%" allowlimitvalue="false" onvalidation="CommonUtil.onValidation(e,'numberRange',[0,100])"/>
				</td>
			</tr>
			<tr>
				<td class="red-star">计息起息日</td>
				<td>
					<input id="refBeginDate" name="refBeginDate" class="mini-datepicker" required="true"  width="60%" />
				</td>
				<td class="red-star">计息到期日</td>
				<td>
					<input id="refEndDate" name="refEndDate" class="mini-datepicker" required="true"  width="60%" />
				</td>
				<td class="red-star">计划收息日</td>
				<td>
					<input id="theoryPaymentDate" name="theoryPaymentDate" class="mini-datepicker" required="true"  width="60%" />
				</td>
			</tr>
			<tr>
				<td colspan="4">
					<a id="fresh_btn"    class="mini-button" style="display: none"    onclick="query">刷新计划</a>
					<a id="update_btn" class="mini-button" style="display: none"   onclick="update">修改</a>
					<a id="search_btn" class="mini-button" style="display: none"   onclick="searchDailyInt">计提信息</a>
				</td>
			</tr>
		</table>
	</div>
	</fieldset>
	<div class="mini-fit">
		<div id="grid1" class="mini-datagrid  borderAll" style="width:100%;height:100%;" 
			allowResize="true" onrowclick="onRowClick" border="true" sortMode="client"
			pageSize="10" sizeList="[10,20]" multiSelect="false" allowAlternating="true"
			multiSelect="false">
			<div property="columns">
				<div type="indexcolumn" headerAlign="center"  allowSort="false" width="40" >序号</div>
				<div field="cfType" name="cfType" headerAlign= "center" allowSort= "true" width="80" renderer="CommonUtil.dictRenderer" data-options="{dict:'cfType'}" align="center">现金流类型</div>
				<div field="dayCounter"        headerAlign= "center" allowSort= "true" width="80" renderer="CommonUtil.dictRenderer" data-options="{dict:'DayCounter'}" align="center">计息基础</div> 
				<div field="executeRate"  headerAlign= "center" allowSort= "true" width="80" align = "right" numberFormat="p4">利率</div>
				<div field="refBeginDate" headerAlign= "center" allowSort= "true" width="80" dateFormat="yyyy-MM-dd" align="center">计息起息日</div> 
				<div field="refEndDate" headerAlign= "center" allowSort= "true" width="80" dateFormat="yyyy-MM-dd" align="center">计息到期日</div>
				<div field="theoryPaymentDate" headerAlign= "center" allowSort= "true" width="80" dateFormat="yyyy-MM-dd" align="center">计划收息日</div>
				<div field="interestAmt" headerAlign= "center" allowSort= "true" width="120" align="right" numberFormat="#,0.00">应收利息金额(元)</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		var url = window.location.search;
		var prdNo = CommonUtil.getParam(url,"prdNo");
		//操作类型
		var operType = CommonUtil.getParam(url,"operType");// A新增 D详情 U更新 approve审批
		var dealNo = CommonUtil.getParam(url,"dealNo");

		mini.parse();

		function tradeManagerWin()
		{
			if(operType == 'A'){
				return top["tradeEditTabs_" + prdNo + "_" + operType];
			} else {
				return top["tradeEditTabs_" + prdNo + "_" + operType + "_" + dealNo];
			}
		}
		
		//获取miniUI组件
		var grid = mini.get("grid1");
		//父页面的参数值
		var tradeData = tradeManagerWin().param;

		if($.inArray(operType,['A','U']) == -1){//不是新增和修改 禁用按钮
			mini.get("update_btn").setEnabled(false);
		}
		//设置隐藏域
		mini.get("refNo").setValue(tradeData.dealNo);
		mini.get("dealType").setValue(tradeData.dealType);

		grid.on("beforeload", function (e) {
			e.cancel = true;
			var pageIndex = e.data.pageIndex; 
			var pageSize = e.data.pageSize;
			search(pageSize,pageIndex);
		});

		function search(pageSize,pageIndex)
		{
			var data = {};
			data['pageNumber'] = pageIndex+1;
			data['pageSize'] = pageSize;
			data['prdNo'] = prdNo;
			data['dealType'] = tradeData.dealType;
			data['branchId'] = branchId;
			data['dealNo'] = tradeData.dealNo;
			var params = mini.encode(data);
			CommonUtil.ajax({
				url:"/tmCalCashFlowController/getInterestCashFlowPageForApprove",
				data:params,
				showLoadingMsg:false,
				messageId:mini.loading('正在查询数据...', "请稍后"),
				callback : function(data) {
					grid.setTotalCount(data.obj.total);
					grid.setPageIndex(pageIndex);
					grid.setPageSize(pageSize);
					grid.setData(data.obj.rows);
				}
			});
			
		}// end search

		function query(){
			search(grid.pageSize,0);
		}

		query();

		function update() {
			if(CommonUtil.isNull(grid.getSelected())){
				mini.alert("请确认是否选择收息计划!","提示");
				return;
			}

			if(!validateDate()){
				return;
			}
			
			var param = tradeManagerWin().getTabData("tadeInfo");
			var data = new mini.Form("data_form").getData(true);
			param["tmCashflowInterest"] = data;
			param["dealNo"]= tradeData.dealNo;
			param["dealType"]= tradeData.dealType;
			
			var json = mini.encode(param);

			CommonUtil.ajax({
				url:"/tmCalCashFlowController/updateCashFlowInterestByHandle",
				data:json,
				callback:function(data){
					if(data == "SUCCESS")
					{
						mini.alert("收息计划更新成功!<br/>同步完成了:<font size=2 color=red>计提利息</font>更新!", "提示");
						query();
					}
				}
			});
		}

		function validateDate(){
		   	var vDate = mini.get("refBeginDate").getFormValue();
			var mDate = mini.get("refEndDate").getFormValue();
			if(!CommonUtil.dateCompare(mDate,vDate)){
			    mini.alert("计息起息日不能大于计息到期日!", "提示");
				return false;
			}
			var vDate1 = tradeManagerWin().getFieldValue("tadeInfo","vDate");
			var mDate1 = tradeManagerWin().getFieldValue("tadeInfo","mDate");
			if(!CommonUtil.dateCompare(vDate,vDate1)){
				mini.alert("计息起息日不能小于交易单起息日!", "提示");
				 return false;
			}
			if(mDate1 != null && mDate1 != "" && !CommonUtil.dateCompare(mDate1,mDate)){
				mini.alert("计息到期日不能大于交易单到期日!", "提示");
				return false;
			}
			return true;
		}

		function add(){
			var form = new mini.Form("data_form");
			form.validate();
			if(form.isValid() == false){
				mini.alert("交易验证失败,请检查表单是否填写正确!","消息提示")
				return
			};
			var row = form.getData(true);
			grid.addRow(row, grid.data.length);
		}

		function del(){
			var rows = grid.getSelecteds();
			if (rows.length > 0) {
				mini.confirm("确定删除记录？", "确定?",
					function (action) {
						if (action == "ok") 
						{
							grid.removeRows(rows, true);
							mini.alert("删除成功!","消息提示");
						} 
					}
				);
				
			}else{
				mini.alert("请选择要删除的列!","消息提示");
			}
		}

		function onRowClick()
		{
			var form = new mini.Form("data_form");
			var row =  grid.getSelected();
			form.setData(row,true,false);
			form.setIsValid(true);

			var controls = mini.findControls(function(control){
				if(control.type == "buttonedit") return true;    //获取所有mini-textbox控件
			});
			for (var i = 0; i< controls.length; i++) 
			{
				if(CommonUtil.isNotNull(controls[i].setText) && CommonUtil.isNotNull(controls[i].getValue)){
					controls[i].setText(controls[i].getValue());
				} 
			}
		}

		function searchDailyInt(){
			var version = "0";
			if(CommonUtil.isNotNull(tradeData.selectData) && CommonUtil.isNotNull(tradeData.selectData.version)){
				version = tradeData.selectData.version;
			}
			mini.open({
				url : CommonUtil.baseWebPath() +"/mini_cashflow/MiniDailyInterestCashFlowEdit.jsp?dealNo="+tradeData.dealNo+"&version="+version+"&dealType="+tradeData.dealType,
				title : "计提信息",
				width : 900,
				height : 600
			});
		}
	</script>
</body>
</html>