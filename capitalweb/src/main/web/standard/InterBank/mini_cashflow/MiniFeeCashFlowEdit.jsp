<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<html>
<body style="width:100%;height:100%;background:white">
	<fieldset><legend>费用计划表</legend>
	<div id="search_form">
		<input class="mini-hidden" id="refNo" name="refNo" />
		<input class="mini-hidden" id="dealType" name="dealType" />
		<table width="100%">
			<tr>
				<td>费用类型</td>
				<td>
					<input id="cftype" name="cftype" class="mini-combobox" width="80%" 
						data="CommonUtil.serverData.dictionary.feeCategory" />
				</td>
				<td>客户编号</td>
				<td>
					<input id="feeCustNo" name="feeCustNo" class="mini-buttonedit" width="80%" 
						onbuttonclick="onCustQuery" required="false" vtype="maxLength:32"  allowInput="false" />
				</td>
				<td>客户名称</td>
				<td>
					<input id="feeCustName" name="feeCustName" class="mini-textbox" width="80%" 
						required="false" vtype="maxLength:50"/>
				</td>
			</tr>
			<tr>
				<td>计划收费日开始</td>
				<td>
					<input id="theoryPaymentDate1" name="theoryPaymentDate1" class="mini-datepicker" required="false" width="80%" />
				</td>
				<td>计划收费日结束</td>
				<td>
					<input id="theoryPaymentDate2" name="theoryPaymentDate2" class="mini-datepicker" required="false" width="80%" />
				</td>
			</tr>

			<tr>
				<td colspan="4">
					<a id="refresh_btn"  class="mini-button" style="display: none"   onclick="query">查询</a>
					<a id="clear_btn"   class="mini-button" style="display: none"     onclick="clear">清空</a>
				</td>
			</tr>
		</table>
	</div>
	</fieldset>
	<div class="mini-fit">
		<div id="feeCashFlowEdit_grid" class="mini-datagrid  borderAll" style="width:100%;height:100%;" 
		allowResize="true" border="true" sortMode="client"
		pageSize="10"  multiSelect="false" allowAlternating="true">
			<div property="columns">
				<div type="indexcolumn"     headerAlign= "center" allowSort="false" width="40" >序号</div>
				<div field="feeCustNo" 	    headerAlign= "center" allowSort= "true" width="100" align="center">客户编号</div>
				<div field="feeCustName" 	headerAlign= "center" allowSort= "true" width="220">客户名称</div>
				<div field="cfType"         headerAlign= "center" width="120" renderer="CommonUtil.dictRenderer" data-options="{dict:'feeCategory'}" align="center">费用类型</div>
				<div field="payDirection"         headerAlign= "center" width="60" renderer="CommonUtil.dictRenderer" data-options="{dict:'feeSituation'}" align="center">收付方向</div>
				<div field="dayCounter"     headerAlign= "center" width="80" renderer="CommonUtil.dictRenderer" data-options="{dict:'DayCounter'}" align="center">费用基准</div>
				<div field="executeRate"    headerAlign= "center" numberFormat="p4"  width="80" align="right" allowSort="true">费用利率</div>
				<div field="refBeginDate"   headerAlign= "center" allowSort= "true" width="80" align="center">费用起息日</div> 
				<div field="refEndDate"     headerAlign= "center" allowSort= "true" width="80" align="center">费用到期日</div>
				<div field="theoryPaymentDate" headerAlign= "center" allowSort= "true" width="80" align="center">计划费用日</div>
				<div field="interestAmt"    headerAlign= "center" allowSort= "true" width="120" align="right" numberFormat="#,0.00">应收利息金额</div>
			</div>
		</div>
	</div>
<script type="text/javascript">
	var url = window.location.search;
	var prdNo = CommonUtil.getParam(url,"prdNo");
	var operType = CommonUtil.getParam(url,"operType");// A新增 D详情 U更新 approve审批
	var dealNo = CommonUtil.getParam(url,"dealNo");

	mini.parse();

	function tradeManagerWin()
	{
		if(operType == 'A'){
			return top["tradeEditTabs_" + prdNo + "_" + operType];
		} else {
			return top["tradeEditTabs_" + prdNo + "_" + operType + "_" + dealNo];
		}
	}
	
	//父页面的参数值
	var tradeData = tradeManagerWin().param;

	var grid = mini.get("feeCashFlowEdit_grid");
	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});

	function search(pageSize,pageIndex)
	{
		var form = new mini.Form("#search_form");
		form.validate();
		if(form.isValid() == false){
			mini.alert("表单填写错误,请确认!","提示信息")
			return;
		}

		var data = form.getData();
		data['pageNumber'] = pageIndex+1;
		data['pageSize'] = pageSize;
		data['prdNo'] = prdNo;
		data['branchId'] = branchId;
		data['dealType'] = tradeData.dealType;
		data['dealNo'] = tradeData.dealNo;
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:"/tmCalCashFlowController/getCashFlowFeeInterestPage",
			data:params,
			showLoadingMsg:false,
			messageId:mini.loading('正在查询数据...', "请稍后"),
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
				grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
		
	}// end search

	function onCustQuery(e) {
		var btnEdit = e.sender;
		mini.open({
			url : CommonUtil.baseWebPath() + "/mini_cp/MiniCoreCustQueryEdits.jsp",
			title : "客户选择",
			width : 750,
			height : 500,
			ondestroy : function(action) {	
				if (action == "ok") 
				{
					var iframe = this.getIFrameEl();
					var data = iframe.contentWindow.GetData();
					data = mini.clone(data); //必须
					if (data && !data.cifNo) {
						btnEdit.setValue(data.party_id);
						btnEdit.setText(data.party_id);
						btnEdit.focus();

						mini.get("feeCustName").setValue(data.party_name);
						
					}else if(data && data.cifNo){
						data.core_custno = data.cifNo;
						data.party_id = data.cifNo;
						data.party_name = data.custName;
						data.party_code = data.cifNo;

						btnEdit.setValue(data.party_id);
						btnEdit.setText(data.party_id);
						btnEdit.focus();

						mini.get("feeCustName").setValue(data.party_name);
					}
				}// end if

			}
		});
	}

	function query(){
		search(grid.pageSize,0);
	}

	function clear(){
		var form = new mini.Form("#search_form");
		form.clear();
	}

	$(document).ready(function()
	{
		query();
	});

</script>
</body>
</html>