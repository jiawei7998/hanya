<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="../../global.jsp"%>
<html>
<head>
<title>大额支付往账交易经办</title>   
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>
<body style="width:100%;height:100%;background:white">
	<fieldset id="fd1" class="mini-fieldset">
		<legend><label>查询条件</label></legend>
			<div id="search_form" style="width:100%;">
				<input id="dealNo_s" name="dealNo_s" class="mini-textbox" labelField="true" label="核实单号：" labelStyle="text-align:right;" emptyText="请输入核实单号"/>
				<input id="custNo_s" name="custNo_s" class="mini-buttonedit" onbuttonclick="onButtonEdit" allowInput="false" labelField="true" label="客户编号：" labelStyle="text-align:right;" emptyText="请输入客户编号"/>
				<input id="custName_s" name="custName_s" class="mini-textbox" labelField="true" label="客户名称：" labelStyle="text-align:right;" emptyText="请输入客户名称"/>
				<input id="product_s" name="product_s"  textField='text' valueField='value' multiSelect='true' class="mini-combobox" labelField="true" label="产品类型：" labelStyle="text-align:right;" emptyText="请选择产品类型"/>
				<input id="startVdate" name="startVdate" class="mini-datepicker" labelField="true" label="结算日期：" onValuechanged="compareDate" labelStyle="text-align:right;" emptyText="请选择结算开始日期"/>
				<input id="endVdate" name="endVdate" class="mini-datepicker" labelField="true" label="~" onValuechanged="compareDate" labelStyle="text-align:center;" emptyText="请选择结算结束日期"/>
			
				<span style="float:right;margin-right:150px">
					<a id="search_btn" class="mini-button" style="display: none"    onclick="query()">查询</a>
					<a id="clear_btn" class="mini-button" style="display: none"    onClick="clear">清空</a>
				</span>
			</div>
	</fieldset>
	 <fieldset id="fd2" class="mini-fieldset">
	 <legend><label>大额支付往账交易经办</label></legend>
		<div id="find2" class="fieldset-body">
			<table id="field_form"  class="form-table" width="100%">
				<tr>
					<td>收款人编号：</td>
					<td>
						<input id="cno" name="cno" readonly="true" class="mini-textbox" enabled="false" style="width:95%"/>
					</td>
					<td>资金编号：</td>
					<td>
						<input id="dealNo" name="dealNo" readonly="true" class="mini-textbox" enabled="false" style="width:95%"/>
					</td>
					<td>证件发行国家：</td>
					<td>
						<input id="cerIssuerCountry" name="cerIssuerCountry" style="width:95%" value = "IC-资金系统组织机构代码" readonly="true" class="mini-textbox" enabled="false"/>
					</td>
					<td>证件类型：</td>
					<td>
						<input id="cerType" name="cerType" style="width:95%" value = "CN-中华人民共和国" readonly="true" class="mini-textbox" enabled="false"/>
					</td>
				</tr>
				
				<tr>
					<td>申请人账号：</td>
					<td>
						<input id="payUserId" name="payUserId" style="width:95%" readonly="true" class="mini-textbox" enabled="false"/>
					</td>
					<td>申请人名称</td>
					<td>
						<input id="payUserName" name="payUserName" style="width:95%" readonly="true" class="mini-textbox" enabled="false"/>
					</td>
					<td>汇兑组号：</td>
					<td>
						<input id="userDealNo" name="userDealNo"  style="width:95%" readonly="true" class="mini-textbox" enabled="false"/>
					</td>
					<td>支付优先级：</td>
					<td>
						<input id="level" name="level" style="width:95%" value = "HIGH-紧急" readonly="true" class="mini-textbox" enabled="false"/>
					</td>
				</tr>
				
				<tr>
					<td>收款人账号：</td>
					<td>
						<input id="recUserId" name="recUserId" style="width:95%" class="mini-buttonedit searchbox"  onbuttonclick="recUserEdit"  maxLength="32" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" required="true" />
					</td>
					<td>收款人名称：</td>
					<td>
						<input id="recUserName" name="recUserName" style="width:95%" class="mini-textbox"  required="true"  vtype="maxLength:50"/>
					</td>
					<td>接收行行号：</td>
					<td>
						<input id="recBankId" name="recBankId"     style="width:95%" required="true"  class="mini-textbox" maxLength="14" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" />
					</td>
					<td>接收行名称：</td>
					<td>
						<input id="recBankName" name="recBankName" style="width:95%" class="mini-textbox" required="true"  vtype="maxLength:50"/>
					</td>
				</tr>
				
				<tr>
					<td>收款人开户行：</td>
					<td>
						<input id="recBankId_1" name="recBankId_1" style="width:95%" readonly="true" class="mini-textbox" enabled="false"/>
					</td>
					<td>开户行名称：</td>
					<td>
						<input id="recBankName_1" name="recBankName_1" style="width:95%" readonly="true" class="mini-textbox" enabled="false"/>
					</td>
					<td>业务类型：</td>
					<td>
						 <input id="hvpType" name="hvpType"   style="width:95%"  class="mini-combobox" data="[{text:'A200-行间划拨',id:'A200'},{text:'A100-普通汇兑',id:'A100'}]" /> 
					 	
					</td>
					<td>币种代码：</td>
					<td>
						<input id="ccy_1" name="ccy_1" readonly="true" style="width:95%" value="RMB=人民币" class="mini-textbox" enabled="false"/>
					</td>
				</tr>
				
				<tr>
					<td>金额：</td>
					<td>
						<input id="amount" name="amount" style="width:95%" format="#,0.00" class="mini-spinner" changeOnMousewheel="false" enabled="false"/>
					</td>
					<td>手续费：</td>
					<td>
						<input id="amount_1" name="amount_1" readonly="true" style="width:95%" value="0.00" class="mini-spinner" changeOnMousewheel="false" enabled="false"/>
					</td>
					<td>附言：</td>
					<td>
						<input id="remarkFy" name="remarkFy"   style="width:95%" class="mini-textbox" vtype="maxLength:50"/>
					</td>
				</tr>
			</table>
		</div>
		<div class="btn_div" style="float: right;margin: 15px">
        	<a id="send_btn" class="mini-button" style="display: none"    onClick="btn_send">交易经办</a>
        	
			</div>
	</fieldset>
	
		
	
	 <!-- <fieldset id="fd3" class="mini-fieldset"> -->
		<label>经办交易列表</label>
		<div id="SettleCnapsManages" class="mini-datagrid borderAll" style="width:100%;height:380px;" 
	      allowResize="true" pageSize="10" sortMode="client"   allowAlternating="true"  allowAlternating="true"
		  frozenStartColumn="0" frozenEndColumn="1" >
	     	<div property="columns" >
				<div type="indexcolumn" headerAlign="center" align="center" width="50px">序号</div>
	     		<div field="refNo" width="180" align="center"  headerAlign="center" allowSort="true">原业务流水</div>
	     		<div field="amount" width="120"  numberFormat="#,0.00" align="right"  headerAlign="center" allowSort="true">结算金额</div>
	     		<div field="custName" width="120" align="left"  headerAlign="center" allowSort="false">客户名称</div>
	     		<div field="vdate" width="120" align="center"  headerAlign="center" allowSort="false">结算日期</div>
	     		<div field="payrecnd" width="80" align="center"  headerAlign="center" allowSort="false" renderer="payrecnd">方向</div>
	     		<div field="ccy" width="80" align="left"  headerAlign="center" allowSort="true"  renderer="CommonUtil.dictRenderer" data-options="{dict:'Currency'}">币种</div>
	     		<div field="recBankId" width="120" align="left"  headerAlign="center" allowSort="false">收款行行号</div>
	     		<div field="recBankName" width="120" align="left"  headerAlign="center" allowSort="false">收款行名称</div>
	     		<div field="recUserId" width="120" align="left"  headerAlign="center" allowSort="false">收款人账号</div>
	     		<div field="recUserName" width="120" align="left"  headerAlign="center" allowSort="false">收款人名称</div>
	     		<div field="dealNo" width="180" align="center"  headerAlign="center" allowSort="false">流水号</div>
	     		<div field="prodType" width="120" align="left"  headerAlign="center" allowSort="false">产品类型</div>
	     	</div>
	    </div>
	<!--  </fieldset> -->

     
	<script type="text/javascript">
		mini.parse();
		top['SettleCnaps'] = window;
			//分页
		var grid = mini.get("SettleCnapsManages");
		var form = new mini.Form("#search_form");
		grid.on("beforeload", function (e) {
			e.cancel = true;
			var pageIndex = e.data.pageIndex; 
			var pageSize = e.data.pageSize;
			search(pageSize,pageIndex);
		});
		function query(){
			search(grid.pageSize,0);
		}
		//方向
		var payRecive = [{
			id: 'P',
			text: '付'
		}, {
			id: 'R',
			text: '收'
		}];
		function payrecnd(e) {
			for (var i = 0, l = payRecive.length; i < 2; i++) {
				var g = payRecive[i];
				if (g.id == e.value)
					return g.text;
			}
			return " ";
		}

		$(document).ready(function(){
			institution();
			search(10,0);
		
			grid.on("select",function(e){
				var row=e.record;
				//var form=new mini.Form("find1");  //提交表单
				//form.setData(row);
				//收款人编号
				var cno=mini.get("cno");
				cno.setValue(row.cno);
				cno.setText(row.cno); 
				//资金编号
				var dealNo=mini.get("dealNo");
				dealNo.setValue(row.dealNo);
				dealNo.setText(row.dealNo);
				//申请人账号
				var payUserId=mini.get("payUserId");
				payUserId.setValue(row.payUserId);
				payUserId.setText(row.payUserId);
				//申请人名称
				var payUserName=mini.get("payUserName");
				payUserName.setValue(row.payUserName);
				payUserName.setText(row.payUserName);
				//兑换组号
				var userDealNo=mini.get("userDealNo");
				userDealNo.setValue(row.userDealNo);
				userDealNo.setText(row.userDealNo);
				//收款人账号
				var recUserId=mini.get("recUserId");
				recUserId.setValue(row.recUserId);
				recUserId.setText(row.recUserId); 
				recUserId.setIsValid(true);
				//收款人名称
				var recUserName=mini.get("recUserName");
				recUserName.setValue(row.recUserName);
				recUserName.setText(row.recUserName); 
				recUserName.setIsValid(true);
				//接收行行号
				var recBankId=mini.get("recBankId");
				recBankId.setValue(row.recBankId);
				recBankId.setText(row.recBankId); 
				recBankId.setIsValid(true);
				//接收行名称
				var recBankName=mini.get("recBankName");
				recBankName.setValue(row.recBankName);
				recBankName.setText(row.recBankName);
				recBankName.setIsValid(true);
				//收款人开户行
				var recBankId=mini.get("recBankId_1");
				recBankId.setValue(row.recBankId);
				recBankId.setText(row.recBankId);
				//收款人名称
				var recBankName=mini.get("recBankName_1");
				recBankName.setValue(row.recBankName);
				recBankName.setText(row.recBankName);
				//业务类型
				var hvpType=mini.get("hvpType");
				hvpType.setValue(inithvpType(row.hvpType));
				hvpType.setText(inithvpType(row.hvpType));
				//金额
				var amount=mini.get("amount");
				amount.setValue(row.amount);
				amount.setText(row.amount);
				//手续费
				/* var amount_1=mini.get("amount_1");
				amount_1.setValue(row.amount_1);
				amount_1.setText(row.amount_1); */
				//附言
				var remarkFy=mini.get("remarkFy");
				remarkFy.setValue(row.remarkFy);
				remarkFy.setText(row.remarkFy);
				
				//收款人账号
				//loadrecUser(row.dealNo);
			});
		});

		//交易经办
		function btn_send(){
			var row = grid.getSelected(); 
			if(row){
				var form = new mini.Form("field_form");
				form.validate();
				if(form.isValid() == false){
					mini.alert("表单填写有误，请检查！","系统提示");
					return;
				}

				var data = form.getData();
				var param = mini.encode(data);
				param['dealNo']=row.dealNo;
				mini.confirm("确认提交该笔大额支付交易吗","操作提示",function(value){
					if (value=="ok") {
						CommonUtil.ajax({
							url:"/TdTrdSettleController/submitSettle",
							data:param,
							callback : function(data){
								if (data.code="error.common.0000") {
									mini.alert(data.desc,"系统提示",function(){
										form.clear();
										mini.get("cerIssuerCountry").setValue("IC-资金系统组织机构代码");
										mini.get("cerType").setValue("CN-中华人民共和国");
									});
									search(grid.pageSize,0);
								}else{
									mini.alert(data.desc,"系统提示");
									search(grid.pageSize,0);
								}
							}
						});
					}
				});
			}else{
				mini.alert("请先选择一条记录操作","提示")
			}
		}
		//查询时间判断
		function compareDate(){
			var sta=mini.get("startVdate");
			var end=mini.get("endVdate");
			if(sta.getValue()>end.getValue() && end.getValue("")){
				mini.alert("开始时间不能大于结束时间","系统提示");
				return end.setValue(sta.getValue());
			}
		}
		
		//加载所有产品类型
			function institution(){
			var param = mini.encode({});//  序列化json
			CommonUtil.ajax({
				url:"/TdTrdSettleController/loadCustomProduct",
				data:param,
				callback: function (data) {
					var grid=mini.get("product_s");
					grid.setData(data.obj);
				}
			});
		} 
				
			//查询
		function search(pageSize,pageIndex){
			form.validate();              //表单验证
			if(form.isValid() == false){
				mini.alert("表单填写错误,请确认!","提示信息")
				return;
			}
			var data=form.getData();      //获取表单数据
			data['pageNumber']=pageIndex+1;
			data['pageSize']=pageSize;
			var param=mini.encode(data);   //序列化json格式
			
			CommonUtil.ajax({
				url:"/TdTrdSettleController/searchSubmitSettleInstList",
				data:param,
				callback : function(data){
					var grid = mini.get("SettleCnapsManages");
					//设置分页
					grid.setTotalCount(data.obj.total);
					grid.setPageIndex(pageIndex);
					grid.setPageSize(pageSize);
					grid.setData(data.obj.rows);
				}
			});	
		}

		//清空
		function clear(){
			mini.get("dealNo_s").setValue("");
			mini.get("custNo_s").setValue("");
			// custNo_s 的值已经在buttonEdit获取的到了，所以设置setTest为空就可以了
			mini.get("custNo_s").setText("");
			mini.get("custName_s").setValue("");
			mini.get("product_s").setValue("");
			mini.get("startVdate").setValue("");
			mini.get("endVdate").setValue("");
			
		} 
		//客户编号选择
		function onButtonEdit(e) {
			var btnEdit = this;
			var custName_s=mini.get("custName_s");
			mini.open({
				url : CommonUtil.baseWebPath() +"/mini_cp/MiniCoreCustQueryEdits.jsp",
				title : "交易对手选择",
				width : 600,
				height : 600,
				ondestroy : function(action) {
					//if (action == "close") return false;
					if (action == "ok") {
						var iframe = this.getIFrameEl();
						var data = iframe.contentWindow.GetData();
						data = mini.clone(data); //必须
						if (data) {
							btnEdit.setValue(data.party_id);
							btnEdit.setText(data.party_id);
							
							custName_s.setValue(data.party_name);
							custName_s.setText(data.party_name); 
						}
					}

				}
			});
		}
		//收款人账号选择
		function recUserEdit() {
			//重新赋值给 :收款人账号、收款人名称、接收行行号、接收行名称
			var editBtn=this;
			//var recUserId=mini.get("recUserId");
			var recUserName=mini.get("recUserName");
			var recBankId=mini.get("recBankId");
			var recBankName=mini.get("recBankName");
			var grid = mini.get("SettleCnapsManages");
			var row= grid.getSelected();
				
			if(row){ 
				mini.open({
					url : CommonUtil.baseWebPath() +"/mini_settle/MiniSettleResrecUserId.jsp?custmerCode="+row.cno,
					title : "收款人账号选择",
					width : 933,
					height : 400,
					ondestroy : function(action) {
						if (action == "ok") {
							var iframe = this.getIFrameEl();
							var data = iframe.contentWindow.GetData();
							data = mini.clone(data); //必须
							if(data){
								//收款人账号赋值
								editBtn.setValue(data.bankaccid);
								editBtn.setText(data.bankaccid);
								editBtn.setIsValid(true);
								//收款人名称
								recUserName.setValue(data.bankaccname);
								recUserName.setIsValid(true);
								

								//接收行行号
								recBankId.setValue(data.openBankLargeAccno);
								recBankId.setText(data.openBankLargeAccno);
								recBankId.setIsValid(true);
								//接收行名称
								recBankName.setValue(data.openBankName);
								recBankName.setText(data.openBankName);
								recBankName.setIsValid(true);
							}
							
						}
					}
				});
			
			}else{
				mini.alert("请先选择一条记录","系统提示");
			} 
		}
		//初始化业务类型并赋值
		function inithvpType(data){
			if(data=="A100"){
				return "A100-普通汇兑";
			}
			return "A200-行间拨款";
		}
	
	</script>
</body>
</html>