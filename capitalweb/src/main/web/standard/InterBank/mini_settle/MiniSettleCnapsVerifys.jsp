<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="../../global.jsp" %>
<html>
<head>
    <title>大额支付往账交易签发</title>
    <script src="<%=basePath %>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset id="fd1" class="mini-fieldset">
    <legend><label>查询条件</label></legend>
    <div id="search_form" style="width:100%;">
        <input id="dealNo_s" name="dealNo_s" class="mini-textbox" labelField="true" label="核实单号："
               labelStyle="text-align:right;" emptyText="请输入核实单号"/>
        <input id="custNo_s" name="custNo_s" class="mini-buttonedit" onbuttonclick="onButtonEdit" allowInput="false"
               labelField="true" label="客户编号：" labelStyle="text-align:right;" emptyText="请输入客户编号"/>
        <input id="custName_s" name="custName_s" class="mini-textbox" labelField="true" label="客户名称："
               labelStyle="text-align:right;" emptyText="请输入客户名称"/>
        <input id="product_s" name="product_s" textField='text' valueField='value' multiSelect='true'
               class="mini-combobox" labelField="true" label="产品类型：" labelStyle="text-align:right;"
               emptyText="请选择产品类型"/>
        <input id="startVdate" name="startVdate" class="mini-datepicker" onValuechanged="compareDate" labelField="true"
               label="结算日期：" labelStyle="text-align:right;" emptyText="请选择结算开始日期"/>
        <input id="endVdate" name="endVdate" class="mini-datepicker" onValuechanged="compareDate" labelField="true"
               label="~" labelStyle="text-align:center;" emptyText="请选择结算结束日期"/>
        <input id="quota_flag" name="quota_flag" class="mini-combobox" labelField="true" label="寸头开关："
               labelStyle="text-align:right;" emptyText="请选择..." onValuechanged="flag_1();" value="0"
               data="[{text:'启动',id:'0'},{text:'关闭',id:'1'}]"/>

        <span style="float:right;margin-right: 100px">
                        <a id="search_btn" class="mini-button" style="display: none" onclick="query()">查询</a>
                        <a id="clear_btn" class="mini-button" style="display: none" onClick="clear">重置</a>
                        <a id="searchStatus_btn" class="mini-button" style="display: none"
                           onclick="searchStatus">查询前置状态</a>
                    </span>
    </div>
</fieldset>

<fieldset id="fd2" class="mini-fieldset">
    <legend><label>大额支付往账交易经办</label></legend>
    <div id="find2" class="fieldset-body">
        <table id="field_form" class="form-table" width="100%">
            <tr>
                <td>收款人编号：</td>
                <td>
                    <input id="cno" name="cno" style="width:95%;" readonly="true" class="mini-textbox"/>
                </td>
                <td>资金编号：</td>
                <td>
                    <input id="dealNo" name="dealNo" style="width:95%;" readonly="true" class="mini-textbox"/>
                </td>
                <td>证件发行国家：</td>
                <td>
                    <input id="cerIssuerCountry" name="cerIssuerCountry" style="width:95%;" value="IC-资金系统组织机构代码"
                           readonly="true" class="mini-textbox"/>
                </td>
                <td>证件类型：</td>
                <td>
                    <input id="cerType" name="cerType" style="width:95%;" value="CN-中华人民共和国" readonly="true"
                           class="mini-textbox"/>
                </td>
            </tr>

            <tr>
                <td>申请人账号：</td>
                <td>
                    <input id="payUserId" name="payUserId" style="width:95%;" readonly="true" class="mini-textbox"/>
                </td>
                <td>申请人名称：</td>
                <td>
                    <input id="payUserName" name="payUserName" style="width:95%;" readonly="true" class="mini-textbox"/>
                </td>
                <td>汇兑组号：</td>
                <td>
                    <input id="userDealNo" name="userDealNo" style="width:95%;" readonly="true" class="mini-textbox"/>
                </td>
                <td>支付优先级：</td>
                <td>
                    <input id="level" name="level" style="width:95%;" value="HIGH-紧急" readonly="true"
                           class="mini-textbox"/>
                </td>
            </tr>

            <tr>
                <td>收款人账号：</td>
                <td>
                    <input id="recUserId" name="recUserId" style="width:95%;" readonly="true" class="mini-textbox"/>
                </td>
                <td>收款人名称：</td>
                <td>
                    <input id="recUserName" name="recUserName" style="width:95%;" readonly="true" class="mini-textbox"/>
                </td>
                <td>接收行行号：</td>
                <td>
                    <input id="recBankId" name="recBankId" style="width:95%;" readonly="true" class="mini-textbox"/>
                </td>
                <td>接收行名称：</td>
                <td>
                    <input id="recBankName" name="recBankName" style="width:95%;" readonly="true" class="mini-textbox"/>
                </td>
            </tr>

            <tr>
                <td>收款人开户行：</td>
                <td>
                    <input id="recBankId_1" name="recBankId_1" style="width:95%;" readonly="true" class="mini-textbox"/>
                </td>
                <td>开户行名称：</td>
                <td>
                    <input id="recBankName_1" name="recBankName_1" style="width:95%;" readonly="true"
                           class="mini-textbox"/>
                </td>
                <td>业务类型：</td>
                <td>
                    <input id="hvpType" name="hvpType" style="width:95%;" readonly="true" class="mini-textbox"/>
                </td>
                <td>币种代码：</td>
                <td>
                    <input id="ccy_1" name="ccy_1" style="width:95%;" readonly="true" value="RMB=人民币"
                           class="mini-textbox"/>
                </td>
            </tr>

            <tr>
                <td>金额：</td>
                <td>
                    <input id="amount" name="amount" style="width:95%;" format="#,0.00" readonly="true"
                           class="mini-spinner" changeOnMousewheel="false"/>
                </td>
                <td>手续费：</td>
                <td>
                    <input id="amount_1" name="amount_1" style="width:95%;" readonly="true" value="0.00"
                           class="mini-spinner" changeOnMousewheel="false"/>
                </td>
                <td>附言：</td>
                <td>
                    <input id="remarkFy" name="remarkFy" style="width:95%;" class="mini-textbox"/>
                </td>
            </tr>
        </table>
    </div>
    <div class="btn_div" style="float: right;margin: 15px">
        <a id="btn_send" class="mini-button" style="display: none" onClick="btn_send">确认签发</a>
        <a id="btn_back" class="mini-button" style="display: none" onClick="btn_back">退回经办</a>

    </div>
</fieldset>
<br/>

<div id="approveType" name="approveType" labelField="true" label="处理列表选择：" class="mini-checkboxlist" repeatItems="0"
     repeatLayout="flow"
     value="0" textField="text" valueField="id" multiSelect="false"
     data="[{id:'0',text:'待处理列表'},{id:'1',text:'已处理列表'}]"
     onvaluechanged="checkBoxValuechanged">
</div>
<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:400px;" allowResize="true"
     border="true" sortMode="client" frozenStartColumn="0" frozenEndColumn="1" allowAlternating="true">
    <div property="columns">
        <div type="indexcolumn" headerAlign="center" align="center" width="50px">序号</div>
        <div field="refNo" width="180" align="center" headerAlign="center" allowSort="true">原业务流水</div>
        <div field="settFlag" width="80" align="center" headerAlign="center" allowSort="true"
             renderer="CommonUtil.dictRenderer" data-options="{dict:'rbstatus'}">清算状态
        </div>
        <div field="amount" width="120" numberFormat="#,0.00" align="right" headerAlign="center" allowSort="false">
            结算金额
        </div>
        <div field="custName" width="120" headerAlign="center" allowSort="false">客户名称</div>
        <div field="vdate" width="120" align="center" headerAlign="center" allowSort="false">结算日期</div>
        <div field="payrecnd" width="80" align="center" headerAlign="center" allowSort="false" renderer="payrecnd">方向
        </div>
        <div field="ccy" width="80" align="left" headerAlign="center" allowSort="false"
             renderer="CommonUtil.dictRenderer" data-options="{dict:'Currency'}">币种
        </div>
        <div field="recBankId" width="120" align="left" headerAlign="center" allowSort="false">收款行行号</div>
        <div field="recBankName" width="120" headerAlign="center" allowSort="false">收款行名称</div>
        <div field="recUserId" width="120" align="left" headerAlign="center" allowSort="false">收款人账号</div>
        <div field="recUserName" width="120" headerAlign="center" allowSort="false">收款人名称</div>
        <div field="dealNo" width="180" align="center" headerAlign="center" allowSort="false">流水号</div>
        <div field="prodType" width="150" align="left" headerAlign="center" allowSort="false">产品类型</div>
    </div>
</div>

<script type="text/javascript">
    mini.parse();
    //分页
    var grid = mini.get("datagrid");
    var form = new mini.Form("#search_form");
    var form2 = new mini.Form("#field_form");
    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize, pageIndex);
    });

    //初始化方法 页面加载完成后被调用

    function init(param) {

    }

    function query() {
        search(grid.pageSize, 0);
    }

    //确认签发
    function btn_send() {
        var grid = mini.get("datagrid");
        var row = grid.getSelected();
        if (row) {
            // var form=new mini.Form("find1");
            var data = form2.getData();
            var param = mini.encode(data);
            var quota_flag = param.quota_flag = '0';
            //param.quota_flag=row.quota_flag;


            mini.confirm("确认签发该笔大额支付交易吗？", "系统警告", function (value) {
                if (value == "ok") {
                    CommonUtil.ajax({
                        url: "/TdTrdSettleController/verifySettle",
                        data: {dealNo: row.dealNo, quota_flag: quota_flag},
                        callback: function (data) {
                            if (data.code == 'error.common.0000') {
                                mini.alert(data.desc, "系统提示");
                                search(10, 0);
                            } else {
                                mini.alert(data.desc, "系统提示");
                            }
                        }

                    });
                }
            });
        } else {
            mini.alert("请先选择一条记录", "系统提示");
            return;
        }

    }

    //退回经办
    function btn_back() {
        //var grid=mini.get("datagrid");
        var rows = grid.getSelected();
        if (rows) {
            mini.confirm("确认退回该笔大额支付交易吗？", "系统警告", function (value) {
                if (value == "ok") {
                    CommonUtil.ajax({
                        url: "/TdTrdSettleController/backSettle",
                        data: {dealNo: rows.dealNo},
                        callback: function (data) {
                            if (data.code == 'error.common.0000') {
                                mini.alert(data.desc, "系统提示");
                                search(10, 0);
                            } else {
                                search(10, 0);
                                mini.alert(data.desc, "系统提示");
                            }
                        }

                    });
                }
            });
        } else {
            mini.alert("请先选择一条记录", "系统提示");
            return;
        }

    }

    //查询前置状态
    function searchStatus() {
        //var grid=mini.get("datagrid");
        var row = grid.getSelected();
        if (row) {
            var messageid = mini.loading("加载中,请稍后... ", "提示信息");

            /* mini.confirm("确认签发该笔大额支付交易吗？","系统警告",function(value){
                if (value=="ok") { */
            CommonUtil.ajax({
                url: "/TdTrdSettleController/selectStatus",
                data: {dealNo: row.dealNo},
                callback: function (data) {
                    //setTimeout(function () {
                    mini.hideMessageBox(messageid);
                    //}, 3000);
                    if (data.code == 'error.common.0000') {
                        search(10, 0);
                        mini.alert(data.desc, "系统提示")
                    } else {
                        search(10, 0);
                        mini.alert(data.desc, "系统提示");
                    }
                }

            });
            /* }
            }); */
        } else {
            mini.alert("请先选择一条记录", "系统提示");
            return;
        }
    }

    //查询
    function search(pageSize, pageIndex) {
        //var form = new mini.Form("#search_form");
        form.validate();
        if (form.isValid() == false) {
            mini.alert("表单填写错误,请确认!", "提示信息")
            return;
        }
        var data = form.getData(true);
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;

        var searchType = mini.get("approveType").getValue();
        data['searchType'] = searchType;
        if (searchType == "0") {
            url = "/TdTrdSettleController/searchVerifySettleInstList";
        } else {
            url = "/TdTrdSettleController/searchVerifySettleInstList";
        }

        var params = mini.encode(data);
        CommonUtil.ajax({
            url: url,
            data: params,
            callback: function (data) {
                //var grid = mini.get("datagrid");
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });

    }// end search
    function boxChange() {
        var approveType = mini.get("approveType").getValue();
        if (approveType == "0") {
            mini.get("search_btn").setEnabled(true);
            mini.get("btn_back").setVisible(true);
            mini.get("btn_send").setVisible(true);
            mini.get("searchStatus_btn").setVisible(false);
        } else {
            mini.get("search_btn").setEnabled(true);
            mini.get("btn_back").setVisible(false);
            mini.get("btn_send").setVisible(false);
            mini.get("searchStatus_btn").setVisible(true);
        }
    }

    function checkBoxValuechanged() {
        boxChange();
        search(grid.pageSize, 0);
    }

    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize, pageIndex);
    });


    grid.on("select", function (e) {
        var row = e.record;
        //var form=new mini.Form("find1");  //提交表单
        //form.setData(row);
        //收款人编号
        var cno = mini.get("cno");
        cno.setValue(row.cno);
        cno.setText(row.cno);
        //资金编号
        var dealNo = mini.get("dealNo");
        dealNo.setValue(row.dealNo);
        dealNo.setText(row.dealNo);
        //申请人账号
        var payUserId = mini.get("payUserId");
        payUserId.setValue(row.payUserId);
        payUserId.setText(row.payUserId);
        //申请人名称
        var payUserName = mini.get("payUserName");
        payUserName.setValue(row.payUserName);
        payUserName.setText(row.payUserName);
        //兑换组号
        var userDealNo = mini.get("userDealNo");
        userDealNo.setValue(row.userDealNo);
        userDealNo.setText(row.userDealNo);
        //收款人账号
        var recUserId = mini.get("recUserId");
        recUserId.setValue(row.recUserId);
        recUserId.setText(row.recUserId);
        //收款人名称
        var recUserName = mini.get("recUserName");
        recUserName.setValue(row.recUserName);
        recUserName.setText(row.recUserName);
        //接收行行号
        var recBankId = mini.get("recBankId");
        recBankId.setValue(row.recBankId);
        recBankId.setText(row.recBankId);
        //接收行名称
        var recBankName = mini.get("recBankName");
        recBankName.setValue(row.recBankName);
        recBankName.setText(row.recBankName);
        //收款人开户行
        var recBankId = mini.get("recBankId_1");
        recBankId.setValue(row.recBankId);
        recBankId.setText(row.recBankId);
        //收款人名称
        var recBankName = mini.get("recBankName_1");
        recBankName.setValue(row.recBankName);
        recBankName.setText(row.recBankName);
        //业务类型
        var hvpType = mini.get("hvpType");
        hvpType.setValue(row.hvpType);
        hvpType.setText(row.hvpType);
        //金额
        var amount = mini.get("amount");
        amount.setValue(row.amount);
        amount.setText(row.amount);

        //附言
        var remarkFy = mini.get("remarkFy");
        remarkFy.setValue(row.remarkFy);
        remarkFy.setText(row.remarkFy);
    });

    //寸头开关提示语  有bug
    function flag_1() {
        var flag_1 = mini.get("quota_flag");
        ////console.log(flag_1);
        if (flag_1.value == '0') {
            mini.alert("开启头寸开关后系统会向头寸系统发送头寸核实报文", "系统提示");
            //return;
        } else {
            mini.alert("开启头寸开关后系统将停止向头寸系统发送头寸核实报文", "系统提示");
            // return;
        }
    }

    //查询时间判断
    function compareDate() {
        var sta = mini.get("startVdate");
        var end = mini.get("endVdate");
        // //console.log(sta);
        if (sta.getValue() > end.getValue() && end.getValue("")) {
            mini.alert("开始时间不能大于结束时间", "系统提示");
            return end.setValue(sta.getValue());


        }
    }


    //加载所有产品类型
    function institution() {
        var param = mini.encode({});//  序列化json
        CommonUtil.ajax({
            url: "/TdTrdSettleController/loadCustomProduct",
            data: param,
            callback: function (data) {
                var grid = mini.get("product_s");
                grid.setData(data.obj);
            }
        });
    }


    //清空
    function clear() {
        mini.get("dealNo_s").setValue("");
        mini.get("custNo_s").setValue("");
        // custNo_s 的值已经在buttonEdit获取的到了，所以设置setTest为空就可以了
        mini.get("custNo_s").setText("");
        mini.get("custName_s").setValue("");
        mini.get("product_s").setValue("");
        mini.get("startVdate").setValue("");
        mini.get("endVdate").setValue("");
        mini.get("searchStatus_btn").setValue("");
        mini.get("quota_flag").setValue(0);

    } //客户编号选择
    function onButtonEdit(e) {
        var btnEdit = this;
        var custName_s = mini.get("custName_s");
        mini.open({
            url: CommonUtil.baseWebPath() + "/mini_cp/MiniCoreCustQueryEdits.jsp",
            title: "交易对手选择",
            width: 600,
            height: 600,
            ondestroy: function (action) {
                //if (action == "close") return false;
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data); //必须
                    if (data) {
                        ////console.log(data.party_id);
                        btnEdit.setValue(data.party_id);
                        btnEdit.setText(data.party_id);

                        custName_s.setValue(data.party_name);
                        ////console.log(data.party_name);
                        custName_s.setText(data.party_name);

                    }
                }

            }
        });
    }

    //初始化业务类型并赋值
    function inithvpType(data) {
        if (data == "A100") {
            return "A100-普通汇兑";
        }
        return "A200-行间拨款";
    }

    $(document).ready(function () {
        search(10, 0);
        form2.setEnabled(false);
        checkBoxValuechanged();
        institution();//加载产品类型

    });

    //方向
    var payRecive = [{
        id: 'P',
        text: '付'
    }, {
        id: 'R',
        text: '收'
    }];

    function payrecnd(e) {
        for (var i = 0, l = payRecive.length; i < 2; i++) {
            var g = payRecive[i];
            if (g.id == e.value)
                return g.text;
        }
        return " ";
    }
</script>

</body>
</html>