<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp" %>
<script type="text/javascript" src="../script/md5.js"></script>
<%-- 
/**
 *授权页面
 * @author Lihb
 *
 */
 --%>
<script type="text/javascript">
var SettleAuthEdit = {};
SettleAuthEdit.dialogOptions=$.getDialogOptions();
SettleAuthEdit.settleInstData;
SettleAuthEdit.validateMsg;
SettleAuthEdit.validate = function(obj){
	SettleAuthEdit.validateMsg = "";
	if(obj.authType == "settle" || obj.authType == "earse"){
		SettleAuthEdit.settleInstData = SettleInstVerify.settleInstObj;
		if(SettleAuthEdit.settleInstData){
			if(obj.auth_user_id == SettleAuthEdit.settleInstData.settle_trader){
				SettleAuthEdit.validateMsg += "授权用户与交易员不能是同一个人!";
				return false; 
			}
			if(obj.auth_user_id == SettleAuthEdit.settleInstData.manage_user){
				SettleAuthEdit.validateMsg += "授权用户与经办用户不能是同一个人!"; 
				return false;
			}
		} 
		if(obj.authType == "settle"){
			if(obj.auth_user_id == "<%=__sessionUser.getUserId()%>"){
				SettleAuthEdit.validateMsg += "授权用户与复核员不能是同一个人!"; 
				return false;
			}
		}
		else{
			if(obj.auth_user_id == SettleAuthEdit.settleInstData.verfy_user){
				SettleAuthEdit.validateMsg += "授权用户与复核员不能是同一个人!"; 
				return false;
			}
		}
	}
	if(SettleAuthEdit.validateMsg != ""){
		return false;
	}
	else{
		return true;
	}
};
SettleAuthEdit.settleAuth = function(){
	var authType = SettleAuthEdit.dialogOptions.param['authType'];
	var authKey1 = SettleAuthEdit.dialogOptions.param['authKey1'];
	if(!CommonUtil.isValid('#SettleAuthEdit_div')){return false;}
	var params = CommonUtil.getJson('#SettleAuthEdit_div');
	params.auth_user_pwd = hex_md5(params.auth_user_pwd);
	params.authType = authType;
	params.authKey1 = authKey1;
	if(!SettleAuthEdit.validate(params)){
		$.messager.alert("提示信息",SettleAuthEdit.validateMsg);
		return;
	}
	CommonUtil.ajax( {
		url:"/SettleController/settleAuth",
		data:params,
		callback : function(data) {
			if(!CommonUtil.isNull(data)){
				if(data.isPass)
					CommonUtil.closedailog(null,true);
				else
					$.messager.alert("错误信息",data.checkMsg);
			}
			else
				CommonUtil.closedailog(null,false);
		},
		callerror:function(data){
			CommonUtil.closedailog(null,false);
		}
	});
};
/***********************************页面初始化*******************************************/	
$(document).ready(function(){
	$.parser.parse($('#SettleAuthEdit_div'));
	//注册确定方法
	$('#SettleAuthEdit_div').find('#auth_save_btn').click(function(){SettleAuthEdit.settleAuth();});
	$('#SettleAuthEdit_div').find('#cancel_save_btn').click(function(){
		CommonUtil.closedailog(false);
	});
	//注册：密码输入框回车事件
	var t = $('#SettleAuthEdit_div').find('#auth_user_pwd');
	t.textbox('textbox').bind('keydown', function(e){
		if (e.keyCode == 13){	
		}
	});
	t.textbox('textbox').bind('keyup', function(e){
		if (e.keyCode == 13){	// 当按下回车键时接受输入的值。
			SettleAuthEdit.settleAuth();
		}
	});
});
</script>

<div id="SettleAuthEdit_div" class="easyui-layout" style="position:relative">
	<div id="auth_search_panel" data-options="region:'center',border:false" title="">
		<table class="form_table" cols="2">
			<tr>
				<td style="font-size:12px;color:black;" class="red-star">用户名</td>
				<td><input id="auth_user_id" data-options="required:true,width:150" class="easyui-textbox easyui-validatebox"/></td>
			</tr>
			<tr>
				<td style="font-size:12px;color:black;" class="red-star">密码</td>
				<td><input id="auth_user_pwd" class="easyui-textbox easyui-validatebox" data-options="required:true,width:150" type="password"/></td>
			</tr>
		</table>
		<div class="btn_div">
		  	<a href="javascript:void(0)"  id="auth_save_btn" class="easyui-linkbutton" data-options="iconCls:'icon-ok'">确定</a>
		  	<a href="javascript:void(0)"  id="cancel_save_btn" class="easyui-linkbutton" data-options="iconCls:'icon-cancel'">取消</a>
	    </div> 
	</div>
</div>