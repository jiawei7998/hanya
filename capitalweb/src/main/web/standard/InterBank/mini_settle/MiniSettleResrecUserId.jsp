<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="../../global.jsp" %>
<html>
<head>
    <title></title>
</head>
<body style="width:100%;height:100%;background:white">
<!-- <fieldset id="fd1" class="mini-fieldset">
	<legend><label>查询</label></legend> -->
<div id="search_form" style="width:100%;">
    <input id="custmerCode" name="custmerCode" class="mini-hidden" labelField="true" label="客户代码："
           labelStyle="text-align:right;" emptyText="请输入客户代码"/>
    <input id="cny" name="cny" class="mini-hidden" labelField="true" label="币种：" value="CNY"
           labelStyle="text-align:right;" emptyText=""/>
    <input id="state" name="state" class="mini-hidden" labelField="true" label="审批状态：" value="6"
           labelStyle="text-align:right;" emptyText=""/>
    <!-- <span style="float:right;margin-right: 150px">
            <a id="search_btn" class="mini-button" style="display: none"   onclick="query">查询</a>
            <a id="clear_btn" class="mini-button" style="display: none"   onclick="clear">清空</a>
    </span> -->
</div>
<!-- </fieldset> -->

<fieldset id="fd1" class="mini-fieldset">
    <legend><label>列表信息</label></legend>
    <div id="datagrid1" class="mini-datagrid borderAll" sortMode="client"
         frozenStartColumn="0" frozenEndColumn="1" height="300px" onrowdblclick="onRowDblClick">
        <div property="columns">
            <div type="indexcolumn" headerAlign="center" align="center" width="50">序号</div>
            <div field="bankaccid" width="180" align="center" headerAlign="center">收款人账号</div>
            <div field="bankaccname" width="160" headerAlign="center">收款人名称</div>
            <div field="openBankLargeAccno" width="180" align="center" headerAlign="center">接收行行号</div>
            <div field="openBankName" width="160" headerAlign="center">接收行名称</div>


        </div>
    </div>
</fieldset>

<script type="text/javascript">
    mini.parse();
    var grid = mini.get("datagrid1");
    var form = new mini.Form("#search_form");

    /* var url=window.location.search;
   var action=CommonUtil.getParam(url,"action");
   var row=top['SettleCnaps'].getData(action);
   var custmerCode=row.cno;
*/
    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        ////console.log("起始页="+pageIndex);
        var pageSize = e.data.pageSize;
        ////console.log("每页大小="+pageSize);
        search(pageSize, pageIndex);
    });

    function query() {
        search(grid.pageSize, 0);
    }

    $(document).ready(function () {

        search(10, 0);

    });

    //清空
    function clear() {
        form.clear();
    }

    var recBankId = "";
    var recBankName = "";
    var dealNo = "";
    var cny = "";
    var state = "";
    var custmerCode = mini.get("custmerCode");

    //recBankId = GetQueryString("recBankId");
    //recBankName = GetQueryString("recBankName");
    custmerCode = GetQueryString("custmerCode");
    ////console.log(custmerCode);
    //dealNo = GetQueryString("dealNo");
    state = GetQueryString("state");
    cny = GetQueryString("cny");
    ////console.log(dealNo+"///////////////");


    //查询核心客户信息列表的方法
    function search(pageSize, pageIndex) {
        //var form=new mini.Form("find1");  //提交表单
        form.validate();              //表单验证
        if (form.isValid() == false) {
            mini.alert("表单填写错误,请确认!", "提示信息")
            return;
        }
        //提交数据
        var data = form.getData();//获取表单多个控件的数据
        //data.dealNo=dealNo;
        //data.custmerCode=custmerCode;
        /* data.cno=custmerCode;
        data.cny=cny;
        data.state=state; */
        data['custmerCode'] = custmerCode;
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        var param = mini.encode(data); //序列化成JSON
        ////console.log(custmerCode+"lllllllllll")
        CommonUtil.ajax({
            //url : "/TdCustAccController/searchTdCustAccPage?dealNo="+dealNo+"&custmerCode="+data.cno+"&state="+data.state+"&cny="+data.cny,
            url: "/TdCustAccController/searchTdCustAccPage",
            //data : param,
            //data : {custmerCode:data.cno, state:'6',cny:'CNY'},
            data: param,
            callback: function (data) {
                ////console.log(data.obj.rows+"-----------------");
                var grid = mini.get("datagrid1");
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }

    /* 获取所选行的属性的值 */
    function GetData() {
        mini.parse();
        var grid = mini.get("datagrid1");
        var row = grid.getSelected();
        //alert(row.party_id);
        return row;
    }

    function onRowDblClick(e) {
        onOk();
    }

    //////////////////////////////////
    function CloseWindow(action) {
        if (window.CloseOwnerWindow)
            return window.CloseOwnerWindow(action);
        else
            window.close();
    }

    function onOk() {
        CloseWindow("ok");
    }

    function GetQueryString(name) {
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
        var r = window.location.search.substr(1).match(reg);
        if (r != null)
            return unescape(r[2]);
        return null;
    }
</script>


</body>
</html>