<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<head>
  <head>
  <title>客户管理-同步</title>
  </head>
  
  <body style="background:white">
    <fieldset class="mini-fieldset title">
    	<legend>客户信息同步</legend>
	</fieldset>
	<div>
		<form class="mini-form" style="width:100%">
			<table style="width:100%">
		        <tr>
		            <td style="width:50%;"><input class="mini-textbox" style="width:70%;" labelStyle = "width:160px;" labelField="true" label="客户号" emptyText="请输入精确客户代码" /></td>
		            <td style="width:50%;"><input class="mini-textbox" style="width:70%;" labelStyle = "width:160px;" labelField="true" label="CN客户名称"  emptyText="ECIF同步反显" enabled="false"/></td>
		        </tr>
		        <tr>
		            <td ><input class="mini-textbox" style="width:70%;" labelStyle = "width:160px;" labelField="true" label="GB客户名称" emptyText="ECIF同步反显" enabled="false"/></td>
		            <td ><input class="mini-combobox" style="width:70%;" labelStyle = "width:160px;" labelField="true" label="客户类型" emptyText="ECIF同步反显" enabled="false"/></td>
		        </tr>
		        <tr>
		            <td ><input class="mini-textbox" style="width:70%;" labelStyle = "width:160px;" labelField="true" label="证件类型"  emptyText="请选择精确证件类型" /></td>
		            <td ><input class="mini-combobox" style="width:70%;" labelStyle = "width:160px;" labelField="true" label="证件号码"  emptyText="请输入精确证件号码" /></td>
		        </tr>
		        <tr>
		            <td ><input class="mini-textbox" style="width:70%;" labelStyle = "width:160px;" labelField="true" label="登记日期"  emptyText="ECIF同步反显" enabled="false"/></td>
		            <td ><input class="mini-combobox" style="width:70%;" labelStyle = "width:160px;" labelField="true" label="国别"  emptyText="ECIF同步反显" enabled="false"/></td>
		        </tr>
		        <tr>
		        	<td ><input class="mini-combobox" style="width:70%;" labelStyle = "width:160px;" labelField="true" label="行业类别"  emptyText="ECIF同步反显" enabled="false"/></td>
		        	<td ><input class="mini-textbox" style="width:70%;" labelStyle = "width:160px;" labelField="true" label="金融机构类型" emptyText="ECIF同步反显" enabled="false"></td>
		        </tr>
		    </table>
		    <input class="mini-textbox" labelField="true" label="所在街道" style="width:70%;" labelStyle = "width:160px;" emptyText="ECIF同步反显" enabled="false">
		    <input class="mini-textbox" labelField="true" label="GB所在街道" style="width:70%;" labelStyle = "width:160px;" emptyText="ECIF同步反显" enabled="false">
		    
		    <table style="width:100%">
		        <tr>
		            <td style="width:50%;"><input class="mini-textbox" style="width:70%;" labelStyle = "width:160px;" labelField="true" label="客户经理"  emptyText="ECIF同步反显" enabled="false"/></td>
		            <td style="width:50%;"><input class="mini-textbox" style="width:70%;" labelStyle = "width:160px;" labelField="true" label="客户经理比例"  emptyText="ECIF同步反显" enabled="false"/></td>
		        </tr>
		        <tr>
		            <td ><input class="mini-textbox" labelField="true" style="width:70%;" labelStyle = "width:160px;" label="客户状态"  emptyText="ECIF同步反显" enabled="false"/></td>
		            <td ><input class="mini-combobox" labelField="true" style="width:70%;" labelStyle = "width:160px;" label="同业客户标识"  emptyText="ECIF同步反显" enabled="false"/></td>
		        </tr>
		    </table>
	    </form>
    </div>
    <div class="miniui-elem-quote" style="margin-bottom:70px">
        <a class="mini-button" style="display: none" >同步客户信息</a>
        <a class="mini-button" style="display: none" >保存已同步信息</a>
        <a class="mini-button" style="display: none" >关闭界面</a>
    </div>
  </body>
</html>
