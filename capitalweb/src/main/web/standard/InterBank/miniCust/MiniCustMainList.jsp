<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
  <title>客户管理</title>
  </head>
  
<body style="width:100%;height:100%;background:white">
	<fieldset class="mini-fieldset title">
    	<legend>客户信息查询</legend>
	</fieldset>
	<div id="searchForm" style="height:90px;">
		<table >
        <tr>
		     <td style="width:310px;">
		     	<input class="mini-textbox" name="custmerCode" labelField="true" label="客户代码" style="width:100%;" emptyText="请输入精确客户代码" >
		     </td>
		     <td style="width:410px;">
		    	<input class="mini-textbox" name="cnName" labelField="true" label="客户名称" style="width:100%;" emptyText="请输入模糊客户名称" >
		     </td>
		</tr>
		<tr>
			<td colspan="4">
		     	<a class="mini-button" style="display: none"  onclick="search(10,0)">查询</a>
		     	<a class="mini-button" style="display: none"  onclick="btnCustSync()">客户同步</a>
		     	<a class="mini-button" style="display: none"  onclick="">划款路径</a>
		     	<a class="mini-button" style="display: none"  onclick="">同业信息</a>
		     	<a class="mini-button" style="display: none"  onclick="">准入信息</a>
		     	<a class="mini-button" style="display: none"  onclick="">分层信息</a>
		     	<a class="mini-button" style="display: none"  onclick="">联系人信息</a>
			</td>
		</tr>
		<tr>
			<td colspan="4">
				
			</td>
		</tr>
		</table>
	</div>
	<div class="mini-fit">
	    <div id="datagrid1" class="mini-datagrid borderAll" style="width:100%;height:100%;" allowResize="true" idField="id"  frozenStartColumn="0" frozenEndColumn="1" >
	    </div>
    </div>
    <script type="text/javascript">
    	function btnCustSync(){
		var url = CommonUtil.baseWebPath() + "/miniCust/MiniCustSync.jsp";
		var tab = {"id": "cust_sync", iconCls: "fa fa-send-o", text: "客户信息同步" ,url:url,innerText: "客户信息同步"};
		top["win"].showTab(tab);
		}
	
        mini.parse();
        var grid = mini.get("datagrid1");
        grid.set({
        columns: [
	            { field: "custmerCode", width: 70, headerAlign: "center", allowSort: true, header: "客户代码"},
	            { field: "cnName", width: 180, headerAlign: "center", allowSort: true, header: "CN客户名称"},
	            { field: "gbName", width: 180, headerAlign: "center", allowSort: true, header: "GB客户名称"},
	            { field: "custType", width: 70, headerAlign: "center", allowSort: true, header: "客户类型"},
	            { field: "tyCustId", width: 80, headerAlign: "center", allowSort: true, header: "同业客户标识"},
	            { field: "idType", width: 70, headerAlign: "center", allowSort: true, header: "证件类型"},
	            { field: "idNumber", width: 120, headerAlign: "center", allowSort: true, header: "证件号码"},
	            { field: "inputDt", width: 80, headerAlign: "center", allowSort: true, header: "登记日期"},
	            { field: "sfCptyCountry", width: 70, headerAlign: "center", allowSort: true, header: "国别"},
	            { field: "merType", width: 80, headerAlign: "center", allowSort: true, header: "行业类别"},
	            { field: "strent", width: 180, headerAlign: "center", allowSort: true, header: "所在街道"},
	            { field: "bankType", width: 80, headerAlign: "center", allowSort: true, header: "金融机构类型"},
	            { field: "manager", width: 80, headerAlign: "center", allowSort: true, header: "客户经理"},
	            { field: "managerProp", width: 80, headerAlign: "center", allowSort: true, header: "客户经理比例"}
	        ]
	    });
       
        grid.frozenColumns(0, 1);
        grid.on("beforeload", function (e) {
	      e.cancel = true;
	      var pageIndex = e.data.pageIndex; 
	      var pageSize = e.data.pageSize;
	      search(pageSize,pageIndex);
	    });
	    search(10,0);
	    function search(pageSize,pageIndex) {
	    	var param = new mini.Form("searchForm")
		    param.validate();
		    var data = param.getData();
		    data.pageNumber=pageIndex+1;
		        data.pageSize=pageSize;
		    var json = mini.encode(data);
		    url = '/TdCustEcifController/searchTdCustEcifPage';
		    CommonUtil.ajax({
		      url : url,
		      data : json,
		      callback : function(data) {
		        grid.setTotalCount(data.obj.total);
		        grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
		        grid.setData(data.obj.rows);
		      }
		    });
		 };
    </script>
</body>
</html>
