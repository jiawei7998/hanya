<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title></title>
  </head>
  
<body style="width:100%;height:100%;background:white">
		<span id="labell"><a href="javascript:CommonUtil.activeTab();">交易冲正</a> >> 修改</span>
<fieldset class="mini-fieldset">
	<legend>交易信息</legend>
<div id="TradeOffsetEdit">
	<table id="field_form" class="mini-table" style="width:100%">
		<tr>
			<td><input id="refNo" name="refNo" class="mini-textbox"  labelField="true" enabled="false"  label="原交易单号" style="width:100%;" labelStyle = "width:110px;" /></td>
			<td><input id="cName" name="cName" class="mini-textbox"  labelField="true" enabled="false" label="客户" style="width:100%;" labelStyle = "width:110px;" />
			<input class="mini-hidden" id="dealNo" name="dealNo"  >
			<input class="mini-hidden" id="cNo" name="cNo">
			<input class="mini-hidden" id="aDate" name="aDate" />
			<input class="mini-hidden" id="version" name="version"></td>
			<td><input id="prdName" name="prdName" class="mini-textbox"  labelField="true" enabled="false" label="产品名称" style="width:100%;" labelStyle = "width:110px;" /></td>
		</tr>
		<tr>
			<td><input id="sponsorName" name="sponsorName" class="mini-textbox"  labelField="true" enabled="false" label="原交易发起人" style="width:100%;" labelStyle = "width:110px;" />
			<input id="sponsor" name="sponsor" class="mini-hidden" /></td>
			<td><input id="sponInstName" name="sponInstName" class="mini-textbox"  labelField="true" enabled="false" label="原交易发起机构" style="width:100%;" labelStyle = "width:110px;" />
			<input id="sponInst" name="sponInst" class="mini-hidden" /></td>
			<td><input id="rateType" name="rateType" class="mini-combobox"  labelField="true" data="CommonUtil.serverData.dictionary.rateType"  enabled="false" label="利率类型" style="width:100%;" labelStyle = "width:110px;" /></td>
		</tr>
		<tr>
			<td><input id="acturalRate" name="acturalRate" class="mini-spinner"   changeOnMousewheel='false'   labelField="true"  format="p4" enabled="false" label="原交易利率" style="width:100%;" labelStyle = "width:110px;" /></td>
			<td><input id="amt" name="amt" class="mini-spinner"   changeOnMousewheel='false'   labelField="true"  maxValue="9999999999999"  format="n2" enabled="false" label="原交易本金(元)" style="width:100%;" labelStyle = "width:110px;" /></td>
			<td><input id="mDate" name="mDate" class="mini-datepicker"  labelField="true"   enabled="false" label="到期日期" style="width:100%;" labelStyle = "width:110px;" /></td>
			<td><input id="accountingDate" name="accountingDate" class="mini-datepicker"  labelField="true"   enabled="false" label="账务日期" style="width:100%;" labelStyle = "width:110px;" /></td>
		</tr>
		<tr>
			<td colspan="3"><input id="remark" name="remark" class="mini-textarea"  labelField="true" required="true"  vtype="maxLength:100"  label="冲正备注" style="width:100%;" labelStyle = "width:110px;" /></td>
		</tr>
	</table>
</div>
</fieldset>
<%@ include file="../mini_base/mini_flow/MiniApproveOpCommon.jsp"%>
<div  class="mini-toolbar" style="text-align:left" >
	<a onclick="save()" id="save_btn" class="mini-button" style="display: none"   >保存</a>
	<a onclick="colse()" id="close_btn" class="mini-button" style="display: none"   >关闭</a>
</div>
<script type="text/javascript">
	mini.parse();
	//获取当前tab
	mini.get("accountingDate").setVisible(false);
	var _bizDate = '<%=__bizDate %>';
	var admin =userId;
	var instId=instId;
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var row=params.selectData;
	var url=window.location.search;
	var action=CommonUtil.getParam(url,"action");
	// var row=top["tradeOffsetManage"].getData(action);
	var form=new mini.Form("#field_form");
	var tradeData={};
	tradeData.selectData=row;
	tradeData.operType=action;
	tradeData.serial_no=row.dealNo;
	tradeData.task_id=row.taskId;
	//加载信息
	function initform(){
		if($.inArray(action,["edit","approve","detail"])>-1){
			$("#labell").html("<a href='javascript:CommonUtil.activeTab();'>交易冲正</a> >> 详情");
			var params={};
			params['dealNo']=row.refNo;
			params['version']=row.version;
			CommonUtil.ajax({
				url:"/DurationController/getTdProductApproveMain",
				data:mini.encode(params),
				callback:function(data){
					form.setData(row);
					mini.get("cName").setValue(data.counterParty.party_name);
					mini.get("prdName").setValue(data.product.prdName);
					mini.get("cNo").setValue(data.counterParty.party_id);
					mini.get("amt").setValue(data.amt);
					mini.get("sponsor").setValue(admin);
					mini.get("sponInst").setValue(instId);
					mini.get("sponsorName").setValue(data.user.userName);
					mini.get("sponInstName").setValue(data.institution.instName);
					mini.get("rateType").setValue(data.rateType);
					mini.get("acturalRate").setValue(data.contractRate);
					mini.get("mDate").setValue(data.mDate);
					mini.get("accountingDate").setValue(_bizDate);
				}
			});
		}else{
			$("#labell").html("<a href='javascript:CommonUtil.activeTab();'>交易冲正</a> >> 新增");
			var data={dealNo:row.dealNo};
			CommonUtil.ajax({
			url:"/ProductApproveController/searchProductApproveForDuration",
			data:mini.encode(data),
			callback : function(data) {
				form.setData(data.obj);
				mini.get("dealNo").setValue('');
				mini.get("version").setValue(data.obj.version);
				mini.get("refNo").setValue(data.obj.dealNo);
				mini.get("cName").setValue(data.obj.counterParty.party_name);
				mini.get("prdName").setValue(data.obj.product.prdName);
				mini.get("cNo").setValue(data.obj.counterParty.party_id);
				mini.get("amt").setValue(data.obj.amt);
				mini.get("sponsorName").setValue(data.obj.user.userName);
				mini.get("sponInstName").setValue(data.obj.institution.instName);
				mini.get("acturalRate").setValue(data.obj.contractRate);
				mini.get("mDate").setValue(data.obj.mDate);
				mini.get("accountingDate").setValue(_bizDate);

				mini.get("sponsor").setValue("<%=__sessionUser.getUserId()%>");
				mini.get("sponInst").setValue("<%=__sessionUser.getInstId() %>");
				mini.get("aDate").setValue("<%=__bizDate %>");
			}
		});
		}
	}
	//保存信息
	function save(){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统也提示");
			return;
		}
		var messageid = mini.loading("保存中, 请稍等 ...", "Loading");
		var data=form.getData(true,false);
		data['trdtype']="3003";
		// var saveUrl = "/ApproveManageController/saveApprove";
		// if(row.dealType=="2"){
			saveUrl = "/TradeManageController/saveTrade";
		// }
		var params=mini.encode(data);
		CommonUtil.ajax({
			url:saveUrl,
			data:params,
			callback:function(data){
				mini.hideMessageBox(messageid);
				mini.alert("保存成功","提示",function(){
					setTimeout(function(){top["win"].closeMenuTab();},10)
				});
				
			}
		});
	}
	//关闭表单
	function colse(){
		top["win"].closeMenuTab();
	}
	$(document).ready(function(){
		if($.inArray(action,["approve","detail"])>-1){
			mini.get("save_btn").setVisible(false);
			form.setEnabled(false);
		}
		initform();
	});
</script>
<script type="text/javascript" src="<%=basePath%>/standard/InterBank/mini_base/mini_flow/MiniApproveOpCommon.js"></script>
</body>
</html>
