<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title></title>
  </head>
  
<body style="width:100%;height:100%;background:white">
	<span id="labell"><a href="javascript:CommonUtil.activeTab();">到期展期</a> >> 修改</span>
<div id="tabs1" class="mini-tabs" activeIndex="0" style="width:100%;height:70%;background:white" >
	<div id="TradeExtendEdit" title="到期展期合同信息">
		<table id="field_form" class="mini-table" style="width:100%;background:white;">
			<tr>
				<td><input id="refNo" name="refNo" class="mini-textbox"  labelField="true" enabled="false"  label="原交易单号" style="width:100%;" labelStyle = "width:110px;" /></td>
				<td><input id="cName" name="cName" class="mini-textbox"  labelField="true" enabled="false" label="交易对手名称" style="width:100%;" labelStyle = "width:110px;" />
				<input class="mini-hidden" id="dealNo" name="dealNo"  >
				<input class="mini-hidden" id="cNo" name="cNo">
				<input class="mini-hidden" id="aDate" name="aDate" />
				<input class="mini-hidden" id="dealDate" name="dealDate" />
				<input class="mini-hidden" id="version" name="version"></td>
				<td><input id="prdName" name="prdName" class="mini-textbox"  labelField="true" enabled="false" label="产品名称" style="width:100%;" labelStyle = "width:110px;" /></td>
			</tr>
			<tr>
				<td><input id="originalAmt" name="originalAmt" class="mini-spinner"   changeOnMousewheel='false'   labelField="true"   format="n2" enabled="false" label="原交易本金(元)" style="width:100%;" labelStyle = "width:110px;" minValue="0" maxValue="9999999999999"/></td>
				<td><input id="sponsorName" name="sponsorName" class="mini-textbox"  labelField="true" enabled="false" label="原交易发起人" style="width:100%;" labelStyle = "width:110px;" />
				<input id="sponsor" name="sponsor" class="mini-hidden" /></td>
				<td><input id="sponInstName" name="sponInstName" class="mini-textbox"  labelField="true" enabled="false" label="原交易发起机构" style="width:100%;" labelStyle = "width:110px;" />
				<input id="sponInst" name="sponInst" class="mini-hidden" /></td>
			</tr>
			<tr>
				<td><input id="rateType" name="rateType" class="mini-combobox"  labelField="true" data="CommonUtil.serverData.dictionary.rateType"  enabled="false" label="利率类型" style="width:100%;" labelStyle = "width:110px;" /></td>
				<td><input id="acturalRate" name="acturalRate" class="mini-spinner"   changeOnMousewheel='false'   labelField="true"  format="p4" enabled="false" label="原交易利率" style="width:100%;" labelStyle = "width:110px;" /></td>
				<td><input id="vDate" name="vDate" class="mini-datepicker"  labelField="true"   enabled="false" label="起息日期" style="width:100%;" labelStyle = "width:110px;" /></td>
			</tr>
			<tr>
				<td><input id="mDate" name="mDate" class="mini-datepicker"  labelField="true"   enabled="false" label="到期日期" style="width:100%;" labelStyle = "width:110px;" /></td>
				<td><input id="term" name="term" class="mini-textbox"  labelField="true" enabled="false" label="计息天数(天)" style="width:100%;" labelStyle = "width:110px;" />
				<td><input id="basis" name="basis" class="mini-combobox"  labelField="true" enabled="false" label="计息基础" data="CommonUtil.serverData.dictionary.DayCounter" style="width:100%;" labelStyle = "width:110px;" />
			</tr>
			<tr>
				<td><input id="mInt" name="mInt" class="mini-spinner"   changeOnMousewheel='false'   labelField="true"   format="n2" enabled="false" label="到期利息(元)" style="width:100%;" labelStyle = "width:110px;" minValue="0" maxValue="9999999999999"/></td>
				<td><input id="mAmt" name="mAmt" class="mini-spinner"   changeOnMousewheel='false'   labelField="true"   format="n2" enabled="false" label="到期金额(元)" style="width:100%;" labelStyle = "width:110px;" minValue="0" maxValue="9999999999999"/></td>
				<td><input id="remainAmt" name="remainAmt" class="mini-spinner"   changeOnMousewheel='false'   labelField="true" minValue="0" maxValue="9999999999999" format="n2" enabled="false" label="持仓本金金额(元)" style="width:100%;" labelStyle = "width:110px;" /></td>
			</tr>
			<tfoot>
				<tr>
					<td><input id="expAmt" name="expAmt" class="mini-spinner"   changeOnMousewheel='false'  required="true"  labelField="true"  minValue="0" maxValue="9999999999999"  format="n2" label="展期金额(元)" style="width:100%;" labelStyle = "width:110px;" /></td>
					<td><input id="expRate" name="expRate" class="mini-spinner"   changeOnMousewheel='false'  required="true"  labelField="true"  format="p4" label="展期利率" style="width:100%;" labelStyle = "width:110px;" />
						<input id="feeRate" name="feeRate" class="mini-hidden"/></td>
					<td><input id="expVdate" name="expVdate" class="mini-datepicker" required="true"  labelField="true"  label="展期起息日" style="width:100%;" labelStyle = "width:110px;" enabled="false"/></td>
				</tr>
				<tr>
					<td><input id="expMdate" name="expMdate" class="mini-datepicker" required="true"  labelField="true" label="展期到期日" style="width:100%;" labelStyle = "width:110px;" /></td>
				</tr>
				<tr>
					<td colspan="3"><input id="expReason" name="expReason" class="mini-textarea" required="true"  vtype="maxLength:100" labelField="true"  label="交易展期备注" style="width:100%;" labelStyle = "width:110px;" /></td>
				</tr>
				<tr>
					<td colspan="3">
						<div  class="mini-toolbar" style="text-align:left">
							<a onclick="save()" id="save_btn" class="mini-button" style="display: none"   >保存</a>
							<a onclick="colse()" id="close_btn" class="mini-button" style="display: none"   >关闭</a>
						</div>
					</td>
				</tr>
			</tfoot>
			
		</table>
	</div>
	<div id="InterestSchedule" name="InterestSchedule" title="展期收息计划" style="background:white">
		<fieldset class="mini-fieldset">
		<legend>收息计划调整</legend>
		<div id="InterestSchedule_from_div">
			<input id="dayCounter" name="dayCounter" class="mini-hidden">
			<table class='mini-table' style="width:100%;background:white;">
				<tr>
					<td><input id="cfType" name="cfType" class="mini-combobox" value="Interest"  labelField="true" data="CommonUtil.serverData.dictionary.cfType"  enabled="false" label="现金流类型" style="width:100%;" labelStyle = "width:110px;" /></td>
					<td><input id="executeRate" name="executeRate" class="mini-spinner" changeOnMousewheel='false' format="p4"  labelField="true" required="true"   label="区间利率" style="width:100%;" labelStyle = "width:110px;" enabled="false"/></td>
					<td><input id="interestAmt" name="interestAmt" class="mini-spinner" changeOnMousewheel='false'  labelField="true" maxValue="999999999999"  format="n2" enabled="false"  label="收息金额（计划）" style="width:100%;" labelStyle = "width:110px;" /></td>
				</tr>
				<tr>
					<td><input id="refBeginDate" name="refBeginDate" class="mini-datepicker" labelField="true"  required="true"  label="区间起始日期" style="width:100%;" labelStyle = "width:110px;" /></td>
					<td><input id="refEndDate" name="refEndDate" class="mini-datepicker" onValuechanged="refEndDateValuechanged" labelField="true"  required="true"  label="区间结束日期" style="width:100%;" labelStyle = "width:110px;" /></td>
					<td><input id="theoryPaymentDate" name="theoryPaymentDate" class="mini-datepicker" labelField="true"  label="收息日期（计划）" style="width:100%;" labelStyle = "width:110px;" /></td>
				</tr>
				<tr>
					<td colspan="2" >
						<div  class="mini-toolbar" style="text-align:left" >
							<a id="fresh_btn"   class="mini-button" style="display: none"   onclick="loadInterestPlan">重置计划</a>
							<a id="updateInterest_btn"  class="mini-button" style="display: none"      onclick="updateInterestPlan">修改</a>
							<a id="saveInterest_btn" class="mini-button" style="display: none"    onclick="saveInterestPlan()" >保存</a>
						</div>
					</td>
				</tr>
			</table>
		</div>
		</fieldset>
		<fieldset style="min-width:0;">
		<legend>收息计划详情</legend>
		<div id="interest_grid" class="mini-datagrid  borderAll" style="width:100%;height:300px;" mini-fit="true"
			allowResize="true"  border="true" sortMode="client" showPager="true"
			pageSize="10" sizeList="[10,20]" multiSelect="true"  onrowclick="onRowClick"
			allowCellEdit="true" allowCellSelect="true" multiSelect="false" editNextOnEnterKey="true"  editNextRowCell="true">
		
			<div property="columns">
				<div type="indexcolumn"     headerAlign="center"  allowSort="false" width="40" >序号</div> 
				
				<div field="cfType"         headerAlign= "center" allowSort= "true" width="120"
					renderer="CommonUtil.dictRenderer" data-options="{dict:'cfType'}" align="center">现金流类型
				</div> 
				
				<div field="executeRate"  headerAlign= "center" allowSort= "true" width="120" align = "right"  numberFormat="p4">区间利率</div>  
				
				<div field="refBeginDate" headerAlign= "center" allowSort= "true" width="100" dateFormat="yyyy-MM-dd" align="center">区间起始日期
				</div>
		
				<div field="refEndDate" headerAlign= "center" allowSort= "true" width="100" dateFormat="yyyy-MM-dd" align="center">区间结束日期
				</div> 
		
				<div field="theoryPaymentDate" headerAlign= "center" allowSort= "true" width="100" dateFormat="yyyy-MM-dd" align="center">收息日期（计划）
				</div>
		
				<div field="interestAmt"  headerAlign= "center" allowSort= "true" width="120" align = "right" dataType="float" numberFormat="#,0.00">收息金额（计划）
				</div>             
				
				<div field="actualDate" headerAlign= "center" allowSort= "true" width="100" dateFormat="yyyy-MM-dd" align="center">收息日期（实际）
				</div>                          
				
				<div field="actualRamt"  headerAlign= "center" allowSort= "true" width="120" align = "right" dataType="float" numberFormat="#,0.00" >收息金额（实际）
				</div>                              
			</div>
		</div>
		</fieldset>
	</div>
	<div id="CapitalSchedule" name="CapitalSchedule" title="展期还本计划" style="background:white">
		<fieldset class="mini-fieldset">
		<legend>还本计划调整</legend>
		<div id="CapitalSchedule_from_div">
			<table class='mini-table' style="width:100%;background:white;">
				<tr>
					<td><input id="cfType" name="cfType" class="mini-combobox" value="Principal"  labelField="true" data="CommonUtil.serverData.dictionary.cfType"  enabled="false" label="现金流类型" style="width:100%;" labelStyle = "width:110px;" /></td>
					<td><input id="payDirection" name="payDirection" class="mini-combobox" value="Recevie"  labelField="true" data="CommonUtil.serverData.dictionary.feeSituation"  enabled="false" label="收付方向" style="width:100%;" labelStyle = "width:110px;" /></td>
					<td><input id="repaymentSdate" name="repaymentSdate" class="mini-datepicker" labelField="true"  required="true"  label="还本日期（计划）" style="width:100%;" labelStyle = "width:110px;" /></td>
				</tr>
				<tr>
					<td><input id="repaymentSamt" name="repaymentSamt" class="mini-spinner"   changeOnMousewheel='false'   labelField="true" required="true"  maxValue="999999999999"  format="n2"   label="还本金额（计划）" style="width:100%;" labelStyle = "width:110px;" /></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td colspan="3" >
						<div  class="mini-toolbar" style="text-align:left" >
							<a  id="cap_add_btn" class="mini-button" style="display: none"    onclick="addcap()">新增</a>
							<a  id="cap_delete_btn" class="mini-button" style="display: none"    onclick="delcap()">删除</a>
							<a  id="cap_save_btn" class="mini-button" style="display: none"    onclick="saveCapPlan()" >保存</a>
						</div>
					</td>
				</tr>
			</table>
		</div>
		</fieldset>
		<fieldset style="min-width:0;">
		<legend>还本计划详情</legend>
		<div id="capital_grid" class="mini-datagrid  borderAll" style="width:100%;height:300px;" mini-fit="true"
			allowResize="true"  border="true" sortMode="client" showPager="false"
			pageSize="100"  multiSelect="true" 
			allowCellEdit="true" allowCellSelect="true" multiSelect="false" editNextOnEnterKey="true"  editNextRowCell="true">
		
			<div property="columns">
				<div type="indexcolumn"     headerAlign="center"  allowSort="false" width="40" >序号</div>
				<div type="checkcolumn"     headerAlign="center"></div>    
				
				<div field="cfType"         headerAlign= "center" allowSort= "true" width="120"
					renderer="CommonUtil.dictRenderer" data-options="{dict:'cfType'}" align="center">现金流类型
				</div> 
				
				<div field="payDirection"        headerAlign= "center" allowSort= "true" width="120" type="comboboxcolumn" 
					renderer="CommonUtil.dictRenderer" data-options="{dict:'feeSituation'}"  align="center">收付方向
				</div> 
				
				<div field="repaymentSdate" headerAlign= "center" allowSort= "true" width="100" dateFormat="yyyy-MM-dd" align="center">还本日期(计划)
					<input class="mini-datepicker"  required="true"  property="editor"/>
				</div> 
				
				<div field="repaymentSamt"  headerAlign= "center" allowSort= "true" width="120" align = "right" dataType="float" numberFormat="#,0.00">还本金额(计划)
					<input class="mini-spinner"   changeOnMousewheel='false'  required="true"  property="editor"
						minValue="0" maxValue="999999999999999" changeOnMousewheel="false"/>
				</div>             
				
				<div field="repaymentTdate" headerAlign= "center" allowSort= "true" width="100" dateFormat="yyyy-MM-dd" align="center">还本日期(实际)
				</div>                     
				
				<div field="repaymentTamt"  headerAlign= "center" allowSort= "true" width="120" align = "right" dataType="float" numberFormat="#,0.00" >还本金额(实际)
				</div>                              
			</div>
		</div>
		</fieldset>
	</div>
</div>
<table class="mini-from" width="100%" cols="4">
	<tr>
		<td colspan="2" style="font-size:16px;border-bottom: 2px solid #B3DE94;border-top: 2px solid #B3DE94;">
			&nbsp;<span><font color="red">展期交易说明:</font></span><BR/>
			&nbsp;<span>"原交易"默认到期日发生展期,展期起息日必须大于"原交易"到期日期;</span><BR/>
			&nbsp;<span>"展期金额"可以选择"全部"或"部分"金额展期;"展期利率"可以手工填写;默认"原交易执行利率"</span><BR/>
			&nbsp;<span>"还本计划"默认到期一次还本，需要修改还本计划，请审批通过后更改"还本计划"</span><BR/>
			&nbsp;<span>"收息计划"默认与原交易一致，需要修改收息计划，请审批通过后更改"收息计划"</span>
		</td>
	</tr>
</table>
<%@ include file="../mini_base/mini_flow/MiniApproveOpCommon.jsp"%>
<script type="text/javascript">
	mini.parse();
	//获取当前tab
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var row = params.selectData;
	var url = window.location.search;
	var action = CommonUtil.getParam(url,"action");
	var _bizDate = '<%=__bizDate %>';
	var form = new mini.Form("#field_form");
	var tabs = mini.get("tabs1");
	var capGrid = mini.get("capital_grid");
	var intGrid = mini.get("interest_grid");

	var tradeData={};
	tradeData.selectData=row;
	tradeData.operType=action;
	tradeData.serial_no=row.dealNo;
	tradeData.task_id=row.taskId; 

	var initIntTab = false;
	var initCapTab = false;
	tabs.on("beforeactivechanged",function(e){
        if($.inArray(e.tab.name,['InterestSchedule','CapitalSchedule']) > -1) 
		{	
			if(CommonUtil.isNull(mini.get("dealNo").getValue()))
			{
				mini.alert("请先保存展期交易!","系统提示");
				e.cancel = true;
				return;
			}else{
				if(e.tab.name == 'InterestSchedule'){//加载利息
					if(initIntTab == false){
						loadInterestPlan();
						initIntTab = true;
					}
				}else if(e.tab.name == 'CapitalSchedule'){
					if(initCapTab == false){
						loadCapitalPlan();
						initCapTab = true;
					}
				}
			}
        }
    });

	var dataResult = null;
	intGrid.on("beforeload", function (e){
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		searchIntPlan(pageSize,pageIndex);
	});

	function searchIntPlan(pageSize,pageIndex)
	{
		if(CommonUtil.isNull(dataResult)){
			dataResult = [];
		}
		var totalCount = dataResult.length;
		var data = dataResult;
        var arr = [];
        var start = pageIndex * pageSize, end = start + pageSize;
		for (var i = start, l = end; i < l; i++) 
		{
            var record = data[i];
            if (!record) continue;
            arr.push(record);
        }
		intGrid.setTotalCount(totalCount);
		intGrid.setPageIndex(pageIndex);
		intGrid.setPageSize(pageSize);
		intGrid.setData(arr);
	}// end search


	//初始化tab页
	function initTabs(){
		tabs.setTabPosition("top");
		tabs.setTabAlign("left");
	}
	//加载信息
	function initform(){
		if($.inArray(action,["edit","approve","detail"])>-1){
			$("#labell").html("<a href='javascript:CommonUtil.activeTab();'>到期展期</a> >> 详情");
			var params={};
			params['dealNo']=row.refNo;
			params['version']=row.version;
			CommonUtil.ajax({
				url:"/DurationController/getTdProductApproveMain",
				data:mini.encode(params),
				callback:function(data){
					form.setData(row);
					mini.get("refNo").setValue( data.dealNo);
					mini.get("cName").setValue( data.counterParty.party_name);
					mini.get("cNo").setValue(data.counterParty.party_id);
					mini.get("prdName").setValue( data.product.prdName);
					mini.get("originalAmt").setValue( data.amt);
					mini.get("rateType").setValue( data.rateType);
					mini.get("dealDate").setValue( _bizDate);
					mini.get("vDate").setValue( data.vDate);
					mini.get("mDate").setValue( data.mDate);
					mini.get("sponsor").setValue( data.user.userId);
					mini.get("sponInst").setValue(data.institution.instId);
					mini.get("sponsorName").setValue( data.user.userName);
					mini.get("sponInstName").setValue( data.institution.instName);
					mini.get("version").setValue(data.version);
					mini.get("basis").setValue(data.basis);
					mini.get("mInt").setValue(data.mInt);
					mini.get("mAmt").setValue(data.mAmt);
					mini.get("term").setValue(data.occupTerm);
					mini.get("dealNo").setValue(row.dealNo);
					mini.get("expMdate").setValue(row.expMdate);
					mini.get("acturalRate").setValue(data.contractRate);	
					CommonUtil.ajax({
						url:"/TdFeesPassAgewayController/getSumFeeRateByRefNo",
						data:{refNo:row.refNo},
						callback:function(data){
							mini.get("feeRate").setValue(data.obj);
						}
					});
					//loadInterestPlan();
					//loadCapitalPlan();
				}
			});
			
			
		}else {
			$("#labell").html("<a href='javascript:CommonUtil.activeTab();'>到期展期</a> >> 新增");
			var data={dealNo:row.dealNo};
			intGrid.setData(row);
			CommonUtil.ajax({
				url:"/ProductApproveController/searchProductApproveForDuration",
				data:mini.encode(data),
				callback : function(data) {
					form.setData(row);
					mini.get("dealDate").setValue( _bizDate);
					mini.get("dealNo").setValue('');
					mini.get("refNo").setValue( data.obj.dealNo);
					mini.get("cName").setValue( data.obj.counterParty.party_name);
					mini.get("cNo").setValue(data.obj.counterParty.party_id);
					mini.get("prdName").setValue( data.obj.product.prdName);
					mini.get("sponsorName").setValue( data.obj.user.userName);
					mini.get("sponInstName").setValue( data.obj.institution.instName);
					mini.get("version").setValue(data.obj.version);
					mini.get("acturalRate").setValue(data.obj.contractRate);
					mini.get("expRate").setValue(data.obj.contractRate);
					mini.get("originalAmt").setValue(data.obj.amt);
					mini.get("expVdate").setValue(data.obj.mDate);
					mini.get("remainAmt").setValue(row.settlAmt);
					mini.get("expAmt").setValue(row.settlAmt);

					mini.get("sponsor").setValue("<%=__sessionUser.getUserId()%>");
					mini.get("sponInst").setValue("<%=__sessionUser.getInstId() %>");
					mini.get("aDate").setValue("<%=__bizDate %>");

					CommonUtil.ajax({
						url:"/TdFeesPassAgewayController/getSumFeeRateByRefNo",
						data:{refNo:data.obj.dealNo},
						callback:function(data){
							mini.get("feeRate").setValue(data.obj);
						}
					});
				}
			});
		}
	}

	function loadInterestPlan(){
		var dealNo = mini.get("dealNo").getValue();
		CommonUtil.ajax({
			url:"/tmCalCashFlowController/getApproveInterestForApprove",
			data:mini.encode({dealNo : dealNo, dealType:'2'}),
			callback : function(data) {
				data = mini.decode(data);
				dataResult = data.obj;
				searchIntPlan(intGrid.pageSize,0);
			}
		});
	}

	function updateInterestPlan(){
		var selectData =  intGrid.getSelected();
		if(!selectData){
			mini.alert("请选择要修改的收息计划！","系统提示");
			return;
		}
		if(CommonUtil.isNotNull(selectData.actualDate))
		{
			mini.alert("该条计划已经确认过,不能再次修改！","系统提示");
			return;
		}

		var addform = new mini.Form("InterestSchedule_from_div");
		addform.validate();
		if(addform.isValid() == false){
			mini.alert("交易验证失败,请检查表单是否填写正确!","消息提示")
			return
		};
		var row = addform.getData(true);
		var param = form.getData(true);
		if(row.refBeginDate < param.expVdate){
			mini.alert("区间起始日期不能小于展期起息日.","提示");
			return;
		}
		if(row.refEndDate < row.refBeginDate || row.refBeginDate > param.expMdate){
			mini.alert("当前区间起息日不能大于区间到期日或者展期到期日","提示");
			return;
		}
		if(row.refEndDate < param.expVdate || row.refEndDate > param.expMdate){
			mini.alert("区间结束日期不能小于展期起息日或大于展期到期日.","提示");
			return;
		}
		if(row.theoryPaymentDate < param.expVdate || row.theoryPaymentDate > param.expMdate){
			mini.alert("计划收息日期不能展期起息日或大于展期到期日","提示");
			return;
		}
		//dealNo refNo tmCashflowInterest tmCashflowInterestList
		var paramData = {};
		paramData['refNo'] = mini.get("refNo").getValue();
		paramData['dealNo'] = mini.get("dealNo").getValue();
		paramData['dealType'] = '2';
		paramData['tmCashflowInterest'] = row;
		paramData['tmCashflowInterestList'] = dataResult;
		CommonUtil.ajax({
			url : '/DurationController/calExtendInterestPlanForTemp',
			data : mini.encode(paramData),
			callback:function(data){
				if(data && data.obj && data.obj.code == '000'){
					dataResult = data.obj.data;
					searchIntPlan(intGrid.pageSize,intGrid.pageIndex);

					mini.alert("操作成功！","系统提示");
				}else{
					mini.alert(data.obj.msg,"系统提示");
				}
			}
		});
	}

	function loadCapitalPlan(){
		var dealNo = mini.get("dealNo").getValue();
		CommonUtil.ajax({
			url:"/tmCalCashFlowController/getApproveCapitalForApprove",
			data:mini.encode({dealNo : dealNo,dealType:'2'}),
			callback : function(data) {
				data = mini.decode(data);
				capGrid.setData(data.obj);
			}
		});
	}

	function onRowClick()
	{
		var form = new mini.Form("InterestSchedule_from_div");
		var row =  intGrid.getSelected();
		form.setData(row,true,false);
		form.setIsValid(true);
	}

	function refEndDateValuechanged(e){
		mini.get("theoryPaymentDate").setValue(mini.get("refEndDate").getFormValue());
	}

	function saveInterestPlan(){
		//收息计划
		var expVdate=mini.get("expVdate").getFormValue();
		var expMdate = mini.get("expMdate").getFormValue();
		var interestData = dataResult;
	
		if(interestData.length <= 0){
			mini.alert("请维护收息计划（至少一条）!","提示");
			return false;
		}
		for(var i = 0; i < interestData.length; i++){
			var intdate = CommonUtil.stampToDate(interestData[i].theoryPaymentDate);
			if(intdate < expVdate || intdate > expMdate)
			{
				mini.alert("收息计划应在展期起息日和展期到期日之间！","提示");
				return false;
			}
		}
		var data = form.getData(true,false); 
		data["approveCashflowInterest"] = interestData;
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:"/DurationController/saveExtendCashFlowForApprove",
			data:params,
			callback:function(data){
				mini.alert("保存成功!","提示");
				loadInterestPlan();
			}
		});
		
	}

	function saveCapPlan(){
		//还本计划
		var expVdate = mini.get("expVdate").getFormValue();
		var expMdate = mini.get("expMdate").getFormValue();
		var expAmt = mini.get("expAmt").getValue();
		var capitalData = capGrid.getData();
		if(capitalData.length <= 0){
			mini.alert("请维护还本计划（至少一条）!","提示");
			return false;
		}

		var flag = false;
		var tempDateArr = {};
		var amt = 0;
		for(var i = 0; i < capitalData.length; i++){
			var capdate = CommonUtil.stampToDate(capitalData[i].repaymentSdate);
			if(CommonUtil.isNull(capdate)){
				mini.alert("还本日期不能为空！","提示");
				return false;
			}
			if(capdate <= expVdate || capdate > expMdate){
				mini.alert("还本日期必须大于展期起息日、小于等于展期到期日！","提示");
				return false;
			}
			if(expMdate == capdate)
			{
				flag = true;	
				if(parseFloat(capitalData[i].repaymentSamt) <= 0){
					mini.alert("展期到期日的还本计划金额必须大于0!","提示");
					return false;
				}
			}

			if(CommonUtil.isNotNull(tempDateArr[capdate]))
			{
				mini.alert("计划还本日期["+capdate+"]不能重复!","提示");
				return false;
			}else{
				tempDateArr[capdate] = 1;
			}

			if(parseFloat(capitalData[i].repaymentSamt) <= 0){
				mini.alert("计划还本日期["+capdate+"]对应的金额必须大于0!","提示");
				return false;
			}
			amt += parseFloat(capitalData[i].repaymentSamt);
		}

		if(flag == false){
			mini.alert("到期日必须填写还本计划!", "提示");
			return false;
		}
		
		if(parseFloat(expAmt) != parseFloat(amt)){
			mini.alert("还本调整金額必须等于展期金额!","提示");
			return false;
		}
		
		var data = form.getData(true,false);
		data["approveCashflowCapitals"] = capitalData;
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:"/DurationController/saveExtendCashFlowForApprove",
			data:params,
			callback:function(data){
				mini.alert("保存成功!","提示");
				loadCapitalPlan();
			}
		});
	}

	//保存信息
	function save(){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统也提示");
			return;
		}
		var data = form.getData(true,false);
		data['trdtype']="3001_TDEXT";

		var remainAmt =isNaN(parseFloat(mini.get("remainAmt").getValue()))==true?0:parseFloat(mini.get("remainAmt").getValue());
		var expAmt =isNaN(parseFloat(mini.get("expAmt").getValue()))==true?0:parseFloat(mini.get("expAmt").getValue());
		var rate = isNaN(parseFloat(mini.get("expRate").getValue()))==true?0:parseFloat(mini.get("expRate").getValue());
		var feeRate = isNaN(parseFloat(mini.get("feeRate").getValue()))==true?0:parseFloat(mini.get("feeRate").getValue());
		if(rate<=0){
			mini.alert("利率不能小于零!","提示");
			return false;
		}
		if(expAmt<=0){
			mini.alert("展期交易金额不能小于等于零!","提示");
			return false;
		}
		if(remainAmt<expAmt){
			mini.alert("展期交易金额不能大于交易持仓金额-剩余本金!","提示");
			return false;
		}
		if(CommonUtil.dateDiff(data.expVdate,data.mDate)>0){
			mini.alert("展期交易起息日期不能小于原交易到期日期!","提示");
			return false;
		}
		if(CommonUtil.dateDiff(data.expMdate,data.expVdate)>=0){
			mini.alert("展期交易到期日期应大于展期交易起息日期!","提示");
			return false;
		}
		if(feeRate >= rate){
			mini.alert("展期利率需大于通道总利率!","提示");
			return false;
		}
		
		data["dealType"]=row.dealType;
		var saveUrl = "/ApproveManageController/saveApprove";
		if(row.dealType=="2"){
			saveUrl = "/TradeManageController/saveTrade";
		}
		var params=mini.encode(data);
		CommonUtil.ajax({
			url:saveUrl,
			data:params,
			callback:function(data){
				mini.get("dealNo").setValue(data.trade_id);

				//切换tab时重新加载收息还本计划
				initIntTab = false;
				initCapTab = false;

				mini.alert("保存成功","提示");
			}
		});
	}
	/***********************还本计划****************************************/
	//添加还本计划
	function addcap(){
		var addform = new mini.Form("CapitalSchedule_from_div");
		addform.validate();
		if(addform.isValid() == false){
			mini.alert("交易验证失败,请检查表单是否填写正确!","消息提示")
			return
		}
		var row = addform.getData(true);

		var expVdate = mini.get("expVdate").getFormValue();
		var expMdate = mini.get("expMdate").getFormValue();
		var repaymentSdate = mini.get("repaymentSdate").getFormValue();
		if(repaymentSdate <= expVdate || repaymentSdate > expMdate){
			mini.alert("还本日期必须大于展期起息日,小于等于展期到期日!","消息提示")
			return;
		}

		var repaymentSamt = row.repaymentSamt; 
		if(parseFloat(repaymentSamt) <= 0){
			mini.alert("还本金额必须大于0!","消息提示")
			return;
		}
		
		capGrid.addRow(row, capGrid.data.length);
	}
	//删除还本计划
	function delcap(){
		var rows = capGrid.getSelecteds();
		if (rows.length > 0) {
			mini.confirm("确定删除记录？", "确定?",
				function (action) {
					if (action == "ok") 
					{
						capGrid.removeRows(rows, true);
						mini.alert("删除成功!","消息提示");
					} 
				}
			);
		}else{
			mini.alert("请选择要删除的列!","消息提示");
		}
	}
	/*************************收息计划*********************************/
	//添加收息计划
	function addint(){
		var addform = new mini.Form("InterestSchedule_from_div");
		addform.validate();
		if(addform.isValid() == false){
			mini.alert("交易验证失败,请检查表单是否填写正确!","消息提示")
			return
		};
		var row = addform.getData(true);
		intGrid.addRow(row, intGrid.data.length);
	}
	//删除收息计划
	function delint(){
		var rows = intGrid.getSelecteds();
		if (rows.length > 0) {
			mini.confirm("确定删除记录？", "确定?",
				function (action) {
					if (action == "ok") 
					{
						intGrid.removeRows(rows, true);
						mini.alert("删除成功!","消息提示");
					} 
				}
			);
		}else{
			mini.alert("请选择要删除的列!","消息提示");
		}
	}
	//关闭表单
	function colse(){
		top["win"].closeMenuTab();
	}
	$(document).ready(function(){
		if($.inArray(action,["approve","detail"])>-1){
			mini.get("save_btn").setVisible(false);
			form.setEnabled(false);
			mini.get("cap_add_btn").setVisible(false);
			mini.get("cap_delete_btn").setVisible(false);

			mini.get("save_btn").setVisible(false);
			mini.get("updateInterest_btn").setVisible(false);
			mini.get("saveInterest_btn").setVisible(false);
			mini.get("fresh_btn").setVisible(false);
			
			mini.get("cap_add_btn").setVisible(false);
			mini.get("cap_delete_btn").setVisible(false);
			mini.get("cap_save_btn").setVisible(false);
		}
		initform();
		initTabs();
	});
</script>
<script type="text/javascript" src="<%=basePath%>/standard/InterBank/mini_base/mini_flow/MiniApproveOpCommon.js"></script>
</body>
</html>
