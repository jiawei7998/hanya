<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="../../global.jsp"%>
<html>
  <head>
	  <title></title>
	  <script src="<%=basePath%>/sl/TaDictController/dictionary.js"  type="text/javascript"></script>
  </head>
  
  <body style="width:100%;height:100%;background:white">
		<fieldset class="mini-fieldset">
				<legend>查询</legend>
		<div id="search_form">
			<!-- <table id="search_form" class="mini-form" style="width:100%;">
				<tr> -->
					
						<input id="trade_id" name="trade_id" class="mini-textbox" vtype="maxLength:50" labelField="true"   label="交易单号：" emptyText='请输入交易单号' labelStyle="text-align:right;"/>
					
					
						<input id="party_name" name="party_name" class="mini-textbox" labelField="true"   label="客户：" emptyText='请输入客户简称' labelStyle="text-align:right;" />
					
						<input id="product_name_search" name="product_name_search"  class="mini-textbox" labelField="true"  label="产品名称：" emptyText='请输入产品名称' labelStyle="text-align:right;" />
					
				
						<span style="float:right;margin-right: 150px">
							<a  id="search_btn" class="mini-button" style="display: none"   onclick="search(10,0)">查询</a>
							<a  id="clear_btn" class="mini-button" style="display: none"   onclick='clear()'>清空</a>
						</span>
					
			<!-- </table> -->
		</div>
		</fieldset>
		 
			<div id="dataGrid" class="mini-dataGrid borderAll" style="width:100%;height:350px;" 
			idField="dealNo" allowResize="true" onrowdblclick="onRowDblClick" border="true" >
				<div property="columns" >
					<div type="indexcolumn" headerAlign="center" allowSort="false" width="40">序号</div>
					<div field="dealNo" width="150" align="center"  headerAlign="center" allowSort="true">交易单号</div>    
					<div field="counterParty.party_name" width="150" align="left"  headerAlign="center" allowSort="true">客户简称</div>                            
					<div field="product.prdName" width="180" align="left" headerAlign="center" >产品名称</div>
					<div field="amt" width="180" numberFormat="#,0.00" align="right" headerAlign="center" allowSort="true" >合同额（元）</div>
					<div field="vDate" width="180" align="center" headerAlign="center" >起息日</div>
				</div>
			</div>

			<script type="text/javascript">
				mini.parse();
				var url=window.location.search;
				//var mDate=CommonUtil.getParam(url,"mDate");
				var businessType=CommonUtil.getParam(url,"businessType");
				var grid=mini.get("dataGrid");
				var form=new mini.Form("#search_form");
				//清空
				function clear(){
					form.clear();
				}
				//查询
				function search(pageSize,pageIndex){
					form.validate();
					if(form.isValid()==false){
						mini.alert("信息填写有误","系统提示");
						return ;
					}
					var data=form.getData();
					data['pageSize']=pageSize;
					data['pageNumber']=pageIndex+1;
					//data['mDate']=mDate;
					data['businessType']=businessType;
					var params=mini.encode(data);
					CommonUtil.ajax({
						url:'/trdTposController/getBalance',
						data:params,
						callback : function(data) {
							var grid = mini.get("dataGrid");
							grid.setTotalCount(data.obj.total);
							grid.setPageIndex(pageIndex);
							grid.setPageSize(pageSize);
							grid.setData(data.obj.rows);
						}
					});
				}
			
				grid.on("beforeload",function(e){
					e.cancel = true;
					var pageSize=e.data.pageSize;
					var pageIndex=e.data.pageIndex;
					search(pageSize,pageIndex);
				});
			
				function onRowDblClick(e){
					var row=grid.getSelected();
					if(row.businessType!="investFollowup"){
					CommonUtil.ajax({
						url:"/TdBalanceController/ifExistBalance",
						data:{'refNo':row.dealNo},
						callback : function(data){
							if(data.obj != null){
								if(data.obj.sponsor != null){
									mini.alert('该交易已存在存续期业务<br>【业务类型：'+data.obj.remark+'】<br>【业务员：'+data.obj.sponsor+'】<br>【单号：'+data.obj.dealNo+'】','系统提示！');
								}else{
									mini.alert('该交易已存在未到期存续期业务<br>【单号：'+data.obj.dealNo+'】','系统提示！');
								}
								return false;
							}else{
								CommonUtil.ajax({
									url:"/TdBalanceController/ifTradeEnd",
									data:{'refNo':row.dealNo},
									callback : function(data) {
										if(data.obj!=null){
											mini.alert("该笔交易已经被发起结清。","提示！");
											return false;
										}
										 //CommonUtil.closedailog(null,row);
										 CloseWindow("ok");
									}
								});
							}
						}
					});
					}else{

					CloseWindow("ok");
					}
				}
				function CloseWindow(action) {
				   if (window.CloseOwnerWindow) return window.CloseOwnerWindow(action);
				   else window.close();
				}
				function GetData() {
					var grid=mini.get("dataGrid");
					var row = grid.getSelected();
					return row;
				}
			
			
				$(document).ready(function(){
					search(10,0);
				});
			</script>
	

</body>
</html>