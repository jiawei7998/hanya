<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title></title>
  </head>
  
<body style="width:100%;height:100%;background:white">
		<span id="labell"><a href="javascript:CommonUtil.activeTab();">资产卖断</a> >> 修改</span>
<fieldset class="mini-fieldset">
	<legend>交易详情</legend>
<div id="TradeOutRightSaleEdit">
	<table id="field_form" class="mini-table" style="width:100%">
		<tr>
			<td><input id="refNo" name="refNo" class="mini-textbox"  labelField="true" enabled="false"  label="原交易单号" style="width:100%;" labelStyle = "width:110px;" /></td>
			<td><input id="cName" name="cName" class="mini-textbox"  labelField="true" enabled="false" label="交易对手名称" style="width:100%;" labelStyle = "width:110px;" />
			<input class="mini-hidden" id="dealNo" name="dealNo"/>
			<input class="mini-hidden" id="cNo" name="cNo"/>
			<input class="mini-hidden" id="aDate" name="aDate"/>
			<input class="mini-hidden" id="dealDate" name="dealDate" />
			<input class="mini-hidden" id="version" name="version"/></td>
			<td><input id="prdName" name="prdName" class="mini-textbox"  labelField="true" enabled="false" label="产品名称" style="width:100%;" labelStyle = "width:110px;" /></td>
		</tr>
		<tr>
			<td><input id="originalAmt" name="originalAmt" class="mini-spinner"  changeOnMousewheel="false"  labelField="true"  maxValue="9999999999999"  format="n2" enabled="false" label="原交易本金(元)" style="width:100%;" labelStyle = "width:110px;" /></td>
			<td><input id="sponsorName" name="sponsorName" class="mini-textbox"  labelField="true" enabled="false" label="原交易发起人" style="width:100%;" labelStyle = "width:110px;" />
			<input id="sponsor" name="sponsor" class="mini-hidden" /></td>
			<td><input id="sponInstName" name="sponInstName" class="mini-textbox"  labelField="true" enabled="false" label="原交易发起机构" style="width:100%;" labelStyle = "width:110px;" />
			<input id="sponInst" name="sponInst" class="mini-hidden" /></td>
		</tr>
		<tr>
			<td><input id="rateType" name="rateType" class="mini-combobox"  labelField="true" data="CommonUtil.serverData.dictionary.rateType"  enabled="false" label="利率类型" style="width:100%;" labelStyle = "width:110px;" /></td>
			<td><input id="orgiRate" name="orgiRate" class="mini-spinner"  changeOnMousewheel="false"  labelField="true"  format="p4" enabled="false" label="原交易利率" style="width:100%;" labelStyle = "width:110px;" /></td>
			<td><input id="vDate" name="vDate" class="mini-datepicker"  labelField="true"   enabled="false" label="起息日期" style="width:100%;" labelStyle = "width:110px;" /></td>
		</tr>
		<tr>
			<td><input id="orgiMdate" name="orgiMdate" class="mini-datepicker"  labelField="true"   enabled="false" label="到期日期" style="width:100%;" labelStyle = "width:110px;" /></td>
			<td><input id="term" name="term" class="mini-spinner" maxValue="10000000"  changeOnMousewheel="false"  labelField="true"  enabled="false"  label="占款期限(天)" style="width:100%;" labelStyle = "width:110px;" /></td>
			<td><input id="basis" name="basis" class="mini-combobox"  labelField="true" data="CommonUtil.serverData.dictionary.DayCounter"  enabled="false" label="计息基础" style="width:100%;" labelStyle = "width:110px;" /></td>
		</tr>
		<tr>
			<td><input id="mInt" name="mInt" class="mini-spinner" maxValue="9999999999999" changeOnMousewheel="false"  labelField="true"  format="n2" enabled="false" label="到期利息(元)" style="width:100%;" labelStyle = "width:110px;" /></td>
			<td><input id="mAmt" name="mAmt" class="mini-spinner"  changeOnMousewheel="false"  maxValue="9999999999999" labelField="true"  format="n2" enabled="false" label="到期金额(元)" style="width:100%;" labelStyle = "width:110px;" /></td>
			<td><input id="remainAmt" name="remainAmt" class="mini-spinner"  changeOnMousewheel="false"  labelField="true"  format="n2" enabled="false" maxValue="9999999999999" label="持仓本金金额(元)" style="width:100%;" labelStyle = "width:110px;" /></td>
		</tr>
		<tfoot>
			<tr>
				<td><input id="osaleCno" name="osaleCno" class="mini-buttonedit" onbuttonclick="onInsQuery" vtype="maxLength:32" allowInput="false" labelField="true"   label="客户编号(交易方)" style="width:100%;" labelStyle = "width:110px;" required='true'/></td>
				<td><input id="osalePartyName" name="osalePartyName" class="mini-textbox"   labelField="true"  enabled="false" label="客户名称" style="width:100%;" labelStyle = "width:110px;" /></td>
				<td><input id="osaleVdate" name="osaleVdate" class="mini-datepicker" onValuechanged="osaleVdateChanged()" required='true' labelField="true"   label="卖断起息日" style="width:100%;" labelStyle = "width:110px;" /></td>
			</tr>
			<tr>
				<td><input id="osaleAmt" name="osaleAmt" class="mini-spinner"  changeOnMousewheel="false" maxValue="9999999999999" enabled="false" labelField="true"  format="n2" label="卖断金额(元)" style="width:100%;" labelStyle = "width:110px;" /></td>
				<td><input id="osaleRate" name="osaleRate" class="mini-spinner" onValuechanged="osaleRateChanged()" changeOnMousewheel="false"  labelField="true"  format="p4"  label="卖断利率" style="width:100%;" labelStyle = "width:110px;" /></td>
				<td><input id="osaleSrate" name="osaleSrate" class="mini-spinner"  changeOnMousewheel="false"  labelField="true"  format="p4" enabled="false" label="卖断利差" style="width:100%;" labelStyle = "width:110px;" /></td>
			</tr>
			<tr>
					<td><input id="accruedTint" name="accruedTint" class="mini-spinner"  changeOnMousewheel="false" maxValue="100000000000"  labelField="true"  format="n2" label="应计利息累计致当前日金额(元)" enabled="false" style="width:100%;" labelStyle = "width:110px;" /></td>
				<td><input id="osaleIamt" name="osaleIamt" class="mini-spinner"  changeOnMousewheel="false" maxValue="9999999999999"  labelField="true"  format="n2" label="卖断应收利息(元)" enabled="false" style="width:100%;" labelStyle = "width:110px;" /></td>
			</tr>
			<tr>
				<td colspan="3"><input id="resReason" name="resReason" required="true"  vtype="maxLength:100" class="mini-textarea"  labelField="true"  label="交易卖断备注" style="width:100%;" labelStyle = "width:110px;" /></td>
			</tr>
		</tfoot>
	</table>
</div>
</fieldset>
<table class="mini-form" width="100%" cols="4">
	<tr>
		<td colspan="4" style="font-size:16px;border-bottom: 2px solid #B3DE94;border-top: 2px solid #B3DE94;">
			&nbsp;<span><font color="red">卖断交易说明:</font></span><BR/>
			&nbsp;<span>"卖断金额"必须是持仓余额全额卖断</span><BR/>
			&nbsp;<span>卖断后还本计划、收息计划、中收计划全部截止</span><BR/>
			&nbsp;<span>卖断后需要从来账分拣里面确认真实卖断利息</span><BR/>
		</td>
	</tr>
</table>
<%@ include file="../mini_base/mini_flow/MiniApproveOpCommon.jsp"%>
<div  class="mini-toolbar" style="text-align:left" >
	<a onclick="save()" id="save_btn" class="mini-button" style="display: none"   >保存</a>
	<a onclick="colse()" id="close_btn" class="mini-button" style="display: none"   >关闭</a>
</div>
<script type="text/javascript">
	mini.parse();
	//获取当前tab
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var row=params.selectData;
	var url=window.location.search;
	var action=CommonUtil.getParam(url,"action");
	
	// var row=top["tradeOutRightSaleManage"].getData(action);
	var osaleIamt='';//卖断应收利息
	var _bizDate = '<%=__bizDate %>';
	var form=new mini.Form("#field_form");
	var tradeData={};
	tradeData.selectData=row;
	tradeData.operType=action;
	tradeData.serial_no=row.dealNo;
	tradeData.task_id=row.taskId;
	//查询客户
	function onInsQuery(){
		var btnEdit = this;
		mini.open({
			url : CommonUtil.baseWebPath() +"/mini_cp/MiniCoreCustQueryEdits.jsp",
			title : "客户选择",
			width : 900,
			height : 600,
			ondestroy : function(action) {	
				if (action == "ok") 
				{
					var iframe = this.getIFrameEl();
					var data = iframe.contentWindow.GetData();
					data = mini.clone(data); //必须
					if (data) {
						if(data.party_id){
							data.cifNo=data.party_id;
						}
						if(data.party_name){
							data.custName=data.party_name;
						}
						var cName =mini.get("cName").getValue();
						if(cName == data.custName) {
							mini.alert("转让交易对手不能和原交易对手相同！","提示");
						}else{
							btnEdit.setValue(data.cifNo);
							btnEdit.setText(data.cifNo);
							mini.get("osalePartyName").setText(data.custName);
							mini.get("osalePartyName").setValue(data.custName);
							btnEdit.setIsValid(true);
							btnEdit.focus();
						}
						
					}
				}

			}
		});
	}
	//加载信息
	function initform(){
		if($.inArray(action,["edit","approve","detail"])>-1){
			$("#labell").html("<a href='javascript:CommonUtil.activeTab();'>资产卖断</a> >> 详情");
			var params={};
			params['dealNo']=row.refNo;
			//params['version']=row.version;
			CommonUtil.ajax({
				url:"/ProductApproveController/searchProductApproveForDuration",
				data:mini.encode(params),
				callback:function(data){
					form.setData(row);
					mini.get("refNo").setValue(data.obj.dealNo);
					mini.get("cName").setValue(data.obj.counterParty.party_name);
					mini.get("cNo").setValue(data.obj.counterParty.party_id);
					mini.get("osaleCno").setValue(data.obj.osaleCno);
					mini.get("osaleCno").setText(data.obj.osaleCno);
					mini.get("osalePartyName").setValue(data.obj.osalePartyName);
					mini.get("prdName").setValue(data.obj.product.prdName);
					mini.get("originalAmt").setValue(data.obj.amt);
					mini.get("rateType").setValue(data.obj.rateType);
					mini.get("vDate").setValue(data.obj.vDate);
					mini.get("orgiMdate").setValue(data.obj.mDate);
					mini.get("sponsor").setValue(data.obj.user.userId);
					mini.get("sponInst").setValue(data.obj.institution.instId);
					mini.get("sponsorName").setValue(data.obj.user.userName);
					mini.get("sponInstName").setValue(data.obj.institution.instName);
					mini.get("version").setValue(data.obj.version);
					mini.get("orgiRate").setValue(data.obj.contractRate);
					mini.get("basis").setValue(data.obj.basis);
					mini.get("mInt").setValue(data.obj.mInt);
					mini.get("mAmt").setValue(data.obj.mAmt);
					mini.get("term").setValue(data.obj.occupTerm);
					osaleIamt=data.obj.tpos.accruedTint;
					mini.get("accruedTint").setValue(data.obj.tpos.accruedTint);
				    mini.get("osaleCno").setValue(row.osaleCno);
					mini.get("osaleCno").setText(row.osaleCno);
					mini.get("osalePartyName").setValue(row.osalePartyName);
					mini.get("osaleVdate").setValue(row.osaleVdate);
					mini.get("osaleRate").setValue(row.osaleRate);
					mini.get("osaleSrate").setValue(row.osaleSrate);
					mini.get("osaleIamt").setValue(row.osaleIamt);
					mini.get("dealDate").setValue(_bizDate);
					mini.get("resReason").setValue(row.resReason);
				}
			});
		}else {
			$("#labell").html("<a href='javascript:CommonUtil.activeTab();'>资产卖断</a> >> 新增");
			var data={dealNo:row.dealNo};
			CommonUtil.ajax({
			url:"/ProductApproveController/searchProductApproveForDuration",
			data:mini.encode(data),
			callback : function(data) {
				form.setData(data.obj);
				mini.get("dealDate").setValue( _bizDate);
				mini.get("dealNo").setValue('');
				mini.get("refNo").setValue( data.obj.dealNo);
				mini.get("cName").setValue( data.obj.counterParty.party_name);
				mini.get("cNo").setValue(data.obj.counterParty.party_id);
				mini.get("prdName").setValue( data.obj.product.prdName);
				mini.get("sponsorName").setValue( data.obj.user.userName);
				mini.get("sponInstName").setValue( data.obj.institution.instName);
				mini.get("version").setValue(data.obj.version);
				mini.get("orgiRate").setValue(data.obj.contractRate);
				mini.get("osaleRate").setValue(data.obj.contractRate);
				mini.get("osaleSrate").setValue(0.00);
				mini.get("osaleIamt").setValue(data.obj.tpos.accruedTint);
				osaleIamt=data.obj.tpos.accruedTint;
				mini.get("accruedTint").setValue(data.obj.tpos.accruedTint);
				mini.get("originalAmt").setValue(data.obj.amt);
				mini.get("osaleVdate").setValue( _bizDate);
				mini.get("orgiMdate").setValue( data.obj.mDate);
				mini.get("remainAmt").setValue(row.settlAmt);
				mini.get("osaleAmt").setValue(row.settlAmt);

				mini.get("sponsor").setValue("<%=__sessionUser.getUserId()%>");
				mini.get("sponInst").setValue("<%=__sessionUser.getInstId() %>");
				mini.get("aDate").setValue("<%=__bizDate %>");
			}
		});
		}
	}

	//监听卖断利率 || 计算卖断利差
	
	function osaleRateChanged(){
		var osaleRate =mini.get("osaleRate").getValue();//卖断利率
		var orgiRate =mini.get("orgiRate").getValue();//原交易利率
		if(osaleRate-orgiRate>0){
			mini.alert("卖断利率必须小于原交易利率","系统提示",function(){
				mini.get("osaleSrate").setValue(orgiRate);
				mini.get("osaleIamt").setValue(0);
				mini.get("osaleRate").setValue(0);
				osaleIamtTotel();//计算总值
			});
			return;
		}
		var osaleSrate =orgiRate-osaleRate;//卖断利差
		mini.get("osaleSrate").setValue(osaleSrate);
		osaleIamtTotel();//计算总值
	}
    //总值计算 --> 这里获取  利息计提差额(利息计提差额 = 计提差额 - 通道费用[计算计提差额这段时间内的参与计算的通道费用])

	function osaleIamtTotel() {
		var osaleVdateFormValue = mini.get("osaleVdate").getFormValue();
			CommonUtil.ajax({
				url: "/DurationController/getAmInt",
				data: mini.encode({ "refNo": mini.get("refNo").getValue(), "amDate": osaleVdateFormValue, "isFees": "true" }),
				callback: function (data) {
					var tongdao = data.obj;
					maiduan(tongdao);
				}
			});
		}
	//计算卖断应收利息
	function maiduan(tongdao){
		var osaleRate =mini.get("osaleRate").getValue();//卖断利率
		var orgiRate =mini.get("orgiRate").getValue();//原交易利率
		var osaleSrate =orgiRate-osaleRate;//卖断利差
		var settlAmtValue=mini.get("remainAmt").getValue();//持仓本金
		var dateValue = ((mini.get("orgiMdate").getValue() - mini.get("osaleVdate").getValue())/(24*60*60*1000));
		var basisValue=mini.get("basis").getValue();
	    var basisValueSub=basisValue.substring(7,10);
		//                   （ 持仓本金       * 卖断利差   *  日期差值            / 计息基础 ） +   卖断应收利息 + 利息计提差额(利息计提差额 = 计提差额 - 通道费用[计算计提差额这段时间内的参与计算的通道费用])
		var osaleIamtValue =  osaleIamt + (tongdao);
		if(osaleSrate !=0 && dateValue !=0 && basisValueSub !=0){
			osaleIamtValue += (settlAmtValue * osaleSrate * Math.abs(dateValue))/basisValueSub;
		}
		mini.get("osaleIamt").setValue(osaleIamtValue);
		
	}
	
	//监控卖断起息日 
	function osaleVdateChanged() {
			var osaleVdate = mini.get("osaleVdate").getValue();//卖断起息日
			var orgiMdate = mini.get("orgiMdate").getValue();//到期日期
			if (orgiMdate - osaleVdate < 0) {
				mini.alert("卖断起息日不能大于到期日期", "系统提示", function () {
					mini.get("osaleVdate").setValue(_bizDate);
					osaleIamtTotel();//计算总值
				});
				return;
			}
			var time_bizDate = (new Date(_bizDate)).getTime();
			var osaleVdateFormValue = mini.get("osaleVdate").getFormValue();
			if (!CommonUtil.dateCompare(osaleVdateFormValue, _bizDate)) {
				mini.alert("卖断起息日必须大于等于当前账务日期", "系统提示", function () {
					mini.get("osaleVdate").setValue(_bizDate);
					osaleIamtTotel();//计算总值
				});
				return;
			}
			osaleIamtTotel();//计算总值
		}
	//保存信息
	function save(){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统也提示");
			return;
		}

		var osaleVdate=mini.get("osaleVdate").getValue();//卖断起息日
		var orgiMdate=mini.get("orgiMdate").getValue();//到期日期
		if(orgiMdate-osaleVdate<0){
           mini.alert("卖断起息日不能大于到期日期","系统提示");
		   return;
		}
		var osaleVdateFormValue =mini.get("osaleVdate").getFormValue();
		if(!CommonUtil.dateCompare(osaleVdateFormValue,_bizDate)){
		mini.alert("卖断起息日必须大于等于当前账务日期","系统提示");
		return;
		}

		var osaleRate =mini.get("osaleRate").getValue();//卖断利率
		var orgiRate =mini.get("orgiRate").getValue();//原交易利率
		if(osaleRate-orgiRate>=0){
			mini.alert("卖断利率必须小于原交易利率","系统提示",function(){
				//mini.get("osaleRate").setValue();
			});
			return;
		}

		var osaleCno =mini.get("osaleCno").getValue();//客户名称
		var cNo = mini.get("cNo").getValue();//交易对手名称
		if(osaleCno && cNo){
			if(osaleCno == cNo){
				mini.alert("交易对手不能跟客户一样","系统提示");
				return;
			}

		}
		var messageid = mini.loading("保存中, 请稍等 ...", "Loading");
		var data=form.getData(true,false);
		data['trdtype']="3001_ORSALE";
		data['osaleType']="S";

		//var remainAmt =isNaN(parseFloat(mini.get("remainAmt").getValue()))==true?0:parseFloat(mini.get("remainAmt").getValue());
		var osaleAmt =isNaN(parseFloat(mini.get("osaleAmt").getValue()))==true?0:parseFloat(mini.get("osaleAmt").getValue());
		var rate = isNaN(parseFloat(mini.get("osaleRate").getValue()))==true?0:parseFloat(mini.get("osaleRate").getValue());
		if(rate<=0){
			mini.alert("利率不能小于零!","系统提示");
			mini.hideMessageBox(messageid);
			return false;
		}
		var saveUrl = "/ApproveManageController/saveApprove";
		if(row.dealType=="2"){
			saveUrl = "/TradeManageController/saveTrade";
		}
		var params=mini.encode(data);
		CommonUtil.ajax({
			url:saveUrl,
			data:params,
			callback:function(data){
				mini.hideMessageBox(messageid);
				mini.alert("保存成功","提示",function(){
					setTimeout(function(){top["win"].closeMenuTab()},10);
				});
			}
		});
	}
	//起息日变更
	function osaleVdatechange(e){
		var formData=form.getData(true);
		var newData=formData.osaleVdate;
		var dDate='<%=__bizDate %>';
		var orgiMdate = formData.orgiMdate; 
		var refNo =mini.get("refNo").getValue();
		if(refNo == null) {
			return false;
		}
		if(CommonUtil.dateDiff(dDate,newData) < 0 && action != "detail"){
			mini.alert("卖断交易起息日期不能小于当前账务日期!","提示");
			return false;
		}
		if(CommonUtil.dateDiff(newData,orgiMdate) < 0 && action != "detail"){
			mini.alert("卖断交易起息日期不能大于到期日期!","提示");
			return false;
		}
	}
	//卖断利率变更
	function osaleRatechange(e){
		var newValue=e.value;
		var orgiRate =mini.get("orgiRate").getValue();
		
		if(orgiRate<=newValue){
			mini.alert("转让利率必须小于原交易利率","提示");
			mini.get("osaleRate").setValue(0);
			return false;
		}
		
		//转让利差(%)
		var osaleSrate = orgiRate - newValue;
		mini.get("osaleSrate").setValue(osaleSrate);
	}
	//关闭表单
	function colse(){
		top["win"].closeMenuTab();
	}
	$(document).ready(function(){
		if($.inArray(action,["approve","detail"])>-1){
			mini.get("save_btn").setVisible(false);
			form.setEnabled(false);
		}
		initform();
	});
</script>
<script type="text/javascript" src="<%=basePath%>/standard/InterBank/mini_base/mini_flow/MiniApproveOpCommon.js"></script>
</body>
</html>
