<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
	<legend>结息确认查询</legend>
<div id="InterestSettleConfirm" >   
	<div id="search_form" class="mini-form" width="80%">
		<input id="dealNo" name="dealNo" class="mini-textbox" vtype="maxLength:50" labelField="true"  label="交易单号：" labelStyle="text-align:right;" emptyText="请输入交易单号"/>
		<input id="refNo" name="refNo" class="mini-textbox" vtype="maxLength:50" labelField="true"  label="原交易单号：" labelStyle="text-align:right;" emptyText="请输入原交易单号"/>
		<input id="dealType" name="dealType" class="mini-combobox" vtype="maxLength:50" labelField="true"  label="审批类型：" labelStyle="text-align:right;" data = "CommonUtil.serverData.dictionary.DealType" emptyText="请选择审批类型"/>
		<span style="float:right;margin-right: 150px">
			<a  id="search_btn" class="mini-button" style="display: none"   onclick="search(10,0)">查询</a>
			<a  id="clear_btn" class="mini-button" style="display: none"   onclick='clear()'>清空</a>
		</span>
	</div>
</div>
</fieldset>
<span style="margin:2px;display: block;">
	<a  id="add_btn" class="mini-button" style="display: none"   onclick="add()">新增</a>
	<a  id="edit_btn" class="mini-button" style="display: none"   onclick="edit()">修改</a>
	<a  id="delete_btn" class="mini-button" style="display: none"   onclick="del()">删除</a>
	<a  id="approve_commit_btn" class="mini-button" style="display: none"    onclick="approve()">审批</a>
	<a  id="verify_mine_commit_btn" class="mini-button" style="display: none"    onclick="commit()">提交审批</a>
	<a  id="approve_btn" class="mini-button" style="display: none"     onclick="searchlog()">审批日志</a>
	<a  id="print_btn" class="mini-button" style="display: none"    onclick="print()">打印</a>
	<div id = "approveType" name = "approveType" class="mini-checkboxlist" style="float:right;" labelField="true" label="审批列表选择：" labelStyle="text-align:right;" 
				value="approve" textField="text" valueField="id" multiSelect="false"
				data="[{id:'approve',text:'待审批列表'},{id:'finished',text:'已审批列表'},{id:'mine',text:'我发起的'}]" onvaluechanged="checkBoxValuechanged">
	</div>
</span>
<div id="InterestSettleConfirmFit" class="mini-fit">
		<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" 
		idField="dealNo" allowResize="true"  border="true" onrowdblclick="onRowDblClick">
			<div property="columns" >
				<div type="indexcolumn" headerAlign="center" width="40">序号</div>
				<div field="dealNo" width="180" align="center"  headerAlign="center" allowSort="true">交易单号</div>    
				<div field="refNo" width="200" headerAlign="center" allowSort="true">原交易单号</div>                            
				<div field="party.party_name" width="120"   headerAlign="center" allowSort="true" >客户</div>
				<div field="product.prdName" width="120"   headerAlign="center" allowSort="true" >产品种类</div>
				<div field="dealType" width="120"   headerAlign="center" allowSort="true" renderer="CommonUtil.dictRenderer" data-options="{dict:'DealType'}">审批类型</div>
				<div field="approveStatus" width="120"   headerAlign="center" allowSort="true" renderer="CommonUtil.dictRenderer" data-options="{dict:'ApproveStatus'}">审批状态</div>
				<div field="interestSettlAmt" width="120"   headerAlign="center" align="right" allowSort="true" numberFormat="#,0.00">自动结息金额</div>
				<div field="settlInt" width="120"   headerAlign="center" align="right" numberFormat="#,0.00" allowSort="true" >实际结息金额</div>
				<div field="amReason" width="120"   headerAlign="center" allowSort="true" >结息备注</div>                          
			</div>
		</div>
</div>
<script>
mini.parse();

var grid = mini.get("datagrid");
var approveType = mini.get("approveType");
var offsetDate ="<%=__bizDate %>";
var form  =new mini.Form("search_form");
$(document).ready(function(){
	//控制按钮显示
	$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
		grid.on("beforeload", function (e) {
			e.cancel = true;
			var pageIndex = e.data.pageIndex;
			var pageSize = e.data.pageSize;
			search(pageSize, pageIndex);
		});
		checkBoxValuechanged();
	});
})

//查询
function search(pageSize,pageIndex){
	form.validate();
	if (form.isValid() == false) {
		mini.alert("信息填写有误，请重新填写", "系统也提示");
		return;
	}
	var data = form.getData();
	data['pageNumber'] = pageIndex + 1;
	data['pageSize'] = pageSize;
	var url = null;
	var approveType = mini.get("approveType").getValue();
	if (approveType == "mine") {
		url = "/DurationController/searchPageInterestSettleMySelf";
	} else if (approveType == "approve") {
		url = "/DurationController/searchPageInterestSettleUnfinished";
	} else {
		url = "/DurationController/searchPageInterestSettleFinished";
	}

	var params = mini.encode(data);
		CommonUtil.ajax({
			url:url,
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});

}

//新增
function add() {
	mini.open({
		url: CommonUtil.baseWebPath() + "/mini_duration/MiniDurationTradeManage.jsp?trdtype=3001_InterSettle",
		title: "交易选择",
		width: 850,
		height: 500,
		ondestroy: function (action) {
			if (action == "ok") {
				var iframe = this.getIFrameEl();
				var data = iframe.contentWindow.GetData();
				data = mini.clone(data); //必须
				var row = data;
				var url = CommonUtil.baseWebPath() + "/mini_duration/MiniInterestSettleConfirmEdit.jsp?action=add";
				var tab = { id: "InterestSettleComfirmAdd", name: "InterestSettleComfirmAdd", title: "结息确认新增", url: url, parentId: top["win"].tabs.getActiveTab().name, showCloseButton: true };
				var paramData = { selectData: row };
				CommonUtil.openNewMenuTab(tab, paramData);
			}
		}
	});
}
//修改
function edit(){
	var record =grid.getSelected();
	if(!record){
		mini.alert("请选择一条数据");
		return;
	}
	if(record.approveStatus!=3){
		mini.alert("新建状态才能修改","系统提示");
		return;
	}
	
	var url = CommonUtil.baseWebPath() + "/mini_duration/MiniInterestSettleConfirmEdit.jsp?action=edit";
	var tab = { "id": "InterestSettleConfirmEdit", name: "InterestSettleConfirmEdit", url: url, title: "结息确认修改", parentId: top["win"].tabs.getActiveTab().name, showCloseButton: true };
	var paramData = { selectData: record };
	CommonUtil.openNewMenuTab(tab, paramData);
}
//删除
function del(){
	var record = grid.getSelected();
	if(!record){
		mini.alert("请选中一行数据","提示");
		return;
	}

	if(record.approveStatus !='3'){
		mini.alert("只有新建状态才能删除","系统提示");return;
	}
	mini.confirm("您确认要删除选中记录?","系统警告",function(value){   
		if (value=='ok'){   
			params=mini.encode({"dealNo":record.dealNo});
			CommonUtil.ajax( {
				url:"/DurationController/removeInterestSettle",
				data:params,
				callback : function(data) {
					mini.alert("删除成功.","系统提示",function(){
						search(10,0);
					});
				}
			});
		}
	});
}


//详情
//查看详情
function onRowDblClick() {
	var row = grid.getSelected();
	var url = CommonUtil.baseWebPath() + "/mini_duration/MiniInterestSettleConfirmEdit.jsp?action=detail";
	var tab = { "id": "MiniInterestSettleConfirmDetail", name: "MiniInterestSettleConfirmDetail", url: url, title: "结息确认详情", parentId: top["win"].tabs.getActiveTab().name, showCloseButton: true };
	var paramData = { selectData: row };
	CommonUtil.openNewMenuTab(tab, paramData);
}
//提交审批
function commit() {
	if (approveType.getValue() == 'mine') {
		var record = grid.getSelected();
		if (record) {
			if (record.approveStatus == '3') {
				Approve.approveCommit(Approve.FlowType.VerifyApproveFlow, record.dealNo, Approve.OrderStatus.New,
					function () { checkBoxValuechanged(); }, null, '');
			} else {
				mini.alert("新建状态才能提交审批", "系统提示");
			}
		} else {
			mini.alert("请选择一条数据", "温馨提示");
		}
	}
}
//审批
function approve(){
	var approveType = mini.get("approveType").getValue();
	if(approveType =='approve'){
		var record = grid.getSelected();
		if(record){
			if(record.approveStatus !='3'){
				var url = CommonUtil.baseWebPath() + "/mini_duration/MiniInterestSettleConfirmEdit.jsp?action=approve";
				var tab = { id: "MiniInterestSettleConfirmApprove", name: "MiniInterestSettleConfirmApprove", title: "结息确认审批", url: url, parentId: top["win"].tabs.getActiveTab().name, showCloseButton: true };
				var paramData = { selectData: record };
				CommonUtil.openNewMenuTab(tab, paramData);
			}
		}else{
			mini.alert("请选择一条要审批的记录","系统提示");
		}
	}
	
}

//查看日志
function searchlog(){
	var record=grid.getSelected();
    if(record){
		if(record.dealType == "1"){//业务审批流程
			flow_type = "1";
		}else if(record.dealType == "2"){//4:核实流程
			flow_type = "4";
		}
        Approve.approveLog(flow_type,record.dealNo);
    }else{
        mini.alert("请选择一条数据","系统提示");
    }
}
//按钮控制
function checkBoxValuechanged(){
	$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
		search(10, 0);
		var approveType = mini.get("approveType").getValue();
		if (approveType == "approve") {
			if (visibleBtn.add_btn) mini.get("add_btn").setVisible(false);
			if (visibleBtn.edit_btn) mini.get("edit_btn").setVisible(false);
			if (visibleBtn.delete_btn) mini.get("delete_btn").setVisible(false);
			if (visibleBtn.approve_commit_btn) mini.get("approve_commit_btn").setVisible(true);
			if (visibleBtn.verify_mine_commit_btn) mini.get("verify_mine_commit_btn").setVisible(false);
		} else if (approveType == "finished") {
			if (visibleBtn.add_btn) mini.get("add_btn").setVisible(false);
			if (visibleBtn.edit_btn) mini.get("edit_btn").setVisible(false);
			if (visibleBtn.delete_btn) mini.get("delete_btn").setVisible(false);
			if (visibleBtn.approve_commit_btn) mini.get("approve_commit_btn").setVisible(false);
			if (visibleBtn.verify_mine_commit_btn) mini.get("verify_mine_commit_btn").setVisible(false);
		} else {
			if (visibleBtn.add_btn) mini.get("add_btn").setVisible(true);
			if (visibleBtn.edit_btn) mini.get("edit_btn").setVisible(true);
			if (visibleBtn.delete_btn) mini.get("delete_btn").setVisible(true);
			if (visibleBtn.approve_commit_btn) mini.get("approve_commit_btn").setVisible(false);
			if (visibleBtn.verify_mine_commit_btn) mini.get("verify_mine_commit_btn").setVisible(true);
		}
	});
	}
//清空	
function clear(){
	form.clear();
}	
//打印
function print() {
	var record = grid.getSelected();
	if (record) {
		var canPrint = record.dealNo;
		if (!CommonUtil.isNull(canPrint)) {
			var actionStr = CommonUtil.pPath + "/sl/PrintController/exportDownload/print/311/" + canPrint;
			$('<form target="_blank" action=' + actionStr + ' method="post"></form>').appendTo('body').submit().remove();
		}
	} else {
		mini.alert("请选中一行数据","提示");
		return;
	}
}
</script>
</body>	