<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title></title>
  </head>
  
<body style="width:100%;height:100%;background:white">
		<span id="labell"><a href="javascript:CommonUtil.activeTab();">资产到期</a> >> 修改</span>
<fieldset class="mini-fieldset">
	<legend>交易详情</legend>
<div id="TradeExpireEdit">
	<table id="field_form" class="mini-table" style="width:100%">
		<tr>
			<td><input id="refNo" name="refNo" class="mini-textbox"  labelField="true" enabled="false"  label="原交易单号" style="width:100%;" labelStyle = "width:110px;" /></td>
			<td><input id="cName" name="cName" class="mini-textbox"  labelField="true" enabled="false" label="客户" style="width:100%;" labelStyle = "width:110px;" />
			<input class="mini-hidden" id="dealNo" name="dealNo"  >
			<input class="mini-hidden" id="cNo" name="cNo">
			<input class="mini-hidden" id="version" name="version"></td>
			<td><input id="prdName" name="prdName" class="mini-textbox"  labelField="true" enabled="false" label="产品名称" style="width:100%;" labelStyle = "width:110px;" /></td>
		</tr>
		<tr>
			<td><input id="originalAmt" name="originalAmt" class="mini-spinner"  changeOnMousewheel='false'    labelField="true"  maxValue="999999999999"  format="n2" enabled="false" label="原交易本金(元)" style="width:100%;" labelStyle = "width:110px;" /></td>
			<td><input id="sponsorName" name="sponsorName" class="mini-textbox"  labelField="true" enabled="false" label="原交易发起人" style="width:100%;" labelStyle = "width:110px;" />
			<input id="sponsor" name="sponsor" class="mini-hidden" /></td>
			<td><input id="sponInstName" name="sponInstName" class="mini-textbox"  labelField="true" enabled="false" label="原交易发起机构" style="width:100%;" labelStyle = "width:110px;" />
			<input id="sponInst" name="sponInst" class="mini-hidden" /></td>
		</tr>
		<tr>
			<td><input id="rateType" name="rateType" class="mini-combobox"  labelField="true" data="CommonUtil.serverData.dictionary.rateTypeFund"  enabled="false" label="利率类型" style="width:100%;" labelStyle = "width:110px;" /></td>
			<td><input id="acturalRate" name="acturalRate" class="mini-spinner"  changeOnMousewheel='false'    labelField="true"  format="p4" enabled="false" label="原交易利率" style="width:100%;" labelStyle = "width:110px;" /></td>
			<td><input id="basis" name="basis" class="mini-combobox" onvaluechanged="basisChanged" labelField="true" data="CommonUtil.serverData.dictionary.DayCounter"  enabled="false" label="计息基础" style="width:100%;" labelStyle = "width:110px;" /></td>
		</tr>
		<tr>
			<td><input id="remainAmt" name="remainAmt" class="mini-spinner"  changeOnMousewheel='false'    labelField="true"  maxValue="999999999999"  format="n2" enabled="false" label="持仓本金金额(元)" style="width:100%;" labelStyle = "width:110px;" /></td>
			<td><input id="vDate" name="vDate" class="mini-datepicker"  labelField="true"   enabled="false" label="起息日期" style="width:100%;" labelStyle = "width:110px;" /></td>
			<td><input id="tDate" name="tDate" class="mini-datepicker"  labelField="true"   enabled="false" label="划款日期" style="width:100%;" labelStyle = "width:110px;" /></td>
		</tr>
		<tfoot>
			<tr>
				<td><input id="expMDate" name="expMDate" class="mini-datepicker" onvaluechanged="getExpTerm" labelField="true"  label="到期日期" style="width:100%;" labelStyle = "width:110px;" /></td>
				<td><input id="expMeDate" name="expMeDate" class="mini-datepicker" onvaluechanged="expMeDateChanged" labelField="true"  label="到帐日期" style="width:100%;" labelStyle = "width:110px;" /></td>
				<td><input id="expTerm" name="expTerm" class="mini-textbox"  labelField="true" enabled="false" label="计息天数(天)" style="width:100%;" labelStyle = "width:110px;" /></td>
			</tr>
			<tr>
				<td><input id="expOccupTerm" name="expOccupTerm" class="mini-textbox" onvaluechanged="expOccupTermChanged" labelField="true" enabled="false" label="占款期限(天)" style="width:100%;" labelStyle = "width:110px;" /></td>
				<td><input id="expMInt" name="expMInt" class="mini-spinner"  changeOnMousewheel='false'   onvaluechanged="expMIntChanged" labelField="true" minValue="0" maxValue="999999999999"  required="true"   format="n2" label="到期利息(元)" style="width:100%;" labelStyle = "width:110px;" /></td>
				<td><input id="expMAmt" name="expMAmt" class="mini-spinner"  changeOnMousewheel='false'   onvaluechanged="expMAmtChanged" labelField="true" format="n2" maxValue="999999999999" enabled="false" label="到期金额(元)" style="width:100%;" labelStyle = "width:110px;" /></td>
			</tr>
			<tr>
				<td><input id="expRate" name="expRate" class="mini-spinner"  changeOnMousewheel='false'    labelField="true"  format="p4" required="true"  minValue="0" maxValue="999999999999" enabled="false" label="持有期利率" style="width:100%;" labelStyle = "width:110px;" /></td>
				<td><input id="aDate" name="aDate" class="mini-hidden"  labelField="true" enabled="false" value="_bizDate" label="交易日期" style="width:100%;" labelStyle = "width:110px;" /></td>
				<td></td>
			</tr>
			<tr>
				<td colspan="3"><input id="reason" name="reason" class="mini-textarea" required ='true' labelField="true"  vtype="maxLength:100"  label="资产到期备注" style="width:100%;" labelStyle = "width:110px;" /></td>
			</tr>
		</tfoot>
	</table>
</div>
</fieldset>
<table class="mini-form" width="100%" cols="4">
	<tr>
		<td colspan="4" style="font-size:16px;border-bottom: 2px solid #B3DE94;border-top: 2px solid #B3DE94;">
			&nbsp;<span><font color="#0065d2">资产到期说明:</font></span><BR/>
			&nbsp;<span>交易到期时业务人员通过到期资产维护功能录入：到账日期、 计息天数、占款期限、到期利息(元)、
				到期金额(元)字段</span><BR/>
			&nbsp;<span>系统根据(划款日期 、到账日期、本金、计息基础)自动反算持有期利率。</span><BR/>
		</td>
	</tr>
</table>
<%@ include file="../mini_base/mini_flow/MiniApproveOpCommon.jsp"%>
<div  class="mini-toolbar" style="text-align:left" >
	<a onclick="save()" id="save_btn" class="mini-button" style="display: none"   >保存</a>
	<a onclick="colse()" id="close_btn" class="mini-button" style="display: none"   >关闭</a>
</div>
<script type="text/javascript">
	mini.parse();
	//获取当前tab
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var row=params.selectData;
	var url=window.location.search;
	var _bizDate = '<%=__bizDate %>';
	var action=CommonUtil.getParam(url,"action");
	// var row=top["tradeExpireManage"].getData(action);
	var form=new mini.Form("#field_form");
	var tradeData={};
	tradeData.selectData=row;
	tradeData.operType=action;
	tradeData.serial_no=row.dealNo;
	tradeData.task_id=row.taskId;
	//加载信息
	function initform(){
		if($.inArray(action,["edit","approve","detail"])>-1){
			$("#labell").html("<a href='javascript:CommonUtil.activeTab();'>资产到期</a> >> 详情");
			var params={};
			params['dealNo']=row.refNo;
			params['version']=row.version;
			CommonUtil.ajax({
				url:"/DurationController/getTdProductApproveMain",
				data:mini.encode(params),
				callback:function(data){
					form.setData(row);
					mini.get("refNo").setValue( data.dealNo);
					mini.get("cName").setValue( data.counterParty.party_name);
					mini.get("cNo").setValue(data.counterParty.party_id);
					mini.get("prdName").setValue( data.product.prdName);
					mini.get("originalAmt").setValue( data.amt);
					mini.get("rateType").setValue( data.rateType);
					mini.get("vDate").setValue( data.vDate);
					mini.get("tDate").setValue( data.tDate);
					mini.get("acturalRate").setValue( data.contractRate);
					mini.get("sponsor").setValue( data.user.userId);
					mini.get("sponInst").setValue(data.institution.instId);
					mini.get("sponsorName").setValue( data.user.userName);
					mini.get("sponInstName").setValue( data.institution.instName);
					mini.get("version").setValue(data.version);
					mini.get("basis").setValue(data.basis);
					mini.get("dealNo").setValue(row.dealNo);
					CommonUtil.ajax({
						url: "/trdTposController/getSettlAmt",
						data: mini.encode({"i_code":row.refNo}),
						callback: function (data) {
							if(data.obj[0]){
								var settlAmt = data.obj[0].settlAmt;
								mini.get("remainAmt").setValue(settlAmt);//持仓本金
							}
						 }
					});
				}
			});
		}else{
			$("#labell").html("<a href='javascript:CommonUtil.activeTab();'>资产到期</a> >> 新增");
			var data={dealNo:row.dealNo};
			CommonUtil.ajax({
			url:"/ProductApproveController/searchProductApproveForDuration",
			data:mini.encode(data),
			callback : function(data) {
				form.setData(data.obj);
				mini.get("expMDate").setValue( _bizDate);
				mini.get("expMeDate").setValue( data.obj.tDate);//这里赋值  到账日期 默认赋值为划款日
				mini.get("dealNo").setValue('');
				mini.get("refNo").setValue( data.obj.dealNo);
				mini.get("cName").setValue( data.obj.counterParty.party_name);
				mini.get("cNo").setValue(data.obj.counterParty.party_id);
				mini.get("prdName").setValue( data.obj.product.prdName);
				mini.get("sponsorName").setValue( data.obj.user.userName);
				mini.get("sponInstName").setValue( data.obj.institution.instName);
				mini.get("version").setValue(data.obj.version);
				mini.get("acturalRate").setValue( data.obj.contractRate);
				mini.get("expRate").setValue( data.obj.contractRate);
				mini.get("originalAmt").setValue( data.obj.amt);
				mini.get("remainAmt").setValue(row.settlAmt);
				// mini.get("expMAmt").setValue( row.remainAmt);
				
				mini.get("sponsor").setValue("<%=__sessionUser.getUserId()%>");
				mini.get("sponInst").setValue("<%=__sessionUser.getInstId() %>");
				mini.get("aDate").setValue("<%=__bizDate %>");

				getExpTerm();//计算计息天数
				jisuanExpOccupTerm();
			
			}
		});
		}
	}
	//计算计息天数

	function getExpTerm(){
		var expMDate = mini.get("expMDate").getValue();
		var vDate =mini.get("vDate").getValue();//起息日期
		if(expMDate - vDate < 0){
           mini.alert("到期日期必须大于起息日期","系统提示",function(){
			mini.get("expMDate").setValue(_bizDate);
		   });
		   return;  
		}
		var expMDateFormValue=mini.get("expMDate").getFormValue();
		if(!CommonUtil.dateCompare(expMDateFormValue,_bizDate)){
			mini.alert("到期日期必须大于当前日期","系统提示",function(){
				mini.get("expMDate").setValue(_bizDate);
				jisuanjixi();
			});
		}
		jisuanjixi();
		
	}

	function jisuanjixi(){
		var expMDate = mini.get("expMDate").getValue();
		var vDate =mini.get("vDate").getValue();//起息日期
		if(expMDate && vDate){
			var expTerm =(expMDate - vDate)/(24*60*60*1000);
			mini.get("expTerm").setValue(expTerm);
		}
	}

	/**********************************************************************************/
    //判断到账日期
	function expMeDateChanged(){
		var expMeDate =mini.get("expMeDate").getFormValue();//到账日期
		var tDate = mini.get("tDate").getFormValue();//划款日期
		if(CommonUtil.dateDiff(tDate,expMeDate) < 0){
			mini.alert("到账日期不能小于划款日期","系统提示",function(){
				mini.get("expMeDate").setValue(_bizDate);
				jisuanExpOccupTerm();
				expMIntChanged();
			});
            return;
		}
		jisuanExpOccupTerm();
		expMIntChanged();
	}

	//计算占款期限
	function jisuanExpOccupTerm(){
		var expMeDate =mini.get("expMeDate").getValue();//到账日期
		var tDate = mini.get("tDate").getValue();//划款日期
		if(expMeDate && tDate){
			var expOccupTerm =(expMeDate - tDate)/(24*60*60*1000);
			mini.get("expOccupTerm").setValue(expOccupTerm);
		}
	}
/**********************************************************************************/
	//到期金额
	function expMIntChanged(){
		var originalAmt = mini.get("originalAmt").getValue();//原交易本金
		var expMInt = mini.get("expMInt").getValue();//到期利息
		mini.get("expMAmt").setValue(originalAmt + expMInt );

		//计算持有利率
		var tianshu =mini.get("expOccupTerm").getValue();//天数
		var basis =mini.get("basis").getValue();
		var basisValueSub=basis.substring(7,10);//计息基础
		if(expMInt>0 && tianshu>0){
			var expRate = (expMInt * basisValueSub) / (tianshu * originalAmt);
			mini.get("expRate").setValue(expRate);
		}else{
			mini.get("expRate").setValue();
		}
        
	}
	//保存信息
	function save(){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统也提示");
			return;
		}
		//到账日期 || 划款日期
		var expMeDate =mini.get("expMeDate").getFormValue();//到账日期
		var tDate = mini.get("tDate").getFormValue();//划款日期
		if(CommonUtil.dateDiff(tDate,expMeDate) <=  0){
			mini.alert("到账日期必须大于划款日期","系统提示");
			return;
		}
		//到期日期 || 起息日期
		var expMDate = mini.get("expMDate").getValue();
		var vDate =mini.get("vDate").getValue();//起息日期
		if(expMDate - vDate < 0){
           mini.alert("到期日期必须大于起息日期","系统提示",function(){
			mini.get("expMDate").setValue(_bizDate);
		   });
		   return; 
		} 
		var messageid = mini.loading("保存中, 请稍等 ...", "Loading");
		var data=form.getData(true,false);
		data['trdtype']="3001_TDEXP";
		var saveUrl = "/ApproveManageController/saveApprove";
		if(row.dealType=="2"){
			saveUrl = "/TradeManageController/saveTrade";
		}
		var params=mini.encode(data);
		CommonUtil.ajax({
			url:saveUrl,
			data:params,
			callback:function(data){
				mini.hideMessageBox(messageid);
				mini.alert("保存成功","提示",function(){
					setTimeout(function(){top["win"].closeMenuTab()},10);
				});
			}
		});
	}
	//关闭表单
	function colse(){
		top["win"].closeMenuTab();
	}
	//日期验证
	function expMDateChanged(){
		var param=form.getData(true);
		if(param.expMDate == null || param.expMDate == "") {
			return false;
		}
		if(!CommonUtil.dateCompare(param.expMDate,_bizDate)) {
			mini.alert("到期日期不能小于账务日期！","提示");
			mini.get("expMDate").setValue( _bizDate);
			return false;
		}
		var vDate = param.vDate;
		//计息天数
		var expTerm = CommonUtil.dateDiff(vDate, param.expMDate);
		mini.get("expTerm").setValue(expTerm);
	}

	// function expMeDateChanged(){
	// 	var param=form.getData(true);
	// 	if(param.expMeDate == null || param.expMeDate == "") {
	// 		return false;
	// 	}
	// 	if(!CommonUtil.dateCompare(param.expMeDate,_bizDate)) {
	// 		mini.alert("到账日期不能小于账务日期！","提示");
	// 		mini.get("expMeDate").setValue( _bizDate);
	// 		return false;
	// 	}
	// 	var tDate = mini.get("tDate").getValue();
	// 	//占款天数
	// 	var expOccupTerm = CommonUtil.dateDiff(tDate, param.expMeDate);
	// 	mini.get("expOccupTerm").setValue(expOccupTerm);
	// }

	//占款期限
	function expOccupTermChanged(){
		rateMath();
	}
	function basisChanged(){
		rateMath();
	}
	function expMAmtChanged(){
		rateMath();
	}
	// function expMIntChanged(){
	// 	rateMath();
	// }
	function rateMath(){
		var expOccupTerm = mini.get("expOccupTerm").getValue();
		var basis = mini.get("basis").getValue();
		var expMAmt = mini.get("expMAmt").getValue();
		var expMInt = mini.get("expMInt").getValue();
		
		if(CommonUtil.isNull(expOccupTerm) || CommonUtil.isNull(basis) || CommonUtil.isNull(expMAmt) || CommonUtil.isNull(expMInt)) {
			return false;
		}
		var expRate = expMInt*expOccupTerm/switchBasis(basis)*expMAmt;
		mini.get("expRate").setValue(expRate);
	}
	//获得计息基础天数
	function switchBasis(basis){
		if (basis == "Actual/365") {
			basis = 365;
		} else if (basis == "Actual/Actual(ISMA)") {
			basis = 365;
		} else if (basis == "Actual/365(Fixed)") {
			basis = 365;
		} else if (basis == "Actual/360") {
			basis = 360;
		} else if (basis == "A/360") {
			basis = 360;
		} else if (basis == "A/365") {
			basis = 365;
		}
		return basis;
	}
	$(document).ready(function(){
		if($.inArray(action,["approve","detail"])>-1){
			mini.get("save_btn").setVisible(false);
			form.setEnabled(false);
		}
		initform();
		
	});
</script>
<script type="text/javascript" src="<%=basePath%>/standard/InterBank/mini_base/mini_flow/MiniApproveOpCommon.js"></script>
</body>
</html>
