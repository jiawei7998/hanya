<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
		<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title></title>
  </head>
  
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
	<legend>还本计划调整</legend>
<div id="CapitalSchedule_from_div">
	<table class='mini-table' id="CapitalSchedule" name="CapitalSchedule" style="width:100%">
		<tr>
			<td><input id="cfType" name="cfType" class="mini-combobox" value="Principal"  labelField="true" data="CommonUtil.serverData.dictionary.cfType"  enabled="false" label="现金流类型" style="width:100%;" labelStyle = "width:110px;" /></td>
			<td><input id="payDirection" name="payDirection" class="mini-combobox" value="Recevie"  labelField="true" data="CommonUtil.serverData.dictionary.feeSituation"  enabled="false" label="收付方向" style="width:100%;" labelStyle = "width:110px;" /></td>
			<td><input id="repaymentSdate" name="repaymentSdate" class="mini-datepicker" labelField="true"  required="true"  label="还本日期（计划）" style="width:100%;" labelStyle = "width:110px;" /></td>
		</tr>
		<tr>
			<td><input id="repaymentSamt" name="repaymentSamt" class="mini-spinner"   changeOnMousewheel='false'   labelField="true" required="true"  maxValue="100000000000"  format="n2"   label="还本金额（计划）" style="width:100%;" labelStyle = "width:110px;" /></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td colspan="3" >
				<div  class="mini-toolbar" style="text-align:left" >
					<a  id="cap_add_btn" class="mini-button" style="display: none"    onclick="addcap()">新增</a>
					<a  id="cap_delete_btn" class="mini-button" style="display: none"    onclick="delcap()">删除</a>
				</div>
			</td>
		</tr>
	</table>
</div>
</fieldset>
<fieldset style="min-width:0;">
		<legend>还本计划详情</legend>
<div id="capital_grid" class="mini-datagrid  borderAll" style="width:100%;" mini-fit="true"
	allowResize="true"  border="true" sortMode="client" showPager="false"
	pageSize="100"  multiSelect="true" 
	allowCellEdit="true" allowCellSelect="true" multiSelect="false" editNextOnEnterKey="true"  editNextRowCell="true">

	<div property="columns">
		<div type="indexcolumn"     headerAlign="center"  allowSort="false" width="40" >序号</div>
		<div type="checkcolumn"     headerAlign="center"></div>    
		
		<div field="cfType"         headerAlign= "center" allowSort= "true" width="120" type="comboboxcolumn" align="center">现金流类型
			<input class="mini-combobox" property="editor"
				enabled="false" data="CommonUtil.serverData.dictionary.cfType"  />
		</div> 
		
		<div field="payDirection"        headerAlign= "center" allowSort= "true" width="120" type="comboboxcolumn" align="center">收付方向
			<input class="mini-combobox" property="editor"
				enabled="false"	 data="CommonUtil.serverData.dictionary.feeSituation"  />
		</div> 
		
		<div field="repaymentSdate" headerAlign= "center" allowSort= "true" width="100" dateFormat="yyyy-MM-dd" align="center">还本日期(计划)
			<input class="mini-datepicker"  required="true"  property="editor"/>
		</div> 
		
		<div field="repaymentSamt"  headerAlign= "center" allowSort= "true" width="120" align = "right" dataType="float" numberFormat="#,0.00">还本金额(计划)
			<input class="mini-spinner"   changeOnMousewheel='false'  required="true"  property="editor"
				minValue="0" maxValue="999999999999999" changeOnMousewheel="false"/>
		</div>             
		
		<div field="repaymentTdate" headerAlign= "center" allowSort= "true" width="100" dateFormat="yyyy-MM-dd" align="center">还本日期(实际)
			<input class="mini-datepicker" property="editor" />
		</div>                          
		
		<div field="repaymentTamt"  headerAlign= "center" allowSort= "true" width="120" align = "right" dataType="float" numberFormat="#,0.00" >还本金额(实际)
			<input class="mini-spinner"   changeOnMousewheel='false'  property="editor"
				minValue="0" maxValue="999999999999999" changeOnMousewheel="false"/>
		</div>                              
	</div>
</div>
</fieldset>
<script type="text/javascript">
	mini.parse();
	top["capitalScheduleTab"]=window;
	var url=window.location.search;
	var action=CommonUtil.getParam(url,"action");
	var row=top["tradeExtendMangage"].getData(action);
	var capGrid=mini.get("capital_grid");
	
	//初始化信息
	function initData(){
		if($.inArray(action,["edit","approve","detail"])>-1){
			CommonUtil.ajax( {
				url:"/tmCalCashFlowController/getApproveCapitalForApprove",
				data:mini.encode({dealNo : row.dealNo,
				dealType:row.dealType}),
				callback : function(data) {
					data=mini.decode(data);
					capGrid.setData(data.obj);
				}
			});
		}
	}
	//添加还本计划
	function addcap(){
		var addform = new mini.Form("CapitalSchedule_from_div");
		addform.validate();
		if(addform.isValid() == false){
			mini.alert("交易验证失败,请检查表单是否填写正确!","消息提示")
			return
		};
		var row = addform.getData(true);
		capGrid.addRow(row, capGrid.data.length);
	}
	//删除还本计划
	function delcap(){
		var rows = capGrid.getSelecteds();
		if (rows.length > 0) {
			mini.confirm("确定删除记录？", "确定?",
				function (action) {
					if (action == "ok") 
					{
						capGrid.removeRows(rows, true);
						mini.alert("删除成功!","消息提示");
					} 
				}
			);
		}else{
			mini.alert("请选择要删除的列!","消息提示");
		}
	}
	//获取表格数据
	function getGridData(){
		return capGrid.getData();
	}
	$(document).ready(function(){
		if($.inArray(action,["approve","detail"])>-1){
			mini.get("cap_add_btn").setVisible(false);
			mini.get("cap_delete_btn").setVisible(false);
		}
		initData();
	});
</script>
</body>
</html>
