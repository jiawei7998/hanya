<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="../../global.jsp" %>
<html>
<head>
    <title>分行收息计划</title>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset id="fd1" class="mini-fieldset">
    <legend><label>查询条件</label></legend>
    <div id="search_form" style="width:100%;">

        <input id="dealNo" name="dealNo" class="mini-textbox" labelField="true" label="交易编号："
               labelStyle="text-align:right;" emptyText="请输入交易编号"/>
        <input id="theoryPaymentDate" name="theoryPaymentDate" class="mini-datepicker" width="250" labelField="true"
               label="计息收息日：" labelStyle="text-align:right;" emptyText="请输入收息日">
        <!-- <input id="refBeginDate" name="refBeginDate" class="mini-datepicker" onValuechanged="compareDate"  labelField="true"  label="计息起息日：" labelStyle="text-align:right;"emptyText="请输入计息起息日"/>
            <input id="refEndDate" name="refEndDate" class="mini-datepicker"  onValuechanged="compareDate" labelField="true"  label="计息到期日：" labelStyle="text-align:right;"emptyText="请输入计息到期日"/> -->
        <input id="vDate1" name="vDate1" class="mini-datepicker" labelField="true" label="计息起息日："
               onValuechanged="compareDate" labelStyle="text-align:right;" emptyText="请选择起息开始日期"/>
        <input id="vDate2" name="vDate2" class="mini-datepicker" labelField="true" label="~"
               onValuechanged="compareDateSta2" labelStyle="text-align:center;" emptyText="请选择起息结束日期"/>
        <input id="mDate1" name="mDate1" class="mini-datepicker" labelField="true" onValuechanged="compareDateEnd1"
               label="计息到期日：" labelStyle="text-align:right;" emptyText="请选择到计息起始日期"/>
        <input id="mDate2" name="mDate2" class="mini-datepicker" labelField="true" onValuechanged="compareDateEnd2"
               label="~" labelStyle="text-align:center;" emptyText="请选择计息到日结束日期"/>
        <input id="confirmFlag" name="confirmFlag" class="mini-combobox" value="2"
               data="[{text:'已确认',id:'1'},{text:'未确认',id:'2'}]" labelField="true" label="确认状态："
               labelStyle="text-align:right;"></input>
        <span style="float:right;margin-right: 150px">
                                            <a id="search_btn" class="mini-button" style="display: none"
                                               onclick="search(10,0)">查询</a>
                                            <a id="clear_btn" class="mini-button" style="display: none"
                                               onclick='clear()'>清空</a>
                                    </span>
    </div>
</fieldset>
<span style="margin:2px;display: block;">
                    <a id="save_btn" class="mini-button" style="display: none" onclick="save_btn();">确认</a>
                    <a id="print_btn" class="mini-button" style="display: none" onclick="print();">打印</a>

            </span>
<div id="OrderSortingManage" class="mini-fit">
    <!-- <fieldset id="fd2" class="mini-fieldset">
            <legend><label>收息计划列表</label></legend> -->
    <div id="CashFlowConfirmManages" class="mini-datagrid borderAll" style="width:100%;height:100%;"
         allowResize="true" pageSize="10" sortMode="client" allowAlternating="true" idField="dealNo" border="true"
         frozenStartColumn="0" frozenEndColumn="2">
        <div property="columns">
            <div type="indexcolumn" headerAlign="center" align="center" width="50px">序号</div>
            <div field="cashflowId" width="80" align="center" headerAlign="center" align="center">现金流序列号</div>
            <div field="dealNo" width="120" align="center" headerAlign="center" align="center" allowSort="true">交易编号
            </div>
            <div field="instName" width="180" align="center" headerAlign="center" align="left">营销机构</div>
            <div field="prdName" width="150" align="left" headerAlign="center">资产名称</div>
            <div field="dayCounter" width="80" align="center" headerAlign="center">计息基率</div>
            <div field="executeRate" width="80" align="right" headerAlign="center" numberFormat="p4">计息利率</div>
            <div field="refBeginDate" width="80" align="center" headerAlign="center">计息起息日</div>
            <div field="refEndDate" width="80" align="center" headerAlign="center">计息到期日</div>
            <div field="theoryPaymentDate" width="80" align="center" headerAlign="center">计划收息日</div>
            <div field="interestAmt" width="120" align="right" headerAlign="center" numberFormat="#,0.00">应收利息金额</div>
            <div field="actualRamt" width="120" align="right" headerAlign="center" allowSort="false"
                 numberFormat="#,0.00">已收利息金额
            </div>
            <div field="amt" width="120" align="right" headerAlign="center" allowSort="false" numberFormat="#,0.00">本金
            </div>
            <!-- <div field="partyAccCode" width="150" align="center"  headerAlign="center" allowSort="false">账号</div>
            <div field="partyBankName" width="150" align="center"  headerAlign="center" allowSort="false">保管行</div> -->
            <div field="confirmFlag" width="80" align="center" headerAlign="center" allowSort="false"
                 renderer="onConfirmFlag">确认状态
            </div>
        </div>

    </div>
    <!--  </fieldset> -->
</div>

<script>

    mini.parse();
    var form = new mini.Form("search_form");  //提交表单
    var grid = mini.get("CashFlowConfirmManages");
    //分页
    var grid = mini.get("CashFlowConfirmManages");
    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        ////console.log("起始页="+pageIndex);
        var pageSize = e.data.pageSize;
        ////console.log("每页大小="+pageSize);
        search(pageSize, pageIndex);
    });
    $(document).ready(function () {
        search(10, 0);
    });

    function compareDate() {
        var sta = mini.get("vDate1");
        var end = mini.get("vDate2");
        if (sta.getValue() > end.getValue() && end.getValue("") && sta.getValue() != null) {
            mini.alert("开始时间不能大于结束时间", "系统提示");
            return end.setValue(sta.getValue());
        }
    }

    function compareDateSta2() {
        var sta = mini.get("vDate1");
        var end = mini.get("vDate2");
        if (end.getValue() != null && end.getValue("") && end.getValue() < sta.getValue()) {
            mini.alert("结束时间不能小于开始时间", "系统提示");
            return sta.setValue(end.getValue());
        }
    }

    function compareDateEnd1() {
        /*  var vDate2 = mini.get("vDate2");
         var mDate1 = mini.get("mDate1");
         if (vDate2.getValue() > mDate1.getValue() && mDate1.getValue("")) {
             mini.alert("计息的起息日的结束日期不能大于计息起息到期的开始日期", "系统提示");
             return mDate1.setValue(vDate2.getValue());
         } */
        var sta = mini.get("mDate1");
        var end = mini.get("mDate2");
        if (sta.getValue() > end.getValue() && end.getValue("")) {
            mini.alert("开始时间不能大于结束时间", "系统提示");
            return end.setValue(sta.getValue());
        }
    }

    function compareDateEnd2() {
        var sta = mini.get("mDate1");
        var end = mini.get("mDate2");
        if (end.getValue() != null && end.getValue("") && end.getValue() < sta.getValue()) {
            mini.alert("结束时间不能小于开始时间", "系统提示");
            return sta.setValue(end.getValue());
        }
    }

    //确认
    function save_btn() {
        var grid = mini.get("CashFlowConfirmManages");
        var row = grid.getSelected();
        if (row) {
            if (row.confirmFlag == '1') {
                mini.alert("该条收息计划已被确认", "提示")
            } else {
                var str = "";
                var data = {};
                var reg = /,$/gi;
                str = str.replace(reg, "");
                data['cashflowId'] = row.cashflowId;
                CommonUtil.ajax({
                    url: "/TdCashflowInterestController/saveCashFlowConfirm",
                    data: data,
                    callback: function (data) {
                        mini.alert("确认成功", "系统提示")
                        search(10, 0);
                    }
                });
            }

        } else {
            mini.alert("请先选择一条记录", "操作提示");
        }


    }

    //打印
    function print() {
        var selections = grid.getSelecteds();
        if (selections == null || selections.length == 0) {
            mini.alert('请选择一条要打印的数据！', '系统提示');
            return false;
        } else if (selections.length > 1) {
            mini.alert('暂不支持多条数据同时打印，请选择一条要打印的数据！', '系统提示');
            return false;
        }
        var canPrint = selections[0].cashflowId; //cashflowId;

        if (!CommonUtil.isNull(canPrint)) {
            var actionStr = CommonUtil.pPath + "/sl/PrintUtilController/exportload/print/1800/" + canPrint;
            $('<form target="_blank" action=' + actionStr + ' method="post"></form>').appendTo('body').submit().remove();
        }
        ;
    }

    //查询
    function search(pageSize, pageIndex) {
        form.validate();              //表单验证
        var data = form.getData(true);      //获取表单数据
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        var param = mini.encode(data);   //序列化json格式
        CommonUtil.ajax({
            url: '/TdCashflowInterestController/searchPageCfComfirm',
            data: param,
            callback: function (data) {
                // initParam();
                //设置分页
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);

            }
        });

    }

    var confirmFlag = [{"id": "1", "text": "确认"}, {"id": null, "text": "未确认"}, {"id": '', "text": "未确认"}];

    function onConfirmFlag(e) {
        for (var i = 0, l = confirmFlag.length; i < l; i++) {
            var g = confirmFlag[i];
            if (g.id == e.value) return g.text;
        }
        return "";
    }

    //清空查询条件
    function clear() {
        form.clear();
    }
</script>

</body>
</html>