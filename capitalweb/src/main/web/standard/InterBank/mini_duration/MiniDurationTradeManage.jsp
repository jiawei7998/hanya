<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
    <title></title>
  </head>
  
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
	<legend>交易查询</legend>
	<div id="TradeManage">
		<div id="search_form" class="mini-form" width="80%">
			<input id="trade_id" name="trade_id" class="mini-textbox" vtype="maxLength:50" labelField="true" labelStyle="text-align:right;"  label="交易单号："
			  emptyText="请输入交易单号"/>
			<input id="party_name" name="party_name" class="mini-textbox" labelField="true" labelStyle="text-align:right;"  label="客户名称："
			  emptyText="请输入客户名称"/>
			<input id="product_name_search" name="product_name_search"  class="mini-textbox" labelField="true" labelStyle="text-align:right;" label="产品名称："
			  emptyText="请输入产品名称"/>
			
			<span style="float:right;margin-right: 100px">
				<a  id="search_btn" class="mini-button" style="display: none"   onclick="search(10,0)">查询</a>
				<a  id="clear_btn" class="mini-button" style="display: none"   onclick='clear()'>清空</a>
			</span>
		</div>
	</div>
</fieldset>
<div id="TradeManage" class="mini-fit">   
	<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" 
	idField="dealNo" allowResize="true" onrowdblclick="onRowDblClick" border="true" sizeList="[10,20]">
		<div property="columns" >
			<div type="indexcolumn" headerAlign="center" width="40">序号</div>
			<div field="dealNo" width="180" align="center"  headerAlign="center" allowSort="true">交易单号</div>    
			<div field="counterParty.party_name" width="200" headerAlign="center" allowSort="true">客户简称</div>                            
			<div field="product.prdName" width="180" headerAlign="center" >产品名称</div>                                
			<div field="amt" width="120" numberFormat="#,0.00" align="right" headerAlign="center" allowSort="true" >合同额（元）</div>
			<div field="vDate" width="80" align="center" headerAlign="center" >起息日</div>
		</div>
	</div>
</div> 
<script type="text/javascript">
	mini.parse();
	var url=window.location.search;
	var mDate=CommonUtil.getParam(url,"mDate");
	var offsetDate=CommonUtil.getParam(url,"offsetDate");
	var businessType=CommonUtil.getParam(url,"businessType");
	var trdtype=CommonUtil.getParam(url,"trdtype");
	var dealNoStr ='<%=request.getParameter("dealNoStr") %>';
	var grid=mini.get("datagrid");
	var form=new mini.Form("#search_form");
	//清空
	function clear(){
		form.clear();
	}
	//查询
	function search(pageSize,pageIndex){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误","系统提示");
			return ;
		}
		var data=form.getData();
		data['pageSize']=pageSize;
		data['pageNumber']=pageIndex+1;
		data['businessType']=businessType;
		data['branchId']=branchId;
		data['trdtype']=trdtype;
		if(mDate!=null&&mDate!=""){
			data['mDate']=mDate;
		}
		if(offsetDate!=null&&offsetDate!=""){
			data['offsetDate']=offsetDate;
		}
		var params=mini.encode(data);
		CommonUtil.ajax({
			url:"/trdTposController/searchDurationCustomVoPageByType",
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}

	grid.on("beforeload",function(e){
		e.cancel = true;
		var pageSize=e.data.pageSize;
		var pageIndex=e.data.pageIndex;
		search(pageSize,pageIndex);
	});

	function onRowDblClick(e){
		var row=e.record;
		if(dealNoStr.indexOf(row.dealNo)>=0){
			CloseWindow("ok");			 
		}else{
			if(businessType != "investFollowup"){
				//检查原交易是否已存在存续期业务交易
				CommonUtil.ajax({
					url:"/DurationController/ifExistDuration",
					data:{'refNo':row.dealNo},
					callback : function(data) {
						if(data.obj != null){
							if(data.obj.sponsor != null){
								mini.alert('该交易已存在存续期业务<br>【业务类型：'+data.obj.remark+'】<br>【业务员：'+data.obj.sponsor+'】<br>【单号：'+data.obj.dealNo+'】','系统提示');
							}else{
								mini.alert( '该交易已存在未到期存续期业务<br>【单号：'+data.obj.dealNo+'】','系统提示');
							}
							return false;
						}else{
							CloseWindow("ok");							}
					}
				});
			}else{
				CloseWindow("ok");				  
			}
		}
	}
	function CloseWindow(action) {
	   if (window.CloseOwnerWindow) return window.CloseOwnerWindow(action);
	   else window.close();
	}
	function GetData() {
	    var row = grid.getSelected();
	    return row;
	}


	$(document).ready(function(){
		search(grid.pageSize,0);
	});
</script>
</body>
</html>